	.file	"csa-generator.cc"
	.text
	.section	.text._ZNK2v88internal6torque15InstructionBase17IsBlockTerminatorEv,"axG",@progbits,_ZNK2v88internal6torque15InstructionBase17IsBlockTerminatorEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal6torque15InstructionBase17IsBlockTerminatorEv
	.type	_ZNK2v88internal6torque15InstructionBase17IsBlockTerminatorEv, @function
_ZNK2v88internal6torque15InstructionBase17IsBlockTerminatorEv:
.LFB6070:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE6070:
	.size	_ZNK2v88internal6torque15InstructionBase17IsBlockTerminatorEv, .-_ZNK2v88internal6torque15InstructionBase17IsBlockTerminatorEv
	.section	.text._ZNK2v88internal6torque15InstructionBase21AppendSuccessorBlocksEPSt6vectorIPNS1_5BlockESaIS5_EE,"axG",@progbits,_ZNK2v88internal6torque15InstructionBase21AppendSuccessorBlocksEPSt6vectorIPNS1_5BlockESaIS5_EE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal6torque15InstructionBase21AppendSuccessorBlocksEPSt6vectorIPNS1_5BlockESaIS5_EE
	.type	_ZNK2v88internal6torque15InstructionBase21AppendSuccessorBlocksEPSt6vectorIPNS1_5BlockESaIS5_EE, @function
_ZNK2v88internal6torque15InstructionBase21AppendSuccessorBlocksEPSt6vectorIPNS1_5BlockESaIS5_EE:
.LFB6071:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE6071:
	.size	_ZNK2v88internal6torque15InstructionBase21AppendSuccessorBlocksEPSt6vectorIPNS1_5BlockESaIS5_EE, .-_ZNK2v88internal6torque15InstructionBase21AppendSuccessorBlocksEPSt6vectorIPNS1_5BlockESaIS5_EE
	.section	.rodata._ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0.str1.1,"aMS",@progbits,1
.LC0:
	.string	"%lu"
	.section	.text._ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0,"ax",@progbits
	.p2align 4
	.type	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0, @function
_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0:
.LFB12390:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$232, %rsp
	.cfi_offset 3, -40
	movq	%r8, -176(%rbp)
	movq	%r9, -168(%rbp)
	testb	%al, %al
	je	.L5
	movaps	%xmm0, -160(%rbp)
	movaps	%xmm1, -144(%rbp)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm3, -112(%rbp)
	movaps	%xmm4, -96(%rbp)
	movaps	%xmm5, -80(%rbp)
	movaps	%xmm6, -64(%rbp)
	movaps	%xmm7, -48(%rbp)
.L5:
	movq	%fs:40, %rax
	movq	%rax, -216(%rbp)
	xorl	%eax, %eax
	movq	%rsp, %rax
	cmpq	%rax, %rsp
	je	.L7
.L27:
	subq	$4096, %rsp
	orq	$0, 4088(%rsp)
	cmpq	%rax, %rsp
	jne	.L27
.L7:
	subq	$48, %rsp
	orq	$0, 40(%rsp)
	leaq	.LC0(%rip), %r8
	movl	$32, %ecx
	movl	$32, %esi
	leaq	15(%rsp), %rdx
	leaq	16(%rbp), %rax
	movl	$32, -240(%rbp)
	movq	%rdx, %rbx
	movl	$1, %edx
	movq	%rax, -232(%rbp)
	leaq	-240(%rbp), %r9
	andq	$-16, %rbx
	leaq	-208(%rbp), %rax
	movl	$48, -236(%rbp)
	movq	%rbx, %rdi
	movq	%rax, -224(%rbp)
	call	__vsnprintf_chk@PLT
	movslq	%eax, %rdx
	leaq	16(%r12), %rax
	movq	%rax, (%r12)
	movq	%rdx, %r13
	movq	%rdx, -248(%rbp)
	cmpl	$15, %edx
	jg	.L28
	cmpq	$1, %rdx
	jne	.L10
	movzbl	(%rbx), %ecx
	movb	%cl, 16(%r12)
.L11:
	movq	%rdx, 8(%r12)
	movb	$0, (%rax,%rdx)
	movq	-216(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L29
	leaq	-24(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L28:
	.cfi_restore_state
	leaq	-248(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-248(%rbp), %rcx
	movq	%rax, (%r12)
	movq	%rcx, 16(%r12)
.L10:
	cmpl	$8, %r13d
	jnb	.L12
	testb	$4, %r13b
	jne	.L30
	testl	%r13d, %r13d
	je	.L13
	movzbl	(%rbx), %edx
	movb	%dl, (%rax)
	testb	$2, %r13b
	jne	.L31
.L13:
	movq	-248(%rbp), %rdx
	movq	(%r12), %rax
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L12:
	movq	(%rbx), %rdx
	leaq	8(%rax), %rdi
	andq	$-8, %rdi
	movq	%rdx, (%rax)
	movl	%r13d, %edx
	movq	-8(%rbx,%rdx), %rcx
	movq	%rcx, -8(%rax,%rdx)
	subq	%rdi, %rax
	subq	%rax, %rbx
	addl	%r13d, %eax
	andl	$-8, %eax
	movq	%rbx, %rdx
	cmpl	$8, %eax
	jb	.L13
	andl	$-8, %eax
	xorl	%ecx, %ecx
.L16:
	movl	%ecx, %esi
	addl	$8, %ecx
	movq	(%rdx,%rsi), %r8
	movq	%r8, (%rdi,%rsi)
	cmpl	%eax, %ecx
	jb	.L16
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L30:
	movl	(%rbx), %edx
	movl	%r13d, %r13d
	movl	%edx, (%rax)
	movl	-4(%rbx,%r13), %edx
	movl	%edx, -4(%rax,%r13)
	jmp	.L13
.L31:
	movl	%r13d, %r13d
	movzwl	-2(%rbx,%r13), %edx
	movw	%dx, -2(%rax,%r13)
	jmp	.L13
.L29:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE12390:
	.size	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0, .-_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0
	.section	.text._ZN2v88internal6torque14MessageBuilderD2Ev,"axG",@progbits,_ZN2v88internal6torque14MessageBuilderD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque14MessageBuilderD2Ev
	.type	_ZN2v88internal6torque14MessageBuilderD2Ev, @function
_ZN2v88internal6torque14MessageBuilderD2Ev:
.LFB4827:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA4827
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	addq	$16, %rbx
	subq	$8, %rsp
	call	_ZNK2v88internal6torque14MessageBuilder6ReportEv@PLT
	movq	-16(%rbx), %rdi
	cmpq	%rbx, %rdi
	je	.L32
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L32:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4827:
	.globl	__gxx_personality_v0
	.section	.gcc_except_table._ZN2v88internal6torque14MessageBuilderD2Ev,"aG",@progbits,_ZN2v88internal6torque14MessageBuilderD5Ev,comdat
.LLSDA4827:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE4827-.LLSDACSB4827
.LLSDACSB4827:
.LLSDACSE4827:
	.section	.text._ZN2v88internal6torque14MessageBuilderD2Ev,"axG",@progbits,_ZN2v88internal6torque14MessageBuilderD5Ev,comdat
	.size	_ZN2v88internal6torque14MessageBuilderD2Ev, .-_ZN2v88internal6torque14MessageBuilderD2Ev
	.weak	_ZN2v88internal6torque14MessageBuilderD1Ev
	.set	_ZN2v88internal6torque14MessageBuilderD1Ev,_ZN2v88internal6torque14MessageBuilderD2Ev
	.section	.text._ZN2v88internal6torquelsERSoRKNS1_4TypeE,"axG",@progbits,_ZN2v88internal6torquelsERSoRKNS1_4TypeE,comdat
	.p2align 4
	.weak	_ZN2v88internal6torquelsERSoRKNS1_4TypeE
	.type	_ZN2v88internal6torquelsERSoRKNS1_4TypeE, @function
_ZN2v88internal6torquelsERSoRKNS1_4TypeE:
.LFB5969:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA5969
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-64(%rbp), %rdi
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
.LEHB0:
	call	_ZNK2v88internal6torque4Type8ToStringB5cxx11Ev@PLT
.LEHE0:
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rsi
	movq	%r12, %rdi
.LEHB1:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE1:
	movq	-64(%rbp), %rdi
	leaq	-48(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L36
	call	_ZdlPv@PLT
.L36:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L43
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L43:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
.L40:
	endbr64
	movq	%rax, %r12
.L37:
	movq	-64(%rbp), %rdi
	leaq	-48(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L38
	call	_ZdlPv@PLT
.L38:
	movq	%r12, %rdi
.LEHB2:
	call	_Unwind_Resume@PLT
.LEHE2:
	.cfi_endproc
.LFE5969:
	.section	.gcc_except_table._ZN2v88internal6torquelsERSoRKNS1_4TypeE,"aG",@progbits,_ZN2v88internal6torquelsERSoRKNS1_4TypeE,comdat
.LLSDA5969:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE5969-.LLSDACSB5969
.LLSDACSB5969:
	.uleb128 .LEHB0-.LFB5969
	.uleb128 .LEHE0-.LEHB0
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB1-.LFB5969
	.uleb128 .LEHE1-.LEHB1
	.uleb128 .L40-.LFB5969
	.uleb128 0
	.uleb128 .LEHB2-.LFB5969
	.uleb128 .LEHE2-.LEHB2
	.uleb128 0
	.uleb128 0
.LLSDACSE5969:
	.section	.text._ZN2v88internal6torquelsERSoRKNS1_4TypeE,"axG",@progbits,_ZN2v88internal6torquelsERSoRKNS1_4TypeE,comdat
	.size	_ZN2v88internal6torquelsERSoRKNS1_4TypeE, .-_ZN2v88internal6torquelsERSoRKNS1_4TypeE
	.section	.rodata._ZN2v88internal6torque12CSAGenerator18EmitSourcePositionENS1_14SourcePositionEb.str1.1,"aMS",@progbits,1
.LC1:
	.string	"    ca_.SetSourcePosition(\""
.LC2:
	.string	"\", "
.LC3:
	.string	");\n"
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator18EmitSourcePositionENS1_14SourcePositionEb,"ax",@progbits
	.align 2
.LCOLDB4:
	.section	.text._ZN2v88internal6torque12CSAGenerator18EmitSourcePositionENS1_14SourcePositionEb,"ax",@progbits
.LHOTB4:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque12CSAGenerator18EmitSourcePositionENS1_14SourcePositionEb
	.type	_ZN2v88internal6torque12CSAGenerator18EmitSourcePositionENS1_14SourcePositionEb, @function
_ZN2v88internal6torque12CSAGenerator18EmitSourcePositionENS1_14SourcePositionEb:
.LFB6913:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6913
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%esi, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	leaq	-80(%rbp), %rdi
	subq	$56, %rsp
	movl	16(%rbp), %esi
	movl	20(%rbp), %r12d
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
.LEHB3:
	call	_ZN2v88internal6torque13SourceFileMap12AbsolutePathB5cxx11ENS1_8SourceIdE@PLT
.LEHE3:
	testb	%r13b, %r13b
	jne	.L45
	cmpl	%r12d, 36(%rbx)
	je	.L57
.L45:
	movq	-80(%rbp), %rax
	movq	-72(%rbp), %rdx
	addq	%rax, %rdx
	cmpq	%rax, %rdx
	je	.L47
	.p2align 4,,10
	.p2align 3
.L49:
	cmpb	$92, (%rax)
	jne	.L48
	movb	$47, (%rax)
.L48:
	addq	$1, %rax
	cmpq	%rdx, %rax
	jne	.L49
.L47:
	movq	8(%rbx), %r13
	movl	$27, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r13, %rdi
.LEHB4:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-72(%rbp), %rdx
	movq	-80(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$3, %edx
	leaq	.LC2(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leal	1(%r12), %esi
	movq	%r13, %rdi
	call	_ZNSolsEi@PLT
	movq	%rax, %rdi
	movl	$3, %edx
	leaq	.LC3(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE4:
	movl	32(%rbp), %eax
	movdqu	16(%rbp), %xmm0
	movl	%eax, 48(%rbx)
	movups	%xmm0, 32(%rbx)
.L46:
	movq	-80(%rbp), %rdi
	leaq	-64(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L44
	call	_ZdlPv@PLT
.L44:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L58
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L57:
	.cfi_restore_state
	movl	16(%rbp), %eax
	cmpl	%eax, 32(%rbx)
	jne	.L45
	jmp	.L46
.L58:
	call	__stack_chk_fail@PLT
.L54:
	endbr64
	movq	%rax, %r12
	jmp	.L51
	.section	.gcc_except_table._ZN2v88internal6torque12CSAGenerator18EmitSourcePositionENS1_14SourcePositionEb,"a",@progbits
.LLSDA6913:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6913-.LLSDACSB6913
.LLSDACSB6913:
	.uleb128 .LEHB3-.LFB6913
	.uleb128 .LEHE3-.LEHB3
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB4-.LFB6913
	.uleb128 .LEHE4-.LEHB4
	.uleb128 .L54-.LFB6913
	.uleb128 0
.LLSDACSE6913:
	.section	.text._ZN2v88internal6torque12CSAGenerator18EmitSourcePositionENS1_14SourcePositionEb
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator18EmitSourcePositionENS1_14SourcePositionEb
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6913
	.type	_ZN2v88internal6torque12CSAGenerator18EmitSourcePositionENS1_14SourcePositionEb.cold, @function
_ZN2v88internal6torque12CSAGenerator18EmitSourcePositionENS1_14SourcePositionEb.cold:
.LFSB6913:
.L51:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movq	-80(%rbp), %rdi
	leaq	-64(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L52
	call	_ZdlPv@PLT
.L52:
	movq	%r12, %rdi
.LEHB5:
	call	_Unwind_Resume@PLT
.LEHE5:
	.cfi_endproc
.LFE6913:
	.section	.gcc_except_table._ZN2v88internal6torque12CSAGenerator18EmitSourcePositionENS1_14SourcePositionEb
.LLSDAC6913:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6913-.LLSDACSBC6913
.LLSDACSBC6913:
	.uleb128 .LEHB5-.LCOLDB4
	.uleb128 .LEHE5-.LEHB5
	.uleb128 0
	.uleb128 0
.LLSDACSEC6913:
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator18EmitSourcePositionENS1_14SourcePositionEb
	.section	.text._ZN2v88internal6torque12CSAGenerator18EmitSourcePositionENS1_14SourcePositionEb
	.size	_ZN2v88internal6torque12CSAGenerator18EmitSourcePositionENS1_14SourcePositionEb, .-_ZN2v88internal6torque12CSAGenerator18EmitSourcePositionENS1_14SourcePositionEb
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator18EmitSourcePositionENS1_14SourcePositionEb
	.size	_ZN2v88internal6torque12CSAGenerator18EmitSourcePositionENS1_14SourcePositionEb.cold, .-_ZN2v88internal6torque12CSAGenerator18EmitSourcePositionENS1_14SourcePositionEb.cold
.LCOLDE4:
	.section	.text._ZN2v88internal6torque12CSAGenerator18EmitSourcePositionENS1_14SourcePositionEb
.LHOTE4:
	.section	.rodata._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_15PokeInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC5:
	.string	"vector::_M_range_check: __n (which is %zu) >= this->size() (which is %zu)"
	.align 8
.LC6:
	.string	"basic_string::_M_construct null not valid"
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_15PokeInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE,"ax",@progbits
	.align 2
.LCOLDB7:
	.section	.text._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_15PokeInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE,"ax",@progbits
.LHOTB7:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_15PokeInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.type	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_15PokeInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE, @function
_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_15PokeInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE:
.LFB6916:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6916
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$72, %rsp
	movq	(%rdx), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8(%rbx), %rax
	subq	%rdx, %rax
	je	.L96
	leaq	-32(%rdx,%rax), %rax
	leaq	-80(%rbp), %r13
	movq	%rsi, %r15
	movq	%r13, -96(%rbp)
	movq	(%rax), %r14
	movq	8(%rax), %r12
	movq	%r14, %rax
	addq	%r12, %rax
	je	.L61
	testq	%r14, %r14
	je	.L97
.L61:
	movq	%r12, -104(%rbp)
	cmpq	$15, %r12
	ja	.L98
	cmpq	$1, %r12
	jne	.L64
	movzbl	(%r14), %eax
	movb	%al, -80(%rbp)
	movq	%r13, %rax
.L65:
	movq	%r12, -88(%rbp)
	movb	$0, (%rax,%r12)
	movq	(%rbx), %rsi
	movq	8(%rbx), %rdx
	movq	32(%r15), %r12
	subq	%rsi, %rdx
	sarq	$5, %rdx
	cmpq	%r12, %rdx
	jbe	.L99
	salq	$5, %r12
	movq	-96(%rbp), %rax
	addq	%rsi, %r12
	movq	(%r12), %rdi
	cmpq	%r13, %rax
	je	.L100
	leaq	16(%r12), %rdx
	cmpq	%rdx, %rdi
	je	.L101
	movq	%rax, (%r12)
	movq	-88(%rbp), %rax
	movq	16(%r12), %rdx
	movq	%rax, 8(%r12)
	movq	-80(%rbp), %rax
	movq	%rax, 16(%r12)
	testq	%rdi, %rdi
	je	.L72
	movq	%rdi, -96(%rbp)
	movq	%rdx, -80(%rbp)
.L70:
	movq	$0, -88(%rbp)
	movb	$0, (%rdi)
	movq	-96(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L73
	call	_ZdlPv@PLT
.L73:
	movq	8(%rbx), %rax
	movq	%r13, -96(%rbp)
	movq	-32(%rax), %rcx
	leaq	-16(%rax), %rdx
	cmpq	%rdx, %rcx
	je	.L102
	movq	%rcx, -96(%rbp)
	movq	-16(%rax), %rcx
	movq	%rcx, -80(%rbp)
.L75:
	movq	-24(%rax), %rcx
	movq	%rcx, -88(%rbp)
	movq	%rdx, -32(%rax)
	movq	$0, -24(%rax)
	movb	$0, -16(%rax)
	movq	8(%rbx), %rax
	leaq	-32(%rax), %rdx
	subq	$16, %rax
	movq	%rdx, 8(%rbx)
	movq	-16(%rax), %rdi
	cmpq	%rax, %rdi
	je	.L76
	call	_ZdlPv@PLT
.L76:
	movq	-96(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L59
	call	_ZdlPv@PLT
.L59:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L103
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L64:
	.cfi_restore_state
	testq	%r12, %r12
	jne	.L104
	movq	%r13, %rax
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L98:
	leaq	-96(%rbp), %rdi
	leaq	-104(%rbp), %rsi
	xorl	%edx, %edx
.LEHB6:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -96(%rbp)
	movq	%rax, %rdi
	movq	-104(%rbp), %rax
	movq	%rax, -80(%rbp)
.L63:
	movq	%r12, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-104(%rbp), %r12
	movq	-96(%rbp), %rax
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L100:
	movq	-88(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L68
	cmpq	$1, %rdx
	je	.L105
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	-88(%rbp), %rdx
	movq	(%r12), %rdi
.L68:
	movq	%rdx, 8(%r12)
	movb	$0, (%rdi,%rdx)
	movq	-96(%rbp), %rdi
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L102:
	movdqu	-16(%rax), %xmm0
	movaps	%xmm0, -80(%rbp)
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L101:
	movq	%rax, (%r12)
	movq	-88(%rbp), %rax
	movq	%rax, 8(%r12)
	movq	-80(%rbp), %rax
	movq	%rax, 16(%r12)
.L72:
	movq	%r13, -96(%rbp)
	leaq	-80(%rbp), %r13
	movq	%r13, %rdi
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L105:
	movzbl	-80(%rbp), %eax
	movb	%al, (%rdi)
	movq	-88(%rbp), %rdx
	movq	(%r12), %rdi
	jmp	.L68
.L97:
	leaq	.LC6(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE6:
.L99:
	movq	%r12, %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
.LEHB7:
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.LEHE7:
.L103:
	call	__stack_chk_fail@PLT
.L96:
	xorl	%edx, %edx
	orq	$-1, %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
.LEHB8:
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.LEHE8:
.L104:
	movq	%r13, %rdi
	jmp	.L63
.L82:
	endbr64
	movq	%rax, %r12
	jmp	.L78
	.section	.gcc_except_table._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_15PokeInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE,"a",@progbits
.LLSDA6916:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6916-.LLSDACSB6916
.LLSDACSB6916:
	.uleb128 .LEHB6-.LFB6916
	.uleb128 .LEHE6-.LEHB6
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB7-.LFB6916
	.uleb128 .LEHE7-.LEHB7
	.uleb128 .L82-.LFB6916
	.uleb128 0
	.uleb128 .LEHB8-.LFB6916
	.uleb128 .LEHE8-.LEHB8
	.uleb128 0
	.uleb128 0
.LLSDACSE6916:
	.section	.text._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_15PokeInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_15PokeInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6916
	.type	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_15PokeInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.cold, @function
_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_15PokeInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.cold:
.LFSB6916:
.L78:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-96(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L79
	call	_ZdlPv@PLT
.L79:
	movq	%r12, %rdi
.LEHB9:
	call	_Unwind_Resume@PLT
.LEHE9:
	.cfi_endproc
.LFE6916:
	.section	.gcc_except_table._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_15PokeInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
.LLSDAC6916:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6916-.LLSDACSBC6916
.LLSDACSBC6916:
	.uleb128 .LEHB9-.LCOLDB7
	.uleb128 .LEHE9-.LEHB9
	.uleb128 0
	.uleb128 0
.LLSDACSEC6916:
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_15PokeInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.section	.text._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_15PokeInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.size	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_15PokeInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE, .-_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_15PokeInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_15PokeInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.size	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_15PokeInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.cold, .-_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_15PokeInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.cold
.LCOLDE7:
	.section	.text._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_15PokeInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
.LHOTE7:
	.section	.rodata._ZN2v88internal6torque12CSAGenerator31PreCallableExceptionPreparationB5cxx11ENS_4base8OptionalIPNS1_5BlockEEE.str1.1,"aMS",@progbits,1
.LC8:
	.string	"catch"
	.section	.rodata._ZN2v88internal6torque12CSAGenerator31PreCallableExceptionPreparationB5cxx11ENS_4base8OptionalIPNS1_5BlockEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC9:
	.string	"    compiler::CodeAssemblerExceptionHandlerLabel "
	.align 8
.LC10:
	.string	"__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);\n"
	.align 8
.LC11:
	.string	"    { compiler::CodeAssemblerScopedExceptionHandler s(&ca_, &"
	.section	.rodata._ZN2v88internal6torque12CSAGenerator31PreCallableExceptionPreparationB5cxx11ENS_4base8OptionalIPNS1_5BlockEEE.str1.1
.LC12:
	.string	"__label);\n"
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator31PreCallableExceptionPreparationB5cxx11ENS_4base8OptionalIPNS1_5BlockEEE,"ax",@progbits
	.align 2
.LCOLDB13:
	.section	.text._ZN2v88internal6torque12CSAGenerator31PreCallableExceptionPreparationB5cxx11ENS_4base8OptionalIPNS1_5BlockEEE,"ax",@progbits
.LHOTB13:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque12CSAGenerator31PreCallableExceptionPreparationB5cxx11ENS_4base8OptionalIPNS1_5BlockEEE
	.type	_ZN2v88internal6torque12CSAGenerator31PreCallableExceptionPreparationB5cxx11ENS_4base8OptionalIPNS1_5BlockEEE, @function
_ZN2v88internal6torque12CSAGenerator31PreCallableExceptionPreparationB5cxx11ENS_4base8OptionalIPNS1_5BlockEEE:
.LFB6943:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6943
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	16(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$80, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%r13, (%rdi)
	movq	$0, 8(%rdi)
	movb	$0, 16(%rdi)
	testb	%dl, %dl
	jne	.L134
.L106:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L135
	addq	$80, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L134:
	.cfi_restore_state
	movq	16(%rsi), %r8
	movq	%rsi, %rbx
	leaq	-80(%rbp), %r14
	movl	$32, %edx
	leaq	.LC0(%rip), %rcx
	movq	%r14, %rdi
	leaq	1(%r8), %rax
	movq	%rax, 16(%rsi)
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	xorl	%eax, %eax
.LEHB10:
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0
.LEHE10:
	movl	$5, %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r14, %rdi
	leaq	.LC8(%rip), %rcx
.LEHB11:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.LEHE11:
	leaq	-96(%rbp), %r14
	leaq	16(%rax), %rdx
	movq	%r14, -112(%rbp)
	movq	(%rax), %rcx
	cmpq	%rdx, %rcx
	je	.L136
	movq	%rcx, -112(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -96(%rbp)
.L109:
	movq	8(%rax), %rcx
	movb	$0, 16(%rax)
	movq	%rcx, -104(%rbp)
	movq	%rdx, (%rax)
	movq	-80(%rbp), %rdi
	movq	$0, 8(%rax)
	leaq	-64(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L110
	call	_ZdlPv@PLT
.L110:
	movq	-112(%rbp), %rax
	movq	(%r12), %rdi
	movq	-104(%rbp), %rdx
	cmpq	%r14, %rax
	je	.L137
	movq	-96(%rbp), %rcx
	cmpq	%rdi, %r13
	je	.L138
	movq	%rdx, %xmm0
	movq	%rcx, %xmm1
	movq	16(%r12), %rsi
	movq	%rax, (%r12)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 8(%r12)
	testq	%rdi, %rdi
	je	.L120
	movq	%rdi, -112(%rbp)
	movq	%rsi, -96(%rbp)
.L118:
	movq	$0, -104(%rbp)
	movb	$0, (%rdi)
	movq	-112(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L121
	call	_ZdlPv@PLT
.L121:
	movq	8(%rbx), %r14
	movl	$49, %edx
	leaq	.LC9(%rip), %rsi
	movq	%r14, %rdi
.LEHB12:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%r12), %rdx
	movq	(%r12), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rax, %rdi
	movl	$56, %edx
	leaq	.LC10(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%rbx), %r14
	movl	$61, %edx
	leaq	.LC11(%rip), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%r12), %rdx
	movq	(%r12), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rax, %rdi
	movl	$10, %edx
	leaq	.LC12(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE12:
	jmp	.L106
	.p2align 4,,10
	.p2align 3
.L138:
	movq	%rdx, %xmm0
	movq	%rcx, %xmm3
	movq	%rax, (%r12)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 8(%r12)
.L120:
	movq	%r14, -112(%rbp)
	leaq	-96(%rbp), %r14
	movq	%r14, %rdi
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L136:
	movdqu	16(%rax), %xmm2
	movaps	%xmm2, -96(%rbp)
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L137:
	testq	%rdx, %rdx
	je	.L116
	cmpq	$1, %rdx
	je	.L139
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-104(%rbp), %rdx
	movq	(%r12), %rdi
.L116:
	movq	%rdx, 8(%r12)
	movb	$0, (%rdi,%rdx)
	movq	-112(%rbp), %rdi
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L139:
	movzbl	-96(%rbp), %eax
	movb	%al, (%rdi)
	movq	-104(%rbp), %rdx
	movq	(%r12), %rdi
	jmp	.L116
.L135:
	call	__stack_chk_fail@PLT
.L124:
	endbr64
	movq	%rax, %r14
	jmp	.L115
.L125:
	endbr64
	movq	%rax, %r14
	jmp	.L113
	.section	.gcc_except_table._ZN2v88internal6torque12CSAGenerator31PreCallableExceptionPreparationB5cxx11ENS_4base8OptionalIPNS1_5BlockEEE,"a",@progbits
.LLSDA6943:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6943-.LLSDACSB6943
.LLSDACSB6943:
	.uleb128 .LEHB10-.LFB6943
	.uleb128 .LEHE10-.LEHB10
	.uleb128 .L124-.LFB6943
	.uleb128 0
	.uleb128 .LEHB11-.LFB6943
	.uleb128 .LEHE11-.LEHB11
	.uleb128 .L125-.LFB6943
	.uleb128 0
	.uleb128 .LEHB12-.LFB6943
	.uleb128 .LEHE12-.LEHB12
	.uleb128 .L124-.LFB6943
	.uleb128 0
.LLSDACSE6943:
	.section	.text._ZN2v88internal6torque12CSAGenerator31PreCallableExceptionPreparationB5cxx11ENS_4base8OptionalIPNS1_5BlockEEE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator31PreCallableExceptionPreparationB5cxx11ENS_4base8OptionalIPNS1_5BlockEEE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6943
	.type	_ZN2v88internal6torque12CSAGenerator31PreCallableExceptionPreparationB5cxx11ENS_4base8OptionalIPNS1_5BlockEEE.cold, @function
_ZN2v88internal6torque12CSAGenerator31PreCallableExceptionPreparationB5cxx11ENS_4base8OptionalIPNS1_5BlockEEE.cold:
.LFSB6943:
.L113:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	movq	-80(%rbp), %rdi
	leaq	-64(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L115
	call	_ZdlPv@PLT
.L115:
	movq	(%r12), %rdi
	cmpq	%rdi, %r13
	je	.L122
	call	_ZdlPv@PLT
.L122:
	movq	%r14, %rdi
.LEHB13:
	call	_Unwind_Resume@PLT
.LEHE13:
	.cfi_endproc
.LFE6943:
	.section	.gcc_except_table._ZN2v88internal6torque12CSAGenerator31PreCallableExceptionPreparationB5cxx11ENS_4base8OptionalIPNS1_5BlockEEE
.LLSDAC6943:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6943-.LLSDACSBC6943
.LLSDACSBC6943:
	.uleb128 .LEHB13-.LCOLDB13
	.uleb128 .LEHE13-.LEHB13
	.uleb128 0
	.uleb128 0
.LLSDACSEC6943:
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator31PreCallableExceptionPreparationB5cxx11ENS_4base8OptionalIPNS1_5BlockEEE
	.section	.text._ZN2v88internal6torque12CSAGenerator31PreCallableExceptionPreparationB5cxx11ENS_4base8OptionalIPNS1_5BlockEEE
	.size	_ZN2v88internal6torque12CSAGenerator31PreCallableExceptionPreparationB5cxx11ENS_4base8OptionalIPNS1_5BlockEEE, .-_ZN2v88internal6torque12CSAGenerator31PreCallableExceptionPreparationB5cxx11ENS_4base8OptionalIPNS1_5BlockEEE
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator31PreCallableExceptionPreparationB5cxx11ENS_4base8OptionalIPNS1_5BlockEEE
	.size	_ZN2v88internal6torque12CSAGenerator31PreCallableExceptionPreparationB5cxx11ENS_4base8OptionalIPNS1_5BlockEEE.cold, .-_ZN2v88internal6torque12CSAGenerator31PreCallableExceptionPreparationB5cxx11ENS_4base8OptionalIPNS1_5BlockEEE.cold
.LCOLDE13:
	.section	.text._ZN2v88internal6torque12CSAGenerator31PreCallableExceptionPreparationB5cxx11ENS_4base8OptionalIPNS1_5BlockEEE
.LHOTE13:
	.section	.rodata._ZN2v88internal6torque12CSAGenerator32PostCallableExceptionPreparationERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKNS1_4TypeENS_4base8OptionalIPNS1_5BlockEEEPNS1_5StackIS8_EE.str1.1,"aMS",@progbits,1
.LC14:
	.string	"block"
.LC15:
	.string	"    }\n"
.LC16:
	.string	"    if ("
.LC17:
	.string	"__label.is_used()) {\n"
	.section	.rodata._ZN2v88internal6torque12CSAGenerator32PostCallableExceptionPreparationERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKNS1_4TypeENS_4base8OptionalIPNS1_5BlockEEEPNS1_5StackIS8_EE.str1.8,"aMS",@progbits,1
	.align 8
.LC18:
	.string	"      compiler::CodeAssemblerLabel "
	.section	.rodata._ZN2v88internal6torque12CSAGenerator32PostCallableExceptionPreparationERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKNS1_4TypeENS_4base8OptionalIPNS1_5BlockEEEPNS1_5StackIS8_EE.str1.1
.LC19:
	.string	"_skip(&ca_);\n"
.LC20:
	.string	"      ca_.Goto(&"
.LC21:
	.string	"_skip);\n"
	.section	.rodata._ZN2v88internal6torque12CSAGenerator32PostCallableExceptionPreparationERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKNS1_4TypeENS_4base8OptionalIPNS1_5BlockEEEPNS1_5StackIS8_EE.str1.8
	.align 8
.LC22:
	.string	"      compiler::TNode<Object> "
	.section	.rodata._ZN2v88internal6torque12CSAGenerator32PostCallableExceptionPreparationERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKNS1_4TypeENS_4base8OptionalIPNS1_5BlockEEEPNS1_5StackIS8_EE.str1.1
.LC23:
	.string	"_exception_object;\n"
.LC24:
	.string	"      ca_.Bind(&"
.LC25:
	.string	"__label, &"
.LC26:
	.string	"_exception_object);\n"
.LC27:
	.string	", "
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator32PostCallableExceptionPreparationERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKNS1_4TypeENS_4base8OptionalIPNS1_5BlockEEEPNS1_5StackIS8_EE,"ax",@progbits
	.align 2
.LCOLDB28:
	.section	.text._ZN2v88internal6torque12CSAGenerator32PostCallableExceptionPreparationERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKNS1_4TypeENS_4base8OptionalIPNS1_5BlockEEEPNS1_5StackIS8_EE,"ax",@progbits
.LHOTB28:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque12CSAGenerator32PostCallableExceptionPreparationERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKNS1_4TypeENS_4base8OptionalIPNS1_5BlockEEEPNS1_5StackIS8_EE
	.type	_ZN2v88internal6torque12CSAGenerator32PostCallableExceptionPreparationERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKNS1_4TypeENS_4base8OptionalIPNS1_5BlockEEEPNS1_5StackIS8_EE, @function
_ZN2v88internal6torque12CSAGenerator32PostCallableExceptionPreparationERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKNS1_4TypeENS_4base8OptionalIPNS1_5BlockEEEPNS1_5StackIS8_EE:
.LFB6944:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6944
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -136(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testb	%cl, %cl
	jne	.L177
.L140:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L178
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L177:
	.cfi_restore_state
	leaq	-96(%rbp), %rax
	movq	64(%r8), %r8
	movq	%rsi, %r15
	movl	$32, %edx
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	movq	%rax, %r14
	movq	%rdi, %r13
	movq	%rax, %rdi
	movq	%rax, -152(%rbp)
	leaq	.LC0(%rip), %rcx
	xorl	%eax, %eax
	movq	%r9, %rbx
.LEHB14:
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0
.LEHE14:
	movl	$5, %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
	leaq	.LC14(%rip), %rcx
	movq	%r14, %rdi
.LEHB15:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.LEHE15:
	leaq	-112(%rbp), %rcx
	leaq	16(%rax), %rdx
	movq	%rcx, -160(%rbp)
	movq	%rcx, -128(%rbp)
	movq	(%rax), %rcx
	cmpq	%rdx, %rcx
	je	.L179
	movq	%rcx, -128(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -112(%rbp)
.L143:
	movq	8(%rax), %rcx
	movb	$0, 16(%rax)
	movq	%rcx, -120(%rbp)
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	movq	%rax, -144(%rbp)
	cmpq	%rax, %rdi
	je	.L144
	call	_ZdlPv@PLT
.L144:
	movq	8(%r13), %rdi
	movl	$6, %edx
	leaq	.LC15(%rip), %rsi
.LEHB16:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%r13), %r12
	movl	$8, %edx
	leaq	.LC16(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%r15), %rdx
	movq	(%r15), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rax, %rdi
	movl	$21, %edx
	leaq	.LC17(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%r13), %r12
	movl	$35, %edx
	leaq	.LC18(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%r15), %rdx
	movq	(%r15), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rax, %rdi
	movl	$13, %edx
	leaq	.LC19(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE16:
	movq	-144(%rbp), %r12
	movq	-152(%rbp), %rsi
	movb	$114, -76(%rbp)
	movq	-136(%rbp), %rdi
	movl	$1702258030, -80(%rbp)
	movq	%r12, -96(%rbp)
	movq	$5, -88(%rbp)
	movb	$0, -75(%rbp)
.LEHB17:
	call	_ZNK2v88internal6torque4Type14IsAbstractNameERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
.LEHE17:
	movq	-96(%rbp), %rdi
	movl	%eax, %r14d
	cmpq	%r12, %rdi
	je	.L148
	call	_ZdlPv@PLT
.L148:
	movq	8(%r13), %r12
	testb	%r14b, %r14b
	je	.L180
.L149:
	movl	$30, %edx
	leaq	.LC22(%rip), %rsi
	movq	%r12, %rdi
.LEHB18:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%r15), %rdx
	movq	(%r15), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rax, %rdi
	movl	$19, %edx
	leaq	.LC23(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%r13), %r12
	movl	$16, %edx
	leaq	.LC24(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%r15), %rdx
	movq	(%r15), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$10, %edx
	leaq	.LC25(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%r15), %rdx
	movq	(%r15), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rax, %rdi
	movl	$20, %edx
	leaq	.LC26(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%r13), %r12
	movl	$16, %edx
	leaq	.LC20(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-120(%rbp), %rdx
	movq	-128(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%rbx), %rax
	xorl	%r12d, %r12d
	cmpq	%rax, (%rbx)
	je	.L156
	.p2align 4,,10
	.p2align 3
.L154:
	movq	8(%r13), %r14
	movl	$2, %edx
	leaq	.LC27(%rip), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r12, %rax
	movq	%r14, %rdi
	salq	$5, %rax
	addq	(%rbx), %rax
	movq	8(%rax), %rdx
	movq	(%rax), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%rbx), %rax
	subq	(%rbx), %rax
	addq	$1, %r12
	sarq	$5, %rax
	cmpq	%rax, %r12
	jb	.L154
.L156:
	movq	8(%r13), %r12
	movl	$2, %edx
	leaq	.LC27(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%r15), %rdx
	movq	(%r15), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rax, %rdi
	movl	$20, %edx
	leaq	.LC26(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE18:
	movq	-144(%rbp), %r14
	movq	-152(%rbp), %rsi
	movb	$114, -76(%rbp)
	movq	-136(%rbp), %rdi
	movl	$1702258030, -80(%rbp)
	movq	%r14, -96(%rbp)
	movq	$5, -88(%rbp)
	movb	$0, -75(%rbp)
.LEHB19:
	call	_ZNK2v88internal6torque4Type14IsAbstractNameERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
.LEHE19:
	movq	-96(%rbp), %rdi
	movl	%eax, %ebx
	cmpq	%r14, %rdi
	je	.L157
	call	_ZdlPv@PLT
.L157:
	movq	8(%r13), %r12
	testb	%bl, %bl
	jne	.L158
	movl	$16, %edx
	leaq	.LC24(%rip), %rsi
	movq	%r12, %rdi
.LEHB20:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%r15), %rdx
	movq	(%r15), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rax, %rdi
	movl	$8, %edx
	leaq	.LC21(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%r13), %r12
.L158:
	movl	$6, %edx
	leaq	.LC15(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-128(%rbp), %rdi
	cmpq	-160(%rbp), %rdi
	je	.L140
	call	_ZdlPv@PLT
	jmp	.L140
	.p2align 4,,10
	.p2align 3
.L180:
	movl	$16, %edx
	leaq	.LC20(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%r15), %rdx
	movq	(%r15), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rax, %rdi
	movl	$8, %edx
	leaq	.LC21(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE20:
	movq	8(%r13), %r12
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L179:
	movdqu	16(%rax), %xmm0
	movaps	%xmm0, -112(%rbp)
	jmp	.L143
.L178:
	call	__stack_chk_fail@PLT
.L166:
	endbr64
	movq	%rax, %r12
	jmp	.L153
.L168:
	endbr64
	movq	%rax, %r12
	jmp	.L151
.L167:
	endbr64
	movq	%rax, %r12
	jmp	.L146
.L169:
	endbr64
	movq	%rax, %r12
	jmp	.L160
	.section	.gcc_except_table._ZN2v88internal6torque12CSAGenerator32PostCallableExceptionPreparationERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKNS1_4TypeENS_4base8OptionalIPNS1_5BlockEEEPNS1_5StackIS8_EE,"a",@progbits
.LLSDA6944:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6944-.LLSDACSB6944
.LLSDACSB6944:
	.uleb128 .LEHB14-.LFB6944
	.uleb128 .LEHE14-.LEHB14
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB15-.LFB6944
	.uleb128 .LEHE15-.LEHB15
	.uleb128 .L167-.LFB6944
	.uleb128 0
	.uleb128 .LEHB16-.LFB6944
	.uleb128 .LEHE16-.LEHB16
	.uleb128 .L166-.LFB6944
	.uleb128 0
	.uleb128 .LEHB17-.LFB6944
	.uleb128 .LEHE17-.LEHB17
	.uleb128 .L168-.LFB6944
	.uleb128 0
	.uleb128 .LEHB18-.LFB6944
	.uleb128 .LEHE18-.LEHB18
	.uleb128 .L166-.LFB6944
	.uleb128 0
	.uleb128 .LEHB19-.LFB6944
	.uleb128 .LEHE19-.LEHB19
	.uleb128 .L169-.LFB6944
	.uleb128 0
	.uleb128 .LEHB20-.LFB6944
	.uleb128 .LEHE20-.LEHB20
	.uleb128 .L166-.LFB6944
	.uleb128 0
.LLSDACSE6944:
	.section	.text._ZN2v88internal6torque12CSAGenerator32PostCallableExceptionPreparationERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKNS1_4TypeENS_4base8OptionalIPNS1_5BlockEEEPNS1_5StackIS8_EE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator32PostCallableExceptionPreparationERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKNS1_4TypeENS_4base8OptionalIPNS1_5BlockEEEPNS1_5StackIS8_EE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6944
	.type	_ZN2v88internal6torque12CSAGenerator32PostCallableExceptionPreparationERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKNS1_4TypeENS_4base8OptionalIPNS1_5BlockEEEPNS1_5StackIS8_EE.cold, @function
_ZN2v88internal6torque12CSAGenerator32PostCallableExceptionPreparationERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKNS1_4TypeENS_4base8OptionalIPNS1_5BlockEEEPNS1_5StackIS8_EE.cold:
.LFSB6944:
.L151:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-96(%rbp), %rdi
	cmpq	-144(%rbp), %rdi
	je	.L153
	call	_ZdlPv@PLT
.L153:
	movq	-128(%rbp), %rdi
	cmpq	-160(%rbp), %rdi
	je	.L163
	call	_ZdlPv@PLT
.L163:
	movq	%r12, %rdi
.LEHB21:
	call	_Unwind_Resume@PLT
.L146:
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L147
	call	_ZdlPv@PLT
.L147:
	movq	%r12, %rdi
	call	_Unwind_Resume@PLT
.LEHE21:
.L160:
	movq	-96(%rbp), %rdi
	cmpq	-144(%rbp), %rdi
	je	.L153
	call	_ZdlPv@PLT
	jmp	.L153
	.cfi_endproc
.LFE6944:
	.section	.gcc_except_table._ZN2v88internal6torque12CSAGenerator32PostCallableExceptionPreparationERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKNS1_4TypeENS_4base8OptionalIPNS1_5BlockEEEPNS1_5StackIS8_EE
.LLSDAC6944:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6944-.LLSDACSBC6944
.LLSDACSBC6944:
	.uleb128 .LEHB21-.LCOLDB28
	.uleb128 .LEHE21-.LEHB21
	.uleb128 0
	.uleb128 0
.LLSDACSEC6944:
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator32PostCallableExceptionPreparationERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKNS1_4TypeENS_4base8OptionalIPNS1_5BlockEEEPNS1_5StackIS8_EE
	.section	.text._ZN2v88internal6torque12CSAGenerator32PostCallableExceptionPreparationERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKNS1_4TypeENS_4base8OptionalIPNS1_5BlockEEEPNS1_5StackIS8_EE
	.size	_ZN2v88internal6torque12CSAGenerator32PostCallableExceptionPreparationERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKNS1_4TypeENS_4base8OptionalIPNS1_5BlockEEEPNS1_5StackIS8_EE, .-_ZN2v88internal6torque12CSAGenerator32PostCallableExceptionPreparationERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKNS1_4TypeENS_4base8OptionalIPNS1_5BlockEEEPNS1_5StackIS8_EE
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator32PostCallableExceptionPreparationERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKNS1_4TypeENS_4base8OptionalIPNS1_5BlockEEEPNS1_5StackIS8_EE
	.size	_ZN2v88internal6torque12CSAGenerator32PostCallableExceptionPreparationERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKNS1_4TypeENS_4base8OptionalIPNS1_5BlockEEEPNS1_5StackIS8_EE.cold, .-_ZN2v88internal6torque12CSAGenerator32PostCallableExceptionPreparationERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKNS1_4TypeENS_4base8OptionalIPNS1_5BlockEEEPNS1_5StackIS8_EE.cold
.LCOLDE28:
	.section	.text._ZN2v88internal6torque12CSAGenerator32PostCallableExceptionPreparationERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKNS1_4TypeENS_4base8OptionalIPNS1_5BlockEEEPNS1_5StackIS8_EE
.LHOTE28:
	.section	.rodata._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_17BranchInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.str1.1,"aMS",@progbits,1
.LC29:
	.string	"    ca_.Branch("
.LC30:
	.string	", &"
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_17BranchInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE,"ax",@progbits
	.align 2
.LCOLDB31:
	.section	.text._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_17BranchInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE,"ax",@progbits
.LHOTB31:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_17BranchInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.type	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_17BranchInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE, @function
_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_17BranchInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE:
.LFB6946:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6946
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	leaq	.LC29(%rip), %rsi
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	movl	$15, %edx
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$184, %rsp
	movq	8(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r13, %rdi
.LEHB22:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE22:
	movq	8(%r12), %rax
	leaq	-176(%rbp), %rcx
	movq	%rcx, -200(%rbp)
	movq	%rcx, -192(%rbp)
	movq	-32(%rax), %rcx
	leaq	-16(%rax), %rdx
	cmpq	%rdx, %rcx
	je	.L219
	movq	%rcx, -192(%rbp)
	movq	-16(%rax), %rcx
	movq	%rcx, -176(%rbp)
.L183:
	movq	-24(%rax), %rcx
	movq	%rcx, -184(%rbp)
	movq	%rdx, -32(%rax)
	movq	$0, -24(%rax)
	movb	$0, -16(%rax)
	movq	8(%r12), %rax
	leaq	-32(%rax), %rdx
	subq	$16, %rax
	movq	%rdx, 8(%r12)
	movq	-16(%rax), %rdi
	cmpq	%rax, %rdi
	je	.L184
	call	_ZdlPv@PLT
.L184:
	movq	-184(%rbp), %rdx
	movq	-192(%rbp), %rsi
	movq	%r13, %rdi
.LEHB23:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$3, %edx
	leaq	.LC30(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	32(%r15), %rax
	leaq	-96(%rbp), %r14
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	leaq	.LC0(%rip), %rcx
	movl	$32, %edx
	movq	%r14, %rdi
	movq	64(%rax), %r8
	xorl	%eax, %eax
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0
.LEHE23:
	movl	$5, %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r14, %rdi
	leaq	.LC14(%rip), %rcx
.LEHB24:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.LEHE24:
	leaq	-144(%rbp), %rcx
	leaq	16(%rax), %rdx
	movq	%rcx, -216(%rbp)
	movq	%rcx, -160(%rbp)
	movq	(%rax), %rcx
	cmpq	%rdx, %rcx
	je	.L220
	movq	%rcx, -160(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -144(%rbp)
.L186:
	movq	8(%rax), %rcx
	movb	$0, 16(%rax)
	movq	%rcx, -152(%rbp)
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	movq	%rax, -208(%rbp)
	cmpq	%rax, %rdi
	je	.L187
	call	_ZdlPv@PLT
.L187:
	movq	-152(%rbp), %rdx
	movq	-160(%rbp), %rsi
	movq	%r13, %rdi
.LEHB25:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$3, %edx
	leaq	.LC30(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	40(%r15), %rax
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	movl	$32, %edx
	movq	%r14, %rdi
	leaq	.LC0(%rip), %rcx
	movq	64(%rax), %r8
	xorl	%eax, %eax
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0
.LEHE25:
	movl	$5, %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r14, %rdi
	leaq	.LC14(%rip), %rcx
.LEHB26:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.LEHE26:
	leaq	-112(%rbp), %r14
	leaq	16(%rax), %rdx
	movq	%r14, -128(%rbp)
	movq	(%rax), %rcx
	cmpq	%rdx, %rcx
	je	.L221
	movq	%rcx, -128(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -112(%rbp)
.L193:
	movq	8(%rax), %rcx
	movb	$0, 16(%rax)
	movq	%rcx, -120(%rbp)
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movq	-96(%rbp), %rdi
	cmpq	-208(%rbp), %rdi
	je	.L194
	call	_ZdlPv@PLT
.L194:
	movq	-120(%rbp), %rdx
	movq	-128(%rbp), %rsi
	movq	%r13, %rdi
.LEHB27:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE27:
	movq	-128(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L199
	call	_ZdlPv@PLT
.L199:
	movq	-160(%rbp), %rdi
	cmpq	-216(%rbp), %rdi
	je	.L200
	call	_ZdlPv@PLT
.L200:
	movq	-192(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	je	.L201
	call	_ZdlPv@PLT
.L201:
	movq	(%r12), %rax
	movq	8(%r12), %r14
	cmpq	%rax, %r14
	je	.L202
	movq	%rax, %r12
	leaq	.LC27(%rip), %r15
	.p2align 4,,10
	.p2align 3
.L203:
	movq	8(%rbx), %r13
	movl	$2, %edx
	movq	%r15, %rsi
	addq	$32, %r12
	movq	%r13, %rdi
.LEHB28:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-24(%r12), %rdx
	movq	-32(%r12), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpq	%r12, %r14
	jne	.L203
.L202:
	movq	8(%rbx), %rdi
	movl	$3, %edx
	leaq	.LC3(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE28:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L222
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L219:
	.cfi_restore_state
	movdqu	-16(%rax), %xmm0
	movaps	%xmm0, -176(%rbp)
	jmp	.L183
	.p2align 4,,10
	.p2align 3
.L221:
	movdqu	16(%rax), %xmm2
	movaps	%xmm2, -112(%rbp)
	jmp	.L193
	.p2align 4,,10
	.p2align 3
.L220:
	movdqu	16(%rax), %xmm1
	movaps	%xmm1, -144(%rbp)
	jmp	.L186
.L222:
	call	__stack_chk_fail@PLT
.L211:
	endbr64
	movq	%rax, %r12
	jmp	.L204
.L213:
	endbr64
	movq	%rax, %r12
	jmp	.L196
.L212:
	endbr64
	movq	%rax, %r12
	jmp	.L189
.L210:
	endbr64
	movq	%rax, %r12
	jmp	.L198
.L209:
	endbr64
	movq	%rax, %r12
	jmp	.L191
	.section	.gcc_except_table._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_17BranchInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE,"a",@progbits
.LLSDA6946:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6946-.LLSDACSB6946
.LLSDACSB6946:
	.uleb128 .LEHB22-.LFB6946
	.uleb128 .LEHE22-.LEHB22
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB23-.LFB6946
	.uleb128 .LEHE23-.LEHB23
	.uleb128 .L209-.LFB6946
	.uleb128 0
	.uleb128 .LEHB24-.LFB6946
	.uleb128 .LEHE24-.LEHB24
	.uleb128 .L212-.LFB6946
	.uleb128 0
	.uleb128 .LEHB25-.LFB6946
	.uleb128 .LEHE25-.LEHB25
	.uleb128 .L210-.LFB6946
	.uleb128 0
	.uleb128 .LEHB26-.LFB6946
	.uleb128 .LEHE26-.LEHB26
	.uleb128 .L213-.LFB6946
	.uleb128 0
	.uleb128 .LEHB27-.LFB6946
	.uleb128 .LEHE27-.LEHB27
	.uleb128 .L211-.LFB6946
	.uleb128 0
	.uleb128 .LEHB28-.LFB6946
	.uleb128 .LEHE28-.LEHB28
	.uleb128 0
	.uleb128 0
.LLSDACSE6946:
	.section	.text._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_17BranchInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_17BranchInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6946
	.type	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_17BranchInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.cold, @function
_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_17BranchInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.cold:
.LFSB6946:
.L204:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-128(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L198
	call	_ZdlPv@PLT
.L198:
	movq	-160(%rbp), %rdi
	cmpq	-216(%rbp), %rdi
	je	.L191
	call	_ZdlPv@PLT
.L191:
	movq	-192(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	je	.L207
	call	_ZdlPv@PLT
.L207:
	movq	%r12, %rdi
.LEHB29:
	call	_Unwind_Resume@PLT
.LEHE29:
.L196:
	movq	-96(%rbp), %rdi
	cmpq	-208(%rbp), %rdi
	je	.L198
	call	_ZdlPv@PLT
	jmp	.L198
.L189:
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L191
	call	_ZdlPv@PLT
	jmp	.L191
	.cfi_endproc
.LFE6946:
	.section	.gcc_except_table._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_17BranchInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
.LLSDAC6946:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6946-.LLSDACSBC6946
.LLSDACSBC6946:
	.uleb128 .LEHB29-.LCOLDB31
	.uleb128 .LEHE29-.LEHB29
	.uleb128 0
	.uleb128 0
.LLSDACSEC6946:
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_17BranchInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.section	.text._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_17BranchInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.size	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_17BranchInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE, .-_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_17BranchInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_17BranchInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.size	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_17BranchInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.cold, .-_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_17BranchInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.cold
.LCOLDE31:
	.section	.text._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_17BranchInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
.LHOTE31:
	.section	.rodata._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_26ConstexprBranchInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.str1.1,"aMS",@progbits,1
.LC32:
	.string	"    if (("
.LC33:
	.string	")) {\n"
.LC34:
	.string	"    } else {\n"
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_26ConstexprBranchInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE,"ax",@progbits
	.align 2
.LCOLDB35:
	.section	.text._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_26ConstexprBranchInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE,"ax",@progbits
.LHOTB35:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_26ConstexprBranchInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.type	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_26ConstexprBranchInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE, @function
_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_26ConstexprBranchInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE:
.LFB6947:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6947
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$136, %rsp
	movq	%rsi, -160(%rbp)
	movq	8(%rdi), %r12
	leaq	.LC32(%rip), %rsi
	movq	%rdx, -144(%rbp)
	movl	$9, %edx
	movq	%r12, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
.LEHB30:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	40(%r14), %rdx
	movq	32(%r14), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$5, %edx
	leaq	.LC33(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%rbx), %r12
	movl	$16, %edx
	leaq	.LC20(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	64(%r14), %rax
	leaq	-96(%rbp), %rdi
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	movq	%rdi, %r15
	leaq	.LC0(%rip), %rcx
	movl	$32, %edx
	movq	%rdi, -168(%rbp)
	movq	64(%rax), %r8
	xorl	%eax, %eax
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0
.LEHE30:
	movl	$5, %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
	leaq	.LC14(%rip), %rcx
	movq	%r15, %rdi
.LEHB31:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.LEHE31:
	leaq	-112(%rbp), %rcx
	leaq	16(%rax), %rdx
	movq	%rcx, -136(%rbp)
	movq	%rcx, -128(%rbp)
	movq	(%rax), %rcx
	cmpq	%rdx, %rcx
	je	.L257
	movq	%rcx, -128(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -112(%rbp)
.L225:
	movq	8(%rax), %rcx
	movq	%rcx, -120(%rbp)
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movb	$0, 16(%rax)
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	movq	%rax, -152(%rbp)
	cmpq	%rax, %rdi
	je	.L226
	call	_ZdlPv@PLT
.L226:
	movq	-120(%rbp), %rdx
	movq	-128(%rbp), %rsi
	movq	%r12, %rdi
.LEHB32:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE32:
	movq	-128(%rbp), %rdi
	cmpq	-136(%rbp), %rdi
	je	.L230
	call	_ZdlPv@PLT
.L230:
	movq	-144(%rbp), %rax
	movq	(%rax), %rcx
	movq	8(%rax), %r12
	cmpq	%rcx, %r12
	je	.L231
	movq	%rcx, %r15
	leaq	.LC27(%rip), %r13
	.p2align 4,,10
	.p2align 3
.L232:
	movq	8(%rbx), %r14
	movl	$2, %edx
	movq	%r13, %rsi
	addq	$32, %r15
	movq	%r14, %rdi
.LEHB33:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-24(%r15), %rdx
	movq	-32(%r15), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpq	%r15, %r12
	jne	.L232
.L231:
	movq	8(%rbx), %rdi
	movl	$3, %edx
	leaq	.LC3(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%rbx), %rdi
	movl	$13, %edx
	leaq	.LC34(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%rbx), %r12
	movl	$16, %edx
	leaq	.LC20(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-160(%rbp), %rax
	movq	-168(%rbp), %r15
	leaq	.LC0(%rip), %rcx
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	movl	$32, %edx
	movq	72(%rax), %rax
	movq	%r15, %rdi
	movq	64(%rax), %r8
	xorl	%eax, %eax
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0
.LEHE33:
	movl	$5, %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
	leaq	.LC14(%rip), %rcx
	movq	%r15, %rdi
.LEHB34:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.LEHE34:
	movq	-136(%rbp), %rcx
	leaq	16(%rax), %rdx
	movq	%rcx, -128(%rbp)
	movq	(%rax), %rcx
	cmpq	%rdx, %rcx
	je	.L258
	movq	%rcx, -128(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -112(%rbp)
.L234:
	movq	8(%rax), %rcx
	movq	%rcx, -120(%rbp)
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movb	$0, 16(%rax)
	movq	-96(%rbp), %rdi
	cmpq	-152(%rbp), %rdi
	je	.L235
	call	_ZdlPv@PLT
.L235:
	movq	-120(%rbp), %rdx
	movq	-128(%rbp), %rsi
	movq	%r12, %rdi
.LEHB35:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE35:
	movq	-128(%rbp), %rdi
	cmpq	-136(%rbp), %rdi
	je	.L239
	call	_ZdlPv@PLT
.L239:
	movq	-144(%rbp), %rax
	movq	(%rax), %r12
	movq	8(%rax), %r14
	cmpq	%r12, %r14
	je	.L240
	leaq	.LC27(%rip), %r15
	.p2align 4,,10
	.p2align 3
.L241:
	movq	8(%rbx), %r13
	movl	$2, %edx
	movq	%r15, %rsi
	addq	$32, %r12
	movq	%r13, %rdi
.LEHB36:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-24(%r12), %rdx
	movq	-32(%r12), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpq	%r12, %r14
	jne	.L241
.L240:
	movq	8(%rbx), %rdi
	movl	$3, %edx
	leaq	.LC3(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%rbx), %rdi
	movl	$6, %edx
	leaq	.LC15(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE36:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L259
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L257:
	.cfi_restore_state
	movdqu	16(%rax), %xmm0
	movaps	%xmm0, -112(%rbp)
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L258:
	movdqu	16(%rax), %xmm1
	movaps	%xmm1, -112(%rbp)
	jmp	.L234
.L259:
	call	__stack_chk_fail@PLT
.L248:
	endbr64
	movq	%rax, %r12
	jmp	.L244
.L249:
	endbr64
	movq	%rax, %r12
	jmp	.L228
.L250:
	endbr64
	movq	%rax, %r12
	jmp	.L237
.L247:
	endbr64
	movq	%rax, %r12
	jmp	.L242
	.section	.gcc_except_table._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_26ConstexprBranchInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE,"a",@progbits
.LLSDA6947:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6947-.LLSDACSB6947
.LLSDACSB6947:
	.uleb128 .LEHB30-.LFB6947
	.uleb128 .LEHE30-.LEHB30
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB31-.LFB6947
	.uleb128 .LEHE31-.LEHB31
	.uleb128 .L249-.LFB6947
	.uleb128 0
	.uleb128 .LEHB32-.LFB6947
	.uleb128 .LEHE32-.LEHB32
	.uleb128 .L247-.LFB6947
	.uleb128 0
	.uleb128 .LEHB33-.LFB6947
	.uleb128 .LEHE33-.LEHB33
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB34-.LFB6947
	.uleb128 .LEHE34-.LEHB34
	.uleb128 .L250-.LFB6947
	.uleb128 0
	.uleb128 .LEHB35-.LFB6947
	.uleb128 .LEHE35-.LEHB35
	.uleb128 .L248-.LFB6947
	.uleb128 0
	.uleb128 .LEHB36-.LFB6947
	.uleb128 .LEHE36-.LEHB36
	.uleb128 0
	.uleb128 0
.LLSDACSE6947:
	.section	.text._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_26ConstexprBranchInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_26ConstexprBranchInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6947
	.type	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_26ConstexprBranchInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.cold, @function
_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_26ConstexprBranchInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.cold:
.LFSB6947:
.L244:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-128(%rbp), %rdi
	cmpq	-136(%rbp), %rdi
	je	.L245
	call	_ZdlPv@PLT
.L245:
	movq	%r12, %rdi
.LEHB37:
	call	_Unwind_Resume@PLT
.L228:
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L229
	call	_ZdlPv@PLT
.L229:
	movq	%r12, %rdi
	call	_Unwind_Resume@PLT
.L237:
	movq	-96(%rbp), %rdi
	cmpq	-152(%rbp), %rdi
	je	.L238
	call	_ZdlPv@PLT
.L238:
	movq	%r12, %rdi
	call	_Unwind_Resume@PLT
.L242:
	movq	-128(%rbp), %rdi
	cmpq	-136(%rbp), %rdi
	je	.L243
	call	_ZdlPv@PLT
.L243:
	movq	%r12, %rdi
	call	_Unwind_Resume@PLT
.LEHE37:
	.cfi_endproc
.LFE6947:
	.section	.gcc_except_table._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_26ConstexprBranchInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
.LLSDAC6947:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6947-.LLSDACSBC6947
.LLSDACSBC6947:
	.uleb128 .LEHB37-.LCOLDB35
	.uleb128 .LEHE37-.LEHB37
	.uleb128 0
	.uleb128 0
.LLSDACSEC6947:
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_26ConstexprBranchInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.section	.text._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_26ConstexprBranchInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.size	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_26ConstexprBranchInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE, .-_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_26ConstexprBranchInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_26ConstexprBranchInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.size	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_26ConstexprBranchInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.cold, .-_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_26ConstexprBranchInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.cold
.LCOLDE35:
	.section	.text._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_26ConstexprBranchInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
.LHOTE35:
	.section	.rodata._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_15GotoInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.str1.1,"aMS",@progbits,1
.LC36:
	.string	"    ca_.Goto(&"
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_15GotoInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE,"ax",@progbits
	.align 2
.LCOLDB37:
	.section	.text._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_15GotoInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE,"ax",@progbits
.LHOTB37:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_15GotoInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.type	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_15GotoInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE, @function
_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_15GotoInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE:
.LFB6948:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6948
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-96(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	movl	$14, %edx
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	leaq	.LC36(%rip), %rsi
	subq	$88, %rsp
	movq	8(%rdi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r14, %rdi
.LEHB38:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	32(%rbx), %rax
	movl	$32, %edx
	movq	%r15, %rdi
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	leaq	.LC0(%rip), %rcx
	movq	64(%rax), %r8
	xorl	%eax, %eax
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0
.LEHE38:
	movl	$5, %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
	leaq	.LC14(%rip), %rcx
	movq	%r15, %rdi
.LEHB39:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.LEHE39:
	leaq	-112(%rbp), %rbx
	leaq	16(%rax), %rdx
	movq	%rbx, -128(%rbp)
	movq	(%rax), %rcx
	cmpq	%rdx, %rcx
	je	.L279
	movq	%rcx, -128(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -112(%rbp)
.L262:
	movq	8(%rax), %rcx
	movb	$0, 16(%rax)
	movq	%rcx, -120(%rbp)
	movq	%rdx, (%rax)
	movq	-96(%rbp), %rdi
	movq	$0, 8(%rax)
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L263
	call	_ZdlPv@PLT
.L263:
	movq	-120(%rbp), %rdx
	movq	-128(%rbp), %rsi
	movq	%r14, %rdi
.LEHB40:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE40:
	movq	-128(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L267
	call	_ZdlPv@PLT
.L267:
	movq	(%r12), %rbx
	movq	8(%r12), %r14
	cmpq	%rbx, %r14
	je	.L268
	leaq	.LC27(%rip), %r15
	.p2align 4,,10
	.p2align 3
.L269:
	movq	8(%r13), %r12
	movl	$2, %edx
	movq	%r15, %rsi
	addq	$32, %rbx
	movq	%r12, %rdi
.LEHB41:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-24(%rbx), %rdx
	movq	-32(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpq	%rbx, %r14
	jne	.L269
.L268:
	movq	8(%r13), %rdi
	movl	$3, %edx
	leaq	.LC3(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE41:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L280
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L279:
	.cfi_restore_state
	movdqu	16(%rax), %xmm0
	movaps	%xmm0, -112(%rbp)
	jmp	.L262
.L280:
	call	__stack_chk_fail@PLT
.L274:
	endbr64
	movq	%rax, %r12
	jmp	.L265
.L273:
	endbr64
	movq	%rax, %r12
	jmp	.L270
	.section	.gcc_except_table._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_15GotoInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE,"a",@progbits
.LLSDA6948:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6948-.LLSDACSB6948
.LLSDACSB6948:
	.uleb128 .LEHB38-.LFB6948
	.uleb128 .LEHE38-.LEHB38
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB39-.LFB6948
	.uleb128 .LEHE39-.LEHB39
	.uleb128 .L274-.LFB6948
	.uleb128 0
	.uleb128 .LEHB40-.LFB6948
	.uleb128 .LEHE40-.LEHB40
	.uleb128 .L273-.LFB6948
	.uleb128 0
	.uleb128 .LEHB41-.LFB6948
	.uleb128 .LEHE41-.LEHB41
	.uleb128 0
	.uleb128 0
.LLSDACSE6948:
	.section	.text._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_15GotoInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_15GotoInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6948
	.type	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_15GotoInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.cold, @function
_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_15GotoInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.cold:
.LFSB6948:
.L265:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L266
	call	_ZdlPv@PLT
.L266:
	movq	%r12, %rdi
.LEHB42:
	call	_Unwind_Resume@PLT
.L270:
	movq	-128(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L271
	call	_ZdlPv@PLT
.L271:
	movq	%r12, %rdi
	call	_Unwind_Resume@PLT
.LEHE42:
	.cfi_endproc
.LFE6948:
	.section	.gcc_except_table._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_15GotoInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
.LLSDAC6948:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6948-.LLSDACSBC6948
.LLSDACSBC6948:
	.uleb128 .LEHB42-.LCOLDB37
	.uleb128 .LEHE42-.LEHB42
	.uleb128 0
	.uleb128 0
.LLSDACSEC6948:
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_15GotoInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.section	.text._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_15GotoInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.size	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_15GotoInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE, .-_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_15GotoInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_15GotoInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.size	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_15GotoInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.cold, .-_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_15GotoInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.cold
.LCOLDE37:
	.section	.text._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_15GotoInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
.LHOTE37:
	.section	.rodata._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_23GotoExternalInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.str1.1,"aMS",@progbits,1
.LC38:
	.string	"    *"
.LC39:
	.string	" = "
.LC40:
	.string	";\n"
.LC41:
	.string	"    ca_.Goto("
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_23GotoExternalInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE,"ax",@progbits
	.align 2
.LCOLDB42:
	.section	.text._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_23GotoExternalInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE,"ax",@progbits
.LHOTB42:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_23GotoExternalInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.type	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_23GotoExternalInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE, @function
_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_23GotoExternalInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE:
.LFB6949:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6949
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -104(%rbp)
	movq	%rsi, -112(%rbp)
	movq	%fs:40, %rbx
	movq	%rbx, -56(%rbp)
	xorl	%ebx, %ebx
	movq	72(%rsi), %rbx
	cmpq	64(%rsi), %rbx
	je	.L282
	movq	%rdx, %r12
	leaq	-80(%rbp), %r13
	leaq	.LC38(%rip), %r15
	.p2align 4,,10
	.p2align 3
.L288:
	movq	-104(%rbp), %rax
	movl	$5, %edx
	movq	%r15, %rsi
	movq	8(%rax), %r14
	movq	%r14, %rdi
.LEHB43:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-24(%rbx), %rdx
	movq	-32(%rbx), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$3, %edx
	leaq	.LC39(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r14
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE43:
	movq	8(%r12), %rax
	movq	%r13, -96(%rbp)
	movq	-32(%rax), %rcx
	leaq	-16(%rax), %rdx
	cmpq	%rdx, %rcx
	je	.L295
	movq	%rcx, -96(%rbp)
	movq	-16(%rax), %rcx
	movq	%rcx, -80(%rbp)
.L284:
	movq	-24(%rax), %rcx
	movq	%rcx, -88(%rbp)
	movq	%rdx, -32(%rax)
	movq	$0, -24(%rax)
	movb	$0, -16(%rax)
	movq	8(%r12), %rax
	leaq	-32(%rax), %rdx
	subq	$16, %rax
	movq	%rdx, 8(%r12)
	movq	-16(%rax), %rdi
	cmpq	%rax, %rdi
	je	.L285
	call	_ZdlPv@PLT
.L285:
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r14, %rdi
.LEHB44:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rax, %rdi
	movl	$2, %edx
	leaq	.LC40(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE44:
	movq	-96(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L286
	call	_ZdlPv@PLT
	movq	-112(%rbp), %rax
	subq	$32, %rbx
	cmpq	64(%rax), %rbx
	jne	.L288
.L282:
	movq	-104(%rbp), %rax
	movl	$13, %edx
	leaq	.LC41(%rip), %rsi
	movq	8(%rax), %r12
	movq	%r12, %rdi
.LEHB45:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-112(%rbp), %rax
	movq	%r12, %rdi
	movq	40(%rax), %rdx
	movq	32(%rax), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$3, %edx
	leaq	.LC3(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE45:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L296
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L286:
	.cfi_restore_state
	movq	-112(%rbp), %rax
	subq	$32, %rbx
	cmpq	%rbx, 64(%rax)
	jne	.L288
	jmp	.L282
	.p2align 4,,10
	.p2align 3
.L295:
	movdqu	-16(%rax), %xmm0
	movaps	%xmm0, -80(%rbp)
	jmp	.L284
.L296:
	call	__stack_chk_fail@PLT
.L292:
	endbr64
	movq	%rax, %r12
	jmp	.L289
	.section	.gcc_except_table._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_23GotoExternalInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE,"a",@progbits
.LLSDA6949:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6949-.LLSDACSB6949
.LLSDACSB6949:
	.uleb128 .LEHB43-.LFB6949
	.uleb128 .LEHE43-.LEHB43
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB44-.LFB6949
	.uleb128 .LEHE44-.LEHB44
	.uleb128 .L292-.LFB6949
	.uleb128 0
	.uleb128 .LEHB45-.LFB6949
	.uleb128 .LEHE45-.LEHB45
	.uleb128 0
	.uleb128 0
.LLSDACSE6949:
	.section	.text._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_23GotoExternalInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_23GotoExternalInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6949
	.type	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_23GotoExternalInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.cold, @function
_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_23GotoExternalInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.cold:
.LFSB6949:
.L289:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-96(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L290
	call	_ZdlPv@PLT
.L290:
	movq	%r12, %rdi
.LEHB46:
	call	_Unwind_Resume@PLT
.LEHE46:
	.cfi_endproc
.LFE6949:
	.section	.gcc_except_table._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_23GotoExternalInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
.LLSDAC6949:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6949-.LLSDACSBC6949
.LLSDACSBC6949:
	.uleb128 .LEHB46-.LCOLDB42
	.uleb128 .LEHE46-.LEHB46
	.uleb128 0
	.uleb128 0
.LLSDACSEC6949:
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_23GotoExternalInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.section	.text._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_23GotoExternalInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.size	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_23GotoExternalInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE, .-_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_23GotoExternalInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_23GotoExternalInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.size	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_23GotoExternalInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.cold, .-_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_23GotoExternalInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.cold
.LCOLDE42:
	.section	.text._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_23GotoExternalInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
.LHOTE42:
	.section	.rodata._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_17ReturnInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.str1.1,"aMS",@progbits,1
.LC43:
	.string	"    "
.LC44:
	.string	"arguments"
.LC45:
	.string	".PopAndReturn("
	.section	.rodata._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_17ReturnInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC46:
	.string	"    CodeStubAssembler(state_).Return("
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_17ReturnInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE,"ax",@progbits
	.align 2
.LCOLDB47:
	.section	.text._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_17ReturnInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE,"ax",@progbits
.LHOTB47:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_17ReturnInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.type	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_17ReturnInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE, @function
_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_17ReturnInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE:
.LFB6950:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6950
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdx, %rbx
	subq	$56, %rsp
	movq	8(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpl	$2, 28(%rdi)
	je	.L309
	movl	$37, %edx
	leaq	.LC46(%rip), %rsi
	movq	%r13, %rdi
.LEHB47:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE47:
.L299:
	movq	8(%rbx), %rax
	movq	8(%r12), %r13
	leaq	-64(%rbp), %r12
	movq	%r12, -80(%rbp)
	movq	-32(%rax), %rcx
	leaq	-16(%rax), %rdx
	cmpq	%rdx, %rcx
	je	.L310
	movq	%rcx, -80(%rbp)
	movq	-16(%rax), %rcx
	movq	%rcx, -64(%rbp)
.L301:
	movq	-24(%rax), %rcx
	movq	%rcx, -72(%rbp)
	movq	%rdx, -32(%rax)
	movq	$0, -24(%rax)
	movb	$0, -16(%rax)
	movq	8(%rbx), %rax
	leaq	-32(%rax), %rdx
	subq	$16, %rax
	movq	%rdx, 8(%rbx)
	movq	-16(%rax), %rdi
	cmpq	%rax, %rdi
	je	.L302
	call	_ZdlPv@PLT
.L302:
	movq	-72(%rbp), %rdx
	movq	-80(%rbp), %rsi
	movq	%r13, %rdi
.LEHB48:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rax, %rdi
	movl	$3, %edx
	leaq	.LC3(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE48:
	movq	-80(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L297
	call	_ZdlPv@PLT
.L297:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L311
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L309:
	.cfi_restore_state
	movq	%r13, %rdi
	movl	$4, %edx
	leaq	.LC43(%rip), %rsi
.LEHB49:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r13, %rdi
	movl	$9, %edx
	leaq	.LC44(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$14, %edx
	leaq	.LC45(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE49:
	jmp	.L299
	.p2align 4,,10
	.p2align 3
.L310:
	movdqu	-16(%rax), %xmm0
	movaps	%xmm0, -64(%rbp)
	jmp	.L301
.L311:
	call	__stack_chk_fail@PLT
.L307:
	endbr64
	movq	%rax, %r13
	jmp	.L304
	.section	.gcc_except_table._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_17ReturnInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE,"a",@progbits
.LLSDA6950:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6950-.LLSDACSB6950
.LLSDACSB6950:
	.uleb128 .LEHB47-.LFB6950
	.uleb128 .LEHE47-.LEHB47
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB48-.LFB6950
	.uleb128 .LEHE48-.LEHB48
	.uleb128 .L307-.LFB6950
	.uleb128 0
	.uleb128 .LEHB49-.LFB6950
	.uleb128 .LEHE49-.LEHB49
	.uleb128 0
	.uleb128 0
.LLSDACSE6950:
	.section	.text._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_17ReturnInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_17ReturnInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6950
	.type	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_17ReturnInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.cold, @function
_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_17ReturnInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.cold:
.LFSB6950:
.L304:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movq	-80(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L305
	call	_ZdlPv@PLT
.L305:
	movq	%r13, %rdi
.LEHB50:
	call	_Unwind_Resume@PLT
.LEHE50:
	.cfi_endproc
.LFE6950:
	.section	.gcc_except_table._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_17ReturnInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
.LLSDAC6950:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6950-.LLSDACSBC6950
.LLSDACSBC6950:
	.uleb128 .LEHB50-.LCOLDB47
	.uleb128 .LEHE50-.LEHB50
	.uleb128 0
	.uleb128 0
.LLSDACSEC6950:
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_17ReturnInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.section	.text._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_17ReturnInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.size	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_17ReturnInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE, .-_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_17ReturnInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_17ReturnInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.size	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_17ReturnInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.cold, .-_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_17ReturnInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.cold
.LCOLDE47:
	.section	.text._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_17ReturnInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
.LHOTE47:
	.section	.rodata._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_30PrintConstantStringInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC48:
	.string	"    CodeStubAssembler(state_).Print("
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_30PrintConstantStringInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE,"ax",@progbits
	.align 2
.LCOLDB49:
	.section	.text._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_30PrintConstantStringInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE,"ax",@progbits
.LHOTB49:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_30PrintConstantStringInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.type	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_30PrintConstantStringInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE, @function
_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_30PrintConstantStringInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE:
.LFB6951:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6951
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$36, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	leaq	.LC48(%rip), %rsi
	subq	$48, %rsp
	movq	8(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	%r12, %rdi
.LEHB51:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	-64(%rbp), %rdi
	leaq	32(%rbx), %rsi
	call	_ZN2v88internal6torque18StringLiteralQuoteERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
.LEHE51:
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rsi
	movq	%r12, %rdi
.LEHB52:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rax, %rdi
	movl	$3, %edx
	leaq	.LC3(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE52:
	movq	-64(%rbp), %rdi
	leaq	-48(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L312
	call	_ZdlPv@PLT
.L312:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L319
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L319:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
.L317:
	endbr64
	movq	%rax, %r12
	jmp	.L314
	.section	.gcc_except_table._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_30PrintConstantStringInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE,"a",@progbits
.LLSDA6951:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6951-.LLSDACSB6951
.LLSDACSB6951:
	.uleb128 .LEHB51-.LFB6951
	.uleb128 .LEHE51-.LEHB51
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB52-.LFB6951
	.uleb128 .LEHE52-.LEHB52
	.uleb128 .L317-.LFB6951
	.uleb128 0
.LLSDACSE6951:
	.section	.text._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_30PrintConstantStringInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_30PrintConstantStringInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6951
	.type	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_30PrintConstantStringInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.cold, @function
_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_30PrintConstantStringInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.cold:
.LFSB6951:
.L314:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	movq	-64(%rbp), %rdi
	leaq	-48(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L315
	call	_ZdlPv@PLT
.L315:
	movq	%r12, %rdi
.LEHB53:
	call	_Unwind_Resume@PLT
.LEHE53:
	.cfi_endproc
.LFE6951:
	.section	.gcc_except_table._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_30PrintConstantStringInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
.LLSDAC6951:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6951-.LLSDACSBC6951
.LLSDACSBC6951:
	.uleb128 .LEHB53-.LCOLDB49
	.uleb128 .LEHE53-.LEHB53
	.uleb128 0
	.uleb128 0
.LLSDACSEC6951:
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_30PrintConstantStringInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.section	.text._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_30PrintConstantStringInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.size	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_30PrintConstantStringInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE, .-_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_30PrintConstantStringInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_30PrintConstantStringInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.size	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_30PrintConstantStringInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.cold, .-_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_30PrintConstantStringInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.cold
.LCOLDE49:
	.section	.text._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_30PrintConstantStringInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
.LHOTE49:
	.section	.rodata._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_16AbortInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC50:
	.string	"    CodeStubAssembler(state_).Unreachable();\n"
	.align 8
.LC51:
	.string	"    CodeStubAssembler(state_).DebugBreak();\n"
	.align 8
.LC52:
	.string	"    CodeStubAssembler(state_).FailAssert("
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_16AbortInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE,"ax",@progbits
	.align 2
.LCOLDB53:
	.section	.text._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_16AbortInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE,"ax",@progbits
.LHOTB53:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_16AbortInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.type	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_16AbortInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE, @function
_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_16AbortInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE:
.LFB6952:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6952
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	subq	$80, %rsp
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	28(%rsi), %eax
	cmpl	$1, %eax
	je	.L321
	cmpl	$2, %eax
	je	.L322
	testl	%eax, %eax
	je	.L335
.L320:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L336
	addq	$80, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L321:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	movl	$45, %edx
	leaq	.LC50(%rip), %rsi
.LEHB54:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L320
	.p2align 4,,10
	.p2align 3
.L335:
	movq	8(%rdi), %rdi
	movl	$44, %edx
	leaq	.LC51(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L320
	.p2align 4,,10
	.p2align 3
.L322:
	movl	8(%rsi), %edi
	movq	%rsi, %rbx
	call	_ZN2v88internal6torque13SourceFileMap14PathFromV8RootB5cxx11ENS1_8SourceIdE@PLT
	leaq	-96(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal6torque18StringLiteralQuoteERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
.LEHE54:
	movq	8(%r12), %r12
	movl	$41, %edx
	leaq	.LC52(%rip), %rsi
	movq	%r12, %rdi
.LEHB55:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	-64(%rbp), %rdi
	leaq	32(%rbx), %rsi
	call	_ZN2v88internal6torque18StringLiteralQuoteERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
.LEHE55:
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rsi
	movq	%r12, %rdi
.LEHB56:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$2, %edx
	leaq	.LC27(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$2, %edx
	leaq	.LC27(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	12(%rbx), %esi
	movq	%r12, %rdi
	addl	$1, %esi
	call	_ZNSolsEi@PLT
	movq	%rax, %rdi
	movl	$3, %edx
	leaq	.LC3(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE56:
	movq	-64(%rbp), %rdi
	leaq	-48(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L325
	call	_ZdlPv@PLT
.L325:
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L320
	call	_ZdlPv@PLT
	jmp	.L320
.L336:
	call	__stack_chk_fail@PLT
.L332:
	endbr64
	movq	%rax, %r12
	jmp	.L329
.L333:
	endbr64
	movq	%rax, %r12
	jmp	.L327
	.section	.gcc_except_table._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_16AbortInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE,"a",@progbits
.LLSDA6952:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6952-.LLSDACSB6952
.LLSDACSB6952:
	.uleb128 .LEHB54-.LFB6952
	.uleb128 .LEHE54-.LEHB54
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB55-.LFB6952
	.uleb128 .LEHE55-.LEHB55
	.uleb128 .L332-.LFB6952
	.uleb128 0
	.uleb128 .LEHB56-.LFB6952
	.uleb128 .LEHE56-.LEHB56
	.uleb128 .L333-.LFB6952
	.uleb128 0
.LLSDACSE6952:
	.section	.text._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_16AbortInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_16AbortInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6952
	.type	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_16AbortInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.cold, @function
_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_16AbortInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.cold:
.LFSB6952:
.L327:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	movq	-64(%rbp), %rdi
	leaq	-48(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L329
	call	_ZdlPv@PLT
.L329:
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L330
	call	_ZdlPv@PLT
.L330:
	movq	%r12, %rdi
.LEHB57:
	call	_Unwind_Resume@PLT
.LEHE57:
	.cfi_endproc
.LFE6952:
	.section	.gcc_except_table._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_16AbortInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
.LLSDAC6952:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6952-.LLSDACSBC6952
.LLSDACSBC6952:
	.uleb128 .LEHB57-.LCOLDB53
	.uleb128 .LEHE57-.LEHB57
	.uleb128 0
	.uleb128 0
.LLSDACSEC6952:
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_16AbortInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.section	.text._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_16AbortInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.size	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_16AbortInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE, .-_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_16AbortInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_16AbortInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.size	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_16AbortInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.cold, .-_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_16AbortInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.cold
.LCOLDE53:
	.section	.text._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_16AbortInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
.LHOTE53:
	.section	.rodata._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_21UnsafeCastInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.str1.1,"aMS",@progbits,1
.LC54:
	.string	"ca_.UncheckedCast<"
.LC55:
	.string	"basic_string::append"
.LC56:
	.string	">("
.LC57:
	.string	")"
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_21UnsafeCastInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE,"ax",@progbits
	.align 2
.LCOLDB58:
	.section	.text._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_21UnsafeCastInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE,"ax",@progbits
.LHOTB58:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_21UnsafeCastInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.type	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_21UnsafeCastInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE, @function
_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_21UnsafeCastInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE:
.LFB6953:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6953
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$184, %rsp
	movq	(%rdx), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8(%rbx), %rax
	subq	%rdx, %rax
	je	.L384
	movq	32(%rsi), %rsi
	leaq	-224(%rbp), %r12
	leaq	-32(%rdx,%rax), %r14
	movq	%r12, %rdi
.LEHB58:
	call	_ZNK2v88internal6torque4Type25GetGeneratedTNodeTypeNameB5cxx11Ev@PLT
.LEHE58:
	movl	$18, %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
	leaq	.LC54(%rip), %rcx
	movq	%r12, %rdi
.LEHB59:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.LEHE59:
	leaq	-176(%rbp), %r12
	leaq	16(%rax), %rdx
	movq	%r12, -192(%rbp)
	movq	(%rax), %rcx
	cmpq	%rdx, %rcx
	je	.L385
	movq	%rcx, -192(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -176(%rbp)
.L340:
	movq	8(%rax), %rcx
	movb	$0, 16(%rax)
	movq	%rcx, -184(%rbp)
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movabsq	$4611686018427387903, %rax
	subq	-184(%rbp), %rax
	cmpq	$1, %rax
	jbe	.L386
	leaq	-192(%rbp), %rdi
	movl	$2, %edx
	leaq	.LC56(%rip), %rsi
.LEHB60:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.LEHE60:
	leaq	-144(%rbp), %r13
	leaq	16(%rax), %rdx
	movq	%r13, -160(%rbp)
	movq	(%rax), %rcx
	cmpq	%rdx, %rcx
	je	.L387
	movq	%rcx, -160(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -144(%rbp)
.L343:
	movq	8(%rax), %rcx
	movb	$0, 16(%rax)
	leaq	-160(%rbp), %rdi
	movq	%rcx, -152(%rbp)
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movq	8(%r14), %rdx
	movq	(%r14), %rsi
.LEHB61:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.LEHE61:
	leaq	-112(%rbp), %r14
	leaq	16(%rax), %rdx
	movq	%r14, -128(%rbp)
	movq	(%rax), %rcx
	cmpq	%rdx, %rcx
	je	.L388
	movq	%rcx, -128(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -112(%rbp)
.L345:
	movq	8(%rax), %rcx
	movb	$0, 16(%rax)
	movq	%rcx, -120(%rbp)
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movabsq	$4611686018427387903, %rax
	cmpq	%rax, -120(%rbp)
	je	.L389
	leaq	-128(%rbp), %rdi
	movl	$1, %edx
	leaq	.LC57(%rip), %rsi
.LEHB62:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.LEHE62:
	leaq	-80(%rbp), %r15
	leaq	16(%rax), %rdx
	movq	%r15, -96(%rbp)
	movq	(%rax), %rcx
	cmpq	%rdx, %rcx
	je	.L390
	movq	%rcx, -96(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -80(%rbp)
.L348:
	movq	8(%rax), %rcx
	movb	$0, 16(%rax)
	movq	%rcx, -88(%rbp)
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movq	(%rbx), %rdx
	movq	8(%rbx), %rax
	subq	%rdx, %rax
	movq	%rax, %rbx
	shrq	$5, %rbx
	je	.L391
	leaq	-32(%rdx,%rax), %rbx
	movq	-96(%rbp), %rax
	movq	(%rbx), %rdi
	cmpq	%r15, %rax
	je	.L392
	leaq	16(%rbx), %rdx
	cmpq	%rdx, %rdi
	je	.L393
	movq	%rax, (%rbx)
	movq	-88(%rbp), %rax
	movq	16(%rbx), %rdx
	movq	%rax, 8(%rbx)
	movq	-80(%rbp), %rax
	movq	%rax, 16(%rbx)
	testq	%rdi, %rdi
	je	.L355
	movq	%rdi, -96(%rbp)
	movq	%rdx, -80(%rbp)
.L353:
	movq	$0, -88(%rbp)
	movb	$0, (%rdi)
	movq	-96(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L356
	call	_ZdlPv@PLT
.L356:
	movq	-128(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L357
	call	_ZdlPv@PLT
.L357:
	movq	-160(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L358
	call	_ZdlPv@PLT
.L358:
	movq	-192(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L359
	call	_ZdlPv@PLT
.L359:
	movq	-224(%rbp), %rdi
	leaq	-208(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L337
	call	_ZdlPv@PLT
.L337:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L394
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L393:
	.cfi_restore_state
	movq	%rax, (%rbx)
	movq	-88(%rbp), %rax
	movq	%rax, 8(%rbx)
	movq	-80(%rbp), %rax
	movq	%rax, 16(%rbx)
.L355:
	movq	%r15, -96(%rbp)
	leaq	-80(%rbp), %r15
	movq	%r15, %rdi
	jmp	.L353
	.p2align 4,,10
	.p2align 3
.L385:
	movdqu	16(%rax), %xmm0
	movaps	%xmm0, -176(%rbp)
	jmp	.L340
	.p2align 4,,10
	.p2align 3
.L388:
	movdqu	16(%rax), %xmm2
	movaps	%xmm2, -112(%rbp)
	jmp	.L345
	.p2align 4,,10
	.p2align 3
.L387:
	movdqu	16(%rax), %xmm1
	movaps	%xmm1, -144(%rbp)
	jmp	.L343
	.p2align 4,,10
	.p2align 3
.L390:
	movdqu	16(%rax), %xmm3
	movaps	%xmm3, -80(%rbp)
	jmp	.L348
	.p2align 4,,10
	.p2align 3
.L392:
	movq	-88(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L351
	cmpq	$1, %rdx
	je	.L395
	movq	%r15, %rsi
	call	memcpy@PLT
	movq	-88(%rbp), %rdx
	movq	(%rbx), %rdi
.L351:
	movq	%rdx, 8(%rbx)
	movb	$0, (%rdi,%rdx)
	movq	-96(%rbp), %rdi
	jmp	.L353
	.p2align 4,,10
	.p2align 3
.L395:
	movzbl	-80(%rbp), %eax
	movb	%al, (%rdi)
	movq	-88(%rbp), %rdx
	movq	(%rbx), %rdi
	jmp	.L351
.L394:
	call	__stack_chk_fail@PLT
.L391:
	xorl	%edx, %edx
	orq	$-1, %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
.LEHB63:
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.LEHE63:
.L384:
	xorl	%edx, %edx
	orq	$-1, %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
.LEHB64:
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.LEHE64:
.L389:
	leaq	.LC55(%rip), %rdi
.LEHB65:
	call	_ZSt20__throw_length_errorPKc@PLT
.LEHE65:
.L386:
	leaq	.LC55(%rip), %rdi
.LEHB66:
	call	_ZSt20__throw_length_errorPKc@PLT
.LEHE66:
.L376:
	endbr64
	movq	%rax, %rbx
	jmp	.L361
.L374:
	endbr64
	movq	%rax, %rbx
	jmp	.L365
.L375:
	endbr64
	movq	%rax, %rbx
	jmp	.L363
.L373:
	endbr64
	movq	%rax, %rbx
	jmp	.L367
.L372:
	endbr64
	movq	%rax, %r12
	jmp	.L369
	.section	.gcc_except_table._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_21UnsafeCastInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE,"a",@progbits
.LLSDA6953:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6953-.LLSDACSB6953
.LLSDACSB6953:
	.uleb128 .LEHB58-.LFB6953
	.uleb128 .LEHE58-.LEHB58
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB59-.LFB6953
	.uleb128 .LEHE59-.LEHB59
	.uleb128 .L372-.LFB6953
	.uleb128 0
	.uleb128 .LEHB60-.LFB6953
	.uleb128 .LEHE60-.LEHB60
	.uleb128 .L373-.LFB6953
	.uleb128 0
	.uleb128 .LEHB61-.LFB6953
	.uleb128 .LEHE61-.LEHB61
	.uleb128 .L374-.LFB6953
	.uleb128 0
	.uleb128 .LEHB62-.LFB6953
	.uleb128 .LEHE62-.LEHB62
	.uleb128 .L375-.LFB6953
	.uleb128 0
	.uleb128 .LEHB63-.LFB6953
	.uleb128 .LEHE63-.LEHB63
	.uleb128 .L376-.LFB6953
	.uleb128 0
	.uleb128 .LEHB64-.LFB6953
	.uleb128 .LEHE64-.LEHB64
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB65-.LFB6953
	.uleb128 .LEHE65-.LEHB65
	.uleb128 .L375-.LFB6953
	.uleb128 0
	.uleb128 .LEHB66-.LFB6953
	.uleb128 .LEHE66-.LEHB66
	.uleb128 .L373-.LFB6953
	.uleb128 0
.LLSDACSE6953:
	.section	.text._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_21UnsafeCastInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_21UnsafeCastInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6953
	.type	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_21UnsafeCastInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.cold, @function
_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_21UnsafeCastInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.cold:
.LFSB6953:
.L361:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-96(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L363
	call	_ZdlPv@PLT
.L363:
	movq	-128(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L365
	call	_ZdlPv@PLT
.L365:
	movq	-160(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L367
	call	_ZdlPv@PLT
.L367:
	movq	-192(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L368
	call	_ZdlPv@PLT
.L368:
	movq	%rbx, %r12
.L369:
	movq	-224(%rbp), %rdi
	leaq	-208(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L370
	call	_ZdlPv@PLT
.L370:
	movq	%r12, %rdi
.LEHB67:
	call	_Unwind_Resume@PLT
.LEHE67:
	.cfi_endproc
.LFE6953:
	.section	.gcc_except_table._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_21UnsafeCastInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
.LLSDAC6953:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6953-.LLSDACSBC6953
.LLSDACSBC6953:
	.uleb128 .LEHB67-.LCOLDB58
	.uleb128 .LEHE67-.LEHB67
	.uleb128 0
	.uleb128 0
.LLSDACSEC6953:
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_21UnsafeCastInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.section	.text._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_21UnsafeCastInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.size	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_21UnsafeCastInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE, .-_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_21UnsafeCastInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_21UnsafeCastInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.size	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_21UnsafeCastInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.cold, .-_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_21UnsafeCastInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.cold
.LCOLDE58:
	.section	.text._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_21UnsafeCastInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
.LHOTE58:
	.section	.rodata._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_25StoreReferenceInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC59:
	.string	"    CodeStubAssembler(state_).StoreReference(CodeStubAssembler::Reference{"
	.section	.rodata._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_25StoreReferenceInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.str1.1,"aMS",@progbits,1
.LC60:
	.string	"}, "
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_25StoreReferenceInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE,"ax",@progbits
	.align 2
.LCOLDB61:
	.section	.text._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_25StoreReferenceInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE,"ax",@progbits
.LHOTB61:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_25StoreReferenceInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.type	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_25StoreReferenceInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE, @function
_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_25StoreReferenceInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE:
.LFB6957:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6957
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-144(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8(%rdx), %rax
	movq	%r13, -160(%rbp)
	movq	-32(%rax), %rcx
	leaq	-16(%rax), %rdx
	cmpq	%rdx, %rcx
	je	.L416
	movq	%rcx, -160(%rbp)
	movq	-16(%rax), %rcx
	movq	%rcx, -144(%rbp)
.L398:
	movq	-24(%rax), %rcx
	movq	%rcx, -152(%rbp)
	movq	%rdx, -32(%rax)
	movq	$0, -24(%rax)
	movb	$0, -16(%rax)
	movq	8(%rbx), %rax
	leaq	-32(%rax), %rdx
	subq	$16, %rax
	movq	%rdx, 8(%rbx)
	movq	-16(%rax), %rdi
	cmpq	%rax, %rdi
	je	.L399
	call	_ZdlPv@PLT
.L399:
	movq	8(%rbx), %rax
	leaq	-112(%rbp), %r14
	movq	%r14, -128(%rbp)
	movq	-32(%rax), %rcx
	leaq	-16(%rax), %rdx
	cmpq	%rdx, %rcx
	je	.L417
	movq	%rcx, -128(%rbp)
	movq	-16(%rax), %rcx
	movq	%rcx, -112(%rbp)
.L401:
	movq	-24(%rax), %rcx
	movq	%rcx, -120(%rbp)
	movq	%rdx, -32(%rax)
	movq	$0, -24(%rax)
	movb	$0, -16(%rax)
	movq	8(%rbx), %rax
	leaq	-32(%rax), %rdx
	subq	$16, %rax
	movq	%rdx, 8(%rbx)
	movq	-16(%rax), %rdi
	cmpq	%rax, %rdi
	je	.L402
	call	_ZdlPv@PLT
.L402:
	movq	8(%rbx), %rax
	leaq	-80(%rbp), %r15
	movq	%r15, -96(%rbp)
	movq	-32(%rax), %rcx
	leaq	-16(%rax), %rdx
	cmpq	%rdx, %rcx
	je	.L418
	movq	%rcx, -96(%rbp)
	movq	-16(%rax), %rcx
	movq	%rcx, -80(%rbp)
.L404:
	movq	-24(%rax), %rcx
	movq	%rcx, -88(%rbp)
	movq	%rdx, -32(%rax)
	movq	$0, -24(%rax)
	movb	$0, -16(%rax)
	movq	8(%rbx), %rax
	leaq	-32(%rax), %rdx
	subq	$16, %rax
	movq	%rdx, 8(%rbx)
	movq	-16(%rax), %rdi
	cmpq	%rax, %rdi
	je	.L405
	call	_ZdlPv@PLT
.L405:
	movq	8(%r12), %r12
	movl	$74, %edx
	leaq	.LC59(%rip), %rsi
	movq	%r12, %rdi
.LEHB68:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$2, %edx
	leaq	.LC27(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-120(%rbp), %rdx
	movq	-128(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$3, %edx
	leaq	.LC60(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-152(%rbp), %rdx
	movq	-160(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rax, %rdi
	movl	$3, %edx
	leaq	.LC3(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE68:
	movq	-96(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L406
	call	_ZdlPv@PLT
.L406:
	movq	-128(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L407
	call	_ZdlPv@PLT
.L407:
	movq	-160(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L396
	call	_ZdlPv@PLT
.L396:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L419
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L416:
	.cfi_restore_state
	movdqu	-16(%rax), %xmm0
	movaps	%xmm0, -144(%rbp)
	jmp	.L398
	.p2align 4,,10
	.p2align 3
.L418:
	movdqu	-16(%rax), %xmm2
	movaps	%xmm2, -80(%rbp)
	jmp	.L404
	.p2align 4,,10
	.p2align 3
.L417:
	movdqu	-16(%rax), %xmm1
	movaps	%xmm1, -112(%rbp)
	jmp	.L401
.L419:
	call	__stack_chk_fail@PLT
.L414:
	endbr64
	movq	%rax, %r12
	jmp	.L409
	.section	.gcc_except_table._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_25StoreReferenceInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE,"a",@progbits
.LLSDA6957:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6957-.LLSDACSB6957
.LLSDACSB6957:
	.uleb128 .LEHB68-.LFB6957
	.uleb128 .LEHE68-.LEHB68
	.uleb128 .L414-.LFB6957
	.uleb128 0
.LLSDACSE6957:
	.section	.text._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_25StoreReferenceInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_25StoreReferenceInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6957
	.type	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_25StoreReferenceInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.cold, @function
_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_25StoreReferenceInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.cold:
.LFSB6957:
.L409:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-96(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L410
	call	_ZdlPv@PLT
.L410:
	movq	-128(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L411
	call	_ZdlPv@PLT
.L411:
	movq	-160(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L412
	call	_ZdlPv@PLT
.L412:
	movq	%r12, %rdi
.LEHB69:
	call	_Unwind_Resume@PLT
.LEHE69:
	.cfi_endproc
.LFE6957:
	.section	.gcc_except_table._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_25StoreReferenceInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
.LLSDAC6957:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6957-.LLSDACSBC6957
.LLSDACSBC6957:
	.uleb128 .LEHB69-.LCOLDB61
	.uleb128 .LEHE69-.LEHB69
	.uleb128 0
	.uleb128 0
.LLSDACSEC6957:
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_25StoreReferenceInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.section	.text._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_25StoreReferenceInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.size	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_25StoreReferenceInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE, .-_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_25StoreReferenceInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_25StoreReferenceInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.size	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_25StoreReferenceInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.cold, .-_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_25StoreReferenceInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.cold
.LCOLDE61:
	.section	.text._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_25StoreReferenceInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
.LHOTE61:
	.section	.rodata._ZN2v88internal6torque12CSAGenerator12EmitCSAValueENS1_11VisitResultERKNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEERSo.str1.1,"aMS",@progbits,1
.LC62:
	.string	"{"
.LC63:
	.string	"}"
.LC64:
	.string	"compiler::TNode<"
.LC65:
	.string	">{"
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator12EmitCSAValueENS1_11VisitResultERKNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEERSo,"ax",@progbits
	.align 2
.LCOLDB66:
	.section	.text._ZN2v88internal6torque12CSAGenerator12EmitCSAValueENS1_11VisitResultERKNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEERSo,"ax",@progbits
.LHOTB66:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque12CSAGenerator12EmitCSAValueENS1_11VisitResultERKNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEERSo
	.type	_ZN2v88internal6torque12CSAGenerator12EmitCSAValueENS1_11VisitResultERKNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEERSo, @function
_ZN2v88internal6torque12CSAGenerator12EmitCSAValueENS1_11VisitResultERKNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEERSo:
.LFB6958:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6958
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$232, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 48(%rdi)
	je	.L468
	movq	(%rdi), %r15
	movq	%rsi, %r13
	testq	%r15, %r15
	je	.L423
	cmpl	$4, 8(%r15)
	jne	.L423
	leaq	-128(%rbp), %r14
	movq	%r15, %rsi
	movq	%r14, %rdi
.LEHB70:
	call	_ZNK2v88internal6torque4Type20GetGeneratedTypeNameB5cxx11Ev@PLT
.LEHE70:
	movq	-120(%rbp), %rdx
	movq	-128(%rbp), %rsi
	movq	%r12, %rdi
.LEHB71:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rax, %rdi
	movl	$1, %edx
	leaq	.LC62(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE71:
	movq	-128(%rbp), %rdi
	leaq	-112(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L424
	call	_ZdlPv@PLT
.L424:
	cmpb	$0, 72(%r15)
	jne	.L425
	movq	(%r15), %rax
	movq	%r15, %rdi
.LEHB72:
	call	*96(%rax)
.LEHE72:
.L425:
	movq	88(%r15), %rcx
	movq	80(%r15), %rax
	movq	%rcx, -248(%rbp)
	cmpq	%rcx, %rax
	je	.L426
	addq	$104, %rax
	movq	%rax, -232(%rbp)
	leaq	-176(%rbp), %rax
	movq	%rax, -256(%rbp)
.L427:
	movq	-232(%rbp), %rax
	cmpb	$0, 8(%rbx)
	movb	$0, -200(%rbp)
	movb	$0, -192(%rbp)
	leaq	-56(%rax), %r15
	movq	(%rbx), %rax
	movq	%rax, -208(%rbp)
	jne	.L469
.L428:
	movdqu	48(%rbx), %xmm0
	movq	64(%rbx), %rax
	movq	%r15, %rdx
	movq	%r14, %rdi
	leaq	-208(%rbp), %rsi
	movq	%rax, -144(%rbp)
	movaps	%xmm0, -160(%rbp)
.LEHB73:
	call	_ZN2v88internal6torque18ProjectStructFieldENS1_11VisitResultERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
.LEHE73:
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
.LEHB74:
	call	_ZN2v88internal6torque12CSAGenerator12EmitCSAValueENS1_11VisitResultERKNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEERSo
.LEHE74:
	cmpb	$0, -120(%rbp)
	je	.L437
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L437
	call	_ZdlPv@PLT
.L437:
	cmpb	$0, -200(%rbp)
	je	.L438
	movq	-192(%rbp), %rdi
	cmpq	-256(%rbp), %rdi
	je	.L438
	call	_ZdlPv@PLT
.L438:
	movq	-232(%rbp), %rcx
	cmpq	%rcx, -248(%rbp)
	je	.L426
	movl	$2, %edx
	leaq	.LC27(%rip), %rsi
	movq	%r12, %rdi
.LEHB75:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$104, -232(%rbp)
	jmp	.L427
	.p2align 4,,10
	.p2align 3
.L423:
	movl	$16, %edx
	leaq	.LC64(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rsi
	leaq	-128(%rbp), %rdi
	call	_ZNK2v88internal6torque4Type25GetGeneratedTNodeTypeNameB5cxx11Ev@PLT
.LEHE75:
	movq	-120(%rbp), %rdx
	movq	-128(%rbp), %rsi
	movq	%r12, %rdi
.LEHB76:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$2, %edx
	leaq	.LC65(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	0(%r13), %rax
	movq	8(%r13), %rdx
	movq	56(%rbx), %rsi
	subq	%rax, %rdx
	sarq	$5, %rdx
	cmpq	%rdx, %rsi
	jnb	.L470
	salq	$5, %rsi
	movq	%r12, %rdi
	addq	%rax, %rsi
	movq	8(%rsi), %rdx
	movq	(%rsi), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rax, %rdi
	movl	$1, %edx
	leaq	.LC63(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE76:
	movq	-128(%rbp), %rdi
	leaq	-112(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L420
	call	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L420:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L471
	addq	$232, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L469:
	.cfi_restore_state
	movq	-256(%rbp), %rax
	movq	16(%rbx), %rcx
	movq	24(%rbx), %rdx
	movq	%rax, -192(%rbp)
	movq	%rcx, %rax
	addq	%rdx, %rax
	movq	%rcx, -264(%rbp)
	movq	%rdx, -240(%rbp)
	je	.L429
	testq	%rcx, %rcx
	je	.L472
.L429:
	movq	-240(%rbp), %rax
	movq	%rax, -216(%rbp)
	cmpq	$15, %rax
	ja	.L473
	cmpq	$1, -240(%rbp)
	jne	.L432
	movq	-264(%rbp), %rax
	movzbl	(%rax), %eax
	movb	%al, -176(%rbp)
	movq	-256(%rbp), %rax
.L433:
	movq	-240(%rbp), %rcx
	movq	%rcx, -184(%rbp)
	movb	$0, (%rax,%rcx)
	movb	$1, -200(%rbp)
	jmp	.L428
	.p2align 4,,10
	.p2align 3
.L426:
	movl	$1, %edx
	leaq	.LC63(%rip), %rsi
	movq	%r12, %rdi
.LEHB77:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L420
	.p2align 4,,10
	.p2align 3
.L468:
	movq	24(%rdi), %rdx
	movq	16(%rdi), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE77:
	jmp	.L420
	.p2align 4,,10
	.p2align 3
.L432:
	cmpq	$0, -240(%rbp)
	jne	.L474
	movq	-256(%rbp), %rax
	jmp	.L433
	.p2align 4,,10
	.p2align 3
.L473:
	leaq	-216(%rbp), %rsi
	leaq	-192(%rbp), %rdi
	xorl	%edx, %edx
.LEHB78:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE78:
	movq	%rax, -192(%rbp)
	movq	%rax, %rdi
	movq	-216(%rbp), %rax
	movq	%rax, -176(%rbp)
.L431:
	movq	-240(%rbp), %rdx
	movq	-264(%rbp), %rsi
	call	memcpy@PLT
	movq	-216(%rbp), %rax
	movq	%rax, -240(%rbp)
	movq	-192(%rbp), %rax
	jmp	.L433
.L471:
	call	__stack_chk_fail@PLT
.L474:
	movq	-256(%rbp), %rdi
	jmp	.L431
.L470:
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
.LEHB79:
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.LEHE79:
.L472:
	leaq	.LC6(%rip), %rdi
.LEHB80:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE80:
.L456:
	endbr64
	movq	%rax, %r12
	jmp	.L435
.L453:
	endbr64
	movq	%rax, %r12
	jmp	.L446
.L455:
	endbr64
	movq	%rax, %r12
	jmp	.L448
.L454:
	endbr64
	movq	%rax, %r12
	jmp	.L444
.L452:
	endbr64
	movq	%rax, %r12
	jmp	.L442
	.section	.gcc_except_table._ZN2v88internal6torque12CSAGenerator12EmitCSAValueENS1_11VisitResultERKNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEERSo,"a",@progbits
.LLSDA6958:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6958-.LLSDACSB6958
.LLSDACSB6958:
	.uleb128 .LEHB70-.LFB6958
	.uleb128 .LEHE70-.LEHB70
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB71-.LFB6958
	.uleb128 .LEHE71-.LEHB71
	.uleb128 .L452-.LFB6958
	.uleb128 0
	.uleb128 .LEHB72-.LFB6958
	.uleb128 .LEHE72-.LEHB72
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB73-.LFB6958
	.uleb128 .LEHE73-.LEHB73
	.uleb128 .L453-.LFB6958
	.uleb128 0
	.uleb128 .LEHB74-.LFB6958
	.uleb128 .LEHE74-.LEHB74
	.uleb128 .L454-.LFB6958
	.uleb128 0
	.uleb128 .LEHB75-.LFB6958
	.uleb128 .LEHE75-.LEHB75
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB76-.LFB6958
	.uleb128 .LEHE76-.LEHB76
	.uleb128 .L455-.LFB6958
	.uleb128 0
	.uleb128 .LEHB77-.LFB6958
	.uleb128 .LEHE77-.LEHB77
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB78-.LFB6958
	.uleb128 .LEHE78-.LEHB78
	.uleb128 .L456-.LFB6958
	.uleb128 0
	.uleb128 .LEHB79-.LFB6958
	.uleb128 .LEHE79-.LEHB79
	.uleb128 .L455-.LFB6958
	.uleb128 0
	.uleb128 .LEHB80-.LFB6958
	.uleb128 .LEHE80-.LEHB80
	.uleb128 .L456-.LFB6958
	.uleb128 0
.LLSDACSE6958:
	.section	.text._ZN2v88internal6torque12CSAGenerator12EmitCSAValueENS1_11VisitResultERKNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEERSo
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator12EmitCSAValueENS1_11VisitResultERKNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEERSo
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6958
	.type	_ZN2v88internal6torque12CSAGenerator12EmitCSAValueENS1_11VisitResultERKNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEERSo.cold, @function
_ZN2v88internal6torque12CSAGenerator12EmitCSAValueENS1_11VisitResultERKNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEERSo.cold:
.LFSB6958:
.L435:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	cmpb	$0, -200(%rbp)
	je	.L436
	movq	-192(%rbp), %rdi
	cmpq	-256(%rbp), %rdi
	je	.L436
	call	_ZdlPv@PLT
.L436:
	movq	%r12, %rdi
.LEHB81:
	call	_Unwind_Resume@PLT
.L444:
	cmpb	$0, -120(%rbp)
	je	.L446
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L446
	call	_ZdlPv@PLT
.L446:
	cmpb	$0, -200(%rbp)
	je	.L447
	movq	-192(%rbp), %rdi
	leaq	-176(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L447
	call	_ZdlPv@PLT
.L447:
	movq	%r12, %rdi
	call	_Unwind_Resume@PLT
.L448:
	movq	-128(%rbp), %rdi
	leaq	-112(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L449
	call	_ZdlPv@PLT
.L449:
	movq	%r12, %rdi
	call	_Unwind_Resume@PLT
.L442:
	movq	-128(%rbp), %rdi
	leaq	-112(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L443
	call	_ZdlPv@PLT
.L443:
	movq	%r12, %rdi
	call	_Unwind_Resume@PLT
.LEHE81:
	.cfi_endproc
.LFE6958:
	.section	.gcc_except_table._ZN2v88internal6torque12CSAGenerator12EmitCSAValueENS1_11VisitResultERKNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEERSo
.LLSDAC6958:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6958-.LLSDACSBC6958
.LLSDACSBC6958:
	.uleb128 .LEHB81-.LCOLDB66
	.uleb128 .LEHE81-.LEHB81
	.uleb128 0
	.uleb128 0
.LLSDACSEC6958:
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator12EmitCSAValueENS1_11VisitResultERKNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEERSo
	.section	.text._ZN2v88internal6torque12CSAGenerator12EmitCSAValueENS1_11VisitResultERKNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEERSo
	.size	_ZN2v88internal6torque12CSAGenerator12EmitCSAValueENS1_11VisitResultERKNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEERSo, .-_ZN2v88internal6torque12CSAGenerator12EmitCSAValueENS1_11VisitResultERKNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEERSo
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator12EmitCSAValueENS1_11VisitResultERKNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEERSo
	.size	_ZN2v88internal6torque12CSAGenerator12EmitCSAValueENS1_11VisitResultERKNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEERSo.cold, .-_ZN2v88internal6torque12CSAGenerator12EmitCSAValueENS1_11VisitResultERKNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEERSo.cold
.LCOLDE66:
	.section	.text._ZN2v88internal6torque12CSAGenerator12EmitCSAValueENS1_11VisitResultERKNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEERSo
.LHOTE66:
	.section	.text._ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EED2Ev,"axG",@progbits,_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EED2Ev
	.type	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EED2Ev, @function
_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EED2Ev:
.LFB7248:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	8(%rdi), %rbx
	movq	(%rdi), %r12
	cmpq	%r12, %rbx
	je	.L476
	.p2align 4,,10
	.p2align 3
.L480:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L477
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L480
.L478:
	movq	0(%r13), %r12
.L476:
	testq	%r12, %r12
	je	.L475
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L477:
	.cfi_restore_state
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L480
	jmp	.L478
	.p2align 4,,10
	.p2align 3
.L475:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7248:
	.size	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EED2Ev, .-_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EED2Ev
	.weak	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EED1Ev
	.set	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EED1Ev,_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EED2Ev
	.section	.text.unlikely._ZN2v88internal6torqueL7MessageIJRA38_KcEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0,"ax",@progbits
.LCOLDB67:
	.section	.text._ZN2v88internal6torqueL7MessageIJRA38_KcEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0,"ax",@progbits
.LHOTB67:
	.p2align 4
	.type	_ZN2v88internal6torqueL7MessageIJRA38_KcEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0, @function
_ZN2v88internal6torqueL7MessageIJRA38_KcEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0:
.LFB12391:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA12391
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-432(%rbp), %r14
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%r14, %rdi
	subq	$440, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
.LEHB82:
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEC1Ev@PLT
.LEHE82:
	leaq	-416(%rbp), %rdi
	movq	%r13, %rsi
.LEHB83:
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	leaq	-464(%rbp), %r13
	leaq	-408(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNKSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEE3strEv@PLT
.LEHE83:
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
.LEHB84:
	call	_ZN2v88internal6torque14MessageBuilderC1ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_13TorqueMessage4KindE@PLT
.LEHE84:
	movq	-464(%rbp), %rdi
	leaq	-448(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L484
	call	_ZdlPv@PLT
.L484:
	movq	%r14, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L493
	addq	$440, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L493:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
.L490:
	endbr64
	movq	%rax, %r12
	jmp	.L485
.L489:
	endbr64
	movq	%rax, %r12
	jmp	.L487
	.section	.gcc_except_table._ZN2v88internal6torqueL7MessageIJRA38_KcEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0,"a",@progbits
.LLSDA12391:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE12391-.LLSDACSB12391
.LLSDACSB12391:
	.uleb128 .LEHB82-.LFB12391
	.uleb128 .LEHE82-.LEHB82
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB83-.LFB12391
	.uleb128 .LEHE83-.LEHB83
	.uleb128 .L489-.LFB12391
	.uleb128 0
	.uleb128 .LEHB84-.LFB12391
	.uleb128 .LEHE84-.LEHB84
	.uleb128 .L490-.LFB12391
	.uleb128 0
.LLSDACSE12391:
	.section	.text._ZN2v88internal6torqueL7MessageIJRA38_KcEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torqueL7MessageIJRA38_KcEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC12391
	.type	_ZN2v88internal6torqueL7MessageIJRA38_KcEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0.cold, @function
_ZN2v88internal6torqueL7MessageIJRA38_KcEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0.cold:
.LFSB12391:
.L485:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	movq	-464(%rbp), %rdi
	leaq	-448(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L487
	call	_ZdlPv@PLT
.L487:
	movq	%r14, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r12, %rdi
.LEHB85:
	call	_Unwind_Resume@PLT
.LEHE85:
	.cfi_endproc
.LFE12391:
	.section	.gcc_except_table._ZN2v88internal6torqueL7MessageIJRA38_KcEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
.LLSDAC12391:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC12391-.LLSDACSBC12391
.LLSDACSBC12391:
	.uleb128 .LEHB85-.LCOLDB67
	.uleb128 .LEHE85-.LEHB85
	.uleb128 0
	.uleb128 0
.LLSDACSEC12391:
	.section	.text.unlikely._ZN2v88internal6torqueL7MessageIJRA38_KcEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
	.section	.text._ZN2v88internal6torqueL7MessageIJRA38_KcEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
	.size	_ZN2v88internal6torqueL7MessageIJRA38_KcEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0, .-_ZN2v88internal6torqueL7MessageIJRA38_KcEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
	.section	.text.unlikely._ZN2v88internal6torqueL7MessageIJRA38_KcEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
	.size	_ZN2v88internal6torqueL7MessageIJRA38_KcEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0.cold, .-_ZN2v88internal6torqueL7MessageIJRA38_KcEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0.cold
.LCOLDE67:
	.section	.text._ZN2v88internal6torqueL7MessageIJRA38_KcEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
.LHOTE67:
	.section	.text._ZN2v88internal6torque11ReportErrorIJRA21_KcRKNS1_4TypeERA22_S3_S8_EEEvDpOT_,"axG",@progbits,_ZN2v88internal6torque11ReportErrorIJRA21_KcRKNS1_4TypeERA22_S3_S8_EEEvDpOT_,comdat
	.p2align 4
	.weak	_ZN2v88internal6torque11ReportErrorIJRA21_KcRKNS1_4TypeERA22_S3_S8_EEEvDpOT_
	.type	_ZN2v88internal6torque11ReportErrorIJRA21_KcRKNS1_4TypeERA22_S3_S8_EEEvDpOT_, @function
_ZN2v88internal6torque11ReportErrorIJRA21_KcRKNS1_4TypeERA22_S3_S8_EEEvDpOT_:
.LFB8333:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA8333
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-432(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-448(%rbp), %rbx
	subq	$520, %rsp
	movq	%rdi, -552(%rbp)
	movq	%rbx, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
.LEHB86:
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEC1Ev@PLT
.LEHE86:
	movq	-552(%rbp), %r8
	movq	%r12, %rdi
	movq	%r8, %rsi
.LEHB87:
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6torquelsERSoRKNS1_4TypeE
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6torquelsERSoRKNS1_4TypeE
	leaq	-544(%rbp), %r13
	leaq	-424(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNKSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEE3strEv@PLT
.LEHE87:
	leaq	-512(%rbp), %r12
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
.LEHB88:
	call	_ZN2v88internal6torque14MessageBuilderC1ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_13TorqueMessage4KindE@PLT
.LEHE88:
	movq	-544(%rbp), %rdi
	leaq	-528(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L495
	call	_ZdlPv@PLT
.L495:
	movq	%rbx, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r12, %rdi
.LEHB89:
	call	_ZNK2v88internal6torque14MessageBuilder5ThrowEv@PLT
.LEHE89:
.L502:
	endbr64
	movq	%rax, %r12
	jmp	.L498
.L501:
	endbr64
	movq	%rax, %r13
	jmp	.L499
.L503:
	endbr64
	movq	%rax, %r12
.L496:
	movq	-544(%rbp), %rdi
	leaq	-528(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L498
	call	_ZdlPv@PLT
.L498:
	movq	%rbx, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r12, %rdi
.LEHB90:
	call	_Unwind_Resume@PLT
.L499:
	movq	%r12, %rdi
	call	_ZN2v88internal6torque14MessageBuilderD1Ev
	movq	%r13, %rdi
	call	_Unwind_Resume@PLT
.LEHE90:
	.cfi_endproc
.LFE8333:
	.section	.gcc_except_table._ZN2v88internal6torque11ReportErrorIJRA21_KcRKNS1_4TypeERA22_S3_S8_EEEvDpOT_,"aG",@progbits,_ZN2v88internal6torque11ReportErrorIJRA21_KcRKNS1_4TypeERA22_S3_S8_EEEvDpOT_,comdat
.LLSDA8333:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE8333-.LLSDACSB8333
.LLSDACSB8333:
	.uleb128 .LEHB86-.LFB8333
	.uleb128 .LEHE86-.LEHB86
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB87-.LFB8333
	.uleb128 .LEHE87-.LEHB87
	.uleb128 .L502-.LFB8333
	.uleb128 0
	.uleb128 .LEHB88-.LFB8333
	.uleb128 .LEHE88-.LEHB88
	.uleb128 .L503-.LFB8333
	.uleb128 0
	.uleb128 .LEHB89-.LFB8333
	.uleb128 .LEHE89-.LEHB89
	.uleb128 .L501-.LFB8333
	.uleb128 0
	.uleb128 .LEHB90-.LFB8333
	.uleb128 .LEHE90-.LEHB90
	.uleb128 0
	.uleb128 0
.LLSDACSE8333:
	.section	.text._ZN2v88internal6torque11ReportErrorIJRA21_KcRKNS1_4TypeERA22_S3_S8_EEEvDpOT_,"axG",@progbits,_ZN2v88internal6torque11ReportErrorIJRA21_KcRKNS1_4TypeERA22_S3_S8_EEEvDpOT_,comdat
	.size	_ZN2v88internal6torque11ReportErrorIJRA21_KcRKNS1_4TypeERA22_S3_S8_EEEvDpOT_, .-_ZN2v88internal6torque11ReportErrorIJRA21_KcRKNS1_4TypeERA22_S3_S8_EEEvDpOT_
	.section	.text._ZN2v88internal6torque11ReportErrorIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_,"axG",@progbits,_ZN2v88internal6torque11ReportErrorIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_,comdat
	.p2align 4
	.weak	_ZN2v88internal6torque11ReportErrorIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_
	.type	_ZN2v88internal6torque11ReportErrorIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_, @function
_ZN2v88internal6torque11ReportErrorIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_:
.LFB8342:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA8342
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-432(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%r13, %rdi
	subq	$496, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
.LEHB91:
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEC1Ev@PLT
.LEHE91:
	movq	8(%rbx), %rdx
	movq	(%rbx), %rsi
	leaq	-416(%rbp), %rdi
.LEHB92:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	-528(%rbp), %r14
	leaq	-408(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZNKSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEE3strEv@PLT
.LEHE92:
	leaq	-496(%rbp), %r12
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
.LEHB93:
	call	_ZN2v88internal6torque14MessageBuilderC1ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_13TorqueMessage4KindE@PLT
.LEHE93:
	movq	-528(%rbp), %rdi
	leaq	-512(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L506
	call	_ZdlPv@PLT
.L506:
	movq	%r13, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r12, %rdi
.LEHB94:
	call	_ZNK2v88internal6torque14MessageBuilder5ThrowEv@PLT
.LEHE94:
.L513:
	endbr64
	movq	%rax, %r12
	jmp	.L509
.L512:
	endbr64
	movq	%rax, %r13
	jmp	.L510
.L514:
	endbr64
	movq	%rax, %r12
.L507:
	movq	-528(%rbp), %rdi
	leaq	-512(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L509
	call	_ZdlPv@PLT
.L509:
	movq	%r13, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r12, %rdi
.LEHB95:
	call	_Unwind_Resume@PLT
.L510:
	movq	%r12, %rdi
	call	_ZN2v88internal6torque14MessageBuilderD1Ev
	movq	%r13, %rdi
	call	_Unwind_Resume@PLT
.LEHE95:
	.cfi_endproc
.LFE8342:
	.section	.gcc_except_table._ZN2v88internal6torque11ReportErrorIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_,"aG",@progbits,_ZN2v88internal6torque11ReportErrorIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_,comdat
.LLSDA8342:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE8342-.LLSDACSB8342
.LLSDACSB8342:
	.uleb128 .LEHB91-.LFB8342
	.uleb128 .LEHE91-.LEHB91
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB92-.LFB8342
	.uleb128 .LEHE92-.LEHB92
	.uleb128 .L513-.LFB8342
	.uleb128 0
	.uleb128 .LEHB93-.LFB8342
	.uleb128 .LEHE93-.LEHB93
	.uleb128 .L514-.LFB8342
	.uleb128 0
	.uleb128 .LEHB94-.LFB8342
	.uleb128 .LEHE94-.LEHB94
	.uleb128 .L512-.LFB8342
	.uleb128 0
	.uleb128 .LEHB95-.LFB8342
	.uleb128 .LEHE95-.LEHB95
	.uleb128 0
	.uleb128 0
.LLSDACSE8342:
	.section	.text._ZN2v88internal6torque11ReportErrorIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_,"axG",@progbits,_ZN2v88internal6torque11ReportErrorIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_,comdat
	.size	_ZN2v88internal6torque11ReportErrorIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_, .-_ZN2v88internal6torque11ReportErrorIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_
	.section	.text._ZN2v88internal6torque11ReportErrorIJRA39_KcRKPKNS1_4TypeERA42_S3_EEEvDpOT_,"axG",@progbits,_ZN2v88internal6torque11ReportErrorIJRA39_KcRKPKNS1_4TypeERA42_S3_EEEvDpOT_,comdat
	.p2align 4
	.weak	_ZN2v88internal6torque11ReportErrorIJRA39_KcRKPKNS1_4TypeERA42_S3_EEEvDpOT_
	.type	_ZN2v88internal6torque11ReportErrorIJRA39_KcRKPKNS1_4TypeERA42_S3_EEEvDpOT_, @function
_ZN2v88internal6torque11ReportErrorIJRA39_KcRKPKNS1_4TypeERA42_S3_EEEvDpOT_:
.LFB8389:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA8389
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-448(%rbp), %r15
	movq	%rdi, %r14
	pushq	%r13
	movq	%r15, %rdi
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-432(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$504, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
.LEHB96:
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEC1Ev@PLT
.LEHE96:
	movq	%r14, %rsi
	movq	%r12, %rdi
.LEHB97:
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSo9_M_insertIPKvEERSoT_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	leaq	-544(%rbp), %r13
	leaq	-424(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNKSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEE3strEv@PLT
.LEHE97:
	leaq	-512(%rbp), %r12
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
.LEHB98:
	call	_ZN2v88internal6torque14MessageBuilderC1ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_13TorqueMessage4KindE@PLT
.LEHE98:
	movq	-544(%rbp), %rdi
	leaq	-528(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L517
	call	_ZdlPv@PLT
.L517:
	movq	%r15, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r12, %rdi
.LEHB99:
	call	_ZNK2v88internal6torque14MessageBuilder5ThrowEv@PLT
.LEHE99:
.L524:
	endbr64
	movq	%rax, %r12
	jmp	.L520
.L523:
	endbr64
	movq	%rax, %r13
	jmp	.L521
.L525:
	endbr64
	movq	%rax, %r12
.L518:
	movq	-544(%rbp), %rdi
	leaq	-528(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L520
	call	_ZdlPv@PLT
.L520:
	movq	%r15, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r12, %rdi
.LEHB100:
	call	_Unwind_Resume@PLT
.L521:
	movq	%r12, %rdi
	call	_ZN2v88internal6torque14MessageBuilderD1Ev
	movq	%r13, %rdi
	call	_Unwind_Resume@PLT
.LEHE100:
	.cfi_endproc
.LFE8389:
	.section	.gcc_except_table._ZN2v88internal6torque11ReportErrorIJRA39_KcRKPKNS1_4TypeERA42_S3_EEEvDpOT_,"aG",@progbits,_ZN2v88internal6torque11ReportErrorIJRA39_KcRKPKNS1_4TypeERA42_S3_EEEvDpOT_,comdat
.LLSDA8389:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE8389-.LLSDACSB8389
.LLSDACSB8389:
	.uleb128 .LEHB96-.LFB8389
	.uleb128 .LEHE96-.LEHB96
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB97-.LFB8389
	.uleb128 .LEHE97-.LEHB97
	.uleb128 .L524-.LFB8389
	.uleb128 0
	.uleb128 .LEHB98-.LFB8389
	.uleb128 .LEHE98-.LEHB98
	.uleb128 .L525-.LFB8389
	.uleb128 0
	.uleb128 .LEHB99-.LFB8389
	.uleb128 .LEHE99-.LEHB99
	.uleb128 .L523-.LFB8389
	.uleb128 0
	.uleb128 .LEHB100-.LFB8389
	.uleb128 .LEHE100-.LEHB100
	.uleb128 0
	.uleb128 0
.LLSDACSE8389:
	.section	.text._ZN2v88internal6torque11ReportErrorIJRA39_KcRKPKNS1_4TypeERA42_S3_EEEvDpOT_,"axG",@progbits,_ZN2v88internal6torque11ReportErrorIJRA39_KcRKPKNS1_4TypeERA42_S3_EEEvDpOT_,comdat
	.size	_ZN2v88internal6torque11ReportErrorIJRA39_KcRKPKNS1_4TypeERA42_S3_EEEvDpOT_, .-_ZN2v88internal6torque11ReportErrorIJRA39_KcRKPKNS1_4TypeERA42_S3_EEEvDpOT_
	.section	.rodata._ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_default_appendEm.str1.1,"aMS",@progbits,1
.LC68:
	.string	"vector::_M_default_append"
	.section	.text._ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_default_appendEm,"axG",@progbits,_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_default_appendEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_default_appendEm
	.type	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_default_appendEm, @function
_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_default_appendEm:
.LFB10251:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L551
	movabsq	$288230376151711743, %rdx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	8(%rdi), %rdi
	movq	16(%r13), %rax
	movq	%rdi, %rbx
	subq	0(%r13), %rbx
	subq	%rdi, %rax
	movq	%rbx, %r8
	sarq	$5, %rax
	sarq	$5, %r8
	subq	%r8, %rcx
	cmpq	%rax, %rsi
	ja	.L529
	movq	%rdi, %rax
	movq	%rsi, %rdx
	.p2align 4,,10
	.p2align 3
.L530:
	leaq	16(%rax), %rcx
	movq	$0, 8(%rax)
	addq	$32, %rax
	movq	%rcx, -32(%rax)
	movb	$0, -16(%rax)
	subq	$1, %rdx
	jne	.L530
	salq	$5, %r12
	addq	%r12, %rdi
	movq	%rdi, 8(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L551:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L529:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	cmpq	%rsi, %rcx
	jb	.L556
	cmpq	%r8, %rsi
	movq	%r8, %r14
	movq	%r8, -56(%rbp)
	cmovnb	%rsi, %r14
	addq	%r8, %r14
	cmpq	%rdx, %r14
	cmova	%rdx, %r14
	salq	$5, %r14
	movq	%r14, %rdi
	call	_Znwm@PLT
	movq	-56(%rbp), %r8
	movq	%r12, %rcx
	movq	%rax, %r15
	leaq	(%rax,%rbx), %rdx
	.p2align 4,,10
	.p2align 3
.L534:
	leaq	16(%rdx), %rax
	movq	$0, 8(%rdx)
	addq	$32, %rdx
	movq	%rax, -32(%rdx)
	movb	$0, -16(%rdx)
	subq	$1, %rcx
	jne	.L534
	movq	0(%r13), %r9
	movq	8(%r13), %rdi
	movq	%r9, %rdx
	cmpq	%r9, %rdi
	je	.L535
	movq	%r15, %rcx
	jmp	.L539
	.p2align 4,,10
	.p2align 3
.L536:
	movq	%rsi, (%rcx)
	movq	16(%rdx), %rax
	movq	%rax, 16(%rcx)
.L555:
	movq	8(%rdx), %rax
	addq	$32, %rdx
	addq	$32, %rcx
	movq	%rax, -24(%rcx)
	cmpq	%rdx, %rdi
	je	.L535
.L539:
	leaq	16(%rcx), %rax
	movq	%rax, (%rcx)
	movq	(%rdx), %rsi
	leaq	16(%rdx), %rax
	cmpq	%rax, %rsi
	jne	.L536
	movdqu	16(%rdx), %xmm0
	movups	%xmm0, 16(%rcx)
	jmp	.L555
	.p2align 4,,10
	.p2align 3
.L535:
	testq	%r9, %r9
	je	.L540
	movq	%r9, %rdi
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L540:
	addq	%r8, %r12
	addq	%r15, %r14
	movq	%r15, 0(%r13)
	salq	$5, %r12
	movq	%r14, 16(%r13)
	leaq	(%r15,%r12), %rdx
	movq	%rdx, 8(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L556:
	.cfi_restore_state
	leaq	.LC68(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE10251:
	.size	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_default_appendEm, .-_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_default_appendEm
	.section	.text._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_22DeleteRangeInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_22DeleteRangeInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.type	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_22DeleteRangeInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE, @function
_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_22DeleteRangeInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE:
.LFB6917:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	32(%rsi), %r14
	movq	40(%rsi), %r12
	cmpq	%r14, %r12
	je	.L557
	movq	(%rdx), %rbx
	movq	%rdx, %r13
	movq	8(%rdx), %rdx
	movq	%r14, %r15
	subq	%r12, %r15
	movq	%rdx, %rax
	subq	%rbx, %rax
	sarq	$5, %rax
	cmpq	%rax, %r12
	jnb	.L560
	salq	$5, %r14
	jmp	.L567
	.p2align 4,,10
	.p2align 3
.L561:
	leaq	16(%rbx), %rdx
	cmpq	%rdx, %rdi
	je	.L587
	movq	%rsi, (%rbx)
	movq	8(%rcx), %rsi
	movq	16(%rbx), %rdx
	movq	%rsi, 8(%rbx)
	movq	16(%rcx), %rsi
	movq	%rsi, 16(%rbx)
	testq	%rdi, %rdi
	je	.L566
	movq	%rdi, (%rcx)
	movq	%rdx, 16(%rcx)
.L564:
	movq	$0, 8(%rcx)
	addq	$1, %r12
	addq	$32, %r14
	movb	$0, (%rdi)
	movq	8(%r13), %rdx
	movq	0(%r13), %rbx
	movq	%rdx, %rax
	subq	%rbx, %rax
	sarq	$5, %rax
	cmpq	%r12, %rax
	jbe	.L560
.L567:
	movq	%r12, %rcx
	salq	$5, %rcx
	addq	%rbx, %rcx
	addq	%r14, %rbx
	movq	(%rcx), %rsi
	leaq	16(%rcx), %rax
	movq	(%rbx), %rdi
	cmpq	%rax, %rsi
	jne	.L561
	movq	8(%rcx), %rdx
	testq	%rdx, %rdx
	je	.L562
	cmpq	$1, %rdx
	je	.L588
	movq	%rcx, -56(%rbp)
	call	memcpy@PLT
	movq	-56(%rbp), %rcx
	movq	(%rbx), %rdi
	movq	8(%rcx), %rdx
.L562:
	movq	%rdx, 8(%rbx)
	movb	$0, (%rdi,%rdx)
	movq	(%rcx), %rdi
	jmp	.L564
	.p2align 4,,10
	.p2align 3
.L587:
	movq	%rsi, (%rbx)
	movq	8(%rcx), %rdx
	movq	%rdx, 8(%rbx)
	movq	16(%rcx), %rdx
	movq	%rdx, 16(%rbx)
.L566:
	movq	%rax, (%rcx)
	movq	%rax, %rdi
	jmp	.L564
	.p2align 4,,10
	.p2align 3
.L560:
	movq	%rax, %rcx
	xorl	%esi, %esi
	addq	%r15, %rcx
	setc	%sil
	cmpq	%rax, %rcx
	ja	.L589
	testq	%rsi, %rsi
	jne	.L590
.L557:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L588:
	.cfi_restore_state
	movzbl	16(%rcx), %eax
	movb	%al, (%rdi)
	movq	8(%rcx), %rdx
	movq	(%rbx), %rdi
	jmp	.L562
	.p2align 4,,10
	.p2align 3
.L589:
	addq	$24, %rsp
	movq	%r15, %rsi
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_default_appendEm
	.p2align 4,,10
	.p2align 3
.L590:
	.cfi_restore_state
	salq	$5, %rcx
	addq	%rcx, %rbx
	cmpq	%rdx, %rbx
	je	.L557
	movq	%rbx, %r12
	.p2align 4,,10
	.p2align 3
.L575:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L572
	movq	%rdx, -56(%rbp)
	addq	$32, %r12
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rdx
	cmpq	%rdx, %r12
	jne	.L575
.L573:
	movq	%rbx, 8(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L572:
	.cfi_restore_state
	addq	$32, %r12
	cmpq	%rdx, %r12
	jne	.L575
	jmp	.L573
	.cfi_endproc
.LFE6917:
	.size	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_22DeleteRangeInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE, .-_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_22DeleteRangeInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.section	.rodata._ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_.str1.1,"aMS",@progbits,1
.LC69:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_,"axG",@progbits,_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	.type	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_, @function
_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_:
.LFB10254:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r12
	movq	(%rdi), %r15
	movabsq	$288230376151711743, %rdi
	movq	%r12, %rax
	subq	%r15, %rax
	sarq	$5, %rax
	cmpq	%rdi, %rax
	je	.L618
	movq	%rsi, %rcx
	movq	%rsi, %rbx
	subq	%r15, %rcx
	testq	%rax, %rax
	je	.L609
	movabsq	$9223372036854775776, %rsi
	leaq	(%rax,%rax), %r8
	cmpq	%r8, %rax
	jbe	.L619
.L593:
	movq	%rsi, %rdi
	movq	%rdx, -72(%rbp)
	movq	%rcx, -64(%rbp)
	movq	%rsi, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rsi
	movq	-64(%rbp), %rcx
	movq	%rax, %r14
	movq	-72(%rbp), %rdx
	leaq	(%rax,%rsi), %rax
	leaq	32(%r14), %r8
.L608:
	addq	%r14, %rcx
	movq	(%rdx), %rdi
	leaq	16(%rcx), %rsi
	movq	%rsi, (%rcx)
	leaq	16(%rdx), %rsi
	cmpq	%rsi, %rdi
	je	.L620
	movq	%rdi, (%rcx)
	movq	16(%rdx), %rdi
	movq	%rdi, 16(%rcx)
.L596:
	movq	8(%rdx), %rdi
	movq	%rsi, (%rdx)
	movq	$0, 8(%rdx)
	movq	%rdi, 8(%rcx)
	movb	$0, 16(%rdx)
	cmpq	%r15, %rbx
	je	.L597
	movq	%r14, %rcx
	movq	%r15, %rdx
	jmp	.L601
	.p2align 4,,10
	.p2align 3
.L598:
	movq	%rdi, (%rcx)
	movq	16(%rdx), %rsi
	movq	%rsi, 16(%rcx)
.L617:
	movq	8(%rdx), %rsi
	addq	$32, %rdx
	addq	$32, %rcx
	movq	%rsi, -24(%rcx)
	cmpq	%rdx, %rbx
	je	.L621
.L601:
	leaq	16(%rcx), %rsi
	movq	%rsi, (%rcx)
	movq	(%rdx), %rdi
	leaq	16(%rdx), %rsi
	cmpq	%rsi, %rdi
	jne	.L598
	movdqu	16(%rdx), %xmm1
	movups	%xmm1, 16(%rcx)
	jmp	.L617
	.p2align 4,,10
	.p2align 3
.L621:
	movq	%rbx, %rdx
	subq	%r15, %rdx
	leaq	32(%r14,%rdx), %r8
.L597:
	cmpq	%r12, %rbx
	je	.L602
	movq	%rbx, %rdx
	movq	%r8, %rcx
	.p2align 4,,10
	.p2align 3
.L606:
	leaq	16(%rcx), %rsi
	movq	(%rdx), %rdi
	movq	%rsi, (%rcx)
	leaq	16(%rdx), %rsi
	cmpq	%rsi, %rdi
	je	.L622
	movq	16(%rdx), %rsi
	addq	$32, %rdx
	movq	%rdi, (%rcx)
	addq	$32, %rcx
	movq	%rsi, -16(%rcx)
	movq	-24(%rdx), %rsi
	movq	%rsi, -24(%rcx)
	cmpq	%r12, %rdx
	jne	.L606
.L604:
	subq	%rbx, %r12
	addq	%r12, %r8
.L602:
	testq	%r15, %r15
	je	.L607
	movq	%r15, %rdi
	movq	%rax, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-64(%rbp), %rax
	movq	-56(%rbp), %r8
.L607:
	movq	%r14, %xmm0
	movq	%r8, %xmm3
	movq	%rax, 16(%r13)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 0(%r13)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L622:
	.cfi_restore_state
	movdqu	16(%rdx), %xmm2
	movq	8(%rdx), %rsi
	addq	$32, %rdx
	addq	$32, %rcx
	movups	%xmm2, -16(%rcx)
	movq	%rsi, -24(%rcx)
	cmpq	%rdx, %r12
	jne	.L606
	jmp	.L604
	.p2align 4,,10
	.p2align 3
.L619:
	testq	%r8, %r8
	jne	.L594
	movl	$32, %r8d
	xorl	%eax, %eax
	xorl	%r14d, %r14d
	jmp	.L608
	.p2align 4,,10
	.p2align 3
.L609:
	movl	$32, %esi
	jmp	.L593
	.p2align 4,,10
	.p2align 3
.L620:
	movdqu	16(%rdx), %xmm4
	movups	%xmm4, 16(%rcx)
	jmp	.L596
.L594:
	cmpq	%rdi, %r8
	movq	%rdi, %rax
	cmovbe	%r8, %rax
	salq	$5, %rax
	movq	%rax, %rsi
	jmp	.L593
.L618:
	leaq	.LC69(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE10254:
	.size	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_, .-_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	.section	.text._ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE14_M_insert_rvalEN9__gnu_cxx17__normal_iteratorIPKS5_S7_EEOS5_,"axG",@progbits,_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE14_M_insert_rvalEN9__gnu_cxx17__normal_iteratorIPKS5_S7_EEOS5_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE14_M_insert_rvalEN9__gnu_cxx17__normal_iteratorIPKS5_S7_EEOS5_
	.type	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE14_M_insert_rvalEN9__gnu_cxx17__normal_iteratorIPKS5_S7_EEOS5_, @function
_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE14_M_insert_rvalEN9__gnu_cxx17__normal_iteratorIPKS5_S7_EEOS5_:
.LFB9423:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	8(%rdi), %rax
	subq	(%rdi), %r15
	cmpq	16(%rdi), %rax
	je	.L624
	leaq	16(%rax), %rdx
	leaq	16(%r13), %r8
	movq	%rdx, (%rax)
	cmpq	%rsi, %rax
	je	.L665
	movq	-32(%rax), %rcx
	leaq	-16(%rax), %rdx
	cmpq	%rdx, %rcx
	je	.L666
	movq	%rcx, (%rax)
	movq	-16(%rax), %rcx
	movq	%rcx, 16(%rax)
.L630:
	movq	-24(%rax), %rcx
	movq	%rdx, -32(%rax)
	movq	$0, -24(%rax)
	movq	%rcx, 8(%rax)
	movb	$0, -16(%rax)
	movq	8(%r12), %rbx
	leaq	32(%rbx), %rax
	movq	%rax, 8(%r12)
	leaq	-32(%rbx), %rax
	subq	$48, %rbx
	subq	%r14, %rax
	movq	%rax, %rcx
	sarq	$5, %rcx
	testq	%rax, %rax
	jg	.L641
	jmp	.L640
	.p2align 4,,10
	.p2align 3
.L634:
	leaq	32(%rbx), %rdx
	cmpq	%rdx, %rdi
	je	.L667
	movq	%rax, 16(%rbx)
	movq	-8(%rbx), %rax
	movq	32(%rbx), %rdx
	movq	%rax, 24(%rbx)
	movq	(%rbx), %rax
	movq	%rax, 32(%rbx)
	testq	%rdi, %rdi
	je	.L639
	movq	%rdi, -16(%rbx)
	movq	%rdx, (%rbx)
.L637:
	movq	-16(%rbx), %rax
	movq	$0, -8(%rbx)
	subq	$32, %rbx
	movb	$0, (%rax)
	subq	$1, %rcx
	je	.L640
.L641:
	movq	-16(%rbx), %rax
	movq	16(%rbx), %rdi
	cmpq	%rbx, %rax
	jne	.L634
	movq	-8(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L635
	cmpq	$1, %rdx
	je	.L668
	movq	%rbx, %rsi
	movq	%r8, -64(%rbp)
	movq	%rcx, -56(%rbp)
	call	memcpy@PLT
	movq	16(%rbx), %rdi
	movq	-8(%rbx), %rdx
	movq	-64(%rbp), %r8
	movq	-56(%rbp), %rcx
.L635:
	movq	%rdx, 24(%rbx)
	movb	$0, (%rdi,%rdx)
	jmp	.L637
	.p2align 4,,10
	.p2align 3
.L665:
	movq	0(%r13), %rdx
	cmpq	%r8, %rdx
	je	.L669
	movq	%rdx, (%rax)
	movq	16(%r13), %rdx
	movq	%rdx, 16(%rax)
.L627:
	movq	8(%r13), %rdx
	movq	%r8, 0(%r13)
	movq	$0, 8(%r13)
	movq	%rdx, 8(%rax)
	movb	$0, 16(%r13)
	addq	$32, 8(%r12)
.L628:
	movq	(%r12), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	addq	%r15, %rax
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L667:
	.cfi_restore_state
	movq	%rax, 16(%rbx)
	movq	-8(%rbx), %rax
	movq	%rax, 24(%rbx)
	movq	(%rbx), %rax
	movq	%rax, 32(%rbx)
.L639:
	movq	%rbx, -16(%rbx)
	jmp	.L637
	.p2align 4,,10
	.p2align 3
.L640:
	movq	0(%r13), %rax
	movq	(%r14), %rdi
	cmpq	%r8, %rax
	je	.L670
	leaq	16(%r14), %rdx
	cmpq	%rdx, %rdi
	je	.L671
	movq	%rax, (%r14)
	movq	8(%r13), %rax
	movq	16(%r14), %rdx
	movq	%rax, 8(%r14)
	movq	16(%r13), %rax
	movq	%rax, 16(%r14)
	testq	%rdi, %rdi
	je	.L646
	movq	%rdi, 0(%r13)
	movq	%rdx, 16(%r13)
.L644:
	movq	$0, 8(%r13)
	movb	$0, (%rdi)
	jmp	.L628
	.p2align 4,,10
	.p2align 3
.L671:
	movq	%rax, (%r14)
	movq	8(%r13), %rax
	movq	%rax, 8(%r14)
	movq	16(%r13), %rax
	movq	%rax, 16(%r14)
.L646:
	movq	%r8, 0(%r13)
	movq	%r8, %rdi
	jmp	.L644
	.p2align 4,,10
	.p2align 3
.L668:
	movzbl	(%rbx), %eax
	movb	%al, (%rdi)
	movq	16(%rbx), %rdi
	movq	-8(%rbx), %rdx
	jmp	.L635
	.p2align 4,,10
	.p2align 3
.L624:
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	jmp	.L628
	.p2align 4,,10
	.p2align 3
.L666:
	movdqu	-16(%rax), %xmm0
	movups	%xmm0, 16(%rax)
	jmp	.L630
	.p2align 4,,10
	.p2align 3
.L670:
	movq	8(%r13), %rdx
	testq	%rdx, %rdx
	je	.L642
	cmpq	$1, %rdx
	je	.L672
	movq	%r8, %rsi
	call	memcpy@PLT
	movq	8(%r13), %rdx
	movq	(%r14), %rdi
.L642:
	movq	%rdx, 8(%r14)
	movb	$0, (%rdi,%rdx)
	movq	0(%r13), %rdi
	jmp	.L644
	.p2align 4,,10
	.p2align 3
.L669:
	movdqu	16(%r13), %xmm1
	movups	%xmm1, 16(%rax)
	jmp	.L627
	.p2align 4,,10
	.p2align 3
.L672:
	movzbl	16(%r13), %eax
	movb	%al, (%rdi)
	movq	8(%r13), %rdx
	movq	(%r14), %rdi
	jmp	.L642
	.cfi_endproc
.LFE9423:
	.size	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE14_M_insert_rvalEN9__gnu_cxx17__normal_iteratorIPKS5_S7_EEOS5_, .-_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE14_M_insert_rvalEN9__gnu_cxx17__normal_iteratorIPKS5_S7_EEOS5_
	.section	.rodata._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_22CallRuntimeInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.str1.1,"aMS",@progbits,1
.LC70:
	.string	"vector::reserve"
	.section	.rodata._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_22CallRuntimeInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC71:
	.string	"runtime function must have at most one result"
	.align 8
.LC72:
	.string	"    CodeStubAssembler(state_).TailCallRuntime(Runtime::k"
	.section	.rodata._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_22CallRuntimeInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.str1.1
.LC73:
	.string	"tmp"
.LC74:
	.string	"    compiler::TNode<"
.LC75:
	.string	"> "
.LC76:
	.string	"Object"
.LC77:
	.string	"TORQUE_CAST("
	.section	.rodata._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_22CallRuntimeInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.str1.8
	.align 8
.LC78:
	.string	"CodeStubAssembler(state_).CallRuntime(Runtime::k"
	.section	.rodata._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_22CallRuntimeInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.str1.1
.LC79:
	.string	"; \n"
.LC80:
	.string	"    USE("
	.section	.rodata._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_22CallRuntimeInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.str1.8
	.align 8
.LC81:
	.string	"    CodeStubAssembler(state_).CallRuntime(Runtime::k"
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_22CallRuntimeInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE,"ax",@progbits
	.align 2
.LCOLDB82:
	.section	.text._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_22CallRuntimeInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE,"ax",@progbits
.LHOTB82:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_22CallRuntimeInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.type	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_22CallRuntimeInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE, @function
_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_22CallRuntimeInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE:
.LFB6945:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6945
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$360, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -320(%rbp)
	movq	40(%rsi), %r12
	movq	%rdx, -312(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -288(%rbp)
	movabsq	$288230376151711743, %rax
	movq	$0, -272(%rbp)
	cmpq	%rax, %r12
	ja	.L856
	movq	%r12, %rbx
	movq	%rsi, %r15
	salq	$5, %rbx
	testq	%r12, %r12
	jne	.L857
	movq	8(%rdx), %rax
	movq	%rax, -328(%rbp)
	subq	%rbx, %rax
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L682:
	movq	-312(%rbp), %rax
	movq	(%rax), %rcx
	movq	%rbx, %rax
	subq	%rcx, %rax
	sarq	$5, %rax
	movq	%rax, %rdx
	subq	%r12, %rdx
	jb	.L689
	cmpq	%rdx, %rax
	ja	.L858
.L691:
	movq	32(%r15), %rax
	movq	296(%rax), %rax
	movq	%rax, -328(%rbp)
.LEHB101:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_10TypeOracleEEERPNT_5ScopeEv@PLT
.LEHE101:
	leaq	-112(%rbp), %r14
	leaq	-128(%rbp), %rdi
	movb	$114, -108(%rbp)
	movq	%rdi, -360(%rbp)
	movq	%r14, -336(%rbp)
	movq	%r14, -128(%rbp)
	movl	$1702258030, -112(%rbp)
	movq	$5, -120(%rbp)
	movb	$0, -107(%rbp)
.LEHB102:
	call	_ZN2v88internal6torque12Declarations16LookupGlobalTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
.LEHE102:
	movq	-128(%rbp), %rdi
	movq	%rax, %rbx
	cmpq	%r14, %rdi
	je	.L699
	call	_ZdlPv@PLT
.L699:
	cmpq	%rbx, -328(%rbp)
	je	.L859
	leaq	-256(%rbp), %r12
	movq	-328(%rbp), %rsi
	movq	%r12, %rdi
.LEHB103:
	call	_ZN2v88internal6torque9LowerTypeEPKNS1_4TypeE@PLT
.LEHE103:
	movq	-256(%rbp), %rax
	movq	%rax, %rcx
	movq	%rax, -352(%rbp)
	movq	-248(%rbp), %rax
	subq	%rcx, %rax
	movq	%rax, -368(%rbp)
	cmpq	$15, %rax
	ja	.L860
.L701:
	cmpb	$0, 28(%r15)
	jne	.L861
	movq	-320(%rbp), %rcx
	movq	-360(%rbp), %rbx
	movl	$32, %edx
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	movq	16(%rcx), %r8
	movq	%rbx, %rdi
	leaq	1(%r8), %rax
	movq	%rax, 16(%rcx)
	leaq	.LC0(%rip), %rcx
	xorl	%eax, %eax
.LEHB104:
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0
.LEHE104:
	movl	$3, %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	leaq	.LC73(%rip), %rcx
.LEHB105:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.LEHE105:
	leaq	-208(%rbp), %rcx
	leaq	16(%rax), %rdx
	movq	%rcx, -392(%rbp)
	movq	%rcx, -224(%rbp)
	movq	(%rax), %rcx
	cmpq	%rdx, %rcx
	je	.L862
	movq	%rcx, -224(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -208(%rbp)
.L713:
	movq	8(%rax), %rcx
	movb	$0, 16(%rax)
	movq	%rcx, -216(%rbp)
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movq	-128(%rbp), %rdi
	cmpq	-336(%rbp), %rdi
	je	.L714
	call	_ZdlPv@PLT
.L714:
	cmpq	$8, -368(%rbp)
	je	.L863
.L716:
	leaq	-192(%rbp), %rax
	movl	48(%r15), %edx
	movq	56(%r15), %rcx
	movq	-320(%rbp), %rsi
	movq	%rax, %rdi
	movq	%rax, -400(%rbp)
.LEHB106:
	call	_ZN2v88internal6torque12CSAGenerator31PreCallableExceptionPreparationB5cxx11ENS_4base8OptionalIPNS1_5BlockEEE
.LEHE106:
	movq	-312(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	$0, -240(%rbp)
	movaps	%xmm0, -256(%rbp)
	movq	(%rax), %r14
	movq	8(%rax), %rax
	movq	%rax, %rbx
	movq	%rax, -344(%rbp)
	subq	%r14, %rbx
	movq	%rbx, %rax
	sarq	$5, %rax
	je	.L864
	movabsq	$288230376151711743, %rdx
	cmpq	%rdx, %rax
	ja	.L865
	movq	%rbx, %rdi
.LEHB107:
	call	_Znwm@PLT
.LEHE107:
	movq	%rax, -384(%rbp)
	movq	-312(%rbp), %rax
	movq	8(%rax), %rcx
	movq	(%rax), %r14
	movq	%rcx, -344(%rbp)
.L721:
	movq	-384(%rbp), %rax
	movq	%rax, %xmm0
	addq	%rax, %rbx
	punpcklqdq	%xmm0, %xmm0
	movq	%rbx, -240(%rbp)
	movq	%rax, %rbx
	movaps	%xmm0, -256(%rbp)
	cmpq	%r14, -344(%rbp)
	je	.L723
	leaq	-296(%rbp), %rax
	movq	%rax, -376(%rbp)
	jmp	.L729
	.p2align 4,,10
	.p2align 3
.L725:
	cmpq	$1, %r12
	jne	.L727
	movzbl	0(%r13), %eax
	movb	%al, 16(%rbx)
.L728:
	movq	%r12, 8(%rbx)
	addq	$32, %r14
	addq	$32, %rbx
	movb	$0, (%rdi,%r12)
	cmpq	%r14, -344(%rbp)
	je	.L723
.L729:
	leaq	16(%rbx), %rdi
	movq	%rdi, (%rbx)
	movq	(%r14), %r13
	movq	8(%r14), %r12
	movq	%r13, %rax
	addq	%r12, %rax
	je	.L724
	testq	%r13, %r13
	je	.L866
.L724:
	movq	%r12, -296(%rbp)
	cmpq	$15, %r12
	jbe	.L725
	movq	-376(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rbx, %rdi
.LEHB108:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE108:
	movq	%rax, (%rbx)
	movq	%rax, %rdi
	movq	-296(%rbp), %rax
	movq	%rax, 16(%rbx)
.L726:
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	-296(%rbp), %r12
	movq	(%rbx), %rdi
	jmp	.L728
	.p2align 4,,10
	.p2align 3
.L861:
	movq	-320(%rbp), %rbx
	movl	$56, %edx
	leaq	.LC72(%rip), %rsi
	movq	8(%rbx), %r12
	movq	%r12, %rdi
.LEHB109:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	32(%r15), %rax
	movq	%r12, %rdi
	movq	136(%rax), %rdx
	movq	128(%rax), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rax, %rdi
	movl	$2, %edx
	leaq	.LC27(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%rbx), %r12
	movq	-280(%rbp), %r13
	leaq	.LC27(%rip), %r14
	movq	-288(%rbp), %rbx
	cmpq	%r13, %rbx
	je	.L709
.L708:
	movq	8(%rbx), %rdx
	movq	(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$32, %rbx
	cmpq	%rbx, %r13
	je	.L867
	movl	$2, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L708
	.p2align 4,,10
	.p2align 3
.L727:
	testq	%r12, %r12
	je	.L728
	jmp	.L726
	.p2align 4,,10
	.p2align 3
.L867:
	movq	-320(%rbp), %rax
	movq	8(%rax), %r12
.L709:
	movl	$3, %edx
	leaq	.LC3(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE109:
.L711:
	movq	-352(%rbp), %rax
	testq	%rax, %rax
	je	.L772
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L772:
	movq	-280(%rbp), %rbx
	movq	-288(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L773
	.p2align 4,,10
	.p2align 3
.L777:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L774
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%rbx, %r12
	jne	.L777
.L775:
	movq	-288(%rbp), %r12
.L773:
	testq	%r12, %r12
	je	.L673
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L673:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L868
	addq	$360, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L774:
	.cfi_restore_state
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L777
	jmp	.L775
	.p2align 4,,10
	.p2align 3
.L723:
	cmpq	$8, -368(%rbp)
	movq	%rbx, -248(%rbp)
	je	.L869
	movq	-320(%rbp), %rbx
	movl	$52, %edx
	leaq	.LC81(%rip), %rsi
	leaq	-256(%rbp), %r12
	movq	8(%rbx), %r13
	movq	%r13, %rdi
.LEHB110:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	32(%r15), %rax
	movq	%r13, %rdi
	movq	136(%rax), %rdx
	movq	128(%rax), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rax, %rdi
	movl	$2, %edx
	leaq	.LC27(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%rbx), %r13
	movq	-280(%rbp), %rax
	movq	%r12, %r14
	movq	-288(%rbp), %rbx
	movq	%rax, -312(%rbp)
	cmpq	%rax, %rbx
	je	.L756
.L755:
	movq	8(%rbx), %rdx
	movq	(%rbx), %rsi
	movq	%r13, %rdi
	movq	%r14, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$32, %rbx
	cmpq	%rbx, -312(%rbp)
	je	.L870
	movl	$2, %edx
	leaq	.LC27(%rip), %rsi
	movq	%r13, %rdi
	movq	%r14, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L755
	.p2align 4,,10
	.p2align 3
.L859:
	movq	$0, -352(%rbp)
	movq	$0, -368(%rbp)
	jmp	.L701
	.p2align 4,,10
	.p2align 3
.L870:
	movq	-320(%rbp), %rax
	movq	8(%rax), %r13
.L756:
	movl	$3, %edx
	leaq	.LC3(%rip), %rsi
	movq	%r13, %rdi
	leaq	-256(%rbp), %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_10TypeOracleEEERPNT_5ScopeEv@PLT
.LEHE110:
	movq	-336(%rbp), %r14
	movq	-360(%rbp), %rdi
	movb	$114, -108(%rbp)
	movl	$1702258030, -112(%rbp)
	movq	%r14, -128(%rbp)
	movq	$5, -120(%rbp)
	movb	$0, -107(%rbp)
.LEHB111:
	call	_ZN2v88internal6torque12Declarations16LookupGlobalTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
.LEHE111:
	movq	-128(%rbp), %rdi
	movq	%rax, %rbx
	cmpq	%r14, %rdi
	je	.L758
	call	_ZdlPv@PLT
.L758:
	cmpq	%rbx, -328(%rbp)
	je	.L871
.L754:
	leaq	-256(%rbp), %r12
	movl	48(%r15), %ecx
	movq	56(%r15), %r8
	movq	-328(%rbp), %rdx
	movq	-400(%rbp), %rsi
	movq	%r12, %r9
	movq	-320(%rbp), %rdi
.LEHB112:
	call	_ZN2v88internal6torque12CSAGenerator32PostCallableExceptionPreparationERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKNS1_4TypeENS_4base8OptionalIPNS1_5BlockEEEPNS1_5StackIS8_EE
.LEHE112:
	movq	-248(%rbp), %rbx
	movq	-256(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L764
	.p2align 4,,10
	.p2align 3
.L768:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L765
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L768
.L766:
	movq	-256(%rbp), %r12
.L764:
	testq	%r12, %r12
	je	.L769
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L769:
	movq	-192(%rbp), %rdi
	leaq	-176(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L770
	call	_ZdlPv@PLT
.L770:
	movq	-224(%rbp), %rdi
	cmpq	-392(%rbp), %rdi
	je	.L711
	call	_ZdlPv@PLT
	jmp	.L711
	.p2align 4,,10
	.p2align 3
.L765:
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L768
	jmp	.L766
	.p2align 4,,10
	.p2align 3
.L857:
	movq	%rbx, %rdi
	leaq	-288(%rbp), %r14
.LEHB113:
	call	_Znwm@PLT
	movq	-288(%rbp), %r8
	movq	-280(%rbp), %rsi
	movq	%rax, %r14
	movq	%r8, %rdx
	cmpq	%r8, %rsi
	je	.L676
	.p2align 4,,10
	.p2align 3
.L680:
	leaq	16(%rax), %rcx
	leaq	16(%rdx), %rdi
	movq	%rcx, (%rax)
	movq	(%rdx), %rcx
	cmpq	%rdi, %rcx
	je	.L872
	movq	%rcx, (%rax)
	movq	16(%rdx), %rcx
	addq	$32, %rdx
	addq	$32, %rax
	movq	%rcx, -16(%rax)
	movq	-24(%rdx), %rcx
	movq	%rcx, -24(%rax)
	cmpq	%rdx, %rsi
	jne	.L680
.L676:
	testq	%r8, %r8
	je	.L681
	movq	%r8, %rdi
	call	_ZdlPv@PLT
.L681:
	movq	-312(%rbp), %rax
	movq	%r14, %xmm0
	leaq	(%r14,%rbx), %rsi
	punpcklqdq	%xmm0, %xmm0
	movq	%rsi, -272(%rbp)
	movq	8(%rax), %rax
	movaps	%xmm0, -288(%rbp)
	movq	%rax, %rcx
	subq	%rbx, %rcx
	movq	%rcx, %rbx
	cmpq	%rcx, %rax
	je	.L682
	leaq	-288(%rbp), %r13
	jmp	.L687
	.p2align 4,,10
	.p2align 3
.L684:
	movq	%rdx, (%r14)
	movq	16(%rbx), %rdx
	movq	%rdx, 16(%r14)
.L685:
	movq	8(%rbx), %rdx
	movq	%rdx, 8(%r14)
	movq	%rax, (%rbx)
	movq	$0, 8(%rbx)
	movb	$0, 16(%rbx)
	addq	$32, -280(%rbp)
.L686:
	movq	-312(%rbp), %rax
	addq	$32, %rbx
	cmpq	%rbx, 8(%rax)
	je	.L682
	movq	-280(%rbp), %r14
	movq	-272(%rbp), %rsi
.L687:
	cmpq	%r14, %rsi
	je	.L683
	leaq	16(%r14), %rax
	movq	%rax, (%r14)
	movq	(%rbx), %rdx
	leaq	16(%rbx), %rax
	cmpq	%rax, %rdx
	jne	.L684
	movdqu	16(%rbx), %xmm1
	movups	%xmm1, 16(%r14)
	jmp	.L685
	.p2align 4,,10
	.p2align 3
.L683:
	movq	%rbx, %rdx
	movq	%r13, %rdi
	movq	%r13, %r14
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
.LEHE113:
	jmp	.L686
	.p2align 4,,10
	.p2align 3
.L872:
	movdqu	16(%rdx), %xmm2
	addq	$32, %rdx
	addq	$32, %rax
	movups	%xmm2, -16(%rax)
	movq	-24(%rdx), %rcx
	movq	%rcx, -24(%rax)
	cmpq	%rsi, %rdx
	jne	.L680
	jmp	.L676
	.p2align 4,,10
	.p2align 3
.L858:
	salq	$5, %rdx
	leaq	(%rcx,%rdx), %r12
	cmpq	%rbx, %r12
	je	.L691
	movq	%r12, %r13
	.p2align 4,,10
	.p2align 3
.L696:
	movq	0(%r13), %rdi
	leaq	16(%r13), %rax
	cmpq	%rax, %rdi
	je	.L693
	call	_ZdlPv@PLT
	addq	$32, %r13
	cmpq	%rbx, %r13
	jne	.L696
.L694:
	movq	-312(%rbp), %rax
	movq	%r12, 8(%rax)
	jmp	.L691
	.p2align 4,,10
	.p2align 3
.L693:
	addq	$32, %r13
	cmpq	%rbx, %r13
	jne	.L696
	jmp	.L694
	.p2align 4,,10
	.p2align 3
.L863:
	movq	-320(%rbp), %rax
	movl	$20, %edx
	leaq	.LC74(%rip), %rsi
	movq	8(%rax), %r12
	movq	%r12, %rdi
.LEHB114:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-352(%rbp), %rax
	movq	-360(%rbp), %rdi
	movq	(%rax), %rsi
	call	_ZNK2v88internal6torque4Type25GetGeneratedTNodeTypeNameB5cxx11Ev@PLT
.LEHE114:
	movq	-120(%rbp), %rdx
	movq	-128(%rbp), %rsi
	movq	%r12, %rdi
.LEHB115:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$2, %edx
	leaq	.LC75(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-216(%rbp), %rdx
	movq	-224(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rax, %rdi
	movl	$2, %edx
	leaq	.LC40(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE115:
	movq	-128(%rbp), %rdi
	cmpq	-336(%rbp), %rdi
	je	.L716
	call	_ZdlPv@PLT
	jmp	.L716
	.p2align 4,,10
	.p2align 3
.L869:
	movq	-352(%rbp), %rax
	leaq	-160(%rbp), %r14
	leaq	-256(%rbp), %r12
	movq	%r14, %rdi
	movq	(%rax), %rsi
.LEHB116:
	call	_ZNK2v88internal6torque4Type25GetGeneratedTNodeTypeNameB5cxx11Ev@PLT
.LEHE116:
	movq	-336(%rbp), %rax
	movq	-224(%rbp), %r13
	movq	-216(%rbp), %r12
	movq	%rax, -128(%rbp)
	movq	%r13, %rax
	addq	%r12, %rax
	je	.L739
	testq	%r13, %r13
	je	.L873
.L739:
	movq	%r12, -296(%rbp)
	cmpq	$15, %r12
	ja	.L874
	cmpq	$1, %r12
	jne	.L742
	movzbl	0(%r13), %eax
	movb	%al, -112(%rbp)
	movq	-336(%rbp), %rax
.L743:
	movq	%r12, -120(%rbp)
	movb	$0, (%rax,%r12)
	movq	-312(%rbp), %rax
	movq	8(%rax), %rsi
	cmpq	16(%rax), %rsi
	je	.L744
	leaq	16(%rsi), %rax
	movq	%rax, (%rsi)
	movq	-128(%rbp), %rax
	cmpq	-336(%rbp), %rax
	je	.L875
	movq	%rax, (%rsi)
	movq	-112(%rbp), %rax
	movq	%rax, 16(%rsi)
.L746:
	movq	-120(%rbp), %rax
	movq	%rax, 8(%rsi)
	movq	-312(%rbp), %rax
	addq	$32, 8(%rax)
.L747:
	movq	-320(%rbp), %rbx
	movl	$4, %edx
	leaq	.LC43(%rip), %rsi
	movq	8(%rbx), %r12
	movq	%r12, %rdi
.LEHB117:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-216(%rbp), %rdx
	movq	-224(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rax, %rdi
	movl	$3, %edx
	leaq	.LC39(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	.LC76(%rip), %rsi
	movq	%r14, %rdi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	je	.L748
	movq	8(%rbx), %rdi
	movl	$12, %edx
	leaq	.LC77(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L748:
	movq	-320(%rbp), %rbx
	movl	$48, %edx
	leaq	.LC78(%rip), %rsi
	movq	8(%rbx), %r12
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	32(%r15), %rax
	movq	%r12, %rdi
	movq	136(%rax), %rdx
	movq	128(%rax), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rax, %rdi
	movl	$2, %edx
	leaq	.LC27(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%rbx), %r12
	movq	-280(%rbp), %r13
	movq	-288(%rbp), %rbx
	cmpq	%r13, %rbx
	je	.L750
.L749:
	movq	8(%rbx), %rdx
	movq	(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$32, %rbx
	cmpq	%rbx, %r13
	je	.L876
	movl	$2, %edx
	leaq	.LC27(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L749
	.p2align 4,,10
	.p2align 3
.L876:
	movq	-320(%rbp), %rax
	movq	8(%rax), %r12
.L750:
	movl	$1, %edx
	leaq	.LC57(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	.LC76(%rip), %rsi
	movq	%r14, %rdi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	je	.L752
	movq	-320(%rbp), %rax
	movl	$1, %edx
	leaq	.LC57(%rip), %rsi
	movq	8(%rax), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L752:
	movq	-320(%rbp), %rbx
	movl	$3, %edx
	leaq	.LC79(%rip), %rsi
	movq	8(%rbx), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%rbx), %r12
	movl	$8, %edx
	leaq	.LC80(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-216(%rbp), %rdx
	movq	-224(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rax, %rdi
	movl	$3, %edx
	leaq	.LC3(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE117:
	movq	-160(%rbp), %rdi
	leaq	-144(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L754
	call	_ZdlPv@PLT
	jmp	.L754
	.p2align 4,,10
	.p2align 3
.L862:
	movdqu	16(%rax), %xmm3
	movaps	%xmm3, -208(%rbp)
	jmp	.L713
	.p2align 4,,10
	.p2align 3
.L864:
	movq	$0, -384(%rbp)
	jmp	.L721
	.p2align 4,,10
	.p2align 3
.L871:
	movq	-320(%rbp), %rax
	movl	$45, %edx
	leaq	.LC50(%rip), %rsi
	leaq	-256(%rbp), %r12
	movq	8(%rax), %rdi
.LEHB118:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE118:
	jmp	.L754
	.p2align 4,,10
	.p2align 3
.L742:
	testq	%r12, %r12
	jne	.L877
	movq	-336(%rbp), %rax
	jmp	.L743
	.p2align 4,,10
	.p2align 3
.L874:
	movq	-360(%rbp), %rdi
	leaq	-296(%rbp), %rsi
	xorl	%edx, %edx
.LEHB119:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE119:
	movq	%rax, -128(%rbp)
	movq	%rax, %rdi
	movq	-296(%rbp), %rax
	movq	%rax, -112(%rbp)
.L741:
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	-296(%rbp), %r12
	movq	-128(%rbp), %rax
	jmp	.L743
	.p2align 4,,10
	.p2align 3
.L875:
	movdqa	-112(%rbp), %xmm4
	movups	%xmm4, 16(%rsi)
	jmp	.L746
	.p2align 4,,10
	.p2align 3
.L744:
	movq	-360(%rbp), %rdx
	movq	%rax, %rdi
.LEHB120:
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
.LEHE120:
	movq	-128(%rbp), %rdi
	cmpq	-336(%rbp), %rdi
	je	.L747
	call	_ZdlPv@PLT
	jmp	.L747
.L689:
	movq	%r12, %rsi
	movq	-312(%rbp), %rdi
	leaq	-288(%rbp), %r14
	negq	%rsi
.LEHB121:
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_default_appendEm
.LEHE121:
	jmp	.L691
.L866:
	leaq	.LC6(%rip), %rdi
.LEHB122:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE122:
.L868:
	call	__stack_chk_fail@PLT
.L856:
	leaq	.LC70(%rip), %rdi
	leaq	-288(%rbp), %r14
.LEHB123:
	call	_ZSt20__throw_length_errorPKc@PLT
.LEHE123:
.L860:
	movq	-360(%rbp), %rbx
	leaq	.LC71(%rip), %rsi
	movq	%rbx, %rdi
.LEHB124:
	call	_ZN2v88internal6torqueL7MessageIJRA38_KcEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
.LEHE124:
	movq	%rbx, %rdi
.LEHB125:
	call	_ZNK2v88internal6torque14MessageBuilder5ThrowEv@PLT
.LEHE125:
.L865:
.LEHB126:
	call	_ZSt17__throw_bad_allocv@PLT
.LEHE126:
.L873:
	leaq	.LC6(%rip), %rdi
.LEHB127:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE127:
.L877:
	movq	-336(%rbp), %rdi
	jmp	.L741
.L797:
	endbr64
	movq	%rax, %rbx
	jmp	.L784
.L802:
	endbr64
	movq	%rax, %r12
	jmp	.L705
.L798:
	endbr64
	movq	%rax, %rbx
	jmp	.L782
.L806:
	endbr64
	movq	%rax, %rbx
	jmp	.L761
.L808:
	endbr64
	movq	%rax, %r12
	jmp	.L704
.L800:
	endbr64
	movq	%rax, %r12
	jmp	.L702
.L807:
	endbr64
	movq	%rax, %r12
	jmp	.L704
.L793:
	endbr64
	movq	%rax, %r12
	jmp	.L706
.L801:
	endbr64
	movq	%rax, %r12
	jmp	.L698
.L803:
	endbr64
	movq	%rax, %r12
	jmp	.L717
.L796:
	endbr64
	movq	%rax, %r12
	jmp	.L738
.L795:
	endbr64
	movq	%rax, %r12
	jmp	.L779
.L799:
	endbr64
	movq	%rax, %rbx
	jmp	.L763
.L794:
	endbr64
	movq	%rax, %r12
	jmp	.L781
.L805:
	endbr64
	movq	%rax, %rdi
	jmp	.L732
	.section	.gcc_except_table._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_22CallRuntimeInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE,"a",@progbits
	.align 4
.LLSDA6945:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATT6945-.LLSDATTD6945
.LLSDATTD6945:
	.byte	0x1
	.uleb128 .LLSDACSE6945-.LLSDACSB6945
.LLSDACSB6945:
	.uleb128 .LEHB101-.LFB6945
	.uleb128 .LEHE101-.LEHB101
	.uleb128 .L807-.LFB6945
	.uleb128 0
	.uleb128 .LEHB102-.LFB6945
	.uleb128 .LEHE102-.LEHB102
	.uleb128 .L800-.LFB6945
	.uleb128 0
	.uleb128 .LEHB103-.LFB6945
	.uleb128 .LEHE103-.LEHB103
	.uleb128 .L808-.LFB6945
	.uleb128 0
	.uleb128 .LEHB104-.LFB6945
	.uleb128 .LEHE104-.LEHB104
	.uleb128 .L793-.LFB6945
	.uleb128 0
	.uleb128 .LEHB105-.LFB6945
	.uleb128 .LEHE105-.LEHB105
	.uleb128 .L803-.LFB6945
	.uleb128 0
	.uleb128 .LEHB106-.LFB6945
	.uleb128 .LEHE106-.LEHB106
	.uleb128 .L794-.LFB6945
	.uleb128 0
	.uleb128 .LEHB107-.LFB6945
	.uleb128 .LEHE107-.LEHB107
	.uleb128 .L796-.LFB6945
	.uleb128 0
	.uleb128 .LEHB108-.LFB6945
	.uleb128 .LEHE108-.LEHB108
	.uleb128 .L805-.LFB6945
	.uleb128 0x1
	.uleb128 .LEHB109-.LFB6945
	.uleb128 .LEHE109-.LEHB109
	.uleb128 .L793-.LFB6945
	.uleb128 0
	.uleb128 .LEHB110-.LFB6945
	.uleb128 .LEHE110-.LEHB110
	.uleb128 .L799-.LFB6945
	.uleb128 0
	.uleb128 .LEHB111-.LFB6945
	.uleb128 .LEHE111-.LEHB111
	.uleb128 .L806-.LFB6945
	.uleb128 0
	.uleb128 .LEHB112-.LFB6945
	.uleb128 .LEHE112-.LEHB112
	.uleb128 .L799-.LFB6945
	.uleb128 0
	.uleb128 .LEHB113-.LFB6945
	.uleb128 .LEHE113-.LEHB113
	.uleb128 .L801-.LFB6945
	.uleb128 0
	.uleb128 .LEHB114-.LFB6945
	.uleb128 .LEHE114-.LEHB114
	.uleb128 .L794-.LFB6945
	.uleb128 0
	.uleb128 .LEHB115-.LFB6945
	.uleb128 .LEHE115-.LEHB115
	.uleb128 .L795-.LFB6945
	.uleb128 0
	.uleb128 .LEHB116-.LFB6945
	.uleb128 .LEHE116-.LEHB116
	.uleb128 .L799-.LFB6945
	.uleb128 0
	.uleb128 .LEHB117-.LFB6945
	.uleb128 .LEHE117-.LEHB117
	.uleb128 .L797-.LFB6945
	.uleb128 0
	.uleb128 .LEHB118-.LFB6945
	.uleb128 .LEHE118-.LEHB118
	.uleb128 .L799-.LFB6945
	.uleb128 0
	.uleb128 .LEHB119-.LFB6945
	.uleb128 .LEHE119-.LEHB119
	.uleb128 .L797-.LFB6945
	.uleb128 0
	.uleb128 .LEHB120-.LFB6945
	.uleb128 .LEHE120-.LEHB120
	.uleb128 .L798-.LFB6945
	.uleb128 0
	.uleb128 .LEHB121-.LFB6945
	.uleb128 .LEHE121-.LEHB121
	.uleb128 .L801-.LFB6945
	.uleb128 0
	.uleb128 .LEHB122-.LFB6945
	.uleb128 .LEHE122-.LEHB122
	.uleb128 .L805-.LFB6945
	.uleb128 0x1
	.uleb128 .LEHB123-.LFB6945
	.uleb128 .LEHE123-.LEHB123
	.uleb128 .L801-.LFB6945
	.uleb128 0
	.uleb128 .LEHB124-.LFB6945
	.uleb128 .LEHE124-.LEHB124
	.uleb128 .L793-.LFB6945
	.uleb128 0
	.uleb128 .LEHB125-.LFB6945
	.uleb128 .LEHE125-.LEHB125
	.uleb128 .L802-.LFB6945
	.uleb128 0
	.uleb128 .LEHB126-.LFB6945
	.uleb128 .LEHE126-.LEHB126
	.uleb128 .L796-.LFB6945
	.uleb128 0
	.uleb128 .LEHB127-.LFB6945
	.uleb128 .LEHE127-.LEHB127
	.uleb128 .L797-.LFB6945
	.uleb128 0
.LLSDACSE6945:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATT6945:
	.section	.text._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_22CallRuntimeInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_22CallRuntimeInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6945
	.type	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_22CallRuntimeInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.cold, @function
_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_22CallRuntimeInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.cold:
.LFSB6945:
.L782:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-128(%rbp), %rdi
	cmpq	-336(%rbp), %rdi
	je	.L784
	call	_ZdlPv@PLT
.L784:
	movq	-160(%rbp), %rdi
	leaq	-144(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L785
	call	_ZdlPv@PLT
.L785:
	leaq	-256(%rbp), %r12
.L763:
	movq	%r12, %rdi
	movq	%rbx, %r12
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EED1Ev
.L738:
	movq	-192(%rbp), %rdi
	leaq	-176(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L781
	call	_ZdlPv@PLT
.L781:
	movq	-224(%rbp), %rdi
	cmpq	-392(%rbp), %rdi
	je	.L706
	call	_ZdlPv@PLT
	jmp	.L706
.L705:
	movq	-360(%rbp), %rdi
	call	_ZN2v88internal6torque14MessageBuilderD1Ev
.L706:
	cmpq	$0, -352(%rbp)
	je	.L704
	movq	-352(%rbp), %rdi
	call	_ZdlPv@PLT
.L704:
	leaq	-288(%rbp), %rdi
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EED1Ev
	movq	%r12, %rdi
.LEHB128:
	call	_Unwind_Resume@PLT
.L761:
	movq	-128(%rbp), %rdi
	cmpq	-336(%rbp), %rdi
	je	.L762
	call	_ZdlPv@PLT
.L762:
	leaq	-256(%rbp), %r12
	jmp	.L763
.L702:
	movq	-128(%rbp), %rdi
	cmpq	-336(%rbp), %rdi
	je	.L704
	call	_ZdlPv@PLT
	jmp	.L704
.L698:
	movq	%r14, %rdi
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EED1Ev
	movq	%r12, %rdi
	call	_Unwind_Resume@PLT
.LEHE128:
.L717:
	movq	-128(%rbp), %rdi
	cmpq	-336(%rbp), %rdi
	je	.L706
	call	_ZdlPv@PLT
	jmp	.L706
.L779:
	movq	-128(%rbp), %rdi
	cmpq	-336(%rbp), %rdi
	je	.L781
	call	_ZdlPv@PLT
	jmp	.L781
.L732:
	call	__cxa_begin_catch@PLT
	movq	-384(%rbp), %r12
.L735:
	cmpq	%rbx, %r12
	jne	.L878
.LEHB129:
	call	__cxa_rethrow@PLT
.LEHE129:
.L878:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L734
	call	_ZdlPv@PLT
.L734:
	addq	$32, %r12
	jmp	.L735
.L804:
	endbr64
	movq	%rax, %r12
	call	__cxa_end_catch@PLT
	movq	-256(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L738
	call	_ZdlPv@PLT
	jmp	.L738
	.cfi_endproc
.LFE6945:
	.section	.gcc_except_table._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_22CallRuntimeInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.align 4
.LLSDAC6945:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATTC6945-.LLSDATTDC6945
.LLSDATTDC6945:
	.byte	0x1
	.uleb128 .LLSDACSEC6945-.LLSDACSBC6945
.LLSDACSBC6945:
	.uleb128 .LEHB128-.LCOLDB82
	.uleb128 .LEHE128-.LEHB128
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB129-.LCOLDB82
	.uleb128 .LEHE129-.LEHB129
	.uleb128 .L804-.LCOLDB82
	.uleb128 0
.LLSDACSEC6945:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATTC6945:
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_22CallRuntimeInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.section	.text._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_22CallRuntimeInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.size	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_22CallRuntimeInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE, .-_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_22CallRuntimeInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_22CallRuntimeInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.size	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_22CallRuntimeInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.cold, .-_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_22CallRuntimeInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.cold
.LCOLDE82:
	.section	.text._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_22CallRuntimeInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
.LHOTE82:
	.section	.rodata._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_28NamespaceConstantInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.str1.1,"aMS",@progbits,1
.LC83:
	.string	"std::tie("
.LC84:
	.string	") = "
.LC85:
	.string	"(state_)"
.LC86:
	.string	".Flatten();\n"
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_28NamespaceConstantInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE,"ax",@progbits
	.align 2
.LCOLDB87:
	.section	.text._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_28NamespaceConstantInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE,"ax",@progbits
.LHOTB87:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_28NamespaceConstantInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.type	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_28NamespaceConstantInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE, @function
_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_28NamespaceConstantInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE:
.LFB6920:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6920
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	leaq	-160(%rbp), %rdi
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$232, %rsp
	movq	%rsi, -264(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rsi), %rax
	movaps	%xmm0, -192(%rbp)
	movq	$0, -176(%rbp)
	movq	72(%rax), %rsi
	movq	%rsi, -256(%rbp)
.LEHB130:
	call	_ZN2v88internal6torque9LowerTypeEPKNS1_4TypeE@PLT
.LEHE130:
	movq	-152(%rbp), %rcx
	movq	-160(%rbp), %rax
	movq	%rcx, -248(%rbp)
	cmpq	%rcx, %rax
	je	.L880
	movq	%rax, -216(%rbp)
	leaq	-112(%rbp), %rax
	leaq	-96(%rbp), %r14
	movq	%rax, -224(%rbp)
	jmp	.L905
	.p2align 4,,10
	.p2align 3
.L894:
	cmpq	$1, %r12
	jne	.L896
	movq	-240(%rbp), %rax
	movzbl	(%rax), %eax
	movb	%al, -80(%rbp)
	movq	%r13, %rax
.L897:
	movq	%r12, -88(%rbp)
	movb	$0, (%rax,%r12)
	movq	8(%rbx), %rsi
	cmpq	16(%rbx), %rsi
	je	.L898
	leaq	16(%rsi), %rax
	movq	%rax, (%rsi)
	movq	-96(%rbp), %rax
	cmpq	%r13, %rax
	je	.L957
	movq	%rax, (%rsi)
	movq	-80(%rbp), %rax
	movq	%rax, 16(%rsi)
.L900:
	movq	-88(%rbp), %rax
	movq	%rax, 8(%rsi)
	addq	$32, 8(%rbx)
.L901:
	movq	8(%r15), %r12
	movl	$20, %edx
	leaq	.LC74(%rip), %rsi
	movq	%r12, %rdi
.LEHB131:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-232(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZNK2v88internal6torque4Type25GetGeneratedTNodeTypeNameB5cxx11Ev@PLT
.LEHE131:
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r12, %rdi
.LEHB132:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$2, %edx
	leaq	.LC75(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rdx
	movq	8(%rbx), %rax
	subq	%rdx, %rax
	je	.L958
	leaq	-32(%rdx,%rax), %rax
	movq	%r12, %rdi
	movq	8(%rax), %rdx
	movq	(%rax), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rax, %rdi
	movl	$2, %edx
	leaq	.LC40(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE132:
	movq	-96(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L903
	call	_ZdlPv@PLT
.L903:
	movq	8(%r15), %r12
	movl	$8, %edx
	leaq	.LC80(%rip), %rsi
	movq	%r12, %rdi
.LEHB133:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rdx
	movq	8(%rbx), %rax
	subq	%rdx, %rax
	je	.L959
	leaq	-32(%rdx,%rax), %rax
	movq	%r12, %rdi
	movq	8(%rax), %rdx
	movq	(%rax), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rax, %rdi
	movl	$3, %edx
	leaq	.LC3(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$8, -216(%rbp)
	movq	-216(%rbp), %rax
	cmpq	%rax, -248(%rbp)
	je	.L960
.L905:
	movq	-216(%rbp), %rax
	movq	16(%r15), %r8
	movq	%r14, %rdi
	leaq	.LC0(%rip), %rcx
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	movl	$32, %edx
	movq	(%rax), %rax
	movq	%rax, -232(%rbp)
	leaq	1(%r8), %rax
	movq	%rax, 16(%r15)
	xorl	%eax, %eax
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0
.LEHE133:
	movl	$3, %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r14, %rdi
	leaq	.LC73(%rip), %rcx
.LEHB134:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.LEHE134:
	movq	-224(%rbp), %rcx
	leaq	16(%rax), %rdx
	movq	%rcx, -128(%rbp)
	movq	(%rax), %rcx
	cmpq	%rdx, %rcx
	je	.L961
	movq	%rcx, -128(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -112(%rbp)
.L882:
	movq	8(%rax), %rcx
	movb	$0, 16(%rax)
	leaq	-80(%rbp), %r13
	movq	%rcx, -120(%rbp)
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movq	-96(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L883
	call	_ZdlPv@PLT
.L883:
	movq	-184(%rbp), %rsi
	cmpq	-176(%rbp), %rsi
	je	.L962
	leaq	16(%rsi), %rax
	movq	%rax, (%rsi)
	movq	-128(%rbp), %rax
	cmpq	-224(%rbp), %rax
	je	.L963
	movq	%rax, (%rsi)
	movq	-112(%rbp), %rax
	movq	%rax, 16(%rsi)
.L890:
	movq	-120(%rbp), %rax
	movq	%rax, 8(%rsi)
	movq	-184(%rbp), %rax
	addq	$32, %rax
	movq	%rax, -184(%rbp)
.L891:
	movq	%r13, -96(%rbp)
	movq	-32(%rax), %rcx
	movq	-24(%rax), %r12
	movq	%rcx, %rax
	movq	%rcx, -240(%rbp)
	addq	%r12, %rax
	je	.L893
	testq	%rcx, %rcx
	je	.L964
.L893:
	movq	%r12, -200(%rbp)
	cmpq	$15, %r12
	jbe	.L894
	leaq	-200(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r14, %rdi
.LEHB135:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE135:
	movq	%rax, -96(%rbp)
	movq	%rax, %rdi
	movq	-200(%rbp), %rax
	movq	%rax, -80(%rbp)
.L895:
	movq	-240(%rbp), %rsi
	movq	%r12, %rdx
	call	memcpy@PLT
	movq	-200(%rbp), %r12
	movq	-96(%rbp), %rax
	jmp	.L897
	.p2align 4,,10
	.p2align 3
.L896:
	testq	%r12, %r12
	jne	.L965
	movq	%r13, %rax
	jmp	.L897
	.p2align 4,,10
	.p2align 3
.L961:
	movdqu	16(%rax), %xmm1
	movaps	%xmm1, -112(%rbp)
	jmp	.L882
	.p2align 4,,10
	.p2align 3
.L957:
	movdqa	-80(%rbp), %xmm3
	movups	%xmm3, 16(%rsi)
	jmp	.L900
	.p2align 4,,10
	.p2align 3
.L963:
	movdqa	-112(%rbp), %xmm2
	movups	%xmm2, 16(%rsi)
	jmp	.L890
	.p2align 4,,10
	.p2align 3
.L898:
	movq	%r14, %rdx
	movq	%rbx, %rdi
.LEHB136:
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
.LEHE136:
	movq	-96(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L901
	call	_ZdlPv@PLT
	jmp	.L901
	.p2align 4,,10
	.p2align 3
.L962:
	leaq	-192(%rbp), %r12
	leaq	-128(%rbp), %rdx
	movq	%r12, %rdi
.LEHB137:
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
.LEHE137:
	movq	-128(%rbp), %rdi
	cmpq	-224(%rbp), %rdi
	je	.L955
	call	_ZdlPv@PLT
.L955:
	movq	-184(%rbp), %rax
	jmp	.L891
	.p2align 4,,10
	.p2align 3
.L960:
	movq	-160(%rbp), %rax
	movq	%rax, -248(%rbp)
.L880:
	movq	-248(%rbp), %rax
	testq	%rax, %rax
	je	.L906
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L906:
	movq	8(%r15), %rdi
	movl	$4, %edx
	leaq	.LC43(%rip), %rsi
.LEHB138:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-256(%rbp), %rax
	movq	8(%r15), %rdi
	cmpl	$4, 8(%rax)
	je	.L966
	movq	-192(%rbp), %rcx
	movq	-184(%rbp), %rax
	subq	%rcx, %rax
	cmpq	$32, %rax
	je	.L967
.L911:
	movq	-264(%rbp), %rax
	movq	32(%rax), %rax
	movq	176(%rax), %rdx
	movq	168(%rax), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rax, %rdi
	movl	$8, %edx
	leaq	.LC85(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-256(%rbp), %rax
	movq	8(%r15), %rdi
	cmpl	$4, 8(%rax)
	je	.L968
	movl	$2, %edx
	leaq	.LC40(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L913:
	movq	-184(%rbp), %rbx
	movq	-192(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L914
	.p2align 4,,10
	.p2align 3
.L918:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L915
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L918
.L916:
	movq	-192(%rbp), %r12
.L914:
	testq	%r12, %r12
	je	.L879
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L879:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L969
	addq	$232, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L915:
	.cfi_restore_state
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L918
	jmp	.L916
	.p2align 4,,10
	.p2align 3
.L968:
	movl	$12, %edx
	leaq	.LC86(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L913
	.p2align 4,,10
	.p2align 3
.L966:
	movl	$9, %edx
	leaq	.LC83(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-192(%rbp), %rbx
	movq	-184(%rbp), %r13
	leaq	.LC27(%rip), %r14
	movq	8(%r15), %r12
	cmpq	%r13, %rbx
	je	.L909
.L908:
	movq	8(%rbx), %rdx
	movq	(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$32, %rbx
	cmpq	%rbx, %r13
	je	.L970
	movl	$2, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L908
	.p2align 4,,10
	.p2align 3
.L970:
	movq	8(%r15), %r12
.L909:
	movl	$4, %edx
	leaq	.LC84(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L956:
	movq	8(%r15), %rdi
	jmp	.L911
	.p2align 4,,10
	.p2align 3
.L967:
	movq	8(%rcx), %rdx
	movq	(%rcx), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rax, %rdi
	movl	$3, %edx
	leaq	.LC39(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE138:
	jmp	.L956
.L959:
	xorl	%edx, %edx
	movq	$-1, %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
.LEHB139:
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.LEHE139:
.L958:
	xorl	%edx, %edx
	movq	$-1, %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
.LEHB140:
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.LEHE140:
.L964:
	leaq	.LC6(%rip), %rdi
.LEHB141:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE141:
.L965:
	movq	%r13, %rdi
	jmp	.L895
.L969:
	call	__stack_chk_fail@PLT
.L932:
	endbr64
	movq	%rax, %rbx
	jmp	.L922
.L934:
	endbr64
	movq	%rax, %rbx
	leaq	-192(%rbp), %r12
	jmp	.L927
.L935:
	endbr64
	movq	%rax, %rbx
	jmp	.L886
.L931:
	endbr64
	movq	%rax, %rbx
	jmp	.L920
.L930:
	endbr64
	movq	%rax, %rbx
	leaq	-192(%rbp), %r12
	jmp	.L888
.L933:
	endbr64
	movq	%rax, %rbx
	jmp	.L924
	.section	.gcc_except_table._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_28NamespaceConstantInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE,"a",@progbits
.LLSDA6920:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6920-.LLSDACSB6920
.LLSDACSB6920:
	.uleb128 .LEHB130-.LFB6920
	.uleb128 .LEHE130-.LEHB130
	.uleb128 .L934-.LFB6920
	.uleb128 0
	.uleb128 .LEHB131-.LFB6920
	.uleb128 .LEHE131-.LEHB131
	.uleb128 .L930-.LFB6920
	.uleb128 0
	.uleb128 .LEHB132-.LFB6920
	.uleb128 .LEHE132-.LEHB132
	.uleb128 .L933-.LFB6920
	.uleb128 0
	.uleb128 .LEHB133-.LFB6920
	.uleb128 .LEHE133-.LEHB133
	.uleb128 .L930-.LFB6920
	.uleb128 0
	.uleb128 .LEHB134-.LFB6920
	.uleb128 .LEHE134-.LEHB134
	.uleb128 .L935-.LFB6920
	.uleb128 0
	.uleb128 .LEHB135-.LFB6920
	.uleb128 .LEHE135-.LEHB135
	.uleb128 .L930-.LFB6920
	.uleb128 0
	.uleb128 .LEHB136-.LFB6920
	.uleb128 .LEHE136-.LEHB136
	.uleb128 .L932-.LFB6920
	.uleb128 0
	.uleb128 .LEHB137-.LFB6920
	.uleb128 .LEHE137-.LEHB137
	.uleb128 .L931-.LFB6920
	.uleb128 0
	.uleb128 .LEHB138-.LFB6920
	.uleb128 .LEHE138-.LEHB138
	.uleb128 .L934-.LFB6920
	.uleb128 0
	.uleb128 .LEHB139-.LFB6920
	.uleb128 .LEHE139-.LEHB139
	.uleb128 .L930-.LFB6920
	.uleb128 0
	.uleb128 .LEHB140-.LFB6920
	.uleb128 .LEHE140-.LEHB140
	.uleb128 .L933-.LFB6920
	.uleb128 0
	.uleb128 .LEHB141-.LFB6920
	.uleb128 .LEHE141-.LEHB141
	.uleb128 .L930-.LFB6920
	.uleb128 0
.LLSDACSE6920:
	.section	.text._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_28NamespaceConstantInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_28NamespaceConstantInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6920
	.type	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_28NamespaceConstantInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.cold, @function
_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_28NamespaceConstantInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.cold:
.LFSB6920:
.L922:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-96(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L923
	call	_ZdlPv@PLT
.L923:
	leaq	-192(%rbp), %r12
.L888:
	movq	-160(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L927
	call	_ZdlPv@PLT
.L927:
	movq	%r12, %rdi
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EED1Ev
	movq	%rbx, %rdi
.LEHB142:
	call	_Unwind_Resume@PLT
.LEHE142:
.L886:
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L887
	call	_ZdlPv@PLT
.L887:
	leaq	-192(%rbp), %r12
	jmp	.L888
.L924:
	movq	-96(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L925
	call	_ZdlPv@PLT
.L925:
	leaq	-192(%rbp), %r12
	jmp	.L888
.L920:
	movq	-128(%rbp), %rdi
	cmpq	-224(%rbp), %rdi
	je	.L888
	call	_ZdlPv@PLT
	jmp	.L888
	.cfi_endproc
.LFE6920:
	.section	.gcc_except_table._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_28NamespaceConstantInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
.LLSDAC6920:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6920-.LLSDACSBC6920
.LLSDACSBC6920:
	.uleb128 .LEHB142-.LCOLDB87
	.uleb128 .LEHE142-.LEHB142
	.uleb128 0
	.uleb128 0
.LLSDACSEC6920:
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_28NamespaceConstantInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.section	.text._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_28NamespaceConstantInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.size	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_28NamespaceConstantInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE, .-_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_28NamespaceConstantInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_28NamespaceConstantInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.size	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_28NamespaceConstantInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.cold, .-_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_28NamespaceConstantInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.cold
.LCOLDE87:
	.section	.text._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_28NamespaceConstantInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
.LHOTE87:
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator22ProcessArgumentsCommonERKSt6vectorIPKNS1_4TypeESaIS6_EEPS3_INSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaISG_EESJ_PNS1_5StackISG_EE,"ax",@progbits
	.align 2
.LCOLDB91:
	.section	.text._ZN2v88internal6torque12CSAGenerator22ProcessArgumentsCommonERKSt6vectorIPKNS1_4TypeESaIS6_EEPS3_INSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaISG_EESJ_PNS1_5StackISG_EE,"ax",@progbits
.LHOTB91:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque12CSAGenerator22ProcessArgumentsCommonERKSt6vectorIPKNS1_4TypeESaIS6_EEPS3_INSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaISG_EESJ_PNS1_5StackISG_EE
	.type	_ZN2v88internal6torque12CSAGenerator22ProcessArgumentsCommonERKSt6vectorIPKNS1_4TypeESaIS6_EEPS3_INSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaISG_EESJ_PNS1_5StackISG_EE, @function
_ZN2v88internal6torque12CSAGenerator22ProcessArgumentsCommonERKSt6vectorIPKNS1_4TypeESaIS6_EEPS3_INSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaISG_EESJ_PNS1_5StackISG_EE:
.LFB6921:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6921
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$616, %rsp
	.cfi_offset 3, -56
	movq	%rcx, -592(%rbp)
	movq	.LC88(%rip), %xmm4
	movq	%rsi, -600(%rbp)
	movq	8(%rax), %rcx
	movhps	.LC89(%rip), %xmm4
	movq	%fs:40, %rsi
	movq	%rsi, -56(%rbp)
	xorl	%esi, %esi
	movq	%rcx, -568(%rbp)
	movaps	%xmm4, -640(%rbp)
	cmpq	(%rax), %rcx
	jne	.L972
	jmp	.L1030
	.p2align 4,,10
	.p2align 3
.L1064:
	movq	-592(%rbp), %rax
	movq	8(%r12), %rsi
	movq	8(%rax), %rdx
	cmpq	16(%r12), %rsi
	je	.L976
	leaq	16(%rsi), %rax
	movq	%rax, (%rsi)
	movq	-32(%rdx), %rcx
	leaq	-16(%rdx), %rax
	cmpq	%rax, %rcx
	je	.L1063
	movq	%rcx, (%rsi)
	movq	-16(%rdx), %rcx
	movq	%rcx, 16(%rsi)
.L978:
	movq	-24(%rdx), %rcx
	movq	%rax, -32(%rdx)
	movq	$0, -24(%rdx)
	movq	%rcx, 8(%rsi)
	movb	$0, -16(%rdx)
	addq	$32, 8(%r12)
.L979:
	movq	-592(%rbp), %rcx
	movq	8(%rcx), %rax
	leaq	-32(%rax), %rdx
	subq	$16, %rax
	movq	%rdx, 8(%rcx)
	movq	-16(%rax), %rdi
	cmpq	%rax, %rdi
	je	.L981
	call	_ZdlPv@PLT
.L981:
	movq	-600(%rbp), %rcx
	subq	$8, -568(%rbp)
	movq	-568(%rbp), %rax
	cmpq	%rax, (%rcx)
	je	.L1030
.L972:
	movq	-568(%rbp), %rax
	movq	-8(%rax), %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
.LEHB143:
	call	*32(%rax)
.LEHE143:
	testb	%al, %al
	jne	.L1064
	leaq	-320(%rbp), %rax
	leaq	-448(%rbp), %r15
	movq	%rax, %rdi
	movq	%r15, -624(%rbp)
	movq	%rax, -584(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rcx
	xorl	%eax, %eax
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	movw	%ax, -96(%rbp)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%rcx, -320(%rbp)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	$0, -104(%rbp)
	movq	%rcx, -448(%rbp)
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	-24(%rcx), %rax
	movq	%rdx, -448(%rbp,%rax)
	movq	-448(%rbp), %rax
	movq	$0, -440(%rbp)
	addq	-24(%rax), %r15
	movq	%r15, %rdi
.LEHB144:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE144:
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	leaq	-432(%rbp), %r14
	xorl	%esi, %esi
	movq	%rcx, -432(%rbp)
	movq	-24(%rcx), %rax
	movq	%rdx, -432(%rbp,%rax)
	movq	-432(%rbp), %rax
	movq	-24(%rax), %rcx
	addq	%r14, %rcx
	movq	%rcx, %rdi
.LEHB145:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE145:
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	pxor	%xmm0, %xmm0
	movdqa	-640(%rbp), %xmm2
	movq	%rcx, -448(%rbp)
	movq	-24(%rcx), %rax
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	leaq	80(%rcx), %rsi
	movq	%rdx, -448(%rbp,%rax)
	movq	%rcx, -448(%rbp)
	leaq	-368(%rbp), %rcx
	movq	%rcx, %rdi
	movq	%rsi, -320(%rbp)
	movq	%rcx, -616(%rbp)
	movaps	%xmm2, -432(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	-336(%rbp), %rsi
	movq	-584(%rbp), %rdi
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%rsi, -608(%rbp)
	movq	%rsi, -352(%rbp)
	leaq	-424(%rbp), %rsi
	movq	%rdx, -424(%rbp)
	movl	$24, -360(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
.LEHB146:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE146:
	movq	%rbx, %rdi
.LEHB147:
	call	_ZN2v88internal6torque16LoweredSlotCountEPKNS1_4TypeE@PLT
.LEHE147:
	movq	%rax, %rdi
	movq	%rax, -576(%rbp)
	movq	8(%r13), %rax
	movq	%r13, %rsi
	subq	0(%r13), %rax
	leaq	-528(%rbp), %r15
	movq	%rbx, -528(%rbp)
	sarq	$5, %rax
	movb	$0, -520(%rbp)
	movq	%rax, %rdx
	movb	$0, -512(%rbp)
	subq	%rdi, %rdx
	movq	%r15, %rdi
	movb	$1, -480(%rbp)
	movq	%rdx, -472(%rbp)
	movq	%r14, %rdx
	movq	%rax, -464(%rbp)
.LEHB148:
	call	_ZN2v88internal6torque12CSAGenerator12EmitCSAValueENS1_11VisitResultERKNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEERSo
.LEHE148:
	cmpb	$0, -520(%rbp)
	je	.L987
	movq	-512(%rbp), %rdi
	leaq	-496(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L987
	call	_ZdlPv@PLT
.L987:
	movq	-384(%rbp), %rax
	leaq	-512(%rbp), %rbx
	movq	$0, -520(%rbp)
	movq	%rbx, -528(%rbp)
	movb	$0, -512(%rbp)
	testq	%rax, %rax
	je	.L988
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	jbe	.L989
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%rax, %r8
.LEHB149:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L990:
	movq	8(%r12), %rsi
	cmpq	16(%r12), %rsi
	je	.L1065
.L991:
	leaq	16(%rsi), %rax
	movq	%rax, (%rsi)
	movq	-528(%rbp), %rax
	cmpq	%rbx, %rax
	je	.L1066
	movq	%rax, (%rsi)
	movq	-512(%rbp), %rax
	movq	%rax, 16(%rsi)
.L997:
	movq	-520(%rbp), %rax
	movq	%rax, 8(%rsi)
	addq	$32, 8(%r12)
.L998:
	movq	$0, -544(%rbp)
	pxor	%xmm0, %xmm0
	movabsq	$288230376151711743, %rax
	movaps	%xmm0, -560(%rbp)
	cmpq	%rax, -576(%rbp)
	ja	.L1067
	movq	-576(%rbp), %rax
	movq	%rax, %rbx
	salq	$5, %rbx
	testq	%rax, %rax
	jne	.L1068
	movq	8(%r13), %rax
	subq	%rbx, %rax
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L1007:
	movq	0(%r13), %rdx
	movq	%rbx, %rax
	subq	%rdx, %rax
	sarq	$5, %rax
	movq	%rax, %r14
	subq	-576(%rbp), %r14
	jb	.L1014
	cmpq	%r14, %rax
	ja	.L1069
.L1016:
	movq	-552(%rbp), %rbx
	movq	-560(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L1022
	.p2align 4,,10
	.p2align 3
.L1023:
	movq	(%r14), %rdi
	leaq	16(%r14), %rax
	cmpq	%rax, %rdi
	je	.L1025
	call	_ZdlPv@PLT
	addq	$32, %r14
	cmpq	%rbx, %r14
	jne	.L1023
.L1026:
	movq	-560(%rbp), %r14
.L1022:
	testq	%r14, %r14
	je	.L1028
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1028:
	movq	.LC88(%rip), %xmm0
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-352(%rbp), %rdi
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movhps	.LC90(%rip), %xmm0
	movq	%rax, -320(%rbp)
	movaps	%xmm0, -432(%rbp)
	cmpq	-608(%rbp), %rdi
	je	.L1029
	call	_ZdlPv@PLT
.L1029:
	movq	-616(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdi
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rdi, -448(%rbp,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdi
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rdi, -432(%rbp,%rax)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdi
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rdi, -448(%rbp,%rax)
	movq	-584(%rbp), %rdi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	$0, -440(%rbp)
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	jmp	.L981
	.p2align 4,,10
	.p2align 3
.L1025:
	addq	$32, %r14
	cmpq	%r14, %rbx
	jne	.L1023
	jmp	.L1026
	.p2align 4,,10
	.p2align 3
.L989:
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.LEHE149:
	movq	8(%r12), %rsi
	cmpq	16(%r12), %rsi
	jne	.L991
.L1065:
	movq	%r15, %rdx
	movq	%r12, %rdi
.LEHB150:
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
.LEHE150:
	movq	-528(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L998
	call	_ZdlPv@PLT
	jmp	.L998
	.p2align 4,,10
	.p2align 3
.L1066:
	movdqa	-512(%rbp), %xmm6
	movups	%xmm6, 16(%rsi)
	jmp	.L997
	.p2align 4,,10
	.p2align 3
.L1063:
	movdqu	-16(%rdx), %xmm5
	movups	%xmm5, 16(%rsi)
	jmp	.L978
	.p2align 4,,10
	.p2align 3
.L1030:
	movq	8(%r12), %rbx
	movq	(%r12), %r12
	cmpq	%rbx, %r12
	je	.L971
	subq	$32, %rbx
	cmpq	%r12, %rbx
	jbe	.L971
	.p2align 4,,10
	.p2align 3
.L1031:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	subq	$32, %rbx
	addq	$32, %r12
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE4swapERS4_@PLT
	cmpq	%rbx, %r12
	jb	.L1031
.L971:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1070
	addq	$616, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1068:
	.cfi_restore_state
	movq	%rbx, %rdi
	leaq	-560(%rbp), %r15
.LEHB151:
	call	_Znwm@PLT
	movq	-560(%rbp), %r9
	movq	-552(%rbp), %r8
	movq	%rax, %rsi
	movq	%r9, %rax
	cmpq	%r9, %r8
	je	.L1001
	movq	%rsi, %rdx
	.p2align 4,,10
	.p2align 3
.L1005:
	leaq	16(%rdx), %rcx
	leaq	16(%rax), %rdi
	movq	%rcx, (%rdx)
	movq	(%rax), %rcx
	cmpq	%rdi, %rcx
	je	.L1071
	movq	%rcx, (%rdx)
	movq	16(%rax), %rcx
	addq	$32, %rax
	addq	$32, %rdx
	movq	%rcx, -16(%rdx)
	movq	-24(%rax), %rcx
	movq	%rcx, -24(%rdx)
	cmpq	%rax, %r8
	jne	.L1005
.L1001:
	testq	%r9, %r9
	je	.L1006
	movq	%r9, %rdi
	movq	%rsi, -648(%rbp)
	call	_ZdlPv@PLT
	movq	-648(%rbp), %rsi
.L1006:
	movq	8(%r13), %rdx
	movq	%rsi, %xmm0
	leaq	(%rsi,%rbx), %rax
	punpcklqdq	%xmm0, %xmm0
	movq	%rax, -544(%rbp)
	movq	%rdx, %rcx
	movaps	%xmm0, -560(%rbp)
	subq	%rbx, %rcx
	movq	%rcx, %rbx
	cmpq	%rcx, %rdx
	je	.L1007
	leaq	-560(%rbp), %r14
	jmp	.L1012
	.p2align 4,,10
	.p2align 3
.L1009:
	movq	%rdx, (%rsi)
	movq	16(%rbx), %rdx
	movq	%rdx, 16(%rsi)
.L1010:
	movq	8(%rbx), %rdx
	movq	%rdx, 8(%rsi)
	movq	%rax, (%rbx)
	movq	$0, 8(%rbx)
	movb	$0, 16(%rbx)
	addq	$32, -552(%rbp)
.L1011:
	addq	$32, %rbx
	cmpq	%rbx, 8(%r13)
	je	.L1007
	movq	-552(%rbp), %rsi
	movq	-544(%rbp), %rax
.L1012:
	cmpq	%rax, %rsi
	je	.L1008
	leaq	16(%rsi), %rax
	movq	%rax, (%rsi)
	movq	(%rbx), %rdx
	leaq	16(%rbx), %rax
	cmpq	%rax, %rdx
	jne	.L1009
	movdqu	16(%rbx), %xmm1
	movups	%xmm1, 16(%rsi)
	jmp	.L1010
	.p2align 4,,10
	.p2align 3
.L1008:
	movq	%rbx, %rdx
	movq	%r14, %rdi
	movq	%r14, %r15
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
.LEHE151:
	jmp	.L1011
	.p2align 4,,10
	.p2align 3
.L1071:
	movdqu	16(%rax), %xmm3
	addq	$32, %rax
	addq	$32, %rdx
	movups	%xmm3, -16(%rdx)
	movq	-24(%rax), %rcx
	movq	%rcx, -24(%rdx)
	cmpq	%r8, %rax
	jne	.L1005
	jmp	.L1001
	.p2align 4,,10
	.p2align 3
.L1069:
	salq	$5, %r14
	addq	%rdx, %r14
	cmpq	%r14, %rbx
	je	.L1016
	movq	%r14, %r15
	.p2align 4,,10
	.p2align 3
.L1021:
	movq	(%r15), %rdi
	leaq	16(%r15), %rax
	cmpq	%rax, %rdi
	je	.L1018
	call	_ZdlPv@PLT
	addq	$32, %r15
	cmpq	%rbx, %r15
	jne	.L1021
	movq	%r14, 8(%r13)
	jmp	.L1016
	.p2align 4,,10
	.p2align 3
.L1018:
	addq	$32, %r15
	cmpq	%r15, %rbx
	jne	.L1021
	movq	%r14, 8(%r13)
	jmp	.L1016
	.p2align 4,,10
	.p2align 3
.L988:
	leaq	-352(%rbp), %rsi
	movq	%r15, %rdi
.LEHB152:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
.LEHE152:
	jmp	.L990
	.p2align 4,,10
	.p2align 3
.L976:
	subq	$32, %rdx
	movq	%r12, %rdi
.LEHB153:
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
.LEHE153:
	jmp	.L979
.L1067:
	leaq	.LC70(%rip), %rdi
	leaq	-560(%rbp), %r15
.LEHB154:
	call	_ZSt20__throw_length_errorPKc@PLT
.L1014:
	movq	-576(%rbp), %rsi
	movq	%r13, %rdi
	leaq	-560(%rbp), %r15
	negq	%rsi
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_default_appendEm
.LEHE154:
	jmp	.L1016
.L1070:
	call	__stack_chk_fail@PLT
.L1039:
	endbr64
	movq	%rax, %r12
	jmp	.L1034
.L1044:
	endbr64
	movq	%rax, %r12
	jmp	.L1024
.L1042:
	endbr64
	movq	%rax, %rbx
	jmp	.L983
.L1040:
	endbr64
	movq	%rax, %rbx
	jmp	.L984
.L1037:
	endbr64
	movq	%rax, %r12
	jmp	.L995
.L1041:
	endbr64
	movq	%rax, %rbx
	jmp	.L985
.L1043:
	endbr64
	movq	%rax, %r12
	jmp	.L1034
.L1038:
	endbr64
	movq	%rax, %r12
	jmp	.L1032
	.section	.gcc_except_table._ZN2v88internal6torque12CSAGenerator22ProcessArgumentsCommonERKSt6vectorIPKNS1_4TypeESaIS6_EEPS3_INSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaISG_EESJ_PNS1_5StackISG_EE,"a",@progbits
.LLSDA6921:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6921-.LLSDACSB6921
.LLSDACSB6921:
	.uleb128 .LEHB143-.LFB6921
	.uleb128 .LEHE143-.LEHB143
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB144-.LFB6921
	.uleb128 .LEHE144-.LEHB144
	.uleb128 .L1040-.LFB6921
	.uleb128 0
	.uleb128 .LEHB145-.LFB6921
	.uleb128 .LEHE145-.LEHB145
	.uleb128 .L1042-.LFB6921
	.uleb128 0
	.uleb128 .LEHB146-.LFB6921
	.uleb128 .LEHE146-.LEHB146
	.uleb128 .L1041-.LFB6921
	.uleb128 0
	.uleb128 .LEHB147-.LFB6921
	.uleb128 .LEHE147-.LEHB147
	.uleb128 .L1037-.LFB6921
	.uleb128 0
	.uleb128 .LEHB148-.LFB6921
	.uleb128 .LEHE148-.LEHB148
	.uleb128 .L1038-.LFB6921
	.uleb128 0
	.uleb128 .LEHB149-.LFB6921
	.uleb128 .LEHE149-.LEHB149
	.uleb128 .L1043-.LFB6921
	.uleb128 0
	.uleb128 .LEHB150-.LFB6921
	.uleb128 .LEHE150-.LEHB150
	.uleb128 .L1039-.LFB6921
	.uleb128 0
	.uleb128 .LEHB151-.LFB6921
	.uleb128 .LEHE151-.LEHB151
	.uleb128 .L1044-.LFB6921
	.uleb128 0
	.uleb128 .LEHB152-.LFB6921
	.uleb128 .LEHE152-.LEHB152
	.uleb128 .L1043-.LFB6921
	.uleb128 0
	.uleb128 .LEHB153-.LFB6921
	.uleb128 .LEHE153-.LEHB153
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB154-.LFB6921
	.uleb128 .LEHE154-.LEHB154
	.uleb128 .L1044-.LFB6921
	.uleb128 0
.LLSDACSE6921:
	.section	.text._ZN2v88internal6torque12CSAGenerator22ProcessArgumentsCommonERKSt6vectorIPKNS1_4TypeESaIS6_EEPS3_INSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaISG_EESJ_PNS1_5StackISG_EE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator22ProcessArgumentsCommonERKSt6vectorIPKNS1_4TypeESaIS6_EEPS3_INSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaISG_EESJ_PNS1_5StackISG_EE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6921
	.type	_ZN2v88internal6torque12CSAGenerator22ProcessArgumentsCommonERKSt6vectorIPKNS1_4TypeESaIS6_EEPS3_INSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaISG_EESJ_PNS1_5StackISG_EE.cold, @function
_ZN2v88internal6torque12CSAGenerator22ProcessArgumentsCommonERKSt6vectorIPKNS1_4TypeESaIS6_EEPS3_INSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaISG_EESJ_PNS1_5StackISG_EE.cold:
.LFSB6921:
.L1034:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-528(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L995
.L1061:
	call	_ZdlPv@PLT
	jmp	.L995
.L1024:
	movq	%r15, %rdi
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EED1Ev
.L995:
	movq	-624(%rbp), %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r12, %rdi
.LEHB155:
	call	_Unwind_Resume@PLT
.L983:
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rsi
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rsi, -448(%rbp,%rax)
	movq	$0, -440(%rbp)
.L984:
	movq	-584(%rbp), %rdi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	%rbx, %rdi
	call	_Unwind_Resume@PLT
.LEHE155:
.L985:
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-352(%rbp), %rdi
	movq	%rax, -424(%rbp)
	cmpq	-608(%rbp), %rdi
	je	.L986
	call	_ZdlPv@PLT
.L986:
	movq	-616(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rsi
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rsi, -448(%rbp,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rsi
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rsi, -432(%rbp,%rax)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rsi
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rsi, -448(%rbp,%rax)
	movq	$0, -440(%rbp)
	jmp	.L984
.L1032:
	cmpb	$0, -520(%rbp)
	je	.L995
	movq	-512(%rbp), %rdi
	leaq	-496(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L1061
	jmp	.L995
	.cfi_endproc
.LFE6921:
	.section	.gcc_except_table._ZN2v88internal6torque12CSAGenerator22ProcessArgumentsCommonERKSt6vectorIPKNS1_4TypeESaIS6_EEPS3_INSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaISG_EESJ_PNS1_5StackISG_EE
.LLSDAC6921:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6921-.LLSDACSBC6921
.LLSDACSBC6921:
	.uleb128 .LEHB155-.LCOLDB91
	.uleb128 .LEHE155-.LEHB155
	.uleb128 0
	.uleb128 0
.LLSDACSEC6921:
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator22ProcessArgumentsCommonERKSt6vectorIPKNS1_4TypeESaIS6_EEPS3_INSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaISG_EESJ_PNS1_5StackISG_EE
	.section	.text._ZN2v88internal6torque12CSAGenerator22ProcessArgumentsCommonERKSt6vectorIPKNS1_4TypeESaIS6_EEPS3_INSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaISG_EESJ_PNS1_5StackISG_EE
	.size	_ZN2v88internal6torque12CSAGenerator22ProcessArgumentsCommonERKSt6vectorIPKNS1_4TypeESaIS6_EEPS3_INSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaISG_EESJ_PNS1_5StackISG_EE, .-_ZN2v88internal6torque12CSAGenerator22ProcessArgumentsCommonERKSt6vectorIPKNS1_4TypeESaIS6_EEPS3_INSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaISG_EESJ_PNS1_5StackISG_EE
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator22ProcessArgumentsCommonERKSt6vectorIPKNS1_4TypeESaIS6_EEPS3_INSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaISG_EESJ_PNS1_5StackISG_EE
	.size	_ZN2v88internal6torque12CSAGenerator22ProcessArgumentsCommonERKSt6vectorIPKNS1_4TypeESaIS6_EEPS3_INSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaISG_EESJ_PNS1_5StackISG_EE.cold, .-_ZN2v88internal6torque12CSAGenerator22ProcessArgumentsCommonERKSt6vectorIPKNS1_4TypeESaIS6_EEPS3_INSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaISG_EESJ_PNS1_5StackISG_EE.cold
.LCOLDE91:
	.section	.text._ZN2v88internal6torque12CSAGenerator22ProcessArgumentsCommonERKSt6vectorIPKNS1_4TypeESaIS6_EEPS3_INSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaISG_EESJ_PNS1_5StackISG_EE
.LHOTE91:
	.section	.rodata._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_23CallCsaMacroInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.str1.1,"aMS",@progbits,1
.LC92:
	.string	"(state_)."
.LC93:
	.string	"("
.LC94:
	.string	").Flatten();\n"
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_23CallCsaMacroInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE,"ax",@progbits
	.align 2
.LCOLDB95:
	.section	.text._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_23CallCsaMacroInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE,"ax",@progbits
.LHOTB95:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_23CallCsaMacroInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.type	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_23CallCsaMacroInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE, @function
_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_23CallCsaMacroInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE:
.LFB6929:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6929
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$376, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	48(%rsi), %rax
	movq	40(%rsi), %r13
	movq	%rdi, -352(%rbp)
	movq	%rsi, -384(%rbp)
	movq	%rax, %rbx
	subq	%r13, %rbx
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	movq	%rax, -344(%rbp)
	movq	%rbx, %rax
	sarq	$5, %rax
	movaps	%xmm0, -320(%rbp)
	movq	$0, -304(%rbp)
	je	.L1275
	movabsq	$288230376151711743, %rdx
	cmpq	%rdx, %rax
	ja	.L1276
	movq	%rbx, %rdi
.LEHB156:
	call	_Znwm@PLT
.LEHE156:
	movq	%rax, -368(%rbp)
	movq	-384(%rbp), %rax
	movq	48(%rax), %rcx
	movq	40(%rax), %r13
	movq	%rcx, -344(%rbp)
.L1074:
	movq	-368(%rbp), %rax
	movq	%rax, %xmm0
	addq	%rax, %rbx
	punpcklqdq	%xmm0, %xmm0
	movq	%rbx, -304(%rbp)
	movq	%rax, %rbx
	movaps	%xmm0, -320(%rbp)
	cmpq	-344(%rbp), %r13
	je	.L1076
	leaq	-160(%rbp), %rax
	movq	%rax, -360(%rbp)
	jmp	.L1082
	.p2align 4,,10
	.p2align 3
.L1078:
	cmpq	$1, %r12
	jne	.L1080
	movzbl	(%r14), %eax
	movb	%al, 16(%rbx)
.L1081:
	movq	%r12, 8(%rbx)
	addq	$32, %r13
	addq	$32, %rbx
	movb	$0, (%rdi,%r12)
	cmpq	%r13, -344(%rbp)
	je	.L1076
.L1082:
	leaq	16(%rbx), %rdi
	movq	%rdi, (%rbx)
	movq	0(%r13), %r14
	movq	8(%r13), %r12
	movq	%r14, %rax
	addq	%r12, %rax
	je	.L1077
	testq	%r14, %r14
	je	.L1277
.L1077:
	movq	%r12, -160(%rbp)
	cmpq	$15, %r12
	jbe	.L1078
	movq	-360(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rbx, %rdi
.LEHB157:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE157:
	movq	%rax, (%rbx)
	movq	%rax, %rdi
	movq	-160(%rbp), %rax
	movq	%rax, 16(%rbx)
.L1079:
	movq	%r12, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-160(%rbp), %r12
	movq	(%rbx), %rdi
	jmp	.L1081
	.p2align 4,,10
	.p2align 3
.L1080:
	testq	%r12, %r12
	je	.L1081
	jmp	.L1079
	.p2align 4,,10
	.p2align 3
.L1076:
	movq	-384(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	%rbx, -312(%rbp)
	movq	$0, -272(%rbp)
	movq	32(%rax), %r12
	movaps	%xmm0, -288(%rbp)
	movq	$0, -240(%rbp)
	movq	264(%r12), %rax
	movq	256(%r12), %rsi
	movaps	%xmm0, -256(%rbp)
	movq	%rax, %rbx
	subq	%rsi, %rbx
	movq	%rbx, %rdx
	sarq	$3, %rdx
	je	.L1278
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L1279
	movq	%rbx, %rdi
.LEHB158:
	call	_Znwm@PLT
.LEHE158:
	movq	%rax, %rcx
	movq	264(%r12), %rax
	movq	256(%r12), %rsi
	movq	%rax, %r12
	subq	%rsi, %r12
.L1084:
	movq	%rcx, %xmm0
	addq	%rcx, %rbx
	punpcklqdq	%xmm0, %xmm0
	movq	%rbx, -240(%rbp)
	movaps	%xmm0, -256(%rbp)
	cmpq	%rax, %rsi
	je	.L1092
	movq	%rcx, %rdi
	movq	%r12, %rdx
	call	memmove@PLT
	movq	%rax, %rcx
.L1092:
	addq	%r12, %rcx
	leaq	-288(%rbp), %rax
	movq	%r15, %r8
	movq	-352(%rbp), %rdi
	movq	%rcx, -248(%rbp)
	leaq	-256(%rbp), %rsi
	leaq	-320(%rbp), %rcx
	movq	%rax, %rdx
	movq	%rcx, -408(%rbp)
	movq	%rax, -400(%rbp)
.LEHB159:
	call	_ZN2v88internal6torque12CSAGenerator22ProcessArgumentsCommonERKSt6vectorIPKNS1_4TypeESaIS6_EEPS3_INSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaISG_EESJ_PNS1_5StackISG_EE
	movq	8(%r15), %rax
	movq	(%r15), %r13
	pxor	%xmm0, %xmm0
	movq	$0, -208(%rbp)
	movaps	%xmm0, -224(%rbp)
	movq	%rax, %rbx
	movq	%rax, -344(%rbp)
	subq	%r13, %rbx
	movq	%rbx, %rax
	sarq	$5, %rax
	je	.L1280
	movabsq	$288230376151711743, %rdx
	cmpq	%rdx, %rax
	ja	.L1281
	movq	%rbx, %rdi
	call	_Znwm@PLT
.LEHE159:
	movq	%rax, -368(%rbp)
	movq	8(%r15), %rax
	movq	(%r15), %r13
	movq	%rax, -344(%rbp)
.L1094:
	movq	-368(%rbp), %rax
	movq	%rax, %xmm0
	addq	%rax, %rbx
	punpcklqdq	%xmm0, %xmm0
	movq	%rbx, -208(%rbp)
	movq	%rax, %rbx
	movaps	%xmm0, -224(%rbp)
	cmpq	%r13, -344(%rbp)
	je	.L1096
	leaq	-160(%rbp), %rax
	movq	%rax, -360(%rbp)
	jmp	.L1102
	.p2align 4,,10
	.p2align 3
.L1098:
	cmpq	$1, %r12
	jne	.L1100
	movzbl	(%r14), %eax
	movb	%al, 16(%rbx)
.L1101:
	movq	%r12, 8(%rbx)
	addq	$32, %r13
	addq	$32, %rbx
	movb	$0, (%rdi,%r12)
	cmpq	%r13, -344(%rbp)
	je	.L1096
.L1102:
	leaq	16(%rbx), %rdi
	movq	%rdi, (%rbx)
	movq	0(%r13), %r14
	movq	8(%r13), %r12
	movq	%r14, %rax
	addq	%r12, %rax
	je	.L1097
	testq	%r14, %r14
	je	.L1282
.L1097:
	movq	%r12, -160(%rbp)
	cmpq	$15, %r12
	jbe	.L1098
	movq	-360(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rbx, %rdi
.LEHB160:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE160:
	movq	%rax, (%rbx)
	movq	%rax, %rdi
	movq	-160(%rbp), %rax
	movq	%rax, 16(%rbx)
.L1099:
	movq	%r12, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-160(%rbp), %r12
	movq	(%rbx), %rdi
	jmp	.L1101
	.p2align 4,,10
	.p2align 3
.L1100:
	testq	%r12, %r12
	je	.L1101
	jmp	.L1099
	.p2align 4,,10
	.p2align 3
.L1096:
	movq	-384(%rbp), %rax
	pxor	%xmm0, %xmm0
	leaq	-160(%rbp), %rdi
	movq	%rbx, -216(%rbp)
	movq	$0, -176(%rbp)
	movq	32(%rax), %rax
	movaps	%xmm0, -192(%rbp)
	movq	296(%rax), %rsi
	movq	%rsi, -392(%rbp)
.LEHB161:
	call	_ZN2v88internal6torque9LowerTypeEPKNS1_4TypeE@PLT
.LEHE161:
	movq	-152(%rbp), %rcx
	movq	-160(%rbp), %rax
	movq	%rcx, -376(%rbp)
	cmpq	%rcx, %rax
	je	.L1111
	movq	%rax, -344(%rbp)
	leaq	-112(%rbp), %rax
	leaq	-96(%rbp), %r13
	movq	%rax, -360(%rbp)
	jmp	.L1136
	.p2align 4,,10
	.p2align 3
.L1292:
	movzbl	(%r14), %eax
	movb	%al, -80(%rbp)
	movq	%rbx, %rax
.L1128:
	movq	%r12, -88(%rbp)
	movb	$0, (%rax,%r12)
	movq	8(%r15), %rsi
	cmpq	16(%r15), %rsi
	je	.L1129
	leaq	16(%rsi), %rax
	movq	%rax, (%rsi)
	movq	-96(%rbp), %rax
	cmpq	%rbx, %rax
	je	.L1283
	movq	%rax, (%rsi)
	movq	-80(%rbp), %rax
	movq	%rax, 16(%rsi)
.L1131:
	movq	-88(%rbp), %rax
	movq	%rax, 8(%rsi)
	addq	$32, 8(%r15)
.L1132:
	movq	-352(%rbp), %rax
	movl	$20, %edx
	leaq	.LC74(%rip), %rsi
	movq	8(%rax), %r12
	movq	%r12, %rdi
.LEHB162:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-368(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNK2v88internal6torque4Type25GetGeneratedTNodeTypeNameB5cxx11Ev@PLT
.LEHE162:
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r12, %rdi
.LEHB163:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$2, %edx
	leaq	.LC75(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r15), %rdx
	movq	8(%r15), %rax
	subq	%rdx, %rax
	je	.L1284
	leaq	-32(%rdx,%rax), %rax
	movq	%r12, %rdi
	movq	8(%rax), %rdx
	movq	(%rax), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rax, %rdi
	movl	$2, %edx
	leaq	.LC40(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE163:
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1134
	call	_ZdlPv@PLT
.L1134:
	movq	-352(%rbp), %rax
	movl	$8, %edx
	leaq	.LC80(%rip), %rsi
	movq	8(%rax), %r12
	movq	%r12, %rdi
.LEHB164:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r15), %rdx
	movq	8(%r15), %rax
	subq	%rdx, %rax
	je	.L1285
	leaq	-32(%rdx,%rax), %rax
	movq	%r12, %rdi
	movq	8(%rax), %rdx
	movq	(%rax), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rax, %rdi
	movl	$3, %edx
	leaq	.LC3(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$8, -344(%rbp)
	movq	-344(%rbp), %rax
	cmpq	%rax, -376(%rbp)
	je	.L1286
.L1136:
	movq	-352(%rbp), %rcx
	movq	-344(%rbp), %rax
	movl	$32, %edx
	movq	%r13, %rdi
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	movq	(%rax), %rax
	movq	16(%rcx), %r8
	movq	%rax, -368(%rbp)
	leaq	1(%r8), %rax
	movq	%rax, 16(%rcx)
	leaq	.LC0(%rip), %rcx
	xorl	%eax, %eax
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0
.LEHE164:
	movl	$3, %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r13, %rdi
	leaq	.LC73(%rip), %rcx
.LEHB165:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.LEHE165:
	movq	-360(%rbp), %rcx
	leaq	16(%rax), %rdx
	movq	%rcx, -128(%rbp)
	movq	(%rax), %rcx
	cmpq	%rdx, %rcx
	je	.L1287
	movq	%rcx, -128(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -112(%rbp)
.L1113:
	movq	8(%rax), %rcx
	leaq	-80(%rbp), %rbx
	movq	%rcx, -120(%rbp)
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movb	$0, 16(%rax)
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1114
	call	_ZdlPv@PLT
.L1114:
	movq	-184(%rbp), %rsi
	cmpq	-176(%rbp), %rsi
	je	.L1288
	leaq	16(%rsi), %rax
	movq	%rax, (%rsi)
	movq	-128(%rbp), %rax
	cmpq	-360(%rbp), %rax
	je	.L1289
	movq	%rax, (%rsi)
	movq	-112(%rbp), %rax
	movq	%rax, 16(%rsi)
.L1121:
	movq	-120(%rbp), %rax
	movq	%rax, 8(%rsi)
	movq	-184(%rbp), %rax
	addq	$32, %rax
	movq	%rax, -184(%rbp)
.L1122:
	movq	%rbx, -96(%rbp)
	movq	-32(%rax), %r14
	movq	-24(%rax), %r12
	movq	%r14, %rax
	addq	%r12, %rax
	je	.L1124
	testq	%r14, %r14
	je	.L1290
.L1124:
	movq	%r12, -328(%rbp)
	cmpq	$15, %r12
	ja	.L1291
	cmpq	$1, %r12
	je	.L1292
	testq	%r12, %r12
	jne	.L1293
	movq	%rbx, %rax
	jmp	.L1128
	.p2align 4,,10
	.p2align 3
.L1291:
	leaq	-328(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r13, %rdi
.LEHB166:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE166:
	movq	%rax, -96(%rbp)
	movq	%rax, %rdi
	movq	-328(%rbp), %rax
	movq	%rax, -80(%rbp)
.L1126:
	movq	%r12, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-328(%rbp), %r12
	movq	-96(%rbp), %rax
	jmp	.L1128
	.p2align 4,,10
	.p2align 3
.L1287:
	movdqu	16(%rax), %xmm1
	movaps	%xmm1, -112(%rbp)
	jmp	.L1113
	.p2align 4,,10
	.p2align 3
.L1283:
	movdqa	-80(%rbp), %xmm3
	movups	%xmm3, 16(%rsi)
	jmp	.L1131
	.p2align 4,,10
	.p2align 3
.L1289:
	movdqa	-112(%rbp), %xmm2
	movups	%xmm2, 16(%rsi)
	jmp	.L1121
	.p2align 4,,10
	.p2align 3
.L1288:
	leaq	-192(%rbp), %r14
	leaq	-128(%rbp), %rdx
	movq	%r14, %rdi
.LEHB167:
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
.LEHE167:
	movq	-128(%rbp), %rdi
	cmpq	-360(%rbp), %rdi
	je	.L1274
	call	_ZdlPv@PLT
.L1274:
	movq	-184(%rbp), %rax
	jmp	.L1122
	.p2align 4,,10
	.p2align 3
.L1129:
	movq	%r13, %rdx
	movq	%r15, %rdi
.LEHB168:
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
.LEHE168:
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1132
	call	_ZdlPv@PLT
	jmp	.L1132
	.p2align 4,,10
	.p2align 3
.L1286:
	movq	-160(%rbp), %rax
	movq	%rax, -376(%rbp)
.L1111:
	movq	-376(%rbp), %rax
	testq	%rax, %rax
	je	.L1137
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L1137:
	movq	-384(%rbp), %rax
	movq	-352(%rbp), %rbx
	leaq	-128(%rbp), %rdi
	movq	%rdi, -360(%rbp)
	movl	64(%rax), %edx
	movq	72(%rax), %rcx
	movq	%rbx, %rsi
.LEHB169:
	call	_ZN2v88internal6torque12CSAGenerator31PreCallableExceptionPreparationB5cxx11ENS_4base8OptionalIPNS1_5BlockEEE
.LEHE169:
	movq	8(%rbx), %rdi
	movl	$4, %edx
	leaq	.LC43(%rip), %rsi
	leaq	-224(%rbp), %r12
.LEHB170:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-392(%rbp), %rax
	movl	8(%rax), %eax
	movl	%eax, -344(%rbp)
	cmpl	$4, %eax
	je	.L1294
	movq	-192(%rbp), %rcx
	movq	-184(%rbp), %rax
	subq	%rcx, %rax
	cmpq	$32, %rax
	je	.L1295
.L1142:
	movq	-384(%rbp), %rax
	movq	32(%rax), %rax
	testq	%rax, %rax
	je	.L1143
	cmpl	$2, 8(%rax)
	jne	.L1143
	movq	376(%rax), %rdx
	movq	368(%rax), %rsi
	leaq	-224(%rbp), %r12
	movq	-352(%rbp), %rax
	movq	8(%rax), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rax, %rdi
	movl	$9, %edx
	leaq	.LC92(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L1144:
	movq	-384(%rbp), %rax
	movq	-352(%rbp), %rbx
	leaq	-224(%rbp), %r12
	movq	32(%rax), %rax
	movq	8(%rbx), %rdi
	movq	136(%rax), %rdx
	movq	128(%rax), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rax, %rdi
	movl	$1, %edx
	leaq	.LC93(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%rbx), %r13
	movq	-280(%rbp), %r15
	movq	%r12, %r14
	movq	-288(%rbp), %rbx
	cmpq	%r15, %rbx
	je	.L1147
.L1146:
	movq	8(%rbx), %rdx
	movq	(%rbx), %rsi
	movq	%r13, %rdi
	movq	%r14, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$32, %rbx
	cmpq	%rbx, %r15
	je	.L1296
	movl	$2, %edx
	leaq	.LC27(%rip), %rsi
	movq	%r13, %rdi
	movq	%r14, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L1146
	.p2align 4,,10
	.p2align 3
.L1296:
	movq	-352(%rbp), %rax
	movq	8(%rax), %r13
.L1147:
	cmpl	$4, -344(%rbp)
	leaq	-224(%rbp), %r12
	je	.L1297
	movl	$3, %edx
	leaq	.LC3(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L1150:
	movq	-384(%rbp), %rax
	leaq	-224(%rbp), %r12
	movq	-392(%rbp), %rdx
	movq	-360(%rbp), %rsi
	movq	-352(%rbp), %rdi
	movq	%r12, %r9
	movl	64(%rax), %ecx
	movq	72(%rax), %r8
	call	_ZN2v88internal6torque12CSAGenerator32PostCallableExceptionPreparationERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKNS1_4TypeENS_4base8OptionalIPNS1_5BlockEEEPNS1_5StackIS8_EE
.LEHE170:
	movq	-128(%rbp), %rdi
	leaq	-112(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1151
	call	_ZdlPv@PLT
.L1151:
	movq	-184(%rbp), %rbx
	movq	-192(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L1152
	.p2align 4,,10
	.p2align 3
.L1156:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1153
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L1156
.L1154:
	movq	-192(%rbp), %r12
.L1152:
	testq	%r12, %r12
	je	.L1157
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1157:
	movq	-216(%rbp), %rbx
	movq	-224(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L1158
	.p2align 4,,10
	.p2align 3
.L1162:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1159
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L1162
.L1160:
	movq	-224(%rbp), %r12
.L1158:
	testq	%r12, %r12
	je	.L1163
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1163:
	movq	-256(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1164
	call	_ZdlPv@PLT
.L1164:
	movq	-280(%rbp), %rbx
	movq	-288(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L1165
	.p2align 4,,10
	.p2align 3
.L1169:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1166
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%rbx, %r12
	jne	.L1169
.L1167:
	movq	-288(%rbp), %r12
.L1165:
	testq	%r12, %r12
	je	.L1170
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1170:
	movq	-312(%rbp), %rbx
	movq	-320(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L1171
	.p2align 4,,10
	.p2align 3
.L1175:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1172
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%rbx, %r12
	jne	.L1175
.L1173:
	movq	-320(%rbp), %r12
.L1171:
	testq	%r12, %r12
	je	.L1072
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1072:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1298
	addq	$376, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1172:
	.cfi_restore_state
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L1175
	jmp	.L1173
	.p2align 4,,10
	.p2align 3
.L1166:
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L1169
	jmp	.L1167
	.p2align 4,,10
	.p2align 3
.L1159:
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L1162
	jmp	.L1160
	.p2align 4,,10
	.p2align 3
.L1153:
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L1156
	jmp	.L1154
	.p2align 4,,10
	.p2align 3
.L1143:
	movq	-288(%rbp), %rsi
	movq	-400(%rbp), %rdi
	leaq	-80(%rbp), %rbx
	movl	$24421, %eax
	leaq	-96(%rbp), %rdx
	movq	%rbx, -96(%rbp)
	movl	$1952543859, -80(%rbp)
	movw	%ax, -76(%rbp)
	movq	$6, -88(%rbp)
	movb	$0, -74(%rbp)
.LEHB171:
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE14_M_insert_rvalEN9__gnu_cxx17__normal_iteratorIPKS5_S7_EEOS5_
.LEHE171:
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1144
	call	_ZdlPv@PLT
	jmp	.L1144
	.p2align 4,,10
	.p2align 3
.L1297:
	movl	$13, %edx
	leaq	.LC94(%rip), %rsi
	movq	%r13, %rdi
.LEHB172:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L1150
	.p2align 4,,10
	.p2align 3
.L1275:
	movq	$0, -368(%rbp)
	jmp	.L1074
	.p2align 4,,10
	.p2align 3
.L1278:
	movq	%rbx, %r12
	xorl	%ecx, %ecx
	jmp	.L1084
	.p2align 4,,10
	.p2align 3
.L1280:
	movq	$0, -368(%rbp)
	jmp	.L1094
	.p2align 4,,10
	.p2align 3
.L1294:
	movq	-352(%rbp), %rbx
	movl	$9, %edx
	leaq	.LC83(%rip), %rsi
	movq	8(%rbx), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%rbx), %r13
	movq	-184(%rbp), %r15
	movq	%r12, %r14
	movq	-192(%rbp), %rbx
	cmpq	%r15, %rbx
	je	.L1140
.L1139:
	movq	8(%rbx), %rdx
	movq	(%rbx), %rsi
	movq	%r13, %rdi
	movq	%r14, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$32, %rbx
	cmpq	%rbx, %r15
	je	.L1299
	movl	$2, %edx
	leaq	.LC27(%rip), %rsi
	movq	%r13, %rdi
	movq	%r14, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L1139
	.p2align 4,,10
	.p2align 3
.L1299:
	movq	-352(%rbp), %rax
	movq	8(%rax), %r13
.L1140:
	movl	$4, %edx
	leaq	.LC84(%rip), %rsi
	movq	%r13, %rdi
	leaq	-224(%rbp), %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L1142
	.p2align 4,,10
	.p2align 3
.L1295:
	movq	8(%rcx), %rdx
	movq	-352(%rbp), %rax
	leaq	-224(%rbp), %r12
	movq	(%rcx), %rsi
	movq	8(%rax), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rax, %rdi
	movl	$3, %edx
	leaq	.LC39(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE172:
	jmp	.L1142
.L1284:
	xorl	%edx, %edx
	movq	$-1, %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
.LEHB173:
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.LEHE173:
.L1285:
	xorl	%edx, %edx
	movq	$-1, %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
.LEHB174:
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.LEHE174:
.L1277:
	leaq	.LC6(%rip), %rdi
.LEHB175:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE175:
.L1290:
	leaq	.LC6(%rip), %rdi
.LEHB176:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE176:
.L1282:
	leaq	.LC6(%rip), %rdi
.LEHB177:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE177:
.L1281:
.LEHB178:
	call	_ZSt17__throw_bad_allocv@PLT
.LEHE178:
.L1298:
	call	__stack_chk_fail@PLT
.L1293:
	movq	%rbx, %rdi
	jmp	.L1126
.L1276:
.LEHB179:
	call	_ZSt17__throw_bad_allocv@PLT
.LEHE179:
.L1279:
.LEHB180:
	call	_ZSt17__throw_bad_allocv@PLT
.LEHE180:
.L1196:
	endbr64
	movq	%rax, %rbx
	jmp	.L1110
.L1195:
	endbr64
	movq	%rax, %rbx
	leaq	-320(%rbp), %rax
	movq	%rax, -408(%rbp)
	leaq	-288(%rbp), %rax
	movq	%rax, -400(%rbp)
	jmp	.L1190
.L1200:
	endbr64
	movq	%rax, %r12
	jmp	.L1181
.L1208:
	endbr64
	movq	%rax, %rbx
	jmp	.L1117
.L1205:
	endbr64
	movq	%rax, %rdi
	jmp	.L1085
.L1207:
	endbr64
	movq	%rax, %rdi
	jmp	.L1104
.L1197:
	endbr64
	movq	%rax, %rbx
	leaq	-192(%rbp), %r14
	jmp	.L1119
.L1201:
	endbr64
	movq	%rax, %rbx
	leaq	-192(%rbp), %r14
	leaq	-224(%rbp), %r12
	jmp	.L1184
.L1199:
	endbr64
	movq	%rax, %r12
	jmp	.L1179
.L1198:
	endbr64
	movq	%rax, %rbx
	jmp	.L1177
.L1203:
	endbr64
	movq	%rax, %r12
	jmp	.L1185
.L1202:
	endbr64
	movq	%rax, %rbx
	jmp	.L1187
	.section	.gcc_except_table._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_23CallCsaMacroInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE,"a",@progbits
	.align 4
.LLSDA6929:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATT6929-.LLSDATTD6929
.LLSDATTD6929:
	.byte	0x1
	.uleb128 .LLSDACSE6929-.LLSDACSB6929
.LLSDACSB6929:
	.uleb128 .LEHB156-.LFB6929
	.uleb128 .LEHE156-.LEHB156
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB157-.LFB6929
	.uleb128 .LEHE157-.LEHB157
	.uleb128 .L1205-.LFB6929
	.uleb128 0x1
	.uleb128 .LEHB158-.LFB6929
	.uleb128 .LEHE158-.LEHB158
	.uleb128 .L1195-.LFB6929
	.uleb128 0
	.uleb128 .LEHB159-.LFB6929
	.uleb128 .LEHE159-.LEHB159
	.uleb128 .L1196-.LFB6929
	.uleb128 0
	.uleb128 .LEHB160-.LFB6929
	.uleb128 .LEHE160-.LEHB160
	.uleb128 .L1207-.LFB6929
	.uleb128 0x1
	.uleb128 .LEHB161-.LFB6929
	.uleb128 .LEHE161-.LEHB161
	.uleb128 .L1201-.LFB6929
	.uleb128 0
	.uleb128 .LEHB162-.LFB6929
	.uleb128 .LEHE162-.LEHB162
	.uleb128 .L1197-.LFB6929
	.uleb128 0
	.uleb128 .LEHB163-.LFB6929
	.uleb128 .LEHE163-.LEHB163
	.uleb128 .L1200-.LFB6929
	.uleb128 0
	.uleb128 .LEHB164-.LFB6929
	.uleb128 .LEHE164-.LEHB164
	.uleb128 .L1197-.LFB6929
	.uleb128 0
	.uleb128 .LEHB165-.LFB6929
	.uleb128 .LEHE165-.LEHB165
	.uleb128 .L1208-.LFB6929
	.uleb128 0
	.uleb128 .LEHB166-.LFB6929
	.uleb128 .LEHE166-.LEHB166
	.uleb128 .L1197-.LFB6929
	.uleb128 0
	.uleb128 .LEHB167-.LFB6929
	.uleb128 .LEHE167-.LEHB167
	.uleb128 .L1198-.LFB6929
	.uleb128 0
	.uleb128 .LEHB168-.LFB6929
	.uleb128 .LEHE168-.LEHB168
	.uleb128 .L1199-.LFB6929
	.uleb128 0
	.uleb128 .LEHB169-.LFB6929
	.uleb128 .LEHE169-.LEHB169
	.uleb128 .L1201-.LFB6929
	.uleb128 0
	.uleb128 .LEHB170-.LFB6929
	.uleb128 .LEHE170-.LEHB170
	.uleb128 .L1202-.LFB6929
	.uleb128 0
	.uleb128 .LEHB171-.LFB6929
	.uleb128 .LEHE171-.LEHB171
	.uleb128 .L1203-.LFB6929
	.uleb128 0
	.uleb128 .LEHB172-.LFB6929
	.uleb128 .LEHE172-.LEHB172
	.uleb128 .L1202-.LFB6929
	.uleb128 0
	.uleb128 .LEHB173-.LFB6929
	.uleb128 .LEHE173-.LEHB173
	.uleb128 .L1200-.LFB6929
	.uleb128 0
	.uleb128 .LEHB174-.LFB6929
	.uleb128 .LEHE174-.LEHB174
	.uleb128 .L1197-.LFB6929
	.uleb128 0
	.uleb128 .LEHB175-.LFB6929
	.uleb128 .LEHE175-.LEHB175
	.uleb128 .L1205-.LFB6929
	.uleb128 0x1
	.uleb128 .LEHB176-.LFB6929
	.uleb128 .LEHE176-.LEHB176
	.uleb128 .L1197-.LFB6929
	.uleb128 0
	.uleb128 .LEHB177-.LFB6929
	.uleb128 .LEHE177-.LEHB177
	.uleb128 .L1207-.LFB6929
	.uleb128 0x1
	.uleb128 .LEHB178-.LFB6929
	.uleb128 .LEHE178-.LEHB178
	.uleb128 .L1196-.LFB6929
	.uleb128 0
	.uleb128 .LEHB179-.LFB6929
	.uleb128 .LEHE179-.LEHB179
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB180-.LFB6929
	.uleb128 .LEHE180-.LEHB180
	.uleb128 .L1195-.LFB6929
	.uleb128 0
.LLSDACSE6929:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATT6929:
	.section	.text._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_23CallCsaMacroInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_23CallCsaMacroInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6929
	.type	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_23CallCsaMacroInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.cold, @function
_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_23CallCsaMacroInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.cold:
.LFSB6929:
	nop
.L1206:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	endbr64
	movq	%rax, %rbx
	call	__cxa_end_catch@PLT
	movq	-224(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1110
	call	_ZdlPv@PLT
.L1110:
	movq	-256(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1190
	call	_ZdlPv@PLT
.L1190:
	movq	-400(%rbp), %rdi
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EED1Ev
	movq	-408(%rbp), %rdi
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EED1Ev
	movq	%rbx, %rdi
.LEHB181:
	call	_Unwind_Resume@PLT
.LEHE181:
.L1181:
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1182
	call	_ZdlPv@PLT
.L1182:
	movq	%r12, %rbx
	leaq	-192(%rbp), %r14
.L1119:
	movq	-160(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1183
	call	_ZdlPv@PLT
.L1183:
	leaq	-224(%rbp), %r12
.L1184:
	movq	%r14, %rdi
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EED1Ev
	movq	%r12, %rdi
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EED1Ev
	jmp	.L1110
.L1117:
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1118
	call	_ZdlPv@PLT
.L1118:
	leaq	-192(%rbp), %r14
	jmp	.L1119
.L1085:
	call	__cxa_begin_catch@PLT
	movq	-368(%rbp), %r12
.L1088:
	cmpq	%r12, %rbx
	jne	.L1300
.LEHB182:
	call	__cxa_rethrow@PLT
.LEHE182:
.L1104:
	call	__cxa_begin_catch@PLT
	movq	-368(%rbp), %r12
.L1107:
	cmpq	%rbx, %r12
	jne	.L1301
.LEHB183:
	call	__cxa_rethrow@PLT
.LEHE183:
.L1300:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1087
	call	_ZdlPv@PLT
.L1087:
	addq	$32, %r12
	jmp	.L1088
.L1204:
	endbr64
	movq	%rax, %r12
	call	__cxa_end_catch@PLT
	movq	-320(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1090
	call	_ZdlPv@PLT
.L1090:
	movq	%r12, %rdi
.LEHB184:
	call	_Unwind_Resume@PLT
.LEHE184:
.L1179:
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1180
	call	_ZdlPv@PLT
.L1180:
	movq	%r12, %rbx
	leaq	-192(%rbp), %r14
	jmp	.L1119
.L1177:
	movq	-128(%rbp), %rdi
	cmpq	-360(%rbp), %rdi
	je	.L1119
	call	_ZdlPv@PLT
	jmp	.L1119
.L1185:
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1186
	call	_ZdlPv@PLT
.L1186:
	movq	%r12, %rbx
	leaq	-224(%rbp), %r12
.L1187:
	movq	-128(%rbp), %rdi
	leaq	-112(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1188
	call	_ZdlPv@PLT
.L1188:
	leaq	-192(%rbp), %r14
	jmp	.L1184
.L1301:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1106
	call	_ZdlPv@PLT
.L1106:
	addq	$32, %r12
	jmp	.L1107
	.cfi_endproc
.LFE6929:
	.section	.gcc_except_table._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_23CallCsaMacroInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.align 4
.LLSDAC6929:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATTC6929-.LLSDATTDC6929
.LLSDATTDC6929:
	.byte	0x1
	.uleb128 .LLSDACSEC6929-.LLSDACSBC6929
.LLSDACSBC6929:
	.uleb128 .LEHB181-.LCOLDB95
	.uleb128 .LEHE181-.LEHB181
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB182-.LCOLDB95
	.uleb128 .LEHE182-.LEHB182
	.uleb128 .L1204-.LCOLDB95
	.uleb128 0
	.uleb128 .LEHB183-.LCOLDB95
	.uleb128 .LEHE183-.LEHB183
	.uleb128 .L1206-.LCOLDB95
	.uleb128 0
	.uleb128 .LEHB184-.LCOLDB95
	.uleb128 .LEHE184-.LEHB184
	.uleb128 0
	.uleb128 0
.LLSDACSEC6929:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATTC6929:
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_23CallCsaMacroInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.section	.text._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_23CallCsaMacroInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.size	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_23CallCsaMacroInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE, .-_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_23CallCsaMacroInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_23CallCsaMacroInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.size	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_23CallCsaMacroInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.cold, .-_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_23CallCsaMacroInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.cold
.LCOLDE95:
	.section	.text._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_23CallCsaMacroInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
.LHOTE95:
	.section	.rodata._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_29PushBuiltinPointerInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC96:
	.string	"ca_.UncheckedCast<BuiltinPtr>(ca_.SmiConstant(Builtins::k"
	.section	.rodata._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_29PushBuiltinPointerInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.str1.1,"aMS",@progbits,1
.LC97:
	.string	"))"
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_29PushBuiltinPointerInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE,"ax",@progbits
	.align 2
.LCOLDB98:
	.section	.text._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_29PushBuiltinPointerInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE,"ax",@progbits
.LHOTB98:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_29PushBuiltinPointerInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.type	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_29PushBuiltinPointerInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE, @function
_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_29PushBuiltinPointerInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE:
.LFB6919:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6919
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-112(%rbp), %r14
	movq	%rsi, %r13
	pushq	%r12
	movq	%r14, %rdi
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	leaq	-96(%rbp), %rbx
	subq	$80, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	40(%rsi), %rax
	movq	%rbx, -112(%rbp)
	movq	$0, -104(%rbp)
	leaq	57(%rax), %rsi
	movb	$0, -96(%rbp)
.LEHB185:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7reserveEm@PLT
	movabsq	$4611686018427387903, %rax
	subq	-104(%rbp), %rax
	cmpq	$56, %rax
	jbe	.L1325
	movl	$57, %edx
	leaq	.LC96(%rip), %rsi
	movq	%r14, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	40(%r13), %rdx
	movq	32(%r13), %rsi
	movq	%r14, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.LEHE185:
	movabsq	$4611686018427387903, %rax
	subq	-104(%rbp), %rax
	cmpq	$1, %rax
	jbe	.L1326
	movl	$2, %edx
	leaq	.LC97(%rip), %rsi
	movq	%r14, %rdi
.LEHB186:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.LEHE186:
	leaq	-64(%rbp), %r13
	leaq	16(%rax), %rdx
	movq	%r13, -80(%rbp)
	movq	(%rax), %rcx
	cmpq	%rdx, %rcx
	je	.L1327
	movq	%rcx, -80(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -64(%rbp)
.L1309:
	movq	8(%rax), %rcx
	movb	$0, 16(%rax)
	movq	%rcx, -72(%rbp)
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movq	8(%r12), %rsi
	cmpq	16(%r12), %rsi
	je	.L1310
	leaq	16(%rsi), %rax
	movq	%rax, (%rsi)
	movq	-80(%rbp), %rax
	cmpq	%r13, %rax
	je	.L1328
	movq	%rax, (%rsi)
	movq	-64(%rbp), %rax
	movq	%rax, 16(%rsi)
.L1312:
	movq	-72(%rbp), %rax
	movq	%rax, 8(%rsi)
	addq	$32, 8(%r12)
.L1313:
	movq	-112(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1302
	call	_ZdlPv@PLT
.L1302:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1329
	addq	$80, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1327:
	.cfi_restore_state
	movdqu	16(%rax), %xmm0
	movaps	%xmm0, -64(%rbp)
	jmp	.L1309
	.p2align 4,,10
	.p2align 3
.L1328:
	movdqa	-64(%rbp), %xmm1
	movups	%xmm1, 16(%rsi)
	jmp	.L1312
	.p2align 4,,10
	.p2align 3
.L1310:
	leaq	-80(%rbp), %rdx
	movq	%r12, %rdi
.LEHB187:
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
.LEHE187:
	movq	-80(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L1313
	call	_ZdlPv@PLT
	jmp	.L1313
.L1329:
	call	__stack_chk_fail@PLT
.L1326:
	leaq	.LC55(%rip), %rdi
.LEHB188:
	call	_ZSt20__throw_length_errorPKc@PLT
.LEHE188:
.L1325:
	leaq	.LC55(%rip), %rdi
.LEHB189:
	call	_ZSt20__throw_length_errorPKc@PLT
.LEHE189:
.L1321:
	endbr64
	movq	%rax, %r12
	jmp	.L1315
.L1320:
	endbr64
	movq	%rax, %r12
	jmp	.L1317
.L1322:
	endbr64
	movq	%rax, %r12
	jmp	.L1305
	.section	.gcc_except_table._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_29PushBuiltinPointerInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE,"a",@progbits
.LLSDA6919:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6919-.LLSDACSB6919
.LLSDACSB6919:
	.uleb128 .LEHB185-.LFB6919
	.uleb128 .LEHE185-.LEHB185
	.uleb128 .L1322-.LFB6919
	.uleb128 0
	.uleb128 .LEHB186-.LFB6919
	.uleb128 .LEHE186-.LEHB186
	.uleb128 .L1320-.LFB6919
	.uleb128 0
	.uleb128 .LEHB187-.LFB6919
	.uleb128 .LEHE187-.LEHB187
	.uleb128 .L1321-.LFB6919
	.uleb128 0
	.uleb128 .LEHB188-.LFB6919
	.uleb128 .LEHE188-.LEHB188
	.uleb128 .L1320-.LFB6919
	.uleb128 0
	.uleb128 .LEHB189-.LFB6919
	.uleb128 .LEHE189-.LEHB189
	.uleb128 .L1322-.LFB6919
	.uleb128 0
.LLSDACSE6919:
	.section	.text._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_29PushBuiltinPointerInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_29PushBuiltinPointerInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6919
	.type	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_29PushBuiltinPointerInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.cold, @function
_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_29PushBuiltinPointerInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.cold:
.LFSB6919:
.L1315:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	movq	-80(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L1317
	call	_ZdlPv@PLT
.L1317:
	movq	-112(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1318
	call	_ZdlPv@PLT
.L1318:
	movq	%r12, %rdi
.LEHB190:
	call	_Unwind_Resume@PLT
.L1305:
	movq	-112(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1306
	call	_ZdlPv@PLT
.L1306:
	movq	%r12, %rdi
	call	_Unwind_Resume@PLT
.LEHE190:
	.cfi_endproc
.LFE6919:
	.section	.gcc_except_table._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_29PushBuiltinPointerInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
.LLSDAC6919:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6919-.LLSDACSBC6919
.LLSDACSBC6919:
	.uleb128 .LEHB190-.LCOLDB98
	.uleb128 .LEHE190-.LEHB190
	.uleb128 0
	.uleb128 0
.LLSDACSEC6919:
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_29PushBuiltinPointerInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.section	.text._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_29PushBuiltinPointerInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.size	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_29PushBuiltinPointerInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE, .-_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_29PushBuiltinPointerInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_29PushBuiltinPointerInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.size	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_29PushBuiltinPointerInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.cold, .-_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_29PushBuiltinPointerInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.cold
.LCOLDE98:
	.section	.text._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_29PushBuiltinPointerInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
.LHOTE98:
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_15PeekInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE,"ax",@progbits
	.align 2
.LCOLDB99:
	.section	.text._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_15PeekInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE,"ax",@progbits
.LHOTB99:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_15PeekInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.type	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_15PeekInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE, @function
_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_15PeekInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE:
.LFB6915:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6915
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$64, %rsp
	.cfi_offset 3, -48
	movq	32(%rsi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdx), %rax
	movq	8(%rdx), %rdx
	subq	%rax, %rdx
	sarq	$5, %rdx
	cmpq	%rsi, %rdx
	jbe	.L1353
	salq	$5, %rsi
	leaq	-64(%rbp), %rbx
	addq	%rsi, %rax
	movq	%rbx, -80(%rbp)
	movq	(%rax), %r14
	movq	8(%rax), %r13
	movq	%r14, %rax
	addq	%r13, %rax
	je	.L1332
	testq	%r14, %r14
	je	.L1354
.L1332:
	movq	%r13, -88(%rbp)
	cmpq	$15, %r13
	ja	.L1355
	cmpq	$1, %r13
	jne	.L1335
	movzbl	(%r14), %eax
	movb	%al, -64(%rbp)
	movq	%rbx, %rax
.L1336:
	movq	%r13, -72(%rbp)
	movb	$0, (%rax,%r13)
	movq	8(%r12), %rsi
	cmpq	16(%r12), %rsi
	je	.L1337
	leaq	16(%rsi), %rax
	movq	%rax, (%rsi)
	movq	-80(%rbp), %rax
	cmpq	%rbx, %rax
	je	.L1356
	movq	%rax, (%rsi)
	movq	-64(%rbp), %rax
	movq	%rax, 16(%rsi)
.L1339:
	movq	-72(%rbp), %rax
	movq	%rax, 8(%rsi)
	addq	$32, 8(%r12)
.L1330:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1357
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1335:
	.cfi_restore_state
	testq	%r13, %r13
	jne	.L1358
	movq	%rbx, %rax
	jmp	.L1336
	.p2align 4,,10
	.p2align 3
.L1355:
	leaq	-80(%rbp), %rdi
	leaq	-88(%rbp), %rsi
	xorl	%edx, %edx
.LEHB191:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE191:
	movq	%rax, -80(%rbp)
	movq	%rax, %rdi
	movq	-88(%rbp), %rax
	movq	%rax, -64(%rbp)
.L1334:
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-88(%rbp), %r13
	movq	-80(%rbp), %rax
	jmp	.L1336
	.p2align 4,,10
	.p2align 3
.L1356:
	movdqa	-64(%rbp), %xmm0
	movups	%xmm0, 16(%rsi)
	jmp	.L1339
	.p2align 4,,10
	.p2align 3
.L1337:
	leaq	-80(%rbp), %rdx
	movq	%r12, %rdi
.LEHB192:
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
.LEHE192:
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1330
	call	_ZdlPv@PLT
	jmp	.L1330
.L1354:
	leaq	.LC6(%rip), %rdi
.LEHB193:
	call	_ZSt19__throw_logic_errorPKc@PLT
.L1353:
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.LEHE193:
.L1357:
	call	__stack_chk_fail@PLT
.L1358:
	movq	%rbx, %rdi
	jmp	.L1334
.L1345:
	endbr64
	movq	%rax, %r12
	jmp	.L1341
	.section	.gcc_except_table._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_15PeekInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE,"a",@progbits
.LLSDA6915:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6915-.LLSDACSB6915
.LLSDACSB6915:
	.uleb128 .LEHB191-.LFB6915
	.uleb128 .LEHE191-.LEHB191
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB192-.LFB6915
	.uleb128 .LEHE192-.LEHB192
	.uleb128 .L1345-.LFB6915
	.uleb128 0
	.uleb128 .LEHB193-.LFB6915
	.uleb128 .LEHE193-.LEHB193
	.uleb128 0
	.uleb128 0
.LLSDACSE6915:
	.section	.text._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_15PeekInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_15PeekInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6915
	.type	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_15PeekInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.cold, @function
_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_15PeekInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.cold:
.LFSB6915:
.L1341:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1342
	call	_ZdlPv@PLT
.L1342:
	movq	%r12, %rdi
.LEHB194:
	call	_Unwind_Resume@PLT
.LEHE194:
	.cfi_endproc
.LFE6915:
	.section	.gcc_except_table._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_15PeekInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
.LLSDAC6915:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6915-.LLSDACSBC6915
.LLSDACSBC6915:
	.uleb128 .LEHB194-.LCOLDB99
	.uleb128 .LEHE194-.LEHB194
	.uleb128 0
	.uleb128 0
.LLSDACSEC6915:
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_15PeekInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.section	.text._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_15PeekInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.size	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_15PeekInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE, .-_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_15PeekInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_15PeekInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.size	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_15PeekInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.cold, .-_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_15PeekInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.cold
.LCOLDE99:
	.section	.text._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_15PeekInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
.LHOTE99:
	.section	.rodata._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_28PushUninitializedInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.str1.1,"aMS",@progbits,1
.LC100:
	.string	"ca_.Uninitialized<"
.LC101:
	.string	">()"
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_28PushUninitializedInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE,"ax",@progbits
	.align 2
.LCOLDB102:
	.section	.text._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_28PushUninitializedInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE,"ax",@progbits
.LHOTB102:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_28PushUninitializedInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.type	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_28PushUninitializedInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE, @function
_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_28PushUninitializedInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE:
.LFB6918:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6918
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-144(%rbp), %r13
	movq	%rdx, %r12
	pushq	%rbx
	movq	%r13, %rdi
	subq	$120, %rsp
	.cfi_offset 3, -40
	movq	32(%rsi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
.LEHB195:
	call	_ZNK2v88internal6torque4Type25GetGeneratedTNodeTypeNameB5cxx11Ev@PLT
.LEHE195:
	movl	$18, %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
	leaq	.LC100(%rip), %rcx
	movq	%r13, %rdi
.LEHB196:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.LEHE196:
	leaq	-96(%rbp), %rbx
	leaq	16(%rax), %rdx
	movq	%rbx, -112(%rbp)
	movq	(%rax), %rcx
	cmpq	%rdx, %rcx
	je	.L1382
	movq	%rcx, -112(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -96(%rbp)
.L1361:
	movq	8(%rax), %rcx
	movb	$0, 16(%rax)
	movq	%rcx, -104(%rbp)
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movabsq	$4611686018427387903, %rax
	subq	-104(%rbp), %rax
	cmpq	$2, %rax
	jbe	.L1383
	leaq	-112(%rbp), %rdi
	movl	$3, %edx
	leaq	.LC101(%rip), %rsi
.LEHB197:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.LEHE197:
	leaq	-64(%rbp), %r13
	leaq	16(%rax), %rdx
	movq	%r13, -80(%rbp)
	movq	(%rax), %rcx
	cmpq	%rdx, %rcx
	je	.L1384
	movq	%rcx, -80(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -64(%rbp)
.L1364:
	movq	8(%rax), %rcx
	movb	$0, 16(%rax)
	movq	%rcx, -72(%rbp)
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movq	8(%r12), %rsi
	cmpq	16(%r12), %rsi
	je	.L1365
	leaq	16(%rsi), %rax
	movq	%rax, (%rsi)
	movq	-80(%rbp), %rax
	cmpq	%r13, %rax
	je	.L1385
	movq	%rax, (%rsi)
	movq	-64(%rbp), %rax
	movq	%rax, 16(%rsi)
.L1367:
	movq	-72(%rbp), %rax
	movq	%rax, 8(%rsi)
	addq	$32, 8(%r12)
.L1368:
	movq	-112(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1369
	call	_ZdlPv@PLT
.L1369:
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1359
	call	_ZdlPv@PLT
.L1359:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1386
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1382:
	.cfi_restore_state
	movdqu	16(%rax), %xmm0
	movaps	%xmm0, -96(%rbp)
	jmp	.L1361
	.p2align 4,,10
	.p2align 3
.L1384:
	movdqu	16(%rax), %xmm1
	movaps	%xmm1, -64(%rbp)
	jmp	.L1364
	.p2align 4,,10
	.p2align 3
.L1385:
	movdqa	-64(%rbp), %xmm2
	movups	%xmm2, 16(%rsi)
	jmp	.L1367
	.p2align 4,,10
	.p2align 3
.L1365:
	leaq	-80(%rbp), %rdx
	movq	%r12, %rdi
.LEHB198:
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
.LEHE198:
	movq	-80(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L1368
	call	_ZdlPv@PLT
	jmp	.L1368
.L1383:
	leaq	.LC55(%rip), %rdi
.LEHB199:
	call	_ZSt20__throw_length_errorPKc@PLT
.LEHE199:
.L1386:
	call	__stack_chk_fail@PLT
.L1379:
	endbr64
	movq	%rax, %r12
	jmp	.L1373
.L1380:
	endbr64
	movq	%rax, %r12
	jmp	.L1371
.L1378:
	endbr64
	movq	%rax, %r12
	jmp	.L1375
	.section	.gcc_except_table._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_28PushUninitializedInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE,"a",@progbits
.LLSDA6918:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6918-.LLSDACSB6918
.LLSDACSB6918:
	.uleb128 .LEHB195-.LFB6918
	.uleb128 .LEHE195-.LEHB195
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB196-.LFB6918
	.uleb128 .LEHE196-.LEHB196
	.uleb128 .L1378-.LFB6918
	.uleb128 0
	.uleb128 .LEHB197-.LFB6918
	.uleb128 .LEHE197-.LEHB197
	.uleb128 .L1379-.LFB6918
	.uleb128 0
	.uleb128 .LEHB198-.LFB6918
	.uleb128 .LEHE198-.LEHB198
	.uleb128 .L1380-.LFB6918
	.uleb128 0
	.uleb128 .LEHB199-.LFB6918
	.uleb128 .LEHE199-.LEHB199
	.uleb128 .L1379-.LFB6918
	.uleb128 0
.LLSDACSE6918:
	.section	.text._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_28PushUninitializedInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_28PushUninitializedInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6918
	.type	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_28PushUninitializedInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.cold, @function
_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_28PushUninitializedInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.cold:
.LFSB6918:
.L1371:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movq	-80(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L1373
	call	_ZdlPv@PLT
.L1373:
	movq	-112(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1375
	call	_ZdlPv@PLT
.L1375:
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1376
	call	_ZdlPv@PLT
.L1376:
	movq	%r12, %rdi
.LEHB200:
	call	_Unwind_Resume@PLT
.LEHE200:
	.cfi_endproc
.LFE6918:
	.section	.gcc_except_table._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_28PushUninitializedInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
.LLSDAC6918:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6918-.LLSDACSBC6918
.LLSDACSBC6918:
	.uleb128 .LEHB200-.LCOLDB102
	.uleb128 .LEHE200-.LEHB200
	.uleb128 0
	.uleb128 0
.LLSDACSEC6918:
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_28PushUninitializedInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.section	.text._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_28PushUninitializedInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.size	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_28PushUninitializedInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE, .-_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_28PushUninitializedInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_28PushUninitializedInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.size	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_28PushUninitializedInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.cold, .-_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_28PushUninitializedInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.cold
.LCOLDE102:
	.section	.text._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_28PushUninitializedInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
.LHOTE102:
	.section	.rodata._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_24CallIntrinsicInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.str1.1,"aMS",@progbits,1
.LC103:
	.string	"%RawDownCast"
	.section	.rodata._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_24CallIntrinsicInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC104:
	.string	"%RawDownCast must take a single parameter"
	.section	.rodata._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_24CallIntrinsicInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.str1.1
.LC105:
	.string	" is not a subtype of "
.LC106:
	.string	"%RawDownCast error: "
.LC107:
	.string	"TORQUE_CAST"
.LC108:
	.string	"%FromConstexpr"
	.section	.rodata._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_24CallIntrinsicInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.str1.8
	.align 8
.LC109:
	.string	"%FromConstexpr must take a single parameter with constexpr type"
	.align 8
.LC110:
	.string	"%FromConstexpr must return a non-constexpr type"
	.section	.rodata._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_24CallIntrinsicInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.str1.1
.LC111:
	.string	"ca_.SmiConstant"
.LC112:
	.string	"ca_.NumberConstant"
.LC113:
	.string	"ca_.StringConstant"
	.section	.rodata._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_24CallIntrinsicInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.str1.8
	.align 8
.LC114:
	.string	"%FromConstexpr cannot cast to subclass of HeapObject unless it's a String or Number"
	.section	.rodata._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_24CallIntrinsicInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.str1.1
.LC115:
	.string	"ca_.IntPtrConstant"
.LC116:
	.string	"ca_.UintPtrConstant"
.LC117:
	.string	"ca_.Int32Constant"
	.section	.rodata._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_24CallIntrinsicInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.str1.8
	.align 8
.LC118:
	.string	"%FromConstexpr does not support return type "
	.section	.rodata._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_24CallIntrinsicInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.str1.1
.LC119:
	.string	"%GetAllocationBaseSize"
	.section	.rodata._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_24CallIntrinsicInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.str1.8
	.align 8
.LC120:
	.string	"incorrect number of specialization classes for %GetAllocationBaseSize (should be one)"
	.align 8
.LC121:
	.string	"CodeStubAssembler(state_).IntPtrConstant(("
	.align 8
.LC122:
	.string	"CodeStubAssembler(state_).TimesTaggedSize(CodeStubAssembler(state_).LoadMapInstanceSizeInWords("
	.section	.rodata._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_24CallIntrinsicInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.str1.1
.LC123:
	.string	"%Allocate"
	.section	.rodata._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_24CallIntrinsicInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.str1.8
	.align 8
.LC124:
	.string	">(CodeStubAssembler(state_).Allocate"
	.section	.rodata._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_24CallIntrinsicInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.str1.1
.LC125:
	.string	"%GetStructMap"
	.section	.rodata._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_24CallIntrinsicInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.str1.8
	.align 8
.LC126:
	.string	"CodeStubAssembler(state_).GetStructMap"
	.align 8
.LC127:
	.string	"no built in intrinsic with name "
	.align 8
.LC128:
	.string	"    CodeStubAssembler(state_).InitializeFieldsWithRoot("
	.align 8
.LC129:
	.string	"CodeStubAssembler(state_).IntPtrConstant("
	.section	.rodata._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_24CallIntrinsicInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.str1.1
.LC130:
	.string	"), "
	.section	.rodata._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_24CallIntrinsicInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.str1.8
	.align 8
.LC131:
	.string	", RootIndex::kUndefinedValue);\n"
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_24CallIntrinsicInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE,"ax",@progbits
	.align 2
.LCOLDB132:
	.section	.text._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_24CallIntrinsicInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE,"ax",@progbits
.LHOTB132:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_24CallIntrinsicInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.type	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_24CallIntrinsicInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE, @function
_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_24CallIntrinsicInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE:
.LFB6925:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6925
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$728, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	72(%rsi), %rax
	movq	64(%rsi), %r13
	movq	%rdi, -696(%rbp)
	movq	%rsi, -736(%rbp)
	movq	%rax, %rbx
	subq	%r13, %rbx
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	movq	%rax, -704(%rbp)
	movq	%rbx, %rax
	sarq	$5, %rax
	movaps	%xmm0, -672(%rbp)
	movq	$0, -656(%rbp)
	je	.L1740
	movabsq	$288230376151711743, %rdx
	cmpq	%rdx, %rax
	ja	.L1741
	movq	%rbx, %rdi
.LEHB201:
	call	_Znwm@PLT
.LEHE201:
	movq	%rax, -720(%rbp)
	movq	-736(%rbp), %rax
	movq	72(%rax), %rcx
	movq	64(%rax), %r13
	movq	%rcx, -704(%rbp)
.L1389:
	movq	-720(%rbp), %rax
	movq	%rax, %xmm0
	addq	%rax, %rbx
	punpcklqdq	%xmm0, %xmm0
	movq	%rbx, -656(%rbp)
	movq	%rax, %rbx
	movaps	%xmm0, -672(%rbp)
	cmpq	-704(%rbp), %r13
	je	.L1391
	leaq	-512(%rbp), %rax
	movq	%rax, -712(%rbp)
	jmp	.L1397
	.p2align 4,,10
	.p2align 3
.L1393:
	cmpq	$1, %r12
	jne	.L1395
	movzbl	(%r14), %eax
	movb	%al, 16(%rbx)
.L1396:
	movq	%r12, 8(%rbx)
	addq	$32, %r13
	addq	$32, %rbx
	movb	$0, (%rdi,%r12)
	cmpq	%r13, -704(%rbp)
	je	.L1391
.L1397:
	leaq	16(%rbx), %rdi
	movq	%rdi, (%rbx)
	movq	0(%r13), %r14
	movq	8(%r13), %r12
	movq	%r14, %rax
	addq	%r12, %rax
	je	.L1392
	testq	%r14, %r14
	je	.L1742
.L1392:
	movq	%r12, -512(%rbp)
	cmpq	$15, %r12
	jbe	.L1393
	movq	-712(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rbx, %rdi
.LEHB202:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE202:
	movq	%rax, (%rbx)
	movq	%rax, %rdi
	movq	-512(%rbp), %rax
	movq	%rax, 16(%rbx)
.L1394:
	movq	%r12, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-512(%rbp), %r12
	movq	(%rbx), %rdi
	jmp	.L1396
	.p2align 4,,10
	.p2align 3
.L1395:
	testq	%r12, %r12
	je	.L1396
	jmp	.L1394
	.p2align 4,,10
	.p2align 3
.L1391:
	movq	-736(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	%rbx, -664(%rbp)
	movq	$0, -624(%rbp)
	movq	32(%rax), %r12
	movaps	%xmm0, -640(%rbp)
	movq	$0, -592(%rbp)
	movq	264(%r12), %rax
	movq	256(%r12), %rsi
	movaps	%xmm0, -608(%rbp)
	movq	%rax, %rbx
	subq	%rsi, %rbx
	movq	%rbx, %rdx
	sarq	$3, %rdx
	je	.L1743
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L1744
	movq	%rbx, %rdi
.LEHB203:
	call	_Znwm@PLT
.LEHE203:
	movq	%rax, %rcx
	movq	264(%r12), %rax
	movq	256(%r12), %rsi
	movq	%rax, %r12
	subq	%rsi, %r12
.L1399:
	movq	%rcx, %xmm0
	addq	%rcx, %rbx
	punpcklqdq	%xmm0, %xmm0
	movq	%rbx, -592(%rbp)
	movaps	%xmm0, -608(%rbp)
	cmpq	%rsi, %rax
	je	.L1407
	movq	%rcx, %rdi
	movq	%r12, %rdx
	call	memmove@PLT
	movq	%rax, %rcx
.L1407:
	addq	%r12, %rcx
	leaq	-640(%rbp), %rax
	movq	%r15, %r8
	movq	-696(%rbp), %rdi
	movq	%rcx, -600(%rbp)
	leaq	-608(%rbp), %rsi
	leaq	-672(%rbp), %rcx
	movq	%rax, %rdx
	movq	%rcx, -752(%rbp)
	movq	%rax, -760(%rbp)
.LEHB204:
	call	_ZN2v88internal6torque12CSAGenerator22ProcessArgumentsCommonERKSt6vectorIPKNS1_4TypeESaIS6_EEPS3_INSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaISG_EESJ_PNS1_5StackISG_EE
	movq	8(%r15), %rax
	movq	(%r15), %r13
	pxor	%xmm0, %xmm0
	movq	$0, -560(%rbp)
	movaps	%xmm0, -576(%rbp)
	movq	%rax, %rbx
	movq	%rax, -704(%rbp)
	subq	%r13, %rbx
	movq	%rbx, %rax
	sarq	$5, %rax
	je	.L1745
	movabsq	$288230376151711743, %rdx
	cmpq	%rdx, %rax
	ja	.L1746
	movq	%rbx, %rdi
	call	_Znwm@PLT
.LEHE204:
	movq	%rax, -720(%rbp)
	movq	8(%r15), %rax
	movq	(%r15), %r13
	movq	%rax, -704(%rbp)
.L1409:
	movq	-720(%rbp), %rax
	movq	%rax, %xmm0
	addq	%rax, %rbx
	punpcklqdq	%xmm0, %xmm0
	movq	%rbx, -560(%rbp)
	movq	%rax, %rbx
	movaps	%xmm0, -576(%rbp)
	cmpq	-704(%rbp), %r13
	je	.L1411
	leaq	-512(%rbp), %rax
	movq	%rax, -712(%rbp)
	jmp	.L1417
	.p2align 4,,10
	.p2align 3
.L1413:
	cmpq	$1, %r12
	jne	.L1415
	movzbl	(%r14), %eax
	movb	%al, 16(%rbx)
.L1416:
	movq	%r12, 8(%rbx)
	addq	$32, %r13
	addq	$32, %rbx
	movb	$0, (%rdi,%r12)
	cmpq	%r13, -704(%rbp)
	je	.L1411
.L1417:
	leaq	16(%rbx), %rdi
	movq	%rdi, (%rbx)
	movq	0(%r13), %r14
	movq	8(%r13), %r12
	movq	%r14, %rax
	addq	%r12, %rax
	je	.L1412
	testq	%r14, %r14
	je	.L1747
.L1412:
	movq	%r12, -512(%rbp)
	cmpq	$15, %r12
	jbe	.L1413
	movq	-712(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rbx, %rdi
.LEHB205:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE205:
	movq	%rax, (%rbx)
	movq	%rax, %rdi
	movq	-512(%rbp), %rax
	movq	%rax, 16(%rbx)
.L1414:
	movq	%r12, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-512(%rbp), %r12
	movq	(%rbx), %rdi
	jmp	.L1416
	.p2align 4,,10
	.p2align 3
.L1415:
	testq	%r12, %r12
	je	.L1416
	jmp	.L1414
	.p2align 4,,10
	.p2align 3
.L1411:
	movq	-736(%rbp), %rax
	pxor	%xmm0, %xmm0
	leaq	-512(%rbp), %rdi
	movq	%rbx, -568(%rbp)
	movq	$0, -528(%rbp)
	movq	32(%rax), %rax
	movaps	%xmm0, -544(%rbp)
	movq	296(%rax), %rsi
	movq	%rsi, -744(%rbp)
.LEHB206:
	call	_ZN2v88internal6torque9LowerTypeEPKNS1_4TypeE@PLT
.LEHE206:
	movq	-504(%rbp), %rax
	movq	-512(%rbp), %rbx
	movq	%rax, -728(%rbp)
	cmpq	%rax, %rbx
	je	.L1426
	leaq	-464(%rbp), %rax
	leaq	-448(%rbp), %r14
	movq	%rax, -704(%rbp)
	jmp	.L1451
	.p2align 4,,10
	.p2align 3
.L1757:
	movq	-720(%rbp), %rax
	movzbl	(%rax), %eax
	movb	%al, -432(%rbp)
	movq	%r13, %rax
.L1443:
	movq	%r12, -440(%rbp)
	movb	$0, (%rax,%r12)
	movq	8(%r15), %rsi
	cmpq	16(%r15), %rsi
	je	.L1444
	leaq	16(%rsi), %rax
	movq	%rax, (%rsi)
	movq	-448(%rbp), %rax
	cmpq	%r13, %rax
	je	.L1748
	movq	%rax, (%rsi)
	movq	-432(%rbp), %rax
	movq	%rax, 16(%rsi)
.L1446:
	movq	-440(%rbp), %rax
	movq	%rax, 8(%rsi)
	addq	$32, 8(%r15)
.L1447:
	movq	-696(%rbp), %rax
	movl	$20, %edx
	leaq	.LC74(%rip), %rsi
	movq	8(%rax), %r12
	movq	%r12, %rdi
.LEHB207:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-712(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZNK2v88internal6torque4Type25GetGeneratedTNodeTypeNameB5cxx11Ev@PLT
.LEHE207:
	movq	-440(%rbp), %rdx
	movq	-448(%rbp), %rsi
	movq	%r12, %rdi
.LEHB208:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$2, %edx
	leaq	.LC75(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r15), %rdx
	movq	8(%r15), %rax
	subq	%rdx, %rax
	je	.L1749
	leaq	-32(%rdx,%rax), %rax
	movq	%r12, %rdi
	movq	8(%rax), %rdx
	movq	(%rax), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rax, %rdi
	movl	$2, %edx
	leaq	.LC40(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE208:
	movq	-448(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L1449
	call	_ZdlPv@PLT
.L1449:
	movq	-696(%rbp), %rax
	movl	$8, %edx
	leaq	.LC80(%rip), %rsi
	movq	8(%rax), %r12
	movq	%r12, %rdi
.LEHB209:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r15), %rdx
	movq	8(%r15), %rax
	subq	%rdx, %rax
	je	.L1750
	leaq	-32(%rdx,%rax), %rax
	movq	%r12, %rdi
	movq	8(%rax), %rdx
	movq	(%rax), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rax, %rdi
	movl	$3, %edx
	leaq	.LC3(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$8, %rbx
	cmpq	%rbx, -728(%rbp)
	je	.L1751
.L1451:
	movq	-696(%rbp), %rcx
	movq	(%rbx), %rax
	movl	$32, %edx
	movq	%r14, %rdi
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	movq	16(%rcx), %r8
	movq	%rax, -712(%rbp)
	leaq	1(%r8), %rax
	movq	%rax, 16(%rcx)
	leaq	.LC0(%rip), %rcx
	xorl	%eax, %eax
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0
.LEHE209:
	movl	$3, %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r14, %rdi
	leaq	.LC73(%rip), %rcx
.LEHB210:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.LEHE210:
	movq	-704(%rbp), %rcx
	leaq	16(%rax), %rdx
	movq	%rcx, -480(%rbp)
	movq	(%rax), %rcx
	cmpq	%rdx, %rcx
	je	.L1752
	movq	%rcx, -480(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -464(%rbp)
.L1428:
	movq	8(%rax), %rcx
	leaq	-432(%rbp), %r13
	movq	%rcx, -472(%rbp)
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movb	$0, 16(%rax)
	movq	-448(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L1429
	call	_ZdlPv@PLT
.L1429:
	movq	-536(%rbp), %rsi
	cmpq	-528(%rbp), %rsi
	je	.L1753
	leaq	16(%rsi), %rax
	movq	%rax, (%rsi)
	movq	-480(%rbp), %rax
	cmpq	-704(%rbp), %rax
	je	.L1754
	movq	%rax, (%rsi)
	movq	-464(%rbp), %rax
	movq	%rax, 16(%rsi)
.L1436:
	movq	-472(%rbp), %rax
	movq	%rax, 8(%rsi)
	movq	-536(%rbp), %rax
	addq	$32, %rax
	movq	%rax, -536(%rbp)
.L1437:
	movq	%r13, -448(%rbp)
	movq	-32(%rax), %rcx
	movq	-24(%rax), %r12
	movq	%rcx, %rax
	movq	%rcx, -720(%rbp)
	addq	%r12, %rax
	je	.L1439
	testq	%rcx, %rcx
	je	.L1755
.L1439:
	movq	%r12, -680(%rbp)
	cmpq	$15, %r12
	ja	.L1756
	cmpq	$1, %r12
	je	.L1757
	testq	%r12, %r12
	jne	.L1758
	movq	%r13, %rax
	jmp	.L1443
	.p2align 4,,10
	.p2align 3
.L1756:
	leaq	-680(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r14, %rdi
.LEHB211:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE211:
	movq	%rax, -448(%rbp)
	movq	%rax, %rdi
	movq	-680(%rbp), %rax
	movq	%rax, -432(%rbp)
.L1441:
	movq	-720(%rbp), %rsi
	movq	%r12, %rdx
	call	memcpy@PLT
	movq	-680(%rbp), %r12
	movq	-448(%rbp), %rax
	jmp	.L1443
	.p2align 4,,10
	.p2align 3
.L1752:
	movdqu	16(%rax), %xmm1
	movaps	%xmm1, -464(%rbp)
	jmp	.L1428
	.p2align 4,,10
	.p2align 3
.L1748:
	movdqa	-432(%rbp), %xmm3
	movups	%xmm3, 16(%rsi)
	jmp	.L1446
	.p2align 4,,10
	.p2align 3
.L1754:
	movdqa	-464(%rbp), %xmm2
	movups	%xmm2, 16(%rsi)
	jmp	.L1436
	.p2align 4,,10
	.p2align 3
.L1753:
	leaq	-480(%rbp), %rdx
	leaq	-544(%rbp), %rdi
.LEHB212:
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
.LEHE212:
	movq	-480(%rbp), %rdi
	cmpq	-704(%rbp), %rdi
	je	.L1717
	call	_ZdlPv@PLT
.L1717:
	movq	-536(%rbp), %rax
	jmp	.L1437
	.p2align 4,,10
	.p2align 3
.L1444:
	movq	%r14, %rdx
	movq	%r15, %rdi
.LEHB213:
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
.LEHE213:
	movq	-448(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L1447
	call	_ZdlPv@PLT
	jmp	.L1447
	.p2align 4,,10
	.p2align 3
.L1751:
	movq	-512(%rbp), %rax
	movq	%rax, -728(%rbp)
.L1426:
	movq	-728(%rbp), %rax
	testq	%rax, %rax
	je	.L1452
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L1452:
	movq	-696(%rbp), %rax
	movl	$4, %edx
	leaq	.LC43(%rip), %rsi
	movq	8(%rax), %rdi
.LEHB214:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-744(%rbp), %rax
	cmpl	$4, 8(%rax)
	je	.L1759
	movq	-544(%rbp), %rcx
	movq	-536(%rbp), %rax
	subq	%rcx, %rax
	cmpq	$32, %rax
	je	.L1760
.L1457:
	movq	-736(%rbp), %rax
	leaq	.LC103(%rip), %rsi
	movq	32(%rax), %rax
	leaq	128(%rax), %rdi
	movq	%rax, -704(%rbp)
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	jne	.L1458
	movq	-608(%rbp), %rdx
	movq	-600(%rbp), %rax
	subq	%rdx, %rax
	cmpq	$8, %rax
	jne	.L1761
	movq	-744(%rbp), %rdi
	movq	(%rdx), %rsi
	movq	(%rdi), %rax
	call	*16(%rax)
	testb	%al, %al
	je	.L1762
	movq	-744(%rbp), %rax
	movq	(%rax), %rax
	movq	16(%rax), %rbx
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_10TypeOracleEEERPNT_5ScopeEv@PLT
.LEHE214:
	leaq	-448(%rbp), %r14
	movl	$25701, %r9d
	leaq	-432(%rbp), %r13
	movl	$1734828372, -432(%rbp)
	movq	%r14, %rdi
	movq	%r13, -448(%rbp)
	movw	%r9w, -428(%rbp)
	movq	$6, -440(%rbp)
	movb	$0, -426(%rbp)
.LEHB215:
	call	_ZN2v88internal6torque12Declarations16LookupGlobalTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
.LEHE215:
	movq	-448(%rbp), %rdi
	movq	%rax, %r12
	cmpq	%r13, %rdi
	je	.L1463
	call	_ZdlPv@PLT
.L1463:
	movq	-744(%rbp), %rdi
	movq	%r12, %rsi
.LEHB216:
	call	*%rbx
	movl	%eax, %ebx
	testb	%al, %al
	jne	.L1763
.L1467:
	movq	-696(%rbp), %rax
	movq	8(%rax), %rdi
.L1471:
	movl	$1, %edx
	leaq	.LC93(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-696(%rbp), %rax
	movq	-640(%rbp), %rbx
	leaq	.LC27(%rip), %r14
	movq	-632(%rbp), %r13
	movq	8(%rax), %r12
	cmpq	%r13, %rbx
	je	.L1542
.L1538:
	movq	8(%rbx), %rdx
	movq	(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$32, %rbx
	cmpq	%rbx, %r13
	je	.L1542
	movl	$2, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L1538
	.p2align 4,,10
	.p2align 3
.L1542:
	movq	-736(%rbp), %rax
	leaq	.LC123(%rip), %rsi
	movq	32(%rax), %rax
	leaq	128(%rax), %rdi
	movq	%rax, -704(%rbp)
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	je	.L1539
.L1540:
	movq	-736(%rbp), %rax
	leaq	.LC119(%rip), %rsi
	movq	32(%rax), %rax
	leaq	128(%rax), %rdi
	movq	%rax, -704(%rbp)
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	je	.L1764
.L1543:
	movq	-696(%rbp), %rax
	movq	8(%rax), %rdi
	movq	-744(%rbp), %rax
	cmpl	$4, 8(%rax)
	je	.L1765
	movl	$3, %edx
	leaq	.LC3(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L1545:
	movq	-736(%rbp), %rax
	leaq	.LC123(%rip), %rsi
	movq	32(%rax), %rdi
	subq	$-128, %rdi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	jne	.L1546
	movq	-696(%rbp), %rbx
	movl	$55, %edx
	leaq	.LC128(%rip), %rsi
	movq	8(%rbx), %r12
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-544(%rbp), %rax
	movq	%r12, %rdi
	movq	8(%rax), %rdx
	movq	(%rax), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rax, %rdi
	movl	$2, %edx
	leaq	.LC27(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%rbx), %r12
	movl	$41, %edx
	leaq	.LC129(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-744(%rbp), %rax
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	leaq	-448(%rbp), %rdi
	leaq	.LC0(%rip), %rcx
	movl	$32, %edx
	movq	168(%rax), %r8
	xorl	%eax, %eax
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0
.LEHE216:
	movq	-440(%rbp), %rdx
	movq	-448(%rbp), %rsi
	movq	%r12, %rdi
.LEHB217:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rax, %rdi
	movl	$3, %edx
	leaq	.LC130(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE217:
	movq	-448(%rbp), %rdi
	leaq	-432(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1547
	call	_ZdlPv@PLT
.L1547:
	movq	-696(%rbp), %rax
	movq	-640(%rbp), %rbx
	leaq	.LC27(%rip), %r14
	movq	-632(%rbp), %r13
	movq	8(%rax), %r12
	cmpq	%r13, %rbx
	je	.L1549
.L1548:
	movq	8(%rbx), %rdx
	movq	(%rbx), %rsi
	movq	%r12, %rdi
.LEHB218:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$32, %rbx
	cmpq	%rbx, %r13
	je	.L1766
	movl	$2, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L1548
	.p2align 4,,10
	.p2align 3
.L1766:
	movq	-696(%rbp), %rax
	movq	8(%rax), %r12
.L1549:
	movl	$31, %edx
	leaq	.LC131(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L1546:
	movq	-536(%rbp), %rbx
	movq	-544(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L1551
	.p2align 4,,10
	.p2align 3
.L1555:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1552
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L1555
.L1553:
	movq	-544(%rbp), %r12
.L1551:
	testq	%r12, %r12
	je	.L1556
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1556:
	movq	-568(%rbp), %rbx
	movq	-576(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L1557
	.p2align 4,,10
	.p2align 3
.L1561:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1558
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L1561
.L1559:
	movq	-576(%rbp), %r12
.L1557:
	testq	%r12, %r12
	je	.L1562
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1562:
	movq	-608(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1563
	call	_ZdlPv@PLT
.L1563:
	movq	-632(%rbp), %rbx
	movq	-640(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L1564
	.p2align 4,,10
	.p2align 3
.L1568:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1565
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%rbx, %r12
	jne	.L1568
.L1566:
	movq	-640(%rbp), %r12
.L1564:
	testq	%r12, %r12
	je	.L1569
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1569:
	movq	-664(%rbp), %rbx
	movq	-672(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L1570
	.p2align 4,,10
	.p2align 3
.L1574:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1571
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L1574
.L1572:
	movq	-672(%rbp), %r12
.L1570:
	testq	%r12, %r12
	je	.L1387
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1387:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1767
	addq	$728, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1571:
	.cfi_restore_state
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L1574
	jmp	.L1572
	.p2align 4,,10
	.p2align 3
.L1565:
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L1568
	jmp	.L1566
	.p2align 4,,10
	.p2align 3
.L1558:
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L1561
	jmp	.L1559
	.p2align 4,,10
	.p2align 3
.L1552:
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L1555
	jmp	.L1553
	.p2align 4,,10
	.p2align 3
.L1539:
	movq	-696(%rbp), %rax
	movl	$1, %edx
	leaq	.LC57(%rip), %rsi
	movq	8(%rax), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L1540
	.p2align 4,,10
	.p2align 3
.L1763:
	movq	-608(%rbp), %rax
	movq	%r14, %rdi
	movq	(%rax), %rsi
	call	_ZNK2v88internal6torque4Type25GetGeneratedTNodeTypeNameB5cxx11Ev@PLT
.LEHE218:
	movq	-744(%rbp), %rsi
	leaq	-480(%rbp), %rdi
.LEHB219:
	call	_ZNK2v88internal6torque4Type25GetGeneratedTNodeTypeNameB5cxx11Ev@PLT
.LEHE219:
	movq	-472(%rbp), %rdx
	movq	-480(%rbp), %r12
	cmpq	-440(%rbp), %rdx
	je	.L1768
	xorl	%ebx, %ebx
.L1468:
	leaq	-464(%rbp), %rax
	cmpq	%rax, %r12
	je	.L1469
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1469:
	movq	-448(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L1470
	call	_ZdlPv@PLT
.L1470:
	movq	-696(%rbp), %rax
	movq	8(%rax), %rdi
	testb	%bl, %bl
	jne	.L1471
	movl	$11, %edx
	leaq	.LC107(%rip), %rsi
.LEHB220:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L1467
	.p2align 4,,10
	.p2align 3
.L1765:
	movl	$13, %edx
	leaq	.LC94(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L1545
	.p2align 4,,10
	.p2align 3
.L1764:
	movq	-696(%rbp), %rax
	movl	$2, %edx
	leaq	.LC97(%rip), %rsi
	movq	8(%rax), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L1543
	.p2align 4,,10
	.p2align 3
.L1458:
	movq	-736(%rbp), %rax
	leaq	.LC108(%rip), %rsi
	movq	32(%rax), %rax
	leaq	128(%rax), %rdi
	movq	%rax, -704(%rbp)
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	jne	.L1472
	movq	-608(%rbp), %rdx
	movq	-600(%rbp), %rax
	subq	%rdx, %rax
	cmpq	$8, %rax
	jne	.L1476
	movq	(%rdx), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	testb	%al, %al
	je	.L1476
	movq	-744(%rbp), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	testb	%al, %al
	jne	.L1769
	movq	-744(%rbp), %rax
	movq	(%rax), %rax
	movq	16(%rax), %rbx
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_10TypeOracleEEERPNT_5ScopeEv@PLT
.LEHE220:
	leaq	-448(%rbp), %r14
	leaq	-432(%rbp), %r13
	movl	$27987, %r8d
	movb	$105, -430(%rbp)
	movq	%r14, %rdi
	movq	%r13, -448(%rbp)
	movw	%r8w, -432(%rbp)
	movq	$3, -440(%rbp)
	movb	$0, -429(%rbp)
.LEHB221:
	call	_ZN2v88internal6torque12Declarations16LookupGlobalTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
.LEHE221:
	movq	-448(%rbp), %rdi
	movq	%rax, %r12
	cmpq	%r13, %rdi
	je	.L1480
	call	_ZdlPv@PLT
.L1480:
	movq	-744(%rbp), %rdi
	movq	%r12, %rsi
.LEHB222:
	call	*%rbx
	testb	%al, %al
	jne	.L1770
	movq	-744(%rbp), %rax
	movq	(%rax), %rax
	movq	16(%rax), %rbx
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_10TypeOracleEEERPNT_5ScopeEv@PLT
.LEHE222:
	movl	$29285, %edi
	movq	%r13, -448(%rbp)
	movw	%di, -428(%rbp)
	movq	%r14, %rdi
	movl	$1651340622, -432(%rbp)
	movq	$6, -440(%rbp)
	movb	$0, -426(%rbp)
.LEHB223:
	call	_ZN2v88internal6torque12Declarations16LookupGlobalTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
.LEHE223:
	movq	-448(%rbp), %rdi
	movq	%rax, %r12
	cmpq	%r13, %rdi
	je	.L1485
	call	_ZdlPv@PLT
.L1485:
	movq	-744(%rbp), %rdi
	movq	%r12, %rsi
.LEHB224:
	call	*%rbx
	testb	%al, %al
	je	.L1489
	movq	-696(%rbp), %rbx
	movl	$18, %edx
	leaq	.LC112(%rip), %rsi
	movq	8(%rbx), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L1718
	.p2align 4,,10
	.p2align 3
.L1745:
	movq	$0, -720(%rbp)
	jmp	.L1409
	.p2align 4,,10
	.p2align 3
.L1743:
	movq	%rbx, %r12
	xorl	%ecx, %ecx
	jmp	.L1399
	.p2align 4,,10
	.p2align 3
.L1740:
	movq	$0, -720(%rbp)
	jmp	.L1389
	.p2align 4,,10
	.p2align 3
.L1759:
	movq	-696(%rbp), %rbx
	movl	$9, %edx
	leaq	.LC83(%rip), %rsi
	movq	8(%rbx), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%rbx), %r12
	movq	-536(%rbp), %r13
	leaq	.LC27(%rip), %r14
	movq	-544(%rbp), %rbx
	cmpq	%r13, %rbx
	je	.L1455
.L1454:
	movq	8(%rbx), %rdx
	movq	(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$32, %rbx
	cmpq	%rbx, %r13
	je	.L1771
	movl	$2, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L1454
	.p2align 4,,10
	.p2align 3
.L1771:
	movq	-696(%rbp), %rax
	movq	8(%rax), %r12
.L1455:
	movl	$4, %edx
	leaq	.LC84(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L1457
	.p2align 4,,10
	.p2align 3
.L1770:
	movq	-696(%rbp), %rbx
	movl	$15, %edx
	leaq	.LC111(%rip), %rsi
	movq	8(%rbx), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L1718:
	movq	8(%rbx), %rdi
	jmp	.L1471
.L1768:
	testq	%rdx, %rdx
	je	.L1468
	movq	-448(%rbp), %rsi
	movq	%r12, %rdi
	call	memcmp@PLT
	testl	%eax, %eax
	sete	%bl
	jmp	.L1468
	.p2align 4,,10
	.p2align 3
.L1760:
	movq	-696(%rbp), %rax
	movq	8(%rcx), %rdx
	movq	(%rcx), %rsi
	movq	8(%rax), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rax, %rdi
	movl	$3, %edx
	leaq	.LC39(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L1457
	.p2align 4,,10
	.p2align 3
.L1472:
	movq	-736(%rbp), %rbx
	leaq	.LC119(%rip), %rsi
	movq	32(%rbx), %rax
	leaq	128(%rax), %rdi
	movq	%rax, -704(%rbp)
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	jne	.L1516
	movq	48(%rbx), %rax
	movq	40(%rbx), %rdx
	movq	%rax, -704(%rbp)
	subq	%rdx, %rax
	cmpq	$8, %rax
	jne	.L1772
	movq	(%rdx), %rbx
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_10TypeOracleEEERPNT_5ScopeEv@PLT
.LEHE224:
	leaq	-448(%rbp), %r14
	leaq	-432(%rbp), %r13
	movabsq	$8386658438684300106, %rax
	movq	$8, -440(%rbp)
	movq	%r14, %rdi
	movq	%r13, -448(%rbp)
	movq	%rax, -432(%rbp)
	movb	$0, -424(%rbp)
.LEHB225:
	call	_ZN2v88internal6torque12Declarations16LookupGlobalTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
.LEHE225:
	movq	-448(%rbp), %rdi
	movq	%rax, %r12
	cmpq	%r13, %rdi
	je	.L1519
	call	_ZdlPv@PLT
.L1519:
	movq	-696(%rbp), %rax
	movq	8(%rax), %rdi
	cmpq	%r12, %rbx
	je	.L1773
	movl	$42, %edx
	leaq	.LC121(%rip), %rsi
.LEHB226:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	168(%rbx), %r8
	movl	$32, %edx
	movq	%r14, %rdi
	xorl	%eax, %eax
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	leaq	.LC0(%rip), %rcx
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0
	movq	-640(%rbp), %rbx
	movq	-448(%rbp), %rax
	movq	(%rbx), %rdi
	cmpq	%r13, %rax
	je	.L1774
	leaq	16(%rbx), %rdx
	cmpq	%rdx, %rdi
	je	.L1775
	movq	%rax, (%rbx)
	movq	-440(%rbp), %rax
	movq	16(%rbx), %rdx
	movq	%rax, 8(%rbx)
	movq	-432(%rbp), %rax
	movq	%rax, 16(%rbx)
	testq	%rdi, %rdi
	je	.L1529
	movq	%rdi, -448(%rbp)
	movq	%rdx, -432(%rbp)
.L1527:
	movq	$0, -440(%rbp)
	movb	$0, (%rdi)
	movq	-448(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L1467
.L1727:
	call	_ZdlPv@PLT
	jmp	.L1467
.L1489:
	movq	-744(%rbp), %rax
	movq	(%rax), %rax
	movq	16(%rax), %rbx
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_10TypeOracleEEERPNT_5ScopeEv@PLT
.LEHE226:
	movl	$26478, %esi
	movq	%r14, %rdi
	movq	%r13, -448(%rbp)
	movl	$1769108563, -432(%rbp)
	movw	%si, -428(%rbp)
	movq	$6, -440(%rbp)
	movb	$0, -426(%rbp)
.LEHB227:
	call	_ZN2v88internal6torque12Declarations16LookupGlobalTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
.LEHE227:
	movq	-448(%rbp), %rdi
	movq	%rax, %r12
	cmpq	%r13, %rdi
	je	.L1490
	call	_ZdlPv@PLT
.L1490:
	movq	-744(%rbp), %rdi
	movq	%r12, %rsi
.LEHB228:
	call	*%rbx
	testb	%al, %al
	jne	.L1776
	movq	-744(%rbp), %rax
	movq	(%rax), %rax
	movq	16(%rax), %rbx
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_10TypeOracleEEERPNT_5ScopeEv@PLT
.LEHE228:
	movl	$29795, %ecx
	movq	%r14, %rdi
	movq	%r13, -448(%rbp)
	movl	$1701470799, -432(%rbp)
	movw	%cx, -428(%rbp)
	movq	$6, -440(%rbp)
	movb	$0, -426(%rbp)
.LEHB229:
	call	_ZN2v88internal6torque12Declarations16LookupGlobalTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
.LEHE229:
	movq	-448(%rbp), %rdi
	movq	%rax, %r12
	cmpq	%r13, %rdi
	je	.L1495
	call	_ZdlPv@PLT
.L1495:
	movq	-744(%rbp), %rdi
	movq	%r12, %rsi
.LEHB230:
	call	*%rbx
	testb	%al, %al
	jne	.L1777
	movq	-744(%rbp), %rax
	movq	(%rax), %rax
	movq	16(%rax), %rbx
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_10TypeOracleEEERPNT_5ScopeEv@PLT
.LEHE230:
	movl	$29300, %edx
	movq	%r14, %rdi
	movq	%r13, -448(%rbp)
	movl	$1886678633, -432(%rbp)
	movw	%dx, -428(%rbp)
	movq	$6, -440(%rbp)
	movb	$0, -426(%rbp)
.LEHB231:
	call	_ZN2v88internal6torque12Declarations16LookupGlobalTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
.LEHE231:
	movq	-448(%rbp), %rdi
	movq	%rax, %r12
	cmpq	%r13, %rdi
	je	.L1501
	call	_ZdlPv@PLT
.L1501:
	movq	-744(%rbp), %rdi
	movq	%r12, %rsi
.LEHB232:
	call	*%rbx
	testb	%al, %al
	je	.L1505
	movq	-696(%rbp), %rbx
	movl	$18, %edx
	leaq	.LC115(%rip), %rsi
	movq	8(%rbx), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L1718
.L1776:
	movq	-696(%rbp), %rbx
	movl	$18, %edx
	leaq	.LC113(%rip), %rsi
	movq	8(%rbx), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L1718
.L1774:
	movq	-440(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L1525
	cmpq	$1, %rdx
	je	.L1778
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	-440(%rbp), %rdx
	movq	(%rbx), %rdi
.L1525:
	movq	%rdx, 8(%rbx)
	movb	$0, (%rdi,%rdx)
	movq	-448(%rbp), %rdi
	jmp	.L1527
.L1516:
	movq	-736(%rbp), %rax
	leaq	.LC123(%rip), %rsi
	movq	32(%rax), %rax
	leaq	128(%rax), %rdi
	movq	%rax, -704(%rbp)
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	jne	.L1531
	movq	-696(%rbp), %rax
	movl	$18, %edx
	leaq	.LC54(%rip), %rsi
	movq	8(%rax), %r12
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-744(%rbp), %rsi
	leaq	-448(%rbp), %rdi
	call	_ZNK2v88internal6torque4Type25GetGeneratedTNodeTypeNameB5cxx11Ev@PLT
.LEHE232:
	movq	-440(%rbp), %rdx
	movq	-448(%rbp), %rsi
	movq	%r12, %rdi
.LEHB233:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rax, %rdi
	movl	$36, %edx
	leaq	.LC124(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE233:
	movq	-448(%rbp), %rdi
	leaq	-432(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L1727
	jmp	.L1467
.L1773:
	movl	$95, %edx
	leaq	.LC122(%rip), %rsi
.LEHB234:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L1467
.L1531:
	movq	-736(%rbp), %rax
	leaq	.LC125(%rip), %rsi
	movq	32(%rax), %rax
	leaq	128(%rax), %rdi
	movq	%rax, -704(%rbp)
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	jne	.L1533
	movq	-696(%rbp), %rbx
	movl	$38, %edx
	leaq	.LC126(%rip), %rsi
	movq	8(%rbx), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L1718
.L1505:
	movq	-744(%rbp), %rax
	movq	(%rax), %rax
	movq	16(%rax), %rbx
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_10TypeOracleEEERPNT_5ScopeEv@PLT
.LEHE234:
	movl	$29808, %eax
	movq	%r14, %rdi
	movq	%r13, -448(%rbp)
	movl	$1953393013, -432(%rbp)
	movw	%ax, -428(%rbp)
	movb	$114, -426(%rbp)
	movq	$7, -440(%rbp)
	movb	$0, -425(%rbp)
.LEHB235:
	call	_ZN2v88internal6torque12Declarations16LookupGlobalTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
.LEHE235:
	movq	-448(%rbp), %rdi
	movq	%rax, %r12
	cmpq	%r13, %rdi
	je	.L1506
	call	_ZdlPv@PLT
.L1506:
	movq	-744(%rbp), %rdi
	movq	%r12, %rsi
.LEHB236:
	call	*%rbx
	testb	%al, %al
	je	.L1510
	movq	-696(%rbp), %rbx
	movl	$19, %edx
	leaq	.LC116(%rip), %rsi
	movq	8(%rbx), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L1718
.L1775:
	movq	%rax, (%rbx)
	movq	-440(%rbp), %rax
	movq	%rax, 8(%rbx)
	movq	-432(%rbp), %rax
	movq	%rax, 16(%rbx)
.L1529:
	movq	%r13, -448(%rbp)
	leaq	-432(%rbp), %r13
	movq	%r13, %rdi
	jmp	.L1527
.L1510:
	movq	-744(%rbp), %rax
	movq	(%rax), %rax
	movq	16(%rax), %rbx
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_10TypeOracleEEERPNT_5ScopeEv@PLT
.LEHE236:
	movq	%r14, %rdi
	movq	%r13, -448(%rbp)
	movl	$863268457, -432(%rbp)
	movb	$50, -428(%rbp)
	movq	$5, -440(%rbp)
	movb	$0, -427(%rbp)
.LEHB237:
	call	_ZN2v88internal6torque12Declarations16LookupGlobalTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
.LEHE237:
	movq	-448(%rbp), %rdi
	movq	%rax, %r12
	cmpq	%r13, %rdi
	je	.L1511
	call	_ZdlPv@PLT
.L1511:
	movq	-744(%rbp), %rdi
	movq	%r12, %rsi
.LEHB238:
	call	*%rbx
	testb	%al, %al
	je	.L1779
	movq	-696(%rbp), %rbx
	movl	$17, %edx
	leaq	.LC117(%rip), %rsi
	movq	8(%rbx), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE238:
	jmp	.L1718
.L1749:
	xorl	%edx, %edx
	movq	$-1, %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
.LEHB239:
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.LEHE239:
.L1750:
	xorl	%edx, %edx
	movq	$-1, %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
.LEHB240:
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.LEHE240:
.L1778:
	movzbl	-432(%rbp), %eax
	movb	%al, (%rdi)
	movq	-440(%rbp), %rdx
	movq	(%rbx), %rdi
	jmp	.L1525
.L1747:
	leaq	.LC6(%rip), %rdi
.LEHB241:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE241:
.L1742:
	leaq	.LC6(%rip), %rdi
.LEHB242:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE242:
.L1755:
	leaq	.LC6(%rip), %rdi
.LEHB243:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE243:
.L1779:
	movq	%r14, %rdi
.LEHB244:
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEC1Ev@PLT
.LEHE244:
	movl	$44, %edx
	leaq	.LC118(%rip), %rsi
	movq	%r13, %rdi
.LEHB245:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-744(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal6torquelsERSoRKNS1_4TypeE
	leaq	-480(%rbp), %r12
	leaq	-424(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNKSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEE3strEv@PLT
.LEHE245:
	movq	%r12, %rdi
.LEHB246:
	call	_ZN2v88internal6torque11ReportErrorIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_
.LEHE246:
.L1777:
	leaq	.LC114(%rip), %rsi
	movq	%r14, %rdi
.LEHB247:
	call	_ZN2v88internal6torqueL7MessageIJRA38_KcEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
.LEHE247:
	movq	%r14, %rdi
.LEHB248:
	call	_ZNK2v88internal6torque14MessageBuilder5ThrowEv@PLT
.LEHE248:
.L1533:
	movq	-736(%rbp), %rax
	leaq	-432(%rbp), %r13
	movq	$0, -440(%rbp)
	leaq	-448(%rbp), %r14
	movq	%r13, -448(%rbp)
	movq	%r14, %rdi
	movq	32(%rax), %rbx
	movb	$0, -432(%rbp)
	movq	136(%rbx), %rax
	leaq	32(%rax), %rsi
.LEHB249:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7reserveEm@PLT
	movabsq	$4611686018427387903, %rax
	subq	-440(%rbp), %rax
	cmpq	$31, %rax
	jbe	.L1780
	movl	$32, %edx
	leaq	.LC127(%rip), %rsi
	movq	%r14, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	136(%rbx), %rdx
	movq	128(%rbx), %rsi
	movq	%r14, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.LEHE249:
	movq	%r14, %rdi
.LEHB250:
	call	_ZN2v88internal6torque11ReportErrorIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_
.LEHE250:
.L1744:
.LEHB251:
	call	_ZSt17__throw_bad_allocv@PLT
.LEHE251:
.L1772:
	leaq	-448(%rbp), %r14
	leaq	.LC120(%rip), %rsi
	movq	%r14, %rdi
.LEHB252:
	call	_ZN2v88internal6torqueL7MessageIJRA38_KcEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
.LEHE252:
	movq	%r14, %rdi
.LEHB253:
	call	_ZNK2v88internal6torque14MessageBuilder5ThrowEv@PLT
.LEHE253:
.L1762:
	movq	-608(%rbp), %rax
	movq	-744(%rbp), %rsi
	leaq	.LC105(%rip), %rdx
	leaq	.LC106(%rip), %rdi
	movq	(%rax), %rcx
.LEHB254:
	call	_ZN2v88internal6torque11ReportErrorIJRA21_KcRKNS1_4TypeERA22_S3_S8_EEEvDpOT_
.LEHE254:
.L1780:
	leaq	.LC55(%rip), %rdi
.LEHB255:
	call	_ZSt20__throw_length_errorPKc@PLT
.LEHE255:
.L1769:
	leaq	-448(%rbp), %r14
	leaq	.LC110(%rip), %rsi
	movq	%r14, %rdi
.LEHB256:
	call	_ZN2v88internal6torqueL7MessageIJRA38_KcEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
.LEHE256:
	movq	%r14, %rdi
.LEHB257:
	call	_ZNK2v88internal6torque14MessageBuilder5ThrowEv@PLT
.LEHE257:
.L1746:
.LEHB258:
	call	_ZSt17__throw_bad_allocv@PLT
.LEHE258:
.L1767:
	call	__stack_chk_fail@PLT
.L1758:
	movq	%r13, %rdi
	jmp	.L1441
.L1761:
	leaq	-448(%rbp), %r14
	leaq	.LC104(%rip), %rsi
	movq	%r14, %rdi
.LEHB259:
	call	_ZN2v88internal6torqueL7MessageIJRA38_KcEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
.LEHE259:
	movq	%r14, %rdi
.LEHB260:
	call	_ZNK2v88internal6torque14MessageBuilder5ThrowEv@PLT
.LEHE260:
.L1741:
.LEHB261:
	call	_ZSt17__throw_bad_allocv@PLT
.LEHE261:
.L1476:
	leaq	-448(%rbp), %r14
	leaq	.LC109(%rip), %rsi
	movq	%r14, %rdi
.LEHB262:
	call	_ZN2v88internal6torqueL7MessageIJRA38_KcEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
.LEHE262:
	movq	%r14, %rdi
.LEHB263:
	call	_ZNK2v88internal6torque14MessageBuilder5ThrowEv@PLT
.LEHE263:
.L1608:
	endbr64
	movq	%rax, %rbx
	jmp	.L1590
.L1612:
	endbr64
	movq	%rax, %rbx
	jmp	.L1590
.L1621:
	endbr64
	movq	%rax, %rbx
	jmp	.L1518
.L1631:
	endbr64
	movq	%rax, %rbx
	jmp	.L1518
.L1619:
	endbr64
	movq	%rax, %rbx
	jmp	.L1518
.L1633:
	endbr64
	movq	%rax, %rbx
	jmp	.L1590
.L1622:
	endbr64
	movq	%rax, %rbx
	jmp	.L1518
.L1610:
	endbr64
	movq	%rax, %rbx
	jmp	.L1585
.L1609:
	endbr64
	movq	%rax, %rbx
	jmp	.L1587
.L1623:
	endbr64
	movq	%rax, %rbx
	jmp	.L1590
.L1613:
	endbr64
	movq	%rax, %rbx
	jmp	.L1592
.L1606:
	endbr64
	movq	%rax, %rbx
	jmp	.L1580
.L1605:
	endbr64
	movq	%rax, %rbx
	jmp	.L1578
.L1601:
	endbr64
	movq	%rax, %rbx
	leaq	-672(%rbp), %rax
	movq	%rax, -752(%rbp)
	leaq	-640(%rbp), %rax
	movq	%rax, -760(%rbp)
	jmp	.L1595
.L1604:
	endbr64
	movq	%rax, %rbx
	jmp	.L1576
.L1628:
	endbr64
	movq	%rax, %rbx
	jmp	.L1590
.L1626:
	endbr64
	movq	%rax, %rbx
	jmp	.L1590
.L1630:
	endbr64
	movq	%rax, %rbx
	jmp	.L1590
.L1629:
	endbr64
	movq	%rax, %rbx
	jmp	.L1590
.L1632:
	endbr64
	movq	%rax, %rbx
	jmp	.L1590
.L1625:
	endbr64
	movq	%rax, %rbx
	jmp	.L1590
.L1611:
	endbr64
	movq	%rax, %rbx
	jmp	.L1592
.L1627:
	endbr64
	movq	%rax, %rbx
	jmp	.L1518
.L1624:
	endbr64
	movq	%rax, %rbx
	jmp	.L1590
.L1618:
	endbr64
	movq	%rax, %rbx
	jmp	.L1432
.L1603:
	endbr64
	movq	%rax, %rbx
	jmp	.L1434
.L1607:
	endbr64
	movq	%rax, %rbx
	jmp	.L1461
.L1617:
	endbr64
	movq	%rax, %rdi
	jmp	.L1419
.L1615:
	endbr64
	movq	%rax, %rdi
	jmp	.L1400
.L1602:
	endbr64
	movq	%rax, %rbx
	jmp	.L1425
.L1620:
	endbr64
	movq	%rax, %rbx
	jmp	.L1590
	.section	.gcc_except_table._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_24CallIntrinsicInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE,"a",@progbits
	.align 4
.LLSDA6925:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATT6925-.LLSDATTD6925
.LLSDATTD6925:
	.byte	0x1
	.uleb128 .LLSDACSE6925-.LLSDACSB6925
.LLSDACSB6925:
	.uleb128 .LEHB201-.LFB6925
	.uleb128 .LEHE201-.LEHB201
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB202-.LFB6925
	.uleb128 .LEHE202-.LEHB202
	.uleb128 .L1615-.LFB6925
	.uleb128 0x1
	.uleb128 .LEHB203-.LFB6925
	.uleb128 .LEHE203-.LEHB203
	.uleb128 .L1601-.LFB6925
	.uleb128 0
	.uleb128 .LEHB204-.LFB6925
	.uleb128 .LEHE204-.LEHB204
	.uleb128 .L1602-.LFB6925
	.uleb128 0
	.uleb128 .LEHB205-.LFB6925
	.uleb128 .LEHE205-.LEHB205
	.uleb128 .L1617-.LFB6925
	.uleb128 0x1
	.uleb128 .LEHB206-.LFB6925
	.uleb128 .LEHE206-.LEHB206
	.uleb128 .L1607-.LFB6925
	.uleb128 0
	.uleb128 .LEHB207-.LFB6925
	.uleb128 .LEHE207-.LEHB207
	.uleb128 .L1603-.LFB6925
	.uleb128 0
	.uleb128 .LEHB208-.LFB6925
	.uleb128 .LEHE208-.LEHB208
	.uleb128 .L1606-.LFB6925
	.uleb128 0
	.uleb128 .LEHB209-.LFB6925
	.uleb128 .LEHE209-.LEHB209
	.uleb128 .L1603-.LFB6925
	.uleb128 0
	.uleb128 .LEHB210-.LFB6925
	.uleb128 .LEHE210-.LEHB210
	.uleb128 .L1618-.LFB6925
	.uleb128 0
	.uleb128 .LEHB211-.LFB6925
	.uleb128 .LEHE211-.LEHB211
	.uleb128 .L1603-.LFB6925
	.uleb128 0
	.uleb128 .LEHB212-.LFB6925
	.uleb128 .LEHE212-.LEHB212
	.uleb128 .L1604-.LFB6925
	.uleb128 0
	.uleb128 .LEHB213-.LFB6925
	.uleb128 .LEHE213-.LEHB213
	.uleb128 .L1605-.LFB6925
	.uleb128 0
	.uleb128 .LEHB214-.LFB6925
	.uleb128 .LEHE214-.LEHB214
	.uleb128 .L1607-.LFB6925
	.uleb128 0
	.uleb128 .LEHB215-.LFB6925
	.uleb128 .LEHE215-.LEHB215
	.uleb128 .L1620-.LFB6925
	.uleb128 0
	.uleb128 .LEHB216-.LFB6925
	.uleb128 .LEHE216-.LEHB216
	.uleb128 .L1607-.LFB6925
	.uleb128 0
	.uleb128 .LEHB217-.LFB6925
	.uleb128 .LEHE217-.LEHB217
	.uleb128 .L1613-.LFB6925
	.uleb128 0
	.uleb128 .LEHB218-.LFB6925
	.uleb128 .LEHE218-.LEHB218
	.uleb128 .L1607-.LFB6925
	.uleb128 0
	.uleb128 .LEHB219-.LFB6925
	.uleb128 .LEHE219-.LEHB219
	.uleb128 .L1608-.LFB6925
	.uleb128 0
	.uleb128 .LEHB220-.LFB6925
	.uleb128 .LEHE220-.LEHB220
	.uleb128 .L1607-.LFB6925
	.uleb128 0
	.uleb128 .LEHB221-.LFB6925
	.uleb128 .LEHE221-.LEHB221
	.uleb128 .L1623-.LFB6925
	.uleb128 0
	.uleb128 .LEHB222-.LFB6925
	.uleb128 .LEHE222-.LEHB222
	.uleb128 .L1607-.LFB6925
	.uleb128 0
	.uleb128 .LEHB223-.LFB6925
	.uleb128 .LEHE223-.LEHB223
	.uleb128 .L1624-.LFB6925
	.uleb128 0
	.uleb128 .LEHB224-.LFB6925
	.uleb128 .LEHE224-.LEHB224
	.uleb128 .L1607-.LFB6925
	.uleb128 0
	.uleb128 .LEHB225-.LFB6925
	.uleb128 .LEHE225-.LEHB225
	.uleb128 .L1632-.LFB6925
	.uleb128 0
	.uleb128 .LEHB226-.LFB6925
	.uleb128 .LEHE226-.LEHB226
	.uleb128 .L1607-.LFB6925
	.uleb128 0
	.uleb128 .LEHB227-.LFB6925
	.uleb128 .LEHE227-.LEHB227
	.uleb128 .L1625-.LFB6925
	.uleb128 0
	.uleb128 .LEHB228-.LFB6925
	.uleb128 .LEHE228-.LEHB228
	.uleb128 .L1607-.LFB6925
	.uleb128 0
	.uleb128 .LEHB229-.LFB6925
	.uleb128 .LEHE229-.LEHB229
	.uleb128 .L1626-.LFB6925
	.uleb128 0
	.uleb128 .LEHB230-.LFB6925
	.uleb128 .LEHE230-.LEHB230
	.uleb128 .L1607-.LFB6925
	.uleb128 0
	.uleb128 .LEHB231-.LFB6925
	.uleb128 .LEHE231-.LEHB231
	.uleb128 .L1628-.LFB6925
	.uleb128 0
	.uleb128 .LEHB232-.LFB6925
	.uleb128 .LEHE232-.LEHB232
	.uleb128 .L1607-.LFB6925
	.uleb128 0
	.uleb128 .LEHB233-.LFB6925
	.uleb128 .LEHE233-.LEHB233
	.uleb128 .L1611-.LFB6925
	.uleb128 0
	.uleb128 .LEHB234-.LFB6925
	.uleb128 .LEHE234-.LEHB234
	.uleb128 .L1607-.LFB6925
	.uleb128 0
	.uleb128 .LEHB235-.LFB6925
	.uleb128 .LEHE235-.LEHB235
	.uleb128 .L1629-.LFB6925
	.uleb128 0
	.uleb128 .LEHB236-.LFB6925
	.uleb128 .LEHE236-.LEHB236
	.uleb128 .L1607-.LFB6925
	.uleb128 0
	.uleb128 .LEHB237-.LFB6925
	.uleb128 .LEHE237-.LEHB237
	.uleb128 .L1630-.LFB6925
	.uleb128 0
	.uleb128 .LEHB238-.LFB6925
	.uleb128 .LEHE238-.LEHB238
	.uleb128 .L1607-.LFB6925
	.uleb128 0
	.uleb128 .LEHB239-.LFB6925
	.uleb128 .LEHE239-.LEHB239
	.uleb128 .L1606-.LFB6925
	.uleb128 0
	.uleb128 .LEHB240-.LFB6925
	.uleb128 .LEHE240-.LEHB240
	.uleb128 .L1603-.LFB6925
	.uleb128 0
	.uleb128 .LEHB241-.LFB6925
	.uleb128 .LEHE241-.LEHB241
	.uleb128 .L1617-.LFB6925
	.uleb128 0x1
	.uleb128 .LEHB242-.LFB6925
	.uleb128 .LEHE242-.LEHB242
	.uleb128 .L1615-.LFB6925
	.uleb128 0x1
	.uleb128 .LEHB243-.LFB6925
	.uleb128 .LEHE243-.LEHB243
	.uleb128 .L1603-.LFB6925
	.uleb128 0
	.uleb128 .LEHB244-.LFB6925
	.uleb128 .LEHE244-.LEHB244
	.uleb128 .L1607-.LFB6925
	.uleb128 0
	.uleb128 .LEHB245-.LFB6925
	.uleb128 .LEHE245-.LEHB245
	.uleb128 .L1609-.LFB6925
	.uleb128 0
	.uleb128 .LEHB246-.LFB6925
	.uleb128 .LEHE246-.LEHB246
	.uleb128 .L1610-.LFB6925
	.uleb128 0
	.uleb128 .LEHB247-.LFB6925
	.uleb128 .LEHE247-.LEHB247
	.uleb128 .L1607-.LFB6925
	.uleb128 0
	.uleb128 .LEHB248-.LFB6925
	.uleb128 .LEHE248-.LEHB248
	.uleb128 .L1627-.LFB6925
	.uleb128 0
	.uleb128 .LEHB249-.LFB6925
	.uleb128 .LEHE249-.LEHB249
	.uleb128 .L1633-.LFB6925
	.uleb128 0
	.uleb128 .LEHB250-.LFB6925
	.uleb128 .LEHE250-.LEHB250
	.uleb128 .L1612-.LFB6925
	.uleb128 0
	.uleb128 .LEHB251-.LFB6925
	.uleb128 .LEHE251-.LEHB251
	.uleb128 .L1601-.LFB6925
	.uleb128 0
	.uleb128 .LEHB252-.LFB6925
	.uleb128 .LEHE252-.LEHB252
	.uleb128 .L1607-.LFB6925
	.uleb128 0
	.uleb128 .LEHB253-.LFB6925
	.uleb128 .LEHE253-.LEHB253
	.uleb128 .L1631-.LFB6925
	.uleb128 0
	.uleb128 .LEHB254-.LFB6925
	.uleb128 .LEHE254-.LEHB254
	.uleb128 .L1607-.LFB6925
	.uleb128 0
	.uleb128 .LEHB255-.LFB6925
	.uleb128 .LEHE255-.LEHB255
	.uleb128 .L1633-.LFB6925
	.uleb128 0
	.uleb128 .LEHB256-.LFB6925
	.uleb128 .LEHE256-.LEHB256
	.uleb128 .L1607-.LFB6925
	.uleb128 0
	.uleb128 .LEHB257-.LFB6925
	.uleb128 .LEHE257-.LEHB257
	.uleb128 .L1622-.LFB6925
	.uleb128 0
	.uleb128 .LEHB258-.LFB6925
	.uleb128 .LEHE258-.LEHB258
	.uleb128 .L1602-.LFB6925
	.uleb128 0
	.uleb128 .LEHB259-.LFB6925
	.uleb128 .LEHE259-.LEHB259
	.uleb128 .L1607-.LFB6925
	.uleb128 0
	.uleb128 .LEHB260-.LFB6925
	.uleb128 .LEHE260-.LEHB260
	.uleb128 .L1619-.LFB6925
	.uleb128 0
	.uleb128 .LEHB261-.LFB6925
	.uleb128 .LEHE261-.LEHB261
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB262-.LFB6925
	.uleb128 .LEHE262-.LEHB262
	.uleb128 .L1607-.LFB6925
	.uleb128 0
	.uleb128 .LEHB263-.LFB6925
	.uleb128 .LEHE263-.LEHB263
	.uleb128 .L1621-.LFB6925
	.uleb128 0
.LLSDACSE6925:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATT6925:
	.section	.text._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_24CallIntrinsicInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_24CallIntrinsicInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6925
	.type	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_24CallIntrinsicInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.cold, @function
_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_24CallIntrinsicInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.cold:
.LFSB6925:
.L1590:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-448(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L1461
.L1721:
	call	_ZdlPv@PLT
	jmp	.L1461
.L1518:
	movq	%r14, %rdi
	call	_ZN2v88internal6torque14MessageBuilderD1Ev
.L1461:
	leaq	-544(%rbp), %rdi
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EED1Ev
	leaq	-576(%rbp), %rdi
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EED1Ev
.L1425:
	movq	-608(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1595
	call	_ZdlPv@PLT
.L1595:
	movq	-760(%rbp), %rdi
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EED1Ev
	movq	-752(%rbp), %rdi
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EED1Ev
	movq	%rbx, %rdi
.LEHB264:
	call	_Unwind_Resume@PLT
.LEHE264:
.L1585:
	movq	-480(%rbp), %rdi
	leaq	-464(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1587
	call	_ZdlPv@PLT
.L1587:
	movq	%r14, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	jmp	.L1461
.L1580:
	movq	-448(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L1434
	call	_ZdlPv@PLT
.L1434:
	movq	-512(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1721
	jmp	.L1461
.L1576:
	movq	-480(%rbp), %rdi
	cmpq	-704(%rbp), %rdi
	je	.L1434
	call	_ZdlPv@PLT
	jmp	.L1434
.L1592:
	movq	-448(%rbp), %rdi
	leaq	-432(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L1721
	jmp	.L1461
.L1578:
	movq	-448(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L1434
	call	_ZdlPv@PLT
	jmp	.L1434
.L1432:
	movq	-448(%rbp), %rdi
	leaq	-432(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1434
	call	_ZdlPv@PLT
	jmp	.L1434
.L1419:
	call	__cxa_begin_catch@PLT
	movq	-720(%rbp), %r12
.L1422:
	cmpq	%rbx, %r12
	jne	.L1781
.LEHB265:
	call	__cxa_rethrow@PLT
.LEHE265:
.L1400:
	call	__cxa_begin_catch@PLT
	movq	-720(%rbp), %r12
.L1403:
	cmpq	%rbx, %r12
	jne	.L1782
.LEHB266:
	call	__cxa_rethrow@PLT
.LEHE266:
.L1781:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1421
	call	_ZdlPv@PLT
.L1421:
	addq	$32, %r12
	jmp	.L1422
.L1782:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1402
	call	_ZdlPv@PLT
.L1402:
	addq	$32, %r12
	jmp	.L1403
.L1616:
	endbr64
	movq	%rax, %rbx
	call	__cxa_end_catch@PLT
	movq	-576(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1425
	call	_ZdlPv@PLT
	jmp	.L1425
.L1614:
	endbr64
	movq	%rax, %r12
	call	__cxa_end_catch@PLT
	movq	-672(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1405
	call	_ZdlPv@PLT
.L1405:
	movq	%r12, %rdi
.LEHB267:
	call	_Unwind_Resume@PLT
.LEHE267:
	.cfi_endproc
.LFE6925:
	.section	.gcc_except_table._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_24CallIntrinsicInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.align 4
.LLSDAC6925:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATTC6925-.LLSDATTDC6925
.LLSDATTDC6925:
	.byte	0x1
	.uleb128 .LLSDACSEC6925-.LLSDACSBC6925
.LLSDACSBC6925:
	.uleb128 .LEHB264-.LCOLDB132
	.uleb128 .LEHE264-.LEHB264
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB265-.LCOLDB132
	.uleb128 .LEHE265-.LEHB265
	.uleb128 .L1616-.LCOLDB132
	.uleb128 0
	.uleb128 .LEHB266-.LCOLDB132
	.uleb128 .LEHE266-.LEHB266
	.uleb128 .L1614-.LCOLDB132
	.uleb128 0
	.uleb128 .LEHB267-.LCOLDB132
	.uleb128 .LEHE267-.LEHB267
	.uleb128 0
	.uleb128 0
.LLSDACSEC6925:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATTC6925:
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_24CallIntrinsicInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.section	.text._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_24CallIntrinsicInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.size	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_24CallIntrinsicInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE, .-_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_24CallIntrinsicInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_24CallIntrinsicInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.size	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_24CallIntrinsicInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.cold, .-_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_24CallIntrinsicInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.cold
.LCOLDE132:
	.section	.text._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_24CallIntrinsicInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
.LHOTE132:
	.section	.rodata._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_24LoadReferenceInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC133:
	.string	" = CodeStubAssembler(state_).LoadReference<"
	.align 8
.LC134:
	.string	">(CodeStubAssembler::Reference{"
	.section	.rodata._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_24LoadReferenceInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.str1.1,"aMS",@progbits,1
.LC135:
	.string	"});\n"
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_24LoadReferenceInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE,"ax",@progbits
	.align 2
.LCOLDB136:
	.section	.text._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_24LoadReferenceInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE,"ax",@progbits
.LHOTB136:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_24LoadReferenceInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.type	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_24LoadReferenceInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE, @function
_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_24LoadReferenceInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE:
.LFB6956:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6956
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-96(%rbp), %r15
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	movl	$32, %edx
	pushq	%rbx
	subq	$248, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -272(%rbp)
	movq	16(%rdi), %r8
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	1(%r8), %rax
	movq	%rax, 16(%rdi)
	movq	%r15, %rdi
	xorl	%eax, %eax
.LEHB268:
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0
.LEHE268:
	movl	$3, %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
	leaq	.LC73(%rip), %rcx
	movq	%r15, %rdi
.LEHB269:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.LEHE269:
	leaq	-208(%rbp), %rbx
	leaq	16(%rax), %rdx
	movq	%rbx, -224(%rbp)
	movq	(%rax), %rcx
	movq	%rbx, -248(%rbp)
	cmpq	%rdx, %rcx
	je	.L1835
	movq	%rcx, -224(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -208(%rbp)
.L1785:
	movq	8(%rax), %rcx
	movb	$0, 16(%rax)
	leaq	-80(%rbp), %rbx
	movq	%rcx, -216(%rbp)
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1786
	call	_ZdlPv@PLT
.L1786:
	movq	8(%r12), %rax
	leaq	-176(%rbp), %rcx
	movq	%rcx, -256(%rbp)
	movq	%rcx, -192(%rbp)
	movq	-32(%rax), %rcx
	leaq	-16(%rax), %rdx
	cmpq	%rdx, %rcx
	je	.L1836
	movq	%rcx, -192(%rbp)
	movq	-16(%rax), %rcx
	movq	%rcx, -176(%rbp)
.L1791:
	movq	-24(%rax), %rcx
	movq	%rcx, -184(%rbp)
	movq	%rdx, -32(%rax)
	movq	$0, -24(%rax)
	movb	$0, -16(%rax)
	movq	8(%r12), %rax
	leaq	-32(%rax), %rdx
	subq	$16, %rax
	movq	%rdx, 8(%r12)
	movq	-16(%rax), %rdi
	cmpq	%rax, %rdi
	je	.L1792
	call	_ZdlPv@PLT
.L1792:
	movq	8(%r12), %rax
	leaq	-144(%rbp), %rsi
	movq	%rsi, -160(%rbp)
	movq	-32(%rax), %rcx
	leaq	-16(%rax), %rdx
	movq	%rsi, -264(%rbp)
	cmpq	%rdx, %rcx
	je	.L1837
	movq	%rcx, -160(%rbp)
	movq	-16(%rax), %rcx
	movq	%rcx, -144(%rbp)
.L1794:
	movq	-24(%rax), %rcx
	movq	%rcx, -152(%rbp)
	movq	%rdx, -32(%rax)
	movq	$0, -24(%rax)
	movb	$0, -16(%rax)
	movq	8(%r12), %rax
	leaq	-32(%rax), %rdx
	subq	$16, %rax
	movq	%rdx, 8(%r12)
	movq	-16(%rax), %rdi
	cmpq	%rax, %rdi
	je	.L1795
	call	_ZdlPv@PLT
.L1795:
	movq	-224(%rbp), %rax
	movq	-216(%rbp), %r13
	movq	%rbx, -96(%rbp)
	movq	%rax, %rcx
	movq	%rax, -280(%rbp)
	addq	%r13, %rcx
	je	.L1796
	testq	%rax, %rax
	je	.L1838
.L1796:
	movq	%r13, -232(%rbp)
	cmpq	$15, %r13
	ja	.L1839
	cmpq	$1, %r13
	jne	.L1799
	movq	-280(%rbp), %rax
	movzbl	(%rax), %eax
	movb	%al, -80(%rbp)
	movq	%rbx, %rax
.L1800:
	movq	%r13, -88(%rbp)
	movb	$0, (%rax,%r13)
	movq	8(%r12), %rsi
	cmpq	16(%r12), %rsi
	je	.L1801
	leaq	16(%rsi), %rax
	movq	%rax, (%rsi)
	movq	-96(%rbp), %rax
	cmpq	%rbx, %rax
	je	.L1840
	movq	%rax, (%rsi)
	movq	-80(%rbp), %rax
	movq	%rax, 16(%rsi)
.L1803:
	movq	-88(%rbp), %rax
	movq	%rax, 8(%rsi)
	addq	$32, 8(%r12)
.L1804:
	movq	8(%r14), %r12
	movl	$4, %edx
	leaq	.LC43(%rip), %rsi
	movq	%r12, %rdi
.LEHB270:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-272(%rbp), %r14
	leaq	-128(%rbp), %rdi
	movq	32(%r14), %rsi
	call	_ZNK2v88internal6torque4Type20GetGeneratedTypeNameB5cxx11Ev@PLT
.LEHE270:
	movq	-120(%rbp), %rdx
	movq	-128(%rbp), %rsi
	movq	%r12, %rdi
.LEHB271:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-216(%rbp), %rdx
	movq	-224(%rbp), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$43, %edx
	leaq	.LC133(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	32(%r14), %rsi
	movq	%r15, %rdi
	call	_ZNK2v88internal6torque4Type25GetGeneratedTNodeTypeNameB5cxx11Ev@PLT
.LEHE271:
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r12, %rdi
.LEHB272:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$31, %edx
	leaq	.LC134(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-152(%rbp), %rdx
	movq	-160(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$2, %edx
	leaq	.LC27(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-184(%rbp), %rdx
	movq	-192(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rax, %rdi
	movl	$4, %edx
	leaq	.LC135(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE272:
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1805
	call	_ZdlPv@PLT
.L1805:
	movq	-128(%rbp), %rdi
	leaq	-112(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1806
	call	_ZdlPv@PLT
.L1806:
	movq	-160(%rbp), %rdi
	cmpq	-264(%rbp), %rdi
	je	.L1807
	call	_ZdlPv@PLT
.L1807:
	movq	-192(%rbp), %rdi
	cmpq	-256(%rbp), %rdi
	je	.L1808
	call	_ZdlPv@PLT
.L1808:
	movq	-224(%rbp), %rdi
	cmpq	-248(%rbp), %rdi
	je	.L1783
	call	_ZdlPv@PLT
.L1783:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1841
	addq	$248, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1799:
	.cfi_restore_state
	testq	%r13, %r13
	jne	.L1842
	movq	%rbx, %rax
	jmp	.L1800
	.p2align 4,,10
	.p2align 3
.L1839:
	leaq	-232(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r15, %rdi
.LEHB273:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE273:
	movq	%rax, -96(%rbp)
	movq	%rax, %rdi
	movq	-232(%rbp), %rax
	movq	%rax, -80(%rbp)
.L1798:
	movq	-280(%rbp), %rsi
	movq	%r13, %rdx
	call	memcpy@PLT
	movq	-232(%rbp), %r13
	movq	-96(%rbp), %rax
	jmp	.L1800
	.p2align 4,,10
	.p2align 3
.L1836:
	movdqu	-16(%rax), %xmm1
	movaps	%xmm1, -176(%rbp)
	jmp	.L1791
	.p2align 4,,10
	.p2align 3
.L1835:
	movdqu	16(%rax), %xmm0
	movaps	%xmm0, -208(%rbp)
	jmp	.L1785
	.p2align 4,,10
	.p2align 3
.L1837:
	movdqu	-16(%rax), %xmm2
	movaps	%xmm2, -144(%rbp)
	jmp	.L1794
	.p2align 4,,10
	.p2align 3
.L1840:
	movdqa	-80(%rbp), %xmm3
	movups	%xmm3, 16(%rsi)
	jmp	.L1803
	.p2align 4,,10
	.p2align 3
.L1801:
	movq	%r15, %rdx
	movq	%r12, %rdi
.LEHB274:
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
.LEHE274:
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1804
	call	_ZdlPv@PLT
	jmp	.L1804
.L1838:
	leaq	.LC6(%rip), %rdi
.LEHB275:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE275:
.L1841:
	call	__stack_chk_fail@PLT
.L1842:
	movq	%rbx, %rdi
	jmp	.L1798
.L1825:
	endbr64
	movq	%rax, %r12
	jmp	.L1813
.L1824:
	endbr64
	movq	%rax, %r12
	jmp	.L1815
.L1822:
	endbr64
	movq	%rax, %r12
	jmp	.L1812
.L1826:
	endbr64
	movq	%rax, %r12
	jmp	.L1789
.L1823:
	endbr64
	movq	%rax, %r12
	jmp	.L1810
	.section	.gcc_except_table._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_24LoadReferenceInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE,"a",@progbits
.LLSDA6956:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6956-.LLSDACSB6956
.LLSDACSB6956:
	.uleb128 .LEHB268-.LFB6956
	.uleb128 .LEHE268-.LEHB268
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB269-.LFB6956
	.uleb128 .LEHE269-.LEHB269
	.uleb128 .L1826-.LFB6956
	.uleb128 0
	.uleb128 .LEHB270-.LFB6956
	.uleb128 .LEHE270-.LEHB270
	.uleb128 .L1822-.LFB6956
	.uleb128 0
	.uleb128 .LEHB271-.LFB6956
	.uleb128 .LEHE271-.LEHB271
	.uleb128 .L1824-.LFB6956
	.uleb128 0
	.uleb128 .LEHB272-.LFB6956
	.uleb128 .LEHE272-.LEHB272
	.uleb128 .L1825-.LFB6956
	.uleb128 0
	.uleb128 .LEHB273-.LFB6956
	.uleb128 .LEHE273-.LEHB273
	.uleb128 .L1822-.LFB6956
	.uleb128 0
	.uleb128 .LEHB274-.LFB6956
	.uleb128 .LEHE274-.LEHB274
	.uleb128 .L1823-.LFB6956
	.uleb128 0
	.uleb128 .LEHB275-.LFB6956
	.uleb128 .LEHE275-.LEHB275
	.uleb128 .L1822-.LFB6956
	.uleb128 0
.LLSDACSE6956:
	.section	.text._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_24LoadReferenceInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_24LoadReferenceInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6956
	.type	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_24LoadReferenceInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.cold, @function
_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_24LoadReferenceInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.cold:
.LFSB6956:
.L1813:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1815
	call	_ZdlPv@PLT
.L1815:
	movq	-128(%rbp), %rdi
	leaq	-112(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1812
	call	_ZdlPv@PLT
.L1812:
	movq	-160(%rbp), %rdi
	cmpq	-264(%rbp), %rdi
	je	.L1817
	call	_ZdlPv@PLT
.L1817:
	movq	-192(%rbp), %rdi
	cmpq	-256(%rbp), %rdi
	je	.L1818
	call	_ZdlPv@PLT
.L1818:
	movq	-224(%rbp), %rdi
	cmpq	-248(%rbp), %rdi
	je	.L1819
	call	_ZdlPv@PLT
.L1819:
	movq	%r12, %rdi
.LEHB276:
	call	_Unwind_Resume@PLT
.L1789:
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1790
	call	_ZdlPv@PLT
.L1790:
	movq	%r12, %rdi
	call	_Unwind_Resume@PLT
.LEHE276:
.L1810:
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1812
	call	_ZdlPv@PLT
	jmp	.L1812
	.cfi_endproc
.LFE6956:
	.section	.gcc_except_table._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_24LoadReferenceInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
.LLSDAC6956:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6956-.LLSDACSBC6956
.LLSDACSBC6956:
	.uleb128 .LEHB276-.LCOLDB136
	.uleb128 .LEHE276-.LEHB276
	.uleb128 0
	.uleb128 0
.LLSDACSEC6956:
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_24LoadReferenceInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.section	.text._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_24LoadReferenceInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.size	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_24LoadReferenceInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE, .-_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_24LoadReferenceInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_24LoadReferenceInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.size	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_24LoadReferenceInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.cold, .-_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_24LoadReferenceInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.cold
.LCOLDE136:
	.section	.text._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_24LoadReferenceInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
.LHOTE136:
	.section	.rodata._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_29CallBuiltinPointerInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC137:
	.string	"builtins must have exactly one result"
	.align 8
.LC138:
	.string	"tail-calls to builtin pointers are not supported"
	.align 8
.LC139:
	.string	"CodeStubAssembler(state_).CallBuiltinPointer(Builtins::CallableFor(ca_.isolate(),ExampleBuiltinForTorqueFunctionPointerType("
	.section	.rodata._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_29CallBuiltinPointerInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.str1.1,"aMS",@progbits,1
.LC140:
	.string	")).descriptor(), "
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_29CallBuiltinPointerInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE,"ax",@progbits
	.align 2
.LCOLDB141:
	.section	.text._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_29CallBuiltinPointerInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE,"ax",@progbits
.LHOTB141:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_29CallBuiltinPointerInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.type	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_29CallBuiltinPointerInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE, @function
_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_29CallBuiltinPointerInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE:
.LFB6942:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6942
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$216, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	40(%rsi), %rax
	movaps	%xmm0, -224(%rbp)
	movq	$0, -208(%rbp)
	movq	%rax, -248(%rbp)
	leaq	1(%rax), %r15
	movabsq	$288230376151711743, %rax
	cmpq	%rax, %r15
	ja	.L1939
	movq	%r15, %rbx
	movq	%rdi, %r13
	movq	%rsi, %r14
	movq	%rdx, %r12
	salq	$5, %rbx
	testq	%r15, %r15
	jne	.L1940
	movq	8(%rdx), %rax
	subq	%rbx, %rax
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L1852:
	movq	(%r12), %rdx
	movq	%rbx, %rax
	subq	%rdx, %rax
	sarq	$5, %rax
	movq	%rax, %rcx
	subq	%r15, %rcx
	movq	%rcx, %r15
	jb	.L1859
	cmpq	%rcx, %rax
	ja	.L1941
.L1861:
	movq	32(%r14), %rax
	leaq	-192(%rbp), %rdi
	movq	96(%rax), %rsi
.LEHB277:
	call	_ZN2v88internal6torque9LowerTypeEPKNS1_4TypeE@PLT
.LEHE277:
	movq	-184(%rbp), %rax
	subq	-192(%rbp), %rax
	cmpq	$8, %rax
	jne	.L1942
	cmpb	$0, 28(%r14)
	jne	.L1943
	movq	16(%r13), %r8
	leaq	-128(%rbp), %r15
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	leaq	.LC0(%rip), %rcx
	movl	$32, %edx
	movq	%r15, %rdi
	leaq	1(%r8), %rax
	movq	%rax, 16(%r13)
	xorl	%eax, %eax
.LEHB278:
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0
.LEHE278:
	movl	$3, %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	leaq	.LC73(%rip), %rcx
.LEHB279:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.LEHE279:
	leaq	-144(%rbp), %rbx
	leaq	16(%rax), %rdx
	movq	%rbx, -160(%rbp)
	movq	(%rax), %rcx
	cmpq	%rdx, %rcx
	je	.L1944
	movq	%rcx, -160(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -144(%rbp)
.L1875:
	movq	8(%rax), %rcx
	movb	$0, 16(%rax)
	movq	%rcx, -152(%rbp)
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movq	-128(%rbp), %rdi
	leaq	-112(%rbp), %rax
	movq	%rax, -240(%rbp)
	cmpq	%rax, %rdi
	je	.L1876
	call	_ZdlPv@PLT
.L1876:
	movq	8(%r12), %rsi
	cmpq	16(%r12), %rsi
	je	.L1945
	leaq	16(%rsi), %rax
	movq	%rax, (%rsi)
	movq	-160(%rbp), %rax
	cmpq	%rbx, %rax
	je	.L1946
	movq	%rax, (%rsi)
	movq	-144(%rbp), %rax
	movq	%rax, 16(%rsi)
.L1882:
	movq	-152(%rbp), %rax
	movq	%rax, 8(%rsi)
	addq	$32, 8(%r12)
.L1883:
	movq	-192(%rbp), %rax
	movq	%r15, %rdi
	movq	(%rax), %rsi
.LEHB280:
	call	_ZNK2v88internal6torque4Type25GetGeneratedTNodeTypeNameB5cxx11Ev@PLT
.LEHE280:
	movq	8(%r13), %rbx
	movl	$20, %edx
	leaq	.LC74(%rip), %rsi
	movq	%rbx, %rdi
.LEHB281:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-120(%rbp), %rdx
	movq	-128(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$2, %edx
	leaq	.LC75(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %rbx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r12), %rdx
	movq	8(%r12), %rax
	subq	%rdx, %rax
	je	.L1947
	leaq	-32(%rdx,%rax), %rax
	movq	%rbx, %rdi
	movq	8(%rax), %rdx
	movq	(%rax), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rax, %rdi
	movl	$3, %edx
	leaq	.LC39(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	.LC76(%rip), %rsi
	movq	%r15, %rdi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	jne	.L1948
.L1885:
	movq	8(%r13), %rbx
	movl	$124, %edx
	leaq	.LC139(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	32(%r14), %rax
	movq	%rbx, %rdi
	movq	104(%rax), %rsi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movq	%rax, %rdi
	movl	$17, %edx
	leaq	.LC140(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-216(%rbp), %rax
	movq	-224(%rbp), %rbx
	movq	8(%r13), %r14
	movq	%rax, -232(%rbp)
	cmpq	%rax, %rbx
	je	.L1887
.L1886:
	movq	8(%rbx), %rdx
	movq	(%rbx), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$32, %rbx
	cmpq	%rbx, -232(%rbp)
	je	.L1949
	movl	$2, %edx
	leaq	.LC27(%rip), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L1886
	.p2align 4,,10
	.p2align 3
.L1949:
	movq	8(%r13), %r14
.L1887:
	movl	$1, %edx
	leaq	.LC57(%rip), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	.LC76(%rip), %rsi
	movq	%r15, %rdi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	jne	.L1950
.L1889:
	movq	8(%r13), %rdi
	movl	$3, %edx
	leaq	.LC79(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%r13), %r13
	movl	$8, %edx
	leaq	.LC80(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r12), %rdx
	movq	8(%r12), %rax
	subq	%rdx, %rax
	je	.L1951
	leaq	-32(%rdx,%rax), %rax
	movq	%r13, %rdi
	movq	8(%rax), %rdx
	movq	(%rax), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rax, %rdi
	movl	$3, %edx
	leaq	.LC3(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-128(%rbp), %rdi
	cmpq	-240(%rbp), %rdi
	je	.L1891
	call	_ZdlPv@PLT
.L1891:
	movq	-192(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1892
	call	_ZdlPv@PLT
.L1892:
	movq	-216(%rbp), %rbx
	movq	-224(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L1893
	.p2align 4,,10
	.p2align 3
.L1897:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1894
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L1897
.L1895:
	movq	-224(%rbp), %r12
.L1893:
	testq	%r12, %r12
	je	.L1843
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1843:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1952
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1894:
	.cfi_restore_state
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L1897
	jmp	.L1895
	.p2align 4,,10
	.p2align 3
.L1950:
	movq	8(%r13), %rdi
	movl	$1, %edx
	leaq	.LC57(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L1889
	.p2align 4,,10
	.p2align 3
.L1948:
	movq	8(%r13), %rdi
	movl	$12, %edx
	leaq	.LC77(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE281:
	jmp	.L1885
	.p2align 4,,10
	.p2align 3
.L1944:
	movdqu	16(%rax), %xmm3
	movaps	%xmm3, -144(%rbp)
	jmp	.L1875
	.p2align 4,,10
	.p2align 3
.L1946:
	movdqa	-144(%rbp), %xmm4
	movups	%xmm4, 16(%rsi)
	jmp	.L1882
	.p2align 4,,10
	.p2align 3
.L1940:
	leaq	-224(%rbp), %rax
	movq	%rbx, %rdi
	movq	%rax, -240(%rbp)
.LEHB282:
	call	_Znwm@PLT
	movq	-224(%rbp), %r9
	movq	-216(%rbp), %rdi
	movq	%r9, %rdx
	cmpq	%r9, %rdi
	je	.L1846
	movq	%rax, %rcx
	jmp	.L1850
	.p2align 4,,10
	.p2align 3
.L1847:
	movq	%rsi, (%rcx)
	movq	16(%rdx), %rsi
	movq	%rsi, 16(%rcx)
.L1938:
	movq	8(%rdx), %rsi
	addq	$32, %rdx
	addq	$32, %rcx
	movq	%rsi, -24(%rcx)
	cmpq	%rdx, %rdi
	je	.L1846
.L1850:
	leaq	16(%rcx), %rsi
	leaq	16(%rdx), %r8
	movq	%rsi, (%rcx)
	movq	(%rdx), %rsi
	cmpq	%r8, %rsi
	jne	.L1847
	movdqu	16(%rdx), %xmm2
	movups	%xmm2, 16(%rcx)
	jmp	.L1938
	.p2align 4,,10
	.p2align 3
.L1846:
	testq	%r9, %r9
	je	.L1851
	movq	%r9, %rdi
	movq	%rax, -232(%rbp)
	call	_ZdlPv@PLT
	movq	-232(%rbp), %rax
.L1851:
	movq	8(%r12), %rdx
	movq	%rax, %xmm0
	leaq	(%rax,%rbx), %rsi
	punpcklqdq	%xmm0, %xmm0
	movq	%rsi, -208(%rbp)
	movq	%rdx, %rcx
	movaps	%xmm0, -224(%rbp)
	subq	%rbx, %rcx
	movq	%rcx, %rbx
	cmpq	%rcx, %rdx
	je	.L1852
	leaq	-224(%rbp), %rcx
	movq	%rcx, -232(%rbp)
	jmp	.L1857
	.p2align 4,,10
	.p2align 3
.L1854:
	movq	%rcx, (%rax)
	movq	16(%rbx), %rcx
	movq	%rcx, 16(%rax)
.L1855:
	movq	8(%rbx), %rcx
	movq	%rcx, 8(%rax)
	movq	%rdx, (%rbx)
	movq	$0, 8(%rbx)
	movb	$0, 16(%rbx)
	addq	$32, -216(%rbp)
.L1856:
	addq	$32, %rbx
	cmpq	%rbx, 8(%r12)
	je	.L1852
	movq	-216(%rbp), %rax
	movq	-208(%rbp), %rsi
.L1857:
	cmpq	%rax, %rsi
	je	.L1853
	leaq	16(%rax), %rdx
	movq	%rdx, (%rax)
	movq	(%rbx), %rcx
	leaq	16(%rbx), %rdx
	cmpq	%rdx, %rcx
	jne	.L1854
	movdqu	16(%rbx), %xmm1
	movups	%xmm1, 16(%rax)
	jmp	.L1855
	.p2align 4,,10
	.p2align 3
.L1853:
	movq	-232(%rbp), %rdi
	movq	%rbx, %rdx
	movq	%rdi, -240(%rbp)
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
.LEHE282:
	jmp	.L1856
	.p2align 4,,10
	.p2align 3
.L1941:
	salq	$5, %r15
	leaq	(%rdx,%r15), %rax
	movq	%rax, -232(%rbp)
	cmpq	%rbx, %rax
	je	.L1861
	movq	%rax, %r15
	.p2align 4,,10
	.p2align 3
.L1866:
	movq	(%r15), %rdi
	leaq	16(%r15), %rdx
	cmpq	%rdx, %rdi
	je	.L1863
	call	_ZdlPv@PLT
	addq	$32, %r15
	cmpq	%rbx, %r15
	jne	.L1866
.L1864:
	movq	-232(%rbp), %rax
	movq	%rax, 8(%r12)
	jmp	.L1861
	.p2align 4,,10
	.p2align 3
.L1863:
	addq	$32, %r15
	cmpq	%rbx, %r15
	jne	.L1866
	jmp	.L1864
	.p2align 4,,10
	.p2align 3
.L1945:
	leaq	-160(%rbp), %rdx
	movq	%r12, %rdi
.LEHB283:
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
.LEHE283:
	movq	-160(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1883
	call	_ZdlPv@PLT
	jmp	.L1883
.L1859:
	movq	-248(%rbp), %rsi
	leaq	-224(%rbp), %rax
	movq	%r12, %rdi
	movq	%rax, -240(%rbp)
	notq	%rsi
.LEHB284:
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_default_appendEm
	jmp	.L1861
.L1939:
	leaq	-224(%rbp), %rax
	leaq	.LC70(%rip), %rdi
	movq	%rax, -240(%rbp)
	call	_ZSt20__throw_length_errorPKc@PLT
.LEHE284:
.L1942:
	leaq	-128(%rbp), %r15
	leaq	.LC137(%rip), %rsi
	movq	%r15, %rdi
.LEHB285:
	call	_ZN2v88internal6torqueL7MessageIJRA38_KcEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
.LEHE285:
	movq	%r15, %rdi
.LEHB286:
	call	_ZNK2v88internal6torque14MessageBuilder5ThrowEv@PLT
.LEHE286:
.L1943:
	leaq	-128(%rbp), %r15
	leaq	.LC138(%rip), %rsi
	movq	%r15, %rdi
.LEHB287:
	call	_ZN2v88internal6torqueL7MessageIJRA38_KcEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
.LEHE287:
	movq	%r15, %rdi
.LEHB288:
	call	_ZNK2v88internal6torque14MessageBuilder5ThrowEv@PLT
.LEHE288:
.L1951:
	xorl	%edx, %edx
	orq	$-1, %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
.LEHB289:
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L1952:
	call	__stack_chk_fail@PLT
.L1947:
	xorl	%edx, %edx
	orq	$-1, %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.LEHE289:
.L1911:
	endbr64
	movq	%rax, %rbx
	jmp	.L1870
.L1912:
	endbr64
	movq	%rax, %rbx
	jmp	.L1873
.L1910:
	endbr64
	movq	%rax, %r12
	jmp	.L1868
.L1909:
	endbr64
	movq	%rax, %rbx
	jmp	.L1901
.L1908:
	endbr64
	movq	%rax, %r12
	jmp	.L1899
.L1906:
	endbr64
	movq	%rax, %rbx
	jmp	.L1904
.L1913:
	endbr64
	movq	%rax, %rbx
	jmp	.L1879
.L1907:
	endbr64
	movq	%rax, %rbx
	jmp	.L1871
	.section	.gcc_except_table._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_29CallBuiltinPointerInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE,"a",@progbits
.LLSDA6942:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6942-.LLSDACSB6942
.LLSDACSB6942:
	.uleb128 .LEHB277-.LFB6942
	.uleb128 .LEHE277-.LEHB277
	.uleb128 .L1906-.LFB6942
	.uleb128 0
	.uleb128 .LEHB278-.LFB6942
	.uleb128 .LEHE278-.LEHB278
	.uleb128 .L1907-.LFB6942
	.uleb128 0
	.uleb128 .LEHB279-.LFB6942
	.uleb128 .LEHE279-.LEHB279
	.uleb128 .L1913-.LFB6942
	.uleb128 0
	.uleb128 .LEHB280-.LFB6942
	.uleb128 .LEHE280-.LEHB280
	.uleb128 .L1907-.LFB6942
	.uleb128 0
	.uleb128 .LEHB281-.LFB6942
	.uleb128 .LEHE281-.LEHB281
	.uleb128 .L1909-.LFB6942
	.uleb128 0
	.uleb128 .LEHB282-.LFB6942
	.uleb128 .LEHE282-.LEHB282
	.uleb128 .L1910-.LFB6942
	.uleb128 0
	.uleb128 .LEHB283-.LFB6942
	.uleb128 .LEHE283-.LEHB283
	.uleb128 .L1908-.LFB6942
	.uleb128 0
	.uleb128 .LEHB284-.LFB6942
	.uleb128 .LEHE284-.LEHB284
	.uleb128 .L1910-.LFB6942
	.uleb128 0
	.uleb128 .LEHB285-.LFB6942
	.uleb128 .LEHE285-.LEHB285
	.uleb128 .L1907-.LFB6942
	.uleb128 0
	.uleb128 .LEHB286-.LFB6942
	.uleb128 .LEHE286-.LEHB286
	.uleb128 .L1911-.LFB6942
	.uleb128 0
	.uleb128 .LEHB287-.LFB6942
	.uleb128 .LEHE287-.LEHB287
	.uleb128 .L1907-.LFB6942
	.uleb128 0
	.uleb128 .LEHB288-.LFB6942
	.uleb128 .LEHE288-.LEHB288
	.uleb128 .L1912-.LFB6942
	.uleb128 0
	.uleb128 .LEHB289-.LFB6942
	.uleb128 .LEHE289-.LEHB289
	.uleb128 .L1909-.LFB6942
	.uleb128 0
.LLSDACSE6942:
	.section	.text._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_29CallBuiltinPointerInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_29CallBuiltinPointerInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6942
	.type	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_29CallBuiltinPointerInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.cold, @function
_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_29CallBuiltinPointerInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.cold:
.LFSB6942:
.L1870:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	%r15, %rdi
	call	_ZN2v88internal6torque14MessageBuilderD1Ev
.L1871:
	movq	-192(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1904
	call	_ZdlPv@PLT
.L1904:
	leaq	-224(%rbp), %rdi
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EED1Ev
	movq	%rbx, %rdi
.LEHB290:
	call	_Unwind_Resume@PLT
.L1873:
	movq	%r15, %rdi
	call	_ZN2v88internal6torque14MessageBuilderD1Ev
	jmp	.L1871
.L1868:
	movq	-240(%rbp), %rdi
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EED1Ev
	movq	%r12, %rdi
	call	_Unwind_Resume@PLT
.LEHE290:
.L1901:
	movq	-128(%rbp), %rdi
	cmpq	-240(%rbp), %rdi
	je	.L1871
	call	_ZdlPv@PLT
	jmp	.L1871
.L1879:
	movq	-128(%rbp), %rdi
	leaq	-112(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1871
	call	_ZdlPv@PLT
	jmp	.L1871
.L1899:
	movq	-160(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1900
	call	_ZdlPv@PLT
.L1900:
	movq	%r12, %rbx
	jmp	.L1871
	.cfi_endproc
.LFE6942:
	.section	.gcc_except_table._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_29CallBuiltinPointerInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
.LLSDAC6942:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6942-.LLSDACSBC6942
.LLSDACSBC6942:
	.uleb128 .LEHB290-.LCOLDB141
	.uleb128 .LEHE290-.LEHB290
	.uleb128 0
	.uleb128 0
.LLSDACSEC6942:
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_29CallBuiltinPointerInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.section	.text._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_29CallBuiltinPointerInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.size	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_29CallBuiltinPointerInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE, .-_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_29CallBuiltinPointerInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_29CallBuiltinPointerInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.size	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_29CallBuiltinPointerInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.cold, .-_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_29CallBuiltinPointerInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.cold
.LCOLDE141:
	.section	.text._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_29CallBuiltinPointerInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
.LHOTE141:
	.section	.rodata._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_31CreateFieldReferenceInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC142:
	.string	" which does not inherit from a class type"
	.align 8
.LC143:
	.string	"Cannot create field reference of type "
	.section	.rodata._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_31CreateFieldReferenceInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.str1.1,"aMS",@progbits,1
.LC144:
	.string	"    compiler::TNode<IntPtrT> "
.LC145:
	.string	" = ca_.IntPtrConstant("
.LC146:
	.string	"::k"
.LC147:
	.string	"Offset"
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_31CreateFieldReferenceInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE,"ax",@progbits
	.align 2
.LCOLDB148:
	.section	.text._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_31CreateFieldReferenceInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE,"ax",@progbits
.LHOTB148:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_31CreateFieldReferenceInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.type	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_31CreateFieldReferenceInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE, @function
_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_31CreateFieldReferenceInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE:
.LFB6954:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6954
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$168, %rsp
	movq	32(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
.LEHB291:
	call	_ZNK2v88internal6torque4Type14ClassSupertypeEv@PLT
	testb	%al, %al
	je	.L1996
	movq	%rdx, %rdi
	leaq	40(%r13), %rsi
	leaq	-96(%rbp), %r15
	call	_ZNK2v88internal6torque13AggregateType11LookupFieldERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	16(%r12), %r8
	movl	$32, %edx
	movq	%r15, %rdi
	movq	%rax, -184(%rbp)
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	leaq	.LC0(%rip), %rcx
	leaq	1(%r8), %rax
	movq	%rax, 16(%r12)
	xorl	%eax, %eax
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0
.LEHE291:
	movl	$3, %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
	leaq	.LC73(%rip), %rcx
	movq	%r15, %rdi
.LEHB292:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.LEHE292:
	leaq	-144(%rbp), %rcx
	leaq	16(%rax), %rdx
	movq	%rcx, -192(%rbp)
	movq	%rcx, -160(%rbp)
	movq	(%rax), %rcx
	cmpq	%rdx, %rcx
	je	.L1997
	movq	%rcx, -160(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -144(%rbp)
.L1956:
	movq	8(%rax), %rcx
	movb	$0, 16(%rax)
	leaq	-80(%rbp), %r14
	movq	%rcx, -152(%rbp)
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L1957
	call	_ZdlPv@PLT
.L1957:
	movq	-160(%rbp), %rax
	movq	-152(%rbp), %r13
	movq	%r14, -96(%rbp)
	movq	%rax, %rcx
	movq	%rax, -200(%rbp)
	addq	%r13, %rcx
	je	.L1960
	testq	%rax, %rax
	je	.L1998
.L1960:
	movq	%r13, -168(%rbp)
	cmpq	$15, %r13
	ja	.L1999
	cmpq	$1, %r13
	jne	.L1965
	movq	-200(%rbp), %rax
	movzbl	(%rax), %eax
	movb	%al, -80(%rbp)
	movq	%r14, %rax
.L1966:
	movq	%r13, -88(%rbp)
	movb	$0, (%rax,%r13)
	movq	8(%rbx), %rsi
	cmpq	16(%rbx), %rsi
	je	.L1967
	leaq	16(%rsi), %rax
	movq	%rax, (%rsi)
	movq	-96(%rbp), %rax
	cmpq	%r14, %rax
	je	.L2000
	movq	%rax, (%rsi)
	movq	-80(%rbp), %rax
	movq	%rax, 16(%rsi)
.L1969:
	movq	-88(%rbp), %rax
	movq	%rax, 8(%rsi)
	addq	$32, 8(%rbx)
.L1970:
	movq	8(%r12), %r13
	movl	$29, %edx
	leaq	.LC144(%rip), %rsi
	movq	%r13, %rdi
.LEHB293:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-152(%rbp), %rdx
	movq	-160(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rax, %rdi
	movl	$22, %edx
	leaq	.LC145(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-184(%rbp), %rax
	leaq	-128(%rbp), %rdi
	movq	8(%r12), %r13
	movq	24(%rax), %rsi
	call	_ZNK2v88internal6torque4Type25GetGeneratedTNodeTypeNameB5cxx11Ev@PLT
.LEHE293:
	movq	-120(%rbp), %rdx
	movq	-128(%rbp), %rsi
	movq	%r13, %rdi
.LEHB294:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$3, %edx
	leaq	.LC146(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-184(%rbp), %rsi
	movq	%r15, %rdi
	addq	$48, %rsi
	call	_ZN2v88internal6torque14CamelifyStringERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
.LEHE294:
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r13, %rdi
.LEHB295:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rax, %rdi
	movl	$6, %edx
	leaq	.LC147(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE295:
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L1971
	call	_ZdlPv@PLT
.L1971:
	movq	-128(%rbp), %rdi
	leaq	-112(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1972
	call	_ZdlPv@PLT
.L1972:
	movq	8(%r12), %r12
	movl	$3, %edx
	leaq	.LC3(%rip), %rsi
	movq	%r12, %rdi
.LEHB296:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$8, %edx
	leaq	.LC80(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rdx
	movq	8(%rbx), %rax
	subq	%rdx, %rax
	je	.L2001
	leaq	-32(%rdx,%rax), %rax
	movq	%r12, %rdi
	movq	8(%rax), %rdx
	movq	(%rax), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rax, %rdi
	movl	$3, %edx
	leaq	.LC3(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-160(%rbp), %rdi
	cmpq	-192(%rbp), %rdi
	je	.L1974
	call	_ZdlPv@PLT
.L1974:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2002
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1965:
	.cfi_restore_state
	testq	%r13, %r13
	jne	.L2003
	movq	%r14, %rax
	jmp	.L1966
	.p2align 4,,10
	.p2align 3
.L1999:
	leaq	-168(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE296:
	movq	%rax, -96(%rbp)
	movq	%rax, %rdi
	movq	-168(%rbp), %rax
	movq	%rax, -80(%rbp)
.L1964:
	movq	-200(%rbp), %rsi
	movq	%r13, %rdx
	call	memcpy@PLT
	movq	-168(%rbp), %r13
	movq	-96(%rbp), %rax
	jmp	.L1966
	.p2align 4,,10
	.p2align 3
.L1997:
	movdqu	16(%rax), %xmm0
	movaps	%xmm0, -144(%rbp)
	jmp	.L1956
	.p2align 4,,10
	.p2align 3
.L2000:
	movdqa	-80(%rbp), %xmm1
	movups	%xmm1, 16(%rsi)
	jmp	.L1969
	.p2align 4,,10
	.p2align 3
.L1967:
	movq	%r15, %rdx
	movq	%rbx, %rdi
.LEHB297:
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
.LEHE297:
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L1970
	call	_ZdlPv@PLT
	jmp	.L1970
.L1998:
	leaq	.LC6(%rip), %rdi
.LEHB298:
	call	_ZSt19__throw_logic_errorPKc@PLT
.L2001:
	xorl	%edx, %edx
	orq	$-1, %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.LEHE298:
.L2002:
	call	__stack_chk_fail@PLT
.L1996:
	leaq	32(%r13), %rsi
	leaq	.LC142(%rip), %rdx
	leaq	.LC143(%rip), %rdi
.LEHB299:
	call	_ZN2v88internal6torque11ReportErrorIJRA39_KcRKPKNS1_4TypeERA42_S3_EEEvDpOT_
.LEHE299:
.L2003:
	movq	%r14, %rdi
	jmp	.L1964
.L1989:
	endbr64
	movq	%rax, %r12
	jmp	.L1961
.L1987:
	endbr64
	movq	%rax, %r12
	jmp	.L1980
.L1988:
	endbr64
	movq	%rax, %r12
	jmp	.L1978
.L1986:
	endbr64
	movq	%rax, %r12
	jmp	.L1975
.L1985:
	endbr64
	movq	%rax, %r12
	jmp	.L1977
	.section	.gcc_except_table._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_31CreateFieldReferenceInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE,"a",@progbits
.LLSDA6954:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6954-.LLSDACSB6954
.LLSDACSB6954:
	.uleb128 .LEHB291-.LFB6954
	.uleb128 .LEHE291-.LEHB291
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB292-.LFB6954
	.uleb128 .LEHE292-.LEHB292
	.uleb128 .L1989-.LFB6954
	.uleb128 0
	.uleb128 .LEHB293-.LFB6954
	.uleb128 .LEHE293-.LEHB293
	.uleb128 .L1985-.LFB6954
	.uleb128 0
	.uleb128 .LEHB294-.LFB6954
	.uleb128 .LEHE294-.LEHB294
	.uleb128 .L1987-.LFB6954
	.uleb128 0
	.uleb128 .LEHB295-.LFB6954
	.uleb128 .LEHE295-.LEHB295
	.uleb128 .L1988-.LFB6954
	.uleb128 0
	.uleb128 .LEHB296-.LFB6954
	.uleb128 .LEHE296-.LEHB296
	.uleb128 .L1985-.LFB6954
	.uleb128 0
	.uleb128 .LEHB297-.LFB6954
	.uleb128 .LEHE297-.LEHB297
	.uleb128 .L1986-.LFB6954
	.uleb128 0
	.uleb128 .LEHB298-.LFB6954
	.uleb128 .LEHE298-.LEHB298
	.uleb128 .L1985-.LFB6954
	.uleb128 0
	.uleb128 .LEHB299-.LFB6954
	.uleb128 .LEHE299-.LEHB299
	.uleb128 0
	.uleb128 0
.LLSDACSE6954:
	.section	.text._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_31CreateFieldReferenceInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_31CreateFieldReferenceInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6954
	.type	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_31CreateFieldReferenceInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.cold, @function
_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_31CreateFieldReferenceInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.cold:
.LFSB6954:
.L1961:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1982
.L1995:
	call	_ZdlPv@PLT
.L1982:
	movq	%r12, %rdi
.LEHB300:
	call	_Unwind_Resume@PLT
.LEHE300:
.L1978:
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L1980
	call	_ZdlPv@PLT
.L1980:
	movq	-128(%rbp), %rdi
	leaq	-112(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1977
	call	_ZdlPv@PLT
.L1977:
	movq	-160(%rbp), %rdi
	cmpq	-192(%rbp), %rdi
	jne	.L1995
	jmp	.L1982
.L1975:
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L1977
	call	_ZdlPv@PLT
	jmp	.L1977
	.cfi_endproc
.LFE6954:
	.section	.gcc_except_table._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_31CreateFieldReferenceInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
.LLSDAC6954:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6954-.LLSDACSBC6954
.LLSDACSBC6954:
	.uleb128 .LEHB300-.LCOLDB148
	.uleb128 .LEHE300-.LEHB300
	.uleb128 0
	.uleb128 0
.LLSDACSEC6954:
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_31CreateFieldReferenceInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.section	.text._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_31CreateFieldReferenceInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.size	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_31CreateFieldReferenceInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE, .-_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_31CreateFieldReferenceInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_31CreateFieldReferenceInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.size	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_31CreateFieldReferenceInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.cold, .-_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_31CreateFieldReferenceInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.cold
.LCOLDE148:
	.section	.text._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_31CreateFieldReferenceInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
.LHOTE148:
	.section	.rodata._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_22CallBuiltinInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC149:
	.string	"   CodeStubAssembler(state_).TailCallBuiltin(Builtins::k"
	.align 8
.LC150:
	.string	"CodeStubAssembler(state_).CallBuiltin(Builtins::k"
	.align 8
.LC151:
	.string	"    CodeStubAssembler(state_).CallBuiltin(Builtins::k"
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_22CallBuiltinInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE,"ax",@progbits
	.align 2
.LCOLDB152:
	.section	.text._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_22CallBuiltinInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE,"ax",@progbits
.LHOTB152:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_22CallBuiltinInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.type	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_22CallBuiltinInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE, @function
_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_22CallBuiltinInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE:
.LFB6941:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6941
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$344, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -320(%rbp)
	movq	40(%rsi), %r12
	movq	%rdx, -312(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -288(%rbp)
	movabsq	$288230376151711743, %rax
	movq	$0, -272(%rbp)
	cmpq	%rax, %r12
	ja	.L2175
	movq	%r12, %rbx
	movq	%rsi, %r15
	salq	$5, %rbx
	testq	%r12, %r12
	jne	.L2176
	movq	8(%rdx), %rax
	movq	%rax, -328(%rbp)
	subq	%rbx, %rax
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L2013:
	movq	-312(%rbp), %rax
	movq	(%rax), %rcx
	movq	%rbx, %rax
	subq	%rcx, %rax
	sarq	$5, %rax
	movq	%rax, %rdx
	subq	%r12, %rdx
	jb	.L2020
	cmpq	%rdx, %rax
	ja	.L2177
.L2022:
	movq	32(%r15), %rax
	leaq	-256(%rbp), %rdi
	movq	296(%rax), %rsi
.LEHB301:
	call	_ZN2v88internal6torque9LowerTypeEPKNS1_4TypeE@PLT
.LEHE301:
	cmpb	$0, 28(%r15)
	jne	.L2178
	movq	-320(%rbp), %rcx
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	movl	$32, %edx
	movq	16(%rcx), %r8
	leaq	1(%r8), %rax
	movq	%rax, 16(%rcx)
	leaq	-96(%rbp), %rax
	leaq	.LC0(%rip), %rcx
	movq	%rax, -376(%rbp)
	movq	%rax, %rbx
	movq	%rax, %rdi
	xorl	%eax, %eax
.LEHB302:
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0
.LEHE302:
	movl	$3, %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	leaq	.LC73(%rip), %rcx
.LEHB303:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.LEHE303:
	leaq	-176(%rbp), %rcx
	leaq	16(%rax), %rdx
	movq	%rcx, -360(%rbp)
	movq	%rcx, -192(%rbp)
	movq	(%rax), %rcx
	cmpq	%rdx, %rcx
	je	.L2179
	movq	%rcx, -192(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -176(%rbp)
.L2036:
	movq	8(%rax), %rcx
	movb	$0, 16(%rax)
	movq	%rcx, -184(%rbp)
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	movq	%rax, -344(%rbp)
	cmpq	%rax, %rdi
	je	.L2037
	call	_ZdlPv@PLT
.L2037:
	movq	-248(%rbp), %rax
	subq	-256(%rbp), %rax
	cmpq	$8, %rax
	je	.L2180
.L2039:
	leaq	-160(%rbp), %rax
	movl	48(%r15), %edx
	movq	56(%r15), %rcx
	movq	-320(%rbp), %rsi
	movq	%rax, %rdi
	movq	%rax, -368(%rbp)
.LEHB304:
	call	_ZN2v88internal6torque12CSAGenerator31PreCallableExceptionPreparationB5cxx11ENS_4base8OptionalIPNS1_5BlockEEE
.LEHE304:
	movq	-312(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	$0, -208(%rbp)
	movaps	%xmm0, -224(%rbp)
	movq	(%rax), %r14
	movq	8(%rax), %rax
	movq	%rax, %rbx
	movq	%rax, -328(%rbp)
	subq	%r14, %rbx
	movq	%rbx, %rax
	sarq	$5, %rax
	je	.L2181
	movabsq	$288230376151711743, %rdx
	cmpq	%rdx, %rax
	ja	.L2182
	movq	%rbx, %rdi
.LEHB305:
	call	_Znwm@PLT
.LEHE305:
	movq	%rax, -352(%rbp)
	movq	-312(%rbp), %rax
	movq	8(%rax), %rcx
	movq	(%rax), %r14
	movq	%rcx, -328(%rbp)
.L2045:
	movq	-352(%rbp), %rax
	movq	%rax, %xmm0
	addq	%rax, %rbx
	punpcklqdq	%xmm0, %xmm0
	movq	%rbx, -208(%rbp)
	movq	%rax, %rbx
	movaps	%xmm0, -224(%rbp)
	cmpq	-328(%rbp), %r14
	je	.L2047
	leaq	-296(%rbp), %rax
	movq	%rax, -336(%rbp)
	jmp	.L2053
	.p2align 4,,10
	.p2align 3
.L2049:
	cmpq	$1, %r12
	jne	.L2051
	movzbl	0(%r13), %eax
	movb	%al, 16(%rbx)
.L2052:
	movq	%r12, 8(%rbx)
	addq	$32, %r14
	addq	$32, %rbx
	movb	$0, (%rdi,%r12)
	cmpq	%r14, -328(%rbp)
	je	.L2047
.L2053:
	leaq	16(%rbx), %rdi
	movq	%rdi, (%rbx)
	movq	(%r14), %r13
	movq	8(%r14), %r12
	movq	%r13, %rax
	addq	%r12, %rax
	je	.L2048
	testq	%r13, %r13
	je	.L2183
.L2048:
	movq	%r12, -296(%rbp)
	cmpq	$15, %r12
	jbe	.L2049
	movq	-336(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rbx, %rdi
.LEHB306:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE306:
	movq	%rax, (%rbx)
	movq	%rax, %rdi
	movq	-296(%rbp), %rax
	movq	%rax, 16(%rbx)
.L2050:
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	-296(%rbp), %r12
	movq	(%rbx), %rdi
	jmp	.L2052
	.p2align 4,,10
	.p2align 3
.L2178:
	movq	-320(%rbp), %rbx
	movl	$56, %edx
	leaq	.LC149(%rip), %rsi
	movq	8(%rbx), %r12
	movq	%r12, %rdi
.LEHB307:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	32(%r15), %rax
	movq	%r12, %rdi
	movq	136(%rax), %rdx
	movq	128(%rax), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rax, %rdi
	movl	$2, %edx
	leaq	.LC27(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%rbx), %r12
	movq	-280(%rbp), %r13
	leaq	.LC27(%rip), %r14
	movq	-288(%rbp), %rbx
	cmpq	%r13, %rbx
	je	.L2032
.L2031:
	movq	8(%rbx), %rdx
	movq	(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$32, %rbx
	cmpq	%rbx, %r13
	je	.L2184
	movl	$2, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L2031
	.p2align 4,,10
	.p2align 3
.L2051:
	testq	%r12, %r12
	je	.L2052
	jmp	.L2050
	.p2align 4,,10
	.p2align 3
.L2184:
	movq	-320(%rbp), %rax
	movq	8(%rax), %r12
.L2032:
	movl	$3, %edx
	leaq	.LC3(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE307:
.L2034:
	movq	-256(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2097
	call	_ZdlPv@PLT
.L2097:
	movq	-280(%rbp), %rbx
	movq	-288(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L2098
	.p2align 4,,10
	.p2align 3
.L2102:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2099
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%rbx, %r12
	jne	.L2102
.L2100:
	movq	-288(%rbp), %r12
.L2098:
	testq	%r12, %r12
	je	.L2004
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L2004:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2185
	addq	$344, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2099:
	.cfi_restore_state
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L2102
	jmp	.L2100
	.p2align 4,,10
	.p2align 3
.L2047:
	movq	-256(%rbp), %rdx
	movq	-248(%rbp), %rax
	movq	%rbx, -216(%rbp)
	subq	%rdx, %rax
	cmpq	$8, %rax
	je	.L2186
	movq	-320(%rbp), %rbx
	movl	$53, %edx
	leaq	.LC151(%rip), %rsi
	leaq	-224(%rbp), %r12
	movq	8(%rbx), %r13
	movq	%r13, %rdi
.LEHB308:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	32(%r15), %rax
	movq	%r13, %rdi
	movq	136(%rax), %rdx
	movq	128(%rax), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rax, %rdi
	movl	$2, %edx
	leaq	.LC27(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%rbx), %r13
	movq	-280(%rbp), %rax
	movq	%r12, %r14
	movq	-288(%rbp), %rbx
	movq	%rax, -312(%rbp)
	cmpq	%rax, %rbx
	je	.L2081
.L2080:
	movq	8(%rbx), %rdx
	movq	(%rbx), %rsi
	movq	%r13, %rdi
	movq	%r14, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$32, %rbx
	cmpq	%rbx, -312(%rbp)
	je	.L2187
	movl	$2, %edx
	leaq	.LC27(%rip), %rsi
	movq	%r13, %rdi
	movq	%r14, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L2080
	.p2align 4,,10
	.p2align 3
.L2187:
	movq	-320(%rbp), %rax
	movq	8(%rax), %r13
.L2081:
	movl	$3, %edx
	leaq	.LC3(%rip), %rsi
	movq	%r13, %rdi
	leaq	-224(%rbp), %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L2079:
	movq	-256(%rbp), %rax
	cmpq	-248(%rbp), %rax
	je	.L2188
	movq	(%rax), %rdx
.L2085:
	leaq	-224(%rbp), %r12
	movl	48(%r15), %ecx
	movq	56(%r15), %r8
	movq	-368(%rbp), %rsi
	movq	-320(%rbp), %rdi
	movq	%r12, %r9
	call	_ZN2v88internal6torque12CSAGenerator32PostCallableExceptionPreparationERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKNS1_4TypeENS_4base8OptionalIPNS1_5BlockEEEPNS1_5StackIS8_EE
.LEHE308:
	movq	-216(%rbp), %rbx
	movq	-224(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L2089
	.p2align 4,,10
	.p2align 3
.L2093:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2090
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L2093
.L2091:
	movq	-224(%rbp), %r12
.L2089:
	testq	%r12, %r12
	je	.L2094
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L2094:
	movq	-160(%rbp), %rdi
	leaq	-144(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2095
	call	_ZdlPv@PLT
.L2095:
	movq	-192(%rbp), %rdi
	cmpq	-360(%rbp), %rdi
	je	.L2034
	call	_ZdlPv@PLT
	jmp	.L2034
	.p2align 4,,10
	.p2align 3
.L2090:
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L2093
	jmp	.L2091
	.p2align 4,,10
	.p2align 3
.L2176:
	movq	%rbx, %rdi
	leaq	-288(%rbp), %r14
.LEHB309:
	call	_Znwm@PLT
	movq	-288(%rbp), %r8
	movq	-280(%rbp), %rsi
	movq	%rax, %r14
	movq	%r8, %rdx
	cmpq	%r8, %rsi
	je	.L2007
	.p2align 4,,10
	.p2align 3
.L2011:
	leaq	16(%rax), %rcx
	leaq	16(%rdx), %rdi
	movq	%rcx, (%rax)
	movq	(%rdx), %rcx
	cmpq	%rdi, %rcx
	je	.L2189
	movq	%rcx, (%rax)
	movq	16(%rdx), %rcx
	addq	$32, %rdx
	addq	$32, %rax
	movq	%rcx, -16(%rax)
	movq	-24(%rdx), %rcx
	movq	%rcx, -24(%rax)
	cmpq	%rdx, %rsi
	jne	.L2011
.L2007:
	testq	%r8, %r8
	je	.L2012
	movq	%r8, %rdi
	call	_ZdlPv@PLT
.L2012:
	movq	-312(%rbp), %rax
	movq	%r14, %xmm0
	leaq	(%r14,%rbx), %rsi
	punpcklqdq	%xmm0, %xmm0
	movq	%rsi, -272(%rbp)
	movq	8(%rax), %rax
	movaps	%xmm0, -288(%rbp)
	movq	%rax, %rcx
	subq	%rbx, %rcx
	movq	%rcx, %rbx
	cmpq	%rcx, %rax
	je	.L2013
	leaq	-288(%rbp), %r13
	jmp	.L2018
	.p2align 4,,10
	.p2align 3
.L2015:
	movq	%rdx, (%r14)
	movq	16(%rbx), %rdx
	movq	%rdx, 16(%r14)
.L2016:
	movq	8(%rbx), %rdx
	movq	%rdx, 8(%r14)
	movq	%rax, (%rbx)
	movq	$0, 8(%rbx)
	movb	$0, 16(%rbx)
	addq	$32, -280(%rbp)
.L2017:
	movq	-312(%rbp), %rax
	addq	$32, %rbx
	cmpq	%rbx, 8(%rax)
	je	.L2013
	movq	-280(%rbp), %r14
	movq	-272(%rbp), %rsi
.L2018:
	cmpq	%r14, %rsi
	je	.L2014
	leaq	16(%r14), %rax
	movq	%rax, (%r14)
	movq	(%rbx), %rdx
	leaq	16(%rbx), %rax
	cmpq	%rax, %rdx
	jne	.L2015
	movdqu	16(%rbx), %xmm1
	movups	%xmm1, 16(%r14)
	jmp	.L2016
	.p2align 4,,10
	.p2align 3
.L2014:
	movq	%rbx, %rdx
	movq	%r13, %rdi
	movq	%r13, %r14
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
.LEHE309:
	jmp	.L2017
	.p2align 4,,10
	.p2align 3
.L2189:
	movdqu	16(%rdx), %xmm2
	addq	$32, %rdx
	addq	$32, %rax
	movups	%xmm2, -16(%rax)
	movq	-24(%rdx), %rcx
	movq	%rcx, -24(%rax)
	cmpq	%rsi, %rdx
	jne	.L2011
	jmp	.L2007
	.p2align 4,,10
	.p2align 3
.L2177:
	salq	$5, %rdx
	leaq	(%rcx,%rdx), %r12
	cmpq	%rbx, %r12
	je	.L2022
	movq	%r12, %r13
	.p2align 4,,10
	.p2align 3
.L2027:
	movq	0(%r13), %rdi
	leaq	16(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2024
	call	_ZdlPv@PLT
	addq	$32, %r13
	cmpq	%rbx, %r13
	jne	.L2027
.L2025:
	movq	-312(%rbp), %rax
	movq	%r12, 8(%rax)
	jmp	.L2022
	.p2align 4,,10
	.p2align 3
.L2024:
	addq	$32, %r13
	cmpq	%rbx, %r13
	jne	.L2027
	jmp	.L2025
	.p2align 4,,10
	.p2align 3
.L2180:
	movq	-320(%rbp), %rax
	movl	$20, %edx
	leaq	.LC74(%rip), %rsi
	movq	8(%rax), %r12
	movq	%r12, %rdi
.LEHB310:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-256(%rbp), %rax
	movq	-376(%rbp), %rdi
	movq	(%rax), %rsi
	call	_ZNK2v88internal6torque4Type25GetGeneratedTNodeTypeNameB5cxx11Ev@PLT
.LEHE310:
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r12, %rdi
.LEHB311:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$2, %edx
	leaq	.LC75(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-184(%rbp), %rdx
	movq	-192(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rax, %rdi
	movl	$2, %edx
	leaq	.LC40(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE311:
	movq	-96(%rbp), %rdi
	cmpq	-344(%rbp), %rdi
	je	.L2039
	call	_ZdlPv@PLT
	jmp	.L2039
	.p2align 4,,10
	.p2align 3
.L2186:
	leaq	-128(%rbp), %r14
	movq	(%rdx), %rsi
	leaq	-224(%rbp), %r12
	movq	%r14, %rdi
.LEHB312:
	call	_ZNK2v88internal6torque4Type25GetGeneratedTNodeTypeNameB5cxx11Ev@PLT
.LEHE312:
	movq	-344(%rbp), %rax
	movq	-192(%rbp), %r13
	movq	-184(%rbp), %r12
	movq	%rax, -96(%rbp)
	movq	%r13, %rax
	addq	%r12, %rax
	je	.L2063
	testq	%r13, %r13
	je	.L2190
.L2063:
	movq	%r12, -296(%rbp)
	cmpq	$15, %r12
	ja	.L2191
	cmpq	$1, %r12
	jne	.L2066
	movzbl	0(%r13), %eax
	movb	%al, -80(%rbp)
	movq	-344(%rbp), %rax
.L2067:
	movq	%r12, -88(%rbp)
	movb	$0, (%rax,%r12)
	movq	-312(%rbp), %rax
	movq	8(%rax), %rsi
	cmpq	16(%rax), %rsi
	je	.L2068
	leaq	16(%rsi), %rax
	movq	%rax, (%rsi)
	movq	-96(%rbp), %rax
	cmpq	-344(%rbp), %rax
	je	.L2192
	movq	%rax, (%rsi)
	movq	-80(%rbp), %rax
	movq	%rax, 16(%rsi)
.L2070:
	movq	-88(%rbp), %rax
	movq	%rax, 8(%rsi)
	movq	-312(%rbp), %rax
	addq	$32, 8(%rax)
.L2071:
	movq	-320(%rbp), %rbx
	movl	$4, %edx
	leaq	.LC43(%rip), %rsi
	movq	8(%rbx), %r12
	movq	%r12, %rdi
.LEHB313:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-184(%rbp), %rdx
	movq	-192(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rax, %rdi
	movl	$3, %edx
	leaq	.LC39(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	.LC76(%rip), %rsi
	movq	%r14, %rdi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	je	.L2072
	movq	8(%rbx), %rdi
	movl	$12, %edx
	leaq	.LC77(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L2072:
	movq	-320(%rbp), %rbx
	movl	$49, %edx
	leaq	.LC150(%rip), %rsi
	movq	8(%rbx), %r12
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	32(%r15), %rax
	movq	%r12, %rdi
	movq	136(%rax), %rdx
	movq	128(%rax), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rax, %rdi
	movl	$2, %edx
	leaq	.LC27(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%rbx), %r12
	movq	-280(%rbp), %r13
	movq	-288(%rbp), %rbx
	cmpq	%r13, %rbx
	je	.L2077
.L2073:
	movq	8(%rbx), %rdx
	movq	(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$32, %rbx
	cmpq	%rbx, %r13
	je	.L2077
	movl	$2, %edx
	leaq	.LC27(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L2073
	.p2align 4,,10
	.p2align 3
.L2077:
	leaq	.LC76(%rip), %rsi
	movq	%r14, %rdi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	je	.L2075
	movq	-320(%rbp), %rax
	movl	$1, %edx
	leaq	.LC57(%rip), %rsi
	movq	8(%rax), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L2075:
	movq	-320(%rbp), %rbx
	movl	$3, %edx
	leaq	.LC3(%rip), %rsi
	movq	8(%rbx), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%rbx), %r12
	movl	$8, %edx
	leaq	.LC80(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-184(%rbp), %rdx
	movq	-192(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rax, %rdi
	movl	$3, %edx
	leaq	.LC3(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE313:
	movq	-128(%rbp), %rdi
	leaq	-112(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2079
	call	_ZdlPv@PLT
	jmp	.L2079
	.p2align 4,,10
	.p2align 3
.L2179:
	movdqu	16(%rax), %xmm3
	movaps	%xmm3, -176(%rbp)
	jmp	.L2036
	.p2align 4,,10
	.p2align 3
.L2181:
	movq	$0, -352(%rbp)
	jmp	.L2045
	.p2align 4,,10
	.p2align 3
.L2188:
	leaq	-224(%rbp), %r12
.LEHB314:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_10TypeOracleEEERPNT_5ScopeEv@PLT
.LEHE314:
	movq	-344(%rbp), %rbx
	movq	-376(%rbp), %rdi
	movb	$0, -76(%rbp)
	movl	$1684631414, -80(%rbp)
	movq	%rbx, -96(%rbp)
	movq	$4, -88(%rbp)
.LEHB315:
	call	_ZN2v88internal6torque12Declarations16LookupGlobalTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
.LEHE315:
	movq	-96(%rbp), %rdi
	movq	%rax, %rdx
	cmpq	%rbx, %rdi
	je	.L2085
	movq	%rax, -312(%rbp)
	call	_ZdlPv@PLT
	movq	-312(%rbp), %rdx
	jmp	.L2085
	.p2align 4,,10
	.p2align 3
.L2066:
	testq	%r12, %r12
	jne	.L2193
	movq	-344(%rbp), %rax
	jmp	.L2067
	.p2align 4,,10
	.p2align 3
.L2191:
	movq	-376(%rbp), %rdi
	leaq	-296(%rbp), %rsi
	xorl	%edx, %edx
.LEHB316:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE316:
	movq	%rax, -96(%rbp)
	movq	%rax, %rdi
	movq	-296(%rbp), %rax
	movq	%rax, -80(%rbp)
.L2065:
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	-296(%rbp), %r12
	movq	-96(%rbp), %rax
	jmp	.L2067
	.p2align 4,,10
	.p2align 3
.L2192:
	movdqa	-80(%rbp), %xmm4
	movups	%xmm4, 16(%rsi)
	jmp	.L2070
	.p2align 4,,10
	.p2align 3
.L2068:
	movq	-376(%rbp), %rdx
	movq	%rax, %rdi
.LEHB317:
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
.LEHE317:
	movq	-96(%rbp), %rdi
	cmpq	-344(%rbp), %rdi
	je	.L2071
	call	_ZdlPv@PLT
	jmp	.L2071
.L2183:
	leaq	.LC6(%rip), %rdi
.LEHB318:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE318:
.L2020:
	movq	%r12, %rsi
	movq	-312(%rbp), %rdi
	leaq	-288(%rbp), %r14
	negq	%rsi
.LEHB319:
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_default_appendEm
	jmp	.L2022
.L2175:
	leaq	.LC70(%rip), %rdi
	leaq	-288(%rbp), %r14
	call	_ZSt20__throw_length_errorPKc@PLT
.LEHE319:
.L2185:
	call	__stack_chk_fail@PLT
.L2182:
.LEHB320:
	call	_ZSt17__throw_bad_allocv@PLT
.LEHE320:
.L2193:
	movq	-344(%rbp), %rdi
	jmp	.L2065
.L2190:
	leaq	.LC6(%rip), %rdi
.LEHB321:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE321:
.L2122:
	endbr64
	movq	%rax, %rbx
	jmp	.L2062
.L2120:
	endbr64
	movq	%rax, %rbx
	jmp	.L2106
.L2130:
	endbr64
	movq	%rax, %rbx
	jmp	.L2086
.L2123:
	endbr64
	movq	%rax, %rbx
	jmp	.L2109
.L2124:
	endbr64
	movq	%rax, %rbx
	jmp	.L2107
.L2121:
	endbr64
	movq	%rax, %rbx
	jmp	.L2104
.L2126:
	endbr64
	movq	%rax, %r12
	jmp	.L2029
.L2125:
	endbr64
	movq	%rax, %rbx
	jmp	.L2088
.L2118:
	endbr64
	movq	%rax, %rbx
	jmp	.L2114
.L2129:
	endbr64
	movq	%rax, %rdi
	jmp	.L2056
.L2119:
	endbr64
	movq	%rax, %rbx
	jmp	.L2042
.L2127:
	endbr64
	movq	%rax, %rbx
	jmp	.L2040
	.section	.gcc_except_table._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_22CallBuiltinInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE,"a",@progbits
	.align 4
.LLSDA6941:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATT6941-.LLSDATTD6941
.LLSDATTD6941:
	.byte	0x1
	.uleb128 .LLSDACSE6941-.LLSDACSB6941
.LLSDACSB6941:
	.uleb128 .LEHB301-.LFB6941
	.uleb128 .LEHE301-.LEHB301
	.uleb128 .L2118-.LFB6941
	.uleb128 0
	.uleb128 .LEHB302-.LFB6941
	.uleb128 .LEHE302-.LEHB302
	.uleb128 .L2119-.LFB6941
	.uleb128 0
	.uleb128 .LEHB303-.LFB6941
	.uleb128 .LEHE303-.LEHB303
	.uleb128 .L2127-.LFB6941
	.uleb128 0
	.uleb128 .LEHB304-.LFB6941
	.uleb128 .LEHE304-.LEHB304
	.uleb128 .L2120-.LFB6941
	.uleb128 0
	.uleb128 .LEHB305-.LFB6941
	.uleb128 .LEHE305-.LEHB305
	.uleb128 .L2122-.LFB6941
	.uleb128 0
	.uleb128 .LEHB306-.LFB6941
	.uleb128 .LEHE306-.LEHB306
	.uleb128 .L2129-.LFB6941
	.uleb128 0x1
	.uleb128 .LEHB307-.LFB6941
	.uleb128 .LEHE307-.LEHB307
	.uleb128 .L2119-.LFB6941
	.uleb128 0
	.uleb128 .LEHB308-.LFB6941
	.uleb128 .LEHE308-.LEHB308
	.uleb128 .L2125-.LFB6941
	.uleb128 0
	.uleb128 .LEHB309-.LFB6941
	.uleb128 .LEHE309-.LEHB309
	.uleb128 .L2126-.LFB6941
	.uleb128 0
	.uleb128 .LEHB310-.LFB6941
	.uleb128 .LEHE310-.LEHB310
	.uleb128 .L2120-.LFB6941
	.uleb128 0
	.uleb128 .LEHB311-.LFB6941
	.uleb128 .LEHE311-.LEHB311
	.uleb128 .L2121-.LFB6941
	.uleb128 0
	.uleb128 .LEHB312-.LFB6941
	.uleb128 .LEHE312-.LEHB312
	.uleb128 .L2125-.LFB6941
	.uleb128 0
	.uleb128 .LEHB313-.LFB6941
	.uleb128 .LEHE313-.LEHB313
	.uleb128 .L2123-.LFB6941
	.uleb128 0
	.uleb128 .LEHB314-.LFB6941
	.uleb128 .LEHE314-.LEHB314
	.uleb128 .L2125-.LFB6941
	.uleb128 0
	.uleb128 .LEHB315-.LFB6941
	.uleb128 .LEHE315-.LEHB315
	.uleb128 .L2130-.LFB6941
	.uleb128 0
	.uleb128 .LEHB316-.LFB6941
	.uleb128 .LEHE316-.LEHB316
	.uleb128 .L2123-.LFB6941
	.uleb128 0
	.uleb128 .LEHB317-.LFB6941
	.uleb128 .LEHE317-.LEHB317
	.uleb128 .L2124-.LFB6941
	.uleb128 0
	.uleb128 .LEHB318-.LFB6941
	.uleb128 .LEHE318-.LEHB318
	.uleb128 .L2129-.LFB6941
	.uleb128 0x1
	.uleb128 .LEHB319-.LFB6941
	.uleb128 .LEHE319-.LEHB319
	.uleb128 .L2126-.LFB6941
	.uleb128 0
	.uleb128 .LEHB320-.LFB6941
	.uleb128 .LEHE320-.LEHB320
	.uleb128 .L2122-.LFB6941
	.uleb128 0
	.uleb128 .LEHB321-.LFB6941
	.uleb128 .LEHE321-.LEHB321
	.uleb128 .L2123-.LFB6941
	.uleb128 0
.LLSDACSE6941:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATT6941:
	.section	.text._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_22CallBuiltinInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_22CallBuiltinInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6941
	.type	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_22CallBuiltinInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.cold, @function
_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_22CallBuiltinInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.cold:
.LFSB6941:
	nop
.L2128:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	endbr64
	movq	%rax, %rbx
	call	__cxa_end_catch@PLT
	movq	-224(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2062
	call	_ZdlPv@PLT
.L2062:
	movq	-160(%rbp), %rdi
	leaq	-144(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2106
	call	_ZdlPv@PLT
.L2106:
	movq	-192(%rbp), %rdi
	cmpq	-360(%rbp), %rdi
	je	.L2042
	call	_ZdlPv@PLT
.L2042:
	movq	-256(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2114
	call	_ZdlPv@PLT
.L2114:
	leaq	-288(%rbp), %rdi
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EED1Ev
	movq	%rbx, %rdi
.LEHB322:
	call	_Unwind_Resume@PLT
.L2086:
	movq	-96(%rbp), %rdi
	cmpq	-344(%rbp), %rdi
	je	.L2087
	call	_ZdlPv@PLT
.L2087:
	leaq	-224(%rbp), %r12
.L2088:
	movq	%r12, %rdi
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EED1Ev
	jmp	.L2062
.L2107:
	movq	-96(%rbp), %rdi
	cmpq	-344(%rbp), %rdi
	je	.L2109
	call	_ZdlPv@PLT
.L2109:
	movq	-128(%rbp), %rdi
	leaq	-112(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2110
	call	_ZdlPv@PLT
.L2110:
	leaq	-224(%rbp), %r12
	jmp	.L2088
.L2104:
	movq	-96(%rbp), %rdi
	cmpq	-344(%rbp), %rdi
	je	.L2106
	call	_ZdlPv@PLT
	jmp	.L2106
.L2029:
	movq	%r14, %rdi
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EED1Ev
	movq	%r12, %rdi
	call	_Unwind_Resume@PLT
.LEHE322:
.L2056:
	call	__cxa_begin_catch@PLT
	movq	-352(%rbp), %r12
.L2059:
	cmpq	%rbx, %r12
	jne	.L2194
.LEHB323:
	call	__cxa_rethrow@PLT
.LEHE323:
.L2040:
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2042
	call	_ZdlPv@PLT
	jmp	.L2042
.L2194:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2058
	call	_ZdlPv@PLT
.L2058:
	addq	$32, %r12
	jmp	.L2059
	.cfi_endproc
.LFE6941:
	.section	.gcc_except_table._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_22CallBuiltinInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.align 4
.LLSDAC6941:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATTC6941-.LLSDATTDC6941
.LLSDATTDC6941:
	.byte	0x1
	.uleb128 .LLSDACSEC6941-.LLSDACSBC6941
.LLSDACSBC6941:
	.uleb128 .LEHB322-.LCOLDB152
	.uleb128 .LEHE322-.LEHB322
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB323-.LCOLDB152
	.uleb128 .LEHE323-.LEHB323
	.uleb128 .L2128-.LCOLDB152
	.uleb128 0
.LLSDACSEC6941:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATTC6941:
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_22CallBuiltinInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.section	.text._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_22CallBuiltinInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.size	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_22CallBuiltinInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE, .-_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_22CallBuiltinInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_22CallBuiltinInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.size	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_22CallBuiltinInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.cold, .-_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_22CallBuiltinInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.cold
.LCOLDE152:
	.section	.text._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_22CallBuiltinInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
.LHOTE152:
	.section	.text._ZNSt6vectorIS_INSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_,"axG",@progbits,_ZNSt6vectorIS_INSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIS_INSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	.type	_ZNSt6vectorIS_INSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_, @function
_ZNSt6vectorIS_INSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_:
.LFB10284:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movabsq	$-6148914691236517205, %rsi
	subq	$40, %rsp
	movq	8(%rdi), %rcx
	movq	(%rdi), %r13
	movabsq	$384307168202282325, %rdi
	movq	%rcx, %rax
	subq	%r13, %rax
	sarq	$3, %rax
	imulq	%rsi, %rax
	cmpq	%rdi, %rax
	je	.L2227
	movq	%rbx, %r9
	subq	%r13, %r9
	testq	%rax, %rax
	je	.L2210
	movabsq	$9223372036854775800, %rsi
	leaq	(%rax,%rax), %r8
	cmpq	%r8, %rax
	jbe	.L2228
.L2197:
	movq	%rsi, %rdi
	movq	%rdx, -80(%rbp)
	movq	%r9, -72(%rbp)
	movq	%rcx, -64(%rbp)
	movq	%rsi, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rsi
	movq	-64(%rbp), %rcx
	movq	-72(%rbp), %r9
	movq	-80(%rbp), %rdx
	movq	%rax, %r12
	leaq	24(%rax), %r8
	addq	%rax, %rsi
.L2209:
	movdqu	(%rdx), %xmm2
	movq	16(%rdx), %rdi
	leaq	(%r12,%r9), %rax
	pxor	%xmm0, %xmm0
	movq	$0, 16(%rdx)
	movq	%rdi, 16(%rax)
	movups	%xmm2, (%rax)
	movups	%xmm0, (%rdx)
	cmpq	%r13, %rbx
	je	.L2199
	leaq	-24(%rbx), %rax
	movq	%r13, %rdx
	movabsq	$768614336404564651, %rdi
	subq	%r13, %rax
	shrq	$3, %rax
	imulq	%rdi, %rax
	leaq	47(%r12), %rdi
	subq	%r13, %rdi
	cmpq	$94, %rdi
	jbe	.L2212
	movabsq	$2305843009213693950, %rdi
	testq	%rdi, %rax
	je	.L2212
	movabsq	$2305843009213693951, %r8
	movq	%r12, %rdi
	andq	%rax, %r8
	addq	$1, %r8
	movq	%r8, %r9
	shrq	%r9
	leaq	(%r9,%r9,2), %r9
	salq	$4, %r9
	addq	%r13, %r9
	.p2align 4,,10
	.p2align 3
.L2201:
	movdqu	16(%rdx), %xmm1
	movdqu	(%rdx), %xmm2
	addq	$48, %rdx
	addq	$48, %rdi
	movdqu	-16(%rdx), %xmm0
	movups	%xmm2, -48(%rdi)
	movups	%xmm1, -32(%rdi)
	movups	%xmm0, -16(%rdi)
	cmpq	%r9, %rdx
	jne	.L2201
	movq	%r8, %rdi
	andq	$-2, %rdi
	leaq	(%rdi,%rdi,2), %rdx
	salq	$3, %rdx
	leaq	0(%r13,%rdx), %r9
	addq	%r12, %rdx
	cmpq	%rdi, %r8
	je	.L2203
	movq	16(%r9), %rdi
	movdqu	(%r9), %xmm3
	movq	%rdi, 16(%rdx)
	movups	%xmm3, (%rdx)
.L2203:
	leaq	(%rax,%rax,2), %rax
	leaq	48(%r12,%rax,8), %r8
.L2199:
	cmpq	%rcx, %rbx
	je	.L2204
	subq	%rbx, %rcx
	movq	%rbx, %rdx
	leaq	-24(%rcx), %rax
	movabsq	$768614336404564651, %rcx
	shrq	$3, %rax
	imulq	%rcx, %rax
	movabsq	$2305843009213693951, %rcx
	andq	%rcx, %rax
	leaq	1(%rax), %rcx
	movq	%r8, %rax
	je	.L2205
	movq	%rcx, %rdi
	shrq	%rdi
	leaq	(%rdi,%rdi,2), %rdi
	salq	$4, %rdi
	addq	%rbx, %rdi
	.p2align 4,,10
	.p2align 3
.L2206:
	movdqu	(%rdx), %xmm5
	movdqu	16(%rdx), %xmm6
	addq	$48, %rdx
	addq	$48, %rax
	movdqu	-16(%rdx), %xmm7
	movups	%xmm5, -48(%rax)
	movups	%xmm6, -32(%rax)
	movups	%xmm7, -16(%rax)
	cmpq	%rdi, %rdx
	jne	.L2206
	movq	%rcx, %rdx
	andq	$-2, %rdx
	leaq	(%rdx,%rdx,2), %rax
	salq	$3, %rax
	leaq	(%rbx,%rax), %r15
	addq	%r8, %rax
	cmpq	%rdx, %rcx
	je	.L2207
.L2205:
	movq	16(%r15), %rdx
	movdqu	(%r15), %xmm1
	movq	%rdx, 16(%rax)
	movups	%xmm1, (%rax)
.L2207:
	leaq	(%rcx,%rcx,2), %rax
	leaq	(%r8,%rax,8), %r8
.L2204:
	testq	%r13, %r13
	je	.L2208
	movq	%r13, %rdi
	movq	%r8, -64(%rbp)
	movq	%rsi, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-64(%rbp), %r8
	movq	-56(%rbp), %rsi
.L2208:
	movq	%r12, %xmm0
	movq	%r8, %xmm3
	movq	%rsi, 16(%r14)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, (%r14)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2228:
	.cfi_restore_state
	testq	%r8, %r8
	jne	.L2198
	movl	$24, %r8d
	xorl	%esi, %esi
	xorl	%r12d, %r12d
	jmp	.L2209
	.p2align 4,,10
	.p2align 3
.L2210:
	movl	$24, %esi
	jmp	.L2197
	.p2align 4,,10
	.p2align 3
.L2212:
	movq	%r12, %rdi
	movq	%r13, %rdx
	.p2align 4,,10
	.p2align 3
.L2200:
	movdqu	(%rdx), %xmm4
	addq	$24, %rdx
	addq	$24, %rdi
	movups	%xmm4, -24(%rdi)
	movq	-8(%rdx), %r8
	movq	%r8, -8(%rdi)
	cmpq	%rdx, %rbx
	jne	.L2200
	jmp	.L2203
.L2227:
	leaq	.LC69(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L2198:
	cmpq	%rdi, %r8
	movq	%rdi, %rsi
	cmovbe	%r8, %rsi
	imulq	$24, %rsi, %rsi
	jmp	.L2197
	.cfi_endproc
.LFE10284:
	.size	_ZNSt6vectorIS_INSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_, .-_ZNSt6vectorIS_INSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	.section	.rodata._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_32CallCsaMacroAndBranchInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.str1.1,"aMS",@progbits,1
.LC153:
	.string	"label"
	.section	.rodata._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_32CallCsaMacroAndBranchInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC154:
	.string	"    compiler::CodeAssemblerLabel "
	.section	.rodata._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_32CallCsaMacroAndBranchInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.str1.1
.LC155:
	.string	"result_"
.LC156:
	.string	"_"
	.section	.rodata._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_32CallCsaMacroAndBranchInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.str1.8
	.align 8
.LC157:
	.string	"    compiler::TypedCodeAssemblerVariable<"
	.section	.rodata._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_32CallCsaMacroAndBranchInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.str1.1
.LC158:
	.string	"(&ca_);\n"
.LC159:
	.string	"&"
.LC160:
	.string	".is_used()) {\n"
.LC161:
	.string	".value()"
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_32CallCsaMacroAndBranchInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE,"ax",@progbits
	.align 2
.LCOLDB162:
	.section	.text._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_32CallCsaMacroAndBranchInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE,"ax",@progbits
.LHOTB162:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_32CallCsaMacroAndBranchInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.type	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_32CallCsaMacroAndBranchInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE, @function
_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_32CallCsaMacroAndBranchInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE:
.LFB6930:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6930
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$568, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	48(%rsi), %r15
	movq	40(%rsi), %r13
	movq	%rdi, -488(%rbp)
	movq	%rsi, -560(%rbp)
	movq	%rdx, -584(%rbp)
	movq	%fs:40, %rbx
	movq	%rbx, -56(%rbp)
	xorl	%ebx, %ebx
	movq	%r15, %rbx
	movaps	%xmm0, -480(%rbp)
	movq	$0, -464(%rbp)
	subq	%r13, %rbx
	movq	%rbx, %rax
	sarq	$5, %rax
	je	.L2613
	movabsq	$288230376151711743, %rdx
	cmpq	%rdx, %rax
	ja	.L2614
	movq	%rbx, %rdi
.LEHB324:
	call	_Znwm@PLT
.LEHE324:
	movq	%rax, -504(%rbp)
	movq	-560(%rbp), %rax
	movq	48(%rax), %r15
	movq	40(%rax), %r13
.L2231:
	movq	-504(%rbp), %rax
	movq	%rax, %xmm0
	addq	%rax, %rbx
	punpcklqdq	%xmm0, %xmm0
	movq	%rbx, -464(%rbp)
	movq	%rax, %rbx
	movaps	%xmm0, -480(%rbp)
	cmpq	%r15, %r13
	je	.L2233
	leaq	-256(%rbp), %rax
	movq	%rax, -496(%rbp)
	jmp	.L2239
	.p2align 4,,10
	.p2align 3
.L2235:
	cmpq	$1, %r12
	jne	.L2237
	movzbl	(%r14), %eax
	movb	%al, 16(%rbx)
.L2238:
	addq	$32, %r13
	movq	%r12, 8(%rbx)
	addq	$32, %rbx
	movb	$0, (%rdi,%r12)
	cmpq	%r13, %r15
	je	.L2233
.L2239:
	leaq	16(%rbx), %rdi
	movq	%rdi, (%rbx)
	movq	0(%r13), %r14
	movq	8(%r13), %r12
	movq	%r14, %rax
	addq	%r12, %rax
	je	.L2234
	testq	%r14, %r14
	je	.L2615
.L2234:
	movq	%r12, -256(%rbp)
	cmpq	$15, %r12
	jbe	.L2235
	movq	-496(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rbx, %rdi
.LEHB325:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE325:
	movq	%rax, (%rbx)
	movq	%rax, %rdi
	movq	-256(%rbp), %rax
	movq	%rax, 16(%rbx)
.L2236:
	movq	%r12, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-256(%rbp), %r12
	movq	(%rbx), %rdi
	jmp	.L2238
	.p2align 4,,10
	.p2align 3
.L2237:
	testq	%r12, %r12
	je	.L2238
	jmp	.L2236
	.p2align 4,,10
	.p2align 3
.L2233:
	movq	-560(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	%rbx, -472(%rbp)
	movq	$0, -432(%rbp)
	movq	32(%rax), %r12
	movaps	%xmm0, -448(%rbp)
	movq	$0, -400(%rbp)
	movq	264(%r12), %rax
	movq	256(%r12), %rsi
	movaps	%xmm0, -416(%rbp)
	movq	%rax, %rbx
	subq	%rsi, %rbx
	movq	%rbx, %rdx
	sarq	$3, %rdx
	je	.L2616
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L2617
	movq	%rbx, %rdi
.LEHB326:
	call	_Znwm@PLT
.LEHE326:
	movq	%rax, %rcx
	movq	264(%r12), %rax
	movq	256(%r12), %rsi
	movq	%rax, %r12
	subq	%rsi, %r12
.L2241:
	movq	%rcx, %xmm0
	addq	%rcx, %rbx
	punpcklqdq	%xmm0, %xmm0
	movq	%rbx, -400(%rbp)
	movaps	%xmm0, -416(%rbp)
	cmpq	%rax, %rsi
	je	.L2249
	movq	%rcx, %rdi
	movq	%r12, %rdx
	call	memmove@PLT
	movq	%rax, %rcx
.L2249:
	movq	-584(%rbp), %rbx
	addq	%r12, %rcx
	leaq	-448(%rbp), %rax
	movq	-488(%rbp), %rdi
	movq	%rcx, -408(%rbp)
	leaq	-416(%rbp), %rsi
	leaq	-480(%rbp), %rcx
	movq	%rax, %rdx
	movq	%rbx, %r8
	movq	%rcx, -608(%rbp)
	movq	%rax, -600(%rbp)
.LEHB327:
	call	_ZN2v88internal6torque12CSAGenerator22ProcessArgumentsCommonERKSt6vectorIPKNS1_4TypeESaIS6_EEPS3_INSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaISG_EESJ_PNS1_5StackISG_EE
	movq	8(%rbx), %r15
	movq	(%rbx), %r13
	pxor	%xmm0, %xmm0
	movq	$0, -368(%rbp)
	movaps	%xmm0, -384(%rbp)
	movq	%r15, %rbx
	subq	%r13, %rbx
	movq	%rbx, %rax
	sarq	$5, %rax
	je	.L2618
	movabsq	$288230376151711743, %rdx
	cmpq	%rdx, %rax
	ja	.L2619
	movq	%rbx, %rdi
	call	_Znwm@PLT
.LEHE327:
	movq	%rax, -504(%rbp)
	movq	-584(%rbp), %rax
	movq	8(%rax), %r15
	movq	(%rax), %r13
.L2251:
	movq	-504(%rbp), %rax
	movq	%rax, %xmm0
	addq	%rax, %rbx
	punpcklqdq	%xmm0, %xmm0
	movq	%rbx, -368(%rbp)
	movq	%rax, %rbx
	movaps	%xmm0, -384(%rbp)
	cmpq	%r13, %r15
	je	.L2253
	leaq	-256(%rbp), %rax
	movq	%rax, -496(%rbp)
	jmp	.L2259
	.p2align 4,,10
	.p2align 3
.L2255:
	cmpq	$1, %r12
	jne	.L2257
	movzbl	(%r14), %eax
	movb	%al, 16(%rbx)
.L2258:
	addq	$32, %r13
	movq	%r12, 8(%rbx)
	addq	$32, %rbx
	movb	$0, (%rdi,%r12)
	cmpq	%r13, %r15
	je	.L2253
.L2259:
	leaq	16(%rbx), %rdi
	movq	%rdi, (%rbx)
	movq	0(%r13), %r14
	movq	8(%r13), %r12
	movq	%r14, %rax
	addq	%r12, %rax
	je	.L2254
	testq	%r14, %r14
	je	.L2620
.L2254:
	movq	%r12, -256(%rbp)
	cmpq	$15, %r12
	jbe	.L2255
	movq	-496(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rbx, %rdi
.LEHB328:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE328:
	movq	%rax, (%rbx)
	movq	%rax, %rdi
	movq	-256(%rbp), %rax
	movq	%rax, 16(%rbx)
.L2256:
	movq	%r12, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-256(%rbp), %r12
	movq	(%rbx), %rdi
	jmp	.L2258
	.p2align 4,,10
	.p2align 3
.L2257:
	testq	%r12, %r12
	je	.L2258
	jmp	.L2256
	.p2align 4,,10
	.p2align 3
.L2253:
	movq	-560(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	%rbx, -376(%rbp)
	movq	$0, -336(%rbp)
	movq	32(%rax), %rax
	movaps	%xmm0, -352(%rbp)
	movq	296(%rax), %rax
	movq	%rax, -592(%rbp)
.LEHB329:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_10TypeOracleEEERPNT_5ScopeEv@PLT
.LEHE329:
	leaq	-80(%rbp), %r14
	leaq	-96(%rbp), %rdi
	movb	$114, -76(%rbp)
	movq	%rdi, -528(%rbp)
	movq	%r14, -496(%rbp)
	movq	%r14, -96(%rbp)
	movl	$1702258030, -80(%rbp)
	movq	$5, -88(%rbp)
	movb	$0, -75(%rbp)
.LEHB330:
	call	_ZN2v88internal6torque12Declarations16LookupGlobalTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
.LEHE330:
	movq	-96(%rbp), %rdi
	movq	%rax, %rbx
	cmpq	%r14, %rdi
	je	.L2268
	call	_ZdlPv@PLT
.L2268:
	movq	-560(%rbp), %rax
	movq	32(%rax), %rax
	movq	%rax, -576(%rbp)
	cmpq	%rbx, -592(%rbp)
	je	.L2270
	movq	296(%rax), %rsi
	leaq	-256(%rbp), %rdi
.LEHB331:
	call	_ZN2v88internal6torque9LowerTypeEPKNS1_4TypeE@PLT
.LEHE331:
	movq	-248(%rbp), %rax
	movq	-256(%rbp), %rbx
	movq	%rax, -504(%rbp)
	cmpq	%rax, %rbx
	je	.L2274
	movq	vsnprintf@GOTPCREL(%rip), %r15
	leaq	-112(%rbp), %r14
	jmp	.L2287
	.p2align 4,,10
	.p2align 3
.L2275:
	movq	%rcx, -128(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -112(%rbp)
.L2276:
	movq	8(%rax), %rcx
	movq	%rcx, -120(%rbp)
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movb	$0, 16(%rax)
	movq	-96(%rbp), %rdi
	cmpq	-496(%rbp), %rdi
	je	.L2277
	call	_ZdlPv@PLT
.L2277:
	movq	-344(%rbp), %rsi
	cmpq	-336(%rbp), %rsi
	je	.L2621
	leaq	16(%rsi), %rax
	movq	%rax, (%rsi)
	movq	-128(%rbp), %rax
	cmpq	%r14, %rax
	je	.L2622
	movq	%rax, (%rsi)
	movq	-112(%rbp), %rax
	movq	%rax, 16(%rsi)
.L2284:
	movq	-120(%rbp), %rax
	movq	%rax, 8(%rsi)
	addq	$32, -344(%rbp)
.L2285:
	movq	-488(%rbp), %rax
	movl	$20, %edx
	leaq	.LC74(%rip), %rsi
	movq	8(%rax), %r12
	movq	%r12, %rdi
.LEHB332:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-528(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZNK2v88internal6torque4Type25GetGeneratedTNodeTypeNameB5cxx11Ev@PLT
.LEHE332:
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r12, %rdi
.LEHB333:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$2, %edx
	leaq	.LC75(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-344(%rbp), %rax
	movq	%r12, %rdi
	movq	-24(%rax), %rdx
	movq	-32(%rax), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rax, %rdi
	movl	$2, %edx
	leaq	.LC40(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE333:
	movq	-96(%rbp), %rdi
	cmpq	-496(%rbp), %rdi
	je	.L2286
	call	_ZdlPv@PLT
.L2286:
	movq	-488(%rbp), %rax
	movl	$8, %edx
	leaq	.LC80(%rip), %rsi
	movq	8(%rax), %r12
	movq	%r12, %rdi
.LEHB334:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-344(%rbp), %rax
	movq	%r12, %rdi
	movq	-24(%rax), %rdx
	movq	-32(%rax), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rax, %rdi
	movl	$3, %edx
	leaq	.LC3(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$8, %rbx
	cmpq	%rbx, -504(%rbp)
	je	.L2623
.L2287:
	movq	-488(%rbp), %rcx
	movq	(%rbx), %r13
	movl	$32, %edx
	movq	%r15, %rsi
	movq	-528(%rbp), %r12
	movq	16(%rcx), %r8
	movq	%r12, %rdi
	leaq	1(%r8), %rax
	movq	%rax, 16(%rcx)
	leaq	.LC0(%rip), %rcx
	xorl	%eax, %eax
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0
.LEHE334:
	movl	$3, %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	leaq	.LC73(%rip), %rcx
.LEHB335:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.LEHE335:
	movq	%r14, -128(%rbp)
	movq	(%rax), %rcx
	leaq	16(%rax), %rdx
	cmpq	%rdx, %rcx
	jne	.L2275
	movdqu	16(%rax), %xmm7
	movaps	%xmm7, -112(%rbp)
	jmp	.L2276
.L2623:
	movq	-256(%rbp), %rax
	movq	%rax, -504(%rbp)
.L2274:
	movq	-504(%rbp), %rax
	testq	%rax, %rax
	je	.L2288
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L2288:
	movq	-560(%rbp), %rax
	movq	32(%rax), %rax
	movq	%rax, -576(%rbp)
.L2270:
	pxor	%xmm0, %xmm0
	movq	$0, -304(%rbp)
	movq	304(%rax), %rbx
	movq	$0, -272(%rbp)
	movq	$0, -520(%rbp)
	movaps	%xmm0, -320(%rbp)
	movaps	%xmm0, -288(%rbp)
	cmpq	%rbx, 312(%rax)
	je	.L2335
	.p2align 4,,10
	.p2align 3
.L2289:
	movq	-520(%rbp), %rax
	salq	$5, %rax
	addq	%rax, %rbx
	movq	%rax, -568(%rbp)
	movq	16(%rbx), %rax
	movq	8(%rbx), %rsi
	movq	%rax, %r13
	subq	%rsi, %r13
	movq	%r13, %rdx
	sarq	$3, %rdx
	je	.L2476
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L2624
	movq	%r13, %rdi
.LEHB336:
	call	_Znwm@PLT
.LEHE336:
	movq	%rax, -544(%rbp)
	movq	16(%rbx), %rax
	movq	8(%rbx), %rsi
	movq	%rax, %r13
	subq	%rsi, %r13
.L2291:
	cmpq	%rax, %rsi
	je	.L2293
	movq	-544(%rbp), %rdi
	movq	%r13, %rdx
	call	memmove@PLT
.L2293:
	movq	vsnprintf@GOTPCREL(%rip), %r15
	leaq	-128(%rbp), %rax
	movq	-520(%rbp), %r8
	leaq	.LC0(%rip), %rcx
	movq	%rax, -536(%rbp)
	movq	%rax, %rbx
	movq	%rax, %rdi
	movl	$32, %edx
	movq	%r15, %rsi
	xorl	%eax, %eax
.LEHB337:
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0
.LEHE337:
	movl	$5, %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	leaq	.LC153(%rip), %rcx
.LEHB338:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.LEHE338:
	movq	-496(%rbp), %rbx
	leaq	16(%rax), %rdx
	movq	%rbx, -96(%rbp)
	movq	(%rax), %rcx
	cmpq	%rdx, %rcx
	je	.L2625
	movq	%rcx, -96(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -80(%rbp)
.L2295:
	movq	8(%rax), %rcx
	movq	%rcx, -88(%rbp)
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movq	-312(%rbp), %rsi
	movb	$0, 16(%rax)
	cmpq	-304(%rbp), %rsi
	je	.L2296
	leaq	16(%rsi), %rax
	movq	%rax, (%rsi)
	movq	-96(%rbp), %rax
	cmpq	-496(%rbp), %rax
	je	.L2626
	movq	%rax, (%rsi)
	movq	-80(%rbp), %rax
	movq	%rax, 16(%rsi)
.L2298:
	movq	-88(%rbp), %rax
	movq	%rax, 8(%rsi)
	addq	$32, -312(%rbp)
.L2299:
	movq	-128(%rbp), %rdi
	leaq	-112(%rbp), %rax
	movq	%rax, -512(%rbp)
	cmpq	%rax, %rdi
	je	.L2300
	call	_ZdlPv@PLT
.L2300:
	pxor	%xmm0, %xmm0
	movq	-280(%rbp), %rsi
	movq	$0, -240(%rbp)
	movaps	%xmm0, -256(%rbp)
	cmpq	-272(%rbp), %rsi
	je	.L2301
	movq	-256(%rbp), %rax
	movq	%rax, (%rsi)
	movq	-248(%rbp), %rax
	movq	%rax, 8(%rsi)
	movq	-240(%rbp), %rax
	addq	$24, -280(%rbp)
	movq	%rax, 16(%rsi)
.L2302:
	sarq	$3, %r13
	movq	%r13, -552(%rbp)
	je	.L2331
	movq	-520(%rbp), %rax
	xorl	%ebx, %ebx
	leaq	(%rax,%rax,2), %rax
	salq	$3, %rax
	movq	%rax, -504(%rbp)
	.p2align 4,,10
	.p2align 3
.L2332:
	movq	-536(%rbp), %rdi
	movq	%rbx, %r8
	movq	%r15, %rsi
	xorl	%eax, %eax
	leaq	.LC0(%rip), %rcx
	movl	$32, %edx
	movq	-504(%rbp), %r12
	addq	-288(%rbp), %r12
.LEHB339:
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0
.LEHE339:
	leaq	-224(%rbp), %r13
	movl	$32, %edx
	movq	%r15, %rsi
	xorl	%eax, %eax
	movq	-520(%rbp), %r8
	leaq	.LC0(%rip), %rcx
	movq	%r13, %rdi
.LEHB340:
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0
.LEHE340:
	movl	$7, %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r13, %rdi
	leaq	.LC155(%rip), %rcx
.LEHB341:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.LEHE341:
	leaq	-176(%rbp), %r14
	leaq	16(%rax), %rdx
	movq	%r14, -192(%rbp)
	movq	(%rax), %rcx
	cmpq	%rdx, %rcx
	je	.L2627
	movq	%rcx, -192(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -176(%rbp)
.L2311:
	movq	8(%rax), %rcx
	movb	$0, 16(%rax)
	movq	%rcx, -184(%rbp)
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movabsq	$4611686018427387903, %rax
	cmpq	%rax, -184(%rbp)
	je	.L2628
	leaq	-192(%rbp), %rdi
	movl	$1, %edx
	leaq	.LC156(%rip), %rsi
.LEHB342:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.LEHE342:
	leaq	-144(%rbp), %r13
	leaq	16(%rax), %rdx
	movq	%r13, -160(%rbp)
	movq	(%rax), %rcx
	cmpq	%rdx, %rcx
	je	.L2629
	movq	%rcx, -160(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -144(%rbp)
.L2314:
	movq	8(%rax), %rcx
	movq	%rcx, -152(%rbp)
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movb	$0, 16(%rax)
	movq	-160(%rbp), %r9
	movl	$15, %eax
	movq	-152(%rbp), %r8
	movq	-120(%rbp), %rdx
	movq	%rax, %rdi
	cmpq	%r13, %r9
	cmovne	-144(%rbp), %rdi
	movq	-128(%rbp), %rsi
	leaq	(%r8,%rdx), %rcx
	cmpq	%rdi, %rcx
	jbe	.L2316
	cmpq	-512(%rbp), %rsi
	cmovne	-112(%rbp), %rax
	cmpq	%rax, %rcx
	jbe	.L2630
.L2316:
	leaq	-160(%rbp), %rdi
.LEHB343:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.LEHE343:
.L2318:
	movq	-496(%rbp), %rcx
	leaq	16(%rax), %rdx
	movq	%rcx, -96(%rbp)
	movq	(%rax), %rcx
	cmpq	%rdx, %rcx
	je	.L2631
	movq	%rcx, -96(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -80(%rbp)
.L2320:
	movq	8(%rax), %rcx
	movq	%rcx, -88(%rbp)
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movb	$0, 16(%rax)
	movq	8(%r12), %rsi
	cmpq	16(%r12), %rsi
	je	.L2321
	leaq	16(%rsi), %rax
	movq	%rax, (%rsi)
	movq	-96(%rbp), %rax
	cmpq	-496(%rbp), %rax
	je	.L2632
	movq	%rax, (%rsi)
	movq	-80(%rbp), %rax
	movq	%rax, 16(%rsi)
.L2323:
	movq	-88(%rbp), %rax
	movq	%rax, 8(%rsi)
	movq	$0, -88(%rbp)
	movb	$0, -80(%rbp)
	addq	$32, 8(%r12)
.L2324:
	movq	-160(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L2325
	call	_ZdlPv@PLT
.L2325:
	movq	-192(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L2326
	call	_ZdlPv@PLT
.L2326:
	movq	-224(%rbp), %rdi
	leaq	-208(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2327
	call	_ZdlPv@PLT
.L2327:
	movq	-128(%rbp), %rdi
	cmpq	-512(%rbp), %rdi
	je	.L2328
	call	_ZdlPv@PLT
.L2328:
	movq	-488(%rbp), %rax
	movl	$41, %edx
	leaq	.LC157(%rip), %rsi
	movq	8(%rax), %r12
	movq	%r12, %rdi
.LEHB344:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE344:
	movq	-544(%rbp), %rax
	movq	-528(%rbp), %rdi
	movq	(%rax,%rbx,8), %rsi
.LEHB345:
	call	_ZNK2v88internal6torque4Type25GetGeneratedTNodeTypeNameB5cxx11Ev@PLT
.LEHE345:
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r12, %rdi
.LEHB346:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$2, %edx
	leaq	.LC75(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-288(%rbp), %rdx
	movq	-504(%rbp), %rcx
	movq	%rbx, %rax
	movq	%r12, %rdi
	salq	$5, %rax
	addq	(%rdx,%rcx), %rax
	movq	8(%rax), %rdx
	movq	(%rax), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rax, %rdi
	movl	$8, %edx
	leaq	.LC158(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE346:
	movq	-96(%rbp), %rdi
	cmpq	-496(%rbp), %rdi
	je	.L2329
	call	_ZdlPv@PLT
	addq	$1, %rbx
	cmpq	-552(%rbp), %rbx
	jne	.L2332
.L2331:
	movq	-488(%rbp), %rax
	movl	$33, %edx
	leaq	.LC154(%rip), %rsi
	movq	8(%rax), %r12
	movq	%r12, %rdi
.LEHB347:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-568(%rbp), %rax
	addq	-320(%rbp), %rax
	movq	%r12, %rdi
	movq	8(%rax), %rdx
	movq	(%rax), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rax, %rdi
	movl	$8, %edx
	leaq	.LC158(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE347:
	movq	-544(%rbp), %rax
	testq	%rax, %rax
	je	.L2333
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L2333:
	movq	-576(%rbp), %rax
	addq	$1, -520(%rbp)
	movq	-520(%rbp), %rcx
	movq	304(%rax), %rbx
	movq	312(%rax), %rax
	movq	%rax, -504(%rbp)
	subq	%rbx, %rax
	sarq	$5, %rax
	cmpq	%rcx, %rax
	ja	.L2289
.L2335:
	movq	-560(%rbp), %rax
	movq	-488(%rbp), %rsi
	leaq	-160(%rbp), %rdi
	movq	%rdi, -504(%rbp)
	movl	104(%rax), %edx
	movq	112(%rax), %rcx
.LEHB348:
	call	_ZN2v88internal6torque12CSAGenerator31PreCallableExceptionPreparationB5cxx11ENS_4base8OptionalIPNS1_5BlockEEE
.LEHE348:
	movq	-488(%rbp), %rax
	movl	$4, %edx
	leaq	.LC43(%rip), %rsi
	movq	8(%rax), %rdi
.LEHB349:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-352(%rbp), %rcx
	movq	-344(%rbp), %rax
	subq	%rcx, %rax
	movq	%rax, %rdx
	sarq	$5, %rdx
	cmpq	$32, %rax
	je	.L2633
	cmpq	$1, %rdx
	ja	.L2634
.L2337:
	movq	-560(%rbp), %rax
	movq	32(%rax), %rax
	testq	%rax, %rax
	je	.L2341
	cmpl	$2, 8(%rax)
	jne	.L2341
	movq	376(%rax), %rdx
	movq	368(%rax), %rsi
	movq	-488(%rbp), %rax
	movq	8(%rax), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rax, %rdi
	movl	$9, %edx
	leaq	.LC92(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L2342:
	movq	-560(%rbp), %rax
	movq	-488(%rbp), %rbx
	movq	32(%rax), %rax
	movq	8(%rbx), %rdi
	movq	136(%rax), %rdx
	movq	128(%rax), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rax, %rdi
	movl	$1, %edx
	leaq	.LC93(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%rbx), %r12
	movq	-440(%rbp), %r13
	leaq	.LC27(%rip), %r14
	movq	-448(%rbp), %rbx
	cmpq	%r13, %rbx
	je	.L2635
.L2344:
	movq	8(%rbx), %rdx
	movq	(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$32, %rbx
	cmpq	%rbx, %r13
	je	.L2636
	movl	$2, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE349:
	jmp	.L2344
	.p2align 4,,10
	.p2align 3
.L2329:
	addq	$1, %rbx
	cmpq	-552(%rbp), %rbx
	jne	.L2332
	jmp	.L2331
	.p2align 4,,10
	.p2align 3
.L2627:
	movdqu	16(%rax), %xmm1
	movaps	%xmm1, -176(%rbp)
	jmp	.L2311
	.p2align 4,,10
	.p2align 3
.L2629:
	movdqu	16(%rax), %xmm2
	movaps	%xmm2, -144(%rbp)
	jmp	.L2314
	.p2align 4,,10
	.p2align 3
.L2631:
	movdqu	16(%rax), %xmm3
	movaps	%xmm3, -80(%rbp)
	jmp	.L2320
	.p2align 4,,10
	.p2align 3
.L2632:
	movdqa	-80(%rbp), %xmm4
	movups	%xmm4, 16(%rsi)
	jmp	.L2323
	.p2align 4,,10
	.p2align 3
.L2630:
	movq	-536(%rbp), %rdi
	movq	%r9, %rcx
	xorl	%edx, %edx
	xorl	%esi, %esi
.LEHB350:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.LEHE350:
	jmp	.L2318
	.p2align 4,,10
	.p2align 3
.L2321:
	movq	-528(%rbp), %rdx
	movq	%r12, %rdi
.LEHB351:
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
.LEHE351:
	movq	-96(%rbp), %rdi
	cmpq	-496(%rbp), %rdi
	je	.L2324
	call	_ZdlPv@PLT
	jmp	.L2324
	.p2align 4,,10
	.p2align 3
.L2625:
	movdqu	16(%rax), %xmm5
	movaps	%xmm5, -80(%rbp)
	jmp	.L2295
	.p2align 4,,10
	.p2align 3
.L2476:
	movq	$0, -544(%rbp)
	jmp	.L2291
	.p2align 4,,10
	.p2align 3
.L2626:
	movdqa	-80(%rbp), %xmm7
	movups	%xmm7, 16(%rsi)
	jmp	.L2298
	.p2align 4,,10
	.p2align 3
.L2301:
	leaq	-256(%rbp), %r12
	leaq	-288(%rbp), %rdi
	movq	%r12, %rdx
.LEHB352:
	call	_ZNSt6vectorIS_INSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
.LEHE352:
	movq	-248(%rbp), %rbx
	movq	-256(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L2303
	.p2align 4,,10
	.p2align 3
.L2307:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2304
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%rbx, %r12
	jne	.L2307
.L2305:
	movq	-256(%rbp), %r12
.L2303:
	testq	%r12, %r12
	je	.L2302
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	jmp	.L2302
	.p2align 4,,10
	.p2align 3
.L2304:
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L2307
	jmp	.L2305
	.p2align 4,,10
	.p2align 3
.L2296:
	leaq	-320(%rbp), %r12
	movq	-528(%rbp), %rdx
	movq	%r12, %rdi
.LEHB353:
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
.LEHE353:
	movq	-96(%rbp), %rdi
	cmpq	-496(%rbp), %rdi
	je	.L2299
	call	_ZdlPv@PLT
	jmp	.L2299
	.p2align 4,,10
	.p2align 3
.L2636:
	movq	-488(%rbp), %rbx
	movq	-440(%rbp), %rdx
	movq	-448(%rbp), %rax
	movq	8(%rbx), %r12
	movq	-320(%rbp), %rbx
	cmpq	%rbx, -312(%rbp)
	je	.L2347
	xorl	%r15d, %r15d
	cmpq	%rax, %rdx
	jne	.L2350
	.p2align 4,,10
	.p2align 3
.L2348:
	movl	$1, %edx
	leaq	.LC159(%rip), %rsi
	movq	%r12, %rdi
.LEHB354:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r15, %rax
	movq	%r12, %rdi
	salq	$5, %rax
	addq	-320(%rbp), %rax
	movq	8(%rax), %rdx
	movq	(%rax), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-288(%rbp), %rax
	leaq	(%r15,%r15,2), %r13
	xorl	%ebx, %ebx
	leaq	.LC30(%rip), %r14
	salq	$3, %r13
	addq	%r13, %rax
	movq	8(%rax), %rcx
	cmpq	%rcx, (%rax)
	je	.L2351
	.p2align 4,,10
	.p2align 3
.L2349:
	movq	-488(%rbp), %rax
	movl	$3, %edx
	movq	%r14, %rsi
	movq	8(%rax), %r12
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-288(%rbp), %rdx
	movq	%rbx, %rax
	movq	%r12, %rdi
	salq	$5, %rax
	addq	(%rdx,%r13), %rax
	movq	8(%rax), %rdx
	movq	(%rax), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-288(%rbp), %rdx
	addq	$1, %rbx
	addq	%r13, %rdx
	movq	8(%rdx), %rax
	subq	(%rdx), %rax
	sarq	$5, %rax
	cmpq	%rax, %rbx
	jb	.L2349
.L2351:
	movq	-488(%rbp), %rax
	addq	$1, %r15
	movq	8(%rax), %r12
	movq	-312(%rbp), %rax
	subq	-320(%rbp), %rax
	sarq	$5, %rax
	cmpq	%rax, %r15
	jnb	.L2347
.L2350:
	movl	$2, %edx
	leaq	.LC27(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-488(%rbp), %rax
	movq	8(%rax), %r12
	jmp	.L2348
.L2635:
	movq	-320(%rbp), %rax
	xorl	%r15d, %r15d
	cmpq	%rax, -312(%rbp)
	jne	.L2348
	.p2align 4,,10
	.p2align 3
.L2347:
	movq	-592(%rbp), %rax
	cmpl	$4, 8(%rax)
	je	.L2637
	movl	$3, %edx
	leaq	.LC3(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L2353:
	movq	-560(%rbp), %rbx
	movq	-592(%rbp), %rdx
	leaq	-384(%rbp), %r9
	movq	-504(%rbp), %rsi
	movq	-488(%rbp), %rdi
	movl	104(%rbx), %ecx
	movq	112(%rbx), %r8
	call	_ZN2v88internal6torque12CSAGenerator32PostCallableExceptionPreparationERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKNS1_4TypeENS_4base8OptionalIPNS1_5BlockEEEPNS1_5StackIS8_EE
	cmpb	$0, 64(%rbx)
	jne	.L2638
.L2354:
	movq	-312(%rbp), %rax
	xorl	%r14d, %r14d
	leaq	.LC27(%rip), %r13
	cmpq	%rax, -320(%rbp)
	je	.L2386
	.p2align 4,,10
	.p2align 3
.L2369:
	movq	-488(%rbp), %r15
	movl	$8, %edx
	leaq	.LC16(%rip), %rsi
	movq	8(%r15), %r12
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-320(%rbp), %rax
	movq	%r14, %rbx
	movq	%r12, %rdi
	salq	$5, %rbx
	addq	%rbx, %rax
	movq	8(%rax), %rdx
	movq	(%rax), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rax, %rdi
	movl	$14, %edx
	leaq	.LC160(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%r15), %r12
	movl	$16, %edx
	leaq	.LC24(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	-320(%rbp), %rbx
	movq	%r12, %rdi
	movq	8(%rbx), %rdx
	movq	(%rbx), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rax, %rdi
	movl	$3, %edx
	leaq	.LC3(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%r15), %r12
	movl	$16, %edx
	leaq	.LC20(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-560(%rbp), %rax
	movq	-528(%rbp), %rbx
	leaq	.LC0(%rip), %rcx
	movl	$32, %edx
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	movq	80(%rax), %rax
	movq	%rbx, %rdi
	movq	(%rax,%r14,8), %rax
	movq	64(%rax), %r8
	xorl	%eax, %eax
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0
.LEHE354:
	movl	$5, %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	leaq	.LC14(%rip), %rcx
.LEHB355:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.LEHE355:
	leaq	-112(%rbp), %rbx
	leaq	16(%rax), %rdx
	movq	%rbx, -128(%rbp)
	movq	(%rax), %rcx
	movq	%rbx, -512(%rbp)
	cmpq	%rdx, %rcx
	je	.L2639
	movq	%rcx, -128(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -112(%rbp)
.L2373:
	movq	8(%rax), %rcx
	movq	%rcx, -120(%rbp)
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movb	$0, 16(%rax)
	movq	-96(%rbp), %rdi
	cmpq	-496(%rbp), %rdi
	je	.L2374
	call	_ZdlPv@PLT
.L2374:
	movq	-120(%rbp), %rdx
	movq	-128(%rbp), %rsi
	movq	%r12, %rdi
.LEHB356:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE356:
	movq	-128(%rbp), %rdi
	cmpq	-512(%rbp), %rdi
	je	.L2378
	call	_ZdlPv@PLT
.L2378:
	movq	-584(%rbp), %rax
	movq	8(%rax), %r15
	movq	(%rax), %rbx
	cmpq	%rbx, %r15
	je	.L2382
	.p2align 4,,10
	.p2align 3
.L2383:
	movq	-488(%rbp), %rax
	movl	$2, %edx
	movq	%r13, %rsi
	movq	8(%rax), %r12
	movq	%r12, %rdi
.LEHB357:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%rbx), %rdx
	movq	(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$32, %rbx
	cmpq	%rbx, %r15
	jne	.L2383
.L2382:
	movq	-288(%rbp), %rdx
	leaq	(%r14,%r14,2), %rax
	leaq	(%rdx,%rax,8), %rax
	movq	8(%rax), %r15
	movq	(%rax), %rbx
	cmpq	%rbx, %r15
	je	.L2381
	.p2align 4,,10
	.p2align 3
.L2385:
	movq	-488(%rbp), %rax
	movl	$2, %edx
	movq	%r13, %rsi
	movq	8(%rax), %r12
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%rbx), %rdx
	movq	(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rax, %rdi
	movl	$8, %edx
	leaq	.LC161(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$32, %rbx
	cmpq	%rbx, %r15
	jne	.L2385
.L2381:
	movq	-488(%rbp), %rax
	movl	$3, %edx
	leaq	.LC3(%rip), %rsi
	movq	8(%rax), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-488(%rbp), %rax
	movl	$6, %edx
	leaq	.LC15(%rip), %rsi
	movq	8(%rax), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-312(%rbp), %rax
	subq	-320(%rbp), %rax
	addq	$1, %r14
	sarq	$5, %rax
	cmpq	%rax, %r14
	jb	.L2369
.L2386:
	movq	-160(%rbp), %rdi
	leaq	-144(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2371
	call	_ZdlPv@PLT
.L2371:
	movq	-280(%rbp), %r14
	movq	-288(%rbp), %r13
	cmpq	%r13, %r14
	je	.L2387
	.p2align 4,,10
	.p2align 3
.L2396:
	movq	8(%r13), %rbx
	movq	0(%r13), %r12
	cmpq	%r12, %rbx
	je	.L2388
	.p2align 4,,10
	.p2align 3
.L2392:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2389
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%rbx, %r12
	jne	.L2392
.L2390:
	movq	0(%r13), %r12
.L2388:
	testq	%r12, %r12
	je	.L2393
	movq	%r12, %rdi
	addq	$24, %r13
	call	_ZdlPv@PLT
	cmpq	%r14, %r13
	jne	.L2396
.L2394:
	movq	-288(%rbp), %r13
.L2387:
	testq	%r13, %r13
	je	.L2397
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L2397:
	movq	-312(%rbp), %rbx
	movq	-320(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L2398
	.p2align 4,,10
	.p2align 3
.L2402:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2399
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%rbx, %r12
	jne	.L2402
.L2400:
	movq	-320(%rbp), %r12
.L2398:
	testq	%r12, %r12
	je	.L2403
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L2403:
	movq	-344(%rbp), %rbx
	movq	-352(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L2404
	.p2align 4,,10
	.p2align 3
.L2408:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2405
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%rbx, %r12
	jne	.L2408
.L2406:
	movq	-352(%rbp), %r12
.L2404:
	testq	%r12, %r12
	je	.L2409
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L2409:
	movq	-376(%rbp), %rbx
	movq	-384(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L2410
	.p2align 4,,10
	.p2align 3
.L2414:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2411
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%rbx, %r12
	jne	.L2414
.L2412:
	movq	-384(%rbp), %r12
.L2410:
	testq	%r12, %r12
	je	.L2415
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L2415:
	movq	-416(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2416
	call	_ZdlPv@PLT
.L2416:
	movq	-440(%rbp), %rbx
	movq	-448(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L2417
	.p2align 4,,10
	.p2align 3
.L2421:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2418
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%rbx, %r12
	jne	.L2421
.L2419:
	movq	-448(%rbp), %r12
.L2417:
	testq	%r12, %r12
	je	.L2422
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L2422:
	movq	-472(%rbp), %rbx
	movq	-480(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L2423
	.p2align 4,,10
	.p2align 3
.L2427:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2424
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%rbx, %r12
	jne	.L2427
.L2425:
	movq	-480(%rbp), %r12
.L2423:
	testq	%r12, %r12
	je	.L2229
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L2229:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2640
	addq	$568, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2389:
	.cfi_restore_state
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L2392
	jmp	.L2390
	.p2align 4,,10
	.p2align 3
.L2418:
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L2421
	jmp	.L2419
	.p2align 4,,10
	.p2align 3
.L2424:
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L2427
	jmp	.L2425
	.p2align 4,,10
	.p2align 3
.L2411:
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L2414
	jmp	.L2412
	.p2align 4,,10
	.p2align 3
.L2393:
	addq	$24, %r13
	cmpq	%r13, %r14
	jne	.L2396
	jmp	.L2394
	.p2align 4,,10
	.p2align 3
.L2399:
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L2402
	jmp	.L2400
	.p2align 4,,10
	.p2align 3
.L2405:
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L2408
	jmp	.L2406
	.p2align 4,,10
	.p2align 3
.L2639:
	movdqu	16(%rax), %xmm6
	movaps	%xmm6, -112(%rbp)
	jmp	.L2373
.L2634:
	movq	-488(%rbp), %rbx
	movl	$9, %edx
	leaq	.LC83(%rip), %rsi
	movq	8(%rbx), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%rbx), %r12
	movq	-344(%rbp), %r13
	leaq	.LC27(%rip), %r14
	movq	-352(%rbp), %rbx
	cmpq	%r13, %rbx
	je	.L2339
.L2338:
	movq	8(%rbx), %rdx
	movq	(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$32, %rbx
	cmpq	%rbx, %r13
	je	.L2641
	movl	$2, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE357:
	jmp	.L2338
	.p2align 4,,10
	.p2align 3
.L2622:
	movdqa	-112(%rbp), %xmm5
	movups	%xmm5, 16(%rsi)
	jmp	.L2284
	.p2align 4,,10
	.p2align 3
.L2621:
	leaq	-352(%rbp), %r12
	leaq	-128(%rbp), %rdx
	movq	%r12, %rdi
.LEHB358:
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
.LEHE358:
	movq	-128(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L2285
	call	_ZdlPv@PLT
	jmp	.L2285
.L2341:
	movq	-496(%rbp), %rbx
	movl	$24421, %eax
	movb	$0, -74(%rbp)
	movq	-528(%rbp), %rdx
	movq	-448(%rbp), %rsi
	movw	%ax, -76(%rbp)
	movq	-600(%rbp), %rdi
	movq	%rbx, -96(%rbp)
	movl	$1952543859, -80(%rbp)
	movq	$6, -88(%rbp)
.LEHB359:
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE14_M_insert_rvalEN9__gnu_cxx17__normal_iteratorIPKS5_S7_EEOS5_
.LEHE359:
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L2342
	call	_ZdlPv@PLT
	jmp	.L2342
.L2637:
	movl	$13, %edx
	leaq	.LC94(%rip), %rsi
	movq	%r12, %rdi
.LEHB360:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L2353
.L2638:
	movq	-488(%rbp), %rax
	movl	$14, %edx
	leaq	.LC36(%rip), %rsi
	movq	8(%rax), %r12
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-560(%rbp), %rax
	movq	-528(%rbp), %rbx
	leaq	.LC0(%rip), %rcx
	movl	$32, %edx
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	movq	72(%rax), %rax
	movq	%rbx, %rdi
	movq	64(%rax), %r8
	xorl	%eax, %eax
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0
.LEHE360:
	movl	$5, %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	leaq	.LC14(%rip), %rcx
.LEHB361:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.LEHE361:
	leaq	-112(%rbp), %rbx
	leaq	16(%rax), %rdx
	movq	%rbx, -128(%rbp)
	movq	(%rax), %rcx
	movq	%rbx, -512(%rbp)
	cmpq	%rdx, %rcx
	je	.L2642
	movq	%rcx, -128(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -112(%rbp)
.L2356:
	movq	8(%rax), %rcx
	movq	%rcx, -120(%rbp)
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movb	$0, 16(%rax)
	movq	-96(%rbp), %rdi
	cmpq	-496(%rbp), %rdi
	je	.L2357
	call	_ZdlPv@PLT
.L2357:
	movq	-120(%rbp), %rdx
	movq	-128(%rbp), %rsi
	movq	%r12, %rdi
.LEHB362:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE362:
	movq	-128(%rbp), %rdi
	cmpq	-512(%rbp), %rdi
	je	.L2362
	call	_ZdlPv@PLT
.L2362:
	movq	-584(%rbp), %rax
	leaq	.LC27(%rip), %r14
	movq	8(%rax), %r13
	movq	(%rax), %rbx
	cmpq	%rbx, %r13
	je	.L2366
	.p2align 4,,10
	.p2align 3
.L2367:
	movq	-488(%rbp), %rax
	movl	$2, %edx
	movq	%r14, %rsi
	movq	8(%rax), %r12
	movq	%r12, %rdi
.LEHB363:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%rbx), %rdx
	movq	(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$32, %rbx
	cmpq	%rbx, %r13
	jne	.L2367
.L2366:
	movq	-344(%rbp), %r13
	movq	-352(%rbp), %rbx
	leaq	.LC27(%rip), %r14
	cmpq	%rbx, %r13
	je	.L2365
	.p2align 4,,10
	.p2align 3
.L2368:
	movq	-488(%rbp), %rax
	movl	$2, %edx
	movq	%r14, %rsi
	movq	8(%rax), %r12
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%rbx), %rdx
	movq	(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$32, %rbx
	cmpq	%rbx, %r13
	jne	.L2368
.L2365:
	movq	-488(%rbp), %rax
	movl	$3, %edx
	leaq	.LC3(%rip), %rsi
	movq	8(%rax), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L2354
.L2641:
	movq	-488(%rbp), %rax
	movq	8(%rax), %r12
.L2339:
	movl	$4, %edx
	leaq	.LC84(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L2337
.L2618:
	movq	$0, -504(%rbp)
	jmp	.L2251
.L2616:
	movq	%rbx, %r12
	xorl	%ecx, %ecx
	jmp	.L2241
.L2613:
	movq	$0, -504(%rbp)
	jmp	.L2231
.L2633:
	movq	-488(%rbp), %rax
	movq	8(%rcx), %rdx
	movq	(%rcx), %rsi
	movq	8(%rax), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rax, %rdi
	movl	$3, %edx
	leaq	.LC39(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE363:
	jmp	.L2337
.L2642:
	movdqu	16(%rax), %xmm6
	movaps	%xmm6, -112(%rbp)
	jmp	.L2356
.L2628:
	leaq	.LC55(%rip), %rdi
.LEHB364:
	call	_ZSt20__throw_length_errorPKc@PLT
.LEHE364:
.L2617:
.LEHB365:
	call	_ZSt17__throw_bad_allocv@PLT
.LEHE365:
.L2615:
	leaq	.LC6(%rip), %rdi
.LEHB366:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE366:
.L2614:
.LEHB367:
	call	_ZSt17__throw_bad_allocv@PLT
.LEHE367:
.L2620:
	leaq	.LC6(%rip), %rdi
.LEHB368:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE368:
.L2619:
.LEHB369:
	call	_ZSt17__throw_bad_allocv@PLT
.LEHE369:
.L2640:
	call	__stack_chk_fail@PLT
.L2624:
.LEHB370:
	call	_ZSt17__throw_bad_allocv@PLT
.LEHE370:
.L2494:
	endbr64
	movq	%rax, %rbx
	jmp	.L2446
.L2493:
	endbr64
	movq	%rax, %rbx
	jmp	.L2448
.L2497:
	endbr64
	movq	%rax, %rbx
	jmp	.L2440
.L2502:
	endbr64
	movq	%rax, %rbx
	jmp	.L2459
.L2511:
	endbr64
	movq	%rax, %rbx
	jmp	.L2455
.L2486:
	endbr64
	movq	%rax, %rbx
	leaq	-352(%rbp), %r12
	jmp	.L2282
.L2509:
	endbr64
	movq	%rax, %rbx
	jmp	.L2280
.L2488:
	endbr64
	movq	%rax, %rbx
	jmp	.L2431
.L2487:
	endbr64
	movq	%rax, %rbx
	jmp	.L2429
.L2508:
	endbr64
	movq	%rax, %rbx
	jmp	.L2271
.L2510:
	endbr64
	movq	%rax, %rbx
	jmp	.L2455
.L2501:
	endbr64
	movq	%rax, %rbx
	jmp	.L2455
.L2490:
	endbr64
	movq	%rax, %rbx
	leaq	-320(%rbp), %r12
	jmp	.L2436
.L2512:
	endbr64
	movq	%rax, %rbx
	leaq	-320(%rbp), %r12
	jmp	.L2438
.L2499:
	endbr64
	movq	%rax, %rbx
	leaq	-320(%rbp), %r12
	jmp	.L2454
.L2483:
	endbr64
	movq	%rax, %rbx
	leaq	-480(%rbp), %rax
	movq	%rax, -608(%rbp)
	leaq	-448(%rbp), %rax
	movq	%rax, -600(%rbp)
	jmp	.L2470
.L2500:
	endbr64
	movq	%rax, %rbx
	jmp	.L2361
.L2498:
	endbr64
	movq	%rax, %rbx
	jmp	.L2450
.L2489:
	endbr64
	movq	%rax, %rbx
	jmp	.L2452
.L2496:
	endbr64
	movq	%rax, %rbx
	jmp	.L2442
.L2495:
	endbr64
	movq	%rax, %rbx
	jmp	.L2444
.L2484:
	endbr64
	movq	%rax, %rbx
	jmp	.L2267
.L2505:
	endbr64
	movq	%rax, %rdi
	jmp	.L2242
.L2491:
	endbr64
	movq	%rax, %rbx
	jmp	.L2434
.L2492:
	endbr64
	movq	%rax, %rbx
	jmp	.L2439
.L2485:
	endbr64
	movq	%rax, %rbx
	leaq	-352(%rbp), %r12
	jmp	.L2273
.L2507:
	endbr64
	movq	%rax, %rdi
	jmp	.L2261
.L2503:
	endbr64
	movq	%rax, %rbx
	jmp	.L2459
	.section	.gcc_except_table._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_32CallCsaMacroAndBranchInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE,"a",@progbits
	.align 4
.LLSDA6930:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATT6930-.LLSDATTD6930
.LLSDATTD6930:
	.byte	0x1
	.uleb128 .LLSDACSE6930-.LLSDACSB6930
.LLSDACSB6930:
	.uleb128 .LEHB324-.LFB6930
	.uleb128 .LEHE324-.LEHB324
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB325-.LFB6930
	.uleb128 .LEHE325-.LEHB325
	.uleb128 .L2505-.LFB6930
	.uleb128 0x1
	.uleb128 .LEHB326-.LFB6930
	.uleb128 .LEHE326-.LEHB326
	.uleb128 .L2483-.LFB6930
	.uleb128 0
	.uleb128 .LEHB327-.LFB6930
	.uleb128 .LEHE327-.LEHB327
	.uleb128 .L2484-.LFB6930
	.uleb128 0
	.uleb128 .LEHB328-.LFB6930
	.uleb128 .LEHE328-.LEHB328
	.uleb128 .L2507-.LFB6930
	.uleb128 0x1
	.uleb128 .LEHB329-.LFB6930
	.uleb128 .LEHE329-.LEHB329
	.uleb128 .L2485-.LFB6930
	.uleb128 0
	.uleb128 .LEHB330-.LFB6930
	.uleb128 .LEHE330-.LEHB330
	.uleb128 .L2508-.LFB6930
	.uleb128 0
	.uleb128 .LEHB331-.LFB6930
	.uleb128 .LEHE331-.LEHB331
	.uleb128 .L2485-.LFB6930
	.uleb128 0
	.uleb128 .LEHB332-.LFB6930
	.uleb128 .LEHE332-.LEHB332
	.uleb128 .L2486-.LFB6930
	.uleb128 0
	.uleb128 .LEHB333-.LFB6930
	.uleb128 .LEHE333-.LEHB333
	.uleb128 .L2488-.LFB6930
	.uleb128 0
	.uleb128 .LEHB334-.LFB6930
	.uleb128 .LEHE334-.LEHB334
	.uleb128 .L2486-.LFB6930
	.uleb128 0
	.uleb128 .LEHB335-.LFB6930
	.uleb128 .LEHE335-.LEHB335
	.uleb128 .L2509-.LFB6930
	.uleb128 0
	.uleb128 .LEHB336-.LFB6930
	.uleb128 .LEHE336-.LEHB336
	.uleb128 .L2499-.LFB6930
	.uleb128 0
	.uleb128 .LEHB337-.LFB6930
	.uleb128 .LEHE337-.LEHB337
	.uleb128 .L2512-.LFB6930
	.uleb128 0
	.uleb128 .LEHB338-.LFB6930
	.uleb128 .LEHE338-.LEHB338
	.uleb128 .L2490-.LFB6930
	.uleb128 0
	.uleb128 .LEHB339-.LFB6930
	.uleb128 .LEHE339-.LEHB339
	.uleb128 .L2512-.LFB6930
	.uleb128 0
	.uleb128 .LEHB340-.LFB6930
	.uleb128 .LEHE340-.LEHB340
	.uleb128 .L2493-.LFB6930
	.uleb128 0
	.uleb128 .LEHB341-.LFB6930
	.uleb128 .LEHE341-.LEHB341
	.uleb128 .L2494-.LFB6930
	.uleb128 0
	.uleb128 .LEHB342-.LFB6930
	.uleb128 .LEHE342-.LEHB342
	.uleb128 .L2495-.LFB6930
	.uleb128 0
	.uleb128 .LEHB343-.LFB6930
	.uleb128 .LEHE343-.LEHB343
	.uleb128 .L2496-.LFB6930
	.uleb128 0
	.uleb128 .LEHB344-.LFB6930
	.uleb128 .LEHE344-.LEHB344
	.uleb128 .L2512-.LFB6930
	.uleb128 0
	.uleb128 .LEHB345-.LFB6930
	.uleb128 .LEHE345-.LEHB345
	.uleb128 .L2489-.LFB6930
	.uleb128 0
	.uleb128 .LEHB346-.LFB6930
	.uleb128 .LEHE346-.LEHB346
	.uleb128 .L2498-.LFB6930
	.uleb128 0
	.uleb128 .LEHB347-.LFB6930
	.uleb128 .LEHE347-.LEHB347
	.uleb128 .L2512-.LFB6930
	.uleb128 0
	.uleb128 .LEHB348-.LFB6930
	.uleb128 .LEHE348-.LEHB348
	.uleb128 .L2499-.LFB6930
	.uleb128 0
	.uleb128 .LEHB349-.LFB6930
	.uleb128 .LEHE349-.LEHB349
	.uleb128 .L2500-.LFB6930
	.uleb128 0
	.uleb128 .LEHB350-.LFB6930
	.uleb128 .LEHE350-.LEHB350
	.uleb128 .L2496-.LFB6930
	.uleb128 0
	.uleb128 .LEHB351-.LFB6930
	.uleb128 .LEHE351-.LEHB351
	.uleb128 .L2497-.LFB6930
	.uleb128 0
	.uleb128 .LEHB352-.LFB6930
	.uleb128 .LEHE352-.LEHB352
	.uleb128 .L2492-.LFB6930
	.uleb128 0
	.uleb128 .LEHB353-.LFB6930
	.uleb128 .LEHE353-.LEHB353
	.uleb128 .L2491-.LFB6930
	.uleb128 0
	.uleb128 .LEHB354-.LFB6930
	.uleb128 .LEHE354-.LEHB354
	.uleb128 .L2500-.LFB6930
	.uleb128 0
	.uleb128 .LEHB355-.LFB6930
	.uleb128 .LEHE355-.LEHB355
	.uleb128 .L2511-.LFB6930
	.uleb128 0
	.uleb128 .LEHB356-.LFB6930
	.uleb128 .LEHE356-.LEHB356
	.uleb128 .L2503-.LFB6930
	.uleb128 0
	.uleb128 .LEHB357-.LFB6930
	.uleb128 .LEHE357-.LEHB357
	.uleb128 .L2500-.LFB6930
	.uleb128 0
	.uleb128 .LEHB358-.LFB6930
	.uleb128 .LEHE358-.LEHB358
	.uleb128 .L2487-.LFB6930
	.uleb128 0
	.uleb128 .LEHB359-.LFB6930
	.uleb128 .LEHE359-.LEHB359
	.uleb128 .L2501-.LFB6930
	.uleb128 0
	.uleb128 .LEHB360-.LFB6930
	.uleb128 .LEHE360-.LEHB360
	.uleb128 .L2500-.LFB6930
	.uleb128 0
	.uleb128 .LEHB361-.LFB6930
	.uleb128 .LEHE361-.LEHB361
	.uleb128 .L2510-.LFB6930
	.uleb128 0
	.uleb128 .LEHB362-.LFB6930
	.uleb128 .LEHE362-.LEHB362
	.uleb128 .L2502-.LFB6930
	.uleb128 0
	.uleb128 .LEHB363-.LFB6930
	.uleb128 .LEHE363-.LEHB363
	.uleb128 .L2500-.LFB6930
	.uleb128 0
	.uleb128 .LEHB364-.LFB6930
	.uleb128 .LEHE364-.LEHB364
	.uleb128 .L2495-.LFB6930
	.uleb128 0
	.uleb128 .LEHB365-.LFB6930
	.uleb128 .LEHE365-.LEHB365
	.uleb128 .L2483-.LFB6930
	.uleb128 0
	.uleb128 .LEHB366-.LFB6930
	.uleb128 .LEHE366-.LEHB366
	.uleb128 .L2505-.LFB6930
	.uleb128 0x1
	.uleb128 .LEHB367-.LFB6930
	.uleb128 .LEHE367-.LEHB367
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB368-.LFB6930
	.uleb128 .LEHE368-.LEHB368
	.uleb128 .L2507-.LFB6930
	.uleb128 0x1
	.uleb128 .LEHB369-.LFB6930
	.uleb128 .LEHE369-.LEHB369
	.uleb128 .L2484-.LFB6930
	.uleb128 0
	.uleb128 .LEHB370-.LFB6930
	.uleb128 .LEHE370-.LEHB370
	.uleb128 .L2499-.LFB6930
	.uleb128 0
.LLSDACSE6930:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATT6930:
	.section	.text._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_32CallCsaMacroAndBranchInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_32CallCsaMacroAndBranchInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6930
	.type	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_32CallCsaMacroAndBranchInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.cold, @function
_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_32CallCsaMacroAndBranchInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.cold:
.LFSB6930:
.L2440:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-96(%rbp), %rdi
	cmpq	-496(%rbp), %rdi
	je	.L2442
	call	_ZdlPv@PLT
.L2442:
	movq	-160(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L2444
	call	_ZdlPv@PLT
.L2444:
	movq	-192(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L2446
	call	_ZdlPv@PLT
.L2446:
	movq	-224(%rbp), %rdi
	leaq	-208(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2448
	call	_ZdlPv@PLT
.L2448:
	movq	-128(%rbp), %rdi
	cmpq	-512(%rbp), %rdi
	je	.L2449
	call	_ZdlPv@PLT
.L2449:
	leaq	-320(%rbp), %r12
.L2438:
	cmpq	$0, -544(%rbp)
	je	.L2454
.L2471:
	movq	-544(%rbp), %rdi
	call	_ZdlPv@PLT
.L2454:
	movq	-280(%rbp), %rax
	movq	-288(%rbp), %r15
	movq	%rax, -488(%rbp)
.L2467:
	cmpq	%r15, -488(%rbp)
	jne	.L2643
	movq	-288(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2468
	call	_ZdlPv@PLT
.L2468:
	movq	%r12, %rdi
	leaq	-352(%rbp), %r12
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EED1Ev
.L2273:
	movq	%r12, %rdi
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EED1Ev
	leaq	-384(%rbp), %rdi
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EED1Ev
.L2267:
	movq	-416(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2470
	call	_ZdlPv@PLT
.L2470:
	movq	-600(%rbp), %rdi
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EED1Ev
	movq	-608(%rbp), %rdi
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EED1Ev
	movq	%rbx, %rdi
.LEHB371:
	call	_Unwind_Resume@PLT
.LEHE371:
.L2459:
	movq	-128(%rbp), %rdi
	cmpq	-512(%rbp), %rdi
	je	.L2361
.L2609:
	call	_ZdlPv@PLT
.L2361:
	movq	-160(%rbp), %rdi
	leaq	-144(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2461
	call	_ZdlPv@PLT
.L2461:
	leaq	-320(%rbp), %r12
	jmp	.L2454
.L2643:
	movq	8(%r15), %r14
	movq	(%r15), %r13
.L2465:
	cmpq	%r13, %r14
	jne	.L2644
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L2466
	call	_ZdlPv@PLT
.L2466:
	addq	$24, %r15
	jmp	.L2467
.L2644:
	movq	0(%r13), %rdi
	leaq	16(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2464
	call	_ZdlPv@PLT
.L2464:
	addq	$32, %r13
	jmp	.L2465
.L2455:
	movq	-96(%rbp), %rdi
	cmpq	-496(%rbp), %rdi
	jne	.L2609
	jmp	.L2361
.L2280:
	movq	-96(%rbp), %rdi
	cmpq	-496(%rbp), %rdi
	je	.L2281
	call	_ZdlPv@PLT
.L2281:
	leaq	-352(%rbp), %r12
.L2282:
	movq	-256(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2273
	call	_ZdlPv@PLT
	jmp	.L2273
.L2431:
	movq	-96(%rbp), %rdi
	cmpq	-496(%rbp), %rdi
	je	.L2432
	call	_ZdlPv@PLT
.L2432:
	leaq	-352(%rbp), %r12
	jmp	.L2282
.L2429:
	movq	-128(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L2282
	call	_ZdlPv@PLT
	jmp	.L2282
.L2271:
	movq	-96(%rbp), %rdi
	cmpq	-496(%rbp), %rdi
	je	.L2272
	call	_ZdlPv@PLT
.L2272:
	leaq	-352(%rbp), %r12
	jmp	.L2273
.L2434:
	movq	-96(%rbp), %rdi
	cmpq	-496(%rbp), %rdi
	je	.L2436
	call	_ZdlPv@PLT
.L2436:
	movq	-128(%rbp), %rdi
	leaq	-112(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2438
	call	_ZdlPv@PLT
	jmp	.L2438
.L2450:
	movq	-96(%rbp), %rdi
	cmpq	-496(%rbp), %rdi
	je	.L2452
	call	_ZdlPv@PLT
.L2452:
	leaq	-320(%rbp), %r12
	jmp	.L2471
.L2242:
	call	__cxa_begin_catch@PLT
	movq	-504(%rbp), %r12
.L2245:
	cmpq	%r12, %rbx
	jne	.L2645
.LEHB372:
	call	__cxa_rethrow@PLT
.LEHE372:
.L2645:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2244
	call	_ZdlPv@PLT
.L2244:
	addq	$32, %r12
	jmp	.L2245
.L2504:
	endbr64
	movq	%rax, %r12
	call	__cxa_end_catch@PLT
	movq	-480(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2247
	call	_ZdlPv@PLT
.L2247:
	movq	%r12, %rdi
.LEHB373:
	call	_Unwind_Resume@PLT
.LEHE373:
.L2439:
	movq	%r12, %rdi
	leaq	-320(%rbp), %r12
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EED1Ev
	jmp	.L2438
.L2261:
	call	__cxa_begin_catch@PLT
	movq	-504(%rbp), %r12
.L2264:
	cmpq	%r12, %rbx
	jne	.L2646
.LEHB374:
	call	__cxa_rethrow@PLT
.LEHE374:
.L2646:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2263
	call	_ZdlPv@PLT
.L2263:
	addq	$32, %r12
	jmp	.L2264
.L2506:
	endbr64
	movq	%rax, %rbx
	call	__cxa_end_catch@PLT
	movq	-384(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2267
	call	_ZdlPv@PLT
	jmp	.L2267
	.cfi_endproc
.LFE6930:
	.section	.gcc_except_table._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_32CallCsaMacroAndBranchInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.align 4
.LLSDAC6930:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATTC6930-.LLSDATTDC6930
.LLSDATTDC6930:
	.byte	0x1
	.uleb128 .LLSDACSEC6930-.LLSDACSBC6930
.LLSDACSBC6930:
	.uleb128 .LEHB371-.LCOLDB162
	.uleb128 .LEHE371-.LEHB371
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB372-.LCOLDB162
	.uleb128 .LEHE372-.LEHB372
	.uleb128 .L2504-.LCOLDB162
	.uleb128 0
	.uleb128 .LEHB373-.LCOLDB162
	.uleb128 .LEHE373-.LEHB373
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB374-.LCOLDB162
	.uleb128 .LEHE374-.LEHB374
	.uleb128 .L2506-.LCOLDB162
	.uleb128 0
.LLSDACSEC6930:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATTC6930:
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_32CallCsaMacroAndBranchInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.section	.text._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_32CallCsaMacroAndBranchInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.size	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_32CallCsaMacroAndBranchInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE, .-_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_32CallCsaMacroAndBranchInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_32CallCsaMacroAndBranchInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.size	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_32CallCsaMacroAndBranchInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.cold, .-_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_32CallCsaMacroAndBranchInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.cold
.LCOLDE162:
	.section	.text._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_32CallCsaMacroAndBranchInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
.LHOTE162:
	.section	.text._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_11InstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_11InstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.type	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_11InstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE, @function
_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_11InstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE:
.LFB6914:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$56, %rsp
	movq	8(%rsi), %rax
	xorl	%esi, %esi
	movdqu	8(%rax), %xmm0
	movups	%xmm0, (%rsp)
	movl	24(%rax), %eax
	movl	%eax, 16(%rsp)
	call	_ZN2v88internal6torque12CSAGenerator18EmitSourcePositionENS1_14SourcePositionEb
	addq	$32, %rsp
	cmpl	$22, (%rbx)
	ja	.L2647
	movl	(%rbx), %eax
	leaq	.L2650(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_11InstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE,"a",@progbits
	.align 4
	.align 4
.L2650:
	.long	.L2672-.L2650
	.long	.L2671-.L2650
	.long	.L2670-.L2650
	.long	.L2669-.L2650
	.long	.L2668-.L2650
	.long	.L2667-.L2650
	.long	.L2666-.L2650
	.long	.L2665-.L2650
	.long	.L2664-.L2650
	.long	.L2663-.L2650
	.long	.L2662-.L2650
	.long	.L2661-.L2650
	.long	.L2660-.L2650
	.long	.L2659-.L2650
	.long	.L2658-.L2650
	.long	.L2657-.L2650
	.long	.L2656-.L2650
	.long	.L2655-.L2650
	.long	.L2654-.L2650
	.long	.L2653-.L2650
	.long	.L2652-.L2650
	.long	.L2651-.L2650
	.long	.L2649-.L2650
	.section	.text._ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_11InstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.p2align 4,,10
	.p2align 3
.L2675:
	movq	%rax, %rdx
	xorl	%ecx, %ecx
	addq	%r8, %rdx
	setc	%cl
	cmpq	%rax, %rdx
	ja	.L2702
	testq	%rcx, %rcx
	je	.L2647
	salq	$5, %rdx
	leaq	(%rbx,%rdx), %r14
	cmpq	%r12, %r14
	je	.L2647
	movq	%r14, %rbx
	.p2align 4,,10
	.p2align 3
.L2690:
	movq	(%rbx), %rdi
	leaq	16(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2687
	call	_ZdlPv@PLT
	addq	$32, %rbx
	cmpq	%r12, %rbx
	jne	.L2690
.L2688:
	movq	%r14, 8(%r13)
.L2647:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2651:
	.cfi_restore_state
	movq	8(%rbx), %rsi
	leaq	-40(%rbp), %rsp
	movq	%r13, %rdx
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_16AbortInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.p2align 4,,10
	.p2align 3
.L2652:
	.cfi_restore_state
	movq	8(%rbx), %rsi
	leaq	-40(%rbp), %rsp
	movq	%r13, %rdx
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_30PrintConstantStringInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.p2align 4,,10
	.p2align 3
.L2653:
	.cfi_restore_state
	movq	8(%rbx), %rsi
	leaq	-40(%rbp), %rsp
	movq	%r13, %rdx
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_17ReturnInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.p2align 4,,10
	.p2align 3
.L2654:
	.cfi_restore_state
	movq	8(%rbx), %rsi
	leaq	-40(%rbp), %rsp
	movq	%r13, %rdx
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_23GotoExternalInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.p2align 4,,10
	.p2align 3
.L2655:
	.cfi_restore_state
	movq	8(%rbx), %rsi
	leaq	-40(%rbp), %rsp
	movq	%r13, %rdx
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_15GotoInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.p2align 4,,10
	.p2align 3
.L2656:
	.cfi_restore_state
	movq	8(%rbx), %rsi
	leaq	-40(%rbp), %rsp
	movq	%r13, %rdx
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_26ConstexprBranchInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.p2align 4,,10
	.p2align 3
.L2657:
	.cfi_restore_state
	movq	8(%rbx), %rsi
	leaq	-40(%rbp), %rsp
	movq	%r13, %rdx
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_17BranchInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.p2align 4,,10
	.p2align 3
.L2658:
	.cfi_restore_state
	movq	8(%rbx), %rsi
	leaq	-40(%rbp), %rsp
	movq	%r13, %rdx
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_29CallBuiltinPointerInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.p2align 4,,10
	.p2align 3
.L2659:
	.cfi_restore_state
	movq	8(%rbx), %rsi
	leaq	-40(%rbp), %rsp
	movq	%r13, %rdx
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_22CallRuntimeInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.p2align 4,,10
	.p2align 3
.L2660:
	.cfi_restore_state
	movq	8(%rbx), %rsi
	leaq	-40(%rbp), %rsp
	movq	%r13, %rdx
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_22CallBuiltinInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.p2align 4,,10
	.p2align 3
.L2661:
	.cfi_restore_state
	movq	8(%rbx), %rsi
	leaq	-40(%rbp), %rsp
	movq	%r13, %rdx
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_32CallCsaMacroAndBranchInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.p2align 4,,10
	.p2align 3
.L2662:
	.cfi_restore_state
	movq	8(%rbx), %rsi
	leaq	-40(%rbp), %rsp
	movq	%r13, %rdx
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_28NamespaceConstantInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.p2align 4,,10
	.p2align 3
.L2663:
	.cfi_restore_state
	movq	8(%rbx), %rsi
	leaq	-40(%rbp), %rsp
	movq	%r13, %rdx
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_24CallIntrinsicInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.p2align 4,,10
	.p2align 3
.L2664:
	.cfi_restore_state
	movq	8(%rbx), %rsi
	leaq	-40(%rbp), %rsp
	movq	%r13, %rdx
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_23CallCsaMacroInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.p2align 4,,10
	.p2align 3
.L2665:
	.cfi_restore_state
	movq	8(%rbx), %rsi
	leaq	-40(%rbp), %rsp
	movq	%r13, %rdx
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_25StoreReferenceInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.p2align 4,,10
	.p2align 3
.L2666:
	.cfi_restore_state
	movq	8(%rbx), %rsi
	leaq	-40(%rbp), %rsp
	movq	%r13, %rdx
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_24LoadReferenceInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.p2align 4,,10
	.p2align 3
.L2667:
	.cfi_restore_state
	movq	8(%rbx), %rsi
	leaq	-40(%rbp), %rsp
	movq	%r13, %rdx
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_31CreateFieldReferenceInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.p2align 4,,10
	.p2align 3
.L2668:
	.cfi_restore_state
	movq	8(%rbx), %rsi
	leaq	-40(%rbp), %rsp
	movq	%r13, %rdx
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_29PushBuiltinPointerInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.p2align 4,,10
	.p2align 3
.L2669:
	.cfi_restore_state
	movq	8(%rbx), %rsi
	leaq	-40(%rbp), %rsp
	movq	%r13, %rdx
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_28PushUninitializedInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.p2align 4,,10
	.p2align 3
.L2670:
	.cfi_restore_state
	movq	8(%rbx), %rax
	movq	32(%rax), %r15
	movq	40(%rax), %r14
	cmpq	%r15, %r14
	je	.L2647
	movq	8(%r13), %r12
	movq	0(%r13), %rbx
	movq	%r15, %r8
	subq	%r14, %r8
	movq	%r12, %rax
	subq	%rbx, %rax
	sarq	$5, %rax
	cmpq	%rax, %r14
	jnb	.L2675
	salq	$5, %r15
	jmp	.L2682
	.p2align 4,,10
	.p2align 3
.L2676:
	leaq	16(%rbx), %rdx
	cmpq	%rdx, %rdi
	je	.L2703
	movq	%rsi, (%rbx)
	movq	8(%r12), %rcx
	movq	16(%rbx), %rdx
	movq	%rcx, 8(%rbx)
	movq	16(%r12), %rcx
	movq	%rcx, 16(%rbx)
	testq	%rdi, %rdi
	je	.L2681
	movq	%rdi, (%r12)
	movq	%rdx, 16(%r12)
.L2679:
	movq	$0, 8(%r12)
	addq	$1, %r14
	addq	$32, %r15
	movb	$0, (%rdi)
	movq	8(%r13), %r12
	movq	0(%r13), %rbx
	movq	%r12, %rax
	subq	%rbx, %rax
	sarq	$5, %rax
	cmpq	%r14, %rax
	jbe	.L2675
.L2682:
	movq	%r14, %r12
	salq	$5, %r12
	addq	%rbx, %r12
	addq	%r15, %rbx
	movq	(%r12), %rsi
	leaq	16(%r12), %rax
	movq	(%rbx), %rdi
	cmpq	%rax, %rsi
	jne	.L2676
	movq	8(%r12), %rdx
	testq	%rdx, %rdx
	je	.L2677
	cmpq	$1, %rdx
	je	.L2704
	movq	%r8, -56(%rbp)
	call	memcpy@PLT
	movq	8(%r12), %rdx
	movq	(%rbx), %rdi
	movq	-56(%rbp), %r8
.L2677:
	movq	%rdx, 8(%rbx)
	movb	$0, (%rdi,%rdx)
	movq	(%r12), %rdi
	jmp	.L2679
	.p2align 4,,10
	.p2align 3
.L2671:
	movq	8(%rbx), %rsi
	leaq	-40(%rbp), %rsp
	movq	%r13, %rdx
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_15PokeInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.p2align 4,,10
	.p2align 3
.L2672:
	.cfi_restore_state
	movq	8(%rbx), %rsi
	leaq	-40(%rbp), %rsp
	movq	%r13, %rdx
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_15PeekInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.p2align 4,,10
	.p2align 3
.L2649:
	.cfi_restore_state
	movq	8(%rbx), %rsi
	leaq	-40(%rbp), %rsp
	movq	%r13, %rdx
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_21UnsafeCastInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.p2align 4,,10
	.p2align 3
.L2703:
	.cfi_restore_state
	movq	%rsi, (%rbx)
	movq	8(%r12), %rdx
	movq	%rdx, 8(%rbx)
	movq	16(%r12), %rdx
	movq	%rdx, 16(%rbx)
.L2681:
	movq	%rax, (%r12)
	movq	%rax, %rdi
	jmp	.L2679
	.p2align 4,,10
	.p2align 3
.L2687:
	addq	$32, %rbx
	cmpq	%r12, %rbx
	jne	.L2690
	jmp	.L2688
	.p2align 4,,10
	.p2align 3
.L2704:
	movzbl	16(%r12), %eax
	movb	%al, (%rdi)
	movq	8(%r12), %rdx
	movq	(%rbx), %rdi
	jmp	.L2677
	.p2align 4,,10
	.p2align 3
.L2702:
	leaq	-40(%rbp), %rsp
	movq	%r13, %rdi
	movq	%r8, %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_default_appendEm
	.cfi_endproc
.LFE6914:
	.size	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_11InstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE, .-_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_11InstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.section	.rodata._ZN2v88internal6torque12CSAGenerator9EmitBlockB5cxx11EPKNS1_5BlockE.str1.1,"aMS",@progbits,1
.LC163:
	.string	"    ca_.Bind(&"
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator9EmitBlockB5cxx11EPKNS1_5BlockE,"ax",@progbits
	.align 2
.LCOLDB164:
	.section	.text._ZN2v88internal6torque12CSAGenerator9EmitBlockB5cxx11EPKNS1_5BlockE,"ax",@progbits
.LHOTB164:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque12CSAGenerator9EmitBlockB5cxx11EPKNS1_5BlockE
	.type	_ZN2v88internal6torque12CSAGenerator9EmitBlockB5cxx11EPKNS1_5BlockE, @function
_ZN2v88internal6torque12CSAGenerator9EmitBlockB5cxx11EPKNS1_5BlockE:
.LFB6906:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6906
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$136, %rsp
	movq	%rdx, -168(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, 16(%rdi)
	movups	%xmm0, (%rdi)
	movq	48(%rdx), %rcx
	movq	40(%rdx), %rax
	movq	%rcx, -160(%rbp)
	cmpq	%rcx, %rax
	je	.L2721
	movq	%rax, %r15
	leaq	-112(%rbp), %rax
	leaq	-96(%rbp), %r14
	movq	%rax, -136(%rbp)
	.p2align 4,,10
	.p2align 3
.L2723:
	movq	(%r15), %rax
	movq	16(%rbx), %r8
	movl	$32, %edx
	movq	%r14, %rdi
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	leaq	.LC0(%rip), %rcx
	movq	%rax, -144(%rbp)
	leaq	1(%r8), %rax
	movq	%rax, 16(%rbx)
	xorl	%eax, %eax
.LEHB375:
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0
.LEHE375:
	movl	$3, %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r14, %rdi
	leaq	.LC73(%rip), %rcx
.LEHB376:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.LEHE376:
	movq	-136(%rbp), %rcx
	leaq	16(%rax), %rdx
	movq	%rcx, -128(%rbp)
	movq	(%rax), %rcx
	cmpq	%rdx, %rcx
	je	.L2762
	movq	%rcx, -128(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -112(%rbp)
.L2709:
	movq	8(%rax), %rcx
	movq	%rcx, -120(%rbp)
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movb	$0, 16(%rax)
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	movq	%rax, -152(%rbp)
	cmpq	%rax, %rdi
	je	.L2710
	call	_ZdlPv@PLT
.L2710:
	movq	8(%r12), %rsi
	cmpq	16(%r12), %rsi
	je	.L2763
	leaq	16(%rsi), %rax
	movq	%rax, (%rsi)
	movq	-128(%rbp), %rax
	cmpq	-136(%rbp), %rax
	je	.L2764
	movq	%rax, (%rsi)
	movq	-112(%rbp), %rax
	movq	%rax, 16(%rsi)
.L2717:
	movq	-120(%rbp), %rax
	movq	%rax, 8(%rsi)
	addq	$32, 8(%r12)
.L2718:
	movq	8(%rbx), %r13
	movl	$20, %edx
	leaq	.LC74(%rip), %rsi
	movq	%r13, %rdi
.LEHB377:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-144(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZNK2v88internal6torque4Type25GetGeneratedTNodeTypeNameB5cxx11Ev@PLT
.LEHE377:
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r13, %rdi
.LEHB378:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$2, %edx
	leaq	.LC75(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r12), %rdx
	movq	8(%r12), %rax
	subq	%rdx, %rax
	je	.L2765
	leaq	-32(%rdx,%rax), %rax
	movq	%r13, %rdi
	movq	8(%rax), %rdx
	movq	(%rax), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rax, %rdi
	movl	$2, %edx
	leaq	.LC40(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE378:
	movq	-96(%rbp), %rdi
	cmpq	-152(%rbp), %rdi
	je	.L2720
	call	_ZdlPv@PLT
	addq	$8, %r15
	cmpq	%r15, -160(%rbp)
	jne	.L2723
.L2721:
	movq	8(%rbx), %r13
	movl	$14, %edx
	leaq	.LC163(%rip), %rsi
	movq	%r13, %rdi
.LEHB379:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-168(%rbp), %rax
	leaq	-96(%rbp), %r14
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	leaq	.LC0(%rip), %rcx
	movl	$32, %edx
	movq	%r14, %rdi
	movq	64(%rax), %r8
	xorl	%eax, %eax
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0
.LEHE379:
	movl	$5, %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r14, %rdi
	leaq	.LC14(%rip), %rcx
.LEHB380:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.LEHE380:
	leaq	-112(%rbp), %r14
	leaq	16(%rax), %rdx
	movq	%r14, -128(%rbp)
	movq	(%rax), %rcx
	cmpq	%rdx, %rcx
	je	.L2766
	movq	%rcx, -128(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -112(%rbp)
.L2725:
	movq	8(%rax), %rcx
	movq	%rcx, -120(%rbp)
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movb	$0, 16(%rax)
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2726
	call	_ZdlPv@PLT
.L2726:
	movq	-120(%rbp), %rdx
	movq	-128(%rbp), %rsi
	movq	%r13, %rdi
.LEHB381:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE381:
	movq	-128(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L2730
	call	_ZdlPv@PLT
.L2730:
	movq	8(%r12), %rax
	movq	(%r12), %r13
	leaq	.LC30(%rip), %r15
	movq	%rax, -136(%rbp)
	cmpq	%r13, %rax
	je	.L2733
	.p2align 4,,10
	.p2align 3
.L2734:
	movq	8(%rbx), %r14
	movl	$3, %edx
	movq	%r15, %rsi
	movq	%r14, %rdi
.LEHB382:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%r13), %rdx
	movq	0(%r13), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$32, %r13
	cmpq	%r13, -136(%rbp)
	jne	.L2734
.L2733:
	movq	8(%rbx), %rdi
	movl	$3, %edx
	leaq	.LC3(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-168(%rbp), %rax
	movq	16(%rax), %r14
	movq	8(%rax), %r13
	cmpq	%r14, %r13
	je	.L2705
	.p2align 4,,10
	.p2align 3
.L2736:
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_11InstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
.LEHE382:
	addq	$16, %r13
	cmpq	%r13, %r14
	jne	.L2736
.L2705:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2767
	addq	$136, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2720:
	.cfi_restore_state
	addq	$8, %r15
	cmpq	%r15, -160(%rbp)
	jne	.L2723
	jmp	.L2721
	.p2align 4,,10
	.p2align 3
.L2762:
	movdqu	16(%rax), %xmm1
	movaps	%xmm1, -112(%rbp)
	jmp	.L2709
	.p2align 4,,10
	.p2align 3
.L2764:
	movdqa	-112(%rbp), %xmm2
	movups	%xmm2, 16(%rsi)
	jmp	.L2717
	.p2align 4,,10
	.p2align 3
.L2763:
	leaq	-128(%rbp), %rdx
	movq	%r12, %rdi
.LEHB383:
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
.LEHE383:
	movq	-128(%rbp), %rdi
	cmpq	-136(%rbp), %rdi
	je	.L2718
	call	_ZdlPv@PLT
	jmp	.L2718
	.p2align 4,,10
	.p2align 3
.L2766:
	movdqu	16(%rax), %xmm3
	movaps	%xmm3, -112(%rbp)
	jmp	.L2725
.L2765:
	xorl	%edx, %edx
	movq	$-1, %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
.LEHB384:
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.LEHE384:
.L2767:
	call	__stack_chk_fail@PLT
.L2750:
	endbr64
	movq	%rax, %r13
	jmp	.L2728
.L2747:
	endbr64
	movq	%rax, %r13
	jmp	.L2740
.L2749:
	endbr64
	movq	%rax, %r13
	jmp	.L2728
.L2745:
	endbr64
	movq	%rax, %r13
	jmp	.L2715
.L2746:
	endbr64
	movq	%rax, %r13
	jmp	.L2738
.L2748:
	endbr64
	movq	%rax, %r13
	jmp	.L2742
	.section	.gcc_except_table._ZN2v88internal6torque12CSAGenerator9EmitBlockB5cxx11EPKNS1_5BlockE,"a",@progbits
.LLSDA6906:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6906-.LLSDACSB6906
.LLSDACSB6906:
	.uleb128 .LEHB375-.LFB6906
	.uleb128 .LEHE375-.LEHB375
	.uleb128 .L2745-.LFB6906
	.uleb128 0
	.uleb128 .LEHB376-.LFB6906
	.uleb128 .LEHE376-.LEHB376
	.uleb128 .L2749-.LFB6906
	.uleb128 0
	.uleb128 .LEHB377-.LFB6906
	.uleb128 .LEHE377-.LEHB377
	.uleb128 .L2745-.LFB6906
	.uleb128 0
	.uleb128 .LEHB378-.LFB6906
	.uleb128 .LEHE378-.LEHB378
	.uleb128 .L2747-.LFB6906
	.uleb128 0
	.uleb128 .LEHB379-.LFB6906
	.uleb128 .LEHE379-.LEHB379
	.uleb128 .L2745-.LFB6906
	.uleb128 0
	.uleb128 .LEHB380-.LFB6906
	.uleb128 .LEHE380-.LEHB380
	.uleb128 .L2750-.LFB6906
	.uleb128 0
	.uleb128 .LEHB381-.LFB6906
	.uleb128 .LEHE381-.LEHB381
	.uleb128 .L2748-.LFB6906
	.uleb128 0
	.uleb128 .LEHB382-.LFB6906
	.uleb128 .LEHE382-.LEHB382
	.uleb128 .L2745-.LFB6906
	.uleb128 0
	.uleb128 .LEHB383-.LFB6906
	.uleb128 .LEHE383-.LEHB383
	.uleb128 .L2746-.LFB6906
	.uleb128 0
	.uleb128 .LEHB384-.LFB6906
	.uleb128 .LEHE384-.LEHB384
	.uleb128 .L2747-.LFB6906
	.uleb128 0
.LLSDACSE6906:
	.section	.text._ZN2v88internal6torque12CSAGenerator9EmitBlockB5cxx11EPKNS1_5BlockE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator9EmitBlockB5cxx11EPKNS1_5BlockE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6906
	.type	_ZN2v88internal6torque12CSAGenerator9EmitBlockB5cxx11EPKNS1_5BlockE.cold, @function
_ZN2v88internal6torque12CSAGenerator9EmitBlockB5cxx11EPKNS1_5BlockE.cold:
.LFSB6906:
.L2728:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2715
.L2759:
	call	_ZdlPv@PLT
.L2715:
	movq	%r12, %rdi
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EED1Ev
	movq	%r13, %rdi
.LEHB385:
	call	_Unwind_Resume@PLT
.LEHE385:
.L2740:
	movq	-96(%rbp), %rdi
	cmpq	-152(%rbp), %rdi
	jne	.L2759
	jmp	.L2715
.L2738:
	movq	-128(%rbp), %rdi
	cmpq	-136(%rbp), %rdi
	jne	.L2759
	jmp	.L2715
.L2742:
	movq	-128(%rbp), %rdi
	cmpq	%r14, %rdi
	jne	.L2759
	jmp	.L2715
	.cfi_endproc
.LFE6906:
	.section	.gcc_except_table._ZN2v88internal6torque12CSAGenerator9EmitBlockB5cxx11EPKNS1_5BlockE
.LLSDAC6906:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6906-.LLSDACSBC6906
.LLSDACSBC6906:
	.uleb128 .LEHB385-.LCOLDB164
	.uleb128 .LEHE385-.LEHB385
	.uleb128 0
	.uleb128 0
.LLSDACSEC6906:
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator9EmitBlockB5cxx11EPKNS1_5BlockE
	.section	.text._ZN2v88internal6torque12CSAGenerator9EmitBlockB5cxx11EPKNS1_5BlockE
	.size	_ZN2v88internal6torque12CSAGenerator9EmitBlockB5cxx11EPKNS1_5BlockE, .-_ZN2v88internal6torque12CSAGenerator9EmitBlockB5cxx11EPKNS1_5BlockE
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator9EmitBlockB5cxx11EPKNS1_5BlockE
	.size	_ZN2v88internal6torque12CSAGenerator9EmitBlockB5cxx11EPKNS1_5BlockE.cold, .-_ZN2v88internal6torque12CSAGenerator9EmitBlockB5cxx11EPKNS1_5BlockE.cold
.LCOLDE164:
	.section	.text._ZN2v88internal6torque12CSAGenerator9EmitBlockB5cxx11EPKNS1_5BlockE
.LHOTE164:
	.section	.rodata._ZN2v88internal6torque12CSAGenerator9EmitGraphENS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.str1.1,"aMS",@progbits,1
.LC165:
	.string	"kDeferred"
.LC166:
	.string	"kNonDeferred"
	.section	.rodata._ZN2v88internal6torque12CSAGenerator9EmitGraphENS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC167:
	.string	"  compiler::CodeAssemblerParameterizedLabel<"
	.align 8
.LC168:
	.string	"(&ca_, compiler::CodeAssemblerLabel::"
	.section	.rodata._ZN2v88internal6torque12CSAGenerator9EmitGraphENS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.str1.1
.LC169:
	.string	"\n  if ("
.LC170:
	.string	"  }\n"
.LC171:
	.string	"\n"
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator9EmitGraphENS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE,"ax",@progbits
	.align 2
.LCOLDB172:
	.section	.text._ZN2v88internal6torque12CSAGenerator9EmitGraphENS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE,"ax",@progbits
.LHOTB172:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque12CSAGenerator9EmitGraphENS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.type	_ZN2v88internal6torque12CSAGenerator9EmitGraphENS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE, @function
_ZN2v88internal6torque12CSAGenerator9EmitGraphENS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE:
.LFB6884:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6884
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$200, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -232(%rbp)
	movq	%rdi, -224(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	40(%rax), %rbx
	movq	32(%rax), %rdx
	movq	%rbx, -216(%rbp)
	cmpq	%rbx, %rdx
	je	.L2769
	leaq	-80(%rbp), %rax
	movq	%rdx, -184(%rbp)
	leaq	-96(%rbp), %r15
	movq	%rax, -200(%rbp)
	leaq	-112(%rbp), %rax
	movq	%rax, -208(%rbp)
	.p2align 4,,10
	.p2align 3
.L2787:
	movq	-184(%rbp), %rax
	movq	8(%r14), %rdi
	movl	$44, %edx
	leaq	.LC167(%rip), %rsi
	movq	(%rax), %r13
.LEHB386:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%r14), %r12
	movq	48(%r13), %rax
	movq	40(%r13), %rbx
	movq	%rax, -192(%rbp)
	cmpq	%rax, %rbx
	je	.L2770
.L2771:
	movq	(%rbx), %rsi
	movq	%r15, %rdi
	call	_ZNK2v88internal6torque4Type25GetGeneratedTNodeTypeNameB5cxx11Ev@PLT
.LEHE386:
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r12, %rdi
.LEHB387:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE387:
	movq	-96(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	je	.L2772
	call	_ZdlPv@PLT
	addq	$8, %rbx
	cmpq	%rbx, -192(%rbp)
	je	.L2773
.L2774:
	movl	$2, %edx
	leaq	.LC27(%rip), %rsi
	movq	%r12, %rdi
.LEHB388:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L2771
	.p2align 4,,10
	.p2align 3
.L2772:
	addq	$8, %rbx
	cmpq	%rbx, -192(%rbp)
	jne	.L2774
.L2773:
	movq	8(%r14), %r12
.L2770:
	movl	$2, %edx
	leaq	.LC75(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	64(%r13), %r8
	movl	$32, %edx
	xorl	%eax, %eax
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	leaq	.LC0(%rip), %rcx
	movq	%r15, %rdi
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0
.LEHE388:
	movl	$5, %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
	leaq	.LC14(%rip), %rcx
	movq	%r15, %rdi
.LEHB389:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.LEHE389:
	movq	-208(%rbp), %rbx
	leaq	16(%rax), %rdx
	movq	%rbx, -128(%rbp)
	movq	(%rax), %rcx
	cmpq	%rdx, %rcx
	je	.L2829
	movq	%rcx, -128(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -112(%rbp)
.L2778:
	movq	8(%rax), %rcx
	movq	%rcx, -120(%rbp)
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movb	$0, 16(%rax)
	movq	-96(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	je	.L2779
	call	_ZdlPv@PLT
.L2779:
	movq	-120(%rbp), %rdx
	movq	-128(%rbp), %rsi
	movq	%r12, %rdi
.LEHB390:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$37, %edx
	leaq	.LC168(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$1, 72(%r13)
	leaq	.LC166(%rip), %rsi
	leaq	.LC165(%rip), %rax
	movq	%r12, %rdi
	sbbq	%rdx, %rdx
	andl	$3, %edx
	addq	$9, %rdx
	cmpb	$0, 72(%r13)
	cmovne	%rax, %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$3, %edx
	leaq	.LC3(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE390:
	movq	-128(%rbp), %rdi
	cmpq	-208(%rbp), %rdi
	je	.L2784
	call	_ZdlPv@PLT
	addq	$8, -184(%rbp)
	movq	-184(%rbp), %rax
	cmpq	%rax, -216(%rbp)
	jne	.L2787
.L2786:
	movq	(%r14), %rax
.L2769:
	movq	24(%rax), %rbx
	leaq	16+_ZTVN2v88internal6torque15InstructionBaseE(%rip), %rax
	movq	%rax, -176(%rbp)
.LEHB391:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
	movq	-232(%rbp), %rdx
	movq	%r14, %rdi
	movq	(%rax), %rax
	movdqu	(%rax), %xmm2
	movups	%xmm2, -168(%rbp)
	movl	16(%rax), %eax
	movq	%rbx, -144(%rbp)
	movl	%eax, -152(%rbp)
	leaq	16+_ZTVN2v88internal6torque15GotoInstructionE(%rip), %rax
	movq	%rax, -176(%rbp)
	leaq	-176(%rbp), %rax
	movq	%rax, %rsi
	movq	%rax, -200(%rbp)
	call	_ZN2v88internal6torque12CSAGenerator15EmitInstructionERKNS1_15GotoInstructionEPNS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	movq	(%r14), %rdx
	movq	40(%rdx), %rax
	movq	32(%rdx), %rbx
	movq	%rax, -192(%rbp)
	cmpq	%rax, %rbx
	je	.L2788
	leaq	-96(%rbp), %r15
	.p2align 4,,10
	.p2align 3
.L2806:
	movq	56(%rdx), %rax
	movq	(%rbx), %r12
	movq	64(%rdx), %rcx
	testb	%al, %al
	je	.L2792
	cmpq	%rcx, %r12
	je	.L2791
.L2792:
	movq	8(%r14), %r13
	movl	$7, %edx
	leaq	.LC169(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	64(%r12), %r8
	movq	%r15, %rdi
	xorl	%eax, %eax
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	leaq	.LC0(%rip), %rcx
	movl	$32, %edx
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0
.LEHE391:
	movl	$5, %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
	leaq	.LC14(%rip), %rcx
	movq	%r15, %rdi
.LEHB392:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.LEHE392:
	leaq	-112(%rbp), %rcx
	leaq	16(%rax), %rdx
	movq	%rcx, -184(%rbp)
	movq	%rcx, -128(%rbp)
	movq	(%rax), %rcx
	cmpq	%rdx, %rcx
	je	.L2830
	movq	%rcx, -128(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -112(%rbp)
.L2794:
	movq	8(%rax), %rcx
	movq	%rcx, -120(%rbp)
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movb	$0, 16(%rax)
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2795
	call	_ZdlPv@PLT
.L2795:
	movq	-120(%rbp), %rdx
	movq	-128(%rbp), %rsi
	movq	%r13, %rdi
.LEHB393:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rax, %rdi
	movl	$14, %edx
	leaq	.LC160(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE393:
	movq	-128(%rbp), %rdi
	cmpq	-184(%rbp), %rdi
	je	.L2799
	call	_ZdlPv@PLT
.L2799:
	movq	-200(%rbp), %rdi
	movq	%r12, %rdx
	movq	%r14, %rsi
.LEHB394:
	call	_ZN2v88internal6torque12CSAGenerator9EmitBlockB5cxx11EPKNS1_5BlockE
	movq	-168(%rbp), %r13
	movq	-176(%rbp), %r12
	cmpq	%r12, %r13
	je	.L2800
	.p2align 4,,10
	.p2align 3
.L2804:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2801
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r12, %r13
	jne	.L2804
.L2802:
	movq	-176(%rbp), %r12
.L2800:
	testq	%r12, %r12
	je	.L2805
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L2805:
	movq	8(%r14), %rdi
	movl	$4, %edx
	leaq	.LC170(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r14), %rdx
.L2791:
	addq	$8, %rbx
	cmpq	%rbx, -192(%rbp)
	jne	.L2806
.L2788:
	cmpb	$0, 56(%rdx)
	jne	.L2831
	movq	-224(%rbp), %rax
	movb	$0, (%rax)
	movb	$0, 8(%rax)
.L2768:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2832
	movq	-224(%rbp), %rax
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2801:
	.cfi_restore_state
	addq	$32, %r12
	cmpq	%r12, %r13
	jne	.L2804
	jmp	.L2802
	.p2align 4,,10
	.p2align 3
.L2784:
	addq	$8, -184(%rbp)
	movq	-184(%rbp), %rax
	cmpq	%rax, -216(%rbp)
	jne	.L2787
	jmp	.L2786
	.p2align 4,,10
	.p2align 3
.L2829:
	movdqu	16(%rax), %xmm0
	movaps	%xmm0, -112(%rbp)
	jmp	.L2778
	.p2align 4,,10
	.p2align 3
.L2830:
	movdqu	16(%rax), %xmm1
	movaps	%xmm1, -112(%rbp)
	jmp	.L2794
	.p2align 4,,10
	.p2align 3
.L2831:
	movq	8(%r14), %rdi
	movl	$1, %edx
	leaq	.LC171(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r14), %rax
	movq	-200(%rbp), %rdi
	movq	%r14, %rsi
	movq	64(%rax), %rdx
	call	_ZN2v88internal6torque12CSAGenerator9EmitBlockB5cxx11EPKNS1_5BlockE
.LEHE394:
	movq	-224(%rbp), %rbx
	movdqa	-176(%rbp), %xmm3
	movq	-160(%rbp), %rax
	movb	$1, (%rbx)
	movq	%rax, 24(%rbx)
	movups	%xmm3, 8(%rbx)
	jmp	.L2768
.L2832:
	call	__stack_chk_fail@PLT
.L2819:
	endbr64
	movq	%rax, %r12
	jmp	.L2775
.L2817:
	endbr64
	movq	%rax, %r12
	jmp	.L2811
.L2816:
	endbr64
	movq	%rax, %r12
	jmp	.L2809
.L2820:
	endbr64
	movq	%rax, %r12
	jmp	.L2797
.L2818:
	endbr64
	movq	%rax, %r12
	jmp	.L2781
	.section	.gcc_except_table._ZN2v88internal6torque12CSAGenerator9EmitGraphENS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE,"a",@progbits
.LLSDA6884:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6884-.LLSDACSB6884
.LLSDACSB6884:
	.uleb128 .LEHB386-.LFB6884
	.uleb128 .LEHE386-.LEHB386
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB387-.LFB6884
	.uleb128 .LEHE387-.LEHB387
	.uleb128 .L2819-.LFB6884
	.uleb128 0
	.uleb128 .LEHB388-.LFB6884
	.uleb128 .LEHE388-.LEHB388
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB389-.LFB6884
	.uleb128 .LEHE389-.LEHB389
	.uleb128 .L2818-.LFB6884
	.uleb128 0
	.uleb128 .LEHB390-.LFB6884
	.uleb128 .LEHE390-.LEHB390
	.uleb128 .L2816-.LFB6884
	.uleb128 0
	.uleb128 .LEHB391-.LFB6884
	.uleb128 .LEHE391-.LEHB391
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB392-.LFB6884
	.uleb128 .LEHE392-.LEHB392
	.uleb128 .L2820-.LFB6884
	.uleb128 0
	.uleb128 .LEHB393-.LFB6884
	.uleb128 .LEHE393-.LEHB393
	.uleb128 .L2817-.LFB6884
	.uleb128 0
	.uleb128 .LEHB394-.LFB6884
	.uleb128 .LEHE394-.LEHB394
	.uleb128 0
	.uleb128 0
.LLSDACSE6884:
	.section	.text._ZN2v88internal6torque12CSAGenerator9EmitGraphENS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator9EmitGraphENS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6884
	.type	_ZN2v88internal6torque12CSAGenerator9EmitGraphENS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.cold, @function
_ZN2v88internal6torque12CSAGenerator9EmitGraphENS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.cold:
.LFSB6884:
.L2775:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-96(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	je	.L2776
	call	_ZdlPv@PLT
.L2776:
	movq	%r12, %rdi
.LEHB395:
	call	_Unwind_Resume@PLT
.L2811:
	movq	-128(%rbp), %rdi
	cmpq	-184(%rbp), %rdi
	je	.L2812
	call	_ZdlPv@PLT
.L2812:
	movq	%r12, %rdi
	call	_Unwind_Resume@PLT
.L2797:
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2798
	call	_ZdlPv@PLT
.L2798:
	movq	%r12, %rdi
	call	_Unwind_Resume@PLT
.L2809:
	movq	-128(%rbp), %rdi
	cmpq	-208(%rbp), %rdi
	je	.L2810
	call	_ZdlPv@PLT
.L2810:
	movq	%r12, %rdi
	call	_Unwind_Resume@PLT
.L2781:
	movq	-96(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	je	.L2782
	call	_ZdlPv@PLT
.L2782:
	movq	%r12, %rdi
	call	_Unwind_Resume@PLT
.LEHE395:
	.cfi_endproc
.LFE6884:
	.section	.gcc_except_table._ZN2v88internal6torque12CSAGenerator9EmitGraphENS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
.LLSDAC6884:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6884-.LLSDACSBC6884
.LLSDACSBC6884:
	.uleb128 .LEHB395-.LCOLDB172
	.uleb128 .LEHE395-.LEHB395
	.uleb128 0
	.uleb128 0
.LLSDACSEC6884:
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator9EmitGraphENS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.section	.text._ZN2v88internal6torque12CSAGenerator9EmitGraphENS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.size	_ZN2v88internal6torque12CSAGenerator9EmitGraphENS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE, .-_ZN2v88internal6torque12CSAGenerator9EmitGraphENS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.section	.text.unlikely._ZN2v88internal6torque12CSAGenerator9EmitGraphENS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.size	_ZN2v88internal6torque12CSAGenerator9EmitGraphENS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.cold, .-_ZN2v88internal6torque12CSAGenerator9EmitGraphENS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE.cold
.LCOLDE172:
	.section	.text._ZN2v88internal6torque12CSAGenerator9EmitGraphENS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
.LHOTE172:
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal6torque12CSAGenerator9EmitGraphENS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal6torque12CSAGenerator9EmitGraphENS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE, @function
_GLOBAL__sub_I__ZN2v88internal6torque12CSAGenerator9EmitGraphENS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE:
.LFB12211:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE12211:
	.size	_GLOBAL__sub_I__ZN2v88internal6torque12CSAGenerator9EmitGraphENS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE, .-_GLOBAL__sub_I__ZN2v88internal6torque12CSAGenerator9EmitGraphENS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal6torque12CSAGenerator9EmitGraphENS1_5StackINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.weak	_ZTVN2v88internal6torque15InstructionBaseE
	.section	.data.rel.ro._ZTVN2v88internal6torque15InstructionBaseE,"awG",@progbits,_ZTVN2v88internal6torque15InstructionBaseE,comdat
	.align 8
	.type	_ZTVN2v88internal6torque15InstructionBaseE, @object
	.size	_ZTVN2v88internal6torque15InstructionBaseE, 72
_ZTVN2v88internal6torque15InstructionBaseE:
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	_ZNK2v88internal6torque15InstructionBase17IsBlockTerminatorEv
	.quad	_ZNK2v88internal6torque15InstructionBase21AppendSuccessorBlocksEPSt6vectorIPNS1_5BlockESaIS5_EE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.data.rel.ro,"aw"
	.align 8
.LC88:
	.quad	_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE+64
	.align 8
.LC89:
	.quad	_ZTVSt15basic_streambufIcSt11char_traitsIcEE+16
	.align 8
.LC90:
	.quad	_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE+16
	.hidden	DW.ref.__gxx_personality_v0
	.weak	DW.ref.__gxx_personality_v0
	.section	.data.rel.local.DW.ref.__gxx_personality_v0,"awG",@progbits,DW.ref.__gxx_personality_v0,comdat
	.align 8
	.type	DW.ref.__gxx_personality_v0, @object
	.size	DW.ref.__gxx_personality_v0, 8
DW.ref.__gxx_personality_v0:
	.quad	__gxx_personality_v0
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
