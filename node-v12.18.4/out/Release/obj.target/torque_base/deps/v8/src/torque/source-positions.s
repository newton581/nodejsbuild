	.file	"source-positions.cc"
	.text
	.section	.text._ZN2v88internal6torque21ContextualVariableTopINS1_17CurrentSourceFileEEERPNT_5ScopeEv,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal6torque21ContextualVariableTopINS1_17CurrentSourceFileEEERPNT_5ScopeEv
	.type	_ZN2v88internal6torque21ContextualVariableTopINS1_17CurrentSourceFileEEERPNT_5ScopeEv, @function
_ZN2v88internal6torque21ContextualVariableTopINS1_17CurrentSourceFileEEERPNT_5ScopeEv:
.LFB4242:
	.cfi_startproc
	endbr64
	movq	%fs:0, %rax
	addq	$_ZZN2v88internal6torque21ContextualVariableTopINS1_17CurrentSourceFileEEERPNT_5ScopeEvE3top@tpoff, %rax
	ret
	.cfi_endproc
.LFE4242:
	.size	_ZN2v88internal6torque21ContextualVariableTopINS1_17CurrentSourceFileEEERPNT_5ScopeEv, .-_ZN2v88internal6torque21ContextualVariableTopINS1_17CurrentSourceFileEEERPNT_5ScopeEv
	.section	.text._ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv
	.type	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv, @function
_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv:
.LFB4243:
	.cfi_startproc
	endbr64
	movq	%fs:0, %rax
	addq	$_ZZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEvE3top@tpoff, %rax
	ret
	.cfi_endproc
.LFE4243:
	.size	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv, .-_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv
	.section	.text._ZN2v88internal6torque21ContextualVariableTopINS1_13SourceFileMapEEERPNT_5ScopeEv,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal6torque21ContextualVariableTopINS1_13SourceFileMapEEERPNT_5ScopeEv
	.type	_ZN2v88internal6torque21ContextualVariableTopINS1_13SourceFileMapEEERPNT_5ScopeEv, @function
_ZN2v88internal6torque21ContextualVariableTopINS1_13SourceFileMapEEERPNT_5ScopeEv:
.LFB4244:
	.cfi_startproc
	endbr64
	movq	%fs:0, %rax
	addq	$_ZZN2v88internal6torque21ContextualVariableTopINS1_13SourceFileMapEEERPNT_5ScopeEvE3top@tpoff, %rax
	ret
	.cfi_endproc
.LFE4244:
	.size	_ZN2v88internal6torque21ContextualVariableTopINS1_13SourceFileMapEEERPNT_5ScopeEv, .-_ZN2v88internal6torque21ContextualVariableTopINS1_13SourceFileMapEEERPNT_5ScopeEv
	.section	.rodata._ZN2v88internal6torque13SourceFileMap14PathFromV8RootB5cxx11ENS1_8SourceIdE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"file.IsValid()"
.LC1:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal6torque13SourceFileMap14PathFromV8RootB5cxx11ENS1_8SourceIdE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque13SourceFileMap14PathFromV8RootB5cxx11ENS1_8SourceIdE
	.type	_ZN2v88internal6torque13SourceFileMap14PathFromV8RootB5cxx11ENS1_8SourceIdE, @function
_ZN2v88internal6torque13SourceFileMap14PathFromV8RootB5cxx11ENS1_8SourceIdE:
.LFB4245:
	.cfi_startproc
	endbr64
	cmpl	$-1, %edi
	je	.L10
	movslq	%edi, %rax
	salq	$5, %rax
	movq	%rax, %rdi
	movq	%fs:_ZZN2v88internal6torque21ContextualVariableTopINS1_13SourceFileMapEEERPNT_5ScopeEvE3top@tpoff, %rax
	addq	(%rax), %rdi
	movq	%rdi, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE4245:
	.size	_ZN2v88internal6torque13SourceFileMap14PathFromV8RootB5cxx11ENS1_8SourceIdE, .-_ZN2v88internal6torque13SourceFileMap14PathFromV8RootB5cxx11ENS1_8SourceIdE
	.section	.rodata._ZN2v88internal6torque13SourceFileMap12AbsolutePathB5cxx11ENS1_8SourceIdE.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"basic_string::_M_construct null not valid"
	.section	.rodata._ZN2v88internal6torque13SourceFileMap12AbsolutePathB5cxx11ENS1_8SourceIdE.str1.1,"aMS",@progbits,1
.LC3:
	.string	"basic_string::append"
.LC4:
	.string	"/"
	.section	.text.unlikely._ZN2v88internal6torque13SourceFileMap12AbsolutePathB5cxx11ENS1_8SourceIdE,"ax",@progbits
	.align 2
.LCOLDB5:
	.section	.text._ZN2v88internal6torque13SourceFileMap12AbsolutePathB5cxx11ENS1_8SourceIdE,"ax",@progbits
.LHOTB5:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque13SourceFileMap12AbsolutePathB5cxx11ENS1_8SourceIdE
	.type	_ZN2v88internal6torque13SourceFileMap12AbsolutePathB5cxx11ENS1_8SourceIdE, @function
_ZN2v88internal6torque13SourceFileMap12AbsolutePathB5cxx11ENS1_8SourceIdE:
.LFB4246:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA4246
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$-1, %esi
	je	.L79
	movslq	%esi, %rbx
	movl	$12090, %ecx
	leaq	-112(%rbp), %r15
	movq	%rdi, %r12
	movq	%fs:_ZZN2v88internal6torque21ContextualVariableTopINS1_13SourceFileMapEEERPNT_5ScopeEvE3top@tpoff, %rax
	salq	$5, %rbx
	leaq	-80(%rbp), %r13
	movq	(%rax), %rdx
	movl	$1701603686, -112(%rbp)
	movw	%cx, -108(%rbp)
	addq	%rbx, %rdx
	movb	$47, -106(%rbp)
	movq	$7, -120(%rbp)
	movb	$0, -105(%rbp)
	cmpq	$6, 8(%rdx)
	movq	%r15, -128(%rbp)
	ja	.L80
.L48:
	addq	(%rax), %rbx
	movq	%r13, -96(%rbp)
	movq	24(%rax), %r15
	movq	32(%rax), %r14
	movq	%r15, %rax
	addq	%r14, %rax
	je	.L56
	testq	%r15, %r15
	je	.L31
.L56:
	movq	%r14, -136(%rbp)
	cmpq	$15, %r14
	ja	.L81
	cmpq	$1, %r14
	jne	.L35
	movzbl	(%r15), %eax
	movb	%al, -80(%rbp)
	movq	%r13, %rax
.L36:
	movq	%r14, -88(%rbp)
	movb	$0, (%rax,%r14)
	movabsq	$4611686018427387903, %rax
	cmpq	%rax, -88(%rbp)
	je	.L82
	leaq	-96(%rbp), %r14
	movl	$1, %edx
	leaq	.LC4(%rip), %rsi
	movq	%r14, %rdi
.LEHB0:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.LEHE0:
	movq	8(%rbx), %rdx
	movq	(%rbx), %rsi
	movq	%r14, %rdi
.LEHB1:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.LEHE1:
	leaq	16(%r12), %rdx
	movq	%rdx, (%r12)
	movq	(%rax), %rcx
	leaq	16(%rax), %rdx
	cmpq	%rdx, %rcx
	je	.L83
	movq	%rcx, (%r12)
	movq	16(%rax), %rcx
	movq	%rcx, 16(%r12)
.L42:
	movq	8(%rax), %rcx
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movq	%rcx, 8(%r12)
	movb	$0, 16(%rax)
	movq	-96(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L11
	call	_ZdlPv@PLT
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L80:
	movq	%r13, -96(%rbp)
	movq	8(%rdx), %rcx
	movl	$7, %eax
	movq	(%rdx), %rdi
	cmpq	$7, %rcx
	cmovbe	%rcx, %rax
	movq	%rdi, %rsi
	addq	%rax, %rsi
	setne	%r14b
	testq	%rdi, %rdi
	sete	%sil
	andb	%sil, %r14b
	jne	.L84
	cmpq	$1, %rcx
	je	.L85
	testq	%rcx, %rcx
	jne	.L86
.L15:
	movq	%rax, -88(%rbp)
	movb	$0, -80(%rbp,%rax)
	cmpq	$7, -88(%rbp)
	movq	-96(%rbp), %rdi
	je	.L18
.L19:
	cmpq	%r13, %rdi
	je	.L23
	movq	%rdx, -152(%rbp)
	call	_ZdlPv@PLT
	movq	-128(%rbp), %rdi
	movq	-152(%rbp), %rdx
	cmpq	%r15, %rdi
	je	.L23
	call	_ZdlPv@PLT
	movq	-152(%rbp), %rdx
.L23:
	testb	%r14b, %r14b
	je	.L24
	leaq	16(%r12), %rdi
	movq	%rdi, (%r12)
	movq	(%rdx), %r14
	movq	8(%rdx), %r13
	movq	%r14, %rax
	addq	%r13, %rax
	je	.L25
	testq	%r14, %r14
	je	.L31
.L25:
	movq	%r13, -136(%rbp)
	cmpq	$15, %r13
	ja	.L87
	cmpq	$1, %r13
	jne	.L28
	movzbl	(%r14), %eax
	movb	%al, 16(%r12)
.L29:
	movq	%r13, 8(%r12)
	movb	$0, (%rdi,%r13)
.L11:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L88
	addq	$120, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L35:
	.cfi_restore_state
	testq	%r14, %r14
	jne	.L89
	movq	%r13, %rax
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L28:
	testq	%r13, %r13
	je	.L29
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L86:
	movl	%eax, %r9d
	testl	%eax, %eax
	je	.L15
	xorl	%ecx, %ecx
.L16:
	movl	%ecx, %esi
	addl	$1, %ecx
	movzbl	(%rdi,%rsi), %r8d
	movb	%r8b, 0(%r13,%rsi)
	cmpl	%r9d, %ecx
	jb	.L16
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L85:
	movzbl	(%rdi), %ecx
	movb	%cl, -80(%rbp)
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L18:
	movl	-112(%rbp), %eax
	cmpl	%eax, (%rdi)
	je	.L90
.L20:
	movl	$1, %eax
.L21:
	testl	%eax, %eax
	sete	%r14b
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L87:
	movq	%r12, %rdi
	leaq	-136(%rbp), %rsi
	xorl	%edx, %edx
.LEHB2:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%r12)
	movq	%rax, %rdi
	movq	-136(%rbp), %rax
	movq	%rax, 16(%r12)
.L27:
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-136(%rbp), %r13
	movq	(%r12), %rdi
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L81:
	leaq	-96(%rbp), %rdi
	leaq	-136(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -96(%rbp)
	movq	%rax, %rdi
	movq	-136(%rbp), %rax
	movq	%rax, -80(%rbp)
.L34:
	movq	%r14, %rdx
	movq	%r15, %rsi
	call	memcpy@PLT
	movq	-136(%rbp), %r14
	movq	-96(%rbp), %rax
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L83:
	movdqu	16(%rax), %xmm0
	movups	%xmm0, 16(%r12)
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L79:
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
.LEHE2:
	.p2align 4,,10
	.p2align 3
.L24:
	movq	%fs:_ZZN2v88internal6torque21ContextualVariableTopINS1_13SourceFileMapEEERPNT_5ScopeEvE3top@tpoff, %rax
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L90:
	movzwl	-108(%rbp), %eax
	cmpw	%ax, 4(%rdi)
	jne	.L20
	movzbl	6(%r15), %eax
	cmpb	%al, 6(%rdi)
	jne	.L20
	xorl	%eax, %eax
	jmp	.L21
.L88:
	call	__stack_chk_fail@PLT
.L82:
	leaq	.LC3(%rip), %rdi
.LEHB3:
	call	_ZSt20__throw_length_errorPKc@PLT
.LEHE3:
.L31:
	leaq	.LC2(%rip), %rdi
.LEHB4:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE4:
.L84:
	leaq	.LC2(%rip), %rdi
.LEHB5:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE5:
.L89:
	movq	%r13, %rdi
	jmp	.L34
.L53:
	endbr64
	movq	%rax, %r12
	jmp	.L44
.L54:
	endbr64
	movq	%rax, %r12
	jmp	.L46
.L55:
	endbr64
	movq	%rax, %r12
	jmp	.L46
	.globl	__gxx_personality_v0
	.section	.gcc_except_table._ZN2v88internal6torque13SourceFileMap12AbsolutePathB5cxx11ENS1_8SourceIdE,"a",@progbits
.LLSDA4246:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE4246-.LLSDACSB4246
.LLSDACSB4246:
	.uleb128 .LEHB0-.LFB4246
	.uleb128 .LEHE0-.LEHB0
	.uleb128 .L55-.LFB4246
	.uleb128 0
	.uleb128 .LEHB1-.LFB4246
	.uleb128 .LEHE1-.LEHB1
	.uleb128 .L54-.LFB4246
	.uleb128 0
	.uleb128 .LEHB2-.LFB4246
	.uleb128 .LEHE2-.LEHB2
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB3-.LFB4246
	.uleb128 .LEHE3-.LEHB3
	.uleb128 .L55-.LFB4246
	.uleb128 0
	.uleb128 .LEHB4-.LFB4246
	.uleb128 .LEHE4-.LEHB4
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB5-.LFB4246
	.uleb128 .LEHE5-.LEHB5
	.uleb128 .L53-.LFB4246
	.uleb128 0
.LLSDACSE4246:
	.section	.text._ZN2v88internal6torque13SourceFileMap12AbsolutePathB5cxx11ENS1_8SourceIdE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque13SourceFileMap12AbsolutePathB5cxx11ENS1_8SourceIdE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC4246
	.type	_ZN2v88internal6torque13SourceFileMap12AbsolutePathB5cxx11ENS1_8SourceIdE.cold, @function
_ZN2v88internal6torque13SourceFileMap12AbsolutePathB5cxx11ENS1_8SourceIdE.cold:
.LFSB4246:
.L44:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-128(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L47
.L77:
	call	_ZdlPv@PLT
.L47:
	movq	%r12, %rdi
.LEHB6:
	call	_Unwind_Resume@PLT
.LEHE6:
.L46:
	movq	-96(%rbp), %rdi
	cmpq	%r13, %rdi
	jne	.L77
	jmp	.L47
	.cfi_endproc
.LFE4246:
	.section	.gcc_except_table._ZN2v88internal6torque13SourceFileMap12AbsolutePathB5cxx11ENS1_8SourceIdE
.LLSDAC4246:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC4246-.LLSDACSBC4246
.LLSDACSBC4246:
	.uleb128 .LEHB6-.LCOLDB5
	.uleb128 .LEHE6-.LEHB6
	.uleb128 0
	.uleb128 0
.LLSDACSEC4246:
	.section	.text.unlikely._ZN2v88internal6torque13SourceFileMap12AbsolutePathB5cxx11ENS1_8SourceIdE
	.section	.text._ZN2v88internal6torque13SourceFileMap12AbsolutePathB5cxx11ENS1_8SourceIdE
	.size	_ZN2v88internal6torque13SourceFileMap12AbsolutePathB5cxx11ENS1_8SourceIdE, .-_ZN2v88internal6torque13SourceFileMap12AbsolutePathB5cxx11ENS1_8SourceIdE
	.section	.text.unlikely._ZN2v88internal6torque13SourceFileMap12AbsolutePathB5cxx11ENS1_8SourceIdE
	.size	_ZN2v88internal6torque13SourceFileMap12AbsolutePathB5cxx11ENS1_8SourceIdE.cold, .-_ZN2v88internal6torque13SourceFileMap12AbsolutePathB5cxx11ENS1_8SourceIdE.cold
.LCOLDE5:
	.section	.text._ZN2v88internal6torque13SourceFileMap12AbsolutePathB5cxx11ENS1_8SourceIdE
.LHOTE5:
	.section	.text._ZN2v88internal6torque13SourceFileMap11GetSourceIdERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque13SourceFileMap11GetSourceIdERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN2v88internal6torque13SourceFileMap11GetSourceIdERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN2v88internal6torque13SourceFileMap11GetSourceIdERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB4249:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:_ZZN2v88internal6torque21ContextualVariableTopINS1_13SourceFileMapEEERPNT_5ScopeEvE3top@tpoff, %rax
	movq	(%rax), %r12
	movq	8(%rax), %r15
	subq	%r12, %r15
	sarq	$5, %r15
	je	.L92
	movq	8(%rdi), %r13
	movq	%rdi, %r14
	xorl	%ebx, %ebx
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L93:
	addq	$1, %rbx
	addq	$32, %r12
	cmpq	%r15, %rbx
	je	.L92
.L97:
	cmpq	%r13, 8(%r12)
	jne	.L93
	testq	%r13, %r13
	je	.L96
	movq	(%r14), %rsi
	movq	(%r12), %rdi
	movq	%r13, %rdx
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L93
.L96:
	addq	$8, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L92:
	.cfi_restore_state
	addq	$8, %rsp
	movl	$-1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4249:
	.size	_ZN2v88internal6torque13SourceFileMap11GetSourceIdERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN2v88internal6torque13SourceFileMap11GetSourceIdERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.rodata._ZN2v88internal6torque13SourceFileMap10AllSourcesEv.str1.1,"aMS",@progbits,1
.LC6:
	.string	"vector::_M_realloc_insert"
	.section	.text.unlikely._ZN2v88internal6torque13SourceFileMap10AllSourcesEv,"ax",@progbits
	.align 2
.LCOLDB7:
	.section	.text._ZN2v88internal6torque13SourceFileMap10AllSourcesEv,"ax",@progbits
.LHOTB7:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque13SourceFileMap10AllSourcesEv
	.type	_ZN2v88internal6torque13SourceFileMap10AllSourcesEv, @function
_ZN2v88internal6torque13SourceFileMap10AllSourcesEv:
.LFB4250:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA4250
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:_ZZN2v88internal6torque21ContextualVariableTopINS1_13SourceFileMapEEERPNT_5ScopeEvE3top@tpoff, %r14
	movq	$0, 16(%rdi)
	movups	%xmm0, (%rdi)
	movq	8(%r14), %rax
	subq	(%r14), %rax
	sarq	$5, %rax
	testl	%eax, %eax
	jle	.L106
	xorl	%eax, %eax
	xorl	%ebx, %ebx
	xorl	%r12d, %r12d
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L142:
	movl	%r12d, (%rbx)
	movq	8(%r13), %rax
	leaq	4(%rax), %rbx
	movq	%rbx, 8(%r13)
.L109:
	movq	8(%r14), %rax
	subq	(%r14), %rax
	addl	$1, %r12d
	sarq	$5, %rax
	cmpl	%eax, %r12d
	jge	.L106
	movq	16(%r13), %rax
.L119:
	cmpq	%rbx, %rax
	jne	.L142
	movabsq	$2305843009213693951, %rdi
	movq	0(%r13), %r15
	subq	%r15, %rax
	movq	%rax, -64(%rbp)
	sarq	$2, %rax
	cmpq	%rdi, %rax
	je	.L143
	testq	%rax, %rax
	je	.L122
	movabsq	$9223372036854775804, %rdi
	leaq	(%rax,%rax), %rdx
	movq	%rdi, -56(%rbp)
	cmpq	%rdx, %rax
	jbe	.L144
.L111:
	movq	-56(%rbp), %rdi
.LEHB7:
	call	_Znwm@PLT
	movq	-56(%rbp), %r8
	movq	-64(%rbp), %rdi
	leaq	4(%rax), %rdx
	movl	%r12d, (%rax,%rdi)
	addq	%rax, %r8
	cmpq	%rbx, %r15
	je	.L125
.L146:
	movq	%rbx, %rdx
	subq	%r15, %rdx
	leaq	-4(%rdx), %rdi
	leaq	15(%r15), %rdx
	movq	%rdi, %rsi
	subq	%rax, %rdx
	shrq	$2, %rsi
	cmpq	$30, %rdx
	jbe	.L126
	movabsq	$4611686018427387900, %rcx
	testq	%rcx, %rsi
	je	.L126
	addq	$1, %rsi
	xorl	%edx, %edx
	movq	%rsi, %rcx
	shrq	$2, %rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L115:
	movdqu	(%r15,%rdx), %xmm1
	movups	%xmm1, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rcx
	jne	.L115
	movq	%rsi, %rcx
	andq	$-4, %rcx
	leaq	0(,%rcx,4), %rdx
	leaq	(%r15,%rdx), %r9
	addq	%rax, %rdx
	cmpq	%rcx, %rsi
	je	.L117
	movl	(%r9), %ecx
	movl	%ecx, (%rdx)
	leaq	4(%r9), %rcx
	cmpq	%rcx, %rbx
	je	.L117
	movl	4(%r9), %ecx
	movl	%ecx, 4(%rdx)
	leaq	8(%r9), %rcx
	cmpq	%rcx, %rbx
	je	.L117
	movl	8(%r9), %ecx
	movl	%ecx, 8(%rdx)
.L117:
	leaq	8(%rax,%rdi), %rbx
.L113:
	testq	%r15, %r15
	je	.L118
	movq	%r15, %rdi
	movq	%rax, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-64(%rbp), %rax
	movq	-56(%rbp), %r8
.L118:
	movq	%rax, %xmm0
	movq	%rbx, %xmm2
	movq	%r8, 16(%r13)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 0(%r13)
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L144:
	testq	%rdx, %rdx
	jne	.L145
	movq	-64(%rbp), %rdi
	xorl	%eax, %eax
	movl	$4, %edx
	xorl	%r8d, %r8d
	movl	%r12d, (%rax,%rdi)
	cmpq	%rbx, %r15
	jne	.L146
.L125:
	movq	%rdx, %rbx
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L106:
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L122:
	.cfi_restore_state
	movq	$4, -56(%rbp)
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L126:
	movq	%rax, %rcx
	movq	%r15, %rdx
	.p2align 4,,10
	.p2align 3
.L114:
	movl	(%rdx), %esi
	addq	$4, %rdx
	addq	$4, %rcx
	movl	%esi, -4(%rcx)
	cmpq	%rbx, %rdx
	jne	.L114
	jmp	.L117
.L143:
	leaq	.LC6(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.LEHE7:
.L145:
	movabsq	$2305843009213693951, %rax
	cmpq	%rax, %rdx
	cmova	%rax, %rdx
	leaq	0(,%rdx,4), %rax
	movq	%rax, -56(%rbp)
	jmp	.L111
.L127:
	endbr64
	movq	%rax, %r12
	jmp	.L120
	.section	.gcc_except_table._ZN2v88internal6torque13SourceFileMap10AllSourcesEv,"a",@progbits
.LLSDA4250:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE4250-.LLSDACSB4250
.LLSDACSB4250:
	.uleb128 .LEHB7-.LFB4250
	.uleb128 .LEHE7-.LEHB7
	.uleb128 .L127-.LFB4250
	.uleb128 0
.LLSDACSE4250:
	.section	.text._ZN2v88internal6torque13SourceFileMap10AllSourcesEv
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque13SourceFileMap10AllSourcesEv
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC4250
	.type	_ZN2v88internal6torque13SourceFileMap10AllSourcesEv.cold, @function
_ZN2v88internal6torque13SourceFileMap10AllSourcesEv.cold:
.LFSB4250:
.L120:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L121
	call	_ZdlPv@PLT
.L121:
	movq	%r12, %rdi
.LEHB8:
	call	_Unwind_Resume@PLT
.LEHE8:
	.cfi_endproc
.LFE4250:
	.section	.gcc_except_table._ZN2v88internal6torque13SourceFileMap10AllSourcesEv
.LLSDAC4250:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC4250-.LLSDACSBC4250
.LLSDACSBC4250:
	.uleb128 .LEHB8-.LCOLDB7
	.uleb128 .LEHE8-.LEHB8
	.uleb128 0
	.uleb128 0
.LLSDACSEC4250:
	.section	.text.unlikely._ZN2v88internal6torque13SourceFileMap10AllSourcesEv
	.section	.text._ZN2v88internal6torque13SourceFileMap10AllSourcesEv
	.size	_ZN2v88internal6torque13SourceFileMap10AllSourcesEv, .-_ZN2v88internal6torque13SourceFileMap10AllSourcesEv
	.section	.text.unlikely._ZN2v88internal6torque13SourceFileMap10AllSourcesEv
	.size	_ZN2v88internal6torque13SourceFileMap10AllSourcesEv.cold, .-_ZN2v88internal6torque13SourceFileMap10AllSourcesEv.cold
.LCOLDE7:
	.section	.text._ZN2v88internal6torque13SourceFileMap10AllSourcesEv
.LHOTE7:
	.section	.text.unlikely._ZN2v88internal6torque13SourceFileMap26FileRelativeToV8RootExistsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"ax",@progbits
	.align 2
.LCOLDB8:
	.section	.text._ZN2v88internal6torque13SourceFileMap26FileRelativeToV8RootExistsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"ax",@progbits
.LHOTB8:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque13SourceFileMap26FileRelativeToV8RootExistsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN2v88internal6torque13SourceFileMap26FileRelativeToV8RootExistsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN2v88internal6torque13SourceFileMap26FileRelativeToV8RootExistsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB4266:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA4266
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-560(%rbp), %r12
	pushq	%rbx
	subq	$600, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%fs:_ZZN2v88internal6torque21ContextualVariableTopINS1_13SourceFileMapEEERPNT_5ScopeEvE3top@tpoff, %rax
	movq	%r12, -576(%rbp)
	movq	24(%rax), %r15
	movq	32(%rax), %r13
	movq	%r15, %rax
	addq	%r13, %rax
	je	.L148
	testq	%r15, %r15
	je	.L191
.L148:
	movq	%r13, -616(%rbp)
	cmpq	$15, %r13
	ja	.L192
	cmpq	$1, %r13
	jne	.L151
	movzbl	(%r15), %eax
	movb	%al, -560(%rbp)
	movq	%r12, %rax
.L152:
	movq	%r13, -568(%rbp)
	movb	$0, (%rax,%r13)
	movabsq	$4611686018427387903, %rax
	cmpq	%rax, -568(%rbp)
	je	.L193
	leaq	-576(%rbp), %rbx
	movl	$1, %edx
	leaq	.LC4(%rip), %rsi
	movq	%rbx, %rdi
.LEHB9:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.LEHE9:
	movq	8(%r14), %rdx
	movq	(%r14), %rsi
	movq	%rbx, %rdi
.LEHB10:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.LEHE10:
	leaq	-592(%rbp), %rcx
	leaq	16(%rax), %rdx
	movq	%rcx, -632(%rbp)
	movq	%rcx, -608(%rbp)
	movq	(%rax), %rcx
	cmpq	%rdx, %rcx
	je	.L194
	movq	%rcx, -608(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -592(%rbp)
.L158:
	movq	8(%rax), %rcx
	movb	$0, 16(%rax)
	movq	%rcx, -600(%rbp)
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movq	-576(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L159
	call	_ZdlPv@PLT
.L159:
	leaq	-320(%rbp), %r14
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %r15
	movq	%r14, %rdi
	call	_ZNSt8ios_baseC2Ev@PLT
	movq	8+_ZTTSt14basic_ifstreamIcSt11char_traitsIcEE(%rip), %r13
	pxor	%xmm0, %xmm0
	xorl	%edx, %edx
	movq	%r15, -320(%rbp)
	movq	16+_ZTTSt14basic_ifstreamIcSt11char_traitsIcEE(%rip), %rsi
	movq	%r13, -576(%rbp)
	movq	$0, -104(%rbp)
	movw	%dx, -96(%rbp)
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	-24(%r13), %rax
	movq	%rsi, -576(%rbp,%rax)
	movq	-576(%rbp), %rax
	xorl	%esi, %esi
	movq	$0, -568(%rbp)
	movq	-24(%rax), %rdi
	addq	%rbx, %rdi
.LEHB11:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE11:
	leaq	24+_ZTVSt14basic_ifstreamIcSt11char_traitsIcEE(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, -576(%rbp)
	addq	$40, %rax
	movq	%rax, -320(%rbp)
.LEHB12:
	call	_ZNSt13basic_filebufIcSt11char_traitsIcEEC1Ev@PLT
.LEHE12:
	movq	%r12, %rsi
	movq	%r14, %rdi
.LEHB13:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	-608(%rbp), %rsi
	movl	$8, %edx
	movq	%r12, %rdi
	call	_ZNSt13basic_filebufIcSt11char_traitsIcEE4openEPKcSt13_Ios_Openmode@PLT
	movq	-576(%rbp), %rdx
	addq	-24(%rdx), %rbx
	movq	%rbx, %rdi
	testq	%rax, %rax
	je	.L195
	xorl	%esi, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
.LEHE13:
.L161:
	movl	-288(%rbp), %eax
	movq	%r12, %rdi
	testl	%eax, %eax
	leaq	24+_ZTVSt14basic_ifstreamIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -576(%rbp)
	sete	%bl
	addq	$40, %rax
	movq	%rax, -320(%rbp)
	leaq	16+_ZTVSt13basic_filebufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -560(%rbp)
.LEHB14:
	call	_ZNSt13basic_filebufIcSt11char_traitsIcEE5closeEv@PLT
.LEHE14:
.L162:
	leaq	-456(%rbp), %rdi
	call	_ZNSt12__basic_fileIcED1Ev@PLT
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	-504(%rbp), %rdi
	movq	%rax, -560(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	%r13, -576(%rbp)
	movq	-24(%r13), %rax
	movq	%r14, %rdi
	movq	16+_ZTTSt14basic_ifstreamIcSt11char_traitsIcEE(%rip), %rsi
	movq	%rsi, -576(%rbp,%rax)
	movq	$0, -568(%rbp)
	movq	%r15, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-608(%rbp), %rdi
	cmpq	-632(%rbp), %rdi
	je	.L147
	call	_ZdlPv@PLT
.L147:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L196
	addq	$600, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L151:
	.cfi_restore_state
	testq	%r13, %r13
	jne	.L197
	movq	%r12, %rax
	jmp	.L152
	.p2align 4,,10
	.p2align 3
.L192:
	leaq	-576(%rbp), %rdi
	leaq	-616(%rbp), %rsi
	xorl	%edx, %edx
.LEHB15:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE15:
	movq	%rax, -576(%rbp)
	movq	%rax, %rdi
	movq	-616(%rbp), %rax
	movq	%rax, -560(%rbp)
.L150:
	movq	%r13, %rdx
	movq	%r15, %rsi
	call	memcpy@PLT
	movq	-616(%rbp), %r13
	movq	-576(%rbp), %rax
	jmp	.L152
	.p2align 4,,10
	.p2align 3
.L195:
	movl	32(%rbx), %esi
	orl	$4, %esi
.LEHB16:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
.LEHE16:
	jmp	.L161
	.p2align 4,,10
	.p2align 3
.L194:
	movdqu	16(%rax), %xmm1
	movaps	%xmm1, -592(%rbp)
	jmp	.L158
.L191:
	leaq	.LC2(%rip), %rdi
.LEHB17:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE17:
.L193:
	leaq	.LC3(%rip), %rdi
.LEHB18:
	call	_ZSt20__throw_length_errorPKc@PLT
.LEHE18:
.L196:
	call	__stack_chk_fail@PLT
.L197:
	movq	%r12, %rdi
	jmp	.L150
.L178:
	endbr64
	movq	%rax, %rbx
	jmp	.L164
.L177:
	endbr64
	movq	%rax, %r12
	jmp	.L165
.L179:
	endbr64
	movq	%rax, %rbx
	jmp	.L163
.L180:
	endbr64
	movq	%rax, %rdi
	jmp	.L170
.L176:
	endbr64
	movq	%rax, %r13
	jmp	.L171
.L175:
	endbr64
	movq	%rax, %r13
	jmp	.L171
	.section	.gcc_except_table._ZN2v88internal6torque13SourceFileMap26FileRelativeToV8RootExistsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"a",@progbits
	.align 4
.LLSDA4266:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATT4266-.LLSDATTD4266
.LLSDATTD4266:
	.byte	0x1
	.uleb128 .LLSDACSE4266-.LLSDACSB4266
.LLSDACSB4266:
	.uleb128 .LEHB9-.LFB4266
	.uleb128 .LEHE9-.LEHB9
	.uleb128 .L176-.LFB4266
	.uleb128 0
	.uleb128 .LEHB10-.LFB4266
	.uleb128 .LEHE10-.LEHB10
	.uleb128 .L175-.LFB4266
	.uleb128 0
	.uleb128 .LEHB11-.LFB4266
	.uleb128 .LEHE11-.LEHB11
	.uleb128 .L177-.LFB4266
	.uleb128 0
	.uleb128 .LEHB12-.LFB4266
	.uleb128 .LEHE12-.LEHB12
	.uleb128 .L178-.LFB4266
	.uleb128 0
	.uleb128 .LEHB13-.LFB4266
	.uleb128 .LEHE13-.LEHB13
	.uleb128 .L179-.LFB4266
	.uleb128 0
	.uleb128 .LEHB14-.LFB4266
	.uleb128 .LEHE14-.LEHB14
	.uleb128 .L180-.LFB4266
	.uleb128 0x1
	.uleb128 .LEHB15-.LFB4266
	.uleb128 .LEHE15-.LEHB15
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB16-.LFB4266
	.uleb128 .LEHE16-.LEHB16
	.uleb128 .L179-.LFB4266
	.uleb128 0
	.uleb128 .LEHB17-.LFB4266
	.uleb128 .LEHE17-.LEHB17
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB18-.LFB4266
	.uleb128 .LEHE18-.LEHB18
	.uleb128 .L176-.LFB4266
	.uleb128 0
.LLSDACSE4266:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATT4266:
	.section	.text._ZN2v88internal6torque13SourceFileMap26FileRelativeToV8RootExistsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque13SourceFileMap26FileRelativeToV8RootExistsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC4266
	.type	_ZN2v88internal6torque13SourceFileMap26FileRelativeToV8RootExistsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.cold, @function
_ZN2v88internal6torque13SourceFileMap26FileRelativeToV8RootExistsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.cold:
.LFSB4266:
.L163:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	%r12, %rdi
	call	_ZNSt13basic_filebufIcSt11char_traitsIcEED1Ev@PLT
.L164:
	movq	%r13, -576(%rbp)
	movq	16+_ZTTSt14basic_ifstreamIcSt11char_traitsIcEE(%rip), %rdx
	movq	%rbx, %r12
	movq	-24(%r13), %rax
	movq	%rdx, -576(%rbp,%rax)
	movq	$0, -568(%rbp)
.L165:
	movq	%r14, %rdi
	movq	%r15, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-608(%rbp), %rdi
	cmpq	-632(%rbp), %rdi
	je	.L167
	call	_ZdlPv@PLT
.L167:
	movq	%r12, %rdi
.LEHB19:
	call	_Unwind_Resume@PLT
.LEHE19:
.L170:
	call	__cxa_begin_catch@PLT
	call	__cxa_end_catch@PLT
	jmp	.L162
.L171:
	movq	-576(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L172
	call	_ZdlPv@PLT
.L172:
	movq	%r13, %rdi
.LEHB20:
	call	_Unwind_Resume@PLT
.LEHE20:
	.cfi_endproc
.LFE4266:
	.section	.gcc_except_table._ZN2v88internal6torque13SourceFileMap26FileRelativeToV8RootExistsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.align 4
.LLSDAC4266:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATTC4266-.LLSDATTDC4266
.LLSDATTDC4266:
	.byte	0x1
	.uleb128 .LLSDACSEC4266-.LLSDACSBC4266
.LLSDACSBC4266:
	.uleb128 .LEHB19-.LCOLDB8
	.uleb128 .LEHE19-.LEHB19
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB20-.LCOLDB8
	.uleb128 .LEHE20-.LEHB20
	.uleb128 0
	.uleb128 0
.LLSDACSEC4266:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATTC4266:
	.section	.text.unlikely._ZN2v88internal6torque13SourceFileMap26FileRelativeToV8RootExistsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.text._ZN2v88internal6torque13SourceFileMap26FileRelativeToV8RootExistsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.size	_ZN2v88internal6torque13SourceFileMap26FileRelativeToV8RootExistsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN2v88internal6torque13SourceFileMap26FileRelativeToV8RootExistsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.text.unlikely._ZN2v88internal6torque13SourceFileMap26FileRelativeToV8RootExistsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.size	_ZN2v88internal6torque13SourceFileMap26FileRelativeToV8RootExistsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.cold, .-_ZN2v88internal6torque13SourceFileMap26FileRelativeToV8RootExistsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.cold
.LCOLDE8:
	.section	.text._ZN2v88internal6torque13SourceFileMap26FileRelativeToV8RootExistsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
.LHOTE8:
	.section	.rodata._ZN2v88internal6torque13SourceFileMap30PathFromV8RootWithoutExtensionB5cxx11ENS1_8SourceIdE.str1.1,"aMS",@progbits,1
.LC9:
	.string	"Not a .tq file: "
	.section	.text.unlikely._ZN2v88internal6torque13SourceFileMap30PathFromV8RootWithoutExtensionB5cxx11ENS1_8SourceIdE,"ax",@progbits
	.align 2
.LCOLDB13:
	.section	.text._ZN2v88internal6torque13SourceFileMap30PathFromV8RootWithoutExtensionB5cxx11ENS1_8SourceIdE,"ax",@progbits
.LHOTB13:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque13SourceFileMap30PathFromV8RootWithoutExtensionB5cxx11ENS1_8SourceIdE
	.type	_ZN2v88internal6torque13SourceFileMap30PathFromV8RootWithoutExtensionB5cxx11ENS1_8SourceIdE, @function
_ZN2v88internal6torque13SourceFileMap30PathFromV8RootWithoutExtensionB5cxx11ENS1_8SourceIdE:
.LFB4247:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA4247
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$568, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$-1, %esi
	je	.L257
	movq	%fs:_ZZN2v88internal6torque21ContextualVariableTopINS1_13SourceFileMapEEERPNT_5ScopeEvE3top@tpoff, %rax
	movslq	%esi, %rsi
	leaq	16(%rdi), %rbx
	movq	%rdi, %r12
	salq	$5, %rsi
	addq	(%rax), %rsi
	movq	%rbx, (%rdi)
	movq	(%rsi), %r14
	movq	8(%rsi), %r13
	movq	%r14, %rax
	addq	%r13, %rax
	je	.L200
	testq	%r14, %r14
	je	.L258
.L200:
	movq	%r13, -552(%rbp)
	cmpq	$15, %r13
	ja	.L259
	cmpq	$1, %r13
	jne	.L203
	movzbl	(%r14), %eax
	movb	%al, 16(%r12)
	movq	%rbx, %rax
.L204:
	movq	%r13, 8(%r12)
	movb	$0, (%rax,%r13)
	movq	8(%r12), %rsi
	movl	$29742, %eax
	leaq	-496(%rbp), %r13
	movq	%r13, -512(%rbp)
	movw	%ax, -496(%rbp)
	movb	$113, -494(%rbp)
	movq	$3, -504(%rbp)
	movb	$0, -493(%rbp)
	cmpq	$2, %rsi
	ja	.L260
.L232:
	leaq	-320(%rbp), %rax
	leaq	-448(%rbp), %r15
	movq	%rax, %rdi
	movq	%r15, -608(%rbp)
	movq	%rax, -576(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %r14
	xorl	%esi, %esi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rcx
	pxor	%xmm0, %xmm0
	movq	%rcx, -320(%rbp)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%r14, -448(%rbp)
	movq	$0, -104(%rbp)
	movw	$0, -96(%rbp)
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	-24(%r14), %rax
	movq	%rcx, -448(%rbp,%rax)
	movq	-448(%rbp), %rax
	movq	$0, -440(%rbp)
	addq	-24(%rax), %r15
	movq	%r15, %rdi
.LEHB21:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE21:
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	leaq	-432(%rbp), %r15
	xorl	%esi, %esi
	movq	%rcx, -432(%rbp)
	movq	-24(%rcx), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rcx, -432(%rbp,%rax)
	movq	-432(%rbp), %rax
	movq	-24(%rax), %rcx
	addq	%r15, %rcx
	movq	%rcx, %rdi
.LEHB22:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE22:
	movq	.LC10(%rip), %xmm0
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rcx, -448(%rbp)
	movq	-24(%rcx), %rax
	movhps	.LC11(%rip), %xmm0
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rcx, -448(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rcx, -448(%rbp)
	addq	$80, %rcx
	movq	%rcx, -320(%rbp)
	leaq	-368(%rbp), %rcx
	movq	%rcx, %rdi
	movaps	%xmm0, -432(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rcx, -584(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	-576(%rbp), %rdi
	leaq	-424(%rbp), %rsi
	movq	%rcx, -424(%rbp)
	leaq	-336(%rbp), %rcx
	movl	$24, -360(%rbp)
	movq	%rcx, -600(%rbp)
	movq	%rcx, -352(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
.LEHB23:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE23:
	movl	$16, %edx
	leaq	.LC9(%rip), %rsi
	movq	%r15, %rdi
.LEHB24:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%r12), %rdx
	movq	(%r12), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE24:
	leaq	-528(%rbp), %rax
	movb	$0, -528(%rbp)
	leaq	-544(%rbp), %r15
	movq	%rax, -568(%rbp)
	movq	%rax, -544(%rbp)
	movq	-384(%rbp), %rax
	movq	$0, -536(%rbp)
	testq	%rax, %rax
	je	.L216
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	jbe	.L217
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%rax, %r8
.LEHB25:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.LEHE25:
.L218:
	leaq	-512(%rbp), %rax
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, -592(%rbp)
.LEHB26:
	call	_ZN2v88internal6torque14MessageBuilderC1ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_13TorqueMessage4KindE@PLT
.LEHE26:
	movq	-544(%rbp), %rdi
	cmpq	-568(%rbp), %rdi
	je	.L223
	call	_ZdlPv@PLT
.L223:
	movq	.LC10(%rip), %xmm0
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-352(%rbp), %rdi
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movhps	.LC12(%rip), %xmm0
	movq	%rax, -320(%rbp)
	movaps	%xmm0, -432(%rbp)
	cmpq	-600(%rbp), %rdi
	je	.L224
	call	_ZdlPv@PLT
.L224:
	movq	-584(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	-576(%rbp), %rdi
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -432(%rbp,%rax)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%r14, -448(%rbp)
	movq	-24(%r14), %rax
	movq	%rcx, -448(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	$0, -440(%rbp)
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-592(%rbp), %rdi
.LEHB27:
	call	_ZNK2v88internal6torque14MessageBuilder5ThrowEv@PLT
.LEHE27:
	.p2align 4,,10
	.p2align 3
.L203:
	testq	%r13, %r13
	jne	.L261
	movq	%rbx, %rax
	jmp	.L204
	.p2align 4,,10
	.p2align 3
.L260:
	subq	$3, %rsi
	leaq	-432(%rbp), %rax
	movq	%rax, -448(%rbp)
	movq	%rsi, %rdx
	addq	(%r12), %rdx
	je	.L262
	movzwl	(%rdx), %eax
	movw	%ax, -432(%rbp)
	movzbl	2(%rdx), %edx
	movq	$3, -440(%rbp)
	movb	%dl, -430(%rbp)
	movb	$0, -429(%rbp)
	cmpw	$29742, %ax
	jne	.L232
	cmpb	$113, %dl
	jne	.L232
	xorl	%edx, %edx
	movq	%r12, %rdi
.LEHB28:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE6resizeEmc@PLT
.LEHE28:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L263
	addq	$568, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L259:
	.cfi_restore_state
	movq	%r12, %rdi
	leaq	-552(%rbp), %rsi
	xorl	%edx, %edx
.LEHB29:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%r12)
	movq	%rax, %rdi
	movq	-552(%rbp), %rax
	movq	%rax, 16(%r12)
.L202:
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-552(%rbp), %r13
	movq	(%r12), %rax
	jmp	.L204
	.p2align 4,,10
	.p2align 3
.L257:
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
.L258:
	leaq	.LC2(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE29:
.L216:
	leaq	-352(%rbp), %rsi
	movq	%r15, %rdi
.LEHB30:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L218
.L263:
	call	__stack_chk_fail@PLT
.L217:
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.LEHE30:
	jmp	.L218
.L262:
	leaq	.LC2(%rip), %rdi
.LEHB31:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE31:
.L261:
	movq	%rbx, %rdi
	jmp	.L202
.L235:
	endbr64
	movq	%rax, %r14
	jmp	.L256
.L236:
	endbr64
	movq	%rax, %r13
	jmp	.L215
.L241:
	endbr64
	movq	%rax, %r13
	jmp	.L213
.L239:
	endbr64
	movq	%rax, %r13
	jmp	.L225
.L240:
	endbr64
	movq	%rax, %r13
	jmp	.L212
.L243:
	endbr64
	movq	%rax, %r13
	jmp	.L220
.L238:
	endbr64
	movq	%rax, %r13
	jmp	.L222
.L237:
	endbr64
	movq	%rax, %r14
	jmp	.L229
.L242:
	endbr64
	movq	%rax, %r13
	jmp	.L211
	.section	.gcc_except_table._ZN2v88internal6torque13SourceFileMap30PathFromV8RootWithoutExtensionB5cxx11ENS1_8SourceIdE,"a",@progbits
.LLSDA4247:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE4247-.LLSDACSB4247
.LLSDACSB4247:
	.uleb128 .LEHB21-.LFB4247
	.uleb128 .LEHE21-.LEHB21
	.uleb128 .L240-.LFB4247
	.uleb128 0
	.uleb128 .LEHB22-.LFB4247
	.uleb128 .LEHE22-.LEHB22
	.uleb128 .L242-.LFB4247
	.uleb128 0
	.uleb128 .LEHB23-.LFB4247
	.uleb128 .LEHE23-.LEHB23
	.uleb128 .L241-.LFB4247
	.uleb128 0
	.uleb128 .LEHB24-.LFB4247
	.uleb128 .LEHE24-.LEHB24
	.uleb128 .L238-.LFB4247
	.uleb128 0
	.uleb128 .LEHB25-.LFB4247
	.uleb128 .LEHE25-.LEHB25
	.uleb128 .L243-.LFB4247
	.uleb128 0
	.uleb128 .LEHB26-.LFB4247
	.uleb128 .LEHE26-.LEHB26
	.uleb128 .L239-.LFB4247
	.uleb128 0
	.uleb128 .LEHB27-.LFB4247
	.uleb128 .LEHE27-.LEHB27
	.uleb128 .L237-.LFB4247
	.uleb128 0
	.uleb128 .LEHB28-.LFB4247
	.uleb128 .LEHE28-.LEHB28
	.uleb128 .L236-.LFB4247
	.uleb128 0
	.uleb128 .LEHB29-.LFB4247
	.uleb128 .LEHE29-.LEHB29
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB30-.LFB4247
	.uleb128 .LEHE30-.LEHB30
	.uleb128 .L243-.LFB4247
	.uleb128 0
	.uleb128 .LEHB31-.LFB4247
	.uleb128 .LEHE31-.LEHB31
	.uleb128 .L235-.LFB4247
	.uleb128 0
.LLSDACSE4247:
	.section	.text._ZN2v88internal6torque13SourceFileMap30PathFromV8RootWithoutExtensionB5cxx11ENS1_8SourceIdE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque13SourceFileMap30PathFromV8RootWithoutExtensionB5cxx11ENS1_8SourceIdE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC4247
	.type	_ZN2v88internal6torque13SourceFileMap30PathFromV8RootWithoutExtensionB5cxx11ENS1_8SourceIdE.cold, @function
_ZN2v88internal6torque13SourceFileMap30PathFromV8RootWithoutExtensionB5cxx11ENS1_8SourceIdE.cold:
.LFSB4247:
.L229:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-592(%rbp), %rdi
	call	_ZNK2v88internal6torque14MessageBuilder6ReportEv@PLT
.L256:
	movq	-512(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L230
	call	_ZdlPv@PLT
.L230:
	movq	%r14, %r13
.L215:
	movq	(%r12), %rdi
	cmpq	%rdi, %rbx
	je	.L231
	call	_ZdlPv@PLT
.L231:
	movq	%r13, %rdi
.LEHB32:
	call	_Unwind_Resume@PLT
.LEHE32:
.L220:
	movq	-544(%rbp), %rdi
	cmpq	-568(%rbp), %rdi
	je	.L222
	call	_ZdlPv@PLT
.L222:
	movq	-608(%rbp), %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	jmp	.L215
.L213:
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-352(%rbp), %rdi
	movq	%rax, -424(%rbp)
	cmpq	-600(%rbp), %rdi
	je	.L214
	call	_ZdlPv@PLT
.L214:
	movq	-584(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -432(%rbp,%rax)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%r14, -448(%rbp)
	movq	-24(%r14), %rax
	movq	%rcx, -448(%rbp,%rax)
	movq	$0, -440(%rbp)
.L212:
	movq	-576(%rbp), %rdi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	jmp	.L215
.L211:
	movq	%r14, -448(%rbp)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	-24(%r14), %rax
	movq	%rcx, -448(%rbp,%rax)
	movq	$0, -440(%rbp)
	jmp	.L212
.L225:
	movq	-544(%rbp), %rdi
	cmpq	-568(%rbp), %rdi
	je	.L222
	call	_ZdlPv@PLT
	jmp	.L222
	.cfi_endproc
.LFE4247:
	.section	.gcc_except_table._ZN2v88internal6torque13SourceFileMap30PathFromV8RootWithoutExtensionB5cxx11ENS1_8SourceIdE
.LLSDAC4247:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC4247-.LLSDACSBC4247
.LLSDACSBC4247:
	.uleb128 .LEHB32-.LCOLDB13
	.uleb128 .LEHE32-.LEHB32
	.uleb128 0
	.uleb128 0
.LLSDACSEC4247:
	.section	.text.unlikely._ZN2v88internal6torque13SourceFileMap30PathFromV8RootWithoutExtensionB5cxx11ENS1_8SourceIdE
	.section	.text._ZN2v88internal6torque13SourceFileMap30PathFromV8RootWithoutExtensionB5cxx11ENS1_8SourceIdE
	.size	_ZN2v88internal6torque13SourceFileMap30PathFromV8RootWithoutExtensionB5cxx11ENS1_8SourceIdE, .-_ZN2v88internal6torque13SourceFileMap30PathFromV8RootWithoutExtensionB5cxx11ENS1_8SourceIdE
	.section	.text.unlikely._ZN2v88internal6torque13SourceFileMap30PathFromV8RootWithoutExtensionB5cxx11ENS1_8SourceIdE
	.size	_ZN2v88internal6torque13SourceFileMap30PathFromV8RootWithoutExtensionB5cxx11ENS1_8SourceIdE.cold, .-_ZN2v88internal6torque13SourceFileMap30PathFromV8RootWithoutExtensionB5cxx11ENS1_8SourceIdE.cold
.LCOLDE13:
	.section	.text._ZN2v88internal6torque13SourceFileMap30PathFromV8RootWithoutExtensionB5cxx11ENS1_8SourceIdE
.LHOTE13:
	.section	.text._ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_,"axG",@progbits,_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	.type	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_, @function
_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_:
.LFB4894:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r12
	movq	(%rdi), %r15
	movabsq	$288230376151711743, %rdi
	movq	%r12, %rax
	subq	%r15, %rax
	sarq	$5, %rax
	cmpq	%rdi, %rax
	je	.L291
	movq	%rsi, %rcx
	movq	%rsi, %rbx
	subq	%r15, %rcx
	testq	%rax, %rax
	je	.L282
	movabsq	$9223372036854775776, %rsi
	leaq	(%rax,%rax), %r8
	cmpq	%r8, %rax
	jbe	.L292
.L266:
	movq	%rsi, %rdi
	movq	%rdx, -72(%rbp)
	movq	%rcx, -64(%rbp)
	movq	%rsi, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rsi
	movq	-64(%rbp), %rcx
	movq	%rax, %r14
	movq	-72(%rbp), %rdx
	leaq	(%rax,%rsi), %rax
	leaq	32(%r14), %r8
.L281:
	addq	%r14, %rcx
	movq	(%rdx), %rdi
	leaq	16(%rcx), %rsi
	movq	%rsi, (%rcx)
	leaq	16(%rdx), %rsi
	cmpq	%rsi, %rdi
	je	.L293
	movq	%rdi, (%rcx)
	movq	16(%rdx), %rdi
	movq	%rdi, 16(%rcx)
.L269:
	movq	8(%rdx), %rdi
	movq	%rsi, (%rdx)
	movq	$0, 8(%rdx)
	movq	%rdi, 8(%rcx)
	movb	$0, 16(%rdx)
	cmpq	%r15, %rbx
	je	.L270
	movq	%r14, %rcx
	movq	%r15, %rdx
	jmp	.L274
	.p2align 4,,10
	.p2align 3
.L271:
	movq	%rdi, (%rcx)
	movq	16(%rdx), %rsi
	movq	%rsi, 16(%rcx)
.L290:
	movq	8(%rdx), %rsi
	addq	$32, %rdx
	addq	$32, %rcx
	movq	%rsi, -24(%rcx)
	cmpq	%rdx, %rbx
	je	.L294
.L274:
	leaq	16(%rcx), %rsi
	movq	%rsi, (%rcx)
	movq	(%rdx), %rdi
	leaq	16(%rdx), %rsi
	cmpq	%rsi, %rdi
	jne	.L271
	movdqu	16(%rdx), %xmm1
	movups	%xmm1, 16(%rcx)
	jmp	.L290
	.p2align 4,,10
	.p2align 3
.L294:
	movq	%rbx, %rdx
	subq	%r15, %rdx
	leaq	32(%r14,%rdx), %r8
.L270:
	cmpq	%r12, %rbx
	je	.L275
	movq	%rbx, %rdx
	movq	%r8, %rcx
	.p2align 4,,10
	.p2align 3
.L279:
	leaq	16(%rcx), %rsi
	movq	(%rdx), %rdi
	movq	%rsi, (%rcx)
	leaq	16(%rdx), %rsi
	cmpq	%rsi, %rdi
	je	.L295
	movq	16(%rdx), %rsi
	addq	$32, %rdx
	movq	%rdi, (%rcx)
	addq	$32, %rcx
	movq	%rsi, -16(%rcx)
	movq	-24(%rdx), %rsi
	movq	%rsi, -24(%rcx)
	cmpq	%r12, %rdx
	jne	.L279
.L277:
	subq	%rbx, %r12
	addq	%r12, %r8
.L275:
	testq	%r15, %r15
	je	.L280
	movq	%r15, %rdi
	movq	%rax, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-64(%rbp), %rax
	movq	-56(%rbp), %r8
.L280:
	movq	%r14, %xmm0
	movq	%r8, %xmm3
	movq	%rax, 16(%r13)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 0(%r13)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L295:
	.cfi_restore_state
	movdqu	16(%rdx), %xmm2
	movq	8(%rdx), %rsi
	addq	$32, %rdx
	addq	$32, %rcx
	movups	%xmm2, -16(%rcx)
	movq	%rsi, -24(%rcx)
	cmpq	%rdx, %r12
	jne	.L279
	jmp	.L277
	.p2align 4,,10
	.p2align 3
.L292:
	testq	%r8, %r8
	jne	.L267
	movl	$32, %r8d
	xorl	%eax, %eax
	xorl	%r14d, %r14d
	jmp	.L281
	.p2align 4,,10
	.p2align 3
.L282:
	movl	$32, %esi
	jmp	.L266
	.p2align 4,,10
	.p2align 3
.L293:
	movdqu	16(%rdx), %xmm4
	movups	%xmm4, 16(%rcx)
	jmp	.L269
.L267:
	cmpq	%rdi, %r8
	movq	%rdi, %rax
	cmovbe	%r8, %rax
	salq	$5, %rax
	movq	%rax, %rsi
	jmp	.L266
.L291:
	leaq	.LC6(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE4894:
	.size	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_, .-_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	.section	.text._ZN2v88internal6torque13SourceFileMap9AddSourceENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque13SourceFileMap9AddSourceENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN2v88internal6torque13SourceFileMap9AddSourceENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN2v88internal6torque13SourceFileMap9AddSourceENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB4248:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rdx
	movq	%fs:_ZZN2v88internal6torque21ContextualVariableTopINS1_13SourceFileMapEEERPNT_5ScopeEvE3top@tpoff, %rdi
	movq	8(%rdi), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	cmpq	16(%rdi), %rsi
	je	.L297
	leaq	16(%rsi), %rax
	movq	%rax, (%rsi)
	movq	(%rdx), %rcx
	leaq	16(%rdx), %rax
	cmpq	%rax, %rcx
	je	.L302
	movq	%rcx, (%rsi)
	movq	16(%rdx), %rcx
	movq	%rcx, 16(%rsi)
.L299:
	movq	8(%rdx), %rcx
	movq	%rax, (%rdx)
	movq	$0, 8(%rdx)
	movq	%rcx, 8(%rsi)
	movb	$0, 16(%rdx)
	addq	$32, 8(%rdi)
.L300:
	movq	%fs:_ZZN2v88internal6torque21ContextualVariableTopINS1_13SourceFileMapEEERPNT_5ScopeEvE3top@tpoff, %rdx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	8(%rdx), %rax
	subq	(%rdx), %rax
	sarq	$5, %rax
	subl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L302:
	.cfi_restore_state
	movdqu	16(%rdx), %xmm0
	movups	%xmm0, 16(%rsi)
	jmp	.L299
	.p2align 4,,10
	.p2align 3
.L297:
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	jmp	.L300
	.cfi_endproc
.LFE4248:
	.size	_ZN2v88internal6torque13SourceFileMap9AddSourceENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN2v88internal6torque13SourceFileMap9AddSourceENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal6torque21ContextualVariableTopINS1_17CurrentSourceFileEEERPNT_5ScopeEv,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal6torque21ContextualVariableTopINS1_17CurrentSourceFileEEERPNT_5ScopeEv, @function
_GLOBAL__sub_I__ZN2v88internal6torque21ContextualVariableTopINS1_17CurrentSourceFileEEERPNT_5ScopeEv:
.LFB5169:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE5169:
	.size	_GLOBAL__sub_I__ZN2v88internal6torque21ContextualVariableTopINS1_17CurrentSourceFileEEERPNT_5ScopeEv, .-_GLOBAL__sub_I__ZN2v88internal6torque21ContextualVariableTopINS1_17CurrentSourceFileEEERPNT_5ScopeEv
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal6torque21ContextualVariableTopINS1_17CurrentSourceFileEEERPNT_5ScopeEv
	.section	.tbss._ZZN2v88internal6torque21ContextualVariableTopINS1_13SourceFileMapEEERPNT_5ScopeEvE3top,"awT",@nobits
	.align 8
	.type	_ZZN2v88internal6torque21ContextualVariableTopINS1_13SourceFileMapEEERPNT_5ScopeEvE3top, @object
	.size	_ZZN2v88internal6torque21ContextualVariableTopINS1_13SourceFileMapEEERPNT_5ScopeEvE3top, 8
_ZZN2v88internal6torque21ContextualVariableTopINS1_13SourceFileMapEEERPNT_5ScopeEvE3top:
	.zero	8
	.section	.tbss._ZZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEvE3top,"awT",@nobits
	.align 8
	.type	_ZZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEvE3top, @object
	.size	_ZZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEvE3top, 8
_ZZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEvE3top:
	.zero	8
	.section	.tbss._ZZN2v88internal6torque21ContextualVariableTopINS1_17CurrentSourceFileEEERPNT_5ScopeEvE3top,"awT",@nobits
	.align 8
	.type	_ZZN2v88internal6torque21ContextualVariableTopINS1_17CurrentSourceFileEEERPNT_5ScopeEvE3top, @object
	.size	_ZZN2v88internal6torque21ContextualVariableTopINS1_17CurrentSourceFileEEERPNT_5ScopeEvE3top, 8
_ZZN2v88internal6torque21ContextualVariableTopINS1_17CurrentSourceFileEEERPNT_5ScopeEvE3top:
	.zero	8
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.data.rel.ro,"aw"
	.align 8
.LC10:
	.quad	_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE+64
	.align 8
.LC11:
	.quad	_ZTVSt15basic_streambufIcSt11char_traitsIcEE+16
	.align 8
.LC12:
	.quad	_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE+16
	.hidden	DW.ref.__gxx_personality_v0
	.weak	DW.ref.__gxx_personality_v0
	.section	.data.rel.local.DW.ref.__gxx_personality_v0,"awG",@progbits,DW.ref.__gxx_personality_v0,comdat
	.align 8
	.type	DW.ref.__gxx_personality_v0, @object
	.size	DW.ref.__gxx_personality_v0, 8
DW.ref.__gxx_personality_v0:
	.quad	__gxx_personality_v0
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
