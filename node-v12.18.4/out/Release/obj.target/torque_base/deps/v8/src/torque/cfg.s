	.file	"cfg.cc"
	.text
	.section	.text._ZNK2v88internal6torque15InstructionBase17IsBlockTerminatorEv,"axG",@progbits,_ZNK2v88internal6torque15InstructionBase17IsBlockTerminatorEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal6torque15InstructionBase17IsBlockTerminatorEv
	.type	_ZNK2v88internal6torque15InstructionBase17IsBlockTerminatorEv, @function
_ZNK2v88internal6torque15InstructionBase17IsBlockTerminatorEv:
.LFB6070:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE6070:
	.size	_ZNK2v88internal6torque15InstructionBase17IsBlockTerminatorEv, .-_ZNK2v88internal6torque15InstructionBase17IsBlockTerminatorEv
	.section	.text._ZNK2v88internal6torque15InstructionBase21AppendSuccessorBlocksEPSt6vectorIPNS1_5BlockESaIS5_EE,"axG",@progbits,_ZNK2v88internal6torque15InstructionBase21AppendSuccessorBlocksEPSt6vectorIPNS1_5BlockESaIS5_EE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal6torque15InstructionBase21AppendSuccessorBlocksEPSt6vectorIPNS1_5BlockESaIS5_EE
	.type	_ZNK2v88internal6torque15InstructionBase21AppendSuccessorBlocksEPSt6vectorIPNS1_5BlockESaIS5_EE, @function
_ZNK2v88internal6torque15InstructionBase21AppendSuccessorBlocksEPSt6vectorIPNS1_5BlockESaIS5_EE:
.LFB6071:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE6071:
	.size	_ZNK2v88internal6torque15InstructionBase21AppendSuccessorBlocksEPSt6vectorIPNS1_5BlockESaIS5_EE, .-_ZNK2v88internal6torque15InstructionBase21AppendSuccessorBlocksEPSt6vectorIPNS1_5BlockESaIS5_EE
	.section	.text._ZNSt6vectorIPKN2v88internal6torque4TypeESaIS5_EEaSERKS7_,"axG",@progbits,_ZNSt6vectorIPKN2v88internal6torque4TypeESaIS5_EEaSERKS7_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPKN2v88internal6torque4TypeESaIS5_EEaSERKS7_
	.type	_ZNSt6vectorIPKN2v88internal6torque4TypeESaIS5_EEaSERKS7_, @function
_ZNSt6vectorIPKN2v88internal6torque4TypeESaIS5_EEaSERKS7_:
.LFB7772:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	cmpq	%rdi, %rsi
	je	.L5
	movq	8(%rsi), %rdx
	movq	(%rsi), %r14
	movq	%rsi, %rbx
	movq	(%rdi), %r15
	movq	16(%rdi), %rax
	movq	%rdx, %r13
	subq	%r14, %r13
	subq	%r15, %rax
	movq	%r13, %rcx
	sarq	$3, %rax
	sarq	$3, %rcx
	cmpq	%rax, %rcx
	ja	.L25
	movq	8(%rdi), %rdi
	movq	%rdi, %rsi
	subq	%r15, %rsi
	movq	%rsi, %rax
	sarq	$3, %rax
	cmpq	%rax, %rcx
	ja	.L13
	cmpq	%rdx, %r14
	je	.L24
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	memmove@PLT
	addq	(%r12), %r13
	movq	%r13, 8(%r12)
.L5:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	.cfi_restore_state
	xorl	%ebx, %ebx
	testq	%rcx, %rcx
	je	.L8
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rcx
	ja	.L26
	movq	%r13, %rdi
	movq	%rdx, -56(%rbp)
	call	_Znwm@PLT
	movq	(%r12), %r15
	movq	-56(%rbp), %rdx
	movq	%rax, %rbx
.L8:
	cmpq	%rdx, %r14
	je	.L10
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	memcpy@PLT
.L10:
	testq	%r15, %r15
	je	.L11
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L11:
	addq	%rbx, %r13
	movq	%rbx, (%r12)
	movq	%r13, 16(%r12)
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L13:
	testq	%rsi, %rsi
	je	.L15
	movq	%rsi, %rdx
	movq	%r15, %rdi
	movq	%r14, %rsi
	call	memmove@PLT
	movq	8(%r12), %rdi
	movq	(%r12), %r15
	movq	8(%rbx), %rdx
	movq	(%rbx), %r14
	movq	%rdi, %rsi
	subq	%r15, %rsi
.L15:
	addq	%r14, %rsi
	cmpq	%rdx, %rsi
	jne	.L16
.L24:
	addq	%r15, %r13
.L12:
	movq	%r13, 8(%r12)
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L16:
	subq	%rsi, %rdx
	call	memmove@PLT
	addq	(%r12), %r13
	jmp	.L12
.L26:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE7772:
	.size	_ZNSt6vectorIPKN2v88internal6torque4TypeESaIS5_EEaSERKS7_, .-_ZNSt6vectorIPKN2v88internal6torque4TypeESaIS5_EEaSERKS7_
	.section	.text._ZN2v88internal6torque11ReportErrorIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_,"axG",@progbits,_ZN2v88internal6torque11ReportErrorIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_,comdat
	.p2align 4
	.weak	_ZN2v88internal6torque11ReportErrorIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_
	.type	_ZN2v88internal6torque11ReportErrorIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_, @function
_ZN2v88internal6torque11ReportErrorIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_:
.LFB8059:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA8059
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-432(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%r13, %rdi
	subq	$496, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
.LEHB0:
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEC1Ev@PLT
.LEHE0:
	movq	8(%rbx), %rdx
	movq	(%rbx), %rsi
	leaq	-416(%rbp), %rdi
.LEHB1:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	-528(%rbp), %r14
	leaq	-408(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZNKSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEE3strEv@PLT
.LEHE1:
	leaq	-496(%rbp), %r12
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
.LEHB2:
	call	_ZN2v88internal6torque14MessageBuilderC1ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_13TorqueMessage4KindE@PLT
.LEHE2:
	movq	-528(%rbp), %rdi
	leaq	-512(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L28
	call	_ZdlPv@PLT
.L28:
	movq	%r13, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r12, %rdi
.LEHB3:
	call	_ZNK2v88internal6torque14MessageBuilder5ThrowEv@PLT
.LEHE3:
.L36:
	endbr64
	movq	%rax, %r12
	jmp	.L31
.L35:
	endbr64
	movq	%rax, %r13
	jmp	.L32
.L37:
	endbr64
	movq	%rax, %r12
.L29:
	movq	-528(%rbp), %rdi
	leaq	-512(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L31
	call	_ZdlPv@PLT
.L31:
	movq	%r13, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r12, %rdi
.LEHB4:
	call	_Unwind_Resume@PLT
.LEHE4:
.L32:
	movq	%r12, %rdi
	call	_ZNK2v88internal6torque14MessageBuilder6ReportEv@PLT
	movq	-496(%rbp), %rdi
	leaq	-480(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L33
	call	_ZdlPv@PLT
.L33:
	movq	%r13, %rdi
.LEHB5:
	call	_Unwind_Resume@PLT
.LEHE5:
	.cfi_endproc
.LFE8059:
	.globl	__gxx_personality_v0
	.section	.gcc_except_table._ZN2v88internal6torque11ReportErrorIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_,"aG",@progbits,_ZN2v88internal6torque11ReportErrorIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_,comdat
.LLSDA8059:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE8059-.LLSDACSB8059
.LLSDACSB8059:
	.uleb128 .LEHB0-.LFB8059
	.uleb128 .LEHE0-.LEHB0
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB1-.LFB8059
	.uleb128 .LEHE1-.LEHB1
	.uleb128 .L36-.LFB8059
	.uleb128 0
	.uleb128 .LEHB2-.LFB8059
	.uleb128 .LEHE2-.LEHB2
	.uleb128 .L37-.LFB8059
	.uleb128 0
	.uleb128 .LEHB3-.LFB8059
	.uleb128 .LEHE3-.LEHB3
	.uleb128 .L35-.LFB8059
	.uleb128 0
	.uleb128 .LEHB4-.LFB8059
	.uleb128 .LEHE4-.LEHB4
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB5-.LFB8059
	.uleb128 .LEHE5-.LEHB5
	.uleb128 0
	.uleb128 0
.LLSDACSE8059:
	.section	.text._ZN2v88internal6torque11ReportErrorIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_,"axG",@progbits,_ZN2v88internal6torque11ReportErrorIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_,comdat
	.size	_ZN2v88internal6torque11ReportErrorIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_, .-_ZN2v88internal6torque11ReportErrorIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_
	.section	.text._ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E,"axG",@progbits,_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	.type	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E, @function
_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E:
.LFB8515:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L54
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
.L43:
	movq	24(%r12), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r12), %rdi
	leaq	48(%r12), %rax
	movq	16(%r12), %rbx
	cmpq	%rax, %rdi
	je	.L41
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L39
.L42:
	movq	%rbx, %r12
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L41:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L42
.L39:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L54:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.cfi_endproc
.LFE8515:
	.size	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E, .-_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	.section	.text._ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE16_M_insert_uniqueIRKS5_EESt4pairISt17_Rb_tree_iteratorIS5_EbEOT_,"axG",@progbits,_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE16_M_insert_uniqueIRKS5_EESt4pairISt17_Rb_tree_iteratorIS5_EbEOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE16_M_insert_uniqueIRKS5_EESt4pairISt17_Rb_tree_iteratorIS5_EbEOT_
	.type	_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE16_M_insert_uniqueIRKS5_EESt4pairISt17_Rb_tree_iteratorIS5_EbEOT_, @function
_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE16_M_insert_uniqueIRKS5_EESt4pairISt17_Rb_tree_iteratorIS5_EbEOT_:
.LFB8564:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	8(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r12
	testq	%r12, %r12
	jne	.L59
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L76:
	movq	16(%r12), %rdx
	testq	%rdx, %rdx
	je	.L60
.L77:
	movq	%rdx, %r12
.L59:
	movq	32(%r12), %rsi
	movq	(%r14), %rdi
	call	_ZN2v88internal6torqueltERKNS1_4TypeES4_@PLT
	testb	%al, %al
	jne	.L76
	movq	24(%r12), %rdx
	testq	%rdx, %rdx
	jne	.L77
.L60:
	testb	%al, %al
	je	.L78
	cmpq	24(%r13), %r12
	je	.L67
.L69:
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	(%r14), %rsi
	movq	32(%rax), %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal6torqueltERKNS1_4TypeES4_@PLT
	testb	%al, %al
	jne	.L67
.L68:
	addq	$24, %rsp
	movq	%rbx, %rax
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L78:
	.cfi_restore_state
	movq	32(%r12), %rdi
	movq	(%r14), %rsi
	movq	%r12, %rbx
	call	_ZN2v88internal6torqueltERKNS1_4TypeES4_@PLT
	testb	%al, %al
	je	.L68
.L67:
	movl	$1, %r8d
	cmpq	%r15, %r12
	jne	.L79
.L65:
	movl	$40, %edi
	movl	%r8d, -52(%rbp)
	call	_Znwm@PLT
	movl	-52(%rbp), %r8d
	movq	%r12, %rdx
	movq	%r15, %rcx
	movq	%rax, %rbx
	movq	(%r14), %rax
	movq	%rbx, %rsi
	movl	%r8d, %edi
	movq	%rax, 32(%rbx)
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 40(%r13)
	addq	$24, %rsp
	movq	%rbx, %rax
	movl	$1, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L79:
	.cfi_restore_state
	movq	32(%r12), %rsi
	movq	(%r14), %rdi
	call	_ZN2v88internal6torqueltERKNS1_4TypeES4_@PLT
	movzbl	%al, %r8d
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L75:
	movq	%r15, %r12
	cmpq	24(%rdi), %r15
	jne	.L69
	movl	$1, %r8d
	jmp	.L65
	.cfi_endproc
.LFE8564:
	.size	_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE16_M_insert_uniqueIRKS5_EESt4pairISt17_Rb_tree_iteratorIS5_EbEOT_, .-_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE16_M_insert_uniqueIRKS5_EESt4pairISt17_Rb_tree_iteratorIS5_EbEOT_
	.section	.text._ZN2v88internal6torque9UnionType6ExtendEPKNS1_4TypeE,"axG",@progbits,_ZN2v88internal6torque9UnionType6ExtendEPKNS1_4TypeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque9UnionType6ExtendEPKNS1_4TypeE
	.type	_ZN2v88internal6torque9UnionType6ExtendEPKNS1_4TypeE, @function
_ZN2v88internal6torque9UnionType6ExtendEPKNS1_4TypeE:
.LFB5813:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	movq	%rsi, %rdi
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rsi, -40(%rbp)
	testq	%rsi, %rsi
	je	.L81
	cmpl	$3, 8(%rsi)
	jne	.L81
	movq	96(%rsi), %r12
	leaq	80(%rsi), %rbx
	cmpq	%rbx, %r12
	je	.L80
.L82:
	movq	32(%r12), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal6torque9UnionType6ExtendEPKNS1_4TypeE
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r12
	cmpq	%rax, %rbx
	jne	.L82
.L80:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L81:
	.cfi_restore_state
	movq	(%rdi), %rax
	movq	%r13, %rsi
	call	*16(%rax)
	testb	%al, %al
	jne	.L80
	movq	16(%r13), %rdi
	movq	-40(%rbp), %rsi
	leaq	80(%r13), %rbx
	call	_ZN2v88internal6torque4Type15CommonSupertypeEPKS2_S4_@PLT
	movq	96(%r13), %r12
	movq	%rax, 16(%r13)
	cmpq	%rbx, %r12
	je	.L86
	.p2align 4,,10
	.p2align 3
.L85:
	movq	32(%r12), %rdi
	movq	-40(%rbp), %rsi
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	%r12, %rdi
	testb	%al, %al
	jne	.L97
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r12
	cmpq	%rax, %rbx
	jne	.L85
.L86:
	leaq	-40(%rbp), %rsi
	leaq	72(%r13), %rdi
	call	_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE16_M_insert_uniqueIRKS5_EESt4pairISt17_Rb_tree_iteratorIS5_EbEOT_
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L97:
	.cfi_restore_state
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZSt28_Rb_tree_rebalance_for_erasePSt18_Rb_tree_node_baseRS_@PLT
	movq	%rax, %rdi
	call	_ZdlPv@PLT
	subq	$1, 112(%r13)
	cmpq	%r14, %rbx
	je	.L86
	movq	%r14, %r12
	jmp	.L85
	.cfi_endproc
.LFE5813:
	.size	_ZN2v88internal6torque9UnionType6ExtendEPKNS1_4TypeE, .-_ZN2v88internal6torque9UnionType6ExtendEPKNS1_4TypeE
	.section	.text._ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E,"axG",@progbits,_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	.type	_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E, @function
_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E:
.LFB8589:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L106
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
.L100:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	%rbx, %rdi
	movq	16(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L100
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L106:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE8589:
	.size	_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E, .-_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	.section	.rodata._ZNSt6vectorIPN2v88internal6torque5BlockESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_.str1.1,"aMS",@progbits,1
.LC0:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorIPN2v88internal6torque5BlockESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_,"axG",@progbits,_ZNSt6vectorIPN2v88internal6torque5BlockESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPN2v88internal6torque5BlockESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	.type	_ZNSt6vectorIPN2v88internal6torque5BlockESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_, @function
_ZNSt6vectorIPN2v88internal6torque5BlockESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_:
.LFB8744:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r9
	movq	%r13, %rax
	subq	%r9, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L123
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L119
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L124
.L111:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L118:
	movq	(%r15), %rax
	subq	%r8, %r13
	leaq	8(%rbx,%rdx), %r15
	movq	%rax, (%rbx,%rdx)
	leaq	(%r15,%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L125
	testq	%r13, %r13
	jg	.L114
	testq	%r9, %r9
	jne	.L117
.L115:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L125:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jg	.L114
.L117:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L124:
	testq	%rsi, %rsi
	jne	.L112
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L114:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L115
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L119:
	movl	$8, %r14d
	jmp	.L111
.L123:
	leaq	.LC0(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L112:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$3, %r14
	jmp	.L111
	.cfi_endproc
.LFE8744:
	.size	_ZNSt6vectorIPN2v88internal6torque5BlockESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_, .-_ZNSt6vectorIPN2v88internal6torque5BlockESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	.section	.text._ZN2v88internal6torque12CfgAssembler4BindEPNS1_5BlockE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque12CfgAssembler4BindEPNS1_5BlockE
	.type	_ZN2v88internal6torque12CfgAssembler4BindEPNS1_5BlockE, @function
_ZN2v88internal6torque12CfgAssembler4BindEPNS1_5BlockE:
.LFB6736:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	40(%rsi), %rax
	movq	%rsi, 120(%rdi)
	cmpq	%rax, %rdi
	je	.L127
	movq	48(%rsi), %rdx
	movq	40(%rsi), %r14
	movq	(%rdi), %r15
	movq	16(%rdi), %rax
	movq	%rdx, %r13
	subq	%r14, %r13
	subq	%r15, %rax
	movq	%r13, %rcx
	sarq	$3, %rax
	sarq	$3, %rcx
	cmpq	%rax, %rcx
	ja	.L150
	movq	8(%rdi), %rdi
	movq	%rdi, %rsi
	subq	%r15, %rsi
	movq	%rsi, %rax
	sarq	$3, %rax
	cmpq	%rax, %rcx
	ja	.L135
	cmpq	%rdx, %r14
	je	.L149
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	memmove@PLT
	addq	(%rbx), %r13
	movq	%r13, 8(%rbx)
.L127:
	movq	%r12, -64(%rbp)
	movq	64(%rbx), %rsi
	cmpq	72(%rbx), %rsi
	je	.L139
.L153:
	movq	%r12, (%rsi)
	addq	$8, 64(%rbx)
.L126:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L151
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L150:
	.cfi_restore_state
	testq	%rcx, %rcx
	je	.L130
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rcx
	ja	.L152
	movq	%r13, %rdi
	movq	%rdx, -72(%rbp)
	call	_Znwm@PLT
	movq	(%rbx), %r15
	movq	-72(%rbp), %rdx
	movq	%rax, %rcx
.L130:
	cmpq	%rdx, %r14
	je	.L132
	movq	%rcx, %rdi
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	%rax, %rcx
.L132:
	testq	%r15, %r15
	je	.L133
	movq	%r15, %rdi
	movq	%rcx, -72(%rbp)
	call	_ZdlPv@PLT
	movq	-72(%rbp), %rcx
.L133:
	addq	%rcx, %r13
	movq	%rcx, (%rbx)
	movq	%r13, 16(%rbx)
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L135:
	testq	%rsi, %rsi
	je	.L137
	movq	%rsi, %rdx
	movq	%r15, %rdi
	movq	%r14, %rsi
	call	memmove@PLT
	movq	8(%rbx), %rdi
	movq	(%rbx), %r15
	movq	48(%r12), %rdx
	movq	40(%r12), %r14
	movq	%rdi, %rsi
	subq	%r15, %rsi
.L137:
	addq	%r14, %rsi
	cmpq	%rdx, %rsi
	jne	.L138
.L149:
	addq	%r15, %r13
.L134:
	movq	%r13, 8(%rbx)
	movq	64(%rbx), %rsi
	movq	%r12, -64(%rbp)
	cmpq	72(%rbx), %rsi
	jne	.L153
.L139:
	leaq	-64(%rbp), %rdx
	leaq	56(%rbx), %rdi
	call	_ZNSt6vectorIPN2v88internal6torque5BlockESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L138:
	subq	%rsi, %rdx
	call	memmove@PLT
	addq	(%rbx), %r13
	jmp	.L134
.L151:
	call	__stack_chk_fail@PLT
.L152:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE6736:
	.size	_ZN2v88internal6torque12CfgAssembler4BindEPNS1_5BlockE, .-_ZN2v88internal6torque12CfgAssembler4BindEPNS1_5BlockE
	.section	.text._ZNSt6vectorIPKN2v88internal6torque4TypeESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_,"axG",@progbits,_ZNSt6vectorIPKN2v88internal6torque4TypeESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPKN2v88internal6torque4TypeESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	.type	_ZNSt6vectorIPKN2v88internal6torque4TypeESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_, @function
_ZNSt6vectorIPKN2v88internal6torque4TypeESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_:
.LFB9471:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r9
	movq	%r13, %rax
	subq	%r9, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L168
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L164
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L169
.L156:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L163:
	movq	(%r15), %rax
	subq	%r8, %r13
	leaq	8(%rbx,%rdx), %r15
	movq	%rax, (%rbx,%rdx)
	leaq	(%r15,%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L170
	testq	%r13, %r13
	jg	.L159
	testq	%r9, %r9
	jne	.L162
.L160:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L170:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jg	.L159
.L162:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L160
	.p2align 4,,10
	.p2align 3
.L169:
	testq	%rsi, %rsi
	jne	.L157
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L163
	.p2align 4,,10
	.p2align 3
.L159:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L160
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L164:
	movl	$8, %r14d
	jmp	.L156
.L168:
	leaq	.LC0(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L157:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$3, %r14
	jmp	.L156
	.cfi_endproc
.LFE9471:
	.size	_ZNSt6vectorIPKN2v88internal6torque4TypeESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_, .-_ZNSt6vectorIPKN2v88internal6torque4TypeESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	.section	.text._ZNSt6vectorIN2v88internal6torque11InstructionESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal6torque11InstructionESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal6torque11InstructionESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.type	_ZNSt6vectorIN2v88internal6torque11InstructionESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, @function
_ZNSt6vectorIN2v88internal6torque11InstructionESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_:
.LFB9541:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA9541
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	8(%rdi), %r14
	movq	(%rdi), %rdx
	movq	%rdi, -96(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r14, %rax
	movq	%rdx, -72(%rbp)
	subq	%rdx, %rax
	movabsq	$576460752303423487, %rdx
	sarq	$4, %rax
	cmpq	%rdx, %rax
	je	.L236
	movq	%rsi, %r13
	movq	%rsi, %r15
	movq	%rsi, %rbx
	subq	-72(%rbp), %r13
	testq	%rax, %rax
	je	.L206
	leaq	(%rax,%rax), %rcx
	movq	%rcx, -88(%rbp)
	cmpq	%rcx, %rax
	jbe	.L237
	movabsq	$9223372036854775792, %rax
	movq	%rax, -88(%rbp)
.L173:
	movq	-88(%rbp), %rdi
.LEHB6:
	call	_Znwm@PLT
.LEHE6:
	movq	%rax, -80(%rbp)
.L204:
	movq	%r13, %rcx
	movq	-80(%rbp), %r13
	movl	(%r12), %eax
	movq	8(%r12), %rsi
	addq	%r13, %rcx
	movl	%eax, (%rcx)
	movq	(%rsi), %rax
	leaq	8(%rcx), %rdi
	movq	%rcx, -104(%rbp)
.LEHB7:
	call	*(%rax)
.LEHE7:
	movq	-72(%rbp), %r12
	cmpq	%r12, %r15
	je	.L208
	.p2align 4,,10
	.p2align 3
.L176:
	movl	(%r12), %eax
	leaq	8(%r13), %rdi
	movl	%eax, 0(%r13)
	movq	8(%r12), %rsi
	movq	(%rsi), %rax
.LEHB8:
	call	*(%rax)
.LEHE8:
	addq	$16, %r12
	addq	$16, %r13
	cmpq	%r12, %r15
	jne	.L176
.L175:
	addq	$16, %r13
	movq	%r13, %r12
	cmpq	%r14, %r15
	je	.L177
	.p2align 4,,10
	.p2align 3
.L178:
	movl	(%rbx), %eax
	movq	8(%rbx), %rsi
	leaq	8(%r12), %rdi
	movl	%eax, (%r12)
	movq	(%rsi), %rax
.LEHB9:
	call	*(%rax)
.LEHE9:
	addq	$16, %rbx
	addq	$16, %r12
	cmpq	%rbx, %r14
	jne	.L178
.L177:
	movq	-72(%rbp), %rax
	movq	%rax, %rbx
	cmpq	%r14, %rax
	je	.L195
	.p2align 4,,10
	.p2align 3
.L185:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L194
	movq	(%rdi), %rax
	addq	$16, %rbx
	call	*24(%rax)
	cmpq	%r14, %rbx
	jne	.L185
.L195:
	cmpq	$0, -72(%rbp)
	je	.L187
	movq	-72(%rbp), %rdi
	call	_ZdlPv@PLT
.L187:
	movq	-80(%rbp), %rax
	movq	-96(%rbp), %rdx
	movq	%r12, %xmm1
	movq	%rax, %xmm0
	addq	-88(%rbp), %rax
	movq	%rax, 16(%rdx)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rdx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L238
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L237:
	.cfi_restore_state
	testq	%rcx, %rcx
	jne	.L174
	movq	$0, -80(%rbp)
	jmp	.L204
	.p2align 4,,10
	.p2align 3
.L194:
	addq	$16, %rbx
	cmpq	%r14, %rbx
	jne	.L185
	jmp	.L195
	.p2align 4,,10
	.p2align 3
.L206:
	movq	$16, -88(%rbp)
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L208:
	movq	-80(%rbp), %r13
	jmp	.L175
.L236:
	leaq	.LC0(%rip), %rdi
.LEHB10:
	call	_ZSt20__throw_length_errorPKc@PLT
.LEHE10:
.L238:
	call	__stack_chk_fail@PLT
.L174:
	movq	-88(%rbp), %rax
	cmpq	%rdx, %rax
	cmovbe	%rax, %rdx
	salq	$4, %rdx
	movq	%rdx, -88(%rbp)
	jmp	.L173
.L213:
	endbr64
	movq	%rax, %rdi
	jmp	.L179
.L215:
	endbr64
	movq	%rax, %rdi
	jmp	.L188
.L216:
	endbr64
	movq	%rax, %rdi
	jmp	.L197
.L179:
	call	__cxa_begin_catch@PLT
	movq	-80(%rbp), %rbx
.L182:
	cmpq	%r13, %rbx
	jne	.L239
.LEHB11:
	call	__cxa_rethrow@PLT
.LEHE11:
.L188:
	call	__cxa_begin_catch@PLT
	movq	%r13, %rbx
.L191:
	cmpq	%r12, %rbx
	jne	.L240
.LEHB12:
	call	__cxa_rethrow@PLT
.LEHE12:
.L197:
	call	__cxa_begin_catch@PLT
	cmpq	$0, -80(%rbp)
	je	.L184
.L198:
	movq	-80(%rbp), %rdi
	call	_ZdlPv@PLT
.L202:
.LEHB13:
	call	__cxa_rethrow@PLT
.LEHE13:
.L239:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L181
	movq	(%rdi), %rax
	call	*24(%rax)
.L181:
	addq	$16, %rbx
	jmp	.L182
.L212:
	endbr64
	movq	%rax, %r12
	jmp	.L183
.L240:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L190
	movq	(%rdi), %rax
	call	*24(%rax)
.L190:
	addq	$16, %rbx
	jmp	.L191
.L183:
	call	__cxa_end_catch@PLT
	movq	%r12, %rdi
	call	__cxa_begin_catch@PLT
.L184:
	movq	-104(%rbp), %rax
	movq	8(%rax), %rdi
	testq	%rdi, %rdi
	je	.L200
	movq	(%rdi), %rax
	call	*24(%rax)
.L200:
	cmpq	$0, -80(%rbp)
	jne	.L198
	jmp	.L202
.L211:
	endbr64
	movq	%rax, %r12
	jmp	.L203
.L214:
	endbr64
	movq	%rax, %r12
	jmp	.L192
.L203:
	call	__cxa_end_catch@PLT
	movq	%r12, %rdi
.LEHB14:
	call	_Unwind_Resume@PLT
.LEHE14:
.L192:
	call	__cxa_end_catch@PLT
	movq	%r12, %rdi
	call	__cxa_begin_catch@PLT
	movq	-80(%rbp), %rbx
.L193:
	cmpq	%rbx, %r13
	je	.L200
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L201
	movq	(%rdi), %rax
	call	*24(%rax)
.L201:
	addq	$16, %rbx
	jmp	.L193
	.cfi_endproc
.LFE9541:
	.section	.gcc_except_table._ZNSt6vectorIN2v88internal6torque11InstructionESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,"aG",@progbits,_ZNSt6vectorIN2v88internal6torque11InstructionESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,comdat
	.align 4
.LLSDA9541:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATT9541-.LLSDATTD9541
.LLSDATTD9541:
	.byte	0x1
	.uleb128 .LLSDACSE9541-.LLSDACSB9541
.LLSDACSB9541:
	.uleb128 .LEHB6-.LFB9541
	.uleb128 .LEHE6-.LEHB6
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB7-.LFB9541
	.uleb128 .LEHE7-.LEHB7
	.uleb128 .L216-.LFB9541
	.uleb128 0x1
	.uleb128 .LEHB8-.LFB9541
	.uleb128 .LEHE8-.LEHB8
	.uleb128 .L213-.LFB9541
	.uleb128 0x1
	.uleb128 .LEHB9-.LFB9541
	.uleb128 .LEHE9-.LEHB9
	.uleb128 .L215-.LFB9541
	.uleb128 0x1
	.uleb128 .LEHB10-.LFB9541
	.uleb128 .LEHE10-.LEHB10
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB11-.LFB9541
	.uleb128 .LEHE11-.LEHB11
	.uleb128 .L212-.LFB9541
	.uleb128 0x3
	.uleb128 .LEHB12-.LFB9541
	.uleb128 .LEHE12-.LEHB12
	.uleb128 .L214-.LFB9541
	.uleb128 0x3
	.uleb128 .LEHB13-.LFB9541
	.uleb128 .LEHE13-.LEHB13
	.uleb128 .L211-.LFB9541
	.uleb128 0
	.uleb128 .LEHB14-.LFB9541
	.uleb128 .LEHE14-.LEHB14
	.uleb128 0
	.uleb128 0
.LLSDACSE9541:
	.byte	0x1
	.byte	0
	.byte	0
	.byte	0x7d
	.align 4
	.long	0

.LLSDATT9541:
	.section	.text._ZNSt6vectorIN2v88internal6torque11InstructionESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal6torque11InstructionESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,comdat
	.size	_ZNSt6vectorIN2v88internal6torque11InstructionESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, .-_ZNSt6vectorIN2v88internal6torque11InstructionESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.section	.text.unlikely._ZN2v88internal6torque12CfgAssembler11DeleteRangeENS1_10StackRangeE,"ax",@progbits
	.align 2
.LCOLDB1:
	.section	.text._ZN2v88internal6torque12CfgAssembler11DeleteRangeENS1_10StackRangeE,"ax",@progbits
.LHOTB1:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque12CfgAssembler11DeleteRangeENS1_10StackRangeE
	.type	_ZN2v88internal6torque12CfgAssembler11DeleteRangeENS1_10StackRangeE, @function
_ZN2v88internal6torque12CfgAssembler11DeleteRangeENS1_10StackRangeE:
.LFB6752:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6752
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$80, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpq	%rsi, %rdx
	je	.L241
	movq	%rdi, %r13
	movq	%rsi, %rbx
	movq	%rdx, %r14
.LEHB15:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
	movl	$48, %edi
	movq	(%rax), %rax
	movdqu	(%rax), %xmm1
	movl	16(%rax), %eax
	movups	%xmm1, -88(%rbp)
	movl	%eax, -72(%rbp)
	call	_Znwm@PLT
.LEHE15:
	movdqu	-88(%rbp), %xmm2
	leaq	24(%r13), %rdx
	movq	%r13, %rsi
	movq	%rax, %r12
	movq	%rbx, %xmm0
	movq	%r14, %xmm3
	movups	%xmm2, 8(%rax)
	movl	-72(%rbp), %eax
	punpcklqdq	%xmm3, %xmm0
	movq	%r12, %rdi
	movups	%xmm0, 32(%r12)
	movl	%eax, 24(%r12)
	leaq	16+_ZTVN2v88internal6torque22DeleteRangeInstructionE(%rip), %rax
	movq	%rax, (%r12)
.LEHB16:
	call	_ZNK2v88internal6torque22DeleteRangeInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE@PLT
	movl	_ZN2v88internal6torque22DeleteRangeInstruction5kKindE(%rip), %eax
	movq	120(%r13), %rbx
	leaq	-104(%rbp), %rdi
	movq	%r12, %rsi
	movl	%eax, -112(%rbp)
	movq	(%r12), %rax
	call	*(%rax)
.LEHE16:
	movq	16(%rbx), %rsi
	cmpq	24(%rbx), %rsi
	je	.L243
	movl	-112(%rbp), %eax
	leaq	8(%rsi), %rdi
	movl	%eax, (%rsi)
	movq	-104(%rbp), %r8
	movq	(%r8), %rax
	movq	%r8, %rsi
.LEHB17:
	call	*(%rax)
	addq	$16, 16(%rbx)
.L244:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L245
	movq	(%rdi), %rax
	call	*24(%rax)
.L245:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
.L241:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L259
	addq	$80, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L243:
	.cfi_restore_state
	leaq	-112(%rbp), %rdx
	leaq	8(%rbx), %rdi
	call	_ZNSt6vectorIN2v88internal6torque11InstructionESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
.LEHE17:
	jmp	.L244
.L259:
	call	__stack_chk_fail@PLT
.L251:
	endbr64
	movq	%rax, %r13
	jmp	.L246
.L250:
	endbr64
	movq	%rax, %r13
	jmp	.L248
	.section	.gcc_except_table._ZN2v88internal6torque12CfgAssembler11DeleteRangeENS1_10StackRangeE,"a",@progbits
.LLSDA6752:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6752-.LLSDACSB6752
.LLSDACSB6752:
	.uleb128 .LEHB15-.LFB6752
	.uleb128 .LEHE15-.LEHB15
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB16-.LFB6752
	.uleb128 .LEHE16-.LEHB16
	.uleb128 .L250-.LFB6752
	.uleb128 0
	.uleb128 .LEHB17-.LFB6752
	.uleb128 .LEHE17-.LEHB17
	.uleb128 .L251-.LFB6752
	.uleb128 0
.LLSDACSE6752:
	.section	.text._ZN2v88internal6torque12CfgAssembler11DeleteRangeENS1_10StackRangeE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque12CfgAssembler11DeleteRangeENS1_10StackRangeE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6752
	.type	_ZN2v88internal6torque12CfgAssembler11DeleteRangeENS1_10StackRangeE.cold, @function
_ZN2v88internal6torque12CfgAssembler11DeleteRangeENS1_10StackRangeE.cold:
.LFSB6752:
.L246:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L248
	movq	(%rdi), %rax
	call	*24(%rax)
.L248:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	movq	%r13, %rdi
.LEHB18:
	call	_Unwind_Resume@PLT
.LEHE18:
	.cfi_endproc
.LFE6752:
	.section	.gcc_except_table._ZN2v88internal6torque12CfgAssembler11DeleteRangeENS1_10StackRangeE
.LLSDAC6752:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6752-.LLSDACSBC6752
.LLSDACSBC6752:
	.uleb128 .LEHB18-.LCOLDB1
	.uleb128 .LEHE18-.LEHB18
	.uleb128 0
	.uleb128 0
.LLSDACSEC6752:
	.section	.text.unlikely._ZN2v88internal6torque12CfgAssembler11DeleteRangeENS1_10StackRangeE
	.section	.text._ZN2v88internal6torque12CfgAssembler11DeleteRangeENS1_10StackRangeE
	.size	_ZN2v88internal6torque12CfgAssembler11DeleteRangeENS1_10StackRangeE, .-_ZN2v88internal6torque12CfgAssembler11DeleteRangeENS1_10StackRangeE
	.section	.text.unlikely._ZN2v88internal6torque12CfgAssembler11DeleteRangeENS1_10StackRangeE
	.size	_ZN2v88internal6torque12CfgAssembler11DeleteRangeENS1_10StackRangeE.cold, .-_ZN2v88internal6torque12CfgAssembler11DeleteRangeENS1_10StackRangeE.cold
.LCOLDE1:
	.section	.text._ZN2v88internal6torque12CfgAssembler11DeleteRangeENS1_10StackRangeE
.LHOTE1:
	.section	.text.unlikely._ZN2v88internal6torque12CfgAssembler4PokeENS1_10StackRangeES3_NS_4base8OptionalIPKNS1_4TypeEEE,"ax",@progbits
	.align 2
.LCOLDB2:
	.section	.text._ZN2v88internal6torque12CfgAssembler4PokeENS1_10StackRangeES3_NS_4base8OptionalIPKNS1_4TypeEEE,"ax",@progbits
.LHOTB2:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque12CfgAssembler4PokeENS1_10StackRangeES3_NS_4base8OptionalIPKNS1_4TypeEEE
	.type	_ZN2v88internal6torque12CfgAssembler4PokeENS1_10StackRangeES3_NS_4base8OptionalIPKNS1_4TypeEEE, @function
_ZN2v88internal6torque12CfgAssembler4PokeENS1_10StackRangeES3_NS_4base8OptionalIPKNS1_4TypeEEE:
.LFB6758:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6758
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$136, %rsp
	movq	%rsi, -152(%rbp)
	movq	16(%rbp), %r12
	movq	24(%rbp), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movb	%r12b, -137(%rbp)
	testb	%r12b, %r12b
	jne	.L261
	movq	$0, -176(%rbp)
	subq	$1, %rbx
	subq	%rcx, %rbx
	js	.L260
.L262:
	cmpb	$1, %r12b
	sbbl	%eax, %eax
	addl	$1, %eax
	movb	%al, -138(%rbp)
	leaq	24(%r13), %rax
	movq	%rax, -160(%rbp)
	leaq	-120(%rbp), %rax
	movq	%rax, -168(%rbp)
	jmp	.L278
	.p2align 4,,10
	.p2align 3
.L302:
	movq	-176(%rbp), %rax
	movq	(%rax,%rbx,8), %r14
.L266:
.LEHB19:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rax
	movl	$56, %edi
	movdqu	(%rax), %xmm0
	movups	%xmm0, -104(%rbp)
	movl	16(%rax), %eax
	movl	%eax, -88(%rbp)
	call	_Znwm@PLT
.LEHE19:
	movdqu	-104(%rbp), %xmm1
	movq	%rax, %r12
	movq	-160(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movups	%xmm1, 8(%rax)
	movl	-88(%rbp), %eax
	movq	%r14, 48(%r12)
	movl	%eax, 24(%r12)
	leaq	16+_ZTVN2v88internal6torque15PokeInstructionE(%rip), %rax
	movq	%rax, (%r12)
	movq	-136(%rbp), %rax
	movq	%rax, 32(%r12)
	movzbl	-138(%rbp), %eax
	movb	%al, 40(%r12)
.LEHB20:
	call	_ZNK2v88internal6torque15PokeInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE@PLT
	movl	_ZN2v88internal6torque15PokeInstruction5kKindE(%rip), %eax
	movq	120(%r13), %r14
	movq	%r12, %rsi
	movq	-168(%rbp), %rdi
	movl	%eax, -128(%rbp)
	movq	(%r12), %rax
	call	*(%rax)
.LEHE20:
	movq	16(%r14), %rsi
	cmpq	24(%r14), %rsi
	je	.L267
	movl	-128(%rbp), %eax
	leaq	8(%rsi), %rdi
	movl	%eax, (%rsi)
	movq	-120(%rbp), %r8
	movq	(%r8), %rax
	movq	%r8, %rsi
.LEHB21:
	call	*(%rax)
	addq	$16, 16(%r14)
.L268:
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L269
	movq	(%rdi), %rax
	call	*24(%rax)
.L269:
	movq	(%r12), %rax
	subq	$1, %rbx
	movq	%r12, %rdi
	call	*24(%rax)
	cmpq	$-1, %rbx
	je	.L264
.L278:
	movq	-152(%rbp), %rax
	addq	%rbx, %rax
	cmpb	$0, -137(%rbp)
	movq	%rax, -136(%rbp)
	jne	.L302
	xorb	%r15b, %r15b
	movq	%r15, %r14
	jmp	.L266
	.p2align 4,,10
	.p2align 3
.L267:
	leaq	-128(%rbp), %rdx
	leaq	8(%r14), %rdi
	call	_ZNSt6vectorIN2v88internal6torque11InstructionESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
.LEHE21:
	jmp	.L268
	.p2align 4,,10
	.p2align 3
.L261:
	leaq	-112(%rbp), %rdi
.LEHB22:
	call	_ZN2v88internal6torque9LowerTypeEPKNS1_4TypeE@PLT
.LEHE22:
	movq	-112(%rbp), %rax
	subq	$1, %rbx
	movq	%rax, -176(%rbp)
	subq	%r14, %rbx
	jns	.L262
	.p2align 4,,10
	.p2align 3
.L264:
	movq	-176(%rbp), %rax
	testq	%rax, %rax
	je	.L260
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L260:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L303
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L303:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
.L283:
	endbr64
	movq	%rax, %rbx
	jmp	.L271
.L282:
	endbr64
	movq	%rax, %rbx
	jmp	.L273
.L281:
	endbr64
	movq	%rax, %r12
	jmp	.L275
.L284:
	endbr64
	movq	%rax, %r12
	jmp	.L277
	.section	.gcc_except_table._ZN2v88internal6torque12CfgAssembler4PokeENS1_10StackRangeES3_NS_4base8OptionalIPKNS1_4TypeEEE,"a",@progbits
.LLSDA6758:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6758-.LLSDACSB6758
.LLSDACSB6758:
	.uleb128 .LEHB19-.LFB6758
	.uleb128 .LEHE19-.LEHB19
	.uleb128 .L281-.LFB6758
	.uleb128 0
	.uleb128 .LEHB20-.LFB6758
	.uleb128 .LEHE20-.LEHB20
	.uleb128 .L282-.LFB6758
	.uleb128 0
	.uleb128 .LEHB21-.LFB6758
	.uleb128 .LEHE21-.LEHB21
	.uleb128 .L283-.LFB6758
	.uleb128 0
	.uleb128 .LEHB22-.LFB6758
	.uleb128 .LEHE22-.LEHB22
	.uleb128 .L284-.LFB6758
	.uleb128 0
.LLSDACSE6758:
	.section	.text._ZN2v88internal6torque12CfgAssembler4PokeENS1_10StackRangeES3_NS_4base8OptionalIPKNS1_4TypeEEE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque12CfgAssembler4PokeENS1_10StackRangeES3_NS_4base8OptionalIPKNS1_4TypeEEE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6758
	.type	_ZN2v88internal6torque12CfgAssembler4PokeENS1_10StackRangeES3_NS_4base8OptionalIPKNS1_4TypeEEE.cold, @function
_ZN2v88internal6torque12CfgAssembler4PokeENS1_10StackRangeES3_NS_4base8OptionalIPKNS1_4TypeEEE.cold:
.LFSB6758:
.L271:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L273
	movq	(%rdi), %rax
	call	*24(%rax)
.L273:
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	%rbx, %r12
	call	*24(%rax)
.L275:
	cmpq	$0, -176(%rbp)
	je	.L277
	movq	-176(%rbp), %rdi
	call	_ZdlPv@PLT
.L277:
	movq	%r12, %rdi
.LEHB23:
	call	_Unwind_Resume@PLT
.LEHE23:
	.cfi_endproc
.LFE6758:
	.section	.gcc_except_table._ZN2v88internal6torque12CfgAssembler4PokeENS1_10StackRangeES3_NS_4base8OptionalIPKNS1_4TypeEEE
.LLSDAC6758:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6758-.LLSDACSBC6758
.LLSDACSBC6758:
	.uleb128 .LEHB23-.LCOLDB2
	.uleb128 .LEHE23-.LEHB23
	.uleb128 0
	.uleb128 0
.LLSDACSEC6758:
	.section	.text.unlikely._ZN2v88internal6torque12CfgAssembler4PokeENS1_10StackRangeES3_NS_4base8OptionalIPKNS1_4TypeEEE
	.section	.text._ZN2v88internal6torque12CfgAssembler4PokeENS1_10StackRangeES3_NS_4base8OptionalIPKNS1_4TypeEEE
	.size	_ZN2v88internal6torque12CfgAssembler4PokeENS1_10StackRangeES3_NS_4base8OptionalIPKNS1_4TypeEEE, .-_ZN2v88internal6torque12CfgAssembler4PokeENS1_10StackRangeES3_NS_4base8OptionalIPKNS1_4TypeEEE
	.section	.text.unlikely._ZN2v88internal6torque12CfgAssembler4PokeENS1_10StackRangeES3_NS_4base8OptionalIPKNS1_4TypeEEE
	.size	_ZN2v88internal6torque12CfgAssembler4PokeENS1_10StackRangeES3_NS_4base8OptionalIPKNS1_4TypeEEE.cold, .-_ZN2v88internal6torque12CfgAssembler4PokeENS1_10StackRangeES3_NS_4base8OptionalIPKNS1_4TypeEEE.cold
.LCOLDE2:
	.section	.text._ZN2v88internal6torque12CfgAssembler4PokeENS1_10StackRangeES3_NS_4base8OptionalIPKNS1_4TypeEEE
.LHOTE2:
	.section	.text.unlikely._ZN2v88internal6torque12CfgAssembler4PeekENS1_10StackRangeENS_4base8OptionalIPKNS1_4TypeEEE,"ax",@progbits
	.align 2
.LCOLDB3:
	.section	.text._ZN2v88internal6torque12CfgAssembler4PeekENS1_10StackRangeENS_4base8OptionalIPKNS1_4TypeEEE,"ax",@progbits
.LHOTB3:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque12CfgAssembler4PeekENS1_10StackRangeENS_4base8OptionalIPKNS1_4TypeEEE
	.type	_ZN2v88internal6torque12CfgAssembler4PeekENS1_10StackRangeENS_4base8OptionalIPKNS1_4TypeEEE, @function
_ZN2v88internal6torque12CfgAssembler4PeekENS1_10StackRangeENS_4base8OptionalIPKNS1_4TypeEEE:
.LFB6754:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6754
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%ecx, %ebx
	subq	$152, %rsp
	movq	%rsi, -184(%rbp)
	movq	%rdx, -136(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movb	%cl, -137(%rbp)
	movq	$0, -176(%rbp)
	testb	%cl, %cl
	jne	.L341
.L305:
	movq	-184(%rbp), %r13
	movq	%r13, %rax
	cmpq	%r13, -136(%rbp)
	je	.L306
	cmpb	$1, %bl
	sbbl	%ecx, %ecx
	negq	%rax
	addl	$1, %ecx
	movb	%cl, -138(%rbp)
	movq	-176(%rbp), %rcx
	leaq	(%rcx,%rax,8), %rax
	movq	%rax, -168(%rbp)
	leaq	24(%r14), %rax
	movq	%rax, -152(%rbp)
	leaq	-120(%rbp), %rax
	movq	%rax, -160(%rbp)
	jmp	.L320
	.p2align 4,,10
	.p2align 3
.L342:
	movq	-168(%rbp), %rax
	movq	(%rax,%r13,8), %rbx
.L308:
.LEHB24:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rax
	movl	$56, %edi
	movdqu	(%rax), %xmm0
	movups	%xmm0, -104(%rbp)
	movl	16(%rax), %eax
	movl	%eax, -88(%rbp)
	call	_Znwm@PLT
.LEHE24:
	movdqu	-104(%rbp), %xmm1
	movq	%rax, %r12
	movq	-152(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movups	%xmm1, 8(%rax)
	movl	-88(%rbp), %eax
	movq	%r13, 32(%r12)
	movl	%eax, 24(%r12)
	leaq	16+_ZTVN2v88internal6torque15PeekInstructionE(%rip), %rax
	movq	%rax, (%r12)
	movzbl	-138(%rbp), %eax
	movq	%rbx, 48(%r12)
	movb	%al, 40(%r12)
.LEHB25:
	call	_ZNK2v88internal6torque15PeekInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE@PLT
	movl	_ZN2v88internal6torque15PeekInstruction5kKindE(%rip), %eax
	movq	120(%r14), %rbx
	movq	%r12, %rsi
	movq	-160(%rbp), %rdi
	movl	%eax, -128(%rbp)
	movq	(%r12), %rax
	call	*(%rax)
.LEHE25:
	movq	16(%rbx), %rsi
	cmpq	24(%rbx), %rsi
	je	.L309
	movl	-128(%rbp), %eax
	leaq	8(%rsi), %rdi
	movl	%eax, (%rsi)
	movq	-120(%rbp), %r8
	movq	(%r8), %rax
	movq	%r8, %rsi
.LEHB26:
	call	*(%rax)
	addq	$16, 16(%rbx)
.L310:
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L311
	movq	(%rdi), %rax
	call	*24(%rax)
.L311:
	movq	(%r12), %rax
	movq	%r12, %rdi
	addq	$1, %r13
	call	*24(%rax)
	cmpq	%r13, -136(%rbp)
	je	.L306
.L320:
	cmpb	$0, -137(%rbp)
	jne	.L342
	xorb	%r15b, %r15b
	movq	%r15, %rbx
	jmp	.L308
	.p2align 4,,10
	.p2align 3
.L309:
	leaq	-128(%rbp), %rdx
	leaq	8(%rbx), %rdi
	call	_ZNSt6vectorIN2v88internal6torque11InstructionESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
.LEHE26:
	jmp	.L310
	.p2align 4,,10
	.p2align 3
.L306:
	movq	8(%r14), %r12
	movq	-176(%rbp), %rax
	subq	(%r14), %r12
	movq	-184(%rbp), %rsi
	sarq	$3, %r12
	subq	-136(%rbp), %rsi
	leaq	(%rsi,%r12), %r13
	testq	%rax, %rax
	je	.L316
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L316:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L343
	addq	$152, %rsp
	movq	%r13, %rax
	movq	%r12, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L341:
	.cfi_restore_state
	leaq	-112(%rbp), %rdi
	movq	%r8, %rsi
.LEHB27:
	call	_ZN2v88internal6torque9LowerTypeEPKNS1_4TypeE@PLT
.LEHE27:
	movq	-112(%rbp), %rax
	movq	%rax, -176(%rbp)
	jmp	.L305
.L343:
	call	__stack_chk_fail@PLT
.L325:
	endbr64
	movq	%rax, %rbx
	jmp	.L313
.L324:
	endbr64
	movq	%rax, %rbx
	jmp	.L315
.L323:
	endbr64
	movq	%rax, %r12
	jmp	.L317
.L326:
	endbr64
	movq	%rax, %r12
	jmp	.L319
	.section	.gcc_except_table._ZN2v88internal6torque12CfgAssembler4PeekENS1_10StackRangeENS_4base8OptionalIPKNS1_4TypeEEE,"a",@progbits
.LLSDA6754:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6754-.LLSDACSB6754
.LLSDACSB6754:
	.uleb128 .LEHB24-.LFB6754
	.uleb128 .LEHE24-.LEHB24
	.uleb128 .L323-.LFB6754
	.uleb128 0
	.uleb128 .LEHB25-.LFB6754
	.uleb128 .LEHE25-.LEHB25
	.uleb128 .L324-.LFB6754
	.uleb128 0
	.uleb128 .LEHB26-.LFB6754
	.uleb128 .LEHE26-.LEHB26
	.uleb128 .L325-.LFB6754
	.uleb128 0
	.uleb128 .LEHB27-.LFB6754
	.uleb128 .LEHE27-.LEHB27
	.uleb128 .L326-.LFB6754
	.uleb128 0
.LLSDACSE6754:
	.section	.text._ZN2v88internal6torque12CfgAssembler4PeekENS1_10StackRangeENS_4base8OptionalIPKNS1_4TypeEEE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque12CfgAssembler4PeekENS1_10StackRangeENS_4base8OptionalIPKNS1_4TypeEEE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6754
	.type	_ZN2v88internal6torque12CfgAssembler4PeekENS1_10StackRangeENS_4base8OptionalIPKNS1_4TypeEEE.cold, @function
_ZN2v88internal6torque12CfgAssembler4PeekENS1_10StackRangeENS_4base8OptionalIPKNS1_4TypeEEE.cold:
.LFSB6754:
.L313:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L315
	movq	(%rdi), %rax
	call	*24(%rax)
.L315:
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	%rbx, %r12
	call	*24(%rax)
.L317:
	cmpq	$0, -176(%rbp)
	je	.L319
	movq	-176(%rbp), %rdi
	call	_ZdlPv@PLT
.L319:
	movq	%r12, %rdi
.LEHB28:
	call	_Unwind_Resume@PLT
.LEHE28:
	.cfi_endproc
.LFE6754:
	.section	.gcc_except_table._ZN2v88internal6torque12CfgAssembler4PeekENS1_10StackRangeENS_4base8OptionalIPKNS1_4TypeEEE
.LLSDAC6754:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6754-.LLSDACSBC6754
.LLSDACSBC6754:
	.uleb128 .LEHB28-.LCOLDB3
	.uleb128 .LEHE28-.LEHB28
	.uleb128 0
	.uleb128 0
.LLSDACSEC6754:
	.section	.text.unlikely._ZN2v88internal6torque12CfgAssembler4PeekENS1_10StackRangeENS_4base8OptionalIPKNS1_4TypeEEE
	.section	.text._ZN2v88internal6torque12CfgAssembler4PeekENS1_10StackRangeENS_4base8OptionalIPKNS1_4TypeEEE
	.size	_ZN2v88internal6torque12CfgAssembler4PeekENS1_10StackRangeENS_4base8OptionalIPKNS1_4TypeEEE, .-_ZN2v88internal6torque12CfgAssembler4PeekENS1_10StackRangeENS_4base8OptionalIPKNS1_4TypeEEE
	.section	.text.unlikely._ZN2v88internal6torque12CfgAssembler4PeekENS1_10StackRangeENS_4base8OptionalIPKNS1_4TypeEEE
	.size	_ZN2v88internal6torque12CfgAssembler4PeekENS1_10StackRangeENS_4base8OptionalIPKNS1_4TypeEEE.cold, .-_ZN2v88internal6torque12CfgAssembler4PeekENS1_10StackRangeENS_4base8OptionalIPKNS1_4TypeEEE.cold
.LCOLDE3:
	.section	.text._ZN2v88internal6torque12CfgAssembler4PeekENS1_10StackRangeENS_4base8OptionalIPKNS1_4TypeEEE
.LHOTE3:
	.section	.text.unlikely._ZN2v88internal6torque12CfgAssembler6BranchEPNS1_5BlockES4_,"ax",@progbits
	.align 2
.LCOLDB4:
	.section	.text._ZN2v88internal6torque12CfgAssembler6BranchEPNS1_5BlockES4_,"ax",@progbits
.LHOTB4:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque12CfgAssembler6BranchEPNS1_5BlockES4_
	.type	_ZN2v88internal6torque12CfgAssembler6BranchEPNS1_5BlockES4_, @function
_ZN2v88internal6torque12CfgAssembler6BranchEPNS1_5BlockES4_:
.LFB6748:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6748
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$96, %rsp
	movq	%rsi, -104(%rbp)
	movq	%rdx, -112(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
.LEHB29:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
	movl	$48, %edi
	movq	(%rax), %rax
	movdqu	(%rax), %xmm1
	movl	16(%rax), %eax
	movups	%xmm1, -72(%rbp)
	movl	%eax, -56(%rbp)
	call	_Znwm@PLT
.LEHE29:
	movdqu	-72(%rbp), %xmm2
	leaq	24(%rbx), %rdx
	movq	%rbx, %rsi
	movq	%rax, %r12
	movq	-104(%rbp), %xmm0
	movups	%xmm2, 8(%rax)
	movl	-56(%rbp), %eax
	movq	%r12, %rdi
	movhps	-112(%rbp), %xmm0
	movl	%eax, 24(%r12)
	leaq	16+_ZTVN2v88internal6torque17BranchInstructionE(%rip), %rax
	movq	%rax, (%r12)
	movups	%xmm0, 32(%r12)
.LEHB30:
	call	_ZNK2v88internal6torque17BranchInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE@PLT
	movl	_ZN2v88internal6torque17BranchInstruction5kKindE(%rip), %eax
	movq	120(%rbx), %rbx
	leaq	-88(%rbp), %rdi
	movq	%r12, %rsi
	movl	%eax, -96(%rbp)
	movq	(%r12), %rax
	call	*(%rax)
.LEHE30:
	movq	16(%rbx), %rsi
	cmpq	24(%rbx), %rsi
	je	.L345
	movl	-96(%rbp), %eax
	leaq	8(%rsi), %rdi
	movl	%eax, (%rsi)
	movq	-88(%rbp), %r8
	movq	(%r8), %rax
	movq	%r8, %rsi
.LEHB31:
	call	*(%rax)
	addq	$16, 16(%rbx)
.L346:
	movq	-88(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L347
	movq	(%rdi), %rax
	call	*24(%rax)
.L347:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L361
	addq	$96, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L345:
	.cfi_restore_state
	leaq	-96(%rbp), %rdx
	leaq	8(%rbx), %rdi
	call	_ZNSt6vectorIN2v88internal6torque11InstructionESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
.LEHE31:
	jmp	.L346
.L361:
	call	__stack_chk_fail@PLT
.L352:
	endbr64
	movq	%rax, %rbx
	jmp	.L350
.L353:
	endbr64
	movq	%rax, %rbx
	jmp	.L348
	.section	.gcc_except_table._ZN2v88internal6torque12CfgAssembler6BranchEPNS1_5BlockES4_,"a",@progbits
.LLSDA6748:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6748-.LLSDACSB6748
.LLSDACSB6748:
	.uleb128 .LEHB29-.LFB6748
	.uleb128 .LEHE29-.LEHB29
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB30-.LFB6748
	.uleb128 .LEHE30-.LEHB30
	.uleb128 .L352-.LFB6748
	.uleb128 0
	.uleb128 .LEHB31-.LFB6748
	.uleb128 .LEHE31-.LEHB31
	.uleb128 .L353-.LFB6748
	.uleb128 0
.LLSDACSE6748:
	.section	.text._ZN2v88internal6torque12CfgAssembler6BranchEPNS1_5BlockES4_
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque12CfgAssembler6BranchEPNS1_5BlockES4_
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6748
	.type	_ZN2v88internal6torque12CfgAssembler6BranchEPNS1_5BlockES4_.cold, @function
_ZN2v88internal6torque12CfgAssembler6BranchEPNS1_5BlockES4_.cold:
.LFSB6748:
.L348:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	movq	-88(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L350
	movq	(%rdi), %rax
	call	*24(%rax)
.L350:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	movq	%rbx, %rdi
.LEHB32:
	call	_Unwind_Resume@PLT
.LEHE32:
	.cfi_endproc
.LFE6748:
	.section	.gcc_except_table._ZN2v88internal6torque12CfgAssembler6BranchEPNS1_5BlockES4_
.LLSDAC6748:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6748-.LLSDACSBC6748
.LLSDACSBC6748:
	.uleb128 .LEHB32-.LCOLDB4
	.uleb128 .LEHE32-.LEHB32
	.uleb128 0
	.uleb128 0
.LLSDACSEC6748:
	.section	.text.unlikely._ZN2v88internal6torque12CfgAssembler6BranchEPNS1_5BlockES4_
	.section	.text._ZN2v88internal6torque12CfgAssembler6BranchEPNS1_5BlockES4_
	.size	_ZN2v88internal6torque12CfgAssembler6BranchEPNS1_5BlockES4_, .-_ZN2v88internal6torque12CfgAssembler6BranchEPNS1_5BlockES4_
	.section	.text.unlikely._ZN2v88internal6torque12CfgAssembler6BranchEPNS1_5BlockES4_
	.size	_ZN2v88internal6torque12CfgAssembler6BranchEPNS1_5BlockES4_.cold, .-_ZN2v88internal6torque12CfgAssembler6BranchEPNS1_5BlockES4_.cold
.LCOLDE4:
	.section	.text._ZN2v88internal6torque12CfgAssembler6BranchEPNS1_5BlockES4_
.LHOTE4:
	.section	.text.unlikely._ZN2v88internal6torque12CfgAssembler6DropToENS1_12BottomOffsetE,"ax",@progbits
	.align 2
.LCOLDB5:
	.section	.text._ZN2v88internal6torque12CfgAssembler6DropToENS1_12BottomOffsetE,"ax",@progbits
.LHOTB5:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque12CfgAssembler6DropToENS1_12BottomOffsetE
	.type	_ZN2v88internal6torque12CfgAssembler6DropToENS1_12BottomOffsetE, @function
_ZN2v88internal6torque12CfgAssembler6DropToENS1_12BottomOffsetE:
.LFB6753:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6753
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$80, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	8(%rdi), %r13
	subq	(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	sarq	$3, %r13
	cmpq	%r13, %rsi
	je	.L362
	movq	%rdi, %rbx
	movq	%rsi, %r14
.LEHB33:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
	movl	$48, %edi
	movq	(%rax), %rax
	movdqu	(%rax), %xmm1
	movl	16(%rax), %eax
	movups	%xmm1, -88(%rbp)
	movl	%eax, -72(%rbp)
	call	_Znwm@PLT
.LEHE33:
	movdqu	-88(%rbp), %xmm2
	leaq	24(%rbx), %rdx
	movq	%rbx, %rsi
	movq	%rax, %r12
	movq	%r14, %xmm0
	movq	%r13, %xmm3
	movups	%xmm2, 8(%rax)
	movl	-72(%rbp), %eax
	punpcklqdq	%xmm3, %xmm0
	movq	%r12, %rdi
	movups	%xmm0, 32(%r12)
	movl	%eax, 24(%r12)
	leaq	16+_ZTVN2v88internal6torque22DeleteRangeInstructionE(%rip), %rax
	movq	%rax, (%r12)
.LEHB34:
	call	_ZNK2v88internal6torque22DeleteRangeInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE@PLT
	movl	_ZN2v88internal6torque22DeleteRangeInstruction5kKindE(%rip), %eax
	movq	120(%rbx), %rbx
	leaq	-104(%rbp), %rdi
	movq	%r12, %rsi
	movl	%eax, -112(%rbp)
	movq	(%r12), %rax
	call	*(%rax)
.LEHE34:
	movq	16(%rbx), %rsi
	cmpq	24(%rbx), %rsi
	je	.L364
	movl	-112(%rbp), %eax
	leaq	8(%rsi), %rdi
	movl	%eax, (%rsi)
	movq	-104(%rbp), %r8
	movq	(%r8), %rax
	movq	%r8, %rsi
.LEHB35:
	call	*(%rax)
	addq	$16, 16(%rbx)
.L365:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L366
	movq	(%rdi), %rax
	call	*24(%rax)
.L366:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
.L362:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L380
	addq	$80, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L364:
	.cfi_restore_state
	leaq	-112(%rbp), %rdx
	leaq	8(%rbx), %rdi
	call	_ZNSt6vectorIN2v88internal6torque11InstructionESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
.LEHE35:
	jmp	.L365
.L380:
	call	__stack_chk_fail@PLT
.L372:
	endbr64
	movq	%rax, %r13
	jmp	.L367
.L371:
	endbr64
	movq	%rax, %r13
	jmp	.L369
	.section	.gcc_except_table._ZN2v88internal6torque12CfgAssembler6DropToENS1_12BottomOffsetE,"a",@progbits
.LLSDA6753:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6753-.LLSDACSB6753
.LLSDACSB6753:
	.uleb128 .LEHB33-.LFB6753
	.uleb128 .LEHE33-.LEHB33
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB34-.LFB6753
	.uleb128 .LEHE34-.LEHB34
	.uleb128 .L371-.LFB6753
	.uleb128 0
	.uleb128 .LEHB35-.LFB6753
	.uleb128 .LEHE35-.LEHB35
	.uleb128 .L372-.LFB6753
	.uleb128 0
.LLSDACSE6753:
	.section	.text._ZN2v88internal6torque12CfgAssembler6DropToENS1_12BottomOffsetE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque12CfgAssembler6DropToENS1_12BottomOffsetE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6753
	.type	_ZN2v88internal6torque12CfgAssembler6DropToENS1_12BottomOffsetE.cold, @function
_ZN2v88internal6torque12CfgAssembler6DropToENS1_12BottomOffsetE.cold:
.LFSB6753:
.L367:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L369
	movq	(%rdi), %rax
	call	*24(%rax)
.L369:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	movq	%r13, %rdi
.LEHB36:
	call	_Unwind_Resume@PLT
.LEHE36:
	.cfi_endproc
.LFE6753:
	.section	.gcc_except_table._ZN2v88internal6torque12CfgAssembler6DropToENS1_12BottomOffsetE
.LLSDAC6753:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6753-.LLSDACSBC6753
.LLSDACSBC6753:
	.uleb128 .LEHB36-.LCOLDB5
	.uleb128 .LEHE36-.LEHB36
	.uleb128 0
	.uleb128 0
.LLSDACSEC6753:
	.section	.text.unlikely._ZN2v88internal6torque12CfgAssembler6DropToENS1_12BottomOffsetE
	.section	.text._ZN2v88internal6torque12CfgAssembler6DropToENS1_12BottomOffsetE
	.size	_ZN2v88internal6torque12CfgAssembler6DropToENS1_12BottomOffsetE, .-_ZN2v88internal6torque12CfgAssembler6DropToENS1_12BottomOffsetE
	.section	.text.unlikely._ZN2v88internal6torque12CfgAssembler6DropToENS1_12BottomOffsetE
	.size	_ZN2v88internal6torque12CfgAssembler6DropToENS1_12BottomOffsetE.cold, .-_ZN2v88internal6torque12CfgAssembler6DropToENS1_12BottomOffsetE.cold
.LCOLDE5:
	.section	.text._ZN2v88internal6torque12CfgAssembler6DropToENS1_12BottomOffsetE
.LHOTE5:
	.section	.text.unlikely._ZN2v88internal6torque12CfgAssembler4GotoEPNS1_5BlockE,"ax",@progbits
	.align 2
.LCOLDB6:
	.section	.text._ZN2v88internal6torque12CfgAssembler4GotoEPNS1_5BlockE,"ax",@progbits
.LHOTB6:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque12CfgAssembler4GotoEPNS1_5BlockE
	.type	_ZN2v88internal6torque12CfgAssembler4GotoEPNS1_5BlockE, @function
_ZN2v88internal6torque12CfgAssembler4GotoEPNS1_5BlockE:
.LFB6737:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6737
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 32(%rsi)
	jne	.L399
.L382:
.LEHB37:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
	movl	$40, %edi
	movq	(%rax), %rax
	movdqu	(%rax), %xmm0
	movl	16(%rax), %eax
	movups	%xmm0, -72(%rbp)
	movl	%eax, -56(%rbp)
	call	_Znwm@PLT
.LEHE37:
	movdqu	-72(%rbp), %xmm1
	leaq	24(%rbx), %rdx
	movq	%rbx, %rsi
	movq	%rax, %r12
	movups	%xmm1, 8(%rax)
	movl	-56(%rbp), %eax
	movq	%r12, %rdi
	movq	%r13, 32(%r12)
	movl	%eax, 24(%r12)
	leaq	16+_ZTVN2v88internal6torque15GotoInstructionE(%rip), %rax
	movq	%rax, (%r12)
.LEHB38:
	call	_ZNK2v88internal6torque15GotoInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE@PLT
	movl	_ZN2v88internal6torque15GotoInstruction5kKindE(%rip), %eax
	movq	120(%rbx), %rbx
	leaq	-88(%rbp), %rdi
	movq	%r12, %rsi
	movl	%eax, -96(%rbp)
	movq	(%r12), %rax
	call	*(%rax)
.LEHE38:
	movq	16(%rbx), %rsi
	cmpq	24(%rbx), %rsi
	je	.L383
	movl	-96(%rbp), %eax
	leaq	8(%rsi), %rdi
	movl	%eax, (%rsi)
	movq	-88(%rbp), %r8
	movq	(%r8), %rax
	movq	%r8, %rsi
.LEHB39:
	call	*(%rax)
.LEHE39:
	addq	$16, 16(%rbx)
.L384:
	movq	-88(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L385
	movq	(%rdi), %rax
	call	*24(%rax)
.L385:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L400
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L399:
	.cfi_restore_state
	movq	8(%rdi), %rdx
	movq	48(%rsi), %rsi
	subq	(%rdi), %rdx
	subq	40(%r13), %rsi
	sarq	$3, %rsi
	sarq	$3, %rdx
.LEHB40:
	call	_ZN2v88internal6torque12CfgAssembler11DeleteRangeENS1_10StackRangeE
.LEHE40:
	jmp	.L382
	.p2align 4,,10
	.p2align 3
.L383:
	leaq	-96(%rbp), %rdx
	leaq	8(%rbx), %rdi
.LEHB41:
	call	_ZNSt6vectorIN2v88internal6torque11InstructionESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
.LEHE41:
	jmp	.L384
.L400:
	call	__stack_chk_fail@PLT
.L390:
	endbr64
	movq	%rax, %r13
	jmp	.L388
.L391:
	endbr64
	movq	%rax, %r13
	jmp	.L386
	.section	.gcc_except_table._ZN2v88internal6torque12CfgAssembler4GotoEPNS1_5BlockE,"a",@progbits
.LLSDA6737:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6737-.LLSDACSB6737
.LLSDACSB6737:
	.uleb128 .LEHB37-.LFB6737
	.uleb128 .LEHE37-.LEHB37
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB38-.LFB6737
	.uleb128 .LEHE38-.LEHB38
	.uleb128 .L390-.LFB6737
	.uleb128 0
	.uleb128 .LEHB39-.LFB6737
	.uleb128 .LEHE39-.LEHB39
	.uleb128 .L391-.LFB6737
	.uleb128 0
	.uleb128 .LEHB40-.LFB6737
	.uleb128 .LEHE40-.LEHB40
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB41-.LFB6737
	.uleb128 .LEHE41-.LEHB41
	.uleb128 .L391-.LFB6737
	.uleb128 0
.LLSDACSE6737:
	.section	.text._ZN2v88internal6torque12CfgAssembler4GotoEPNS1_5BlockE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque12CfgAssembler4GotoEPNS1_5BlockE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6737
	.type	_ZN2v88internal6torque12CfgAssembler4GotoEPNS1_5BlockE.cold, @function
_ZN2v88internal6torque12CfgAssembler4GotoEPNS1_5BlockE.cold:
.LFSB6737:
.L386:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movq	-88(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L388
	movq	(%rdi), %rax
	call	*24(%rax)
.L388:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	movq	%r13, %rdi
.LEHB42:
	call	_Unwind_Resume@PLT
.LEHE42:
	.cfi_endproc
.LFE6737:
	.section	.gcc_except_table._ZN2v88internal6torque12CfgAssembler4GotoEPNS1_5BlockE
.LLSDAC6737:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6737-.LLSDACSBC6737
.LLSDACSBC6737:
	.uleb128 .LEHB42-.LCOLDB6
	.uleb128 .LEHE42-.LEHB42
	.uleb128 0
	.uleb128 0
.LLSDACSEC6737:
	.section	.text.unlikely._ZN2v88internal6torque12CfgAssembler4GotoEPNS1_5BlockE
	.section	.text._ZN2v88internal6torque12CfgAssembler4GotoEPNS1_5BlockE
	.size	_ZN2v88internal6torque12CfgAssembler4GotoEPNS1_5BlockE, .-_ZN2v88internal6torque12CfgAssembler4GotoEPNS1_5BlockE
	.section	.text.unlikely._ZN2v88internal6torque12CfgAssembler4GotoEPNS1_5BlockE
	.size	_ZN2v88internal6torque12CfgAssembler4GotoEPNS1_5BlockE.cold, .-_ZN2v88internal6torque12CfgAssembler4GotoEPNS1_5BlockE.cold
.LCOLDE6:
	.section	.text._ZN2v88internal6torque12CfgAssembler4GotoEPNS1_5BlockE
.LHOTE6:
	.section	.text.unlikely._ZN2v88internal6torque12CfgAssembler11UnreachableEv,"ax",@progbits
	.align 2
.LCOLDB7:
	.section	.text._ZN2v88internal6torque12CfgAssembler11UnreachableEv,"ax",@progbits
.LHOTB7:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque12CfgAssembler11UnreachableEv
	.type	_ZN2v88internal6torque12CfgAssembler11UnreachableEv, @function
_ZN2v88internal6torque12CfgAssembler11UnreachableEv:
.LFB6770:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6770
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-144(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$136, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN2v88internal6torque15InstructionBaseE(%rip), %rax
	movq	%r13, -160(%rbp)
	movq	$0, -152(%rbp)
	movb	$0, -144(%rbp)
	movq	%rax, -128(%rbp)
.LEHB43:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
.LEHE43:
	movq	(%rax), %rax
	leaq	16+_ZTVN2v88internal6torque16AbortInstructionE(%rip), %r15
	leaq	-80(%rbp), %r14
	movdqu	(%rax), %xmm0
	movq	%r15, -128(%rbp)
	movups	%xmm0, -120(%rbp)
	movl	16(%rax), %eax
	movl	$1, -100(%rbp)
	movl	%eax, -104(%rbp)
	movq	-160(%rbp), %rax
	movq	%r14, -96(%rbp)
	cmpq	%r13, %rax
	je	.L431
	movq	%rax, -96(%rbp)
	movq	-144(%rbp), %rax
	movq	%rax, -80(%rbp)
.L403:
	movq	-152(%rbp), %rax
	movl	$64, %edi
	movq	%r13, -160(%rbp)
	movq	$0, -152(%rbp)
	movq	%rax, -88(%rbp)
	movb	$0, -144(%rbp)
.LEHB44:
	call	_Znwm@PLT
.LEHE44:
	movdqu	-120(%rbp), %xmm1
	movq	%rax, %r12
	movups	%xmm1, 8(%rax)
	movl	-104(%rbp), %eax
	movq	%r15, (%r12)
	movl	%eax, 24(%r12)
	movl	-100(%rbp), %eax
	movl	%eax, 28(%r12)
	leaq	48(%r12), %rax
	movq	%rax, 32(%r12)
	movq	-96(%rbp), %rax
	cmpq	%r14, %rax
	je	.L432
	movq	%rax, 32(%r12)
	movq	-80(%rbp), %rax
	movq	%rax, 48(%r12)
.L405:
	movq	-88(%rbp), %rax
	leaq	24(%rbx), %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%r14, -96(%rbp)
	movq	%rax, 40(%r12)
	movq	$0, -88(%rbp)
	movb	$0, -80(%rbp)
.LEHB45:
	call	_ZNK2v88internal6torque16AbortInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE@PLT
	movl	_ZN2v88internal6torque16AbortInstruction5kKindE(%rip), %eax
	movq	120(%rbx), %rbx
	leaq	-168(%rbp), %rdi
	movq	%r12, %rsi
	movl	%eax, -176(%rbp)
	movq	(%r12), %rax
	call	*(%rax)
.LEHE45:
	movq	16(%rbx), %rsi
	cmpq	24(%rbx), %rsi
	je	.L406
	movl	-176(%rbp), %eax
	leaq	8(%rsi), %rdi
	movl	%eax, (%rsi)
	movq	-168(%rbp), %r8
	movq	(%r8), %rax
	movq	%r8, %rsi
.LEHB46:
	call	*(%rax)
	addq	$16, 16(%rbx)
.L407:
	movq	-168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L408
	movq	(%rdi), %rax
	call	*24(%rax)
.L408:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	movq	-96(%rbp), %rdi
	movq	%r15, -128(%rbp)
	cmpq	%r14, %rdi
	je	.L418
	call	_ZdlPv@PLT
.L418:
	movq	-160(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L401
	call	_ZdlPv@PLT
.L401:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L433
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L431:
	.cfi_restore_state
	movdqa	-144(%rbp), %xmm2
	movaps	%xmm2, -80(%rbp)
	jmp	.L403
	.p2align 4,,10
	.p2align 3
.L432:
	movdqa	-80(%rbp), %xmm3
	movups	%xmm3, 48(%r12)
	jmp	.L405
	.p2align 4,,10
	.p2align 3
.L406:
	leaq	-176(%rbp), %rdx
	leaq	8(%rbx), %rdi
	call	_ZNSt6vectorIN2v88internal6torque11InstructionESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
.LEHE46:
	jmp	.L407
.L433:
	call	__stack_chk_fail@PLT
.L423:
	endbr64
	movq	%rax, %rbx
	jmp	.L409
.L420:
	endbr64
	movq	%rax, %r12
	jmp	.L415
.L422:
	endbr64
	movq	%rax, %rbx
	jmp	.L411
.L421:
	endbr64
	movq	%rax, %r12
	jmp	.L413
	.section	.gcc_except_table._ZN2v88internal6torque12CfgAssembler11UnreachableEv,"a",@progbits
.LLSDA6770:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6770-.LLSDACSB6770
.LLSDACSB6770:
	.uleb128 .LEHB43-.LFB6770
	.uleb128 .LEHE43-.LEHB43
	.uleb128 .L420-.LFB6770
	.uleb128 0
	.uleb128 .LEHB44-.LFB6770
	.uleb128 .LEHE44-.LEHB44
	.uleb128 .L421-.LFB6770
	.uleb128 0
	.uleb128 .LEHB45-.LFB6770
	.uleb128 .LEHE45-.LEHB45
	.uleb128 .L422-.LFB6770
	.uleb128 0
	.uleb128 .LEHB46-.LFB6770
	.uleb128 .LEHE46-.LEHB46
	.uleb128 .L423-.LFB6770
	.uleb128 0
.LLSDACSE6770:
	.section	.text._ZN2v88internal6torque12CfgAssembler11UnreachableEv
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque12CfgAssembler11UnreachableEv
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6770
	.type	_ZN2v88internal6torque12CfgAssembler11UnreachableEv.cold, @function
_ZN2v88internal6torque12CfgAssembler11UnreachableEv.cold:
.LFSB6770:
.L409:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L411
	movq	(%rdi), %rax
	call	*24(%rax)
.L411:
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	%rbx, %r12
	call	*24(%rax)
.L413:
	movq	-96(%rbp), %rdi
	movq	%r15, -128(%rbp)
	cmpq	%r14, %rdi
	je	.L415
	call	_ZdlPv@PLT
.L415:
	movq	-160(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L416
	call	_ZdlPv@PLT
.L416:
	movq	%r12, %rdi
.LEHB47:
	call	_Unwind_Resume@PLT
.LEHE47:
	.cfi_endproc
.LFE6770:
	.section	.gcc_except_table._ZN2v88internal6torque12CfgAssembler11UnreachableEv
.LLSDAC6770:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6770-.LLSDACSBC6770
.LLSDACSBC6770:
	.uleb128 .LEHB47-.LCOLDB7
	.uleb128 .LEHE47-.LEHB47
	.uleb128 0
	.uleb128 0
.LLSDACSEC6770:
	.section	.text.unlikely._ZN2v88internal6torque12CfgAssembler11UnreachableEv
	.section	.text._ZN2v88internal6torque12CfgAssembler11UnreachableEv
	.size	_ZN2v88internal6torque12CfgAssembler11UnreachableEv, .-_ZN2v88internal6torque12CfgAssembler11UnreachableEv
	.section	.text.unlikely._ZN2v88internal6torque12CfgAssembler11UnreachableEv
	.size	_ZN2v88internal6torque12CfgAssembler11UnreachableEv.cold, .-_ZN2v88internal6torque12CfgAssembler11UnreachableEv.cold
.LCOLDE7:
	.section	.text._ZN2v88internal6torque12CfgAssembler11UnreachableEv
.LHOTE7:
	.section	.text.unlikely._ZN2v88internal6torque12CfgAssembler10DebugBreakEv,"ax",@progbits
	.align 2
.LCOLDB8:
	.section	.text._ZN2v88internal6torque12CfgAssembler10DebugBreakEv,"ax",@progbits
.LHOTB8:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque12CfgAssembler10DebugBreakEv
	.type	_ZN2v88internal6torque12CfgAssembler10DebugBreakEv, @function
_ZN2v88internal6torque12CfgAssembler10DebugBreakEv:
.LFB6771:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6771
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-144(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$136, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN2v88internal6torque15InstructionBaseE(%rip), %rax
	movq	%r13, -160(%rbp)
	movq	$0, -152(%rbp)
	movb	$0, -144(%rbp)
	movq	%rax, -128(%rbp)
.LEHB48:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
.LEHE48:
	movq	(%rax), %rax
	leaq	16+_ZTVN2v88internal6torque16AbortInstructionE(%rip), %r15
	leaq	-80(%rbp), %r14
	movdqu	(%rax), %xmm0
	movq	%r15, -128(%rbp)
	movups	%xmm0, -120(%rbp)
	movl	16(%rax), %eax
	movl	$0, -100(%rbp)
	movl	%eax, -104(%rbp)
	movq	-160(%rbp), %rax
	movq	%r14, -96(%rbp)
	cmpq	%r13, %rax
	je	.L464
	movq	%rax, -96(%rbp)
	movq	-144(%rbp), %rax
	movq	%rax, -80(%rbp)
.L436:
	movq	-152(%rbp), %rax
	movl	$64, %edi
	movq	%r13, -160(%rbp)
	movq	$0, -152(%rbp)
	movq	%rax, -88(%rbp)
	movb	$0, -144(%rbp)
.LEHB49:
	call	_Znwm@PLT
.LEHE49:
	movdqu	-120(%rbp), %xmm1
	movq	%rax, %r12
	movups	%xmm1, 8(%rax)
	movl	-104(%rbp), %eax
	movq	%r15, (%r12)
	movl	%eax, 24(%r12)
	movl	-100(%rbp), %eax
	movl	%eax, 28(%r12)
	leaq	48(%r12), %rax
	movq	%rax, 32(%r12)
	movq	-96(%rbp), %rax
	cmpq	%r14, %rax
	je	.L465
	movq	%rax, 32(%r12)
	movq	-80(%rbp), %rax
	movq	%rax, 48(%r12)
.L438:
	movq	-88(%rbp), %rax
	leaq	24(%rbx), %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%r14, -96(%rbp)
	movq	%rax, 40(%r12)
	movq	$0, -88(%rbp)
	movb	$0, -80(%rbp)
.LEHB50:
	call	_ZNK2v88internal6torque16AbortInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE@PLT
	movl	_ZN2v88internal6torque16AbortInstruction5kKindE(%rip), %eax
	movq	120(%rbx), %rbx
	leaq	-168(%rbp), %rdi
	movq	%r12, %rsi
	movl	%eax, -176(%rbp)
	movq	(%r12), %rax
	call	*(%rax)
.LEHE50:
	movq	16(%rbx), %rsi
	cmpq	24(%rbx), %rsi
	je	.L439
	movl	-176(%rbp), %eax
	leaq	8(%rsi), %rdi
	movl	%eax, (%rsi)
	movq	-168(%rbp), %r8
	movq	(%r8), %rax
	movq	%r8, %rsi
.LEHB51:
	call	*(%rax)
	addq	$16, 16(%rbx)
.L440:
	movq	-168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L441
	movq	(%rdi), %rax
	call	*24(%rax)
.L441:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	movq	-96(%rbp), %rdi
	movq	%r15, -128(%rbp)
	cmpq	%r14, %rdi
	je	.L451
	call	_ZdlPv@PLT
.L451:
	movq	-160(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L434
	call	_ZdlPv@PLT
.L434:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L466
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L464:
	.cfi_restore_state
	movdqa	-144(%rbp), %xmm2
	movaps	%xmm2, -80(%rbp)
	jmp	.L436
	.p2align 4,,10
	.p2align 3
.L465:
	movdqa	-80(%rbp), %xmm3
	movups	%xmm3, 48(%r12)
	jmp	.L438
	.p2align 4,,10
	.p2align 3
.L439:
	leaq	-176(%rbp), %rdx
	leaq	8(%rbx), %rdi
	call	_ZNSt6vectorIN2v88internal6torque11InstructionESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
.LEHE51:
	jmp	.L440
.L466:
	call	__stack_chk_fail@PLT
.L456:
	endbr64
	movq	%rax, %rbx
	jmp	.L442
.L453:
	endbr64
	movq	%rax, %r12
	jmp	.L448
.L455:
	endbr64
	movq	%rax, %rbx
	jmp	.L444
.L454:
	endbr64
	movq	%rax, %r12
	jmp	.L446
	.section	.gcc_except_table._ZN2v88internal6torque12CfgAssembler10DebugBreakEv,"a",@progbits
.LLSDA6771:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6771-.LLSDACSB6771
.LLSDACSB6771:
	.uleb128 .LEHB48-.LFB6771
	.uleb128 .LEHE48-.LEHB48
	.uleb128 .L453-.LFB6771
	.uleb128 0
	.uleb128 .LEHB49-.LFB6771
	.uleb128 .LEHE49-.LEHB49
	.uleb128 .L454-.LFB6771
	.uleb128 0
	.uleb128 .LEHB50-.LFB6771
	.uleb128 .LEHE50-.LEHB50
	.uleb128 .L455-.LFB6771
	.uleb128 0
	.uleb128 .LEHB51-.LFB6771
	.uleb128 .LEHE51-.LEHB51
	.uleb128 .L456-.LFB6771
	.uleb128 0
.LLSDACSE6771:
	.section	.text._ZN2v88internal6torque12CfgAssembler10DebugBreakEv
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque12CfgAssembler10DebugBreakEv
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6771
	.type	_ZN2v88internal6torque12CfgAssembler10DebugBreakEv.cold, @function
_ZN2v88internal6torque12CfgAssembler10DebugBreakEv.cold:
.LFSB6771:
.L442:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L444
	movq	(%rdi), %rax
	call	*24(%rax)
.L444:
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	%rbx, %r12
	call	*24(%rax)
.L446:
	movq	-96(%rbp), %rdi
	movq	%r15, -128(%rbp)
	cmpq	%r14, %rdi
	je	.L448
	call	_ZdlPv@PLT
.L448:
	movq	-160(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L449
	call	_ZdlPv@PLT
.L449:
	movq	%r12, %rdi
.LEHB52:
	call	_Unwind_Resume@PLT
.LEHE52:
	.cfi_endproc
.LFE6771:
	.section	.gcc_except_table._ZN2v88internal6torque12CfgAssembler10DebugBreakEv
.LLSDAC6771:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6771-.LLSDACSBC6771
.LLSDACSBC6771:
	.uleb128 .LEHB52-.LCOLDB8
	.uleb128 .LEHE52-.LEHB52
	.uleb128 0
	.uleb128 0
.LLSDACSEC6771:
	.section	.text.unlikely._ZN2v88internal6torque12CfgAssembler10DebugBreakEv
	.section	.text._ZN2v88internal6torque12CfgAssembler10DebugBreakEv
	.size	_ZN2v88internal6torque12CfgAssembler10DebugBreakEv, .-_ZN2v88internal6torque12CfgAssembler10DebugBreakEv
	.section	.text.unlikely._ZN2v88internal6torque12CfgAssembler10DebugBreakEv
	.size	_ZN2v88internal6torque12CfgAssembler10DebugBreakEv.cold, .-_ZN2v88internal6torque12CfgAssembler10DebugBreakEv.cold
.LCOLDE8:
	.section	.text._ZN2v88internal6torque12CfgAssembler10DebugBreakEv
.LHOTE8:
	.section	.text.unlikely._ZN2v88internal6torque12CfgAssembler5PrintENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"ax",@progbits
	.align 2
.LCOLDB9:
	.section	.text._ZN2v88internal6torque12CfgAssembler5PrintENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"ax",@progbits
.LHOTB9:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque12CfgAssembler5PrintENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN2v88internal6torque12CfgAssembler5PrintENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN2v88internal6torque12CfgAssembler5PrintENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB6762:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6762
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-144(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$136, %rsp
	movq	(%rsi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16(%rsi), %rax
	movq	%r13, -160(%rbp)
	cmpq	%rax, %rdx
	je	.L499
	movq	%rdx, -160(%rbp)
	movq	16(%rsi), %rdx
	movq	%rdx, -144(%rbp)
.L469:
	movq	8(%rsi), %rdx
	movq	%rax, (%rsi)
	leaq	16+_ZTVN2v88internal6torque15InstructionBaseE(%rip), %rax
	movq	$0, 8(%rsi)
	movb	$0, 16(%rsi)
	movq	%rdx, -152(%rbp)
	movq	%rax, -128(%rbp)
.LEHB53:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
.LEHE53:
	movq	(%rax), %rax
	leaq	16+_ZTVN2v88internal6torque30PrintConstantStringInstructionE(%rip), %r15
	leaq	-80(%rbp), %r14
	movdqu	(%rax), %xmm0
	movq	%r15, -128(%rbp)
	movups	%xmm0, -120(%rbp)
	movl	16(%rax), %eax
	movq	%r14, -96(%rbp)
	movl	%eax, -104(%rbp)
	movq	-160(%rbp), %rax
	cmpq	%r13, %rax
	je	.L500
	movq	%rax, -96(%rbp)
	movq	-144(%rbp), %rax
	movq	%rax, -80(%rbp)
.L471:
	movq	-152(%rbp), %rax
	movl	$64, %edi
	movq	%r13, -160(%rbp)
	movq	$0, -152(%rbp)
	movq	%rax, -88(%rbp)
	movb	$0, -144(%rbp)
.LEHB54:
	call	_Znwm@PLT
.LEHE54:
	movdqu	-120(%rbp), %xmm1
	movq	%rax, %r12
	movups	%xmm1, 8(%rax)
	movl	-104(%rbp), %eax
	movq	%r15, (%r12)
	movl	%eax, 24(%r12)
	leaq	48(%r12), %rax
	movq	%rax, 32(%r12)
	movq	-96(%rbp), %rax
	cmpq	%r14, %rax
	je	.L501
	movq	%rax, 32(%r12)
	movq	-80(%rbp), %rax
	movq	%rax, 48(%r12)
.L473:
	movq	-88(%rbp), %rax
	leaq	24(%rbx), %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%r14, -96(%rbp)
	movq	%rax, 40(%r12)
	movq	$0, -88(%rbp)
	movb	$0, -80(%rbp)
.LEHB55:
	call	_ZNK2v88internal6torque30PrintConstantStringInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE@PLT
	movl	_ZN2v88internal6torque30PrintConstantStringInstruction5kKindE(%rip), %eax
	movq	120(%rbx), %rbx
	leaq	-168(%rbp), %rdi
	movq	%r12, %rsi
	movl	%eax, -176(%rbp)
	movq	(%r12), %rax
	call	*(%rax)
.LEHE55:
	movq	16(%rbx), %rsi
	cmpq	24(%rbx), %rsi
	je	.L474
	movl	-176(%rbp), %eax
	leaq	8(%rsi), %rdi
	movl	%eax, (%rsi)
	movq	-168(%rbp), %r8
	movq	(%r8), %rax
	movq	%r8, %rsi
.LEHB56:
	call	*(%rax)
	addq	$16, 16(%rbx)
.L475:
	movq	-168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L476
	movq	(%rdi), %rax
	call	*24(%rax)
.L476:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	movq	-96(%rbp), %rdi
	movq	%r15, -128(%rbp)
	cmpq	%r14, %rdi
	je	.L486
	call	_ZdlPv@PLT
.L486:
	movq	-160(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L467
	call	_ZdlPv@PLT
.L467:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L502
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L499:
	.cfi_restore_state
	movdqu	16(%rsi), %xmm2
	movaps	%xmm2, -144(%rbp)
	jmp	.L469
	.p2align 4,,10
	.p2align 3
.L501:
	movdqa	-80(%rbp), %xmm4
	movups	%xmm4, 48(%r12)
	jmp	.L473
	.p2align 4,,10
	.p2align 3
.L500:
	movdqa	-144(%rbp), %xmm3
	movaps	%xmm3, -80(%rbp)
	jmp	.L471
	.p2align 4,,10
	.p2align 3
.L474:
	leaq	-176(%rbp), %rdx
	leaq	8(%rbx), %rdi
	call	_ZNSt6vectorIN2v88internal6torque11InstructionESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
.LEHE56:
	jmp	.L475
.L502:
	call	__stack_chk_fail@PLT
.L491:
	endbr64
	movq	%rax, %rbx
	jmp	.L477
.L489:
	endbr64
	movq	%rax, %r12
	jmp	.L481
.L490:
	endbr64
	movq	%rax, %rbx
	jmp	.L479
.L488:
	endbr64
	movq	%rax, %r12
	jmp	.L483
	.section	.gcc_except_table._ZN2v88internal6torque12CfgAssembler5PrintENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"a",@progbits
.LLSDA6762:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6762-.LLSDACSB6762
.LLSDACSB6762:
	.uleb128 .LEHB53-.LFB6762
	.uleb128 .LEHE53-.LEHB53
	.uleb128 .L488-.LFB6762
	.uleb128 0
	.uleb128 .LEHB54-.LFB6762
	.uleb128 .LEHE54-.LEHB54
	.uleb128 .L489-.LFB6762
	.uleb128 0
	.uleb128 .LEHB55-.LFB6762
	.uleb128 .LEHE55-.LEHB55
	.uleb128 .L490-.LFB6762
	.uleb128 0
	.uleb128 .LEHB56-.LFB6762
	.uleb128 .LEHE56-.LEHB56
	.uleb128 .L491-.LFB6762
	.uleb128 0
.LLSDACSE6762:
	.section	.text._ZN2v88internal6torque12CfgAssembler5PrintENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque12CfgAssembler5PrintENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6762
	.type	_ZN2v88internal6torque12CfgAssembler5PrintENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.cold, @function
_ZN2v88internal6torque12CfgAssembler5PrintENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.cold:
.LFSB6762:
.L477:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L479
	movq	(%rdi), %rax
	call	*24(%rax)
.L479:
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	%rbx, %r12
	call	*24(%rax)
.L481:
	movq	-96(%rbp), %rdi
	movq	%r15, -128(%rbp)
	cmpq	%r14, %rdi
	je	.L483
	call	_ZdlPv@PLT
.L483:
	movq	-160(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L484
	call	_ZdlPv@PLT
.L484:
	movq	%r12, %rdi
.LEHB57:
	call	_Unwind_Resume@PLT
.LEHE57:
	.cfi_endproc
.LFE6762:
	.section	.gcc_except_table._ZN2v88internal6torque12CfgAssembler5PrintENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
.LLSDAC6762:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6762-.LLSDACSBC6762
.LLSDACSBC6762:
	.uleb128 .LEHB57-.LCOLDB9
	.uleb128 .LEHE57-.LEHB57
	.uleb128 0
	.uleb128 0
.LLSDACSEC6762:
	.section	.text.unlikely._ZN2v88internal6torque12CfgAssembler5PrintENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.text._ZN2v88internal6torque12CfgAssembler5PrintENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.size	_ZN2v88internal6torque12CfgAssembler5PrintENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN2v88internal6torque12CfgAssembler5PrintENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.text.unlikely._ZN2v88internal6torque12CfgAssembler5PrintENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.size	_ZN2v88internal6torque12CfgAssembler5PrintENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.cold, .-_ZN2v88internal6torque12CfgAssembler5PrintENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.cold
.LCOLDE9:
	.section	.text._ZN2v88internal6torque12CfgAssembler5PrintENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
.LHOTE9:
	.section	.text.unlikely._ZN2v88internal6torque12CfgAssembler16AssertionFailureENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"ax",@progbits
	.align 2
.LCOLDB10:
	.section	.text._ZN2v88internal6torque12CfgAssembler16AssertionFailureENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"ax",@progbits
.LHOTB10:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque12CfgAssembler16AssertionFailureENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN2v88internal6torque12CfgAssembler16AssertionFailureENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN2v88internal6torque12CfgAssembler16AssertionFailureENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB6766:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6766
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-144(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$136, %rsp
	movq	(%rsi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16(%rsi), %rax
	movq	%r13, -160(%rbp)
	cmpq	%rax, %rdx
	je	.L535
	movq	%rdx, -160(%rbp)
	movq	16(%rsi), %rdx
	movq	%rdx, -144(%rbp)
.L505:
	movq	8(%rsi), %rdx
	movq	%rax, (%rsi)
	leaq	16+_ZTVN2v88internal6torque15InstructionBaseE(%rip), %rax
	movq	$0, 8(%rsi)
	movb	$0, 16(%rsi)
	movq	%rdx, -152(%rbp)
	movq	%rax, -128(%rbp)
.LEHB58:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
.LEHE58:
	movq	(%rax), %rax
	leaq	16+_ZTVN2v88internal6torque16AbortInstructionE(%rip), %r15
	leaq	-80(%rbp), %r14
	movdqu	(%rax), %xmm0
	movq	%r15, -128(%rbp)
	movups	%xmm0, -120(%rbp)
	movl	16(%rax), %eax
	movl	$2, -100(%rbp)
	movl	%eax, -104(%rbp)
	movq	-160(%rbp), %rax
	movq	%r14, -96(%rbp)
	cmpq	%r13, %rax
	je	.L536
	movq	%rax, -96(%rbp)
	movq	-144(%rbp), %rax
	movq	%rax, -80(%rbp)
.L507:
	movq	-152(%rbp), %rax
	movl	$64, %edi
	movq	%r13, -160(%rbp)
	movq	$0, -152(%rbp)
	movq	%rax, -88(%rbp)
	movb	$0, -144(%rbp)
.LEHB59:
	call	_Znwm@PLT
.LEHE59:
	movdqu	-120(%rbp), %xmm1
	movq	%rax, %r12
	movups	%xmm1, 8(%rax)
	movl	-104(%rbp), %eax
	movq	%r15, (%r12)
	movl	%eax, 24(%r12)
	movl	-100(%rbp), %eax
	movl	%eax, 28(%r12)
	leaq	48(%r12), %rax
	movq	%rax, 32(%r12)
	movq	-96(%rbp), %rax
	cmpq	%r14, %rax
	je	.L537
	movq	%rax, 32(%r12)
	movq	-80(%rbp), %rax
	movq	%rax, 48(%r12)
.L509:
	movq	-88(%rbp), %rax
	leaq	24(%rbx), %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%r14, -96(%rbp)
	movq	%rax, 40(%r12)
	movq	$0, -88(%rbp)
	movb	$0, -80(%rbp)
.LEHB60:
	call	_ZNK2v88internal6torque16AbortInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE@PLT
	movl	_ZN2v88internal6torque16AbortInstruction5kKindE(%rip), %eax
	movq	120(%rbx), %rbx
	leaq	-168(%rbp), %rdi
	movq	%r12, %rsi
	movl	%eax, -176(%rbp)
	movq	(%r12), %rax
	call	*(%rax)
.LEHE60:
	movq	16(%rbx), %rsi
	cmpq	24(%rbx), %rsi
	je	.L510
	movl	-176(%rbp), %eax
	leaq	8(%rsi), %rdi
	movl	%eax, (%rsi)
	movq	-168(%rbp), %r8
	movq	(%r8), %rax
	movq	%r8, %rsi
.LEHB61:
	call	*(%rax)
	addq	$16, 16(%rbx)
.L511:
	movq	-168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L512
	movq	(%rdi), %rax
	call	*24(%rax)
.L512:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	movq	-96(%rbp), %rdi
	movq	%r15, -128(%rbp)
	cmpq	%r14, %rdi
	je	.L522
	call	_ZdlPv@PLT
.L522:
	movq	-160(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L503
	call	_ZdlPv@PLT
.L503:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L538
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L535:
	.cfi_restore_state
	movdqu	16(%rsi), %xmm2
	movaps	%xmm2, -144(%rbp)
	jmp	.L505
	.p2align 4,,10
	.p2align 3
.L537:
	movdqa	-80(%rbp), %xmm4
	movups	%xmm4, 48(%r12)
	jmp	.L509
	.p2align 4,,10
	.p2align 3
.L536:
	movdqa	-144(%rbp), %xmm3
	movaps	%xmm3, -80(%rbp)
	jmp	.L507
	.p2align 4,,10
	.p2align 3
.L510:
	leaq	-176(%rbp), %rdx
	leaq	8(%rbx), %rdi
	call	_ZNSt6vectorIN2v88internal6torque11InstructionESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
.LEHE61:
	jmp	.L511
.L538:
	call	__stack_chk_fail@PLT
.L527:
	endbr64
	movq	%rax, %rbx
	jmp	.L513
.L525:
	endbr64
	movq	%rax, %r12
	jmp	.L517
.L526:
	endbr64
	movq	%rax, %rbx
	jmp	.L515
.L524:
	endbr64
	movq	%rax, %r12
	jmp	.L519
	.section	.gcc_except_table._ZN2v88internal6torque12CfgAssembler16AssertionFailureENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"a",@progbits
.LLSDA6766:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6766-.LLSDACSB6766
.LLSDACSB6766:
	.uleb128 .LEHB58-.LFB6766
	.uleb128 .LEHE58-.LEHB58
	.uleb128 .L524-.LFB6766
	.uleb128 0
	.uleb128 .LEHB59-.LFB6766
	.uleb128 .LEHE59-.LEHB59
	.uleb128 .L525-.LFB6766
	.uleb128 0
	.uleb128 .LEHB60-.LFB6766
	.uleb128 .LEHE60-.LEHB60
	.uleb128 .L526-.LFB6766
	.uleb128 0
	.uleb128 .LEHB61-.LFB6766
	.uleb128 .LEHE61-.LEHB61
	.uleb128 .L527-.LFB6766
	.uleb128 0
.LLSDACSE6766:
	.section	.text._ZN2v88internal6torque12CfgAssembler16AssertionFailureENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque12CfgAssembler16AssertionFailureENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6766
	.type	_ZN2v88internal6torque12CfgAssembler16AssertionFailureENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.cold, @function
_ZN2v88internal6torque12CfgAssembler16AssertionFailureENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.cold:
.LFSB6766:
.L513:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L515
	movq	(%rdi), %rax
	call	*24(%rax)
.L515:
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	%rbx, %r12
	call	*24(%rax)
.L517:
	movq	-96(%rbp), %rdi
	movq	%r15, -128(%rbp)
	cmpq	%r14, %rdi
	je	.L519
	call	_ZdlPv@PLT
.L519:
	movq	-160(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L520
	call	_ZdlPv@PLT
.L520:
	movq	%r12, %rdi
.LEHB62:
	call	_Unwind_Resume@PLT
.LEHE62:
	.cfi_endproc
.LFE6766:
	.section	.gcc_except_table._ZN2v88internal6torque12CfgAssembler16AssertionFailureENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
.LLSDAC6766:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6766-.LLSDACSBC6766
.LLSDACSBC6766:
	.uleb128 .LEHB62-.LCOLDB10
	.uleb128 .LEHE62-.LEHB62
	.uleb128 0
	.uleb128 0
.LLSDACSEC6766:
	.section	.text.unlikely._ZN2v88internal6torque12CfgAssembler16AssertionFailureENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.text._ZN2v88internal6torque12CfgAssembler16AssertionFailureENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.size	_ZN2v88internal6torque12CfgAssembler16AssertionFailureENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN2v88internal6torque12CfgAssembler16AssertionFailureENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.text.unlikely._ZN2v88internal6torque12CfgAssembler16AssertionFailureENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.size	_ZN2v88internal6torque12CfgAssembler16AssertionFailureENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.cold, .-_ZN2v88internal6torque12CfgAssembler16AssertionFailureENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.cold
.LCOLDE10:
	.section	.text._ZN2v88internal6torque12CfgAssembler16AssertionFailureENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
.LHOTE10:
	.section	.text.unlikely._ZN2v88internal6torque12CfgAssembler4GotoEPNS1_5BlockEm,"ax",@progbits
	.align 2
.LCOLDB11:
	.section	.text._ZN2v88internal6torque12CfgAssembler4GotoEPNS1_5BlockEm,"ax",@progbits
.LHOTB11:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque12CfgAssembler4GotoEPNS1_5BlockEm
	.type	_ZN2v88internal6torque12CfgAssembler4GotoEPNS1_5BlockEm, @function
_ZN2v88internal6torque12CfgAssembler4GotoEPNS1_5BlockEm:
.LFB6744:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6744
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$104, %rsp
	movq	%rdx, -136(%rbp)
	movq	48(%rsi), %r14
	movq	8(%rdi), %r15
	subq	40(%rsi), %r14
	subq	(%rdi), %r15
	sarq	$3, %r14
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	sarq	$3, %r15
	subq	%rdx, %r14
	subq	%rdx, %r15
.LEHB63:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
	movl	$48, %edi
	movq	(%rax), %rax
	movdqu	(%rax), %xmm1
	movl	16(%rax), %eax
	movups	%xmm1, -104(%rbp)
	movl	%eax, -88(%rbp)
	call	_Znwm@PLT
.LEHE63:
	movdqu	-104(%rbp), %xmm2
	movq	%r15, %xmm3
	movq	%rbx, %rsi
	movq	%rax, %r12
	movq	%r14, %xmm0
	leaq	24(%rbx), %r15
	movups	%xmm2, 8(%rax)
	movl	-88(%rbp), %eax
	punpcklqdq	%xmm3, %xmm0
	movq	%r15, %rdx
	movups	%xmm0, 32(%r12)
	movq	%r12, %rdi
	movl	%eax, 24(%r12)
	leaq	16+_ZTVN2v88internal6torque22DeleteRangeInstructionE(%rip), %rax
	movq	%rax, (%r12)
.LEHB64:
	call	_ZNK2v88internal6torque22DeleteRangeInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE@PLT
	movl	_ZN2v88internal6torque22DeleteRangeInstruction5kKindE(%rip), %eax
	movq	120(%rbx), %r14
	movq	%r12, %rsi
	movl	%eax, -128(%rbp)
	leaq	-120(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	%rax, %rdi
	movq	(%r12), %rax
	call	*(%rax)
.LEHE64:
	movq	16(%r14), %rsi
	cmpq	24(%r14), %rsi
	je	.L540
	movl	-128(%rbp), %eax
	leaq	8(%rsi), %rdi
	movl	%eax, (%rsi)
	movq	-120(%rbp), %r8
	movq	(%r8), %rax
	movq	%r8, %rsi
.LEHB65:
	call	*(%rax)
.LEHE65:
	addq	$16, 16(%r14)
.L541:
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L542
	movq	(%rdi), %rax
	call	*24(%rax)
.L542:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	movq	8(%rbx), %r14
	subq	(%rbx), %r14
	sarq	$3, %r14
	movq	%r14, %rax
	subq	-136(%rbp), %rax
	movq	%rax, -136(%rbp)
.LEHB66:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
	movl	$40, %edi
	movq	(%rax), %rax
	movdqu	(%rax), %xmm4
	movl	16(%rax), %eax
	movups	%xmm4, -104(%rbp)
	movl	%eax, -88(%rbp)
	call	_Znwm@PLT
.LEHE66:
	movdqu	-104(%rbp), %xmm5
	movq	%r15, %rdx
	movq	%rbx, %rsi
	movq	%rax, %r12
	movups	%xmm5, 8(%rax)
	movl	-88(%rbp), %eax
	movq	%r12, %rdi
	movq	%r13, 32(%r12)
	movl	%eax, 24(%r12)
	leaq	16+_ZTVN2v88internal6torque15GotoInstructionE(%rip), %rax
	movq	%rax, (%r12)
.LEHB67:
	call	_ZNK2v88internal6torque15GotoInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE@PLT
	movl	_ZN2v88internal6torque15GotoInstruction5kKindE(%rip), %eax
	movq	120(%rbx), %rbx
	movq	%r12, %rsi
	movq	-144(%rbp), %rdi
	movl	%eax, -128(%rbp)
	movq	(%r12), %rax
	call	*(%rax)
.LEHE67:
	movq	16(%rbx), %rsi
	cmpq	24(%rbx), %rsi
	je	.L546
	movl	-128(%rbp), %eax
	leaq	8(%rsi), %rdi
	movl	%eax, (%rsi)
	movq	-120(%rbp), %r8
	movq	(%r8), %rax
	movq	%r8, %rsi
.LEHB68:
	call	*(%rax)
.LEHE68:
	addq	$16, 16(%rbx)
.L547:
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L548
	movq	(%rdi), %rax
	call	*24(%rax)
.L548:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L573
	movq	-136(%rbp), %rax
	addq	$104, %rsp
	movq	%r14, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L540:
	.cfi_restore_state
	leaq	-128(%rbp), %rdx
	leaq	8(%r14), %rdi
.LEHB69:
	call	_ZNSt6vectorIN2v88internal6torque11InstructionESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
.LEHE69:
	jmp	.L541
	.p2align 4,,10
	.p2align 3
.L546:
	leaq	-128(%rbp), %rdx
	leaq	8(%rbx), %rdi
.LEHB70:
	call	_ZNSt6vectorIN2v88internal6torque11InstructionESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
.LEHE70:
	jmp	.L547
.L573:
	call	__stack_chk_fail@PLT
.L557:
	endbr64
	movq	%rax, %r13
	jmp	.L549
.L554:
	endbr64
	movq	%rax, %r13
	jmp	.L545
.L555:
	endbr64
	movq	%rax, %r13
	jmp	.L551
.L556:
	endbr64
	movq	%rax, %r13
	jmp	.L543
	.section	.gcc_except_table._ZN2v88internal6torque12CfgAssembler4GotoEPNS1_5BlockEm,"a",@progbits
.LLSDA6744:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6744-.LLSDACSB6744
.LLSDACSB6744:
	.uleb128 .LEHB63-.LFB6744
	.uleb128 .LEHE63-.LEHB63
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB64-.LFB6744
	.uleb128 .LEHE64-.LEHB64
	.uleb128 .L554-.LFB6744
	.uleb128 0
	.uleb128 .LEHB65-.LFB6744
	.uleb128 .LEHE65-.LEHB65
	.uleb128 .L556-.LFB6744
	.uleb128 0
	.uleb128 .LEHB66-.LFB6744
	.uleb128 .LEHE66-.LEHB66
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB67-.LFB6744
	.uleb128 .LEHE67-.LEHB67
	.uleb128 .L555-.LFB6744
	.uleb128 0
	.uleb128 .LEHB68-.LFB6744
	.uleb128 .LEHE68-.LEHB68
	.uleb128 .L557-.LFB6744
	.uleb128 0
	.uleb128 .LEHB69-.LFB6744
	.uleb128 .LEHE69-.LEHB69
	.uleb128 .L556-.LFB6744
	.uleb128 0
	.uleb128 .LEHB70-.LFB6744
	.uleb128 .LEHE70-.LEHB70
	.uleb128 .L557-.LFB6744
	.uleb128 0
.LLSDACSE6744:
	.section	.text._ZN2v88internal6torque12CfgAssembler4GotoEPNS1_5BlockEm
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque12CfgAssembler4GotoEPNS1_5BlockEm
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6744
	.type	_ZN2v88internal6torque12CfgAssembler4GotoEPNS1_5BlockEm.cold, @function
_ZN2v88internal6torque12CfgAssembler4GotoEPNS1_5BlockEm.cold:
.LFSB6744:
.L549:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L551
	movq	(%rdi), %rax
	call	*24(%rax)
.L551:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	movq	%r13, %rdi
.LEHB71:
	call	_Unwind_Resume@PLT
.L543:
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L545
	movq	(%rdi), %rax
	call	*24(%rax)
.L545:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	movq	%r13, %rdi
	call	_Unwind_Resume@PLT
.LEHE71:
	.cfi_endproc
.LFE6744:
	.section	.gcc_except_table._ZN2v88internal6torque12CfgAssembler4GotoEPNS1_5BlockEm
.LLSDAC6744:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6744-.LLSDACSBC6744
.LLSDACSBC6744:
	.uleb128 .LEHB71-.LCOLDB11
	.uleb128 .LEHE71-.LEHB71
	.uleb128 0
	.uleb128 0
.LLSDACSEC6744:
	.section	.text.unlikely._ZN2v88internal6torque12CfgAssembler4GotoEPNS1_5BlockEm
	.section	.text._ZN2v88internal6torque12CfgAssembler4GotoEPNS1_5BlockEm
	.size	_ZN2v88internal6torque12CfgAssembler4GotoEPNS1_5BlockEm, .-_ZN2v88internal6torque12CfgAssembler4GotoEPNS1_5BlockEm
	.section	.text.unlikely._ZN2v88internal6torque12CfgAssembler4GotoEPNS1_5BlockEm
	.size	_ZN2v88internal6torque12CfgAssembler4GotoEPNS1_5BlockEm.cold, .-_ZN2v88internal6torque12CfgAssembler4GotoEPNS1_5BlockEm.cold
.LCOLDE11:
	.section	.text._ZN2v88internal6torque12CfgAssembler4GotoEPNS1_5BlockEm
.LHOTE11:
	.section	.rodata._ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE7_M_copyINSB_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS5_EPKSF_PSt18_Rb_tree_node_baseRT_.str1.8,"aMS",@progbits,1
	.align 8
.LC12:
	.string	"basic_string::_M_construct null not valid"
	.section	.text._ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE7_M_copyINSB_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS5_EPKSF_PSt18_Rb_tree_node_baseRT_,"axG",@progbits,_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE7_M_copyINSB_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS5_EPKSF_PSt18_Rb_tree_node_baseRT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE7_M_copyINSB_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS5_EPKSF_PSt18_Rb_tree_node_baseRT_
	.type	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE7_M_copyINSB_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS5_EPKSF_PSt18_Rb_tree_node_baseRT_, @function
_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE7_M_copyINSB_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS5_EPKSF_PSt18_Rb_tree_node_baseRT_:
.LFB10088:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA10088
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$56, %rsp
	movq	%rdi, -80(%rbp)
	movl	$64, %edi
	movq	%rcx, -88(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
.LEHB72:
	call	_Znwm@PLT
.LEHE72:
	movq	32(%rbx), %r13
	movq	40(%rbx), %r12
	leaq	48(%rax), %rdi
	movq	%rax, %r14
	movq	%rdi, 32(%rax)
	movq	%r13, %rax
	addq	%r12, %rax
	je	.L575
	testq	%r13, %r13
	je	.L628
.L575:
	movq	%r12, -64(%rbp)
	cmpq	$15, %r12
	ja	.L629
	cmpq	$1, %r12
	jne	.L578
	movzbl	0(%r13), %eax
	movb	%al, 48(%r14)
.L579:
	movq	%r12, 40(%r14)
	movb	$0, (%rdi,%r12)
	movl	(%rbx), %eax
	movq	24(%rbx), %rsi
	movq	$0, 16(%r14)
	movl	%eax, (%r14)
	movq	$0, 24(%r14)
	movq	%r15, 8(%r14)
	testq	%rsi, %rsi
	je	.L581
	movq	-88(%rbp), %rcx
	movq	-80(%rbp), %rdi
	movq	%r14, %rdx
.LEHB73:
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE7_M_copyINSB_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS5_EPKSF_PSt18_Rb_tree_node_baseRT_
	movq	%rax, 24(%r14)
.L581:
	movq	16(%rbx), %rbx
	leaq	-64(%rbp), %rax
	movq	%r14, %r12
	movq	%rax, -96(%rbp)
	testq	%rbx, %rbx
	je	.L574
.L584:
	movl	$64, %edi
	movq	%r12, -72(%rbp)
	call	_Znwm@PLT
	leaq	48(%rax), %rdi
	movq	%rax, %r12
	movq	%rdi, 32(%rax)
	movq	32(%rbx), %r15
	movq	40(%rbx), %r13
	movq	%r15, %rax
	addq	%r13, %rax
	je	.L585
	testq	%r15, %r15
	je	.L630
.L585:
	movq	%r13, -64(%rbp)
	cmpq	$15, %r13
	ja	.L631
	cmpq	$1, %r13
	jne	.L588
	movzbl	(%r15), %eax
	movb	%al, 48(%r12)
.L589:
	movq	%r13, 40(%r12)
	movb	$0, (%rdi,%r13)
	movl	(%rbx), %eax
	movq	$0, 16(%r12)
	movl	%eax, (%r12)
	movq	-72(%rbp), %rax
	movq	$0, 24(%r12)
	movq	%r12, 16(%rax)
	movq	%rax, 8(%r12)
	movq	24(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L591
	movq	-88(%rbp), %rcx
	movq	-80(%rbp), %rdi
	movq	%r12, %rdx
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE7_M_copyINSB_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS5_EPKSF_PSt18_Rb_tree_node_baseRT_
.LEHE73:
	movq	%rax, 24(%r12)
.L591:
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L584
.L574:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L632
	addq	$56, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L578:
	.cfi_restore_state
	testq	%r12, %r12
	je	.L579
	jmp	.L577
	.p2align 4,,10
	.p2align 3
.L588:
	testq	%r13, %r13
	je	.L589
	jmp	.L587
	.p2align 4,,10
	.p2align 3
.L631:
	movq	-96(%rbp), %rsi
	leaq	32(%r12), %rdi
	xorl	%edx, %edx
.LEHB74:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE74:
	movq	%rax, 32(%r12)
	movq	%rax, %rdi
	movq	-64(%rbp), %rax
	movq	%rax, 48(%r12)
.L587:
	movq	%r13, %rdx
	movq	%r15, %rsi
	call	memcpy@PLT
	movq	-64(%rbp), %r13
	movq	32(%r12), %rdi
	jmp	.L589
	.p2align 4,,10
	.p2align 3
.L629:
	leaq	-64(%rbp), %rsi
	leaq	32(%r14), %rdi
	xorl	%edx, %edx
.LEHB75:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE75:
	movq	%rax, 32(%r14)
	movq	%rax, %rdi
	movq	-64(%rbp), %rax
	movq	%rax, 48(%r14)
.L577:
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	-64(%rbp), %r12
	movq	32(%r14), %rdi
	jmp	.L579
.L630:
	leaq	.LC12(%rip), %rdi
.LEHB76:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE76:
.L632:
	call	__stack_chk_fail@PLT
.L628:
	leaq	.LC12(%rip), %rdi
.LEHB77:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE77:
.L603:
	endbr64
	movq	%rax, %rdi
	jmp	.L592
.L599:
	endbr64
	movq	%rax, %rdi
	jmp	.L594
.L601:
	endbr64
	movq	%rax, %rdi
	jmp	.L582
.L592:
	call	__cxa_begin_catch@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.LEHB78:
	call	__cxa_rethrow@PLT
.LEHE78:
.L593:
	call	__cxa_end_catch@PLT
	movq	%rbx, %rdi
.L594:
	call	__cxa_begin_catch@PLT
	movq	-80(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
.LEHB79:
	call	__cxa_rethrow@PLT
.LEHE79:
.L582:
	call	__cxa_begin_catch@PLT
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.LEHB80:
	call	__cxa_rethrow@PLT
.LEHE80:
.L604:
	endbr64
	movq	%rax, %rbx
	jmp	.L593
.L600:
	endbr64
	movq	%rax, %r12
	jmp	.L596
.L602:
	endbr64
	movq	%rax, %r12
	jmp	.L583
.L596:
	call	__cxa_end_catch@PLT
	movq	%r12, %rdi
.LEHB81:
	call	_Unwind_Resume@PLT
.LEHE81:
.L583:
	call	__cxa_end_catch@PLT
	movq	%r12, %rdi
.LEHB82:
	call	_Unwind_Resume@PLT
.LEHE82:
	.cfi_endproc
.LFE10088:
	.section	.gcc_except_table._ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE7_M_copyINSB_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS5_EPKSF_PSt18_Rb_tree_node_baseRT_,"aG",@progbits,_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE7_M_copyINSB_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS5_EPKSF_PSt18_Rb_tree_node_baseRT_,comdat
	.align 4
.LLSDA10088:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATT10088-.LLSDATTD10088
.LLSDATTD10088:
	.byte	0x1
	.uleb128 .LLSDACSE10088-.LLSDACSB10088
.LLSDACSB10088:
	.uleb128 .LEHB72-.LFB10088
	.uleb128 .LEHE72-.LEHB72
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB73-.LFB10088
	.uleb128 .LEHE73-.LEHB73
	.uleb128 .L599-.LFB10088
	.uleb128 0x1
	.uleb128 .LEHB74-.LFB10088
	.uleb128 .LEHE74-.LEHB74
	.uleb128 .L603-.LFB10088
	.uleb128 0x1
	.uleb128 .LEHB75-.LFB10088
	.uleb128 .LEHE75-.LEHB75
	.uleb128 .L601-.LFB10088
	.uleb128 0x1
	.uleb128 .LEHB76-.LFB10088
	.uleb128 .LEHE76-.LEHB76
	.uleb128 .L603-.LFB10088
	.uleb128 0x1
	.uleb128 .LEHB77-.LFB10088
	.uleb128 .LEHE77-.LEHB77
	.uleb128 .L601-.LFB10088
	.uleb128 0x1
	.uleb128 .LEHB78-.LFB10088
	.uleb128 .LEHE78-.LEHB78
	.uleb128 .L604-.LFB10088
	.uleb128 0x3
	.uleb128 .LEHB79-.LFB10088
	.uleb128 .LEHE79-.LEHB79
	.uleb128 .L600-.LFB10088
	.uleb128 0
	.uleb128 .LEHB80-.LFB10088
	.uleb128 .LEHE80-.LEHB80
	.uleb128 .L602-.LFB10088
	.uleb128 0
	.uleb128 .LEHB81-.LFB10088
	.uleb128 .LEHE81-.LEHB81
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB82-.LFB10088
	.uleb128 .LEHE82-.LEHB82
	.uleb128 0
	.uleb128 0
.LLSDACSE10088:
	.byte	0x1
	.byte	0
	.byte	0
	.byte	0x7d
	.align 4
	.long	0

.LLSDATT10088:
	.section	.text._ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE7_M_copyINSB_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS5_EPKSF_PSt18_Rb_tree_node_baseRT_,"axG",@progbits,_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE7_M_copyINSB_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS5_EPKSF_PSt18_Rb_tree_node_baseRT_,comdat
	.size	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE7_M_copyINSB_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS5_EPKSF_PSt18_Rb_tree_node_baseRT_, .-_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE7_M_copyINSB_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS5_EPKSF_PSt18_Rb_tree_node_baseRT_
	.section	.text._ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE7_M_copyINSA_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS5_EPKSE_PSt18_Rb_tree_node_baseRT_,"axG",@progbits,_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE7_M_copyINSA_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS5_EPKSE_PSt18_Rb_tree_node_baseRT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE7_M_copyINSA_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS5_EPKSE_PSt18_Rb_tree_node_baseRT_
	.type	_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE7_M_copyINSA_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS5_EPKSE_PSt18_Rb_tree_node_baseRT_, @function
_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE7_M_copyINSA_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS5_EPKSE_PSt18_Rb_tree_node_baseRT_:
.LFB10095:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA10095
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	movl	$40, %edi
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	%rcx, -56(%rbp)
.LEHB83:
	call	_Znwm@PLT
.LEHE83:
	movq	24(%rbx), %rsi
	movq	%rax, %r13
	movq	32(%rbx), %rax
	movq	$0, 16(%r13)
	movq	%rax, 32(%r13)
	movl	(%rbx), %eax
	movq	$0, 24(%r13)
	movl	%eax, 0(%r13)
	movq	%r12, 8(%r13)
	testq	%rsi, %rsi
	je	.L634
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r15, %rdi
.LEHB84:
	call	_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE7_M_copyINSA_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS5_EPKSE_PSt18_Rb_tree_node_baseRT_
	movq	%rax, 24(%r13)
.L634:
	movq	16(%rbx), %r12
	testq	%r12, %r12
	je	.L633
	movq	%r13, %rbx
.L637:
	movl	$40, %edi
	movq	%rbx, %r14
	call	_Znwm@PLT
	movq	%rax, %rbx
	movq	32(%r12), %rax
	movq	%rax, 32(%rbx)
	movl	(%r12), %eax
	movq	$0, 16(%rbx)
	movl	%eax, (%rbx)
	movq	$0, 24(%rbx)
	movq	%rbx, 16(%r14)
	movq	%r14, 8(%rbx)
	movq	24(%r12), %rsi
	testq	%rsi, %rsi
	je	.L636
	movq	-56(%rbp), %rcx
	movq	%rbx, %rdx
	movq	%r15, %rdi
	call	_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE7_M_copyINSA_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS5_EPKSE_PSt18_Rb_tree_node_baseRT_
.LEHE84:
	movq	%rax, 24(%rbx)
.L636:
	movq	16(%r12), %r12
	testq	%r12, %r12
	jne	.L637
.L633:
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L640:
	.cfi_restore_state
	endbr64
	movq	%rax, %rdi
.L638:
	call	__cxa_begin_catch@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
.LEHB85:
	call	__cxa_rethrow@PLT
.LEHE85:
.L641:
	endbr64
	movq	%rax, %r12
.L639:
	call	__cxa_end_catch@PLT
	movq	%r12, %rdi
.LEHB86:
	call	_Unwind_Resume@PLT
.LEHE86:
	.cfi_endproc
.LFE10095:
	.section	.gcc_except_table._ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE7_M_copyINSA_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS5_EPKSE_PSt18_Rb_tree_node_baseRT_,"aG",@progbits,_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE7_M_copyINSA_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS5_EPKSE_PSt18_Rb_tree_node_baseRT_,comdat
	.align 4
.LLSDA10095:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATT10095-.LLSDATTD10095
.LLSDATTD10095:
	.byte	0x1
	.uleb128 .LLSDACSE10095-.LLSDACSB10095
.LLSDACSB10095:
	.uleb128 .LEHB83-.LFB10095
	.uleb128 .LEHE83-.LEHB83
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB84-.LFB10095
	.uleb128 .LEHE84-.LEHB84
	.uleb128 .L640-.LFB10095
	.uleb128 0x1
	.uleb128 .LEHB85-.LFB10095
	.uleb128 .LEHE85-.LEHB85
	.uleb128 .L641-.LFB10095
	.uleb128 0
	.uleb128 .LEHB86-.LFB10095
	.uleb128 .LEHE86-.LEHB86
	.uleb128 0
	.uleb128 0
.LLSDACSE10095:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATT10095:
	.section	.text._ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE7_M_copyINSA_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS5_EPKSE_PSt18_Rb_tree_node_baseRT_,"axG",@progbits,_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE7_M_copyINSA_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS5_EPKSE_PSt18_Rb_tree_node_baseRT_,comdat
	.size	_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE7_M_copyINSA_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS5_EPKSE_PSt18_Rb_tree_node_baseRT_, .-_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE7_M_copyINSA_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS5_EPKSE_PSt18_Rb_tree_node_baseRT_
	.section	.text._ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS5_ERKS5_,"axG",@progbits,_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS5_ERKS5_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS5_ERKS5_
	.type	_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS5_ERKS5_, @function
_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS5_ERKS5_:
.LFB10116:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	8(%rdi), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	cmpq	%rsi, %r14
	je	.L703
	movq	32(%rsi), %rsi
	movq	(%rdx), %rdi
	call	_ZN2v88internal6torqueltERKNS1_4TypeES4_@PLT
	testb	%al, %al
	je	.L664
	movq	24(%r12), %rax
	movq	%rax, %rdx
	cmpq	%r15, %rax
	je	.L656
	movq	%r15, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	0(%r13), %rsi
	movq	32(%rax), %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal6torqueltERKNS1_4TypeES4_@PLT
	testb	%al, %al
	je	.L666
	cmpq	$0, 24(%rbx)
	movl	$0, %eax
	movq	%r15, %rdx
	cmovne	%r15, %rax
	cmove	%rbx, %rdx
.L656:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L703:
	.cfi_restore_state
	cmpq	$0, 40(%rdi)
	je	.L655
	movq	32(%rdi), %rax
	movq	(%rdx), %rsi
	movq	32(%rax), %rdi
	call	_ZN2v88internal6torqueltERKNS1_4TypeES4_@PLT
	testb	%al, %al
	jne	.L704
.L655:
	movq	16(%r12), %rbx
	testq	%rbx, %rbx
	jne	.L658
	jmp	.L705
	.p2align 4,,10
	.p2align 3
.L706:
	movq	16(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L681
.L707:
	movq	%rdx, %rbx
.L658:
	movq	32(%rbx), %rsi
	movq	0(%r13), %rdi
	call	_ZN2v88internal6torqueltERKNS1_4TypeES4_@PLT
	testb	%al, %al
	jne	.L706
	movq	24(%rbx), %rdx
	testq	%rdx, %rdx
	jne	.L707
.L681:
	testb	%al, %al
	je	.L708
.L679:
	cmpq	%rbx, 24(%r12)
	je	.L696
	movq	%rbx, %rdi
	movq	%rbx, %r15
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	%rax, %rbx
.L684:
	movq	32(%rbx), %rdi
	movq	0(%r13), %rsi
	call	_ZN2v88internal6torqueltERKNS1_4TypeES4_@PLT
	xorl	%edx, %edx
	testb	%al, %al
	cmovne	%rdx, %rbx
	cmove	%rdx, %r15
.L685:
	addq	$8, %rsp
	movq	%rbx, %rax
	movq	%r15, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L664:
	.cfi_restore_state
	movq	32(%r15), %rdi
	movq	0(%r13), %rsi
	call	_ZN2v88internal6torqueltERKNS1_4TypeES4_@PLT
	testb	%al, %al
	je	.L675
	movq	32(%r12), %rdx
	xorl	%eax, %eax
	cmpq	%r15, %rdx
	je	.L656
	movq	%r15, %rdi
	call	_ZSt18_Rb_tree_incrementPSt18_Rb_tree_node_base@PLT
	movq	0(%r13), %rdi
	movq	32(%rax), %rsi
	movq	%rax, %rbx
	call	_ZN2v88internal6torqueltERKNS1_4TypeES4_@PLT
	testb	%al, %al
	je	.L677
	cmpq	$0, 24(%r15)
	movl	$0, %eax
	movq	%r15, %rdx
	cmovne	%rbx, %rax
	cmovne	%rbx, %rdx
	jmp	.L656
	.p2align 4,,10
	.p2align 3
.L704:
	movq	32(%r12), %rdx
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L708:
	.cfi_restore_state
	movq	%rbx, %r15
	jmp	.L684
	.p2align 4,,10
	.p2align 3
.L675:
	movq	%r15, %rax
	xorl	%edx, %edx
	jmp	.L656
	.p2align 4,,10
	.p2align 3
.L696:
	movq	%rbx, %r15
	xorl	%ebx, %ebx
	jmp	.L685
	.p2align 4,,10
	.p2align 3
.L666:
	movq	16(%r12), %rbx
	testq	%rbx, %rbx
	jne	.L669
	jmp	.L694
	.p2align 4,,10
	.p2align 3
.L709:
	movq	16(%rbx), %rdx
.L672:
	testq	%rdx, %rdx
	je	.L681
	movq	%rdx, %rbx
.L669:
	movq	32(%rbx), %rsi
	movq	0(%r13), %rdi
	call	_ZN2v88internal6torqueltERKNS1_4TypeES4_@PLT
	testb	%al, %al
	jne	.L709
	movq	24(%rbx), %rdx
	jmp	.L672
	.p2align 4,,10
	.p2align 3
.L705:
	movq	%r15, %rbx
	jmp	.L679
	.p2align 4,,10
	.p2align 3
.L677:
	movq	16(%r12), %rbx
	testq	%rbx, %rbx
	jne	.L680
	jmp	.L694
	.p2align 4,,10
	.p2align 3
.L710:
	movq	16(%rbx), %rdx
.L683:
	testq	%rdx, %rdx
	je	.L681
	movq	%rdx, %rbx
.L680:
	movq	32(%rbx), %rsi
	movq	0(%r13), %rdi
	call	_ZN2v88internal6torqueltERKNS1_4TypeES4_@PLT
	testb	%al, %al
	jne	.L710
	movq	24(%rbx), %rdx
	jmp	.L683
.L694:
	movq	%r14, %rbx
	jmp	.L679
	.cfi_endproc
.LFE10116:
	.size	_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS5_ERKS5_, .-_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS5_ERKS5_
	.section	.text._ZNSt10_HashtableIN2v88internal6torque9UnionTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_rehashEmRKm,"axG",@progbits,_ZNSt10_HashtableIN2v88internal6torque9UnionTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_rehashEmRKm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIN2v88internal6torque9UnionTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_rehashEmRKm
	.type	_ZNSt10_HashtableIN2v88internal6torque9UnionTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_rehashEmRKm, @function
_ZNSt10_HashtableIN2v88internal6torque9UnionTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_rehashEmRKm:
.LFB11053:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA11053
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	cmpq	$1, %rsi
	je	.L733
	movabsq	$1152921504606846975, %rax
	movq	%rdx, %r13
	cmpq	%rax, %rsi
	ja	.L734
	leaq	0(,%rsi,8), %r14
	movq	%r14, %rdi
.LEHB87:
	call	_Znwm@PLT
	movq	%r14, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	memset@PLT
	leaq	48(%rbx), %r9
.L713:
	movq	16(%rbx), %rsi
	movq	$0, 16(%rbx)
	testq	%rsi, %rsi
	je	.L715
	xorl	%r8d, %r8d
	leaq	16(%rbx), %r10
	jmp	.L716
	.p2align 4,,10
	.p2align 3
.L717:
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	(%rdi), %rax
	movq	%rcx, (%rax)
.L718:
	testq	%rsi, %rsi
	je	.L715
.L716:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	128(%rcx), %rax
	divq	%r12
	leaq	0(%r13,%rdx,8), %rdi
	movq	(%rdi), %rax
	testq	%rax, %rax
	jne	.L717
	movq	16(%rbx), %rax
	movq	%rax, (%rcx)
	movq	%rcx, 16(%rbx)
	movq	%r10, (%rdi)
	cmpq	$0, (%rcx)
	je	.L722
	movq	%rcx, 0(%r13,%r8,8)
	movq	%rdx, %r8
	testq	%rsi, %rsi
	jne	.L716
	.p2align 4,,10
	.p2align 3
.L715:
	movq	(%rbx), %rdi
	cmpq	%r9, %rdi
	je	.L719
	call	_ZdlPv@PLT
.L719:
	movq	%r12, 8(%rbx)
	movq	%r13, (%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L722:
	.cfi_restore_state
	movq	%rdx, %r8
	jmp	.L718
	.p2align 4,,10
	.p2align 3
.L733:
	leaq	48(%rdi), %r13
	movq	$0, 48(%rdi)
	movq	%r13, %r9
	jmp	.L713
.L734:
	call	_ZSt17__throw_bad_allocv@PLT
.LEHE87:
.L723:
	endbr64
	movq	%rax, %rdi
.L720:
	call	__cxa_begin_catch@PLT
	movq	0(%r13), %rax
	movq	%rax, 40(%rbx)
.LEHB88:
	call	__cxa_rethrow@PLT
.LEHE88:
.L724:
	endbr64
	movq	%rax, %r12
.L721:
	call	__cxa_end_catch@PLT
	movq	%r12, %rdi
.LEHB89:
	call	_Unwind_Resume@PLT
.LEHE89:
	.cfi_endproc
.LFE11053:
	.section	.gcc_except_table._ZNSt10_HashtableIN2v88internal6torque9UnionTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_rehashEmRKm,"aG",@progbits,_ZNSt10_HashtableIN2v88internal6torque9UnionTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_rehashEmRKm,comdat
	.align 4
.LLSDA11053:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATT11053-.LLSDATTD11053
.LLSDATTD11053:
	.byte	0x1
	.uleb128 .LLSDACSE11053-.LLSDACSB11053
.LLSDACSB11053:
	.uleb128 .LEHB87-.LFB11053
	.uleb128 .LEHE87-.LEHB87
	.uleb128 .L723-.LFB11053
	.uleb128 0x1
	.uleb128 .LEHB88-.LFB11053
	.uleb128 .LEHE88-.LEHB88
	.uleb128 .L724-.LFB11053
	.uleb128 0
	.uleb128 .LEHB89-.LFB11053
	.uleb128 .LEHE89-.LEHB89
	.uleb128 0
	.uleb128 0
.LLSDACSE11053:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATT11053:
	.section	.text._ZNSt10_HashtableIN2v88internal6torque9UnionTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_rehashEmRKm,"axG",@progbits,_ZNSt10_HashtableIN2v88internal6torque9UnionTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_rehashEmRKm,comdat
	.size	_ZNSt10_HashtableIN2v88internal6torque9UnionTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_rehashEmRKm, .-_ZNSt10_HashtableIN2v88internal6torque9UnionTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_rehashEmRKm
	.section	.text._ZN2v88internal6torque9UnionTypeD2Ev,"axG",@progbits,_ZN2v88internal6torque9UnionTypeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque9UnionTypeD2Ev
	.type	_ZN2v88internal6torque9UnionTypeD2Ev, @function
_ZN2v88internal6torque9UnionTypeD2Ev:
.LFB11780:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal6torque9UnionTypeE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	88(%rdi), %rbx
	movq	%rax, (%rdi)
	testq	%rbx, %rbx
	je	.L736
	leaq	72(%rdi), %r13
.L737:
	movq	24(%rbx), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	%rbx, %rdi
	movq	16(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L737
.L736:
	leaq	16+_ZTVN2v88internal6torque4TypeE(%rip), %rax
	leaq	24(%r12), %r13
	movq	%rax, (%r12)
	movq	40(%r12), %r12
	testq	%r12, %r12
	je	.L735
.L741:
	movq	24(%r12), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r12), %rdi
	leaq	48(%r12), %rax
	movq	16(%r12), %rbx
	cmpq	%rax, %rdi
	je	.L739
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L735
.L740:
	movq	%rbx, %r12
	jmp	.L741
	.p2align 4,,10
	.p2align 3
.L739:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L740
.L735:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11780:
	.size	_ZN2v88internal6torque9UnionTypeD2Ev, .-_ZN2v88internal6torque9UnionTypeD2Ev
	.weak	_ZN2v88internal6torque9UnionTypeD1Ev
	.set	_ZN2v88internal6torque9UnionTypeD1Ev,_ZN2v88internal6torque9UnionTypeD2Ev
	.section	.text._ZNSt10_HashtableIN2v88internal6torque9UnionTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb1EEEm,"axG",@progbits,_ZNSt10_HashtableIN2v88internal6torque9UnionTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb1EEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIN2v88internal6torque9UnionTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb1EEEm
	.type	_ZNSt10_HashtableIN2v88internal6torque9UnionTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb1EEEm, @function
_ZNSt10_HashtableIN2v88internal6torque9UnionTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb1EEEm:
.LFB10777:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA10777
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rcx, %r12
	movq	%r8, %rcx
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	addq	$32, %rdi
	subq	$16, %rsp
	movq	-8(%rdi), %rdx
	movq	-24(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	%rax, -48(%rbp)
.LEHB90:
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
.LEHE90:
	testb	%al, %al
	je	.L757
	movq	%rdx, %rsi
	movq	%rbx, %rdi
	leaq	-48(%rbp), %rdx
.LEHB91:
	call	_ZNSt10_HashtableIN2v88internal6torque9UnionTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_rehashEmRKm
.LEHE91:
	movq	%r14, %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	%rdx, %r13
.L757:
	movq	%r14, 128(%r12)
	movq	(%rbx), %rax
	leaq	0(,%r13,8), %rcx
	movq	(%rax,%r13,8), %rax
	testq	%rax, %rax
	je	.L758
	movq	(%rax), %rax
	movq	%rax, (%r12)
	movq	(%rbx), %rax
	movq	(%rax,%r13,8), %rax
	movq	%r12, (%rax)
.L759:
	addq	$1, 24(%rbx)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L771
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L758:
	.cfi_restore_state
	movq	16(%rbx), %rax
	movq	%rax, (%r12)
	movq	%r12, 16(%rbx)
	movq	(%r12), %rax
	testq	%rax, %rax
	je	.L760
	movq	128(%rax), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	(%rbx), %rax
	movq	%r12, (%rax,%rdx,8)
.L760:
	movq	(%rbx), %rax
	leaq	16(%rbx), %rdx
	movq	%rdx, (%rax,%rcx)
	jmp	.L759
.L771:
	call	__stack_chk_fail@PLT
.L764:
	endbr64
	movq	%rax, %rdi
.L761:
	call	__cxa_begin_catch@PLT
	leaq	8(%r12), %rdi
	call	_ZN2v88internal6torque9UnionTypeD1Ev
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.LEHB92:
	call	__cxa_rethrow@PLT
.LEHE92:
.L765:
	endbr64
	movq	%rax, %r12
.L762:
	call	__cxa_end_catch@PLT
	movq	%r12, %rdi
.LEHB93:
	call	_Unwind_Resume@PLT
.LEHE93:
	.cfi_endproc
.LFE10777:
	.section	.gcc_except_table._ZNSt10_HashtableIN2v88internal6torque9UnionTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb1EEEm,"aG",@progbits,_ZNSt10_HashtableIN2v88internal6torque9UnionTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb1EEEm,comdat
	.align 4
.LLSDA10777:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATT10777-.LLSDATTD10777
.LLSDATTD10777:
	.byte	0x1
	.uleb128 .LLSDACSE10777-.LLSDACSB10777
.LLSDACSB10777:
	.uleb128 .LEHB90-.LFB10777
	.uleb128 .LEHE90-.LEHB90
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB91-.LFB10777
	.uleb128 .LEHE91-.LEHB91
	.uleb128 .L764-.LFB10777
	.uleb128 0x1
	.uleb128 .LEHB92-.LFB10777
	.uleb128 .LEHE92-.LEHB92
	.uleb128 .L765-.LFB10777
	.uleb128 0
	.uleb128 .LEHB93-.LFB10777
	.uleb128 .LEHE93-.LEHB93
	.uleb128 0
	.uleb128 0
.LLSDACSE10777:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATT10777:
	.section	.text._ZNSt10_HashtableIN2v88internal6torque9UnionTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb1EEEm,"axG",@progbits,_ZNSt10_HashtableIN2v88internal6torque9UnionTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb1EEEm,comdat
	.size	_ZNSt10_HashtableIN2v88internal6torque9UnionTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb1EEEm, .-_ZNSt10_HashtableIN2v88internal6torque9UnionTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb1EEEm
	.section	.text._ZNSt10_HashtableIN2v88internal6torque9UnionTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_insertIS3_NS5_10_AllocNodeISaINS5_10_Hash_nodeIS3_Lb1EEEEEEEESt4pairINS5_14_Node_iteratorIS3_Lb1ELb1EEEbEOT_RKT0_St17integral_constantIbLb1EEm,"axG",@progbits,_ZNSt10_HashtableIN2v88internal6torque9UnionTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_insertIS3_NS5_10_AllocNodeISaINS5_10_Hash_nodeIS3_Lb1EEEEEEEESt4pairINS5_14_Node_iteratorIS3_Lb1ELb1EEEbEOT_RKT0_St17integral_constantIbLb1EEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIN2v88internal6torque9UnionTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_insertIS3_NS5_10_AllocNodeISaINS5_10_Hash_nodeIS3_Lb1EEEEEEEESt4pairINS5_14_Node_iteratorIS3_Lb1ELb1EEEbEOT_RKT0_St17integral_constantIbLb1EEm
	.type	_ZNSt10_HashtableIN2v88internal6torque9UnionTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_insertIS3_NS5_10_AllocNodeISaINS5_10_Hash_nodeIS3_Lb1EEEEEEEESt4pairINS5_14_Node_iteratorIS3_Lb1ELb1EEEbEOT_RKT0_St17integral_constantIbLb1EEm, @function
_ZNSt10_HashtableIN2v88internal6torque9UnionTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_insertIS3_NS5_10_AllocNodeISaINS5_10_Hash_nodeIS3_Lb1EEEEEEEESt4pairINS5_14_Node_iteratorIS3_Lb1ELb1EEEbEOT_RKT0_St17integral_constantIbLb1EEm:
.LFB10390:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	80(%rsi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	xorl	%r13d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$56, %rsp
	movq	96(%rsi), %r14
	movq	%rcx, -88(%rbp)
	movq	%rax, -56(%rbp)
	cmpq	%rax, %r14
	je	.L773
	.p2align 4,,10
	.p2align 3
.L774:
	movq	32(%r14), %r15
	movq	%r13, %rdi
	call	_ZN2v84base10hash_valueEm@PLT
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZN2v84base10hash_valueEm@PLT
	xorl	%edi, %edi
	movq	%rax, %rsi
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	%r14, %rdi
	movq	%rax, %r13
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r14
	cmpq	%rax, -56(%rbp)
	jne	.L774
.L773:
	movq	8(%r12), %rsi
	movq	%r13, %rax
	xorl	%edx, %edx
	divq	%rsi
	movq	(%r12), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r14
	testq	%rax, %rax
	je	.L775
	movq	(%rax), %r15
	movq	128(%r15), %rdi
	jmp	.L780
	.p2align 4,,10
	.p2align 3
.L776:
	movq	(%r15), %r15
	testq	%r15, %r15
	je	.L775
	movq	128(%r15), %rdi
	xorl	%edx, %edx
	movq	%rdi, %rax
	divq	%rsi
	cmpq	%rdx, %r14
	jne	.L775
.L780:
	cmpq	%rdi, %r13
	jne	.L776
	movq	120(%r15), %rax
	cmpq	%rax, 112(%rbx)
	jne	.L776
	movq	104(%r15), %r10
	movq	96(%rbx), %r8
	cmpq	%r8, -56(%rbp)
	je	.L777
	.p2align 4,,10
	.p2align 3
.L779:
	movq	%r10, -80(%rbp)
	movq	32(%r10), %rax
	cmpq	%rax, 32(%r8)
	jne	.L776
	movq	%r8, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	-80(%rbp), %r10
	movq	%rax, -64(%rbp)
	movq	%r10, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	-64(%rbp), %r8
	cmpq	%r8, -56(%rbp)
	movq	-72(%rbp), %rsi
	movq	%rax, %r10
	jne	.L779
.L777:
	addq	$56, %rsp
	movq	%r15, %rax
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L775:
	.cfi_restore_state
	movl	$136, %edi
	call	_Znwm@PLT
	movq	$0, (%rax)
	movq	%rax, %rcx
	movl	8(%rbx), %eax
	movl	%eax, 16(%rcx)
	leaq	16+_ZTVN2v88internal6torque4TypeE(%rip), %rax
	movq	%rax, 8(%rcx)
	movq	16(%rbx), %rax
	movq	%rax, 24(%rcx)
	movq	40(%rbx), %rax
	testq	%rax, %rax
	je	.L781
	movl	32(%rbx), %edx
	movq	%rax, 48(%rcx)
	movl	%edx, 40(%rcx)
	movq	48(%rbx), %rdx
	movq	%rdx, 56(%rcx)
	movq	56(%rbx), %rdx
	movq	%rdx, 64(%rcx)
	leaq	40(%rcx), %rdx
	movq	%rdx, 8(%rax)
	movq	64(%rbx), %rax
	movq	$0, 40(%rbx)
	movq	%rax, 72(%rcx)
	leaq	32(%rbx), %rax
	movq	%rax, 48(%rbx)
	movq	%rax, 56(%rbx)
	movq	$0, 64(%rbx)
.L782:
	leaq	16+_ZTVN2v88internal6torque9UnionTypeE(%rip), %rax
	leaq	88(%rcx), %rdx
	movq	%rax, 8(%rcx)
	movq	88(%rbx), %rax
	testq	%rax, %rax
	je	.L783
	movl	80(%rbx), %esi
	movq	%rax, 96(%rcx)
	movl	%esi, 88(%rcx)
	movq	96(%rbx), %rsi
	movq	%rsi, 104(%rcx)
	movq	104(%rbx), %rsi
	movq	%rsi, 112(%rcx)
	movq	%rdx, 8(%rax)
	movq	112(%rbx), %rax
	movq	$0, 88(%rbx)
	movq	%rax, 120(%rcx)
	movq	-56(%rbp), %rax
	movq	$0, 112(%rbx)
	movq	%rax, 96(%rbx)
	movq	%rax, 104(%rbx)
.L784:
	movq	-88(%rbp), %r8
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZNSt10_HashtableIN2v88internal6torque9UnionTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb1EEEm
	addq	$56, %rsp
	movl	$1, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L783:
	.cfi_restore_state
	movl	$0, 88(%rcx)
	movq	$0, 96(%rcx)
	movq	%rdx, 104(%rcx)
	movq	%rdx, 112(%rcx)
	movq	$0, 120(%rcx)
	jmp	.L784
	.p2align 4,,10
	.p2align 3
.L781:
	leaq	40(%rcx), %rax
	movl	$0, 40(%rcx)
	movq	$0, 48(%rcx)
	movq	%rax, 56(%rcx)
	movq	%rax, 64(%rcx)
	movq	$0, 72(%rcx)
	jmp	.L782
	.cfi_endproc
.LFE10390:
	.size	_ZNSt10_HashtableIN2v88internal6torque9UnionTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_insertIS3_NS5_10_AllocNodeISaINS5_10_Hash_nodeIS3_Lb1EEEEEEEESt4pairINS5_14_Node_iteratorIS3_Lb1ELb1EEEbEOT_RKT0_St17integral_constantIbLb1EEm, .-_ZNSt10_HashtableIN2v88internal6torque9UnionTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_insertIS3_NS5_10_AllocNodeISaINS5_10_Hash_nodeIS3_Lb1EEEEEEEESt4pairINS5_14_Node_iteratorIS3_Lb1ELb1EEEbEOT_RKT0_St17integral_constantIbLb1EEm
	.section	.rodata._ZN2v88internal6torque5Block13SetInputTypesERKNS1_5StackIPKNS1_4TypeEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC13:
	.string	"incompatible types at branch:\n"
	.section	.rodata._ZN2v88internal6torque5Block13SetInputTypesERKNS1_5StackIPKNS1_4TypeEEE.str1.1,"aMS",@progbits,1
.LC14:
	.string	"\n"
.LC15:
	.string	"/*missing*/"
.LC16:
	.string	"   =>   "
	.section	.text.unlikely._ZN2v88internal6torque5Block13SetInputTypesERKNS1_5StackIPKNS1_4TypeEEE,"ax",@progbits
	.align 2
.LCOLDB18:
	.section	.text._ZN2v88internal6torque5Block13SetInputTypesERKNS1_5StackIPKNS1_4TypeEEE,"ax",@progbits
.LHOTB18:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque5Block13SetInputTypesERKNS1_5StackIPKNS1_4TypeEEE
	.type	_ZN2v88internal6torque5Block13SetInputTypesERKNS1_5StackIPKNS1_4TypeEEE, @function
_ZN2v88internal6torque5Block13SetInputTypesERKNS1_5StackIPKNS1_4TypeEEE:
.LFB6735:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6735
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$952, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -920(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	32(%rdi), %eax
	movb	%al, -929(%rbp)
	testb	%al, %al
	je	.L1055
	movq	48(%rdi), %rax
	movq	40(%rdi), %rbx
	movq	%rax, -960(%rbp)
	subq	%rbx, %rax
	movq	%rax, %rdx
	movq	-920(%rbp), %rax
	movq	(%rax), %rcx
	movq	8(%rax), %rax
	movq	%rax, -928(%rbp)
	subq	%rcx, %rax
	movq	%rcx, -944(%rbp)
	cmpq	%rax, %rdx
	je	.L1056
	movq	$0, -880(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -896(%rbp)
	cmpq	-960(%rbp), %rbx
	je	.L809
.L921:
	leaq	-664(%rbp), %rax
	movq	%rbx, -928(%rbp)
	movq	%rax, -992(%rbp)
	leaq	-840(%rbp), %rax
	movb	$0, -930(%rbp)
	movq	%rax, -984(%rbp)
.L863:
	movq	-928(%rbp), %rax
	addq	$8, -944(%rbp)
	movq	(%rax), %r14
	movq	-944(%rbp), %rax
	movq	-8(%rax), %r12
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%r12, %rsi
.LEHB94:
	call	*16(%rax)
	testb	%al, %al
	jne	.L810
	movq	(%r12), %rax
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	*16(%rax)
	testb	%al, %al
	jne	.L923
	cmpl	$3, 8(%r14)
	movl	$3, -856(%rbp)
	jne	.L811
	leaq	-832(%rbp), %rdx
	leaq	16+_ZTVN2v88internal6torque4TypeE(%rip), %rax
	movq	%rax, -864(%rbp)
	movq	16(%r14), %rax
	movq	$0, -824(%rbp)
	movq	%rdx, -816(%rbp)
	movq	%rdx, -808(%rbp)
	movq	$0, -800(%rbp)
	movq	40(%r14), %rsi
	movq	%rax, -848(%rbp)
	movl	$0, -832(%rbp)
	movq	%rdx, -976(%rbp)
	testq	%rsi, %rsi
	je	.L812
	movq	-984(%rbp), %rdi
	leaq	-608(%rbp), %r13
	movq	%r13, %rcx
	movq	%rdi, -608(%rbp)
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE7_M_copyINSB_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS5_EPKSF_PSt18_Rb_tree_node_baseRT_
.LEHE94:
	movq	%rax, %rcx
.L813:
	movq	%rax, %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L813
	movq	%rdx, -816(%rbp)
	movq	%rcx, %rax
.L814:
	movq	%rax, %rdx
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L814
	movq	64(%r14), %rax
	movq	%rdx, -808(%rbp)
	movq	%rcx, -824(%rbp)
	movq	%rax, -800(%rbp)
.L812:
	leaq	-784(%rbp), %rdx
	leaq	16+_ZTVN2v88internal6torque9UnionTypeE(%rip), %rax
	movq	$0, -776(%rbp)
	movq	%rdx, -768(%rbp)
	movq	%rdx, -760(%rbp)
	movq	$0, -752(%rbp)
	movq	88(%r14), %rsi
	movq	%rax, -864(%rbp)
	movl	$0, -784(%rbp)
	movq	%rdx, -952(%rbp)
	testq	%rsi, %rsi
	je	.L815
	leaq	-608(%rbp), %r13
	leaq	-792(%rbp), %rdi
	movq	%r13, %rcx
	movq	%rdi, -608(%rbp)
.LEHB95:
	call	_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE7_M_copyINSA_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS5_EPKSE_PSt18_Rb_tree_node_baseRT_
.LEHE95:
	movq	%rax, %rcx
.L816:
	movq	%rax, %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L816
	movq	%rdx, -768(%rbp)
	movq	%rcx, %rax
.L817:
	movq	%rax, %rdx
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L817
	movq	112(%r14), %rax
	movq	%rdx, -760(%rbp)
	movq	%rcx, -776(%rbp)
	movq	%rax, -752(%rbp)
.L815:
	movq	%r12, -608(%rbp)
	cmpl	$3, 8(%r12)
	jne	.L1057
	movq	96(%r12), %r13
	addq	$80, %r12
	leaq	-864(%rbp), %rbx
	cmpq	%r12, %r13
	je	.L1044
.L823:
	movq	32(%r13), %rsi
	movq	%rbx, %rdi
.LEHB96:
	call	_ZN2v88internal6torque9UnionType6ExtendEPKNS1_4TypeE
.LEHE96:
	movq	%r13, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r13
	cmpq	%rax, %r12
	jne	.L823
.L1044:
	movq	-848(%rbp), %rdi
.L824:
	movl	-856(%rbp), %eax
	movq	%rdi, -720(%rbp)
	movl	%eax, -728(%rbp)
	movq	-824(%rbp), %rax
	testq	%rax, %rax
	je	.L828
	movl	-832(%rbp), %edx
	leaq	-704(%rbp), %rbx
	movq	%rax, -696(%rbp)
	movl	%edx, -704(%rbp)
	movq	-816(%rbp), %rdx
	movq	%rdx, -688(%rbp)
	movq	-808(%rbp), %rdx
	movq	%rdx, -680(%rbp)
	movq	%rbx, 8(%rax)
	movq	-800(%rbp), %rax
	movq	$0, -824(%rbp)
	movq	%rax, -672(%rbp)
	movq	-976(%rbp), %rax
	movq	$0, -800(%rbp)
	movq	%rax, -816(%rbp)
	movq	%rax, -808(%rbp)
.L829:
	leaq	16+_ZTVN2v88internal6torque9UnionTypeE(%rip), %rax
	movq	%rax, -736(%rbp)
	movq	-776(%rbp), %rax
	testq	%rax, %rax
	je	.L830
	movl	-784(%rbp), %edx
	leaq	-656(%rbp), %r12
	movq	%rax, -648(%rbp)
	movq	-952(%rbp), %rcx
	movl	%edx, -656(%rbp)
	movq	-768(%rbp), %rdx
	movq	%rdx, -640(%rbp)
	movq	-760(%rbp), %rdx
	movq	%rdx, -632(%rbp)
	movq	%r12, 8(%rax)
	movq	-752(%rbp), %rax
	movq	$0, -776(%rbp)
	movq	%rax, -624(%rbp)
	movq	%rcx, -768(%rbp)
	movq	%rcx, -760(%rbp)
	movq	$0, -752(%rbp)
	cmpq	$1, %rax
	jne	.L832
	movq	-640(%rbp), %rax
	movq	32(%rax), %r12
.L833:
	movq	-648(%rbp), %rbx
	leaq	16+_ZTVN2v88internal6torque9UnionTypeE(%rip), %rax
	movq	-992(%rbp), %r13
	movq	%rax, -736(%rbp)
	testq	%rbx, %rbx
	je	.L849
.L844:
	movq	24(%rbx), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	%rbx, %rdi
	movq	16(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L844
.L849:
	movq	-696(%rbp), %r13
	leaq	16+_ZTVN2v88internal6torque4TypeE(%rip), %rax
	movq	%rax, -736(%rbp)
	testq	%r13, %r13
	je	.L845
	movq	%r12, -952(%rbp)
	leaq	-712(%rbp), %rbx
.L846:
	movq	24(%r13), %rsi
	movq	%rbx, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r13), %rdi
	leaq	48(%r13), %rdx
	movq	16(%r13), %r12
	cmpq	%rdx, %rdi
	je	.L852
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%r12, %r12
	je	.L1040
.L853:
	movq	%r12, %r13
	jmp	.L846
.L856:
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L857
.L1042:
	movq	-952(%rbp), %r12
.L810:
	movq	(%r12), %rax
	movq	%r14, %rsi
	movq	%r12, %rdi
.LEHB97:
	call	*16(%rax)
.LEHE97:
	testb	%al, %al
	movzbl	-929(%rbp), %ecx
	movzbl	-930(%rbp), %eax
	movq	%r12, -736(%rbp)
	movq	-888(%rbp), %rsi
	cmove	%ecx, %eax
	movb	%al, -930(%rbp)
	cmpq	-880(%rbp), %rsi
	je	.L1058
	movq	%r12, (%rsi)
	addq	$8, -888(%rbp)
.L862:
	addq	$8, -928(%rbp)
	movq	-928(%rbp), %rax
	cmpq	%rax, -960(%rbp)
	jne	.L863
	movq	-888(%rbp), %rdx
	movq	-896(%rbp), %rax
	movq	48(%r15), %rcx
	subq	40(%r15), %rcx
	movq	%rdx, %rbx
	subq	%rax, %rbx
	cmpq	%rcx, %rbx
	jne	.L864
	cmpb	$0, -930(%rbp)
	jne	.L1059
.L865:
	testq	%rax, %rax
	je	.L798
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L798:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1060
	addq	$952, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L852:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L853
.L1040:
	movq	-952(%rbp), %r12
.L845:
	movq	-776(%rbp), %rbx
	leaq	16+_ZTVN2v88internal6torque9UnionTypeE(%rip), %rax
	leaq	-792(%rbp), %r13
	movq	%rax, -864(%rbp)
	testq	%rbx, %rbx
	je	.L850
.L851:
	movq	24(%rbx), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	%rbx, %rdi
	movq	16(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L851
.L850:
	movq	-824(%rbp), %r13
	leaq	16+_ZTVN2v88internal6torque4TypeE(%rip), %rax
	movq	%rax, -864(%rbp)
	testq	%r13, %r13
	je	.L810
	movq	%r12, -952(%rbp)
	movq	-984(%rbp), %rbx
.L855:
	movq	24(%r13), %rsi
	movq	%rbx, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r13), %rdi
	leaq	48(%r13), %rdx
	movq	16(%r13), %r12
	cmpq	%rdx, %rdi
	je	.L856
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%r12, %r12
	je	.L1042
.L857:
	movq	%r12, %r13
	jmp	.L855
.L923:
	movq	%r14, %r12
	jmp	.L810
.L830:
	leaq	-656(%rbp), %r12
	movl	$0, -656(%rbp)
	movq	$0, -648(%rbp)
	movq	%r12, -640(%rbp)
	movq	%r12, -632(%rbp)
	movq	$0, -624(%rbp)
.L832:
.LEHB98:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_10TypeOracleEEERPNT_5ScopeEv@PLT
.LEHE98:
	movq	(%rax), %rdi
	movl	-728(%rbp), %eax
	movl	%eax, -600(%rbp)
	movq	-720(%rbp), %rax
	movq	%rax, -592(%rbp)
	movq	-696(%rbp), %rax
	testq	%rax, %rax
	je	.L834
	movl	-704(%rbp), %edx
	movq	%rax, -568(%rbp)
	movl	%edx, -576(%rbp)
	movq	-688(%rbp), %rdx
	movq	%rdx, -560(%rbp)
	movq	-680(%rbp), %rdx
	movq	%rdx, -552(%rbp)
	leaq	-576(%rbp), %rdx
	movq	%rdx, 8(%rax)
	movq	-672(%rbp), %rax
	movq	$0, -696(%rbp)
	movq	%rax, -544(%rbp)
	movq	%rbx, -688(%rbp)
	movq	%rbx, -680(%rbp)
	movq	$0, -672(%rbp)
.L835:
	leaq	16+_ZTVN2v88internal6torque9UnionTypeE(%rip), %rax
	movq	%rax, -608(%rbp)
	movq	-648(%rbp), %rax
	testq	%rax, %rax
	je	.L836
	movl	-656(%rbp), %edx
	movq	%rax, -520(%rbp)
	movl	%edx, -528(%rbp)
	movq	-640(%rbp), %rdx
	movq	%rdx, -512(%rbp)
	movq	-632(%rbp), %rdx
	movq	%rdx, -504(%rbp)
	leaq	-528(%rbp), %rdx
	movq	%rdx, 8(%rax)
	movq	-624(%rbp), %rax
	movq	$0, -648(%rbp)
	movq	%rax, -496(%rbp)
	movq	%r12, -640(%rbp)
	movq	%r12, -632(%rbp)
	movq	$0, -624(%rbp)
.L837:
	leaq	-608(%rbp), %r13
	addq	$80, %rdi
	movl	$1, %ecx
	leaq	-904(%rbp), %rdx
	movq	%r13, %rsi
	movq	%rdi, -904(%rbp)
.LEHB99:
	call	_ZNSt10_HashtableIN2v88internal6torque9UnionTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_insertIS3_NS5_10_AllocNodeISaINS5_10_Hash_nodeIS3_Lb1EEEEEEEESt4pairINS5_14_Node_iteratorIS3_Lb1ELb1EEEbEOT_RKT0_St17integral_constantIbLb1EEm
.LEHE99:
	movq	-520(%rbp), %rbx
	leaq	8(%rax), %r12
	leaq	16+_ZTVN2v88internal6torque9UnionTypeE(%rip), %rax
	movq	%rax, -608(%rbp)
	leaq	-536(%rbp), %r13
	testq	%rbx, %rbx
	je	.L841
.L838:
	movq	24(%rbx), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	%rbx, %rdi
	movq	16(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L838
.L841:
	movq	-568(%rbp), %r13
	leaq	16+_ZTVN2v88internal6torque4TypeE(%rip), %rax
	movq	%rax, -608(%rbp)
	testq	%r13, %r13
	je	.L833
	movq	%r12, -952(%rbp)
	leaq	-584(%rbp), %rbx
.L840:
	movq	24(%r13), %rsi
	movq	%rbx, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r13), %rdi
	leaq	48(%r13), %rdx
	movq	16(%r13), %r12
	cmpq	%rdx, %rdi
	je	.L842
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%r12, %r12
	je	.L1038
.L843:
	movq	%r12, %r13
	jmp	.L840
.L1058:
	leaq	-736(%rbp), %rdx
	leaq	-896(%rbp), %rdi
.LEHB100:
	call	_ZNSt6vectorIPKN2v88internal6torque4TypeESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
.LEHE100:
	jmp	.L862
.L842:
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L843
.L1038:
	movq	-952(%rbp), %r12
	jmp	.L833
.L1057:
	movq	(%r12), %rax
	leaq	-864(%rbp), %rbx
	movq	%r12, %rdi
	movq	%rbx, %rsi
.LEHB101:
	call	*16(%rax)
	movq	-848(%rbp), %rdi
	testb	%al, %al
	jne	.L824
	movq	-608(%rbp), %rsi
	call	_ZN2v88internal6torque4Type15CommonSupertypeEPKS2_S4_@PLT
	movq	%rax, -848(%rbp)
	movq	-768(%rbp), %r12
	cmpq	-952(%rbp), %r12
	jne	.L825
	jmp	.L826
	.p2align 4,,10
	.p2align 3
.L827:
	cmpq	-952(%rbp), %r13
	je	.L826
.L1061:
	movq	%r13, %r12
.L825:
	movq	32(%r12), %rdi
	movq	-608(%rbp), %rsi
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	%r12, %rdi
	movb	%al, -968(%rbp)
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movzbl	-968(%rbp), %edx
	movq	%rax, %r13
	testb	%dl, %dl
	je	.L827
	movq	-952(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZSt28_Rb_tree_rebalance_for_erasePSt18_Rb_tree_node_baseRS_@PLT
	movq	%rax, %rdi
	call	_ZdlPv@PLT
	subq	$1, -752(%rbp)
	cmpq	-952(%rbp), %r13
	jne	.L1061
.L826:
	leaq	-608(%rbp), %r13
	leaq	-792(%rbp), %rax
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE16_M_insert_uniqueIRKS5_EESt4pairISt17_Rb_tree_iteratorIS5_EbEOT_
.LEHE101:
	jmp	.L1044
.L811:
	leaq	-832(%rbp), %rax
	leaq	-784(%rbp), %rbx
	movq	%r14, -848(%rbp)
	movq	%rax, -976(%rbp)
	leaq	-792(%rbp), %rdi
	movq	%rbx, %rsi
	leaq	-448(%rbp), %rdx
	movq	%rax, -816(%rbp)
	movq	%rax, -808(%rbp)
	leaq	16+_ZTVN2v88internal6torque9UnionTypeE(%rip), %rax
	movl	$0, -832(%rbp)
	movq	$0, -824(%rbp)
	movq	$0, -800(%rbp)
	movq	%rax, -864(%rbp)
	movq	%r14, -448(%rbp)
	movl	$0, -784(%rbp)
	movq	$0, -776(%rbp)
	movq	%rbx, -952(%rbp)
	movq	%rbx, -768(%rbp)
	movq	%rbx, -760(%rbp)
	movq	$0, -752(%rbp)
	movq	%rdi, -968(%rbp)
.LEHB102:
	call	_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS5_ERKS5_
	movq	%rdx, %r13
	testq	%rdx, %rdx
	je	.L815
	testq	%rax, %rax
	setne	%al
	cmpq	%rbx, %rdx
	sete	%bl
	orb	%al, %bl
	je	.L1062
.L821:
	movl	$40, %edi
	call	_Znwm@PLT
	movq	%rax, %rsi
	movq	-448(%rbp), %rax
	movzbl	%bl, %edi
	movq	%r13, %rdx
	movq	-952(%rbp), %rcx
	movq	%rax, 32(%rsi)
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, -752(%rbp)
	jmp	.L815
.L828:
	leaq	-704(%rbp), %rbx
	movl	$0, -704(%rbp)
	movq	$0, -696(%rbp)
	movq	%rbx, -688(%rbp)
	movq	%rbx, -680(%rbp)
	movq	$0, -672(%rbp)
	jmp	.L829
.L1062:
	movq	32(%rdx), %rsi
	movq	-448(%rbp), %rdi
	call	_ZN2v88internal6torqueltERKNS1_4TypeES4_@PLT
.LEHE102:
	movl	%eax, %ebx
	jmp	.L821
.L1056:
	testq	%rdx, %rdx
	je	.L798
	movq	%rcx, %rsi
	movq	%rbx, %rdi
	call	memcmp@PLT
	testl	%eax, %eax
	je	.L798
	movq	$0, -880(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -896(%rbp)
	cmpq	-960(%rbp), %rbx
	jne	.L921
.L864:
	leaq	-320(%rbp), %r13
	leaq	-448(%rbp), %r14
	movq	%r13, %rdi
	movq	%r14, -944(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	xorl	%esi, %esi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	pxor	%xmm0, %xmm0
	xorl	%eax, %eax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rbx, -448(%rbp)
	movq	$0, -104(%rbp)
	movw	%ax, -96(%rbp)
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	-24(%rbx), %rax
	movq	%rcx, -448(%rbp,%rax)
	movq	-448(%rbp), %rax
	movq	$0, -440(%rbp)
	movq	-24(%rax), %rdi
	addq	%r14, %rdi
.LEHB103:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE103:
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	leaq	-432(%rbp), %r12
	xorl	%esi, %esi
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -432(%rbp,%rax)
	movq	-432(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
.LEHB104:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE104:
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	leaq	-368(%rbp), %r14
	movq	%r14, %rdi
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movq	%rax, -320(%rbp)
	subq	$40, %rax
	movq	%rax, %xmm0
	movhps	.LC17(%rip), %xmm0
	movaps	%xmm0, -432(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r13, %rdi
	leaq	-424(%rbp), %rsi
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movl	$24, -360(%rbp)
	movq	%rax, -928(%rbp)
	movq	%rax, -352(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
.LEHB105:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE105:
	movl	$30, %edx
	leaq	.LC13(%rip), %rsi
	movq	%r12, %rdi
.LEHB106:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-920(%rbp), %rax
	movq	48(%r15), %rdx
	movq	40(%r15), %rsi
	movq	(%rax), %rcx
	movq	8(%rax), %rax
	movq	%rdx, %rbx
	subq	%rsi, %rbx
	movq	%rax, %rdi
	sarq	$3, %rbx
	subq	%rcx, %rdi
	sarq	$3, %rdi
	cmpq	%rdi, %rbx
	cmovb	%rdi, %rbx
	subq	$1, %rbx
	js	.L888
	leaq	-480(%rbp), %rdi
	movq	%rdi, -928(%rbp)
	jmp	.L908
	.p2align 4,,10
	.p2align 3
.L1063:
	movq	(%rcx,%rbx,8), %r8
	cmpq	%rbx, %rdx
	ja	.L890
	xorl	%r13d, %r13d
	xorl	%r14d, %r14d
.L891:
	movq	-928(%rbp), %rdi
	movq	%r8, %rsi
	call	_ZNK2v88internal6torque4Type8ToStringB5cxx11Ev@PLT
.LEHE106:
	movq	-472(%rbp), %rdx
	movq	-480(%rbp), %rsi
	movq	%r12, %rdi
.LEHB107:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE107:
	movq	-480(%rbp), %rdi
	leaq	-464(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L899
	call	_ZdlPv@PLT
.L899:
	movl	$8, %edx
	leaq	.LC16(%rip), %rsi
	movq	%r12, %rdi
.LEHB108:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	testb	%r13b, %r13b
	je	.L903
	movq	-928(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZNK2v88internal6torque4Type8ToStringB5cxx11Ev@PLT
.LEHE108:
	movq	-472(%rbp), %rdx
	movq	-480(%rbp), %rsi
	movq	%r12, %rdi
.LEHB109:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE109:
.L1054:
	movq	-480(%rbp), %rdi
	leaq	-464(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L905
	call	_ZdlPv@PLT
.L905:
	movl	$1, %edx
	leaq	.LC14(%rip), %rsi
	movq	%r12, %rdi
.LEHB110:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	subq	$1, %rbx
	cmpq	$-1, %rbx
	je	.L888
	movq	-920(%rbp), %rax
	movq	40(%r15), %rsi
	movq	48(%r15), %rdx
	movq	(%rax), %rcx
	movq	8(%rax), %rax
.L908:
	subq	%rcx, %rax
	subq	%rsi, %rdx
	sarq	$3, %rax
	sarq	$3, %rdx
	cmpq	%rbx, %rax
	ja	.L1063
	cmpq	%rbx, %rdx
	ja	.L892
	xorl	%r13d, %r13d
	xorl	%r14d, %r14d
.L893:
	movl	$11, %edx
	leaq	.LC15(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L899
	.p2align 4,,10
	.p2align 3
.L903:
	movl	$11, %edx
	leaq	.LC15(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L905
	.p2align 4,,10
	.p2align 3
.L890:
	movq	(%rsi,%rbx,8), %r14
	cmpq	%r8, %r14
	je	.L920
	movzbl	-929(%rbp), %r13d
	jmp	.L891
	.p2align 4,,10
	.p2align 3
.L892:
	movq	(%rsi,%rbx,8), %r14
	movzbl	-929(%rbp), %r13d
	jmp	.L893
	.p2align 4,,10
	.p2align 3
.L920:
	movq	-928(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZNK2v88internal6torque4Type8ToStringB5cxx11Ev@PLT
.LEHE110:
	movq	-472(%rbp), %rdx
	movq	-480(%rbp), %rsi
	movq	%r12, %rdi
.LEHB111:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE111:
	jmp	.L1054
.L888:
	movq	-384(%rbp), %rax
	leaq	-464(%rbp), %rbx
	movq	$0, -472(%rbp)
	leaq	-480(%rbp), %r12
	movq	%rbx, -480(%rbp)
	movb	$0, -464(%rbp)
	testq	%rax, %rax
	je	.L909
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	ja	.L1064
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
.LEHB112:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.LEHE112:
.L911:
	movq	%r12, %rdi
.LEHB113:
	call	_ZN2v88internal6torque11ReportErrorIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_
.LEHE113:
.L1055:
	movq	8(%rsi), %rbx
	subq	(%rsi), %rbx
	pxor	%xmm0, %xmm0
	movq	$0, 56(%rdi)
	movq	%rbx, %rdx
	movups	%xmm0, 40(%rdi)
	sarq	$3, %rdx
	je	.L1065
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L1066
	movq	%rbx, %rdi
.LEHB114:
	call	_Znwm@PLT
.LEHE114:
	movq	%rax, %rcx
.L801:
	addq	%rcx, %rbx
	movq	%rcx, %xmm0
	movq	%rbx, 56(%r15)
	movq	-920(%rbp), %rbx
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 40(%r15)
	movq	8(%rbx), %rax
	movq	(%rbx), %rsi
	movq	%rax, %rbx
	subq	%rsi, %rbx
	cmpq	%rsi, %rax
	je	.L804
	movq	%rcx, %rdi
	movq	%rbx, %rdx
	call	memmove@PLT
	movq	%rax, %rcx
.L804:
	addq	%rbx, %rcx
	movb	$1, 32(%r15)
	movq	%rcx, 48(%r15)
	jmp	.L798
.L1064:
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r8
.LEHB115:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.LEHE115:
	jmp	.L911
.L1065:
	xorl	%ecx, %ecx
	jmp	.L801
.L1059:
	cmpb	$0, 32(%r15)
	jne	.L1067
	movq	%rbx, %rcx
	pxor	%xmm0, %xmm0
	movq	$0, 56(%r15)
	sarq	$3, %rcx
	movups	%xmm0, 40(%r15)
	movq	%rcx, %r13
	je	.L1068
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rcx
	ja	.L1069
	movq	%rbx, %rdi
	movq	%rbx, %r12
.LEHB116:
	call	_Znwm@PLT
	movq	-888(%rbp), %rdx
	movq	%rax, %r8
	movq	-896(%rbp), %rax
	movq	%rdx, %rbx
	subq	%rax, %rbx
	movq	%rbx, %r13
	sarq	$3, %r13
.L869:
	movq	%r8, %xmm0
	addq	%r8, %r12
	punpcklqdq	%xmm0, %xmm0
	movq	%r12, 56(%r15)
	movups	%xmm0, 40(%r15)
	cmpq	%rdx, %rax
	je	.L871
	movq	%r8, %rdi
	movq	%rbx, %rdx
	movq	%rax, %rsi
	call	memmove@PLT
	movq	%rax, %r8
.L871:
	leaq	(%r8,%rbx), %rax
	movb	$1, 32(%r15)
	movq	%rax, 48(%r15)
.L867:
	pxor	%xmm0, %xmm0
	movq	%rbx, %rdx
	xorl	%edi, %edi
	movq	$0, -592(%rbp)
	movaps	%xmm0, -608(%rbp)
	testq	%r13, %r13
	je	.L873
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %r13
	ja	.L1070
	movq	%rbx, %rdi
	call	_Znwm@PLT
.LEHE116:
	movq	%rax, %rdi
	movq	48(%r15), %rax
	movq	40(%r15), %r8
	movq	%rax, %rdx
	subq	%r8, %rdx
.L873:
	movq	%rdi, %xmm0
	addq	%rdi, %rbx
	punpcklqdq	%xmm0, %xmm0
	movq	%rbx, -592(%rbp)
	leaq	(%rdi,%rdx), %rbx
	movaps	%xmm0, -608(%rbp)
	cmpq	%r8, %rax
	je	.L875
	movq	%r8, %rsi
	call	memmove@PLT
	movq	%rbx, -600(%rbp)
	movq	16(%r15), %r12
	movq	8(%r15), %rbx
	movq	%rax, %rdi
	cmpq	%r12, %rbx
	je	.L877
.L876:
	leaq	-608(%rbp), %r13
.L879:
	movq	8(%rbx), %rdi
	movq	(%r15), %rdx
	movq	%r13, %rsi
	movq	(%rdi), %rax
.LEHB117:
	call	*32(%rax)
.LEHE117:
	addq	$16, %rbx
	cmpq	%r12, %rbx
	jne	.L879
	movq	-608(%rbp), %rdi
.L878:
	testq	%rdi, %rdi
	je	.L880
.L877:
	call	_ZdlPv@PLT
.L880:
	movq	-896(%rbp), %rax
	jmp	.L865
.L809:
	testq	%rdx, %rdx
	je	.L798
	jmp	.L864
.L836:
	leaq	-528(%rbp), %rax
	movl	$0, -528(%rbp)
	movq	$0, -520(%rbp)
	movq	%rax, -512(%rbp)
	movq	%rax, -504(%rbp)
	movq	$0, -496(%rbp)
	jmp	.L837
.L834:
	leaq	-576(%rbp), %rax
	movl	$0, -576(%rbp)
	movq	$0, -568(%rbp)
	movq	%rax, -560(%rbp)
	movq	%rax, -552(%rbp)
	movq	$0, -544(%rbp)
	jmp	.L835
.L1067:
	leaq	-896(%rbp), %rsi
	leaq	40(%r15), %rdi
.LEHB118:
	call	_ZNSt6vectorIPKN2v88internal6torque4TypeESaIS5_EEaSERKS7_
.LEHE118:
	movq	48(%r15), %rax
	movq	40(%r15), %r8
	movq	%rax, %rbx
	subq	%r8, %rbx
	movq	%rbx, %r13
	sarq	$3, %r13
	jmp	.L867
.L875:
	movq	%rbx, -600(%rbp)
	movq	16(%r15), %r12
	movq	8(%r15), %rbx
	cmpq	%r12, %rbx
	jne	.L876
	jmp	.L878
.L909:
	leaq	-352(%rbp), %rsi
	movq	%r12, %rdi
.LEHB119:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
.LEHE119:
	jmp	.L911
.L1060:
	call	__stack_chk_fail@PLT
	.p2align 4,,10
	.p2align 3
.L1068:
	movq	%rbx, %r12
	xorl	%r8d, %r8d
	jmp	.L869
.L1069:
.LEHB120:
	call	_ZSt17__throw_bad_allocv@PLT
.L1070:
	call	_ZSt17__throw_bad_allocv@PLT
.LEHE120:
.L1066:
.LEHB121:
	call	_ZSt17__throw_bad_allocv@PLT
.LEHE121:
.L932:
	endbr64
	movq	%rax, %r12
	jmp	.L881
.L926:
	endbr64
	movq	%rax, %r12
	jmp	.L914
.L939:
	endbr64
	movq	%rax, %r12
	jmp	.L914
.L931:
	endbr64
	movq	%rax, %rbx
	jmp	.L847
.L930:
	endbr64
	movq	%rax, %r12
	jmp	.L822
.L929:
	endbr64
	movq	%rax, %r12
	jmp	.L1048
.L927:
	endbr64
	movq	%rax, %r12
	jmp	.L859
.L928:
	endbr64
	movq	%rax, %rbx
	jmp	.L848
.L933:
	endbr64
	movq	%rax, %r12
	jmp	.L885
.L938:
	endbr64
	movq	%rax, %r12
	jmp	.L906
.L936:
	endbr64
	movq	%rax, %r12
	jmp	.L906
.L935:
	endbr64
	movq	%rax, %r12
	jmp	.L884
.L934:
	endbr64
	movq	%rax, %r12
	jmp	.L886
.L925:
	endbr64
	movq	%rax, %r12
	jmp	.L898
.L937:
	endbr64
	movq	%rax, %r12
	jmp	.L906
.L924:
	endbr64
	movq	%rax, %r12
	jmp	.L819
	.section	.gcc_except_table._ZN2v88internal6torque5Block13SetInputTypesERKNS1_5StackIPKNS1_4TypeEEE,"a",@progbits
.LLSDA6735:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6735-.LLSDACSB6735
.LLSDACSB6735:
	.uleb128 .LEHB94-.LFB6735
	.uleb128 .LEHE94-.LEHB94
	.uleb128 .L924-.LFB6735
	.uleb128 0
	.uleb128 .LEHB95-.LFB6735
	.uleb128 .LEHE95-.LEHB95
	.uleb128 .L929-.LFB6735
	.uleb128 0
	.uleb128 .LEHB96-.LFB6735
	.uleb128 .LEHE96-.LEHB96
	.uleb128 .L927-.LFB6735
	.uleb128 0
	.uleb128 .LEHB97-.LFB6735
	.uleb128 .LEHE97-.LEHB97
	.uleb128 .L924-.LFB6735
	.uleb128 0
	.uleb128 .LEHB98-.LFB6735
	.uleb128 .LEHE98-.LEHB98
	.uleb128 .L928-.LFB6735
	.uleb128 0
	.uleb128 .LEHB99-.LFB6735
	.uleb128 .LEHE99-.LEHB99
	.uleb128 .L931-.LFB6735
	.uleb128 0
	.uleb128 .LEHB100-.LFB6735
	.uleb128 .LEHE100-.LEHB100
	.uleb128 .L924-.LFB6735
	.uleb128 0
	.uleb128 .LEHB101-.LFB6735
	.uleb128 .LEHE101-.LEHB101
	.uleb128 .L927-.LFB6735
	.uleb128 0
	.uleb128 .LEHB102-.LFB6735
	.uleb128 .LEHE102-.LEHB102
	.uleb128 .L930-.LFB6735
	.uleb128 0
	.uleb128 .LEHB103-.LFB6735
	.uleb128 .LEHE103-.LEHB103
	.uleb128 .L933-.LFB6735
	.uleb128 0
	.uleb128 .LEHB104-.LFB6735
	.uleb128 .LEHE104-.LEHB104
	.uleb128 .L935-.LFB6735
	.uleb128 0
	.uleb128 .LEHB105-.LFB6735
	.uleb128 .LEHE105-.LEHB105
	.uleb128 .L934-.LFB6735
	.uleb128 0
	.uleb128 .LEHB106-.LFB6735
	.uleb128 .LEHE106-.LEHB106
	.uleb128 .L925-.LFB6735
	.uleb128 0
	.uleb128 .LEHB107-.LFB6735
	.uleb128 .LEHE107-.LEHB107
	.uleb128 .L937-.LFB6735
	.uleb128 0
	.uleb128 .LEHB108-.LFB6735
	.uleb128 .LEHE108-.LEHB108
	.uleb128 .L925-.LFB6735
	.uleb128 0
	.uleb128 .LEHB109-.LFB6735
	.uleb128 .LEHE109-.LEHB109
	.uleb128 .L938-.LFB6735
	.uleb128 0
	.uleb128 .LEHB110-.LFB6735
	.uleb128 .LEHE110-.LEHB110
	.uleb128 .L925-.LFB6735
	.uleb128 0
	.uleb128 .LEHB111-.LFB6735
	.uleb128 .LEHE111-.LEHB111
	.uleb128 .L936-.LFB6735
	.uleb128 0
	.uleb128 .LEHB112-.LFB6735
	.uleb128 .LEHE112-.LEHB112
	.uleb128 .L939-.LFB6735
	.uleb128 0
	.uleb128 .LEHB113-.LFB6735
	.uleb128 .LEHE113-.LEHB113
	.uleb128 .L926-.LFB6735
	.uleb128 0
	.uleb128 .LEHB114-.LFB6735
	.uleb128 .LEHE114-.LEHB114
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB115-.LFB6735
	.uleb128 .LEHE115-.LEHB115
	.uleb128 .L939-.LFB6735
	.uleb128 0
	.uleb128 .LEHB116-.LFB6735
	.uleb128 .LEHE116-.LEHB116
	.uleb128 .L924-.LFB6735
	.uleb128 0
	.uleb128 .LEHB117-.LFB6735
	.uleb128 .LEHE117-.LEHB117
	.uleb128 .L932-.LFB6735
	.uleb128 0
	.uleb128 .LEHB118-.LFB6735
	.uleb128 .LEHE118-.LEHB118
	.uleb128 .L924-.LFB6735
	.uleb128 0
	.uleb128 .LEHB119-.LFB6735
	.uleb128 .LEHE119-.LEHB119
	.uleb128 .L939-.LFB6735
	.uleb128 0
	.uleb128 .LEHB120-.LFB6735
	.uleb128 .LEHE120-.LEHB120
	.uleb128 .L924-.LFB6735
	.uleb128 0
	.uleb128 .LEHB121-.LFB6735
	.uleb128 .LEHE121-.LEHB121
	.uleb128 0
	.uleb128 0
.LLSDACSE6735:
	.section	.text._ZN2v88internal6torque5Block13SetInputTypesERKNS1_5StackIPKNS1_4TypeEEE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque5Block13SetInputTypesERKNS1_5StackIPKNS1_4TypeEEE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6735
	.type	_ZN2v88internal6torque5Block13SetInputTypesERKNS1_5StackIPKNS1_4TypeEEE.cold, @function
_ZN2v88internal6torque5Block13SetInputTypesERKNS1_5StackIPKNS1_4TypeEEE.cold:
.LFSB6735:
.L881:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-608(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L819
	call	_ZdlPv@PLT
.L819:
	movq	-896(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L916
	call	_ZdlPv@PLT
.L916:
	movq	%r12, %rdi
.LEHB122:
	call	_Unwind_Resume@PLT
.LEHE122:
.L914:
	movq	-480(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L898
.L1047:
	call	_ZdlPv@PLT
.L898:
	movq	-944(%rbp), %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	jmp	.L819
.L847:
	movq	%r13, %rdi
	call	_ZN2v88internal6torque9UnionTypeD1Ev
.L848:
	leaq	-736(%rbp), %rdi
	movq	%rbx, %r12
	leaq	-864(%rbp), %rbx
	call	_ZN2v88internal6torque9UnionTypeD1Ev
.L859:
	movq	%rbx, %rdi
	call	_ZN2v88internal6torque9UnionTypeD1Ev
	jmp	.L819
.L822:
	movq	-776(%rbp), %rsi
	movq	-968(%rbp), %rdi
	call	_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
.L1048:
	movq	-824(%rbp), %rsi
	leaq	16+_ZTVN2v88internal6torque4TypeE(%rip), %rax
	leaq	-840(%rbp), %rdi
	movq	%rax, -864(%rbp)
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	jmp	.L819
.L884:
	movq	%rbx, -448(%rbp)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	-24(%rbx), %rax
	movq	%rcx, -448(%rbp,%rax)
	movq	$0, -440(%rbp)
.L885:
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	jmp	.L819
.L906:
	movq	-480(%rbp), %rdi
	leaq	-464(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L1047
	jmp	.L898
.L886:
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-352(%rbp), %rdi
	movq	%rax, -424(%rbp)
	cmpq	-928(%rbp), %rdi
	je	.L887
	call	_ZdlPv@PLT
.L887:
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%r14, %rdi
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -432(%rbp,%rax)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rbx, -448(%rbp)
	movq	-24(%rbx), %rax
	movq	%rcx, -448(%rbp,%rax)
	movq	$0, -440(%rbp)
	jmp	.L885
	.cfi_endproc
.LFE6735:
	.section	.gcc_except_table._ZN2v88internal6torque5Block13SetInputTypesERKNS1_5StackIPKNS1_4TypeEEE
.LLSDAC6735:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6735-.LLSDACSBC6735
.LLSDACSBC6735:
	.uleb128 .LEHB122-.LCOLDB18
	.uleb128 .LEHE122-.LEHB122
	.uleb128 0
	.uleb128 0
.LLSDACSEC6735:
	.section	.text.unlikely._ZN2v88internal6torque5Block13SetInputTypesERKNS1_5StackIPKNS1_4TypeEEE
	.section	.text._ZN2v88internal6torque5Block13SetInputTypesERKNS1_5StackIPKNS1_4TypeEEE
	.size	_ZN2v88internal6torque5Block13SetInputTypesERKNS1_5StackIPKNS1_4TypeEEE, .-_ZN2v88internal6torque5Block13SetInputTypesERKNS1_5StackIPKNS1_4TypeEEE
	.section	.text.unlikely._ZN2v88internal6torque5Block13SetInputTypesERKNS1_5StackIPKNS1_4TypeEEE
	.size	_ZN2v88internal6torque5Block13SetInputTypesERKNS1_5StackIPKNS1_4TypeEEE.cold, .-_ZN2v88internal6torque5Block13SetInputTypesERKNS1_5StackIPKNS1_4TypeEEE.cold
.LCOLDE18:
	.section	.text._ZN2v88internal6torque5Block13SetInputTypesERKNS1_5StackIPKNS1_4TypeEEE
.LHOTE18:
	.section	.text._ZN2v88internal6torque9UnionTypeD0Ev,"axG",@progbits,_ZN2v88internal6torque9UnionTypeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque9UnionTypeD0Ev
	.type	_ZN2v88internal6torque9UnionTypeD0Ev, @function
_ZN2v88internal6torque9UnionTypeD0Ev:
.LFB11782:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal6torque9UnionTypeE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	88(%rdi), %r13
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L1072
	leaq	72(%rdi), %rbx
.L1073:
	movq	24(%r13), %rsi
	movq	%rbx, %rdi
	call	_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	%r13, %rdi
	movq	16(%r13), %r13
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L1073
.L1072:
	movq	40(%r12), %r13
	leaq	16+_ZTVN2v88internal6torque4TypeE(%rip), %rax
	leaq	24(%r12), %r14
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L1074
.L1077:
	movq	24(%r13), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r13), %rdi
	leaq	48(%r13), %rax
	movq	16(%r13), %rbx
	cmpq	%rax, %rdi
	je	.L1075
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L1074
.L1076:
	movq	%rbx, %r13
	jmp	.L1077
	.p2align 4,,10
	.p2align 3
.L1075:
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L1076
.L1074:
	popq	%rbx
	movq	%r12, %rdi
	movl	$120, %esi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11782:
	.size	_ZN2v88internal6torque9UnionTypeD0Ev, .-_ZN2v88internal6torque9UnionTypeD0Ev
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal6torque5Block13SetInputTypesERKNS1_5StackIPKNS1_4TypeEEE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal6torque5Block13SetInputTypesERKNS1_5StackIPKNS1_4TypeEEE, @function
_GLOBAL__sub_I__ZN2v88internal6torque5Block13SetInputTypesERKNS1_5StackIPKNS1_4TypeEEE:
.LFB11829:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE11829:
	.size	_GLOBAL__sub_I__ZN2v88internal6torque5Block13SetInputTypesERKNS1_5StackIPKNS1_4TypeEEE, .-_GLOBAL__sub_I__ZN2v88internal6torque5Block13SetInputTypesERKNS1_5StackIPKNS1_4TypeEEE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal6torque5Block13SetInputTypesERKNS1_5StackIPKNS1_4TypeEEE
	.weak	_ZTVN2v88internal6torque15InstructionBaseE
	.section	.data.rel.ro._ZTVN2v88internal6torque15InstructionBaseE,"awG",@progbits,_ZTVN2v88internal6torque15InstructionBaseE,comdat
	.align 8
	.type	_ZTVN2v88internal6torque15InstructionBaseE, @object
	.size	_ZTVN2v88internal6torque15InstructionBaseE, 72
_ZTVN2v88internal6torque15InstructionBaseE:
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	_ZNK2v88internal6torque15InstructionBase17IsBlockTerminatorEv
	.quad	_ZNK2v88internal6torque15InstructionBase21AppendSuccessorBlocksEPSt6vectorIPNS1_5BlockESaIS5_EE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.data.rel.ro,"aw"
	.align 8
.LC17:
	.quad	_ZTVSt15basic_streambufIcSt11char_traitsIcEE+16
	.hidden	DW.ref.__gxx_personality_v0
	.weak	DW.ref.__gxx_personality_v0
	.section	.data.rel.local.DW.ref.__gxx_personality_v0,"awG",@progbits,DW.ref.__gxx_personality_v0,comdat
	.align 8
	.type	DW.ref.__gxx_personality_v0, @object
	.size	DW.ref.__gxx_personality_v0, 8
DW.ref.__gxx_personality_v0:
	.quad	__gxx_personality_v0
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
