	.file	"server-data.cc"
	.text
	.section	.text._ZN2v88internal6torque21ContextualVariableTopINS1_18LanguageServerDataEEERPNT_5ScopeEv,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal6torque21ContextualVariableTopINS1_18LanguageServerDataEEERPNT_5ScopeEv
	.type	_ZN2v88internal6torque21ContextualVariableTopINS1_18LanguageServerDataEEERPNT_5ScopeEv, @function
_ZN2v88internal6torque21ContextualVariableTopINS1_18LanguageServerDataEEERPNT_5ScopeEv:
.LFB7037:
	.cfi_startproc
	endbr64
	movq	%fs:0, %rax
	addq	$_ZZN2v88internal6torque21ContextualVariableTopINS1_18LanguageServerDataEEERPNT_5ScopeEvE3top@tpoff, %rax
	ret
	.cfi_endproc
.LFE7037:
	.size	_ZN2v88internal6torque21ContextualVariableTopINS1_18LanguageServerDataEEERPNT_5ScopeEv, .-_ZN2v88internal6torque21ContextualVariableTopINS1_18LanguageServerDataEEERPNT_5ScopeEv
	.section	.text._ZN2v88internal6torque18LanguageServerData14FindDefinitionENS1_8SourceIdENS1_13LineAndColumnE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque18LanguageServerData14FindDefinitionENS1_8SourceIdENS1_13LineAndColumnE
	.type	_ZN2v88internal6torque18LanguageServerData14FindDefinitionENS1_8SourceIdENS1_13LineAndColumnE, @function
_ZN2v88internal6torque18LanguageServerData14FindDefinitionENS1_8SourceIdENS1_13LineAndColumnE:
.LFB7039:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	cmpl	$-1, %esi
	je	.L12
	movq	%fs:_ZZN2v88internal6torque21ContextualVariableTopINS1_18LanguageServerDataEEERPNT_5ScopeEvE3top@tpoff, %rcx
	leaq	8(%rcx), %r8
	movq	16(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L12
	movq	%r8, %rdi
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L29:
	movq	%rcx, %rdi
	movq	16(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L8
.L7:
	cmpl	%esi, 32(%rcx)
	jge	.L29
	movq	24(%rcx), %rcx
	testq	%rcx, %rcx
	jne	.L7
.L8:
	cmpq	%rdi, %r8
	je	.L12
	cmpl	%esi, 32(%rdi)
	jg	.L12
	movq	40(%rdi), %rcx
	movq	48(%rdi), %r9
	cmpq	%rcx, %r9
	je	.L12
	movl	%edx, %esi
	shrq	$32, %rdx
.L16:
	movl	4(%rcx), %edi
	movl	12(%rcx), %r8d
	cmpl	%edi, %esi
	jl	.L13
	cmpl	%r8d, %esi
	jg	.L13
	cmpl	8(%rcx), %edx
	jge	.L17
	cmpl	%edi, %esi
	je	.L13
.L17:
	cmpl	16(%rcx), %edx
	jl	.L18
	cmpl	%r8d, %esi
	je	.L13
.L18:
	movdqu	20(%rcx), %xmm0
	movl	36(%rcx), %edx
	movb	$1, (%rax)
	movl	%edx, 20(%rax)
	movups	%xmm0, 4(%rax)
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	addq	$40, %rcx
	cmpq	%rcx, %r9
	jne	.L16
.L12:
	movb	$0, (%rax)
	movb	$0, 4(%rax)
	ret
	.cfi_endproc
.LFE7039:
	.size	_ZN2v88internal6torque18LanguageServerData14FindDefinitionENS1_8SourceIdENS1_13LineAndColumnE, .-_ZN2v88internal6torque18LanguageServerData14FindDefinitionENS1_8SourceIdENS1_13LineAndColumnE
	.section	.rodata._ZNSt6vectorISt4pairIN2v88internal6torque14SourcePositionES4_ESaIS5_EE17_M_realloc_insertIJRS4_S9_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_.str1.1,"aMS",@progbits,1
.LC0:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorISt4pairIN2v88internal6torque14SourcePositionES4_ESaIS5_EE17_M_realloc_insertIJRS4_S9_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_,"axG",@progbits,_ZNSt6vectorISt4pairIN2v88internal6torque14SourcePositionES4_ESaIS5_EE17_M_realloc_insertIJRS4_S9_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorISt4pairIN2v88internal6torque14SourcePositionES4_ESaIS5_EE17_M_realloc_insertIJRS4_S9_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	.type	_ZNSt6vectorISt4pairIN2v88internal6torque14SourcePositionES4_ESaIS5_EE17_M_realloc_insertIJRS4_S9_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_, @function
_ZNSt6vectorISt4pairIN2v88internal6torque14SourcePositionES4_ESaIS5_EE17_M_realloc_insertIJRS4_S9_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_:
.LFB9829:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	8(%rdi), %rsi
	movq	(%rdi), %r15
	movabsq	$-3689348814741910323, %rdi
	movq	%rsi, %rax
	subq	%r15, %rax
	sarq	$3, %rax
	imulq	%rdi, %rax
	movabsq	$230584300921369395, %rdi
	cmpq	%rdi, %rax
	je	.L47
	movq	%r12, %r9
	subq	%r15, %r9
	testq	%rax, %rax
	je	.L39
	movabsq	$9223372036854775800, %rbx
	leaq	(%rax,%rax), %r8
	cmpq	%r8, %rax
	jbe	.L48
.L32:
	movq	%rbx, %rdi
	movq	%rcx, -88(%rbp)
	movq	%rdx, -80(%rbp)
	movq	%r9, -72(%rbp)
	movq	%rsi, -64(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rsi
	movq	-72(%rbp), %r9
	movq	%rax, %r13
	leaq	(%rax,%rbx), %rax
	movq	-80(%rbp), %rdx
	movq	-88(%rbp), %rcx
	movq	%rax, -56(%rbp)
	leaq	40(%r13), %rbx
.L38:
	movdqu	(%rdx), %xmm3
	movl	16(%rdx), %edx
	leaq	0(%r13,%r9), %rax
	movdqu	(%rcx), %xmm4
	movl	%edx, 16(%rax)
	movl	16(%rcx), %edx
	movups	%xmm3, (%rax)
	movl	%edx, 36(%rax)
	movups	%xmm4, 20(%rax)
	cmpq	%r15, %r12
	je	.L34
	movq	%r13, %rdx
	movq	%r15, %rax
	.p2align 4,,10
	.p2align 3
.L35:
	movdqu	(%rax), %xmm1
	addq	$40, %rax
	addq	$40, %rdx
	movups	%xmm1, -40(%rdx)
	movdqu	-24(%rax), %xmm2
	movups	%xmm2, -24(%rdx)
	movq	-8(%rax), %rcx
	movq	%rcx, -8(%rdx)
	cmpq	%rax, %r12
	jne	.L35
	leaq	-40(%r12), %rax
	subq	%r15, %rax
	shrq	$3, %rax
	leaq	80(%r13,%rax,8), %rbx
.L34:
	cmpq	%rsi, %r12
	je	.L36
	subq	%r12, %rsi
	movq	%rbx, %rdi
	leaq	-40(%rsi), %rax
	movq	%r12, %rsi
	shrq	$3, %rax
	leaq	40(,%rax,8), %rdx
	movq	%rdx, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %rdx
	addq	%rdx, %rbx
.L36:
	testq	%r15, %r15
	je	.L37
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L37:
	movq	-56(%rbp), %rax
	movq	%r13, %xmm0
	movq	%rbx, %xmm5
	punpcklqdq	%xmm5, %xmm0
	movq	%rax, 16(%r14)
	movups	%xmm0, (%r14)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L48:
	.cfi_restore_state
	testq	%r8, %r8
	jne	.L33
	movq	$0, -56(%rbp)
	movl	$40, %ebx
	xorl	%r13d, %r13d
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L39:
	movl	$40, %ebx
	jmp	.L32
.L33:
	cmpq	%rdi, %r8
	movq	%rdi, %rbx
	cmovbe	%r8, %rbx
	imulq	$40, %rbx, %rbx
	jmp	.L32
.L47:
	leaq	.LC0(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE9829:
	.size	_ZNSt6vectorISt4pairIN2v88internal6torque14SourcePositionES4_ESaIS5_EE17_M_realloc_insertIJRS4_S9_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_, .-_ZNSt6vectorISt4pairIN2v88internal6torque14SourcePositionES4_ESaIS5_EE17_M_realloc_insertIJRS4_S9_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	.section	.text._ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_St6vectorIPNS2_10DeclarableESaIS8_EEESt10_Select1stISB_ESt4lessIS3_ESaISB_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorISB_ERS5_,"axG",@progbits,_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_St6vectorIPNS2_10DeclarableESaIS8_EEESt10_Select1stISB_ESt4lessIS3_ESaISB_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorISB_ERS5_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_St6vectorIPNS2_10DeclarableESaIS8_EEESt10_Select1stISB_ESt4lessIS3_ESaISB_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorISB_ERS5_
	.type	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_St6vectorIPNS2_10DeclarableESaIS8_EEESt10_Select1stISB_ESt4lessIS3_ESaISB_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorISB_ERS5_, @function
_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_St6vectorIPNS2_10DeclarableESaIS8_EEESt10_Select1stISB_ESt4lessIS3_ESaISB_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorISB_ERS5_:
.LFB10736:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	8(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	cmpq	%rsi, %r15
	je	.L99
	movl	(%rdx), %r14d
	cmpl	%r14d, 32(%rsi)
	jle	.L60
	movq	24(%rdi), %rbx
	movq	%rbx, %rax
	movq	%rbx, %rdx
	cmpq	%rsi, %rbx
	je	.L52
	movq	%rsi, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	%rax, %rdx
	cmpl	%r14d, 32(%rax)
	jge	.L62
	movl	$0, %ebx
	cmpq	$0, 24(%rax)
	movq	%rbx, %rax
	cmovne	%r12, %rdx
	cmovne	%r12, %rax
.L52:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L60:
	.cfi_restore_state
	jge	.L71
	movq	32(%rdi), %rdx
	cmpq	%rsi, %rdx
	je	.L98
	movq	%rsi, %rdi
	call	_ZSt18_Rb_tree_incrementPSt18_Rb_tree_node_base@PLT
	movq	%rax, %rdx
	cmpl	32(%rax), %r14d
	jge	.L73
	movl	$0, %ebx
	cmpq	$0, 24(%r12)
	movq	%rbx, %rax
	cmovne	%rdx, %rax
	cmove	%r12, %rdx
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L99:
	cmpq	$0, 40(%rdi)
	je	.L51
	movq	32(%rdi), %rdx
	movl	32(%rdx), %eax
	cmpl	%eax, (%r14)
	jg	.L98
.L51:
	movq	16(%r13), %rbx
	testq	%rbx, %rbx
	je	.L82
	movl	(%r14), %esi
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L100:
	movq	16(%rbx), %rax
	movl	$1, %ecx
	testq	%rax, %rax
	je	.L55
.L101:
	movq	%rax, %rbx
.L54:
	movl	32(%rbx), %edx
	cmpl	%edx, %esi
	jl	.L100
	movq	24(%rbx), %rax
	xorl	%ecx, %ecx
	testq	%rax, %rax
	jne	.L101
.L55:
	movq	%rbx, %r12
	testb	%cl, %cl
	jne	.L53
.L58:
	xorl	%eax, %eax
	cmpl	%esi, %edx
	cmovl	%rax, %rbx
	cmovge	%rax, %r12
.L59:
	addq	$8, %rsp
	movq	%rbx, %rax
	movq	%r12, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L71:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%rsi, %rax
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L98:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L62:
	.cfi_restore_state
	movq	16(%r13), %r12
	testq	%r12, %r12
	jne	.L65
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L103:
	movq	16(%r12), %rax
	movl	$1, %esi
.L68:
	testq	%rax, %rax
	je	.L66
	movq	%rax, %r12
.L65:
	movl	32(%r12), %ecx
	cmpl	%ecx, %r14d
	jl	.L103
	movq	24(%r12), %rax
	xorl	%esi, %esi
	jmp	.L68
	.p2align 4,,10
	.p2align 3
.L82:
	movq	%r12, %rbx
.L53:
	cmpq	%rbx, 24(%r13)
	je	.L84
	movq	%rbx, %rdi
	movq	%rbx, %r12
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movl	(%r14), %esi
	movl	32(%rax), %edx
	movq	%rax, %rbx
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L66:
	movq	%r12, %rdx
	testb	%sil, %sil
	jne	.L64
.L69:
	xorl	%eax, %eax
	cmpl	%ecx, %r14d
	cmovg	%rax, %r12
	cmovle	%rax, %rdx
.L70:
	movq	%r12, %rax
	jmp	.L52
.L102:
	movq	%r15, %r12
.L64:
	cmpq	%r12, %rbx
	je	.L88
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	%r12, %rdx
	movl	32(%rax), %ecx
	movq	%rax, %r12
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L73:
	movq	16(%r13), %rbx
	testq	%rbx, %rbx
	jne	.L76
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L105:
	movq	16(%rbx), %rax
	movl	$1, %esi
.L79:
	testq	%rax, %rax
	je	.L77
	movq	%rax, %rbx
.L76:
	movl	32(%rbx), %ecx
	cmpl	%ecx, %r14d
	jl	.L105
	movq	24(%rbx), %rax
	xorl	%esi, %esi
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L77:
	movq	%rbx, %rdx
	testb	%sil, %sil
	jne	.L75
.L80:
	xorl	%eax, %eax
	cmpl	%ecx, %r14d
	cmovg	%rax, %rbx
	cmovle	%rax, %rdx
.L81:
	movq	%rbx, %rax
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L84:
	movq	%rbx, %r12
	xorl	%ebx, %ebx
	jmp	.L59
.L104:
	movq	%r15, %rbx
.L75:
	cmpq	%rbx, 24(%r13)
	je	.L92
	movq	%rbx, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	%rbx, %rdx
	movl	32(%rax), %ecx
	movq	%rax, %rbx
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L88:
	movq	%r12, %rdx
	xorl	%r12d, %r12d
	jmp	.L70
.L92:
	movq	%rbx, %rdx
	xorl	%ebx, %ebx
	jmp	.L81
	.cfi_endproc
.LFE10736:
	.size	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_St6vectorIPNS2_10DeclarableESaIS8_EEESt10_Select1stISB_ESt4lessIS3_ESaISB_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorISB_ERS5_, .-_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_St6vectorIPNS2_10DeclarableESaIS8_EEESt10_Select1stISB_ESt4lessIS3_ESaISB_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorISB_ERS5_
	.section	.text._ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_St6vectorIS4_INS2_14SourcePositionES7_ESaIS8_EEESt10_Select1stISB_ESt4lessIS3_ESaISB_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorISB_ERS5_,"axG",@progbits,_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_St6vectorIS4_INS2_14SourcePositionES7_ESaIS8_EEESt10_Select1stISB_ESt4lessIS3_ESaISB_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorISB_ERS5_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_St6vectorIS4_INS2_14SourcePositionES7_ESaIS8_EEESt10_Select1stISB_ESt4lessIS3_ESaISB_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorISB_ERS5_
	.type	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_St6vectorIS4_INS2_14SourcePositionES7_ESaIS8_EEESt10_Select1stISB_ESt4lessIS3_ESaISB_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorISB_ERS5_, @function
_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_St6vectorIS4_INS2_14SourcePositionES7_ESaIS8_EEESt10_Select1stISB_ESt4lessIS3_ESaISB_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorISB_ERS5_:
.LFB10887:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	8(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	cmpq	%rsi, %r15
	je	.L156
	movl	(%rdx), %r14d
	cmpl	%r14d, 32(%rsi)
	jle	.L117
	movq	24(%rdi), %rbx
	movq	%rbx, %rax
	movq	%rbx, %rdx
	cmpq	%rsi, %rbx
	je	.L109
	movq	%rsi, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	%rax, %rdx
	cmpl	%r14d, 32(%rax)
	jge	.L119
	movl	$0, %ebx
	cmpq	$0, 24(%rax)
	movq	%rbx, %rax
	cmovne	%r12, %rdx
	cmovne	%r12, %rax
.L109:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L117:
	.cfi_restore_state
	jge	.L128
	movq	32(%rdi), %rdx
	cmpq	%rsi, %rdx
	je	.L155
	movq	%rsi, %rdi
	call	_ZSt18_Rb_tree_incrementPSt18_Rb_tree_node_base@PLT
	movq	%rax, %rdx
	cmpl	32(%rax), %r14d
	jge	.L130
	movl	$0, %ebx
	cmpq	$0, 24(%r12)
	movq	%rbx, %rax
	cmovne	%rdx, %rax
	cmove	%r12, %rdx
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L156:
	cmpq	$0, 40(%rdi)
	je	.L108
	movq	32(%rdi), %rdx
	movl	32(%rdx), %eax
	cmpl	%eax, (%r14)
	jg	.L155
.L108:
	movq	16(%r13), %rbx
	testq	%rbx, %rbx
	je	.L139
	movl	(%r14), %esi
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L157:
	movq	16(%rbx), %rax
	movl	$1, %ecx
	testq	%rax, %rax
	je	.L112
.L158:
	movq	%rax, %rbx
.L111:
	movl	32(%rbx), %edx
	cmpl	%edx, %esi
	jl	.L157
	movq	24(%rbx), %rax
	xorl	%ecx, %ecx
	testq	%rax, %rax
	jne	.L158
.L112:
	movq	%rbx, %r12
	testb	%cl, %cl
	jne	.L110
.L115:
	xorl	%eax, %eax
	cmpl	%esi, %edx
	cmovl	%rax, %rbx
	cmovge	%rax, %r12
.L116:
	addq	$8, %rsp
	movq	%rbx, %rax
	movq	%r12, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L128:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%rsi, %rax
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L155:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L119:
	.cfi_restore_state
	movq	16(%r13), %r12
	testq	%r12, %r12
	jne	.L122
	jmp	.L159
	.p2align 4,,10
	.p2align 3
.L160:
	movq	16(%r12), %rax
	movl	$1, %esi
.L125:
	testq	%rax, %rax
	je	.L123
	movq	%rax, %r12
.L122:
	movl	32(%r12), %ecx
	cmpl	%ecx, %r14d
	jl	.L160
	movq	24(%r12), %rax
	xorl	%esi, %esi
	jmp	.L125
	.p2align 4,,10
	.p2align 3
.L139:
	movq	%r12, %rbx
.L110:
	cmpq	%rbx, 24(%r13)
	je	.L141
	movq	%rbx, %rdi
	movq	%rbx, %r12
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movl	(%r14), %esi
	movl	32(%rax), %edx
	movq	%rax, %rbx
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L123:
	movq	%r12, %rdx
	testb	%sil, %sil
	jne	.L121
.L126:
	xorl	%eax, %eax
	cmpl	%ecx, %r14d
	cmovg	%rax, %r12
	cmovle	%rax, %rdx
.L127:
	movq	%r12, %rax
	jmp	.L109
.L159:
	movq	%r15, %r12
.L121:
	cmpq	%r12, %rbx
	je	.L145
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	%r12, %rdx
	movl	32(%rax), %ecx
	movq	%rax, %r12
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L130:
	movq	16(%r13), %rbx
	testq	%rbx, %rbx
	jne	.L133
	jmp	.L161
	.p2align 4,,10
	.p2align 3
.L162:
	movq	16(%rbx), %rax
	movl	$1, %esi
.L136:
	testq	%rax, %rax
	je	.L134
	movq	%rax, %rbx
.L133:
	movl	32(%rbx), %ecx
	cmpl	%ecx, %r14d
	jl	.L162
	movq	24(%rbx), %rax
	xorl	%esi, %esi
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L134:
	movq	%rbx, %rdx
	testb	%sil, %sil
	jne	.L132
.L137:
	xorl	%eax, %eax
	cmpl	%ecx, %r14d
	cmovg	%rax, %rbx
	cmovle	%rax, %rdx
.L138:
	movq	%rbx, %rax
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L141:
	movq	%rbx, %r12
	xorl	%ebx, %ebx
	jmp	.L116
.L161:
	movq	%r15, %rbx
.L132:
	cmpq	%rbx, 24(%r13)
	je	.L149
	movq	%rbx, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	%rbx, %rdx
	movl	32(%rax), %ecx
	movq	%rax, %rbx
	jmp	.L137
	.p2align 4,,10
	.p2align 3
.L145:
	movq	%r12, %rdx
	xorl	%r12d, %r12d
	jmp	.L127
.L149:
	movq	%rbx, %rdx
	xorl	%ebx, %ebx
	jmp	.L138
	.cfi_endproc
.LFE10887:
	.size	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_St6vectorIS4_INS2_14SourcePositionES7_ESaIS8_EEESt10_Select1stISB_ESt4lessIS3_ESaISB_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorISB_ERS5_, .-_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_St6vectorIS4_INS2_14SourcePositionES7_ESaIS8_EEESt10_Select1stISB_ESt4lessIS3_ESaISB_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorISB_ERS5_
	.section	.text._ZN2v88internal6torque18LanguageServerData13AddDefinitionENS1_14SourcePositionES3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque18LanguageServerData13AddDefinitionENS1_14SourcePositionES3_
	.type	_ZN2v88internal6torque18LanguageServerData13AddDefinitionENS1_14SourcePositionES3_, @function
_ZN2v88internal6torque18LanguageServerData13AddDefinitionENS1_14SourcePositionES3_:
.LFB7038:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:_ZZN2v88internal6torque21ContextualVariableTopINS1_18LanguageServerDataEEERPNT_5ScopeEvE3top@tpoff, %rbx
	movq	16(%rbx), %rax
	leaq	8(%rbx), %r13
	testq	%rax, %rax
	je	.L176
	movl	16(%rbp), %edx
	movq	%r13, %r12
	jmp	.L165
	.p2align 4,,10
	.p2align 3
.L182:
	movq	%rax, %r12
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L166
.L165:
	cmpl	%edx, 32(%rax)
	jge	.L182
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L165
.L166:
	cmpq	%r13, %r12
	je	.L164
	cmpl	32(%r12), %edx
	jl	.L164
.L169:
	movq	48(%r12), %rsi
	cmpq	56(%r12), %rsi
	je	.L174
.L184:
	movdqu	16(%rbp), %xmm1
	movups	%xmm1, (%rsi)
	movl	32(%rbp), %eax
	movl	%eax, 16(%rsi)
	movdqu	40(%rbp), %xmm2
	movups	%xmm2, 20(%rsi)
	movl	56(%rbp), %eax
	movl	%eax, 36(%rsi)
	addq	$40, 48(%r12)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L176:
	.cfi_restore_state
	movq	%r13, %r12
	.p2align 4,,10
	.p2align 3
.L164:
	movl	$64, %edi
	movq	%r12, %r14
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movq	%rax, %r12
	movl	16(%rbp), %eax
	movq	$0, 56(%r12)
	leaq	32(%r12), %rdx
	movl	%eax, 32(%r12)
	movups	%xmm0, 40(%r12)
	call	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_St6vectorIS4_INS2_14SourcePositionES7_ESaIS8_EEESt10_Select1stISB_ESt4lessIS3_ESaISB_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorISB_ERS5_
	movq	%rax, %r14
	testq	%rdx, %rdx
	je	.L170
	cmpq	%rdx, %r13
	je	.L177
	testq	%rax, %rax
	je	.L183
.L177:
	movl	$1, %edi
.L171:
	movq	%r12, %rsi
	movq	%r13, %rcx
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 40(%rbx)
	movq	48(%r12), %rsi
	cmpq	56(%r12), %rsi
	jne	.L184
.L174:
	leaq	40(%rbp), %rcx
	leaq	40(%r12), %rdi
	leaq	16(%rbp), %rdx
	call	_ZNSt6vectorISt4pairIN2v88internal6torque14SourcePositionES4_ESaIS5_EE17_M_realloc_insertIJRS4_S9_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L170:
	.cfi_restore_state
	movq	40(%r12), %rdi
	testq	%rdi, %rdi
	je	.L173
	call	_ZdlPv@PLT
.L173:
	movq	%r12, %rdi
	movq	%r14, %r12
	call	_ZdlPv@PLT
	jmp	.L169
	.p2align 4,,10
	.p2align 3
.L183:
	xorl	%edi, %edi
	movl	32(%rdx), %eax
	cmpl	%eax, 32(%r12)
	setl	%dil
	jmp	.L171
	.cfi_endproc
.LFE7038:
	.size	_ZN2v88internal6torque18LanguageServerData13AddDefinitionENS1_14SourcePositionES3_, .-_ZN2v88internal6torque18LanguageServerData13AddDefinitionENS1_14SourcePositionES3_
	.section	.text._ZNSt6vectorIPN2v88internal6torque10DeclarableESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_,"axG",@progbits,_ZNSt6vectorIPN2v88internal6torque10DeclarableESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPN2v88internal6torque10DeclarableESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	.type	_ZNSt6vectorIPN2v88internal6torque10DeclarableESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_, @function
_ZNSt6vectorIPN2v88internal6torque10DeclarableESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_:
.LFB10915:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r9
	movq	%r13, %rax
	subq	%r9, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L199
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L195
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L200
.L187:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L194:
	movq	(%r15), %rax
	subq	%r8, %r13
	leaq	8(%rbx,%rdx), %r15
	movq	%rax, (%rbx,%rdx)
	leaq	(%r15,%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L201
	testq	%r13, %r13
	jg	.L190
	testq	%r9, %r9
	jne	.L193
.L191:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L201:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jg	.L190
.L193:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L191
	.p2align 4,,10
	.p2align 3
.L200:
	testq	%rsi, %rsi
	jne	.L188
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L194
	.p2align 4,,10
	.p2align 3
.L190:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L191
	jmp	.L193
	.p2align 4,,10
	.p2align 3
.L195:
	movl	$8, %r14d
	jmp	.L187
.L199:
	leaq	.LC0(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L188:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$3, %r14
	jmp	.L187
	.cfi_endproc
.LFE10915:
	.size	_ZNSt6vectorIPN2v88internal6torque10DeclarableESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_, .-_ZNSt6vectorIPN2v88internal6torque10DeclarableESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	.section	.text._ZN2v88internal6torque18LanguageServerData27PrepareAllDeclarableSymbolsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque18LanguageServerData27PrepareAllDeclarableSymbolsEv
	.type	_ZN2v88internal6torque18LanguageServerData27PrepareAllDeclarableSymbolsEv, @function
_ZN2v88internal6torque18LanguageServerData27PrepareAllDeclarableSymbolsEv:
.LFB7040:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	96(%rdi), %rax
	movq	120(%rax), %r14
	movq	112(%rax), %r15
	cmpq	%r14, %r15
	je	.L202
	movq	%rdi, %rbx
	leaq	56(%rdi), %r13
	.p2align 4,,10
	.p2align 3
.L217:
	movq	(%r15), %rdx
	cmpb	$0, 64(%rdx)
	je	.L204
	movq	64(%rbx), %rax
	movl	24(%rdx), %ecx
	movq	%r13, %r12
	testq	%rax, %rax
	jne	.L206
	jmp	.L205
	.p2align 4,,10
	.p2align 3
.L228:
	movq	%rax, %r12
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L207
.L206:
	cmpl	32(%rax), %ecx
	jle	.L228
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L206
.L207:
	cmpq	%r12, %r13
	je	.L205
	cmpl	32(%r12), %ecx
	jge	.L210
.L205:
	movl	$64, %edi
	movl	%ecx, -76(%rbp)
	movq	%r12, -72(%rbp)
	call	_Znwm@PLT
	movl	-76(%rbp), %ecx
	pxor	%xmm0, %xmm0
	movq	-72(%rbp), %rsi
	movups	%xmm0, 40(%rax)
	leaq	32(%rax), %rdx
	leaq	48(%rbx), %rdi
	movq	%rax, %r12
	movl	%ecx, 32(%rax)
	movq	$0, 56(%rax)
	call	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_St6vectorIPNS2_10DeclarableESaIS8_EEESt10_Select1stISB_ESt4lessIS3_ESaISB_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorISB_ERS5_
	testq	%rdx, %rdx
	je	.L211
	cmpq	%rdx, %r13
	je	.L221
	testq	%rax, %rax
	je	.L229
.L221:
	movl	$1, %edi
.L212:
	movq	%r13, %rcx
	movq	%r12, %rsi
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 88(%rbx)
.L213:
	movq	(%r15), %rdx
.L210:
	movq	%rdx, -64(%rbp)
	movq	48(%r12), %rsi
	cmpq	56(%r12), %rsi
	je	.L215
	movq	%rdx, (%rsi)
	addq	$8, 48(%r12)
.L204:
	addq	$8, %r15
	cmpq	%r15, %r14
	jne	.L217
.L202:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L230
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L211:
	.cfi_restore_state
	movq	40(%r12), %rdi
	testq	%rdi, %rdi
	je	.L214
	movq	%rax, -72(%rbp)
	call	_ZdlPv@PLT
	movq	-72(%rbp), %rax
.L214:
	movq	%r12, %rdi
	movq	%rax, -72(%rbp)
	call	_ZdlPv@PLT
	movq	-72(%rbp), %rax
	movq	%rax, %r12
	jmp	.L213
	.p2align 4,,10
	.p2align 3
.L215:
	leaq	-64(%rbp), %rdx
	leaq	40(%r12), %rdi
	addq	$8, %r15
	call	_ZNSt6vectorIPN2v88internal6torque10DeclarableESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	cmpq	%r15, %r14
	jne	.L217
	jmp	.L202
	.p2align 4,,10
	.p2align 3
.L229:
	xorl	%edi, %edi
	movl	32(%rdx), %eax
	cmpl	%eax, 32(%r12)
	setl	%dil
	jmp	.L212
.L230:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7040:
	.size	_ZN2v88internal6torque18LanguageServerData27PrepareAllDeclarableSymbolsEv, .-_ZN2v88internal6torque18LanguageServerData27PrepareAllDeclarableSymbolsEv
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal6torque21ContextualVariableTopINS1_18LanguageServerDataEEERPNT_5ScopeEv,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal6torque21ContextualVariableTopINS1_18LanguageServerDataEEERPNT_5ScopeEv, @function
_GLOBAL__sub_I__ZN2v88internal6torque21ContextualVariableTopINS1_18LanguageServerDataEEERPNT_5ScopeEv:
.LFB13353:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE13353:
	.size	_GLOBAL__sub_I__ZN2v88internal6torque21ContextualVariableTopINS1_18LanguageServerDataEEERPNT_5ScopeEv, .-_GLOBAL__sub_I__ZN2v88internal6torque21ContextualVariableTopINS1_18LanguageServerDataEEERPNT_5ScopeEv
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal6torque21ContextualVariableTopINS1_18LanguageServerDataEEERPNT_5ScopeEv
	.section	.tbss._ZZN2v88internal6torque21ContextualVariableTopINS1_18LanguageServerDataEEERPNT_5ScopeEvE3top,"awT",@nobits
	.align 8
	.type	_ZZN2v88internal6torque21ContextualVariableTopINS1_18LanguageServerDataEEERPNT_5ScopeEvE3top, @object
	.size	_ZZN2v88internal6torque21ContextualVariableTopINS1_18LanguageServerDataEEERPNT_5ScopeEvE3top, 8
_ZZN2v88internal6torque21ContextualVariableTopINS1_18LanguageServerDataEEERPNT_5ScopeEvE3top:
	.zero	8
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
