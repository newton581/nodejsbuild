	.file	"declaration-visitor.cc"
	.text
	.section	.rodata._ZNK2v88internal6torque10Declarable9type_nameEv.str1.1,"aMS",@progbits,1
.LC0:
	.string	"<<unknown>>"
	.section	.text._ZNK2v88internal6torque10Declarable9type_nameEv,"axG",@progbits,_ZNK2v88internal6torque10Declarable9type_nameEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal6torque10Declarable9type_nameEv
	.type	_ZNK2v88internal6torque10Declarable9type_nameEv, @function
_ZNK2v88internal6torque10Declarable9type_nameEv:
.LFB5936:
	.cfi_startproc
	endbr64
	leaq	.LC0(%rip), %rax
	ret
	.cfi_endproc
.LFE5936:
	.size	_ZNK2v88internal6torque10Declarable9type_nameEv, .-_ZNK2v88internal6torque10Declarable9type_nameEv
	.section	.rodata._ZNK2v88internal6torque5Scope9type_nameEv.str1.1,"aMS",@progbits,1
.LC1:
	.string	"scope"
	.section	.text._ZNK2v88internal6torque5Scope9type_nameEv,"axG",@progbits,_ZNK2v88internal6torque5Scope9type_nameEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal6torque5Scope9type_nameEv
	.type	_ZNK2v88internal6torque5Scope9type_nameEv, @function
_ZNK2v88internal6torque5Scope9type_nameEv:
.LFB5949:
	.cfi_startproc
	endbr64
	leaq	.LC1(%rip), %rax
	ret
	.cfi_endproc
.LFE5949:
	.size	_ZNK2v88internal6torque5Scope9type_nameEv, .-_ZNK2v88internal6torque5Scope9type_nameEv
	.section	.text._ZN2v88internal6torque10DeclarableD2Ev,"axG",@progbits,_ZN2v88internal6torque10DeclarableD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque10DeclarableD2Ev
	.type	_ZN2v88internal6torque10DeclarableD2Ev, @function
_ZN2v88internal6torque10DeclarableD2Ev:
.LFB5954:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE5954:
	.size	_ZN2v88internal6torque10DeclarableD2Ev, .-_ZN2v88internal6torque10DeclarableD2Ev
	.weak	_ZN2v88internal6torque10DeclarableD1Ev
	.set	_ZN2v88internal6torque10DeclarableD1Ev,_ZN2v88internal6torque10DeclarableD2Ev
	.section	.rodata._ZNK2v88internal6torque9Namespace9type_nameEv.str1.1,"aMS",@progbits,1
.LC2:
	.string	"namespace"
	.section	.text._ZNK2v88internal6torque9Namespace9type_nameEv,"axG",@progbits,_ZNK2v88internal6torque9Namespace9type_nameEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal6torque9Namespace9type_nameEv
	.type	_ZNK2v88internal6torque9Namespace9type_nameEv, @function
_ZNK2v88internal6torque9Namespace9type_nameEv:
.LFB6003:
	.cfi_startproc
	endbr64
	leaq	.LC2(%rip), %rax
	ret
	.cfi_endproc
.LFE6003:
	.size	_ZNK2v88internal6torque9Namespace9type_nameEv, .-_ZNK2v88internal6torque9Namespace9type_nameEv
	.section	.text._ZN2v88internal6torque10DeclarableD0Ev,"axG",@progbits,_ZN2v88internal6torque10DeclarableD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque10DeclarableD0Ev
	.type	_ZN2v88internal6torque10DeclarableD0Ev, @function
_ZN2v88internal6torque10DeclarableD0Ev:
.LFB5956:
	.cfi_startproc
	endbr64
	movl	$72, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE5956:
	.size	_ZN2v88internal6torque10DeclarableD0Ev, .-_ZN2v88internal6torque10DeclarableD0Ev
	.section	.text._ZN2v88internal6torque5ScopeD2Ev,"axG",@progbits,_ZN2v88internal6torque5ScopeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque5ScopeD2Ev
	.type	_ZN2v88internal6torque5ScopeD2Ev, @function
_ZN2v88internal6torque5ScopeD2Ev:
.LFB6008:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal6torque5ScopeE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	88(%rdi), %r12
	movq	%rax, (%rdi)
	testq	%r12, %r12
	jne	.L12
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L27:
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	je	.L8
.L11:
	movq	%r13, %r12
.L12:
	movq	40(%r12), %rdi
	movq	(%r12), %r13
	testq	%rdi, %rdi
	je	.L9
	call	_ZdlPv@PLT
.L9:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	jne	.L27
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L11
.L8:
	movq	80(%rbx), %rax
	movq	72(%rbx), %rdi
	xorl	%esi, %esi
	addq	$120, %rbx
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	-48(%rbx), %rdi
	movq	$0, -24(%rbx)
	movq	$0, -32(%rbx)
	cmpq	%rbx, %rdi
	je	.L7
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L7:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE6008:
	.size	_ZN2v88internal6torque5ScopeD2Ev, .-_ZN2v88internal6torque5ScopeD2Ev
	.weak	_ZN2v88internal6torque5ScopeD1Ev
	.set	_ZN2v88internal6torque5ScopeD1Ev,_ZN2v88internal6torque5ScopeD2Ev
	.section	.text._ZN2v88internal6torque9NamespaceD0Ev,"axG",@progbits,_ZN2v88internal6torque9NamespaceD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque9NamespaceD0Ev
	.type	_ZN2v88internal6torque9NamespaceD0Ev, @function
_ZN2v88internal6torque9NamespaceD0Ev:
.LFB8836:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal6torque9NamespaceE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	%rax, (%rdi)
	movq	128(%rdi), %rdi
	leaq	144(%r12), %rax
	cmpq	%rax, %rdi
	je	.L29
	call	_ZdlPv@PLT
.L29:
	movq	88(%r12), %r13
	leaq	16+_ZTVN2v88internal6torque5ScopeE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	jne	.L34
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L49:
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L30
.L33:
	movq	%rbx, %r13
.L34:
	movq	40(%r13), %rdi
	movq	0(%r13), %rbx
	testq	%rdi, %rdi
	je	.L31
	call	_ZdlPv@PLT
.L31:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	jne	.L49
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L33
.L30:
	movq	80(%r12), %rax
	movq	72(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	72(%r12), %rdi
	leaq	120(%r12), %rax
	movq	$0, 96(%r12)
	movq	$0, 88(%r12)
	cmpq	%rax, %rdi
	je	.L35
	call	_ZdlPv@PLT
.L35:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$160, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE8836:
	.size	_ZN2v88internal6torque9NamespaceD0Ev, .-_ZN2v88internal6torque9NamespaceD0Ev
	.section	.text._ZN2v88internal6torque5ScopeD0Ev,"axG",@progbits,_ZN2v88internal6torque5ScopeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque5ScopeD0Ev
	.type	_ZN2v88internal6torque5ScopeD0Ev, @function
_ZN2v88internal6torque5ScopeD0Ev:
.LFB6010:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal6torque5ScopeE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	88(%rdi), %r13
	movq	%rax, (%rdi)
	testq	%r13, %r13
	jne	.L55
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L70:
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L51
.L54:
	movq	%rbx, %r13
.L55:
	movq	40(%r13), %rdi
	movq	0(%r13), %rbx
	testq	%rdi, %rdi
	je	.L52
	call	_ZdlPv@PLT
.L52:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	jne	.L70
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L54
.L51:
	movq	80(%r12), %rax
	movq	72(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	72(%r12), %rdi
	leaq	120(%r12), %rax
	movq	$0, 96(%r12)
	movq	$0, 88(%r12)
	cmpq	%rax, %rdi
	je	.L56
	call	_ZdlPv@PLT
.L56:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$128, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE6010:
	.size	_ZN2v88internal6torque5ScopeD0Ev, .-_ZN2v88internal6torque5ScopeD0Ev
	.section	.text._ZN2v88internal6torque9NamespaceD2Ev,"axG",@progbits,_ZN2v88internal6torque9NamespaceD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque9NamespaceD2Ev
	.type	_ZN2v88internal6torque9NamespaceD2Ev, @function
_ZN2v88internal6torque9NamespaceD2Ev:
.LFB8834:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal6torque9NamespaceE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	128(%rdi), %rdi
	leaq	144(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L72
	call	_ZdlPv@PLT
.L72:
	movq	88(%rbx), %r12
	leaq	16+_ZTVN2v88internal6torque5ScopeE(%rip), %rax
	movq	%rax, (%rbx)
	testq	%r12, %r12
	jne	.L77
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L92:
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	je	.L73
.L76:
	movq	%r13, %r12
.L77:
	movq	40(%r12), %rdi
	movq	(%r12), %r13
	testq	%rdi, %rdi
	je	.L74
	call	_ZdlPv@PLT
.L74:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	jne	.L92
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L76
.L73:
	movq	80(%rbx), %rax
	movq	72(%rbx), %rdi
	xorl	%esi, %esi
	addq	$120, %rbx
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	-48(%rbx), %rdi
	movq	$0, -24(%rbx)
	movq	$0, -32(%rbx)
	cmpq	%rbx, %rdi
	je	.L71
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L71:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8834:
	.size	_ZN2v88internal6torque9NamespaceD2Ev, .-_ZN2v88internal6torque9NamespaceD2Ev
	.weak	_ZN2v88internal6torque9NamespaceD1Ev
	.set	_ZN2v88internal6torque9NamespaceD1Ev,_ZN2v88internal6torque9NamespaceD2Ev
	.section	.text._ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0,"ax",@progbits
	.p2align 4
	.type	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0, @function
_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0:
.LFB12539:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$232, %rsp
	movq	%r8, -176(%rbp)
	movq	%r9, -168(%rbp)
	testb	%al, %al
	je	.L94
	movaps	%xmm0, -160(%rbp)
	movaps	%xmm1, -144(%rbp)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm3, -112(%rbp)
	movaps	%xmm4, -96(%rbp)
	movaps	%xmm5, -80(%rbp)
	movaps	%xmm6, -64(%rbp)
	movaps	%xmm7, -48(%rbp)
.L94:
	movq	%fs:40, %rax
	movq	%rax, -216(%rbp)
	xorl	%eax, %eax
	leaq	23(%rsi), %rax
	movq	%rsp, %rdi
	movq	%rax, %rdx
	andq	$-4096, %rax
	subq	%rax, %rdi
	andq	$-16, %rdx
	movq	%rdi, %rax
	cmpq	%rax, %rsp
	je	.L96
.L110:
	subq	$4096, %rsp
	orq	$0, 4088(%rsp)
	cmpq	%rax, %rsp
	jne	.L110
.L96:
	andl	$4095, %edx
	subq	%rdx, %rsp
	testq	%rdx, %rdx
	jne	.L111
.L97:
	leaq	15(%rsp), %r14
	leaq	16(%rbp), %rax
	movq	%rcx, %r8
	movl	$1, %edx
	andq	$-16, %r14
	movq	%rax, -232(%rbp)
	leaq	-240(%rbp), %r9
	leaq	-208(%rbp), %rax
	movq	%r14, %rdi
	movq	$-1, %rcx
	movl	$32, -240(%rbp)
	movl	$48, -236(%rbp)
	movq	%rax, -224(%rbp)
	call	__vsnprintf_chk@PLT
	leaq	16(%r12), %rdi
	movslq	%eax, %r13
	movq	%rdi, (%r12)
	movq	%r13, -248(%rbp)
	cmpq	$15, %r13
	ja	.L112
	cmpq	$1, %r13
	jne	.L100
	movzbl	(%r14), %eax
	movb	%al, 16(%r12)
.L101:
	movq	%r13, 8(%r12)
	movb	$0, (%rdi,%r13)
	movq	-216(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L113
	leaq	-24(%rbp), %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L100:
	.cfi_restore_state
	testq	%r13, %r13
	je	.L101
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L112:
	movq	%r12, %rdi
	leaq	-248(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%r12)
	movq	%rax, %rdi
	movq	-248(%rbp), %rax
	movq	%rax, 16(%r12)
.L99:
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-248(%rbp), %r13
	movq	(%r12), %rdi
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L111:
	orq	$0, -8(%rsp,%rdx)
	jmp	.L97
.L113:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE12539:
	.size	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0, .-_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0
	.section	.rodata._ZN2v88internal6torque16PositionAsStringB5cxx11ENS1_14SourcePositionE.str1.1,"aMS",@progbits,1
.LC3:
	.string	"%d"
	.section	.rodata._ZN2v88internal6torque16PositionAsStringB5cxx11ENS1_14SourcePositionE.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"basic_string::_M_construct null not valid"
	.section	.rodata._ZN2v88internal6torque16PositionAsStringB5cxx11ENS1_14SourcePositionE.str1.1
.LC5:
	.string	"basic_string::append"
.LC6:
	.string	":"
	.section	.text._ZN2v88internal6torque16PositionAsStringB5cxx11ENS1_14SourcePositionE,"axG",@progbits,_ZN2v88internal6torque16PositionAsStringB5cxx11ENS1_14SourcePositionE,comdat
	.p2align 4
	.weak	_ZN2v88internal6torque16PositionAsStringB5cxx11ENS1_14SourcePositionE
	.type	_ZN2v88internal6torque16PositionAsStringB5cxx11ENS1_14SourcePositionE, @function
_ZN2v88internal6torque16PositionAsStringB5cxx11ENS1_14SourcePositionE:
.LFB4539:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA4539
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC3(%rip), %rcx
	movl	$16, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-192(%rbp), %r15
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	leaq	-96(%rbp), %rdi
	pushq	%rbx
	subq	$216, %rsp
	.cfi_offset 3, -56
	movq	vsnprintf@GOTPCREL(%rip), %r13
	movl	24(%rbp), %r8d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -256(%rbp)
	addl	$1, %r8d
	movq	%r13, %rsi
.LEHB0:
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0
.LEHE0:
	movl	20(%rbp), %r8d
	movl	$16, %edx
	xorl	%eax, %eax
	leaq	.LC3(%rip), %rcx
	movq	%r13, %rsi
	movq	%r15, %rdi
	addl	$1, %r8d
.LEHB1:
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0
.LEHE1:
	movl	16(%rbp), %edi
.LEHB2:
	call	_ZN2v88internal6torque13SourceFileMap14PathFromV8RootB5cxx11ENS1_8SourceIdE@PLT
.LEHE2:
	leaq	-208(%rbp), %rbx
	movq	8(%rax), %r13
	movq	%rbx, -224(%rbp)
	movq	(%rax), %r14
	movq	%r14, %rax
	addq	%r13, %rax
	je	.L115
	testq	%r14, %r14
	je	.L177
.L115:
	movq	%r13, -232(%rbp)
	cmpq	$15, %r13
	ja	.L178
	cmpq	$1, %r13
	jne	.L118
	movzbl	(%r14), %eax
	movb	%al, -208(%rbp)
	movq	%rbx, %rax
.L119:
	movq	%r13, -216(%rbp)
	movb	$0, (%rax,%r13)
	movabsq	$4611686018427387903, %rax
	cmpq	%rax, -216(%rbp)
	je	.L179
	leaq	-224(%rbp), %r13
	movl	$1, %edx
	leaq	.LC6(%rip), %rsi
	movq	%r13, %rdi
.LEHB3:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.LEHE3:
	movq	-224(%rbp), %r9
	movq	-216(%rbp), %r8
	movl	$15, %eax
	movq	-184(%rbp), %rdx
	movq	%rax, %rdi
	movq	-192(%rbp), %rsi
	cmpq	%rbx, %r9
	cmovne	-208(%rbp), %rdi
	leaq	(%r8,%rdx), %rcx
	cmpq	%rdi, %rcx
	jbe	.L180
	leaq	-176(%rbp), %rdi
	cmpq	%rdi, %rsi
	cmovne	-176(%rbp), %rax
	movq	%rdi, -248(%rbp)
	cmpq	%rax, %rcx
	jbe	.L181
.L126:
	movq	%r13, %rdi
.LEHB4:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.LEHE4:
.L128:
	leaq	-144(%rbp), %r14
	leaq	16(%rax), %rdx
	movq	%r14, -160(%rbp)
	movq	(%rax), %rcx
	cmpq	%rdx, %rcx
	je	.L182
	movq	%rcx, -160(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -144(%rbp)
.L130:
	movq	8(%rax), %rcx
	movb	$0, 16(%rax)
	movq	%rcx, -152(%rbp)
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movabsq	$4611686018427387903, %rax
	cmpq	%rax, -152(%rbp)
	je	.L183
	leaq	-160(%rbp), %rdi
	movl	$1, %edx
	leaq	.LC6(%rip), %rsi
.LEHB5:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.LEHE5:
	leaq	-112(%rbp), %r13
	leaq	16(%rax), %rdx
	movq	%r13, -128(%rbp)
	movq	(%rax), %rcx
	cmpq	%rdx, %rcx
	je	.L184
	movq	%rcx, -128(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -112(%rbp)
.L133:
	movq	8(%rax), %rcx
	movb	$0, 16(%rax)
	leaq	-80(%rbp), %r15
	movq	%rcx, -120(%rbp)
	movq	%rdx, (%rax)
	movq	-128(%rbp), %r9
	movq	$0, 8(%rax)
	movl	$15, %eax
	movq	-120(%rbp), %r8
	movq	-88(%rbp), %rdx
	cmpq	%r13, %r9
	movq	%rax, %rdi
	cmovne	-112(%rbp), %rdi
	movq	-96(%rbp), %rsi
	leaq	(%r8,%rdx), %rcx
	cmpq	%rdi, %rcx
	jbe	.L135
	cmpq	%r15, %rsi
	cmovne	-80(%rbp), %rax
	cmpq	%rax, %rcx
	jbe	.L185
.L135:
	leaq	-128(%rbp), %rdi
.LEHB6:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.LEHE6:
.L137:
	leaq	16(%r12), %rdx
	movq	%rdx, (%r12)
	movq	(%rax), %rcx
	leaq	16(%rax), %rdx
	cmpq	%rdx, %rcx
	je	.L186
	movq	%rcx, (%r12)
	movq	16(%rax), %rcx
	movq	%rcx, 16(%r12)
.L139:
	movq	8(%rax), %rcx
	movq	%rdx, (%rax)
	movq	-128(%rbp), %rdi
	movq	$0, 8(%rax)
	movq	%rcx, 8(%r12)
	movb	$0, 16(%rax)
	cmpq	%r13, %rdi
	je	.L140
	call	_ZdlPv@PLT
.L140:
	movq	-160(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L141
	call	_ZdlPv@PLT
.L141:
	movq	-224(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L142
	call	_ZdlPv@PLT
.L142:
	movq	-192(%rbp), %rdi
	cmpq	-248(%rbp), %rdi
	je	.L143
	call	_ZdlPv@PLT
.L143:
	movq	-96(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L114
	call	_ZdlPv@PLT
.L114:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L187
	addq	$216, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L118:
	.cfi_restore_state
	testq	%r13, %r13
	jne	.L188
	movq	%rbx, %rax
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L178:
	leaq	-232(%rbp), %rsi
	leaq	-224(%rbp), %rdi
	xorl	%edx, %edx
.LEHB7:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE7:
	movq	%rax, -224(%rbp)
	movq	%rax, %rdi
	movq	-232(%rbp), %rax
	movq	%rax, -208(%rbp)
.L117:
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-232(%rbp), %r13
	movq	-224(%rbp), %rax
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L180:
	leaq	-176(%rbp), %rax
	movq	%rax, -248(%rbp)
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L182:
	movdqu	16(%rax), %xmm0
	movaps	%xmm0, -144(%rbp)
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L184:
	movdqu	16(%rax), %xmm1
	movaps	%xmm1, -112(%rbp)
	jmp	.L133
	.p2align 4,,10
	.p2align 3
.L186:
	movdqu	16(%rax), %xmm2
	movups	%xmm2, 16(%r12)
	jmp	.L139
	.p2align 4,,10
	.p2align 3
.L181:
	movq	%r9, %rcx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
.LEHB8:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.LEHE8:
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L185:
	movq	-256(%rbp), %rdi
	movq	%r9, %rcx
	xorl	%edx, %edx
	xorl	%esi, %esi
.LEHB9:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.LEHE9:
	jmp	.L137
.L177:
	leaq	.LC4(%rip), %rdi
.LEHB10:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE10:
.L179:
	leaq	.LC5(%rip), %rdi
.LEHB11:
	call	_ZSt20__throw_length_errorPKc@PLT
.LEHE11:
.L183:
	leaq	.LC5(%rip), %rdi
.LEHB12:
	call	_ZSt20__throw_length_errorPKc@PLT
.LEHE12:
.L187:
	call	__stack_chk_fail@PLT
.L188:
	movq	%rbx, %rdi
	jmp	.L117
.L163:
	endbr64
	movq	%rax, %r12
	leaq	-80(%rbp), %r15
	jmp	.L147
.L164:
	endbr64
	movq	%rax, %r12
.L145:
	movq	-128(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L147
	call	_ZdlPv@PLT
.L147:
	movq	-160(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L149
	call	_ZdlPv@PLT
.L149:
	movq	-224(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L125
	call	_ZdlPv@PLT
.L125:
	movq	-192(%rbp), %rdi
	cmpq	-248(%rbp), %rdi
	je	.L151
	call	_ZdlPv@PLT
.L151:
	movq	%r12, %rbx
.L152:
	movq	-96(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L153
	call	_ZdlPv@PLT
.L153:
	movq	%rbx, %rdi
.LEHB13:
	call	_Unwind_Resume@PLT
.LEHE13:
.L162:
	endbr64
	movq	%rax, %r12
	leaq	-80(%rbp), %r15
	jmp	.L149
.L165:
	endbr64
	movq	%rax, %r12
	jmp	.L123
.L160:
	endbr64
	movq	%rax, %rbx
	leaq	-80(%rbp), %r15
	jmp	.L152
.L161:
	endbr64
	movq	%rax, %r12
	leaq	-176(%rbp), %rax
	leaq	-80(%rbp), %r15
	movq	%rax, -248(%rbp)
	jmp	.L125
.L123:
	movq	-224(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L124
	call	_ZdlPv@PLT
.L124:
	leaq	-176(%rbp), %rax
	leaq	-80(%rbp), %r15
	movq	%rax, -248(%rbp)
	jmp	.L125
	.cfi_endproc
.LFE4539:
	.globl	__gxx_personality_v0
	.section	.gcc_except_table._ZN2v88internal6torque16PositionAsStringB5cxx11ENS1_14SourcePositionE,"aG",@progbits,_ZN2v88internal6torque16PositionAsStringB5cxx11ENS1_14SourcePositionE,comdat
.LLSDA4539:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE4539-.LLSDACSB4539
.LLSDACSB4539:
	.uleb128 .LEHB0-.LFB4539
	.uleb128 .LEHE0-.LEHB0
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB1-.LFB4539
	.uleb128 .LEHE1-.LEHB1
	.uleb128 .L160-.LFB4539
	.uleb128 0
	.uleb128 .LEHB2-.LFB4539
	.uleb128 .LEHE2-.LEHB2
	.uleb128 .L161-.LFB4539
	.uleb128 0
	.uleb128 .LEHB3-.LFB4539
	.uleb128 .LEHE3-.LEHB3
	.uleb128 .L165-.LFB4539
	.uleb128 0
	.uleb128 .LEHB4-.LFB4539
	.uleb128 .LEHE4-.LEHB4
	.uleb128 .L162-.LFB4539
	.uleb128 0
	.uleb128 .LEHB5-.LFB4539
	.uleb128 .LEHE5-.LEHB5
	.uleb128 .L163-.LFB4539
	.uleb128 0
	.uleb128 .LEHB6-.LFB4539
	.uleb128 .LEHE6-.LEHB6
	.uleb128 .L164-.LFB4539
	.uleb128 0
	.uleb128 .LEHB7-.LFB4539
	.uleb128 .LEHE7-.LEHB7
	.uleb128 .L161-.LFB4539
	.uleb128 0
	.uleb128 .LEHB8-.LFB4539
	.uleb128 .LEHE8-.LEHB8
	.uleb128 .L162-.LFB4539
	.uleb128 0
	.uleb128 .LEHB9-.LFB4539
	.uleb128 .LEHE9-.LEHB9
	.uleb128 .L164-.LFB4539
	.uleb128 0
	.uleb128 .LEHB10-.LFB4539
	.uleb128 .LEHE10-.LEHB10
	.uleb128 .L161-.LFB4539
	.uleb128 0
	.uleb128 .LEHB11-.LFB4539
	.uleb128 .LEHE11-.LEHB11
	.uleb128 .L165-.LFB4539
	.uleb128 0
	.uleb128 .LEHB12-.LFB4539
	.uleb128 .LEHE12-.LEHB12
	.uleb128 .L163-.LFB4539
	.uleb128 0
	.uleb128 .LEHB13-.LFB4539
	.uleb128 .LEHE13-.LEHB13
	.uleb128 0
	.uleb128 0
.LLSDACSE4539:
	.section	.text._ZN2v88internal6torque16PositionAsStringB5cxx11ENS1_14SourcePositionE,"axG",@progbits,_ZN2v88internal6torque16PositionAsStringB5cxx11ENS1_14SourcePositionE,comdat
	.size	_ZN2v88internal6torque16PositionAsStringB5cxx11ENS1_14SourcePositionE, .-_ZN2v88internal6torque16PositionAsStringB5cxx11ENS1_14SourcePositionE
	.section	.text._ZN2v88internal6torque14MessageBuilderD2Ev,"axG",@progbits,_ZN2v88internal6torque14MessageBuilderD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque14MessageBuilderD2Ev
	.type	_ZN2v88internal6torque14MessageBuilderD2Ev, @function
_ZN2v88internal6torque14MessageBuilderD2Ev:
.LFB4670:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA4670
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	addq	$16, %rbx
	subq	$8, %rsp
	call	_ZNK2v88internal6torque14MessageBuilder6ReportEv@PLT
	movq	-16(%rbx), %rdi
	cmpq	%rbx, %rdi
	je	.L189
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L189:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4670:
	.section	.gcc_except_table._ZN2v88internal6torque14MessageBuilderD2Ev,"aG",@progbits,_ZN2v88internal6torque14MessageBuilderD5Ev,comdat
.LLSDA4670:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE4670-.LLSDACSB4670
.LLSDACSB4670:
.LLSDACSE4670:
	.section	.text._ZN2v88internal6torque14MessageBuilderD2Ev,"axG",@progbits,_ZN2v88internal6torque14MessageBuilderD5Ev,comdat
	.size	_ZN2v88internal6torque14MessageBuilderD2Ev, .-_ZN2v88internal6torque14MessageBuilderD2Ev
	.weak	_ZN2v88internal6torque14MessageBuilderD1Ev
	.set	_ZN2v88internal6torque14MessageBuilderD1Ev,_ZN2v88internal6torque14MessageBuilderD2Ev
	.section	.text._ZN2v88internal6torquelsERSoRKNS1_4TypeE,"axG",@progbits,_ZN2v88internal6torquelsERSoRKNS1_4TypeE,comdat
	.p2align 4
	.weak	_ZN2v88internal6torquelsERSoRKNS1_4TypeE
	.type	_ZN2v88internal6torquelsERSoRKNS1_4TypeE, @function
_ZN2v88internal6torquelsERSoRKNS1_4TypeE:
.LFB5812:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA5812
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-64(%rbp), %rdi
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
.LEHB14:
	call	_ZNK2v88internal6torque4Type8ToStringB5cxx11Ev@PLT
.LEHE14:
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rsi
	movq	%r12, %rdi
.LEHB15:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE15:
	movq	-64(%rbp), %rdi
	leaq	-48(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L193
	call	_ZdlPv@PLT
.L193:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L200
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L200:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
.L197:
	endbr64
	movq	%rax, %r12
.L194:
	movq	-64(%rbp), %rdi
	leaq	-48(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L195
	call	_ZdlPv@PLT
.L195:
	movq	%r12, %rdi
.LEHB16:
	call	_Unwind_Resume@PLT
.LEHE16:
	.cfi_endproc
.LFE5812:
	.section	.gcc_except_table._ZN2v88internal6torquelsERSoRKNS1_4TypeE,"aG",@progbits,_ZN2v88internal6torquelsERSoRKNS1_4TypeE,comdat
.LLSDA5812:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE5812-.LLSDACSB5812
.LLSDACSB5812:
	.uleb128 .LEHB14-.LFB5812
	.uleb128 .LEHE14-.LEHB14
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB15-.LFB5812
	.uleb128 .LEHE15-.LEHB15
	.uleb128 .L197-.LFB5812
	.uleb128 0
	.uleb128 .LEHB16-.LFB5812
	.uleb128 .LEHE16-.LEHB16
	.uleb128 0
	.uleb128 0
.LLSDACSE5812:
	.section	.text._ZN2v88internal6torquelsERSoRKNS1_4TypeE,"axG",@progbits,_ZN2v88internal6torquelsERSoRKNS1_4TypeE,comdat
	.size	_ZN2v88internal6torquelsERSoRKNS1_4TypeE, .-_ZN2v88internal6torquelsERSoRKNS1_4TypeE
	.section	.text._ZN2v88internal6torque13QualifiedNameD2Ev,"axG",@progbits,_ZN2v88internal6torque13QualifiedNameD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque13QualifiedNameD2Ev
	.type	_ZN2v88internal6torque13QualifiedNameD2Ev, @function
_ZN2v88internal6torque13QualifiedNameD2Ev:
.LFB5915:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	leaq	40(%rbx), %rax
	subq	$8, %rsp
	movq	24(%rdi), %rdi
	cmpq	%rax, %rdi
	je	.L202
	call	_ZdlPv@PLT
.L202:
	movq	8(%rbx), %r13
	movq	(%rbx), %r12
	cmpq	%r12, %r13
	je	.L203
	.p2align 4,,10
	.p2align 3
.L207:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L204
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r12, %r13
	jne	.L207
.L205:
	movq	(%rbx), %r12
.L203:
	testq	%r12, %r12
	je	.L201
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L204:
	.cfi_restore_state
	addq	$32, %r12
	cmpq	%r12, %r13
	jne	.L207
	jmp	.L205
	.p2align 4,,10
	.p2align 3
.L201:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5915:
	.size	_ZN2v88internal6torque13QualifiedNameD2Ev, .-_ZN2v88internal6torque13QualifiedNameD2Ev
	.weak	_ZN2v88internal6torque13QualifiedNameD1Ev
	.set	_ZN2v88internal6torque13QualifiedNameD1Ev,_ZN2v88internal6torque13QualifiedNameD2Ev
	.section	.text._ZN2v88internal6torque9SignatureD2Ev,"axG",@progbits,_ZN2v88internal6torque9SignatureD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque9SignatureD2Ev
	.type	_ZN2v88internal6torque9SignatureD2Ev, @function
_ZN2v88internal6torque9SignatureD2Ev:
.LFB6102:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	120(%rdi), %r13
	movq	112(%rdi), %r12
	cmpq	%r12, %r13
	je	.L211
	.p2align 4,,10
	.p2align 3
.L215:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L212
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r13, %r12
	jne	.L215
.L213:
	movq	112(%rbx), %r12
.L211:
	testq	%r12, %r12
	je	.L216
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L216:
	movq	64(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L217
	call	_ZdlPv@PLT
.L217:
	cmpb	$0, 24(%rbx)
	je	.L218
	movq	32(%rbx), %rdi
	leaq	48(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L218
	call	_ZdlPv@PLT
.L218:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L210
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L212:
	.cfi_restore_state
	addq	$32, %r12
	cmpq	%r12, %r13
	jne	.L215
	jmp	.L213
	.p2align 4,,10
	.p2align 3
.L210:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE6102:
	.size	_ZN2v88internal6torque9SignatureD2Ev, .-_ZN2v88internal6torque9SignatureD2Ev
	.weak	_ZN2v88internal6torque9SignatureD1Ev
	.set	_ZN2v88internal6torque9SignatureD1Ev,_ZN2v88internal6torque9SignatureD2Ev
	.section	.text.unlikely._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_24ExternalMacroDeclarationE,"ax",@progbits
	.align 2
.LCOLDB7:
	.section	.text._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_24ExternalMacroDeclarationE,"ax",@progbits
.LHOTB7:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_24ExternalMacroDeclarationE
	.type	_ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_24ExternalMacroDeclarationE, @function
_ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_24ExternalMacroDeclarationE:
.LFB6543:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6543
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$280, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 200(%rdi)
	movb	$0, -256(%rbp)
	movb	$0, -248(%rbp)
	jne	.L288
.L228:
	leaq	-208(%rbp), %r14
	movq	%r12, %rsi
	movq	%r14, %rdi
.LEHB17:
	call	_ZN2v88internal6torque11TypeVisitor13MakeSignatureEPKNS1_19CallableDeclarationE@PLT
.LEHE17:
	movq	240(%r12), %r15
	movq	248(%r12), %r13
	leaq	-280(%rbp), %rbx
	movb	$1, -304(%rbp)
	movq	%rbx, -296(%rbp)
	movq	%r15, %rax
	addq	%r13, %rax
	je	.L237
	testq	%r15, %r15
	je	.L289
.L237:
	movq	%r13, -312(%rbp)
	cmpq	$15, %r13
	ja	.L290
	cmpq	$1, %r13
	jne	.L240
	movzbl	(%r15), %eax
	movb	%al, -280(%rbp)
	movq	%rbx, %rax
.L241:
	movq	%r13, -288(%rbp)
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	%r14, %rcx
	movb	$0, (%rax,%r13)
	movq	40(%r12), %rdi
	movl	$1, %esi
	leaq	-256(%rbp), %rax
	pushq	$1
	leaq	-304(%rbp), %rdx
	pushq	%rax
	addq	$32, %rdi
.LEHB18:
	.cfi_escape 0x2e,0x10
	call	_ZN2v88internal6torque12Declarations12DeclareMacroERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEbNS_4base8OptionalIS8_EERKNS1_9SignatureENSC_IPNS1_9StatementEEESD_b@PLT
.LEHE18:
	cmpb	$0, -304(%rbp)
	popq	%rax
	popq	%rdx
	je	.L242
	movq	-296(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L242
	call	_ZdlPv@PLT
.L242:
	movq	-88(%rbp), %r13
	movq	-96(%rbp), %r12
	cmpq	%r12, %r13
	je	.L243
	.p2align 4,,10
	.p2align 3
.L247:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L244
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r12, %r13
	jne	.L247
.L245:
	movq	-96(%rbp), %r12
.L243:
	testq	%r12, %r12
	je	.L248
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L248:
	movq	-144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L249
	call	_ZdlPv@PLT
.L249:
	cmpb	$0, -184(%rbp)
	je	.L250
	movq	-176(%rbp), %rdi
	leaq	-160(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L250
	call	_ZdlPv@PLT
.L250:
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L251
	call	_ZdlPv@PLT
.L251:
	cmpb	$0, -256(%rbp)
	je	.L227
	movq	-248(%rbp), %rdi
	leaq	-232(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L227
	call	_ZdlPv@PLT
.L227:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L291
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L240:
	.cfi_restore_state
	testq	%r13, %r13
	jne	.L292
	movq	%rbx, %rax
	jmp	.L241
	.p2align 4,,10
	.p2align 3
.L244:
	addq	$32, %r12
	cmpq	%r12, %r13
	jne	.L247
	jmp	.L245
	.p2align 4,,10
	.p2align 3
.L288:
	movq	208(%rdi), %r14
	movq	216(%rdi), %r13
	leaq	-232(%rbp), %rbx
	movq	%rbx, -248(%rbp)
	movq	%r14, %rax
	addq	%r13, %rax
	je	.L229
	testq	%r14, %r14
	je	.L293
.L229:
	movq	%r13, -312(%rbp)
	cmpq	$15, %r13
	ja	.L294
	cmpq	$1, %r13
	jne	.L232
	movzbl	(%r14), %eax
	movb	%al, -232(%rbp)
.L233:
	movq	%r13, -240(%rbp)
	movb	$0, (%rbx,%r13)
	movb	$1, -256(%rbp)
	jmp	.L228
	.p2align 4,,10
	.p2align 3
.L290:
	leaq	-312(%rbp), %rsi
	leaq	-296(%rbp), %rdi
	xorl	%edx, %edx
.LEHB19:
	.cfi_escape 0x2e,0
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE19:
	movq	%rax, -296(%rbp)
	movq	%rax, %rdi
	movq	-312(%rbp), %rax
	movq	%rax, -280(%rbp)
.L239:
	movq	%r13, %rdx
	movq	%r15, %rsi
	call	memcpy@PLT
	movq	-312(%rbp), %r13
	movq	-296(%rbp), %rax
	jmp	.L241
	.p2align 4,,10
	.p2align 3
.L232:
	testq	%r13, %r13
	je	.L233
	movq	%rbx, %rdi
	jmp	.L231
	.p2align 4,,10
	.p2align 3
.L294:
	leaq	-312(%rbp), %rsi
	leaq	-248(%rbp), %rdi
	xorl	%edx, %edx
.LEHB20:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE20:
	movq	%rax, -248(%rbp)
	movq	%rax, %rdi
	movq	-312(%rbp), %rax
	movq	%rax, -232(%rbp)
.L231:
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-312(%rbp), %r13
	movq	-248(%rbp), %rbx
	jmp	.L233
.L289:
	leaq	.LC4(%rip), %rdi
.LEHB21:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE21:
.L291:
	call	__stack_chk_fail@PLT
.L292:
	movq	%rbx, %rdi
	jmp	.L239
.L293:
	leaq	.LC4(%rip), %rdi
.LEHB22:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE22:
.L264:
	endbr64
	movq	%rax, %r12
	jmp	.L235
.L262:
	endbr64
	movq	%rax, %r12
	jmp	.L255
.L261:
	endbr64
	movq	%rax, %r12
	jmp	.L256
.L263:
	endbr64
	movq	%rax, %r12
	jmp	.L253
	.section	.gcc_except_table._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_24ExternalMacroDeclarationE,"a",@progbits
.LLSDA6543:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6543-.LLSDACSB6543
.LLSDACSB6543:
	.uleb128 .LEHB17-.LFB6543
	.uleb128 .LEHE17-.LEHB17
	.uleb128 .L261-.LFB6543
	.uleb128 0
	.uleb128 .LEHB18-.LFB6543
	.uleb128 .LEHE18-.LEHB18
	.uleb128 .L263-.LFB6543
	.uleb128 0
	.uleb128 .LEHB19-.LFB6543
	.uleb128 .LEHE19-.LEHB19
	.uleb128 .L262-.LFB6543
	.uleb128 0
	.uleb128 .LEHB20-.LFB6543
	.uleb128 .LEHE20-.LEHB20
	.uleb128 .L264-.LFB6543
	.uleb128 0
	.uleb128 .LEHB21-.LFB6543
	.uleb128 .LEHE21-.LEHB21
	.uleb128 .L262-.LFB6543
	.uleb128 0
	.uleb128 .LEHB22-.LFB6543
	.uleb128 .LEHE22-.LEHB22
	.uleb128 .L264-.LFB6543
	.uleb128 0
.LLSDACSE6543:
	.section	.text._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_24ExternalMacroDeclarationE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_24ExternalMacroDeclarationE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6543
	.type	_ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_24ExternalMacroDeclarationE.cold, @function
_ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_24ExternalMacroDeclarationE.cold:
.LFSB6543:
.L235:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	cmpb	$0, -256(%rbp)
	je	.L236
	movq	-248(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L236
	call	_ZdlPv@PLT
.L236:
	movq	%r12, %rdi
.LEHB23:
	call	_Unwind_Resume@PLT
.L253:
	cmpb	$0, -304(%rbp)
	je	.L255
	movq	-296(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L255
	call	_ZdlPv@PLT
.L255:
	movq	%r14, %rdi
	call	_ZN2v88internal6torque9SignatureD1Ev
.L256:
	cmpb	$0, -256(%rbp)
	je	.L257
	movq	-248(%rbp), %rdi
	leaq	-232(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L257
	call	_ZdlPv@PLT
.L257:
	movq	%r12, %rdi
	call	_Unwind_Resume@PLT
.LEHE23:
	.cfi_endproc
.LFE6543:
	.section	.gcc_except_table._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_24ExternalMacroDeclarationE
.LLSDAC6543:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6543-.LLSDACSBC6543
.LLSDACSBC6543:
	.uleb128 .LEHB23-.LCOLDB7
	.uleb128 .LEHE23-.LEHB23
	.uleb128 0
	.uleb128 0
.LLSDACSEC6543:
	.section	.text.unlikely._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_24ExternalMacroDeclarationE
	.section	.text._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_24ExternalMacroDeclarationE
	.size	_ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_24ExternalMacroDeclarationE, .-_ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_24ExternalMacroDeclarationE
	.section	.text.unlikely._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_24ExternalMacroDeclarationE
	.size	_ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_24ExternalMacroDeclarationE.cold, .-_ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_24ExternalMacroDeclarationE.cold
.LCOLDE7:
	.section	.text._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_24ExternalMacroDeclarationE
.LHOTE7:
	.section	.text.unlikely._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_22TorqueMacroDeclarationE,"ax",@progbits
	.align 2
.LCOLDB8:
	.section	.text._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_22TorqueMacroDeclarationE,"ax",@progbits
.LHOTB8:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_22TorqueMacroDeclarationE
	.type	_ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_22TorqueMacroDeclarationE, @function
_ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_22TorqueMacroDeclarationE:
.LFB6545:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6545
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$272, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 200(%rdi)
	movb	$0, -240(%rbp)
	movb	$0, -232(%rbp)
	jne	.L343
.L296:
	leaq	-192(%rbp), %r12
	movq	%rbx, %rsi
	movq	%r12, %rdi
.LEHB24:
	call	_ZN2v88internal6torque11TypeVisitor13MakeSignatureEPKNS1_19CallableDeclarationE@PLT
.LEHE24:
	movq	40(%rbx), %rax
	movzbl	240(%rbx), %esi
	pushq	$1
	movq	%r12, %rcx
	leaq	-288(%rbp), %rdx
	movb	$0, -288(%rbp)
	leaq	32(%rax), %rdi
	leaq	-240(%rbp), %rax
	movb	$0, -280(%rbp)
	pushq	%rax
	movl	248(%rbx), %r8d
	movq	256(%rbx), %r9
.LEHB25:
	.cfi_escape 0x2e,0x10
	call	_ZN2v88internal6torque12Declarations12DeclareMacroERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEbNS_4base8OptionalIS8_EERKNS1_9SignatureENSC_IPNS1_9StatementEEESD_b@PLT
.LEHE25:
	cmpb	$0, -288(%rbp)
	movq	%rax, %r13
	popq	%rax
	popq	%rdx
	jne	.L344
.L305:
	movq	-72(%rbp), %r14
	movq	-80(%rbp), %r12
	cmpq	%r12, %r14
	je	.L306
	.p2align 4,,10
	.p2align 3
.L310:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L307
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r14, %r12
	jne	.L310
.L308:
	movq	-80(%rbp), %r12
.L306:
	testq	%r12, %r12
	je	.L311
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L311:
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L312
	call	_ZdlPv@PLT
.L312:
	cmpb	$0, -168(%rbp)
	jne	.L345
.L313:
	movq	-192(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L314
	call	_ZdlPv@PLT
.L314:
	cmpb	$0, -240(%rbp)
	je	.L315
	movq	-232(%rbp), %rdi
	leaq	-216(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L315
	call	_ZdlPv@PLT
.L315:
	movdqu	12(%rbx), %xmm0
	movups	%xmm0, 24(%r13)
	movl	28(%rbx), %eax
	movl	%eax, 40(%r13)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L346
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L307:
	.cfi_restore_state
	addq	$32, %r12
	cmpq	%r12, %r14
	jne	.L310
	jmp	.L308
	.p2align 4,,10
	.p2align 3
.L345:
	movq	-160(%rbp), %rdi
	leaq	-144(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L313
	call	_ZdlPv@PLT
	jmp	.L313
	.p2align 4,,10
	.p2align 3
.L344:
	movq	-280(%rbp), %rdi
	leaq	-264(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L305
	call	_ZdlPv@PLT
	jmp	.L305
	.p2align 4,,10
	.p2align 3
.L343:
	movq	208(%rdi), %r14
	movq	216(%rdi), %r12
	leaq	-216(%rbp), %r13
	movq	%r13, -232(%rbp)
	movq	%r14, %rax
	addq	%r12, %rax
	je	.L297
	testq	%r14, %r14
	je	.L347
.L297:
	movq	%r12, -296(%rbp)
	cmpq	$15, %r12
	ja	.L348
	cmpq	$1, %r12
	jne	.L300
	movzbl	(%r14), %eax
	movb	%al, -216(%rbp)
.L301:
	movq	%r12, -224(%rbp)
	movb	$0, 0(%r13,%r12)
	movb	$1, -240(%rbp)
	jmp	.L296
	.p2align 4,,10
	.p2align 3
.L300:
	testq	%r12, %r12
	je	.L301
	movq	%r13, %rdi
	jmp	.L299
	.p2align 4,,10
	.p2align 3
.L348:
	leaq	-296(%rbp), %rsi
	leaq	-232(%rbp), %rdi
	xorl	%edx, %edx
.LEHB26:
	.cfi_escape 0x2e,0
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -232(%rbp)
	movq	%rax, %rdi
	movq	-296(%rbp), %rax
	movq	%rax, -216(%rbp)
.L299:
	movq	%r12, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-296(%rbp), %r12
	movq	-232(%rbp), %r13
	jmp	.L301
.L346:
	call	__stack_chk_fail@PLT
.L347:
	leaq	.LC4(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE26:
.L324:
	endbr64
	movq	%rax, %r12
	jmp	.L303
.L323:
	endbr64
	movq	%rax, %rbx
	jmp	.L316
.L322:
	endbr64
	movq	%rax, %r12
	jmp	.L318
	.section	.gcc_except_table._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_22TorqueMacroDeclarationE,"a",@progbits
.LLSDA6545:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6545-.LLSDACSB6545
.LLSDACSB6545:
	.uleb128 .LEHB24-.LFB6545
	.uleb128 .LEHE24-.LEHB24
	.uleb128 .L322-.LFB6545
	.uleb128 0
	.uleb128 .LEHB25-.LFB6545
	.uleb128 .LEHE25-.LEHB25
	.uleb128 .L323-.LFB6545
	.uleb128 0
	.uleb128 .LEHB26-.LFB6545
	.uleb128 .LEHE26-.LEHB26
	.uleb128 .L324-.LFB6545
	.uleb128 0
.LLSDACSE6545:
	.section	.text._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_22TorqueMacroDeclarationE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_22TorqueMacroDeclarationE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6545
	.type	_ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_22TorqueMacroDeclarationE.cold, @function
_ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_22TorqueMacroDeclarationE.cold:
.LFSB6545:
.L303:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	cmpb	$0, -240(%rbp)
	je	.L304
	movq	-232(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L304
	call	_ZdlPv@PLT
.L304:
	movq	%r12, %rdi
.LEHB27:
	call	_Unwind_Resume@PLT
.L316:
	cmpb	$0, -288(%rbp)
	je	.L317
	movq	-280(%rbp), %rdi
	leaq	-264(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L317
	call	_ZdlPv@PLT
.L317:
	movq	%r12, %rdi
	movq	%rbx, %r12
	call	_ZN2v88internal6torque9SignatureD1Ev
.L318:
	cmpb	$0, -240(%rbp)
	je	.L319
	movq	-232(%rbp), %rdi
	leaq	-216(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L319
	call	_ZdlPv@PLT
.L319:
	movq	%r12, %rdi
	call	_Unwind_Resume@PLT
.LEHE27:
	.cfi_endproc
.LFE6545:
	.section	.gcc_except_table._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_22TorqueMacroDeclarationE
.LLSDAC6545:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6545-.LLSDACSBC6545
.LLSDACSBC6545:
	.uleb128 .LEHB27-.LCOLDB8
	.uleb128 .LEHE27-.LEHB27
	.uleb128 0
	.uleb128 0
.LLSDACSEC6545:
	.section	.text.unlikely._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_22TorqueMacroDeclarationE
	.section	.text._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_22TorqueMacroDeclarationE
	.size	_ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_22TorqueMacroDeclarationE, .-_ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_22TorqueMacroDeclarationE
	.section	.text.unlikely._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_22TorqueMacroDeclarationE
	.size	_ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_22TorqueMacroDeclarationE.cold, .-_ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_22TorqueMacroDeclarationE.cold
.LCOLDE8:
	.section	.text._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_22TorqueMacroDeclarationE
.LHOTE8:
	.section	.text.unlikely._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_20IntrinsicDeclarationE,"ax",@progbits
	.align 2
.LCOLDB9:
	.section	.text._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_20IntrinsicDeclarationE,"ax",@progbits
.LHOTB9:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_20IntrinsicDeclarationE
	.type	_ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_20IntrinsicDeclarationE, @function
_ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_20IntrinsicDeclarationE:
.LFB6546:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6546
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-192(%rbp), %r13
	movq	%rdi, %r12
	pushq	%rbx
	movq	%r13, %rdi
	subq	$168, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
.LEHB28:
	call	_ZN2v88internal6torque11TypeVisitor13MakeSignatureEPKNS1_19CallableDeclarationE@PLT
.LEHE28:
	movq	40(%r12), %rdi
	movq	%r13, %rsi
	addq	$32, %rdi
.LEHB29:
	call	_ZN2v88internal6torque12Declarations16DeclareIntrinsicERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKNS1_9SignatureE@PLT
.LEHE29:
	movq	-72(%rbp), %rbx
	movq	-80(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L350
	.p2align 4,,10
	.p2align 3
.L354:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L351
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%rbx, %r12
	jne	.L354
.L352:
	movq	-80(%rbp), %r12
.L350:
	testq	%r12, %r12
	je	.L355
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L355:
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L356
	call	_ZdlPv@PLT
.L356:
	cmpb	$0, -168(%rbp)
	je	.L357
	movq	-160(%rbp), %rdi
	leaq	-144(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L357
	call	_ZdlPv@PLT
.L357:
	movq	-192(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L349
	call	_ZdlPv@PLT
.L349:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L372
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L351:
	.cfi_restore_state
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L354
	jmp	.L352
.L372:
	call	__stack_chk_fail@PLT
.L361:
	endbr64
	movq	%rax, %r12
	jmp	.L359
	.section	.gcc_except_table._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_20IntrinsicDeclarationE,"a",@progbits
.LLSDA6546:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6546-.LLSDACSB6546
.LLSDACSB6546:
	.uleb128 .LEHB28-.LFB6546
	.uleb128 .LEHE28-.LEHB28
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB29-.LFB6546
	.uleb128 .LEHE29-.LEHB29
	.uleb128 .L361-.LFB6546
	.uleb128 0
.LLSDACSE6546:
	.section	.text._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_20IntrinsicDeclarationE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_20IntrinsicDeclarationE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6546
	.type	_ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_20IntrinsicDeclarationE.cold, @function
_ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_20IntrinsicDeclarationE.cold:
.LFSB6546:
.L359:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movq	%r13, %rdi
	call	_ZN2v88internal6torque9SignatureD1Ev
	movq	%r12, %rdi
.LEHB30:
	call	_Unwind_Resume@PLT
.LEHE30:
	.cfi_endproc
.LFE6546:
	.section	.gcc_except_table._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_20IntrinsicDeclarationE
.LLSDAC6546:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6546-.LLSDACSBC6546
.LLSDACSBC6546:
	.uleb128 .LEHB30-.LCOLDB9
	.uleb128 .LEHE30-.LEHB30
	.uleb128 0
	.uleb128 0
.LLSDACSEC6546:
	.section	.text.unlikely._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_20IntrinsicDeclarationE
	.section	.text._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_20IntrinsicDeclarationE
	.size	_ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_20IntrinsicDeclarationE, .-_ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_20IntrinsicDeclarationE
	.section	.text.unlikely._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_20IntrinsicDeclarationE
	.size	_ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_20IntrinsicDeclarationE.cold, .-_ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_20IntrinsicDeclarationE.cold
.LCOLDE9:
	.section	.text._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_20IntrinsicDeclarationE
.LHOTE9:
	.section	.text._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_16ConstDeclarationE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_16ConstDeclarationE
	.type	_ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_16ConstDeclarationE, @function
_ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_16ConstDeclarationE:
.LFB6547:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	48(%rdi), %r12
	movq	40(%rdi), %rdi
	call	_ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_14TypeExpressionE@PLT
	movq	32(%rbx), %rdi
	movq	%r12, %rdx
	popq	%rbx
	movq	%rax, %rsi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal6torque12Declarations24DeclareNamespaceConstantEPNS1_10IdentifierEPKNS1_4TypeEPNS1_10ExpressionE@PLT
	.cfi_endproc
.LFE6547:
	.size	_ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_16ConstDeclarationE, .-_ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_16ConstDeclarationE
	.section	.text.unlikely._ZN2v88internal6torque21PredeclarationVisitor22ResolvePredeclarationsEv,"ax",@progbits
	.align 2
.LCOLDB10:
	.section	.text._ZN2v88internal6torque21PredeclarationVisitor22ResolvePredeclarationsEv,"ax",@progbits
.LHOTB10:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque21PredeclarationVisitor22ResolvePredeclarationsEv
	.type	_ZN2v88internal6torque21PredeclarationVisitor22ResolvePredeclarationsEv, @function
_ZN2v88internal6torque21PredeclarationVisitor22ResolvePredeclarationsEv:
.LFB6568:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6568
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$64, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
.LEHB31:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_13GlobalContextEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rax
	movq	112(%rax), %rbx
	movq	120(%rax), %r13
	cmpq	%r13, %rbx
	je	.L375
	leaq	-96(%rbp), %r14
	.p2align 4,,10
	.p2align 3
.L378:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L377
	cmpl	$9, 8(%r12)
	jne	.L377
	movq	16(%r12), %rax
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rax
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
.LEHE31:
	movq	%r14, (%rax)
	movl	40(%r12), %eax
	movdqu	24(%r12), %xmm0
	movl	%eax, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
.LEHB32:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rax
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
.LEHE32:
	leaq	-80(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rdx, (%rax)
.LEHB33:
	call	_ZNK2v88internal6torque9TypeAlias7ResolveEv@PLT
.LEHE33:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
	movq	-56(%rbp), %rdx
	movq	%rdx, (%rax)
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
	movq	-88(%rbp), %rdx
	movq	%rdx, (%rax)
.L377:
	addq	$8, %rbx
	cmpq	%rbx, %r13
	jne	.L378
.L375:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L389
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L389:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
.L383:
	endbr64
	movq	%rax, %r12
	jmp	.L379
.L382:
	endbr64
	movq	%rax, %r12
	jmp	.L380
	.section	.gcc_except_table._ZN2v88internal6torque21PredeclarationVisitor22ResolvePredeclarationsEv,"a",@progbits
.LLSDA6568:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6568-.LLSDACSB6568
.LLSDACSB6568:
	.uleb128 .LEHB31-.LFB6568
	.uleb128 .LEHE31-.LEHB31
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB32-.LFB6568
	.uleb128 .LEHE32-.LEHB32
	.uleb128 .L382-.LFB6568
	.uleb128 0
	.uleb128 .LEHB33-.LFB6568
	.uleb128 .LEHE33-.LEHB33
	.uleb128 .L383-.LFB6568
	.uleb128 0
.LLSDACSE6568:
	.section	.text._ZN2v88internal6torque21PredeclarationVisitor22ResolvePredeclarationsEv
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque21PredeclarationVisitor22ResolvePredeclarationsEv
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6568
	.type	_ZN2v88internal6torque21PredeclarationVisitor22ResolvePredeclarationsEv.cold, @function
_ZN2v88internal6torque21PredeclarationVisitor22ResolvePredeclarationsEv.cold:
.LFSB6568:
.L379:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
	movq	-56(%rbp), %rdx
	movq	%rdx, (%rax)
.L380:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
	movq	-88(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rdx, (%rax)
.LEHB34:
	call	_Unwind_Resume@PLT
.LEHE34:
	.cfi_endproc
.LFE6568:
	.section	.gcc_except_table._ZN2v88internal6torque21PredeclarationVisitor22ResolvePredeclarationsEv
.LLSDAC6568:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6568-.LLSDACSBC6568
.LLSDACSBC6568:
	.uleb128 .LEHB34-.LCOLDB10
	.uleb128 .LEHE34-.LEHB34
	.uleb128 0
	.uleb128 0
.LLSDACSEC6568:
	.section	.text.unlikely._ZN2v88internal6torque21PredeclarationVisitor22ResolvePredeclarationsEv
	.section	.text._ZN2v88internal6torque21PredeclarationVisitor22ResolvePredeclarationsEv
	.size	_ZN2v88internal6torque21PredeclarationVisitor22ResolvePredeclarationsEv, .-_ZN2v88internal6torque21PredeclarationVisitor22ResolvePredeclarationsEv
	.section	.text.unlikely._ZN2v88internal6torque21PredeclarationVisitor22ResolvePredeclarationsEv
	.size	_ZN2v88internal6torque21PredeclarationVisitor22ResolvePredeclarationsEv.cold, .-_ZN2v88internal6torque21PredeclarationVisitor22ResolvePredeclarationsEv.cold
.LCOLDE10:
	.section	.text._ZN2v88internal6torque21PredeclarationVisitor22ResolvePredeclarationsEv
.LHOTE10:
	.section	.text._ZN2v84base8internal15OptionalStorageINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEELb0ELb0EEC2EOS9_,"axG",@progbits,_ZN2v84base8internal15OptionalStorageINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEELb0ELb0EEC5EOS9_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v84base8internal15OptionalStorageINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEELb0ELb0EEC2EOS9_
	.type	_ZN2v84base8internal15OptionalStorageINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEELb0ELb0EEC2EOS9_, @function
_ZN2v84base8internal15OptionalStorageINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEELb0ELb0EEC2EOS9_:
.LFB7048:
	.cfi_startproc
	endbr64
	movb	$0, (%rdi)
	movb	$0, 8(%rdi)
	cmpb	$0, (%rsi)
	je	.L390
	leaq	24(%rdi), %rax
	movq	%rax, 8(%rdi)
	movq	8(%rsi), %rdx
	leaq	24(%rsi), %rax
	cmpq	%rax, %rdx
	je	.L394
	movq	%rdx, 8(%rdi)
	movq	24(%rsi), %rdx
	movq	%rdx, 24(%rdi)
.L393:
	movq	16(%rsi), %rdx
	movq	%rax, 8(%rsi)
	movq	$0, 16(%rsi)
	movq	%rdx, 16(%rdi)
	movb	$1, (%rdi)
	movb	$0, 24(%rsi)
.L390:
	ret
	.p2align 4,,10
	.p2align 3
.L394:
	movdqu	24(%rsi), %xmm0
	movups	%xmm0, 24(%rdi)
	jmp	.L393
	.cfi_endproc
.LFE7048:
	.size	_ZN2v84base8internal15OptionalStorageINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEELb0ELb0EEC2EOS9_, .-_ZN2v84base8internal15OptionalStorageINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEELb0ELb0EEC2EOS9_
	.weak	_ZN2v84base8internal15OptionalStorageINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEELb0ELb0EEC1EOS9_
	.set	_ZN2v84base8internal15OptionalStorageINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEELb0ELb0EEC1EOS9_,_ZN2v84base8internal15OptionalStorageINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEELb0ELb0EEC2EOS9_
	.section	.text._ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED2Ev,"axG",@progbits,_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED2Ev
	.type	_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED2Ev, @function
_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED2Ev:
.LFB7202:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	72(%rdi), %rdi
	leaq	88(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L396
	call	_ZdlPv@PLT
.L396:
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	56(%rbx), %rdi
	movq	%rax, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt6localeD1Ev@PLT
	.cfi_endproc
.LFE7202:
	.size	_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED2Ev, .-_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED2Ev
	.weak	_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED1Ev
	.set	_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED1Ev,_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED2Ev
	.section	.text._ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED0Ev,"axG",@progbits,_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED0Ev
	.type	_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED0Ev, @function
_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED0Ev:
.LFB7204:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	72(%rdi), %rdi
	leaq	88(%r12), %rax
	cmpq	%rax, %rdi
	je	.L399
	call	_ZdlPv@PLT
.L399:
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	56(%r12), %rdi
	movq	%rax, (%r12)
	call	_ZNSt6localeD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$104, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE7204:
	.size	_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED0Ev, .-_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED0Ev
	.section	.text.unlikely._ZN2v88internal6torqueL7MessageIJRA98_KcRPKNS1_4TypeEEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0,"ax",@progbits
.LCOLDB11:
	.section	.text._ZN2v88internal6torqueL7MessageIJRA98_KcRPKNS1_4TypeEEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0,"ax",@progbits
.LHOTB11:
	.p2align 4
	.type	_ZN2v88internal6torqueL7MessageIJRA98_KcRPKNS1_4TypeEEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0, @function
_ZN2v88internal6torqueL7MessageIJRA98_KcRPKNS1_4TypeEEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0:
.LFB12542:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA12542
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-448(%rbp), %r14
	leaq	-432(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r14, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$440, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
.LEHB35:
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEC1Ev@PLT
.LEHE35:
	movq	%r13, %rsi
	movq	%r15, %rdi
.LEHB36:
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	(%rbx), %rsi
	movq	%r15, %rdi
	call	_ZNSo9_M_insertIPKvEERSoT_@PLT
	leaq	-480(%rbp), %r13
	leaq	-424(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNKSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEE3strEv@PLT
.LEHE36:
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
.LEHB37:
	call	_ZN2v88internal6torque14MessageBuilderC1ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_13TorqueMessage4KindE@PLT
.LEHE37:
	movq	-480(%rbp), %rdi
	leaq	-464(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L402
	call	_ZdlPv@PLT
.L402:
	movq	%r14, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L411
	addq	$440, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L411:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
.L408:
	endbr64
	movq	%rax, %r12
	jmp	.L403
.L407:
	endbr64
	movq	%rax, %r12
	jmp	.L405
	.section	.gcc_except_table._ZN2v88internal6torqueL7MessageIJRA98_KcRPKNS1_4TypeEEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0,"a",@progbits
.LLSDA12542:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE12542-.LLSDACSB12542
.LLSDACSB12542:
	.uleb128 .LEHB35-.LFB12542
	.uleb128 .LEHE35-.LEHB35
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB36-.LFB12542
	.uleb128 .LEHE36-.LEHB36
	.uleb128 .L407-.LFB12542
	.uleb128 0
	.uleb128 .LEHB37-.LFB12542
	.uleb128 .LEHE37-.LEHB37
	.uleb128 .L408-.LFB12542
	.uleb128 0
.LLSDACSE12542:
	.section	.text._ZN2v88internal6torqueL7MessageIJRA98_KcRPKNS1_4TypeEEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torqueL7MessageIJRA98_KcRPKNS1_4TypeEEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC12542
	.type	_ZN2v88internal6torqueL7MessageIJRA98_KcRPKNS1_4TypeEEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0.cold, @function
_ZN2v88internal6torqueL7MessageIJRA98_KcRPKNS1_4TypeEEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0.cold:
.LFSB12542:
.L403:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-480(%rbp), %rdi
	leaq	-464(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L405
	call	_ZdlPv@PLT
.L405:
	movq	%r14, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r12, %rdi
.LEHB38:
	call	_Unwind_Resume@PLT
.LEHE38:
	.cfi_endproc
.LFE12542:
	.section	.gcc_except_table._ZN2v88internal6torqueL7MessageIJRA98_KcRPKNS1_4TypeEEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
.LLSDAC12542:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC12542-.LLSDACSBC12542
.LLSDACSBC12542:
	.uleb128 .LEHB38-.LCOLDB11
	.uleb128 .LEHE38-.LEHB38
	.uleb128 0
	.uleb128 0
.LLSDACSEC12542:
	.section	.text.unlikely._ZN2v88internal6torqueL7MessageIJRA98_KcRPKNS1_4TypeEEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
	.section	.text._ZN2v88internal6torqueL7MessageIJRA98_KcRPKNS1_4TypeEEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
	.size	_ZN2v88internal6torqueL7MessageIJRA98_KcRPKNS1_4TypeEEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0, .-_ZN2v88internal6torqueL7MessageIJRA98_KcRPKNS1_4TypeEEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
	.section	.text.unlikely._ZN2v88internal6torqueL7MessageIJRA98_KcRPKNS1_4TypeEEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
	.size	_ZN2v88internal6torqueL7MessageIJRA98_KcRPKNS1_4TypeEEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0.cold, .-_ZN2v88internal6torqueL7MessageIJRA98_KcRPKNS1_4TypeEEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0.cold
.LCOLDE11:
	.section	.text._ZN2v88internal6torqueL7MessageIJRA98_KcRPKNS1_4TypeEEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
.LHOTE11:
	.section	.rodata._ZN2v88internal6torque18DeclarationVisitor13CreateBuiltinEPNS1_18BuiltinDeclarationENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_NS1_9SignatureENS_4base8OptionalIPNS1_9StatementEEE.str1.1,"aMS",@progbits,1
.LC12:
	.string	"Rest parameters require "
.LC13:
	.string	" to be a JavaScript builtin"
	.section	.rodata._ZN2v88internal6torque18DeclarationVisitor13CreateBuiltinEPNS1_18BuiltinDeclarationENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_NS1_9SignatureENS_4base8OptionalIPNS1_9StatementEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC14:
	.string	"Return type of JavaScript-linkage builtins has to be JSAny."
	.align 8
.LC15:
	.string	"Parameters of JavaScript-linkage builtins have to be JSAny."
	.section	.rodata._ZN2v88internal6torque18DeclarationVisitor13CreateBuiltinEPNS1_18BuiltinDeclarationENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_NS1_9SignatureENS_4base8OptionalIPNS1_9StatementEEE.str1.1
.LC16:
	.string	"Builtin '"
.LC17:
	.string	"' uses the struct '"
.LC18:
	.string	"' as argument '"
.LC19:
	.string	"', which is not supported."
.LC20:
	.string	" "
.LC21:
	.string	"Untagged argument "
.LC22:
	.string	"at position "
.LC23:
	.string	" to builtin "
.LC24:
	.string	" is not supported."
.LC25:
	.string	"Builtins "
.LC26:
	.string	" cannot return structs "
	.section	.text.unlikely._ZN2v88internal6torque18DeclarationVisitor13CreateBuiltinEPNS1_18BuiltinDeclarationENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_NS1_9SignatureENS_4base8OptionalIPNS1_9StatementEEE,"ax",@progbits
	.align 2
.LCOLDB29:
	.section	.text._ZN2v88internal6torque18DeclarationVisitor13CreateBuiltinEPNS1_18BuiltinDeclarationENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_NS1_9SignatureENS_4base8OptionalIPNS1_9StatementEEE,"ax",@progbits
.LHOTB29:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque18DeclarationVisitor13CreateBuiltinEPNS1_18BuiltinDeclarationENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_NS1_9SignatureENS_4base8OptionalIPNS1_9StatementEEE
	.type	_ZN2v88internal6torque18DeclarationVisitor13CreateBuiltinEPNS1_18BuiltinDeclarationENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_NS1_9SignatureENS_4base8OptionalIPNS1_9StatementEEE, @function
_ZN2v88internal6torque18DeclarationVisitor13CreateBuiltinEPNS1_18BuiltinDeclarationENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_NS1_9SignatureENS_4base8OptionalIPNS1_9StatementEEE:
.LFB6540:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6540
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$712, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -672(%rbp)
	movq	%rsi, -712(%rbp)
	movq	%rdx, -720(%rbp)
	movl	%r8d, -744(%rbp)
	movq	%r9, -752(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 200(%rdi)
	movzbl	128(%rdi), %eax
	je	.L413
	testb	%al, %al
	movq	104(%rcx), %r13
	leaq	-432(%rbp), %r12
	setne	%al
	movzbl	%al, %eax
	addl	$1, %eax
	movl	%eax, -740(%rbp)
	movq	0(%r13), %rax
	movq	16(%rax), %rbx
.LEHB39:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_10TypeOracleEEERPNT_5ScopeEv@PLT
.LEHE39:
	leaq	-448(%rbp), %rax
	movq	%r12, -448(%rbp)
	movq	%rax, %rdi
	movq	%rax, -664(%rbp)
	movl	$1849774922, -432(%rbp)
	movb	$121, -428(%rbp)
	movq	$5, -440(%rbp)
	movb	$0, -427(%rbp)
.LEHB40:
	call	_ZN2v88internal6torque12Declarations16LookupGlobalTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
.LEHE40:
	movq	-448(%rbp), %rdi
	movq	%rax, %r14
	cmpq	%r12, %rdi
	je	.L436
	call	_ZdlPv@PLT
.L436:
	movq	%r14, %rsi
	movq	%r13, %rdi
.LEHB41:
	call	*%rbx
.LEHE41:
	testb	%al, %al
	je	.L677
.L437:
	movq	72(%r15), %rcx
	movq	64(%r15), %rax
	movq	96(%r15), %rbx
	movq	%rcx, %rdx
	subq	%rax, %rdx
	sarq	$3, %rdx
	cmpq	%rbx, %rdx
	jbe	.L460
	movq	.LC27(%rip), %xmm5
	movhps	.LC28(%rip), %xmm5
	movaps	%xmm5, -704(%rbp)
	jmp	.L485
	.p2align 4,,10
	.p2align 3
.L479:
	movd	%ecx, %xmm1
	movd	%edi, %xmm4
	movd	%edx, %xmm0
	movl	%eax, -460(%rbp)
	movd	%esi, %xmm5
	punpckldq	%xmm4, %xmm1
	punpckldq	%xmm5, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -476(%rbp)
.L483:
	movq	%r13, %rdi
	leaq	-496(%rbp), %r14
	call	_ZNK2v88internal6torque14MessageBuilder6ReportEv@PLT
	movq	-512(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L463
	call	_ZdlPv@PLT
.L463:
	movq	72(%r15), %rcx
	movq	64(%r15), %rax
	addq	$1, %rbx
	movq	%rcx, %rdx
	subq	%rax, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rbx
	jnb	.L460
.L485:
	leaq	0(,%rbx,8), %rcx
	movq	(%rax,%rbx,8), %r13
	movq	%rcx, -600(%rbp)
.LEHB42:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_10TypeOracleEEERPNT_5ScopeEv@PLT
.LEHE42:
	movq	%r12, -448(%rbp)
	movq	-664(%rbp), %rdi
	movl	$1849774922, (%r12)
	movb	$121, 4(%r12)
	movq	$5, -440(%rbp)
	movb	$0, -427(%rbp)
.LEHB43:
	call	_ZN2v88internal6torque12Declarations16LookupGlobalTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
.LEHE43:
	movq	-448(%rbp), %rdi
	movq	%rax, %r14
	cmpq	%r12, %rdi
	je	.L461
	call	_ZdlPv@PLT
.L461:
	cmpq	%r14, %r13
	je	.L463
	leaq	-320(%rbp), %r14
	movq	%r14, %rdi
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	xorl	%edi, %edi
	pxor	%xmm0, %xmm0
	movq	%rax, -320(%rbp)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movw	%di, -96(%rbp)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rsi
	movq	%rax, -448(%rbp)
	movq	-664(%rbp), %rdi
	movq	$0, -104(%rbp)
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	-24(%rax), %rax
	movq	%rsi, -448(%rbp,%rax)
	movq	-448(%rbp), %rax
	xorl	%esi, %esi
	movq	$0, -440(%rbp)
	addq	-24(%rax), %rdi
.LEHB44:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE44:
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rsi
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rsi, -432(%rbp,%rax)
	movq	-432(%rbp), %rax
	xorl	%esi, %esi
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
.LEHB45:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE45:
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	pxor	%xmm0, %xmm0
	movdqa	-704(%rbp), %xmm6
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -632(%rbp)
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movq	%rax, -656(%rbp)
	movq	%rax, -320(%rbp)
	leaq	-368(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -640(%rbp)
	movaps	%xmm6, -432(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r14, %rdi
	movl	$24, -360(%rbp)
	movq	%rax, -624(%rbp)
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movq	%rax, -648(%rbp)
	movq	%rax, -352(%rbp)
	leaq	-424(%rbp), %rax
	movq	%rax, %rsi
	movb	$0, -336(%rbp)
	movq	$0, -344(%rbp)
	movq	%rax, -680(%rbp)
.LEHB46:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE46:
	movl	$59, %edx
	leaq	.LC15(%rip), %rsi
	movq	%r12, %rdi
.LEHB47:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE47:
	leaq	-528(%rbp), %rax
	leaq	-544(%rbp), %rdi
	movq	$0, -536(%rbp)
	movq	%rax, -608(%rbp)
	movq	%rax, -544(%rbp)
	movq	-384(%rbp), %rax
	movq	%rdi, -600(%rbp)
	movb	$0, -528(%rbp)
	testq	%rax, %rax
	je	.L470
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	jbe	.L471
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r8
.LEHB48:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.LEHE48:
.L472:
	leaq	-512(%rbp), %r13
	movq	-600(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r13, %rdi
.LEHB49:
	call	_ZN2v88internal6torque14MessageBuilderC1ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_13TorqueMessage4KindE@PLT
.LEHE49:
	movq	-544(%rbp), %rdi
	cmpq	-608(%rbp), %rdi
	je	.L477
	call	_ZdlPv@PLT
.L477:
	movq	-632(%rbp), %rax
	movq	-352(%rbp), %rdi
	movq	.LC27(%rip), %xmm0
	movq	%rax, -448(%rbp)
	movq	-656(%rbp), %rax
	movhps	-624(%rbp), %xmm0
	movq	%rax, -320(%rbp)
	movaps	%xmm0, -432(%rbp)
	cmpq	-648(%rbp), %rdi
	je	.L478
	call	_ZdlPv@PLT
.L478:
	movq	-640(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rsi
	movq	%r14, %rdi
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rsi, -448(%rbp,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rsi
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -432(%rbp,%rax)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rsi, -448(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	movq	$0, -440(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-672(%rbp), %rax
	leaq	0(,%rbx,8), %rcx
	cmpb	$0, -480(%rbp)
	movq	72(%rax), %rax
	movq	(%rax,%rcx), %rax
	movl	12(%rax), %edx
	movl	16(%rax), %esi
	movl	20(%rax), %ecx
	movl	24(%rax), %edi
	movl	28(%rax), %eax
	jne	.L479
	movd	%ecx, %xmm1
	movd	%edi, %xmm6
	movd	%edx, %xmm0
	movl	%eax, -460(%rbp)
	movd	%esi, %xmm7
	punpckldq	%xmm6, %xmm1
	movb	$1, -480(%rbp)
	punpckldq	%xmm7, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -476(%rbp)
	jmp	.L483
	.p2align 4,,10
	.p2align 3
.L413:
	testb	%al, %al
	je	.L416
	leaq	-320(%rbp), %r14
	leaq	-448(%rbp), %r13
	movq	%r14, %rdi
	movq	%r13, -664(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	pxor	%xmm0, %xmm0
	xorl	%r9d, %r9d
	xorl	%esi, %esi
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movw	%r9w, -96(%rbp)
	movq	%rax, -320(%rbp)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rbx, -448(%rbp)
	movq	$0, -104(%rbp)
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	%rax, -704(%rbp)
	movq	-24(%rbx), %rax
	movq	%rcx, -448(%rbp,%rax)
	movq	-448(%rbp), %rax
	movq	$0, -440(%rbp)
	movq	-24(%rax), %rdi
	addq	%r13, %rdi
.LEHB50:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE50:
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	leaq	-432(%rbp), %r12
	xorl	%esi, %esi
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -432(%rbp,%rax)
	movq	-432(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
.LEHB51:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE51:
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rsi
	movq	.LC27(%rip), %xmm0
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rsi, -448(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -632(%rbp)
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movq	%rax, -656(%rbp)
	movq	%rax, -320(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -600(%rbp)
	leaq	-368(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -640(%rbp)
	movhps	-600(%rbp), %xmm0
	movaps	%xmm0, -432(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r14, %rdi
	movl	$24, -360(%rbp)
	movq	%rax, -624(%rbp)
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movq	%rax, -648(%rbp)
	movq	%rax, -352(%rbp)
	leaq	-424(%rbp), %rax
	movq	%rax, %rsi
	movb	$0, -336(%rbp)
	movq	$0, -344(%rbp)
	movq	%rax, -680(%rbp)
.LEHB52:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE52:
	movl	$24, %edx
	leaq	.LC12(%rip), %rsi
	movq	%r12, %rdi
.LEHB53:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-672(%rbp), %rax
	movq	%r12, %rdi
	movq	40(%rax), %rax
	movq	40(%rax), %rdx
	movq	32(%rax), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$27, %edx
	leaq	.LC13(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE53:
	leaq	-528(%rbp), %rax
	leaq	-544(%rbp), %rdi
	movq	$0, -536(%rbp)
	movq	%rax, -608(%rbp)
	movq	%rax, -544(%rbp)
	movq	-384(%rbp), %rax
	movq	%rdi, -600(%rbp)
	movb	$0, -528(%rbp)
	testq	%rax, %rax
	je	.L421
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	ja	.L678
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
.LEHB54:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.LEHE54:
.L423:
	leaq	-512(%rbp), %r13
	movq	-600(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r13, %rdi
.LEHB55:
	call	_ZN2v88internal6torque14MessageBuilderC1ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_13TorqueMessage4KindE@PLT
.LEHE55:
	movq	-544(%rbp), %rdi
	cmpq	-608(%rbp), %rdi
	je	.L428
	call	_ZdlPv@PLT
.L428:
	movq	-632(%rbp), %rax
	movq	-352(%rbp), %rdi
	movq	.LC27(%rip), %xmm0
	movq	%rax, -448(%rbp)
	movq	-656(%rbp), %rax
	movhps	-624(%rbp), %xmm0
	movq	%rax, -320(%rbp)
	movaps	%xmm0, -432(%rbp)
	cmpq	-648(%rbp), %rdi
	je	.L429
	call	_ZdlPv@PLT
.L429:
	movq	-640(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%r14, %rdi
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rsi
	leaq	-496(%rbp), %r14
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rsi, -432(%rbp,%rax)
	movq	%rbx, -448(%rbp)
	movq	-24(%rbx), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	movq	%rbx, -448(%rbp,%rax)
	movq	-704(%rbp), %rax
	movq	$0, -440(%rbp)
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal6torque14MessageBuilder6ReportEv@PLT
	movq	-512(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L431
	call	_ZdlPv@PLT
.L431:
	movl	$0, -740(%rbp)
	movq	64(%r15), %rax
	movq	72(%r15), %rcx
.L460:
	movq	.LC27(%rip), %xmm4
	xorl	%ebx, %ebx
	movhps	.LC28(%rip), %xmm4
	movaps	%xmm4, -704(%rbp)
	cmpq	%rcx, %rax
	jne	.L434
	jmp	.L435
	.p2align 4,,10
	.p2align 3
.L680:
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r8
.LEHB56:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.LEHE56:
.L493:
	leaq	-512(%rbp), %r13
	movq	-600(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r13, %rdi
.LEHB57:
	call	_ZN2v88internal6torque14MessageBuilderC1ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_13TorqueMessage4KindE@PLT
.LEHE57:
	movq	-544(%rbp), %rdi
	cmpq	-608(%rbp), %rdi
	je	.L498
	call	_ZdlPv@PLT
.L498:
	movq	-632(%rbp), %rax
	movq	-352(%rbp), %rdi
	movq	.LC27(%rip), %xmm0
	movq	%rax, -448(%rbp)
	movq	-656(%rbp), %rax
	movhps	-624(%rbp), %xmm0
	movq	%rax, -320(%rbp)
	movaps	%xmm0, -432(%rbp)
	cmpq	-648(%rbp), %rdi
	je	.L499
	call	_ZdlPv@PLT
.L499:
	movq	-640(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%r14, %rdi
	leaq	-496(%rbp), %r14
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -432(%rbp,%rax)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	movq	$0, -440(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal6torque14MessageBuilder6ReportEv@PLT
	movq	-512(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L501
	call	_ZdlPv@PLT
.L501:
	movq	64(%r15), %rax
	movq	72(%r15), %rcx
.L486:
	movq	%rcx, %rdx
	addq	$1, %rbx
	subq	%rax, %rdx
	sarq	$3, %rdx
	cmpq	%rbx, %rdx
	jbe	.L679
.L434:
	movq	(%rax,%rbx,8), %r13
	leaq	0(,%rbx,8), %rdx
	testq	%r13, %r13
	je	.L486
	cmpl	$4, 8(%r13)
	jne	.L486
	leaq	-320(%rbp), %r14
	addq	(%r15), %rdx
	movq	%r14, %rdi
	movq	%rdx, -600(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	movq	%rax, -320(%rbp)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movw	%si, -96(%rbp)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	xorl	%esi, %esi
	movq	%rax, -448(%rbp)
	movq	-664(%rbp), %rdi
	movq	$0, -104(%rbp)
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	movq	-448(%rbp), %rax
	movq	$0, -440(%rbp)
	addq	-24(%rax), %rdi
.LEHB58:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE58:
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	leaq	-432(%rbp), %r12
	xorl	%esi, %esi
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -432(%rbp,%rax)
	movq	-432(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
.LEHB59:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE59:
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	pxor	%xmm0, %xmm0
	movdqa	-704(%rbp), %xmm2
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -632(%rbp)
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movq	%rax, -656(%rbp)
	movq	%rax, -320(%rbp)
	leaq	-368(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -640(%rbp)
	movaps	%xmm2, -432(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r14, %rdi
	movl	$24, -360(%rbp)
	movq	%rax, -624(%rbp)
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movq	%rax, -648(%rbp)
	movq	%rax, -352(%rbp)
	leaq	-424(%rbp), %rax
	movq	%rax, %rsi
	movb	$0, -336(%rbp)
	movq	$0, -344(%rbp)
	movq	%rax, -680(%rbp)
.LEHB60:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE60:
	movl	$9, %edx
	leaq	.LC16(%rip), %rsi
	movq	%r12, %rdi
.LEHB61:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-672(%rbp), %rax
	movq	%r12, %rdi
	movq	40(%rax), %rax
	movq	40(%rax), %rdx
	movq	32(%rax), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$19, %edx
	leaq	.LC17(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	120(%r13), %rdx
	movq	112(%r13), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$15, %edx
	leaq	.LC18(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-600(%rbp), %rax
	movq	%r12, %rdi
	movq	(%rax), %rax
	movq	40(%rax), %rdx
	movq	32(%rax), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$26, %edx
	leaq	.LC19(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE61:
	leaq	-528(%rbp), %rax
	leaq	-544(%rbp), %rdi
	movq	$0, -536(%rbp)
	movq	%rax, -608(%rbp)
	movq	%rax, -544(%rbp)
	movq	-384(%rbp), %rax
	movq	%rdi, -600(%rbp)
	movb	$0, -528(%rbp)
	testq	%rax, %rax
	je	.L491
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	ja	.L680
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
.LEHB62:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.LEHE62:
	jmp	.L493
	.p2align 4,,10
	.p2align 3
.L435:
	movq	104(%r15), %rbx
	testq	%rbx, %rbx
	je	.L676
	cmpl	$4, 8(%rbx)
	jne	.L676
	leaq	-320(%rbp), %r14
	movq	%r14, %rdi
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	movq	%rax, -320(%rbp)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -704(%rbp)
	xorl	%eax, %eax
	movq	-664(%rbp), %rdi
	movw	%ax, -96(%rbp)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	$0, -104(%rbp)
	movq	%rax, -448(%rbp)
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	movq	-448(%rbp), %rax
	movq	$0, -440(%rbp)
	addq	-24(%rax), %rdi
.LEHB63:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE63:
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	leaq	-432(%rbp), %r12
	xorl	%esi, %esi
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -432(%rbp,%rax)
	movq	-432(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
.LEHB64:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE64:
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	.LC27(%rip), %xmm0
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -632(%rbp)
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movq	%rax, -656(%rbp)
	movq	%rax, -320(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -600(%rbp)
	leaq	-368(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -640(%rbp)
	movhps	-600(%rbp), %xmm0
	movaps	%xmm0, -432(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r14, %rdi
	movl	$24, -360(%rbp)
	movq	%rax, -624(%rbp)
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movq	%rax, -648(%rbp)
	movq	%rax, -352(%rbp)
	leaq	-424(%rbp), %rax
	movq	%rax, %rsi
	movb	$0, -336(%rbp)
	movq	$0, -344(%rbp)
	movq	%rax, -680(%rbp)
.LEHB65:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE65:
	movl	$9, %edx
	leaq	.LC25(%rip), %rsi
	movq	%r12, %rdi
.LEHB66:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-672(%rbp), %rax
	movq	%r12, %rdi
	movq	40(%rax), %rax
	movq	40(%rax), %rdx
	movq	32(%rax), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$23, %edx
	leaq	.LC26(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	120(%rbx), %rdx
	movq	112(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE66:
	leaq	-528(%rbp), %rax
	leaq	-544(%rbp), %rdi
	movq	$0, -536(%rbp)
	movq	%rax, -608(%rbp)
	movq	%rax, -544(%rbp)
	movq	-384(%rbp), %rax
	movq	%rdi, -600(%rbp)
	movb	$0, -528(%rbp)
	testq	%rax, %rax
	je	.L550
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	jbe	.L551
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r8
.LEHB67:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.LEHE67:
.L552:
	leaq	-512(%rbp), %r13
	movq	-600(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r13, %rdi
.LEHB68:
	call	_ZN2v88internal6torque14MessageBuilderC1ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_13TorqueMessage4KindE@PLT
.LEHE68:
	movq	-544(%rbp), %rdi
	cmpq	-608(%rbp), %rdi
	je	.L557
	call	_ZdlPv@PLT
.L557:
	movq	-632(%rbp), %rax
	movq	-352(%rbp), %rdi
	movq	.LC27(%rip), %xmm0
	movq	%rax, -448(%rbp)
	movq	-656(%rbp), %rax
	movhps	-624(%rbp), %xmm0
	movq	%rax, -320(%rbp)
	movaps	%xmm0, -432(%rbp)
	cmpq	-648(%rbp), %rdi
	je	.L558
	call	_ZdlPv@PLT
.L558:
	movq	-640(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	movq	%r14, %rdi
	leaq	-496(%rbp), %r14
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rbx, -448(%rbp,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rbx, -432(%rbp,%rax)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rbx, -448(%rbp,%rax)
	movq	-704(%rbp), %rax
	movq	$0, -440(%rbp)
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal6torque14MessageBuilder6ReportEv@PLT
	movq	-512(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L545
	call	_ZdlPv@PLT
.L545:
	movq	16(%r15), %rax
	movdqu	(%r15), %xmm4
	movq	$0, 16(%r15)
	pxor	%xmm0, %xmm0
	movq	-680(%rbp), %rdi
	movups	%xmm0, (%r15)
	leaq	24(%r15), %rsi
	movq	%rax, -432(%rbp)
	movaps	%xmm4, -448(%rbp)
	call	_ZN2v84base8internal15OptionalStorageINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEELb0ELb0EEC1EOS9_
	movq	80(%r15), %rax
	pxor	%xmm0, %xmm0
	movdqu	120(%r15), %xmm3
	movdqu	64(%r15), %xmm6
	movdqu	104(%r15), %xmm7
	movq	$0, 80(%r15)
	movq	%rax, -368(%rbp)
	movzbl	88(%r15), %eax
	movq	$0, 128(%r15)
	movb	%al, -360(%rbp)
	movq	96(%r15), %rax
	movq	%r14, -512(%rbp)
	movq	%rax, -352(%rbp)
	movzbl	136(%r15), %eax
	movaps	%xmm6, -384(%rbp)
	movb	%al, -312(%rbp)
	movq	-720(%rbp), %rax
	movups	%xmm0, 64(%r15)
	movq	(%rax), %rdx
	addq	$16, %rax
	movups	%xmm7, -344(%rbp)
	movups	%xmm3, -328(%rbp)
	movups	%xmm0, 112(%r15)
	cmpq	%rax, %rdx
	je	.L681
	movq	-720(%rbp), %rbx
	movq	%rdx, -512(%rbp)
	movq	16(%rbx), %rdx
	movq	%rdx, -496(%rbp)
.L564:
	movq	%rax, (%rbx)
	movq	-608(%rbp), %rax
	movq	8(%rbx), %rdx
	movb	$0, 16(%rbx)
	movq	%rax, -544(%rbp)
	movq	-712(%rbp), %rax
	movq	%rdx, -504(%rbp)
	movq	(%rax), %rdx
	addq	$16, %rax
	movq	$0, 8(%rbx)
	cmpq	%rax, %rdx
	je	.L682
	movq	-712(%rbp), %rbx
	movq	%rdx, -544(%rbp)
	movq	16(%rbx), %rdx
	movq	%rdx, -528(%rbp)
.L566:
	movq	8(%rbx), %rdx
	movq	%rax, (%rbx)
	movq	%r13, %rsi
	movq	$0, 8(%rbx)
	movl	-744(%rbp), %r8d
	movb	$0, 16(%rbx)
	movq	-752(%rbp), %r9
	movq	%rdx, -536(%rbp)
	movq	-664(%rbp), %rcx
	movl	-740(%rbp), %edx
	movq	-600(%rbp), %rdi
.LEHB69:
	call	_ZN2v88internal6torque12Declarations13CreateBuiltinENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES8_NS1_7Builtin4KindENS1_9SignatureENS_4base8OptionalIPNS1_9StatementEEE@PLT
.LEHE69:
	movq	-544(%rbp), %rdi
	movq	%rax, %r13
	cmpq	-608(%rbp), %rdi
	je	.L567
	call	_ZdlPv@PLT
.L567:
	movq	-512(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L568
	call	_ZdlPv@PLT
.L568:
	movq	-328(%rbp), %rbx
	movq	-336(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L569
	.p2align 4,,10
	.p2align 3
.L573:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L570
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L573
.L571:
	movq	-336(%rbp), %r12
.L569:
	testq	%r12, %r12
	je	.L574
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L574:
	movq	-384(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L575
	call	_ZdlPv@PLT
.L575:
	cmpb	$0, -424(%rbp)
	je	.L576
	movq	-416(%rbp), %rdi
	leaq	-400(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L576
	call	_ZdlPv@PLT
.L576:
	movq	-448(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L412
	call	_ZdlPv@PLT
.L412:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L683
	addq	$712, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L570:
	.cfi_restore_state
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L573
	jmp	.L571
	.p2align 4,,10
	.p2align 3
.L491:
	leaq	-352(%rbp), %rsi
.LEHB70:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
.LEHE70:
	jmp	.L493
	.p2align 4,,10
	.p2align 3
.L679:
	movq	-672(%rbp), %rbx
	cmpl	$47, 8(%rbx)
	jne	.L435
	cmpq	%rax, %rcx
	je	.L435
	movq	.LC27(%rip), %xmm5
	xorl	%ebx, %ebx
	leaq	-432(%rbp), %r12
	movhps	.LC28(%rip), %xmm5
	movaps	%xmm5, -736(%rbp)
	jmp	.L544
	.p2align 4,,10
	.p2align 3
.L687:
	leaq	0(,%rbx,8), %rax
	movq	(%rdx,%rax), %rax
	movq	%rax, -704(%rbp)
	testq	%rax, %rax
	je	.L511
	movq	32(%rax), %r14
	movq	40(%rax), %r13
	leaq	-560(%rbp), %rsi
	movq	%rsi, -688(%rbp)
	movq	%r14, %rax
	movq	%rsi, -576(%rbp)
	addq	%r13, %rax
	je	.L512
	testq	%r14, %r14
	je	.L684
.L512:
	movq	%r13, -584(%rbp)
	cmpq	$15, %r13
	ja	.L685
	cmpq	$1, %r13
	jne	.L515
	movzbl	(%r14), %eax
	movb	%al, -560(%rbp)
	movq	-688(%rbp), %rax
.L516:
	movq	%r13, -568(%rbp)
	movb	$0, (%rax,%r13)
	movabsq	$4611686018427387903, %rax
	cmpq	%rax, -568(%rbp)
	je	.L686
	leaq	-576(%rbp), %rdi
	movl	$1, %edx
	leaq	.LC20(%rip), %rsi
.LEHB71:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.LEHE71:
.L518:
	leaq	-320(%rbp), %r14
	movq	%r14, %rdi
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	movw	%dx, -96(%rbp)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -320(%rbp)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	$0, -104(%rbp)
	movq	-664(%rbp), %rdi
	movq	%rax, -448(%rbp)
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	movq	-448(%rbp), %rax
	movq	$0, -440(%rbp)
	addq	-24(%rax), %rdi
.LEHB72:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE72:
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	xorl	%esi, %esi
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -432(%rbp,%rax)
	movq	-432(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
.LEHB73:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE73:
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	pxor	%xmm0, %xmm0
	movdqa	-736(%rbp), %xmm3
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -632(%rbp)
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movq	%rax, -656(%rbp)
	movq	%rax, -320(%rbp)
	leaq	-368(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -640(%rbp)
	movaps	%xmm3, -432(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r14, %rdi
	movl	$24, -360(%rbp)
	movq	%rax, -624(%rbp)
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movq	%rax, -648(%rbp)
	movq	%rax, -352(%rbp)
	leaq	-424(%rbp), %rax
	movq	%rax, %rsi
	movb	$0, -336(%rbp)
	movq	$0, -344(%rbp)
	movq	%rax, -680(%rbp)
.LEHB74:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE74:
	movl	$18, %edx
	leaq	.LC21(%rip), %rsi
	movq	%r12, %rdi
.LEHB75:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-568(%rbp), %rdx
	movq	-576(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$12, %edx
	leaq	.LC22(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$12, %edx
	leaq	.LC23(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-672(%rbp), %rax
	movq	%r12, %rdi
	movq	40(%rax), %rax
	movq	40(%rax), %rdx
	movq	32(%rax), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$18, %edx
	leaq	.LC24(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE75:
	leaq	-528(%rbp), %rax
	leaq	-544(%rbp), %rdi
	movq	$0, -536(%rbp)
	movq	%rax, -608(%rbp)
	movq	%rax, -544(%rbp)
	movq	-384(%rbp), %rax
	movq	%rdi, -600(%rbp)
	movb	$0, -528(%rbp)
	testq	%rax, %rax
	je	.L526
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	jbe	.L527
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r8
.LEHB76:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.LEHE76:
.L528:
	leaq	-512(%rbp), %r13
	movq	-600(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r13, %rdi
.LEHB77:
	call	_ZN2v88internal6torque14MessageBuilderC1ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_13TorqueMessage4KindE@PLT
.LEHE77:
	movq	-544(%rbp), %rdi
	cmpq	-608(%rbp), %rdi
	je	.L533
	call	_ZdlPv@PLT
.L533:
	movq	-632(%rbp), %rax
	movq	-352(%rbp), %rdi
	movq	.LC27(%rip), %xmm0
	movq	%rax, -448(%rbp)
	movq	-656(%rbp), %rax
	movhps	-624(%rbp), %xmm0
	movq	%rax, -320(%rbp)
	movaps	%xmm0, -432(%rbp)
	cmpq	-648(%rbp), %rdi
	je	.L534
	call	_ZdlPv@PLT
.L534:
	movq	-640(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%r14, %rdi
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -432(%rbp,%rax)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	movq	$0, -440(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-704(%rbp), %rcx
	movq	-672(%rbp), %rax
	testq	%rcx, %rcx
	leaq	12(%rax), %rdx
	leaq	12(%rcx), %rax
	cmove	%rdx, %rax
	cmpb	$0, -480(%rbp)
	movl	(%rax), %edx
	movl	4(%rax), %esi
	movl	8(%rax), %ecx
	movl	12(%rax), %edi
	movl	16(%rax), %eax
	je	.L540
	movd	%ecx, %xmm1
	movd	%edi, %xmm7
	movd	%edx, %xmm0
	movl	%eax, -460(%rbp)
	movd	%esi, %xmm6
	punpckldq	%xmm7, %xmm1
	punpckldq	%xmm6, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -476(%rbp)
.L541:
	movq	%r13, %rdi
	leaq	-496(%rbp), %r14
	call	_ZNK2v88internal6torque14MessageBuilder6ReportEv@PLT
	movq	-512(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L542
	call	_ZdlPv@PLT
.L542:
	movq	-576(%rbp), %rdi
	cmpq	-688(%rbp), %rdi
	je	.L507
	call	_ZdlPv@PLT
.L507:
	movq	64(%r15), %rax
	movq	72(%r15), %rdx
	addq	$1, %rbx
	subq	%rax, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rbx
	jnb	.L435
.L544:
	movq	(%rax,%rbx,8), %r13
	leaq	0(,%rbx,8), %rcx
	movq	%rcx, -600(%rbp)
	movq	0(%r13), %rax
	movq	16(%rax), %r14
.LEHB78:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_10TypeOracleEEERPNT_5ScopeEv@PLT
.LEHE78:
	movl	$25701, %ecx
	movq	%r12, -448(%rbp)
	movq	-664(%rbp), %rdi
	movl	$1734828372, (%r12)
	movw	%cx, 4(%r12)
	movq	$6, -440(%rbp)
	movb	$0, -426(%rbp)
.LEHB79:
	call	_ZN2v88internal6torque12Declarations16LookupGlobalTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
.LEHE79:
	movq	-448(%rbp), %rdi
	movq	%rax, %rsi
	cmpq	%r12, %rdi
	je	.L506
	movq	%rax, -600(%rbp)
	call	_ZdlPv@PLT
	movq	-600(%rbp), %rsi
.L506:
	movq	%r13, %rdi
.LEHB80:
	call	*%r14
.LEHE80:
	testb	%al, %al
	jne	.L507
	movq	(%r15), %rdx
	movq	8(%r15), %rax
	subq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%rbx, %rax
	ja	.L687
.L511:
	leaq	-560(%rbp), %rax
	movq	$0, -568(%rbp)
	movq	%rax, -688(%rbp)
	movq	%rax, -576(%rbp)
	movb	$0, -560(%rbp)
	movq	$0, -704(%rbp)
	jmp	.L518
	.p2align 4,,10
	.p2align 3
.L540:
	movd	%ecx, %xmm1
	movd	%edi, %xmm7
	movd	%edx, %xmm0
	movl	%eax, -460(%rbp)
	movd	%esi, %xmm5
	punpckldq	%xmm7, %xmm1
	movb	$1, -480(%rbp)
	punpckldq	%xmm5, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -476(%rbp)
	jmp	.L541
	.p2align 4,,10
	.p2align 3
.L527:
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
.LEHB81:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.LEHE81:
	jmp	.L528
	.p2align 4,,10
	.p2align 3
.L471:
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
.LEHB82:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.LEHE82:
	jmp	.L472
	.p2align 4,,10
	.p2align 3
.L526:
	leaq	-352(%rbp), %rsi
.LEHB83:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
.LEHE83:
	jmp	.L528
	.p2align 4,,10
	.p2align 3
.L676:
	leaq	-424(%rbp), %rax
	leaq	-512(%rbp), %r13
	movq	%rax, -680(%rbp)
	leaq	-544(%rbp), %rax
	leaq	-496(%rbp), %r14
	movq	%rax, -600(%rbp)
	leaq	-528(%rbp), %rax
	movq	%rax, -608(%rbp)
	jmp	.L545
	.p2align 4,,10
	.p2align 3
.L416:
	leaq	-448(%rbp), %rbx
	movq	64(%rcx), %rax
	movl	$0, -740(%rbp)
	movq	%rbx, -664(%rbp)
	movq	72(%rcx), %rcx
	jmp	.L460
	.p2align 4,,10
	.p2align 3
.L682:
	movq	-712(%rbp), %rbx
	movdqu	16(%rbx), %xmm4
	movaps	%xmm4, -624(%rbp)
	movaps	%xmm4, -528(%rbp)
	jmp	.L566
	.p2align 4,,10
	.p2align 3
.L681:
	movq	-720(%rbp), %rbx
	movdqu	16(%rbx), %xmm3
	movaps	%xmm3, -624(%rbp)
	movaps	%xmm3, -496(%rbp)
	jmp	.L564
	.p2align 4,,10
	.p2align 3
.L551:
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
.LEHB84:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.LEHE84:
	jmp	.L552
	.p2align 4,,10
	.p2align 3
.L515:
	testq	%r13, %r13
	jne	.L688
	movq	-688(%rbp), %rax
	jmp	.L516
	.p2align 4,,10
	.p2align 3
.L685:
	leaq	-576(%rbp), %rdi
	leaq	-584(%rbp), %rsi
	xorl	%edx, %edx
.LEHB85:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE85:
	movq	%rax, -576(%rbp)
	movq	%rax, %rdi
	movq	-584(%rbp), %rax
	movq	%rax, -560(%rbp)
.L514:
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-584(%rbp), %r13
	movq	-576(%rbp), %rax
	jmp	.L516
	.p2align 4,,10
	.p2align 3
.L470:
	leaq	-352(%rbp), %rsi
.LEHB86:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
.LEHE86:
	jmp	.L472
	.p2align 4,,10
	.p2align 3
.L678:
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r8
.LEHB87:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.LEHE87:
	jmp	.L423
	.p2align 4,,10
	.p2align 3
.L550:
	leaq	-352(%rbp), %rsi
.LEHB88:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
.LEHE88:
	jmp	.L552
	.p2align 4,,10
	.p2align 3
.L677:
	leaq	-320(%rbp), %r14
	movq	%r14, %rdi
	call	_ZNSt8ios_baseC2Ev@PLT
	pxor	%xmm0, %xmm0
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movw	%r8w, -96(%rbp)
	movq	%rax, -320(%rbp)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rbx, -448(%rbp)
	movq	-664(%rbp), %rdi
	movq	$0, -104(%rbp)
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	%rax, -704(%rbp)
	movq	-24(%rbx), %rax
	movq	%rcx, -448(%rbp,%rax)
	movq	-448(%rbp), %rax
	movq	$0, -440(%rbp)
	addq	-24(%rax), %rdi
.LEHB89:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE89:
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	xorl	%esi, %esi
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -432(%rbp,%rax)
	movq	-432(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
.LEHB90:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE90:
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	.LC27(%rip), %xmm0
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -632(%rbp)
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movq	%rax, -656(%rbp)
	movq	%rax, -320(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -600(%rbp)
	leaq	-368(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -640(%rbp)
	movhps	-600(%rbp), %xmm0
	movaps	%xmm0, -432(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r14, %rdi
	movl	$24, -360(%rbp)
	movq	%rax, -624(%rbp)
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movq	%rax, -648(%rbp)
	movq	%rax, -352(%rbp)
	leaq	-424(%rbp), %rax
	movq	%rax, %rsi
	movb	$0, -336(%rbp)
	movq	$0, -344(%rbp)
	movq	%rax, -680(%rbp)
.LEHB91:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE91:
	movl	$59, %edx
	leaq	.LC14(%rip), %rsi
	movq	%r12, %rdi
.LEHB92:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE92:
	leaq	-528(%rbp), %rax
	leaq	-544(%rbp), %rdi
	movq	$0, -536(%rbp)
	movq	%rax, -608(%rbp)
	movq	%rax, -544(%rbp)
	movq	-384(%rbp), %rax
	movq	%rdi, -600(%rbp)
	movb	$0, -528(%rbp)
	testq	%rax, %rax
	je	.L445
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	ja	.L689
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
.LEHB93:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.LEHE93:
.L447:
	leaq	-512(%rbp), %r13
	movq	-600(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r13, %rdi
.LEHB94:
	call	_ZN2v88internal6torque14MessageBuilderC1ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_13TorqueMessage4KindE@PLT
.LEHE94:
	movq	-544(%rbp), %rdi
	cmpq	-608(%rbp), %rdi
	je	.L452
	call	_ZdlPv@PLT
.L452:
	movq	-632(%rbp), %rax
	movq	-352(%rbp), %rdi
	movq	.LC27(%rip), %xmm0
	movq	%rax, -448(%rbp)
	movq	-656(%rbp), %rax
	movhps	-624(%rbp), %xmm0
	movq	%rax, -320(%rbp)
	movaps	%xmm0, -432(%rbp)
	cmpq	-648(%rbp), %rdi
	je	.L453
	call	_ZdlPv@PLT
.L453:
	movq	-640(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rsi
	movq	%r14, %rdi
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rsi, -448(%rbp,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -432(%rbp,%rax)
	movq	%rbx, -448(%rbp)
	movq	-24(%rbx), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	movq	%rbx, -448(%rbp,%rax)
	movq	-704(%rbp), %rax
	movq	$0, -440(%rbp)
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-672(%rbp), %rax
	cmpb	$0, -480(%rbp)
	movq	168(%rax), %rdx
	movl	12(%rdx), %eax
	movl	16(%rdx), %esi
	movl	20(%rdx), %ecx
	movl	24(%rdx), %edi
	movl	28(%rdx), %edx
	je	.L690
	movd	%ecx, %xmm1
	movd	%edi, %xmm3
	movd	%eax, %xmm0
	movl	%edx, -460(%rbp)
	movd	%esi, %xmm4
	punpckldq	%xmm3, %xmm1
	punpckldq	%xmm4, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -476(%rbp)
.L458:
	movq	%r13, %rdi
	leaq	-496(%rbp), %r14
	call	_ZNK2v88internal6torque14MessageBuilder6ReportEv@PLT
	movq	-512(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L437
	call	_ZdlPv@PLT
	jmp	.L437
.L421:
	leaq	-352(%rbp), %rsi
.LEHB95:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
.LEHE95:
	jmp	.L423
.L689:
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r8
.LEHB96:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L447
.L690:
	movd	%ecx, %xmm1
	movd	%edi, %xmm3
	movd	%eax, %xmm0
	movl	%edx, -460(%rbp)
	movd	%esi, %xmm4
	punpckldq	%xmm3, %xmm1
	movb	$1, -480(%rbp)
	punpckldq	%xmm4, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -476(%rbp)
	jmp	.L458
.L445:
	leaq	-352(%rbp), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
.LEHE96:
	jmp	.L447
.L684:
	leaq	.LC4(%rip), %rdi
.LEHB97:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE97:
.L686:
	leaq	.LC5(%rip), %rdi
.LEHB98:
	call	_ZSt20__throw_length_errorPKc@PLT
.LEHE98:
.L688:
	movq	-688(%rbp), %rdi
	jmp	.L514
.L683:
	call	__stack_chk_fail@PLT
.L594:
	endbr64
	movq	%rax, %r12
	jmp	.L451
.L596:
	endbr64
	movq	%rax, %r12
	jmp	.L443
.L611:
	endbr64
	movq	%rax, %rbx
	jmp	.L488
.L592:
	endbr64
	movq	%rax, %r12
	jmp	.L425
.L607:
	endbr64
	movq	%rax, %r12
	jmp	.L497
.L588:
	endbr64
	movq	%rax, %r12
	jmp	.L432
.L610:
	endbr64
	movq	%rax, %rbx
	jmp	.L490
.L609:
	endbr64
	movq	%rax, %rbx
	jmp	.L489
.L612:
	endbr64
	movq	%rax, %r12
	jmp	.L495
.L595:
	endbr64
	movq	%rax, %r12
	jmp	.L456
.L608:
	endbr64
	movq	%rax, %r12
	jmp	.L502
.L586:
	endbr64
	movq	%rax, %r12
	jmp	.L579
.L605:
	endbr64
	movq	%rax, %rbx
	jmp	.L467
.L617:
	endbr64
	movq	%rax, %rbx
	jmp	.L523
.L601:
	endbr64
	movq	%rax, %r12
	jmp	.L476
.L618:
	endbr64
	movq	%rax, %rbx
	jmp	.L524
.L589:
	endbr64
	movq	%rax, %r12
	jmp	.L419
.L597:
	endbr64
	movq	%rax, %r12
	jmp	.L444
.L625:
	endbr64
	movq	%rax, %rbx
	jmp	.L547
.L587:
	endbr64
	movq	%rax, %r12
	jmp	.L427
.L626:
	endbr64
	movq	%rax, %r12
	jmp	.L554
.L616:
	endbr64
	movq	%rax, %rbx
	jmp	.L537
.L621:
	endbr64
	movq	%rax, %r12
	jmp	.L556
.L593:
	endbr64
	movq	%rax, %r13
	jmp	.L439
.L624:
	endbr64
	movq	%rax, %rbx
	jmp	.L549
.L599:
	endbr64
	movq	%rax, %r12
	jmp	.L449
.L598:
	endbr64
	movq	%rax, %r12
	jmp	.L442
.L620:
	endbr64
	movq	%rax, %rbx
	jmp	.L530
.L614:
	endbr64
	movq	%rax, %r12
	jmp	.L519
.L615:
	endbr64
	movq	%rax, %rbx
	jmp	.L532
.L613:
	endbr64
	movq	%rax, %r13
	jmp	.L509
.L619:
	endbr64
	movq	%rax, %rbx
	jmp	.L522
.L602:
	endbr64
	movq	%rax, %r12
	jmp	.L481
.L603:
	endbr64
	movq	%rax, %rbx
	jmp	.L468
.L606:
	endbr64
	movq	%rax, %r12
	jmp	.L474
.L623:
	endbr64
	movq	%rax, %rbx
	jmp	.L548
.L604:
	endbr64
	movq	%rax, %rbx
	jmp	.L469
.L590:
	endbr64
	movq	%rax, %r12
	jmp	.L420
.L622:
	endbr64
	movq	%rax, %r12
	jmp	.L561
.L600:
	endbr64
	movq	%rax, %r13
	jmp	.L464
.L591:
	endbr64
	movq	%rax, %r12
	jmp	.L418
	.section	.gcc_except_table._ZN2v88internal6torque18DeclarationVisitor13CreateBuiltinEPNS1_18BuiltinDeclarationENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_NS1_9SignatureENS_4base8OptionalIPNS1_9StatementEEE,"a",@progbits
.LLSDA6540:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6540-.LLSDACSB6540
.LLSDACSB6540:
	.uleb128 .LEHB39-.LFB6540
	.uleb128 .LEHE39-.LEHB39
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB40-.LFB6540
	.uleb128 .LEHE40-.LEHB40
	.uleb128 .L593-.LFB6540
	.uleb128 0
	.uleb128 .LEHB41-.LFB6540
	.uleb128 .LEHE41-.LEHB41
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB42-.LFB6540
	.uleb128 .LEHE42-.LEHB42
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB43-.LFB6540
	.uleb128 .LEHE43-.LEHB43
	.uleb128 .L600-.LFB6540
	.uleb128 0
	.uleb128 .LEHB44-.LFB6540
	.uleb128 .LEHE44-.LEHB44
	.uleb128 .L603-.LFB6540
	.uleb128 0
	.uleb128 .LEHB45-.LFB6540
	.uleb128 .LEHE45-.LEHB45
	.uleb128 .L605-.LFB6540
	.uleb128 0
	.uleb128 .LEHB46-.LFB6540
	.uleb128 .LEHE46-.LEHB46
	.uleb128 .L604-.LFB6540
	.uleb128 0
	.uleb128 .LEHB47-.LFB6540
	.uleb128 .LEHE47-.LEHB47
	.uleb128 .L601-.LFB6540
	.uleb128 0
	.uleb128 .LEHB48-.LFB6540
	.uleb128 .LEHE48-.LEHB48
	.uleb128 .L606-.LFB6540
	.uleb128 0
	.uleb128 .LEHB49-.LFB6540
	.uleb128 .LEHE49-.LEHB49
	.uleb128 .L602-.LFB6540
	.uleb128 0
	.uleb128 .LEHB50-.LFB6540
	.uleb128 .LEHE50-.LEHB50
	.uleb128 .L589-.LFB6540
	.uleb128 0
	.uleb128 .LEHB51-.LFB6540
	.uleb128 .LEHE51-.LEHB51
	.uleb128 .L591-.LFB6540
	.uleb128 0
	.uleb128 .LEHB52-.LFB6540
	.uleb128 .LEHE52-.LEHB52
	.uleb128 .L590-.LFB6540
	.uleb128 0
	.uleb128 .LEHB53-.LFB6540
	.uleb128 .LEHE53-.LEHB53
	.uleb128 .L587-.LFB6540
	.uleb128 0
	.uleb128 .LEHB54-.LFB6540
	.uleb128 .LEHE54-.LEHB54
	.uleb128 .L592-.LFB6540
	.uleb128 0
	.uleb128 .LEHB55-.LFB6540
	.uleb128 .LEHE55-.LEHB55
	.uleb128 .L588-.LFB6540
	.uleb128 0
	.uleb128 .LEHB56-.LFB6540
	.uleb128 .LEHE56-.LEHB56
	.uleb128 .L612-.LFB6540
	.uleb128 0
	.uleb128 .LEHB57-.LFB6540
	.uleb128 .LEHE57-.LEHB57
	.uleb128 .L608-.LFB6540
	.uleb128 0
	.uleb128 .LEHB58-.LFB6540
	.uleb128 .LEHE58-.LEHB58
	.uleb128 .L609-.LFB6540
	.uleb128 0
	.uleb128 .LEHB59-.LFB6540
	.uleb128 .LEHE59-.LEHB59
	.uleb128 .L611-.LFB6540
	.uleb128 0
	.uleb128 .LEHB60-.LFB6540
	.uleb128 .LEHE60-.LEHB60
	.uleb128 .L610-.LFB6540
	.uleb128 0
	.uleb128 .LEHB61-.LFB6540
	.uleb128 .LEHE61-.LEHB61
	.uleb128 .L607-.LFB6540
	.uleb128 0
	.uleb128 .LEHB62-.LFB6540
	.uleb128 .LEHE62-.LEHB62
	.uleb128 .L612-.LFB6540
	.uleb128 0
	.uleb128 .LEHB63-.LFB6540
	.uleb128 .LEHE63-.LEHB63
	.uleb128 .L623-.LFB6540
	.uleb128 0
	.uleb128 .LEHB64-.LFB6540
	.uleb128 .LEHE64-.LEHB64
	.uleb128 .L625-.LFB6540
	.uleb128 0
	.uleb128 .LEHB65-.LFB6540
	.uleb128 .LEHE65-.LEHB65
	.uleb128 .L624-.LFB6540
	.uleb128 0
	.uleb128 .LEHB66-.LFB6540
	.uleb128 .LEHE66-.LEHB66
	.uleb128 .L621-.LFB6540
	.uleb128 0
	.uleb128 .LEHB67-.LFB6540
	.uleb128 .LEHE67-.LEHB67
	.uleb128 .L626-.LFB6540
	.uleb128 0
	.uleb128 .LEHB68-.LFB6540
	.uleb128 .LEHE68-.LEHB68
	.uleb128 .L622-.LFB6540
	.uleb128 0
	.uleb128 .LEHB69-.LFB6540
	.uleb128 .LEHE69-.LEHB69
	.uleb128 .L586-.LFB6540
	.uleb128 0
	.uleb128 .LEHB70-.LFB6540
	.uleb128 .LEHE70-.LEHB70
	.uleb128 .L612-.LFB6540
	.uleb128 0
	.uleb128 .LEHB71-.LFB6540
	.uleb128 .LEHE71-.LEHB71
	.uleb128 .L614-.LFB6540
	.uleb128 0
	.uleb128 .LEHB72-.LFB6540
	.uleb128 .LEHE72-.LEHB72
	.uleb128 .L617-.LFB6540
	.uleb128 0
	.uleb128 .LEHB73-.LFB6540
	.uleb128 .LEHE73-.LEHB73
	.uleb128 .L619-.LFB6540
	.uleb128 0
	.uleb128 .LEHB74-.LFB6540
	.uleb128 .LEHE74-.LEHB74
	.uleb128 .L618-.LFB6540
	.uleb128 0
	.uleb128 .LEHB75-.LFB6540
	.uleb128 .LEHE75-.LEHB75
	.uleb128 .L615-.LFB6540
	.uleb128 0
	.uleb128 .LEHB76-.LFB6540
	.uleb128 .LEHE76-.LEHB76
	.uleb128 .L620-.LFB6540
	.uleb128 0
	.uleb128 .LEHB77-.LFB6540
	.uleb128 .LEHE77-.LEHB77
	.uleb128 .L616-.LFB6540
	.uleb128 0
	.uleb128 .LEHB78-.LFB6540
	.uleb128 .LEHE78-.LEHB78
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB79-.LFB6540
	.uleb128 .LEHE79-.LEHB79
	.uleb128 .L613-.LFB6540
	.uleb128 0
	.uleb128 .LEHB80-.LFB6540
	.uleb128 .LEHE80-.LEHB80
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB81-.LFB6540
	.uleb128 .LEHE81-.LEHB81
	.uleb128 .L620-.LFB6540
	.uleb128 0
	.uleb128 .LEHB82-.LFB6540
	.uleb128 .LEHE82-.LEHB82
	.uleb128 .L606-.LFB6540
	.uleb128 0
	.uleb128 .LEHB83-.LFB6540
	.uleb128 .LEHE83-.LEHB83
	.uleb128 .L620-.LFB6540
	.uleb128 0
	.uleb128 .LEHB84-.LFB6540
	.uleb128 .LEHE84-.LEHB84
	.uleb128 .L626-.LFB6540
	.uleb128 0
	.uleb128 .LEHB85-.LFB6540
	.uleb128 .LEHE85-.LEHB85
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB86-.LFB6540
	.uleb128 .LEHE86-.LEHB86
	.uleb128 .L606-.LFB6540
	.uleb128 0
	.uleb128 .LEHB87-.LFB6540
	.uleb128 .LEHE87-.LEHB87
	.uleb128 .L592-.LFB6540
	.uleb128 0
	.uleb128 .LEHB88-.LFB6540
	.uleb128 .LEHE88-.LEHB88
	.uleb128 .L626-.LFB6540
	.uleb128 0
	.uleb128 .LEHB89-.LFB6540
	.uleb128 .LEHE89-.LEHB89
	.uleb128 .L596-.LFB6540
	.uleb128 0
	.uleb128 .LEHB90-.LFB6540
	.uleb128 .LEHE90-.LEHB90
	.uleb128 .L598-.LFB6540
	.uleb128 0
	.uleb128 .LEHB91-.LFB6540
	.uleb128 .LEHE91-.LEHB91
	.uleb128 .L597-.LFB6540
	.uleb128 0
	.uleb128 .LEHB92-.LFB6540
	.uleb128 .LEHE92-.LEHB92
	.uleb128 .L594-.LFB6540
	.uleb128 0
	.uleb128 .LEHB93-.LFB6540
	.uleb128 .LEHE93-.LEHB93
	.uleb128 .L599-.LFB6540
	.uleb128 0
	.uleb128 .LEHB94-.LFB6540
	.uleb128 .LEHE94-.LEHB94
	.uleb128 .L595-.LFB6540
	.uleb128 0
	.uleb128 .LEHB95-.LFB6540
	.uleb128 .LEHE95-.LEHB95
	.uleb128 .L592-.LFB6540
	.uleb128 0
	.uleb128 .LEHB96-.LFB6540
	.uleb128 .LEHE96-.LEHB96
	.uleb128 .L599-.LFB6540
	.uleb128 0
	.uleb128 .LEHB97-.LFB6540
	.uleb128 .LEHE97-.LEHB97
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB98-.LFB6540
	.uleb128 .LEHE98-.LEHB98
	.uleb128 .L614-.LFB6540
	.uleb128 0
.LLSDACSE6540:
	.section	.text._ZN2v88internal6torque18DeclarationVisitor13CreateBuiltinEPNS1_18BuiltinDeclarationENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_NS1_9SignatureENS_4base8OptionalIPNS1_9StatementEEE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque18DeclarationVisitor13CreateBuiltinEPNS1_18BuiltinDeclarationENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_NS1_9SignatureENS_4base8OptionalIPNS1_9StatementEEE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6540
	.type	_ZN2v88internal6torque18DeclarationVisitor13CreateBuiltinEPNS1_18BuiltinDeclarationENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_NS1_9SignatureENS_4base8OptionalIPNS1_9StatementEEE.cold, @function
_ZN2v88internal6torque18DeclarationVisitor13CreateBuiltinEPNS1_18BuiltinDeclarationENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_NS1_9SignatureENS_4base8OptionalIPNS1_9StatementEEE.cold:
.LFSB6540:
.L449:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-544(%rbp), %rdi
	cmpq	-608(%rbp), %rdi
	je	.L451
	call	_ZdlPv@PLT
.L451:
	movq	-664(%rbp), %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r12, %rdi
.LEHB99:
	call	_Unwind_Resume@PLT
.L442:
	movq	%rbx, -448(%rbp)
	movq	-24(%rbx), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	movq	%rbx, -448(%rbp,%rax)
	movq	$0, -440(%rbp)
.L443:
	movq	-704(%rbp), %rax
	movq	%r14, %rdi
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	%r12, %rdi
	call	_Unwind_Resume@PLT
.L432:
	movq	-544(%rbp), %rdi
	cmpq	-608(%rbp), %rdi
	je	.L427
	call	_ZdlPv@PLT
	jmp	.L427
.L425:
	movq	-544(%rbp), %rdi
	cmpq	-608(%rbp), %rdi
	je	.L427
	call	_ZdlPv@PLT
.L427:
	movq	-664(%rbp), %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r12, %rdi
	call	_Unwind_Resume@PLT
.L488:
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rsi
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rsi, -448(%rbp,%rax)
	movq	$0, -440(%rbp)
.L489:
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%r14, %rdi
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	%rbx, %rdi
	call	_Unwind_Resume@PLT
.L490:
	movq	-680(%rbp), %rdi
	call	_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED1Ev
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rsi
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rsi, -448(%rbp,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rsi
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rsi, -432(%rbp,%rax)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rsi
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rsi, -448(%rbp,%rax)
	movq	$0, -440(%rbp)
	jmp	.L489
.L495:
	movq	-544(%rbp), %rdi
	cmpq	-608(%rbp), %rdi
	je	.L497
	call	_ZdlPv@PLT
.L497:
	movq	-664(%rbp), %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r12, %rdi
	call	_Unwind_Resume@PLT
.L554:
	movq	-544(%rbp), %rdi
	cmpq	-608(%rbp), %rdi
	je	.L556
	call	_ZdlPv@PLT
.L556:
	movq	-664(%rbp), %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r12, %rdi
	call	_Unwind_Resume@PLT
.L537:
	movq	-544(%rbp), %rdi
	cmpq	-608(%rbp), %rdi
	je	.L532
	call	_ZdlPv@PLT
	jmp	.L532
.L467:
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rsi
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rsi, -448(%rbp,%rax)
	movq	$0, -440(%rbp)
.L468:
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%r14, %rdi
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	%rbx, %rdi
	call	_Unwind_Resume@PLT
.L439:
	movq	-448(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L440
	call	_ZdlPv@PLT
.L440:
	movq	%r13, %rdi
	call	_Unwind_Resume@PLT
.L549:
	movq	-680(%rbp), %rdi
	call	_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED1Ev
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rsi
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rsi, -448(%rbp,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rsi
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -432(%rbp,%rax)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rsi, -448(%rbp,%rax)
	movq	$0, -440(%rbp)
.L548:
	movq	-704(%rbp), %rax
	movq	%r14, %rdi
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	%rbx, %rdi
	call	_Unwind_Resume@PLT
.L418:
	movq	%rbx, -448(%rbp)
	movq	-24(%rbx), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	movq	%rbx, -448(%rbp,%rax)
	movq	$0, -440(%rbp)
.L419:
	movq	-704(%rbp), %rax
	movq	%r14, %rdi
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	%r12, %rdi
	call	_Unwind_Resume@PLT
.L530:
	movq	-544(%rbp), %rdi
	cmpq	-608(%rbp), %rdi
	je	.L532
	call	_ZdlPv@PLT
.L532:
	movq	-664(%rbp), %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
.L525:
	movq	-576(%rbp), %rdi
	cmpq	-688(%rbp), %rdi
	je	.L578
	call	_ZdlPv@PLT
.L578:
	movq	%rbx, %rdi
	call	_Unwind_Resume@PLT
.L519:
	movq	-576(%rbp), %rdi
	cmpq	-688(%rbp), %rdi
	je	.L520
	call	_ZdlPv@PLT
.L520:
	movq	%r12, %rdi
	call	_Unwind_Resume@PLT
.L474:
	movq	-544(%rbp), %rdi
	cmpq	-608(%rbp), %rdi
	je	.L476
	call	_ZdlPv@PLT
.L476:
	movq	-664(%rbp), %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r12, %rdi
	call	_Unwind_Resume@PLT
.L502:
	movq	-544(%rbp), %rdi
	cmpq	-608(%rbp), %rdi
	je	.L497
	call	_ZdlPv@PLT
	jmp	.L497
.L509:
	movq	-448(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L510
	call	_ZdlPv@PLT
.L510:
	movq	%r13, %rdi
	call	_Unwind_Resume@PLT
.L547:
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	movq	$0, -440(%rbp)
	jmp	.L548
.L522:
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	movq	$0, -440(%rbp)
.L523:
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%r14, %rdi
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	jmp	.L525
.L481:
	movq	-544(%rbp), %rdi
	cmpq	-608(%rbp), %rdi
	je	.L476
	call	_ZdlPv@PLT
	jmp	.L476
.L444:
	movq	-680(%rbp), %rdi
	call	_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED1Ev
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rsi
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rsi, -448(%rbp,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rsi
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rsi, -432(%rbp,%rax)
	movq	%rbx, -448(%rbp)
	movq	-24(%rbx), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	movq	%rbx, -448(%rbp,%rax)
	movq	$0, -440(%rbp)
	jmp	.L443
.L469:
	movq	-680(%rbp), %rdi
	call	_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED1Ev
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rsi
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rsi, -448(%rbp,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rsi
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rsi, -432(%rbp,%rax)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rsi
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rsi, -448(%rbp,%rax)
	movq	$0, -440(%rbp)
	jmp	.L468
.L456:
	movq	-544(%rbp), %rdi
	cmpq	-608(%rbp), %rdi
	je	.L451
	call	_ZdlPv@PLT
	jmp	.L451
.L420:
	movq	-680(%rbp), %rdi
	call	_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED1Ev
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rsi
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rsi, -448(%rbp,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rsi
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rsi, -432(%rbp,%rax)
	movq	%rbx, -448(%rbp)
	movq	-24(%rbx), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	movq	%rbx, -448(%rbp,%rax)
	movq	$0, -440(%rbp)
	jmp	.L419
.L561:
	movq	-544(%rbp), %rdi
	cmpq	-608(%rbp), %rdi
	je	.L556
	call	_ZdlPv@PLT
	jmp	.L556
.L579:
	movq	-544(%rbp), %rdi
	cmpq	-608(%rbp), %rdi
	je	.L580
	call	_ZdlPv@PLT
.L580:
	movq	-512(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L581
	call	_ZdlPv@PLT
.L581:
	movq	-664(%rbp), %rdi
	call	_ZN2v88internal6torque9SignatureD1Ev
	movq	%r12, %rdi
	call	_Unwind_Resume@PLT
.L524:
	movq	-680(%rbp), %rdi
	call	_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED1Ev
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rsi
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rsi, -448(%rbp,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rsi
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -432(%rbp,%rax)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rsi, -448(%rbp,%rax)
	movq	$0, -440(%rbp)
	jmp	.L523
.L464:
	movq	-448(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L465
	call	_ZdlPv@PLT
.L465:
	movq	%r13, %rdi
	call	_Unwind_Resume@PLT
.LEHE99:
	.cfi_endproc
.LFE6540:
	.section	.gcc_except_table._ZN2v88internal6torque18DeclarationVisitor13CreateBuiltinEPNS1_18BuiltinDeclarationENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_NS1_9SignatureENS_4base8OptionalIPNS1_9StatementEEE
.LLSDAC6540:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6540-.LLSDACSBC6540
.LLSDACSBC6540:
	.uleb128 .LEHB99-.LCOLDB29
	.uleb128 .LEHE99-.LEHB99
	.uleb128 0
	.uleb128 0
.LLSDACSEC6540:
	.section	.text.unlikely._ZN2v88internal6torque18DeclarationVisitor13CreateBuiltinEPNS1_18BuiltinDeclarationENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_NS1_9SignatureENS_4base8OptionalIPNS1_9StatementEEE
	.section	.text._ZN2v88internal6torque18DeclarationVisitor13CreateBuiltinEPNS1_18BuiltinDeclarationENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_NS1_9SignatureENS_4base8OptionalIPNS1_9StatementEEE
	.size	_ZN2v88internal6torque18DeclarationVisitor13CreateBuiltinEPNS1_18BuiltinDeclarationENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_NS1_9SignatureENS_4base8OptionalIPNS1_9StatementEEE, .-_ZN2v88internal6torque18DeclarationVisitor13CreateBuiltinEPNS1_18BuiltinDeclarationENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_NS1_9SignatureENS_4base8OptionalIPNS1_9StatementEEE
	.section	.text.unlikely._ZN2v88internal6torque18DeclarationVisitor13CreateBuiltinEPNS1_18BuiltinDeclarationENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_NS1_9SignatureENS_4base8OptionalIPNS1_9StatementEEE
	.size	_ZN2v88internal6torque18DeclarationVisitor13CreateBuiltinEPNS1_18BuiltinDeclarationENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_NS1_9SignatureENS_4base8OptionalIPNS1_9StatementEEE.cold, .-_ZN2v88internal6torque18DeclarationVisitor13CreateBuiltinEPNS1_18BuiltinDeclarationENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_NS1_9SignatureENS_4base8OptionalIPNS1_9StatementEEE.cold
.LCOLDE29:
	.section	.text._ZN2v88internal6torque18DeclarationVisitor13CreateBuiltinEPNS1_18BuiltinDeclarationENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_NS1_9SignatureENS_4base8OptionalIPNS1_9StatementEEE
.LHOTE29:
	.section	.text._ZN2v88internal6torque11ReportErrorIJRA30_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_,"axG",@progbits,_ZN2v88internal6torque11ReportErrorIJRA30_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_,comdat
	.p2align 4
	.weak	_ZN2v88internal6torque11ReportErrorIJRA30_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_
	.type	_ZN2v88internal6torque11ReportErrorIJRA30_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_, @function
_ZN2v88internal6torque11ReportErrorIJRA30_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_:
.LFB7467:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA7467
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-432(%rbp), %r13
	leaq	-416(%rbp), %r14
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$496, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
.LEHB100:
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEC1Ev@PLT
.LEHE100:
	movq	%r12, %rsi
	movq	%r14, %rdi
.LEHB101:
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	8(%rbx), %rdx
	movq	(%rbx), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	-528(%rbp), %r14
	leaq	-408(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZNKSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEE3strEv@PLT
.LEHE101:
	leaq	-496(%rbp), %r12
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
.LEHB102:
	call	_ZN2v88internal6torque14MessageBuilderC1ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_13TorqueMessage4KindE@PLT
.LEHE102:
	movq	-528(%rbp), %rdi
	leaq	-512(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L692
	call	_ZdlPv@PLT
.L692:
	movq	%r13, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r12, %rdi
.LEHB103:
	call	_ZNK2v88internal6torque14MessageBuilder5ThrowEv@PLT
.LEHE103:
.L699:
	endbr64
	movq	%rax, %r12
	jmp	.L695
.L698:
	endbr64
	movq	%rax, %r13
	jmp	.L696
.L700:
	endbr64
	movq	%rax, %r12
.L693:
	movq	-528(%rbp), %rdi
	leaq	-512(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L695
	call	_ZdlPv@PLT
.L695:
	movq	%r13, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r12, %rdi
.LEHB104:
	call	_Unwind_Resume@PLT
.L696:
	movq	%r12, %rdi
	call	_ZN2v88internal6torque14MessageBuilderD1Ev
	movq	%r13, %rdi
	call	_Unwind_Resume@PLT
.LEHE104:
	.cfi_endproc
.LFE7467:
	.section	.gcc_except_table._ZN2v88internal6torque11ReportErrorIJRA30_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_,"aG",@progbits,_ZN2v88internal6torque11ReportErrorIJRA30_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_,comdat
.LLSDA7467:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE7467-.LLSDACSB7467
.LLSDACSB7467:
	.uleb128 .LEHB100-.LFB7467
	.uleb128 .LEHE100-.LEHB100
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB101-.LFB7467
	.uleb128 .LEHE101-.LEHB101
	.uleb128 .L699-.LFB7467
	.uleb128 0
	.uleb128 .LEHB102-.LFB7467
	.uleb128 .LEHE102-.LEHB102
	.uleb128 .L700-.LFB7467
	.uleb128 0
	.uleb128 .LEHB103-.LFB7467
	.uleb128 .LEHE103-.LEHB103
	.uleb128 .L698-.LFB7467
	.uleb128 0
	.uleb128 .LEHB104-.LFB7467
	.uleb128 .LEHE104-.LEHB104
	.uleb128 0
	.uleb128 0
.LLSDACSE7467:
	.section	.text._ZN2v88internal6torque11ReportErrorIJRA30_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_,"axG",@progbits,_ZN2v88internal6torque11ReportErrorIJRA30_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_,comdat
	.size	_ZN2v88internal6torque11ReportErrorIJRA30_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_, .-_ZN2v88internal6torque11ReportErrorIJRA30_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_
	.section	.text._ZN2v88internal6torque11ReportErrorIJRA77_KcRKNS1_4TypeEEEEvDpOT_,"axG",@progbits,_ZN2v88internal6torque11ReportErrorIJRA77_KcRKNS1_4TypeEEEEvDpOT_,comdat
	.p2align 4
	.weak	_ZN2v88internal6torque11ReportErrorIJRA77_KcRKNS1_4TypeEEEEvDpOT_
	.type	_ZN2v88internal6torque11ReportErrorIJRA77_KcRKNS1_4TypeEEEEvDpOT_, @function
_ZN2v88internal6torque11ReportErrorIJRA77_KcRKNS1_4TypeEEEEvDpOT_:
.LFB7950:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA7950
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-432(%rbp), %r14
	leaq	-416(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%r14, %rdi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	subq	$496, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
.LEHB105:
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEC1Ev@PLT
.LEHE105:
	movq	%r13, %rsi
	movq	%r15, %rdi
.LEHB106:
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal6torquelsERSoRKNS1_4TypeE
	leaq	-528(%rbp), %r13
	leaq	-408(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNKSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEE3strEv@PLT
.LEHE106:
	leaq	-496(%rbp), %r12
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
.LEHB107:
	call	_ZN2v88internal6torque14MessageBuilderC1ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_13TorqueMessage4KindE@PLT
.LEHE107:
	movq	-528(%rbp), %rdi
	leaq	-512(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L703
	call	_ZdlPv@PLT
.L703:
	movq	%r14, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r12, %rdi
.LEHB108:
	call	_ZNK2v88internal6torque14MessageBuilder5ThrowEv@PLT
.LEHE108:
.L710:
	endbr64
	movq	%rax, %r12
	jmp	.L706
.L709:
	endbr64
	movq	%rax, %r13
	jmp	.L707
.L711:
	endbr64
	movq	%rax, %r12
.L704:
	movq	-528(%rbp), %rdi
	leaq	-512(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L706
	call	_ZdlPv@PLT
.L706:
	movq	%r14, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r12, %rdi
.LEHB109:
	call	_Unwind_Resume@PLT
.L707:
	movq	%r12, %rdi
	call	_ZN2v88internal6torque14MessageBuilderD1Ev
	movq	%r13, %rdi
	call	_Unwind_Resume@PLT
.LEHE109:
	.cfi_endproc
.LFE7950:
	.section	.gcc_except_table._ZN2v88internal6torque11ReportErrorIJRA77_KcRKNS1_4TypeEEEEvDpOT_,"aG",@progbits,_ZN2v88internal6torque11ReportErrorIJRA77_KcRKNS1_4TypeEEEEvDpOT_,comdat
.LLSDA7950:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE7950-.LLSDACSB7950
.LLSDACSB7950:
	.uleb128 .LEHB105-.LFB7950
	.uleb128 .LEHE105-.LEHB105
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB106-.LFB7950
	.uleb128 .LEHE106-.LEHB106
	.uleb128 .L710-.LFB7950
	.uleb128 0
	.uleb128 .LEHB107-.LFB7950
	.uleb128 .LEHE107-.LEHB107
	.uleb128 .L711-.LFB7950
	.uleb128 0
	.uleb128 .LEHB108-.LFB7950
	.uleb128 .LEHE108-.LEHB108
	.uleb128 .L709-.LFB7950
	.uleb128 0
	.uleb128 .LEHB109-.LFB7950
	.uleb128 .LEHE109-.LEHB109
	.uleb128 0
	.uleb128 0
.LLSDACSE7950:
	.section	.text._ZN2v88internal6torque11ReportErrorIJRA77_KcRKNS1_4TypeEEEEvDpOT_,"axG",@progbits,_ZN2v88internal6torque11ReportErrorIJRA77_KcRKNS1_4TypeEEEEvDpOT_,comdat
	.size	_ZN2v88internal6torque11ReportErrorIJRA77_KcRKNS1_4TypeEEEEvDpOT_, .-_ZN2v88internal6torque11ReportErrorIJRA77_KcRKNS1_4TypeEEEEvDpOT_
	.section	.rodata._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_26ExternalRuntimeDeclarationE.str1.8,"aMS",@progbits,1
	.align 8
.LC30:
	.string	"first parameter to runtime functions has to be the context and have type Context, but found type "
	.align 8
.LC31:
	.string	"runtime functions can only return tagged values, but found type "
	.align 8
.LC32:
	.string	"runtime functions can only take tagged values as parameters, but found type "
	.section	.text.unlikely._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_26ExternalRuntimeDeclarationE,"ax",@progbits
	.align 2
.LCOLDB33:
	.section	.text._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_26ExternalRuntimeDeclarationE,"ax",@progbits
.LHOTB33:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_26ExternalRuntimeDeclarationE
	.type	_ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_26ExternalRuntimeDeclarationE, @function
_ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_26ExternalRuntimeDeclarationE:
.LFB6542:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6542
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$264, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -304(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-208(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -296(%rbp)
.LEHB110:
	call	_ZN2v88internal6torque11TypeVisitor13MakeSignatureEPKNS1_19CallableDeclarationE@PLT
.LEHE110:
	movq	-144(%rbp), %rdx
	cmpq	-136(%rbp), %rdx
	je	.L787
	movq	(%rdx), %r12
.LEHB111:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_10TypeOracleEEERPNT_5ScopeEv@PLT
.LEHE111:
	leaq	-272(%rbp), %r15
	movl	$30821, %ecx
	leaq	-256(%rbp), %rbx
	movl	$1953394499, -256(%rbp)
	movq	%r15, %rdi
	movq	%rbx, -272(%rbp)
	movw	%cx, -252(%rbp)
	movb	$116, -250(%rbp)
	movq	$7, -264(%rbp)
	movb	$0, -249(%rbp)
.LEHB112:
	call	_ZN2v88internal6torque12Declarations16LookupGlobalTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
.LEHE112:
	movq	-272(%rbp), %rdi
	movq	%rax, %r13
	cmpq	%rbx, %rdi
	je	.L715
	call	_ZdlPv@PLT
.L715:
	cmpq	%r13, %r12
	jne	.L788
	movq	-104(%rbp), %r12
	movq	(%r12), %rax
	movq	16(%rax), %r13
.LEHB113:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_10TypeOracleEEERPNT_5ScopeEv@PLT
.LEHE113:
	movl	$29795, %edx
	movq	%r15, %rdi
	movq	%rbx, -272(%rbp)
	movl	$1701470799, -256(%rbp)
	movw	%dx, -252(%rbp)
	movq	$6, -264(%rbp)
	movb	$0, -250(%rbp)
.LEHB114:
	call	_ZN2v88internal6torque12Declarations16LookupGlobalTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
.LEHE114:
	movq	-272(%rbp), %rdi
	movq	%rax, %r14
	cmpq	%rbx, %rdi
	je	.L721
	call	_ZdlPv@PLT
.L721:
	movq	%r14, %rsi
	movq	%r12, %rdi
.LEHB115:
	call	*%r13
	testb	%al, %al
	je	.L789
.L725:
	movq	-136(%rbp), %rax
	movq	-144(%rbp), %r12
	movq	%rax, -288(%rbp)
	cmpq	%r12, %rax
	je	.L743
	.p2align 4,,10
	.p2align 3
.L742:
	movq	(%r12), %r13
	movq	0(%r13), %rax
	movq	16(%rax), %r14
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_10TypeOracleEEERPNT_5ScopeEv@PLT
.LEHE115:
	movl	$29795, %eax
	movq	%rbx, -272(%rbp)
	movq	%r15, %rdi
	movl	$1701470799, (%rbx)
	movw	%ax, 4(%rbx)
	movq	$6, -264(%rbp)
	movb	$0, -250(%rbp)
.LEHB116:
	call	_ZN2v88internal6torque12Declarations16LookupGlobalTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
.LEHE116:
	movq	-272(%rbp), %rdi
	movq	%rax, %rsi
	cmpq	%rbx, %rdi
	je	.L737
	movq	%rax, -280(%rbp)
	call	_ZdlPv@PLT
	movq	-280(%rbp), %rsi
.L737:
	movq	%r13, %rdi
.LEHB117:
	call	*%r14
	testb	%al, %al
	je	.L790
	addq	$8, %r12
	cmpq	%r12, -288(%rbp)
	jne	.L742
.L743:
	movq	-304(%rbp), %rax
	movq	-296(%rbp), %rsi
	movq	40(%rax), %rdi
	addq	$32, %rdi
	call	_ZN2v88internal6torque12Declarations22DeclareRuntimeFunctionERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKNS1_9SignatureE@PLT
	movq	-88(%rbp), %rbx
	movq	-96(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L744
	.p2align 4,,10
	.p2align 3
.L748:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L745
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L748
.L746:
	movq	-96(%rbp), %r12
.L744:
	testq	%r12, %r12
	je	.L749
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L749:
	movq	-144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L750
	call	_ZdlPv@PLT
.L750:
	cmpb	$0, -184(%rbp)
	jne	.L791
.L751:
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L713
	call	_ZdlPv@PLT
.L713:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L792
	addq	$264, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L745:
	.cfi_restore_state
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L748
	jmp	.L746
	.p2align 4,,10
	.p2align 3
.L791:
	movq	-176(%rbp), %rdi
	leaq	-160(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L751
	call	_ZdlPv@PLT
	jmp	.L751
	.p2align 4,,10
	.p2align 3
.L789:
	movq	-104(%rbp), %r12
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_10TypeOracleEEERPNT_5ScopeEv@PLT
.LEHE117:
	movq	%r15, %rdi
	movq	%rbx, -272(%rbp)
	movl	$1684631414, -256(%rbp)
	movq	$4, -264(%rbp)
	movb	$0, -252(%rbp)
.LEHB118:
	call	_ZN2v88internal6torque12Declarations16LookupGlobalTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
.LEHE118:
	movq	-272(%rbp), %rdi
	movq	%rax, %r13
	cmpq	%rbx, %rdi
	je	.L726
	call	_ZdlPv@PLT
.L726:
	cmpq	%r13, %r12
	je	.L725
	movq	-104(%rbp), %r12
.LEHB119:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_10TypeOracleEEERPNT_5ScopeEv@PLT
.LEHE119:
	movq	%r15, %rdi
	movq	%rbx, -272(%rbp)
	movl	$1702258030, -256(%rbp)
	movb	$114, -252(%rbp)
	movq	$5, -264(%rbp)
	movb	$0, -251(%rbp)
.LEHB120:
	call	_ZN2v88internal6torque12Declarations16LookupGlobalTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
.LEHE120:
	movq	-272(%rbp), %rdi
	movq	%rax, %r13
	cmpq	%rbx, %rdi
	je	.L730
	call	_ZdlPv@PLT
.L730:
	cmpq	%r13, %r12
	je	.L725
	leaq	-104(%rbp), %rdx
	leaq	.LC31(%rip), %rsi
	movq	%r15, %rdi
.LEHB121:
	call	_ZN2v88internal6torqueL7MessageIJRA98_KcRPKNS1_4TypeEEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
.LEHE121:
	movq	%r15, %rdi
.LEHB122:
	call	_ZNK2v88internal6torque14MessageBuilder5ThrowEv@PLT
.LEHE122:
	.p2align 4,,10
	.p2align 3
.L790:
	movq	%r13, %rsi
	leaq	.LC32(%rip), %rdi
.LEHB123:
	call	_ZN2v88internal6torque11ReportErrorIJRA77_KcRKNS1_4TypeEEEEvDpOT_
.L787:
	leaq	-272(%rbp), %r15
.L714:
	leaq	.LC30(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal6torqueL7MessageIJRA98_KcRPKNS1_4TypeEEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
.LEHE123:
	movq	%r15, %rdi
.LEHB124:
	call	_ZNK2v88internal6torque14MessageBuilder5ThrowEv@PLT
.LEHE124:
.L792:
	call	__stack_chk_fail@PLT
.L788:
	movq	-144(%rbp), %rdx
	jmp	.L714
.L759:
	endbr64
	movq	%rax, %r12
	jmp	.L739
.L756:
	endbr64
	movq	%rax, %r12
	jmp	.L736
.L758:
	endbr64
	movq	%rax, %r12
	jmp	.L739
.L760:
	endbr64
	movq	%rax, %r12
	jmp	.L736
.L761:
	endbr64
	movq	%rax, %r12
	jmp	.L739
.L757:
	endbr64
	movq	%rax, %r12
	jmp	.L739
.L755:
	endbr64
	movq	%rax, %r12
	jmp	.L739
.L754:
	endbr64
	movq	%rax, %r12
	jmp	.L719
	.section	.gcc_except_table._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_26ExternalRuntimeDeclarationE,"a",@progbits
.LLSDA6542:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6542-.LLSDACSB6542
.LLSDACSB6542:
	.uleb128 .LEHB110-.LFB6542
	.uleb128 .LEHE110-.LEHB110
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB111-.LFB6542
	.uleb128 .LEHE111-.LEHB111
	.uleb128 .L754-.LFB6542
	.uleb128 0
	.uleb128 .LEHB112-.LFB6542
	.uleb128 .LEHE112-.LEHB112
	.uleb128 .L755-.LFB6542
	.uleb128 0
	.uleb128 .LEHB113-.LFB6542
	.uleb128 .LEHE113-.LEHB113
	.uleb128 .L754-.LFB6542
	.uleb128 0
	.uleb128 .LEHB114-.LFB6542
	.uleb128 .LEHE114-.LEHB114
	.uleb128 .L757-.LFB6542
	.uleb128 0
	.uleb128 .LEHB115-.LFB6542
	.uleb128 .LEHE115-.LEHB115
	.uleb128 .L754-.LFB6542
	.uleb128 0
	.uleb128 .LEHB116-.LFB6542
	.uleb128 .LEHE116-.LEHB116
	.uleb128 .L761-.LFB6542
	.uleb128 0
	.uleb128 .LEHB117-.LFB6542
	.uleb128 .LEHE117-.LEHB117
	.uleb128 .L754-.LFB6542
	.uleb128 0
	.uleb128 .LEHB118-.LFB6542
	.uleb128 .LEHE118-.LEHB118
	.uleb128 .L758-.LFB6542
	.uleb128 0
	.uleb128 .LEHB119-.LFB6542
	.uleb128 .LEHE119-.LEHB119
	.uleb128 .L754-.LFB6542
	.uleb128 0
	.uleb128 .LEHB120-.LFB6542
	.uleb128 .LEHE120-.LEHB120
	.uleb128 .L759-.LFB6542
	.uleb128 0
	.uleb128 .LEHB121-.LFB6542
	.uleb128 .LEHE121-.LEHB121
	.uleb128 .L754-.LFB6542
	.uleb128 0
	.uleb128 .LEHB122-.LFB6542
	.uleb128 .LEHE122-.LEHB122
	.uleb128 .L760-.LFB6542
	.uleb128 0
	.uleb128 .LEHB123-.LFB6542
	.uleb128 .LEHE123-.LEHB123
	.uleb128 .L754-.LFB6542
	.uleb128 0
	.uleb128 .LEHB124-.LFB6542
	.uleb128 .LEHE124-.LEHB124
	.uleb128 .L756-.LFB6542
	.uleb128 0
.LLSDACSE6542:
	.section	.text._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_26ExternalRuntimeDeclarationE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_26ExternalRuntimeDeclarationE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6542
	.type	_ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_26ExternalRuntimeDeclarationE.cold, @function
_ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_26ExternalRuntimeDeclarationE.cold:
.LFSB6542:
.L739:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-272(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L719
	call	_ZdlPv@PLT
	jmp	.L719
.L736:
	movq	%r15, %rdi
	call	_ZN2v88internal6torque14MessageBuilderD1Ev
.L719:
	movq	-296(%rbp), %rdi
	call	_ZN2v88internal6torque9SignatureD1Ev
	movq	%r12, %rdi
.LEHB125:
	call	_Unwind_Resume@PLT
.LEHE125:
	.cfi_endproc
.LFE6542:
	.section	.gcc_except_table._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_26ExternalRuntimeDeclarationE
.LLSDAC6542:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6542-.LLSDACSBC6542
.LLSDACSBC6542:
	.uleb128 .LEHB125-.LCOLDB33
	.uleb128 .LEHE125-.LEHB125
	.uleb128 0
	.uleb128 0
.LLSDACSEC6542:
	.section	.text.unlikely._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_26ExternalRuntimeDeclarationE
	.section	.text._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_26ExternalRuntimeDeclarationE
	.size	_ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_26ExternalRuntimeDeclarationE, .-_ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_26ExternalRuntimeDeclarationE
	.section	.text.unlikely._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_26ExternalRuntimeDeclarationE
	.size	_ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_26ExternalRuntimeDeclarationE.cold, .-_ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_26ExternalRuntimeDeclarationE.cold
.LCOLDE33:
	.section	.text._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_26ExternalRuntimeDeclarationE
.LHOTE33:
	.section	.text._ZN2v88internal6torque11ReportErrorIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_,"axG",@progbits,_ZN2v88internal6torque11ReportErrorIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_,comdat
	.p2align 4
	.weak	_ZN2v88internal6torque11ReportErrorIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_
	.type	_ZN2v88internal6torque11ReportErrorIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_, @function
_ZN2v88internal6torque11ReportErrorIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_:
.LFB7973:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA7973
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-432(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%r13, %rdi
	subq	$496, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
.LEHB126:
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEC1Ev@PLT
.LEHE126:
	movq	8(%rbx), %rdx
	movq	(%rbx), %rsi
	leaq	-416(%rbp), %rdi
.LEHB127:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	-528(%rbp), %r14
	leaq	-408(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZNKSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEE3strEv@PLT
.LEHE127:
	leaq	-496(%rbp), %r12
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
.LEHB128:
	call	_ZN2v88internal6torque14MessageBuilderC1ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_13TorqueMessage4KindE@PLT
.LEHE128:
	movq	-528(%rbp), %rdi
	leaq	-512(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L794
	call	_ZdlPv@PLT
.L794:
	movq	%r13, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r12, %rdi
.LEHB129:
	call	_ZNK2v88internal6torque14MessageBuilder5ThrowEv@PLT
.LEHE129:
.L801:
	endbr64
	movq	%rax, %r12
	jmp	.L797
.L800:
	endbr64
	movq	%rax, %r13
	jmp	.L798
.L802:
	endbr64
	movq	%rax, %r12
.L795:
	movq	-528(%rbp), %rdi
	leaq	-512(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L797
	call	_ZdlPv@PLT
.L797:
	movq	%r13, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r12, %rdi
.LEHB130:
	call	_Unwind_Resume@PLT
.L798:
	movq	%r12, %rdi
	call	_ZN2v88internal6torque14MessageBuilderD1Ev
	movq	%r13, %rdi
	call	_Unwind_Resume@PLT
.LEHE130:
	.cfi_endproc
.LFE7973:
	.section	.gcc_except_table._ZN2v88internal6torque11ReportErrorIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_,"aG",@progbits,_ZN2v88internal6torque11ReportErrorIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_,comdat
.LLSDA7973:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE7973-.LLSDACSB7973
.LLSDACSB7973:
	.uleb128 .LEHB126-.LFB7973
	.uleb128 .LEHE126-.LEHB126
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB127-.LFB7973
	.uleb128 .LEHE127-.LEHB127
	.uleb128 .L801-.LFB7973
	.uleb128 0
	.uleb128 .LEHB128-.LFB7973
	.uleb128 .LEHE128-.LEHB128
	.uleb128 .L802-.LFB7973
	.uleb128 0
	.uleb128 .LEHB129-.LFB7973
	.uleb128 .LEHE129-.LEHB129
	.uleb128 .L800-.LFB7973
	.uleb128 0
	.uleb128 .LEHB130-.LFB7973
	.uleb128 .LEHE130-.LEHB130
	.uleb128 0
	.uleb128 0
.LLSDACSE7973:
	.section	.text._ZN2v88internal6torque11ReportErrorIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_,"axG",@progbits,_ZN2v88internal6torque11ReportErrorIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_,comdat
	.size	_ZN2v88internal6torque11ReportErrorIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_, .-_ZN2v88internal6torque11ReportErrorIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_
	.section	.rodata._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_22ExternConstDeclarationE.str1.8,"aMS",@progbits,1
	.align 8
.LC34:
	.string	"extern constants must have constexpr type, but found: \""
	.section	.rodata._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_22ExternConstDeclarationE.str1.1,"aMS",@progbits,1
.LC35:
	.string	"\"\n"
	.section	.text.unlikely._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_22ExternConstDeclarationE,"ax",@progbits
	.align 2
.LCOLDB36:
	.section	.text._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_22ExternConstDeclarationE,"ax",@progbits
.LHOTB36:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_22ExternConstDeclarationE
	.type	_ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_22ExternConstDeclarationE, @function
_ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_22ExternConstDeclarationE:
.LFB6561:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6561
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$472, %rsp
	movq	40(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
.LEHB131:
	call	_ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_14TypeExpressionE@PLT
	movq	%rax, %r13
	movq	(%rax), %rax
	movq	%r13, %rdi
	call	*32(%rax)
.LEHE131:
	testb	%al, %al
	je	.L829
	movq	48(%rbx), %r8
	movq	56(%rbx), %r12
	leaq	-432(%rbp), %r14
	movq	%r14, -448(%rbp)
	movq	%r8, %rax
	addq	%r12, %rax
	je	.L806
	testq	%r8, %r8
	je	.L830
.L806:
	movq	%r12, -488(%rbp)
	cmpq	$15, %r12
	ja	.L831
	cmpq	$1, %r12
	jne	.L809
	movzbl	(%r8), %eax
	leaq	-448(%rbp), %r15
	movb	%al, -432(%rbp)
	movq	%r14, %rax
.L810:
	movq	%r12, -440(%rbp)
	movq	%r15, %rdx
	movq	%r13, %rsi
	movb	$0, (%rax,%r12)
	movq	32(%rbx), %rdi
.LEHB132:
	call	_ZN2v88internal6torque12Declarations21DeclareExternConstantEPNS1_10IdentifierEPKNS1_4TypeENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
.LEHE132:
	movq	-448(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L804
	call	_ZdlPv@PLT
.L804:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L832
	addq	$472, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L809:
	.cfi_restore_state
	testq	%r12, %r12
	jne	.L833
	movq	%r14, %rax
	leaq	-448(%rbp), %r15
	jmp	.L810
	.p2align 4,,10
	.p2align 3
.L831:
	leaq	-448(%rbp), %r15
	leaq	-488(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r8, -504(%rbp)
	movq	%r15, %rdi
.LEHB133:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-504(%rbp), %r8
	movq	%rax, -448(%rbp)
	movq	%rax, %rdi
	movq	-488(%rbp), %rax
	movq	%rax, -432(%rbp)
.L808:
	movq	%r12, %rdx
	movq	%r8, %rsi
	call	memcpy@PLT
	movq	-488(%rbp), %r12
	movq	-448(%rbp), %rax
	jmp	.L810
.L830:
	leaq	.LC4(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L832:
	call	__stack_chk_fail@PLT
.L829:
	leaq	-448(%rbp), %r15
	leaq	-432(%rbp), %r14
	movq	%r15, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEC1Ev@PLT
.LEHE133:
	movl	$55, %edx
	leaq	.LC34(%rip), %rsi
	movq	%r14, %rdi
.LEHB134:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal6torquelsERSoRKNS1_4TypeE
	movq	%rax, %rdi
	leaq	.LC35(%rip), %rsi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	leaq	-480(%rbp), %r12
	leaq	-424(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNKSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEE3strEv@PLT
.LEHE134:
	movq	%r12, %rdi
.LEHB135:
	call	_ZN2v88internal6torque11ReportErrorIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_
.LEHE135:
.L833:
	movq	%r14, %rdi
	leaq	-448(%rbp), %r15
	jmp	.L808
.L821:
	endbr64
	movq	%rax, %r12
	jmp	.L815
.L820:
	endbr64
	movq	%rax, %r12
	jmp	.L812
.L819:
	endbr64
	movq	%rax, %r12
	jmp	.L814
	.section	.gcc_except_table._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_22ExternConstDeclarationE,"a",@progbits
.LLSDA6561:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6561-.LLSDACSB6561
.LLSDACSB6561:
	.uleb128 .LEHB131-.LFB6561
	.uleb128 .LEHE131-.LEHB131
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB132-.LFB6561
	.uleb128 .LEHE132-.LEHB132
	.uleb128 .L821-.LFB6561
	.uleb128 0
	.uleb128 .LEHB133-.LFB6561
	.uleb128 .LEHE133-.LEHB133
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB134-.LFB6561
	.uleb128 .LEHE134-.LEHB134
	.uleb128 .L819-.LFB6561
	.uleb128 0
	.uleb128 .LEHB135-.LFB6561
	.uleb128 .LEHE135-.LEHB135
	.uleb128 .L820-.LFB6561
	.uleb128 0
.LLSDACSE6561:
	.section	.text._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_22ExternConstDeclarationE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_22ExternConstDeclarationE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6561
	.type	_ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_22ExternConstDeclarationE.cold, @function
_ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_22ExternConstDeclarationE.cold:
.LFSB6561:
.L815:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-448(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L816
	call	_ZdlPv@PLT
.L816:
	movq	%r12, %rdi
.LEHB136:
	call	_Unwind_Resume@PLT
.L812:
	movq	-480(%rbp), %rdi
	leaq	-464(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L814
	call	_ZdlPv@PLT
.L814:
	movq	%r15, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r12, %rdi
	call	_Unwind_Resume@PLT
.LEHE136:
	.cfi_endproc
.LFE6561:
	.section	.gcc_except_table._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_22ExternConstDeclarationE
.LLSDAC6561:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6561-.LLSDACSBC6561
.LLSDACSBC6561:
	.uleb128 .LEHB136-.LCOLDB36
	.uleb128 .LEHE136-.LEHB136
	.uleb128 0
	.uleb128 0
.LLSDACSEC6561:
	.section	.text.unlikely._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_22ExternConstDeclarationE
	.section	.text._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_22ExternConstDeclarationE
	.size	_ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_22ExternConstDeclarationE, .-_ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_22ExternConstDeclarationE
	.section	.text.unlikely._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_22ExternConstDeclarationE
	.size	_ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_22ExternConstDeclarationE.cold, .-_ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_22ExternConstDeclarationE.cold
.LCOLDE36:
	.section	.text._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_22ExternConstDeclarationE
.LHOTE36:
	.section	.rodata._ZN2v88internal6torque18DeclarationVisitor23DeclareSpecializedTypesERKNS1_17SpecializationKeyINS1_7GenericEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC37:
	.string	"Wrong generic argument count for specialization of \""
	.section	.rodata._ZN2v88internal6torque18DeclarationVisitor23DeclareSpecializedTypesERKNS1_17SpecializationKeyINS1_7GenericEEE.str1.1,"aMS",@progbits,1
.LC38:
	.string	"\", expected: "
.LC39:
	.string	", actual: "
	.section	.text.unlikely._ZN2v88internal6torque18DeclarationVisitor23DeclareSpecializedTypesERKNS1_17SpecializationKeyINS1_7GenericEEE,"ax",@progbits
	.align 2
.LCOLDB40:
	.section	.text._ZN2v88internal6torque18DeclarationVisitor23DeclareSpecializedTypesERKNS1_17SpecializationKeyINS1_7GenericEEE,"ax",@progbits
.LHOTB40:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque18DeclarationVisitor23DeclareSpecializedTypesERKNS1_17SpecializationKeyINS1_7GenericEEE
	.type	_ZN2v88internal6torque18DeclarationVisitor23DeclareSpecializedTypesERKNS1_17SpecializationKeyINS1_7GenericEEE, @function
_ZN2v88internal6torque18DeclarationVisitor23DeclareSpecializedTypesERKNS1_17SpecializationKeyINS1_7GenericEEE:
.LFB6563:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6563
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$456, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	104(%rax), %r12
	movq	40(%r12), %rax
	movq	32(%r12), %rsi
	movq	%rax, %r13
	subq	%rsi, %r13
	movq	%r13, %rbx
	sarq	$3, %rbx
	je	.L864
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rbx
	ja	.L843
	movq	%r13, %rdi
.LEHB137:
	call	_Znwm@PLT
	movq	32(%r12), %rsi
	movq	%rax, %rdi
	movq	40(%r12), %rax
	movq	%rax, %r13
	subq	%rsi, %r13
	movq	%r13, %rbx
	sarq	$3, %rbx
	cmpq	%rsi, %rax
	je	.L839
.L836:
	movq	%r13, %rdx
	call	memmove@PLT
	movq	%rax, %rdi
.L839:
	call	_ZdlPv@PLT
.L837:
	movq	16(%r14), %rax
	movq	8(%r14), %r12
	movq	%rax, -488(%rbp)
	subq	%r12, %rax
	cmpq	%r13, %rax
	jne	.L865
	xorl	%ebx, %ebx
	cmpq	-488(%rbp), %r12
	jne	.L841
	jmp	.L834
	.p2align 4,,10
	.p2align 3
.L866:
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rcx
	ja	.L843
	call	_Znwm@PLT
	movq	32(%r15), %rsi
	movq	%rax, %r8
	movq	40(%r15), %rax
	movq	%rax, %rdi
	subq	%rsi, %rdi
	cmpq	%rsi, %rax
	je	.L863
.L851:
	movq	%rdi, %rdx
	movq	%r8, %rdi
	call	memmove@PLT
	movq	%rax, %r8
.L863:
	movq	(%r8,%rbx), %r9
	movq	%r8, %rdi
	movq	%r9, -496(%rbp)
	call	_ZdlPv@PLT
	movq	-496(%rbp), %r9
.L850:
	movq	%r13, %rsi
	movq	%r9, %rdi
	addq	$8, %rbx
	call	_ZN2v88internal6torque12Declarations11DeclareTypeEPKNS1_10IdentifierEPKNS1_4TypeE@PLT
	movb	$0, 64(%rax)
	leaq	(%r12,%rbx), %rax
	cmpq	%rax, -488(%rbp)
	je	.L834
.L841:
	movq	(%r14), %rax
	movq	(%r12,%rbx), %r13
	movq	104(%rax), %r15
	movq	40(%r15), %rax
	movq	32(%r15), %rsi
	movq	%rax, %rdi
	subq	%rsi, %rdi
	movq	%rdi, %rcx
	sarq	$3, %rcx
	jne	.L866
	cmpq	%rax, %rsi
	jne	.L867
	movq	(%rbx), %r9
	jmp	.L850
	.p2align 4,,10
	.p2align 3
.L834:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L868
	addq	$456, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L864:
	.cfi_restore_state
	xorl	%edi, %edi
	cmpq	%rax, %rsi
	jne	.L836
	jmp	.L837
.L843:
	call	_ZSt17__throw_bad_allocv@PLT
.L865:
	leaq	-448(%rbp), %r13
	leaq	-432(%rbp), %r12
	movq	%r13, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEC1Ev@PLT
.LEHE137:
	movl	$52, %edx
	leaq	.LC37(%rip), %rsi
	movq	%r12, %rdi
.LEHB138:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r14), %rax
	movq	%r12, %rdi
	movq	80(%rax), %rdx
	movq	72(%rax), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$13, %edx
	leaq	.LC38(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$10, %edx
	leaq	.LC39(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	16(%r14), %rsi
	subq	8(%r14), %rsi
	movq	%r12, %rdi
	sarq	$3, %rsi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	leaq	-480(%rbp), %r12
	leaq	-424(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNKSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEE3strEv@PLT
.LEHE138:
	movq	%r12, %rdi
.LEHB139:
	call	_ZN2v88internal6torque11ReportErrorIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_
.LEHE139:
.L868:
	call	__stack_chk_fail@PLT
.L867:
	xorl	%r8d, %r8d
	jmp	.L851
.L858:
	endbr64
	movq	%rax, %r12
	jmp	.L846
.L857:
	endbr64
	movq	%rax, %r12
	jmp	.L848
	.section	.gcc_except_table._ZN2v88internal6torque18DeclarationVisitor23DeclareSpecializedTypesERKNS1_17SpecializationKeyINS1_7GenericEEE,"a",@progbits
.LLSDA6563:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6563-.LLSDACSB6563
.LLSDACSB6563:
	.uleb128 .LEHB137-.LFB6563
	.uleb128 .LEHE137-.LEHB137
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB138-.LFB6563
	.uleb128 .LEHE138-.LEHB138
	.uleb128 .L857-.LFB6563
	.uleb128 0
	.uleb128 .LEHB139-.LFB6563
	.uleb128 .LEHE139-.LEHB139
	.uleb128 .L858-.LFB6563
	.uleb128 0
.LLSDACSE6563:
	.section	.text._ZN2v88internal6torque18DeclarationVisitor23DeclareSpecializedTypesERKNS1_17SpecializationKeyINS1_7GenericEEE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque18DeclarationVisitor23DeclareSpecializedTypesERKNS1_17SpecializationKeyINS1_7GenericEEE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6563
	.type	_ZN2v88internal6torque18DeclarationVisitor23DeclareSpecializedTypesERKNS1_17SpecializationKeyINS1_7GenericEEE.cold, @function
_ZN2v88internal6torque18DeclarationVisitor23DeclareSpecializedTypesERKNS1_17SpecializationKeyINS1_7GenericEEE.cold:
.LFSB6563:
.L846:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-480(%rbp), %rdi
	leaq	-464(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L848
	call	_ZdlPv@PLT
.L848:
	movq	%r13, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r12, %rdi
.LEHB140:
	call	_Unwind_Resume@PLT
.LEHE140:
	.cfi_endproc
.LFE6563:
	.section	.gcc_except_table._ZN2v88internal6torque18DeclarationVisitor23DeclareSpecializedTypesERKNS1_17SpecializationKeyINS1_7GenericEEE
.LLSDAC6563:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6563-.LLSDACSBC6563
.LLSDACSBC6563:
	.uleb128 .LEHB140-.LCOLDB40
	.uleb128 .LEHE140-.LEHB140
	.uleb128 0
	.uleb128 0
.LLSDACSEC6563:
	.section	.text.unlikely._ZN2v88internal6torque18DeclarationVisitor23DeclareSpecializedTypesERKNS1_17SpecializationKeyINS1_7GenericEEE
	.section	.text._ZN2v88internal6torque18DeclarationVisitor23DeclareSpecializedTypesERKNS1_17SpecializationKeyINS1_7GenericEEE
	.size	_ZN2v88internal6torque18DeclarationVisitor23DeclareSpecializedTypesERKNS1_17SpecializationKeyINS1_7GenericEEE, .-_ZN2v88internal6torque18DeclarationVisitor23DeclareSpecializedTypesERKNS1_17SpecializationKeyINS1_7GenericEEE
	.section	.text.unlikely._ZN2v88internal6torque18DeclarationVisitor23DeclareSpecializedTypesERKNS1_17SpecializationKeyINS1_7GenericEEE
	.size	_ZN2v88internal6torque18DeclarationVisitor23DeclareSpecializedTypesERKNS1_17SpecializationKeyINS1_7GenericEEE.cold, .-_ZN2v88internal6torque18DeclarationVisitor23DeclareSpecializedTypesERKNS1_17SpecializationKeyINS1_7GenericEEE.cold
.LCOLDE40:
	.section	.text._ZN2v88internal6torque18DeclarationVisitor23DeclareSpecializedTypesERKNS1_17SpecializationKeyINS1_7GenericEEE
.LHOTE40:
	.section	.text.unlikely._ZN2v88internal6torque18DeclarationVisitor24MakeSpecializedSignatureERKNS1_17SpecializationKeyINS1_7GenericEEE,"ax",@progbits
	.align 2
.LCOLDB42:
	.section	.text._ZN2v88internal6torque18DeclarationVisitor24MakeSpecializedSignatureERKNS1_17SpecializationKeyINS1_7GenericEEE,"ax",@progbits
.LHOTB42:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque18DeclarationVisitor24MakeSpecializedSignatureERKNS1_17SpecializationKeyINS1_7GenericEEE
	.type	_ZN2v88internal6torque18DeclarationVisitor24MakeSpecializedSignatureERKNS1_17SpecializationKeyINS1_7GenericEEE, @function
_ZN2v88internal6torque18DeclarationVisitor24MakeSpecializedSignatureERKNS1_17SpecializationKeyINS1_7GenericEEE:
.LFB6564:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6564
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$264, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	16(%rax), %rax
	movq	%rax, -288(%rbp)
.LEHB141:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rax
	movq	%rax, -280(%rbp)
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
.LEHE141:
	leaq	-288(%rbp), %rdx
	movq	%rdx, (%rax)
	leaq	-240(%rbp), %rax
	movq	%rax, -296(%rbp)
	movq	%rax, -256(%rbp)
	leaq	16+_ZTVN2v88internal6torque10DeclarableE(%rip), %rax
	movl	$1886221407, -240(%rbp)
	movq	$4, -248(%rbp)
	movb	$0, -236(%rbp)
	movq	%rax, -224(%rbp)
	movl	$0, -216(%rbp)
.LEHB142:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rax
	movq	(%rax), %rax
	movq	%rax, -208(%rbp)
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
.LEHE142:
	movq	(%rax), %rax
	movq	-256(%rbp), %r15
	pcmpeqd	%xmm0, %xmm0
	leaq	-80(%rbp), %r14
	movq	-248(%rbp), %r12
	movdqu	(%rax), %xmm1
	movups	%xmm1, -200(%rbp)
	movl	16(%rax), %eax
	movl	$-1, -164(%rbp)
	movl	%eax, -184(%rbp)
	leaq	-104(%rbp), %rax
	movq	%rax, -304(%rbp)
	movq	%rax, -152(%rbp)
	leaq	16+_ZTVN2v88internal6torque9NamespaceE(%rip), %rax
	movq	%rax, -224(%rbp)
	movq	%r15, %rax
	addq	%r12, %rax
	movq	%r14, -96(%rbp)
	movb	$1, -160(%rbp)
	movq	$1, -144(%rbp)
	movq	$0, -136(%rbp)
	movq	$0, -128(%rbp)
	movl	$0x3f800000, -120(%rbp)
	movq	$0, -112(%rbp)
	movq	$0, -104(%rbp)
	movups	%xmm0, -180(%rbp)
	je	.L870
	testq	%r15, %r15
	je	.L924
.L870:
	movq	%r12, -272(%rbp)
	cmpq	$15, %r12
	ja	.L925
	cmpq	$1, %r12
	jne	.L873
	movzbl	(%r15), %eax
	movb	%al, -80(%rbp)
	movq	%r14, %rax
.L874:
	movq	%r12, -88(%rbp)
	movb	$0, (%rax,%r12)
	movq	-256(%rbp), %rdi
	cmpq	-296(%rbp), %rdi
	je	.L876
	call	_ZdlPv@PLT
.L876:
	leaq	-224(%rbp), %r12
	movq	%r12, -272(%rbp)
.LEHB143:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rax
	movq	%rax, -264(%rbp)
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
.LEHE143:
	leaq	-272(%rbp), %rdx
	movq	%rbx, %rdi
	movq	%rdx, (%rax)
.LEHB144:
	call	_ZN2v88internal6torque18DeclarationVisitor23DeclareSpecializedTypesERKNS1_17SpecializationKeyINS1_7GenericEEE
	movq	(%rbx), %rax
	movq	%r13, %rdi
	movq	104(%rax), %rax
	movq	56(%rax), %rsi
	call	_ZN2v88internal6torque11TypeVisitor13MakeSignatureEPKNS1_19CallableDeclarationE@PLT
.LEHE144:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
	movq	-264(%rbp), %rdx
	movq	%rdx, (%rax)
	movq	-96(%rbp), %rdi
	leaq	16+_ZTVN2v88internal6torque9NamespaceE(%rip), %rax
	movq	%rax, -224(%rbp)
	cmpq	%r14, %rdi
	je	.L884
	call	_ZdlPv@PLT
.L884:
	movq	-136(%rbp), %r12
	leaq	16+_ZTVN2v88internal6torque5ScopeE(%rip), %rax
	movq	%rax, -224(%rbp)
	testq	%r12, %r12
	jne	.L885
	jmp	.L890
	.p2align 4,,10
	.p2align 3
.L926:
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L890
.L891:
	movq	%rbx, %r12
.L885:
	movq	40(%r12), %rdi
	movq	(%r12), %rbx
	testq	%rdi, %rdi
	je	.L888
	call	_ZdlPv@PLT
.L888:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	jne	.L926
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L891
.L890:
	movq	-144(%rbp), %rax
	movq	-152(%rbp), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	$0, -128(%rbp)
	movq	-152(%rbp), %rdi
	movq	$0, -136(%rbp)
	cmpq	-304(%rbp), %rdi
	je	.L886
	call	_ZdlPv@PLT
.L886:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
	movq	-280(%rbp), %rdx
	movq	%rdx, (%rax)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L927
	addq	$264, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L873:
	.cfi_restore_state
	testq	%r12, %r12
	jne	.L928
	movq	%r14, %rax
	jmp	.L874
	.p2align 4,,10
	.p2align 3
.L925:
	leaq	-272(%rbp), %rsi
	leaq	-96(%rbp), %rdi
	xorl	%edx, %edx
.LEHB145:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -96(%rbp)
	movq	%rax, %rdi
	movq	-272(%rbp), %rax
	movq	%rax, -80(%rbp)
.L872:
	movq	%r12, %rdx
	movq	%r15, %rsi
	call	memcpy@PLT
	movq	-272(%rbp), %r12
	movq	-96(%rbp), %rax
	jmp	.L874
.L924:
	leaq	.LC4(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE145:
.L927:
	call	__stack_chk_fail@PLT
.L928:
	movq	%r14, %rdi
	jmp	.L872
.L901:
	endbr64
	movq	%rax, %r12
	jmp	.L877
.L898:
	endbr64
	movq	%rax, %r12
	jmp	.L883
.L899:
	endbr64
	movq	%rax, %rbx
	jmp	.L895
.L900:
	endbr64
	movq	%rax, %rbx
	jmp	.L894
	.section	.gcc_except_table._ZN2v88internal6torque18DeclarationVisitor24MakeSpecializedSignatureERKNS1_17SpecializationKeyINS1_7GenericEEE,"a",@progbits
.LLSDA6564:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6564-.LLSDACSB6564
.LLSDACSB6564:
	.uleb128 .LEHB141-.LFB6564
	.uleb128 .LEHE141-.LEHB141
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB142-.LFB6564
	.uleb128 .LEHE142-.LEHB142
	.uleb128 .L898-.LFB6564
	.uleb128 0
	.uleb128 .LEHB143-.LFB6564
	.uleb128 .LEHE143-.LEHB143
	.uleb128 .L899-.LFB6564
	.uleb128 0
	.uleb128 .LEHB144-.LFB6564
	.uleb128 .LEHE144-.LEHB144
	.uleb128 .L900-.LFB6564
	.uleb128 0
	.uleb128 .LEHB145-.LFB6564
	.uleb128 .LEHE145-.LEHB145
	.uleb128 .L901-.LFB6564
	.uleb128 0
.LLSDACSE6564:
	.section	.text._ZN2v88internal6torque18DeclarationVisitor24MakeSpecializedSignatureERKNS1_17SpecializationKeyINS1_7GenericEEE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque18DeclarationVisitor24MakeSpecializedSignatureERKNS1_17SpecializationKeyINS1_7GenericEEE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6564
	.type	_ZN2v88internal6torque18DeclarationVisitor24MakeSpecializedSignatureERKNS1_17SpecializationKeyINS1_7GenericEEE.cold, @function
_ZN2v88internal6torque18DeclarationVisitor24MakeSpecializedSignatureERKNS1_17SpecializationKeyINS1_7GenericEEE.cold:
.LFSB6564:
.L877:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	leaq	16+_ZTVN2v88internal6torque5ScopeE(%rip), %rax
	movq	-136(%rbp), %r13
	movq	%rax, -224(%rbp)
.L881:
	testq	%r13, %r13
	jne	.L929
	movq	-144(%rbp), %rax
	movq	-152(%rbp), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	$0, -128(%rbp)
	movq	-152(%rbp), %rdi
	movq	$0, -136(%rbp)
	cmpq	-304(%rbp), %rdi
	jne	.L930
.L883:
	movq	-256(%rbp), %rdi
	cmpq	-296(%rbp), %rdi
	je	.L893
	call	_ZdlPv@PLT
.L893:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
	movq	-280(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rdx, (%rax)
.LEHB146:
	call	_Unwind_Resume@PLT
.LEHE146:
.L894:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
	movq	-264(%rbp), %rdx
	movq	%rdx, (%rax)
.L895:
	movq	%r12, %rdi
	movq	%rbx, %r12
	call	_ZN2v88internal6torque9NamespaceD1Ev
	jmp	.L893
.L929:
	movq	40(%r13), %rdi
	movq	0(%r13), %rbx
	testq	%rdi, %rdi
	je	.L879
	call	_ZdlPv@PLT
.L879:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L880
	call	_ZdlPv@PLT
.L880:
	movq	%r13, %rdi
	movq	%rbx, %r13
	call	_ZdlPv@PLT
	jmp	.L881
.L930:
	call	_ZdlPv@PLT
	jmp	.L883
	.cfi_endproc
.LFE6564:
	.section	.gcc_except_table._ZN2v88internal6torque18DeclarationVisitor24MakeSpecializedSignatureERKNS1_17SpecializationKeyINS1_7GenericEEE
.LLSDAC6564:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6564-.LLSDACSBC6564
.LLSDACSBC6564:
	.uleb128 .LEHB146-.LCOLDB42
	.uleb128 .LEHE146-.LEHB146
	.uleb128 0
	.uleb128 0
.LLSDACSEC6564:
	.section	.text.unlikely._ZN2v88internal6torque18DeclarationVisitor24MakeSpecializedSignatureERKNS1_17SpecializationKeyINS1_7GenericEEE
	.section	.text._ZN2v88internal6torque18DeclarationVisitor24MakeSpecializedSignatureERKNS1_17SpecializationKeyINS1_7GenericEEE
	.size	_ZN2v88internal6torque18DeclarationVisitor24MakeSpecializedSignatureERKNS1_17SpecializationKeyINS1_7GenericEEE, .-_ZN2v88internal6torque18DeclarationVisitor24MakeSpecializedSignatureERKNS1_17SpecializationKeyINS1_7GenericEEE
	.section	.text.unlikely._ZN2v88internal6torque18DeclarationVisitor24MakeSpecializedSignatureERKNS1_17SpecializationKeyINS1_7GenericEEE
	.size	_ZN2v88internal6torque18DeclarationVisitor24MakeSpecializedSignatureERKNS1_17SpecializationKeyINS1_7GenericEEE.cold, .-_ZN2v88internal6torque18DeclarationVisitor24MakeSpecializedSignatureERKNS1_17SpecializationKeyINS1_7GenericEEE.cold
.LCOLDE42:
	.section	.text._ZN2v88internal6torque18DeclarationVisitor24MakeSpecializedSignatureERKNS1_17SpecializationKeyINS1_7GenericEEE
.LHOTE42:
	.section	.text._ZN2v88internal6torque11ReportErrorIJRA27_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA14_S3_RKSt6vectorIPKNS1_4TypeESaISJ_EERA15_S3_NS1_14SourcePositionEEEEvDpOT_,"axG",@progbits,_ZN2v88internal6torque11ReportErrorIJRA27_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA14_S3_RKSt6vectorIPKNS1_4TypeESaISJ_EERA15_S3_NS1_14SourcePositionEEEEvDpOT_,comdat
	.p2align 4
	.weak	_ZN2v88internal6torque11ReportErrorIJRA27_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA14_S3_RKSt6vectorIPKNS1_4TypeESaISJ_EERA15_S3_NS1_14SourcePositionEEEEvDpOT_
	.type	_ZN2v88internal6torque11ReportErrorIJRA27_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA14_S3_RKSt6vectorIPKNS1_4TypeESaISJ_EERA15_S3_NS1_14SourcePositionEEEEvDpOT_, @function
_ZN2v88internal6torque11ReportErrorIJRA27_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA14_S3_RKSt6vectorIPKNS1_4TypeESaISJ_EERA15_S3_NS1_14SourcePositionEEEEvDpOT_:
.LFB7990:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA7990
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-448(%rbp), %r15
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-432(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$568, %rsp
	movq	%rdi, -600(%rbp)
	movq	%r15, %rdi
	movq	%rcx, -584(%rbp)
	movq	%r8, -592(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
.LEHB147:
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEC1Ev@PLT
.LEHE147:
	movq	-600(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %rsi
.LEHB148:
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	8(%r13), %rdx
	movq	0(%r13), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	-584(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6torquelsERSoRKSt6vectorIPKNS1_4TypeESaIS6_EE@PLT
	movq	-592(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movdqu	(%rbx), %xmm0
	movl	16(%rbx), %eax
	leaq	-512(%rbp), %r13
	subq	$32, %rsp
	movq	%r13, %rdi
	movl	%eax, 16(%rsp)
	movups	%xmm0, (%rsp)
	.cfi_escape 0x2e,0x20
	call	_ZN2v88internal6torque16PositionAsStringB5cxx11ENS1_14SourcePositionE
.LEHE148:
	movq	-504(%rbp), %rdx
	movq	-512(%rbp), %rsi
	addq	$32, %rsp
	movq	%r12, %rdi
.LEHB149:
	.cfi_escape 0x2e,0
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE149:
	movq	-512(%rbp), %rdi
	leaq	-496(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L932
	call	_ZdlPv@PLT
.L932:
	leaq	-544(%rbp), %r12
	leaq	-424(%rbp), %rsi
	movq	%r12, %rdi
.LEHB150:
	call	_ZNKSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEE3strEv@PLT
.LEHE150:
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
.LEHB151:
	call	_ZN2v88internal6torque14MessageBuilderC1ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_13TorqueMessage4KindE@PLT
.LEHE151:
	movq	-544(%rbp), %rdi
	leaq	-528(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L937
	call	_ZdlPv@PLT
.L937:
	movq	%r15, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r13, %rdi
.LEHB152:
	call	_ZNK2v88internal6torque14MessageBuilder5ThrowEv@PLT
.LEHE152:
.L943:
	endbr64
	movq	%rax, %r12
	jmp	.L936
.L942:
	endbr64
	movq	%rax, %r12
	jmp	.L940
.L938:
	movq	-544(%rbp), %rdi
	leaq	-528(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L936
.L948:
	call	_ZdlPv@PLT
.L936:
	movq	%r15, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r12, %rdi
.LEHB153:
	call	_Unwind_Resume@PLT
.L940:
	movq	%r13, %rdi
	call	_ZN2v88internal6torque14MessageBuilderD1Ev
	movq	%r12, %rdi
	call	_Unwind_Resume@PLT
.LEHE153:
.L944:
	endbr64
	movq	%rax, %r12
	jmp	.L938
.L945:
	endbr64
	movq	%rax, %r12
.L934:
	movq	-512(%rbp), %rdi
	leaq	-496(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L948
	jmp	.L936
	.cfi_endproc
.LFE7990:
	.section	.gcc_except_table._ZN2v88internal6torque11ReportErrorIJRA27_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA14_S3_RKSt6vectorIPKNS1_4TypeESaISJ_EERA15_S3_NS1_14SourcePositionEEEEvDpOT_,"aG",@progbits,_ZN2v88internal6torque11ReportErrorIJRA27_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA14_S3_RKSt6vectorIPKNS1_4TypeESaISJ_EERA15_S3_NS1_14SourcePositionEEEEvDpOT_,comdat
.LLSDA7990:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE7990-.LLSDACSB7990
.LLSDACSB7990:
	.uleb128 .LEHB147-.LFB7990
	.uleb128 .LEHE147-.LEHB147
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB148-.LFB7990
	.uleb128 .LEHE148-.LEHB148
	.uleb128 .L943-.LFB7990
	.uleb128 0
	.uleb128 .LEHB149-.LFB7990
	.uleb128 .LEHE149-.LEHB149
	.uleb128 .L945-.LFB7990
	.uleb128 0
	.uleb128 .LEHB150-.LFB7990
	.uleb128 .LEHE150-.LEHB150
	.uleb128 .L943-.LFB7990
	.uleb128 0
	.uleb128 .LEHB151-.LFB7990
	.uleb128 .LEHE151-.LEHB151
	.uleb128 .L944-.LFB7990
	.uleb128 0
	.uleb128 .LEHB152-.LFB7990
	.uleb128 .LEHE152-.LEHB152
	.uleb128 .L942-.LFB7990
	.uleb128 0
	.uleb128 .LEHB153-.LFB7990
	.uleb128 .LEHE153-.LEHB153
	.uleb128 0
	.uleb128 0
.LLSDACSE7990:
	.section	.text._ZN2v88internal6torque11ReportErrorIJRA27_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA14_S3_RKSt6vectorIPKNS1_4TypeESaISJ_EERA15_S3_NS1_14SourcePositionEEEEvDpOT_,"axG",@progbits,_ZN2v88internal6torque11ReportErrorIJRA27_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA14_S3_RKSt6vectorIPKNS1_4TypeESaISJ_EERA15_S3_NS1_14SourcePositionEEEEvDpOT_,comdat
	.size	_ZN2v88internal6torque11ReportErrorIJRA27_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA14_S3_RKSt6vectorIPKNS1_4TypeESaISJ_EERA15_S3_NS1_14SourcePositionEEEEvDpOT_, .-_ZN2v88internal6torque11ReportErrorIJRA27_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA14_S3_RKSt6vectorIPKNS1_4TypeESaISJ_EERA15_S3_NS1_14SourcePositionEEEEvDpOT_
	.section	.text._ZN2v88internal6torque11ReportErrorIJRA36_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA14_S3_RKSt6vectorIPKNS1_4TypeESaISJ_EERA2_S3_EEEvDpOT_,"axG",@progbits,_ZN2v88internal6torque11ReportErrorIJRA36_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA14_S3_RKSt6vectorIPKNS1_4TypeESaISJ_EERA2_S3_EEEvDpOT_,comdat
	.p2align 4
	.weak	_ZN2v88internal6torque11ReportErrorIJRA36_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA14_S3_RKSt6vectorIPKNS1_4TypeESaISJ_EERA2_S3_EEEvDpOT_
	.type	_ZN2v88internal6torque11ReportErrorIJRA36_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA14_S3_RKSt6vectorIPKNS1_4TypeESaISJ_EERA2_S3_EEEvDpOT_, @function
_ZN2v88internal6torque11ReportErrorIJRA36_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA14_S3_RKSt6vectorIPKNS1_4TypeESaISJ_EERA2_S3_EEEvDpOT_:
.LFB8011:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA8011
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-448(%rbp), %r15
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-432(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$520, %rsp
	movq	%rdi, -560(%rbp)
	movq	%r15, %rdi
	movq	%r8, -552(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
.LEHB154:
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEC1Ev@PLT
.LEHE154:
	movq	-560(%rbp), %r9
	movq	%r12, %rdi
	movq	%r9, %rsi
.LEHB155:
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	8(%rbx), %rdx
	movq	(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6torquelsERSoRKSt6vectorIPKNS1_4TypeESaIS6_EE@PLT
	movq	-552(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	leaq	-544(%rbp), %r13
	leaq	-424(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNKSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEE3strEv@PLT
.LEHE155:
	leaq	-512(%rbp), %r12
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
.LEHB156:
	call	_ZN2v88internal6torque14MessageBuilderC1ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_13TorqueMessage4KindE@PLT
.LEHE156:
	movq	-544(%rbp), %rdi
	leaq	-528(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L950
	call	_ZdlPv@PLT
.L950:
	movq	%r15, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r12, %rdi
.LEHB157:
	call	_ZNK2v88internal6torque14MessageBuilder5ThrowEv@PLT
.LEHE157:
.L957:
	endbr64
	movq	%rax, %r12
	jmp	.L953
.L956:
	endbr64
	movq	%rax, %r13
	jmp	.L954
.L958:
	endbr64
	movq	%rax, %r12
.L951:
	movq	-544(%rbp), %rdi
	leaq	-528(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L953
	call	_ZdlPv@PLT
.L953:
	movq	%r15, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r12, %rdi
.LEHB158:
	call	_Unwind_Resume@PLT
.L954:
	movq	%r12, %rdi
	call	_ZN2v88internal6torque14MessageBuilderD1Ev
	movq	%r13, %rdi
	call	_Unwind_Resume@PLT
.LEHE158:
	.cfi_endproc
.LFE8011:
	.section	.gcc_except_table._ZN2v88internal6torque11ReportErrorIJRA36_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA14_S3_RKSt6vectorIPKNS1_4TypeESaISJ_EERA2_S3_EEEvDpOT_,"aG",@progbits,_ZN2v88internal6torque11ReportErrorIJRA36_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA14_S3_RKSt6vectorIPKNS1_4TypeESaISJ_EERA2_S3_EEEvDpOT_,comdat
.LLSDA8011:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE8011-.LLSDACSB8011
.LLSDACSB8011:
	.uleb128 .LEHB154-.LFB8011
	.uleb128 .LEHE154-.LEHB154
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB155-.LFB8011
	.uleb128 .LEHE155-.LEHB155
	.uleb128 .L957-.LFB8011
	.uleb128 0
	.uleb128 .LEHB156-.LFB8011
	.uleb128 .LEHE156-.LEHB156
	.uleb128 .L958-.LFB8011
	.uleb128 0
	.uleb128 .LEHB157-.LFB8011
	.uleb128 .LEHE157-.LEHB157
	.uleb128 .L956-.LFB8011
	.uleb128 0
	.uleb128 .LEHB158-.LFB8011
	.uleb128 .LEHE158-.LEHB158
	.uleb128 0
	.uleb128 0
.LLSDACSE8011:
	.section	.text._ZN2v88internal6torque11ReportErrorIJRA36_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA14_S3_RKSt6vectorIPKNS1_4TypeESaISJ_EERA2_S3_EEEvDpOT_,"axG",@progbits,_ZN2v88internal6torque11ReportErrorIJRA36_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA14_S3_RKSt6vectorIPKNS1_4TypeESaISJ_EERA2_S3_EEEvDpOT_,comdat
	.size	_ZN2v88internal6torque11ReportErrorIJRA36_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA14_S3_RKSt6vectorIPKNS1_4TypeESaISJ_EERA2_S3_EEEvDpOT_, .-_ZN2v88internal6torque11ReportErrorIJRA36_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA14_S3_RKSt6vectorIPKNS1_4TypeESaISJ_EERA2_S3_EEEvDpOT_
	.section	.rodata._ZNSt6vectorIPKN2v88internal6torque4TypeESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_.str1.1,"aMS",@progbits,1
.LC43:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorIPKN2v88internal6torque4TypeESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_,"axG",@progbits,_ZNSt6vectorIPKN2v88internal6torque4TypeESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPKN2v88internal6torque4TypeESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	.type	_ZNSt6vectorIPKN2v88internal6torque4TypeESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_, @function
_ZNSt6vectorIPKN2v88internal6torque4TypeESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_:
.LFB9553:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r9
	movq	%r13, %rax
	subq	%r9, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L974
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L970
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L975
.L962:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L969:
	movq	(%r15), %rax
	subq	%r8, %r13
	leaq	8(%rbx,%rdx), %r15
	movq	%rax, (%rbx,%rdx)
	leaq	(%r15,%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L976
	testq	%r13, %r13
	jg	.L965
	testq	%r9, %r9
	jne	.L968
.L966:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L976:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jg	.L965
.L968:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L966
	.p2align 4,,10
	.p2align 3
.L975:
	testq	%rsi, %rsi
	jne	.L963
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L969
	.p2align 4,,10
	.p2align 3
.L965:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L966
	jmp	.L968
	.p2align 4,,10
	.p2align 3
.L970:
	movl	$8, %r14d
	jmp	.L962
.L974:
	leaq	.LC43(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L963:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$3, %r14
	jmp	.L962
	.cfi_endproc
.LFE9553:
	.size	_ZNSt6vectorIPKN2v88internal6torque4TypeESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_, .-_ZNSt6vectorIPKN2v88internal6torque4TypeESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	.section	.text._ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_,"axG",@progbits,_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	.type	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_, @function
_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_:
.LFB9681:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r12
	movq	(%rdi), %r15
	movabsq	$288230376151711743, %rdi
	movq	%r12, %rax
	subq	%r15, %rax
	sarq	$5, %rax
	cmpq	%rdi, %rax
	je	.L1004
	movq	%rsi, %rcx
	movq	%rsi, %rbx
	subq	%r15, %rcx
	testq	%rax, %rax
	je	.L995
	movabsq	$9223372036854775776, %rsi
	leaq	(%rax,%rax), %r8
	cmpq	%r8, %rax
	jbe	.L1005
.L979:
	movq	%rsi, %rdi
	movq	%rdx, -72(%rbp)
	movq	%rcx, -64(%rbp)
	movq	%rsi, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rsi
	movq	-64(%rbp), %rcx
	movq	%rax, %r14
	movq	-72(%rbp), %rdx
	leaq	(%rax,%rsi), %rax
	leaq	32(%r14), %r8
.L994:
	addq	%r14, %rcx
	movq	(%rdx), %rdi
	leaq	16(%rcx), %rsi
	movq	%rsi, (%rcx)
	leaq	16(%rdx), %rsi
	cmpq	%rsi, %rdi
	je	.L1006
	movq	%rdi, (%rcx)
	movq	16(%rdx), %rdi
	movq	%rdi, 16(%rcx)
.L982:
	movq	8(%rdx), %rdi
	movq	%rsi, (%rdx)
	movq	$0, 8(%rdx)
	movq	%rdi, 8(%rcx)
	movb	$0, 16(%rdx)
	cmpq	%r15, %rbx
	je	.L983
	movq	%r14, %rcx
	movq	%r15, %rdx
	jmp	.L987
	.p2align 4,,10
	.p2align 3
.L984:
	movq	%rdi, (%rcx)
	movq	16(%rdx), %rsi
	movq	%rsi, 16(%rcx)
.L1003:
	movq	8(%rdx), %rsi
	addq	$32, %rdx
	addq	$32, %rcx
	movq	%rsi, -24(%rcx)
	cmpq	%rdx, %rbx
	je	.L1007
.L987:
	leaq	16(%rcx), %rsi
	movq	%rsi, (%rcx)
	movq	(%rdx), %rdi
	leaq	16(%rdx), %rsi
	cmpq	%rsi, %rdi
	jne	.L984
	movdqu	16(%rdx), %xmm1
	movups	%xmm1, 16(%rcx)
	jmp	.L1003
	.p2align 4,,10
	.p2align 3
.L1007:
	movq	%rbx, %rdx
	subq	%r15, %rdx
	leaq	32(%r14,%rdx), %r8
.L983:
	cmpq	%r12, %rbx
	je	.L988
	movq	%rbx, %rdx
	movq	%r8, %rcx
	.p2align 4,,10
	.p2align 3
.L992:
	leaq	16(%rcx), %rsi
	movq	(%rdx), %rdi
	movq	%rsi, (%rcx)
	leaq	16(%rdx), %rsi
	cmpq	%rsi, %rdi
	je	.L1008
	movq	16(%rdx), %rsi
	addq	$32, %rdx
	movq	%rdi, (%rcx)
	addq	$32, %rcx
	movq	%rsi, -16(%rcx)
	movq	-24(%rdx), %rsi
	movq	%rsi, -24(%rcx)
	cmpq	%r12, %rdx
	jne	.L992
.L990:
	subq	%rbx, %r12
	addq	%r12, %r8
.L988:
	testq	%r15, %r15
	je	.L993
	movq	%r15, %rdi
	movq	%rax, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-64(%rbp), %rax
	movq	-56(%rbp), %r8
.L993:
	movq	%r14, %xmm0
	movq	%r8, %xmm3
	movq	%rax, 16(%r13)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 0(%r13)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1008:
	.cfi_restore_state
	movdqu	16(%rdx), %xmm2
	movq	8(%rdx), %rsi
	addq	$32, %rdx
	addq	$32, %rcx
	movups	%xmm2, -16(%rcx)
	movq	%rsi, -24(%rcx)
	cmpq	%rdx, %r12
	jne	.L992
	jmp	.L990
	.p2align 4,,10
	.p2align 3
.L1005:
	testq	%r8, %r8
	jne	.L980
	movl	$32, %r8d
	xorl	%eax, %eax
	xorl	%r14d, %r14d
	jmp	.L994
	.p2align 4,,10
	.p2align 3
.L995:
	movl	$32, %esi
	jmp	.L979
	.p2align 4,,10
	.p2align 3
.L1006:
	movdqu	16(%rdx), %xmm4
	movups	%xmm4, 16(%rcx)
	jmp	.L982
.L980:
	cmpq	%rdi, %r8
	movq	%rdi, %rax
	cmovbe	%r8, %rax
	salq	$5, %rax
	movq	%rax, %rsi
	jmp	.L979
.L1004:
	leaq	.LC43(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE9681:
	.size	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_, .-_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	.section	.text.unlikely._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_21CppIncludeDeclarationE,"ax",@progbits
	.align 2
.LCOLDB44:
	.section	.text._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_21CppIncludeDeclarationE,"ax",@progbits
.LHOTB44:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_21CppIncludeDeclarationE
	.type	_ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_21CppIncludeDeclarationE, @function
_ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_21CppIncludeDeclarationE:
.LFB6562:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6562
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	leaq	-64(%rbp), %rbx
	subq	$72, %rsp
	movq	32(%rdi), %r13
	movq	40(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%rbx, -80(%rbp)
	movq	%r13, %rax
	addq	%r12, %rax
	je	.L1010
	testq	%r13, %r13
	je	.L1031
.L1010:
	movq	%r12, -88(%rbp)
	cmpq	$15, %r12
	ja	.L1032
	cmpq	$1, %r12
	jne	.L1013
	movzbl	0(%r13), %eax
	movb	%al, -64(%rbp)
	movq	%rbx, %rax
.L1014:
	movq	%r12, -72(%rbp)
	movb	$0, (%rax,%r12)
.LEHB159:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_13GlobalContextEEERPNT_5ScopeEv@PLT
.LEHE159:
	movq	(%rax), %rdi
	movq	144(%rdi), %rsi
	cmpq	152(%rdi), %rsi
	je	.L1015
	leaq	16(%rsi), %rax
	movq	%rax, (%rsi)
	movq	-80(%rbp), %rax
	cmpq	%rbx, %rax
	je	.L1033
	movq	%rax, (%rsi)
	movq	-64(%rbp), %rax
	movq	%rax, 16(%rsi)
.L1017:
	movq	-72(%rbp), %rax
	movq	%rax, 8(%rsi)
	addq	$32, 144(%rdi)
.L1009:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1034
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1013:
	.cfi_restore_state
	testq	%r12, %r12
	jne	.L1035
	movq	%rbx, %rax
	jmp	.L1014
	.p2align 4,,10
	.p2align 3
.L1032:
	leaq	-80(%rbp), %rdi
	leaq	-88(%rbp), %rsi
	xorl	%edx, %edx
.LEHB160:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE160:
	movq	%rax, -80(%rbp)
	movq	%rax, %rdi
	movq	-88(%rbp), %rax
	movq	%rax, -64(%rbp)
.L1012:
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	-88(%rbp), %r12
	movq	-80(%rbp), %rax
	jmp	.L1014
	.p2align 4,,10
	.p2align 3
.L1033:
	movdqa	-64(%rbp), %xmm0
	movups	%xmm0, 16(%rsi)
	jmp	.L1017
	.p2align 4,,10
	.p2align 3
.L1015:
	leaq	-80(%rbp), %rdx
	addq	$136, %rdi
.LEHB161:
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
.LEHE161:
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1009
	call	_ZdlPv@PLT
	jmp	.L1009
.L1031:
	leaq	.LC4(%rip), %rdi
.LEHB162:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE162:
.L1034:
	call	__stack_chk_fail@PLT
.L1035:
	movq	%rbx, %rdi
	jmp	.L1012
.L1023:
	endbr64
	movq	%rax, %r12
	jmp	.L1019
	.section	.gcc_except_table._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_21CppIncludeDeclarationE,"a",@progbits
.LLSDA6562:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6562-.LLSDACSB6562
.LLSDACSB6562:
	.uleb128 .LEHB159-.LFB6562
	.uleb128 .LEHE159-.LEHB159
	.uleb128 .L1023-.LFB6562
	.uleb128 0
	.uleb128 .LEHB160-.LFB6562
	.uleb128 .LEHE160-.LEHB160
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB161-.LFB6562
	.uleb128 .LEHE161-.LEHB161
	.uleb128 .L1023-.LFB6562
	.uleb128 0
	.uleb128 .LEHB162-.LFB6562
	.uleb128 .LEHE162-.LEHB162
	.uleb128 0
	.uleb128 0
.LLSDACSE6562:
	.section	.text._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_21CppIncludeDeclarationE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_21CppIncludeDeclarationE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6562
	.type	_ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_21CppIncludeDeclarationE.cold, @function
_ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_21CppIncludeDeclarationE.cold:
.LFSB6562:
.L1019:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1020
	call	_ZdlPv@PLT
.L1020:
	movq	%r12, %rdi
.LEHB163:
	call	_Unwind_Resume@PLT
.LEHE163:
	.cfi_endproc
.LFE6562:
	.section	.gcc_except_table._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_21CppIncludeDeclarationE
.LLSDAC6562:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6562-.LLSDACSBC6562
.LLSDACSBC6562:
	.uleb128 .LEHB163-.LCOLDB44
	.uleb128 .LEHE163-.LEHB163
	.uleb128 0
	.uleb128 0
.LLSDACSEC6562:
	.section	.text.unlikely._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_21CppIncludeDeclarationE
	.section	.text._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_21CppIncludeDeclarationE
	.size	_ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_21CppIncludeDeclarationE, .-_ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_21CppIncludeDeclarationE
	.section	.text.unlikely._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_21CppIncludeDeclarationE
	.size	_ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_21CppIncludeDeclarationE.cold, .-_ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_21CppIncludeDeclarationE.cold
.LCOLDE44:
	.section	.text._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_21CppIncludeDeclarationE
.LHOTE44:
	.section	.text._ZNSt6vectorIPN2v88internal6torque9NamespaceESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_,"axG",@progbits,_ZNSt6vectorIPN2v88internal6torque9NamespaceESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPN2v88internal6torque9NamespaceESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	.type	_ZNSt6vectorIPN2v88internal6torque9NamespaceESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_, @function
_ZNSt6vectorIPN2v88internal6torque9NamespaceESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_:
.LFB10075:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r9
	movq	%r13, %rax
	subq	%r9, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L1050
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L1046
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L1051
.L1038:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L1045:
	movq	(%r15), %rax
	subq	%r8, %r13
	leaq	8(%rbx,%rdx), %r15
	movq	%rax, (%rbx,%rdx)
	leaq	(%r15,%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L1052
	testq	%r13, %r13
	jg	.L1041
	testq	%r9, %r9
	jne	.L1044
.L1042:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1052:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jg	.L1041
.L1044:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L1042
	.p2align 4,,10
	.p2align 3
.L1051:
	testq	%rsi, %rsi
	jne	.L1039
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L1045
	.p2align 4,,10
	.p2align 3
.L1041:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L1042
	jmp	.L1044
	.p2align 4,,10
	.p2align 3
.L1046:
	movl	$8, %r14d
	jmp	.L1038
.L1050:
	leaq	.LC43(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1039:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$3, %r14
	jmp	.L1038
	.cfi_endproc
.LFE10075:
	.size	_ZNSt6vectorIPN2v88internal6torque9NamespaceESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_, .-_ZNSt6vectorIPN2v88internal6torque9NamespaceESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	.section	.text._ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_St6vectorIPN2v88internal6torque10DeclarableESaISD_EEESaISG_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSI_18_Mod_range_hashingENSI_20_Default_ranged_hashENSI_20_Prime_rehash_policyENSI_17_Hashtable_traitsILb1ELb0ELb1EEEE9_M_rehashEmRKm,"axG",@progbits,_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_St6vectorIPN2v88internal6torque10DeclarableESaISD_EEESaISG_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSI_18_Mod_range_hashingENSI_20_Default_ranged_hashENSI_20_Prime_rehash_policyENSI_17_Hashtable_traitsILb1ELb0ELb1EEEE9_M_rehashEmRKm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_St6vectorIPN2v88internal6torque10DeclarableESaISD_EEESaISG_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSI_18_Mod_range_hashingENSI_20_Default_ranged_hashENSI_20_Prime_rehash_policyENSI_17_Hashtable_traitsILb1ELb0ELb1EEEE9_M_rehashEmRKm
	.type	_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_St6vectorIPN2v88internal6torque10DeclarableESaISD_EEESaISG_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSI_18_Mod_range_hashingENSI_20_Default_ranged_hashENSI_20_Prime_rehash_policyENSI_17_Hashtable_traitsILb1ELb0ELb1EEEE9_M_rehashEmRKm, @function
_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_St6vectorIPN2v88internal6torque10DeclarableESaISD_EEESaISG_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSI_18_Mod_range_hashingENSI_20_Default_ranged_hashENSI_20_Prime_rehash_policyENSI_17_Hashtable_traitsILb1ELb0ELb1EEEE9_M_rehashEmRKm:
.LFB10382:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA10382
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	cmpq	$1, %rsi
	je	.L1075
	movabsq	$1152921504606846975, %rax
	movq	%rdx, %r13
	cmpq	%rax, %rsi
	ja	.L1076
	leaq	0(,%rsi,8), %r14
	movq	%r14, %rdi
.LEHB164:
	call	_Znwm@PLT
	movq	%r14, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	memset@PLT
	leaq	48(%rbx), %r9
.L1055:
	movq	16(%rbx), %rsi
	movq	$0, 16(%rbx)
	testq	%rsi, %rsi
	je	.L1057
	xorl	%r8d, %r8d
	leaq	16(%rbx), %r10
	jmp	.L1058
	.p2align 4,,10
	.p2align 3
.L1059:
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	(%rdi), %rax
	movq	%rcx, (%rax)
.L1060:
	testq	%rsi, %rsi
	je	.L1057
.L1058:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	64(%rcx), %rax
	divq	%r12
	leaq	0(%r13,%rdx,8), %rdi
	movq	(%rdi), %rax
	testq	%rax, %rax
	jne	.L1059
	movq	16(%rbx), %rax
	movq	%rax, (%rcx)
	movq	%rcx, 16(%rbx)
	movq	%r10, (%rdi)
	cmpq	$0, (%rcx)
	je	.L1064
	movq	%rcx, 0(%r13,%r8,8)
	movq	%rdx, %r8
	testq	%rsi, %rsi
	jne	.L1058
	.p2align 4,,10
	.p2align 3
.L1057:
	movq	(%rbx), %rdi
	cmpq	%r9, %rdi
	je	.L1061
	call	_ZdlPv@PLT
.L1061:
	movq	%r12, 8(%rbx)
	movq	%r13, (%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1064:
	.cfi_restore_state
	movq	%rdx, %r8
	jmp	.L1060
	.p2align 4,,10
	.p2align 3
.L1075:
	leaq	48(%rdi), %r13
	movq	$0, 48(%rdi)
	movq	%r13, %r9
	jmp	.L1055
.L1076:
	call	_ZSt17__throw_bad_allocv@PLT
.LEHE164:
.L1065:
	endbr64
	movq	%rax, %rdi
.L1062:
	call	__cxa_begin_catch@PLT
	movq	0(%r13), %rax
	movq	%rax, 40(%rbx)
.LEHB165:
	call	__cxa_rethrow@PLT
.LEHE165:
.L1066:
	endbr64
	movq	%rax, %r12
.L1063:
	call	__cxa_end_catch@PLT
	movq	%r12, %rdi
.LEHB166:
	call	_Unwind_Resume@PLT
.LEHE166:
	.cfi_endproc
.LFE10382:
	.section	.gcc_except_table._ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_St6vectorIPN2v88internal6torque10DeclarableESaISD_EEESaISG_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSI_18_Mod_range_hashingENSI_20_Default_ranged_hashENSI_20_Prime_rehash_policyENSI_17_Hashtable_traitsILb1ELb0ELb1EEEE9_M_rehashEmRKm,"aG",@progbits,_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_St6vectorIPN2v88internal6torque10DeclarableESaISD_EEESaISG_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSI_18_Mod_range_hashingENSI_20_Default_ranged_hashENSI_20_Prime_rehash_policyENSI_17_Hashtable_traitsILb1ELb0ELb1EEEE9_M_rehashEmRKm,comdat
	.align 4
.LLSDA10382:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATT10382-.LLSDATTD10382
.LLSDATTD10382:
	.byte	0x1
	.uleb128 .LLSDACSE10382-.LLSDACSB10382
.LLSDACSB10382:
	.uleb128 .LEHB164-.LFB10382
	.uleb128 .LEHE164-.LEHB164
	.uleb128 .L1065-.LFB10382
	.uleb128 0x1
	.uleb128 .LEHB165-.LFB10382
	.uleb128 .LEHE165-.LEHB165
	.uleb128 .L1066-.LFB10382
	.uleb128 0
	.uleb128 .LEHB166-.LFB10382
	.uleb128 .LEHE166-.LEHB166
	.uleb128 0
	.uleb128 0
.LLSDACSE10382:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATT10382:
	.section	.text._ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_St6vectorIPN2v88internal6torque10DeclarableESaISD_EEESaISG_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSI_18_Mod_range_hashingENSI_20_Default_ranged_hashENSI_20_Prime_rehash_policyENSI_17_Hashtable_traitsILb1ELb0ELb1EEEE9_M_rehashEmRKm,"axG",@progbits,_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_St6vectorIPN2v88internal6torque10DeclarableESaISD_EEESaISG_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSI_18_Mod_range_hashingENSI_20_Default_ranged_hashENSI_20_Prime_rehash_policyENSI_17_Hashtable_traitsILb1ELb0ELb1EEEE9_M_rehashEmRKm,comdat
	.size	_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_St6vectorIPN2v88internal6torque10DeclarableESaISD_EEESaISG_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSI_18_Mod_range_hashingENSI_20_Default_ranged_hashENSI_20_Prime_rehash_policyENSI_17_Hashtable_traitsILb1ELb0ELb1EEEE9_M_rehashEmRKm, .-_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_St6vectorIPN2v88internal6torque10DeclarableESaISD_EEESaISG_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSI_18_Mod_range_hashingENSI_20_Default_ranged_hashENSI_20_Prime_rehash_policyENSI_17_Hashtable_traitsILb1ELb0ELb1EEEE9_M_rehashEmRKm
	.section	.text._ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_St6vectorIPN2v88internal6torque10DeclarableESaISD_EEESaISG_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSI_18_Mod_range_hashingENSI_20_Default_ranged_hashENSI_20_Prime_rehash_policyENSI_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSI_10_Hash_nodeISG_Lb1EEEm,"axG",@progbits,_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_St6vectorIPN2v88internal6torque10DeclarableESaISD_EEESaISG_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSI_18_Mod_range_hashingENSI_20_Default_ranged_hashENSI_20_Prime_rehash_policyENSI_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSI_10_Hash_nodeISG_Lb1EEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_St6vectorIPN2v88internal6torque10DeclarableESaISD_EEESaISG_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSI_18_Mod_range_hashingENSI_20_Default_ranged_hashENSI_20_Prime_rehash_policyENSI_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSI_10_Hash_nodeISG_Lb1EEEm
	.type	_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_St6vectorIPN2v88internal6torque10DeclarableESaISD_EEESaISG_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSI_18_Mod_range_hashingENSI_20_Default_ranged_hashENSI_20_Prime_rehash_policyENSI_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSI_10_Hash_nodeISG_Lb1EEEm, @function
_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_St6vectorIPN2v88internal6torque10DeclarableESaISD_EEESaISG_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSI_18_Mod_range_hashingENSI_20_Default_ranged_hashENSI_20_Prime_rehash_policyENSI_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSI_10_Hash_nodeISG_Lb1EEEm:
.LFB9597:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA9597
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rcx, %r12
	movq	%r8, %rcx
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	addq	$32, %rdi
	subq	$16, %rsp
	movq	-8(%rdi), %rdx
	movq	-24(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	%rax, -48(%rbp)
.LEHB167:
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
.LEHE167:
	testb	%al, %al
	je	.L1078
	movq	%rdx, %rsi
	movq	%rbx, %rdi
	leaq	-48(%rbp), %rdx
.LEHB168:
	call	_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_St6vectorIPN2v88internal6torque10DeclarableESaISD_EEESaISG_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSI_18_Mod_range_hashingENSI_20_Default_ranged_hashENSI_20_Prime_rehash_policyENSI_17_Hashtable_traitsILb1ELb0ELb1EEEE9_M_rehashEmRKm
.LEHE168:
	movq	%r14, %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	%rdx, %r13
.L1078:
	movq	%r14, 64(%r12)
	movq	(%rbx), %rax
	leaq	0(,%r13,8), %rcx
	movq	(%rax,%r13,8), %rax
	testq	%rax, %rax
	je	.L1079
	movq	(%rax), %rax
	movq	%rax, (%r12)
	movq	(%rbx), %rax
	movq	(%rax,%r13,8), %rax
	movq	%r12, (%rax)
.L1080:
	addq	$1, 24(%rbx)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1097
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1079:
	.cfi_restore_state
	movq	16(%rbx), %rax
	movq	%rax, (%r12)
	movq	%r12, 16(%rbx)
	movq	(%r12), %rax
	testq	%rax, %rax
	je	.L1081
	movq	64(%rax), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	(%rbx), %rax
	movq	%r12, (%rax,%rdx,8)
.L1081:
	movq	(%rbx), %rax
	leaq	16(%rbx), %rdx
	movq	%rdx, (%rax,%rcx)
	jmp	.L1080
.L1097:
	call	__stack_chk_fail@PLT
.L1087:
	endbr64
	movq	%rax, %rdi
.L1082:
	call	__cxa_begin_catch@PLT
	movq	40(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1083
	call	_ZdlPv@PLT
.L1083:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1084
	call	_ZdlPv@PLT
.L1084:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.LEHB169:
	call	__cxa_rethrow@PLT
.LEHE169:
.L1088:
	endbr64
	movq	%rax, %r12
.L1085:
	call	__cxa_end_catch@PLT
	movq	%r12, %rdi
.LEHB170:
	call	_Unwind_Resume@PLT
.LEHE170:
	.cfi_endproc
.LFE9597:
	.section	.gcc_except_table._ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_St6vectorIPN2v88internal6torque10DeclarableESaISD_EEESaISG_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSI_18_Mod_range_hashingENSI_20_Default_ranged_hashENSI_20_Prime_rehash_policyENSI_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSI_10_Hash_nodeISG_Lb1EEEm,"aG",@progbits,_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_St6vectorIPN2v88internal6torque10DeclarableESaISD_EEESaISG_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSI_18_Mod_range_hashingENSI_20_Default_ranged_hashENSI_20_Prime_rehash_policyENSI_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSI_10_Hash_nodeISG_Lb1EEEm,comdat
	.align 4
.LLSDA9597:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATT9597-.LLSDATTD9597
.LLSDATTD9597:
	.byte	0x1
	.uleb128 .LLSDACSE9597-.LLSDACSB9597
.LLSDACSB9597:
	.uleb128 .LEHB167-.LFB9597
	.uleb128 .LEHE167-.LEHB167
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB168-.LFB9597
	.uleb128 .LEHE168-.LEHB168
	.uleb128 .L1087-.LFB9597
	.uleb128 0x1
	.uleb128 .LEHB169-.LFB9597
	.uleb128 .LEHE169-.LEHB169
	.uleb128 .L1088-.LFB9597
	.uleb128 0
	.uleb128 .LEHB170-.LFB9597
	.uleb128 .LEHE170-.LEHB170
	.uleb128 0
	.uleb128 0
.LLSDACSE9597:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATT9597:
	.section	.text._ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_St6vectorIPN2v88internal6torque10DeclarableESaISD_EEESaISG_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSI_18_Mod_range_hashingENSI_20_Default_ranged_hashENSI_20_Prime_rehash_policyENSI_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSI_10_Hash_nodeISG_Lb1EEEm,"axG",@progbits,_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_St6vectorIPN2v88internal6torque10DeclarableESaISD_EEESaISG_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSI_18_Mod_range_hashingENSI_20_Default_ranged_hashENSI_20_Prime_rehash_policyENSI_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSI_10_Hash_nodeISG_Lb1EEEm,comdat
	.size	_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_St6vectorIPN2v88internal6torque10DeclarableESaISD_EEESaISG_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSI_18_Mod_range_hashingENSI_20_Default_ranged_hashENSI_20_Prime_rehash_policyENSI_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSI_10_Hash_nodeISG_Lb1EEEm, .-_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_St6vectorIPN2v88internal6torque10DeclarableESaISD_EEESaISG_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSI_18_Mod_range_hashingENSI_20_Default_ranged_hashENSI_20_Prime_rehash_policyENSI_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSI_10_Hash_nodeISG_Lb1EEEm
	.section	.text._ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_St6vectorIPN2v88internal6torque10DeclarableESaISE_EEESaISH_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS8_,"axG",@progbits,_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_St6vectorIPN2v88internal6torque10DeclarableESaISE_EEESaISH_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS8_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_St6vectorIPN2v88internal6torque10DeclarableESaISE_EEESaISH_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS8_
	.type	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_St6vectorIPN2v88internal6torque10DeclarableESaISE_EEESaISH_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS8_, @function
_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_St6vectorIPN2v88internal6torque10DeclarableESaISE_EEESaISH_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS8_:
.LFB8663:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA8663
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$3339675911, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rsi), %rsi
	movq	(%r15), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZSt11_Hash_bytesPKvmm@PLT
	movq	8(%r14), %rcx
	xorl	%edx, %edx
	movq	%rax, %r12
	divq	%rcx
	movq	(%r14), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %rbx
	testq	%rax, %rax
	je	.L1099
	movq	(%rax), %r13
	movq	64(%r13), %rsi
.L1102:
	cmpq	%rsi, %r12
	je	.L1137
.L1100:
	movq	0(%r13), %r13
	testq	%r13, %r13
	je	.L1099
	movq	64(%r13), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rcx
	cmpq	%rdx, %rbx
	je	.L1102
.L1099:
	movl	$72, %edi
.LEHB171:
	call	_Znwm@PLT
	leaq	24(%rax), %rdi
	movq	$0, (%rax)
	movq	%rax, %r13
	movq	%rdi, 8(%rax)
	movq	(%r15), %rax
	movq	8(%r15), %r15
	movq	%rax, %rcx
	movq	%rax, -72(%rbp)
	addq	%r15, %rcx
	je	.L1103
	testq	%rax, %rax
	je	.L1138
.L1103:
	movq	%r15, -64(%rbp)
	cmpq	$15, %r15
	ja	.L1139
	cmpq	$1, %r15
	jne	.L1106
	movq	-72(%rbp), %rax
	movzbl	(%rax), %eax
	movb	%al, 24(%r13)
.L1107:
	movq	%r15, 16(%r13)
	pxor	%xmm0, %xmm0
	movq	%r13, %rcx
	movq	%r12, %rdx
	movb	$0, (%rdi,%r15)
	movl	$1, %r8d
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movq	$0, 56(%r13)
	movups	%xmm0, 40(%r13)
	call	_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_St6vectorIPN2v88internal6torque10DeclarableESaISD_EEESaISG_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSI_18_Mod_range_hashingENSI_20_Default_ranged_hashENSI_20_Prime_rehash_policyENSI_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSI_10_Hash_nodeISG_Lb1EEEm
.LEHE171:
	addq	$40, %rax
	jmp	.L1098
	.p2align 4,,10
	.p2align 3
.L1137:
	movq	8(%r15), %rdx
	cmpq	16(%r13), %rdx
	jne	.L1100
	movq	%rcx, -72(%rbp)
	testq	%rdx, %rdx
	je	.L1101
	movq	8(%r13), %rsi
	movq	(%r15), %rdi
	call	memcmp@PLT
	movq	-72(%rbp), %rcx
	testl	%eax, %eax
	jne	.L1100
.L1101:
	leaq	40(%r13), %rax
.L1098:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1140
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1106:
	.cfi_restore_state
	testq	%r15, %r15
	je	.L1107
	jmp	.L1105
	.p2align 4,,10
	.p2align 3
.L1139:
	leaq	-64(%rbp), %rsi
	leaq	8(%r13), %rdi
	xorl	%edx, %edx
.LEHB172:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, 8(%r13)
	movq	%rax, %rdi
	movq	-64(%rbp), %rax
	movq	%rax, 24(%r13)
.L1105:
	movq	-72(%rbp), %rsi
	movq	%r15, %rdx
	call	memcpy@PLT
	movq	-64(%rbp), %r15
	movq	8(%r13), %rdi
	jmp	.L1107
.L1140:
	call	__stack_chk_fail@PLT
.L1138:
	leaq	.LC4(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE172:
.L1112:
	endbr64
	movq	%rax, %rdi
.L1109:
	call	__cxa_begin_catch@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.LEHB173:
	call	__cxa_rethrow@PLT
.LEHE173:
.L1113:
	endbr64
	movq	%rax, %r12
.L1110:
	call	__cxa_end_catch@PLT
	movq	%r12, %rdi
.LEHB174:
	call	_Unwind_Resume@PLT
.LEHE174:
	.cfi_endproc
.LFE8663:
	.section	.gcc_except_table._ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_St6vectorIPN2v88internal6torque10DeclarableESaISE_EEESaISH_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS8_,"aG",@progbits,_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_St6vectorIPN2v88internal6torque10DeclarableESaISE_EEESaISH_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS8_,comdat
	.align 4
.LLSDA8663:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATT8663-.LLSDATTD8663
.LLSDATTD8663:
	.byte	0x1
	.uleb128 .LLSDACSE8663-.LLSDACSB8663
.LLSDACSB8663:
	.uleb128 .LEHB171-.LFB8663
	.uleb128 .LEHE171-.LEHB171
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB172-.LFB8663
	.uleb128 .LEHE172-.LEHB172
	.uleb128 .L1112-.LFB8663
	.uleb128 0x1
	.uleb128 .LEHB173-.LFB8663
	.uleb128 .LEHE173-.LEHB173
	.uleb128 .L1113-.LFB8663
	.uleb128 0
	.uleb128 .LEHB174-.LFB8663
	.uleb128 .LEHE174-.LEHB174
	.uleb128 0
	.uleb128 0
.LLSDACSE8663:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATT8663:
	.section	.text._ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_St6vectorIPN2v88internal6torque10DeclarableESaISE_EEESaISH_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS8_,"axG",@progbits,_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_St6vectorIPN2v88internal6torque10DeclarableESaISE_EEESaISH_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS8_,comdat
	.size	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_St6vectorIPN2v88internal6torque10DeclarableESaISE_EEESaISH_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS8_, .-_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_St6vectorIPN2v88internal6torque10DeclarableESaISE_EEESaISH_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS8_
	.section	.rodata._ZN2v88internal6torque5Scope13LookupShallowERKNS1_13QualifiedNameE.str1.1,"aMS",@progbits,1
.LC45:
	.string	"ambiguous reference to scope "
	.section	.rodata._ZN2v88internal6torque5Scope13LookupShallowERKNS1_13QualifiedNameE.str1.8,"aMS",@progbits,1
	.align 8
.LC46:
	.string	"cannot create std::vector larger than max_size()"
	.section	.text._ZN2v88internal6torque5Scope13LookupShallowERKNS1_13QualifiedNameE,"axG",@progbits,_ZN2v88internal6torque5Scope13LookupShallowERKNS1_13QualifiedNameE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque5Scope13LookupShallowERKNS1_13QualifiedNameE
	.type	_ZN2v88internal6torque5Scope13LookupShallowERKNS1_13QualifiedNameE, @function
_ZN2v88internal6torque5Scope13LookupShallowERKNS1_13QualifiedNameE:
.LFB5980:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA5980
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$168, %rsp
	movq	%rdi, -176(%rbp)
	movq	(%rdx), %r8
	leaq	72(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	8(%rdx), %r8
	je	.L1226
	movq	%r8, %rsi
.LEHB175:
	call	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_St6vectorIPN2v88internal6torque10DeclarableESaISE_EEESaISH_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS8_
.LEHE175:
	movq	%rax, %rdx
	movq	(%rax), %rax
	movq	8(%rdx), %rcx
	cmpq	%rax, %rcx
	je	.L1149
	xorl	%r13d, %r13d
	.p2align 4,,10
	.p2align 3
.L1151:
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1150
	cmpl	$6, 8(%rdx)
	ja	.L1150
	testq	%r13, %r13
	jne	.L1227
	movq	%rdx, %r13
.L1150:
	addq	$8, %rax
	cmpq	%rax, %rcx
	jne	.L1151
	testq	%r13, %r13
	je	.L1149
	movq	24(%rbx), %r14
	leaq	-128(%rbp), %rax
	movq	32(%rbx), %r12
	movq	%rax, -184(%rbp)
	movq	%rax, -144(%rbp)
	movq	%r14, %rax
	addq	%r12, %rax
	je	.L1153
	testq	%r14, %r14
	je	.L1228
.L1153:
	movq	%r12, -152(%rbp)
	cmpq	$15, %r12
	ja	.L1229
	cmpq	$1, %r12
	jne	.L1156
	movzbl	(%r14), %eax
	movb	%al, -128(%rbp)
	movq	-184(%rbp), %rax
.L1157:
	movq	%r12, -136(%rbp)
	movb	$0, (%rax,%r12)
	movq	(%rbx), %rax
	leaq	32(%rax), %r14
	movq	8(%rbx), %rax
	movq	%rax, %rbx
	movq	%rax, -168(%rbp)
	subq	%r14, %rbx
	movq	%rbx, %rax
	sarq	$5, %rax
	testq	%rbx, %rbx
	js	.L1230
	testq	%rax, %rax
	je	.L1193
	movq	%rbx, %rdi
.LEHB176:
	call	_Znwm@PLT
.LEHE176:
	movq	%rax, -200(%rbp)
.L1159:
	movq	-200(%rbp), %rax
	addq	%rax, %rbx
	movq	%rbx, -208(%rbp)
	movq	%rax, %rbx
	cmpq	%r14, -168(%rbp)
	je	.L1162
	leaq	-152(%rbp), %rax
	movq	%rax, -192(%rbp)
	jmp	.L1168
	.p2align 4,,10
	.p2align 3
.L1164:
	cmpq	$1, %r12
	jne	.L1166
	movzbl	(%r15), %eax
	movb	%al, 16(%rbx)
.L1167:
	movq	%r12, 8(%rbx)
	addq	$32, %r14
	addq	$32, %rbx
	movb	$0, (%rdi,%r12)
	cmpq	%r14, -168(%rbp)
	je	.L1162
.L1168:
	leaq	16(%rbx), %rdi
	movq	%rdi, (%rbx)
	movq	(%r14), %r15
	movq	8(%r14), %r12
	movq	%r15, %rax
	addq	%r12, %rax
	je	.L1163
	testq	%r15, %r15
	je	.L1231
.L1163:
	movq	%r12, -152(%rbp)
	cmpq	$15, %r12
	jbe	.L1164
	movq	-192(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rbx, %rdi
.LEHB177:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE177:
	movq	%rax, (%rbx)
	movq	%rax, %rdi
	movq	-152(%rbp), %rax
	movq	%rax, 16(%rbx)
.L1165:
	movq	%r12, %rdx
	movq	%r15, %rsi
	call	memcpy@PLT
	movq	-152(%rbp), %r12
	movq	(%rbx), %rdi
	jmp	.L1167
	.p2align 4,,10
	.p2align 3
.L1149:
	movq	-176(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	$0, 16(%rax)
	movups	%xmm0, (%rax)
.L1141:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1232
	movq	-176(%rbp), %rax
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1226:
	.cfi_restore_state
	leaq	24(%rdx), %rsi
.LEHB178:
	call	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_St6vectorIPN2v88internal6torque10DeclarableESaISE_EEESaISH_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS8_
	pxor	%xmm0, %xmm0
	movq	8(%rax), %rbx
	movq	%rax, %r12
	subq	(%rax), %rbx
	movq	-176(%rbp), %rax
	movq	$0, 16(%rax)
	movups	%xmm0, (%rax)
	movq	%rbx, %rax
	sarq	$3, %rax
	je	.L1233
	movabsq	$1152921504606846975, %rdx
	cmpq	%rdx, %rax
	ja	.L1234
	movq	%rbx, %rdi
	call	_Znwm@PLT
.LEHE178:
	movq	%rax, %rcx
.L1144:
	movq	-176(%rbp), %rax
	movq	%rcx, %xmm0
	addq	%rcx, %rbx
	punpcklqdq	%xmm0, %xmm0
	movq	%rbx, 16(%rax)
	movups	%xmm0, (%rax)
	movq	8(%r12), %rax
	movq	(%r12), %rsi
	movq	%rax, %rbx
	subq	%rsi, %rbx
	cmpq	%rsi, %rax
	je	.L1147
	movq	%rcx, %rdi
	movq	%rbx, %rdx
	call	memmove@PLT
	movq	%rax, %rcx
.L1147:
	movq	-176(%rbp), %rax
	addq	%rbx, %rcx
	movq	%rcx, 8(%rax)
	jmp	.L1141
.L1156:
	testq	%r12, %r12
	jne	.L1235
	movq	-184(%rbp), %rax
	jmp	.L1157
	.p2align 4,,10
	.p2align 3
.L1166:
	testq	%r12, %r12
	je	.L1167
	jmp	.L1165
	.p2align 4,,10
	.p2align 3
.L1162:
	movq	-200(%rbp), %xmm0
	movq	-208(%rbp), %rax
	movq	%rbx, %xmm1
	leaq	-72(%rbp), %rbx
	movq	%rbx, -88(%rbp)
	punpcklqdq	%xmm1, %xmm0
	movq	%rax, -96(%rbp)
	movq	-144(%rbp), %rax
	movaps	%xmm0, -112(%rbp)
	cmpq	-184(%rbp), %rax
	je	.L1236
	movq	%rax, -88(%rbp)
	movq	-128(%rbp), %rax
	movq	%rax, -72(%rbp)
.L1179:
	movq	-136(%rbp), %rax
	leaq	-112(%rbp), %r12
	movq	%r13, %rsi
	movb	$0, -128(%rbp)
	movq	-176(%rbp), %rdi
	movq	%r12, %rdx
	movq	$0, -136(%rbp)
	movq	%rax, -80(%rbp)
	movq	-184(%rbp), %rax
	movq	%rax, -144(%rbp)
.LEHB179:
	call	_ZN2v88internal6torque5Scope13LookupShallowERKNS1_13QualifiedNameE
.LEHE179:
	movq	-88(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1180
	call	_ZdlPv@PLT
.L1180:
	movq	-104(%rbp), %rbx
	movq	-112(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L1181
	.p2align 4,,10
	.p2align 3
.L1185:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1182
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%rbx, %r12
	jne	.L1185
.L1183:
	movq	-112(%rbp), %r12
.L1181:
	testq	%r12, %r12
	je	.L1186
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1186:
	movq	-144(%rbp), %rdi
	cmpq	-184(%rbp), %rdi
	je	.L1141
	call	_ZdlPv@PLT
	jmp	.L1141
	.p2align 4,,10
	.p2align 3
.L1182:
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L1185
	jmp	.L1183
.L1229:
	leaq	-144(%rbp), %rdi
	leaq	-152(%rbp), %rsi
	xorl	%edx, %edx
.LEHB180:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -144(%rbp)
	movq	%rax, %rdi
	movq	-152(%rbp), %rax
	movq	%rax, -128(%rbp)
.L1155:
	movq	%r12, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-152(%rbp), %r12
	movq	-144(%rbp), %rax
	jmp	.L1157
.L1236:
	movdqa	-128(%rbp), %xmm2
	movups	%xmm2, -72(%rbp)
	jmp	.L1179
.L1193:
	movq	$0, -200(%rbp)
	jmp	.L1159
.L1233:
	xorl	%ecx, %ecx
	jmp	.L1144
.L1227:
	movq	(%rbx), %rsi
	leaq	.LC45(%rip), %rdi
	call	_ZN2v88internal6torque11ReportErrorIJRA30_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_
.LEHE180:
.L1235:
	movq	-184(%rbp), %rdi
	jmp	.L1155
.L1231:
	leaq	.LC4(%rip), %rdi
.LEHB181:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE181:
.L1230:
	leaq	.LC46(%rip), %rdi
.LEHB182:
	call	_ZSt20__throw_length_errorPKc@PLT
.LEHE182:
.L1228:
	leaq	.LC4(%rip), %rdi
.LEHB183:
	call	_ZSt19__throw_logic_errorPKc@PLT
.L1232:
	call	__stack_chk_fail@PLT
.L1234:
	call	_ZSt17__throw_bad_allocv@PLT
.L1195:
	endbr64
	movq	%rax, %rbx
	jmp	.L1188
.L1197:
	endbr64
	movq	%rax, %rdi
	jmp	.L1171
.L1188:
	movq	%r12, %rdi
	call	_ZN2v88internal6torque13QualifiedNameD1Ev
.L1178:
	movq	-144(%rbp), %rdi
	cmpq	-184(%rbp), %rdi
	je	.L1189
	call	_ZdlPv@PLT
.L1189:
	movq	%rbx, %rdi
	call	_Unwind_Resume@PLT
.LEHE183:
.L1171:
	call	__cxa_begin_catch@PLT
	movq	-200(%rbp), %r12
.L1174:
	cmpq	%rbx, %r12
	jne	.L1237
.LEHB184:
	call	__cxa_rethrow@PLT
.LEHE184:
.L1199:
	endbr64
.L1225:
	movq	%rax, %rbx
	jmp	.L1177
.L1198:
	endbr64
	jmp	.L1225
.L1177:
	jmp	.L1178
.L1237:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1173
	call	_ZdlPv@PLT
.L1173:
	addq	$32, %r12
	jmp	.L1174
.L1196:
	endbr64
	movq	%rax, %rbx
.L1175:
	call	__cxa_end_catch@PLT
	cmpq	$0, -200(%rbp)
	je	.L1178
	movq	-200(%rbp), %rdi
	call	_ZdlPv@PLT
	jmp	.L1178
	.cfi_endproc
.LFE5980:
	.section	.gcc_except_table._ZN2v88internal6torque5Scope13LookupShallowERKNS1_13QualifiedNameE,"aG",@progbits,_ZN2v88internal6torque5Scope13LookupShallowERKNS1_13QualifiedNameE,comdat
	.align 4
.LLSDA5980:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATT5980-.LLSDATTD5980
.LLSDATTD5980:
	.byte	0x1
	.uleb128 .LLSDACSE5980-.LLSDACSB5980
.LLSDACSB5980:
	.uleb128 .LEHB175-.LFB5980
	.uleb128 .LEHE175-.LEHB175
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB176-.LFB5980
	.uleb128 .LEHE176-.LEHB176
	.uleb128 .L1199-.LFB5980
	.uleb128 0
	.uleb128 .LEHB177-.LFB5980
	.uleb128 .LEHE177-.LEHB177
	.uleb128 .L1197-.LFB5980
	.uleb128 0x1
	.uleb128 .LEHB178-.LFB5980
	.uleb128 .LEHE178-.LEHB178
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB179-.LFB5980
	.uleb128 .LEHE179-.LEHB179
	.uleb128 .L1195-.LFB5980
	.uleb128 0
	.uleb128 .LEHB180-.LFB5980
	.uleb128 .LEHE180-.LEHB180
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB181-.LFB5980
	.uleb128 .LEHE181-.LEHB181
	.uleb128 .L1197-.LFB5980
	.uleb128 0x1
	.uleb128 .LEHB182-.LFB5980
	.uleb128 .LEHE182-.LEHB182
	.uleb128 .L1198-.LFB5980
	.uleb128 0
	.uleb128 .LEHB183-.LFB5980
	.uleb128 .LEHE183-.LEHB183
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB184-.LFB5980
	.uleb128 .LEHE184-.LEHB184
	.uleb128 .L1196-.LFB5980
	.uleb128 0
.LLSDACSE5980:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATT5980:
	.section	.text._ZN2v88internal6torque5Scope13LookupShallowERKNS1_13QualifiedNameE,"axG",@progbits,_ZN2v88internal6torque5Scope13LookupShallowERKNS1_13QualifiedNameE,comdat
	.size	_ZN2v88internal6torque5Scope13LookupShallowERKNS1_13QualifiedNameE, .-_ZN2v88internal6torque5Scope13LookupShallowERKNS1_13QualifiedNameE
	.section	.text.unlikely._ZN2v88internal6torque20GetOrCreateNamespaceERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"ax",@progbits
.LCOLDB47:
	.section	.text._ZN2v88internal6torque20GetOrCreateNamespaceERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"ax",@progbits
.LHOTB47:
	.p2align 4
	.globl	_ZN2v88internal6torque20GetOrCreateNamespaceERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN2v88internal6torque20GetOrCreateNamespaceERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN2v88internal6torque20GetOrCreateNamespaceERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB6528:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6528
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-160(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	subq	$232, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %r15
	movq	8(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r13, -176(%rbp)
	movq	%r15, %rax
	addq	%r12, %rax
	je	.L1239
	testq	%r15, %r15
	je	.L1304
.L1239:
	movq	%r12, -208(%rbp)
	cmpq	$15, %r12
	ja	.L1305
	cmpq	$1, %r12
	jne	.L1242
	movzbl	(%r15), %eax
	movb	%al, -160(%rbp)
	movq	%r13, %rax
.L1243:
	movq	%r12, -168(%rbp)
	movb	$0, (%rax,%r12)
	movq	-176(%rbp), %rax
	cmpq	%r13, %rax
	je	.L1306
	leaq	-72(%rbp), %rsi
	movq	-160(%rbp), %rcx
	pxor	%xmm0, %xmm0
	movq	-168(%rbp), %rdx
	movq	%rsi, -264(%rbp)
	movq	%rsi, -88(%rbp)
	leaq	-128(%rbp), %rsi
	movq	%rax, -144(%rbp)
	movq	%rcx, -128(%rbp)
	movq	%rdx, -136(%rbp)
	movq	%r13, -176(%rbp)
	movq	$0, -168(%rbp)
	movb	$0, -160(%rbp)
	movq	$0, -96(%rbp)
	movaps	%xmm0, -112(%rbp)
	cmpq	%rsi, %rax
	je	.L1245
	movq	%rax, -88(%rbp)
	movq	%rcx, -72(%rbp)
.L1247:
	movq	%rdx, -80(%rbp)
	leaq	-112(%rbp), %r15
.LEHB185:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rax
	leaq	-208(%rbp), %rdi
	movq	%r15, %rdx
	movq	(%rax), %rsi
	call	_ZN2v88internal6torque5Scope13LookupShallowERKNS1_13QualifiedNameE
.LEHE185:
	movq	-208(%rbp), %rbx
	pxor	%xmm0, %xmm0
	movq	-200(%rbp), %r12
	movq	$0, -224(%rbp)
	movaps	%xmm0, -240(%rbp)
	cmpq	%r12, %rbx
	je	.L1248
	leaq	-248(%rbp), %rax
	movq	%rax, -272(%rbp)
	jmp	.L1253
	.p2align 4,,10
	.p2align 3
.L1308:
	movq	%rax, (%rsi)
	addq	$8, -232(%rbp)
.L1250:
	addq	$8, %rbx
	cmpq	%rbx, %r12
	je	.L1307
.L1253:
	movq	(%rbx), %rax
	testq	%rax, %rax
	je	.L1250
	movl	8(%rax), %edx
	testl	%edx, %edx
	jne	.L1250
	movq	%rax, -248(%rbp)
	movq	-232(%rbp), %rsi
	cmpq	-224(%rbp), %rsi
	jne	.L1308
	movq	-272(%rbp), %rdx
	leaq	-240(%rbp), %rdi
.LEHB186:
	call	_ZNSt6vectorIPN2v88internal6torque9NamespaceESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
.LEHE186:
	addq	$8, %rbx
	cmpq	%rbx, %r12
	jne	.L1253
	.p2align 4,,10
	.p2align 3
.L1307:
	movq	-208(%rbp), %r12
.L1248:
	testq	%r12, %r12
	je	.L1258
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1258:
	movq	-88(%rbp), %rdi
	cmpq	-264(%rbp), %rdi
	je	.L1259
	call	_ZdlPv@PLT
.L1259:
	movq	-104(%rbp), %rbx
	movq	-112(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L1260
	.p2align 4,,10
	.p2align 3
.L1264:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1261
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L1264
.L1262:
	movq	-112(%rbp), %r12
.L1260:
	testq	%r12, %r12
	je	.L1265
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1265:
	movq	-176(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L1266
	call	_ZdlPv@PLT
.L1266:
	movq	-240(%rbp), %rdi
	cmpq	%rdi, -232(%rbp)
	je	.L1309
	movq	(%rdi), %r12
.L1269:
	call	_ZdlPv@PLT
.L1238:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1310
	addq	$232, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1242:
	.cfi_restore_state
	testq	%r12, %r12
	jne	.L1311
	movq	%r13, %rax
	jmp	.L1243
	.p2align 4,,10
	.p2align 3
.L1261:
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L1264
	jmp	.L1262
	.p2align 4,,10
	.p2align 3
.L1305:
	leaq	-176(%rbp), %rdi
	leaq	-208(%rbp), %rsi
	xorl	%edx, %edx
.LEHB187:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE187:
	movq	%rax, -176(%rbp)
	movq	%rax, %rdi
	movq	-208(%rbp), %rax
	movq	%rax, -160(%rbp)
.L1241:
	movq	%r12, %rdx
	movq	%r15, %rsi
	call	memcpy@PLT
	movq	-208(%rbp), %r12
	movq	-176(%rbp), %rax
	jmp	.L1243
	.p2align 4,,10
	.p2align 3
.L1306:
	leaq	-72(%rbp), %rax
	movdqa	-160(%rbp), %xmm1
	pxor	%xmm0, %xmm0
	movq	-168(%rbp), %rdx
	movb	$0, -160(%rbp)
	movq	$0, -168(%rbp)
	movq	$0, -96(%rbp)
	movq	%rax, -264(%rbp)
	movq	%rax, -88(%rbp)
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
.L1245:
	movdqa	-128(%rbp), %xmm2
	movups	%xmm2, -72(%rbp)
	jmp	.L1247
	.p2align 4,,10
	.p2align 3
.L1309:
	movq	%r14, %rdi
.LEHB188:
	call	_ZN2v88internal6torque12Declarations16DeclareNamespaceERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
.LEHE188:
	movq	-240(%rbp), %rdi
	movq	%rax, %r12
	testq	%rdi, %rdi
	je	.L1238
	jmp	.L1269
.L1304:
	leaq	.LC4(%rip), %rdi
.LEHB189:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE189:
.L1310:
	call	__stack_chk_fail@PLT
.L1311:
	movq	%r13, %rdi
	jmp	.L1241
.L1279:
	endbr64
	movq	%rax, %r12
	jmp	.L1254
.L1277:
	endbr64
	movq	%rax, %r12
	jmp	.L1271
.L1278:
	endbr64
	movq	%rax, %r12
	jmp	.L1273
	.section	.gcc_except_table._ZN2v88internal6torque20GetOrCreateNamespaceERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"a",@progbits
.LLSDA6528:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6528-.LLSDACSB6528
.LLSDACSB6528:
	.uleb128 .LEHB185-.LFB6528
	.uleb128 .LEHE185-.LEHB185
	.uleb128 .L1277-.LFB6528
	.uleb128 0
	.uleb128 .LEHB186-.LFB6528
	.uleb128 .LEHE186-.LEHB186
	.uleb128 .L1279-.LFB6528
	.uleb128 0
	.uleb128 .LEHB187-.LFB6528
	.uleb128 .LEHE187-.LEHB187
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB188-.LFB6528
	.uleb128 .LEHE188-.LEHB188
	.uleb128 .L1278-.LFB6528
	.uleb128 0
	.uleb128 .LEHB189-.LFB6528
	.uleb128 .LEHE189-.LEHB189
	.uleb128 0
	.uleb128 0
.LLSDACSE6528:
	.section	.text._ZN2v88internal6torque20GetOrCreateNamespaceERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque20GetOrCreateNamespaceERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6528
	.type	_ZN2v88internal6torque20GetOrCreateNamespaceERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.cold, @function
_ZN2v88internal6torque20GetOrCreateNamespaceERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.cold:
.LFSB6528:
.L1254:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1255
	call	_ZdlPv@PLT
.L1255:
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1271
	call	_ZdlPv@PLT
.L1271:
	movq	%r15, %rdi
	call	_ZN2v88internal6torque13QualifiedNameD1Ev
	movq	-176(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L1274
.L1303:
	call	_ZdlPv@PLT
.L1274:
	movq	%r12, %rdi
.LEHB190:
	call	_Unwind_Resume@PLT
.LEHE190:
.L1273:
	movq	-240(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1303
	jmp	.L1274
	.cfi_endproc
.LFE6528:
	.section	.gcc_except_table._ZN2v88internal6torque20GetOrCreateNamespaceERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
.LLSDAC6528:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6528-.LLSDACSBC6528
.LLSDACSBC6528:
	.uleb128 .LEHB190-.LCOLDB47
	.uleb128 .LEHE190-.LEHB190
	.uleb128 0
	.uleb128 0
.LLSDACSEC6528:
	.section	.text.unlikely._ZN2v88internal6torque20GetOrCreateNamespaceERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.text._ZN2v88internal6torque20GetOrCreateNamespaceERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.size	_ZN2v88internal6torque20GetOrCreateNamespaceERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN2v88internal6torque20GetOrCreateNamespaceERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.text.unlikely._ZN2v88internal6torque20GetOrCreateNamespaceERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.size	_ZN2v88internal6torque20GetOrCreateNamespaceERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.cold, .-_ZN2v88internal6torque20GetOrCreateNamespaceERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.cold
.LCOLDE47:
	.section	.text._ZN2v88internal6torque20GetOrCreateNamespaceERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
.LHOTE47:
	.section	.text.unlikely._ZN2v88internal6torque21PredeclarationVisitor10PredeclareEPNS1_11DeclarationE,"ax",@progbits
	.align 2
.LCOLDB48:
	.section	.text._ZN2v88internal6torque21PredeclarationVisitor10PredeclareEPNS1_11DeclarationE,"ax",@progbits
.LHOTB48:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque21PredeclarationVisitor10PredeclareEPNS1_11DeclarationE
	.type	_ZN2v88internal6torque21PredeclarationVisitor10PredeclareEPNS1_11DeclarationE, @function
_ZN2v88internal6torque21PredeclarationVisitor10PredeclareEPNS1_11DeclarationE:
.LFB6538:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6538
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	subq	$64, %rsp
	.cfi_offset 3, -32
	movdqu	12(%rdi), %xmm0
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	28(%rdi), %eax
	movaps	%xmm0, -64(%rbp)
	movl	%eax, -48(%rbp)
.LEHB191:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rax
	movq	%rax, -40(%rbp)
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
.LEHE191:
	leaq	-64(%rbp), %rdx
	movq	%rdx, (%rax)
	movl	8(%r12), %eax
	subl	$36, %eax
	cmpl	$7, %eax
	ja	.L1313
	leaq	.L1315(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal6torque21PredeclarationVisitor10PredeclareEPNS1_11DeclarationE,"a",@progbits
	.align 4
	.align 4
.L1315:
	.long	.L1318-.L1315
	.long	.L1318-.L1315
	.long	.L1318-.L1315
	.long	.L1317-.L1315
	.long	.L1316-.L1315
	.long	.L1313-.L1315
	.long	.L1313-.L1315
	.long	.L1314-.L1315
	.section	.text._ZN2v88internal6torque21PredeclarationVisitor10PredeclareEPNS1_11DeclarationE
	.p2align 4,,10
	.p2align 3
.L1318:
	movq	32(%r12), %rdi
	xorl	%edx, %edx
	movq	%r12, %rsi
.LEHB192:
	call	_ZN2v88internal6torque12Declarations19PredeclareTypeAliasEPKNS1_10IdentifierEPNS1_15TypeDeclarationEb@PLT
.LEHE192:
.L1324:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
	movq	-40(%rbp), %rdx
	movq	%rdx, (%rax)
.L1312:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1338
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1316:
	.cfi_restore_state
	movq	56(%r12), %rax
	movq	%r12, %rsi
	movq	40(%rax), %rdi
	addq	$32, %rdi
.LEHB193:
	call	_ZN2v88internal6torque12Declarations14DeclareGenericERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPNS1_18GenericDeclarationE@PLT
	jmp	.L1324
	.p2align 4,,10
	.p2align 3
.L1317:
	movq	32(%r12), %rdi
	movq	88(%r12), %rax
	cmpq	%rax, 96(%r12)
	je	.L1322
	addq	$32, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal6torque12Declarations24DeclareGenericStructTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPNS1_17StructDeclarationE@PLT
.LEHE193:
	jmp	.L1324
	.p2align 4,,10
	.p2align 3
.L1313:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
	movq	-40(%rbp), %rdx
	movq	%rdx, (%rax)
	jmp	.L1312
	.p2align 4,,10
	.p2align 3
.L1314:
	leaq	56(%r12), %rdi
.LEHB194:
	call	_ZN2v88internal6torque20GetOrCreateNamespaceERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rax
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
.LEHE194:
	leaq	-80(%rbp), %rdx
	movq	%rdx, (%rax)
	movq	40(%r12), %rbx
	movq	32(%r12), %r12
	cmpq	%rbx, %r12
	je	.L1326
	.p2align 4,,10
	.p2align 3
.L1325:
	movq	(%r12), %rdi
.LEHB195:
	call	_ZN2v88internal6torque21PredeclarationVisitor10PredeclareEPNS1_11DeclarationE
.LEHE195:
	addq	$8, %r12
	cmpq	%r12, %rbx
	jne	.L1325
.L1326:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
	movq	-72(%rbp), %rdx
	movq	%rdx, (%rax)
	jmp	.L1324
	.p2align 4,,10
	.p2align 3
.L1322:
	xorl	%edx, %edx
	movq	%r12, %rsi
.LEHB196:
	call	_ZN2v88internal6torque12Declarations19PredeclareTypeAliasEPKNS1_10IdentifierEPNS1_15TypeDeclarationEb@PLT
.LEHE196:
	jmp	.L1324
.L1338:
	call	__stack_chk_fail@PLT
.L1331:
	endbr64
	movq	%rax, %r12
	jmp	.L1328
.L1332:
	endbr64
	movq	%rax, %r12
	jmp	.L1327
	.section	.gcc_except_table._ZN2v88internal6torque21PredeclarationVisitor10PredeclareEPNS1_11DeclarationE,"a",@progbits
.LLSDA6538:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6538-.LLSDACSB6538
.LLSDACSB6538:
	.uleb128 .LEHB191-.LFB6538
	.uleb128 .LEHE191-.LEHB191
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB192-.LFB6538
	.uleb128 .LEHE192-.LEHB192
	.uleb128 .L1331-.LFB6538
	.uleb128 0
	.uleb128 .LEHB193-.LFB6538
	.uleb128 .LEHE193-.LEHB193
	.uleb128 .L1331-.LFB6538
	.uleb128 0
	.uleb128 .LEHB194-.LFB6538
	.uleb128 .LEHE194-.LEHB194
	.uleb128 .L1331-.LFB6538
	.uleb128 0
	.uleb128 .LEHB195-.LFB6538
	.uleb128 .LEHE195-.LEHB195
	.uleb128 .L1332-.LFB6538
	.uleb128 0
	.uleb128 .LEHB196-.LFB6538
	.uleb128 .LEHE196-.LEHB196
	.uleb128 .L1331-.LFB6538
	.uleb128 0
.LLSDACSE6538:
	.section	.text._ZN2v88internal6torque21PredeclarationVisitor10PredeclareEPNS1_11DeclarationE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque21PredeclarationVisitor10PredeclareEPNS1_11DeclarationE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6538
	.type	_ZN2v88internal6torque21PredeclarationVisitor10PredeclareEPNS1_11DeclarationE.cold, @function
_ZN2v88internal6torque21PredeclarationVisitor10PredeclareEPNS1_11DeclarationE.cold:
.LFSB6538:
.L1327:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
	movq	-72(%rbp), %rdx
	movq	%rdx, (%rax)
.L1328:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
	movq	-40(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rdx, (%rax)
.LEHB197:
	call	_Unwind_Resume@PLT
.LEHE197:
	.cfi_endproc
.LFE6538:
	.section	.gcc_except_table._ZN2v88internal6torque21PredeclarationVisitor10PredeclareEPNS1_11DeclarationE
.LLSDAC6538:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6538-.LLSDACSBC6538
.LLSDACSBC6538:
	.uleb128 .LEHB197-.LCOLDB48
	.uleb128 .LEHE197-.LEHB197
	.uleb128 0
	.uleb128 0
.LLSDACSEC6538:
	.section	.text.unlikely._ZN2v88internal6torque21PredeclarationVisitor10PredeclareEPNS1_11DeclarationE
	.section	.text._ZN2v88internal6torque21PredeclarationVisitor10PredeclareEPNS1_11DeclarationE
	.size	_ZN2v88internal6torque21PredeclarationVisitor10PredeclareEPNS1_11DeclarationE, .-_ZN2v88internal6torque21PredeclarationVisitor10PredeclareEPNS1_11DeclarationE
	.section	.text.unlikely._ZN2v88internal6torque21PredeclarationVisitor10PredeclareEPNS1_11DeclarationE
	.size	_ZN2v88internal6torque21PredeclarationVisitor10PredeclareEPNS1_11DeclarationE.cold, .-_ZN2v88internal6torque21PredeclarationVisitor10PredeclareEPNS1_11DeclarationE.cold
.LCOLDE48:
	.section	.text._ZN2v88internal6torque21PredeclarationVisitor10PredeclareEPNS1_11DeclarationE
.LHOTE48:
	.section	.text._ZNSt6vectorIPN2v88internal6torque10DeclarableESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_,"axG",@progbits,_ZNSt6vectorIPN2v88internal6torque10DeclarableESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPN2v88internal6torque10DeclarableESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	.type	_ZNSt6vectorIPN2v88internal6torque10DeclarableESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_, @function
_ZNSt6vectorIPN2v88internal6torque10DeclarableESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_:
.LFB11175:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r9
	movq	%r13, %rax
	subq	%r9, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L1353
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L1349
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L1354
.L1341:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L1348:
	movq	(%r15), %rax
	subq	%r8, %r13
	leaq	8(%rbx,%rdx), %r15
	movq	%rax, (%rbx,%rdx)
	leaq	(%r15,%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L1355
	testq	%r13, %r13
	jg	.L1344
	testq	%r9, %r9
	jne	.L1347
.L1345:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1355:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jg	.L1344
.L1347:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L1345
	.p2align 4,,10
	.p2align 3
.L1354:
	testq	%rsi, %rsi
	jne	.L1342
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L1348
	.p2align 4,,10
	.p2align 3
.L1344:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L1345
	jmp	.L1347
	.p2align 4,,10
	.p2align 3
.L1349:
	movl	$8, %r14d
	jmp	.L1341
.L1353:
	leaq	.LC43(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1342:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$3, %r14
	jmp	.L1341
	.cfi_endproc
.LFE11175:
	.size	_ZNSt6vectorIPN2v88internal6torque10DeclarableESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_, .-_ZNSt6vectorIPN2v88internal6torque10DeclarableESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	.section	.text.unlikely._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_26ExternalBuiltinDeclarationE,"ax",@progbits
	.align 2
.LCOLDB49:
	.section	.text._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_26ExternalBuiltinDeclarationE,"ax",@progbits
.LHOTB49:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_26ExternalBuiltinDeclarationE
	.type	_ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_26ExternalBuiltinDeclarationE, @function
_ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_26ExternalBuiltinDeclarationE:
.LFB6541:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6541
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-208(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-224(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r15, %rdi
	subq	$264, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
.LEHB198:
	call	_ZN2v88internal6torque11TypeVisitor13MakeSignatureEPKNS1_19CallableDeclarationE@PLT
.LEHE198:
	movq	40(%rbx), %rax
	movq	%r13, -240(%rbp)
	movq	32(%rax), %r14
	movq	40(%rax), %r12
	movq	%r14, %rax
	addq	%r12, %rax
	je	.L1357
	testq	%r14, %r14
	je	.L1414
.L1357:
	movq	%r12, -280(%rbp)
	cmpq	$15, %r12
	ja	.L1415
	cmpq	$1, %r12
	jne	.L1360
	movzbl	(%r14), %eax
	movb	%al, -224(%rbp)
	movq	%r13, %rax
.L1361:
	movq	%r12, -232(%rbp)
	leaq	-256(%rbp), %rcx
	movb	$0, (%rax,%r12)
	movq	40(%rbx), %rax
	movq	%rcx, -296(%rbp)
	movq	32(%rax), %r14
	movq	40(%rax), %r12
	movq	%rcx, -272(%rbp)
	movq	%r14, %rax
	addq	%r12, %rax
	je	.L1362
	testq	%r14, %r14
	je	.L1416
.L1362:
	movq	%r12, -280(%rbp)
	cmpq	$15, %r12
	ja	.L1417
	cmpq	$1, %r12
	jne	.L1365
	movzbl	(%r14), %eax
	leaq	-272(%rbp), %rdx
	movq	%rdx, -304(%rbp)
	movb	%al, -256(%rbp)
	movq	-296(%rbp), %rax
.L1366:
	movq	%r12, -264(%rbp)
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	%r15, %rcx
	movb	$0, (%rax,%r12)
	movq	-304(%rbp), %rsi
	leaq	-240(%rbp), %rdx
	movq	%rbx, %rdi
.LEHB199:
	call	_ZN2v88internal6torque18DeclarationVisitor13CreateBuiltinEPNS1_18BuiltinDeclarationENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_NS1_9SignatureENS_4base8OptionalIPNS1_9StatementEEE
	movq	40(%rbx), %r14
	movq	%rax, %r12
	addq	$32, %r14
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rax
	movq	%r14, %rsi
	movq	(%rax), %rdi
	addq	$72, %rdi
	call	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_St6vectorIPN2v88internal6torque10DeclarableESaISE_EEESaISH_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS8_
.LEHE199:
	movq	%r12, -280(%rbp)
	movq	8(%rax), %rsi
	cmpq	16(%rax), %rsi
	je	.L1367
	movq	%r12, (%rsi)
	addq	$8, 8(%rax)
.L1368:
	movq	-272(%rbp), %rdi
	cmpq	-296(%rbp), %rdi
	je	.L1369
	call	_ZdlPv@PLT
.L1369:
	movq	-240(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L1370
	call	_ZdlPv@PLT
.L1370:
	movq	-88(%rbp), %rbx
	movq	-96(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L1371
	.p2align 4,,10
	.p2align 3
.L1375:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1372
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L1375
.L1373:
	movq	-96(%rbp), %r12
.L1371:
	testq	%r12, %r12
	je	.L1376
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1376:
	movq	-144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1377
	call	_ZdlPv@PLT
.L1377:
	cmpb	$0, -184(%rbp)
	je	.L1378
	movq	-176(%rbp), %rdi
	leaq	-160(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1378
	call	_ZdlPv@PLT
.L1378:
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1379
	call	_ZdlPv@PLT
.L1379:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1418
	addq	$264, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1360:
	.cfi_restore_state
	testq	%r12, %r12
	jne	.L1419
	movq	%r13, %rax
	jmp	.L1361
	.p2align 4,,10
	.p2align 3
.L1372:
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L1375
	jmp	.L1373
	.p2align 4,,10
	.p2align 3
.L1365:
	testq	%r12, %r12
	jne	.L1420
	leaq	-272(%rbp), %rdx
	movq	-296(%rbp), %rax
	movq	%rdx, -304(%rbp)
	jmp	.L1366
	.p2align 4,,10
	.p2align 3
.L1415:
	leaq	-280(%rbp), %rsi
	leaq	-240(%rbp), %rdi
	xorl	%edx, %edx
.LEHB200:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE200:
	movq	%rax, -240(%rbp)
	movq	%rax, %rdi
	movq	-280(%rbp), %rax
	movq	%rax, -224(%rbp)
.L1359:
	movq	%r12, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-280(%rbp), %r12
	movq	-240(%rbp), %rax
	jmp	.L1361
	.p2align 4,,10
	.p2align 3
.L1417:
	leaq	-272(%rbp), %rax
	leaq	-280(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rax, %rdi
	movq	%rax, -304(%rbp)
.LEHB201:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE201:
	movq	%rax, -272(%rbp)
	movq	%rax, %rdi
	movq	-280(%rbp), %rax
	movq	%rax, -256(%rbp)
.L1364:
	movq	%r12, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-280(%rbp), %r12
	movq	-272(%rbp), %rax
	jmp	.L1366
	.p2align 4,,10
	.p2align 3
.L1367:
	leaq	-280(%rbp), %rdx
	movq	%rax, %rdi
.LEHB202:
	call	_ZNSt6vectorIPN2v88internal6torque10DeclarableESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
.LEHE202:
	jmp	.L1368
.L1414:
	leaq	.LC4(%rip), %rdi
.LEHB203:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE203:
.L1418:
	call	__stack_chk_fail@PLT
.L1416:
	leaq	.LC4(%rip), %rdi
.LEHB204:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE204:
.L1420:
	leaq	-272(%rbp), %rax
	movq	-296(%rbp), %rdi
	movq	%rax, -304(%rbp)
	jmp	.L1364
.L1419:
	movq	%r13, %rdi
	jmp	.L1359
.L1390:
	endbr64
	movq	%rax, %rbx
	jmp	.L1380
.L1389:
	endbr64
	movq	%rax, %rbx
	jmp	.L1382
.L1388:
	endbr64
	movq	%rax, %rbx
	jmp	.L1384
	.section	.gcc_except_table._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_26ExternalBuiltinDeclarationE,"a",@progbits
.LLSDA6541:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6541-.LLSDACSB6541
.LLSDACSB6541:
	.uleb128 .LEHB198-.LFB6541
	.uleb128 .LEHE198-.LEHB198
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB199-.LFB6541
	.uleb128 .LEHE199-.LEHB199
	.uleb128 .L1390-.LFB6541
	.uleb128 0
	.uleb128 .LEHB200-.LFB6541
	.uleb128 .LEHE200-.LEHB200
	.uleb128 .L1388-.LFB6541
	.uleb128 0
	.uleb128 .LEHB201-.LFB6541
	.uleb128 .LEHE201-.LEHB201
	.uleb128 .L1389-.LFB6541
	.uleb128 0
	.uleb128 .LEHB202-.LFB6541
	.uleb128 .LEHE202-.LEHB202
	.uleb128 .L1390-.LFB6541
	.uleb128 0
	.uleb128 .LEHB203-.LFB6541
	.uleb128 .LEHE203-.LEHB203
	.uleb128 .L1388-.LFB6541
	.uleb128 0
	.uleb128 .LEHB204-.LFB6541
	.uleb128 .LEHE204-.LEHB204
	.uleb128 .L1389-.LFB6541
	.uleb128 0
.LLSDACSE6541:
	.section	.text._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_26ExternalBuiltinDeclarationE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_26ExternalBuiltinDeclarationE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6541
	.type	_ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_26ExternalBuiltinDeclarationE.cold, @function
_ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_26ExternalBuiltinDeclarationE.cold:
.LFSB6541:
.L1380:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-272(%rbp), %rdi
	cmpq	-296(%rbp), %rdi
	je	.L1382
	call	_ZdlPv@PLT
.L1382:
	movq	-240(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L1384
	call	_ZdlPv@PLT
.L1384:
	movq	%r15, %rdi
	call	_ZN2v88internal6torque9SignatureD1Ev
	movq	%rbx, %rdi
.LEHB205:
	call	_Unwind_Resume@PLT
.LEHE205:
	.cfi_endproc
.LFE6541:
	.section	.gcc_except_table._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_26ExternalBuiltinDeclarationE
.LLSDAC6541:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6541-.LLSDACSBC6541
.LLSDACSBC6541:
	.uleb128 .LEHB205-.LCOLDB49
	.uleb128 .LEHE205-.LEHB205
	.uleb128 0
	.uleb128 0
.LLSDACSEC6541:
	.section	.text.unlikely._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_26ExternalBuiltinDeclarationE
	.section	.text._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_26ExternalBuiltinDeclarationE
	.size	_ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_26ExternalBuiltinDeclarationE, .-_ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_26ExternalBuiltinDeclarationE
	.section	.text.unlikely._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_26ExternalBuiltinDeclarationE
	.size	_ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_26ExternalBuiltinDeclarationE.cold, .-_ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_26ExternalBuiltinDeclarationE.cold
.LCOLDE49:
	.section	.text._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_26ExternalBuiltinDeclarationE
.LHOTE49:
	.section	.text.unlikely._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_24TorqueBuiltinDeclarationE,"ax",@progbits
	.align 2
.LCOLDB50:
	.section	.text._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_24TorqueBuiltinDeclarationE,"ax",@progbits
.LHOTB50:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_24TorqueBuiltinDeclarationE
	.type	_ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_24TorqueBuiltinDeclarationE, @function
_ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_24TorqueBuiltinDeclarationE:
.LFB6544:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6544
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-208(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-224(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r15, %rdi
	subq	$264, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
.LEHB206:
	call	_ZN2v88internal6torque11TypeVisitor13MakeSignatureEPKNS1_19CallableDeclarationE@PLT
.LEHE206:
	movq	40(%rbx), %rax
	movq	%r13, -240(%rbp)
	movq	32(%rax), %r14
	movq	40(%rax), %r12
	movq	%r14, %rax
	addq	%r12, %rax
	je	.L1422
	testq	%r14, %r14
	je	.L1478
.L1422:
	movq	%r12, -280(%rbp)
	cmpq	$15, %r12
	ja	.L1479
	cmpq	$1, %r12
	jne	.L1425
	movzbl	(%r14), %eax
	movb	%al, -224(%rbp)
	movq	%r13, %rax
.L1426:
	movq	%r12, -232(%rbp)
	leaq	-256(%rbp), %rcx
	movb	$0, (%rax,%r12)
	movq	40(%rbx), %rax
	movq	%rcx, -296(%rbp)
	movq	32(%rax), %r14
	movq	40(%rax), %r12
	movq	%rcx, -272(%rbp)
	movq	%r14, %rax
	addq	%r12, %rax
	je	.L1427
	testq	%r14, %r14
	je	.L1480
.L1427:
	movq	%r12, -280(%rbp)
	cmpq	$15, %r12
	ja	.L1481
	cmpq	$1, %r12
	jne	.L1430
	movzbl	(%r14), %eax
	leaq	-272(%rbp), %rdx
	movq	%rdx, -304(%rbp)
	movb	%al, -256(%rbp)
	movq	-296(%rbp), %rax
.L1431:
	movq	%r12, -264(%rbp)
	movq	-304(%rbp), %rsi
	movq	%r15, %rcx
	movq	%rbx, %rdi
	movb	$0, (%rax,%r12)
	movl	208(%rbx), %r8d
	leaq	-240(%rbp), %rdx
	movq	216(%rbx), %r9
.LEHB207:
	call	_ZN2v88internal6torque18DeclarationVisitor13CreateBuiltinEPNS1_18BuiltinDeclarationENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_NS1_9SignatureENS_4base8OptionalIPNS1_9StatementEEE
	movq	40(%rbx), %r14
	movq	%rax, %r12
	addq	$32, %r14
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rax
	movq	%r14, %rsi
	movq	(%rax), %rdi
	addq	$72, %rdi
	call	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_St6vectorIPN2v88internal6torque10DeclarableESaISE_EEESaISH_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS8_
.LEHE207:
	movq	%r12, -280(%rbp)
	movq	8(%rax), %rsi
	cmpq	16(%rax), %rsi
	je	.L1432
	movq	%r12, (%rsi)
	addq	$8, 8(%rax)
.L1433:
	movq	-272(%rbp), %rdi
	cmpq	-296(%rbp), %rdi
	je	.L1434
	call	_ZdlPv@PLT
.L1434:
	movq	-240(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L1435
	call	_ZdlPv@PLT
.L1435:
	movq	-88(%rbp), %rbx
	movq	-96(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L1436
	.p2align 4,,10
	.p2align 3
.L1440:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1437
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L1440
.L1438:
	movq	-96(%rbp), %r12
.L1436:
	testq	%r12, %r12
	je	.L1441
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1441:
	movq	-144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1442
	call	_ZdlPv@PLT
.L1442:
	cmpb	$0, -184(%rbp)
	je	.L1443
	movq	-176(%rbp), %rdi
	leaq	-160(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1443
	call	_ZdlPv@PLT
.L1443:
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1421
	call	_ZdlPv@PLT
.L1421:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1482
	addq	$264, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1425:
	.cfi_restore_state
	testq	%r12, %r12
	jne	.L1483
	movq	%r13, %rax
	jmp	.L1426
	.p2align 4,,10
	.p2align 3
.L1437:
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L1440
	jmp	.L1438
	.p2align 4,,10
	.p2align 3
.L1430:
	testq	%r12, %r12
	jne	.L1484
	leaq	-272(%rbp), %rdx
	movq	-296(%rbp), %rax
	movq	%rdx, -304(%rbp)
	jmp	.L1431
	.p2align 4,,10
	.p2align 3
.L1479:
	leaq	-280(%rbp), %rsi
	leaq	-240(%rbp), %rdi
	xorl	%edx, %edx
.LEHB208:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE208:
	movq	%rax, -240(%rbp)
	movq	%rax, %rdi
	movq	-280(%rbp), %rax
	movq	%rax, -224(%rbp)
.L1424:
	movq	%r12, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-280(%rbp), %r12
	movq	-240(%rbp), %rax
	jmp	.L1426
	.p2align 4,,10
	.p2align 3
.L1481:
	leaq	-272(%rbp), %rax
	leaq	-280(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rax, %rdi
	movq	%rax, -304(%rbp)
.LEHB209:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE209:
	movq	%rax, -272(%rbp)
	movq	%rax, %rdi
	movq	-280(%rbp), %rax
	movq	%rax, -256(%rbp)
.L1429:
	movq	%r12, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-280(%rbp), %r12
	movq	-272(%rbp), %rax
	jmp	.L1431
	.p2align 4,,10
	.p2align 3
.L1432:
	leaq	-280(%rbp), %rdx
	movq	%rax, %rdi
.LEHB210:
	call	_ZNSt6vectorIPN2v88internal6torque10DeclarableESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
.LEHE210:
	jmp	.L1433
.L1478:
	leaq	.LC4(%rip), %rdi
.LEHB211:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE211:
.L1482:
	call	__stack_chk_fail@PLT
.L1480:
	leaq	.LC4(%rip), %rdi
.LEHB212:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE212:
.L1484:
	leaq	-272(%rbp), %rax
	movq	-296(%rbp), %rdi
	movq	%rax, -304(%rbp)
	jmp	.L1429
.L1483:
	movq	%r13, %rdi
	jmp	.L1424
.L1455:
	endbr64
	movq	%rax, %rbx
	jmp	.L1445
.L1454:
	endbr64
	movq	%rax, %rbx
	jmp	.L1447
.L1453:
	endbr64
	movq	%rax, %rbx
	jmp	.L1449
	.section	.gcc_except_table._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_24TorqueBuiltinDeclarationE,"a",@progbits
.LLSDA6544:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6544-.LLSDACSB6544
.LLSDACSB6544:
	.uleb128 .LEHB206-.LFB6544
	.uleb128 .LEHE206-.LEHB206
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB207-.LFB6544
	.uleb128 .LEHE207-.LEHB207
	.uleb128 .L1455-.LFB6544
	.uleb128 0
	.uleb128 .LEHB208-.LFB6544
	.uleb128 .LEHE208-.LEHB208
	.uleb128 .L1453-.LFB6544
	.uleb128 0
	.uleb128 .LEHB209-.LFB6544
	.uleb128 .LEHE209-.LEHB209
	.uleb128 .L1454-.LFB6544
	.uleb128 0
	.uleb128 .LEHB210-.LFB6544
	.uleb128 .LEHE210-.LEHB210
	.uleb128 .L1455-.LFB6544
	.uleb128 0
	.uleb128 .LEHB211-.LFB6544
	.uleb128 .LEHE211-.LEHB211
	.uleb128 .L1453-.LFB6544
	.uleb128 0
	.uleb128 .LEHB212-.LFB6544
	.uleb128 .LEHE212-.LEHB212
	.uleb128 .L1454-.LFB6544
	.uleb128 0
.LLSDACSE6544:
	.section	.text._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_24TorqueBuiltinDeclarationE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_24TorqueBuiltinDeclarationE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6544
	.type	_ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_24TorqueBuiltinDeclarationE.cold, @function
_ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_24TorqueBuiltinDeclarationE.cold:
.LFSB6544:
.L1445:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-272(%rbp), %rdi
	cmpq	-296(%rbp), %rdi
	je	.L1447
	call	_ZdlPv@PLT
.L1447:
	movq	-240(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L1449
	call	_ZdlPv@PLT
.L1449:
	movq	%r15, %rdi
	call	_ZN2v88internal6torque9SignatureD1Ev
	movq	%rbx, %rdi
.LEHB213:
	call	_Unwind_Resume@PLT
.LEHE213:
	.cfi_endproc
.LFE6544:
	.section	.gcc_except_table._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_24TorqueBuiltinDeclarationE
.LLSDAC6544:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6544-.LLSDACSBC6544
.LLSDACSBC6544:
	.uleb128 .LEHB213-.LCOLDB50
	.uleb128 .LEHE213-.LEHB213
	.uleb128 0
	.uleb128 0
.LLSDACSEC6544:
	.section	.text.unlikely._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_24TorqueBuiltinDeclarationE
	.section	.text._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_24TorqueBuiltinDeclarationE
	.size	_ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_24TorqueBuiltinDeclarationE, .-_ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_24TorqueBuiltinDeclarationE
	.section	.text.unlikely._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_24TorqueBuiltinDeclarationE
	.size	_ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_24TorqueBuiltinDeclarationE.cold, .-_ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_24TorqueBuiltinDeclarationE.cold
.LCOLDE50:
	.section	.text._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_24TorqueBuiltinDeclarationE
.LHOTE50:
	.section	.text._ZNSt10_HashtableISt6vectorIPKN2v88internal6torque4TypeESaIS6_EESt4pairIKS8_PNS3_8CallableEESaISD_ENSt8__detail10_Select1stESt8equal_toIS8_ENS1_4base4hashIS8_EENSF_18_Mod_range_hashingENSF_20_Default_ranged_hashENSF_20_Prime_rehash_policyENSF_17_Hashtable_traitsILb1ELb0ELb1EEEE9_M_rehashEmRKm,"axG",@progbits,_ZNSt10_HashtableISt6vectorIPKN2v88internal6torque4TypeESaIS6_EESt4pairIKS8_PNS3_8CallableEESaISD_ENSt8__detail10_Select1stESt8equal_toIS8_ENS1_4base4hashIS8_EENSF_18_Mod_range_hashingENSF_20_Default_ranged_hashENSF_20_Prime_rehash_policyENSF_17_Hashtable_traitsILb1ELb0ELb1EEEE9_M_rehashEmRKm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableISt6vectorIPKN2v88internal6torque4TypeESaIS6_EESt4pairIKS8_PNS3_8CallableEESaISD_ENSt8__detail10_Select1stESt8equal_toIS8_ENS1_4base4hashIS8_EENSF_18_Mod_range_hashingENSF_20_Default_ranged_hashENSF_20_Prime_rehash_policyENSF_17_Hashtable_traitsILb1ELb0ELb1EEEE9_M_rehashEmRKm
	.type	_ZNSt10_HashtableISt6vectorIPKN2v88internal6torque4TypeESaIS6_EESt4pairIKS8_PNS3_8CallableEESaISD_ENSt8__detail10_Select1stESt8equal_toIS8_ENS1_4base4hashIS8_EENSF_18_Mod_range_hashingENSF_20_Default_ranged_hashENSF_20_Prime_rehash_policyENSF_17_Hashtable_traitsILb1ELb0ELb1EEEE9_M_rehashEmRKm, @function
_ZNSt10_HashtableISt6vectorIPKN2v88internal6torque4TypeESaIS6_EESt4pairIKS8_PNS3_8CallableEESaISD_ENSt8__detail10_Select1stESt8equal_toIS8_ENS1_4base4hashIS8_EENSF_18_Mod_range_hashingENSF_20_Default_ranged_hashENSF_20_Prime_rehash_policyENSF_17_Hashtable_traitsILb1ELb0ELb1EEEE9_M_rehashEmRKm:
.LFB11180:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA11180
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	cmpq	$1, %rsi
	je	.L1507
	movabsq	$1152921504606846975, %rax
	movq	%rdx, %r13
	cmpq	%rax, %rsi
	ja	.L1508
	leaq	0(,%rsi,8), %r14
	movq	%r14, %rdi
.LEHB214:
	call	_Znwm@PLT
	movq	%r14, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	memset@PLT
	leaq	48(%rbx), %r9
.L1487:
	movq	16(%rbx), %rsi
	movq	$0, 16(%rbx)
	testq	%rsi, %rsi
	je	.L1489
	xorl	%r8d, %r8d
	leaq	16(%rbx), %r10
	jmp	.L1490
	.p2align 4,,10
	.p2align 3
.L1491:
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	(%rdi), %rax
	movq	%rcx, (%rax)
.L1492:
	testq	%rsi, %rsi
	je	.L1489
.L1490:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	40(%rcx), %rax
	divq	%r12
	leaq	0(%r13,%rdx,8), %rdi
	movq	(%rdi), %rax
	testq	%rax, %rax
	jne	.L1491
	movq	16(%rbx), %rax
	movq	%rax, (%rcx)
	movq	%rcx, 16(%rbx)
	movq	%r10, (%rdi)
	cmpq	$0, (%rcx)
	je	.L1496
	movq	%rcx, 0(%r13,%r8,8)
	movq	%rdx, %r8
	testq	%rsi, %rsi
	jne	.L1490
	.p2align 4,,10
	.p2align 3
.L1489:
	movq	(%rbx), %rdi
	cmpq	%r9, %rdi
	je	.L1493
	call	_ZdlPv@PLT
.L1493:
	movq	%r12, 8(%rbx)
	movq	%r13, (%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1496:
	.cfi_restore_state
	movq	%rdx, %r8
	jmp	.L1492
	.p2align 4,,10
	.p2align 3
.L1507:
	leaq	48(%rdi), %r13
	movq	$0, 48(%rdi)
	movq	%r13, %r9
	jmp	.L1487
.L1508:
	call	_ZSt17__throw_bad_allocv@PLT
.LEHE214:
.L1497:
	endbr64
	movq	%rax, %rdi
.L1494:
	call	__cxa_begin_catch@PLT
	movq	0(%r13), %rax
	movq	%rax, 40(%rbx)
.LEHB215:
	call	__cxa_rethrow@PLT
.LEHE215:
.L1498:
	endbr64
	movq	%rax, %r12
.L1495:
	call	__cxa_end_catch@PLT
	movq	%r12, %rdi
.LEHB216:
	call	_Unwind_Resume@PLT
.LEHE216:
	.cfi_endproc
.LFE11180:
	.section	.gcc_except_table._ZNSt10_HashtableISt6vectorIPKN2v88internal6torque4TypeESaIS6_EESt4pairIKS8_PNS3_8CallableEESaISD_ENSt8__detail10_Select1stESt8equal_toIS8_ENS1_4base4hashIS8_EENSF_18_Mod_range_hashingENSF_20_Default_ranged_hashENSF_20_Prime_rehash_policyENSF_17_Hashtable_traitsILb1ELb0ELb1EEEE9_M_rehashEmRKm,"aG",@progbits,_ZNSt10_HashtableISt6vectorIPKN2v88internal6torque4TypeESaIS6_EESt4pairIKS8_PNS3_8CallableEESaISD_ENSt8__detail10_Select1stESt8equal_toIS8_ENS1_4base4hashIS8_EENSF_18_Mod_range_hashingENSF_20_Default_ranged_hashENSF_20_Prime_rehash_policyENSF_17_Hashtable_traitsILb1ELb0ELb1EEEE9_M_rehashEmRKm,comdat
	.align 4
.LLSDA11180:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATT11180-.LLSDATTD11180
.LLSDATTD11180:
	.byte	0x1
	.uleb128 .LLSDACSE11180-.LLSDACSB11180
.LLSDACSB11180:
	.uleb128 .LEHB214-.LFB11180
	.uleb128 .LEHE214-.LEHB214
	.uleb128 .L1497-.LFB11180
	.uleb128 0x1
	.uleb128 .LEHB215-.LFB11180
	.uleb128 .LEHE215-.LEHB215
	.uleb128 .L1498-.LFB11180
	.uleb128 0
	.uleb128 .LEHB216-.LFB11180
	.uleb128 .LEHE216-.LEHB216
	.uleb128 0
	.uleb128 0
.LLSDACSE11180:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATT11180:
	.section	.text._ZNSt10_HashtableISt6vectorIPKN2v88internal6torque4TypeESaIS6_EESt4pairIKS8_PNS3_8CallableEESaISD_ENSt8__detail10_Select1stESt8equal_toIS8_ENS1_4base4hashIS8_EENSF_18_Mod_range_hashingENSF_20_Default_ranged_hashENSF_20_Prime_rehash_policyENSF_17_Hashtable_traitsILb1ELb0ELb1EEEE9_M_rehashEmRKm,"axG",@progbits,_ZNSt10_HashtableISt6vectorIPKN2v88internal6torque4TypeESaIS6_EESt4pairIKS8_PNS3_8CallableEESaISD_ENSt8__detail10_Select1stESt8equal_toIS8_ENS1_4base4hashIS8_EENSF_18_Mod_range_hashingENSF_20_Default_ranged_hashENSF_20_Prime_rehash_policyENSF_17_Hashtable_traitsILb1ELb0ELb1EEEE9_M_rehashEmRKm,comdat
	.size	_ZNSt10_HashtableISt6vectorIPKN2v88internal6torque4TypeESaIS6_EESt4pairIKS8_PNS3_8CallableEESaISD_ENSt8__detail10_Select1stESt8equal_toIS8_ENS1_4base4hashIS8_EENSF_18_Mod_range_hashingENSF_20_Default_ranged_hashENSF_20_Prime_rehash_policyENSF_17_Hashtable_traitsILb1ELb0ELb1EEEE9_M_rehashEmRKm, .-_ZNSt10_HashtableISt6vectorIPKN2v88internal6torque4TypeESaIS6_EESt4pairIKS8_PNS3_8CallableEESaISD_ENSt8__detail10_Select1stESt8equal_toIS8_ENS1_4base4hashIS8_EENSF_18_Mod_range_hashingENSF_20_Default_ranged_hashENSF_20_Prime_rehash_policyENSF_17_Hashtable_traitsILb1ELb0ELb1EEEE9_M_rehashEmRKm
	.section	.text._ZNSt10_HashtableISt6vectorIPKN2v88internal6torque4TypeESaIS6_EESt4pairIKS8_PNS3_8CallableEESaISD_ENSt8__detail10_Select1stESt8equal_toIS8_ENS1_4base4hashIS8_EENSF_18_Mod_range_hashingENSF_20_Default_ranged_hashENSF_20_Prime_rehash_policyENSF_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSF_10_Hash_nodeISD_Lb1EEEm,"axG",@progbits,_ZNSt10_HashtableISt6vectorIPKN2v88internal6torque4TypeESaIS6_EESt4pairIKS8_PNS3_8CallableEESaISD_ENSt8__detail10_Select1stESt8equal_toIS8_ENS1_4base4hashIS8_EENSF_18_Mod_range_hashingENSF_20_Default_ranged_hashENSF_20_Prime_rehash_policyENSF_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSF_10_Hash_nodeISD_Lb1EEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableISt6vectorIPKN2v88internal6torque4TypeESaIS6_EESt4pairIKS8_PNS3_8CallableEESaISD_ENSt8__detail10_Select1stESt8equal_toIS8_ENS1_4base4hashIS8_EENSF_18_Mod_range_hashingENSF_20_Default_ranged_hashENSF_20_Prime_rehash_policyENSF_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSF_10_Hash_nodeISD_Lb1EEEm
	.type	_ZNSt10_HashtableISt6vectorIPKN2v88internal6torque4TypeESaIS6_EESt4pairIKS8_PNS3_8CallableEESaISD_ENSt8__detail10_Select1stESt8equal_toIS8_ENS1_4base4hashIS8_EENSF_18_Mod_range_hashingENSF_20_Default_ranged_hashENSF_20_Prime_rehash_policyENSF_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSF_10_Hash_nodeISD_Lb1EEEm, @function
_ZNSt10_HashtableISt6vectorIPKN2v88internal6torque4TypeESaIS6_EESt4pairIKS8_PNS3_8CallableEESaISD_ENSt8__detail10_Select1stESt8equal_toIS8_ENS1_4base4hashIS8_EENSF_18_Mod_range_hashingENSF_20_Default_ranged_hashENSF_20_Prime_rehash_policyENSF_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSF_10_Hash_nodeISD_Lb1EEEm:
.LFB10773:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA10773
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rcx, %r12
	movq	%r8, %rcx
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	addq	$32, %rdi
	subq	$16, %rsp
	movq	-8(%rdi), %rdx
	movq	-24(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	%rax, -48(%rbp)
.LEHB217:
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
.LEHE217:
	testb	%al, %al
	je	.L1510
	movq	%rdx, %rsi
	movq	%rbx, %rdi
	leaq	-48(%rbp), %rdx
.LEHB218:
	call	_ZNSt10_HashtableISt6vectorIPKN2v88internal6torque4TypeESaIS6_EESt4pairIKS8_PNS3_8CallableEESaISD_ENSt8__detail10_Select1stESt8equal_toIS8_ENS1_4base4hashIS8_EENSF_18_Mod_range_hashingENSF_20_Default_ranged_hashENSF_20_Prime_rehash_policyENSF_17_Hashtable_traitsILb1ELb0ELb1EEEE9_M_rehashEmRKm
.LEHE218:
	movq	%r14, %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	%rdx, %r13
.L1510:
	movq	%r14, 40(%r12)
	movq	(%rbx), %rax
	leaq	0(,%r13,8), %rcx
	movq	(%rax,%r13,8), %rax
	testq	%rax, %rax
	je	.L1511
	movq	(%rax), %rax
	movq	%rax, (%r12)
	movq	(%rbx), %rax
	movq	(%rax,%r13,8), %rax
	movq	%r12, (%rax)
.L1512:
	addq	$1, 24(%rbx)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1528
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1511:
	.cfi_restore_state
	movq	16(%rbx), %rax
	movq	%rax, (%r12)
	movq	%r12, 16(%rbx)
	movq	(%r12), %rax
	testq	%rax, %rax
	je	.L1513
	movq	40(%rax), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	(%rbx), %rax
	movq	%r12, (%rax,%rdx,8)
.L1513:
	movq	(%rbx), %rax
	leaq	16(%rbx), %rdx
	movq	%rdx, (%rax,%rcx)
	jmp	.L1512
.L1528:
	call	__stack_chk_fail@PLT
.L1518:
	endbr64
	movq	%rax, %rdi
.L1514:
	call	__cxa_begin_catch@PLT
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1515
	call	_ZdlPv@PLT
.L1515:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.LEHB219:
	call	__cxa_rethrow@PLT
.LEHE219:
.L1519:
	endbr64
	movq	%rax, %r12
.L1516:
	call	__cxa_end_catch@PLT
	movq	%r12, %rdi
.LEHB220:
	call	_Unwind_Resume@PLT
.LEHE220:
	.cfi_endproc
.LFE10773:
	.section	.gcc_except_table._ZNSt10_HashtableISt6vectorIPKN2v88internal6torque4TypeESaIS6_EESt4pairIKS8_PNS3_8CallableEESaISD_ENSt8__detail10_Select1stESt8equal_toIS8_ENS1_4base4hashIS8_EENSF_18_Mod_range_hashingENSF_20_Default_ranged_hashENSF_20_Prime_rehash_policyENSF_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSF_10_Hash_nodeISD_Lb1EEEm,"aG",@progbits,_ZNSt10_HashtableISt6vectorIPKN2v88internal6torque4TypeESaIS6_EESt4pairIKS8_PNS3_8CallableEESaISD_ENSt8__detail10_Select1stESt8equal_toIS8_ENS1_4base4hashIS8_EENSF_18_Mod_range_hashingENSF_20_Default_ranged_hashENSF_20_Prime_rehash_policyENSF_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSF_10_Hash_nodeISD_Lb1EEEm,comdat
	.align 4
.LLSDA10773:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATT10773-.LLSDATTD10773
.LLSDATTD10773:
	.byte	0x1
	.uleb128 .LLSDACSE10773-.LLSDACSB10773
.LLSDACSB10773:
	.uleb128 .LEHB217-.LFB10773
	.uleb128 .LEHE217-.LEHB217
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB218-.LFB10773
	.uleb128 .LEHE218-.LEHB218
	.uleb128 .L1518-.LFB10773
	.uleb128 0x1
	.uleb128 .LEHB219-.LFB10773
	.uleb128 .LEHE219-.LEHB219
	.uleb128 .L1519-.LFB10773
	.uleb128 0
	.uleb128 .LEHB220-.LFB10773
	.uleb128 .LEHE220-.LEHB220
	.uleb128 0
	.uleb128 0
.LLSDACSE10773:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATT10773:
	.section	.text._ZNSt10_HashtableISt6vectorIPKN2v88internal6torque4TypeESaIS6_EESt4pairIKS8_PNS3_8CallableEESaISD_ENSt8__detail10_Select1stESt8equal_toIS8_ENS1_4base4hashIS8_EENSF_18_Mod_range_hashingENSF_20_Default_ranged_hashENSF_20_Prime_rehash_policyENSF_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSF_10_Hash_nodeISD_Lb1EEEm,"axG",@progbits,_ZNSt10_HashtableISt6vectorIPKN2v88internal6torque4TypeESaIS6_EESt4pairIKS8_PNS3_8CallableEESaISD_ENSt8__detail10_Select1stESt8equal_toIS8_ENS1_4base4hashIS8_EENSF_18_Mod_range_hashingENSF_20_Default_ranged_hashENSF_20_Prime_rehash_policyENSF_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSF_10_Hash_nodeISD_Lb1EEEm,comdat
	.size	_ZNSt10_HashtableISt6vectorIPKN2v88internal6torque4TypeESaIS6_EESt4pairIKS8_PNS3_8CallableEESaISD_ENSt8__detail10_Select1stESt8equal_toIS8_ENS1_4base4hashIS8_EENSF_18_Mod_range_hashingENSF_20_Default_ranged_hashENSF_20_Prime_rehash_policyENSF_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSF_10_Hash_nodeISD_Lb1EEEm, .-_ZNSt10_HashtableISt6vectorIPKN2v88internal6torque4TypeESaIS6_EESt4pairIKS8_PNS3_8CallableEESaISD_ENSt8__detail10_Select1stESt8equal_toIS8_ENS1_4base4hashIS8_EENSF_18_Mod_range_hashingENSF_20_Default_ranged_hashENSF_20_Prime_rehash_policyENSF_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSF_10_Hash_nodeISD_Lb1EEEm
	.section	.text._ZNSt8__detail9_Map_baseISt6vectorIPKN2v88internal6torque4TypeESaIS7_EESt4pairIKS9_PNS4_8CallableEESaISE_ENS_10_Select1stESt8equal_toIS9_ENS2_4base4hashIS9_EENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERSB_,"axG",@progbits,_ZNSt8__detail9_Map_baseISt6vectorIPKN2v88internal6torque4TypeESaIS7_EESt4pairIKS9_PNS4_8CallableEESaISE_ENS_10_Select1stESt8equal_toIS9_ENS2_4base4hashIS9_EENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERSB_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8__detail9_Map_baseISt6vectorIPKN2v88internal6torque4TypeESaIS7_EESt4pairIKS9_PNS4_8CallableEESaISE_ENS_10_Select1stESt8equal_toIS9_ENS2_4base4hashIS9_EENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERSB_
	.type	_ZNSt8__detail9_Map_baseISt6vectorIPKN2v88internal6torque4TypeESaIS7_EESt4pairIKS9_PNS4_8CallableEESaISE_ENS_10_Select1stESt8equal_toIS9_ENS2_4base4hashIS9_EENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERSB_, @function
_ZNSt8__detail9_Map_baseISt6vectorIPKN2v88internal6torque4TypeESaIS7_EESt4pairIKS9_PNS4_8CallableEESaISE_ENS_10_Select1stESt8equal_toIS9_ENS2_4base4hashIS9_EENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERSB_:
.LFB10102:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA10102
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	(%rsi), %r14
	movq	8(%rsi), %r15
	cmpq	%r15, %r14
	je	.L1530
	.p2align 4,,10
	.p2align 3
.L1531:
	movq	(%r14), %r8
	movq	%r12, %rdi
	addq	$8, %r14
	movq	%r8, -56(%rbp)
.LEHB221:
	call	_ZN2v84base10hash_valueEm@PLT
	movq	-56(%rbp), %r8
	movq	%rax, %r12
	movq	%r8, %rdi
	call	_ZN2v84base10hash_valueEm@PLT
	xorl	%edi, %edi
	movq	%rax, %rsi
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	%rax, %r12
	cmpq	%r14, %r15
	jne	.L1531
.L1530:
	movq	8(%r13), %r8
	movq	%r12, %rax
	xorl	%edx, %edx
	divq	%r8
	movq	0(%r13), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r15
	testq	%rax, %rax
	je	.L1532
	movq	(%rax), %r14
	movq	40(%r14), %rcx
.L1535:
	cmpq	%rcx, %r12
	je	.L1561
.L1533:
	movq	(%r14), %r14
	testq	%r14, %r14
	je	.L1532
	movq	40(%r14), %rcx
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%r8
	cmpq	%rdx, %r15
	je	.L1535
.L1532:
	movl	$48, %edi
	call	_Znwm@PLT
.LEHE221:
	movq	8(%rbx), %rdx
	movq	(%rbx), %rsi
	pxor	%xmm0, %xmm0
	movq	%rax, %r14
	movq	%rdx, %rax
	movups	%xmm0, (%r14)
	subq	%rsi, %rax
	movups	%xmm0, 16(%r14)
	movq	%rax, -56(%rbp)
	sarq	$3, %rax
	je	.L1562
	movabsq	$1152921504606846975, %rdx
	cmpq	%rdx, %rax
	ja	.L1563
	movq	-56(%rbp), %rdi
.LEHB222:
	call	_Znwm@PLT
.LEHE222:
	movq	8(%rbx), %rdx
	movq	(%rbx), %rsi
	movq	%rax, %rcx
	movq	%rdx, %rbx
	subq	%rsi, %rbx
.L1537:
	movq	-56(%rbp), %rax
	movq	%rcx, %xmm0
	punpcklqdq	%xmm0, %xmm0
	addq	%rcx, %rax
	movups	%xmm0, 8(%r14)
	movq	%rax, 24(%r14)
	cmpq	%rdx, %rsi
	je	.L1539
	movq	%rcx, %rdi
	movq	%rbx, %rdx
	call	memmove@PLT
	movq	%rax, %rcx
.L1539:
	addq	%rbx, %rcx
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rcx, 16(%r14)
	movl	$1, %r8d
	movq	%r14, %rcx
	movq	$0, 32(%r14)
.LEHB223:
	call	_ZNSt10_HashtableISt6vectorIPKN2v88internal6torque4TypeESaIS6_EESt4pairIKS8_PNS3_8CallableEESaISD_ENSt8__detail10_Select1stESt8equal_toIS8_ENS1_4base4hashIS8_EENSF_18_Mod_range_hashingENSF_20_Default_ranged_hashENSF_20_Prime_rehash_policyENSF_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSF_10_Hash_nodeISD_Lb1EEEm
.LEHE223:
	addq	$24, %rsp
	popq	%rbx
	addq	$32, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1561:
	.cfi_restore_state
	movq	(%rbx), %rdi
	movq	8(%rbx), %rdx
	movq	8(%r14), %rsi
	movq	16(%r14), %rax
	subq	%rdi, %rdx
	subq	%rsi, %rax
	cmpq	%rax, %rdx
	jne	.L1533
	testq	%rdx, %rdx
	jne	.L1564
.L1534:
	addq	$24, %rsp
	leaq	32(%r14), %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1564:
	.cfi_restore_state
	movq	%r8, -56(%rbp)
	call	memcmp@PLT
	movq	-56(%rbp), %r8
	testl	%eax, %eax
	je	.L1534
	jmp	.L1533
	.p2align 4,,10
	.p2align 3
.L1562:
	movq	-56(%rbp), %rbx
	xorl	%ecx, %ecx
	jmp	.L1537
.L1563:
.LEHB224:
	call	_ZSt17__throw_bad_allocv@PLT
.LEHE224:
.L1544:
	endbr64
	movq	%rax, %rdi
.L1541:
	call	__cxa_begin_catch@PLT
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.LEHB225:
	call	__cxa_rethrow@PLT
.LEHE225:
.L1545:
	endbr64
	movq	%rax, %r12
.L1542:
	call	__cxa_end_catch@PLT
	movq	%r12, %rdi
.LEHB226:
	call	_Unwind_Resume@PLT
.LEHE226:
	.cfi_endproc
.LFE10102:
	.section	.gcc_except_table._ZNSt8__detail9_Map_baseISt6vectorIPKN2v88internal6torque4TypeESaIS7_EESt4pairIKS9_PNS4_8CallableEESaISE_ENS_10_Select1stESt8equal_toIS9_ENS2_4base4hashIS9_EENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERSB_,"aG",@progbits,_ZNSt8__detail9_Map_baseISt6vectorIPKN2v88internal6torque4TypeESaIS7_EESt4pairIKS9_PNS4_8CallableEESaISE_ENS_10_Select1stESt8equal_toIS9_ENS2_4base4hashIS9_EENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERSB_,comdat
	.align 4
.LLSDA10102:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATT10102-.LLSDATTD10102
.LLSDATTD10102:
	.byte	0x1
	.uleb128 .LLSDACSE10102-.LLSDACSB10102
.LLSDACSB10102:
	.uleb128 .LEHB221-.LFB10102
	.uleb128 .LEHE221-.LEHB221
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB222-.LFB10102
	.uleb128 .LEHE222-.LEHB222
	.uleb128 .L1544-.LFB10102
	.uleb128 0x1
	.uleb128 .LEHB223-.LFB10102
	.uleb128 .LEHE223-.LEHB223
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB224-.LFB10102
	.uleb128 .LEHE224-.LEHB224
	.uleb128 .L1544-.LFB10102
	.uleb128 0x1
	.uleb128 .LEHB225-.LFB10102
	.uleb128 .LEHE225-.LEHB225
	.uleb128 .L1545-.LFB10102
	.uleb128 0
	.uleb128 .LEHB226-.LFB10102
	.uleb128 .LEHE226-.LEHB226
	.uleb128 0
	.uleb128 0
.LLSDACSE10102:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATT10102:
	.section	.text._ZNSt8__detail9_Map_baseISt6vectorIPKN2v88internal6torque4TypeESaIS7_EESt4pairIKS9_PNS4_8CallableEESaISE_ENS_10_Select1stESt8equal_toIS9_ENS2_4base4hashIS9_EENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERSB_,"axG",@progbits,_ZNSt8__detail9_Map_baseISt6vectorIPKN2v88internal6torque4TypeESaIS7_EESt4pairIKS9_PNS4_8CallableEESaISE_ENS_10_Select1stESt8equal_toIS9_ENS2_4base4hashIS9_EENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERSB_,comdat
	.size	_ZNSt8__detail9_Map_baseISt6vectorIPKN2v88internal6torque4TypeESaIS7_EESt4pairIKS9_PNS4_8CallableEESaISE_ENS_10_Select1stESt8equal_toIS9_ENS2_4base4hashIS9_EENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERSB_, .-_ZNSt8__detail9_Map_baseISt6vectorIPKN2v88internal6torque4TypeESaIS7_EESt4pairIKS9_PNS4_8CallableEESaISE_ENS_10_Select1stESt8equal_toIS9_ENS2_4base4hashIS9_EENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERSB_
	.section	.rodata._ZN2v88internal6torque18DeclarationVisitor10SpecializeERKNS1_17SpecializationKeyINS1_7GenericEEEPNS1_19CallableDeclarationENS_4base8OptionalIPKNS1_25SpecializationDeclarationEEENSB_IPNS1_9StatementEEENS1_14SourcePositionE.str1.8,"aMS",@progbits,1
	.align 8
.LC51:
	.string	"number of template parameters ("
	.section	.rodata._ZN2v88internal6torque18DeclarationVisitor10SpecializeERKNS1_17SpecializationKeyINS1_7GenericEEEPNS1_19CallableDeclarationENS_4base8OptionalIPKNS1_25SpecializationDeclarationEEENSB_IPNS1_9StatementEEENS1_14SourcePositionE.str1.1,"aMS",@progbits,1
.LC52:
	.string	"%lu"
.LC53:
	.string	") to intantiation of generic "
	.section	.rodata._ZN2v88internal6torque18DeclarationVisitor10SpecializeERKNS1_17SpecializationKeyINS1_7GenericEEEPNS1_19CallableDeclarationENS_4base8OptionalIPKNS1_25SpecializationDeclarationEEENSB_IPNS1_9StatementEEENS1_14SourcePositionE.str1.8
	.align 8
.LC54:
	.string	" doesnt match the generic's declaration ("
	.section	.rodata._ZN2v88internal6torque18DeclarationVisitor10SpecializeERKNS1_17SpecializationKeyINS1_7GenericEEEPNS1_19CallableDeclarationENS_4base8OptionalIPKNS1_25SpecializationDeclarationEEENSB_IPNS1_9StatementEEENS1_14SourcePositionE.str1.1
.LC55:
	.string	")"
.LC56:
	.string	"<"
.LC57:
	.string	", "
.LC58:
	.string	">"
.LC59:
	.string	"unimplemented code"
.LC60:
	.string	" with types <"
	.section	.rodata._ZN2v88internal6torque18DeclarationVisitor10SpecializeERKNS1_17SpecializationKeyINS1_7GenericEEEPNS1_19CallableDeclarationENS_4base8OptionalIPKNS1_25SpecializationDeclarationEEENSB_IPNS1_9StatementEEENS1_14SourcePositionE.str1.8
	.align 8
.LC61:
	.string	"cannot redeclare specialization of "
	.section	.text.unlikely._ZN2v88internal6torque18DeclarationVisitor10SpecializeERKNS1_17SpecializationKeyINS1_7GenericEEEPNS1_19CallableDeclarationENS_4base8OptionalIPKNS1_25SpecializationDeclarationEEENSB_IPNS1_9StatementEEENS1_14SourcePositionE,"ax",@progbits
	.align 2
.LCOLDB63:
	.section	.text._ZN2v88internal6torque18DeclarationVisitor10SpecializeERKNS1_17SpecializationKeyINS1_7GenericEEEPNS1_19CallableDeclarationENS_4base8OptionalIPKNS1_25SpecializationDeclarationEEENSB_IPNS1_9StatementEEENS1_14SourcePositionE,"ax",@progbits
.LHOTB63:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque18DeclarationVisitor10SpecializeERKNS1_17SpecializationKeyINS1_7GenericEEEPNS1_19CallableDeclarationENS_4base8OptionalIPKNS1_25SpecializationDeclarationEEENSB_IPNS1_9StatementEEENS1_14SourcePositionE
	.type	_ZN2v88internal6torque18DeclarationVisitor10SpecializeERKNS1_17SpecializationKeyINS1_7GenericEEEPNS1_19CallableDeclarationENS_4base8OptionalIPKNS1_25SpecializationDeclarationEEENSB_IPNS1_9StatementEEENS1_14SourcePositionE, @function
_ZN2v88internal6torque18DeclarationVisitor10SpecializeERKNS1_17SpecializationKeyINS1_7GenericEEEPNS1_19CallableDeclarationENS_4base8OptionalIPKNS1_25SpecializationDeclarationEEENSB_IPNS1_9StatementEEENS1_14SourcePositionE:
.LFB6566:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6566
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$920, %rsp
	movq	%rsi, -904(%rbp)
	movdqu	16(%rbp), %xmm1
	movl	%edx, -888(%rbp)
	movq	%rdi, -896(%rbp)
	movq	%rcx, -920(%rbp)
	movq	%r9, -944(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	32(%rbp), %eax
	movaps	%xmm1, -864(%rbp)
	movl	%eax, -848(%rbp)
.LEHB227:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rax
	movq	%rax, -840(%rbp)
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
.LEHE227:
	leaq	-864(%rbp), %rdx
	movq	%rdx, (%rax)
	movq	(%rbx), %rax
	movq	104(%rax), %rbx
	movq	40(%rbx), %rdx
	movq	32(%rbx), %rsi
	movq	%rdx, %r12
	subq	%rsi, %r12
	movq	%r12, %rax
	sarq	$3, %rax
	je	.L1893
	movabsq	$1152921504606846975, %rdx
	cmpq	%rdx, %rax
	ja	.L1894
	movq	%r12, %rdi
.LEHB228:
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	40(%rbx), %rax
	movq	32(%rbx), %rsi
	movq	%rax, %r12
	subq	%rsi, %r12
	movq	%r12, %r13
	sarq	$3, %r13
	cmpq	%rsi, %rax
	je	.L1570
.L1567:
	movq	%r12, %rdx
	call	memmove@PLT
	movq	%rax, %rdi
.L1570:
	call	_ZdlPv@PLT
.L1568:
	movq	-896(%rbp), %rax
	movq	16(%rax), %r14
	movq	8(%rax), %rbx
	movq	%r14, %rax
	subq	%rbx, %rax
	cmpq	%r12, %rax
	jne	.L1895
	movq	-896(%rbp), %rax
	xorl	%r12d, %r12d
	movq	(%rax), %r15
	addq	$8, %rax
	movq	%rax, -912(%rbp)
	cmpq	%r14, %rbx
	je	.L1574
	.p2align 4,,10
	.p2align 3
.L1575:
	movq	%r12, %rdi
	movq	(%rbx), %r13
	call	_ZN2v84base10hash_valueEm@PLT
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZN2v84base10hash_valueEm@PLT
	movq	%rax, %rsi
	xorl	%edi, %edi
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	%rax, %rdi
	movq	%r12, %rsi
	call	_ZN2v84base12hash_combineEmm@PLT
	addq	$8, %rbx
	movq	%rax, %r12
	cmpq	%rbx, %r14
	jne	.L1575
.L1574:
	movq	120(%r15), %r13
	movq	%r12, %rax
	xorl	%edx, %edx
	divq	%r13
	movq	112(%r15), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r14
	testq	%rax, %rax
	je	.L1576
	movq	(%rax), %rbx
	movq	-896(%rbp), %r15
	movq	40(%rbx), %rcx
.L1579:
	cmpq	%rcx, %r12
	je	.L1896
.L1577:
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L1576
	movq	40(%rbx), %rcx
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%r13
	cmpq	%rdx, %r14
	je	.L1579
.L1576:
	leaq	-736(%rbp), %rax
	cmpb	$0, -888(%rbp)
	movq	%rax, -952(%rbp)
	jne	.L1897
	movq	-896(%rbp), %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal6torque18DeclarationVisitor24MakeSpecializedSignatureERKNS1_17SpecializationKeyINS1_7GenericEEE
.LEHE228:
.L1581:
	movq	-904(%rbp), %rax
	movq	-912(%rbp), %rdx
	leaq	-832(%rbp), %rdi
	movq	40(%rax), %rax
	leaq	32(%rax), %rsi
	movq	%rax, -888(%rbp)
.LEHB229:
	call	_ZN2v88internal6torque12Declarations24GetGeneratedCallableNameERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKSt6vectorIPKNS1_4TypeESaISE_EE@PLT
.LEHE229:
	leaq	-320(%rbp), %r15
	leaq	-448(%rbp), %rbx
	movq	%r15, %rdi
	movq	%rbx, -960(%rbp)
	movq	%r15, -920(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	movw	%cx, -96(%rbp)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%rax, -320(%rbp)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	$0, -104(%rbp)
	movq	%rax, -448(%rbp)
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	-24(%rax), %rax
	movq	%rdx, -448(%rbp,%rax)
	movq	-448(%rbp), %rax
	movq	$0, -440(%rbp)
	addq	-24(%rax), %rbx
	movq	%rbx, %rdi
.LEHB230:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE230:
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	leaq	-432(%rbp), %r12
	xorl	%esi, %esi
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rdx, -432(%rbp,%rax)
	movq	-432(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
.LEHB231:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE231:
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	leaq	-424(%rbp), %r13
	movq	.LC27(%rip), %xmm0
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movhps	.LC28(%rip), %xmm0
	movq	%rdx, -448(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movq	%rax, -320(%rbp)
	leaq	-368(%rbp), %rax
	movq	%rax, %rdi
	movaps	%xmm0, -432(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rax, -928(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movl	$24, -360(%rbp)
	movq	%rax, -936(%rbp)
	movq	%rax, -352(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
.LEHB232:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE232:
	movq	-904(%rbp), %rax
	movq	%r12, %rdi
	movq	40(%rax), %rax
	movq	40(%rax), %rdx
	movq	32(%rax), %rsi
.LEHB233:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rax, %rdi
	movl	$1, %edx
	leaq	.LC56(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-896(%rbp), %rax
	movq	8(%rax), %rbx
	movq	16(%rax), %r15
	cmpq	%r15, %rbx
	je	.L1587
	leaq	-576(%rbp), %rax
	movq	(%rbx), %r13
	leaq	-592(%rbp), %r14
	movq	%rax, -888(%rbp)
	jmp	.L1589
	.p2align 4,,10
	.p2align 3
.L1898:
	movl	$2, %edx
	leaq	.LC57(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rbx), %r13
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L1589:
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZNK2v88internal6torque4Type8ToStringB5cxx11Ev@PLT
.LEHE233:
	movq	-584(%rbp), %rdx
	movq	-592(%rbp), %rsi
	movq	%r12, %rdi
.LEHB234:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE234:
	movq	-592(%rbp), %rdi
	cmpq	-888(%rbp), %rdi
	je	.L1588
	call	_ZdlPv@PLT
.L1588:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	jne	.L1898
.L1587:
	movl	$1, %edx
	leaq	.LC58(%rip), %rsi
	movq	%r12, %rdi
.LEHB235:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE235:
	movq	-904(%rbp), %rax
	movl	8(%rax), %ecx
	cmpl	$53, %ecx
	ja	.L1593
	leaq	.L1595(%rip), %rsi
	movl	%ecx, %edx
	movslq	(%rsi,%rdx,4), %rax
	addq	%rsi, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal6torque18DeclarationVisitor10SpecializeERKNS1_17SpecializationKeyINS1_7GenericEEEPNS1_19CallableDeclarationENS_4base8OptionalIPKNS1_25SpecializationDeclarationEEENSB_IPNS1_9StatementEEENS1_14SourcePositionE,"a",@progbits
	.align 4
	.align 4
.L1595:
	.long	.L1594-.L1595
	.long	.L1594-.L1595
	.long	.L1594-.L1595
	.long	.L1594-.L1595
	.long	.L1594-.L1595
	.long	.L1594-.L1595
	.long	.L1594-.L1595
	.long	.L1594-.L1595
	.long	.L1594-.L1595
	.long	.L1594-.L1595
	.long	.L1594-.L1595
	.long	.L1594-.L1595
	.long	.L1594-.L1595
	.long	.L1594-.L1595
	.long	.L1594-.L1595
	.long	.L1594-.L1595
	.long	.L1594-.L1595
	.long	.L1594-.L1595
	.long	.L1594-.L1595
	.long	.L1594-.L1595
	.long	.L1594-.L1595
	.long	.L1594-.L1595
	.long	.L1594-.L1595
	.long	.L1594-.L1595
	.long	.L1594-.L1595
	.long	.L1594-.L1595
	.long	.L1594-.L1595
	.long	.L1594-.L1595
	.long	.L1594-.L1595
	.long	.L1594-.L1595
	.long	.L1594-.L1595
	.long	.L1594-.L1595
	.long	.L1594-.L1595
	.long	.L1594-.L1595
	.long	.L1594-.L1595
	.long	.L1594-.L1595
	.long	.L1594-.L1595
	.long	.L1594-.L1595
	.long	.L1594-.L1595
	.long	.L1594-.L1595
	.long	.L1594-.L1595
	.long	.L1594-.L1595
	.long	.L1594-.L1595
	.long	.L1594-.L1595
	.long	.L1594-.L1595
	.long	.L1594-.L1595
	.long	.L1596-.L1595
	.long	.L1594-.L1595
	.long	.L1596-.L1595
	.long	.L1594-.L1595
	.long	.L1594-.L1595
	.long	.L1594-.L1595
	.long	.L1594-.L1595
	.long	.L1594-.L1595
	.section	.text._ZN2v88internal6torque18DeclarationVisitor10SpecializeERKNS1_17SpecializationKeyINS1_7GenericEEEPNS1_19CallableDeclarationENS_4base8OptionalIPKNS1_25SpecializationDeclarationEEENSB_IPNS1_9StatementEEENS1_14SourcePositionE
	.p2align 4,,10
	.p2align 3
.L1896:
	movq	8(%r15), %rdi
	movq	16(%r15), %rdx
	movq	8(%rbx), %rsi
	movq	16(%rbx), %rax
	subq	%rdi, %rdx
	subq	%rsi, %rax
	cmpq	%rax, %rdx
	jne	.L1577
	testq	%rdx, %rdx
	jne	.L1899
.L1578:
	movq	-896(%rbp), %rax
	movq	-912(%rbp), %rcx
	leaq	.LC58(%rip), %r8
	leaq	.LC60(%rip), %rdx
	leaq	.LC61(%rip), %rdi
	movq	(%rax), %rsi
	addq	$72, %rsi
.LEHB236:
	call	_ZN2v88internal6torque11ReportErrorIJRA36_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA14_S3_RKSt6vectorIPKNS1_4TypeESaISJ_EERA2_S3_EEEvDpOT_
.LEHE236:
	.p2align 4,,10
	.p2align 3
.L1594:
	cmpl	$51, %ecx
	jne	.L1900
	movq	-904(%rbp), %rax
	movq	-952(%rbp), %rsi
	movq	40(%rax), %rdi
	addq	$32, %rdi
.LEHB237:
	call	_ZN2v88internal6torque12Declarations15CreateIntrinsicERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKNS1_9SignatureE@PLT
	movq	%rax, %r12
	.p2align 4,,10
	.p2align 3
.L1662:
	movq	-896(%rbp), %rax
	movq	-912(%rbp), %rsi
	movq	(%rax), %rdi
	addq	$112, %rdi
	call	_ZNSt8__detail9_Map_baseISt6vectorIPKN2v88internal6torque4TypeESaIS7_EESt4pairIKS9_PNS4_8CallableEESaISE_ENS_10_Select1stESt8equal_toIS9_ENS2_4base4hashIS9_EENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERSB_
.LEHE237:
	movq	.LC27(%rip), %xmm0
	movq	%r12, (%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movq	-352(%rbp), %rdi
	movhps	.LC62(%rip), %xmm0
	movq	%rax, -320(%rbp)
	movaps	%xmm0, -432(%rbp)
	cmpq	-936(%rbp), %rdi
	je	.L1726
	call	_ZdlPv@PLT
.L1726:
	movq	-928(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	-920(%rbp), %rdi
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rdx, -448(%rbp,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rdx, -432(%rbp,%rax)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rdx, -448(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	movq	$0, -440(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-832(%rbp), %rdi
	leaq	-816(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1727
	call	_ZdlPv@PLT
.L1727:
	movq	-616(%rbp), %rbx
	movq	-624(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L1728
	.p2align 4,,10
	.p2align 3
.L1732:
	movq	8(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1729
	call	_ZdlPv@PLT
	addq	$32, %r13
	cmpq	%r13, %rbx
	jne	.L1732
.L1730:
	movq	-624(%rbp), %r13
.L1728:
	testq	%r13, %r13
	je	.L1733
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1733:
	movq	-672(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1734
	call	_ZdlPv@PLT
.L1734:
	cmpb	$0, -712(%rbp)
	je	.L1735
	movq	-704(%rbp), %rdi
	leaq	-688(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1735
	call	_ZdlPv@PLT
.L1735:
	movq	-736(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1736
	call	_ZdlPv@PLT
.L1736:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
	movq	-840(%rbp), %rdx
	movq	%rdx, (%rax)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1901
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1897:
	.cfi_restore_state
	movq	-920(%rbp), %rsi
	movq	%rax, %rdi
.LEHB238:
	call	_ZN2v88internal6torque11TypeVisitor13MakeSignatureEPKNS1_19CallableDeclarationE@PLT
.LEHE238:
	jmp	.L1581
.L1596:
	movq	-728(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	-736(%rbp), %rsi
	movq	$0, -576(%rbp)
	movaps	%xmm0, -592(%rbp)
	movq	%rax, %rbx
	subq	%rsi, %rbx
	movq	%rbx, %rdx
	sarq	$3, %rdx
	je	.L1902
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L1903
	movq	%rbx, %rdi
.LEHB239:
	call	_Znwm@PLT
.LEHE239:
	movq	%rax, %rcx
	movq	-728(%rbp), %rax
	movq	-736(%rbp), %rsi
	movq	%rax, %r12
	subq	%rsi, %r12
.L1600:
	movq	%rcx, %xmm0
	addq	%rcx, %rbx
	punpcklqdq	%xmm0, %xmm0
	movq	%rbx, -576(%rbp)
	movaps	%xmm0, -592(%rbp)
	cmpq	%rsi, %rax
	je	.L1602
	movq	%rcx, %rdi
	movq	%r12, %rdx
	call	memmove@PLT
	movq	%rax, %rcx
.L1602:
	addq	%r12, %rcx
	cmpb	$0, -712(%rbp)
	movb	$0, -568(%rbp)
	movq	%rcx, -584(%rbp)
	movb	$0, -560(%rbp)
	jne	.L1904
.L1603:
	movq	-664(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	-672(%rbp), %rsi
	movq	$0, -512(%rbp)
	movaps	%xmm0, -528(%rbp)
	movq	%rax, %rbx
	subq	%rsi, %rbx
	movq	%rbx, %rdx
	sarq	$3, %rdx
	je	.L1905
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L1906
	movq	%rbx, %rdi
.LEHB240:
	call	_Znwm@PLT
.LEHE240:
	movq	%rax, %rcx
	movq	-664(%rbp), %rax
	movq	-672(%rbp), %rsi
	movq	%rax, %r12
	subq	%rsi, %r12
.L1610:
	movq	%rcx, %xmm0
	addq	%rcx, %rbx
	punpcklqdq	%xmm0, %xmm0
	movq	%rbx, -512(%rbp)
	movaps	%xmm0, -528(%rbp)
	cmpq	%rsi, %rax
	je	.L1615
	movq	%rcx, %rdi
	movq	%r12, %rdx
	call	memmove@PLT
	movq	%rax, %rcx
.L1615:
	movzbl	-648(%rbp), %eax
	movq	-616(%rbp), %r14
	addq	%r12, %rcx
	movq	$0, -480(%rbp)
	movq	-624(%rbp), %r13
	movq	%rcx, -520(%rbp)
	movb	%al, -504(%rbp)
	movq	%r14, %rbx
	movq	-640(%rbp), %rax
	movq	$0, -472(%rbp)
	subq	%r13, %rbx
	movq	%rax, -496(%rbp)
	movq	-632(%rbp), %rax
	movq	$0, -464(%rbp)
	movq	%rax, -488(%rbp)
	movq	%rbx, %rax
	sarq	$5, %rax
	je	.L1907
	movabsq	$288230376151711743, %rdx
	cmpq	%rdx, %rax
	ja	.L1908
	movq	%rbx, %rdi
.LEHB241:
	call	_Znwm@PLT
.LEHE241:
	movq	%rax, -888(%rbp)
	movq	-616(%rbp), %r14
	movq	-624(%rbp), %r13
.L1617:
	movq	-888(%rbp), %rax
	movq	%rax, %xmm0
	addq	%rax, %rbx
	punpcklqdq	%xmm0, %xmm0
	movq	%rbx, -464(%rbp)
	movq	%rax, %rbx
	movaps	%xmm0, -480(%rbp)
	cmpq	%r13, %r14
	je	.L1619
	movabsq	$1152921504606846975, %r15
	jmp	.L1625
	.p2align 4,,10
	.p2align 3
.L1620:
	cmpq	%r15, %rax
	ja	.L1909
	movq	%r12, %rdi
.LEHB242:
	call	_Znwm@PLT
.LEHE242:
	movq	%rax, %rcx
.L1621:
	movq	%rcx, %xmm0
	addq	%rcx, %r12
	punpcklqdq	%xmm0, %xmm0
	movq	%r12, 24(%rbx)
	movups	%xmm0, 8(%rbx)
	movq	16(%r13), %rax
	movq	8(%r13), %rsi
	movq	%rax, %r12
	subq	%rsi, %r12
	cmpq	%rsi, %rax
	je	.L1880
	movq	%rcx, %rdi
	movq	%r12, %rdx
	call	memmove@PLT
	movq	%rax, %rcx
.L1880:
	addq	%r12, %rcx
	addq	$32, %r13
	addq	$32, %rbx
	movq	%rcx, -16(%rbx)
	cmpq	%r13, %r14
	je	.L1619
.L1625:
	movq	0(%r13), %rax
	movq	%rax, (%rbx)
	movq	16(%r13), %r12
	subq	8(%r13), %r12
	movq	$0, 8(%rbx)
	movq	%r12, %rax
	movq	$0, 16(%rbx)
	sarq	$3, %rax
	movq	$0, 24(%rbx)
	jne	.L1620
	xorl	%ecx, %ecx
	jmp	.L1621
	.p2align 4,,10
	.p2align 3
.L1729:
	addq	$32, %r13
	cmpq	%r13, %rbx
	jne	.L1732
	jmp	.L1730
	.p2align 4,,10
	.p2align 3
.L1619:
	movzbl	-600(%rbp), %eax
	movq	%rbx, -472(%rbp)
	leaq	-800(%rbp), %r15
	movq	$0, -792(%rbp)
	movb	%al, -456(%rbp)
	leaq	-784(%rbp), %rax
	movq	%rax, -888(%rbp)
	movq	%rax, -800(%rbp)
	movq	-384(%rbp), %rax
	movb	$0, -784(%rbp)
	testq	%rax, %rax
	je	.L1910
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	jbe	.L1639
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%rax, %r8
.LEHB243:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.LEHE243:
.L1640:
	movq	-832(%rbp), %r14
	movq	-824(%rbp), %r12
	leaq	-752(%rbp), %rbx
	movq	%rbx, -768(%rbp)
	movq	%r14, %rax
	addq	%r12, %rax
	je	.L1643
	testq	%r14, %r14
	je	.L1911
.L1643:
	movq	%r12, -872(%rbp)
	cmpq	$15, %r12
	ja	.L1912
	cmpq	$1, %r12
	jne	.L1649
	movzbl	(%r14), %eax
	leaq	-768(%rbp), %r13
	movb	%al, -752(%rbp)
	movq	%rbx, %rax
.L1650:
	subq	$8, %rsp
	movq	%r12, -760(%rbp)
	xorl	%edx, %edx
	movq	%r15, %rsi
	movb	$0, (%rax,%r12)
	leaq	-592(%rbp), %r14
	movq	-944(%rbp), %r9
	movq	%r13, %rdi
	pushq	$1
	movl	$1, %r8d
	movq	%r14, %rcx
.LEHB244:
	.cfi_escape 0x2e,0x10
	call	_ZN2v88internal6torque12Declarations17CreateTorqueMacroENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES8_bNS1_9SignatureENS_4base8OptionalIPNS1_9StatementEEEb@PLT
.LEHE244:
	movq	-768(%rbp), %rdi
	movq	%rax, %r12
	popq	%rax
	popq	%rdx
	cmpq	%rbx, %rdi
	je	.L1651
	call	_ZdlPv@PLT
.L1651:
	movq	-800(%rbp), %rdi
	cmpq	-888(%rbp), %rdi
	je	.L1652
	call	_ZdlPv@PLT
.L1652:
	movq	-472(%rbp), %rbx
	movq	-480(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L1717
	.p2align 4,,10
	.p2align 3
.L1657:
	movq	8(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1654
	call	_ZdlPv@PLT
	addq	$32, %r13
	cmpq	%r13, %rbx
	jne	.L1657
.L1719:
	movq	-480(%rbp), %r13
.L1717:
	testq	%r13, %r13
	je	.L1722
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1722:
	movq	-528(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1723
	call	_ZdlPv@PLT
.L1723:
	cmpb	$0, -568(%rbp)
	je	.L1724
	movq	-560(%rbp), %rdi
	leaq	-544(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1724
	call	_ZdlPv@PLT
.L1724:
	movq	-592(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1662
	call	_ZdlPv@PLT
	jmp	.L1662
	.p2align 4,,10
	.p2align 3
.L1654:
	addq	$32, %r13
	cmpq	%r13, %rbx
	jne	.L1657
	jmp	.L1719
	.p2align 4,,10
	.p2align 3
.L1639:
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
.LEHB245:
	.cfi_escape 0x2e,0
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.LEHE245:
	jmp	.L1640
	.p2align 4,,10
	.p2align 3
.L1893:
	movq	%rax, %r13
	xorl	%edi, %edi
	cmpq	%rdx, %rsi
	jne	.L1567
	jmp	.L1568
	.p2align 4,,10
	.p2align 3
.L1649:
	testq	%r12, %r12
	jne	.L1913
	movq	%rbx, %rax
	leaq	-768(%rbp), %r13
	jmp	.L1650
	.p2align 4,,10
	.p2align 3
.L1904:
	movq	-704(%rbp), %r13
	movq	-696(%rbp), %r12
	leaq	-544(%rbp), %rbx
	movq	%rbx, -560(%rbp)
	movq	%r13, %rax
	addq	%r12, %rax
	je	.L1604
	testq	%r13, %r13
	je	.L1914
.L1604:
	movq	%r12, -872(%rbp)
	cmpq	$15, %r12
	ja	.L1915
	cmpq	$1, %r12
	jne	.L1607
	movzbl	0(%r13), %eax
	movb	%al, -544(%rbp)
.L1608:
	movq	%r12, -552(%rbp)
	movb	$0, (%rbx,%r12)
	movb	$1, -568(%rbp)
	jmp	.L1603
	.p2align 4,,10
	.p2align 3
.L1912:
	leaq	-768(%rbp), %r13
	leaq	-872(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r13, %rdi
.LEHB246:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE246:
	movq	%rax, -768(%rbp)
	movq	%rax, %rdi
	movq	-872(%rbp), %rax
	movq	%rax, -752(%rbp)
.L1648:
	movq	%r12, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-872(%rbp), %r12
	movq	-768(%rbp), %rax
	jmp	.L1650
	.p2align 4,,10
	.p2align 3
.L1905:
	movq	%rbx, %r12
	xorl	%ecx, %ecx
	jmp	.L1610
	.p2align 4,,10
	.p2align 3
.L1902:
	movq	%rbx, %r12
	xorl	%ecx, %ecx
	jmp	.L1600
	.p2align 4,,10
	.p2align 3
.L1907:
	movq	$0, -888(%rbp)
	jmp	.L1617
.L1915:
	leaq	-872(%rbp), %rsi
	leaq	-560(%rbp), %rdi
	xorl	%edx, %edx
.LEHB247:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE247:
	movq	%rax, -560(%rbp)
	movq	%rax, %rdi
	movq	-872(%rbp), %rax
	movq	%rax, -544(%rbp)
.L1606:
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	-872(%rbp), %r12
	movq	-560(%rbp), %rbx
	jmp	.L1608
	.p2align 4,,10
	.p2align 3
.L1899:
	call	memcmp@PLT
	testl	%eax, %eax
	je	.L1578
	jmp	.L1577
	.p2align 4,,10
	.p2align 3
.L1910:
	leaq	-352(%rbp), %rsi
	movq	%r15, %rdi
.LEHB248:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
.LEHE248:
	jmp	.L1640
.L1607:
	testq	%r12, %r12
	je	.L1608
	movq	%rbx, %rdi
	jmp	.L1606
	.p2align 4,,10
	.p2align 3
.L1900:
	movq	-728(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	-736(%rbp), %rsi
	movq	$0, -576(%rbp)
	movaps	%xmm0, -592(%rbp)
	movq	%rax, %rbx
	subq	%rsi, %rbx
	movq	%rbx, %rdx
	sarq	$3, %rdx
	je	.L1916
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L1917
	movq	%rbx, %rdi
.LEHB249:
	call	_Znwm@PLT
.LEHE249:
	movq	%rax, %rcx
	movq	-728(%rbp), %rax
	movq	-736(%rbp), %rsi
	movq	%rax, %r12
	subq	%rsi, %r12
.L1664:
	movq	%rcx, %xmm0
	addq	%rcx, %rbx
	punpcklqdq	%xmm0, %xmm0
	movq	%rbx, -576(%rbp)
	movaps	%xmm0, -592(%rbp)
	cmpq	%rsi, %rax
	je	.L1666
	movq	%rcx, %rdi
	movq	%r12, %rdx
	call	memmove@PLT
	movq	%rax, %rcx
.L1666:
	addq	%r12, %rcx
	cmpb	$0, -712(%rbp)
	movb	$0, -568(%rbp)
	movq	%rcx, -584(%rbp)
	movb	$0, -560(%rbp)
	jne	.L1918
.L1667:
	movq	-664(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	-672(%rbp), %rsi
	movq	$0, -512(%rbp)
	movaps	%xmm0, -528(%rbp)
	movq	%rax, %rbx
	subq	%rsi, %rbx
	movq	%rbx, %rdx
	sarq	$3, %rdx
	je	.L1919
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L1920
	movq	%rbx, %rdi
.LEHB250:
	call	_Znwm@PLT
.LEHE250:
	movq	%rax, %rcx
	movq	-664(%rbp), %rax
	movq	-672(%rbp), %rsi
	movq	%rax, %r12
	subq	%rsi, %r12
.L1674:
	movq	%rcx, %xmm0
	addq	%rcx, %rbx
	punpcklqdq	%xmm0, %xmm0
	movq	%rbx, -512(%rbp)
	movaps	%xmm0, -528(%rbp)
	cmpq	%rsi, %rax
	je	.L1679
	movq	%rcx, %rdi
	movq	%r12, %rdx
	call	memmove@PLT
	movq	%rax, %rcx
.L1679:
	movzbl	-648(%rbp), %eax
	movq	-616(%rbp), %r14
	addq	%r12, %rcx
	movq	$0, -480(%rbp)
	movq	-624(%rbp), %r13
	movq	%rcx, -520(%rbp)
	movb	%al, -504(%rbp)
	movq	%r14, %rbx
	movq	-640(%rbp), %rax
	movq	$0, -472(%rbp)
	subq	%r13, %rbx
	movq	%rax, -496(%rbp)
	movq	-632(%rbp), %rax
	movq	$0, -464(%rbp)
	movq	%rax, -488(%rbp)
	movq	%rbx, %rax
	sarq	$5, %rax
	je	.L1921
	movabsq	$288230376151711743, %rdx
	cmpq	%rdx, %rax
	ja	.L1922
	movq	%rbx, %rdi
.LEHB251:
	call	_Znwm@PLT
.LEHE251:
	movq	%rax, -888(%rbp)
	movq	-616(%rbp), %r14
	movq	-624(%rbp), %r13
.L1681:
	movq	-888(%rbp), %rax
	movq	%rax, %xmm0
	addq	%rax, %rbx
	punpcklqdq	%xmm0, %xmm0
	movq	%rbx, -464(%rbp)
	movq	%rax, %rbx
	movaps	%xmm0, -480(%rbp)
	cmpq	%r13, %r14
	je	.L1683
	movabsq	$1152921504606846975, %r15
	jmp	.L1689
	.p2align 4,,10
	.p2align 3
.L1684:
	cmpq	%r15, %rax
	ja	.L1923
	movq	%r12, %rdi
.LEHB252:
	call	_Znwm@PLT
.LEHE252:
	movq	%rax, %rcx
.L1685:
	movq	%rcx, %xmm0
	addq	%rcx, %r12
	punpcklqdq	%xmm0, %xmm0
	movq	%r12, 24(%rbx)
	movups	%xmm0, 8(%rbx)
	movq	16(%r13), %rax
	movq	8(%r13), %rsi
	movq	%rax, %r12
	subq	%rsi, %r12
	cmpq	%rsi, %rax
	je	.L1881
	movq	%rcx, %rdi
	movq	%r12, %rdx
	call	memmove@PLT
	movq	%rax, %rcx
.L1881:
	addq	%r12, %rcx
	addq	$32, %r13
	addq	$32, %rbx
	movq	%rcx, -16(%rbx)
	cmpq	%r13, %r14
	je	.L1683
.L1689:
	movq	0(%r13), %rax
	movq	%rax, (%rbx)
	movq	16(%r13), %r12
	subq	8(%r13), %r12
	movq	$0, 8(%rbx)
	movq	%r12, %rax
	movq	$0, 16(%rbx)
	sarq	$3, %rax
	movq	$0, 24(%rbx)
	jne	.L1684
	xorl	%ecx, %ecx
	jmp	.L1685
.L1916:
	movq	%rbx, %r12
	xorl	%ecx, %ecx
	jmp	.L1664
.L1683:
	movzbl	-600(%rbp), %eax
	movq	%rbx, -472(%rbp)
	leaq	-800(%rbp), %r15
	movq	$0, -792(%rbp)
	movb	%al, -456(%rbp)
	leaq	-784(%rbp), %rax
	movq	%rax, -888(%rbp)
	movq	%rax, -800(%rbp)
	movq	-384(%rbp), %rax
	movb	$0, -784(%rbp)
	testq	%rax, %rax
	je	.L1924
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	jbe	.L1703
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%rax, %r8
.LEHB253:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.LEHE253:
.L1704:
	movq	-832(%rbp), %r14
	movq	-824(%rbp), %r12
	leaq	-752(%rbp), %rbx
	movq	%rbx, -768(%rbp)
	movq	%r14, %rax
	addq	%r12, %rax
	je	.L1707
	testq	%r14, %r14
	je	.L1925
.L1707:
	movq	%r12, -872(%rbp)
	cmpq	$15, %r12
	ja	.L1926
	cmpq	$1, %r12
	jne	.L1713
	movzbl	(%r14), %eax
	leaq	-768(%rbp), %r13
	movb	%al, -752(%rbp)
	movq	%rbx, %rax
.L1714:
	movq	%r12, -760(%rbp)
	movl	$1, %r8d
	movq	%r15, %rdx
	leaq	-592(%rbp), %r14
	movb	$0, (%rax,%r12)
	movq	-944(%rbp), %r9
	movq	%r14, %rcx
	movq	%r13, %rsi
	movq	-904(%rbp), %rdi
.LEHB254:
	call	_ZN2v88internal6torque18DeclarationVisitor13CreateBuiltinEPNS1_18BuiltinDeclarationENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_NS1_9SignatureENS_4base8OptionalIPNS1_9StatementEEE
.LEHE254:
	movq	-768(%rbp), %rdi
	movq	%rax, %r12
	cmpq	%rbx, %rdi
	je	.L1715
	call	_ZdlPv@PLT
.L1715:
	movq	-800(%rbp), %rdi
	cmpq	-888(%rbp), %rdi
	je	.L1716
	call	_ZdlPv@PLT
.L1716:
	movq	-472(%rbp), %rbx
	movq	-480(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L1717
.L1721:
	movq	8(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1718
.L1927:
	call	_ZdlPv@PLT
	addq	$32, %r13
	cmpq	%rbx, %r13
	je	.L1719
	movq	8(%r13), %rdi
	testq	%rdi, %rdi
	jne	.L1927
.L1718:
	addq	$32, %r13
	cmpq	%r13, %rbx
	jne	.L1721
	jmp	.L1719
.L1703:
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
.LEHB255:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.LEHE255:
	jmp	.L1704
.L1713:
	testq	%r12, %r12
	jne	.L1928
	movq	%rbx, %rax
	leaq	-768(%rbp), %r13
	jmp	.L1714
.L1926:
	leaq	-768(%rbp), %r13
	leaq	-872(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r13, %rdi
.LEHB256:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE256:
	movq	%rax, -768(%rbp)
	movq	%rax, %rdi
	movq	-872(%rbp), %rax
	movq	%rax, -752(%rbp)
.L1712:
	movq	%r12, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-872(%rbp), %r12
	movq	-768(%rbp), %rax
	jmp	.L1714
.L1918:
	movq	-704(%rbp), %r13
	movq	-696(%rbp), %r12
	leaq	-544(%rbp), %rbx
	movq	%rbx, -560(%rbp)
	movq	%r13, %rax
	addq	%r12, %rax
	je	.L1668
	testq	%r13, %r13
	je	.L1929
.L1668:
	movq	%r12, -872(%rbp)
	cmpq	$15, %r12
	ja	.L1930
	cmpq	$1, %r12
	jne	.L1671
	movzbl	0(%r13), %eax
	movb	%al, -544(%rbp)
.L1672:
	movq	%r12, -552(%rbp)
	movb	$0, (%rbx,%r12)
	movb	$1, -568(%rbp)
	jmp	.L1667
.L1921:
	movq	$0, -888(%rbp)
	jmp	.L1681
.L1919:
	movq	%rbx, %r12
	xorl	%ecx, %ecx
	jmp	.L1674
.L1924:
	leaq	-352(%rbp), %rsi
	movq	%r15, %rdi
.LEHB257:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
.LEHE257:
	jmp	.L1704
.L1671:
	testq	%r12, %r12
	je	.L1672
	movq	%rbx, %rdi
.L1670:
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	-872(%rbp), %r12
	movq	-560(%rbp), %rbx
	jmp	.L1672
.L1930:
	leaq	-872(%rbp), %rsi
	leaq	-560(%rbp), %rdi
	xorl	%edx, %edx
.LEHB258:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE258:
	movq	%rax, -560(%rbp)
	movq	%rax, %rdi
	movq	-872(%rbp), %rax
	movq	%rax, -544(%rbp)
	jmp	.L1670
.L1909:
.LEHB259:
	call	_ZSt17__throw_bad_allocv@PLT
.LEHE259:
.L1593:
	leaq	.LC59(%rip), %rdi
	xorl	%eax, %eax
.LEHB260:
	call	_Z8V8_FatalPKcz@PLT
.LEHE260:
.L1928:
	movq	%rbx, %rdi
	leaq	-768(%rbp), %r13
	jmp	.L1712
.L1929:
	leaq	.LC4(%rip), %rdi
.LEHB261:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE261:
.L1925:
	leaq	.LC4(%rip), %rdi
.LEHB262:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE262:
.L1901:
	call	__stack_chk_fail@PLT
.L1923:
.LEHB263:
	call	_ZSt17__throw_bad_allocv@PLT
.LEHE263:
.L1913:
	movq	%rbx, %rdi
	leaq	-768(%rbp), %r13
	jmp	.L1648
.L1908:
.LEHB264:
	call	_ZSt17__throw_bad_allocv@PLT
.LEHE264:
.L1903:
.LEHB265:
	call	_ZSt17__throw_bad_allocv@PLT
.LEHE265:
.L1906:
.LEHB266:
	call	_ZSt17__throw_bad_allocv@PLT
.LEHE266:
.L1895:
	leaq	-448(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -960(%rbp)
.LEHB267:
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEC1Ev@PLT
.LEHE267:
	leaq	-432(%rbp), %r12
	movl	$31, %edx
	leaq	.LC51(%rip), %rsi
	movq	%r12, %rdi
.LEHB268:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-896(%rbp), %rax
	movq	vsnprintf@GOTPCREL(%rip), %r15
	leaq	-736(%rbp), %rdi
	leaq	.LC52(%rip), %rcx
	movl	$32, %edx
	movq	16(%rax), %r8
	subq	8(%rax), %r8
	movq	%r15, %rsi
	xorl	%eax, %eax
	sarq	$3, %r8
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0
.LEHE268:
	movq	-728(%rbp), %rdx
	movq	-736(%rbp), %rsi
	movq	%r12, %rdi
.LEHB269:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$29, %edx
	leaq	.LC53(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-904(%rbp), %rax
	movq	%r12, %rdi
	movq	40(%rax), %rax
	movq	40(%rax), %rdx
	movq	32(%rax), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$41, %edx
	leaq	.LC54(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	-592(%rbp), %r14
	movq	%r13, %r8
	movq	%r15, %rsi
	xorl	%eax, %eax
	leaq	.LC52(%rip), %rcx
	movl	$32, %edx
	movq	%r14, %rdi
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0
.LEHE269:
	movq	-584(%rbp), %rdx
	movq	-592(%rbp), %rsi
	movq	%r12, %rdi
.LEHB270:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rax, %rdi
	leaq	.LC55(%rip), %rsi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
.LEHE270:
	movq	-592(%rbp), %rdi
	leaq	-576(%rbp), %rbx
	cmpq	%rbx, %rdi
	je	.L1572
	call	_ZdlPv@PLT
.L1572:
	movq	-736(%rbp), %rdi
	leaq	-720(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1573
	call	_ZdlPv@PLT
.L1573:
	leaq	-424(%rbp), %rsi
	movq	%r14, %rdi
.LEHB271:
	call	_ZNKSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEE3strEv@PLT
.LEHE271:
	movq	%r14, %rdi
.LEHB272:
	call	_ZN2v88internal6torque11ReportErrorIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_
.LEHE272:
.L1894:
.LEHB273:
	call	_ZSt17__throw_bad_allocv@PLT
.LEHE273:
.L1911:
	leaq	.LC4(%rip), %rdi
.LEHB274:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE274:
.L1922:
.LEHB275:
	call	_ZSt17__throw_bad_allocv@PLT
.LEHE275:
.L1920:
.LEHB276:
	call	_ZSt17__throw_bad_allocv@PLT
.LEHE276:
.L1917:
.LEHB277:
	call	_ZSt17__throw_bad_allocv@PLT
.LEHE277:
.L1914:
	leaq	.LC4(%rip), %rdi
.LEHB278:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE278:
.L1781:
	endbr64
	movq	%rax, %r12
	jmp	.L1611
.L1767:
	endbr64
	movq	%rax, %r12
	jmp	.L1737
.L1770:
	endbr64
	movq	%rax, %rbx
	jmp	.L1592
.L1789:
	endbr64
	movq	%rax, %rdi
	jmp	.L1692
.L1783:
	endbr64
	movq	%rax, %rdi
	jmp	.L1628
.L1768:
	endbr64
	movq	%rax, %r12
	jmp	.L1742
.L1778:
	endbr64
	movq	%rax, %rbx
	jmp	.L1590
.L1771:
	endbr64
	movq	%rax, %r12
	leaq	-592(%rbp), %r14
	jmp	.L1747
.L1780:
	endbr64
	movq	%rax, %rbx
	jmp	.L1634
.L1786:
	endbr64
	movq	%rax, %rbx
	jmp	.L1698
.L1769:
	endbr64
	movq	%rax, %rbx
	jmp	.L1754
.L1775:
	endbr64
	movq	%rax, %rbx
	jmp	.L1584
.L1777:
	endbr64
	movq	%rax, %rbx
	jmp	.L1583
.L1776:
	endbr64
	movq	%rax, %rbx
	jmp	.L1585
.L1764:
	endbr64
	movq	%rax, %r12
	jmp	.L1744
.L1784:
	endbr64
	movq	%rax, %r12
	jmp	.L1644
.L1772:
	endbr64
	movq	%rax, %r12
	jmp	.L1745
.L1779:
	endbr64
	movq	%rax, %rbx
	jmp	.L1636
.L1773:
	endbr64
	movq	%rax, %r12
	leaq	-592(%rbp), %r14
	jmp	.L1751
.L1785:
	endbr64
	movq	%rax, %rbx
	jmp	.L1700
.L1766:
	endbr64
	movq	%rax, %r12
	jmp	.L1739
.L1765:
	endbr64
	movq	%rax, %r12
	jmp	.L1741
.L1774:
	endbr64
	movq	%rax, %r12
	jmp	.L1749
.L1790:
	endbr64
	movq	%rax, %r12
	jmp	.L1708
.L1787:
	endbr64
	movq	%rax, %r12
	jmp	.L1675
	.section	.gcc_except_table._ZN2v88internal6torque18DeclarationVisitor10SpecializeERKNS1_17SpecializationKeyINS1_7GenericEEEPNS1_19CallableDeclarationENS_4base8OptionalIPKNS1_25SpecializationDeclarationEEENSB_IPNS1_9StatementEEENS1_14SourcePositionE,"a",@progbits
	.align 4
.LLSDA6566:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATT6566-.LLSDATTD6566
.LLSDATTD6566:
	.byte	0x1
	.uleb128 .LLSDACSE6566-.LLSDACSB6566
.LLSDACSB6566:
	.uleb128 .LEHB227-.LFB6566
	.uleb128 .LEHE227-.LEHB227
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB228-.LFB6566
	.uleb128 .LEHE228-.LEHB228
	.uleb128 .L1764-.LFB6566
	.uleb128 0
	.uleb128 .LEHB229-.LFB6566
	.uleb128 .LEHE229-.LEHB229
	.uleb128 .L1769-.LFB6566
	.uleb128 0
	.uleb128 .LEHB230-.LFB6566
	.uleb128 .LEHE230-.LEHB230
	.uleb128 .L1775-.LFB6566
	.uleb128 0
	.uleb128 .LEHB231-.LFB6566
	.uleb128 .LEHE231-.LEHB231
	.uleb128 .L1777-.LFB6566
	.uleb128 0
	.uleb128 .LEHB232-.LFB6566
	.uleb128 .LEHE232-.LEHB232
	.uleb128 .L1776-.LFB6566
	.uleb128 0
	.uleb128 .LEHB233-.LFB6566
	.uleb128 .LEHE233-.LEHB233
	.uleb128 .L1770-.LFB6566
	.uleb128 0
	.uleb128 .LEHB234-.LFB6566
	.uleb128 .LEHE234-.LEHB234
	.uleb128 .L1778-.LFB6566
	.uleb128 0
	.uleb128 .LEHB235-.LFB6566
	.uleb128 .LEHE235-.LEHB235
	.uleb128 .L1770-.LFB6566
	.uleb128 0
	.uleb128 .LEHB236-.LFB6566
	.uleb128 .LEHE236-.LEHB236
	.uleb128 .L1764-.LFB6566
	.uleb128 0
	.uleb128 .LEHB237-.LFB6566
	.uleb128 .LEHE237-.LEHB237
	.uleb128 .L1770-.LFB6566
	.uleb128 0
	.uleb128 .LEHB238-.LFB6566
	.uleb128 .LEHE238-.LEHB238
	.uleb128 .L1764-.LFB6566
	.uleb128 0
	.uleb128 .LEHB239-.LFB6566
	.uleb128 .LEHE239-.LEHB239
	.uleb128 .L1770-.LFB6566
	.uleb128 0
	.uleb128 .LEHB240-.LFB6566
	.uleb128 .LEHE240-.LEHB240
	.uleb128 .L1779-.LFB6566
	.uleb128 0
	.uleb128 .LEHB241-.LFB6566
	.uleb128 .LEHE241-.LEHB241
	.uleb128 .L1780-.LFB6566
	.uleb128 0
	.uleb128 .LEHB242-.LFB6566
	.uleb128 .LEHE242-.LEHB242
	.uleb128 .L1783-.LFB6566
	.uleb128 0x1
	.uleb128 .LEHB243-.LFB6566
	.uleb128 .LEHE243-.LEHB243
	.uleb128 .L1784-.LFB6566
	.uleb128 0
	.uleb128 .LEHB244-.LFB6566
	.uleb128 .LEHE244-.LEHB244
	.uleb128 .L1772-.LFB6566
	.uleb128 0
	.uleb128 .LEHB245-.LFB6566
	.uleb128 .LEHE245-.LEHB245
	.uleb128 .L1784-.LFB6566
	.uleb128 0
	.uleb128 .LEHB246-.LFB6566
	.uleb128 .LEHE246-.LEHB246
	.uleb128 .L1771-.LFB6566
	.uleb128 0
	.uleb128 .LEHB247-.LFB6566
	.uleb128 .LEHE247-.LEHB247
	.uleb128 .L1781-.LFB6566
	.uleb128 0
	.uleb128 .LEHB248-.LFB6566
	.uleb128 .LEHE248-.LEHB248
	.uleb128 .L1784-.LFB6566
	.uleb128 0
	.uleb128 .LEHB249-.LFB6566
	.uleb128 .LEHE249-.LEHB249
	.uleb128 .L1770-.LFB6566
	.uleb128 0
	.uleb128 .LEHB250-.LFB6566
	.uleb128 .LEHE250-.LEHB250
	.uleb128 .L1785-.LFB6566
	.uleb128 0
	.uleb128 .LEHB251-.LFB6566
	.uleb128 .LEHE251-.LEHB251
	.uleb128 .L1786-.LFB6566
	.uleb128 0
	.uleb128 .LEHB252-.LFB6566
	.uleb128 .LEHE252-.LEHB252
	.uleb128 .L1789-.LFB6566
	.uleb128 0x1
	.uleb128 .LEHB253-.LFB6566
	.uleb128 .LEHE253-.LEHB253
	.uleb128 .L1790-.LFB6566
	.uleb128 0
	.uleb128 .LEHB254-.LFB6566
	.uleb128 .LEHE254-.LEHB254
	.uleb128 .L1774-.LFB6566
	.uleb128 0
	.uleb128 .LEHB255-.LFB6566
	.uleb128 .LEHE255-.LEHB255
	.uleb128 .L1790-.LFB6566
	.uleb128 0
	.uleb128 .LEHB256-.LFB6566
	.uleb128 .LEHE256-.LEHB256
	.uleb128 .L1773-.LFB6566
	.uleb128 0
	.uleb128 .LEHB257-.LFB6566
	.uleb128 .LEHE257-.LEHB257
	.uleb128 .L1790-.LFB6566
	.uleb128 0
	.uleb128 .LEHB258-.LFB6566
	.uleb128 .LEHE258-.LEHB258
	.uleb128 .L1787-.LFB6566
	.uleb128 0
	.uleb128 .LEHB259-.LFB6566
	.uleb128 .LEHE259-.LEHB259
	.uleb128 .L1783-.LFB6566
	.uleb128 0x1
	.uleb128 .LEHB260-.LFB6566
	.uleb128 .LEHE260-.LEHB260
	.uleb128 .L1770-.LFB6566
	.uleb128 0
	.uleb128 .LEHB261-.LFB6566
	.uleb128 .LEHE261-.LEHB261
	.uleb128 .L1787-.LFB6566
	.uleb128 0
	.uleb128 .LEHB262-.LFB6566
	.uleb128 .LEHE262-.LEHB262
	.uleb128 .L1773-.LFB6566
	.uleb128 0
	.uleb128 .LEHB263-.LFB6566
	.uleb128 .LEHE263-.LEHB263
	.uleb128 .L1789-.LFB6566
	.uleb128 0x1
	.uleb128 .LEHB264-.LFB6566
	.uleb128 .LEHE264-.LEHB264
	.uleb128 .L1780-.LFB6566
	.uleb128 0
	.uleb128 .LEHB265-.LFB6566
	.uleb128 .LEHE265-.LEHB265
	.uleb128 .L1770-.LFB6566
	.uleb128 0
	.uleb128 .LEHB266-.LFB6566
	.uleb128 .LEHE266-.LEHB266
	.uleb128 .L1779-.LFB6566
	.uleb128 0
	.uleb128 .LEHB267-.LFB6566
	.uleb128 .LEHE267-.LEHB267
	.uleb128 .L1764-.LFB6566
	.uleb128 0
	.uleb128 .LEHB268-.LFB6566
	.uleb128 .LEHE268-.LEHB268
	.uleb128 .L1765-.LFB6566
	.uleb128 0
	.uleb128 .LEHB269-.LFB6566
	.uleb128 .LEHE269-.LEHB269
	.uleb128 .L1766-.LFB6566
	.uleb128 0
	.uleb128 .LEHB270-.LFB6566
	.uleb128 .LEHE270-.LEHB270
	.uleb128 .L1767-.LFB6566
	.uleb128 0
	.uleb128 .LEHB271-.LFB6566
	.uleb128 .LEHE271-.LEHB271
	.uleb128 .L1765-.LFB6566
	.uleb128 0
	.uleb128 .LEHB272-.LFB6566
	.uleb128 .LEHE272-.LEHB272
	.uleb128 .L1768-.LFB6566
	.uleb128 0
	.uleb128 .LEHB273-.LFB6566
	.uleb128 .LEHE273-.LEHB273
	.uleb128 .L1764-.LFB6566
	.uleb128 0
	.uleb128 .LEHB274-.LFB6566
	.uleb128 .LEHE274-.LEHB274
	.uleb128 .L1771-.LFB6566
	.uleb128 0
	.uleb128 .LEHB275-.LFB6566
	.uleb128 .LEHE275-.LEHB275
	.uleb128 .L1786-.LFB6566
	.uleb128 0
	.uleb128 .LEHB276-.LFB6566
	.uleb128 .LEHE276-.LEHB276
	.uleb128 .L1785-.LFB6566
	.uleb128 0
	.uleb128 .LEHB277-.LFB6566
	.uleb128 .LEHE277-.LEHB277
	.uleb128 .L1770-.LFB6566
	.uleb128 0
	.uleb128 .LEHB278-.LFB6566
	.uleb128 .LEHE278-.LEHB278
	.uleb128 .L1781-.LFB6566
	.uleb128 0
.LLSDACSE6566:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATT6566:
	.section	.text._ZN2v88internal6torque18DeclarationVisitor10SpecializeERKNS1_17SpecializationKeyINS1_7GenericEEEPNS1_19CallableDeclarationENS_4base8OptionalIPKNS1_25SpecializationDeclarationEEENSB_IPNS1_9StatementEEENS1_14SourcePositionE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque18DeclarationVisitor10SpecializeERKNS1_17SpecializationKeyINS1_7GenericEEEPNS1_19CallableDeclarationENS_4base8OptionalIPKNS1_25SpecializationDeclarationEEENSB_IPNS1_9StatementEEENS1_14SourcePositionE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6566
	.type	_ZN2v88internal6torque18DeclarationVisitor10SpecializeERKNS1_17SpecializationKeyINS1_7GenericEEEPNS1_19CallableDeclarationENS_4base8OptionalIPKNS1_25SpecializationDeclarationEEENSB_IPNS1_9StatementEEENS1_14SourcePositionE.cold, @function
_ZN2v88internal6torque18DeclarationVisitor10SpecializeERKNS1_17SpecializationKeyINS1_7GenericEEEPNS1_19CallableDeclarationENS_4base8OptionalIPKNS1_25SpecializationDeclarationEEENSB_IPNS1_9StatementEEENS1_14SourcePositionE.cold:
.LFSB6566:
.L1611:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	cmpb	$0, -568(%rbp)
	je	.L1612
	movq	-560(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1612
	call	_ZdlPv@PLT
.L1612:
	movq	%r12, %rbx
.L1613:
	movq	-592(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1931
.L1592:
	movq	-960(%rbp), %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
.L1586:
	movq	-832(%rbp), %rdi
	leaq	-816(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1754
	call	_ZdlPv@PLT
.L1754:
	movq	-952(%rbp), %rdi
	movq	%rbx, %r12
	call	_ZN2v88internal6torque9SignatureD1Ev
	jmp	.L1744
.L1737:
	movq	-592(%rbp), %rdi
	leaq	-576(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1739
	call	_ZdlPv@PLT
.L1739:
	movq	-736(%rbp), %rdi
	leaq	-720(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1741
	call	_ZdlPv@PLT
.L1741:
	movq	-960(%rbp), %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
.L1744:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
	movq	-840(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rdx, (%rax)
.LEHB279:
	call	_Unwind_Resume@PLT
.LEHE279:
.L1742:
	movq	-592(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1741
	call	_ZdlPv@PLT
	jmp	.L1741
.L1590:
	movq	-592(%rbp), %rdi
	leaq	-576(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1592
	call	_ZdlPv@PLT
	jmp	.L1592
.L1692:
	call	__cxa_begin_catch@PLT
	movq	-888(%rbp), %r12
.L1695:
	cmpq	%rbx, %r12
	jne	.L1932
.LEHB280:
	call	__cxa_rethrow@PLT
.LEHE280:
.L1745:
	movq	-768(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1747
	call	_ZdlPv@PLT
.L1747:
	movq	-800(%rbp), %rdi
	cmpq	-888(%rbp), %rdi
	je	.L1646
	call	_ZdlPv@PLT
.L1646:
	movq	%r14, %rdi
	movq	%r12, %rbx
	call	_ZN2v88internal6torque9SignatureD1Ev
	jmp	.L1592
.L1628:
	call	__cxa_begin_catch@PLT
	movq	-888(%rbp), %r12
.L1631:
	cmpq	%rbx, %r12
	jne	.L1933
.LEHB281:
	call	__cxa_rethrow@PLT
.LEHE281:
.L1782:
	endbr64
	movq	%rax, %rbx
	call	__cxa_end_catch@PLT
	movq	-480(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1634
	call	_ZdlPv@PLT
.L1634:
	movq	-528(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1636
	call	_ZdlPv@PLT
.L1636:
	cmpb	$0, -568(%rbp)
	je	.L1613
	movq	-560(%rbp), %rdi
	leaq	-544(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1613
	call	_ZdlPv@PLT
	jmp	.L1613
.L1931:
	call	_ZdlPv@PLT
	jmp	.L1592
.L1932:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1694
	call	_ZdlPv@PLT
.L1694:
	addq	$32, %r12
	jmp	.L1695
.L1788:
	endbr64
	movq	%rax, %rbx
	call	__cxa_end_catch@PLT
	movq	-480(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1698
	call	_ZdlPv@PLT
.L1698:
	movq	-528(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1700
	call	_ZdlPv@PLT
.L1700:
	cmpb	$0, -568(%rbp)
	je	.L1677
	movq	-560(%rbp), %rdi
	leaq	-544(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1677
	call	_ZdlPv@PLT
.L1677:
	movq	-592(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1592
	call	_ZdlPv@PLT
	jmp	.L1592
.L1933:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1630
	call	_ZdlPv@PLT
.L1630:
	addq	$32, %r12
	jmp	.L1631
.L1583:
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rdx, -448(%rbp,%rax)
	movq	$0, -440(%rbp)
.L1584:
	movq	-920(%rbp), %rdi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	jmp	.L1586
.L1585:
	movq	%r13, %rdi
	call	_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED1Ev
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rdx, -448(%rbp,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rdx, -432(%rbp,%rax)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rdx, -448(%rbp,%rax)
	movq	$0, -440(%rbp)
	jmp	.L1584
.L1644:
	movq	-800(%rbp), %rdi
	cmpq	-888(%rbp), %rdi
	je	.L1645
	call	_ZdlPv@PLT
.L1645:
	leaq	-592(%rbp), %r14
	jmp	.L1646
.L1749:
	movq	-768(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1751
	call	_ZdlPv@PLT
.L1751:
	movq	-800(%rbp), %rdi
	cmpq	-888(%rbp), %rdi
	je	.L1710
	call	_ZdlPv@PLT
.L1710:
	movq	%r14, %rdi
	movq	%r12, %rbx
	call	_ZN2v88internal6torque9SignatureD1Ev
	jmp	.L1592
.L1708:
	movq	-800(%rbp), %rdi
	cmpq	-888(%rbp), %rdi
	je	.L1709
	call	_ZdlPv@PLT
.L1709:
	leaq	-592(%rbp), %r14
	jmp	.L1710
.L1675:
	cmpb	$0, -568(%rbp)
	je	.L1676
	movq	-560(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1676
	call	_ZdlPv@PLT
.L1676:
	movq	%r12, %rbx
	jmp	.L1677
	.cfi_endproc
.LFE6566:
	.section	.gcc_except_table._ZN2v88internal6torque18DeclarationVisitor10SpecializeERKNS1_17SpecializationKeyINS1_7GenericEEEPNS1_19CallableDeclarationENS_4base8OptionalIPKNS1_25SpecializationDeclarationEEENSB_IPNS1_9StatementEEENS1_14SourcePositionE
	.align 4
.LLSDAC6566:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATTC6566-.LLSDATTDC6566
.LLSDATTDC6566:
	.byte	0x1
	.uleb128 .LLSDACSEC6566-.LLSDACSBC6566
.LLSDACSBC6566:
	.uleb128 .LEHB279-.LCOLDB63
	.uleb128 .LEHE279-.LEHB279
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB280-.LCOLDB63
	.uleb128 .LEHE280-.LEHB280
	.uleb128 .L1788-.LCOLDB63
	.uleb128 0
	.uleb128 .LEHB281-.LCOLDB63
	.uleb128 .LEHE281-.LEHB281
	.uleb128 .L1782-.LCOLDB63
	.uleb128 0
.LLSDACSEC6566:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATTC6566:
	.section	.text.unlikely._ZN2v88internal6torque18DeclarationVisitor10SpecializeERKNS1_17SpecializationKeyINS1_7GenericEEEPNS1_19CallableDeclarationENS_4base8OptionalIPKNS1_25SpecializationDeclarationEEENSB_IPNS1_9StatementEEENS1_14SourcePositionE
	.section	.text._ZN2v88internal6torque18DeclarationVisitor10SpecializeERKNS1_17SpecializationKeyINS1_7GenericEEEPNS1_19CallableDeclarationENS_4base8OptionalIPKNS1_25SpecializationDeclarationEEENSB_IPNS1_9StatementEEENS1_14SourcePositionE
	.size	_ZN2v88internal6torque18DeclarationVisitor10SpecializeERKNS1_17SpecializationKeyINS1_7GenericEEEPNS1_19CallableDeclarationENS_4base8OptionalIPKNS1_25SpecializationDeclarationEEENSB_IPNS1_9StatementEEENS1_14SourcePositionE, .-_ZN2v88internal6torque18DeclarationVisitor10SpecializeERKNS1_17SpecializationKeyINS1_7GenericEEEPNS1_19CallableDeclarationENS_4base8OptionalIPKNS1_25SpecializationDeclarationEEENSB_IPNS1_9StatementEEENS1_14SourcePositionE
	.section	.text.unlikely._ZN2v88internal6torque18DeclarationVisitor10SpecializeERKNS1_17SpecializationKeyINS1_7GenericEEEPNS1_19CallableDeclarationENS_4base8OptionalIPKNS1_25SpecializationDeclarationEEENSB_IPNS1_9StatementEEENS1_14SourcePositionE
	.size	_ZN2v88internal6torque18DeclarationVisitor10SpecializeERKNS1_17SpecializationKeyINS1_7GenericEEEPNS1_19CallableDeclarationENS_4base8OptionalIPKNS1_25SpecializationDeclarationEEENSB_IPNS1_9StatementEEENS1_14SourcePositionE.cold, .-_ZN2v88internal6torque18DeclarationVisitor10SpecializeERKNS1_17SpecializationKeyINS1_7GenericEEEPNS1_19CallableDeclarationENS_4base8OptionalIPKNS1_25SpecializationDeclarationEEENSB_IPNS1_9StatementEEENS1_14SourcePositionE.cold
.LCOLDE63:
	.section	.text._ZN2v88internal6torque18DeclarationVisitor10SpecializeERKNS1_17SpecializationKeyINS1_7GenericEEEPNS1_19CallableDeclarationENS_4base8OptionalIPKNS1_25SpecializationDeclarationEEENSB_IPNS1_9StatementEEENS1_14SourcePositionE
.LHOTE63:
	.section	.rodata._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_25SpecializationDeclarationE.str1.1,"aMS",@progbits,1
.LC64:
	.string	"specialization of "
	.section	.rodata._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_25SpecializationDeclarationE.str1.8,"aMS",@progbits,1
	.align 8
.LC65:
	.string	" is ambigous, it matches more than one generic declaration ("
	.section	.rodata._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_25SpecializationDeclarationE.str1.1
.LC66:
	.string	" and "
	.section	.rodata._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_25SpecializationDeclarationE.str1.8
	.align 8
.LC67:
	.string	"no generic defined with the name "
	.align 8
.LC68:
	.string	" doesn't match any generic declaration\n"
	.section	.rodata._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_25SpecializationDeclarationE.str1.1
.LC69:
	.string	"specialization signature:"
.LC70:
	.string	"\n  "
.LC71:
	.string	"\ncandidates are:"
	.section	.text.unlikely._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_25SpecializationDeclarationE,"ax",@progbits
	.align 2
.LCOLDB72:
	.section	.text._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_25SpecializationDeclarationE,"ax",@progbits
.LHOTB72:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_25SpecializationDeclarationE
	.type	_ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_25SpecializationDeclarationE, @function
_ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_25SpecializationDeclarationE:
.LFB6548:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6548
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	leaq	-832(%rbp), %rdi
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$888, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	40(%r14), %rax
	leaq	32(%rax), %rsi
.LEHB282:
	call	_ZN2v88internal6torque12Declarations13LookupGenericERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
.LEHE282:
	leaq	-736(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -912(%rbp)
.LEHB283:
	call	_ZN2v88internal6torque11TypeVisitor13MakeSignatureEPKNS1_19CallableDeclarationE@PLT
.LEHE283:
	movq	-824(%rbp), %rax
	movq	-832(%rbp), %r12
	leaq	-864(%rbp), %rcx
	movq	$0, -896(%rbp)
	movq	%rcx, -904(%rbp)
	movq	%rax, -920(%rbp)
	cmpq	%r12, %rax
	je	.L1969
	.p2align 4,,10
	.p2align 3
.L1968:
	movq	(%r12), %rax
	movq	200(%r14), %rbx
	movq	$0, -792(%rbp)
	movq	208(%r14), %r13
	movq	$0, -784(%rbp)
	movq	%rax, -888(%rbp)
	movq	%rax, -800(%rbp)
	movq	$0, -776(%rbp)
	cmpq	%r13, %rbx
	jne	.L1940
	jmp	.L1937
	.p2align 4,,10
	.p2align 3
.L2138:
	addq	$8, %rbx
	movq	%rax, (%rsi)
	addq	$8, -784(%rbp)
	cmpq	%rbx, %r13
	je	.L1937
.L1940:
	movq	(%rbx), %rdi
.LEHB284:
	call	_ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_14TypeExpressionE@PLT
	movq	%rax, -864(%rbp)
	movq	-784(%rbp), %rsi
	cmpq	-776(%rbp), %rsi
	jne	.L2138
	movq	-904(%rbp), %rdx
	leaq	-792(%rbp), %rdi
	call	_ZNSt6vectorIPKN2v88internal6torque4TypeESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
.LEHE284:
	addq	$8, %rbx
	cmpq	%rbx, %r13
	jne	.L1940
	.p2align 4,,10
	.p2align 3
.L1937:
	leaq	-592(%rbp), %r13
	leaq	-800(%rbp), %r15
	movq	%r15, %rsi
	movq	%r13, %rdi
.LEHB285:
	call	_ZN2v88internal6torque18DeclarationVisitor24MakeSpecializedSignatureERKNS1_17SpecializationKeyINS1_7GenericEEE
.LEHE285:
	movq	-792(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1944
	call	_ZdlPv@PLT
.L1944:
	movq	-912(%rbp), %rdi
	movl	$1, %edx
	movq	%r13, %rsi
.LEHB286:
	call	_ZNK2v88internal6torque9Signature14HasSameTypesAsERKS2_NS1_13ParameterModeE@PLT
.LEHE286:
	testb	%al, %al
	je	.L1945
	cmpq	$0, -896(%rbp)
	jne	.L2139
	movq	-888(%rbp), %rax
	movq	%rax, -896(%rbp)
.L1945:
	movq	-472(%rbp), %rbx
	movq	-480(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L1957
	.p2align 4,,10
	.p2align 3
.L1961:
	movq	8(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1958
	call	_ZdlPv@PLT
	addq	$32, %r13
	cmpq	%rbx, %r13
	jne	.L1961
.L1959:
	movq	-480(%rbp), %r13
.L1957:
	testq	%r13, %r13
	je	.L1962
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1962:
	movq	-528(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1963
	call	_ZdlPv@PLT
.L1963:
	cmpb	$0, -568(%rbp)
	je	.L1964
	movq	-560(%rbp), %rdi
	leaq	-544(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1964
	call	_ZdlPv@PLT
.L1964:
	movq	-592(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1965
	call	_ZdlPv@PLT
	addq	$8, %r12
	cmpq	%r12, -920(%rbp)
	jne	.L1968
	cmpq	$0, -896(%rbp)
	je	.L1969
.L2129:
.LEHB287:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_13GlobalContextEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rax
	cmpb	$0, (%rax)
	je	.L2003
	movq	-896(%rbp), %rcx
	cmpl	$-1, 44(%rcx)
	leaq	24(%rcx), %rax
	je	.L2005
	leaq	44(%rcx), %rax
.L2005:
	movdqu	(%rax), %xmm0
	subq	$48, %rsp
	movaps	%xmm0, -864(%rbp)
	movl	16(%rax), %eax
	movups	%xmm0, 24(%rsp)
	movl	%eax, -848(%rbp)
	movl	%eax, 40(%rsp)
	movq	40(%r14), %rax
	movdqu	12(%rax), %xmm3
	movups	%xmm3, (%rsp)
	movl	28(%rax), %eax
	movl	%eax, 16(%rsp)
	.cfi_escape 0x2e,0x30
	call	_ZN2v88internal6torque18LanguageServerData13AddDefinitionENS1_14SourcePositionES3_@PLT
.LEHE287:
	addq	$48, %rsp
.L2003:
	movq	-896(%rbp), %rcx
	movq	200(%r14), %rbx
	movq	208(%r14), %r12
	movq	224(%r14), %r13
	movq	104(%rcx), %rax
	movq	56(%rax), %rax
	movq	%rcx, -800(%rbp)
	movq	$0, -792(%rbp)
	movq	%rax, -888(%rbp)
	movq	$0, -784(%rbp)
	movq	$0, -776(%rbp)
	cmpq	%r12, %rbx
	je	.L2006
	leaq	-872(%rbp), %rax
	movq	%rax, -896(%rbp)
	jmp	.L2009
.L2140:
	addq	$8, %rbx
	movq	%rax, (%rsi)
	addq	$8, -784(%rbp)
	cmpq	%rbx, %r12
	je	.L2006
.L2009:
	movq	(%rbx), %rdi
.LEHB288:
	.cfi_escape 0x2e,0
	call	_ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_14TypeExpressionE@PLT
	movq	%rax, -872(%rbp)
	movq	-784(%rbp), %rsi
	cmpq	-776(%rbp), %rsi
	jne	.L2140
	movq	-896(%rbp), %rdx
	leaq	-792(%rbp), %rdi
	call	_ZNSt6vectorIPKN2v88internal6torque4TypeESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
.LEHE288:
	addq	$8, %rbx
	cmpq	%rbx, %r12
	jne	.L2009
.L2006:
	movdqu	12(%r14), %xmm4
	subq	$32, %rsp
	movq	%r13, %r9
	movq	%r14, %rcx
	movq	-888(%rbp), %rsi
	movl	$1, %r8d
	movl	$1, %edx
	movq	%r15, %rdi
	movups	%xmm4, (%rsp)
	movl	28(%r14), %eax
	movl	%eax, 16(%rsp)
.LEHB289:
	.cfi_escape 0x2e,0x20
	call	_ZN2v88internal6torque18DeclarationVisitor10SpecializeERKNS1_17SpecializationKeyINS1_7GenericEEEPNS1_19CallableDeclarationENS_4base8OptionalIPKNS1_25SpecializationDeclarationEEENSB_IPNS1_9StatementEEENS1_14SourcePositionE
.LEHE289:
	movq	-792(%rbp), %rdi
	addq	$32, %rsp
	testq	%rdi, %rdi
	je	.L2012
	call	_ZdlPv@PLT
.L2012:
	movq	-616(%rbp), %rbx
	movq	-624(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L2013
.L2017:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2014
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%rbx, %r12
	jne	.L2017
.L2015:
	movq	-624(%rbp), %r12
.L2013:
	testq	%r12, %r12
	je	.L2018
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L2018:
	movq	-672(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2019
	call	_ZdlPv@PLT
.L2019:
	cmpb	$0, -712(%rbp)
	je	.L2020
	movq	-704(%rbp), %rdi
	leaq	-688(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2020
	call	_ZdlPv@PLT
.L2020:
	movq	-736(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2021
	call	_ZdlPv@PLT
.L2021:
	movq	-832(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1934
	call	_ZdlPv@PLT
.L1934:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2141
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1958:
	.cfi_restore_state
	addq	$32, %r13
	cmpq	%r13, %rbx
	jne	.L1961
	jmp	.L1959
	.p2align 4,,10
	.p2align 3
.L1965:
	addq	$8, %r12
	cmpq	%r12, -920(%rbp)
	jne	.L1968
	cmpq	$0, -896(%rbp)
	jne	.L2129
.L1969:
	leaq	-320(%rbp), %r12
	leaq	-448(%rbp), %rbx
	movq	%r12, %rdi
	movq	%rbx, -920(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %r13
	xorl	%edx, %edx
	xorl	%esi, %esi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	pxor	%xmm0, %xmm0
	movw	%dx, -96(%rbp)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%r13, -448(%rbp)
	movq	%rax, -320(%rbp)
	movq	$0, -104(%rbp)
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	%rax, -904(%rbp)
	movq	-24(%r13), %rax
	movq	%rcx, -448(%rbp,%rax)
	movq	-448(%rbp), %rax
	movq	$0, -440(%rbp)
	addq	-24(%rax), %rbx
	movq	%rbx, %rdi
.LEHB290:
	.cfi_escape 0x2e,0
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE290:
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	leaq	-432(%rbp), %r15
	xorl	%esi, %esi
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -432(%rbp,%rax)
	movq	-432(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r15, %rdi
.LEHB291:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE291:
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	leaq	-368(%rbp), %rdi
	leaq	-424(%rbp), %rbx
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	leaq	64+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movq	%rcx, %xmm0
	movq	%rax, -320(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -432(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movl	$24, -360(%rbp)
	movq	%rax, -352(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
.LEHB292:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE292:
	movq	-824(%rbp), %rax
	cmpq	%rax, -832(%rbp)
	je	.L2142
	movl	$18, %edx
	leaq	.LC64(%rip), %rsi
	movq	%r15, %rdi
.LEHB293:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	40(%r14), %rax
	movq	%r15, %rdi
	movq	40(%rax), %rdx
	movq	32(%rax), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rax, %rdi
	movl	$39, %edx
	leaq	.LC68(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$25, %edx
	leaq	.LC69(%rip), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$3, %edx
	leaq	.LC70(%rip), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-912(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal6torquelsERSoRKNS1_9SignatureE@PLT
	movl	$16, %edx
	leaq	.LC71(%rip), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-824(%rbp), %rax
	movq	-832(%rbp), %r12
	movq	%rax, -904(%rbp)
	cmpq	%r12, %rax
	je	.L2143
	leaq	-864(%rbp), %rax
	movq	%rax, -896(%rbp)
	.p2align 4,,10
	.p2align 3
.L1998:
	movl	$3, %edx
	leaq	.LC70(%rip), %rsi
	movq	%r15, %rdi
	movq	(%r12), %rbx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE293:
	movq	%rbx, -800(%rbp)
	movq	208(%r14), %r13
	movq	200(%r14), %rbx
	movq	$0, -792(%rbp)
	movq	$0, -784(%rbp)
	movq	$0, -776(%rbp)
	cmpq	%r13, %rbx
	jne	.L1982
	jmp	.L1979
	.p2align 4,,10
	.p2align 3
.L2144:
	addq	$8, %rbx
	movq	%rax, (%rsi)
	addq	$8, -784(%rbp)
	cmpq	%rbx, %r13
	je	.L1979
.L1982:
	movq	(%rbx), %rdi
.LEHB294:
	call	_ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_14TypeExpressionE@PLT
	movq	%rax, -864(%rbp)
	movq	-784(%rbp), %rsi
	cmpq	-776(%rbp), %rsi
	jne	.L2144
	movq	-896(%rbp), %rdx
	leaq	-792(%rbp), %rdi
	call	_ZNSt6vectorIPKN2v88internal6torque4TypeESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
.LEHE294:
	addq	$8, %rbx
	cmpq	%rbx, %r13
	jne	.L1982
	.p2align 4,,10
	.p2align 3
.L1979:
	leaq	-592(%rbp), %r13
	leaq	-800(%rbp), %rsi
	movq	%r13, %rdi
.LEHB295:
	call	_ZN2v88internal6torque18DeclarationVisitor24MakeSpecializedSignatureERKNS1_17SpecializationKeyINS1_7GenericEEE
.LEHE295:
	movq	%r13, %rsi
	movq	%r15, %rdi
.LEHB296:
	call	_ZN2v88internal6torquelsERSoRKNS1_9SignatureE@PLT
.LEHE296:
	movq	-472(%rbp), %rax
	movq	-480(%rbp), %rbx
	cmpq	%rbx, %rax
	je	.L1986
	.p2align 4,,10
	.p2align 3
.L1990:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1987
	movq	%rax, -888(%rbp)
	addq	$32, %rbx
	call	_ZdlPv@PLT
	movq	-888(%rbp), %rax
	cmpq	%rax, %rbx
	jne	.L1990
.L1988:
	movq	-480(%rbp), %rbx
.L1986:
	testq	%rbx, %rbx
	je	.L1991
	movq	%rbx, %rdi
	call	_ZdlPv@PLT
.L1991:
	movq	-528(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1992
	call	_ZdlPv@PLT
.L1992:
	cmpb	$0, -568(%rbp)
	je	.L1993
	movq	-560(%rbp), %rdi
	leaq	-544(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1993
	call	_ZdlPv@PLT
.L1993:
	movq	-592(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1994
	call	_ZdlPv@PLT
.L1994:
	movq	-792(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1995
	call	_ZdlPv@PLT
	addq	$8, %r12
	cmpq	%r12, -904(%rbp)
	jne	.L1998
.L1997:
	movq	-384(%rbp), %rax
	leaq	-576(%rbp), %r12
	movq	$0, -584(%rbp)
	movq	%r12, -592(%rbp)
	movb	$0, -576(%rbp)
	testq	%rax, %rax
	je	.L2145
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	ja	.L2146
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r13, %rdi
.LEHB297:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.LEHE297:
.L2000:
	movq	%r13, %rdi
.LEHB298:
	call	_ZN2v88internal6torque11ReportErrorIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_
.LEHE298:
.L2014:
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L2017
	jmp	.L2015
	.p2align 4,,10
	.p2align 3
.L1987:
	addq	$32, %rbx
	cmpq	%rbx, %rax
	jne	.L1990
	jmp	.L1988
	.p2align 4,,10
	.p2align 3
.L1995:
	addq	$8, %r12
	cmpq	%r12, -904(%rbp)
	jne	.L1998
	jmp	.L1997
.L2146:
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r13, %rdi
	movq	%rax, %r8
.LEHB299:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L2000
.L2145:
	leaq	-352(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
.LEHE299:
	jmp	.L2000
.L2143:
	leaq	-592(%rbp), %r13
	jmp	.L1997
.L2141:
	call	__stack_chk_fail@PLT
	.p2align 4,,10
	.p2align 3
.L2139:
	leaq	-320(%rbp), %r12
	leaq	-448(%rbp), %rbx
	movq	%r12, %rdi
	movq	%rbx, -920(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	movq	%rax, -320(%rbp)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -904(%rbp)
	xorl	%eax, %eax
	movw	%ax, -96(%rbp)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	$0, -104(%rbp)
	movq	%rax, -448(%rbp)
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	movq	-448(%rbp), %rax
	movq	$0, -440(%rbp)
	addq	-24(%rax), %rbx
	movq	%rbx, %rdi
.LEHB300:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE300:
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	leaq	-432(%rbp), %r15
	xorl	%esi, %esi
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -432(%rbp,%rax)
	movq	-432(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r15, %rdi
.LEHB301:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE301:
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	leaq	-368(%rbp), %rdi
	leaq	-424(%rbp), %rbx
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	leaq	64+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movq	%rcx, %xmm0
	movq	%rax, -320(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -432(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movl	$24, -360(%rbp)
	movq	%rax, -352(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
.LEHB302:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE302:
	movl	$18, %edx
	leaq	.LC64(%rip), %rsi
	movq	%r15, %rdi
.LEHB303:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	40(%r14), %rax
	movq	%r15, %rdi
	movq	40(%rax), %rdx
	movq	32(%rax), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$60, %edx
	leaq	.LC65(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-896(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6torquelsERSoRKNS1_7GenericE@PLT
	movl	$5, %edx
	leaq	.LC66(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-888(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6torquelsERSoRKNS1_7GenericE@PLT
	movq	%rax, %rdi
	movl	$1, %edx
	leaq	.LC55(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE303:
	movq	-384(%rbp), %rax
	leaq	-752(%rbp), %r12
	movq	$0, -760(%rbp)
	leaq	-768(%rbp), %r14
	movq	%r12, -768(%rbp)
	movb	$0, -752(%rbp)
	testq	%rax, %rax
	je	.L1951
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	jbe	.L1952
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r14, %rdi
	movq	%rax, %r8
.LEHB304:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.LEHE304:
.L1953:
	movq	%r14, %rdi
.LEHB305:
	call	_ZN2v88internal6torque11ReportErrorIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_
.LEHE305:
.L2142:
	movl	$33, %edx
	leaq	.LC67(%rip), %rsi
	movq	%r15, %rdi
.LEHB306:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	40(%r14), %rax
	movq	%r15, %rdi
	movq	40(%rax), %rdx
	movq	32(%rax), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	-592(%rbp), %r13
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZNKSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEE3strEv@PLT
.LEHE306:
	movq	%r13, %rdi
.LEHB307:
	call	_ZN2v88internal6torque11ReportErrorIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_
.LEHE307:
	.p2align 4,,10
	.p2align 3
.L1952:
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r14, %rdi
.LEHB308:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L1953
.L1951:
	leaq	-352(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
.LEHE308:
	jmp	.L1953
.L2047:
	endbr64
	movq	%rax, %rbx
	jmp	.L2027
.L2044:
	endbr64
	movq	%rax, %rbx
	jmp	.L2025
.L2056:
	endbr64
	movq	%rax, %rbx
	jmp	.L1954
.L2043:
	endbr64
	movq	%rax, %rbx
	jmp	.L1956
.L2054:
	endbr64
	movq	%rax, %r14
	jmp	.L1949
.L2055:
	endbr64
	movq	%rax, %rbx
	jmp	.L1947
.L2053:
	endbr64
	movq	%rax, %rbx
	jmp	.L1948
.L2061:
	endbr64
	movq	%rax, %rbx
	jmp	.L2001
.L2060:
	endbr64
	movq	%rax, %rbx
	jmp	.L1983
.L2050:
	endbr64
	movq	%rax, %rbx
	jmp	.L2032
.L2042:
	endbr64
	movq	%rax, %rbx
	jmp	.L1950
.L2052:
	endbr64
	movq	%rax, %rbx
	jmp	.L2034
.L2051:
	endbr64
	movq	%rax, %rbx
	jmp	.L2034
.L2040:
	endbr64
	movq	%rax, %rbx
	jmp	.L2036
.L2045:
	endbr64
	movq	%rax, %rbx
	jmp	.L1943
.L2048:
	endbr64
	movq	%rax, %rbx
	jmp	.L2030
.L2059:
	endbr64
	movq	%rax, %r15
	jmp	.L1972
.L2062:
	endbr64
	movq	%rax, %rbx
	jmp	.L2034
.L2046:
	endbr64
	movq	%rax, %rbx
	jmp	.L1985
.L2041:
	endbr64
	movq	%rax, %rbx
	jmp	.L2034
.L2058:
	endbr64
	movq	%rax, %r15
	jmp	.L1974
.L2049:
	endbr64
	movq	%rax, %rbx
	jmp	.L2029
.L2057:
	endbr64
	movq	%rax, %r15
	jmp	.L1973
	.section	.gcc_except_table._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_25SpecializationDeclarationE,"a",@progbits
.LLSDA6548:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6548-.LLSDACSB6548
.LLSDACSB6548:
	.uleb128 .LEHB282-.LFB6548
	.uleb128 .LEHE282-.LEHB282
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB283-.LFB6548
	.uleb128 .LEHE283-.LEHB283
	.uleb128 .L2040-.LFB6548
	.uleb128 0
	.uleb128 .LEHB284-.LFB6548
	.uleb128 .LEHE284-.LEHB284
	.uleb128 .L2052-.LFB6548
	.uleb128 0
	.uleb128 .LEHB285-.LFB6548
	.uleb128 .LEHE285-.LEHB285
	.uleb128 .L2041-.LFB6548
	.uleb128 0
	.uleb128 .LEHB286-.LFB6548
	.uleb128 .LEHE286-.LEHB286
	.uleb128 .L2042-.LFB6548
	.uleb128 0
	.uleb128 .LEHB287-.LFB6548
	.uleb128 .LEHE287-.LEHB287
	.uleb128 .L2045-.LFB6548
	.uleb128 0
	.uleb128 .LEHB288-.LFB6548
	.uleb128 .LEHE288-.LEHB288
	.uleb128 .L2062-.LFB6548
	.uleb128 0
	.uleb128 .LEHB289-.LFB6548
	.uleb128 .LEHE289-.LEHB289
	.uleb128 .L2051-.LFB6548
	.uleb128 0
	.uleb128 .LEHB290-.LFB6548
	.uleb128 .LEHE290-.LEHB290
	.uleb128 .L2057-.LFB6548
	.uleb128 0
	.uleb128 .LEHB291-.LFB6548
	.uleb128 .LEHE291-.LEHB291
	.uleb128 .L2059-.LFB6548
	.uleb128 0
	.uleb128 .LEHB292-.LFB6548
	.uleb128 .LEHE292-.LEHB292
	.uleb128 .L2058-.LFB6548
	.uleb128 0
	.uleb128 .LEHB293-.LFB6548
	.uleb128 .LEHE293-.LEHB293
	.uleb128 .L2046-.LFB6548
	.uleb128 0
	.uleb128 .LEHB294-.LFB6548
	.uleb128 .LEHE294-.LEHB294
	.uleb128 .L2060-.LFB6548
	.uleb128 0
	.uleb128 .LEHB295-.LFB6548
	.uleb128 .LEHE295-.LEHB295
	.uleb128 .L2048-.LFB6548
	.uleb128 0
	.uleb128 .LEHB296-.LFB6548
	.uleb128 .LEHE296-.LEHB296
	.uleb128 .L2049-.LFB6548
	.uleb128 0
	.uleb128 .LEHB297-.LFB6548
	.uleb128 .LEHE297-.LEHB297
	.uleb128 .L2061-.LFB6548
	.uleb128 0
	.uleb128 .LEHB298-.LFB6548
	.uleb128 .LEHE298-.LEHB298
	.uleb128 .L2050-.LFB6548
	.uleb128 0
	.uleb128 .LEHB299-.LFB6548
	.uleb128 .LEHE299-.LEHB299
	.uleb128 .L2061-.LFB6548
	.uleb128 0
	.uleb128 .LEHB300-.LFB6548
	.uleb128 .LEHE300-.LEHB300
	.uleb128 .L2053-.LFB6548
	.uleb128 0
	.uleb128 .LEHB301-.LFB6548
	.uleb128 .LEHE301-.LEHB301
	.uleb128 .L2055-.LFB6548
	.uleb128 0
	.uleb128 .LEHB302-.LFB6548
	.uleb128 .LEHE302-.LEHB302
	.uleb128 .L2054-.LFB6548
	.uleb128 0
	.uleb128 .LEHB303-.LFB6548
	.uleb128 .LEHE303-.LEHB303
	.uleb128 .L2043-.LFB6548
	.uleb128 0
	.uleb128 .LEHB304-.LFB6548
	.uleb128 .LEHE304-.LEHB304
	.uleb128 .L2056-.LFB6548
	.uleb128 0
	.uleb128 .LEHB305-.LFB6548
	.uleb128 .LEHE305-.LEHB305
	.uleb128 .L2044-.LFB6548
	.uleb128 0
	.uleb128 .LEHB306-.LFB6548
	.uleb128 .LEHE306-.LEHB306
	.uleb128 .L2046-.LFB6548
	.uleb128 0
	.uleb128 .LEHB307-.LFB6548
	.uleb128 .LEHE307-.LEHB307
	.uleb128 .L2047-.LFB6548
	.uleb128 0
	.uleb128 .LEHB308-.LFB6548
	.uleb128 .LEHE308-.LEHB308
	.uleb128 .L2056-.LFB6548
	.uleb128 0
.LLSDACSE6548:
	.section	.text._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_25SpecializationDeclarationE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_25SpecializationDeclarationE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6548
	.type	_ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_25SpecializationDeclarationE.cold, @function
_ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_25SpecializationDeclarationE.cold:
.LFSB6548:
.L2027:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-592(%rbp), %rdi
	leaq	-576(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1985
	call	_ZdlPv@PLT
.L1985:
	movq	-920(%rbp), %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	jmp	.L1943
.L2025:
	movq	-768(%rbp), %rdi
	cmpq	%r12, %rdi
	jne	.L2147
.L1956:
	movq	-920(%rbp), %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
.L1950:
	movq	%r13, %rdi
	call	_ZN2v88internal6torque9SignatureD1Ev
.L1943:
	movq	-912(%rbp), %rdi
	call	_ZN2v88internal6torque9SignatureD1Ev
.L2036:
	movq	-832(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2037
	call	_ZdlPv@PLT
.L2037:
	movq	%rbx, %rdi
.LEHB309:
	call	_Unwind_Resume@PLT
.LEHE309:
.L1954:
	movq	-768(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L1956
	call	_ZdlPv@PLT
	jmp	.L1956
.L1947:
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	movq	$0, -440(%rbp)
.L1948:
	movq	-904(%rbp), %rax
	movq	%r12, %rdi
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	jmp	.L1950
.L2147:
	call	_ZdlPv@PLT
	jmp	.L1956
.L1949:
	movq	%rbx, %rdi
	movq	%r14, %rbx
	call	_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED1Ev
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -432(%rbp,%rax)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	movq	$0, -440(%rbp)
	jmp	.L1948
.L2001:
	movq	-592(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L1985
	call	_ZdlPv@PLT
	jmp	.L1985
.L1983:
	movq	-792(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1985
	call	_ZdlPv@PLT
	jmp	.L1985
.L2032:
	movq	-592(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L1985
	call	_ZdlPv@PLT
	jmp	.L1985
.L2034:
	movq	-792(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1943
	call	_ZdlPv@PLT
	jmp	.L1943
.L2029:
	movq	%r13, %rdi
	call	_ZN2v88internal6torque9SignatureD1Ev
.L2030:
	movq	-792(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1985
	call	_ZdlPv@PLT
	jmp	.L1985
.L1974:
	movq	%rbx, %rdi
	call	_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED1Ev
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -432(%rbp,%rax)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%r13, -448(%rbp)
	movq	-24(%r13), %rax
	movq	%rcx, -448(%rbp,%rax)
	movq	$0, -440(%rbp)
.L1973:
	movq	-904(%rbp), %rax
	movq	%r12, %rdi
	movq	%r15, %rbx
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	jmp	.L1943
.L1972:
	movq	%r13, -448(%rbp)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	-24(%r13), %rax
	movq	%rcx, -448(%rbp,%rax)
	movq	$0, -440(%rbp)
	jmp	.L1973
	.cfi_endproc
.LFE6548:
	.section	.gcc_except_table._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_25SpecializationDeclarationE
.LLSDAC6548:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6548-.LLSDACSBC6548
.LLSDACSBC6548:
	.uleb128 .LEHB309-.LCOLDB72
	.uleb128 .LEHE309-.LEHB309
	.uleb128 0
	.uleb128 0
.LLSDACSEC6548:
	.section	.text.unlikely._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_25SpecializationDeclarationE
	.section	.text._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_25SpecializationDeclarationE
	.size	_ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_25SpecializationDeclarationE, .-_ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_25SpecializationDeclarationE
	.section	.text.unlikely._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_25SpecializationDeclarationE
	.size	_ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_25SpecializationDeclarationE.cold, .-_ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_25SpecializationDeclarationE.cold
.LCOLDE72:
	.section	.text._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_25SpecializationDeclarationE
.LHOTE72:
	.section	.text.unlikely._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_11DeclarationE,"ax",@progbits
	.align 2
.LCOLDB73:
	.section	.text._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_11DeclarationE,"ax",@progbits
.LHOTB73:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_11DeclarationE
	.type	_ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_11DeclarationE, @function
_ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_11DeclarationE:
.LFB6539:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6539
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$216, %rsp
	.cfi_offset 3, -40
	movdqu	12(%rdi), %xmm0
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	28(%rdi), %eax
	movaps	%xmm0, -224(%rbp)
	movl	%eax, -208(%rbp)
.LEHB310:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rax
	movq	%rax, -200(%rbp)
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
.LEHE310:
	leaq	-224(%rbp), %rdx
	movq	%rdx, (%rax)
	movl	8(%r12), %eax
	subl	$36, %eax
	cmpl	$15, %eax
	ja	.L2149
	leaq	.L2151(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_11DeclarationE,"a",@progbits
	.align 4
	.align 4
.L2151:
	.long	.L2203-.L2151
	.long	.L2203-.L2151
	.long	.L2203-.L2151
	.long	.L2163-.L2151
	.long	.L2162-.L2151
	.long	.L2161-.L2151
	.long	.L2160-.L2151
	.long	.L2159-.L2151
	.long	.L2158-.L2151
	.long	.L2157-.L2151
	.long	.L2156-.L2151
	.long	.L2155-.L2151
	.long	.L2154-.L2151
	.long	.L2153-.L2151
	.long	.L2152-.L2151
	.long	.L2150-.L2151
	.section	.text._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_11DeclarationE
	.p2align 4,,10
	.p2align 3
.L2158:
	movq	40(%r12), %rdi
	movq	48(%r12), %r13
.LEHB311:
	call	_ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_14TypeExpressionE@PLT
	movq	32(%r12), %rdi
	movq	%rax, %rsi
	movq	%r13, %rdx
	call	_ZN2v88internal6torque12Declarations24DeclareNamespaceConstantEPNS1_10IdentifierEPKNS1_4TypeEPNS1_10ExpressionE@PLT
.LEHE311:
	.p2align 4,,10
	.p2align 3
.L2162:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
	movq	-200(%rbp), %rdx
	movq	%rdx, (%rax)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2204
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2163:
	.cfi_restore_state
	movq	88(%r12), %rax
	cmpq	%rax, 96(%r12)
	jne	.L2162
	.p2align 4,,10
	.p2align 3
.L2203:
	movq	32(%r12), %rdi
.LEHB312:
	call	_ZN2v88internal6torque12Declarations10LookupTypeEPKNS1_10IdentifierE@PLT
	jmp	.L2162
	.p2align 4,,10
	.p2align 3
.L2152:
	movq	%r12, %rdi
	call	_ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_26ExternalRuntimeDeclarationE
	jmp	.L2162
	.p2align 4,,10
	.p2align 3
.L2153:
	movq	%r12, %rdi
	call	_ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_26ExternalBuiltinDeclarationE
	jmp	.L2162
	.p2align 4,,10
	.p2align 3
.L2161:
	movq	%r12, %rdi
	call	_ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_25SpecializationDeclarationE
	jmp	.L2162
	.p2align 4,,10
	.p2align 3
.L2160:
	movq	%r12, %rdi
	call	_ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_22ExternConstDeclarationE
	jmp	.L2162
	.p2align 4,,10
	.p2align 3
.L2159:
	leaq	56(%r12), %rdi
	call	_ZN2v88internal6torque20GetOrCreateNamespaceERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	%rax, -240(%rbp)
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rax
	movq	%rax, -232(%rbp)
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
.LEHE312:
	leaq	-240(%rbp), %rdx
	movq	%rdx, (%rax)
	movq	40(%r12), %rbx
	movq	32(%r12), %r12
	cmpq	%rbx, %r12
	je	.L2170
	.p2align 4,,10
	.p2align 3
.L2169:
	movq	(%r12), %rdi
.LEHB313:
	call	_ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_11DeclarationE
.LEHE313:
	addq	$8, %r12
	cmpq	%r12, %rbx
	jne	.L2169
.L2170:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
	movq	-232(%rbp), %rdx
	movq	%rdx, (%rax)
	jmp	.L2162
	.p2align 4,,10
	.p2align 3
.L2156:
	movq	%r12, %rdi
.LEHB314:
	call	_ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_22TorqueMacroDeclarationE
	jmp	.L2162
	.p2align 4,,10
	.p2align 3
.L2155:
	movq	%r12, %rdi
	call	_ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_24TorqueBuiltinDeclarationE
	jmp	.L2162
	.p2align 4,,10
	.p2align 3
.L2154:
	movq	%r12, %rdi
	call	_ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_24ExternalMacroDeclarationE
	jmp	.L2162
	.p2align 4,,10
	.p2align 3
.L2150:
	leaq	-192(%rbp), %r13
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal6torque11TypeVisitor13MakeSignatureEPKNS1_19CallableDeclarationE@PLT
.LEHE314:
	movq	40(%r12), %rdi
	movq	%r13, %rsi
	addq	$32, %rdi
.LEHB315:
	call	_ZN2v88internal6torque12Declarations16DeclareIntrinsicERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKNS1_9SignatureE@PLT
.LEHE315:
	movq	-72(%rbp), %rbx
	movq	-80(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L2173
	.p2align 4,,10
	.p2align 3
.L2177:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2174
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L2177
.L2175:
	movq	-80(%rbp), %r12
.L2173:
	testq	%r12, %r12
	je	.L2178
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L2178:
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2179
	call	_ZdlPv@PLT
.L2179:
	cmpb	$0, -168(%rbp)
	je	.L2180
	movq	-160(%rbp), %rdi
	leaq	-144(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2180
	call	_ZdlPv@PLT
.L2180:
	movq	-192(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2162
	call	_ZdlPv@PLT
	jmp	.L2162
	.p2align 4,,10
	.p2align 3
.L2157:
	movq	%r12, %rdi
.LEHB316:
	call	_ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_21CppIncludeDeclarationE
	jmp	.L2162
	.p2align 4,,10
	.p2align 3
.L2174:
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L2177
	jmp	.L2175
.L2149:
	leaq	.LC59(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.LEHE316:
.L2204:
	call	__stack_chk_fail@PLT
.L2186:
	endbr64
	movq	%rax, %r12
	jmp	.L2182
.L2185:
	endbr64
	movq	%rax, %r12
	jmp	.L2171
.L2184:
	endbr64
	movq	%rax, %r12
	jmp	.L2172
	.section	.gcc_except_table._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_11DeclarationE,"a",@progbits
.LLSDA6539:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6539-.LLSDACSB6539
.LLSDACSB6539:
	.uleb128 .LEHB310-.LFB6539
	.uleb128 .LEHE310-.LEHB310
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB311-.LFB6539
	.uleb128 .LEHE311-.LEHB311
	.uleb128 .L2184-.LFB6539
	.uleb128 0
	.uleb128 .LEHB312-.LFB6539
	.uleb128 .LEHE312-.LEHB312
	.uleb128 .L2184-.LFB6539
	.uleb128 0
	.uleb128 .LEHB313-.LFB6539
	.uleb128 .LEHE313-.LEHB313
	.uleb128 .L2185-.LFB6539
	.uleb128 0
	.uleb128 .LEHB314-.LFB6539
	.uleb128 .LEHE314-.LEHB314
	.uleb128 .L2184-.LFB6539
	.uleb128 0
	.uleb128 .LEHB315-.LFB6539
	.uleb128 .LEHE315-.LEHB315
	.uleb128 .L2186-.LFB6539
	.uleb128 0
	.uleb128 .LEHB316-.LFB6539
	.uleb128 .LEHE316-.LEHB316
	.uleb128 .L2184-.LFB6539
	.uleb128 0
.LLSDACSE6539:
	.section	.text._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_11DeclarationE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_11DeclarationE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6539
	.type	_ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_11DeclarationE.cold, @function
_ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_11DeclarationE.cold:
.LFSB6539:
.L2182:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movq	%r13, %rdi
	call	_ZN2v88internal6torque9SignatureD1Ev
.L2172:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
	movq	-200(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rdx, (%rax)
.LEHB317:
	call	_Unwind_Resume@PLT
.LEHE317:
.L2171:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
	movq	-232(%rbp), %rdx
	movq	%rdx, (%rax)
	jmp	.L2172
	.cfi_endproc
.LFE6539:
	.section	.gcc_except_table._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_11DeclarationE
.LLSDAC6539:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6539-.LLSDACSBC6539
.LLSDACSBC6539:
	.uleb128 .LEHB317-.LCOLDB73
	.uleb128 .LEHE317-.LEHB317
	.uleb128 0
	.uleb128 0
.LLSDACSEC6539:
	.section	.text.unlikely._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_11DeclarationE
	.section	.text._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_11DeclarationE
	.size	_ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_11DeclarationE, .-_ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_11DeclarationE
	.section	.text.unlikely._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_11DeclarationE
	.size	_ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_11DeclarationE.cold, .-_ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_11DeclarationE.cold
.LCOLDE73:
	.section	.text._ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_11DeclarationE
.LHOTE73:
	.section	.rodata._ZN2v88internal6torque18DeclarationVisitor18SpecializeImplicitERKNS1_17SpecializationKeyINS1_7GenericEEE.str1.1,"aMS",@progbits,1
.LC74:
	.string	"> declared at "
.LC75:
	.string	"missing specialization of "
	.section	.text.unlikely._ZN2v88internal6torque18DeclarationVisitor18SpecializeImplicitERKNS1_17SpecializationKeyINS1_7GenericEEE,"ax",@progbits
	.align 2
.LCOLDB76:
	.section	.text._ZN2v88internal6torque18DeclarationVisitor18SpecializeImplicitERKNS1_17SpecializationKeyINS1_7GenericEEE,"ax",@progbits
.LHOTB76:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque18DeclarationVisitor18SpecializeImplicitERKNS1_17SpecializationKeyINS1_7GenericEEE
	.type	_ZN2v88internal6torque18DeclarationVisitor18SpecializeImplicitERKNS1_17SpecializationKeyINS1_7GenericEEE, @function
_ZN2v88internal6torque18DeclarationVisitor18SpecializeImplicitERKNS1_17SpecializationKeyINS1_7GenericEEE:
.LFB6565:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6565
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -40
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
.LEHB318:
	call	_ZN2v88internal6torque7Generic12CallableBodyEv@PLT
	movq	(%r12), %rsi
	movq	%rdx, %r13
	movl	%eax, %ebx
	testb	%al, %al
	jne	.L2207
	movq	104(%rsi), %rax
	movq	56(%rax), %rax
	testq	%rax, %rax
	je	.L2208
	cmpl	$51, 8(%rax)
	jne	.L2208
.L2207:
	movq	16(%rsi), %rax
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rax
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
.LEHE318:
	leaq	-80(%rbp), %rdx
	movq	%rdx, (%rax)
.LEHB319:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rax
	movq	(%r12), %rdx
	subq	$32, %rsp
	movl	%ebx, %r8d
	movq	%r13, %r9
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movq	104(%rdx), %rdx
	movdqu	(%rax), %xmm0
	movq	56(%rdx), %rsi
	movups	%xmm0, (%rsp)
	movl	16(%rax), %eax
	xorl	%edx, %edx
	movl	%eax, 16(%rsp)
	.cfi_escape 0x2e,0x20
	call	_ZN2v88internal6torque18DeclarationVisitor10SpecializeERKNS1_17SpecializationKeyINS1_7GenericEEEPNS1_19CallableDeclarationENS_4base8OptionalIPKNS1_25SpecializationDeclarationEEENSB_IPNS1_9StatementEEENS1_14SourcePositionE
	movb	$0, 64(%rax)
	addq	$32, %rsp
	movq	%rax, %r13
	movq	%rax, -64(%rbp)
	.cfi_escape 0x2e,0
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rax
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
.LEHE319:
	leaq	-64(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rdx, (%rax)
.LEHB320:
	call	_ZN2v88internal6torque18DeclarationVisitor23DeclareSpecializedTypesERKNS1_17SpecializationKeyINS1_7GenericEEE
.LEHE320:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
	movq	-56(%rbp), %rdx
	movq	%rdx, (%rax)
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
	movq	-72(%rbp), %rdx
	movq	%rdx, (%rax)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2221
	leaq	-24(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2208:
	.cfi_restore_state
	movdqu	24(%rsi), %xmm1
	leaq	8(%r12), %rcx
	addq	$72, %rsi
	leaq	-64(%rbp), %r9
	leaq	.LC74(%rip), %r8
	leaq	.LC60(%rip), %rdx
	movaps	%xmm1, -64(%rbp)
	movl	-32(%rsi), %eax
	leaq	.LC75(%rip), %rdi
	movl	%eax, -48(%rbp)
.LEHB321:
	call	_ZN2v88internal6torque11ReportErrorIJRA27_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA14_S3_RKSt6vectorIPKNS1_4TypeESaISJ_EERA15_S3_NS1_14SourcePositionEEEEvDpOT_
.LEHE321:
.L2221:
	call	__stack_chk_fail@PLT
.L2214:
	endbr64
	movq	%rax, %r12
	jmp	.L2210
.L2213:
	endbr64
	movq	%rax, %r12
	jmp	.L2211
	.section	.gcc_except_table._ZN2v88internal6torque18DeclarationVisitor18SpecializeImplicitERKNS1_17SpecializationKeyINS1_7GenericEEE,"a",@progbits
.LLSDA6565:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6565-.LLSDACSB6565
.LLSDACSB6565:
	.uleb128 .LEHB318-.LFB6565
	.uleb128 .LEHE318-.LEHB318
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB319-.LFB6565
	.uleb128 .LEHE319-.LEHB319
	.uleb128 .L2213-.LFB6565
	.uleb128 0
	.uleb128 .LEHB320-.LFB6565
	.uleb128 .LEHE320-.LEHB320
	.uleb128 .L2214-.LFB6565
	.uleb128 0
	.uleb128 .LEHB321-.LFB6565
	.uleb128 .LEHE321-.LEHB321
	.uleb128 0
	.uleb128 0
.LLSDACSE6565:
	.section	.text._ZN2v88internal6torque18DeclarationVisitor18SpecializeImplicitERKNS1_17SpecializationKeyINS1_7GenericEEE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque18DeclarationVisitor18SpecializeImplicitERKNS1_17SpecializationKeyINS1_7GenericEEE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6565
	.type	_ZN2v88internal6torque18DeclarationVisitor18SpecializeImplicitERKNS1_17SpecializationKeyINS1_7GenericEEE.cold, @function
_ZN2v88internal6torque18DeclarationVisitor18SpecializeImplicitERKNS1_17SpecializationKeyINS1_7GenericEEE.cold:
.LFSB6565:
.L2210:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
	movq	-56(%rbp), %rdx
	movq	%rdx, (%rax)
.L2211:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
	movq	-72(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rdx, (%rax)
.LEHB322:
	call	_Unwind_Resume@PLT
.LEHE322:
	.cfi_endproc
.LFE6565:
	.section	.gcc_except_table._ZN2v88internal6torque18DeclarationVisitor18SpecializeImplicitERKNS1_17SpecializationKeyINS1_7GenericEEE
.LLSDAC6565:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6565-.LLSDACSBC6565
.LLSDACSBC6565:
	.uleb128 .LEHB322-.LCOLDB76
	.uleb128 .LEHE322-.LEHB322
	.uleb128 0
	.uleb128 0
.LLSDACSEC6565:
	.section	.text.unlikely._ZN2v88internal6torque18DeclarationVisitor18SpecializeImplicitERKNS1_17SpecializationKeyINS1_7GenericEEE
	.section	.text._ZN2v88internal6torque18DeclarationVisitor18SpecializeImplicitERKNS1_17SpecializationKeyINS1_7GenericEEE
	.size	_ZN2v88internal6torque18DeclarationVisitor18SpecializeImplicitERKNS1_17SpecializationKeyINS1_7GenericEEE, .-_ZN2v88internal6torque18DeclarationVisitor18SpecializeImplicitERKNS1_17SpecializationKeyINS1_7GenericEEE
	.section	.text.unlikely._ZN2v88internal6torque18DeclarationVisitor18SpecializeImplicitERKNS1_17SpecializationKeyINS1_7GenericEEE
	.size	_ZN2v88internal6torque18DeclarationVisitor18SpecializeImplicitERKNS1_17SpecializationKeyINS1_7GenericEEE.cold, .-_ZN2v88internal6torque18DeclarationVisitor18SpecializeImplicitERKNS1_17SpecializationKeyINS1_7GenericEEE.cold
.LCOLDE76:
	.section	.text._ZN2v88internal6torque18DeclarationVisitor18SpecializeImplicitERKNS1_17SpecializationKeyINS1_7GenericEEE
.LHOTE76:
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal6torque20GetOrCreateNamespaceERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal6torque20GetOrCreateNamespaceERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_GLOBAL__sub_I__ZN2v88internal6torque20GetOrCreateNamespaceERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB12253:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE12253:
	.size	_GLOBAL__sub_I__ZN2v88internal6torque20GetOrCreateNamespaceERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_GLOBAL__sub_I__ZN2v88internal6torque20GetOrCreateNamespaceERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal6torque20GetOrCreateNamespaceERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.weak	_ZTVN2v88internal6torque10DeclarableE
	.section	.data.rel.ro.local._ZTVN2v88internal6torque10DeclarableE,"awG",@progbits,_ZTVN2v88internal6torque10DeclarableE,comdat
	.align 8
	.type	_ZTVN2v88internal6torque10DeclarableE, @object
	.size	_ZTVN2v88internal6torque10DeclarableE, 40
_ZTVN2v88internal6torque10DeclarableE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal6torque10DeclarableD1Ev
	.quad	_ZN2v88internal6torque10DeclarableD0Ev
	.quad	_ZNK2v88internal6torque10Declarable9type_nameEv
	.weak	_ZTVN2v88internal6torque5ScopeE
	.section	.data.rel.ro.local._ZTVN2v88internal6torque5ScopeE,"awG",@progbits,_ZTVN2v88internal6torque5ScopeE,comdat
	.align 8
	.type	_ZTVN2v88internal6torque5ScopeE, @object
	.size	_ZTVN2v88internal6torque5ScopeE, 40
_ZTVN2v88internal6torque5ScopeE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal6torque5ScopeD1Ev
	.quad	_ZN2v88internal6torque5ScopeD0Ev
	.quad	_ZNK2v88internal6torque5Scope9type_nameEv
	.weak	_ZTVN2v88internal6torque9NamespaceE
	.section	.data.rel.ro.local._ZTVN2v88internal6torque9NamespaceE,"awG",@progbits,_ZTVN2v88internal6torque9NamespaceE,comdat
	.align 8
	.type	_ZTVN2v88internal6torque9NamespaceE, @object
	.size	_ZTVN2v88internal6torque9NamespaceE, 40
_ZTVN2v88internal6torque9NamespaceE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal6torque9NamespaceD1Ev
	.quad	_ZN2v88internal6torque9NamespaceD0Ev
	.quad	_ZNK2v88internal6torque9Namespace9type_nameEv
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.data.rel.ro,"aw"
	.align 8
.LC27:
	.quad	_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE+64
	.align 8
.LC28:
	.quad	_ZTVSt15basic_streambufIcSt11char_traitsIcEE+16
	.align 8
.LC62:
	.quad	_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE+16
	.hidden	DW.ref.__gxx_personality_v0
	.weak	DW.ref.__gxx_personality_v0
	.section	.data.rel.local.DW.ref.__gxx_personality_v0,"awG",@progbits,DW.ref.__gxx_personality_v0,comdat
	.align 8
	.type	DW.ref.__gxx_personality_v0, @object
	.size	DW.ref.__gxx_personality_v0, 8
DW.ref.__gxx_personality_v0:
	.quad	__gxx_personality_v0
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
