	.file	"types.cc"
	.text
	.section	.text._ZNK2v88internal6torque4Type11IsConstexprEv,"axG",@progbits,_ZNK2v88internal6torque4Type11IsConstexprEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal6torque4Type11IsConstexprEv
	.type	_ZNK2v88internal6torque4Type11IsConstexprEv, @function
_ZNK2v88internal6torque4Type11IsConstexprEv:
.LFB5567:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE5567:
	.size	_ZNK2v88internal6torque4Type11IsConstexprEv, .-_ZNK2v88internal6torque4Type11IsConstexprEv
	.section	.text._ZNK2v88internal6torque4Type11IsTransientEv,"axG",@progbits,_ZNK2v88internal6torque4Type11IsTransientEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal6torque4Type11IsTransientEv
	.type	_ZNK2v88internal6torque4Type11IsTransientEv, @function
_ZNK2v88internal6torque4Type11IsTransientEv:
.LFB5568:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE5568:
	.size	_ZNK2v88internal6torque4Type11IsTransientEv, .-_ZNK2v88internal6torque4Type11IsTransientEv
	.section	.text._ZNK2v88internal6torque4Type19NonConstexprVersionEv,"axG",@progbits,_ZNK2v88internal6torque4Type19NonConstexprVersionEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal6torque4Type19NonConstexprVersionEv
	.type	_ZNK2v88internal6torque4Type19NonConstexprVersionEv, @function
_ZNK2v88internal6torque4Type19NonConstexprVersionEv:
.LFB5569:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	ret
	.cfi_endproc
.LFE5569:
	.size	_ZNK2v88internal6torque4Type19NonConstexprVersionEv, .-_ZNK2v88internal6torque4Type19NonConstexprVersionEv
	.section	.text._ZNK2v88internal6torque4Type16ConstexprVersionEv,"axG",@progbits,_ZNK2v88internal6torque4Type16ConstexprVersionEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal6torque4Type16ConstexprVersionEv
	.type	_ZNK2v88internal6torque4Type16ConstexprVersionEv, @function
_ZNK2v88internal6torque4Type16ConstexprVersionEv:
.LFB5570:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE5570:
	.size	_ZNK2v88internal6torque4Type16ConstexprVersionEv, .-_ZNK2v88internal6torque4Type16ConstexprVersionEv
	.section	.text._ZNK2v88internal6torque4Type15GetRuntimeTypesB5cxx11Ev,"axG",@progbits,_ZNK2v88internal6torque4Type15GetRuntimeTypesB5cxx11Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal6torque4Type15GetRuntimeTypesB5cxx11Ev
	.type	_ZNK2v88internal6torque4Type15GetRuntimeTypesB5cxx11Ev, @function
_ZNK2v88internal6torque4Type15GetRuntimeTypesB5cxx11Ev:
.LFB5571:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movq	$0, 16(%rdi)
	movq	%rdi, %rax
	movups	%xmm0, (%rdi)
	ret
	.cfi_endproc
.LFE5571:
	.size	_ZNK2v88internal6torque4Type15GetRuntimeTypesB5cxx11Ev, .-_ZNK2v88internal6torque4Type15GetRuntimeTypesB5cxx11Ev
	.section	.text._ZNK2v88internal6torque12AbstractType11IsConstexprEv,"axG",@progbits,_ZNK2v88internal6torque12AbstractType11IsConstexprEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal6torque12AbstractType11IsConstexprEv
	.type	_ZNK2v88internal6torque12AbstractType11IsConstexprEv, @function
_ZNK2v88internal6torque12AbstractType11IsConstexprEv:
.LFB5620:
	.cfi_startproc
	endbr64
	cmpq	$0, 144(%rdi)
	setne	%al
	ret
	.cfi_endproc
.LFE5620:
	.size	_ZNK2v88internal6torque12AbstractType11IsConstexprEv, .-_ZNK2v88internal6torque12AbstractType11IsConstexprEv
	.section	.text._ZNK2v88internal6torque12AbstractType19NonConstexprVersionEv,"axG",@progbits,_ZNK2v88internal6torque12AbstractType19NonConstexprVersionEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal6torque12AbstractType19NonConstexprVersionEv
	.type	_ZNK2v88internal6torque12AbstractType19NonConstexprVersionEv, @function
_ZNK2v88internal6torque12AbstractType19NonConstexprVersionEv:
.LFB5621:
	.cfi_startproc
	endbr64
	movq	144(%rdi), %rax
	testq	%rax, %rax
	cmove	%rdi, %rax
	ret
	.cfi_endproc
.LFE5621:
	.size	_ZNK2v88internal6torque12AbstractType19NonConstexprVersionEv, .-_ZNK2v88internal6torque12AbstractType19NonConstexprVersionEv
	.section	.text._ZNK2v88internal6torque12AbstractType16ConstexprVersionEv,"axG",@progbits,_ZNK2v88internal6torque12AbstractType16ConstexprVersionEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal6torque12AbstractType16ConstexprVersionEv
	.type	_ZNK2v88internal6torque12AbstractType16ConstexprVersionEv, @function
_ZNK2v88internal6torque12AbstractType16ConstexprVersionEv:
.LFB5622:
	.cfi_startproc
	endbr64
	movq	152(%rdi), %rax
	testq	%rax, %rax
	je	.L14
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	movq	144(%rdi), %rax
	testq	%rax, %rax
	cmovne	%rdi, %rax
	ret
	.cfi_endproc
.LFE5622:
	.size	_ZNK2v88internal6torque12AbstractType16ConstexprVersionEv, .-_ZNK2v88internal6torque12AbstractType16ConstexprVersionEv
	.section	.text._ZNK2v88internal6torque12AbstractType11IsTransientEv,"axG",@progbits,_ZNK2v88internal6torque12AbstractType11IsTransientEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal6torque12AbstractType11IsTransientEv
	.type	_ZNK2v88internal6torque12AbstractType11IsTransientEv, @function
_ZNK2v88internal6torque12AbstractType11IsTransientEv:
.LFB5628:
	.cfi_startproc
	endbr64
	movzbl	72(%rdi), %eax
	ret
	.cfi_endproc
.LFE5628:
	.size	_ZNK2v88internal6torque12AbstractType11IsTransientEv, .-_ZNK2v88internal6torque12AbstractType11IsTransientEv
	.section	.text._ZNK2v88internal6torque13AggregateType15HasIndexedFieldEv,"axG",@progbits,_ZNK2v88internal6torque13AggregateType15HasIndexedFieldEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal6torque13AggregateType15HasIndexedFieldEv
	.type	_ZNK2v88internal6torque13AggregateType15HasIndexedFieldEv, @function
_ZNK2v88internal6torque13AggregateType15HasIndexedFieldEv:
.LFB5715:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE5715:
	.size	_ZNK2v88internal6torque13AggregateType15HasIndexedFieldEv, .-_ZNK2v88internal6torque13AggregateType15HasIndexedFieldEv
	.section	.text._ZNK2v88internal6torque9ClassType11IsTransientEv,"axG",@progbits,_ZNK2v88internal6torque9ClassType11IsTransientEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal6torque9ClassType11IsTransientEv
	.type	_ZNK2v88internal6torque9ClassType11IsTransientEv, @function
_ZNK2v88internal6torque9ClassType11IsTransientEv:
.LFB5790:
	.cfi_startproc
	endbr64
	movl	176(%rdi), %eax
	shrl	$3, %eax
	andl	$1, %eax
	ret
	.cfi_endproc
.LFE5790:
	.size	_ZNK2v88internal6torque9ClassType11IsTransientEv, .-_ZNK2v88internal6torque9ClassType11IsTransientEv
	.section	.text._ZN2v88internal6torque21FieldAccessExpressionD2Ev,"axG",@progbits,_ZN2v88internal6torque21FieldAccessExpressionD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque21FieldAccessExpressionD2Ev
	.type	_ZN2v88internal6torque21FieldAccessExpressionD2Ev, @function
_ZN2v88internal6torque21FieldAccessExpressionD2Ev:
.LFB10064:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE10064:
	.size	_ZN2v88internal6torque21FieldAccessExpressionD2Ev, .-_ZN2v88internal6torque21FieldAccessExpressionD2Ev
	.weak	_ZN2v88internal6torque21FieldAccessExpressionD1Ev
	.set	_ZN2v88internal6torque21FieldAccessExpressionD1Ev,_ZN2v88internal6torque21FieldAccessExpressionD2Ev
	.section	.text._ZN2v88internal6torque15ReturnStatementD2Ev,"axG",@progbits,_ZN2v88internal6torque15ReturnStatementD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque15ReturnStatementD2Ev
	.type	_ZN2v88internal6torque15ReturnStatementD2Ev, @function
_ZN2v88internal6torque15ReturnStatementD2Ev:
.LFB10088:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE10088:
	.size	_ZN2v88internal6torque15ReturnStatementD2Ev, .-_ZN2v88internal6torque15ReturnStatementD2Ev
	.weak	_ZN2v88internal6torque15ReturnStatementD1Ev
	.set	_ZN2v88internal6torque15ReturnStatementD1Ev,_ZN2v88internal6torque15ReturnStatementD2Ev
	.section	.text._ZN2v88internal6torque19ExpressionStatementD2Ev,"axG",@progbits,_ZN2v88internal6torque19ExpressionStatementD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque19ExpressionStatementD2Ev
	.type	_ZN2v88internal6torque19ExpressionStatementD2Ev, @function
_ZN2v88internal6torque19ExpressionStatementD2Ev:
.LFB10136:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE10136:
	.size	_ZN2v88internal6torque19ExpressionStatementD2Ev, .-_ZN2v88internal6torque19ExpressionStatementD2Ev
	.weak	_ZN2v88internal6torque19ExpressionStatementD1Ev
	.set	_ZN2v88internal6torque19ExpressionStatementD1Ev,_ZN2v88internal6torque19ExpressionStatementD2Ev
	.section	.text._ZN2v88internal6torque10IdentifierD2Ev,"axG",@progbits,_ZN2v88internal6torque10IdentifierD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque10IdentifierD2Ev
	.type	_ZN2v88internal6torque10IdentifierD2Ev, @function
_ZN2v88internal6torque10IdentifierD2Ev:
.LFB10004:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %r8
	leaq	16+_ZTVN2v88internal6torque10IdentifierE(%rip), %rax
	addq	$48, %rdi
	movq	%rax, -48(%rdi)
	cmpq	%rdi, %r8
	je	.L21
	movq	%r8, %rdi
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L21:
	ret
	.cfi_endproc
.LFE10004:
	.size	_ZN2v88internal6torque10IdentifierD2Ev, .-_ZN2v88internal6torque10IdentifierD2Ev
	.weak	_ZN2v88internal6torque10IdentifierD1Ev
	.set	_ZN2v88internal6torque10IdentifierD1Ev,_ZN2v88internal6torque10IdentifierD2Ev
	.section	.text._ZN2v88internal6torque20AssignmentExpressionD2Ev,"axG",@progbits,_ZN2v88internal6torque20AssignmentExpressionD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque20AssignmentExpressionD2Ev
	.type	_ZN2v88internal6torque20AssignmentExpressionD2Ev, @function
_ZN2v88internal6torque20AssignmentExpressionD2Ev:
.LFB10115:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal6torque20AssignmentExpressionE(%rip), %rax
	cmpb	$0, 40(%rdi)
	movq	%rax, (%rdi)
	je	.L23
	movq	48(%rdi), %r8
	addq	$64, %rdi
	cmpq	%rdi, %r8
	je	.L23
	movq	%r8, %rdi
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L23:
	ret
	.cfi_endproc
.LFE10115:
	.size	_ZN2v88internal6torque20AssignmentExpressionD2Ev, .-_ZN2v88internal6torque20AssignmentExpressionD2Ev
	.weak	_ZN2v88internal6torque20AssignmentExpressionD1Ev
	.set	_ZN2v88internal6torque20AssignmentExpressionD1Ev,_ZN2v88internal6torque20AssignmentExpressionD2Ev
	.section	.text._ZN2v88internal6torque15ReturnStatementD0Ev,"axG",@progbits,_ZN2v88internal6torque15ReturnStatementD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque15ReturnStatementD0Ev
	.type	_ZN2v88internal6torque15ReturnStatementD0Ev, @function
_ZN2v88internal6torque15ReturnStatementD0Ev:
.LFB10090:
	.cfi_startproc
	endbr64
	movl	$48, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE10090:
	.size	_ZN2v88internal6torque15ReturnStatementD0Ev, .-_ZN2v88internal6torque15ReturnStatementD0Ev
	.section	.text._ZN2v88internal6torque19ExpressionStatementD0Ev,"axG",@progbits,_ZN2v88internal6torque19ExpressionStatementD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque19ExpressionStatementD0Ev
	.type	_ZN2v88internal6torque19ExpressionStatementD0Ev, @function
_ZN2v88internal6torque19ExpressionStatementD0Ev:
.LFB10138:
	.cfi_startproc
	endbr64
	movl	$40, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE10138:
	.size	_ZN2v88internal6torque19ExpressionStatementD0Ev, .-_ZN2v88internal6torque19ExpressionStatementD0Ev
	.section	.text._ZN2v88internal6torque21FieldAccessExpressionD0Ev,"axG",@progbits,_ZN2v88internal6torque21FieldAccessExpressionD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque21FieldAccessExpressionD0Ev
	.type	_ZN2v88internal6torque21FieldAccessExpressionD0Ev, @function
_ZN2v88internal6torque21FieldAccessExpressionD0Ev:
.LFB10066:
	.cfi_startproc
	endbr64
	movl	$48, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE10066:
	.size	_ZN2v88internal6torque21FieldAccessExpressionD0Ev, .-_ZN2v88internal6torque21FieldAccessExpressionD0Ev
	.section	.text._ZN2v88internal6torque10IdentifierD0Ev,"axG",@progbits,_ZN2v88internal6torque10IdentifierD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque10IdentifierD0Ev
	.type	_ZN2v88internal6torque10IdentifierD0Ev, @function
_ZN2v88internal6torque10IdentifierD0Ev:
.LFB10006:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal6torque10IdentifierE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	32(%rdi), %rdi
	leaq	48(%r12), %rax
	cmpq	%rax, %rdi
	je	.L29
	call	_ZdlPv@PLT
.L29:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$64, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE10006:
	.size	_ZN2v88internal6torque10IdentifierD0Ev, .-_ZN2v88internal6torque10IdentifierD0Ev
	.section	.text._ZN2v88internal6torque20AssignmentExpressionD0Ev,"axG",@progbits,_ZN2v88internal6torque20AssignmentExpressionD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque20AssignmentExpressionD0Ev
	.type	_ZN2v88internal6torque20AssignmentExpressionD0Ev, @function
_ZN2v88internal6torque20AssignmentExpressionD0Ev:
.LFB10117:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal6torque20AssignmentExpressionE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	cmpb	$0, 40(%rdi)
	movq	%rax, (%rdi)
	je	.L32
	movq	48(%rdi), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L32
	call	_ZdlPv@PLT
.L32:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$88, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE10117:
	.size	_ZN2v88internal6torque20AssignmentExpressionD0Ev, .-_ZN2v88internal6torque20AssignmentExpressionD0Ev
	.section	.text._ZNK2v88internal6torque9UnionType11IsSubtypeOfEPKNS1_4TypeE,"axG",@progbits,_ZNK2v88internal6torque9UnionType11IsSubtypeOfEPKNS1_4TypeE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal6torque9UnionType11IsSubtypeOfEPKNS1_4TypeE
	.type	_ZNK2v88internal6torque9UnionType11IsSubtypeOfEPKNS1_4TypeE, @function
_ZNK2v88internal6torque9UnionType11IsSubtypeOfEPKNS1_4TypeE:
.LFB5653:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	leaq	80(%rdi), %rbx
	subq	$8, %rsp
	movq	96(%rdi), %r12
	cmpq	%rbx, %r12
	je	.L35
	movq	%rsi, %r13
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L43:
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r12
	cmpq	%rax, %rbx
	je	.L35
.L37:
	movq	32(%r12), %rdi
	movq	%r13, %rsi
	movq	(%rdi), %rax
	call	*16(%rax)
	testb	%al, %al
	jne	.L43
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L35:
	.cfi_restore_state
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5653:
	.size	_ZNK2v88internal6torque9UnionType11IsSubtypeOfEPKNS1_4TypeE, .-_ZNK2v88internal6torque9UnionType11IsSubtypeOfEPKNS1_4TypeE
	.section	.text._ZNK2v88internal6torque9UnionType11IsTransientEv,"axG",@progbits,_ZNK2v88internal6torque9UnionType11IsTransientEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal6torque9UnionType11IsTransientEv
	.type	_ZNK2v88internal6torque9UnionType11IsTransientEv, @function
_ZNK2v88internal6torque9UnionType11IsTransientEv:
.LFB5655:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	96(%rdi), %r12
	leaq	80(%rdi), %rbx
	cmpq	%rbx, %r12
	jne	.L47
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L50:
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r12
	cmpq	%rax, %rbx
	je	.L45
.L47:
	movq	32(%r12), %rdi
	movq	(%rdi), %rax
	call	*40(%rax)
	testb	%al, %al
	je	.L50
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L45:
	.cfi_restore_state
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5655:
	.size	_ZNK2v88internal6torque9UnionType11IsTransientEv, .-_ZNK2v88internal6torque9UnionType11IsTransientEv
	.section	.rodata._ZNK2v88internal6torque13AggregateType24GetGeneratedTypeNameImplB5cxx11Ev.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZNK2v88internal6torque13AggregateType24GetGeneratedTypeNameImplB5cxx11Ev,"axG",@progbits,_ZNK2v88internal6torque13AggregateType24GetGeneratedTypeNameImplB5cxx11Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal6torque13AggregateType24GetGeneratedTypeNameImplB5cxx11Ev
	.type	_ZNK2v88internal6torque13AggregateType24GetGeneratedTypeNameImplB5cxx11Ev, @function
_ZNK2v88internal6torque13AggregateType24GetGeneratedTypeNameImplB5cxx11Ev:
.LFB5713:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE5713:
	.size	_ZNK2v88internal6torque13AggregateType24GetGeneratedTypeNameImplB5cxx11Ev, .-_ZNK2v88internal6torque13AggregateType24GetGeneratedTypeNameImplB5cxx11Ev
	.section	.text._ZNK2v88internal6torque13AggregateType29GetGeneratedTNodeTypeNameImplB5cxx11Ev,"axG",@progbits,_ZNK2v88internal6torque13AggregateType29GetGeneratedTNodeTypeNameImplB5cxx11Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal6torque13AggregateType29GetGeneratedTNodeTypeNameImplB5cxx11Ev
	.type	_ZNK2v88internal6torque13AggregateType29GetGeneratedTNodeTypeNameImplB5cxx11Ev, @function
_ZNK2v88internal6torque13AggregateType29GetGeneratedTNodeTypeNameImplB5cxx11Ev:
.LFB5714:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE5714:
	.size	_ZNK2v88internal6torque13AggregateType29GetGeneratedTNodeTypeNameImplB5cxx11Ev, .-_ZNK2v88internal6torque13AggregateType29GetGeneratedTNodeTypeNameImplB5cxx11Ev
	.section	.text._ZN2v88internal6torque20IdentifierExpression22VisitAllSubExpressionsESt8functionIFvPNS1_10ExpressionEEE,"axG",@progbits,_ZN2v88internal6torque20IdentifierExpression22VisitAllSubExpressionsESt8functionIFvPNS1_10ExpressionEEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque20IdentifierExpression22VisitAllSubExpressionsESt8functionIFvPNS1_10ExpressionEEE
	.type	_ZN2v88internal6torque20IdentifierExpression22VisitAllSubExpressionsESt8functionIFvPNS1_10ExpressionEEE, @function
_ZN2v88internal6torque20IdentifierExpression22VisitAllSubExpressionsESt8functionIFvPNS1_10ExpressionEEE:
.LFB4725:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rdx
	movq	%rdx, -8(%rbp)
	xorl	%edx, %edx
	cmpq	$0, 16(%rsi)
	movq	%rdi, -16(%rbp)
	je	.L59
	movq	%rsi, %rax
	leaq	-16(%rbp), %rsi
	movq	%rax, %rdi
	call	*24(%rax)
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L60
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L59:
	.cfi_restore_state
	call	_ZSt25__throw_bad_function_callv@PLT
.L60:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4725:
	.size	_ZN2v88internal6torque20IdentifierExpression22VisitAllSubExpressionsESt8functionIFvPNS1_10ExpressionEEE, .-_ZN2v88internal6torque20IdentifierExpression22VisitAllSubExpressionsESt8functionIFvPNS1_10ExpressionEEE
	.section	.rodata._ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag.constprop.0.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"basic_string::_M_construct null not valid"
	.section	.text._ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag.constprop.0, @function
_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag.constprop.0:
.LFB13127:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L62
	testq	%rsi, %rsi
	je	.L78
.L62:
	subq	%r13, %r12
	movq	%r12, -48(%rbp)
	cmpq	$15, %r12
	ja	.L79
	movq	(%rbx), %rdi
	cmpq	$1, %r12
	jne	.L65
	movzbl	0(%r13), %eax
	movb	%al, (%rdi)
	movq	-48(%rbp), %r12
	movq	(%rbx), %rdi
.L66:
	movq	%r12, 8(%rbx)
	movb	$0, (%rdi,%r12)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L80
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L65:
	.cfi_restore_state
	testq	%r12, %r12
	je	.L66
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L79:
	movq	%rbx, %rdi
	leaq	-48(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%rbx)
	movq	%rax, %rdi
	movq	-48(%rbp), %rax
	movq	%rax, 16(%rbx)
.L64:
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	-48(%rbp), %r12
	movq	(%rbx), %rdi
	jmp	.L66
.L78:
	leaq	.LC1(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L80:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13127:
	.size	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag.constprop.0, .-_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag.constprop.0
	.section	.text._ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0, @function
_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0:
.LFB12868:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	leaq	16(%rdi), %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%r14, (%rdi)
	testq	%rsi, %rsi
	je	.L92
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	movq	%rsi, %r13
	call	strlen@PLT
	movq	%rax, -48(%rbp)
	movq	%rax, %r12
	cmpq	$15, %rax
	ja	.L93
	cmpq	$1, %rax
	jne	.L85
	movzbl	0(%r13), %edx
	movb	%dl, 16(%rbx)
.L86:
	movq	%rax, 8(%rbx)
	movb	$0, (%r14,%rax)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L94
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L85:
	.cfi_restore_state
	testq	%rax, %rax
	je	.L86
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L93:
	leaq	-48(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%rbx)
	movq	%rax, %r14
	movq	-48(%rbp), %rax
	movq	%rax, 16(%rbx)
.L84:
	movq	%r14, %rdi
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	-48(%rbp), %rax
	movq	(%rbx), %r14
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L92:
	leaq	.LC1(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L94:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE12868:
	.size	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0, .-_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	.section	.text._ZN2v88internal6torque20IdentifierExpressionD0Ev,"axG",@progbits,_ZN2v88internal6torque20IdentifierExpressionD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque20IdentifierExpressionD0Ev
	.type	_ZN2v88internal6torque20IdentifierExpressionD0Ev, @function
_ZN2v88internal6torque20IdentifierExpressionD0Ev:
.LFB10037:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal6torque20IdentifierExpressionE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rax, (%rdi)
	movq	64(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L96
	call	_ZdlPv@PLT
.L96:
	movq	40(%r13), %rbx
	movq	32(%r13), %r12
	cmpq	%r12, %rbx
	je	.L97
	.p2align 4,,10
	.p2align 3
.L101:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L98
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L101
.L99:
	movq	32(%r13), %r12
.L97:
	testq	%r12, %r12
	je	.L102
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L102:
	addq	$8, %rsp
	movq	%r13, %rdi
	movl	$88, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L98:
	.cfi_restore_state
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L101
	jmp	.L99
	.cfi_endproc
.LFE10037:
	.size	_ZN2v88internal6torque20IdentifierExpressionD0Ev, .-_ZN2v88internal6torque20IdentifierExpressionD0Ev
	.section	.text._ZN2v88internal6torque20IdentifierExpressionD2Ev,"axG",@progbits,_ZN2v88internal6torque20IdentifierExpressionD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque20IdentifierExpressionD2Ev
	.type	_ZN2v88internal6torque20IdentifierExpressionD2Ev, @function
_ZN2v88internal6torque20IdentifierExpressionD2Ev:
.LFB10035:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal6torque20IdentifierExpressionE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	64(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L111
	call	_ZdlPv@PLT
.L111:
	movq	40(%rbx), %r13
	movq	32(%rbx), %r12
	cmpq	%r12, %r13
	je	.L112
	.p2align 4,,10
	.p2align 3
.L116:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L113
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r12, %r13
	jne	.L116
.L114:
	movq	32(%rbx), %r12
.L112:
	testq	%r12, %r12
	je	.L110
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L113:
	.cfi_restore_state
	addq	$32, %r12
	cmpq	%r12, %r13
	jne	.L116
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L110:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10035:
	.size	_ZN2v88internal6torque20IdentifierExpressionD2Ev, .-_ZN2v88internal6torque20IdentifierExpressionD2Ev
	.weak	_ZN2v88internal6torque20IdentifierExpressionD1Ev
	.set	_ZN2v88internal6torque20IdentifierExpressionD1Ev,_ZN2v88internal6torque20IdentifierExpressionD2Ev
	.section	.rodata._ZNK2v88internal6torque12AbstractType24GetGeneratedTypeNameImplB5cxx11Ev.str1.1,"aMS",@progbits,1
.LC2:
	.string	"basic_string::append"
.LC3:
	.string	"compiler::TNode<"
.LC4:
	.string	">"
	.section	.text._ZNK2v88internal6torque12AbstractType24GetGeneratedTypeNameImplB5cxx11Ev,"axG",@progbits,_ZNK2v88internal6torque12AbstractType24GetGeneratedTypeNameImplB5cxx11Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal6torque12AbstractType24GetGeneratedTypeNameImplB5cxx11Ev
	.type	_ZNK2v88internal6torque12AbstractType24GetGeneratedTypeNameImplB5cxx11Ev, @function
_ZNK2v88internal6torque12AbstractType24GetGeneratedTypeNameImplB5cxx11Ev:
.LFB5619:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA5619
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 144(%rsi)
	je	.L123
	leaq	16(%rdi), %rax
	movq	%rax, (%rdi)
	movq	112(%rsi), %rsi
	movq	120(%rbx), %rdx
	addq	%rsi, %rdx
.LEHB0:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag.constprop.0
.LEHE0:
.L122:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L143
	addq	$48, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L123:
	.cfi_restore_state
	movq	120(%rsi), %rax
	leaq	-80(%rbp), %r14
	leaq	-64(%rbp), %r13
	movb	$0, -64(%rbp)
	movq	%r14, %rdi
	movq	%r13, -80(%rbp)
	leaq	16(%rax), %rsi
	movq	$0, -72(%rbp)
.LEHB1:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7reserveEm@PLT
	movabsq	$4611686018427387903, %rax
	subq	-72(%rbp), %rax
	cmpq	$15, %rax
	jbe	.L144
	movl	$16, %edx
	leaq	.LC3(%rip), %rsi
	movq	%r14, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	120(%rbx), %rdx
	movq	112(%rbx), %rsi
	movq	%r14, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.LEHE1:
	movabsq	$4611686018427387903, %rax
	cmpq	%rax, -72(%rbp)
	je	.L145
	movl	$1, %edx
	leaq	.LC4(%rip), %rsi
	movq	%r14, %rdi
.LEHB2:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.LEHE2:
	leaq	16(%r12), %rdx
	movq	%rdx, (%r12)
	movq	(%rax), %rcx
	leaq	16(%rax), %rdx
	cmpq	%rdx, %rcx
	je	.L146
	movq	%rcx, (%r12)
	movq	16(%rax), %rcx
	movq	%rcx, 16(%r12)
.L131:
	movq	8(%rax), %rcx
	movq	%rdx, (%rax)
	movq	-80(%rbp), %rdi
	movq	$0, 8(%rax)
	movq	%rcx, 8(%r12)
	movb	$0, 16(%rax)
	cmpq	%r13, %rdi
	je	.L122
	call	_ZdlPv@PLT
	jmp	.L122
	.p2align 4,,10
	.p2align 3
.L146:
	movdqu	16(%rax), %xmm0
	movups	%xmm0, 16(%r12)
	jmp	.L131
.L143:
	call	__stack_chk_fail@PLT
.L144:
	leaq	.LC2(%rip), %rdi
.LEHB3:
	call	_ZSt20__throw_length_errorPKc@PLT
.LEHE3:
.L145:
	leaq	.LC2(%rip), %rdi
.LEHB4:
	call	_ZSt20__throw_length_errorPKc@PLT
.LEHE4:
.L137:
	endbr64
.L142:
	movq	%rax, %r12
	jmp	.L133
.L136:
	endbr64
	jmp	.L142
.L133:
	movq	-80(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L134
	call	_ZdlPv@PLT
.L134:
	movq	%r12, %rdi
.LEHB5:
	call	_Unwind_Resume@PLT
.LEHE5:
	.cfi_endproc
.LFE5619:
	.globl	__gxx_personality_v0
	.section	.gcc_except_table._ZNK2v88internal6torque12AbstractType24GetGeneratedTypeNameImplB5cxx11Ev,"aG",@progbits,_ZNK2v88internal6torque12AbstractType24GetGeneratedTypeNameImplB5cxx11Ev,comdat
.LLSDA5619:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE5619-.LLSDACSB5619
.LLSDACSB5619:
	.uleb128 .LEHB0-.LFB5619
	.uleb128 .LEHE0-.LEHB0
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB1-.LFB5619
	.uleb128 .LEHE1-.LEHB1
	.uleb128 .L137-.LFB5619
	.uleb128 0
	.uleb128 .LEHB2-.LFB5619
	.uleb128 .LEHE2-.LEHB2
	.uleb128 .L136-.LFB5619
	.uleb128 0
	.uleb128 .LEHB3-.LFB5619
	.uleb128 .LEHE3-.LEHB3
	.uleb128 .L137-.LFB5619
	.uleb128 0
	.uleb128 .LEHB4-.LFB5619
	.uleb128 .LEHE4-.LEHB4
	.uleb128 .L136-.LFB5619
	.uleb128 0
	.uleb128 .LEHB5-.LFB5619
	.uleb128 .LEHE5-.LEHB5
	.uleb128 0
	.uleb128 0
.LLSDACSE5619:
	.section	.text._ZNK2v88internal6torque12AbstractType24GetGeneratedTypeNameImplB5cxx11Ev,"axG",@progbits,_ZNK2v88internal6torque12AbstractType24GetGeneratedTypeNameImplB5cxx11Ev,comdat
	.size	_ZNK2v88internal6torque12AbstractType24GetGeneratedTypeNameImplB5cxx11Ev, .-_ZNK2v88internal6torque12AbstractType24GetGeneratedTypeNameImplB5cxx11Ev
	.section	.text._ZN2v88internal6torque21FieldAccessExpression22VisitAllSubExpressionsESt8functionIFvPNS1_10ExpressionEEE,"axG",@progbits,_ZN2v88internal6torque21FieldAccessExpression22VisitAllSubExpressionsESt8functionIFvPNS1_10ExpressionEEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque21FieldAccessExpression22VisitAllSubExpressionsESt8functionIFvPNS1_10ExpressionEEE
	.type	_ZN2v88internal6torque21FieldAccessExpression22VisitAllSubExpressionsESt8functionIFvPNS1_10ExpressionEEE, @function
_ZN2v88internal6torque21FieldAccessExpression22VisitAllSubExpressionsESt8functionIFvPNS1_10ExpressionEEE:
.LFB4830:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA4830
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-96(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$72, %rsp
	movq	32(%rdi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%r14), %rax
	movq	16(%rax), %r15
	movq	16(%rsi), %rax
	movq	$0, -80(%rbp)
	testq	%rax, %rax
	je	.L148
	movl	$2, %edx
	movq	%r13, %rdi
.LEHB6:
	call	*%rax
.LEHE6:
	movdqu	16(%rbx), %xmm0
	movaps	%xmm0, -80(%rbp)
.L148:
	movq	%r13, %rsi
	movq	%r14, %rdi
.LEHB7:
	call	*%r15
.LEHE7:
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L152
	movl	$3, %edx
	movq	%r13, %rsi
	movq	%r13, %rdi
	call	*%rax
.L152:
	cmpq	$0, 16(%rbx)
	movq	%r12, -104(%rbp)
	je	.L174
	leaq	-104(%rbp), %rsi
	movq	%rbx, %rdi
.LEHB8:
	call	*24(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L175
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L174:
	.cfi_restore_state
	call	_ZSt25__throw_bad_function_callv@PLT
.LEHE8:
.L175:
	call	__stack_chk_fail@PLT
.L157:
	endbr64
	movq	%rax, %r12
	jmp	.L154
.L158:
	endbr64
	movq	%rax, %r12
	jmp	.L150
.L154:
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L155
	movl	$3, %edx
	movq	%r13, %rsi
	movq	%r13, %rdi
	call	*%rax
.L155:
	movq	%r12, %rdi
.LEHB9:
	call	_Unwind_Resume@PLT
.LEHE9:
.L150:
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L155
	movl	$3, %edx
	movq	%r13, %rsi
	movq	%r13, %rdi
	call	*%rax
	jmp	.L155
	.cfi_endproc
.LFE4830:
	.section	.gcc_except_table._ZN2v88internal6torque21FieldAccessExpression22VisitAllSubExpressionsESt8functionIFvPNS1_10ExpressionEEE,"aG",@progbits,_ZN2v88internal6torque21FieldAccessExpression22VisitAllSubExpressionsESt8functionIFvPNS1_10ExpressionEEE,comdat
.LLSDA4830:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE4830-.LLSDACSB4830
.LLSDACSB4830:
	.uleb128 .LEHB6-.LFB4830
	.uleb128 .LEHE6-.LEHB6
	.uleb128 .L158-.LFB4830
	.uleb128 0
	.uleb128 .LEHB7-.LFB4830
	.uleb128 .LEHE7-.LEHB7
	.uleb128 .L157-.LFB4830
	.uleb128 0
	.uleb128 .LEHB8-.LFB4830
	.uleb128 .LEHE8-.LEHB8
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB9-.LFB4830
	.uleb128 .LEHE9-.LEHB9
	.uleb128 0
	.uleb128 0
.LLSDACSE4830:
	.section	.text._ZN2v88internal6torque21FieldAccessExpression22VisitAllSubExpressionsESt8functionIFvPNS1_10ExpressionEEE,"axG",@progbits,_ZN2v88internal6torque21FieldAccessExpression22VisitAllSubExpressionsESt8functionIFvPNS1_10ExpressionEEE,comdat
	.size	_ZN2v88internal6torque21FieldAccessExpression22VisitAllSubExpressionsESt8functionIFvPNS1_10ExpressionEEE, .-_ZN2v88internal6torque21FieldAccessExpression22VisitAllSubExpressionsESt8functionIFvPNS1_10ExpressionEEE
	.section	.text._ZN2v88internal6torque20AssignmentExpression22VisitAllSubExpressionsESt8functionIFvPNS1_10ExpressionEEE,"axG",@progbits,_ZN2v88internal6torque20AssignmentExpression22VisitAllSubExpressionsESt8functionIFvPNS1_10ExpressionEEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque20AssignmentExpression22VisitAllSubExpressionsESt8functionIFvPNS1_10ExpressionEEE
	.type	_ZN2v88internal6torque20AssignmentExpression22VisitAllSubExpressionsESt8functionIFvPNS1_10ExpressionEEE, @function
_ZN2v88internal6torque20AssignmentExpression22VisitAllSubExpressionsESt8functionIFvPNS1_10ExpressionEEE:
.LFB4864:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA4864
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-96(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$72, %rsp
	movq	32(%rdi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%r14), %rax
	movq	16(%rax), %r15
	movq	16(%rsi), %rax
	movq	$0, -80(%rbp)
	testq	%rax, %rax
	je	.L177
	movl	$2, %edx
	movq	%r12, %rdi
.LEHB10:
	call	*%rax
.LEHE10:
	movdqu	16(%rbx), %xmm0
	movaps	%xmm0, -80(%rbp)
.L177:
	movq	%r12, %rsi
	movq	%r14, %rdi
.LEHB11:
	call	*%r15
.LEHE11:
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L181
	movl	$3, %edx
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	*%rax
.L181:
	movq	80(%r13), %r14
	movq	(%r14), %rax
	movq	16(%rax), %r15
	movq	16(%rbx), %rax
	movq	$0, -80(%rbp)
	testq	%rax, %rax
	je	.L182
	movl	$2, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
.LEHB12:
	call	*%rax
.LEHE12:
	movdqu	16(%rbx), %xmm1
	movaps	%xmm1, -80(%rbp)
.L182:
	movq	%r12, %rsi
	movq	%r14, %rdi
.LEHB13:
	call	*%r15
.LEHE13:
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L186
	movl	$3, %edx
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	*%rax
.L186:
	cmpq	$0, 16(%rbx)
	movq	%r13, -104(%rbp)
	je	.L225
	leaq	-104(%rbp), %rsi
	movq	%rbx, %rdi
.LEHB14:
	call	*24(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L226
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L225:
	.cfi_restore_state
	call	_ZSt25__throw_bad_function_callv@PLT
.LEHE14:
.L226:
	call	__stack_chk_fail@PLT
.L195:
	endbr64
	movq	%rax, %r13
	jmp	.L179
.L193:
	endbr64
	movq	%rax, %r13
	jmp	.L188
.L179:
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L191
	movl	$3, %edx
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	*%rax
.L191:
	movq	%r13, %rdi
.LEHB15:
	call	_Unwind_Resume@PLT
.LEHE15:
.L188:
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L191
	movl	$3, %edx
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	*%rax
	jmp	.L191
.L196:
	endbr64
	movq	%rax, %r13
	jmp	.L184
.L194:
	endbr64
	movq	%rax, %r13
	jmp	.L190
.L184:
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L191
	movl	$3, %edx
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	*%rax
	jmp	.L191
.L190:
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L191
	movl	$3, %edx
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	*%rax
	jmp	.L191
	.cfi_endproc
.LFE4864:
	.section	.gcc_except_table._ZN2v88internal6torque20AssignmentExpression22VisitAllSubExpressionsESt8functionIFvPNS1_10ExpressionEEE,"aG",@progbits,_ZN2v88internal6torque20AssignmentExpression22VisitAllSubExpressionsESt8functionIFvPNS1_10ExpressionEEE,comdat
.LLSDA4864:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE4864-.LLSDACSB4864
.LLSDACSB4864:
	.uleb128 .LEHB10-.LFB4864
	.uleb128 .LEHE10-.LEHB10
	.uleb128 .L195-.LFB4864
	.uleb128 0
	.uleb128 .LEHB11-.LFB4864
	.uleb128 .LEHE11-.LEHB11
	.uleb128 .L193-.LFB4864
	.uleb128 0
	.uleb128 .LEHB12-.LFB4864
	.uleb128 .LEHE12-.LEHB12
	.uleb128 .L196-.LFB4864
	.uleb128 0
	.uleb128 .LEHB13-.LFB4864
	.uleb128 .LEHE13-.LEHB13
	.uleb128 .L194-.LFB4864
	.uleb128 0
	.uleb128 .LEHB14-.LFB4864
	.uleb128 .LEHE14-.LEHB14
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB15-.LFB4864
	.uleb128 .LEHE15-.LEHB15
	.uleb128 0
	.uleb128 0
.LLSDACSE4864:
	.section	.text._ZN2v88internal6torque20AssignmentExpression22VisitAllSubExpressionsESt8functionIFvPNS1_10ExpressionEEE,"axG",@progbits,_ZN2v88internal6torque20AssignmentExpression22VisitAllSubExpressionsESt8functionIFvPNS1_10ExpressionEEE,comdat
	.size	_ZN2v88internal6torque20AssignmentExpression22VisitAllSubExpressionsESt8functionIFvPNS1_10ExpressionEEE, .-_ZN2v88internal6torque20AssignmentExpression22VisitAllSubExpressionsESt8functionIFvPNS1_10ExpressionEEE
	.section	.rodata._ZNK2v88internal6torque12AbstractType11MangledNameB5cxx11Ev.str1.1,"aMS",@progbits,1
.LC5:
	.string	"AT"
	.section	.text._ZNK2v88internal6torque12AbstractType11MangledNameB5cxx11Ev,"axG",@progbits,_ZNK2v88internal6torque12AbstractType11MangledNameB5cxx11Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal6torque12AbstractType11MangledNameB5cxx11Ev
	.type	_ZNK2v88internal6torque12AbstractType11MangledNameB5cxx11Ev, @function
_ZNK2v88internal6torque12AbstractType11MangledNameB5cxx11Ev:
.LFB5618:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA5618
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	leaq	-80(%rbp), %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	leaq	-64(%rbp), %rbx
	subq	$48, %rsp
	movq	80(%rsi), %r8
	movq	88(%rsi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%rbx, -80(%rbp)
	addq	%r8, %rdx
	movq	%r8, %rsi
.LEHB16:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag.constprop.0
.LEHE16:
	movq	-80(%rbp), %rax
	movq	-72(%rbp), %rsi
	leaq	(%rax,%rsi), %rdx
	cmpq	%rdx, %rax
	je	.L228
	.p2align 4,,10
	.p2align 3
.L230:
	cmpb	$32, (%rax)
	jne	.L229
	movb	$95, (%rax)
.L229:
	addq	$1, %rax
	cmpq	%rdx, %rax
	jne	.L230
	movq	-72(%rbp), %rsi
.L228:
	leaq	16(%r12), %r13
	movb	$0, 16(%r12)
	addq	$2, %rsi
	movq	%r12, %rdi
	movq	%r13, (%r12)
	movq	$0, 8(%r12)
.LEHB17:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7reserveEm@PLT
	movabsq	$4611686018427387903, %rax
	subq	8(%r12), %rax
	cmpq	$1, %rax
	jbe	.L243
	movl	$2, %edx
	leaq	.LC5(%rip), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-72(%rbp), %rdx
	movq	-80(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L227
	call	_ZdlPv@PLT
.L227:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L244
	addq	$48, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L243:
	.cfi_restore_state
	leaq	.LC2(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.LEHE17:
.L244:
	call	__stack_chk_fail@PLT
.L239:
	endbr64
	movq	%rax, %r14
.L233:
	movq	(%r12), %rdi
	cmpq	%rdi, %r13
	je	.L234
	call	_ZdlPv@PLT
.L234:
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L236
	call	_ZdlPv@PLT
.L236:
	movq	%r14, %rdi
.LEHB18:
	call	_Unwind_Resume@PLT
.LEHE18:
	.cfi_endproc
.LFE5618:
	.section	.gcc_except_table._ZNK2v88internal6torque12AbstractType11MangledNameB5cxx11Ev,"aG",@progbits,_ZNK2v88internal6torque12AbstractType11MangledNameB5cxx11Ev,comdat
.LLSDA5618:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE5618-.LLSDACSB5618
.LLSDACSB5618:
	.uleb128 .LEHB16-.LFB5618
	.uleb128 .LEHE16-.LEHB16
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB17-.LFB5618
	.uleb128 .LEHE17-.LEHB17
	.uleb128 .L239-.LFB5618
	.uleb128 0
	.uleb128 .LEHB18-.LFB5618
	.uleb128 .LEHE18-.LEHB18
	.uleb128 0
	.uleb128 0
.LLSDACSE5618:
	.section	.text._ZNK2v88internal6torque12AbstractType11MangledNameB5cxx11Ev,"axG",@progbits,_ZNK2v88internal6torque12AbstractType11MangledNameB5cxx11Ev,comdat
	.size	_ZNK2v88internal6torque12AbstractType11MangledNameB5cxx11Ev, .-_ZNK2v88internal6torque12AbstractType11MangledNameB5cxx11Ev
	.section	.text._ZNK2v88internal6torque18BuiltinPointerType15GetRuntimeTypesB5cxx11Ev,"axG",@progbits,_ZNK2v88internal6torque18BuiltinPointerType15GetRuntimeTypesB5cxx11Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal6torque18BuiltinPointerType15GetRuntimeTypesB5cxx11Ev
	.type	_ZNK2v88internal6torque18BuiltinPointerType15GetRuntimeTypesB5cxx11Ev, @function
_ZNK2v88internal6torque18BuiltinPointerType15GetRuntimeTypesB5cxx11Ev:
.LFB5640:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA5640
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, 16(%rdi)
	leaq	-80(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	%rax, -96(%rbp)
	movl	$27987, %eax
	movups	%xmm0, (%rdi)
	movl	$32, %edi
	movw	%ax, -80(%rbp)
	movb	$105, -78(%rbp)
	movq	$3, -88(%rbp)
	movb	$0, -77(%rbp)
.LEHB19:
	call	_Znwm@PLT
.LEHE19:
	movq	-96(%rbp), %r14
	leaq	16(%rax), %rdi
	movq	-88(%rbp), %r12
	leaq	32(%rax), %r15
	movq	%rax, 0(%r13)
	movq	%rax, %rbx
	movq	%rdi, (%rax)
	movq	%r14, %rax
	addq	%r12, %rax
	movq	%r15, 16(%r13)
	je	.L246
	testq	%r14, %r14
	je	.L274
.L246:
	movq	%r12, -104(%rbp)
	cmpq	$15, %r12
	ja	.L275
	cmpq	$1, %r12
	jne	.L249
	movzbl	(%r14), %eax
	movb	%al, 16(%rbx)
.L250:
	movq	%r12, 8(%rbx)
	movb	$0, (%rdi,%r12)
	movq	-96(%rbp), %rdi
	movq	%r15, 8(%r13)
	cmpq	-120(%rbp), %rdi
	je	.L245
	call	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L245:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L276
	addq	$88, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L249:
	.cfi_restore_state
	testq	%r12, %r12
	je	.L250
	jmp	.L248
	.p2align 4,,10
	.p2align 3
.L275:
	leaq	-104(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rbx, %rdi
.LEHB20:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%rbx)
	movq	%rax, %rdi
	movq	-104(%rbp), %rax
	movq	%rax, 16(%rbx)
.L248:
	movq	%r12, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-104(%rbp), %r12
	movq	(%rbx), %rdi
	jmp	.L250
.L274:
	leaq	.LC1(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE20:
.L276:
	call	__stack_chk_fail@PLT
.L258:
	endbr64
	movq	%rax, %r12
	jmp	.L254
.L260:
	endbr64
	movq	%rax, %rdi
	jmp	.L252
.L253:
	call	__cxa_end_catch@PLT
.L254:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L255
	call	_ZdlPv@PLT
.L255:
	movq	-96(%rbp), %rdi
	cmpq	-120(%rbp), %rdi
	je	.L256
	call	_ZdlPv@PLT
.L256:
	movq	%r12, %rdi
.LEHB21:
	call	_Unwind_Resume@PLT
.LEHE21:
.L252:
	call	__cxa_begin_catch@PLT
.LEHB22:
	call	__cxa_rethrow@PLT
.LEHE22:
.L259:
	endbr64
	movq	%rax, %r12
	jmp	.L253
	.cfi_endproc
.LFE5640:
	.section	.gcc_except_table._ZNK2v88internal6torque18BuiltinPointerType15GetRuntimeTypesB5cxx11Ev,"aG",@progbits,_ZNK2v88internal6torque18BuiltinPointerType15GetRuntimeTypesB5cxx11Ev,comdat
	.align 4
.LLSDA5640:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATT5640-.LLSDATTD5640
.LLSDATTD5640:
	.byte	0x1
	.uleb128 .LLSDACSE5640-.LLSDACSB5640
.LLSDACSB5640:
	.uleb128 .LEHB19-.LFB5640
	.uleb128 .LEHE19-.LEHB19
	.uleb128 .L258-.LFB5640
	.uleb128 0
	.uleb128 .LEHB20-.LFB5640
	.uleb128 .LEHE20-.LEHB20
	.uleb128 .L260-.LFB5640
	.uleb128 0x1
	.uleb128 .LEHB21-.LFB5640
	.uleb128 .LEHE21-.LEHB21
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB22-.LFB5640
	.uleb128 .LEHE22-.LEHB22
	.uleb128 .L259-.LFB5640
	.uleb128 0
.LLSDACSE5640:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATT5640:
	.section	.text._ZNK2v88internal6torque18BuiltinPointerType15GetRuntimeTypesB5cxx11Ev,"axG",@progbits,_ZNK2v88internal6torque18BuiltinPointerType15GetRuntimeTypesB5cxx11Ev,comdat
	.size	_ZNK2v88internal6torque18BuiltinPointerType15GetRuntimeTypesB5cxx11Ev, .-_ZNK2v88internal6torque18BuiltinPointerType15GetRuntimeTypesB5cxx11Ev
	.section	.text._ZNK2v88internal6torque4Type11IsSubtypeOfEPKS2_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal6torque4Type11IsSubtypeOfEPKS2_
	.type	_ZNK2v88internal6torque4Type11IsSubtypeOfEPKS2_, @function
_ZNK2v88internal6torque4Type11IsSubtypeOfEPKS2_:
.LFB6510:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	8(%rsi), %eax
	testl	%eax, %eax
	jne	.L297
.L278:
	movl	$1, %eax
.L277:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L298
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L297:
	.cfi_restore_state
	leaq	-64(%rbp), %rdx
	cmpl	$1, 8(%rdi)
	movb	$114, -60(%rbp)
	movq	%rdi, %rbx
	movq	%rdx, -80(%rbp)
	movl	$1702258030, -64(%rbp)
	movq	$5, -72(%rbp)
	movb	$0, -59(%rbp)
	jne	.L282
	cmpq	$5, 88(%rdi)
	je	.L299
.L282:
	cmpl	$3, %eax
	jne	.L280
	movq	96(%rsi), %r12
	leaq	80(%rsi), %r13
	cmpq	%r13, %r12
	je	.L286
	.p2align 4,,10
	.p2align 3
.L287:
	movq	(%rbx), %rax
	movq	32(%r12), %rsi
	movq	%rbx, %rdi
	call	*16(%rax)
	testb	%al, %al
	jne	.L278
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r12
	cmpq	%rax, %r13
	jne	.L287
.L286:
	xorl	%eax, %eax
	jmp	.L277
	.p2align 4,,10
	.p2align 3
.L280:
	cmpq	%rbx, %rsi
	je	.L278
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L280
	xorl	%eax, %eax
	jmp	.L277
	.p2align 4,,10
	.p2align 3
.L299:
	movq	80(%rdi), %rdx
	cmpl	$1702258030, (%rdx)
	jne	.L282
	cmpb	$114, 4(%rdx)
	jne	.L282
	jmp	.L278
.L298:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6510:
	.size	_ZNK2v88internal6torque4Type11IsSubtypeOfEPKS2_, .-_ZNK2v88internal6torque4Type11IsSubtypeOfEPKS2_
	.section	.text._ZNK2v88internal6torque12AbstractType15GetRuntimeTypesB5cxx11Ev,"axG",@progbits,_ZNK2v88internal6torque12AbstractType15GetRuntimeTypesB5cxx11Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal6torque12AbstractType15GetRuntimeTypesB5cxx11Ev
	.type	_ZNK2v88internal6torque12AbstractType15GetRuntimeTypesB5cxx11Ev, @function
_ZNK2v88internal6torque12AbstractType15GetRuntimeTypesB5cxx11Ev:
.LFB5623:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA5623
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	leaq	-96(%rbp), %rdi
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	80(%rsi), %r8
	movq	88(%rsi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-80(%rbp), %rax
	addq	%r8, %rdx
	movq	%r8, %rsi
	movq	%rax, -120(%rbp)
	movq	%rax, -96(%rbp)
.LEHB23:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag.constprop.0
.LEHE23:
	pxor	%xmm0, %xmm0
	movl	$32, %edi
	movq	$0, 16(%r13)
	movups	%xmm0, 0(%r13)
.LEHB24:
	call	_Znwm@PLT
.LEHE24:
	movq	-96(%rbp), %r14
	leaq	16(%rax), %rdi
	movq	-88(%rbp), %r12
	leaq	32(%rax), %r15
	movq	%rax, 0(%r13)
	movq	%rax, %rbx
	movq	%rdi, (%rax)
	movq	%r14, %rax
	addq	%r12, %rax
	movq	%r15, 16(%r13)
	je	.L301
	testq	%r14, %r14
	je	.L329
.L301:
	movq	%r12, -104(%rbp)
	cmpq	$15, %r12
	ja	.L330
	cmpq	$1, %r12
	jne	.L304
	movzbl	(%r14), %eax
	movb	%al, 16(%rbx)
.L305:
	movq	%r12, 8(%rbx)
	movb	$0, (%rdi,%r12)
	movq	-96(%rbp), %rdi
	movq	%r15, 8(%r13)
	cmpq	-120(%rbp), %rdi
	je	.L300
	call	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L300:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L331
	addq	$88, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L304:
	.cfi_restore_state
	testq	%r12, %r12
	je	.L305
	jmp	.L303
	.p2align 4,,10
	.p2align 3
.L330:
	leaq	-104(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rbx, %rdi
.LEHB25:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%rbx)
	movq	%rax, %rdi
	movq	-104(%rbp), %rax
	movq	%rax, 16(%rbx)
.L303:
	movq	%r12, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-104(%rbp), %r12
	movq	(%rbx), %rdi
	jmp	.L305
.L329:
	leaq	.LC1(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE25:
.L331:
	call	__stack_chk_fail@PLT
.L313:
	endbr64
	movq	%rax, %r12
	jmp	.L309
.L315:
	endbr64
	movq	%rax, %rdi
	jmp	.L307
.L308:
	call	__cxa_end_catch@PLT
.L309:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L310
	call	_ZdlPv@PLT
.L310:
	movq	-96(%rbp), %rdi
	cmpq	-120(%rbp), %rdi
	je	.L311
	call	_ZdlPv@PLT
.L311:
	movq	%r12, %rdi
.LEHB26:
	call	_Unwind_Resume@PLT
.LEHE26:
.L307:
	call	__cxa_begin_catch@PLT
.LEHB27:
	call	__cxa_rethrow@PLT
.LEHE27:
.L314:
	endbr64
	movq	%rax, %r12
	jmp	.L308
	.cfi_endproc
.LFE5623:
	.section	.gcc_except_table._ZNK2v88internal6torque12AbstractType15GetRuntimeTypesB5cxx11Ev,"aG",@progbits,_ZNK2v88internal6torque12AbstractType15GetRuntimeTypesB5cxx11Ev,comdat
	.align 4
.LLSDA5623:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATT5623-.LLSDATTD5623
.LLSDATTD5623:
	.byte	0x1
	.uleb128 .LLSDACSE5623-.LLSDACSB5623
.LLSDACSB5623:
	.uleb128 .LEHB23-.LFB5623
	.uleb128 .LEHE23-.LEHB23
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB24-.LFB5623
	.uleb128 .LEHE24-.LEHB24
	.uleb128 .L313-.LFB5623
	.uleb128 0
	.uleb128 .LEHB25-.LFB5623
	.uleb128 .LEHE25-.LEHB25
	.uleb128 .L315-.LFB5623
	.uleb128 0x1
	.uleb128 .LEHB26-.LFB5623
	.uleb128 .LEHE26-.LEHB26
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB27-.LFB5623
	.uleb128 .LEHE27-.LEHB27
	.uleb128 .L314-.LFB5623
	.uleb128 0
.LLSDACSE5623:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATT5623:
	.section	.text._ZNK2v88internal6torque12AbstractType15GetRuntimeTypesB5cxx11Ev,"axG",@progbits,_ZNK2v88internal6torque12AbstractType15GetRuntimeTypesB5cxx11Ev,comdat
	.size	_ZNK2v88internal6torque12AbstractType15GetRuntimeTypesB5cxx11Ev, .-_ZNK2v88internal6torque12AbstractType15GetRuntimeTypesB5cxx11Ev
	.section	.text._ZNK2v88internal6torque13AggregateType15GetRuntimeTypesB5cxx11Ev,"axG",@progbits,_ZNK2v88internal6torque13AggregateType15GetRuntimeTypesB5cxx11Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal6torque13AggregateType15GetRuntimeTypesB5cxx11Ev
	.type	_ZNK2v88internal6torque13AggregateType15GetRuntimeTypesB5cxx11Ev, @function
_ZNK2v88internal6torque13AggregateType15GetRuntimeTypesB5cxx11Ev:
.LFB5728:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA5728
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	leaq	-96(%rbp), %rdi
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	112(%rsi), %r8
	movq	120(%rsi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-80(%rbp), %rax
	addq	%r8, %rdx
	movq	%r8, %rsi
	movq	%rax, -120(%rbp)
	movq	%rax, -96(%rbp)
.LEHB28:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag.constprop.0
.LEHE28:
	pxor	%xmm0, %xmm0
	movl	$32, %edi
	movq	$0, 16(%r13)
	movups	%xmm0, 0(%r13)
.LEHB29:
	call	_Znwm@PLT
.LEHE29:
	movq	-96(%rbp), %r14
	leaq	16(%rax), %rdi
	movq	-88(%rbp), %r12
	leaq	32(%rax), %r15
	movq	%rax, 0(%r13)
	movq	%rax, %rbx
	movq	%rdi, (%rax)
	movq	%r14, %rax
	addq	%r12, %rax
	movq	%r15, 16(%r13)
	je	.L333
	testq	%r14, %r14
	je	.L361
.L333:
	movq	%r12, -104(%rbp)
	cmpq	$15, %r12
	ja	.L362
	cmpq	$1, %r12
	jne	.L336
	movzbl	(%r14), %eax
	movb	%al, 16(%rbx)
.L337:
	movq	%r12, 8(%rbx)
	movb	$0, (%rdi,%r12)
	movq	-96(%rbp), %rdi
	movq	%r15, 8(%r13)
	cmpq	-120(%rbp), %rdi
	je	.L332
	call	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L332:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L363
	addq	$88, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L336:
	.cfi_restore_state
	testq	%r12, %r12
	je	.L337
	jmp	.L335
	.p2align 4,,10
	.p2align 3
.L362:
	leaq	-104(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rbx, %rdi
.LEHB30:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%rbx)
	movq	%rax, %rdi
	movq	-104(%rbp), %rax
	movq	%rax, 16(%rbx)
.L335:
	movq	%r12, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-104(%rbp), %r12
	movq	(%rbx), %rdi
	jmp	.L337
.L361:
	leaq	.LC1(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE30:
.L363:
	call	__stack_chk_fail@PLT
.L345:
	endbr64
	movq	%rax, %r12
	jmp	.L341
.L347:
	endbr64
	movq	%rax, %rdi
	jmp	.L339
.L340:
	call	__cxa_end_catch@PLT
.L341:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L342
	call	_ZdlPv@PLT
.L342:
	movq	-96(%rbp), %rdi
	cmpq	-120(%rbp), %rdi
	je	.L343
	call	_ZdlPv@PLT
.L343:
	movq	%r12, %rdi
.LEHB31:
	call	_Unwind_Resume@PLT
.LEHE31:
.L339:
	call	__cxa_begin_catch@PLT
.LEHB32:
	call	__cxa_rethrow@PLT
.LEHE32:
.L346:
	endbr64
	movq	%rax, %r12
	jmp	.L340
	.cfi_endproc
.LFE5728:
	.section	.gcc_except_table._ZNK2v88internal6torque13AggregateType15GetRuntimeTypesB5cxx11Ev,"aG",@progbits,_ZNK2v88internal6torque13AggregateType15GetRuntimeTypesB5cxx11Ev,comdat
	.align 4
.LLSDA5728:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATT5728-.LLSDATTD5728
.LLSDATTD5728:
	.byte	0x1
	.uleb128 .LLSDACSE5728-.LLSDACSB5728
.LLSDACSB5728:
	.uleb128 .LEHB28-.LFB5728
	.uleb128 .LEHE28-.LEHB28
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB29-.LFB5728
	.uleb128 .LEHE29-.LEHB29
	.uleb128 .L345-.LFB5728
	.uleb128 0
	.uleb128 .LEHB30-.LFB5728
	.uleb128 .LEHE30-.LEHB30
	.uleb128 .L347-.LFB5728
	.uleb128 0x1
	.uleb128 .LEHB31-.LFB5728
	.uleb128 .LEHE31-.LEHB31
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB32-.LFB5728
	.uleb128 .LEHE32-.LEHB32
	.uleb128 .L346-.LFB5728
	.uleb128 0
.LLSDACSE5728:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATT5728:
	.section	.text._ZNK2v88internal6torque13AggregateType15GetRuntimeTypesB5cxx11Ev,"axG",@progbits,_ZNK2v88internal6torque13AggregateType15GetRuntimeTypesB5cxx11Ev,comdat
	.size	_ZNK2v88internal6torque13AggregateType15GetRuntimeTypesB5cxx11Ev, .-_ZNK2v88internal6torque13AggregateType15GetRuntimeTypesB5cxx11Ev
	.section	.text._ZNK2v88internal6torque12AbstractType16ToExplicitStringB5cxx11Ev,"axG",@progbits,_ZNK2v88internal6torque12AbstractType16ToExplicitStringB5cxx11Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal6torque12AbstractType16ToExplicitStringB5cxx11Ev
	.type	_ZNK2v88internal6torque12AbstractType16ToExplicitStringB5cxx11Ev, @function
_ZNK2v88internal6torque12AbstractType16ToExplicitStringB5cxx11Ev:
.LFB5617:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	addq	$16, %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%rdi, (%r12)
	movq	80(%rsi), %r14
	movq	88(%rsi), %r13
	movq	%r14, %rax
	addq	%r13, %rax
	je	.L365
	testq	%r14, %r14
	je	.L381
.L365:
	movq	%r13, -48(%rbp)
	cmpq	$15, %r13
	ja	.L382
	cmpq	$1, %r13
	jne	.L368
	movzbl	(%r14), %eax
	movb	%al, 16(%r12)
.L369:
	movq	%r13, 8(%r12)
	movb	$0, (%rdi,%r13)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L383
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L368:
	.cfi_restore_state
	testq	%r13, %r13
	je	.L369
	jmp	.L367
	.p2align 4,,10
	.p2align 3
.L382:
	movq	%r12, %rdi
	leaq	-48(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%r12)
	movq	%rax, %rdi
	movq	-48(%rbp), %rax
	movq	%rax, 16(%r12)
.L367:
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-48(%rbp), %r13
	movq	(%r12), %rdi
	jmp	.L369
.L381:
	leaq	.LC1(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L383:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5617:
	.size	_ZNK2v88internal6torque12AbstractType16ToExplicitStringB5cxx11Ev, .-_ZNK2v88internal6torque12AbstractType16ToExplicitStringB5cxx11Ev
	.section	.text._ZNK2v88internal6torque12AbstractType29GetGeneratedTNodeTypeNameImplB5cxx11Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal6torque12AbstractType29GetGeneratedTNodeTypeNameImplB5cxx11Ev
	.type	_ZNK2v88internal6torque12AbstractType29GetGeneratedTNodeTypeNameImplB5cxx11Ev, @function
_ZNK2v88internal6torque12AbstractType29GetGeneratedTNodeTypeNameImplB5cxx11Ev:
.LFB6517:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	addq	$16, %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%rdi, (%r12)
	movq	112(%rsi), %r14
	movq	120(%rsi), %r13
	movq	%r14, %rax
	addq	%r13, %rax
	je	.L385
	testq	%r14, %r14
	je	.L401
.L385:
	movq	%r13, -48(%rbp)
	cmpq	$15, %r13
	ja	.L402
	cmpq	$1, %r13
	jne	.L388
	movzbl	(%r14), %eax
	movb	%al, 16(%r12)
.L389:
	movq	%r13, 8(%r12)
	movb	$0, (%rdi,%r13)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L403
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L388:
	.cfi_restore_state
	testq	%r13, %r13
	je	.L389
	jmp	.L387
	.p2align 4,,10
	.p2align 3
.L402:
	movq	%r12, %rdi
	leaq	-48(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%r12)
	movq	%rax, %rdi
	movq	-48(%rbp), %rax
	movq	%rax, 16(%r12)
.L387:
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-48(%rbp), %r13
	movq	(%r12), %rdi
	jmp	.L389
.L401:
	leaq	.LC1(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L403:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6517:
	.size	_ZNK2v88internal6torque12AbstractType29GetGeneratedTNodeTypeNameImplB5cxx11Ev, .-_ZNK2v88internal6torque12AbstractType29GetGeneratedTNodeTypeNameImplB5cxx11Ev
	.section	.text._ZNK2v88internal6torque9ClassType29GetGeneratedTNodeTypeNameImplB5cxx11Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal6torque9ClassType29GetGeneratedTNodeTypeNameImplB5cxx11Ev
	.type	_ZNK2v88internal6torque9ClassType29GetGeneratedTNodeTypeNameImplB5cxx11Ev, @function
_ZNK2v88internal6torque9ClassType29GetGeneratedTNodeTypeNameImplB5cxx11Ev:
.LFB6589:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	addq	$16, %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%rdi, (%r12)
	movq	184(%rsi), %r14
	movq	192(%rsi), %r13
	movq	%r14, %rax
	addq	%r13, %rax
	je	.L405
	testq	%r14, %r14
	je	.L421
.L405:
	movq	%r13, -48(%rbp)
	cmpq	$15, %r13
	ja	.L422
	cmpq	$1, %r13
	jne	.L408
	movzbl	(%r14), %eax
	movb	%al, 16(%r12)
.L409:
	movq	%r13, 8(%r12)
	movb	$0, (%rdi,%r13)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L423
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L408:
	.cfi_restore_state
	testq	%r13, %r13
	je	.L409
	jmp	.L407
	.p2align 4,,10
	.p2align 3
.L422:
	movq	%r12, %rdi
	leaq	-48(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%r12)
	movq	%rax, %rdi
	movq	-48(%rbp), %rax
	movq	%rax, 16(%r12)
.L407:
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-48(%rbp), %r13
	movq	(%r12), %rdi
	jmp	.L409
.L421:
	leaq	.LC1(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L423:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6589:
	.size	_ZNK2v88internal6torque9ClassType29GetGeneratedTNodeTypeNameImplB5cxx11Ev, .-_ZNK2v88internal6torque9ClassType29GetGeneratedTNodeTypeNameImplB5cxx11Ev
	.section	.text._ZNK2v88internal6torque13AggregateType11MangledNameB5cxx11Ev,"axG",@progbits,_ZNK2v88internal6torque13AggregateType11MangledNameB5cxx11Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal6torque13AggregateType11MangledNameB5cxx11Ev
	.type	_ZNK2v88internal6torque13AggregateType11MangledNameB5cxx11Ev, @function
_ZNK2v88internal6torque13AggregateType11MangledNameB5cxx11Ev:
.LFB5712:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	addq	$16, %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%rdi, (%r12)
	movq	112(%rsi), %r14
	movq	120(%rsi), %r13
	movq	%r14, %rax
	addq	%r13, %rax
	je	.L425
	testq	%r14, %r14
	je	.L441
.L425:
	movq	%r13, -48(%rbp)
	cmpq	$15, %r13
	ja	.L442
	cmpq	$1, %r13
	jne	.L428
	movzbl	(%r14), %eax
	movb	%al, 16(%r12)
.L429:
	movq	%r13, 8(%r12)
	movb	$0, (%rdi,%r13)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L443
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L428:
	.cfi_restore_state
	testq	%r13, %r13
	je	.L429
	jmp	.L427
	.p2align 4,,10
	.p2align 3
.L442:
	movq	%r12, %rdi
	leaq	-48(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%r12)
	movq	%rax, %rdi
	movq	-48(%rbp), %rax
	movq	%rax, 16(%r12)
.L427:
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-48(%rbp), %r13
	movq	(%r12), %rdi
	jmp	.L429
.L441:
	leaq	.LC1(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L443:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5712:
	.size	_ZNK2v88internal6torque13AggregateType11MangledNameB5cxx11Ev, .-_ZNK2v88internal6torque13AggregateType11MangledNameB5cxx11Ev
	.section	.text._ZN2v88internal6torque14MessageBuilderD2Ev,"axG",@progbits,_ZN2v88internal6torque14MessageBuilderD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque14MessageBuilderD2Ev
	.type	_ZN2v88internal6torque14MessageBuilderD2Ev, @function
_ZN2v88internal6torque14MessageBuilderD2Ev:
.LFB4538:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA4538
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	addq	$16, %rbx
	subq	$8, %rsp
	call	_ZNK2v88internal6torque14MessageBuilder6ReportEv@PLT
	movq	-16(%rbx), %rdi
	cmpq	%rbx, %rdi
	je	.L444
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L444:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4538:
	.section	.gcc_except_table._ZN2v88internal6torque14MessageBuilderD2Ev,"aG",@progbits,_ZN2v88internal6torque14MessageBuilderD5Ev,comdat
.LLSDA4538:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE4538-.LLSDACSB4538
.LLSDACSB4538:
.LLSDACSE4538:
	.section	.text._ZN2v88internal6torque14MessageBuilderD2Ev,"axG",@progbits,_ZN2v88internal6torque14MessageBuilderD5Ev,comdat
	.size	_ZN2v88internal6torque14MessageBuilderD2Ev, .-_ZN2v88internal6torque14MessageBuilderD2Ev
	.weak	_ZN2v88internal6torque14MessageBuilderD1Ev
	.set	_ZN2v88internal6torque14MessageBuilderD1Ev,_ZN2v88internal6torque14MessageBuilderD2Ev
	.section	.text._ZN2v88internal6torque9SignatureD2Ev,"axG",@progbits,_ZN2v88internal6torque9SignatureD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque9SignatureD2Ev
	.type	_ZN2v88internal6torque9SignatureD2Ev, @function
_ZN2v88internal6torque9SignatureD2Ev:
.LFB6102:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	120(%rdi), %r13
	movq	112(%rdi), %r12
	cmpq	%r12, %r13
	je	.L448
	.p2align 4,,10
	.p2align 3
.L452:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L449
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r13, %r12
	jne	.L452
.L450:
	movq	112(%rbx), %r12
.L448:
	testq	%r12, %r12
	je	.L453
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L453:
	movq	64(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L454
	call	_ZdlPv@PLT
.L454:
	cmpb	$0, 24(%rbx)
	je	.L455
	movq	32(%rbx), %rdi
	leaq	48(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L455
	call	_ZdlPv@PLT
.L455:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L447
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L449:
	.cfi_restore_state
	addq	$32, %r12
	cmpq	%r12, %r13
	jne	.L452
	jmp	.L450
	.p2align 4,,10
	.p2align 3
.L447:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE6102:
	.size	_ZN2v88internal6torque9SignatureD2Ev, .-_ZN2v88internal6torque9SignatureD2Ev
	.weak	_ZN2v88internal6torque9SignatureD1Ev
	.set	_ZN2v88internal6torque9SignatureD1Ev,_ZN2v88internal6torque9SignatureD2Ev
	.section	.rodata._ZN2v88internal6torque10TypeOracle27IsImplicitlyConvertableFromEPKNS1_4TypeES5_.str1.8,"aMS",@progbits,1
	.align 8
.LC6:
	.string	"cannot create std::vector larger than max_size()"
	.section	.text._ZN2v88internal6torque10TypeOracle27IsImplicitlyConvertableFromEPKNS1_4TypeES5_,"axG",@progbits,_ZN2v88internal6torque10TypeOracle27IsImplicitlyConvertableFromEPKNS1_4TypeES5_,comdat
	.p2align 4
	.weak	_ZN2v88internal6torque10TypeOracle27IsImplicitlyConvertableFromEPKNS1_4TypeES5_
	.type	_ZN2v88internal6torque10TypeOracle27IsImplicitlyConvertableFromEPKNS1_4TypeES5_, @function
_ZN2v88internal6torque10TypeOracle27IsImplicitlyConvertableFromEPKNS1_4TypeES5_:
.LFB6505:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6505
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	-80(%rbp), %rbx
	subq	$136, %rsp
	movq	%rdi, -136(%rbp)
	leaq	-128(%rbp), %rdi
	movq	%rsi, -168(%rbp)
	leaq	-96(%rbp), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rbx, -96(%rbp)
	movabsq	$8317707897189397062, %rax
	movq	%rax, -80(%rbp)
	movl	$1886938484, -72(%rbp)
	movb	$114, -68(%rbp)
	movq	$13, -88(%rbp)
	movb	$0, -67(%rbp)
.LEHB33:
	call	_ZN2v88internal6torque12Declarations13LookupGenericERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
.LEHE33:
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L465
	call	_ZdlPv@PLT
.L465:
	movq	-120(%rbp), %rax
	movq	-128(%rbp), %r13
	movq	%rax, -144(%rbp)
	cmpq	%r13, %rax
	je	.L466
	movq	-136(%rbp), %xmm1
	movhps	-168(%rbp), %xmm1
	movaps	%xmm1, -160(%rbp)
	.p2align 4,,10
	.p2align 3
.L488:
	movl	$16, %edi
	movq	0(%r13), %r14
.LEHB34:
	call	_Znwm@PLT
.LEHE34:
	movdqa	-160(%rbp), %xmm0
	movq	%rax, %r12
	xorl	%ebx, %ebx
	xorl	%edi, %edi
	movups	%xmm0, (%rax)
	.p2align 4,,10
	.p2align 3
.L467:
	movq	(%r12,%rbx), %r15
.LEHB35:
	call	_ZN2v84base10hash_valueEm@PLT
	movq	%r15, %rdi
	movq	%rax, -136(%rbp)
	call	_ZN2v84base10hash_valueEm@PLT
	movq	%rax, %rsi
	xorl	%edi, %edi
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	-136(%rbp), %rsi
	movq	%rax, %rdi
	call	_ZN2v84base12hash_combineEmm@PLT
.LEHE35:
	addq	$8, %rbx
	movq	%rax, %rdi
	cmpq	$16, %rbx
	jne	.L467
	movq	120(%r14), %r8
	xorl	%edx, %edx
	divq	%r8
	movq	112(%r14), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L530
	movq	(%rax), %rcx
	movq	40(%rcx), %rsi
.L476:
	cmpq	%rsi, %rdi
	je	.L532
.L472:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L530
	movq	40(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%r8
	cmpq	%rdx, %r9
	je	.L476
.L530:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L477:
	addq	$8, %r13
	cmpq	%r13, -144(%rbp)
	jne	.L488
	movq	-128(%rbp), %r13
.L466:
	testq	%r13, %r13
	je	.L489
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L489:
	xorl	%eax, %eax
.L464:
	movq	-56(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L533
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L532:
	.cfi_restore_state
	movq	8(%rcx), %rdx
	movq	16(%rcx), %rax
	subq	%rdx, %rax
	cmpq	$16, %rax
	jne	.L472
	movq	(%r12), %rax
	movq	8(%r12), %rsi
	xorq	(%rdx), %rax
	xorq	8(%rdx), %rsi
	orq	%rax, %rsi
	jne	.L472
	movq	%r12, %rdi
	movq	32(%rcx), %rbx
	call	_ZdlPv@PLT
	movl	$8, %edi
.LEHB36:
	call	_Znwm@PLT
.LEHE36:
	movabsq	$1152921504606846975, %rcx
	movq	%rax, %r12
	movq	-168(%rbp), %rax
	movq	%rax, (%r12)
	movq	256(%rbx), %rax
	movq	288(%rbx), %rdx
	movq	264(%rbx), %r14
	leaq	(%rax,%rdx,8), %r15
	movq	%r14, %rbx
	subq	%r15, %rbx
	movq	%rbx, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	ja	.L534
	testq	%rax, %rax
	je	.L479
	movq	%rbx, %rdi
.LEHB37:
	call	_Znwm@PLT
	movq	%rax, %rdi
	cmpq	%r15, %r14
	je	.L529
.L481:
	movq	%rbx, %rdx
	movq	%r15, %rsi
	call	memcpy@PLT
	movq	%rax, %rdi
.L529:
	cmpq	$8, %rbx
	je	.L482
	call	_ZdlPv@PLT
	jmp	.L530
	.p2align 4,,10
	.p2align 3
.L479:
	xorl	%edi, %edi
	cmpq	%r15, %r14
	jne	.L481
	cmpq	$8, %rbx
	jne	.L530
	.p2align 4,,10
	.p2align 3
.L482:
	movq	(%rdi), %rbx
	movq	(%r12), %r14
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	cmpq	%r14, %rbx
	jne	.L477
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L486
	call	_ZdlPv@PLT
.L486:
	movl	$1, %eax
	jmp	.L464
.L534:
	leaq	.LC6(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.LEHE37:
.L533:
	call	__stack_chk_fail@PLT
.L498:
	endbr64
	movq	%rax, %rbx
	jmp	.L471
.L500:
	endbr64
.L531:
	movq	%rax, %rbx
.L485:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L471:
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L492
	call	_ZdlPv@PLT
.L492:
	movq	%rbx, %rdi
.LEHB38:
	call	_Unwind_Resume@PLT
.L497:
	endbr64
	movq	%rax, %r12
	jmp	.L490
.L499:
	endbr64
	jmp	.L531
.L490:
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L491
	call	_ZdlPv@PLT
.L491:
	movq	%r12, %rdi
	call	_Unwind_Resume@PLT
.LEHE38:
	.cfi_endproc
.LFE6505:
	.section	.gcc_except_table._ZN2v88internal6torque10TypeOracle27IsImplicitlyConvertableFromEPKNS1_4TypeES5_,"aG",@progbits,_ZN2v88internal6torque10TypeOracle27IsImplicitlyConvertableFromEPKNS1_4TypeES5_,comdat
.LLSDA6505:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6505-.LLSDACSB6505
.LLSDACSB6505:
	.uleb128 .LEHB33-.LFB6505
	.uleb128 .LEHE33-.LEHB33
	.uleb128 .L497-.LFB6505
	.uleb128 0
	.uleb128 .LEHB34-.LFB6505
	.uleb128 .LEHE34-.LEHB34
	.uleb128 .L498-.LFB6505
	.uleb128 0
	.uleb128 .LEHB35-.LFB6505
	.uleb128 .LEHE35-.LEHB35
	.uleb128 .L499-.LFB6505
	.uleb128 0
	.uleb128 .LEHB36-.LFB6505
	.uleb128 .LEHE36-.LEHB36
	.uleb128 .L498-.LFB6505
	.uleb128 0
	.uleb128 .LEHB37-.LFB6505
	.uleb128 .LEHE37-.LEHB37
	.uleb128 .L500-.LFB6505
	.uleb128 0
	.uleb128 .LEHB38-.LFB6505
	.uleb128 .LEHE38-.LEHB38
	.uleb128 0
	.uleb128 0
.LLSDACSE6505:
	.section	.text._ZN2v88internal6torque10TypeOracle27IsImplicitlyConvertableFromEPKNS1_4TypeES5_,"axG",@progbits,_ZN2v88internal6torque10TypeOracle27IsImplicitlyConvertableFromEPKNS1_4TypeES5_,comdat
	.size	_ZN2v88internal6torque10TypeOracle27IsImplicitlyConvertableFromEPKNS1_4TypeES5_, .-_ZN2v88internal6torque10TypeOracle27IsImplicitlyConvertableFromEPKNS1_4TypeES5_
	.section	.text._ZNK2v88internal6torque4Type14ClassSupertypeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal6torque4Type14ClassSupertypeEv
	.type	_ZNK2v88internal6torque4Type14ClassSupertypeEv, @function
_ZNK2v88internal6torque4Type14ClassSupertypeEv:
.LFB6511:
	.cfi_startproc
	endbr64
.L538:
	cmpl	$5, 8(%rdi)
	jne	.L536
	movq	%rdi, %rdx
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L536:
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	jne	.L538
	xorl	%edx, %edx
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE6511:
	.size	_ZNK2v88internal6torque4Type14ClassSupertypeEv, .-_ZNK2v88internal6torque4Type14ClassSupertypeEv
	.section	.text._ZNK2v88internal6torque4Type5DepthEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal6torque4Type5DepthEv
	.type	_ZNK2v88internal6torque4Type5DepthEv, @function
_ZNK2v88internal6torque4Type5DepthEv:
.LFB6513:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	xorl	%r8d, %r8d
	testq	%rax, %rax
	je	.L540
	.p2align 4,,10
	.p2align 3
.L542:
	movq	16(%rax), %rax
	addl	$1, %r8d
	testq	%rax, %rax
	jne	.L542
.L540:
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE6513:
	.size	_ZNK2v88internal6torque4Type5DepthEv, .-_ZNK2v88internal6torque4Type5DepthEv
	.section	.text._ZNK2v88internal6torque4Type14IsAbstractNameERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal6torque4Type14IsAbstractNameERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZNK2v88internal6torque4Type14IsAbstractNameERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZNK2v88internal6torque4Type14IsAbstractNameERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB6514:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpl	$1, 8(%rdi)
	je	.L555
.L552:
	ret
	.p2align 4,,10
	.p2align 3
.L555:
	movq	88(%rdi), %rdx
	cmpq	8(%rsi), %rdx
	jne	.L552
	movl	$1, %eax
	testq	%rdx, %rdx
	je	.L552
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	80(%rdi), %rdi
	movq	(%rsi), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	memcmp@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	testl	%eax, %eax
	sete	%al
	ret
	.cfi_endproc
.LFE6514:
	.size	_ZNK2v88internal6torque4Type14IsAbstractNameERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZNK2v88internal6torque4Type14IsAbstractNameERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.text._ZNK2v88internal6torque13AggregateType8HasFieldERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal6torque13AggregateType8HasFieldERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZNK2v88internal6torque13AggregateType8HasFieldERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZNK2v88internal6torque13AggregateType8HasFieldERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB6555:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
.L562:
	cmpb	$0, 72(%r14)
	jne	.L557
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*96(%rax)
.L557:
	movq	80(%r14), %rbx
	movq	88(%r14), %r13
	cmpq	%r13, %rbx
	je	.L558
	movq	8(%r15), %r12
	jmp	.L561
	.p2align 4,,10
	.p2align 3
.L559:
	addq	$104, %rbx
	cmpq	%rbx, %r13
	je	.L558
.L561:
	cmpq	%r12, 56(%rbx)
	jne	.L559
	testq	%r12, %r12
	je	.L564
	movq	48(%rbx), %rdi
	movq	(%r15), %rsi
	movq	%r12, %rdx
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L559
.L564:
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L558:
	.cfi_restore_state
	movq	16(%r14), %r14
	testq	%r14, %r14
	je	.L566
	cmpl	$5, 8(%r14)
	je	.L562
.L566:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE6555:
	.size	_ZNK2v88internal6torque13AggregateType8HasFieldERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZNK2v88internal6torque13AggregateType8HasFieldERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.text._ZN2v88internal6torque10StructType17MatchUnaryGenericEPKNS1_4TypeEPNS1_17GenericStructTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque10StructType17MatchUnaryGenericEPKNS1_4TypeEPNS1_17GenericStructTypeE
	.type	_ZN2v88internal6torque10StructType17MatchUnaryGenericEPKNS1_4TypeEPNS1_17GenericStructTypeE, @function
_ZN2v88internal6torque10StructType17MatchUnaryGenericEPKNS1_4TypeEPNS1_17GenericStructTypeE:
.LFB6562:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L570
	cmpl	$4, 8(%rdi)
	jne	.L570
	cmpb	$0, 176(%rdi)
	jne	.L571
.L573:
	xorl	%edx, %edx
	xorl	%eax, %eax
.L572:
	andl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L571:
	cmpq	184(%rdi), %rsi
	jne	.L573
	movq	192(%rdi), %rdx
	movq	200(%rdi), %rax
	subq	%rdx, %rax
	cmpq	$8, %rax
	jne	.L573
	movq	(%rdx), %rdx
	movl	$1, %eax
	jmp	.L572
	.p2align 4,,10
	.p2align 3
.L570:
	xorl	%edx, %edx
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE6562:
	.size	_ZN2v88internal6torque10StructType17MatchUnaryGenericEPKNS1_4TypeEPNS1_17GenericStructTypeE, .-_ZN2v88internal6torque10StructType17MatchUnaryGenericEPKNS1_4TypeEPNS1_17GenericStructTypeE
	.section	.text._ZN2v88internal6torque10StructType17MatchUnaryGenericEPKS2_PNS1_17GenericStructTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque10StructType17MatchUnaryGenericEPKS2_PNS1_17GenericStructTypeE
	.type	_ZN2v88internal6torque10StructType17MatchUnaryGenericEPKS2_PNS1_17GenericStructTypeE, @function
_ZN2v88internal6torque10StructType17MatchUnaryGenericEPKS2_PNS1_17GenericStructTypeE:
.LFB6563:
	.cfi_startproc
	endbr64
	cmpb	$0, 176(%rdi)
	je	.L581
	cmpq	%rsi, 184(%rdi)
	je	.L582
.L581:
	xorl	%edx, %edx
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L582:
	movq	192(%rdi), %rdx
	movq	200(%rdi), %rax
	subq	%rdx, %rax
	cmpq	$8, %rax
	jne	.L581
	movq	(%rdx), %rdx
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE6563:
	.size	_ZN2v88internal6torque10StructType17MatchUnaryGenericEPKS2_PNS1_17GenericStructTypeE, .-_ZN2v88internal6torque10StructType17MatchUnaryGenericEPKS2_PNS1_17GenericStructTypeE
	.section	.text._ZNK2v88internal6torque9ClassType18AllowInstantiationEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal6torque9ClassType18AllowInstantiationEv
	.type	_ZNK2v88internal6torque9ClassType18AllowInstantiationEv, @function
_ZNK2v88internal6torque9ClassType18AllowInstantiationEv:
.LFB6592:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movl	176(%rdi), %edx
	testb	$1, %dl
	jne	.L594
.L584:
	movl	%edx, %eax
	shrl	$5, %eax
	andl	$1, %eax
	andl	$16, %edx
	movl	$1, %edx
	cmove	%edx, %eax
.L583:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L594:
	.cfi_restore_state
	movq	%rdi, %rbx
	movq	104(%rdi), %rdi
	call	_ZNK2v88internal6torque9Namespace18IsDefaultNamespaceEv@PLT
	testb	%al, %al
	je	.L583
	movl	176(%rbx), %edx
	jmp	.L584
	.cfi_endproc
.LFE6592:
	.size	_ZNK2v88internal6torque9ClassType18AllowInstantiationEv, .-_ZNK2v88internal6torque9ClassType18AllowInstantiationEv
	.section	.text.unlikely._ZNK2v88internal6torque9Signature14HasSameTypesAsERKS2_NS1_13ParameterModeE,"ax",@progbits
	.align 2
.LCOLDB7:
	.section	.text._ZNK2v88internal6torque9Signature14HasSameTypesAsERKS2_NS1_13ParameterModeE,"ax",@progbits
.LHOTB7:
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal6torque9Signature14HasSameTypesAsERKS2_NS1_13ParameterModeE
	.type	_ZNK2v88internal6torque9Signature14HasSameTypesAsERKS2_NS1_13ParameterModeE, @function
_ZNK2v88internal6torque9Signature14HasSameTypesAsERKS2_NS1_13ParameterModeE:
.LFB6620:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6620
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	72(%rdi), %rax
	movq	64(%rdi), %rsi
	movl	%edx, -64(%rbp)
	movq	%rax, %r13
	subq	%rsi, %r13
	movq	%r13, %rdx
	sarq	$3, %rdx
	je	.L626
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L665
	movq	%r13, %rdi
.LEHB39:
	call	_Znwm@PLT
.LEHE39:
	movq	64(%rbx), %rsi
	movq	%rax, %r15
	movq	72(%rbx), %rax
	movq	%rax, %r13
	subq	%rsi, %r13
.L596:
	cmpq	%rsi, %rax
	je	.L598
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	memmove@PLT
.L598:
	movq	72(%r12), %rax
	movq	64(%r12), %rsi
	movq	%rax, %r14
	subq	%rsi, %r14
	movq	%r14, %rdx
	sarq	$3, %rdx
	je	.L627
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L666
	movq	%r14, %rdi
.LEHB40:
	call	_Znwm@PLT
.LEHE40:
	movq	%rax, -56(%rbp)
	movq	72(%r12), %rax
	movq	64(%r12), %rsi
	movq	%rax, %r14
	subq	%rsi, %r14
.L599:
	cmpq	%rsi, %rax
	je	.L601
	movq	-56(%rbp), %rdi
	movq	%r14, %rdx
	call	memmove@PLT
.L601:
	cmpl	$1, -64(%rbp)
	je	.L667
.L602:
	xorl	%r8d, %r8d
	cmpq	%r14, %r13
	je	.L668
.L613:
	cmpq	$0, -56(%rbp)
	jne	.L616
.L619:
	testq	%r15, %r15
	je	.L595
	movq	%r15, %rdi
	movb	%r8b, -56(%rbp)
	call	_ZdlPv@PLT
	movzbl	-56(%rbp), %r8d
.L595:
	addq	$40, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L669:
	.cfi_restore_state
	movq	-56(%rbp), %rsi
	movq	%r13, %rdx
	movq	%r15, %rdi
	movb	%r8b, -64(%rbp)
	call	memcmp@PLT
	movl	$0, %r8d
	testl	%eax, %eax
	jne	.L616
	movzbl	88(%r12), %eax
	cmpb	%al, 88(%rbx)
	je	.L625
	.p2align 4,,10
	.p2align 3
.L616:
	movq	-56(%rbp), %rdi
	movb	%r8b, -64(%rbp)
	call	_ZdlPv@PLT
	movzbl	-64(%rbp), %r8d
	jmp	.L619
	.p2align 4,,10
	.p2align 3
.L668:
	testq	%r13, %r13
	jne	.L669
	movzbl	88(%r12), %eax
	cmpb	%al, 88(%rbx)
	jne	.L613
.L625:
	movq	104(%r12), %rax
	xorl	%r8d, %r8d
	cmpq	%rax, 104(%rbx)
	jne	.L613
	movq	120(%rbx), %r14
	movq	112(%rbx), %r13
	movq	120(%r12), %rax
	movq	112(%r12), %rbx
	movq	%r14, %rdx
	subq	%rbx, %rax
	subq	%r13, %rdx
	cmpq	%rdx, %rax
	jne	.L613
	cmpq	%r14, %r13
	je	.L634
	addq	$8, %rbx
	.p2align 4,,10
	.p2align 3
.L618:
	movq	8(%r13), %rdi
	movq	16(%r13), %rdx
	movq	(%rbx), %rsi
	movq	8(%rbx), %rax
	subq	%rdi, %rdx
	subq	%rsi, %rax
	cmpq	%rax, %rdx
	je	.L670
.L636:
	xorl	%r8d, %r8d
	jmp	.L613
	.p2align 4,,10
	.p2align 3
.L626:
	xorl	%r15d, %r15d
	jmp	.L596
	.p2align 4,,10
	.p2align 3
.L627:
	movq	$0, -56(%rbp)
	jmp	.L599
	.p2align 4,,10
	.p2align 3
.L667:
	movq	64(%rbx), %rax
	movq	96(%rbx), %rdx
	movq	72(%rbx), %rcx
	leaq	(%rax,%rdx,8), %r14
	movq	%rcx, -72(%rbp)
	subq	%r14, %rcx
	movq	%rcx, %rax
	movq	%rcx, %r13
	sarq	$3, %rax
	testq	%rcx, %rcx
	js	.L671
	testq	%rax, %rax
	je	.L628
	movq	%rcx, %rdi
.LEHB41:
	call	_Znwm@PLT
.LEHE41:
	movq	%rax, -64(%rbp)
.L604:
	cmpq	-72(%rbp), %r14
	je	.L607
	movq	-64(%rbp), %rdi
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
.L607:
	testq	%r15, %r15
	je	.L608
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L608:
	movq	64(%r12), %rax
	movq	96(%r12), %rdx
	movq	72(%r12), %rcx
	leaq	(%rax,%rdx,8), %r15
	movq	%rcx, -72(%rbp)
	subq	%r15, %rcx
	movq	%rcx, %rax
	movq	%rcx, %r14
	sarq	$3, %rax
	testq	%rcx, %rcx
	js	.L672
	testq	%rax, %rax
	je	.L629
	movq	%rcx, %rdi
.LEHB42:
	call	_Znwm@PLT
.LEHE42:
	movq	%rax, %rcx
.L610:
	cmpq	%r15, -72(%rbp)
	je	.L611
	movq	%rcx, %rdi
	movq	%r14, %rdx
	movq	%r15, %rsi
	call	memcpy@PLT
	movq	%rax, %rcx
.L611:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L612
	movq	%rcx, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rcx
.L612:
	movq	%rcx, -56(%rbp)
	movq	-64(%rbp), %r15
	jmp	.L602
	.p2align 4,,10
	.p2align 3
.L628:
	movq	$0, -64(%rbp)
	jmp	.L604
	.p2align 4,,10
	.p2align 3
.L670:
	testq	%rdx, %rdx
	jne	.L673
.L617:
	addq	$32, %r13
	addq	$32, %rbx
	cmpq	%r13, %r14
	jne	.L618
.L634:
	movl	$1, %r8d
	jmp	.L613
	.p2align 4,,10
	.p2align 3
.L673:
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L636
	jmp	.L617
	.p2align 4,,10
	.p2align 3
.L629:
	xorl	%ecx, %ecx
	jmp	.L610
.L666:
.LEHB43:
	call	_ZSt17__throw_bad_allocv@PLT
.LEHE43:
.L665:
.LEHB44:
	call	_ZSt17__throw_bad_allocv@PLT
.LEHE44:
.L671:
	leaq	.LC6(%rip), %rdi
.LEHB45:
	call	_ZSt20__throw_length_errorPKc@PLT
.LEHE45:
.L672:
	leaq	.LC6(%rip), %rdi
.LEHB46:
	call	_ZSt20__throw_length_errorPKc@PLT
.LEHE46:
.L639:
	endbr64
	movq	%rax, %rbx
	jmp	.L606
.L638:
	endbr64
	movq	%rax, %rbx
	jmp	.L623
.L641:
	endbr64
	movq	%rax, %rbx
	jmp	.L605
.L640:
	endbr64
	movq	%rax, %rbx
	jmp	.L621
	.section	.gcc_except_table._ZNK2v88internal6torque9Signature14HasSameTypesAsERKS2_NS1_13ParameterModeE,"a",@progbits
.LLSDA6620:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6620-.LLSDACSB6620
.LLSDACSB6620:
	.uleb128 .LEHB39-.LFB6620
	.uleb128 .LEHE39-.LEHB39
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB40-.LFB6620
	.uleb128 .LEHE40-.LEHB40
	.uleb128 .L638-.LFB6620
	.uleb128 0
	.uleb128 .LEHB41-.LFB6620
	.uleb128 .LEHE41-.LEHB41
	.uleb128 .L641-.LFB6620
	.uleb128 0
	.uleb128 .LEHB42-.LFB6620
	.uleb128 .LEHE42-.LEHB42
	.uleb128 .L639-.LFB6620
	.uleb128 0
	.uleb128 .LEHB43-.LFB6620
	.uleb128 .LEHE43-.LEHB43
	.uleb128 .L638-.LFB6620
	.uleb128 0
	.uleb128 .LEHB44-.LFB6620
	.uleb128 .LEHE44-.LEHB44
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB45-.LFB6620
	.uleb128 .LEHE45-.LEHB45
	.uleb128 .L640-.LFB6620
	.uleb128 0
	.uleb128 .LEHB46-.LFB6620
	.uleb128 .LEHE46-.LEHB46
	.uleb128 .L639-.LFB6620
	.uleb128 0
.LLSDACSE6620:
	.section	.text._ZNK2v88internal6torque9Signature14HasSameTypesAsERKS2_NS1_13ParameterModeE
	.cfi_endproc
	.section	.text.unlikely._ZNK2v88internal6torque9Signature14HasSameTypesAsERKS2_NS1_13ParameterModeE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6620
	.type	_ZNK2v88internal6torque9Signature14HasSameTypesAsERKS2_NS1_13ParameterModeE.cold, @function
_ZNK2v88internal6torque9Signature14HasSameTypesAsERKS2_NS1_13ParameterModeE.cold:
.LFSB6620:
.L605:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	%r15, -64(%rbp)
.L606:
	cmpq	$0, -56(%rbp)
	je	.L622
	movq	-56(%rbp), %rdi
	call	_ZdlPv@PLT
.L622:
	movq	-64(%rbp), %r15
.L623:
	testq	%r15, %r15
	je	.L624
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L624:
	movq	%rbx, %rdi
.LEHB47:
	call	_Unwind_Resume@PLT
.LEHE47:
.L621:
	movq	%r15, -64(%rbp)
	jmp	.L606
	.cfi_endproc
.LFE6620:
	.section	.gcc_except_table._ZNK2v88internal6torque9Signature14HasSameTypesAsERKS2_NS1_13ParameterModeE
.LLSDAC6620:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6620-.LLSDACSBC6620
.LLSDACSBC6620:
	.uleb128 .LEHB47-.LCOLDB7
	.uleb128 .LEHE47-.LEHB47
	.uleb128 0
	.uleb128 0
.LLSDACSEC6620:
	.section	.text.unlikely._ZNK2v88internal6torque9Signature14HasSameTypesAsERKS2_NS1_13ParameterModeE
	.section	.text._ZNK2v88internal6torque9Signature14HasSameTypesAsERKS2_NS1_13ParameterModeE
	.size	_ZNK2v88internal6torque9Signature14HasSameTypesAsERKS2_NS1_13ParameterModeE, .-_ZNK2v88internal6torque9Signature14HasSameTypesAsERKS2_NS1_13ParameterModeE
	.section	.text.unlikely._ZNK2v88internal6torque9Signature14HasSameTypesAsERKS2_NS1_13ParameterModeE
	.size	_ZNK2v88internal6torque9Signature14HasSameTypesAsERKS2_NS1_13ParameterModeE.cold, .-_ZNK2v88internal6torque9Signature14HasSameTypesAsERKS2_NS1_13ParameterModeE.cold
.LCOLDE7:
	.section	.text._ZNK2v88internal6torque9Signature14HasSameTypesAsERKS2_NS1_13ParameterModeE
.LHOTE7:
	.section	.text._ZN2v88internal6torque16IsAssignableFromEPKNS1_4TypeES4_,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal6torque16IsAssignableFromEPKNS1_4TypeES4_
	.type	_ZN2v88internal6torque16IsAssignableFromEPKNS1_4TypeES4_, @function
_ZN2v88internal6torque16IsAssignableFromEPKNS1_4TypeES4_:
.LFB6621:
	.cfi_startproc
	endbr64
	cmpq	%rsi, %rdi
	je	.L679
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	(%rsi), %rax
	movq	%rsi, %r12
	movq	%rdi, %rsi
	movq	%r12, %rdi
	call	*16(%rax)
	testb	%al, %al
	je	.L682
	popq	%r12
	movl	$1, %eax
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L682:
	.cfi_restore_state
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal6torque10TypeOracle27IsImplicitlyConvertableFromEPKNS1_4TypeES5_
	.p2align 4,,10
	.p2align 3
.L679:
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE6621:
	.size	_ZN2v88internal6torque16IsAssignableFromEPKNS1_4TypeES4_, .-_ZN2v88internal6torque16IsAssignableFromEPKNS1_4TypeES4_
	.section	.text.unlikely._ZN2v88internal6torqueltERKNS1_4TypeES4_,"ax",@progbits
.LCOLDB8:
	.section	.text._ZN2v88internal6torqueltERKNS1_4TypeES4_,"ax",@progbits
.LHOTB8:
	.p2align 4
	.globl	_ZN2v88internal6torqueltERKNS1_4TypeES4_
	.type	_ZN2v88internal6torqueltERKNS1_4TypeES4_, @function
_ZN2v88internal6torqueltERKNS1_4TypeES4_:
.LFB6622:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6622
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	leaq	-96(%rbp), %rdi
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
.LEHB48:
	call	*24(%rax)
.LEHE48:
	movq	(%r12), %rax
	leaq	-128(%rbp), %rdi
	movq	%r12, %rsi
.LEHB49:
	call	*24(%rax)
.LEHE49:
	movq	-120(%rbp), %rbx
	movq	-88(%rbp), %r14
	movq	-96(%rbp), %r13
	movq	-128(%rbp), %r15
	cmpq	%r14, %rbx
	movq	%r14, %rdx
	cmovbe	%rbx, %rdx
	testq	%rdx, %rdx
	je	.L684
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	memcmp@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	jne	.L701
.L684:
	subq	%r14, %rbx
	xorl	%r12d, %r12d
	cmpq	$2147483647, %rbx
	jg	.L685
	cmpq	$-2147483648, %rbx
	jl	.L692
	shrl	$31, %ebx
	movl	%ebx, %r12d
.L685:
	leaq	-112(%rbp), %rax
	cmpq	%rax, %r15
	je	.L686
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	movq	-96(%rbp), %r13
.L686:
	leaq	-80(%rbp), %rax
	cmpq	%rax, %r13
	je	.L683
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L683:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L702
	addq	$88, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L701:
	.cfi_restore_state
	shrl	$31, %r12d
	jmp	.L685
	.p2align 4,,10
	.p2align 3
.L692:
	movl	$1, %r12d
	jmp	.L685
.L702:
	call	__stack_chk_fail@PLT
.L693:
	endbr64
	movq	%rax, %r12
	jmp	.L688
	.section	.gcc_except_table._ZN2v88internal6torqueltERKNS1_4TypeES4_,"a",@progbits
.LLSDA6622:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6622-.LLSDACSB6622
.LLSDACSB6622:
	.uleb128 .LEHB48-.LFB6622
	.uleb128 .LEHE48-.LEHB48
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB49-.LFB6622
	.uleb128 .LEHE49-.LEHB49
	.uleb128 .L693-.LFB6622
	.uleb128 0
.LLSDACSE6622:
	.section	.text._ZN2v88internal6torqueltERKNS1_4TypeES4_
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torqueltERKNS1_4TypeES4_
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6622
	.type	_ZN2v88internal6torqueltERKNS1_4TypeES4_.cold, @function
_ZN2v88internal6torqueltERKNS1_4TypeES4_.cold:
.LFSB6622:
.L688:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L689
	call	_ZdlPv@PLT
.L689:
	movq	%r12, %rdi
.LEHB50:
	call	_Unwind_Resume@PLT
.LEHE50:
	.cfi_endproc
.LFE6622:
	.section	.gcc_except_table._ZN2v88internal6torqueltERKNS1_4TypeES4_
.LLSDAC6622:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6622-.LLSDACSBC6622
.LLSDACSBC6622:
	.uleb128 .LEHB50-.LCOLDB8
	.uleb128 .LEHE50-.LEHB50
	.uleb128 0
	.uleb128 0
.LLSDACSEC6622:
	.section	.text.unlikely._ZN2v88internal6torqueltERKNS1_4TypeES4_
	.section	.text._ZN2v88internal6torqueltERKNS1_4TypeES4_
	.size	_ZN2v88internal6torqueltERKNS1_4TypeES4_, .-_ZN2v88internal6torqueltERKNS1_4TypeES4_
	.section	.text.unlikely._ZN2v88internal6torqueltERKNS1_4TypeES4_
	.size	_ZN2v88internal6torqueltERKNS1_4TypeES4_.cold, .-_ZN2v88internal6torqueltERKNS1_4TypeES4_.cold
.LCOLDE8:
	.section	.text._ZN2v88internal6torqueltERKNS1_4TypeES4_
.LHOTE8:
	.section	.text.unlikely._ZN2v88internal6torque11VisitResult11NeverResultEv,"ax",@progbits
	.align 2
.LCOLDB9:
	.section	.text._ZN2v88internal6torque11VisitResult11NeverResultEv,"ax",@progbits
.LHOTB9:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque11VisitResult11NeverResultEv
	.type	_ZN2v88internal6torque11VisitResult11NeverResultEv, @function
_ZN2v88internal6torque11VisitResult11NeverResultEv:
.LFB6629:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6629
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, 64(%rdi)
	movups	%xmm0, (%rdi)
	movups	%xmm0, 16(%rdi)
	movups	%xmm0, 32(%rdi)
	movups	%xmm0, 48(%rdi)
.LEHB51:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_10TypeOracleEEERPNT_5ScopeEv@PLT
.LEHE51:
	leaq	-64(%rbp), %r13
	leaq	-80(%rbp), %rdi
	movb	$114, -60(%rbp)
	movq	%r13, -80(%rbp)
	movl	$1702258030, -64(%rbp)
	movq	$5, -72(%rbp)
	movb	$0, -59(%rbp)
.LEHB52:
	call	_ZN2v88internal6torque12Declarations16LookupGlobalTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
.LEHE52:
	movq	-80(%rbp), %rdi
	movq	%rax, %rbx
	cmpq	%r13, %rdi
	je	.L704
	call	_ZdlPv@PLT
.L704:
	movq	%rbx, (%r12)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L714
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L714:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
.L711:
	endbr64
	movq	%rax, %rbx
	jmp	.L705
.L710:
	endbr64
	movq	%rax, %r13
	jmp	.L707
	.section	.gcc_except_table._ZN2v88internal6torque11VisitResult11NeverResultEv,"a",@progbits
.LLSDA6629:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6629-.LLSDACSB6629
.LLSDACSB6629:
	.uleb128 .LEHB51-.LFB6629
	.uleb128 .LEHE51-.LEHB51
	.uleb128 .L710-.LFB6629
	.uleb128 0
	.uleb128 .LEHB52-.LFB6629
	.uleb128 .LEHE52-.LEHB52
	.uleb128 .L711-.LFB6629
	.uleb128 0
.LLSDACSE6629:
	.section	.text._ZN2v88internal6torque11VisitResult11NeverResultEv
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque11VisitResult11NeverResultEv
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6629
	.type	_ZN2v88internal6torque11VisitResult11NeverResultEv.cold, @function
_ZN2v88internal6torque11VisitResult11NeverResultEv.cold:
.LFSB6629:
.L705:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movq	-80(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L706
	call	_ZdlPv@PLT
.L706:
	movq	%rbx, %r13
.L707:
	cmpb	$0, 8(%r12)
	je	.L708
	movq	16(%r12), %rdi
	addq	$32, %r12
	cmpq	%r12, %rdi
	je	.L708
	call	_ZdlPv@PLT
.L708:
	movq	%r13, %rdi
.LEHB53:
	call	_Unwind_Resume@PLT
.LEHE53:
	.cfi_endproc
.LFE6629:
	.section	.gcc_except_table._ZN2v88internal6torque11VisitResult11NeverResultEv
.LLSDAC6629:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6629-.LLSDACSBC6629
.LLSDACSBC6629:
	.uleb128 .LEHB53-.LCOLDB9
	.uleb128 .LEHE53-.LEHB53
	.uleb128 0
	.uleb128 0
.LLSDACSEC6629:
	.section	.text.unlikely._ZN2v88internal6torque11VisitResult11NeverResultEv
	.section	.text._ZN2v88internal6torque11VisitResult11NeverResultEv
	.size	_ZN2v88internal6torque11VisitResult11NeverResultEv, .-_ZN2v88internal6torque11VisitResult11NeverResultEv
	.section	.text.unlikely._ZN2v88internal6torque11VisitResult11NeverResultEv
	.size	_ZN2v88internal6torque11VisitResult11NeverResultEv.cold, .-_ZN2v88internal6torque11VisitResult11NeverResultEv.cold
.LCOLDE9:
	.section	.text._ZN2v88internal6torque11VisitResult11NeverResultEv
.LHOTE9:
	.section	.text._ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EED2Ev,"axG",@progbits,_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EED2Ev
	.type	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EED2Ev, @function
_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EED2Ev:
.LFB6950:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	8(%rdi), %rbx
	movq	(%rdi), %r12
	cmpq	%r12, %rbx
	je	.L716
	.p2align 4,,10
	.p2align 3
.L720:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L717
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L720
.L718:
	movq	0(%r13), %r12
.L716:
	testq	%r12, %r12
	je	.L715
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L717:
	.cfi_restore_state
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L720
	jmp	.L718
	.p2align 4,,10
	.p2align 3
.L715:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE6950:
	.size	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EED2Ev, .-_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EED2Ev
	.weak	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EED1Ev
	.set	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EED1Ev,_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EED2Ev
	.section	.text._ZN2v84base8internal15OptionalStorageINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEELb0ELb0EEC2EOS9_,"axG",@progbits,_ZN2v84base8internal15OptionalStorageINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEELb0ELb0EEC5EOS9_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v84base8internal15OptionalStorageINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEELb0ELb0EEC2EOS9_
	.type	_ZN2v84base8internal15OptionalStorageINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEELb0ELb0EEC2EOS9_, @function
_ZN2v84base8internal15OptionalStorageINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEELb0ELb0EEC2EOS9_:
.LFB7151:
	.cfi_startproc
	endbr64
	movb	$0, (%rdi)
	movb	$0, 8(%rdi)
	cmpb	$0, (%rsi)
	je	.L723
	leaq	24(%rdi), %rax
	movq	%rax, 8(%rdi)
	movq	8(%rsi), %rdx
	leaq	24(%rsi), %rax
	cmpq	%rax, %rdx
	je	.L727
	movq	%rdx, 8(%rdi)
	movq	24(%rsi), %rdx
	movq	%rdx, 24(%rdi)
.L726:
	movq	16(%rsi), %rdx
	movq	%rax, 8(%rsi)
	movq	$0, 16(%rsi)
	movq	%rdx, 16(%rdi)
	movb	$1, (%rdi)
	movb	$0, 24(%rsi)
.L723:
	ret
	.p2align 4,,10
	.p2align 3
.L727:
	movdqu	24(%rsi), %xmm0
	movups	%xmm0, 24(%rdi)
	jmp	.L726
	.cfi_endproc
.LFE7151:
	.size	_ZN2v84base8internal15OptionalStorageINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEELb0ELb0EEC2EOS9_, .-_ZN2v84base8internal15OptionalStorageINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEELb0ELb0EEC2EOS9_
	.weak	_ZN2v84base8internal15OptionalStorageINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEELb0ELb0EEC1EOS9_
	.set	_ZN2v84base8internal15OptionalStorageINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEELb0ELb0EEC1EOS9_,_ZN2v84base8internal15OptionalStorageINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEELb0ELb0EEC2EOS9_
	.section	.text._ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED2Ev,"axG",@progbits,_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED2Ev
	.type	_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED2Ev, @function
_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED2Ev:
.LFB7305:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	72(%rdi), %rdi
	leaq	88(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L729
	call	_ZdlPv@PLT
.L729:
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	56(%rbx), %rdi
	movq	%rax, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt6localeD1Ev@PLT
	.cfi_endproc
.LFE7305:
	.size	_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED2Ev, .-_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED2Ev
	.weak	_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED1Ev
	.set	_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED1Ev,_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED2Ev
	.section	.text._ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED0Ev,"axG",@progbits,_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED0Ev
	.type	_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED0Ev, @function
_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED0Ev:
.LFB7307:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	72(%rdi), %rdi
	leaq	88(%r12), %rax
	cmpq	%rax, %rdi
	je	.L732
	call	_ZdlPv@PLT
.L732:
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	56(%r12), %rdi
	movq	%rax, (%r12)
	call	_ZNSt6localeD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$104, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE7307:
	.size	_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED0Ev, .-_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED0Ev
	.section	.rodata._ZNK2v88internal6torque4Type8ToStringB5cxx11Ev.str1.1,"aMS",@progbits,1
.LC10:
	.string	")"
.LC11:
	.string	" (aka. "
.LC12:
	.string	", "
	.section	.text.unlikely._ZNK2v88internal6torque4Type8ToStringB5cxx11Ev,"ax",@progbits
	.align 2
.LCOLDB13:
	.section	.text._ZNK2v88internal6torque4Type8ToStringB5cxx11Ev,"ax",@progbits
.LHOTB13:
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal6torque4Type8ToStringB5cxx11Ev
	.type	_ZNK2v88internal6torque4Type8ToStringB5cxx11Ev, @function
_ZNK2v88internal6torque4Type8ToStringB5cxx11Ev:
.LFB6509:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6509
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$424, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	64(%rsi), %rax
	testq	%rax, %rax
	jne	.L735
	movq	(%rsi), %rax
.LEHB54:
	call	*72(%rax)
.L734:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L762
	addq	$424, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L735:
	.cfi_restore_state
	cmpq	$1, %rax
	je	.L763
	leaq	-448(%rbp), %rax
	addq	$32, %r13
	xorl	%ebx, %ebx
	movq	%rax, %rdi
	movq	%rax, -456(%rbp)
	leaq	-432(%rbp), %r15
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEC1Ev@PLT
.LEHE54:
	movq	16(%r13), %r12
	cmpq	%r12, %r13
	jne	.L738
	jmp	.L743
	.p2align 4,,10
	.p2align 3
.L740:
	cmpl	$1, %ebx
	je	.L761
	movl	$2, %edx
	leaq	.LC12(%rip), %rsi
	movq	%r15, %rdi
.LEHB55:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L761:
	movq	40(%r12), %rdx
	movq	32(%r12), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L741:
	movq	%r12, %rdi
	addl	$1, %ebx
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r12
	cmpq	%rax, %r13
	je	.L743
.L738:
	testl	%ebx, %ebx
	jne	.L740
	movq	40(%r12), %rdx
	movq	32(%r12), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rax, %rdi
	movl	$7, %edx
	leaq	.LC11(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L741
	.p2align 4,,10
	.p2align 3
.L743:
	movl	$1, %edx
	leaq	.LC10(%rip), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE55:
	movq	-384(%rbp), %rax
	leaq	16(%r14), %rbx
	movb	$0, 16(%r14)
	movq	%rbx, (%r14)
	movq	$0, 8(%r14)
	testq	%rax, %rax
	je	.L744
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	ja	.L764
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r14, %rdi
.LEHB56:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7replaceEmmPKcm@PLT
.LEHE56:
.L746:
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	leaq	64+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	-352(%rbp), %rdi
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movq	%rcx, %xmm0
	movq	%rax, -320(%rbp)
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, %xmm1
	leaq	-336(%rbp), %rax
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -432(%rbp)
	cmpq	%rax, %rdi
	je	.L748
	call	_ZdlPv@PLT
.L748:
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	-368(%rbp), %rdi
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	leaq	-320(%rbp), %rdi
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rdx, -448(%rbp,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rdx, -432(%rbp,%rax)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rdx, -448(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	$0, -440(%rbp)
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	jmp	.L734
	.p2align 4,,10
	.p2align 3
.L763:
	movq	48(%rsi), %rax
	leaq	16(%rdi), %rdx
	movq	%rdx, (%rdi)
	movq	32(%rax), %rsi
	movq	40(%rax), %rdx
	addq	%rsi, %rdx
.LEHB57:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag.constprop.0
.LEHE57:
	jmp	.L734
	.p2align 4,,10
	.p2align 3
.L764:
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r14, %rdi
	movq	%rax, %r8
.LEHB58:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7replaceEmmPKcm@PLT
	jmp	.L746
	.p2align 4,,10
	.p2align 3
.L744:
	leaq	-352(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
.LEHE58:
	jmp	.L746
.L762:
	call	__stack_chk_fail@PLT
.L755:
	endbr64
	movq	%rax, %r12
	jmp	.L749
.L754:
	endbr64
	movq	%rax, %r12
	jmp	.L751
	.section	.gcc_except_table._ZNK2v88internal6torque4Type8ToStringB5cxx11Ev,"a",@progbits
.LLSDA6509:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6509-.LLSDACSB6509
.LLSDACSB6509:
	.uleb128 .LEHB54-.LFB6509
	.uleb128 .LEHE54-.LEHB54
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB55-.LFB6509
	.uleb128 .LEHE55-.LEHB55
	.uleb128 .L754-.LFB6509
	.uleb128 0
	.uleb128 .LEHB56-.LFB6509
	.uleb128 .LEHE56-.LEHB56
	.uleb128 .L755-.LFB6509
	.uleb128 0
	.uleb128 .LEHB57-.LFB6509
	.uleb128 .LEHE57-.LEHB57
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB58-.LFB6509
	.uleb128 .LEHE58-.LEHB58
	.uleb128 .L755-.LFB6509
	.uleb128 0
.LLSDACSE6509:
	.section	.text._ZNK2v88internal6torque4Type8ToStringB5cxx11Ev
	.cfi_endproc
	.section	.text.unlikely._ZNK2v88internal6torque4Type8ToStringB5cxx11Ev
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6509
	.type	_ZNK2v88internal6torque4Type8ToStringB5cxx11Ev.cold, @function
_ZNK2v88internal6torque4Type8ToStringB5cxx11Ev.cold:
.LFSB6509:
.L749:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	(%r14), %rdi
	cmpq	%rdi, %rbx
	je	.L751
	call	_ZdlPv@PLT
.L751:
	movq	-456(%rbp), %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r12, %rdi
.LEHB59:
	call	_Unwind_Resume@PLT
.LEHE59:
	.cfi_endproc
.LFE6509:
	.section	.gcc_except_table._ZNK2v88internal6torque4Type8ToStringB5cxx11Ev
.LLSDAC6509:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6509-.LLSDACSBC6509
.LLSDACSBC6509:
	.uleb128 .LEHB59-.LCOLDB13
	.uleb128 .LEHE59-.LEHB59
	.uleb128 0
	.uleb128 0
.LLSDACSEC6509:
	.section	.text.unlikely._ZNK2v88internal6torque4Type8ToStringB5cxx11Ev
	.section	.text._ZNK2v88internal6torque4Type8ToStringB5cxx11Ev
	.size	_ZNK2v88internal6torque4Type8ToStringB5cxx11Ev, .-_ZNK2v88internal6torque4Type8ToStringB5cxx11Ev
	.section	.text.unlikely._ZNK2v88internal6torque4Type8ToStringB5cxx11Ev
	.size	_ZNK2v88internal6torque4Type8ToStringB5cxx11Ev.cold, .-_ZNK2v88internal6torque4Type8ToStringB5cxx11Ev.cold
.LCOLDE13:
	.section	.text._ZNK2v88internal6torque4Type8ToStringB5cxx11Ev
.LHOTE13:
	.section	.text.unlikely._ZN2v88internal6torquelsERSoRKSt6vectorIPKNS1_4TypeESaIS6_EE,"ax",@progbits
.LCOLDB14:
	.section	.text._ZN2v88internal6torquelsERSoRKSt6vectorIPKNS1_4TypeESaIS6_EE,"ax",@progbits
.LHOTB14:
	.p2align 4
	.globl	_ZN2v88internal6torquelsERSoRKSt6vectorIPKNS1_4TypeESaIS6_EE
	.type	_ZN2v88internal6torquelsERSoRKSt6vectorIPKNS1_4TypeESaIS6_EE, @function
_ZN2v88internal6torquelsERSoRKSt6vectorIPKNS1_4TypeESaIS6_EE:
.LFB6618:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6618
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	(%rsi), %rbx
	movq	8(%rsi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	%r13, %rbx
	je	.L766
	leaq	-80(%rbp), %rax
	leaq	-96(%rbp), %r15
	movq	%rax, -104(%rbp)
	leaq	.LC12(%rip), %r14
.L767:
	movq	(%rbx), %rsi
	movq	%r15, %rdi
.LEHB60:
	call	_ZNK2v88internal6torque4Type8ToStringB5cxx11Ev
.LEHE60:
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r12, %rdi
.LEHB61:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE61:
	movq	-96(%rbp), %rdi
	cmpq	-104(%rbp), %rdi
	je	.L768
	call	_ZdlPv@PLT
	addq	$8, %rbx
	cmpq	%rbx, %r13
	je	.L766
.L769:
	movl	$2, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
.LEHB62:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE62:
	jmp	.L767
	.p2align 4,,10
	.p2align 3
.L768:
	addq	$8, %rbx
	cmpq	%rbx, %r13
	jne	.L769
.L766:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L775
	addq	$72, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L775:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
.L773:
	endbr64
	movq	%rax, %r12
	jmp	.L770
	.section	.gcc_except_table._ZN2v88internal6torquelsERSoRKSt6vectorIPKNS1_4TypeESaIS6_EE,"a",@progbits
.LLSDA6618:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6618-.LLSDACSB6618
.LLSDACSB6618:
	.uleb128 .LEHB60-.LFB6618
	.uleb128 .LEHE60-.LEHB60
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB61-.LFB6618
	.uleb128 .LEHE61-.LEHB61
	.uleb128 .L773-.LFB6618
	.uleb128 0
	.uleb128 .LEHB62-.LFB6618
	.uleb128 .LEHE62-.LEHB62
	.uleb128 0
	.uleb128 0
.LLSDACSE6618:
	.section	.text._ZN2v88internal6torquelsERSoRKSt6vectorIPKNS1_4TypeESaIS6_EE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torquelsERSoRKSt6vectorIPKNS1_4TypeESaIS6_EE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6618
	.type	_ZN2v88internal6torquelsERSoRKSt6vectorIPKNS1_4TypeESaIS6_EE.cold, @function
_ZN2v88internal6torquelsERSoRKSt6vectorIPKNS1_4TypeESaIS6_EE.cold:
.LFSB6618:
.L770:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-96(%rbp), %rdi
	cmpq	-104(%rbp), %rdi
	je	.L771
	call	_ZdlPv@PLT
.L771:
	movq	%r12, %rdi
.LEHB63:
	call	_Unwind_Resume@PLT
.LEHE63:
	.cfi_endproc
.LFE6618:
	.section	.gcc_except_table._ZN2v88internal6torquelsERSoRKSt6vectorIPKNS1_4TypeESaIS6_EE
.LLSDAC6618:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6618-.LLSDACSBC6618
.LLSDACSBC6618:
	.uleb128 .LEHB63-.LCOLDB14
	.uleb128 .LEHE63-.LEHB63
	.uleb128 0
	.uleb128 0
.LLSDACSEC6618:
	.section	.text.unlikely._ZN2v88internal6torquelsERSoRKSt6vectorIPKNS1_4TypeESaIS6_EE
	.section	.text._ZN2v88internal6torquelsERSoRKSt6vectorIPKNS1_4TypeESaIS6_EE
	.size	_ZN2v88internal6torquelsERSoRKSt6vectorIPKNS1_4TypeESaIS6_EE, .-_ZN2v88internal6torquelsERSoRKSt6vectorIPKNS1_4TypeESaIS6_EE
	.section	.text.unlikely._ZN2v88internal6torquelsERSoRKSt6vectorIPKNS1_4TypeESaIS6_EE
	.size	_ZN2v88internal6torquelsERSoRKSt6vectorIPKNS1_4TypeESaIS6_EE.cold, .-_ZN2v88internal6torquelsERSoRKSt6vectorIPKNS1_4TypeESaIS6_EE.cold
.LCOLDE14:
	.section	.text._ZN2v88internal6torquelsERSoRKSt6vectorIPKNS1_4TypeESaIS6_EE
.LHOTE14:
	.section	.rodata._ZN2v88internal6torquelsERSoRKNS1_14ParameterTypesE.str1.1,"aMS",@progbits,1
.LC15:
	.string	"..."
	.section	.text.unlikely._ZN2v88internal6torquelsERSoRKNS1_14ParameterTypesE,"ax",@progbits
.LCOLDB16:
	.section	.text._ZN2v88internal6torquelsERSoRKNS1_14ParameterTypesE,"ax",@progbits
.LHOTB16:
	.p2align 4
	.globl	_ZN2v88internal6torquelsERSoRKNS1_14ParameterTypesE
	.type	_ZN2v88internal6torquelsERSoRKNS1_14ParameterTypesE, @function
_ZN2v88internal6torquelsERSoRKNS1_14ParameterTypesE:
.LFB6619:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6619
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	(%rsi), %rbx
	movq	8(%rsi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	%r14, %rbx
	je	.L777
	leaq	-80(%rbp), %rax
	leaq	-96(%rbp), %r15
	movq	%rax, -104(%rbp)
.L778:
	movq	(%rbx), %rsi
	movq	%r15, %rdi
.LEHB64:
	call	_ZNK2v88internal6torque4Type8ToStringB5cxx11Ev
.LEHE64:
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r13, %rdi
.LEHB65:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE65:
	movq	-96(%rbp), %rdi
	cmpq	-104(%rbp), %rdi
	je	.L779
	call	_ZdlPv@PLT
	addq	$8, %rbx
	cmpq	%rbx, %r14
	je	.L781
.L780:
	movl	$2, %edx
	leaq	.LC12(%rip), %rsi
	movq	%r13, %rdi
.LEHB66:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L778
	.p2align 4,,10
	.p2align 3
.L779:
	addq	$8, %rbx
	cmpq	%rbx, %r14
	jne	.L780
.L781:
	cmpb	$0, 24(%r12)
	je	.L785
	movq	8(%r12), %rax
	cmpq	%rax, (%r12)
	je	.L787
	movl	$2, %edx
	leaq	.LC12(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L787:
	movl	$3, %edx
	leaq	.LC15(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE66:
.L785:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L794
	addq	$72, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L777:
	.cfi_restore_state
	cmpb	$0, 24(%rsi)
	jne	.L787
	jmp	.L785
.L794:
	call	__stack_chk_fail@PLT
.L791:
	endbr64
	movq	%rax, %r12
	jmp	.L782
	.section	.gcc_except_table._ZN2v88internal6torquelsERSoRKNS1_14ParameterTypesE,"a",@progbits
.LLSDA6619:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6619-.LLSDACSB6619
.LLSDACSB6619:
	.uleb128 .LEHB64-.LFB6619
	.uleb128 .LEHE64-.LEHB64
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB65-.LFB6619
	.uleb128 .LEHE65-.LEHB65
	.uleb128 .L791-.LFB6619
	.uleb128 0
	.uleb128 .LEHB66-.LFB6619
	.uleb128 .LEHE66-.LEHB66
	.uleb128 0
	.uleb128 0
.LLSDACSE6619:
	.section	.text._ZN2v88internal6torquelsERSoRKNS1_14ParameterTypesE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torquelsERSoRKNS1_14ParameterTypesE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6619
	.type	_ZN2v88internal6torquelsERSoRKNS1_14ParameterTypesE.cold, @function
_ZN2v88internal6torquelsERSoRKNS1_14ParameterTypesE.cold:
.LFSB6619:
.L782:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-96(%rbp), %rdi
	cmpq	-104(%rbp), %rdi
	je	.L783
	call	_ZdlPv@PLT
.L783:
	movq	%r12, %rdi
.LEHB67:
	call	_Unwind_Resume@PLT
.LEHE67:
	.cfi_endproc
.LFE6619:
	.section	.gcc_except_table._ZN2v88internal6torquelsERSoRKNS1_14ParameterTypesE
.LLSDAC6619:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6619-.LLSDACSBC6619
.LLSDACSBC6619:
	.uleb128 .LEHB67-.LCOLDB16
	.uleb128 .LEHE67-.LEHB67
	.uleb128 0
	.uleb128 0
.LLSDACSEC6619:
	.section	.text.unlikely._ZN2v88internal6torquelsERSoRKNS1_14ParameterTypesE
	.section	.text._ZN2v88internal6torquelsERSoRKNS1_14ParameterTypesE
	.size	_ZN2v88internal6torquelsERSoRKNS1_14ParameterTypesE, .-_ZN2v88internal6torquelsERSoRKNS1_14ParameterTypesE
	.section	.text.unlikely._ZN2v88internal6torquelsERSoRKNS1_14ParameterTypesE
	.size	_ZN2v88internal6torquelsERSoRKNS1_14ParameterTypesE.cold, .-_ZN2v88internal6torquelsERSoRKNS1_14ParameterTypesE.cold
.LCOLDE16:
	.section	.text._ZN2v88internal6torquelsERSoRKNS1_14ParameterTypesE
.LHOTE16:
	.section	.rodata._ZN2v88internal6torquelsERSoRKNS1_5FieldE.str1.1,"aMS",@progbits,1
.LC17:
	.string	": "
.LC18:
	.string	" (weak)"
	.section	.text.unlikely._ZN2v88internal6torquelsERSoRKNS1_5FieldE,"ax",@progbits
.LCOLDB19:
	.section	.text._ZN2v88internal6torquelsERSoRKNS1_5FieldE,"ax",@progbits
.LHOTB19:
	.p2align 4
	.globl	_ZN2v88internal6torquelsERSoRKNS1_5FieldE
	.type	_ZN2v88internal6torquelsERSoRKNS1_5FieldE, @function
_ZN2v88internal6torquelsERSoRKNS1_5FieldE:
.LFB6616:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6616
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	subq	$48, %rsp
	movq	56(%rsi), %rdx
	movq	48(%rsi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
.LEHB68:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$2, %edx
	leaq	.LC17(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	80(%rbx), %rsi
	leaq	-64(%rbp), %rdi
	call	_ZNK2v88internal6torque4Type8ToStringB5cxx11Ev
.LEHE68:
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rsi
	movq	%r12, %rdi
.LEHB69:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE69:
	movq	-64(%rbp), %rdi
	leaq	-48(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L796
	call	_ZdlPv@PLT
.L796:
	cmpb	$0, 96(%rbx)
	jne	.L804
.L798:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L805
	addq	$48, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L804:
	.cfi_restore_state
	movl	$7, %edx
	leaq	.LC18(%rip), %rsi
	movq	%r12, %rdi
.LEHB70:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE70:
	jmp	.L798
.L805:
	call	__stack_chk_fail@PLT
.L802:
	endbr64
	movq	%rax, %r12
	jmp	.L799
	.section	.gcc_except_table._ZN2v88internal6torquelsERSoRKNS1_5FieldE,"a",@progbits
.LLSDA6616:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6616-.LLSDACSB6616
.LLSDACSB6616:
	.uleb128 .LEHB68-.LFB6616
	.uleb128 .LEHE68-.LEHB68
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB69-.LFB6616
	.uleb128 .LEHE69-.LEHB69
	.uleb128 .L802-.LFB6616
	.uleb128 0
	.uleb128 .LEHB70-.LFB6616
	.uleb128 .LEHE70-.LEHB70
	.uleb128 0
	.uleb128 0
.LLSDACSE6616:
	.section	.text._ZN2v88internal6torquelsERSoRKNS1_5FieldE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torquelsERSoRKNS1_5FieldE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6616
	.type	_ZN2v88internal6torquelsERSoRKNS1_5FieldE.cold, @function
_ZN2v88internal6torquelsERSoRKNS1_5FieldE.cold:
.LFSB6616:
.L799:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	movq	-64(%rbp), %rdi
	leaq	-48(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L800
	call	_ZdlPv@PLT
.L800:
	movq	%r12, %rdi
.LEHB71:
	call	_Unwind_Resume@PLT
.LEHE71:
	.cfi_endproc
.LFE6616:
	.section	.gcc_except_table._ZN2v88internal6torquelsERSoRKNS1_5FieldE
.LLSDAC6616:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6616-.LLSDACSBC6616
.LLSDACSBC6616:
	.uleb128 .LEHB71-.LCOLDB19
	.uleb128 .LEHE71-.LEHB71
	.uleb128 0
	.uleb128 0
.LLSDACSEC6616:
	.section	.text.unlikely._ZN2v88internal6torquelsERSoRKNS1_5FieldE
	.section	.text._ZN2v88internal6torquelsERSoRKNS1_5FieldE
	.size	_ZN2v88internal6torquelsERSoRKNS1_5FieldE, .-_ZN2v88internal6torquelsERSoRKNS1_5FieldE
	.section	.text.unlikely._ZN2v88internal6torquelsERSoRKNS1_5FieldE
	.size	_ZN2v88internal6torquelsERSoRKNS1_5FieldE.cold, .-_ZN2v88internal6torquelsERSoRKNS1_5FieldE.cold
.LCOLDE19:
	.section	.text._ZN2v88internal6torquelsERSoRKNS1_5FieldE
.LHOTE19:
	.section	.text.unlikely._ZN2v88internal6torquelsERSoRKNS1_11NameAndTypeE,"ax",@progbits
.LCOLDB20:
	.section	.text._ZN2v88internal6torquelsERSoRKNS1_11NameAndTypeE,"ax",@progbits
.LHOTB20:
	.p2align 4
	.globl	_ZN2v88internal6torquelsERSoRKNS1_11NameAndTypeE
	.type	_ZN2v88internal6torquelsERSoRKNS1_11NameAndTypeE, @function
_ZN2v88internal6torquelsERSoRKNS1_11NameAndTypeE:
.LFB6615:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6615
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	subq	$48, %rsp
	movq	8(%rsi), %rdx
	movq	(%rsi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
.LEHB72:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$2, %edx
	leaq	.LC17(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	32(%rbx), %rsi
	leaq	-64(%rbp), %rdi
	call	_ZNK2v88internal6torque4Type8ToStringB5cxx11Ev
.LEHE72:
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rsi
	movq	%r12, %rdi
.LEHB73:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE73:
	movq	-64(%rbp), %rdi
	leaq	-48(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L807
	call	_ZdlPv@PLT
.L807:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L814
	addq	$48, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L814:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
.L811:
	endbr64
	movq	%rax, %r12
	jmp	.L808
	.section	.gcc_except_table._ZN2v88internal6torquelsERSoRKNS1_11NameAndTypeE,"a",@progbits
.LLSDA6615:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6615-.LLSDACSB6615
.LLSDACSB6615:
	.uleb128 .LEHB72-.LFB6615
	.uleb128 .LEHE72-.LEHB72
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB73-.LFB6615
	.uleb128 .LEHE73-.LEHB73
	.uleb128 .L811-.LFB6615
	.uleb128 0
.LLSDACSE6615:
	.section	.text._ZN2v88internal6torquelsERSoRKNS1_11NameAndTypeE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torquelsERSoRKNS1_11NameAndTypeE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6615
	.type	_ZN2v88internal6torquelsERSoRKNS1_11NameAndTypeE.cold, @function
_ZN2v88internal6torquelsERSoRKNS1_11NameAndTypeE.cold:
.LFSB6615:
.L808:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	movq	-64(%rbp), %rdi
	leaq	-48(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L809
	call	_ZdlPv@PLT
.L809:
	movq	%r12, %rdi
.LEHB74:
	call	_Unwind_Resume@PLT
.LEHE74:
	.cfi_endproc
.LFE6615:
	.section	.gcc_except_table._ZN2v88internal6torquelsERSoRKNS1_11NameAndTypeE
.LLSDAC6615:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6615-.LLSDACSBC6615
.LLSDACSBC6615:
	.uleb128 .LEHB74-.LCOLDB20
	.uleb128 .LEHE74-.LEHB74
	.uleb128 0
	.uleb128 0
.LLSDACSEC6615:
	.section	.text.unlikely._ZN2v88internal6torquelsERSoRKNS1_11NameAndTypeE
	.section	.text._ZN2v88internal6torquelsERSoRKNS1_11NameAndTypeE
	.size	_ZN2v88internal6torquelsERSoRKNS1_11NameAndTypeE, .-_ZN2v88internal6torquelsERSoRKNS1_11NameAndTypeE
	.section	.text.unlikely._ZN2v88internal6torquelsERSoRKNS1_11NameAndTypeE
	.size	_ZN2v88internal6torquelsERSoRKNS1_11NameAndTypeE.cold, .-_ZN2v88internal6torquelsERSoRKNS1_11NameAndTypeE.cold
.LCOLDE20:
	.section	.text._ZN2v88internal6torquelsERSoRKNS1_11NameAndTypeE
.LHOTE20:
	.section	.rodata._ZNK2v88internal6torque18BuiltinPointerType11MangledNameB5cxx11Ev.str1.1,"aMS",@progbits,1
.LC21:
	.string	"FT"
	.section	.text.unlikely._ZNK2v88internal6torque18BuiltinPointerType11MangledNameB5cxx11Ev,"ax",@progbits
	.align 2
.LCOLDB25:
	.section	.text._ZNK2v88internal6torque18BuiltinPointerType11MangledNameB5cxx11Ev,"ax",@progbits
.LHOTB25:
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal6torque18BuiltinPointerType11MangledNameB5cxx11Ev
	.type	_ZNK2v88internal6torque18BuiltinPointerType11MangledNameB5cxx11Ev, @function
_ZNK2v88internal6torque18BuiltinPointerType11MangledNameB5cxx11Ev:
.LFB6519:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6519
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-320(%rbp), %r14
	pushq	%r13
	movq	%r14, %rdi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	-448(%rbp), %rbx
	subq	$488, %rsp
	movq	%rsi, -496(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rbx, -528(%rbp)
	movq	%r14, -504(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	movq	%rax, -320(%rbp)
	xorl	%eax, %eax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movw	%ax, -96(%rbp)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	$0, -104(%rbp)
	movq	%rax, -448(%rbp)
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	movq	-448(%rbp), %rax
	movq	$0, -440(%rbp)
	addq	-24(%rax), %rbx
	movq	%rbx, %rdi
.LEHB75:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE75:
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	leaq	-432(%rbp), %r12
	xorl	%esi, %esi
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -432(%rbp,%rax)
	movq	-432(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
.LEHB76:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE76:
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	leaq	-424(%rbp), %r13
	movq	.LC22(%rip), %xmm0
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movhps	.LC23(%rip), %xmm0
	movq	%rcx, -448(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movq	%rax, -320(%rbp)
	leaq	-368(%rbp), %rax
	movq	%rax, %rdi
	movaps	%xmm0, -432(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rax, -520(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movl	$24, -360(%rbp)
	movq	%rax, -512(%rbp)
	movq	%rax, -352(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
.LEHB77:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE77:
	movl	$2, %edx
	leaq	.LC21(%rip), %rsi
	movq	%r12, %rdi
.LEHB78:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-496(%rbp), %rax
	leaq	-480(%rbp), %r13
	movq	72(%rax), %rcx
	movq	80(%rax), %r14
	leaq	-464(%rbp), %rax
	movq	%rax, -488(%rbp)
	movq	%rcx, %rbx
	cmpq	%r14, %rcx
	je	.L824
	.p2align 4,,10
	.p2align 3
.L825:
	movq	(%rbx), %rsi
	movq	%r13, %rdi
	movq	(%rsi), %rax
	call	*24(%rax)
.LEHE78:
	movq	-472(%rbp), %rsi
	movq	%r12, %rdi
.LEHB79:
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movq	-472(%rbp), %rdx
	movq	-480(%rbp), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE79:
	movq	-480(%rbp), %rdi
	cmpq	-488(%rbp), %rdi
	je	.L822
	call	_ZdlPv@PLT
	addq	$8, %rbx
	cmpq	%rbx, %r14
	jne	.L825
.L824:
	movq	-496(%rbp), %rax
	movq	%r13, %rdi
	movq	96(%rax), %rsi
	movq	(%rsi), %rax
.LEHB80:
	call	*24(%rax)
.LEHE80:
	movq	-472(%rbp), %rsi
	movq	%r12, %rdi
.LEHB81:
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movq	-472(%rbp), %rdx
	movq	-480(%rbp), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE81:
	movq	-384(%rbp), %rax
	leaq	16(%r15), %rbx
	movb	$0, 16(%r15)
	movq	%rbx, (%r15)
	movq	$0, 8(%r15)
	testq	%rax, %rax
	je	.L826
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	ja	.L852
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
.LEHB82:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L828:
	movq	-480(%rbp), %rdi
	leaq	-464(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L830
	call	_ZdlPv@PLT
.L830:
	movq	.LC22(%rip), %xmm0
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-352(%rbp), %rdi
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movhps	.LC24(%rip), %xmm0
	movq	%rax, -320(%rbp)
	movaps	%xmm0, -432(%rbp)
	cmpq	-512(%rbp), %rdi
	je	.L834
	call	_ZdlPv@PLT
.L834:
	movq	-520(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	-504(%rbp), %rdi
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rdx, -448(%rbp,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -432(%rbp,%rax)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rdx, -448(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	movq	$0, -440(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L853
	addq	$488, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L822:
	.cfi_restore_state
	addq	$8, %rbx
	cmpq	%rbx, %r14
	jne	.L825
	jmp	.L824
	.p2align 4,,10
	.p2align 3
.L852:
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L828
	.p2align 4,,10
	.p2align 3
.L826:
	leaq	-352(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
.LEHE82:
	jmp	.L828
.L853:
	call	__stack_chk_fail@PLT
.L842:
	endbr64
	movq	%rax, %r12
	jmp	.L833
.L841:
	endbr64
	movq	%rax, %r12
	jmp	.L835
.L840:
	endbr64
	movq	%rax, %r12
	jmp	.L837
.L844:
	endbr64
	movq	%rax, %rbx
	jmp	.L819
.L845:
	endbr64
	movq	%rax, %rbx
	jmp	.L817
.L843:
	endbr64
	movq	%rax, %rbx
	jmp	.L818
.L846:
	endbr64
	movq	%rax, %r12
	jmp	.L831
	.section	.gcc_except_table._ZNK2v88internal6torque18BuiltinPointerType11MangledNameB5cxx11Ev,"a",@progbits
.LLSDA6519:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6519-.LLSDACSB6519
.LLSDACSB6519:
	.uleb128 .LEHB75-.LFB6519
	.uleb128 .LEHE75-.LEHB75
	.uleb128 .L843-.LFB6519
	.uleb128 0
	.uleb128 .LEHB76-.LFB6519
	.uleb128 .LEHE76-.LEHB76
	.uleb128 .L845-.LFB6519
	.uleb128 0
	.uleb128 .LEHB77-.LFB6519
	.uleb128 .LEHE77-.LEHB77
	.uleb128 .L844-.LFB6519
	.uleb128 0
	.uleb128 .LEHB78-.LFB6519
	.uleb128 .LEHE78-.LEHB78
	.uleb128 .L840-.LFB6519
	.uleb128 0
	.uleb128 .LEHB79-.LFB6519
	.uleb128 .LEHE79-.LEHB79
	.uleb128 .L841-.LFB6519
	.uleb128 0
	.uleb128 .LEHB80-.LFB6519
	.uleb128 .LEHE80-.LEHB80
	.uleb128 .L840-.LFB6519
	.uleb128 0
	.uleb128 .LEHB81-.LFB6519
	.uleb128 .LEHE81-.LEHB81
	.uleb128 .L842-.LFB6519
	.uleb128 0
	.uleb128 .LEHB82-.LFB6519
	.uleb128 .LEHE82-.LEHB82
	.uleb128 .L846-.LFB6519
	.uleb128 0
.LLSDACSE6519:
	.section	.text._ZNK2v88internal6torque18BuiltinPointerType11MangledNameB5cxx11Ev
	.cfi_endproc
	.section	.text.unlikely._ZNK2v88internal6torque18BuiltinPointerType11MangledNameB5cxx11Ev
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6519
	.type	_ZNK2v88internal6torque18BuiltinPointerType11MangledNameB5cxx11Ev.cold, @function
_ZNK2v88internal6torque18BuiltinPointerType11MangledNameB5cxx11Ev.cold:
.LFSB6519:
.L831:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	(%r15), %rdi
	cmpq	%rdi, %rbx
	je	.L833
	call	_ZdlPv@PLT
.L833:
	movq	-480(%rbp), %rdi
	leaq	-464(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L837
	call	_ZdlPv@PLT
.L837:
	movq	-528(%rbp), %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r12, %rdi
.LEHB83:
	call	_Unwind_Resume@PLT
.L835:
	movq	-480(%rbp), %rdi
	leaq	-464(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L837
	call	_ZdlPv@PLT
	jmp	.L837
.L819:
	movq	%r13, %rdi
	call	_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED1Ev
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -432(%rbp,%rax)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	movq	$0, -440(%rbp)
.L818:
	movq	-504(%rbp), %rdi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	%rbx, %rdi
	call	_Unwind_Resume@PLT
.LEHE83:
.L817:
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	movq	$0, -440(%rbp)
	jmp	.L818
	.cfi_endproc
.LFE6519:
	.section	.gcc_except_table._ZNK2v88internal6torque18BuiltinPointerType11MangledNameB5cxx11Ev
.LLSDAC6519:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6519-.LLSDACSBC6519
.LLSDACSBC6519:
	.uleb128 .LEHB83-.LCOLDB25
	.uleb128 .LEHE83-.LEHB83
	.uleb128 0
	.uleb128 0
.LLSDACSEC6519:
	.section	.text.unlikely._ZNK2v88internal6torque18BuiltinPointerType11MangledNameB5cxx11Ev
	.section	.text._ZNK2v88internal6torque18BuiltinPointerType11MangledNameB5cxx11Ev
	.size	_ZNK2v88internal6torque18BuiltinPointerType11MangledNameB5cxx11Ev, .-_ZNK2v88internal6torque18BuiltinPointerType11MangledNameB5cxx11Ev
	.section	.text.unlikely._ZNK2v88internal6torque18BuiltinPointerType11MangledNameB5cxx11Ev
	.size	_ZNK2v88internal6torque18BuiltinPointerType11MangledNameB5cxx11Ev.cold, .-_ZNK2v88internal6torque18BuiltinPointerType11MangledNameB5cxx11Ev.cold
.LCOLDE25:
	.section	.text._ZNK2v88internal6torque18BuiltinPointerType11MangledNameB5cxx11Ev
.LHOTE25:
	.section	.rodata._ZNK2v88internal6torque9UnionType16ToExplicitStringB5cxx11Ev.str1.1,"aMS",@progbits,1
.LC26:
	.string	"("
.LC27:
	.string	" | "
	.section	.text.unlikely._ZNK2v88internal6torque9UnionType16ToExplicitStringB5cxx11Ev,"ax",@progbits
	.align 2
.LCOLDB28:
	.section	.text._ZNK2v88internal6torque9UnionType16ToExplicitStringB5cxx11Ev,"ax",@progbits
.LHOTB28:
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal6torque9UnionType16ToExplicitStringB5cxx11Ev
	.type	_ZNK2v88internal6torque9UnionType16ToExplicitStringB5cxx11Ev, @function
_ZNK2v88internal6torque9UnionType16ToExplicitStringB5cxx11Ev:
.LFB6520:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6520
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-320(%rbp), %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	-448(%rbp), %rbx
	subq	$488, %rsp
	movq	%rdi, -496(%rbp)
	movq	%r14, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rbx, -528(%rbp)
	movq	%r14, -504(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	movq	%rax, -320(%rbp)
	xorl	%eax, %eax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movw	%ax, -96(%rbp)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	$0, -104(%rbp)
	movq	%rax, -448(%rbp)
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	movq	-448(%rbp), %rax
	movq	$0, -440(%rbp)
	addq	-24(%rax), %rbx
	movq	%rbx, %rdi
.LEHB84:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE84:
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	leaq	-432(%rbp), %r13
	xorl	%esi, %esi
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -432(%rbp,%rax)
	movq	-432(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r13, %rdi
.LEHB85:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE85:
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	leaq	-424(%rbp), %r12
	movq	.LC22(%rip), %xmm0
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movhps	.LC23(%rip), %xmm0
	movq	%rcx, -448(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movq	%rax, -320(%rbp)
	leaq	-368(%rbp), %rax
	movq	%rax, %rdi
	movaps	%xmm0, -432(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rax, -512(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movl	$24, -360(%rbp)
	movq	%rax, -520(%rbp)
	movq	%rax, -352(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
.LEHB86:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE86:
	movl	$1, %edx
	leaq	.LC26(%rip), %rsi
	movq	%r13, %rdi
.LEHB87:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	96(%r15), %r12
	addq	$80, %r15
	cmpq	%r15, %r12
	je	.L859
	leaq	-464(%rbp), %rax
	movq	32(%r12), %r14
	leaq	-480(%rbp), %rbx
	movq	%rax, -488(%rbp)
	jmp	.L861
	.p2align 4,,10
	.p2align 3
.L882:
	movl	$3, %edx
	leaq	.LC27(%rip), %rsi
	movq	%r13, %rdi
	movq	32(%rax), %r14
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L861:
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZNK2v88internal6torque4Type8ToStringB5cxx11Ev
.LEHE87:
	movq	-472(%rbp), %rdx
	movq	-480(%rbp), %rsi
	movq	%r13, %rdi
.LEHB88:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE88:
	movq	-480(%rbp), %rdi
	cmpq	-488(%rbp), %rdi
	je	.L860
	call	_ZdlPv@PLT
.L860:
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r12
	cmpq	%rax, %r15
	jne	.L882
.L859:
	movl	$1, %edx
	leaq	.LC10(%rip), %rsi
	movq	%r13, %rdi
.LEHB89:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE89:
	movq	-496(%rbp), %rdi
	movq	-384(%rbp), %rax
	leaq	16(%rdi), %rbx
	movq	$0, 8(%rdi)
	movq	%rbx, (%rdi)
	movb	$0, 16(%rdi)
	testq	%rax, %rax
	je	.L865
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	ja	.L883
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
.LEHB90:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L867:
	movq	.LC22(%rip), %xmm0
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-352(%rbp), %rdi
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movhps	.LC24(%rip), %xmm0
	movq	%rax, -320(%rbp)
	movaps	%xmm0, -432(%rbp)
	cmpq	-520(%rbp), %rdi
	je	.L869
	call	_ZdlPv@PLT
.L869:
	movq	-512(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	-504(%rbp), %rdi
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -432(%rbp,%rax)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	movq	$0, -440(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L884
	movq	-496(%rbp), %rax
	addq	$488, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L883:
	.cfi_restore_state
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L867
	.p2align 4,,10
	.p2align 3
.L865:
	leaq	-352(%rbp), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
.LEHE90:
	jmp	.L867
.L884:
	call	__stack_chk_fail@PLT
.L877:
	endbr64
	movq	%rax, %r12
	jmp	.L862
.L873:
	endbr64
	movq	%rax, %r12
	jmp	.L864
.L875:
	endbr64
	movq	%rax, %rbx
	jmp	.L858
.L876:
	endbr64
	movq	%rax, %rbx
	jmp	.L856
.L874:
	endbr64
	movq	%rax, %rbx
	jmp	.L857
.L878:
	endbr64
	movq	%rax, %r12
	jmp	.L870
	.section	.gcc_except_table._ZNK2v88internal6torque9UnionType16ToExplicitStringB5cxx11Ev,"a",@progbits
.LLSDA6520:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6520-.LLSDACSB6520
.LLSDACSB6520:
	.uleb128 .LEHB84-.LFB6520
	.uleb128 .LEHE84-.LEHB84
	.uleb128 .L874-.LFB6520
	.uleb128 0
	.uleb128 .LEHB85-.LFB6520
	.uleb128 .LEHE85-.LEHB85
	.uleb128 .L876-.LFB6520
	.uleb128 0
	.uleb128 .LEHB86-.LFB6520
	.uleb128 .LEHE86-.LEHB86
	.uleb128 .L875-.LFB6520
	.uleb128 0
	.uleb128 .LEHB87-.LFB6520
	.uleb128 .LEHE87-.LEHB87
	.uleb128 .L873-.LFB6520
	.uleb128 0
	.uleb128 .LEHB88-.LFB6520
	.uleb128 .LEHE88-.LEHB88
	.uleb128 .L877-.LFB6520
	.uleb128 0
	.uleb128 .LEHB89-.LFB6520
	.uleb128 .LEHE89-.LEHB89
	.uleb128 .L873-.LFB6520
	.uleb128 0
	.uleb128 .LEHB90-.LFB6520
	.uleb128 .LEHE90-.LEHB90
	.uleb128 .L878-.LFB6520
	.uleb128 0
.LLSDACSE6520:
	.section	.text._ZNK2v88internal6torque9UnionType16ToExplicitStringB5cxx11Ev
	.cfi_endproc
	.section	.text.unlikely._ZNK2v88internal6torque9UnionType16ToExplicitStringB5cxx11Ev
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6520
	.type	_ZNK2v88internal6torque9UnionType16ToExplicitStringB5cxx11Ev.cold, @function
_ZNK2v88internal6torque9UnionType16ToExplicitStringB5cxx11Ev.cold:
.LFSB6520:
.L862:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-480(%rbp), %rdi
	leaq	-464(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L864
	call	_ZdlPv@PLT
.L864:
	movq	-528(%rbp), %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r12, %rdi
.LEHB91:
	call	_Unwind_Resume@PLT
.L858:
	movq	%r12, %rdi
	call	_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED1Ev
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -432(%rbp,%rax)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	movq	$0, -440(%rbp)
.L857:
	movq	-504(%rbp), %rdi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	%rbx, %rdi
	call	_Unwind_Resume@PLT
.LEHE91:
.L856:
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	movq	$0, -440(%rbp)
	jmp	.L857
.L870:
	movq	-496(%rbp), %rax
	movq	(%rax), %rdi
	cmpq	%rdi, %rbx
	je	.L864
	call	_ZdlPv@PLT
	jmp	.L864
	.cfi_endproc
.LFE6520:
	.section	.gcc_except_table._ZNK2v88internal6torque9UnionType16ToExplicitStringB5cxx11Ev
.LLSDAC6520:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6520-.LLSDACSBC6520
.LLSDACSBC6520:
	.uleb128 .LEHB91-.LCOLDB28
	.uleb128 .LEHE91-.LEHB91
	.uleb128 0
	.uleb128 0
.LLSDACSEC6520:
	.section	.text.unlikely._ZNK2v88internal6torque9UnionType16ToExplicitStringB5cxx11Ev
	.section	.text._ZNK2v88internal6torque9UnionType16ToExplicitStringB5cxx11Ev
	.size	_ZNK2v88internal6torque9UnionType16ToExplicitStringB5cxx11Ev, .-_ZNK2v88internal6torque9UnionType16ToExplicitStringB5cxx11Ev
	.section	.text.unlikely._ZNK2v88internal6torque9UnionType16ToExplicitStringB5cxx11Ev
	.size	_ZNK2v88internal6torque9UnionType16ToExplicitStringB5cxx11Ev.cold, .-_ZNK2v88internal6torque9UnionType16ToExplicitStringB5cxx11Ev.cold
.LCOLDE28:
	.section	.text._ZNK2v88internal6torque9UnionType16ToExplicitStringB5cxx11Ev
.LHOTE28:
	.section	.rodata._ZNK2v88internal6torque9UnionType11MangledNameB5cxx11Ev.str1.1,"aMS",@progbits,1
.LC29:
	.string	"UT"
	.section	.text.unlikely._ZNK2v88internal6torque9UnionType11MangledNameB5cxx11Ev,"ax",@progbits
	.align 2
.LCOLDB30:
	.section	.text._ZNK2v88internal6torque9UnionType11MangledNameB5cxx11Ev,"ax",@progbits
.LHOTB30:
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal6torque9UnionType11MangledNameB5cxx11Ev
	.type	_ZNK2v88internal6torque9UnionType11MangledNameB5cxx11Ev, @function
_ZNK2v88internal6torque9UnionType11MangledNameB5cxx11Ev:
.LFB6521:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6521
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-448(%rbp), %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$488, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-320(%rbp), %rax
	movq	%r14, -520(%rbp)
	movq	%rax, %rdi
	movq	%rax, -496(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rcx
	xorl	%eax, %eax
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	movw	%ax, -96(%rbp)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%rcx, -320(%rbp)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	$0, -104(%rbp)
	movq	%rcx, -448(%rbp)
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	-24(%rcx), %rax
	movq	%rdx, -448(%rbp,%rax)
	movq	-448(%rbp), %rax
	movq	$0, -440(%rbp)
	addq	-24(%rax), %r14
	movq	%r14, %rdi
.LEHB92:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE92:
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	leaq	-432(%rbp), %r13
	xorl	%esi, %esi
	movq	%rcx, -432(%rbp)
	movq	-24(%rcx), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rcx, -432(%rbp,%rax)
	movq	-432(%rbp), %rax
	movq	-24(%rax), %rcx
	addq	%r13, %rcx
	movq	%rcx, %rdi
.LEHB93:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE93:
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	.LC22(%rip), %xmm0
	leaq	-424(%rbp), %r12
	movq	%rdx, -448(%rbp)
	movq	-24(%rdx), %rax
	movhps	.LC23(%rip), %xmm0
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%rdx, -448(%rbp,%rax)
	leaq	80(%rcx), %rdx
	movq	%rcx, -448(%rbp)
	leaq	-368(%rbp), %rcx
	movq	%rcx, %rdi
	movaps	%xmm0, -432(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rdx, -320(%rbp)
	movq	%rcx, -504(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	movq	-496(%rbp), %rdi
	leaq	-336(%rbp), %rdx
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%r12, %rsi
	movq	%rcx, -424(%rbp)
	movl	$24, -360(%rbp)
	movq	%rdx, -512(%rbp)
	movq	%rdx, -352(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
.LEHB94:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE94:
	movl	$2, %edx
	leaq	.LC29(%rip), %rsi
	movq	%r13, %rdi
.LEHB95:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	96(%rbx), %r12
	leaq	-464(%rbp), %rax
	addq	$80, %rbx
	leaq	-480(%rbp), %r14
	movq	%rax, -488(%rbp)
	cmpq	%rbx, %r12
	je	.L895
	.p2align 4,,10
	.p2align 3
.L890:
	movq	32(%r12), %rsi
	movq	%r14, %rdi
	movq	(%rsi), %rax
	call	*24(%rax)
.LEHE95:
	movq	-472(%rbp), %rsi
	movq	%r13, %rdi
.LEHB96:
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movq	-472(%rbp), %rdx
	movq	-480(%rbp), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE96:
	movq	-480(%rbp), %rdi
	cmpq	-488(%rbp), %rdi
	je	.L893
	call	_ZdlPv@PLT
.L893:
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r12
	cmpq	%rax, %rbx
	jne	.L890
.L895:
	movq	-384(%rbp), %rax
	leaq	16(%r15), %rbx
	movb	$0, 16(%r15)
	movq	%rbx, (%r15)
	movq	$0, 8(%r15)
	testq	%rax, %rax
	je	.L919
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	ja	.L920
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
.LEHB97:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L897:
	movq	.LC22(%rip), %xmm0
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-352(%rbp), %rdi
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movhps	.LC24(%rip), %xmm0
	movq	%rax, -320(%rbp)
	movaps	%xmm0, -432(%rbp)
	cmpq	-512(%rbp), %rdi
	je	.L899
	call	_ZdlPv@PLT
.L899:
	movq	-504(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	movq	-496(%rbp), %rdi
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rbx, -448(%rbp,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rbx, -432(%rbp,%rax)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rbx, -448(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	movq	$0, -440(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L921
	addq	$488, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L920:
	.cfi_restore_state
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L897
	.p2align 4,,10
	.p2align 3
.L919:
	leaq	-352(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
.LEHE97:
	jmp	.L897
.L921:
	call	__stack_chk_fail@PLT
.L907:
	endbr64
	movq	%rax, %r12
	jmp	.L903
.L906:
	endbr64
	movq	%rax, %r12
	jmp	.L902
.L909:
	endbr64
	movq	%rax, %rbx
	jmp	.L889
.L910:
	endbr64
	movq	%rax, %rbx
	jmp	.L887
.L908:
	endbr64
	movq	%rax, %rbx
	jmp	.L888
.L911:
	endbr64
	movq	%rax, %r12
	jmp	.L900
	.section	.gcc_except_table._ZNK2v88internal6torque9UnionType11MangledNameB5cxx11Ev,"a",@progbits
.LLSDA6521:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6521-.LLSDACSB6521
.LLSDACSB6521:
	.uleb128 .LEHB92-.LFB6521
	.uleb128 .LEHE92-.LEHB92
	.uleb128 .L908-.LFB6521
	.uleb128 0
	.uleb128 .LEHB93-.LFB6521
	.uleb128 .LEHE93-.LEHB93
	.uleb128 .L910-.LFB6521
	.uleb128 0
	.uleb128 .LEHB94-.LFB6521
	.uleb128 .LEHE94-.LEHB94
	.uleb128 .L909-.LFB6521
	.uleb128 0
	.uleb128 .LEHB95-.LFB6521
	.uleb128 .LEHE95-.LEHB95
	.uleb128 .L906-.LFB6521
	.uleb128 0
	.uleb128 .LEHB96-.LFB6521
	.uleb128 .LEHE96-.LEHB96
	.uleb128 .L907-.LFB6521
	.uleb128 0
	.uleb128 .LEHB97-.LFB6521
	.uleb128 .LEHE97-.LEHB97
	.uleb128 .L911-.LFB6521
	.uleb128 0
.LLSDACSE6521:
	.section	.text._ZNK2v88internal6torque9UnionType11MangledNameB5cxx11Ev
	.cfi_endproc
	.section	.text.unlikely._ZNK2v88internal6torque9UnionType11MangledNameB5cxx11Ev
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6521
	.type	_ZNK2v88internal6torque9UnionType11MangledNameB5cxx11Ev.cold, @function
_ZNK2v88internal6torque9UnionType11MangledNameB5cxx11Ev.cold:
.LFSB6521:
.L903:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-480(%rbp), %rdi
	leaq	-464(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L902
	call	_ZdlPv@PLT
.L902:
	movq	-520(%rbp), %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r12, %rdi
.LEHB98:
	call	_Unwind_Resume@PLT
.L889:
	movq	%r12, %rdi
	call	_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED1Ev
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rdx, -448(%rbp,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rdx, -432(%rbp,%rax)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rdx, -448(%rbp,%rax)
	movq	$0, -440(%rbp)
.L888:
	movq	-496(%rbp), %rdi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	%rbx, %rdi
	call	_Unwind_Resume@PLT
.LEHE98:
.L887:
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rdx, -448(%rbp,%rax)
	movq	$0, -440(%rbp)
	jmp	.L888
.L900:
	movq	(%r15), %rdi
	cmpq	%rdi, %rbx
	je	.L902
	call	_ZdlPv@PLT
	jmp	.L902
	.cfi_endproc
.LFE6521:
	.section	.gcc_except_table._ZNK2v88internal6torque9UnionType11MangledNameB5cxx11Ev
.LLSDAC6521:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6521-.LLSDACSBC6521
.LLSDACSBC6521:
	.uleb128 .LEHB98-.LCOLDB30
	.uleb128 .LEHE98-.LEHB98
	.uleb128 0
	.uleb128 0
.LLSDACSEC6521:
	.section	.text.unlikely._ZNK2v88internal6torque9UnionType11MangledNameB5cxx11Ev
	.section	.text._ZNK2v88internal6torque9UnionType11MangledNameB5cxx11Ev
	.size	_ZNK2v88internal6torque9UnionType11MangledNameB5cxx11Ev, .-_ZNK2v88internal6torque9UnionType11MangledNameB5cxx11Ev
	.section	.text.unlikely._ZNK2v88internal6torque9UnionType11MangledNameB5cxx11Ev
	.size	_ZNK2v88internal6torque9UnionType11MangledNameB5cxx11Ev.cold, .-_ZNK2v88internal6torque9UnionType11MangledNameB5cxx11Ev.cold
.LCOLDE30:
	.section	.text._ZNK2v88internal6torque9UnionType11MangledNameB5cxx11Ev
.LHOTE30:
	.section	.rodata._ZN2v88internal6torque10StructType11ComputeNameERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS_4base8OptionalINS1_17SpecializationKeyINS1_17GenericStructTypeEEEEE.str1.1,"aMS",@progbits,1
.LC31:
	.string	"<"
	.section	.text.unlikely._ZN2v88internal6torque10StructType11ComputeNameERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS_4base8OptionalINS1_17SpecializationKeyINS1_17GenericStructTypeEEEEE,"ax",@progbits
	.align 2
.LCOLDB32:
	.section	.text._ZN2v88internal6torque10StructType11ComputeNameERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS_4base8OptionalINS1_17SpecializationKeyINS1_17GenericStructTypeEEEEE,"ax",@progbits
.LHOTB32:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque10StructType11ComputeNameERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS_4base8OptionalINS1_17SpecializationKeyINS1_17GenericStructTypeEEEEE
	.type	_ZN2v88internal6torque10StructType11ComputeNameERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS_4base8OptionalINS1_17SpecializationKeyINS1_17GenericStructTypeEEEEE, @function
_ZN2v88internal6torque10StructType11ComputeNameERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS_4base8OptionalINS1_17SpecializationKeyINS1_17GenericStructTypeEEEEE:
.LFB6559:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6559
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$488, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, (%rdx)
	jne	.L923
	leaq	16(%rdi), %rax
	movq	%rax, (%rdi)
	movq	(%rsi), %rsi
	movq	8(%rbx), %rdx
	addq	%rsi, %rdx
.LEHB99:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag.constprop.0
.LEHE99:
.L922:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L951
	addq	$488, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L923:
	.cfi_restore_state
	leaq	-320(%rbp), %rax
	leaq	-448(%rbp), %r14
	movq	%rdx, %r13
	movq	%rax, %rdi
	movq	%r14, -528(%rbp)
	movq	%rax, -504(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rcx
	xorl	%eax, %eax
	xorl	%esi, %esi
	movq	%rcx, -320(%rbp)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	pxor	%xmm0, %xmm0
	movq	$0, -104(%rbp)
	movq	%rcx, -448(%rbp)
	movw	%ax, -96(%rbp)
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	-24(%rcx), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rcx, -448(%rbp,%rax)
	movq	-448(%rbp), %rax
	movq	$0, -440(%rbp)
	addq	-24(%rax), %r14
	movq	%r14, %rdi
.LEHB100:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE100:
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	leaq	-432(%rbp), %r12
	xorl	%esi, %esi
	movq	%rcx, -432(%rbp)
	movq	-24(%rcx), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rcx, -432(%rbp,%rax)
	movq	-432(%rbp), %rax
	movq	-24(%rax), %rcx
	addq	%r12, %rcx
	movq	%rcx, %rdi
.LEHB101:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE101:
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	.LC22(%rip), %xmm0
	leaq	-424(%rbp), %r14
	movq	%rcx, -448(%rbp)
	movq	-24(%rcx), %rax
	movhps	.LC23(%rip), %xmm0
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rcx, -448(%rbp,%rax)
	leaq	80(%rdx), %rcx
	movq	%rcx, -320(%rbp)
	leaq	-368(%rbp), %rcx
	movq	%rcx, %rdi
	movaps	%xmm0, -432(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rdx, -448(%rbp)
	movq	%rcx, -512(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	movq	-504(%rbp), %rdi
	leaq	-336(%rbp), %rcx
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%r14, %rsi
	movq	%rdx, -424(%rbp)
	movl	$24, -360(%rbp)
	movq	%rcx, -520(%rbp)
	movq	%rcx, -352(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
.LEHB102:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE102:
	movq	8(%rbx), %rdx
	movq	(%rbx), %rsi
	movq	%r12, %rdi
.LEHB103:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rax, %rdi
	movl	$1, %edx
	leaq	.LC31(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	24(%r13), %rax
	movq	16(%r13), %rbx
	movq	%rax, -488(%rbp)
	cmpq	%rbx, %rax
	je	.L929
	leaq	-464(%rbp), %rax
	movq	(%rbx), %r13
	leaq	-480(%rbp), %r14
	movq	%rax, -496(%rbp)
	jmp	.L931
	.p2align 4,,10
	.p2align 3
.L952:
	movl	$2, %edx
	leaq	.LC12(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rbx), %r13
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L931:
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZNK2v88internal6torque4Type8ToStringB5cxx11Ev
.LEHE103:
	movq	-472(%rbp), %rdx
	movq	-480(%rbp), %rsi
	movq	%r12, %rdi
.LEHB104:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE104:
	movq	-480(%rbp), %rdi
	cmpq	-496(%rbp), %rdi
	je	.L930
	call	_ZdlPv@PLT
.L930:
	addq	$8, %rbx
	cmpq	%rbx, -488(%rbp)
	jne	.L952
.L929:
	movl	$1, %edx
	leaq	.LC4(%rip), %rsi
	movq	%r12, %rdi
.LEHB105:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE105:
	movq	-384(%rbp), %rax
	leaq	16(%r15), %rbx
	movb	$0, 16(%r15)
	movq	%rbx, (%r15)
	movq	$0, 8(%r15)
	testq	%rax, %rax
	je	.L932
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	jbe	.L933
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%rax, %r8
.LEHB106:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L934:
	movq	.LC22(%rip), %xmm0
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-352(%rbp), %rdi
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movhps	.LC24(%rip), %xmm0
	movq	%rax, -320(%rbp)
	movaps	%xmm0, -432(%rbp)
	cmpq	-520(%rbp), %rdi
	je	.L936
	call	_ZdlPv@PLT
.L936:
	movq	-512(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	-504(%rbp), %rdi
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rbx, -448(%rbp,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rbx, -432(%rbp,%rax)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	$0, -440(%rbp)
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	jmp	.L922
	.p2align 4,,10
	.p2align 3
.L933:
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L934
	.p2align 4,,10
	.p2align 3
.L932:
	leaq	-352(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
.LEHE106:
	jmp	.L934
.L951:
	call	__stack_chk_fail@PLT
.L945:
	endbr64
	movq	%rax, %rbx
	jmp	.L927
.L946:
	endbr64
	movq	%rax, %rbx
	jmp	.L928
.L943:
	endbr64
	movq	%rax, %r12
	jmp	.L939
.L948:
	endbr64
	movq	%rax, %r12
	jmp	.L937
.L947:
	endbr64
	movq	%rax, %rbx
	jmp	.L926
.L944:
	endbr64
	movq	%rax, %r12
	jmp	.L940
	.section	.gcc_except_table._ZN2v88internal6torque10StructType11ComputeNameERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS_4base8OptionalINS1_17SpecializationKeyINS1_17GenericStructTypeEEEEE,"a",@progbits
.LLSDA6559:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6559-.LLSDACSB6559
.LLSDACSB6559:
	.uleb128 .LEHB99-.LFB6559
	.uleb128 .LEHE99-.LEHB99
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB100-.LFB6559
	.uleb128 .LEHE100-.LEHB100
	.uleb128 .L945-.LFB6559
	.uleb128 0
	.uleb128 .LEHB101-.LFB6559
	.uleb128 .LEHE101-.LEHB101
	.uleb128 .L947-.LFB6559
	.uleb128 0
	.uleb128 .LEHB102-.LFB6559
	.uleb128 .LEHE102-.LEHB102
	.uleb128 .L946-.LFB6559
	.uleb128 0
	.uleb128 .LEHB103-.LFB6559
	.uleb128 .LEHE103-.LEHB103
	.uleb128 .L943-.LFB6559
	.uleb128 0
	.uleb128 .LEHB104-.LFB6559
	.uleb128 .LEHE104-.LEHB104
	.uleb128 .L944-.LFB6559
	.uleb128 0
	.uleb128 .LEHB105-.LFB6559
	.uleb128 .LEHE105-.LEHB105
	.uleb128 .L943-.LFB6559
	.uleb128 0
	.uleb128 .LEHB106-.LFB6559
	.uleb128 .LEHE106-.LEHB106
	.uleb128 .L948-.LFB6559
	.uleb128 0
.LLSDACSE6559:
	.section	.text._ZN2v88internal6torque10StructType11ComputeNameERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS_4base8OptionalINS1_17SpecializationKeyINS1_17GenericStructTypeEEEEE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque10StructType11ComputeNameERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS_4base8OptionalINS1_17SpecializationKeyINS1_17GenericStructTypeEEEEE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6559
	.type	_ZN2v88internal6torque10StructType11ComputeNameERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS_4base8OptionalINS1_17SpecializationKeyINS1_17GenericStructTypeEEEEE.cold, @function
_ZN2v88internal6torque10StructType11ComputeNameERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS_4base8OptionalINS1_17SpecializationKeyINS1_17GenericStructTypeEEEEE.cold:
.LFSB6559:
.L926:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rdx, -448(%rbp,%rax)
	movq	$0, -440(%rbp)
.L927:
	movq	-504(%rbp), %rdi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	%rbx, %rdi
.LEHB107:
	call	_Unwind_Resume@PLT
.L928:
	movq	%r14, %rdi
	call	_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED1Ev
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rdx, -448(%rbp,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rdx, -432(%rbp,%rax)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rdx, -448(%rbp,%rax)
	movq	$0, -440(%rbp)
	jmp	.L927
.L937:
	movq	(%r15), %rdi
	cmpq	%rdi, %rbx
	je	.L939
	call	_ZdlPv@PLT
.L939:
	movq	-528(%rbp), %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r12, %rdi
	call	_Unwind_Resume@PLT
.LEHE107:
.L940:
	movq	-480(%rbp), %rdi
	leaq	-464(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L939
	call	_ZdlPv@PLT
	jmp	.L939
	.cfi_endproc
.LFE6559:
	.section	.gcc_except_table._ZN2v88internal6torque10StructType11ComputeNameERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS_4base8OptionalINS1_17SpecializationKeyINS1_17GenericStructTypeEEEEE
.LLSDAC6559:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6559-.LLSDACSBC6559
.LLSDACSBC6559:
	.uleb128 .LEHB107-.LCOLDB32
	.uleb128 .LEHE107-.LEHB107
	.uleb128 0
	.uleb128 0
.LLSDACSEC6559:
	.section	.text.unlikely._ZN2v88internal6torque10StructType11ComputeNameERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS_4base8OptionalINS1_17SpecializationKeyINS1_17GenericStructTypeEEEEE
	.section	.text._ZN2v88internal6torque10StructType11ComputeNameERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS_4base8OptionalINS1_17SpecializationKeyINS1_17GenericStructTypeEEEEE
	.size	_ZN2v88internal6torque10StructType11ComputeNameERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS_4base8OptionalINS1_17SpecializationKeyINS1_17GenericStructTypeEEEEE, .-_ZN2v88internal6torque10StructType11ComputeNameERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS_4base8OptionalINS1_17SpecializationKeyINS1_17GenericStructTypeEEEEE
	.section	.text.unlikely._ZN2v88internal6torque10StructType11ComputeNameERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS_4base8OptionalINS1_17SpecializationKeyINS1_17GenericStructTypeEEEEE
	.size	_ZN2v88internal6torque10StructType11ComputeNameERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS_4base8OptionalINS1_17SpecializationKeyINS1_17GenericStructTypeEEEEE.cold, .-_ZN2v88internal6torque10StructType11ComputeNameERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS_4base8OptionalINS1_17SpecializationKeyINS1_17GenericStructTypeEEEEE.cold
.LCOLDE32:
	.section	.text._ZN2v88internal6torque10StructType11ComputeNameERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS_4base8OptionalINS1_17SpecializationKeyINS1_17GenericStructTypeEEEEE
.LHOTE32:
	.section	.text.unlikely._ZNK2v88internal6torque10StructType11MangledNameB5cxx11Ev,"ax",@progbits
	.align 2
.LCOLDB33:
	.section	.text._ZNK2v88internal6torque10StructType11MangledNameB5cxx11Ev,"ax",@progbits
.LHOTB33:
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal6torque10StructType11MangledNameB5cxx11Ev
	.type	_ZNK2v88internal6torque10StructType11MangledNameB5cxx11Ev, @function
_ZNK2v88internal6torque10StructType11MangledNameB5cxx11Ev:
.LFB6561:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6561
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	-448(%rbp), %rbx
	subq	$488, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-320(%rbp), %rax
	movq	%rbx, -520(%rbp)
	movq	%rax, %rdi
	movq	%rax, -496(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rcx
	xorl	%eax, %eax
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	movw	%ax, -96(%rbp)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%rcx, -320(%rbp)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	$0, -104(%rbp)
	movq	%rcx, -448(%rbp)
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	-24(%rcx), %rax
	movq	%rdx, -448(%rbp,%rax)
	movq	-448(%rbp), %rax
	movq	$0, -440(%rbp)
	addq	-24(%rax), %rbx
	movq	%rbx, %rdi
.LEHB108:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE108:
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	leaq	-432(%rbp), %r12
	xorl	%esi, %esi
	movq	%rcx, -432(%rbp)
	movq	-24(%rcx), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rcx, -432(%rbp,%rax)
	movq	-432(%rbp), %rax
	movq	-24(%rax), %rcx
	addq	%r12, %rcx
	movq	%rcx, %rdi
.LEHB109:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE109:
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	.LC22(%rip), %xmm0
	leaq	-424(%rbp), %r14
	movq	%rdx, -448(%rbp)
	movq	-24(%rdx), %rax
	movhps	.LC23(%rip), %xmm0
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%rdx, -448(%rbp,%rax)
	leaq	80(%rcx), %rdx
	movq	%rcx, -448(%rbp)
	leaq	-368(%rbp), %rcx
	movq	%rcx, %rdi
	movaps	%xmm0, -432(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rdx, -320(%rbp)
	movq	%rcx, -504(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	movq	-496(%rbp), %rdi
	leaq	-336(%rbp), %rdx
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%r14, %rsi
	movq	%rcx, -424(%rbp)
	movl	$24, -360(%rbp)
	movq	%rdx, -512(%rbp)
	movq	%rdx, -352(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
.LEHB110:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE110:
	movq	168(%r13), %rax
	movq	%r12, %rdi
	movq	32(%rax), %rax
	movq	40(%rax), %rdx
	movq	32(%rax), %rsi
.LEHB111:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 176(%r13)
	je	.L961
	movq	192(%r13), %rbx
	movq	200(%r13), %r13
	cmpq	%r13, %rbx
	je	.L961
	leaq	-464(%rbp), %rax
	leaq	-480(%rbp), %r14
	movq	%rax, -488(%rbp)
	.p2align 4,,10
	.p2align 3
.L964:
	movq	(%rbx), %rsi
	movq	%r14, %rdi
	movq	(%rsi), %rax
	call	*24(%rax)
.LEHE111:
	movq	-472(%rbp), %rsi
	movq	%r12, %rdi
.LEHB112:
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movq	-472(%rbp), %rdx
	movq	-480(%rbp), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE112:
	movq	-480(%rbp), %rdi
	cmpq	-488(%rbp), %rdi
	je	.L962
	call	_ZdlPv@PLT
	addq	$8, %rbx
	cmpq	%rbx, %r13
	jne	.L964
.L961:
	movq	-384(%rbp), %rax
	leaq	16(%r15), %rbx
	movb	$0, 16(%r15)
	movq	%rbx, (%r15)
	movq	$0, 8(%r15)
	testq	%rax, %rax
	je	.L985
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	ja	.L986
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
.LEHB113:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L966:
	movq	.LC22(%rip), %xmm0
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-352(%rbp), %rdi
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movhps	.LC24(%rip), %xmm0
	movq	%rax, -320(%rbp)
	movaps	%xmm0, -432(%rbp)
	cmpq	-512(%rbp), %rdi
	je	.L968
	call	_ZdlPv@PLT
.L968:
	movq	-504(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	-496(%rbp), %rdi
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rdx, -448(%rbp,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -432(%rbp,%rax)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rdx, -448(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	movq	$0, -440(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L987
	addq	$488, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L962:
	.cfi_restore_state
	addq	$8, %rbx
	cmpq	%rbx, %r13
	jne	.L964
	jmp	.L961
	.p2align 4,,10
	.p2align 3
.L986:
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L966
	.p2align 4,,10
	.p2align 3
.L985:
	leaq	-352(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
.LEHE113:
	jmp	.L966
.L987:
	call	__stack_chk_fail@PLT
.L976:
	endbr64
	movq	%rax, %r12
	jmp	.L972
.L975:
	endbr64
	movq	%rax, %r12
	jmp	.L971
.L978:
	endbr64
	movq	%rax, %rbx
	jmp	.L957
.L979:
	endbr64
	movq	%rax, %rbx
	jmp	.L955
.L977:
	endbr64
	movq	%rax, %rbx
	jmp	.L956
.L980:
	endbr64
	movq	%rax, %r12
	jmp	.L969
	.section	.gcc_except_table._ZNK2v88internal6torque10StructType11MangledNameB5cxx11Ev,"a",@progbits
.LLSDA6561:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6561-.LLSDACSB6561
.LLSDACSB6561:
	.uleb128 .LEHB108-.LFB6561
	.uleb128 .LEHE108-.LEHB108
	.uleb128 .L977-.LFB6561
	.uleb128 0
	.uleb128 .LEHB109-.LFB6561
	.uleb128 .LEHE109-.LEHB109
	.uleb128 .L979-.LFB6561
	.uleb128 0
	.uleb128 .LEHB110-.LFB6561
	.uleb128 .LEHE110-.LEHB110
	.uleb128 .L978-.LFB6561
	.uleb128 0
	.uleb128 .LEHB111-.LFB6561
	.uleb128 .LEHE111-.LEHB111
	.uleb128 .L975-.LFB6561
	.uleb128 0
	.uleb128 .LEHB112-.LFB6561
	.uleb128 .LEHE112-.LEHB112
	.uleb128 .L976-.LFB6561
	.uleb128 0
	.uleb128 .LEHB113-.LFB6561
	.uleb128 .LEHE113-.LEHB113
	.uleb128 .L980-.LFB6561
	.uleb128 0
.LLSDACSE6561:
	.section	.text._ZNK2v88internal6torque10StructType11MangledNameB5cxx11Ev
	.cfi_endproc
	.section	.text.unlikely._ZNK2v88internal6torque10StructType11MangledNameB5cxx11Ev
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6561
	.type	_ZNK2v88internal6torque10StructType11MangledNameB5cxx11Ev.cold, @function
_ZNK2v88internal6torque10StructType11MangledNameB5cxx11Ev.cold:
.LFSB6561:
.L972:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-480(%rbp), %rdi
	leaq	-464(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L971
	call	_ZdlPv@PLT
.L971:
	movq	-520(%rbp), %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r12, %rdi
.LEHB114:
	call	_Unwind_Resume@PLT
.L957:
	movq	%r14, %rdi
	call	_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED1Ev
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -432(%rbp,%rax)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	movq	$0, -440(%rbp)
.L956:
	movq	-496(%rbp), %rdi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	%rbx, %rdi
	call	_Unwind_Resume@PLT
.LEHE114:
.L955:
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	movq	$0, -440(%rbp)
	jmp	.L956
.L969:
	movq	(%r15), %rdi
	cmpq	%rdi, %rbx
	je	.L971
	call	_ZdlPv@PLT
	jmp	.L971
	.cfi_endproc
.LFE6561:
	.section	.gcc_except_table._ZNK2v88internal6torque10StructType11MangledNameB5cxx11Ev
.LLSDAC6561:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6561-.LLSDACSBC6561
.LLSDACSBC6561:
	.uleb128 .LEHB114-.LCOLDB33
	.uleb128 .LEHE114-.LEHB114
	.uleb128 0
	.uleb128 0
.LLSDACSEC6561:
	.section	.text.unlikely._ZNK2v88internal6torque10StructType11MangledNameB5cxx11Ev
	.section	.text._ZNK2v88internal6torque10StructType11MangledNameB5cxx11Ev
	.size	_ZNK2v88internal6torque10StructType11MangledNameB5cxx11Ev, .-_ZNK2v88internal6torque10StructType11MangledNameB5cxx11Ev
	.section	.text.unlikely._ZNK2v88internal6torque10StructType11MangledNameB5cxx11Ev
	.size	_ZNK2v88internal6torque10StructType11MangledNameB5cxx11Ev.cold, .-_ZNK2v88internal6torque10StructType11MangledNameB5cxx11Ev.cold
.LCOLDE33:
	.section	.text._ZNK2v88internal6torque10StructType11MangledNameB5cxx11Ev
.LHOTE33:
	.section	.rodata._ZNK2v88internal6torque10StructType24GetGeneratedTypeNameImplB5cxx11Ev.str1.1,"aMS",@progbits,1
.LC34:
	.string	"TorqueStruct"
	.section	.text.unlikely._ZNK2v88internal6torque10StructType24GetGeneratedTypeNameImplB5cxx11Ev,"ax",@progbits
	.align 2
.LCOLDB35:
	.section	.text._ZNK2v88internal6torque10StructType24GetGeneratedTypeNameImplB5cxx11Ev,"ax",@progbits
.LHOTB35:
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal6torque10StructType24GetGeneratedTypeNameImplB5cxx11Ev
	.type	_ZNK2v88internal6torque10StructType24GetGeneratedTypeNameImplB5cxx11Ev, @function
_ZNK2v88internal6torque10StructType24GetGeneratedTypeNameImplB5cxx11Ev:
.LFB6558:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6558
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-64(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
.LEHB115:
	call	_ZNK2v88internal6torque10StructType11MangledNameB5cxx11Ev
.LEHE115:
	movl	$12, %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
	leaq	.LC34(%rip), %rcx
	movq	%r13, %rdi
.LEHB116:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.LEHE116:
	leaq	16(%r12), %rdx
	movq	%rdx, (%r12)
	movq	(%rax), %rcx
	leaq	16(%rax), %rdx
	cmpq	%rdx, %rcx
	je	.L997
	movq	%rcx, (%r12)
	movq	16(%rax), %rcx
	movq	%rcx, 16(%r12)
.L990:
	movq	8(%rax), %rcx
	movq	%rdx, (%rax)
	movq	-64(%rbp), %rdi
	movq	$0, 8(%rax)
	movq	%rcx, 8(%r12)
	movb	$0, 16(%rax)
	leaq	-48(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L988
	call	_ZdlPv@PLT
.L988:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L998
	addq	$48, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L997:
	.cfi_restore_state
	movdqu	16(%rax), %xmm0
	movups	%xmm0, 16(%r12)
	jmp	.L990
.L998:
	call	__stack_chk_fail@PLT
.L995:
	endbr64
	movq	%rax, %r12
	jmp	.L992
	.section	.gcc_except_table._ZNK2v88internal6torque10StructType24GetGeneratedTypeNameImplB5cxx11Ev,"a",@progbits
.LLSDA6558:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6558-.LLSDACSB6558
.LLSDACSB6558:
	.uleb128 .LEHB115-.LFB6558
	.uleb128 .LEHE115-.LEHB115
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB116-.LFB6558
	.uleb128 .LEHE116-.LEHB116
	.uleb128 .L995-.LFB6558
	.uleb128 0
.LLSDACSE6558:
	.section	.text._ZNK2v88internal6torque10StructType24GetGeneratedTypeNameImplB5cxx11Ev
	.cfi_endproc
	.section	.text.unlikely._ZNK2v88internal6torque10StructType24GetGeneratedTypeNameImplB5cxx11Ev
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6558
	.type	_ZNK2v88internal6torque10StructType24GetGeneratedTypeNameImplB5cxx11Ev.cold, @function
_ZNK2v88internal6torque10StructType24GetGeneratedTypeNameImplB5cxx11Ev.cold:
.LFSB6558:
.L992:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movq	-64(%rbp), %rdi
	leaq	-48(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L993
	call	_ZdlPv@PLT
.L993:
	movq	%r12, %rdi
.LEHB117:
	call	_Unwind_Resume@PLT
.LEHE117:
	.cfi_endproc
.LFE6558:
	.section	.gcc_except_table._ZNK2v88internal6torque10StructType24GetGeneratedTypeNameImplB5cxx11Ev
.LLSDAC6558:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6558-.LLSDACSBC6558
.LLSDACSBC6558:
	.uleb128 .LEHB117-.LCOLDB35
	.uleb128 .LEHE117-.LEHB117
	.uleb128 0
	.uleb128 0
.LLSDACSEC6558:
	.section	.text.unlikely._ZNK2v88internal6torque10StructType24GetGeneratedTypeNameImplB5cxx11Ev
	.section	.text._ZNK2v88internal6torque10StructType24GetGeneratedTypeNameImplB5cxx11Ev
	.size	_ZNK2v88internal6torque10StructType24GetGeneratedTypeNameImplB5cxx11Ev, .-_ZNK2v88internal6torque10StructType24GetGeneratedTypeNameImplB5cxx11Ev
	.section	.text.unlikely._ZNK2v88internal6torque10StructType24GetGeneratedTypeNameImplB5cxx11Ev
	.size	_ZNK2v88internal6torque10StructType24GetGeneratedTypeNameImplB5cxx11Ev.cold, .-_ZNK2v88internal6torque10StructType24GetGeneratedTypeNameImplB5cxx11Ev.cold
.LCOLDE35:
	.section	.text._ZNK2v88internal6torque10StructType24GetGeneratedTypeNameImplB5cxx11Ev
.LHOTE35:
	.section	.rodata._ZNK2v88internal6torque10StructType16ToExplicitStringB5cxx11Ev.str1.1,"aMS",@progbits,1
.LC36:
	.string	"struct "
	.section	.text.unlikely._ZNK2v88internal6torque10StructType16ToExplicitStringB5cxx11Ev,"ax",@progbits
	.align 2
.LCOLDB37:
	.section	.text._ZNK2v88internal6torque10StructType16ToExplicitStringB5cxx11Ev,"ax",@progbits
.LHOTB37:
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal6torque10StructType16ToExplicitStringB5cxx11Ev
	.type	_ZNK2v88internal6torque10StructType16ToExplicitStringB5cxx11Ev, @function
_ZNK2v88internal6torque10StructType16ToExplicitStringB5cxx11Ev:
.LFB6578:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6578
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-448(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-320(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$440, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r15, -464(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	movq	%rax, -320(%rbp)
	xorl	%eax, %eax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movw	%ax, -96(%rbp)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	$0, -104(%rbp)
	movq	%rax, -448(%rbp)
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	movq	-448(%rbp), %rax
	movq	$0, -440(%rbp)
	movq	-24(%rax), %rdi
	addq	%r15, %rdi
.LEHB118:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE118:
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	leaq	-432(%rbp), %r14
	xorl	%esi, %esi
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rdx, -432(%rbp,%rax)
	movq	-432(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r14, %rdi
.LEHB119:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE119:
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	leaq	-368(%rbp), %r15
	movq	.LC22(%rip), %xmm0
	movq	%r15, %rdi
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movhps	.LC23(%rip), %xmm0
	movq	%rcx, -448(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movaps	%xmm0, -432(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rax, -320(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r13, %rdi
	movl	$24, -360(%rbp)
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movq	%rax, -456(%rbp)
	movq	%rax, -352(%rbp)
	leaq	-424(%rbp), %rax
	movq	%rax, %rsi
	movb	$0, -336(%rbp)
	movq	$0, -344(%rbp)
	movq	%rax, -472(%rbp)
.LEHB120:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE120:
	movl	$7, %edx
	leaq	.LC36(%rip), %rsi
	movq	%r14, %rdi
.LEHB121:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	120(%rbx), %rdx
	movq	112(%rbx), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE121:
	movq	-384(%rbp), %rax
	leaq	16(%r12), %rbx
	movq	$0, 8(%r12)
	movq	%rbx, (%r12)
	movb	$0, 16(%r12)
	testq	%rax, %rax
	je	.L1004
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	ja	.L1021
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
.LEHB122:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L1006:
	movq	.LC22(%rip), %xmm0
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-352(%rbp), %rdi
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movhps	.LC24(%rip), %xmm0
	movq	%rax, -320(%rbp)
	movaps	%xmm0, -432(%rbp)
	cmpq	-456(%rbp), %rdi
	je	.L1008
	call	_ZdlPv@PLT
.L1008:
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%r15, %rdi
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	movq	%r13, %rdi
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rbx, -448(%rbp,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rdx, -432(%rbp,%rax)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rbx, -448(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	movq	$0, -440(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1022
	addq	$440, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1021:
	.cfi_restore_state
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L1006
	.p2align 4,,10
	.p2align 3
.L1004:
	leaq	-352(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
.LEHE122:
	jmp	.L1006
.L1022:
	call	__stack_chk_fail@PLT
.L1013:
	endbr64
	movq	%rax, %r13
	jmp	.L1011
.L1015:
	endbr64
	movq	%rax, %rbx
	jmp	.L1003
.L1014:
	endbr64
	movq	%rax, %rbx
	jmp	.L1002
.L1016:
	endbr64
	movq	%rax, %rbx
	jmp	.L1001
.L1017:
	endbr64
	movq	%rax, %r13
	jmp	.L1009
	.section	.gcc_except_table._ZNK2v88internal6torque10StructType16ToExplicitStringB5cxx11Ev,"a",@progbits
.LLSDA6578:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6578-.LLSDACSB6578
.LLSDACSB6578:
	.uleb128 .LEHB118-.LFB6578
	.uleb128 .LEHE118-.LEHB118
	.uleb128 .L1014-.LFB6578
	.uleb128 0
	.uleb128 .LEHB119-.LFB6578
	.uleb128 .LEHE119-.LEHB119
	.uleb128 .L1016-.LFB6578
	.uleb128 0
	.uleb128 .LEHB120-.LFB6578
	.uleb128 .LEHE120-.LEHB120
	.uleb128 .L1015-.LFB6578
	.uleb128 0
	.uleb128 .LEHB121-.LFB6578
	.uleb128 .LEHE121-.LEHB121
	.uleb128 .L1013-.LFB6578
	.uleb128 0
	.uleb128 .LEHB122-.LFB6578
	.uleb128 .LEHE122-.LEHB122
	.uleb128 .L1017-.LFB6578
	.uleb128 0
.LLSDACSE6578:
	.section	.text._ZNK2v88internal6torque10StructType16ToExplicitStringB5cxx11Ev
	.cfi_endproc
	.section	.text.unlikely._ZNK2v88internal6torque10StructType16ToExplicitStringB5cxx11Ev
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6578
	.type	_ZNK2v88internal6torque10StructType16ToExplicitStringB5cxx11Ev.cold, @function
_ZNK2v88internal6torque10StructType16ToExplicitStringB5cxx11Ev.cold:
.LFSB6578:
.L1009:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	(%r12), %rdi
	cmpq	%rdi, %rbx
	je	.L1011
	call	_ZdlPv@PLT
.L1011:
	movq	-464(%rbp), %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r13, %rdi
.LEHB123:
	call	_Unwind_Resume@PLT
.L1003:
	movq	-472(%rbp), %rdi
	call	_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED1Ev
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -432(%rbp,%rax)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	movq	$0, -440(%rbp)
.L1002:
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	%rbx, %rdi
	call	_Unwind_Resume@PLT
.LEHE123:
.L1001:
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	movq	$0, -440(%rbp)
	jmp	.L1002
	.cfi_endproc
.LFE6578:
	.section	.gcc_except_table._ZNK2v88internal6torque10StructType16ToExplicitStringB5cxx11Ev
.LLSDAC6578:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6578-.LLSDACSBC6578
.LLSDACSBC6578:
	.uleb128 .LEHB123-.LCOLDB37
	.uleb128 .LEHE123-.LEHB123
	.uleb128 0
	.uleb128 0
.LLSDACSEC6578:
	.section	.text.unlikely._ZNK2v88internal6torque10StructType16ToExplicitStringB5cxx11Ev
	.section	.text._ZNK2v88internal6torque10StructType16ToExplicitStringB5cxx11Ev
	.size	_ZNK2v88internal6torque10StructType16ToExplicitStringB5cxx11Ev, .-_ZNK2v88internal6torque10StructType16ToExplicitStringB5cxx11Ev
	.section	.text.unlikely._ZNK2v88internal6torque10StructType16ToExplicitStringB5cxx11Ev
	.size	_ZNK2v88internal6torque10StructType16ToExplicitStringB5cxx11Ev.cold, .-_ZNK2v88internal6torque10StructType16ToExplicitStringB5cxx11Ev.cold
.LCOLDE37:
	.section	.text._ZNK2v88internal6torque10StructType16ToExplicitStringB5cxx11Ev
.LHOTE37:
	.section	.rodata._ZNK2v88internal6torque9ClassType16ToExplicitStringB5cxx11Ev.str1.1,"aMS",@progbits,1
.LC38:
	.string	"class "
	.section	.text.unlikely._ZNK2v88internal6torque9ClassType16ToExplicitStringB5cxx11Ev,"ax",@progbits
	.align 2
.LCOLDB39:
	.section	.text._ZNK2v88internal6torque9ClassType16ToExplicitStringB5cxx11Ev,"ax",@progbits
.LHOTB39:
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal6torque9ClassType16ToExplicitStringB5cxx11Ev
	.type	_ZNK2v88internal6torque9ClassType16ToExplicitStringB5cxx11Ev, @function
_ZNK2v88internal6torque9ClassType16ToExplicitStringB5cxx11Ev:
.LFB6591:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6591
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-448(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-320(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$440, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r15, -464(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	movq	%rax, -320(%rbp)
	xorl	%eax, %eax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movw	%ax, -96(%rbp)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	$0, -104(%rbp)
	movq	%rax, -448(%rbp)
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	movq	-448(%rbp), %rax
	movq	$0, -440(%rbp)
	movq	-24(%rax), %rdi
	addq	%r15, %rdi
.LEHB124:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE124:
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	leaq	-432(%rbp), %r14
	xorl	%esi, %esi
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rdx, -432(%rbp,%rax)
	movq	-432(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r14, %rdi
.LEHB125:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE125:
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	leaq	-368(%rbp), %r15
	movq	.LC22(%rip), %xmm0
	movq	%r15, %rdi
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movhps	.LC23(%rip), %xmm0
	movq	%rcx, -448(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movaps	%xmm0, -432(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rax, -320(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r13, %rdi
	movl	$24, -360(%rbp)
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movq	%rax, -456(%rbp)
	movq	%rax, -352(%rbp)
	leaq	-424(%rbp), %rax
	movq	%rax, %rsi
	movb	$0, -336(%rbp)
	movq	$0, -344(%rbp)
	movq	%rax, -472(%rbp)
.LEHB126:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE126:
	movl	$6, %edx
	leaq	.LC38(%rip), %rsi
	movq	%r14, %rdi
.LEHB127:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	120(%rbx), %rdx
	movq	112(%rbx), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE127:
	movq	-384(%rbp), %rax
	leaq	16(%r12), %rbx
	movq	$0, 8(%r12)
	movq	%rbx, (%r12)
	movb	$0, 16(%r12)
	testq	%rax, %rax
	je	.L1028
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	ja	.L1045
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
.LEHB128:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L1030:
	movq	.LC22(%rip), %xmm0
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-352(%rbp), %rdi
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movhps	.LC24(%rip), %xmm0
	movq	%rax, -320(%rbp)
	movaps	%xmm0, -432(%rbp)
	cmpq	-456(%rbp), %rdi
	je	.L1032
	call	_ZdlPv@PLT
.L1032:
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%r15, %rdi
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	movq	%r13, %rdi
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rbx, -448(%rbp,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rdx, -432(%rbp,%rax)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rbx, -448(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	movq	$0, -440(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1046
	addq	$440, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1045:
	.cfi_restore_state
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L1030
	.p2align 4,,10
	.p2align 3
.L1028:
	leaq	-352(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
.LEHE128:
	jmp	.L1030
.L1046:
	call	__stack_chk_fail@PLT
.L1037:
	endbr64
	movq	%rax, %r13
	jmp	.L1035
.L1039:
	endbr64
	movq	%rax, %rbx
	jmp	.L1027
.L1038:
	endbr64
	movq	%rax, %rbx
	jmp	.L1026
.L1040:
	endbr64
	movq	%rax, %rbx
	jmp	.L1025
.L1041:
	endbr64
	movq	%rax, %r13
	jmp	.L1033
	.section	.gcc_except_table._ZNK2v88internal6torque9ClassType16ToExplicitStringB5cxx11Ev,"a",@progbits
.LLSDA6591:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6591-.LLSDACSB6591
.LLSDACSB6591:
	.uleb128 .LEHB124-.LFB6591
	.uleb128 .LEHE124-.LEHB124
	.uleb128 .L1038-.LFB6591
	.uleb128 0
	.uleb128 .LEHB125-.LFB6591
	.uleb128 .LEHE125-.LEHB125
	.uleb128 .L1040-.LFB6591
	.uleb128 0
	.uleb128 .LEHB126-.LFB6591
	.uleb128 .LEHE126-.LEHB126
	.uleb128 .L1039-.LFB6591
	.uleb128 0
	.uleb128 .LEHB127-.LFB6591
	.uleb128 .LEHE127-.LEHB127
	.uleb128 .L1037-.LFB6591
	.uleb128 0
	.uleb128 .LEHB128-.LFB6591
	.uleb128 .LEHE128-.LEHB128
	.uleb128 .L1041-.LFB6591
	.uleb128 0
.LLSDACSE6591:
	.section	.text._ZNK2v88internal6torque9ClassType16ToExplicitStringB5cxx11Ev
	.cfi_endproc
	.section	.text.unlikely._ZNK2v88internal6torque9ClassType16ToExplicitStringB5cxx11Ev
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6591
	.type	_ZNK2v88internal6torque9ClassType16ToExplicitStringB5cxx11Ev.cold, @function
_ZNK2v88internal6torque9ClassType16ToExplicitStringB5cxx11Ev.cold:
.LFSB6591:
.L1033:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	(%r12), %rdi
	cmpq	%rdi, %rbx
	je	.L1035
	call	_ZdlPv@PLT
.L1035:
	movq	-464(%rbp), %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r13, %rdi
.LEHB129:
	call	_Unwind_Resume@PLT
.L1027:
	movq	-472(%rbp), %rdi
	call	_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED1Ev
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -432(%rbp,%rax)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	movq	$0, -440(%rbp)
.L1026:
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	%rbx, %rdi
	call	_Unwind_Resume@PLT
.LEHE129:
.L1025:
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	movq	$0, -440(%rbp)
	jmp	.L1026
	.cfi_endproc
.LFE6591:
	.section	.gcc_except_table._ZNK2v88internal6torque9ClassType16ToExplicitStringB5cxx11Ev
.LLSDAC6591:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6591-.LLSDACSBC6591
.LLSDACSBC6591:
	.uleb128 .LEHB129-.LCOLDB39
	.uleb128 .LEHE129-.LEHB129
	.uleb128 0
	.uleb128 0
.LLSDACSEC6591:
	.section	.text.unlikely._ZNK2v88internal6torque9ClassType16ToExplicitStringB5cxx11Ev
	.section	.text._ZNK2v88internal6torque9ClassType16ToExplicitStringB5cxx11Ev
	.size	_ZNK2v88internal6torque9ClassType16ToExplicitStringB5cxx11Ev, .-_ZNK2v88internal6torque9ClassType16ToExplicitStringB5cxx11Ev
	.section	.text.unlikely._ZNK2v88internal6torque9ClassType16ToExplicitStringB5cxx11Ev
	.size	_ZNK2v88internal6torque9ClassType16ToExplicitStringB5cxx11Ev.cold, .-_ZNK2v88internal6torque9ClassType16ToExplicitStringB5cxx11Ev.cold
.LCOLDE39:
	.section	.text._ZNK2v88internal6torque9ClassType16ToExplicitStringB5cxx11Ev
.LHOTE39:
	.section	.text.unlikely._ZN2v88internal6torqueL7MessageIJRA38_KcNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA41_S3_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0,"ax",@progbits
.LCOLDB40:
	.section	.text._ZN2v88internal6torqueL7MessageIJRA38_KcNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA41_S3_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0,"ax",@progbits
.LHOTB40:
	.p2align 4
	.type	_ZN2v88internal6torqueL7MessageIJRA38_KcNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA41_S3_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0, @function
_ZN2v88internal6torqueL7MessageIJRA38_KcNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA41_S3_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0:
.LFB13126:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA13126
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-448(%rbp), %r15
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-432(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r15, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$456, %rsp
	movq	%rsi, -488(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
.LEHB130:
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEC1Ev@PLT
.LEHE130:
	movq	-488(%rbp), %rsi
	movq	%r13, %rdi
.LEHB131:
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	8(%rbx), %rdx
	movq	(%rbx), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	leaq	-480(%rbp), %r13
	leaq	-424(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNKSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEE3strEv@PLT
.LEHE131:
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
.LEHB132:
	call	_ZN2v88internal6torque14MessageBuilderC1ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_13TorqueMessage4KindE@PLT
.LEHE132:
	movq	-480(%rbp), %rdi
	leaq	-464(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1048
	call	_ZdlPv@PLT
.L1048:
	movq	%r15, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1057
	addq	$456, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1057:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
.L1054:
	endbr64
	movq	%rax, %r12
	jmp	.L1049
.L1053:
	endbr64
	movq	%rax, %r12
	jmp	.L1051
	.section	.gcc_except_table._ZN2v88internal6torqueL7MessageIJRA38_KcNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA41_S3_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0,"a",@progbits
.LLSDA13126:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE13126-.LLSDACSB13126
.LLSDACSB13126:
	.uleb128 .LEHB130-.LFB13126
	.uleb128 .LEHE130-.LEHB130
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB131-.LFB13126
	.uleb128 .LEHE131-.LEHB131
	.uleb128 .L1053-.LFB13126
	.uleb128 0
	.uleb128 .LEHB132-.LFB13126
	.uleb128 .LEHE132-.LEHB132
	.uleb128 .L1054-.LFB13126
	.uleb128 0
.LLSDACSE13126:
	.section	.text._ZN2v88internal6torqueL7MessageIJRA38_KcNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA41_S3_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torqueL7MessageIJRA38_KcNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA41_S3_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC13126
	.type	_ZN2v88internal6torqueL7MessageIJRA38_KcNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA41_S3_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0.cold, @function
_ZN2v88internal6torqueL7MessageIJRA38_KcNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA41_S3_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0.cold:
.LFSB13126:
.L1049:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-480(%rbp), %rdi
	leaq	-464(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1051
	call	_ZdlPv@PLT
.L1051:
	movq	%r15, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r12, %rdi
.LEHB133:
	call	_Unwind_Resume@PLT
.LEHE133:
	.cfi_endproc
.LFE13126:
	.section	.gcc_except_table._ZN2v88internal6torqueL7MessageIJRA38_KcNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA41_S3_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
.LLSDAC13126:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC13126-.LLSDACSBC13126
.LLSDACSBC13126:
	.uleb128 .LEHB133-.LCOLDB40
	.uleb128 .LEHE133-.LEHB133
	.uleb128 0
	.uleb128 0
.LLSDACSEC13126:
	.section	.text.unlikely._ZN2v88internal6torqueL7MessageIJRA38_KcNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA41_S3_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
	.section	.text._ZN2v88internal6torqueL7MessageIJRA38_KcNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA41_S3_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
	.size	_ZN2v88internal6torqueL7MessageIJRA38_KcNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA41_S3_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0, .-_ZN2v88internal6torqueL7MessageIJRA38_KcNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA41_S3_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
	.section	.text.unlikely._ZN2v88internal6torqueL7MessageIJRA38_KcNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA41_S3_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
	.size	_ZN2v88internal6torqueL7MessageIJRA38_KcNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA41_S3_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0.cold, .-_ZN2v88internal6torqueL7MessageIJRA38_KcNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA41_S3_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0.cold
.LCOLDE40:
	.section	.text._ZN2v88internal6torqueL7MessageIJRA38_KcNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA41_S3_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
.LHOTE40:
	.section	.rodata._ZNK2v88internal6torque4Type20GetGeneratedTypeNameB5cxx11Ev.str1.1,"aMS",@progbits,1
.LC41:
	.string	"compiler::TNode<>"
	.section	.rodata._ZNK2v88internal6torque4Type20GetGeneratedTypeNameB5cxx11Ev.str1.8,"aMS",@progbits,1
	.align 8
.LC42:
	.string	"'. Use 'generates' clause in definition."
	.align 8
.LC43:
	.string	"Generated type is required for type '"
	.section	.text.unlikely._ZNK2v88internal6torque4Type20GetGeneratedTypeNameB5cxx11Ev,"ax",@progbits
	.align 2
.LCOLDB44:
	.section	.text._ZNK2v88internal6torque4Type20GetGeneratedTypeNameB5cxx11Ev,"ax",@progbits
.LHOTB44:
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal6torque4Type20GetGeneratedTypeNameB5cxx11Ev
	.type	_ZNK2v88internal6torque4Type20GetGeneratedTypeNameB5cxx11Ev, @function
_ZNK2v88internal6torque4Type20GetGeneratedTypeNameB5cxx11Ev:
.LFB6515:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6515
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	subq	$112, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
.LEHB134:
	call	*80(%rax)
.LEHE134:
	cmpq	$0, 8(%r12)
	je	.L1061
	leaq	.LC41(%rip), %rsi
	movq	%r12, %rdi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	je	.L1061
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1077
	addq	$112, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1061:
	.cfi_restore_state
	leaq	-144(%rbp), %r15
	movq	%r13, %rsi
	movq	%r15, %rdi
.LEHB135:
	call	_ZNK2v88internal6torque4Type8ToStringB5cxx11Ev
.LEHE135:
	leaq	-112(%rbp), %r14
	leaq	.LC42(%rip), %rcx
	movq	%r15, %rdx
	leaq	.LC43(%rip), %rsi
	movq	%r14, %rdi
.LEHB136:
	call	_ZN2v88internal6torqueL7MessageIJRA38_KcNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA41_S3_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
.LEHE136:
	movq	%r14, %rdi
.LEHB137:
	call	_ZNK2v88internal6torque14MessageBuilder5ThrowEv@PLT
.LEHE137:
.L1077:
	call	__stack_chk_fail@PLT
.L1070:
	endbr64
	movq	%rax, %r13
	jmp	.L1062
.L1069:
	endbr64
	movq	%rax, %r13
	jmp	.L1063
.L1068:
	endbr64
	movq	%rax, %r13
	jmp	.L1065
	.section	.gcc_except_table._ZNK2v88internal6torque4Type20GetGeneratedTypeNameB5cxx11Ev,"a",@progbits
.LLSDA6515:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6515-.LLSDACSB6515
.LLSDACSB6515:
	.uleb128 .LEHB134-.LFB6515
	.uleb128 .LEHE134-.LEHB134
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB135-.LFB6515
	.uleb128 .LEHE135-.LEHB135
	.uleb128 .L1068-.LFB6515
	.uleb128 0
	.uleb128 .LEHB136-.LFB6515
	.uleb128 .LEHE136-.LEHB136
	.uleb128 .L1069-.LFB6515
	.uleb128 0
	.uleb128 .LEHB137-.LFB6515
	.uleb128 .LEHE137-.LEHB137
	.uleb128 .L1070-.LFB6515
	.uleb128 0
.LLSDACSE6515:
	.section	.text._ZNK2v88internal6torque4Type20GetGeneratedTypeNameB5cxx11Ev
	.cfi_endproc
	.section	.text.unlikely._ZNK2v88internal6torque4Type20GetGeneratedTypeNameB5cxx11Ev
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6515
	.type	_ZNK2v88internal6torque4Type20GetGeneratedTypeNameB5cxx11Ev.cold, @function
_ZNK2v88internal6torque4Type20GetGeneratedTypeNameB5cxx11Ev.cold:
.LFSB6515:
.L1062:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	%r14, %rdi
	call	_ZN2v88internal6torque14MessageBuilderD1Ev
.L1063:
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1065
	call	_ZdlPv@PLT
.L1065:
	movq	(%r12), %rdi
	addq	$16, %r12
	cmpq	%r12, %rdi
	je	.L1066
	call	_ZdlPv@PLT
.L1066:
	movq	%r13, %rdi
.LEHB138:
	call	_Unwind_Resume@PLT
.LEHE138:
	.cfi_endproc
.LFE6515:
	.section	.gcc_except_table._ZNK2v88internal6torque4Type20GetGeneratedTypeNameB5cxx11Ev
.LLSDAC6515:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6515-.LLSDACSBC6515
.LLSDACSBC6515:
	.uleb128 .LEHB138-.LCOLDB44
	.uleb128 .LEHE138-.LEHB138
	.uleb128 0
	.uleb128 0
.LLSDACSEC6515:
	.section	.text.unlikely._ZNK2v88internal6torque4Type20GetGeneratedTypeNameB5cxx11Ev
	.section	.text._ZNK2v88internal6torque4Type20GetGeneratedTypeNameB5cxx11Ev
	.size	_ZNK2v88internal6torque4Type20GetGeneratedTypeNameB5cxx11Ev, .-_ZNK2v88internal6torque4Type20GetGeneratedTypeNameB5cxx11Ev
	.section	.text.unlikely._ZNK2v88internal6torque4Type20GetGeneratedTypeNameB5cxx11Ev
	.size	_ZNK2v88internal6torque4Type20GetGeneratedTypeNameB5cxx11Ev.cold, .-_ZNK2v88internal6torque4Type20GetGeneratedTypeNameB5cxx11Ev.cold
.LCOLDE44:
	.section	.text._ZNK2v88internal6torque4Type20GetGeneratedTypeNameB5cxx11Ev
.LHOTE44:
	.section	.text._ZNK2v88internal6torque18BuiltinPointerType24GetGeneratedTypeNameImplB5cxx11Ev,"axG",@progbits,_ZNK2v88internal6torque18BuiltinPointerType24GetGeneratedTypeNameImplB5cxx11Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal6torque18BuiltinPointerType24GetGeneratedTypeNameImplB5cxx11Ev
	.type	_ZNK2v88internal6torque18BuiltinPointerType24GetGeneratedTypeNameImplB5cxx11Ev, @function
_ZNK2v88internal6torque18BuiltinPointerType24GetGeneratedTypeNameImplB5cxx11Ev:
.LFB5633:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA5633
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	subq	$112, %rsp
	movq	16(%rsi), %r13
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	0(%r13), %rax
	movq	%r13, %rsi
.LEHB139:
	call	*80(%rax)
.LEHE139:
	cmpq	$0, 8(%r12)
	je	.L1081
	leaq	.LC41(%rip), %rsi
	movq	%r12, %rdi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	je	.L1081
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1098
	addq	$112, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1081:
	.cfi_restore_state
	leaq	-144(%rbp), %r15
	movq	%r13, %rsi
	movq	%r15, %rdi
.LEHB140:
	call	_ZNK2v88internal6torque4Type8ToStringB5cxx11Ev
.LEHE140:
	leaq	-112(%rbp), %r14
	leaq	.LC42(%rip), %rcx
	movq	%r15, %rdx
	leaq	.LC43(%rip), %rsi
	movq	%r14, %rdi
.LEHB141:
	call	_ZN2v88internal6torqueL7MessageIJRA38_KcNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA41_S3_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
.LEHE141:
	movq	%r14, %rdi
.LEHB142:
	call	_ZNK2v88internal6torque14MessageBuilder5ThrowEv@PLT
.LEHE142:
.L1098:
	call	__stack_chk_fail@PLT
.L1091:
	endbr64
	movq	%rax, %r13
	jmp	.L1083
.L1090:
	endbr64
	movq	%rax, %r13
	jmp	.L1084
.L1089:
	endbr64
	movq	%rax, %r13
	jmp	.L1086
.L1083:
	movq	%r14, %rdi
	call	_ZN2v88internal6torque14MessageBuilderD1Ev
.L1084:
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1086
	call	_ZdlPv@PLT
.L1086:
	movq	(%r12), %rdi
	addq	$16, %r12
	cmpq	%r12, %rdi
	je	.L1087
	call	_ZdlPv@PLT
.L1087:
	movq	%r13, %rdi
.LEHB143:
	call	_Unwind_Resume@PLT
.LEHE143:
	.cfi_endproc
.LFE5633:
	.section	.gcc_except_table._ZNK2v88internal6torque18BuiltinPointerType24GetGeneratedTypeNameImplB5cxx11Ev,"aG",@progbits,_ZNK2v88internal6torque18BuiltinPointerType24GetGeneratedTypeNameImplB5cxx11Ev,comdat
.LLSDA5633:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE5633-.LLSDACSB5633
.LLSDACSB5633:
	.uleb128 .LEHB139-.LFB5633
	.uleb128 .LEHE139-.LEHB139
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB140-.LFB5633
	.uleb128 .LEHE140-.LEHB140
	.uleb128 .L1089-.LFB5633
	.uleb128 0
	.uleb128 .LEHB141-.LFB5633
	.uleb128 .LEHE141-.LEHB141
	.uleb128 .L1090-.LFB5633
	.uleb128 0
	.uleb128 .LEHB142-.LFB5633
	.uleb128 .LEHE142-.LEHB142
	.uleb128 .L1091-.LFB5633
	.uleb128 0
	.uleb128 .LEHB143-.LFB5633
	.uleb128 .LEHE143-.LEHB143
	.uleb128 0
	.uleb128 0
.LLSDACSE5633:
	.section	.text._ZNK2v88internal6torque18BuiltinPointerType24GetGeneratedTypeNameImplB5cxx11Ev,"axG",@progbits,_ZNK2v88internal6torque18BuiltinPointerType24GetGeneratedTypeNameImplB5cxx11Ev,comdat
	.size	_ZNK2v88internal6torque18BuiltinPointerType24GetGeneratedTypeNameImplB5cxx11Ev, .-_ZNK2v88internal6torque18BuiltinPointerType24GetGeneratedTypeNameImplB5cxx11Ev
	.section	.text._ZNSt6vectorIN2v88internal6torque5FieldESaIS3_EED2Ev,"axG",@progbits,_ZNSt6vectorIN2v88internal6torque5FieldESaIS3_EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal6torque5FieldESaIS3_EED2Ev
	.type	_ZNSt6vectorIN2v88internal6torque5FieldESaIS3_EED2Ev, @function
_ZNSt6vectorIN2v88internal6torque5FieldESaIS3_EED2Ev:
.LFB7429:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	8(%rdi), %rbx
	movq	(%rdi), %r12
	cmpq	%r12, %rbx
	je	.L1100
	.p2align 4,,10
	.p2align 3
.L1104:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1101
	call	_ZdlPv@PLT
	addq	$104, %r12
	cmpq	%r12, %rbx
	jne	.L1104
.L1102:
	movq	0(%r13), %r12
.L1100:
	testq	%r12, %r12
	je	.L1099
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L1101:
	.cfi_restore_state
	addq	$104, %r12
	cmpq	%r12, %rbx
	jne	.L1104
	jmp	.L1102
	.p2align 4,,10
	.p2align 3
.L1099:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7429:
	.size	_ZNSt6vectorIN2v88internal6torque5FieldESaIS3_EED2Ev, .-_ZNSt6vectorIN2v88internal6torque5FieldESaIS3_EED2Ev
	.weak	_ZNSt6vectorIN2v88internal6torque5FieldESaIS3_EED1Ev
	.set	_ZNSt6vectorIN2v88internal6torque5FieldESaIS3_EED1Ev,_ZNSt6vectorIN2v88internal6torque5FieldESaIS3_EED2Ev
	.section	.text._ZN2v88internal6torque11ReportErrorIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_,"axG",@progbits,_ZN2v88internal6torque11ReportErrorIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_,comdat
	.p2align 4
	.weak	_ZN2v88internal6torque11ReportErrorIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_
	.type	_ZN2v88internal6torque11ReportErrorIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_, @function
_ZN2v88internal6torque11ReportErrorIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_:
.LFB7872:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA7872
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-432(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%r13, %rdi
	subq	$496, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
.LEHB144:
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEC1Ev@PLT
.LEHE144:
	movq	8(%rbx), %rdx
	movq	(%rbx), %rsi
	leaq	-416(%rbp), %rdi
.LEHB145:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	-528(%rbp), %r14
	leaq	-408(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZNKSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEE3strEv@PLT
.LEHE145:
	leaq	-496(%rbp), %r12
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
.LEHB146:
	call	_ZN2v88internal6torque14MessageBuilderC1ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_13TorqueMessage4KindE@PLT
.LEHE146:
	movq	-528(%rbp), %rdi
	leaq	-512(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1108
	call	_ZdlPv@PLT
.L1108:
	movq	%r13, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r12, %rdi
.LEHB147:
	call	_ZNK2v88internal6torque14MessageBuilder5ThrowEv@PLT
.LEHE147:
.L1115:
	endbr64
	movq	%rax, %r12
	jmp	.L1111
.L1114:
	endbr64
	movq	%rax, %r13
	jmp	.L1112
.L1116:
	endbr64
	movq	%rax, %r12
.L1109:
	movq	-528(%rbp), %rdi
	leaq	-512(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1111
	call	_ZdlPv@PLT
.L1111:
	movq	%r13, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r12, %rdi
.LEHB148:
	call	_Unwind_Resume@PLT
.L1112:
	movq	%r12, %rdi
	call	_ZN2v88internal6torque14MessageBuilderD1Ev
	movq	%r13, %rdi
	call	_Unwind_Resume@PLT
.LEHE148:
	.cfi_endproc
.LFE7872:
	.section	.gcc_except_table._ZN2v88internal6torque11ReportErrorIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_,"aG",@progbits,_ZN2v88internal6torque11ReportErrorIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_,comdat
.LLSDA7872:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE7872-.LLSDACSB7872
.LLSDACSB7872:
	.uleb128 .LEHB144-.LFB7872
	.uleb128 .LEHE144-.LEHB144
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB145-.LFB7872
	.uleb128 .LEHE145-.LEHB145
	.uleb128 .L1115-.LFB7872
	.uleb128 0
	.uleb128 .LEHB146-.LFB7872
	.uleb128 .LEHE146-.LEHB146
	.uleb128 .L1116-.LFB7872
	.uleb128 0
	.uleb128 .LEHB147-.LFB7872
	.uleb128 .LEHE147-.LEHB147
	.uleb128 .L1114-.LFB7872
	.uleb128 0
	.uleb128 .LEHB148-.LFB7872
	.uleb128 .LEHE148-.LEHB148
	.uleb128 0
	.uleb128 0
.LLSDACSE7872:
	.section	.text._ZN2v88internal6torque11ReportErrorIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_,"axG",@progbits,_ZN2v88internal6torque11ReportErrorIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_,comdat
	.size	_ZN2v88internal6torque11ReportErrorIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_, .-_ZN2v88internal6torque11ReportErrorIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_
	.section	.rodata._ZN2v88internal6torque4Type15CommonSupertypeEPKS2_S4_.str1.1,"aMS",@progbits,1
.LC45:
	.string	"types "
.LC46:
	.string	" and "
.LC47:
	.string	" have no common supertype"
	.section	.text.unlikely._ZN2v88internal6torque4Type15CommonSupertypeEPKS2_S4_,"ax",@progbits
	.align 2
.LCOLDB48:
	.section	.text._ZN2v88internal6torque4Type15CommonSupertypeEPKS2_S4_,"ax",@progbits
.LHOTB48:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque4Type15CommonSupertypeEPKS2_S4_
	.type	_ZN2v88internal6torque4Type15CommonSupertypeEPKS2_S4_, @function
_ZN2v88internal6torque4Type15CommonSupertypeEPKS2_S4_:
.LFB6512:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6512
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$232, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rsi), %rax
	testq	%rdx, %rdx
	je	.L1119
	movq	%rdx, %rdi
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L1121:
	movq	16(%rdi), %rdi
	addl	$1, %ecx
	testq	%rdi, %rdi
	jne	.L1121
	testq	%rax, %rax
	je	.L1125
.L1163:
	movq	%rax, %rdi
	xorl	%r8d, %r8d
	.p2align 4,,10
	.p2align 3
.L1123:
	movq	16(%rdi), %rdi
	addl	$1, %r8d
	testq	%rdi, %rdi
	jne	.L1123
	subl	%r8d, %ecx
	testl	%ecx, %ecx
	jle	.L1195
.L1125:
	movl	%ecx, %eax
	subl	$1, %eax
	je	.L1127
	.p2align 4,,10
	.p2align 3
.L1128:
	movq	16(%rdx), %rdx
	subl	$1, %eax
	jne	.L1128
.L1127:
	movq	%rsi, %rax
.L1126:
	testq	%rax, %rax
	je	.L1129
	testq	%rdx, %rdx
	jne	.L1134
	jmp	.L1129
	.p2align 4,,10
	.p2align 3
.L1196:
	movq	16(%rdx), %rdx
	movq	16(%rax), %rax
	testq	%rdx, %rdx
	je	.L1129
	testq	%rax, %rax
	je	.L1129
.L1134:
	cmpq	%rax, %rdx
	jne	.L1196
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1197
	addq	$232, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1119:
	.cfi_restore_state
	testq	%rax, %rax
	je	.L1169
	xorl	%ecx, %ecx
	jmp	.L1163
.L1195:
	je	.L1169
	movq	%r12, %rdx
	addl	$1, %ecx
	je	.L1134
	.p2align 4,,10
	.p2align 3
.L1131:
	movq	16(%rax), %rax
	addl	$1, %ecx
	je	.L1126
	movq	16(%rax), %rax
	addl	$1, %ecx
	jne	.L1131
	jmp	.L1126
.L1169:
	movq	%r12, %rdx
	movq	%rsi, %rax
	jmp	.L1134
.L1129:
	leaq	-160(%rbp), %r14
	leaq	-256(%rbp), %r13
	movq	%r14, %rdi
.LEHB149:
	call	_ZNK2v88internal6torque4Type8ToStringB5cxx11Ev
.LEHE149:
	movq	%r12, %rsi
	movq	%r13, %rdi
.LEHB150:
	call	_ZNK2v88internal6torque4Type8ToStringB5cxx11Ev
.LEHE150:
	movl	$6, %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r13, %rdi
	leaq	.LC45(%rip), %rcx
.LEHB151:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.LEHE151:
	leaq	-208(%rbp), %r13
	leaq	16(%rax), %rdx
	movq	%r13, -224(%rbp)
	movq	(%rax), %rcx
	cmpq	%rdx, %rcx
	je	.L1198
	movq	%rcx, -224(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -208(%rbp)
.L1137:
	movq	8(%rax), %rcx
	movb	$0, 16(%rax)
	movq	%rcx, -216(%rbp)
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movabsq	$4611686018427387903, %rax
	subq	-216(%rbp), %rax
	cmpq	$4, %rax
	jbe	.L1199
	leaq	-224(%rbp), %rdi
	movl	$5, %edx
	leaq	.LC46(%rip), %rsi
.LEHB152:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.LEHE152:
	leaq	-176(%rbp), %r12
	leaq	16(%rax), %rdx
	movq	%r12, -192(%rbp)
	movq	(%rax), %rcx
	cmpq	%rdx, %rcx
	je	.L1200
	movq	%rcx, -192(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -176(%rbp)
.L1140:
	movq	8(%rax), %rcx
	movb	$0, 16(%rax)
	leaq	-144(%rbp), %rbx
	movq	%rcx, -184(%rbp)
	movq	%rdx, (%rax)
	movq	-192(%rbp), %rcx
	movq	$0, 8(%rax)
	movl	$15, %eax
	movq	-184(%rbp), %r8
	movq	-152(%rbp), %rdx
	cmpq	%r12, %rcx
	movq	%rax, %r9
	cmovne	-176(%rbp), %r9
	movq	-160(%rbp), %rsi
	leaq	(%r8,%rdx), %rdi
	cmpq	%r9, %rdi
	jbe	.L1142
	cmpq	%rbx, %rsi
	cmovne	-144(%rbp), %rax
	cmpq	%rax, %rdi
	jbe	.L1201
.L1142:
	leaq	-192(%rbp), %rdi
.LEHB153:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.LEHE153:
.L1144:
	leaq	-112(%rbp), %r14
	leaq	16(%rax), %rdx
	movq	%r14, -128(%rbp)
	movq	(%rax), %rcx
	cmpq	%rdx, %rcx
	je	.L1202
	movq	%rcx, -128(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -112(%rbp)
.L1146:
	movq	8(%rax), %rcx
	movb	$0, 16(%rax)
	movq	%rcx, -120(%rbp)
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movabsq	$4611686018427387903, %rax
	subq	-120(%rbp), %rax
	cmpq	$24, %rax
	jbe	.L1203
	leaq	-128(%rbp), %rdi
	movl	$25, %edx
	leaq	.LC47(%rip), %rsi
.LEHB154:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.LEHE154:
	leaq	-80(%rbp), %r15
	leaq	16(%rax), %rdx
	movq	%r15, -96(%rbp)
	movq	(%rax), %rcx
	cmpq	%rdx, %rcx
	je	.L1204
	movq	%rcx, -96(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -80(%rbp)
.L1149:
	movq	8(%rax), %rcx
	movb	$0, 16(%rax)
	leaq	-96(%rbp), %rdi
	movq	%rcx, -88(%rbp)
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
.LEHB155:
	call	_ZN2v88internal6torque11ReportErrorIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_
.LEHE155:
.L1200:
	movdqu	16(%rax), %xmm1
	movaps	%xmm1, -176(%rbp)
	jmp	.L1140
.L1201:
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r14, %rdi
.LEHB156:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7replaceEmmPKcm@PLT
.LEHE156:
	jmp	.L1144
.L1198:
	movdqu	16(%rax), %xmm0
	movaps	%xmm0, -208(%rbp)
	jmp	.L1137
.L1204:
	movdqu	16(%rax), %xmm3
	movaps	%xmm3, -80(%rbp)
	jmp	.L1149
.L1202:
	movdqu	16(%rax), %xmm2
	movaps	%xmm2, -112(%rbp)
	jmp	.L1146
.L1199:
	leaq	.LC2(%rip), %rdi
.LEHB157:
	call	_ZSt20__throw_length_errorPKc@PLT
.LEHE157:
.L1203:
	leaq	.LC2(%rip), %rdi
.LEHB158:
	call	_ZSt20__throw_length_errorPKc@PLT
.LEHE158:
.L1197:
	call	__stack_chk_fail@PLT
.L1173:
	endbr64
	movq	%rax, %r15
	jmp	.L1154
.L1175:
	endbr64
	movq	%rax, -264(%rbp)
	jmp	.L1150
.L1174:
	endbr64
	movq	%rax, %r15
	jmp	.L1152
.L1172:
	endbr64
	movq	%rax, %r12
	leaq	-144(%rbp), %rbx
	jmp	.L1156
.L1171:
	endbr64
	movq	%rax, %r12
	leaq	-144(%rbp), %rbx
	jmp	.L1158
.L1170:
	endbr64
	movq	%rax, %r12
	leaq	-144(%rbp), %rbx
	jmp	.L1160
	.section	.gcc_except_table._ZN2v88internal6torque4Type15CommonSupertypeEPKS2_S4_,"a",@progbits
.LLSDA6512:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6512-.LLSDACSB6512
.LLSDACSB6512:
	.uleb128 .LEHB149-.LFB6512
	.uleb128 .LEHE149-.LEHB149
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB150-.LFB6512
	.uleb128 .LEHE150-.LEHB150
	.uleb128 .L1170-.LFB6512
	.uleb128 0
	.uleb128 .LEHB151-.LFB6512
	.uleb128 .LEHE151-.LEHB151
	.uleb128 .L1171-.LFB6512
	.uleb128 0
	.uleb128 .LEHB152-.LFB6512
	.uleb128 .LEHE152-.LEHB152
	.uleb128 .L1172-.LFB6512
	.uleb128 0
	.uleb128 .LEHB153-.LFB6512
	.uleb128 .LEHE153-.LEHB153
	.uleb128 .L1173-.LFB6512
	.uleb128 0
	.uleb128 .LEHB154-.LFB6512
	.uleb128 .LEHE154-.LEHB154
	.uleb128 .L1174-.LFB6512
	.uleb128 0
	.uleb128 .LEHB155-.LFB6512
	.uleb128 .LEHE155-.LEHB155
	.uleb128 .L1175-.LFB6512
	.uleb128 0
	.uleb128 .LEHB156-.LFB6512
	.uleb128 .LEHE156-.LEHB156
	.uleb128 .L1173-.LFB6512
	.uleb128 0
	.uleb128 .LEHB157-.LFB6512
	.uleb128 .LEHE157-.LEHB157
	.uleb128 .L1172-.LFB6512
	.uleb128 0
	.uleb128 .LEHB158-.LFB6512
	.uleb128 .LEHE158-.LEHB158
	.uleb128 .L1174-.LFB6512
	.uleb128 0
.LLSDACSE6512:
	.section	.text._ZN2v88internal6torque4Type15CommonSupertypeEPKS2_S4_
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque4Type15CommonSupertypeEPKS2_S4_
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6512
	.type	_ZN2v88internal6torque4Type15CommonSupertypeEPKS2_S4_.cold, @function
_ZN2v88internal6torque4Type15CommonSupertypeEPKS2_S4_.cold:
.LFSB6512:
.L1150:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-96(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L1151
	call	_ZdlPv@PLT
.L1151:
	movq	-264(%rbp), %r15
.L1152:
	movq	-128(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L1154
	call	_ZdlPv@PLT
.L1154:
	movq	-192(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L1155
	call	_ZdlPv@PLT
.L1155:
	movq	%r15, %r12
.L1156:
	movq	-224(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L1158
	call	_ZdlPv@PLT
.L1158:
	movq	-256(%rbp), %rdi
	leaq	-240(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1160
	call	_ZdlPv@PLT
.L1160:
	movq	-160(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1161
	call	_ZdlPv@PLT
.L1161:
	movq	%r12, %rdi
.LEHB159:
	call	_Unwind_Resume@PLT
.LEHE159:
	.cfi_endproc
.LFE6512:
	.section	.gcc_except_table._ZN2v88internal6torque4Type15CommonSupertypeEPKS2_S4_
.LLSDAC6512:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6512-.LLSDACSBC6512
.LLSDACSBC6512:
	.uleb128 .LEHB159-.LCOLDB48
	.uleb128 .LEHE159-.LEHB159
	.uleb128 0
	.uleb128 0
.LLSDACSEC6512:
	.section	.text.unlikely._ZN2v88internal6torque4Type15CommonSupertypeEPKS2_S4_
	.section	.text._ZN2v88internal6torque4Type15CommonSupertypeEPKS2_S4_
	.size	_ZN2v88internal6torque4Type15CommonSupertypeEPKS2_S4_, .-_ZN2v88internal6torque4Type15CommonSupertypeEPKS2_S4_
	.section	.text.unlikely._ZN2v88internal6torque4Type15CommonSupertypeEPKS2_S4_
	.size	_ZN2v88internal6torque4Type15CommonSupertypeEPKS2_S4_.cold, .-_ZN2v88internal6torque4Type15CommonSupertypeEPKS2_S4_.cold
.LCOLDE48:
	.section	.text._ZN2v88internal6torque4Type15CommonSupertypeEPKS2_S4_
.LHOTE48:
	.section	.text._ZN2v88internal6torque9UnionType15RecomputeParentEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque9UnionType15RecomputeParentEv
	.type	_ZN2v88internal6torque9UnionType15RecomputeParentEv, @function
_ZN2v88internal6torque9UnionType15RecomputeParentEv:
.LFB6523:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	xorl	%r13d, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	96(%rdi), %r12
	leaq	80(%rdi), %rbx
	cmpq	%rbx, %r12
	je	.L1207
	movq	32(%r12), %r13
.L1210:
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r12
	cmpq	%rax, %rbx
	je	.L1207
.L1209:
	movq	32(%r12), %rsi
	testq	%r13, %r13
	je	.L1211
	movq	%r13, %rdi
	call	_ZN2v88internal6torque4Type15CommonSupertypeEPKS2_S4_
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r12
	cmpq	%rax, %rbx
	jne	.L1209
.L1207:
	popq	%rbx
	popq	%r12
	movq	%r13, 16(%r14)
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1211:
	.cfi_restore_state
	movq	%rsi, %r13
	jmp	.L1210
	.cfi_endproc
.LFE6523:
	.size	_ZN2v88internal6torque9UnionType15RecomputeParentEv, .-_ZN2v88internal6torque9UnionType15RecomputeParentEv
	.section	.text._ZN2v88internal6torque11ReportErrorIJRA44_KcNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA41_S3_EEEvDpOT_,"axG",@progbits,_ZN2v88internal6torque11ReportErrorIJRA44_KcNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA41_S3_EEEvDpOT_,comdat
	.p2align 4
	.weak	_ZN2v88internal6torque11ReportErrorIJRA44_KcNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA41_S3_EEEvDpOT_
	.type	_ZN2v88internal6torque11ReportErrorIJRA44_KcNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA41_S3_EEEvDpOT_, @function
_ZN2v88internal6torque11ReportErrorIJRA44_KcNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA41_S3_EEEvDpOT_:
.LFB7876:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA7876
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %rcx
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-96(%rbp), %r12
	movq	%r12, %rdi
	subq	$80, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
.LEHB160:
	call	_ZN2v88internal6torqueL7MessageIJRA38_KcNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA41_S3_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
.LEHE160:
	movq	%r12, %rdi
.LEHB161:
	call	_ZNK2v88internal6torque14MessageBuilder5ThrowEv@PLT
.LEHE161:
.L1217:
	endbr64
	movq	%rax, %r13
.L1215:
	movq	%r12, %rdi
	call	_ZN2v88internal6torque14MessageBuilderD1Ev
	movq	%r13, %rdi
.LEHB162:
	call	_Unwind_Resume@PLT
.LEHE162:
	.cfi_endproc
.LFE7876:
	.section	.gcc_except_table._ZN2v88internal6torque11ReportErrorIJRA44_KcNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA41_S3_EEEvDpOT_,"aG",@progbits,_ZN2v88internal6torque11ReportErrorIJRA44_KcNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA41_S3_EEEvDpOT_,comdat
.LLSDA7876:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE7876-.LLSDACSB7876
.LLSDACSB7876:
	.uleb128 .LEHB160-.LFB7876
	.uleb128 .LEHE160-.LEHB160
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB161-.LFB7876
	.uleb128 .LEHE161-.LEHB161
	.uleb128 .L1217-.LFB7876
	.uleb128 0
	.uleb128 .LEHB162-.LFB7876
	.uleb128 .LEHE162-.LEHB162
	.uleb128 0
	.uleb128 0
.LLSDACSE7876:
	.section	.text._ZN2v88internal6torque11ReportErrorIJRA44_KcNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA41_S3_EEEvDpOT_,"axG",@progbits,_ZN2v88internal6torque11ReportErrorIJRA44_KcNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA41_S3_EEEvDpOT_,comdat
	.size	_ZN2v88internal6torque11ReportErrorIJRA44_KcNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA41_S3_EEEvDpOT_, .-_ZN2v88internal6torque11ReportErrorIJRA44_KcNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA41_S3_EEEvDpOT_
	.section	.rodata._ZNK2v88internal6torque4Type25GetGeneratedTNodeTypeNameB5cxx11Ev.str1.8,"aMS",@progbits,1
	.align 8
.LC49:
	.string	"Generated TNode type is required for type '"
	.section	.text.unlikely._ZNK2v88internal6torque4Type25GetGeneratedTNodeTypeNameB5cxx11Ev,"ax",@progbits
	.align 2
.LCOLDB50:
	.section	.text._ZNK2v88internal6torque4Type25GetGeneratedTNodeTypeNameB5cxx11Ev,"ax",@progbits
.LHOTB50:
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal6torque4Type25GetGeneratedTNodeTypeNameB5cxx11Ev
	.type	_ZNK2v88internal6torque4Type25GetGeneratedTNodeTypeNameB5cxx11Ev, @function
_ZNK2v88internal6torque4Type25GetGeneratedTNodeTypeNameB5cxx11Ev:
.LFB6516:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6516
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
.LEHB163:
	call	*88(%rax)
.LEHE163:
	cmpq	$0, 8(%r13)
	je	.L1222
	movq	(%r12), %rax
	movq	%r12, %rdi
.LEHB164:
	call	*32(%rax)
	testb	%al, %al
	jne	.L1222
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1233
	addq	$56, %rsp
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1222:
	.cfi_restore_state
	leaq	-80(%rbp), %r14
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZNK2v88internal6torque4Type8ToStringB5cxx11Ev
.LEHE164:
	leaq	.LC42(%rip), %rdx
	movq	%r14, %rsi
	leaq	.LC49(%rip), %rdi
.LEHB165:
	call	_ZN2v88internal6torque11ReportErrorIJRA44_KcNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA41_S3_EEEvDpOT_
.LEHE165:
.L1233:
	call	__stack_chk_fail@PLT
.L1228:
	endbr64
	movq	%rax, %r12
	jmp	.L1225
.L1229:
	endbr64
	movq	%rax, %r12
	jmp	.L1223
	.section	.gcc_except_table._ZNK2v88internal6torque4Type25GetGeneratedTNodeTypeNameB5cxx11Ev,"a",@progbits
.LLSDA6516:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6516-.LLSDACSB6516
.LLSDACSB6516:
	.uleb128 .LEHB163-.LFB6516
	.uleb128 .LEHE163-.LEHB163
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB164-.LFB6516
	.uleb128 .LEHE164-.LEHB164
	.uleb128 .L1228-.LFB6516
	.uleb128 0
	.uleb128 .LEHB165-.LFB6516
	.uleb128 .LEHE165-.LEHB165
	.uleb128 .L1229-.LFB6516
	.uleb128 0
.LLSDACSE6516:
	.section	.text._ZNK2v88internal6torque4Type25GetGeneratedTNodeTypeNameB5cxx11Ev
	.cfi_endproc
	.section	.text.unlikely._ZNK2v88internal6torque4Type25GetGeneratedTNodeTypeNameB5cxx11Ev
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6516
	.type	_ZNK2v88internal6torque4Type25GetGeneratedTNodeTypeNameB5cxx11Ev.cold, @function
_ZNK2v88internal6torque4Type25GetGeneratedTNodeTypeNameB5cxx11Ev.cold:
.LFSB6516:
.L1223:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	movq	-80(%rbp), %rdi
	leaq	-64(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1225
	call	_ZdlPv@PLT
.L1225:
	movq	0(%r13), %rdi
	addq	$16, %r13
	cmpq	%r13, %rdi
	je	.L1226
	call	_ZdlPv@PLT
.L1226:
	movq	%r12, %rdi
.LEHB166:
	call	_Unwind_Resume@PLT
.LEHE166:
	.cfi_endproc
.LFE6516:
	.section	.gcc_except_table._ZNK2v88internal6torque4Type25GetGeneratedTNodeTypeNameB5cxx11Ev
.LLSDAC6516:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6516-.LLSDACSBC6516
.LLSDACSBC6516:
	.uleb128 .LEHB166-.LCOLDB50
	.uleb128 .LEHE166-.LEHB166
	.uleb128 0
	.uleb128 0
.LLSDACSEC6516:
	.section	.text.unlikely._ZNK2v88internal6torque4Type25GetGeneratedTNodeTypeNameB5cxx11Ev
	.section	.text._ZNK2v88internal6torque4Type25GetGeneratedTNodeTypeNameB5cxx11Ev
	.size	_ZNK2v88internal6torque4Type25GetGeneratedTNodeTypeNameB5cxx11Ev, .-_ZNK2v88internal6torque4Type25GetGeneratedTNodeTypeNameB5cxx11Ev
	.section	.text.unlikely._ZNK2v88internal6torque4Type25GetGeneratedTNodeTypeNameB5cxx11Ev
	.size	_ZNK2v88internal6torque4Type25GetGeneratedTNodeTypeNameB5cxx11Ev.cold, .-_ZNK2v88internal6torque4Type25GetGeneratedTNodeTypeNameB5cxx11Ev.cold
.LCOLDE50:
	.section	.text._ZNK2v88internal6torque4Type25GetGeneratedTNodeTypeNameB5cxx11Ev
.LHOTE50:
	.section	.text.unlikely._ZNK2v88internal6torque4Type25GetGeneratedTNodeTypeNameB5cxx11Ev.constprop.1,"ax",@progbits
	.align 2
.LCOLDB51:
	.section	.text._ZNK2v88internal6torque4Type25GetGeneratedTNodeTypeNameB5cxx11Ev.constprop.1,"ax",@progbits
.LHOTB51:
	.align 2
	.p2align 4
	.type	_ZNK2v88internal6torque4Type25GetGeneratedTNodeTypeNameB5cxx11Ev.constprop.1, @function
_ZNK2v88internal6torque4Type25GetGeneratedTNodeTypeNameB5cxx11Ev.constprop.1:
.LFB13128:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA13128
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	leaq	16(%rdi), %rbx
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%rbx, (%rdi)
	movq	184(%rsi), %rsi
	movq	192(%r13), %rdx
	addq	%rsi, %rdx
.LEHB167:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag.constprop.0
.LEHE167:
	cmpq	$0, 8(%r12)
	je	.L1245
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1246
	addq	$48, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1245:
	.cfi_restore_state
	leaq	-80(%rbp), %r14
	movq	%r13, %rsi
	movq	%r14, %rdi
.LEHB168:
	call	_ZNK2v88internal6torque4Type8ToStringB5cxx11Ev
.LEHE168:
	leaq	.LC42(%rip), %rdx
	movq	%r14, %rsi
	leaq	.LC49(%rip), %rdi
.LEHB169:
	call	_ZN2v88internal6torque11ReportErrorIJRA44_KcNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA41_S3_EEEvDpOT_
.LEHE169:
.L1246:
	call	__stack_chk_fail@PLT
.L1242:
	endbr64
	movq	%rax, %r13
	jmp	.L1236
.L1241:
	endbr64
	movq	%rax, %r13
	jmp	.L1238
	.section	.gcc_except_table._ZNK2v88internal6torque4Type25GetGeneratedTNodeTypeNameB5cxx11Ev.constprop.1,"a",@progbits
.LLSDA13128:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE13128-.LLSDACSB13128
.LLSDACSB13128:
	.uleb128 .LEHB167-.LFB13128
	.uleb128 .LEHE167-.LEHB167
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB168-.LFB13128
	.uleb128 .LEHE168-.LEHB168
	.uleb128 .L1241-.LFB13128
	.uleb128 0
	.uleb128 .LEHB169-.LFB13128
	.uleb128 .LEHE169-.LEHB169
	.uleb128 .L1242-.LFB13128
	.uleb128 0
.LLSDACSE13128:
	.section	.text._ZNK2v88internal6torque4Type25GetGeneratedTNodeTypeNameB5cxx11Ev.constprop.1
	.cfi_endproc
	.section	.text.unlikely._ZNK2v88internal6torque4Type25GetGeneratedTNodeTypeNameB5cxx11Ev.constprop.1
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC13128
	.type	_ZNK2v88internal6torque4Type25GetGeneratedTNodeTypeNameB5cxx11Ev.constprop.1.cold, @function
_ZNK2v88internal6torque4Type25GetGeneratedTNodeTypeNameB5cxx11Ev.constprop.1.cold:
.LFSB13128:
.L1236:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	movq	-80(%rbp), %rdi
	leaq	-64(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1238
	call	_ZdlPv@PLT
.L1238:
	movq	(%r12), %rdi
	cmpq	%rbx, %rdi
	je	.L1239
	call	_ZdlPv@PLT
.L1239:
	movq	%r13, %rdi
.LEHB170:
	call	_Unwind_Resume@PLT
.LEHE170:
	.cfi_endproc
.LFE13128:
	.section	.gcc_except_table._ZNK2v88internal6torque4Type25GetGeneratedTNodeTypeNameB5cxx11Ev.constprop.1
.LLSDAC13128:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC13128-.LLSDACSBC13128
.LLSDACSBC13128:
	.uleb128 .LEHB170-.LCOLDB51
	.uleb128 .LEHE170-.LEHB170
	.uleb128 0
	.uleb128 0
.LLSDACSEC13128:
	.section	.text.unlikely._ZNK2v88internal6torque4Type25GetGeneratedTNodeTypeNameB5cxx11Ev.constprop.1
	.section	.text._ZNK2v88internal6torque4Type25GetGeneratedTNodeTypeNameB5cxx11Ev.constprop.1
	.size	_ZNK2v88internal6torque4Type25GetGeneratedTNodeTypeNameB5cxx11Ev.constprop.1, .-_ZNK2v88internal6torque4Type25GetGeneratedTNodeTypeNameB5cxx11Ev.constprop.1
	.section	.text.unlikely._ZNK2v88internal6torque4Type25GetGeneratedTNodeTypeNameB5cxx11Ev.constprop.1
	.size	_ZNK2v88internal6torque4Type25GetGeneratedTNodeTypeNameB5cxx11Ev.constprop.1.cold, .-_ZNK2v88internal6torque4Type25GetGeneratedTNodeTypeNameB5cxx11Ev.constprop.1.cold
.LCOLDE51:
	.section	.text._ZNK2v88internal6torque4Type25GetGeneratedTNodeTypeNameB5cxx11Ev.constprop.1
.LHOTE51:
	.section	.text.unlikely._ZNK2v88internal6torque9ClassType24GetGeneratedTypeNameImplB5cxx11Ev,"ax",@progbits
	.align 2
.LCOLDB52:
	.section	.text._ZNK2v88internal6torque9ClassType24GetGeneratedTypeNameImplB5cxx11Ev,"ax",@progbits
.LHOTB52:
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal6torque9ClassType24GetGeneratedTypeNameImplB5cxx11Ev
	.type	_ZNK2v88internal6torque9ClassType24GetGeneratedTypeNameImplB5cxx11Ev, @function
_ZNK2v88internal6torque9ClassType24GetGeneratedTypeNameImplB5cxx11Ev:
.LFB6590:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6590
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-96(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$80, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
.LEHB171:
	call	_ZNK2v88internal6torque4Type25GetGeneratedTNodeTypeNameB5cxx11Ev.constprop.1
.LEHE171:
	movl	$16, %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r13, %rdi
	leaq	.LC3(%rip), %rcx
.LEHB172:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.LEHE172:
	leaq	-48(%rbp), %r13
	leaq	16(%rax), %rdx
	movq	%r13, -64(%rbp)
	movq	(%rax), %rcx
	cmpq	%rdx, %rcx
	je	.L1273
	movq	%rcx, -64(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -48(%rbp)
.L1254:
	movq	8(%rax), %rcx
	movb	$0, 16(%rax)
	movq	%rcx, -56(%rbp)
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movabsq	$4611686018427387903, %rax
	cmpq	%rax, -56(%rbp)
	je	.L1274
	leaq	-64(%rbp), %rdi
	movl	$1, %edx
	leaq	.LC4(%rip), %rsi
.LEHB173:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	leaq	16(%r12), %rdx
	movq	%rdx, (%r12)
	movq	(%rax), %rcx
	leaq	16(%rax), %rdx
	cmpq	%rdx, %rcx
	je	.L1275
	movq	%rcx, (%r12)
	movq	16(%rax), %rcx
	movq	%rcx, 16(%r12)
.L1257:
	movq	8(%rax), %rcx
	movq	%rdx, (%rax)
	movq	-64(%rbp), %rdi
	movq	$0, 8(%rax)
	movq	%rcx, 8(%r12)
	movb	$0, 16(%rax)
	cmpq	%r13, %rdi
	je	.L1258
	call	_ZdlPv@PLT
.L1258:
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1247
	call	_ZdlPv@PLT
.L1247:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1276
	addq	$80, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1273:
	.cfi_restore_state
	movdqu	16(%rax), %xmm0
	movaps	%xmm0, -48(%rbp)
	jmp	.L1254
	.p2align 4,,10
	.p2align 3
.L1275:
	movdqu	16(%rax), %xmm1
	movups	%xmm1, 16(%r12)
	jmp	.L1257
.L1276:
	call	__stack_chk_fail@PLT
.L1274:
	leaq	.LC2(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.LEHE173:
.L1265:
	endbr64
	movq	%rax, %r12
	jmp	.L1260
.L1266:
	endbr64
	movq	%rax, %r12
	jmp	.L1271
.L1264:
	endbr64
	movq	%rax, %r12
	jmp	.L1262
	.section	.gcc_except_table._ZNK2v88internal6torque9ClassType24GetGeneratedTypeNameImplB5cxx11Ev,"a",@progbits
.LLSDA6590:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6590-.LLSDACSB6590
.LLSDACSB6590:
	.uleb128 .LEHB171-.LFB6590
	.uleb128 .LEHE171-.LEHB171
	.uleb128 .L1264-.LFB6590
	.uleb128 0
	.uleb128 .LEHB172-.LFB6590
	.uleb128 .LEHE172-.LEHB172
	.uleb128 .L1266-.LFB6590
	.uleb128 0
	.uleb128 .LEHB173-.LFB6590
	.uleb128 .LEHE173-.LEHB173
	.uleb128 .L1265-.LFB6590
	.uleb128 0
.LLSDACSE6590:
	.section	.text._ZNK2v88internal6torque9ClassType24GetGeneratedTypeNameImplB5cxx11Ev
	.cfi_endproc
	.section	.text.unlikely._ZNK2v88internal6torque9ClassType24GetGeneratedTypeNameImplB5cxx11Ev
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6590
	.type	_ZNK2v88internal6torque9ClassType24GetGeneratedTypeNameImplB5cxx11Ev.cold, @function
_ZNK2v88internal6torque9ClassType24GetGeneratedTypeNameImplB5cxx11Ev.cold:
.LFSB6590:
.L1260:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movq	-64(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L1271
	call	_ZdlPv@PLT
.L1271:
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1262
	call	_ZdlPv@PLT
.L1262:
	movq	%r12, %rdi
.LEHB174:
	call	_Unwind_Resume@PLT
.LEHE174:
	.cfi_endproc
.LFE6590:
	.section	.gcc_except_table._ZNK2v88internal6torque9ClassType24GetGeneratedTypeNameImplB5cxx11Ev
.LLSDAC6590:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6590-.LLSDACSBC6590
.LLSDACSBC6590:
	.uleb128 .LEHB174-.LCOLDB52
	.uleb128 .LEHE174-.LEHB174
	.uleb128 0
	.uleb128 0
.LLSDACSEC6590:
	.section	.text.unlikely._ZNK2v88internal6torque9ClassType24GetGeneratedTypeNameImplB5cxx11Ev
	.section	.text._ZNK2v88internal6torque9ClassType24GetGeneratedTypeNameImplB5cxx11Ev
	.size	_ZNK2v88internal6torque9ClassType24GetGeneratedTypeNameImplB5cxx11Ev, .-_ZNK2v88internal6torque9ClassType24GetGeneratedTypeNameImplB5cxx11Ev
	.section	.text.unlikely._ZNK2v88internal6torque9ClassType24GetGeneratedTypeNameImplB5cxx11Ev
	.size	_ZNK2v88internal6torque9ClassType24GetGeneratedTypeNameImplB5cxx11Ev.cold, .-_ZNK2v88internal6torque9ClassType24GetGeneratedTypeNameImplB5cxx11Ev.cold
.LCOLDE52:
	.section	.text._ZNK2v88internal6torque9ClassType24GetGeneratedTypeNameImplB5cxx11Ev
.LHOTE52:
	.section	.text._ZNK2v88internal6torque18BuiltinPointerType29GetGeneratedTNodeTypeNameImplB5cxx11Ev,"axG",@progbits,_ZNK2v88internal6torque18BuiltinPointerType29GetGeneratedTNodeTypeNameImplB5cxx11Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal6torque18BuiltinPointerType29GetGeneratedTNodeTypeNameImplB5cxx11Ev
	.type	_ZNK2v88internal6torque18BuiltinPointerType29GetGeneratedTNodeTypeNameImplB5cxx11Ev, @function
_ZNK2v88internal6torque18BuiltinPointerType29GetGeneratedTNodeTypeNameImplB5cxx11Ev:
.LFB5634:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA5634
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$56, %rsp
	movq	16(%rsi), %r13
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	0(%r13), %rax
	movq	%r13, %rsi
.LEHB175:
	call	*88(%rax)
.LEHE175:
	cmpq	$0, 8(%r12)
	je	.L1280
	movq	0(%r13), %rax
	movq	%r13, %rdi
.LEHB176:
	call	*32(%rax)
	testb	%al, %al
	jne	.L1280
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1292
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1280:
	.cfi_restore_state
	leaq	-80(%rbp), %r14
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZNK2v88internal6torque4Type8ToStringB5cxx11Ev
.LEHE176:
	leaq	.LC42(%rip), %rdx
	movq	%r14, %rsi
	leaq	.LC49(%rip), %rdi
.LEHB177:
	call	_ZN2v88internal6torque11ReportErrorIJRA44_KcNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA41_S3_EEEvDpOT_
.LEHE177:
.L1292:
	call	__stack_chk_fail@PLT
.L1287:
	endbr64
	movq	%rax, %r13
	jmp	.L1284
.L1288:
	endbr64
	movq	%rax, %r13
.L1282:
	movq	-80(%rbp), %rdi
	leaq	-64(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1284
	call	_ZdlPv@PLT
.L1284:
	movq	(%r12), %rdi
	addq	$16, %r12
	cmpq	%r12, %rdi
	je	.L1285
	call	_ZdlPv@PLT
.L1285:
	movq	%r13, %rdi
.LEHB178:
	call	_Unwind_Resume@PLT
.LEHE178:
	.cfi_endproc
.LFE5634:
	.section	.gcc_except_table._ZNK2v88internal6torque18BuiltinPointerType29GetGeneratedTNodeTypeNameImplB5cxx11Ev,"aG",@progbits,_ZNK2v88internal6torque18BuiltinPointerType29GetGeneratedTNodeTypeNameImplB5cxx11Ev,comdat
.LLSDA5634:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE5634-.LLSDACSB5634
.LLSDACSB5634:
	.uleb128 .LEHB175-.LFB5634
	.uleb128 .LEHE175-.LEHB175
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB176-.LFB5634
	.uleb128 .LEHE176-.LEHB176
	.uleb128 .L1287-.LFB5634
	.uleb128 0
	.uleb128 .LEHB177-.LFB5634
	.uleb128 .LEHE177-.LEHB177
	.uleb128 .L1288-.LFB5634
	.uleb128 0
	.uleb128 .LEHB178-.LFB5634
	.uleb128 .LEHE178-.LEHB178
	.uleb128 0
	.uleb128 0
.LLSDACSE5634:
	.section	.text._ZNK2v88internal6torque18BuiltinPointerType29GetGeneratedTNodeTypeNameImplB5cxx11Ev,"axG",@progbits,_ZNK2v88internal6torque18BuiltinPointerType29GetGeneratedTNodeTypeNameImplB5cxx11Ev,comdat
	.size	_ZNK2v88internal6torque18BuiltinPointerType29GetGeneratedTNodeTypeNameImplB5cxx11Ev, .-_ZNK2v88internal6torque18BuiltinPointerType29GetGeneratedTNodeTypeNameImplB5cxx11Ev
	.section	.text._ZN2v88internal6torque23PrintCommaSeparatedListISt6vectorIPKNS1_4TypeESaIS6_EELi0EEEvRSoRKT_,"axG",@progbits,_ZN2v88internal6torque23PrintCommaSeparatedListISt6vectorIPKNS1_4TypeESaIS6_EELi0EEEvRSoRKT_,comdat
	.p2align 4
	.weak	_ZN2v88internal6torque23PrintCommaSeparatedListISt6vectorIPKNS1_4TypeESaIS6_EELi0EEEvRSoRKT_
	.type	_ZN2v88internal6torque23PrintCommaSeparatedListISt6vectorIPKNS1_4TypeESaIS6_EELi0EEEvRSoRKT_, @function
_ZN2v88internal6torque23PrintCommaSeparatedListISt6vectorIPKNS1_4TypeESaIS6_EELi0EEEvRSoRKT_:
.LFB7878:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA7878
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rsi), %rbx
	movq	8(%rsi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	%r13, %rbx
	je	.L1293
	movq	%rdi, %r12
	leaq	-96(%rbp), %r15
	leaq	-80(%rbp), %r14
.L1295:
	movq	(%rbx), %rsi
	movq	%r15, %rdi
.LEHB179:
	call	_ZNK2v88internal6torque4Type8ToStringB5cxx11Ev
.LEHE179:
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r12, %rdi
.LEHB180:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE180:
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L1296
	call	_ZdlPv@PLT
	addq	$8, %rbx
	cmpq	%rbx, %r13
	je	.L1293
.L1297:
	movl	$2, %edx
	leaq	.LC12(%rip), %rsi
	movq	%r12, %rdi
.LEHB181:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L1295
	.p2align 4,,10
	.p2align 3
.L1296:
	addq	$8, %rbx
	cmpq	%rbx, %r13
	jne	.L1297
.L1293:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1303
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1303:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
.L1301:
	endbr64
	movq	%rax, %r12
.L1298:
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L1299
	call	_ZdlPv@PLT
.L1299:
	movq	%r12, %rdi
	call	_Unwind_Resume@PLT
.LEHE181:
	.cfi_endproc
.LFE7878:
	.section	.gcc_except_table._ZN2v88internal6torque23PrintCommaSeparatedListISt6vectorIPKNS1_4TypeESaIS6_EELi0EEEvRSoRKT_,"aG",@progbits,_ZN2v88internal6torque23PrintCommaSeparatedListISt6vectorIPKNS1_4TypeESaIS6_EELi0EEEvRSoRKT_,comdat
.LLSDA7878:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE7878-.LLSDACSB7878
.LLSDACSB7878:
	.uleb128 .LEHB179-.LFB7878
	.uleb128 .LEHE179-.LEHB179
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB180-.LFB7878
	.uleb128 .LEHE180-.LEHB180
	.uleb128 .L1301-.LFB7878
	.uleb128 0
	.uleb128 .LEHB181-.LFB7878
	.uleb128 .LEHE181-.LEHB181
	.uleb128 0
	.uleb128 0
.LLSDACSE7878:
	.section	.text._ZN2v88internal6torque23PrintCommaSeparatedListISt6vectorIPKNS1_4TypeESaIS6_EELi0EEEvRSoRKT_,"axG",@progbits,_ZN2v88internal6torque23PrintCommaSeparatedListISt6vectorIPKNS1_4TypeESaIS6_EELi0EEEvRSoRKT_,comdat
	.size	_ZN2v88internal6torque23PrintCommaSeparatedListISt6vectorIPKNS1_4TypeESaIS6_EELi0EEEvRSoRKT_, .-_ZN2v88internal6torque23PrintCommaSeparatedListISt6vectorIPKNS1_4TypeESaIS6_EELi0EEEvRSoRKT_
	.section	.rodata._ZN2v88internal6torque14PrintSignatureERSoRKNS1_9SignatureEb.str1.1,"aMS",@progbits,1
.LC53:
	.string	"implicit "
.LC54:
	.string	")("
.LC55:
	.string	" labels "
	.section	.text.unlikely._ZN2v88internal6torque14PrintSignatureERSoRKNS1_9SignatureEb,"ax",@progbits
.LCOLDB56:
	.section	.text._ZN2v88internal6torque14PrintSignatureERSoRKNS1_9SignatureEb,"ax",@progbits
.LHOTB56:
	.p2align 4
	.globl	_ZN2v88internal6torque14PrintSignatureERSoRKNS1_9SignatureEb
	.type	_ZN2v88internal6torque14PrintSignatureERSoRKNS1_9SignatureEb, @function
_ZN2v88internal6torque14PrintSignatureERSoRKNS1_9SignatureEb:
.LFB6614:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6614
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	movl	$1, %edx
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	leaq	.LC26(%rip), %rsi
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
.LEHB182:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	72(%rbx), %rax
	cmpq	%rax, 64(%rbx)
	je	.L1346
	leaq	-80(%rbp), %rax
	xorl	%r12d, %r12d
	leaq	-96(%rbp), %r15
	movq	%rax, -104(%rbp)
	.p2align 4,,10
	.p2align 3
.L1312:
	testq	%r12, %r12
	jne	.L1306
	cmpq	$0, 96(%rbx)
	jne	.L1347
.L1308:
	leaq	0(,%r12,8), %rcx
	testb	%r14b, %r14b
	je	.L1309
	movq	8(%rbx), %rax
	movq	(%rbx), %rdx
	cmpq	%rax, %rdx
	je	.L1309
	subq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%r12, %rax
	ja	.L1348
.L1309:
	movq	64(%rbx), %rax
	movq	%r15, %rdi
	movq	(%rax,%rcx), %rsi
	call	_ZNK2v88internal6torque4Type8ToStringB5cxx11Ev
.LEHE182:
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r13, %rdi
.LEHB183:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE183:
	movq	-96(%rbp), %rdi
	cmpq	-104(%rbp), %rdi
	je	.L1310
	call	_ZdlPv@PLT
	movq	72(%rbx), %rax
	subq	64(%rbx), %rax
	addq	$1, %r12
	sarq	$3, %rax
	cmpq	%rax, %r12
	jb	.L1312
.L1305:
	cmpb	$0, 88(%rbx)
	je	.L1315
	movq	8(%rbx), %rax
	cmpq	%rax, (%rbx)
	je	.L1316
	movl	$2, %edx
	leaq	.LC12(%rip), %rsi
	movq	%r13, %rdi
.LEHB184:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L1316:
	movl	$3, %edx
	leaq	.LC15(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L1315:
	movl	$1, %edx
	leaq	.LC10(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$2, %edx
	leaq	.LC17(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	104(%rbx), %rsi
	movq	%r15, %rdi
	call	_ZNK2v88internal6torque4Type8ToStringB5cxx11Ev
.LEHE184:
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r13, %rdi
.LEHB185:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE185:
	movq	-96(%rbp), %rdi
	cmpq	-104(%rbp), %rdi
	je	.L1317
	call	_ZdlPv@PLT
.L1317:
	movq	112(%rbx), %rax
	cmpq	%rax, 120(%rbx)
	je	.L1304
	movl	$8, %edx
	leaq	.LC55(%rip), %rsi
	movq	%r13, %rdi
	xorl	%r15d, %r15d
.LEHB186:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	.LC26(%rip), %r14
	movq	112(%rbx), %rax
	cmpq	120(%rbx), %rax
	je	.L1304
	.p2align 4,,10
	.p2align 3
.L1321:
	movq	%r15, %r12
	movq	%r13, %rdi
	salq	$5, %r12
	movq	(%rax,%r12), %rax
	movq	40(%rax), %rdx
	movq	32(%rax), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	112(%rbx), %rax
	movq	16(%rax,%r12), %rcx
	cmpq	%rcx, 8(%rax,%r12)
	je	.L1324
	movl	$1, %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	112(%rbx), %rsi
	movq	%r13, %rdi
	addq	%r12, %rsi
	addq	$8, %rsi
	call	_ZN2v88internal6torque23PrintCommaSeparatedListISt6vectorIPKNS1_4TypeESaIS6_EELi0EEEvRSoRKT_
	movl	$1, %edx
	leaq	.LC10(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	112(%rbx), %rax
.L1324:
	movq	120(%rbx), %rdx
	addq	$1, %r15
	subq	%rax, %rdx
	sarq	$5, %rdx
	cmpq	%rdx, %r15
	jnb	.L1304
	testq	%r15, %r15
	je	.L1321
	movl	$2, %edx
	leaq	.LC12(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	112(%rbx), %rax
	jmp	.L1321
	.p2align 4,,10
	.p2align 3
.L1306:
	movq	96(%rbx), %rax
	testq	%rax, %rax
	je	.L1328
	cmpq	%rax, %r12
	jne	.L1328
	movl	$2, %edx
	leaq	.LC54(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L1308
	.p2align 4,,10
	.p2align 3
.L1310:
	movq	72(%rbx), %rax
	subq	64(%rbx), %rax
	addq	$1, %r12
	sarq	$3, %rax
	cmpq	%r12, %rax
	ja	.L1312
	jmp	.L1305
	.p2align 4,,10
	.p2align 3
.L1328:
	movl	$2, %edx
	leaq	.LC12(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L1308
	.p2align 4,,10
	.p2align 3
.L1347:
	movl	$9, %edx
	leaq	.LC53(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L1308
	.p2align 4,,10
	.p2align 3
.L1304:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1349
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1348:
	.cfi_restore_state
	movq	(%rdx,%rcx), %rax
	movq	%r13, %rdi
	movq	%rcx, -112(%rbp)
	movq	40(%rax), %rdx
	movq	32(%rax), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$2, %edx
	leaq	.LC17(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE186:
	movq	-112(%rbp), %rcx
	jmp	.L1309
	.p2align 4,,10
	.p2align 3
.L1346:
	leaq	-80(%rbp), %rax
	leaq	-96(%rbp), %r15
	movq	%rax, -104(%rbp)
	jmp	.L1305
.L1349:
	call	__stack_chk_fail@PLT
.L1331:
	endbr64
	movq	%rax, %r12
	jmp	.L1313
.L1332:
	endbr64
	movq	%rax, %r12
	jmp	.L1319
	.section	.gcc_except_table._ZN2v88internal6torque14PrintSignatureERSoRKNS1_9SignatureEb,"a",@progbits
.LLSDA6614:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6614-.LLSDACSB6614
.LLSDACSB6614:
	.uleb128 .LEHB182-.LFB6614
	.uleb128 .LEHE182-.LEHB182
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB183-.LFB6614
	.uleb128 .LEHE183-.LEHB183
	.uleb128 .L1331-.LFB6614
	.uleb128 0
	.uleb128 .LEHB184-.LFB6614
	.uleb128 .LEHE184-.LEHB184
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB185-.LFB6614
	.uleb128 .LEHE185-.LEHB185
	.uleb128 .L1332-.LFB6614
	.uleb128 0
	.uleb128 .LEHB186-.LFB6614
	.uleb128 .LEHE186-.LEHB186
	.uleb128 0
	.uleb128 0
.LLSDACSE6614:
	.section	.text._ZN2v88internal6torque14PrintSignatureERSoRKNS1_9SignatureEb
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque14PrintSignatureERSoRKNS1_9SignatureEb
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6614
	.type	_ZN2v88internal6torque14PrintSignatureERSoRKNS1_9SignatureEb.cold, @function
_ZN2v88internal6torque14PrintSignatureERSoRKNS1_9SignatureEb.cold:
.LFSB6614:
.L1313:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-96(%rbp), %rdi
	cmpq	-104(%rbp), %rdi
	je	.L1314
	call	_ZdlPv@PLT
.L1314:
	movq	%r12, %rdi
.LEHB187:
	call	_Unwind_Resume@PLT
.L1319:
	movq	-96(%rbp), %rdi
	cmpq	-104(%rbp), %rdi
	je	.L1320
	call	_ZdlPv@PLT
.L1320:
	movq	%r12, %rdi
	call	_Unwind_Resume@PLT
.LEHE187:
	.cfi_endproc
.LFE6614:
	.section	.gcc_except_table._ZN2v88internal6torque14PrintSignatureERSoRKNS1_9SignatureEb
.LLSDAC6614:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6614-.LLSDACSBC6614
.LLSDACSBC6614:
	.uleb128 .LEHB187-.LCOLDB56
	.uleb128 .LEHE187-.LEHB187
	.uleb128 0
	.uleb128 0
.LLSDACSEC6614:
	.section	.text.unlikely._ZN2v88internal6torque14PrintSignatureERSoRKNS1_9SignatureEb
	.section	.text._ZN2v88internal6torque14PrintSignatureERSoRKNS1_9SignatureEb
	.size	_ZN2v88internal6torque14PrintSignatureERSoRKNS1_9SignatureEb, .-_ZN2v88internal6torque14PrintSignatureERSoRKNS1_9SignatureEb
	.section	.text.unlikely._ZN2v88internal6torque14PrintSignatureERSoRKNS1_9SignatureEb
	.size	_ZN2v88internal6torque14PrintSignatureERSoRKNS1_9SignatureEb.cold, .-_ZN2v88internal6torque14PrintSignatureERSoRKNS1_9SignatureEb.cold
.LCOLDE56:
	.section	.text._ZN2v88internal6torque14PrintSignatureERSoRKNS1_9SignatureEb
.LHOTE56:
	.section	.text._ZN2v88internal6torquelsERSoRKNS1_9SignatureE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal6torquelsERSoRKNS1_9SignatureE
	.type	_ZN2v88internal6torquelsERSoRKNS1_9SignatureE, @function
_ZN2v88internal6torquelsERSoRKNS1_9SignatureE:
.LFB6617:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN2v88internal6torque14PrintSignatureERSoRKNS1_9SignatureEb
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE6617:
	.size	_ZN2v88internal6torquelsERSoRKNS1_9SignatureE, .-_ZN2v88internal6torquelsERSoRKNS1_9SignatureE
	.section	.rodata._ZNK2v88internal6torque18BuiltinPointerType16ToExplicitStringB5cxx11Ev.str1.1,"aMS",@progbits,1
.LC57:
	.string	"builtin ("
.LC58:
	.string	") => "
	.section	.text.unlikely._ZNK2v88internal6torque18BuiltinPointerType16ToExplicitStringB5cxx11Ev,"ax",@progbits
	.align 2
.LCOLDB59:
	.section	.text._ZNK2v88internal6torque18BuiltinPointerType16ToExplicitStringB5cxx11Ev,"ax",@progbits
.LHOTB59:
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal6torque18BuiltinPointerType16ToExplicitStringB5cxx11Ev
	.type	_ZNK2v88internal6torque18BuiltinPointerType16ToExplicitStringB5cxx11Ev, @function
_ZNK2v88internal6torque18BuiltinPointerType16ToExplicitStringB5cxx11Ev:
.LFB6518:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6518
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-320(%rbp), %r14
	leaq	-448(%rbp), %r15
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r14, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$472, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r15, -496(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	movq	%rax, -320(%rbp)
	xorl	%eax, %eax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movw	%ax, -96(%rbp)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	$0, -104(%rbp)
	movq	%rax, -448(%rbp)
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	movq	-448(%rbp), %rax
	movq	$0, -440(%rbp)
	movq	-24(%rax), %rdi
	addq	%r15, %rdi
.LEHB188:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE188:
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	leaq	-432(%rbp), %r13
	xorl	%esi, %esi
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rdx, -432(%rbp,%rax)
	movq	-432(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r13, %rdi
.LEHB189:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE189:
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	leaq	-368(%rbp), %r15
	movq	.LC22(%rip), %xmm0
	movq	%r15, %rdi
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movhps	.LC23(%rip), %xmm0
	movq	%rcx, -448(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movaps	%xmm0, -432(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rax, -320(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r14, %rdi
	movl	$24, -360(%rbp)
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movq	%rax, -488(%rbp)
	movq	%rax, -352(%rbp)
	leaq	-424(%rbp), %rax
	movq	%rax, %rsi
	movb	$0, -336(%rbp)
	movq	$0, -344(%rbp)
	movq	%rax, -504(%rbp)
.LEHB190:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE190:
	movl	$9, %edx
	leaq	.LC57(%rip), %rsi
	movq	%r13, %rdi
.LEHB191:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	72(%rbx), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal6torque23PrintCommaSeparatedListISt6vectorIPKNS1_4TypeESaIS6_EELi0EEEvRSoRKT_
	movl	$5, %edx
	leaq	.LC58(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	96(%rbx), %rsi
	leaq	-480(%rbp), %rdi
	call	_ZNK2v88internal6torque4Type8ToStringB5cxx11Ev
.LEHE191:
	movq	-472(%rbp), %rdx
	movq	-480(%rbp), %rsi
	movq	%r13, %rdi
.LEHB192:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE192:
	movq	-480(%rbp), %rdi
	leaq	-464(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1357
	call	_ZdlPv@PLT
.L1357:
	movq	-384(%rbp), %rax
	leaq	16(%r12), %rbx
	movq	$0, 8(%r12)
	movq	%rbx, (%r12)
	movb	$0, 16(%r12)
	testq	%rax, %rax
	je	.L1380
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	ja	.L1381
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
.LEHB193:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L1364:
	movq	.LC22(%rip), %xmm0
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-352(%rbp), %rdi
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movhps	.LC24(%rip), %xmm0
	movq	%rax, -320(%rbp)
	movaps	%xmm0, -432(%rbp)
	cmpq	-488(%rbp), %rdi
	je	.L1366
	call	_ZdlPv@PLT
.L1366:
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%r15, %rdi
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	movq	%r14, %rdi
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rbx, -448(%rbp,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rdx, -432(%rbp,%rax)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rbx, -448(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	movq	$0, -440(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1382
	addq	$472, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1381:
	.cfi_restore_state
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L1364
	.p2align 4,,10
	.p2align 3
.L1380:
	leaq	-352(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
.LEHE193:
	jmp	.L1364
.L1382:
	call	__stack_chk_fail@PLT
.L1374:
	endbr64
	movq	%rax, %r12
	jmp	.L1360
.L1370:
	endbr64
	movq	%rax, %r12
	jmp	.L1362
.L1372:
	endbr64
	movq	%rax, %rbx
	jmp	.L1356
.L1373:
	endbr64
	movq	%rax, %rbx
	jmp	.L1354
.L1371:
	endbr64
	movq	%rax, %rbx
	jmp	.L1355
.L1375:
	endbr64
	movq	%rax, %r13
	jmp	.L1367
	.section	.gcc_except_table._ZNK2v88internal6torque18BuiltinPointerType16ToExplicitStringB5cxx11Ev,"a",@progbits
.LLSDA6518:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6518-.LLSDACSB6518
.LLSDACSB6518:
	.uleb128 .LEHB188-.LFB6518
	.uleb128 .LEHE188-.LEHB188
	.uleb128 .L1371-.LFB6518
	.uleb128 0
	.uleb128 .LEHB189-.LFB6518
	.uleb128 .LEHE189-.LEHB189
	.uleb128 .L1373-.LFB6518
	.uleb128 0
	.uleb128 .LEHB190-.LFB6518
	.uleb128 .LEHE190-.LEHB190
	.uleb128 .L1372-.LFB6518
	.uleb128 0
	.uleb128 .LEHB191-.LFB6518
	.uleb128 .LEHE191-.LEHB191
	.uleb128 .L1370-.LFB6518
	.uleb128 0
	.uleb128 .LEHB192-.LFB6518
	.uleb128 .LEHE192-.LEHB192
	.uleb128 .L1374-.LFB6518
	.uleb128 0
	.uleb128 .LEHB193-.LFB6518
	.uleb128 .LEHE193-.LEHB193
	.uleb128 .L1375-.LFB6518
	.uleb128 0
.LLSDACSE6518:
	.section	.text._ZNK2v88internal6torque18BuiltinPointerType16ToExplicitStringB5cxx11Ev
	.cfi_endproc
	.section	.text.unlikely._ZNK2v88internal6torque18BuiltinPointerType16ToExplicitStringB5cxx11Ev
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6518
	.type	_ZNK2v88internal6torque18BuiltinPointerType16ToExplicitStringB5cxx11Ev.cold, @function
_ZNK2v88internal6torque18BuiltinPointerType16ToExplicitStringB5cxx11Ev.cold:
.LFSB6518:
.L1360:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-480(%rbp), %rdi
	leaq	-464(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1362
	call	_ZdlPv@PLT
.L1362:
	movq	-496(%rbp), %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r12, %rdi
.LEHB194:
	call	_Unwind_Resume@PLT
.L1356:
	movq	-504(%rbp), %rdi
	call	_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED1Ev
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -432(%rbp,%rax)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	movq	$0, -440(%rbp)
.L1355:
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%r14, %rdi
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	%rbx, %rdi
	call	_Unwind_Resume@PLT
.LEHE194:
.L1354:
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	movq	$0, -440(%rbp)
	jmp	.L1355
.L1367:
	movq	(%r12), %rdi
	cmpq	%rdi, %rbx
	je	.L1368
	call	_ZdlPv@PLT
.L1368:
	movq	%r13, %r12
	jmp	.L1362
	.cfi_endproc
.LFE6518:
	.section	.gcc_except_table._ZNK2v88internal6torque18BuiltinPointerType16ToExplicitStringB5cxx11Ev
.LLSDAC6518:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6518-.LLSDACSBC6518
.LLSDACSBC6518:
	.uleb128 .LEHB194-.LCOLDB59
	.uleb128 .LEHE194-.LEHB194
	.uleb128 0
	.uleb128 0
.LLSDACSEC6518:
	.section	.text.unlikely._ZNK2v88internal6torque18BuiltinPointerType16ToExplicitStringB5cxx11Ev
	.section	.text._ZNK2v88internal6torque18BuiltinPointerType16ToExplicitStringB5cxx11Ev
	.size	_ZNK2v88internal6torque18BuiltinPointerType16ToExplicitStringB5cxx11Ev, .-_ZNK2v88internal6torque18BuiltinPointerType16ToExplicitStringB5cxx11Ev
	.section	.text.unlikely._ZNK2v88internal6torque18BuiltinPointerType16ToExplicitStringB5cxx11Ev
	.size	_ZNK2v88internal6torque18BuiltinPointerType16ToExplicitStringB5cxx11Ev.cold, .-_ZNK2v88internal6torque18BuiltinPointerType16ToExplicitStringB5cxx11Ev.cold
.LCOLDE59:
	.section	.text._ZNK2v88internal6torque18BuiltinPointerType16ToExplicitStringB5cxx11Ev
.LHOTE59:
	.section	.text._ZN2v88internal6torque11ReportErrorIJRNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA3_KcRKS8_RA35_SA_SE_RA17_SA_EEEvDpOT_,"axG",@progbits,_ZN2v88internal6torque11ReportErrorIJRNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA3_KcRKS8_RA35_SA_SE_RA17_SA_EEEvDpOT_,comdat
	.p2align 4
	.weak	_ZN2v88internal6torque11ReportErrorIJRNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA3_KcRKS8_RA35_SA_SE_RA17_SA_EEEvDpOT_
	.type	_ZN2v88internal6torque11ReportErrorIJRNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA3_KcRKS8_RA35_SA_SE_RA17_SA_EEEvDpOT_, @function
_ZN2v88internal6torque11ReportErrorIJRNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA3_KcRKS8_RA35_SA_SE_RA17_SA_EEEvDpOT_:
.LFB7928:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA7928
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-448(%rbp), %r15
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-432(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$536, %rsp
	movq	%rdi, -568(%rbp)
	movq	%r15, %rdi
	movq	%rcx, -552(%rbp)
	movq	%r9, -560(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
.LEHB195:
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEC1Ev@PLT
.LEHE195:
	movq	-568(%rbp), %rax
	movq	%r12, %rdi
	movq	8(%rax), %rdx
	movq	(%rax), %rsi
.LEHB196:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	8(%r13), %rdx
	movq	0(%r13), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-552(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	8(%rbx), %rdx
	movq	(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-560(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	leaq	-544(%rbp), %r13
	leaq	-424(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNKSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEE3strEv@PLT
.LEHE196:
	leaq	-512(%rbp), %r12
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
.LEHB197:
	call	_ZN2v88internal6torque14MessageBuilderC1ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_13TorqueMessage4KindE@PLT
.LEHE197:
	movq	-544(%rbp), %rdi
	leaq	-528(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1384
	call	_ZdlPv@PLT
.L1384:
	movq	%r15, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r12, %rdi
.LEHB198:
	call	_ZNK2v88internal6torque14MessageBuilder5ThrowEv@PLT
.LEHE198:
.L1391:
	endbr64
	movq	%rax, %r12
	jmp	.L1387
.L1390:
	endbr64
	movq	%rax, %r13
	jmp	.L1388
.L1392:
	endbr64
	movq	%rax, %r12
.L1385:
	movq	-544(%rbp), %rdi
	leaq	-528(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1387
	call	_ZdlPv@PLT
.L1387:
	movq	%r15, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r12, %rdi
.LEHB199:
	call	_Unwind_Resume@PLT
.L1388:
	movq	%r12, %rdi
	call	_ZN2v88internal6torque14MessageBuilderD1Ev
	movq	%r13, %rdi
	call	_Unwind_Resume@PLT
.LEHE199:
	.cfi_endproc
.LFE7928:
	.section	.gcc_except_table._ZN2v88internal6torque11ReportErrorIJRNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA3_KcRKS8_RA35_SA_SE_RA17_SA_EEEvDpOT_,"aG",@progbits,_ZN2v88internal6torque11ReportErrorIJRNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA3_KcRKS8_RA35_SA_SE_RA17_SA_EEEvDpOT_,comdat
.LLSDA7928:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE7928-.LLSDACSB7928
.LLSDACSB7928:
	.uleb128 .LEHB195-.LFB7928
	.uleb128 .LEHE195-.LEHB195
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB196-.LFB7928
	.uleb128 .LEHE196-.LEHB196
	.uleb128 .L1391-.LFB7928
	.uleb128 0
	.uleb128 .LEHB197-.LFB7928
	.uleb128 .LEHE197-.LEHB197
	.uleb128 .L1392-.LFB7928
	.uleb128 0
	.uleb128 .LEHB198-.LFB7928
	.uleb128 .LEHE198-.LEHB198
	.uleb128 .L1390-.LFB7928
	.uleb128 0
	.uleb128 .LEHB199-.LFB7928
	.uleb128 .LEHE199-.LEHB199
	.uleb128 0
	.uleb128 0
.LLSDACSE7928:
	.section	.text._ZN2v88internal6torque11ReportErrorIJRNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA3_KcRKS8_RA35_SA_SE_RA17_SA_EEEvDpOT_,"axG",@progbits,_ZN2v88internal6torque11ReportErrorIJRNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA3_KcRKS8_RA35_SA_SE_RA17_SA_EEEvDpOT_,comdat
	.size	_ZN2v88internal6torque11ReportErrorIJRNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA3_KcRKS8_RA35_SA_SE_RA17_SA_EEEvDpOT_, .-_ZN2v88internal6torque11ReportErrorIJRNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA3_KcRKS8_RA35_SA_SE_RA17_SA_EEEvDpOT_
	.section	.text._ZN2v88internal6torque11ReportErrorIJRNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA3_KcRKS8_RA35_SA_SE_RA45_SA_SE_RA2_SA_EEEvDpOT_,"axG",@progbits,_ZN2v88internal6torque11ReportErrorIJRNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA3_KcRKS8_RA35_SA_SE_RA45_SA_SE_RA2_SA_EEEvDpOT_,comdat
	.p2align 4
	.weak	_ZN2v88internal6torque11ReportErrorIJRNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA3_KcRKS8_RA35_SA_SE_RA45_SA_SE_RA2_SA_EEEvDpOT_
	.type	_ZN2v88internal6torque11ReportErrorIJRNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA3_KcRKS8_RA35_SA_SE_RA45_SA_SE_RA2_SA_EEEvDpOT_, @function
_ZN2v88internal6torque11ReportErrorIJRNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA3_KcRKS8_RA35_SA_SE_RA45_SA_SE_RA2_SA_EEEvDpOT_:
.LFB7933:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA7933
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-448(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-432(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$552, %rsp
	movq	24(%rbp), %rax
	movq	%rdi, -584(%rbp)
	movq	%r14, %rdi
	movq	%rsi, -552(%rbp)
	movq	16(%rbp), %r15
	movq	%rax, -576(%rbp)
	movq	%rcx, -560(%rbp)
	movq	%r9, -568(%rbp)
	movq	%fs:40, %rdx
	movq	%rdx, -56(%rbp)
	xorl	%edx, %edx
.LEHB200:
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEC1Ev@PLT
.LEHE200:
	movq	-584(%rbp), %rax
	movq	%r12, %rdi
	movq	8(%rax), %rdx
	movq	(%rax), %rsi
.LEHB201:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-552(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	8(%r13), %rdx
	movq	0(%r13), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-560(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	8(%rbx), %rdx
	movq	(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-568(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	8(%r15), %rdx
	movq	(%r15), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-576(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	leaq	-544(%rbp), %r13
	leaq	-424(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNKSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEE3strEv@PLT
.LEHE201:
	leaq	-512(%rbp), %r12
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
.LEHB202:
	call	_ZN2v88internal6torque14MessageBuilderC1ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_13TorqueMessage4KindE@PLT
.LEHE202:
	movq	-544(%rbp), %rdi
	leaq	-528(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1395
	call	_ZdlPv@PLT
.L1395:
	movq	%r14, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r12, %rdi
.LEHB203:
	call	_ZNK2v88internal6torque14MessageBuilder5ThrowEv@PLT
.LEHE203:
.L1402:
	endbr64
	movq	%rax, %r12
	jmp	.L1398
.L1401:
	endbr64
	movq	%rax, %r13
	jmp	.L1399
.L1403:
	endbr64
	movq	%rax, %r12
.L1396:
	movq	-544(%rbp), %rdi
	leaq	-528(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1398
	call	_ZdlPv@PLT
.L1398:
	movq	%r14, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r12, %rdi
.LEHB204:
	call	_Unwind_Resume@PLT
.L1399:
	movq	%r12, %rdi
	call	_ZN2v88internal6torque14MessageBuilderD1Ev
	movq	%r13, %rdi
	call	_Unwind_Resume@PLT
.LEHE204:
	.cfi_endproc
.LFE7933:
	.section	.gcc_except_table._ZN2v88internal6torque11ReportErrorIJRNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA3_KcRKS8_RA35_SA_SE_RA45_SA_SE_RA2_SA_EEEvDpOT_,"aG",@progbits,_ZN2v88internal6torque11ReportErrorIJRNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA3_KcRKS8_RA35_SA_SE_RA45_SA_SE_RA2_SA_EEEvDpOT_,comdat
.LLSDA7933:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE7933-.LLSDACSB7933
.LLSDACSB7933:
	.uleb128 .LEHB200-.LFB7933
	.uleb128 .LEHE200-.LEHB200
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB201-.LFB7933
	.uleb128 .LEHE201-.LEHB201
	.uleb128 .L1402-.LFB7933
	.uleb128 0
	.uleb128 .LEHB202-.LFB7933
	.uleb128 .LEHE202-.LEHB202
	.uleb128 .L1403-.LFB7933
	.uleb128 0
	.uleb128 .LEHB203-.LFB7933
	.uleb128 .LEHE203-.LEHB203
	.uleb128 .L1401-.LFB7933
	.uleb128 0
	.uleb128 .LEHB204-.LFB7933
	.uleb128 .LEHE204-.LEHB204
	.uleb128 0
	.uleb128 0
.LLSDACSE7933:
	.section	.text._ZN2v88internal6torque11ReportErrorIJRNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA3_KcRKS8_RA35_SA_SE_RA45_SA_SE_RA2_SA_EEEvDpOT_,"axG",@progbits,_ZN2v88internal6torque11ReportErrorIJRNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA3_KcRKS8_RA35_SA_SE_RA45_SA_SE_RA2_SA_EEEvDpOT_,comdat
	.size	_ZN2v88internal6torque11ReportErrorIJRNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA3_KcRKS8_RA35_SA_SE_RA45_SA_SE_RA2_SA_EEEvDpOT_, .-_ZN2v88internal6torque11ReportErrorIJRNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA3_KcRKS8_RA35_SA_SE_RA45_SA_SE_RA2_SA_EEEvDpOT_
	.section	.text._ZN2v88internal6torque11ReportErrorIJRA10_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA11_S3_SB_EEEvDpOT_,"axG",@progbits,_ZN2v88internal6torque11ReportErrorIJRA10_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA11_S3_SB_EEEvDpOT_,comdat
	.p2align 4
	.weak	_ZN2v88internal6torque11ReportErrorIJRA10_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA11_S3_SB_EEEvDpOT_
	.type	_ZN2v88internal6torque11ReportErrorIJRA10_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA11_S3_SB_EEEvDpOT_, @function
_ZN2v88internal6torque11ReportErrorIJRA10_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA11_S3_SB_EEEvDpOT_:
.LFB7948:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA7948
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-448(%rbp), %r15
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-432(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$520, %rsp
	movq	%rdi, -552(%rbp)
	movq	%r15, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
.LEHB205:
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEC1Ev@PLT
.LEHE205:
	movq	-552(%rbp), %r8
	movq	%r12, %rdi
	movq	%r8, %rsi
.LEHB206:
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	8(%r13), %rdx
	movq	0(%r13), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	8(%rbx), %rdx
	movq	(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	-544(%rbp), %r13
	leaq	-424(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNKSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEE3strEv@PLT
.LEHE206:
	leaq	-512(%rbp), %r12
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
.LEHB207:
	call	_ZN2v88internal6torque14MessageBuilderC1ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_13TorqueMessage4KindE@PLT
.LEHE207:
	movq	-544(%rbp), %rdi
	leaq	-528(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1406
	call	_ZdlPv@PLT
.L1406:
	movq	%r15, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r12, %rdi
.LEHB208:
	call	_ZNK2v88internal6torque14MessageBuilder5ThrowEv@PLT
.LEHE208:
.L1413:
	endbr64
	movq	%rax, %r12
	jmp	.L1409
.L1412:
	endbr64
	movq	%rax, %r13
	jmp	.L1410
.L1414:
	endbr64
	movq	%rax, %r12
.L1407:
	movq	-544(%rbp), %rdi
	leaq	-528(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1409
	call	_ZdlPv@PLT
.L1409:
	movq	%r15, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r12, %rdi
.LEHB209:
	call	_Unwind_Resume@PLT
.L1410:
	movq	%r12, %rdi
	call	_ZN2v88internal6torque14MessageBuilderD1Ev
	movq	%r13, %rdi
	call	_Unwind_Resume@PLT
.LEHE209:
	.cfi_endproc
.LFE7948:
	.section	.gcc_except_table._ZN2v88internal6torque11ReportErrorIJRA10_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA11_S3_SB_EEEvDpOT_,"aG",@progbits,_ZN2v88internal6torque11ReportErrorIJRA10_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA11_S3_SB_EEEvDpOT_,comdat
.LLSDA7948:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE7948-.LLSDACSB7948
.LLSDACSB7948:
	.uleb128 .LEHB205-.LFB7948
	.uleb128 .LEHE205-.LEHB205
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB206-.LFB7948
	.uleb128 .LEHE206-.LEHB206
	.uleb128 .L1413-.LFB7948
	.uleb128 0
	.uleb128 .LEHB207-.LFB7948
	.uleb128 .LEHE207-.LEHB207
	.uleb128 .L1414-.LFB7948
	.uleb128 0
	.uleb128 .LEHB208-.LFB7948
	.uleb128 .LEHE208-.LEHB208
	.uleb128 .L1412-.LFB7948
	.uleb128 0
	.uleb128 .LEHB209-.LFB7948
	.uleb128 .LEHE209-.LEHB209
	.uleb128 0
	.uleb128 0
.LLSDACSE7948:
	.section	.text._ZN2v88internal6torque11ReportErrorIJRA10_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA11_S3_SB_EEEvDpOT_,"axG",@progbits,_ZN2v88internal6torque11ReportErrorIJRA10_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA11_S3_SB_EEEvDpOT_,comdat
	.size	_ZN2v88internal6torque11ReportErrorIJRA10_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA11_S3_SB_EEEvDpOT_, .-_ZN2v88internal6torque11ReportErrorIJRA10_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA11_S3_SB_EEEvDpOT_
	.section	.rodata._ZNK2v88internal6torque13AggregateType19LookupFieldInternalERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.str1.1,"aMS",@progbits,1
.LC60:
	.string	" found in "
.LC61:
	.string	"no field "
	.section	.text.unlikely._ZNK2v88internal6torque13AggregateType19LookupFieldInternalERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"ax",@progbits
	.align 2
.LCOLDB62:
	.section	.text._ZNK2v88internal6torque13AggregateType19LookupFieldInternalERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"ax",@progbits
.LHOTB62:
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal6torque13AggregateType19LookupFieldInternalERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZNK2v88internal6torque13AggregateType19LookupFieldInternalERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZNK2v88internal6torque13AggregateType19LookupFieldInternalERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB6556:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6556
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	80(%rdi), %r12
	movq	88(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	%r13, %r12
	je	.L1417
	movq	8(%rsi), %rbx
	jmp	.L1420
	.p2align 4,,10
	.p2align 3
.L1418:
	addq	$104, %r12
	cmpq	%r12, %r13
	je	.L1417
.L1420:
	cmpq	%rbx, 56(%r12)
	jne	.L1418
	testq	%rbx, %rbx
	je	.L1416
	movq	48(%r12), %rdi
	movq	(%r14), %rsi
	movq	%rbx, %rdx
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L1418
.L1416:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1438
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1417:
	.cfi_restore_state
	movq	16(%r15), %r12
	testq	%r12, %r12
	je	.L1421
	cmpl	$5, 8(%r12)
	jne	.L1421
	cmpb	$0, 72(%r12)
	jne	.L1422
	movq	(%r12), %rax
	movq	%r12, %rdi
.LEHB210:
	call	*96(%rax)
.L1422:
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZNK2v88internal6torque13AggregateType19LookupFieldInternalERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	%rax, %r12
	jmp	.L1416
	.p2align 4,,10
	.p2align 3
.L1421:
	leaq	-96(%rbp), %r12
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZNK2v88internal6torque4Type8ToStringB5cxx11Ev
.LEHE210:
	movq	%r12, %rcx
	movq	%r14, %rsi
	leaq	.LC60(%rip), %rdx
	leaq	.LC61(%rip), %rdi
.LEHB211:
	call	_ZN2v88internal6torque11ReportErrorIJRA10_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA11_S3_SB_EEEvDpOT_
.LEHE211:
.L1438:
	call	__stack_chk_fail@PLT
.L1426:
	endbr64
	movq	%rax, %r12
	jmp	.L1423
	.section	.gcc_except_table._ZNK2v88internal6torque13AggregateType19LookupFieldInternalERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"a",@progbits
.LLSDA6556:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6556-.LLSDACSB6556
.LLSDACSB6556:
	.uleb128 .LEHB210-.LFB6556
	.uleb128 .LEHE210-.LEHB210
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB211-.LFB6556
	.uleb128 .LEHE211-.LEHB211
	.uleb128 .L1426-.LFB6556
	.uleb128 0
.LLSDACSE6556:
	.section	.text._ZNK2v88internal6torque13AggregateType19LookupFieldInternalERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.cfi_endproc
	.section	.text.unlikely._ZNK2v88internal6torque13AggregateType19LookupFieldInternalERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6556
	.type	_ZNK2v88internal6torque13AggregateType19LookupFieldInternalERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.cold, @function
_ZNK2v88internal6torque13AggregateType19LookupFieldInternalERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.cold:
.LFSB6556:
.L1423:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1424
	call	_ZdlPv@PLT
.L1424:
	movq	%r12, %rdi
.LEHB212:
	call	_Unwind_Resume@PLT
.LEHE212:
	.cfi_endproc
.LFE6556:
	.section	.gcc_except_table._ZNK2v88internal6torque13AggregateType19LookupFieldInternalERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
.LLSDAC6556:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6556-.LLSDACSBC6556
.LLSDACSBC6556:
	.uleb128 .LEHB212-.LCOLDB62
	.uleb128 .LEHE212-.LEHB212
	.uleb128 0
	.uleb128 0
.LLSDACSEC6556:
	.section	.text.unlikely._ZNK2v88internal6torque13AggregateType19LookupFieldInternalERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.text._ZNK2v88internal6torque13AggregateType19LookupFieldInternalERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.size	_ZNK2v88internal6torque13AggregateType19LookupFieldInternalERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZNK2v88internal6torque13AggregateType19LookupFieldInternalERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.text.unlikely._ZNK2v88internal6torque13AggregateType19LookupFieldInternalERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.size	_ZNK2v88internal6torque13AggregateType19LookupFieldInternalERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.cold, .-_ZNK2v88internal6torque13AggregateType19LookupFieldInternalERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.cold
.LCOLDE62:
	.section	.text._ZNK2v88internal6torque13AggregateType19LookupFieldInternalERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
.LHOTE62:
	.section	.text._ZNK2v88internal6torque13AggregateType11LookupFieldERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal6torque13AggregateType11LookupFieldERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZNK2v88internal6torque13AggregateType11LookupFieldERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZNK2v88internal6torque13AggregateType11LookupFieldERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB6557:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	cmpb	$0, 72(%rdi)
	movq	%rdi, %r12
	jne	.L1440
	movq	(%rdi), %rax
	call	*96(%rax)
.L1440:
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNK2v88internal6torque13AggregateType19LookupFieldInternalERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.cfi_endproc
.LFE6557:
	.size	_ZNK2v88internal6torque13AggregateType11LookupFieldERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZNK2v88internal6torque13AggregateType11LookupFieldERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.text._ZN2v88internal6torque11ReportErrorIJRA9_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA28_S3_SD_RA2_S3_EEEvDpOT_,"axG",@progbits,_ZN2v88internal6torque11ReportErrorIJRA9_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA28_S3_SD_RA2_S3_EEEvDpOT_,comdat
	.p2align 4
	.weak	_ZN2v88internal6torque11ReportErrorIJRA9_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA28_S3_SD_RA2_S3_EEEvDpOT_
	.type	_ZN2v88internal6torque11ReportErrorIJRA9_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA28_S3_SD_RA2_S3_EEEvDpOT_, @function
_ZN2v88internal6torque11ReportErrorIJRA9_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA28_S3_SD_RA2_S3_EEEvDpOT_:
.LFB8037:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA8037
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-448(%rbp), %r15
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-432(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$520, %rsp
	movq	%rdi, -560(%rbp)
	movq	%r15, %rdi
	movq	%r8, -552(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
.LEHB213:
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEC1Ev@PLT
.LEHE213:
	movq	-560(%rbp), %r9
	movq	%r12, %rdi
	movq	%r9, %rsi
.LEHB214:
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	8(%r13), %rdx
	movq	0(%r13), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	8(%rbx), %rdx
	movq	(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-552(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	leaq	-544(%rbp), %r13
	leaq	-424(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNKSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEE3strEv@PLT
.LEHE214:
	leaq	-512(%rbp), %r12
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
.LEHB215:
	call	_ZN2v88internal6torque14MessageBuilderC1ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_13TorqueMessage4KindE@PLT
.LEHE215:
	movq	-544(%rbp), %rdi
	leaq	-528(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1443
	call	_ZdlPv@PLT
.L1443:
	movq	%r15, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r12, %rdi
.LEHB216:
	call	_ZNK2v88internal6torque14MessageBuilder5ThrowEv@PLT
.LEHE216:
.L1450:
	endbr64
	movq	%rax, %r12
	jmp	.L1446
.L1449:
	endbr64
	movq	%rax, %r13
	jmp	.L1447
.L1451:
	endbr64
	movq	%rax, %r12
.L1444:
	movq	-544(%rbp), %rdi
	leaq	-528(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1446
	call	_ZdlPv@PLT
.L1446:
	movq	%r15, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r12, %rdi
.LEHB217:
	call	_Unwind_Resume@PLT
.L1447:
	movq	%r12, %rdi
	call	_ZN2v88internal6torque14MessageBuilderD1Ev
	movq	%r13, %rdi
	call	_Unwind_Resume@PLT
.LEHE217:
	.cfi_endproc
.LFE8037:
	.section	.gcc_except_table._ZN2v88internal6torque11ReportErrorIJRA9_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA28_S3_SD_RA2_S3_EEEvDpOT_,"aG",@progbits,_ZN2v88internal6torque11ReportErrorIJRA9_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA28_S3_SD_RA2_S3_EEEvDpOT_,comdat
.LLSDA8037:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE8037-.LLSDACSB8037
.LLSDACSB8037:
	.uleb128 .LEHB213-.LFB8037
	.uleb128 .LEHE213-.LEHB213
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB214-.LFB8037
	.uleb128 .LEHE214-.LEHB214
	.uleb128 .L1450-.LFB8037
	.uleb128 0
	.uleb128 .LEHB215-.LFB8037
	.uleb128 .LEHE215-.LEHB215
	.uleb128 .L1451-.LFB8037
	.uleb128 0
	.uleb128 .LEHB216-.LFB8037
	.uleb128 .LEHE216-.LEHB216
	.uleb128 .L1449-.LFB8037
	.uleb128 0
	.uleb128 .LEHB217-.LFB8037
	.uleb128 .LEHE217-.LEHB217
	.uleb128 0
	.uleb128 0
.LLSDACSE8037:
	.section	.text._ZN2v88internal6torque11ReportErrorIJRA9_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA28_S3_SD_RA2_S3_EEEvDpOT_,"axG",@progbits,_ZN2v88internal6torque11ReportErrorIJRA9_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA28_S3_SD_RA2_S3_EEEvDpOT_,comdat
	.size	_ZN2v88internal6torque11ReportErrorIJRA9_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA28_S3_SD_RA2_S3_EEEvDpOT_, .-_ZN2v88internal6torque11ReportErrorIJRA9_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA28_S3_SD_RA2_S3_EEEvDpOT_
	.section	.text._ZN2v88internal6torque11ReportErrorIJRA16_KcRKNS1_4TypeERA25_S3_EEEvDpOT_,"axG",@progbits,_ZN2v88internal6torque11ReportErrorIJRA16_KcRKNS1_4TypeERA25_S3_EEEvDpOT_,comdat
	.p2align 4
	.weak	_ZN2v88internal6torque11ReportErrorIJRA16_KcRKNS1_4TypeERA25_S3_EEEvDpOT_
	.type	_ZN2v88internal6torque11ReportErrorIJRA16_KcRKNS1_4TypeERA25_S3_EEEvDpOT_, @function
_ZN2v88internal6torque11ReportErrorIJRA16_KcRKNS1_4TypeERA25_S3_EEEvDpOT_:
.LFB8041:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA8041
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-432(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-448(%rbp), %rbx
	movq	%rbx, %rdi
	subq	$504, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
.LEHB218:
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEC1Ev@PLT
.LEHE218:
	movq	%r12, %rsi
	movq	%r13, %rdi
.LEHB219:
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	leaq	-512(%rbp), %r12
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZNK2v88internal6torque4Type8ToStringB5cxx11Ev
.LEHE219:
	movq	-504(%rbp), %rdx
	movq	-512(%rbp), %rsi
	movq	%r13, %rdi
.LEHB220:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE220:
	movq	-512(%rbp), %rdi
	leaq	-496(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1454
	call	_ZdlPv@PLT
.L1454:
	movq	%r14, %rsi
	movq	%r13, %rdi
.LEHB221:
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	leaq	-544(%rbp), %r13
	leaq	-424(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNKSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEE3strEv@PLT
.LEHE221:
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
.LEHB222:
	call	_ZN2v88internal6torque14MessageBuilderC1ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_13TorqueMessage4KindE@PLT
.LEHE222:
	movq	-544(%rbp), %rdi
	leaq	-528(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1459
	call	_ZdlPv@PLT
.L1459:
	movq	%rbx, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r12, %rdi
.LEHB223:
	call	_ZNK2v88internal6torque14MessageBuilder5ThrowEv@PLT
.LEHE223:
.L1465:
	endbr64
	movq	%rax, %r12
	jmp	.L1458
.L1464:
	endbr64
	movq	%rax, %r13
	jmp	.L1462
.L1460:
	movq	-544(%rbp), %rdi
	leaq	-528(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1458
.L1470:
	call	_ZdlPv@PLT
.L1458:
	movq	%rbx, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r12, %rdi
.LEHB224:
	call	_Unwind_Resume@PLT
.L1462:
	movq	%r12, %rdi
	call	_ZN2v88internal6torque14MessageBuilderD1Ev
	movq	%r13, %rdi
	call	_Unwind_Resume@PLT
.LEHE224:
.L1466:
	endbr64
	movq	%rax, %r12
	jmp	.L1460
.L1467:
	endbr64
	movq	%rax, %r12
.L1456:
	movq	-512(%rbp), %rdi
	leaq	-496(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L1470
	jmp	.L1458
	.cfi_endproc
.LFE8041:
	.section	.gcc_except_table._ZN2v88internal6torque11ReportErrorIJRA16_KcRKNS1_4TypeERA25_S3_EEEvDpOT_,"aG",@progbits,_ZN2v88internal6torque11ReportErrorIJRA16_KcRKNS1_4TypeERA25_S3_EEEvDpOT_,comdat
.LLSDA8041:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE8041-.LLSDACSB8041
.LLSDACSB8041:
	.uleb128 .LEHB218-.LFB8041
	.uleb128 .LEHE218-.LEHB218
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB219-.LFB8041
	.uleb128 .LEHE219-.LEHB219
	.uleb128 .L1465-.LFB8041
	.uleb128 0
	.uleb128 .LEHB220-.LFB8041
	.uleb128 .LEHE220-.LEHB220
	.uleb128 .L1467-.LFB8041
	.uleb128 0
	.uleb128 .LEHB221-.LFB8041
	.uleb128 .LEHE221-.LEHB221
	.uleb128 .L1465-.LFB8041
	.uleb128 0
	.uleb128 .LEHB222-.LFB8041
	.uleb128 .LEHE222-.LEHB222
	.uleb128 .L1466-.LFB8041
	.uleb128 0
	.uleb128 .LEHB223-.LFB8041
	.uleb128 .LEHE223-.LEHB223
	.uleb128 .L1464-.LFB8041
	.uleb128 0
	.uleb128 .LEHB224-.LFB8041
	.uleb128 .LEHE224-.LEHB224
	.uleb128 0
	.uleb128 0
.LLSDACSE8041:
	.section	.text._ZN2v88internal6torque11ReportErrorIJRA16_KcRKNS1_4TypeERA25_S3_EEEvDpOT_,"axG",@progbits,_ZN2v88internal6torque11ReportErrorIJRA16_KcRKNS1_4TypeERA25_S3_EEEvDpOT_,comdat
	.size	_ZN2v88internal6torque11ReportErrorIJRA16_KcRKNS1_4TypeERA25_S3_EEEvDpOT_, .-_ZN2v88internal6torque11ReportErrorIJRA16_KcRKNS1_4TypeERA25_S3_EEEvDpOT_
	.section	.rodata._ZNK2v88internal6torque5Field23GetFieldSizeInformationB5cxx11Ev.str1.1,"aMS",@progbits,1
.LC63:
	.string	"kTaggedSize"
.LC64:
	.string	"kSystemPointerSize"
.LC65:
	.string	"0"
.LC66:
	.string	"int8"
.LC67:
	.string	"kUInt8Size"
.LC68:
	.string	"uint8"
.LC69:
	.string	"int16"
.LC70:
	.string	"kUInt16Size"
.LC71:
	.string	"uint16"
.LC72:
	.string	"int32"
.LC73:
	.string	"kInt32Size"
.LC74:
	.string	"uint32"
.LC75:
	.string	"float64"
.LC76:
	.string	"kDoubleSize"
.LC77:
	.string	"intptr"
.LC78:
	.string	"kIntptrSize"
.LC79:
	.string	"uintptr"
.LC80:
	.string	" are not (yet) supported"
.LC81:
	.string	"fields of type "
	.section	.text.unlikely._ZNK2v88internal6torque5Field23GetFieldSizeInformationB5cxx11Ev,"ax",@progbits
	.align 2
.LCOLDB82:
	.section	.text._ZNK2v88internal6torque5Field23GetFieldSizeInformationB5cxx11Ev,"ax",@progbits
.LHOTB82:
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal6torque5Field23GetFieldSizeInformationB5cxx11Ev
	.type	_ZNK2v88internal6torque5Field23GetFieldSizeInformationB5cxx11Ev, @function
_ZNK2v88internal6torque5Field23GetFieldSizeInformationB5cxx11Ev:
.LFB6635:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6635
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-112(%rbp), %r15
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -56
	movq	80(%rsi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movb	$0, -104(%rbp)
	movabsq	$7312272888175750691, %rax
	movq	%rax, -112(%rbp)
	movq	%r15, -128(%rbp)
	movq	$8, -120(%rbp)
	movq	0(%r13), %rax
	movq	16(%rax), %rax
	movq	%rax, -136(%rbp)
.LEHB225:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_10TypeOracleEEERPNT_5ScopeEv@PLT
.LEHE225:
	leaq	-96(%rbp), %r14
	leaq	-80(%rbp), %rbx
	movl	$25701, %edx
	movl	$1734828372, -80(%rbp)
	movq	%r14, %rdi
	movq	%rbx, -96(%rbp)
	movw	%dx, -76(%rbp)
	movq	$6, -88(%rbp)
	movb	$0, -74(%rbp)
.LEHB226:
	call	_ZN2v88internal6torque12Declarations16LookupGlobalTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
.LEHE226:
	movq	-96(%rbp), %rdi
	movq	%rax, %rsi
	cmpq	%rbx, %rdi
	je	.L1472
	movq	%rax, -144(%rbp)
	call	_ZdlPv@PLT
	movq	-144(%rbp), %rsi
.L1472:
	movq	-136(%rbp), %rax
	movq	%r13, %rdi
.LEHB227:
	call	*%rax
	testb	%al, %al
	je	.L1477
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_18TargetArchitectureEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rax
	movq	-120(%rbp), %rdx
	leaq	-128(%rbp), %rdi
	xorl	%esi, %esi
	movl	$11, %r8d
	leaq	.LC63(%rip), %rcx
	movslq	(%rax), %rbx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L1478:
	leaq	16(%r12), %rax
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, (%r12)
	addq	%rsi, %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-128(%rbp), %rdi
	movq	%rbx, 32(%r12)
	cmpq	%r15, %rdi
	je	.L1471
	call	_ZdlPv@PLT
.L1471:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1576
	addq	$104, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1477:
	.cfi_restore_state
	movq	0(%r13), %rax
	movq	16(%rax), %rax
	movq	%rax, -136(%rbp)
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_10TypeOracleEEERPNT_5ScopeEv@PLT
.LEHE227:
	movl	$29300, %eax
	movq	%r14, %rdi
	movq	%rbx, -96(%rbp)
	movl	$1350000978, -80(%rbp)
	movw	%ax, -76(%rbp)
	movq	$6, -88(%rbp)
	movb	$0, -74(%rbp)
.LEHB228:
	call	_ZN2v88internal6torque12Declarations16LookupGlobalTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
.LEHE228:
	movq	-96(%rbp), %rdi
	movq	%rax, %rsi
	cmpq	%rbx, %rdi
	je	.L1479
	movq	%rax, -144(%rbp)
	call	_ZdlPv@PLT
	movq	-144(%rbp), %rsi
.L1479:
	movq	-136(%rbp), %rax
	movq	%r13, %rdi
.LEHB229:
	call	*%rax
	testb	%al, %al
	jne	.L1577
	movq	0(%r13), %rax
	movq	16(%rax), %rax
	movq	%rax, -136(%rbp)
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_10TypeOracleEEERPNT_5ScopeEv@PLT
.LEHE229:
	movq	%r14, %rdi
	movq	%rbx, -96(%rbp)
	movl	$1684631414, -80(%rbp)
	movq	$4, -88(%rbp)
	movb	$0, -76(%rbp)
.LEHB230:
	call	_ZN2v88internal6torque12Declarations16LookupGlobalTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
.LEHE230:
	movq	-96(%rbp), %rdi
	movq	%rax, %rsi
	cmpq	%rbx, %rdi
	je	.L1484
	movq	%rax, -144(%rbp)
	call	_ZdlPv@PLT
	movq	-144(%rbp), %rsi
.L1484:
	movq	-136(%rbp), %rax
	movq	%r13, %rdi
.LEHB231:
	call	*%rax
	testb	%al, %al
	jne	.L1578
	movq	0(%r13), %rax
	movq	16(%rax), %rax
	movq	%rax, -136(%rbp)
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_10TypeOracleEEERPNT_5ScopeEv@PLT
	leaq	.LC66(%rip), %rsi
	movq	%r14, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
.LEHE231:
	movq	%r14, %rdi
.LEHB232:
	call	_ZN2v88internal6torque12Declarations16LookupGlobalTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
.LEHE232:
	movq	-96(%rbp), %rdi
	movq	%rax, %rsi
	cmpq	%rbx, %rdi
	je	.L1489
	movq	%rax, -144(%rbp)
	call	_ZdlPv@PLT
	movq	-144(%rbp), %rsi
.L1489:
	movq	-136(%rbp), %rax
	movq	%r13, %rdi
.LEHB233:
	call	*%rax
	testb	%al, %al
	jne	.L1559
	movq	0(%r13), %rax
	movq	16(%rax), %rax
	movq	%rax, -136(%rbp)
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_10TypeOracleEEERPNT_5ScopeEv@PLT
	leaq	.LC68(%rip), %rsi
	movq	%r14, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
.LEHE233:
	movq	%r14, %rdi
.LEHB234:
	call	_ZN2v88internal6torque12Declarations16LookupGlobalTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
.LEHE234:
	movq	-96(%rbp), %rdi
	movq	%rax, %rsi
	cmpq	%rbx, %rdi
	je	.L1494
	movq	%rax, -144(%rbp)
	call	_ZdlPv@PLT
	movq	-144(%rbp), %rsi
.L1494:
	movq	-136(%rbp), %rax
	movq	%r13, %rdi
.LEHB235:
	call	*%rax
	testb	%al, %al
	jne	.L1559
	movq	0(%r13), %rax
	movq	16(%rax), %rax
	movq	%rax, -136(%rbp)
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_10TypeOracleEEERPNT_5ScopeEv@PLT
	leaq	.LC69(%rip), %rsi
	movq	%r14, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
.LEHE235:
	movq	%r14, %rdi
.LEHB236:
	call	_ZN2v88internal6torque12Declarations16LookupGlobalTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
.LEHE236:
	movq	-96(%rbp), %rdi
	movq	%rax, %rsi
	cmpq	%rbx, %rdi
	je	.L1500
	movq	%rax, -144(%rbp)
	call	_ZdlPv@PLT
	movq	-144(%rbp), %rsi
.L1500:
	movq	-136(%rbp), %rax
	movq	%r13, %rdi
.LEHB237:
	call	*%rax
	testb	%al, %al
	jne	.L1561
	movq	0(%r13), %rax
	movq	16(%rax), %rax
	movq	%rax, -136(%rbp)
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_10TypeOracleEEERPNT_5ScopeEv@PLT
	leaq	.LC71(%rip), %rsi
	movq	%r14, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
.LEHE237:
	movq	%r14, %rdi
.LEHB238:
	call	_ZN2v88internal6torque12Declarations16LookupGlobalTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
.LEHE238:
	movq	-96(%rbp), %rdi
	movq	%rax, %rsi
	cmpq	%rbx, %rdi
	je	.L1505
	movq	%rax, -144(%rbp)
	call	_ZdlPv@PLT
	movq	-144(%rbp), %rsi
.L1505:
	movq	-136(%rbp), %rax
	movq	%r13, %rdi
.LEHB239:
	call	*%rax
	testb	%al, %al
	jne	.L1561
	movq	0(%r13), %rax
	movq	16(%rax), %rax
	movq	%rax, -136(%rbp)
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_10TypeOracleEEERPNT_5ScopeEv@PLT
	leaq	.LC72(%rip), %rsi
	movq	%r14, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
.LEHE239:
	movq	%r14, %rdi
.LEHB240:
	call	_ZN2v88internal6torque12Declarations16LookupGlobalTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
.LEHE240:
	movq	-96(%rbp), %rdi
	movq	%rax, %rsi
	cmpq	%rbx, %rdi
	je	.L1511
	movq	%rax, -144(%rbp)
	call	_ZdlPv@PLT
	movq	-144(%rbp), %rsi
.L1511:
	movq	-136(%rbp), %rax
	movq	%r13, %rdi
.LEHB241:
	call	*%rax
	testb	%al, %al
	jne	.L1563
	movq	0(%r13), %rax
	movq	16(%rax), %rax
	movq	%rax, -136(%rbp)
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_10TypeOracleEEERPNT_5ScopeEv@PLT
	leaq	.LC74(%rip), %rsi
	movq	%r14, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
.LEHE241:
	movq	%r14, %rdi
.LEHB242:
	call	_ZN2v88internal6torque12Declarations16LookupGlobalTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
.LEHE242:
	movq	-96(%rbp), %rdi
	movq	%rax, %rsi
	cmpq	%rbx, %rdi
	je	.L1516
	movq	%rax, -144(%rbp)
	call	_ZdlPv@PLT
	movq	-144(%rbp), %rsi
.L1516:
	movq	-136(%rbp), %rax
	movq	%r13, %rdi
.LEHB243:
	call	*%rax
	testb	%al, %al
	jne	.L1563
	movq	0(%r13), %rax
	movq	16(%rax), %rax
	movq	%rax, -136(%rbp)
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_10TypeOracleEEERPNT_5ScopeEv@PLT
	leaq	.LC75(%rip), %rsi
	movq	%r14, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
.LEHE243:
	movq	%r14, %rdi
.LEHB244:
	call	_ZN2v88internal6torque12Declarations16LookupGlobalTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
.LEHE244:
	movq	-96(%rbp), %rdi
	movq	%rax, %rsi
	cmpq	%rbx, %rdi
	je	.L1522
	movq	%rax, -144(%rbp)
	call	_ZdlPv@PLT
	movq	-144(%rbp), %rsi
.L1522:
	movq	-136(%rbp), %rax
	movq	%r13, %rdi
.LEHB245:
	call	*%rax
	testb	%al, %al
	je	.L1526
	leaq	-128(%rbp), %rdi
	leaq	.LC76(%rip), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE6assignEPKc@PLT
	movl	$8, %ebx
	jmp	.L1478
	.p2align 4,,10
	.p2align 3
.L1577:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_18TargetArchitectureEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rax
	movq	-120(%rbp), %rdx
	leaq	-128(%rbp), %rdi
	xorl	%esi, %esi
	movl	$18, %r8d
	leaq	.LC64(%rip), %rcx
	movslq	4(%rax), %rbx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L1478
	.p2align 4,,10
	.p2align 3
.L1578:
	movq	-120(%rbp), %rdx
	leaq	-128(%rbp), %rdi
	movl	$1, %r8d
	xorl	%esi, %esi
	leaq	.LC65(%rip), %rcx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	xorl	%ebx, %ebx
	jmp	.L1478
	.p2align 4,,10
	.p2align 3
.L1559:
	movq	-120(%rbp), %rdx
	leaq	-128(%rbp), %rdi
	movl	$10, %r8d
	xorl	%esi, %esi
	leaq	.LC67(%rip), %rcx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movl	$1, %ebx
	jmp	.L1478
	.p2align 4,,10
	.p2align 3
.L1561:
	leaq	-128(%rbp), %rdi
	leaq	.LC70(%rip), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE6assignEPKc@PLT
	movl	$2, %ebx
	jmp	.L1478
.L1563:
	leaq	-128(%rbp), %rdi
	leaq	.LC73(%rip), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE6assignEPKc@PLT
	movl	$4, %ebx
	jmp	.L1478
.L1526:
	movq	0(%r13), %rax
	movq	16(%rax), %rax
	movq	%rax, -136(%rbp)
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_10TypeOracleEEERPNT_5ScopeEv@PLT
	leaq	.LC77(%rip), %rsi
	movq	%r14, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
.LEHE245:
	movq	%r14, %rdi
.LEHB246:
	call	_ZN2v88internal6torque12Declarations16LookupGlobalTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
.LEHE246:
	movq	-96(%rbp), %rdi
	movq	%rax, %rsi
	cmpq	%rbx, %rdi
	je	.L1527
	movq	%rax, -144(%rbp)
	call	_ZdlPv@PLT
	movq	-144(%rbp), %rsi
.L1527:
	movq	-136(%rbp), %rax
	movq	%r13, %rdi
.LEHB247:
	call	*%rax
	testb	%al, %al
	jne	.L1568
	movq	0(%r13), %rax
	movq	16(%rax), %rax
	movq	%rax, -136(%rbp)
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_10TypeOracleEEERPNT_5ScopeEv@PLT
	leaq	.LC79(%rip), %rsi
	movq	%r14, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
.LEHE247:
	movq	%r14, %rdi
.LEHB248:
	call	_ZN2v88internal6torque12Declarations16LookupGlobalTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
.LEHE248:
	movq	-96(%rbp), %rdi
	movq	%rax, %r14
	cmpq	%rbx, %rdi
	je	.L1532
	call	_ZdlPv@PLT
.L1532:
	movq	-136(%rbp), %rax
	movq	%r14, %rsi
	movq	%r13, %rdi
.LEHB249:
	call	*%rax
	testb	%al, %al
	je	.L1579
.L1568:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_18TargetArchitectureEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rax
	leaq	-128(%rbp), %rdi
	leaq	.LC78(%rip), %rsi
	movslq	4(%rax), %rbx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE6assignEPKc@PLT
	jmp	.L1478
.L1576:
	call	__stack_chk_fail@PLT
.L1579:
	leaq	.LC80(%rip), %rdx
	movq	%r13, %rsi
	leaq	.LC81(%rip), %rdi
	call	_ZN2v88internal6torque11ReportErrorIJRA16_KcRKNS1_4TypeERA25_S3_EEEvDpOT_
.LEHE249:
.L1553:
	endbr64
	movq	%rax, %r12
	jmp	.L1529
.L1550:
	endbr64
	movq	%rax, %r12
	jmp	.L1513
.L1552:
	endbr64
	movq	%rax, %r12
	jmp	.L1524
.L1549:
	endbr64
	movq	%rax, %r12
	jmp	.L1507
.L1551:
	endbr64
	movq	%rax, %r12
	jmp	.L1518
.L1554:
	endbr64
	movq	%rax, %r12
	jmp	.L1534
.L1548:
	endbr64
	movq	%rax, %r12
	jmp	.L1502
.L1544:
	endbr64
	movq	%rax, %r12
	jmp	.L1481
.L1545:
	endbr64
	movq	%rax, %r12
	jmp	.L1486
.L1547:
	endbr64
	movq	%rax, %r12
	jmp	.L1496
.L1546:
	endbr64
	movq	%rax, %r12
	jmp	.L1491
.L1543:
	endbr64
	movq	%rax, %r12
	jmp	.L1474
.L1542:
	endbr64
	movq	%rax, %r12
	jmp	.L1476
	.section	.gcc_except_table._ZNK2v88internal6torque5Field23GetFieldSizeInformationB5cxx11Ev,"a",@progbits
.LLSDA6635:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6635-.LLSDACSB6635
.LLSDACSB6635:
	.uleb128 .LEHB225-.LFB6635
	.uleb128 .LEHE225-.LEHB225
	.uleb128 .L1542-.LFB6635
	.uleb128 0
	.uleb128 .LEHB226-.LFB6635
	.uleb128 .LEHE226-.LEHB226
	.uleb128 .L1543-.LFB6635
	.uleb128 0
	.uleb128 .LEHB227-.LFB6635
	.uleb128 .LEHE227-.LEHB227
	.uleb128 .L1542-.LFB6635
	.uleb128 0
	.uleb128 .LEHB228-.LFB6635
	.uleb128 .LEHE228-.LEHB228
	.uleb128 .L1544-.LFB6635
	.uleb128 0
	.uleb128 .LEHB229-.LFB6635
	.uleb128 .LEHE229-.LEHB229
	.uleb128 .L1542-.LFB6635
	.uleb128 0
	.uleb128 .LEHB230-.LFB6635
	.uleb128 .LEHE230-.LEHB230
	.uleb128 .L1545-.LFB6635
	.uleb128 0
	.uleb128 .LEHB231-.LFB6635
	.uleb128 .LEHE231-.LEHB231
	.uleb128 .L1542-.LFB6635
	.uleb128 0
	.uleb128 .LEHB232-.LFB6635
	.uleb128 .LEHE232-.LEHB232
	.uleb128 .L1546-.LFB6635
	.uleb128 0
	.uleb128 .LEHB233-.LFB6635
	.uleb128 .LEHE233-.LEHB233
	.uleb128 .L1542-.LFB6635
	.uleb128 0
	.uleb128 .LEHB234-.LFB6635
	.uleb128 .LEHE234-.LEHB234
	.uleb128 .L1547-.LFB6635
	.uleb128 0
	.uleb128 .LEHB235-.LFB6635
	.uleb128 .LEHE235-.LEHB235
	.uleb128 .L1542-.LFB6635
	.uleb128 0
	.uleb128 .LEHB236-.LFB6635
	.uleb128 .LEHE236-.LEHB236
	.uleb128 .L1548-.LFB6635
	.uleb128 0
	.uleb128 .LEHB237-.LFB6635
	.uleb128 .LEHE237-.LEHB237
	.uleb128 .L1542-.LFB6635
	.uleb128 0
	.uleb128 .LEHB238-.LFB6635
	.uleb128 .LEHE238-.LEHB238
	.uleb128 .L1549-.LFB6635
	.uleb128 0
	.uleb128 .LEHB239-.LFB6635
	.uleb128 .LEHE239-.LEHB239
	.uleb128 .L1542-.LFB6635
	.uleb128 0
	.uleb128 .LEHB240-.LFB6635
	.uleb128 .LEHE240-.LEHB240
	.uleb128 .L1550-.LFB6635
	.uleb128 0
	.uleb128 .LEHB241-.LFB6635
	.uleb128 .LEHE241-.LEHB241
	.uleb128 .L1542-.LFB6635
	.uleb128 0
	.uleb128 .LEHB242-.LFB6635
	.uleb128 .LEHE242-.LEHB242
	.uleb128 .L1551-.LFB6635
	.uleb128 0
	.uleb128 .LEHB243-.LFB6635
	.uleb128 .LEHE243-.LEHB243
	.uleb128 .L1542-.LFB6635
	.uleb128 0
	.uleb128 .LEHB244-.LFB6635
	.uleb128 .LEHE244-.LEHB244
	.uleb128 .L1552-.LFB6635
	.uleb128 0
	.uleb128 .LEHB245-.LFB6635
	.uleb128 .LEHE245-.LEHB245
	.uleb128 .L1542-.LFB6635
	.uleb128 0
	.uleb128 .LEHB246-.LFB6635
	.uleb128 .LEHE246-.LEHB246
	.uleb128 .L1553-.LFB6635
	.uleb128 0
	.uleb128 .LEHB247-.LFB6635
	.uleb128 .LEHE247-.LEHB247
	.uleb128 .L1542-.LFB6635
	.uleb128 0
	.uleb128 .LEHB248-.LFB6635
	.uleb128 .LEHE248-.LEHB248
	.uleb128 .L1554-.LFB6635
	.uleb128 0
	.uleb128 .LEHB249-.LFB6635
	.uleb128 .LEHE249-.LEHB249
	.uleb128 .L1542-.LFB6635
	.uleb128 0
.LLSDACSE6635:
	.section	.text._ZNK2v88internal6torque5Field23GetFieldSizeInformationB5cxx11Ev
	.cfi_endproc
	.section	.text.unlikely._ZNK2v88internal6torque5Field23GetFieldSizeInformationB5cxx11Ev
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6635
	.type	_ZNK2v88internal6torque5Field23GetFieldSizeInformationB5cxx11Ev.cold, @function
_ZNK2v88internal6torque5Field23GetFieldSizeInformationB5cxx11Ev.cold:
.LFSB6635:
.L1529:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1476
	call	_ZdlPv@PLT
	jmp	.L1476
.L1513:
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1476
	call	_ZdlPv@PLT
.L1476:
	movq	-128(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L1538
	call	_ZdlPv@PLT
.L1538:
	movq	%r12, %rdi
.LEHB250:
	call	_Unwind_Resume@PLT
.LEHE250:
.L1507:
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1476
	call	_ZdlPv@PLT
	jmp	.L1476
.L1524:
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1476
	call	_ZdlPv@PLT
	jmp	.L1476
.L1518:
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1476
	call	_ZdlPv@PLT
	jmp	.L1476
.L1534:
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1476
	call	_ZdlPv@PLT
	jmp	.L1476
.L1502:
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1476
	call	_ZdlPv@PLT
	jmp	.L1476
.L1481:
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1476
	call	_ZdlPv@PLT
	jmp	.L1476
.L1486:
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1476
	call	_ZdlPv@PLT
	jmp	.L1476
.L1496:
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1476
	call	_ZdlPv@PLT
	jmp	.L1476
.L1491:
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1476
	call	_ZdlPv@PLT
	jmp	.L1476
.L1474:
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1476
	call	_ZdlPv@PLT
	jmp	.L1476
	.cfi_endproc
.LFE6635:
	.section	.gcc_except_table._ZNK2v88internal6torque5Field23GetFieldSizeInformationB5cxx11Ev
.LLSDAC6635:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6635-.LLSDACSBC6635
.LLSDACSBC6635:
	.uleb128 .LEHB250-.LCOLDB82
	.uleb128 .LEHE250-.LEHB250
	.uleb128 0
	.uleb128 0
.LLSDACSEC6635:
	.section	.text.unlikely._ZNK2v88internal6torque5Field23GetFieldSizeInformationB5cxx11Ev
	.section	.text._ZNK2v88internal6torque5Field23GetFieldSizeInformationB5cxx11Ev
	.size	_ZNK2v88internal6torque5Field23GetFieldSizeInformationB5cxx11Ev, .-_ZNK2v88internal6torque5Field23GetFieldSizeInformationB5cxx11Ev
	.section	.text.unlikely._ZNK2v88internal6torque5Field23GetFieldSizeInformationB5cxx11Ev
	.size	_ZNK2v88internal6torque5Field23GetFieldSizeInformationB5cxx11Ev.cold, .-_ZNK2v88internal6torque5Field23GetFieldSizeInformationB5cxx11Ev.cold
.LCOLDE82:
	.section	.text._ZNK2v88internal6torque5Field23GetFieldSizeInformationB5cxx11Ev
.LHOTE82:
	.section	.text._ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E,"axG",@progbits,_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	.type	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E, @function
_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E:
.LFB8469:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L1595
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
.L1584:
	movq	24(%r12), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r12), %rdi
	leaq	48(%r12), %rax
	movq	16(%r12), %rbx
	cmpq	%rax, %rdi
	je	.L1582
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L1580
.L1583:
	movq	%rbx, %r12
	jmp	.L1584
	.p2align 4,,10
	.p2align 3
.L1582:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L1583
.L1580:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1595:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.cfi_endproc
.LFE8469:
	.size	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E, .-_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	.section	.text._ZN2v88internal6torque12AbstractTypeD0Ev,"axG",@progbits,_ZN2v88internal6torque12AbstractTypeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque12AbstractTypeD0Ev
	.type	_ZN2v88internal6torque12AbstractTypeD0Ev, @function
_ZN2v88internal6torque12AbstractTypeD0Ev:
.LFB8799:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal6torque12AbstractTypeE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rax, (%rdi)
	movq	112(%rdi), %rdi
	leaq	128(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1599
	call	_ZdlPv@PLT
.L1599:
	movq	80(%r12), %rdi
	leaq	96(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1600
	call	_ZdlPv@PLT
.L1600:
	movq	40(%r12), %r13
	leaq	16+_ZTVN2v88internal6torque4TypeE(%rip), %rax
	leaq	24(%r12), %r14
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L1601
.L1604:
	movq	24(%r13), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r13), %rdi
	leaq	48(%r13), %rax
	movq	16(%r13), %rbx
	cmpq	%rax, %rdi
	je	.L1602
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L1601
.L1603:
	movq	%rbx, %r13
	jmp	.L1604
	.p2align 4,,10
	.p2align 3
.L1602:
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L1603
.L1601:
	popq	%rbx
	movq	%r12, %rdi
	movl	$160, %esi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE8799:
	.size	_ZN2v88internal6torque12AbstractTypeD0Ev, .-_ZN2v88internal6torque12AbstractTypeD0Ev
	.section	.text._ZN2v88internal6torque10StructTypeD0Ev,"axG",@progbits,_ZN2v88internal6torque10StructTypeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque10StructTypeD0Ev
	.type	_ZN2v88internal6torque10StructTypeD0Ev, @function
_ZN2v88internal6torque10StructTypeD0Ev:
.LFB8821:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal6torque10StructTypeE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	cmpb	$0, 176(%rdi)
	movq	%rax, (%rdi)
	je	.L1616
	movq	192(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L1616
	call	_ZdlPv@PLT
.L1616:
	movq	144(%r12), %rdi
	leaq	16+_ZTVN2v88internal6torque13AggregateTypeE(%rip), %rax
	movq	%rax, (%r12)
	testq	%rdi, %rdi
	je	.L1617
	call	_ZdlPv@PLT
.L1617:
	movq	112(%r12), %rdi
	leaq	128(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1618
	call	_ZdlPv@PLT
.L1618:
	movq	88(%r12), %rbx
	movq	80(%r12), %r13
	cmpq	%r13, %rbx
	je	.L1619
	.p2align 4,,10
	.p2align 3
.L1623:
	movq	48(%r13), %rdi
	leaq	64(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1620
	call	_ZdlPv@PLT
	addq	$104, %r13
	cmpq	%r13, %rbx
	jne	.L1623
.L1621:
	movq	80(%r12), %r13
.L1619:
	testq	%r13, %r13
	je	.L1624
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1624:
	movq	40(%r12), %r13
	leaq	16+_ZTVN2v88internal6torque4TypeE(%rip), %rax
	leaq	24(%r12), %r14
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L1627
.L1625:
	movq	24(%r13), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r13), %rdi
	leaq	48(%r13), %rax
	movq	16(%r13), %rbx
	cmpq	%rax, %rdi
	je	.L1626
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L1627
.L1628:
	movq	%rbx, %r13
	jmp	.L1625
	.p2align 4,,10
	.p2align 3
.L1620:
	addq	$104, %r13
	cmpq	%r13, %rbx
	jne	.L1623
	jmp	.L1621
	.p2align 4,,10
	.p2align 3
.L1626:
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L1628
.L1627:
	popq	%rbx
	movq	%r12, %rdi
	movl	$216, %esi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE8821:
	.size	_ZN2v88internal6torque10StructTypeD0Ev, .-_ZN2v88internal6torque10StructTypeD0Ev
	.section	.text._ZN2v88internal6torque18BuiltinPointerTypeD0Ev,"axG",@progbits,_ZN2v88internal6torque18BuiltinPointerTypeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque18BuiltinPointerTypeD0Ev
	.type	_ZN2v88internal6torque18BuiltinPointerTypeD0Ev, @function
_ZN2v88internal6torque18BuiltinPointerTypeD0Ev:
.LFB12341:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal6torque18BuiltinPointerTypeE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rax, (%rdi)
	movq	72(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L1647
	call	_ZdlPv@PLT
.L1647:
	movq	40(%r12), %r13
	leaq	16+_ZTVN2v88internal6torque4TypeE(%rip), %rax
	leaq	24(%r12), %r14
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L1648
.L1651:
	movq	24(%r13), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r13), %rdi
	leaq	48(%r13), %rax
	movq	16(%r13), %rbx
	cmpq	%rax, %rdi
	je	.L1649
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L1648
.L1650:
	movq	%rbx, %r13
	jmp	.L1651
	.p2align 4,,10
	.p2align 3
.L1649:
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L1650
.L1648:
	popq	%rbx
	movq	%r12, %rdi
	movl	$112, %esi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE12341:
	.size	_ZN2v88internal6torque18BuiltinPointerTypeD0Ev, .-_ZN2v88internal6torque18BuiltinPointerTypeD0Ev
	.section	.text._ZN2v88internal6torque9ClassTypeD0Ev,"axG",@progbits,_ZN2v88internal6torque9ClassTypeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque9ClassTypeD0Ev
	.type	_ZN2v88internal6torque9ClassTypeD0Ev, @function
_ZN2v88internal6torque9ClassTypeD0Ev:
.LFB8855:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal6torque9ClassTypeE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rax, (%rdi)
	movq	184(%rdi), %rdi
	leaq	200(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1666
	call	_ZdlPv@PLT
.L1666:
	movq	144(%r12), %rdi
	leaq	16+_ZTVN2v88internal6torque13AggregateTypeE(%rip), %rax
	movq	%rax, (%r12)
	testq	%rdi, %rdi
	je	.L1667
	call	_ZdlPv@PLT
.L1667:
	movq	112(%r12), %rdi
	leaq	128(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1668
	call	_ZdlPv@PLT
.L1668:
	movq	88(%r12), %rbx
	movq	80(%r12), %r13
	cmpq	%r13, %rbx
	je	.L1669
	.p2align 4,,10
	.p2align 3
.L1673:
	movq	48(%r13), %rdi
	leaq	64(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1670
	call	_ZdlPv@PLT
	addq	$104, %r13
	cmpq	%r13, %rbx
	jne	.L1673
.L1671:
	movq	80(%r12), %r13
.L1669:
	testq	%r13, %r13
	je	.L1674
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1674:
	movq	40(%r12), %r13
	leaq	16+_ZTVN2v88internal6torque4TypeE(%rip), %rax
	leaq	24(%r12), %r14
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L1677
.L1675:
	movq	24(%r13), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r13), %rdi
	leaq	48(%r13), %rax
	movq	16(%r13), %rbx
	cmpq	%rax, %rdi
	je	.L1676
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L1677
.L1678:
	movq	%rbx, %r13
	jmp	.L1675
	.p2align 4,,10
	.p2align 3
.L1670:
	addq	$104, %r13
	cmpq	%r13, %rbx
	jne	.L1673
	jmp	.L1671
	.p2align 4,,10
	.p2align 3
.L1676:
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L1678
.L1677:
	popq	%rbx
	movq	%r12, %rdi
	movl	$232, %esi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE8855:
	.size	_ZN2v88internal6torque9ClassTypeD0Ev, .-_ZN2v88internal6torque9ClassTypeD0Ev
	.section	.text._ZN2v88internal6torque13AggregateTypeD0Ev,"axG",@progbits,_ZN2v88internal6torque13AggregateTypeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque13AggregateTypeD0Ev
	.type	_ZN2v88internal6torque13AggregateTypeD0Ev, @function
_ZN2v88internal6torque13AggregateTypeD0Ev:
.LFB5777:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal6torque13AggregateTypeE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rax, (%rdi)
	movq	144(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L1694
	call	_ZdlPv@PLT
.L1694:
	movq	112(%r12), %rdi
	leaq	128(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1695
	call	_ZdlPv@PLT
.L1695:
	movq	88(%r12), %rbx
	movq	80(%r12), %r13
	cmpq	%r13, %rbx
	je	.L1696
	.p2align 4,,10
	.p2align 3
.L1700:
	movq	48(%r13), %rdi
	leaq	64(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1697
	call	_ZdlPv@PLT
	addq	$104, %r13
	cmpq	%r13, %rbx
	jne	.L1700
.L1698:
	movq	80(%r12), %r13
.L1696:
	testq	%r13, %r13
	je	.L1701
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1701:
	movq	40(%r12), %r13
	leaq	16+_ZTVN2v88internal6torque4TypeE(%rip), %rax
	leaq	24(%r12), %r14
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L1704
.L1702:
	movq	24(%r13), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r13), %rdi
	leaq	48(%r13), %rax
	movq	16(%r13), %rbx
	cmpq	%rax, %rdi
	je	.L1703
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L1704
.L1705:
	movq	%rbx, %r13
	jmp	.L1702
	.p2align 4,,10
	.p2align 3
.L1697:
	addq	$104, %r13
	cmpq	%r13, %rbx
	jne	.L1700
	jmp	.L1698
	.p2align 4,,10
	.p2align 3
.L1703:
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L1705
.L1704:
	popq	%rbx
	movq	%r12, %rdi
	movl	$168, %esi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE5777:
	.size	_ZN2v88internal6torque13AggregateTypeD0Ev, .-_ZN2v88internal6torque13AggregateTypeD0Ev
	.section	.text._ZN2v88internal6torque9ClassTypeD2Ev,"axG",@progbits,_ZN2v88internal6torque9ClassTypeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque9ClassTypeD2Ev
	.type	_ZN2v88internal6torque9ClassTypeD2Ev, @function
_ZN2v88internal6torque9ClassTypeD2Ev:
.LFB8853:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal6torque9ClassTypeE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	184(%rdi), %rdi
	leaq	200(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1721
	call	_ZdlPv@PLT
.L1721:
	movq	144(%rbx), %rdi
	leaq	16+_ZTVN2v88internal6torque13AggregateTypeE(%rip), %rax
	movq	%rax, (%rbx)
	testq	%rdi, %rdi
	je	.L1722
	call	_ZdlPv@PLT
.L1722:
	movq	112(%rbx), %rdi
	leaq	128(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1723
	call	_ZdlPv@PLT
.L1723:
	movq	88(%rbx), %r13
	movq	80(%rbx), %r12
	cmpq	%r12, %r13
	je	.L1724
	.p2align 4,,10
	.p2align 3
.L1728:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1725
	call	_ZdlPv@PLT
	addq	$104, %r12
	cmpq	%r12, %r13
	jne	.L1728
.L1726:
	movq	80(%rbx), %r12
.L1724:
	testq	%r12, %r12
	je	.L1729
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1729:
	movq	40(%rbx), %r12
	leaq	16+_ZTVN2v88internal6torque4TypeE(%rip), %rax
	leaq	24(%rbx), %r13
	movq	%rax, (%rbx)
	testq	%r12, %r12
	je	.L1720
.L1730:
	movq	24(%r12), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r12), %rdi
	leaq	48(%r12), %rax
	movq	16(%r12), %rbx
	cmpq	%rax, %rdi
	je	.L1731
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L1720
.L1733:
	movq	%rbx, %r12
	jmp	.L1730
	.p2align 4,,10
	.p2align 3
.L1725:
	addq	$104, %r12
	cmpq	%r12, %r13
	jne	.L1728
	jmp	.L1726
	.p2align 4,,10
	.p2align 3
.L1731:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L1733
.L1720:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8853:
	.size	_ZN2v88internal6torque9ClassTypeD2Ev, .-_ZN2v88internal6torque9ClassTypeD2Ev
	.weak	_ZN2v88internal6torque9ClassTypeD1Ev
	.set	_ZN2v88internal6torque9ClassTypeD1Ev,_ZN2v88internal6torque9ClassTypeD2Ev
	.section	.text._ZN2v88internal6torque10StructTypeD2Ev,"axG",@progbits,_ZN2v88internal6torque10StructTypeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque10StructTypeD2Ev
	.type	_ZN2v88internal6torque10StructTypeD2Ev, @function
_ZN2v88internal6torque10StructTypeD2Ev:
.LFB8819:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal6torque10StructTypeE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpb	$0, 176(%rdi)
	movq	%rax, (%rdi)
	je	.L1749
	movq	192(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L1749
	call	_ZdlPv@PLT
.L1749:
	movq	144(%rbx), %rdi
	leaq	16+_ZTVN2v88internal6torque13AggregateTypeE(%rip), %rax
	movq	%rax, (%rbx)
	testq	%rdi, %rdi
	je	.L1750
	call	_ZdlPv@PLT
.L1750:
	movq	112(%rbx), %rdi
	leaq	128(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1751
	call	_ZdlPv@PLT
.L1751:
	movq	88(%rbx), %r13
	movq	80(%rbx), %r12
	cmpq	%r12, %r13
	je	.L1752
	.p2align 4,,10
	.p2align 3
.L1756:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1753
	call	_ZdlPv@PLT
	addq	$104, %r12
	cmpq	%r12, %r13
	jne	.L1756
.L1754:
	movq	80(%rbx), %r12
.L1752:
	testq	%r12, %r12
	je	.L1757
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1757:
	movq	40(%rbx), %r12
	leaq	16+_ZTVN2v88internal6torque4TypeE(%rip), %rax
	leaq	24(%rbx), %r13
	movq	%rax, (%rbx)
	testq	%r12, %r12
	je	.L1748
.L1758:
	movq	24(%r12), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r12), %rdi
	leaq	48(%r12), %rax
	movq	16(%r12), %rbx
	cmpq	%rax, %rdi
	je	.L1759
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L1748
.L1761:
	movq	%rbx, %r12
	jmp	.L1758
	.p2align 4,,10
	.p2align 3
.L1753:
	addq	$104, %r12
	cmpq	%r12, %r13
	jne	.L1756
	jmp	.L1754
	.p2align 4,,10
	.p2align 3
.L1759:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L1761
.L1748:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8819:
	.size	_ZN2v88internal6torque10StructTypeD2Ev, .-_ZN2v88internal6torque10StructTypeD2Ev
	.weak	_ZN2v88internal6torque10StructTypeD1Ev
	.set	_ZN2v88internal6torque10StructTypeD1Ev,_ZN2v88internal6torque10StructTypeD2Ev
	.section	.text._ZN2v88internal6torque18BuiltinPointerTypeD2Ev,"axG",@progbits,_ZN2v88internal6torque18BuiltinPointerTypeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque18BuiltinPointerTypeD2Ev
	.type	_ZN2v88internal6torque18BuiltinPointerTypeD2Ev, @function
_ZN2v88internal6torque18BuiltinPointerTypeD2Ev:
.LFB12339:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal6torque18BuiltinPointerTypeE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	%rax, (%rdi)
	movq	72(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L1780
	call	_ZdlPv@PLT
.L1780:
	leaq	16+_ZTVN2v88internal6torque4TypeE(%rip), %rax
	leaq	24(%r12), %r13
	movq	%rax, (%r12)
	movq	40(%r12), %r12
	testq	%r12, %r12
	je	.L1779
.L1784:
	movq	24(%r12), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r12), %rdi
	leaq	48(%r12), %rax
	movq	16(%r12), %rbx
	cmpq	%rax, %rdi
	je	.L1782
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L1779
.L1783:
	movq	%rbx, %r12
	jmp	.L1784
	.p2align 4,,10
	.p2align 3
.L1782:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L1783
.L1779:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE12339:
	.size	_ZN2v88internal6torque18BuiltinPointerTypeD2Ev, .-_ZN2v88internal6torque18BuiltinPointerTypeD2Ev
	.weak	_ZN2v88internal6torque18BuiltinPointerTypeD1Ev
	.set	_ZN2v88internal6torque18BuiltinPointerTypeD1Ev,_ZN2v88internal6torque18BuiltinPointerTypeD2Ev
	.section	.text._ZN2v88internal6torque12AbstractTypeD2Ev,"axG",@progbits,_ZN2v88internal6torque12AbstractTypeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque12AbstractTypeD2Ev
	.type	_ZN2v88internal6torque12AbstractTypeD2Ev, @function
_ZN2v88internal6torque12AbstractTypeD2Ev:
.LFB8797:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal6torque12AbstractTypeE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	112(%rdi), %rdi
	leaq	128(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1799
	call	_ZdlPv@PLT
.L1799:
	movq	80(%rbx), %rdi
	leaq	96(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1800
	call	_ZdlPv@PLT
.L1800:
	movq	40(%rbx), %r12
	leaq	16+_ZTVN2v88internal6torque4TypeE(%rip), %rax
	leaq	24(%rbx), %r13
	movq	%rax, (%rbx)
	testq	%r12, %r12
	je	.L1798
.L1804:
	movq	24(%r12), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r12), %rdi
	leaq	48(%r12), %rax
	movq	16(%r12), %rbx
	cmpq	%rax, %rdi
	je	.L1802
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L1798
.L1803:
	movq	%rbx, %r12
	jmp	.L1804
	.p2align 4,,10
	.p2align 3
.L1802:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L1803
.L1798:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8797:
	.size	_ZN2v88internal6torque12AbstractTypeD2Ev, .-_ZN2v88internal6torque12AbstractTypeD2Ev
	.weak	_ZN2v88internal6torque12AbstractTypeD1Ev
	.set	_ZN2v88internal6torque12AbstractTypeD1Ev,_ZN2v88internal6torque12AbstractTypeD2Ev
	.section	.text._ZN2v88internal6torque13AggregateTypeD2Ev,"axG",@progbits,_ZN2v88internal6torque13AggregateTypeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque13AggregateTypeD2Ev
	.type	_ZN2v88internal6torque13AggregateTypeD2Ev, @function
_ZN2v88internal6torque13AggregateTypeD2Ev:
.LFB5775:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal6torque13AggregateTypeE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	144(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L1816
	call	_ZdlPv@PLT
.L1816:
	movq	112(%rbx), %rdi
	leaq	128(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1817
	call	_ZdlPv@PLT
.L1817:
	movq	88(%rbx), %r13
	movq	80(%rbx), %r12
	cmpq	%r12, %r13
	je	.L1818
	.p2align 4,,10
	.p2align 3
.L1822:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1819
	call	_ZdlPv@PLT
	addq	$104, %r12
	cmpq	%r12, %r13
	jne	.L1822
.L1820:
	movq	80(%rbx), %r12
.L1818:
	testq	%r12, %r12
	je	.L1823
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1823:
	movq	40(%rbx), %r12
	leaq	16+_ZTVN2v88internal6torque4TypeE(%rip), %rax
	leaq	24(%rbx), %r13
	movq	%rax, (%rbx)
	testq	%r12, %r12
	je	.L1815
.L1824:
	movq	24(%r12), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r12), %rdi
	leaq	48(%r12), %rax
	movq	16(%r12), %rbx
	cmpq	%rax, %rdi
	je	.L1825
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L1815
.L1827:
	movq	%rbx, %r12
	jmp	.L1824
	.p2align 4,,10
	.p2align 3
.L1819:
	addq	$104, %r12
	cmpq	%r12, %r13
	jne	.L1822
	jmp	.L1820
	.p2align 4,,10
	.p2align 3
.L1825:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L1827
.L1815:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5775:
	.size	_ZN2v88internal6torque13AggregateTypeD2Ev, .-_ZN2v88internal6torque13AggregateTypeD2Ev
	.weak	_ZN2v88internal6torque13AggregateTypeD1Ev
	.set	_ZN2v88internal6torque13AggregateTypeD1Ev,_ZN2v88internal6torque13AggregateTypeD2Ev
	.section	.text.unlikely._ZN2v88internal6torque9ClassTypeC2EPKNS1_4TypeEPNS1_9NamespaceERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS_4base5FlagsINS1_9ClassFlagEiEESF_PKNS1_16ClassDeclarationEPKNS1_9TypeAliasE,"ax",@progbits
	.align 2
.LCOLDB83:
	.section	.text._ZN2v88internal6torque9ClassTypeC2EPKNS1_4TypeEPNS1_9NamespaceERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS_4base5FlagsINS1_9ClassFlagEiEESF_PKNS1_16ClassDeclarationEPKNS1_9TypeAliasE,"ax",@progbits
.LHOTB83:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque9ClassTypeC2EPKNS1_4TypeEPNS1_9NamespaceERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS_4base5FlagsINS1_9ClassFlagEiEESF_PKNS1_16ClassDeclarationEPKNS1_9TypeAliasE
	.type	_ZN2v88internal6torque9ClassTypeC2EPKNS1_4TypeEPNS1_9NamespaceERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS_4base5FlagsINS1_9ClassFlagEiEESF_PKNS1_16ClassDeclarationEPKNS1_9TypeAliasE, @function
_ZN2v88internal6torque9ClassTypeC2EPKNS1_4TypeEPNS1_9NamespaceERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS_4base5FlagsINS1_9ClassFlagEiEESF_PKNS1_16ClassDeclarationEPKNS1_9TypeAliasE:
.LFB6586:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6586
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	32(%rdi), %rax
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%r9, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	leaq	112(%rdi), %rdi
	pushq	%rbx
	.cfi_offset 3, -40
	movl	%r8d, %ebx
	subq	$8, %rsp
	movq	%rax, -64(%rdi)
	movq	%rax, -56(%rdi)
	leaq	16+_ZTVN2v88internal6torque13AggregateTypeE(%rip), %rax
	movq	%rax, -112(%rdi)
	leaq	128(%r12), %rax
	movl	$5, -104(%rdi)
	movq	%rsi, -96(%rdi)
	movl	$0, -80(%rdi)
	movq	$0, -72(%rdi)
	movq	$0, -48(%rdi)
	movb	$0, -40(%rdi)
	movups	%xmm0, -32(%rdi)
	movq	$0, -16(%rdi)
	movq	%rdx, -8(%rdi)
	movq	%rax, 112(%r12)
	movq	(%rcx), %rsi
	movq	8(%rcx), %rdx
	addq	%rsi, %rdx
.LEHB251:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag.constprop.0
.LEHE251:
	leaq	16+_ZTVN2v88internal6torque9ClassTypeE(%rip), %rax
	pxor	%xmm0, %xmm0
	andb	$-2, %bh
	movq	$0, 160(%r12)
	movq	%rax, (%r12)
	leaq	200(%r12), %rax
	leaq	184(%r12), %rdi
	movq	$0, 168(%r12)
	movl	%ebx, 176(%r12)
	movq	%rax, 184(%r12)
	movups	%xmm0, 144(%r12)
	movq	0(%r13), %rsi
	movq	8(%r13), %rdx
	addq	%rsi, %rdx
.LEHB252:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag.constprop.0
.LEHE252:
	movq	16(%rbp), %xmm0
	movhps	24(%rbp), %xmm0
	movups	%xmm0, 216(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1847:
	.cfi_restore_state
	endbr64
	movq	%rax, %r13
	jmp	.L1844
.L1846:
	endbr64
	movq	%rax, %r13
	jmp	.L1845
	.section	.gcc_except_table._ZN2v88internal6torque9ClassTypeC2EPKNS1_4TypeEPNS1_9NamespaceERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS_4base5FlagsINS1_9ClassFlagEiEESF_PKNS1_16ClassDeclarationEPKNS1_9TypeAliasE,"a",@progbits
.LLSDA6586:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6586-.LLSDACSB6586
.LLSDACSB6586:
	.uleb128 .LEHB251-.LFB6586
	.uleb128 .LEHE251-.LEHB251
	.uleb128 .L1847-.LFB6586
	.uleb128 0
	.uleb128 .LEHB252-.LFB6586
	.uleb128 .LEHE252-.LEHB252
	.uleb128 .L1846-.LFB6586
	.uleb128 0
.LLSDACSE6586:
	.section	.text._ZN2v88internal6torque9ClassTypeC2EPKNS1_4TypeEPNS1_9NamespaceERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS_4base5FlagsINS1_9ClassFlagEiEESF_PKNS1_16ClassDeclarationEPKNS1_9TypeAliasE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque9ClassTypeC2EPKNS1_4TypeEPNS1_9NamespaceERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS_4base5FlagsINS1_9ClassFlagEiEESF_PKNS1_16ClassDeclarationEPKNS1_9TypeAliasE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6586
	.type	_ZN2v88internal6torque9ClassTypeC2EPKNS1_4TypeEPNS1_9NamespaceERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS_4base5FlagsINS1_9ClassFlagEiEESF_PKNS1_16ClassDeclarationEPKNS1_9TypeAliasE.cold, @function
_ZN2v88internal6torque9ClassTypeC2EPKNS1_4TypeEPNS1_9NamespaceERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS_4base5FlagsINS1_9ClassFlagEiEESF_PKNS1_16ClassDeclarationEPKNS1_9TypeAliasE.cold:
.LFSB6586:
.L1844:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	leaq	80(%r12), %rdi
	call	_ZNSt6vectorIN2v88internal6torque5FieldESaIS3_EED1Ev
	movq	40(%r12), %rsi
	leaq	24(%r12), %rdi
	leaq	16+_ZTVN2v88internal6torque4TypeE(%rip), %rax
	movq	%rax, (%r12)
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	%r13, %rdi
.LEHB253:
	call	_Unwind_Resume@PLT
.L1845:
	movq	%r12, %rdi
	call	_ZN2v88internal6torque13AggregateTypeD2Ev
	movq	%r13, %rdi
	call	_Unwind_Resume@PLT
.LEHE253:
	.cfi_endproc
.LFE6586:
	.section	.gcc_except_table._ZN2v88internal6torque9ClassTypeC2EPKNS1_4TypeEPNS1_9NamespaceERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS_4base5FlagsINS1_9ClassFlagEiEESF_PKNS1_16ClassDeclarationEPKNS1_9TypeAliasE
.LLSDAC6586:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6586-.LLSDACSBC6586
.LLSDACSBC6586:
	.uleb128 .LEHB253-.LCOLDB83
	.uleb128 .LEHE253-.LEHB253
	.uleb128 0
	.uleb128 0
.LLSDACSEC6586:
	.section	.text.unlikely._ZN2v88internal6torque9ClassTypeC2EPKNS1_4TypeEPNS1_9NamespaceERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS_4base5FlagsINS1_9ClassFlagEiEESF_PKNS1_16ClassDeclarationEPKNS1_9TypeAliasE
	.section	.text._ZN2v88internal6torque9ClassTypeC2EPKNS1_4TypeEPNS1_9NamespaceERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS_4base5FlagsINS1_9ClassFlagEiEESF_PKNS1_16ClassDeclarationEPKNS1_9TypeAliasE
	.size	_ZN2v88internal6torque9ClassTypeC2EPKNS1_4TypeEPNS1_9NamespaceERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS_4base5FlagsINS1_9ClassFlagEiEESF_PKNS1_16ClassDeclarationEPKNS1_9TypeAliasE, .-_ZN2v88internal6torque9ClassTypeC2EPKNS1_4TypeEPNS1_9NamespaceERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS_4base5FlagsINS1_9ClassFlagEiEESF_PKNS1_16ClassDeclarationEPKNS1_9TypeAliasE
	.section	.text.unlikely._ZN2v88internal6torque9ClassTypeC2EPKNS1_4TypeEPNS1_9NamespaceERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS_4base5FlagsINS1_9ClassFlagEiEESF_PKNS1_16ClassDeclarationEPKNS1_9TypeAliasE
	.size	_ZN2v88internal6torque9ClassTypeC2EPKNS1_4TypeEPNS1_9NamespaceERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS_4base5FlagsINS1_9ClassFlagEiEESF_PKNS1_16ClassDeclarationEPKNS1_9TypeAliasE.cold, .-_ZN2v88internal6torque9ClassTypeC2EPKNS1_4TypeEPNS1_9NamespaceERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS_4base5FlagsINS1_9ClassFlagEiEESF_PKNS1_16ClassDeclarationEPKNS1_9TypeAliasE.cold
.LCOLDE83:
	.section	.text._ZN2v88internal6torque9ClassTypeC2EPKNS1_4TypeEPNS1_9NamespaceERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS_4base5FlagsINS1_9ClassFlagEiEESF_PKNS1_16ClassDeclarationEPKNS1_9TypeAliasE
.LHOTE83:
	.globl	_ZN2v88internal6torque9ClassTypeC1EPKNS1_4TypeEPNS1_9NamespaceERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS_4base5FlagsINS1_9ClassFlagEiEESF_PKNS1_16ClassDeclarationEPKNS1_9TypeAliasE
	.set	_ZN2v88internal6torque9ClassTypeC1EPKNS1_4TypeEPNS1_9NamespaceERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS_4base5FlagsINS1_9ClassFlagEiEESF_PKNS1_16ClassDeclarationEPKNS1_9TypeAliasE,_ZN2v88internal6torque9ClassTypeC2EPKNS1_4TypeEPNS1_9NamespaceERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS_4base5FlagsINS1_9ClassFlagEiEESF_PKNS1_16ClassDeclarationEPKNS1_9TypeAliasE
	.section	.text._ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E,"axG",@progbits,_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	.type	_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E, @function
_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E:
.LFB8542:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L1859
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
.L1853:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	%rbx, %rdi
	movq	16(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L1853
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1859:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE8542:
	.size	_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E, .-_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	.section	.text._ZN2v88internal6torque9UnionTypeD0Ev,"axG",@progbits,_ZN2v88internal6torque9UnionTypeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque9UnionTypeD0Ev
	.type	_ZN2v88internal6torque9UnionTypeD0Ev, @function
_ZN2v88internal6torque9UnionTypeD0Ev:
.LFB12337:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal6torque9UnionTypeE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	88(%rdi), %r13
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L1863
	leaq	72(%rdi), %rbx
.L1864:
	movq	24(%r13), %rsi
	movq	%rbx, %rdi
	call	_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	%r13, %rdi
	movq	16(%r13), %r13
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L1864
.L1863:
	movq	40(%r12), %r13
	leaq	16+_ZTVN2v88internal6torque4TypeE(%rip), %rax
	leaq	24(%r12), %r14
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L1865
.L1868:
	movq	24(%r13), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r13), %rdi
	leaq	48(%r13), %rax
	movq	16(%r13), %rbx
	cmpq	%rax, %rdi
	je	.L1866
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L1865
.L1867:
	movq	%rbx, %r13
	jmp	.L1868
	.p2align 4,,10
	.p2align 3
.L1866:
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L1867
.L1865:
	popq	%rbx
	movq	%r12, %rdi
	movl	$120, %esi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE12337:
	.size	_ZN2v88internal6torque9UnionTypeD0Ev, .-_ZN2v88internal6torque9UnionTypeD0Ev
	.section	.text._ZN2v88internal6torque9UnionTypeD2Ev,"axG",@progbits,_ZN2v88internal6torque9UnionTypeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque9UnionTypeD2Ev
	.type	_ZN2v88internal6torque9UnionTypeD2Ev, @function
_ZN2v88internal6torque9UnionTypeD2Ev:
.LFB12335:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal6torque9UnionTypeE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	88(%rdi), %rbx
	movq	%rax, (%rdi)
	testq	%rbx, %rbx
	je	.L1884
	leaq	72(%rdi), %r13
.L1885:
	movq	24(%rbx), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	%rbx, %rdi
	movq	16(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L1885
.L1884:
	leaq	16+_ZTVN2v88internal6torque4TypeE(%rip), %rax
	leaq	24(%r12), %r13
	movq	%rax, (%r12)
	movq	40(%r12), %r12
	testq	%r12, %r12
	je	.L1883
.L1889:
	movq	24(%r12), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r12), %rdi
	leaq	48(%r12), %rax
	movq	16(%r12), %rbx
	cmpq	%rax, %rdi
	je	.L1887
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L1883
.L1888:
	movq	%rbx, %r12
	jmp	.L1889
	.p2align 4,,10
	.p2align 3
.L1887:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L1888
.L1883:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE12335:
	.size	_ZN2v88internal6torque9UnionTypeD2Ev, .-_ZN2v88internal6torque9UnionTypeD2Ev
	.weak	_ZN2v88internal6torque9UnionTypeD1Ev
	.set	_ZN2v88internal6torque9UnionTypeD1Ev,_ZN2v88internal6torque9UnionTypeD2Ev
	.section	.rodata._ZNSt6vectorIN2v88internal6torque5FieldESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_.str1.1,"aMS",@progbits,1
.LC84:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorIN2v88internal6torque5FieldESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal6torque5FieldESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal6torque5FieldESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.type	_ZNSt6vectorIN2v88internal6torque5FieldESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, @function
_ZNSt6vectorIN2v88internal6torque5FieldESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_:
.LFB8578:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA8578
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	movabsq	$5675921253449092805, %rdx
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r14
	movq	(%rdi), %r15
	movq	%rdi, -72(%rbp)
	movq	%r14, %rax
	subq	%r15, %rax
	sarq	$3, %rax
	imulq	%rdx, %rax
	movabsq	$88686269585142075, %rdx
	cmpq	%rdx, %rax
	je	.L1937
	movq	%rsi, %r12
	movq	%rsi, %rbx
	subq	%r15, %r12
	testq	%rax, %rax
	je	.L1926
	leaq	(%rax,%rax), %rsi
	movq	%rsi, -64(%rbp)
	cmpq	%rsi, %rax
	jbe	.L1938
	movabsq	$9223372036854775800, %rax
	movq	%rax, -64(%rbp)
.L1906:
	movq	-64(%rbp), %rdi
.LEHB254:
	call	_Znwm@PLT
.LEHE254:
	movq	%rax, -56(%rbp)
.L1925:
	movl	16(%r13), %eax
	addq	-56(%rbp), %r12
	movdqu	0(%r13), %xmm7
	movq	48(%r13), %rsi
	leaq	48(%r12), %rdi
	movl	%eax, 16(%r12)
	movq	24(%r13), %rax
	movq	56(%r13), %rdx
	movups	%xmm7, (%r12)
	movdqu	32(%r13), %xmm7
	movq	%rax, 24(%r12)
	leaq	64(%r12), %rax
	movq	%rax, 48(%r12)
	addq	%rsi, %rdx
	movups	%xmm7, 32(%r12)
	movq	%rax, -80(%rbp)
.LEHB255:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag.constprop.0
.LEHE255:
	movq	88(%r13), %rdx
	movzbl	98(%r13), %eax
	movq	80(%r13), %rcx
	movq	%rdx, 88(%r12)
	movzwl	96(%r13), %edx
	movq	%rcx, 80(%r12)
	movw	%dx, 96(%r12)
	movb	%al, 98(%r12)
	cmpq	%r15, %rbx
	je	.L1928
	movq	-56(%rbp), %r13
	leaq	64(%r15), %r12
	jmp	.L1914
	.p2align 4,,10
	.p2align 3
.L1909:
	movq	%rax, 48(%r13)
	movq	(%r12), %rax
	movq	%rax, 64(%r13)
.L1910:
	movq	-8(%r12), %rax
	movq	%rax, 56(%r13)
	movq	16(%r12), %rax
	movq	%r12, -16(%r12)
	movq	$0, -8(%r12)
	movb	$0, (%r12)
	movq	%rax, 80(%r13)
	movq	24(%r12), %rax
	movq	%rax, 88(%r13)
	movzbl	32(%r12), %eax
	movb	%al, 96(%r13)
	movzbl	33(%r12), %eax
	movb	%al, 97(%r13)
	movzbl	34(%r12), %eax
	movb	%al, 98(%r13)
	movq	-16(%r12), %rdi
	cmpq	%r12, %rdi
	je	.L1911
	call	_ZdlPv@PLT
.L1911:
	leaq	104(%r12), %rax
	addq	$40, %r12
	addq	$104, %r13
	cmpq	%r12, %rbx
	je	.L1912
	movq	%rax, %r12
.L1914:
	movdqu	-64(%r12), %xmm1
	movups	%xmm1, 0(%r13)
	movl	-48(%r12), %eax
	movl	%eax, 16(%r13)
	movq	-40(%r12), %rax
	movq	%rax, 24(%r13)
	movdqu	-32(%r12), %xmm2
	leaq	64(%r13), %rax
	movq	%rax, 48(%r13)
	movups	%xmm2, 32(%r13)
	movq	-16(%r12), %rax
	cmpq	%r12, %rax
	jne	.L1909
	movdqu	(%r12), %xmm5
	movups	%xmm5, 64(%r13)
	jmp	.L1910
	.p2align 4,,10
	.p2align 3
.L1938:
	testq	%rsi, %rsi
	jne	.L1907
	movq	$0, -56(%rbp)
	jmp	.L1925
	.p2align 4,,10
	.p2align 3
.L1912:
	leaq	-104(%rbx), %rax
	movq	-56(%rbp), %rsi
	movabsq	$1064235235021704901, %rdx
	subq	%r15, %rax
	shrq	$3, %rax
	imulq	%rdx, %rax
	movabsq	$2305843009213693951, %rdx
	andq	%rdx, %rax
	addq	$1, %rax
	leaq	(%rax,%rax,2), %rdx
	leaq	(%rax,%rdx,4), %rax
	leaq	(%rsi,%rax,8), %r12
.L1908:
	addq	$104, %r12
	cmpq	%r14, %rbx
	je	.L1915
	movq	%rbx, %rax
	movq	%r12, %rdx
	jmp	.L1918
	.p2align 4,,10
	.p2align 3
.L1916:
	movq	%rcx, 48(%rdx)
	movq	64(%rax), %rcx
	movq	%rcx, 64(%rdx)
.L1917:
	movq	56(%rax), %rcx
	addq	$104, %rax
	addq	$104, %rdx
	movq	%rcx, -48(%rdx)
	movq	-24(%rax), %rcx
	movq	%rcx, -24(%rdx)
	movq	-16(%rax), %rcx
	movq	%rcx, -16(%rdx)
	movzbl	-8(%rax), %ecx
	movb	%cl, -8(%rdx)
	movzbl	-7(%rax), %ecx
	movb	%cl, -7(%rdx)
	movzbl	-6(%rax), %ecx
	movb	%cl, -6(%rdx)
	cmpq	%r14, %rax
	je	.L1939
.L1918:
	movl	16(%rax), %ecx
	movdqu	(%rax), %xmm3
	leaq	64(%rax), %rsi
	movdqu	32(%rax), %xmm4
	movl	%ecx, 16(%rdx)
	movq	24(%rax), %rcx
	movups	%xmm3, (%rdx)
	movq	%rcx, 24(%rdx)
	leaq	64(%rdx), %rcx
	movq	%rcx, 48(%rdx)
	movq	48(%rax), %rcx
	movups	%xmm4, 32(%rdx)
	cmpq	%rsi, %rcx
	jne	.L1916
	movdqu	64(%rax), %xmm6
	movups	%xmm6, 64(%rdx)
	jmp	.L1917
	.p2align 4,,10
	.p2align 3
.L1939:
	movabsq	$1064235235021704901, %rdx
	subq	%rbx, %rax
	subq	$104, %rax
	shrq	$3, %rax
	imulq	%rdx, %rax
	movabsq	$2305843009213693951, %rdx
	andq	%rdx, %rax
	addq	$1, %rax
	leaq	(%rax,%rax,2), %rdx
	leaq	(%rax,%rdx,4), %rax
	leaq	(%r12,%rax,8), %r12
.L1915:
	testq	%r15, %r15
	je	.L1919
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L1919:
	movq	-56(%rbp), %rax
	movq	-72(%rbp), %rbx
	movq	%r12, %xmm7
	movq	%rax, %xmm0
	addq	-64(%rbp), %rax
	punpcklqdq	%xmm7, %xmm0
	movq	%rax, 16(%rbx)
	movups	%xmm0, (%rbx)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1926:
	.cfi_restore_state
	movq	$104, -64(%rbp)
	jmp	.L1906
	.p2align 4,,10
	.p2align 3
.L1928:
	movq	-56(%rbp), %r12
	jmp	.L1908
.L1937:
	leaq	.LC84(%rip), %rdi
.LEHB256:
	call	_ZSt20__throw_length_errorPKc@PLT
.LEHE256:
.L1907:
	movq	-64(%rbp), %rax
	cmpq	%rdx, %rax
	cmovbe	%rax, %rdx
	imulq	$104, %rdx, %rax
	movq	%rax, -64(%rbp)
	jmp	.L1906
.L1929:
	endbr64
	movq	%rax, %rdi
.L1920:
	call	__cxa_begin_catch@PLT
	cmpq	$0, -56(%rbp)
	je	.L1940
	movq	-56(%rbp), %rdi
	call	_ZdlPv@PLT
.L1924:
.LEHB257:
	call	__cxa_rethrow@PLT
.LEHE257:
.L1940:
	movq	48(%r12), %rdi
	cmpq	-80(%rbp), %rdi
	je	.L1924
	call	_ZdlPv@PLT
	jmp	.L1924
.L1930:
	endbr64
	movq	%rax, %r12
.L1923:
	call	__cxa_end_catch@PLT
	movq	%r12, %rdi
.LEHB258:
	call	_Unwind_Resume@PLT
.LEHE258:
	.cfi_endproc
.LFE8578:
	.section	.gcc_except_table._ZNSt6vectorIN2v88internal6torque5FieldESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,"aG",@progbits,_ZNSt6vectorIN2v88internal6torque5FieldESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,comdat
	.align 4
.LLSDA8578:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATT8578-.LLSDATTD8578
.LLSDATTD8578:
	.byte	0x1
	.uleb128 .LLSDACSE8578-.LLSDACSB8578
.LLSDACSB8578:
	.uleb128 .LEHB254-.LFB8578
	.uleb128 .LEHE254-.LEHB254
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB255-.LFB8578
	.uleb128 .LEHE255-.LEHB255
	.uleb128 .L1929-.LFB8578
	.uleb128 0x1
	.uleb128 .LEHB256-.LFB8578
	.uleb128 .LEHE256-.LEHB256
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB257-.LFB8578
	.uleb128 .LEHE257-.LEHB257
	.uleb128 .L1930-.LFB8578
	.uleb128 0
	.uleb128 .LEHB258-.LFB8578
	.uleb128 .LEHE258-.LEHB258
	.uleb128 0
	.uleb128 0
.LLSDACSE8578:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATT8578:
	.section	.text._ZNSt6vectorIN2v88internal6torque5FieldESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal6torque5FieldESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,comdat
	.size	_ZNSt6vectorIN2v88internal6torque5FieldESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, .-_ZNSt6vectorIN2v88internal6torque5FieldESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.section	.text._ZN2v88internal6torque9ClassType13RegisterFieldENS1_5FieldE,"axG",@progbits,_ZN2v88internal6torque9ClassType13RegisterFieldENS1_5FieldE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque9ClassType13RegisterFieldENS1_5FieldE
	.type	_ZN2v88internal6torque9ClassType13RegisterFieldENS1_5FieldE, @function
_ZN2v88internal6torque9ClassType13RegisterFieldENS1_5FieldE:
.LFB5798:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA5798
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 32(%rsi)
	je	.L1942
	orl	$256, 176(%rdi)
.L1942:
	movl	16(%rbx), %eax
	movq	48(%rbx), %rsi
	leaq	-96(%rbp), %r14
	leaq	-112(%rbp), %rdi
	movq	56(%rbx), %rdx
	movdqu	(%rbx), %xmm0
	movq	%r14, -112(%rbp)
	leaq	-160(%rbp), %r15
	movl	%eax, -144(%rbp)
	movdqu	32(%rbx), %xmm1
	movq	24(%rbx), %rax
	addq	%rsi, %rdx
	movaps	%xmm0, -160(%rbp)
	movaps	%xmm1, -128(%rbp)
	movq	%rax, -136(%rbp)
.LEHB259:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag.constprop.0
.LEHE259:
	movq	88(%rbx), %rdx
	movzbl	98(%rbx), %eax
	movq	80(%rbx), %rcx
	movq	88(%r13), %r12
	movq	%rdx, -72(%rbp)
	movzwl	96(%rbx), %edx
	movq	%rcx, -80(%rbp)
	movw	%dx, -64(%rbp)
	movb	%al, -62(%rbp)
	cmpq	96(%r13), %r12
	je	.L1943
	movdqa	-160(%rbp), %xmm2
	leaq	48(%r12), %rdi
	movups	%xmm2, (%r12)
	movl	-144(%rbp), %eax
	movl	%eax, 16(%r12)
	movq	-136(%rbp), %rax
	movq	%rax, 24(%r12)
	movdqa	-128(%rbp), %xmm3
	leaq	64(%r12), %rax
	movq	%rax, 48(%r12)
	movups	%xmm3, 32(%r12)
	movq	-112(%rbp), %rsi
	movq	-104(%rbp), %rdx
	addq	%rsi, %rdx
.LEHB260:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-80(%rbp), %rax
	movq	%rax, 80(%r12)
	movq	-72(%rbp), %rax
	movq	%rax, 88(%r12)
	movzbl	-64(%rbp), %eax
	movb	%al, 96(%r12)
	movzbl	-63(%rbp), %eax
	movb	%al, 97(%r12)
	movzbl	-62(%rbp), %eax
	movb	%al, 98(%r12)
	movq	88(%r13), %r12
	leaq	104(%r12), %rax
	movq	%rax, 88(%r13)
.L1944:
	movq	-112(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L1941
	call	_ZdlPv@PLT
.L1941:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1951
	addq	$120, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1943:
	.cfi_restore_state
	leaq	80(%r13), %rdi
	movq	%r15, %rdx
	movq	%r12, %rsi
	call	_ZNSt6vectorIN2v88internal6torque5FieldESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
.LEHE260:
	movq	88(%r13), %r12
	subq	$104, %r12
	jmp	.L1944
.L1951:
	call	__stack_chk_fail@PLT
.L1949:
	endbr64
	movq	%rax, %r12
.L1946:
	movq	-112(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L1947
	call	_ZdlPv@PLT
.L1947:
	movq	%r12, %rdi
.LEHB261:
	call	_Unwind_Resume@PLT
.LEHE261:
	.cfi_endproc
.LFE5798:
	.section	.gcc_except_table._ZN2v88internal6torque9ClassType13RegisterFieldENS1_5FieldE,"aG",@progbits,_ZN2v88internal6torque9ClassType13RegisterFieldENS1_5FieldE,comdat
.LLSDA5798:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE5798-.LLSDACSB5798
.LLSDACSB5798:
	.uleb128 .LEHB259-.LFB5798
	.uleb128 .LEHE259-.LEHB259
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB260-.LFB5798
	.uleb128 .LEHE260-.LEHB260
	.uleb128 .L1949-.LFB5798
	.uleb128 0
	.uleb128 .LEHB261-.LFB5798
	.uleb128 .LEHE261-.LEHB261
	.uleb128 0
	.uleb128 0
.LLSDACSE5798:
	.section	.text._ZN2v88internal6torque9ClassType13RegisterFieldENS1_5FieldE,"axG",@progbits,_ZN2v88internal6torque9ClassType13RegisterFieldENS1_5FieldE,comdat
	.size	_ZN2v88internal6torque9ClassType13RegisterFieldENS1_5FieldE, .-_ZN2v88internal6torque9ClassType13RegisterFieldENS1_5FieldE
	.section	.text._ZN2v88internal6torque13AggregateType13RegisterFieldENS1_5FieldE,"axG",@progbits,_ZN2v88internal6torque13AggregateType13RegisterFieldENS1_5FieldE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque13AggregateType13RegisterFieldENS1_5FieldE
	.type	_ZN2v88internal6torque13AggregateType13RegisterFieldENS1_5FieldE, @function
_ZN2v88internal6torque13AggregateType13RegisterFieldENS1_5FieldE:
.LFB5725:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	88(%rdi), %r13
	cmpq	96(%rdi), %r13
	je	.L1953
	movl	16(%rsi), %eax
	movdqu	(%rsi), %xmm0
	leaq	48(%r13), %rdi
	movdqu	32(%rsi), %xmm1
	movl	%eax, 16(%r13)
	movq	24(%rsi), %rax
	movups	%xmm0, 0(%r13)
	movq	%rax, 24(%r13)
	leaq	64(%r13), %rax
	movq	%rax, 48(%r13)
	movups	%xmm1, 32(%r13)
	movq	56(%r12), %rdx
	movq	48(%rsi), %rsi
	addq	%rsi, %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	80(%r12), %rax
	movq	%rax, 80(%r13)
	movq	88(%r12), %rdx
	movzbl	98(%r12), %eax
	movq	%rdx, 88(%r13)
	movzwl	96(%r12), %edx
	movb	%al, 98(%r13)
	movw	%dx, 96(%r13)
	movq	88(%rbx), %rax
	leaq	104(%rax), %rdx
	movq	%rdx, 88(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1953:
	.cfi_restore_state
	movq	%rsi, %rdx
	leaq	80(%rdi), %rdi
	movq	%r13, %rsi
	call	_ZNSt6vectorIN2v88internal6torque5FieldESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	movq	88(%rbx), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	subq	$104, %rax
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5725:
	.size	_ZN2v88internal6torque13AggregateType13RegisterFieldENS1_5FieldE, .-_ZN2v88internal6torque13AggregateType13RegisterFieldENS1_5FieldE
	.section	.text._ZNSt6vectorIPN2v88internal6torque6MethodESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_,"axG",@progbits,_ZNSt6vectorIPN2v88internal6torque6MethodESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPN2v88internal6torque6MethodESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	.type	_ZNSt6vectorIPN2v88internal6torque6MethodESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_, @function
_ZNSt6vectorIPN2v88internal6torque6MethodESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_:
.LFB8585:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r9
	movq	%r13, %rax
	subq	%r9, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L1970
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L1966
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L1971
.L1958:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L1965:
	movq	(%r15), %rax
	subq	%r8, %r13
	leaq	8(%rbx,%rdx), %r15
	movq	%rax, (%rbx,%rdx)
	leaq	(%r15,%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L1972
	testq	%r13, %r13
	jg	.L1961
	testq	%r9, %r9
	jne	.L1964
.L1962:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1972:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jg	.L1961
.L1964:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L1962
	.p2align 4,,10
	.p2align 3
.L1971:
	testq	%rsi, %rsi
	jne	.L1959
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L1965
	.p2align 4,,10
	.p2align 3
.L1961:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L1962
	jmp	.L1964
	.p2align 4,,10
	.p2align 3
.L1966:
	movl	$8, %r14d
	jmp	.L1958
.L1970:
	leaq	.LC84(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1959:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$3, %r14
	jmp	.L1958
	.cfi_endproc
.LFE8585:
	.size	_ZNSt6vectorIPN2v88internal6torque6MethodESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_, .-_ZNSt6vectorIPN2v88internal6torque6MethodESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	.section	.text.unlikely._ZNK2v88internal6torque13AggregateType7MethodsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"ax",@progbits
	.align 2
.LCOLDB85:
	.section	.text._ZNK2v88internal6torque13AggregateType7MethodsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"ax",@progbits
.LHOTB85:
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal6torque13AggregateType7MethodsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZNK2v88internal6torque13AggregateType7MethodsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZNK2v88internal6torque13AggregateType7MethodsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB6564:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6564
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 72(%rsi)
	jne	.L1974
	movq	(%rsi), %rax
	movq	%rsi, %rdi
.LEHB262:
	call	*96(%rax)
.LEHE262:
.L1974:
	pxor	%xmm0, %xmm0
	leaq	-80(%rbp), %r14
	leaq	-96(%rbp), %rdi
	movq	$0, 16(%r12)
	movups	%xmm0, (%r12)
	movq	0(%r13), %rsi
	movq	8(%r13), %rdx
	movq	%r14, -96(%rbp)
	addq	%rsi, %rdx
.LEHB263:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag.constprop.0
.LEHE263:
	movq	152(%rbx), %r13
	movq	144(%rbx), %rbx
	movq	-96(%rbp), %r15
	cmpq	%rbx, %r13
	jne	.L1979
	jmp	.L1975
	.p2align 4,,10
	.p2align 3
.L1976:
	addq	$8, %rbx
	cmpq	%rbx, %r13
	je	.L1975
.L1979:
	movq	(%rbx), %rcx
	movq	168(%rcx), %rdx
	cmpq	-88(%rbp), %rdx
	jne	.L1976
	testq	%rdx, %rdx
	je	.L1977
	movq	160(%rcx), %rdi
	movq	%r15, %rsi
	movq	%rcx, -104(%rbp)
	call	memcmp@PLT
	movq	-104(%rbp), %rcx
	testl	%eax, %eax
	jne	.L1976
.L1977:
	movq	8(%r12), %rsi
	cmpq	16(%r12), %rsi
	je	.L1978
	addq	$8, %rbx
	movq	%rcx, (%rsi)
	movq	-96(%rbp), %r15
	addq	$8, 8(%r12)
	cmpq	%rbx, %r13
	jne	.L1979
.L1975:
	cmpq	%r14, %r15
	je	.L1973
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L1973:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1996
	addq	$72, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1978:
	.cfi_restore_state
	movq	%rbx, %rdx
	movq	%r12, %rdi
.LEHB264:
	call	_ZNSt6vectorIPN2v88internal6torque6MethodESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
.LEHE264:
	movq	-96(%rbp), %r15
	jmp	.L1976
.L1996:
	call	__stack_chk_fail@PLT
.L1986:
	endbr64
	movq	%rax, %r13
	jmp	.L1983
.L1987:
	endbr64
	movq	%rax, %r13
	jmp	.L1981
	.section	.gcc_except_table._ZNK2v88internal6torque13AggregateType7MethodsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"a",@progbits
.LLSDA6564:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6564-.LLSDACSB6564
.LLSDACSB6564:
	.uleb128 .LEHB262-.LFB6564
	.uleb128 .LEHE262-.LEHB262
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB263-.LFB6564
	.uleb128 .LEHE263-.LEHB263
	.uleb128 .L1986-.LFB6564
	.uleb128 0
	.uleb128 .LEHB264-.LFB6564
	.uleb128 .LEHE264-.LEHB264
	.uleb128 .L1987-.LFB6564
	.uleb128 0
.LLSDACSE6564:
	.section	.text._ZNK2v88internal6torque13AggregateType7MethodsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.cfi_endproc
	.section	.text.unlikely._ZNK2v88internal6torque13AggregateType7MethodsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6564
	.type	_ZNK2v88internal6torque13AggregateType7MethodsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.cold, @function
_ZNK2v88internal6torque13AggregateType7MethodsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.cold:
.LFSB6564:
.L1981:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L1983
	call	_ZdlPv@PLT
.L1983:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1984
	call	_ZdlPv@PLT
.L1984:
	movq	%r13, %rdi
.LEHB265:
	call	_Unwind_Resume@PLT
.LEHE265:
	.cfi_endproc
.LFE6564:
	.section	.gcc_except_table._ZNK2v88internal6torque13AggregateType7MethodsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
.LLSDAC6564:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6564-.LLSDACSBC6564
.LLSDACSBC6564:
	.uleb128 .LEHB265-.LCOLDB85
	.uleb128 .LEHE265-.LEHB265
	.uleb128 0
	.uleb128 0
.LLSDACSEC6564:
	.section	.text.unlikely._ZNK2v88internal6torque13AggregateType7MethodsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.text._ZNK2v88internal6torque13AggregateType7MethodsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.size	_ZNK2v88internal6torque13AggregateType7MethodsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZNK2v88internal6torque13AggregateType7MethodsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.text.unlikely._ZNK2v88internal6torque13AggregateType7MethodsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.size	_ZNK2v88internal6torque13AggregateType7MethodsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.cold, .-_ZNK2v88internal6torque13AggregateType7MethodsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.cold
.LCOLDE85:
	.section	.text._ZNK2v88internal6torque13AggregateType7MethodsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
.LHOTE85:
	.section	.text._ZNSt6vectorIPKN2v88internal6torque4TypeESaIS5_EE12emplace_backIJS5_EEEvDpOT_,"axG",@progbits,_ZNSt6vectorIPKN2v88internal6torque4TypeESaIS5_EE12emplace_backIJS5_EEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPKN2v88internal6torque4TypeESaIS5_EE12emplace_backIJS5_EEEvDpOT_
	.type	_ZNSt6vectorIPKN2v88internal6torque4TypeESaIS5_EE12emplace_backIJS5_EEEvDpOT_, @function
_ZNSt6vectorIPKN2v88internal6torque4TypeESaIS5_EE12emplace_backIJS5_EEEvDpOT_:
.LFB8648:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	8(%rdi), %rdx
	cmpq	16(%rdi), %rdx
	je	.L1998
	movq	(%rsi), %rax
	movq	%rax, (%rdx)
	addq	$8, 8(%rdi)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1998:
	.cfi_restore_state
	movq	(%rdi), %r15
	subq	%r15, %rdx
	movq	%rdx, %rax
	movq	%rdx, %r12
	movabsq	$1152921504606846975, %rdx
	sarq	$3, %rax
	cmpq	%rdx, %rax
	je	.L2013
	testq	%rax, %rax
	je	.L2006
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rcx
	cmpq	%rcx, %rax
	jbe	.L2014
.L2001:
	movq	%r14, %rdi
	movq	%rsi, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r13
	addq	%rax, %r14
.L2002:
	movq	(%rsi), %rax
	movq	%rax, 0(%r13,%r12)
	leaq	8(%r13,%r12), %rax
	movq	%rax, -56(%rbp)
	testq	%r12, %r12
	jg	.L2015
	testq	%r15, %r15
	jne	.L2004
.L2005:
	movq	%r13, %xmm0
	movq	%r14, 16(%rbx)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2015:
	.cfi_restore_state
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	memmove@PLT
.L2004:
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	jmp	.L2005
	.p2align 4,,10
	.p2align 3
.L2014:
	testq	%rcx, %rcx
	jne	.L2016
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	jmp	.L2002
	.p2align 4,,10
	.p2align 3
.L2006:
	movl	$8, %r14d
	jmp	.L2001
.L2013:
	leaq	.LC84(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L2016:
	cmpq	%rdx, %rcx
	cmovbe	%rcx, %rdx
	leaq	0(,%rdx,8), %r14
	jmp	.L2001
	.cfi_endproc
.LFE8648:
	.size	_ZNSt6vectorIPKN2v88internal6torque4TypeESaIS5_EE12emplace_backIJS5_EEEvDpOT_, .-_ZNSt6vectorIPKN2v88internal6torque4TypeESaIS5_EE12emplace_backIJS5_EEEvDpOT_
	.section	.text._ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE16_M_insert_uniqueIS5_EESt4pairISt17_Rb_tree_iteratorIS5_EbEOT_,"axG",@progbits,_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE16_M_insert_uniqueIS5_EESt4pairISt17_Rb_tree_iteratorIS5_EbEOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE16_M_insert_uniqueIS5_EESt4pairISt17_Rb_tree_iteratorIS5_EbEOT_
	.type	_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE16_M_insert_uniqueIS5_EESt4pairISt17_Rb_tree_iteratorIS5_EbEOT_, @function
_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE16_M_insert_uniqueIS5_EESt4pairISt17_Rb_tree_iteratorIS5_EbEOT_:
.LFB8970:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	8(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r12
	testq	%r12, %r12
	jne	.L2019
	jmp	.L2035
	.p2align 4,,10
	.p2align 3
.L2036:
	movq	16(%r12), %rdx
	testq	%rdx, %rdx
	je	.L2020
.L2037:
	movq	%rdx, %r12
.L2019:
	movq	32(%r12), %rsi
	movq	(%r14), %rdi
	call	_ZN2v88internal6torqueltERKNS1_4TypeES4_
	testb	%al, %al
	jne	.L2036
	movq	24(%r12), %rdx
	testq	%rdx, %rdx
	jne	.L2037
.L2020:
	testb	%al, %al
	je	.L2038
	cmpq	24(%r13), %r12
	je	.L2027
.L2029:
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	(%r14), %rsi
	movq	32(%rax), %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal6torqueltERKNS1_4TypeES4_
	testb	%al, %al
	jne	.L2027
.L2028:
	addq	$24, %rsp
	movq	%rbx, %rax
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2038:
	.cfi_restore_state
	movq	32(%r12), %rdi
	movq	(%r14), %rsi
	movq	%r12, %rbx
	call	_ZN2v88internal6torqueltERKNS1_4TypeES4_
	testb	%al, %al
	je	.L2028
.L2027:
	movl	$1, %r8d
	cmpq	%r15, %r12
	jne	.L2039
.L2025:
	movl	$40, %edi
	movl	%r8d, -52(%rbp)
	call	_Znwm@PLT
	movl	-52(%rbp), %r8d
	movq	%r12, %rdx
	movq	%r15, %rcx
	movq	%rax, %rbx
	movq	(%r14), %rax
	movq	%rbx, %rsi
	movl	%r8d, %edi
	movq	%rax, 32(%rbx)
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 40(%r13)
	addq	$24, %rsp
	movq	%rbx, %rax
	movl	$1, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2039:
	.cfi_restore_state
	movq	32(%r12), %rsi
	movq	(%r14), %rdi
	call	_ZN2v88internal6torqueltERKNS1_4TypeES4_
	movzbl	%al, %r8d
	jmp	.L2025
	.p2align 4,,10
	.p2align 3
.L2035:
	movq	%r15, %r12
	cmpq	24(%rdi), %r15
	jne	.L2029
	movl	$1, %r8d
	jmp	.L2025
	.cfi_endproc
.LFE8970:
	.size	_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE16_M_insert_uniqueIS5_EESt4pairISt17_Rb_tree_iteratorIS5_EbEOT_, .-_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE16_M_insert_uniqueIS5_EESt4pairISt17_Rb_tree_iteratorIS5_EbEOT_
	.section	.text.unlikely._ZN2v88internal6torque9UnionType8SubtractEPKNS1_4TypeE,"ax",@progbits
	.align 2
.LCOLDB86:
	.section	.text._ZN2v88internal6torque9UnionType8SubtractEPKNS1_4TypeE,"ax",@progbits
.LHOTB86:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque9UnionType8SubtractEPKNS1_4TypeE
	.type	_ZN2v88internal6torque9UnionType8SubtractEPKNS1_4TypeE, @function
_ZN2v88internal6torque9UnionType8SubtractEPKNS1_4TypeE:
.LFB6524:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6524
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	80(%rdi), %rbx
	subq	$72, %rsp
	movq	96(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	%rbx, %r12
	je	.L2065
	.p2align 4,,10
	.p2align 3
.L2041:
	movq	32(%r12), %rdi
	movq	%r14, %rsi
	movq	(%rdi), %rax
.LEHB266:
	call	*16(%rax)
	movq	%r12, %rdi
	testb	%al, %al
	jne	.L2066
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r12
	cmpq	%rbx, %rax
	jne	.L2041
.L2045:
	cmpq	$0, 112(%r13)
	je	.L2055
.L2046:
	movq	96(%r13), %r12
	cmpq	%rbx, %r12
	je	.L2064
	movq	32(%r12), %r14
.L2054:
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r12
	cmpq	%rbx, %rax
	je	.L2051
.L2053:
	movq	32(%r12), %rsi
	testq	%r14, %r14
	je	.L2058
	movq	%r14, %rdi
	call	_ZN2v88internal6torque4Type15CommonSupertypeEPKS2_S4_
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r12
	cmpq	%rbx, %rax
	jne	.L2053
.L2051:
	movq	%r14, 16(%r13)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2067
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2058:
	.cfi_restore_state
	movq	%rsi, %r14
	jmp	.L2054
	.p2align 4,,10
	.p2align 3
.L2066:
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZSt28_Rb_tree_rebalance_for_erasePSt18_Rb_tree_node_baseRS_@PLT
	movq	%rax, %rdi
	call	_ZdlPv@PLT
	subq	$1, 112(%r13)
	cmpq	%rbx, %r15
	je	.L2045
	movq	%r15, %r12
	jmp	.L2041
	.p2align 4,,10
	.p2align 3
.L2065:
	cmpq	$0, 112(%rdi)
	jne	.L2064
	.p2align 4,,10
	.p2align 3
.L2055:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_10TypeOracleEEERPNT_5ScopeEv@PLT
.LEHE266:
	leaq	-80(%rbp), %r14
	leaq	-96(%rbp), %rdi
	movl	$1702258030, -80(%rbp)
	movq	%r14, -96(%rbp)
	movb	$114, -76(%rbp)
	movq	$5, -88(%rbp)
	movb	$0, -75(%rbp)
.LEHB267:
	call	_ZN2v88internal6torque12Declarations16LookupGlobalTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
.LEHE267:
	movq	-96(%rbp), %rdi
	movq	%rax, %r12
	cmpq	%r14, %rdi
	je	.L2047
	call	_ZdlPv@PLT
.L2047:
	leaq	-104(%rbp), %rsi
	leaq	72(%r13), %rdi
	movq	%r12, -104(%rbp)
.LEHB268:
	call	_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE16_M_insert_uniqueIS5_EESt4pairISt17_Rb_tree_iteratorIS5_EbEOT_
.LEHE268:
	jmp	.L2046
	.p2align 4,,10
	.p2align 3
.L2064:
	xorl	%r14d, %r14d
	jmp	.L2051
.L2067:
	call	__stack_chk_fail@PLT
.L2059:
	endbr64
	movq	%rax, %r12
	jmp	.L2048
	.section	.gcc_except_table._ZN2v88internal6torque9UnionType8SubtractEPKNS1_4TypeE,"a",@progbits
.LLSDA6524:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6524-.LLSDACSB6524
.LLSDACSB6524:
	.uleb128 .LEHB266-.LFB6524
	.uleb128 .LEHE266-.LEHB266
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB267-.LFB6524
	.uleb128 .LEHE267-.LEHB267
	.uleb128 .L2059-.LFB6524
	.uleb128 0
	.uleb128 .LEHB268-.LFB6524
	.uleb128 .LEHE268-.LEHB268
	.uleb128 0
	.uleb128 0
.LLSDACSE6524:
	.section	.text._ZN2v88internal6torque9UnionType8SubtractEPKNS1_4TypeE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque9UnionType8SubtractEPKNS1_4TypeE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6524
	.type	_ZN2v88internal6torque9UnionType8SubtractEPKNS1_4TypeE.cold, @function
_ZN2v88internal6torque9UnionType8SubtractEPKNS1_4TypeE.cold:
.LFSB6524:
.L2048:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L2049
	call	_ZdlPv@PLT
.L2049:
	movq	%r12, %rdi
.LEHB269:
	call	_Unwind_Resume@PLT
.LEHE269:
	.cfi_endproc
.LFE6524:
	.section	.gcc_except_table._ZN2v88internal6torque9UnionType8SubtractEPKNS1_4TypeE
.LLSDAC6524:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6524-.LLSDACSBC6524
.LLSDACSBC6524:
	.uleb128 .LEHB269-.LCOLDB86
	.uleb128 .LEHE269-.LEHB269
	.uleb128 0
	.uleb128 0
.LLSDACSEC6524:
	.section	.text.unlikely._ZN2v88internal6torque9UnionType8SubtractEPKNS1_4TypeE
	.section	.text._ZN2v88internal6torque9UnionType8SubtractEPKNS1_4TypeE
	.size	_ZN2v88internal6torque9UnionType8SubtractEPKNS1_4TypeE, .-_ZN2v88internal6torque9UnionType8SubtractEPKNS1_4TypeE
	.section	.text.unlikely._ZN2v88internal6torque9UnionType8SubtractEPKNS1_4TypeE
	.size	_ZN2v88internal6torque9UnionType8SubtractEPKNS1_4TypeE.cold, .-_ZN2v88internal6torque9UnionType8SubtractEPKNS1_4TypeE.cold
.LCOLDE86:
	.section	.text._ZN2v88internal6torque9UnionType8SubtractEPKNS1_4TypeE
.LHOTE86:
	.section	.text._ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_PKN2v88internal6torque13AggregateTypeEESt10_Select1stISE_ESt4lessIS5_ESaISE_EE8_M_eraseEPSt13_Rb_tree_nodeISE_E,"axG",@progbits,_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_PKN2v88internal6torque13AggregateTypeEESt10_Select1stISE_ESt4lessIS5_ESaISE_EE8_M_eraseEPSt13_Rb_tree_nodeISE_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_PKN2v88internal6torque13AggregateTypeEESt10_Select1stISE_ESt4lessIS5_ESaISE_EE8_M_eraseEPSt13_Rb_tree_nodeISE_E
	.type	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_PKN2v88internal6torque13AggregateTypeEESt10_Select1stISE_ESt4lessIS5_ESaISE_EE8_M_eraseEPSt13_Rb_tree_nodeISE_E, @function
_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_PKN2v88internal6torque13AggregateTypeEESt10_Select1stISE_ESt4lessIS5_ESaISE_EE8_M_eraseEPSt13_Rb_tree_nodeISE_E:
.LFB8989:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L2083
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
.L2072:
	movq	24(%r12), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_PKN2v88internal6torque13AggregateTypeEESt10_Select1stISE_ESt4lessIS5_ESaISE_EE8_M_eraseEPSt13_Rb_tree_nodeISE_E
	movq	32(%r12), %rdi
	leaq	48(%r12), %rax
	movq	16(%r12), %rbx
	cmpq	%rax, %rdi
	je	.L2070
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L2068
.L2071:
	movq	%rbx, %r12
	jmp	.L2072
	.p2align 4,,10
	.p2align 3
.L2070:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L2071
.L2068:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2083:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.cfi_endproc
.LFE8989:
	.size	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_PKN2v88internal6torque13AggregateTypeEESt10_Select1stISE_ESt4lessIS5_ESaISE_EE8_M_eraseEPSt13_Rb_tree_nodeISE_E, .-_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_PKN2v88internal6torque13AggregateTypeEESt10_Select1stISE_ESt4lessIS5_ESaISE_EE8_M_eraseEPSt13_Rb_tree_nodeISE_E
	.section	.text._ZNSt6vectorIPKN2v88internal6torque13AggregateTypeESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_,"axG",@progbits,_ZNSt6vectorIPKN2v88internal6torque13AggregateTypeESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPKN2v88internal6torque13AggregateTypeESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	.type	_ZNSt6vectorIPKN2v88internal6torque13AggregateTypeESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_, @function
_ZNSt6vectorIPKN2v88internal6torque13AggregateTypeESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_:
.LFB9031:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r9
	movq	%r13, %rax
	subq	%r9, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L2100
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L2096
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L2101
.L2088:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L2095:
	movq	(%r15), %rax
	subq	%r8, %r13
	leaq	8(%rbx,%rdx), %r15
	movq	%rax, (%rbx,%rdx)
	leaq	(%r15,%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L2102
	testq	%r13, %r13
	jg	.L2091
	testq	%r9, %r9
	jne	.L2094
.L2092:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2102:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jg	.L2091
.L2094:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L2092
	.p2align 4,,10
	.p2align 3
.L2101:
	testq	%rsi, %rsi
	jne	.L2089
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L2095
	.p2align 4,,10
	.p2align 3
.L2091:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L2092
	jmp	.L2094
	.p2align 4,,10
	.p2align 3
.L2096:
	movl	$8, %r14d
	jmp	.L2088
.L2100:
	leaq	.LC84(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L2089:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$3, %r14
	jmp	.L2088
	.cfi_endproc
.LFE9031:
	.size	_ZNSt6vectorIPKN2v88internal6torque13AggregateTypeESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_, .-_ZNSt6vectorIPKN2v88internal6torque13AggregateTypeESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	.section	.text.unlikely._ZNK2v88internal6torque13AggregateType12GetHierarchyEv,"ax",@progbits
	.align 2
.LCOLDB87:
	.section	.text._ZNK2v88internal6torque13AggregateType12GetHierarchyEv,"ax",@progbits
.LHOTB87:
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal6torque13AggregateType12GetHierarchyEv
	.type	_ZNK2v88internal6torque13AggregateType12GetHierarchyEv, @function
_ZNK2v88internal6torque13AggregateType12GetHierarchyEv:
.LFB6548:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6548
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 72(%rsi)
	jne	.L2104
	movq	(%rsi), %rax
	movq	%rsi, %rdi
.LEHB270:
	call	*96(%rax)
.LEHE270:
.L2104:
	pxor	%xmm0, %xmm0
	xorl	%eax, %eax
	xorl	%esi, %esi
	movq	%rbx, -48(%rbp)
	movq	$0, 16(%r12)
	leaq	-48(%rbp), %r13
	movups	%xmm0, (%r12)
	cmpq	%rax, %rsi
	je	.L2105
	.p2align 4,,10
	.p2align 3
.L2139:
	movq	%rbx, (%rsi)
	movq	8(%r12), %rax
	leaq	8(%rax), %rsi
	movq	%rsi, 8(%r12)
.L2106:
	movq	-48(%rbp), %rax
	cmpl	$5, 8(%rax)
	je	.L2137
.L2107:
	movq	(%r12), %rcx
	movq	$0, -48(%rbp)
	cmpq	%rcx, %rsi
	je	.L2103
	leaq	-8(%rsi), %rdx
	cmpq	%rcx, %rdx
	jbe	.L2103
	movq	%rsi, %rdi
	movq	%rcx, %rax
	subq	%rcx, %rdi
	subq	$9, %rdi
	movq	%rdi, %r8
	shrq	$4, %r8
	addq	$1, %r8
	movq	%r8, %r9
	leaq	(%rcx,%r8,8), %r10
	negq	%r9
	leaq	(%rsi,%r9,8), %r9
	cmpq	%r9, %r10
	setbe	%r10b
	cmpq	%rcx, %rsi
	setbe	%r9b
	orb	%r9b, %r10b
	je	.L2118
	cmpq	$31, %rdi
	jbe	.L2118
	movq	%r8, %rdi
	subq	$16, %rsi
	shrq	%rdi
	salq	$4, %rdi
	addq	%rcx, %rdi
	.p2align 4,,10
	.p2align 3
.L2115:
	movdqu	(%rsi), %xmm1
	movdqu	(%rax), %xmm0
	addq	$16, %rax
	subq	$16, %rsi
	shufpd	$1, %xmm1, %xmm1
	shufpd	$1, %xmm0, %xmm0
	movups	%xmm1, -16(%rax)
	movups	%xmm0, 16(%rsi)
	cmpq	%rdi, %rax
	jne	.L2115
	movq	%r8, %rsi
	andq	$-2, %rsi
	movq	%rsi, %rax
	negq	%rax
	leaq	(%rdx,%rax,8), %rax
	leaq	(%rcx,%rsi,8), %rdx
	cmpq	%rsi, %r8
	je	.L2103
	movq	(%rdx), %rcx
	movq	(%rax), %rsi
	movq	%rsi, (%rdx)
	movq	%rcx, (%rax)
.L2103:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2138
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2118:
	.cfi_restore_state
	movq	(%rax), %rcx
	movq	(%rdx), %rsi
	addq	$8, %rax
	subq	$8, %rdx
	movq	%rsi, -8(%rax)
	movq	%rcx, 8(%rdx)
	cmpq	%rdx, %rax
	jb	.L2118
	jmp	.L2103
	.p2align 4,,10
	.p2align 3
.L2137:
	movq	16(%rax), %rbx
	testq	%rbx, %rbx
	je	.L2107
	cmpl	$5, 8(%rbx)
	jne	.L2107
	movq	16(%r12), %rax
	movq	%rbx, -48(%rbp)
	cmpq	%rax, %rsi
	jne	.L2139
.L2105:
	movq	%r13, %rdx
	movq	%r12, %rdi
.LEHB271:
	call	_ZNSt6vectorIPKN2v88internal6torque13AggregateTypeESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
.LEHE271:
	movq	8(%r12), %rsi
	jmp	.L2106
.L2138:
	call	__stack_chk_fail@PLT
.L2124:
	endbr64
	movq	%rax, %r13
	jmp	.L2120
	.section	.gcc_except_table._ZNK2v88internal6torque13AggregateType12GetHierarchyEv,"a",@progbits
.LLSDA6548:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6548-.LLSDACSB6548
.LLSDACSB6548:
	.uleb128 .LEHB270-.LFB6548
	.uleb128 .LEHE270-.LEHB270
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB271-.LFB6548
	.uleb128 .LEHE271-.LEHB271
	.uleb128 .L2124-.LFB6548
	.uleb128 0
.LLSDACSE6548:
	.section	.text._ZNK2v88internal6torque13AggregateType12GetHierarchyEv
	.cfi_endproc
	.section	.text.unlikely._ZNK2v88internal6torque13AggregateType12GetHierarchyEv
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6548
	.type	_ZNK2v88internal6torque13AggregateType12GetHierarchyEv.cold, @function
_ZNK2v88internal6torque13AggregateType12GetHierarchyEv.cold:
.LFSB6548:
.L2120:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2121
	call	_ZdlPv@PLT
.L2121:
	movq	%r13, %rdi
.LEHB272:
	call	_Unwind_Resume@PLT
.LEHE272:
	.cfi_endproc
.LFE6548:
	.section	.gcc_except_table._ZNK2v88internal6torque13AggregateType12GetHierarchyEv
.LLSDAC6548:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6548-.LLSDACSBC6548
.LLSDACSBC6548:
	.uleb128 .LEHB272-.LCOLDB87
	.uleb128 .LEHE272-.LEHB272
	.uleb128 0
	.uleb128 0
.LLSDACSEC6548:
	.section	.text.unlikely._ZNK2v88internal6torque13AggregateType12GetHierarchyEv
	.section	.text._ZNK2v88internal6torque13AggregateType12GetHierarchyEv
	.size	_ZNK2v88internal6torque13AggregateType12GetHierarchyEv, .-_ZNK2v88internal6torque13AggregateType12GetHierarchyEv
	.section	.text.unlikely._ZNK2v88internal6torque13AggregateType12GetHierarchyEv
	.size	_ZNK2v88internal6torque13AggregateType12GetHierarchyEv.cold, .-_ZNK2v88internal6torque13AggregateType12GetHierarchyEv.cold
.LCOLDE87:
	.section	.text._ZNK2v88internal6torque13AggregateType12GetHierarchyEv
.LHOTE87:
	.section	.text._ZNSt6vectorIPKN2v88internal6torque4TypeESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_,"axG",@progbits,_ZNSt6vectorIPKN2v88internal6torque4TypeESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPKN2v88internal6torque4TypeESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	.type	_ZNSt6vectorIPKN2v88internal6torque4TypeESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_, @function
_ZNSt6vectorIPKN2v88internal6torque4TypeESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_:
.LFB9157:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r9
	movq	%r13, %rax
	subq	%r9, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L2154
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L2150
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L2155
.L2142:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L2149:
	movq	(%r15), %rax
	subq	%r8, %r13
	leaq	8(%rbx,%rdx), %r15
	movq	%rax, (%rbx,%rdx)
	leaq	(%r15,%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L2156
	testq	%r13, %r13
	jg	.L2145
	testq	%r9, %r9
	jne	.L2148
.L2146:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2156:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jg	.L2145
.L2148:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L2146
	.p2align 4,,10
	.p2align 3
.L2155:
	testq	%rsi, %rsi
	jne	.L2143
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L2149
	.p2align 4,,10
	.p2align 3
.L2145:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L2146
	jmp	.L2148
	.p2align 4,,10
	.p2align 3
.L2150:
	movl	$8, %r14d
	jmp	.L2142
.L2154:
	leaq	.LC84(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L2143:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$3, %r14
	jmp	.L2142
	.cfi_endproc
.LFE9157:
	.size	_ZNSt6vectorIPKN2v88internal6torque4TypeESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_, .-_ZNSt6vectorIPKN2v88internal6torque4TypeESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	.section	.text.unlikely._ZN2v88internal6torque12_GLOBAL__N_118AppendLoweredTypesEPKNS1_4TypeEPSt6vectorIS5_SaIS5_EE,"ax",@progbits
.LCOLDB88:
	.section	.text._ZN2v88internal6torque12_GLOBAL__N_118AppendLoweredTypesEPKNS1_4TypeEPSt6vectorIS5_SaIS5_EE,"ax",@progbits
.LHOTB88:
	.p2align 4
	.type	_ZN2v88internal6torque12_GLOBAL__N_118AppendLoweredTypesEPKNS1_4TypeEPSt6vectorIS5_SaIS5_EE, @function
_ZN2v88internal6torque12_GLOBAL__N_118AppendLoweredTypesEPKNS1_4TypeEPSt6vectorIS5_SaIS5_EE:
.LFB6624:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6624
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -40
	movq	%rdi, -88(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
.LEHB273:
	call	*32(%rax)
	testb	%al, %al
	je	.L2177
.L2157:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2178
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2177:
	.cfi_restore_state
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_10TypeOracleEEERPNT_5ScopeEv@PLT
.LEHE273:
	leaq	-64(%rbp), %r13
	leaq	-80(%rbp), %rdi
	movl	$1684631414, -64(%rbp)
	movq	%r13, -80(%rbp)
	movq	$4, -72(%rbp)
	movb	$0, -60(%rbp)
.LEHB274:
	call	_ZN2v88internal6torque12Declarations16LookupGlobalTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
.LEHE274:
	movq	-80(%rbp), %rdi
	movq	%rax, %rbx
	cmpq	%r13, %rdi
	je	.L2160
	call	_ZdlPv@PLT
.L2160:
	movq	-88(%rbp), %r13
	cmpq	%rbx, %r13
	je	.L2157
	testq	%r13, %r13
	je	.L2165
	cmpl	$4, 8(%r13)
	jne	.L2165
	cmpb	$0, 72(%r13)
	je	.L2179
.L2166:
	movq	80(%r13), %rbx
	movq	88(%r13), %r13
	cmpq	%r13, %rbx
	je	.L2157
.L2167:
	movq	80(%rbx), %rdi
	movq	%r12, %rsi
	addq	$104, %rbx
.LEHB275:
	call	_ZN2v88internal6torque12_GLOBAL__N_118AppendLoweredTypesEPKNS1_4TypeEPSt6vectorIS5_SaIS5_EE
	cmpq	%rbx, %r13
	jne	.L2167
	jmp	.L2157
	.p2align 4,,10
	.p2align 3
.L2179:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*96(%rax)
	jmp	.L2166
	.p2align 4,,10
	.p2align 3
.L2165:
	movq	8(%r12), %rsi
	cmpq	16(%r12), %rsi
	je	.L2168
	movq	%r13, (%rsi)
	addq	$8, 8(%r12)
	jmp	.L2157
	.p2align 4,,10
	.p2align 3
.L2168:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorIPKN2v88internal6torque4TypeESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
.LEHE275:
	jmp	.L2157
.L2178:
	call	__stack_chk_fail@PLT
.L2170:
	endbr64
	movq	%rax, %r12
	jmp	.L2163
	.section	.gcc_except_table._ZN2v88internal6torque12_GLOBAL__N_118AppendLoweredTypesEPKNS1_4TypeEPSt6vectorIS5_SaIS5_EE,"a",@progbits
.LLSDA6624:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6624-.LLSDACSB6624
.LLSDACSB6624:
	.uleb128 .LEHB273-.LFB6624
	.uleb128 .LEHE273-.LEHB273
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB274-.LFB6624
	.uleb128 .LEHE274-.LEHB274
	.uleb128 .L2170-.LFB6624
	.uleb128 0
	.uleb128 .LEHB275-.LFB6624
	.uleb128 .LEHE275-.LEHB275
	.uleb128 0
	.uleb128 0
.LLSDACSE6624:
	.section	.text._ZN2v88internal6torque12_GLOBAL__N_118AppendLoweredTypesEPKNS1_4TypeEPSt6vectorIS5_SaIS5_EE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque12_GLOBAL__N_118AppendLoweredTypesEPKNS1_4TypeEPSt6vectorIS5_SaIS5_EE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6624
	.type	_ZN2v88internal6torque12_GLOBAL__N_118AppendLoweredTypesEPKNS1_4TypeEPSt6vectorIS5_SaIS5_EE.cold, @function
_ZN2v88internal6torque12_GLOBAL__N_118AppendLoweredTypesEPKNS1_4TypeEPSt6vectorIS5_SaIS5_EE.cold:
.LFSB6624:
.L2163:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movq	-80(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L2164
	call	_ZdlPv@PLT
.L2164:
	movq	%r12, %rdi
.LEHB276:
	call	_Unwind_Resume@PLT
.LEHE276:
	.cfi_endproc
.LFE6624:
	.section	.gcc_except_table._ZN2v88internal6torque12_GLOBAL__N_118AppendLoweredTypesEPKNS1_4TypeEPSt6vectorIS5_SaIS5_EE
.LLSDAC6624:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6624-.LLSDACSBC6624
.LLSDACSBC6624:
	.uleb128 .LEHB276-.LCOLDB88
	.uleb128 .LEHE276-.LEHB276
	.uleb128 0
	.uleb128 0
.LLSDACSEC6624:
	.section	.text.unlikely._ZN2v88internal6torque12_GLOBAL__N_118AppendLoweredTypesEPKNS1_4TypeEPSt6vectorIS5_SaIS5_EE
	.section	.text._ZN2v88internal6torque12_GLOBAL__N_118AppendLoweredTypesEPKNS1_4TypeEPSt6vectorIS5_SaIS5_EE
	.size	_ZN2v88internal6torque12_GLOBAL__N_118AppendLoweredTypesEPKNS1_4TypeEPSt6vectorIS5_SaIS5_EE, .-_ZN2v88internal6torque12_GLOBAL__N_118AppendLoweredTypesEPKNS1_4TypeEPSt6vectorIS5_SaIS5_EE
	.section	.text.unlikely._ZN2v88internal6torque12_GLOBAL__N_118AppendLoweredTypesEPKNS1_4TypeEPSt6vectorIS5_SaIS5_EE
	.size	_ZN2v88internal6torque12_GLOBAL__N_118AppendLoweredTypesEPKNS1_4TypeEPSt6vectorIS5_SaIS5_EE.cold, .-_ZN2v88internal6torque12_GLOBAL__N_118AppendLoweredTypesEPKNS1_4TypeEPSt6vectorIS5_SaIS5_EE.cold
.LCOLDE88:
	.section	.text._ZN2v88internal6torque12_GLOBAL__N_118AppendLoweredTypesEPKNS1_4TypeEPSt6vectorIS5_SaIS5_EE
.LHOTE88:
	.section	.text.unlikely._ZN2v88internal6torque9LowerTypeEPKNS1_4TypeE,"ax",@progbits
.LCOLDB89:
	.section	.text._ZN2v88internal6torque9LowerTypeEPKNS1_4TypeE,"ax",@progbits
.LHOTB89:
	.p2align 4
	.globl	_ZN2v88internal6torque9LowerTypeEPKNS1_4TypeE
	.type	_ZN2v88internal6torque9LowerTypeEPKNS1_4TypeE, @function
_ZN2v88internal6torque9LowerTypeEPKNS1_4TypeE:
.LFB6625:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6625
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	%rsi, %rdi
	movq	$0, 16(%r12)
	movq	%r12, %rsi
	movups	%xmm0, (%r12)
.LEHB277:
	call	_ZN2v88internal6torque12_GLOBAL__N_118AppendLoweredTypesEPKNS1_4TypeEPSt6vectorIS5_SaIS5_EE
.LEHE277:
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2183:
	.cfi_restore_state
	endbr64
	movq	%rax, %r13
	jmp	.L2181
	.section	.gcc_except_table._ZN2v88internal6torque9LowerTypeEPKNS1_4TypeE,"a",@progbits
.LLSDA6625:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6625-.LLSDACSB6625
.LLSDACSB6625:
	.uleb128 .LEHB277-.LFB6625
	.uleb128 .LEHE277-.LEHB277
	.uleb128 .L2183-.LFB6625
	.uleb128 0
.LLSDACSE6625:
	.section	.text._ZN2v88internal6torque9LowerTypeEPKNS1_4TypeE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque9LowerTypeEPKNS1_4TypeE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6625
	.type	_ZN2v88internal6torque9LowerTypeEPKNS1_4TypeE.cold, @function
_ZN2v88internal6torque9LowerTypeEPKNS1_4TypeE.cold:
.LFSB6625:
.L2181:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2182
	call	_ZdlPv@PLT
.L2182:
	movq	%r13, %rdi
.LEHB278:
	call	_Unwind_Resume@PLT
.LEHE278:
	.cfi_endproc
.LFE6625:
	.section	.gcc_except_table._ZN2v88internal6torque9LowerTypeEPKNS1_4TypeE
.LLSDAC6625:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6625-.LLSDACSBC6625
.LLSDACSBC6625:
	.uleb128 .LEHB278-.LCOLDB89
	.uleb128 .LEHE278-.LEHB278
	.uleb128 0
	.uleb128 0
.LLSDACSEC6625:
	.section	.text.unlikely._ZN2v88internal6torque9LowerTypeEPKNS1_4TypeE
	.section	.text._ZN2v88internal6torque9LowerTypeEPKNS1_4TypeE
	.size	_ZN2v88internal6torque9LowerTypeEPKNS1_4TypeE, .-_ZN2v88internal6torque9LowerTypeEPKNS1_4TypeE
	.section	.text.unlikely._ZN2v88internal6torque9LowerTypeEPKNS1_4TypeE
	.size	_ZN2v88internal6torque9LowerTypeEPKNS1_4TypeE.cold, .-_ZN2v88internal6torque9LowerTypeEPKNS1_4TypeE.cold
.LCOLDE89:
	.section	.text._ZN2v88internal6torque9LowerTypeEPKNS1_4TypeE
.LHOTE89:
	.section	.rodata._ZN2v88internal6torque18ProjectStructFieldENS1_11VisitResultERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.str1.1,"aMS",@progbits,1
.LC90:
	.string	"'"
.LC91:
	.string	"' doesn't contain a field '"
.LC92:
	.string	"struct '"
	.section	.text.unlikely._ZN2v88internal6torque18ProjectStructFieldENS1_11VisitResultERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"ax",@progbits
.LCOLDB93:
	.section	.text._ZN2v88internal6torque18ProjectStructFieldENS1_11VisitResultERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"ax",@progbits
.LHOTB93:
	.p2align 4
	.globl	_ZN2v88internal6torque18ProjectStructFieldENS1_11VisitResultERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN2v88internal6torque18ProjectStructFieldENS1_11VisitResultERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN2v88internal6torque18ProjectStructFieldENS1_11VisitResultERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB6623:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6623
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -96(%rbp)
	movq	(%rsi), %rdi
	movq	56(%rsi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 72(%rdi)
	movq	%rdi, -104(%rbp)
	jne	.L2190
	movq	(%rdi), %rax
.LEHB279:
	call	*96(%rax)
.LEHE279:
.L2190:
	movq	-104(%rbp), %rax
	movq	80(%rax), %rbx
	movq	88(%rax), %r15
	cmpq	%r15, %rbx
	je	.L2191
	leaq	-80(%rbp), %r14
	jmp	.L2198
	.p2align 4,,10
	.p2align 3
.L2196:
	addq	$104, %rbx
	cmpq	%rbx, %r15
	je	.L2191
	movq	%rcx, %r12
.L2198:
	movq	80(%rbx), %rdi
	pxor	%xmm1, %xmm1
	movq	%r14, %rsi
	movq	$0, -64(%rbp)
	movaps	%xmm1, -80(%rbp)
.LEHB280:
	call	_ZN2v88internal6torque12_GLOBAL__N_118AppendLoweredTypesEPKNS1_4TypeEPSt6vectorIS5_SaIS5_EE
.LEHE280:
	movq	-80(%rbp), %rdi
	movq	-72(%rbp), %rcx
	subq	%rdi, %rcx
	sarq	$3, %rcx
	testq	%rdi, %rdi
	je	.L2195
	movq	%rcx, -88(%rbp)
	call	_ZdlPv@PLT
	movq	-88(%rbp), %rcx
.L2195:
	movq	56(%rbx), %rdx
	addq	%r12, %rcx
	cmpq	8(%r13), %rdx
	jne	.L2196
	testq	%rdx, %rdx
	je	.L2197
	movq	48(%rbx), %rdi
	movq	0(%r13), %rsi
	movq	%rcx, -88(%rbp)
	call	memcmp@PLT
	movq	-88(%rbp), %rcx
	testl	%eax, %eax
	jne	.L2196
.L2197:
	movq	80(%rbx), %rax
	movq	-96(%rbp), %rsi
	movq	%r12, %xmm0
	movq	%rcx, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movq	%rax, (%rsi)
	movb	$0, 8(%rsi)
	movb	$0, 16(%rsi)
	movb	$1, 48(%rsi)
	movups	%xmm0, 56(%rsi)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2214
	addq	$72, %rsp
	movq	%rsi, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2191:
	.cfi_restore_state
	movq	-104(%rbp), %rsi
	leaq	.LC90(%rip), %r8
	movq	%r13, %rcx
	leaq	.LC91(%rip), %rdx
	leaq	.LC92(%rip), %rdi
	addq	$112, %rsi
.LEHB281:
	call	_ZN2v88internal6torque11ReportErrorIJRA9_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA28_S3_SD_RA2_S3_EEEvDpOT_
.LEHE281:
.L2214:
	call	__stack_chk_fail@PLT
.L2201:
	endbr64
	movq	%rax, %r12
	jmp	.L2193
	.section	.gcc_except_table._ZN2v88internal6torque18ProjectStructFieldENS1_11VisitResultERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"a",@progbits
.LLSDA6623:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6623-.LLSDACSB6623
.LLSDACSB6623:
	.uleb128 .LEHB279-.LFB6623
	.uleb128 .LEHE279-.LEHB279
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB280-.LFB6623
	.uleb128 .LEHE280-.LEHB280
	.uleb128 .L2201-.LFB6623
	.uleb128 0
	.uleb128 .LEHB281-.LFB6623
	.uleb128 .LEHE281-.LEHB281
	.uleb128 0
	.uleb128 0
.LLSDACSE6623:
	.section	.text._ZN2v88internal6torque18ProjectStructFieldENS1_11VisitResultERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque18ProjectStructFieldENS1_11VisitResultERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6623
	.type	_ZN2v88internal6torque18ProjectStructFieldENS1_11VisitResultERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.cold, @function
_ZN2v88internal6torque18ProjectStructFieldENS1_11VisitResultERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.cold:
.LFSB6623:
.L2193:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2194
	call	_ZdlPv@PLT
.L2194:
	movq	%r12, %rdi
.LEHB282:
	call	_Unwind_Resume@PLT
.LEHE282:
	.cfi_endproc
.LFE6623:
	.section	.gcc_except_table._ZN2v88internal6torque18ProjectStructFieldENS1_11VisitResultERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
.LLSDAC6623:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6623-.LLSDACSBC6623
.LLSDACSBC6623:
	.uleb128 .LEHB282-.LCOLDB93
	.uleb128 .LEHE282-.LEHB282
	.uleb128 0
	.uleb128 0
.LLSDACSEC6623:
	.section	.text.unlikely._ZN2v88internal6torque18ProjectStructFieldENS1_11VisitResultERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.text._ZN2v88internal6torque18ProjectStructFieldENS1_11VisitResultERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.size	_ZN2v88internal6torque18ProjectStructFieldENS1_11VisitResultERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN2v88internal6torque18ProjectStructFieldENS1_11VisitResultERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.text.unlikely._ZN2v88internal6torque18ProjectStructFieldENS1_11VisitResultERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.size	_ZN2v88internal6torque18ProjectStructFieldENS1_11VisitResultERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.cold, .-_ZN2v88internal6torque18ProjectStructFieldENS1_11VisitResultERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.cold
.LCOLDE93:
	.section	.text._ZN2v88internal6torque18ProjectStructFieldENS1_11VisitResultERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
.LHOTE93:
	.section	.text.unlikely._ZN2v88internal6torque16LoweredSlotCountEPKNS1_4TypeE,"ax",@progbits
.LCOLDB94:
	.section	.text._ZN2v88internal6torque16LoweredSlotCountEPKNS1_4TypeE,"ax",@progbits
.LHOTB94:
	.p2align 4
	.globl	_ZN2v88internal6torque16LoweredSlotCountEPKNS1_4TypeE
	.type	_ZN2v88internal6torque16LoweredSlotCountEPKNS1_4TypeE, @function
_ZN2v88internal6torque16LoweredSlotCountEPKNS1_4TypeE:
.LFB6626:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6626
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	leaq	-48(%rbp), %rsi
	subq	$40, %rsp
	.cfi_offset 12, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	$0, -32(%rbp)
	movaps	%xmm0, -48(%rbp)
.LEHB283:
	call	_ZN2v88internal6torque12_GLOBAL__N_118AppendLoweredTypesEPKNS1_4TypeEPSt6vectorIS5_SaIS5_EE
.LEHE283:
	movq	-48(%rbp), %rdi
	movq	-40(%rbp), %rax
	subq	%rdi, %rax
	sarq	$3, %rax
	movq	%rax, %r12
	testq	%rdi, %rdi
	je	.L2215
	call	_ZdlPv@PLT
.L2215:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2230
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2230:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
.L2221:
	endbr64
	movq	%rax, %r12
	jmp	.L2217
	.section	.gcc_except_table._ZN2v88internal6torque16LoweredSlotCountEPKNS1_4TypeE,"a",@progbits
.LLSDA6626:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6626-.LLSDACSB6626
.LLSDACSB6626:
	.uleb128 .LEHB283-.LFB6626
	.uleb128 .LEHE283-.LEHB283
	.uleb128 .L2221-.LFB6626
	.uleb128 0
.LLSDACSE6626:
	.section	.text._ZN2v88internal6torque16LoweredSlotCountEPKNS1_4TypeE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque16LoweredSlotCountEPKNS1_4TypeE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6626
	.type	_ZN2v88internal6torque16LoweredSlotCountEPKNS1_4TypeE.cold, @function
_ZN2v88internal6torque16LoweredSlotCountEPKNS1_4TypeE.cold:
.LFSB6626:
.L2217:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2218
	call	_ZdlPv@PLT
.L2218:
	movq	%r12, %rdi
.LEHB284:
	call	_Unwind_Resume@PLT
.LEHE284:
	.cfi_endproc
.LFE6626:
	.section	.gcc_except_table._ZN2v88internal6torque16LoweredSlotCountEPKNS1_4TypeE
.LLSDAC6626:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6626-.LLSDACSBC6626
.LLSDACSBC6626:
	.uleb128 .LEHB284-.LCOLDB94
	.uleb128 .LEHE284-.LEHB284
	.uleb128 0
	.uleb128 0
.LLSDACSEC6626:
	.section	.text.unlikely._ZN2v88internal6torque16LoweredSlotCountEPKNS1_4TypeE
	.section	.text._ZN2v88internal6torque16LoweredSlotCountEPKNS1_4TypeE
	.size	_ZN2v88internal6torque16LoweredSlotCountEPKNS1_4TypeE, .-_ZN2v88internal6torque16LoweredSlotCountEPKNS1_4TypeE
	.section	.text.unlikely._ZN2v88internal6torque16LoweredSlotCountEPKNS1_4TypeE
	.size	_ZN2v88internal6torque16LoweredSlotCountEPKNS1_4TypeE.cold, .-_ZN2v88internal6torque16LoweredSlotCountEPKNS1_4TypeE.cold
.LCOLDE94:
	.section	.text._ZN2v88internal6torque16LoweredSlotCountEPKNS1_4TypeE
.LHOTE94:
	.section	.text.unlikely._ZN2v88internal6torque19LowerParameterTypesERKNS1_14ParameterTypesEm,"ax",@progbits
.LCOLDB95:
	.section	.text._ZN2v88internal6torque19LowerParameterTypesERKNS1_14ParameterTypesEm,"ax",@progbits
.LHOTB95:
	.p2align 4
	.globl	_ZN2v88internal6torque19LowerParameterTypesERKNS1_14ParameterTypesEm
	.type	_ZN2v88internal6torque19LowerParameterTypesERKNS1_14ParameterTypesEm, @function
_ZN2v88internal6torque19LowerParameterTypesERKNS1_14ParameterTypesEm:
.LFB6628:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6628
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	%rdx, -104(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, 16(%rdi)
	movups	%xmm0, (%rdi)
	movq	(%rsi), %rbx
	movq	8(%rsi), %r14
	cmpq	%r14, %rbx
	je	.L2245
	movq	%rsi, %r15
	.p2align 4,,10
	.p2align 3
.L2233:
	movq	(%rbx), %rdi
	movq	%r12, %rsi
.LEHB285:
	call	_ZN2v88internal6torque12_GLOBAL__N_118AppendLoweredTypesEPKNS1_4TypeEPSt6vectorIS5_SaIS5_EE
.LEHE285:
	addq	$8, %rbx
	cmpq	%rbx, %r14
	jne	.L2233
	movq	(%r15), %r14
	movq	8(%r15), %rbx
.L2232:
	subq	%r14, %rbx
	leaq	-96(%rbp), %r15
	sarq	$3, %rbx
	cmpq	%rbx, -104(%rbp)
	jbe	.L2231
	.p2align 4,,10
	.p2align 3
.L2236:
.LEHB286:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_10TypeOracleEEERPNT_5ScopeEv@PLT
.LEHE286:
	leaq	-80(%rbp), %r14
	movl	$29795, %eax
	movq	%r15, %rdi
	movb	$0, -74(%rbp)
	movq	%r14, -96(%rbp)
	movl	$1701470799, -80(%rbp)
	movw	%ax, -76(%rbp)
	movq	$6, -88(%rbp)
.LEHB287:
	call	_ZN2v88internal6torque12Declarations16LookupGlobalTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
.LEHE287:
	movq	-96(%rbp), %rdi
	movq	%rax, %r13
	cmpq	%r14, %rdi
	je	.L2237
	call	_ZdlPv@PLT
.L2237:
	movq	%r12, %rsi
	movq	%r13, %rdi
.LEHB288:
	call	_ZN2v88internal6torque12_GLOBAL__N_118AppendLoweredTypesEPKNS1_4TypeEPSt6vectorIS5_SaIS5_EE
.LEHE288:
	addq	$1, %rbx
	cmpq	%rbx, -104(%rbp)
	ja	.L2236
.L2231:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2259
	addq	$72, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2245:
	.cfi_restore_state
	movq	%r14, %rbx
	jmp	.L2232
.L2259:
	call	__stack_chk_fail@PLT
.L2246:
	endbr64
	movq	%rax, %r13
	jmp	.L2241
.L2247:
	endbr64
	movq	%rax, %r13
	jmp	.L2234
.L2248:
	endbr64
	movq	%rax, %r13
	jmp	.L2239
	.section	.gcc_except_table._ZN2v88internal6torque19LowerParameterTypesERKNS1_14ParameterTypesEm,"a",@progbits
.LLSDA6628:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6628-.LLSDACSB6628
.LLSDACSB6628:
	.uleb128 .LEHB285-.LFB6628
	.uleb128 .LEHE285-.LEHB285
	.uleb128 .L2247-.LFB6628
	.uleb128 0
	.uleb128 .LEHB286-.LFB6628
	.uleb128 .LEHE286-.LEHB286
	.uleb128 .L2246-.LFB6628
	.uleb128 0
	.uleb128 .LEHB287-.LFB6628
	.uleb128 .LEHE287-.LEHB287
	.uleb128 .L2248-.LFB6628
	.uleb128 0
	.uleb128 .LEHB288-.LFB6628
	.uleb128 .LEHE288-.LEHB288
	.uleb128 .L2246-.LFB6628
	.uleb128 0
.LLSDACSE6628:
	.section	.text._ZN2v88internal6torque19LowerParameterTypesERKNS1_14ParameterTypesEm
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque19LowerParameterTypesERKNS1_14ParameterTypesEm
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6628
	.type	_ZN2v88internal6torque19LowerParameterTypesERKNS1_14ParameterTypesEm.cold, @function
_ZN2v88internal6torque19LowerParameterTypesERKNS1_14ParameterTypesEm.cold:
.LFSB6628:
.L2239:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L2241
	call	_ZdlPv@PLT
.L2241:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2243
	call	_ZdlPv@PLT
.L2243:
	movq	%r13, %rdi
.LEHB289:
	call	_Unwind_Resume@PLT
.L2234:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2235
	call	_ZdlPv@PLT
.L2235:
	movq	%r13, %rdi
	call	_Unwind_Resume@PLT
.LEHE289:
	.cfi_endproc
.LFE6628:
	.section	.gcc_except_table._ZN2v88internal6torque19LowerParameterTypesERKNS1_14ParameterTypesEm
.LLSDAC6628:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6628-.LLSDACSBC6628
.LLSDACSBC6628:
	.uleb128 .LEHB289-.LCOLDB95
	.uleb128 .LEHE289-.LEHB289
	.uleb128 0
	.uleb128 0
.LLSDACSEC6628:
	.section	.text.unlikely._ZN2v88internal6torque19LowerParameterTypesERKNS1_14ParameterTypesEm
	.section	.text._ZN2v88internal6torque19LowerParameterTypesERKNS1_14ParameterTypesEm
	.size	_ZN2v88internal6torque19LowerParameterTypesERKNS1_14ParameterTypesEm, .-_ZN2v88internal6torque19LowerParameterTypesERKNS1_14ParameterTypesEm
	.section	.text.unlikely._ZN2v88internal6torque19LowerParameterTypesERKNS1_14ParameterTypesEm
	.size	_ZN2v88internal6torque19LowerParameterTypesERKNS1_14ParameterTypesEm.cold, .-_ZN2v88internal6torque19LowerParameterTypesERKNS1_14ParameterTypesEm.cold
.LCOLDE95:
	.section	.text._ZN2v88internal6torque19LowerParameterTypesERKNS1_14ParameterTypesEm
.LHOTE95:
	.section	.text.unlikely._ZN2v88internal6torque19LowerParameterTypesERKSt6vectorIPKNS1_4TypeESaIS5_EE,"ax",@progbits
.LCOLDB96:
	.section	.text._ZN2v88internal6torque19LowerParameterTypesERKSt6vectorIPKNS1_4TypeESaIS5_EE,"ax",@progbits
.LHOTB96:
	.p2align 4
	.globl	_ZN2v88internal6torque19LowerParameterTypesERKSt6vectorIPKNS1_4TypeESaIS5_EE
	.type	_ZN2v88internal6torque19LowerParameterTypesERKSt6vectorIPKNS1_4TypeESaIS5_EE, @function
_ZN2v88internal6torque19LowerParameterTypesERKSt6vectorIPKNS1_4TypeESaIS5_EE:
.LFB6627:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6627
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, 16(%rdi)
	movups	%xmm0, (%rdi)
	movq	(%rsi), %rbx
	movq	8(%rsi), %r15
	cmpq	%r15, %rbx
	je	.L2260
	leaq	-96(%rbp), %rax
	movq	%rax, -120(%rbp)
	.p2align 4,,10
	.p2align 3
.L2275:
	movq	(%rbx), %rdi
	movq	%rdi, -104(%rbp)
	movq	(%rdi), %rax
.LEHB290:
	call	*32(%rax)
	testb	%al, %al
	jne	.L2263
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_10TypeOracleEEERPNT_5ScopeEv@PLT
.LEHE290:
	movq	-120(%rbp), %rdi
	leaq	-80(%rbp), %r12
	movb	$0, -76(%rbp)
	movq	%r12, -96(%rbp)
	movl	$1684631414, -80(%rbp)
	movq	$4, -88(%rbp)
.LEHB291:
	call	_ZN2v88internal6torque12Declarations16LookupGlobalTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
.LEHE291:
	movq	-96(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L2264
	movq	%rax, -128(%rbp)
	call	_ZdlPv@PLT
	movq	-128(%rbp), %rax
.L2264:
	movq	-104(%rbp), %r13
	cmpq	%rax, %r13
	je	.L2263
	testq	%r13, %r13
	je	.L2270
	cmpl	$4, 8(%r13)
	jne	.L2270
	cmpb	$0, 72(%r13)
	jne	.L2273
	movq	0(%r13), %rax
	movq	%r13, %rdi
.LEHB292:
	call	*96(%rax)
.L2273:
	movq	80(%r13), %r12
	movq	88(%r13), %r13
	cmpq	%r13, %r12
	je	.L2263
.L2272:
	movq	80(%r12), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal6torque12_GLOBAL__N_118AppendLoweredTypesEPKNS1_4TypeEPSt6vectorIS5_SaIS5_EE
	addq	$104, %r12
	cmpq	%r12, %r13
	jne	.L2272
.L2263:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	jne	.L2275
.L2260:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2291
	addq	$88, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2270:
	.cfi_restore_state
	movq	8(%r14), %rsi
	cmpq	16(%r14), %rsi
	je	.L2274
	addq	$8, %rbx
	movq	%r13, (%rsi)
	addq	$8, 8(%r14)
	cmpq	%rbx, %r15
	jne	.L2275
	jmp	.L2260
	.p2align 4,,10
	.p2align 3
.L2274:
	leaq	-104(%rbp), %rdx
	movq	%r14, %rdi
	call	_ZNSt6vectorIPKN2v88internal6torque4TypeESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
.LEHE292:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	jne	.L2275
	jmp	.L2260
.L2291:
	call	__stack_chk_fail@PLT
.L2279:
	endbr64
	movq	%rax, %rbx
	jmp	.L2267
.L2278:
	endbr64
	movq	%rax, %r12
	jmp	.L2269
	.section	.gcc_except_table._ZN2v88internal6torque19LowerParameterTypesERKSt6vectorIPKNS1_4TypeESaIS5_EE,"a",@progbits
.LLSDA6627:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6627-.LLSDACSB6627
.LLSDACSB6627:
	.uleb128 .LEHB290-.LFB6627
	.uleb128 .LEHE290-.LEHB290
	.uleb128 .L2278-.LFB6627
	.uleb128 0
	.uleb128 .LEHB291-.LFB6627
	.uleb128 .LEHE291-.LEHB291
	.uleb128 .L2279-.LFB6627
	.uleb128 0
	.uleb128 .LEHB292-.LFB6627
	.uleb128 .LEHE292-.LEHB292
	.uleb128 .L2278-.LFB6627
	.uleb128 0
.LLSDACSE6627:
	.section	.text._ZN2v88internal6torque19LowerParameterTypesERKSt6vectorIPKNS1_4TypeESaIS5_EE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque19LowerParameterTypesERKSt6vectorIPKNS1_4TypeESaIS5_EE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6627
	.type	_ZN2v88internal6torque19LowerParameterTypesERKSt6vectorIPKNS1_4TypeESaIS5_EE.cold, @function
_ZN2v88internal6torque19LowerParameterTypesERKSt6vectorIPKNS1_4TypeESaIS5_EE.cold:
.LFSB6627:
.L2267:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-96(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L2268
	call	_ZdlPv@PLT
.L2268:
	movq	%rbx, %r12
.L2269:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L2276
	call	_ZdlPv@PLT
.L2276:
	movq	%r12, %rdi
.LEHB293:
	call	_Unwind_Resume@PLT
.LEHE293:
	.cfi_endproc
.LFE6627:
	.section	.gcc_except_table._ZN2v88internal6torque19LowerParameterTypesERKSt6vectorIPKNS1_4TypeESaIS5_EE
.LLSDAC6627:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6627-.LLSDACSBC6627
.LLSDACSBC6627:
	.uleb128 .LEHB293-.LCOLDB96
	.uleb128 .LEHE293-.LEHB293
	.uleb128 0
	.uleb128 0
.LLSDACSEC6627:
	.section	.text.unlikely._ZN2v88internal6torque19LowerParameterTypesERKSt6vectorIPKNS1_4TypeESaIS5_EE
	.section	.text._ZN2v88internal6torque19LowerParameterTypesERKSt6vectorIPKNS1_4TypeESaIS5_EE
	.size	_ZN2v88internal6torque19LowerParameterTypesERKSt6vectorIPKNS1_4TypeESaIS5_EE, .-_ZN2v88internal6torque19LowerParameterTypesERKSt6vectorIPKNS1_4TypeESaIS5_EE
	.section	.text.unlikely._ZN2v88internal6torque19LowerParameterTypesERKSt6vectorIPKNS1_4TypeESaIS5_EE
	.size	_ZN2v88internal6torque19LowerParameterTypesERKSt6vectorIPKNS1_4TypeESaIS5_EE.cold, .-_ZN2v88internal6torque19LowerParameterTypesERKSt6vectorIPKNS1_4TypeESaIS5_EE.cold
.LCOLDE96:
	.section	.text._ZN2v88internal6torque19LowerParameterTypesERKSt6vectorIPKNS1_4TypeESaIS5_EE
.LHOTE96:
	.section	.text._ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE24_M_get_insert_unique_posERKS5_,"axG",@progbits,_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE24_M_get_insert_unique_posERKS5_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE24_M_get_insert_unique_posERKS5_
	.type	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE24_M_get_insert_unique_posERKS5_, @function
_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE24_M_get_insert_unique_posERKS5_:
.LFB9420:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r15
	movq	%rdi, -72(%rbp)
	movq	%rsi, -64(%rbp)
	testq	%r15, %r15
	je	.L2318
	movq	8(%rsi), %r14
	movq	(%rsi), %rbx
	jmp	.L2295
	.p2align 4,,10
	.p2align 3
.L2300:
	movq	16(%r15), %rax
	movl	$1, %esi
	testq	%rax, %rax
	je	.L2296
.L2319:
	movq	%rax, %r15
.L2295:
	movq	40(%r15), %r12
	movq	32(%r15), %r13
	cmpq	%r12, %r14
	movq	%r12, %rdx
	cmovbe	%r14, %rdx
	testq	%rdx, %rdx
	je	.L2297
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movq	%rdx, -56(%rbp)
	call	memcmp@PLT
	movq	-56(%rbp), %rdx
	testl	%eax, %eax
	jne	.L2298
.L2297:
	movq	%r14, %rax
	movl	$2147483648, %ecx
	subq	%r12, %rax
	cmpq	%rcx, %rax
	jge	.L2299
	movabsq	$-2147483649, %rdi
	cmpq	%rdi, %rax
	jle	.L2300
.L2298:
	testl	%eax, %eax
	js	.L2300
.L2299:
	movq	24(%r15), %rax
	xorl	%esi, %esi
	testq	%rax, %rax
	jne	.L2319
.L2296:
	movq	%r15, %r8
	testb	%sil, %sil
	jne	.L2294
.L2302:
	testq	%rdx, %rdx
	je	.L2305
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%r8, -56(%rbp)
	call	memcmp@PLT
	movq	-56(%rbp), %r8
	testl	%eax, %eax
	jne	.L2306
.L2305:
	movq	%r12, %rcx
	subq	%r14, %rcx
	cmpq	$2147483647, %rcx
	jg	.L2307
	cmpq	$-2147483648, %rcx
	jl	.L2308
	movl	%ecx, %eax
.L2306:
	testl	%eax, %eax
	js	.L2308
.L2307:
	addq	$40, %rsp
	movq	%r15, %rax
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2308:
	.cfi_restore_state
	addq	$40, %rsp
	xorl	%eax, %eax
	movq	%r8, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2318:
	.cfi_restore_state
	leaq	8(%rdi), %r15
.L2294:
	movq	-72(%rbp), %rax
	cmpq	24(%rax), %r15
	je	.L2320
	movq	%r15, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	-64(%rbp), %rbx
	movq	%r15, %r8
	movq	40(%rax), %r12
	movq	32(%rax), %r13
	movq	%rax, %r15
	movq	8(%rbx), %r14
	movq	(%rbx), %rbx
	cmpq	%r14, %r12
	movq	%r14, %rdx
	cmovbe	%r12, %rdx
	jmp	.L2302
	.p2align 4,,10
	.p2align 3
.L2320:
	addq	$40, %rsp
	movq	%r15, %rdx
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9420:
	.size	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE24_M_get_insert_unique_posERKS5_, .-_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE24_M_get_insert_unique_posERKS5_
	.section	.rodata._ZNK2v88internal6torque9UnionType29GetGeneratedTNodeTypeNameImplB5cxx11Ev.str1.1,"aMS",@progbits,1
.LC97:
	.string	"Numeric"
	.section	.text.unlikely._ZNK2v88internal6torque9UnionType29GetGeneratedTNodeTypeNameImplB5cxx11Ev,"ax",@progbits
	.align 2
.LCOLDB98:
	.section	.text._ZNK2v88internal6torque9UnionType29GetGeneratedTNodeTypeNameImplB5cxx11Ev,"ax",@progbits
.LHOTB98:
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal6torque9UnionType29GetGeneratedTNodeTypeNameImplB5cxx11Ev
	.type	_ZNK2v88internal6torque9UnionType29GetGeneratedTNodeTypeNameImplB5cxx11Ev, @function
_ZNK2v88internal6torque9UnionType29GetGeneratedTNodeTypeNameImplB5cxx11Ev:
.LFB6522:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6522
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$328, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -344(%rbp)
	movq	%rsi, -368(%rbp)
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	cmpq	$3, 112(%rsi)
	ja	.L2322
	leaq	-296(%rbp), %rcx
	movq	%rsi, %rax
	movq	96(%rsi), %r12
	movl	$0, -296(%rbp)
	movq	%rcx, -320(%rbp)
	addq	$80, %rax
	leaq	-304(%rbp), %r15
	leaq	-144(%rbp), %r14
	movq	%rcx, -280(%rbp)
	movq	%rcx, -272(%rbp)
	leaq	-160(%rbp), %rcx
	movq	$0, -288(%rbp)
	movq	$0, -264(%rbp)
	movq	%rax, -336(%rbp)
	movq	%rcx, -312(%rbp)
	cmpq	%rax, %r12
	je	.L2478
	.p2align 4,,10
	.p2align 3
.L2323:
	movq	-312(%rbp), %rbx
	movq	32(%r12), %rsi
	movq	%rbx, %rdi
.LEHB294:
	call	_ZNK2v88internal6torque4Type25GetGeneratedTNodeTypeNameB5cxx11Ev
.LEHE294:
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE24_M_get_insert_unique_posERKS5_
	movq	%rdx, %r13
	testq	%rdx, %rdx
	je	.L2479
	testq	%rax, %rax
	setne	%bl
	cmpq	-320(%rbp), %rdx
	sete	%dil
	orb	%dil, %bl
	je	.L2480
.L2326:
	movl	$64, %edi
.LEHB295:
	call	_Znwm@PLT
.LEHE295:
	movq	%rax, %rsi
	leaq	48(%rax), %rax
	movq	%r14, -328(%rbp)
	movq	%rax, 32(%rsi)
	movq	-160(%rbp), %rax
	cmpq	%r14, %rax
	je	.L2481
	movq	%rax, 32(%rsi)
	movq	-144(%rbp), %rax
	movq	%rax, 48(%rsi)
.L2330:
	movq	-152(%rbp), %rax
	movq	-320(%rbp), %rcx
	movzbl	%bl, %edi
	movq	%r13, %rdx
	movq	%r14, -160(%rbp)
	movq	%rax, 40(%rsi)
	movq	$0, -152(%rbp)
	movb	$0, -144(%rbp)
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, -264(%rbp)
.L2325:
	movq	-160(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L2331
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r12
	cmpq	%rax, -336(%rbp)
	jne	.L2323
.L2333:
	movq	-328(%rbp), %rax
	movl	$27987, %edi
	leaq	-248(%rbp), %r14
	movl	$29285, %r8d
	movw	%di, -144(%rbp)
	movq	-312(%rbp), %rbx
	leaq	-256(%rbp), %r15
	movq	%rax, -160(%rbp)
	leaq	-112(%rbp), %rax
	movb	$105, -142(%rbp)
	movq	$3, -152(%rbp)
	movb	$0, -141(%rbp)
	movw	%r8w, -104(%rbp)
	movq	$10, -120(%rbp)
	movb	$0, -102(%rbp)
	movl	$0, -248(%rbp)
	movq	$0, -240(%rbp)
	movq	%r14, -232(%rbp)
	movq	%r14, -224(%rbp)
	movq	$0, -216(%rbp)
	movq	%rax, -360(%rbp)
	movq	%rax, -128(%rbp)
	movabsq	$7092453967931729224, %rax
	movq	%rax, -112(%rbp)
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L2324:
	testq	%rax, %rax
	je	.L2334
	movq	-224(%rbp), %r12
	movq	8(%rbx), %rcx
	movq	40(%r12), %r13
	movq	%rcx, %rdx
	cmpq	%rcx, %r13
	cmovbe	%r13, %rdx
	testq	%rdx, %rdx
	je	.L2335
	movq	32(%r12), %rdi
	movq	(%rbx), %rsi
	movq	%rcx, -336(%rbp)
	call	memcmp@PLT
	movq	-336(%rbp), %rcx
	testl	%eax, %eax
	jne	.L2336
.L2335:
	movq	%r13, %rax
	subq	%rcx, %rax
	movl	$2147483648, %ecx
	cmpq	%rcx, %rax
	jge	.L2334
	movabsq	$-2147483649, %rcx
	cmpq	%rcx, %rax
	jle	.L2337
.L2336:
	testl	%eax, %eax
	js	.L2337
	.p2align 4,,10
	.p2align 3
.L2334:
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE24_M_get_insert_unique_posERKS5_
	movq	%rdx, %r12
	testq	%rdx, %rdx
	jne	.L2339
	movq	-216(%rbp), %rax
.L2340:
	addq	$32, %rbx
	leaq	-96(%rbp), %rdx
	cmpq	%rdx, %rbx
	jne	.L2324
	cmpq	%rax, -264(%rbp)
	je	.L2348
.L2354:
	movb	$0, -352(%rbp)
.L2349:
	movq	-240(%rbp), %r12
	leaq	-256(%rbp), %rbx
	testq	%r12, %r12
	je	.L2361
.L2357:
	movq	24(%r12), %rsi
	movq	%rbx, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r12), %rdi
	leaq	48(%r12), %rax
	movq	16(%r12), %r13
	cmpq	%rax, %rdi
	je	.L2360
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	je	.L2361
.L2362:
	movq	%r13, %r12
	jmp	.L2357
	.p2align 4,,10
	.p2align 3
.L2331:
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r12
	cmpq	-336(%rbp), %rax
	jne	.L2323
	jmp	.L2333
	.p2align 4,,10
	.p2align 3
.L2479:
	movq	%r14, -328(%rbp)
	jmp	.L2325
	.p2align 4,,10
	.p2align 3
.L2339:
	testq	%rax, %rax
	setne	%dil
.L2338:
	cmpq	%r14, %r12
	sete	%al
	orb	%dil, %al
	movb	%al, -336(%rbp)
	je	.L2482
.L2341:
	movl	$64, %edi
.LEHB296:
	call	_Znwm@PLT
.LEHE296:
	movq	%rax, %r13
	leaq	32(%rax), %rdi
	leaq	48(%rax), %rax
	movq	(%rbx), %rsi
	movq	8(%rbx), %rdx
	movq	%rax, 32(%r13)
	addq	%rsi, %rdx
.LEHB297:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag.constprop.0
.LEHE297:
	movzbl	-336(%rbp), %edi
	movq	%r14, %rcx
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	movq	-216(%rbp), %rax
	addq	$1, %rax
	movq	%rax, -216(%rbp)
	jmp	.L2340
	.p2align 4,,10
	.p2align 3
.L2360:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L2362
.L2361:
	movq	-128(%rbp), %rdi
	cmpq	-360(%rbp), %rdi
	je	.L2359
	call	_ZdlPv@PLT
.L2359:
	movq	-160(%rbp), %rdi
	cmpq	-328(%rbp), %rdi
	je	.L2363
	call	_ZdlPv@PLT
.L2363:
	cmpb	$0, -352(%rbp)
	je	.L2364
	movq	-344(%rbp), %rcx
	movl	$29285, %esi
	leaq	16(%rcx), %rax
	movl	$1651340622, 16(%rcx)
	movq	%rax, (%rcx)
	movw	%si, 20(%rcx)
	movq	$6, 8(%rcx)
	movb	$0, 22(%rcx)
.L2365:
	movq	-288(%rbp), %r12
	leaq	-304(%rbp), %r15
	testq	%r12, %r12
	je	.L2321
.L2395:
	movq	24(%r12), %rsi
	movq	%r15, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r12), %rdi
	leaq	48(%r12), %rax
	movq	16(%r12), %rbx
	cmpq	%rax, %rdi
	je	.L2397
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L2321
.L2399:
	movq	%rbx, %r12
	jmp	.L2395
	.p2align 4,,10
	.p2align 3
.L2481:
	movdqa	-144(%rbp), %xmm0
	movups	%xmm0, 48(%rsi)
	jmp	.L2330
	.p2align 4,,10
	.p2align 3
.L2482:
	movq	8(%rbx), %r13
	movq	40(%r12), %rcx
	cmpq	%rcx, %r13
	movq	%rcx, %rdx
	cmovbe	%r13, %rdx
	testq	%rdx, %rdx
	je	.L2342
	movq	32(%r12), %rsi
	movq	(%rbx), %rdi
	movq	%rcx, -352(%rbp)
	call	memcmp@PLT
	movq	-352(%rbp), %rcx
	testl	%eax, %eax
	jne	.L2343
.L2342:
	movq	%r13, %rax
	subq	%rcx, %rax
	movl	$2147483648, %ecx
	cmpq	%rcx, %rax
	jge	.L2341
	movabsq	$-2147483649, %rcx
	cmpq	%rcx, %rax
	jle	.L2405
.L2343:
	shrl	$31, %eax
	movl	%eax, -336(%rbp)
	jmp	.L2341
	.p2align 4,,10
	.p2align 3
.L2480:
	movq	-152(%rbp), %rcx
	movq	40(%rdx), %r8
	cmpq	%r8, %rcx
	movq	%r8, %rdx
	cmovbe	%rcx, %rdx
	testq	%rdx, %rdx
	je	.L2327
	movq	32(%r13), %rsi
	movq	-160(%rbp), %rdi
	movq	%r8, -352(%rbp)
	movq	%rcx, -328(%rbp)
	call	memcmp@PLT
	movq	-328(%rbp), %rcx
	movq	-352(%rbp), %r8
	testl	%eax, %eax
	jne	.L2328
.L2327:
	movq	%rcx, %rax
	movl	$2147483648, %ecx
	subq	%r8, %rax
	cmpq	%rcx, %rax
	jge	.L2326
	movabsq	$-2147483649, %rcx
	cmpq	%rcx, %rax
	jle	.L2404
.L2328:
	shrl	$31, %eax
	movl	%eax, %ebx
	jmp	.L2326
	.p2align 4,,10
	.p2align 3
.L2337:
	xorl	%edi, %edi
	jmp	.L2338
	.p2align 4,,10
	.p2align 3
.L2364:
	movq	-328(%rbp), %rax
	movl	$29806, %ecx
	movb	$0, -102(%rbp)
	leaq	-200(%rbp), %r15
	movw	%cx, -76(%rbp)
	leaq	-64(%rbp), %rcx
	movl	$29285, %edx
	movq	-312(%rbp), %rbx
	movq	%rax, -160(%rbp)
	movl	$27987, %eax
	movw	%ax, -144(%rbp)
	movq	-360(%rbp), %rax
	movq	%rcx, -328(%rbp)
	leaq	-208(%rbp), %rcx
	movq	%rax, -128(%rbp)
	movabsq	$7092453967931729224, %rax
	movb	$105, -142(%rbp)
	movq	$3, -152(%rbp)
	movb	$0, -141(%rbp)
	movw	%dx, -104(%rbp)
	movq	$10, -120(%rbp)
	movl	$1231513922, -80(%rbp)
	movq	$6, -88(%rbp)
	movb	$0, -74(%rbp)
	movl	$0, -200(%rbp)
	movq	$0, -192(%rbp)
	movq	%r15, -184(%rbp)
	movq	%r15, -176(%rbp)
	movq	$0, -168(%rbp)
	movq	%rcx, -336(%rbp)
	movq	%rax, -112(%rbp)
	leaq	-80(%rbp), %rax
	movq	%rax, -96(%rbp)
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L2378:
	testq	%rax, %rax
	je	.L2366
	movq	-176(%rbp), %r12
	movq	8(%rbx), %r14
	movq	40(%r12), %r13
	movq	%r14, %rdx
	cmpq	%r14, %r13
	cmovbe	%r13, %rdx
	testq	%rdx, %rdx
	je	.L2367
	movq	32(%r12), %rdi
	movq	(%rbx), %rsi
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L2368
.L2367:
	movq	%r13, %rax
	movl	$2147483648, %ecx
	subq	%r14, %rax
	cmpq	%rcx, %rax
	jge	.L2366
	movabsq	$-2147483649, %rcx
	cmpq	%rcx, %rax
	jle	.L2369
.L2368:
	testl	%eax, %eax
	js	.L2369
	.p2align 4,,10
	.p2align 3
.L2366:
	movq	-336(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE24_M_get_insert_unique_posERKS5_
	movq	%rdx, %r12
	testq	%rdx, %rdx
	jne	.L2371
	movq	-168(%rbp), %rax
.L2372:
	addq	$32, %rbx
	cmpq	-328(%rbp), %rbx
	jne	.L2378
	cmpq	%rax, -264(%rbp)
	je	.L2483
.L2380:
	movq	-192(%rbp), %r12
	leaq	-208(%rbp), %rbx
	testq	%r12, %r12
	je	.L2389
.L2386:
	movq	24(%r12), %rsi
	movq	%rbx, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r12), %rdi
	leaq	48(%r12), %rax
	movq	16(%r12), %r13
	cmpq	%rax, %rdi
	je	.L2388
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	je	.L2389
.L2390:
	movq	%r13, %r12
	jmp	.L2386
	.p2align 4,,10
	.p2align 3
.L2369:
	xorl	%eax, %eax
	cmpq	%r15, %r12
	sete	%r13b
	orb	%al, %r13b
	je	.L2484
	.p2align 4,,10
	.p2align 3
.L2373:
	movl	$64, %edi
.LEHB298:
	call	_Znwm@PLT
.LEHE298:
	movq	%rax, %r14
	leaq	32(%rax), %rdi
	leaq	48(%rax), %rax
	movq	(%rbx), %rsi
	movq	8(%rbx), %rdx
	movq	%rax, 32(%r14)
	addq	%rsi, %rdx
.LEHB299:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag.constprop.0
.LEHE299:
	movzbl	%r13b, %edi
	movq	%r15, %rcx
	movq	%r12, %rdx
	movq	%r14, %rsi
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	movq	-168(%rbp), %rax
	addq	$1, %rax
	movq	%rax, -168(%rbp)
	jmp	.L2372
	.p2align 4,,10
	.p2align 3
.L2371:
	testq	%rax, %rax
	setne	%al
	cmpq	%r15, %r12
	sete	%r13b
	orb	%al, %r13b
	jne	.L2373
.L2484:
	leaq	32(%r12), %rsi
	movq	%rbx, %rdi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareERKS4_@PLT
	shrl	$31, %eax
	movl	%eax, %r13d
	jmp	.L2373
	.p2align 4,,10
	.p2align 3
.L2388:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L2390
.L2389:
	movq	-328(%rbp), %rbx
.L2387:
	subq	$32, %rbx
	movq	(%rbx), %rdi
	leaq	16(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2391
	call	_ZdlPv@PLT
	cmpq	-312(%rbp), %rbx
	jne	.L2387
	cmpb	$0, -352(%rbp)
	jne	.L2485
.L2394:
	movq	-288(%rbp), %rsi
	leaq	-304(%rbp), %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
.L2322:
	movq	-368(%rbp), %rax
	movq	-344(%rbp), %rdi
	movq	16(%rax), %rsi
.LEHB300:
	call	_ZNK2v88internal6torque4Type25GetGeneratedTNodeTypeNameB5cxx11Ev
.LEHE300:
.L2321:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2486
	movq	-344(%rbp), %rax
	addq	$328, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2397:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L2399
	jmp	.L2321
	.p2align 4,,10
	.p2align 3
.L2391:
	cmpq	-312(%rbp), %rbx
	jne	.L2387
	cmpb	$0, -352(%rbp)
	je	.L2394
.L2485:
	movq	-344(%rbp), %rdi
	leaq	.LC97(%rip), %rsi
	leaq	-304(%rbp), %r15
.LEHB301:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
.LEHE301:
	jmp	.L2365
	.p2align 4,,10
	.p2align 3
.L2348:
	movq	-232(%rbp), %r13
	movq	-280(%rbp), %r12
	cmpq	-320(%rbp), %r12
	je	.L2353
	.p2align 4,,10
	.p2align 3
.L2356:
	movq	40(%r12), %rdx
	cmpq	40(%r13), %rdx
	jne	.L2354
	testq	%rdx, %rdx
	je	.L2355
	movq	32(%r13), %rsi
	movq	32(%r12), %rdi
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L2354
.L2355:
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r13
	cmpq	-320(%rbp), %r12
	jne	.L2356
.L2353:
	movb	$1, -352(%rbp)
	jmp	.L2349
	.p2align 4,,10
	.p2align 3
.L2478:
	movq	%r14, -328(%rbp)
	jmp	.L2333
	.p2align 4,,10
	.p2align 3
.L2483:
	movq	-280(%rbp), %r12
	movq	-320(%rbp), %rbx
	movq	-184(%rbp), %r13
	cmpq	%rbx, %r12
	je	.L2382
	.p2align 4,,10
	.p2align 3
.L2385:
	movq	40(%r12), %rdx
	cmpq	40(%r13), %rdx
	jne	.L2380
	testq	%rdx, %rdx
	je	.L2384
	movq	32(%r13), %rsi
	movq	32(%r12), %rdi
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L2380
.L2384:
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r13
	cmpq	%rbx, %r12
	jne	.L2385
.L2382:
	movb	$1, -352(%rbp)
	jmp	.L2380
.L2404:
	movl	$1, %ebx
	jmp	.L2326
.L2405:
	movb	$1, -336(%rbp)
	jmp	.L2341
.L2486:
	call	__stack_chk_fail@PLT
.L2408:
	endbr64
	movq	%rax, %r12
	jmp	.L2347
.L2409:
	endbr64
	movq	%rax, %rdi
	jmp	.L2345
.L2407:
	endbr64
	movq	%rax, %r12
	jmp	.L2352
.L2412:
	endbr64
	movq	%rax, %rdi
	jmp	.L2375
.L2411:
	endbr64
	movq	%rax, %r12
	jmp	.L2377
.L2406:
	endbr64
	movq	%rax, %r12
	jmp	.L2400
	.section	.gcc_except_table._ZNK2v88internal6torque9UnionType29GetGeneratedTNodeTypeNameImplB5cxx11Ev,"a",@progbits
	.align 4
.LLSDA6522:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATT6522-.LLSDATTD6522
.LLSDATTD6522:
	.byte	0x1
	.uleb128 .LLSDACSE6522-.LLSDACSB6522
.LLSDACSB6522:
	.uleb128 .LEHB294-.LFB6522
	.uleb128 .LEHE294-.LEHB294
	.uleb128 .L2407-.LFB6522
	.uleb128 0
	.uleb128 .LEHB295-.LFB6522
	.uleb128 .LEHE295-.LEHB295
	.uleb128 .L2406-.LFB6522
	.uleb128 0
	.uleb128 .LEHB296-.LFB6522
	.uleb128 .LEHE296-.LEHB296
	.uleb128 .L2408-.LFB6522
	.uleb128 0
	.uleb128 .LEHB297-.LFB6522
	.uleb128 .LEHE297-.LEHB297
	.uleb128 .L2409-.LFB6522
	.uleb128 0x1
	.uleb128 .LEHB298-.LFB6522
	.uleb128 .LEHE298-.LEHB298
	.uleb128 .L2411-.LFB6522
	.uleb128 0
	.uleb128 .LEHB299-.LFB6522
	.uleb128 .LEHE299-.LEHB299
	.uleb128 .L2412-.LFB6522
	.uleb128 0x1
	.uleb128 .LEHB300-.LFB6522
	.uleb128 .LEHE300-.LEHB300
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB301-.LFB6522
	.uleb128 .LEHE301-.LEHB301
	.uleb128 .L2407-.LFB6522
	.uleb128 0
.LLSDACSE6522:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATT6522:
	.section	.text._ZNK2v88internal6torque9UnionType29GetGeneratedTNodeTypeNameImplB5cxx11Ev
	.cfi_endproc
	.section	.text.unlikely._ZNK2v88internal6torque9UnionType29GetGeneratedTNodeTypeNameImplB5cxx11Ev
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6522
	.type	_ZNK2v88internal6torque9UnionType29GetGeneratedTNodeTypeNameImplB5cxx11Ev.cold, @function
_ZNK2v88internal6torque9UnionType29GetGeneratedTNodeTypeNameImplB5cxx11Ev.cold:
.LFSB6522:
	nop
.L2410:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	endbr64
	movq	%rax, %r12
	call	__cxa_end_catch@PLT
.L2347:
	movq	-240(%rbp), %rsi
	leaq	-256(%rbp), %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	-128(%rbp), %rdi
	cmpq	-360(%rbp), %rdi
	je	.L2350
	call	_ZdlPv@PLT
.L2350:
	movq	-160(%rbp), %rdi
	cmpq	-328(%rbp), %rdi
	je	.L2477
	call	_ZdlPv@PLT
.L2477:
	leaq	-304(%rbp), %r15
.L2352:
	movq	-288(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	%r12, %rdi
.LEHB302:
	call	_Unwind_Resume@PLT
.LEHE302:
.L2345:
	call	__cxa_begin_catch@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.LEHB303:
	call	__cxa_rethrow@PLT
.LEHE303:
.L2375:
	call	__cxa_begin_catch@PLT
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.LEHB304:
	call	__cxa_rethrow@PLT
.LEHE304:
.L2413:
	endbr64
	movq	%rax, %r12
	call	__cxa_end_catch@PLT
.L2377:
	movq	-192(%rbp), %rsi
	leaq	-208(%rbp), %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	-328(%rbp), %rbx
.L2381:
	subq	$32, %rbx
	movq	(%rbx), %rdi
	leaq	16(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2402
	call	_ZdlPv@PLT
.L2402:
	cmpq	-312(%rbp), %rbx
	je	.L2477
	jmp	.L2381
.L2400:
	movq	-160(%rbp), %rdi
	leaq	-144(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2352
	call	_ZdlPv@PLT
	jmp	.L2352
	.cfi_endproc
.LFE6522:
	.section	.gcc_except_table._ZNK2v88internal6torque9UnionType29GetGeneratedTNodeTypeNameImplB5cxx11Ev
	.align 4
.LLSDAC6522:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATTC6522-.LLSDATTDC6522
.LLSDATTDC6522:
	.byte	0x1
	.uleb128 .LLSDACSEC6522-.LLSDACSBC6522
.LLSDACSBC6522:
	.uleb128 .LEHB302-.LCOLDB98
	.uleb128 .LEHE302-.LEHB302
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB303-.LCOLDB98
	.uleb128 .LEHE303-.LEHB303
	.uleb128 .L2410-.LCOLDB98
	.uleb128 0
	.uleb128 .LEHB304-.LCOLDB98
	.uleb128 .LEHE304-.LEHB304
	.uleb128 .L2413-.LCOLDB98
	.uleb128 0
.LLSDACSEC6522:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATTC6522:
	.section	.text.unlikely._ZNK2v88internal6torque9UnionType29GetGeneratedTNodeTypeNameImplB5cxx11Ev
	.section	.text._ZNK2v88internal6torque9UnionType29GetGeneratedTNodeTypeNameImplB5cxx11Ev
	.size	_ZNK2v88internal6torque9UnionType29GetGeneratedTNodeTypeNameImplB5cxx11Ev, .-_ZNK2v88internal6torque9UnionType29GetGeneratedTNodeTypeNameImplB5cxx11Ev
	.section	.text.unlikely._ZNK2v88internal6torque9UnionType29GetGeneratedTNodeTypeNameImplB5cxx11Ev
	.size	_ZNK2v88internal6torque9UnionType29GetGeneratedTNodeTypeNameImplB5cxx11Ev.cold, .-_ZNK2v88internal6torque9UnionType29GetGeneratedTNodeTypeNameImplB5cxx11Ev.cold
.LCOLDE98:
	.section	.text._ZNK2v88internal6torque9UnionType29GetGeneratedTNodeTypeNameImplB5cxx11Ev
.LHOTE98:
	.section	.text._ZNK2v88internal6torque9UnionType24GetGeneratedTypeNameImplB5cxx11Ev,"axG",@progbits,_ZNK2v88internal6torque9UnionType24GetGeneratedTypeNameImplB5cxx11Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal6torque9UnionType24GetGeneratedTypeNameImplB5cxx11Ev
	.type	_ZNK2v88internal6torque9UnionType24GetGeneratedTypeNameImplB5cxx11Ev, @function
_ZNK2v88internal6torque9UnionType24GetGeneratedTypeNameImplB5cxx11Ev:
.LFB5649:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA5649
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-112(%rbp), %r14
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%r14, %rdi
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
.LEHB305:
	call	_ZNK2v88internal6torque9UnionType29GetGeneratedTNodeTypeNameImplB5cxx11Ev
.LEHE305:
	cmpq	$0, -104(%rbp)
	je	.L2488
	movl	$16, %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r14, %rdi
	leaq	.LC3(%rip), %rcx
.LEHB306:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.LEHE306:
	leaq	-64(%rbp), %r13
	leaq	16(%rax), %rdx
	movq	%r13, -80(%rbp)
	movq	(%rax), %rcx
	cmpq	%rdx, %rcx
	je	.L2516
	movq	%rcx, -80(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -64(%rbp)
.L2495:
	movq	8(%rax), %rcx
	movq	%rcx, -72(%rbp)
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movb	$0, 16(%rax)
	movabsq	$4611686018427387903, %rax
	cmpq	%rax, -72(%rbp)
	je	.L2517
	leaq	-80(%rbp), %rdi
	movl	$1, %edx
	leaq	.LC4(%rip), %rsi
.LEHB307:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.LEHE307:
	leaq	16(%r12), %rdx
	movq	%rdx, (%r12)
	movq	(%rax), %rcx
	leaq	16(%rax), %rdx
	cmpq	%rdx, %rcx
	je	.L2518
	movq	%rcx, (%r12)
	movq	16(%rax), %rcx
	movq	%rcx, 16(%r12)
.L2498:
	movq	8(%rax), %rcx
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movq	%rcx, 8(%r12)
	movb	$0, 16(%rax)
	movq	-80(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L2499
	call	_ZdlPv@PLT
.L2499:
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2487
	call	_ZdlPv@PLT
.L2487:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2519
	addq	$88, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2516:
	.cfi_restore_state
	movdqu	16(%rax), %xmm0
	movaps	%xmm0, -64(%rbp)
	jmp	.L2495
	.p2align 4,,10
	.p2align 3
.L2518:
	movdqu	16(%rax), %xmm1
	movups	%xmm1, 16(%r12)
	jmp	.L2498
.L2488:
	leaq	-80(%rbp), %r12
	movq	%r13, %rsi
	movq	%r12, %rdi
.LEHB308:
	call	_ZNK2v88internal6torque4Type8ToStringB5cxx11Ev
.LEHE308:
	leaq	.LC42(%rip), %rdx
	movq	%r12, %rsi
	leaq	.LC49(%rip), %rdi
.LEHB309:
	call	_ZN2v88internal6torque11ReportErrorIJRA44_KcNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA41_S3_EEEvDpOT_
.LEHE309:
.L2519:
	call	__stack_chk_fail@PLT
.L2517:
	leaq	.LC2(%rip), %rdi
.LEHB310:
	call	_ZSt20__throw_length_errorPKc@PLT
.LEHE310:
.L2506:
	endbr64
.L2514:
	movq	%rax, %r12
	jmp	.L2503
.L2507:
	endbr64
	movq	%rax, %r12
.L2501:
	movq	-80(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L2503
.L2515:
	call	_ZdlPv@PLT
.L2503:
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2504
	call	_ZdlPv@PLT
.L2504:
	movq	%r12, %rdi
.LEHB311:
	call	_Unwind_Resume@PLT
.LEHE311:
.L2509:
	endbr64
	movq	%rax, %r12
	jmp	.L2490
.L2508:
	endbr64
	jmp	.L2514
.L2490:
	movq	-80(%rbp), %rdi
	leaq	-64(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L2515
	jmp	.L2503
	.cfi_endproc
.LFE5649:
	.section	.gcc_except_table._ZNK2v88internal6torque9UnionType24GetGeneratedTypeNameImplB5cxx11Ev,"aG",@progbits,_ZNK2v88internal6torque9UnionType24GetGeneratedTypeNameImplB5cxx11Ev,comdat
.LLSDA5649:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE5649-.LLSDACSB5649
.LLSDACSB5649:
	.uleb128 .LEHB305-.LFB5649
	.uleb128 .LEHE305-.LEHB305
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB306-.LFB5649
	.uleb128 .LEHE306-.LEHB306
	.uleb128 .L2506-.LFB5649
	.uleb128 0
	.uleb128 .LEHB307-.LFB5649
	.uleb128 .LEHE307-.LEHB307
	.uleb128 .L2507-.LFB5649
	.uleb128 0
	.uleb128 .LEHB308-.LFB5649
	.uleb128 .LEHE308-.LEHB308
	.uleb128 .L2508-.LFB5649
	.uleb128 0
	.uleb128 .LEHB309-.LFB5649
	.uleb128 .LEHE309-.LEHB309
	.uleb128 .L2509-.LFB5649
	.uleb128 0
	.uleb128 .LEHB310-.LFB5649
	.uleb128 .LEHE310-.LEHB310
	.uleb128 .L2507-.LFB5649
	.uleb128 0
	.uleb128 .LEHB311-.LFB5649
	.uleb128 .LEHE311-.LEHB311
	.uleb128 0
	.uleb128 0
.LLSDACSE5649:
	.section	.text._ZNK2v88internal6torque9UnionType24GetGeneratedTypeNameImplB5cxx11Ev,"axG",@progbits,_ZNK2v88internal6torque9UnionType24GetGeneratedTypeNameImplB5cxx11Ev,comdat
	.size	_ZNK2v88internal6torque9UnionType24GetGeneratedTypeNameImplB5cxx11Ev, .-_ZNK2v88internal6torque9UnionType24GetGeneratedTypeNameImplB5cxx11Ev
	.section	.text._ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE24_M_get_insert_unique_posERKS5_,"axG",@progbits,_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE24_M_get_insert_unique_posERKS5_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE24_M_get_insert_unique_posERKS5_
	.type	_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE24_M_get_insert_unique_posERKS5_, @function
_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE24_M_get_insert_unique_posERKS5_:
.LFB9481:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA9481
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-128(%rbp), %r15
	leaq	-96(%rbp), %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -176(%rbp)
	movq	16(%rdi), %rbx
	movq	%rsi, -160(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-112(%rbp), %rax
	movq	%rax, -168(%rbp)
	testq	%rbx, %rbx
	jne	.L2521
	jmp	.L2550
	.p2align 4,,10
	.p2align 3
.L2528:
	movq	16(%rbx), %rax
	movl	$1, %edx
	testq	%rax, %rax
	je	.L2523
.L2551:
	movq	%rax, %rbx
.L2521:
	movq	32(%rbx), %rsi
	movq	-160(%rbp), %rax
	movq	%r15, %rdi
	movq	(%rax), %r12
	movq	(%rsi), %rax
.LEHB312:
	call	*24(%rax)
.LEHE312:
	movq	(%r12), %rax
	movq	%r12, %rsi
	movq	%r14, %rdi
.LEHB313:
	call	*24(%rax)
.LEHE313:
	movq	-88(%rbp), %rcx
	movq	-120(%rbp), %r9
	movq	-128(%rbp), %r12
	movq	-96(%rbp), %rdi
	cmpq	%r9, %rcx
	movq	%r9, %rdx
	cmovbe	%rcx, %rdx
	testq	%rdx, %rdx
	je	.L2524
	movq	%r12, %rsi
	movq	%r9, -152(%rbp)
	movq	%rcx, -144(%rbp)
	movq	%rdi, -136(%rbp)
	call	memcmp@PLT
	movq	-136(%rbp), %rdi
	movq	-144(%rbp), %rcx
	testl	%eax, %eax
	movq	-152(%rbp), %r9
	movl	%eax, %r13d
	jne	.L2525
.L2524:
	subq	%r9, %rcx
	movl	$2147483648, %eax
	movl	$2147483647, %r13d
	cmpq	%rax, %rcx
	jge	.L2525
	movabsq	$-2147483649, %rax
	movl	$-2147483648, %r13d
	cmpq	%rax, %rcx
	cmovg	%ecx, %r13d
.L2525:
	leaq	-80(%rbp), %rdx
	cmpq	%rdx, %rdi
	je	.L2526
	call	_ZdlPv@PLT
	movq	-128(%rbp), %r12
.L2526:
	cmpq	-168(%rbp), %r12
	je	.L2527
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L2527:
	testl	%r13d, %r13d
	js	.L2528
	movq	24(%rbx), %rax
	xorl	%edx, %edx
	testq	%rax, %rax
	jne	.L2551
.L2523:
	testb	%dl, %dl
	je	.L2540
.L2522:
	movq	-176(%rbp), %rax
	xorl	%r8d, %r8d
	movq	%rbx, %rdx
	cmpq	%rbx, 24(%rax)
	je	.L2535
	movq	%rbx, %rdi
	movq	%rbx, %r13
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	%rax, %rbx
.L2533:
	movq	-160(%rbp), %rax
	movq	32(%rbx), %rdi
	movq	(%rax), %rsi
.LEHB314:
	call	_ZN2v88internal6torqueltERKNS1_4TypeES4_
	xorl	%edx, %edx
	testb	%al, %al
	cmovne	%rdx, %rbx
	cmovne	%r13, %rdx
	movq	%rbx, %r8
.L2535:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2552
	addq	$136, %rsp
	movq	%r8, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2540:
	.cfi_restore_state
	movq	%rbx, %r13
	jmp	.L2533
	.p2align 4,,10
	.p2align 3
.L2550:
	leaq	8(%rdi), %rbx
	jmp	.L2522
.L2552:
	call	__stack_chk_fail@PLT
.L2541:
	endbr64
	movq	%rax, %r12
.L2530:
	movq	-128(%rbp), %rdi
	cmpq	-168(%rbp), %rdi
	je	.L2531
	call	_ZdlPv@PLT
.L2531:
	movq	%r12, %rdi
	call	_Unwind_Resume@PLT
.LEHE314:
	.cfi_endproc
.LFE9481:
	.section	.gcc_except_table._ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE24_M_get_insert_unique_posERKS5_,"aG",@progbits,_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE24_M_get_insert_unique_posERKS5_,comdat
.LLSDA9481:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE9481-.LLSDACSB9481
.LLSDACSB9481:
	.uleb128 .LEHB312-.LFB9481
	.uleb128 .LEHE312-.LEHB312
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB313-.LFB9481
	.uleb128 .LEHE313-.LEHB313
	.uleb128 .L2541-.LFB9481
	.uleb128 0
	.uleb128 .LEHB314-.LFB9481
	.uleb128 .LEHE314-.LEHB314
	.uleb128 0
	.uleb128 0
.LLSDACSE9481:
	.section	.text._ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE24_M_get_insert_unique_posERKS5_,"axG",@progbits,_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE24_M_get_insert_unique_posERKS5_,comdat
	.size	_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE24_M_get_insert_unique_posERKS5_, .-_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE24_M_get_insert_unique_posERKS5_
	.section	.rodata._ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE15_M_range_insertIN9__gnu_cxx17__normal_iteratorIPS5_S7_EEEEvSC_T_SD_St20forward_iterator_tag.str1.1,"aMS",@progbits,1
.LC99:
	.string	"vector::_M_range_insert"
	.section	.text._ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE15_M_range_insertIN9__gnu_cxx17__normal_iteratorIPS5_S7_EEEEvSC_T_SD_St20forward_iterator_tag,"axG",@progbits,_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE15_M_range_insertIN9__gnu_cxx17__normal_iteratorIPS5_S7_EEEEvSC_T_SD_St20forward_iterator_tag,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE15_M_range_insertIN9__gnu_cxx17__normal_iteratorIPS5_S7_EEEEvSC_T_SD_St20forward_iterator_tag
	.type	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE15_M_range_insertIN9__gnu_cxx17__normal_iteratorIPS5_S7_EEEEvSC_T_SD_St20forward_iterator_tag, @function
_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE15_M_range_insertIN9__gnu_cxx17__normal_iteratorIPS5_S7_EEEEvSC_T_SD_St20forward_iterator_tag:
.LFB9512:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA9512
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -64(%rbp)
	movq	%rcx, -56(%rbp)
	cmpq	%rdx, %rcx
	je	.L2553
	movq	%rcx, %rax
	movq	%rdi, %r15
	movq	8(%rdi), %rdi
	movq	%rdx, %rbx
	subq	%rdx, %rax
	movq	%rsi, %r12
	movq	%rax, %rcx
	sarq	$5, %rax
	movq	%rdi, -72(%rbp)
	movq	%rax, -80(%rbp)
	movq	%rax, %r14
	movq	16(%r15), %rax
	subq	%rdi, %rax
	sarq	$5, %rax
	cmpq	%r14, %rax
	jb	.L2556
	movq	%rdi, %rdx
	subq	%rsi, %rdi
	movq	%rdi, -88(%rbp)
	sarq	$5, %rdi
	movq	%rdi, -96(%rbp)
	movq	%rdi, -104(%rbp)
	cmpq	%rdi, %r14
	jnb	.L2557
	movq	%rdx, %r13
	movq	%rdx, %rdi
	subq	%rcx, %r13
	subq	%r13, %rdi
	leaq	16(%r13), %rax
	addq	%rdx, %rdi
	jmp	.L2561
	.p2align 4,,10
	.p2align 3
.L2558:
	movq	%rsi, (%rdx)
	movq	(%rax), %rsi
	movq	%rsi, 16(%rdx)
.L2658:
	movq	-8(%rax), %rsi
	addq	$32, %rdx
	movq	%rsi, -24(%rdx)
	movq	%rax, -16(%rax)
	addq	$32, %rax
	movq	$0, -40(%rax)
	movb	$0, -32(%rax)
	cmpq	%rdi, %rdx
	je	.L2659
.L2561:
	leaq	16(%rdx), %rsi
	movq	%rsi, (%rdx)
	movq	-16(%rax), %rsi
	cmpq	%rax, %rsi
	jne	.L2558
	movdqu	(%rax), %xmm1
	movups	%xmm1, 16(%rdx)
	jmp	.L2658
	.p2align 4,,10
	.p2align 3
.L2556:
	movabsq	$288230376151711743, %rsi
	movq	(%r15), %rcx
	movq	%rdi, %r13
	movq	%rsi, %rax
	subq	%rcx, %r13
	sarq	$5, %r13
	subq	%r13, %rax
	cmpq	%rax, -80(%rbp)
	ja	.L2660
	movq	-80(%rbp), %rax
	cmpq	%r13, %rax
	cmovb	%r13, %rax
	addq	%rax, %r13
	jc	.L2621
	movq	$0, -72(%rbp)
	testq	%r13, %r13
	je	.L2591
	cmpq	%rsi, %r13
	cmova	%rsi, %r13
	salq	$5, %r13
.L2590:
	movq	%r13, %rdi
	movq	%rdx, -80(%rbp)
.LEHB315:
	call	_Znwm@PLT
.LEHE315:
	movq	(%r15), %rcx
	movq	-80(%rbp), %rdx
	movq	%rax, -72(%rbp)
.L2591:
	movq	-64(%rbp), %rbx
	cmpq	%rcx, %rbx
	je	.L2623
	subq	%rcx, %rbx
	leaq	16(%rcx), %rax
	movq	-72(%rbp), %rcx
	movq	%rbx, %r8
	leaq	(%rcx,%rbx), %rdi
	.p2align 4,,10
	.p2align 3
.L2596:
	leaq	16(%rcx), %rsi
	movq	%rsi, (%rcx)
	movq	-16(%rax), %rsi
	cmpq	%rax, %rsi
	je	.L2661
	movq	%rsi, (%rcx)
	movq	(%rax), %rsi
	addq	$32, %rcx
	movq	%rsi, -16(%rcx)
	movq	-8(%rax), %rsi
	movq	%rsi, -24(%rcx)
	movq	%rax, -16(%rax)
	addq	$32, %rax
	movq	$0, -40(%rax)
	movb	$0, -32(%rax)
	cmpq	%rdi, %rcx
	jne	.L2596
.L2595:
	movq	-72(%rbp), %rax
	leaq	(%rax,%r8), %r14
.L2592:
	movq	%rdx, %rbx
	movq	%r14, %r12
	.p2align 4,,10
	.p2align 3
.L2597:
	leaq	16(%r12), %rax
	movq	(%rbx), %rsi
	movq	8(%rbx), %rdx
	movq	%r12, %rdi
	movq	%rax, (%r12)
	addq	%rsi, %rdx
.LEHB316:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag.constprop.0
.LEHE316:
	addq	$32, %r12
	addq	$32, %rbx
	cmpq	%rbx, -56(%rbp)
	jne	.L2597
	movq	8(%r15), %r14
	cmpq	%r14, -64(%rbp)
	je	.L2599
	movq	-64(%rbp), %rbx
	movq	%r14, %r8
	movq	%r12, %rdx
	subq	%rbx, %r8
	leaq	16(%rbx), %rax
	leaq	(%r12,%r8), %rsi
	.p2align 4,,10
	.p2align 3
.L2609:
	leaq	16(%rdx), %rcx
	movq	%rcx, (%rdx)
	movq	-16(%rax), %rcx
	cmpq	%rax, %rcx
	je	.L2662
	movq	%rcx, (%rdx)
	movq	(%rax), %rcx
	addq	$32, %rdx
	movq	%rax, -16(%rax)
	addq	$32, %rax
	movq	%rcx, -16(%rdx)
	movq	-40(%rax), %rcx
	movb	$0, -32(%rax)
	movq	%rcx, -24(%rdx)
	movq	$0, -40(%rax)
	cmpq	%rsi, %rdx
	jne	.L2609
.L2608:
	movq	8(%r15), %r14
	addq	%r8, %r12
.L2599:
	movq	(%r15), %rbx
	cmpq	%r14, %rbx
	je	.L2610
	.p2align 4,,10
	.p2align 3
.L2614:
	movq	(%rbx), %rdi
	leaq	16(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2611
	call	_ZdlPv@PLT
	addq	$32, %rbx
	cmpq	%rbx, %r14
	jne	.L2614
.L2612:
	movq	(%r15), %r14
.L2610:
	testq	%r14, %r14
	je	.L2615
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L2615:
	movq	-72(%rbp), %rax
	movq	%r12, %xmm5
	addq	%rax, %r13
	movq	%rax, %xmm0
	movq	%r13, 16(%r15)
	punpcklqdq	%xmm5, %xmm0
	movups	%xmm0, (%r15)
.L2553:
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2557:
	.cfi_restore_state
	movq	-88(%rbp), %rax
	leaq	(%rbx,%rax), %r13
	cmpq	%r13, -56(%rbp)
	je	.L2620
	movq	-72(%rbp), %r14
	.p2align 4,,10
	.p2align 3
.L2574:
	leaq	16(%r14), %rax
	movq	%r14, %rdi
	movq	%rax, (%r14)
	movq	0(%r13), %rsi
	movq	8(%r13), %rdx
	addq	%rsi, %rdx
.LEHB317:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag.constprop.0
.LEHE317:
	addq	$32, %r14
	addq	$32, %r13
	cmpq	%r13, -56(%rbp)
	jne	.L2574
	movq	8(%r15), %rax
.L2573:
	movq	-72(%rbp), %rcx
	movq	-64(%rbp), %rdi
	movq	-80(%rbp), %rdx
	subq	-96(%rbp), %rdx
	salq	$5, %rdx
	movq	%rcx, %rsi
	addq	%rax, %rdx
	subq	%rdi, %rsi
	leaq	16(%rdi), %rax
	movq	%rdx, 8(%r15)
	addq	%rdx, %rsi
	cmpq	%rdi, %rcx
	je	.L2576
	.p2align 4,,10
	.p2align 3
.L2585:
	leaq	16(%rdx), %rcx
	movq	%rcx, (%rdx)
	movq	-16(%rax), %rcx
	cmpq	%rax, %rcx
	je	.L2663
	movq	%rcx, (%rdx)
	movq	(%rax), %rcx
	addq	$32, %rdx
	movq	%rcx, -16(%rdx)
	movq	-8(%rax), %rcx
	movq	%rcx, -24(%rdx)
	movq	%rax, -16(%rax)
	addq	$32, %rax
	movq	$0, -40(%rax)
	movb	$0, -32(%rax)
	cmpq	%rsi, %rdx
	jne	.L2585
.L2584:
	movq	8(%r15), %rdx
.L2576:
	movq	-88(%rbp), %rax
	addq	%rax, %rdx
	movq	%rdx, 8(%r15)
	testq	%rax, %rax
	jle	.L2553
	movq	-104(%rbp), %r13
	.p2align 4,,10
	.p2align 3
.L2586:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	addq	$32, %rbx
	addq	$32, %r12
.LEHB318:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	subq	$1, %r13
	jne	.L2586
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2663:
	.cfi_restore_state
	movdqu	(%rax), %xmm2
	addq	$32, %rdx
	movups	%xmm2, -16(%rdx)
	movq	-8(%rax), %rcx
	movq	%rcx, -24(%rdx)
	movq	%rax, -16(%rax)
	addq	$32, %rax
	movq	$0, -40(%rax)
	movb	$0, -32(%rax)
	cmpq	%rdx, %rsi
	jne	.L2585
	jmp	.L2584
	.p2align 4,,10
	.p2align 3
.L2659:
	movq	%r13, %rax
	addq	%rcx, 8(%r15)
	subq	-64(%rbp), %rax
	subq	$16, %r13
	movq	-72(%rbp), %r15
	movq	%rax, %r8
	sarq	$5, %r8
	subq	$16, %r15
	testq	%rax, %rax
	jle	.L2571
	movq	%r12, -56(%rbp)
	movq	%rcx, %r12
	movq	%r14, -64(%rbp)
	movq	%rbx, %r14
	movq	%r8, %rbx
	jmp	.L2572
	.p2align 4,,10
	.p2align 3
.L2565:
	cmpq	%rdi, %r15
	je	.L2664
	movq	%rax, -16(%r15)
	movq	-8(%r13), %rax
	movq	(%r15), %rdx
	movq	%rax, -8(%r15)
	movq	0(%r13), %rax
	movq	%rax, (%r15)
	testq	%rdi, %rdi
	je	.L2570
	movq	%rdi, -16(%r13)
	movq	%rdx, 0(%r13)
.L2568:
	movq	-16(%r13), %rax
	movq	$0, -8(%r13)
	subq	$32, %r15
	subq	$32, %r13
	movb	$0, (%rax)
	subq	$1, %rbx
	je	.L2665
.L2572:
	movq	-16(%r13), %rax
	movq	-16(%r15), %rdi
	cmpq	%r13, %rax
	jne	.L2565
	movq	-8(%r13), %rdx
	testq	%rdx, %rdx
	je	.L2566
	cmpq	$1, %rdx
	je	.L2666
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	-16(%r15), %rdi
	movq	-8(%r13), %rdx
.L2566:
	movq	%rdx, -8(%r15)
	movb	$0, (%rdi,%rdx)
	jmp	.L2568
	.p2align 4,,10
	.p2align 3
.L2664:
	movq	%rax, -16(%r15)
	movq	-8(%r13), %rax
	movq	%rax, -8(%r15)
	movq	0(%r13), %rax
	movq	%rax, (%r15)
.L2570:
	movq	%r13, -16(%r13)
	jmp	.L2568
	.p2align 4,,10
	.p2align 3
.L2665:
	movq	%r12, %rcx
	movq	%r14, %rbx
	movq	-56(%rbp), %r12
	movq	-64(%rbp), %r14
.L2571:
	testq	%rcx, %rcx
	jle	.L2553
	.p2align 4,,10
	.p2align 3
.L2564:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	addq	$32, %rbx
	addq	$32, %r12
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	subq	$1, %r14
	jne	.L2564
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2666:
	.cfi_restore_state
	movzbl	0(%r13), %eax
	movb	%al, (%rdi)
	movq	-16(%r15), %rdi
	movq	-8(%r13), %rdx
	jmp	.L2566
	.p2align 4,,10
	.p2align 3
.L2621:
	movabsq	$9223372036854775776, %r13
	jmp	.L2590
	.p2align 4,,10
	.p2align 3
.L2611:
	addq	$32, %rbx
	cmpq	%r14, %rbx
	jne	.L2614
	jmp	.L2612
	.p2align 4,,10
	.p2align 3
.L2662:
	movdqu	(%rax), %xmm4
	movq	-8(%rax), %rcx
	addq	$32, %rdx
	movb	$0, (%rax)
	movq	$0, -8(%rax)
	addq	$32, %rax
	movups	%xmm4, -16(%rdx)
	movq	%rcx, -24(%rdx)
	cmpq	%rdx, %rsi
	jne	.L2609
	jmp	.L2608
	.p2align 4,,10
	.p2align 3
.L2661:
	movdqu	(%rax), %xmm3
	addq	$32, %rcx
	movups	%xmm3, -16(%rcx)
	movq	-8(%rax), %rsi
	movq	%rsi, -24(%rcx)
	movq	%rax, -16(%rax)
	addq	$32, %rax
	movq	$0, -40(%rax)
	movb	$0, -32(%rax)
	cmpq	%rcx, %rdi
	jne	.L2596
	jmp	.L2595
	.p2align 4,,10
	.p2align 3
.L2620:
	movq	-72(%rbp), %rax
	jmp	.L2573
	.p2align 4,,10
	.p2align 3
.L2623:
	movq	-72(%rbp), %r14
	jmp	.L2592
.L2660:
	leaq	.LC99(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.LEHE318:
.L2628:
	endbr64
	movq	%rax, %rdi
	jmp	.L2600
.L2626:
	endbr64
	movq	%rax, %rdi
	jmp	.L2577
.L2600:
	call	__cxa_begin_catch@PLT
	movq	%r14, %rbx
.L2603:
	cmpq	%r12, %rbx
	jne	.L2667
.LEHB319:
	call	__cxa_rethrow@PLT
.LEHE319:
.L2577:
	call	__cxa_begin_catch@PLT
	movq	-72(%rbp), %rbx
.L2580:
	cmpq	%r14, %rbx
	jne	.L2668
.LEHB320:
	call	__cxa_rethrow@PLT
.LEHE320:
.L2667:
	movq	(%rbx), %rdi
	leaq	16(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2602
	call	_ZdlPv@PLT
.L2602:
	addq	$32, %rbx
	jmp	.L2603
.L2668:
	movq	(%rbx), %rdi
	leaq	16(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2579
	call	_ZdlPv@PLT
.L2579:
	addq	$32, %rbx
	jmp	.L2580
.L2627:
	endbr64
	movq	%rax, %r12
	jmp	.L2604
.L2625:
	endbr64
	movq	%rax, %r12
	jmp	.L2581
.L2604:
	call	__cxa_end_catch@PLT
	movq	%r12, %rdi
	call	__cxa_begin_catch@PLT
	movq	-72(%rbp), %rbx
.L2605:
	cmpq	%r14, %rbx
	jne	.L2669
	cmpq	$0, -72(%rbp)
	je	.L2618
	movq	-72(%rbp), %rdi
	call	_ZdlPv@PLT
.L2618:
.LEHB321:
	call	__cxa_rethrow@PLT
.LEHE321:
.L2581:
	call	__cxa_end_catch@PLT
	movq	%r12, %rdi
.LEHB322:
	call	_Unwind_Resume@PLT
.LEHE322:
.L2669:
	movq	(%rbx), %rdi
	leaq	16(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2617
	call	_ZdlPv@PLT
.L2617:
	addq	$32, %rbx
	jmp	.L2605
.L2624:
	endbr64
	movq	%rax, %r12
.L2619:
	call	__cxa_end_catch@PLT
	movq	%r12, %rdi
.LEHB323:
	call	_Unwind_Resume@PLT
.LEHE323:
	.cfi_endproc
.LFE9512:
	.section	.gcc_except_table._ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE15_M_range_insertIN9__gnu_cxx17__normal_iteratorIPS5_S7_EEEEvSC_T_SD_St20forward_iterator_tag,"aG",@progbits,_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE15_M_range_insertIN9__gnu_cxx17__normal_iteratorIPS5_S7_EEEEvSC_T_SD_St20forward_iterator_tag,comdat
	.align 4
.LLSDA9512:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATT9512-.LLSDATTD9512
.LLSDATTD9512:
	.byte	0x1
	.uleb128 .LLSDACSE9512-.LLSDACSB9512
.LLSDACSB9512:
	.uleb128 .LEHB315-.LFB9512
	.uleb128 .LEHE315-.LEHB315
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB316-.LFB9512
	.uleb128 .LEHE316-.LEHB316
	.uleb128 .L2628-.LFB9512
	.uleb128 0x1
	.uleb128 .LEHB317-.LFB9512
	.uleb128 .LEHE317-.LEHB317
	.uleb128 .L2626-.LFB9512
	.uleb128 0x1
	.uleb128 .LEHB318-.LFB9512
	.uleb128 .LEHE318-.LEHB318
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB319-.LFB9512
	.uleb128 .LEHE319-.LEHB319
	.uleb128 .L2627-.LFB9512
	.uleb128 0x3
	.uleb128 .LEHB320-.LFB9512
	.uleb128 .LEHE320-.LEHB320
	.uleb128 .L2625-.LFB9512
	.uleb128 0
	.uleb128 .LEHB321-.LFB9512
	.uleb128 .LEHE321-.LEHB321
	.uleb128 .L2624-.LFB9512
	.uleb128 0
	.uleb128 .LEHB322-.LFB9512
	.uleb128 .LEHE322-.LEHB322
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB323-.LFB9512
	.uleb128 .LEHE323-.LEHB323
	.uleb128 0
	.uleb128 0
.LLSDACSE9512:
	.byte	0x1
	.byte	0
	.byte	0
	.byte	0x7d
	.align 4
	.long	0

.LLSDATT9512:
	.section	.text._ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE15_M_range_insertIN9__gnu_cxx17__normal_iteratorIPS5_S7_EEEEvSC_T_SD_St20forward_iterator_tag,"axG",@progbits,_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE15_M_range_insertIN9__gnu_cxx17__normal_iteratorIPS5_S7_EEEEvSC_T_SD_St20forward_iterator_tag,comdat
	.size	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE15_M_range_insertIN9__gnu_cxx17__normal_iteratorIPS5_S7_EEEEvSC_T_SD_St20forward_iterator_tag, .-_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE15_M_range_insertIN9__gnu_cxx17__normal_iteratorIPS5_S7_EEEEvSC_T_SD_St20forward_iterator_tag
	.section	.text._ZNK2v88internal6torque9UnionType15GetRuntimeTypesB5cxx11Ev,"axG",@progbits,_ZNK2v88internal6torque9UnionType15GetRuntimeTypesB5cxx11Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal6torque9UnionType15GetRuntimeTypesB5cxx11Ev
	.type	_ZNK2v88internal6torque9UnionType15GetRuntimeTypesB5cxx11Ev, @function
_ZNK2v88internal6torque9UnionType15GetRuntimeTypesB5cxx11Ev:
.LFB5704:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA5704
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r15
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, 16(%rdi)
	leaq	80(%rsi), %rax
	movups	%xmm0, (%rdi)
	movq	96(%rsi), %r13
	movq	%rax, -88(%rbp)
	cmpq	%rax, %r13
	je	.L2670
	.p2align 4,,10
	.p2align 3
.L2671:
	movq	32(%r13), %rsi
	movq	%r15, %rdi
	movq	(%rsi), %rax
.LEHB324:
	call	*64(%rax)
.LEHE324:
	movq	8(%r14), %rsi
	movq	-72(%rbp), %rcx
	movq	%r14, %rdi
	movq	-80(%rbp), %rdx
.LEHB325:
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE15_M_range_insertIN9__gnu_cxx17__normal_iteratorIPS5_S7_EEEEvSC_T_SD_St20forward_iterator_tag
.LEHE325:
	movq	-72(%rbp), %rbx
	movq	-80(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L2672
	.p2align 4,,10
	.p2align 3
.L2676:
	movq	(%r12), %rdi
	leaq	16(%r12), %rdx
	cmpq	%rdx, %rdi
	je	.L2673
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L2676
.L2674:
	movq	-80(%rbp), %r12
.L2672:
	testq	%r12, %r12
	je	.L2677
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L2677:
	movq	%r13, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r13
	cmpq	%rax, -88(%rbp)
	jne	.L2671
.L2670:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2688
	addq	$56, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2673:
	.cfi_restore_state
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L2676
	jmp	.L2674
.L2688:
	call	__stack_chk_fail@PLT
.L2684:
	endbr64
	movq	%rax, %r12
	jmp	.L2680
.L2683:
	endbr64
	movq	%rax, %r12
	jmp	.L2681
.L2680:
	movq	%r15, %rdi
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EED1Ev
.L2681:
	movq	%r14, %rdi
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EED1Ev
	movq	%r12, %rdi
.LEHB326:
	call	_Unwind_Resume@PLT
.LEHE326:
	.cfi_endproc
.LFE5704:
	.section	.gcc_except_table._ZNK2v88internal6torque9UnionType15GetRuntimeTypesB5cxx11Ev,"aG",@progbits,_ZNK2v88internal6torque9UnionType15GetRuntimeTypesB5cxx11Ev,comdat
.LLSDA5704:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE5704-.LLSDACSB5704
.LLSDACSB5704:
	.uleb128 .LEHB324-.LFB5704
	.uleb128 .LEHE324-.LEHB324
	.uleb128 .L2683-.LFB5704
	.uleb128 0
	.uleb128 .LEHB325-.LFB5704
	.uleb128 .LEHE325-.LEHB325
	.uleb128 .L2684-.LFB5704
	.uleb128 0
	.uleb128 .LEHB326-.LFB5704
	.uleb128 .LEHE326-.LEHB326
	.uleb128 0
	.uleb128 0
.LLSDACSE5704:
	.section	.text._ZNK2v88internal6torque9UnionType15GetRuntimeTypesB5cxx11Ev,"axG",@progbits,_ZNK2v88internal6torque9UnionType15GetRuntimeTypesB5cxx11Ev,comdat
	.size	_ZNK2v88internal6torque9UnionType15GetRuntimeTypesB5cxx11Ev, .-_ZNK2v88internal6torque9UnionType15GetRuntimeTypesB5cxx11Ev
	.section	.text._ZNSt6vectorIN2v88internal6torque5FieldESaIS3_EE15_M_range_insertIN9__gnu_cxx17__normal_iteratorIPKS3_S5_EEEEvNS8_IPS3_S5_EET_SE_St20forward_iterator_tag,"axG",@progbits,_ZNSt6vectorIN2v88internal6torque5FieldESaIS3_EE15_M_range_insertIN9__gnu_cxx17__normal_iteratorIPKS3_S5_EEEEvNS8_IPS3_S5_EET_SE_St20forward_iterator_tag,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal6torque5FieldESaIS3_EE15_M_range_insertIN9__gnu_cxx17__normal_iteratorIPKS3_S5_EEEEvNS8_IPS3_S5_EET_SE_St20forward_iterator_tag
	.type	_ZNSt6vectorIN2v88internal6torque5FieldESaIS3_EE15_M_range_insertIN9__gnu_cxx17__normal_iteratorIPKS3_S5_EEEEvNS8_IPS3_S5_EET_SE_St20forward_iterator_tag, @function
_ZNSt6vectorIN2v88internal6torque5FieldESaIS3_EE15_M_range_insertIN9__gnu_cxx17__normal_iteratorIPKS3_S5_EEEEvNS8_IPS3_S5_EET_SE_St20forward_iterator_tag:
.LFB9991:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA9991
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -56(%rbp)
	movq	%rdx, -72(%rbp)
	cmpq	%rdx, %rcx
	je	.L2689
	movq	%rcx, %r8
	movq	8(%rdi), %r13
	movq	%rcx, %r14
	movq	%rdi, %r15
	subq	%rdx, %r8
	movabsq	$5675921253449092805, %rdx
	movq	%r8, %rax
	sarq	$3, %rax
	imulq	%rdx, %rax
	movq	%rax, -80(%rbp)
	movq	%rax, %rcx
	movq	16(%rdi), %rax
	subq	%r13, %rax
	sarq	$3, %rax
	imulq	%rdx, %rax
	cmpq	%rcx, %rax
	jb	.L2692
	movq	%r13, %rax
	subq	%rsi, %rax
	movq	%rax, -88(%rbp)
	sarq	$3, %rax
	imulq	%rdx, %rax
	movq	%rax, -96(%rbp)
	movq	%rax, -64(%rbp)
	cmpq	%rax, %rcx
	jb	.L2810
	movq	-72(%rbp), %rbx
	addq	-88(%rbp), %rbx
	cmpq	%rbx, %r14
	je	.L2763
	movq	%r13, %r12
	.p2align 4,,10
	.p2align 3
.L2716:
	movdqu	(%rbx), %xmm1
	leaq	48(%r12), %rdi
	movups	%xmm1, (%r12)
	movl	16(%rbx), %eax
	movl	%eax, 16(%r12)
	movq	24(%rbx), %rax
	movq	%rax, 24(%r12)
	movdqu	32(%rbx), %xmm2
	leaq	64(%r12), %rax
	movq	%rax, 48(%r12)
	movups	%xmm2, 32(%r12)
	movq	48(%rbx), %rsi
	movq	56(%rbx), %rdx
	addq	%rsi, %rdx
.LEHB327:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag.constprop.0
.LEHE327:
	movq	80(%rbx), %rax
	addq	$104, %rbx
	addq	$104, %r12
	movq	%rax, -24(%r12)
	movq	-16(%rbx), %rax
	movq	%rax, -16(%r12)
	movzbl	-8(%rbx), %eax
	movb	%al, -8(%r12)
	movzbl	-7(%rbx), %eax
	movb	%al, -7(%r12)
	movzbl	-6(%rbx), %eax
	movb	%al, -6(%r12)
	cmpq	%rbx, %r14
	jne	.L2716
	movq	8(%r15), %rcx
.L2715:
	movq	-80(%rbp), %rax
	subq	-96(%rbp), %rax
	leaq	(%rax,%rax,2), %rdx
	movq	-56(%rbp), %rdi
	leaq	(%rax,%rdx,4), %rax
	leaq	(%rcx,%rax,8), %rcx
	leaq	64(%rdi), %rax
	movq	%rcx, 8(%r15)
	cmpq	%rdi, %r13
	jne	.L2726
	jmp	.L2718
	.p2align 4,,10
	.p2align 3
.L2724:
	movq	%rdx, 48(%rcx)
	movq	(%rax), %rdx
	movq	%rdx, 64(%rcx)
.L2725:
	movq	-8(%rax), %rdx
	addq	$104, %rcx
	movq	%rdx, -48(%rcx)
	movq	16(%rax), %rdx
	movq	%rax, -16(%rax)
	movq	$0, -8(%rax)
	movb	$0, (%rax)
	movq	%rdx, -24(%rcx)
	movq	24(%rax), %rdx
	movq	%rdx, -16(%rcx)
	movzbl	32(%rax), %edx
	movb	%dl, -8(%rcx)
	movzbl	33(%rax), %edx
	movb	%dl, -7(%rcx)
	movzbl	34(%rax), %edx
	movb	%dl, -6(%rcx)
	leaq	104(%rax), %rdx
	addq	$40, %rax
	cmpq	%rax, %r13
	je	.L2811
	movq	%rdx, %rax
.L2726:
	movdqu	-64(%rax), %xmm3
	movups	%xmm3, (%rcx)
	movl	-48(%rax), %edx
	movl	%edx, 16(%rcx)
	movq	-40(%rax), %rdx
	movq	%rdx, 24(%rcx)
	movdqu	-32(%rax), %xmm4
	leaq	64(%rcx), %rdx
	movq	%rdx, 48(%rcx)
	movups	%xmm4, 32(%rcx)
	movq	-16(%rax), %rdx
	cmpq	%rax, %rdx
	jne	.L2724
	movdqu	(%rax), %xmm5
	movups	%xmm5, 64(%rcx)
	jmp	.L2725
	.p2align 4,,10
	.p2align 3
.L2815:
	movabsq	$1064235235021704901, %rdx
	subq	-56(%rbp), %r14
	leaq	-104(%r14), %rax
	movq	8(%r15), %r14
	shrq	$3, %rax
	imulq	%rdx, %rax
	movabsq	$2305843009213693951, %rdx
	andq	%rdx, %rax
	addq	$1, %rax
	leaq	(%rax,%rax,2), %rdx
	leaq	(%rax,%rdx,4), %rax
	leaq	(%r12,%rax,8), %r12
.L2742:
	movq	(%r15), %rbx
	cmpq	%r14, %rbx
	je	.L2752
	.p2align 4,,10
	.p2align 3
.L2756:
	movq	48(%rbx), %rdi
	leaq	64(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2753
	call	_ZdlPv@PLT
	addq	$104, %rbx
	cmpq	%rbx, %r14
	jne	.L2756
.L2754:
	movq	(%r15), %r14
.L2752:
	testq	%r14, %r14
	je	.L2757
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L2757:
	movq	-64(%rbp), %rax
	movq	%r12, %xmm1
	addq	%rax, %r13
	movq	%rax, %xmm0
	movq	%r13, 16(%r15)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%r15)
.L2689:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2692:
	.cfi_restore_state
	movq	(%rdi), %rsi
	subq	%rsi, %r13
	sarq	$3, %r13
	imulq	%rdx, %r13
	movabsq	$88686269585142075, %rdx
	movq	%rdx, %rax
	subq	%r13, %rax
	cmpq	%rax, -80(%rbp)
	ja	.L2812
	movq	-80(%rbp), %rax
	cmpq	%r13, %rax
	cmovb	%r13, %rax
	addq	%rax, %r13
	jc	.L2765
	movq	$0, -64(%rbp)
	testq	%r13, %r13
	je	.L2735
	cmpq	%rdx, %r13
	cmova	%rdx, %r13
	leaq	0(%r13,%r13,2), %rax
	leaq	0(%r13,%rax,4), %r13
	salq	$3, %r13
.L2734:
	movq	%r13, %rdi
.LEHB328:
	call	_Znwm@PLT
	movq	(%r15), %rsi
	movq	%rax, -64(%rbp)
.L2735:
	cmpq	%rsi, -56(%rbp)
	je	.L2767
	movq	-64(%rbp), %rdx
	leaq	64(%rsi), %rax
	jmp	.L2739
	.p2align 4,,10
	.p2align 3
.L2737:
	movq	%rcx, 48(%rdx)
	movq	(%rax), %rcx
	movq	%rcx, 64(%rdx)
.L2738:
	movq	-8(%rax), %rcx
	addq	$104, %rdx
	leaq	104(%rax), %rdi
	movq	%rcx, -48(%rdx)
	movq	16(%rax), %rcx
	movq	%rax, -16(%rax)
	movq	$0, -8(%rax)
	movb	$0, (%rax)
	movq	%rcx, -24(%rdx)
	movq	24(%rax), %rcx
	movq	%rcx, -16(%rdx)
	movzbl	32(%rax), %ecx
	movb	%cl, -8(%rdx)
	movzbl	33(%rax), %ecx
	movb	%cl, -7(%rdx)
	movzbl	34(%rax), %ecx
	movb	%cl, -6(%rdx)
	leaq	40(%rax), %rcx
	cmpq	%rcx, -56(%rbp)
	je	.L2813
	movq	%rdi, %rax
.L2739:
	movdqu	-64(%rax), %xmm6
	movups	%xmm6, (%rdx)
	movl	-48(%rax), %ecx
	movl	%ecx, 16(%rdx)
	movq	-40(%rax), %rcx
	movq	%rcx, 24(%rdx)
	movdqu	-32(%rax), %xmm7
	leaq	64(%rdx), %rcx
	movq	%rcx, 48(%rdx)
	movups	%xmm7, 32(%rdx)
	movq	-16(%rax), %rcx
	cmpq	%rax, %rcx
	jne	.L2737
	movdqu	(%rax), %xmm5
	movups	%xmm5, 64(%rdx)
	jmp	.L2738
	.p2align 4,,10
	.p2align 3
.L2811:
	movq	8(%r15), %rcx
.L2718:
	movq	-88(%rbp), %rdi
	leaq	(%rcx,%rdi), %rax
	movq	%rax, 8(%r15)
	testq	%rdi, %rdi
	jle	.L2689
	movq	-56(%rbp), %rbx
	movq	-72(%rbp), %r12
	addq	$48, %rbx
	addq	$48, %r12
	jmp	.L2730
	.p2align 4,,10
	.p2align 3
.L2814:
	movq	-8(%r12), %rdx
	movq	%rdx, -8(%rbx)
	testb	%al, %al
	jne	.L2729
	movb	$1, -16(%rbx)
.L2729:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	addq	$104, %r12
	addq	$104, %rbx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
.LEHE328:
	movq	-72(%r12), %rax
	movq	%rax, -72(%rbx)
	movq	-64(%r12), %rax
	movq	%rax, -64(%rbx)
	movzbl	-56(%r12), %eax
	movb	%al, -56(%rbx)
	movzbl	-55(%r12), %eax
	movb	%al, -55(%rbx)
	movzbl	-54(%r12), %eax
	movb	%al, -54(%rbx)
	subq	$1, -64(%rbp)
	je	.L2689
.L2730:
	movdqu	-48(%r12), %xmm5
	movups	%xmm5, -48(%rbx)
	movl	-32(%r12), %eax
	movl	%eax, -32(%rbx)
	movq	-24(%r12), %rax
	movq	%rax, -24(%rbx)
	cmpb	$0, -16(%r12)
	movzbl	-16(%rbx), %eax
	jne	.L2814
	testb	%al, %al
	je	.L2729
	movb	$0, -16(%rbx)
	jmp	.L2729
	.p2align 4,,10
	.p2align 3
.L2765:
	movabsq	$9223372036854775800, %r13
	jmp	.L2734
	.p2align 4,,10
	.p2align 3
.L2813:
	subq	$64, %rax
	movq	-64(%rbp), %rdi
	movabsq	$1064235235021704901, %rdx
	subq	%rsi, %rax
	shrq	$3, %rax
	imulq	%rdx, %rax
	movabsq	$2305843009213693951, %rdx
	andq	%rdx, %rax
	addq	$1, %rax
	leaq	(%rax,%rax,2), %rdx
	leaq	(%rax,%rdx,4), %rax
	leaq	(%rdi,%rax,8), %rax
	movq	%rax, -80(%rbp)
	movq	%rax, %r12
.L2736:
	movq	-72(%rbp), %rbx
	.p2align 4,,10
	.p2align 3
.L2740:
	movl	16(%rbx), %eax
	movdqu	(%rbx), %xmm0
	leaq	48(%r12), %rdi
	movdqu	32(%rbx), %xmm6
	movq	48(%rbx), %rsi
	movl	%eax, 16(%r12)
	movq	24(%rbx), %rax
	movq	56(%rbx), %rdx
	movups	%xmm0, (%r12)
	movq	%rax, 24(%r12)
	leaq	64(%r12), %rax
	movq	%rax, 48(%r12)
	addq	%rsi, %rdx
	movups	%xmm6, 32(%r12)
.LEHB329:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag.constprop.0
.LEHE329:
	movq	80(%rbx), %rax
	addq	$104, %rbx
	addq	$104, %r12
	movq	%rax, -24(%r12)
	movq	-16(%rbx), %rax
	movq	%rax, -16(%r12)
	movzbl	-8(%rbx), %eax
	movb	%al, -8(%r12)
	movzbl	-7(%rbx), %eax
	movb	%al, -7(%r12)
	movzbl	-6(%rbx), %eax
	movb	%al, -6(%r12)
	cmpq	%rbx, %r14
	jne	.L2740
	movq	-56(%rbp), %rdi
	movq	8(%r15), %r14
	movq	%r12, %rcx
	leaq	64(%rdi), %rax
	cmpq	%r14, %rdi
	jne	.L2751
	jmp	.L2742
	.p2align 4,,10
	.p2align 3
.L2749:
	movq	%rdx, 48(%rcx)
	movq	(%rax), %rdx
	movq	%rdx, 64(%rcx)
.L2750:
	movq	-8(%rax), %rdx
	movq	%rax, -16(%rax)
	addq	$104, %rcx
	movq	$0, -8(%rax)
	movq	%rdx, -48(%rcx)
	movq	16(%rax), %rdx
	movb	$0, (%rax)
	movq	%rdx, -24(%rcx)
	movq	24(%rax), %rdx
	movq	%rdx, -16(%rcx)
	movzbl	32(%rax), %edx
	movb	%dl, -8(%rcx)
	movzbl	33(%rax), %edx
	movb	%dl, -7(%rcx)
	movzbl	34(%rax), %edx
	movb	%dl, -6(%rcx)
	leaq	104(%rax), %rdx
	addq	$40, %rax
	cmpq	%rax, %r14
	je	.L2815
	movq	%rdx, %rax
.L2751:
	movl	-48(%rax), %edx
	movdqu	-64(%rax), %xmm7
	movdqu	-32(%rax), %xmm0
	movl	%edx, 16(%rcx)
	movq	-40(%rax), %rdx
	movups	%xmm7, (%rcx)
	movq	%rdx, 24(%rcx)
	leaq	64(%rcx), %rdx
	movq	%rdx, 48(%rcx)
	movq	-16(%rax), %rdx
	movups	%xmm0, 32(%rcx)
	cmpq	%rax, %rdx
	jne	.L2749
	movdqu	(%rax), %xmm6
	movups	%xmm6, 64(%rcx)
	jmp	.L2750
	.p2align 4,,10
	.p2align 3
.L2753:
	addq	$104, %rbx
	cmpq	%r14, %rbx
	jne	.L2756
	jmp	.L2754
	.p2align 4,,10
	.p2align 3
.L2810:
	movq	%r13, %r12
	movq	%r13, %rdx
	subq	%r8, %r12
	leaq	64(%r12), %rax
	jmp	.L2696
	.p2align 4,,10
	.p2align 3
.L2694:
	movq	%rsi, 48(%rdx)
	movq	(%rax), %rsi
	movq	%rsi, 64(%rdx)
.L2695:
	movq	-8(%rax), %rsi
	addq	$104, %rdx
	leaq	104(%rax), %rdi
	movq	%rsi, -48(%rdx)
	movq	16(%rax), %rsi
	movq	%rax, -16(%rax)
	movq	$0, -8(%rax)
	movb	$0, (%rax)
	movq	%rsi, -24(%rdx)
	movq	24(%rax), %rsi
	movq	%rsi, -16(%rdx)
	movzbl	32(%rax), %esi
	movb	%sil, -8(%rdx)
	movzbl	33(%rax), %esi
	movb	%sil, -7(%rdx)
	movzbl	34(%rax), %esi
	movb	%sil, -6(%rdx)
	leaq	40(%rax), %rsi
	cmpq	%rsi, %r13
	je	.L2816
	movq	%rdi, %rax
.L2696:
	movdqu	-64(%rax), %xmm1
	movups	%xmm1, (%rdx)
	movl	-48(%rax), %esi
	movl	%esi, 16(%rdx)
	movq	-40(%rax), %rsi
	movq	%rsi, 24(%rdx)
	movdqu	-32(%rax), %xmm2
	leaq	64(%rdx), %rsi
	movq	%rsi, 48(%rdx)
	movups	%xmm2, 32(%rdx)
	movq	-16(%rax), %rsi
	cmpq	%rax, %rsi
	jne	.L2694
	movdqu	(%rax), %xmm7
	movups	%xmm7, 64(%rdx)
	jmp	.L2695
	.p2align 4,,10
	.p2align 3
.L2816:
	movq	%r12, %rdx
	subq	-56(%rbp), %rdx
	addq	%r8, 8(%r15)
	subq	$40, %r12
	movabsq	$5675921253449092805, %rsi
	movq	%rdx, %r13
	movq	%rax, %rbx
	sarq	$3, %r13
	imulq	%rsi, %r13
	testq	%rdx, %rdx
	jg	.L2710
	jmp	.L2709
	.p2align 4,,10
	.p2align 3
.L2819:
	movq	-24(%r12), %rax
	cmpb	$0, -32(%rbx)
	movq	%rax, -24(%rbx)
	jne	.L2702
	movb	$1, -32(%rbx)
.L2702:
	movq	-16(%r12), %rax
	movq	-16(%rbx), %rdi
	cmpq	%r12, %rax
	je	.L2817
.L2703:
	cmpq	%rbx, %rdi
	je	.L2818
	movq	%rax, -16(%rbx)
	movq	-8(%r12), %rax
	movq	(%rbx), %rdx
	movq	%rax, -8(%rbx)
	movq	(%r12), %rax
	movq	%rax, (%rbx)
	testq	%rdi, %rdi
	je	.L2708
	movq	%rdi, -16(%r12)
	movq	%rdx, (%r12)
.L2706:
	movq	-16(%r12), %rax
	subq	$104, %rbx
	subq	$104, %r12
	movq	$0, 96(%r12)
	movb	$0, (%rax)
	movq	120(%r12), %rax
	movq	%rax, 120(%rbx)
	movq	128(%r12), %rax
	movq	%rax, 128(%rbx)
	movzbl	136(%r12), %eax
	movb	%al, 136(%rbx)
	movzbl	137(%r12), %eax
	movb	%al, 137(%rbx)
	movzbl	138(%r12), %eax
	movb	%al, 138(%rbx)
	subq	$1, %r13
	je	.L2709
.L2710:
	movdqu	-64(%r12), %xmm3
	movups	%xmm3, -64(%rbx)
	movl	-48(%r12), %eax
	movl	%eax, -48(%rbx)
	movq	-40(%r12), %rax
	movq	%rax, -40(%rbx)
	cmpb	$0, -32(%r12)
	jne	.L2819
	cmpb	$0, -32(%rbx)
	je	.L2702
	movb	$0, -32(%rbx)
	movq	-16(%r12), %rax
	movq	-16(%rbx), %rdi
	cmpq	%r12, %rax
	jne	.L2703
.L2817:
	movq	-8(%r12), %rdx
	testq	%rdx, %rdx
	je	.L2704
	cmpq	$1, %rdx
	je	.L2820
	movq	%r12, %rsi
	movq	%rcx, -80(%rbp)
	movq	%r8, -64(%rbp)
	call	memcpy@PLT
	movq	-16(%rbx), %rdi
	movq	-8(%r12), %rdx
	movq	-80(%rbp), %rcx
	movq	-64(%rbp), %r8
.L2704:
	movq	%rdx, -8(%rbx)
	movb	$0, (%rdi,%rdx)
	jmp	.L2706
	.p2align 4,,10
	.p2align 3
.L2818:
	movq	%rax, -16(%rbx)
	movq	-8(%r12), %rax
	movq	%rax, -8(%rbx)
	movq	(%r12), %rax
	movq	%rax, (%rbx)
.L2708:
	movq	%r12, -16(%r12)
	jmp	.L2706
	.p2align 4,,10
	.p2align 3
.L2709:
	testq	%r8, %r8
	jle	.L2689
	movq	-56(%rbp), %rbx
	movq	-72(%rbp), %r12
	addq	$48, %rbx
	addq	$48, %r12
	jmp	.L2714
	.p2align 4,,10
	.p2align 3
.L2821:
	movq	-8(%r12), %rdx
	movq	%rdx, -8(%rbx)
	testb	%al, %al
	jne	.L2713
	movb	$1, -16(%rbx)
.L2713:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%rcx, -56(%rbp)
	addq	$104, %rbx
.LEHB330:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	movq	32(%r12), %rax
	movq	-56(%rbp), %rcx
	addq	$104, %r12
	movq	%rax, -72(%rbx)
	movq	-64(%r12), %rax
	movq	%rax, -64(%rbx)
	movzbl	-56(%r12), %eax
	movb	%al, -56(%rbx)
	movzbl	-55(%r12), %eax
	movb	%al, -55(%rbx)
	movzbl	-54(%r12), %eax
	movb	%al, -54(%rbx)
	subq	$1, %rcx
	je	.L2689
.L2714:
	movdqu	-48(%r12), %xmm4
	movups	%xmm4, -48(%rbx)
	movl	-32(%r12), %eax
	movl	%eax, -32(%rbx)
	movq	-24(%r12), %rax
	movq	%rax, -24(%rbx)
	cmpb	$0, -16(%r12)
	movzbl	-16(%rbx), %eax
	jne	.L2821
	testb	%al, %al
	je	.L2713
	movb	$0, -16(%rbx)
	jmp	.L2713
	.p2align 4,,10
	.p2align 3
.L2820:
	movzbl	(%r12), %eax
	movb	%al, (%rdi)
	movq	-16(%rbx), %rdi
	movq	-8(%r12), %rdx
	jmp	.L2704
	.p2align 4,,10
	.p2align 3
.L2763:
	movq	%r13, %rcx
	jmp	.L2715
.L2767:
	movq	-64(%rbp), %rax
	movq	%rax, -80(%rbp)
	movq	%rax, %r12
	jmp	.L2736
.L2812:
	leaq	.LC99(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.LEHE330:
.L2774:
	endbr64
	movq	%rax, %rdi
	jmp	.L2743
.L2772:
	endbr64
	movq	%rax, %rdi
	jmp	.L2719
.L2743:
	call	__cxa_begin_catch@PLT
	movq	-80(%rbp), %rbx
.L2746:
	cmpq	%r12, %rbx
	jne	.L2822
.LEHB331:
	call	__cxa_rethrow@PLT
.LEHE331:
.L2719:
	call	__cxa_begin_catch@PLT
.L2722:
	cmpq	%r12, %r13
	jne	.L2823
.LEHB332:
	call	__cxa_rethrow@PLT
.LEHE332:
.L2823:
	movq	48(%r13), %rdi
	leaq	64(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2721
	call	_ZdlPv@PLT
.L2721:
	addq	$104, %r13
	jmp	.L2722
.L2822:
	movq	48(%rbx), %rdi
	leaq	64(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2745
	call	_ZdlPv@PLT
.L2745:
	addq	$104, %rbx
	jmp	.L2746
.L2771:
	endbr64
	movq	%rax, %r12
	jmp	.L2723
.L2773:
	endbr64
	movq	%rax, %r12
	jmp	.L2747
.L2723:
	call	__cxa_end_catch@PLT
	movq	%r12, %rdi
.LEHB333:
	call	_Unwind_Resume@PLT
.LEHE333:
.L2747:
	call	__cxa_end_catch@PLT
	movq	%r12, %rdi
	call	__cxa_begin_catch@PLT
	movq	-64(%rbp), %rbx
.L2748:
	cmpq	-80(%rbp), %rbx
	jne	.L2824
	cmpq	$0, -64(%rbp)
	je	.L2760
	movq	-64(%rbp), %rdi
	call	_ZdlPv@PLT
.L2760:
.LEHB334:
	call	__cxa_rethrow@PLT
.LEHE334:
.L2824:
	movq	48(%rbx), %rdi
	leaq	64(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2759
	call	_ZdlPv@PLT
.L2759:
	addq	$104, %rbx
	jmp	.L2748
.L2770:
	endbr64
	movq	%rax, %r12
.L2761:
	call	__cxa_end_catch@PLT
	movq	%r12, %rdi
.LEHB335:
	call	_Unwind_Resume@PLT
.LEHE335:
	.cfi_endproc
.LFE9991:
	.section	.gcc_except_table._ZNSt6vectorIN2v88internal6torque5FieldESaIS3_EE15_M_range_insertIN9__gnu_cxx17__normal_iteratorIPKS3_S5_EEEEvNS8_IPS3_S5_EET_SE_St20forward_iterator_tag,"aG",@progbits,_ZNSt6vectorIN2v88internal6torque5FieldESaIS3_EE15_M_range_insertIN9__gnu_cxx17__normal_iteratorIPKS3_S5_EEEEvNS8_IPS3_S5_EET_SE_St20forward_iterator_tag,comdat
	.align 4
.LLSDA9991:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATT9991-.LLSDATTD9991
.LLSDATTD9991:
	.byte	0x1
	.uleb128 .LLSDACSE9991-.LLSDACSB9991
.LLSDACSB9991:
	.uleb128 .LEHB327-.LFB9991
	.uleb128 .LEHE327-.LEHB327
	.uleb128 .L2772-.LFB9991
	.uleb128 0x1
	.uleb128 .LEHB328-.LFB9991
	.uleb128 .LEHE328-.LEHB328
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB329-.LFB9991
	.uleb128 .LEHE329-.LEHB329
	.uleb128 .L2774-.LFB9991
	.uleb128 0x1
	.uleb128 .LEHB330-.LFB9991
	.uleb128 .LEHE330-.LEHB330
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB331-.LFB9991
	.uleb128 .LEHE331-.LEHB331
	.uleb128 .L2773-.LFB9991
	.uleb128 0x3
	.uleb128 .LEHB332-.LFB9991
	.uleb128 .LEHE332-.LEHB332
	.uleb128 .L2771-.LFB9991
	.uleb128 0
	.uleb128 .LEHB333-.LFB9991
	.uleb128 .LEHE333-.LEHB333
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB334-.LFB9991
	.uleb128 .LEHE334-.LEHB334
	.uleb128 .L2770-.LFB9991
	.uleb128 0
	.uleb128 .LEHB335-.LFB9991
	.uleb128 .LEHE335-.LEHB335
	.uleb128 0
	.uleb128 0
.LLSDACSE9991:
	.byte	0x1
	.byte	0
	.byte	0
	.byte	0x7d
	.align 4
	.long	0

.LLSDATT9991:
	.section	.text._ZNSt6vectorIN2v88internal6torque5FieldESaIS3_EE15_M_range_insertIN9__gnu_cxx17__normal_iteratorIPKS3_S5_EEEEvNS8_IPS3_S5_EET_SE_St20forward_iterator_tag,"axG",@progbits,_ZNSt6vectorIN2v88internal6torque5FieldESaIS3_EE15_M_range_insertIN9__gnu_cxx17__normal_iteratorIPKS3_S5_EEEEvNS8_IPS3_S5_EET_SE_St20forward_iterator_tag,comdat
	.size	_ZNSt6vectorIN2v88internal6torque5FieldESaIS3_EE15_M_range_insertIN9__gnu_cxx17__normal_iteratorIPKS3_S5_EEEEvNS8_IPS3_S5_EET_SE_St20forward_iterator_tag, .-_ZNSt6vectorIN2v88internal6torque5FieldESaIS3_EE15_M_range_insertIN9__gnu_cxx17__normal_iteratorIPKS3_S5_EEEEvNS8_IPS3_S5_EET_SE_St20forward_iterator_tag
	.section	.text._ZNSt6vectorIPN2v88internal6torque10IdentifierESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_,"axG",@progbits,_ZNSt6vectorIPN2v88internal6torque10IdentifierESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPN2v88internal6torque10IdentifierESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	.type	_ZNSt6vectorIPN2v88internal6torque10IdentifierESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_, @function
_ZNSt6vectorIPN2v88internal6torque10IdentifierESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_:
.LFB10054:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r9
	movq	%r13, %rax
	subq	%r9, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L2839
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L2835
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L2840
.L2827:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L2834:
	movq	(%r15), %rax
	subq	%r8, %r13
	leaq	8(%rbx,%rdx), %r15
	movq	%rax, (%rbx,%rdx)
	leaq	(%r15,%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L2841
	testq	%r13, %r13
	jg	.L2830
	testq	%r9, %r9
	jne	.L2833
.L2831:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2841:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jg	.L2830
.L2833:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L2831
	.p2align 4,,10
	.p2align 3
.L2840:
	testq	%rsi, %rsi
	jne	.L2828
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L2834
	.p2align 4,,10
	.p2align 3
.L2830:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L2831
	jmp	.L2833
	.p2align 4,,10
	.p2align 3
.L2835:
	movl	$8, %r14d
	jmp	.L2827
.L2839:
	leaq	.LC84(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L2828:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$3, %r14
	jmp	.L2827
	.cfi_endproc
.LFE10054:
	.size	_ZNSt6vectorIPN2v88internal6torque10IdentifierESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_, .-_ZNSt6vectorIPN2v88internal6torque10IdentifierESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	.section	.text._ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE7_M_copyINSB_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS5_EPKSF_PSt18_Rb_tree_node_baseRT_,"axG",@progbits,_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE7_M_copyINSB_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS5_EPKSF_PSt18_Rb_tree_node_baseRT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE7_M_copyINSB_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS5_EPKSF_PSt18_Rb_tree_node_baseRT_
	.type	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE7_M_copyINSB_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS5_EPKSF_PSt18_Rb_tree_node_baseRT_, @function
_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE7_M_copyINSB_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS5_EPKSF_PSt18_Rb_tree_node_baseRT_:
.LFB10331:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA10331
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$56, %rsp
	movq	%rdi, -80(%rbp)
	movl	$64, %edi
	movq	%rcx, -88(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
.LEHB336:
	call	_Znwm@PLT
.LEHE336:
	movq	32(%rbx), %rsi
	movq	40(%rbx), %rdx
	movq	%rax, %r14
	leaq	32(%rax), %rdi
	leaq	48(%rax), %rax
	addq	%rsi, %rdx
	movq	%rax, 32(%r14)
.LEHB337:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag.constprop.0
.LEHE337:
	movl	(%rbx), %eax
	movq	24(%rbx), %rsi
	movq	%r12, 8(%r14)
	movq	$0, 16(%r14)
	movl	%eax, (%r14)
	movq	$0, 24(%r14)
	testq	%rsi, %rsi
	je	.L2846
	movq	-88(%rbp), %rcx
	movq	-80(%rbp), %rdi
	movq	%r14, %rdx
.LEHB338:
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE7_M_copyINSB_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS5_EPKSF_PSt18_Rb_tree_node_baseRT_
	movq	%rax, 24(%r14)
.L2846:
	movq	16(%rbx), %rbx
	leaq	-64(%rbp), %rax
	movq	%r14, %r12
	movq	%rax, -96(%rbp)
	testq	%rbx, %rbx
	je	.L2842
.L2847:
	movl	$64, %edi
	movq	%r12, -72(%rbp)
	call	_Znwm@PLT
	leaq	48(%rax), %rdi
	movq	%rax, %r12
	movq	%rdi, 32(%rax)
	movq	32(%rbx), %r15
	movq	40(%rbx), %r13
	movq	%r15, %rax
	addq	%r13, %rax
	je	.L2848
	testq	%r15, %r15
	je	.L2886
.L2848:
	movq	%r13, -64(%rbp)
	cmpq	$15, %r13
	ja	.L2887
	cmpq	$1, %r13
	jne	.L2851
	movzbl	(%r15), %eax
	movb	%al, 48(%r12)
.L2852:
	movq	%r13, 40(%r12)
	movb	$0, (%rdi,%r13)
	movl	(%rbx), %eax
	movq	$0, 16(%r12)
	movl	%eax, (%r12)
	movq	-72(%rbp), %rax
	movq	$0, 24(%r12)
	movq	%r12, 16(%rax)
	movq	%rax, 8(%r12)
	movq	24(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L2854
	movq	-88(%rbp), %rcx
	movq	-80(%rbp), %rdi
	movq	%r12, %rdx
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE7_M_copyINSB_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS5_EPKSF_PSt18_Rb_tree_node_baseRT_
.LEHE338:
	movq	%rax, 24(%r12)
.L2854:
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L2847
.L2842:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2888
	addq	$56, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2851:
	.cfi_restore_state
	testq	%r13, %r13
	je	.L2852
	jmp	.L2850
	.p2align 4,,10
	.p2align 3
.L2887:
	movq	-96(%rbp), %rsi
	leaq	32(%r12), %rdi
	xorl	%edx, %edx
.LEHB339:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, 32(%r12)
	movq	%rax, %rdi
	movq	-64(%rbp), %rax
	movq	%rax, 48(%r12)
.L2850:
	movq	%r13, %rdx
	movq	%r15, %rsi
	call	memcpy@PLT
	movq	-64(%rbp), %r13
	movq	32(%r12), %rdi
	jmp	.L2852
.L2886:
	leaq	.LC1(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE339:
.L2888:
	call	__stack_chk_fail@PLT
.L2866:
	endbr64
	movq	%rax, %rdi
	jmp	.L2855
.L2862:
	endbr64
	movq	%rax, %rdi
	jmp	.L2857
.L2864:
	endbr64
	movq	%rax, %rdi
	jmp	.L2844
.L2855:
	call	__cxa_begin_catch@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.LEHB340:
	call	__cxa_rethrow@PLT
.LEHE340:
.L2856:
	call	__cxa_end_catch@PLT
	movq	%rbx, %rdi
.L2857:
	call	__cxa_begin_catch@PLT
	movq	-80(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
.LEHB341:
	call	__cxa_rethrow@PLT
.LEHE341:
.L2844:
	call	__cxa_begin_catch@PLT
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.LEHB342:
	call	__cxa_rethrow@PLT
.LEHE342:
.L2867:
	endbr64
	movq	%rax, %rbx
	jmp	.L2856
.L2863:
	endbr64
	movq	%rax, %r12
	jmp	.L2859
.L2865:
	endbr64
	movq	%rax, %r12
	jmp	.L2845
.L2859:
	call	__cxa_end_catch@PLT
	movq	%r12, %rdi
.LEHB343:
	call	_Unwind_Resume@PLT
.LEHE343:
.L2845:
	call	__cxa_end_catch@PLT
	movq	%r12, %rdi
.LEHB344:
	call	_Unwind_Resume@PLT
.LEHE344:
	.cfi_endproc
.LFE10331:
	.section	.gcc_except_table._ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE7_M_copyINSB_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS5_EPKSF_PSt18_Rb_tree_node_baseRT_,"aG",@progbits,_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE7_M_copyINSB_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS5_EPKSF_PSt18_Rb_tree_node_baseRT_,comdat
	.align 4
.LLSDA10331:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATT10331-.LLSDATTD10331
.LLSDATTD10331:
	.byte	0x1
	.uleb128 .LLSDACSE10331-.LLSDACSB10331
.LLSDACSB10331:
	.uleb128 .LEHB336-.LFB10331
	.uleb128 .LEHE336-.LEHB336
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB337-.LFB10331
	.uleb128 .LEHE337-.LEHB337
	.uleb128 .L2864-.LFB10331
	.uleb128 0x1
	.uleb128 .LEHB338-.LFB10331
	.uleb128 .LEHE338-.LEHB338
	.uleb128 .L2862-.LFB10331
	.uleb128 0x1
	.uleb128 .LEHB339-.LFB10331
	.uleb128 .LEHE339-.LEHB339
	.uleb128 .L2866-.LFB10331
	.uleb128 0x1
	.uleb128 .LEHB340-.LFB10331
	.uleb128 .LEHE340-.LEHB340
	.uleb128 .L2867-.LFB10331
	.uleb128 0x3
	.uleb128 .LEHB341-.LFB10331
	.uleb128 .LEHE341-.LEHB341
	.uleb128 .L2863-.LFB10331
	.uleb128 0
	.uleb128 .LEHB342-.LFB10331
	.uleb128 .LEHE342-.LEHB342
	.uleb128 .L2865-.LFB10331
	.uleb128 0
	.uleb128 .LEHB343-.LFB10331
	.uleb128 .LEHE343-.LEHB343
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB344-.LFB10331
	.uleb128 .LEHE344-.LEHB344
	.uleb128 0
	.uleb128 0
.LLSDACSE10331:
	.byte	0x1
	.byte	0
	.byte	0
	.byte	0x7d
	.align 4
	.long	0

.LLSDATT10331:
	.section	.text._ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE7_M_copyINSB_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS5_EPKSF_PSt18_Rb_tree_node_baseRT_,"axG",@progbits,_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE7_M_copyINSB_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS5_EPKSF_PSt18_Rb_tree_node_baseRT_,comdat
	.size	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE7_M_copyINSB_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS5_EPKSF_PSt18_Rb_tree_node_baseRT_, .-_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE7_M_copyINSB_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS5_EPKSF_PSt18_Rb_tree_node_baseRT_
	.section	.text._ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE7_M_copyINSA_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS5_EPKSE_PSt18_Rb_tree_node_baseRT_,"axG",@progbits,_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE7_M_copyINSA_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS5_EPKSE_PSt18_Rb_tree_node_baseRT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE7_M_copyINSA_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS5_EPKSE_PSt18_Rb_tree_node_baseRT_
	.type	_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE7_M_copyINSA_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS5_EPKSE_PSt18_Rb_tree_node_baseRT_, @function
_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE7_M_copyINSA_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS5_EPKSE_PSt18_Rb_tree_node_baseRT_:
.LFB10338:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA10338
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	movl	$40, %edi
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	%rcx, -56(%rbp)
.LEHB345:
	call	_Znwm@PLT
.LEHE345:
	movq	24(%rbx), %rsi
	movq	%rax, %r13
	movq	32(%rbx), %rax
	movq	$0, 16(%r13)
	movq	%rax, 32(%r13)
	movl	(%rbx), %eax
	movq	$0, 24(%r13)
	movl	%eax, 0(%r13)
	movq	%r12, 8(%r13)
	testq	%rsi, %rsi
	je	.L2890
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r15, %rdi
.LEHB346:
	call	_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE7_M_copyINSA_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS5_EPKSE_PSt18_Rb_tree_node_baseRT_
	movq	%rax, 24(%r13)
.L2890:
	movq	16(%rbx), %r12
	testq	%r12, %r12
	je	.L2889
	movq	%r13, %rbx
.L2893:
	movl	$40, %edi
	movq	%rbx, %r14
	call	_Znwm@PLT
	movq	%rax, %rbx
	movq	32(%r12), %rax
	movq	%rax, 32(%rbx)
	movl	(%r12), %eax
	movq	$0, 16(%rbx)
	movl	%eax, (%rbx)
	movq	$0, 24(%rbx)
	movq	%rbx, 16(%r14)
	movq	%r14, 8(%rbx)
	movq	24(%r12), %rsi
	testq	%rsi, %rsi
	je	.L2892
	movq	-56(%rbp), %rcx
	movq	%rbx, %rdx
	movq	%r15, %rdi
	call	_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE7_M_copyINSA_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS5_EPKSE_PSt18_Rb_tree_node_baseRT_
.LEHE346:
	movq	%rax, 24(%rbx)
.L2892:
	movq	16(%r12), %r12
	testq	%r12, %r12
	jne	.L2893
.L2889:
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2896:
	.cfi_restore_state
	endbr64
	movq	%rax, %rdi
.L2894:
	call	__cxa_begin_catch@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
.LEHB347:
	call	__cxa_rethrow@PLT
.LEHE347:
.L2897:
	endbr64
	movq	%rax, %r12
.L2895:
	call	__cxa_end_catch@PLT
	movq	%r12, %rdi
.LEHB348:
	call	_Unwind_Resume@PLT
.LEHE348:
	.cfi_endproc
.LFE10338:
	.section	.gcc_except_table._ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE7_M_copyINSA_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS5_EPKSE_PSt18_Rb_tree_node_baseRT_,"aG",@progbits,_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE7_M_copyINSA_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS5_EPKSE_PSt18_Rb_tree_node_baseRT_,comdat
	.align 4
.LLSDA10338:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATT10338-.LLSDATTD10338
.LLSDATTD10338:
	.byte	0x1
	.uleb128 .LLSDACSE10338-.LLSDACSB10338
.LLSDACSB10338:
	.uleb128 .LEHB345-.LFB10338
	.uleb128 .LEHE345-.LEHB345
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB346-.LFB10338
	.uleb128 .LEHE346-.LEHB346
	.uleb128 .L2896-.LFB10338
	.uleb128 0x1
	.uleb128 .LEHB347-.LFB10338
	.uleb128 .LEHE347-.LEHB347
	.uleb128 .L2897-.LFB10338
	.uleb128 0
	.uleb128 .LEHB348-.LFB10338
	.uleb128 .LEHE348-.LEHB348
	.uleb128 0
	.uleb128 0
.LLSDACSE10338:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATT10338:
	.section	.text._ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE7_M_copyINSA_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS5_EPKSE_PSt18_Rb_tree_node_baseRT_,"axG",@progbits,_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE7_M_copyINSA_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS5_EPKSE_PSt18_Rb_tree_node_baseRT_,comdat
	.size	_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE7_M_copyINSA_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS5_EPKSE_PSt18_Rb_tree_node_baseRT_, .-_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE7_M_copyINSA_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS5_EPKSE_PSt18_Rb_tree_node_baseRT_
	.section	.text._ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS5_ERKS5_,"axG",@progbits,_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS5_ERKS5_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS5_ERKS5_
	.type	_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS5_ERKS5_, @function
_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS5_ERKS5_:
.LFB10359:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	8(%rdi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	cmpq	%rax, %rsi
	je	.L2930
	movq	%rsi, %rbx
	movq	(%rdx), %rdi
	movq	32(%rsi), %rsi
	call	_ZN2v88internal6torqueltERKNS1_4TypeES4_
	testb	%al, %al
	je	.L2913
	movq	24(%r12), %rax
	movq	%rax, %rdx
	cmpq	%rbx, %rax
	je	.L2912
	movq	%rbx, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	0(%r13), %rsi
	movq	32(%rax), %rdi
	movq	%rax, %r14
	call	_ZN2v88internal6torqueltERKNS1_4TypeES4_
	testb	%al, %al
	je	.L2915
	cmpq	$0, 24(%r14)
	movl	$0, %eax
	cmovne	%rbx, %rax
	cmove	%r14, %rbx
	movq	%rbx, %rdx
.L2912:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2930:
	.cfi_restore_state
	cmpq	$0, 40(%rdi)
	je	.L2915
	movq	32(%rdi), %rax
	movq	(%rdx), %rsi
	movq	32(%rax), %rdi
	call	_ZN2v88internal6torqueltERKNS1_4TypeES4_
	testb	%al, %al
	je	.L2915
	popq	%rbx
	movq	32(%r12), %rdx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2915:
	.cfi_restore_state
	popq	%rbx
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE24_M_get_insert_unique_posERKS5_
	.p2align 4,,10
	.p2align 3
.L2913:
	.cfi_restore_state
	movq	32(%rbx), %rdi
	movq	0(%r13), %rsi
	call	_ZN2v88internal6torqueltERKNS1_4TypeES4_
	testb	%al, %al
	je	.L2917
	movq	32(%r12), %rdx
	xorl	%eax, %eax
	cmpq	%rbx, %rdx
	je	.L2912
	movq	%rbx, %rdi
	call	_ZSt18_Rb_tree_incrementPSt18_Rb_tree_node_base@PLT
	movq	0(%r13), %rdi
	movq	32(%rax), %rsi
	movq	%rax, %r14
	call	_ZN2v88internal6torqueltERKNS1_4TypeES4_
	testb	%al, %al
	je	.L2915
	cmpq	$0, 24(%rbx)
	movl	$0, %eax
	cmovne	%r14, %rbx
	cmovne	%r14, %rax
	movq	%rbx, %rdx
	jmp	.L2912
	.p2align 4,,10
	.p2align 3
.L2917:
	movq	%rbx, %rax
	xorl	%edx, %edx
	jmp	.L2912
	.cfi_endproc
.LFE10359:
	.size	_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS5_ERKS5_, .-_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS5_ERKS5_
	.section	.text._ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_PKN2v88internal6torque13AggregateTypeEESt10_Select1stISE_ESt4lessIS5_ESaISE_EE24_M_get_insert_unique_posERS7_,"axG",@progbits,_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_PKN2v88internal6torque13AggregateTypeEESt10_Select1stISE_ESt4lessIS5_ESaISE_EE24_M_get_insert_unique_posERS7_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_PKN2v88internal6torque13AggregateTypeEESt10_Select1stISE_ESt4lessIS5_ESaISE_EE24_M_get_insert_unique_posERS7_
	.type	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_PKN2v88internal6torque13AggregateTypeEESt10_Select1stISE_ESt4lessIS5_ESaISE_EE24_M_get_insert_unique_posERS7_, @function
_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_PKN2v88internal6torque13AggregateTypeEESt10_Select1stISE_ESt4lessIS5_ESaISE_EE24_M_get_insert_unique_posERS7_:
.LFB10647:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r15
	movq	%rdi, -72(%rbp)
	movq	%rsi, -64(%rbp)
	testq	%r15, %r15
	je	.L2957
	movq	8(%rsi), %r14
	movq	(%rsi), %rbx
	jmp	.L2934
	.p2align 4,,10
	.p2align 3
.L2939:
	movq	16(%r15), %rax
	movl	$1, %esi
	testq	%rax, %rax
	je	.L2935
.L2958:
	movq	%rax, %r15
.L2934:
	movq	40(%r15), %r12
	movq	32(%r15), %r13
	cmpq	%r12, %r14
	movq	%r12, %rdx
	cmovbe	%r14, %rdx
	testq	%rdx, %rdx
	je	.L2936
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movq	%rdx, -56(%rbp)
	call	memcmp@PLT
	movq	-56(%rbp), %rdx
	testl	%eax, %eax
	jne	.L2937
.L2936:
	movq	%r14, %rax
	movl	$2147483648, %ecx
	subq	%r12, %rax
	cmpq	%rcx, %rax
	jge	.L2938
	movabsq	$-2147483649, %rdi
	cmpq	%rdi, %rax
	jle	.L2939
.L2937:
	testl	%eax, %eax
	js	.L2939
.L2938:
	movq	24(%r15), %rax
	xorl	%esi, %esi
	testq	%rax, %rax
	jne	.L2958
.L2935:
	movq	%r15, %r8
	testb	%sil, %sil
	jne	.L2933
.L2941:
	testq	%rdx, %rdx
	je	.L2944
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%r8, -56(%rbp)
	call	memcmp@PLT
	movq	-56(%rbp), %r8
	testl	%eax, %eax
	jne	.L2945
.L2944:
	movq	%r12, %rcx
	subq	%r14, %rcx
	cmpq	$2147483647, %rcx
	jg	.L2946
	cmpq	$-2147483648, %rcx
	jl	.L2947
	movl	%ecx, %eax
.L2945:
	testl	%eax, %eax
	js	.L2947
.L2946:
	addq	$40, %rsp
	movq	%r15, %rax
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2947:
	.cfi_restore_state
	addq	$40, %rsp
	xorl	%eax, %eax
	movq	%r8, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2957:
	.cfi_restore_state
	leaq	8(%rdi), %r15
.L2933:
	movq	-72(%rbp), %rax
	cmpq	24(%rax), %r15
	je	.L2959
	movq	%r15, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	-64(%rbp), %rbx
	movq	%r15, %r8
	movq	40(%rax), %r12
	movq	32(%rax), %r13
	movq	%rax, %r15
	movq	8(%rbx), %r14
	movq	(%rbx), %rbx
	cmpq	%r14, %r12
	movq	%r14, %rdx
	cmovbe	%r12, %rdx
	jmp	.L2941
	.p2align 4,,10
	.p2align 3
.L2959:
	addq	$40, %rsp
	movq	%r15, %rdx
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10647:
	.size	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_PKN2v88internal6torque13AggregateTypeEESt10_Select1stISE_ESt4lessIS5_ESaISE_EE24_M_get_insert_unique_posERS7_, .-_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_PKN2v88internal6torque13AggregateTypeEESt10_Select1stISE_ESt4lessIS5_ESaISE_EE24_M_get_insert_unique_posERS7_
	.section	.text._ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_PKN2v88internal6torque13AggregateTypeEESt10_Select1stISE_ESt4lessIS5_ESaISE_EE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS7_EESP_IJEEEEESt17_Rb_tree_iteratorISE_ESt23_Rb_tree_const_iteratorISE_EDpOT_,"axG",@progbits,_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_PKN2v88internal6torque13AggregateTypeEESt10_Select1stISE_ESt4lessIS5_ESaISE_EE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS7_EESP_IJEEEEESt17_Rb_tree_iteratorISE_ESt23_Rb_tree_const_iteratorISE_EDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_PKN2v88internal6torque13AggregateTypeEESt10_Select1stISE_ESt4lessIS5_ESaISE_EE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS7_EESP_IJEEEEESt17_Rb_tree_iteratorISE_ESt23_Rb_tree_const_iteratorISE_EDpOT_
	.type	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_PKN2v88internal6torque13AggregateTypeEESt10_Select1stISE_ESt4lessIS5_ESaISE_EE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS7_EESP_IJEEEEESt17_Rb_tree_iteratorISE_ESt23_Rb_tree_const_iteratorISE_EDpOT_, @function
_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_PKN2v88internal6torque13AggregateTypeEESt10_Select1stISE_ESt4lessIS5_ESaISE_EE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS7_EESP_IJEEEEESt17_Rb_tree_iteratorISE_ESt23_Rb_tree_const_iteratorISE_EDpOT_:
.LFB9021:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA9021
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movl	$72, %edi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$88, %rsp
.LEHB349:
	call	_Znwm@PLT
.LEHE349:
	movq	%rax, %r12
	movq	(%r14), %rax
	leaq	48(%r12), %rcx
	leaq	32(%r12), %rdi
	movq	%rcx, 32(%r12)
	movq	(%rax), %rsi
	movq	8(%rax), %rdx
	movq	%rdi, -64(%rbp)
	movq	%rcx, -72(%rbp)
	addq	%rsi, %rdx
.LEHB350:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag.constprop.0
.LEHE350:
	movq	$0, 64(%r12)
	leaq	8(%r13), %rax
	movq	%rbx, %r15
	movq	%rax, -56(%rbp)
	cmpq	%rbx, %rax
	je	.L3025
	movq	40(%r12), %r14
	movq	40(%rbx), %r9
	movq	32(%rbx), %r8
	movq	32(%r12), %r11
	cmpq	%r9, %r14
	movq	%r9, %r10
	cmovbe	%r14, %r10
	testq	%r10, %r10
	je	.L2970
	movq	%r10, %rdx
	movq	%r8, %rsi
	movq	%r11, %rdi
	movq	%r9, -104(%rbp)
	movq	%r10, -96(%rbp)
	movq	%r8, -88(%rbp)
	movq	%r11, -80(%rbp)
	call	memcmp@PLT
	movq	-80(%rbp), %r11
	movq	-88(%rbp), %r8
	testl	%eax, %eax
	movq	-96(%rbp), %r10
	movq	-104(%rbp), %r9
	jne	.L3026
	movq	%r14, %rax
	subq	%r9, %rax
	cmpq	$2147483647, %rax
	jg	.L2996
.L2997:
	cmpq	$-2147483648, %rax
	jl	.L2973
	testl	%eax, %eax
	js	.L2973
	testq	%r10, %r10
	je	.L2980
.L2996:
	movq	%r10, %rdx
	movq	%r11, %rsi
	movq	%r8, %rdi
	movq	%r9, -104(%rbp)
	movq	%r10, -96(%rbp)
	movq	%r11, -88(%rbp)
	movq	%r8, -80(%rbp)
	call	memcmp@PLT
	movq	-80(%rbp), %r8
	movq	-88(%rbp), %r11
	testl	%eax, %eax
	movq	-96(%rbp), %r10
	movq	-104(%rbp), %r9
	jne	.L2981
.L2980:
	movq	%r9, %rax
	subq	%r14, %rax
	cmpq	$2147483647, %rax
	jg	.L2982
	cmpq	$-2147483648, %rax
	jl	.L2983
.L2981:
	testl	%eax, %eax
	js	.L2983
.L2982:
	cmpq	-72(%rbp), %r11
	je	.L2994
	movq	%r11, %rdi
	call	_ZdlPv@PLT
.L2994:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	addq	$88, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2970:
	.cfi_restore_state
	movq	%r14, %rax
	subq	%r9, %rax
	cmpq	$2147483647, %rax
	jle	.L2997
	jmp	.L2980
	.p2align 4,,10
	.p2align 3
.L3026:
	jns	.L2996
.L2973:
	movq	%r11, -80(%rbp)
	cmpq	%rbx, 24(%r13)
	je	.L2990
	movq	%rbx, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	40(%rax), %r9
	movq	%rax, %r10
	cmpq	%r9, %r14
	movq	%r9, %rdx
	cmovbe	%r14, %rdx
	testq	%rdx, %rdx
	je	.L2976
	movq	-80(%rbp), %r11
	movq	32(%rax), %rdi
	movq	%r9, -96(%rbp)
	movq	%rax, -88(%rbp)
	movq	%r11, %rsi
	call	memcmp@PLT
	movq	-88(%rbp), %r10
	movq	-96(%rbp), %r9
	testl	%eax, %eax
	jne	.L2977
.L2976:
	subq	%r14, %r9
	cmpq	$2147483647, %r9
	jg	.L2986
	cmpq	$-2147483648, %r9
	jl	.L2979
	movl	%r9d, %eax
.L2977:
	testl	%eax, %eax
	jns	.L2986
.L2979:
	cmpq	$0, 24(%r10)
	je	.L2999
	.p2align 4,,10
	.p2align 3
.L2990:
	testq	%rbx, %rbx
	setne	%al
.L2995:
	cmpq	%r15, -56(%rbp)
	je	.L3003
	testb	%al, %al
	je	.L3027
.L3003:
	movl	$1, %edi
.L2989:
	movq	-56(%rbp), %rcx
	movq	%r15, %rdx
	movq	%r12, %rsi
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 40(%r13)
	addq	$88, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3025:
	.cfi_restore_state
	cmpq	$0, 40(%r13)
	je	.L2986
	movq	32(%r13), %r15
	movq	40(%r12), %r14
	movq	40(%r15), %rbx
	movq	%r14, %rdx
	cmpq	%r14, %rbx
	cmovbe	%rbx, %rdx
	testq	%rdx, %rdx
	je	.L2966
	movq	32(%r12), %rsi
	movq	32(%r15), %rdi
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L2967
.L2966:
	subq	%r14, %rbx
	cmpq	$2147483647, %rbx
	jg	.L2986
	cmpq	$-2147483648, %rbx
	jl	.L2968
	movl	%ebx, %eax
.L2967:
	testl	%eax, %eax
	js	.L2968
.L2986:
	movq	-64(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_PKN2v88internal6torque13AggregateTypeEESt10_Select1stISE_ESt4lessIS5_ESaISE_EE24_M_get_insert_unique_posERS7_
	movq	%rax, %rbx
	movq	%rdx, %r15
	testq	%rdx, %rdx
	jne	.L2990
	movq	32(%r12), %r11
	movq	%rax, %r15
	jmp	.L2982
	.p2align 4,,10
	.p2align 3
.L2983:
	movq	%r8, -104(%rbp)
	movq	%r10, -96(%rbp)
	movq	%r9, -88(%rbp)
	movq	%r11, -80(%rbp)
	cmpq	%rbx, 32(%r13)
	je	.L3000
	movq	%rbx, %rdi
	call	_ZSt18_Rb_tree_incrementPSt18_Rb_tree_node_base@PLT
	movq	-80(%rbp), %r11
	movq	-88(%rbp), %r9
	movq	%rax, %rcx
	movq	40(%rax), %rax
	movq	-96(%rbp), %r10
	movq	-104(%rbp), %r8
	cmpq	%rax, %r14
	movq	%rax, %rdx
	movq	%rax, -112(%rbp)
	cmovbe	%r14, %rdx
	testq	%rdx, %rdx
	je	.L2984
	movq	32(%rcx), %rsi
	movq	%r11, %rdi
	movq	%r8, -120(%rbp)
	movq	%r10, -104(%rbp)
	movq	%r9, -96(%rbp)
	movq	%rcx, -88(%rbp)
	call	memcmp@PLT
	movq	-80(%rbp), %r11
	movq	-88(%rbp), %rcx
	testl	%eax, %eax
	movq	-96(%rbp), %r9
	movq	-104(%rbp), %r10
	movq	-120(%rbp), %r8
	jne	.L2985
.L2984:
	movq	%r14, %rax
	subq	-112(%rbp), %rax
	cmpq	$2147483647, %rax
	jg	.L2986
	cmpq	$-2147483648, %rax
	jl	.L2987
.L2985:
	testl	%eax, %eax
	jns	.L2986
.L2987:
	cmpq	$0, 24(%rbx)
	je	.L2988
	movq	%rcx, %r15
	movl	$1, %edi
	jmp	.L2989
	.p2align 4,,10
	.p2align 3
.L2999:
	movq	%r10, %r15
.L2968:
	xorl	%eax, %eax
	jmp	.L2995
	.p2align 4,,10
	.p2align 3
.L3027:
	movq	40(%r12), %r14
	movq	40(%r15), %r9
	movq	32(%r15), %r8
	movq	32(%r12), %r11
	cmpq	%r14, %r9
	movq	%r14, %r10
	cmovbe	%r9, %r10
.L2988:
	testq	%r10, %r10
	je	.L2991
	movq	%r11, %rdi
	movq	%r10, %rdx
	movq	%r8, %rsi
	movq	%r9, -64(%rbp)
	call	memcmp@PLT
	movq	-64(%rbp), %r9
	testl	%eax, %eax
	movl	%eax, %edi
	jne	.L2992
.L2991:
	subq	%r9, %r14
	xorl	%edi, %edi
	cmpq	$2147483647, %r14
	jg	.L2989
	cmpq	$-2147483648, %r14
	jl	.L3003
	movl	%r14d, %edi
.L2992:
	shrl	$31, %edi
	jmp	.L2989
	.p2align 4,,10
	.p2align 3
.L3000:
	xorl	%ebx, %ebx
	jmp	.L2990
.L3005:
	endbr64
	movq	%rax, %rdi
.L2963:
	call	__cxa_begin_catch@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.LEHB351:
	call	__cxa_rethrow@PLT
.LEHE351:
.L3004:
	endbr64
	movq	%rax, %r12
.L2964:
	call	__cxa_end_catch@PLT
	movq	%r12, %rdi
.LEHB352:
	call	_Unwind_Resume@PLT
.LEHE352:
	.cfi_endproc
.LFE9021:
	.section	.gcc_except_table._ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_PKN2v88internal6torque13AggregateTypeEESt10_Select1stISE_ESt4lessIS5_ESaISE_EE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS7_EESP_IJEEEEESt17_Rb_tree_iteratorISE_ESt23_Rb_tree_const_iteratorISE_EDpOT_,"aG",@progbits,_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_PKN2v88internal6torque13AggregateTypeEESt10_Select1stISE_ESt4lessIS5_ESaISE_EE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS7_EESP_IJEEEEESt17_Rb_tree_iteratorISE_ESt23_Rb_tree_const_iteratorISE_EDpOT_,comdat
	.align 4
.LLSDA9021:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATT9021-.LLSDATTD9021
.LLSDATTD9021:
	.byte	0x1
	.uleb128 .LLSDACSE9021-.LLSDACSB9021
.LLSDACSB9021:
	.uleb128 .LEHB349-.LFB9021
	.uleb128 .LEHE349-.LEHB349
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB350-.LFB9021
	.uleb128 .LEHE350-.LEHB350
	.uleb128 .L3005-.LFB9021
	.uleb128 0x1
	.uleb128 .LEHB351-.LFB9021
	.uleb128 .LEHE351-.LEHB351
	.uleb128 .L3004-.LFB9021
	.uleb128 0
	.uleb128 .LEHB352-.LFB9021
	.uleb128 .LEHE352-.LEHB352
	.uleb128 0
	.uleb128 0
.LLSDACSE9021:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATT9021:
	.section	.text._ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_PKN2v88internal6torque13AggregateTypeEESt10_Select1stISE_ESt4lessIS5_ESaISE_EE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS7_EESP_IJEEEEESt17_Rb_tree_iteratorISE_ESt23_Rb_tree_const_iteratorISE_EDpOT_,"axG",@progbits,_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_PKN2v88internal6torque13AggregateTypeEESt10_Select1stISE_ESt4lessIS5_ESaISE_EE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS7_EESP_IJEEEEESt17_Rb_tree_iteratorISE_ESt23_Rb_tree_const_iteratorISE_EDpOT_,comdat
	.size	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_PKN2v88internal6torque13AggregateTypeEESt10_Select1stISE_ESt4lessIS5_ESaISE_EE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS7_EESP_IJEEEEESt17_Rb_tree_iteratorISE_ESt23_Rb_tree_const_iteratorISE_EDpOT_, .-_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_PKN2v88internal6torque13AggregateTypeEESt10_Select1stISE_ESt4lessIS5_ESaISE_EE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS7_EESP_IJEEEEESt17_Rb_tree_iteratorISE_ESt23_Rb_tree_const_iteratorISE_EDpOT_
	.section	.rodata._ZNK2v88internal6torque13AggregateType23CheckForDuplicateFieldsEv.str1.1,"aMS",@progbits,1
.LC100:
	.string	"class"
.LC101:
	.string	"struct"
.LC102:
	.string	"' more than once"
	.section	.rodata._ZNK2v88internal6torque13AggregateType23CheckForDuplicateFieldsEv.str1.8,"aMS",@progbits,1
	.align 8
.LC103:
	.string	"' declares a field with the name '"
	.section	.rodata._ZNK2v88internal6torque13AggregateType23CheckForDuplicateFieldsEv.str1.1
.LC104:
	.string	" '"
	.section	.rodata._ZNK2v88internal6torque13AggregateType23CheckForDuplicateFieldsEv.str1.8
	.align 8
.LC105:
	.string	"' that masks an inherited field from class '"
	.section	.text.unlikely._ZNK2v88internal6torque13AggregateType23CheckForDuplicateFieldsEv,"ax",@progbits
	.align 2
.LCOLDB106:
	.section	.text._ZNK2v88internal6torque13AggregateType23CheckForDuplicateFieldsEv,"ax",@progbits
.LHOTB106:
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal6torque13AggregateType23CheckForDuplicateFieldsEv
	.type	_ZNK2v88internal6torque13AggregateType23CheckForDuplicateFieldsEv, @function
_ZNK2v88internal6torque13AggregateType23CheckForDuplicateFieldsEv:
.LFB6526:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6526
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$264, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -304(%rbp)
	leaq	-208(%rbp), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
.LEHB353:
	call	_ZNK2v88internal6torque13AggregateType12GetHierarchyEv
.LEHE353:
	leaq	-136(%rbp), %rax
	movq	-200(%rbp), %rdi
	movl	$0, -136(%rbp)
	movq	%rax, -232(%rbp)
	movq	%rax, -120(%rbp)
	movq	%rax, -112(%rbp)
	movq	-208(%rbp), %rax
	movq	$0, -128(%rbp)
	movq	$0, -104(%rbp)
	movq	%rdi, -296(%rbp)
	cmpq	%rdi, %rax
	je	.L3029
	movq	%rax, -288(%rbp)
.L3034:
	movq	-288(%rbp), %rax
	movq	(%rax), %rax
	cmpb	$0, 72(%rax)
	movq	%rax, -264(%rbp)
	jne	.L3033
	movq	%rax, %rdi
	movq	(%rax), %rax
	leaq	-144(%rbp), %r15
.LEHB354:
	call	*96(%rax)
.L3033:
	movq	-264(%rbp), %rax
	movq	88(%rax), %rsi
	movq	80(%rax), %rbx
	leaq	-209(%rbp), %rax
	movq	%rax, -280(%rbp)
	movq	%rsi, -272(%rbp)
	cmpq	%rsi, %rbx
	je	.L3032
	.p2align 4,,10
	.p2align 3
.L3059:
	movq	-128(%rbp), %r15
	leaq	48(%rbx), %rax
	movq	%rax, -256(%rbp)
	testq	%r15, %r15
	je	.L3076
	movq	48(%rbx), %r14
	movq	56(%rbx), %r13
	movq	%r15, -240(%rbp)
	movq	-232(%rbp), %r12
	movq	%rbx, -248(%rbp)
	movq	%r12, %rbx
	movq	%r15, %r12
	movq	%r14, %r15
	movq	%r13, %r14
	jmp	.L3037
	.p2align 4,,10
	.p2align 3
.L3042:
	movq	24(%r12), %r12
	testq	%r12, %r12
	je	.L3038
.L3037:
	movq	40(%r12), %r13
	movq	%r14, %rdx
	cmpq	%r14, %r13
	cmovbe	%r13, %rdx
	testq	%rdx, %rdx
	je	.L3039
	movq	32(%r12), %rdi
	movq	%r15, %rsi
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L3040
.L3039:
	movq	%r13, %rax
	movl	$2147483648, %ecx
	subq	%r14, %rax
	cmpq	%rcx, %rax
	jge	.L3041
	movabsq	$-2147483649, %rcx
	cmpq	%rcx, %rax
	jle	.L3042
.L3040:
	testl	%eax, %eax
	js	.L3042
.L3041:
	movq	%r12, %rbx
	movq	16(%r12), %r12
	testq	%r12, %r12
	jne	.L3037
.L3038:
	movq	%rbx, %r12
	movq	%r14, %r13
	movq	-248(%rbp), %rbx
	movq	%r15, %r14
	movq	-240(%rbp), %r15
	cmpq	-232(%rbp), %r12
	je	.L3044
	movq	40(%r12), %r8
	cmpq	%r8, %r13
	movq	%r8, %rdx
	cmovbe	%r13, %rdx
	testq	%rdx, %rdx
	je	.L3045
	movq	32(%r12), %rsi
	movq	%r14, %rdi
	movq	%r8, -240(%rbp)
	call	memcmp@PLT
	movq	-240(%rbp), %r8
	testl	%eax, %eax
	jne	.L3046
.L3045:
	movq	%r13, %rax
	movl	$2147483648, %esi
	subq	%r8, %rax
	cmpq	%rsi, %rax
	jge	.L3047
	movabsq	$-2147483649, %rdi
	cmpq	%rdi, %rax
	jle	.L3044
.L3046:
	testl	%eax, %eax
	js	.L3044
.L3047:
	movdqu	(%rbx), %xmm0
	leaq	-144(%rbp), %r15
	movaps	%xmm0, -176(%rbp)
	movl	16(%rbx), %eax
	movl	%eax, -160(%rbp)
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rax
	movq	%rax, -152(%rbp)
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
.LEHE354:
	leaq	-176(%rbp), %rdx
	leaq	.LC100(%rip), %r13
	movq	%rdx, (%rax)
	movq	-264(%rbp), %rax
	leaq	-80(%rbp), %rbx
	movq	%rbx, -96(%rbp)
	cmpl	$5, 8(%rax)
	leaq	.LC101(%rip), %rax
	cmovne	%rax, %r13
	movq	%r13, %rdi
	call	strlen@PLT
	xorl	%edx, %edx
	movl	%eax, %edi
	testl	%eax, %eax
	je	.L3072
.L3071:
	movl	%edx, %ecx
	addl	$1, %edx
	movzbl	0(%r13,%rcx), %esi
	movb	%sil, (%rbx,%rcx)
	cmpl	%edi, %edx
	jb	.L3071
.L3072:
	movq	%rax, -88(%rbp)
	movq	-304(%rbp), %rsi
	movb	$0, -80(%rbp,%rax)
	movq	64(%r12), %rax
	leaq	112(%rsi), %rdx
	cmpq	%rsi, %rax
	je	.L3116
	leaq	.LC90(%rip), %rsi
	addq	$112, %rax
	leaq	-96(%rbp), %rdi
	movq	-256(%rbp), %r8
	pushq	%rsi
	leaq	.LC105(%rip), %r9
	leaq	.LC103(%rip), %rcx
	pushq	%rax
	leaq	.LC104(%rip), %rsi
.LEHB355:
	.cfi_escape 0x2e,0x10
	call	_ZN2v88internal6torque11ReportErrorIJRNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA3_KcRKS8_RA35_SA_SE_RA45_SA_SE_RA2_SA_EEEvDpOT_
.LEHE355:
	.p2align 4,,10
	.p2align 3
.L3044:
	movq	-232(%rbp), %r12
	movq	%rbx, -240(%rbp)
	movq	%r14, %rbx
	movq	%r12, %r14
	jmp	.L3069
	.p2align 4,,10
	.p2align 3
.L3054:
	movq	24(%r15), %r15
	testq	%r15, %r15
	je	.L3050
.L3069:
	movq	40(%r15), %r12
	movq	%r13, %rdx
	cmpq	%r13, %r12
	cmovbe	%r12, %rdx
	testq	%rdx, %rdx
	je	.L3051
	movq	32(%r15), %rdi
	movq	%rbx, %rsi
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L3052
.L3051:
	movq	%r12, %rax
	movl	$2147483648, %esi
	subq	%r13, %rax
	cmpq	%rsi, %rax
	jge	.L3053
	movabsq	$-2147483649, %rcx
	cmpq	%rcx, %rax
	jle	.L3054
.L3052:
	testl	%eax, %eax
	js	.L3054
.L3053:
	movq	%r15, %r14
	movq	16(%r15), %r15
	testq	%r15, %r15
	jne	.L3069
.L3050:
	movq	%r14, %r12
	movq	%rbx, %r14
	movq	-240(%rbp), %rbx
	cmpq	-232(%rbp), %r12
	je	.L3036
	movq	40(%r12), %r15
	cmpq	%r15, %r13
	movq	%r15, %rdx
	cmovbe	%r13, %rdx
	testq	%rdx, %rdx
	je	.L3056
	movq	32(%r12), %rsi
	movq	%r14, %rdi
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L3057
.L3056:
	movq	%r13, %rcx
	movl	$2147483648, %eax
	subq	%r15, %rcx
	cmpq	%rax, %rcx
	jge	.L3058
	movabsq	$-2147483649, %rax
	cmpq	%rax, %rcx
	jle	.L3036
	movl	%ecx, %eax
.L3057:
	testl	%eax, %eax
	jns	.L3058
.L3036:
	movq	-256(%rbp), %rax
	leaq	-144(%rbp), %r15
	movq	-280(%rbp), %r8
	leaq	-176(%rbp), %rcx
	leaq	_ZStL19piecewise_construct(%rip), %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	%rax, -176(%rbp)
.LEHB356:
	.cfi_escape 0x2e,0
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_PKN2v88internal6torque13AggregateTypeEESt10_Select1stISE_ESt4lessIS5_ESaISE_EE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS7_EESP_IJEEEEESt17_Rb_tree_iteratorISE_ESt23_Rb_tree_const_iteratorISE_EDpOT_
.LEHE356:
	movq	%rax, %r12
.L3058:
	movq	-264(%rbp), %rax
	addq	$104, %rbx
	movq	%rax, 64(%r12)
	cmpq	%rbx, -272(%rbp)
	jne	.L3059
.L3032:
	addq	$8, -288(%rbp)
	movq	-288(%rbp), %rax
	cmpq	%rax, -296(%rbp)
	jne	.L3034
	movq	-128(%rbp), %r12
	testq	%r12, %r12
	je	.L3062
	leaq	-144(%rbp), %r15
.L3060:
	movq	24(%r12), %rsi
	movq	%r15, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_PKN2v88internal6torque13AggregateTypeEESt10_Select1stISE_ESt4lessIS5_ESaISE_EE8_M_eraseEPSt13_Rb_tree_nodeISE_E
	movq	32(%r12), %rdi
	leaq	48(%r12), %rax
	movq	16(%r12), %rbx
	cmpq	%rax, %rdi
	je	.L3061
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L3062
.L3063:
	movq	%rbx, %r12
	jmp	.L3060
	.p2align 4,,10
	.p2align 3
.L3076:
	movq	-232(%rbp), %r12
	jmp	.L3036
.L3116:
	movq	-256(%rbp), %r8
	leaq	-96(%rbp), %rdi
	leaq	.LC102(%rip), %r9
	leaq	.LC103(%rip), %rcx
	leaq	.LC104(%rip), %rsi
.LEHB357:
	call	_ZN2v88internal6torque11ReportErrorIJRNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA3_KcRKS8_RA35_SA_SE_RA17_SA_EEEvDpOT_
.LEHE357:
	.p2align 4,,10
	.p2align 3
.L3061:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L3063
.L3062:
	movq	-208(%rbp), %rax
	movq	%rax, -296(%rbp)
.L3029:
	movq	-296(%rbp), %rax
	testq	%rax, %rax
	je	.L3028
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L3028:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3117
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3117:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
.L3080:
	endbr64
	movq	%rax, %r12
	jmp	.L3065
.L3079:
	endbr64
	movq	%rax, %r12
	jmp	.L3067
	.section	.gcc_except_table._ZNK2v88internal6torque13AggregateType23CheckForDuplicateFieldsEv,"a",@progbits
.LLSDA6526:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6526-.LLSDACSB6526
.LLSDACSB6526:
	.uleb128 .LEHB353-.LFB6526
	.uleb128 .LEHE353-.LEHB353
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB354-.LFB6526
	.uleb128 .LEHE354-.LEHB354
	.uleb128 .L3079-.LFB6526
	.uleb128 0
	.uleb128 .LEHB355-.LFB6526
	.uleb128 .LEHE355-.LEHB355
	.uleb128 .L3080-.LFB6526
	.uleb128 0
	.uleb128 .LEHB356-.LFB6526
	.uleb128 .LEHE356-.LEHB356
	.uleb128 .L3079-.LFB6526
	.uleb128 0
	.uleb128 .LEHB357-.LFB6526
	.uleb128 .LEHE357-.LEHB357
	.uleb128 .L3080-.LFB6526
	.uleb128 0
.LLSDACSE6526:
	.section	.text._ZNK2v88internal6torque13AggregateType23CheckForDuplicateFieldsEv
	.cfi_endproc
	.section	.text.unlikely._ZNK2v88internal6torque13AggregateType23CheckForDuplicateFieldsEv
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6526
	.type	_ZNK2v88internal6torque13AggregateType23CheckForDuplicateFieldsEv.cold, @function
_ZNK2v88internal6torque13AggregateType23CheckForDuplicateFieldsEv.cold:
.LFSB6526:
.L3065:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L3066
	call	_ZdlPv@PLT
.L3066:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
	movq	-152(%rbp), %rdx
	leaq	-144(%rbp), %r15
	movq	%rdx, (%rax)
.L3067:
	movq	-128(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_PKN2v88internal6torque13AggregateTypeEESt10_Select1stISE_ESt4lessIS5_ESaISE_EE8_M_eraseEPSt13_Rb_tree_nodeISE_E
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3068
	call	_ZdlPv@PLT
.L3068:
	movq	%r12, %rdi
.LEHB358:
	call	_Unwind_Resume@PLT
.LEHE358:
	.cfi_endproc
.LFE6526:
	.section	.gcc_except_table._ZNK2v88internal6torque13AggregateType23CheckForDuplicateFieldsEv
.LLSDAC6526:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6526-.LLSDACSBC6526
.LLSDACSBC6526:
	.uleb128 .LEHB358-.LCOLDB106
	.uleb128 .LEHE358-.LEHB358
	.uleb128 0
	.uleb128 0
.LLSDACSEC6526:
	.section	.text.unlikely._ZNK2v88internal6torque13AggregateType23CheckForDuplicateFieldsEv
	.section	.text._ZNK2v88internal6torque13AggregateType23CheckForDuplicateFieldsEv
	.size	_ZNK2v88internal6torque13AggregateType23CheckForDuplicateFieldsEv, .-_ZNK2v88internal6torque13AggregateType23CheckForDuplicateFieldsEv
	.section	.text.unlikely._ZNK2v88internal6torque13AggregateType23CheckForDuplicateFieldsEv
	.size	_ZNK2v88internal6torque13AggregateType23CheckForDuplicateFieldsEv.cold, .-_ZNK2v88internal6torque13AggregateType23CheckForDuplicateFieldsEv.cold
.LCOLDE106:
	.section	.text._ZNK2v88internal6torque13AggregateType23CheckForDuplicateFieldsEv
.LHOTE106:
	.section	.text.unlikely._ZNK2v88internal6torque10StructType8FinalizeEv,"ax",@progbits
	.align 2
.LCOLDB107:
	.section	.text._ZNK2v88internal6torque10StructType8FinalizeEv,"ax",@progbits
.LHOTB107:
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal6torque10StructType8FinalizeEv
	.type	_ZNK2v88internal6torque10StructType8FinalizeEv, @function
_ZNK2v88internal6torque10StructType8FinalizeEv:
.LFB6579:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6579
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$72, %rsp
	.cfi_offset 12, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 72(%rdi)
	je	.L3126
.L3118:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3127
	addq	$72, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3126:
	.cfi_restore_state
	movq	104(%rdi), %rax
	movq	%rdi, %r12
	movq	%rax, -80(%rbp)
.LEHB359:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rax
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
.LEHE359:
	leaq	-80(%rbp), %rdx
	movq	%rdx, (%rax)
	movq	168(%r12), %rax
	movdqu	12(%rax), %xmm0
	movl	28(%rax), %eax
	movaps	%xmm0, -64(%rbp)
	movl	%eax, -48(%rbp)
.LEHB360:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rax
	movq	%rax, -40(%rbp)
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
.LEHE360:
	leaq	-64(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rdx, (%rax)
	movq	168(%r12), %rsi
.LEHB361:
	call	_ZN2v88internal6torque11TypeVisitor18VisitStructMethodsEPNS1_10StructTypeEPKNS1_17StructDeclarationE@PLT
.LEHE361:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
	movq	-40(%rbp), %rdx
	movq	%rdx, (%rax)
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
	movq	-72(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rdx, (%rax)
	movb	$1, 72(%r12)
.LEHB362:
	call	_ZNK2v88internal6torque13AggregateType23CheckForDuplicateFieldsEv
.LEHE362:
	jmp	.L3118
.L3127:
	call	__stack_chk_fail@PLT
.L3123:
	endbr64
	movq	%rax, %r12
	jmp	.L3121
.L3124:
	endbr64
	movq	%rax, %r12
	jmp	.L3120
	.section	.gcc_except_table._ZNK2v88internal6torque10StructType8FinalizeEv,"a",@progbits
.LLSDA6579:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6579-.LLSDACSB6579
.LLSDACSB6579:
	.uleb128 .LEHB359-.LFB6579
	.uleb128 .LEHE359-.LEHB359
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB360-.LFB6579
	.uleb128 .LEHE360-.LEHB360
	.uleb128 .L3123-.LFB6579
	.uleb128 0
	.uleb128 .LEHB361-.LFB6579
	.uleb128 .LEHE361-.LEHB361
	.uleb128 .L3124-.LFB6579
	.uleb128 0
	.uleb128 .LEHB362-.LFB6579
	.uleb128 .LEHE362-.LEHB362
	.uleb128 0
	.uleb128 0
.LLSDACSE6579:
	.section	.text._ZNK2v88internal6torque10StructType8FinalizeEv
	.cfi_endproc
	.section	.text.unlikely._ZNK2v88internal6torque10StructType8FinalizeEv
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6579
	.type	_ZNK2v88internal6torque10StructType8FinalizeEv.cold, @function
_ZNK2v88internal6torque10StructType8FinalizeEv.cold:
.LFSB6579:
.L3120:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
	movq	-40(%rbp), %rdx
	movq	%rdx, (%rax)
.L3121:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
	movq	-72(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rdx, (%rax)
.LEHB363:
	call	_Unwind_Resume@PLT
.LEHE363:
	.cfi_endproc
.LFE6579:
	.section	.gcc_except_table._ZNK2v88internal6torque10StructType8FinalizeEv
.LLSDAC6579:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6579-.LLSDACSBC6579
.LLSDACSBC6579:
	.uleb128 .LEHB363-.LCOLDB107
	.uleb128 .LEHE363-.LEHB363
	.uleb128 0
	.uleb128 0
.LLSDACSEC6579:
	.section	.text.unlikely._ZNK2v88internal6torque10StructType8FinalizeEv
	.section	.text._ZNK2v88internal6torque10StructType8FinalizeEv
	.size	_ZNK2v88internal6torque10StructType8FinalizeEv, .-_ZNK2v88internal6torque10StructType8FinalizeEv
	.section	.text.unlikely._ZNK2v88internal6torque10StructType8FinalizeEv
	.size	_ZNK2v88internal6torque10StructType8FinalizeEv.cold, .-_ZNK2v88internal6torque10StructType8FinalizeEv.cold
.LCOLDE107:
	.section	.text._ZNK2v88internal6torque10StructType8FinalizeEv
.LHOTE107:
	.section	.rodata._ZNK2v88internal6torque9ClassType8FinalizeEv.str1.8,"aMS",@progbits,1
	.align 8
.LC108:
	.string	"Super class must either be abstract (annotate super class with @abstract) or this class must have the same instance type as the super class (annotate this class with @hasSameInstanceTypeAsParent)."
	.align 8
.LC109:
	.string	"Generation of C++ class for Torque class "
	.align 8
.LC110:
	.string	" is not supported yet, because field "
	.section	.rodata._ZNK2v88internal6torque9ClassType8FinalizeEv.str1.1,"aMS",@progbits,1
.LC111:
	.string	" is a weak field."
	.section	.text.unlikely._ZNK2v88internal6torque9ClassType8FinalizeEv,"ax",@progbits
	.align 2
.LCOLDB112:
	.section	.text._ZNK2v88internal6torque9ClassType8FinalizeEv,"ax",@progbits
.LHOTB112:
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal6torque9ClassType8FinalizeEv
	.type	_ZNK2v88internal6torque9ClassType8FinalizeEv, @function
_ZNK2v88internal6torque9ClassType8FinalizeEv:
.LFB6593:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6593
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$648, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 72(%rdi)
	je	.L3199
.L3128:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3200
	addq	$648, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3199:
	.cfi_restore_state
	movq	224(%rdi), %rax
	movq	%rdi, %r12
	movq	16(%rax), %rax
	movq	%rax, -624(%rbp)
.LEHB364:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rax
	movq	%rax, -616(%rbp)
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
.LEHE364:
	leaq	-624(%rbp), %rdx
	movq	%rdx, (%rax)
	movq	216(%r12), %rax
	movdqu	12(%rax), %xmm4
	movl	28(%rax), %eax
	movaps	%xmm4, -608(%rbp)
	movl	%eax, -592(%rbp)
.LEHB365:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rax
	movq	%rax, -584(%rbp)
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
.LEHE365:
	leaq	-608(%rbp), %rdx
	movq	%rdx, (%rax)
	movq	16(%r12), %rbx
	testq	%rbx, %rbx
	je	.L3132
	cmpl	$5, 8(%rbx)
	jne	.L3132
	cmpb	$0, 72(%rbx)
	je	.L3134
.L3137:
	movl	176(%rbx), %eax
	testb	$1, %ah
	je	.L3136
	orl	$256, 176(%r12)
	movl	176(%rbx), %eax
.L3136:
	testb	$16, %al
	je	.L3201
.L3132:
	movq	216(%r12), %rsi
	movq	%r12, %rdi
.LEHB366:
	call	_ZN2v88internal6torque11TypeVisitor26VisitClassFieldsAndMethodsEPNS1_9ClassTypeEPKNS1_16ClassDeclarationE@PLT
.LEHE366:
	movl	176(%r12), %eax
	movb	$1, 72(%r12)
	testb	$-128, %al
	jne	.L3147
	testb	$1, %al
	jne	.L3149
.L3147:
	movq	88(%r12), %rax
	movq	80(%r12), %rbx
	movq	%rax, -632(%rbp)
	cmpq	%rax, %rbx
	je	.L3149
	movq	.LC22(%rip), %xmm3
	movhps	.LC23(%rip), %xmm3
	movaps	%xmm3, -688(%rbp)
	jmp	.L3174
	.p2align 4,,10
	.p2align 3
.L3150:
	addq	$104, %rbx
	cmpq	%rbx, -632(%rbp)
	je	.L3149
.L3174:
	cmpb	$0, 96(%rbx)
	je	.L3150
	movq	80(%rbx), %rax
	leaq	-320(%rbp), %r15
	leaq	-448(%rbp), %r14
	movq	%r15, %rdi
	movq	%r14, -672(%rbp)
	movq	%rax, -640(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	movq	%rax, -320(%rbp)
	xorl	%eax, %eax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movw	%ax, -96(%rbp)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	$0, -104(%rbp)
	movq	%rax, -448(%rbp)
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	movq	-448(%rbp), %rax
	movq	$0, -440(%rbp)
	movq	-24(%rax), %rdi
	addq	%r14, %rdi
.LEHB367:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE367:
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	leaq	-432(%rbp), %r13
	xorl	%esi, %esi
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -432(%rbp,%rax)
	movq	-432(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r13, %rdi
.LEHB368:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE368:
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	pxor	%xmm0, %xmm0
	leaq	-424(%rbp), %r14
	movdqa	-688(%rbp), %xmm2
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movq	%rax, -320(%rbp)
	leaq	-368(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -648(%rbp)
	movaps	%xmm2, -432(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movl	$24, -360(%rbp)
	movq	%rax, -656(%rbp)
	movq	%rax, -352(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
.LEHB369:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE369:
	movl	$41, %edx
	leaq	.LC109(%rip), %rsi
	movq	%r13, %rdi
.LEHB370:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	120(%r12), %rdx
	movq	112(%r12), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$37, %edx
	leaq	.LC110(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	56(%rbx), %rdx
	movq	48(%rbx), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$2, %edx
	leaq	.LC17(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	-512(%rbp), %r14
	movq	-640(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZNK2v88internal6torque4Type8ToStringB5cxx11Ev
.LEHE370:
	movq	-504(%rbp), %rdx
	movq	-512(%rbp), %rsi
	movq	%r13, %rdi
.LEHB371:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE371:
	movq	-512(%rbp), %rdi
	leaq	-496(%rbp), %rax
	movq	%rax, -664(%rbp)
	cmpq	%rax, %rdi
	je	.L3155
	call	_ZdlPv@PLT
.L3155:
	movl	$17, %edx
	leaq	.LC111(%rip), %rsi
	movq	%r13, %rdi
.LEHB372:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE372:
	leaq	-560(%rbp), %rax
	movb	$0, -560(%rbp)
	leaq	-576(%rbp), %r13
	movq	%rax, -640(%rbp)
	movq	%rax, -576(%rbp)
	movq	-384(%rbp), %rax
	movq	$0, -568(%rbp)
	testq	%rax, %rax
	je	.L3160
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	jbe	.L3161
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r13, %rdi
	movq	%rax, %r8
.LEHB373:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.LEHE373:
.L3162:
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
.LEHB374:
	call	_ZN2v88internal6torque14MessageBuilderC1ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_13TorqueMessage4KindE@PLT
.LEHE374:
	movq	-576(%rbp), %rdi
	cmpq	-640(%rbp), %rdi
	je	.L3166
	call	_ZdlPv@PLT
.L3166:
	movq	.LC22(%rip), %xmm0
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-352(%rbp), %rdi
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movhps	.LC24(%rip), %xmm0
	movq	%rax, -320(%rbp)
	movaps	%xmm0, -432(%rbp)
	cmpq	-656(%rbp), %rdi
	je	.L3167
	call	_ZdlPv@PLT
.L3167:
	movq	-648(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%r15, %rdi
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -432(%rbp,%rax)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	movq	$0, -440(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	cmpb	$0, -480(%rbp)
	movl	(%rbx), %eax
	movl	4(%rbx), %esi
	movl	8(%rbx), %edx
	movl	12(%rbx), %edi
	movl	16(%rbx), %ecx
	je	.L3202
	movd	%edx, %xmm1
	movd	%edi, %xmm5
	movd	%eax, %xmm0
	movl	%ecx, -460(%rbp)
	movd	%esi, %xmm6
	punpckldq	%xmm5, %xmm1
	punpckldq	%xmm6, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -476(%rbp)
.L3172:
	movq	%r14, %rdi
	call	_ZNK2v88internal6torque14MessageBuilder6ReportEv@PLT
	movq	-512(%rbp), %rdi
	cmpq	-664(%rbp), %rdi
	je	.L3150
	call	_ZdlPv@PLT
	addq	$104, %rbx
	cmpq	%rbx, -632(%rbp)
	jne	.L3174
	.p2align 4,,10
	.p2align 3
.L3149:
	movq	%r12, %rdi
.LEHB375:
	call	_ZNK2v88internal6torque13AggregateType23CheckForDuplicateFieldsEv
.LEHE375:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
	movq	-584(%rbp), %rdx
	movq	%rdx, (%rax)
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
	movq	-616(%rbp), %rdx
	movq	%rdx, (%rax)
	jmp	.L3128
	.p2align 4,,10
	.p2align 3
.L3202:
	movd	%edx, %xmm1
	movd	%edi, %xmm7
	movd	%eax, %xmm0
	movl	%ecx, -460(%rbp)
	movd	%esi, %xmm4
	punpckldq	%xmm7, %xmm1
	movb	$1, -480(%rbp)
	punpckldq	%xmm4, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -476(%rbp)
	jmp	.L3172
	.p2align 4,,10
	.p2align 3
.L3161:
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r13, %rdi
.LEHB376:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.LEHE376:
	jmp	.L3162
	.p2align 4,,10
	.p2align 3
.L3201:
	testb	$64, 176(%r12)
	jne	.L3132
	leaq	-448(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -672(%rbp)
.LEHB377:
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEC1Ev@PLT
.LEHE377:
	leaq	-432(%rbp), %rdi
	leaq	.LC108(%rip), %rsi
.LEHB378:
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	leaq	-512(%rbp), %r14
	leaq	-424(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZNKSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEE3strEv@PLT
.LEHE378:
	leaq	-576(%rbp), %r13
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
.LEHB379:
	call	_ZN2v88internal6torque14MessageBuilderC1ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_13TorqueMessage4KindE@PLT
.LEHE379:
	movq	-512(%rbp), %rdi
	leaq	-496(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3138
	call	_ZdlPv@PLT
.L3138:
	movq	-672(%rbp), %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	216(%r12), %rax
	cmpb	$0, -544(%rbp)
	movq	32(%rax), %rdx
	movl	12(%rdx), %eax
	movl	16(%rdx), %esi
	movl	20(%rdx), %ecx
	movl	24(%rdx), %edi
	movl	28(%rdx), %edx
	je	.L3203
	movd	%ecx, %xmm1
	movd	%edi, %xmm5
	movd	%eax, %xmm0
	movl	%edx, -524(%rbp)
	movd	%esi, %xmm6
	punpckldq	%xmm5, %xmm1
	punpckldq	%xmm6, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -540(%rbp)
.L3145:
	movq	%r13, %rdi
	call	_ZNK2v88internal6torque14MessageBuilder6ReportEv@PLT
	movq	-576(%rbp), %rdi
	leaq	-560(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3132
	call	_ZdlPv@PLT
	jmp	.L3132
	.p2align 4,,10
	.p2align 3
.L3134:
	movq	%rbx, %rdi
.LEHB380:
	call	_ZNK2v88internal6torque9ClassType8FinalizeEv
.LEHE380:
	jmp	.L3137
	.p2align 4,,10
	.p2align 3
.L3160:
	leaq	-352(%rbp), %rsi
	movq	%r13, %rdi
.LEHB381:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
.LEHE381:
	jmp	.L3162
.L3203:
	movd	%ecx, %xmm1
	movd	%edi, %xmm7
	movd	%eax, %xmm0
	movl	%edx, -524(%rbp)
	movd	%esi, %xmm5
	punpckldq	%xmm7, %xmm1
	movb	$1, -544(%rbp)
	punpckldq	%xmm5, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -540(%rbp)
	jmp	.L3145
.L3200:
	call	__stack_chk_fail@PLT
.L3182:
	endbr64
	movq	%rax, %rbx
	jmp	.L3170
.L3187:
	endbr64
	movq	%rax, %rbx
	jmp	.L3164
.L3177:
	endbr64
	movq	%rax, %rbx
	jmp	.L3175
.L3180:
	endbr64
	movq	%rax, %rbx
	jmp	.L3141
.L3179:
	endbr64
	movq	%rax, %rbx
	jmp	.L3143
.L3178:
	endbr64
	movq	%rax, %rbx
	jmp	.L3144
.L3183:
	endbr64
	movq	%rax, %rbx
	jmp	.L3153
.L3185:
	endbr64
	movq	%rax, %rbx
	jmp	.L3152
.L3181:
	endbr64
	movq	%rax, %rbx
	jmp	.L3159
.L3184:
	endbr64
	movq	%rax, %rbx
	jmp	.L3154
.L3186:
	endbr64
	movq	%rax, %rbx
	jmp	.L3157
	.section	.gcc_except_table._ZNK2v88internal6torque9ClassType8FinalizeEv,"a",@progbits
.LLSDA6593:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6593-.LLSDACSB6593
.LLSDACSB6593:
	.uleb128 .LEHB364-.LFB6593
	.uleb128 .LEHE364-.LEHB364
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB365-.LFB6593
	.uleb128 .LEHE365-.LEHB365
	.uleb128 .L3177-.LFB6593
	.uleb128 0
	.uleb128 .LEHB366-.LFB6593
	.uleb128 .LEHE366-.LEHB366
	.uleb128 .L3178-.LFB6593
	.uleb128 0
	.uleb128 .LEHB367-.LFB6593
	.uleb128 .LEHE367-.LEHB367
	.uleb128 .L3183-.LFB6593
	.uleb128 0
	.uleb128 .LEHB368-.LFB6593
	.uleb128 .LEHE368-.LEHB368
	.uleb128 .L3185-.LFB6593
	.uleb128 0
	.uleb128 .LEHB369-.LFB6593
	.uleb128 .LEHE369-.LEHB369
	.uleb128 .L3184-.LFB6593
	.uleb128 0
	.uleb128 .LEHB370-.LFB6593
	.uleb128 .LEHE370-.LEHB370
	.uleb128 .L3181-.LFB6593
	.uleb128 0
	.uleb128 .LEHB371-.LFB6593
	.uleb128 .LEHE371-.LEHB371
	.uleb128 .L3186-.LFB6593
	.uleb128 0
	.uleb128 .LEHB372-.LFB6593
	.uleb128 .LEHE372-.LEHB372
	.uleb128 .L3181-.LFB6593
	.uleb128 0
	.uleb128 .LEHB373-.LFB6593
	.uleb128 .LEHE373-.LEHB373
	.uleb128 .L3187-.LFB6593
	.uleb128 0
	.uleb128 .LEHB374-.LFB6593
	.uleb128 .LEHE374-.LEHB374
	.uleb128 .L3182-.LFB6593
	.uleb128 0
	.uleb128 .LEHB375-.LFB6593
	.uleb128 .LEHE375-.LEHB375
	.uleb128 .L3178-.LFB6593
	.uleb128 0
	.uleb128 .LEHB376-.LFB6593
	.uleb128 .LEHE376-.LEHB376
	.uleb128 .L3187-.LFB6593
	.uleb128 0
	.uleb128 .LEHB377-.LFB6593
	.uleb128 .LEHE377-.LEHB377
	.uleb128 .L3178-.LFB6593
	.uleb128 0
	.uleb128 .LEHB378-.LFB6593
	.uleb128 .LEHE378-.LEHB378
	.uleb128 .L3179-.LFB6593
	.uleb128 0
	.uleb128 .LEHB379-.LFB6593
	.uleb128 .LEHE379-.LEHB379
	.uleb128 .L3180-.LFB6593
	.uleb128 0
	.uleb128 .LEHB380-.LFB6593
	.uleb128 .LEHE380-.LEHB380
	.uleb128 .L3178-.LFB6593
	.uleb128 0
	.uleb128 .LEHB381-.LFB6593
	.uleb128 .LEHE381-.LEHB381
	.uleb128 .L3187-.LFB6593
	.uleb128 0
.LLSDACSE6593:
	.section	.text._ZNK2v88internal6torque9ClassType8FinalizeEv
	.cfi_endproc
	.section	.text.unlikely._ZNK2v88internal6torque9ClassType8FinalizeEv
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6593
	.type	_ZNK2v88internal6torque9ClassType8FinalizeEv.cold, @function
_ZNK2v88internal6torque9ClassType8FinalizeEv.cold:
.LFSB6593:
.L3170:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-576(%rbp), %rdi
	cmpq	-640(%rbp), %rdi
	je	.L3159
	call	_ZdlPv@PLT
	jmp	.L3159
.L3164:
	movq	-576(%rbp), %rdi
	cmpq	-640(%rbp), %rdi
	je	.L3159
	call	_ZdlPv@PLT
.L3159:
	movq	-672(%rbp), %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
.L3144:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
	movq	-584(%rbp), %rdx
	movq	%rdx, (%rax)
.L3175:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
	movq	-616(%rbp), %rdx
	movq	%rbx, %rdi
	movq	%rdx, (%rax)
.LEHB382:
	call	_Unwind_Resume@PLT
.LEHE382:
.L3141:
	movq	-512(%rbp), %rdi
	leaq	-496(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3143
	call	_ZdlPv@PLT
.L3143:
	movq	-672(%rbp), %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	jmp	.L3144
.L3152:
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	movq	$0, -440(%rbp)
.L3153:
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%r15, %rdi
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	jmp	.L3144
.L3154:
	movq	%r14, %rdi
	call	_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED1Ev
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -432(%rbp,%rax)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	movq	$0, -440(%rbp)
	jmp	.L3153
.L3157:
	movq	-512(%rbp), %rdi
	leaq	-496(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3159
	call	_ZdlPv@PLT
	jmp	.L3159
	.cfi_endproc
.LFE6593:
	.section	.gcc_except_table._ZNK2v88internal6torque9ClassType8FinalizeEv
.LLSDAC6593:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6593-.LLSDACSBC6593
.LLSDACSBC6593:
	.uleb128 .LEHB382-.LCOLDB112
	.uleb128 .LEHE382-.LEHB382
	.uleb128 0
	.uleb128 0
.LLSDACSEC6593:
	.section	.text.unlikely._ZNK2v88internal6torque9ClassType8FinalizeEv
	.section	.text._ZNK2v88internal6torque9ClassType8FinalizeEv
	.size	_ZNK2v88internal6torque9ClassType8FinalizeEv, .-_ZNK2v88internal6torque9ClassType8FinalizeEv
	.section	.text.unlikely._ZNK2v88internal6torque9ClassType8FinalizeEv
	.size	_ZNK2v88internal6torque9ClassType8FinalizeEv.cold, .-_ZNK2v88internal6torque9ClassType8FinalizeEv.cold
.LCOLDE112:
	.section	.text._ZNK2v88internal6torque9ClassType8FinalizeEv
.LHOTE112:
	.section	.text._ZNK2v88internal6torque9ClassType15HasIndexedFieldEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal6torque9ClassType15HasIndexedFieldEv
	.type	_ZNK2v88internal6torque9ClassType15HasIndexedFieldEv, @function
_ZNK2v88internal6torque9ClassType15HasIndexedFieldEv:
.LFB6588:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpb	$0, 72(%rdi)
	je	.L3207
.L3205:
	movl	176(%rbx), %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	shrl	$8, %eax
	andl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L3207:
	.cfi_restore_state
	call	_ZNK2v88internal6torque9ClassType8FinalizeEv
	jmp	.L3205
	.cfi_endproc
.LFE6588:
	.size	_ZNK2v88internal6torque9ClassType15HasIndexedFieldEv, .-_ZNK2v88internal6torque9ClassType15HasIndexedFieldEv
	.section	.text.unlikely._ZNK2v88internal6torque9ClassType16ComputeAllFieldsEv,"ax",@progbits
	.align 2
.LCOLDB113:
	.section	.text._ZNK2v88internal6torque9ClassType16ComputeAllFieldsEv,"ax",@progbits
.LHOTB113:
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal6torque9ClassType16ComputeAllFieldsEv
	.type	_ZNK2v88internal6torque9ClassType16ComputeAllFieldsEv, @function
_ZNK2v88internal6torque9ClassType16ComputeAllFieldsEv:
.LFB6594:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6594
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, 16(%rdi)
	movups	%xmm0, (%rdi)
	movq	16(%rsi), %rsi
	testq	%rsi, %rsi
	je	.L3209
	cmpl	$5, 8(%rsi)
	jne	.L3209
	leaq	-80(%rbp), %rdi
.LEHB383:
	call	_ZNK2v88internal6torque9ClassType16ComputeAllFieldsEv
	movq	0(%r13), %r15
	movdqa	-80(%rbp), %xmm1
	pxor	%xmm0, %xmm0
	movq	-64(%rbp), %rax
	movq	8(%r13), %r14
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	movq	%r15, %r12
	movq	%rax, 16(%r13)
	movups	%xmm1, 0(%r13)
	cmpq	%r14, %r15
	je	.L3214
	.p2align 4,,10
	.p2align 3
.L3210:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3213
	call	_ZdlPv@PLT
	addq	$104, %r12
	cmpq	%r12, %r14
	jne	.L3210
.L3214:
	testq	%r15, %r15
	je	.L3212
.L3211:
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L3212:
	movq	-72(%rbp), %r14
	movq	-80(%rbp), %r12
	cmpq	%r12, %r14
	je	.L3216
	.p2align 4,,10
	.p2align 3
.L3220:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3217
	call	_ZdlPv@PLT
	addq	$104, %r12
	cmpq	%r14, %r12
	jne	.L3220
	movq	-80(%rbp), %r12
.L3216:
	testq	%r12, %r12
	je	.L3209
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L3209:
	cmpb	$0, 72(%rbx)
	jne	.L3222
	movq	%rbx, %rdi
	call	_ZNK2v88internal6torque9ClassType8FinalizeEv
.L3222:
	movq	88(%rbx), %rcx
	movq	80(%rbx), %rdx
	movq	%r13, %rdi
	movq	8(%r13), %rsi
	call	_ZNSt6vectorIN2v88internal6torque5FieldESaIS3_EE15_M_range_insertIN9__gnu_cxx17__normal_iteratorIPKS3_S5_EEEEvNS8_IPS3_S5_EET_SE_St20forward_iterator_tag
.LEHE383:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3236
	addq	$40, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3217:
	.cfi_restore_state
	addq	$104, %r12
	cmpq	%r12, %r14
	jne	.L3220
	movq	-80(%rbp), %r12
	jmp	.L3216
	.p2align 4,,10
	.p2align 3
.L3213:
	addq	$104, %r12
	cmpq	%r12, %r14
	jne	.L3210
	testq	%r15, %r15
	jne	.L3211
	jmp	.L3212
.L3236:
	call	__stack_chk_fail@PLT
.L3226:
	endbr64
	movq	%rax, %r12
	jmp	.L3223
	.section	.gcc_except_table._ZNK2v88internal6torque9ClassType16ComputeAllFieldsEv,"a",@progbits
.LLSDA6594:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6594-.LLSDACSB6594
.LLSDACSB6594:
	.uleb128 .LEHB383-.LFB6594
	.uleb128 .LEHE383-.LEHB383
	.uleb128 .L3226-.LFB6594
	.uleb128 0
.LLSDACSE6594:
	.section	.text._ZNK2v88internal6torque9ClassType16ComputeAllFieldsEv
	.cfi_endproc
	.section	.text.unlikely._ZNK2v88internal6torque9ClassType16ComputeAllFieldsEv
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6594
	.type	_ZNK2v88internal6torque9ClassType16ComputeAllFieldsEv.cold, @function
_ZNK2v88internal6torque9ClassType16ComputeAllFieldsEv.cold:
.LFSB6594:
.L3223:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	%r13, %rdi
	call	_ZNSt6vectorIN2v88internal6torque5FieldESaIS3_EED1Ev
	movq	%r12, %rdi
.LEHB384:
	call	_Unwind_Resume@PLT
.LEHE384:
	.cfi_endproc
.LFE6594:
	.section	.gcc_except_table._ZNK2v88internal6torque9ClassType16ComputeAllFieldsEv
.LLSDAC6594:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6594-.LLSDACSBC6594
.LLSDACSBC6594:
	.uleb128 .LEHB384-.LCOLDB113
	.uleb128 .LEHE384-.LEHB384
	.uleb128 0
	.uleb128 0
.LLSDACSEC6594:
	.section	.text.unlikely._ZNK2v88internal6torque9ClassType16ComputeAllFieldsEv
	.section	.text._ZNK2v88internal6torque9ClassType16ComputeAllFieldsEv
	.size	_ZNK2v88internal6torque9ClassType16ComputeAllFieldsEv, .-_ZNK2v88internal6torque9ClassType16ComputeAllFieldsEv
	.section	.text.unlikely._ZNK2v88internal6torque9ClassType16ComputeAllFieldsEv
	.size	_ZNK2v88internal6torque9ClassType16ComputeAllFieldsEv.cold, .-_ZNK2v88internal6torque9ClassType16ComputeAllFieldsEv.cold
.LCOLDE113:
	.section	.text._ZNK2v88internal6torque9ClassType16ComputeAllFieldsEv
.LHOTE113:
	.section	.text._ZNSt6vectorISt10unique_ptrIN2v88internal6torque7AstNodeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_,"axG",@progbits,_ZNSt6vectorISt10unique_ptrIN2v88internal6torque7AstNodeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorISt10unique_ptrIN2v88internal6torque7AstNodeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	.type	_ZNSt6vectorISt10unique_ptrIN2v88internal6torque7AstNodeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_, @function
_ZNSt6vectorISt10unique_ptrIN2v88internal6torque7AstNodeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_:
.LFB11359:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rax
	movq	(%rdi), %r12
	movq	%rdi, -80(%rbp)
	movq	%rax, -72(%rbp)
	subq	%r12, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L3261
	movq	%rsi, %r9
	movq	%rsi, %r15
	movq	%rsi, %rbx
	subq	%r12, %r9
	testq	%rax, %rax
	je	.L3252
	movabsq	$9223372036854775800, %r13
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L3262
.L3239:
	movq	%r13, %rdi
	movq	%rdx, -96(%rbp)
	movq	%r9, -88(%rbp)
	call	_Znwm@PLT
	movq	-88(%rbp), %r9
	movq	-96(%rbp), %rdx
	addq	%rax, %r13
	movq	%rax, -56(%rbp)
	leaq	8(%rax), %rcx
	movq	%r13, -64(%rbp)
.L3251:
	movq	(%rdx), %rax
	movq	-56(%rbp), %rsi
	movq	$0, (%rdx)
	movq	%rax, (%rsi,%r9)
	cmpq	%r12, %r15
	je	.L3241
	movq	%rsi, %r13
	movq	%r12, %r14
	.p2align 4,,10
	.p2align 3
.L3245:
	movq	(%r14), %rcx
	movq	$0, (%r14)
	movq	%rcx, 0(%r13)
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L3242
	movq	(%rdi), %rcx
	addq	$8, %r14
	addq	$8, %r13
	call	*8(%rcx)
	cmpq	%r14, %r15
	jne	.L3245
.L3243:
	movq	-56(%rbp), %rsi
	movq	%r15, %rax
	subq	%r12, %rax
	leaq	8(%rsi,%rax), %rcx
.L3241:
	movq	-72(%rbp), %rax
	cmpq	%rax, %r15
	je	.L3246
	movq	%rax, %r14
	subq	%r15, %r14
	leaq	-8(%r14), %r9
	movq	%r9, %rdi
	shrq	$3, %rdi
	addq	$1, %rdi
	testq	%r9, %r9
	je	.L3254
	movq	%rdi, %rdx
	xorl	%eax, %eax
	shrq	%rdx
	salq	$4, %rdx
	.p2align 4,,10
	.p2align 3
.L3248:
	movdqu	(%r15,%rax), %xmm1
	movups	%xmm1, (%rcx,%rax)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L3248
	movq	%rdi, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %r8
	leaq	(%rcx,%r8), %rdx
	leaq	(%r15,%r8), %rbx
	cmpq	%rax, %rdi
	je	.L3249
.L3247:
	movq	(%rbx), %rax
	movq	%rax, (%rdx)
.L3249:
	leaq	8(%rcx,%r9), %rcx
.L3246:
	testq	%r12, %r12
	je	.L3250
	movq	%r12, %rdi
	movq	%rcx, -72(%rbp)
	call	_ZdlPv@PLT
	movq	-72(%rbp), %rcx
.L3250:
	movq	-56(%rbp), %xmm0
	movq	-80(%rbp), %rax
	movq	%rcx, %xmm2
	movq	-64(%rbp), %rbx
	punpcklqdq	%xmm2, %xmm0
	movq	%rbx, 16(%rax)
	movups	%xmm0, (%rax)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3242:
	.cfi_restore_state
	addq	$8, %r14
	addq	$8, %r13
	cmpq	%r14, %r15
	jne	.L3245
	jmp	.L3243
	.p2align 4,,10
	.p2align 3
.L3262:
	testq	%rdi, %rdi
	jne	.L3240
	movq	$0, -64(%rbp)
	movl	$8, %ecx
	movq	$0, -56(%rbp)
	jmp	.L3251
	.p2align 4,,10
	.p2align 3
.L3252:
	movl	$8, %r13d
	jmp	.L3239
.L3254:
	movq	%rcx, %rdx
	jmp	.L3247
.L3240:
	cmpq	%rcx, %rdi
	movq	%rcx, %rax
	cmovbe	%rdi, %rax
	leaq	0(,%rax,8), %r13
	jmp	.L3239
.L3261:
	leaq	.LC84(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE11359:
	.size	_ZNSt6vectorISt10unique_ptrIN2v88internal6torque7AstNodeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_, .-_ZNSt6vectorISt10unique_ptrIN2v88internal6torque7AstNodeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	.section	.text._ZN2v88internal6torque8MakeNodeINS1_10IdentifierEJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEPT_DpT0_,"axG",@progbits,_ZN2v88internal6torque8MakeNodeINS1_10IdentifierEJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEPT_DpT0_,comdat
	.p2align 4
	.weak	_ZN2v88internal6torque8MakeNodeINS1_10IdentifierEJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEPT_DpT0_
	.type	_ZN2v88internal6torque8MakeNodeINS1_10IdentifierEJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEPT_DpT0_, @function
_ZN2v88internal6torque8MakeNodeINS1_10IdentifierEJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEPT_DpT0_:
.LFB7999:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA7999
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-80(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
.LEHB385:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_10CurrentAstEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rbx
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
.LEHE385:
	movq	(%r12), %rdx
	movq	(%rax), %rax
	movl	(%rax), %r15d
	movq	4(%rax), %r14
	movq	%r13, -96(%rbp)
	movq	12(%rax), %rax
	movq	%rax, -120(%rbp)
	leaq	16(%r12), %rax
	cmpq	%rax, %rdx
	je	.L3287
	movq	%rdx, -96(%rbp)
	movq	16(%r12), %rdx
	movq	%rdx, -80(%rbp)
.L3265:
	movq	8(%r12), %rdx
	movq	%rax, (%r12)
	movl	$64, %edi
	movq	$0, 8(%r12)
	movb	$0, 16(%r12)
	movq	%rdx, -88(%rbp)
.LEHB386:
	call	_Znwm@PLT
.LEHE386:
	movq	%rax, %r12
	movl	$52, 8(%rax)
	movl	%r15d, 12(%rax)
	movq	%r14, 16(%rax)
	movq	-120(%rbp), %rax
	movq	%rax, 24(%r12)
	leaq	16+_ZTVN2v88internal6torque10IdentifierE(%rip), %rax
	movq	%rax, (%r12)
	leaq	48(%r12), %rax
	movq	%rax, 32(%r12)
	movq	-96(%rbp), %rax
	cmpq	%r13, %rax
	je	.L3288
	movq	%rax, 32(%r12)
	movq	-80(%rbp), %rax
	movq	%rax, 48(%r12)
.L3267:
	movq	-88(%rbp), %rax
	movq	%r13, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%rax, 40(%r12)
	movq	32(%rbx), %rsi
	movb	$0, -80(%rbp)
	movq	%r12, -104(%rbp)
	cmpq	40(%rbx), %rsi
	je	.L3268
	movq	$0, -104(%rbp)
	movq	%r12, (%rsi)
	addq	$8, 32(%rbx)
.L3269:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3270
	movq	(%rdi), %rax
	call	*8(%rax)
.L3270:
	movq	-96(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L3263
	call	_ZdlPv@PLT
.L3263:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3289
	addq	$88, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3287:
	.cfi_restore_state
	movdqu	16(%r12), %xmm0
	movaps	%xmm0, -80(%rbp)
	jmp	.L3265
	.p2align 4,,10
	.p2align 3
.L3288:
	movdqa	-80(%rbp), %xmm1
	movups	%xmm1, 48(%r12)
	jmp	.L3267
	.p2align 4,,10
	.p2align 3
.L3268:
	leaq	-104(%rbp), %rdx
	leaq	24(%rbx), %rdi
.LEHB387:
	call	_ZNSt6vectorISt10unique_ptrIN2v88internal6torque7AstNodeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
.LEHE387:
	jmp	.L3269
.L3289:
	call	__stack_chk_fail@PLT
.L3279:
	endbr64
	movq	%rax, %r12
	jmp	.L3273
.L3278:
	endbr64
	movq	%rax, %r12
	jmp	.L3275
.L3273:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3275
	movq	(%rdi), %rax
	call	*8(%rax)
.L3275:
	movq	-96(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L3276
	call	_ZdlPv@PLT
.L3276:
	movq	%r12, %rdi
.LEHB388:
	call	_Unwind_Resume@PLT
.LEHE388:
	.cfi_endproc
.LFE7999:
	.section	.gcc_except_table._ZN2v88internal6torque8MakeNodeINS1_10IdentifierEJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEPT_DpT0_,"aG",@progbits,_ZN2v88internal6torque8MakeNodeINS1_10IdentifierEJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEPT_DpT0_,comdat
.LLSDA7999:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE7999-.LLSDACSB7999
.LLSDACSB7999:
	.uleb128 .LEHB385-.LFB7999
	.uleb128 .LEHE385-.LEHB385
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB386-.LFB7999
	.uleb128 .LEHE386-.LEHB386
	.uleb128 .L3278-.LFB7999
	.uleb128 0
	.uleb128 .LEHB387-.LFB7999
	.uleb128 .LEHE387-.LEHB387
	.uleb128 .L3279-.LFB7999
	.uleb128 0
	.uleb128 .LEHB388-.LFB7999
	.uleb128 .LEHE388-.LEHB388
	.uleb128 0
	.uleb128 0
.LLSDACSE7999:
	.section	.text._ZN2v88internal6torque8MakeNodeINS1_10IdentifierEJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEPT_DpT0_,"axG",@progbits,_ZN2v88internal6torque8MakeNodeINS1_10IdentifierEJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEPT_DpT0_,comdat
	.size	_ZN2v88internal6torque8MakeNodeINS1_10IdentifierEJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEPT_DpT0_, .-_ZN2v88internal6torque8MakeNodeINS1_10IdentifierEJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEPT_DpT0_
	.section	.text._ZN2v88internal6torque8MakeNodeINS1_20AssignmentExpressionEJPNS1_21FieldAccessExpressionEPNS1_20IdentifierExpressionEEEEPT_DpT0_,"axG",@progbits,_ZN2v88internal6torque8MakeNodeINS1_20AssignmentExpressionEJPNS1_21FieldAccessExpressionEPNS1_20IdentifierExpressionEEEEPT_DpT0_,comdat
	.p2align 4
	.weak	_ZN2v88internal6torque8MakeNodeINS1_20AssignmentExpressionEJPNS1_21FieldAccessExpressionEPNS1_20IdentifierExpressionEEEEPT_DpT0_
	.type	_ZN2v88internal6torque8MakeNodeINS1_20AssignmentExpressionEJPNS1_21FieldAccessExpressionEPNS1_20IdentifierExpressionEEEEPT_DpT0_, @function
_ZN2v88internal6torque8MakeNodeINS1_20AssignmentExpressionEJPNS1_21FieldAccessExpressionEPNS1_20IdentifierExpressionEEEEPT_DpT0_:
.LFB8022:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA8022
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
.LEHB389:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_10CurrentAstEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rbx
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
	movl	$88, %edi
	movq	(%rax), %rax
	movl	(%rax), %ecx
	movq	4(%rax), %rdx
	movq	12(%rax), %r15
	movl	%ecx, -124(%rbp)
	movq	%rdx, -120(%rbp)
	call	_Znwm@PLT
.LEHE389:
	movl	-124(%rbp), %ecx
	movq	-120(%rbp), %rdx
	leaq	-96(%rbp), %rsi
	movq	%rax, %r12
	movl	$14, 8(%rax)
	movl	%ecx, 12(%rax)
	leaq	40(%r12), %rdi
	movq	%rdx, 16(%rax)
	movq	%r15, 24(%rax)
	leaq	16+_ZTVN2v88internal6torque20AssignmentExpressionE(%rip), %rax
	movq	%rax, (%r12)
	movq	%r14, 32(%r12)
	movb	$0, -96(%rbp)
	movb	$0, -88(%rbp)
	call	_ZN2v84base8internal15OptionalStorageINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEELb0ELb0EEC1EOS9_
	cmpb	$0, -96(%rbp)
	movq	%r13, 80(%r12)
	je	.L3291
	movq	-88(%rbp), %rdi
	leaq	-72(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3291
	call	_ZdlPv@PLT
.L3291:
	movq	%r12, -104(%rbp)
	movq	32(%rbx), %rsi
	cmpq	40(%rbx), %rsi
	je	.L3292
	movq	$0, -104(%rbp)
	movq	%r12, (%rsi)
	addq	$8, 32(%rbx)
.L3293:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3290
	movq	(%rdi), %rax
	call	*8(%rax)
.L3290:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3306
	addq	$88, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3292:
	.cfi_restore_state
	leaq	-104(%rbp), %rdx
	leaq	24(%rbx), %rdi
.LEHB390:
	call	_ZNSt6vectorISt10unique_ptrIN2v88internal6torque7AstNodeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
.LEHE390:
	jmp	.L3293
.L3306:
	call	__stack_chk_fail@PLT
.L3298:
	endbr64
	movq	%rax, %r12
.L3295:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3296
	movq	(%rdi), %rax
	call	*8(%rax)
.L3296:
	movq	%r12, %rdi
.LEHB391:
	call	_Unwind_Resume@PLT
.LEHE391:
	.cfi_endproc
.LFE8022:
	.section	.gcc_except_table._ZN2v88internal6torque8MakeNodeINS1_20AssignmentExpressionEJPNS1_21FieldAccessExpressionEPNS1_20IdentifierExpressionEEEEPT_DpT0_,"aG",@progbits,_ZN2v88internal6torque8MakeNodeINS1_20AssignmentExpressionEJPNS1_21FieldAccessExpressionEPNS1_20IdentifierExpressionEEEEPT_DpT0_,comdat
.LLSDA8022:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE8022-.LLSDACSB8022
.LLSDACSB8022:
	.uleb128 .LEHB389-.LFB8022
	.uleb128 .LEHE389-.LEHB389
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB390-.LFB8022
	.uleb128 .LEHE390-.LEHB390
	.uleb128 .L3298-.LFB8022
	.uleb128 0
	.uleb128 .LEHB391-.LFB8022
	.uleb128 .LEHE391-.LEHB391
	.uleb128 0
	.uleb128 0
.LLSDACSE8022:
	.section	.text._ZN2v88internal6torque8MakeNodeINS1_20AssignmentExpressionEJPNS1_21FieldAccessExpressionEPNS1_20IdentifierExpressionEEEEPT_DpT0_,"axG",@progbits,_ZN2v88internal6torque8MakeNodeINS1_20AssignmentExpressionEJPNS1_21FieldAccessExpressionEPNS1_20IdentifierExpressionEEEEPT_DpT0_,comdat
	.size	_ZN2v88internal6torque8MakeNodeINS1_20AssignmentExpressionEJPNS1_21FieldAccessExpressionEPNS1_20IdentifierExpressionEEEEPT_DpT0_, .-_ZN2v88internal6torque8MakeNodeINS1_20AssignmentExpressionEJPNS1_21FieldAccessExpressionEPNS1_20IdentifierExpressionEEEEPT_DpT0_
	.section	.text._ZN2v88internal6torque8MakeNodeINS1_19ExpressionStatementEJPNS1_20AssignmentExpressionEEEEPT_DpT0_,"axG",@progbits,_ZN2v88internal6torque8MakeNodeINS1_19ExpressionStatementEJPNS1_20AssignmentExpressionEEEEPT_DpT0_,comdat
	.p2align 4
	.weak	_ZN2v88internal6torque8MakeNodeINS1_19ExpressionStatementEJPNS1_20AssignmentExpressionEEEEPT_DpT0_
	.type	_ZN2v88internal6torque8MakeNodeINS1_19ExpressionStatementEJPNS1_20AssignmentExpressionEEEEPT_DpT0_, @function
_ZN2v88internal6torque8MakeNodeINS1_19ExpressionStatementEJPNS1_20AssignmentExpressionEEEEPT_DpT0_:
.LFB8025:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA8025
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
.LEHB392:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_10CurrentAstEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rbx
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
	movl	$40, %edi
	movq	(%rax), %rax
	movl	(%rax), %edx
	movq	4(%rax), %r15
	movq	12(%rax), %r14
	movl	%edx, -68(%rbp)
	call	_Znwm@PLT
.LEHE392:
	movl	-68(%rbp), %edx
	movl	$24, 8(%rax)
	movq	%rax, %r12
	movl	%edx, 12(%rax)
	movq	%r15, 16(%rax)
	movq	%r14, 24(%rax)
	leaq	16+_ZTVN2v88internal6torque19ExpressionStatementE(%rip), %rax
	movq	%rax, (%r12)
	movq	%r13, 32(%r12)
	movq	32(%rbx), %rsi
	movq	%r12, -64(%rbp)
	cmpq	40(%rbx), %rsi
	je	.L3308
	movq	$0, -64(%rbp)
	movq	%r12, (%rsi)
	addq	$8, 32(%rbx)
.L3309:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3307
	movq	(%rdi), %rax
	call	*8(%rax)
.L3307:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3322
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3308:
	.cfi_restore_state
	leaq	-64(%rbp), %rdx
	leaq	24(%rbx), %rdi
.LEHB393:
	call	_ZNSt6vectorISt10unique_ptrIN2v88internal6torque7AstNodeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
.LEHE393:
	jmp	.L3309
.L3322:
	call	__stack_chk_fail@PLT
.L3314:
	endbr64
	movq	%rax, %r12
.L3311:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3312
	movq	(%rdi), %rax
	call	*8(%rax)
.L3312:
	movq	%r12, %rdi
.LEHB394:
	call	_Unwind_Resume@PLT
.LEHE394:
	.cfi_endproc
.LFE8025:
	.section	.gcc_except_table._ZN2v88internal6torque8MakeNodeINS1_19ExpressionStatementEJPNS1_20AssignmentExpressionEEEEPT_DpT0_,"aG",@progbits,_ZN2v88internal6torque8MakeNodeINS1_19ExpressionStatementEJPNS1_20AssignmentExpressionEEEEPT_DpT0_,comdat
.LLSDA8025:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE8025-.LLSDACSB8025
.LLSDACSB8025:
	.uleb128 .LEHB392-.LFB8025
	.uleb128 .LEHE392-.LEHB392
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB393-.LFB8025
	.uleb128 .LEHE393-.LEHB393
	.uleb128 .L3314-.LFB8025
	.uleb128 0
	.uleb128 .LEHB394-.LFB8025
	.uleb128 .LEHE394-.LEHB394
	.uleb128 0
	.uleb128 0
.LLSDACSE8025:
	.section	.text._ZN2v88internal6torque8MakeNodeINS1_19ExpressionStatementEJPNS1_20AssignmentExpressionEEEEPT_DpT0_,"axG",@progbits,_ZN2v88internal6torque8MakeNodeINS1_19ExpressionStatementEJPNS1_20AssignmentExpressionEEEEPT_DpT0_,comdat
	.size	_ZN2v88internal6torque8MakeNodeINS1_19ExpressionStatementEJPNS1_20AssignmentExpressionEEEEPT_DpT0_, .-_ZN2v88internal6torque8MakeNodeINS1_19ExpressionStatementEJPNS1_20AssignmentExpressionEEEEPT_DpT0_
	.section	.text._ZN2v88internal6torque8MakeNodeINS1_15ReturnStatementEJPNS1_21FieldAccessExpressionEEEEPT_DpT0_,"axG",@progbits,_ZN2v88internal6torque8MakeNodeINS1_15ReturnStatementEJPNS1_21FieldAccessExpressionEEEEPT_DpT0_,comdat
	.p2align 4
	.weak	_ZN2v88internal6torque8MakeNodeINS1_15ReturnStatementEJPNS1_21FieldAccessExpressionEEEEPT_DpT0_
	.type	_ZN2v88internal6torque8MakeNodeINS1_15ReturnStatementEJPNS1_21FieldAccessExpressionEEEEPT_DpT0_, @function
_ZN2v88internal6torque8MakeNodeINS1_15ReturnStatementEJPNS1_21FieldAccessExpressionEEEEPT_DpT0_:
.LFB8016:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA8016
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
.LEHB395:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_10CurrentAstEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rbx
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
	movl	$48, %edi
	movq	(%rax), %rax
	movl	(%rax), %edx
	movq	4(%rax), %r15
	movq	12(%rax), %r14
	movl	%edx, -68(%rbp)
	call	_Znwm@PLT
.LEHE395:
	movl	-68(%rbp), %edx
	movl	$30, 8(%rax)
	movq	%rax, %r12
	movl	%edx, 12(%rax)
	movq	%r15, 16(%rax)
	movq	%r14, 24(%rax)
	leaq	16+_ZTVN2v88internal6torque15ReturnStatementE(%rip), %rax
	movq	%rax, (%r12)
	movb	$1, 32(%r12)
	movq	%r13, 40(%r12)
	movq	32(%rbx), %rsi
	movq	%r12, -64(%rbp)
	cmpq	40(%rbx), %rsi
	je	.L3324
	movq	$0, -64(%rbp)
	movq	%r12, (%rsi)
	addq	$8, 32(%rbx)
.L3325:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3323
	movq	(%rdi), %rax
	call	*8(%rax)
.L3323:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3338
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3324:
	.cfi_restore_state
	leaq	-64(%rbp), %rdx
	leaq	24(%rbx), %rdi
.LEHB396:
	call	_ZNSt6vectorISt10unique_ptrIN2v88internal6torque7AstNodeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
.LEHE396:
	jmp	.L3325
.L3338:
	call	__stack_chk_fail@PLT
.L3330:
	endbr64
	movq	%rax, %r12
.L3327:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3328
	movq	(%rdi), %rax
	call	*8(%rax)
.L3328:
	movq	%r12, %rdi
.LEHB397:
	call	_Unwind_Resume@PLT
.LEHE397:
	.cfi_endproc
.LFE8016:
	.section	.gcc_except_table._ZN2v88internal6torque8MakeNodeINS1_15ReturnStatementEJPNS1_21FieldAccessExpressionEEEEPT_DpT0_,"aG",@progbits,_ZN2v88internal6torque8MakeNodeINS1_15ReturnStatementEJPNS1_21FieldAccessExpressionEEEEPT_DpT0_,comdat
.LLSDA8016:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE8016-.LLSDACSB8016
.LLSDACSB8016:
	.uleb128 .LEHB395-.LFB8016
	.uleb128 .LEHE395-.LEHB395
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB396-.LFB8016
	.uleb128 .LEHE396-.LEHB396
	.uleb128 .L3330-.LFB8016
	.uleb128 0
	.uleb128 .LEHB397-.LFB8016
	.uleb128 .LEHE397-.LEHB397
	.uleb128 0
	.uleb128 0
.LLSDACSE8016:
	.section	.text._ZN2v88internal6torque8MakeNodeINS1_15ReturnStatementEJPNS1_21FieldAccessExpressionEEEEPT_DpT0_,"axG",@progbits,_ZN2v88internal6torque8MakeNodeINS1_15ReturnStatementEJPNS1_21FieldAccessExpressionEEEEPT_DpT0_,comdat
	.size	_ZN2v88internal6torque8MakeNodeINS1_15ReturnStatementEJPNS1_21FieldAccessExpressionEEEEPT_DpT0_, .-_ZN2v88internal6torque8MakeNodeINS1_15ReturnStatementEJPNS1_21FieldAccessExpressionEEEEPT_DpT0_
	.section	.text._ZN2v88internal6torque8MakeNodeINS1_21FieldAccessExpressionEJPNS1_20IdentifierExpressionEPNS1_10IdentifierEEEEPT_DpT0_,"axG",@progbits,_ZN2v88internal6torque8MakeNodeINS1_21FieldAccessExpressionEJPNS1_20IdentifierExpressionEPNS1_10IdentifierEEEEPT_DpT0_,comdat
	.p2align 4
	.weak	_ZN2v88internal6torque8MakeNodeINS1_21FieldAccessExpressionEJPNS1_20IdentifierExpressionEPNS1_10IdentifierEEEEPT_DpT0_
	.type	_ZN2v88internal6torque8MakeNodeINS1_21FieldAccessExpressionEJPNS1_20IdentifierExpressionEPNS1_10IdentifierEEEEPT_DpT0_, @function
_ZN2v88internal6torque8MakeNodeINS1_21FieldAccessExpressionEJPNS1_20IdentifierExpressionEPNS1_10IdentifierEEEEPT_DpT0_:
.LFB8012:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA8012
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %xmm1
	movq	%rdi, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
.LEHB398:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_10CurrentAstEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rbx
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
	movl	$48, %edi
	movq	(%rax), %rax
	movl	(%rax), %r15d
	movq	4(%rax), %r14
	movq	12(%rax), %r13
	call	_Znwm@PLT
.LEHE398:
	movdqa	-80(%rbp), %xmm0
	movl	$11, 8(%rax)
	movq	%rax, %r12
	movl	%r15d, 12(%rax)
	movq	%r14, 16(%rax)
	movq	%r13, 24(%rax)
	leaq	16+_ZTVN2v88internal6torque21FieldAccessExpressionE(%rip), %rax
	movq	%rax, (%r12)
	movups	%xmm0, 32(%r12)
	movq	32(%rbx), %rsi
	movq	%r12, -64(%rbp)
	cmpq	40(%rbx), %rsi
	je	.L3340
	movq	$0, -64(%rbp)
	movq	%r12, (%rsi)
	addq	$8, 32(%rbx)
.L3341:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3339
	movq	(%rdi), %rax
	call	*8(%rax)
.L3339:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3354
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3340:
	.cfi_restore_state
	leaq	-64(%rbp), %rdx
	leaq	24(%rbx), %rdi
.LEHB399:
	call	_ZNSt6vectorISt10unique_ptrIN2v88internal6torque7AstNodeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
.LEHE399:
	jmp	.L3341
.L3354:
	call	__stack_chk_fail@PLT
.L3346:
	endbr64
	movq	%rax, %r12
.L3343:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3344
	movq	(%rdi), %rax
	call	*8(%rax)
.L3344:
	movq	%r12, %rdi
.LEHB400:
	call	_Unwind_Resume@PLT
.LEHE400:
	.cfi_endproc
.LFE8012:
	.section	.gcc_except_table._ZN2v88internal6torque8MakeNodeINS1_21FieldAccessExpressionEJPNS1_20IdentifierExpressionEPNS1_10IdentifierEEEEPT_DpT0_,"aG",@progbits,_ZN2v88internal6torque8MakeNodeINS1_21FieldAccessExpressionEJPNS1_20IdentifierExpressionEPNS1_10IdentifierEEEEPT_DpT0_,comdat
.LLSDA8012:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE8012-.LLSDACSB8012
.LLSDACSB8012:
	.uleb128 .LEHB398-.LFB8012
	.uleb128 .LEHE398-.LEHB398
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB399-.LFB8012
	.uleb128 .LEHE399-.LEHB399
	.uleb128 .L3346-.LFB8012
	.uleb128 0
	.uleb128 .LEHB400-.LFB8012
	.uleb128 .LEHE400-.LEHB400
	.uleb128 0
	.uleb128 0
.LLSDACSE8012:
	.section	.text._ZN2v88internal6torque8MakeNodeINS1_21FieldAccessExpressionEJPNS1_20IdentifierExpressionEPNS1_10IdentifierEEEEPT_DpT0_,"axG",@progbits,_ZN2v88internal6torque8MakeNodeINS1_21FieldAccessExpressionEJPNS1_20IdentifierExpressionEPNS1_10IdentifierEEEEPT_DpT0_,comdat
	.size	_ZN2v88internal6torque8MakeNodeINS1_21FieldAccessExpressionEJPNS1_20IdentifierExpressionEPNS1_10IdentifierEEEEPT_DpT0_, .-_ZN2v88internal6torque8MakeNodeINS1_21FieldAccessExpressionEJPNS1_20IdentifierExpressionEPNS1_10IdentifierEEEEPT_DpT0_
	.section	.text._ZN2v88internal6torque8MakeNodeINS1_20IdentifierExpressionEJPNS1_10IdentifierEEEEPT_DpT0_,"axG",@progbits,_ZN2v88internal6torque8MakeNodeINS1_20IdentifierExpressionEJPNS1_10IdentifierEEEEPT_DpT0_,comdat
	.p2align 4
	.weak	_ZN2v88internal6torque8MakeNodeINS1_20IdentifierExpressionEJPNS1_10IdentifierEEEEPT_DpT0_
	.type	_ZN2v88internal6torque8MakeNodeINS1_20IdentifierExpressionEJPNS1_10IdentifierEEEEPT_DpT0_, @function
_ZN2v88internal6torque8MakeNodeINS1_20IdentifierExpressionEJPNS1_10IdentifierEEEEPT_DpT0_:
.LFB8002:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA8002
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
.LEHB401:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_10CurrentAstEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rbx
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
	movl	$88, %edi
	movq	(%rax), %rax
	movl	(%rax), %edx
	movq	4(%rax), %r15
	movq	12(%rax), %r14
	movl	%edx, -68(%rbp)
	call	_Znwm@PLT
.LEHE401:
	movl	-68(%rbp), %edx
	pxor	%xmm0, %xmm0
	movl	$8, 8(%rax)
	movq	%rax, %r12
	movl	%edx, 12(%rax)
	movq	%r15, 16(%rax)
	movq	%r14, 24(%rax)
	leaq	16+_ZTVN2v88internal6torque20IdentifierExpressionE(%rip), %rax
	movq	%rax, (%r12)
	movq	$0, 48(%r12)
	movq	%r13, 56(%r12)
	movq	$0, 80(%r12)
	movups	%xmm0, 32(%r12)
	movups	%xmm0, 64(%r12)
	movq	32(%rbx), %rsi
	movq	%r12, -64(%rbp)
	cmpq	40(%rbx), %rsi
	je	.L3356
	movq	$0, -64(%rbp)
	movq	%r12, (%rsi)
	addq	$8, 32(%rbx)
.L3357:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3355
	movq	(%rdi), %rax
	call	*8(%rax)
.L3355:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3370
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3356:
	.cfi_restore_state
	leaq	-64(%rbp), %rdx
	leaq	24(%rbx), %rdi
.LEHB402:
	call	_ZNSt6vectorISt10unique_ptrIN2v88internal6torque7AstNodeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
.LEHE402:
	jmp	.L3357
.L3370:
	call	__stack_chk_fail@PLT
.L3362:
	endbr64
	movq	%rax, %r12
.L3359:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3360
	movq	(%rdi), %rax
	call	*8(%rax)
.L3360:
	movq	%r12, %rdi
.LEHB403:
	call	_Unwind_Resume@PLT
.LEHE403:
	.cfi_endproc
.LFE8002:
	.section	.gcc_except_table._ZN2v88internal6torque8MakeNodeINS1_20IdentifierExpressionEJPNS1_10IdentifierEEEEPT_DpT0_,"aG",@progbits,_ZN2v88internal6torque8MakeNodeINS1_20IdentifierExpressionEJPNS1_10IdentifierEEEEPT_DpT0_,comdat
.LLSDA8002:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE8002-.LLSDACSB8002
.LLSDACSB8002:
	.uleb128 .LEHB401-.LFB8002
	.uleb128 .LEHE401-.LEHB401
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB402-.LFB8002
	.uleb128 .LEHE402-.LEHB402
	.uleb128 .L3362-.LFB8002
	.uleb128 0
	.uleb128 .LEHB403-.LFB8002
	.uleb128 .LEHE403-.LEHB403
	.uleb128 0
	.uleb128 0
.LLSDACSE8002:
	.section	.text._ZN2v88internal6torque8MakeNodeINS1_20IdentifierExpressionEJPNS1_10IdentifierEEEEPT_DpT0_,"axG",@progbits,_ZN2v88internal6torque8MakeNodeINS1_20IdentifierExpressionEJPNS1_10IdentifierEEEEPT_DpT0_,comdat
	.size	_ZN2v88internal6torque8MakeNodeINS1_20IdentifierExpressionEJPNS1_10IdentifierEEEEPT_DpT0_, .-_ZN2v88internal6torque8MakeNodeINS1_20IdentifierExpressionEJPNS1_10IdentifierEEEEPT_DpT0_
	.section	.text._ZN2v88internal6torque8MakeNodeINS1_20IdentifierExpressionEJSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaISA_EEPNS1_10IdentifierEEEEPT_DpT0_,"axG",@progbits,_ZN2v88internal6torque8MakeNodeINS1_20IdentifierExpressionEJSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaISA_EEPNS1_10IdentifierEEEEPT_DpT0_,comdat
	.p2align 4
	.weak	_ZN2v88internal6torque8MakeNodeINS1_20IdentifierExpressionEJSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaISA_EEPNS1_10IdentifierEEEEPT_DpT0_
	.type	_ZN2v88internal6torque8MakeNodeINS1_20IdentifierExpressionEJSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaISA_EEPNS1_10IdentifierEEEEPT_DpT0_, @function
_ZN2v88internal6torque8MakeNodeINS1_20IdentifierExpressionEJSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaISA_EEPNS1_10IdentifierEEEEPT_DpT0_:
.LFB8020:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA8020
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -104(%rbp)
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
.LEHB404:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_10CurrentAstEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rbx
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
.LEHE404:
	movdqu	(%r12), %xmm1
	pxor	%xmm0, %xmm0
	movl	$88, %edi
	movq	(%rax), %rax
	movl	(%rax), %r15d
	movq	4(%rax), %r14
	movq	12(%rax), %r13
	movq	16(%r12), %rax
	movups	%xmm0, (%r12)
	movq	$0, 16(%r12)
	movq	%rax, -64(%rbp)
	movaps	%xmm1, -80(%rbp)
.LEHB405:
	call	_Znwm@PLT
.LEHE405:
	movl	%r15d, 12(%rax)
	movq	%rax, %r12
	movdqa	-80(%rbp), %xmm2
	pxor	%xmm0, %xmm0
	movl	$8, 8(%rax)
	movq	%r14, 16(%rax)
	movq	%r13, 24(%rax)
	leaq	16+_ZTVN2v88internal6torque20IdentifierExpressionE(%rip), %rax
	movq	%rax, (%r12)
	movq	-64(%rbp), %rax
	movq	$0, 80(%r12)
	movq	%rax, 48(%r12)
	movq	-104(%rbp), %rax
	movups	%xmm2, 32(%r12)
	movq	%rax, 56(%r12)
	movups	%xmm0, 64(%r12)
	movq	32(%rbx), %rsi
	movq	$0, -64(%rbp)
	movq	%r12, -88(%rbp)
	movaps	%xmm0, -80(%rbp)
	cmpq	40(%rbx), %rsi
	je	.L3372
	movq	$0, -88(%rbp)
	movq	%r12, (%rsi)
	addq	$8, 32(%rbx)
.L3373:
	movq	-88(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3374
	movq	(%rdi), %rax
	call	*8(%rax)
.L3374:
	movq	-72(%rbp), %rbx
	movq	-80(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L3375
	.p2align 4,,10
	.p2align 3
.L3376:
	movq	0(%r13), %rdi
	leaq	16(%r13), %rax
	cmpq	%rax, %rdi
	je	.L3380
	call	_ZdlPv@PLT
	addq	$32, %r13
	cmpq	%rbx, %r13
	jne	.L3376
.L3381:
	movq	-80(%rbp), %r13
.L3375:
	testq	%r13, %r13
	je	.L3371
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L3371:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3397
	addq	$72, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3380:
	.cfi_restore_state
	addq	$32, %r13
	cmpq	%r13, %rbx
	jne	.L3376
	jmp	.L3381
	.p2align 4,,10
	.p2align 3
.L3372:
	leaq	-88(%rbp), %rdx
	leaq	24(%rbx), %rdi
.LEHB406:
	call	_ZNSt6vectorISt10unique_ptrIN2v88internal6torque7AstNodeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
.LEHE406:
	jmp	.L3373
.L3397:
	call	__stack_chk_fail@PLT
.L3386:
	endbr64
	movq	%rax, %r12
	jmp	.L3377
.L3385:
	endbr64
	movq	%rax, %r12
	jmp	.L3379
.L3377:
	movq	-88(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3379
	movq	(%rdi), %rax
	call	*8(%rax)
.L3379:
	leaq	-80(%rbp), %rdi
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EED1Ev
	movq	%r12, %rdi
.LEHB407:
	call	_Unwind_Resume@PLT
.LEHE407:
	.cfi_endproc
.LFE8020:
	.section	.gcc_except_table._ZN2v88internal6torque8MakeNodeINS1_20IdentifierExpressionEJSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaISA_EEPNS1_10IdentifierEEEEPT_DpT0_,"aG",@progbits,_ZN2v88internal6torque8MakeNodeINS1_20IdentifierExpressionEJSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaISA_EEPNS1_10IdentifierEEEEPT_DpT0_,comdat
.LLSDA8020:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE8020-.LLSDACSB8020
.LLSDACSB8020:
	.uleb128 .LEHB404-.LFB8020
	.uleb128 .LEHE404-.LEHB404
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB405-.LFB8020
	.uleb128 .LEHE405-.LEHB405
	.uleb128 .L3385-.LFB8020
	.uleb128 0
	.uleb128 .LEHB406-.LFB8020
	.uleb128 .LEHE406-.LEHB406
	.uleb128 .L3386-.LFB8020
	.uleb128 0
	.uleb128 .LEHB407-.LFB8020
	.uleb128 .LEHE407-.LEHB407
	.uleb128 0
	.uleb128 0
.LLSDACSE8020:
	.section	.text._ZN2v88internal6torque8MakeNodeINS1_20IdentifierExpressionEJSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaISA_EEPNS1_10IdentifierEEEEPT_DpT0_,"axG",@progbits,_ZN2v88internal6torque8MakeNodeINS1_20IdentifierExpressionEJSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaISA_EEPNS1_10IdentifierEEEEPT_DpT0_,comdat
	.size	_ZN2v88internal6torque8MakeNodeINS1_20IdentifierExpressionEJSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaISA_EEPNS1_10IdentifierEEEEPT_DpT0_, .-_ZN2v88internal6torque8MakeNodeINS1_20IdentifierExpressionEJSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaISA_EEPNS1_10IdentifierEEEEPT_DpT0_
	.section	.text._ZN2v88internal6torque8MakeNodeINS1_10IdentifierEJPKcEEEPT_DpT0_,"axG",@progbits,_ZN2v88internal6torque8MakeNodeINS1_10IdentifierEJPKcEEEPT_DpT0_,comdat
	.p2align 4
	.weak	_ZN2v88internal6torque8MakeNodeINS1_10IdentifierEJPKcEEEPT_DpT0_
	.type	_ZN2v88internal6torque8MakeNodeINS1_10IdentifierEJPKcEEEPT_DpT0_, @function
_ZN2v88internal6torque8MakeNodeINS1_10IdentifierEJPKcEEEPT_DpT0_:
.LFB8009:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA8009
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-80(%rbp), %rbx
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
.LEHB408:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_10CurrentAstEEERPNT_5ScopeEv@PLT
	movq	(%rax), %r13
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
.LEHE408:
	movq	(%rax), %rax
	movl	(%rax), %r15d
	movq	4(%rax), %r14
	movq	%rbx, -96(%rbp)
	movq	12(%rax), %rax
	movq	%rax, -120(%rbp)
	leaq	-96(%rbp), %rax
	movq	%rax, -128(%rbp)
	testq	%r12, %r12
	je	.L3426
	movq	%r12, %rdi
	call	strlen@PLT
	movq	%rax, -104(%rbp)
	movq	%rax, %r8
	cmpq	$15, %rax
	ja	.L3427
	cmpq	$1, %rax
	jne	.L3402
	movzbl	(%r12), %edx
	movb	%dl, -80(%rbp)
	movq	%rbx, %rdx
.L3403:
	movq	%rax, -88(%rbp)
	movl	$64, %edi
	movb	$0, (%rdx,%rax)
.LEHB409:
	call	_Znwm@PLT
.LEHE409:
	movq	%rax, %r12
	movl	$52, 8(%rax)
	movl	%r15d, 12(%rax)
	movq	%r14, 16(%rax)
	movq	-120(%rbp), %rax
	movq	%rax, 24(%r12)
	leaq	16+_ZTVN2v88internal6torque10IdentifierE(%rip), %rax
	movq	%rax, (%r12)
	leaq	48(%r12), %rax
	movq	%rax, 32(%r12)
	movq	-96(%rbp), %rax
	cmpq	%rbx, %rax
	je	.L3428
	movq	%rax, 32(%r12)
	movq	-80(%rbp), %rax
	movq	%rax, 48(%r12)
.L3405:
	movq	-88(%rbp), %rax
	movq	%r12, -104(%rbp)
	movq	%rbx, -96(%rbp)
	movq	%rax, 40(%r12)
	movq	32(%r13), %rsi
	movq	$0, -88(%rbp)
	movb	$0, -80(%rbp)
	cmpq	40(%r13), %rsi
	je	.L3406
	movq	$0, -104(%rbp)
	movq	%r12, (%rsi)
	addq	$8, 32(%r13)
.L3407:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3408
	movq	(%rdi), %rax
	call	*8(%rax)
.L3408:
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L3398
	call	_ZdlPv@PLT
.L3398:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3429
	addq	$104, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3402:
	.cfi_restore_state
	testq	%rax, %rax
	jne	.L3430
	movq	%rbx, %rdx
	jmp	.L3403
	.p2align 4,,10
	.p2align 3
.L3427:
	movq	-128(%rbp), %rdi
	leaq	-104(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rax, -136(%rbp)
.LEHB410:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-136(%rbp), %r8
	movq	%rax, -96(%rbp)
	movq	%rax, %rdi
	movq	-104(%rbp), %rax
	movq	%rax, -80(%rbp)
.L3401:
	movq	%r8, %rdx
	movq	%r12, %rsi
	call	memcpy@PLT
	movq	-104(%rbp), %rax
	movq	-96(%rbp), %rdx
	jmp	.L3403
	.p2align 4,,10
	.p2align 3
.L3426:
	leaq	.LC1(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE410:
	.p2align 4,,10
	.p2align 3
.L3428:
	movdqa	-80(%rbp), %xmm0
	movups	%xmm0, 48(%r12)
	jmp	.L3405
	.p2align 4,,10
	.p2align 3
.L3406:
	leaq	-104(%rbp), %rdx
	leaq	24(%r13), %rdi
.LEHB411:
	call	_ZNSt6vectorISt10unique_ptrIN2v88internal6torque7AstNodeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
.LEHE411:
	jmp	.L3407
.L3429:
	call	__stack_chk_fail@PLT
.L3430:
	movq	%rbx, %rdi
	jmp	.L3401
.L3418:
	endbr64
	movq	%rax, %r12
	jmp	.L3411
.L3417:
	endbr64
	movq	%rax, %r12
	jmp	.L3413
.L3411:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3413
	movq	(%rdi), %rax
	call	*8(%rax)
.L3413:
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L3414
	call	_ZdlPv@PLT
.L3414:
	movq	%r12, %rdi
.LEHB412:
	call	_Unwind_Resume@PLT
.LEHE412:
	.cfi_endproc
.LFE8009:
	.section	.gcc_except_table._ZN2v88internal6torque8MakeNodeINS1_10IdentifierEJPKcEEEPT_DpT0_,"aG",@progbits,_ZN2v88internal6torque8MakeNodeINS1_10IdentifierEJPKcEEEPT_DpT0_,comdat
.LLSDA8009:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE8009-.LLSDACSB8009
.LLSDACSB8009:
	.uleb128 .LEHB408-.LFB8009
	.uleb128 .LEHE408-.LEHB408
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB409-.LFB8009
	.uleb128 .LEHE409-.LEHB409
	.uleb128 .L3417-.LFB8009
	.uleb128 0
	.uleb128 .LEHB410-.LFB8009
	.uleb128 .LEHE410-.LEHB410
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB411-.LFB8009
	.uleb128 .LEHE411-.LEHB411
	.uleb128 .L3418-.LFB8009
	.uleb128 0
	.uleb128 .LEHB412-.LFB8009
	.uleb128 .LEHE412-.LEHB412
	.uleb128 0
	.uleb128 0
.LLSDACSE8009:
	.section	.text._ZN2v88internal6torque8MakeNodeINS1_10IdentifierEJPKcEEEPT_DpT0_,"axG",@progbits,_ZN2v88internal6torque8MakeNodeINS1_10IdentifierEJPKcEEEPT_DpT0_,comdat
	.size	_ZN2v88internal6torque8MakeNodeINS1_10IdentifierEJPKcEEEPT_DpT0_, .-_ZN2v88internal6torque8MakeNodeINS1_10IdentifierEJPKcEEEPT_DpT0_
	.section	.rodata._ZN2v88internal6torque9ClassType17GenerateAccessorsEv.str1.1,"aMS",@progbits,1
.LC114:
	.string	"Load"
.LC115:
	.string	"o"
.LC116:
	.string	"Store"
.LC117:
	.string	"v"
	.section	.text.unlikely._ZN2v88internal6torque9ClassType17GenerateAccessorsEv,"ax",@progbits
	.align 2
.LCOLDB118:
	.section	.text._ZN2v88internal6torque9ClassType17GenerateAccessorsEv,"ax",@progbits
.LHOTB118:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque9ClassType17GenerateAccessorsEv
	.type	_ZN2v88internal6torque9ClassType17GenerateAccessorsEv, @function
_ZN2v88internal6torque9ClassType17GenerateAccessorsEv:
.LFB6601:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6601
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$632, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	80(%rdi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	88(%rdi), %rax
	movq	%rax, -624(%rbp)
	cmpq	%rbx, %rax
	je	.L3431
	movq	%rdi, %r14
	.p2align 4,,10
	.p2align 3
.L3503:
	cmpb	$0, 32(%rbx)
	jne	.L3438
	leaq	-208(%rbp), %r12
	movq	80(%rbx), %r15
	leaq	-192(%rbp), %r13
.LEHB413:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_10TypeOracleEEERPNT_5ScopeEv@PLT
.LEHE413:
	movq	%r12, %rdi
	movq	%r13, -208(%rbp)
	movl	$1684631414, -192(%rbp)
	movq	$4, -200(%rbp)
	movb	$0, -188(%rbp)
.LEHB414:
	call	_ZN2v88internal6torque12Declarations16LookupGlobalTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
.LEHE414:
	movq	-208(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L3434
	movq	%rax, -616(%rbp)
	call	_ZdlPv@PLT
	movq	-616(%rbp), %rax
.L3434:
	cmpq	%rax, %r15
	je	.L3438
	movdqu	(%rbx), %xmm1
	movaps	%xmm1, -576(%rbp)
	movl	16(%rbx), %eax
	movl	%eax, -560(%rbp)
.LEHB415:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rax
	movq	%rax, -552(%rbp)
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
.LEHE415:
	leaq	-576(%rbp), %rdx
	movl	$111, %r8d
	movq	%r12, %rdi
	movq	%rdx, (%rax)
	movq	%r13, -208(%rbp)
	movq	$1, -200(%rbp)
	movw	%r8w, -192(%rbp)
.LEHB416:
	call	_ZN2v88internal6torque8MakeNodeINS1_10IdentifierEJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEPT_DpT0_
	movq	%rax, %rdi
	call	_ZN2v88internal6torque8MakeNodeINS1_20IdentifierExpressionEJPNS1_10IdentifierEEEEPT_DpT0_
.LEHE416:
	movq	-208(%rbp), %rdi
	movq	%rax, -640(%rbp)
	cmpq	%r13, %rdi
	je	.L3439
	call	_ZdlPv@PLT
.L3439:
	leaq	-544(%rbp), %rdi
	leaq	48(%rbx), %rsi
.LEHB417:
	call	_ZN2v88internal6torque14CamelifyStringERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
.LEHE417:
	movq	120(%r14), %rax
	movq	%r12, %rdi
	movq	%r13, -208(%rbp)
	movq	$0, -200(%rbp)
	leaq	4(%rax), %rsi
	movb	$0, -192(%rbp)
.LEHB418:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7reserveEm@PLT
	movabsq	$4611686018427387903, %rax
	subq	-200(%rbp), %rax
	cmpq	$3, %rax
	jbe	.L3585
	movl	$4, %edx
	leaq	.LC114(%rip), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	120(%r14), %rdx
	movq	112(%r14), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.LEHE418:
	movq	-536(%rbp), %rdx
	movq	-544(%rbp), %rsi
	movq	%r12, %rdi
.LEHB419:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.LEHE419:
	leaq	-496(%rbp), %rcx
	leaq	16(%rax), %rdx
	movq	%rcx, -648(%rbp)
	movq	%rcx, -512(%rbp)
	movq	(%rax), %rcx
	cmpq	%rdx, %rcx
	je	.L3586
	movq	%rcx, -512(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -496(%rbp)
.L3446:
	movq	8(%rax), %rcx
	movb	$0, 16(%rax)
	movq	%rcx, -504(%rbp)
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movq	-208(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L3447
	call	_ZdlPv@PLT
.L3447:
	pxor	%xmm0, %xmm0
	leaq	-352(%rbp), %rax
	leaq	.LC115(%rip), %rdi
	movq	$0, -336(%rbp)
	movb	$0, -328(%rbp)
	movb	$0, -320(%rbp)
	movq	$0, -272(%rbp)
	movq	$0, -256(%rbp)
	movq	$0, -224(%rbp)
	movb	$0, -216(%rbp)
	movq	%rax, -632(%rbp)
	movaps	%xmm0, -352(%rbp)
	movaps	%xmm0, -288(%rbp)
	movaps	%xmm0, -240(%rbp)
.LEHB420:
	call	_ZN2v88internal6torque8MakeNodeINS1_10IdentifierEJPKcEEEPT_DpT0_
	movq	%rax, -608(%rbp)
	movq	-344(%rbp), %rsi
	cmpq	-336(%rbp), %rsi
	je	.L3448
	movq	%rax, (%rsi)
	leaq	-608(%rbp), %rax
	movq	%rax, -616(%rbp)
	leaq	-352(%rbp), %rax
	addq	$8, -344(%rbp)
	movq	%rax, -632(%rbp)
.L3449:
	movq	-616(%rbp), %rsi
	leaq	-288(%rbp), %rdi
	movq	%r14, -608(%rbp)
	call	_ZNSt6vectorIPKN2v88internal6torque4TypeESaIS5_EE12emplace_backIJS5_EEEvDpOT_
	movb	$0, -264(%rbp)
	movq	80(%rbx), %rax
	movq	%r12, %rdi
	movq	%r13, -208(%rbp)
	movq	%rax, -248(%rbp)
	movq	48(%rbx), %rsi
	movq	56(%rbx), %rdx
	addq	%rsi, %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag.constprop.0
.LEHE420:
	movq	%r12, %rdi
.LEHB421:
	call	_ZN2v88internal6torque8MakeNodeINS1_10IdentifierEJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEPT_DpT0_
	movq	-640(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal6torque8MakeNodeINS1_21FieldAccessExpressionEJPNS1_20IdentifierExpressionEPNS1_10IdentifierEEEEPT_DpT0_
	movq	%rax, %rdi
	call	_ZN2v88internal6torque8MakeNodeINS1_15ReturnStatementEJPNS1_21FieldAccessExpressionEEEEPT_DpT0_
.LEHE421:
	movq	-208(%rbp), %rdi
	movq	%rax, %r9
	cmpq	%r13, %rdi
	je	.L3450
	movq	%rax, -656(%rbp)
	call	_ZdlPv@PLT
	movq	-656(%rbp), %r9
.L3450:
	pushq	$1
	leaq	-400(%rbp), %r15
	movq	-632(%rbp), %rcx
	leaq	-512(%rbp), %rdi
	pushq	%r12
	movl	$1, %r8d
	movq	%r15, %rdx
	movl	$1, %esi
	movb	$0, -208(%rbp)
	movb	$0, -200(%rbp)
	movb	$0, -400(%rbp)
	movb	$0, -392(%rbp)
.LEHB422:
	.cfi_escape 0x2e,0x10
	call	_ZN2v88internal6torque12Declarations12DeclareMacroERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEbNS_4base8OptionalIS8_EERKNS1_9SignatureENSC_IPNS1_9StatementEEESD_b@PLT
.LEHE422:
	cmpb	$0, -400(%rbp)
	popq	%rsi
	popq	%rdi
	je	.L3451
	movq	-392(%rbp), %rdi
	leaq	-376(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3451
	call	_ZdlPv@PLT
.L3451:
	cmpb	$0, -208(%rbp)
	je	.L3452
	movq	-200(%rbp), %rdi
	leaq	-184(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3452
	call	_ZdlPv@PLT
.L3452:
	movl	$118, %ecx
	movq	%r12, %rdi
	movq	%r13, -208(%rbp)
	movq	$1, -200(%rbp)
	movw	%cx, -192(%rbp)
.LEHB423:
	.cfi_escape 0x2e,0
	call	_ZN2v88internal6torque8MakeNodeINS1_10IdentifierEJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEPT_DpT0_
.LEHE423:
	movq	-616(%rbp), %rdi
	pxor	%xmm0, %xmm0
	movq	%rax, %rsi
	movq	$0, -592(%rbp)
	movaps	%xmm0, -608(%rbp)
.LEHB424:
	call	_ZN2v88internal6torque8MakeNodeINS1_20IdentifierExpressionEJSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaISA_EEPNS1_10IdentifierEEEEPT_DpT0_
.LEHE424:
	movq	%rax, -664(%rbp)
	movq	-608(%rbp), %r8
	movq	-600(%rbp), %rax
	cmpq	%r8, %rax
	je	.L3453
	movq	%r13, -656(%rbp)
	movq	%rbx, %r13
	movq	%r8, %rbx
	movq	%r15, -672(%rbp)
	movq	%r12, %r15
	movq	%rax, %r12
	.p2align 4,,10
	.p2align 3
.L3457:
	movq	(%rbx), %rdi
	leaq	16(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L3454
	call	_ZdlPv@PLT
	addq	$32, %rbx
	cmpq	%r12, %rbx
	jne	.L3457
.L3573:
	movq	%r13, %rbx
	movq	%r15, %r12
	movq	-656(%rbp), %r13
	movq	-672(%rbp), %r15
	movq	-608(%rbp), %r8
.L3453:
	testq	%r8, %r8
	je	.L3458
	movq	%r8, %rdi
	call	_ZdlPv@PLT
.L3458:
	movq	-208(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L3459
	call	_ZdlPv@PLT
.L3459:
	movq	120(%r14), %rax
	movq	%r12, %rdi
	movq	%r13, -208(%rbp)
	movq	$0, -200(%rbp)
	leaq	5(%rax), %rsi
	movb	$0, -192(%rbp)
.LEHB425:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7reserveEm@PLT
	movabsq	$4611686018427387903, %rax
	subq	-200(%rbp), %rax
	cmpq	$4, %rax
	jbe	.L3587
	movl	$5, %edx
	leaq	.LC116(%rip), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	120(%r14), %rdx
	movq	112(%r14), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.LEHE425:
	movq	-536(%rbp), %rdx
	movq	-544(%rbp), %rsi
	movq	%r12, %rdi
.LEHB426:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.LEHE426:
	leaq	-464(%rbp), %rcx
	leaq	16(%rax), %rdx
	movq	%rcx, -656(%rbp)
	movq	%rcx, -480(%rbp)
	movq	(%rax), %rcx
	cmpq	%rdx, %rcx
	je	.L3588
	movq	%rcx, -480(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -464(%rbp)
.L3466:
	movq	8(%rax), %rcx
	movb	$0, 16(%rax)
	movq	%rcx, -472(%rbp)
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movq	-208(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L3467
	call	_ZdlPv@PLT
.L3467:
	pxor	%xmm0, %xmm0
	leaq	.LC115(%rip), %rdi
	movq	$0, -192(%rbp)
	movb	$0, -184(%rbp)
	movb	$0, -176(%rbp)
	movq	$0, -128(%rbp)
	movq	$0, -112(%rbp)
	movq	$0, -80(%rbp)
	movb	$0, -72(%rbp)
	movaps	%xmm0, -208(%rbp)
	movaps	%xmm0, -144(%rbp)
	movaps	%xmm0, -96(%rbp)
.LEHB427:
	call	_ZN2v88internal6torque8MakeNodeINS1_10IdentifierEJPKcEEEPT_DpT0_
	movq	%rax, -608(%rbp)
	movq	-200(%rbp), %rsi
	cmpq	-192(%rbp), %rsi
	je	.L3468
	movq	%rax, (%rsi)
	addq	$8, -200(%rbp)
.L3469:
	leaq	.LC117(%rip), %rdi
	call	_ZN2v88internal6torque8MakeNodeINS1_10IdentifierEJPKcEEEPT_DpT0_
	movq	%rax, -608(%rbp)
	movq	-200(%rbp), %rsi
	cmpq	-192(%rbp), %rsi
	je	.L3470
	movq	%rax, (%rsi)
	addq	$8, -200(%rbp)
.L3471:
	leaq	-144(%rbp), %r13
	movq	-616(%rbp), %rsi
	movq	%r14, -608(%rbp)
	movq	%r13, %rdi
	call	_ZNSt6vectorIPKN2v88internal6torque4TypeESaIS5_EE12emplace_backIJS5_EEEvDpOT_
	movq	-136(%rbp), %rsi
	cmpq	-128(%rbp), %rsi
	je	.L3472
	movq	80(%rbx), %rax
	movq	%rax, (%rsi)
	addq	$8, -136(%rbp)
.L3473:
	movb	$0, -120(%rbp)
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_10TypeOracleEEERPNT_5ScopeEv@PLT
.LEHE427:
	leaq	-384(%rbp), %r13
	movq	%r15, %rdi
	movl	$1684631414, -384(%rbp)
	movq	%r13, -400(%rbp)
	movq	$4, -392(%rbp)
	movb	$0, -380(%rbp)
.LEHB428:
	call	_ZN2v88internal6torque12Declarations16LookupGlobalTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
.LEHE428:
	movq	-400(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L3474
	movq	%rax, -616(%rbp)
	call	_ZdlPv@PLT
	movq	-616(%rbp), %rax
.L3474:
	movq	%rax, -104(%rbp)
	movq	%r15, %rdi
	movq	%r13, -400(%rbp)
	movq	48(%rbx), %rsi
	movq	56(%rbx), %rdx
	addq	%rsi, %rdx
.LEHB429:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag.constprop.0
.LEHE429:
	movq	%r15, %rdi
.LEHB430:
	call	_ZN2v88internal6torque8MakeNodeINS1_10IdentifierEJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEPT_DpT0_
	movq	-640(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal6torque8MakeNodeINS1_21FieldAccessExpressionEJPNS1_20IdentifierExpressionEPNS1_10IdentifierEEEEPT_DpT0_
	movq	-664(%rbp), %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal6torque8MakeNodeINS1_20AssignmentExpressionEJPNS1_21FieldAccessExpressionEPNS1_20IdentifierExpressionEEEEPT_DpT0_
	movq	%rax, %rdi
	call	_ZN2v88internal6torque8MakeNodeINS1_19ExpressionStatementEJPNS1_20AssignmentExpressionEEEEPT_DpT0_
.LEHE430:
	movq	-400(%rbp), %rdi
	movq	%rax, %r9
	cmpq	%r13, %rdi
	je	.L3479
	movq	%rax, -616(%rbp)
	call	_ZdlPv@PLT
	movq	-616(%rbp), %r9
.L3479:
	pushq	$0
	movl	$1, %r8d
	movq	%r12, %rcx
	movl	$1, %esi
	pushq	%r15
	leaq	-448(%rbp), %rdx
	leaq	-480(%rbp), %rdi
	movb	$0, -400(%rbp)
	movb	$0, -392(%rbp)
	movb	$0, -448(%rbp)
	movb	$0, -440(%rbp)
.LEHB431:
	.cfi_escape 0x2e,0x10
	call	_ZN2v88internal6torque12Declarations12DeclareMacroERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEbNS_4base8OptionalIS8_EERKNS1_9SignatureENSC_IPNS1_9StatementEEESD_b@PLT
.LEHE431:
	cmpb	$0, -448(%rbp)
	popq	%rax
	popq	%rdx
	je	.L3480
	movq	-440(%rbp), %rdi
	leaq	-424(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3480
	call	_ZdlPv@PLT
.L3480:
	cmpb	$0, -400(%rbp)
	je	.L3481
	movq	-392(%rbp), %rdi
	leaq	-376(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3481
	call	_ZdlPv@PLT
.L3481:
	movq	-88(%rbp), %r13
	movq	-96(%rbp), %r12
	cmpq	%r12, %r13
	je	.L3482
	.p2align 4,,10
	.p2align 3
.L3486:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L3483
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r12, %r13
	jne	.L3486
.L3484:
	movq	-96(%rbp), %r12
.L3482:
	testq	%r12, %r12
	je	.L3487
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L3487:
	movq	-144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3488
	call	_ZdlPv@PLT
.L3488:
	cmpb	$0, -184(%rbp)
	je	.L3489
	movq	-176(%rbp), %rdi
	leaq	-160(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3489
	call	_ZdlPv@PLT
.L3489:
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3490
	call	_ZdlPv@PLT
.L3490:
	movq	-480(%rbp), %rdi
	cmpq	-656(%rbp), %rdi
	je	.L3491
	call	_ZdlPv@PLT
.L3491:
	movq	-232(%rbp), %r13
	movq	-240(%rbp), %r12
	cmpq	%r12, %r13
	je	.L3492
	.p2align 4,,10
	.p2align 3
.L3496:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L3493
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r13, %r12
	jne	.L3496
.L3494:
	movq	-240(%rbp), %r12
.L3492:
	testq	%r12, %r12
	je	.L3497
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L3497:
	movq	-288(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3498
	call	_ZdlPv@PLT
.L3498:
	cmpb	$0, -328(%rbp)
	je	.L3499
	movq	-320(%rbp), %rdi
	leaq	-304(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3499
	call	_ZdlPv@PLT
.L3499:
	movq	-352(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3500
	call	_ZdlPv@PLT
.L3500:
	movq	-512(%rbp), %rdi
	cmpq	-648(%rbp), %rdi
	je	.L3501
	call	_ZdlPv@PLT
.L3501:
	movq	-544(%rbp), %rdi
	leaq	-528(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3502
	call	_ZdlPv@PLT
.L3502:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
	movq	-552(%rbp), %rdx
	movq	%rdx, (%rax)
.L3438:
	addq	$104, %rbx
	cmpq	%rbx, -624(%rbp)
	jne	.L3503
.L3431:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3589
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3493:
	.cfi_restore_state
	addq	$32, %r12
	cmpq	%r12, %r13
	jne	.L3496
	jmp	.L3494
	.p2align 4,,10
	.p2align 3
.L3483:
	addq	$32, %r12
	cmpq	%r12, %r13
	jne	.L3486
	jmp	.L3484
	.p2align 4,,10
	.p2align 3
.L3454:
	addq	$32, %rbx
	cmpq	%rbx, %r12
	jne	.L3457
	jmp	.L3573
	.p2align 4,,10
	.p2align 3
.L3588:
	movdqu	16(%rax), %xmm3
	movaps	%xmm3, -464(%rbp)
	jmp	.L3466
	.p2align 4,,10
	.p2align 3
.L3586:
	movdqu	16(%rax), %xmm2
	movaps	%xmm2, -496(%rbp)
	jmp	.L3446
	.p2align 4,,10
	.p2align 3
.L3448:
	leaq	-352(%rbp), %rax
	leaq	-608(%rbp), %rdx
	movq	%rax, %rdi
	movq	%rdx, -616(%rbp)
	movq	%rax, -632(%rbp)
.LEHB432:
	.cfi_escape 0x2e,0
	call	_ZNSt6vectorIPN2v88internal6torque10IdentifierESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
.LEHE432:
	jmp	.L3449
	.p2align 4,,10
	.p2align 3
.L3472:
	leaq	80(%rbx), %rdx
	movq	%r13, %rdi
.LEHB433:
	call	_ZNSt6vectorIPKN2v88internal6torque4TypeESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	jmp	.L3473
	.p2align 4,,10
	.p2align 3
.L3470:
	movq	-616(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorIPN2v88internal6torque10IdentifierESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	jmp	.L3471
	.p2align 4,,10
	.p2align 3
.L3468:
	movq	-616(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorIPN2v88internal6torque10IdentifierESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
.LEHE433:
	jmp	.L3469
.L3585:
	leaq	.LC2(%rip), %rdi
.LEHB434:
	call	_ZSt20__throw_length_errorPKc@PLT
.LEHE434:
.L3587:
	leaq	.LC2(%rip), %rdi
.LEHB435:
	call	_ZSt20__throw_length_errorPKc@PLT
.LEHE435:
.L3589:
	call	__stack_chk_fail@PLT
.L3530:
	endbr64
	movq	%rax, %r12
	jmp	.L3507
.L3529:
	endbr64
	movq	%rax, %r12
	jmp	.L3505
.L3534:
	endbr64
	movq	%rax, %rbx
	jmp	.L3512
.L3533:
	endbr64
	movq	%rax, %rbx
	jmp	.L3518
.L3544:
	endbr64
	movq	%rax, %rbx
	jmp	.L3520
.L3542:
	endbr64
	movq	%rax, %rbx
	jmp	.L3508
.L3543:
	endbr64
	movq	%rax, %rbx
	jmp	.L3518
.L3541:
	endbr64
	movq	%rax, %r12
	jmp	.L3436
.L3532:
	endbr64
	movq	%rax, %rbx
	jmp	.L3464
.L3536:
	endbr64
	movq	%rax, %rbx
	jmp	.L3515
.L3535:
	endbr64
	movq	%rax, %rbx
	jmp	.L3518
.L3540:
	endbr64
	movq	%rax, %rbx
	jmp	.L3522
.L3539:
	endbr64
	movq	%rax, %rbx
	jmp	.L3520
.L3538:
	endbr64
	movq	%rax, %rbx
	jmp	.L3478
.L3537:
	endbr64
	movq	%rax, %rbx
	jmp	.L3518
.L3531:
	endbr64
	movq	%rax, %rbx
	jmp	.L3508
	.section	.gcc_except_table._ZN2v88internal6torque9ClassType17GenerateAccessorsEv,"a",@progbits
.LLSDA6601:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6601-.LLSDACSB6601
.LLSDACSB6601:
	.uleb128 .LEHB413-.LFB6601
	.uleb128 .LEHE413-.LEHB413
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB414-.LFB6601
	.uleb128 .LEHE414-.LEHB414
	.uleb128 .L3541-.LFB6601
	.uleb128 0
	.uleb128 .LEHB415-.LFB6601
	.uleb128 .LEHE415-.LEHB415
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB416-.LFB6601
	.uleb128 .LEHE416-.LEHB416
	.uleb128 .L3529-.LFB6601
	.uleb128 0
	.uleb128 .LEHB417-.LFB6601
	.uleb128 .LEHE417-.LEHB417
	.uleb128 .L3530-.LFB6601
	.uleb128 0
	.uleb128 .LEHB418-.LFB6601
	.uleb128 .LEHE418-.LEHB418
	.uleb128 .L3542-.LFB6601
	.uleb128 0
	.uleb128 .LEHB419-.LFB6601
	.uleb128 .LEHE419-.LEHB419
	.uleb128 .L3531-.LFB6601
	.uleb128 0
	.uleb128 .LEHB420-.LFB6601
	.uleb128 .LEHE420-.LEHB420
	.uleb128 .L3532-.LFB6601
	.uleb128 0
	.uleb128 .LEHB421-.LFB6601
	.uleb128 .LEHE421-.LEHB421
	.uleb128 .L3533-.LFB6601
	.uleb128 0
	.uleb128 .LEHB422-.LFB6601
	.uleb128 .LEHE422-.LEHB422
	.uleb128 .L3534-.LFB6601
	.uleb128 0
	.uleb128 .LEHB423-.LFB6601
	.uleb128 .LEHE423-.LEHB423
	.uleb128 .L3535-.LFB6601
	.uleb128 0
	.uleb128 .LEHB424-.LFB6601
	.uleb128 .LEHE424-.LEHB424
	.uleb128 .L3536-.LFB6601
	.uleb128 0
	.uleb128 .LEHB425-.LFB6601
	.uleb128 .LEHE425-.LEHB425
	.uleb128 .L3543-.LFB6601
	.uleb128 0
	.uleb128 .LEHB426-.LFB6601
	.uleb128 .LEHE426-.LEHB426
	.uleb128 .L3537-.LFB6601
	.uleb128 0
	.uleb128 .LEHB427-.LFB6601
	.uleb128 .LEHE427-.LEHB427
	.uleb128 .L3538-.LFB6601
	.uleb128 0
	.uleb128 .LEHB428-.LFB6601
	.uleb128 .LEHE428-.LEHB428
	.uleb128 .L3544-.LFB6601
	.uleb128 0
	.uleb128 .LEHB429-.LFB6601
	.uleb128 .LEHE429-.LEHB429
	.uleb128 .L3538-.LFB6601
	.uleb128 0
	.uleb128 .LEHB430-.LFB6601
	.uleb128 .LEHE430-.LEHB430
	.uleb128 .L3539-.LFB6601
	.uleb128 0
	.uleb128 .LEHB431-.LFB6601
	.uleb128 .LEHE431-.LEHB431
	.uleb128 .L3540-.LFB6601
	.uleb128 0
	.uleb128 .LEHB432-.LFB6601
	.uleb128 .LEHE432-.LEHB432
	.uleb128 .L3532-.LFB6601
	.uleb128 0
	.uleb128 .LEHB433-.LFB6601
	.uleb128 .LEHE433-.LEHB433
	.uleb128 .L3538-.LFB6601
	.uleb128 0
	.uleb128 .LEHB434-.LFB6601
	.uleb128 .LEHE434-.LEHB434
	.uleb128 .L3542-.LFB6601
	.uleb128 0
	.uleb128 .LEHB435-.LFB6601
	.uleb128 .LEHE435-.LEHB435
	.uleb128 .L3543-.LFB6601
	.uleb128 0
.LLSDACSE6601:
	.section	.text._ZN2v88internal6torque9ClassType17GenerateAccessorsEv
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque9ClassType17GenerateAccessorsEv
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6601
	.type	_ZN2v88internal6torque9ClassType17GenerateAccessorsEv.cold, @function
_ZN2v88internal6torque9ClassType17GenerateAccessorsEv.cold:
.LFSB6601:
.L3505:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-208(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L3507
	call	_ZdlPv@PLT
.L3507:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
	movq	-552(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rdx, (%rax)
.LEHB436:
	call	_Unwind_Resume@PLT
.L3512:
	cmpb	$0, -400(%rbp)
	je	.L3513
	movq	-392(%rbp), %rdi
	leaq	-376(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3513
	call	_ZdlPv@PLT
.L3513:
	cmpb	$0, -208(%rbp)
	je	.L3464
	movq	-200(%rbp), %rdi
	leaq	-184(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3464
.L3578:
	call	_ZdlPv@PLT
.L3464:
	movq	-632(%rbp), %rdi
	call	_ZN2v88internal6torque9SignatureD1Ev
	movq	-512(%rbp), %rdi
	cmpq	-648(%rbp), %rdi
	je	.L3444
.L3579:
	call	_ZdlPv@PLT
.L3444:
	movq	-544(%rbp), %rdi
	leaq	-528(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3527
	call	_ZdlPv@PLT
.L3527:
	movq	%rbx, %r12
	jmp	.L3507
.L3515:
	movq	-616(%rbp), %rdi
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EED1Ev
.L3518:
	movq	-208(%rbp), %rdi
	cmpq	%r13, %rdi
	jne	.L3578
	jmp	.L3464
.L3520:
	movq	-400(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L3478
.L3575:
	call	_ZdlPv@PLT
.L3478:
	movq	%r12, %rdi
	call	_ZN2v88internal6torque9SignatureD1Ev
	movq	-480(%rbp), %rdi
	cmpq	-656(%rbp), %rdi
	jne	.L3578
	jmp	.L3464
.L3508:
	movq	-208(%rbp), %rdi
	cmpq	%r13, %rdi
	jne	.L3579
	jmp	.L3444
.L3522:
	cmpb	$0, -448(%rbp)
	je	.L3523
	movq	-440(%rbp), %rdi
	leaq	-424(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3523
	call	_ZdlPv@PLT
.L3523:
	cmpb	$0, -400(%rbp)
	je	.L3478
	movq	-392(%rbp), %rdi
	leaq	-376(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L3575
	jmp	.L3478
.L3436:
	movq	-208(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L3437
	call	_ZdlPv@PLT
.L3437:
	movq	%r12, %rdi
	call	_Unwind_Resume@PLT
.LEHE436:
	.cfi_endproc
.LFE6601:
	.section	.gcc_except_table._ZN2v88internal6torque9ClassType17GenerateAccessorsEv
.LLSDAC6601:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6601-.LLSDACSBC6601
.LLSDACSBC6601:
	.uleb128 .LEHB436-.LCOLDB118
	.uleb128 .LEHE436-.LEHB436
	.uleb128 0
	.uleb128 0
.LLSDACSEC6601:
	.section	.text.unlikely._ZN2v88internal6torque9ClassType17GenerateAccessorsEv
	.section	.text._ZN2v88internal6torque9ClassType17GenerateAccessorsEv
	.size	_ZN2v88internal6torque9ClassType17GenerateAccessorsEv, .-_ZN2v88internal6torque9ClassType17GenerateAccessorsEv
	.section	.text.unlikely._ZN2v88internal6torque9ClassType17GenerateAccessorsEv
	.size	_ZN2v88internal6torque9ClassType17GenerateAccessorsEv.cold, .-_ZN2v88internal6torque9ClassType17GenerateAccessorsEv.cold
.LCOLDE118:
	.section	.text._ZN2v88internal6torque9ClassType17GenerateAccessorsEv
.LHOTE118:
	.section	.text._ZNSt10_HashtableIN2v88internal6torque9UnionTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_rehashEmRKm,"axG",@progbits,_ZNSt10_HashtableIN2v88internal6torque9UnionTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_rehashEmRKm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIN2v88internal6torque9UnionTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_rehashEmRKm
	.type	_ZNSt10_HashtableIN2v88internal6torque9UnionTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_rehashEmRKm, @function
_ZNSt10_HashtableIN2v88internal6torque9UnionTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_rehashEmRKm:
.LFB11729:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA11729
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	cmpq	$1, %rsi
	je	.L3612
	movabsq	$1152921504606846975, %rax
	movq	%rdx, %r13
	cmpq	%rax, %rsi
	ja	.L3613
	leaq	0(,%rsi,8), %r14
	movq	%r14, %rdi
.LEHB437:
	call	_Znwm@PLT
	movq	%r14, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	memset@PLT
	leaq	48(%rbx), %r9
.L3592:
	movq	16(%rbx), %rsi
	movq	$0, 16(%rbx)
	testq	%rsi, %rsi
	je	.L3594
	xorl	%r8d, %r8d
	leaq	16(%rbx), %r10
	jmp	.L3595
	.p2align 4,,10
	.p2align 3
.L3596:
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	(%rdi), %rax
	movq	%rcx, (%rax)
.L3597:
	testq	%rsi, %rsi
	je	.L3594
.L3595:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	128(%rcx), %rax
	divq	%r12
	leaq	0(%r13,%rdx,8), %rdi
	movq	(%rdi), %rax
	testq	%rax, %rax
	jne	.L3596
	movq	16(%rbx), %rax
	movq	%rax, (%rcx)
	movq	%rcx, 16(%rbx)
	movq	%r10, (%rdi)
	cmpq	$0, (%rcx)
	je	.L3601
	movq	%rcx, 0(%r13,%r8,8)
	movq	%rdx, %r8
	testq	%rsi, %rsi
	jne	.L3595
	.p2align 4,,10
	.p2align 3
.L3594:
	movq	(%rbx), %rdi
	cmpq	%r9, %rdi
	je	.L3598
	call	_ZdlPv@PLT
.L3598:
	movq	%r12, 8(%rbx)
	movq	%r13, (%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3601:
	.cfi_restore_state
	movq	%rdx, %r8
	jmp	.L3597
	.p2align 4,,10
	.p2align 3
.L3612:
	leaq	48(%rdi), %r13
	movq	$0, 48(%rdi)
	movq	%r13, %r9
	jmp	.L3592
.L3613:
	call	_ZSt17__throw_bad_allocv@PLT
.LEHE437:
.L3602:
	endbr64
	movq	%rax, %rdi
.L3599:
	call	__cxa_begin_catch@PLT
	movq	0(%r13), %rax
	movq	%rax, 40(%rbx)
.LEHB438:
	call	__cxa_rethrow@PLT
.LEHE438:
.L3603:
	endbr64
	movq	%rax, %r12
.L3600:
	call	__cxa_end_catch@PLT
	movq	%r12, %rdi
.LEHB439:
	call	_Unwind_Resume@PLT
.LEHE439:
	.cfi_endproc
.LFE11729:
	.section	.gcc_except_table._ZNSt10_HashtableIN2v88internal6torque9UnionTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_rehashEmRKm,"aG",@progbits,_ZNSt10_HashtableIN2v88internal6torque9UnionTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_rehashEmRKm,comdat
	.align 4
.LLSDA11729:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATT11729-.LLSDATTD11729
.LLSDATTD11729:
	.byte	0x1
	.uleb128 .LLSDACSE11729-.LLSDACSB11729
.LLSDACSB11729:
	.uleb128 .LEHB437-.LFB11729
	.uleb128 .LEHE437-.LEHB437
	.uleb128 .L3602-.LFB11729
	.uleb128 0x1
	.uleb128 .LEHB438-.LFB11729
	.uleb128 .LEHE438-.LEHB438
	.uleb128 .L3603-.LFB11729
	.uleb128 0
	.uleb128 .LEHB439-.LFB11729
	.uleb128 .LEHE439-.LEHB439
	.uleb128 0
	.uleb128 0
.LLSDACSE11729:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATT11729:
	.section	.text._ZNSt10_HashtableIN2v88internal6torque9UnionTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_rehashEmRKm,"axG",@progbits,_ZNSt10_HashtableIN2v88internal6torque9UnionTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_rehashEmRKm,comdat
	.size	_ZNSt10_HashtableIN2v88internal6torque9UnionTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_rehashEmRKm, .-_ZNSt10_HashtableIN2v88internal6torque9UnionTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_rehashEmRKm
	.section	.text._ZNSt10_HashtableIN2v88internal6torque9UnionTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb1EEEm,"axG",@progbits,_ZNSt10_HashtableIN2v88internal6torque9UnionTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb1EEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIN2v88internal6torque9UnionTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb1EEEm
	.type	_ZNSt10_HashtableIN2v88internal6torque9UnionTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb1EEEm, @function
_ZNSt10_HashtableIN2v88internal6torque9UnionTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb1EEEm:
.LFB11279:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA11279
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rcx, %r12
	movq	%r8, %rcx
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	addq	$32, %rdi
	subq	$16, %rsp
	movq	-8(%rdi), %rdx
	movq	-24(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	%rax, -48(%rbp)
.LEHB440:
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
.LEHE440:
	testb	%al, %al
	je	.L3615
	movq	%rdx, %rsi
	movq	%rbx, %rdi
	leaq	-48(%rbp), %rdx
.LEHB441:
	call	_ZNSt10_HashtableIN2v88internal6torque9UnionTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_rehashEmRKm
.LEHE441:
	movq	%r14, %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	%rdx, %r13
.L3615:
	movq	%r14, 128(%r12)
	movq	(%rbx), %rax
	leaq	0(,%r13,8), %rcx
	movq	(%rax,%r13,8), %rax
	testq	%rax, %rax
	je	.L3616
	movq	(%rax), %rax
	movq	%rax, (%r12)
	movq	(%rbx), %rax
	movq	(%rax,%r13,8), %rax
	movq	%r12, (%rax)
.L3617:
	addq	$1, 24(%rbx)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3629
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3616:
	.cfi_restore_state
	movq	16(%rbx), %rax
	movq	%rax, (%r12)
	movq	%r12, 16(%rbx)
	movq	(%r12), %rax
	testq	%rax, %rax
	je	.L3618
	movq	128(%rax), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	(%rbx), %rax
	movq	%r12, (%rax,%rdx,8)
.L3618:
	movq	(%rbx), %rax
	leaq	16(%rbx), %rdx
	movq	%rdx, (%rax,%rcx)
	jmp	.L3617
.L3629:
	call	__stack_chk_fail@PLT
.L3622:
	endbr64
	movq	%rax, %rdi
.L3619:
	call	__cxa_begin_catch@PLT
	leaq	8(%r12), %rdi
	call	_ZN2v88internal6torque9UnionTypeD1Ev
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.LEHB442:
	call	__cxa_rethrow@PLT
.LEHE442:
.L3623:
	endbr64
	movq	%rax, %r12
.L3620:
	call	__cxa_end_catch@PLT
	movq	%r12, %rdi
.LEHB443:
	call	_Unwind_Resume@PLT
.LEHE443:
	.cfi_endproc
.LFE11279:
	.section	.gcc_except_table._ZNSt10_HashtableIN2v88internal6torque9UnionTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb1EEEm,"aG",@progbits,_ZNSt10_HashtableIN2v88internal6torque9UnionTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb1EEEm,comdat
	.align 4
.LLSDA11279:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATT11279-.LLSDATTD11279
.LLSDATTD11279:
	.byte	0x1
	.uleb128 .LLSDACSE11279-.LLSDACSB11279
.LLSDACSB11279:
	.uleb128 .LEHB440-.LFB11279
	.uleb128 .LEHE440-.LEHB440
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB441-.LFB11279
	.uleb128 .LEHE441-.LEHB441
	.uleb128 .L3622-.LFB11279
	.uleb128 0x1
	.uleb128 .LEHB442-.LFB11279
	.uleb128 .LEHE442-.LEHB442
	.uleb128 .L3623-.LFB11279
	.uleb128 0
	.uleb128 .LEHB443-.LFB11279
	.uleb128 .LEHE443-.LEHB443
	.uleb128 0
	.uleb128 0
.LLSDACSE11279:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATT11279:
	.section	.text._ZNSt10_HashtableIN2v88internal6torque9UnionTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb1EEEm,"axG",@progbits,_ZNSt10_HashtableIN2v88internal6torque9UnionTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb1EEEm,comdat
	.size	_ZNSt10_HashtableIN2v88internal6torque9UnionTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb1EEEm, .-_ZNSt10_HashtableIN2v88internal6torque9UnionTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb1EEEm
	.section	.text._ZNSt10_HashtableIN2v88internal6torque9UnionTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_insertIS3_NS5_10_AllocNodeISaINS5_10_Hash_nodeIS3_Lb1EEEEEEEESt4pairINS5_14_Node_iteratorIS3_Lb1ELb1EEEbEOT_RKT0_St17integral_constantIbLb1EEm,"axG",@progbits,_ZNSt10_HashtableIN2v88internal6torque9UnionTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_insertIS3_NS5_10_AllocNodeISaINS5_10_Hash_nodeIS3_Lb1EEEEEEEESt4pairINS5_14_Node_iteratorIS3_Lb1ELb1EEEbEOT_RKT0_St17integral_constantIbLb1EEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIN2v88internal6torque9UnionTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_insertIS3_NS5_10_AllocNodeISaINS5_10_Hash_nodeIS3_Lb1EEEEEEEESt4pairINS5_14_Node_iteratorIS3_Lb1ELb1EEEbEOT_RKT0_St17integral_constantIbLb1EEm
	.type	_ZNSt10_HashtableIN2v88internal6torque9UnionTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_insertIS3_NS5_10_AllocNodeISaINS5_10_Hash_nodeIS3_Lb1EEEEEEEESt4pairINS5_14_Node_iteratorIS3_Lb1ELb1EEEbEOT_RKT0_St17integral_constantIbLb1EEm, @function
_ZNSt10_HashtableIN2v88internal6torque9UnionTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_insertIS3_NS5_10_AllocNodeISaINS5_10_Hash_nodeIS3_Lb1EEEEEEEESt4pairINS5_14_Node_iteratorIS3_Lb1ELb1EEEbEOT_RKT0_St17integral_constantIbLb1EEm:
.LFB10599:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	80(%rsi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	xorl	%r13d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$56, %rsp
	movq	96(%rsi), %r14
	movq	%rcx, -88(%rbp)
	movq	%rax, -56(%rbp)
	cmpq	%rax, %r14
	je	.L3631
	.p2align 4,,10
	.p2align 3
.L3632:
	movq	32(%r14), %r15
	movq	%r13, %rdi
	call	_ZN2v84base10hash_valueEm@PLT
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZN2v84base10hash_valueEm@PLT
	xorl	%edi, %edi
	movq	%rax, %rsi
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	%r14, %rdi
	movq	%rax, %r13
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r14
	cmpq	%rax, -56(%rbp)
	jne	.L3632
.L3631:
	movq	8(%r12), %rsi
	movq	%r13, %rax
	xorl	%edx, %edx
	divq	%rsi
	movq	(%r12), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r14
	testq	%rax, %rax
	je	.L3633
	movq	(%rax), %r15
	movq	128(%r15), %rdi
	jmp	.L3638
	.p2align 4,,10
	.p2align 3
.L3634:
	movq	(%r15), %r15
	testq	%r15, %r15
	je	.L3633
	movq	128(%r15), %rdi
	xorl	%edx, %edx
	movq	%rdi, %rax
	divq	%rsi
	cmpq	%rdx, %r14
	jne	.L3633
.L3638:
	cmpq	%rdi, %r13
	jne	.L3634
	movq	120(%r15), %rax
	cmpq	%rax, 112(%rbx)
	jne	.L3634
	movq	104(%r15), %r10
	movq	96(%rbx), %r8
	cmpq	%r8, -56(%rbp)
	je	.L3635
	.p2align 4,,10
	.p2align 3
.L3637:
	movq	%r10, -80(%rbp)
	movq	32(%r10), %rax
	cmpq	%rax, 32(%r8)
	jne	.L3634
	movq	%r8, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	-80(%rbp), %r10
	movq	%rax, -64(%rbp)
	movq	%r10, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	-64(%rbp), %r8
	cmpq	%r8, -56(%rbp)
	movq	-72(%rbp), %rsi
	movq	%rax, %r10
	jne	.L3637
.L3635:
	addq	$56, %rsp
	movq	%r15, %rax
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3633:
	.cfi_restore_state
	movl	$136, %edi
	call	_Znwm@PLT
	movq	$0, (%rax)
	movq	%rax, %rcx
	movl	8(%rbx), %eax
	movl	%eax, 16(%rcx)
	leaq	16+_ZTVN2v88internal6torque4TypeE(%rip), %rax
	movq	%rax, 8(%rcx)
	movq	16(%rbx), %rax
	movq	%rax, 24(%rcx)
	movq	40(%rbx), %rax
	testq	%rax, %rax
	je	.L3639
	movl	32(%rbx), %edx
	movq	%rax, 48(%rcx)
	movl	%edx, 40(%rcx)
	movq	48(%rbx), %rdx
	movq	%rdx, 56(%rcx)
	movq	56(%rbx), %rdx
	movq	%rdx, 64(%rcx)
	leaq	40(%rcx), %rdx
	movq	%rdx, 8(%rax)
	movq	64(%rbx), %rax
	movq	$0, 40(%rbx)
	movq	%rax, 72(%rcx)
	leaq	32(%rbx), %rax
	movq	%rax, 48(%rbx)
	movq	%rax, 56(%rbx)
	movq	$0, 64(%rbx)
.L3640:
	leaq	16+_ZTVN2v88internal6torque9UnionTypeE(%rip), %rax
	leaq	88(%rcx), %rdx
	movq	%rax, 8(%rcx)
	movq	88(%rbx), %rax
	testq	%rax, %rax
	je	.L3641
	movl	80(%rbx), %esi
	movq	%rax, 96(%rcx)
	movl	%esi, 88(%rcx)
	movq	96(%rbx), %rsi
	movq	%rsi, 104(%rcx)
	movq	104(%rbx), %rsi
	movq	%rsi, 112(%rcx)
	movq	%rdx, 8(%rax)
	movq	112(%rbx), %rax
	movq	$0, 88(%rbx)
	movq	%rax, 120(%rcx)
	movq	-56(%rbp), %rax
	movq	$0, 112(%rbx)
	movq	%rax, 96(%rbx)
	movq	%rax, 104(%rbx)
.L3642:
	movq	-88(%rbp), %r8
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZNSt10_HashtableIN2v88internal6torque9UnionTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb1EEEm
	addq	$56, %rsp
	movl	$1, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3641:
	.cfi_restore_state
	movl	$0, 88(%rcx)
	movq	$0, 96(%rcx)
	movq	%rdx, 104(%rcx)
	movq	%rdx, 112(%rcx)
	movq	$0, 120(%rcx)
	jmp	.L3642
	.p2align 4,,10
	.p2align 3
.L3639:
	leaq	40(%rcx), %rax
	movl	$0, 40(%rcx)
	movq	$0, 48(%rcx)
	movq	%rax, 56(%rcx)
	movq	%rax, 64(%rcx)
	movq	$0, 72(%rcx)
	jmp	.L3640
	.cfi_endproc
.LFE10599:
	.size	_ZNSt10_HashtableIN2v88internal6torque9UnionTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_insertIS3_NS5_10_AllocNodeISaINS5_10_Hash_nodeIS3_Lb1EEEEEEEESt4pairINS5_14_Node_iteratorIS3_Lb1ELb1EEEbEOT_RKT0_St17integral_constantIbLb1EEm, .-_ZNSt10_HashtableIN2v88internal6torque9UnionTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_insertIS3_NS5_10_AllocNodeISaINS5_10_Hash_nodeIS3_Lb1EEEEEEEESt4pairINS5_14_Node_iteratorIS3_Lb1ELb1EEEbEOT_RKT0_St17integral_constantIbLb1EEm
	.section	.text.unlikely._ZN2v88internal6torque12SubtractTypeEPKNS1_4TypeES4_,"ax",@progbits
.LCOLDB119:
	.section	.text._ZN2v88internal6torque12SubtractTypeEPKNS1_4TypeES4_,"ax",@progbits
.LHOTB119:
	.p2align 4
	.globl	_ZN2v88internal6torque12SubtractTypeEPKNS1_4TypeES4_
	.type	_ZN2v88internal6torque12SubtractTypeEPKNS1_4TypeES4_, @function
_ZN2v88internal6torque12SubtractTypeEPKNS1_4TypeES4_:
.LFB6525:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6525
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$440, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L3657
	cmpl	$3, 8(%rdi)
	jne	.L3657
	leaq	-416(%rbp), %rdx
	movl	$3, -440(%rbp)
	movq	16(%rdi), %rax
	leaq	16+_ZTVN2v88internal6torque4TypeE(%rip), %r13
	movq	40(%rdi), %rsi
	movq	%r13, -448(%rbp)
	movq	%rax, -432(%rbp)
	movl	$0, -416(%rbp)
	movq	$0, -408(%rbp)
	movq	%rdx, -400(%rbp)
	movq	%rdx, -392(%rbp)
	movq	$0, -384(%rbp)
	testq	%rsi, %rsi
	je	.L3658
	leaq	-424(%rbp), %r14
	leaq	-192(%rbp), %rcx
	movq	%r14, %rdi
	movq	%r14, -192(%rbp)
.LEHB444:
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE7_M_copyINSB_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS5_EPKSF_PSt18_Rb_tree_node_baseRT_
.LEHE444:
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L3659:
	movq	%rdx, %rcx
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L3659
	movq	%rcx, -400(%rbp)
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L3660:
	movq	%rdx, %rcx
	movq	24(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L3660
	movq	64(%r15), %rdx
	movq	%rcx, -392(%rbp)
	movq	%rax, -408(%rbp)
	movq	%rdx, -384(%rbp)
.L3658:
	movq	88(%r15), %rsi
	leaq	-368(%rbp), %rdx
	leaq	16+_ZTVN2v88internal6torque9UnionTypeE(%rip), %rbx
	movl	$0, -368(%rbp)
	movq	%rbx, -448(%rbp)
	movq	$0, -360(%rbp)
	movq	%rdx, -352(%rbp)
	movq	%rdx, -344(%rbp)
	movq	$0, -336(%rbp)
	testq	%rsi, %rsi
	je	.L3661
	leaq	-376(%rbp), %r14
	leaq	-192(%rbp), %rcx
	movq	%r14, %rdi
	movq	%r14, -192(%rbp)
.LEHB445:
	call	_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE7_M_copyINSA_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS5_EPKSE_PSt18_Rb_tree_node_baseRT_
.LEHE445:
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L3662:
	movq	%rdx, %rcx
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L3662
	movq	%rcx, -352(%rbp)
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L3663:
	movq	%rdx, %rcx
	movq	24(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L3663
	movq	112(%r15), %rdx
	movq	%rcx, -344(%rbp)
	movq	%rax, -360(%rbp)
	movq	%rdx, -336(%rbp)
	.p2align 4,,10
	.p2align 3
.L3661:
	leaq	-448(%rbp), %rax
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -472(%rbp)
.LEHB446:
	call	_ZN2v88internal6torque9UnionType8SubtractEPKNS1_4TypeE
	movl	-440(%rbp), %eax
	movq	-408(%rbp), %rsi
	leaq	-288(%rbp), %r15
	leaq	16+_ZTVN2v88internal6torque4TypeE(%rip), %r13
	movq	%r13, -320(%rbp)
	movl	%eax, -312(%rbp)
	movq	-432(%rbp), %rax
	movl	$0, -288(%rbp)
	movq	%rax, -304(%rbp)
	movq	$0, -280(%rbp)
	movq	%r15, -272(%rbp)
	movq	%r15, -264(%rbp)
	movq	$0, -256(%rbp)
	testq	%rsi, %rsi
	je	.L3668
	leaq	-296(%rbp), %r12
	leaq	-192(%rbp), %rcx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%r12, -192(%rbp)
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE7_M_copyINSB_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS5_EPKSF_PSt18_Rb_tree_node_baseRT_
.LEHE446:
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L3669:
	movq	%rdx, %rcx
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L3669
	movq	%rcx, -272(%rbp)
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L3670:
	movq	%rdx, %rcx
	movq	24(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L3670
	movq	-384(%rbp), %rdx
	movq	%rcx, -264(%rbp)
	movq	%rax, -280(%rbp)
	movq	%rdx, -256(%rbp)
.L3668:
	movq	-360(%rbp), %rsi
	leaq	-240(%rbp), %r12
	movq	%rbx, -320(%rbp)
	movl	$0, -240(%rbp)
	movq	$0, -232(%rbp)
	movq	%r12, -224(%rbp)
	movq	%r12, -216(%rbp)
	movq	$0, -208(%rbp)
	testq	%rsi, %rsi
	je	.L3671
	leaq	-248(%rbp), %r8
	leaq	-192(%rbp), %rcx
	movq	%r12, %rdx
	movq	%r8, %rdi
	movq	%r8, -192(%rbp)
.LEHB447:
	call	_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE7_M_copyINSA_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS5_EPKSE_PSt18_Rb_tree_node_baseRT_
.LEHE447:
	movq	%rax, %r14
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L3672:
	movq	%rdx, %rcx
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L3672
	movq	%rcx, -224(%rbp)
	movq	%r14, %rdx
	.p2align 4,,10
	.p2align 3
.L3673:
	movq	%rdx, %rsi
	movq	24(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L3673
	movq	-336(%rbp), %rdx
	movq	%rsi, -216(%rbp)
	movq	%r14, -232(%rbp)
	movq	%rdx, -208(%rbp)
	cmpq	$1, %rdx
	jne	.L3671
	movq	32(%rcx), %rax
	movq	%rax, -480(%rbp)
.L3677:
	movq	%rbx, -320(%rbp)
	leaq	-248(%rbp), %r8
	testq	%r14, %r14
	je	.L3693
.L3688:
	movq	24(%r14), %rsi
	movq	%r8, %rdi
	movq	%r8, -472(%rbp)
	call	_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	%r14, %rdi
	movq	16(%r14), %r14
	call	_ZdlPv@PLT
	movq	-472(%rbp), %r8
	testq	%r14, %r14
	jne	.L3688
.L3693:
	movq	-280(%rbp), %r15
	movq	%r13, -320(%rbp)
	leaq	-296(%rbp), %r12
	testq	%r15, %r15
	je	.L3689
.L3690:
	movq	24(%r15), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r15), %rdi
	leaq	48(%r15), %rax
	movq	16(%r15), %r14
	cmpq	%rax, %rdi
	je	.L3696
	call	_ZdlPv@PLT
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	testq	%r14, %r14
	je	.L3689
.L3697:
	movq	%r14, %r15
	jmp	.L3690
	.p2align 4,,10
	.p2align 3
.L3671:
.LEHB448:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_10TypeOracleEEERPNT_5ScopeEv@PLT
.LEHE448:
	movq	(%rax), %rdi
	movl	-312(%rbp), %eax
	movl	%eax, -184(%rbp)
	movq	-304(%rbp), %rax
	movq	%rax, -176(%rbp)
	movq	-280(%rbp), %rax
	testq	%rax, %rax
	je	.L3678
	movl	-288(%rbp), %edx
	movq	%rax, -152(%rbp)
	movl	%edx, -160(%rbp)
	movq	-272(%rbp), %rdx
	movq	%rdx, -144(%rbp)
	movq	-264(%rbp), %rdx
	movq	%rdx, -136(%rbp)
	leaq	-160(%rbp), %rdx
	movq	%rdx, 8(%rax)
	movq	-256(%rbp), %rax
	movq	$0, -280(%rbp)
	movq	%rax, -128(%rbp)
	movq	%r15, -272(%rbp)
	movq	%r15, -264(%rbp)
	movq	$0, -256(%rbp)
.L3679:
	movq	-232(%rbp), %rax
	movq	%rbx, -192(%rbp)
	testq	%rax, %rax
	je	.L3680
	movl	-240(%rbp), %edx
	movq	%rax, -104(%rbp)
	movl	%edx, -112(%rbp)
	movq	-224(%rbp), %rdx
	movq	%rdx, -96(%rbp)
	movq	-216(%rbp), %rdx
	movq	%rdx, -88(%rbp)
	leaq	-112(%rbp), %rdx
	movq	%rdx, 8(%rax)
	movq	-208(%rbp), %rax
	movq	$0, -232(%rbp)
	movq	%rax, -80(%rbp)
	movq	%r12, -224(%rbp)
	movq	%r12, -216(%rbp)
	movq	$0, -208(%rbp)
.L3681:
	leaq	-192(%rbp), %r12
	addq	$80, %rdi
	movl	$1, %ecx
	leaq	-456(%rbp), %rdx
	movq	%r12, %rsi
	movq	%rdi, -456(%rbp)
.LEHB449:
	call	_ZNSt10_HashtableIN2v88internal6torque9UnionTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_insertIS3_NS5_10_AllocNodeISaINS5_10_Hash_nodeIS3_Lb1EEEEEEEESt4pairINS5_14_Node_iteratorIS3_Lb1ELb1EEEbEOT_RKT0_St17integral_constantIbLb1EEm
.LEHE449:
	movq	-104(%rbp), %r12
	addq	$8, %rax
	movq	%rbx, -192(%rbp)
	leaq	-120(%rbp), %r14
	movq	%rax, -480(%rbp)
	testq	%r12, %r12
	je	.L3685
.L3682:
	movq	24(%r12), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	%r12, %rdi
	movq	16(%r12), %r12
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L3682
.L3685:
	movq	-152(%rbp), %r12
	movq	%r13, -192(%rbp)
	leaq	-168(%rbp), %r14
	testq	%r12, %r12
	je	.L3683
.L3684:
	movq	24(%r12), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r12), %rdi
	leaq	48(%r12), %rdx
	movq	16(%r12), %r15
	cmpq	%rdx, %rdi
	je	.L3686
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r15, %r15
	je	.L3683
.L3687:
	movq	%r15, %r12
	jmp	.L3684
	.p2align 4,,10
	.p2align 3
.L3696:
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	testq	%r14, %r14
	jne	.L3697
.L3689:
	movq	%rbx, -448(%rbp)
	movq	-360(%rbp), %rbx
	leaq	-376(%rbp), %r14
	testq	%rbx, %rbx
	je	.L3694
.L3695:
	movq	24(%rbx), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	%rbx, %rdi
	movq	16(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L3695
.L3694:
	movq	-408(%rbp), %r12
	movq	%r13, -448(%rbp)
	leaq	-424(%rbp), %r14
	testq	%r12, %r12
	je	.L3656
.L3699:
	movq	24(%r12), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r12), %rdi
	leaq	48(%r12), %rax
	movq	16(%r12), %rbx
	cmpq	%rax, %rdi
	je	.L3700
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L3656
.L3701:
	movq	%rbx, %r12
	jmp	.L3699
	.p2align 4,,10
	.p2align 3
.L3700:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L3701
.L3656:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3779
	movq	-480(%rbp), %rax
	addq	$440, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3686:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r15, %r15
	jne	.L3687
.L3683:
	movq	-232(%rbp), %r14
	jmp	.L3677
	.p2align 4,,10
	.p2align 3
.L3657:
	movq	%r15, -64(%rbp)
	leaq	-376(%rbp), %r14
	leaq	-64(%rbp), %rdx
	movq	%r15, -432(%rbp)
	leaq	-368(%rbp), %r15
	leaq	-416(%rbp), %rax
	movq	%r14, %rdi
	leaq	16+_ZTVN2v88internal6torque9UnionTypeE(%rip), %rbx
	movq	%r15, %rsi
	movl	$3, -440(%rbp)
	movl	$0, -416(%rbp)
	movq	$0, -408(%rbp)
	movq	%rax, -400(%rbp)
	movq	%rax, -392(%rbp)
	movq	$0, -384(%rbp)
	movq	%rbx, -448(%rbp)
	movl	$0, -368(%rbp)
	movq	$0, -360(%rbp)
	movq	%r15, -352(%rbp)
	movq	%r15, -344(%rbp)
	movq	$0, -336(%rbp)
.LEHB450:
	call	_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS5_ERKS5_
	movq	%rdx, %r13
	testq	%rdx, %rdx
	je	.L3661
	testq	%rax, %rax
	setne	%dil
	cmpq	%r15, %rdx
	sete	%al
	orb	%al, %dil
	movb	%dil, -472(%rbp)
	je	.L3780
.L3666:
	movl	$40, %edi
	call	_Znwm@PLT
	movq	%rax, %rsi
	movq	-64(%rbp), %rax
	movq	%r15, %rcx
	movq	%r13, %rdx
	movzbl	-472(%rbp), %edi
	movq	%rax, 32(%rsi)
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, -336(%rbp)
	jmp	.L3661
	.p2align 4,,10
	.p2align 3
.L3780:
	movq	32(%rdx), %rsi
	movq	-64(%rbp), %rdi
	call	_ZN2v88internal6torqueltERKNS1_4TypeES4_
.LEHE450:
	movb	%al, -472(%rbp)
	jmp	.L3666
	.p2align 4,,10
	.p2align 3
.L3678:
	leaq	-160(%rbp), %rax
	movq	$0, -128(%rbp)
	movl	$0, -160(%rbp)
	movq	$0, -152(%rbp)
	movq	%rax, -144(%rbp)
	movq	%rax, -136(%rbp)
	jmp	.L3679
	.p2align 4,,10
	.p2align 3
.L3680:
	leaq	-112(%rbp), %rax
	movl	$0, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	%rax, -96(%rbp)
	movq	%rax, -88(%rbp)
	movq	$0, -80(%rbp)
	jmp	.L3681
.L3779:
	call	__stack_chk_fail@PLT
.L3704:
	endbr64
	movq	%rax, %r12
	jmp	.L3692
.L3707:
	endbr64
	movq	%rax, %r12
	jmp	.L3675
.L3706:
	endbr64
	movq	%rax, %r12
	jmp	.L3667
.L3703:
	endbr64
	movq	%rax, %r12
	jmp	.L3676
.L3708:
	endbr64
	movq	%rax, %rbx
	jmp	.L3691
.L3705:
	endbr64
	movq	%rax, %r12
	jmp	.L3664
	.section	.gcc_except_table._ZN2v88internal6torque12SubtractTypeEPKNS1_4TypeES4_,"a",@progbits
.LLSDA6525:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6525-.LLSDACSB6525
.LLSDACSB6525:
	.uleb128 .LEHB444-.LFB6525
	.uleb128 .LEHE444-.LEHB444
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB445-.LFB6525
	.uleb128 .LEHE445-.LEHB445
	.uleb128 .L3705-.LFB6525
	.uleb128 0
	.uleb128 .LEHB446-.LFB6525
	.uleb128 .LEHE446-.LEHB446
	.uleb128 .L3703-.LFB6525
	.uleb128 0
	.uleb128 .LEHB447-.LFB6525
	.uleb128 .LEHE447-.LEHB447
	.uleb128 .L3707-.LFB6525
	.uleb128 0
	.uleb128 .LEHB448-.LFB6525
	.uleb128 .LEHE448-.LEHB448
	.uleb128 .L3704-.LFB6525
	.uleb128 0
	.uleb128 .LEHB449-.LFB6525
	.uleb128 .LEHE449-.LEHB449
	.uleb128 .L3708-.LFB6525
	.uleb128 0
	.uleb128 .LEHB450-.LFB6525
	.uleb128 .LEHE450-.LEHB450
	.uleb128 .L3706-.LFB6525
	.uleb128 0
.LLSDACSE6525:
	.section	.text._ZN2v88internal6torque12SubtractTypeEPKNS1_4TypeES4_
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque12SubtractTypeEPKNS1_4TypeES4_
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6525
	.type	_ZN2v88internal6torque12SubtractTypeEPKNS1_4TypeES4_.cold, @function
_ZN2v88internal6torque12SubtractTypeEPKNS1_4TypeES4_.cold:
.LFSB6525:
.L3691:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	%r12, %rdi
	movq	%rbx, %r12
	call	_ZN2v88internal6torque9UnionTypeD1Ev
.L3692:
	leaq	-320(%rbp), %rdi
	call	_ZN2v88internal6torque9UnionTypeD1Ev
.L3676:
	movq	-472(%rbp), %rdi
	call	_ZN2v88internal6torque9UnionTypeD1Ev
	movq	%r12, %rdi
.LEHB451:
	call	_Unwind_Resume@PLT
.L3675:
	movq	-280(%rbp), %rsi
	leaq	-296(%rbp), %rdi
	movq	%r13, -320(%rbp)
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	jmp	.L3676
.L3667:
	movq	-360(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	-408(%rbp), %rsi
	leaq	16+_ZTVN2v88internal6torque4TypeE(%rip), %rax
	leaq	-424(%rbp), %rdi
	movq	%rax, -448(%rbp)
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	%r12, %rdi
	call	_Unwind_Resume@PLT
.L3664:
	movq	-408(%rbp), %rsi
	leaq	-424(%rbp), %rdi
	movq	%r13, -448(%rbp)
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	%r12, %rdi
	call	_Unwind_Resume@PLT
.LEHE451:
	.cfi_endproc
.LFE6525:
	.section	.gcc_except_table._ZN2v88internal6torque12SubtractTypeEPKNS1_4TypeES4_
.LLSDAC6525:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6525-.LLSDACSBC6525
.LLSDACSBC6525:
	.uleb128 .LEHB451-.LCOLDB119
	.uleb128 .LEHE451-.LEHB451
	.uleb128 0
	.uleb128 0
.LLSDACSEC6525:
	.section	.text.unlikely._ZN2v88internal6torque12SubtractTypeEPKNS1_4TypeES4_
	.section	.text._ZN2v88internal6torque12SubtractTypeEPKNS1_4TypeES4_
	.size	_ZN2v88internal6torque12SubtractTypeEPKNS1_4TypeES4_, .-_ZN2v88internal6torque12SubtractTypeEPKNS1_4TypeES4_
	.section	.text.unlikely._ZN2v88internal6torque12SubtractTypeEPKNS1_4TypeES4_
	.size	_ZN2v88internal6torque12SubtractTypeEPKNS1_4TypeES4_.cold, .-_ZN2v88internal6torque12SubtractTypeEPKNS1_4TypeES4_.cold
.LCOLDE119:
	.section	.text._ZN2v88internal6torque12SubtractTypeEPKNS1_4TypeES4_
.LHOTE119:
	.section	.text.startup._GLOBAL__sub_I__ZNK2v88internal6torque4Type8ToStringB5cxx11Ev,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZNK2v88internal6torque4Type8ToStringB5cxx11Ev, @function
_GLOBAL__sub_I__ZNK2v88internal6torque4Type8ToStringB5cxx11Ev:
.LFB12613:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE12613:
	.size	_GLOBAL__sub_I__ZNK2v88internal6torque4Type8ToStringB5cxx11Ev, .-_GLOBAL__sub_I__ZNK2v88internal6torque4Type8ToStringB5cxx11Ev
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZNK2v88internal6torque4Type8ToStringB5cxx11Ev
	.weak	_ZTVN2v88internal6torque10IdentifierE
	.section	.data.rel.ro.local._ZTVN2v88internal6torque10IdentifierE,"awG",@progbits,_ZTVN2v88internal6torque10IdentifierE,comdat
	.align 8
	.type	_ZTVN2v88internal6torque10IdentifierE, @object
	.size	_ZTVN2v88internal6torque10IdentifierE, 32
_ZTVN2v88internal6torque10IdentifierE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal6torque10IdentifierD1Ev
	.quad	_ZN2v88internal6torque10IdentifierD0Ev
	.weak	_ZTVN2v88internal6torque20IdentifierExpressionE
	.section	.data.rel.ro.local._ZTVN2v88internal6torque20IdentifierExpressionE,"awG",@progbits,_ZTVN2v88internal6torque20IdentifierExpressionE,comdat
	.align 8
	.type	_ZTVN2v88internal6torque20IdentifierExpressionE, @object
	.size	_ZTVN2v88internal6torque20IdentifierExpressionE, 40
_ZTVN2v88internal6torque20IdentifierExpressionE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal6torque20IdentifierExpressionD1Ev
	.quad	_ZN2v88internal6torque20IdentifierExpressionD0Ev
	.quad	_ZN2v88internal6torque20IdentifierExpression22VisitAllSubExpressionsESt8functionIFvPNS1_10ExpressionEEE
	.weak	_ZTVN2v88internal6torque21FieldAccessExpressionE
	.section	.data.rel.ro.local._ZTVN2v88internal6torque21FieldAccessExpressionE,"awG",@progbits,_ZTVN2v88internal6torque21FieldAccessExpressionE,comdat
	.align 8
	.type	_ZTVN2v88internal6torque21FieldAccessExpressionE, @object
	.size	_ZTVN2v88internal6torque21FieldAccessExpressionE, 40
_ZTVN2v88internal6torque21FieldAccessExpressionE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal6torque21FieldAccessExpressionD1Ev
	.quad	_ZN2v88internal6torque21FieldAccessExpressionD0Ev
	.quad	_ZN2v88internal6torque21FieldAccessExpression22VisitAllSubExpressionsESt8functionIFvPNS1_10ExpressionEEE
	.weak	_ZTVN2v88internal6torque20AssignmentExpressionE
	.section	.data.rel.ro.local._ZTVN2v88internal6torque20AssignmentExpressionE,"awG",@progbits,_ZTVN2v88internal6torque20AssignmentExpressionE,comdat
	.align 8
	.type	_ZTVN2v88internal6torque20AssignmentExpressionE, @object
	.size	_ZTVN2v88internal6torque20AssignmentExpressionE, 40
_ZTVN2v88internal6torque20AssignmentExpressionE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal6torque20AssignmentExpressionD1Ev
	.quad	_ZN2v88internal6torque20AssignmentExpressionD0Ev
	.quad	_ZN2v88internal6torque20AssignmentExpression22VisitAllSubExpressionsESt8functionIFvPNS1_10ExpressionEEE
	.weak	_ZTVN2v88internal6torque19ExpressionStatementE
	.section	.data.rel.ro.local._ZTVN2v88internal6torque19ExpressionStatementE,"awG",@progbits,_ZTVN2v88internal6torque19ExpressionStatementE,comdat
	.align 8
	.type	_ZTVN2v88internal6torque19ExpressionStatementE, @object
	.size	_ZTVN2v88internal6torque19ExpressionStatementE, 32
_ZTVN2v88internal6torque19ExpressionStatementE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal6torque19ExpressionStatementD1Ev
	.quad	_ZN2v88internal6torque19ExpressionStatementD0Ev
	.weak	_ZTVN2v88internal6torque15ReturnStatementE
	.section	.data.rel.ro.local._ZTVN2v88internal6torque15ReturnStatementE,"awG",@progbits,_ZTVN2v88internal6torque15ReturnStatementE,comdat
	.align 8
	.type	_ZTVN2v88internal6torque15ReturnStatementE, @object
	.size	_ZTVN2v88internal6torque15ReturnStatementE, 32
_ZTVN2v88internal6torque15ReturnStatementE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal6torque15ReturnStatementD1Ev
	.quad	_ZN2v88internal6torque15ReturnStatementD0Ev
	.weak	_ZTVN2v88internal6torque13AggregateTypeE
	.section	.data.rel.ro._ZTVN2v88internal6torque13AggregateTypeE,"awG",@progbits,_ZTVN2v88internal6torque13AggregateTypeE,comdat
	.align 8
	.type	_ZTVN2v88internal6torque13AggregateTypeE, @object
	.size	_ZTVN2v88internal6torque13AggregateTypeE, 136
_ZTVN2v88internal6torque13AggregateTypeE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	_ZNK2v88internal6torque4Type11IsSubtypeOfEPKS2_
	.quad	_ZNK2v88internal6torque13AggregateType11MangledNameB5cxx11Ev
	.quad	_ZNK2v88internal6torque4Type11IsConstexprEv
	.quad	_ZNK2v88internal6torque4Type11IsTransientEv
	.quad	_ZNK2v88internal6torque4Type19NonConstexprVersionEv
	.quad	_ZNK2v88internal6torque4Type16ConstexprVersionEv
	.quad	_ZNK2v88internal6torque13AggregateType15GetRuntimeTypesB5cxx11Ev
	.quad	__cxa_pure_virtual
	.quad	_ZNK2v88internal6torque13AggregateType24GetGeneratedTypeNameImplB5cxx11Ev
	.quad	_ZNK2v88internal6torque13AggregateType29GetGeneratedTNodeTypeNameImplB5cxx11Ev
	.quad	__cxa_pure_virtual
	.quad	_ZNK2v88internal6torque13AggregateType15HasIndexedFieldEv
	.quad	_ZN2v88internal6torque13AggregateType13RegisterFieldENS1_5FieldE
	.weak	_ZTVN2v88internal6torque4TypeE
	.section	.data.rel.ro._ZTVN2v88internal6torque4TypeE,"awG",@progbits,_ZTVN2v88internal6torque4TypeE,comdat
	.align 8
	.type	_ZTVN2v88internal6torque4TypeE, @object
	.size	_ZTVN2v88internal6torque4TypeE, 112
_ZTVN2v88internal6torque4TypeE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	_ZNK2v88internal6torque4Type11IsSubtypeOfEPKS2_
	.quad	__cxa_pure_virtual
	.quad	_ZNK2v88internal6torque4Type11IsConstexprEv
	.quad	_ZNK2v88internal6torque4Type11IsTransientEv
	.quad	_ZNK2v88internal6torque4Type19NonConstexprVersionEv
	.quad	_ZNK2v88internal6torque4Type16ConstexprVersionEv
	.quad	_ZNK2v88internal6torque4Type15GetRuntimeTypesB5cxx11Ev
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.weak	_ZTVN2v88internal6torque12AbstractTypeE
	.section	.data.rel.ro.local._ZTVN2v88internal6torque12AbstractTypeE,"awG",@progbits,_ZTVN2v88internal6torque12AbstractTypeE,comdat
	.align 8
	.type	_ZTVN2v88internal6torque12AbstractTypeE, @object
	.size	_ZTVN2v88internal6torque12AbstractTypeE, 112
_ZTVN2v88internal6torque12AbstractTypeE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal6torque12AbstractTypeD1Ev
	.quad	_ZN2v88internal6torque12AbstractTypeD0Ev
	.quad	_ZNK2v88internal6torque4Type11IsSubtypeOfEPKS2_
	.quad	_ZNK2v88internal6torque12AbstractType11MangledNameB5cxx11Ev
	.quad	_ZNK2v88internal6torque12AbstractType11IsConstexprEv
	.quad	_ZNK2v88internal6torque12AbstractType11IsTransientEv
	.quad	_ZNK2v88internal6torque12AbstractType19NonConstexprVersionEv
	.quad	_ZNK2v88internal6torque12AbstractType16ConstexprVersionEv
	.quad	_ZNK2v88internal6torque12AbstractType15GetRuntimeTypesB5cxx11Ev
	.quad	_ZNK2v88internal6torque12AbstractType16ToExplicitStringB5cxx11Ev
	.quad	_ZNK2v88internal6torque12AbstractType24GetGeneratedTypeNameImplB5cxx11Ev
	.quad	_ZNK2v88internal6torque12AbstractType29GetGeneratedTNodeTypeNameImplB5cxx11Ev
	.weak	_ZTVN2v88internal6torque18BuiltinPointerTypeE
	.section	.data.rel.ro.local._ZTVN2v88internal6torque18BuiltinPointerTypeE,"awG",@progbits,_ZTVN2v88internal6torque18BuiltinPointerTypeE,comdat
	.align 8
	.type	_ZTVN2v88internal6torque18BuiltinPointerTypeE, @object
	.size	_ZTVN2v88internal6torque18BuiltinPointerTypeE, 112
_ZTVN2v88internal6torque18BuiltinPointerTypeE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal6torque18BuiltinPointerTypeD1Ev
	.quad	_ZN2v88internal6torque18BuiltinPointerTypeD0Ev
	.quad	_ZNK2v88internal6torque4Type11IsSubtypeOfEPKS2_
	.quad	_ZNK2v88internal6torque18BuiltinPointerType11MangledNameB5cxx11Ev
	.quad	_ZNK2v88internal6torque4Type11IsConstexprEv
	.quad	_ZNK2v88internal6torque4Type11IsTransientEv
	.quad	_ZNK2v88internal6torque4Type19NonConstexprVersionEv
	.quad	_ZNK2v88internal6torque4Type16ConstexprVersionEv
	.quad	_ZNK2v88internal6torque18BuiltinPointerType15GetRuntimeTypesB5cxx11Ev
	.quad	_ZNK2v88internal6torque18BuiltinPointerType16ToExplicitStringB5cxx11Ev
	.quad	_ZNK2v88internal6torque18BuiltinPointerType24GetGeneratedTypeNameImplB5cxx11Ev
	.quad	_ZNK2v88internal6torque18BuiltinPointerType29GetGeneratedTNodeTypeNameImplB5cxx11Ev
	.weak	_ZTVN2v88internal6torque9UnionTypeE
	.section	.data.rel.ro.local._ZTVN2v88internal6torque9UnionTypeE,"awG",@progbits,_ZTVN2v88internal6torque9UnionTypeE,comdat
	.align 8
	.type	_ZTVN2v88internal6torque9UnionTypeE, @object
	.size	_ZTVN2v88internal6torque9UnionTypeE, 112
_ZTVN2v88internal6torque9UnionTypeE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal6torque9UnionTypeD1Ev
	.quad	_ZN2v88internal6torque9UnionTypeD0Ev
	.quad	_ZNK2v88internal6torque9UnionType11IsSubtypeOfEPKNS1_4TypeE
	.quad	_ZNK2v88internal6torque9UnionType11MangledNameB5cxx11Ev
	.quad	_ZNK2v88internal6torque4Type11IsConstexprEv
	.quad	_ZNK2v88internal6torque9UnionType11IsTransientEv
	.quad	_ZNK2v88internal6torque4Type19NonConstexprVersionEv
	.quad	_ZNK2v88internal6torque4Type16ConstexprVersionEv
	.quad	_ZNK2v88internal6torque9UnionType15GetRuntimeTypesB5cxx11Ev
	.quad	_ZNK2v88internal6torque9UnionType16ToExplicitStringB5cxx11Ev
	.quad	_ZNK2v88internal6torque9UnionType24GetGeneratedTypeNameImplB5cxx11Ev
	.quad	_ZNK2v88internal6torque9UnionType29GetGeneratedTNodeTypeNameImplB5cxx11Ev
	.weak	_ZTVN2v88internal6torque10StructTypeE
	.section	.data.rel.ro.local._ZTVN2v88internal6torque10StructTypeE,"awG",@progbits,_ZTVN2v88internal6torque10StructTypeE,comdat
	.align 8
	.type	_ZTVN2v88internal6torque10StructTypeE, @object
	.size	_ZTVN2v88internal6torque10StructTypeE, 136
_ZTVN2v88internal6torque10StructTypeE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal6torque10StructTypeD1Ev
	.quad	_ZN2v88internal6torque10StructTypeD0Ev
	.quad	_ZNK2v88internal6torque4Type11IsSubtypeOfEPKS2_
	.quad	_ZNK2v88internal6torque10StructType11MangledNameB5cxx11Ev
	.quad	_ZNK2v88internal6torque4Type11IsConstexprEv
	.quad	_ZNK2v88internal6torque4Type11IsTransientEv
	.quad	_ZNK2v88internal6torque4Type19NonConstexprVersionEv
	.quad	_ZNK2v88internal6torque4Type16ConstexprVersionEv
	.quad	_ZNK2v88internal6torque13AggregateType15GetRuntimeTypesB5cxx11Ev
	.quad	_ZNK2v88internal6torque10StructType16ToExplicitStringB5cxx11Ev
	.quad	_ZNK2v88internal6torque10StructType24GetGeneratedTypeNameImplB5cxx11Ev
	.quad	_ZNK2v88internal6torque13AggregateType29GetGeneratedTNodeTypeNameImplB5cxx11Ev
	.quad	_ZNK2v88internal6torque10StructType8FinalizeEv
	.quad	_ZNK2v88internal6torque13AggregateType15HasIndexedFieldEv
	.quad	_ZN2v88internal6torque13AggregateType13RegisterFieldENS1_5FieldE
	.weak	_ZTVN2v88internal6torque9ClassTypeE
	.section	.data.rel.ro.local._ZTVN2v88internal6torque9ClassTypeE,"awG",@progbits,_ZTVN2v88internal6torque9ClassTypeE,comdat
	.align 8
	.type	_ZTVN2v88internal6torque9ClassTypeE, @object
	.size	_ZTVN2v88internal6torque9ClassTypeE, 136
_ZTVN2v88internal6torque9ClassTypeE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal6torque9ClassTypeD1Ev
	.quad	_ZN2v88internal6torque9ClassTypeD0Ev
	.quad	_ZNK2v88internal6torque4Type11IsSubtypeOfEPKS2_
	.quad	_ZNK2v88internal6torque13AggregateType11MangledNameB5cxx11Ev
	.quad	_ZNK2v88internal6torque4Type11IsConstexprEv
	.quad	_ZNK2v88internal6torque9ClassType11IsTransientEv
	.quad	_ZNK2v88internal6torque4Type19NonConstexprVersionEv
	.quad	_ZNK2v88internal6torque4Type16ConstexprVersionEv
	.quad	_ZNK2v88internal6torque13AggregateType15GetRuntimeTypesB5cxx11Ev
	.quad	_ZNK2v88internal6torque9ClassType16ToExplicitStringB5cxx11Ev
	.quad	_ZNK2v88internal6torque9ClassType24GetGeneratedTypeNameImplB5cxx11Ev
	.quad	_ZNK2v88internal6torque9ClassType29GetGeneratedTNodeTypeNameImplB5cxx11Ev
	.quad	_ZNK2v88internal6torque9ClassType8FinalizeEv
	.quad	_ZNK2v88internal6torque9ClassType15HasIndexedFieldEv
	.quad	_ZN2v88internal6torque9ClassType13RegisterFieldENS1_5FieldE
	.globl	_ZN2v88internal6torque9ClassType14kInternalFlagsE
	.section	.rodata._ZN2v88internal6torque9ClassType14kInternalFlagsE,"a"
	.align 4
	.type	_ZN2v88internal6torque9ClassType14kInternalFlagsE, @object
	.size	_ZN2v88internal6torque9ClassType14kInternalFlagsE, 4
_ZN2v88internal6torque9ClassType14kInternalFlagsE:
	.long	256
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata._ZStL19piecewise_construct,"a"
	.type	_ZStL19piecewise_construct, @object
	.size	_ZStL19piecewise_construct, 1
_ZStL19piecewise_construct:
	.zero	1
	.section	.data.rel.ro,"aw"
	.align 8
.LC22:
	.quad	_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE+64
	.align 8
.LC23:
	.quad	_ZTVSt15basic_streambufIcSt11char_traitsIcEE+16
	.align 8
.LC24:
	.quad	_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE+16
	.hidden	DW.ref.__gxx_personality_v0
	.weak	DW.ref.__gxx_personality_v0
	.section	.data.rel.local.DW.ref.__gxx_personality_v0,"awG",@progbits,DW.ref.__gxx_personality_v0,comdat
	.align 8
	.type	DW.ref.__gxx_personality_v0, @object
	.size	DW.ref.__gxx_personality_v0, 8
DW.ref.__gxx_personality_v0:
	.quad	__gxx_personality_v0
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
