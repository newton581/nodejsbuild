	.file	"instructions.cc"
	.text
	.section	.text._ZNK2v88internal6torque4Type11IsConstexprEv,"axG",@progbits,_ZNK2v88internal6torque4Type11IsConstexprEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal6torque4Type11IsConstexprEv
	.type	_ZNK2v88internal6torque4Type11IsConstexprEv, @function
_ZNK2v88internal6torque4Type11IsConstexprEv:
.LFB5435:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE5435:
	.size	_ZNK2v88internal6torque4Type11IsConstexprEv, .-_ZNK2v88internal6torque4Type11IsConstexprEv
	.section	.text._ZNK2v88internal6torque4Type11IsTransientEv,"axG",@progbits,_ZNK2v88internal6torque4Type11IsTransientEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal6torque4Type11IsTransientEv
	.type	_ZNK2v88internal6torque4Type11IsTransientEv, @function
_ZNK2v88internal6torque4Type11IsTransientEv:
.LFB5436:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE5436:
	.size	_ZNK2v88internal6torque4Type11IsTransientEv, .-_ZNK2v88internal6torque4Type11IsTransientEv
	.section	.text._ZNK2v88internal6torque4Type19NonConstexprVersionEv,"axG",@progbits,_ZNK2v88internal6torque4Type19NonConstexprVersionEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal6torque4Type19NonConstexprVersionEv
	.type	_ZNK2v88internal6torque4Type19NonConstexprVersionEv, @function
_ZNK2v88internal6torque4Type19NonConstexprVersionEv:
.LFB5437:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	ret
	.cfi_endproc
.LFE5437:
	.size	_ZNK2v88internal6torque4Type19NonConstexprVersionEv, .-_ZNK2v88internal6torque4Type19NonConstexprVersionEv
	.section	.text._ZNK2v88internal6torque4Type16ConstexprVersionEv,"axG",@progbits,_ZNK2v88internal6torque4Type16ConstexprVersionEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal6torque4Type16ConstexprVersionEv
	.type	_ZNK2v88internal6torque4Type16ConstexprVersionEv, @function
_ZNK2v88internal6torque4Type16ConstexprVersionEv:
.LFB5438:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE5438:
	.size	_ZNK2v88internal6torque4Type16ConstexprVersionEv, .-_ZNK2v88internal6torque4Type16ConstexprVersionEv
	.section	.text._ZNK2v88internal6torque4Type15GetRuntimeTypesB5cxx11Ev,"axG",@progbits,_ZNK2v88internal6torque4Type15GetRuntimeTypesB5cxx11Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal6torque4Type15GetRuntimeTypesB5cxx11Ev
	.type	_ZNK2v88internal6torque4Type15GetRuntimeTypesB5cxx11Ev, @function
_ZNK2v88internal6torque4Type15GetRuntimeTypesB5cxx11Ev:
.LFB5439:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movq	$0, 16(%rdi)
	movq	%rdi, %rax
	movups	%xmm0, (%rdi)
	ret
	.cfi_endproc
.LFE5439:
	.size	_ZNK2v88internal6torque4Type15GetRuntimeTypesB5cxx11Ev, .-_ZNK2v88internal6torque4Type15GetRuntimeTypesB5cxx11Ev
	.section	.text._ZNK2v88internal6torque15InstructionBase17IsBlockTerminatorEv,"axG",@progbits,_ZNK2v88internal6torque15InstructionBase17IsBlockTerminatorEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal6torque15InstructionBase17IsBlockTerminatorEv
	.type	_ZNK2v88internal6torque15InstructionBase17IsBlockTerminatorEv, @function
_ZNK2v88internal6torque15InstructionBase17IsBlockTerminatorEv:
.LFB5781:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE5781:
	.size	_ZNK2v88internal6torque15InstructionBase17IsBlockTerminatorEv, .-_ZNK2v88internal6torque15InstructionBase17IsBlockTerminatorEv
	.section	.text._ZNK2v88internal6torque15InstructionBase21AppendSuccessorBlocksEPSt6vectorIPNS1_5BlockESaIS5_EE,"axG",@progbits,_ZNK2v88internal6torque15InstructionBase21AppendSuccessorBlocksEPSt6vectorIPNS1_5BlockESaIS5_EE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal6torque15InstructionBase21AppendSuccessorBlocksEPSt6vectorIPNS1_5BlockESaIS5_EE
	.type	_ZNK2v88internal6torque15InstructionBase21AppendSuccessorBlocksEPSt6vectorIPNS1_5BlockESaIS5_EE, @function
_ZNK2v88internal6torque15InstructionBase21AppendSuccessorBlocksEPSt6vectorIPNS1_5BlockESaIS5_EE:
.LFB5782:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE5782:
	.size	_ZNK2v88internal6torque15InstructionBase21AppendSuccessorBlocksEPSt6vectorIPNS1_5BlockESaIS5_EE, .-_ZNK2v88internal6torque15InstructionBase21AppendSuccessorBlocksEPSt6vectorIPNS1_5BlockESaIS5_EE
	.section	.text._ZNK2v88internal6torque32CallCsaMacroAndBranchInstruction17IsBlockTerminatorEv,"axG",@progbits,_ZNK2v88internal6torque32CallCsaMacroAndBranchInstruction17IsBlockTerminatorEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal6torque32CallCsaMacroAndBranchInstruction17IsBlockTerminatorEv
	.type	_ZNK2v88internal6torque32CallCsaMacroAndBranchInstruction17IsBlockTerminatorEv, @function
_ZNK2v88internal6torque32CallCsaMacroAndBranchInstruction17IsBlockTerminatorEv:
.LFB5842:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE5842:
	.size	_ZNK2v88internal6torque32CallCsaMacroAndBranchInstruction17IsBlockTerminatorEv, .-_ZNK2v88internal6torque32CallCsaMacroAndBranchInstruction17IsBlockTerminatorEv
	.section	.text._ZNK2v88internal6torque22CallBuiltinInstruction17IsBlockTerminatorEv,"axG",@progbits,_ZNK2v88internal6torque22CallBuiltinInstruction17IsBlockTerminatorEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal6torque22CallBuiltinInstruction17IsBlockTerminatorEv
	.type	_ZNK2v88internal6torque22CallBuiltinInstruction17IsBlockTerminatorEv, @function
_ZNK2v88internal6torque22CallBuiltinInstruction17IsBlockTerminatorEv:
.LFB5844:
	.cfi_startproc
	endbr64
	movzbl	28(%rdi), %eax
	ret
	.cfi_endproc
.LFE5844:
	.size	_ZNK2v88internal6torque22CallBuiltinInstruction17IsBlockTerminatorEv, .-_ZNK2v88internal6torque22CallBuiltinInstruction17IsBlockTerminatorEv
	.section	.text._ZNK2v88internal6torque29CallBuiltinPointerInstruction17IsBlockTerminatorEv,"axG",@progbits,_ZNK2v88internal6torque29CallBuiltinPointerInstruction17IsBlockTerminatorEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal6torque29CallBuiltinPointerInstruction17IsBlockTerminatorEv
	.type	_ZNK2v88internal6torque29CallBuiltinPointerInstruction17IsBlockTerminatorEv, @function
_ZNK2v88internal6torque29CallBuiltinPointerInstruction17IsBlockTerminatorEv:
.LFB5849:
	.cfi_startproc
	endbr64
	movzbl	28(%rdi), %eax
	ret
	.cfi_endproc
.LFE5849:
	.size	_ZNK2v88internal6torque29CallBuiltinPointerInstruction17IsBlockTerminatorEv, .-_ZNK2v88internal6torque29CallBuiltinPointerInstruction17IsBlockTerminatorEv
	.section	.text._ZNK2v88internal6torque17BranchInstruction17IsBlockTerminatorEv,"axG",@progbits,_ZNK2v88internal6torque17BranchInstruction17IsBlockTerminatorEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal6torque17BranchInstruction17IsBlockTerminatorEv
	.type	_ZNK2v88internal6torque17BranchInstruction17IsBlockTerminatorEv, @function
_ZNK2v88internal6torque17BranchInstruction17IsBlockTerminatorEv:
.LFB5857:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE5857:
	.size	_ZNK2v88internal6torque17BranchInstruction17IsBlockTerminatorEv, .-_ZNK2v88internal6torque17BranchInstruction17IsBlockTerminatorEv
	.section	.text._ZNK2v88internal6torque26ConstexprBranchInstruction17IsBlockTerminatorEv,"axG",@progbits,_ZNK2v88internal6torque26ConstexprBranchInstruction17IsBlockTerminatorEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal6torque26ConstexprBranchInstruction17IsBlockTerminatorEv
	.type	_ZNK2v88internal6torque26ConstexprBranchInstruction17IsBlockTerminatorEv, @function
_ZNK2v88internal6torque26ConstexprBranchInstruction17IsBlockTerminatorEv:
.LFB5862:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE5862:
	.size	_ZNK2v88internal6torque26ConstexprBranchInstruction17IsBlockTerminatorEv, .-_ZNK2v88internal6torque26ConstexprBranchInstruction17IsBlockTerminatorEv
	.section	.text._ZNK2v88internal6torque15GotoInstruction17IsBlockTerminatorEv,"axG",@progbits,_ZNK2v88internal6torque15GotoInstruction17IsBlockTerminatorEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal6torque15GotoInstruction17IsBlockTerminatorEv
	.type	_ZNK2v88internal6torque15GotoInstruction17IsBlockTerminatorEv, @function
_ZNK2v88internal6torque15GotoInstruction17IsBlockTerminatorEv:
.LFB5867:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE5867:
	.size	_ZNK2v88internal6torque15GotoInstruction17IsBlockTerminatorEv, .-_ZNK2v88internal6torque15GotoInstruction17IsBlockTerminatorEv
	.section	.text._ZNK2v88internal6torque23GotoExternalInstruction17IsBlockTerminatorEv,"axG",@progbits,_ZNK2v88internal6torque23GotoExternalInstruction17IsBlockTerminatorEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal6torque23GotoExternalInstruction17IsBlockTerminatorEv
	.type	_ZNK2v88internal6torque23GotoExternalInstruction17IsBlockTerminatorEv, @function
_ZNK2v88internal6torque23GotoExternalInstruction17IsBlockTerminatorEv:
.LFB5872:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE5872:
	.size	_ZNK2v88internal6torque23GotoExternalInstruction17IsBlockTerminatorEv, .-_ZNK2v88internal6torque23GotoExternalInstruction17IsBlockTerminatorEv
	.section	.text._ZNK2v88internal6torque17ReturnInstruction17IsBlockTerminatorEv,"axG",@progbits,_ZNK2v88internal6torque17ReturnInstruction17IsBlockTerminatorEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal6torque17ReturnInstruction17IsBlockTerminatorEv
	.type	_ZNK2v88internal6torque17ReturnInstruction17IsBlockTerminatorEv, @function
_ZNK2v88internal6torque17ReturnInstruction17IsBlockTerminatorEv:
.LFB5876:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE5876:
	.size	_ZNK2v88internal6torque17ReturnInstruction17IsBlockTerminatorEv, .-_ZNK2v88internal6torque17ReturnInstruction17IsBlockTerminatorEv
	.section	.text._ZNK2v88internal6torque16AbortInstruction17IsBlockTerminatorEv,"axG",@progbits,_ZNK2v88internal6torque16AbortInstruction17IsBlockTerminatorEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal6torque16AbortInstruction17IsBlockTerminatorEv
	.type	_ZNK2v88internal6torque16AbortInstruction17IsBlockTerminatorEv, @function
_ZNK2v88internal6torque16AbortInstruction17IsBlockTerminatorEv:
.LFB5880:
	.cfi_startproc
	endbr64
	movl	28(%rdi), %eax
	testl	%eax, %eax
	setne	%al
	ret
	.cfi_endproc
.LFE5880:
	.size	_ZNK2v88internal6torque16AbortInstruction17IsBlockTerminatorEv, .-_ZNK2v88internal6torque16AbortInstruction17IsBlockTerminatorEv
	.section	.text._ZN2v88internal6torque22DeleteRangeInstruction6AssignERKNS1_15InstructionBaseE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque22DeleteRangeInstruction6AssignERKNS1_15InstructionBaseE
	.type	_ZN2v88internal6torque22DeleteRangeInstruction6AssignERKNS1_15InstructionBaseE, @function
_ZN2v88internal6torque22DeleteRangeInstruction6AssignERKNS1_15InstructionBaseE:
.LFB6756:
	.cfi_startproc
	endbr64
	movdqu	8(%rsi), %xmm0
	movups	%xmm0, 8(%rdi)
	movl	24(%rsi), %eax
	movl	%eax, 24(%rdi)
	movdqu	32(%rsi), %xmm1
	movups	%xmm1, 32(%rdi)
	ret
	.cfi_endproc
.LFE6756:
	.size	_ZN2v88internal6torque22DeleteRangeInstruction6AssignERKNS1_15InstructionBaseE, .-_ZN2v88internal6torque22DeleteRangeInstruction6AssignERKNS1_15InstructionBaseE
	.section	.text._ZN2v88internal6torque28PushUninitializedInstruction6AssignERKNS1_15InstructionBaseE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque28PushUninitializedInstruction6AssignERKNS1_15InstructionBaseE
	.type	_ZN2v88internal6torque28PushUninitializedInstruction6AssignERKNS1_15InstructionBaseE, @function
_ZN2v88internal6torque28PushUninitializedInstruction6AssignERKNS1_15InstructionBaseE:
.LFB6762:
	.cfi_startproc
	endbr64
	movdqu	8(%rsi), %xmm0
	movups	%xmm0, 8(%rdi)
	movl	24(%rsi), %eax
	movl	%eax, 24(%rdi)
	movq	32(%rsi), %rax
	movq	%rax, 32(%rdi)
	ret
	.cfi_endproc
.LFE6762:
	.size	_ZN2v88internal6torque28PushUninitializedInstruction6AssignERKNS1_15InstructionBaseE, .-_ZN2v88internal6torque28PushUninitializedInstruction6AssignERKNS1_15InstructionBaseE
	.section	.text._ZN2v88internal6torque24LoadReferenceInstruction6AssignERKNS1_15InstructionBaseE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque24LoadReferenceInstruction6AssignERKNS1_15InstructionBaseE
	.type	_ZN2v88internal6torque24LoadReferenceInstruction6AssignERKNS1_15InstructionBaseE, @function
_ZN2v88internal6torque24LoadReferenceInstruction6AssignERKNS1_15InstructionBaseE:
.LFB6780:
	.cfi_startproc
	endbr64
	movdqu	8(%rsi), %xmm0
	movups	%xmm0, 8(%rdi)
	movl	24(%rsi), %eax
	movl	%eax, 24(%rdi)
	movq	32(%rsi), %rax
	movq	%rax, 32(%rdi)
	ret
	.cfi_endproc
.LFE6780:
	.size	_ZN2v88internal6torque24LoadReferenceInstruction6AssignERKNS1_15InstructionBaseE, .-_ZN2v88internal6torque24LoadReferenceInstruction6AssignERKNS1_15InstructionBaseE
	.section	.text._ZN2v88internal6torque25StoreReferenceInstruction6AssignERKNS1_15InstructionBaseE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque25StoreReferenceInstruction6AssignERKNS1_15InstructionBaseE
	.type	_ZN2v88internal6torque25StoreReferenceInstruction6AssignERKNS1_15InstructionBaseE, @function
_ZN2v88internal6torque25StoreReferenceInstruction6AssignERKNS1_15InstructionBaseE:
.LFB6786:
	.cfi_startproc
	endbr64
	movdqu	8(%rsi), %xmm0
	movups	%xmm0, 8(%rdi)
	movl	24(%rsi), %eax
	movl	%eax, 24(%rdi)
	movq	32(%rsi), %rax
	movq	%rax, 32(%rdi)
	ret
	.cfi_endproc
.LFE6786:
	.size	_ZN2v88internal6torque25StoreReferenceInstruction6AssignERKNS1_15InstructionBaseE, .-_ZN2v88internal6torque25StoreReferenceInstruction6AssignERKNS1_15InstructionBaseE
	.section	.text._ZN2v88internal6torque28NamespaceConstantInstruction6AssignERKNS1_15InstructionBaseE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque28NamespaceConstantInstruction6AssignERKNS1_15InstructionBaseE
	.type	_ZN2v88internal6torque28NamespaceConstantInstruction6AssignERKNS1_15InstructionBaseE, @function
_ZN2v88internal6torque28NamespaceConstantInstruction6AssignERKNS1_15InstructionBaseE:
.LFB6805:
	.cfi_startproc
	endbr64
	movdqu	8(%rsi), %xmm0
	movups	%xmm0, 8(%rdi)
	movl	24(%rsi), %eax
	movl	%eax, 24(%rdi)
	movq	32(%rsi), %rax
	movq	%rax, 32(%rdi)
	ret
	.cfi_endproc
.LFE6805:
	.size	_ZN2v88internal6torque28NamespaceConstantInstruction6AssignERKNS1_15InstructionBaseE, .-_ZN2v88internal6torque28NamespaceConstantInstruction6AssignERKNS1_15InstructionBaseE
	.section	.text._ZN2v88internal6torque29CallBuiltinPointerInstruction6AssignERKNS1_15InstructionBaseE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque29CallBuiltinPointerInstruction6AssignERKNS1_15InstructionBaseE
	.type	_ZN2v88internal6torque29CallBuiltinPointerInstruction6AssignERKNS1_15InstructionBaseE, @function
_ZN2v88internal6torque29CallBuiltinPointerInstruction6AssignERKNS1_15InstructionBaseE:
.LFB6829:
	.cfi_startproc
	endbr64
	movdqu	8(%rsi), %xmm0
	movups	%xmm0, 8(%rdi)
	movl	24(%rsi), %eax
	movl	%eax, 24(%rdi)
	movzbl	28(%rsi), %eax
	movb	%al, 28(%rdi)
	movq	32(%rsi), %rax
	movq	%rax, 32(%rdi)
	movq	40(%rsi), %rax
	movq	%rax, 40(%rdi)
	ret
	.cfi_endproc
.LFE6829:
	.size	_ZN2v88internal6torque29CallBuiltinPointerInstruction6AssignERKNS1_15InstructionBaseE, .-_ZN2v88internal6torque29CallBuiltinPointerInstruction6AssignERKNS1_15InstructionBaseE
	.section	.text._ZN2v88internal6torque17BranchInstruction6AssignERKNS1_15InstructionBaseE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque17BranchInstruction6AssignERKNS1_15InstructionBaseE
	.type	_ZN2v88internal6torque17BranchInstruction6AssignERKNS1_15InstructionBaseE, @function
_ZN2v88internal6torque17BranchInstruction6AssignERKNS1_15InstructionBaseE:
.LFB6835:
	.cfi_startproc
	endbr64
	movdqu	8(%rsi), %xmm0
	movups	%xmm0, 8(%rdi)
	movl	24(%rsi), %eax
	movl	%eax, 24(%rdi)
	movq	32(%rsi), %rax
	movq	%rax, 32(%rdi)
	movq	40(%rsi), %rax
	movq	%rax, 40(%rdi)
	ret
	.cfi_endproc
.LFE6835:
	.size	_ZN2v88internal6torque17BranchInstruction6AssignERKNS1_15InstructionBaseE, .-_ZN2v88internal6torque17BranchInstruction6AssignERKNS1_15InstructionBaseE
	.section	.text._ZN2v88internal6torque15GotoInstruction6AssignERKNS1_15InstructionBaseE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque15GotoInstruction6AssignERKNS1_15InstructionBaseE
	.type	_ZN2v88internal6torque15GotoInstruction6AssignERKNS1_15InstructionBaseE, @function
_ZN2v88internal6torque15GotoInstruction6AssignERKNS1_15InstructionBaseE:
.LFB6847:
	.cfi_startproc
	endbr64
	movdqu	8(%rsi), %xmm0
	movups	%xmm0, 8(%rdi)
	movl	24(%rsi), %eax
	movl	%eax, 24(%rdi)
	movq	32(%rsi), %rax
	movq	%rax, 32(%rdi)
	ret
	.cfi_endproc
.LFE6847:
	.size	_ZN2v88internal6torque15GotoInstruction6AssignERKNS1_15InstructionBaseE, .-_ZN2v88internal6torque15GotoInstruction6AssignERKNS1_15InstructionBaseE
	.section	.text._ZN2v88internal6torque17ReturnInstruction6AssignERKNS1_15InstructionBaseE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque17ReturnInstruction6AssignERKNS1_15InstructionBaseE
	.type	_ZN2v88internal6torque17ReturnInstruction6AssignERKNS1_15InstructionBaseE, @function
_ZN2v88internal6torque17ReturnInstruction6AssignERKNS1_15InstructionBaseE:
.LFB6859:
	.cfi_startproc
	endbr64
	movdqu	8(%rsi), %xmm0
	movups	%xmm0, 8(%rdi)
	movl	24(%rsi), %eax
	movl	%eax, 24(%rdi)
	ret
	.cfi_endproc
.LFE6859:
	.size	_ZN2v88internal6torque17ReturnInstruction6AssignERKNS1_15InstructionBaseE, .-_ZN2v88internal6torque17ReturnInstruction6AssignERKNS1_15InstructionBaseE
	.section	.text._ZN2v88internal6torque21UnsafeCastInstruction6AssignERKNS1_15InstructionBaseE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque21UnsafeCastInstruction6AssignERKNS1_15InstructionBaseE
	.type	_ZN2v88internal6torque21UnsafeCastInstruction6AssignERKNS1_15InstructionBaseE, @function
_ZN2v88internal6torque21UnsafeCastInstruction6AssignERKNS1_15InstructionBaseE:
.LFB6877:
	.cfi_startproc
	endbr64
	movdqu	8(%rsi), %xmm0
	movups	%xmm0, 8(%rdi)
	movl	24(%rsi), %eax
	movl	%eax, 24(%rdi)
	movq	32(%rsi), %rax
	movq	%rax, 32(%rdi)
	ret
	.cfi_endproc
.LFE6877:
	.size	_ZN2v88internal6torque21UnsafeCastInstruction6AssignERKNS1_15InstructionBaseE, .-_ZN2v88internal6torque21UnsafeCastInstruction6AssignERKNS1_15InstructionBaseE
	.section	.text._ZNK2v88internal6torque30PrintConstantStringInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal6torque30PrintConstantStringInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.type	_ZNK2v88internal6torque30PrintConstantStringInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE, @function
_ZNK2v88internal6torque30PrintConstantStringInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE:
.LFB6899:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE6899:
	.size	_ZNK2v88internal6torque30PrintConstantStringInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE, .-_ZNK2v88internal6torque30PrintConstantStringInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.globl	_ZNK2v88internal6torque16AbortInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.set	_ZNK2v88internal6torque16AbortInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE,_ZNK2v88internal6torque30PrintConstantStringInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.section	.text._ZN2v88internal6torque21UnsafeCastInstructionD2Ev,"axG",@progbits,_ZN2v88internal6torque21UnsafeCastInstructionD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque21UnsafeCastInstructionD2Ev
	.type	_ZN2v88internal6torque21UnsafeCastInstructionD2Ev, @function
_ZN2v88internal6torque21UnsafeCastInstructionD2Ev:
.LFB11722:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE11722:
	.size	_ZN2v88internal6torque21UnsafeCastInstructionD2Ev, .-_ZN2v88internal6torque21UnsafeCastInstructionD2Ev
	.weak	_ZN2v88internal6torque21UnsafeCastInstructionD1Ev
	.set	_ZN2v88internal6torque21UnsafeCastInstructionD1Ev,_ZN2v88internal6torque21UnsafeCastInstructionD2Ev
	.section	.text._ZN2v88internal6torque17ReturnInstructionD2Ev,"axG",@progbits,_ZN2v88internal6torque17ReturnInstructionD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque17ReturnInstructionD2Ev
	.type	_ZN2v88internal6torque17ReturnInstructionD2Ev, @function
_ZN2v88internal6torque17ReturnInstructionD2Ev:
.LFB11734:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE11734:
	.size	_ZN2v88internal6torque17ReturnInstructionD2Ev, .-_ZN2v88internal6torque17ReturnInstructionD2Ev
	.weak	_ZN2v88internal6torque17ReturnInstructionD1Ev
	.set	_ZN2v88internal6torque17ReturnInstructionD1Ev,_ZN2v88internal6torque17ReturnInstructionD2Ev
	.section	.text._ZN2v88internal6torque15GotoInstructionD2Ev,"axG",@progbits,_ZN2v88internal6torque15GotoInstructionD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque15GotoInstructionD2Ev
	.type	_ZN2v88internal6torque15GotoInstructionD2Ev, @function
_ZN2v88internal6torque15GotoInstructionD2Ev:
.LFB11742:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE11742:
	.size	_ZN2v88internal6torque15GotoInstructionD2Ev, .-_ZN2v88internal6torque15GotoInstructionD2Ev
	.weak	_ZN2v88internal6torque15GotoInstructionD1Ev
	.set	_ZN2v88internal6torque15GotoInstructionD1Ev,_ZN2v88internal6torque15GotoInstructionD2Ev
	.section	.text._ZN2v88internal6torque17BranchInstructionD2Ev,"axG",@progbits,_ZN2v88internal6torque17BranchInstructionD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque17BranchInstructionD2Ev
	.type	_ZN2v88internal6torque17BranchInstructionD2Ev, @function
_ZN2v88internal6torque17BranchInstructionD2Ev:
.LFB11750:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE11750:
	.size	_ZN2v88internal6torque17BranchInstructionD2Ev, .-_ZN2v88internal6torque17BranchInstructionD2Ev
	.weak	_ZN2v88internal6torque17BranchInstructionD1Ev
	.set	_ZN2v88internal6torque17BranchInstructionD1Ev,_ZN2v88internal6torque17BranchInstructionD2Ev
	.section	.text._ZN2v88internal6torque29CallBuiltinPointerInstructionD2Ev,"axG",@progbits,_ZN2v88internal6torque29CallBuiltinPointerInstructionD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque29CallBuiltinPointerInstructionD2Ev
	.type	_ZN2v88internal6torque29CallBuiltinPointerInstructionD2Ev, @function
_ZN2v88internal6torque29CallBuiltinPointerInstructionD2Ev:
.LFB11754:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE11754:
	.size	_ZN2v88internal6torque29CallBuiltinPointerInstructionD2Ev, .-_ZN2v88internal6torque29CallBuiltinPointerInstructionD2Ev
	.weak	_ZN2v88internal6torque29CallBuiltinPointerInstructionD1Ev
	.set	_ZN2v88internal6torque29CallBuiltinPointerInstructionD1Ev,_ZN2v88internal6torque29CallBuiltinPointerInstructionD2Ev
	.section	.text._ZN2v88internal6torque22CallRuntimeInstructionD2Ev,"axG",@progbits,_ZN2v88internal6torque22CallRuntimeInstructionD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque22CallRuntimeInstructionD2Ev
	.type	_ZN2v88internal6torque22CallRuntimeInstructionD2Ev, @function
_ZN2v88internal6torque22CallRuntimeInstructionD2Ev:
.LFB11758:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE11758:
	.size	_ZN2v88internal6torque22CallRuntimeInstructionD2Ev, .-_ZN2v88internal6torque22CallRuntimeInstructionD2Ev
	.weak	_ZN2v88internal6torque22CallRuntimeInstructionD1Ev
	.set	_ZN2v88internal6torque22CallRuntimeInstructionD1Ev,_ZN2v88internal6torque22CallRuntimeInstructionD2Ev
	.section	.text._ZN2v88internal6torque22CallBuiltinInstructionD2Ev,"axG",@progbits,_ZN2v88internal6torque22CallBuiltinInstructionD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque22CallBuiltinInstructionD2Ev
	.type	_ZN2v88internal6torque22CallBuiltinInstructionD2Ev, @function
_ZN2v88internal6torque22CallBuiltinInstructionD2Ev:
.LFB11762:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE11762:
	.size	_ZN2v88internal6torque22CallBuiltinInstructionD2Ev, .-_ZN2v88internal6torque22CallBuiltinInstructionD2Ev
	.weak	_ZN2v88internal6torque22CallBuiltinInstructionD1Ev
	.set	_ZN2v88internal6torque22CallBuiltinInstructionD1Ev,_ZN2v88internal6torque22CallBuiltinInstructionD2Ev
	.section	.text._ZN2v88internal6torque28NamespaceConstantInstructionD2Ev,"axG",@progbits,_ZN2v88internal6torque28NamespaceConstantInstructionD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque28NamespaceConstantInstructionD2Ev
	.type	_ZN2v88internal6torque28NamespaceConstantInstructionD2Ev, @function
_ZN2v88internal6torque28NamespaceConstantInstructionD2Ev:
.LFB11770:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE11770:
	.size	_ZN2v88internal6torque28NamespaceConstantInstructionD2Ev, .-_ZN2v88internal6torque28NamespaceConstantInstructionD2Ev
	.weak	_ZN2v88internal6torque28NamespaceConstantInstructionD1Ev
	.set	_ZN2v88internal6torque28NamespaceConstantInstructionD1Ev,_ZN2v88internal6torque28NamespaceConstantInstructionD2Ev
	.section	.text._ZN2v88internal6torque25StoreReferenceInstructionD2Ev,"axG",@progbits,_ZN2v88internal6torque25StoreReferenceInstructionD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque25StoreReferenceInstructionD2Ev
	.type	_ZN2v88internal6torque25StoreReferenceInstructionD2Ev, @function
_ZN2v88internal6torque25StoreReferenceInstructionD2Ev:
.LFB11782:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE11782:
	.size	_ZN2v88internal6torque25StoreReferenceInstructionD2Ev, .-_ZN2v88internal6torque25StoreReferenceInstructionD2Ev
	.weak	_ZN2v88internal6torque25StoreReferenceInstructionD1Ev
	.set	_ZN2v88internal6torque25StoreReferenceInstructionD1Ev,_ZN2v88internal6torque25StoreReferenceInstructionD2Ev
	.section	.text._ZN2v88internal6torque24LoadReferenceInstructionD2Ev,"axG",@progbits,_ZN2v88internal6torque24LoadReferenceInstructionD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque24LoadReferenceInstructionD2Ev
	.type	_ZN2v88internal6torque24LoadReferenceInstructionD2Ev, @function
_ZN2v88internal6torque24LoadReferenceInstructionD2Ev:
.LFB11786:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE11786:
	.size	_ZN2v88internal6torque24LoadReferenceInstructionD2Ev, .-_ZN2v88internal6torque24LoadReferenceInstructionD2Ev
	.weak	_ZN2v88internal6torque24LoadReferenceInstructionD1Ev
	.set	_ZN2v88internal6torque24LoadReferenceInstructionD1Ev,_ZN2v88internal6torque24LoadReferenceInstructionD2Ev
	.section	.text._ZN2v88internal6torque28PushUninitializedInstructionD2Ev,"axG",@progbits,_ZN2v88internal6torque28PushUninitializedInstructionD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque28PushUninitializedInstructionD2Ev
	.type	_ZN2v88internal6torque28PushUninitializedInstructionD2Ev, @function
_ZN2v88internal6torque28PushUninitializedInstructionD2Ev:
.LFB11798:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE11798:
	.size	_ZN2v88internal6torque28PushUninitializedInstructionD2Ev, .-_ZN2v88internal6torque28PushUninitializedInstructionD2Ev
	.weak	_ZN2v88internal6torque28PushUninitializedInstructionD1Ev
	.set	_ZN2v88internal6torque28PushUninitializedInstructionD1Ev,_ZN2v88internal6torque28PushUninitializedInstructionD2Ev
	.section	.text._ZN2v88internal6torque22DeleteRangeInstructionD2Ev,"axG",@progbits,_ZN2v88internal6torque22DeleteRangeInstructionD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque22DeleteRangeInstructionD2Ev
	.type	_ZN2v88internal6torque22DeleteRangeInstructionD2Ev, @function
_ZN2v88internal6torque22DeleteRangeInstructionD2Ev:
.LFB11802:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE11802:
	.size	_ZN2v88internal6torque22DeleteRangeInstructionD2Ev, .-_ZN2v88internal6torque22DeleteRangeInstructionD2Ev
	.weak	_ZN2v88internal6torque22DeleteRangeInstructionD1Ev
	.set	_ZN2v88internal6torque22DeleteRangeInstructionD1Ev,_ZN2v88internal6torque22DeleteRangeInstructionD2Ev
	.section	.text._ZN2v88internal6torque15PokeInstructionD2Ev,"axG",@progbits,_ZN2v88internal6torque15PokeInstructionD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque15PokeInstructionD2Ev
	.type	_ZN2v88internal6torque15PokeInstructionD2Ev, @function
_ZN2v88internal6torque15PokeInstructionD2Ev:
.LFB11806:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE11806:
	.size	_ZN2v88internal6torque15PokeInstructionD2Ev, .-_ZN2v88internal6torque15PokeInstructionD2Ev
	.weak	_ZN2v88internal6torque15PokeInstructionD1Ev
	.set	_ZN2v88internal6torque15PokeInstructionD1Ev,_ZN2v88internal6torque15PokeInstructionD2Ev
	.section	.text._ZN2v88internal6torque15PeekInstructionD2Ev,"axG",@progbits,_ZN2v88internal6torque15PeekInstructionD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque15PeekInstructionD2Ev
	.type	_ZN2v88internal6torque15PeekInstructionD2Ev, @function
_ZN2v88internal6torque15PeekInstructionD2Ev:
.LFB11810:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE11810:
	.size	_ZN2v88internal6torque15PeekInstructionD2Ev, .-_ZN2v88internal6torque15PeekInstructionD2Ev
	.weak	_ZN2v88internal6torque15PeekInstructionD1Ev
	.set	_ZN2v88internal6torque15PeekInstructionD1Ev,_ZN2v88internal6torque15PeekInstructionD2Ev
	.section	.text._ZNK2v88internal6torque15PeekInstruction5CloneEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal6torque15PeekInstruction5CloneEv
	.type	_ZNK2v88internal6torque15PeekInstruction5CloneEv, @function
_ZNK2v88internal6torque15PeekInstruction5CloneEv:
.LFB6735:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movl	$56, %edi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	call	_Znwm@PLT
	movl	24(%rbx), %edx
	movdqu	8(%rbx), %xmm0
	leaq	16+_ZTVN2v88internal6torque15PeekInstructionE(%rip), %rcx
	movdqu	40(%rbx), %xmm1
	movq	%rcx, (%rax)
	movl	%edx, 24(%rax)
	movq	32(%rbx), %rdx
	movups	%xmm0, 8(%rax)
	movq	%rdx, 32(%rax)
	movups	%xmm1, 40(%rax)
	popq	%rbx
	movq	%rax, (%r12)
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE6735:
	.size	_ZNK2v88internal6torque15PeekInstruction5CloneEv, .-_ZNK2v88internal6torque15PeekInstruction5CloneEv
	.section	.text._ZNK2v88internal6torque15PokeInstruction5CloneEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal6torque15PokeInstruction5CloneEv
	.type	_ZNK2v88internal6torque15PokeInstruction5CloneEv, @function
_ZNK2v88internal6torque15PokeInstruction5CloneEv:
.LFB6746:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movl	$56, %edi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	call	_Znwm@PLT
	movl	24(%rbx), %edx
	movdqu	8(%rbx), %xmm0
	leaq	16+_ZTVN2v88internal6torque15PokeInstructionE(%rip), %rcx
	movdqu	40(%rbx), %xmm1
	movq	%rcx, (%rax)
	movl	%edx, 24(%rax)
	movq	32(%rbx), %rdx
	movups	%xmm0, 8(%rax)
	movq	%rdx, 32(%rax)
	movups	%xmm1, 40(%rax)
	popq	%rbx
	movq	%rax, (%r12)
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE6746:
	.size	_ZNK2v88internal6torque15PokeInstruction5CloneEv, .-_ZNK2v88internal6torque15PokeInstruction5CloneEv
	.section	.text._ZNK2v88internal6torque22DeleteRangeInstruction5CloneEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal6torque22DeleteRangeInstruction5CloneEv
	.type	_ZNK2v88internal6torque22DeleteRangeInstruction5CloneEv, @function
_ZNK2v88internal6torque22DeleteRangeInstruction5CloneEv:
.LFB6752:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movl	$48, %edi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	call	_Znwm@PLT
	movdqu	8(%rbx), %xmm0
	movl	24(%rbx), %edx
	leaq	16+_ZTVN2v88internal6torque22DeleteRangeInstructionE(%rip), %rcx
	movdqu	32(%rbx), %xmm1
	movq	%rcx, (%rax)
	movl	%edx, 24(%rax)
	movups	%xmm0, 8(%rax)
	movups	%xmm1, 32(%rax)
	popq	%rbx
	movq	%rax, (%r12)
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE6752:
	.size	_ZNK2v88internal6torque22DeleteRangeInstruction5CloneEv, .-_ZNK2v88internal6torque22DeleteRangeInstruction5CloneEv
	.section	.text._ZNK2v88internal6torque28PushUninitializedInstruction5CloneEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal6torque28PushUninitializedInstruction5CloneEv
	.type	_ZNK2v88internal6torque28PushUninitializedInstruction5CloneEv, @function
_ZNK2v88internal6torque28PushUninitializedInstruction5CloneEv:
.LFB6758:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movl	$40, %edi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	call	_Znwm@PLT
	movl	24(%rbx), %edx
	movdqu	8(%rbx), %xmm0
	leaq	16+_ZTVN2v88internal6torque28PushUninitializedInstructionE(%rip), %rcx
	movq	%rcx, (%rax)
	movl	%edx, 24(%rax)
	movq	32(%rbx), %rdx
	movups	%xmm0, 8(%rax)
	movq	%rdx, 32(%rax)
	popq	%rbx
	movq	%rax, (%r12)
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE6758:
	.size	_ZNK2v88internal6torque28PushUninitializedInstruction5CloneEv, .-_ZNK2v88internal6torque28PushUninitializedInstruction5CloneEv
	.section	.text._ZNK2v88internal6torque24LoadReferenceInstruction5CloneEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal6torque24LoadReferenceInstruction5CloneEv
	.type	_ZNK2v88internal6torque24LoadReferenceInstruction5CloneEv, @function
_ZNK2v88internal6torque24LoadReferenceInstruction5CloneEv:
.LFB6776:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movl	$40, %edi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	call	_Znwm@PLT
	movl	24(%rbx), %edx
	movdqu	8(%rbx), %xmm0
	leaq	16+_ZTVN2v88internal6torque24LoadReferenceInstructionE(%rip), %rcx
	movq	%rcx, (%rax)
	movl	%edx, 24(%rax)
	movq	32(%rbx), %rdx
	movups	%xmm0, 8(%rax)
	movq	%rdx, 32(%rax)
	popq	%rbx
	movq	%rax, (%r12)
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE6776:
	.size	_ZNK2v88internal6torque24LoadReferenceInstruction5CloneEv, .-_ZNK2v88internal6torque24LoadReferenceInstruction5CloneEv
	.section	.text._ZNK2v88internal6torque25StoreReferenceInstruction5CloneEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal6torque25StoreReferenceInstruction5CloneEv
	.type	_ZNK2v88internal6torque25StoreReferenceInstruction5CloneEv, @function
_ZNK2v88internal6torque25StoreReferenceInstruction5CloneEv:
.LFB6782:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movl	$40, %edi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	call	_Znwm@PLT
	movl	24(%rbx), %edx
	movdqu	8(%rbx), %xmm0
	leaq	16+_ZTVN2v88internal6torque25StoreReferenceInstructionE(%rip), %rcx
	movq	%rcx, (%rax)
	movl	%edx, 24(%rax)
	movq	32(%rbx), %rdx
	movups	%xmm0, 8(%rax)
	movq	%rdx, 32(%rax)
	popq	%rbx
	movq	%rax, (%r12)
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE6782:
	.size	_ZNK2v88internal6torque25StoreReferenceInstruction5CloneEv, .-_ZNK2v88internal6torque25StoreReferenceInstruction5CloneEv
	.section	.text._ZNK2v88internal6torque28NamespaceConstantInstruction5CloneEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal6torque28NamespaceConstantInstruction5CloneEv
	.type	_ZNK2v88internal6torque28NamespaceConstantInstruction5CloneEv, @function
_ZNK2v88internal6torque28NamespaceConstantInstruction5CloneEv:
.LFB6801:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movl	$40, %edi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	call	_Znwm@PLT
	movl	24(%rbx), %edx
	movdqu	8(%rbx), %xmm0
	leaq	16+_ZTVN2v88internal6torque28NamespaceConstantInstructionE(%rip), %rcx
	movq	%rcx, (%rax)
	movl	%edx, 24(%rax)
	movq	32(%rbx), %rdx
	movups	%xmm0, 8(%rax)
	movq	%rdx, 32(%rax)
	popq	%rbx
	movq	%rax, (%r12)
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE6801:
	.size	_ZNK2v88internal6torque28NamespaceConstantInstruction5CloneEv, .-_ZNK2v88internal6torque28NamespaceConstantInstruction5CloneEv
	.section	.text._ZNK2v88internal6torque22CallBuiltinInstruction5CloneEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal6torque22CallBuiltinInstruction5CloneEv
	.type	_ZNK2v88internal6torque22CallBuiltinInstruction5CloneEv, @function
_ZNK2v88internal6torque22CallBuiltinInstruction5CloneEv:
.LFB6813:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movl	$64, %edi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	call	_Znwm@PLT
	movl	24(%rbx), %edx
	movdqu	8(%rbx), %xmm0
	leaq	16+_ZTVN2v88internal6torque22CallBuiltinInstructionE(%rip), %rcx
	movdqu	48(%rbx), %xmm1
	movq	%rcx, (%rax)
	movl	%edx, 24(%rax)
	movzbl	28(%rbx), %edx
	movups	%xmm0, 8(%rax)
	movb	%dl, 28(%rax)
	movq	32(%rbx), %rdx
	movups	%xmm1, 48(%rax)
	movq	%rdx, 32(%rax)
	movq	40(%rbx), %rdx
	movq	%rax, (%r12)
	movq	%rdx, 40(%rax)
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE6813:
	.size	_ZNK2v88internal6torque22CallBuiltinInstruction5CloneEv, .-_ZNK2v88internal6torque22CallBuiltinInstruction5CloneEv
	.section	.text._ZNK2v88internal6torque22CallRuntimeInstruction5CloneEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal6torque22CallRuntimeInstruction5CloneEv
	.type	_ZNK2v88internal6torque22CallRuntimeInstruction5CloneEv, @function
_ZNK2v88internal6torque22CallRuntimeInstruction5CloneEv:
.LFB6819:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movl	$64, %edi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	call	_Znwm@PLT
	movl	24(%rbx), %edx
	movdqu	8(%rbx), %xmm0
	leaq	16+_ZTVN2v88internal6torque22CallRuntimeInstructionE(%rip), %rcx
	movdqu	48(%rbx), %xmm1
	movq	%rcx, (%rax)
	movl	%edx, 24(%rax)
	movzbl	28(%rbx), %edx
	movups	%xmm0, 8(%rax)
	movb	%dl, 28(%rax)
	movq	32(%rbx), %rdx
	movups	%xmm1, 48(%rax)
	movq	%rdx, 32(%rax)
	movq	40(%rbx), %rdx
	movq	%rax, (%r12)
	movq	%rdx, 40(%rax)
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE6819:
	.size	_ZNK2v88internal6torque22CallRuntimeInstruction5CloneEv, .-_ZNK2v88internal6torque22CallRuntimeInstruction5CloneEv
	.section	.text._ZNK2v88internal6torque29CallBuiltinPointerInstruction5CloneEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal6torque29CallBuiltinPointerInstruction5CloneEv
	.type	_ZNK2v88internal6torque29CallBuiltinPointerInstruction5CloneEv, @function
_ZNK2v88internal6torque29CallBuiltinPointerInstruction5CloneEv:
.LFB6825:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movl	$48, %edi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	call	_Znwm@PLT
	movl	24(%rbx), %edx
	movdqu	8(%rbx), %xmm0
	leaq	16+_ZTVN2v88internal6torque29CallBuiltinPointerInstructionE(%rip), %rcx
	movq	%rcx, (%rax)
	movl	%edx, 24(%rax)
	movzbl	28(%rbx), %edx
	movups	%xmm0, 8(%rax)
	movb	%dl, 28(%rax)
	movq	32(%rbx), %rdx
	movq	%rdx, 32(%rax)
	movq	40(%rbx), %rdx
	movq	%rax, (%r12)
	movq	%rdx, 40(%rax)
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE6825:
	.size	_ZNK2v88internal6torque29CallBuiltinPointerInstruction5CloneEv, .-_ZNK2v88internal6torque29CallBuiltinPointerInstruction5CloneEv
	.section	.text._ZNK2v88internal6torque17BranchInstruction5CloneEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal6torque17BranchInstruction5CloneEv
	.type	_ZNK2v88internal6torque17BranchInstruction5CloneEv, @function
_ZNK2v88internal6torque17BranchInstruction5CloneEv:
.LFB6831:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movl	$48, %edi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	call	_Znwm@PLT
	movdqu	8(%rbx), %xmm0
	movl	24(%rbx), %edx
	leaq	16+_ZTVN2v88internal6torque17BranchInstructionE(%rip), %rcx
	movdqu	32(%rbx), %xmm1
	movq	%rcx, (%rax)
	movl	%edx, 24(%rax)
	movups	%xmm0, 8(%rax)
	movups	%xmm1, 32(%rax)
	popq	%rbx
	movq	%rax, (%r12)
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE6831:
	.size	_ZNK2v88internal6torque17BranchInstruction5CloneEv, .-_ZNK2v88internal6torque17BranchInstruction5CloneEv
	.section	.text._ZNK2v88internal6torque15GotoInstruction5CloneEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal6torque15GotoInstruction5CloneEv
	.type	_ZNK2v88internal6torque15GotoInstruction5CloneEv, @function
_ZNK2v88internal6torque15GotoInstruction5CloneEv:
.LFB6843:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movl	$40, %edi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	call	_Znwm@PLT
	movl	24(%rbx), %edx
	movdqu	8(%rbx), %xmm0
	leaq	16+_ZTVN2v88internal6torque15GotoInstructionE(%rip), %rcx
	movq	%rcx, (%rax)
	movl	%edx, 24(%rax)
	movq	32(%rbx), %rdx
	movups	%xmm0, 8(%rax)
	movq	%rdx, 32(%rax)
	popq	%rbx
	movq	%rax, (%r12)
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE6843:
	.size	_ZNK2v88internal6torque15GotoInstruction5CloneEv, .-_ZNK2v88internal6torque15GotoInstruction5CloneEv
	.section	.text._ZNK2v88internal6torque17ReturnInstruction5CloneEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal6torque17ReturnInstruction5CloneEv
	.type	_ZNK2v88internal6torque17ReturnInstruction5CloneEv, @function
_ZNK2v88internal6torque17ReturnInstruction5CloneEv:
.LFB6855:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movl	$32, %edi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	call	_Znwm@PLT
	movdqu	8(%rbx), %xmm0
	movl	24(%rbx), %edx
	leaq	16+_ZTVN2v88internal6torque17ReturnInstructionE(%rip), %rcx
	movq	%rcx, (%rax)
	movl	%edx, 24(%rax)
	movups	%xmm0, 8(%rax)
	popq	%rbx
	movq	%rax, (%r12)
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE6855:
	.size	_ZNK2v88internal6torque17ReturnInstruction5CloneEv, .-_ZNK2v88internal6torque17ReturnInstruction5CloneEv
	.section	.text._ZNK2v88internal6torque21UnsafeCastInstruction5CloneEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal6torque21UnsafeCastInstruction5CloneEv
	.type	_ZNK2v88internal6torque21UnsafeCastInstruction5CloneEv, @function
_ZNK2v88internal6torque21UnsafeCastInstruction5CloneEv:
.LFB6873:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movl	$40, %edi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	call	_Znwm@PLT
	movl	24(%rbx), %edx
	movdqu	8(%rbx), %xmm0
	leaq	16+_ZTVN2v88internal6torque21UnsafeCastInstructionE(%rip), %rcx
	movq	%rcx, (%rax)
	movl	%edx, 24(%rax)
	movq	32(%rbx), %rdx
	movups	%xmm0, 8(%rax)
	movq	%rdx, 32(%rax)
	popq	%rbx
	movq	%rax, (%r12)
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE6873:
	.size	_ZNK2v88internal6torque21UnsafeCastInstruction5CloneEv, .-_ZNK2v88internal6torque21UnsafeCastInstruction5CloneEv
	.section	.text._ZN2v88internal6torque21UnsafeCastInstructionD0Ev,"axG",@progbits,_ZN2v88internal6torque21UnsafeCastInstructionD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque21UnsafeCastInstructionD0Ev
	.type	_ZN2v88internal6torque21UnsafeCastInstructionD0Ev, @function
_ZN2v88internal6torque21UnsafeCastInstructionD0Ev:
.LFB11724:
	.cfi_startproc
	endbr64
	movl	$40, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11724:
	.size	_ZN2v88internal6torque21UnsafeCastInstructionD0Ev, .-_ZN2v88internal6torque21UnsafeCastInstructionD0Ev
	.section	.text._ZN2v88internal6torque17ReturnInstructionD0Ev,"axG",@progbits,_ZN2v88internal6torque17ReturnInstructionD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque17ReturnInstructionD0Ev
	.type	_ZN2v88internal6torque17ReturnInstructionD0Ev, @function
_ZN2v88internal6torque17ReturnInstructionD0Ev:
.LFB11736:
	.cfi_startproc
	endbr64
	movl	$32, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11736:
	.size	_ZN2v88internal6torque17ReturnInstructionD0Ev, .-_ZN2v88internal6torque17ReturnInstructionD0Ev
	.section	.text._ZN2v88internal6torque15GotoInstructionD0Ev,"axG",@progbits,_ZN2v88internal6torque15GotoInstructionD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque15GotoInstructionD0Ev
	.type	_ZN2v88internal6torque15GotoInstructionD0Ev, @function
_ZN2v88internal6torque15GotoInstructionD0Ev:
.LFB11744:
	.cfi_startproc
	endbr64
	movl	$40, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11744:
	.size	_ZN2v88internal6torque15GotoInstructionD0Ev, .-_ZN2v88internal6torque15GotoInstructionD0Ev
	.section	.text._ZN2v88internal6torque17BranchInstructionD0Ev,"axG",@progbits,_ZN2v88internal6torque17BranchInstructionD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque17BranchInstructionD0Ev
	.type	_ZN2v88internal6torque17BranchInstructionD0Ev, @function
_ZN2v88internal6torque17BranchInstructionD0Ev:
.LFB11752:
	.cfi_startproc
	endbr64
	movl	$48, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11752:
	.size	_ZN2v88internal6torque17BranchInstructionD0Ev, .-_ZN2v88internal6torque17BranchInstructionD0Ev
	.section	.text._ZN2v88internal6torque29CallBuiltinPointerInstructionD0Ev,"axG",@progbits,_ZN2v88internal6torque29CallBuiltinPointerInstructionD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque29CallBuiltinPointerInstructionD0Ev
	.type	_ZN2v88internal6torque29CallBuiltinPointerInstructionD0Ev, @function
_ZN2v88internal6torque29CallBuiltinPointerInstructionD0Ev:
.LFB11756:
	.cfi_startproc
	endbr64
	movl	$48, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11756:
	.size	_ZN2v88internal6torque29CallBuiltinPointerInstructionD0Ev, .-_ZN2v88internal6torque29CallBuiltinPointerInstructionD0Ev
	.section	.text._ZN2v88internal6torque22CallRuntimeInstructionD0Ev,"axG",@progbits,_ZN2v88internal6torque22CallRuntimeInstructionD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque22CallRuntimeInstructionD0Ev
	.type	_ZN2v88internal6torque22CallRuntimeInstructionD0Ev, @function
_ZN2v88internal6torque22CallRuntimeInstructionD0Ev:
.LFB11760:
	.cfi_startproc
	endbr64
	movl	$64, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11760:
	.size	_ZN2v88internal6torque22CallRuntimeInstructionD0Ev, .-_ZN2v88internal6torque22CallRuntimeInstructionD0Ev
	.section	.text._ZN2v88internal6torque22CallBuiltinInstructionD0Ev,"axG",@progbits,_ZN2v88internal6torque22CallBuiltinInstructionD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque22CallBuiltinInstructionD0Ev
	.type	_ZN2v88internal6torque22CallBuiltinInstructionD0Ev, @function
_ZN2v88internal6torque22CallBuiltinInstructionD0Ev:
.LFB11764:
	.cfi_startproc
	endbr64
	movl	$64, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11764:
	.size	_ZN2v88internal6torque22CallBuiltinInstructionD0Ev, .-_ZN2v88internal6torque22CallBuiltinInstructionD0Ev
	.section	.text._ZN2v88internal6torque28NamespaceConstantInstructionD0Ev,"axG",@progbits,_ZN2v88internal6torque28NamespaceConstantInstructionD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque28NamespaceConstantInstructionD0Ev
	.type	_ZN2v88internal6torque28NamespaceConstantInstructionD0Ev, @function
_ZN2v88internal6torque28NamespaceConstantInstructionD0Ev:
.LFB11772:
	.cfi_startproc
	endbr64
	movl	$40, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11772:
	.size	_ZN2v88internal6torque28NamespaceConstantInstructionD0Ev, .-_ZN2v88internal6torque28NamespaceConstantInstructionD0Ev
	.section	.text._ZN2v88internal6torque25StoreReferenceInstructionD0Ev,"axG",@progbits,_ZN2v88internal6torque25StoreReferenceInstructionD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque25StoreReferenceInstructionD0Ev
	.type	_ZN2v88internal6torque25StoreReferenceInstructionD0Ev, @function
_ZN2v88internal6torque25StoreReferenceInstructionD0Ev:
.LFB11784:
	.cfi_startproc
	endbr64
	movl	$40, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11784:
	.size	_ZN2v88internal6torque25StoreReferenceInstructionD0Ev, .-_ZN2v88internal6torque25StoreReferenceInstructionD0Ev
	.section	.text._ZN2v88internal6torque24LoadReferenceInstructionD0Ev,"axG",@progbits,_ZN2v88internal6torque24LoadReferenceInstructionD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque24LoadReferenceInstructionD0Ev
	.type	_ZN2v88internal6torque24LoadReferenceInstructionD0Ev, @function
_ZN2v88internal6torque24LoadReferenceInstructionD0Ev:
.LFB11788:
	.cfi_startproc
	endbr64
	movl	$40, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11788:
	.size	_ZN2v88internal6torque24LoadReferenceInstructionD0Ev, .-_ZN2v88internal6torque24LoadReferenceInstructionD0Ev
	.section	.text._ZN2v88internal6torque28PushUninitializedInstructionD0Ev,"axG",@progbits,_ZN2v88internal6torque28PushUninitializedInstructionD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque28PushUninitializedInstructionD0Ev
	.type	_ZN2v88internal6torque28PushUninitializedInstructionD0Ev, @function
_ZN2v88internal6torque28PushUninitializedInstructionD0Ev:
.LFB11800:
	.cfi_startproc
	endbr64
	movl	$40, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11800:
	.size	_ZN2v88internal6torque28PushUninitializedInstructionD0Ev, .-_ZN2v88internal6torque28PushUninitializedInstructionD0Ev
	.section	.text._ZN2v88internal6torque22DeleteRangeInstructionD0Ev,"axG",@progbits,_ZN2v88internal6torque22DeleteRangeInstructionD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque22DeleteRangeInstructionD0Ev
	.type	_ZN2v88internal6torque22DeleteRangeInstructionD0Ev, @function
_ZN2v88internal6torque22DeleteRangeInstructionD0Ev:
.LFB11804:
	.cfi_startproc
	endbr64
	movl	$48, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11804:
	.size	_ZN2v88internal6torque22DeleteRangeInstructionD0Ev, .-_ZN2v88internal6torque22DeleteRangeInstructionD0Ev
	.section	.text._ZN2v88internal6torque15PokeInstructionD0Ev,"axG",@progbits,_ZN2v88internal6torque15PokeInstructionD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque15PokeInstructionD0Ev
	.type	_ZN2v88internal6torque15PokeInstructionD0Ev, @function
_ZN2v88internal6torque15PokeInstructionD0Ev:
.LFB11808:
	.cfi_startproc
	endbr64
	movl	$56, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11808:
	.size	_ZN2v88internal6torque15PokeInstructionD0Ev, .-_ZN2v88internal6torque15PokeInstructionD0Ev
	.section	.text._ZN2v88internal6torque15PeekInstructionD0Ev,"axG",@progbits,_ZN2v88internal6torque15PeekInstructionD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque15PeekInstructionD0Ev
	.type	_ZN2v88internal6torque15PeekInstructionD0Ev, @function
_ZN2v88internal6torque15PeekInstructionD0Ev:
.LFB11812:
	.cfi_startproc
	endbr64
	movl	$56, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11812:
	.size	_ZN2v88internal6torque15PeekInstructionD0Ev, .-_ZN2v88internal6torque15PeekInstructionD0Ev
	.section	.text._ZN2v88internal6torque16AbortInstructionD2Ev,"axG",@progbits,_ZN2v88internal6torque16AbortInstructionD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque16AbortInstructionD2Ev
	.type	_ZN2v88internal6torque16AbortInstructionD2Ev, @function
_ZN2v88internal6torque16AbortInstructionD2Ev:
.LFB11726:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %r8
	leaq	16+_ZTVN2v88internal6torque16AbortInstructionE(%rip), %rax
	addq	$48, %rdi
	movq	%rax, -48(%rdi)
	cmpq	%rdi, %r8
	je	.L85
	movq	%r8, %rdi
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L85:
	ret
	.cfi_endproc
.LFE11726:
	.size	_ZN2v88internal6torque16AbortInstructionD2Ev, .-_ZN2v88internal6torque16AbortInstructionD2Ev
	.weak	_ZN2v88internal6torque16AbortInstructionD1Ev
	.set	_ZN2v88internal6torque16AbortInstructionD1Ev,_ZN2v88internal6torque16AbortInstructionD2Ev
	.section	.text._ZN2v88internal6torque16AbortInstructionD0Ev,"axG",@progbits,_ZN2v88internal6torque16AbortInstructionD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque16AbortInstructionD0Ev
	.type	_ZN2v88internal6torque16AbortInstructionD0Ev, @function
_ZN2v88internal6torque16AbortInstructionD0Ev:
.LFB11728:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal6torque16AbortInstructionE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	32(%rdi), %rdi
	leaq	48(%r12), %rax
	cmpq	%rax, %rdi
	je	.L88
	call	_ZdlPv@PLT
.L88:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$64, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11728:
	.size	_ZN2v88internal6torque16AbortInstructionD0Ev, .-_ZN2v88internal6torque16AbortInstructionD0Ev
	.section	.text._ZN2v88internal6torque30PrintConstantStringInstructionD2Ev,"axG",@progbits,_ZN2v88internal6torque30PrintConstantStringInstructionD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque30PrintConstantStringInstructionD2Ev
	.type	_ZN2v88internal6torque30PrintConstantStringInstructionD2Ev, @function
_ZN2v88internal6torque30PrintConstantStringInstructionD2Ev:
.LFB11730:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %r8
	leaq	16+_ZTVN2v88internal6torque30PrintConstantStringInstructionE(%rip), %rax
	addq	$48, %rdi
	movq	%rax, -48(%rdi)
	cmpq	%rdi, %r8
	je	.L90
	movq	%r8, %rdi
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L90:
	ret
	.cfi_endproc
.LFE11730:
	.size	_ZN2v88internal6torque30PrintConstantStringInstructionD2Ev, .-_ZN2v88internal6torque30PrintConstantStringInstructionD2Ev
	.weak	_ZN2v88internal6torque30PrintConstantStringInstructionD1Ev
	.set	_ZN2v88internal6torque30PrintConstantStringInstructionD1Ev,_ZN2v88internal6torque30PrintConstantStringInstructionD2Ev
	.section	.text._ZN2v88internal6torque30PrintConstantStringInstructionD0Ev,"axG",@progbits,_ZN2v88internal6torque30PrintConstantStringInstructionD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque30PrintConstantStringInstructionD0Ev
	.type	_ZN2v88internal6torque30PrintConstantStringInstructionD0Ev, @function
_ZN2v88internal6torque30PrintConstantStringInstructionD0Ev:
.LFB11732:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal6torque30PrintConstantStringInstructionE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	32(%rdi), %rdi
	leaq	48(%r12), %rax
	cmpq	%rax, %rdi
	je	.L93
	call	_ZdlPv@PLT
.L93:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$64, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11732:
	.size	_ZN2v88internal6torque30PrintConstantStringInstructionD0Ev, .-_ZN2v88internal6torque30PrintConstantStringInstructionD0Ev
	.section	.text._ZN2v88internal6torque26ConstexprBranchInstructionD2Ev,"axG",@progbits,_ZN2v88internal6torque26ConstexprBranchInstructionD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque26ConstexprBranchInstructionD2Ev
	.type	_ZN2v88internal6torque26ConstexprBranchInstructionD2Ev, @function
_ZN2v88internal6torque26ConstexprBranchInstructionD2Ev:
.LFB11746:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %r8
	leaq	16+_ZTVN2v88internal6torque26ConstexprBranchInstructionE(%rip), %rax
	addq	$48, %rdi
	movq	%rax, -48(%rdi)
	cmpq	%rdi, %r8
	je	.L95
	movq	%r8, %rdi
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L95:
	ret
	.cfi_endproc
.LFE11746:
	.size	_ZN2v88internal6torque26ConstexprBranchInstructionD2Ev, .-_ZN2v88internal6torque26ConstexprBranchInstructionD2Ev
	.weak	_ZN2v88internal6torque26ConstexprBranchInstructionD1Ev
	.set	_ZN2v88internal6torque26ConstexprBranchInstructionD1Ev,_ZN2v88internal6torque26ConstexprBranchInstructionD2Ev
	.section	.text._ZN2v88internal6torque26ConstexprBranchInstructionD0Ev,"axG",@progbits,_ZN2v88internal6torque26ConstexprBranchInstructionD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque26ConstexprBranchInstructionD0Ev
	.type	_ZN2v88internal6torque26ConstexprBranchInstructionD0Ev, @function
_ZN2v88internal6torque26ConstexprBranchInstructionD0Ev:
.LFB11748:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal6torque26ConstexprBranchInstructionE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	32(%rdi), %rdi
	leaq	48(%r12), %rax
	cmpq	%rax, %rdi
	je	.L98
	call	_ZdlPv@PLT
.L98:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$80, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11748:
	.size	_ZN2v88internal6torque26ConstexprBranchInstructionD0Ev, .-_ZN2v88internal6torque26ConstexprBranchInstructionD0Ev
	.section	.text._ZN2v88internal6torque31CreateFieldReferenceInstructionD2Ev,"axG",@progbits,_ZN2v88internal6torque31CreateFieldReferenceInstructionD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque31CreateFieldReferenceInstructionD2Ev
	.type	_ZN2v88internal6torque31CreateFieldReferenceInstructionD2Ev, @function
_ZN2v88internal6torque31CreateFieldReferenceInstructionD2Ev:
.LFB11790:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %r8
	leaq	16+_ZTVN2v88internal6torque31CreateFieldReferenceInstructionE(%rip), %rax
	addq	$56, %rdi
	movq	%rax, -56(%rdi)
	cmpq	%rdi, %r8
	je	.L100
	movq	%r8, %rdi
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L100:
	ret
	.cfi_endproc
.LFE11790:
	.size	_ZN2v88internal6torque31CreateFieldReferenceInstructionD2Ev, .-_ZN2v88internal6torque31CreateFieldReferenceInstructionD2Ev
	.weak	_ZN2v88internal6torque31CreateFieldReferenceInstructionD1Ev
	.set	_ZN2v88internal6torque31CreateFieldReferenceInstructionD1Ev,_ZN2v88internal6torque31CreateFieldReferenceInstructionD2Ev
	.section	.text._ZN2v88internal6torque31CreateFieldReferenceInstructionD0Ev,"axG",@progbits,_ZN2v88internal6torque31CreateFieldReferenceInstructionD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque31CreateFieldReferenceInstructionD0Ev
	.type	_ZN2v88internal6torque31CreateFieldReferenceInstructionD0Ev, @function
_ZN2v88internal6torque31CreateFieldReferenceInstructionD0Ev:
.LFB11792:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal6torque31CreateFieldReferenceInstructionE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	40(%rdi), %rdi
	leaq	56(%r12), %rax
	cmpq	%rax, %rdi
	je	.L103
	call	_ZdlPv@PLT
.L103:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$72, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11792:
	.size	_ZN2v88internal6torque31CreateFieldReferenceInstructionD0Ev, .-_ZN2v88internal6torque31CreateFieldReferenceInstructionD0Ev
	.section	.text._ZN2v88internal6torque29PushBuiltinPointerInstructionD2Ev,"axG",@progbits,_ZN2v88internal6torque29PushBuiltinPointerInstructionD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque29PushBuiltinPointerInstructionD2Ev
	.type	_ZN2v88internal6torque29PushBuiltinPointerInstructionD2Ev, @function
_ZN2v88internal6torque29PushBuiltinPointerInstructionD2Ev:
.LFB11794:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %r8
	leaq	16+_ZTVN2v88internal6torque29PushBuiltinPointerInstructionE(%rip), %rax
	addq	$48, %rdi
	movq	%rax, -48(%rdi)
	cmpq	%rdi, %r8
	je	.L105
	movq	%r8, %rdi
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L105:
	ret
	.cfi_endproc
.LFE11794:
	.size	_ZN2v88internal6torque29PushBuiltinPointerInstructionD2Ev, .-_ZN2v88internal6torque29PushBuiltinPointerInstructionD2Ev
	.weak	_ZN2v88internal6torque29PushBuiltinPointerInstructionD1Ev
	.set	_ZN2v88internal6torque29PushBuiltinPointerInstructionD1Ev,_ZN2v88internal6torque29PushBuiltinPointerInstructionD2Ev
	.section	.text._ZN2v88internal6torque29PushBuiltinPointerInstructionD0Ev,"axG",@progbits,_ZN2v88internal6torque29PushBuiltinPointerInstructionD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque29PushBuiltinPointerInstructionD0Ev
	.type	_ZN2v88internal6torque29PushBuiltinPointerInstructionD0Ev, @function
_ZN2v88internal6torque29PushBuiltinPointerInstructionD0Ev:
.LFB11796:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal6torque29PushBuiltinPointerInstructionE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	32(%rdi), %rdi
	leaq	48(%r12), %rax
	cmpq	%rax, %rdi
	je	.L108
	call	_ZdlPv@PLT
.L108:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$72, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11796:
	.size	_ZN2v88internal6torque29PushBuiltinPointerInstructionD0Ev, .-_ZN2v88internal6torque29PushBuiltinPointerInstructionD0Ev
	.section	.text._ZN2v88internal6torque29PushBuiltinPointerInstruction6AssignERKNS1_15InstructionBaseE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque29PushBuiltinPointerInstruction6AssignERKNS1_15InstructionBaseE
	.type	_ZN2v88internal6torque29PushBuiltinPointerInstruction6AssignERKNS1_15InstructionBaseE, @function
_ZN2v88internal6torque29PushBuiltinPointerInstruction6AssignERKNS1_15InstructionBaseE:
.LFB6768:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	leaq	32(%rsi), %rsi
	pushq	%rbx
	.cfi_offset 3, -32
	movdqu	-24(%rsi), %xmm0
	movq	%rdi, %rbx
	leaq	32(%rdi), %rdi
	movups	%xmm0, -24(%rdi)
	movl	-8(%rsi), %eax
	movl	%eax, -8(%rdi)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	movq	64(%r12), %rax
	movq	%rax, 64(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE6768:
	.size	_ZN2v88internal6torque29PushBuiltinPointerInstruction6AssignERKNS1_15InstructionBaseE, .-_ZN2v88internal6torque29PushBuiltinPointerInstruction6AssignERKNS1_15InstructionBaseE
	.section	.text._ZN2v88internal6torque31CreateFieldReferenceInstruction6AssignERKNS1_15InstructionBaseE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque31CreateFieldReferenceInstruction6AssignERKNS1_15InstructionBaseE
	.type	_ZN2v88internal6torque31CreateFieldReferenceInstruction6AssignERKNS1_15InstructionBaseE, @function
_ZN2v88internal6torque31CreateFieldReferenceInstruction6AssignERKNS1_15InstructionBaseE:
.LFB6774:
	.cfi_startproc
	endbr64
	movdqu	8(%rsi), %xmm0
	addq	$40, %rdi
	addq	$40, %rsi
	movups	%xmm0, -32(%rdi)
	movl	-16(%rsi), %eax
	movl	%eax, -16(%rdi)
	movq	-8(%rsi), %rax
	movq	%rax, -8(%rdi)
	jmp	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	.cfi_endproc
.LFE6774:
	.size	_ZN2v88internal6torque31CreateFieldReferenceInstruction6AssignERKNS1_15InstructionBaseE, .-_ZN2v88internal6torque31CreateFieldReferenceInstruction6AssignERKNS1_15InstructionBaseE
	.section	.text._ZN2v88internal6torque26ConstexprBranchInstruction6AssignERKNS1_15InstructionBaseE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque26ConstexprBranchInstruction6AssignERKNS1_15InstructionBaseE
	.type	_ZN2v88internal6torque26ConstexprBranchInstruction6AssignERKNS1_15InstructionBaseE, @function
_ZN2v88internal6torque26ConstexprBranchInstruction6AssignERKNS1_15InstructionBaseE:
.LFB6841:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	leaq	32(%rsi), %rsi
	pushq	%rbx
	.cfi_offset 3, -32
	movdqu	-24(%rsi), %xmm0
	movq	%rdi, %rbx
	leaq	32(%rdi), %rdi
	movups	%xmm0, -24(%rdi)
	movl	-8(%rsi), %eax
	movl	%eax, -8(%rdi)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	movq	64(%r12), %rax
	movq	%rax, 64(%rbx)
	movq	72(%r12), %rax
	movq	%rax, 72(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE6841:
	.size	_ZN2v88internal6torque26ConstexprBranchInstruction6AssignERKNS1_15InstructionBaseE, .-_ZN2v88internal6torque26ConstexprBranchInstruction6AssignERKNS1_15InstructionBaseE
	.section	.text._ZN2v88internal6torque30PrintConstantStringInstruction6AssignERKNS1_15InstructionBaseE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque30PrintConstantStringInstruction6AssignERKNS1_15InstructionBaseE
	.type	_ZN2v88internal6torque30PrintConstantStringInstruction6AssignERKNS1_15InstructionBaseE, @function
_ZN2v88internal6torque30PrintConstantStringInstruction6AssignERKNS1_15InstructionBaseE:
.LFB6865:
	.cfi_startproc
	endbr64
	movdqu	8(%rsi), %xmm0
	addq	$32, %rdi
	addq	$32, %rsi
	movups	%xmm0, -24(%rdi)
	movl	-8(%rsi), %eax
	movl	%eax, -8(%rdi)
	jmp	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	.cfi_endproc
.LFE6865:
	.size	_ZN2v88internal6torque30PrintConstantStringInstruction6AssignERKNS1_15InstructionBaseE, .-_ZN2v88internal6torque30PrintConstantStringInstruction6AssignERKNS1_15InstructionBaseE
	.section	.text._ZN2v88internal6torque16AbortInstruction6AssignERKNS1_15InstructionBaseE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque16AbortInstruction6AssignERKNS1_15InstructionBaseE
	.type	_ZN2v88internal6torque16AbortInstruction6AssignERKNS1_15InstructionBaseE, @function
_ZN2v88internal6torque16AbortInstruction6AssignERKNS1_15InstructionBaseE:
.LFB6871:
	.cfi_startproc
	endbr64
	movdqu	8(%rsi), %xmm0
	addq	$32, %rdi
	addq	$32, %rsi
	movups	%xmm0, -24(%rdi)
	movl	-8(%rsi), %eax
	movl	%eax, -8(%rdi)
	movl	-4(%rsi), %eax
	movl	%eax, -4(%rdi)
	jmp	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	.cfi_endproc
.LFE6871:
	.size	_ZN2v88internal6torque16AbortInstruction6AssignERKNS1_15InstructionBaseE, .-_ZN2v88internal6torque16AbortInstruction6AssignERKNS1_15InstructionBaseE
	.section	.text._ZNK2v88internal6torque7TopType29GetGeneratedTNodeTypeNameImplB5cxx11Ev,"axG",@progbits,_ZNK2v88internal6torque7TopType29GetGeneratedTNodeTypeNameImplB5cxx11Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal6torque7TopType29GetGeneratedTNodeTypeNameImplB5cxx11Ev
	.type	_ZNK2v88internal6torque7TopType29GetGeneratedTNodeTypeNameImplB5cxx11Ev, @function
_ZNK2v88internal6torque7TopType29GetGeneratedTNodeTypeNameImplB5cxx11Ev:
.LFB5469:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	104(%rsi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal6torque4Type25GetGeneratedTNodeTypeNameB5cxx11Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L120
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L120:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5469:
	.size	_ZNK2v88internal6torque7TopType29GetGeneratedTNodeTypeNameImplB5cxx11Ev, .-_ZNK2v88internal6torque7TopType29GetGeneratedTNodeTypeNameImplB5cxx11Ev
	.section	.rodata._ZNK2v88internal6torque7TopType24GetGeneratedTypeNameImplB5cxx11Ev.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZNK2v88internal6torque7TopType24GetGeneratedTypeNameImplB5cxx11Ev,"axG",@progbits,_ZNK2v88internal6torque7TopType24GetGeneratedTypeNameImplB5cxx11Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal6torque7TopType24GetGeneratedTypeNameImplB5cxx11Ev
	.type	_ZNK2v88internal6torque7TopType24GetGeneratedTypeNameImplB5cxx11Ev, @function
_ZNK2v88internal6torque7TopType24GetGeneratedTypeNameImplB5cxx11Ev:
.LFB5468:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE5468:
	.size	_ZNK2v88internal6torque7TopType24GetGeneratedTypeNameImplB5cxx11Ev, .-_ZNK2v88internal6torque7TopType24GetGeneratedTypeNameImplB5cxx11Ev
	.section	.text._ZNK2v88internal6torque26ConstexprBranchInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal6torque26ConstexprBranchInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.type	_ZNK2v88internal6torque26ConstexprBranchInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE, @function
_ZNK2v88internal6torque26ConstexprBranchInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE:
.LFB6895:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	64(%rdi), %rdi
	call	_ZN2v88internal6torque5Block13SetInputTypesERKNS1_5StackIPKNS1_4TypeEEE@PLT
	movq	72(%rbx), %rdi
	movq	%r12, %rsi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal6torque5Block13SetInputTypesERKNS1_5StackIPKNS1_4TypeEEE@PLT
	.cfi_endproc
.LFE6895:
	.size	_ZNK2v88internal6torque26ConstexprBranchInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE, .-_ZNK2v88internal6torque26ConstexprBranchInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.section	.text._ZNK2v88internal6torque15GotoInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal6torque15GotoInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.type	_ZNK2v88internal6torque15GotoInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE, @function
_ZNK2v88internal6torque15GotoInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE:
.LFB6896:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rdi
	jmp	_ZN2v88internal6torque5Block13SetInputTypesERKNS1_5StackIPKNS1_4TypeEEE@PLT
	.cfi_endproc
.LFE6896:
	.size	_ZNK2v88internal6torque15GotoInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE, .-_ZNK2v88internal6torque15GotoInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.section	.text._ZN2v88internal6torque15PeekInstruction6AssignERKNS1_15InstructionBaseE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque15PeekInstruction6AssignERKNS1_15InstructionBaseE
	.type	_ZN2v88internal6torque15PeekInstruction6AssignERKNS1_15InstructionBaseE, @function
_ZN2v88internal6torque15PeekInstruction6AssignERKNS1_15InstructionBaseE:
.LFB6742:
	.cfi_startproc
	endbr64
	movdqu	8(%rsi), %xmm0
	movups	%xmm0, 8(%rdi)
	movl	24(%rsi), %eax
	movl	%eax, 24(%rdi)
	movq	32(%rsi), %rax
	movq	%rax, 32(%rdi)
	cmpb	$0, 40(%rsi)
	movzbl	40(%rdi), %eax
	je	.L127
	movq	48(%rsi), %rdx
	movq	%rdx, 48(%rdi)
	testb	%al, %al
	je	.L128
	ret
	.p2align 4,,10
	.p2align 3
.L127:
	testb	%al, %al
	je	.L126
	movb	$0, 40(%rdi)
.L126:
	ret
	.p2align 4,,10
	.p2align 3
.L128:
	movb	$1, 40(%rdi)
	ret
	.cfi_endproc
.LFE6742:
	.size	_ZN2v88internal6torque15PeekInstruction6AssignERKNS1_15InstructionBaseE, .-_ZN2v88internal6torque15PeekInstruction6AssignERKNS1_15InstructionBaseE
	.section	.text._ZN2v88internal6torque15PokeInstruction6AssignERKNS1_15InstructionBaseE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque15PokeInstruction6AssignERKNS1_15InstructionBaseE
	.type	_ZN2v88internal6torque15PokeInstruction6AssignERKNS1_15InstructionBaseE, @function
_ZN2v88internal6torque15PokeInstruction6AssignERKNS1_15InstructionBaseE:
.LFB6750:
	.cfi_startproc
	endbr64
	movdqu	8(%rsi), %xmm0
	movups	%xmm0, 8(%rdi)
	movl	24(%rsi), %eax
	movl	%eax, 24(%rdi)
	movq	32(%rsi), %rax
	movq	%rax, 32(%rdi)
	cmpb	$0, 40(%rsi)
	movzbl	40(%rdi), %eax
	je	.L134
	movq	48(%rsi), %rdx
	movq	%rdx, 48(%rdi)
	testb	%al, %al
	je	.L135
	ret
	.p2align 4,,10
	.p2align 3
.L134:
	testb	%al, %al
	je	.L133
	movb	$0, 40(%rdi)
.L133:
	ret
	.p2align 4,,10
	.p2align 3
.L135:
	movb	$1, 40(%rdi)
	ret
	.cfi_endproc
.LFE6750:
	.size	_ZN2v88internal6torque15PokeInstruction6AssignERKNS1_15InstructionBaseE, .-_ZN2v88internal6torque15PokeInstruction6AssignERKNS1_15InstructionBaseE
	.section	.text._ZN2v88internal6torque22CallBuiltinInstruction6AssignERKNS1_15InstructionBaseE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque22CallBuiltinInstruction6AssignERKNS1_15InstructionBaseE
	.type	_ZN2v88internal6torque22CallBuiltinInstruction6AssignERKNS1_15InstructionBaseE, @function
_ZN2v88internal6torque22CallBuiltinInstruction6AssignERKNS1_15InstructionBaseE:
.LFB6817:
	.cfi_startproc
	endbr64
	movdqu	8(%rsi), %xmm0
	movups	%xmm0, 8(%rdi)
	movl	24(%rsi), %eax
	movl	%eax, 24(%rdi)
	movzbl	28(%rsi), %eax
	movb	%al, 28(%rdi)
	movq	32(%rsi), %rax
	movq	%rax, 32(%rdi)
	movq	40(%rsi), %rax
	movq	%rax, 40(%rdi)
	cmpb	$0, 48(%rsi)
	movzbl	48(%rdi), %eax
	je	.L141
	movq	56(%rsi), %rdx
	movq	%rdx, 56(%rdi)
	testb	%al, %al
	je	.L142
	ret
	.p2align 4,,10
	.p2align 3
.L141:
	testb	%al, %al
	je	.L140
	movb	$0, 48(%rdi)
.L140:
	ret
	.p2align 4,,10
	.p2align 3
.L142:
	movb	$1, 48(%rdi)
	ret
	.cfi_endproc
.LFE6817:
	.size	_ZN2v88internal6torque22CallBuiltinInstruction6AssignERKNS1_15InstructionBaseE, .-_ZN2v88internal6torque22CallBuiltinInstruction6AssignERKNS1_15InstructionBaseE
	.section	.text._ZN2v88internal6torque22CallRuntimeInstruction6AssignERKNS1_15InstructionBaseE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque22CallRuntimeInstruction6AssignERKNS1_15InstructionBaseE
	.type	_ZN2v88internal6torque22CallRuntimeInstruction6AssignERKNS1_15InstructionBaseE, @function
_ZN2v88internal6torque22CallRuntimeInstruction6AssignERKNS1_15InstructionBaseE:
.LFB6823:
	.cfi_startproc
	endbr64
	movdqu	8(%rsi), %xmm0
	movups	%xmm0, 8(%rdi)
	movl	24(%rsi), %eax
	movl	%eax, 24(%rdi)
	movzbl	28(%rsi), %eax
	movb	%al, 28(%rdi)
	movq	32(%rsi), %rax
	movq	%rax, 32(%rdi)
	movq	40(%rsi), %rax
	movq	%rax, 40(%rdi)
	cmpb	$0, 48(%rsi)
	movzbl	48(%rdi), %eax
	je	.L148
	movq	56(%rsi), %rdx
	movq	%rdx, 56(%rdi)
	testb	%al, %al
	je	.L149
	ret
	.p2align 4,,10
	.p2align 3
.L148:
	testb	%al, %al
	je	.L147
	movb	$0, 48(%rdi)
.L147:
	ret
	.p2align 4,,10
	.p2align 3
.L149:
	movb	$1, 48(%rdi)
	ret
	.cfi_endproc
.LFE6823:
	.size	_ZN2v88internal6torque22CallRuntimeInstruction6AssignERKNS1_15InstructionBaseE, .-_ZN2v88internal6torque22CallRuntimeInstruction6AssignERKNS1_15InstructionBaseE
	.section	.rodata._ZNK2v88internal6torque21UnsafeCastInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"vector::_M_range_check: __n (which is %zu) >= this->size() (which is %zu)"
	.section	.text._ZNK2v88internal6torque21UnsafeCastInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal6torque21UnsafeCastInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.type	_ZNK2v88internal6torque21UnsafeCastInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE, @function
_ZNK2v88internal6torque21UnsafeCastInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE:
.LFB6901:
	.cfi_startproc
	endbr64
	movq	8(%rsi), %rax
	movq	32(%rdi), %rcx
	movq	%rax, %rdx
	subq	(%rsi), %rdx
	shrq	$3, %rdx
	je	.L159
	movq	%rcx, -8(%rax)
	ret
.L159:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	orq	$-1, %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE6901:
	.size	_ZNK2v88internal6torque21UnsafeCastInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE, .-_ZNK2v88internal6torque21UnsafeCastInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.section	.text._ZNK2v88internal6torque7TopType11MangledNameB5cxx11Ev,"axG",@progbits,_ZNK2v88internal6torque7TopType11MangledNameB5cxx11Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal6torque7TopType11MangledNameB5cxx11Ev
	.type	_ZNK2v88internal6torque7TopType11MangledNameB5cxx11Ev, @function
_ZNK2v88internal6torque7TopType11MangledNameB5cxx11Ev:
.LFB5467:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rdx
	movb	$112, 18(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$28532, %edx
	movw	%dx, 16(%rdi)
	movq	$3, 8(%rdi)
	movb	$0, 19(%rdi)
	ret
	.cfi_endproc
.LFE5467:
	.size	_ZNK2v88internal6torque7TopType11MangledNameB5cxx11Ev, .-_ZNK2v88internal6torque7TopType11MangledNameB5cxx11Ev
	.section	.text._ZN2v88internal6torque23CallCsaMacroInstructionD2Ev,"axG",@progbits,_ZN2v88internal6torque23CallCsaMacroInstructionD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque23CallCsaMacroInstructionD2Ev
	.type	_ZN2v88internal6torque23CallCsaMacroInstructionD2Ev, @function
_ZN2v88internal6torque23CallCsaMacroInstructionD2Ev:
.LFB11778:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal6torque23CallCsaMacroInstructionE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	48(%rdi), %r13
	movq	40(%rdi), %r12
	movq	%rax, (%rdi)
	cmpq	%r12, %r13
	je	.L162
	.p2align 4,,10
	.p2align 3
.L166:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L163
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r13, %r12
	jne	.L166
.L164:
	movq	40(%rbx), %r12
.L162:
	testq	%r12, %r12
	je	.L161
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L163:
	.cfi_restore_state
	addq	$32, %r12
	cmpq	%r12, %r13
	jne	.L166
	jmp	.L164
	.p2align 4,,10
	.p2align 3
.L161:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11778:
	.size	_ZN2v88internal6torque23CallCsaMacroInstructionD2Ev, .-_ZN2v88internal6torque23CallCsaMacroInstructionD2Ev
	.weak	_ZN2v88internal6torque23CallCsaMacroInstructionD1Ev
	.set	_ZN2v88internal6torque23CallCsaMacroInstructionD1Ev,_ZN2v88internal6torque23CallCsaMacroInstructionD2Ev
	.section	.text._ZN2v88internal6torque23GotoExternalInstructionD0Ev,"axG",@progbits,_ZN2v88internal6torque23GotoExternalInstructionD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque23GotoExternalInstructionD0Ev
	.type	_ZN2v88internal6torque23GotoExternalInstructionD0Ev, @function
_ZN2v88internal6torque23GotoExternalInstructionD0Ev:
.LFB11740:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal6torque23GotoExternalInstructionE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	72(%rdi), %rbx
	movq	64(%rdi), %r12
	movq	%rax, (%rdi)
	cmpq	%r12, %rbx
	je	.L170
	.p2align 4,,10
	.p2align 3
.L174:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L171
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%rbx, %r12
	jne	.L174
.L172:
	movq	64(%r13), %r12
.L170:
	testq	%r12, %r12
	je	.L175
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L175:
	movq	32(%r13), %rdi
	leaq	48(%r13), %rax
	cmpq	%rax, %rdi
	je	.L176
	call	_ZdlPv@PLT
.L176:
	addq	$8, %rsp
	movq	%r13, %rdi
	movl	$88, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L171:
	.cfi_restore_state
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L174
	jmp	.L172
	.cfi_endproc
.LFE11740:
	.size	_ZN2v88internal6torque23GotoExternalInstructionD0Ev, .-_ZN2v88internal6torque23GotoExternalInstructionD0Ev
	.section	.text._ZN2v88internal6torque32CallCsaMacroAndBranchInstructionD0Ev,"axG",@progbits,_ZN2v88internal6torque32CallCsaMacroAndBranchInstructionD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque32CallCsaMacroAndBranchInstructionD0Ev
	.type	_ZN2v88internal6torque32CallCsaMacroAndBranchInstructionD0Ev, @function
_ZN2v88internal6torque32CallCsaMacroAndBranchInstructionD0Ev:
.LFB11768:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal6torque32CallCsaMacroAndBranchInstructionE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rax, (%rdi)
	movq	80(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L182
	call	_ZdlPv@PLT
.L182:
	movq	48(%r13), %rbx
	movq	40(%r13), %r12
	cmpq	%r12, %rbx
	je	.L183
	.p2align 4,,10
	.p2align 3
.L187:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L184
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L187
.L185:
	movq	40(%r13), %r12
.L183:
	testq	%r12, %r12
	je	.L188
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L188:
	addq	$8, %rsp
	movq	%r13, %rdi
	movl	$120, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L184:
	.cfi_restore_state
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L187
	jmp	.L185
	.cfi_endproc
.LFE11768:
	.size	_ZN2v88internal6torque32CallCsaMacroAndBranchInstructionD0Ev, .-_ZN2v88internal6torque32CallCsaMacroAndBranchInstructionD0Ev
	.section	.text._ZN2v88internal6torque24CallIntrinsicInstructionD0Ev,"axG",@progbits,_ZN2v88internal6torque24CallIntrinsicInstructionD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque24CallIntrinsicInstructionD0Ev
	.type	_ZN2v88internal6torque24CallIntrinsicInstructionD0Ev, @function
_ZN2v88internal6torque24CallIntrinsicInstructionD0Ev:
.LFB11776:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal6torque24CallIntrinsicInstructionE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	72(%rdi), %rbx
	movq	64(%rdi), %r12
	movq	%rax, (%rdi)
	cmpq	%r12, %rbx
	je	.L197
	.p2align 4,,10
	.p2align 3
.L201:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L198
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%rbx, %r12
	jne	.L201
.L199:
	movq	64(%r13), %r12
.L197:
	testq	%r12, %r12
	je	.L202
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L202:
	movq	40(%r13), %rdi
	testq	%rdi, %rdi
	je	.L203
	call	_ZdlPv@PLT
.L203:
	addq	$8, %rsp
	movq	%r13, %rdi
	movl	$88, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L198:
	.cfi_restore_state
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L201
	jmp	.L199
	.cfi_endproc
.LFE11776:
	.size	_ZN2v88internal6torque24CallIntrinsicInstructionD0Ev, .-_ZN2v88internal6torque24CallIntrinsicInstructionD0Ev
	.section	.text._ZN2v88internal6torque23CallCsaMacroInstructionD0Ev,"axG",@progbits,_ZN2v88internal6torque23CallCsaMacroInstructionD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque23CallCsaMacroInstructionD0Ev
	.type	_ZN2v88internal6torque23CallCsaMacroInstructionD0Ev, @function
_ZN2v88internal6torque23CallCsaMacroInstructionD0Ev:
.LFB11780:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal6torque23CallCsaMacroInstructionE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	48(%rdi), %rbx
	movq	40(%rdi), %r12
	movq	%rax, (%rdi)
	cmpq	%r12, %rbx
	je	.L212
	.p2align 4,,10
	.p2align 3
.L216:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L213
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%rbx, %r12
	jne	.L216
.L214:
	movq	40(%r13), %r12
.L212:
	testq	%r12, %r12
	je	.L217
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L217:
	addq	$8, %rsp
	movq	%r13, %rdi
	movl	$80, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L213:
	.cfi_restore_state
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L216
	jmp	.L214
	.cfi_endproc
.LFE11780:
	.size	_ZN2v88internal6torque23CallCsaMacroInstructionD0Ev, .-_ZN2v88internal6torque23CallCsaMacroInstructionD0Ev
	.section	.rodata._ZNK2v88internal6torque15GotoInstruction21AppendSuccessorBlocksEPSt6vectorIPNS1_5BlockESaIS5_EE.str1.1,"aMS",@progbits,1
.LC2:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNK2v88internal6torque15GotoInstruction21AppendSuccessorBlocksEPSt6vectorIPNS1_5BlockESaIS5_EE,"axG",@progbits,_ZNK2v88internal6torque15GotoInstruction21AppendSuccessorBlocksEPSt6vectorIPNS1_5BlockESaIS5_EE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal6torque15GotoInstruction21AppendSuccessorBlocksEPSt6vectorIPNS1_5BlockESaIS5_EE
	.type	_ZNK2v88internal6torque15GotoInstruction21AppendSuccessorBlocksEPSt6vectorIPNS1_5BlockESaIS5_EE, @function
_ZNK2v88internal6torque15GotoInstruction21AppendSuccessorBlocksEPSt6vectorIPNS1_5BlockESaIS5_EE:
.LFB5868:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	8(%rsi), %rdx
	cmpq	16(%rsi), %rdx
	je	.L223
	movq	32(%rdi), %rax
	movq	%rax, (%rdx)
	addq	$8, 8(%rsi)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L223:
	.cfi_restore_state
	movq	(%rsi), %r8
	subq	%r8, %rdx
	movq	%rdx, %rax
	movq	%rdx, %r12
	movabsq	$1152921504606846975, %rdx
	sarq	$3, %rax
	cmpq	%rdx, %rax
	je	.L238
	testq	%rax, %rax
	je	.L231
	movabsq	$9223372036854775800, %r15
	leaq	(%rax,%rax), %rcx
	cmpq	%rcx, %rax
	jbe	.L239
.L226:
	movq	%r15, %rdi
	movq	%r8, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r8
	movq	%rax, %r14
	addq	%rax, %r15
.L227:
	movq	32(%r13), %rax
	leaq	8(%r14,%r12), %r13
	movq	%rax, (%r14,%r12)
	testq	%r12, %r12
	jg	.L240
	testq	%r8, %r8
	jne	.L229
.L230:
	movq	%r14, %xmm0
	movq	%r13, %xmm1
	movq	%r15, 16(%rbx)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L240:
	.cfi_restore_state
	movq	%r8, %rsi
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r8, -56(%rbp)
	call	memmove@PLT
	movq	-56(%rbp), %r8
.L229:
	movq	%r8, %rdi
	call	_ZdlPv@PLT
	jmp	.L230
	.p2align 4,,10
	.p2align 3
.L239:
	testq	%rcx, %rcx
	jne	.L241
	xorl	%r15d, %r15d
	xorl	%r14d, %r14d
	jmp	.L227
	.p2align 4,,10
	.p2align 3
.L231:
	movl	$8, %r15d
	jmp	.L226
.L238:
	leaq	.LC2(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L241:
	cmpq	%rdx, %rcx
	cmovbe	%rcx, %rdx
	leaq	0(,%rdx,8), %r15
	jmp	.L226
	.cfi_endproc
.LFE5868:
	.size	_ZNK2v88internal6torque15GotoInstruction21AppendSuccessorBlocksEPSt6vectorIPNS1_5BlockESaIS5_EE, .-_ZNK2v88internal6torque15GotoInstruction21AppendSuccessorBlocksEPSt6vectorIPNS1_5BlockESaIS5_EE
	.section	.text._ZN2v88internal6torque23GotoExternalInstructionD2Ev,"axG",@progbits,_ZN2v88internal6torque23GotoExternalInstructionD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque23GotoExternalInstructionD2Ev
	.type	_ZN2v88internal6torque23GotoExternalInstructionD2Ev, @function
_ZN2v88internal6torque23GotoExternalInstructionD2Ev:
.LFB11738:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal6torque23GotoExternalInstructionE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	72(%rdi), %r13
	movq	64(%rdi), %r12
	movq	%rax, (%rdi)
	cmpq	%r12, %r13
	je	.L243
	.p2align 4,,10
	.p2align 3
.L247:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L244
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r13, %r12
	jne	.L247
.L245:
	movq	64(%rbx), %r12
.L243:
	testq	%r12, %r12
	je	.L248
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L248:
	movq	32(%rbx), %rdi
	addq	$48, %rbx
	cmpq	%rbx, %rdi
	je	.L242
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L244:
	.cfi_restore_state
	addq	$32, %r12
	cmpq	%r12, %r13
	jne	.L247
	jmp	.L245
	.p2align 4,,10
	.p2align 3
.L242:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11738:
	.size	_ZN2v88internal6torque23GotoExternalInstructionD2Ev, .-_ZN2v88internal6torque23GotoExternalInstructionD2Ev
	.weak	_ZN2v88internal6torque23GotoExternalInstructionD1Ev
	.set	_ZN2v88internal6torque23GotoExternalInstructionD1Ev,_ZN2v88internal6torque23GotoExternalInstructionD2Ev
	.section	.text._ZN2v88internal6torque32CallCsaMacroAndBranchInstructionD2Ev,"axG",@progbits,_ZN2v88internal6torque32CallCsaMacroAndBranchInstructionD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque32CallCsaMacroAndBranchInstructionD2Ev
	.type	_ZN2v88internal6torque32CallCsaMacroAndBranchInstructionD2Ev, @function
_ZN2v88internal6torque32CallCsaMacroAndBranchInstructionD2Ev:
.LFB11766:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal6torque32CallCsaMacroAndBranchInstructionE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	80(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L255
	call	_ZdlPv@PLT
.L255:
	movq	48(%rbx), %r13
	movq	40(%rbx), %r12
	cmpq	%r12, %r13
	je	.L256
	.p2align 4,,10
	.p2align 3
.L260:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L257
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r12, %r13
	jne	.L260
.L258:
	movq	40(%rbx), %r12
.L256:
	testq	%r12, %r12
	je	.L254
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L257:
	.cfi_restore_state
	addq	$32, %r12
	cmpq	%r12, %r13
	jne	.L260
	jmp	.L258
	.p2align 4,,10
	.p2align 3
.L254:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11766:
	.size	_ZN2v88internal6torque32CallCsaMacroAndBranchInstructionD2Ev, .-_ZN2v88internal6torque32CallCsaMacroAndBranchInstructionD2Ev
	.weak	_ZN2v88internal6torque32CallCsaMacroAndBranchInstructionD1Ev
	.set	_ZN2v88internal6torque32CallCsaMacroAndBranchInstructionD1Ev,_ZN2v88internal6torque32CallCsaMacroAndBranchInstructionD2Ev
	.section	.text._ZN2v88internal6torque24CallIntrinsicInstructionD2Ev,"axG",@progbits,_ZN2v88internal6torque24CallIntrinsicInstructionD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque24CallIntrinsicInstructionD2Ev
	.type	_ZN2v88internal6torque24CallIntrinsicInstructionD2Ev, @function
_ZN2v88internal6torque24CallIntrinsicInstructionD2Ev:
.LFB11774:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal6torque24CallIntrinsicInstructionE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	72(%rdi), %r13
	movq	64(%rdi), %r12
	movq	%rax, (%rdi)
	cmpq	%r12, %r13
	je	.L267
	.p2align 4,,10
	.p2align 3
.L271:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L268
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r13, %r12
	jne	.L271
.L269:
	movq	64(%rbx), %r12
.L267:
	testq	%r12, %r12
	je	.L272
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L272:
	movq	40(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L266
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L268:
	.cfi_restore_state
	addq	$32, %r12
	cmpq	%r12, %r13
	jne	.L271
	jmp	.L269
	.p2align 4,,10
	.p2align 3
.L266:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11774:
	.size	_ZN2v88internal6torque24CallIntrinsicInstructionD2Ev, .-_ZN2v88internal6torque24CallIntrinsicInstructionD2Ev
	.weak	_ZN2v88internal6torque24CallIntrinsicInstructionD1Ev
	.set	_ZN2v88internal6torque24CallIntrinsicInstructionD1Ev,_ZN2v88internal6torque24CallIntrinsicInstructionD2Ev
	.section	.text.unlikely._ZNK2v88internal6torque22CallRuntimeInstruction17IsBlockTerminatorEv,"ax",@progbits
	.align 2
.LCOLDB3:
	.section	.text._ZNK2v88internal6torque22CallRuntimeInstruction17IsBlockTerminatorEv,"ax",@progbits
.LHOTB3:
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal6torque22CallRuntimeInstruction17IsBlockTerminatorEv
	.type	_ZNK2v88internal6torque22CallRuntimeInstruction17IsBlockTerminatorEv, @function
_ZNK2v88internal6torque22CallRuntimeInstruction17IsBlockTerminatorEv:
.LFB6905:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6905
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movzbl	28(%rdi), %eax
	testb	%al, %al
	je	.L286
.L278:
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L287
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L286:
	.cfi_restore_state
	movq	32(%rdi), %rax
	leaq	-64(%rbp), %r12
	movq	296(%rax), %r13
.LEHB0:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_10TypeOracleEEERPNT_5ScopeEv@PLT
.LEHE0:
	leaq	-80(%rbp), %rdi
	movq	%r12, -80(%rbp)
	movl	$1702258030, -64(%rbp)
	movb	$114, -60(%rbp)
	movq	$5, -72(%rbp)
	movb	$0, -59(%rbp)
.LEHB1:
	call	_ZN2v88internal6torque12Declarations16LookupGlobalTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
.LEHE1:
	movq	-80(%rbp), %rdi
	movq	%rax, %rbx
	cmpq	%r12, %rdi
	je	.L280
	call	_ZdlPv@PLT
.L280:
	cmpq	%rbx, %r13
	sete	%al
	jmp	.L278
.L287:
	call	__stack_chk_fail@PLT
.L284:
	endbr64
	movq	%rax, %r13
	jmp	.L281
	.globl	__gxx_personality_v0
	.section	.gcc_except_table._ZNK2v88internal6torque22CallRuntimeInstruction17IsBlockTerminatorEv,"a",@progbits
.LLSDA6905:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6905-.LLSDACSB6905
.LLSDACSB6905:
	.uleb128 .LEHB0-.LFB6905
	.uleb128 .LEHE0-.LEHB0
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB1-.LFB6905
	.uleb128 .LEHE1-.LEHB1
	.uleb128 .L284-.LFB6905
	.uleb128 0
.LLSDACSE6905:
	.section	.text._ZNK2v88internal6torque22CallRuntimeInstruction17IsBlockTerminatorEv
	.cfi_endproc
	.section	.text.unlikely._ZNK2v88internal6torque22CallRuntimeInstruction17IsBlockTerminatorEv
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6905
	.type	_ZNK2v88internal6torque22CallRuntimeInstruction17IsBlockTerminatorEv.cold, @function
_ZNK2v88internal6torque22CallRuntimeInstruction17IsBlockTerminatorEv.cold:
.LFSB6905:
.L281:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movq	-80(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L282
	call	_ZdlPv@PLT
.L282:
	movq	%r13, %rdi
.LEHB2:
	call	_Unwind_Resume@PLT
.LEHE2:
	.cfi_endproc
.LFE6905:
	.section	.gcc_except_table._ZNK2v88internal6torque22CallRuntimeInstruction17IsBlockTerminatorEv
.LLSDAC6905:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6905-.LLSDACSBC6905
.LLSDACSBC6905:
	.uleb128 .LEHB2-.LCOLDB3
	.uleb128 .LEHE2-.LEHB2
	.uleb128 0
	.uleb128 0
.LLSDACSEC6905:
	.section	.text.unlikely._ZNK2v88internal6torque22CallRuntimeInstruction17IsBlockTerminatorEv
	.section	.text._ZNK2v88internal6torque22CallRuntimeInstruction17IsBlockTerminatorEv
	.size	_ZNK2v88internal6torque22CallRuntimeInstruction17IsBlockTerminatorEv, .-_ZNK2v88internal6torque22CallRuntimeInstruction17IsBlockTerminatorEv
	.section	.text.unlikely._ZNK2v88internal6torque22CallRuntimeInstruction17IsBlockTerminatorEv
	.size	_ZNK2v88internal6torque22CallRuntimeInstruction17IsBlockTerminatorEv.cold, .-_ZNK2v88internal6torque22CallRuntimeInstruction17IsBlockTerminatorEv.cold
.LCOLDE3:
	.section	.text._ZNK2v88internal6torque22CallRuntimeInstruction17IsBlockTerminatorEv
.LHOTE3:
	.section	.text.unlikely._ZNK2v88internal6torque28NamespaceConstantInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE,"ax",@progbits
	.align 2
.LCOLDB4:
	.section	.text._ZNK2v88internal6torque28NamespaceConstantInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE,"ax",@progbits
.LHOTB4:
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal6torque28NamespaceConstantInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.type	_ZNK2v88internal6torque28NamespaceConstantInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE, @function
_ZNK2v88internal6torque28NamespaceConstantInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE:
.LFB6886:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6886
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	leaq	-80(%rbp), %rdi
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%r8), %rax
	movq	72(%rax), %rsi
.LEHB3:
	call	_ZN2v88internal6torque9LowerTypeEPKNS1_4TypeE@PLT
.LEHE3:
	movq	-80(%rbp), %r12
	movq	-72(%rbp), %r14
	cmpq	%r14, %r12
	je	.L289
	movq	8(%rbx), %r8
	jmp	.L298
	.p2align 4,,10
	.p2align 3
.L319:
	movq	%rax, (%r8)
	movq	8(%rbx), %rax
	addq	$8, %r12
	leaq	8(%rax), %r8
	movq	%r8, 8(%rbx)
	cmpq	%r12, %r14
	je	.L318
.L298:
	movq	(%r12), %rax
	movq	%rax, -96(%rbp)
	cmpq	%r8, 16(%rbx)
	jne	.L319
	movabsq	$1152921504606846975, %rdx
	movq	(%rbx), %r15
	subq	%r15, %r8
	movq	%r8, %rax
	movq	%r8, %r13
	sarq	$3, %rax
	cmpq	%rdx, %rax
	je	.L320
	testq	%rax, %rax
	je	.L303
	movabsq	$9223372036854775800, %rsi
	leaq	(%rax,%rax), %rdx
	movq	%rsi, -88(%rbp)
	cmpq	%rdx, %rax
	jbe	.L321
.L293:
	movq	-88(%rbp), %rdi
.LEHB4:
	call	_Znwm@PLT
	movq	%rax, %rcx
	movq	-88(%rbp), %rax
	addq	%rcx, %rax
	movq	%rax, -88(%rbp)
.L294:
	movq	-96(%rbp), %rax
	leaq	8(%rcx,%r13), %r8
	movq	%rax, (%rcx,%r13)
	testq	%r13, %r13
	jg	.L322
	testq	%r15, %r15
	jne	.L296
.L297:
	movq	-88(%rbp), %rax
	movq	%rcx, %xmm0
	movq	%r8, %xmm1
	addq	$8, %r12
	punpcklqdq	%xmm1, %xmm0
	movq	%rax, 16(%rbx)
	movups	%xmm0, (%rbx)
	cmpq	%r12, %r14
	jne	.L298
	.p2align 4,,10
	.p2align 3
.L318:
	movq	-80(%rbp), %r14
.L289:
	testq	%r14, %r14
	je	.L288
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L288:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L323
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L321:
	.cfi_restore_state
	testq	%rdx, %rdx
	jne	.L324
	movq	$0, -88(%rbp)
	xorl	%ecx, %ecx
	jmp	.L294
	.p2align 4,,10
	.p2align 3
.L322:
	movq	%rcx, %rdi
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r8, -96(%rbp)
	call	memmove@PLT
	movq	-96(%rbp), %r8
	movq	%rax, %rcx
.L296:
	movq	%r15, %rdi
	movq	%rcx, -104(%rbp)
	movq	%r8, -96(%rbp)
	call	_ZdlPv@PLT
	movq	-104(%rbp), %rcx
	movq	-96(%rbp), %r8
	jmp	.L297
	.p2align 4,,10
	.p2align 3
.L303:
	movq	$8, -88(%rbp)
	jmp	.L293
.L320:
	leaq	.LC2(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.LEHE4:
.L323:
	call	__stack_chk_fail@PLT
.L324:
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	cmova	%rax, %rdx
	leaq	0(,%rdx,8), %rax
	movq	%rax, -88(%rbp)
	jmp	.L293
.L306:
	endbr64
	movq	%rax, %r12
	jmp	.L300
	.section	.gcc_except_table._ZNK2v88internal6torque28NamespaceConstantInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE,"a",@progbits
.LLSDA6886:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6886-.LLSDACSB6886
.LLSDACSB6886:
	.uleb128 .LEHB3-.LFB6886
	.uleb128 .LEHE3-.LEHB3
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB4-.LFB6886
	.uleb128 .LEHE4-.LEHB4
	.uleb128 .L306-.LFB6886
	.uleb128 0
.LLSDACSE6886:
	.section	.text._ZNK2v88internal6torque28NamespaceConstantInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.cfi_endproc
	.section	.text.unlikely._ZNK2v88internal6torque28NamespaceConstantInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6886
	.type	_ZNK2v88internal6torque28NamespaceConstantInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE.cold, @function
_ZNK2v88internal6torque28NamespaceConstantInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE.cold:
.LFSB6886:
.L300:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L301
	call	_ZdlPv@PLT
.L301:
	movq	%r12, %rdi
.LEHB5:
	call	_Unwind_Resume@PLT
.LEHE5:
	.cfi_endproc
.LFE6886:
	.section	.gcc_except_table._ZNK2v88internal6torque28NamespaceConstantInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
.LLSDAC6886:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6886-.LLSDACSBC6886
.LLSDACSBC6886:
	.uleb128 .LEHB5-.LCOLDB4
	.uleb128 .LEHE5-.LEHB5
	.uleb128 0
	.uleb128 0
.LLSDACSEC6886:
	.section	.text.unlikely._ZNK2v88internal6torque28NamespaceConstantInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.section	.text._ZNK2v88internal6torque28NamespaceConstantInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.size	_ZNK2v88internal6torque28NamespaceConstantInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE, .-_ZNK2v88internal6torque28NamespaceConstantInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.section	.text.unlikely._ZNK2v88internal6torque28NamespaceConstantInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.size	_ZNK2v88internal6torque28NamespaceConstantInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE.cold, .-_ZNK2v88internal6torque28NamespaceConstantInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE.cold
.LCOLDE4:
	.section	.text._ZNK2v88internal6torque28NamespaceConstantInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
.LHOTE4:
	.section	.rodata._ZNK2v88internal6torque32CallCsaMacroAndBranchInstruction5CloneEv.str1.8,"aMS",@progbits,1
	.align 8
.LC5:
	.string	"basic_string::_M_construct null not valid"
	.section	.text.unlikely._ZNK2v88internal6torque32CallCsaMacroAndBranchInstruction5CloneEv,"ax",@progbits
	.align 2
.LCOLDB6:
	.section	.text._ZNK2v88internal6torque32CallCsaMacroAndBranchInstruction5CloneEv,"ax",@progbits
.LHOTB6:
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal6torque32CallCsaMacroAndBranchInstruction5CloneEv
	.type	_ZNK2v88internal6torque32CallCsaMacroAndBranchInstruction5CloneEv, @function
_ZNK2v88internal6torque32CallCsaMacroAndBranchInstruction5CloneEv:
.LFB6807:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6807
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$72, %rsp
	movq	%rdi, -88(%rbp)
	movl	$120, %edi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
.LEHB6:
	call	_Znwm@PLT
.LEHE6:
	movdqu	8(%rbx), %xmm1
	movq	48(%rbx), %rdx
	movq	%rax, %r12
	movups	%xmm1, 8(%rax)
	movl	24(%rbx), %eax
	movq	%rdx, %r13
	movq	%rdx, -80(%rbp)
	movl	%eax, 24(%r12)
	leaq	16+_ZTVN2v88internal6torque32CallCsaMacroAndBranchInstructionE(%rip), %rax
	movq	%rax, (%r12)
	movq	32(%rbx), %rax
	movq	$0, 40(%r12)
	movq	%rax, 32(%r12)
	movq	40(%rbx), %rax
	movq	$0, 48(%r12)
	movq	$0, 56(%r12)
	subq	%rax, %r13
	movq	%r13, %rdx
	sarq	$5, %rdx
	je	.L376
	movabsq	$288230376151711743, %rax
	cmpq	%rax, %rdx
	ja	.L377
	movq	%r13, %rdi
.LEHB7:
	call	_Znwm@PLT
.LEHE7:
	movq	%rax, -96(%rbp)
	movq	48(%rbx), %rax
	movq	%rax, -80(%rbp)
	movq	40(%rbx), %rax
.L327:
	movq	-96(%rbp), %rcx
	movq	%rcx, %xmm0
	addq	%rcx, %r13
	punpcklqdq	%xmm0, %xmm0
	movq	%r13, 56(%r12)
	movups	%xmm0, 40(%r12)
	cmpq	-80(%rbp), %rax
	je	.L353
	movq	%rax, -72(%rbp)
	leaq	-64(%rbp), %rax
	movq	%rcx, %r13
	movq	%rax, -104(%rbp)
	jmp	.L335
	.p2align 4,,10
	.p2align 3
.L380:
	movzbl	(%r15), %eax
	movb	%al, 16(%r13)
.L334:
	movq	%r14, 8(%r13)
	addq	$32, %r13
	addq	$32, -72(%rbp)
	movq	-72(%rbp), %rax
	movb	$0, (%rdi,%r14)
	cmpq	%rax, -80(%rbp)
	je	.L329
.L335:
	movq	-72(%rbp), %rax
	leaq	16(%r13), %rdi
	movq	%rdi, 0(%r13)
	movq	(%rax), %r15
	movq	8(%rax), %r14
	movq	%r15, %rax
	addq	%r14, %rax
	je	.L330
	testq	%r15, %r15
	je	.L378
.L330:
	movq	%r14, -64(%rbp)
	cmpq	$15, %r14
	ja	.L379
	cmpq	$1, %r14
	je	.L380
	testq	%r14, %r14
	je	.L334
	jmp	.L332
	.p2align 4,,10
	.p2align 3
.L379:
	movq	-104(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r13, %rdi
.LEHB8:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE8:
	movq	%rax, 0(%r13)
	movq	%rax, %rdi
	movq	-64(%rbp), %rax
	movq	%rax, 16(%r13)
.L332:
	movq	%r14, %rdx
	movq	%r15, %rsi
	call	memcpy@PLT
	movq	-64(%rbp), %r14
	movq	0(%r13), %rdi
	jmp	.L334
	.p2align 4,,10
	.p2align 3
.L353:
	movq	%rcx, %r13
	.p2align 4,,10
	.p2align 3
.L329:
	movq	88(%rbx), %rax
	movq	80(%rbx), %rsi
	movq	%r13, 48(%r12)
	pxor	%xmm0, %xmm0
	movdqu	64(%rbx), %xmm2
	movups	%xmm0, 80(%r12)
	movq	$0, 96(%r12)
	movq	%rax, %r13
	subq	%rsi, %r13
	movups	%xmm2, 64(%r12)
	movq	%r13, %rdx
	sarq	$3, %rdx
	je	.L381
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L382
	movq	%r13, %rdi
.LEHB9:
	call	_Znwm@PLT
.LEHE9:
	movq	%rax, %rcx
	movq	88(%rbx), %rax
	movq	80(%rbx), %rsi
	movq	%rax, %r14
	subq	%rsi, %r14
.L337:
	movq	%rcx, %xmm0
	addq	%rcx, %r13
	punpcklqdq	%xmm0, %xmm0
	movq	%r13, 96(%r12)
	movups	%xmm0, 80(%r12)
	cmpq	%rsi, %rax
	je	.L346
	movq	%rcx, %rdi
	movq	%r14, %rdx
	call	memmove@PLT
	movq	%rax, %rcx
.L346:
	movq	-88(%rbp), %rax
	addq	%r14, %rcx
	movdqu	104(%rbx), %xmm3
	movq	%rcx, 88(%r12)
	movq	%r12, (%rax)
	movups	%xmm3, 104(%r12)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L383
	movq	-88(%rbp), %rax
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L376:
	.cfi_restore_state
	movq	$0, -96(%rbp)
	jmp	.L327
	.p2align 4,,10
	.p2align 3
.L381:
	movq	%r13, %r14
	xorl	%ecx, %ecx
	jmp	.L337
.L378:
	leaq	.LC5(%rip), %rdi
.LEHB10:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE10:
.L383:
	call	__stack_chk_fail@PLT
.L382:
.LEHB11:
	call	_ZSt17__throw_bad_allocv@PLT
.LEHE11:
.L377:
.LEHB12:
	call	_ZSt17__throw_bad_allocv@PLT
.LEHE12:
.L354:
	endbr64
	movq	%rax, %r13
	jmp	.L344
.L355:
	endbr64
	movq	%rax, %r13
	jmp	.L347
.L357:
	endbr64
	movq	%rax, %rdi
	jmp	.L338
	.section	.gcc_except_table._ZNK2v88internal6torque32CallCsaMacroAndBranchInstruction5CloneEv,"a",@progbits
	.align 4
.LLSDA6807:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATT6807-.LLSDATTD6807
.LLSDATTD6807:
	.byte	0x1
	.uleb128 .LLSDACSE6807-.LLSDACSB6807
.LLSDACSB6807:
	.uleb128 .LEHB6-.LFB6807
	.uleb128 .LEHE6-.LEHB6
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB7-.LFB6807
	.uleb128 .LEHE7-.LEHB7
	.uleb128 .L354-.LFB6807
	.uleb128 0
	.uleb128 .LEHB8-.LFB6807
	.uleb128 .LEHE8-.LEHB8
	.uleb128 .L357-.LFB6807
	.uleb128 0x1
	.uleb128 .LEHB9-.LFB6807
	.uleb128 .LEHE9-.LEHB9
	.uleb128 .L355-.LFB6807
	.uleb128 0
	.uleb128 .LEHB10-.LFB6807
	.uleb128 .LEHE10-.LEHB10
	.uleb128 .L357-.LFB6807
	.uleb128 0x1
	.uleb128 .LEHB11-.LFB6807
	.uleb128 .LEHE11-.LEHB11
	.uleb128 .L355-.LFB6807
	.uleb128 0
	.uleb128 .LEHB12-.LFB6807
	.uleb128 .LEHE12-.LEHB12
	.uleb128 .L354-.LFB6807
	.uleb128 0
.LLSDACSE6807:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATT6807:
	.section	.text._ZNK2v88internal6torque32CallCsaMacroAndBranchInstruction5CloneEv
	.cfi_endproc
	.section	.text.unlikely._ZNK2v88internal6torque32CallCsaMacroAndBranchInstruction5CloneEv
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6807
	.type	_ZNK2v88internal6torque32CallCsaMacroAndBranchInstruction5CloneEv.cold, @function
_ZNK2v88internal6torque32CallCsaMacroAndBranchInstruction5CloneEv.cold:
.LFSB6807:
	nop
.L356:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	endbr64
	movq	%rax, %r13
	call	__cxa_end_catch@PLT
	movq	40(%r12), %rdi
	testq	%rdi, %rdi
	je	.L344
	call	_ZdlPv@PLT
.L344:
	movq	%r12, %rdi
	movl	$120, %esi
	call	_ZdlPvm@PLT
	movq	%r13, %rdi
.LEHB13:
	call	_Unwind_Resume@PLT
.LEHE13:
.L347:
	movq	48(%r12), %r14
	movq	40(%r12), %rbx
.L350:
	cmpq	%rbx, %r14
	jne	.L384
	movq	40(%r12), %rdi
	testq	%rdi, %rdi
	je	.L344
	call	_ZdlPv@PLT
	jmp	.L344
.L338:
	call	__cxa_begin_catch@PLT
.L341:
	cmpq	%r13, -96(%rbp)
	jne	.L385
.LEHB14:
	call	__cxa_rethrow@PLT
.LEHE14:
.L385:
	movq	-96(%rbp), %rax
	movq	(%rax), %rdi
	addq	$16, %rax
	cmpq	%rax, %rdi
	je	.L340
	call	_ZdlPv@PLT
.L340:
	addq	$32, -96(%rbp)
	jmp	.L341
.L384:
	movq	(%rbx), %rdi
	leaq	16(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L349
	call	_ZdlPv@PLT
.L349:
	addq	$32, %rbx
	jmp	.L350
	.cfi_endproc
.LFE6807:
	.section	.gcc_except_table._ZNK2v88internal6torque32CallCsaMacroAndBranchInstruction5CloneEv
	.align 4
.LLSDAC6807:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATTC6807-.LLSDATTDC6807
.LLSDATTDC6807:
	.byte	0x1
	.uleb128 .LLSDACSEC6807-.LLSDACSBC6807
.LLSDACSBC6807:
	.uleb128 .LEHB13-.LCOLDB6
	.uleb128 .LEHE13-.LEHB13
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB14-.LCOLDB6
	.uleb128 .LEHE14-.LEHB14
	.uleb128 .L356-.LCOLDB6
	.uleb128 0
.LLSDACSEC6807:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATTC6807:
	.section	.text.unlikely._ZNK2v88internal6torque32CallCsaMacroAndBranchInstruction5CloneEv
	.section	.text._ZNK2v88internal6torque32CallCsaMacroAndBranchInstruction5CloneEv
	.size	_ZNK2v88internal6torque32CallCsaMacroAndBranchInstruction5CloneEv, .-_ZNK2v88internal6torque32CallCsaMacroAndBranchInstruction5CloneEv
	.section	.text.unlikely._ZNK2v88internal6torque32CallCsaMacroAndBranchInstruction5CloneEv
	.size	_ZNK2v88internal6torque32CallCsaMacroAndBranchInstruction5CloneEv.cold, .-_ZNK2v88internal6torque32CallCsaMacroAndBranchInstruction5CloneEv.cold
.LCOLDE6:
	.section	.text._ZNK2v88internal6torque32CallCsaMacroAndBranchInstruction5CloneEv
.LHOTE6:
	.section	.text.unlikely._ZNK2v88internal6torque24CallIntrinsicInstruction5CloneEv,"ax",@progbits
	.align 2
.LCOLDB7:
	.section	.text._ZNK2v88internal6torque24CallIntrinsicInstruction5CloneEv,"ax",@progbits
.LHOTB7:
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal6torque24CallIntrinsicInstruction5CloneEv
	.type	_ZNK2v88internal6torque24CallIntrinsicInstruction5CloneEv, @function
_ZNK2v88internal6torque24CallIntrinsicInstruction5CloneEv:
.LFB6795:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6795
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$56, %rsp
	movq	%rdi, -80(%rbp)
	movl	$88, %edi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
.LEHB15:
	call	_Znwm@PLT
.LEHE15:
	movdqu	8(%rbx), %xmm1
	movq	40(%rbx), %rsi
	movq	%rax, %r12
	movups	%xmm1, 8(%rax)
	movl	24(%rbx), %eax
	movq	$0, 40(%r12)
	movl	%eax, 24(%r12)
	leaq	16+_ZTVN2v88internal6torque24CallIntrinsicInstructionE(%rip), %rax
	movq	%rax, (%r12)
	movq	32(%rbx), %rax
	movq	$0, 48(%r12)
	movq	%rax, 32(%r12)
	movq	48(%rbx), %rax
	movq	$0, 56(%r12)
	movq	%rax, %r13
	subq	%rsi, %r13
	movq	%r13, %rdx
	sarq	$3, %rdx
	je	.L434
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L435
	movq	%r13, %rdi
.LEHB16:
	call	_Znwm@PLT
.LEHE16:
	movq	%rax, %rcx
	movq	48(%rbx), %rax
	movq	40(%rbx), %rsi
	movq	%rax, %r14
	subq	%rsi, %r14
.L388:
	movq	%rcx, %xmm0
	addq	%rcx, %r13
	punpcklqdq	%xmm0, %xmm0
	movq	%r13, 56(%r12)
	movups	%xmm0, 40(%r12)
	cmpq	%rax, %rsi
	je	.L390
	movq	%rcx, %rdi
	movq	%r14, %rdx
	call	memmove@PLT
	movq	%rax, %rcx
.L390:
	movq	72(%rbx), %rax
	movq	64(%rbx), %r13
	addq	%r14, %rcx
	pxor	%xmm0, %xmm0
	movq	%rcx, 48(%r12)
	movq	%rax, %r14
	movq	%rax, -72(%rbp)
	subq	%r13, %r14
	movups	%xmm0, 64(%r12)
	movq	$0, 80(%r12)
	movq	%r14, %rax
	sarq	$5, %rax
	je	.L436
	movabsq	$288230376151711743, %rdx
	cmpq	%rdx, %rax
	ja	.L437
	movq	%r14, %rdi
.LEHB17:
	call	_Znwm@PLT
.LEHE17:
	movq	%rax, -88(%rbp)
	movq	72(%rbx), %rax
	movq	64(%rbx), %r13
	movq	%rax, -72(%rbp)
.L392:
	movq	-88(%rbp), %rbx
	movq	%rbx, %xmm0
	addq	%rbx, %r14
	punpcklqdq	%xmm0, %xmm0
	movq	%r14, 80(%r12)
	movups	%xmm0, 64(%r12)
	cmpq	-72(%rbp), %r13
	je	.L394
	leaq	-64(%rbp), %rax
	movq	%rax, -96(%rbp)
	jmp	.L400
	.p2align 4,,10
	.p2align 3
.L396:
	cmpq	$1, %r14
	jne	.L398
	movzbl	(%r15), %eax
	movb	%al, 16(%rbx)
.L399:
	movq	%r14, 8(%rbx)
	addq	$32, %r13
	addq	$32, %rbx
	movb	$0, (%rdi,%r14)
	cmpq	%r13, -72(%rbp)
	je	.L394
.L400:
	leaq	16(%rbx), %rdi
	movq	%rdi, (%rbx)
	movq	0(%r13), %r15
	movq	8(%r13), %r14
	movq	%r15, %rax
	addq	%r14, %rax
	je	.L395
	testq	%r15, %r15
	je	.L438
.L395:
	movq	%r14, -64(%rbp)
	cmpq	$15, %r14
	jbe	.L396
	movq	-96(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rbx, %rdi
.LEHB18:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%rbx)
	movq	%rax, %rdi
	movq	-64(%rbp), %rax
	movq	%rax, 16(%rbx)
.L397:
	movq	%r14, %rdx
	movq	%r15, %rsi
	call	memcpy@PLT
	movq	-64(%rbp), %r14
	movq	(%rbx), %rdi
	jmp	.L399
	.p2align 4,,10
	.p2align 3
.L398:
	testq	%r14, %r14
	je	.L399
	jmp	.L397
	.p2align 4,,10
	.p2align 3
.L394:
	movq	-80(%rbp), %rax
	movq	%rbx, 72(%r12)
	movq	%r12, (%rax)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L439
	movq	-80(%rbp), %rax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L434:
	.cfi_restore_state
	movq	%r13, %r14
	xorl	%ecx, %ecx
	jmp	.L388
	.p2align 4,,10
	.p2align 3
.L436:
	movq	$0, -88(%rbp)
	jmp	.L392
.L438:
	leaq	.LC5(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE18:
.L439:
	call	__stack_chk_fail@PLT
.L435:
.LEHB19:
	call	_ZSt17__throw_bad_allocv@PLT
.LEHE19:
.L437:
.LEHB20:
	call	_ZSt17__throw_bad_allocv@PLT
.LEHE20:
.L413:
	endbr64
	movq	%rax, %r13
	jmp	.L407
.L415:
	endbr64
	movq	%rax, %rdi
	jmp	.L401
.L412:
	endbr64
	movq	%rax, %r13
	jmp	.L409
	.section	.gcc_except_table._ZNK2v88internal6torque24CallIntrinsicInstruction5CloneEv,"a",@progbits
	.align 4
.LLSDA6795:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATT6795-.LLSDATTD6795
.LLSDATTD6795:
	.byte	0x1
	.uleb128 .LLSDACSE6795-.LLSDACSB6795
.LLSDACSB6795:
	.uleb128 .LEHB15-.LFB6795
	.uleb128 .LEHE15-.LEHB15
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB16-.LFB6795
	.uleb128 .LEHE16-.LEHB16
	.uleb128 .L412-.LFB6795
	.uleb128 0
	.uleb128 .LEHB17-.LFB6795
	.uleb128 .LEHE17-.LEHB17
	.uleb128 .L413-.LFB6795
	.uleb128 0
	.uleb128 .LEHB18-.LFB6795
	.uleb128 .LEHE18-.LEHB18
	.uleb128 .L415-.LFB6795
	.uleb128 0x1
	.uleb128 .LEHB19-.LFB6795
	.uleb128 .LEHE19-.LEHB19
	.uleb128 .L412-.LFB6795
	.uleb128 0
	.uleb128 .LEHB20-.LFB6795
	.uleb128 .LEHE20-.LEHB20
	.uleb128 .L413-.LFB6795
	.uleb128 0
.LLSDACSE6795:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATT6795:
	.section	.text._ZNK2v88internal6torque24CallIntrinsicInstruction5CloneEv
	.cfi_endproc
	.section	.text.unlikely._ZNK2v88internal6torque24CallIntrinsicInstruction5CloneEv
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6795
	.type	_ZNK2v88internal6torque24CallIntrinsicInstruction5CloneEv.cold, @function
_ZNK2v88internal6torque24CallIntrinsicInstruction5CloneEv.cold:
.LFSB6795:
	nop
.L414:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	endbr64
	movq	%rax, %r13
	call	__cxa_end_catch@PLT
	movq	64(%r12), %rdi
	testq	%rdi, %rdi
	je	.L407
	call	_ZdlPv@PLT
.L407:
	movq	40(%r12), %rdi
	testq	%rdi, %rdi
	je	.L409
	call	_ZdlPv@PLT
.L409:
	movq	%r12, %rdi
	movl	$88, %esi
	call	_ZdlPvm@PLT
	movq	%r13, %rdi
.LEHB21:
	call	_Unwind_Resume@PLT
.LEHE21:
.L401:
	call	__cxa_begin_catch@PLT
.L404:
	cmpq	%rbx, -88(%rbp)
	jne	.L440
.LEHB22:
	call	__cxa_rethrow@PLT
.LEHE22:
.L440:
	movq	-88(%rbp), %rax
	movq	(%rax), %rdi
	addq	$16, %rax
	cmpq	%rax, %rdi
	je	.L403
	call	_ZdlPv@PLT
.L403:
	addq	$32, -88(%rbp)
	jmp	.L404
	.cfi_endproc
.LFE6795:
	.section	.gcc_except_table._ZNK2v88internal6torque24CallIntrinsicInstruction5CloneEv
	.align 4
.LLSDAC6795:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATTC6795-.LLSDATTDC6795
.LLSDATTDC6795:
	.byte	0x1
	.uleb128 .LLSDACSEC6795-.LLSDACSBC6795
.LLSDACSBC6795:
	.uleb128 .LEHB21-.LCOLDB7
	.uleb128 .LEHE21-.LEHB21
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB22-.LCOLDB7
	.uleb128 .LEHE22-.LEHB22
	.uleb128 .L414-.LCOLDB7
	.uleb128 0
.LLSDACSEC6795:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATTC6795:
	.section	.text.unlikely._ZNK2v88internal6torque24CallIntrinsicInstruction5CloneEv
	.section	.text._ZNK2v88internal6torque24CallIntrinsicInstruction5CloneEv
	.size	_ZNK2v88internal6torque24CallIntrinsicInstruction5CloneEv, .-_ZNK2v88internal6torque24CallIntrinsicInstruction5CloneEv
	.section	.text.unlikely._ZNK2v88internal6torque24CallIntrinsicInstruction5CloneEv
	.size	_ZNK2v88internal6torque24CallIntrinsicInstruction5CloneEv.cold, .-_ZNK2v88internal6torque24CallIntrinsicInstruction5CloneEv.cold
.LCOLDE7:
	.section	.text._ZNK2v88internal6torque24CallIntrinsicInstruction5CloneEv
.LHOTE7:
	.section	.text.unlikely._ZNK2v88internal6torque23CallCsaMacroInstruction5CloneEv,"ax",@progbits
	.align 2
.LCOLDB8:
	.section	.text._ZNK2v88internal6torque23CallCsaMacroInstruction5CloneEv,"ax",@progbits
.LHOTB8:
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal6torque23CallCsaMacroInstruction5CloneEv
	.type	_ZNK2v88internal6torque23CallCsaMacroInstruction5CloneEv, @function
_ZNK2v88internal6torque23CallCsaMacroInstruction5CloneEv:
.LFB6788:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6788
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -88(%rbp)
	movl	$80, %edi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
.LEHB23:
	call	_Znwm@PLT
.LEHE23:
	movdqu	8(%r13), %xmm1
	movq	48(%r13), %rcx
	movq	%rax, %r12
	movups	%xmm1, 8(%rax)
	movl	24(%r13), %eax
	movq	%rcx, %rbx
	movq	%rcx, -80(%rbp)
	movl	%eax, 24(%r12)
	leaq	16+_ZTVN2v88internal6torque23CallCsaMacroInstructionE(%rip), %rax
	movq	%rax, (%r12)
	movq	32(%r13), %rax
	movq	$0, 40(%r12)
	movq	%rax, 32(%r12)
	movq	40(%r13), %rax
	movq	$0, 48(%r12)
	movq	$0, 56(%r12)
	subq	%rax, %rbx
	movq	%rbx, %rdx
	sarq	$5, %rdx
	je	.L479
	movabsq	$288230376151711743, %rax
	cmpq	%rax, %rdx
	ja	.L480
	movq	%rbx, %rdi
.LEHB24:
	call	_Znwm@PLT
.LEHE24:
	movq	%rax, -96(%rbp)
	movq	48(%r13), %rax
	movq	%rax, -80(%rbp)
	movq	40(%r13), %rax
.L443:
	movq	-96(%rbp), %rdx
	movq	%rdx, %xmm0
	addq	%rdx, %rbx
	punpcklqdq	%xmm0, %xmm0
	movq	%rbx, 56(%r12)
	movups	%xmm0, 40(%r12)
	cmpq	%rax, -80(%rbp)
	je	.L460
	movq	%rax, -72(%rbp)
	leaq	-64(%rbp), %rax
	movq	%rdx, %rbx
	movq	%rax, -104(%rbp)
	jmp	.L451
	.p2align 4,,10
	.p2align 3
.L483:
	movzbl	(%r15), %eax
	movb	%al, 16(%rbx)
.L450:
	movq	%r14, 8(%rbx)
	addq	$32, %rbx
	addq	$32, -72(%rbp)
	movq	-72(%rbp), %rax
	movb	$0, (%rdi,%r14)
	cmpq	%rax, -80(%rbp)
	je	.L445
.L451:
	movq	-72(%rbp), %rax
	leaq	16(%rbx), %rdi
	movq	%rdi, (%rbx)
	movq	(%rax), %r15
	movq	8(%rax), %r14
	movq	%r15, %rax
	addq	%r14, %rax
	je	.L446
	testq	%r15, %r15
	je	.L481
.L446:
	movq	%r14, -64(%rbp)
	cmpq	$15, %r14
	ja	.L482
	cmpq	$1, %r14
	je	.L483
	testq	%r14, %r14
	je	.L450
	jmp	.L448
	.p2align 4,,10
	.p2align 3
.L482:
	movq	-104(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rbx, %rdi
.LEHB25:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%rbx)
	movq	%rax, %rdi
	movq	-64(%rbp), %rax
	movq	%rax, 16(%rbx)
.L448:
	movq	%r14, %rdx
	movq	%r15, %rsi
	call	memcpy@PLT
	movq	-64(%rbp), %r14
	movq	(%rbx), %rdi
	jmp	.L450
	.p2align 4,,10
	.p2align 3
.L460:
	movq	%rdx, %rbx
	.p2align 4,,10
	.p2align 3
.L445:
	movq	-88(%rbp), %rax
	movdqu	64(%r13), %xmm2
	movq	%rbx, 48(%r12)
	movq	%r12, (%rax)
	movups	%xmm2, 64(%r12)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L484
	movq	-88(%rbp), %rax
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L479:
	.cfi_restore_state
	movq	$0, -96(%rbp)
	jmp	.L443
.L481:
	leaq	.LC5(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE25:
.L484:
	call	__stack_chk_fail@PLT
.L480:
.LEHB26:
	call	_ZSt17__throw_bad_allocv@PLT
.LEHE26:
.L463:
	endbr64
	movq	%rax, %rdi
	jmp	.L452
.L461:
	endbr64
	movq	%rax, %r13
	jmp	.L458
	.section	.gcc_except_table._ZNK2v88internal6torque23CallCsaMacroInstruction5CloneEv,"a",@progbits
	.align 4
.LLSDA6788:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATT6788-.LLSDATTD6788
.LLSDATTD6788:
	.byte	0x1
	.uleb128 .LLSDACSE6788-.LLSDACSB6788
.LLSDACSB6788:
	.uleb128 .LEHB23-.LFB6788
	.uleb128 .LEHE23-.LEHB23
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB24-.LFB6788
	.uleb128 .LEHE24-.LEHB24
	.uleb128 .L461-.LFB6788
	.uleb128 0
	.uleb128 .LEHB25-.LFB6788
	.uleb128 .LEHE25-.LEHB25
	.uleb128 .L463-.LFB6788
	.uleb128 0x1
	.uleb128 .LEHB26-.LFB6788
	.uleb128 .LEHE26-.LEHB26
	.uleb128 .L461-.LFB6788
	.uleb128 0
.LLSDACSE6788:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATT6788:
	.section	.text._ZNK2v88internal6torque23CallCsaMacroInstruction5CloneEv
	.cfi_endproc
	.section	.text.unlikely._ZNK2v88internal6torque23CallCsaMacroInstruction5CloneEv
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6788
	.type	_ZNK2v88internal6torque23CallCsaMacroInstruction5CloneEv.cold, @function
_ZNK2v88internal6torque23CallCsaMacroInstruction5CloneEv.cold:
.LFSB6788:
.L452:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	call	__cxa_begin_catch@PLT
.L455:
	cmpq	%rbx, -96(%rbp)
	jne	.L485
.LEHB27:
	call	__cxa_rethrow@PLT
.LEHE27:
.L462:
	endbr64
	movq	%rax, %r13
	call	__cxa_end_catch@PLT
	movq	40(%r12), %rdi
	testq	%rdi, %rdi
	je	.L458
	call	_ZdlPv@PLT
.L458:
	movq	%r12, %rdi
	movl	$80, %esi
	call	_ZdlPvm@PLT
	movq	%r13, %rdi
.LEHB28:
	call	_Unwind_Resume@PLT
.LEHE28:
.L485:
	movq	-96(%rbp), %rax
	movq	(%rax), %rdi
	addq	$16, %rax
	cmpq	%rax, %rdi
	je	.L454
	call	_ZdlPv@PLT
.L454:
	addq	$32, -96(%rbp)
	jmp	.L455
	.cfi_endproc
.LFE6788:
	.section	.gcc_except_table._ZNK2v88internal6torque23CallCsaMacroInstruction5CloneEv
	.align 4
.LLSDAC6788:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATTC6788-.LLSDATTDC6788
.LLSDATTDC6788:
	.byte	0x1
	.uleb128 .LLSDACSEC6788-.LLSDACSBC6788
.LLSDACSBC6788:
	.uleb128 .LEHB27-.LCOLDB8
	.uleb128 .LEHE27-.LEHB27
	.uleb128 .L462-.LCOLDB8
	.uleb128 0
	.uleb128 .LEHB28-.LCOLDB8
	.uleb128 .LEHE28-.LEHB28
	.uleb128 0
	.uleb128 0
.LLSDACSEC6788:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATTC6788:
	.section	.text.unlikely._ZNK2v88internal6torque23CallCsaMacroInstruction5CloneEv
	.section	.text._ZNK2v88internal6torque23CallCsaMacroInstruction5CloneEv
	.size	_ZNK2v88internal6torque23CallCsaMacroInstruction5CloneEv, .-_ZNK2v88internal6torque23CallCsaMacroInstruction5CloneEv
	.section	.text.unlikely._ZNK2v88internal6torque23CallCsaMacroInstruction5CloneEv
	.size	_ZNK2v88internal6torque23CallCsaMacroInstruction5CloneEv.cold, .-_ZNK2v88internal6torque23CallCsaMacroInstruction5CloneEv.cold
.LCOLDE8:
	.section	.text._ZNK2v88internal6torque23CallCsaMacroInstruction5CloneEv
.LHOTE8:
	.section	.text._ZNK2v88internal6torque28PushUninitializedInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal6torque28PushUninitializedInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.type	_ZNK2v88internal6torque28PushUninitializedInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE, @function
_ZNK2v88internal6torque28PushUninitializedInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE:
.LFB6884:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	32(%rdi), %r13
	movq	8(%rsi), %rdx
	cmpq	16(%rsi), %rdx
	je	.L487
	movq	%r13, (%rdx)
	addq	$8, 8(%rsi)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L487:
	.cfi_restore_state
	movq	(%rsi), %r8
	subq	%r8, %rdx
	movq	%rdx, %rax
	movq	%rdx, %r12
	movabsq	$1152921504606846975, %rdx
	sarq	$3, %rax
	cmpq	%rdx, %rax
	je	.L502
	testq	%rax, %rax
	je	.L495
	movabsq	$9223372036854775800, %r15
	leaq	(%rax,%rax), %rcx
	cmpq	%rcx, %rax
	jbe	.L503
.L490:
	movq	%r15, %rdi
	movq	%r8, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r8
	movq	%rax, %r14
	addq	%rax, %r15
.L491:
	movq	%r13, (%r14,%r12)
	leaq	8(%r14,%r12), %r13
	testq	%r12, %r12
	jg	.L504
	testq	%r8, %r8
	jne	.L493
.L494:
	movq	%r14, %xmm0
	movq	%r13, %xmm1
	movq	%r15, 16(%rbx)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L504:
	.cfi_restore_state
	movq	%r8, %rsi
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r8, -56(%rbp)
	call	memmove@PLT
	movq	-56(%rbp), %r8
.L493:
	movq	%r8, %rdi
	call	_ZdlPv@PLT
	jmp	.L494
	.p2align 4,,10
	.p2align 3
.L503:
	testq	%rcx, %rcx
	jne	.L505
	xorl	%r15d, %r15d
	xorl	%r14d, %r14d
	jmp	.L491
	.p2align 4,,10
	.p2align 3
.L495:
	movl	$8, %r15d
	jmp	.L490
.L502:
	leaq	.LC2(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L505:
	cmpq	%rdx, %rcx
	cmovbe	%rcx, %rdx
	leaq	0(,%rdx,8), %r15
	jmp	.L490
	.cfi_endproc
.LFE6884:
	.size	_ZNK2v88internal6torque28PushUninitializedInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE, .-_ZNK2v88internal6torque28PushUninitializedInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.section	.text._ZNK2v88internal6torque29PushBuiltinPointerInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal6torque29PushBuiltinPointerInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.type	_ZNK2v88internal6torque29PushBuiltinPointerInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE, @function
_ZNK2v88internal6torque29PushBuiltinPointerInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE:
.LFB6885:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	64(%rdi), %r13
	movq	8(%rsi), %rdx
	cmpq	16(%rsi), %rdx
	je	.L507
	movq	%r13, (%rdx)
	addq	$8, 8(%rsi)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L507:
	.cfi_restore_state
	movq	(%rsi), %r8
	subq	%r8, %rdx
	movq	%rdx, %rax
	movq	%rdx, %r12
	movabsq	$1152921504606846975, %rdx
	sarq	$3, %rax
	cmpq	%rdx, %rax
	je	.L522
	testq	%rax, %rax
	je	.L515
	movabsq	$9223372036854775800, %r15
	leaq	(%rax,%rax), %rcx
	cmpq	%rcx, %rax
	jbe	.L523
.L510:
	movq	%r15, %rdi
	movq	%r8, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r8
	movq	%rax, %r14
	addq	%rax, %r15
.L511:
	movq	%r13, (%r14,%r12)
	leaq	8(%r14,%r12), %r13
	testq	%r12, %r12
	jg	.L524
	testq	%r8, %r8
	jne	.L513
.L514:
	movq	%r14, %xmm0
	movq	%r13, %xmm1
	movq	%r15, 16(%rbx)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L524:
	.cfi_restore_state
	movq	%r8, %rsi
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r8, -56(%rbp)
	call	memmove@PLT
	movq	-56(%rbp), %r8
.L513:
	movq	%r8, %rdi
	call	_ZdlPv@PLT
	jmp	.L514
	.p2align 4,,10
	.p2align 3
.L523:
	testq	%rcx, %rcx
	jne	.L525
	xorl	%r15d, %r15d
	xorl	%r14d, %r14d
	jmp	.L511
	.p2align 4,,10
	.p2align 3
.L515:
	movl	$8, %r15d
	jmp	.L510
.L522:
	leaq	.LC2(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L525:
	cmpq	%rdx, %rcx
	cmovbe	%rcx, %rdx
	leaq	0(,%rdx,8), %r15
	jmp	.L510
	.cfi_endproc
.LFE6885:
	.size	_ZNK2v88internal6torque29PushBuiltinPointerInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE, .-_ZNK2v88internal6torque29PushBuiltinPointerInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.section	.text.unlikely._ZNK2v88internal6torque23GotoExternalInstruction5CloneEv,"ax",@progbits
	.align 2
.LCOLDB9:
	.section	.text._ZNK2v88internal6torque23GotoExternalInstruction5CloneEv,"ax",@progbits
.LHOTB9:
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal6torque23GotoExternalInstruction5CloneEv
	.type	_ZNK2v88internal6torque23GotoExternalInstruction5CloneEv, @function
_ZNK2v88internal6torque23GotoExternalInstruction5CloneEv:
.LFB6849:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6849
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$72, %rsp
	movq	%rdi, -80(%rbp)
	movl	$88, %edi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
.LEHB29:
	call	_Znwm@PLT
.LEHE29:
	movdqu	8(%rbx), %xmm1
	movq	32(%rbx), %r14
	movq	%rax, %r12
	movq	40(%rbx), %r13
	movups	%xmm1, 8(%rax)
	movl	24(%rbx), %eax
	movl	%eax, 24(%r12)
	leaq	16+_ZTVN2v88internal6torque23GotoExternalInstructionE(%rip), %rax
	movq	%rax, (%r12)
	leaq	48(%r12), %rax
	movq	%rax, -104(%rbp)
	movq	%rax, 32(%r12)
	movq	%r14, %rax
	addq	%r13, %rax
	je	.L527
	testq	%r14, %r14
	je	.L579
.L527:
	movq	%r13, -64(%rbp)
	cmpq	$15, %r13
	ja	.L580
	cmpq	$1, %r13
	jne	.L530
	movzbl	(%r14), %eax
	movb	%al, 48(%r12)
	movq	-104(%rbp), %rax
.L531:
	movq	%r13, 40(%r12)
	pxor	%xmm0, %xmm0
	movb	$0, (%rax,%r13)
	movq	72(%rbx), %rax
	movq	64(%rbx), %r13
	movups	%xmm0, 64(%r12)
	movq	%rax, %r14
	movq	%rax, -72(%rbp)
	movq	$0, 80(%r12)
	subq	%r13, %r14
	movq	%r14, %rax
	sarq	$5, %rax
	je	.L581
	movabsq	$288230376151711743, %rdx
	cmpq	%rdx, %rax
	ja	.L582
	movq	%r14, %rdi
.LEHB30:
	call	_Znwm@PLT
.LEHE30:
	movq	%rax, -88(%rbp)
	movq	72(%rbx), %rax
	movq	64(%rbx), %r13
	movq	%rax, -72(%rbp)
.L533:
	movq	-88(%rbp), %rbx
	movq	%rbx, %xmm0
	addq	%rbx, %r14
	punpcklqdq	%xmm0, %xmm0
	movq	%r14, 80(%r12)
	movups	%xmm0, 64(%r12)
	cmpq	%r13, -72(%rbp)
	je	.L535
	leaq	-64(%rbp), %rax
	movq	%rax, -96(%rbp)
	jmp	.L541
	.p2align 4,,10
	.p2align 3
.L585:
	movzbl	(%r15), %eax
	movb	%al, 16(%rbx)
.L540:
	movq	%r14, 8(%rbx)
	addq	$32, %r13
	addq	$32, %rbx
	movb	$0, (%rdi,%r14)
	cmpq	%r13, -72(%rbp)
	je	.L535
.L541:
	leaq	16(%rbx), %rdi
	movq	%rdi, (%rbx)
	movq	0(%r13), %r15
	movq	8(%r13), %r14
	movq	%r15, %rax
	addq	%r14, %rax
	je	.L536
	testq	%r15, %r15
	je	.L583
.L536:
	movq	%r14, -64(%rbp)
	cmpq	$15, %r14
	ja	.L584
	cmpq	$1, %r14
	je	.L585
	testq	%r14, %r14
	je	.L540
	.p2align 4,,10
	.p2align 3
.L538:
	movq	%r14, %rdx
	movq	%r15, %rsi
	call	memcpy@PLT
	movq	-64(%rbp), %r14
	movq	(%rbx), %rdi
	jmp	.L540
	.p2align 4,,10
	.p2align 3
.L530:
	testq	%r13, %r13
	jne	.L586
	movq	-104(%rbp), %rax
	jmp	.L531
	.p2align 4,,10
	.p2align 3
.L584:
	movq	-96(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rbx, %rdi
.LEHB31:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE31:
	movq	%rax, (%rbx)
	movq	%rax, %rdi
	movq	-64(%rbp), %rax
	movq	%rax, 16(%rbx)
	jmp	.L538
	.p2align 4,,10
	.p2align 3
.L535:
	movq	-80(%rbp), %rax
	movq	%rbx, 72(%r12)
	movq	%r12, (%rax)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L587
	movq	-80(%rbp), %rax
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L580:
	.cfi_restore_state
	leaq	-64(%rbp), %rsi
	leaq	32(%r12), %rdi
	xorl	%edx, %edx
.LEHB32:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, 32(%r12)
	movq	%rax, %rdi
	movq	-64(%rbp), %rax
	movq	%rax, 48(%r12)
.L529:
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-64(%rbp), %r13
	movq	32(%r12), %rax
	jmp	.L531
	.p2align 4,,10
	.p2align 3
.L581:
	movq	$0, -88(%rbp)
	jmp	.L533
.L579:
	leaq	.LC5(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE32:
.L583:
	leaq	.LC5(%rip), %rdi
.LEHB33:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE33:
.L587:
	call	__stack_chk_fail@PLT
.L582:
.LEHB34:
	call	_ZSt17__throw_bad_allocv@PLT
.LEHE34:
.L586:
	movq	-104(%rbp), %rdi
	jmp	.L529
.L554:
	endbr64
	movq	%rax, %r13
	jmp	.L550
.L557:
	endbr64
	movq	%rax, %rdi
	jmp	.L542
.L555:
	endbr64
	movq	%rax, %r13
	jmp	.L548
	.section	.gcc_except_table._ZNK2v88internal6torque23GotoExternalInstruction5CloneEv,"a",@progbits
	.align 4
.LLSDA6849:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATT6849-.LLSDATTD6849
.LLSDATTD6849:
	.byte	0x1
	.uleb128 .LLSDACSE6849-.LLSDACSB6849
.LLSDACSB6849:
	.uleb128 .LEHB29-.LFB6849
	.uleb128 .LEHE29-.LEHB29
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB30-.LFB6849
	.uleb128 .LEHE30-.LEHB30
	.uleb128 .L555-.LFB6849
	.uleb128 0
	.uleb128 .LEHB31-.LFB6849
	.uleb128 .LEHE31-.LEHB31
	.uleb128 .L557-.LFB6849
	.uleb128 0x1
	.uleb128 .LEHB32-.LFB6849
	.uleb128 .LEHE32-.LEHB32
	.uleb128 .L554-.LFB6849
	.uleb128 0
	.uleb128 .LEHB33-.LFB6849
	.uleb128 .LEHE33-.LEHB33
	.uleb128 .L557-.LFB6849
	.uleb128 0x1
	.uleb128 .LEHB34-.LFB6849
	.uleb128 .LEHE34-.LEHB34
	.uleb128 .L555-.LFB6849
	.uleb128 0
.LLSDACSE6849:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATT6849:
	.section	.text._ZNK2v88internal6torque23GotoExternalInstruction5CloneEv
	.cfi_endproc
	.section	.text.unlikely._ZNK2v88internal6torque23GotoExternalInstruction5CloneEv
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6849
	.type	_ZNK2v88internal6torque23GotoExternalInstruction5CloneEv.cold, @function
_ZNK2v88internal6torque23GotoExternalInstruction5CloneEv.cold:
.LFSB6849:
	nop
.L556:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	endbr64
	movq	%rax, %r13
	call	__cxa_end_catch@PLT
	movq	64(%r12), %rdi
	testq	%rdi, %rdi
	je	.L548
	call	_ZdlPv@PLT
.L548:
	movq	32(%r12), %rdi
	cmpq	%rdi, -104(%rbp)
	je	.L550
	call	_ZdlPv@PLT
.L550:
	movq	%r12, %rdi
	movl	$88, %esi
	call	_ZdlPvm@PLT
	movq	%r13, %rdi
.LEHB35:
	call	_Unwind_Resume@PLT
.LEHE35:
.L542:
	call	__cxa_begin_catch@PLT
.L545:
	cmpq	-88(%rbp), %rbx
	jne	.L588
.LEHB36:
	call	__cxa_rethrow@PLT
.LEHE36:
.L588:
	movq	-88(%rbp), %rax
	movq	(%rax), %rdi
	addq	$16, %rax
	cmpq	%rax, %rdi
	je	.L544
	call	_ZdlPv@PLT
.L544:
	addq	$32, -88(%rbp)
	jmp	.L545
	.cfi_endproc
.LFE6849:
	.section	.gcc_except_table._ZNK2v88internal6torque23GotoExternalInstruction5CloneEv
	.align 4
.LLSDAC6849:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATTC6849-.LLSDATTDC6849
.LLSDATTDC6849:
	.byte	0x1
	.uleb128 .LLSDACSEC6849-.LLSDACSBC6849
.LLSDACSBC6849:
	.uleb128 .LEHB35-.LCOLDB9
	.uleb128 .LEHE35-.LEHB35
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB36-.LCOLDB9
	.uleb128 .LEHE36-.LEHB36
	.uleb128 .L556-.LCOLDB9
	.uleb128 0
.LLSDACSEC6849:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATTC6849:
	.section	.text.unlikely._ZNK2v88internal6torque23GotoExternalInstruction5CloneEv
	.section	.text._ZNK2v88internal6torque23GotoExternalInstruction5CloneEv
	.size	_ZNK2v88internal6torque23GotoExternalInstruction5CloneEv, .-_ZNK2v88internal6torque23GotoExternalInstruction5CloneEv
	.section	.text.unlikely._ZNK2v88internal6torque23GotoExternalInstruction5CloneEv
	.size	_ZNK2v88internal6torque23GotoExternalInstruction5CloneEv.cold, .-_ZNK2v88internal6torque23GotoExternalInstruction5CloneEv.cold
.LCOLDE9:
	.section	.text._ZNK2v88internal6torque23GotoExternalInstruction5CloneEv
.LHOTE9:
	.section	.text.unlikely._ZNK2v88internal6torque26ConstexprBranchInstruction5CloneEv,"ax",@progbits
	.align 2
.LCOLDB10:
	.section	.text._ZNK2v88internal6torque26ConstexprBranchInstruction5CloneEv,"ax",@progbits
.LHOTB10:
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal6torque26ConstexprBranchInstruction5CloneEv
	.type	_ZNK2v88internal6torque26ConstexprBranchInstruction5CloneEv, @function
_ZNK2v88internal6torque26ConstexprBranchInstruction5CloneEv:
.LFB6837:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6837
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movl	$80, %edi
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
.LEHB37:
	call	_Znwm@PLT
.LEHE37:
	movdqu	8(%rbx), %xmm0
	movq	32(%rbx), %r15
	movq	%rax, %r12
	movq	40(%rbx), %r13
	movups	%xmm0, 8(%rax)
	movl	24(%rbx), %eax
	leaq	48(%r12), %rdi
	movq	%rdi, 32(%r12)
	movl	%eax, 24(%r12)
	leaq	16+_ZTVN2v88internal6torque26ConstexprBranchInstructionE(%rip), %rax
	movq	%rax, (%r12)
	movq	%r15, %rax
	addq	%r13, %rax
	je	.L590
	testq	%r15, %r15
	je	.L609
.L590:
	movq	%r13, -64(%rbp)
	cmpq	$15, %r13
	ja	.L610
	cmpq	$1, %r13
	jne	.L593
	movzbl	(%r15), %eax
	movb	%al, 48(%r12)
.L594:
	movq	%r13, 40(%r12)
	movb	$0, (%rdi,%r13)
	movdqu	64(%rbx), %xmm1
	movq	%r12, (%r14)
	movups	%xmm1, 64(%r12)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L611
	addq	$24, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L593:
	.cfi_restore_state
	testq	%r13, %r13
	je	.L594
	jmp	.L592
	.p2align 4,,10
	.p2align 3
.L610:
	leaq	-64(%rbp), %rsi
	leaq	32(%r12), %rdi
	xorl	%edx, %edx
.LEHB38:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, 32(%r12)
	movq	%rax, %rdi
	movq	-64(%rbp), %rax
	movq	%rax, 48(%r12)
.L592:
	movq	%r13, %rdx
	movq	%r15, %rsi
	call	memcpy@PLT
	movq	-64(%rbp), %r13
	movq	32(%r12), %rdi
	jmp	.L594
.L609:
	leaq	.LC5(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE38:
.L611:
	call	__stack_chk_fail@PLT
.L597:
	endbr64
	movq	%rax, %r13
	jmp	.L595
	.section	.gcc_except_table._ZNK2v88internal6torque26ConstexprBranchInstruction5CloneEv,"a",@progbits
.LLSDA6837:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6837-.LLSDACSB6837
.LLSDACSB6837:
	.uleb128 .LEHB37-.LFB6837
	.uleb128 .LEHE37-.LEHB37
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB38-.LFB6837
	.uleb128 .LEHE38-.LEHB38
	.uleb128 .L597-.LFB6837
	.uleb128 0
.LLSDACSE6837:
	.section	.text._ZNK2v88internal6torque26ConstexprBranchInstruction5CloneEv
	.cfi_endproc
	.section	.text.unlikely._ZNK2v88internal6torque26ConstexprBranchInstruction5CloneEv
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6837
	.type	_ZNK2v88internal6torque26ConstexprBranchInstruction5CloneEv.cold, @function
_ZNK2v88internal6torque26ConstexprBranchInstruction5CloneEv.cold:
.LFSB6837:
.L595:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	%r12, %rdi
	movl	$80, %esi
	call	_ZdlPvm@PLT
	movq	%r13, %rdi
.LEHB39:
	call	_Unwind_Resume@PLT
.LEHE39:
	.cfi_endproc
.LFE6837:
	.section	.gcc_except_table._ZNK2v88internal6torque26ConstexprBranchInstruction5CloneEv
.LLSDAC6837:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6837-.LLSDACSBC6837
.LLSDACSBC6837:
	.uleb128 .LEHB39-.LCOLDB10
	.uleb128 .LEHE39-.LEHB39
	.uleb128 0
	.uleb128 0
.LLSDACSEC6837:
	.section	.text.unlikely._ZNK2v88internal6torque26ConstexprBranchInstruction5CloneEv
	.section	.text._ZNK2v88internal6torque26ConstexprBranchInstruction5CloneEv
	.size	_ZNK2v88internal6torque26ConstexprBranchInstruction5CloneEv, .-_ZNK2v88internal6torque26ConstexprBranchInstruction5CloneEv
	.section	.text.unlikely._ZNK2v88internal6torque26ConstexprBranchInstruction5CloneEv
	.size	_ZNK2v88internal6torque26ConstexprBranchInstruction5CloneEv.cold, .-_ZNK2v88internal6torque26ConstexprBranchInstruction5CloneEv.cold
.LCOLDE10:
	.section	.text._ZNK2v88internal6torque26ConstexprBranchInstruction5CloneEv
.LHOTE10:
	.section	.text.unlikely._ZNK2v88internal6torque29PushBuiltinPointerInstruction5CloneEv,"ax",@progbits
	.align 2
.LCOLDB11:
	.section	.text._ZNK2v88internal6torque29PushBuiltinPointerInstruction5CloneEv,"ax",@progbits
.LHOTB11:
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal6torque29PushBuiltinPointerInstruction5CloneEv
	.type	_ZNK2v88internal6torque29PushBuiltinPointerInstruction5CloneEv, @function
_ZNK2v88internal6torque29PushBuiltinPointerInstruction5CloneEv:
.LFB6764:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6764
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movl	$72, %edi
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
.LEHB40:
	call	_Znwm@PLT
.LEHE40:
	movdqu	8(%rbx), %xmm0
	movq	32(%rbx), %r15
	movq	%rax, %r12
	movq	40(%rbx), %r13
	movups	%xmm0, 8(%rax)
	movl	24(%rbx), %eax
	leaq	48(%r12), %rdi
	movq	%rdi, 32(%r12)
	movl	%eax, 24(%r12)
	leaq	16+_ZTVN2v88internal6torque29PushBuiltinPointerInstructionE(%rip), %rax
	movq	%rax, (%r12)
	movq	%r15, %rax
	addq	%r13, %rax
	je	.L613
	testq	%r15, %r15
	je	.L632
.L613:
	movq	%r13, -64(%rbp)
	cmpq	$15, %r13
	ja	.L633
	cmpq	$1, %r13
	jne	.L616
	movzbl	(%r15), %eax
	movb	%al, 48(%r12)
.L617:
	movq	%r13, 40(%r12)
	movb	$0, (%rdi,%r13)
	movq	64(%rbx), %rax
	movq	%r12, (%r14)
	movq	%rax, 64(%r12)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L634
	addq	$24, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L616:
	.cfi_restore_state
	testq	%r13, %r13
	je	.L617
	jmp	.L615
	.p2align 4,,10
	.p2align 3
.L633:
	leaq	-64(%rbp), %rsi
	leaq	32(%r12), %rdi
	xorl	%edx, %edx
.LEHB41:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, 32(%r12)
	movq	%rax, %rdi
	movq	-64(%rbp), %rax
	movq	%rax, 48(%r12)
.L615:
	movq	%r13, %rdx
	movq	%r15, %rsi
	call	memcpy@PLT
	movq	-64(%rbp), %r13
	movq	32(%r12), %rdi
	jmp	.L617
.L632:
	leaq	.LC5(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE41:
.L634:
	call	__stack_chk_fail@PLT
.L620:
	endbr64
	movq	%rax, %r13
	jmp	.L618
	.section	.gcc_except_table._ZNK2v88internal6torque29PushBuiltinPointerInstruction5CloneEv,"a",@progbits
.LLSDA6764:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6764-.LLSDACSB6764
.LLSDACSB6764:
	.uleb128 .LEHB40-.LFB6764
	.uleb128 .LEHE40-.LEHB40
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB41-.LFB6764
	.uleb128 .LEHE41-.LEHB41
	.uleb128 .L620-.LFB6764
	.uleb128 0
.LLSDACSE6764:
	.section	.text._ZNK2v88internal6torque29PushBuiltinPointerInstruction5CloneEv
	.cfi_endproc
	.section	.text.unlikely._ZNK2v88internal6torque29PushBuiltinPointerInstruction5CloneEv
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6764
	.type	_ZNK2v88internal6torque29PushBuiltinPointerInstruction5CloneEv.cold, @function
_ZNK2v88internal6torque29PushBuiltinPointerInstruction5CloneEv.cold:
.LFSB6764:
.L618:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	%r12, %rdi
	movl	$72, %esi
	call	_ZdlPvm@PLT
	movq	%r13, %rdi
.LEHB42:
	call	_Unwind_Resume@PLT
.LEHE42:
	.cfi_endproc
.LFE6764:
	.section	.gcc_except_table._ZNK2v88internal6torque29PushBuiltinPointerInstruction5CloneEv
.LLSDAC6764:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6764-.LLSDACSBC6764
.LLSDACSBC6764:
	.uleb128 .LEHB42-.LCOLDB11
	.uleb128 .LEHE42-.LEHB42
	.uleb128 0
	.uleb128 0
.LLSDACSEC6764:
	.section	.text.unlikely._ZNK2v88internal6torque29PushBuiltinPointerInstruction5CloneEv
	.section	.text._ZNK2v88internal6torque29PushBuiltinPointerInstruction5CloneEv
	.size	_ZNK2v88internal6torque29PushBuiltinPointerInstruction5CloneEv, .-_ZNK2v88internal6torque29PushBuiltinPointerInstruction5CloneEv
	.section	.text.unlikely._ZNK2v88internal6torque29PushBuiltinPointerInstruction5CloneEv
	.size	_ZNK2v88internal6torque29PushBuiltinPointerInstruction5CloneEv.cold, .-_ZNK2v88internal6torque29PushBuiltinPointerInstruction5CloneEv.cold
.LCOLDE11:
	.section	.text._ZNK2v88internal6torque29PushBuiltinPointerInstruction5CloneEv
.LHOTE11:
	.section	.text.unlikely._ZNK2v88internal6torque31CreateFieldReferenceInstruction5CloneEv,"ax",@progbits
	.align 2
.LCOLDB12:
	.section	.text._ZNK2v88internal6torque31CreateFieldReferenceInstruction5CloneEv,"ax",@progbits
.LHOTB12:
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal6torque31CreateFieldReferenceInstruction5CloneEv
	.type	_ZNK2v88internal6torque31CreateFieldReferenceInstruction5CloneEv, @function
_ZNK2v88internal6torque31CreateFieldReferenceInstruction5CloneEv:
.LFB6770:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6770
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movl	$72, %edi
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
.LEHB43:
	call	_Znwm@PLT
.LEHE43:
	movdqu	8(%rbx), %xmm0
	movq	40(%rbx), %r15
	movq	%rax, %r12
	movq	48(%rbx), %r13
	movups	%xmm0, 8(%rax)
	movl	24(%rbx), %eax
	leaq	56(%r12), %rdi
	movq	%rdi, 40(%r12)
	movl	%eax, 24(%r12)
	leaq	16+_ZTVN2v88internal6torque31CreateFieldReferenceInstructionE(%rip), %rax
	movq	%rax, (%r12)
	movq	32(%rbx), %rax
	movq	%rax, 32(%r12)
	movq	%r15, %rax
	addq	%r13, %rax
	je	.L636
	testq	%r15, %r15
	je	.L655
.L636:
	movq	%r13, -64(%rbp)
	cmpq	$15, %r13
	ja	.L656
	cmpq	$1, %r13
	jne	.L639
	movzbl	(%r15), %eax
	movb	%al, 56(%r12)
.L640:
	movq	%r13, 48(%r12)
	movb	$0, (%rdi,%r13)
	movq	%r12, (%r14)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L657
	addq	$24, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L639:
	.cfi_restore_state
	testq	%r13, %r13
	je	.L640
	jmp	.L638
	.p2align 4,,10
	.p2align 3
.L656:
	leaq	-64(%rbp), %rsi
	leaq	40(%r12), %rdi
	xorl	%edx, %edx
.LEHB44:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, 40(%r12)
	movq	%rax, %rdi
	movq	-64(%rbp), %rax
	movq	%rax, 56(%r12)
.L638:
	movq	%r13, %rdx
	movq	%r15, %rsi
	call	memcpy@PLT
	movq	-64(%rbp), %r13
	movq	40(%r12), %rdi
	jmp	.L640
.L655:
	leaq	.LC5(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE44:
.L657:
	call	__stack_chk_fail@PLT
.L643:
	endbr64
	movq	%rax, %r13
	jmp	.L641
	.section	.gcc_except_table._ZNK2v88internal6torque31CreateFieldReferenceInstruction5CloneEv,"a",@progbits
.LLSDA6770:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6770-.LLSDACSB6770
.LLSDACSB6770:
	.uleb128 .LEHB43-.LFB6770
	.uleb128 .LEHE43-.LEHB43
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB44-.LFB6770
	.uleb128 .LEHE44-.LEHB44
	.uleb128 .L643-.LFB6770
	.uleb128 0
.LLSDACSE6770:
	.section	.text._ZNK2v88internal6torque31CreateFieldReferenceInstruction5CloneEv
	.cfi_endproc
	.section	.text.unlikely._ZNK2v88internal6torque31CreateFieldReferenceInstruction5CloneEv
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6770
	.type	_ZNK2v88internal6torque31CreateFieldReferenceInstruction5CloneEv.cold, @function
_ZNK2v88internal6torque31CreateFieldReferenceInstruction5CloneEv.cold:
.LFSB6770:
.L641:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	%r12, %rdi
	movl	$72, %esi
	call	_ZdlPvm@PLT
	movq	%r13, %rdi
.LEHB45:
	call	_Unwind_Resume@PLT
.LEHE45:
	.cfi_endproc
.LFE6770:
	.section	.gcc_except_table._ZNK2v88internal6torque31CreateFieldReferenceInstruction5CloneEv
.LLSDAC6770:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6770-.LLSDACSBC6770
.LLSDACSBC6770:
	.uleb128 .LEHB45-.LCOLDB12
	.uleb128 .LEHE45-.LEHB45
	.uleb128 0
	.uleb128 0
.LLSDACSEC6770:
	.section	.text.unlikely._ZNK2v88internal6torque31CreateFieldReferenceInstruction5CloneEv
	.section	.text._ZNK2v88internal6torque31CreateFieldReferenceInstruction5CloneEv
	.size	_ZNK2v88internal6torque31CreateFieldReferenceInstruction5CloneEv, .-_ZNK2v88internal6torque31CreateFieldReferenceInstruction5CloneEv
	.section	.text.unlikely._ZNK2v88internal6torque31CreateFieldReferenceInstruction5CloneEv
	.size	_ZNK2v88internal6torque31CreateFieldReferenceInstruction5CloneEv.cold, .-_ZNK2v88internal6torque31CreateFieldReferenceInstruction5CloneEv.cold
.LCOLDE12:
	.section	.text._ZNK2v88internal6torque31CreateFieldReferenceInstruction5CloneEv
.LHOTE12:
	.section	.text.unlikely._ZNK2v88internal6torque16AbortInstruction5CloneEv,"ax",@progbits
	.align 2
.LCOLDB13:
	.section	.text._ZNK2v88internal6torque16AbortInstruction5CloneEv,"ax",@progbits
.LHOTB13:
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal6torque16AbortInstruction5CloneEv
	.type	_ZNK2v88internal6torque16AbortInstruction5CloneEv, @function
_ZNK2v88internal6torque16AbortInstruction5CloneEv:
.LFB6867:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6867
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movl	$64, %edi
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
.LEHB46:
	call	_Znwm@PLT
.LEHE46:
	movdqu	8(%rbx), %xmm0
	movq	32(%rbx), %r15
	movq	%rax, %r12
	movq	40(%rbx), %r13
	movups	%xmm0, 8(%rax)
	movl	24(%rbx), %eax
	leaq	48(%r12), %rdi
	movq	%rdi, 32(%r12)
	movl	%eax, 24(%r12)
	leaq	16+_ZTVN2v88internal6torque16AbortInstructionE(%rip), %rax
	movq	%rax, (%r12)
	movl	28(%rbx), %eax
	movl	%eax, 28(%r12)
	movq	%r15, %rax
	addq	%r13, %rax
	je	.L659
	testq	%r15, %r15
	je	.L678
.L659:
	movq	%r13, -64(%rbp)
	cmpq	$15, %r13
	ja	.L679
	cmpq	$1, %r13
	jne	.L662
	movzbl	(%r15), %eax
	movb	%al, 48(%r12)
.L663:
	movq	%r13, 40(%r12)
	movb	$0, (%rdi,%r13)
	movq	%r12, (%r14)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L680
	addq	$24, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L662:
	.cfi_restore_state
	testq	%r13, %r13
	je	.L663
	jmp	.L661
	.p2align 4,,10
	.p2align 3
.L679:
	leaq	-64(%rbp), %rsi
	leaq	32(%r12), %rdi
	xorl	%edx, %edx
.LEHB47:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, 32(%r12)
	movq	%rax, %rdi
	movq	-64(%rbp), %rax
	movq	%rax, 48(%r12)
.L661:
	movq	%r13, %rdx
	movq	%r15, %rsi
	call	memcpy@PLT
	movq	-64(%rbp), %r13
	movq	32(%r12), %rdi
	jmp	.L663
.L678:
	leaq	.LC5(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE47:
.L680:
	call	__stack_chk_fail@PLT
.L666:
	endbr64
	movq	%rax, %r13
	jmp	.L664
	.section	.gcc_except_table._ZNK2v88internal6torque16AbortInstruction5CloneEv,"a",@progbits
.LLSDA6867:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6867-.LLSDACSB6867
.LLSDACSB6867:
	.uleb128 .LEHB46-.LFB6867
	.uleb128 .LEHE46-.LEHB46
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB47-.LFB6867
	.uleb128 .LEHE47-.LEHB47
	.uleb128 .L666-.LFB6867
	.uleb128 0
.LLSDACSE6867:
	.section	.text._ZNK2v88internal6torque16AbortInstruction5CloneEv
	.cfi_endproc
	.section	.text.unlikely._ZNK2v88internal6torque16AbortInstruction5CloneEv
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6867
	.type	_ZNK2v88internal6torque16AbortInstruction5CloneEv.cold, @function
_ZNK2v88internal6torque16AbortInstruction5CloneEv.cold:
.LFSB6867:
.L664:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	%r12, %rdi
	movl	$64, %esi
	call	_ZdlPvm@PLT
	movq	%r13, %rdi
.LEHB48:
	call	_Unwind_Resume@PLT
.LEHE48:
	.cfi_endproc
.LFE6867:
	.section	.gcc_except_table._ZNK2v88internal6torque16AbortInstruction5CloneEv
.LLSDAC6867:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6867-.LLSDACSBC6867
.LLSDACSBC6867:
	.uleb128 .LEHB48-.LCOLDB13
	.uleb128 .LEHE48-.LEHB48
	.uleb128 0
	.uleb128 0
.LLSDACSEC6867:
	.section	.text.unlikely._ZNK2v88internal6torque16AbortInstruction5CloneEv
	.section	.text._ZNK2v88internal6torque16AbortInstruction5CloneEv
	.size	_ZNK2v88internal6torque16AbortInstruction5CloneEv, .-_ZNK2v88internal6torque16AbortInstruction5CloneEv
	.section	.text.unlikely._ZNK2v88internal6torque16AbortInstruction5CloneEv
	.size	_ZNK2v88internal6torque16AbortInstruction5CloneEv.cold, .-_ZNK2v88internal6torque16AbortInstruction5CloneEv.cold
.LCOLDE13:
	.section	.text._ZNK2v88internal6torque16AbortInstruction5CloneEv
.LHOTE13:
	.section	.rodata._ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0.str1.1,"aMS",@progbits,1
.LC14:
	.string	"%d"
	.section	.text._ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0,"ax",@progbits
	.p2align 4
	.type	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0, @function
_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0:
.LFB12377:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	subq	$208, %rsp
	.cfi_offset 3, -32
	movq	%r8, -160(%rbp)
	movq	%r9, -152(%rbp)
	testb	%al, %al
	je	.L682
	movaps	%xmm0, -144(%rbp)
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm2, -112(%rbp)
	movaps	%xmm3, -96(%rbp)
	movaps	%xmm4, -80(%rbp)
	movaps	%xmm5, -64(%rbp)
	movaps	%xmm6, -48(%rbp)
	movaps	%xmm7, -32(%rbp)
.L682:
	movq	%fs:40, %rax
	movq	%rax, -200(%rbp)
	xorl	%eax, %eax
	movq	%rsp, %rax
	cmpq	%rax, %rsp
	je	.L684
.L703:
	subq	$4096, %rsp
	orq	$0, 4088(%rsp)
	cmpq	%rax, %rsp
	jne	.L703
.L684:
	subq	$32, %rsp
	orq	$0, 24(%rsp)
	movl	$16, %ecx
	movl	$16, %esi
	movl	$1, %edx
	leaq	.LC14(%rip), %r8
	leaq	15(%rsp), %rbx
	leaq	16(%rbp), %rax
	movl	$32, -224(%rbp)
	andq	$-16, %rbx
	movq	%rax, -216(%rbp)
	leaq	-224(%rbp), %r9
	leaq	-192(%rbp), %rax
	movq	%rbx, %rdi
	movq	%rax, -208(%rbp)
	movl	$48, -220(%rbp)
	call	__vsnprintf_chk@PLT
	leaq	16(%r12), %rcx
	movq	%rcx, (%r12)
	movslq	%eax, %rsi
	cmpl	$1, %eax
	jne	.L686
	movzbl	(%rbx), %eax
	movl	$1, %esi
	movb	%al, 16(%r12)
.L687:
	movq	%rsi, 8(%r12)
	movb	$0, (%rcx,%rsi)
	movq	-200(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L704
	leaq	-16(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L686:
	.cfi_restore_state
	cmpl	$8, %eax
	jnb	.L688
	testb	$4, %al
	jne	.L705
	testl	%eax, %eax
	je	.L689
	movzbl	(%rbx), %edx
	movb	%dl, 16(%r12)
	testb	$2, %al
	jne	.L706
.L689:
	movq	(%r12), %rcx
	jmp	.L687
	.p2align 4,,10
	.p2align 3
.L688:
	movq	(%rbx), %rdx
	movq	%rdx, 16(%r12)
	movl	%eax, %edx
	movq	-8(%rbx,%rdx), %rdi
	movq	%rdi, -8(%rcx,%rdx)
	leaq	24(%r12), %rdi
	andq	$-8, %rdi
	subq	%rdi, %rcx
	addl	%ecx, %eax
	subq	%rcx, %rbx
	andl	$-8, %eax
	cmpl	$8, %eax
	jb	.L689
	andl	$-8, %eax
	xorl	%edx, %edx
.L692:
	movl	%edx, %ecx
	addl	$8, %edx
	movq	(%rbx,%rcx), %r8
	movq	%r8, (%rdi,%rcx)
	cmpl	%eax, %edx
	jb	.L692
	jmp	.L689
	.p2align 4,,10
	.p2align 3
.L705:
	movl	(%rbx), %edx
	movl	%edx, 16(%r12)
	movl	%eax, %edx
	movl	-4(%rbx,%rdx), %eax
	movl	%eax, -4(%rcx,%rdx)
	jmp	.L689
.L706:
	movl	%eax, %edx
	movzwl	-2(%rbx,%rdx), %eax
	movw	%ax, -2(%rcx,%rdx)
	jmp	.L689
.L704:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE12377:
	.size	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0, .-_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0
	.section	.text.unlikely._ZNK2v88internal6torque30PrintConstantStringInstruction5CloneEv,"ax",@progbits
	.align 2
.LCOLDB15:
	.section	.text._ZNK2v88internal6torque30PrintConstantStringInstruction5CloneEv,"ax",@progbits
.LHOTB15:
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal6torque30PrintConstantStringInstruction5CloneEv
	.type	_ZNK2v88internal6torque30PrintConstantStringInstruction5CloneEv, @function
_ZNK2v88internal6torque30PrintConstantStringInstruction5CloneEv:
.LFB6861:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6861
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movl	$64, %edi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	subq	$16, %rsp
	.cfi_offset 12, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
.LEHB49:
	call	_Znwm@PLT
.LEHE49:
	movdqu	8(%r13), %xmm0
	movq	32(%r13), %r15
	movq	%rax, %r12
	movups	%xmm0, 8(%rax)
	movl	24(%r13), %eax
	movq	40(%r13), %r13
	leaq	48(%r12), %rdi
	movq	%rdi, 32(%r12)
	movl	%eax, 24(%r12)
	leaq	16+_ZTVN2v88internal6torque30PrintConstantStringInstructionE(%rip), %rax
	movq	%rax, (%r12)
	movq	%r15, %rax
	addq	%r13, %rax
	je	.L708
	testq	%r15, %r15
	je	.L727
.L708:
	movq	%r13, -48(%rbp)
	cmpq	$15, %r13
	ja	.L728
	cmpq	$1, %r13
	jne	.L711
	movzbl	(%r15), %eax
	movb	%al, 48(%r12)
.L712:
	movq	%r13, 40(%r12)
	movb	$0, (%rdi,%r13)
	movq	%r12, (%r14)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L729
	addq	$16, %rsp
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L711:
	.cfi_restore_state
	testq	%r13, %r13
	je	.L712
	jmp	.L710
	.p2align 4,,10
	.p2align 3
.L728:
	leaq	-48(%rbp), %rsi
	leaq	32(%r12), %rdi
	xorl	%edx, %edx
.LEHB50:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, 32(%r12)
	movq	%rax, %rdi
	movq	-48(%rbp), %rax
	movq	%rax, 48(%r12)
.L710:
	movq	%r13, %rdx
	movq	%r15, %rsi
	call	memcpy@PLT
	movq	-48(%rbp), %r13
	movq	32(%r12), %rdi
	jmp	.L712
.L727:
	leaq	.LC5(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE50:
.L729:
	call	__stack_chk_fail@PLT
.L715:
	endbr64
	movq	%rax, %r13
	jmp	.L713
	.section	.gcc_except_table._ZNK2v88internal6torque30PrintConstantStringInstruction5CloneEv,"a",@progbits
.LLSDA6861:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6861-.LLSDACSB6861
.LLSDACSB6861:
	.uleb128 .LEHB49-.LFB6861
	.uleb128 .LEHE49-.LEHB49
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB50-.LFB6861
	.uleb128 .LEHE50-.LEHB50
	.uleb128 .L715-.LFB6861
	.uleb128 0
.LLSDACSE6861:
	.section	.text._ZNK2v88internal6torque30PrintConstantStringInstruction5CloneEv
	.cfi_endproc
	.section	.text.unlikely._ZNK2v88internal6torque30PrintConstantStringInstruction5CloneEv
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6861
	.type	_ZNK2v88internal6torque30PrintConstantStringInstruction5CloneEv.cold, @function
_ZNK2v88internal6torque30PrintConstantStringInstruction5CloneEv.cold:
.LFSB6861:
.L713:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	%r12, %rdi
	movl	$64, %esi
	call	_ZdlPvm@PLT
	movq	%r13, %rdi
.LEHB51:
	call	_Unwind_Resume@PLT
.LEHE51:
	.cfi_endproc
.LFE6861:
	.section	.gcc_except_table._ZNK2v88internal6torque30PrintConstantStringInstruction5CloneEv
.LLSDAC6861:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6861-.LLSDACSBC6861
.LLSDACSBC6861:
	.uleb128 .LEHB51-.LCOLDB15
	.uleb128 .LEHE51-.LEHB51
	.uleb128 0
	.uleb128 0
.LLSDACSEC6861:
	.section	.text.unlikely._ZNK2v88internal6torque30PrintConstantStringInstruction5CloneEv
	.section	.text._ZNK2v88internal6torque30PrintConstantStringInstruction5CloneEv
	.size	_ZNK2v88internal6torque30PrintConstantStringInstruction5CloneEv, .-_ZNK2v88internal6torque30PrintConstantStringInstruction5CloneEv
	.section	.text.unlikely._ZNK2v88internal6torque30PrintConstantStringInstruction5CloneEv
	.size	_ZNK2v88internal6torque30PrintConstantStringInstruction5CloneEv.cold, .-_ZNK2v88internal6torque30PrintConstantStringInstruction5CloneEv.cold
.LCOLDE15:
	.section	.text._ZNK2v88internal6torque30PrintConstantStringInstruction5CloneEv
.LHOTE15:
	.section	.text._ZN2v88internal6torque14MessageBuilderD2Ev,"axG",@progbits,_ZN2v88internal6torque14MessageBuilderD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque14MessageBuilderD2Ev
	.type	_ZN2v88internal6torque14MessageBuilderD2Ev, @function
_ZN2v88internal6torque14MessageBuilderD2Ev:
.LFB4538:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA4538
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	addq	$16, %rbx
	subq	$8, %rsp
	call	_ZNK2v88internal6torque14MessageBuilder6ReportEv@PLT
	movq	-16(%rbx), %rdi
	cmpq	%rbx, %rdi
	je	.L730
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L730:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4538:
	.section	.gcc_except_table._ZN2v88internal6torque14MessageBuilderD2Ev,"aG",@progbits,_ZN2v88internal6torque14MessageBuilderD5Ev,comdat
.LLSDA4538:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE4538-.LLSDACSB4538
.LLSDACSB4538:
.LLSDACSE4538:
	.section	.text._ZN2v88internal6torque14MessageBuilderD2Ev,"axG",@progbits,_ZN2v88internal6torque14MessageBuilderD5Ev,comdat
	.size	_ZN2v88internal6torque14MessageBuilderD2Ev, .-_ZN2v88internal6torque14MessageBuilderD2Ev
	.weak	_ZN2v88internal6torque14MessageBuilderD1Ev
	.set	_ZN2v88internal6torque14MessageBuilderD1Ev,_ZN2v88internal6torque14MessageBuilderD2Ev
	.section	.text._ZN2v88internal6torquelsERSoRKNS1_4TypeE,"axG",@progbits,_ZN2v88internal6torquelsERSoRKNS1_4TypeE,comdat
	.p2align 4
	.weak	_ZN2v88internal6torquelsERSoRKNS1_4TypeE
	.type	_ZN2v88internal6torquelsERSoRKNS1_4TypeE, @function
_ZN2v88internal6torquelsERSoRKNS1_4TypeE:
.LFB5680:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA5680
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-64(%rbp), %rdi
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
.LEHB52:
	call	_ZNK2v88internal6torque4Type8ToStringB5cxx11Ev@PLT
.LEHE52:
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rsi
	movq	%r12, %rdi
.LEHB53:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE53:
	movq	-64(%rbp), %rdi
	leaq	-48(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L734
	call	_ZdlPv@PLT
.L734:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L741
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L741:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
.L738:
	endbr64
	movq	%rax, %r12
.L735:
	movq	-64(%rbp), %rdi
	leaq	-48(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L736
	call	_ZdlPv@PLT
.L736:
	movq	%r12, %rdi
.LEHB54:
	call	_Unwind_Resume@PLT
.LEHE54:
	.cfi_endproc
.LFE5680:
	.section	.gcc_except_table._ZN2v88internal6torquelsERSoRKNS1_4TypeE,"aG",@progbits,_ZN2v88internal6torquelsERSoRKNS1_4TypeE,comdat
.LLSDA5680:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE5680-.LLSDACSB5680
.LLSDACSB5680:
	.uleb128 .LEHB52-.LFB5680
	.uleb128 .LEHE52-.LEHB52
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB53-.LFB5680
	.uleb128 .LEHE53-.LEHB53
	.uleb128 .L738-.LFB5680
	.uleb128 0
	.uleb128 .LEHB54-.LFB5680
	.uleb128 .LEHE54-.LEHB54
	.uleb128 0
	.uleb128 0
.LLSDACSE5680:
	.section	.text._ZN2v88internal6torquelsERSoRKNS1_4TypeE,"axG",@progbits,_ZN2v88internal6torquelsERSoRKNS1_4TypeE,comdat
	.size	_ZN2v88internal6torquelsERSoRKNS1_4TypeE, .-_ZN2v88internal6torquelsERSoRKNS1_4TypeE
	.section	.text._ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED2Ev,"axG",@progbits,_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED2Ev
	.type	_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED2Ev, @function
_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED2Ev:
.LFB7537:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	72(%rdi), %rdi
	leaq	88(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L743
	call	_ZdlPv@PLT
.L743:
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	56(%rbx), %rdi
	movq	%rax, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt6localeD1Ev@PLT
	.cfi_endproc
.LFE7537:
	.size	_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED2Ev, .-_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED2Ev
	.weak	_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED1Ev
	.set	_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED1Ev,_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED2Ev
	.section	.text._ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED0Ev,"axG",@progbits,_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED0Ev
	.type	_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED0Ev, @function
_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED0Ev:
.LFB7539:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	72(%rdi), %rdi
	leaq	88(%r12), %rax
	cmpq	%rax, %rdi
	je	.L746
	call	_ZdlPv@PLT
.L746:
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	56(%r12), %rdi
	movq	%rax, (%r12)
	call	_ZNSt6localeD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$104, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE7539:
	.size	_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED0Ev, .-_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED0Ev
	.section	.rodata._ZNK2v88internal6torque7TopType16ToExplicitStringB5cxx11Ev.str1.1,"aMS",@progbits,1
.LC16:
	.string	"inaccessible "
	.section	.text._ZNK2v88internal6torque7TopType16ToExplicitStringB5cxx11Ev,"axG",@progbits,_ZNK2v88internal6torque7TopType16ToExplicitStringB5cxx11Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal6torque7TopType16ToExplicitStringB5cxx11Ev
	.type	_ZNK2v88internal6torque7TopType16ToExplicitStringB5cxx11Ev, @function
_ZNK2v88internal6torque7TopType16ToExplicitStringB5cxx11Ev:
.LFB5470:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA5470
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-448(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-320(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$504, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r14, -536(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	movq	%rax, -320(%rbp)
	xorl	%eax, %eax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movw	%ax, -96(%rbp)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	$0, -104(%rbp)
	movq	%rax, -448(%rbp)
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	-24(%rax), %rax
	movq	%rdx, -448(%rbp,%rax)
	movq	-448(%rbp), %rax
	movq	$0, -440(%rbp)
	movq	-24(%rax), %rdi
	addq	%r14, %rdi
.LEHB55:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE55:
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	xorl	%esi, %esi
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rdx, -432(%rbp,%rax)
	leaq	-432(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -528(%rbp)
	movq	-432(%rbp), %rax
	addq	-24(%rax), %rdi
.LEHB56:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE56:
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	leaq	-368(%rbp), %r14
	leaq	-424(%rbp), %r15
	movq	.LC17(%rip), %xmm0
	movq	%r14, %rdi
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movhps	.LC18(%rip), %xmm0
	movq	%rdx, -448(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movaps	%xmm0, -432(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rax, -320(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movl	$24, -360(%rbp)
	movq	%rax, -520(%rbp)
	movq	%rax, -352(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
.LEHB57:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE57:
	leaq	-512(%rbp), %r15
	movq	104(%rbx), %rsi
	movq	%r15, %rdi
.LEHB58:
	call	_ZNK2v88internal6torque4Type8ToStringB5cxx11Ev@PLT
.LEHE58:
	movl	$13, %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	leaq	.LC16(%rip), %rcx
.LEHB59:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.LEHE59:
	leaq	-464(%rbp), %rbx
	leaq	16(%rax), %rdx
	movq	%rbx, -480(%rbp)
	movq	(%rax), %rcx
	cmpq	%rdx, %rcx
	je	.L781
	movq	%rcx, -480(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -464(%rbp)
.L754:
	movq	8(%rax), %rcx
	movq	-528(%rbp), %rdi
	movb	$0, 16(%rax)
	movq	%rcx, -472(%rbp)
	movq	%rdx, (%rax)
	movq	-480(%rbp), %rsi
	movq	$0, 8(%rax)
	movq	-472(%rbp), %rdx
.LEHB60:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE60:
	movq	-480(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L755
	call	_ZdlPv@PLT
.L755:
	movq	-512(%rbp), %rdi
	leaq	-496(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L756
	call	_ZdlPv@PLT
.L756:
	movq	-384(%rbp), %rax
	leaq	16(%r12), %rbx
	movq	$0, 8(%r12)
	movq	%rbx, (%r12)
	movb	$0, 16(%r12)
	testq	%rax, %rax
	je	.L757
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	ja	.L782
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
.LEHB61:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L759:
	movq	.LC17(%rip), %xmm0
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-352(%rbp), %rdi
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movhps	.LC19(%rip), %xmm0
	movq	%rax, -320(%rbp)
	movaps	%xmm0, -432(%rbp)
	cmpq	-520(%rbp), %rdi
	je	.L761
	call	_ZdlPv@PLT
.L761:
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%r14, %rdi
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	movq	%r13, %rdi
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rbx, -448(%rbp,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rbx, -432(%rbp,%rax)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	movq	$0, -440(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L783
	addq	$504, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L782:
	.cfi_restore_state
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L759
	.p2align 4,,10
	.p2align 3
.L781:
	movdqu	16(%rax), %xmm1
	movaps	%xmm1, -464(%rbp)
	jmp	.L754
	.p2align 4,,10
	.p2align 3
.L757:
	leaq	-352(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
.LEHE61:
	jmp	.L759
.L783:
	call	__stack_chk_fail@PLT
.L771:
	endbr64
	movq	%rax, %r12
	jmp	.L767
.L772:
	endbr64
	movq	%rax, %r12
	jmp	.L765
.L770:
	endbr64
	movq	%rax, %r12
	jmp	.L764
.L765:
	movq	-480(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L767
	call	_ZdlPv@PLT
.L767:
	movq	-512(%rbp), %rdi
	leaq	-496(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L764
	call	_ZdlPv@PLT
.L764:
	movq	-536(%rbp), %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r12, %rdi
.LEHB62:
	call	_Unwind_Resume@PLT
.L774:
	endbr64
	movq	%rax, %rbx
	jmp	.L752
.L775:
	endbr64
	movq	%rax, %rbx
	jmp	.L750
.L752:
	movq	%r15, %rdi
	call	_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED1Ev
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rdx, -448(%rbp,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rdx, -432(%rbp,%rax)
.L780:
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rdx, -448(%rbp,%rax)
	movq	$0, -440(%rbp)
.L751:
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	%rbx, %rdi
	call	_Unwind_Resume@PLT
.LEHE62:
.L750:
	jmp	.L780
.L773:
	endbr64
	movq	%rax, %rbx
	jmp	.L751
.L776:
	endbr64
	movq	%rax, %r13
.L762:
	movq	(%r12), %rdi
	cmpq	%rdi, %rbx
	je	.L763
	call	_ZdlPv@PLT
.L763:
	movq	%r13, %r12
	jmp	.L764
	.cfi_endproc
.LFE5470:
	.section	.gcc_except_table._ZNK2v88internal6torque7TopType16ToExplicitStringB5cxx11Ev,"aG",@progbits,_ZNK2v88internal6torque7TopType16ToExplicitStringB5cxx11Ev,comdat
.LLSDA5470:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE5470-.LLSDACSB5470
.LLSDACSB5470:
	.uleb128 .LEHB55-.LFB5470
	.uleb128 .LEHE55-.LEHB55
	.uleb128 .L773-.LFB5470
	.uleb128 0
	.uleb128 .LEHB56-.LFB5470
	.uleb128 .LEHE56-.LEHB56
	.uleb128 .L775-.LFB5470
	.uleb128 0
	.uleb128 .LEHB57-.LFB5470
	.uleb128 .LEHE57-.LEHB57
	.uleb128 .L774-.LFB5470
	.uleb128 0
	.uleb128 .LEHB58-.LFB5470
	.uleb128 .LEHE58-.LEHB58
	.uleb128 .L770-.LFB5470
	.uleb128 0
	.uleb128 .LEHB59-.LFB5470
	.uleb128 .LEHE59-.LEHB59
	.uleb128 .L771-.LFB5470
	.uleb128 0
	.uleb128 .LEHB60-.LFB5470
	.uleb128 .LEHE60-.LEHB60
	.uleb128 .L772-.LFB5470
	.uleb128 0
	.uleb128 .LEHB61-.LFB5470
	.uleb128 .LEHE61-.LEHB61
	.uleb128 .L776-.LFB5470
	.uleb128 0
	.uleb128 .LEHB62-.LFB5470
	.uleb128 .LEHE62-.LEHB62
	.uleb128 0
	.uleb128 0
.LLSDACSE5470:
	.section	.text._ZNK2v88internal6torque7TopType16ToExplicitStringB5cxx11Ev,"axG",@progbits,_ZNK2v88internal6torque7TopType16ToExplicitStringB5cxx11Ev,comdat
	.size	_ZNK2v88internal6torque7TopType16ToExplicitStringB5cxx11Ev, .-_ZNK2v88internal6torque7TopType16ToExplicitStringB5cxx11Ev
	.section	.text.unlikely._ZN2v88internal6torqueL7MessageIJRA23_KcEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0,"ax",@progbits
.LCOLDB20:
	.section	.text._ZN2v88internal6torqueL7MessageIJRA23_KcEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0,"ax",@progbits
.LHOTB20:
	.p2align 4
	.type	_ZN2v88internal6torqueL7MessageIJRA23_KcEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0, @function
_ZN2v88internal6torqueL7MessageIJRA23_KcEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0:
.LFB12381:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA12381
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-432(%rbp), %r14
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%r14, %rdi
	subq	$440, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
.LEHB63:
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEC1Ev@PLT
.LEHE63:
	leaq	-416(%rbp), %rdi
	movq	%r13, %rsi
.LEHB64:
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	leaq	-464(%rbp), %r13
	leaq	-408(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNKSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEE3strEv@PLT
.LEHE64:
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
.LEHB65:
	call	_ZN2v88internal6torque14MessageBuilderC1ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_13TorqueMessage4KindE@PLT
.LEHE65:
	movq	-464(%rbp), %rdi
	leaq	-448(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L785
	call	_ZdlPv@PLT
.L785:
	movq	%r14, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L794
	addq	$440, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L794:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
.L791:
	endbr64
	movq	%rax, %r12
	jmp	.L786
.L790:
	endbr64
	movq	%rax, %r12
	jmp	.L788
	.section	.gcc_except_table._ZN2v88internal6torqueL7MessageIJRA23_KcEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0,"a",@progbits
.LLSDA12381:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE12381-.LLSDACSB12381
.LLSDACSB12381:
	.uleb128 .LEHB63-.LFB12381
	.uleb128 .LEHE63-.LEHB63
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB64-.LFB12381
	.uleb128 .LEHE64-.LEHB64
	.uleb128 .L790-.LFB12381
	.uleb128 0
	.uleb128 .LEHB65-.LFB12381
	.uleb128 .LEHE65-.LEHB65
	.uleb128 .L791-.LFB12381
	.uleb128 0
.LLSDACSE12381:
	.section	.text._ZN2v88internal6torqueL7MessageIJRA23_KcEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torqueL7MessageIJRA23_KcEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC12381
	.type	_ZN2v88internal6torqueL7MessageIJRA23_KcEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0.cold, @function
_ZN2v88internal6torqueL7MessageIJRA23_KcEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0.cold:
.LFSB12381:
.L786:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	movq	-464(%rbp), %rdi
	leaq	-448(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L788
	call	_ZdlPv@PLT
.L788:
	movq	%r14, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r12, %rdi
.LEHB66:
	call	_Unwind_Resume@PLT
.LEHE66:
	.cfi_endproc
.LFE12381:
	.section	.gcc_except_table._ZN2v88internal6torqueL7MessageIJRA23_KcEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
.LLSDAC12381:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC12381-.LLSDACSBC12381
.LLSDACSBC12381:
	.uleb128 .LEHB66-.LCOLDB20
	.uleb128 .LEHE66-.LEHB66
	.uleb128 0
	.uleb128 0
.LLSDACSEC12381:
	.section	.text.unlikely._ZN2v88internal6torqueL7MessageIJRA23_KcEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
	.section	.text._ZN2v88internal6torqueL7MessageIJRA23_KcEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
	.size	_ZN2v88internal6torqueL7MessageIJRA23_KcEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0, .-_ZN2v88internal6torqueL7MessageIJRA23_KcEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
	.section	.text.unlikely._ZN2v88internal6torqueL7MessageIJRA23_KcEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
	.size	_ZN2v88internal6torqueL7MessageIJRA23_KcEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0.cold, .-_ZN2v88internal6torqueL7MessageIJRA23_KcEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0.cold
.LCOLDE20:
	.section	.text._ZN2v88internal6torqueL7MessageIJRA23_KcEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
.LHOTE20:
	.section	.rodata._ZNK2v88internal6torque23GotoExternalInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE.str1.8,"aMS",@progbits,1
	.align 8
.LC21:
	.string	"goto external label with wrong parameter count."
	.section	.text.unlikely._ZNK2v88internal6torque23GotoExternalInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE,"ax",@progbits
	.align 2
.LCOLDB22:
	.section	.text._ZNK2v88internal6torque23GotoExternalInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE,"ax",@progbits
.LHOTB22:
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal6torque23GotoExternalInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.type	_ZNK2v88internal6torque23GotoExternalInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE, @function
_ZNK2v88internal6torque23GotoExternalInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE:
.LFB6897:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6897
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	subq	$80, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	8(%rsi), %rax
	subq	(%rsi), %rax
	sarq	$3, %rax
	movq	%rax, %rdx
	movq	72(%rdi), %rax
	subq	64(%rdi), %rax
	sarq	$5, %rax
	cmpq	%rax, %rdx
	jne	.L801
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L802
	addq	$80, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L801:
	.cfi_restore_state
	leaq	-96(%rbp), %r13
	leaq	.LC21(%rip), %rsi
	movq	%r13, %rdi
.LEHB67:
	call	_ZN2v88internal6torqueL7MessageIJRA23_KcEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
.LEHE67:
	movq	%r13, %rdi
.LEHB68:
	call	_ZNK2v88internal6torque14MessageBuilder5ThrowEv@PLT
.LEHE68:
.L802:
	call	__stack_chk_fail@PLT
.L799:
	endbr64
	movq	%rax, %r12
	jmp	.L797
	.section	.gcc_except_table._ZNK2v88internal6torque23GotoExternalInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE,"a",@progbits
.LLSDA6897:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6897-.LLSDACSB6897
.LLSDACSB6897:
	.uleb128 .LEHB67-.LFB6897
	.uleb128 .LEHE67-.LEHB67
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB68-.LFB6897
	.uleb128 .LEHE68-.LEHB68
	.uleb128 .L799-.LFB6897
	.uleb128 0
.LLSDACSE6897:
	.section	.text._ZNK2v88internal6torque23GotoExternalInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.cfi_endproc
	.section	.text.unlikely._ZNK2v88internal6torque23GotoExternalInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6897
	.type	_ZNK2v88internal6torque23GotoExternalInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE.cold, @function
_ZNK2v88internal6torque23GotoExternalInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE.cold:
.LFSB6897:
.L797:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movq	%r13, %rdi
	call	_ZN2v88internal6torque14MessageBuilderD1Ev
	movq	%r12, %rdi
.LEHB69:
	call	_Unwind_Resume@PLT
.LEHE69:
	.cfi_endproc
.LFE6897:
	.section	.gcc_except_table._ZNK2v88internal6torque23GotoExternalInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
.LLSDAC6897:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6897-.LLSDACSBC6897
.LLSDACSBC6897:
	.uleb128 .LEHB69-.LCOLDB22
	.uleb128 .LEHE69-.LEHB69
	.uleb128 0
	.uleb128 0
.LLSDACSEC6897:
	.section	.text.unlikely._ZNK2v88internal6torque23GotoExternalInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.section	.text._ZNK2v88internal6torque23GotoExternalInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.size	_ZNK2v88internal6torque23GotoExternalInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE, .-_ZNK2v88internal6torque23GotoExternalInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.section	.text.unlikely._ZNK2v88internal6torque23GotoExternalInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.size	_ZNK2v88internal6torque23GotoExternalInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE.cold, .-_ZNK2v88internal6torque23GotoExternalInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE.cold
.LCOLDE22:
	.section	.text._ZNK2v88internal6torque23GotoExternalInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
.LHOTE22:
	.section	.rodata._ZNK2v88internal6torque17BranchInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE.str1.8,"aMS",@progbits,1
	.align 8
.LC23:
	.string	"condition has to have type bool"
	.section	.text.unlikely._ZNK2v88internal6torque17BranchInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE,"ax",@progbits
	.align 2
.LCOLDB24:
	.section	.text._ZNK2v88internal6torque17BranchInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE,"ax",@progbits
.LHOTB24:
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal6torque17BranchInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.type	_ZNK2v88internal6torque17BranchInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE, @function
_ZNK2v88internal6torque17BranchInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE:
.LFB6894:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6894
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-128(%rbp), %r15
	leaq	-112(%rbp), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8(%rsi), %rax
	movq	-8(%rax), %r13
	subq	$8, %rax
	movq	%rax, 8(%rsi)
.LEHB70:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_10TypeOracleEEERPNT_5ScopeEv@PLT
.LEHE70:
	movq	%r15, %rdi
	movq	%r14, -128(%rbp)
	movl	$1819242338, -112(%rbp)
	movq	$4, -120(%rbp)
	movb	$0, -108(%rbp)
.LEHB71:
	call	_ZN2v88internal6torque12Declarations16LookupGlobalTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
.LEHE71:
	movq	-128(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L804
	movq	%rax, -136(%rbp)
	call	_ZdlPv@PLT
	movq	-136(%rbp), %rax
.L804:
	cmpq	%rax, %r13
	jne	.L815
	movq	32(%rbx), %rdi
	movq	%r12, %rsi
.LEHB72:
	call	_ZN2v88internal6torque5Block13SetInputTypesERKNS1_5StackIPKNS1_4TypeEEE@PLT
	movq	40(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal6torque5Block13SetInputTypesERKNS1_5StackIPKNS1_4TypeEEE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L816
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L815:
	.cfi_restore_state
	movq	%r15, %rdi
	leaq	.LC23(%rip), %rsi
	call	_ZN2v88internal6torqueL7MessageIJRA23_KcEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
.LEHE72:
	movq	%r15, %rdi
.LEHB73:
	call	_ZNK2v88internal6torque14MessageBuilder5ThrowEv@PLT
.LEHE73:
.L816:
	call	__stack_chk_fail@PLT
.L811:
	endbr64
	movq	%rax, %r12
	jmp	.L807
.L812:
	endbr64
	movq	%rax, %r12
	jmp	.L809
	.section	.gcc_except_table._ZNK2v88internal6torque17BranchInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE,"a",@progbits
.LLSDA6894:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6894-.LLSDACSB6894
.LLSDACSB6894:
	.uleb128 .LEHB70-.LFB6894
	.uleb128 .LEHE70-.LEHB70
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB71-.LFB6894
	.uleb128 .LEHE71-.LEHB71
	.uleb128 .L811-.LFB6894
	.uleb128 0
	.uleb128 .LEHB72-.LFB6894
	.uleb128 .LEHE72-.LEHB72
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB73-.LFB6894
	.uleb128 .LEHE73-.LEHB73
	.uleb128 .L812-.LFB6894
	.uleb128 0
.LLSDACSE6894:
	.section	.text._ZNK2v88internal6torque17BranchInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.cfi_endproc
	.section	.text.unlikely._ZNK2v88internal6torque17BranchInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6894
	.type	_ZNK2v88internal6torque17BranchInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE.cold, @function
_ZNK2v88internal6torque17BranchInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE.cold:
.LFSB6894:
.L807:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-128(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L808
	call	_ZdlPv@PLT
.L808:
	movq	%r12, %rdi
.LEHB74:
	call	_Unwind_Resume@PLT
.L809:
	movq	%r15, %rdi
	call	_ZN2v88internal6torque14MessageBuilderD1Ev
	movq	%r12, %rdi
	call	_Unwind_Resume@PLT
.LEHE74:
	.cfi_endproc
.LFE6894:
	.section	.gcc_except_table._ZNK2v88internal6torque17BranchInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
.LLSDAC6894:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6894-.LLSDACSBC6894
.LLSDACSBC6894:
	.uleb128 .LEHB74-.LCOLDB24
	.uleb128 .LEHE74-.LEHB74
	.uleb128 0
	.uleb128 0
.LLSDACSEC6894:
	.section	.text.unlikely._ZNK2v88internal6torque17BranchInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.section	.text._ZNK2v88internal6torque17BranchInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.size	_ZNK2v88internal6torque17BranchInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE, .-_ZNK2v88internal6torque17BranchInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.section	.text.unlikely._ZNK2v88internal6torque17BranchInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.size	_ZNK2v88internal6torque17BranchInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE.cold, .-_ZNK2v88internal6torque17BranchInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE.cold
.LCOLDE24:
	.section	.text._ZNK2v88internal6torque17BranchInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
.LHOTE24:
	.section	.text.unlikely._ZN2v88internal6torqueL7MessageIJRA22_KcRKNS1_4TypeERA13_S3_S8_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0,"ax",@progbits
.LCOLDB25:
	.section	.text._ZN2v88internal6torqueL7MessageIJRA22_KcRKNS1_4TypeERA13_S3_S8_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0,"ax",@progbits
.LHOTB25:
	.p2align 4
	.type	_ZN2v88internal6torqueL7MessageIJRA22_KcRKNS1_4TypeERA13_S3_S8_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0, @function
_ZN2v88internal6torqueL7MessageIJRA22_KcRKNS1_4TypeERA13_S3_S8_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0:
.LFB12378:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA12378
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-432(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-448(%rbp), %rbx
	movq	%rbx, %rdi
	subq	$456, %rsp
	movq	%rsi, -496(%rbp)
	movq	%r8, -488(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
.LEHB75:
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEC1Ev@PLT
.LEHE75:
	movq	-496(%rbp), %rsi
	movq	%r12, %rdi
.LEHB76:
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6torquelsERSoRKNS1_4TypeE
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	-488(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6torquelsERSoRKNS1_4TypeE
	leaq	-480(%rbp), %r12
	leaq	-424(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNKSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEE3strEv@PLT
.LEHE76:
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
.LEHB77:
	call	_ZN2v88internal6torque14MessageBuilderC1ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_13TorqueMessage4KindE@PLT
.LEHE77:
	movq	-480(%rbp), %rdi
	leaq	-464(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L818
	call	_ZdlPv@PLT
.L818:
	movq	%rbx, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L827
	addq	$456, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L827:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
.L824:
	endbr64
	movq	%rax, %r12
	jmp	.L819
.L823:
	endbr64
	movq	%rax, %r12
	jmp	.L821
	.section	.gcc_except_table._ZN2v88internal6torqueL7MessageIJRA22_KcRKNS1_4TypeERA13_S3_S8_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0,"a",@progbits
.LLSDA12378:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE12378-.LLSDACSB12378
.LLSDACSB12378:
	.uleb128 .LEHB75-.LFB12378
	.uleb128 .LEHE75-.LEHB75
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB76-.LFB12378
	.uleb128 .LEHE76-.LEHB76
	.uleb128 .L823-.LFB12378
	.uleb128 0
	.uleb128 .LEHB77-.LFB12378
	.uleb128 .LEHE77-.LEHB77
	.uleb128 .L824-.LFB12378
	.uleb128 0
.LLSDACSE12378:
	.section	.text._ZN2v88internal6torqueL7MessageIJRA22_KcRKNS1_4TypeERA13_S3_S8_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torqueL7MessageIJRA22_KcRKNS1_4TypeERA13_S3_S8_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC12378
	.type	_ZN2v88internal6torqueL7MessageIJRA22_KcRKNS1_4TypeERA13_S3_S8_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0.cold, @function
_ZN2v88internal6torqueL7MessageIJRA22_KcRKNS1_4TypeERA13_S3_S8_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0.cold:
.LFSB12378:
.L819:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-480(%rbp), %rdi
	leaq	-464(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L821
	call	_ZdlPv@PLT
.L821:
	movq	%rbx, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r12, %rdi
.LEHB78:
	call	_Unwind_Resume@PLT
.LEHE78:
	.cfi_endproc
.LFE12378:
	.section	.gcc_except_table._ZN2v88internal6torqueL7MessageIJRA22_KcRKNS1_4TypeERA13_S3_S8_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
.LLSDAC12378:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC12378-.LLSDACSBC12378
.LLSDACSBC12378:
	.uleb128 .LEHB78-.LCOLDB25
	.uleb128 .LEHE78-.LEHB78
	.uleb128 0
	.uleb128 0
.LLSDACSEC12378:
	.section	.text.unlikely._ZN2v88internal6torqueL7MessageIJRA22_KcRKNS1_4TypeERA13_S3_S8_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
	.section	.text._ZN2v88internal6torqueL7MessageIJRA22_KcRKNS1_4TypeERA13_S3_S8_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
	.size	_ZN2v88internal6torqueL7MessageIJRA22_KcRKNS1_4TypeERA13_S3_S8_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0, .-_ZN2v88internal6torqueL7MessageIJRA22_KcRKNS1_4TypeERA13_S3_S8_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
	.section	.text.unlikely._ZN2v88internal6torqueL7MessageIJRA22_KcRKNS1_4TypeERA13_S3_S8_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
	.size	_ZN2v88internal6torqueL7MessageIJRA22_KcRKNS1_4TypeERA13_S3_S8_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0.cold, .-_ZN2v88internal6torqueL7MessageIJRA22_KcRKNS1_4TypeERA13_S3_S8_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0.cold
.LCOLDE25:
	.section	.text._ZN2v88internal6torqueL7MessageIJRA22_KcRKNS1_4TypeERA13_S3_S8_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
.LHOTE25:
	.section	.rodata._ZNK2v88internal6torque17ReturnInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE.str1.1,"aMS",@progbits,1
.LC26:
	.string	" instead of "
.LC27:
	.string	"expected return type "
	.section	.text.unlikely._ZNK2v88internal6torque17ReturnInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE,"ax",@progbits
	.align 2
.LCOLDB28:
	.section	.text._ZNK2v88internal6torque17ReturnInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE,"ax",@progbits
.LHOTB28:
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal6torque17ReturnInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.type	_ZNK2v88internal6torque17ReturnInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE, @function
_ZNK2v88internal6torque17ReturnInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE:
.LFB6898:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6898
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	subq	$80, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	8(%rsi), %rax
	movq	-8(%rax), %r8
	subq	$8, %rax
	movq	%rax, 8(%rsi)
	cmpb	$0, 72(%rdx)
	je	.L835
	movq	80(%rdx), %rdx
	cmpq	%rdx, %r8
	jne	.L836
.L828:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L837
	addq	$80, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L835:
	.cfi_restore_state
	movq	%r8, 80(%rdx)
	movb	$1, 72(%rdx)
	jmp	.L828
.L837:
	call	__stack_chk_fail@PLT
.L836:
	leaq	-96(%rbp), %r13
	leaq	.LC26(%rip), %rcx
	movq	%r13, %rdi
	leaq	.LC27(%rip), %rsi
.LEHB79:
	call	_ZN2v88internal6torqueL7MessageIJRA22_KcRKNS1_4TypeERA13_S3_S8_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
.LEHE79:
	movq	%r13, %rdi
.LEHB80:
	call	_ZNK2v88internal6torque14MessageBuilder5ThrowEv@PLT
.LEHE80:
.L833:
	endbr64
	movq	%rax, %r12
	jmp	.L831
	.section	.gcc_except_table._ZNK2v88internal6torque17ReturnInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE,"a",@progbits
.LLSDA6898:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6898-.LLSDACSB6898
.LLSDACSB6898:
	.uleb128 .LEHB79-.LFB6898
	.uleb128 .LEHE79-.LEHB79
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB80-.LFB6898
	.uleb128 .LEHE80-.LEHB80
	.uleb128 .L833-.LFB6898
	.uleb128 0
.LLSDACSE6898:
	.section	.text._ZNK2v88internal6torque17ReturnInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.cfi_endproc
	.section	.text.unlikely._ZNK2v88internal6torque17ReturnInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6898
	.type	_ZNK2v88internal6torque17ReturnInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE.cold, @function
_ZNK2v88internal6torque17ReturnInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE.cold:
.LFSB6898:
.L831:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movq	%r13, %rdi
	call	_ZN2v88internal6torque14MessageBuilderD1Ev
	movq	%r12, %rdi
.LEHB81:
	call	_Unwind_Resume@PLT
.LEHE81:
	.cfi_endproc
.LFE6898:
	.section	.gcc_except_table._ZNK2v88internal6torque17ReturnInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
.LLSDAC6898:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6898-.LLSDACSBC6898
.LLSDACSBC6898:
	.uleb128 .LEHB81-.LCOLDB28
	.uleb128 .LEHE81-.LEHB81
	.uleb128 0
	.uleb128 0
.LLSDACSEC6898:
	.section	.text.unlikely._ZNK2v88internal6torque17ReturnInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.section	.text._ZNK2v88internal6torque17ReturnInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.size	_ZNK2v88internal6torque17ReturnInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE, .-_ZNK2v88internal6torque17ReturnInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.section	.text.unlikely._ZNK2v88internal6torque17ReturnInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.size	_ZNK2v88internal6torque17ReturnInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE.cold, .-_ZNK2v88internal6torque17ReturnInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE.cold
.LCOLDE28:
	.section	.text._ZNK2v88internal6torque17ReturnInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
.LHOTE28:
	.section	.rodata._ZN2v88internal6torque12_GLOBAL__N_113ExpectSubtypeEPKNS1_4TypeES5_.part.0.str1.1,"aMS",@progbits,1
.LC29:
	.string	" is not a subtype of "
.LC30:
	.string	"type "
	.section	.text.unlikely._ZN2v88internal6torque12_GLOBAL__N_113ExpectSubtypeEPKNS1_4TypeES5_.part.0,"ax",@progbits
.LCOLDB31:
	.section	.text._ZN2v88internal6torque12_GLOBAL__N_113ExpectSubtypeEPKNS1_4TypeES5_.part.0,"ax",@progbits
.LHOTB31:
	.p2align 4
	.type	_ZN2v88internal6torque12_GLOBAL__N_113ExpectSubtypeEPKNS1_4TypeES5_.part.0, @function
_ZN2v88internal6torque12_GLOBAL__N_113ExpectSubtypeEPKNS1_4TypeES5_.part.0:
.LFB12290:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA12290
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rdx
	movq	%rsi, %r8
	leaq	.LC29(%rip), %rcx
	leaq	.LC30(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-96(%rbp), %r12
	movq	%r12, %rdi
	subq	$80, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
.LEHB82:
	call	_ZN2v88internal6torqueL7MessageIJRA22_KcRKNS1_4TypeERA13_S3_S8_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
.LEHE82:
	movq	%r12, %rdi
.LEHB83:
	call	_ZNK2v88internal6torque14MessageBuilder5ThrowEv@PLT
.LEHE83:
.L841:
	endbr64
	movq	%rax, %r13
	jmp	.L839
	.section	.gcc_except_table._ZN2v88internal6torque12_GLOBAL__N_113ExpectSubtypeEPKNS1_4TypeES5_.part.0,"a",@progbits
.LLSDA12290:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE12290-.LLSDACSB12290
.LLSDACSB12290:
	.uleb128 .LEHB82-.LFB12290
	.uleb128 .LEHE82-.LEHB82
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB83-.LFB12290
	.uleb128 .LEHE83-.LEHB83
	.uleb128 .L841-.LFB12290
	.uleb128 0
.LLSDACSE12290:
	.section	.text._ZN2v88internal6torque12_GLOBAL__N_113ExpectSubtypeEPKNS1_4TypeES5_.part.0
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque12_GLOBAL__N_113ExpectSubtypeEPKNS1_4TypeES5_.part.0
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC12290
	.type	_ZN2v88internal6torque12_GLOBAL__N_113ExpectSubtypeEPKNS1_4TypeES5_.part.0.cold, @function
_ZN2v88internal6torque12_GLOBAL__N_113ExpectSubtypeEPKNS1_4TypeES5_.part.0.cold:
.LFSB12290:
.L839:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movq	%r12, %rdi
	call	_ZN2v88internal6torque14MessageBuilderD1Ev
	movq	%r13, %rdi
.LEHB84:
	call	_Unwind_Resume@PLT
.LEHE84:
	.cfi_endproc
.LFE12290:
	.section	.gcc_except_table._ZN2v88internal6torque12_GLOBAL__N_113ExpectSubtypeEPKNS1_4TypeES5_.part.0
.LLSDAC12290:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC12290-.LLSDACSBC12290
.LLSDACSBC12290:
	.uleb128 .LEHB84-.LCOLDB31
	.uleb128 .LEHE84-.LEHB84
	.uleb128 0
	.uleb128 0
.LLSDACSEC12290:
	.section	.text.unlikely._ZN2v88internal6torque12_GLOBAL__N_113ExpectSubtypeEPKNS1_4TypeES5_.part.0
	.section	.text._ZN2v88internal6torque12_GLOBAL__N_113ExpectSubtypeEPKNS1_4TypeES5_.part.0
	.size	_ZN2v88internal6torque12_GLOBAL__N_113ExpectSubtypeEPKNS1_4TypeES5_.part.0, .-_ZN2v88internal6torque12_GLOBAL__N_113ExpectSubtypeEPKNS1_4TypeES5_.part.0
	.section	.text.unlikely._ZN2v88internal6torque12_GLOBAL__N_113ExpectSubtypeEPKNS1_4TypeES5_.part.0
	.size	_ZN2v88internal6torque12_GLOBAL__N_113ExpectSubtypeEPKNS1_4TypeES5_.part.0.cold, .-_ZN2v88internal6torque12_GLOBAL__N_113ExpectSubtypeEPKNS1_4TypeES5_.part.0.cold
.LCOLDE31:
	.section	.text._ZN2v88internal6torque12_GLOBAL__N_113ExpectSubtypeEPKNS1_4TypeES5_.part.0
.LHOTE31:
	.section	.text._ZNK2v88internal6torque15PokeInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal6torque15PokeInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.type	_ZNK2v88internal6torque15PokeInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE, @function
_ZNK2v88internal6torque15PokeInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE:
.LFB6882:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	8(%rsi), %rcx
	movq	(%rsi), %rax
	movq	%rcx, %rdx
	subq	%rax, %rdx
	sarq	$3, %rdx
	je	.L849
	cmpb	$0, 40(%rdi)
	movq	-8(%rcx), %r12
	movq	%rdi, %r13
	movq	%rsi, %rbx
	jne	.L850
.L845:
	movq	32(%r13), %rsi
	cmpq	%rdx, %rsi
	jnb	.L851
	movq	%r12, (%rax,%rsi,8)
	subq	$8, 8(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L850:
	.cfi_restore_state
	movq	48(%rdi), %r14
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	*16(%rax)
	testb	%al, %al
	je	.L852
	movq	(%rbx), %rax
	movq	8(%rbx), %rdx
	movq	48(%r13), %r12
	subq	%rax, %rdx
	sarq	$3, %rdx
	jmp	.L845
.L849:
	orq	$-1, %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L851:
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L852:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6torque12_GLOBAL__N_113ExpectSubtypeEPKNS1_4TypeES5_.part.0
	.cfi_endproc
.LFE6882:
	.size	_ZNK2v88internal6torque15PokeInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE, .-_ZNK2v88internal6torque15PokeInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.section	.rodata._ZN2v88internal6torque12_GLOBAL__N_110ExpectTypeEPKNS1_4TypeES5_.part.0.str1.1,"aMS",@progbits,1
.LC32:
	.string	" but found "
.LC33:
	.string	"expected type "
	.section	.text.unlikely._ZN2v88internal6torque12_GLOBAL__N_110ExpectTypeEPKNS1_4TypeES5_.part.0,"ax",@progbits
.LCOLDB34:
	.section	.text._ZN2v88internal6torque12_GLOBAL__N_110ExpectTypeEPKNS1_4TypeES5_.part.0,"ax",@progbits
.LHOTB34:
	.p2align 4
	.type	_ZN2v88internal6torque12_GLOBAL__N_110ExpectTypeEPKNS1_4TypeES5_.part.0, @function
_ZN2v88internal6torque12_GLOBAL__N_110ExpectTypeEPKNS1_4TypeES5_.part.0:
.LFB12292:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA12292
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rdx
	movq	%rsi, %r8
	leaq	.LC32(%rip), %rcx
	leaq	.LC33(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-96(%rbp), %r12
	movq	%r12, %rdi
	subq	$80, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
.LEHB85:
	call	_ZN2v88internal6torqueL7MessageIJRA22_KcRKNS1_4TypeERA13_S3_S8_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
.LEHE85:
	movq	%r12, %rdi
.LEHB86:
	call	_ZNK2v88internal6torque14MessageBuilder5ThrowEv@PLT
.LEHE86:
.L856:
	endbr64
	movq	%rax, %r13
	jmp	.L854
	.section	.gcc_except_table._ZN2v88internal6torque12_GLOBAL__N_110ExpectTypeEPKNS1_4TypeES5_.part.0,"a",@progbits
.LLSDA12292:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE12292-.LLSDACSB12292
.LLSDACSB12292:
	.uleb128 .LEHB85-.LFB12292
	.uleb128 .LEHE85-.LEHB85
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB86-.LFB12292
	.uleb128 .LEHE86-.LEHB86
	.uleb128 .L856-.LFB12292
	.uleb128 0
.LLSDACSE12292:
	.section	.text._ZN2v88internal6torque12_GLOBAL__N_110ExpectTypeEPKNS1_4TypeES5_.part.0
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque12_GLOBAL__N_110ExpectTypeEPKNS1_4TypeES5_.part.0
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC12292
	.type	_ZN2v88internal6torque12_GLOBAL__N_110ExpectTypeEPKNS1_4TypeES5_.part.0.cold, @function
_ZN2v88internal6torque12_GLOBAL__N_110ExpectTypeEPKNS1_4TypeES5_.part.0.cold:
.LFSB12292:
.L854:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movq	%r12, %rdi
	call	_ZN2v88internal6torque14MessageBuilderD1Ev
	movq	%r13, %rdi
.LEHB87:
	call	_Unwind_Resume@PLT
.LEHE87:
	.cfi_endproc
.LFE12292:
	.section	.gcc_except_table._ZN2v88internal6torque12_GLOBAL__N_110ExpectTypeEPKNS1_4TypeES5_.part.0
.LLSDAC12292:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC12292-.LLSDACSBC12292
.LLSDACSBC12292:
	.uleb128 .LEHB87-.LCOLDB34
	.uleb128 .LEHE87-.LEHB87
	.uleb128 0
	.uleb128 0
.LLSDACSEC12292:
	.section	.text.unlikely._ZN2v88internal6torque12_GLOBAL__N_110ExpectTypeEPKNS1_4TypeES5_.part.0
	.section	.text._ZN2v88internal6torque12_GLOBAL__N_110ExpectTypeEPKNS1_4TypeES5_.part.0
	.size	_ZN2v88internal6torque12_GLOBAL__N_110ExpectTypeEPKNS1_4TypeES5_.part.0, .-_ZN2v88internal6torque12_GLOBAL__N_110ExpectTypeEPKNS1_4TypeES5_.part.0
	.section	.text.unlikely._ZN2v88internal6torque12_GLOBAL__N_110ExpectTypeEPKNS1_4TypeES5_.part.0
	.size	_ZN2v88internal6torque12_GLOBAL__N_110ExpectTypeEPKNS1_4TypeES5_.part.0.cold, .-_ZN2v88internal6torque12_GLOBAL__N_110ExpectTypeEPKNS1_4TypeES5_.part.0.cold
.LCOLDE34:
	.section	.text._ZN2v88internal6torque12_GLOBAL__N_110ExpectTypeEPKNS1_4TypeES5_.part.0
.LHOTE34:
	.section	.text.unlikely._ZNK2v88internal6torque25StoreReferenceInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE,"ax",@progbits
	.align 2
.LCOLDB35:
	.section	.text._ZNK2v88internal6torque25StoreReferenceInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE,"ax",@progbits
.LHOTB35:
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal6torque25StoreReferenceInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.type	_ZNK2v88internal6torque25StoreReferenceInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE, @function
_ZNK2v88internal6torque25StoreReferenceInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE:
.LFB6904:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6904
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$56, %rsp
	movq	32(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8(%rsi), %rax
	movq	-8(%rax), %r12
	subq	$8, %rax
	movq	%rax, 8(%rsi)
	movq	%r13, %rsi
	movq	(%r12), %rax
	movq	%r12, %rdi
.LEHB88:
	call	*16(%rax)
	testb	%al, %al
	je	.L874
	movq	8(%rbx), %rax
	leaq	-96(%rbp), %r15
	leaq	-80(%rbp), %r12
	movq	-8(%rax), %r13
	subq	$8, %rax
	movq	%rax, 8(%rbx)
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_10TypeOracleEEERPNT_5ScopeEv@PLT
.LEHE88:
	movl	$29300, %edx
	movq	%r15, %rdi
	movq	%r12, -96(%rbp)
	movl	$1886678633, -80(%rbp)
	movw	%dx, -76(%rbp)
	movq	$6, -88(%rbp)
	movb	$0, -74(%rbp)
.LEHB89:
	call	_ZN2v88internal6torque12Declarations16LookupGlobalTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
.LEHE89:
	movq	-96(%rbp), %rdi
	movq	%rax, %r14
	cmpq	%r12, %rdi
	je	.L860
	call	_ZdlPv@PLT
.L860:
	cmpq	%r14, %r13
	jne	.L878
.LEHB90:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_10TypeOracleEEERPNT_5ScopeEv@PLT
.LEHE90:
	movq	%r15, %rdi
	movq	%r12, -96(%rbp)
	movabsq	$7307761438689420616, %rax
	movq	%rax, -80(%rbp)
	movl	$29795, %eax
	movw	%ax, -72(%rbp)
	movq	$10, -88(%rbp)
	movb	$0, -70(%rbp)
.LEHB91:
	call	_ZN2v88internal6torque12Declarations16LookupGlobalTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
.LEHE91:
	movq	-96(%rbp), %rdi
	movq	%rax, %r13
	cmpq	%r12, %rdi
	je	.L865
	call	_ZdlPv@PLT
.L865:
	movq	8(%rbx), %rax
	movq	%r13, %rsi
	movq	-8(%rax), %r12
	subq	$8, %rax
	movq	%rax, 8(%rbx)
	movq	(%r12), %rax
	movq	%r12, %rdi
.LEHB92:
	call	*16(%rax)
	testb	%al, %al
	je	.L874
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L879
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L874:
	.cfi_restore_state
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6torque12_GLOBAL__N_113ExpectSubtypeEPKNS1_4TypeES5_.part.0
.L879:
	call	__stack_chk_fail@PLT
.L878:
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal6torque12_GLOBAL__N_110ExpectTypeEPKNS1_4TypeES5_.part.0
.LEHE92:
.L872:
	endbr64
	movq	%rax, %r13
	jmp	.L868
.L871:
	endbr64
	movq	%rax, %r13
	jmp	.L868
	.section	.gcc_except_table._ZNK2v88internal6torque25StoreReferenceInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE,"a",@progbits
.LLSDA6904:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6904-.LLSDACSB6904
.LLSDACSB6904:
	.uleb128 .LEHB88-.LFB6904
	.uleb128 .LEHE88-.LEHB88
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB89-.LFB6904
	.uleb128 .LEHE89-.LEHB89
	.uleb128 .L871-.LFB6904
	.uleb128 0
	.uleb128 .LEHB90-.LFB6904
	.uleb128 .LEHE90-.LEHB90
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB91-.LFB6904
	.uleb128 .LEHE91-.LEHB91
	.uleb128 .L872-.LFB6904
	.uleb128 0
	.uleb128 .LEHB92-.LFB6904
	.uleb128 .LEHE92-.LEHB92
	.uleb128 0
	.uleb128 0
.LLSDACSE6904:
	.section	.text._ZNK2v88internal6torque25StoreReferenceInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.cfi_endproc
	.section	.text.unlikely._ZNK2v88internal6torque25StoreReferenceInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6904
	.type	_ZNK2v88internal6torque25StoreReferenceInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE.cold, @function
_ZNK2v88internal6torque25StoreReferenceInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE.cold:
.LFSB6904:
.L868:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-96(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L869
	call	_ZdlPv@PLT
.L869:
	movq	%r13, %rdi
.LEHB93:
	call	_Unwind_Resume@PLT
.LEHE93:
	.cfi_endproc
.LFE6904:
	.section	.gcc_except_table._ZNK2v88internal6torque25StoreReferenceInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
.LLSDAC6904:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6904-.LLSDACSBC6904
.LLSDACSBC6904:
	.uleb128 .LEHB93-.LCOLDB35
	.uleb128 .LEHE93-.LEHB93
	.uleb128 0
	.uleb128 0
.LLSDACSEC6904:
	.section	.text.unlikely._ZNK2v88internal6torque25StoreReferenceInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.section	.text._ZNK2v88internal6torque25StoreReferenceInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.size	_ZNK2v88internal6torque25StoreReferenceInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE, .-_ZNK2v88internal6torque25StoreReferenceInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.section	.text.unlikely._ZNK2v88internal6torque25StoreReferenceInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.size	_ZNK2v88internal6torque25StoreReferenceInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE.cold, .-_ZNK2v88internal6torque25StoreReferenceInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE.cold
.LCOLDE35:
	.section	.text._ZNK2v88internal6torque25StoreReferenceInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
.LHOTE35:
	.section	.text._ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EEaSERKS7_,"axG",@progbits,_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EEaSERKS7_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EEaSERKS7_
	.type	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EEaSERKS7_, @function
_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EEaSERKS7_:
.LFB8185:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA8185
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	%rdi, %rsi
	je	.L881
	movq	8(%rsi), %rax
	movq	(%rsi), %rbx
	movq	%rsi, %r12
	movq	(%rdi), %r15
	movq	16(%rdi), %rdx
	movq	%rax, -72(%rbp)
	subq	%rbx, %rax
	movq	%rax, %rcx
	subq	%r15, %rdx
	movq	%rax, -88(%rbp)
	sarq	$5, %rcx
	sarq	$5, %rdx
	movq	%rcx, %rax
	cmpq	%rcx, %rdx
	jb	.L974
	movq	8(%rdi), %rcx
	movq	%rcx, %rdx
	movq	%rcx, -80(%rbp)
	subq	%r15, %rdx
	movq	%rdx, %r14
	sarq	$5, %r14
	cmpq	%r14, %rax
	ja	.L907
	cmpq	$0, -88(%rbp)
	movq	%r15, %r12
	jle	.L973
	.p2align 4,,10
	.p2align 3
.L908:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, -72(%rbp)
	addq	$32, %rbx
.LEHB94:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	movq	-72(%rbp), %rax
	addq	$32, %r12
	subq	$1, %rax
	jne	.L908
	movq	-88(%rbp), %rcx
	movl	$32, %eax
	testq	%rcx, %rcx
	cmovg	%rcx, %rax
	addq	%rax, %r15
	.p2align 4,,10
	.p2align 3
.L973:
	cmpq	%r15, -80(%rbp)
	je	.L970
.L915:
	movq	(%r15), %rdi
	leaq	16(%r15), %rax
	cmpq	%rax, %rdi
	je	.L912
	call	_ZdlPv@PLT
	addq	$32, %r15
	cmpq	%r15, -80(%rbp)
	jne	.L915
.L970:
	movq	-88(%rbp), %rax
	addq	0(%r13), %rax
.L906:
	movq	%rax, 8(%r13)
.L881:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L975
	addq	$56, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L974:
	.cfi_restore_state
	testq	%rcx, %rcx
	je	.L930
	movabsq	$288230376151711743, %rax
	cmpq	%rax, %rcx
	ja	.L976
	movq	-88(%rbp), %rdi
	call	_Znwm@PLT
.LEHE94:
	movq	%rax, -96(%rbp)
.L883:
	leaq	-64(%rbp), %rax
	movq	-96(%rbp), %r14
	movq	%rax, -80(%rbp)
	cmpq	-72(%rbp), %rbx
	jne	.L893
	jmp	.L894
	.p2align 4,,10
	.p2align 3
.L889:
	cmpq	$1, %r12
	jne	.L891
	movzbl	(%r15), %eax
	movb	%al, 16(%r14)
.L892:
	movq	%r12, 8(%r14)
	addq	$32, %rbx
	addq	$32, %r14
	movb	$0, (%rdi,%r12)
	cmpq	%rbx, -72(%rbp)
	je	.L894
.L893:
	leaq	16(%r14), %rdi
	movq	%rdi, (%r14)
	movq	(%rbx), %r15
	movq	8(%rbx), %r12
	movq	%r15, %rax
	addq	%r12, %rax
	je	.L888
	testq	%r15, %r15
	je	.L977
.L888:
	movq	%r12, -64(%rbp)
	cmpq	$15, %r12
	jbe	.L889
	movq	-80(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r14, %rdi
.LEHB95:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE95:
	movq	%rax, (%r14)
	movq	%rax, %rdi
	movq	-64(%rbp), %rax
	movq	%rax, 16(%r14)
.L890:
	movq	%r12, %rdx
	movq	%r15, %rsi
	call	memcpy@PLT
	movq	-64(%rbp), %r12
	movq	(%r14), %rdi
	jmp	.L892
	.p2align 4,,10
	.p2align 3
.L912:
	addq	$32, %r15
	jmp	.L973
	.p2align 4,,10
	.p2align 3
.L907:
	testq	%rdx, %rdx
	jle	.L916
	.p2align 4,,10
	.p2align 3
.L917:
	movq	%rbx, %rsi
	movq	%r15, %rdi
	addq	$32, %rbx
	addq	$32, %r15
.LEHB96:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
.LEHE96:
	subq	$1, %r14
	jne	.L917
	movq	8(%r12), %rsi
	movq	8(%r13), %rax
	movq	0(%r13), %r15
	movq	(%r12), %rbx
	movq	%rsi, -72(%rbp)
	movq	%rax, -80(%rbp)
	subq	%r15, %rax
	movq	%rax, %rdx
.L916:
	leaq	-64(%rbp), %rax
	movq	-80(%rbp), %r14
	addq	%rdx, %rbx
	movq	%rax, -96(%rbp)
	movq	-88(%rbp), %rax
	addq	%r15, %rax
	cmpq	-72(%rbp), %rbx
	jne	.L918
	jmp	.L906
	.p2align 4,,10
	.p2align 3
.L980:
	movzbl	(%r15), %eax
	movb	%al, (%rdi)
	movq	-64(%rbp), %r12
	movq	(%r14), %rdi
.L923:
	movq	%r12, 8(%r14)
	addq	$32, %rbx
	addq	$32, %r14
	movb	$0, (%rdi,%r12)
	cmpq	-72(%rbp), %rbx
	je	.L970
.L918:
	leaq	16(%r14), %rax
	movq	%rax, (%r14)
	movq	(%rbx), %r15
	movq	8(%rbx), %r12
	movq	%r15, %rax
	addq	%r12, %rax
	je	.L919
	testq	%r15, %r15
	je	.L978
.L919:
	movq	%r12, -64(%rbp)
	cmpq	$15, %r12
	ja	.L979
	movq	(%r14), %rdi
	cmpq	$1, %r12
	je	.L980
	testq	%r12, %r12
	je	.L923
	jmp	.L921
	.p2align 4,,10
	.p2align 3
.L979:
	movq	-96(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r14, %rdi
.LEHB97:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE97:
	movq	%rax, (%r14)
	movq	%rax, %rdi
	movq	-64(%rbp), %rax
	movq	%rax, 16(%r14)
.L921:
	movq	%r12, %rdx
	movq	%r15, %rsi
	call	memcpy@PLT
	movq	-64(%rbp), %r12
	movq	(%r14), %rdi
	jmp	.L923
	.p2align 4,,10
	.p2align 3
.L930:
	movq	$0, -96(%rbp)
	jmp	.L883
	.p2align 4,,10
	.p2align 3
.L891:
	testq	%r12, %r12
	je	.L892
	jmp	.L890
	.p2align 4,,10
	.p2align 3
.L894:
	movq	8(%r13), %rbx
	movq	0(%r13), %r12
	cmpq	%r12, %rbx
	je	.L886
	.p2align 4,,10
	.p2align 3
.L887:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L902
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L887
.L903:
	movq	0(%r13), %r12
.L886:
	testq	%r12, %r12
	je	.L905
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L905:
	movq	-96(%rbp), %rax
	movq	%rax, 0(%r13)
	addq	-88(%rbp), %rax
	movq	%rax, 16(%r13)
	jmp	.L906
	.p2align 4,,10
	.p2align 3
.L902:
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L887
	jmp	.L903
.L975:
	call	__stack_chk_fail@PLT
.L977:
	leaq	.LC5(%rip), %rdi
.LEHB98:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE98:
.L978:
	leaq	.LC5(%rip), %rdi
.LEHB99:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE99:
.L976:
.LEHB100:
	call	_ZSt17__throw_bad_allocv@PLT
.LEHE100:
.L937:
	endbr64
	movq	%rax, %rdi
	jmp	.L924
.L935:
	endbr64
	movq	%rax, %rdi
	jmp	.L895
.L924:
	call	__cxa_begin_catch@PLT
.L927:
	cmpq	%r14, -80(%rbp)
	jne	.L981
.LEHB101:
	call	__cxa_rethrow@PLT
.LEHE101:
.L895:
	call	__cxa_begin_catch@PLT
	movq	-96(%rbp), %rbx
.L898:
	cmpq	%rbx, %r14
	jne	.L982
.LEHB102:
	call	__cxa_rethrow@PLT
.LEHE102:
.L981:
	movq	-80(%rbp), %rax
	movq	(%rax), %rdi
	addq	$16, %rax
	cmpq	%rax, %rdi
	je	.L926
	call	_ZdlPv@PLT
.L926:
	addq	$32, -80(%rbp)
	jmp	.L927
.L936:
	endbr64
	movq	%rax, %r12
	jmp	.L928
.L982:
	movq	(%rbx), %rdi
	leaq	16(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L897
	call	_ZdlPv@PLT
.L897:
	addq	$32, %rbx
	jmp	.L898
.L928:
	call	__cxa_end_catch@PLT
	movq	%r12, %rdi
.LEHB103:
	call	_Unwind_Resume@PLT
.LEHE103:
.L934:
	endbr64
	movq	%rax, %r12
.L899:
	call	__cxa_end_catch@PLT
	movq	%r12, %rdi
	call	__cxa_begin_catch@PLT
	cmpq	$0, -96(%rbp)
	je	.L900
	movq	-96(%rbp), %rdi
	call	_ZdlPv@PLT
.L900:
.LEHB104:
	call	__cxa_rethrow@PLT
.LEHE104:
.L933:
	endbr64
	movq	%rax, %r12
.L901:
	call	__cxa_end_catch@PLT
	movq	%r12, %rdi
.LEHB105:
	call	_Unwind_Resume@PLT
.LEHE105:
	.cfi_endproc
.LFE8185:
	.section	.gcc_except_table._ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EEaSERKS7_,"aG",@progbits,_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EEaSERKS7_,comdat
	.align 4
.LLSDA8185:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATT8185-.LLSDATTD8185
.LLSDATTD8185:
	.byte	0x1
	.uleb128 .LLSDACSE8185-.LLSDACSB8185
.LLSDACSB8185:
	.uleb128 .LEHB94-.LFB8185
	.uleb128 .LEHE94-.LEHB94
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB95-.LFB8185
	.uleb128 .LEHE95-.LEHB95
	.uleb128 .L935-.LFB8185
	.uleb128 0x1
	.uleb128 .LEHB96-.LFB8185
	.uleb128 .LEHE96-.LEHB96
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB97-.LFB8185
	.uleb128 .LEHE97-.LEHB97
	.uleb128 .L937-.LFB8185
	.uleb128 0x1
	.uleb128 .LEHB98-.LFB8185
	.uleb128 .LEHE98-.LEHB98
	.uleb128 .L935-.LFB8185
	.uleb128 0x1
	.uleb128 .LEHB99-.LFB8185
	.uleb128 .LEHE99-.LEHB99
	.uleb128 .L937-.LFB8185
	.uleb128 0x1
	.uleb128 .LEHB100-.LFB8185
	.uleb128 .LEHE100-.LEHB100
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB101-.LFB8185
	.uleb128 .LEHE101-.LEHB101
	.uleb128 .L936-.LFB8185
	.uleb128 0
	.uleb128 .LEHB102-.LFB8185
	.uleb128 .LEHE102-.LEHB102
	.uleb128 .L934-.LFB8185
	.uleb128 0x3
	.uleb128 .LEHB103-.LFB8185
	.uleb128 .LEHE103-.LEHB103
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB104-.LFB8185
	.uleb128 .LEHE104-.LEHB104
	.uleb128 .L933-.LFB8185
	.uleb128 0
	.uleb128 .LEHB105-.LFB8185
	.uleb128 .LEHE105-.LEHB105
	.uleb128 0
	.uleb128 0
.LLSDACSE8185:
	.byte	0x1
	.byte	0
	.byte	0
	.byte	0x7d
	.align 4
	.long	0

.LLSDATT8185:
	.section	.text._ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EEaSERKS7_,"axG",@progbits,_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EEaSERKS7_,comdat
	.size	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EEaSERKS7_, .-_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EEaSERKS7_
	.section	.text._ZN2v88internal6torque23CallCsaMacroInstruction6AssignERKNS1_15InstructionBaseE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque23CallCsaMacroInstruction6AssignERKNS1_15InstructionBaseE
	.type	_ZN2v88internal6torque23CallCsaMacroInstruction6AssignERKNS1_15InstructionBaseE, @function
_ZN2v88internal6torque23CallCsaMacroInstruction6AssignERKNS1_15InstructionBaseE:
.LFB6792:
	.cfi_startproc
	endbr64
	movdqu	8(%rsi), %xmm0
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movups	%xmm0, 8(%rdi)
	movl	24(%rsi), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	addq	$40, %rsi
	movl	%eax, 24(%rdi)
	movq	-8(%rsi), %rax
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	addq	$40, %rdi
	movq	%rax, -8(%rdi)
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EEaSERKS7_
	cmpb	$0, 64(%r12)
	je	.L984
	movq	72(%r12), %rax
	cmpb	$0, 64(%rbx)
	movq	%rax, 72(%rbx)
	je	.L985
.L983:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L984:
	.cfi_restore_state
	cmpb	$0, 64(%rbx)
	je	.L983
	movb	$0, 64(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L985:
	.cfi_restore_state
	movb	$1, 64(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE6792:
	.size	_ZN2v88internal6torque23CallCsaMacroInstruction6AssignERKNS1_15InstructionBaseE, .-_ZN2v88internal6torque23CallCsaMacroInstruction6AssignERKNS1_15InstructionBaseE
	.section	.text._ZN2v88internal6torque23GotoExternalInstruction6AssignERKNS1_15InstructionBaseE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque23GotoExternalInstruction6AssignERKNS1_15InstructionBaseE
	.type	_ZN2v88internal6torque23GotoExternalInstruction6AssignERKNS1_15InstructionBaseE, @function
_ZN2v88internal6torque23GotoExternalInstruction6AssignERKNS1_15InstructionBaseE:
.LFB6853:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	leaq	32(%rsi), %rsi
	pushq	%rbx
	.cfi_offset 3, -32
	movdqu	-24(%rsi), %xmm0
	movq	%rdi, %rbx
	leaq	32(%rdi), %rdi
	movups	%xmm0, -24(%rdi)
	movl	-8(%rsi), %eax
	movl	%eax, -8(%rdi)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	leaq	64(%r12), %rsi
	leaq	64(%rbx), %rdi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EEaSERKS7_
	.cfi_endproc
.LFE6853:
	.size	_ZN2v88internal6torque23GotoExternalInstruction6AssignERKNS1_15InstructionBaseE, .-_ZN2v88internal6torque23GotoExternalInstruction6AssignERKNS1_15InstructionBaseE
	.section	.text._ZN2v88internal6torque24CallIntrinsicInstruction6AssignERKNS1_15InstructionBaseE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque24CallIntrinsicInstruction6AssignERKNS1_15InstructionBaseE
	.type	_ZN2v88internal6torque24CallIntrinsicInstruction6AssignERKNS1_15InstructionBaseE, @function
_ZN2v88internal6torque24CallIntrinsicInstruction6AssignERKNS1_15InstructionBaseE:
.LFB6799:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movdqu	8(%rsi), %xmm0
	movups	%xmm0, 8(%rdi)
	movl	24(%rsi), %eax
	movl	%eax, 24(%rdi)
	movq	32(%rsi), %rax
	movq	%rax, 32(%rdi)
	cmpq	%rsi, %rdi
	je	.L991
	movq	48(%rsi), %rdx
	movq	40(%rsi), %r14
	movq	40(%rdi), %r15
	movq	56(%rdi), %rax
	movq	%rdx, %r13
	subq	%r14, %r13
	subq	%r15, %rax
	movq	%r13, %rcx
	sarq	$3, %rax
	sarq	$3, %rcx
	cmpq	%rax, %rcx
	ja	.L1011
	movq	48(%rdi), %rdi
	movq	%rdi, %rsi
	subq	%r15, %rsi
	movq	%rsi, %rax
	sarq	$3, %rax
	cmpq	%rax, %rcx
	ja	.L999
	cmpq	%rdx, %r14
	je	.L1010
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	memmove@PLT
	addq	40(%rbx), %r13
	movq	%r13, 48(%rbx)
.L991:
	addq	$24, %rsp
	leaq	64(%r12), %rsi
	leaq	64(%rbx), %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EEaSERKS7_
	.p2align 4,,10
	.p2align 3
.L1011:
	.cfi_restore_state
	testq	%rcx, %rcx
	je	.L994
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rcx
	ja	.L1012
	movq	%r13, %rdi
	movq	%rdx, -56(%rbp)
	call	_Znwm@PLT
	movq	40(%rbx), %r15
	movq	-56(%rbp), %rdx
	movq	%rax, %rcx
.L994:
	cmpq	%rdx, %r14
	je	.L996
	movq	%rcx, %rdi
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	%rax, %rcx
.L996:
	testq	%r15, %r15
	je	.L997
	movq	%r15, %rdi
	movq	%rcx, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rcx
.L997:
	addq	%rcx, %r13
	movq	%rcx, 40(%rbx)
	movq	%r13, 56(%rbx)
	jmp	.L998
	.p2align 4,,10
	.p2align 3
.L999:
	testq	%rsi, %rsi
	je	.L1001
	movq	%rsi, %rdx
	movq	%r15, %rdi
	movq	%r14, %rsi
	call	memmove@PLT
	movq	48(%rbx), %rdi
	movq	40(%rbx), %r15
	movq	48(%r12), %rdx
	movq	40(%r12), %r14
	movq	%rdi, %rsi
	subq	%r15, %rsi
.L1001:
	addq	%r14, %rsi
	cmpq	%rdx, %rsi
	jne	.L1002
.L1010:
	addq	%r15, %r13
.L998:
	movq	%r13, 48(%rbx)
	jmp	.L991
	.p2align 4,,10
	.p2align 3
.L1002:
	subq	%rsi, %rdx
	call	memmove@PLT
	addq	40(%rbx), %r13
	jmp	.L998
.L1012:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE6799:
	.size	_ZN2v88internal6torque24CallIntrinsicInstruction6AssignERKNS1_15InstructionBaseE, .-_ZN2v88internal6torque24CallIntrinsicInstruction6AssignERKNS1_15InstructionBaseE
	.section	.text._ZN2v88internal6torque32CallCsaMacroAndBranchInstruction6AssignERKNS1_15InstructionBaseE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque32CallCsaMacroAndBranchInstruction6AssignERKNS1_15InstructionBaseE
	.type	_ZN2v88internal6torque32CallCsaMacroAndBranchInstruction6AssignERKNS1_15InstructionBaseE, @function
_ZN2v88internal6torque32CallCsaMacroAndBranchInstruction6AssignERKNS1_15InstructionBaseE:
.LFB6811:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	addq	$40, %rsi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$40, %rdi
	subq	$24, %rsp
	movdqu	-32(%rsi), %xmm0
	movups	%xmm0, -32(%rdi)
	movl	-16(%rsi), %eax
	movl	%eax, -16(%rdi)
	movq	-8(%rsi), %rax
	movq	%rax, -8(%rdi)
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EEaSERKS7_
	cmpb	$0, 64(%r12)
	je	.L1014
	movq	72(%r12), %rax
	cmpb	$0, 64(%rbx)
	movq	%rax, 72(%rbx)
	je	.L1015
.L1016:
	cmpq	%r12, %rbx
	je	.L1017
	movq	88(%r12), %rdx
	movq	80(%r12), %r14
	movq	80(%rbx), %r15
	movq	96(%rbx), %rax
	movq	%rdx, %r13
	subq	%r14, %r13
	subq	%r15, %rax
	movq	%r13, %rcx
	sarq	$3, %rax
	sarq	$3, %rcx
	cmpq	%rax, %rcx
	ja	.L1043
	movq	88(%rbx), %rdi
	movq	%rdi, %rsi
	subq	%r15, %rsi
	movq	%rsi, %rax
	sarq	$3, %rax
	cmpq	%rax, %rcx
	ja	.L1025
	cmpq	%rdx, %r14
	je	.L1042
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	memmove@PLT
	addq	80(%rbx), %r13
	movq	%r13, 88(%rbx)
.L1017:
	cmpb	$0, 104(%r12)
	movzbl	104(%rbx), %eax
	jne	.L1044
.L1029:
	testb	%al, %al
	je	.L1013
	movb	$0, 104(%rbx)
.L1013:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1014:
	.cfi_restore_state
	cmpb	$0, 64(%rbx)
	je	.L1016
	movb	$0, 64(%rbx)
	jmp	.L1016
	.p2align 4,,10
	.p2align 3
.L1044:
	movq	112(%r12), %rdx
	movq	%rdx, 112(%rbx)
	testb	%al, %al
	jne	.L1013
	movb	$1, 104(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1015:
	.cfi_restore_state
	movb	$1, 64(%rbx)
	jmp	.L1016
	.p2align 4,,10
	.p2align 3
.L1043:
	testq	%rcx, %rcx
	je	.L1020
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rcx
	ja	.L1045
	movq	%r13, %rdi
	movq	%rdx, -56(%rbp)
	call	_Znwm@PLT
	movq	80(%rbx), %r15
	movq	-56(%rbp), %rdx
	movq	%rax, %rcx
.L1020:
	cmpq	%rdx, %r14
	je	.L1022
	movq	%rcx, %rdi
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	%rax, %rcx
.L1022:
	testq	%r15, %r15
	je	.L1023
	movq	%r15, %rdi
	movq	%rcx, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rcx
.L1023:
	addq	%rcx, %r13
	movq	%rcx, 80(%rbx)
	movq	%r13, 96(%rbx)
	jmp	.L1024
	.p2align 4,,10
	.p2align 3
.L1025:
	testq	%rsi, %rsi
	je	.L1027
	movq	%rsi, %rdx
	movq	%r15, %rdi
	movq	%r14, %rsi
	call	memmove@PLT
	movq	88(%rbx), %rdi
	movq	80(%rbx), %r15
	movq	88(%r12), %rdx
	movq	80(%r12), %r14
	movq	%rdi, %rsi
	subq	%r15, %rsi
.L1027:
	addq	%r14, %rsi
	cmpq	%rdx, %rsi
	jne	.L1028
.L1042:
	addq	%r15, %r13
.L1024:
	movq	%r13, 88(%rbx)
	cmpb	$0, 104(%r12)
	movzbl	104(%rbx), %eax
	je	.L1029
	jmp	.L1044
	.p2align 4,,10
	.p2align 3
.L1028:
	subq	%rsi, %rdx
	call	memmove@PLT
	addq	80(%rbx), %r13
	jmp	.L1024
.L1045:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE6811:
	.size	_ZN2v88internal6torque32CallCsaMacroAndBranchInstruction6AssignERKNS1_15InstructionBaseE, .-_ZN2v88internal6torque32CallCsaMacroAndBranchInstruction6AssignERKNS1_15InstructionBaseE
	.section	.text._ZN2v88internal6torque11ReportErrorIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_,"axG",@progbits,_ZN2v88internal6torque11ReportErrorIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_,comdat
	.p2align 4
	.weak	_ZN2v88internal6torque11ReportErrorIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_
	.type	_ZN2v88internal6torque11ReportErrorIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_, @function
_ZN2v88internal6torque11ReportErrorIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_:
.LFB8198:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA8198
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-432(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%r13, %rdi
	subq	$496, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
.LEHB106:
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEC1Ev@PLT
.LEHE106:
	movq	8(%rbx), %rdx
	movq	(%rbx), %rsi
	leaq	-416(%rbp), %rdi
.LEHB107:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	-528(%rbp), %r14
	leaq	-408(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZNKSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEE3strEv@PLT
.LEHE107:
	leaq	-496(%rbp), %r12
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
.LEHB108:
	call	_ZN2v88internal6torque14MessageBuilderC1ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_13TorqueMessage4KindE@PLT
.LEHE108:
	movq	-528(%rbp), %rdi
	leaq	-512(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1047
	call	_ZdlPv@PLT
.L1047:
	movq	%r13, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r12, %rdi
.LEHB109:
	call	_ZNK2v88internal6torque14MessageBuilder5ThrowEv@PLT
.LEHE109:
.L1054:
	endbr64
	movq	%rax, %r12
	jmp	.L1050
.L1053:
	endbr64
	movq	%rax, %r13
	jmp	.L1051
.L1055:
	endbr64
	movq	%rax, %r12
.L1048:
	movq	-528(%rbp), %rdi
	leaq	-512(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1050
	call	_ZdlPv@PLT
.L1050:
	movq	%r13, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r12, %rdi
.LEHB110:
	call	_Unwind_Resume@PLT
.L1051:
	movq	%r12, %rdi
	call	_ZN2v88internal6torque14MessageBuilderD1Ev
	movq	%r13, %rdi
	call	_Unwind_Resume@PLT
.LEHE110:
	.cfi_endproc
.LFE8198:
	.section	.gcc_except_table._ZN2v88internal6torque11ReportErrorIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_,"aG",@progbits,_ZN2v88internal6torque11ReportErrorIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_,comdat
.LLSDA8198:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE8198-.LLSDACSB8198
.LLSDACSB8198:
	.uleb128 .LEHB106-.LFB8198
	.uleb128 .LEHE106-.LEHB106
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB107-.LFB8198
	.uleb128 .LEHE107-.LEHB107
	.uleb128 .L1054-.LFB8198
	.uleb128 0
	.uleb128 .LEHB108-.LFB8198
	.uleb128 .LEHE108-.LEHB108
	.uleb128 .L1055-.LFB8198
	.uleb128 0
	.uleb128 .LEHB109-.LFB8198
	.uleb128 .LEHE109-.LEHB109
	.uleb128 .L1053-.LFB8198
	.uleb128 0
	.uleb128 .LEHB110-.LFB8198
	.uleb128 .LEHE110-.LEHB110
	.uleb128 0
	.uleb128 0
.LLSDACSE8198:
	.section	.text._ZN2v88internal6torque11ReportErrorIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_,"axG",@progbits,_ZN2v88internal6torque11ReportErrorIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_,comdat
	.size	_ZN2v88internal6torque11ReportErrorIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_, .-_ZN2v88internal6torque11ReportErrorIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_
	.section	.rodata._ZNK2v88internal6torque15PeekInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE.str1.1,"aMS",@progbits,1
.LC36:
	.string	"basic_string::append"
.LC37:
	.string	"use of "
	.section	.text.unlikely._ZNK2v88internal6torque15PeekInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE,"ax",@progbits
	.align 2
.LCOLDB38:
	.section	.text._ZNK2v88internal6torque15PeekInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE,"ax",@progbits
.LHOTB38:
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal6torque15PeekInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.type	_ZNK2v88internal6torque15PeekInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE, @function
_ZNK2v88internal6torque15PeekInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE:
.LFB6881:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6881
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$120, %rsp
	movq	(%rbx), %rcx
	movq	32(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8(%rbx), %rax
	movq	%rax, %rdx
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rsi, %rdx
	jbe	.L1105
	cmpb	$0, 40(%rdi)
	movq	(%rcx,%rsi,8), %r13
	movq	%rdi, %r12
	je	.L1059
	movl	8(%r13), %eax
	testl	%eax, %eax
	je	.L1106
	movq	48(%rdi), %r14
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	%r14, %rsi
.LEHB111:
	call	*16(%rax)
	testb	%al, %al
	je	.L1107
	movq	48(%r12), %r13
	movq	8(%rbx), %rax
.L1059:
	cmpq	%rax, 16(%rbx)
	je	.L1072
	movq	%r13, (%rax)
	addq	$8, 8(%rbx)
.L1057:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1108
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1072:
	.cfi_restore_state
	movabsq	$1152921504606846975, %rdx
	movq	(%rbx), %r8
	subq	%r8, %rax
	movq	%rax, %r12
	sarq	$3, %rax
	cmpq	%rdx, %rax
	je	.L1109
	testq	%rax, %rax
	je	.L1084
	movabsq	$9223372036854775800, %r15
	leaq	(%rax,%rax), %rcx
	cmpq	%rcx, %rax
	jbe	.L1110
.L1075:
	movq	%r15, %rdi
	movq	%r8, -152(%rbp)
	call	_Znwm@PLT
	movq	-152(%rbp), %r8
	movq	%rax, %r14
	addq	%rax, %r15
.L1076:
	movq	%r13, (%r14,%r12)
	leaq	8(%r14,%r12), %r13
	testq	%r12, %r12
	jg	.L1111
	testq	%r8, %r8
	jne	.L1078
.L1079:
	movq	%r14, %xmm0
	movq	%r13, %xmm1
	movq	%r15, 16(%rbx)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rbx)
	jmp	.L1057
	.p2align 4,,10
	.p2align 3
.L1111:
	movq	%r8, %rsi
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r8, -152(%rbp)
	call	memmove@PLT
	movq	-152(%rbp), %r8
.L1078:
	movq	%r8, %rdi
	call	_ZdlPv@PLT
	jmp	.L1079
	.p2align 4,,10
	.p2align 3
.L1110:
	testq	%rcx, %rcx
	jne	.L1112
	xorl	%r15d, %r15d
	xorl	%r14d, %r14d
	jmp	.L1076
	.p2align 4,,10
	.p2align 3
.L1084:
	movl	$8, %r15d
	jmp	.L1075
.L1108:
	call	__stack_chk_fail@PLT
.L1105:
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.LEHE111:
.L1106:
	movq	72(%r13), %r14
	movq	80(%r13), %r12
	leaq	-112(%rbp), %rbx
	movq	%rbx, -128(%rbp)
	movq	%r14, %rax
	addq	%r12, %rax
	je	.L1061
	testq	%r14, %r14
	je	.L1113
.L1061:
	movq	%r12, -136(%rbp)
	cmpq	$15, %r12
	ja	.L1114
	cmpq	$1, %r12
	jne	.L1064
	movzbl	(%r14), %eax
	movb	%al, -112(%rbp)
.L1065:
	movq	-136(%rbp), %rax
	movq	-128(%rbp), %rdx
	leaq	-96(%rbp), %r12
	leaq	-80(%rbp), %r13
	movq	%r12, %rdi
	movq	%rax, -120(%rbp)
	movb	$0, (%rdx,%rax)
	movq	-120(%rbp), %rax
	movq	%r13, -96(%rbp)
	leaq	7(%rax), %rsi
	movq	$0, -88(%rbp)
	movb	$0, -80(%rbp)
.LEHB112:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7reserveEm@PLT
	movabsq	$4611686018427387903, %rax
	subq	-88(%rbp), %rax
	cmpq	$6, %rax
	jbe	.L1115
	movl	$7, %edx
	leaq	.LC37(%rip), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-120(%rbp), %rdx
	movq	-128(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.LEHE112:
	movq	%r12, %rdi
.LEHB113:
	call	_ZN2v88internal6torque11ReportErrorIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_
.LEHE113:
.L1107:
	movq	%r14, %rsi
	movq	%r13, %rdi
.LEHB114:
	call	_ZN2v88internal6torque12_GLOBAL__N_113ExpectSubtypeEPKNS1_4TypeES5_.part.0
.L1109:
	leaq	.LC2(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1064:
	testq	%r12, %r12
	je	.L1065
	movq	%rbx, %rdi
.L1063:
	movq	%r12, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	jmp	.L1065
.L1114:
	leaq	-128(%rbp), %rdi
	leaq	-136(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -128(%rbp)
	movq	%rax, %rdi
	movq	-136(%rbp), %rax
	movq	%rax, -112(%rbp)
	jmp	.L1063
.L1112:
	cmpq	%rdx, %rcx
	cmovbe	%rcx, %rdx
	leaq	0(,%rdx,8), %r15
	jmp	.L1075
.L1113:
	leaq	.LC5(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE114:
.L1115:
	leaq	.LC36(%rip), %rdi
.LEHB115:
	call	_ZSt20__throw_length_errorPKc@PLT
.LEHE115:
.L1087:
	endbr64
	movq	%rax, %r12
	jmp	.L1080
.L1088:
	endbr64
	movq	%rax, %r12
	jmp	.L1080
	.section	.gcc_except_table._ZNK2v88internal6torque15PeekInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE,"a",@progbits
.LLSDA6881:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6881-.LLSDACSB6881
.LLSDACSB6881:
	.uleb128 .LEHB111-.LFB6881
	.uleb128 .LEHE111-.LEHB111
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB112-.LFB6881
	.uleb128 .LEHE112-.LEHB112
	.uleb128 .L1088-.LFB6881
	.uleb128 0
	.uleb128 .LEHB113-.LFB6881
	.uleb128 .LEHE113-.LEHB113
	.uleb128 .L1087-.LFB6881
	.uleb128 0
	.uleb128 .LEHB114-.LFB6881
	.uleb128 .LEHE114-.LEHB114
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB115-.LFB6881
	.uleb128 .LEHE115-.LEHB115
	.uleb128 .L1088-.LFB6881
	.uleb128 0
.LLSDACSE6881:
	.section	.text._ZNK2v88internal6torque15PeekInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.cfi_endproc
	.section	.text.unlikely._ZNK2v88internal6torque15PeekInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6881
	.type	_ZNK2v88internal6torque15PeekInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE.cold, @function
_ZNK2v88internal6torque15PeekInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE.cold:
.LFSB6881:
.L1080:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-96(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L1070
	call	_ZdlPv@PLT
.L1070:
	movq	-128(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1082
	call	_ZdlPv@PLT
.L1082:
	movq	%r12, %rdi
.LEHB116:
	call	_Unwind_Resume@PLT
.LEHE116:
	.cfi_endproc
.LFE6881:
	.section	.gcc_except_table._ZNK2v88internal6torque15PeekInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
.LLSDAC6881:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6881-.LLSDACSBC6881
.LLSDACSBC6881:
	.uleb128 .LEHB116-.LCOLDB38
	.uleb128 .LEHE116-.LEHB116
	.uleb128 0
	.uleb128 0
.LLSDACSEC6881:
	.section	.text.unlikely._ZNK2v88internal6torque15PeekInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.section	.text._ZNK2v88internal6torque15PeekInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.size	_ZNK2v88internal6torque15PeekInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE, .-_ZNK2v88internal6torque15PeekInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.section	.text.unlikely._ZNK2v88internal6torque15PeekInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.size	_ZNK2v88internal6torque15PeekInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE.cold, .-_ZNK2v88internal6torque15PeekInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE.cold
.LCOLDE38:
	.section	.text._ZNK2v88internal6torque15PeekInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
.LHOTE38:
	.section	.text._ZN2v88internal6torque11ReportErrorIJRA11_KcRlRA17_S3_RKNS1_4TypeES8_SB_EEEvDpOT_,"axG",@progbits,_ZN2v88internal6torque11ReportErrorIJRA11_KcRlRA17_S3_RKNS1_4TypeES8_SB_EEEvDpOT_,comdat
	.p2align 4
	.weak	_ZN2v88internal6torque11ReportErrorIJRA11_KcRlRA17_S3_RKNS1_4TypeES8_SB_EEEvDpOT_
	.type	_ZN2v88internal6torque11ReportErrorIJRA11_KcRlRA17_S3_RKNS1_4TypeES8_SB_EEEvDpOT_, @function
_ZN2v88internal6torque11ReportErrorIJRA11_KcRlRA17_S3_RKNS1_4TypeES8_SB_EEEvDpOT_:
.LFB8211:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA8211
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-432(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-448(%rbp), %rbx
	subq	$536, %rsp
	movq	%rdi, -568(%rbp)
	movq	%rbx, %rdi
	movq	%rsi, -552(%rbp)
	movq	%r9, -560(%rbp)
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
.LEHB117:
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEC1Ev@PLT
.LEHE117:
	movq	-568(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %rsi
.LEHB118:
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	-552(%rbp), %rax
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZNSo9_M_insertIlEERSoT_@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6torquelsERSoRKNS1_4TypeE
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	-560(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6torquelsERSoRKNS1_4TypeE
	leaq	-544(%rbp), %r13
	leaq	-424(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNKSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEE3strEv@PLT
.LEHE118:
	leaq	-512(%rbp), %r12
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
.LEHB119:
	call	_ZN2v88internal6torque14MessageBuilderC1ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_13TorqueMessage4KindE@PLT
.LEHE119:
	movq	-544(%rbp), %rdi
	leaq	-528(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1117
	call	_ZdlPv@PLT
.L1117:
	movq	%rbx, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r12, %rdi
.LEHB120:
	call	_ZNK2v88internal6torque14MessageBuilder5ThrowEv@PLT
.LEHE120:
.L1124:
	endbr64
	movq	%rax, %r12
	jmp	.L1120
.L1123:
	endbr64
	movq	%rax, %r13
	jmp	.L1121
.L1125:
	endbr64
	movq	%rax, %r12
.L1118:
	movq	-544(%rbp), %rdi
	leaq	-528(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1120
	call	_ZdlPv@PLT
.L1120:
	movq	%rbx, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r12, %rdi
.LEHB121:
	call	_Unwind_Resume@PLT
.L1121:
	movq	%r12, %rdi
	call	_ZN2v88internal6torque14MessageBuilderD1Ev
	movq	%r13, %rdi
	call	_Unwind_Resume@PLT
.LEHE121:
	.cfi_endproc
.LFE8211:
	.section	.gcc_except_table._ZN2v88internal6torque11ReportErrorIJRA11_KcRlRA17_S3_RKNS1_4TypeES8_SB_EEEvDpOT_,"aG",@progbits,_ZN2v88internal6torque11ReportErrorIJRA11_KcRlRA17_S3_RKNS1_4TypeES8_SB_EEEvDpOT_,comdat
.LLSDA8211:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE8211-.LLSDACSB8211
.LLSDACSB8211:
	.uleb128 .LEHB117-.LFB8211
	.uleb128 .LEHE117-.LEHB117
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB118-.LFB8211
	.uleb128 .LEHE118-.LEHB118
	.uleb128 .L1124-.LFB8211
	.uleb128 0
	.uleb128 .LEHB119-.LFB8211
	.uleb128 .LEHE119-.LEHB119
	.uleb128 .L1125-.LFB8211
	.uleb128 0
	.uleb128 .LEHB120-.LFB8211
	.uleb128 .LEHE120-.LEHB120
	.uleb128 .L1123-.LFB8211
	.uleb128 0
	.uleb128 .LEHB121-.LFB8211
	.uleb128 .LEHE121-.LEHB121
	.uleb128 0
	.uleb128 0
.LLSDACSE8211:
	.section	.text._ZN2v88internal6torque11ReportErrorIJRA11_KcRlRA17_S3_RKNS1_4TypeES8_SB_EEEvDpOT_,"axG",@progbits,_ZN2v88internal6torque11ReportErrorIJRA11_KcRlRA17_S3_RKNS1_4TypeES8_SB_EEEvDpOT_,comdat
	.size	_ZN2v88internal6torque11ReportErrorIJRA11_KcRlRA17_S3_RKNS1_4TypeES8_SB_EEEvDpOT_, .-_ZN2v88internal6torque11ReportErrorIJRA11_KcRlRA17_S3_RKNS1_4TypeES8_SB_EEEvDpOT_
	.section	.text._ZN2v88internal6torque11ReportErrorIJRA21_KcEEEvDpOT_,"axG",@progbits,_ZN2v88internal6torque11ReportErrorIJRA21_KcEEEvDpOT_,comdat
	.p2align 4
	.weak	_ZN2v88internal6torque11ReportErrorIJRA21_KcEEEvDpOT_
	.type	_ZN2v88internal6torque11ReportErrorIJRA21_KcEEEvDpOT_, @function
_ZN2v88internal6torque11ReportErrorIJRA21_KcEEEvDpOT_:
.LFB8230:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA8230
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-96(%rbp), %r12
	movq	%r12, %rdi
	subq	$80, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
.LEHB122:
	call	_ZN2v88internal6torqueL7MessageIJRA23_KcEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
.LEHE122:
	movq	%r12, %rdi
.LEHB123:
	call	_ZNK2v88internal6torque14MessageBuilder5ThrowEv@PLT
.LEHE123:
.L1130:
	endbr64
	movq	%rax, %r13
.L1128:
	movq	%r12, %rdi
	call	_ZN2v88internal6torque14MessageBuilderD1Ev
	movq	%r13, %rdi
.LEHB124:
	call	_Unwind_Resume@PLT
.LEHE124:
	.cfi_endproc
.LFE8230:
	.section	.gcc_except_table._ZN2v88internal6torque11ReportErrorIJRA21_KcEEEvDpOT_,"aG",@progbits,_ZN2v88internal6torque11ReportErrorIJRA21_KcEEEvDpOT_,comdat
.LLSDA8230:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE8230-.LLSDACSB8230
.LLSDACSB8230:
	.uleb128 .LEHB122-.LFB8230
	.uleb128 .LEHE122-.LEHB122
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB123-.LFB8230
	.uleb128 .LEHE123-.LEHB123
	.uleb128 .L1130-.LFB8230
	.uleb128 0
	.uleb128 .LEHB124-.LFB8230
	.uleb128 .LEHE124-.LEHB124
	.uleb128 0
	.uleb128 0
.LLSDACSE8230:
	.section	.text._ZN2v88internal6torque11ReportErrorIJRA21_KcEEEvDpOT_,"axG",@progbits,_ZN2v88internal6torque11ReportErrorIJRA21_KcEEEvDpOT_,comdat
	.size	_ZN2v88internal6torque11ReportErrorIJRA21_KcEEEvDpOT_, .-_ZN2v88internal6torque11ReportErrorIJRA21_KcEEEvDpOT_
	.section	.text._ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E,"axG",@progbits,_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	.type	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E, @function
_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E:
.LFB8664:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L1147
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
.L1136:
	movq	24(%r12), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r12), %rdi
	leaq	48(%r12), %rax
	movq	16(%r12), %rbx
	cmpq	%rax, %rdi
	je	.L1134
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L1132
.L1135:
	movq	%rbx, %r12
	jmp	.L1136
	.p2align 4,,10
	.p2align 3
.L1134:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L1135
.L1132:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1147:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.cfi_endproc
.LFE8664:
	.size	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E, .-_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	.section	.text._ZN2v88internal6torque7TopTypeD0Ev,"axG",@progbits,_ZN2v88internal6torque7TopTypeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque7TopTypeD0Ev
	.type	_ZN2v88internal6torque7TopTypeD0Ev, @function
_ZN2v88internal6torque7TopTypeD0Ev:
.LFB9178:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal6torque7TopTypeE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rax, (%rdi)
	movq	72(%rdi), %rdi
	leaq	88(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1151
	call	_ZdlPv@PLT
.L1151:
	movq	40(%r12), %r13
	leaq	16+_ZTVN2v88internal6torque4TypeE(%rip), %rax
	leaq	24(%r12), %r14
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L1152
.L1155:
	movq	24(%r13), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r13), %rdi
	leaq	48(%r13), %rax
	movq	16(%r13), %rbx
	cmpq	%rax, %rdi
	je	.L1153
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L1152
.L1154:
	movq	%rbx, %r13
	jmp	.L1155
	.p2align 4,,10
	.p2align 3
.L1153:
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L1154
.L1152:
	popq	%rbx
	movq	%r12, %rdi
	movl	$112, %esi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE9178:
	.size	_ZN2v88internal6torque7TopTypeD0Ev, .-_ZN2v88internal6torque7TopTypeD0Ev
	.section	.text._ZN2v88internal6torque7TopTypeD2Ev,"axG",@progbits,_ZN2v88internal6torque7TopTypeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque7TopTypeD2Ev
	.type	_ZN2v88internal6torque7TopTypeD2Ev, @function
_ZN2v88internal6torque7TopTypeD2Ev:
.LFB9176:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal6torque7TopTypeE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	%rax, (%rdi)
	movq	72(%rdi), %rdi
	leaq	88(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1167
	call	_ZdlPv@PLT
.L1167:
	leaq	16+_ZTVN2v88internal6torque4TypeE(%rip), %rax
	leaq	24(%r12), %r13
	movq	%rax, (%r12)
	movq	40(%r12), %r12
	testq	%r12, %r12
	je	.L1166
.L1171:
	movq	24(%r12), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r12), %rdi
	leaq	48(%r12), %rax
	movq	16(%r12), %rbx
	cmpq	%rax, %rdi
	je	.L1169
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L1166
.L1170:
	movq	%rbx, %r12
	jmp	.L1171
	.p2align 4,,10
	.p2align 3
.L1169:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L1170
.L1166:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9176:
	.size	_ZN2v88internal6torque7TopTypeD2Ev, .-_ZN2v88internal6torque7TopTypeD2Ev
	.weak	_ZN2v88internal6torque7TopTypeD1Ev
	.set	_ZN2v88internal6torque7TopTypeD1Ev,_ZN2v88internal6torque7TopTypeD2Ev
	.section	.text._ZNSt6vectorIPKN2v88internal6torque4TypeESaIS5_EE12emplace_backIJS5_EEEvDpOT_,"axG",@progbits,_ZNSt6vectorIPKN2v88internal6torque4TypeESaIS5_EE12emplace_backIJS5_EEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPKN2v88internal6torque4TypeESaIS5_EE12emplace_backIJS5_EEEvDpOT_
	.type	_ZNSt6vectorIPKN2v88internal6torque4TypeESaIS5_EE12emplace_backIJS5_EEEvDpOT_, @function
_ZNSt6vectorIPKN2v88internal6torque4TypeESaIS5_EE12emplace_backIJS5_EEEvDpOT_:
.LFB8849:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	8(%rdi), %rdx
	cmpq	16(%rdi), %rdx
	je	.L1183
	movq	(%rsi), %rax
	movq	%rax, (%rdx)
	addq	$8, 8(%rdi)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1183:
	.cfi_restore_state
	movq	(%rdi), %r15
	subq	%r15, %rdx
	movq	%rdx, %rax
	movq	%rdx, %r12
	movabsq	$1152921504606846975, %rdx
	sarq	$3, %rax
	cmpq	%rdx, %rax
	je	.L1198
	testq	%rax, %rax
	je	.L1191
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rcx
	cmpq	%rcx, %rax
	jbe	.L1199
.L1186:
	movq	%r14, %rdi
	movq	%rsi, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r13
	addq	%rax, %r14
.L1187:
	movq	(%rsi), %rax
	movq	%rax, 0(%r13,%r12)
	leaq	8(%r13,%r12), %rax
	movq	%rax, -56(%rbp)
	testq	%r12, %r12
	jg	.L1200
	testq	%r15, %r15
	jne	.L1189
.L1190:
	movq	%r13, %xmm0
	movq	%r14, 16(%rbx)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1200:
	.cfi_restore_state
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	memmove@PLT
.L1189:
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	jmp	.L1190
	.p2align 4,,10
	.p2align 3
.L1199:
	testq	%rcx, %rcx
	jne	.L1201
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	jmp	.L1187
	.p2align 4,,10
	.p2align 3
.L1191:
	movl	$8, %r14d
	jmp	.L1186
.L1198:
	leaq	.LC2(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1201:
	cmpq	%rdx, %rcx
	cmovbe	%rcx, %rdx
	leaq	0(,%rdx,8), %r14
	jmp	.L1186
	.cfi_endproc
.LFE8849:
	.size	_ZNSt6vectorIPKN2v88internal6torque4TypeESaIS5_EE12emplace_backIJS5_EEEvDpOT_, .-_ZNSt6vectorIPKN2v88internal6torque4TypeESaIS5_EE12emplace_backIJS5_EEEvDpOT_
	.section	.text.unlikely._ZNK2v88internal6torque31CreateFieldReferenceInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE,"ax",@progbits
	.align 2
.LCOLDB39:
	.section	.text._ZNK2v88internal6torque31CreateFieldReferenceInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE,"ax",@progbits
.LHOTB39:
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal6torque31CreateFieldReferenceInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.type	_ZNK2v88internal6torque31CreateFieldReferenceInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE, @function
_ZNK2v88internal6torque31CreateFieldReferenceInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE:
.LFB6902:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6902
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$64, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	32(%rdi), %r14
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	8(%rsi), %rax
	cmpq	(%rsi), %rax
	je	.L1212
	movq	-8(%rax), %r13
	movq	%rsi, %r12
	movq	%r14, %rsi
	movq	0(%r13), %rax
	movq	%r13, %rdi
.LEHB125:
	call	*16(%rax)
	testb	%al, %al
	je	.L1213
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_10TypeOracleEEERPNT_5ScopeEv@PLT
.LEHE125:
	leaq	-64(%rbp), %r13
	leaq	-80(%rbp), %rdi
	movl	$29300, %eax
	movq	%r13, -80(%rbp)
	movl	$1886678633, -64(%rbp)
	movw	%ax, -60(%rbp)
	movq	$6, -72(%rbp)
	movb	$0, -58(%rbp)
.LEHB126:
	call	_ZN2v88internal6torque12Declarations16LookupGlobalTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
.LEHE126:
	movq	-80(%rbp), %rdi
	movq	%rax, %rbx
	cmpq	%r13, %rdi
	je	.L1205
	call	_ZdlPv@PLT
.L1205:
	leaq	-88(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rbx, -88(%rbp)
.LEHB127:
	call	_ZNSt6vectorIPKN2v88internal6torque4TypeESaIS5_EE12emplace_backIJS5_EEEvDpOT_
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1214
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1212:
	.cfi_restore_state
	xorl	%edx, %edx
	orq	$-1, %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L1214:
	call	__stack_chk_fail@PLT
.L1213:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal6torque12_GLOBAL__N_113ExpectSubtypeEPKNS1_4TypeES5_.part.0
.LEHE127:
.L1209:
	endbr64
	movq	%rax, %r12
	jmp	.L1206
	.section	.gcc_except_table._ZNK2v88internal6torque31CreateFieldReferenceInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE,"a",@progbits
.LLSDA6902:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6902-.LLSDACSB6902
.LLSDACSB6902:
	.uleb128 .LEHB125-.LFB6902
	.uleb128 .LEHE125-.LEHB125
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB126-.LFB6902
	.uleb128 .LEHE126-.LEHB126
	.uleb128 .L1209-.LFB6902
	.uleb128 0
	.uleb128 .LEHB127-.LFB6902
	.uleb128 .LEHE127-.LEHB127
	.uleb128 0
	.uleb128 0
.LLSDACSE6902:
	.section	.text._ZNK2v88internal6torque31CreateFieldReferenceInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.cfi_endproc
	.section	.text.unlikely._ZNK2v88internal6torque31CreateFieldReferenceInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6902
	.type	_ZNK2v88internal6torque31CreateFieldReferenceInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE.cold, @function
_ZNK2v88internal6torque31CreateFieldReferenceInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE.cold:
.LFSB6902:
.L1206:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	movq	-80(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L1207
	call	_ZdlPv@PLT
.L1207:
	movq	%r12, %rdi
.LEHB128:
	call	_Unwind_Resume@PLT
.LEHE128:
	.cfi_endproc
.LFE6902:
	.section	.gcc_except_table._ZNK2v88internal6torque31CreateFieldReferenceInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
.LLSDAC6902:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6902-.LLSDACSBC6902
.LLSDACSBC6902:
	.uleb128 .LEHB128-.LCOLDB39
	.uleb128 .LEHE128-.LEHB128
	.uleb128 0
	.uleb128 0
.LLSDACSEC6902:
	.section	.text.unlikely._ZNK2v88internal6torque31CreateFieldReferenceInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.section	.text._ZNK2v88internal6torque31CreateFieldReferenceInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.size	_ZNK2v88internal6torque31CreateFieldReferenceInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE, .-_ZNK2v88internal6torque31CreateFieldReferenceInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.section	.text.unlikely._ZNK2v88internal6torque31CreateFieldReferenceInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.size	_ZNK2v88internal6torque31CreateFieldReferenceInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE.cold, .-_ZNK2v88internal6torque31CreateFieldReferenceInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE.cold
.LCOLDE39:
	.section	.text._ZNK2v88internal6torque31CreateFieldReferenceInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
.LHOTE39:
	.section	.text.unlikely._ZNK2v88internal6torque24LoadReferenceInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE,"ax",@progbits
	.align 2
.LCOLDB40:
	.section	.text._ZNK2v88internal6torque24LoadReferenceInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE,"ax",@progbits
.LHOTB40:
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal6torque24LoadReferenceInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.type	_ZNK2v88internal6torque24LoadReferenceInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE, @function
_ZNK2v88internal6torque24LoadReferenceInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE:
.LFB6903:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6903
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-96(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-80(%rbp), %rbx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8(%rsi), %rax
	movq	-8(%rax), %r14
	subq	$8, %rax
	movq	%rax, 8(%rsi)
.LEHB129:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_10TypeOracleEEERPNT_5ScopeEv@PLT
.LEHE129:
	movl	$29300, %edx
	movq	%r15, %rdi
	movq	%rbx, -96(%rbp)
	movl	$1886678633, -80(%rbp)
	movw	%dx, -76(%rbp)
	movq	$6, -88(%rbp)
	movb	$0, -74(%rbp)
.LEHB130:
	call	_ZN2v88internal6torque12Declarations16LookupGlobalTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
.LEHE130:
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1216
	movq	%rax, -120(%rbp)
	call	_ZdlPv@PLT
	movq	-120(%rbp), %rax
.L1216:
	cmpq	%rax, %r14
	jne	.L1232
.LEHB131:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_10TypeOracleEEERPNT_5ScopeEv@PLT
.LEHE131:
	movq	%r15, %rdi
	movq	%rbx, -96(%rbp)
	movabsq	$7307761438689420616, %rax
	movq	%rax, -80(%rbp)
	movl	$29795, %eax
	movw	%ax, -72(%rbp)
	movq	$10, -88(%rbp)
	movb	$0, -70(%rbp)
.LEHB132:
	call	_ZN2v88internal6torque12Declarations16LookupGlobalTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
.LEHE132:
	movq	-96(%rbp), %rdi
	movq	%rax, %r14
	cmpq	%rbx, %rdi
	je	.L1221
	call	_ZdlPv@PLT
.L1221:
	movq	8(%r12), %rax
	movq	%r14, %rsi
	movq	-8(%rax), %r15
	subq	$8, %rax
	movq	%rax, 8(%r12)
	movq	(%r15), %rax
	movq	%r15, %rdi
.LEHB133:
	call	*16(%rax)
	testb	%al, %al
	je	.L1233
	movq	32(%r13), %rax
	leaq	-104(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -104(%rbp)
	call	_ZNSt6vectorIPKN2v88internal6torque4TypeESaIS5_EE12emplace_backIJS5_EEEvDpOT_
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1234
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1232:
	.cfi_restore_state
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal6torque12_GLOBAL__N_110ExpectTypeEPKNS1_4TypeES5_.part.0
.L1234:
	call	__stack_chk_fail@PLT
.L1233:
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal6torque12_GLOBAL__N_113ExpectSubtypeEPKNS1_4TypeES5_.part.0
.LEHE133:
.L1227:
	endbr64
	movq	%rax, %r12
	jmp	.L1219
.L1228:
	endbr64
	movq	%rax, %r12
	jmp	.L1224
	.section	.gcc_except_table._ZNK2v88internal6torque24LoadReferenceInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE,"a",@progbits
.LLSDA6903:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6903-.LLSDACSB6903
.LLSDACSB6903:
	.uleb128 .LEHB129-.LFB6903
	.uleb128 .LEHE129-.LEHB129
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB130-.LFB6903
	.uleb128 .LEHE130-.LEHB130
	.uleb128 .L1227-.LFB6903
	.uleb128 0
	.uleb128 .LEHB131-.LFB6903
	.uleb128 .LEHE131-.LEHB131
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB132-.LFB6903
	.uleb128 .LEHE132-.LEHB132
	.uleb128 .L1228-.LFB6903
	.uleb128 0
	.uleb128 .LEHB133-.LFB6903
	.uleb128 .LEHE133-.LEHB133
	.uleb128 0
	.uleb128 0
.LLSDACSE6903:
	.section	.text._ZNK2v88internal6torque24LoadReferenceInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.cfi_endproc
	.section	.text.unlikely._ZNK2v88internal6torque24LoadReferenceInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6903
	.type	_ZNK2v88internal6torque24LoadReferenceInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE.cold, @function
_ZNK2v88internal6torque24LoadReferenceInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE.cold:
.LFSB6903:
.L1219:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1220
	call	_ZdlPv@PLT
.L1220:
	movq	%r12, %rdi
.LEHB134:
	call	_Unwind_Resume@PLT
.L1224:
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1225
	call	_ZdlPv@PLT
.L1225:
	movq	%r12, %rdi
	call	_Unwind_Resume@PLT
.LEHE134:
	.cfi_endproc
.LFE6903:
	.section	.gcc_except_table._ZNK2v88internal6torque24LoadReferenceInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
.LLSDAC6903:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6903-.LLSDACSBC6903
.LLSDACSBC6903:
	.uleb128 .LEHB134-.LCOLDB40
	.uleb128 .LEHE134-.LEHB134
	.uleb128 0
	.uleb128 0
.LLSDACSEC6903:
	.section	.text.unlikely._ZNK2v88internal6torque24LoadReferenceInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.section	.text._ZNK2v88internal6torque24LoadReferenceInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.size	_ZNK2v88internal6torque24LoadReferenceInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE, .-_ZNK2v88internal6torque24LoadReferenceInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.section	.text.unlikely._ZNK2v88internal6torque24LoadReferenceInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.size	_ZNK2v88internal6torque24LoadReferenceInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE.cold, .-_ZNK2v88internal6torque24LoadReferenceInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE.cold
.LCOLDE40:
	.section	.text._ZNK2v88internal6torque24LoadReferenceInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
.LHOTE40:
	.section	.text._ZNSt6vectorIPN2v88internal6torque5BlockESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_,"axG",@progbits,_ZNSt6vectorIPN2v88internal6torque5BlockESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPN2v88internal6torque5BlockESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	.type	_ZNSt6vectorIPN2v88internal6torque5BlockESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_, @function
_ZNSt6vectorIPN2v88internal6torque5BlockESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_:
.LFB8892:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r9
	movq	%r13, %rax
	subq	%r9, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L1249
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L1245
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L1250
.L1237:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L1244:
	movq	(%r15), %rax
	subq	%r8, %r13
	leaq	8(%rbx,%rdx), %r15
	movq	%rax, (%rbx,%rdx)
	leaq	(%r15,%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L1251
	testq	%r13, %r13
	jg	.L1240
	testq	%r9, %r9
	jne	.L1243
.L1241:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1251:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jg	.L1240
.L1243:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L1241
	.p2align 4,,10
	.p2align 3
.L1250:
	testq	%rsi, %rsi
	jne	.L1238
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L1244
	.p2align 4,,10
	.p2align 3
.L1240:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L1241
	jmp	.L1243
	.p2align 4,,10
	.p2align 3
.L1245:
	movl	$8, %r14d
	jmp	.L1237
.L1249:
	leaq	.LC2(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1238:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$3, %r14
	jmp	.L1237
	.cfi_endproc
.LFE8892:
	.size	_ZNSt6vectorIPN2v88internal6torque5BlockESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_, .-_ZNSt6vectorIPN2v88internal6torque5BlockESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	.section	.text._ZNK2v88internal6torque26ConstexprBranchInstruction21AppendSuccessorBlocksEPSt6vectorIPNS1_5BlockESaIS5_EE,"axG",@progbits,_ZNK2v88internal6torque26ConstexprBranchInstruction21AppendSuccessorBlocksEPSt6vectorIPNS1_5BlockESaIS5_EE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal6torque26ConstexprBranchInstruction21AppendSuccessorBlocksEPSt6vectorIPNS1_5BlockESaIS5_EE
	.type	_ZNK2v88internal6torque26ConstexprBranchInstruction21AppendSuccessorBlocksEPSt6vectorIPNS1_5BlockESaIS5_EE, @function
_ZNK2v88internal6torque26ConstexprBranchInstruction21AppendSuccessorBlocksEPSt6vectorIPNS1_5BlockESaIS5_EE:
.LFB5863:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	8(%rsi), %rsi
	cmpq	16(%r12), %rsi
	je	.L1253
	movq	64(%rdi), %rax
	movq	%rax, (%rsi)
	movq	8(%r12), %rax
	leaq	8(%rax), %rsi
	movq	%rsi, 8(%r12)
	cmpq	%rsi, 16(%r12)
	je	.L1255
.L1259:
	movq	72(%rbx), %rax
	movq	%rax, (%rsi)
	popq	%rbx
	addq	$8, 8(%r12)
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1253:
	.cfi_restore_state
	leaq	64(%rdi), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorIPN2v88internal6torque5BlockESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	movq	8(%r12), %rsi
	cmpq	%rsi, 16(%r12)
	jne	.L1259
.L1255:
	leaq	72(%rbx), %rdx
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt6vectorIPN2v88internal6torque5BlockESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	.cfi_endproc
.LFE5863:
	.size	_ZNK2v88internal6torque26ConstexprBranchInstruction21AppendSuccessorBlocksEPSt6vectorIPNS1_5BlockESaIS5_EE, .-_ZNK2v88internal6torque26ConstexprBranchInstruction21AppendSuccessorBlocksEPSt6vectorIPNS1_5BlockESaIS5_EE
	.section	.text._ZNK2v88internal6torque17BranchInstruction21AppendSuccessorBlocksEPSt6vectorIPNS1_5BlockESaIS5_EE,"axG",@progbits,_ZNK2v88internal6torque17BranchInstruction21AppendSuccessorBlocksEPSt6vectorIPNS1_5BlockESaIS5_EE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal6torque17BranchInstruction21AppendSuccessorBlocksEPSt6vectorIPNS1_5BlockESaIS5_EE
	.type	_ZNK2v88internal6torque17BranchInstruction21AppendSuccessorBlocksEPSt6vectorIPNS1_5BlockESaIS5_EE, @function
_ZNK2v88internal6torque17BranchInstruction21AppendSuccessorBlocksEPSt6vectorIPNS1_5BlockESaIS5_EE:
.LFB5858:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	8(%rsi), %rsi
	cmpq	16(%r12), %rsi
	je	.L1261
	movq	32(%rdi), %rax
	movq	%rax, (%rsi)
	movq	8(%r12), %rax
	leaq	8(%rax), %rsi
	movq	%rsi, 8(%r12)
	cmpq	%rsi, 16(%r12)
	je	.L1263
.L1267:
	movq	40(%rbx), %rax
	movq	%rax, (%rsi)
	popq	%rbx
	addq	$8, 8(%r12)
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1261:
	.cfi_restore_state
	leaq	32(%rdi), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorIPN2v88internal6torque5BlockESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	movq	8(%r12), %rsi
	cmpq	%rsi, 16(%r12)
	jne	.L1267
.L1263:
	leaq	40(%rbx), %rdx
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt6vectorIPN2v88internal6torque5BlockESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	.cfi_endproc
.LFE5858:
	.size	_ZNK2v88internal6torque17BranchInstruction21AppendSuccessorBlocksEPSt6vectorIPNS1_5BlockESaIS5_EE, .-_ZNK2v88internal6torque17BranchInstruction21AppendSuccessorBlocksEPSt6vectorIPNS1_5BlockESaIS5_EE
	.section	.text._ZNK2v88internal6torque23CallCsaMacroInstruction21AppendSuccessorBlocksEPSt6vectorIPNS1_5BlockESaIS5_EE,"axG",@progbits,_ZNK2v88internal6torque23CallCsaMacroInstruction21AppendSuccessorBlocksEPSt6vectorIPNS1_5BlockESaIS5_EE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal6torque23CallCsaMacroInstruction21AppendSuccessorBlocksEPSt6vectorIPNS1_5BlockESaIS5_EE
	.type	_ZNK2v88internal6torque23CallCsaMacroInstruction21AppendSuccessorBlocksEPSt6vectorIPNS1_5BlockESaIS5_EE, @function
_ZNK2v88internal6torque23CallCsaMacroInstruction21AppendSuccessorBlocksEPSt6vectorIPNS1_5BlockESaIS5_EE:
.LFB5838:
	.cfi_startproc
	endbr64
	cmpb	$0, 64(%rdi)
	movq	%rsi, %r8
	jne	.L1271
	ret
	.p2align 4,,10
	.p2align 3
.L1271:
	movq	8(%rsi), %rsi
	cmpq	16(%r8), %rsi
	je	.L1270
	movq	72(%rdi), %rax
	movq	%rax, (%rsi)
	addq	$8, 8(%r8)
	ret
	.p2align 4,,10
	.p2align 3
.L1270:
	leaq	72(%rdi), %rdx
	movq	%r8, %rdi
	jmp	_ZNSt6vectorIPN2v88internal6torque5BlockESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	.cfi_endproc
.LFE5838:
	.size	_ZNK2v88internal6torque23CallCsaMacroInstruction21AppendSuccessorBlocksEPSt6vectorIPNS1_5BlockESaIS5_EE, .-_ZNK2v88internal6torque23CallCsaMacroInstruction21AppendSuccessorBlocksEPSt6vectorIPNS1_5BlockESaIS5_EE
	.section	.text._ZNK2v88internal6torque22CallBuiltinInstruction21AppendSuccessorBlocksEPSt6vectorIPNS1_5BlockESaIS5_EE,"axG",@progbits,_ZNK2v88internal6torque22CallBuiltinInstruction21AppendSuccessorBlocksEPSt6vectorIPNS1_5BlockESaIS5_EE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal6torque22CallBuiltinInstruction21AppendSuccessorBlocksEPSt6vectorIPNS1_5BlockESaIS5_EE
	.type	_ZNK2v88internal6torque22CallBuiltinInstruction21AppendSuccessorBlocksEPSt6vectorIPNS1_5BlockESaIS5_EE, @function
_ZNK2v88internal6torque22CallBuiltinInstruction21AppendSuccessorBlocksEPSt6vectorIPNS1_5BlockESaIS5_EE:
.LFB5848:
	.cfi_startproc
	endbr64
	cmpb	$0, 48(%rdi)
	movq	%rsi, %r8
	jne	.L1275
	ret
	.p2align 4,,10
	.p2align 3
.L1275:
	movq	8(%rsi), %rsi
	cmpq	16(%r8), %rsi
	je	.L1274
	movq	56(%rdi), %rax
	movq	%rax, (%rsi)
	addq	$8, 8(%r8)
	ret
	.p2align 4,,10
	.p2align 3
.L1274:
	leaq	56(%rdi), %rdx
	movq	%r8, %rdi
	jmp	_ZNSt6vectorIPN2v88internal6torque5BlockESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	.cfi_endproc
.LFE5848:
	.size	_ZNK2v88internal6torque22CallBuiltinInstruction21AppendSuccessorBlocksEPSt6vectorIPNS1_5BlockESaIS5_EE, .-_ZNK2v88internal6torque22CallBuiltinInstruction21AppendSuccessorBlocksEPSt6vectorIPNS1_5BlockESaIS5_EE
	.section	.text._ZNK2v88internal6torque22CallRuntimeInstruction21AppendSuccessorBlocksEPSt6vectorIPNS1_5BlockESaIS5_EE,"axG",@progbits,_ZNK2v88internal6torque22CallRuntimeInstruction21AppendSuccessorBlocksEPSt6vectorIPNS1_5BlockESaIS5_EE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal6torque22CallRuntimeInstruction21AppendSuccessorBlocksEPSt6vectorIPNS1_5BlockESaIS5_EE
	.type	_ZNK2v88internal6torque22CallRuntimeInstruction21AppendSuccessorBlocksEPSt6vectorIPNS1_5BlockESaIS5_EE, @function
_ZNK2v88internal6torque22CallRuntimeInstruction21AppendSuccessorBlocksEPSt6vectorIPNS1_5BlockESaIS5_EE:
.LFB5856:
	.cfi_startproc
	endbr64
	cmpb	$0, 48(%rdi)
	movq	%rsi, %r8
	jne	.L1279
	ret
	.p2align 4,,10
	.p2align 3
.L1279:
	movq	8(%rsi), %rsi
	cmpq	16(%r8), %rsi
	je	.L1278
	movq	56(%rdi), %rax
	movq	%rax, (%rsi)
	addq	$8, 8(%r8)
	ret
	.p2align 4,,10
	.p2align 3
.L1278:
	leaq	56(%rdi), %rdx
	movq	%r8, %rdi
	jmp	_ZNSt6vectorIPN2v88internal6torque5BlockESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	.cfi_endproc
.LFE5856:
	.size	_ZNK2v88internal6torque22CallRuntimeInstruction21AppendSuccessorBlocksEPSt6vectorIPNS1_5BlockESaIS5_EE, .-_ZNK2v88internal6torque22CallRuntimeInstruction21AppendSuccessorBlocksEPSt6vectorIPNS1_5BlockESaIS5_EE
	.section	.text._ZNK2v88internal6torque32CallCsaMacroAndBranchInstruction21AppendSuccessorBlocksEPSt6vectorIPNS1_5BlockESaIS5_EE,"axG",@progbits,_ZNK2v88internal6torque32CallCsaMacroAndBranchInstruction21AppendSuccessorBlocksEPSt6vectorIPNS1_5BlockESaIS5_EE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal6torque32CallCsaMacroAndBranchInstruction21AppendSuccessorBlocksEPSt6vectorIPNS1_5BlockESaIS5_EE
	.type	_ZNK2v88internal6torque32CallCsaMacroAndBranchInstruction21AppendSuccessorBlocksEPSt6vectorIPNS1_5BlockESaIS5_EE, @function
_ZNK2v88internal6torque32CallCsaMacroAndBranchInstruction21AppendSuccessorBlocksEPSt6vectorIPNS1_5BlockESaIS5_EE:
.LFB5843:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 104(%rdi)
	jne	.L1292
.L1281:
	cmpb	$0, 64(%r13)
	jne	.L1293
.L1283:
	movq	80(%r13), %r12
	movq	88(%r13), %r13
	cmpq	%r12, %r13
	je	.L1280
	leaq	-48(%rbp), %r14
	jmp	.L1288
	.p2align 4,,10
	.p2align 3
.L1294:
	addq	$8, %r12
	movq	%rax, (%rsi)
	addq	$8, 8(%rbx)
	cmpq	%r12, %r13
	je	.L1280
.L1288:
	movq	(%r12), %rax
	movq	8(%rbx), %rsi
	movq	%rax, -48(%rbp)
	cmpq	16(%rbx), %rsi
	jne	.L1294
	movq	%r14, %rdx
	movq	%rbx, %rdi
	addq	$8, %r12
	call	_ZNSt6vectorIPN2v88internal6torque5BlockESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	cmpq	%r12, %r13
	jne	.L1288
	.p2align 4,,10
	.p2align 3
.L1280:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1295
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1292:
	.cfi_restore_state
	movq	8(%rsi), %rsi
	cmpq	16(%rbx), %rsi
	je	.L1282
	movq	112(%rdi), %rax
	movq	%rax, (%rsi)
	addq	$8, 8(%rbx)
	cmpb	$0, 64(%r13)
	je	.L1283
.L1293:
	movq	8(%rbx), %rsi
	cmpq	16(%rbx), %rsi
	je	.L1284
	movq	72(%r13), %rax
	movq	%rax, (%rsi)
	addq	$8, 8(%rbx)
	jmp	.L1283
.L1282:
	leaq	112(%rdi), %rdx
	movq	%rbx, %rdi
	call	_ZNSt6vectorIPN2v88internal6torque5BlockESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	jmp	.L1281
.L1284:
	leaq	72(%r13), %rdx
	movq	%rbx, %rdi
	call	_ZNSt6vectorIPN2v88internal6torque5BlockESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	jmp	.L1283
.L1295:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5843:
	.size	_ZNK2v88internal6torque32CallCsaMacroAndBranchInstruction21AppendSuccessorBlocksEPSt6vectorIPNS1_5BlockESaIS5_EE, .-_ZNK2v88internal6torque32CallCsaMacroAndBranchInstruction21AppendSuccessorBlocksEPSt6vectorIPNS1_5BlockESaIS5_EE
	.section	.text._ZNSt6vectorISt10unique_ptrIN2v88internal6torque4TypeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_,"axG",@progbits,_ZNSt6vectorISt10unique_ptrIN2v88internal6torque4TypeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorISt10unique_ptrIN2v88internal6torque4TypeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	.type	_ZNSt6vectorISt10unique_ptrIN2v88internal6torque4TypeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_, @function
_ZNSt6vectorISt10unique_ptrIN2v88internal6torque4TypeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_:
.LFB9907:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rax
	movq	(%rdi), %r12
	movq	%rdi, -80(%rbp)
	movq	%rax, -72(%rbp)
	subq	%r12, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L1320
	movq	%rsi, %r9
	movq	%rsi, %r15
	movq	%rsi, %rbx
	subq	%r12, %r9
	testq	%rax, %rax
	je	.L1311
	movabsq	$9223372036854775800, %r13
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L1321
.L1298:
	movq	%r13, %rdi
	movq	%rdx, -96(%rbp)
	movq	%r9, -88(%rbp)
	call	_Znwm@PLT
	movq	-88(%rbp), %r9
	movq	-96(%rbp), %rdx
	addq	%rax, %r13
	movq	%rax, -56(%rbp)
	leaq	8(%rax), %rcx
	movq	%r13, -64(%rbp)
.L1310:
	movq	(%rdx), %rax
	movq	-56(%rbp), %rsi
	movq	$0, (%rdx)
	movq	%rax, (%rsi,%r9)
	cmpq	%r12, %r15
	je	.L1300
	movq	%rsi, %r13
	movq	%r12, %r14
	.p2align 4,,10
	.p2align 3
.L1304:
	movq	(%r14), %rcx
	movq	$0, (%r14)
	movq	%rcx, 0(%r13)
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1301
	movq	(%rdi), %rcx
	addq	$8, %r14
	addq	$8, %r13
	call	*8(%rcx)
	cmpq	%r14, %r15
	jne	.L1304
.L1302:
	movq	-56(%rbp), %rsi
	movq	%r15, %rax
	subq	%r12, %rax
	leaq	8(%rsi,%rax), %rcx
.L1300:
	movq	-72(%rbp), %rax
	cmpq	%rax, %r15
	je	.L1305
	movq	%rax, %r14
	subq	%r15, %r14
	leaq	-8(%r14), %r9
	movq	%r9, %rdi
	shrq	$3, %rdi
	addq	$1, %rdi
	testq	%r9, %r9
	je	.L1313
	movq	%rdi, %rdx
	xorl	%eax, %eax
	shrq	%rdx
	salq	$4, %rdx
	.p2align 4,,10
	.p2align 3
.L1307:
	movdqu	(%r15,%rax), %xmm1
	movups	%xmm1, (%rcx,%rax)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L1307
	movq	%rdi, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %r8
	leaq	(%rcx,%r8), %rdx
	leaq	(%r15,%r8), %rbx
	cmpq	%rax, %rdi
	je	.L1308
.L1306:
	movq	(%rbx), %rax
	movq	%rax, (%rdx)
.L1308:
	leaq	8(%rcx,%r9), %rcx
.L1305:
	testq	%r12, %r12
	je	.L1309
	movq	%r12, %rdi
	movq	%rcx, -72(%rbp)
	call	_ZdlPv@PLT
	movq	-72(%rbp), %rcx
.L1309:
	movq	-56(%rbp), %xmm0
	movq	-80(%rbp), %rax
	movq	%rcx, %xmm2
	movq	-64(%rbp), %rbx
	punpcklqdq	%xmm2, %xmm0
	movq	%rbx, 16(%rax)
	movups	%xmm0, (%rax)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1301:
	.cfi_restore_state
	addq	$8, %r14
	addq	$8, %r13
	cmpq	%r14, %r15
	jne	.L1304
	jmp	.L1302
	.p2align 4,,10
	.p2align 3
.L1321:
	testq	%rdi, %rdi
	jne	.L1299
	movq	$0, -64(%rbp)
	movl	$8, %ecx
	movq	$0, -56(%rbp)
	jmp	.L1310
	.p2align 4,,10
	.p2align 3
.L1311:
	movl	$8, %r13d
	jmp	.L1298
.L1313:
	movq	%rcx, %rdx
	jmp	.L1306
.L1299:
	cmpq	%rcx, %rdi
	movq	%rcx, %rax
	cmovbe	%rdi, %rax
	leaq	0(,%rax,8), %r13
	jmp	.L1298
.L1320:
	leaq	.LC2(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE9907:
	.size	_ZNSt6vectorISt10unique_ptrIN2v88internal6torque4TypeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_, .-_ZNSt6vectorISt10unique_ptrIN2v88internal6torque4TypeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	.section	.rodata._ZNK2v88internal6torque15InstructionBase24InvalidateTransientTypesEPNS1_5StackIPKNS1_4TypeEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC41:
	.string	" is made invalid by transitioning callable invocation at "
	.section	.rodata._ZNK2v88internal6torque15InstructionBase24InvalidateTransientTypesEPNS1_5StackIPKNS1_4TypeEEE.str1.1,"aMS",@progbits,1
.LC42:
	.string	":"
	.section	.text.unlikely._ZNK2v88internal6torque15InstructionBase24InvalidateTransientTypesEPNS1_5StackIPKNS1_4TypeEEE,"ax",@progbits
	.align 2
.LCOLDB43:
	.section	.text._ZNK2v88internal6torque15InstructionBase24InvalidateTransientTypesEPNS1_5StackIPKNS1_4TypeEEE,"ax",@progbits
.LHOTB43:
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal6torque15InstructionBase24InvalidateTransientTypesEPNS1_5StackIPKNS1_4TypeEEE
	.type	_ZNK2v88internal6torque15InstructionBase24InvalidateTransientTypesEPNS1_5StackIPKNS1_4TypeEEE, @function
_ZNK2v88internal6torque15InstructionBase24InvalidateTransientTypesEPNS1_5StackIPKNS1_4TypeEEE:
.LFB6887:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6887
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$744, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -712(%rbp)
	movq	.LC17(%rip), %xmm3
	movhps	.LC18(%rip), %xmm3
	movq	%fs:40, %rbx
	movq	%rbx, -56(%rbp)
	xorl	%ebx, %ebx
	movq	(%rsi), %rbx
	movaps	%xmm3, -768(%rbp)
	cmpq	8(%rsi), %rbx
	je	.L1322
	movq	%rdi, %r15
	jmp	.L1323
	.p2align 4,,10
	.p2align 3
.L1324:
	movq	-712(%rbp), %rax
	addq	$8, %rbx
	cmpq	%rbx, 8(%rax)
	je	.L1322
.L1323:
	movq	(%rbx), %rdi
	movq	(%rdi), %rax
.LEHB135:
	call	*40(%rax)
.LEHE135:
	testb	%al, %al
	je	.L1324
	leaq	-320(%rbp), %rax
	leaq	-448(%rbp), %r14
	movq	%rax, %rdi
	movq	%r14, -784(%rbp)
	movq	%rax, -720(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	xorl	%eax, %eax
	pxor	%xmm0, %xmm0
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rsi
	movq	$0, -104(%rbp)
	movq	%rcx, -448(%rbp)
	movq	%rsi, -320(%rbp)
	xorl	%esi, %esi
	movw	%ax, -96(%rbp)
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	-24(%rcx), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rcx, -448(%rbp,%rax)
	movq	-448(%rbp), %rax
	movq	$0, -440(%rbp)
	addq	-24(%rax), %r14
	movq	%r14, %rdi
.LEHB136:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE136:
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	leaq	-432(%rbp), %r13
	movq	%rdx, -432(%rbp)
	movq	-24(%rdx), %rax
	movq	%rcx, -432(%rbp,%rax)
	movq	-432(%rbp), %rax
	movq	-24(%rax), %rsi
	addq	%r13, %rsi
	movq	%rsi, %rdi
	xorl	%esi, %esi
.LEHB137:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE137:
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	pxor	%xmm0, %xmm0
	leaq	-424(%rbp), %r12
	movdqa	-768(%rbp), %xmm1
	movq	%rdx, -448(%rbp)
	movq	-24(%rdx), %rax
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	leaq	80(%rdx), %rsi
	movq	%rcx, -448(%rbp,%rax)
	movq	%rdx, -448(%rbp)
	leaq	-368(%rbp), %rdx
	movq	%rdx, %rdi
	movq	%rsi, -320(%rbp)
	movq	%rdx, -744(%rbp)
	movaps	%xmm1, -432(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	-336(%rbp), %rsi
	movq	-720(%rbp), %rdi
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rsi, -752(%rbp)
	movq	%rsi, -352(%rbp)
	movq	%r12, %rsi
	movq	%rcx, -424(%rbp)
	movl	$24, -360(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
.LEHB138:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE138:
	movl	$5, %edx
	leaq	.LC30(%rip), %rsi
	movq	%r13, %rdi
.LEHB139:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	-480(%rbp), %rax
	movq	(%rbx), %rsi
	movq	%rax, %rdi
	movq	%rax, -728(%rbp)
	call	_ZNK2v88internal6torque4Type8ToStringB5cxx11Ev@PLT
.LEHE139:
	movq	-472(%rbp), %rdx
	movq	-480(%rbp), %rsi
	movq	%r13, %rdi
.LEHB140:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE140:
	movq	-480(%rbp), %rdi
	leaq	-464(%rbp), %rax
	movq	%rax, -696(%rbp)
	cmpq	%rax, %rdi
	je	.L1329
	call	_ZdlPv@PLT
.L1329:
	movl	$57, %edx
	leaq	.LC41(%rip), %rsi
	movq	%r13, %rdi
.LEHB141:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE141:
	movl	24(%r15), %eax
	movl	16(%r15), %r8d
	leaq	.LC14(%rip), %rcx
	movl	$16, %edx
	movq	vsnprintf@GOTPCREL(%rip), %r12
	movdqu	8(%r15), %xmm2
	movq	-728(%rbp), %rdi
	movl	%eax, -656(%rbp)
	addl	$1, %r8d
	xorl	%eax, %eax
	movl	12(%r15), %r14d
	movq	%r12, %rsi
	movaps	%xmm2, -672(%rbp)
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0
	leaq	-576(%rbp), %rax
	movl	$16, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -776(%rbp)
	xorl	%eax, %eax
	leal	1(%r14), %r8d
	leaq	.LC14(%rip), %rcx
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0
	movl	-672(%rbp), %edi
.LEHB142:
	call	_ZN2v88internal6torque13SourceFileMap14PathFromV8RootB5cxx11ENS1_8SourceIdE@PLT
.LEHE142:
	leaq	-592(%rbp), %rcx
	movq	8(%rax), %r12
	movq	%rcx, -608(%rbp)
	movq	(%rax), %r14
	movq	%rcx, -704(%rbp)
	movq	%r14, %rax
	addq	%r12, %rax
	je	.L1334
	testq	%r14, %r14
	je	.L1447
.L1334:
	movq	%r12, -680(%rbp)
	cmpq	$15, %r12
	ja	.L1448
	cmpq	$1, %r12
	jne	.L1337
	movzbl	(%r14), %eax
	movb	%al, -592(%rbp)
	movq	-704(%rbp), %rax
.L1338:
	movq	%r12, -600(%rbp)
	movb	$0, (%rax,%r12)
	movabsq	$4611686018427387903, %rax
	cmpq	%rax, -600(%rbp)
	je	.L1449
	leaq	-608(%rbp), %r12
	movl	$1, %edx
	leaq	.LC42(%rip), %rsi
	movq	%r12, %rdi
.LEHB143:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.LEHE143:
	movq	-600(%rbp), %r8
	movq	-568(%rbp), %rdx
	movl	$15, %eax
	movq	-608(%rbp), %r9
	movq	%rax, %rdi
	cmpq	-704(%rbp), %r9
	cmovne	-592(%rbp), %rdi
	leaq	(%r8,%rdx), %rcx
	movq	-576(%rbp), %rsi
	cmpq	%rdi, %rcx
	jbe	.L1450
	leaq	-560(%rbp), %rdi
	cmpq	%rdi, %rsi
	cmovne	-560(%rbp), %rax
	movq	%rdi, -736(%rbp)
	cmpq	%rax, %rcx
	jbe	.L1451
.L1345:
	movq	%r12, %rdi
.LEHB144:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.LEHE144:
.L1347:
	leaq	-528(%rbp), %r14
	leaq	16(%rax), %rdx
	movq	%r14, -544(%rbp)
	movq	(%rax), %rcx
	cmpq	%rdx, %rcx
	je	.L1452
	movq	%rcx, -544(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -528(%rbp)
.L1349:
	movq	8(%rax), %rcx
	movb	$0, 16(%rax)
	movq	%rcx, -536(%rbp)
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movabsq	$4611686018427387903, %rax
	cmpq	%rax, -536(%rbp)
	je	.L1453
	leaq	-544(%rbp), %rdi
	movl	$1, %edx
	leaq	.LC42(%rip), %rsi
.LEHB145:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.LEHE145:
	leaq	-496(%rbp), %r12
	leaq	16(%rax), %rdx
	movq	%r12, -512(%rbp)
	movq	(%rax), %rcx
	cmpq	%rdx, %rcx
	je	.L1454
	movq	%rcx, -512(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -496(%rbp)
.L1352:
	movq	8(%rax), %rcx
	movq	%rcx, -504(%rbp)
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movb	$0, 16(%rax)
	movq	-512(%rbp), %r9
	movl	$15, %eax
	movq	-504(%rbp), %r8
	movq	-472(%rbp), %rdx
	movq	%rax, %rdi
	cmpq	%r12, %r9
	cmovne	-496(%rbp), %rdi
	movq	-480(%rbp), %rsi
	leaq	(%r8,%rdx), %rcx
	cmpq	%rdi, %rcx
	jbe	.L1354
	cmpq	-696(%rbp), %rsi
	cmovne	-464(%rbp), %rax
	cmpq	%rax, %rcx
	jbe	.L1455
.L1354:
	leaq	-512(%rbp), %rdi
.LEHB146:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.LEHE146:
.L1356:
	leaq	-624(%rbp), %rdx
	movq	%rdx, -640(%rbp)
	movq	(%rax), %rcx
	movq	%rdx, -728(%rbp)
	leaq	16(%rax), %rdx
	cmpq	%rdx, %rcx
	je	.L1456
	movq	%rcx, -640(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -624(%rbp)
.L1358:
	movq	8(%rax), %rcx
	movb	$0, 16(%rax)
	movq	%rcx, -632(%rbp)
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movq	-512(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L1359
	call	_ZdlPv@PLT
.L1359:
	movq	-544(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L1360
	call	_ZdlPv@PLT
.L1360:
	movq	-608(%rbp), %rdi
	cmpq	-704(%rbp), %rdi
	je	.L1361
	call	_ZdlPv@PLT
.L1361:
	movq	-576(%rbp), %rdi
	cmpq	-736(%rbp), %rdi
	je	.L1362
	call	_ZdlPv@PLT
.L1362:
	movq	-480(%rbp), %rdi
	cmpq	-696(%rbp), %rdi
	je	.L1363
	call	_ZdlPv@PLT
.L1363:
	movq	-632(%rbp), %rdx
	movq	-640(%rbp), %rsi
	movq	%r13, %rdi
.LEHB147:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE147:
	movq	-640(%rbp), %rdi
	cmpq	-728(%rbp), %rdi
	je	.L1373
	call	_ZdlPv@PLT
.L1373:
	movq	-384(%rbp), %rax
	movq	(%rbx), %r14
	leaq	-512(%rbp), %rdi
	movq	%r12, -512(%rbp)
	movq	$0, -504(%rbp)
	movb	$0, -496(%rbp)
	testq	%rax, %rax
	je	.L1374
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	jbe	.L1375
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r8
.LEHB148:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.LEHE148:
.L1376:
	movq	-696(%rbp), %rax
	movq	%rax, -480(%rbp)
	movq	-512(%rbp), %rax
	cmpq	%r12, %rax
	je	.L1457
	movq	%rax, -480(%rbp)
	movq	-496(%rbp), %rax
	movq	%rax, -464(%rbp)
.L1381:
	movq	-504(%rbp), %rax
	movl	$112, %edi
	movq	%r12, -512(%rbp)
	movq	$0, -504(%rbp)
	movq	%rax, -472(%rbp)
	movb	$0, -496(%rbp)
.LEHB149:
	call	_Znwm@PLT
.LEHE149:
	movl	$0, 8(%rax)
	movq	%rax, %r13
	leaq	32(%rax), %rax
	movq	$0, -16(%rax)
	movl	$0, (%rax)
	movq	$0, 8(%rax)
	movq	%rax, 48(%r13)
	movq	%rax, 56(%r13)
	leaq	16+_ZTVN2v88internal6torque7TopTypeE(%rip), %rax
	movq	%rax, 0(%r13)
	leaq	88(%r13), %rax
	movq	%rax, 72(%r13)
	movq	-480(%rbp), %rax
	movq	$0, 64(%r13)
	cmpq	-696(%rbp), %rax
	je	.L1458
	movq	%rax, 72(%r13)
	movq	-464(%rbp), %rax
	movq	%rax, 88(%r13)
.L1383:
	movq	-472(%rbp), %rax
	movq	%r14, 104(%r13)
	movq	%rax, 80(%r13)
.LEHB150:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_10TypeOracleEEERPNT_5ScopeEv@PLT
.LEHE150:
	movq	(%rax), %rdi
	movq	%r13, -672(%rbp)
	movq	192(%rdi), %rsi
	cmpq	200(%rdi), %rsi
	je	.L1384
	movq	$0, -672(%rbp)
	movq	%r13, (%rsi)
	addq	$8, 192(%rdi)
.L1385:
	movq	-672(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1386
	movq	(%rdi), %rax
	call	*8(%rax)
.L1386:
	movq	%r13, (%rbx)
	movq	-512(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L1388
	call	_ZdlPv@PLT
.L1388:
	movq	.LC17(%rip), %xmm0
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-352(%rbp), %rdi
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movhps	.LC19(%rip), %xmm0
	movq	%rax, -320(%rbp)
	movaps	%xmm0, -432(%rbp)
	cmpq	-752(%rbp), %rdi
	je	.L1394
	call	_ZdlPv@PLT
.L1394:
	movq	-744(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rsi
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	-720(%rbp), %rdi
	movq	%rcx, -448(%rbp,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rdx, -432(%rbp,%rax)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rsi, -448(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	$0, -440(%rbp)
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	jmp	.L1324
	.p2align 4,,10
	.p2align 3
.L1375:
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
.LEHB151:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.LEHE151:
	jmp	.L1376
	.p2align 4,,10
	.p2align 3
.L1322:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1459
	addq	$744, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1337:
	.cfi_restore_state
	testq	%r12, %r12
	jne	.L1460
	movq	-704(%rbp), %rax
	jmp	.L1338
	.p2align 4,,10
	.p2align 3
.L1448:
	leaq	-680(%rbp), %rsi
	leaq	-608(%rbp), %rdi
	xorl	%edx, %edx
.LEHB152:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE152:
	movq	%rax, -608(%rbp)
	movq	%rax, %rdi
	movq	-680(%rbp), %rax
	movq	%rax, -592(%rbp)
.L1336:
	movq	%r12, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-680(%rbp), %r12
	movq	-608(%rbp), %rax
	jmp	.L1338
	.p2align 4,,10
	.p2align 3
.L1450:
	leaq	-560(%rbp), %rax
	movq	%rax, -736(%rbp)
	jmp	.L1345
	.p2align 4,,10
	.p2align 3
.L1454:
	movdqu	16(%rax), %xmm5
	movaps	%xmm5, -496(%rbp)
	jmp	.L1352
	.p2align 4,,10
	.p2align 3
.L1452:
	movdqu	16(%rax), %xmm4
	movaps	%xmm4, -528(%rbp)
	jmp	.L1349
	.p2align 4,,10
	.p2align 3
.L1458:
	movdqa	-464(%rbp), %xmm4
	movups	%xmm4, 88(%r13)
	jmp	.L1383
	.p2align 4,,10
	.p2align 3
.L1457:
	movdqa	-496(%rbp), %xmm7
	movaps	%xmm7, -464(%rbp)
	jmp	.L1381
	.p2align 4,,10
	.p2align 3
.L1456:
	movdqu	16(%rax), %xmm6
	movaps	%xmm6, -624(%rbp)
	jmp	.L1358
	.p2align 4,,10
	.p2align 3
.L1451:
	movq	-776(%rbp), %rdi
	movq	%r9, %rcx
	xorl	%edx, %edx
	xorl	%esi, %esi
.LEHB153:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.LEHE153:
	jmp	.L1347
	.p2align 4,,10
	.p2align 3
.L1455:
	movq	-728(%rbp), %rdi
	movq	%r9, %rcx
	xorl	%edx, %edx
	xorl	%esi, %esi
.LEHB154:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.LEHE154:
	jmp	.L1356
	.p2align 4,,10
	.p2align 3
.L1374:
	leaq	-352(%rbp), %rsi
.LEHB155:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
.LEHE155:
	jmp	.L1376
	.p2align 4,,10
	.p2align 3
.L1384:
	leaq	-672(%rbp), %rdx
	addq	$184, %rdi
.LEHB156:
	call	_ZNSt6vectorISt10unique_ptrIN2v88internal6torque4TypeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
.LEHE156:
	jmp	.L1385
.L1453:
	leaq	.LC36(%rip), %rdi
.LEHB157:
	call	_ZSt20__throw_length_errorPKc@PLT
.LEHE157:
.L1449:
	leaq	.LC36(%rip), %rdi
.LEHB158:
	call	_ZSt20__throw_length_errorPKc@PLT
.LEHE158:
.L1447:
	leaq	.LC5(%rip), %rdi
.LEHB159:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE159:
.L1460:
	movq	-704(%rbp), %rdi
	jmp	.L1336
.L1459:
	call	__stack_chk_fail@PLT
.L1406:
	endbr64
	movq	%rax, %r12
	jmp	.L1396
.L1415:
	endbr64
	movq	%rax, %rbx
	jmp	.L1365
.L1419:
	endbr64
	movq	%rax, %rbx
	jmp	.L1389
.L1417:
	endbr64
	movq	%rax, %rbx
	jmp	.L1391
.L1413:
	endbr64
	movq	%rax, %r12
	jmp	.L1369
.L1414:
	endbr64
	movq	%rax, %r12
	jmp	.L1367
.L1405:
	endbr64
	movq	%rax, %r12
	jmp	.L1333
.L1409:
	endbr64
	movq	%rax, %rbx
	jmp	.L1328
.L1418:
	endbr64
	movq	%rax, %rbx
	jmp	.L1392
.L1411:
	endbr64
	movq	%rax, %r12
	jmp	.L1331
.L1410:
	endbr64
	movq	%rax, %rbx
	jmp	.L1326
.L1408:
	endbr64
	movq	%rax, %rbx
	jmp	.L1327
.L1407:
	endbr64
	movq	%rax, %rbx
	jmp	.L1391
.L1416:
	endbr64
	movq	%rax, %r12
	jmp	.L1342
.L1412:
	endbr64
	movq	%rax, %r12
	leaq	-560(%rbp), %rax
	movq	%rax, -736(%rbp)
	jmp	.L1344
	.section	.gcc_except_table._ZNK2v88internal6torque15InstructionBase24InvalidateTransientTypesEPNS1_5StackIPKNS1_4TypeEEE,"a",@progbits
.LLSDA6887:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6887-.LLSDACSB6887
.LLSDACSB6887:
	.uleb128 .LEHB135-.LFB6887
	.uleb128 .LEHE135-.LEHB135
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB136-.LFB6887
	.uleb128 .LEHE136-.LEHB136
	.uleb128 .L1408-.LFB6887
	.uleb128 0
	.uleb128 .LEHB137-.LFB6887
	.uleb128 .LEHE137-.LEHB137
	.uleb128 .L1410-.LFB6887
	.uleb128 0
	.uleb128 .LEHB138-.LFB6887
	.uleb128 .LEHE138-.LEHB138
	.uleb128 .L1409-.LFB6887
	.uleb128 0
	.uleb128 .LEHB139-.LFB6887
	.uleb128 .LEHE139-.LEHB139
	.uleb128 .L1405-.LFB6887
	.uleb128 0
	.uleb128 .LEHB140-.LFB6887
	.uleb128 .LEHE140-.LEHB140
	.uleb128 .L1411-.LFB6887
	.uleb128 0
	.uleb128 .LEHB141-.LFB6887
	.uleb128 .LEHE141-.LEHB141
	.uleb128 .L1405-.LFB6887
	.uleb128 0
	.uleb128 .LEHB142-.LFB6887
	.uleb128 .LEHE142-.LEHB142
	.uleb128 .L1412-.LFB6887
	.uleb128 0
	.uleb128 .LEHB143-.LFB6887
	.uleb128 .LEHE143-.LEHB143
	.uleb128 .L1416-.LFB6887
	.uleb128 0
	.uleb128 .LEHB144-.LFB6887
	.uleb128 .LEHE144-.LEHB144
	.uleb128 .L1413-.LFB6887
	.uleb128 0
	.uleb128 .LEHB145-.LFB6887
	.uleb128 .LEHE145-.LEHB145
	.uleb128 .L1414-.LFB6887
	.uleb128 0
	.uleb128 .LEHB146-.LFB6887
	.uleb128 .LEHE146-.LEHB146
	.uleb128 .L1415-.LFB6887
	.uleb128 0
	.uleb128 .LEHB147-.LFB6887
	.uleb128 .LEHE147-.LEHB147
	.uleb128 .L1406-.LFB6887
	.uleb128 0
	.uleb128 .LEHB148-.LFB6887
	.uleb128 .LEHE148-.LEHB148
	.uleb128 .L1417-.LFB6887
	.uleb128 0
	.uleb128 .LEHB149-.LFB6887
	.uleb128 .LEHE149-.LEHB149
	.uleb128 .L1419-.LFB6887
	.uleb128 0
	.uleb128 .LEHB150-.LFB6887
	.uleb128 .LEHE150-.LEHB150
	.uleb128 .L1407-.LFB6887
	.uleb128 0
	.uleb128 .LEHB151-.LFB6887
	.uleb128 .LEHE151-.LEHB151
	.uleb128 .L1417-.LFB6887
	.uleb128 0
	.uleb128 .LEHB152-.LFB6887
	.uleb128 .LEHE152-.LEHB152
	.uleb128 .L1412-.LFB6887
	.uleb128 0
	.uleb128 .LEHB153-.LFB6887
	.uleb128 .LEHE153-.LEHB153
	.uleb128 .L1413-.LFB6887
	.uleb128 0
	.uleb128 .LEHB154-.LFB6887
	.uleb128 .LEHE154-.LEHB154
	.uleb128 .L1415-.LFB6887
	.uleb128 0
	.uleb128 .LEHB155-.LFB6887
	.uleb128 .LEHE155-.LEHB155
	.uleb128 .L1417-.LFB6887
	.uleb128 0
	.uleb128 .LEHB156-.LFB6887
	.uleb128 .LEHE156-.LEHB156
	.uleb128 .L1418-.LFB6887
	.uleb128 0
	.uleb128 .LEHB157-.LFB6887
	.uleb128 .LEHE157-.LEHB157
	.uleb128 .L1414-.LFB6887
	.uleb128 0
	.uleb128 .LEHB158-.LFB6887
	.uleb128 .LEHE158-.LEHB158
	.uleb128 .L1416-.LFB6887
	.uleb128 0
	.uleb128 .LEHB159-.LFB6887
	.uleb128 .LEHE159-.LEHB159
	.uleb128 .L1412-.LFB6887
	.uleb128 0
.LLSDACSE6887:
	.section	.text._ZNK2v88internal6torque15InstructionBase24InvalidateTransientTypesEPNS1_5StackIPKNS1_4TypeEEE
	.cfi_endproc
	.section	.text.unlikely._ZNK2v88internal6torque15InstructionBase24InvalidateTransientTypesEPNS1_5StackIPKNS1_4TypeEEE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6887
	.type	_ZNK2v88internal6torque15InstructionBase24InvalidateTransientTypesEPNS1_5StackIPKNS1_4TypeEEE.cold, @function
_ZNK2v88internal6torque15InstructionBase24InvalidateTransientTypesEPNS1_5StackIPKNS1_4TypeEEE.cold:
.LFSB6887:
.L1396:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-640(%rbp), %rdi
	cmpq	-728(%rbp), %rdi
	je	.L1333
.L1444:
	call	_ZdlPv@PLT
.L1333:
	movq	-784(%rbp), %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r12, %rdi
.LEHB160:
	call	_Unwind_Resume@PLT
.L1365:
	movq	-512(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L1366
	call	_ZdlPv@PLT
.L1366:
	movq	%rbx, %r12
.L1367:
	movq	-544(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L1369
	call	_ZdlPv@PLT
.L1369:
	movq	-608(%rbp), %rdi
	cmpq	-704(%rbp), %rdi
	je	.L1344
	call	_ZdlPv@PLT
.L1344:
	movq	-576(%rbp), %rdi
	cmpq	-736(%rbp), %rdi
	je	.L1371
	call	_ZdlPv@PLT
.L1371:
	movq	-480(%rbp), %rdi
	cmpq	-696(%rbp), %rdi
	jne	.L1444
	jmp	.L1333
.L1389:
	movq	-480(%rbp), %rdi
	cmpq	-696(%rbp), %rdi
	je	.L1391
	call	_ZdlPv@PLT
	jmp	.L1391
.L1328:
	movq	%r12, %rdi
	call	_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED1Ev
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -432(%rbp,%rax)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	movq	$0, -440(%rbp)
.L1327:
	movq	-720(%rbp), %rdi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	%rbx, %rdi
	call	_Unwind_Resume@PLT
.LEHE160:
.L1392:
	movq	-672(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1391
	movq	(%rdi), %rax
	call	*8(%rax)
.L1391:
	movq	-512(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L1398
	call	_ZdlPv@PLT
.L1398:
	movq	%rbx, %r12
	jmp	.L1333
.L1331:
	movq	-480(%rbp), %rdi
	leaq	-464(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L1444
	jmp	.L1333
.L1326:
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	movq	$0, -440(%rbp)
	jmp	.L1327
.L1342:
	movq	-608(%rbp), %rdi
	cmpq	-704(%rbp), %rdi
	je	.L1343
	call	_ZdlPv@PLT
.L1343:
	leaq	-560(%rbp), %rax
	movq	%rax, -736(%rbp)
	jmp	.L1344
	.cfi_endproc
.LFE6887:
	.section	.gcc_except_table._ZNK2v88internal6torque15InstructionBase24InvalidateTransientTypesEPNS1_5StackIPKNS1_4TypeEEE
.LLSDAC6887:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6887-.LLSDACSBC6887
.LLSDACSBC6887:
	.uleb128 .LEHB160-.LCOLDB43
	.uleb128 .LEHE160-.LEHB160
	.uleb128 0
	.uleb128 0
.LLSDACSEC6887:
	.section	.text.unlikely._ZNK2v88internal6torque15InstructionBase24InvalidateTransientTypesEPNS1_5StackIPKNS1_4TypeEEE
	.section	.text._ZNK2v88internal6torque15InstructionBase24InvalidateTransientTypesEPNS1_5StackIPKNS1_4TypeEEE
	.size	_ZNK2v88internal6torque15InstructionBase24InvalidateTransientTypesEPNS1_5StackIPKNS1_4TypeEEE, .-_ZNK2v88internal6torque15InstructionBase24InvalidateTransientTypesEPNS1_5StackIPKNS1_4TypeEEE
	.section	.text.unlikely._ZNK2v88internal6torque15InstructionBase24InvalidateTransientTypesEPNS1_5StackIPKNS1_4TypeEEE
	.size	_ZNK2v88internal6torque15InstructionBase24InvalidateTransientTypesEPNS1_5StackIPKNS1_4TypeEEE.cold, .-_ZNK2v88internal6torque15InstructionBase24InvalidateTransientTypesEPNS1_5StackIPKNS1_4TypeEEE.cold
.LCOLDE43:
	.section	.text._ZNK2v88internal6torque15InstructionBase24InvalidateTransientTypesEPNS1_5StackIPKNS1_4TypeEEE
.LHOTE43:
	.section	.rodata._ZNK2v88internal6torque24CallIntrinsicInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE.str1.1,"aMS",@progbits,1
.LC44:
	.string	" but found type "
.LC45:
	.string	": expected type "
.LC46:
	.string	"parameter "
	.section	.text.unlikely._ZNK2v88internal6torque24CallIntrinsicInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE,"ax",@progbits
	.align 2
.LCOLDB47:
	.section	.text._ZNK2v88internal6torque24CallIntrinsicInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE,"ax",@progbits
.LHOTB47:
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal6torque24CallIntrinsicInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.type	_ZNK2v88internal6torque24CallIntrinsicInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE, @function
_ZNK2v88internal6torque24CallIntrinsicInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE:
.LFB6888:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6888
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	leaq	-112(%rbp), %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%r12), %rax
	leaq	256(%rax), %rsi
.LEHB161:
	call	_ZN2v88internal6torque19LowerParameterTypesERKNS1_14ParameterTypesEm@PLT
.LEHE161:
	movq	-104(%rbp), %rdx
	movq	%rdx, %rax
	subq	-112(%rbp), %rax
	sarq	$3, %rax
	subq	$1, %rax
	movq	%rax, -80(%rbp)
	js	.L1466
	movq	8(%rbx), %rsi
	subq	$8, %rdx
	leaq	-8(%rsi), %rcx
	.p2align 4,,10
	.p2align 3
.L1467:
	movq	(%rcx), %r9
	movq	%rcx, 8(%rbx)
	movq	(%rdx), %r10
	movq	%rdx, -104(%rbp)
	cmpq	%r9, %r10
	jne	.L1510
	subq	$1, %rax
	subq	$8, %rdx
	subq	$8, %rcx
	movq	%rax, -80(%rbp)
	cmpq	$-1, %rax
	jne	.L1467
.L1466:
	movq	32(%r12), %rax
	cmpb	$0, 328(%rax)
	jne	.L1463
.L1464:
	movq	296(%rax), %rsi
	leaq	-80(%rbp), %rdi
.LEHB162:
	call	_ZN2v88internal6torque9LowerTypeEPKNS1_4TypeE@PLT
.LEHE162:
	movq	-80(%rbp), %r12
	movq	-72(%rbp), %r14
	cmpq	%r14, %r12
	je	.L1468
	movq	8(%rbx), %rcx
	jmp	.L1477
	.p2align 4,,10
	.p2align 3
.L1512:
	movq	%r13, (%rcx)
	movq	8(%rbx), %rax
	addq	$8, %r12
	leaq	8(%rax), %rcx
	movq	%rcx, 8(%rbx)
	cmpq	%r12, %r14
	je	.L1511
.L1477:
	movq	(%r12), %r13
	cmpq	%rcx, 16(%rbx)
	jne	.L1512
	movabsq	$1152921504606846975, %rsi
	movq	(%rbx), %rax
	subq	%rax, %rcx
	movq	%rax, -120(%rbp)
	movq	%rcx, %rax
	movq	%rcx, %r15
	sarq	$3, %rax
	cmpq	%rsi, %rax
	je	.L1513
	testq	%rax, %rax
	je	.L1485
	movabsq	$9223372036854775800, %rsi
	leaq	(%rax,%rax), %rdx
	movq	%rsi, -128(%rbp)
	cmpq	%rdx, %rax
	jbe	.L1514
.L1472:
	movq	-128(%rbp), %rdi
.LEHB163:
	call	_Znwm@PLT
.LEHE163:
	movq	-128(%rbp), %r9
	movq	%rax, %r8
	movq	%r13, (%r8,%r15)
	leaq	8(%r8,%r15), %rcx
	addq	%rax, %r9
	testq	%r15, %r15
	jg	.L1515
.L1474:
	cmpq	$0, -120(%rbp)
	jne	.L1475
.L1476:
	movq	%r8, %xmm0
	movq	%rcx, %xmm1
	addq	$8, %r12
	movq	%r9, 16(%rbx)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rbx)
	cmpq	%r12, %r14
	jne	.L1477
	.p2align 4,,10
	.p2align 3
.L1511:
	movq	-80(%rbp), %r14
.L1468:
	testq	%r14, %r14
	je	.L1478
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1478:
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1461
	call	_ZdlPv@PLT
.L1461:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1516
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1514:
	.cfi_restore_state
	testq	%rdx, %rdx
	jne	.L1517
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	%r13, (%r8,%r15)
	leaq	8(%r8,%r15), %rcx
	testq	%r15, %r15
	jle	.L1474
.L1515:
	movq	-120(%rbp), %rsi
	movq	%r8, %rdi
	movq	%r15, %rdx
	movq	%r9, -128(%rbp)
	movq	%rcx, -136(%rbp)
	call	memmove@PLT
	movq	-128(%rbp), %r9
	movq	-136(%rbp), %rcx
	movq	%rax, %r8
.L1475:
	movq	-120(%rbp), %rdi
	movq	%r8, -144(%rbp)
	movq	%rcx, -136(%rbp)
	movq	%r9, -128(%rbp)
	call	_ZdlPv@PLT
	movq	-144(%rbp), %r8
	movq	-136(%rbp), %rcx
	movq	-128(%rbp), %r9
	jmp	.L1476
	.p2align 4,,10
	.p2align 3
.L1463:
	movq	%rbx, %rsi
	movq	%r12, %rdi
.LEHB164:
	call	_ZNK2v88internal6torque15InstructionBase24InvalidateTransientTypesEPNS1_5StackIPKNS1_4TypeEEE
	movq	32(%r12), %rax
	jmp	.L1464
	.p2align 4,,10
	.p2align 3
.L1485:
	movq	$8, -128(%rbp)
	jmp	.L1472
.L1510:
	leaq	-80(%rbp), %rsi
	leaq	.LC44(%rip), %r8
	movq	%r10, %rcx
	leaq	.LC45(%rip), %rdx
	leaq	.LC46(%rip), %rdi
	call	_ZN2v88internal6torque11ReportErrorIJRA11_KcRlRA17_S3_RKNS1_4TypeES8_SB_EEEvDpOT_
.LEHE164:
.L1516:
	call	__stack_chk_fail@PLT
.L1517:
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	cmova	%rax, %rdx
	leaq	0(,%rdx,8), %rax
	movq	%rax, -128(%rbp)
	jmp	.L1472
.L1513:
	leaq	.LC2(%rip), %rdi
.LEHB165:
	call	_ZSt20__throw_length_errorPKc@PLT
.LEHE165:
.L1489:
	endbr64
	movq	%rax, %r12
	jmp	.L1480
.L1488:
	endbr64
	movq	%rax, %r12
	jmp	.L1482
	.section	.gcc_except_table._ZNK2v88internal6torque24CallIntrinsicInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE,"a",@progbits
.LLSDA6888:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6888-.LLSDACSB6888
.LLSDACSB6888:
	.uleb128 .LEHB161-.LFB6888
	.uleb128 .LEHE161-.LEHB161
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB162-.LFB6888
	.uleb128 .LEHE162-.LEHB162
	.uleb128 .L1488-.LFB6888
	.uleb128 0
	.uleb128 .LEHB163-.LFB6888
	.uleb128 .LEHE163-.LEHB163
	.uleb128 .L1489-.LFB6888
	.uleb128 0
	.uleb128 .LEHB164-.LFB6888
	.uleb128 .LEHE164-.LEHB164
	.uleb128 .L1488-.LFB6888
	.uleb128 0
	.uleb128 .LEHB165-.LFB6888
	.uleb128 .LEHE165-.LEHB165
	.uleb128 .L1489-.LFB6888
	.uleb128 0
.LLSDACSE6888:
	.section	.text._ZNK2v88internal6torque24CallIntrinsicInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.cfi_endproc
	.section	.text.unlikely._ZNK2v88internal6torque24CallIntrinsicInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6888
	.type	_ZNK2v88internal6torque24CallIntrinsicInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE.cold, @function
_ZNK2v88internal6torque24CallIntrinsicInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE.cold:
.LFSB6888:
.L1480:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1482
	call	_ZdlPv@PLT
.L1482:
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1483
	call	_ZdlPv@PLT
.L1483:
	movq	%r12, %rdi
.LEHB166:
	call	_Unwind_Resume@PLT
.LEHE166:
	.cfi_endproc
.LFE6888:
	.section	.gcc_except_table._ZNK2v88internal6torque24CallIntrinsicInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
.LLSDAC6888:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6888-.LLSDACSBC6888
.LLSDACSBC6888:
	.uleb128 .LEHB166-.LCOLDB47
	.uleb128 .LEHE166-.LEHB166
	.uleb128 0
	.uleb128 0
.LLSDACSEC6888:
	.section	.text.unlikely._ZNK2v88internal6torque24CallIntrinsicInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.section	.text._ZNK2v88internal6torque24CallIntrinsicInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.size	_ZNK2v88internal6torque24CallIntrinsicInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE, .-_ZNK2v88internal6torque24CallIntrinsicInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.section	.text.unlikely._ZNK2v88internal6torque24CallIntrinsicInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.size	_ZNK2v88internal6torque24CallIntrinsicInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE.cold, .-_ZNK2v88internal6torque24CallIntrinsicInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE.cold
.LCOLDE47:
	.section	.text._ZNK2v88internal6torque24CallIntrinsicInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
.LHOTE47:
	.section	.text.unlikely._ZNK2v88internal6torque23CallCsaMacroInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE,"ax",@progbits
	.align 2
.LCOLDB48:
	.section	.text._ZNK2v88internal6torque23CallCsaMacroInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE,"ax",@progbits
.LHOTB48:
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal6torque23CallCsaMacroInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.type	_ZNK2v88internal6torque23CallCsaMacroInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE, @function
_ZNK2v88internal6torque23CallCsaMacroInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE:
.LFB6889:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6889
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	leaq	-160(%rbp), %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$168, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%r12), %rax
	leaq	256(%rax), %rsi
.LEHB167:
	call	_ZN2v88internal6torque19LowerParameterTypesERKNS1_14ParameterTypesEm@PLT
.LEHE167:
	movq	-152(%rbp), %rdx
	movq	%rdx, %rax
	subq	-160(%rbp), %rax
	sarq	$3, %rax
	subq	$1, %rax
	movq	%rax, -128(%rbp)
	js	.L1523
	movq	8(%rbx), %rsi
	subq	$8, %rdx
	leaq	-8(%rsi), %rcx
	.p2align 4,,10
	.p2align 3
.L1524:
	movq	(%rcx), %r9
	movq	%rcx, 8(%rbx)
	movq	(%rdx), %r10
	movq	%rdx, -152(%rbp)
	cmpq	%r9, %r10
	jne	.L1590
	subq	$1, %rax
	subq	$8, %rdx
	subq	$8, %rcx
	movq	%rax, -128(%rbp)
	cmpq	$-1, %rax
	jne	.L1524
.L1523:
	movq	32(%r12), %rax
	cmpb	$0, 328(%rax)
	jne	.L1591
.L1521:
	cmpb	$0, 64(%r12)
	leaq	-128(%rbp), %r13
	je	.L1525
	movq	8(%rbx), %rax
	movq	(%rbx), %rsi
	pxor	%xmm0, %xmm0
	movq	$0, -112(%rbp)
	movaps	%xmm0, -128(%rbp)
	movq	%rax, %r13
	subq	%rsi, %r13
	movq	%r13, %rdx
	sarq	$3, %rdx
	je	.L1592
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L1593
	movq	%r13, %rdi
.LEHB168:
	call	_Znwm@PLT
.LEHE168:
	movq	%rax, %rcx
	movq	8(%rbx), %rax
	movq	(%rbx), %rsi
	movq	%rax, %r14
	subq	%rsi, %r14
.L1527:
	movq	%rcx, %xmm0
	addq	%rcx, %r13
	punpcklqdq	%xmm0, %xmm0
	movq	%r13, -112(%rbp)
	movaps	%xmm0, -128(%rbp)
	cmpq	%rax, %rsi
	je	.L1529
	movq	%rcx, %rdi
	movq	%r14, %rdx
	call	memmove@PLT
	movq	%rax, %rcx
.L1529:
	addq	%r14, %rcx
	movq	%rcx, -120(%rbp)
.LEHB169:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_10TypeOracleEEERPNT_5ScopeEv@PLT
.LEHE169:
	leaq	-80(%rbp), %r14
	leaq	-96(%rbp), %rdi
	movb	$121, -76(%rbp)
	movq	%r14, -96(%rbp)
	movl	$1849774922, -80(%rbp)
	movq	$5, -88(%rbp)
	movb	$0, -75(%rbp)
.LEHB170:
	call	_ZN2v88internal6torque12Declarations16LookupGlobalTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
.LEHE170:
	movq	-96(%rbp), %rdi
	movq	%rax, %r13
	cmpq	%r14, %rdi
	je	.L1530
	call	_ZdlPv@PLT
.L1530:
	movq	%r13, -168(%rbp)
	leaq	-128(%rbp), %r13
	leaq	-168(%rbp), %rsi
	movq	%r13, %rdi
.LEHB171:
	call	_ZNSt6vectorIPKN2v88internal6torque4TypeESaIS5_EE12emplace_backIJS5_EEEvDpOT_
	movq	72(%r12), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal6torque5Block13SetInputTypesERKNS1_5StackIPKNS1_4TypeEEE@PLT
.LEHE171:
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1525
	call	_ZdlPv@PLT
.L1525:
	movq	32(%r12), %rax
	movq	%r13, %rdi
	movq	296(%rax), %rsi
.LEHB172:
	call	_ZN2v88internal6torque9LowerTypeEPKNS1_4TypeE@PLT
.LEHE172:
	movq	-128(%rbp), %r12
	movq	-120(%rbp), %r14
	cmpq	%r14, %r12
	je	.L1536
	movq	8(%rbx), %rcx
	jmp	.L1545
	.p2align 4,,10
	.p2align 3
.L1595:
	movq	%r13, (%rcx)
	movq	8(%rbx), %rax
	addq	$8, %r12
	leaq	8(%rax), %rcx
	movq	%rcx, 8(%rbx)
	cmpq	%r12, %r14
	je	.L1594
.L1545:
	movq	(%r12), %r13
	cmpq	%rcx, 16(%rbx)
	jne	.L1595
	movabsq	$1152921504606846975, %rsi
	movq	(%rbx), %rax
	subq	%rax, %rcx
	movq	%rax, -184(%rbp)
	movq	%rcx, %rax
	movq	%rcx, %r15
	sarq	$3, %rax
	cmpq	%rsi, %rax
	je	.L1596
	testq	%rax, %rax
	je	.L1554
	movabsq	$9223372036854775800, %rsi
	leaq	(%rax,%rax), %rdx
	movq	%rsi, -192(%rbp)
	cmpq	%rdx, %rax
	jbe	.L1597
.L1540:
	movq	-192(%rbp), %rdi
.LEHB173:
	call	_Znwm@PLT
.LEHE173:
	movq	-192(%rbp), %r9
	movq	%rax, %r8
	movq	%r13, (%r8,%r15)
	leaq	8(%r8,%r15), %rcx
	addq	%rax, %r9
	testq	%r15, %r15
	jg	.L1598
.L1542:
	cmpq	$0, -184(%rbp)
	jne	.L1543
.L1544:
	movq	%r8, %xmm0
	movq	%rcx, %xmm1
	addq	$8, %r12
	movq	%r9, 16(%rbx)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rbx)
	cmpq	%r12, %r14
	jne	.L1545
	.p2align 4,,10
	.p2align 3
.L1594:
	movq	-128(%rbp), %r14
.L1536:
	testq	%r14, %r14
	je	.L1546
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1546:
	movq	-160(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1518
	call	_ZdlPv@PLT
.L1518:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1599
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1597:
	.cfi_restore_state
	testq	%rdx, %rdx
	jne	.L1600
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	%r13, (%r8,%r15)
	leaq	8(%r8,%r15), %rcx
	testq	%r15, %r15
	jle	.L1542
.L1598:
	movq	-184(%rbp), %rsi
	movq	%r8, %rdi
	movq	%r15, %rdx
	movq	%rcx, -200(%rbp)
	movq	%r9, -192(%rbp)
	call	memmove@PLT
	movq	-192(%rbp), %r9
	movq	-200(%rbp), %rcx
	movq	%rax, %r8
.L1543:
	movq	-184(%rbp), %rdi
	movq	%rcx, -208(%rbp)
	movq	%r8, -200(%rbp)
	movq	%r9, -192(%rbp)
	call	_ZdlPv@PLT
	movq	-208(%rbp), %rcx
	movq	-200(%rbp), %r8
	movq	-192(%rbp), %r9
	jmp	.L1544
	.p2align 4,,10
	.p2align 3
.L1554:
	movq	$8, -192(%rbp)
	jmp	.L1540
	.p2align 4,,10
	.p2align 3
.L1591:
	movq	%rbx, %rsi
	movq	%r12, %rdi
.LEHB174:
	call	_ZNK2v88internal6torque15InstructionBase24InvalidateTransientTypesEPNS1_5StackIPKNS1_4TypeEEE
	jmp	.L1521
.L1592:
	movq	%r13, %r14
	xorl	%ecx, %ecx
	jmp	.L1527
.L1590:
	leaq	-128(%rbp), %rsi
	leaq	.LC44(%rip), %r8
	movq	%r10, %rcx
	leaq	.LC45(%rip), %rdx
	leaq	.LC46(%rip), %rdi
	call	_ZN2v88internal6torque11ReportErrorIJRA11_KcRlRA17_S3_RKNS1_4TypeES8_SB_EEEvDpOT_
.LEHE174:
.L1596:
	leaq	.LC2(%rip), %rdi
.LEHB175:
	call	_ZSt20__throw_length_errorPKc@PLT
.LEHE175:
.L1599:
	call	__stack_chk_fail@PLT
.L1600:
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	cmova	%rax, %rdx
	leaq	0(,%rdx,8), %rax
	movq	%rax, -192(%rbp)
	jmp	.L1540
.L1593:
.LEHB176:
	call	_ZSt17__throw_bad_allocv@PLT
.LEHE176:
.L1559:
	endbr64
	movq	%rax, %r12
	jmp	.L1550
.L1558:
	endbr64
	movq	%rax, %r12
	jmp	.L1534
.L1560:
	endbr64
	movq	%rax, %r12
	jmp	.L1532
.L1557:
	endbr64
	movq	%rax, %r12
	jmp	.L1549
	.section	.gcc_except_table._ZNK2v88internal6torque23CallCsaMacroInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE,"a",@progbits
.LLSDA6889:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6889-.LLSDACSB6889
.LLSDACSB6889:
	.uleb128 .LEHB167-.LFB6889
	.uleb128 .LEHE167-.LEHB167
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB168-.LFB6889
	.uleb128 .LEHE168-.LEHB168
	.uleb128 .L1557-.LFB6889
	.uleb128 0
	.uleb128 .LEHB169-.LFB6889
	.uleb128 .LEHE169-.LEHB169
	.uleb128 .L1558-.LFB6889
	.uleb128 0
	.uleb128 .LEHB170-.LFB6889
	.uleb128 .LEHE170-.LEHB170
	.uleb128 .L1560-.LFB6889
	.uleb128 0
	.uleb128 .LEHB171-.LFB6889
	.uleb128 .LEHE171-.LEHB171
	.uleb128 .L1558-.LFB6889
	.uleb128 0
	.uleb128 .LEHB172-.LFB6889
	.uleb128 .LEHE172-.LEHB172
	.uleb128 .L1557-.LFB6889
	.uleb128 0
	.uleb128 .LEHB173-.LFB6889
	.uleb128 .LEHE173-.LEHB173
	.uleb128 .L1559-.LFB6889
	.uleb128 0
	.uleb128 .LEHB174-.LFB6889
	.uleb128 .LEHE174-.LEHB174
	.uleb128 .L1557-.LFB6889
	.uleb128 0
	.uleb128 .LEHB175-.LFB6889
	.uleb128 .LEHE175-.LEHB175
	.uleb128 .L1559-.LFB6889
	.uleb128 0
	.uleb128 .LEHB176-.LFB6889
	.uleb128 .LEHE176-.LEHB176
	.uleb128 .L1557-.LFB6889
	.uleb128 0
.LLSDACSE6889:
	.section	.text._ZNK2v88internal6torque23CallCsaMacroInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.cfi_endproc
	.section	.text.unlikely._ZNK2v88internal6torque23CallCsaMacroInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6889
	.type	_ZNK2v88internal6torque23CallCsaMacroInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE.cold, @function
_ZNK2v88internal6torque23CallCsaMacroInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE.cold:
.LFSB6889:
.L1550:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1549
	call	_ZdlPv@PLT
.L1549:
	movq	-160(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1552
	call	_ZdlPv@PLT
.L1552:
	movq	%r12, %rdi
.LEHB177:
	call	_Unwind_Resume@PLT
.LEHE177:
.L1532:
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L1534
	call	_ZdlPv@PLT
.L1534:
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1549
	call	_ZdlPv@PLT
	jmp	.L1549
	.cfi_endproc
.LFE6889:
	.section	.gcc_except_table._ZNK2v88internal6torque23CallCsaMacroInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
.LLSDAC6889:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6889-.LLSDACSBC6889
.LLSDACSBC6889:
	.uleb128 .LEHB177-.LCOLDB48
	.uleb128 .LEHE177-.LEHB177
	.uleb128 0
	.uleb128 0
.LLSDACSEC6889:
	.section	.text.unlikely._ZNK2v88internal6torque23CallCsaMacroInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.section	.text._ZNK2v88internal6torque23CallCsaMacroInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.size	_ZNK2v88internal6torque23CallCsaMacroInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE, .-_ZNK2v88internal6torque23CallCsaMacroInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.section	.text.unlikely._ZNK2v88internal6torque23CallCsaMacroInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.size	_ZNK2v88internal6torque23CallCsaMacroInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE.cold, .-_ZNK2v88internal6torque23CallCsaMacroInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE.cold
.LCOLDE48:
	.section	.text._ZNK2v88internal6torque23CallCsaMacroInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
.LHOTE48:
	.section	.rodata._ZNK2v88internal6torque32CallCsaMacroAndBranchInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE.str1.1,"aMS",@progbits,1
.LC49:
	.string	"wrong number of labels"
.LC50:
	.string	"missing return continuation."
	.section	.rodata._ZNK2v88internal6torque32CallCsaMacroAndBranchInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE.str1.8,"aMS",@progbits,1
	.align 8
.LC51:
	.string	"unreachable return continuation."
	.section	.text.unlikely._ZNK2v88internal6torque32CallCsaMacroAndBranchInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE,"ax",@progbits
	.align 2
.LCOLDB52:
	.section	.text._ZNK2v88internal6torque32CallCsaMacroAndBranchInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE,"ax",@progbits
.LHOTB52:
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal6torque32CallCsaMacroAndBranchInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.type	_ZNK2v88internal6torque32CallCsaMacroAndBranchInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE, @function
_ZNK2v88internal6torque32CallCsaMacroAndBranchInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE:
.LFB6890:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6890
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	leaq	-224(%rbp), %rdi
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$232, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%r15), %rax
	leaq	256(%rax), %rsi
.LEHB178:
	call	_ZN2v88internal6torque19LowerParameterTypesERKNS1_14ParameterTypesEm@PLT
.LEHE178:
	movq	-216(%rbp), %rdx
	movq	%rdx, %rax
	subq	-224(%rbp), %rax
	sarq	$3, %rax
	subq	$1, %rax
	movq	%rax, -160(%rbp)
	js	.L1606
	movq	8(%r14), %rsi
	subq	$8, %rdx
	leaq	-8(%rsi), %rcx
	.p2align 4,,10
	.p2align 3
.L1607:
	movq	(%rcx), %r9
	movq	%rcx, 8(%r14)
	movq	(%rdx), %r10
	movq	%rdx, -216(%rbp)
	cmpq	%r9, %r10
	jne	.L1744
	subq	$1, %rax
	subq	$8, %rcx
	subq	$8, %rdx
	movq	%rax, -160(%rbp)
	cmpq	$-1, %rax
	jne	.L1607
.L1606:
	movq	88(%r15), %rsi
	movq	32(%r15), %rdx
	movq	80(%r15), %rdi
	movq	%rsi, %rcx
	movq	312(%rdx), %rax
	subq	304(%rdx), %rax
	subq	%rdi, %rcx
	sarq	$5, %rax
	sarq	$3, %rcx
	cmpq	%rcx, %rax
	jne	.L1745
	cmpq	%rsi, %rdi
	je	.L1610
	movq	$0, -232(%rbp)
	.p2align 4,,10
	.p2align 3
.L1629:
	movq	8(%r14), %rax
	movq	(%r14), %rsi
	pxor	%xmm2, %xmm2
	movq	$0, -176(%rbp)
	movaps	%xmm2, -192(%rbp)
	movq	%rax, %rbx
	subq	%rsi, %rbx
	movq	%rbx, %rdx
	sarq	$3, %rdx
	je	.L1746
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L1747
	movq	%rbx, %rdi
.LEHB179:
	call	_Znwm@PLT
.LEHE179:
	movq	%rax, %rcx
	movq	8(%r14), %rax
	movq	(%r14), %rsi
	movq	%rax, %r12
	subq	%rsi, %r12
.L1612:
	movq	%rcx, %xmm0
	addq	%rcx, %rbx
	punpcklqdq	%xmm0, %xmm0
	movq	%rbx, -176(%rbp)
	movaps	%xmm0, -192(%rbp)
	cmpq	%rsi, %rax
	je	.L1614
	movq	%rcx, %rdi
	movq	%r12, %rdx
	call	memmove@PLT
	movq	%rax, %rcx
.L1614:
	movq	32(%r15), %rax
	movq	-232(%rbp), %rsi
	addq	%r12, %rcx
	leaq	-160(%rbp), %rdi
	movq	%rcx, -184(%rbp)
	salq	$5, %rsi
	addq	304(%rax), %rsi
	addq	$8, %rsi
.LEHB180:
	call	_ZN2v88internal6torque19LowerParameterTypesERKSt6vectorIPKNS1_4TypeESaIS5_EE@PLT
.LEHE180:
	movq	-160(%rbp), %rbx
	movq	-152(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L1615
	movq	-184(%rbp), %rcx
	jmp	.L1624
	.p2align 4,,10
	.p2align 3
.L1749:
	movq	%rax, (%rcx)
	movq	-184(%rbp), %rax
	addq	$8, %rbx
	leaq	8(%rax), %rcx
	movq	%rcx, -184(%rbp)
	cmpq	%rbx, %r13
	je	.L1748
.L1624:
	movq	(%rbx), %rax
	movq	%rax, -240(%rbp)
	cmpq	%rcx, -176(%rbp)
	jne	.L1749
	movq	-192(%rbp), %rax
	movabsq	$1152921504606846975, %rdx
	subq	%rax, %rcx
	movq	%rax, -248(%rbp)
	movq	%rcx, %rax
	movq	%rcx, %r12
	sarq	$3, %rax
	cmpq	%rdx, %rax
	je	.L1750
	testq	%rax, %rax
	je	.L1679
	movabsq	$9223372036854775800, %rdi
	leaq	(%rax,%rax), %rdx
	movq	%rdi, -256(%rbp)
	cmpq	%rdx, %rax
	jbe	.L1751
.L1619:
	movq	-256(%rbp), %rdi
.LEHB181:
	call	_Znwm@PLT
.LEHE181:
	movq	-256(%rbp), %r9
	movq	%rax, %r8
	addq	%rax, %r9
.L1620:
	movq	-240(%rbp), %rax
	leaq	8(%r8,%r12), %rcx
	movq	%rax, (%r8,%r12)
	testq	%r12, %r12
	jg	.L1752
	cmpq	$0, -248(%rbp)
	jne	.L1622
.L1623:
	movq	%r8, %xmm0
	movq	%rcx, %xmm1
	addq	$8, %rbx
	movq	%r9, -176(%rbp)
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -192(%rbp)
	cmpq	%rbx, %r13
	jne	.L1624
	.p2align 4,,10
	.p2align 3
.L1748:
	movq	-160(%rbp), %r13
.L1615:
	testq	%r13, %r13
	je	.L1625
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1625:
	movq	80(%r15), %rax
	movq	-232(%rbp), %rbx
	leaq	-192(%rbp), %rsi
	movq	(%rax,%rbx,8), %rdi
.LEHB182:
	call	_ZN2v88internal6torque5Block13SetInputTypesERKNS1_5StackIPKNS1_4TypeEEE@PLT
.LEHE182:
	movq	-192(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1626
	call	_ZdlPv@PLT
	movq	88(%r15), %rax
	subq	80(%r15), %rax
	addq	$1, %rbx
	sarq	$3, %rax
	movq	%rbx, -232(%rbp)
	cmpq	%rax, %rbx
	jb	.L1629
.L1628:
	movq	32(%r15), %rdx
.L1610:
	cmpb	$0, 328(%rdx)
	jne	.L1753
.L1630:
	cmpb	$0, 104(%r15)
	je	.L1631
	movq	8(%r14), %rax
	movq	(%r14), %rsi
	pxor	%xmm0, %xmm0
	movq	$0, -144(%rbp)
	movaps	%xmm0, -160(%rbp)
	movq	%rax, %rbx
	subq	%rsi, %rbx
	movq	%rbx, %rdx
	sarq	$3, %rdx
	je	.L1754
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L1755
	movq	%rbx, %rdi
.LEHB183:
	call	_Znwm@PLT
.LEHE183:
	movq	%rax, %rcx
	movq	8(%r14), %rax
	movq	(%r14), %rsi
	movq	%rax, %r12
	subq	%rsi, %r12
.L1633:
	movq	%rcx, %xmm0
	addq	%rcx, %rbx
	punpcklqdq	%xmm0, %xmm0
	movq	%rbx, -144(%rbp)
	movaps	%xmm0, -160(%rbp)
	cmpq	%rsi, %rax
	je	.L1635
	movq	%rcx, %rdi
	movq	%r12, %rdx
	call	memmove@PLT
	movq	%rax, %rcx
.L1635:
	addq	%r12, %rcx
	movq	%rcx, -152(%rbp)
.LEHB184:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_10TypeOracleEEERPNT_5ScopeEv@PLT
.LEHE184:
	leaq	-112(%rbp), %rbx
	leaq	-128(%rbp), %rdi
	movb	$121, -108(%rbp)
	movq	%rbx, -128(%rbp)
	movl	$1849774922, -112(%rbp)
	movq	$5, -120(%rbp)
	movb	$0, -107(%rbp)
.LEHB185:
	call	_ZN2v88internal6torque12Declarations16LookupGlobalTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
.LEHE185:
	movq	-128(%rbp), %rdi
	movq	%rax, %r12
	cmpq	%rbx, %rdi
	je	.L1636
	call	_ZdlPv@PLT
.L1636:
	movq	%r12, -192(%rbp)
	leaq	-160(%rbp), %r12
	leaq	-192(%rbp), %rsi
	movq	%r12, %rdi
.LEHB186:
	call	_ZNSt6vectorIPKN2v88internal6torque4TypeESaIS5_EE12emplace_backIJS5_EEEvDpOT_
	movq	112(%r15), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal6torque5Block13SetInputTypesERKNS1_5StackIPKNS1_4TypeEEE@PLT
.LEHE186:
	movq	-160(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1631
	call	_ZdlPv@PLT
.L1631:
	movq	32(%r15), %rax
	movq	296(%rax), %r12
.LEHB187:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_10TypeOracleEEERPNT_5ScopeEv@PLT
.LEHE187:
	leaq	-128(%rbp), %rax
	leaq	-112(%rbp), %rbx
	movb	$114, -108(%rbp)
	movq	%rax, %rdi
	movq	%rax, -248(%rbp)
	movq	%rbx, -128(%rbp)
	movl	$1702258030, -112(%rbp)
	movq	$5, -120(%rbp)
	movb	$0, -107(%rbp)
.LEHB188:
	call	_ZN2v88internal6torque12Declarations16LookupGlobalTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
.LEHE188:
	movq	-128(%rbp), %rdi
	movq	%rax, %r13
	cmpq	%rbx, %rdi
	je	.L1642
	call	_ZdlPv@PLT
.L1642:
	cmpq	%r13, %r12
	je	.L1756
	movq	8(%r14), %rax
	movq	(%r14), %rsi
	pxor	%xmm0, %xmm0
	movq	$0, -176(%rbp)
	movaps	%xmm0, -192(%rbp)
	movq	%rax, %rbx
	subq	%rsi, %rbx
	movq	%rbx, %rdx
	sarq	$3, %rdx
	je	.L1757
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L1758
	movq	%rbx, %rdi
.LEHB189:
	call	_Znwm@PLT
.LEHE189:
	movq	%rax, %rcx
	movq	8(%r14), %rax
	movq	(%r14), %rsi
	movq	%rax, %r12
	subq	%rsi, %r12
.L1648:
	movq	%rcx, %xmm0
	addq	%rcx, %rbx
	punpcklqdq	%xmm0, %xmm0
	movq	%rbx, -176(%rbp)
	movaps	%xmm0, -192(%rbp)
	cmpq	%rax, %rsi
	je	.L1650
	movq	%rcx, %rdi
	movq	%r12, %rdx
	call	memmove@PLT
	movq	%rax, %rcx
.L1650:
	movq	32(%r15), %rax
	addq	%r12, %rcx
	leaq	-160(%rbp), %rdi
	movq	%rcx, -184(%rbp)
	movq	296(%rax), %rsi
.LEHB190:
	call	_ZN2v88internal6torque9LowerTypeEPKNS1_4TypeE@PLT
.LEHE190:
	movq	-160(%rbp), %rbx
	movq	-152(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L1651
	movq	-184(%rbp), %rcx
	jmp	.L1660
	.p2align 4,,10
	.p2align 3
.L1760:
	movq	%r12, (%rcx)
	movq	-184(%rbp), %rax
	addq	$8, %rbx
	leaq	8(%rax), %rcx
	movq	%rcx, -184(%rbp)
	cmpq	%rbx, %r13
	je	.L1759
.L1660:
	movq	(%rbx), %r12
	cmpq	%rcx, -176(%rbp)
	jne	.L1760
	movq	-192(%rbp), %rax
	movabsq	$1152921504606846975, %rdx
	subq	%rax, %rcx
	movq	%rax, -232(%rbp)
	movq	%rcx, %rax
	movq	%rcx, %r14
	sarq	$3, %rax
	cmpq	%rdx, %rax
	je	.L1761
	testq	%rax, %rax
	je	.L1682
	movabsq	$9223372036854775800, %rsi
	leaq	(%rax,%rax), %rdx
	movq	%rsi, -240(%rbp)
	cmpq	%rdx, %rax
	jbe	.L1762
.L1655:
	movq	-240(%rbp), %rdi
.LEHB191:
	call	_Znwm@PLT
.LEHE191:
	movq	-240(%rbp), %r9
	movq	%rax, %r8
	addq	%rax, %r9
.L1656:
	movq	%r12, (%r8,%r14)
	leaq	8(%r8,%r14), %rcx
	testq	%r14, %r14
	jg	.L1763
	cmpq	$0, -232(%rbp)
	jne	.L1658
.L1659:
	movq	%r8, %xmm0
	movq	%rcx, %xmm3
	addq	$8, %rbx
	movq	%r9, -176(%rbp)
	punpcklqdq	%xmm3, %xmm0
	movaps	%xmm0, -192(%rbp)
	cmpq	%rbx, %r13
	jne	.L1660
	.p2align 4,,10
	.p2align 3
.L1759:
	movq	-160(%rbp), %r13
.L1651:
	testq	%r13, %r13
	je	.L1661
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1661:
	cmpb	$0, 64(%r15)
	je	.L1764
	movq	72(%r15), %rdi
	leaq	-192(%rbp), %rsi
.LEHB192:
	call	_ZN2v88internal6torque5Block13SetInputTypesERKNS1_5StackIPKNS1_4TypeEEE@PLT
.LEHE192:
	movq	-192(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1666
	call	_ZdlPv@PLT
.L1666:
	movq	-224(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1601
	call	_ZdlPv@PLT
.L1601:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1765
	addq	$232, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1752:
	.cfi_restore_state
	movq	-248(%rbp), %rsi
	movq	%r8, %rdi
	movq	%r12, %rdx
	movq	%r9, -256(%rbp)
	movq	%rcx, -240(%rbp)
	call	memmove@PLT
	movq	-240(%rbp), %rcx
	movq	-256(%rbp), %r9
	movq	%rax, %r8
.L1622:
	movq	-248(%rbp), %rdi
	movq	%r9, -264(%rbp)
	movq	%r8, -256(%rbp)
	movq	%rcx, -240(%rbp)
	call	_ZdlPv@PLT
	movq	-264(%rbp), %r9
	movq	-256(%rbp), %r8
	movq	-240(%rbp), %rcx
	jmp	.L1623
	.p2align 4,,10
	.p2align 3
.L1751:
	testq	%rdx, %rdx
	jne	.L1766
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	jmp	.L1620
	.p2align 4,,10
	.p2align 3
.L1679:
	movq	$8, -256(%rbp)
	jmp	.L1619
	.p2align 4,,10
	.p2align 3
.L1626:
	addq	$1, -232(%rbp)
	movq	88(%r15), %rax
	movq	-232(%rbp), %rdi
	subq	80(%r15), %rax
	sarq	$3, %rax
	cmpq	%rax, %rdi
	jb	.L1629
	jmp	.L1628
	.p2align 4,,10
	.p2align 3
.L1746:
	movq	%rbx, %r12
	xorl	%ecx, %ecx
	jmp	.L1612
	.p2align 4,,10
	.p2align 3
.L1756:
	cmpb	$0, 64(%r15)
	je	.L1666
	movq	-248(%rbp), %rbx
	leaq	.LC51(%rip), %rsi
	movq	%rbx, %rdi
.LEHB193:
	call	_ZN2v88internal6torqueL7MessageIJRA23_KcEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
.LEHE193:
	movq	%rbx, %rdi
.LEHB194:
	call	_ZNK2v88internal6torque14MessageBuilder5ThrowEv@PLT
.LEHE194:
	.p2align 4,,10
	.p2align 3
.L1763:
	movq	-232(%rbp), %rsi
	movq	%r8, %rdi
	movq	%r14, %rdx
	movq	%r9, -256(%rbp)
	movq	%rcx, -240(%rbp)
	call	memmove@PLT
	movq	-240(%rbp), %rcx
	movq	-256(%rbp), %r9
	movq	%rax, %r8
.L1658:
	movq	-232(%rbp), %rdi
	movq	%r9, -264(%rbp)
	movq	%rcx, -256(%rbp)
	movq	%r8, -240(%rbp)
	call	_ZdlPv@PLT
	movq	-264(%rbp), %r9
	movq	-256(%rbp), %rcx
	movq	-240(%rbp), %r8
	jmp	.L1659
	.p2align 4,,10
	.p2align 3
.L1753:
	movq	%r14, %rsi
	movq	%r15, %rdi
.LEHB195:
	call	_ZNK2v88internal6torque15InstructionBase24InvalidateTransientTypesEPNS1_5StackIPKNS1_4TypeEEE
	jmp	.L1630
	.p2align 4,,10
	.p2align 3
.L1762:
	testq	%rdx, %rdx
	jne	.L1767
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	jmp	.L1656
	.p2align 4,,10
	.p2align 3
.L1682:
	movq	$8, -240(%rbp)
	jmp	.L1655
.L1757:
	movq	%rbx, %r12
	xorl	%ecx, %ecx
	jmp	.L1648
.L1754:
	movq	%rbx, %r12
	xorl	%ecx, %ecx
	jmp	.L1633
.L1744:
	leaq	-160(%rbp), %rsi
	leaq	.LC44(%rip), %r8
	movq	%r10, %rcx
	leaq	.LC45(%rip), %rdx
	leaq	.LC46(%rip), %rdi
	call	_ZN2v88internal6torque11ReportErrorIJRA11_KcRlRA17_S3_RKNS1_4TypeES8_SB_EEEvDpOT_
.LEHE195:
.L1750:
	leaq	.LC2(%rip), %rdi
.LEHB196:
	call	_ZSt20__throw_length_errorPKc@PLT
.LEHE196:
.L1747:
.LEHB197:
	call	_ZSt17__throw_bad_allocv@PLT
.LEHE197:
.L1761:
	leaq	.LC2(%rip), %rdi
.LEHB198:
	call	_ZSt20__throw_length_errorPKc@PLT
.LEHE198:
.L1745:
	leaq	-128(%rbp), %rbx
	leaq	.LC49(%rip), %rsi
	movq	%rbx, %rdi
	movq	%rbx, -248(%rbp)
.LEHB199:
	call	_ZN2v88internal6torqueL7MessageIJRA23_KcEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
.LEHE199:
	movq	%rbx, %rdi
.LEHB200:
	call	_ZNK2v88internal6torque14MessageBuilder5ThrowEv@PLT
.LEHE200:
.L1765:
	call	__stack_chk_fail@PLT
.L1767:
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	cmova	%rax, %rdx
	leaq	0(,%rdx,8), %rax
	movq	%rax, -240(%rbp)
	jmp	.L1655
.L1758:
.LEHB201:
	call	_ZSt17__throw_bad_allocv@PLT
.L1766:
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	cmova	%rax, %rdx
	leaq	0(,%rdx,8), %rax
	movq	%rax, -256(%rbp)
	jmp	.L1619
.L1755:
	call	_ZSt17__throw_bad_allocv@PLT
.LEHE201:
.L1764:
	movq	-248(%rbp), %rbx
	leaq	.LC50(%rip), %rsi
	movq	%rbx, %rdi
.LEHB202:
	call	_ZN2v88internal6torqueL7MessageIJRA23_KcEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
.LEHE202:
	movq	%rbx, %rdi
.LEHB203:
	call	_ZNK2v88internal6torque14MessageBuilder5ThrowEv@PLT
.LEHE203:
.L1688:
	endbr64
	movq	%rax, %r12
	jmp	.L1640
.L1695:
	endbr64
	movq	%rax, %r12
	jmp	.L1667
.L1694:
	endbr64
	movq	%rax, %r12
	jmp	.L1663
.L1685:
	endbr64
	movq	%rax, %r12
	jmp	.L1609
.L1691:
	endbr64
	movq	%rax, %r12
	jmp	.L1608
.L1690:
	endbr64
	movq	%rax, %r12
	jmp	.L1674
.L1692:
	endbr64
	movq	%rax, %r12
	jmp	.L1638
.L1689:
	endbr64
	movq	%rax, %r12
	jmp	.L1664
.L1693:
	endbr64
	movq	%rax, %r12
	jmp	.L1645
.L1686:
	endbr64
	movq	%rax, %r12
	jmp	.L1671
.L1687:
	endbr64
	movq	%rax, %r12
	jmp	.L1669
	.section	.gcc_except_table._ZNK2v88internal6torque32CallCsaMacroAndBranchInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE,"a",@progbits
.LLSDA6890:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6890-.LLSDACSB6890
.LLSDACSB6890:
	.uleb128 .LEHB178-.LFB6890
	.uleb128 .LEHE178-.LEHB178
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB179-.LFB6890
	.uleb128 .LEHE179-.LEHB179
	.uleb128 .L1685-.LFB6890
	.uleb128 0
	.uleb128 .LEHB180-.LFB6890
	.uleb128 .LEHE180-.LEHB180
	.uleb128 .L1686-.LFB6890
	.uleb128 0
	.uleb128 .LEHB181-.LFB6890
	.uleb128 .LEHE181-.LEHB181
	.uleb128 .L1687-.LFB6890
	.uleb128 0
	.uleb128 .LEHB182-.LFB6890
	.uleb128 .LEHE182-.LEHB182
	.uleb128 .L1686-.LFB6890
	.uleb128 0
	.uleb128 .LEHB183-.LFB6890
	.uleb128 .LEHE183-.LEHB183
	.uleb128 .L1685-.LFB6890
	.uleb128 0
	.uleb128 .LEHB184-.LFB6890
	.uleb128 .LEHE184-.LEHB184
	.uleb128 .L1688-.LFB6890
	.uleb128 0
	.uleb128 .LEHB185-.LFB6890
	.uleb128 .LEHE185-.LEHB185
	.uleb128 .L1692-.LFB6890
	.uleb128 0
	.uleb128 .LEHB186-.LFB6890
	.uleb128 .LEHE186-.LEHB186
	.uleb128 .L1688-.LFB6890
	.uleb128 0
	.uleb128 .LEHB187-.LFB6890
	.uleb128 .LEHE187-.LEHB187
	.uleb128 .L1685-.LFB6890
	.uleb128 0
	.uleb128 .LEHB188-.LFB6890
	.uleb128 .LEHE188-.LEHB188
	.uleb128 .L1693-.LFB6890
	.uleb128 0
	.uleb128 .LEHB189-.LFB6890
	.uleb128 .LEHE189-.LEHB189
	.uleb128 .L1685-.LFB6890
	.uleb128 0
	.uleb128 .LEHB190-.LFB6890
	.uleb128 .LEHE190-.LEHB190
	.uleb128 .L1689-.LFB6890
	.uleb128 0
	.uleb128 .LEHB191-.LFB6890
	.uleb128 .LEHE191-.LEHB191
	.uleb128 .L1690-.LFB6890
	.uleb128 0
	.uleb128 .LEHB192-.LFB6890
	.uleb128 .LEHE192-.LEHB192
	.uleb128 .L1689-.LFB6890
	.uleb128 0
	.uleb128 .LEHB193-.LFB6890
	.uleb128 .LEHE193-.LEHB193
	.uleb128 .L1685-.LFB6890
	.uleb128 0
	.uleb128 .LEHB194-.LFB6890
	.uleb128 .LEHE194-.LEHB194
	.uleb128 .L1695-.LFB6890
	.uleb128 0
	.uleb128 .LEHB195-.LFB6890
	.uleb128 .LEHE195-.LEHB195
	.uleb128 .L1685-.LFB6890
	.uleb128 0
	.uleb128 .LEHB196-.LFB6890
	.uleb128 .LEHE196-.LEHB196
	.uleb128 .L1687-.LFB6890
	.uleb128 0
	.uleb128 .LEHB197-.LFB6890
	.uleb128 .LEHE197-.LEHB197
	.uleb128 .L1685-.LFB6890
	.uleb128 0
	.uleb128 .LEHB198-.LFB6890
	.uleb128 .LEHE198-.LEHB198
	.uleb128 .L1690-.LFB6890
	.uleb128 0
	.uleb128 .LEHB199-.LFB6890
	.uleb128 .LEHE199-.LEHB199
	.uleb128 .L1685-.LFB6890
	.uleb128 0
	.uleb128 .LEHB200-.LFB6890
	.uleb128 .LEHE200-.LEHB200
	.uleb128 .L1691-.LFB6890
	.uleb128 0
	.uleb128 .LEHB201-.LFB6890
	.uleb128 .LEHE201-.LEHB201
	.uleb128 .L1685-.LFB6890
	.uleb128 0
	.uleb128 .LEHB202-.LFB6890
	.uleb128 .LEHE202-.LEHB202
	.uleb128 .L1689-.LFB6890
	.uleb128 0
	.uleb128 .LEHB203-.LFB6890
	.uleb128 .LEHE203-.LEHB203
	.uleb128 .L1694-.LFB6890
	.uleb128 0
.LLSDACSE6890:
	.section	.text._ZNK2v88internal6torque32CallCsaMacroAndBranchInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.cfi_endproc
	.section	.text.unlikely._ZNK2v88internal6torque32CallCsaMacroAndBranchInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6890
	.type	_ZNK2v88internal6torque32CallCsaMacroAndBranchInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE.cold, @function
_ZNK2v88internal6torque32CallCsaMacroAndBranchInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE.cold:
.LFSB6890:
.L1638:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-128(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1640
	call	_ZdlPv@PLT
.L1640:
	movq	-160(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1609
	call	_ZdlPv@PLT
	jmp	.L1609
.L1667:
	movq	-248(%rbp), %rdi
	call	_ZN2v88internal6torque14MessageBuilderD1Ev
.L1609:
	movq	-224(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1677
	call	_ZdlPv@PLT
.L1677:
	movq	%r12, %rdi
.LEHB204:
	call	_Unwind_Resume@PLT
.LEHE204:
.L1663:
	movq	-248(%rbp), %rdi
	call	_ZN2v88internal6torque14MessageBuilderD1Ev
.L1664:
	movq	-192(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1609
	call	_ZdlPv@PLT
	jmp	.L1609
.L1608:
	movq	-248(%rbp), %rdi
	call	_ZN2v88internal6torque14MessageBuilderD1Ev
	jmp	.L1609
.L1674:
	movq	-160(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1664
	call	_ZdlPv@PLT
	jmp	.L1664
.L1645:
	movq	-128(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1609
	call	_ZdlPv@PLT
	jmp	.L1609
.L1669:
	movq	-160(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1671
	call	_ZdlPv@PLT
.L1671:
	movq	-192(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1609
	call	_ZdlPv@PLT
	jmp	.L1609
	.cfi_endproc
.LFE6890:
	.section	.gcc_except_table._ZNK2v88internal6torque32CallCsaMacroAndBranchInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
.LLSDAC6890:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6890-.LLSDACSBC6890
.LLSDACSBC6890:
	.uleb128 .LEHB204-.LCOLDB52
	.uleb128 .LEHE204-.LEHB204
	.uleb128 0
	.uleb128 0
.LLSDACSEC6890:
	.section	.text.unlikely._ZNK2v88internal6torque32CallCsaMacroAndBranchInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.section	.text._ZNK2v88internal6torque32CallCsaMacroAndBranchInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.size	_ZNK2v88internal6torque32CallCsaMacroAndBranchInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE, .-_ZNK2v88internal6torque32CallCsaMacroAndBranchInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.section	.text.unlikely._ZNK2v88internal6torque32CallCsaMacroAndBranchInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.size	_ZNK2v88internal6torque32CallCsaMacroAndBranchInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE.cold, .-_ZNK2v88internal6torque32CallCsaMacroAndBranchInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE.cold
.LCOLDE52:
	.section	.text._ZNK2v88internal6torque32CallCsaMacroAndBranchInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
.LHOTE52:
	.section	.rodata._ZNSt6vectorIPKN2v88internal6torque4TypeESaIS5_EE17_M_default_appendEm.str1.1,"aMS",@progbits,1
.LC53:
	.string	"vector::_M_default_append"
	.section	.text._ZNSt6vectorIPKN2v88internal6torque4TypeESaIS5_EE17_M_default_appendEm,"axG",@progbits,_ZNSt6vectorIPKN2v88internal6torque4TypeESaIS5_EE17_M_default_appendEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPKN2v88internal6torque4TypeESaIS5_EE17_M_default_appendEm
	.type	_ZNSt6vectorIPKN2v88internal6torque4TypeESaIS5_EE17_M_default_appendEm, @function
_ZNSt6vectorIPKN2v88internal6torque4TypeESaIS5_EE17_M_default_appendEm:
.LFB10127:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L1787
	movabsq	$1152921504606846975, %rdx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	8(%rdi), %rcx
	movq	16(%r12), %rax
	movq	%rcx, %rsi
	subq	(%rdi), %rsi
	subq	%rcx, %rax
	movq	%rdx, %rdi
	movq	%rsi, %r13
	sarq	$3, %rax
	sarq	$3, %r13
	subq	%r13, %rdi
	cmpq	%rax, %rbx
	ja	.L1770
	salq	$3, %rbx
	movq	%rcx, %rdi
	xorl	%esi, %esi
	movq	%rbx, %rdx
	call	memset@PLT
	movq	%rax, %rcx
	addq	%rbx, %rcx
	movq	%rcx, 8(%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1787:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L1770:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	cmpq	%rbx, %rdi
	jb	.L1790
	cmpq	%r13, %rbx
	movq	%r13, %r14
	movq	%rsi, -56(%rbp)
	cmovnb	%rbx, %r14
	addq	%r13, %r14
	cmpq	%rdx, %r14
	cmova	%rdx, %r14
	salq	$3, %r14
	movq	%r14, %rdi
	call	_Znwm@PLT
	movq	-56(%rbp), %rsi
	leaq	0(,%rbx,8), %rdx
	movq	%rax, %r15
	leaq	(%rax,%rsi), %rdi
	xorl	%esi, %esi
	call	memset@PLT
	movq	(%r12), %r8
	movq	8(%r12), %rdx
	subq	%r8, %rdx
	testq	%rdx, %rdx
	jg	.L1791
	testq	%r8, %r8
	jne	.L1774
.L1775:
	addq	%r13, %rbx
	addq	%r15, %r14
	movq	%r15, (%r12)
	leaq	(%r15,%rbx,8), %rax
	movq	%r14, 16(%r12)
	movq	%rax, 8(%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1791:
	.cfi_restore_state
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r8, -56(%rbp)
	call	memmove@PLT
	movq	-56(%rbp), %r8
.L1774:
	movq	%r8, %rdi
	call	_ZdlPv@PLT
	jmp	.L1775
.L1790:
	leaq	.LC53(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE10127:
	.size	_ZNSt6vectorIPKN2v88internal6torque4TypeESaIS5_EE17_M_default_appendEm, .-_ZNSt6vectorIPKN2v88internal6torque4TypeESaIS5_EE17_M_default_appendEm
	.section	.rodata._ZNK2v88internal6torque22CallRuntimeInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE.str1.1,"aMS",@progbits,1
.LC54:
	.string	"vector::reserve"
.LC55:
	.string	"wrong argument types"
	.section	.text.unlikely._ZNK2v88internal6torque22CallRuntimeInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE,"ax",@progbits
	.align 2
.LCOLDB56:
	.section	.text._ZNK2v88internal6torque22CallRuntimeInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE,"ax",@progbits
.LHOTB56:
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal6torque22CallRuntimeInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.type	_ZNK2v88internal6torque22CallRuntimeInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE, @function
_ZNK2v88internal6torque22CallRuntimeInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE:
.LFB6893:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6893
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	40(%rdi), %rax
	movq	%rax, -152(%rbp)
	movq	%rax, %rsi
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rsi
	ja	.L1902
	movq	%rdi, %r12
	leaq	0(,%rsi,8), %r14
	testq	%rsi, %rsi
	jne	.L1903
	movq	8(%r13), %r15
	xorl	%ebx, %ebx
	subq	%r14, %r15
	xorl	%r14d, %r14d
	.p2align 4,,10
	.p2align 3
.L1798:
	movq	0(%r13), %rdx
	movq	%r15, %rax
	subq	%rdx, %rax
	sarq	$3, %rax
	movq	%rax, %rcx
	subq	-152(%rbp), %rcx
	jb	.L1809
	cmpq	%rcx, %rax
	ja	.L1904
.L1811:
	movq	32(%r12), %rax
	leaq	-128(%rbp), %r15
	movq	40(%r12), %rdx
	movq	%r15, %rdi
	leaq	256(%rax), %rsi
.LEHB205:
	call	_ZN2v88internal6torque19LowerParameterTypesERKNS1_14ParameterTypesEm@PLT
	movq	-128(%rbp), %r8
	movq	-120(%rbp), %rcx
	subq	%r14, %rbx
	subq	%r8, %rcx
	cmpq	%rcx, %rbx
	je	.L1905
	testq	%r8, %r8
	je	.L1845
	movq	%r8, %rdi
	call	_ZdlPv@PLT
.L1845:
	leaq	.LC55(%rip), %rdi
	call	_ZN2v88internal6torque11ReportErrorIJRA21_KcEEEvDpOT_
	.p2align 4,,10
	.p2align 3
.L1904:
	leaq	(%rdx,%rcx,8), %rax
	cmpq	%r15, %rax
	je	.L1811
	movq	%rax, 8(%r13)
	jmp	.L1811
	.p2align 4,,10
	.p2align 3
.L1905:
	testq	%rbx, %rbx
	jne	.L1906
	testq	%r8, %r8
	jne	.L1907
.L1818:
	movq	32(%r12), %rax
	cmpb	$0, 328(%rax)
	jne	.L1908
.L1819:
	cmpb	$0, 48(%r12)
	je	.L1820
	movq	8(%r13), %rax
	movq	0(%r13), %rsi
	pxor	%xmm0, %xmm0
	movq	$0, -112(%rbp)
	movaps	%xmm0, -128(%rbp)
	movq	%rax, %rbx
	subq	%rsi, %rbx
	movq	%rbx, %rdx
	sarq	$3, %rdx
	je	.L1909
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L1910
	movq	%rbx, %rdi
	call	_Znwm@PLT
.LEHE205:
	movq	%rax, %rcx
	movq	8(%r13), %rax
	movq	0(%r13), %rsi
	movq	%rax, %rdx
	subq	%rsi, %rdx
.L1822:
	movq	%rcx, %xmm0
	addq	%rcx, %rbx
	punpcklqdq	%xmm0, %xmm0
	movq	%rbx, -112(%rbp)
	movaps	%xmm0, -128(%rbp)
	cmpq	%rsi, %rax
	je	.L1824
	movq	%rcx, %rdi
	movq	%rdx, -152(%rbp)
	call	memmove@PLT
	movq	-152(%rbp), %rdx
	movq	%rax, %rcx
.L1824:
	addq	%rdx, %rcx
	movq	%rcx, -120(%rbp)
.LEHB206:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_10TypeOracleEEERPNT_5ScopeEv@PLT
.LEHE206:
	leaq	-80(%rbp), %rbx
	leaq	-96(%rbp), %rdi
	movb	$121, -76(%rbp)
	movq	%rbx, -96(%rbp)
	movl	$1849774922, -80(%rbp)
	movq	$5, -88(%rbp)
	movb	$0, -75(%rbp)
.LEHB207:
	call	_ZN2v88internal6torque12Declarations16LookupGlobalTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
.LEHE207:
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1825
	movq	%rax, -152(%rbp)
	call	_ZdlPv@PLT
	movq	-152(%rbp), %rax
.L1825:
	leaq	-136(%rbp), %rbx
	movq	%r15, %rdi
	movq	%rax, -136(%rbp)
	movq	%rbx, %rsi
.LEHB208:
	call	_ZNSt6vectorIPKN2v88internal6torque4TypeESaIS5_EE12emplace_backIJS5_EEEvDpOT_
	movq	56(%r12), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal6torque5Block13SetInputTypesERKNS1_5StackIPKNS1_4TypeEEE@PLT
.LEHE208:
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1820
	call	_ZdlPv@PLT
.L1820:
	movq	32(%r12), %rax
	movq	296(%rax), %r12
.LEHB209:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_10TypeOracleEEERPNT_5ScopeEv@PLT
.LEHE209:
	leaq	-80(%rbp), %rbx
	leaq	-96(%rbp), %rdi
	movb	$114, -76(%rbp)
	movq	%rbx, -96(%rbp)
	movl	$1702258030, -80(%rbp)
	movq	$5, -88(%rbp)
	movb	$0, -75(%rbp)
.LEHB210:
	call	_ZN2v88internal6torque12Declarations16LookupGlobalTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
.LEHE210:
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1831
	movq	%rax, -152(%rbp)
	call	_ZdlPv@PLT
	movq	-152(%rbp), %rax
.L1831:
	cmpq	%rax, %r12
	je	.L1833
	movq	%r12, %rsi
	movq	%r15, %rdi
.LEHB211:
	call	_ZN2v88internal6torque9LowerTypeEPKNS1_4TypeE@PLT
.LEHE211:
	movq	-128(%rbp), %r12
	movq	-120(%rbp), %r15
	cmpq	%r15, %r12
	je	.L1837
	leaq	-136(%rbp), %rbx
	.p2align 4,,10
	.p2align 3
.L1838:
	movq	(%r12), %rax
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rax, -136(%rbp)
.LEHB212:
	call	_ZNSt6vectorIPKN2v88internal6torque4TypeESaIS5_EE12emplace_backIJS5_EEEvDpOT_
.LEHE212:
	addq	$8, %r12
	cmpq	%r12, %r15
	jne	.L1838
	movq	-128(%rbp), %r15
.L1837:
	testq	%r15, %r15
	je	.L1833
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L1833:
	testq	%r14, %r14
	je	.L1792
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1792:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1911
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1908:
	.cfi_restore_state
	movq	%r13, %rsi
	movq	%r12, %rdi
.LEHB213:
	call	_ZNK2v88internal6torque15InstructionBase24InvalidateTransientTypesEPNS1_5StackIPKNS1_4TypeEEE
.LEHE213:
	jmp	.L1819
	.p2align 4,,10
	.p2align 3
.L1903:
	movq	%r14, %rdi
.LEHB214:
	call	_Znwm@PLT
.LEHE214:
	movq	%rax, %rbx
	leaq	(%rax,%r14), %r8
	movq	8(%r13), %rax
	movq	%rax, %r15
	subq	%r14, %r15
	cmpq	%r15, %rax
	je	.L1912
	movq	%rbx, %r14
	jmp	.L1807
	.p2align 4,,10
	.p2align 3
.L1913:
	movq	(%r15), %rax
	addq	$8, %rbx
	movq	%rax, -8(%rbx)
.L1800:
	addq	$8, %r15
	cmpq	%r15, 8(%r13)
	je	.L1798
.L1807:
	cmpq	%rbx, %r8
	jne	.L1913
	movabsq	$1152921504606846975, %rsi
	subq	%r14, %r8
	movq	%r8, %rax
	movq	%r8, -160(%rbp)
	sarq	$3, %rax
	cmpq	%rsi, %rax
	je	.L1914
	testq	%rax, %rax
	je	.L1850
	movabsq	$9223372036854775800, %rbx
	leaq	(%rax,%rax), %rdx
	cmpq	%rdx, %rax
	jbe	.L1915
.L1802:
	movq	%rbx, %rdi
.LEHB215:
	call	_Znwm@PLT
	movq	%rax, %rcx
	leaq	(%rax,%rbx), %r8
.L1803:
	movq	-160(%rbp), %rdx
	movq	(%r15), %rax
	leaq	8(%rcx,%rdx), %rbx
	movq	%rax, (%rcx,%rdx)
	testq	%rdx, %rdx
	jg	.L1916
	testq	%r14, %r14
	jne	.L1805
.L1806:
	movq	%rcx, %r14
	jmp	.L1800
	.p2align 4,,10
	.p2align 3
.L1916:
	movq	%rcx, %rdi
	movq	%r14, %rsi
	movq	%r8, -160(%rbp)
	call	memmove@PLT
	movq	-160(%rbp), %r8
	movq	%rax, %rcx
.L1805:
	movq	%r14, %rdi
	movq	%r8, -168(%rbp)
	movq	%rcx, -160(%rbp)
	call	_ZdlPv@PLT
	movq	-168(%rbp), %r8
	movq	-160(%rbp), %rcx
	jmp	.L1806
	.p2align 4,,10
	.p2align 3
.L1915:
	testq	%rdx, %rdx
	jne	.L1917
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	jmp	.L1803
	.p2align 4,,10
	.p2align 3
.L1850:
	movl	$8, %ebx
	jmp	.L1802
	.p2align 4,,10
	.p2align 3
.L1909:
	movq	%rbx, %rdx
	xorl	%ecx, %ecx
	jmp	.L1822
	.p2align 4,,10
	.p2align 3
.L1906:
	movq	%rbx, %rdx
	movq	%r8, %rsi
	movq	%r14, %rdi
	movq	%r8, -152(%rbp)
	call	memcmp@PLT
	movq	-152(%rbp), %r8
	movl	%eax, %ebx
	movq	%r8, %rdi
	call	_ZdlPv@PLT
	testl	%ebx, %ebx
	je	.L1818
	jmp	.L1845
.L1914:
	leaq	.LC2(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1809:
	movq	-152(%rbp), %rsi
	movq	%r13, %rdi
	negq	%rsi
	call	_ZNSt6vectorIPKN2v88internal6torque4TypeESaIS5_EE17_M_default_appendEm
.LEHE215:
	jmp	.L1811
.L1902:
	leaq	.LC54(%rip), %rdi
.LEHB216:
	call	_ZSt20__throw_length_errorPKc@PLT
.LEHE216:
.L1911:
	call	__stack_chk_fail@PLT
.L1910:
.LEHB217:
	call	_ZSt17__throw_bad_allocv@PLT
.LEHE217:
.L1907:
	movq	%r8, %rdi
	call	_ZdlPv@PLT
	jmp	.L1818
.L1917:
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	cmova	%rax, %rdx
	leaq	0(,%rdx,8), %rbx
	jmp	.L1802
.L1912:
	movq	%rbx, %r14
	jmp	.L1798
.L1857:
	endbr64
	movq	%rax, %r12
	jmp	.L1827
.L1858:
	endbr64
	movq	%rax, %r12
	jmp	.L1834
.L1854:
	endbr64
	movq	%rax, %r12
	jmp	.L1829
.L1855:
	endbr64
	movq	%rax, %r12
	jmp	.L1842
.L1859:
	endbr64
.L1901:
	movq	%rax, %r12
	jmp	.L1797
.L1853:
	endbr64
	movq	%rax, %r12
	jmp	.L1836
.L1856:
	endbr64
	movq	%rax, %r12
	jmp	.L1815
.L1860:
	endbr64
	jmp	.L1901
	.section	.gcc_except_table._ZNK2v88internal6torque22CallRuntimeInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE,"a",@progbits
.LLSDA6893:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6893-.LLSDACSB6893
.LLSDACSB6893:
	.uleb128 .LEHB205-.LFB6893
	.uleb128 .LEHE205-.LEHB205
	.uleb128 .L1853-.LFB6893
	.uleb128 0
	.uleb128 .LEHB206-.LFB6893
	.uleb128 .LEHE206-.LEHB206
	.uleb128 .L1854-.LFB6893
	.uleb128 0
	.uleb128 .LEHB207-.LFB6893
	.uleb128 .LEHE207-.LEHB207
	.uleb128 .L1857-.LFB6893
	.uleb128 0
	.uleb128 .LEHB208-.LFB6893
	.uleb128 .LEHE208-.LEHB208
	.uleb128 .L1854-.LFB6893
	.uleb128 0
	.uleb128 .LEHB209-.LFB6893
	.uleb128 .LEHE209-.LEHB209
	.uleb128 .L1853-.LFB6893
	.uleb128 0
	.uleb128 .LEHB210-.LFB6893
	.uleb128 .LEHE210-.LEHB210
	.uleb128 .L1858-.LFB6893
	.uleb128 0
	.uleb128 .LEHB211-.LFB6893
	.uleb128 .LEHE211-.LEHB211
	.uleb128 .L1853-.LFB6893
	.uleb128 0
	.uleb128 .LEHB212-.LFB6893
	.uleb128 .LEHE212-.LEHB212
	.uleb128 .L1855-.LFB6893
	.uleb128 0
	.uleb128 .LEHB213-.LFB6893
	.uleb128 .LEHE213-.LEHB213
	.uleb128 .L1853-.LFB6893
	.uleb128 0
	.uleb128 .LEHB214-.LFB6893
	.uleb128 .LEHE214-.LEHB214
	.uleb128 .L1860-.LFB6893
	.uleb128 0
	.uleb128 .LEHB215-.LFB6893
	.uleb128 .LEHE215-.LEHB215
	.uleb128 .L1856-.LFB6893
	.uleb128 0
	.uleb128 .LEHB216-.LFB6893
	.uleb128 .LEHE216-.LEHB216
	.uleb128 .L1859-.LFB6893
	.uleb128 0
	.uleb128 .LEHB217-.LFB6893
	.uleb128 .LEHE217-.LEHB217
	.uleb128 .L1853-.LFB6893
	.uleb128 0
.LLSDACSE6893:
	.section	.text._ZNK2v88internal6torque22CallRuntimeInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.cfi_endproc
	.section	.text.unlikely._ZNK2v88internal6torque22CallRuntimeInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6893
	.type	_ZNK2v88internal6torque22CallRuntimeInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE.cold, @function
_ZNK2v88internal6torque22CallRuntimeInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE.cold:
.LFSB6893:
.L1827:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1829
	call	_ZdlPv@PLT
.L1829:
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1836
	call	_ZdlPv@PLT
.L1836:
	testq	%r14, %r14
	je	.L1844
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1844:
	movq	%r12, %rdi
.LEHB218:
	call	_Unwind_Resume@PLT
.L1834:
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1836
	call	_ZdlPv@PLT
	jmp	.L1836
.L1842:
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1836
	call	_ZdlPv@PLT
	jmp	.L1836
.L1815:
	testq	%r14, %r14
	je	.L1797
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1797:
	movq	%r12, %rdi
	call	_Unwind_Resume@PLT
.LEHE218:
	.cfi_endproc
.LFE6893:
	.section	.gcc_except_table._ZNK2v88internal6torque22CallRuntimeInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
.LLSDAC6893:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6893-.LLSDACSBC6893
.LLSDACSBC6893:
	.uleb128 .LEHB218-.LCOLDB56
	.uleb128 .LEHE218-.LEHB218
	.uleb128 0
	.uleb128 0
.LLSDACSEC6893:
	.section	.text.unlikely._ZNK2v88internal6torque22CallRuntimeInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.section	.text._ZNK2v88internal6torque22CallRuntimeInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.size	_ZNK2v88internal6torque22CallRuntimeInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE, .-_ZNK2v88internal6torque22CallRuntimeInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.section	.text.unlikely._ZNK2v88internal6torque22CallRuntimeInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.size	_ZNK2v88internal6torque22CallRuntimeInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE.cold, .-_ZNK2v88internal6torque22CallRuntimeInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE.cold
.LCOLDE56:
	.section	.text._ZNK2v88internal6torque22CallRuntimeInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
.LHOTE56:
	.section	.text._ZNK2v88internal6torque22DeleteRangeInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal6torque22DeleteRangeInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.type	_ZNK2v88internal6torque22DeleteRangeInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE, @function
_ZNK2v88internal6torque22DeleteRangeInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE:
.LFB6883:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	movq	%rsi, %rdi
	movq	32(%rax), %rcx
	movq	40(%rax), %rdx
	cmpq	%rcx, %rdx
	je	.L1918
	movq	8(%rdi), %r8
	movq	(%rsi), %rsi
	movq	%rcx, %r9
	subq	%rdx, %r9
	movq	%r8, %rax
	subq	%rsi, %rax
	sarq	$3, %rax
	cmpq	%rdx, %rax
	jbe	.L1921
	salq	$3, %rcx
	.p2align 4,,10
	.p2align 3
.L1922:
	movq	(%rsi,%rdx,8), %rax
	addq	$1, %rdx
	movq	%rax, (%rsi,%rcx)
	movq	8(%rdi), %r8
	addq	$8, %rcx
	movq	(%rdi), %rsi
	movq	%r8, %rax
	subq	%rsi, %rax
	sarq	$3, %rax
	cmpq	%rdx, %rax
	ja	.L1922
.L1921:
	movq	%rax, %rdx
	xorl	%ecx, %ecx
	addq	%r9, %rdx
	setc	%cl
	cmpq	%rax, %rdx
	ja	.L1931
	testq	%rcx, %rcx
	je	.L1918
	leaq	(%rsi,%rdx,8), %rax
	cmpq	%rax, %r8
	je	.L1918
	movq	%rax, 8(%rdi)
.L1918:
	ret
	.p2align 4,,10
	.p2align 3
.L1931:
	movq	%r9, %rsi
	jmp	_ZNSt6vectorIPKN2v88internal6torque4TypeESaIS5_EE17_M_default_appendEm
	.cfi_endproc
.LFE6883:
	.size	_ZNK2v88internal6torque22DeleteRangeInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE, .-_ZNK2v88internal6torque22DeleteRangeInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.section	.rodata._ZNK2v88internal6torque29CallBuiltinPointerInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE.str1.8,"aMS",@progbits,1
	.align 8
.LC57:
	.string	"expected function pointer type"
	.section	.text.unlikely._ZNK2v88internal6torque29CallBuiltinPointerInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE,"ax",@progbits
	.align 2
.LCOLDB58:
	.section	.text._ZNK2v88internal6torque29CallBuiltinPointerInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE,"ax",@progbits
.LHOTB58:
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal6torque29CallBuiltinPointerInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.type	_ZNK2v88internal6torque29CallBuiltinPointerInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE, @function
_ZNK2v88internal6torque29CallBuiltinPointerInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE:
.LFB6892:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6892
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -176(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	40(%rdi), %rax
	movq	%rax, -168(%rbp)
	movq	%rax, %rdx
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L2035
	movq	%rsi, %rbx
	leaq	0(,%rdx,8), %r12
	testq	%rdx, %rdx
	jne	.L2036
	movq	8(%rsi), %rax
	xorl	%r13d, %r13d
	xorl	%r14d, %r14d
	subq	%r12, %rax
	movq	%rax, %r12
	.p2align 4,,10
	.p2align 3
.L1938:
	movq	(%rbx), %rdx
	movq	%r12, %rax
	subq	%rdx, %rax
	sarq	$3, %rax
	movq	%rax, %rcx
	subq	-168(%rbp), %rcx
	jb	.L1949
	cmpq	%rcx, %rax
	ja	.L2037
.L1951:
	movq	-8(%r12), %r15
	subq	$8, %r12
	movq	%r12, 8(%rbx)
	testq	%r15, %r15
	je	.L1952
	cmpl	$2, 8(%r15)
	jne	.L1952
	leaq	-160(%rbp), %r12
	leaq	72(%r15), %rsi
	movq	%r12, %rdi
.LEHB219:
	call	_ZN2v88internal6torque19LowerParameterTypesERKSt6vectorIPKNS1_4TypeESaIS5_EE@PLT
	movq	-160(%rbp), %r8
	movq	-152(%rbp), %rcx
	subq	%r14, %r13
	subq	%r8, %rcx
	cmpq	%rcx, %r13
	je	.L2038
	testq	%r8, %r8
	je	.L1978
	movq	%r8, %rdi
	call	_ZdlPv@PLT
.L1978:
	leaq	.LC55(%rip), %rdi
	call	_ZN2v88internal6torque11ReportErrorIJRA21_KcEEEvDpOT_
	.p2align 4,,10
	.p2align 3
.L2037:
	leaq	(%rdx,%rcx,8), %rax
	cmpq	%r12, %rax
	je	.L1951
	movq	%rax, 8(%rbx)
	movq	%rax, %r12
	jmp	.L1951
	.p2align 4,,10
	.p2align 3
.L2038:
	testq	%r13, %r13
	jne	.L2039
	testq	%r8, %r8
	jne	.L2040
.L1961:
	movq	-176(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZNK2v88internal6torque15InstructionBase24InvalidateTransientTypesEPNS1_5StackIPKNS1_4TypeEEE
	movq	96(%r15), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6torque9LowerTypeEPKNS1_4TypeE@PLT
.LEHE219:
	movq	-160(%rbp), %r12
	movq	-152(%rbp), %r15
	cmpq	%r15, %r12
	je	.L1962
	movq	8(%rbx), %rcx
	jmp	.L1971
	.p2align 4,,10
	.p2align 3
.L2042:
	movq	%rax, (%rcx)
	movq	8(%rbx), %rax
	addq	$8, %r12
	leaq	8(%rax), %rcx
	movq	%rcx, 8(%rbx)
	cmpq	%r12, %r15
	je	.L2041
.L1971:
	movq	(%r12), %rax
	movq	%rax, -184(%rbp)
	cmpq	16(%rbx), %rcx
	jne	.L2042
	movabsq	$1152921504606846975, %rdx
	movq	(%rbx), %rax
	subq	%rax, %rcx
	movq	%rax, -168(%rbp)
	movq	%rcx, %rax
	movq	%rcx, %r13
	sarq	$3, %rax
	cmpq	%rdx, %rax
	je	.L2043
	testq	%rax, %rax
	je	.L1986
	movabsq	$9223372036854775800, %rsi
	leaq	(%rax,%rax), %rdx
	movq	%rsi, -176(%rbp)
	cmpq	%rdx, %rax
	jbe	.L2044
.L1966:
	movq	-176(%rbp), %rdi
.LEHB220:
	call	_Znwm@PLT
.LEHE220:
	movq	-176(%rbp), %r9
	movq	%rax, %r8
	addq	%rax, %r9
.L1967:
	movq	-184(%rbp), %rax
	leaq	8(%r8,%r13), %rcx
	movq	%rax, (%r8,%r13)
	testq	%r13, %r13
	jg	.L2045
	cmpq	$0, -168(%rbp)
	jne	.L1969
.L1970:
	movq	%r8, %xmm0
	movq	%rcx, %xmm1
	addq	$8, %r12
	movq	%r9, 16(%rbx)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rbx)
	cmpq	%r12, %r15
	jne	.L1971
	.p2align 4,,10
	.p2align 3
.L2041:
	movq	-160(%rbp), %r15
.L1962:
	testq	%r15, %r15
	je	.L1972
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L1972:
	testq	%r14, %r14
	je	.L1932
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1932:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2046
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1952:
	.cfi_restore_state
	leaq	-128(%rbp), %r12
	leaq	.LC57(%rip), %rsi
	movq	%r12, %rdi
.LEHB221:
	call	_ZN2v88internal6torqueL7MessageIJRA23_KcEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
.LEHE221:
	movq	%r12, %rdi
.LEHB222:
	call	_ZNK2v88internal6torque14MessageBuilder5ThrowEv@PLT
.LEHE222:
	.p2align 4,,10
	.p2align 3
.L2036:
	movq	%r12, %rdi
.LEHB223:
	call	_Znwm@PLT
.LEHE223:
	leaq	(%rax,%r12), %r8
	movq	%rax, %r13
	movq	8(%rbx), %rax
	movq	%rax, %rsi
	subq	%r12, %rsi
	movq	%rsi, %r12
	cmpq	%rsi, %rax
	je	.L2047
	movq	%r13, %r14
	jmp	.L1947
	.p2align 4,,10
	.p2align 3
.L2048:
	movq	(%r12), %rax
	addq	$8, %r13
	movq	%rax, -8(%r13)
.L1940:
	addq	$8, %r12
	cmpq	%r12, 8(%rbx)
	je	.L1938
.L1947:
	cmpq	%r13, %r8
	jne	.L2048
	movabsq	$1152921504606846975, %rsi
	movq	%r8, %r15
	subq	%r14, %r15
	movq	%r15, %rax
	sarq	$3, %rax
	cmpq	%rsi, %rax
	je	.L2049
	testq	%rax, %rax
	je	.L1982
	movabsq	$9223372036854775800, %r13
	leaq	(%rax,%rax), %rdx
	cmpq	%rdx, %rax
	jbe	.L2050
.L1942:
	movq	%r13, %rdi
.LEHB224:
	call	_Znwm@PLT
	movq	%rax, %rcx
	leaq	(%rax,%r13), %r8
.L1943:
	movq	(%r12), %rax
	leaq	8(%rcx,%r15), %r13
	movq	%rax, (%rcx,%r15)
	testq	%r15, %r15
	jg	.L2051
	testq	%r14, %r14
	jne	.L1945
.L1946:
	movq	%rcx, %r14
	jmp	.L1940
	.p2align 4,,10
	.p2align 3
.L2051:
	movq	%rcx, %rdi
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r8, -184(%rbp)
	call	memmove@PLT
	movq	-184(%rbp), %r8
	movq	%rax, %rcx
.L1945:
	movq	%r14, %rdi
	movq	%r8, -192(%rbp)
	movq	%rcx, -184(%rbp)
	call	_ZdlPv@PLT
	movq	-192(%rbp), %r8
	movq	-184(%rbp), %rcx
	jmp	.L1946
	.p2align 4,,10
	.p2align 3
.L2050:
	testq	%rdx, %rdx
	jne	.L2052
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	jmp	.L1943
	.p2align 4,,10
	.p2align 3
.L1982:
	movl	$8, %r13d
	jmp	.L1942
	.p2align 4,,10
	.p2align 3
.L2039:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r14, %rdi
	movq	%r8, -168(%rbp)
	call	memcmp@PLT
	movq	-168(%rbp), %r8
	movl	%eax, %r13d
	movq	%r8, %rdi
	call	_ZdlPv@PLT
	testl	%r13d, %r13d
	je	.L1961
	jmp	.L1978
	.p2align 4,,10
	.p2align 3
.L2045:
	movq	-168(%rbp), %rsi
	movq	%r8, %rdi
	movq	%r13, %rdx
	movq	%r9, -184(%rbp)
	movq	%rcx, -176(%rbp)
	call	memmove@PLT
	movq	-176(%rbp), %rcx
	movq	-184(%rbp), %r9
	movq	%rax, %r8
.L1969:
	movq	-168(%rbp), %rdi
	movq	%r8, -192(%rbp)
	movq	%r9, -184(%rbp)
	movq	%rcx, -176(%rbp)
	call	_ZdlPv@PLT
	movq	-192(%rbp), %r8
	movq	-184(%rbp), %r9
	movq	-176(%rbp), %rcx
	jmp	.L1970
	.p2align 4,,10
	.p2align 3
.L2044:
	testq	%rdx, %rdx
	jne	.L2053
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	jmp	.L1967
	.p2align 4,,10
	.p2align 3
.L1986:
	movq	$8, -176(%rbp)
	jmp	.L1966
.L2049:
	leaq	.LC2(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1949:
	movq	-168(%rbp), %rsi
	movq	%rbx, %rdi
	negq	%rsi
	call	_ZNSt6vectorIPKN2v88internal6torque4TypeESaIS5_EE17_M_default_appendEm
.LEHE224:
	movq	8(%rbx), %r12
	jmp	.L1951
.L2043:
	leaq	.LC2(%rip), %rdi
.LEHB225:
	call	_ZSt20__throw_length_errorPKc@PLT
.LEHE225:
.L2035:
	leaq	.LC54(%rip), %rdi
.LEHB226:
	call	_ZSt20__throw_length_errorPKc@PLT
.LEHE226:
.L2046:
	call	__stack_chk_fail@PLT
.L2040:
	movq	%r8, %rdi
	call	_ZdlPv@PLT
	jmp	.L1961
.L2053:
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	cmova	%rax, %rdx
	leaq	0(,%rdx,8), %rax
	movq	%rax, -176(%rbp)
	jmp	.L1966
.L2047:
	movq	%r13, %r14
	jmp	.L1938
.L2052:
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	cmova	%rax, %rdx
	leaq	0(,%rdx,8), %r13
	jmp	.L1942
.L1990:
	endbr64
	movq	%rax, %r12
	jmp	.L1974
.L1993:
	endbr64
.L2034:
	movq	%rax, %r12
	jmp	.L1937
.L1992:
	endbr64
	movq	%rax, %rbx
	jmp	.L1957
.L1994:
	endbr64
	jmp	.L2034
.L1989:
	endbr64
	movq	%rax, %r12
	jmp	.L1958
.L1991:
	endbr64
	movq	%rax, %r12
	jmp	.L1955
	.section	.gcc_except_table._ZNK2v88internal6torque29CallBuiltinPointerInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE,"a",@progbits
.LLSDA6892:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6892-.LLSDACSB6892
.LLSDACSB6892:
	.uleb128 .LEHB219-.LFB6892
	.uleb128 .LEHE219-.LEHB219
	.uleb128 .L1989-.LFB6892
	.uleb128 0
	.uleb128 .LEHB220-.LFB6892
	.uleb128 .LEHE220-.LEHB220
	.uleb128 .L1990-.LFB6892
	.uleb128 0
	.uleb128 .LEHB221-.LFB6892
	.uleb128 .LEHE221-.LEHB221
	.uleb128 .L1989-.LFB6892
	.uleb128 0
	.uleb128 .LEHB222-.LFB6892
	.uleb128 .LEHE222-.LEHB222
	.uleb128 .L1992-.LFB6892
	.uleb128 0
	.uleb128 .LEHB223-.LFB6892
	.uleb128 .LEHE223-.LEHB223
	.uleb128 .L1994-.LFB6892
	.uleb128 0
	.uleb128 .LEHB224-.LFB6892
	.uleb128 .LEHE224-.LEHB224
	.uleb128 .L1991-.LFB6892
	.uleb128 0
	.uleb128 .LEHB225-.LFB6892
	.uleb128 .LEHE225-.LEHB225
	.uleb128 .L1990-.LFB6892
	.uleb128 0
	.uleb128 .LEHB226-.LFB6892
	.uleb128 .LEHE226-.LEHB226
	.uleb128 .L1993-.LFB6892
	.uleb128 0
.LLSDACSE6892:
	.section	.text._ZNK2v88internal6torque29CallBuiltinPointerInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.cfi_endproc
	.section	.text.unlikely._ZNK2v88internal6torque29CallBuiltinPointerInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6892
	.type	_ZNK2v88internal6torque29CallBuiltinPointerInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE.cold, @function
_ZNK2v88internal6torque29CallBuiltinPointerInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE.cold:
.LFSB6892:
.L1974:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-160(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1958
	call	_ZdlPv@PLT
.L1958:
	testq	%r14, %r14
	je	.L1976
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1976:
	movq	%r12, %rdi
.LEHB227:
	call	_Unwind_Resume@PLT
.L1955:
	testq	%r14, %r14
	je	.L1937
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1937:
	movq	%r12, %rdi
	call	_Unwind_Resume@PLT
.LEHE227:
.L1957:
	movq	%r12, %rdi
	movq	%rbx, %r12
	call	_ZN2v88internal6torque14MessageBuilderD1Ev
	jmp	.L1958
	.cfi_endproc
.LFE6892:
	.section	.gcc_except_table._ZNK2v88internal6torque29CallBuiltinPointerInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
.LLSDAC6892:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6892-.LLSDACSBC6892
.LLSDACSBC6892:
	.uleb128 .LEHB227-.LCOLDB58
	.uleb128 .LEHE227-.LEHB227
	.uleb128 0
	.uleb128 0
.LLSDACSEC6892:
	.section	.text.unlikely._ZNK2v88internal6torque29CallBuiltinPointerInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.section	.text._ZNK2v88internal6torque29CallBuiltinPointerInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.size	_ZNK2v88internal6torque29CallBuiltinPointerInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE, .-_ZNK2v88internal6torque29CallBuiltinPointerInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.section	.text.unlikely._ZNK2v88internal6torque29CallBuiltinPointerInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.size	_ZNK2v88internal6torque29CallBuiltinPointerInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE.cold, .-_ZNK2v88internal6torque29CallBuiltinPointerInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE.cold
.LCOLDE58:
	.section	.text._ZNK2v88internal6torque29CallBuiltinPointerInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
.LHOTE58:
	.section	.text.unlikely._ZNK2v88internal6torque22CallBuiltinInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE,"ax",@progbits
	.align 2
.LCOLDB59:
	.section	.text._ZNK2v88internal6torque22CallBuiltinInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE,"ax",@progbits
.LHOTB59:
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal6torque22CallBuiltinInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.type	_ZNK2v88internal6torque22CallBuiltinInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE, @function
_ZNK2v88internal6torque22CallBuiltinInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE:
.LFB6891:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6891
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$136, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	40(%rdi), %rax
	movq	%rax, -152(%rbp)
	movq	%rax, %rsi
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rsi
	ja	.L2172
	movq	%rdi, %r13
	leaq	0(,%rsi,8), %r14
	testq	%rsi, %rsi
	jne	.L2173
	movq	8(%rbx), %r15
	xorl	%r12d, %r12d
	subq	%r14, %r15
	xorl	%r14d, %r14d
	.p2align 4,,10
	.p2align 3
.L2060:
	movq	(%rbx), %rdx
	movq	%r15, %rax
	subq	%rdx, %rax
	sarq	$3, %rax
	movq	%rax, %rcx
	subq	-152(%rbp), %rcx
	jb	.L2071
	cmpq	%rcx, %rax
	ja	.L2174
.L2073:
	movq	32(%r13), %rax
	leaq	-128(%rbp), %r15
	xorl	%edx, %edx
	movq	%r15, %rdi
	leaq	256(%rax), %rsi
.LEHB228:
	call	_ZN2v88internal6torque19LowerParameterTypesERKNS1_14ParameterTypesEm@PLT
	movq	-128(%rbp), %r8
	movq	-120(%rbp), %rcx
	subq	%r14, %r12
	subq	%r8, %rcx
	cmpq	%rcx, %r12
	je	.L2175
	testq	%r8, %r8
	je	.L2110
	movq	%r8, %rdi
	call	_ZdlPv@PLT
.L2110:
	leaq	.LC55(%rip), %rdi
	call	_ZN2v88internal6torque11ReportErrorIJRA21_KcEEEvDpOT_
	.p2align 4,,10
	.p2align 3
.L2174:
	leaq	(%rdx,%rcx,8), %rax
	cmpq	%rax, %r15
	je	.L2073
	movq	%rax, 8(%rbx)
	jmp	.L2073
	.p2align 4,,10
	.p2align 3
.L2175:
	testq	%r12, %r12
	jne	.L2176
	testq	%r8, %r8
	jne	.L2177
.L2080:
	movq	32(%r13), %rax
	cmpb	$0, 328(%rax)
	jne	.L2178
.L2081:
	cmpb	$0, 48(%r13)
	je	.L2082
	movq	8(%rbx), %rax
	movq	(%rbx), %rsi
	pxor	%xmm0, %xmm0
	movq	$0, -112(%rbp)
	movaps	%xmm0, -128(%rbp)
	movq	%rax, %r12
	subq	%rsi, %r12
	movq	%r12, %rdx
	sarq	$3, %rdx
	je	.L2179
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L2180
	movq	%r12, %rdi
	call	_Znwm@PLT
.LEHE228:
	movq	%rax, %rcx
	movq	8(%rbx), %rax
	movq	(%rbx), %rsi
	movq	%rax, %rdx
	subq	%rsi, %rdx
.L2084:
	movq	%rcx, %xmm0
	addq	%rcx, %r12
	punpcklqdq	%xmm0, %xmm0
	movq	%r12, -112(%rbp)
	movaps	%xmm0, -128(%rbp)
	cmpq	%rsi, %rax
	je	.L2086
	movq	%rcx, %rdi
	movq	%rdx, -152(%rbp)
	call	memmove@PLT
	movq	-152(%rbp), %rdx
	movq	%rax, %rcx
.L2086:
	addq	%rdx, %rcx
	movq	%rcx, -120(%rbp)
.LEHB229:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_10TypeOracleEEERPNT_5ScopeEv@PLT
.LEHE229:
	leaq	-80(%rbp), %r12
	leaq	-96(%rbp), %rdi
	movb	$121, -76(%rbp)
	movq	%r12, -96(%rbp)
	movl	$1849774922, -80(%rbp)
	movq	$5, -88(%rbp)
	movb	$0, -75(%rbp)
.LEHB230:
	call	_ZN2v88internal6torque12Declarations16LookupGlobalTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
.LEHE230:
	movq	-96(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L2087
	movq	%rax, -152(%rbp)
	call	_ZdlPv@PLT
	movq	-152(%rbp), %rax
.L2087:
	leaq	-136(%rbp), %rsi
	movq	%r15, %rdi
	movq	%rax, -136(%rbp)
.LEHB231:
	call	_ZNSt6vectorIPKN2v88internal6torque4TypeESaIS5_EE12emplace_backIJS5_EEEvDpOT_
	movq	56(%r13), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal6torque5Block13SetInputTypesERKNS1_5StackIPKNS1_4TypeEEE@PLT
.LEHE231:
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2082
	call	_ZdlPv@PLT
.L2082:
	movq	32(%r13), %rax
	movq	%r15, %rdi
	movq	296(%rax), %rsi
.LEHB232:
	call	_ZN2v88internal6torque9LowerTypeEPKNS1_4TypeE@PLT
.LEHE232:
	movq	-128(%rbp), %rax
	movq	-120(%rbp), %r13
	cmpq	%r13, %rax
	je	.L2093
	movq	%rax, -152(%rbp)
	movq	8(%rbx), %r8
	jmp	.L2102
	.p2align 4,,10
	.p2align 3
.L2182:
	movq	%rax, (%r8)
	movq	8(%rbx), %rax
	leaq	8(%rax), %r8
	movq	%r8, 8(%rbx)
.L2095:
	addq	$8, -152(%rbp)
	movq	-152(%rbp), %rax
	cmpq	%rax, %r13
	je	.L2181
.L2102:
	movq	-152(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -168(%rbp)
	cmpq	%r8, 16(%rbx)
	jne	.L2182
	movabsq	$1152921504606846975, %rsi
	movq	(%rbx), %r15
	subq	%r15, %r8
	movq	%r8, %rax
	movq	%r8, %r12
	sarq	$3, %rax
	cmpq	%rsi, %rax
	je	.L2183
	testq	%rax, %rax
	je	.L2118
	movabsq	$9223372036854775800, %rsi
	leaq	(%rax,%rax), %rdx
	movq	%rsi, -160(%rbp)
	cmpq	%rdx, %rax
	jbe	.L2184
.L2097:
	movq	-160(%rbp), %rdi
.LEHB233:
	call	_Znwm@PLT
.LEHE233:
	movq	%rax, %rcx
	movq	-160(%rbp), %rax
	addq	%rcx, %rax
	movq	%rax, -160(%rbp)
.L2098:
	movq	-168(%rbp), %rax
	leaq	8(%rcx,%r12), %r8
	movq	%rax, (%rcx,%r12)
	testq	%r12, %r12
	jg	.L2185
	testq	%r15, %r15
	jne	.L2100
.L2101:
	movq	-160(%rbp), %rax
	movq	%rcx, %xmm0
	movq	%r8, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movq	%rax, 16(%rbx)
	movups	%xmm0, (%rbx)
	jmp	.L2095
	.p2align 4,,10
	.p2align 3
.L2181:
	movq	-128(%rbp), %r13
.L2093:
	testq	%r13, %r13
	je	.L2103
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L2103:
	testq	%r14, %r14
	je	.L2054
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L2054:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2186
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2185:
	.cfi_restore_state
	movq	%rcx, %rdi
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r8, -168(%rbp)
	call	memmove@PLT
	movq	-168(%rbp), %r8
	movq	%rax, %rcx
.L2100:
	movq	%r15, %rdi
	movq	%rcx, -176(%rbp)
	movq	%r8, -168(%rbp)
	call	_ZdlPv@PLT
	movq	-176(%rbp), %rcx
	movq	-168(%rbp), %r8
	jmp	.L2101
	.p2align 4,,10
	.p2align 3
.L2184:
	testq	%rdx, %rdx
	jne	.L2187
	movq	$0, -160(%rbp)
	xorl	%ecx, %ecx
	jmp	.L2098
	.p2align 4,,10
	.p2align 3
.L2118:
	movq	$8, -160(%rbp)
	jmp	.L2097
	.p2align 4,,10
	.p2align 3
.L2178:
	movq	%rbx, %rsi
	movq	%r13, %rdi
.LEHB234:
	call	_ZNK2v88internal6torque15InstructionBase24InvalidateTransientTypesEPNS1_5StackIPKNS1_4TypeEEE
.LEHE234:
	jmp	.L2081
	.p2align 4,,10
	.p2align 3
.L2173:
	movq	%r14, %rdi
.LEHB235:
	call	_Znwm@PLT
.LEHE235:
	movq	%rax, %r12
	leaq	(%rax,%r14), %r8
	movq	8(%rbx), %rax
	movq	%rax, %r15
	subq	%r14, %r15
	cmpq	%r15, %rax
	je	.L2188
	movq	%r12, %r14
	jmp	.L2069
	.p2align 4,,10
	.p2align 3
.L2189:
	movq	(%r15), %rax
	addq	$8, %r12
	movq	%rax, -8(%r12)
.L2062:
	addq	$8, %r15
	cmpq	%r15, 8(%rbx)
	je	.L2060
.L2069:
	cmpq	%r12, %r8
	jne	.L2189
	movabsq	$1152921504606846975, %rdi
	subq	%r14, %r8
	movq	%r8, %rax
	movq	%r8, -160(%rbp)
	sarq	$3, %rax
	cmpq	%rdi, %rax
	je	.L2190
	testq	%rax, %rax
	je	.L2115
	movabsq	$9223372036854775800, %r12
	leaq	(%rax,%rax), %rdx
	cmpq	%rdx, %rax
	jbe	.L2191
.L2064:
	movq	%r12, %rdi
.LEHB236:
	call	_Znwm@PLT
	movq	%rax, %rcx
	leaq	(%rax,%r12), %r8
.L2065:
	movq	-160(%rbp), %rdx
	movq	(%r15), %rax
	leaq	8(%rcx,%rdx), %r12
	movq	%rax, (%rcx,%rdx)
	testq	%rdx, %rdx
	jg	.L2192
	testq	%r14, %r14
	jne	.L2067
.L2068:
	movq	%rcx, %r14
	jmp	.L2062
	.p2align 4,,10
	.p2align 3
.L2192:
	movq	%rcx, %rdi
	movq	%r14, %rsi
	movq	%r8, -160(%rbp)
	call	memmove@PLT
	movq	-160(%rbp), %r8
	movq	%rax, %rcx
.L2067:
	movq	%r14, %rdi
	movq	%r8, -168(%rbp)
	movq	%rcx, -160(%rbp)
	call	_ZdlPv@PLT
	movq	-168(%rbp), %r8
	movq	-160(%rbp), %rcx
	jmp	.L2068
	.p2align 4,,10
	.p2align 3
.L2191:
	testq	%rdx, %rdx
	jne	.L2193
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	jmp	.L2065
	.p2align 4,,10
	.p2align 3
.L2115:
	movl	$8, %r12d
	jmp	.L2064
	.p2align 4,,10
	.p2align 3
.L2179:
	movq	%r12, %rdx
	xorl	%ecx, %ecx
	jmp	.L2084
	.p2align 4,,10
	.p2align 3
.L2176:
	movq	%r12, %rdx
	movq	%r8, %rsi
	movq	%r14, %rdi
	movq	%r8, -152(%rbp)
	call	memcmp@PLT
	movq	-152(%rbp), %r8
	movl	%eax, %r12d
	movq	%r8, %rdi
	call	_ZdlPv@PLT
	testl	%r12d, %r12d
	je	.L2080
	jmp	.L2110
.L2190:
	leaq	.LC2(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.LEHE236:
.L2183:
	leaq	.LC2(%rip), %rdi
.LEHB237:
	call	_ZSt20__throw_length_errorPKc@PLT
.LEHE237:
.L2071:
	movq	-152(%rbp), %rsi
	movq	%rbx, %rdi
	negq	%rsi
.LEHB238:
	call	_ZNSt6vectorIPKN2v88internal6torque4TypeESaIS5_EE17_M_default_appendEm
.LEHE238:
	jmp	.L2073
.L2186:
	call	__stack_chk_fail@PLT
.L2172:
	leaq	.LC54(%rip), %rdi
.LEHB239:
	call	_ZSt20__throw_length_errorPKc@PLT
.LEHE239:
.L2180:
.LEHB240:
	call	_ZSt17__throw_bad_allocv@PLT
.LEHE240:
.L2177:
	movq	%r8, %rdi
	call	_ZdlPv@PLT
	jmp	.L2080
.L2193:
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	cmova	%rax, %rdx
	leaq	0(,%rdx,8), %r12
	jmp	.L2064
.L2188:
	movq	%r12, %r14
	jmp	.L2060
.L2187:
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	cmova	%rax, %rdx
	leaq	0(,%rdx,8), %rax
	movq	%rax, -160(%rbp)
	jmp	.L2097
.L2126:
	endbr64
.L2171:
	movq	%rax, %r12
	jmp	.L2059
.L2123:
	endbr64
	movq	%rax, %r12
	jmp	.L2107
.L2125:
	endbr64
	movq	%rax, %rbx
	jmp	.L2089
.L2122:
	endbr64
	movq	%rax, %r12
	jmp	.L2091
.L2124:
	endbr64
	movq	%rax, %r12
	jmp	.L2077
.L2127:
	endbr64
	jmp	.L2171
.L2121:
	endbr64
	movq	%rax, %r12
	jmp	.L2106
	.section	.gcc_except_table._ZNK2v88internal6torque22CallBuiltinInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE,"a",@progbits
.LLSDA6891:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6891-.LLSDACSB6891
.LLSDACSB6891:
	.uleb128 .LEHB228-.LFB6891
	.uleb128 .LEHE228-.LEHB228
	.uleb128 .L2121-.LFB6891
	.uleb128 0
	.uleb128 .LEHB229-.LFB6891
	.uleb128 .LEHE229-.LEHB229
	.uleb128 .L2122-.LFB6891
	.uleb128 0
	.uleb128 .LEHB230-.LFB6891
	.uleb128 .LEHE230-.LEHB230
	.uleb128 .L2125-.LFB6891
	.uleb128 0
	.uleb128 .LEHB231-.LFB6891
	.uleb128 .LEHE231-.LEHB231
	.uleb128 .L2122-.LFB6891
	.uleb128 0
	.uleb128 .LEHB232-.LFB6891
	.uleb128 .LEHE232-.LEHB232
	.uleb128 .L2121-.LFB6891
	.uleb128 0
	.uleb128 .LEHB233-.LFB6891
	.uleb128 .LEHE233-.LEHB233
	.uleb128 .L2123-.LFB6891
	.uleb128 0
	.uleb128 .LEHB234-.LFB6891
	.uleb128 .LEHE234-.LEHB234
	.uleb128 .L2121-.LFB6891
	.uleb128 0
	.uleb128 .LEHB235-.LFB6891
	.uleb128 .LEHE235-.LEHB235
	.uleb128 .L2127-.LFB6891
	.uleb128 0
	.uleb128 .LEHB236-.LFB6891
	.uleb128 .LEHE236-.LEHB236
	.uleb128 .L2124-.LFB6891
	.uleb128 0
	.uleb128 .LEHB237-.LFB6891
	.uleb128 .LEHE237-.LEHB237
	.uleb128 .L2123-.LFB6891
	.uleb128 0
	.uleb128 .LEHB238-.LFB6891
	.uleb128 .LEHE238-.LEHB238
	.uleb128 .L2124-.LFB6891
	.uleb128 0
	.uleb128 .LEHB239-.LFB6891
	.uleb128 .LEHE239-.LEHB239
	.uleb128 .L2126-.LFB6891
	.uleb128 0
	.uleb128 .LEHB240-.LFB6891
	.uleb128 .LEHE240-.LEHB240
	.uleb128 .L2121-.LFB6891
	.uleb128 0
.LLSDACSE6891:
	.section	.text._ZNK2v88internal6torque22CallBuiltinInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.cfi_endproc
	.section	.text.unlikely._ZNK2v88internal6torque22CallBuiltinInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6891
	.type	_ZNK2v88internal6torque22CallBuiltinInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE.cold, @function
_ZNK2v88internal6torque22CallBuiltinInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE.cold:
.LFSB6891:
.L2107:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2106
	call	_ZdlPv@PLT
.L2106:
	testq	%r14, %r14
	je	.L2109
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L2109:
	movq	%r12, %rdi
.LEHB241:
	call	_Unwind_Resume@PLT
.L2089:
	movq	-96(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L2090
	call	_ZdlPv@PLT
.L2090:
	movq	%rbx, %r12
.L2091:
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2106
	call	_ZdlPv@PLT
	jmp	.L2106
.L2077:
	testq	%r14, %r14
	je	.L2059
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L2059:
	movq	%r12, %rdi
	call	_Unwind_Resume@PLT
.LEHE241:
	.cfi_endproc
.LFE6891:
	.section	.gcc_except_table._ZNK2v88internal6torque22CallBuiltinInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
.LLSDAC6891:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6891-.LLSDACSBC6891
.LLSDACSBC6891:
	.uleb128 .LEHB241-.LCOLDB59
	.uleb128 .LEHE241-.LEHB241
	.uleb128 0
	.uleb128 0
.LLSDACSEC6891:
	.section	.text.unlikely._ZNK2v88internal6torque22CallBuiltinInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.section	.text._ZNK2v88internal6torque22CallBuiltinInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.size	_ZNK2v88internal6torque22CallBuiltinInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE, .-_ZNK2v88internal6torque22CallBuiltinInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.section	.text.unlikely._ZNK2v88internal6torque22CallBuiltinInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.size	_ZNK2v88internal6torque22CallBuiltinInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE.cold, .-_ZNK2v88internal6torque22CallBuiltinInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE.cold
.LCOLDE59:
	.section	.text._ZNK2v88internal6torque22CallBuiltinInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
.LHOTE59:
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal6torque15PeekInstruction5kKindE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal6torque15PeekInstruction5kKindE, @function
_GLOBAL__sub_I__ZN2v88internal6torque15PeekInstruction5kKindE:
.LFB12116:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE12116:
	.size	_GLOBAL__sub_I__ZN2v88internal6torque15PeekInstruction5kKindE, .-_GLOBAL__sub_I__ZN2v88internal6torque15PeekInstruction5kKindE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal6torque15PeekInstruction5kKindE
	.weak	_ZTVN2v88internal6torque7TopTypeE
	.section	.data.rel.ro._ZTVN2v88internal6torque7TopTypeE,"awG",@progbits,_ZTVN2v88internal6torque7TopTypeE,comdat
	.align 8
	.type	_ZTVN2v88internal6torque7TopTypeE, @object
	.size	_ZTVN2v88internal6torque7TopTypeE, 112
_ZTVN2v88internal6torque7TopTypeE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal6torque7TopTypeD1Ev
	.quad	_ZN2v88internal6torque7TopTypeD0Ev
	.quad	_ZNK2v88internal6torque4Type11IsSubtypeOfEPKS2_
	.quad	_ZNK2v88internal6torque7TopType11MangledNameB5cxx11Ev
	.quad	_ZNK2v88internal6torque4Type11IsConstexprEv
	.quad	_ZNK2v88internal6torque4Type11IsTransientEv
	.quad	_ZNK2v88internal6torque4Type19NonConstexprVersionEv
	.quad	_ZNK2v88internal6torque4Type16ConstexprVersionEv
	.quad	_ZNK2v88internal6torque4Type15GetRuntimeTypesB5cxx11Ev
	.quad	_ZNK2v88internal6torque7TopType16ToExplicitStringB5cxx11Ev
	.quad	_ZNK2v88internal6torque7TopType24GetGeneratedTypeNameImplB5cxx11Ev
	.quad	_ZNK2v88internal6torque7TopType29GetGeneratedTNodeTypeNameImplB5cxx11Ev
	.weak	_ZTVN2v88internal6torque15PeekInstructionE
	.section	.data.rel.ro.local._ZTVN2v88internal6torque15PeekInstructionE,"awG",@progbits,_ZTVN2v88internal6torque15PeekInstructionE,comdat
	.align 8
	.type	_ZTVN2v88internal6torque15PeekInstructionE, @object
	.size	_ZTVN2v88internal6torque15PeekInstructionE, 72
_ZTVN2v88internal6torque15PeekInstructionE:
	.quad	0
	.quad	0
	.quad	_ZNK2v88internal6torque15PeekInstruction5CloneEv
	.quad	_ZN2v88internal6torque15PeekInstruction6AssignERKNS1_15InstructionBaseE
	.quad	_ZN2v88internal6torque15PeekInstructionD1Ev
	.quad	_ZN2v88internal6torque15PeekInstructionD0Ev
	.quad	_ZNK2v88internal6torque15PeekInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.quad	_ZNK2v88internal6torque15InstructionBase17IsBlockTerminatorEv
	.quad	_ZNK2v88internal6torque15InstructionBase21AppendSuccessorBlocksEPSt6vectorIPNS1_5BlockESaIS5_EE
	.weak	_ZTVN2v88internal6torque15PokeInstructionE
	.section	.data.rel.ro.local._ZTVN2v88internal6torque15PokeInstructionE,"awG",@progbits,_ZTVN2v88internal6torque15PokeInstructionE,comdat
	.align 8
	.type	_ZTVN2v88internal6torque15PokeInstructionE, @object
	.size	_ZTVN2v88internal6torque15PokeInstructionE, 72
_ZTVN2v88internal6torque15PokeInstructionE:
	.quad	0
	.quad	0
	.quad	_ZNK2v88internal6torque15PokeInstruction5CloneEv
	.quad	_ZN2v88internal6torque15PokeInstruction6AssignERKNS1_15InstructionBaseE
	.quad	_ZN2v88internal6torque15PokeInstructionD1Ev
	.quad	_ZN2v88internal6torque15PokeInstructionD0Ev
	.quad	_ZNK2v88internal6torque15PokeInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.quad	_ZNK2v88internal6torque15InstructionBase17IsBlockTerminatorEv
	.quad	_ZNK2v88internal6torque15InstructionBase21AppendSuccessorBlocksEPSt6vectorIPNS1_5BlockESaIS5_EE
	.weak	_ZTVN2v88internal6torque22DeleteRangeInstructionE
	.section	.data.rel.ro.local._ZTVN2v88internal6torque22DeleteRangeInstructionE,"awG",@progbits,_ZTVN2v88internal6torque22DeleteRangeInstructionE,comdat
	.align 8
	.type	_ZTVN2v88internal6torque22DeleteRangeInstructionE, @object
	.size	_ZTVN2v88internal6torque22DeleteRangeInstructionE, 72
_ZTVN2v88internal6torque22DeleteRangeInstructionE:
	.quad	0
	.quad	0
	.quad	_ZNK2v88internal6torque22DeleteRangeInstruction5CloneEv
	.quad	_ZN2v88internal6torque22DeleteRangeInstruction6AssignERKNS1_15InstructionBaseE
	.quad	_ZN2v88internal6torque22DeleteRangeInstructionD1Ev
	.quad	_ZN2v88internal6torque22DeleteRangeInstructionD0Ev
	.quad	_ZNK2v88internal6torque22DeleteRangeInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.quad	_ZNK2v88internal6torque15InstructionBase17IsBlockTerminatorEv
	.quad	_ZNK2v88internal6torque15InstructionBase21AppendSuccessorBlocksEPSt6vectorIPNS1_5BlockESaIS5_EE
	.weak	_ZTVN2v88internal6torque28PushUninitializedInstructionE
	.section	.data.rel.ro.local._ZTVN2v88internal6torque28PushUninitializedInstructionE,"awG",@progbits,_ZTVN2v88internal6torque28PushUninitializedInstructionE,comdat
	.align 8
	.type	_ZTVN2v88internal6torque28PushUninitializedInstructionE, @object
	.size	_ZTVN2v88internal6torque28PushUninitializedInstructionE, 72
_ZTVN2v88internal6torque28PushUninitializedInstructionE:
	.quad	0
	.quad	0
	.quad	_ZNK2v88internal6torque28PushUninitializedInstruction5CloneEv
	.quad	_ZN2v88internal6torque28PushUninitializedInstruction6AssignERKNS1_15InstructionBaseE
	.quad	_ZN2v88internal6torque28PushUninitializedInstructionD1Ev
	.quad	_ZN2v88internal6torque28PushUninitializedInstructionD0Ev
	.quad	_ZNK2v88internal6torque28PushUninitializedInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.quad	_ZNK2v88internal6torque15InstructionBase17IsBlockTerminatorEv
	.quad	_ZNK2v88internal6torque15InstructionBase21AppendSuccessorBlocksEPSt6vectorIPNS1_5BlockESaIS5_EE
	.weak	_ZTVN2v88internal6torque29PushBuiltinPointerInstructionE
	.section	.data.rel.ro.local._ZTVN2v88internal6torque29PushBuiltinPointerInstructionE,"awG",@progbits,_ZTVN2v88internal6torque29PushBuiltinPointerInstructionE,comdat
	.align 8
	.type	_ZTVN2v88internal6torque29PushBuiltinPointerInstructionE, @object
	.size	_ZTVN2v88internal6torque29PushBuiltinPointerInstructionE, 72
_ZTVN2v88internal6torque29PushBuiltinPointerInstructionE:
	.quad	0
	.quad	0
	.quad	_ZNK2v88internal6torque29PushBuiltinPointerInstruction5CloneEv
	.quad	_ZN2v88internal6torque29PushBuiltinPointerInstruction6AssignERKNS1_15InstructionBaseE
	.quad	_ZN2v88internal6torque29PushBuiltinPointerInstructionD1Ev
	.quad	_ZN2v88internal6torque29PushBuiltinPointerInstructionD0Ev
	.quad	_ZNK2v88internal6torque29PushBuiltinPointerInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.quad	_ZNK2v88internal6torque15InstructionBase17IsBlockTerminatorEv
	.quad	_ZNK2v88internal6torque15InstructionBase21AppendSuccessorBlocksEPSt6vectorIPNS1_5BlockESaIS5_EE
	.weak	_ZTVN2v88internal6torque31CreateFieldReferenceInstructionE
	.section	.data.rel.ro.local._ZTVN2v88internal6torque31CreateFieldReferenceInstructionE,"awG",@progbits,_ZTVN2v88internal6torque31CreateFieldReferenceInstructionE,comdat
	.align 8
	.type	_ZTVN2v88internal6torque31CreateFieldReferenceInstructionE, @object
	.size	_ZTVN2v88internal6torque31CreateFieldReferenceInstructionE, 72
_ZTVN2v88internal6torque31CreateFieldReferenceInstructionE:
	.quad	0
	.quad	0
	.quad	_ZNK2v88internal6torque31CreateFieldReferenceInstruction5CloneEv
	.quad	_ZN2v88internal6torque31CreateFieldReferenceInstruction6AssignERKNS1_15InstructionBaseE
	.quad	_ZN2v88internal6torque31CreateFieldReferenceInstructionD1Ev
	.quad	_ZN2v88internal6torque31CreateFieldReferenceInstructionD0Ev
	.quad	_ZNK2v88internal6torque31CreateFieldReferenceInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.quad	_ZNK2v88internal6torque15InstructionBase17IsBlockTerminatorEv
	.quad	_ZNK2v88internal6torque15InstructionBase21AppendSuccessorBlocksEPSt6vectorIPNS1_5BlockESaIS5_EE
	.weak	_ZTVN2v88internal6torque24LoadReferenceInstructionE
	.section	.data.rel.ro.local._ZTVN2v88internal6torque24LoadReferenceInstructionE,"awG",@progbits,_ZTVN2v88internal6torque24LoadReferenceInstructionE,comdat
	.align 8
	.type	_ZTVN2v88internal6torque24LoadReferenceInstructionE, @object
	.size	_ZTVN2v88internal6torque24LoadReferenceInstructionE, 72
_ZTVN2v88internal6torque24LoadReferenceInstructionE:
	.quad	0
	.quad	0
	.quad	_ZNK2v88internal6torque24LoadReferenceInstruction5CloneEv
	.quad	_ZN2v88internal6torque24LoadReferenceInstruction6AssignERKNS1_15InstructionBaseE
	.quad	_ZN2v88internal6torque24LoadReferenceInstructionD1Ev
	.quad	_ZN2v88internal6torque24LoadReferenceInstructionD0Ev
	.quad	_ZNK2v88internal6torque24LoadReferenceInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.quad	_ZNK2v88internal6torque15InstructionBase17IsBlockTerminatorEv
	.quad	_ZNK2v88internal6torque15InstructionBase21AppendSuccessorBlocksEPSt6vectorIPNS1_5BlockESaIS5_EE
	.weak	_ZTVN2v88internal6torque25StoreReferenceInstructionE
	.section	.data.rel.ro.local._ZTVN2v88internal6torque25StoreReferenceInstructionE,"awG",@progbits,_ZTVN2v88internal6torque25StoreReferenceInstructionE,comdat
	.align 8
	.type	_ZTVN2v88internal6torque25StoreReferenceInstructionE, @object
	.size	_ZTVN2v88internal6torque25StoreReferenceInstructionE, 72
_ZTVN2v88internal6torque25StoreReferenceInstructionE:
	.quad	0
	.quad	0
	.quad	_ZNK2v88internal6torque25StoreReferenceInstruction5CloneEv
	.quad	_ZN2v88internal6torque25StoreReferenceInstruction6AssignERKNS1_15InstructionBaseE
	.quad	_ZN2v88internal6torque25StoreReferenceInstructionD1Ev
	.quad	_ZN2v88internal6torque25StoreReferenceInstructionD0Ev
	.quad	_ZNK2v88internal6torque25StoreReferenceInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.quad	_ZNK2v88internal6torque15InstructionBase17IsBlockTerminatorEv
	.quad	_ZNK2v88internal6torque15InstructionBase21AppendSuccessorBlocksEPSt6vectorIPNS1_5BlockESaIS5_EE
	.weak	_ZTVN2v88internal6torque23CallCsaMacroInstructionE
	.section	.data.rel.ro.local._ZTVN2v88internal6torque23CallCsaMacroInstructionE,"awG",@progbits,_ZTVN2v88internal6torque23CallCsaMacroInstructionE,comdat
	.align 8
	.type	_ZTVN2v88internal6torque23CallCsaMacroInstructionE, @object
	.size	_ZTVN2v88internal6torque23CallCsaMacroInstructionE, 72
_ZTVN2v88internal6torque23CallCsaMacroInstructionE:
	.quad	0
	.quad	0
	.quad	_ZNK2v88internal6torque23CallCsaMacroInstruction5CloneEv
	.quad	_ZN2v88internal6torque23CallCsaMacroInstruction6AssignERKNS1_15InstructionBaseE
	.quad	_ZN2v88internal6torque23CallCsaMacroInstructionD1Ev
	.quad	_ZN2v88internal6torque23CallCsaMacroInstructionD0Ev
	.quad	_ZNK2v88internal6torque23CallCsaMacroInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.quad	_ZNK2v88internal6torque15InstructionBase17IsBlockTerminatorEv
	.quad	_ZNK2v88internal6torque23CallCsaMacroInstruction21AppendSuccessorBlocksEPSt6vectorIPNS1_5BlockESaIS5_EE
	.weak	_ZTVN2v88internal6torque24CallIntrinsicInstructionE
	.section	.data.rel.ro.local._ZTVN2v88internal6torque24CallIntrinsicInstructionE,"awG",@progbits,_ZTVN2v88internal6torque24CallIntrinsicInstructionE,comdat
	.align 8
	.type	_ZTVN2v88internal6torque24CallIntrinsicInstructionE, @object
	.size	_ZTVN2v88internal6torque24CallIntrinsicInstructionE, 72
_ZTVN2v88internal6torque24CallIntrinsicInstructionE:
	.quad	0
	.quad	0
	.quad	_ZNK2v88internal6torque24CallIntrinsicInstruction5CloneEv
	.quad	_ZN2v88internal6torque24CallIntrinsicInstruction6AssignERKNS1_15InstructionBaseE
	.quad	_ZN2v88internal6torque24CallIntrinsicInstructionD1Ev
	.quad	_ZN2v88internal6torque24CallIntrinsicInstructionD0Ev
	.quad	_ZNK2v88internal6torque24CallIntrinsicInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.quad	_ZNK2v88internal6torque15InstructionBase17IsBlockTerminatorEv
	.quad	_ZNK2v88internal6torque15InstructionBase21AppendSuccessorBlocksEPSt6vectorIPNS1_5BlockESaIS5_EE
	.weak	_ZTVN2v88internal6torque28NamespaceConstantInstructionE
	.section	.data.rel.ro.local._ZTVN2v88internal6torque28NamespaceConstantInstructionE,"awG",@progbits,_ZTVN2v88internal6torque28NamespaceConstantInstructionE,comdat
	.align 8
	.type	_ZTVN2v88internal6torque28NamespaceConstantInstructionE, @object
	.size	_ZTVN2v88internal6torque28NamespaceConstantInstructionE, 72
_ZTVN2v88internal6torque28NamespaceConstantInstructionE:
	.quad	0
	.quad	0
	.quad	_ZNK2v88internal6torque28NamespaceConstantInstruction5CloneEv
	.quad	_ZN2v88internal6torque28NamespaceConstantInstruction6AssignERKNS1_15InstructionBaseE
	.quad	_ZN2v88internal6torque28NamespaceConstantInstructionD1Ev
	.quad	_ZN2v88internal6torque28NamespaceConstantInstructionD0Ev
	.quad	_ZNK2v88internal6torque28NamespaceConstantInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.quad	_ZNK2v88internal6torque15InstructionBase17IsBlockTerminatorEv
	.quad	_ZNK2v88internal6torque15InstructionBase21AppendSuccessorBlocksEPSt6vectorIPNS1_5BlockESaIS5_EE
	.weak	_ZTVN2v88internal6torque32CallCsaMacroAndBranchInstructionE
	.section	.data.rel.ro.local._ZTVN2v88internal6torque32CallCsaMacroAndBranchInstructionE,"awG",@progbits,_ZTVN2v88internal6torque32CallCsaMacroAndBranchInstructionE,comdat
	.align 8
	.type	_ZTVN2v88internal6torque32CallCsaMacroAndBranchInstructionE, @object
	.size	_ZTVN2v88internal6torque32CallCsaMacroAndBranchInstructionE, 72
_ZTVN2v88internal6torque32CallCsaMacroAndBranchInstructionE:
	.quad	0
	.quad	0
	.quad	_ZNK2v88internal6torque32CallCsaMacroAndBranchInstruction5CloneEv
	.quad	_ZN2v88internal6torque32CallCsaMacroAndBranchInstruction6AssignERKNS1_15InstructionBaseE
	.quad	_ZN2v88internal6torque32CallCsaMacroAndBranchInstructionD1Ev
	.quad	_ZN2v88internal6torque32CallCsaMacroAndBranchInstructionD0Ev
	.quad	_ZNK2v88internal6torque32CallCsaMacroAndBranchInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.quad	_ZNK2v88internal6torque32CallCsaMacroAndBranchInstruction17IsBlockTerminatorEv
	.quad	_ZNK2v88internal6torque32CallCsaMacroAndBranchInstruction21AppendSuccessorBlocksEPSt6vectorIPNS1_5BlockESaIS5_EE
	.weak	_ZTVN2v88internal6torque22CallBuiltinInstructionE
	.section	.data.rel.ro.local._ZTVN2v88internal6torque22CallBuiltinInstructionE,"awG",@progbits,_ZTVN2v88internal6torque22CallBuiltinInstructionE,comdat
	.align 8
	.type	_ZTVN2v88internal6torque22CallBuiltinInstructionE, @object
	.size	_ZTVN2v88internal6torque22CallBuiltinInstructionE, 72
_ZTVN2v88internal6torque22CallBuiltinInstructionE:
	.quad	0
	.quad	0
	.quad	_ZNK2v88internal6torque22CallBuiltinInstruction5CloneEv
	.quad	_ZN2v88internal6torque22CallBuiltinInstruction6AssignERKNS1_15InstructionBaseE
	.quad	_ZN2v88internal6torque22CallBuiltinInstructionD1Ev
	.quad	_ZN2v88internal6torque22CallBuiltinInstructionD0Ev
	.quad	_ZNK2v88internal6torque22CallBuiltinInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.quad	_ZNK2v88internal6torque22CallBuiltinInstruction17IsBlockTerminatorEv
	.quad	_ZNK2v88internal6torque22CallBuiltinInstruction21AppendSuccessorBlocksEPSt6vectorIPNS1_5BlockESaIS5_EE
	.weak	_ZTVN2v88internal6torque22CallRuntimeInstructionE
	.section	.data.rel.ro.local._ZTVN2v88internal6torque22CallRuntimeInstructionE,"awG",@progbits,_ZTVN2v88internal6torque22CallRuntimeInstructionE,comdat
	.align 8
	.type	_ZTVN2v88internal6torque22CallRuntimeInstructionE, @object
	.size	_ZTVN2v88internal6torque22CallRuntimeInstructionE, 72
_ZTVN2v88internal6torque22CallRuntimeInstructionE:
	.quad	0
	.quad	0
	.quad	_ZNK2v88internal6torque22CallRuntimeInstruction5CloneEv
	.quad	_ZN2v88internal6torque22CallRuntimeInstruction6AssignERKNS1_15InstructionBaseE
	.quad	_ZN2v88internal6torque22CallRuntimeInstructionD1Ev
	.quad	_ZN2v88internal6torque22CallRuntimeInstructionD0Ev
	.quad	_ZNK2v88internal6torque22CallRuntimeInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.quad	_ZNK2v88internal6torque22CallRuntimeInstruction17IsBlockTerminatorEv
	.quad	_ZNK2v88internal6torque22CallRuntimeInstruction21AppendSuccessorBlocksEPSt6vectorIPNS1_5BlockESaIS5_EE
	.weak	_ZTVN2v88internal6torque29CallBuiltinPointerInstructionE
	.section	.data.rel.ro.local._ZTVN2v88internal6torque29CallBuiltinPointerInstructionE,"awG",@progbits,_ZTVN2v88internal6torque29CallBuiltinPointerInstructionE,comdat
	.align 8
	.type	_ZTVN2v88internal6torque29CallBuiltinPointerInstructionE, @object
	.size	_ZTVN2v88internal6torque29CallBuiltinPointerInstructionE, 72
_ZTVN2v88internal6torque29CallBuiltinPointerInstructionE:
	.quad	0
	.quad	0
	.quad	_ZNK2v88internal6torque29CallBuiltinPointerInstruction5CloneEv
	.quad	_ZN2v88internal6torque29CallBuiltinPointerInstruction6AssignERKNS1_15InstructionBaseE
	.quad	_ZN2v88internal6torque29CallBuiltinPointerInstructionD1Ev
	.quad	_ZN2v88internal6torque29CallBuiltinPointerInstructionD0Ev
	.quad	_ZNK2v88internal6torque29CallBuiltinPointerInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.quad	_ZNK2v88internal6torque29CallBuiltinPointerInstruction17IsBlockTerminatorEv
	.quad	_ZNK2v88internal6torque15InstructionBase21AppendSuccessorBlocksEPSt6vectorIPNS1_5BlockESaIS5_EE
	.weak	_ZTVN2v88internal6torque17BranchInstructionE
	.section	.data.rel.ro.local._ZTVN2v88internal6torque17BranchInstructionE,"awG",@progbits,_ZTVN2v88internal6torque17BranchInstructionE,comdat
	.align 8
	.type	_ZTVN2v88internal6torque17BranchInstructionE, @object
	.size	_ZTVN2v88internal6torque17BranchInstructionE, 72
_ZTVN2v88internal6torque17BranchInstructionE:
	.quad	0
	.quad	0
	.quad	_ZNK2v88internal6torque17BranchInstruction5CloneEv
	.quad	_ZN2v88internal6torque17BranchInstruction6AssignERKNS1_15InstructionBaseE
	.quad	_ZN2v88internal6torque17BranchInstructionD1Ev
	.quad	_ZN2v88internal6torque17BranchInstructionD0Ev
	.quad	_ZNK2v88internal6torque17BranchInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.quad	_ZNK2v88internal6torque17BranchInstruction17IsBlockTerminatorEv
	.quad	_ZNK2v88internal6torque17BranchInstruction21AppendSuccessorBlocksEPSt6vectorIPNS1_5BlockESaIS5_EE
	.weak	_ZTVN2v88internal6torque26ConstexprBranchInstructionE
	.section	.data.rel.ro.local._ZTVN2v88internal6torque26ConstexprBranchInstructionE,"awG",@progbits,_ZTVN2v88internal6torque26ConstexprBranchInstructionE,comdat
	.align 8
	.type	_ZTVN2v88internal6torque26ConstexprBranchInstructionE, @object
	.size	_ZTVN2v88internal6torque26ConstexprBranchInstructionE, 72
_ZTVN2v88internal6torque26ConstexprBranchInstructionE:
	.quad	0
	.quad	0
	.quad	_ZNK2v88internal6torque26ConstexprBranchInstruction5CloneEv
	.quad	_ZN2v88internal6torque26ConstexprBranchInstruction6AssignERKNS1_15InstructionBaseE
	.quad	_ZN2v88internal6torque26ConstexprBranchInstructionD1Ev
	.quad	_ZN2v88internal6torque26ConstexprBranchInstructionD0Ev
	.quad	_ZNK2v88internal6torque26ConstexprBranchInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.quad	_ZNK2v88internal6torque26ConstexprBranchInstruction17IsBlockTerminatorEv
	.quad	_ZNK2v88internal6torque26ConstexprBranchInstruction21AppendSuccessorBlocksEPSt6vectorIPNS1_5BlockESaIS5_EE
	.weak	_ZTVN2v88internal6torque15GotoInstructionE
	.section	.data.rel.ro.local._ZTVN2v88internal6torque15GotoInstructionE,"awG",@progbits,_ZTVN2v88internal6torque15GotoInstructionE,comdat
	.align 8
	.type	_ZTVN2v88internal6torque15GotoInstructionE, @object
	.size	_ZTVN2v88internal6torque15GotoInstructionE, 72
_ZTVN2v88internal6torque15GotoInstructionE:
	.quad	0
	.quad	0
	.quad	_ZNK2v88internal6torque15GotoInstruction5CloneEv
	.quad	_ZN2v88internal6torque15GotoInstruction6AssignERKNS1_15InstructionBaseE
	.quad	_ZN2v88internal6torque15GotoInstructionD1Ev
	.quad	_ZN2v88internal6torque15GotoInstructionD0Ev
	.quad	_ZNK2v88internal6torque15GotoInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.quad	_ZNK2v88internal6torque15GotoInstruction17IsBlockTerminatorEv
	.quad	_ZNK2v88internal6torque15GotoInstruction21AppendSuccessorBlocksEPSt6vectorIPNS1_5BlockESaIS5_EE
	.weak	_ZTVN2v88internal6torque23GotoExternalInstructionE
	.section	.data.rel.ro.local._ZTVN2v88internal6torque23GotoExternalInstructionE,"awG",@progbits,_ZTVN2v88internal6torque23GotoExternalInstructionE,comdat
	.align 8
	.type	_ZTVN2v88internal6torque23GotoExternalInstructionE, @object
	.size	_ZTVN2v88internal6torque23GotoExternalInstructionE, 72
_ZTVN2v88internal6torque23GotoExternalInstructionE:
	.quad	0
	.quad	0
	.quad	_ZNK2v88internal6torque23GotoExternalInstruction5CloneEv
	.quad	_ZN2v88internal6torque23GotoExternalInstruction6AssignERKNS1_15InstructionBaseE
	.quad	_ZN2v88internal6torque23GotoExternalInstructionD1Ev
	.quad	_ZN2v88internal6torque23GotoExternalInstructionD0Ev
	.quad	_ZNK2v88internal6torque23GotoExternalInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.quad	_ZNK2v88internal6torque23GotoExternalInstruction17IsBlockTerminatorEv
	.quad	_ZNK2v88internal6torque15InstructionBase21AppendSuccessorBlocksEPSt6vectorIPNS1_5BlockESaIS5_EE
	.weak	_ZTVN2v88internal6torque17ReturnInstructionE
	.section	.data.rel.ro.local._ZTVN2v88internal6torque17ReturnInstructionE,"awG",@progbits,_ZTVN2v88internal6torque17ReturnInstructionE,comdat
	.align 8
	.type	_ZTVN2v88internal6torque17ReturnInstructionE, @object
	.size	_ZTVN2v88internal6torque17ReturnInstructionE, 72
_ZTVN2v88internal6torque17ReturnInstructionE:
	.quad	0
	.quad	0
	.quad	_ZNK2v88internal6torque17ReturnInstruction5CloneEv
	.quad	_ZN2v88internal6torque17ReturnInstruction6AssignERKNS1_15InstructionBaseE
	.quad	_ZN2v88internal6torque17ReturnInstructionD1Ev
	.quad	_ZN2v88internal6torque17ReturnInstructionD0Ev
	.quad	_ZNK2v88internal6torque17ReturnInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.quad	_ZNK2v88internal6torque17ReturnInstruction17IsBlockTerminatorEv
	.quad	_ZNK2v88internal6torque15InstructionBase21AppendSuccessorBlocksEPSt6vectorIPNS1_5BlockESaIS5_EE
	.weak	_ZTVN2v88internal6torque30PrintConstantStringInstructionE
	.section	.data.rel.ro.local._ZTVN2v88internal6torque30PrintConstantStringInstructionE,"awG",@progbits,_ZTVN2v88internal6torque30PrintConstantStringInstructionE,comdat
	.align 8
	.type	_ZTVN2v88internal6torque30PrintConstantStringInstructionE, @object
	.size	_ZTVN2v88internal6torque30PrintConstantStringInstructionE, 72
_ZTVN2v88internal6torque30PrintConstantStringInstructionE:
	.quad	0
	.quad	0
	.quad	_ZNK2v88internal6torque30PrintConstantStringInstruction5CloneEv
	.quad	_ZN2v88internal6torque30PrintConstantStringInstruction6AssignERKNS1_15InstructionBaseE
	.quad	_ZN2v88internal6torque30PrintConstantStringInstructionD1Ev
	.quad	_ZN2v88internal6torque30PrintConstantStringInstructionD0Ev
	.quad	_ZNK2v88internal6torque30PrintConstantStringInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.quad	_ZNK2v88internal6torque15InstructionBase17IsBlockTerminatorEv
	.quad	_ZNK2v88internal6torque15InstructionBase21AppendSuccessorBlocksEPSt6vectorIPNS1_5BlockESaIS5_EE
	.weak	_ZTVN2v88internal6torque16AbortInstructionE
	.section	.data.rel.ro.local._ZTVN2v88internal6torque16AbortInstructionE,"awG",@progbits,_ZTVN2v88internal6torque16AbortInstructionE,comdat
	.align 8
	.type	_ZTVN2v88internal6torque16AbortInstructionE, @object
	.size	_ZTVN2v88internal6torque16AbortInstructionE, 72
_ZTVN2v88internal6torque16AbortInstructionE:
	.quad	0
	.quad	0
	.quad	_ZNK2v88internal6torque16AbortInstruction5CloneEv
	.quad	_ZN2v88internal6torque16AbortInstruction6AssignERKNS1_15InstructionBaseE
	.quad	_ZN2v88internal6torque16AbortInstructionD1Ev
	.quad	_ZN2v88internal6torque16AbortInstructionD0Ev
	.quad	_ZNK2v88internal6torque16AbortInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.quad	_ZNK2v88internal6torque16AbortInstruction17IsBlockTerminatorEv
	.quad	_ZNK2v88internal6torque15InstructionBase21AppendSuccessorBlocksEPSt6vectorIPNS1_5BlockESaIS5_EE
	.weak	_ZTVN2v88internal6torque21UnsafeCastInstructionE
	.section	.data.rel.ro.local._ZTVN2v88internal6torque21UnsafeCastInstructionE,"awG",@progbits,_ZTVN2v88internal6torque21UnsafeCastInstructionE,comdat
	.align 8
	.type	_ZTVN2v88internal6torque21UnsafeCastInstructionE, @object
	.size	_ZTVN2v88internal6torque21UnsafeCastInstructionE, 72
_ZTVN2v88internal6torque21UnsafeCastInstructionE:
	.quad	0
	.quad	0
	.quad	_ZNK2v88internal6torque21UnsafeCastInstruction5CloneEv
	.quad	_ZN2v88internal6torque21UnsafeCastInstruction6AssignERKNS1_15InstructionBaseE
	.quad	_ZN2v88internal6torque21UnsafeCastInstructionD1Ev
	.quad	_ZN2v88internal6torque21UnsafeCastInstructionD0Ev
	.quad	_ZNK2v88internal6torque21UnsafeCastInstruction15TypeInstructionEPNS1_5StackIPKNS1_4TypeEEEPNS1_16ControlFlowGraphE
	.quad	_ZNK2v88internal6torque15InstructionBase17IsBlockTerminatorEv
	.quad	_ZNK2v88internal6torque15InstructionBase21AppendSuccessorBlocksEPSt6vectorIPNS1_5BlockESaIS5_EE
	.globl	_ZN2v88internal6torque21UnsafeCastInstruction5kKindE
	.section	.rodata._ZN2v88internal6torque21UnsafeCastInstruction5kKindE,"a"
	.align 4
	.type	_ZN2v88internal6torque21UnsafeCastInstruction5kKindE, @object
	.size	_ZN2v88internal6torque21UnsafeCastInstruction5kKindE, 4
_ZN2v88internal6torque21UnsafeCastInstruction5kKindE:
	.long	22
	.globl	_ZN2v88internal6torque16AbortInstruction5kKindE
	.section	.rodata._ZN2v88internal6torque16AbortInstruction5kKindE,"a"
	.align 4
	.type	_ZN2v88internal6torque16AbortInstruction5kKindE, @object
	.size	_ZN2v88internal6torque16AbortInstruction5kKindE, 4
_ZN2v88internal6torque16AbortInstruction5kKindE:
	.long	21
	.globl	_ZN2v88internal6torque30PrintConstantStringInstruction5kKindE
	.section	.rodata._ZN2v88internal6torque30PrintConstantStringInstruction5kKindE,"a"
	.align 4
	.type	_ZN2v88internal6torque30PrintConstantStringInstruction5kKindE, @object
	.size	_ZN2v88internal6torque30PrintConstantStringInstruction5kKindE, 4
_ZN2v88internal6torque30PrintConstantStringInstruction5kKindE:
	.long	20
	.globl	_ZN2v88internal6torque17ReturnInstruction5kKindE
	.section	.rodata._ZN2v88internal6torque17ReturnInstruction5kKindE,"a"
	.align 4
	.type	_ZN2v88internal6torque17ReturnInstruction5kKindE, @object
	.size	_ZN2v88internal6torque17ReturnInstruction5kKindE, 4
_ZN2v88internal6torque17ReturnInstruction5kKindE:
	.long	19
	.globl	_ZN2v88internal6torque23GotoExternalInstruction5kKindE
	.section	.rodata._ZN2v88internal6torque23GotoExternalInstruction5kKindE,"a"
	.align 4
	.type	_ZN2v88internal6torque23GotoExternalInstruction5kKindE, @object
	.size	_ZN2v88internal6torque23GotoExternalInstruction5kKindE, 4
_ZN2v88internal6torque23GotoExternalInstruction5kKindE:
	.long	18
	.globl	_ZN2v88internal6torque15GotoInstruction5kKindE
	.section	.rodata._ZN2v88internal6torque15GotoInstruction5kKindE,"a"
	.align 4
	.type	_ZN2v88internal6torque15GotoInstruction5kKindE, @object
	.size	_ZN2v88internal6torque15GotoInstruction5kKindE, 4
_ZN2v88internal6torque15GotoInstruction5kKindE:
	.long	17
	.globl	_ZN2v88internal6torque26ConstexprBranchInstruction5kKindE
	.section	.rodata._ZN2v88internal6torque26ConstexprBranchInstruction5kKindE,"a"
	.align 4
	.type	_ZN2v88internal6torque26ConstexprBranchInstruction5kKindE, @object
	.size	_ZN2v88internal6torque26ConstexprBranchInstruction5kKindE, 4
_ZN2v88internal6torque26ConstexprBranchInstruction5kKindE:
	.long	16
	.globl	_ZN2v88internal6torque17BranchInstruction5kKindE
	.section	.rodata._ZN2v88internal6torque17BranchInstruction5kKindE,"a"
	.align 4
	.type	_ZN2v88internal6torque17BranchInstruction5kKindE, @object
	.size	_ZN2v88internal6torque17BranchInstruction5kKindE, 4
_ZN2v88internal6torque17BranchInstruction5kKindE:
	.long	15
	.globl	_ZN2v88internal6torque29CallBuiltinPointerInstruction5kKindE
	.section	.rodata._ZN2v88internal6torque29CallBuiltinPointerInstruction5kKindE,"a"
	.align 4
	.type	_ZN2v88internal6torque29CallBuiltinPointerInstruction5kKindE, @object
	.size	_ZN2v88internal6torque29CallBuiltinPointerInstruction5kKindE, 4
_ZN2v88internal6torque29CallBuiltinPointerInstruction5kKindE:
	.long	14
	.globl	_ZN2v88internal6torque22CallRuntimeInstruction5kKindE
	.section	.rodata._ZN2v88internal6torque22CallRuntimeInstruction5kKindE,"a"
	.align 4
	.type	_ZN2v88internal6torque22CallRuntimeInstruction5kKindE, @object
	.size	_ZN2v88internal6torque22CallRuntimeInstruction5kKindE, 4
_ZN2v88internal6torque22CallRuntimeInstruction5kKindE:
	.long	13
	.globl	_ZN2v88internal6torque22CallBuiltinInstruction5kKindE
	.section	.rodata._ZN2v88internal6torque22CallBuiltinInstruction5kKindE,"a"
	.align 4
	.type	_ZN2v88internal6torque22CallBuiltinInstruction5kKindE, @object
	.size	_ZN2v88internal6torque22CallBuiltinInstruction5kKindE, 4
_ZN2v88internal6torque22CallBuiltinInstruction5kKindE:
	.long	12
	.globl	_ZN2v88internal6torque32CallCsaMacroAndBranchInstruction5kKindE
	.section	.rodata._ZN2v88internal6torque32CallCsaMacroAndBranchInstruction5kKindE,"a"
	.align 4
	.type	_ZN2v88internal6torque32CallCsaMacroAndBranchInstruction5kKindE, @object
	.size	_ZN2v88internal6torque32CallCsaMacroAndBranchInstruction5kKindE, 4
_ZN2v88internal6torque32CallCsaMacroAndBranchInstruction5kKindE:
	.long	11
	.globl	_ZN2v88internal6torque28NamespaceConstantInstruction5kKindE
	.section	.rodata._ZN2v88internal6torque28NamespaceConstantInstruction5kKindE,"a"
	.align 4
	.type	_ZN2v88internal6torque28NamespaceConstantInstruction5kKindE, @object
	.size	_ZN2v88internal6torque28NamespaceConstantInstruction5kKindE, 4
_ZN2v88internal6torque28NamespaceConstantInstruction5kKindE:
	.long	10
	.globl	_ZN2v88internal6torque24CallIntrinsicInstruction5kKindE
	.section	.rodata._ZN2v88internal6torque24CallIntrinsicInstruction5kKindE,"a"
	.align 4
	.type	_ZN2v88internal6torque24CallIntrinsicInstruction5kKindE, @object
	.size	_ZN2v88internal6torque24CallIntrinsicInstruction5kKindE, 4
_ZN2v88internal6torque24CallIntrinsicInstruction5kKindE:
	.long	9
	.globl	_ZN2v88internal6torque23CallCsaMacroInstruction5kKindE
	.section	.rodata._ZN2v88internal6torque23CallCsaMacroInstruction5kKindE,"a"
	.align 4
	.type	_ZN2v88internal6torque23CallCsaMacroInstruction5kKindE, @object
	.size	_ZN2v88internal6torque23CallCsaMacroInstruction5kKindE, 4
_ZN2v88internal6torque23CallCsaMacroInstruction5kKindE:
	.long	8
	.globl	_ZN2v88internal6torque25StoreReferenceInstruction5kKindE
	.section	.rodata._ZN2v88internal6torque25StoreReferenceInstruction5kKindE,"a"
	.align 4
	.type	_ZN2v88internal6torque25StoreReferenceInstruction5kKindE, @object
	.size	_ZN2v88internal6torque25StoreReferenceInstruction5kKindE, 4
_ZN2v88internal6torque25StoreReferenceInstruction5kKindE:
	.long	7
	.globl	_ZN2v88internal6torque24LoadReferenceInstruction5kKindE
	.section	.rodata._ZN2v88internal6torque24LoadReferenceInstruction5kKindE,"a"
	.align 4
	.type	_ZN2v88internal6torque24LoadReferenceInstruction5kKindE, @object
	.size	_ZN2v88internal6torque24LoadReferenceInstruction5kKindE, 4
_ZN2v88internal6torque24LoadReferenceInstruction5kKindE:
	.long	6
	.globl	_ZN2v88internal6torque31CreateFieldReferenceInstruction5kKindE
	.section	.rodata._ZN2v88internal6torque31CreateFieldReferenceInstruction5kKindE,"a"
	.align 4
	.type	_ZN2v88internal6torque31CreateFieldReferenceInstruction5kKindE, @object
	.size	_ZN2v88internal6torque31CreateFieldReferenceInstruction5kKindE, 4
_ZN2v88internal6torque31CreateFieldReferenceInstruction5kKindE:
	.long	5
	.globl	_ZN2v88internal6torque29PushBuiltinPointerInstruction5kKindE
	.section	.rodata._ZN2v88internal6torque29PushBuiltinPointerInstruction5kKindE,"a"
	.align 4
	.type	_ZN2v88internal6torque29PushBuiltinPointerInstruction5kKindE, @object
	.size	_ZN2v88internal6torque29PushBuiltinPointerInstruction5kKindE, 4
_ZN2v88internal6torque29PushBuiltinPointerInstruction5kKindE:
	.long	4
	.globl	_ZN2v88internal6torque28PushUninitializedInstruction5kKindE
	.section	.rodata._ZN2v88internal6torque28PushUninitializedInstruction5kKindE,"a"
	.align 4
	.type	_ZN2v88internal6torque28PushUninitializedInstruction5kKindE, @object
	.size	_ZN2v88internal6torque28PushUninitializedInstruction5kKindE, 4
_ZN2v88internal6torque28PushUninitializedInstruction5kKindE:
	.long	3
	.globl	_ZN2v88internal6torque22DeleteRangeInstruction5kKindE
	.section	.rodata._ZN2v88internal6torque22DeleteRangeInstruction5kKindE,"a"
	.align 4
	.type	_ZN2v88internal6torque22DeleteRangeInstruction5kKindE, @object
	.size	_ZN2v88internal6torque22DeleteRangeInstruction5kKindE, 4
_ZN2v88internal6torque22DeleteRangeInstruction5kKindE:
	.long	2
	.globl	_ZN2v88internal6torque15PokeInstruction5kKindE
	.section	.rodata._ZN2v88internal6torque15PokeInstruction5kKindE,"a"
	.align 4
	.type	_ZN2v88internal6torque15PokeInstruction5kKindE, @object
	.size	_ZN2v88internal6torque15PokeInstruction5kKindE, 4
_ZN2v88internal6torque15PokeInstruction5kKindE:
	.long	1
	.globl	_ZN2v88internal6torque15PeekInstruction5kKindE
	.section	.rodata._ZN2v88internal6torque15PeekInstruction5kKindE,"a"
	.align 4
	.type	_ZN2v88internal6torque15PeekInstruction5kKindE, @object
	.size	_ZN2v88internal6torque15PeekInstruction5kKindE, 4
_ZN2v88internal6torque15PeekInstruction5kKindE:
	.zero	4
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.data.rel.ro,"aw"
	.align 8
.LC17:
	.quad	_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE+64
	.align 8
.LC18:
	.quad	_ZTVSt15basic_streambufIcSt11char_traitsIcEE+16
	.align 8
.LC19:
	.quad	_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE+16
	.hidden	DW.ref.__gxx_personality_v0
	.weak	DW.ref.__gxx_personality_v0
	.section	.data.rel.local.DW.ref.__gxx_personality_v0,"awG",@progbits,DW.ref.__gxx_personality_v0,comdat
	.align 8
	.type	DW.ref.__gxx_personality_v0, @object
	.size	DW.ref.__gxx_personality_v0, 8
DW.ref.__gxx_personality_v0:
	.quad	__gxx_personality_v0
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
