	.file	"type-visitor.cc"
	.text
	.section	.text._ZNK2v88internal6torque4Type11IsConstexprEv,"axG",@progbits,_ZNK2v88internal6torque4Type11IsConstexprEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal6torque4Type11IsConstexprEv
	.type	_ZNK2v88internal6torque4Type11IsConstexprEv, @function
_ZNK2v88internal6torque4Type11IsConstexprEv:
.LFB5435:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE5435:
	.size	_ZNK2v88internal6torque4Type11IsConstexprEv, .-_ZNK2v88internal6torque4Type11IsConstexprEv
	.section	.text._ZNK2v88internal6torque4Type11IsTransientEv,"axG",@progbits,_ZNK2v88internal6torque4Type11IsTransientEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal6torque4Type11IsTransientEv
	.type	_ZNK2v88internal6torque4Type11IsTransientEv, @function
_ZNK2v88internal6torque4Type11IsTransientEv:
.LFB5436:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE5436:
	.size	_ZNK2v88internal6torque4Type11IsTransientEv, .-_ZNK2v88internal6torque4Type11IsTransientEv
	.section	.text._ZNK2v88internal6torque4Type19NonConstexprVersionEv,"axG",@progbits,_ZNK2v88internal6torque4Type19NonConstexprVersionEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal6torque4Type19NonConstexprVersionEv
	.type	_ZNK2v88internal6torque4Type19NonConstexprVersionEv, @function
_ZNK2v88internal6torque4Type19NonConstexprVersionEv:
.LFB5437:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	ret
	.cfi_endproc
.LFE5437:
	.size	_ZNK2v88internal6torque4Type19NonConstexprVersionEv, .-_ZNK2v88internal6torque4Type19NonConstexprVersionEv
	.section	.text._ZNK2v88internal6torque4Type16ConstexprVersionEv,"axG",@progbits,_ZNK2v88internal6torque4Type16ConstexprVersionEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal6torque4Type16ConstexprVersionEv
	.type	_ZNK2v88internal6torque4Type16ConstexprVersionEv, @function
_ZNK2v88internal6torque4Type16ConstexprVersionEv:
.LFB5438:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE5438:
	.size	_ZNK2v88internal6torque4Type16ConstexprVersionEv, .-_ZNK2v88internal6torque4Type16ConstexprVersionEv
	.section	.text._ZNK2v88internal6torque13AggregateType15HasIndexedFieldEv,"axG",@progbits,_ZNK2v88internal6torque13AggregateType15HasIndexedFieldEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal6torque13AggregateType15HasIndexedFieldEv
	.type	_ZNK2v88internal6torque13AggregateType15HasIndexedFieldEv, @function
_ZNK2v88internal6torque13AggregateType15HasIndexedFieldEv:
.LFB5583:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE5583:
	.size	_ZNK2v88internal6torque13AggregateType15HasIndexedFieldEv, .-_ZNK2v88internal6torque13AggregateType15HasIndexedFieldEv
	.section	.rodata._ZNK2v88internal6torque10Declarable9type_nameEv.str1.1,"aMS",@progbits,1
.LC0:
	.string	"<<unknown>>"
	.section	.text._ZNK2v88internal6torque10Declarable9type_nameEv,"axG",@progbits,_ZNK2v88internal6torque10Declarable9type_nameEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal6torque10Declarable9type_nameEv
	.type	_ZNK2v88internal6torque10Declarable9type_nameEv, @function
_ZNK2v88internal6torque10Declarable9type_nameEv:
.LFB6077:
	.cfi_startproc
	endbr64
	leaq	.LC0(%rip), %rax
	ret
	.cfi_endproc
.LFE6077:
	.size	_ZNK2v88internal6torque10Declarable9type_nameEv, .-_ZNK2v88internal6torque10Declarable9type_nameEv
	.section	.rodata._ZNK2v88internal6torque5Scope9type_nameEv.str1.1,"aMS",@progbits,1
.LC1:
	.string	"scope"
	.section	.text._ZNK2v88internal6torque5Scope9type_nameEv,"axG",@progbits,_ZNK2v88internal6torque5Scope9type_nameEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal6torque5Scope9type_nameEv
	.type	_ZNK2v88internal6torque5Scope9type_nameEv, @function
_ZNK2v88internal6torque5Scope9type_nameEv:
.LFB6090:
	.cfi_startproc
	endbr64
	leaq	.LC1(%rip), %rax
	ret
	.cfi_endproc
.LFE6090:
	.size	_ZNK2v88internal6torque5Scope9type_nameEv, .-_ZNK2v88internal6torque5Scope9type_nameEv
	.section	.text._ZN2v88internal6torque10DeclarableD2Ev,"axG",@progbits,_ZN2v88internal6torque10DeclarableD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque10DeclarableD2Ev
	.type	_ZN2v88internal6torque10DeclarableD2Ev, @function
_ZN2v88internal6torque10DeclarableD2Ev:
.LFB6095:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE6095:
	.size	_ZN2v88internal6torque10DeclarableD2Ev, .-_ZN2v88internal6torque10DeclarableD2Ev
	.weak	_ZN2v88internal6torque10DeclarableD1Ev
	.set	_ZN2v88internal6torque10DeclarableD1Ev,_ZN2v88internal6torque10DeclarableD2Ev
	.section	.rodata._ZNK2v88internal6torque9Namespace9type_nameEv.str1.1,"aMS",@progbits,1
.LC2:
	.string	"namespace"
	.section	.text._ZNK2v88internal6torque9Namespace9type_nameEv,"axG",@progbits,_ZNK2v88internal6torque9Namespace9type_nameEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal6torque9Namespace9type_nameEv
	.type	_ZNK2v88internal6torque9Namespace9type_nameEv, @function
_ZNK2v88internal6torque9Namespace9type_nameEv:
.LFB6144:
	.cfi_startproc
	endbr64
	leaq	.LC2(%rip), %rax
	ret
	.cfi_endproc
.LFE6144:
	.size	_ZNK2v88internal6torque9Namespace9type_nameEv, .-_ZNK2v88internal6torque9Namespace9type_nameEv
	.section	.rodata._ZNK2v88internal6torque13AggregateType29GetGeneratedTNodeTypeNameImplB5cxx11Ev.str1.1,"aMS",@progbits,1
.LC3:
	.string	"unreachable code"
	.section	.text._ZNK2v88internal6torque13AggregateType29GetGeneratedTNodeTypeNameImplB5cxx11Ev,"axG",@progbits,_ZNK2v88internal6torque13AggregateType29GetGeneratedTNodeTypeNameImplB5cxx11Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal6torque13AggregateType29GetGeneratedTNodeTypeNameImplB5cxx11Ev
	.type	_ZNK2v88internal6torque13AggregateType29GetGeneratedTNodeTypeNameImplB5cxx11Ev, @function
_ZNK2v88internal6torque13AggregateType29GetGeneratedTNodeTypeNameImplB5cxx11Ev:
.LFB5582:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE5582:
	.size	_ZNK2v88internal6torque13AggregateType29GetGeneratedTNodeTypeNameImplB5cxx11Ev, .-_ZNK2v88internal6torque13AggregateType29GetGeneratedTNodeTypeNameImplB5cxx11Ev
	.section	.text._ZNK2v88internal6torque13AggregateType24GetGeneratedTypeNameImplB5cxx11Ev,"axG",@progbits,_ZNK2v88internal6torque13AggregateType24GetGeneratedTypeNameImplB5cxx11Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal6torque13AggregateType24GetGeneratedTypeNameImplB5cxx11Ev
	.type	_ZNK2v88internal6torque13AggregateType24GetGeneratedTypeNameImplB5cxx11Ev, @function
_ZNK2v88internal6torque13AggregateType24GetGeneratedTypeNameImplB5cxx11Ev:
.LFB5581:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE5581:
	.size	_ZNK2v88internal6torque13AggregateType24GetGeneratedTypeNameImplB5cxx11Ev, .-_ZNK2v88internal6torque13AggregateType24GetGeneratedTypeNameImplB5cxx11Ev
	.section	.text._ZN2v88internal6torque10IdentifierD2Ev,"axG",@progbits,_ZN2v88internal6torque10IdentifierD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque10IdentifierD2Ev
	.type	_ZN2v88internal6torque10IdentifierD2Ev, @function
_ZN2v88internal6torque10IdentifierD2Ev:
.LFB10235:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %r8
	leaq	16+_ZTVN2v88internal6torque10IdentifierE(%rip), %rax
	addq	$48, %rdi
	movq	%rax, -48(%rdi)
	cmpq	%rdi, %r8
	je	.L15
	movq	%r8, %rdi
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L15:
	ret
	.cfi_endproc
.LFE10235:
	.size	_ZN2v88internal6torque10IdentifierD2Ev, .-_ZN2v88internal6torque10IdentifierD2Ev
	.weak	_ZN2v88internal6torque10IdentifierD1Ev
	.set	_ZN2v88internal6torque10IdentifierD1Ev,_ZN2v88internal6torque10IdentifierD2Ev
	.section	.text._ZN2v88internal6torque10DeclarableD0Ev,"axG",@progbits,_ZN2v88internal6torque10DeclarableD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque10DeclarableD0Ev
	.type	_ZN2v88internal6torque10DeclarableD0Ev, @function
_ZN2v88internal6torque10DeclarableD0Ev:
.LFB6097:
	.cfi_startproc
	endbr64
	movl	$72, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE6097:
	.size	_ZN2v88internal6torque10DeclarableD0Ev, .-_ZN2v88internal6torque10DeclarableD0Ev
	.section	.text._ZN2v88internal6torque10IdentifierD0Ev,"axG",@progbits,_ZN2v88internal6torque10IdentifierD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque10IdentifierD0Ev
	.type	_ZN2v88internal6torque10IdentifierD0Ev, @function
_ZN2v88internal6torque10IdentifierD0Ev:
.LFB10237:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal6torque10IdentifierE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	32(%rdi), %rdi
	leaq	48(%r12), %rax
	cmpq	%rax, %rdi
	je	.L19
	call	_ZdlPv@PLT
.L19:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$64, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE10237:
	.size	_ZN2v88internal6torque10IdentifierD0Ev, .-_ZN2v88internal6torque10IdentifierD0Ev
	.section	.text._ZN2v88internal6torque5ScopeD2Ev,"axG",@progbits,_ZN2v88internal6torque5ScopeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque5ScopeD2Ev
	.type	_ZN2v88internal6torque5ScopeD2Ev, @function
_ZN2v88internal6torque5ScopeD2Ev:
.LFB6149:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal6torque5ScopeE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	88(%rdi), %r12
	movq	%rax, (%rdi)
	testq	%r12, %r12
	jne	.L26
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L41:
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	je	.L22
.L25:
	movq	%r13, %r12
.L26:
	movq	40(%r12), %rdi
	movq	(%r12), %r13
	testq	%rdi, %rdi
	je	.L23
	call	_ZdlPv@PLT
.L23:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	jne	.L41
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L25
.L22:
	movq	80(%rbx), %rax
	movq	72(%rbx), %rdi
	xorl	%esi, %esi
	addq	$120, %rbx
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	-48(%rbx), %rdi
	movq	$0, -24(%rbx)
	movq	$0, -32(%rbx)
	cmpq	%rbx, %rdi
	je	.L21
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L21:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE6149:
	.size	_ZN2v88internal6torque5ScopeD2Ev, .-_ZN2v88internal6torque5ScopeD2Ev
	.weak	_ZN2v88internal6torque5ScopeD1Ev
	.set	_ZN2v88internal6torque5ScopeD1Ev,_ZN2v88internal6torque5ScopeD2Ev
	.section	.text._ZN2v88internal6torque9NamespaceD0Ev,"axG",@progbits,_ZN2v88internal6torque9NamespaceD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque9NamespaceD0Ev
	.type	_ZN2v88internal6torque9NamespaceD0Ev, @function
_ZN2v88internal6torque9NamespaceD0Ev:
.LFB8917:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal6torque9NamespaceE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	%rax, (%rdi)
	movq	128(%rdi), %rdi
	leaq	144(%r12), %rax
	cmpq	%rax, %rdi
	je	.L43
	call	_ZdlPv@PLT
.L43:
	movq	88(%r12), %r13
	leaq	16+_ZTVN2v88internal6torque5ScopeE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	jne	.L48
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L63:
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L44
.L47:
	movq	%rbx, %r13
.L48:
	movq	40(%r13), %rdi
	movq	0(%r13), %rbx
	testq	%rdi, %rdi
	je	.L45
	call	_ZdlPv@PLT
.L45:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	jne	.L63
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L47
.L44:
	movq	80(%r12), %rax
	movq	72(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	72(%r12), %rdi
	leaq	120(%r12), %rax
	movq	$0, 96(%r12)
	movq	$0, 88(%r12)
	cmpq	%rax, %rdi
	je	.L49
	call	_ZdlPv@PLT
.L49:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$160, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE8917:
	.size	_ZN2v88internal6torque9NamespaceD0Ev, .-_ZN2v88internal6torque9NamespaceD0Ev
	.section	.text._ZN2v88internal6torque5ScopeD0Ev,"axG",@progbits,_ZN2v88internal6torque5ScopeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque5ScopeD0Ev
	.type	_ZN2v88internal6torque5ScopeD0Ev, @function
_ZN2v88internal6torque5ScopeD0Ev:
.LFB6151:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal6torque5ScopeE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	88(%rdi), %r13
	movq	%rax, (%rdi)
	testq	%r13, %r13
	jne	.L69
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L84:
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L65
.L68:
	movq	%rbx, %r13
.L69:
	movq	40(%r13), %rdi
	movq	0(%r13), %rbx
	testq	%rdi, %rdi
	je	.L66
	call	_ZdlPv@PLT
.L66:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	jne	.L84
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L68
.L65:
	movq	80(%r12), %rax
	movq	72(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	72(%r12), %rdi
	leaq	120(%r12), %rax
	movq	$0, 96(%r12)
	movq	$0, 88(%r12)
	cmpq	%rax, %rdi
	je	.L70
	call	_ZdlPv@PLT
.L70:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$128, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE6151:
	.size	_ZN2v88internal6torque5ScopeD0Ev, .-_ZN2v88internal6torque5ScopeD0Ev
	.section	.text._ZN2v88internal6torque9NamespaceD2Ev,"axG",@progbits,_ZN2v88internal6torque9NamespaceD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque9NamespaceD2Ev
	.type	_ZN2v88internal6torque9NamespaceD2Ev, @function
_ZN2v88internal6torque9NamespaceD2Ev:
.LFB8915:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal6torque9NamespaceE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	128(%rdi), %rdi
	leaq	144(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L86
	call	_ZdlPv@PLT
.L86:
	movq	88(%rbx), %r12
	leaq	16+_ZTVN2v88internal6torque5ScopeE(%rip), %rax
	movq	%rax, (%rbx)
	testq	%r12, %r12
	jne	.L91
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L106:
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	je	.L87
.L90:
	movq	%r13, %r12
.L91:
	movq	40(%r12), %rdi
	movq	(%r12), %r13
	testq	%rdi, %rdi
	je	.L88
	call	_ZdlPv@PLT
.L88:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	jne	.L106
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L90
.L87:
	movq	80(%rbx), %rax
	movq	72(%rbx), %rdi
	xorl	%esi, %esi
	addq	$120, %rbx
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	-48(%rbx), %rdi
	movq	$0, -24(%rbx)
	movq	$0, -32(%rbx)
	cmpq	%rbx, %rdi
	je	.L85
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L85:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8915:
	.size	_ZN2v88internal6torque9NamespaceD2Ev, .-_ZN2v88internal6torque9NamespaceD2Ev
	.weak	_ZN2v88internal6torque9NamespaceD1Ev
	.set	_ZN2v88internal6torque9NamespaceD1Ev,_ZN2v88internal6torque9NamespaceD2Ev
	.section	.rodata._ZNK2v88internal6torque13AggregateType11MangledNameB5cxx11Ev.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"basic_string::_M_construct null not valid"
	.section	.text._ZNK2v88internal6torque13AggregateType11MangledNameB5cxx11Ev,"axG",@progbits,_ZNK2v88internal6torque13AggregateType11MangledNameB5cxx11Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal6torque13AggregateType11MangledNameB5cxx11Ev
	.type	_ZNK2v88internal6torque13AggregateType11MangledNameB5cxx11Ev, @function
_ZNK2v88internal6torque13AggregateType11MangledNameB5cxx11Ev:
.LFB5580:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	addq	$16, %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%rdi, (%r12)
	movq	112(%rsi), %r14
	movq	120(%rsi), %r13
	movq	%r14, %rax
	addq	%r13, %rax
	je	.L108
	testq	%r14, %r14
	je	.L124
.L108:
	movq	%r13, -48(%rbp)
	cmpq	$15, %r13
	ja	.L125
	cmpq	$1, %r13
	jne	.L111
	movzbl	(%r14), %eax
	movb	%al, 16(%r12)
.L112:
	movq	%r13, 8(%r12)
	movb	$0, (%rdi,%r13)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L126
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L111:
	.cfi_restore_state
	testq	%r13, %r13
	je	.L112
	jmp	.L110
	.p2align 4,,10
	.p2align 3
.L125:
	movq	%r12, %rdi
	leaq	-48(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%r12)
	movq	%rax, %rdi
	movq	-48(%rbp), %rax
	movq	%rax, 16(%r12)
.L110:
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-48(%rbp), %r13
	movq	(%r12), %rdi
	jmp	.L112
.L124:
	leaq	.LC4(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L126:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5580:
	.size	_ZNK2v88internal6torque13AggregateType11MangledNameB5cxx11Ev, .-_ZNK2v88internal6torque13AggregateType11MangledNameB5cxx11Ev
	.section	.text._ZNK2v88internal6torque13AggregateType15GetRuntimeTypesB5cxx11Ev,"axG",@progbits,_ZNK2v88internal6torque13AggregateType15GetRuntimeTypesB5cxx11Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal6torque13AggregateType15GetRuntimeTypesB5cxx11Ev
	.type	_ZNK2v88internal6torque13AggregateType15GetRuntimeTypesB5cxx11Ev, @function
_ZNK2v88internal6torque13AggregateType15GetRuntimeTypesB5cxx11Ev:
.LFB5596:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA5596
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	112(%rsi), %r14
	movq	120(%rsi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r15, -96(%rbp)
	movq	%r14, %rax
	addq	%r12, %rax
	je	.L128
	testq	%r14, %r14
	je	.L168
.L128:
	movq	%r12, -104(%rbp)
	cmpq	$15, %r12
	ja	.L169
	cmpq	$1, %r12
	jne	.L131
	movzbl	(%r14), %eax
	movb	%al, -80(%rbp)
	movq	%r15, %rax
.L132:
	movq	%r12, -88(%rbp)
	pxor	%xmm0, %xmm0
	movl	$32, %edi
	movb	$0, (%rax,%r12)
	movq	$0, 16(%r13)
	movups	%xmm0, 0(%r13)
.LEHB0:
	call	_Znwm@PLT
.LEHE0:
	movq	-96(%rbp), %r14
	movq	%rax, %rbx
	movq	-88(%rbp), %r12
	movq	%rax, 0(%r13)
	leaq	32(%rax), %rax
	leaq	16(%rbx), %rdi
	movq	%rax, -120(%rbp)
	movq	%rax, 16(%r13)
	movq	%r14, %rax
	addq	%r12, %rax
	movq	%rdi, (%rbx)
	je	.L133
	testq	%r14, %r14
	je	.L170
.L133:
	movq	%r12, -104(%rbp)
	cmpq	$15, %r12
	ja	.L171
	cmpq	$1, %r12
	jne	.L136
	movzbl	(%r14), %eax
	movb	%al, 16(%rbx)
.L137:
	movq	%r12, 8(%rbx)
	movq	-120(%rbp), %rax
	movb	$0, (%rdi,%r12)
	movq	-96(%rbp), %rdi
	movq	%rax, 8(%r13)
	cmpq	%r15, %rdi
	je	.L127
	call	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L127:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L172
	addq	$88, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L131:
	.cfi_restore_state
	testq	%r12, %r12
	jne	.L173
	movq	%r15, %rax
	jmp	.L132
	.p2align 4,,10
	.p2align 3
.L136:
	testq	%r12, %r12
	je	.L137
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L171:
	leaq	-104(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rbx, %rdi
.LEHB1:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE1:
	movq	%rax, (%rbx)
	movq	%rax, %rdi
	movq	-104(%rbp), %rax
	movq	%rax, 16(%rbx)
.L135:
	movq	%r12, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-104(%rbp), %r12
	movq	(%rbx), %rdi
	jmp	.L137
	.p2align 4,,10
	.p2align 3
.L169:
	leaq	-96(%rbp), %rdi
	leaq	-104(%rbp), %rsi
	xorl	%edx, %edx
.LEHB2:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -96(%rbp)
	movq	%rax, %rdi
	movq	-104(%rbp), %rax
	movq	%rax, -80(%rbp)
.L130:
	movq	%r12, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-104(%rbp), %r12
	movq	-96(%rbp), %rax
	jmp	.L132
.L168:
	leaq	.LC4(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE2:
.L170:
	leaq	.LC4(%rip), %rdi
.LEHB3:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE3:
.L172:
	call	__stack_chk_fail@PLT
.L173:
	movq	%r15, %rdi
	jmp	.L130
.L148:
	endbr64
	movq	%rax, %rdi
	jmp	.L139
.L146:
	endbr64
	movq	%rax, %r12
	jmp	.L141
.L139:
	call	__cxa_begin_catch@PLT
.LEHB4:
	call	__cxa_rethrow@PLT
.LEHE4:
.L140:
	call	__cxa_end_catch@PLT
.L141:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L142
	call	_ZdlPv@PLT
.L142:
	movq	-96(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L143
	call	_ZdlPv@PLT
.L143:
	movq	%r12, %rdi
.LEHB5:
	call	_Unwind_Resume@PLT
.LEHE5:
.L147:
	endbr64
	movq	%rax, %r12
	jmp	.L140
	.cfi_endproc
.LFE5596:
	.globl	__gxx_personality_v0
	.section	.gcc_except_table._ZNK2v88internal6torque13AggregateType15GetRuntimeTypesB5cxx11Ev,"aG",@progbits,_ZNK2v88internal6torque13AggregateType15GetRuntimeTypesB5cxx11Ev,comdat
	.align 4
.LLSDA5596:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATT5596-.LLSDATTD5596
.LLSDATTD5596:
	.byte	0x1
	.uleb128 .LLSDACSE5596-.LLSDACSB5596
.LLSDACSB5596:
	.uleb128 .LEHB0-.LFB5596
	.uleb128 .LEHE0-.LEHB0
	.uleb128 .L146-.LFB5596
	.uleb128 0
	.uleb128 .LEHB1-.LFB5596
	.uleb128 .LEHE1-.LEHB1
	.uleb128 .L148-.LFB5596
	.uleb128 0x1
	.uleb128 .LEHB2-.LFB5596
	.uleb128 .LEHE2-.LEHB2
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB3-.LFB5596
	.uleb128 .LEHE3-.LEHB3
	.uleb128 .L148-.LFB5596
	.uleb128 0x1
	.uleb128 .LEHB4-.LFB5596
	.uleb128 .LEHE4-.LEHB4
	.uleb128 .L147-.LFB5596
	.uleb128 0
	.uleb128 .LEHB5-.LFB5596
	.uleb128 .LEHE5-.LEHB5
	.uleb128 0
	.uleb128 0
.LLSDACSE5596:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATT5596:
	.section	.text._ZNK2v88internal6torque13AggregateType15GetRuntimeTypesB5cxx11Ev,"axG",@progbits,_ZNK2v88internal6torque13AggregateType15GetRuntimeTypesB5cxx11Ev,comdat
	.size	_ZNK2v88internal6torque13AggregateType15GetRuntimeTypesB5cxx11Ev, .-_ZNK2v88internal6torque13AggregateType15GetRuntimeTypesB5cxx11Ev
	.section	.text._ZN2v88internal6torque14MessageBuilderD2Ev,"axG",@progbits,_ZN2v88internal6torque14MessageBuilderD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque14MessageBuilderD2Ev
	.type	_ZN2v88internal6torque14MessageBuilderD2Ev, @function
_ZN2v88internal6torque14MessageBuilderD2Ev:
.LFB4538:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA4538
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	addq	$16, %rbx
	subq	$8, %rsp
	call	_ZNK2v88internal6torque14MessageBuilder6ReportEv@PLT
	movq	-16(%rbx), %rdi
	cmpq	%rbx, %rdi
	je	.L174
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L174:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4538:
	.section	.gcc_except_table._ZN2v88internal6torque14MessageBuilderD2Ev,"aG",@progbits,_ZN2v88internal6torque14MessageBuilderD5Ev,comdat
.LLSDA4538:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE4538-.LLSDACSB4538
.LLSDACSB4538:
.LLSDACSE4538:
	.section	.text._ZN2v88internal6torque14MessageBuilderD2Ev,"axG",@progbits,_ZN2v88internal6torque14MessageBuilderD5Ev,comdat
	.size	_ZN2v88internal6torque14MessageBuilderD2Ev, .-_ZN2v88internal6torque14MessageBuilderD2Ev
	.weak	_ZN2v88internal6torque14MessageBuilderD1Ev
	.set	_ZN2v88internal6torque14MessageBuilderD1Ev,_ZN2v88internal6torque14MessageBuilderD2Ev
	.section	.text._ZN2v88internal6torquelsERSoRKNS1_4TypeE,"axG",@progbits,_ZN2v88internal6torquelsERSoRKNS1_4TypeE,comdat
	.p2align 4
	.weak	_ZN2v88internal6torquelsERSoRKNS1_4TypeE
	.type	_ZN2v88internal6torquelsERSoRKNS1_4TypeE, @function
_ZN2v88internal6torquelsERSoRKNS1_4TypeE:
.LFB5680:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA5680
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-64(%rbp), %rdi
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
.LEHB6:
	call	_ZNK2v88internal6torque4Type8ToStringB5cxx11Ev@PLT
.LEHE6:
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rsi
	movq	%r12, %rdi
.LEHB7:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE7:
	movq	-64(%rbp), %rdi
	leaq	-48(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L178
	call	_ZdlPv@PLT
.L178:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L185
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L185:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
.L182:
	endbr64
	movq	%rax, %r12
.L179:
	movq	-64(%rbp), %rdi
	leaq	-48(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L180
	call	_ZdlPv@PLT
.L180:
	movq	%r12, %rdi
.LEHB8:
	call	_Unwind_Resume@PLT
.LEHE8:
	.cfi_endproc
.LFE5680:
	.section	.gcc_except_table._ZN2v88internal6torquelsERSoRKNS1_4TypeE,"aG",@progbits,_ZN2v88internal6torquelsERSoRKNS1_4TypeE,comdat
.LLSDA5680:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE5680-.LLSDACSB5680
.LLSDACSB5680:
	.uleb128 .LEHB6-.LFB5680
	.uleb128 .LEHE6-.LEHB6
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB7-.LFB5680
	.uleb128 .LEHE7-.LEHB7
	.uleb128 .L182-.LFB5680
	.uleb128 0
	.uleb128 .LEHB8-.LFB5680
	.uleb128 .LEHE8-.LEHB8
	.uleb128 0
	.uleb128 0
.LLSDACSE5680:
	.section	.text._ZN2v88internal6torquelsERSoRKNS1_4TypeE,"axG",@progbits,_ZN2v88internal6torquelsERSoRKNS1_4TypeE,comdat
	.size	_ZN2v88internal6torquelsERSoRKNS1_4TypeE, .-_ZN2v88internal6torquelsERSoRKNS1_4TypeE
	.section	.text._ZN2v88internal6torque13QualifiedNameD2Ev,"axG",@progbits,_ZN2v88internal6torque13QualifiedNameD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque13QualifiedNameD2Ev
	.type	_ZN2v88internal6torque13QualifiedNameD2Ev, @function
_ZN2v88internal6torque13QualifiedNameD2Ev:
.LFB6056:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	leaq	40(%rbx), %rax
	subq	$8, %rsp
	movq	24(%rdi), %rdi
	cmpq	%rax, %rdi
	je	.L187
	call	_ZdlPv@PLT
.L187:
	movq	8(%rbx), %r13
	movq	(%rbx), %r12
	cmpq	%r12, %r13
	je	.L188
	.p2align 4,,10
	.p2align 3
.L192:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L189
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r12, %r13
	jne	.L192
.L190:
	movq	(%rbx), %r12
.L188:
	testq	%r12, %r12
	je	.L186
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L189:
	.cfi_restore_state
	addq	$32, %r12
	cmpq	%r12, %r13
	jne	.L192
	jmp	.L190
	.p2align 4,,10
	.p2align 3
.L186:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE6056:
	.size	_ZN2v88internal6torque13QualifiedNameD2Ev, .-_ZN2v88internal6torque13QualifiedNameD2Ev
	.weak	_ZN2v88internal6torque13QualifiedNameD1Ev
	.set	_ZN2v88internal6torque13QualifiedNameD1Ev,_ZN2v88internal6torque13QualifiedNameD2Ev
	.section	.text._ZN2v88internal6torque9SignatureD2Ev,"axG",@progbits,_ZN2v88internal6torque9SignatureD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque9SignatureD2Ev
	.type	_ZN2v88internal6torque9SignatureD2Ev, @function
_ZN2v88internal6torque9SignatureD2Ev:
.LFB6243:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	120(%rdi), %r13
	movq	112(%rdi), %r12
	cmpq	%r12, %r13
	je	.L196
	.p2align 4,,10
	.p2align 3
.L200:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L197
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r13, %r12
	jne	.L200
.L198:
	movq	112(%rbx), %r12
.L196:
	testq	%r12, %r12
	je	.L201
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L201:
	movq	64(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L202
	call	_ZdlPv@PLT
.L202:
	cmpb	$0, 24(%rbx)
	je	.L203
	movq	32(%rbx), %rdi
	leaq	48(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L203
	call	_ZdlPv@PLT
.L203:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L195
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L197:
	.cfi_restore_state
	addq	$32, %r12
	cmpq	%r12, %r13
	jne	.L200
	jmp	.L198
	.p2align 4,,10
	.p2align 3
.L195:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE6243:
	.size	_ZN2v88internal6torque9SignatureD2Ev, .-_ZN2v88internal6torque9SignatureD2Ev
	.weak	_ZN2v88internal6torque9SignatureD1Ev
	.set	_ZN2v88internal6torque9SignatureD1Ev,_ZN2v88internal6torque9SignatureD2Ev
	.section	.text._ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EED2Ev,"axG",@progbits,_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EED2Ev
	.type	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EED2Ev, @function
_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EED2Ev:
.LFB6888:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	8(%rdi), %rbx
	movq	(%rdi), %r12
	cmpq	%r12, %rbx
	je	.L213
	.p2align 4,,10
	.p2align 3
.L217:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L214
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L217
.L215:
	movq	0(%r13), %r12
.L213:
	testq	%r12, %r12
	je	.L212
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L214:
	.cfi_restore_state
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L217
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L212:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE6888:
	.size	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EED2Ev, .-_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EED2Ev
	.weak	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EED1Ev
	.set	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EED1Ev,_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EED2Ev
	.section	.text.unlikely._ZN2v88internal6torqueL7MessageIJRA17_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA37_S3_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0,"ax",@progbits
.LCOLDB5:
	.section	.text._ZN2v88internal6torqueL7MessageIJRA17_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA37_S3_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0,"ax",@progbits
.LHOTB5:
	.p2align 4
	.type	_ZN2v88internal6torqueL7MessageIJRA17_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA37_S3_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0, @function
_ZN2v88internal6torqueL7MessageIJRA17_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA37_S3_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0:
.LFB13175:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA13175
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-448(%rbp), %r15
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-432(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r15, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$456, %rsp
	movq	%rsi, -488(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
.LEHB9:
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEC1Ev@PLT
.LEHE9:
	movq	-488(%rbp), %rsi
	movq	%r13, %rdi
.LEHB10:
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	8(%rbx), %rdx
	movq	(%rbx), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	leaq	-480(%rbp), %r13
	leaq	-424(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNKSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEE3strEv@PLT
.LEHE10:
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
.LEHB11:
	call	_ZN2v88internal6torque14MessageBuilderC1ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_13TorqueMessage4KindE@PLT
.LEHE11:
	movq	-480(%rbp), %rdi
	leaq	-464(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L221
	call	_ZdlPv@PLT
.L221:
	movq	%r15, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L230
	addq	$456, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L230:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
.L227:
	endbr64
	movq	%rax, %r12
	jmp	.L222
.L226:
	endbr64
	movq	%rax, %r12
	jmp	.L224
	.section	.gcc_except_table._ZN2v88internal6torqueL7MessageIJRA17_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA37_S3_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0,"a",@progbits
.LLSDA13175:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE13175-.LLSDACSB13175
.LLSDACSB13175:
	.uleb128 .LEHB9-.LFB13175
	.uleb128 .LEHE9-.LEHB9
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB10-.LFB13175
	.uleb128 .LEHE10-.LEHB10
	.uleb128 .L226-.LFB13175
	.uleb128 0
	.uleb128 .LEHB11-.LFB13175
	.uleb128 .LEHE11-.LEHB11
	.uleb128 .L227-.LFB13175
	.uleb128 0
.LLSDACSE13175:
	.section	.text._ZN2v88internal6torqueL7MessageIJRA17_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA37_S3_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torqueL7MessageIJRA17_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA37_S3_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC13175
	.type	_ZN2v88internal6torqueL7MessageIJRA17_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA37_S3_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0.cold, @function
_ZN2v88internal6torqueL7MessageIJRA17_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA37_S3_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0.cold:
.LFSB13175:
.L222:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-480(%rbp), %rdi
	leaq	-464(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L224
	call	_ZdlPv@PLT
.L224:
	movq	%r15, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r12, %rdi
.LEHB12:
	call	_Unwind_Resume@PLT
.LEHE12:
	.cfi_endproc
.LFE13175:
	.section	.gcc_except_table._ZN2v88internal6torqueL7MessageIJRA17_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA37_S3_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
.LLSDAC13175:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC13175-.LLSDACSBC13175
.LLSDACSBC13175:
	.uleb128 .LEHB12-.LCOLDB5
	.uleb128 .LEHE12-.LEHB12
	.uleb128 0
	.uleb128 0
.LLSDACSEC13175:
	.section	.text.unlikely._ZN2v88internal6torqueL7MessageIJRA17_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA37_S3_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
	.section	.text._ZN2v88internal6torqueL7MessageIJRA17_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA37_S3_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
	.size	_ZN2v88internal6torqueL7MessageIJRA17_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA37_S3_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0, .-_ZN2v88internal6torqueL7MessageIJRA17_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA37_S3_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
	.section	.text.unlikely._ZN2v88internal6torqueL7MessageIJRA17_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA37_S3_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
	.size	_ZN2v88internal6torqueL7MessageIJRA17_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA37_S3_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0.cold, .-_ZN2v88internal6torqueL7MessageIJRA17_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA37_S3_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0.cold
.LCOLDE5:
	.section	.text._ZN2v88internal6torqueL7MessageIJRA17_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA37_S3_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
.LHOTE5:
	.section	.rodata._ZN2v88internal6torque12_GLOBAL__N_120ComputeGeneratesTypeENS_4base8OptionalINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEb.str1.1,"aMS",@progbits,1
.LC6:
	.string	"basic_string::substr"
	.section	.rodata._ZN2v88internal6torque12_GLOBAL__N_120ComputeGeneratesTypeENS_4base8OptionalINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEb.str1.8,"aMS",@progbits,1
	.align 8
.LC7:
	.string	"%s: __pos (which is %zu) > this->size() (which is %zu)"
	.section	.rodata._ZN2v88internal6torque12_GLOBAL__N_120ComputeGeneratesTypeENS_4base8OptionalINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEb.str1.1
.LC8:
	.string	">"
	.section	.rodata._ZN2v88internal6torque12_GLOBAL__N_120ComputeGeneratesTypeENS_4base8OptionalINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEb.str1.8
	.align 8
.LC9:
	.string	"\" should be of the form \"TNode<...>\""
	.section	.rodata._ZN2v88internal6torque12_GLOBAL__N_120ComputeGeneratesTypeENS_4base8OptionalINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEb.str1.1
.LC10:
	.string	"generated type \""
.LC11:
	.string	"basic_string::basic_string"
.LC12:
	.string	"TNode<"
	.section	.text.unlikely._ZN2v88internal6torque12_GLOBAL__N_120ComputeGeneratesTypeENS_4base8OptionalINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEb,"ax",@progbits
.LCOLDB13:
	.section	.text._ZN2v88internal6torque12_GLOBAL__N_120ComputeGeneratesTypeENS_4base8OptionalINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEb,"ax",@progbits
.LHOTB13:
	.p2align 4
	.type	_ZN2v88internal6torque12_GLOBAL__N_120ComputeGeneratesTypeENS_4base8OptionalINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEb, @function
_ZN2v88internal6torque12_GLOBAL__N_120ComputeGeneratesTypeENS_4base8OptionalINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEb:
.LFB6530:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6530
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, (%rsi)
	je	.L286
	movq	%rsi, %r15
	testb	%dl, %dl
	je	.L234
	cmpq	$6, 16(%rsi)
	leaq	-128(%rbp), %r13
	ja	.L287
.L241:
	movq	%r13, %rdi
	leaq	8(%r15), %rdx
	leaq	.LC9(%rip), %rcx
	leaq	.LC10(%rip), %rsi
.LEHB13:
	call	_ZN2v88internal6torqueL7MessageIJRA17_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA37_S3_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
.LEHE13:
	movq	%r13, %rdi
.LEHB14:
	call	_ZNK2v88internal6torque14MessageBuilder5ThrowEv@PLT
.LEHE14:
	.p2align 4,,10
	.p2align 3
.L234:
	leaq	16(%rdi), %rdi
	movq	%rdi, (%r12)
	movq	8(%rsi), %r14
	movq	16(%rsi), %r13
	movq	%r14, %rax
	addq	%r13, %rax
	je	.L250
	testq	%r14, %r14
	je	.L288
.L250:
	movq	%r13, -168(%rbp)
	cmpq	$15, %r13
	ja	.L289
	cmpq	$1, %r13
	jne	.L253
	movzbl	(%r14), %eax
	movb	%al, 16(%r12)
	.p2align 4,,10
	.p2align 3
.L254:
	movq	%r13, 8(%r12)
	movb	$0, (%rdi,%r13)
	jmp	.L231
	.p2align 4,,10
	.p2align 3
.L286:
	leaq	16(%rdi), %rax
	movq	$0, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 16(%rdi)
.L231:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L290
	addq	$152, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L289:
	.cfi_restore_state
	movq	%r12, %rdi
	leaq	-168(%rbp), %rsi
	xorl	%edx, %edx
.LEHB15:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%r12)
	movq	%rax, %rdi
	movq	-168(%rbp), %rax
	movq	%rax, 16(%r12)
.L252:
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-168(%rbp), %r13
	movq	(%r12), %rdi
	jmp	.L254
	.p2align 4,,10
	.p2align 3
.L287:
	movq	8(%rsi), %rax
	leaq	-144(%rbp), %r14
	leaq	-160(%rbp), %rdi
	movq	%r14, -160(%rbp)
	testq	%rax, %rax
	je	.L291
	movl	%edx, %ebx
	movl	(%rax), %edx
	leaq	.LC12(%rip), %rsi
	movl	%edx, -144(%rbp)
	movzwl	4(%rax), %eax
	movq	$6, -152(%rbp)
	movw	%ax, -140(%rbp)
	movb	$0, -138(%rbp)
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	je	.L292
	movq	-160(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L241
	call	_ZdlPv@PLT
	jmp	.L241
	.p2align 4,,10
	.p2align 3
.L253:
	testq	%r13, %r13
	je	.L254
	jmp	.L252
	.p2align 4,,10
	.p2align 3
.L292:
	movq	16(%r15), %rdx
	leaq	-1(%rdx), %rax
	cmpq	%rdx, %rax
	ja	.L293
	leaq	-112(%rbp), %rcx
	movq	%rcx, -184(%rbp)
	movq	%rcx, -128(%rbp)
	addq	8(%r15), %rax
	je	.L294
	movzbl	(%rax), %eax
	movq	%r13, %rdi
	leaq	.LC8(%rip), %rsi
	movb	$0, -111(%rbp)
	movq	$1, -120(%rbp)
	movb	%al, -112(%rbp)
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	movq	-128(%rbp), %rdi
	movl	%eax, %ebx
	cmpq	-184(%rbp), %rdi
	je	.L240
	call	_ZdlPv@PLT
.L240:
	movq	-160(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L260
	call	_ZdlPv@PLT
.L260:
	testl	%ebx, %ebx
	jne	.L241
	movq	16(%r15), %rcx
	leaq	-7(%rcx), %r13
	cmpq	$5, %rcx
	jbe	.L295
	leaq	16(%r12), %rdi
	movq	%rdi, (%r12)
	movq	16(%r15), %rcx
	movq	8(%r15), %rbx
	cmpq	$5, %rcx
	jbe	.L296
	subq	$6, %rcx
	cmpq	%r13, %rcx
	cmovbe	%rcx, %r13
	movq	%r13, -168(%rbp)
	cmpq	$15, %r13
	ja	.L297
	cmpq	$1, %r13
	jne	.L248
	movzbl	6(%rbx), %eax
	movb	%al, 16(%r12)
	jmp	.L254
	.p2align 4,,10
	.p2align 3
.L248:
	testq	%r13, %r13
	je	.L254
	.p2align 4,,10
	.p2align 3
.L247:
	movq	%r13, %rdx
	leaq	6(%rbx), %rsi
	call	memcpy@PLT
	movq	-168(%rbp), %r13
	movq	(%r12), %rdi
	jmp	.L254
	.p2align 4,,10
	.p2align 3
.L297:
	movq	%r12, %rdi
	leaq	-168(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%r12)
	movq	%rax, %rdi
	movq	-168(%rbp), %rax
	movq	%rax, 16(%r12)
	jmp	.L247
.L290:
	call	__stack_chk_fail@PLT
.L296:
	movl	$6, %edx
	leaq	.LC11(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L295:
	movl	$6, %edx
	leaq	.LC6(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.LEHE15:
.L291:
	leaq	.LC4(%rip), %rdi
.LEHB16:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE16:
.L288:
	leaq	.LC4(%rip), %rdi
.LEHB17:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE17:
.L294:
	leaq	.LC4(%rip), %rdi
.LEHB18:
	call	_ZSt19__throw_logic_errorPKc@PLT
.L293:
	xorl	%ecx, %ecx
	orq	$-1, %rdx
	leaq	.LC6(%rip), %rsi
	xorl	%eax, %eax
	leaq	.LC7(%rip), %rdi
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.LEHE18:
.L264:
	endbr64
	movq	%rax, %r12
	jmp	.L256
.L265:
	endbr64
	movq	%rax, %r12
	jmp	.L243
.L266:
	endbr64
	movq	%rax, %r12
	jmp	.L257
	.section	.gcc_except_table._ZN2v88internal6torque12_GLOBAL__N_120ComputeGeneratesTypeENS_4base8OptionalINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEb,"a",@progbits
.LLSDA6530:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6530-.LLSDACSB6530
.LLSDACSB6530:
	.uleb128 .LEHB13-.LFB6530
	.uleb128 .LEHE13-.LEHB13
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB14-.LFB6530
	.uleb128 .LEHE14-.LEHB14
	.uleb128 .L265-.LFB6530
	.uleb128 0
	.uleb128 .LEHB15-.LFB6530
	.uleb128 .LEHE15-.LEHB15
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB16-.LFB6530
	.uleb128 .LEHE16-.LEHB16
	.uleb128 .L266-.LFB6530
	.uleb128 0
	.uleb128 .LEHB17-.LFB6530
	.uleb128 .LEHE17-.LEHB17
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB18-.LFB6530
	.uleb128 .LEHE18-.LEHB18
	.uleb128 .L264-.LFB6530
	.uleb128 0
.LLSDACSE6530:
	.section	.text._ZN2v88internal6torque12_GLOBAL__N_120ComputeGeneratesTypeENS_4base8OptionalINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEb
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque12_GLOBAL__N_120ComputeGeneratesTypeENS_4base8OptionalINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEb
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6530
	.type	_ZN2v88internal6torque12_GLOBAL__N_120ComputeGeneratesTypeENS_4base8OptionalINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEb.cold, @function
_ZN2v88internal6torque12_GLOBAL__N_120ComputeGeneratesTypeENS_4base8OptionalINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEb.cold:
.LFSB6530:
.L256:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	testb	%bl, %bl
	je	.L257
	movq	-160(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L257
	call	_ZdlPv@PLT
.L257:
	movq	%r12, %rdi
.LEHB19:
	call	_Unwind_Resume@PLT
.L243:
	movq	%r13, %rdi
	call	_ZN2v88internal6torque14MessageBuilderD1Ev
	movq	%r12, %rdi
	call	_Unwind_Resume@PLT
.LEHE19:
	.cfi_endproc
.LFE6530:
	.section	.gcc_except_table._ZN2v88internal6torque12_GLOBAL__N_120ComputeGeneratesTypeENS_4base8OptionalINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEb
.LLSDAC6530:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6530-.LLSDACSBC6530
.LLSDACSBC6530:
	.uleb128 .LEHB19-.LCOLDB13
	.uleb128 .LEHE19-.LEHB19
	.uleb128 0
	.uleb128 0
.LLSDACSEC6530:
	.section	.text.unlikely._ZN2v88internal6torque12_GLOBAL__N_120ComputeGeneratesTypeENS_4base8OptionalINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEb
	.section	.text._ZN2v88internal6torque12_GLOBAL__N_120ComputeGeneratesTypeENS_4base8OptionalINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEb
	.size	_ZN2v88internal6torque12_GLOBAL__N_120ComputeGeneratesTypeENS_4base8OptionalINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEb, .-_ZN2v88internal6torque12_GLOBAL__N_120ComputeGeneratesTypeENS_4base8OptionalINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEb
	.section	.text.unlikely._ZN2v88internal6torque12_GLOBAL__N_120ComputeGeneratesTypeENS_4base8OptionalINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEb
	.size	_ZN2v88internal6torque12_GLOBAL__N_120ComputeGeneratesTypeENS_4base8OptionalINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEb.cold, .-_ZN2v88internal6torque12_GLOBAL__N_120ComputeGeneratesTypeENS_4base8OptionalINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEb.cold
.LCOLDE13:
	.section	.text._ZN2v88internal6torque12_GLOBAL__N_120ComputeGeneratesTypeENS_4base8OptionalINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEb
.LHOTE13:
	.section	.text.unlikely._ZN2v88internal6torqueL7MessageIJRA55_KcEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0,"ax",@progbits
.LCOLDB14:
	.section	.text._ZN2v88internal6torqueL7MessageIJRA55_KcEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0,"ax",@progbits
.LHOTB14:
	.p2align 4
	.type	_ZN2v88internal6torqueL7MessageIJRA55_KcEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0, @function
_ZN2v88internal6torqueL7MessageIJRA55_KcEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0:
.LFB13174:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA13174
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-432(%rbp), %r14
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%r14, %rdi
	subq	$440, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
.LEHB20:
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEC1Ev@PLT
.LEHE20:
	leaq	-416(%rbp), %rdi
	movq	%r13, %rsi
.LEHB21:
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	leaq	-464(%rbp), %r13
	leaq	-408(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNKSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEE3strEv@PLT
.LEHE21:
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
.LEHB22:
	call	_ZN2v88internal6torque14MessageBuilderC1ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_13TorqueMessage4KindE@PLT
.LEHE22:
	movq	-464(%rbp), %rdi
	leaq	-448(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L299
	call	_ZdlPv@PLT
.L299:
	movq	%r14, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L308
	addq	$440, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L308:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
.L305:
	endbr64
	movq	%rax, %r12
	jmp	.L300
.L304:
	endbr64
	movq	%rax, %r12
	jmp	.L302
	.section	.gcc_except_table._ZN2v88internal6torqueL7MessageIJRA55_KcEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0,"a",@progbits
.LLSDA13174:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE13174-.LLSDACSB13174
.LLSDACSB13174:
	.uleb128 .LEHB20-.LFB13174
	.uleb128 .LEHE20-.LEHB20
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB21-.LFB13174
	.uleb128 .LEHE21-.LEHB21
	.uleb128 .L304-.LFB13174
	.uleb128 0
	.uleb128 .LEHB22-.LFB13174
	.uleb128 .LEHE22-.LEHB22
	.uleb128 .L305-.LFB13174
	.uleb128 0
.LLSDACSE13174:
	.section	.text._ZN2v88internal6torqueL7MessageIJRA55_KcEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torqueL7MessageIJRA55_KcEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC13174
	.type	_ZN2v88internal6torqueL7MessageIJRA55_KcEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0.cold, @function
_ZN2v88internal6torqueL7MessageIJRA55_KcEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0.cold:
.LFSB13174:
.L300:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	movq	-464(%rbp), %rdi
	leaq	-448(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L302
	call	_ZdlPv@PLT
.L302:
	movq	%r14, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r12, %rdi
.LEHB23:
	call	_Unwind_Resume@PLT
.LEHE23:
	.cfi_endproc
.LFE13174:
	.section	.gcc_except_table._ZN2v88internal6torqueL7MessageIJRA55_KcEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
.LLSDAC13174:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC13174-.LLSDACSBC13174
.LLSDACSBC13174:
	.uleb128 .LEHB23-.LCOLDB14
	.uleb128 .LEHE23-.LEHB23
	.uleb128 0
	.uleb128 0
.LLSDACSEC13174:
	.section	.text.unlikely._ZN2v88internal6torqueL7MessageIJRA55_KcEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
	.section	.text._ZN2v88internal6torqueL7MessageIJRA55_KcEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
	.size	_ZN2v88internal6torqueL7MessageIJRA55_KcEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0, .-_ZN2v88internal6torqueL7MessageIJRA55_KcEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
	.section	.text.unlikely._ZN2v88internal6torqueL7MessageIJRA55_KcEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
	.size	_ZN2v88internal6torqueL7MessageIJRA55_KcEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0.cold, .-_ZN2v88internal6torqueL7MessageIJRA55_KcEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0.cold
.LCOLDE14:
	.section	.text._ZN2v88internal6torqueL7MessageIJRA55_KcEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
.LHOTE14:
	.section	.text._ZNSt6vectorIN2v88internal6torque16LabelDeclarationESaIS3_EED2Ev,"axG",@progbits,_ZNSt6vectorIN2v88internal6torque16LabelDeclarationESaIS3_EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal6torque16LabelDeclarationESaIS3_EED2Ev
	.type	_ZNSt6vectorIN2v88internal6torque16LabelDeclarationESaIS3_EED2Ev, @function
_ZNSt6vectorIN2v88internal6torque16LabelDeclarationESaIS3_EED2Ev:
.LFB7474:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	8(%rdi), %rbx
	movq	(%rdi), %r12
	cmpq	%r12, %rbx
	je	.L310
	.p2align 4,,10
	.p2align 3
.L314:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L311
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L314
.L312:
	movq	0(%r13), %r12
.L310:
	testq	%r12, %r12
	je	.L309
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L311:
	.cfi_restore_state
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L314
	jmp	.L312
	.p2align 4,,10
	.p2align 3
.L309:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7474:
	.size	_ZNSt6vectorIN2v88internal6torque16LabelDeclarationESaIS3_EED2Ev, .-_ZNSt6vectorIN2v88internal6torque16LabelDeclarationESaIS3_EED2Ev
	.weak	_ZNSt6vectorIN2v88internal6torque16LabelDeclarationESaIS3_EED1Ev
	.set	_ZNSt6vectorIN2v88internal6torque16LabelDeclarationESaIS3_EED1Ev,_ZNSt6vectorIN2v88internal6torque16LabelDeclarationESaIS3_EED2Ev
	.section	.text._ZN2v88internal6torque11ReportErrorIJRA15_KcRNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA27_S3_RKNS1_4TypeERA2_S3_EEEvDpOT_,"axG",@progbits,_ZN2v88internal6torque11ReportErrorIJRA15_KcRNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA27_S3_RKNS1_4TypeERA2_S3_EEEvDpOT_,comdat
	.p2align 4
	.weak	_ZN2v88internal6torque11ReportErrorIJRA15_KcRNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA27_S3_RKNS1_4TypeERA2_S3_EEEvDpOT_
	.type	_ZN2v88internal6torque11ReportErrorIJRA15_KcRNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA27_S3_RKNS1_4TypeERA2_S3_EEEvDpOT_, @function
_ZN2v88internal6torque11ReportErrorIJRA15_KcRNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA27_S3_RKNS1_4TypeERA2_S3_EEEvDpOT_:
.LFB8005:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA8005
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-448(%rbp), %r15
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-432(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$520, %rsp
	movq	%rdi, -560(%rbp)
	movq	%r15, %rdi
	movq	%r8, -552(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
.LEHB24:
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEC1Ev@PLT
.LEHE24:
	movq	-560(%rbp), %r9
	movq	%r12, %rdi
	movq	%r9, %rsi
.LEHB25:
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	8(%rbx), %rdx
	movq	(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6torquelsERSoRKNS1_4TypeE
	movq	-552(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	leaq	-544(%rbp), %r13
	leaq	-424(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNKSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEE3strEv@PLT
.LEHE25:
	leaq	-512(%rbp), %r12
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
.LEHB26:
	call	_ZN2v88internal6torque14MessageBuilderC1ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_13TorqueMessage4KindE@PLT
.LEHE26:
	movq	-544(%rbp), %rdi
	leaq	-528(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L318
	call	_ZdlPv@PLT
.L318:
	movq	%r15, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r12, %rdi
.LEHB27:
	call	_ZNK2v88internal6torque14MessageBuilder5ThrowEv@PLT
.LEHE27:
.L325:
	endbr64
	movq	%rax, %r12
	jmp	.L321
.L324:
	endbr64
	movq	%rax, %r13
	jmp	.L322
.L326:
	endbr64
	movq	%rax, %r12
.L319:
	movq	-544(%rbp), %rdi
	leaq	-528(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L321
	call	_ZdlPv@PLT
.L321:
	movq	%r15, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r12, %rdi
.LEHB28:
	call	_Unwind_Resume@PLT
.L322:
	movq	%r12, %rdi
	call	_ZN2v88internal6torque14MessageBuilderD1Ev
	movq	%r13, %rdi
	call	_Unwind_Resume@PLT
.LEHE28:
	.cfi_endproc
.LFE8005:
	.section	.gcc_except_table._ZN2v88internal6torque11ReportErrorIJRA15_KcRNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA27_S3_RKNS1_4TypeERA2_S3_EEEvDpOT_,"aG",@progbits,_ZN2v88internal6torque11ReportErrorIJRA15_KcRNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA27_S3_RKNS1_4TypeERA2_S3_EEEvDpOT_,comdat
.LLSDA8005:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE8005-.LLSDACSB8005
.LLSDACSB8005:
	.uleb128 .LEHB24-.LFB8005
	.uleb128 .LEHE24-.LEHB24
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB25-.LFB8005
	.uleb128 .LEHE25-.LEHB25
	.uleb128 .L325-.LFB8005
	.uleb128 0
	.uleb128 .LEHB26-.LFB8005
	.uleb128 .LEHE26-.LEHB26
	.uleb128 .L326-.LFB8005
	.uleb128 0
	.uleb128 .LEHB27-.LFB8005
	.uleb128 .LEHE27-.LEHB27
	.uleb128 .L324-.LFB8005
	.uleb128 0
	.uleb128 .LEHB28-.LFB8005
	.uleb128 .LEHE28-.LEHB28
	.uleb128 0
	.uleb128 0
.LLSDACSE8005:
	.section	.text._ZN2v88internal6torque11ReportErrorIJRA15_KcRNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA27_S3_RKNS1_4TypeERA2_S3_EEEvDpOT_,"axG",@progbits,_ZN2v88internal6torque11ReportErrorIJRA15_KcRNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA27_S3_RKNS1_4TypeERA2_S3_EEEvDpOT_,comdat
	.size	_ZN2v88internal6torque11ReportErrorIJRA15_KcRNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA27_S3_RKNS1_4TypeERA2_S3_EEEvDpOT_, .-_ZN2v88internal6torque11ReportErrorIJRA15_KcRNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA27_S3_RKNS1_4TypeERA2_S3_EEEvDpOT_
	.section	.text._ZN2v88internal6torque11ReportErrorIJRA14_KcRNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA27_S3_EEEvDpOT_,"axG",@progbits,_ZN2v88internal6torque11ReportErrorIJRA14_KcRNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA27_S3_EEEvDpOT_,comdat
	.p2align 4
	.weak	_ZN2v88internal6torque11ReportErrorIJRA14_KcRNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA27_S3_EEEvDpOT_
	.type	_ZN2v88internal6torque11ReportErrorIJRA14_KcRNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA27_S3_EEEvDpOT_, @function
_ZN2v88internal6torque11ReportErrorIJRA14_KcRNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA27_S3_EEEvDpOT_:
.LFB8018:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA8018
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %rcx
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-96(%rbp), %r12
	movq	%r12, %rdi
	subq	$80, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
.LEHB29:
	call	_ZN2v88internal6torqueL7MessageIJRA17_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA37_S3_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
.LEHE29:
	movq	%r12, %rdi
.LEHB30:
	call	_ZNK2v88internal6torque14MessageBuilder5ThrowEv@PLT
.LEHE30:
.L331:
	endbr64
	movq	%rax, %r13
.L329:
	movq	%r12, %rdi
	call	_ZN2v88internal6torque14MessageBuilderD1Ev
	movq	%r13, %rdi
.LEHB31:
	call	_Unwind_Resume@PLT
.LEHE31:
	.cfi_endproc
.LFE8018:
	.section	.gcc_except_table._ZN2v88internal6torque11ReportErrorIJRA14_KcRNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA27_S3_EEEvDpOT_,"aG",@progbits,_ZN2v88internal6torque11ReportErrorIJRA14_KcRNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA27_S3_EEEvDpOT_,comdat
.LLSDA8018:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE8018-.LLSDACSB8018
.LLSDACSB8018:
	.uleb128 .LEHB29-.LFB8018
	.uleb128 .LEHE29-.LEHB29
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB30-.LFB8018
	.uleb128 .LEHE30-.LEHB30
	.uleb128 .L331-.LFB8018
	.uleb128 0
	.uleb128 .LEHB31-.LFB8018
	.uleb128 .LEHE31-.LEHB31
	.uleb128 0
	.uleb128 0
.LLSDACSE8018:
	.section	.text._ZN2v88internal6torque11ReportErrorIJRA14_KcRNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA27_S3_EEEvDpOT_,"axG",@progbits,_ZN2v88internal6torque11ReportErrorIJRA14_KcRNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA27_S3_EEEvDpOT_,comdat
	.size	_ZN2v88internal6torque11ReportErrorIJRA14_KcRNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA27_S3_EEEvDpOT_, .-_ZN2v88internal6torque11ReportErrorIJRA14_KcRNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA27_S3_EEEvDpOT_
	.section	.text._ZN2v88internal6torque11ReportErrorIJRA50_KcEEEvDpOT_,"axG",@progbits,_ZN2v88internal6torque11ReportErrorIJRA50_KcEEEvDpOT_,comdat
	.p2align 4
	.weak	_ZN2v88internal6torque11ReportErrorIJRA50_KcEEEvDpOT_
	.type	_ZN2v88internal6torque11ReportErrorIJRA50_KcEEEvDpOT_, @function
_ZN2v88internal6torque11ReportErrorIJRA50_KcEEEvDpOT_:
.LFB8019:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA8019
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-96(%rbp), %r12
	movq	%r12, %rdi
	subq	$80, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
.LEHB32:
	call	_ZN2v88internal6torqueL7MessageIJRA55_KcEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
.LEHE32:
	movq	%r12, %rdi
.LEHB33:
	call	_ZNK2v88internal6torque14MessageBuilder5ThrowEv@PLT
.LEHE33:
.L336:
	endbr64
	movq	%rax, %r13
.L334:
	movq	%r12, %rdi
	call	_ZN2v88internal6torque14MessageBuilderD1Ev
	movq	%r13, %rdi
.LEHB34:
	call	_Unwind_Resume@PLT
.LEHE34:
	.cfi_endproc
.LFE8019:
	.section	.gcc_except_table._ZN2v88internal6torque11ReportErrorIJRA50_KcEEEvDpOT_,"aG",@progbits,_ZN2v88internal6torque11ReportErrorIJRA50_KcEEEvDpOT_,comdat
.LLSDA8019:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE8019-.LLSDACSB8019
.LLSDACSB8019:
	.uleb128 .LEHB32-.LFB8019
	.uleb128 .LEHE32-.LEHB32
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB33-.LFB8019
	.uleb128 .LEHE33-.LEHB33
	.uleb128 .L336-.LFB8019
	.uleb128 0
	.uleb128 .LEHB34-.LFB8019
	.uleb128 .LEHE34-.LEHB34
	.uleb128 0
	.uleb128 0
.LLSDACSE8019:
	.section	.text._ZN2v88internal6torque11ReportErrorIJRA50_KcEEEvDpOT_,"axG",@progbits,_ZN2v88internal6torque11ReportErrorIJRA50_KcEEEvDpOT_,comdat
	.size	_ZN2v88internal6torque11ReportErrorIJRA50_KcEEEvDpOT_, .-_ZN2v88internal6torque11ReportErrorIJRA50_KcEEEvDpOT_
	.section	.text._ZN2v88internal6torque11ReportErrorIJRA37_KcRKPNS1_10IdentifierERA39_S3_EEEvDpOT_,"axG",@progbits,_ZN2v88internal6torque11ReportErrorIJRA37_KcRKPNS1_10IdentifierERA39_S3_EEEvDpOT_,comdat
	.p2align 4
	.weak	_ZN2v88internal6torque11ReportErrorIJRA37_KcRKPNS1_10IdentifierERA39_S3_EEEvDpOT_
	.type	_ZN2v88internal6torque11ReportErrorIJRA37_KcRKPNS1_10IdentifierERA39_S3_EEEvDpOT_, @function
_ZN2v88internal6torque11ReportErrorIJRA37_KcRKPNS1_10IdentifierERA39_S3_EEEvDpOT_:
.LFB8047:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA8047
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-448(%rbp), %r15
	movq	%rdi, %r14
	pushq	%r13
	movq	%r15, %rdi
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-432(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$504, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
.LEHB35:
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEC1Ev@PLT
.LEHE35:
	movq	%r14, %rsi
	movq	%r12, %rdi
.LEHB36:
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	(%rbx), %rax
	movq	%r12, %rdi
	movq	40(%rax), %rdx
	movq	32(%rax), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	leaq	-544(%rbp), %r13
	leaq	-424(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNKSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEE3strEv@PLT
.LEHE36:
	leaq	-512(%rbp), %r12
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
.LEHB37:
	call	_ZN2v88internal6torque14MessageBuilderC1ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_13TorqueMessage4KindE@PLT
.LEHE37:
	movq	-544(%rbp), %rdi
	leaq	-528(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L339
	call	_ZdlPv@PLT
.L339:
	movq	%r15, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r12, %rdi
.LEHB38:
	call	_ZNK2v88internal6torque14MessageBuilder5ThrowEv@PLT
.LEHE38:
.L346:
	endbr64
	movq	%rax, %r12
	jmp	.L342
.L345:
	endbr64
	movq	%rax, %r13
	jmp	.L343
.L347:
	endbr64
	movq	%rax, %r12
.L340:
	movq	-544(%rbp), %rdi
	leaq	-528(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L342
	call	_ZdlPv@PLT
.L342:
	movq	%r15, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r12, %rdi
.LEHB39:
	call	_Unwind_Resume@PLT
.L343:
	movq	%r12, %rdi
	call	_ZN2v88internal6torque14MessageBuilderD1Ev
	movq	%r13, %rdi
	call	_Unwind_Resume@PLT
.LEHE39:
	.cfi_endproc
.LFE8047:
	.section	.gcc_except_table._ZN2v88internal6torque11ReportErrorIJRA37_KcRKPNS1_10IdentifierERA39_S3_EEEvDpOT_,"aG",@progbits,_ZN2v88internal6torque11ReportErrorIJRA37_KcRKPNS1_10IdentifierERA39_S3_EEEvDpOT_,comdat
.LLSDA8047:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE8047-.LLSDACSB8047
.LLSDACSB8047:
	.uleb128 .LEHB35-.LFB8047
	.uleb128 .LEHE35-.LEHB35
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB36-.LFB8047
	.uleb128 .LEHE36-.LEHB36
	.uleb128 .L346-.LFB8047
	.uleb128 0
	.uleb128 .LEHB37-.LFB8047
	.uleb128 .LEHE37-.LEHB37
	.uleb128 .L347-.LFB8047
	.uleb128 0
	.uleb128 .LEHB38-.LFB8047
	.uleb128 .LEHE38-.LEHB38
	.uleb128 .L345-.LFB8047
	.uleb128 0
	.uleb128 .LEHB39-.LFB8047
	.uleb128 .LEHE39-.LEHB39
	.uleb128 0
	.uleb128 0
.LLSDACSE8047:
	.section	.text._ZN2v88internal6torque11ReportErrorIJRA37_KcRKPNS1_10IdentifierERA39_S3_EEEvDpOT_,"axG",@progbits,_ZN2v88internal6torque11ReportErrorIJRA37_KcRKPNS1_10IdentifierERA39_S3_EEEvDpOT_,comdat
	.size	_ZN2v88internal6torque11ReportErrorIJRA37_KcRKPNS1_10IdentifierERA39_S3_EEEvDpOT_, .-_ZN2v88internal6torque11ReportErrorIJRA37_KcRKPNS1_10IdentifierERA39_S3_EEEvDpOT_
	.section	.text._ZN2v88internal6torque11ReportErrorIJRA7_KcRKPNS1_10IdentifierERA12_S3_RmRA9_S3_SC_RA15_S3_EEEvDpOT_,"axG",@progbits,_ZN2v88internal6torque11ReportErrorIJRA7_KcRKPNS1_10IdentifierERA12_S3_RmRA9_S3_SC_RA15_S3_EEEvDpOT_,comdat
	.p2align 4
	.weak	_ZN2v88internal6torque11ReportErrorIJRA7_KcRKPNS1_10IdentifierERA12_S3_RmRA9_S3_SC_RA15_S3_EEEvDpOT_
	.type	_ZN2v88internal6torque11ReportErrorIJRA7_KcRKPNS1_10IdentifierERA12_S3_RmRA9_S3_SC_RA15_S3_EEEvDpOT_, @function
_ZN2v88internal6torque11ReportErrorIJRA7_KcRKPNS1_10IdentifierERA12_S3_RmRA9_S3_SC_RA15_S3_EEEvDpOT_:
.LFB8081:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA8081
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-448(%rbp), %r15
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-432(%rbp), %r12
	pushq	%rbx
	subq	$536, %rsp
	.cfi_offset 3, -56
	movq	%rdi, -576(%rbp)
	movq	%r15, %rdi
	movq	16(%rbp), %rbx
	movq	%rsi, -552(%rbp)
	movq	%rcx, -560(%rbp)
	movq	%r9, -568(%rbp)
	movq	%fs:40, %rdx
	movq	%rdx, -56(%rbp)
	xorl	%edx, %edx
.LEHB40:
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEC1Ev@PLT
.LEHE40:
	movq	-576(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %rsi
.LEHB41:
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	-552(%rbp), %rax
	movq	%r12, %rdi
	movq	(%rax), %rax
	movq	40(%rax), %rdx
	movq	32(%rax), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	-560(%rbp), %rax
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	-568(%rbp), %rax
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	leaq	-544(%rbp), %r13
	leaq	-424(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNKSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEE3strEv@PLT
.LEHE41:
	leaq	-512(%rbp), %r12
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
.LEHB42:
	call	_ZN2v88internal6torque14MessageBuilderC1ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_13TorqueMessage4KindE@PLT
.LEHE42:
	movq	-544(%rbp), %rdi
	leaq	-528(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L350
	call	_ZdlPv@PLT
.L350:
	movq	%r15, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r12, %rdi
.LEHB43:
	call	_ZNK2v88internal6torque14MessageBuilder5ThrowEv@PLT
.LEHE43:
.L357:
	endbr64
	movq	%rax, %r12
	jmp	.L353
.L356:
	endbr64
	movq	%rax, %r13
	jmp	.L354
.L358:
	endbr64
	movq	%rax, %r12
.L351:
	movq	-544(%rbp), %rdi
	leaq	-528(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L353
	call	_ZdlPv@PLT
.L353:
	movq	%r15, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r12, %rdi
.LEHB44:
	call	_Unwind_Resume@PLT
.L354:
	movq	%r12, %rdi
	call	_ZN2v88internal6torque14MessageBuilderD1Ev
	movq	%r13, %rdi
	call	_Unwind_Resume@PLT
.LEHE44:
	.cfi_endproc
.LFE8081:
	.section	.gcc_except_table._ZN2v88internal6torque11ReportErrorIJRA7_KcRKPNS1_10IdentifierERA12_S3_RmRA9_S3_SC_RA15_S3_EEEvDpOT_,"aG",@progbits,_ZN2v88internal6torque11ReportErrorIJRA7_KcRKPNS1_10IdentifierERA12_S3_RmRA9_S3_SC_RA15_S3_EEEvDpOT_,comdat
.LLSDA8081:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE8081-.LLSDACSB8081
.LLSDACSB8081:
	.uleb128 .LEHB40-.LFB8081
	.uleb128 .LEHE40-.LEHB40
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB41-.LFB8081
	.uleb128 .LEHE41-.LEHB41
	.uleb128 .L357-.LFB8081
	.uleb128 0
	.uleb128 .LEHB42-.LFB8081
	.uleb128 .LEHE42-.LEHB42
	.uleb128 .L358-.LFB8081
	.uleb128 0
	.uleb128 .LEHB43-.LFB8081
	.uleb128 .LEHE43-.LEHB43
	.uleb128 .L356-.LFB8081
	.uleb128 0
	.uleb128 .LEHB44-.LFB8081
	.uleb128 .LEHE44-.LEHB44
	.uleb128 0
	.uleb128 0
.LLSDACSE8081:
	.section	.text._ZN2v88internal6torque11ReportErrorIJRA7_KcRKPNS1_10IdentifierERA12_S3_RmRA9_S3_SC_RA15_S3_EEEvDpOT_,"axG",@progbits,_ZN2v88internal6torque11ReportErrorIJRA7_KcRKPNS1_10IdentifierERA12_S3_RmRA9_S3_SC_RA15_S3_EEEvDpOT_,comdat
	.size	_ZN2v88internal6torque11ReportErrorIJRA7_KcRKPNS1_10IdentifierERA12_S3_RmRA9_S3_SC_RA15_S3_EEEvDpOT_, .-_ZN2v88internal6torque11ReportErrorIJRA7_KcRKPNS1_10IdentifierERA12_S3_RmRA9_S3_SC_RA15_S3_EEEvDpOT_
	.section	.text._ZN2v88internal6torque11ReportErrorIJRKNS1_4TypeERA36_KcEEEvDpOT_,"axG",@progbits,_ZN2v88internal6torque11ReportErrorIJRKNS1_4TypeERA36_KcEEEvDpOT_,comdat
	.p2align 4
	.weak	_ZN2v88internal6torque11ReportErrorIJRKNS1_4TypeERA36_KcEEEvDpOT_
	.type	_ZN2v88internal6torque11ReportErrorIJRKNS1_4TypeERA36_KcEEEvDpOT_, @function
_ZN2v88internal6torque11ReportErrorIJRKNS1_4TypeERA36_KcEEEvDpOT_:
.LFB8086:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA8086
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-432(%rbp), %r14
	leaq	-416(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%r14, %rdi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	subq	$496, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
.LEHB45:
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEC1Ev@PLT
.LEHE45:
	movq	%r13, %rsi
	movq	%r15, %rdi
.LEHB46:
	call	_ZN2v88internal6torquelsERSoRKNS1_4TypeE
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	leaq	-528(%rbp), %r13
	leaq	-408(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNKSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEE3strEv@PLT
.LEHE46:
	leaq	-496(%rbp), %r12
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
.LEHB47:
	call	_ZN2v88internal6torque14MessageBuilderC1ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_13TorqueMessage4KindE@PLT
.LEHE47:
	movq	-528(%rbp), %rdi
	leaq	-512(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L361
	call	_ZdlPv@PLT
.L361:
	movq	%r14, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r12, %rdi
.LEHB48:
	call	_ZNK2v88internal6torque14MessageBuilder5ThrowEv@PLT
.LEHE48:
.L368:
	endbr64
	movq	%rax, %r12
	jmp	.L364
.L367:
	endbr64
	movq	%rax, %r13
	jmp	.L365
.L369:
	endbr64
	movq	%rax, %r12
.L362:
	movq	-528(%rbp), %rdi
	leaq	-512(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L364
	call	_ZdlPv@PLT
.L364:
	movq	%r14, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r12, %rdi
.LEHB49:
	call	_Unwind_Resume@PLT
.L365:
	movq	%r12, %rdi
	call	_ZN2v88internal6torque14MessageBuilderD1Ev
	movq	%r13, %rdi
	call	_Unwind_Resume@PLT
.LEHE49:
	.cfi_endproc
.LFE8086:
	.section	.gcc_except_table._ZN2v88internal6torque11ReportErrorIJRKNS1_4TypeERA36_KcEEEvDpOT_,"aG",@progbits,_ZN2v88internal6torque11ReportErrorIJRKNS1_4TypeERA36_KcEEEvDpOT_,comdat
.LLSDA8086:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE8086-.LLSDACSB8086
.LLSDACSB8086:
	.uleb128 .LEHB45-.LFB8086
	.uleb128 .LEHE45-.LEHB45
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB46-.LFB8086
	.uleb128 .LEHE46-.LEHB46
	.uleb128 .L368-.LFB8086
	.uleb128 0
	.uleb128 .LEHB47-.LFB8086
	.uleb128 .LEHE47-.LEHB47
	.uleb128 .L369-.LFB8086
	.uleb128 0
	.uleb128 .LEHB48-.LFB8086
	.uleb128 .LEHE48-.LEHB48
	.uleb128 .L367-.LFB8086
	.uleb128 0
	.uleb128 .LEHB49-.LFB8086
	.uleb128 .LEHE49-.LEHB49
	.uleb128 0
	.uleb128 0
.LLSDACSE8086:
	.section	.text._ZN2v88internal6torque11ReportErrorIJRKNS1_4TypeERA36_KcEEEvDpOT_,"axG",@progbits,_ZN2v88internal6torque11ReportErrorIJRKNS1_4TypeERA36_KcEEEvDpOT_,comdat
	.size	_ZN2v88internal6torque11ReportErrorIJRKNS1_4TypeERA36_KcEEEvDpOT_, .-_ZN2v88internal6torque11ReportErrorIJRKNS1_4TypeERA36_KcEEEvDpOT_
	.section	.text._ZN2v88internal6torque11ReportErrorIJRA43_KcRNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA18_S3_PS3_EEEvDpOT_,"axG",@progbits,_ZN2v88internal6torque11ReportErrorIJRA43_KcRNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA18_S3_PS3_EEEvDpOT_,comdat
	.p2align 4
	.weak	_ZN2v88internal6torque11ReportErrorIJRA43_KcRNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA18_S3_PS3_EEEvDpOT_
	.type	_ZN2v88internal6torque11ReportErrorIJRA43_KcRNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA18_S3_PS3_EEEvDpOT_, @function
_ZN2v88internal6torque11ReportErrorIJRA43_KcRNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA18_S3_PS3_EEEvDpOT_:
.LFB8110:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA8110
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-448(%rbp), %r15
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-432(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$520, %rsp
	movq	%rdi, -552(%rbp)
	movq	%r15, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
.LEHB50:
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEC1Ev@PLT
.LEHE50:
	movq	-552(%rbp), %r8
	movq	%r12, %rdi
	movq	%r8, %rsi
.LEHB51:
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	8(%rbx), %rdx
	movq	(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	0(%r13), %rsi
	movq	%r12, %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	leaq	-544(%rbp), %r13
	leaq	-424(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNKSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEE3strEv@PLT
.LEHE51:
	leaq	-512(%rbp), %r12
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
.LEHB52:
	call	_ZN2v88internal6torque14MessageBuilderC1ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_13TorqueMessage4KindE@PLT
.LEHE52:
	movq	-544(%rbp), %rdi
	leaq	-528(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L372
	call	_ZdlPv@PLT
.L372:
	movq	%r15, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r12, %rdi
.LEHB53:
	call	_ZNK2v88internal6torque14MessageBuilder5ThrowEv@PLT
.LEHE53:
.L379:
	endbr64
	movq	%rax, %r12
	jmp	.L375
.L378:
	endbr64
	movq	%rax, %r13
	jmp	.L376
.L380:
	endbr64
	movq	%rax, %r12
.L373:
	movq	-544(%rbp), %rdi
	leaq	-528(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L375
	call	_ZdlPv@PLT
.L375:
	movq	%r15, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r12, %rdi
.LEHB54:
	call	_Unwind_Resume@PLT
.L376:
	movq	%r12, %rdi
	call	_ZN2v88internal6torque14MessageBuilderD1Ev
	movq	%r13, %rdi
	call	_Unwind_Resume@PLT
.LEHE54:
	.cfi_endproc
.LFE8110:
	.section	.gcc_except_table._ZN2v88internal6torque11ReportErrorIJRA43_KcRNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA18_S3_PS3_EEEvDpOT_,"aG",@progbits,_ZN2v88internal6torque11ReportErrorIJRA43_KcRNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA18_S3_PS3_EEEvDpOT_,comdat
.LLSDA8110:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE8110-.LLSDACSB8110
.LLSDACSB8110:
	.uleb128 .LEHB50-.LFB8110
	.uleb128 .LEHE50-.LEHB50
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB51-.LFB8110
	.uleb128 .LEHE51-.LEHB51
	.uleb128 .L379-.LFB8110
	.uleb128 0
	.uleb128 .LEHB52-.LFB8110
	.uleb128 .LEHE52-.LEHB52
	.uleb128 .L380-.LFB8110
	.uleb128 0
	.uleb128 .LEHB53-.LFB8110
	.uleb128 .LEHE53-.LEHB53
	.uleb128 .L378-.LFB8110
	.uleb128 0
	.uleb128 .LEHB54-.LFB8110
	.uleb128 .LEHE54-.LEHB54
	.uleb128 0
	.uleb128 0
.LLSDACSE8110:
	.section	.text._ZN2v88internal6torque11ReportErrorIJRA43_KcRNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA18_S3_PS3_EEEvDpOT_,"axG",@progbits,_ZN2v88internal6torque11ReportErrorIJRA43_KcRNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA18_S3_PS3_EEEvDpOT_,comdat
	.size	_ZN2v88internal6torque11ReportErrorIJRA43_KcRNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA18_S3_PS3_EEEvDpOT_, .-_ZN2v88internal6torque11ReportErrorIJRA43_KcRNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA18_S3_PS3_EEEvDpOT_
	.section	.text._ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E,"axG",@progbits,_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	.type	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E, @function
_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E:
.LFB8529:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L397
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
.L386:
	movq	24(%r12), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r12), %rdi
	leaq	48(%r12), %rax
	movq	16(%r12), %rbx
	cmpq	%rax, %rdi
	je	.L384
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L382
.L385:
	movq	%rbx, %r12
	jmp	.L386
	.p2align 4,,10
	.p2align 3
.L384:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L385
.L382:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L397:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.cfi_endproc
.LFE8529:
	.size	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E, .-_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	.section	.text._ZN2v88internal6torque13AggregateTypeD0Ev,"axG",@progbits,_ZN2v88internal6torque13AggregateTypeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque13AggregateTypeD0Ev
	.type	_ZN2v88internal6torque13AggregateTypeD0Ev, @function
_ZN2v88internal6torque13AggregateTypeD0Ev:
.LFB5645:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal6torque13AggregateTypeE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rax, (%rdi)
	movq	144(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L401
	call	_ZdlPv@PLT
.L401:
	movq	112(%r12), %rdi
	leaq	128(%r12), %rax
	cmpq	%rax, %rdi
	je	.L402
	call	_ZdlPv@PLT
.L402:
	movq	88(%r12), %rbx
	movq	80(%r12), %r13
	cmpq	%r13, %rbx
	je	.L403
	.p2align 4,,10
	.p2align 3
.L407:
	movq	48(%r13), %rdi
	leaq	64(%r13), %rax
	cmpq	%rax, %rdi
	je	.L404
	call	_ZdlPv@PLT
	addq	$104, %r13
	cmpq	%r13, %rbx
	jne	.L407
.L405:
	movq	80(%r12), %r13
.L403:
	testq	%r13, %r13
	je	.L408
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L408:
	movq	40(%r12), %r13
	leaq	16+_ZTVN2v88internal6torque4TypeE(%rip), %rax
	leaq	24(%r12), %r14
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L411
.L409:
	movq	24(%r13), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r13), %rdi
	leaq	48(%r13), %rax
	movq	16(%r13), %rbx
	cmpq	%rax, %rdi
	je	.L410
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L411
.L412:
	movq	%rbx, %r13
	jmp	.L409
	.p2align 4,,10
	.p2align 3
.L404:
	addq	$104, %r13
	cmpq	%r13, %rbx
	jne	.L407
	jmp	.L405
	.p2align 4,,10
	.p2align 3
.L410:
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L412
.L411:
	popq	%rbx
	movq	%r12, %rdi
	movl	$168, %esi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE5645:
	.size	_ZN2v88internal6torque13AggregateTypeD0Ev, .-_ZN2v88internal6torque13AggregateTypeD0Ev
	.section	.text._ZN2v88internal6torque13AggregateTypeD2Ev,"axG",@progbits,_ZN2v88internal6torque13AggregateTypeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque13AggregateTypeD2Ev
	.type	_ZN2v88internal6torque13AggregateTypeD2Ev, @function
_ZN2v88internal6torque13AggregateTypeD2Ev:
.LFB5643:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal6torque13AggregateTypeE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	144(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L428
	call	_ZdlPv@PLT
.L428:
	movq	112(%rbx), %rdi
	leaq	128(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L429
	call	_ZdlPv@PLT
.L429:
	movq	88(%rbx), %r13
	movq	80(%rbx), %r12
	cmpq	%r12, %r13
	je	.L430
	.p2align 4,,10
	.p2align 3
.L434:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L431
	call	_ZdlPv@PLT
	addq	$104, %r12
	cmpq	%r12, %r13
	jne	.L434
.L432:
	movq	80(%rbx), %r12
.L430:
	testq	%r12, %r12
	je	.L435
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L435:
	movq	40(%rbx), %r12
	leaq	16+_ZTVN2v88internal6torque4TypeE(%rip), %rax
	leaq	24(%rbx), %r13
	movq	%rax, (%rbx)
	testq	%r12, %r12
	je	.L427
.L436:
	movq	24(%r12), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r12), %rdi
	leaq	48(%r12), %rax
	movq	16(%r12), %rbx
	cmpq	%rax, %rdi
	je	.L437
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L427
.L439:
	movq	%rbx, %r12
	jmp	.L436
	.p2align 4,,10
	.p2align 3
.L431:
	addq	$104, %r12
	cmpq	%r12, %r13
	jne	.L434
	jmp	.L432
	.p2align 4,,10
	.p2align 3
.L437:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L439
.L427:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5643:
	.size	_ZN2v88internal6torque13AggregateTypeD2Ev, .-_ZN2v88internal6torque13AggregateTypeD2Ev
	.weak	_ZN2v88internal6torque13AggregateTypeD1Ev
	.set	_ZN2v88internal6torque13AggregateTypeD1Ev,_ZN2v88internal6torque13AggregateTypeD2Ev
	.section	.text._ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE16_M_insert_uniqueIRKS5_EESt4pairISt17_Rb_tree_iteratorIS5_EbEOT_,"axG",@progbits,_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE16_M_insert_uniqueIRKS5_EESt4pairISt17_Rb_tree_iteratorIS5_EbEOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE16_M_insert_uniqueIRKS5_EESt4pairISt17_Rb_tree_iteratorIS5_EbEOT_
	.type	_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE16_M_insert_uniqueIRKS5_EESt4pairISt17_Rb_tree_iteratorIS5_EbEOT_, @function
_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE16_M_insert_uniqueIRKS5_EESt4pairISt17_Rb_tree_iteratorIS5_EbEOT_:
.LFB8578:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	8(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r12
	testq	%r12, %r12
	jne	.L456
	jmp	.L472
	.p2align 4,,10
	.p2align 3
.L473:
	movq	16(%r12), %rdx
	testq	%rdx, %rdx
	je	.L457
.L474:
	movq	%rdx, %r12
.L456:
	movq	32(%r12), %rsi
	movq	(%r14), %rdi
	call	_ZN2v88internal6torqueltERKNS1_4TypeES4_@PLT
	testb	%al, %al
	jne	.L473
	movq	24(%r12), %rdx
	testq	%rdx, %rdx
	jne	.L474
.L457:
	testb	%al, %al
	je	.L475
	cmpq	24(%r13), %r12
	je	.L464
.L466:
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	(%r14), %rsi
	movq	32(%rax), %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal6torqueltERKNS1_4TypeES4_@PLT
	testb	%al, %al
	jne	.L464
.L465:
	addq	$24, %rsp
	movq	%rbx, %rax
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L475:
	.cfi_restore_state
	movq	32(%r12), %rdi
	movq	(%r14), %rsi
	movq	%r12, %rbx
	call	_ZN2v88internal6torqueltERKNS1_4TypeES4_@PLT
	testb	%al, %al
	je	.L465
.L464:
	movl	$1, %r8d
	cmpq	%r15, %r12
	jne	.L476
.L462:
	movl	$40, %edi
	movl	%r8d, -52(%rbp)
	call	_Znwm@PLT
	movl	-52(%rbp), %r8d
	movq	%r12, %rdx
	movq	%r15, %rcx
	movq	%rax, %rbx
	movq	(%r14), %rax
	movq	%rbx, %rsi
	movl	%r8d, %edi
	movq	%rax, 32(%rbx)
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 40(%r13)
	addq	$24, %rsp
	movq	%rbx, %rax
	movl	$1, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L476:
	.cfi_restore_state
	movq	32(%r12), %rsi
	movq	(%r14), %rdi
	call	_ZN2v88internal6torqueltERKNS1_4TypeES4_@PLT
	movzbl	%al, %r8d
	jmp	.L462
	.p2align 4,,10
	.p2align 3
.L472:
	movq	%r15, %r12
	cmpq	24(%rdi), %r15
	jne	.L466
	movl	$1, %r8d
	jmp	.L462
	.cfi_endproc
.LFE8578:
	.size	_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE16_M_insert_uniqueIRKS5_EESt4pairISt17_Rb_tree_iteratorIS5_EbEOT_, .-_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE16_M_insert_uniqueIRKS5_EESt4pairISt17_Rb_tree_iteratorIS5_EbEOT_
	.section	.text._ZN2v88internal6torque9UnionType6ExtendEPKNS1_4TypeE,"axG",@progbits,_ZN2v88internal6torque9UnionType6ExtendEPKNS1_4TypeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque9UnionType6ExtendEPKNS1_4TypeE
	.type	_ZN2v88internal6torque9UnionType6ExtendEPKNS1_4TypeE, @function
_ZN2v88internal6torque9UnionType6ExtendEPKNS1_4TypeE:
.LFB5524:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	movq	%rsi, %rdi
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rsi, -40(%rbp)
	testq	%rsi, %rsi
	je	.L478
	cmpl	$3, 8(%rsi)
	jne	.L478
	movq	96(%rsi), %r12
	leaq	80(%rsi), %rbx
	cmpq	%rbx, %r12
	je	.L477
.L479:
	movq	32(%r12), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal6torque9UnionType6ExtendEPKNS1_4TypeE
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r12
	cmpq	%rax, %rbx
	jne	.L479
.L477:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L478:
	.cfi_restore_state
	movq	(%rdi), %rax
	movq	%r13, %rsi
	call	*16(%rax)
	testb	%al, %al
	jne	.L477
	movq	16(%r13), %rdi
	movq	-40(%rbp), %rsi
	leaq	80(%r13), %rbx
	call	_ZN2v88internal6torque4Type15CommonSupertypeEPKS2_S4_@PLT
	movq	96(%r13), %r12
	movq	%rax, 16(%r13)
	cmpq	%rbx, %r12
	je	.L483
	.p2align 4,,10
	.p2align 3
.L482:
	movq	32(%r12), %rdi
	movq	-40(%rbp), %rsi
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	%r12, %rdi
	testb	%al, %al
	jne	.L494
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r12
	cmpq	%rax, %rbx
	jne	.L482
.L483:
	leaq	-40(%rbp), %rsi
	leaq	72(%r13), %rdi
	call	_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE16_M_insert_uniqueIRKS5_EESt4pairISt17_Rb_tree_iteratorIS5_EbEOT_
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L494:
	.cfi_restore_state
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZSt28_Rb_tree_rebalance_for_erasePSt18_Rb_tree_node_baseRS_@PLT
	movq	%rax, %rdi
	call	_ZdlPv@PLT
	subq	$1, 112(%r13)
	cmpq	%r14, %rbx
	je	.L483
	movq	%r14, %r12
	jmp	.L482
	.cfi_endproc
.LFE5524:
	.size	_ZN2v88internal6torque9UnionType6ExtendEPKNS1_4TypeE, .-_ZN2v88internal6torque9UnionType6ExtendEPKNS1_4TypeE
	.section	.text._ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E,"axG",@progbits,_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	.type	_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E, @function
_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E:
.LFB8603:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L503
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
.L497:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	%rbx, %rdi
	movq	16(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L497
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L503:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE8603:
	.size	_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E, .-_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	.section	.rodata._ZNSt6vectorIN2v88internal6torque5FieldESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_.str1.1,"aMS",@progbits,1
.LC15:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorIN2v88internal6torque5FieldESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal6torque5FieldESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal6torque5FieldESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.type	_ZNSt6vectorIN2v88internal6torque5FieldESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, @function
_ZNSt6vectorIN2v88internal6torque5FieldESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_:
.LFB8640:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA8640
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	movabsq	$5675921253449092805, %rdx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$72, %rsp
	movq	%rdi, -104(%rbp)
	movq	8(%rdi), %r14
	movq	%r14, %rax
	movq	%fs:40, %rsi
	movq	%rsi, -56(%rbp)
	xorl	%esi, %esi
	movq	(%rdi), %rsi
	subq	%rsi, %rax
	movq	%rsi, -80(%rbp)
	sarq	$3, %rax
	imulq	%rdx, %rax
	movabsq	$88686269585142075, %rdx
	cmpq	%rdx, %rax
	je	.L554
	movq	%rbx, %r12
	subq	-80(%rbp), %r12
	testq	%rax, %rax
	je	.L534
	leaq	(%rax,%rax), %rsi
	movq	%rsi, -88(%rbp)
	cmpq	%rsi, %rax
	jbe	.L555
	movabsq	$9223372036854775800, %rax
	movq	%rax, -88(%rbp)
.L508:
	movq	-88(%rbp), %rdi
.LEHB55:
	call	_Znwm@PLT
.LEHE55:
	movq	%rax, -72(%rbp)
.L532:
	movl	16(%r13), %eax
	addq	-72(%rbp), %r12
	movdqu	0(%r13), %xmm7
	movq	56(%r13), %r15
	movl	%eax, 16(%r12)
	movq	24(%r13), %rax
	movups	%xmm7, (%r12)
	movdqu	32(%r13), %xmm7
	movq	%rax, 24(%r12)
	leaq	64(%r12), %rax
	movq	%rax, -96(%rbp)
	movq	%rax, 48(%r12)
	movq	48(%r13), %rax
	movups	%xmm7, 32(%r12)
	movq	%rax, %rsi
	movq	%rax, -112(%rbp)
	addq	%r15, %rsi
	je	.L510
	testq	%rax, %rax
	je	.L556
.L510:
	movq	%r15, -64(%rbp)
	cmpq	$15, %r15
	ja	.L557
	cmpq	$1, %r15
	jne	.L513
	movq	-112(%rbp), %rax
	movzbl	(%rax), %eax
	movb	%al, 64(%r12)
.L514:
	movq	-96(%rbp), %rax
	movq	%r15, 56(%r12)
	movb	$0, (%rax,%r15)
	movq	88(%r13), %rdx
	movzbl	98(%r13), %eax
	movq	80(%r13), %rcx
	movq	%rdx, 88(%r12)
	movzwl	96(%r13), %edx
	movb	%al, 98(%r12)
	movq	-80(%rbp), %rax
	movq	%rcx, 80(%r12)
	movw	%dx, 96(%r12)
	cmpq	%rax, %rbx
	je	.L536
	movq	-72(%rbp), %r13
	leaq	64(%rax), %r12
	jmp	.L521
	.p2align 4,,10
	.p2align 3
.L516:
	movq	%rax, 48(%r13)
	movq	(%r12), %rax
	movq	%rax, 64(%r13)
.L517:
	movq	-8(%r12), %rax
	movq	%rax, 56(%r13)
	movq	16(%r12), %rax
	movq	%r12, -16(%r12)
	movq	$0, -8(%r12)
	movb	$0, (%r12)
	movq	%rax, 80(%r13)
	movq	24(%r12), %rax
	movq	%rax, 88(%r13)
	movzbl	32(%r12), %eax
	movb	%al, 96(%r13)
	movzbl	33(%r12), %eax
	movb	%al, 97(%r13)
	movzbl	34(%r12), %eax
	movb	%al, 98(%r13)
	movq	-16(%r12), %rdi
	cmpq	%rdi, %r12
	je	.L518
	call	_ZdlPv@PLT
.L518:
	leaq	104(%r12), %rax
	addq	$40, %r12
	addq	$104, %r13
	cmpq	%r12, %rbx
	je	.L519
	movq	%rax, %r12
.L521:
	movdqu	-64(%r12), %xmm1
	movups	%xmm1, 0(%r13)
	movl	-48(%r12), %eax
	movl	%eax, 16(%r13)
	movq	-40(%r12), %rax
	movq	%rax, 24(%r13)
	movdqu	-32(%r12), %xmm2
	leaq	64(%r13), %rax
	movq	%rax, 48(%r13)
	movups	%xmm2, 32(%r13)
	movq	-16(%r12), %rax
	cmpq	%rax, %r12
	jne	.L516
	movdqu	(%r12), %xmm5
	movups	%xmm5, 64(%r13)
	jmp	.L517
	.p2align 4,,10
	.p2align 3
.L555:
	testq	%rsi, %rsi
	jne	.L509
	movq	$0, -72(%rbp)
	jmp	.L532
	.p2align 4,,10
	.p2align 3
.L519:
	leaq	-104(%rbx), %rax
	subq	-80(%rbp), %rax
	movq	-72(%rbp), %rdi
	movabsq	$1064235235021704901, %rdx
	shrq	$3, %rax
	imulq	%rdx, %rax
	movabsq	$2305843009213693951, %rdx
	andq	%rdx, %rax
	addq	$1, %rax
	leaq	(%rax,%rax,2), %rdx
	leaq	(%rax,%rdx,4), %rax
	leaq	(%rdi,%rax,8), %r12
.L515:
	addq	$104, %r12
	cmpq	%r14, %rbx
	je	.L522
	movq	%rbx, %rax
	movq	%r12, %rdx
	jmp	.L525
	.p2align 4,,10
	.p2align 3
.L523:
	movq	%rcx, 48(%rdx)
	movq	64(%rax), %rcx
	movq	%rcx, 64(%rdx)
.L524:
	movq	56(%rax), %rcx
	addq	$104, %rax
	addq	$104, %rdx
	movq	%rcx, -48(%rdx)
	movq	-24(%rax), %rcx
	movq	%rcx, -24(%rdx)
	movq	-16(%rax), %rcx
	movq	%rcx, -16(%rdx)
	movzbl	-8(%rax), %ecx
	movb	%cl, -8(%rdx)
	movzbl	-7(%rax), %ecx
	movb	%cl, -7(%rdx)
	movzbl	-6(%rax), %ecx
	movb	%cl, -6(%rdx)
	cmpq	%r14, %rax
	je	.L558
.L525:
	movl	16(%rax), %ecx
	movdqu	(%rax), %xmm3
	leaq	64(%rax), %rsi
	movdqu	32(%rax), %xmm4
	movl	%ecx, 16(%rdx)
	movq	24(%rax), %rcx
	movups	%xmm3, (%rdx)
	movq	%rcx, 24(%rdx)
	leaq	64(%rdx), %rcx
	movq	%rcx, 48(%rdx)
	movq	48(%rax), %rcx
	movups	%xmm4, 32(%rdx)
	cmpq	%rsi, %rcx
	jne	.L523
	movdqu	64(%rax), %xmm6
	movups	%xmm6, 64(%rdx)
	jmp	.L524
	.p2align 4,,10
	.p2align 3
.L558:
	movabsq	$1064235235021704901, %rdx
	subq	%rbx, %rax
	subq	$104, %rax
	shrq	$3, %rax
	imulq	%rdx, %rax
	movabsq	$2305843009213693951, %rdx
	andq	%rdx, %rax
	addq	$1, %rax
	leaq	(%rax,%rax,2), %rdx
	leaq	(%rax,%rdx,4), %rax
	leaq	(%r12,%rax,8), %r12
.L522:
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L526
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L526:
	movq	-72(%rbp), %rax
	movq	-104(%rbp), %rbx
	movq	%r12, %xmm7
	movq	%rax, %xmm0
	addq	-88(%rbp), %rax
	movq	%rax, 16(%rbx)
	punpcklqdq	%xmm7, %xmm0
	movups	%xmm0, (%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L559
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L534:
	.cfi_restore_state
	movq	$104, -88(%rbp)
	jmp	.L508
	.p2align 4,,10
	.p2align 3
.L513:
	testq	%r15, %r15
	je	.L514
	movq	-96(%rbp), %rdi
	jmp	.L512
	.p2align 4,,10
	.p2align 3
.L557:
	leaq	-64(%rbp), %rsi
	leaq	48(%r12), %rdi
	xorl	%edx, %edx
.LEHB56:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, 48(%r12)
	movq	%rax, %rdi
	movq	-64(%rbp), %rax
	movq	%rax, 64(%r12)
.L512:
	movq	-112(%rbp), %rsi
	movq	%r15, %rdx
	call	memcpy@PLT
	movq	48(%r12), %rax
	movq	-64(%rbp), %r15
	movq	%rax, -96(%rbp)
	jmp	.L514
	.p2align 4,,10
	.p2align 3
.L536:
	movq	-72(%rbp), %r12
	jmp	.L515
.L556:
	leaq	.LC4(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE56:
.L509:
	movq	-88(%rbp), %rax
	cmpq	%rdx, %rax
	cmovbe	%rax, %rdx
	imulq	$104, %rdx, %rax
	movq	%rax, -88(%rbp)
	jmp	.L508
.L559:
	call	__stack_chk_fail@PLT
.L554:
	leaq	.LC15(%rip), %rdi
.LEHB57:
	call	_ZSt20__throw_length_errorPKc@PLT
.LEHE57:
.L537:
	endbr64
	movq	%rax, %rdi
.L527:
	call	__cxa_begin_catch@PLT
	cmpq	$0, -72(%rbp)
	je	.L560
	movq	-72(%rbp), %rdi
	call	_ZdlPv@PLT
.L531:
.LEHB58:
	call	__cxa_rethrow@PLT
.LEHE58:
.L560:
	movq	48(%r12), %rdi
	cmpq	-96(%rbp), %rdi
	je	.L531
	call	_ZdlPv@PLT
	jmp	.L531
.L538:
	endbr64
	movq	%rax, %r12
.L530:
	call	__cxa_end_catch@PLT
	movq	%r12, %rdi
.LEHB59:
	call	_Unwind_Resume@PLT
.LEHE59:
	.cfi_endproc
.LFE8640:
	.section	.gcc_except_table._ZNSt6vectorIN2v88internal6torque5FieldESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,"aG",@progbits,_ZNSt6vectorIN2v88internal6torque5FieldESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,comdat
	.align 4
.LLSDA8640:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATT8640-.LLSDATTD8640
.LLSDATTD8640:
	.byte	0x1
	.uleb128 .LLSDACSE8640-.LLSDACSB8640
.LLSDACSB8640:
	.uleb128 .LEHB55-.LFB8640
	.uleb128 .LEHE55-.LEHB55
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB56-.LFB8640
	.uleb128 .LEHE56-.LEHB56
	.uleb128 .L537-.LFB8640
	.uleb128 0x1
	.uleb128 .LEHB57-.LFB8640
	.uleb128 .LEHE57-.LEHB57
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB58-.LFB8640
	.uleb128 .LEHE58-.LEHB58
	.uleb128 .L538-.LFB8640
	.uleb128 0
	.uleb128 .LEHB59-.LFB8640
	.uleb128 .LEHE59-.LEHB59
	.uleb128 0
	.uleb128 0
.LLSDACSE8640:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATT8640:
	.section	.text._ZNSt6vectorIN2v88internal6torque5FieldESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal6torque5FieldESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,comdat
	.size	_ZNSt6vectorIN2v88internal6torque5FieldESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, .-_ZNSt6vectorIN2v88internal6torque5FieldESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.section	.text._ZN2v88internal6torque13AggregateType13RegisterFieldENS1_5FieldE,"axG",@progbits,_ZN2v88internal6torque13AggregateType13RegisterFieldENS1_5FieldE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque13AggregateType13RegisterFieldENS1_5FieldE
	.type	_ZN2v88internal6torque13AggregateType13RegisterFieldENS1_5FieldE, @function
_ZN2v88internal6torque13AggregateType13RegisterFieldENS1_5FieldE:
.LFB5593:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	88(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	96(%rdi), %r12
	je	.L562
	movl	16(%rsi), %eax
	movdqu	(%rsi), %xmm0
	leaq	64(%r12), %rdi
	movdqu	32(%rsi), %xmm1
	movl	%eax, 16(%r12)
	movq	24(%rsi), %rax
	movups	%xmm0, (%r12)
	movq	%rax, 24(%r12)
	movq	%rdi, 48(%r12)
	movups	%xmm1, 32(%r12)
	movq	48(%rsi), %r15
	movq	56(%rsi), %r14
	movq	%r15, %rax
	addq	%r14, %rax
	je	.L563
	testq	%r15, %r15
	je	.L580
.L563:
	movq	%r14, -64(%rbp)
	cmpq	$15, %r14
	ja	.L581
	cmpq	$1, %r14
	jne	.L566
	movzbl	(%r15), %eax
	movb	%al, 64(%r12)
.L567:
	movq	%r14, 56(%r12)
	movb	$0, (%rdi,%r14)
	movq	80(%r13), %rax
	movq	%rax, 80(%r12)
	movq	88(%r13), %rdx
	movzbl	98(%r13), %eax
	movq	%rdx, 88(%r12)
	movzwl	96(%r13), %edx
	movb	%al, 98(%r12)
	movw	%dx, 96(%r12)
	movq	88(%rbx), %rax
	leaq	104(%rax), %rdx
	movq	%rdx, 88(%rbx)
.L561:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L582
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L581:
	.cfi_restore_state
	leaq	48(%r12), %rdi
	leaq	-64(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, 48(%r12)
	movq	%rax, %rdi
	movq	-64(%rbp), %rax
	movq	%rax, 64(%r12)
.L565:
	movq	%r14, %rdx
	movq	%r15, %rsi
	call	memcpy@PLT
	movq	-64(%rbp), %r14
	movq	48(%r12), %rdi
	jmp	.L567
	.p2align 4,,10
	.p2align 3
.L562:
	movq	%rsi, %rdx
	leaq	80(%rdi), %rdi
	movq	%r12, %rsi
	call	_ZNSt6vectorIN2v88internal6torque5FieldESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	movq	88(%rbx), %rax
	subq	$104, %rax
	jmp	.L561
	.p2align 4,,10
	.p2align 3
.L566:
	testq	%r14, %r14
	je	.L567
	jmp	.L565
.L580:
	leaq	.LC4(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L582:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5593:
	.size	_ZN2v88internal6torque13AggregateType13RegisterFieldENS1_5FieldE, .-_ZN2v88internal6torque13AggregateType13RegisterFieldENS1_5FieldE
	.section	.text._ZNSt6vectorIPKN2v88internal6torque9TypeAliasESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_,"axG",@progbits,_ZNSt6vectorIPKN2v88internal6torque9TypeAliasESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPKN2v88internal6torque9TypeAliasESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	.type	_ZNSt6vectorIPKN2v88internal6torque9TypeAliasESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_, @function
_ZNSt6vectorIPKN2v88internal6torque9TypeAliasESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_:
.LFB8845:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r9
	movq	%r13, %rax
	subq	%r9, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L597
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L593
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L598
.L585:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L592:
	movq	(%r15), %rax
	subq	%r8, %r13
	leaq	8(%rbx,%rdx), %r15
	movq	%rax, (%rbx,%rdx)
	leaq	(%r15,%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L599
	testq	%r13, %r13
	jg	.L588
	testq	%r9, %r9
	jne	.L591
.L589:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L599:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jg	.L588
.L591:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L589
	.p2align 4,,10
	.p2align 3
.L598:
	testq	%rsi, %rsi
	jne	.L586
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L592
	.p2align 4,,10
	.p2align 3
.L588:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L589
	jmp	.L591
	.p2align 4,,10
	.p2align 3
.L593:
	movl	$8, %r14d
	jmp	.L585
.L597:
	leaq	.LC15(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L586:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$3, %r14
	jmp	.L585
	.cfi_endproc
.LFE8845:
	.size	_ZNSt6vectorIPKN2v88internal6torque9TypeAliasESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_, .-_ZNSt6vectorIPKN2v88internal6torque9TypeAliasESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	.section	.text._ZNSt6vectorIPKN2v88internal6torque18BuiltinPointerTypeESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_,"axG",@progbits,_ZNSt6vectorIPKN2v88internal6torque18BuiltinPointerTypeESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPKN2v88internal6torque18BuiltinPointerTypeESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	.type	_ZNSt6vectorIPKN2v88internal6torque18BuiltinPointerTypeESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_, @function
_ZNSt6vectorIPKN2v88internal6torque18BuiltinPointerTypeESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_:
.LFB8946:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r9
	movq	%r13, %rax
	subq	%r9, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L614
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L610
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L615
.L602:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L609:
	movq	(%r15), %rax
	subq	%r8, %r13
	leaq	8(%rbx,%rdx), %r15
	movq	%rax, (%rbx,%rdx)
	leaq	(%r15,%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L616
	testq	%r13, %r13
	jg	.L605
	testq	%r9, %r9
	jne	.L608
.L606:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L616:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jg	.L605
.L608:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L606
	.p2align 4,,10
	.p2align 3
.L615:
	testq	%rsi, %rsi
	jne	.L603
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L609
	.p2align 4,,10
	.p2align 3
.L605:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L606
	jmp	.L608
	.p2align 4,,10
	.p2align 3
.L610:
	movl	$8, %r14d
	jmp	.L602
.L614:
	leaq	.LC15(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L603:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$3, %r14
	jmp	.L602
	.cfi_endproc
.LFE8946:
	.size	_ZNSt6vectorIPKN2v88internal6torque18BuiltinPointerTypeESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_, .-_ZNSt6vectorIPKN2v88internal6torque18BuiltinPointerTypeESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	.section	.text._ZNSt6vectorIN2v88internal6torque16LabelDeclarationESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal6torque16LabelDeclarationESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal6torque16LabelDeclarationESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.type	_ZNSt6vectorIN2v88internal6torque16LabelDeclarationESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, @function
_ZNSt6vectorIN2v88internal6torque16LabelDeclarationESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_:
.LFB9289:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA9289
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	movabsq	$288230376151711743, %rdx
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rax
	movq	(%rdi), %r13
	movq	%rdi, -80(%rbp)
	movq	%rax, -72(%rbp)
	subq	%r13, %rax
	sarq	$5, %rax
	cmpq	%rdx, %rax
	je	.L654
	movq	%rsi, %r14
	movq	%rsi, %rbx
	subq	%r13, %r14
	testq	%rax, %rax
	je	.L638
	leaq	(%rax,%rax), %rdi
	movq	%rdi, -56(%rbp)
	cmpq	%rdi, %rax
	jbe	.L655
	movabsq	$9223372036854775776, %rax
	movq	%rax, -56(%rbp)
.L619:
	movq	-56(%rbp), %rdi
.LEHB60:
	call	_Znwm@PLT
.LEHE60:
	movq	%rax, %r12
.L637:
	movq	(%r15), %rax
	movq	16(%r15), %rdx
	addq	%r12, %r14
	movq	8(%r15), %rsi
	movq	$0, 8(%r14)
	movq	%rax, (%r14)
	movq	%rdx, %rax
	subq	%rsi, %rax
	movq	$0, 16(%r14)
	movq	%rax, -64(%rbp)
	sarq	$3, %rax
	movq	$0, 24(%r14)
	je	.L656
	movabsq	$1152921504606846975, %rdx
	cmpq	%rdx, %rax
	ja	.L657
	movq	-64(%rbp), %rdi
.LEHB61:
	call	_Znwm@PLT
	movq	16(%r15), %rdx
	movq	8(%r15), %rsi
	movq	%rax, %rcx
	movq	%rdx, %r15
	subq	%rsi, %r15
.L622:
	movq	-64(%rbp), %rax
	movq	%rcx, %xmm0
	punpcklqdq	%xmm0, %xmm0
	addq	%rcx, %rax
	movups	%xmm0, 8(%r14)
	movq	%rax, 24(%r14)
	cmpq	%rdx, %rsi
	je	.L624
	movq	%rcx, %rdi
	movq	%r15, %rdx
	call	memmove@PLT
	movq	%rax, %rcx
.L624:
	addq	%r15, %rcx
	movq	%rcx, 16(%r14)
	cmpq	%r13, %rbx
	je	.L640
	leaq	31(%r12), %rax
	subq	%r13, %rax
	cmpq	$62, %rax
	jbe	.L641
	leaq	-32(%rbx), %rsi
	xorl	%eax, %eax
	xorl	%edx, %edx
	movq	%rsi, %rcx
	subq	%r13, %rcx
	shrq	$5, %rcx
	addq	$1, %rcx
	.p2align 4,,10
	.p2align 3
.L628:
	movdqu	16(%r13,%rax), %xmm0
	movdqu	0(%r13,%rax), %xmm3
	addq	$1, %rdx
	movups	%xmm3, (%r12,%rax)
	movups	%xmm0, 16(%r12,%rax)
	addq	$32, %rax
	cmpq	%rcx, %rdx
	jb	.L628
.L627:
	subq	%r13, %rsi
	leaq	32(%r12,%rsi), %rcx
.L625:
	movq	-72(%rbp), %rax
	leaq	32(%rcx), %r14
	cmpq	%rax, %rbx
	je	.L629
	subq	%rbx, %rax
	xorl	%edx, %edx
	leaq	-32(%rax), %rdi
	xorl	%eax, %eax
	movq	%rdi, %rsi
	shrq	$5, %rsi
	addq	$1, %rsi
	.p2align 4,,10
	.p2align 3
.L630:
	movdqu	(%rbx,%rax), %xmm1
	movdqu	16(%rbx,%rax), %xmm2
	addq	$1, %rdx
	movups	%xmm1, 32(%rcx,%rax)
	movups	%xmm2, 48(%rcx,%rax)
	addq	$32, %rax
	cmpq	%rsi, %rdx
	jb	.L630
	leaq	32(%r14,%rdi), %r14
.L629:
	testq	%r13, %r13
	je	.L631
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L631:
	movq	-80(%rbp), %rax
	movq	%r12, %xmm0
	movq	%r14, %xmm4
	addq	-56(%rbp), %r12
	punpcklqdq	%xmm4, %xmm0
	movq	%r12, 16(%rax)
	movups	%xmm0, (%rax)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L655:
	.cfi_restore_state
	testq	%rdi, %rdi
	jne	.L620
	xorl	%r12d, %r12d
	jmp	.L637
	.p2align 4,,10
	.p2align 3
.L638:
	movq	$32, -56(%rbp)
	jmp	.L619
	.p2align 4,,10
	.p2align 3
.L656:
	movq	-64(%rbp), %r15
	xorl	%ecx, %ecx
	jmp	.L622
	.p2align 4,,10
	.p2align 3
.L641:
	movq	%r12, %rdx
	movq	%r13, %rax
	.p2align 4,,10
	.p2align 3
.L626:
	movq	(%rax), %rcx
	addq	$32, %rax
	addq	$32, %rdx
	movq	%rcx, -32(%rdx)
	movq	-24(%rax), %rcx
	movq	%rcx, -24(%rdx)
	movq	-16(%rax), %rcx
	movq	%rcx, -16(%rdx)
	movq	-8(%rax), %rcx
	movq	%rcx, -8(%rdx)
	cmpq	%rax, %rbx
	jne	.L626
	leaq	-32(%rbx), %rsi
	jmp	.L627
	.p2align 4,,10
	.p2align 3
.L640:
	movq	%r12, %rcx
	jmp	.L625
.L657:
	call	_ZSt17__throw_bad_allocv@PLT
.LEHE61:
.L620:
	movq	-56(%rbp), %rax
	cmpq	%rdx, %rax
	cmovbe	%rax, %rdx
	salq	$5, %rdx
	movq	%rdx, -56(%rbp)
	jmp	.L619
.L654:
	leaq	.LC15(%rip), %rdi
.LEHB62:
	call	_ZSt20__throw_length_errorPKc@PLT
.LEHE62:
.L642:
	endbr64
	movq	%rax, %rdi
.L632:
	call	__cxa_begin_catch@PLT
	testq	%r12, %r12
	je	.L658
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L636:
.LEHB63:
	call	__cxa_rethrow@PLT
.LEHE63:
.L658:
	movq	8(%r14), %rdi
	testq	%rdi, %rdi
	je	.L636
	call	_ZdlPv@PLT
	jmp	.L636
.L643:
	endbr64
	movq	%rax, %r12
.L635:
	call	__cxa_end_catch@PLT
	movq	%r12, %rdi
.LEHB64:
	call	_Unwind_Resume@PLT
.LEHE64:
	.cfi_endproc
.LFE9289:
	.section	.gcc_except_table._ZNSt6vectorIN2v88internal6torque16LabelDeclarationESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,"aG",@progbits,_ZNSt6vectorIN2v88internal6torque16LabelDeclarationESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,comdat
	.align 4
.LLSDA9289:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATT9289-.LLSDATTD9289
.LLSDATTD9289:
	.byte	0x1
	.uleb128 .LLSDACSE9289-.LLSDACSB9289
.LLSDACSB9289:
	.uleb128 .LEHB60-.LFB9289
	.uleb128 .LEHE60-.LEHB60
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB61-.LFB9289
	.uleb128 .LEHE61-.LEHB61
	.uleb128 .L642-.LFB9289
	.uleb128 0x1
	.uleb128 .LEHB62-.LFB9289
	.uleb128 .LEHE62-.LEHB62
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB63-.LFB9289
	.uleb128 .LEHE63-.LEHB63
	.uleb128 .L643-.LFB9289
	.uleb128 0
	.uleb128 .LEHB64-.LFB9289
	.uleb128 .LEHE64-.LEHB64
	.uleb128 0
	.uleb128 0
.LLSDACSE9289:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATT9289:
	.section	.text._ZNSt6vectorIN2v88internal6torque16LabelDeclarationESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal6torque16LabelDeclarationESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,comdat
	.size	_ZNSt6vectorIN2v88internal6torque16LabelDeclarationESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, .-_ZNSt6vectorIN2v88internal6torque16LabelDeclarationESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.section	.text._ZNSt6vectorIPN2v88internal6torque14TypeExpressionESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_,"axG",@progbits,_ZNSt6vectorIPN2v88internal6torque14TypeExpressionESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPN2v88internal6torque14TypeExpressionESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	.type	_ZNSt6vectorIPN2v88internal6torque14TypeExpressionESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_, @function
_ZNSt6vectorIPN2v88internal6torque14TypeExpressionESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_:
.LFB9325:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r9
	movq	%r13, %rax
	subq	%r9, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L673
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L669
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L674
.L661:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L668:
	movq	(%r15), %rax
	subq	%r8, %r13
	leaq	8(%rbx,%rdx), %r15
	movq	%rax, (%rbx,%rdx)
	leaq	(%r15,%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L675
	testq	%r13, %r13
	jg	.L664
	testq	%r9, %r9
	jne	.L667
.L665:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L675:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jg	.L664
.L667:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L665
	.p2align 4,,10
	.p2align 3
.L674:
	testq	%rsi, %rsi
	jne	.L662
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L668
	.p2align 4,,10
	.p2align 3
.L664:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L665
	jmp	.L667
	.p2align 4,,10
	.p2align 3
.L669:
	movl	$8, %r14d
	jmp	.L661
.L673:
	leaq	.LC15(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L662:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$3, %r14
	jmp	.L661
	.cfi_endproc
.LFE9325:
	.size	_ZNSt6vectorIPN2v88internal6torque14TypeExpressionESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_, .-_ZNSt6vectorIPN2v88internal6torque14TypeExpressionESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	.section	.text._ZNSt6vectorIPKN2v88internal6torque4TypeESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_,"axG",@progbits,_ZNSt6vectorIPKN2v88internal6torque4TypeESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPKN2v88internal6torque4TypeESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	.type	_ZNSt6vectorIPKN2v88internal6torque4TypeESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_, @function
_ZNSt6vectorIPKN2v88internal6torque4TypeESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_:
.LFB9703:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r9
	movq	%r13, %rax
	subq	%r9, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L690
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L686
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L691
.L678:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L685:
	movq	(%r15), %rax
	subq	%r8, %r13
	leaq	8(%rbx,%rdx), %r15
	movq	%rax, (%rbx,%rdx)
	leaq	(%r15,%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L692
	testq	%r13, %r13
	jg	.L681
	testq	%r9, %r9
	jne	.L684
.L682:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L692:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jg	.L681
.L684:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L682
	.p2align 4,,10
	.p2align 3
.L691:
	testq	%rsi, %rsi
	jne	.L679
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L685
	.p2align 4,,10
	.p2align 3
.L681:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L682
	jmp	.L684
	.p2align 4,,10
	.p2align 3
.L686:
	movl	$8, %r14d
	jmp	.L678
.L690:
	leaq	.LC15(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L679:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$3, %r14
	jmp	.L678
	.cfi_endproc
.LFE9703:
	.size	_ZNSt6vectorIPKN2v88internal6torque4TypeESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_, .-_ZNSt6vectorIPKN2v88internal6torque4TypeESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	.section	.text._ZNSt6vectorISt10unique_ptrIN2v88internal6torque4TypeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_,"axG",@progbits,_ZNSt6vectorISt10unique_ptrIN2v88internal6torque4TypeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorISt10unique_ptrIN2v88internal6torque4TypeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	.type	_ZNSt6vectorISt10unique_ptrIN2v88internal6torque4TypeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_, @function
_ZNSt6vectorISt10unique_ptrIN2v88internal6torque4TypeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_:
.LFB9889:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rax
	movq	(%rdi), %r12
	movq	%rdi, -80(%rbp)
	movq	%rax, -72(%rbp)
	subq	%r12, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L717
	movq	%rsi, %r9
	movq	%rsi, %r15
	movq	%rsi, %rbx
	subq	%r12, %r9
	testq	%rax, %rax
	je	.L708
	movabsq	$9223372036854775800, %r13
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L718
.L695:
	movq	%r13, %rdi
	movq	%rdx, -96(%rbp)
	movq	%r9, -88(%rbp)
	call	_Znwm@PLT
	movq	-88(%rbp), %r9
	movq	-96(%rbp), %rdx
	addq	%rax, %r13
	movq	%rax, -56(%rbp)
	leaq	8(%rax), %rcx
	movq	%r13, -64(%rbp)
.L707:
	movq	(%rdx), %rax
	movq	-56(%rbp), %rsi
	movq	$0, (%rdx)
	movq	%rax, (%rsi,%r9)
	cmpq	%r12, %r15
	je	.L697
	movq	%rsi, %r13
	movq	%r12, %r14
	.p2align 4,,10
	.p2align 3
.L701:
	movq	(%r14), %rcx
	movq	$0, (%r14)
	movq	%rcx, 0(%r13)
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L698
	movq	(%rdi), %rcx
	addq	$8, %r14
	addq	$8, %r13
	call	*8(%rcx)
	cmpq	%r14, %r15
	jne	.L701
.L699:
	movq	-56(%rbp), %rsi
	movq	%r15, %rax
	subq	%r12, %rax
	leaq	8(%rsi,%rax), %rcx
.L697:
	movq	-72(%rbp), %rax
	cmpq	%rax, %r15
	je	.L702
	movq	%rax, %r14
	subq	%r15, %r14
	leaq	-8(%r14), %r9
	movq	%r9, %rdi
	shrq	$3, %rdi
	addq	$1, %rdi
	testq	%r9, %r9
	je	.L710
	movq	%rdi, %rdx
	xorl	%eax, %eax
	shrq	%rdx
	salq	$4, %rdx
	.p2align 4,,10
	.p2align 3
.L704:
	movdqu	(%r15,%rax), %xmm1
	movups	%xmm1, (%rcx,%rax)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L704
	movq	%rdi, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %r8
	leaq	(%rcx,%r8), %rdx
	leaq	(%r15,%r8), %rbx
	cmpq	%rax, %rdi
	je	.L705
.L703:
	movq	(%rbx), %rax
	movq	%rax, (%rdx)
.L705:
	leaq	8(%rcx,%r9), %rcx
.L702:
	testq	%r12, %r12
	je	.L706
	movq	%r12, %rdi
	movq	%rcx, -72(%rbp)
	call	_ZdlPv@PLT
	movq	-72(%rbp), %rcx
.L706:
	movq	-56(%rbp), %xmm0
	movq	-80(%rbp), %rax
	movq	%rcx, %xmm2
	movq	-64(%rbp), %rbx
	punpcklqdq	%xmm2, %xmm0
	movq	%rbx, 16(%rax)
	movups	%xmm0, (%rax)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L698:
	.cfi_restore_state
	addq	$8, %r14
	addq	$8, %r13
	cmpq	%r14, %r15
	jne	.L701
	jmp	.L699
	.p2align 4,,10
	.p2align 3
.L718:
	testq	%rdi, %rdi
	jne	.L696
	movq	$0, -64(%rbp)
	movl	$8, %ecx
	movq	$0, -56(%rbp)
	jmp	.L707
	.p2align 4,,10
	.p2align 3
.L708:
	movl	$8, %r13d
	jmp	.L695
.L710:
	movq	%rcx, %rdx
	jmp	.L703
.L696:
	cmpq	%rcx, %rdi
	movq	%rcx, %rax
	cmovbe	%rdi, %rax
	leaq	0(,%rax,8), %r13
	jmp	.L695
.L717:
	leaq	.LC15(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE9889:
	.size	_ZNSt6vectorISt10unique_ptrIN2v88internal6torque4TypeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_, .-_ZNSt6vectorISt10unique_ptrIN2v88internal6torque4TypeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	.section	.rodata._ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_23AbstractTypeDeclarationE.str1.1,"aMS",@progbits,1
.LC16:
	.string	"\" cannot extend a type union"
.LC17:
	.string	"type \""
.LC18:
	.string	""
	.section	.rodata._ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_23AbstractTypeDeclarationE.str1.8,"aMS",@progbits,1
	.align 8
.LC19:
	.string	"cannot declare a transient type that is also constexpr"
	.section	.rodata._ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_23AbstractTypeDeclarationE.str1.1
.LC20:
	.string	"constexpr "
	.section	.text.unlikely._ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_23AbstractTypeDeclarationE,"ax",@progbits
	.align 2
.LCOLDB21:
	.section	.text._ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_23AbstractTypeDeclarationE,"ax",@progbits
.LHOTB21:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_23AbstractTypeDeclarationE
	.type	_ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_23AbstractTypeDeclarationE, @function
_ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_23AbstractTypeDeclarationE:
.LFB6532:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6532
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$232, %rsp
	movzbl	40(%rdi), %r13d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movb	$0, -128(%rbp)
	xorl	$1, %r13d
	cmpb	$0, 64(%rdi)
	movb	$0, -120(%rbp)
	movzbl	%r13b, %r13d
	jne	.L924
.L720:
	leaq	-224(%rbp), %r12
	leaq	-128(%rbp), %r15
	movl	%r13d, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
.LEHB65:
	call	_ZN2v88internal6torque12_GLOBAL__N_120ComputeGeneratesTypeENS_4base8OptionalINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEb
.LEHE65:
	cmpb	$0, -128(%rbp)
	je	.L729
	movq	-120(%rbp), %rdi
	leaq	-104(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L729
	call	_ZdlPv@PLT
.L729:
	cmpb	$0, 48(%rbx)
	jne	.L925
	leaq	.LC18(%rip), %rsi
	movq	%r12, %rdi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	leaq	-208(%rbp), %rax
	movq	$0, -264(%rbp)
	movq	%rax, -248(%rbp)
.L734:
	cmpb	$0, 40(%rbx)
	je	.L823
.L936:
	cmpb	$0, 41(%rbx)
	jne	.L926
	movq	32(%rbx), %rdx
	leaq	-112(%rbp), %r14
	movl	$10, %eax
	movq	40(%rdx), %rcx
	movq	%r14, -128(%rbp)
	movq	32(%rdx), %rdi
	cmpq	$10, %rcx
	cmovbe	%rcx, %rax
	movq	%rdi, %rsi
	addq	%rax, %rsi
	je	.L745
	testq	%rdi, %rdi
	je	.L927
.L745:
	cmpq	$1, %rcx
	je	.L928
	testq	%rcx, %rcx
	jne	.L929
.L747:
	movq	%r15, %rdi
	leaq	.LC20(%rip), %rsi
	movq	%rdx, -256(%rbp)
	movq	%rax, -120(%rbp)
	movb	$0, -112(%rbp,%rax)
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	movq	-128(%rbp), %rdi
	movq	-256(%rbp), %rdx
	movl	%eax, %r13d
	cmpq	%r14, %rdi
	je	.L754
	call	_ZdlPv@PLT
	movq	-256(%rbp), %rdx
.L754:
	movq	40(%rdx), %r12
	testl	%r13d, %r13d
	je	.L755
	movq	32(%rdx), %rax
	leaq	-176(%rbp), %r13
	movq	%r13, -192(%rbp)
	movq	%rax, %rdx
	movq	%rax, -256(%rbp)
	addq	%r12, %rdx
	je	.L756
	testq	%rax, %rax
	je	.L930
.L756:
	movq	%r12, -232(%rbp)
	cmpq	$15, %r12
	ja	.L931
	cmpq	$1, %r12
	jne	.L759
	movq	-256(%rbp), %rax
	movzbl	(%rax), %eax
	movb	%al, -176(%rbp)
	movq	%r13, %rax
	.p2align 4,,10
	.p2align 3
.L766:
	movq	%r12, -184(%rbp)
	movb	$0, (%rax,%r12)
	movq	-192(%rbp), %rax
	cmpq	%r13, %rax
	je	.L932
	movq	-176(%rbp), %rcx
	movq	-184(%rbp), %rdx
	pxor	%xmm0, %xmm0
	leaq	-88(%rbp), %r12
	leaq	-144(%rbp), %rsi
	movq	%r12, -104(%rbp)
	movq	%rax, -160(%rbp)
	movq	%rcx, -144(%rbp)
	movq	%rdx, -152(%rbp)
	movq	%r13, -192(%rbp)
	movq	$0, -184(%rbp)
	movb	$0, -176(%rbp)
	movq	$0, -112(%rbp)
	movaps	%xmm0, -128(%rbp)
	cmpq	%rsi, %rax
	je	.L768
	movq	%rax, -104(%rbp)
	movq	%rcx, -88(%rbp)
.L770:
	movq	%r15, %rdi
	movq	%rdx, -96(%rbp)
.LEHB66:
	call	_ZN2v88internal6torque12Declarations10LookupTypeERKNS1_13QualifiedNameE@PLT
.LEHE66:
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L771
	cmpl	$1, 8(%rax)
	movl	$0, %eax
	cmovne	%rax, %r13
.L771:
	movq	-104(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L772
	call	_ZdlPv@PLT
.L772:
	movq	-120(%rbp), %rax
	movq	-128(%rbp), %r12
	cmpq	%r12, %rax
	je	.L773
	.p2align 4,,10
	.p2align 3
.L777:
	movq	(%r12), %rdi
	leaq	16(%r12), %rdx
	cmpq	%rdx, %rdi
	je	.L774
	movq	%rax, -256(%rbp)
	addq	$32, %r12
	call	_ZdlPv@PLT
	movq	-256(%rbp), %rax
	cmpq	%r12, %rax
	jne	.L777
.L775:
	movq	-128(%rbp), %r12
.L773:
	testq	%r12, %r12
	je	.L742
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	jmp	.L742
	.p2align 4,,10
	.p2align 3
.L925:
	movq	56(%rbx), %rdi
	leaq	-208(%rbp), %rax
	movq	%rax, -248(%rbp)
.LEHB67:
	call	_ZN2v88internal6torque12Declarations10LookupTypeEPKNS1_10IdentifierE@PLT
	cmpl	$3, 8(%rax)
	movq	%rax, -264(%rbp)
	je	.L933
	leaq	.LC18(%rip), %rsi
	movq	%r12, %rdi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	leaq	-208(%rbp), %rax
	movq	%rax, -248(%rbp)
	jne	.L734
	movq	-264(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZNK2v88internal6torque4Type25GetGeneratedTNodeTypeNameB5cxx11Ev@PLT
.LEHE67:
	movq	-128(%rbp), %rax
	leaq	-112(%rbp), %r14
	movq	-224(%rbp), %rdi
	movq	-120(%rbp), %rdx
	cmpq	%r14, %rax
	je	.L934
	leaq	-208(%rbp), %rsi
	movq	-112(%rbp), %rcx
	movq	%rsi, -248(%rbp)
	cmpq	%rsi, %rdi
	je	.L935
	movq	%rdx, %xmm0
	movq	%rcx, %xmm3
	movq	-208(%rbp), %rsi
	movq	%rax, -224(%rbp)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, -216(%rbp)
	testq	%rdi, %rdi
	je	.L740
	movq	%rdi, -128(%rbp)
	movq	%rsi, -112(%rbp)
.L738:
	movq	$0, -120(%rbp)
	movb	$0, (%rdi)
	movq	-128(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L734
	call	_ZdlPv@PLT
	cmpb	$0, 40(%rbx)
	jne	.L936
	.p2align 4,,10
	.p2align 3
.L823:
	xorl	%r13d, %r13d
	leaq	-112(%rbp), %r14
.L742:
	movq	-224(%rbp), %rax
	movq	-216(%rbp), %r12
	movq	%r14, -128(%rbp)
	movq	%rax, %rcx
	movq	%rax, -256(%rbp)
	addq	%r12, %rcx
	je	.L779
	testq	%rax, %rax
	je	.L937
.L779:
	movq	%r12, -232(%rbp)
	cmpq	$15, %r12
	ja	.L938
	cmpq	$1, %r12
	jne	.L782
	movq	-256(%rbp), %rax
	movzbl	(%rax), %eax
	movb	%al, -112(%rbp)
	movq	%r14, %rax
.L783:
	movq	%r12, -120(%rbp)
	leaq	-144(%rbp), %rcx
	movb	$0, (%rax,%r12)
	movzbl	41(%rbx), %eax
	movq	%rcx, -160(%rbp)
	movb	%al, -265(%rbp)
	movq	32(%rbx), %rax
	movq	%rcx, -256(%rbp)
	movq	32(%rax), %r15
	movq	40(%rax), %r12
	movq	%r15, %rax
	addq	%r12, %rax
	je	.L784
	testq	%r15, %r15
	je	.L939
.L784:
	movq	%r12, -232(%rbp)
	cmpq	$15, %r12
	ja	.L940
	cmpq	$1, %r12
	jne	.L787
	movzbl	(%r15), %eax
	movb	%al, -144(%rbp)
	movq	-256(%rbp), %rax
.L788:
	movq	%r12, -152(%rbp)
	movl	$160, %edi
	movb	$0, (%rax,%r12)
.LEHB68:
	call	_Znwm@PLT
	movq	%rax, %r12
	movl	$1, 8(%rax)
	movq	-264(%rbp), %rax
	movq	-160(%rbp), %rbx
	movq	-152(%rbp), %r15
	movl	$0, 32(%r12)
	movq	%rax, 16(%r12)
	leaq	32(%r12), %rax
	movq	%rax, 48(%r12)
	movq	%rax, 56(%r12)
	leaq	16+_ZTVN2v88internal6torque12AbstractTypeE(%rip), %rax
	movq	%rax, (%r12)
	movzbl	-265(%rbp), %eax
	movq	$0, 40(%r12)
	movb	%al, 72(%r12)
	leaq	96(%r12), %rax
	movq	%rax, -264(%rbp)
	movq	%rax, 80(%r12)
	movq	%rbx, %rax
	movq	$0, 64(%r12)
	addq	%r15, %rax
	je	.L789
	testq	%rbx, %rbx
	je	.L941
.L789:
	movq	%r15, -232(%rbp)
	cmpq	$15, %r15
	ja	.L942
	cmpq	$1, %r15
	jne	.L792
	movzbl	(%rbx), %eax
	movb	%al, 96(%r12)
	movq	-264(%rbp), %rax
.L793:
	movq	%r15, 88(%r12)
	leaq	128(%r12), %rdi
	movb	$0, (%rax,%r15)
	movq	-128(%rbp), %rbx
	movq	-120(%rbp), %r15
	movq	%rdi, 112(%r12)
	movq	%rbx, %rax
	addq	%r15, %rax
	je	.L794
	testq	%rbx, %rbx
	je	.L943
.L794:
	movq	%r15, -232(%rbp)
	cmpq	$15, %r15
	ja	.L944
	cmpq	$1, %r15
	jne	.L797
	movzbl	(%rbx), %eax
	movb	%al, 128(%r12)
.L798:
	movq	%r15, 120(%r12)
	movb	$0, (%rdi,%r15)
	movq	%r13, 144(%r12)
	movq	$0, 152(%r12)
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_10TypeOracleEEERPNT_5ScopeEv@PLT
.LEHE68:
	movq	(%rax), %rdi
	movq	%r12, -232(%rbp)
	movq	144(%rdi), %rsi
	cmpq	152(%rdi), %rsi
	je	.L945
	movq	$0, -232(%rbp)
	movq	%r12, (%rsi)
	addq	$8, 144(%rdi)
.L805:
	movq	-232(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L806
	movq	(%rdi), %rax
	call	*8(%rax)
.L806:
	testq	%r13, %r13
	je	.L807
	movq	%r12, 152(%r13)
.L807:
	movq	-160(%rbp), %rdi
	cmpq	-256(%rbp), %rdi
	je	.L809
	call	_ZdlPv@PLT
.L809:
	movq	-128(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L812
	call	_ZdlPv@PLT
.L812:
	movq	-224(%rbp), %rdi
	cmpq	-248(%rbp), %rdi
	je	.L719
	call	_ZdlPv@PLT
.L719:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L946
	addq	$232, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L787:
	.cfi_restore_state
	testq	%r12, %r12
	jne	.L947
	movq	-256(%rbp), %rax
	jmp	.L788
	.p2align 4,,10
	.p2align 3
.L782:
	testq	%r12, %r12
	jne	.L948
	movq	%r14, %rax
	jmp	.L783
	.p2align 4,,10
	.p2align 3
.L792:
	testq	%r15, %r15
	jne	.L949
	movq	-264(%rbp), %rax
	jmp	.L793
	.p2align 4,,10
	.p2align 3
.L797:
	testq	%r15, %r15
	je	.L798
	jmp	.L796
	.p2align 4,,10
	.p2align 3
.L924:
	movq	72(%rdi), %r15
	movq	80(%rdi), %r14
	leaq	-104(%rbp), %r12
	movq	%r12, -120(%rbp)
	movq	%r15, %rax
	addq	%r14, %rax
	je	.L721
	testq	%r15, %r15
	je	.L950
.L721:
	movq	%r14, -232(%rbp)
	cmpq	$15, %r14
	ja	.L951
	cmpq	$1, %r14
	jne	.L724
	movzbl	(%r15), %eax
	movb	%al, -104(%rbp)
.L725:
	movq	%r14, -112(%rbp)
	movb	$0, (%r12,%r14)
	movb	$1, -128(%rbp)
	jmp	.L720
	.p2align 4,,10
	.p2align 3
.L940:
	leaq	-232(%rbp), %rsi
	leaq	-160(%rbp), %rdi
	xorl	%edx, %edx
.LEHB69:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE69:
	movq	%rax, -160(%rbp)
	movq	%rax, %rdi
	movq	-232(%rbp), %rax
	movq	%rax, -144(%rbp)
.L786:
	movq	%r12, %rdx
	movq	%r15, %rsi
	call	memcpy@PLT
	movq	-232(%rbp), %r12
	movq	-160(%rbp), %rax
	jmp	.L788
	.p2align 4,,10
	.p2align 3
.L938:
	leaq	-232(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r15, %rdi
.LEHB70:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE70:
	movq	%rax, -128(%rbp)
	movq	%rax, %rdi
	movq	-232(%rbp), %rax
	movq	%rax, -112(%rbp)
.L781:
	movq	-256(%rbp), %rsi
	movq	%r12, %rdx
	call	memcpy@PLT
	movq	-232(%rbp), %r12
	movq	-128(%rbp), %rax
	jmp	.L783
	.p2align 4,,10
	.p2align 3
.L942:
	leaq	-232(%rbp), %rsi
	leaq	80(%r12), %rdi
	xorl	%edx, %edx
.LEHB71:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE71:
	movq	%rax, 80(%r12)
	movq	%rax, %rdi
	movq	-232(%rbp), %rax
	movq	%rax, 96(%r12)
.L791:
	movq	%r15, %rdx
	movq	%rbx, %rsi
	call	memcpy@PLT
	movq	-232(%rbp), %r15
	movq	80(%r12), %rax
	jmp	.L793
	.p2align 4,,10
	.p2align 3
.L944:
	leaq	-232(%rbp), %rsi
	leaq	112(%r12), %rdi
	xorl	%edx, %edx
.LEHB72:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE72:
	movq	%rax, 112(%r12)
	movq	%rax, %rdi
	movq	-232(%rbp), %rax
	movq	%rax, 128(%r12)
.L796:
	movq	%r15, %rdx
	movq	%rbx, %rsi
	call	memcpy@PLT
	movq	-232(%rbp), %r15
	movq	112(%r12), %rdi
	jmp	.L798
	.p2align 4,,10
	.p2align 3
.L755:
	cmpq	$9, %r12
	jbe	.L952
	movq	32(%rdx), %rax
	leaq	-176(%rbp), %r13
	subq	$10, %r12
	leaq	-192(%rbp), %rdi
	movq	%r13, -192(%rbp)
	movq	%rax, -256(%rbp)
	movq	%r12, -232(%rbp)
	cmpq	$15, %r12
	ja	.L953
	cmpq	$1, %r12
	jne	.L765
	movzbl	10(%rax), %eax
	movb	%al, -176(%rbp)
	movq	%r13, %rax
	jmp	.L766
	.p2align 4,,10
	.p2align 3
.L929:
	movq	%r14, %r8
	movq	%rdi, %rcx
	cmpl	$8, %eax
	jnb	.L954
.L748:
	xorl	%esi, %esi
	testb	$4, %al
	je	.L751
	movl	(%rcx), %esi
	movl	%esi, (%r8)
	movl	$4, %esi
.L751:
	testb	$2, %al
	je	.L752
	movzwl	(%rcx,%rsi), %edi
	movw	%di, (%r8,%rsi)
	addq	$2, %rsi
.L752:
	testb	$1, %al
	je	.L747
	movzbl	(%rcx,%rsi), %ecx
	movb	%cl, (%r8,%rsi)
	jmp	.L747
	.p2align 4,,10
	.p2align 3
.L724:
	testq	%r14, %r14
	je	.L725
	movq	%r12, %rdi
	jmp	.L723
	.p2align 4,,10
	.p2align 3
.L953:
	leaq	-232(%rbp), %rsi
	xorl	%edx, %edx
.LEHB73:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -192(%rbp)
	movq	%rax, %rdi
	movq	-232(%rbp), %rax
	movq	%rax, -176(%rbp)
.L764:
	movq	-256(%rbp), %rsi
	movq	%r12, %rdx
	addq	$10, %rsi
	call	memcpy@PLT
	movq	-232(%rbp), %r12
	movq	-192(%rbp), %rax
	jmp	.L766
	.p2align 4,,10
	.p2align 3
.L931:
	leaq	-232(%rbp), %rsi
	leaq	-192(%rbp), %rdi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE73:
	movq	%rax, -192(%rbp)
	movq	%rax, %rdi
	movq	-232(%rbp), %rax
	movq	%rax, -176(%rbp)
.L758:
	movq	-256(%rbp), %rsi
	movq	%r12, %rdx
	call	memcpy@PLT
	movq	-232(%rbp), %r12
	movq	-192(%rbp), %rax
	jmp	.L766
	.p2align 4,,10
	.p2align 3
.L934:
	testq	%rdx, %rdx
	je	.L736
	cmpq	$1, %rdx
	je	.L955
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-120(%rbp), %rdx
	movq	-224(%rbp), %rdi
.L736:
	leaq	-208(%rbp), %rax
	movq	%rdx, -216(%rbp)
	movq	%rax, -248(%rbp)
	movb	$0, (%rdi,%rdx)
	movq	-128(%rbp), %rdi
	jmp	.L738
	.p2align 4,,10
	.p2align 3
.L774:
	addq	$32, %r12
	cmpq	%r12, %rax
	jne	.L777
	jmp	.L775
	.p2align 4,,10
	.p2align 3
.L945:
	leaq	-232(%rbp), %rdx
	addq	$136, %rdi
.LEHB74:
	call	_ZNSt6vectorISt10unique_ptrIN2v88internal6torque4TypeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
.LEHE74:
	jmp	.L805
	.p2align 4,,10
	.p2align 3
.L928:
	movzbl	(%rdi), %ecx
	movb	%cl, -112(%rbp)
	jmp	.L747
	.p2align 4,,10
	.p2align 3
.L932:
	leaq	-88(%rbp), %r12
	movdqa	-176(%rbp), %xmm1
	pxor	%xmm0, %xmm0
	movq	$0, -112(%rbp)
	movq	%r12, -104(%rbp)
	movq	-184(%rbp), %rdx
	movaps	%xmm1, -144(%rbp)
	movaps	%xmm0, -128(%rbp)
.L768:
	movdqa	-144(%rbp), %xmm2
	movups	%xmm2, -88(%rbp)
	jmp	.L770
	.p2align 4,,10
	.p2align 3
.L951:
	leaq	-232(%rbp), %rsi
	leaq	-120(%rbp), %rdi
	xorl	%edx, %edx
.LEHB75:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE75:
	movq	%rax, -120(%rbp)
	movq	%rax, %rdi
	movq	-232(%rbp), %rax
	movq	%rax, -104(%rbp)
.L723:
	movq	%r14, %rdx
	movq	%r15, %rsi
	call	memcpy@PLT
	movq	-232(%rbp), %r14
	movq	-120(%rbp), %r12
	jmp	.L725
	.p2align 4,,10
	.p2align 3
.L759:
	testq	%r12, %r12
	jne	.L956
.L825:
	movq	%r13, %rax
	jmp	.L766
	.p2align 4,,10
	.p2align 3
.L765:
	testq	%r12, %r12
	je	.L825
	movq	%r13, %rdi
	jmp	.L764
	.p2align 4,,10
	.p2align 3
.L954:
	movl	%eax, %r9d
	xorl	%ecx, %ecx
	andl	$-8, %r9d
.L749:
	movl	%ecx, %esi
	addl	$8, %ecx
	movq	(%rdi,%rsi), %r8
	movq	%r8, (%r14,%rsi)
	cmpl	%r9d, %ecx
	jb	.L749
	leaq	(%r14,%rcx), %r8
	addq	%rdi, %rcx
	jmp	.L748
	.p2align 4,,10
	.p2align 3
.L935:
	movq	%rdx, %xmm0
	movq	%rcx, %xmm4
	movq	%rax, -224(%rbp)
	punpcklqdq	%xmm4, %xmm0
	movups	%xmm0, -216(%rbp)
.L740:
	movq	%r14, -128(%rbp)
	leaq	-112(%rbp), %r14
	movq	%r14, %rdi
	jmp	.L738
	.p2align 4,,10
	.p2align 3
.L955:
	movzbl	-112(%rbp), %eax
	movb	%al, (%rdi)
	movq	-120(%rbp), %rdx
	movq	-224(%rbp), %rdi
	jmp	.L736
.L946:
	call	__stack_chk_fail@PLT
.L933:
	movq	32(%rbx), %rdx
	leaq	-208(%rbp), %rax
	movq	%r15, %rdi
	leaq	.LC16(%rip), %rcx
	leaq	.LC17(%rip), %rsi
	movq	%rax, -248(%rbp)
	addq	$32, %rdx
.LEHB76:
	call	_ZN2v88internal6torqueL7MessageIJRA17_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA37_S3_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
.LEHE76:
	movq	%r15, %rdi
.LEHB77:
	call	_ZNK2v88internal6torque14MessageBuilder5ThrowEv@PLT
.LEHE77:
.L926:
	leaq	.LC19(%rip), %rsi
	movq	%r15, %rdi
.LEHB78:
	call	_ZN2v88internal6torqueL7MessageIJRA55_KcEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
.LEHE78:
	movq	%r15, %rdi
.LEHB79:
	call	_ZNK2v88internal6torque14MessageBuilder5ThrowEv@PLT
.LEHE79:
.L939:
	leaq	.LC4(%rip), %rdi
.LEHB80:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE80:
.L943:
	leaq	.LC4(%rip), %rdi
.LEHB81:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE81:
.L937:
	leaq	.LC4(%rip), %rdi
.LEHB82:
	call	_ZSt19__throw_logic_errorPKc@PLT
.L952:
	movq	%r12, %rcx
	movl	$10, %edx
	leaq	.LC6(%rip), %rsi
	xorl	%eax, %eax
	leaq	.LC7(%rip), %rdi
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L927:
	leaq	.LC4(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE82:
.L941:
	leaq	.LC4(%rip), %rdi
.LEHB83:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE83:
.L948:
	movq	%r14, %rdi
	jmp	.L781
.L949:
	movq	-264(%rbp), %rdi
	jmp	.L791
.L947:
	movq	-256(%rbp), %rdi
	jmp	.L786
.L956:
	movq	%r13, %rdi
	jmp	.L758
.L930:
	leaq	.LC4(%rip), %rdi
.LEHB84:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE84:
.L950:
	leaq	.LC4(%rip), %rdi
.LEHB85:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE85:
.L835:
	endbr64
	movq	%rax, %r12
	jmp	.L732
.L834:
	endbr64
	movq	%rax, %r13
	jmp	.L727
.L837:
	endbr64
	movq	%rax, %rbx
	jmp	.L810
.L836:
	endbr64
	movq	%rax, %r12
	jmp	.L744
.L833:
	endbr64
	movq	%rax, %rbx
	jmp	.L803
.L839:
	endbr64
	movq	%rax, %rbx
	jmp	.L800
.L832:
	endbr64
	movq	%rax, %rbx
	jmp	.L818
.L831:
	endbr64
	movq	%rax, %r12
	jmp	.L816
.L829:
	endbr64
	movq	%rax, %r12
	jmp	.L814
.L830:
	endbr64
	movq	%rax, %r12
	jmp	.L733
.L838:
	endbr64
	movq	%rax, %rbx
	jmp	.L802
	.section	.gcc_except_table._ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_23AbstractTypeDeclarationE,"a",@progbits
.LLSDA6532:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6532-.LLSDACSB6532
.LLSDACSB6532:
	.uleb128 .LEHB65-.LFB6532
	.uleb128 .LEHE65-.LEHB65
	.uleb128 .L829-.LFB6532
	.uleb128 0
	.uleb128 .LEHB66-.LFB6532
	.uleb128 .LEHE66-.LEHB66
	.uleb128 .L831-.LFB6532
	.uleb128 0
	.uleb128 .LEHB67-.LFB6532
	.uleb128 .LEHE67-.LEHB67
	.uleb128 .L830-.LFB6532
	.uleb128 0
	.uleb128 .LEHB68-.LFB6532
	.uleb128 .LEHE68-.LEHB68
	.uleb128 .L833-.LFB6532
	.uleb128 0
	.uleb128 .LEHB69-.LFB6532
	.uleb128 .LEHE69-.LEHB69
	.uleb128 .L832-.LFB6532
	.uleb128 0
	.uleb128 .LEHB70-.LFB6532
	.uleb128 .LEHE70-.LEHB70
	.uleb128 .L830-.LFB6532
	.uleb128 0
	.uleb128 .LEHB71-.LFB6532
	.uleb128 .LEHE71-.LEHB71
	.uleb128 .L838-.LFB6532
	.uleb128 0
	.uleb128 .LEHB72-.LFB6532
	.uleb128 .LEHE72-.LEHB72
	.uleb128 .L839-.LFB6532
	.uleb128 0
	.uleb128 .LEHB73-.LFB6532
	.uleb128 .LEHE73-.LEHB73
	.uleb128 .L830-.LFB6532
	.uleb128 0
	.uleb128 .LEHB74-.LFB6532
	.uleb128 .LEHE74-.LEHB74
	.uleb128 .L837-.LFB6532
	.uleb128 0
	.uleb128 .LEHB75-.LFB6532
	.uleb128 .LEHE75-.LEHB75
	.uleb128 .L834-.LFB6532
	.uleb128 0
	.uleb128 .LEHB76-.LFB6532
	.uleb128 .LEHE76-.LEHB76
	.uleb128 .L830-.LFB6532
	.uleb128 0
	.uleb128 .LEHB77-.LFB6532
	.uleb128 .LEHE77-.LEHB77
	.uleb128 .L835-.LFB6532
	.uleb128 0
	.uleb128 .LEHB78-.LFB6532
	.uleb128 .LEHE78-.LEHB78
	.uleb128 .L830-.LFB6532
	.uleb128 0
	.uleb128 .LEHB79-.LFB6532
	.uleb128 .LEHE79-.LEHB79
	.uleb128 .L836-.LFB6532
	.uleb128 0
	.uleb128 .LEHB80-.LFB6532
	.uleb128 .LEHE80-.LEHB80
	.uleb128 .L832-.LFB6532
	.uleb128 0
	.uleb128 .LEHB81-.LFB6532
	.uleb128 .LEHE81-.LEHB81
	.uleb128 .L839-.LFB6532
	.uleb128 0
	.uleb128 .LEHB82-.LFB6532
	.uleb128 .LEHE82-.LEHB82
	.uleb128 .L830-.LFB6532
	.uleb128 0
	.uleb128 .LEHB83-.LFB6532
	.uleb128 .LEHE83-.LEHB83
	.uleb128 .L838-.LFB6532
	.uleb128 0
	.uleb128 .LEHB84-.LFB6532
	.uleb128 .LEHE84-.LEHB84
	.uleb128 .L830-.LFB6532
	.uleb128 0
	.uleb128 .LEHB85-.LFB6532
	.uleb128 .LEHE85-.LEHB85
	.uleb128 .L834-.LFB6532
	.uleb128 0
.LLSDACSE6532:
	.section	.text._ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_23AbstractTypeDeclarationE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_23AbstractTypeDeclarationE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6532
	.type	_ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_23AbstractTypeDeclarationE.cold, @function
_ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_23AbstractTypeDeclarationE.cold:
.LFSB6532:
.L732:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	%r15, %rdi
	call	_ZN2v88internal6torque14MessageBuilderD1Ev
	leaq	-208(%rbp), %rax
	movq	%rax, -248(%rbp)
.L733:
	movq	-224(%rbp), %rdi
	cmpq	-248(%rbp), %rdi
	je	.L820
	call	_ZdlPv@PLT
.L820:
	movq	%r12, %rdi
.LEHB86:
	call	_Unwind_Resume@PLT
.L727:
	cmpb	$0, -128(%rbp)
	je	.L728
	movq	-120(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L728
	call	_ZdlPv@PLT
.L728:
	movq	%r13, %rdi
	call	_Unwind_Resume@PLT
.L810:
	movq	-232(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L803
	movq	(%rdi), %rax
	call	*8(%rax)
.L803:
	movq	-160(%rbp), %rdi
	cmpq	-256(%rbp), %rdi
	je	.L818
	call	_ZdlPv@PLT
.L818:
	movq	-128(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L819
	call	_ZdlPv@PLT
.L819:
	movq	%rbx, %r12
	jmp	.L733
.L744:
	movq	%r15, %rdi
	call	_ZN2v88internal6torque14MessageBuilderD1Ev
	jmp	.L733
.L800:
	movq	80(%r12), %rdi
	cmpq	%rdi, -264(%rbp)
	je	.L802
	call	_ZdlPv@PLT
.L802:
	leaq	16+_ZTVN2v88internal6torque4TypeE(%rip), %rax
	movq	40(%r12), %rsi
	leaq	24(%r12), %rdi
	movq	%rax, (%r12)
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movl	$160, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	jmp	.L803
.L816:
	movq	%r15, %rdi
	call	_ZN2v88internal6torque13QualifiedNameD1Ev
	jmp	.L733
.L814:
	cmpb	$0, -128(%rbp)
	je	.L815
	movq	-120(%rbp), %rdi
	leaq	-104(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L815
	call	_ZdlPv@PLT
.L815:
	movq	%r12, %rdi
	call	_Unwind_Resume@PLT
.LEHE86:
	.cfi_endproc
.LFE6532:
	.section	.gcc_except_table._ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_23AbstractTypeDeclarationE
.LLSDAC6532:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6532-.LLSDACSBC6532
.LLSDACSBC6532:
	.uleb128 .LEHB86-.LCOLDB21
	.uleb128 .LEHE86-.LEHB86
	.uleb128 0
	.uleb128 0
.LLSDACSEC6532:
	.section	.text.unlikely._ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_23AbstractTypeDeclarationE
	.section	.text._ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_23AbstractTypeDeclarationE
	.size	_ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_23AbstractTypeDeclarationE, .-_ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_23AbstractTypeDeclarationE
	.section	.text.unlikely._ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_23AbstractTypeDeclarationE
	.size	_ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_23AbstractTypeDeclarationE.cold, .-_ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_23AbstractTypeDeclarationE.cold
.LCOLDE21:
	.section	.text._ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_23AbstractTypeDeclarationE
.LHOTE21:
	.section	.text._ZNSt6vectorISt10unique_ptrIN2v88internal6torque13AggregateTypeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_,"axG",@progbits,_ZNSt6vectorISt10unique_ptrIN2v88internal6torque13AggregateTypeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorISt10unique_ptrIN2v88internal6torque13AggregateTypeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	.type	_ZNSt6vectorISt10unique_ptrIN2v88internal6torque13AggregateTypeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_, @function
_ZNSt6vectorISt10unique_ptrIN2v88internal6torque13AggregateTypeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_:
.LFB9936:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rax
	movq	(%rdi), %r12
	movq	%rdi, -80(%rbp)
	movq	%rax, -72(%rbp)
	subq	%r12, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L981
	movq	%rsi, %r9
	movq	%rsi, %r15
	movq	%rsi, %rbx
	subq	%r12, %r9
	testq	%rax, %rax
	je	.L972
	movabsq	$9223372036854775800, %r13
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L982
.L959:
	movq	%r13, %rdi
	movq	%rdx, -96(%rbp)
	movq	%r9, -88(%rbp)
	call	_Znwm@PLT
	movq	-88(%rbp), %r9
	movq	-96(%rbp), %rdx
	addq	%rax, %r13
	movq	%rax, -56(%rbp)
	leaq	8(%rax), %rcx
	movq	%r13, -64(%rbp)
.L971:
	movq	(%rdx), %rax
	movq	-56(%rbp), %rsi
	movq	$0, (%rdx)
	movq	%rax, (%rsi,%r9)
	cmpq	%r12, %r15
	je	.L961
	movq	%rsi, %r13
	movq	%r12, %r14
	.p2align 4,,10
	.p2align 3
.L965:
	movq	(%r14), %rcx
	movq	$0, (%r14)
	movq	%rcx, 0(%r13)
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L962
	movq	(%rdi), %rcx
	addq	$8, %r14
	addq	$8, %r13
	call	*8(%rcx)
	cmpq	%r14, %r15
	jne	.L965
.L963:
	movq	-56(%rbp), %rsi
	movq	%r15, %rax
	subq	%r12, %rax
	leaq	8(%rsi,%rax), %rcx
.L961:
	movq	-72(%rbp), %rax
	cmpq	%rax, %r15
	je	.L966
	movq	%rax, %r14
	subq	%r15, %r14
	leaq	-8(%r14), %r9
	movq	%r9, %rdi
	shrq	$3, %rdi
	addq	$1, %rdi
	testq	%r9, %r9
	je	.L974
	movq	%rdi, %rdx
	xorl	%eax, %eax
	shrq	%rdx
	salq	$4, %rdx
	.p2align 4,,10
	.p2align 3
.L968:
	movdqu	(%r15,%rax), %xmm1
	movups	%xmm1, (%rcx,%rax)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L968
	movq	%rdi, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %r8
	leaq	(%rcx,%r8), %rdx
	leaq	(%r15,%r8), %rbx
	cmpq	%rax, %rdi
	je	.L969
.L967:
	movq	(%rbx), %rax
	movq	%rax, (%rdx)
.L969:
	leaq	8(%rcx,%r9), %rcx
.L966:
	testq	%r12, %r12
	je	.L970
	movq	%r12, %rdi
	movq	%rcx, -72(%rbp)
	call	_ZdlPv@PLT
	movq	-72(%rbp), %rcx
.L970:
	movq	-56(%rbp), %xmm0
	movq	-80(%rbp), %rax
	movq	%rcx, %xmm2
	movq	-64(%rbp), %rbx
	punpcklqdq	%xmm2, %xmm0
	movq	%rbx, 16(%rax)
	movups	%xmm0, (%rax)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L962:
	.cfi_restore_state
	addq	$8, %r14
	addq	$8, %r13
	cmpq	%r14, %r15
	jne	.L965
	jmp	.L963
	.p2align 4,,10
	.p2align 3
.L982:
	testq	%rdi, %rdi
	jne	.L960
	movq	$0, -64(%rbp)
	movl	$8, %ecx
	movq	$0, -56(%rbp)
	jmp	.L971
	.p2align 4,,10
	.p2align 3
.L972:
	movl	$8, %r13d
	jmp	.L959
.L974:
	movq	%rcx, %rdx
	jmp	.L967
.L960:
	cmpq	%rcx, %rdi
	movq	%rcx, %rax
	cmovbe	%rdi, %rax
	leaq	0(,%rax,8), %r13
	jmp	.L959
.L981:
	leaq	.LC15(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE9936:
	.size	_ZNSt6vectorISt10unique_ptrIN2v88internal6torque13AggregateTypeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_, .-_ZNSt6vectorISt10unique_ptrIN2v88internal6torque13AggregateTypeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	.section	.text._ZNSt6vectorISt10unique_ptrIN2v88internal6torque9NamespaceESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_,"axG",@progbits,_ZNSt6vectorISt10unique_ptrIN2v88internal6torque9NamespaceESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorISt10unique_ptrIN2v88internal6torque9NamespaceESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	.type	_ZNSt6vectorISt10unique_ptrIN2v88internal6torque9NamespaceESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_, @function
_ZNSt6vectorISt10unique_ptrIN2v88internal6torque9NamespaceESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_:
.LFB9961:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rax
	movq	(%rdi), %r12
	movq	%rdi, -80(%rbp)
	movq	%rax, -72(%rbp)
	subq	%r12, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L1007
	movq	%rsi, %r9
	movq	%rsi, %r15
	movq	%rsi, %rbx
	subq	%r12, %r9
	testq	%rax, %rax
	je	.L998
	movabsq	$9223372036854775800, %r13
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L1008
.L985:
	movq	%r13, %rdi
	movq	%rdx, -96(%rbp)
	movq	%r9, -88(%rbp)
	call	_Znwm@PLT
	movq	-88(%rbp), %r9
	movq	-96(%rbp), %rdx
	addq	%rax, %r13
	movq	%rax, -56(%rbp)
	leaq	8(%rax), %rcx
	movq	%r13, -64(%rbp)
.L997:
	movq	(%rdx), %rax
	movq	-56(%rbp), %rsi
	movq	$0, (%rdx)
	movq	%rax, (%rsi,%r9)
	cmpq	%r12, %r15
	je	.L987
	movq	%rsi, %r13
	movq	%r12, %r14
	.p2align 4,,10
	.p2align 3
.L991:
	movq	(%r14), %rcx
	movq	$0, (%r14)
	movq	%rcx, 0(%r13)
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L988
	movq	(%rdi), %rcx
	addq	$8, %r14
	addq	$8, %r13
	call	*8(%rcx)
	cmpq	%r14, %r15
	jne	.L991
.L989:
	movq	-56(%rbp), %rsi
	movq	%r15, %rax
	subq	%r12, %rax
	leaq	8(%rsi,%rax), %rcx
.L987:
	movq	-72(%rbp), %rax
	cmpq	%rax, %r15
	je	.L992
	movq	%rax, %r14
	subq	%r15, %r14
	leaq	-8(%r14), %r9
	movq	%r9, %rdi
	shrq	$3, %rdi
	addq	$1, %rdi
	testq	%r9, %r9
	je	.L1000
	movq	%rdi, %rdx
	xorl	%eax, %eax
	shrq	%rdx
	salq	$4, %rdx
	.p2align 4,,10
	.p2align 3
.L994:
	movdqu	(%r15,%rax), %xmm1
	movups	%xmm1, (%rcx,%rax)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L994
	movq	%rdi, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %r8
	leaq	(%rcx,%r8), %rdx
	leaq	(%r15,%r8), %rbx
	cmpq	%rax, %rdi
	je	.L995
.L993:
	movq	(%rbx), %rax
	movq	%rax, (%rdx)
.L995:
	leaq	8(%rcx,%r9), %rcx
.L992:
	testq	%r12, %r12
	je	.L996
	movq	%r12, %rdi
	movq	%rcx, -72(%rbp)
	call	_ZdlPv@PLT
	movq	-72(%rbp), %rcx
.L996:
	movq	-56(%rbp), %xmm0
	movq	-80(%rbp), %rax
	movq	%rcx, %xmm2
	movq	-64(%rbp), %rbx
	punpcklqdq	%xmm2, %xmm0
	movq	%rbx, 16(%rax)
	movups	%xmm0, (%rax)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L988:
	.cfi_restore_state
	addq	$8, %r14
	addq	$8, %r13
	cmpq	%r14, %r15
	jne	.L991
	jmp	.L989
	.p2align 4,,10
	.p2align 3
.L1008:
	testq	%rdi, %rdi
	jne	.L986
	movq	$0, -64(%rbp)
	movl	$8, %ecx
	movq	$0, -56(%rbp)
	jmp	.L997
	.p2align 4,,10
	.p2align 3
.L998:
	movl	$8, %r13d
	jmp	.L985
.L1000:
	movq	%rcx, %rdx
	jmp	.L993
.L986:
	cmpq	%rcx, %rdi
	movq	%rcx, %rax
	cmovbe	%rdi, %rax
	leaq	0(,%rax,8), %r13
	jmp	.L985
.L1007:
	leaq	.LC15(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE9961:
	.size	_ZNSt6vectorISt10unique_ptrIN2v88internal6torque9NamespaceESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_, .-_ZNSt6vectorISt10unique_ptrIN2v88internal6torque9NamespaceESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	.section	.text._ZN2v88internal6torque10TypeOracle13GetStructTypeEPKNS1_17StructDeclarationENS_4base8OptionalINS1_17SpecializationKeyINS1_17GenericStructTypeEEEEE,"axG",@progbits,_ZN2v88internal6torque10TypeOracle13GetStructTypeEPKNS1_17StructDeclarationENS_4base8OptionalINS1_17SpecializationKeyINS1_17GenericStructTypeEEEEE,comdat
	.p2align 4
	.weak	_ZN2v88internal6torque10TypeOracle13GetStructTypeEPKNS1_17StructDeclarationENS_4base8OptionalINS1_17SpecializationKeyINS1_17GenericStructTypeEEEEE
	.type	_ZN2v88internal6torque10TypeOracle13GetStructTypeEPKNS1_17StructDeclarationENS_4base8OptionalINS1_17SpecializationKeyINS1_17GenericStructTypeEEEEE, @function
_ZN2v88internal6torque10TypeOracle13GetStructTypeEPKNS1_17StructDeclarationENS_4base8OptionalINS1_17SpecializationKeyINS1_17GenericStructTypeEEEEE:
.LFB6447:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6447
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r15
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movl	$160, %edi
	subq	$216, %rsp
	movq	%rsi, -216(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$25461, %eax
	movq	%r15, -96(%rbp)
	movl	$1920234335, -80(%rbp)
	movw	%ax, -76(%rbp)
	movb	$116, -74(%rbp)
	movq	$7, -88(%rbp)
	movb	$0, -73(%rbp)
.LEHB87:
	call	_Znwm@PLT
.LEHE87:
	movq	%rax, %r13
	leaq	16+_ZTVN2v88internal6torque10DeclarableE(%rip), %rax
	movq	%rax, 0(%r13)
	movl	$0, 8(%r13)
.LEHB88:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rax
	movq	(%rax), %rax
	movq	%rax, 16(%r13)
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
.LEHE88:
	movq	(%rax), %rax
	movq	-96(%rbp), %r14
	pcmpeqd	%xmm0, %xmm0
	movb	$1, 64(%r13)
	movq	-88(%rbp), %r12
	leaq	144(%r13), %rdi
	movups	%xmm0, 44(%r13)
	movdqu	(%rax), %xmm1
	movl	16(%rax), %eax
	movl	$-1, 60(%r13)
	movq	$1, 80(%r13)
	movl	%eax, 40(%r13)
	leaq	120(%r13), %rax
	movq	%rax, -232(%rbp)
	movq	%rax, 72(%r13)
	leaq	16+_ZTVN2v88internal6torque9NamespaceE(%rip), %rax
	movq	%rax, 0(%r13)
	movq	%r14, %rax
	addq	%r12, %rax
	movq	$0, 88(%r13)
	movq	$0, 96(%r13)
	movl	$0x3f800000, 104(%r13)
	movq	$0, 112(%r13)
	movq	$0, 120(%r13)
	movq	%rdi, 128(%r13)
	movups	%xmm1, 24(%r13)
	je	.L1010
	testq	%r14, %r14
	je	.L1157
.L1010:
	movq	%r12, -144(%rbp)
	cmpq	$15, %r12
	ja	.L1158
	cmpq	$1, %r12
	jne	.L1013
	movzbl	(%r14), %eax
	movb	%al, 144(%r13)
.L1014:
	movq	%r12, 136(%r13)
	movb	$0, (%rdi,%r12)
	movq	-96(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L1016
	call	_ZdlPv@PLT
.L1016:
	movq	-216(%rbp), %rax
	movb	$0, -184(%rbp)
	movzbl	(%rax), %eax
	movb	%al, -232(%rbp)
	testb	%al, %al
	jne	.L1159
	movl	$216, %edi
.LEHB89:
	call	_Znwm@PLT
.LEHE89:
	movb	$0, -144(%rbp)
	movq	%rax, %r12
	movb	$0, -136(%rbp)
.L1030:
	movq	32(%rbx), %rax
	leaq	-144(%rbp), %rdx
	leaq	-96(%rbp), %rdi
	movq	%rdx, -240(%rbp)
	leaq	32(%rax), %rsi
.LEHB90:
	call	_ZN2v88internal6torque10StructType11ComputeNameERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS_4base8OptionalINS1_17SpecializationKeyINS1_17GenericStructTypeEEEEE@PLT
.LEHE90:
	leaq	32(%r12), %rax
	movq	-88(%rbp), %r14
	pxor	%xmm0, %xmm0
	leaq	128(%r12), %rdi
	movq	%rax, 48(%r12)
	movq	%rax, 56(%r12)
	leaq	16+_ZTVN2v88internal6torque13AggregateTypeE(%rip), %rax
	movq	%rax, (%r12)
	movq	-96(%rbp), %rax
	movl	$4, 8(%r12)
	movq	%rax, %rcx
	movb	$0, 72(%r12)
	addq	%r14, %rcx
	movq	%r13, 104(%r12)
	movq	$0, 16(%r12)
	movl	$0, 32(%r12)
	movq	$0, 40(%r12)
	movq	$0, 64(%r12)
	movq	$0, 96(%r12)
	movq	%rdi, 112(%r12)
	movq	%rax, -216(%rbp)
	movups	%xmm0, 80(%r12)
	je	.L1038
	testq	%rax, %rax
	je	.L1160
.L1038:
	movq	%r14, -200(%rbp)
	cmpq	$15, %r14
	ja	.L1161
	cmpq	$1, %r14
	jne	.L1041
	movq	-216(%rbp), %rax
	movzbl	(%rax), %eax
	movb	%al, 128(%r12)
.L1042:
	movq	%r14, 120(%r12)
	pxor	%xmm0, %xmm0
	movb	$0, (%rdi,%r14)
	movq	-96(%rbp), %rdi
	movq	$0, 160(%r12)
	movups	%xmm0, 144(%r12)
	cmpq	%r15, %rdi
	je	.L1044
	call	_ZdlPv@PLT
.L1044:
	cmpb	$0, -144(%rbp)
	je	.L1052
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1052
	call	_ZdlPv@PLT
.L1052:
	leaq	16+_ZTVN2v88internal6torque10StructTypeE(%rip), %rax
	cmpb	$0, -232(%rbp)
	movq	%rbx, 168(%r12)
	movq	%rax, (%r12)
	movb	$0, 176(%r12)
	jne	.L1053
	movb	$0, 184(%r12)
.L1054:
.LEHB91:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_10TypeOracleEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rdi
	movq	%r12, -144(%rbp)
	movq	168(%rdi), %rsi
	cmpq	176(%rdi), %rsi
	je	.L1065
	movq	$0, -144(%rbp)
	movq	%r12, (%rsi)
	addq	$8, 168(%rdi)
.L1066:
	movq	-144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1067
	movq	(%rdi), %rax
	call	*8(%rax)
.L1067:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_10TypeOracleEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rdi
	movq	%r13, -144(%rbp)
	movq	216(%rdi), %rsi
	cmpq	224(%rdi), %rsi
	je	.L1068
	movq	$0, -144(%rbp)
	movq	%r13, (%rsi)
	addq	$8, 216(%rdi)
.L1069:
	movq	-144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1009
	movq	(%rdi), %rax
	call	*8(%rax)
.L1009:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1162
	addq	$216, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1013:
	.cfi_restore_state
	testq	%r12, %r12
	je	.L1014
	jmp	.L1012
	.p2align 4,,10
	.p2align 3
.L1041:
	testq	%r14, %r14
	je	.L1042
	jmp	.L1040
	.p2align 4,,10
	.p2align 3
.L1159:
	movq	-216(%rbp), %rax
	movq	8(%rax), %rcx
	movq	16(%rax), %rsi
	movq	24(%rax), %rax
	movq	%rcx, -240(%rbp)
	movq	%rax, %r14
	movq	%rcx, -184(%rbp)
	subq	%rsi, %r14
	movq	%r14, %rdx
	sarq	$3, %rdx
	je	.L1080
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L1163
	movq	%r14, %rdi
	call	_Znwm@PLT
.LEHE91:
	movq	-216(%rbp), %rcx
	movq	%rax, -224(%rbp)
	movq	24(%rcx), %rax
	movq	16(%rcx), %rsi
	movq	%rax, %r14
	subq	%rsi, %r14
.L1025:
	cmpq	%rax, %rsi
	je	.L1027
	movq	-224(%rbp), %rdi
	movq	%r14, %rdx
	call	memcpy@PLT
.L1027:
	movq	-224(%rbp), %rax
	movl	$216, %edi
	addq	%r14, %rax
	movq	%rax, -248(%rbp)
.LEHB92:
	call	_Znwm@PLT
.LEHE92:
	movq	%rax, %r12
	movq	-240(%rbp), %rax
	movb	$0, -144(%rbp)
	movq	$0, -128(%rbp)
	movq	%rax, -136(%rbp)
	movq	%r14, %rax
	sarq	$3, %rax
	movq	$0, -120(%rbp)
	movq	$0, -112(%rbp)
	je	.L1164
	movabsq	$1152921504606846975, %rdx
	cmpq	%rdx, %rax
	ja	.L1165
	movq	%r14, %rdi
.LEHB93:
	call	_Znwm@PLT
.LEHE93:
	movq	%rax, %rdi
.L1031:
	leaq	(%rdi,%r14), %rcx
	movq	%rdi, -128(%rbp)
	movq	-224(%rbp), %rsi
	movq	%rcx, -112(%rbp)
	cmpq	%rsi, -248(%rbp)
	je	.L1033
	movq	%r14, %rdx
	movq	%rcx, -216(%rbp)
	call	memcpy@PLT
	movq	-216(%rbp), %rcx
.L1033:
	movq	%rcx, -120(%rbp)
	movb	$1, -144(%rbp)
	jmp	.L1030
	.p2align 4,,10
	.p2align 3
.L1158:
	leaq	-144(%rbp), %rsi
	leaq	128(%r13), %rdi
	xorl	%edx, %edx
.LEHB94:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE94:
	movq	%rax, 128(%r13)
	movq	%rax, %rdi
	movq	-144(%rbp), %rax
	movq	%rax, 144(%r13)
.L1012:
	movq	%r12, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-144(%rbp), %r12
	movq	128(%r13), %rdi
	jmp	.L1014
	.p2align 4,,10
	.p2align 3
.L1053:
	movq	-184(%rbp), %rax
	movq	-248(%rbp), %r14
	movq	$0, 192(%r12)
	subq	-224(%rbp), %r14
	movq	$0, 200(%r12)
	movq	%rax, 184(%r12)
	movq	%r14, %rax
	movq	$0, 208(%r12)
	sarq	$3, %rax
	je	.L1166
	movabsq	$1152921504606846975, %rdx
	cmpq	%rdx, %rax
	ja	.L1167
	movq	%r14, %rdi
.LEHB95:
	call	_Znwm@PLT
.LEHE95:
	movq	%rax, %rdi
.L1056:
	movq	-224(%rbp), %rax
	movq	-248(%rbp), %rcx
	leaq	(%rdi,%r14), %rbx
	movq	%rdi, 192(%r12)
	movq	%rbx, 208(%r12)
	cmpq	%rcx, %rax
	je	.L1058
	movq	%r14, %rdx
	movq	%rax, %rsi
	call	memcpy@PLT
	movq	%rbx, 200(%r12)
	movb	$1, 176(%r12)
.L1059:
	movq	-224(%rbp), %rdi
	call	_ZdlPv@PLT
	jmp	.L1054
	.p2align 4,,10
	.p2align 3
.L1058:
	cmpq	$0, -224(%rbp)
	movq	%rbx, 200(%r12)
	movb	$1, 176(%r12)
	je	.L1054
	jmp	.L1059
	.p2align 4,,10
	.p2align 3
.L1161:
	leaq	-200(%rbp), %rsi
	leaq	112(%r12), %rdi
	xorl	%edx, %edx
.LEHB96:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE96:
	movq	%rax, 112(%r12)
	movq	%rax, %rdi
	movq	-200(%rbp), %rax
	movq	%rax, 128(%r12)
.L1040:
	movq	-216(%rbp), %rsi
	movq	%r14, %rdx
	call	memcpy@PLT
	movq	-200(%rbp), %r14
	movq	112(%r12), %rdi
	jmp	.L1042
	.p2align 4,,10
	.p2align 3
.L1068:
	movq	-240(%rbp), %rdx
	addq	$208, %rdi
.LEHB97:
	call	_ZNSt6vectorISt10unique_ptrIN2v88internal6torque9NamespaceESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
.LEHE97:
	jmp	.L1069
	.p2align 4,,10
	.p2align 3
.L1065:
	movq	-240(%rbp), %rdx
	addq	$160, %rdi
.LEHB98:
	call	_ZNSt6vectorISt10unique_ptrIN2v88internal6torque13AggregateTypeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
.LEHE98:
	jmp	.L1066
	.p2align 4,,10
	.p2align 3
.L1080:
	movq	$0, -224(%rbp)
	jmp	.L1025
	.p2align 4,,10
	.p2align 3
.L1164:
	xorl	%edi, %edi
	jmp	.L1031
	.p2align 4,,10
	.p2align 3
.L1166:
	xorl	%edi, %edi
	jmp	.L1056
.L1157:
	leaq	.LC4(%rip), %rdi
.LEHB99:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE99:
.L1162:
	call	__stack_chk_fail@PLT
.L1160:
	leaq	.LC4(%rip), %rdi
.LEHB100:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE100:
.L1163:
.LEHB101:
	call	_ZSt17__throw_bad_allocv@PLT
.LEHE101:
.L1165:
.LEHB102:
	call	_ZSt17__throw_bad_allocv@PLT
.LEHE102:
.L1167:
.LEHB103:
	call	_ZSt17__throw_bad_allocv@PLT
.LEHE103:
.L1082:
	endbr64
	movq	%rax, %rbx
	jmp	.L1023
.L1089:
	endbr64
	movq	%rax, %r13
	jmp	.L1045
.L1168:
	movq	40(%r14), %rdi
	movq	(%r14), %r12
	testq	%rdi, %rdi
	je	.L1019
	call	_ZdlPv@PLT
.L1019:
	movq	8(%r14), %rdi
	leaq	24(%r14), %rax
	cmpq	%rax, %rdi
	je	.L1020
	call	_ZdlPv@PLT
.L1020:
	movq	%r14, %rdi
	movq	%r12, %r14
	call	_ZdlPv@PLT
.L1021:
	testq	%r14, %r14
	jne	.L1168
	movq	80(%r13), %rax
	movq	72(%r13), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	$0, 96(%r13)
	movq	72(%r13), %rdi
	movq	$0, 88(%r13)
	cmpq	%rdi, -232(%rbp)
	je	.L1023
	call	_ZdlPv@PLT
.L1023:
	movl	$160, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1071:
	movq	-96(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L1074
.L1151:
	call	_ZdlPv@PLT
.L1074:
	movq	%rbx, %rdi
.LEHB104:
	call	_Unwind_Resume@PLT
.L1045:
	movq	88(%r12), %rbx
	movq	80(%r12), %r14
.L1048:
	cmpq	%r14, %rbx
	jne	.L1169
	movq	80(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1049
	call	_ZdlPv@PLT
.L1049:
	leaq	16+_ZTVN2v88internal6torque4TypeE(%rip), %rax
	movq	40(%r12), %rsi
	leaq	24(%r12), %rdi
	movq	%rax, (%r12)
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	-96(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L1051
	call	_ZdlPv@PLT
.L1051:
	movq	%r13, %rbx
.L1063:
	cmpb	$0, -144(%rbp)
	je	.L1037
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1037
	call	_ZdlPv@PLT
	jmp	.L1037
.L1086:
	endbr64
	movq	%rax, %rbx
	jmp	.L1017
.L1083:
	endbr64
	movq	%rax, %rbx
	jmp	.L1073
.L1017:
	leaq	16+_ZTVN2v88internal6torque5ScopeE(%rip), %rax
	movq	88(%r13), %r14
	movq	%rax, 0(%r13)
	jmp	.L1021
.L1061:
	cmpb	$0, 176(%r12)
	je	.L1062
	movq	192(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1062
	call	_ZdlPv@PLT
.L1062:
	movq	%r12, %rdi
	call	_ZN2v88internal6torque13AggregateTypeD2Ev
.L1037:
	movl	$216, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1073:
	cmpb	$0, -232(%rbp)
	je	.L1074
	cmpq	$0, -224(%rbp)
	je	.L1074
	movq	-224(%rbp), %rdi
	jmp	.L1151
.L1081:
	endbr64
	movq	%rax, %rbx
	jmp	.L1071
.L1090:
	endbr64
	movq	%rax, %rbx
	jmp	.L1061
.L1084:
	endbr64
.L1156:
	movq	%rax, %r12
	jmp	.L1077
.L1085:
	endbr64
	jmp	.L1156
.L1077:
	movq	-144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1078
	movq	(%rdi), %rax
	call	*8(%rax)
.L1078:
	movq	%r12, %rdi
	call	_Unwind_Resume@PLT
.LEHE104:
.L1087:
	endbr64
.L1155:
	movq	%rax, %rbx
	jmp	.L1063
.L1088:
	endbr64
	jmp	.L1155
.L1169:
	movq	48(%r14), %rdi
	leaq	64(%r14), %rax
	cmpq	%rax, %rdi
	je	.L1047
	call	_ZdlPv@PLT
.L1047:
	addq	$104, %r14
	jmp	.L1048
	.cfi_endproc
.LFE6447:
	.section	.gcc_except_table._ZN2v88internal6torque10TypeOracle13GetStructTypeEPKNS1_17StructDeclarationENS_4base8OptionalINS1_17SpecializationKeyINS1_17GenericStructTypeEEEEE,"aG",@progbits,_ZN2v88internal6torque10TypeOracle13GetStructTypeEPKNS1_17StructDeclarationENS_4base8OptionalINS1_17SpecializationKeyINS1_17GenericStructTypeEEEEE,comdat
.LLSDA6447:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6447-.LLSDACSB6447
.LLSDACSB6447:
	.uleb128 .LEHB87-.LFB6447
	.uleb128 .LEHE87-.LEHB87
	.uleb128 .L1081-.LFB6447
	.uleb128 0
	.uleb128 .LEHB88-.LFB6447
	.uleb128 .LEHE88-.LEHB88
	.uleb128 .L1082-.LFB6447
	.uleb128 0
	.uleb128 .LEHB89-.LFB6447
	.uleb128 .LEHE89-.LEHB89
	.uleb128 .L1083-.LFB6447
	.uleb128 0
	.uleb128 .LEHB90-.LFB6447
	.uleb128 .LEHE90-.LEHB90
	.uleb128 .L1087-.LFB6447
	.uleb128 0
	.uleb128 .LEHB91-.LFB6447
	.uleb128 .LEHE91-.LEHB91
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB92-.LFB6447
	.uleb128 .LEHE92-.LEHB92
	.uleb128 .L1083-.LFB6447
	.uleb128 0
	.uleb128 .LEHB93-.LFB6447
	.uleb128 .LEHE93-.LEHB93
	.uleb128 .L1088-.LFB6447
	.uleb128 0
	.uleb128 .LEHB94-.LFB6447
	.uleb128 .LEHE94-.LEHB94
	.uleb128 .L1086-.LFB6447
	.uleb128 0
	.uleb128 .LEHB95-.LFB6447
	.uleb128 .LEHE95-.LEHB95
	.uleb128 .L1090-.LFB6447
	.uleb128 0
	.uleb128 .LEHB96-.LFB6447
	.uleb128 .LEHE96-.LEHB96
	.uleb128 .L1089-.LFB6447
	.uleb128 0
	.uleb128 .LEHB97-.LFB6447
	.uleb128 .LEHE97-.LEHB97
	.uleb128 .L1085-.LFB6447
	.uleb128 0
	.uleb128 .LEHB98-.LFB6447
	.uleb128 .LEHE98-.LEHB98
	.uleb128 .L1084-.LFB6447
	.uleb128 0
	.uleb128 .LEHB99-.LFB6447
	.uleb128 .LEHE99-.LEHB99
	.uleb128 .L1086-.LFB6447
	.uleb128 0
	.uleb128 .LEHB100-.LFB6447
	.uleb128 .LEHE100-.LEHB100
	.uleb128 .L1089-.LFB6447
	.uleb128 0
	.uleb128 .LEHB101-.LFB6447
	.uleb128 .LEHE101-.LEHB101
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB102-.LFB6447
	.uleb128 .LEHE102-.LEHB102
	.uleb128 .L1088-.LFB6447
	.uleb128 0
	.uleb128 .LEHB103-.LFB6447
	.uleb128 .LEHE103-.LEHB103
	.uleb128 .L1090-.LFB6447
	.uleb128 0
	.uleb128 .LEHB104-.LFB6447
	.uleb128 .LEHE104-.LEHB104
	.uleb128 0
	.uleb128 0
.LLSDACSE6447:
	.section	.text._ZN2v88internal6torque10TypeOracle13GetStructTypeEPKNS1_17StructDeclarationENS_4base8OptionalINS1_17SpecializationKeyINS1_17GenericStructTypeEEEEE,"axG",@progbits,_ZN2v88internal6torque10TypeOracle13GetStructTypeEPKNS1_17StructDeclarationENS_4base8OptionalINS1_17SpecializationKeyINS1_17GenericStructTypeEEEEE,comdat
	.size	_ZN2v88internal6torque10TypeOracle13GetStructTypeEPKNS1_17StructDeclarationENS_4base8OptionalINS1_17SpecializationKeyINS1_17GenericStructTypeEEEEE, .-_ZN2v88internal6torque10TypeOracle13GetStructTypeEPKNS1_17StructDeclarationENS_4base8OptionalINS1_17SpecializationKeyINS1_17GenericStructTypeEEEEE
	.section	.text._ZNSt6vectorIPN2v88internal6torque10IdentifierESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_,"axG",@progbits,_ZNSt6vectorIPN2v88internal6torque10IdentifierESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPN2v88internal6torque10IdentifierESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	.type	_ZNSt6vectorIPN2v88internal6torque10IdentifierESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_, @function
_ZNSt6vectorIPN2v88internal6torque10IdentifierESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_:
.LFB10265:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r9
	movq	%r13, %rax
	subq	%r9, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L1184
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L1180
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L1185
.L1172:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L1179:
	movq	(%r15), %rax
	subq	%r8, %r13
	leaq	8(%rbx,%rdx), %r15
	movq	%rax, (%rbx,%rdx)
	leaq	(%r15,%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L1186
	testq	%r13, %r13
	jg	.L1175
	testq	%r9, %r9
	jne	.L1178
.L1176:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1186:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jg	.L1175
.L1178:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L1176
	.p2align 4,,10
	.p2align 3
.L1185:
	testq	%rsi, %rsi
	jne	.L1173
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L1179
	.p2align 4,,10
	.p2align 3
.L1175:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L1176
	jmp	.L1178
	.p2align 4,,10
	.p2align 3
.L1180:
	movl	$8, %r14d
	jmp	.L1172
.L1184:
	leaq	.LC15(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1173:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$3, %r14
	jmp	.L1172
	.cfi_endproc
.LFE10265:
	.size	_ZNSt6vectorIPN2v88internal6torque10IdentifierESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_, .-_ZNSt6vectorIPN2v88internal6torque10IdentifierESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	.section	.text._ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE7_M_copyINSB_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS5_EPKSF_PSt18_Rb_tree_node_baseRT_,"axG",@progbits,_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE7_M_copyINSB_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS5_EPKSF_PSt18_Rb_tree_node_baseRT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE7_M_copyINSB_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS5_EPKSF_PSt18_Rb_tree_node_baseRT_
	.type	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE7_M_copyINSB_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS5_EPKSF_PSt18_Rb_tree_node_baseRT_, @function
_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE7_M_copyINSB_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS5_EPKSF_PSt18_Rb_tree_node_baseRT_:
.LFB10502:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA10502
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$56, %rsp
	movq	%rdi, -80(%rbp)
	movl	$64, %edi
	movq	%rcx, -88(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
.LEHB105:
	call	_Znwm@PLT
.LEHE105:
	movq	32(%rbx), %r13
	movq	40(%rbx), %r12
	leaq	48(%rax), %rdi
	movq	%rax, %r14
	movq	%rdi, 32(%rax)
	movq	%r13, %rax
	addq	%r12, %rax
	je	.L1188
	testq	%r13, %r13
	je	.L1241
.L1188:
	movq	%r12, -64(%rbp)
	cmpq	$15, %r12
	ja	.L1242
	cmpq	$1, %r12
	jne	.L1191
	movzbl	0(%r13), %eax
	movb	%al, 48(%r14)
.L1192:
	movq	%r12, 40(%r14)
	movb	$0, (%rdi,%r12)
	movl	(%rbx), %eax
	movq	24(%rbx), %rsi
	movq	$0, 16(%r14)
	movl	%eax, (%r14)
	movq	$0, 24(%r14)
	movq	%r15, 8(%r14)
	testq	%rsi, %rsi
	je	.L1194
	movq	-88(%rbp), %rcx
	movq	-80(%rbp), %rdi
	movq	%r14, %rdx
.LEHB106:
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE7_M_copyINSB_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS5_EPKSF_PSt18_Rb_tree_node_baseRT_
	movq	%rax, 24(%r14)
.L1194:
	movq	16(%rbx), %rbx
	leaq	-64(%rbp), %rax
	movq	%r14, %r12
	movq	%rax, -96(%rbp)
	testq	%rbx, %rbx
	je	.L1187
.L1197:
	movl	$64, %edi
	movq	%r12, -72(%rbp)
	call	_Znwm@PLT
	leaq	48(%rax), %rdi
	movq	%rax, %r12
	movq	%rdi, 32(%rax)
	movq	32(%rbx), %r15
	movq	40(%rbx), %r13
	movq	%r15, %rax
	addq	%r13, %rax
	je	.L1198
	testq	%r15, %r15
	je	.L1243
.L1198:
	movq	%r13, -64(%rbp)
	cmpq	$15, %r13
	ja	.L1244
	cmpq	$1, %r13
	jne	.L1201
	movzbl	(%r15), %eax
	movb	%al, 48(%r12)
.L1202:
	movq	%r13, 40(%r12)
	movb	$0, (%rdi,%r13)
	movl	(%rbx), %eax
	movq	$0, 16(%r12)
	movl	%eax, (%r12)
	movq	-72(%rbp), %rax
	movq	$0, 24(%r12)
	movq	%r12, 16(%rax)
	movq	%rax, 8(%r12)
	movq	24(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L1204
	movq	-88(%rbp), %rcx
	movq	-80(%rbp), %rdi
	movq	%r12, %rdx
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE7_M_copyINSB_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS5_EPKSF_PSt18_Rb_tree_node_baseRT_
.LEHE106:
	movq	%rax, 24(%r12)
.L1204:
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L1197
.L1187:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1245
	addq	$56, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1191:
	.cfi_restore_state
	testq	%r12, %r12
	je	.L1192
	jmp	.L1190
	.p2align 4,,10
	.p2align 3
.L1201:
	testq	%r13, %r13
	je	.L1202
	jmp	.L1200
	.p2align 4,,10
	.p2align 3
.L1244:
	movq	-96(%rbp), %rsi
	leaq	32(%r12), %rdi
	xorl	%edx, %edx
.LEHB107:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE107:
	movq	%rax, 32(%r12)
	movq	%rax, %rdi
	movq	-64(%rbp), %rax
	movq	%rax, 48(%r12)
.L1200:
	movq	%r13, %rdx
	movq	%r15, %rsi
	call	memcpy@PLT
	movq	-64(%rbp), %r13
	movq	32(%r12), %rdi
	jmp	.L1202
	.p2align 4,,10
	.p2align 3
.L1242:
	leaq	-64(%rbp), %rsi
	leaq	32(%r14), %rdi
	xorl	%edx, %edx
.LEHB108:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE108:
	movq	%rax, 32(%r14)
	movq	%rax, %rdi
	movq	-64(%rbp), %rax
	movq	%rax, 48(%r14)
.L1190:
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	-64(%rbp), %r12
	movq	32(%r14), %rdi
	jmp	.L1192
.L1243:
	leaq	.LC4(%rip), %rdi
.LEHB109:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE109:
.L1245:
	call	__stack_chk_fail@PLT
.L1241:
	leaq	.LC4(%rip), %rdi
.LEHB110:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE110:
.L1216:
	endbr64
	movq	%rax, %rdi
	jmp	.L1205
.L1212:
	endbr64
	movq	%rax, %rdi
	jmp	.L1207
.L1214:
	endbr64
	movq	%rax, %rdi
	jmp	.L1195
.L1205:
	call	__cxa_begin_catch@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.LEHB111:
	call	__cxa_rethrow@PLT
.LEHE111:
.L1206:
	call	__cxa_end_catch@PLT
	movq	%rbx, %rdi
.L1207:
	call	__cxa_begin_catch@PLT
	movq	-80(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
.LEHB112:
	call	__cxa_rethrow@PLT
.LEHE112:
.L1195:
	call	__cxa_begin_catch@PLT
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.LEHB113:
	call	__cxa_rethrow@PLT
.LEHE113:
.L1217:
	endbr64
	movq	%rax, %rbx
	jmp	.L1206
.L1213:
	endbr64
	movq	%rax, %r12
	jmp	.L1209
.L1215:
	endbr64
	movq	%rax, %r12
	jmp	.L1196
.L1209:
	call	__cxa_end_catch@PLT
	movq	%r12, %rdi
.LEHB114:
	call	_Unwind_Resume@PLT
.LEHE114:
.L1196:
	call	__cxa_end_catch@PLT
	movq	%r12, %rdi
.LEHB115:
	call	_Unwind_Resume@PLT
.LEHE115:
	.cfi_endproc
.LFE10502:
	.section	.gcc_except_table._ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE7_M_copyINSB_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS5_EPKSF_PSt18_Rb_tree_node_baseRT_,"aG",@progbits,_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE7_M_copyINSB_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS5_EPKSF_PSt18_Rb_tree_node_baseRT_,comdat
	.align 4
.LLSDA10502:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATT10502-.LLSDATTD10502
.LLSDATTD10502:
	.byte	0x1
	.uleb128 .LLSDACSE10502-.LLSDACSB10502
.LLSDACSB10502:
	.uleb128 .LEHB105-.LFB10502
	.uleb128 .LEHE105-.LEHB105
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB106-.LFB10502
	.uleb128 .LEHE106-.LEHB106
	.uleb128 .L1212-.LFB10502
	.uleb128 0x1
	.uleb128 .LEHB107-.LFB10502
	.uleb128 .LEHE107-.LEHB107
	.uleb128 .L1216-.LFB10502
	.uleb128 0x1
	.uleb128 .LEHB108-.LFB10502
	.uleb128 .LEHE108-.LEHB108
	.uleb128 .L1214-.LFB10502
	.uleb128 0x1
	.uleb128 .LEHB109-.LFB10502
	.uleb128 .LEHE109-.LEHB109
	.uleb128 .L1216-.LFB10502
	.uleb128 0x1
	.uleb128 .LEHB110-.LFB10502
	.uleb128 .LEHE110-.LEHB110
	.uleb128 .L1214-.LFB10502
	.uleb128 0x1
	.uleb128 .LEHB111-.LFB10502
	.uleb128 .LEHE111-.LEHB111
	.uleb128 .L1217-.LFB10502
	.uleb128 0x3
	.uleb128 .LEHB112-.LFB10502
	.uleb128 .LEHE112-.LEHB112
	.uleb128 .L1213-.LFB10502
	.uleb128 0
	.uleb128 .LEHB113-.LFB10502
	.uleb128 .LEHE113-.LEHB113
	.uleb128 .L1215-.LFB10502
	.uleb128 0
	.uleb128 .LEHB114-.LFB10502
	.uleb128 .LEHE114-.LEHB114
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB115-.LFB10502
	.uleb128 .LEHE115-.LEHB115
	.uleb128 0
	.uleb128 0
.LLSDACSE10502:
	.byte	0x1
	.byte	0
	.byte	0
	.byte	0x7d
	.align 4
	.long	0

.LLSDATT10502:
	.section	.text._ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE7_M_copyINSB_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS5_EPKSF_PSt18_Rb_tree_node_baseRT_,"axG",@progbits,_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE7_M_copyINSB_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS5_EPKSF_PSt18_Rb_tree_node_baseRT_,comdat
	.size	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE7_M_copyINSB_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS5_EPKSF_PSt18_Rb_tree_node_baseRT_, .-_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE7_M_copyINSB_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS5_EPKSF_PSt18_Rb_tree_node_baseRT_
	.section	.text._ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE7_M_copyINSA_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS5_EPKSE_PSt18_Rb_tree_node_baseRT_,"axG",@progbits,_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE7_M_copyINSA_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS5_EPKSE_PSt18_Rb_tree_node_baseRT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE7_M_copyINSA_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS5_EPKSE_PSt18_Rb_tree_node_baseRT_
	.type	_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE7_M_copyINSA_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS5_EPKSE_PSt18_Rb_tree_node_baseRT_, @function
_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE7_M_copyINSA_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS5_EPKSE_PSt18_Rb_tree_node_baseRT_:
.LFB10509:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA10509
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	movl	$40, %edi
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	%rcx, -56(%rbp)
.LEHB116:
	call	_Znwm@PLT
.LEHE116:
	movq	24(%rbx), %rsi
	movq	%rax, %r13
	movq	32(%rbx), %rax
	movq	$0, 16(%r13)
	movq	%rax, 32(%r13)
	movl	(%rbx), %eax
	movq	$0, 24(%r13)
	movl	%eax, 0(%r13)
	movq	%r12, 8(%r13)
	testq	%rsi, %rsi
	je	.L1247
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r15, %rdi
.LEHB117:
	call	_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE7_M_copyINSA_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS5_EPKSE_PSt18_Rb_tree_node_baseRT_
	movq	%rax, 24(%r13)
.L1247:
	movq	16(%rbx), %r12
	testq	%r12, %r12
	je	.L1246
	movq	%r13, %rbx
.L1250:
	movl	$40, %edi
	movq	%rbx, %r14
	call	_Znwm@PLT
	movq	%rax, %rbx
	movq	32(%r12), %rax
	movq	%rax, 32(%rbx)
	movl	(%r12), %eax
	movq	$0, 16(%rbx)
	movl	%eax, (%rbx)
	movq	$0, 24(%rbx)
	movq	%rbx, 16(%r14)
	movq	%r14, 8(%rbx)
	movq	24(%r12), %rsi
	testq	%rsi, %rsi
	je	.L1249
	movq	-56(%rbp), %rcx
	movq	%rbx, %rdx
	movq	%r15, %rdi
	call	_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE7_M_copyINSA_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS5_EPKSE_PSt18_Rb_tree_node_baseRT_
.LEHE117:
	movq	%rax, 24(%rbx)
.L1249:
	movq	16(%r12), %r12
	testq	%r12, %r12
	jne	.L1250
.L1246:
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1253:
	.cfi_restore_state
	endbr64
	movq	%rax, %rdi
.L1251:
	call	__cxa_begin_catch@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
.LEHB118:
	call	__cxa_rethrow@PLT
.LEHE118:
.L1254:
	endbr64
	movq	%rax, %r12
.L1252:
	call	__cxa_end_catch@PLT
	movq	%r12, %rdi
.LEHB119:
	call	_Unwind_Resume@PLT
.LEHE119:
	.cfi_endproc
.LFE10509:
	.section	.gcc_except_table._ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE7_M_copyINSA_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS5_EPKSE_PSt18_Rb_tree_node_baseRT_,"aG",@progbits,_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE7_M_copyINSA_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS5_EPKSE_PSt18_Rb_tree_node_baseRT_,comdat
	.align 4
.LLSDA10509:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATT10509-.LLSDATTD10509
.LLSDATTD10509:
	.byte	0x1
	.uleb128 .LLSDACSE10509-.LLSDACSB10509
.LLSDACSB10509:
	.uleb128 .LEHB116-.LFB10509
	.uleb128 .LEHE116-.LEHB116
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB117-.LFB10509
	.uleb128 .LEHE117-.LEHB117
	.uleb128 .L1253-.LFB10509
	.uleb128 0x1
	.uleb128 .LEHB118-.LFB10509
	.uleb128 .LEHE118-.LEHB118
	.uleb128 .L1254-.LFB10509
	.uleb128 0
	.uleb128 .LEHB119-.LFB10509
	.uleb128 .LEHE119-.LEHB119
	.uleb128 0
	.uleb128 0
.LLSDACSE10509:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATT10509:
	.section	.text._ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE7_M_copyINSA_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS5_EPKSE_PSt18_Rb_tree_node_baseRT_,"axG",@progbits,_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE7_M_copyINSA_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS5_EPKSE_PSt18_Rb_tree_node_baseRT_,comdat
	.size	_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE7_M_copyINSA_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS5_EPKSE_PSt18_Rb_tree_node_baseRT_, .-_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE7_M_copyINSA_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS5_EPKSE_PSt18_Rb_tree_node_baseRT_
	.section	.text._ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS5_ERKS5_,"axG",@progbits,_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS5_ERKS5_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS5_ERKS5_
	.type	_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS5_ERKS5_, @function
_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS5_ERKS5_:
.LFB10530:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	8(%rdi), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	cmpq	%rsi, %r14
	je	.L1316
	movq	32(%rsi), %rsi
	movq	(%rdx), %rdi
	call	_ZN2v88internal6torqueltERKNS1_4TypeES4_@PLT
	testb	%al, %al
	je	.L1277
	movq	24(%r12), %rax
	movq	%rax, %rdx
	cmpq	%r15, %rax
	je	.L1269
	movq	%r15, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	0(%r13), %rsi
	movq	32(%rax), %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal6torqueltERKNS1_4TypeES4_@PLT
	testb	%al, %al
	je	.L1279
	cmpq	$0, 24(%rbx)
	movl	$0, %eax
	movq	%r15, %rdx
	cmovne	%r15, %rax
	cmove	%rbx, %rdx
.L1269:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1316:
	.cfi_restore_state
	cmpq	$0, 40(%rdi)
	je	.L1268
	movq	32(%rdi), %rax
	movq	(%rdx), %rsi
	movq	32(%rax), %rdi
	call	_ZN2v88internal6torqueltERKNS1_4TypeES4_@PLT
	testb	%al, %al
	jne	.L1317
.L1268:
	movq	16(%r12), %rbx
	testq	%rbx, %rbx
	jne	.L1271
	jmp	.L1318
	.p2align 4,,10
	.p2align 3
.L1319:
	movq	16(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L1294
.L1320:
	movq	%rdx, %rbx
.L1271:
	movq	32(%rbx), %rsi
	movq	0(%r13), %rdi
	call	_ZN2v88internal6torqueltERKNS1_4TypeES4_@PLT
	testb	%al, %al
	jne	.L1319
	movq	24(%rbx), %rdx
	testq	%rdx, %rdx
	jne	.L1320
.L1294:
	testb	%al, %al
	je	.L1321
.L1292:
	cmpq	%rbx, 24(%r12)
	je	.L1309
	movq	%rbx, %rdi
	movq	%rbx, %r15
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	%rax, %rbx
.L1297:
	movq	32(%rbx), %rdi
	movq	0(%r13), %rsi
	call	_ZN2v88internal6torqueltERKNS1_4TypeES4_@PLT
	xorl	%edx, %edx
	testb	%al, %al
	cmovne	%rdx, %rbx
	cmove	%rdx, %r15
.L1298:
	addq	$8, %rsp
	movq	%rbx, %rax
	movq	%r15, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1277:
	.cfi_restore_state
	movq	32(%r15), %rdi
	movq	0(%r13), %rsi
	call	_ZN2v88internal6torqueltERKNS1_4TypeES4_@PLT
	testb	%al, %al
	je	.L1288
	movq	32(%r12), %rdx
	xorl	%eax, %eax
	cmpq	%r15, %rdx
	je	.L1269
	movq	%r15, %rdi
	call	_ZSt18_Rb_tree_incrementPSt18_Rb_tree_node_base@PLT
	movq	0(%r13), %rdi
	movq	32(%rax), %rsi
	movq	%rax, %rbx
	call	_ZN2v88internal6torqueltERKNS1_4TypeES4_@PLT
	testb	%al, %al
	je	.L1290
	cmpq	$0, 24(%r15)
	movl	$0, %eax
	movq	%r15, %rdx
	cmovne	%rbx, %rax
	cmovne	%rbx, %rdx
	jmp	.L1269
	.p2align 4,,10
	.p2align 3
.L1317:
	movq	32(%r12), %rdx
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1321:
	.cfi_restore_state
	movq	%rbx, %r15
	jmp	.L1297
	.p2align 4,,10
	.p2align 3
.L1288:
	movq	%r15, %rax
	xorl	%edx, %edx
	jmp	.L1269
	.p2align 4,,10
	.p2align 3
.L1309:
	movq	%rbx, %r15
	xorl	%ebx, %ebx
	jmp	.L1298
	.p2align 4,,10
	.p2align 3
.L1279:
	movq	16(%r12), %rbx
	testq	%rbx, %rbx
	jne	.L1282
	jmp	.L1307
	.p2align 4,,10
	.p2align 3
.L1322:
	movq	16(%rbx), %rdx
.L1285:
	testq	%rdx, %rdx
	je	.L1294
	movq	%rdx, %rbx
.L1282:
	movq	32(%rbx), %rsi
	movq	0(%r13), %rdi
	call	_ZN2v88internal6torqueltERKNS1_4TypeES4_@PLT
	testb	%al, %al
	jne	.L1322
	movq	24(%rbx), %rdx
	jmp	.L1285
	.p2align 4,,10
	.p2align 3
.L1318:
	movq	%r15, %rbx
	jmp	.L1292
	.p2align 4,,10
	.p2align 3
.L1290:
	movq	16(%r12), %rbx
	testq	%rbx, %rbx
	jne	.L1293
	jmp	.L1307
	.p2align 4,,10
	.p2align 3
.L1323:
	movq	16(%rbx), %rdx
.L1296:
	testq	%rdx, %rdx
	je	.L1294
	movq	%rdx, %rbx
.L1293:
	movq	32(%rbx), %rsi
	movq	0(%r13), %rdi
	call	_ZN2v88internal6torqueltERKNS1_4TypeES4_@PLT
	testb	%al, %al
	jne	.L1323
	movq	24(%rbx), %rdx
	jmp	.L1296
.L1307:
	movq	%r14, %rbx
	jmp	.L1292
	.cfi_endproc
.LFE10530:
	.size	_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS5_ERKS5_, .-_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS5_ERKS5_
	.section	.text._ZNSt6vectorISt10unique_ptrIN2v88internal6torque7AstNodeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_,"axG",@progbits,_ZNSt6vectorISt10unique_ptrIN2v88internal6torque7AstNodeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorISt10unique_ptrIN2v88internal6torque7AstNodeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	.type	_ZNSt6vectorISt10unique_ptrIN2v88internal6torque7AstNodeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_, @function
_ZNSt6vectorISt10unique_ptrIN2v88internal6torque7AstNodeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_:
.LFB11425:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rax
	movq	(%rdi), %r12
	movq	%rdi, -80(%rbp)
	movq	%rax, -72(%rbp)
	subq	%r12, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L1348
	movq	%rsi, %r9
	movq	%rsi, %r15
	movq	%rsi, %rbx
	subq	%r12, %r9
	testq	%rax, %rax
	je	.L1339
	movabsq	$9223372036854775800, %r13
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L1349
.L1326:
	movq	%r13, %rdi
	movq	%rdx, -96(%rbp)
	movq	%r9, -88(%rbp)
	call	_Znwm@PLT
	movq	-88(%rbp), %r9
	movq	-96(%rbp), %rdx
	addq	%rax, %r13
	movq	%rax, -56(%rbp)
	leaq	8(%rax), %rcx
	movq	%r13, -64(%rbp)
.L1338:
	movq	(%rdx), %rax
	movq	-56(%rbp), %rsi
	movq	$0, (%rdx)
	movq	%rax, (%rsi,%r9)
	cmpq	%r12, %r15
	je	.L1328
	movq	%rsi, %r13
	movq	%r12, %r14
	.p2align 4,,10
	.p2align 3
.L1332:
	movq	(%r14), %rcx
	movq	$0, (%r14)
	movq	%rcx, 0(%r13)
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1329
	movq	(%rdi), %rcx
	addq	$8, %r14
	addq	$8, %r13
	call	*8(%rcx)
	cmpq	%r14, %r15
	jne	.L1332
.L1330:
	movq	-56(%rbp), %rsi
	movq	%r15, %rax
	subq	%r12, %rax
	leaq	8(%rsi,%rax), %rcx
.L1328:
	movq	-72(%rbp), %rax
	cmpq	%rax, %r15
	je	.L1333
	movq	%rax, %r14
	subq	%r15, %r14
	leaq	-8(%r14), %r9
	movq	%r9, %rdi
	shrq	$3, %rdi
	addq	$1, %rdi
	testq	%r9, %r9
	je	.L1341
	movq	%rdi, %rdx
	xorl	%eax, %eax
	shrq	%rdx
	salq	$4, %rdx
	.p2align 4,,10
	.p2align 3
.L1335:
	movdqu	(%r15,%rax), %xmm1
	movups	%xmm1, (%rcx,%rax)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L1335
	movq	%rdi, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %r8
	leaq	(%rcx,%r8), %rdx
	leaq	(%r15,%r8), %rbx
	cmpq	%rax, %rdi
	je	.L1336
.L1334:
	movq	(%rbx), %rax
	movq	%rax, (%rdx)
.L1336:
	leaq	8(%rcx,%r9), %rcx
.L1333:
	testq	%r12, %r12
	je	.L1337
	movq	%r12, %rdi
	movq	%rcx, -72(%rbp)
	call	_ZdlPv@PLT
	movq	-72(%rbp), %rcx
.L1337:
	movq	-56(%rbp), %xmm0
	movq	-80(%rbp), %rax
	movq	%rcx, %xmm2
	movq	-64(%rbp), %rbx
	punpcklqdq	%xmm2, %xmm0
	movq	%rbx, 16(%rax)
	movups	%xmm0, (%rax)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1329:
	.cfi_restore_state
	addq	$8, %r14
	addq	$8, %r13
	cmpq	%r14, %r15
	jne	.L1332
	jmp	.L1330
	.p2align 4,,10
	.p2align 3
.L1349:
	testq	%rdi, %rdi
	jne	.L1327
	movq	$0, -64(%rbp)
	movl	$8, %ecx
	movq	$0, -56(%rbp)
	jmp	.L1338
	.p2align 4,,10
	.p2align 3
.L1339:
	movl	$8, %r13d
	jmp	.L1326
.L1341:
	movq	%rcx, %rdx
	jmp	.L1334
.L1327:
	cmpq	%rcx, %rdi
	movq	%rcx, %rax
	cmovbe	%rdi, %rax
	leaq	0(,%rax,8), %r13
	jmp	.L1326
.L1348:
	leaq	.LC15(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE11425:
	.size	_ZNSt6vectorISt10unique_ptrIN2v88internal6torque7AstNodeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_, .-_ZNSt6vectorISt10unique_ptrIN2v88internal6torque7AstNodeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	.section	.text._ZNSt10_HashtableIN2v88internal6torque18BuiltinPointerTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_rehashEmRKm,"axG",@progbits,_ZNSt10_HashtableIN2v88internal6torque18BuiltinPointerTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_rehashEmRKm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIN2v88internal6torque18BuiltinPointerTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_rehashEmRKm
	.type	_ZNSt10_HashtableIN2v88internal6torque18BuiltinPointerTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_rehashEmRKm, @function
_ZNSt10_HashtableIN2v88internal6torque18BuiltinPointerTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_rehashEmRKm:
.LFB11681:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA11681
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	cmpq	$1, %rsi
	je	.L1372
	movabsq	$1152921504606846975, %rax
	movq	%rdx, %r13
	cmpq	%rax, %rsi
	ja	.L1373
	leaq	0(,%rsi,8), %r14
	movq	%r14, %rdi
.LEHB120:
	call	_Znwm@PLT
	movq	%r14, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	memset@PLT
	leaq	48(%rbx), %r9
.L1352:
	movq	16(%rbx), %rsi
	movq	$0, 16(%rbx)
	testq	%rsi, %rsi
	je	.L1354
	xorl	%r8d, %r8d
	leaq	16(%rbx), %r10
	jmp	.L1355
	.p2align 4,,10
	.p2align 3
.L1356:
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	(%rdi), %rax
	movq	%rcx, (%rax)
.L1357:
	testq	%rsi, %rsi
	je	.L1354
.L1355:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	120(%rcx), %rax
	divq	%r12
	leaq	0(%r13,%rdx,8), %rdi
	movq	(%rdi), %rax
	testq	%rax, %rax
	jne	.L1356
	movq	16(%rbx), %rax
	movq	%rax, (%rcx)
	movq	%rcx, 16(%rbx)
	movq	%r10, (%rdi)
	cmpq	$0, (%rcx)
	je	.L1361
	movq	%rcx, 0(%r13,%r8,8)
	movq	%rdx, %r8
	testq	%rsi, %rsi
	jne	.L1355
	.p2align 4,,10
	.p2align 3
.L1354:
	movq	(%rbx), %rdi
	cmpq	%r9, %rdi
	je	.L1358
	call	_ZdlPv@PLT
.L1358:
	movq	%r12, 8(%rbx)
	movq	%r13, (%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1361:
	.cfi_restore_state
	movq	%rdx, %r8
	jmp	.L1357
	.p2align 4,,10
	.p2align 3
.L1372:
	leaq	48(%rdi), %r13
	movq	$0, 48(%rdi)
	movq	%r13, %r9
	jmp	.L1352
.L1373:
	call	_ZSt17__throw_bad_allocv@PLT
.LEHE120:
.L1362:
	endbr64
	movq	%rax, %rdi
.L1359:
	call	__cxa_begin_catch@PLT
	movq	0(%r13), %rax
	movq	%rax, 40(%rbx)
.LEHB121:
	call	__cxa_rethrow@PLT
.LEHE121:
.L1363:
	endbr64
	movq	%rax, %r12
.L1360:
	call	__cxa_end_catch@PLT
	movq	%r12, %rdi
.LEHB122:
	call	_Unwind_Resume@PLT
.LEHE122:
	.cfi_endproc
.LFE11681:
	.section	.gcc_except_table._ZNSt10_HashtableIN2v88internal6torque18BuiltinPointerTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_rehashEmRKm,"aG",@progbits,_ZNSt10_HashtableIN2v88internal6torque18BuiltinPointerTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_rehashEmRKm,comdat
	.align 4
.LLSDA11681:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATT11681-.LLSDATTD11681
.LLSDATTD11681:
	.byte	0x1
	.uleb128 .LLSDACSE11681-.LLSDACSB11681
.LLSDACSB11681:
	.uleb128 .LEHB120-.LFB11681
	.uleb128 .LEHE120-.LEHB120
	.uleb128 .L1362-.LFB11681
	.uleb128 0x1
	.uleb128 .LEHB121-.LFB11681
	.uleb128 .LEHE121-.LEHB121
	.uleb128 .L1363-.LFB11681
	.uleb128 0
	.uleb128 .LEHB122-.LFB11681
	.uleb128 .LEHE122-.LEHB122
	.uleb128 0
	.uleb128 0
.LLSDACSE11681:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATT11681:
	.section	.text._ZNSt10_HashtableIN2v88internal6torque18BuiltinPointerTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_rehashEmRKm,"axG",@progbits,_ZNSt10_HashtableIN2v88internal6torque18BuiltinPointerTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_rehashEmRKm,comdat
	.size	_ZNSt10_HashtableIN2v88internal6torque18BuiltinPointerTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_rehashEmRKm, .-_ZNSt10_HashtableIN2v88internal6torque18BuiltinPointerTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_rehashEmRKm
	.section	.text._ZNSt10_HashtableIN2v88internal6torque9UnionTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_rehashEmRKm,"axG",@progbits,_ZNSt10_HashtableIN2v88internal6torque9UnionTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_rehashEmRKm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIN2v88internal6torque9UnionTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_rehashEmRKm
	.type	_ZNSt10_HashtableIN2v88internal6torque9UnionTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_rehashEmRKm, @function
_ZNSt10_HashtableIN2v88internal6torque9UnionTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_rehashEmRKm:
.LFB11705:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA11705
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	cmpq	$1, %rsi
	je	.L1396
	movabsq	$1152921504606846975, %rax
	movq	%rdx, %r13
	cmpq	%rax, %rsi
	ja	.L1397
	leaq	0(,%rsi,8), %r14
	movq	%r14, %rdi
.LEHB123:
	call	_Znwm@PLT
	movq	%r14, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	memset@PLT
	leaq	48(%rbx), %r9
.L1376:
	movq	16(%rbx), %rsi
	movq	$0, 16(%rbx)
	testq	%rsi, %rsi
	je	.L1378
	xorl	%r8d, %r8d
	leaq	16(%rbx), %r10
	jmp	.L1379
	.p2align 4,,10
	.p2align 3
.L1380:
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	(%rdi), %rax
	movq	%rcx, (%rax)
.L1381:
	testq	%rsi, %rsi
	je	.L1378
.L1379:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	128(%rcx), %rax
	divq	%r12
	leaq	0(%r13,%rdx,8), %rdi
	movq	(%rdi), %rax
	testq	%rax, %rax
	jne	.L1380
	movq	16(%rbx), %rax
	movq	%rax, (%rcx)
	movq	%rcx, 16(%rbx)
	movq	%r10, (%rdi)
	cmpq	$0, (%rcx)
	je	.L1385
	movq	%rcx, 0(%r13,%r8,8)
	movq	%rdx, %r8
	testq	%rsi, %rsi
	jne	.L1379
	.p2align 4,,10
	.p2align 3
.L1378:
	movq	(%rbx), %rdi
	cmpq	%r9, %rdi
	je	.L1382
	call	_ZdlPv@PLT
.L1382:
	movq	%r12, 8(%rbx)
	movq	%r13, (%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1385:
	.cfi_restore_state
	movq	%rdx, %r8
	jmp	.L1381
	.p2align 4,,10
	.p2align 3
.L1396:
	leaq	48(%rdi), %r13
	movq	$0, 48(%rdi)
	movq	%r13, %r9
	jmp	.L1376
.L1397:
	call	_ZSt17__throw_bad_allocv@PLT
.LEHE123:
.L1386:
	endbr64
	movq	%rax, %rdi
.L1383:
	call	__cxa_begin_catch@PLT
	movq	0(%r13), %rax
	movq	%rax, 40(%rbx)
.LEHB124:
	call	__cxa_rethrow@PLT
.LEHE124:
.L1387:
	endbr64
	movq	%rax, %r12
.L1384:
	call	__cxa_end_catch@PLT
	movq	%r12, %rdi
.LEHB125:
	call	_Unwind_Resume@PLT
.LEHE125:
	.cfi_endproc
.LFE11705:
	.section	.gcc_except_table._ZNSt10_HashtableIN2v88internal6torque9UnionTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_rehashEmRKm,"aG",@progbits,_ZNSt10_HashtableIN2v88internal6torque9UnionTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_rehashEmRKm,comdat
	.align 4
.LLSDA11705:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATT11705-.LLSDATTD11705
.LLSDATTD11705:
	.byte	0x1
	.uleb128 .LLSDACSE11705-.LLSDACSB11705
.LLSDACSB11705:
	.uleb128 .LEHB123-.LFB11705
	.uleb128 .LEHE123-.LEHB123
	.uleb128 .L1386-.LFB11705
	.uleb128 0x1
	.uleb128 .LEHB124-.LFB11705
	.uleb128 .LEHE124-.LEHB124
	.uleb128 .L1387-.LFB11705
	.uleb128 0
	.uleb128 .LEHB125-.LFB11705
	.uleb128 .LEHE125-.LEHB125
	.uleb128 0
	.uleb128 0
.LLSDACSE11705:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATT11705:
	.section	.text._ZNSt10_HashtableIN2v88internal6torque9UnionTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_rehashEmRKm,"axG",@progbits,_ZNSt10_HashtableIN2v88internal6torque9UnionTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_rehashEmRKm,comdat
	.size	_ZNSt10_HashtableIN2v88internal6torque9UnionTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_rehashEmRKm, .-_ZNSt10_HashtableIN2v88internal6torque9UnionTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_rehashEmRKm
	.section	.text._ZN2v88internal6torque9UnionTypeD2Ev,"axG",@progbits,_ZN2v88internal6torque9UnionTypeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque9UnionTypeD2Ev
	.type	_ZN2v88internal6torque9UnionTypeD2Ev, @function
_ZN2v88internal6torque9UnionTypeD2Ev:
.LFB12558:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal6torque9UnionTypeE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	88(%rdi), %rbx
	movq	%rax, (%rdi)
	testq	%rbx, %rbx
	je	.L1399
	leaq	72(%rdi), %r13
.L1400:
	movq	24(%rbx), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	%rbx, %rdi
	movq	16(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L1400
.L1399:
	leaq	16+_ZTVN2v88internal6torque4TypeE(%rip), %rax
	leaq	24(%r12), %r13
	movq	%rax, (%r12)
	movq	40(%r12), %r12
	testq	%r12, %r12
	je	.L1398
.L1404:
	movq	24(%r12), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r12), %rdi
	leaq	48(%r12), %rax
	movq	16(%r12), %rbx
	cmpq	%rax, %rdi
	je	.L1402
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L1398
.L1403:
	movq	%rbx, %r12
	jmp	.L1404
	.p2align 4,,10
	.p2align 3
.L1402:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L1403
.L1398:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE12558:
	.size	_ZN2v88internal6torque9UnionTypeD2Ev, .-_ZN2v88internal6torque9UnionTypeD2Ev
	.weak	_ZN2v88internal6torque9UnionTypeD1Ev
	.set	_ZN2v88internal6torque9UnionTypeD1Ev,_ZN2v88internal6torque9UnionTypeD2Ev
	.section	.text._ZNSt10_HashtableIN2v88internal6torque9UnionTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb1EEEm,"axG",@progbits,_ZNSt10_HashtableIN2v88internal6torque9UnionTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb1EEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIN2v88internal6torque9UnionTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb1EEEm
	.type	_ZNSt10_HashtableIN2v88internal6torque9UnionTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb1EEEm, @function
_ZNSt10_HashtableIN2v88internal6torque9UnionTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb1EEEm:
.LFB11335:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA11335
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rcx, %r12
	movq	%r8, %rcx
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	addq	$32, %rdi
	subq	$16, %rsp
	movq	-8(%rdi), %rdx
	movq	-24(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	%rax, -48(%rbp)
.LEHB126:
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
.LEHE126:
	testb	%al, %al
	je	.L1420
	movq	%rdx, %rsi
	movq	%rbx, %rdi
	leaq	-48(%rbp), %rdx
.LEHB127:
	call	_ZNSt10_HashtableIN2v88internal6torque9UnionTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_rehashEmRKm
.LEHE127:
	movq	%r14, %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	%rdx, %r13
.L1420:
	movq	%r14, 128(%r12)
	movq	(%rbx), %rax
	leaq	0(,%r13,8), %rcx
	movq	(%rax,%r13,8), %rax
	testq	%rax, %rax
	je	.L1421
	movq	(%rax), %rax
	movq	%rax, (%r12)
	movq	(%rbx), %rax
	movq	(%rax,%r13,8), %rax
	movq	%r12, (%rax)
.L1422:
	addq	$1, 24(%rbx)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1434
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1421:
	.cfi_restore_state
	movq	16(%rbx), %rax
	movq	%rax, (%r12)
	movq	%r12, 16(%rbx)
	movq	(%r12), %rax
	testq	%rax, %rax
	je	.L1423
	movq	128(%rax), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	(%rbx), %rax
	movq	%r12, (%rax,%rdx,8)
.L1423:
	movq	(%rbx), %rax
	leaq	16(%rbx), %rdx
	movq	%rdx, (%rax,%rcx)
	jmp	.L1422
.L1434:
	call	__stack_chk_fail@PLT
.L1427:
	endbr64
	movq	%rax, %rdi
.L1424:
	call	__cxa_begin_catch@PLT
	leaq	8(%r12), %rdi
	call	_ZN2v88internal6torque9UnionTypeD1Ev
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.LEHB128:
	call	__cxa_rethrow@PLT
.LEHE128:
.L1428:
	endbr64
	movq	%rax, %r12
.L1425:
	call	__cxa_end_catch@PLT
	movq	%r12, %rdi
.LEHB129:
	call	_Unwind_Resume@PLT
.LEHE129:
	.cfi_endproc
.LFE11335:
	.section	.gcc_except_table._ZNSt10_HashtableIN2v88internal6torque9UnionTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb1EEEm,"aG",@progbits,_ZNSt10_HashtableIN2v88internal6torque9UnionTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb1EEEm,comdat
	.align 4
.LLSDA11335:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATT11335-.LLSDATTD11335
.LLSDATTD11335:
	.byte	0x1
	.uleb128 .LLSDACSE11335-.LLSDACSB11335
.LLSDACSB11335:
	.uleb128 .LEHB126-.LFB11335
	.uleb128 .LEHE126-.LEHB126
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB127-.LFB11335
	.uleb128 .LEHE127-.LEHB127
	.uleb128 .L1427-.LFB11335
	.uleb128 0x1
	.uleb128 .LEHB128-.LFB11335
	.uleb128 .LEHE128-.LEHB128
	.uleb128 .L1428-.LFB11335
	.uleb128 0
	.uleb128 .LEHB129-.LFB11335
	.uleb128 .LEHE129-.LEHB129
	.uleb128 0
	.uleb128 0
.LLSDACSE11335:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATT11335:
	.section	.text._ZNSt10_HashtableIN2v88internal6torque9UnionTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb1EEEm,"axG",@progbits,_ZNSt10_HashtableIN2v88internal6torque9UnionTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb1EEEm,comdat
	.size	_ZNSt10_HashtableIN2v88internal6torque9UnionTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb1EEEm, .-_ZNSt10_HashtableIN2v88internal6torque9UnionTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb1EEEm
	.section	.text._ZNSt10_HashtableIN2v88internal6torque9UnionTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_insertIS3_NS5_10_AllocNodeISaINS5_10_Hash_nodeIS3_Lb1EEEEEEEESt4pairINS5_14_Node_iteratorIS3_Lb1ELb1EEEbEOT_RKT0_St17integral_constantIbLb1EEm,"axG",@progbits,_ZNSt10_HashtableIN2v88internal6torque9UnionTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_insertIS3_NS5_10_AllocNodeISaINS5_10_Hash_nodeIS3_Lb1EEEEEEEESt4pairINS5_14_Node_iteratorIS3_Lb1ELb1EEEbEOT_RKT0_St17integral_constantIbLb1EEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIN2v88internal6torque9UnionTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_insertIS3_NS5_10_AllocNodeISaINS5_10_Hash_nodeIS3_Lb1EEEEEEEESt4pairINS5_14_Node_iteratorIS3_Lb1ELb1EEEbEOT_RKT0_St17integral_constantIbLb1EEm
	.type	_ZNSt10_HashtableIN2v88internal6torque9UnionTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_insertIS3_NS5_10_AllocNodeISaINS5_10_Hash_nodeIS3_Lb1EEEEEEEESt4pairINS5_14_Node_iteratorIS3_Lb1ELb1EEEbEOT_RKT0_St17integral_constantIbLb1EEm, @function
_ZNSt10_HashtableIN2v88internal6torque9UnionTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_insertIS3_NS5_10_AllocNodeISaINS5_10_Hash_nodeIS3_Lb1EEEEEEEESt4pairINS5_14_Node_iteratorIS3_Lb1ELb1EEEbEOT_RKT0_St17integral_constantIbLb1EEm:
.LFB10765:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	80(%rsi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	xorl	%r13d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$56, %rsp
	movq	96(%rsi), %r14
	movq	%rcx, -88(%rbp)
	movq	%rax, -56(%rbp)
	cmpq	%rax, %r14
	je	.L1436
	.p2align 4,,10
	.p2align 3
.L1437:
	movq	32(%r14), %r15
	movq	%r13, %rdi
	call	_ZN2v84base10hash_valueEm@PLT
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZN2v84base10hash_valueEm@PLT
	xorl	%edi, %edi
	movq	%rax, %rsi
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	%r14, %rdi
	movq	%rax, %r13
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r14
	cmpq	%rax, -56(%rbp)
	jne	.L1437
.L1436:
	movq	8(%r12), %rsi
	movq	%r13, %rax
	xorl	%edx, %edx
	divq	%rsi
	movq	(%r12), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r14
	testq	%rax, %rax
	je	.L1438
	movq	(%rax), %r15
	movq	128(%r15), %rdi
	jmp	.L1443
	.p2align 4,,10
	.p2align 3
.L1439:
	movq	(%r15), %r15
	testq	%r15, %r15
	je	.L1438
	movq	128(%r15), %rdi
	xorl	%edx, %edx
	movq	%rdi, %rax
	divq	%rsi
	cmpq	%rdx, %r14
	jne	.L1438
.L1443:
	cmpq	%rdi, %r13
	jne	.L1439
	movq	120(%r15), %rax
	cmpq	%rax, 112(%rbx)
	jne	.L1439
	movq	104(%r15), %r10
	movq	96(%rbx), %r8
	cmpq	%r8, -56(%rbp)
	je	.L1440
	.p2align 4,,10
	.p2align 3
.L1442:
	movq	%r10, -80(%rbp)
	movq	32(%r10), %rax
	cmpq	%rax, 32(%r8)
	jne	.L1439
	movq	%r8, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	-80(%rbp), %r10
	movq	%rax, -64(%rbp)
	movq	%r10, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	-64(%rbp), %r8
	cmpq	%r8, -56(%rbp)
	movq	-72(%rbp), %rsi
	movq	%rax, %r10
	jne	.L1442
.L1440:
	addq	$56, %rsp
	movq	%r15, %rax
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1438:
	.cfi_restore_state
	movl	$136, %edi
	call	_Znwm@PLT
	movq	$0, (%rax)
	movq	%rax, %rcx
	movl	8(%rbx), %eax
	movl	%eax, 16(%rcx)
	leaq	16+_ZTVN2v88internal6torque4TypeE(%rip), %rax
	movq	%rax, 8(%rcx)
	movq	16(%rbx), %rax
	movq	%rax, 24(%rcx)
	movq	40(%rbx), %rax
	testq	%rax, %rax
	je	.L1444
	movl	32(%rbx), %edx
	movq	%rax, 48(%rcx)
	movl	%edx, 40(%rcx)
	movq	48(%rbx), %rdx
	movq	%rdx, 56(%rcx)
	movq	56(%rbx), %rdx
	movq	%rdx, 64(%rcx)
	leaq	40(%rcx), %rdx
	movq	%rdx, 8(%rax)
	movq	64(%rbx), %rax
	movq	$0, 40(%rbx)
	movq	%rax, 72(%rcx)
	leaq	32(%rbx), %rax
	movq	%rax, 48(%rbx)
	movq	%rax, 56(%rbx)
	movq	$0, 64(%rbx)
.L1445:
	leaq	16+_ZTVN2v88internal6torque9UnionTypeE(%rip), %rax
	leaq	88(%rcx), %rdx
	movq	%rax, 8(%rcx)
	movq	88(%rbx), %rax
	testq	%rax, %rax
	je	.L1446
	movl	80(%rbx), %esi
	movq	%rax, 96(%rcx)
	movl	%esi, 88(%rcx)
	movq	96(%rbx), %rsi
	movq	%rsi, 104(%rcx)
	movq	104(%rbx), %rsi
	movq	%rsi, 112(%rcx)
	movq	%rdx, 8(%rax)
	movq	112(%rbx), %rax
	movq	$0, 88(%rbx)
	movq	%rax, 120(%rcx)
	movq	-56(%rbp), %rax
	movq	$0, 112(%rbx)
	movq	%rax, 96(%rbx)
	movq	%rax, 104(%rbx)
.L1447:
	movq	-88(%rbp), %r8
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZNSt10_HashtableIN2v88internal6torque9UnionTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb1EEEm
	addq	$56, %rsp
	movl	$1, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1446:
	.cfi_restore_state
	movl	$0, 88(%rcx)
	movq	$0, 96(%rcx)
	movq	%rdx, 104(%rcx)
	movq	%rdx, 112(%rcx)
	movq	$0, 120(%rcx)
	jmp	.L1447
	.p2align 4,,10
	.p2align 3
.L1444:
	leaq	40(%rcx), %rax
	movl	$0, 40(%rcx)
	movq	$0, 48(%rcx)
	movq	%rax, 56(%rcx)
	movq	%rax, 64(%rcx)
	movq	$0, 72(%rcx)
	jmp	.L1445
	.cfi_endproc
.LFE10765:
	.size	_ZNSt10_HashtableIN2v88internal6torque9UnionTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_insertIS3_NS5_10_AllocNodeISaINS5_10_Hash_nodeIS3_Lb1EEEEEEEESt4pairINS5_14_Node_iteratorIS3_Lb1ELb1EEEbEOT_RKT0_St17integral_constantIbLb1EEm, .-_ZNSt10_HashtableIN2v88internal6torque9UnionTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_insertIS3_NS5_10_AllocNodeISaINS5_10_Hash_nodeIS3_Lb1EEEEEEEESt4pairINS5_14_Node_iteratorIS3_Lb1ELb1EEEbEOT_RKT0_St17integral_constantIbLb1EEm
	.section	.text._ZN2v88internal6torque9UnionTypeD0Ev,"axG",@progbits,_ZN2v88internal6torque9UnionTypeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque9UnionTypeD0Ev
	.type	_ZN2v88internal6torque9UnionTypeD0Ev, @function
_ZN2v88internal6torque9UnionTypeD0Ev:
.LFB12560:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal6torque9UnionTypeE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	88(%rdi), %r13
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L1462
	leaq	72(%rdi), %rbx
.L1463:
	movq	24(%r13), %rsi
	movq	%rbx, %rdi
	call	_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	%r13, %rdi
	movq	16(%r13), %r13
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L1463
.L1462:
	movq	40(%r12), %r13
	leaq	16+_ZTVN2v88internal6torque4TypeE(%rip), %rax
	leaq	24(%r12), %r14
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L1464
.L1467:
	movq	24(%r13), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r13), %rdi
	leaq	48(%r13), %rax
	movq	16(%r13), %rbx
	cmpq	%rax, %rdi
	je	.L1465
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L1464
.L1466:
	movq	%rbx, %r13
	jmp	.L1467
	.p2align 4,,10
	.p2align 3
.L1465:
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L1466
.L1464:
	popq	%rbx
	movq	%r12, %rdi
	movl	$120, %esi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE12560:
	.size	_ZN2v88internal6torque9UnionTypeD0Ev, .-_ZN2v88internal6torque9UnionTypeD0Ev
	.section	.text._ZN2v88internal6torque18BuiltinPointerTypeD2Ev,"axG",@progbits,_ZN2v88internal6torque18BuiltinPointerTypeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque18BuiltinPointerTypeD2Ev
	.type	_ZN2v88internal6torque18BuiltinPointerTypeD2Ev, @function
_ZN2v88internal6torque18BuiltinPointerTypeD2Ev:
.LFB12562:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal6torque18BuiltinPointerTypeE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	%rax, (%rdi)
	movq	72(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L1483
	call	_ZdlPv@PLT
.L1483:
	leaq	16+_ZTVN2v88internal6torque4TypeE(%rip), %rax
	leaq	24(%r12), %r13
	movq	%rax, (%r12)
	movq	40(%r12), %r12
	testq	%r12, %r12
	je	.L1482
.L1487:
	movq	24(%r12), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r12), %rdi
	leaq	48(%r12), %rax
	movq	16(%r12), %rbx
	cmpq	%rax, %rdi
	je	.L1485
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L1482
.L1486:
	movq	%rbx, %r12
	jmp	.L1487
	.p2align 4,,10
	.p2align 3
.L1485:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L1486
.L1482:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE12562:
	.size	_ZN2v88internal6torque18BuiltinPointerTypeD2Ev, .-_ZN2v88internal6torque18BuiltinPointerTypeD2Ev
	.weak	_ZN2v88internal6torque18BuiltinPointerTypeD1Ev
	.set	_ZN2v88internal6torque18BuiltinPointerTypeD1Ev,_ZN2v88internal6torque18BuiltinPointerTypeD2Ev
	.section	.text._ZNSt10_HashtableIN2v88internal6torque18BuiltinPointerTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb1EEEm,"axG",@progbits,_ZNSt10_HashtableIN2v88internal6torque18BuiltinPointerTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb1EEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIN2v88internal6torque18BuiltinPointerTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb1EEEm
	.type	_ZNSt10_HashtableIN2v88internal6torque18BuiltinPointerTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb1EEEm, @function
_ZNSt10_HashtableIN2v88internal6torque18BuiltinPointerTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb1EEEm:
.LFB11307:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA11307
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rcx, %r12
	movq	%r8, %rcx
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	addq	$32, %rdi
	subq	$16, %rsp
	movq	-8(%rdi), %rdx
	movq	-24(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	%rax, -48(%rbp)
.LEHB130:
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
.LEHE130:
	testb	%al, %al
	je	.L1502
	movq	%rdx, %rsi
	movq	%rbx, %rdi
	leaq	-48(%rbp), %rdx
.LEHB131:
	call	_ZNSt10_HashtableIN2v88internal6torque18BuiltinPointerTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_rehashEmRKm
.LEHE131:
	movq	%r14, %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	%rdx, %r13
.L1502:
	movq	%r14, 120(%r12)
	movq	(%rbx), %rax
	leaq	0(,%r13,8), %rcx
	movq	(%rax,%r13,8), %rax
	testq	%rax, %rax
	je	.L1503
	movq	(%rax), %rax
	movq	%rax, (%r12)
	movq	(%rbx), %rax
	movq	(%rax,%r13,8), %rax
	movq	%r12, (%rax)
.L1504:
	addq	$1, 24(%rbx)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1516
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1503:
	.cfi_restore_state
	movq	16(%rbx), %rax
	movq	%rax, (%r12)
	movq	%r12, 16(%rbx)
	movq	(%r12), %rax
	testq	%rax, %rax
	je	.L1505
	movq	120(%rax), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	(%rbx), %rax
	movq	%r12, (%rax,%rdx,8)
.L1505:
	movq	(%rbx), %rax
	leaq	16(%rbx), %rdx
	movq	%rdx, (%rax,%rcx)
	jmp	.L1504
.L1516:
	call	__stack_chk_fail@PLT
.L1509:
	endbr64
	movq	%rax, %rdi
.L1506:
	call	__cxa_begin_catch@PLT
	leaq	8(%r12), %rdi
	call	_ZN2v88internal6torque18BuiltinPointerTypeD1Ev
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.LEHB132:
	call	__cxa_rethrow@PLT
.LEHE132:
.L1510:
	endbr64
	movq	%rax, %r12
.L1507:
	call	__cxa_end_catch@PLT
	movq	%r12, %rdi
.LEHB133:
	call	_Unwind_Resume@PLT
.LEHE133:
	.cfi_endproc
.LFE11307:
	.section	.gcc_except_table._ZNSt10_HashtableIN2v88internal6torque18BuiltinPointerTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb1EEEm,"aG",@progbits,_ZNSt10_HashtableIN2v88internal6torque18BuiltinPointerTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb1EEEm,comdat
	.align 4
.LLSDA11307:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATT11307-.LLSDATTD11307
.LLSDATTD11307:
	.byte	0x1
	.uleb128 .LLSDACSE11307-.LLSDACSB11307
.LLSDACSB11307:
	.uleb128 .LEHB130-.LFB11307
	.uleb128 .LEHE130-.LEHB130
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB131-.LFB11307
	.uleb128 .LEHE131-.LEHB131
	.uleb128 .L1509-.LFB11307
	.uleb128 0x1
	.uleb128 .LEHB132-.LFB11307
	.uleb128 .LEHE132-.LEHB132
	.uleb128 .L1510-.LFB11307
	.uleb128 0
	.uleb128 .LEHB133-.LFB11307
	.uleb128 .LEHE133-.LEHB133
	.uleb128 0
	.uleb128 0
.LLSDACSE11307:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATT11307:
	.section	.text._ZNSt10_HashtableIN2v88internal6torque18BuiltinPointerTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb1EEEm,"axG",@progbits,_ZNSt10_HashtableIN2v88internal6torque18BuiltinPointerTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb1EEEm,comdat
	.size	_ZNSt10_HashtableIN2v88internal6torque18BuiltinPointerTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb1EEEm, .-_ZNSt10_HashtableIN2v88internal6torque18BuiltinPointerTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb1EEEm
	.section	.text._ZNSt10_HashtableIN2v88internal6torque18BuiltinPointerTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_insertIS3_NS5_10_AllocNodeISaINS5_10_Hash_nodeIS3_Lb1EEEEEEEESt4pairINS5_14_Node_iteratorIS3_Lb1ELb1EEEbEOT_RKT0_St17integral_constantIbLb1EEm,"axG",@progbits,_ZNSt10_HashtableIN2v88internal6torque18BuiltinPointerTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_insertIS3_NS5_10_AllocNodeISaINS5_10_Hash_nodeIS3_Lb1EEEEEEEESt4pairINS5_14_Node_iteratorIS3_Lb1ELb1EEEbEOT_RKT0_St17integral_constantIbLb1EEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIN2v88internal6torque18BuiltinPointerTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_insertIS3_NS5_10_AllocNodeISaINS5_10_Hash_nodeIS3_Lb1EEEEEEEESt4pairINS5_14_Node_iteratorIS3_Lb1ELb1EEEbEOT_RKT0_St17integral_constantIbLb1EEm
	.type	_ZNSt10_HashtableIN2v88internal6torque18BuiltinPointerTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_insertIS3_NS5_10_AllocNodeISaINS5_10_Hash_nodeIS3_Lb1EEEEEEEESt4pairINS5_14_Node_iteratorIS3_Lb1ELb1EEEbEOT_RKT0_St17integral_constantIbLb1EEm, @function
_ZNSt10_HashtableIN2v88internal6torque18BuiltinPointerTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_insertIS3_NS5_10_AllocNodeISaINS5_10_Hash_nodeIS3_Lb1EEEEEEEESt4pairINS5_14_Node_iteratorIS3_Lb1ELb1EEEbEOT_RKT0_St17integral_constantIbLb1EEm:
.LFB10741:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA10741
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	96(%rsi), %rdi
	movq	%rcx, -64(%rbp)
.LEHB134:
	call	_ZN2v84base10hash_valueEm@PLT
	movq	72(%rbx), %r12
	movq	80(%rbx), %r15
	movq	%rax, %r13
	cmpq	%r15, %r12
	je	.L1518
	.p2align 4,,10
	.p2align 3
.L1519:
	movq	(%r12), %r8
	movq	%r13, %rdi
	addq	$8, %r12
	movq	%r8, -56(%rbp)
	call	_ZN2v84base10hash_valueEm@PLT
	movq	-56(%rbp), %r8
	movq	%rax, %r13
	movq	%r8, %rdi
	call	_ZN2v84base10hash_valueEm@PLT
	xorl	%edi, %edi
	movq	%rax, %rsi
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	%rax, %r13
	cmpq	%r12, %r15
	jne	.L1519
.L1518:
	movq	8(%r14), %rcx
	movq	%r13, %rax
	xorl	%edx, %edx
	divq	%rcx
	movq	(%r14), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r15
	testq	%rax, %rax
	je	.L1520
	movq	(%rax), %r12
	movq	120(%r12), %rsi
	jmp	.L1524
	.p2align 4,,10
	.p2align 3
.L1521:
	movq	(%r12), %r12
	testq	%r12, %r12
	je	.L1520
	movq	120(%r12), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rcx
	cmpq	%rdx, %r15
	jne	.L1520
.L1524:
	cmpq	%rsi, %r13
	jne	.L1521
	movq	72(%rbx), %rdi
	movq	80(%rbx), %rdx
	movq	80(%r12), %rsi
	movq	88(%r12), %rax
	subq	%rdi, %rdx
	subq	%rsi, %rax
	cmpq	%rax, %rdx
	jne	.L1521
	testq	%rdx, %rdx
	jne	.L1549
.L1522:
	movq	104(%r12), %rax
	cmpq	%rax, 96(%rbx)
	jne	.L1521
	addq	$24, %rsp
	movq	%r12, %rax
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1520:
	.cfi_restore_state
	movl	$128, %edi
	call	_Znwm@PLT
.LEHE134:
	movq	$0, (%rax)
	movq	%rax, %r12
	movl	8(%rbx), %eax
	movl	%eax, 16(%r12)
	leaq	16+_ZTVN2v88internal6torque4TypeE(%rip), %rax
	movq	%rax, 8(%r12)
	movq	16(%rbx), %rax
	movq	%rax, 24(%r12)
	movq	40(%rbx), %rax
	testq	%rax, %rax
	je	.L1525
	movl	32(%rbx), %edx
	movq	%rax, 48(%r12)
	movl	%edx, 40(%r12)
	movq	48(%rbx), %rdx
	movq	%rdx, 56(%r12)
	movq	56(%rbx), %rdx
	movq	%rdx, 64(%r12)
	leaq	40(%r12), %rdx
	movq	%rdx, 8(%rax)
	movq	64(%rbx), %rax
	movq	$0, 40(%rbx)
	movq	%rax, 72(%r12)
	leaq	32(%rbx), %rax
	movq	%rax, 48(%rbx)
	movq	%rax, 56(%rbx)
	movq	$0, 64(%rbx)
.L1526:
	movq	80(%rbx), %rdx
	leaq	16+_ZTVN2v88internal6torque18BuiltinPointerTypeE(%rip), %rax
	movq	72(%rbx), %rsi
	pxor	%xmm0, %xmm0
	movq	%rax, 8(%r12)
	movq	%rdx, %rax
	movups	%xmm0, 80(%r12)
	movq	$0, 96(%r12)
	subq	%rsi, %rax
	movq	%rax, -56(%rbp)
	sarq	$3, %rax
	je	.L1550
	movabsq	$1152921504606846975, %rdx
	cmpq	%rdx, %rax
	ja	.L1551
	movq	-56(%rbp), %rdi
.LEHB135:
	call	_Znwm@PLT
.LEHE135:
	movq	80(%rbx), %rdx
	movq	72(%rbx), %rsi
	movq	%rax, %rcx
	movq	%rdx, %r8
	subq	%rsi, %r8
.L1528:
	movq	-56(%rbp), %rax
	movq	%rcx, %xmm0
	punpcklqdq	%xmm0, %xmm0
	addq	%rcx, %rax
	movups	%xmm0, 80(%r12)
	movq	%rax, 96(%r12)
	cmpq	%rsi, %rdx
	je	.L1530
	movq	%r8, %rdx
	movq	%rcx, %rdi
	movq	%r8, -56(%rbp)
	call	memmove@PLT
	movq	-56(%rbp), %r8
	movq	%rax, %rcx
.L1530:
	movq	96(%rbx), %rax
	addq	%r8, %rcx
	movq	-64(%rbp), %r8
	movq	%r13, %rdx
	movq	%rcx, 88(%r12)
	movq	%r15, %rsi
	movq	%r12, %rcx
	movq	%r14, %rdi
	movq	%rax, 104(%r12)
	movq	104(%rbx), %rax
	movq	%rax, 112(%r12)
.LEHB136:
	call	_ZNSt10_HashtableIN2v88internal6torque18BuiltinPointerTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb1EEEm
.LEHE136:
	addq	$24, %rsp
	movl	$1, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1549:
	.cfi_restore_state
	movq	%rcx, -56(%rbp)
	call	memcmp@PLT
	movq	-56(%rbp), %rcx
	testl	%eax, %eax
	jne	.L1521
	jmp	.L1522
	.p2align 4,,10
	.p2align 3
.L1525:
	movl	$0, 40(%r12)
	leaq	40(%r12), %rax
	movq	$0, 48(%r12)
	movq	%rax, 56(%r12)
	movq	%rax, 64(%r12)
	movq	$0, 72(%r12)
	jmp	.L1526
	.p2align 4,,10
	.p2align 3
.L1550:
	movq	-56(%rbp), %r8
	xorl	%ecx, %ecx
	jmp	.L1528
.L1551:
.LEHB137:
	call	_ZSt17__throw_bad_allocv@PLT
.LEHE137:
.L1535:
	endbr64
	movq	%rax, %r13
.L1532:
	leaq	16+_ZTVN2v88internal6torque4TypeE(%rip), %rax
	movq	48(%r12), %rsi
	leaq	32(%r12), %rdi
	movq	%rax, 8(%r12)
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	%r13, %rdi
	call	__cxa_begin_catch@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.LEHB138:
	call	__cxa_rethrow@PLT
.LEHE138:
.L1534:
	endbr64
	movq	%rax, %r12
.L1533:
	call	__cxa_end_catch@PLT
	movq	%r12, %rdi
.LEHB139:
	call	_Unwind_Resume@PLT
.LEHE139:
	.cfi_endproc
.LFE10741:
	.section	.gcc_except_table._ZNSt10_HashtableIN2v88internal6torque18BuiltinPointerTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_insertIS3_NS5_10_AllocNodeISaINS5_10_Hash_nodeIS3_Lb1EEEEEEEESt4pairINS5_14_Node_iteratorIS3_Lb1ELb1EEEbEOT_RKT0_St17integral_constantIbLb1EEm,"aG",@progbits,_ZNSt10_HashtableIN2v88internal6torque18BuiltinPointerTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_insertIS3_NS5_10_AllocNodeISaINS5_10_Hash_nodeIS3_Lb1EEEEEEEESt4pairINS5_14_Node_iteratorIS3_Lb1ELb1EEEbEOT_RKT0_St17integral_constantIbLb1EEm,comdat
	.align 4
.LLSDA10741:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATT10741-.LLSDATTD10741
.LLSDATTD10741:
	.byte	0x1
	.uleb128 .LLSDACSE10741-.LLSDACSB10741
.LLSDACSB10741:
	.uleb128 .LEHB134-.LFB10741
	.uleb128 .LEHE134-.LEHB134
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB135-.LFB10741
	.uleb128 .LEHE135-.LEHB135
	.uleb128 .L1535-.LFB10741
	.uleb128 0x3
	.uleb128 .LEHB136-.LFB10741
	.uleb128 .LEHE136-.LEHB136
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB137-.LFB10741
	.uleb128 .LEHE137-.LEHB137
	.uleb128 .L1535-.LFB10741
	.uleb128 0x3
	.uleb128 .LEHB138-.LFB10741
	.uleb128 .LEHE138-.LEHB138
	.uleb128 .L1534-.LFB10741
	.uleb128 0
	.uleb128 .LEHB139-.LFB10741
	.uleb128 .LEHE139-.LEHB139
	.uleb128 0
	.uleb128 0
.LLSDACSE10741:
	.byte	0x1
	.byte	0
	.byte	0
	.byte	0x7d
	.align 4
	.long	0

.LLSDATT10741:
	.section	.text._ZNSt10_HashtableIN2v88internal6torque18BuiltinPointerTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_insertIS3_NS5_10_AllocNodeISaINS5_10_Hash_nodeIS3_Lb1EEEEEEEESt4pairINS5_14_Node_iteratorIS3_Lb1ELb1EEEbEOT_RKT0_St17integral_constantIbLb1EEm,"axG",@progbits,_ZNSt10_HashtableIN2v88internal6torque18BuiltinPointerTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_insertIS3_NS5_10_AllocNodeISaINS5_10_Hash_nodeIS3_Lb1EEEEEEEESt4pairINS5_14_Node_iteratorIS3_Lb1ELb1EEEbEOT_RKT0_St17integral_constantIbLb1EEm,comdat
	.size	_ZNSt10_HashtableIN2v88internal6torque18BuiltinPointerTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_insertIS3_NS5_10_AllocNodeISaINS5_10_Hash_nodeIS3_Lb1EEEEEEEESt4pairINS5_14_Node_iteratorIS3_Lb1ELb1EEEbEOT_RKT0_St17integral_constantIbLb1EEm, .-_ZNSt10_HashtableIN2v88internal6torque18BuiltinPointerTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_insertIS3_NS5_10_AllocNodeISaINS5_10_Hash_nodeIS3_Lb1EEEEEEEESt4pairINS5_14_Node_iteratorIS3_Lb1ELb1EEEbEOT_RKT0_St17integral_constantIbLb1EEm
	.section	.text.unlikely._ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_14TypeExpressionE,"ax",@progbits
	.align 2
.LCOLDB23:
	.section	.text._ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_14TypeExpressionE,"ax",@progbits
.LHOTB23:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_14TypeExpressionE
	.type	_ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_14TypeExpressionE, @function
_ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_14TypeExpressionE:
.LFB6556:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6556
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$536, %rsp
	.cfi_offset 3, -56
	movq	40(%rdi), %rcx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	8(%rdi), %eax
	movq	%rcx, -552(%rbp)
	cmpl	$20, %eax
	jne	.L1553
	movq	32(%rdi), %r14
	pxor	%xmm0, %xmm0
	movq	$0, -256(%rbp)
	movaps	%xmm0, -272(%rbp)
	subq	%r14, %rcx
	movq	%rcx, %rax
	movq	%rcx, %rbx
	sarq	$5, %rax
	je	.L1846
	movabsq	$288230376151711743, %rdx
	cmpq	%rdx, %rax
	ja	.L1847
	movq	%rcx, %rdi
.LEHB140:
	call	_Znwm@PLT
.LEHE140:
	movq	32(%r12), %r14
	movq	%rax, -568(%rbp)
	movq	40(%r12), %rax
	movq	%rax, -552(%rbp)
.L1555:
	movq	-568(%rbp), %rax
	movq	%rax, %xmm0
	addq	%rax, %rbx
	punpcklqdq	%xmm0, %xmm0
	movq	%rbx, -256(%rbp)
	movq	%rax, %rbx
	movaps	%xmm0, -272(%rbp)
	cmpq	-552(%rbp), %r14
	je	.L1557
	leaq	-400(%rbp), %rax
	movq	%rax, -560(%rbp)
	jmp	.L1563
	.p2align 4,,10
	.p2align 3
.L1559:
	cmpq	$1, %r13
	jne	.L1561
	movzbl	(%r15), %eax
	movb	%al, 16(%rbx)
.L1562:
	movq	%r13, 8(%rbx)
	addq	$32, %r14
	addq	$32, %rbx
	movb	$0, (%rdi,%r13)
	cmpq	%r14, -552(%rbp)
	je	.L1557
.L1563:
	leaq	16(%rbx), %rdi
	movq	%rdi, (%rbx)
	movq	(%r14), %r15
	movq	8(%r14), %r13
	movq	%r15, %rax
	addq	%r13, %rax
	je	.L1558
	testq	%r15, %r15
	je	.L1848
.L1558:
	movq	%r13, -400(%rbp)
	cmpq	$15, %r13
	jbe	.L1559
	movq	-560(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rbx, %rdi
.LEHB141:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE141:
	movq	%rax, (%rbx)
	movq	%rax, %rdi
	movq	-400(%rbp), %rax
	movq	%rax, 16(%rbx)
.L1560:
	movq	%r13, %rdx
	movq	%r15, %rsi
	call	memcpy@PLT
	movq	-400(%rbp), %r13
	movq	(%rbx), %rdi
	jmp	.L1562
	.p2align 4,,10
	.p2align 3
.L1561:
	testq	%r13, %r13
	je	.L1562
	jmp	.L1560
	.p2align 4,,10
	.p2align 3
.L1557:
	movq	64(%r12), %r14
	movq	72(%r12), %r13
	movq	%rbx, -264(%rbp)
	leaq	-128(%rbp), %rbx
	movq	%rbx, -144(%rbp)
	movq	%r14, %rax
	addq	%r13, %rax
	je	.L1566
	testq	%r14, %r14
	je	.L1849
.L1566:
	movq	%r13, -400(%rbp)
	cmpq	$15, %r13
	ja	.L1850
	cmpq	$1, %r13
	jne	.L1575
	movzbl	(%r14), %eax
	movb	%al, -128(%rbp)
	movq	%rbx, %rax
.L1576:
	movq	%r13, -136(%rbp)
	pxor	%xmm0, %xmm0
	leaq	-72(%rbp), %r14
	movb	$0, (%rax,%r13)
	movq	-256(%rbp), %rax
	movdqa	-272(%rbp), %xmm1
	movq	%r14, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	-144(%rbp), %rax
	movq	$0, -256(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm0, -272(%rbp)
	cmpq	%rbx, %rax
	je	.L1851
	movq	%rax, -88(%rbp)
	movq	-128(%rbp), %rax
	movq	%rax, -72(%rbp)
.L1578:
	movq	-136(%rbp), %rax
	pcmpeqd	%xmm0, %xmm0
	leaq	-112(%rbp), %r15
	movl	$-1, -384(%rbp)
	movaps	%xmm0, -400(%rbp)
	movq	%r15, %rdi
	movq	%rax, -80(%rbp)
	movq	96(%r12), %rax
	cmpq	%rax, 104(%r12)
	je	.L1852
.LEHB142:
	call	_ZN2v88internal6torque12Declarations29LookupUniqueGenericStructTypeERKNS1_13QualifiedNameE@PLT
.LEHE142:
	movq	%rax, %r13
	movq	96(%r12), %rbx
	movq	104(%r12), %rax
	pxor	%xmm0, %xmm0
	movq	$0, -256(%rbp)
	movq	%rax, -552(%rbp)
	movaps	%xmm0, -272(%rbp)
	cmpq	%rax, %rbx
	je	.L1583
	leaq	-528(%rbp), %rax
	movq	%rax, -560(%rbp)
	jmp	.L1586
	.p2align 4,,10
	.p2align 3
.L1853:
	movq	%rax, (%rsi)
	addq	$8, %rbx
	addq	$8, -264(%rbp)
	cmpq	%rbx, -552(%rbp)
	je	.L1583
.L1586:
	movq	(%rbx), %rdi
.LEHB143:
	call	_ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_14TypeExpressionE
	movq	%rax, -528(%rbp)
	movq	-264(%rbp), %rsi
	cmpq	-256(%rbp), %rsi
	jne	.L1853
	movq	-560(%rbp), %rdx
	leaq	-272(%rbp), %rdi
	call	_ZNSt6vectorIPKN2v88internal6torque4TypeESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
.LEHE143:
	addq	$8, %rbx
	cmpq	%rbx, -552(%rbp)
	jne	.L1586
	.p2align 4,,10
	.p2align 3
.L1583:
	leaq	-272(%rbp), %rsi
	movq	%r13, %rdi
.LEHB144:
	call	_ZN2v88internal6torque10TypeOracle28GetGenericStructTypeInstanceEPNS1_17GenericStructTypeESt6vectorIPKNS1_4TypeESaIS8_EE@PLT
.LEHE144:
	movq	-272(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1590
	call	_ZdlPv@PLT
.L1590:
	movq	104(%r13), %rax
	movq	32(%rax), %rax
	movdqu	12(%rax), %xmm2
	movaps	%xmm2, -400(%rbp)
	movl	28(%rax), %eax
	movl	%eax, -384(%rbp)
.L1582:
.LEHB145:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_13GlobalContextEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rax
	cmpb	$0, (%rax)
	je	.L1591
	movl	-384(%rbp), %eax
	movdqa	-400(%rbp), %xmm3
	subq	$48, %rsp
	movl	%eax, 40(%rsp)
	movups	%xmm3, 24(%rsp)
	movdqu	12(%r12), %xmm4
	movups	%xmm4, (%rsp)
	movl	28(%r12), %eax
	movl	%eax, 16(%rsp)
	.cfi_escape 0x2e,0x30
	call	_ZN2v88internal6torque18LanguageServerData13AddDefinitionENS1_14SourcePositionES3_@PLT
.LEHE145:
	addq	$48, %rsp
.L1591:
	movq	-88(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L1592
	call	_ZdlPv@PLT
.L1592:
	movq	-104(%rbp), %r13
	movq	-112(%rbp), %r12
	cmpq	%r12, %r13
	je	.L1593
	.p2align 4,,10
	.p2align 3
.L1597:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1594
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r12, %r13
	jne	.L1597
.L1595:
	movq	-112(%rbp), %r12
.L1593:
	testq	%r12, %r12
	je	.L1552
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1552:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1854
	leaq	-40(%rbp), %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1594:
	.cfi_restore_state
	addq	$32, %r12
	cmpq	%r12, %r13
	jne	.L1597
	jmp	.L1595
	.p2align 4,,10
	.p2align 3
.L1553:
	cmpl	$22, %eax
	jne	.L1600
	movq	%rcx, %rdi
.LEHB146:
	call	_ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_14TypeExpressionE
	movq	32(%r12), %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_14TypeExpressionE
	movq	%rbx, %rsi
	movq	%rax, %r15
	movq	(%rax), %rax
	movq	%r15, %rdi
	call	*16(%rax)
	testb	%al, %al
	jne	.L1552
	movq	(%rbx), %rax
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	*16(%rax)
	testb	%al, %al
	jne	.L1696
	cmpl	$3, 8(%r15)
	movl	$3, -520(%rbp)
	jne	.L1602
	leaq	16+_ZTVN2v88internal6torque4TypeE(%rip), %rax
	leaq	-496(%rbp), %rdx
	movq	%rax, -528(%rbp)
	movq	%rax, -552(%rbp)
	movq	16(%r15), %rax
	movl	$0, -496(%rbp)
	movq	%rax, -512(%rbp)
	movq	$0, -488(%rbp)
	movq	%rdx, -480(%rbp)
	movq	%rdx, -472(%rbp)
	movq	$0, -464(%rbp)
	movq	40(%r15), %rsi
	movq	%rdx, -560(%rbp)
	testq	%rsi, %rsi
	je	.L1603
	leaq	-504(%rbp), %r12
	leaq	-272(%rbp), %rcx
	movq	%r12, %rdi
	movq	%r12, -272(%rbp)
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE7_M_copyINSB_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS5_EPKSF_PSt18_Rb_tree_node_baseRT_
.LEHE146:
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L1604:
	movq	%rdx, %rcx
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L1604
	movq	%rcx, -480(%rbp)
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L1605:
	movq	%rdx, %rcx
	movq	24(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L1605
	movq	%rcx, -472(%rbp)
	movq	64(%r15), %rdx
	movq	%rax, -488(%rbp)
	movq	%rdx, -464(%rbp)
.L1603:
	leaq	-448(%rbp), %r13
	leaq	16+_ZTVN2v88internal6torque9UnionTypeE(%rip), %r12
	movl	$0, -448(%rbp)
	movq	%r12, -528(%rbp)
	movq	$0, -440(%rbp)
	movq	%r13, -432(%rbp)
	movq	%r13, -424(%rbp)
	movq	$0, -416(%rbp)
	movq	88(%r15), %rsi
	testq	%rsi, %rsi
	je	.L1606
	leaq	-456(%rbp), %r14
	leaq	-272(%rbp), %rcx
	movq	%r13, %rdx
	movq	%r14, %rdi
	movq	%r14, -272(%rbp)
.LEHB147:
	.cfi_escape 0x2e,0
	call	_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE7_M_copyINSA_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS5_EPKSE_PSt18_Rb_tree_node_baseRT_
.LEHE147:
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L1607:
	movq	%rdx, %rcx
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L1607
	movq	%rcx, -432(%rbp)
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L1608:
	movq	%rdx, %rcx
	movq	24(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L1608
	movq	%rcx, -424(%rbp)
	movq	112(%r15), %rdx
	movq	%rax, -440(%rbp)
	movq	%rdx, -416(%rbp)
	.p2align 4,,10
	.p2align 3
.L1606:
	movq	%rbx, -272(%rbp)
	cmpl	$3, 8(%rbx)
	jne	.L1855
	movq	96(%rbx), %r14
	addq	$80, %rbx
	leaq	-528(%rbp), %r15
	cmpq	%rbx, %r14
	je	.L1843
.L1613:
	movq	32(%r14), %rsi
	movq	%r15, %rdi
.LEHB148:
	call	_ZN2v88internal6torque9UnionType6ExtendEPKNS1_4TypeE
.LEHE148:
	movq	%r14, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r14
	cmpq	%rax, %rbx
	jne	.L1613
.L1843:
	movq	-512(%rbp), %rdi
.L1614:
	movl	-520(%rbp), %eax
	movq	%rdi, -384(%rbp)
	movl	%eax, -392(%rbp)
	leaq	16+_ZTVN2v88internal6torque4TypeE(%rip), %rax
	movq	%rax, -552(%rbp)
	movq	-488(%rbp), %rax
	testq	%rax, %rax
	je	.L1618
	movl	-496(%rbp), %edx
	leaq	-368(%rbp), %rbx
	movq	%rax, -360(%rbp)
	movl	%edx, -368(%rbp)
	movq	-480(%rbp), %rdx
	movq	%rdx, -352(%rbp)
	movq	-472(%rbp), %rdx
	movq	%rdx, -344(%rbp)
	movq	%rbx, 8(%rax)
	movq	-464(%rbp), %rax
	movq	$0, -488(%rbp)
	movq	%rax, -336(%rbp)
	movq	-560(%rbp), %rax
	movq	$0, -464(%rbp)
	movq	%rax, -480(%rbp)
	movq	%rax, -472(%rbp)
.L1619:
	movq	-440(%rbp), %rax
	movq	%r12, -400(%rbp)
	testq	%rax, %rax
	je	.L1620
	movl	-448(%rbp), %edx
	leaq	-320(%rbp), %r14
	movq	%rax, -312(%rbp)
	movl	%edx, -320(%rbp)
	movq	-432(%rbp), %rdx
	movq	%rdx, -304(%rbp)
	movq	-424(%rbp), %rdx
	movq	%rdx, -296(%rbp)
	movq	%r14, 8(%rax)
	movq	-416(%rbp), %rax
	movq	$0, -440(%rbp)
	movq	%rax, -288(%rbp)
	movq	%r13, -432(%rbp)
	movq	%r13, -424(%rbp)
	movq	$0, -416(%rbp)
	cmpq	$1, %rax
	jne	.L1622
	movq	-304(%rbp), %rax
	movq	32(%rax), %rbx
.L1623:
	movq	-312(%rbp), %r13
	movq	%r12, -400(%rbp)
	leaq	-328(%rbp), %r14
	testq	%r13, %r13
	je	.L1639
.L1634:
	movq	24(%r13), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	%r13, %rdi
	movq	16(%r13), %r13
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L1634
.L1639:
	movq	-552(%rbp), %rax
	movq	-360(%rbp), %r13
	leaq	-376(%rbp), %r14
	movq	%rax, -400(%rbp)
	testq	%r13, %r13
	je	.L1635
.L1636:
	movq	24(%r13), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r13), %rdi
	leaq	48(%r13), %rax
	movq	16(%r13), %r15
	cmpq	%rax, %rdi
	je	.L1642
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%r15, %r15
	je	.L1635
.L1643:
	movq	%r15, %r13
	jmp	.L1636
	.p2align 4,,10
	.p2align 3
.L1575:
	testq	%r13, %r13
	jne	.L1856
	movq	%rbx, %rax
	jmp	.L1576
	.p2align 4,,10
	.p2align 3
.L1850:
	leaq	-400(%rbp), %rsi
	leaq	-144(%rbp), %rdi
	xorl	%edx, %edx
.LEHB149:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE149:
	movq	%rax, -144(%rbp)
	movq	%rax, %rdi
	movq	-400(%rbp), %rax
	movq	%rax, -128(%rbp)
.L1574:
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-400(%rbp), %r13
	movq	-144(%rbp), %rax
	jmp	.L1576
	.p2align 4,,10
	.p2align 3
.L1846:
	movq	$0, -568(%rbp)
	jmp	.L1555
	.p2align 4,,10
	.p2align 3
.L1852:
.LEHB150:
	call	_ZN2v88internal6torque12Declarations15LookupTypeAliasERKNS1_13QualifiedNameE@PLT
.LEHE150:
	cmpb	$0, 88(%rax)
	movq	%rax, %r13
	je	.L1580
	movq	96(%rax), %rbx
.L1581:
	movl	124(%r13), %eax
	movdqu	108(%r13), %xmm6
	movl	%eax, -384(%rbp)
	movaps	%xmm6, -400(%rbp)
	jmp	.L1582
	.p2align 4,,10
	.p2align 3
.L1851:
	movdqa	-128(%rbp), %xmm5
	movups	%xmm5, -72(%rbp)
	jmp	.L1578
	.p2align 4,,10
	.p2align 3
.L1600:
	pxor	%xmm0, %xmm0
	movq	32(%rdi), %rbx
	movq	$0, -384(%rbp)
	leaq	-272(%rbp), %r13
	movaps	%xmm0, -400(%rbp)
	cmpq	-552(%rbp), %rbx
	je	.L1654
.L1653:
	movq	(%rbx), %rdi
.LEHB151:
	call	_ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_14TypeExpressionE
	movq	%rax, -272(%rbp)
	movq	-392(%rbp), %rsi
	cmpq	-384(%rbp), %rsi
	je	.L1651
	movq	%rax, (%rsi)
	addq	$8, -392(%rbp)
.L1652:
	addq	$8, %rbx
	cmpq	%rbx, -552(%rbp)
	jne	.L1653
.L1654:
	movq	56(%r12), %rdi
	call	_ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_14TypeExpressionE
	movq	%rax, -568(%rbp)
	movq	-392(%rbp), %rax
	movq	-400(%rbp), %rsi
	movq	%rax, %r13
	subq	%rsi, %r13
	movq	%r13, %rdx
	sarq	$3, %rdx
	je	.L1857
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L1858
	movq	%r13, %rdi
	call	_Znwm@PLT
.LEHE151:
	movq	%rax, -552(%rbp)
	movq	-392(%rbp), %rax
	movq	-400(%rbp), %rsi
	movq	%rax, %r13
	subq	%rsi, %r13
.L1655:
	cmpq	%rax, %rsi
	je	.L1657
	movq	-552(%rbp), %rdi
	movq	%r13, %rdx
	call	memmove@PLT
.L1657:
.LEHB152:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_10TypeOracleEEERPNT_5ScopeEv@PLT
.LEHE152:
	movq	(%rax), %r12
	leaq	-96(%rbp), %rbx
	leaq	-112(%rbp), %rdi
	movb	$0, -86(%rbp)
	movabsq	$5795685719228380482, %rax
	movq	%rbx, -112(%rbp)
	movq	%rax, -96(%rbp)
	movl	$29300, %eax
	movw	%ax, -88(%rbp)
	movq	$10, -104(%rbp)
.LEHB153:
	call	_ZN2v88internal6torque12Declarations16LookupGlobalTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
.LEHE153:
	movq	-112(%rbp), %rdi
	movq	%rax, %r14
	cmpq	%rbx, %rdi
	je	.L1658
	call	_ZdlPv@PLT
.L1658:
	movq	64(%r12), %rax
	movq	%r13, %r15
	subq	56(%r12), %rax
	sarq	$3, %rax
	sarq	$3, %r15
	movq	%rax, %rbx
	je	.L1698
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %r15
	ja	.L1859
	movq	%r13, %rdi
.LEHB154:
	call	_Znwm@PLT
.LEHE154:
	movq	%rax, -560(%rbp)
.L1659:
	testq	%r13, %r13
	je	.L1661
	movq	-552(%rbp), %rsi
	movq	-560(%rbp), %rdi
	movq	%r13, %rdx
	call	memcpy@PLT
.L1661:
	leaq	-240(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	%r14, -256(%rbp)
	leaq	16+_ZTVN2v88internal6torque18BuiltinPointerTypeE(%rip), %r14
	movl	$2, -264(%rbp)
	movl	$0, -240(%rbp)
	movq	$0, -232(%rbp)
	movq	%rax, -224(%rbp)
	movq	%rax, -216(%rbp)
	movq	$0, -208(%rbp)
	movq	%r14, -272(%rbp)
	movq	$0, -184(%rbp)
	movups	%xmm0, -200(%rbp)
	testq	%r15, %r15
	je	.L1699
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %r15
	ja	.L1860
	movq	%r13, %rdi
.LEHB155:
	call	_Znwm@PLT
.LEHE155:
	movq	%rax, %rdi
.L1662:
	leaq	(%rdi,%r13), %r15
	movq	%rdi, -200(%rbp)
	movq	%r15, -184(%rbp)
	testq	%r13, %r13
	je	.L1664
	movq	-560(%rbp), %rsi
	movq	%r13, %rdx
	call	memcpy@PLT
.L1664:
	movq	%r15, -192(%rbp)
	movq	-568(%rbp), %rax
	movl	$1, %ecx
	movq	%r12, %rdi
	leaq	-528(%rbp), %r15
	leaq	-272(%rbp), %r13
	movq	%rbx, -168(%rbp)
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%rax, -176(%rbp)
	movq	%r12, -528(%rbp)
.LEHB156:
	call	_ZNSt10_HashtableIN2v88internal6torque18BuiltinPointerTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_insertIS3_NS5_10_AllocNodeISaINS5_10_Hash_nodeIS3_Lb1EEEEEEEESt4pairINS5_14_Node_iteratorIS3_Lb1ELb1EEEbEOT_RKT0_St17integral_constantIbLb1EEm
.LEHE156:
	movq	-200(%rbp), %rdi
	addq	$8, %rax
	movq	%r14, -272(%rbp)
	movq	%rax, -528(%rbp)
	testq	%rdi, %rdi
	je	.L1668
	call	_ZdlPv@PLT
.L1668:
	movq	-232(%rbp), %r14
	leaq	16+_ZTVN2v88internal6torque4TypeE(%rip), %rax
	leaq	-248(%rbp), %r13
	movq	%rax, -272(%rbp)
	testq	%r14, %r14
	je	.L1673
.L1669:
	movq	24(%r14), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r14), %rdi
	leaq	48(%r14), %rdx
	movq	16(%r14), %rbx
	cmpq	%rdx, %rdi
	je	.L1672
	call	_ZdlPv@PLT
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L1673
.L1674:
	movq	%rbx, %r14
	jmp	.L1669
	.p2align 4,,10
	.p2align 3
.L1672:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L1674
.L1673:
	cmpq	$0, -560(%rbp)
	je	.L1671
	movq	-560(%rbp), %rdi
	call	_ZdlPv@PLT
.L1671:
	movq	64(%r12), %rsi
	movq	-528(%rbp), %rbx
	movq	%rsi, %rax
	subq	56(%r12), %rax
	sarq	$3, %rax
	cmpq	%rax, 104(%rbx)
	je	.L1861
.L1675:
	cmpq	$0, -552(%rbp)
	je	.L1678
	movq	-552(%rbp), %rdi
	call	_ZdlPv@PLT
.L1678:
	movq	-400(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1552
	call	_ZdlPv@PLT
	jmp	.L1552
	.p2align 4,,10
	.p2align 3
.L1696:
	movq	%r15, %rbx
	jmp	.L1552
	.p2align 4,,10
	.p2align 3
.L1651:
	leaq	-400(%rbp), %rdi
	movq	%r13, %rdx
.LEHB157:
	call	_ZNSt6vectorIPKN2v88internal6torque4TypeESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
.LEHE157:
	jmp	.L1652
	.p2align 4,,10
	.p2align 3
.L1580:
	movq	%rax, %rdi
.LEHB158:
	call	_ZNK2v88internal6torque9TypeAlias7ResolveEv@PLT
.LEHE158:
	movq	%rax, %rbx
	jmp	.L1581
	.p2align 4,,10
	.p2align 3
.L1602:
	leaq	-448(%rbp), %r13
	leaq	-112(%rbp), %rdx
	movq	%r15, -112(%rbp)
	leaq	-456(%rbp), %r14
	leaq	-496(%rbp), %rax
	movq	%r13, %rsi
	movq	%r15, -512(%rbp)
	leaq	16+_ZTVN2v88internal6torque9UnionTypeE(%rip), %r12
	movq	%r14, %rdi
	movl	$0, -496(%rbp)
	movq	$0, -488(%rbp)
	movq	%rax, -560(%rbp)
	movq	%rax, -480(%rbp)
	movq	%rax, -472(%rbp)
	movq	$0, -464(%rbp)
	movq	%r12, -528(%rbp)
	movl	$0, -448(%rbp)
	movq	$0, -440(%rbp)
	movq	%r13, -432(%rbp)
	movq	%r13, -424(%rbp)
	movq	$0, -416(%rbp)
.LEHB159:
	call	_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS5_ERKS5_
	movq	%rdx, %r15
	testq	%rdx, %rdx
	je	.L1606
	testq	%rax, %rax
	setne	%dil
	cmpq	%r13, %rdx
	sete	%al
	orb	%al, %dil
	movb	%dil, -552(%rbp)
	je	.L1862
.L1611:
	movl	$40, %edi
	call	_Znwm@PLT
.LEHE159:
	movq	%rax, %rsi
	movq	-112(%rbp), %rax
	movq	%r13, %rcx
	movq	%r15, %rdx
	movzbl	-552(%rbp), %edi
	movq	%rax, 32(%rsi)
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, -416(%rbp)
	jmp	.L1606
	.p2align 4,,10
	.p2align 3
.L1642:
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%r15, %r15
	jne	.L1643
.L1635:
	movq	%r12, -528(%rbp)
	movq	-440(%rbp), %r12
	leaq	-456(%rbp), %r14
	testq	%r12, %r12
	je	.L1640
.L1641:
	movq	24(%r12), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	%r12, %rdi
	movq	16(%r12), %r12
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L1641
.L1640:
	movq	-552(%rbp), %rax
	movq	-488(%rbp), %r13
	leaq	-504(%rbp), %r12
	movq	%rax, -528(%rbp)
	testq	%r13, %r13
	je	.L1552
.L1645:
	movq	24(%r13), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r13), %rdi
	leaq	48(%r13), %rax
	movq	16(%r13), %r14
	cmpq	%rax, %rdi
	je	.L1646
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%r14, %r14
	je	.L1552
.L1647:
	movq	%r14, %r13
	jmp	.L1645
	.p2align 4,,10
	.p2align 3
.L1646:
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%r14, %r14
	jne	.L1647
	jmp	.L1552
	.p2align 4,,10
	.p2align 3
.L1620:
	leaq	-320(%rbp), %r14
	movl	$0, -320(%rbp)
	movq	$0, -312(%rbp)
	movq	%r14, -304(%rbp)
	movq	%r14, -296(%rbp)
	movq	$0, -288(%rbp)
.L1622:
.LEHB160:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_10TypeOracleEEERPNT_5ScopeEv@PLT
.LEHE160:
	movq	(%rax), %rdi
	movl	-392(%rbp), %eax
	movl	%eax, -264(%rbp)
	movq	-384(%rbp), %rax
	movq	%rax, -256(%rbp)
	movq	-360(%rbp), %rax
	testq	%rax, %rax
	je	.L1624
	movl	-368(%rbp), %edx
	movq	%rax, -232(%rbp)
	movl	%edx, -240(%rbp)
	movq	-352(%rbp), %rdx
	movq	%rdx, -224(%rbp)
	movq	-344(%rbp), %rdx
	movq	%rdx, -216(%rbp)
	leaq	-240(%rbp), %rdx
	movq	%rdx, 8(%rax)
	movq	-336(%rbp), %rax
	movq	$0, -360(%rbp)
	movq	%rax, -208(%rbp)
	movq	%rbx, -352(%rbp)
	movq	%rbx, -344(%rbp)
	movq	$0, -336(%rbp)
.L1625:
	movq	-312(%rbp), %rax
	movq	%r12, -272(%rbp)
	testq	%rax, %rax
	je	.L1626
	movl	-320(%rbp), %edx
	movq	%rax, -184(%rbp)
	movl	%edx, -192(%rbp)
	movq	-304(%rbp), %rdx
	movq	%rdx, -176(%rbp)
	movq	-296(%rbp), %rdx
	movq	%rdx, -168(%rbp)
	leaq	-192(%rbp), %rdx
	movq	%rdx, 8(%rax)
	movq	-288(%rbp), %rax
	movq	$0, -312(%rbp)
	movq	%rax, -160(%rbp)
	movq	%r14, -304(%rbp)
	movq	%r14, -296(%rbp)
	movq	$0, -288(%rbp)
.L1627:
	leaq	-272(%rbp), %r13
	addq	$80, %rdi
	movl	$1, %ecx
	leaq	-536(%rbp), %rdx
	movq	%r13, %rsi
	movq	%rdi, -536(%rbp)
.LEHB161:
	call	_ZNSt10_HashtableIN2v88internal6torque9UnionTypeES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_insertIS3_NS5_10_AllocNodeISaINS5_10_Hash_nodeIS3_Lb1EEEEEEEESt4pairINS5_14_Node_iteratorIS3_Lb1ELb1EEEbEOT_RKT0_St17integral_constantIbLb1EEm
.LEHE161:
	movq	-184(%rbp), %r13
	movq	%r12, -272(%rbp)
	leaq	8(%rax), %rbx
	leaq	-200(%rbp), %r14
	testq	%r13, %r13
	je	.L1631
.L1628:
	movq	24(%r13), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	%r13, %rdi
	movq	16(%r13), %r13
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L1628
.L1631:
	movq	-552(%rbp), %rax
	movq	-232(%rbp), %r14
	leaq	-248(%rbp), %r13
	movq	%rax, -272(%rbp)
	testq	%r14, %r14
	je	.L1623
.L1630:
	movq	24(%r14), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r14), %rdi
	leaq	48(%r14), %rax
	movq	16(%r14), %r15
	cmpq	%rax, %rdi
	je	.L1632
	call	_ZdlPv@PLT
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%r15, %r15
	je	.L1623
.L1633:
	movq	%r15, %r14
	jmp	.L1630
	.p2align 4,,10
	.p2align 3
.L1632:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%r15, %r15
	jne	.L1633
	jmp	.L1623
	.p2align 4,,10
	.p2align 3
.L1698:
	movq	$0, -560(%rbp)
	jmp	.L1659
	.p2align 4,,10
	.p2align 3
.L1857:
	movq	$0, -552(%rbp)
	jmp	.L1655
	.p2align 4,,10
	.p2align 3
.L1699:
	xorl	%edi, %edi
	jmp	.L1662
	.p2align 4,,10
	.p2align 3
.L1855:
	movq	(%rbx), %rax
	leaq	-528(%rbp), %r15
	movq	%rbx, %rdi
	movq	%r15, %rsi
.LEHB162:
	call	*16(%rax)
	movq	-512(%rbp), %rdi
	testb	%al, %al
	jne	.L1614
	movq	-272(%rbp), %rsi
	call	_ZN2v88internal6torque4Type15CommonSupertypeEPKS2_S4_@PLT
	movq	-432(%rbp), %r14
	movq	%rax, -512(%rbp)
	cmpq	%r13, %r14
	jne	.L1615
	jmp	.L1616
	.p2align 4,,10
	.p2align 3
.L1617:
	cmpq	%r13, %rbx
	je	.L1616
	movq	%rbx, %r14
.L1615:
	movq	32(%r14), %rdi
	movq	-272(%rbp), %rsi
	movq	(%rdi), %rax
	call	*16(%rax)
.LEHE162:
	movq	%r14, %rdi
	movb	%al, -552(%rbp)
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movzbl	-552(%rbp), %edx
	movq	%rax, %rbx
	testb	%dl, %dl
	je	.L1617
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZSt28_Rb_tree_rebalance_for_erasePSt18_Rb_tree_node_baseRS_@PLT
	movq	%rax, %rdi
	call	_ZdlPv@PLT
	subq	$1, -416(%rbp)
	jmp	.L1617
	.p2align 4,,10
	.p2align 3
.L1861:
	cmpq	72(%r12), %rsi
	je	.L1676
	movq	%rbx, (%rsi)
	addq	$8, 64(%r12)
	movq	-528(%rbp), %rbx
	jmp	.L1675
	.p2align 4,,10
	.p2align 3
.L1618:
	leaq	-368(%rbp), %rbx
	movl	$0, -368(%rbp)
	movq	$0, -360(%rbp)
	movq	%rbx, -352(%rbp)
	movq	%rbx, -344(%rbp)
	movq	$0, -336(%rbp)
	jmp	.L1619
	.p2align 4,,10
	.p2align 3
.L1862:
	movq	32(%rdx), %rsi
	movq	-112(%rbp), %rdi
.LEHB163:
	call	_ZN2v88internal6torqueltERKNS1_4TypeES4_@PLT
.LEHE163:
	movb	%al, -552(%rbp)
	jmp	.L1611
	.p2align 4,,10
	.p2align 3
.L1616:
	leaq	-456(%rbp), %r14
	leaq	-272(%rbp), %rsi
	movq	%r14, %rdi
.LEHB164:
	call	_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE16_M_insert_uniqueIRKS5_EESt4pairISt17_Rb_tree_iteratorIS5_EbEOT_
.LEHE164:
	jmp	.L1843
.L1626:
	leaq	-192(%rbp), %rax
	movl	$0, -192(%rbp)
	movq	$0, -184(%rbp)
	movq	%rax, -176(%rbp)
	movq	%rax, -168(%rbp)
	movq	$0, -160(%rbp)
	jmp	.L1627
	.p2align 4,,10
	.p2align 3
.L1624:
	leaq	-240(%rbp), %rax
	movl	$0, -240(%rbp)
	movq	$0, -232(%rbp)
	movq	%rax, -224(%rbp)
	movq	%rax, -216(%rbp)
	movq	$0, -208(%rbp)
	jmp	.L1625
.L1676:
	leaq	56(%r12), %rdi
	movq	%r15, %rdx
.LEHB165:
	call	_ZNSt6vectorIPKN2v88internal6torque18BuiltinPointerTypeESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
.LEHE165:
	movq	-528(%rbp), %rbx
	jmp	.L1675
.L1848:
	leaq	.LC4(%rip), %rdi
.LEHB166:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE166:
.L1854:
	call	__stack_chk_fail@PLT
.L1847:
.LEHB167:
	call	_ZSt17__throw_bad_allocv@PLT
.LEHE167:
.L1849:
	leaq	.LC4(%rip), %rdi
.LEHB168:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE168:
.L1860:
.LEHB169:
	call	_ZSt17__throw_bad_allocv@PLT
.LEHE169:
.L1858:
.LEHB170:
	call	_ZSt17__throw_bad_allocv@PLT
.LEHE170:
.L1859:
.LEHB171:
	call	_ZSt17__throw_bad_allocv@PLT
.LEHE171:
.L1856:
	movq	%rbx, %rdi
	jmp	.L1574
.L1706:
	endbr64
	movq	%rax, %rdi
	jmp	.L1567
.L1701:
	endbr64
	movq	%rax, %r12
	jmp	.L1589
.L1700:
	endbr64
	movq	%rax, %r12
	jmp	.L1685
.L1704:
	endbr64
	movq	%rax, %r12
	jmp	.L1681
.L1703:
	endbr64
	movq	%rax, %rbx
	jmp	.L1689
.L1712:
	endbr64
	movq	%rax, %rbx
	jmp	.L1637
.L1714:
	endbr64
	movq	%rax, %r12
	jmp	.L1679
.L1711:
	endbr64
	movq	%rax, %r12
	jmp	.L1612
.L1715:
	endbr64
	movq	%rax, %r12
	jmp	.L1666
.L1710:
	endbr64
	movq	%rax, %r12
	jmp	.L1609
.L1708:
	endbr64
	movq	%rax, %rbx
	jmp	.L1648
.L1707:
	endbr64
	movq	%rax, %r12
	jmp	.L1587
.L1702:
	endbr64
	movq	%rax, %r12
	jmp	.L1686
.L1713:
	endbr64
	movq	%rax, %r12
	jmp	.L1682
.L1709:
	endbr64
	movq	%rax, %rbx
	jmp	.L1638
	.section	.gcc_except_table._ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_14TypeExpressionE,"a",@progbits
	.align 4
.LLSDA6556:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATT6556-.LLSDATTD6556
.LLSDATTD6556:
	.byte	0x1
	.uleb128 .LLSDACSE6556-.LLSDACSB6556
.LLSDACSB6556:
	.uleb128 .LEHB140-.LFB6556
	.uleb128 .LEHE140-.LEHB140
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB141-.LFB6556
	.uleb128 .LEHE141-.LEHB141
	.uleb128 .L1706-.LFB6556
	.uleb128 0x1
	.uleb128 .LEHB142-.LFB6556
	.uleb128 .LEHE142-.LEHB142
	.uleb128 .L1701-.LFB6556
	.uleb128 0
	.uleb128 .LEHB143-.LFB6556
	.uleb128 .LEHE143-.LEHB143
	.uleb128 .L1707-.LFB6556
	.uleb128 0
	.uleb128 .LEHB144-.LFB6556
	.uleb128 .LEHE144-.LEHB144
	.uleb128 .L1702-.LFB6556
	.uleb128 0
	.uleb128 .LEHB145-.LFB6556
	.uleb128 .LEHE145-.LEHB145
	.uleb128 .L1701-.LFB6556
	.uleb128 0
	.uleb128 .LEHB146-.LFB6556
	.uleb128 .LEHE146-.LEHB146
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB147-.LFB6556
	.uleb128 .LEHE147-.LEHB147
	.uleb128 .L1710-.LFB6556
	.uleb128 0
	.uleb128 .LEHB148-.LFB6556
	.uleb128 .LEHE148-.LEHB148
	.uleb128 .L1708-.LFB6556
	.uleb128 0
	.uleb128 .LEHB149-.LFB6556
	.uleb128 .LEHE149-.LEHB149
	.uleb128 .L1700-.LFB6556
	.uleb128 0
	.uleb128 .LEHB150-.LFB6556
	.uleb128 .LEHE150-.LEHB150
	.uleb128 .L1701-.LFB6556
	.uleb128 0
	.uleb128 .LEHB151-.LFB6556
	.uleb128 .LEHE151-.LEHB151
	.uleb128 .L1703-.LFB6556
	.uleb128 0
	.uleb128 .LEHB152-.LFB6556
	.uleb128 .LEHE152-.LEHB152
	.uleb128 .L1704-.LFB6556
	.uleb128 0
	.uleb128 .LEHB153-.LFB6556
	.uleb128 .LEHE153-.LEHB153
	.uleb128 .L1714-.LFB6556
	.uleb128 0
	.uleb128 .LEHB154-.LFB6556
	.uleb128 .LEHE154-.LEHB154
	.uleb128 .L1704-.LFB6556
	.uleb128 0
	.uleb128 .LEHB155-.LFB6556
	.uleb128 .LEHE155-.LEHB155
	.uleb128 .L1715-.LFB6556
	.uleb128 0
	.uleb128 .LEHB156-.LFB6556
	.uleb128 .LEHE156-.LEHB156
	.uleb128 .L1713-.LFB6556
	.uleb128 0
	.uleb128 .LEHB157-.LFB6556
	.uleb128 .LEHE157-.LEHB157
	.uleb128 .L1703-.LFB6556
	.uleb128 0
	.uleb128 .LEHB158-.LFB6556
	.uleb128 .LEHE158-.LEHB158
	.uleb128 .L1701-.LFB6556
	.uleb128 0
	.uleb128 .LEHB159-.LFB6556
	.uleb128 .LEHE159-.LEHB159
	.uleb128 .L1711-.LFB6556
	.uleb128 0
	.uleb128 .LEHB160-.LFB6556
	.uleb128 .LEHE160-.LEHB160
	.uleb128 .L1709-.LFB6556
	.uleb128 0
	.uleb128 .LEHB161-.LFB6556
	.uleb128 .LEHE161-.LEHB161
	.uleb128 .L1712-.LFB6556
	.uleb128 0
	.uleb128 .LEHB162-.LFB6556
	.uleb128 .LEHE162-.LEHB162
	.uleb128 .L1708-.LFB6556
	.uleb128 0
	.uleb128 .LEHB163-.LFB6556
	.uleb128 .LEHE163-.LEHB163
	.uleb128 .L1711-.LFB6556
	.uleb128 0
	.uleb128 .LEHB164-.LFB6556
	.uleb128 .LEHE164-.LEHB164
	.uleb128 .L1708-.LFB6556
	.uleb128 0
	.uleb128 .LEHB165-.LFB6556
	.uleb128 .LEHE165-.LEHB165
	.uleb128 .L1704-.LFB6556
	.uleb128 0
	.uleb128 .LEHB166-.LFB6556
	.uleb128 .LEHE166-.LEHB166
	.uleb128 .L1706-.LFB6556
	.uleb128 0x1
	.uleb128 .LEHB167-.LFB6556
	.uleb128 .LEHE167-.LEHB167
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB168-.LFB6556
	.uleb128 .LEHE168-.LEHB168
	.uleb128 .L1700-.LFB6556
	.uleb128 0
	.uleb128 .LEHB169-.LFB6556
	.uleb128 .LEHE169-.LEHB169
	.uleb128 .L1715-.LFB6556
	.uleb128 0
	.uleb128 .LEHB170-.LFB6556
	.uleb128 .LEHE170-.LEHB170
	.uleb128 .L1703-.LFB6556
	.uleb128 0
	.uleb128 .LEHB171-.LFB6556
	.uleb128 .LEHE171-.LEHB171
	.uleb128 .L1704-.LFB6556
	.uleb128 0
.LLSDACSE6556:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATT6556:
	.section	.text._ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_14TypeExpressionE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_14TypeExpressionE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6556
	.type	_ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_14TypeExpressionE.cold, @function
_ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_14TypeExpressionE.cold:
.LFSB6556:
.L1567:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	call	__cxa_begin_catch@PLT
.L1570:
	cmpq	%rbx, -568(%rbp)
	jne	.L1863
.LEHB172:
	call	__cxa_rethrow@PLT
.LEHE172:
.L1587:
	movq	-272(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1589
	call	_ZdlPv@PLT
.L1589:
	movq	%r15, %rdi
	call	_ZN2v88internal6torque13QualifiedNameD1Ev
	movq	%r12, %rdi
.LEHB173:
	call	_Unwind_Resume@PLT
.L1685:
	leaq	-272(%rbp), %rdi
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EED1Ev
	movq	%r12, %rdi
	call	_Unwind_Resume@PLT
.LEHE173:
.L1863:
	movq	-568(%rbp), %rax
	movq	(%rax), %rdi
	addq	$16, %rax
	cmpq	%rax, %rdi
	je	.L1569
	call	_ZdlPv@PLT
.L1569:
	addq	$32, -568(%rbp)
	jmp	.L1570
.L1705:
	endbr64
	movq	%rax, %r12
	call	__cxa_end_catch@PLT
	movq	-272(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1844
	call	_ZdlPv@PLT
.L1844:
	movq	%r12, %rdi
.LEHB174:
	call	_Unwind_Resume@PLT
.L1679:
	movq	-112(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1681
	call	_ZdlPv@PLT
.L1681:
	cmpq	$0, -552(%rbp)
	je	.L1688
	movq	-552(%rbp), %rdi
	call	_ZdlPv@PLT
.L1688:
	movq	%r12, %rbx
.L1689:
	movq	-400(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1690
	call	_ZdlPv@PLT
.L1690:
	movq	%rbx, %rdi
	call	_Unwind_Resume@PLT
.L1637:
	movq	%r13, %rdi
	call	_ZN2v88internal6torque9UnionTypeD1Ev
.L1638:
	leaq	-400(%rbp), %rdi
	leaq	-528(%rbp), %r15
	call	_ZN2v88internal6torque9UnionTypeD1Ev
.L1648:
	movq	%r15, %rdi
	call	_ZN2v88internal6torque9UnionTypeD1Ev
	movq	%rbx, %rdi
	call	_Unwind_Resume@PLT
.LEHE174:
.L1612:
	movq	-440(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	leaq	16+_ZTVN2v88internal6torque4TypeE(%rip), %rax
.L1845:
	movq	-488(%rbp), %rsi
	leaq	-504(%rbp), %rdi
	movq	%rax, -528(%rbp)
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	jmp	.L1844
.L1666:
	movq	-232(%rbp), %rsi
	leaq	16+_ZTVN2v88internal6torque4TypeE(%rip), %rax
	leaq	-248(%rbp), %rdi
	movq	%rax, -272(%rbp)
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
.L1667:
	cmpq	$0, -560(%rbp)
	je	.L1681
	movq	-560(%rbp), %rdi
	call	_ZdlPv@PLT
	jmp	.L1681
.L1609:
	movq	-552(%rbp), %rax
	jmp	.L1845
.L1686:
	movq	-272(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1589
	call	_ZdlPv@PLT
	jmp	.L1589
.L1682:
	movq	%r13, %rdi
	call	_ZN2v88internal6torque18BuiltinPointerTypeD1Ev
	jmp	.L1667
	.cfi_endproc
.LFE6556:
	.section	.gcc_except_table._ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_14TypeExpressionE
	.align 4
.LLSDAC6556:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATTC6556-.LLSDATTDC6556
.LLSDATTDC6556:
	.byte	0x1
	.uleb128 .LLSDACSEC6556-.LLSDACSBC6556
.LLSDACSBC6556:
	.uleb128 .LEHB172-.LCOLDB23
	.uleb128 .LEHE172-.LEHB172
	.uleb128 .L1705-.LCOLDB23
	.uleb128 0
	.uleb128 .LEHB173-.LCOLDB23
	.uleb128 .LEHE173-.LEHB173
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB174-.LCOLDB23
	.uleb128 .LEHE174-.LEHB174
	.uleb128 0
	.uleb128 0
.LLSDACSEC6556:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATTC6556:
	.section	.text.unlikely._ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_14TypeExpressionE
	.section	.text._ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_14TypeExpressionE
	.size	_ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_14TypeExpressionE, .-_ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_14TypeExpressionE
	.section	.text.unlikely._ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_14TypeExpressionE
	.size	_ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_14TypeExpressionE.cold, .-_ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_14TypeExpressionE.cold
.LCOLDE23:
	.section	.text._ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_14TypeExpressionE
.LHOTE23:
	.section	.text.unlikely._ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_20TypeAliasDeclarationE,"ax",@progbits
	.align 2
.LCOLDB24:
	.section	.text._ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_20TypeAliasDeclarationE,"ax",@progbits
.LHOTB24:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_20TypeAliasDeclarationE
	.type	_ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_20TypeAliasDeclarationE, @function
_ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_20TypeAliasDeclarationE:
.LFB6529:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6529
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$120, %rsp
	movq	40(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
.LEHB175:
	call	_ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_14TypeExpressionE
.LEHE175:
	movq	%r14, -96(%rbp)
	movq	%rax, %r13
	movq	32(%rbx), %rax
	movq	32(%rax), %r15
	movq	40(%rax), %r12
	movq	%r15, %rax
	addq	%r12, %rax
	setne	%bl
	testq	%r15, %r15
	sete	%dil
	andb	%dil, %bl
	movb	%bl, -116(%rbp)
	jne	.L1911
	movq	%r12, -104(%rbp)
	cmpq	$15, %r12
	ja	.L1912
	cmpq	$1, %r12
	jne	.L1868
	movzbl	(%r15), %eax
	movb	%al, -80(%rbp)
	movq	%r14, %rax
.L1869:
	movq	%r12, -88(%rbp)
	movb	$0, (%rax,%r12)
	movq	40(%r13), %r12
	leaq	32(%r13), %rax
	movq	%rax, -128(%rbp)
	testq	%r12, %r12
	je	.L1870
	movq	-88(%rbp), %rbx
	movq	-96(%rbp), %r15
	movq	%r13, -152(%rbp)
	movq	%r14, -160(%rbp)
	movq	%rbx, %r13
	movq	%r15, -144(%rbp)
	movq	%r15, %rbx
	jmp	.L1871
	.p2align 4,,10
	.p2align 3
.L1876:
	movq	16(%r12), %rax
	movl	$1, %esi
	testq	%rax, %rax
	je	.L1872
.L1913:
	movq	%rax, %r12
.L1871:
	movq	40(%r12), %r15
	movq	32(%r12), %r11
	cmpq	%r15, %r13
	movq	%r15, %r14
	cmovbe	%r13, %r14
	testq	%r14, %r14
	je	.L1873
	movq	%r11, %rsi
	movq	%r14, %rdx
	movq	%rbx, %rdi
	movq	%r11, -136(%rbp)
	call	memcmp@PLT
	movq	-136(%rbp), %r11
	testl	%eax, %eax
	jne	.L1874
.L1873:
	movq	%r13, %rax
	movl	$2147483648, %edx
	subq	%r15, %rax
	cmpq	%rdx, %rax
	jge	.L1875
	movabsq	$-2147483649, %rcx
	cmpq	%rcx, %rax
	jle	.L1876
.L1874:
	testl	%eax, %eax
	js	.L1876
.L1875:
	movq	24(%r12), %rax
	xorl	%esi, %esi
	testq	%rax, %rax
	jne	.L1913
.L1872:
	movq	%r14, %rdx
	movq	%r15, %rcx
	movq	%rbx, %r15
	movq	%r13, %rbx
	movq	-144(%rbp), %r10
	movq	-160(%rbp), %r14
	movq	-152(%rbp), %r13
	testb	%sil, %sil
	jne	.L1914
.L1879:
	testq	%rdx, %rdx
	je	.L1880
	movq	%r15, %rsi
	movq	%r11, %rdi
	movq	%r10, -144(%rbp)
	movq	%rcx, -136(%rbp)
	call	memcmp@PLT
	movq	-136(%rbp), %rcx
	movq	-144(%rbp), %r10
	testl	%eax, %eax
	jne	.L1881
.L1880:
	subq	%rbx, %rcx
	cmpq	$2147483647, %rcx
	jg	.L1882
	cmpq	$-2147483648, %rcx
	jl	.L1883
	movl	%ecx, %eax
.L1881:
	testl	%eax, %eax
	jns	.L1882
.L1883:
	testq	%r12, %r12
	je	.L1915
.L1884:
	cmpq	%r12, -128(%rbp)
	jne	.L1916
.L1910:
	movb	$1, -116(%rbp)
.L1885:
	movl	$64, %edi
.LEHB176:
	call	_Znwm@PLT
.LEHE176:
	movq	%rax, %rsi
	leaq	48(%rax), %rax
	movq	%rax, 32(%rsi)
	movq	-96(%rbp), %rax
	cmpq	%r14, %rax
	je	.L1917
	movq	%rax, 32(%rsi)
	movq	-80(%rbp), %rax
	movq	%rax, 48(%rsi)
.L1889:
	movq	-88(%rbp), %rax
	movzbl	-116(%rbp), %edi
	movq	%r12, %rdx
	movq	%r14, -96(%rbp)
	movq	-128(%rbp), %rcx
	movq	$0, -88(%rbp)
	movq	%rax, 40(%rsi)
	movb	$0, -80(%rbp)
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 64(%r13)
	movq	-96(%rbp), %r10
.L1882:
	cmpq	%r14, %r10
	je	.L1864
	movq	%r10, %rdi
	call	_ZdlPv@PLT
.L1864:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1918
	addq	$120, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1868:
	.cfi_restore_state
	testq	%r12, %r12
	jne	.L1919
	movq	%r14, %rax
	jmp	.L1869
	.p2align 4,,10
	.p2align 3
.L1914:
	cmpq	48(%r13), %r12
	je	.L1884
.L1893:
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	%rbx, %rdx
	movq	%r15, %r10
	movq	40(%rax), %rcx
	movq	32(%rax), %r11
	cmpq	%rbx, %rcx
	cmovbe	%rcx, %rdx
	jmp	.L1879
	.p2align 4,,10
	.p2align 3
.L1912:
	leaq	-96(%rbp), %r8
	leaq	-104(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r8, %rdi
.LEHB177:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -96(%rbp)
	movq	%rax, %rdi
	movq	-104(%rbp), %rax
	movq	%rax, -80(%rbp)
.L1867:
	movq	%r12, %rdx
	movq	%r15, %rsi
	call	memcpy@PLT
	movq	-104(%rbp), %r12
	movq	-96(%rbp), %rax
	jmp	.L1869
	.p2align 4,,10
	.p2align 3
.L1917:
	movdqa	-80(%rbp), %xmm0
	movups	%xmm0, 48(%rsi)
	jmp	.L1889
	.p2align 4,,10
	.p2align 3
.L1916:
	movq	40(%r12), %rcx
	cmpq	%rcx, %rbx
	movq	%rcx, %rdx
	cmovbe	%rbx, %rdx
	testq	%rdx, %rdx
	je	.L1886
	movq	32(%r12), %rsi
	movq	%r15, %rdi
	movq	%rcx, -136(%rbp)
	call	memcmp@PLT
	movq	-136(%rbp), %rcx
	testl	%eax, %eax
	jne	.L1887
.L1886:
	subq	%rcx, %rbx
	cmpq	$2147483647, %rbx
	jg	.L1885
	cmpq	$-2147483648, %rbx
	jl	.L1910
	movl	%ebx, %eax
.L1887:
	shrl	$31, %eax
	movl	%eax, -116(%rbp)
	jmp	.L1885
	.p2align 4,,10
	.p2align 3
.L1870:
	leaq	32(%r13), %rax
	leaq	32(%r13), %r12
	cmpq	48(%r13), %rax
	je	.L1910
	movq	-96(%rbp), %r15
	movq	-88(%rbp), %rbx
	movq	%rax, %r12
	jmp	.L1893
.L1918:
	call	__stack_chk_fail@PLT
.L1911:
	leaq	.LC4(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE177:
.L1915:
	movq	%r15, %r10
	jmp	.L1882
.L1919:
	movq	%r14, %rdi
	jmp	.L1867
.L1899:
	endbr64
	movq	%rax, %r12
	jmp	.L1891
	.section	.gcc_except_table._ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_20TypeAliasDeclarationE,"a",@progbits
.LLSDA6529:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6529-.LLSDACSB6529
.LLSDACSB6529:
	.uleb128 .LEHB175-.LFB6529
	.uleb128 .LEHE175-.LEHB175
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB176-.LFB6529
	.uleb128 .LEHE176-.LEHB176
	.uleb128 .L1899-.LFB6529
	.uleb128 0
	.uleb128 .LEHB177-.LFB6529
	.uleb128 .LEHE177-.LEHB177
	.uleb128 0
	.uleb128 0
.LLSDACSE6529:
	.section	.text._ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_20TypeAliasDeclarationE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_20TypeAliasDeclarationE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6529
	.type	_ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_20TypeAliasDeclarationE.cold, @function
_ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_20TypeAliasDeclarationE.cold:
.LFSB6529:
.L1891:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L1892
	call	_ZdlPv@PLT
.L1892:
	movq	%r12, %rdi
.LEHB178:
	call	_Unwind_Resume@PLT
.LEHE178:
	.cfi_endproc
.LFE6529:
	.section	.gcc_except_table._ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_20TypeAliasDeclarationE
.LLSDAC6529:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6529-.LLSDACSBC6529
.LLSDACSBC6529:
	.uleb128 .LEHB178-.LCOLDB24
	.uleb128 .LEHE178-.LEHB178
	.uleb128 0
	.uleb128 0
.LLSDACSEC6529:
	.section	.text.unlikely._ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_20TypeAliasDeclarationE
	.section	.text._ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_20TypeAliasDeclarationE
	.size	_ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_20TypeAliasDeclarationE, .-_ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_20TypeAliasDeclarationE
	.section	.text.unlikely._ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_20TypeAliasDeclarationE
	.size	_ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_20TypeAliasDeclarationE.cold, .-_ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_20TypeAliasDeclarationE.cold
.LCOLDE24:
	.section	.text._ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_20TypeAliasDeclarationE
.LHOTE24:
	.section	.rodata._ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_17StructDeclarationENS_4base8OptionalINS1_17SpecializationKeyINS1_17GenericStructTypeEEEEE.str1.1,"aMS",@progbits,1
.LC25:
	.string	"\""
.LC26:
	.string	"\" carries constexpr type \""
.LC27:
	.string	"struct field \""
	.section	.text.unlikely._ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_17StructDeclarationENS_4base8OptionalINS1_17SpecializationKeyINS1_17GenericStructTypeEEEEE,"ax",@progbits
	.align 2
.LCOLDB28:
	.section	.text._ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_17StructDeclarationENS_4base8OptionalINS1_17SpecializationKeyINS1_17GenericStructTypeEEEEE,"ax",@progbits
.LHOTB28:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_17StructDeclarationENS_4base8OptionalINS1_17SpecializationKeyINS1_17GenericStructTypeEEEEE
	.type	_ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_17StructDeclarationENS_4base8OptionalINS1_17SpecializationKeyINS1_17GenericStructTypeEEEEE, @function
_ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_17StructDeclarationENS_4base8OptionalINS1_17SpecializationKeyINS1_17GenericStructTypeEEEEE:
.LFB6535:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6535
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$280, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, (%rsi)
	movb	$0, -208(%rbp)
	movb	$0, -200(%rbp)
	jne	.L1997
.L1921:
	leaq	-208(%rbp), %rax
	movq	%r13, %rdi
	movq	%rax, %rsi
	movq	%rax, -320(%rbp)
.LEHB179:
	call	_ZN2v88internal6torque10TypeOracle13GetStructTypeEPKNS1_17StructDeclarationENS_4base8OptionalINS1_17SpecializationKeyINS1_17GenericStructTypeEEEEE
.LEHE179:
	cmpb	$0, -208(%rbp)
	movq	%rax, %r15
	jne	.L1998
.L1929:
	movq	104(%r15), %rax
	movq	%rax, -256(%rbp)
.LEHB180:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rax
	movq	%rax, -248(%rbp)
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
.LEHE180:
	leaq	-256(%rbp), %rdx
	movq	%rdx, (%rax)
	movdqu	12(%r13), %xmm4
	movl	28(%r13), %eax
	movaps	%xmm4, -240(%rbp)
	movl	%eax, -224(%rbp)
.LEHB181:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rax
	movq	%rax, -216(%rbp)
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
.LEHE181:
	leaq	-240(%rbp), %rdx
	cmpb	$0, (%rbx)
	movq	%rdx, (%rax)
	jne	.L1930
.L1933:
	movq	72(%r13), %rax
	movq	64(%r13), %rbx
	movq	$0, -296(%rbp)
	movq	%rax, -312(%rbp)
	cmpq	%rbx, %rax
	jne	.L1951
	jmp	.L1932
	.p2align 4,,10
	.p2align 3
.L2004:
	movzbl	(%r14), %eax
	movb	%al, -96(%rbp)
	movq	-280(%rbp), %rax
.L1940:
	movq	%r12, -104(%rbp)
	leaq	_ZN2v88internal6torque13AggregateType13RegisterFieldENS1_5FieldE(%rip), %rdx
	movb	$0, (%rax,%r12)
	movq	-296(%rbp), %rax
	movq	%r13, -80(%rbp)
	movq	%rax, -72(%rbp)
	movzbl	16(%rbx), %eax
	movb	%al, -63(%rbp)
	cmpq	%rdx, -288(%rbp)
	jne	.L1941
	movq	88(%r15), %r12
	cmpq	96(%r15), %r12
	je	.L1942
	movdqa	-160(%rbp), %xmm2
	leaq	64(%r12), %rdi
	movq	%rdi, 48(%r12)
	movups	%xmm2, (%r12)
	movl	-144(%rbp), %eax
	movl	%eax, 16(%r12)
	movq	-136(%rbp), %rax
	movq	%rax, 24(%r12)
	movdqa	-128(%rbp), %xmm3
	movups	%xmm3, 32(%r12)
	movq	-112(%rbp), %rax
	movq	-104(%rbp), %r14
	movq	%rax, %rsi
	movq	%rax, -288(%rbp)
	addq	%r14, %rsi
	je	.L1943
	testq	%rax, %rax
	je	.L1999
.L1943:
	movq	%r14, -264(%rbp)
	cmpq	$15, %r14
	ja	.L2000
	cmpq	$1, %r14
	jne	.L1946
	movq	-288(%rbp), %rax
	movzbl	(%rax), %eax
	movb	%al, 64(%r12)
.L1947:
	movq	%r14, 56(%r12)
	movb	$0, (%rdi,%r14)
	movq	-80(%rbp), %rax
	movq	%rax, 80(%r12)
	movq	-72(%rbp), %rax
	movq	%rax, 88(%r12)
	movzbl	-64(%rbp), %eax
	movb	%al, 96(%r12)
	movzbl	-63(%rbp), %eax
	movb	%al, 97(%r12)
	movzbl	-62(%rbp), %eax
	movb	%al, 98(%r12)
	addq	$104, 88(%r15)
.L1948:
	movq	-112(%rbp), %rdi
	cmpq	-280(%rbp), %rdi
	je	.L1950
	call	_ZdlPv@PLT
.L1950:
	movq	%r13, %rdi
.LEHB182:
	call	_ZN2v88internal6torque16LoweredSlotCountEPKNS1_4TypeE@PLT
.LEHE182:
	addq	%rax, -296(%rbp)
	addq	$24, %rbx
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
	movq	-184(%rbp), %rdx
	movq	%rdx, (%rax)
	cmpq	%rbx, -312(%rbp)
	je	.L1932
.L1951:
	movq	8(%rbx), %rax
	movdqu	12(%rax), %xmm0
	movaps	%xmm0, -208(%rbp)
	movl	28(%rax), %eax
	movl	%eax, -192(%rbp)
.LEHB183:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rax
	movq	%rax, -184(%rbp)
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
.LEHE183:
	movq	-320(%rbp), %rdx
	movq	%rdx, (%rax)
	movq	8(%rbx), %rdi
.LEHB184:
	call	_ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_14TypeExpressionE
	movq	%rax, %r13
	movq	(%rax), %rax
	movq	%r13, %rdi
	call	*32(%rax)
	testb	%al, %al
	jne	.L2001
	movq	(%r15), %rax
	leaq	-160(%rbp), %rdi
	movl	$13, %ecx
	movq	%rdi, -304(%rbp)
	movq	112(%rax), %rax
	movq	%rax, -288(%rbp)
	xorl	%eax, %eax
	rep stosq
	leaq	-96(%rbp), %rcx
	movq	%rcx, -280(%rbp)
	movq	(%rbx), %rax
	movdqu	12(%rax), %xmm1
	movl	28(%rax), %eax
	movq	%r15, -136(%rbp)
	movl	%eax, -144(%rbp)
	movaps	%xmm1, -160(%rbp)
	movq	(%rbx), %rax
	movq	%rcx, -112(%rbp)
	movq	32(%rax), %r14
	movq	40(%rax), %r12
	movq	%r14, %rax
	addq	%r12, %rax
	je	.L1936
	testq	%r14, %r14
	je	.L2002
.L1936:
	movq	%r12, -264(%rbp)
	cmpq	$15, %r12
	ja	.L2003
	cmpq	$1, %r12
	je	.L2004
	testq	%r12, %r12
	jne	.L2005
	movq	-280(%rbp), %rax
	jmp	.L1940
	.p2align 4,,10
	.p2align 3
.L2003:
	leaq	-264(%rbp), %rsi
	leaq	-112(%rbp), %rdi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE184:
	movq	%rax, -112(%rbp)
	movq	%rax, %rdi
	movq	-264(%rbp), %rax
	movq	%rax, -96(%rbp)
.L1938:
	movq	%r12, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-264(%rbp), %r12
	movq	-112(%rbp), %rax
	jmp	.L1940
	.p2align 4,,10
	.p2align 3
.L1942:
	movq	-304(%rbp), %rdx
	leaq	80(%r15), %rdi
	movq	%r12, %rsi
.LEHB185:
	call	_ZNSt6vectorIN2v88internal6torque5FieldESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	jmp	.L1948
	.p2align 4,,10
	.p2align 3
.L1941:
	movq	-304(%rbp), %rsi
	movq	-288(%rbp), %rax
	movq	%r15, %rdi
	call	*%rax
	jmp	.L1948
	.p2align 4,,10
	.p2align 3
.L1946:
	testq	%r14, %r14
	je	.L1947
	jmp	.L1945
	.p2align 4,,10
	.p2align 3
.L2000:
	leaq	-264(%rbp), %rsi
	leaq	48(%r12), %rdi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE185:
	movq	%rax, 48(%r12)
	movq	%rax, %rdi
	movq	-264(%rbp), %rax
	movq	%rax, 64(%r12)
.L1945:
	movq	-288(%rbp), %rsi
	movq	%r14, %rdx
	call	memcpy@PLT
	movq	-264(%rbp), %r14
	movq	48(%r12), %rdi
	jmp	.L1947
	.p2align 4,,10
	.p2align 3
.L1932:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
	movq	-216(%rbp), %rdx
	movq	%rdx, (%rax)
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
	movq	-248(%rbp), %rdx
	movq	%rdx, (%rax)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2006
	addq	$280, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1930:
	.cfi_restore_state
	movq	8(%rbx), %rax
	movq	16(%rbx), %r12
	movq	104(%rax), %rax
	movq	88(%rax), %rbx
	movq	96(%rax), %r14
	cmpq	%rbx, %r14
	je	.L1933
	.p2align 4,,10
	.p2align 3
.L1934:
	movq	(%r12), %rsi
	movq	(%rbx), %rdi
.LEHB186:
	call	_ZN2v88internal6torque12Declarations11DeclareTypeEPKNS1_10IdentifierEPKNS1_4TypeE@PLT
.LEHE186:
	addq	$8, %rbx
	movb	$0, 64(%rax)
	addq	$8, %r12
	cmpq	%rbx, %r14
	jne	.L1934
	jmp	.L1933
	.p2align 4,,10
	.p2align 3
.L1998:
	movq	-192(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1929
	call	_ZdlPv@PLT
	jmp	.L1929
	.p2align 4,,10
	.p2align 3
.L1997:
	movq	$0, -192(%rbp)
	movq	8(%rsi), %rax
	movq	$0, -184(%rbp)
	movq	16(%rsi), %rsi
	movq	%rax, -200(%rbp)
	movq	24(%rbx), %rax
	movq	$0, -176(%rbp)
	movq	%rax, %r12
	subq	%rsi, %r12
	movq	%r12, %rdx
	sarq	$3, %rdx
	je	.L2007
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L2008
	movq	%r12, %rdi
.LEHB187:
	call	_Znwm@PLT
.LEHE187:
	movq	%rax, %rcx
	movq	24(%rbx), %rax
	movq	16(%rbx), %rsi
	movq	%rax, %r14
	subq	%rsi, %r14
.L1923:
	addq	%rcx, %r12
	movq	%rcx, -192(%rbp)
	movq	%r12, -176(%rbp)
	cmpq	%rax, %rsi
	je	.L1925
	movq	%rcx, %rdi
	movq	%r14, %rdx
	call	memcpy@PLT
	movq	%rax, %rcx
.L1925:
	addq	%r14, %rcx
	movb	$1, -208(%rbp)
	movq	%rcx, -184(%rbp)
	jmp	.L1921
.L2007:
	movq	%r12, %r14
	xorl	%ecx, %ecx
	jmp	.L1923
.L2001:
	movq	(%rbx), %rsi
	leaq	.LC25(%rip), %r8
	movq	%r13, %rcx
	leaq	.LC26(%rip), %rdx
	leaq	.LC27(%rip), %rdi
	addq	$32, %rsi
.LEHB188:
	call	_ZN2v88internal6torque11ReportErrorIJRA15_KcRNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA27_S3_RKNS1_4TypeERA2_S3_EEEvDpOT_
.LEHE188:
.L1999:
	leaq	.LC4(%rip), %rdi
.LEHB189:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE189:
.L2002:
	leaq	.LC4(%rip), %rdi
.LEHB190:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE190:
.L2008:
.LEHB191:
	call	_ZSt17__throw_bad_allocv@PLT
.LEHE191:
.L2005:
	movq	-280(%rbp), %rdi
	jmp	.L1938
.L2006:
	call	__stack_chk_fail@PLT
.L1961:
	endbr64
	movq	%rax, %r12
	jmp	.L1952
.L1965:
	endbr64
	movq	%rax, %r12
	jmp	.L1954
.L1962:
	endbr64
	movq	%rax, %r12
	jmp	.L1958
.L1966:
	endbr64
	movq	%rax, %r12
	jmp	.L1927
.L1964:
	endbr64
	movq	%rax, %r12
	jmp	.L1956
.L1963:
	endbr64
	movq	%rax, %r12
	jmp	.L1957
	.section	.gcc_except_table._ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_17StructDeclarationENS_4base8OptionalINS1_17SpecializationKeyINS1_17GenericStructTypeEEEEE,"a",@progbits
.LLSDA6535:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6535-.LLSDACSB6535
.LLSDACSB6535:
	.uleb128 .LEHB179-.LFB6535
	.uleb128 .LEHE179-.LEHB179
	.uleb128 .L1961-.LFB6535
	.uleb128 0
	.uleb128 .LEHB180-.LFB6535
	.uleb128 .LEHE180-.LEHB180
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB181-.LFB6535
	.uleb128 .LEHE181-.LEHB181
	.uleb128 .L1962-.LFB6535
	.uleb128 0
	.uleb128 .LEHB182-.LFB6535
	.uleb128 .LEHE182-.LEHB182
	.uleb128 .L1964-.LFB6535
	.uleb128 0
	.uleb128 .LEHB183-.LFB6535
	.uleb128 .LEHE183-.LEHB183
	.uleb128 .L1963-.LFB6535
	.uleb128 0
	.uleb128 .LEHB184-.LFB6535
	.uleb128 .LEHE184-.LEHB184
	.uleb128 .L1964-.LFB6535
	.uleb128 0
	.uleb128 .LEHB185-.LFB6535
	.uleb128 .LEHE185-.LEHB185
	.uleb128 .L1965-.LFB6535
	.uleb128 0
	.uleb128 .LEHB186-.LFB6535
	.uleb128 .LEHE186-.LEHB186
	.uleb128 .L1963-.LFB6535
	.uleb128 0
	.uleb128 .LEHB187-.LFB6535
	.uleb128 .LEHE187-.LEHB187
	.uleb128 .L1966-.LFB6535
	.uleb128 0
	.uleb128 .LEHB188-.LFB6535
	.uleb128 .LEHE188-.LEHB188
	.uleb128 .L1964-.LFB6535
	.uleb128 0
	.uleb128 .LEHB189-.LFB6535
	.uleb128 .LEHE189-.LEHB189
	.uleb128 .L1965-.LFB6535
	.uleb128 0
	.uleb128 .LEHB190-.LFB6535
	.uleb128 .LEHE190-.LEHB190
	.uleb128 .L1964-.LFB6535
	.uleb128 0
	.uleb128 .LEHB191-.LFB6535
	.uleb128 .LEHE191-.LEHB191
	.uleb128 .L1966-.LFB6535
	.uleb128 0
.LLSDACSE6535:
	.section	.text._ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_17StructDeclarationENS_4base8OptionalINS1_17SpecializationKeyINS1_17GenericStructTypeEEEEE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_17StructDeclarationENS_4base8OptionalINS1_17SpecializationKeyINS1_17GenericStructTypeEEEEE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6535
	.type	_ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_17StructDeclarationENS_4base8OptionalINS1_17SpecializationKeyINS1_17GenericStructTypeEEEEE.cold, @function
_ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_17StructDeclarationENS_4base8OptionalINS1_17SpecializationKeyINS1_17GenericStructTypeEEEEE.cold:
.LFSB6535:
.L1952:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	cmpb	$0, -208(%rbp)
	je	.L1953
	movq	-192(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1953
	call	_ZdlPv@PLT
.L1953:
	movq	%r12, %rdi
.LEHB192:
	call	_Unwind_Resume@PLT
.LEHE192:
.L1954:
	movq	-112(%rbp), %rdi
	cmpq	-280(%rbp), %rdi
	je	.L1956
	call	_ZdlPv@PLT
.L1956:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
	movq	-184(%rbp), %rdx
	movq	%rdx, (%rax)
.L1957:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
	movq	-216(%rbp), %rdx
	movq	%rdx, (%rax)
.L1958:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
	movq	-248(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rdx, (%rax)
.LEHB193:
	call	_Unwind_Resume@PLT
.L1927:
	cmpb	$0, -208(%rbp)
	je	.L1928
	movq	-192(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1928
	call	_ZdlPv@PLT
.L1928:
	movq	%r12, %rdi
	call	_Unwind_Resume@PLT
.LEHE193:
	.cfi_endproc
.LFE6535:
	.section	.gcc_except_table._ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_17StructDeclarationENS_4base8OptionalINS1_17SpecializationKeyINS1_17GenericStructTypeEEEEE
.LLSDAC6535:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6535-.LLSDACSBC6535
.LLSDACSBC6535:
	.uleb128 .LEHB192-.LCOLDB28
	.uleb128 .LEHE192-.LEHB192
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB193-.LCOLDB28
	.uleb128 .LEHE193-.LEHB193
	.uleb128 0
	.uleb128 0
.LLSDACSEC6535:
	.section	.text.unlikely._ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_17StructDeclarationENS_4base8OptionalINS1_17SpecializationKeyINS1_17GenericStructTypeEEEEE
	.section	.text._ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_17StructDeclarationENS_4base8OptionalINS1_17SpecializationKeyINS1_17GenericStructTypeEEEEE
	.size	_ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_17StructDeclarationENS_4base8OptionalINS1_17SpecializationKeyINS1_17GenericStructTypeEEEEE, .-_ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_17StructDeclarationENS_4base8OptionalINS1_17SpecializationKeyINS1_17GenericStructTypeEEEEE
	.section	.text.unlikely._ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_17StructDeclarationENS_4base8OptionalINS1_17SpecializationKeyINS1_17GenericStructTypeEEEEE
	.size	_ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_17StructDeclarationENS_4base8OptionalINS1_17SpecializationKeyINS1_17GenericStructTypeEEEEE.cold, .-_ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_17StructDeclarationENS_4base8OptionalINS1_17SpecializationKeyINS1_17GenericStructTypeEEEEE.cold
.LCOLDE28:
	.section	.text._ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_17StructDeclarationENS_4base8OptionalINS1_17SpecializationKeyINS1_17GenericStructTypeEEEEE
.LHOTE28:
	.section	.rodata._ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_16ClassDeclarationE.str1.8,"aMS",@progbits,1
	.align 8
.LC29:
	.string	"Extern class must extend another type."
	.align 8
.LC30:
	.string	"\" must extend either Tagged or an already declared class"
	.section	.rodata._ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_16ClassDeclarationE.str1.1,"aMS",@progbits,1
.LC31:
	.string	"class \""
.LC32:
	.string	" must extend class Struct."
.LC33:
	.string	"Intern class "
	.section	.rodata._ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_16ClassDeclarationE.str1.8
	.align 8
.LC34:
	.string	"Only extern classes can specify a generated type."
	.section	.text.unlikely._ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_16ClassDeclarationE,"ax",@progbits
	.align 2
.LCOLDB35:
	.section	.text._ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_16ClassDeclarationE,"ax",@progbits
.LHOTB35:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_16ClassDeclarationE
	.type	_ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_16ClassDeclarationE, @function
_ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_16ClassDeclarationE:
.LFB6549:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6549
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-208(%rbp), %r15
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$232, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rax
	movq	%r15, -224(%rbp)
	movq	32(%rax), %r13
	movq	40(%rax), %r12
	movq	%r13, %rax
	addq	%r12, %rax
	je	.L2010
	testq	%r13, %r13
	je	.L2039
.L2010:
	movq	%r12, -232(%rbp)
	cmpq	$15, %r12
	ja	.L2169
	cmpq	$1, %r12
	jne	.L2013
	movzbl	0(%r13), %eax
	movb	%al, -208(%rbp)
	movq	%r15, %rax
.L2014:
	movq	%r12, -216(%rbp)
	movb	$0, (%rax,%r12)
	movq	-224(%rbp), %rax
	cmpq	%r15, %rax
	je	.L2170
	movq	-208(%rbp), %rcx
	movq	-216(%rbp), %rdx
	pxor	%xmm0, %xmm0
	leaq	-88(%rbp), %r14
	leaq	-144(%rbp), %rsi
	movq	%r14, -104(%rbp)
	movq	%rax, -160(%rbp)
	movq	%rcx, -144(%rbp)
	movq	%rdx, -152(%rbp)
	movq	%r15, -224(%rbp)
	movq	$0, -216(%rbp)
	movb	$0, -208(%rbp)
	movq	$0, -112(%rbp)
	movaps	%xmm0, -128(%rbp)
	cmpq	%rsi, %rax
	je	.L2016
	movq	%rax, -104(%rbp)
	movq	%rcx, -88(%rbp)
.L2018:
	leaq	-128(%rbp), %r13
	movq	%rdx, -96(%rbp)
	movq	%r13, %rdi
.LEHB194:
	call	_ZN2v88internal6torque12Declarations15LookupTypeAliasERKNS1_13QualifiedNameE@PLT
.LEHE194:
	movq	-104(%rbp), %rdi
	movq	%rax, %r12
	cmpq	%r14, %rdi
	je	.L2019
	call	_ZdlPv@PLT
.L2019:
	movq	-120(%rbp), %rax
	movq	-128(%rbp), %r14
	cmpq	%r14, %rax
	je	.L2020
	.p2align 4,,10
	.p2align 3
.L2024:
	movq	(%r14), %rdi
	leaq	16(%r14), %rdx
	cmpq	%rdx, %rdi
	je	.L2021
	movq	%rax, -248(%rbp)
	addq	$32, %r14
	call	_ZdlPv@PLT
	movq	-248(%rbp), %rax
	cmpq	%r14, %rax
	jne	.L2024
.L2022:
	movq	-128(%rbp), %r14
.L2020:
	testq	%r14, %r14
	je	.L2025
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L2025:
	movq	-224(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L2026
	call	_ZdlPv@PLT
.L2026:
	movq	%r12, -232(%rbp)
.LEHB195:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_13GlobalContextEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rdi
	movq	216(%rdi), %rsi
	cmpq	224(%rdi), %rsi
	je	.L2027
	movq	-232(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, 216(%rdi)
.L2028:
	movzbl	48(%rbx), %eax
	testb	$1, 40(%rbx)
	je	.L2029
	testb	%al, %al
	je	.L2171
	movq	56(%rbx), %rdi
	leaq	-112(%rbp), %r15
	call	_ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_14TypeExpressionE
	movq	%rax, %r14
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_10TypeOracleEEERPNT_5ScopeEv@PLT
.LEHE195:
	movl	$25701, %r10d
	movq	%r13, %rdi
	movq	%r15, -128(%rbp)
	movl	$1734828372, -112(%rbp)
	movw	%r10w, -108(%rbp)
	movq	$6, -120(%rbp)
	movb	$0, -106(%rbp)
.LEHB196:
	call	_ZN2v88internal6torque12Declarations16LookupGlobalTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
.LEHE196:
	movq	-128(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L2032
	movq	%rax, -248(%rbp)
	call	_ZdlPv@PLT
	movq	-248(%rbp), %rax
.L2032:
	cmpq	%rax, %r14
	je	.L2034
	testq	%r14, %r14
	je	.L2037
	cmpl	$5, 8(%r14)
	jne	.L2037
.L2034:
	movq	32(%rbx), %rax
	leaq	-176(%rbp), %rcx
	movq	%rcx, -248(%rbp)
	movq	32(%rax), %r8
	movq	40(%rax), %r15
	movq	%rcx, -192(%rbp)
	movq	%r8, %rax
	addq	%r15, %rax
	je	.L2115
	testq	%r8, %r8
	je	.L2039
.L2115:
	movq	%r15, -232(%rbp)
	cmpq	$15, %r15
	ja	.L2172
	cmpq	$1, %r15
	jne	.L2043
	movzbl	(%r8), %eax
	movb	%al, -176(%rbp)
	movq	-248(%rbp), %rax
.L2044:
	movq	%r15, -184(%rbp)
	movb	$0, (%rax,%r15)
	cmpb	$0, 64(%rbx)
	je	.L2045
	leaq	-104(%rbp), %rax
	movq	80(%rbx), %r15
	movb	$0, -128(%rbp)
	movq	%rax, -256(%rbp)
	movq	%rax, -120(%rbp)
	movq	72(%rbx), %rax
	movq	%rax, %rcx
	movq	%rax, -264(%rbp)
	addq	%r15, %rcx
	jne	.L2173
.L2046:
	movq	%r15, -232(%rbp)
	cmpq	$15, %r15
	ja	.L2174
	cmpq	$1, %r15
	jne	.L2049
	movq	-264(%rbp), %rax
	movzbl	(%rax), %eax
	movb	%al, -104(%rbp)
	movq	-256(%rbp), %rax
.L2050:
	movq	%r15, -112(%rbp)
	leaq	-160(%rbp), %rdi
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$0, (%rax,%r15)
	movb	$1, -128(%rbp)
.LEHB197:
	call	_ZN2v88internal6torque12_GLOBAL__N_120ComputeGeneratesTypeENS_4base8OptionalINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEb
.LEHE197:
	movq	-160(%rbp), %rax
	leaq	-144(%rbp), %r13
	movq	-192(%rbp), %rdi
	movq	-152(%rbp), %rdx
	cmpq	%r13, %rax
	je	.L2175
	movq	-144(%rbp), %rcx
	cmpq	-248(%rbp), %rdi
	je	.L2176
	movq	%rdx, %xmm0
	movq	%rcx, %xmm3
	movq	-176(%rbp), %rsi
	movq	%rax, -192(%rbp)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, -184(%rbp)
	testq	%rdi, %rdi
	je	.L2060
	movq	%rdi, -160(%rbp)
	movq	%rsi, -144(%rbp)
.L2058:
	movq	$0, -152(%rbp)
	movb	$0, (%rdi)
	movq	-160(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L2061
	call	_ZdlPv@PLT
.L2061:
	cmpb	$0, -128(%rbp)
	je	.L2045
	movq	-120(%rbp), %rdi
	cmpq	-256(%rbp), %rdi
	je	.L2045
	call	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L2045:
	movq	32(%rbx), %rax
	addq	$32, %rax
	movq	%rax, -256(%rbp)
	movl	40(%rbx), %eax
	movl	%eax, -264(%rbp)
.LEHB198:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rax
	movq	(%rax), %r15
.L2065:
	testq	%r15, %r15
	je	.L2063
	movl	8(%r15), %r9d
	testl	%r9d, %r9d
	jne	.L2063
	movl	$232, %edi
	call	_Znwm@PLT
.LEHE198:
	pushq	%r12
	movl	-264(%rbp), %r8d
	movq	%r15, %rdx
	movq	%r14, %rsi
	pushq	%rbx
	movq	-256(%rbp), %rcx
	movq	%rax, %rdi
	movq	%rax, %r13
	leaq	-192(%rbp), %r9
.LEHB199:
	.cfi_escape 0x2e,0x10
	call	_ZN2v88internal6torque9ClassTypeC1EPKNS1_4TypeEPNS1_9NamespaceERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS_4base5FlagsINS1_9ClassFlagEiEESF_PKNS1_16ClassDeclarationEPKNS1_9TypeAliasE@PLT
.LEHE199:
	popq	%rdi
	popq	%r8
.LEHB200:
	.cfi_escape 0x2e,0
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_10TypeOracleEEERPNT_5ScopeEv@PLT
.LEHE200:
	movq	(%rax), %rdi
	movq	%r13, -232(%rbp)
	movq	168(%rdi), %rsi
	cmpq	176(%rdi), %rsi
	je	.L2066
	movq	$0, -232(%rbp)
	movq	%r13, (%rsi)
	addq	$8, 168(%rdi)
.L2067:
	movq	-232(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2068
	movq	(%rdi), %rax
	call	*8(%rax)
.L2068:
	movq	-192(%rbp), %rdi
	cmpq	-248(%rbp), %rdi
	je	.L2009
	call	_ZdlPv@PLT
	jmp	.L2009
	.p2align 4,,10
	.p2align 3
.L2013:
	testq	%r12, %r12
	jne	.L2177
	movq	%r15, %rax
	jmp	.L2014
	.p2align 4,,10
	.p2align 3
.L2021:
	addq	$32, %r14
	cmpq	%r14, %rax
	jne	.L2024
	jmp	.L2022
	.p2align 4,,10
	.p2align 3
.L2029:
	testb	%al, %al
	je	.L2116
	movq	56(%rbx), %rdi
.LEHB201:
	call	_ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_14TypeExpressionE
.LEHE201:
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2101
	cmpl	$5, 8(%rax)
	movl	$0, %eax
	cmove	%r14, %rax
	movq	%rax, -248(%rbp)
.L2076:
	leaq	-112(%rbp), %r15
	movl	$29795, %esi
	movq	%r13, %rdi
	movb	$0, -106(%rbp)
	movq	%r15, -128(%rbp)
	movl	$1970435155, -112(%rbp)
	movw	%si, -108(%rbp)
	movq	$6, -120(%rbp)
.LEHB202:
	call	_ZN2v88internal6torque12Declarations16LookupGlobalTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
.LEHE202:
	movq	-128(%rbp), %rdi
	movq	%rax, %r13
	cmpq	%r15, %rdi
	je	.L2077
	call	_ZdlPv@PLT
.L2077:
	movq	-248(%rbp), %rax
	testq	%rax, %rax
	je	.L2116
	cmpq	%rax, %r13
	jne	.L2116
	cmpb	$0, 64(%rbx)
	jne	.L2178
	movq	32(%rbx), %rax
	leaq	32(%rax), %r15
	movl	40(%rbx), %eax
	orl	$6, %eax
	movl	%eax, -248(%rbp)
.LEHB203:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rax
	movq	(%rax), %rdx
.L2083:
	testq	%rdx, %rdx
	je	.L2081
	movl	8(%rdx), %ecx
	testl	%ecx, %ecx
	jne	.L2081
	movl	$232, %edi
	movq	%rdx, -256(%rbp)
	call	_Znwm@PLT
.LEHE203:
	pushq	%r12
	movq	%r15, %r9
	movq	%r15, %rcx
	pushq	%rbx
	movl	-248(%rbp), %r8d
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	-256(%rbp), %rdx
	movq	%rax, %r13
.LEHB204:
	.cfi_escape 0x2e,0x10
	call	_ZN2v88internal6torque9ClassTypeC1EPKNS1_4TypeEPNS1_9NamespaceERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS_4base5FlagsINS1_9ClassFlagEiEESF_PKNS1_16ClassDeclarationEPKNS1_9TypeAliasE@PLT
.LEHE204:
	popq	%rax
	popq	%rdx
.LEHB205:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_10TypeOracleEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rdi
	movq	%r13, -232(%rbp)
	movq	168(%rdi), %rsi
	cmpq	176(%rdi), %rsi
	je	.L2179
	movq	$0, -232(%rbp)
	movq	%r13, (%rsi)
	addq	$8, 168(%rdi)
.L2085:
	movq	-232(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2009
	movq	(%rdi), %rax
	call	*8(%rax)
.L2009:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2180
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2169:
	.cfi_restore_state
	leaq	-224(%rbp), %rdi
	leaq	-232(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -224(%rbp)
	movq	%rax, %rdi
	movq	-232(%rbp), %rax
	movq	%rax, -208(%rbp)
.L2012:
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	-232(%rbp), %r12
	movq	-224(%rbp), %rax
	jmp	.L2014
	.p2align 4,,10
	.p2align 3
.L2170:
	leaq	-88(%rbp), %r14
	movdqa	-208(%rbp), %xmm1
	pxor	%xmm0, %xmm0
	movq	-216(%rbp), %rdx
	movb	$0, -208(%rbp)
	movq	$0, -216(%rbp)
	movq	$0, -112(%rbp)
	movq	%r14, -104(%rbp)
	movaps	%xmm1, -144(%rbp)
	movaps	%xmm0, -128(%rbp)
.L2016:
	movdqa	-144(%rbp), %xmm2
	movups	%xmm2, -88(%rbp)
	jmp	.L2018
	.p2align 4,,10
	.p2align 3
.L2043:
	testq	%r15, %r15
	jne	.L2181
	movq	-248(%rbp), %rax
	jmp	.L2044
	.p2align 4,,10
	.p2align 3
.L2027:
	leaq	-232(%rbp), %rdx
	addq	$208, %rdi
	call	_ZNSt6vectorIPKN2v88internal6torque9TypeAliasESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	jmp	.L2028
	.p2align 4,,10
	.p2align 3
.L2081:
	movq	16(%rdx), %rdx
	jmp	.L2083
	.p2align 4,,10
	.p2align 3
.L2037:
	movq	32(%rbx), %rdx
	movq	%r13, %rdi
	leaq	.LC30(%rip), %rcx
	leaq	.LC31(%rip), %rsi
	addq	$32, %rdx
	call	_ZN2v88internal6torqueL7MessageIJRA17_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA37_S3_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
.LEHE205:
	movq	%r13, %rdi
.LEHB206:
	.cfi_escape 0x2e,0
	call	_ZNK2v88internal6torque14MessageBuilder5ThrowEv@PLT
.LEHE206:
	.p2align 4,,10
	.p2align 3
.L2172:
	leaq	-192(%rbp), %rdi
	leaq	-232(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r8, -256(%rbp)
.LEHB207:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE207:
	movq	-256(%rbp), %r8
	movq	%rax, -192(%rbp)
	movq	%rax, %rdi
	movq	-232(%rbp), %rax
	movq	%rax, -176(%rbp)
.L2042:
	movq	%r15, %rdx
	movq	%r8, %rsi
	call	memcpy@PLT
	movq	-232(%rbp), %r15
	movq	-192(%rbp), %rax
	jmp	.L2044
	.p2align 4,,10
	.p2align 3
.L2063:
	movq	16(%r15), %r15
	jmp	.L2065
	.p2align 4,,10
	.p2align 3
.L2173:
	testq	%rax, %rax
	jne	.L2046
	leaq	.LC4(%rip), %rdi
.LEHB208:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE208:
	.p2align 4,,10
	.p2align 3
.L2179:
	leaq	-232(%rbp), %rdx
	addq	$160, %rdi
.LEHB209:
	call	_ZNSt6vectorISt10unique_ptrIN2v88internal6torque13AggregateTypeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
.LEHE209:
	jmp	.L2085
	.p2align 4,,10
	.p2align 3
.L2066:
	leaq	-232(%rbp), %rdx
	addq	$160, %rdi
.LEHB210:
	call	_ZNSt6vectorISt10unique_ptrIN2v88internal6torque13AggregateTypeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
.LEHE210:
	jmp	.L2067
	.p2align 4,,10
	.p2align 3
.L2174:
	leaq	-232(%rbp), %rsi
	leaq	-120(%rbp), %rdi
	xorl	%edx, %edx
.LEHB211:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE211:
	movq	%rax, -120(%rbp)
	movq	%rax, %rdi
	movq	-232(%rbp), %rax
	movq	%rax, -104(%rbp)
.L2048:
	movq	-264(%rbp), %rsi
	movq	%r15, %rdx
	call	memcpy@PLT
	movq	-232(%rbp), %r15
	movq	-120(%rbp), %rax
	jmp	.L2050
.L2039:
	leaq	.LC4(%rip), %rdi
.LEHB212:
	call	_ZSt19__throw_logic_errorPKc@PLT
	.p2align 4,,10
	.p2align 3
.L2175:
	testq	%rdx, %rdx
	je	.L2056
	cmpq	$1, %rdx
	je	.L2182
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	-152(%rbp), %rdx
	movq	-192(%rbp), %rdi
.L2056:
	movq	%rdx, -184(%rbp)
	movb	$0, (%rdi,%rdx)
	movq	-160(%rbp), %rdi
	jmp	.L2058
	.p2align 4,,10
	.p2align 3
.L2101:
	movq	$0, -248(%rbp)
	jmp	.L2076
	.p2align 4,,10
	.p2align 3
.L2176:
	movq	%rdx, %xmm0
	movq	%rcx, %xmm4
	movq	%rax, -192(%rbp)
	punpcklqdq	%xmm4, %xmm0
	movups	%xmm0, -184(%rbp)
.L2060:
	movq	%r13, -160(%rbp)
	leaq	-144(%rbp), %r13
	movq	%r13, %rdi
	jmp	.L2058
	.p2align 4,,10
	.p2align 3
.L2049:
	testq	%r15, %r15
	jne	.L2183
	movq	-256(%rbp), %rax
	jmp	.L2050
.L2182:
	movzbl	-144(%rbp), %eax
	movb	%al, (%rdi)
	movq	-152(%rbp), %rdx
	movq	-192(%rbp), %rdi
	jmp	.L2056
.L2116:
	movq	32(%rbx), %rsi
	leaq	.LC32(%rip), %rdx
	leaq	.LC33(%rip), %rdi
	addq	$32, %rsi
	call	_ZN2v88internal6torque11ReportErrorIJRA14_KcRNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA27_S3_EEEvDpOT_
.L2180:
	call	__stack_chk_fail@PLT
.L2171:
	movq	%r13, %rdi
	leaq	.LC29(%rip), %rsi
	call	_ZN2v88internal6torqueL7MessageIJRA55_KcEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
.LEHE212:
	movq	%r13, %rdi
.LEHB213:
	call	_ZNK2v88internal6torque14MessageBuilder5ThrowEv@PLT
.LEHE213:
.L2178:
	leaq	.LC34(%rip), %rdi
.LEHB214:
	call	_ZN2v88internal6torque11ReportErrorIJRA50_KcEEEvDpOT_
.LEHE214:
.L2181:
	movq	-248(%rbp), %rdi
	jmp	.L2042
.L2183:
	movq	-256(%rbp), %rdi
	jmp	.L2048
.L2177:
	movq	%r15, %rdi
	jmp	.L2012
.L2109:
	endbr64
	movq	%rax, %r12
	jmp	.L2038
.L2110:
	endbr64
	movq	%rax, %r12
	jmp	.L2052
.L2112:
	endbr64
	movq	%rax, %r12
	jmp	.L2071
.L2104:
	endbr64
	movq	%rax, %r12
	jmp	.L2054
.L2105:
	endbr64
	movq	%rax, %r12
	jmp	.L2092
.L2107:
	endbr64
	movq	%rax, %r12
	jmp	.L2038
.L2108:
	endbr64
	movq	%rax, %r12
	jmp	.L2095
.L2103:
	endbr64
	movq	%rax, %r12
	jmp	.L2090
.L2111:
	endbr64
	movq	%rax, %r12
	jmp	.L2072
.L2113:
	endbr64
	movq	%rax, %r12
	jmp	.L2088
.L2114:
	endbr64
	movq	%rax, %r12
	jmp	.L2087
.L2106:
	endbr64
	movq	%rax, %r12
	jmp	.L2095
	.section	.gcc_except_table._ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_16ClassDeclarationE,"a",@progbits
.LLSDA6549:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6549-.LLSDACSB6549
.LLSDACSB6549:
	.uleb128 .LEHB194-.LFB6549
	.uleb128 .LEHE194-.LEHB194
	.uleb128 .L2103-.LFB6549
	.uleb128 0
	.uleb128 .LEHB195-.LFB6549
	.uleb128 .LEHE195-.LEHB195
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB196-.LFB6549
	.uleb128 .LEHE196-.LEHB196
	.uleb128 .L2108-.LFB6549
	.uleb128 0
	.uleb128 .LEHB197-.LFB6549
	.uleb128 .LEHE197-.LEHB197
	.uleb128 .L2105-.LFB6549
	.uleb128 0
	.uleb128 .LEHB198-.LFB6549
	.uleb128 .LEHE198-.LEHB198
	.uleb128 .L2104-.LFB6549
	.uleb128 0
	.uleb128 .LEHB199-.LFB6549
	.uleb128 .LEHE199-.LEHB199
	.uleb128 .L2112-.LFB6549
	.uleb128 0
	.uleb128 .LEHB200-.LFB6549
	.uleb128 .LEHE200-.LEHB200
	.uleb128 .L2104-.LFB6549
	.uleb128 0
	.uleb128 .LEHB201-.LFB6549
	.uleb128 .LEHE201-.LEHB201
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB202-.LFB6549
	.uleb128 .LEHE202-.LEHB202
	.uleb128 .L2106-.LFB6549
	.uleb128 0
	.uleb128 .LEHB203-.LFB6549
	.uleb128 .LEHE203-.LEHB203
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB204-.LFB6549
	.uleb128 .LEHE204-.LEHB204
	.uleb128 .L2114-.LFB6549
	.uleb128 0
	.uleb128 .LEHB205-.LFB6549
	.uleb128 .LEHE205-.LEHB205
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB206-.LFB6549
	.uleb128 .LEHE206-.LEHB206
	.uleb128 .L2109-.LFB6549
	.uleb128 0
	.uleb128 .LEHB207-.LFB6549
	.uleb128 .LEHE207-.LEHB207
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB208-.LFB6549
	.uleb128 .LEHE208-.LEHB208
	.uleb128 .L2110-.LFB6549
	.uleb128 0
	.uleb128 .LEHB209-.LFB6549
	.uleb128 .LEHE209-.LEHB209
	.uleb128 .L2113-.LFB6549
	.uleb128 0
	.uleb128 .LEHB210-.LFB6549
	.uleb128 .LEHE210-.LEHB210
	.uleb128 .L2111-.LFB6549
	.uleb128 0
	.uleb128 .LEHB211-.LFB6549
	.uleb128 .LEHE211-.LEHB211
	.uleb128 .L2110-.LFB6549
	.uleb128 0
	.uleb128 .LEHB212-.LFB6549
	.uleb128 .LEHE212-.LEHB212
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB213-.LFB6549
	.uleb128 .LEHE213-.LEHB213
	.uleb128 .L2107-.LFB6549
	.uleb128 0
	.uleb128 .LEHB214-.LFB6549
	.uleb128 .LEHE214-.LEHB214
	.uleb128 0
	.uleb128 0
.LLSDACSE6549:
	.section	.text._ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_16ClassDeclarationE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_16ClassDeclarationE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6549
	.type	_ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_16ClassDeclarationE.cold, @function
_ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_16ClassDeclarationE.cold:
.LFSB6549:
.L2038:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	%r13, %rdi
	call	_ZN2v88internal6torque14MessageBuilderD1Ev
.L2166:
	movq	%r12, %rdi
.LEHB215:
	call	_Unwind_Resume@PLT
.L2052:
	cmpb	$0, -128(%rbp)
	je	.L2054
	movq	-120(%rbp), %rdi
	cmpq	-256(%rbp), %rdi
	je	.L2054
	call	_ZdlPv@PLT
	jmp	.L2054
.L2071:
	movl	$232, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2054:
	movq	-192(%rbp), %rdi
	cmpq	-248(%rbp), %rdi
	je	.L2166
.L2167:
	call	_ZdlPv@PLT
	jmp	.L2166
.L2092:
	cmpb	$0, -128(%rbp)
	je	.L2054
	movq	-120(%rbp), %rdi
	cmpq	-256(%rbp), %rdi
	je	.L2054
	call	_ZdlPv@PLT
	jmp	.L2054
.L2095:
	movq	-128(%rbp), %rdi
	cmpq	%r15, %rdi
	jne	.L2167
	jmp	.L2166
.L2090:
	movq	%r13, %rdi
	call	_ZN2v88internal6torque13QualifiedNameD1Ev
	movq	-224(%rbp), %rdi
	cmpq	%r15, %rdi
	jne	.L2167
	jmp	.L2166
.L2072:
	movq	-232(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2054
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L2054
.L2088:
	movq	-232(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2166
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L2166
.L2087:
	movq	%r13, %rdi
	movl	$232, %esi
	call	_ZdlPvm@PLT
	movq	%r12, %rdi
	call	_Unwind_Resume@PLT
.LEHE215:
	.cfi_endproc
.LFE6549:
	.section	.gcc_except_table._ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_16ClassDeclarationE
.LLSDAC6549:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6549-.LLSDACSBC6549
.LLSDACSBC6549:
	.uleb128 .LEHB215-.LCOLDB35
	.uleb128 .LEHE215-.LEHB215
	.uleb128 0
	.uleb128 0
.LLSDACSEC6549:
	.section	.text.unlikely._ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_16ClassDeclarationE
	.section	.text._ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_16ClassDeclarationE
	.size	_ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_16ClassDeclarationE, .-_ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_16ClassDeclarationE
	.section	.text.unlikely._ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_16ClassDeclarationE
	.size	_ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_16ClassDeclarationE.cold, .-_ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_16ClassDeclarationE.cold
.LCOLDE35:
	.section	.text._ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_16ClassDeclarationE
.LHOTE35:
	.section	.rodata._ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_15TypeDeclarationE.str1.1,"aMS",@progbits,1
.LC36:
	.string	"unimplemented code"
	.section	.text.unlikely._ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_15TypeDeclarationE,"ax",@progbits
	.align 2
.LCOLDB37:
	.section	.text._ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_15TypeDeclarationE,"ax",@progbits
.LHOTB37:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_15TypeDeclarationE
	.type	_ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_15TypeDeclarationE, @function
_ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_15TypeDeclarationE:
.LFB6522:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6522
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$88, %rsp
	movdqu	12(%rdi), %xmm0
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	28(%rdi), %eax
	movaps	%xmm0, -96(%rbp)
	movl	%eax, -80(%rbp)
.LEHB216:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rax
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
.LEHE216:
	leaq	-96(%rbp), %rdx
	movq	%rdx, (%rax)
	movl	8(%r12), %eax
	cmpl	$38, %eax
	je	.L2185
	jg	.L2186
	cmpl	$36, %eax
	je	.L2187
	cmpl	$37, %eax
	jne	.L2189
	movq	%r12, %rdi
.LEHB217:
	call	_ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_20TypeAliasDeclarationE
.LEHE217:
.L2207:
	movq	%rax, %r12
.L2191:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
	movq	-72(%rbp), %rdx
	movq	%rdx, (%rax)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2208
	addq	$88, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2186:
	.cfi_restore_state
	cmpl	$39, %eax
	jne	.L2189
	leaq	-64(%rbp), %rsi
	movq	%r12, %rdi
	movb	$0, -64(%rbp)
	movb	$0, -56(%rbp)
.LEHB218:
	call	_ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_17StructDeclarationENS_4base8OptionalINS1_17SpecializationKeyINS1_17GenericStructTypeEEEEE
.LEHE218:
	cmpb	$0, -64(%rbp)
	movq	%rax, %r12
	je	.L2191
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2191
	call	_ZdlPv@PLT
	jmp	.L2191
	.p2align 4,,10
	.p2align 3
.L2187:
	movq	%r12, %rdi
.LEHB219:
	call	_ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_23AbstractTypeDeclarationE
	jmp	.L2207
	.p2align 4,,10
	.p2align 3
.L2185:
	movq	%r12, %rdi
	call	_ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_16ClassDeclarationE
	jmp	.L2207
.L2189:
	leaq	.LC36(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.LEHE219:
.L2208:
	call	__stack_chk_fail@PLT
.L2197:
	endbr64
	movq	%rax, %r12
	jmp	.L2195
.L2198:
	endbr64
	movq	%rax, %r12
	jmp	.L2193
	.section	.gcc_except_table._ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_15TypeDeclarationE,"a",@progbits
.LLSDA6522:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6522-.LLSDACSB6522
.LLSDACSB6522:
	.uleb128 .LEHB216-.LFB6522
	.uleb128 .LEHE216-.LEHB216
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB217-.LFB6522
	.uleb128 .LEHE217-.LEHB217
	.uleb128 .L2197-.LFB6522
	.uleb128 0
	.uleb128 .LEHB218-.LFB6522
	.uleb128 .LEHE218-.LEHB218
	.uleb128 .L2198-.LFB6522
	.uleb128 0
	.uleb128 .LEHB219-.LFB6522
	.uleb128 .LEHE219-.LEHB219
	.uleb128 .L2197-.LFB6522
	.uleb128 0
.LLSDACSE6522:
	.section	.text._ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_15TypeDeclarationE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_15TypeDeclarationE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6522
	.type	_ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_15TypeDeclarationE.cold, @function
_ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_15TypeDeclarationE.cold:
.LFSB6522:
.L2193:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	cmpb	$0, -64(%rbp)
	je	.L2195
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2195
	call	_ZdlPv@PLT
.L2195:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
	movq	-72(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rdx, (%rax)
.LEHB220:
	call	_Unwind_Resume@PLT
.LEHE220:
	.cfi_endproc
.LFE6522:
	.section	.gcc_except_table._ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_15TypeDeclarationE
.LLSDAC6522:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6522-.LLSDACSBC6522
.LLSDACSBC6522:
	.uleb128 .LEHB220-.LCOLDB37
	.uleb128 .LEHE220-.LEHB220
	.uleb128 0
	.uleb128 0
.LLSDACSEC6522:
	.section	.text.unlikely._ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_15TypeDeclarationE
	.section	.text._ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_15TypeDeclarationE
	.size	_ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_15TypeDeclarationE, .-_ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_15TypeDeclarationE
	.section	.text.unlikely._ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_15TypeDeclarationE
	.size	_ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_15TypeDeclarationE.cold, .-_ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_15TypeDeclarationE.cold
.LCOLDE37:
	.section	.text._ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_15TypeDeclarationE
.LHOTE37:
	.section	.text.unlikely._ZN2v88internal6torque11TypeVisitor13MakeSignatureEPKNS1_19CallableDeclarationE,"ax",@progbits
	.align 2
.LCOLDB38:
	.section	.text._ZN2v88internal6torque11TypeVisitor13MakeSignatureEPKNS1_19CallableDeclarationE,"ax",@progbits
.LHOTB38:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque11TypeVisitor13MakeSignatureEPKNS1_19CallableDeclarationE
	.type	_ZN2v88internal6torque11TypeVisitor13MakeSignatureEPKNS1_19CallableDeclarationE, @function
_ZN2v88internal6torque11TypeVisitor13MakeSignatureEPKNS1_19CallableDeclarationE:
.LFB6557:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6557
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-168(%rbp), %r15
	leaq	-208(%rbp), %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$280, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -288(%rbp)
	movq	184(%rsi), %rdx
	movq	%rsi, -272(%rbp)
	movq	176(%rsi), %rbx
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	movq	%rdx, -264(%rbp)
	movq	$0, -224(%rbp)
	movaps	%xmm0, -240(%rbp)
	cmpq	%rdx, %rbx
	je	.L2229
	.p2align 4,,10
	.p2align 3
.L2230:
	movq	(%rbx), %rax
	movq	$0, -168(%rbp)
	movq	$0, -160(%rbp)
	movq	%rax, -176(%rbp)
	movq	$0, -152(%rbp)
	movq	8(%rbx), %r12
	movq	16(%rbx), %r13
	cmpq	%r13, %r12
	jne	.L2216
	jmp	.L2213
	.p2align 4,,10
	.p2align 3
.L2372:
	addq	$8, %r12
	movq	%rax, (%rsi)
	addq	$8, -160(%rbp)
	cmpq	%r12, %r13
	je	.L2213
.L2216:
	movq	(%r12), %rdi
.LEHB221:
	call	_ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_14TypeExpressionE
	movq	%rax, -208(%rbp)
	movq	-160(%rbp), %rsi
	cmpq	-152(%rbp), %rsi
	jne	.L2372
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	_ZNSt6vectorIPKN2v88internal6torque4TypeESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
.LEHE221:
	addq	$8, %r12
	cmpq	%r12, %r13
	jne	.L2216
	.p2align 4,,10
	.p2align 3
.L2213:
	movq	-232(%rbp), %r12
	cmpq	-224(%rbp), %r12
	je	.L2220
	movq	-176(%rbp), %rax
	movq	%rax, (%r12)
	movq	-160(%rbp), %r13
	subq	-168(%rbp), %r13
	movq	$0, 8(%r12)
	movq	$0, 16(%r12)
	movq	%r13, %rax
	movq	$0, 24(%r12)
	sarq	$3, %rax
	je	.L2373
	movabsq	$1152921504606846975, %rdx
	cmpq	%rdx, %rax
	leaq	-240(%rbp), %rax
	movq	%rax, -280(%rbp)
	ja	.L2374
	movq	%r13, %rdi
.LEHB222:
	call	_Znwm@PLT
.LEHE222:
	movq	%rax, %rcx
.L2222:
	movq	%rcx, %xmm0
	addq	%rcx, %r13
	punpcklqdq	%xmm0, %xmm0
	movq	%r13, 24(%r12)
	movups	%xmm0, 8(%r12)
	movq	-160(%rbp), %rax
	movq	-168(%rbp), %r13
	movq	%rax, %rdx
	subq	%r13, %rdx
	cmpq	%r13, %rax
	je	.L2225
	movq	%rcx, %rdi
	movq	%r13, %rsi
	movq	%rdx, -280(%rbp)
	call	memmove@PLT
	movq	-280(%rbp), %rdx
	movq	%rax, %rcx
.L2225:
	addq	%rdx, %rcx
	addq	$32, -232(%rbp)
	movq	%rcx, 16(%r12)
.L2226:
	testq	%r13, %r13
	je	.L2227
	movq	%r13, %rdi
	addq	$32, %rbx
	call	_ZdlPv@PLT
	cmpq	%rbx, -264(%rbp)
	jne	.L2230
.L2229:
	movq	-272(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	$0, -112(%rbp)
	movaps	%xmm0, -144(%rbp)
	movzbl	128(%rax), %ebx
	movaps	%xmm0, -128(%rbp)
	testb	%bl, %bl
	jne	.L2211
.L2212:
	movq	-272(%rbp), %rax
	movq	48(%rax), %rsi
	movq	56(%rax), %rax
	movq	%rax, %r12
	subq	%rsi, %r12
	movq	%r12, %rdx
	sarq	$3, %rdx
	je	.L2375
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L2376
	movq	%r12, %rdi
.LEHB223:
	call	_Znwm@PLT
.LEHE223:
	movq	-272(%rbp), %rcx
	movq	%rax, -264(%rbp)
	movzbl	-144(%rbp), %ebx
	movq	56(%rcx), %rax
	movq	48(%rcx), %rsi
	movq	%rax, %r13
	subq	%rsi, %r13
.L2237:
	movq	-264(%rbp), %rdi
	addq	%rdi, %r12
	movq	%r12, -312(%rbp)
	cmpq	%rax, %rsi
	je	.L2239
	movq	%r13, %rdx
	call	memmove@PLT
.L2239:
	addq	-264(%rbp), %r13
	movb	$0, -96(%rbp)
	movq	%r13, -320(%rbp)
	testb	%bl, %bl
	jne	.L2240
	movb	$0, -88(%rbp)
.L2241:
	movq	-272(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	$0, -160(%rbp)
	leaq	-208(%rbp), %r13
	movaps	%xmm0, -176(%rbp)
	movq	80(%rax), %r12
	movq	72(%rax), %rbx
	cmpq	%r12, %rbx
	jne	.L2254
	jmp	.L2248
	.p2align 4,,10
	.p2align 3
.L2377:
	addq	$8, %rbx
	movq	%rax, (%rsi)
	addq	$8, -168(%rbp)
	cmpq	%rbx, %r12
	je	.L2248
.L2254:
	movq	(%rbx), %rdi
.LEHB224:
	call	_ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_14TypeExpressionE
	movq	%rax, -208(%rbp)
	movq	-168(%rbp), %rsi
	cmpq	-160(%rbp), %rsi
	jne	.L2377
	leaq	-176(%rbp), %rdi
	movq	%r13, %rdx
	call	_ZNSt6vectorIPKN2v88internal6torque4TypeESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
.LEHE224:
	addq	$8, %rbx
	cmpq	%rbx, %r12
	jne	.L2254
	.p2align 4,,10
	.p2align 3
.L2248:
	movq	-272(%rbp), %rdx
	movzbl	128(%rdx), %eax
	movb	%al, -152(%rbp)
	movq	%rdx, %rax
	movq	120(%rdx), %rdx
	movq	168(%rax), %rdi
	movq	%rdx, -296(%rbp)
.LEHB225:
	call	_ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_14TypeExpressionE
	movq	-232(%rbp), %r14
	movq	-240(%rbp), %r13
	movq	%rax, -304(%rbp)
	pxor	%xmm0, %xmm0
	movq	$0, -192(%rbp)
	movq	%r14, %rbx
	movaps	%xmm0, -208(%rbp)
	subq	%r13, %rbx
	movq	%rbx, %rax
	sarq	$5, %rax
	je	.L2378
	movabsq	$288230376151711743, %rdx
	cmpq	%rdx, %rax
	ja	.L2379
	movq	%rbx, %rdi
	call	_Znwm@PLT
.LEHE225:
	movq	%rax, -280(%rbp)
	movq	-232(%rbp), %r14
	movq	-240(%rbp), %r13
.L2259:
	movq	-280(%rbp), %rax
	movq	%rax, %xmm0
	addq	%rax, %rbx
	punpcklqdq	%xmm0, %xmm0
	movq	%rbx, -192(%rbp)
	movq	%rax, %rbx
	movaps	%xmm0, -208(%rbp)
	cmpq	%r14, %r13
	je	.L2261
	movabsq	$1152921504606846975, %r15
	jmp	.L2268
	.p2align 4,,10
	.p2align 3
.L2262:
	cmpq	%r15, %rax
	ja	.L2380
	movq	%r12, %rdi
.LEHB226:
	call	_Znwm@PLT
.LEHE226:
	movq	%rax, %rcx
.L2263:
	movq	%rcx, %xmm0
	addq	%rcx, %r12
	punpcklqdq	%xmm0, %xmm0
	movq	%r12, 24(%rbx)
	movups	%xmm0, 8(%rbx)
	movq	16(%r13), %rax
	movq	8(%r13), %rsi
	movq	%rax, %r12
	subq	%rsi, %r12
	cmpq	%rsi, %rax
	je	.L2368
	movq	%rcx, %rdi
	movq	%r12, %rdx
	call	memmove@PLT
	movq	%rax, %rcx
.L2368:
	addq	%r12, %rcx
	addq	$32, %r13
	addq	$32, %rbx
	movq	%rcx, -16(%rbx)
	cmpq	%r13, %r14
	je	.L2261
.L2268:
	movq	0(%r13), %rax
	movq	%rax, (%rbx)
	movq	16(%r13), %r12
	subq	8(%r13), %r12
	movq	$0, 8(%rbx)
	movq	%r12, %rax
	movq	$0, 16(%rbx)
	sarq	$3, %rax
	movq	$0, 24(%rbx)
	jne	.L2262
	xorl	%ecx, %ecx
	jmp	.L2263
	.p2align 4,,10
	.p2align 3
.L2227:
	addq	$32, %rbx
	cmpq	%rbx, -264(%rbp)
	jne	.L2230
	jmp	.L2229
	.p2align 4,,10
	.p2align 3
.L2220:
	leaq	-240(%rbp), %rax
	leaq	-176(%rbp), %rdx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -280(%rbp)
.LEHB227:
	call	_ZNSt6vectorIN2v88internal6torque16LabelDeclarationESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
.LEHE227:
	movq	-168(%rbp), %r13
	jmp	.L2226
	.p2align 4,,10
	.p2align 3
.L2373:
	xorl	%ecx, %ecx
	jmp	.L2222
	.p2align 4,,10
	.p2align 3
.L2261:
	movq	-272(%rbp), %rax
	movq	-312(%rbp), %rdx
	movq	%rbx, -200(%rbp)
	movq	-264(%rbp), %xmm0
	movzbl	32(%rax), %r13d
	movq	-288(%rbp), %rax
	movhps	-320(%rbp), %xmm0
	movq	%rdx, 16(%rax)
	movb	$0, 24(%rax)
	movb	$0, 32(%rax)
	movups	%xmm0, (%rax)
	movzbl	-96(%rbp), %eax
	testb	%al, %al
	jne	.L2381
.L2270:
	movq	-288(%rbp), %rdi
	movq	-160(%rbp), %rdx
	movdqa	-176(%rbp), %xmm1
	movq	-304(%rbp), %rcx
	movq	%rdx, 80(%rdi)
	movzbl	-152(%rbp), %edx
	movq	%rcx, 104(%rdi)
	movb	%dl, 88(%rdi)
	movq	-296(%rbp), %rdx
	movq	%rbx, 120(%rdi)
	movq	%rdx, 96(%rdi)
	movq	-208(%rbp), %rdx
	movb	%r13b, 136(%rdi)
	movq	%rdx, 112(%rdi)
	movq	-192(%rbp), %rdx
	movups	%xmm1, 64(%rdi)
	movq	%rdx, 128(%rdi)
	testb	%al, %al
	je	.L2284
	movq	-88(%rbp), %rdi
	leaq	-72(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2284
	call	_ZdlPv@PLT
.L2284:
	cmpb	$0, -144(%rbp)
	jne	.L2382
.L2288:
	movq	-232(%rbp), %rbx
	movq	-240(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L2289
	.p2align 4,,10
	.p2align 3
.L2293:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2290
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L2293
.L2291:
	movq	-240(%rbp), %r12
.L2289:
	testq	%r12, %r12
	je	.L2209
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L2209:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2383
	movq	-288(%rbp), %rax
	addq	$280, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2290:
	.cfi_restore_state
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L2293
	jmp	.L2291
	.p2align 4,,10
	.p2align 3
.L2211:
	movq	%rax, %rdx
	leaq	-120(%rbp), %rax
	movq	136(%rdx), %r13
	movq	144(%rdx), %r12
	movq	%rax, -136(%rbp)
	movq	%r13, %rcx
	addq	%r12, %rcx
	je	.L2231
	testq	%r13, %r13
	je	.L2384
.L2231:
	movq	%r12, -176(%rbp)
	cmpq	$15, %r12
	ja	.L2385
	cmpq	$1, %r12
	jne	.L2234
	movzbl	0(%r13), %edx
	movb	%dl, -120(%rbp)
.L2235:
	movq	%r12, -128(%rbp)
	movb	$0, (%rax,%r12)
	movb	$1, -144(%rbp)
	jmp	.L2212
	.p2align 4,,10
	.p2align 3
.L2382:
	movq	-136(%rbp), %rdi
	leaq	-120(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2288
	call	_ZdlPv@PLT
	jmp	.L2288
	.p2align 4,,10
	.p2align 3
.L2240:
	movq	-136(%rbp), %r13
	movq	-128(%rbp), %r12
	leaq	-72(%rbp), %rbx
	movq	%rbx, -88(%rbp)
	movq	%r13, %rax
	addq	%r12, %rax
	je	.L2242
	testq	%r13, %r13
	je	.L2386
.L2242:
	movq	%r12, -176(%rbp)
	cmpq	$15, %r12
	ja	.L2387
	cmpq	$1, %r12
	jne	.L2245
	movzbl	0(%r13), %eax
	movb	%al, -72(%rbp)
.L2246:
	movq	%r12, -80(%rbp)
	movb	$0, (%rbx,%r12)
	movb	$1, -96(%rbp)
	jmp	.L2241
	.p2align 4,,10
	.p2align 3
.L2381:
	movq	-288(%rbp), %rax
	movq	-88(%rbp), %r14
	movq	-80(%rbp), %r12
	leaq	48(%rax), %rbx
	movq	%rbx, 32(%rax)
	movq	%r14, %rax
	addq	%r12, %rax
	je	.L2278
	testq	%r14, %r14
	je	.L2388
.L2278:
	movq	%r12, -248(%rbp)
	cmpq	$15, %r12
	ja	.L2389
	cmpq	$1, %r12
	jne	.L2281
	movzbl	(%r14), %eax
	movq	-288(%rbp), %rcx
	movb	%al, 48(%rcx)
.L2282:
	movq	-288(%rbp), %rax
	movq	%r12, 40(%rax)
	movb	$0, (%rbx,%r12)
	movq	-200(%rbp), %rbx
	movb	$1, 24(%rax)
	movzbl	-96(%rbp), %eax
	jmp	.L2270
	.p2align 4,,10
	.p2align 3
.L2375:
	movq	$0, -264(%rbp)
	movq	%r12, %r13
	jmp	.L2237
	.p2align 4,,10
	.p2align 3
.L2378:
	movq	$0, -280(%rbp)
	jmp	.L2259
	.p2align 4,,10
	.p2align 3
.L2245:
	testq	%r12, %r12
	je	.L2246
	movq	%rbx, %rdi
	jmp	.L2244
	.p2align 4,,10
	.p2align 3
.L2281:
	testq	%r12, %r12
	je	.L2282
	movq	%rbx, %rdi
	jmp	.L2280
	.p2align 4,,10
	.p2align 3
.L2385:
	leaq	-176(%rbp), %rsi
	leaq	-136(%rbp), %rdi
	xorl	%edx, %edx
.LEHB228:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE228:
	movq	%rax, -136(%rbp)
	movq	%rax, %rdi
	movq	-176(%rbp), %rax
	movq	%rax, -120(%rbp)
.L2233:
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	-176(%rbp), %r12
	movq	-136(%rbp), %rax
	jmp	.L2235
	.p2align 4,,10
	.p2align 3
.L2387:
	leaq	-176(%rbp), %rsi
	leaq	-88(%rbp), %rdi
	xorl	%edx, %edx
.LEHB229:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE229:
	movq	%rax, -88(%rbp)
	movq	%rax, %rdi
	movq	-176(%rbp), %rax
	movq	%rax, -72(%rbp)
.L2244:
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	-176(%rbp), %r12
	movq	-88(%rbp), %rbx
	jmp	.L2246
	.p2align 4,,10
	.p2align 3
.L2389:
	movq	-288(%rbp), %r15
	leaq	-248(%rbp), %rsi
	xorl	%edx, %edx
	leaq	32(%r15), %rdi
.LEHB230:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE230:
	movq	%rax, 32(%r15)
	movq	%rax, %rdi
	movq	-248(%rbp), %rax
	movq	%rax, 48(%r15)
.L2280:
	movq	%r12, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-288(%rbp), %rax
	movq	-248(%rbp), %r12
	movq	32(%rax), %rbx
	jmp	.L2282
.L2234:
	testq	%r12, %r12
	je	.L2235
	movq	%rax, %rdi
	jmp	.L2233
	.p2align 4,,10
	.p2align 3
.L2380:
.LEHB231:
	call	_ZSt17__throw_bad_allocv@PLT
.LEHE231:
.L2374:
.LEHB232:
	call	_ZSt17__throw_bad_allocv@PLT
.LEHE232:
.L2379:
.LEHB233:
	call	_ZSt17__throw_bad_allocv@PLT
.LEHE233:
.L2384:
	leaq	.LC4(%rip), %rdi
.LEHB234:
	call	_ZSt19__throw_logic_errorPKc@PLT
.L2383:
	call	__stack_chk_fail@PLT
.L2376:
	call	_ZSt17__throw_bad_allocv@PLT
.LEHE234:
.L2386:
	leaq	.LC4(%rip), %rdi
.LEHB235:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE235:
.L2388:
	leaq	.LC4(%rip), %rdi
.LEHB236:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE236:
.L2310:
	endbr64
	movq	%rax, %r12
	jmp	.L2249
.L2307:
	endbr64
	movq	%rax, %r12
	jmp	.L2300
.L2314:
	endbr64
	movq	%rax, %r12
	jmp	.L2285
.L2308:
	endbr64
	movq	%rax, %rbx
	jmp	.L2277
.L2309:
	endbr64
	movq	%rax, %r12
	jmp	.L2217
.L2313:
	endbr64
	movq	%rax, %rdi
	jmp	.L2271
.L2311:
	endbr64
	movq	%rax, %rbx
	jmp	.L2277
.L2306:
	endbr64
	movq	%rax, %r12
	jmp	.L2295
	.section	.gcc_except_table._ZN2v88internal6torque11TypeVisitor13MakeSignatureEPKNS1_19CallableDeclarationE,"a",@progbits
	.align 4
.LLSDA6557:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATT6557-.LLSDATTD6557
.LLSDATTD6557:
	.byte	0x1
	.uleb128 .LLSDACSE6557-.LLSDACSB6557
.LLSDACSB6557:
	.uleb128 .LEHB221-.LFB6557
	.uleb128 .LEHE221-.LEHB221
	.uleb128 .L2309-.LFB6557
	.uleb128 0
	.uleb128 .LEHB222-.LFB6557
	.uleb128 .LEHE222-.LEHB222
	.uleb128 .L2306-.LFB6557
	.uleb128 0
	.uleb128 .LEHB223-.LFB6557
	.uleb128 .LEHE223-.LEHB223
	.uleb128 .L2307-.LFB6557
	.uleb128 0
	.uleb128 .LEHB224-.LFB6557
	.uleb128 .LEHE224-.LEHB224
	.uleb128 .L2311-.LFB6557
	.uleb128 0
	.uleb128 .LEHB225-.LFB6557
	.uleb128 .LEHE225-.LEHB225
	.uleb128 .L2308-.LFB6557
	.uleb128 0
	.uleb128 .LEHB226-.LFB6557
	.uleb128 .LEHE226-.LEHB226
	.uleb128 .L2313-.LFB6557
	.uleb128 0x1
	.uleb128 .LEHB227-.LFB6557
	.uleb128 .LEHE227-.LEHB227
	.uleb128 .L2306-.LFB6557
	.uleb128 0
	.uleb128 .LEHB228-.LFB6557
	.uleb128 .LEHE228-.LEHB228
	.uleb128 .L2307-.LFB6557
	.uleb128 0
	.uleb128 .LEHB229-.LFB6557
	.uleb128 .LEHE229-.LEHB229
	.uleb128 .L2310-.LFB6557
	.uleb128 0
	.uleb128 .LEHB230-.LFB6557
	.uleb128 .LEHE230-.LEHB230
	.uleb128 .L2314-.LFB6557
	.uleb128 0
	.uleb128 .LEHB231-.LFB6557
	.uleb128 .LEHE231-.LEHB231
	.uleb128 .L2313-.LFB6557
	.uleb128 0x1
	.uleb128 .LEHB232-.LFB6557
	.uleb128 .LEHE232-.LEHB232
	.uleb128 .L2306-.LFB6557
	.uleb128 0
	.uleb128 .LEHB233-.LFB6557
	.uleb128 .LEHE233-.LEHB233
	.uleb128 .L2308-.LFB6557
	.uleb128 0
	.uleb128 .LEHB234-.LFB6557
	.uleb128 .LEHE234-.LEHB234
	.uleb128 .L2307-.LFB6557
	.uleb128 0
	.uleb128 .LEHB235-.LFB6557
	.uleb128 .LEHE235-.LEHB235
	.uleb128 .L2310-.LFB6557
	.uleb128 0
	.uleb128 .LEHB236-.LFB6557
	.uleb128 .LEHE236-.LEHB236
	.uleb128 .L2314-.LFB6557
	.uleb128 0
.LLSDACSE6557:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATT6557:
	.section	.text._ZN2v88internal6torque11TypeVisitor13MakeSignatureEPKNS1_19CallableDeclarationE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque11TypeVisitor13MakeSignatureEPKNS1_19CallableDeclarationE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6557
	.type	_ZN2v88internal6torque11TypeVisitor13MakeSignatureEPKNS1_19CallableDeclarationE.cold, @function
_ZN2v88internal6torque11TypeVisitor13MakeSignatureEPKNS1_19CallableDeclarationE.cold:
.LFSB6557:
.L2249:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	cmpb	$0, -96(%rbp)
	je	.L2251
	movq	-88(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L2251
	call	_ZdlPv@PLT
.L2251:
	cmpq	$0, -264(%rbp)
	je	.L2300
	movq	-264(%rbp), %rdi
	call	_ZdlPv@PLT
.L2300:
	cmpb	$0, -144(%rbp)
	je	.L2301
	movq	-136(%rbp), %rdi
	leaq	-120(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2301
.L2371:
	call	_ZdlPv@PLT
.L2301:
	leaq	-240(%rbp), %rax
	movq	%rax, -280(%rbp)
.L2219:
	movq	-280(%rbp), %rdi
	call	_ZNSt6vectorIN2v88internal6torque16LabelDeclarationESaIS3_EED1Ev
	movq	%r12, %rdi
.LEHB237:
	call	_Unwind_Resume@PLT
.LEHE237:
.L2285:
	movq	-288(%rbp), %rax
	cmpb	$0, 24(%rax)
	je	.L2286
	movq	32(%rax), %rdi
	cmpq	%rdi, %rbx
	je	.L2286
	call	_ZdlPv@PLT
.L2286:
	movq	-288(%rbp), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L2287
	call	_ZdlPv@PLT
.L2287:
	leaq	-208(%rbp), %rdi
	movq	%r12, %rbx
	call	_ZNSt6vectorIN2v88internal6torque16LabelDeclarationESaIS3_EED1Ev
	movq	$0, -264(%rbp)
.L2277:
	movq	-176(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2257
	call	_ZdlPv@PLT
.L2257:
	cmpb	$0, -96(%rbp)
	je	.L2298
	movq	-88(%rbp), %rdi
	leaq	-72(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2298
	call	_ZdlPv@PLT
.L2298:
	movq	%rbx, %r12
	jmp	.L2251
.L2217:
	movq	-168(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L2371
	jmp	.L2301
.L2271:
	call	__cxa_begin_catch@PLT
	movq	-280(%rbp), %r12
.L2274:
	cmpq	%r12, %rbx
	jne	.L2390
.LEHB238:
	call	__cxa_rethrow@PLT
.LEHE238:
.L2390:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2273
	call	_ZdlPv@PLT
.L2273:
	addq	$32, %r12
	jmp	.L2274
.L2312:
	endbr64
	movq	%rax, %rbx
	call	__cxa_end_catch@PLT
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2277
	call	_ZdlPv@PLT
	jmp	.L2277
.L2295:
	movq	-168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2219
	call	_ZdlPv@PLT
	jmp	.L2219
	.cfi_endproc
.LFE6557:
	.section	.gcc_except_table._ZN2v88internal6torque11TypeVisitor13MakeSignatureEPKNS1_19CallableDeclarationE
	.align 4
.LLSDAC6557:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATTC6557-.LLSDATTDC6557
.LLSDATTDC6557:
	.byte	0x1
	.uleb128 .LLSDACSEC6557-.LLSDACSBC6557
.LLSDACSBC6557:
	.uleb128 .LEHB237-.LCOLDB38
	.uleb128 .LEHE237-.LEHB237
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB238-.LCOLDB38
	.uleb128 .LEHE238-.LEHB238
	.uleb128 .L2312-.LCOLDB38
	.uleb128 0
.LLSDACSEC6557:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATTC6557:
	.section	.text.unlikely._ZN2v88internal6torque11TypeVisitor13MakeSignatureEPKNS1_19CallableDeclarationE
	.section	.text._ZN2v88internal6torque11TypeVisitor13MakeSignatureEPKNS1_19CallableDeclarationE
	.size	_ZN2v88internal6torque11TypeVisitor13MakeSignatureEPKNS1_19CallableDeclarationE, .-_ZN2v88internal6torque11TypeVisitor13MakeSignatureEPKNS1_19CallableDeclarationE
	.section	.text.unlikely._ZN2v88internal6torque11TypeVisitor13MakeSignatureEPKNS1_19CallableDeclarationE
	.size	_ZN2v88internal6torque11TypeVisitor13MakeSignatureEPKNS1_19CallableDeclarationE.cold, .-_ZN2v88internal6torque11TypeVisitor13MakeSignatureEPKNS1_19CallableDeclarationE.cold
.LCOLDE38:
	.section	.text._ZN2v88internal6torque11TypeVisitor13MakeSignatureEPKNS1_19CallableDeclarationE
.LHOTE38:
	.section	.text.unlikely._ZN2v88internal6torque14DeclareMethodsEPNS1_13AggregateTypeERKSt6vectorIPNS1_11DeclarationESaIS6_EE,"ax",@progbits
.LCOLDB39:
	.section	.text._ZN2v88internal6torque14DeclareMethodsEPNS1_13AggregateTypeERKSt6vectorIPNS1_11DeclarationESaIS6_EE,"ax",@progbits
.LHOTB39:
	.p2align 4
	.globl	_ZN2v88internal6torque14DeclareMethodsEPNS1_13AggregateTypeERKSt6vectorIPNS1_11DeclarationESaIS6_EE
	.type	_ZN2v88internal6torque14DeclareMethodsEPNS1_13AggregateTypeERKSt6vectorIPNS1_11DeclarationESaIS6_EE, @function
_ZN2v88internal6torque14DeclareMethodsEPNS1_13AggregateTypeERKSt6vectorIPNS1_11DeclarationESaIS6_EE:
.LFB6534:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6534
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$424, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -416(%rbp)
	movq	(%rsi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8(%rsi), %rax
	leaq	-384(%rbp), %rsi
	movq	%rdx, -408(%rbp)
	movq	%rsi, -448(%rbp)
	leaq	-352(%rbp), %rsi
	movq	%rax, -456(%rbp)
	movq	%rsi, -440(%rbp)
	cmpq	%rax, %rdx
	je	.L2391
	.p2align 4,,10
	.p2align 3
.L2475:
	movq	-408(%rbp), %rax
	movq	(%rax), %r12
	movdqu	12(%r12), %xmm1
	movl	28(%r12), %eax
	movaps	%xmm1, -384(%rbp)
	movl	%eax, -368(%rbp)
.LEHB239:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rax
	movq	%rax, -360(%rbp)
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
.LEHE239:
	movq	-448(%rbp), %rdx
	movq	-440(%rbp), %rdi
	movq	%rdx, (%rax)
	movl	$0, %eax
	cmpl	$46, 8(%r12)
	cmovne	%rax, %r12
	movq	%r12, %rsi
.LEHB240:
	call	_ZN2v88internal6torque11TypeVisitor13MakeSignatureEPKNS1_19CallableDeclarationE
.LEHE240:
.LEHB241:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_10CurrentAstEEERPNT_5ScopeEv@PLT
	movq	(%rax), %r13
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
.LEHE241:
	movq	(%rax), %rax
	leaq	-192(%rbp), %r14
	movl	$64, %edi
	movq	4(%rax), %rcx
	movl	(%rax), %r15d
	movq	12(%rax), %rax
	movq	%r14, -208(%rbp)
	movq	%rcx, -424(%rbp)
	movq	%rax, -432(%rbp)
	movl	$1936287860, -192(%rbp)
	movq	$4, -200(%rbp)
	movb	$0, -188(%rbp)
.LEHB242:
	call	_Znwm@PLT
.LEHE242:
	movq	-424(%rbp), %rcx
	movq	%rax, %rbx
	movl	$52, 8(%rax)
	movl	%r15d, 12(%rax)
	movq	%rcx, 16(%rax)
	movq	-432(%rbp), %rax
	movq	%rax, 24(%rbx)
	leaq	16+_ZTVN2v88internal6torque10IdentifierE(%rip), %rax
	movq	%rax, (%rbx)
	leaq	48(%rbx), %rax
	movq	%rax, 32(%rbx)
	movq	-208(%rbp), %rax
	cmpq	%r14, %rax
	je	.L2536
	movq	%rax, 32(%rbx)
	movq	-192(%rbp), %rax
	movq	%rax, 48(%rbx)
.L2395:
	movq	-200(%rbp), %rax
	movq	%r14, -208(%rbp)
	movq	$0, -200(%rbp)
	movq	%rax, 40(%rbx)
	movb	$0, -192(%rbp)
	movq	%rbx, -392(%rbp)
	movq	32(%r13), %rsi
	cmpq	40(%r13), %rsi
	je	.L2396
	movq	$0, -392(%rbp)
	movq	%rbx, (%rsi)
	addq	$8, 32(%r13)
.L2397:
	movq	-392(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2398
	movq	(%rdi), %rax
	call	*8(%rax)
.L2398:
	movq	-208(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L2400
	call	_ZdlPv@PLT
.L2400:
	movq	-352(%rbp), %rax
	movq	-256(%rbp), %rdx
	movq	%rbx, -392(%rbp)
	leaq	(%rax,%rdx,8), %r13
	movq	-344(%rbp), %rax
	cmpq	-336(%rbp), %rax
	je	.L2537
	cmpq	%rax, %r13
	je	.L2538
	movq	-8(%rax), %rdx
	movq	%rdx, (%rax)
	movq	-344(%rbp), %rdi
	leaq	8(%rdi), %rax
	leaq	-8(%rdi), %rdx
	movq	%rax, -344(%rbp)
	cmpq	%rdx, %r13
	je	.L2410
	subq	%r13, %rdx
	movq	%r13, %rsi
	subq	%rdx, %rdi
	call	memmove@PLT
.L2410:
	movq	-392(%rbp), %rax
	movq	%rax, 0(%r13)
.L2409:
	movq	40(%r12), %rax
	movq	-416(%rbp), %rsi
	movq	-256(%rbp), %rdx
	movq	256(%r12), %r15
	addq	$32, %rax
	movq	%rsi, -392(%rbp)
	movq	%rax, -432(%rbp)
	movq	-288(%rbp), %rax
	leaq	(%rax,%rdx,8), %r12
	movq	-280(%rbp), %rax
	cmpq	-272(%rbp), %rax
	je	.L2411
	cmpq	%rax, %r12
	je	.L2539
	movq	-8(%rax), %rdx
	movq	%rdx, (%rax)
	movq	-280(%rbp), %rdi
	leaq	8(%rdi), %rax
	leaq	-8(%rdi), %rdx
	movq	%rax, -280(%rbp)
	cmpq	%rdx, %r12
	je	.L2414
	subq	%r12, %rdx
	movq	%r12, %rsi
	subq	%rdx, %rdi
	call	memmove@PLT
.L2414:
	movq	-392(%rbp), %rax
	movq	%rax, (%r12)
.L2413:
	movq	-344(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	-352(%rbp), %rsi
	movq	$0, -192(%rbp)
	movaps	%xmm0, -208(%rbp)
	movq	%rax, %rbx
	subq	%rsi, %rbx
	movq	%rbx, %rdx
	sarq	$3, %rdx
	je	.L2540
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L2541
	movq	%rbx, %rdi
.LEHB243:
	call	_Znwm@PLT
.LEHE243:
	movq	%rax, %rcx
	movq	-344(%rbp), %rax
	movq	-352(%rbp), %rsi
	movq	%rax, %r12
	subq	%rsi, %r12
.L2416:
	movq	%rcx, %xmm0
	addq	%rcx, %rbx
	punpcklqdq	%xmm0, %xmm0
	movq	%rbx, -192(%rbp)
	movaps	%xmm0, -208(%rbp)
	cmpq	%rsi, %rax
	je	.L2418
	movq	%rcx, %rdi
	movq	%r12, %rdx
	call	memmove@PLT
	movq	%rax, %rcx
.L2418:
	addq	%r12, %rcx
	cmpb	$0, -328(%rbp)
	movb	$0, -184(%rbp)
	movq	%rcx, -200(%rbp)
	movb	$0, -176(%rbp)
	jne	.L2542
.L2419:
	movq	-280(%rbp), %rax
	movq	-288(%rbp), %rsi
	pxor	%xmm0, %xmm0
	movq	$0, -128(%rbp)
	movaps	%xmm0, -144(%rbp)
	movq	%rax, %rbx
	subq	%rsi, %rbx
	movq	%rbx, %rdx
	sarq	$3, %rdx
	je	.L2543
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L2544
	movq	%rbx, %rdi
.LEHB244:
	call	_Znwm@PLT
.LEHE244:
	movq	%rax, %rcx
	movq	-280(%rbp), %rax
	movq	-288(%rbp), %rsi
	movq	%rax, %r12
	subq	%rsi, %r12
.L2426:
	movq	%rcx, %xmm0
	addq	%rcx, %rbx
	punpcklqdq	%xmm0, %xmm0
	movq	%rbx, -128(%rbp)
	movaps	%xmm0, -144(%rbp)
	cmpq	%rsi, %rax
	je	.L2431
	movq	%rcx, %rdi
	movq	%r12, %rdx
	call	memmove@PLT
	movq	%rax, %rcx
.L2431:
	movzbl	-264(%rbp), %eax
	movq	-232(%rbp), %r14
	addq	%r12, %rcx
	movq	$0, -96(%rbp)
	movq	-240(%rbp), %rbx
	movq	%rcx, -136(%rbp)
	movb	%al, -120(%rbp)
	movq	-256(%rbp), %rax
	movq	%r14, %r12
	subq	%rbx, %r12
	movq	$0, -88(%rbp)
	movq	%rax, -112(%rbp)
	movq	-248(%rbp), %rax
	movq	$0, -80(%rbp)
	movq	%rax, -104(%rbp)
	movq	%r12, %rax
	sarq	$5, %rax
	je	.L2545
	movabsq	$288230376151711743, %rsi
	cmpq	%rsi, %rax
	ja	.L2546
	movq	%r12, %rdi
.LEHB245:
	call	_Znwm@PLT
.LEHE245:
	movq	%rax, -424(%rbp)
	movq	-232(%rbp), %r14
	movq	-240(%rbp), %rbx
.L2433:
	movq	-424(%rbp), %rax
	movq	%rax, %xmm0
	addq	%rax, %r12
	punpcklqdq	%xmm0, %xmm0
	movq	%r12, -80(%rbp)
	movq	%rax, %r12
	movaps	%xmm0, -96(%rbp)
	cmpq	%rbx, %r14
	jne	.L2442
	jmp	.L2435
	.p2align 4,,10
	.p2align 3
.L2436:
	movabsq	$1152921504606846975, %rdx
	cmpq	%rdx, %rax
	ja	.L2547
	movq	%r13, %rdi
.LEHB246:
	call	_Znwm@PLT
.LEHE246:
	movq	%rax, %rcx
.L2437:
	movq	%rcx, %xmm0
	addq	%rcx, %r13
	punpcklqdq	%xmm0, %xmm0
	movq	%r13, 24(%r12)
	movups	%xmm0, 8(%r12)
	movq	16(%rbx), %rax
	movq	8(%rbx), %rsi
	movq	%rax, %r13
	subq	%rsi, %r13
	cmpq	%rsi, %rax
	je	.L2534
	movq	%rcx, %rdi
	movq	%r13, %rdx
	call	memmove@PLT
	movq	%rax, %rcx
.L2534:
	addq	%r13, %rcx
	addq	$32, %rbx
	addq	$32, %r12
	movq	%rcx, -16(%r12)
	cmpq	%rbx, %r14
	je	.L2435
.L2442:
	movq	(%rbx), %rax
	movq	%rax, (%r12)
	movq	16(%rbx), %r13
	subq	8(%rbx), %r13
	movq	$0, 8(%r12)
	movq	$0, 16(%r12)
	movq	%r13, %rax
	movq	$0, 24(%r12)
	sarq	$3, %rax
	jne	.L2436
	xorl	%ecx, %ecx
	jmp	.L2437
	.p2align 4,,10
	.p2align 3
.L2435:
	movq	%r12, -88(%rbp)
	movzbl	-216(%rbp), %eax
	leaq	-208(%rbp), %r12
	movq	%r15, %rcx
	movq	-432(%rbp), %rsi
	movq	-416(%rbp), %rdi
	movq	%r12, %rdx
	movb	%al, -72(%rbp)
.LEHB247:
	call	_ZN2v88internal6torque12Declarations12CreateMethodEPNS1_13AggregateTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_9SignatureEPNS1_9StatementE@PLT
.LEHE247:
	movq	-88(%rbp), %rbx
	movq	-96(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L2455
	.p2align 4,,10
	.p2align 3
.L2459:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2456
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L2459
.L2457:
	movq	-96(%rbp), %r12
.L2455:
	testq	%r12, %r12
	je	.L2460
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L2460:
	movq	-144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2461
	call	_ZdlPv@PLT
.L2461:
	cmpb	$0, -184(%rbp)
	je	.L2462
	movq	-176(%rbp), %rdi
	leaq	-160(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2462
	call	_ZdlPv@PLT
.L2462:
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2463
	call	_ZdlPv@PLT
.L2463:
	movq	-232(%rbp), %rbx
	movq	-240(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L2464
	.p2align 4,,10
	.p2align 3
.L2468:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2465
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%rbx, %r12
	jne	.L2468
.L2466:
	movq	-240(%rbp), %r12
.L2464:
	testq	%r12, %r12
	je	.L2469
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L2469:
	movq	-288(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2470
	call	_ZdlPv@PLT
.L2470:
	cmpb	$0, -328(%rbp)
	je	.L2471
	movq	-320(%rbp), %rdi
	leaq	-304(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2471
	call	_ZdlPv@PLT
.L2471:
	movq	-352(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2472
	call	_ZdlPv@PLT
.L2472:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
	movq	-360(%rbp), %rdx
	movq	%rdx, (%rax)
	addq	$8, -408(%rbp)
	movq	-408(%rbp), %rax
	cmpq	%rax, -456(%rbp)
	jne	.L2475
.L2391:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2548
	addq	$424, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2465:
	.cfi_restore_state
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L2468
	jmp	.L2466
	.p2align 4,,10
	.p2align 3
.L2456:
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L2459
	jmp	.L2457
	.p2align 4,,10
	.p2align 3
.L2539:
	movq	%rsi, (%r12)
	addq	$8, -280(%rbp)
	jmp	.L2413
	.p2align 4,,10
	.p2align 3
.L2538:
	movq	%rbx, 0(%r13)
	addq	$8, -344(%rbp)
	jmp	.L2409
	.p2align 4,,10
	.p2align 3
.L2542:
	movq	-320(%rbp), %r13
	movq	-312(%rbp), %r12
	leaq	-160(%rbp), %rbx
	movq	%rbx, -176(%rbp)
	movq	%r13, %rax
	addq	%r12, %rax
	je	.L2420
	testq	%r13, %r13
	je	.L2549
.L2420:
	movq	%r12, -392(%rbp)
	cmpq	$15, %r12
	ja	.L2550
	cmpq	$1, %r12
	jne	.L2423
	movzbl	0(%r13), %eax
	movb	%al, -160(%rbp)
.L2424:
	movq	%r12, -168(%rbp)
	movb	$0, (%rbx,%r12)
	movb	$1, -184(%rbp)
	jmp	.L2419
	.p2align 4,,10
	.p2align 3
.L2536:
	movdqa	-192(%rbp), %xmm2
	movups	%xmm2, 48(%rbx)
	jmp	.L2395
	.p2align 4,,10
	.p2align 3
.L2540:
	movq	%rbx, %r12
	xorl	%ecx, %ecx
	jmp	.L2416
	.p2align 4,,10
	.p2align 3
.L2545:
	movq	$0, -424(%rbp)
	jmp	.L2433
	.p2align 4,,10
	.p2align 3
.L2543:
	movq	%rbx, %r12
	xorl	%ecx, %ecx
	jmp	.L2426
	.p2align 4,,10
	.p2align 3
.L2411:
	leaq	-392(%rbp), %rdx
	leaq	-288(%rbp), %rdi
	movq	%r12, %rsi
.LEHB248:
	call	_ZNSt6vectorIPKN2v88internal6torque4TypeESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	jmp	.L2413
	.p2align 4,,10
	.p2align 3
.L2537:
	movq	-440(%rbp), %rdi
	leaq	-392(%rbp), %rdx
	movq	%r13, %rsi
	call	_ZNSt6vectorIPN2v88internal6torque10IdentifierESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
.LEHE248:
	jmp	.L2409
	.p2align 4,,10
	.p2align 3
.L2396:
	leaq	-392(%rbp), %rdx
	leaq	24(%r13), %rdi
.LEHB249:
	call	_ZNSt6vectorISt10unique_ptrIN2v88internal6torque7AstNodeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
.LEHE249:
	jmp	.L2397
	.p2align 4,,10
	.p2align 3
.L2423:
	testq	%r12, %r12
	je	.L2424
	movq	%rbx, %rdi
	jmp	.L2422
	.p2align 4,,10
	.p2align 3
.L2550:
	leaq	-392(%rbp), %rsi
	leaq	-176(%rbp), %rdi
	xorl	%edx, %edx
.LEHB250:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE250:
	movq	%rax, -176(%rbp)
	movq	%rax, %rdi
	movq	-392(%rbp), %rax
	movq	%rax, -160(%rbp)
.L2422:
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	-392(%rbp), %r12
	movq	-176(%rbp), %rbx
	jmp	.L2424
.L2547:
.LEHB251:
	call	_ZSt17__throw_bad_allocv@PLT
.LEHE251:
.L2546:
.LEHB252:
	call	_ZSt17__throw_bad_allocv@PLT
.LEHE252:
.L2544:
.LEHB253:
	call	_ZSt17__throw_bad_allocv@PLT
.LEHE253:
.L2541:
.LEHB254:
	call	_ZSt17__throw_bad_allocv@PLT
.LEHE254:
.L2549:
	leaq	.LC4(%rip), %rdi
.LEHB255:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE255:
.L2548:
	call	__stack_chk_fail@PLT
.L2488:
	endbr64
	movq	%rax, %r12
	jmp	.L2427
.L2483:
	endbr64
	movq	%rax, %rbx
	jmp	.L2476
.L2487:
	endbr64
	movq	%rax, %rbx
	jmp	.L2450
.L2484:
	endbr64
	movq	%rax, %rbx
	jmp	.L2403
.L2481:
	endbr64
	movq	%rax, %rbx
	jmp	.L2477
.L2490:
	endbr64
	movq	%rax, %rdi
	jmp	.L2444
.L2485:
	endbr64
	movq	%rax, %rbx
	jmp	.L2401
.L2482:
	endbr64
	movq	%rax, %rbx
	jmp	.L2407
.L2486:
	endbr64
	movq	%rax, %rbx
	jmp	.L2452
	.section	.gcc_except_table._ZN2v88internal6torque14DeclareMethodsEPNS1_13AggregateTypeERKSt6vectorIPNS1_11DeclarationESaIS6_EE,"a",@progbits
	.align 4
.LLSDA6534:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATT6534-.LLSDATTD6534
.LLSDATTD6534:
	.byte	0x1
	.uleb128 .LLSDACSE6534-.LLSDACSB6534
.LLSDACSB6534:
	.uleb128 .LEHB239-.LFB6534
	.uleb128 .LEHE239-.LEHB239
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB240-.LFB6534
	.uleb128 .LEHE240-.LEHB240
	.uleb128 .L2481-.LFB6534
	.uleb128 0
	.uleb128 .LEHB241-.LFB6534
	.uleb128 .LEHE241-.LEHB241
	.uleb128 .L2482-.LFB6534
	.uleb128 0
	.uleb128 .LEHB242-.LFB6534
	.uleb128 .LEHE242-.LEHB242
	.uleb128 .L2484-.LFB6534
	.uleb128 0
	.uleb128 .LEHB243-.LFB6534
	.uleb128 .LEHE243-.LEHB243
	.uleb128 .L2482-.LFB6534
	.uleb128 0
	.uleb128 .LEHB244-.LFB6534
	.uleb128 .LEHE244-.LEHB244
	.uleb128 .L2486-.LFB6534
	.uleb128 0
	.uleb128 .LEHB245-.LFB6534
	.uleb128 .LEHE245-.LEHB245
	.uleb128 .L2487-.LFB6534
	.uleb128 0
	.uleb128 .LEHB246-.LFB6534
	.uleb128 .LEHE246-.LEHB246
	.uleb128 .L2490-.LFB6534
	.uleb128 0x1
	.uleb128 .LEHB247-.LFB6534
	.uleb128 .LEHE247-.LEHB247
	.uleb128 .L2483-.LFB6534
	.uleb128 0
	.uleb128 .LEHB248-.LFB6534
	.uleb128 .LEHE248-.LEHB248
	.uleb128 .L2482-.LFB6534
	.uleb128 0
	.uleb128 .LEHB249-.LFB6534
	.uleb128 .LEHE249-.LEHB249
	.uleb128 .L2485-.LFB6534
	.uleb128 0
	.uleb128 .LEHB250-.LFB6534
	.uleb128 .LEHE250-.LEHB250
	.uleb128 .L2488-.LFB6534
	.uleb128 0
	.uleb128 .LEHB251-.LFB6534
	.uleb128 .LEHE251-.LEHB251
	.uleb128 .L2490-.LFB6534
	.uleb128 0x1
	.uleb128 .LEHB252-.LFB6534
	.uleb128 .LEHE252-.LEHB252
	.uleb128 .L2487-.LFB6534
	.uleb128 0
	.uleb128 .LEHB253-.LFB6534
	.uleb128 .LEHE253-.LEHB253
	.uleb128 .L2486-.LFB6534
	.uleb128 0
	.uleb128 .LEHB254-.LFB6534
	.uleb128 .LEHE254-.LEHB254
	.uleb128 .L2482-.LFB6534
	.uleb128 0
	.uleb128 .LEHB255-.LFB6534
	.uleb128 .LEHE255-.LEHB255
	.uleb128 .L2488-.LFB6534
	.uleb128 0
.LLSDACSE6534:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATT6534:
	.section	.text._ZN2v88internal6torque14DeclareMethodsEPNS1_13AggregateTypeERKSt6vectorIPNS1_11DeclarationESaIS6_EE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque14DeclareMethodsEPNS1_13AggregateTypeERKSt6vectorIPNS1_11DeclarationESaIS6_EE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6534
	.type	_ZN2v88internal6torque14DeclareMethodsEPNS1_13AggregateTypeERKSt6vectorIPNS1_11DeclarationESaIS6_EE.cold, @function
_ZN2v88internal6torque14DeclareMethodsEPNS1_13AggregateTypeERKSt6vectorIPNS1_11DeclarationESaIS6_EE.cold:
.LFSB6534:
.L2427:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	cmpb	$0, -184(%rbp)
	je	.L2428
	movq	-176(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L2428
	call	_ZdlPv@PLT
.L2428:
	movq	%r12, %rbx
.L2429:
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2407
.L2535:
	call	_ZdlPv@PLT
	jmp	.L2407
.L2476:
	movq	%r12, %rdi
	call	_ZN2v88internal6torque9SignatureD1Ev
.L2407:
	movq	-440(%rbp), %rdi
	call	_ZN2v88internal6torque9SignatureD1Ev
.L2477:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
	movq	-360(%rbp), %rdx
	movq	%rbx, %rdi
	movq	%rdx, (%rax)
.LEHB256:
	call	_Unwind_Resume@PLT
.LEHE256:
.L2444:
	call	__cxa_begin_catch@PLT
	movq	-424(%rbp), %rbx
.L2447:
	cmpq	%r12, %rbx
	jne	.L2551
.LEHB257:
	call	__cxa_rethrow@PLT
.LEHE257:
.L2489:
	endbr64
	movq	%rax, %rbx
	call	__cxa_end_catch@PLT
	movq	-96(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2450
	call	_ZdlPv@PLT
.L2450:
	movq	-144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2452
	call	_ZdlPv@PLT
.L2452:
	cmpb	$0, -184(%rbp)
	je	.L2429
	movq	-176(%rbp), %rdi
	leaq	-160(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2429
	call	_ZdlPv@PLT
	jmp	.L2429
.L2551:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L2446
	call	_ZdlPv@PLT
.L2446:
	addq	$32, %rbx
	jmp	.L2447
.L2401:
	movq	-392(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2403
	movq	(%rdi), %rax
	call	*8(%rax)
.L2403:
	movq	-208(%rbp), %rdi
	cmpq	%r14, %rdi
	jne	.L2535
	jmp	.L2407
	.cfi_endproc
.LFE6534:
	.section	.gcc_except_table._ZN2v88internal6torque14DeclareMethodsEPNS1_13AggregateTypeERKSt6vectorIPNS1_11DeclarationESaIS6_EE
	.align 4
.LLSDAC6534:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATTC6534-.LLSDATTDC6534
.LLSDATTDC6534:
	.byte	0x1
	.uleb128 .LLSDACSEC6534-.LLSDACSBC6534
.LLSDACSBC6534:
	.uleb128 .LEHB256-.LCOLDB39
	.uleb128 .LEHE256-.LEHB256
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB257-.LCOLDB39
	.uleb128 .LEHE257-.LEHB257
	.uleb128 .L2489-.LCOLDB39
	.uleb128 0
.LLSDACSEC6534:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATTC6534:
	.section	.text.unlikely._ZN2v88internal6torque14DeclareMethodsEPNS1_13AggregateTypeERKSt6vectorIPNS1_11DeclarationESaIS6_EE
	.section	.text._ZN2v88internal6torque14DeclareMethodsEPNS1_13AggregateTypeERKSt6vectorIPNS1_11DeclarationESaIS6_EE
	.size	_ZN2v88internal6torque14DeclareMethodsEPNS1_13AggregateTypeERKSt6vectorIPNS1_11DeclarationESaIS6_EE, .-_ZN2v88internal6torque14DeclareMethodsEPNS1_13AggregateTypeERKSt6vectorIPNS1_11DeclarationESaIS6_EE
	.section	.text.unlikely._ZN2v88internal6torque14DeclareMethodsEPNS1_13AggregateTypeERKSt6vectorIPNS1_11DeclarationESaIS6_EE
	.size	_ZN2v88internal6torque14DeclareMethodsEPNS1_13AggregateTypeERKSt6vectorIPNS1_11DeclarationESaIS6_EE.cold, .-_ZN2v88internal6torque14DeclareMethodsEPNS1_13AggregateTypeERKSt6vectorIPNS1_11DeclarationESaIS6_EE.cold
.LCOLDE39:
	.section	.text._ZN2v88internal6torque14DeclareMethodsEPNS1_13AggregateTypeERKSt6vectorIPNS1_11DeclarationESaIS6_EE
.LHOTE39:
	.section	.text._ZN2v88internal6torque11TypeVisitor18VisitStructMethodsEPNS1_10StructTypeEPKNS1_17StructDeclarationE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque11TypeVisitor18VisitStructMethodsEPNS1_10StructTypeEPKNS1_17StructDeclarationE
	.type	_ZN2v88internal6torque11TypeVisitor18VisitStructMethodsEPNS1_10StructTypeEPKNS1_17StructDeclarationE, @function
_ZN2v88internal6torque11TypeVisitor18VisitStructMethodsEPNS1_10StructTypeEPKNS1_17StructDeclarationE:
.LFB6604:
	.cfi_startproc
	endbr64
	addq	$40, %rsi
	jmp	_ZN2v88internal6torque14DeclareMethodsEPNS1_13AggregateTypeERKSt6vectorIPNS1_11DeclarationESaIS6_EE
	.cfi_endproc
.LFE6604:
	.size	_ZN2v88internal6torque11TypeVisitor18VisitStructMethodsEPNS1_10StructTypeEPKNS1_17StructDeclarationE, .-_ZN2v88internal6torque11TypeVisitor18VisitStructMethodsEPNS1_10StructTypeEPKNS1_17StructDeclarationE
	.section	.rodata._ZN2v88internal6torque11TypeVisitor26VisitClassFieldsAndMethodsEPNS1_9ClassTypeEPKNS1_16ClassDeclarationE.str1.8,"aMS",@progbits,1
	.align 8
.LC40:
	.string	"non-extern classes do not support untagged fields"
	.align 8
.LC41:
	.string	"non-extern classes do not support weak fields"
	.align 8
.LC42:
	.string	"only one indexable field is currently supported per class"
	.align 8
.LC43:
	.string	"\" after an indexable field declaration"
	.align 8
.LC44:
	.string	"cannot declare non-indexable field \""
	.section	.rodata._ZN2v88internal6torque11TypeVisitor26VisitClassFieldsAndMethodsEPNS1_9ClassTypeEPKNS1_16ClassDeclarationE.str1.1,"aMS",@progbits,1
.LC45:
	.string	" is not "
.LC46:
	.string	" at offset "
.LC47:
	.string	"field "
.LC48:
	.string	"-byte aligned."
	.section	.text.unlikely._ZN2v88internal6torque11TypeVisitor26VisitClassFieldsAndMethodsEPNS1_9ClassTypeEPKNS1_16ClassDeclarationE,"ax",@progbits
	.align 2
.LCOLDB49:
	.section	.text._ZN2v88internal6torque11TypeVisitor26VisitClassFieldsAndMethodsEPNS1_9ClassTypeEPKNS1_16ClassDeclarationE,"ax",@progbits
.LHOTB49:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque11TypeVisitor26VisitClassFieldsAndMethodsEPNS1_9ClassTypeEPKNS1_16ClassDeclarationE
	.type	_ZN2v88internal6torque11TypeVisitor26VisitClassFieldsAndMethodsEPNS1_9ClassTypeEPKNS1_16ClassDeclarationE, @function
_ZN2v88internal6torque11TypeVisitor26VisitClassFieldsAndMethodsEPNS1_9ClassTypeEPKNS1_16ClassDeclarationE:
.LFB6567:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6567
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$376, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -392(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	%rax, -416(%rbp)
	testq	%rax, %rax
	je	.L2708
	cmpl	$5, 8(%rax)
	jne	.L2638
	movq	168(%rax), %rax
	movq	%rsi, %rbx
.L2554:
	movq	128(%rbx), %r12
	movq	136(%rbx), %rbx
	movq	%rax, -352(%rbp)
	movq	%rbx, -408(%rbp)
	cmpq	%rbx, %r12
	je	.L2555
	leaq	-336(%rbp), %rax
	movb	$0, -368(%rbp)
	movq	%rax, -400(%rbp)
	leaq	-160(%rbp), %rax
	movq	%rax, -384(%rbp)
	jmp	.L2630
	.p2align 4,,10
	.p2align 3
.L2720:
	cmpb	$0, -368(%rbp)
	jne	.L2568
	movq	-416(%rbp), %rax
	testq	%rax, %rax
	je	.L2567
	movq	%rax, %rdi
.LEHB258:
	call	_ZNK2v88internal6torque9ClassType15HasIndexedFieldEv@PLT
	testb	%al, %al
	jne	.L2568
.L2567:
	leaq	24(%r12), %rsi
	movq	%r13, %rdi
	call	_ZNK2v88internal6torque13AggregateType19LookupFieldInternalERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
.LEHE258:
	movq	%rax, %r8
	movq	(%r12), %rax
	leaq	-208(%rbp), %rcx
	movq	%rcx, -360(%rbp)
	movdqu	12(%rax), %xmm2
	movaps	%xmm2, -272(%rbp)
	movl	28(%rax), %eax
	movq	%r13, -248(%rbp)
	movl	%eax, -256(%rbp)
	movb	$1, -240(%rbp)
	movq	%r8, -232(%rbp)
	movq	(%r12), %rax
	movq	%rcx, -224(%rbp)
	movq	32(%rax), %rbx
	movq	40(%rax), %r15
	movq	%rbx, %rax
	addq	%r15, %rax
	je	.L2570
	testq	%rbx, %rbx
	je	.L2712
.L2570:
	movq	%r15, -344(%rbp)
	cmpq	$15, %r15
	ja	.L2713
	cmpq	$1, %r15
	jne	.L2573
	movzbl	(%rbx), %eax
	movb	%al, -208(%rbp)
	movq	-360(%rbp), %rax
.L2574:
	movq	%r15, -216(%rbp)
	movb	$0, (%rax,%r15)
	movq	-352(%rbp), %rax
	movq	%r14, -192(%rbp)
	cmpb	$0, -240(%rbp)
	movq	%rax, -184(%rbp)
	movzbl	80(%r12), %eax
	movb	%al, -176(%rbp)
	movzbl	81(%r12), %eax
	movb	%al, -175(%rbp)
	movzbl	82(%r12), %eax
	movb	%al, -174(%rbp)
	je	.L2575
	orl	$256, 176(%r13)
.L2575:
	movl	-256(%rbp), %eax
	movq	-224(%rbp), %r15
	leaq	-96(%rbp), %rbx
	movq	-216(%rbp), %r14
	movdqa	-272(%rbp), %xmm3
	movq	%rbx, -112(%rbp)
	movl	%eax, -144(%rbp)
	movq	-248(%rbp), %rax
	movdqa	-240(%rbp), %xmm4
	movaps	%xmm3, -160(%rbp)
	movq	%rax, -136(%rbp)
	movq	%r15, %rax
	addq	%r14, %rax
	movaps	%xmm4, -128(%rbp)
	je	.L2576
	testq	%r15, %r15
	je	.L2714
.L2576:
	movq	%r14, -344(%rbp)
	cmpq	$15, %r14
	ja	.L2715
	cmpq	$1, %r14
	jne	.L2579
	movzbl	(%r15), %eax
	movb	%al, -96(%rbp)
	movq	%rbx, %rax
.L2580:
	movq	%r14, -104(%rbp)
	movb	$0, (%rax,%r14)
	movq	-192(%rbp), %rax
	movq	88(%r13), %r14
	movq	%rax, -80(%rbp)
	movq	-184(%rbp), %rax
	movq	%rax, -72(%rbp)
	movzwl	-176(%rbp), %eax
	movw	%ax, -64(%rbp)
	movzbl	-174(%rbp), %eax
	movb	%al, -62(%rbp)
	cmpq	96(%r13), %r14
	je	.L2581
	movdqa	-160(%rbp), %xmm2
	leaq	64(%r14), %rdi
	movups	%xmm2, (%r14)
	movl	-144(%rbp), %eax
	movl	%eax, 16(%r14)
	movq	-136(%rbp), %rax
	movq	%rax, 24(%r14)
	movdqa	-128(%rbp), %xmm3
	movq	%rdi, 48(%r14)
	movups	%xmm3, 32(%r14)
	movq	-112(%rbp), %rax
	movq	-104(%rbp), %r15
	movq	%rax, %rsi
	movq	%rax, -368(%rbp)
	addq	%r15, %rsi
	je	.L2582
	testq	%rax, %rax
	je	.L2716
.L2582:
	movq	%r15, -344(%rbp)
	cmpq	$15, %r15
	ja	.L2717
	cmpq	$1, %r15
	jne	.L2585
	movq	-368(%rbp), %rax
	movzbl	(%rax), %eax
	movb	%al, 64(%r14)
.L2586:
	movq	%r15, 56(%r14)
	movb	$0, (%rdi,%r15)
	movq	-80(%rbp), %rax
	movq	%rax, 80(%r14)
	movq	-72(%rbp), %rax
	movq	%rax, 88(%r14)
	movzbl	-64(%rbp), %eax
	movb	%al, 96(%r14)
	movzbl	-63(%rbp), %eax
	movb	%al, 97(%r14)
	movzbl	-62(%rbp), %eax
	movb	%al, 98(%r14)
	addq	$104, 88(%r13)
.L2587:
	movq	-112(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L2588
	call	_ZdlPv@PLT
.L2588:
	movq	-224(%rbp), %rdi
	cmpq	-360(%rbp), %rdi
	je	.L2590
	call	_ZdlPv@PLT
.L2590:
	movzbl	-376(%rbp), %eax
	movb	%al, -368(%rbp)
.L2594:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
	movq	-312(%rbp), %rdx
	addq	$88, %r12
	movq	%rdx, (%rax)
	cmpq	%r12, -408(%rbp)
	je	.L2718
.L2630:
	movq	8(%r12), %rax
	movdqu	12(%rax), %xmm1
	movl	28(%rax), %eax
	movaps	%xmm1, -336(%rbp)
	movl	%eax, -320(%rbp)
.LEHB259:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rax
	movq	%rax, -312(%rbp)
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
.LEHE259:
	movq	-400(%rbp), %rcx
	movq	%rcx, (%rax)
	movq	8(%r12), %rdi
.LEHB260:
	call	_ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_14TypeExpressionE
.LEHE260:
	movq	%rax, %r14
	movq	-392(%rbp), %rax
	testb	$1, 40(%rax)
	je	.L2719
.L2556:
	movzbl	16(%r12), %eax
	movb	%al, -376(%rbp)
	testb	%al, %al
	jne	.L2720
	cmpb	$0, -368(%rbp)
	jne	.L2721
	xorl	%eax, %eax
	leaq	-272(%rbp), %rdi
	movl	$13, %ecx
	rep stosq
	leaq	-208(%rbp), %rdx
	movq	%rdx, -360(%rbp)
	movq	(%r12), %rax
	movdqu	12(%rax), %xmm5
	movaps	%xmm5, -272(%rbp)
	movl	28(%rax), %eax
	movq	%r13, -248(%rbp)
	movl	%eax, -256(%rbp)
	movq	(%r12), %rax
	movq	%rdx, -224(%rbp)
	movq	32(%rax), %rbx
	movq	40(%rax), %r15
	movq	%rbx, %rax
	addq	%r15, %rax
	je	.L2596
	testq	%rbx, %rbx
	je	.L2722
.L2596:
	movq	%r15, -344(%rbp)
	cmpq	$15, %r15
	ja	.L2723
	cmpq	$1, %r15
	jne	.L2599
	movzbl	(%rbx), %eax
	movb	%al, -208(%rbp)
	movq	-360(%rbp), %rax
.L2600:
	movq	%r15, -216(%rbp)
	movb	$0, (%rax,%r15)
	movq	-352(%rbp), %rax
	movq	%r14, -192(%rbp)
	cmpb	$0, -240(%rbp)
	movq	%rax, -184(%rbp)
	movzbl	80(%r12), %eax
	movb	%al, -176(%rbp)
	movzbl	81(%r12), %eax
	movb	%al, -175(%rbp)
	movzbl	82(%r12), %eax
	movb	%al, -174(%rbp)
	je	.L2601
	orl	$256, 176(%r13)
.L2601:
	movl	-256(%rbp), %eax
	movq	-224(%rbp), %r15
	leaq	-96(%rbp), %rbx
	movq	-216(%rbp), %r14
	movdqa	-272(%rbp), %xmm6
	movq	%rbx, -112(%rbp)
	movl	%eax, -144(%rbp)
	movq	-248(%rbp), %rax
	movdqa	-240(%rbp), %xmm7
	movaps	%xmm6, -160(%rbp)
	movq	%rax, -136(%rbp)
	movq	%r15, %rax
	addq	%r14, %rax
	movaps	%xmm7, -128(%rbp)
	setne	%dl
	testq	%r15, %r15
	sete	%al
	andb	%al, %dl
	movb	%dl, -368(%rbp)
	jne	.L2724
	movq	%r14, -344(%rbp)
	cmpq	$15, %r14
	ja	.L2725
	cmpq	$1, %r14
	jne	.L2605
	movzbl	(%r15), %eax
	movb	%al, -96(%rbp)
	movq	%rbx, %rax
.L2606:
	movq	%r14, -104(%rbp)
	movb	$0, (%rax,%r14)
	movq	-192(%rbp), %rax
	movq	88(%r13), %r14
	movq	%rax, -80(%rbp)
	movq	-184(%rbp), %rax
	movq	%rax, -72(%rbp)
	movzwl	-176(%rbp), %eax
	movw	%ax, -64(%rbp)
	movzbl	-174(%rbp), %eax
	movb	%al, -62(%rbp)
	cmpq	96(%r13), %r14
	je	.L2607
	movdqa	-160(%rbp), %xmm4
	leaq	64(%r14), %rdi
	movups	%xmm4, (%r14)
	movl	-144(%rbp), %eax
	movl	%eax, 16(%r14)
	movq	-136(%rbp), %rax
	movq	%rax, 24(%r14)
	movdqa	-128(%rbp), %xmm5
	movq	%rdi, 48(%r14)
	movups	%xmm5, 32(%r14)
	movq	-112(%rbp), %rax
	movq	-104(%rbp), %r15
	movq	%rax, %rsi
	movq	%rax, -376(%rbp)
	addq	%r15, %rsi
	je	.L2608
	testq	%rax, %rax
	je	.L2726
.L2608:
	movq	%r15, -344(%rbp)
	cmpq	$15, %r15
	ja	.L2727
	cmpq	$1, %r15
	jne	.L2611
	movq	-376(%rbp), %rax
	movzbl	(%rax), %eax
	movb	%al, 64(%r14)
.L2612:
	movq	%r15, 56(%r14)
	movb	$0, (%rdi,%r15)
	movq	-80(%rbp), %rax
	movq	%rax, 80(%r14)
	movq	-72(%rbp), %rax
	movq	%rax, 88(%r14)
	movzbl	-64(%rbp), %eax
	movb	%al, 96(%r14)
	movzbl	-63(%rbp), %eax
	movb	%al, 97(%r14)
	movzbl	-62(%rbp), %eax
	movb	%al, 98(%r14)
	movq	88(%r13), %r14
	leaq	104(%r14), %rax
	movq	%rax, 88(%r13)
.L2613:
	movq	-112(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L2614
	call	_ZdlPv@PLT
.L2614:
	movq	-224(%rbp), %rdi
	cmpq	-360(%rbp), %rdi
	je	.L2616
	call	_ZdlPv@PLT
.L2616:
	movq	-384(%rbp), %rdi
	leaq	-256(%rbp), %rax
	movq	%r14, %rsi
	leaq	-288(%rbp), %r15
	movq	%r15, -304(%rbp)
	movq	$0, -296(%rbp)
	movb	$0, -288(%rbp)
	movq	%rax, -360(%rbp)
	movq	%rax, -272(%rbp)
	movq	$0, -264(%rbp)
	movb	$0, -256(%rbp)
.LEHB261:
	call	_ZNK2v88internal6torque5Field23GetFieldSizeInformationB5cxx11Ev@PLT
	movq	-160(%rbp), %rax
	leaq	-144(%rbp), %rbx
	movq	-128(%rbp), %r14
	movq	-304(%rbp), %rdi
	movq	-152(%rbp), %rdx
	cmpq	%rbx, %rax
	je	.L2728
	movq	-144(%rbp), %rcx
	cmpq	%r15, %rdi
	je	.L2729
	movq	%rdx, %xmm0
	movq	%rcx, %xmm6
	movq	-288(%rbp), %rsi
	movq	%rax, -304(%rbp)
	punpcklqdq	%xmm6, %xmm0
	movups	%xmm0, -296(%rbp)
	testq	%rdi, %rdi
	je	.L2625
	movq	%rdi, -160(%rbp)
	movq	%rsi, -144(%rbp)
.L2623:
	movq	$0, -152(%rbp)
	movb	$0, (%rdi)
	movq	-160(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L2626
	call	_ZdlPv@PLT
.L2626:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_18TargetArchitectureEEERPNT_5ScopeEv@PLT
.LEHE261:
	movq	(%rax), %rax
	movq	-352(%rbp), %rsi
	movslq	(%rax), %rcx
	cmpq	%r14, %rcx
	cmova	%r14, %rcx
	movq	%rcx, -344(%rbp)
	testq	%rcx, %rcx
	je	.L2627
	movq	%rsi, %rax
	xorl	%edx, %edx
	divq	%rcx
	testq	%rdx, %rdx
	jne	.L2730
.L2627:
	addq	%rsi, %r14
	movq	-272(%rbp), %rdi
	movq	%r14, -352(%rbp)
	cmpq	-360(%rbp), %rdi
	je	.L2628
	call	_ZdlPv@PLT
.L2628:
	movq	-304(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L2594
	call	_ZdlPv@PLT
	jmp	.L2594
	.p2align 4,,10
	.p2align 3
.L2719:
	movq	(%r14), %rax
	movq	16(%rax), %r15
.LEHB262:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_10TypeOracleEEERPNT_5ScopeEv@PLT
.LEHE262:
	movq	-384(%rbp), %rdi
	movl	$25701, %eax
	leaq	-144(%rbp), %rbx
	movl	$1734828372, -144(%rbp)
	movq	%rbx, -160(%rbp)
	movw	%ax, -140(%rbp)
	movq	$6, -152(%rbp)
	movb	$0, -138(%rbp)
.LEHB263:
	call	_ZN2v88internal6torque12Declarations16LookupGlobalTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
.LEHE263:
	movq	-160(%rbp), %rdi
	movq	%rax, %rsi
	cmpq	%rbx, %rdi
	je	.L2557
	movq	%rax, -360(%rbp)
	call	_ZdlPv@PLT
	movq	-360(%rbp), %rsi
.L2557:
	movq	%r14, %rdi
.LEHB264:
	call	*%r15
	testb	%al, %al
	je	.L2731
	cmpb	$0, 80(%r12)
	je	.L2556
	movq	-384(%rbp), %rbx
	leaq	.LC41(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal6torqueL7MessageIJRA55_KcEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
.LEHE264:
	movq	%rbx, %rdi
.LEHB265:
	call	_ZNK2v88internal6torque14MessageBuilder5ThrowEv@PLT
.LEHE265:
	.p2align 4,,10
	.p2align 3
.L2579:
	testq	%r14, %r14
	jne	.L2732
	movq	%rbx, %rax
	jmp	.L2580
	.p2align 4,,10
	.p2align 3
.L2573:
	testq	%r15, %r15
	jne	.L2733
	movq	-360(%rbp), %rax
	jmp	.L2574
	.p2align 4,,10
	.p2align 3
.L2605:
	testq	%r14, %r14
	jne	.L2734
	movq	%rbx, %rax
	jmp	.L2606
	.p2align 4,,10
	.p2align 3
.L2599:
	testq	%r15, %r15
	jne	.L2735
	movq	-360(%rbp), %rax
	jmp	.L2600
	.p2align 4,,10
	.p2align 3
.L2723:
	leaq	-344(%rbp), %rsi
	leaq	-224(%rbp), %rdi
	xorl	%edx, %edx
.LEHB266:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE266:
	movq	%rax, -224(%rbp)
	movq	%rax, %rdi
	movq	-344(%rbp), %rax
	movq	%rax, -208(%rbp)
.L2598:
	movq	%r15, %rdx
	movq	%rbx, %rsi
	call	memcpy@PLT
	movq	-344(%rbp), %r15
	movq	-224(%rbp), %rax
	jmp	.L2600
	.p2align 4,,10
	.p2align 3
.L2715:
	leaq	-344(%rbp), %rsi
	leaq	-112(%rbp), %rdi
	xorl	%edx, %edx
.LEHB267:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE267:
	movq	%rax, -112(%rbp)
	movq	%rax, %rdi
	movq	-344(%rbp), %rax
	movq	%rax, -96(%rbp)
.L2578:
	movq	%r14, %rdx
	movq	%r15, %rsi
	call	memcpy@PLT
	movq	-344(%rbp), %r14
	movq	-112(%rbp), %rax
	jmp	.L2580
	.p2align 4,,10
	.p2align 3
.L2725:
	leaq	-344(%rbp), %rsi
	leaq	-112(%rbp), %rdi
	xorl	%edx, %edx
.LEHB268:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE268:
	movq	%rax, -112(%rbp)
	movq	%rax, %rdi
	movq	-344(%rbp), %rax
	movq	%rax, -96(%rbp)
.L2604:
	movq	%r14, %rdx
	movq	%r15, %rsi
	call	memcpy@PLT
	movq	-344(%rbp), %r14
	movq	-112(%rbp), %rax
	jmp	.L2606
	.p2align 4,,10
	.p2align 3
.L2713:
	leaq	-344(%rbp), %rsi
	leaq	-224(%rbp), %rdi
	xorl	%edx, %edx
.LEHB269:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE269:
	movq	%rax, -224(%rbp)
	movq	%rax, %rdi
	movq	-344(%rbp), %rax
	movq	%rax, -208(%rbp)
.L2572:
	movq	%r15, %rdx
	movq	%rbx, %rsi
	call	memcpy@PLT
	movq	-344(%rbp), %r15
	movq	-224(%rbp), %rax
	jmp	.L2574
	.p2align 4,,10
	.p2align 3
.L2607:
	movq	-384(%rbp), %rdx
	leaq	80(%r13), %rdi
	movq	%r14, %rsi
.LEHB270:
	call	_ZNSt6vectorIN2v88internal6torque5FieldESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
.LEHE270:
	movq	88(%r13), %rax
	leaq	-104(%rax), %r14
	jmp	.L2613
	.p2align 4,,10
	.p2align 3
.L2728:
	testq	%rdx, %rdx
	je	.L2621
	cmpq	$1, %rdx
	je	.L2736
	movq	%rbx, %rsi
	call	memcpy@PLT
	movq	-152(%rbp), %rdx
	movq	-304(%rbp), %rdi
.L2621:
	movq	%rdx, -296(%rbp)
	movb	$0, (%rdi,%rdx)
	movq	-160(%rbp), %rdi
	jmp	.L2623
	.p2align 4,,10
	.p2align 3
.L2581:
	movq	-384(%rbp), %rdx
	leaq	80(%r13), %rdi
	movq	%r14, %rsi
.LEHB271:
	call	_ZNSt6vectorIN2v88internal6torque5FieldESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
.LEHE271:
	jmp	.L2587
	.p2align 4,,10
	.p2align 3
.L2611:
	testq	%r15, %r15
	je	.L2612
	jmp	.L2610
	.p2align 4,,10
	.p2align 3
.L2585:
	testq	%r15, %r15
	je	.L2586
	jmp	.L2584
	.p2align 4,,10
	.p2align 3
.L2727:
	leaq	-344(%rbp), %rsi
	leaq	48(%r14), %rdi
	xorl	%edx, %edx
.LEHB272:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE272:
	movq	%rax, 48(%r14)
	movq	%rax, %rdi
	movq	-344(%rbp), %rax
	movq	%rax, 64(%r14)
.L2610:
	movq	-376(%rbp), %rsi
	movq	%r15, %rdx
	call	memcpy@PLT
	movq	-344(%rbp), %r15
	movq	48(%r14), %rdi
	jmp	.L2612
	.p2align 4,,10
	.p2align 3
.L2717:
	leaq	-344(%rbp), %rsi
	leaq	48(%r14), %rdi
	xorl	%edx, %edx
.LEHB273:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE273:
	movq	%rax, 48(%r14)
	movq	%rax, %rdi
	movq	-344(%rbp), %rax
	movq	%rax, 64(%r14)
.L2584:
	movq	-368(%rbp), %rsi
	movq	%r15, %rdx
	call	memcpy@PLT
	movq	-344(%rbp), %r15
	movq	48(%r14), %rdi
	jmp	.L2586
	.p2align 4,,10
	.p2align 3
.L2729:
	movq	%rdx, %xmm0
	movq	%rcx, %xmm7
	movq	%rax, -304(%rbp)
	punpcklqdq	%xmm7, %xmm0
	movups	%xmm0, -296(%rbp)
.L2625:
	movq	%rbx, -160(%rbp)
	leaq	-144(%rbp), %rbx
	movq	%rbx, %rdi
	jmp	.L2623
	.p2align 4,,10
	.p2align 3
.L2736:
	movzbl	-144(%rbp), %eax
	movb	%al, (%rdi)
	movq	-152(%rbp), %rdx
	movq	-304(%rbp), %rdi
	jmp	.L2621
	.p2align 4,,10
	.p2align 3
.L2718:
	movq	-352(%rbp), %rax
.L2555:
	movq	%rax, 168(%r13)
	movq	%r13, %rdi
.LEHB274:
	call	_ZN2v88internal6torque9ClassType17GenerateAccessorsEv@PLT
	movq	-392(%rbp), %rsi
	movq	%r13, %rdi
	addq	$104, %rsi
	call	_ZN2v88internal6torque14DeclareMethodsEPNS1_13AggregateTypeERKSt6vectorIPNS1_11DeclarationESaIS6_EE
.LEHE274:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2737
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2638:
	.cfi_restore_state
	movq	$0, -416(%rbp)
.L2708:
	xorl	%eax, %eax
	movq	%rsi, %rbx
	jmp	.L2554
.L2568:
	leaq	-160(%rbp), %r13
	leaq	.LC42(%rip), %rsi
	movq	%r13, %rdi
.LEHB275:
	call	_ZN2v88internal6torqueL7MessageIJRA55_KcEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
.LEHE275:
	movq	%r13, %rdi
.LEHB276:
	call	_ZNK2v88internal6torque14MessageBuilder5ThrowEv@PLT
.LEHE276:
.L2721:
	leaq	.LC43(%rip), %rdx
	movq	%r12, %rsi
	leaq	.LC44(%rip), %rdi
.LEHB277:
	call	_ZN2v88internal6torque11ReportErrorIJRA37_KcRKPNS1_10IdentifierERA39_S3_EEEvDpOT_
.L2722:
	leaq	.LC4(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE277:
.L2724:
	leaq	.LC4(%rip), %rdi
.LEHB278:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE278:
.L2731:
	leaq	.LC40(%rip), %rdi
.LEHB279:
	call	_ZN2v88internal6torque11ReportErrorIJRA50_KcEEEvDpOT_
.LEHE279:
.L2730:
	subq	$8, %rsp
	leaq	.LC48(%rip), %rax
	leaq	-352(%rbp), %rcx
	movq	%r12, %rsi
	pushq	%rax
	leaq	-344(%rbp), %r9
	leaq	.LC45(%rip), %r8
	leaq	.LC46(%rip), %rdx
	leaq	.LC47(%rip), %rdi
.LEHB280:
	.cfi_escape 0x2e,0x10
	call	_ZN2v88internal6torque11ReportErrorIJRA7_KcRKPNS1_10IdentifierERA12_S3_RmRA9_S3_SC_RA15_S3_EEEvDpOT_
.LEHE280:
.L2714:
	leaq	.LC4(%rip), %rdi
.LEHB281:
	.cfi_escape 0x2e,0
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE281:
.L2712:
	leaq	.LC4(%rip), %rdi
.LEHB282:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE282:
.L2716:
	leaq	.LC4(%rip), %rdi
.LEHB283:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE283:
.L2726:
	leaq	.LC4(%rip), %rdi
.LEHB284:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE284:
.L2737:
	call	__stack_chk_fail@PLT
.L2735:
	movq	-360(%rbp), %rdi
	jmp	.L2598
.L2734:
	movq	%rbx, %rdi
	jmp	.L2604
.L2733:
	movq	-360(%rbp), %rdi
	jmp	.L2572
.L2732:
	movq	%rbx, %rdi
	jmp	.L2578
.L2643:
	endbr64
	movq	%rax, %r12
	jmp	.L2561
.L2647:
	endbr64
	movq	%rax, %r12
	jmp	.L2559
.L2650:
	endbr64
	movq	%rax, %r12
	jmp	.L2591
.L2648:
	endbr64
	movq	%rax, %r12
	jmp	.L2563
.L2645:
	endbr64
	movq	%rax, %r12
	jmp	.L2619
.L2646:
	endbr64
	movq	%rax, %r12
	jmp	.L2633
.L2651:
	endbr64
	movq	%rax, %r12
	jmp	.L2617
.L2649:
	endbr64
	movq	%rax, %r12
	jmp	.L2569
.L2644:
	endbr64
	movq	%rax, %r12
	jmp	.L2619
	.section	.gcc_except_table._ZN2v88internal6torque11TypeVisitor26VisitClassFieldsAndMethodsEPNS1_9ClassTypeEPKNS1_16ClassDeclarationE,"a",@progbits
.LLSDA6567:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6567-.LLSDACSB6567
.LLSDACSB6567:
	.uleb128 .LEHB258-.LFB6567
	.uleb128 .LEHE258-.LEHB258
	.uleb128 .L2643-.LFB6567
	.uleb128 0
	.uleb128 .LEHB259-.LFB6567
	.uleb128 .LEHE259-.LEHB259
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB260-.LFB6567
	.uleb128 .LEHE260-.LEHB260
	.uleb128 .L2643-.LFB6567
	.uleb128 0
	.uleb128 .LEHB261-.LFB6567
	.uleb128 .LEHE261-.LEHB261
	.uleb128 .L2646-.LFB6567
	.uleb128 0
	.uleb128 .LEHB262-.LFB6567
	.uleb128 .LEHE262-.LEHB262
	.uleb128 .L2643-.LFB6567
	.uleb128 0
	.uleb128 .LEHB263-.LFB6567
	.uleb128 .LEHE263-.LEHB263
	.uleb128 .L2647-.LFB6567
	.uleb128 0
	.uleb128 .LEHB264-.LFB6567
	.uleb128 .LEHE264-.LEHB264
	.uleb128 .L2643-.LFB6567
	.uleb128 0
	.uleb128 .LEHB265-.LFB6567
	.uleb128 .LEHE265-.LEHB265
	.uleb128 .L2648-.LFB6567
	.uleb128 0
	.uleb128 .LEHB266-.LFB6567
	.uleb128 .LEHE266-.LEHB266
	.uleb128 .L2643-.LFB6567
	.uleb128 0
	.uleb128 .LEHB267-.LFB6567
	.uleb128 .LEHE267-.LEHB267
	.uleb128 .L2644-.LFB6567
	.uleb128 0
	.uleb128 .LEHB268-.LFB6567
	.uleb128 .LEHE268-.LEHB268
	.uleb128 .L2645-.LFB6567
	.uleb128 0
	.uleb128 .LEHB269-.LFB6567
	.uleb128 .LEHE269-.LEHB269
	.uleb128 .L2643-.LFB6567
	.uleb128 0
	.uleb128 .LEHB270-.LFB6567
	.uleb128 .LEHE270-.LEHB270
	.uleb128 .L2651-.LFB6567
	.uleb128 0
	.uleb128 .LEHB271-.LFB6567
	.uleb128 .LEHE271-.LEHB271
	.uleb128 .L2650-.LFB6567
	.uleb128 0
	.uleb128 .LEHB272-.LFB6567
	.uleb128 .LEHE272-.LEHB272
	.uleb128 .L2651-.LFB6567
	.uleb128 0
	.uleb128 .LEHB273-.LFB6567
	.uleb128 .LEHE273-.LEHB273
	.uleb128 .L2650-.LFB6567
	.uleb128 0
	.uleb128 .LEHB274-.LFB6567
	.uleb128 .LEHE274-.LEHB274
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB275-.LFB6567
	.uleb128 .LEHE275-.LEHB275
	.uleb128 .L2643-.LFB6567
	.uleb128 0
	.uleb128 .LEHB276-.LFB6567
	.uleb128 .LEHE276-.LEHB276
	.uleb128 .L2649-.LFB6567
	.uleb128 0
	.uleb128 .LEHB277-.LFB6567
	.uleb128 .LEHE277-.LEHB277
	.uleb128 .L2643-.LFB6567
	.uleb128 0
	.uleb128 .LEHB278-.LFB6567
	.uleb128 .LEHE278-.LEHB278
	.uleb128 .L2645-.LFB6567
	.uleb128 0
	.uleb128 .LEHB279-.LFB6567
	.uleb128 .LEHE279-.LEHB279
	.uleb128 .L2643-.LFB6567
	.uleb128 0
	.uleb128 .LEHB280-.LFB6567
	.uleb128 .LEHE280-.LEHB280
	.uleb128 .L2646-.LFB6567
	.uleb128 0
	.uleb128 .LEHB281-.LFB6567
	.uleb128 .LEHE281-.LEHB281
	.uleb128 .L2644-.LFB6567
	.uleb128 0
	.uleb128 .LEHB282-.LFB6567
	.uleb128 .LEHE282-.LEHB282
	.uleb128 .L2643-.LFB6567
	.uleb128 0
	.uleb128 .LEHB283-.LFB6567
	.uleb128 .LEHE283-.LEHB283
	.uleb128 .L2650-.LFB6567
	.uleb128 0
	.uleb128 .LEHB284-.LFB6567
	.uleb128 .LEHE284-.LEHB284
	.uleb128 .L2651-.LFB6567
	.uleb128 0
.LLSDACSE6567:
	.section	.text._ZN2v88internal6torque11TypeVisitor26VisitClassFieldsAndMethodsEPNS1_9ClassTypeEPKNS1_16ClassDeclarationE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque11TypeVisitor26VisitClassFieldsAndMethodsEPNS1_9ClassTypeEPKNS1_16ClassDeclarationE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6567
	.type	_ZN2v88internal6torque11TypeVisitor26VisitClassFieldsAndMethodsEPNS1_9ClassTypeEPKNS1_16ClassDeclarationE.cold, @function
_ZN2v88internal6torque11TypeVisitor26VisitClassFieldsAndMethodsEPNS1_9ClassTypeEPKNS1_16ClassDeclarationE.cold:
.LFSB6567:
.L2563:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-384(%rbp), %rdi
	call	_ZN2v88internal6torque14MessageBuilderD1Ev
.L2561:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
	movq	-312(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rdx, (%rax)
.LEHB285:
	call	_Unwind_Resume@PLT
.LEHE285:
.L2617:
	movq	-112(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L2619
	call	_ZdlPv@PLT
.L2619:
	movq	-224(%rbp), %rdi
	cmpq	-360(%rbp), %rdi
	je	.L2561
.L2710:
	call	_ZdlPv@PLT
	jmp	.L2561
.L2591:
	movq	-112(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L2619
	call	_ZdlPv@PLT
	jmp	.L2619
.L2559:
	movq	-160(%rbp), %rdi
	cmpq	%rbx, %rdi
	jne	.L2710
	jmp	.L2561
.L2633:
	movq	-272(%rbp), %rdi
	cmpq	-360(%rbp), %rdi
	je	.L2634
	call	_ZdlPv@PLT
.L2634:
	movq	-304(%rbp), %rdi
	cmpq	%r15, %rdi
	jne	.L2710
	jmp	.L2561
.L2569:
	movq	%r13, %rdi
	call	_ZN2v88internal6torque14MessageBuilderD1Ev
	jmp	.L2561
	.cfi_endproc
.LFE6567:
	.section	.gcc_except_table._ZN2v88internal6torque11TypeVisitor26VisitClassFieldsAndMethodsEPNS1_9ClassTypeEPKNS1_16ClassDeclarationE
.LLSDAC6567:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6567-.LLSDACSBC6567
.LLSDACSBC6567:
	.uleb128 .LEHB285-.LCOLDB49
	.uleb128 .LEHE285-.LEHB285
	.uleb128 0
	.uleb128 0
.LLSDACSEC6567:
	.section	.text.unlikely._ZN2v88internal6torque11TypeVisitor26VisitClassFieldsAndMethodsEPNS1_9ClassTypeEPKNS1_16ClassDeclarationE
	.section	.text._ZN2v88internal6torque11TypeVisitor26VisitClassFieldsAndMethodsEPNS1_9ClassTypeEPKNS1_16ClassDeclarationE
	.size	_ZN2v88internal6torque11TypeVisitor26VisitClassFieldsAndMethodsEPNS1_9ClassTypeEPKNS1_16ClassDeclarationE, .-_ZN2v88internal6torque11TypeVisitor26VisitClassFieldsAndMethodsEPNS1_9ClassTypeEPKNS1_16ClassDeclarationE
	.section	.text.unlikely._ZN2v88internal6torque11TypeVisitor26VisitClassFieldsAndMethodsEPNS1_9ClassTypeEPKNS1_16ClassDeclarationE
	.size	_ZN2v88internal6torque11TypeVisitor26VisitClassFieldsAndMethodsEPNS1_9ClassTypeEPKNS1_16ClassDeclarationE.cold, .-_ZN2v88internal6torque11TypeVisitor26VisitClassFieldsAndMethodsEPNS1_9ClassTypeEPKNS1_16ClassDeclarationE.cold
.LCOLDE49:
	.section	.text._ZN2v88internal6torque11TypeVisitor26VisitClassFieldsAndMethodsEPNS1_9ClassTypeEPKNS1_16ClassDeclarationE
.LHOTE49:
	.section	.rodata._ZN2v88internal6torque11TypeVisitor30ComputeTypeForStructExpressionEPNS1_14TypeExpressionERKSt6vectorIPKNS1_4TypeESaIS8_EE.str1.8,"aMS",@progbits,1
	.align 8
.LC50:
	.string	"expected basic type expression referring to struct"
	.align 8
.LC51:
	.string	" is not a struct, but used like one"
	.section	.rodata._ZN2v88internal6torque11TypeVisitor30ComputeTypeForStructExpressionEPNS1_14TypeExpressionERKSt6vectorIPKNS1_4TypeESaIS8_EE.str1.1,"aMS",@progbits,1
.LC52:
	.string	"vector::reserve"
.LC53:
	.string	" initialization: "
	.section	.rodata._ZN2v88internal6torque11TypeVisitor30ComputeTypeForStructExpressionEPNS1_14TypeExpressionERKSt6vectorIPKNS1_4TypeESaIS8_EE.str1.8
	.align 8
.LC54:
	.string	"failed to infer type arguments for struct "
	.section	.text.unlikely._ZN2v88internal6torque11TypeVisitor30ComputeTypeForStructExpressionEPNS1_14TypeExpressionERKSt6vectorIPKNS1_4TypeESaIS8_EE,"ax",@progbits
	.align 2
.LCOLDB55:
	.section	.text._ZN2v88internal6torque11TypeVisitor30ComputeTypeForStructExpressionEPNS1_14TypeExpressionERKSt6vectorIPKNS1_4TypeESaIS8_EE,"ax",@progbits
.LHOTB55:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque11TypeVisitor30ComputeTypeForStructExpressionEPNS1_14TypeExpressionERKSt6vectorIPKNS1_4TypeESaIS8_EE
	.type	_ZN2v88internal6torque11TypeVisitor30ComputeTypeForStructExpressionEPNS1_14TypeExpressionERKSt6vectorIPKNS1_4TypeESaIS8_EE, @function
_ZN2v88internal6torque11TypeVisitor30ComputeTypeForStructExpressionEPNS1_14TypeExpressionERKSt6vectorIPKNS1_4TypeESaIS8_EE:
.LFB6605:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6605
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$376, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -416(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L2739
	cmpl	$20, 8(%rdi)
	movq	%rdi, %r15
	jne	.L2739
	movq	40(%rdi), %rax
	movq	32(%rdi), %r13
	pxor	%xmm0, %xmm0
	movq	$0, -256(%rbp)
	movaps	%xmm0, -272(%rbp)
	movq	%rax, %rbx
	movq	%rax, -392(%rbp)
	subq	%r13, %rbx
	movq	%rbx, %rax
	sarq	$5, %rax
	je	.L2899
	movabsq	$288230376151711743, %rdx
	cmpq	%rdx, %rax
	ja	.L2900
	movq	%rbx, %rdi
.LEHB286:
	call	_Znwm@PLT
.LEHE286:
	movq	32(%r15), %r13
	movq	%rax, -408(%rbp)
	movq	40(%r15), %rax
	movq	%rax, -392(%rbp)
.L2743:
	movq	-408(%rbp), %rax
	movq	%rax, %xmm0
	addq	%rax, %rbx
	punpcklqdq	%xmm0, %xmm0
	movq	%rbx, -256(%rbp)
	movq	%rax, %rbx
	movaps	%xmm0, -272(%rbp)
	cmpq	-392(%rbp), %r13
	je	.L2745
	leaq	-304(%rbp), %rax
	movq	%rax, -400(%rbp)
	jmp	.L2751
	.p2align 4,,10
	.p2align 3
.L2747:
	cmpq	$1, %r12
	jne	.L2749
	movzbl	(%r14), %eax
	movb	%al, 16(%rbx)
.L2750:
	movq	%r12, 8(%rbx)
	addq	$32, %r13
	addq	$32, %rbx
	movb	$0, (%rdi,%r12)
	cmpq	%r13, -392(%rbp)
	je	.L2745
.L2751:
	leaq	16(%rbx), %rdi
	movq	%rdi, (%rbx)
	movq	0(%r13), %r14
	movq	8(%r13), %r12
	movq	%r14, %rax
	addq	%r12, %rax
	je	.L2746
	testq	%r14, %r14
	je	.L2901
.L2746:
	movq	%r12, -304(%rbp)
	cmpq	$15, %r12
	jbe	.L2747
	movq	-400(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rbx, %rdi
.LEHB287:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE287:
	movq	%rax, (%rbx)
	movq	%rax, %rdi
	movq	-304(%rbp), %rax
	movq	%rax, 16(%rbx)
.L2748:
	movq	%r12, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-304(%rbp), %r12
	movq	(%rbx), %rdi
	jmp	.L2750
	.p2align 4,,10
	.p2align 3
.L2749:
	testq	%r12, %r12
	je	.L2750
	jmp	.L2748
	.p2align 4,,10
	.p2align 3
.L2745:
	movq	64(%r15), %r13
	movq	72(%r15), %r12
	movq	%rbx, -264(%rbp)
	leaq	-144(%rbp), %rbx
	movq	%rbx, -160(%rbp)
	movq	%r13, %rax
	addq	%r12, %rax
	je	.L2754
	testq	%r13, %r13
	je	.L2902
.L2754:
	movq	%r12, -304(%rbp)
	cmpq	$15, %r12
	ja	.L2903
	cmpq	$1, %r12
	jne	.L2763
	movzbl	0(%r13), %eax
	movb	%al, -144(%rbp)
	movq	%rbx, %rax
.L2764:
	movq	%r12, -152(%rbp)
	pxor	%xmm0, %xmm0
	leaq	-88(%rbp), %r14
	movb	$0, (%rax,%r12)
	movq	-256(%rbp), %rax
	movdqa	-272(%rbp), %xmm1
	movq	%r14, -104(%rbp)
	movq	%rax, -112(%rbp)
	movq	-160(%rbp), %rax
	movq	$0, -256(%rbp)
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm0, -272(%rbp)
	cmpq	%rbx, %rax
	je	.L2904
	movq	%rax, -104(%rbp)
	movq	-144(%rbp), %rax
	movq	%rax, -88(%rbp)
.L2766:
	movq	-152(%rbp), %rax
	movq	%rax, -96(%rbp)
	leaq	-128(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -400(%rbp)
.LEHB288:
	call	_ZN2v88internal6torque12Declarations26TryLookupGenericStructTypeERKNS1_13QualifiedNameE@PLT
	movq	%rdx, %r12
	testb	%al, %al
	jne	.L2767
	movq	%r15, %rdi
	call	_ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_14TypeExpressionE
.LEHE288:
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L2768
	cmpl	$4, 8(%rax)
	jne	.L2768
.L2769:
	movq	-104(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L2798
	call	_ZdlPv@PLT
.L2798:
	movq	-120(%rbp), %rbx
	movq	-128(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L2799
	.p2align 4,,10
	.p2align 3
.L2803:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2800
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%rbx, %r12
	jne	.L2803
.L2801:
	movq	-128(%rbp), %r12
.L2799:
	testq	%r12, %r12
	je	.L2804
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L2804:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2905
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2800:
	.cfi_restore_state
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L2803
	jmp	.L2801
	.p2align 4,,10
	.p2align 3
.L2739:
	leaq	-128(%rbp), %r12
	leaq	.LC50(%rip), %rsi
	movq	%r12, %rdi
.LEHB289:
	call	_ZN2v88internal6torqueL7MessageIJRA55_KcEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
.LEHE289:
	movq	%r12, %rdi
.LEHB290:
	call	_ZNK2v88internal6torque14MessageBuilder5ThrowEv@PLT
.LEHE290:
	.p2align 4,,10
	.p2align 3
.L2767:
	movq	96(%r15), %rbx
	movq	104(%r15), %r13
	pxor	%xmm0, %xmm0
	movq	$0, -352(%rbp)
	movaps	%xmm0, -368(%rbp)
	cmpq	%r13, %rbx
	je	.L2770
	leaq	-272(%rbp), %rax
	movq	%rax, -392(%rbp)
	jmp	.L2773
	.p2align 4,,10
	.p2align 3
.L2906:
	addq	$8, %rbx
	movq	%rax, (%rsi)
	addq	$8, -360(%rbp)
	cmpq	%rbx, %r13
	je	.L2770
.L2773:
	movq	(%rbx), %rdi
.LEHB291:
	call	_ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_14TypeExpressionE
	movq	%rax, -272(%rbp)
	movq	-360(%rbp), %rsi
	cmpq	-352(%rbp), %rsi
	jne	.L2906
	movq	-392(%rbp), %rdx
	leaq	-368(%rbp), %rdi
	call	_ZNSt6vectorIPKN2v88internal6torque4TypeESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
.LEHE291:
	addq	$8, %rbx
	cmpq	%rbx, %r13
	jne	.L2773
	.p2align 4,,10
	.p2align 3
.L2770:
	movq	104(%r12), %rbx
	pxor	%xmm0, %xmm0
	movabsq	$-6148914691236517205, %rax
	movq	$0, -320(%rbp)
	movaps	%xmm0, -336(%rbp)
	movq	72(%rbx), %r13
	movq	64(%rbx), %rcx
	movq	%r13, %rdx
	subq	%rcx, %rdx
	sarq	$3, %rdx
	imulq	%rax, %rdx
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L2907
	xorl	%eax, %eax
	xorl	%r8d, %r8d
	testq	%rdx, %rdx
	jne	.L2908
.L2778:
	cmpq	%r13, %rcx
	je	.L2782
	leaq	24(%rcx), %rbx
	leaq	-336(%rbp), %rcx
	movq	%rcx, -392(%rbp)
	jmp	.L2785
	.p2align 4,,10
	.p2align 3
.L2909:
	movq	-16(%rbx), %rax
	movq	%rax, (%r8)
	addq	$8, -328(%rbp)
	cmpq	%rbx, %r13
	je	.L2782
.L2910:
	movq	-328(%rbp), %r8
	movq	-320(%rbp), %rax
	addq	$24, %rbx
.L2785:
	cmpq	%rax, %r8
	jne	.L2909
	movq	-392(%rbp), %rdi
	leaq	-16(%rbx), %rdx
	movq	%r8, %rsi
.LEHB292:
	call	_ZNSt6vectorIPN2v88internal6torque14TypeExpressionESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	cmpq	%rbx, %r13
	jne	.L2910
	.p2align 4,,10
	.p2align 3
.L2782:
	movq	16(%r12), %rax
	movq	%rax, -384(%rbp)
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rax
	movq	%rax, -376(%rbp)
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
.LEHE292:
	leaq	-384(%rbp), %rdx
	leaq	-272(%rbp), %r13
	movq	-416(%rbp), %r8
	movq	%rdx, (%rax)
	movq	104(%r12), %rax
	movq	%r13, %rdi
	leaq	-336(%rbp), %rcx
	leaq	-368(%rbp), %rdx
	leaq	88(%rax), %rsi
.LEHB293:
	call	_ZN2v88internal6torque21TypeArgumentInferenceC1ERKSt6vectorIPNS1_10IdentifierESaIS5_EERKS3_IPKNS1_4TypeESaISC_EERKS3_IPNS1_14TypeExpressionESaISI_EESG_@PLT
.LEHE293:
	cmpb	$0, -184(%rbp)
	jne	.L2911
.LEHB294:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_13GlobalContextEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rax
	cmpb	$0, (%rax)
	jne	.L2912
.L2787:
	leaq	-304(%rbp), %r15
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZNK2v88internal6torque21TypeArgumentInference9GetResultEv@PLT
.LEHE294:
	movq	%r15, %rsi
	movq	%r12, %rdi
.LEHB295:
	call	_ZN2v88internal6torque10TypeOracle28GetGenericStructTypeInstanceEPNS1_17GenericStructTypeESt6vectorIPKNS1_4TypeESaIS8_EE@PLT
.LEHE295:
	movq	-304(%rbp), %rdi
	movq	%rax, %r13
	testq	%rdi, %rdi
	je	.L2788
	call	_ZdlPv@PLT
.L2788:
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2789
	call	_ZdlPv@PLT
.L2789:
	movq	-248(%rbp), %r12
	testq	%r12, %r12
	jne	.L2790
	jmp	.L2794
	.p2align 4,,10
	.p2align 3
.L2913:
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L2794
.L2795:
	movq	%rbx, %r12
.L2790:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	movq	(%r12), %rbx
	cmpq	%rax, %rdi
	jne	.L2913
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L2795
.L2794:
	movq	-256(%rbp), %rax
	movq	-264(%rbp), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	-264(%rbp), %rdi
	leaq	-216(%rbp), %rax
	movq	$0, -240(%rbp)
	movq	$0, -248(%rbp)
	cmpq	%rax, %rdi
	je	.L2791
	call	_ZdlPv@PLT
.L2791:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
	movq	-376(%rbp), %rdx
	movq	%rdx, (%rax)
	movq	-336(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2796
	call	_ZdlPv@PLT
.L2796:
	movq	-368(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2769
	call	_ZdlPv@PLT
	jmp	.L2769
	.p2align 4,,10
	.p2align 3
.L2763:
	testq	%r12, %r12
	jne	.L2914
	movq	%rbx, %rax
	jmp	.L2764
	.p2align 4,,10
	.p2align 3
.L2903:
	leaq	-304(%rbp), %rsi
	leaq	-160(%rbp), %rdi
	xorl	%edx, %edx
.LEHB296:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE296:
	movq	%rax, -160(%rbp)
	movq	%rax, %rdi
	movq	-304(%rbp), %rax
	movq	%rax, -144(%rbp)
.L2762:
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	-304(%rbp), %r12
	movq	-160(%rbp), %rax
	jmp	.L2764
	.p2align 4,,10
	.p2align 3
.L2899:
	movq	$0, -408(%rbp)
	jmp	.L2743
	.p2align 4,,10
	.p2align 3
.L2912:
	movq	104(%r12), %rax
	subq	$48, %rsp
	movq	32(%rax), %rax
	movdqu	12(%rax), %xmm3
	movups	%xmm3, 24(%rsp)
	movl	28(%rax), %eax
	movl	%eax, 40(%rsp)
	movdqu	12(%r15), %xmm4
	movups	%xmm4, (%rsp)
	movl	28(%r15), %eax
	movl	%eax, 16(%rsp)
.LEHB297:
	.cfi_escape 0x2e,0x30
	call	_ZN2v88internal6torque18LanguageServerData13AddDefinitionENS1_14SourcePositionES3_@PLT
.LEHE297:
	addq	$48, %rsp
	jmp	.L2787
	.p2align 4,,10
	.p2align 3
.L2904:
	movdqa	-144(%rbp), %xmm2
	movups	%xmm2, -88(%rbp)
	jmp	.L2766
	.p2align 4,,10
	.p2align 3
.L2768:
	leaq	.LC51(%rip), %rsi
	movq	%r13, %rdi
.LEHB298:
	.cfi_escape 0x2e,0
	call	_ZN2v88internal6torque11ReportErrorIJRKNS1_4TypeERA36_KcEEEvDpOT_
.LEHE298:
	.p2align 4,,10
	.p2align 3
.L2908:
	leaq	0(,%rdx,8), %r13
	movq	%r13, %rdi
.LEHB299:
	call	_Znwm@PLT
.LEHE299:
	movq	-336(%rbp), %r9
	movq	-328(%rbp), %rdx
	movq	%rax, %r8
	subq	%r9, %rdx
	testq	%rdx, %rdx
	jg	.L2915
	testq	%r9, %r9
	jne	.L2780
.L2781:
	movq	%r8, %xmm0
	leaq	(%r8,%r13), %rax
	movq	64(%rbx), %rcx
	movq	72(%rbx), %r13
	punpcklqdq	%xmm0, %xmm0
	movq	%rax, -320(%rbp)
	movaps	%xmm0, -336(%rbp)
	jmp	.L2778
	.p2align 4,,10
	.p2align 3
.L2915:
	movq	%r9, %rsi
	movq	%rax, %rdi
	movq	%r9, -392(%rbp)
	call	memmove@PLT
	movq	-392(%rbp), %r9
	movq	%rax, %r8
.L2780:
	movq	%r9, %rdi
	movq	%r8, -392(%rbp)
	call	_ZdlPv@PLT
	movq	-392(%rbp), %r8
	jmp	.L2781
.L2901:
	leaq	.LC4(%rip), %rdi
.LEHB300:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE300:
.L2900:
.LEHB301:
	call	_ZSt17__throw_bad_allocv@PLT
.LEHE301:
.L2905:
	call	__stack_chk_fail@PLT
.L2911:
	movq	-176(%rbp), %rax
	leaq	-304(%rbp), %rcx
	leaq	64(%r15), %rsi
	leaq	.LC53(%rip), %rdx
	leaq	.LC54(%rip), %rdi
	movq	%rax, -304(%rbp)
.LEHB302:
	call	_ZN2v88internal6torque11ReportErrorIJRA43_KcRNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA18_S3_PS3_EEEvDpOT_
.LEHE302:
.L2902:
	leaq	.LC4(%rip), %rdi
.LEHB303:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE303:
.L2914:
	movq	%rbx, %rdi
	jmp	.L2762
.L2907:
	leaq	.LC52(%rip), %rdi
.LEHB304:
	call	_ZSt20__throw_length_errorPKc@PLT
.LEHE304:
.L2826:
	endbr64
	movq	%rax, %rbx
	jmp	.L2808
.L2824:
	endbr64
	movq	%rax, %r12
	jmp	.L2815
.L2827:
	endbr64
	movq	%rax, %rbx
	jmp	.L2806
.L2825:
	endbr64
	movq	%rax, %r12
	jmp	.L2814
.L2822:
	endbr64
	movq	%rax, %r12
	jmp	.L2805
.L2823:
	endbr64
	movq	%rax, %r12
	jmp	.L2776
.L2831:
	endbr64
	movq	%rax, %r12
	jmp	.L2774
.L2828:
	endbr64
	movq	%rax, %r13
	jmp	.L2741
.L2830:
	endbr64
	movq	%rax, %rdi
	jmp	.L2755
	.section	.gcc_except_table._ZN2v88internal6torque11TypeVisitor30ComputeTypeForStructExpressionEPNS1_14TypeExpressionERKSt6vectorIPKNS1_4TypeESaIS8_EE,"a",@progbits
	.align 4
.LLSDA6605:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATT6605-.LLSDATTD6605
.LLSDATTD6605:
	.byte	0x1
	.uleb128 .LLSDACSE6605-.LLSDACSB6605
.LLSDACSB6605:
	.uleb128 .LEHB286-.LFB6605
	.uleb128 .LEHE286-.LEHB286
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB287-.LFB6605
	.uleb128 .LEHE287-.LEHB287
	.uleb128 .L2830-.LFB6605
	.uleb128 0x1
	.uleb128 .LEHB288-.LFB6605
	.uleb128 .LEHE288-.LEHB288
	.uleb128 .L2823-.LFB6605
	.uleb128 0
	.uleb128 .LEHB289-.LFB6605
	.uleb128 .LEHE289-.LEHB289
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB290-.LFB6605
	.uleb128 .LEHE290-.LEHB290
	.uleb128 .L2828-.LFB6605
	.uleb128 0
	.uleb128 .LEHB291-.LFB6605
	.uleb128 .LEHE291-.LEHB291
	.uleb128 .L2831-.LFB6605
	.uleb128 0
	.uleb128 .LEHB292-.LFB6605
	.uleb128 .LEHE292-.LEHB292
	.uleb128 .L2824-.LFB6605
	.uleb128 0
	.uleb128 .LEHB293-.LFB6605
	.uleb128 .LEHE293-.LEHB293
	.uleb128 .L2825-.LFB6605
	.uleb128 0
	.uleb128 .LEHB294-.LFB6605
	.uleb128 .LEHE294-.LEHB294
	.uleb128 .L2826-.LFB6605
	.uleb128 0
	.uleb128 .LEHB295-.LFB6605
	.uleb128 .LEHE295-.LEHB295
	.uleb128 .L2827-.LFB6605
	.uleb128 0
	.uleb128 .LEHB296-.LFB6605
	.uleb128 .LEHE296-.LEHB296
	.uleb128 .L2822-.LFB6605
	.uleb128 0
	.uleb128 .LEHB297-.LFB6605
	.uleb128 .LEHE297-.LEHB297
	.uleb128 .L2826-.LFB6605
	.uleb128 0
	.uleb128 .LEHB298-.LFB6605
	.uleb128 .LEHE298-.LEHB298
	.uleb128 .L2823-.LFB6605
	.uleb128 0
	.uleb128 .LEHB299-.LFB6605
	.uleb128 .LEHE299-.LEHB299
	.uleb128 .L2824-.LFB6605
	.uleb128 0
	.uleb128 .LEHB300-.LFB6605
	.uleb128 .LEHE300-.LEHB300
	.uleb128 .L2830-.LFB6605
	.uleb128 0x1
	.uleb128 .LEHB301-.LFB6605
	.uleb128 .LEHE301-.LEHB301
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB302-.LFB6605
	.uleb128 .LEHE302-.LEHB302
	.uleb128 .L2826-.LFB6605
	.uleb128 0
	.uleb128 .LEHB303-.LFB6605
	.uleb128 .LEHE303-.LEHB303
	.uleb128 .L2822-.LFB6605
	.uleb128 0
	.uleb128 .LEHB304-.LFB6605
	.uleb128 .LEHE304-.LEHB304
	.uleb128 .L2824-.LFB6605
	.uleb128 0
.LLSDACSE6605:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATT6605:
	.section	.text._ZN2v88internal6torque11TypeVisitor30ComputeTypeForStructExpressionEPNS1_14TypeExpressionERKSt6vectorIPKNS1_4TypeESaIS8_EE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque11TypeVisitor30ComputeTypeForStructExpressionEPNS1_14TypeExpressionERKSt6vectorIPKNS1_4TypeESaIS8_EE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6605
	.type	_ZN2v88internal6torque11TypeVisitor30ComputeTypeForStructExpressionEPNS1_14TypeExpressionERKSt6vectorIPKNS1_4TypeESaIS8_EE.cold, @function
_ZN2v88internal6torque11TypeVisitor30ComputeTypeForStructExpressionEPNS1_14TypeExpressionERKSt6vectorIPKNS1_4TypeESaIS8_EE.cold:
.LFSB6605:
.L2806:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-304(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2808
	call	_ZdlPv@PLT
.L2808:
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2809
	call	_ZdlPv@PLT
.L2809:
	movq	-248(%rbp), %r12
.L2812:
	testq	%r12, %r12
	jne	.L2916
	movq	-256(%rbp), %rax
	movq	-264(%rbp), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	-264(%rbp), %rdi
	leaq	-216(%rbp), %rax
	movq	$0, -240(%rbp)
	movq	$0, -248(%rbp)
	cmpq	%rax, %rdi
	je	.L2813
	call	_ZdlPv@PLT
.L2813:
	movq	%rbx, %r12
.L2814:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
	movq	-376(%rbp), %rdx
	movq	%rdx, (%rax)
.L2815:
	movq	-336(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2816
	call	_ZdlPv@PLT
.L2816:
	movq	-368(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2776
	call	_ZdlPv@PLT
.L2776:
	movq	-400(%rbp), %rdi
	call	_ZN2v88internal6torque13QualifiedNameD1Ev
	movq	%r12, %rdi
.LEHB305:
	call	_Unwind_Resume@PLT
.L2774:
	movq	-368(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2776
	call	_ZdlPv@PLT
	jmp	.L2776
.L2805:
	leaq	-272(%rbp), %rdi
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EED1Ev
	movq	%r12, %rdi
	call	_Unwind_Resume@PLT
.LEHE305:
.L2755:
	call	__cxa_begin_catch@PLT
.L2758:
	cmpq	%rbx, -408(%rbp)
	jne	.L2917
.LEHB306:
	call	__cxa_rethrow@PLT
.LEHE306:
.L2741:
	movq	%r12, %rdi
	call	_ZN2v88internal6torque14MessageBuilderD1Ev
	movq	%r13, %rdi
.LEHB307:
	call	_Unwind_Resume@PLT
.LEHE307:
.L2917:
	movq	-408(%rbp), %rax
	movq	(%rax), %rdi
	addq	$16, %rax
	cmpq	%rax, %rdi
	je	.L2757
	call	_ZdlPv@PLT
.L2757:
	addq	$32, -408(%rbp)
	jmp	.L2758
.L2916:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	movq	(%r12), %r13
	cmpq	%rax, %rdi
	je	.L2811
	call	_ZdlPv@PLT
.L2811:
	movq	%r12, %rdi
	movq	%r13, %r12
	call	_ZdlPv@PLT
	jmp	.L2812
.L2829:
	endbr64
	movq	%rax, %r12
	call	__cxa_end_catch@PLT
	movq	-272(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2760
	call	_ZdlPv@PLT
.L2760:
	movq	%r12, %rdi
.LEHB308:
	call	_Unwind_Resume@PLT
.LEHE308:
	.cfi_endproc
.LFE6605:
	.section	.gcc_except_table._ZN2v88internal6torque11TypeVisitor30ComputeTypeForStructExpressionEPNS1_14TypeExpressionERKSt6vectorIPKNS1_4TypeESaIS8_EE
	.align 4
.LLSDAC6605:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATTC6605-.LLSDATTDC6605
.LLSDATTDC6605:
	.byte	0x1
	.uleb128 .LLSDACSEC6605-.LLSDACSBC6605
.LLSDACSBC6605:
	.uleb128 .LEHB305-.LCOLDB55
	.uleb128 .LEHE305-.LEHB305
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB306-.LCOLDB55
	.uleb128 .LEHE306-.LEHB306
	.uleb128 .L2829-.LCOLDB55
	.uleb128 0
	.uleb128 .LEHB307-.LCOLDB55
	.uleb128 .LEHE307-.LEHB307
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB308-.LCOLDB55
	.uleb128 .LEHE308-.LEHB308
	.uleb128 0
	.uleb128 0
.LLSDACSEC6605:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATTC6605:
	.section	.text.unlikely._ZN2v88internal6torque11TypeVisitor30ComputeTypeForStructExpressionEPNS1_14TypeExpressionERKSt6vectorIPKNS1_4TypeESaIS8_EE
	.section	.text._ZN2v88internal6torque11TypeVisitor30ComputeTypeForStructExpressionEPNS1_14TypeExpressionERKSt6vectorIPKNS1_4TypeESaIS8_EE
	.size	_ZN2v88internal6torque11TypeVisitor30ComputeTypeForStructExpressionEPNS1_14TypeExpressionERKSt6vectorIPKNS1_4TypeESaIS8_EE, .-_ZN2v88internal6torque11TypeVisitor30ComputeTypeForStructExpressionEPNS1_14TypeExpressionERKSt6vectorIPKNS1_4TypeESaIS8_EE
	.section	.text.unlikely._ZN2v88internal6torque11TypeVisitor30ComputeTypeForStructExpressionEPNS1_14TypeExpressionERKSt6vectorIPKNS1_4TypeESaIS8_EE
	.size	_ZN2v88internal6torque11TypeVisitor30ComputeTypeForStructExpressionEPNS1_14TypeExpressionERKSt6vectorIPKNS1_4TypeESaIS8_EE.cold, .-_ZN2v88internal6torque11TypeVisitor30ComputeTypeForStructExpressionEPNS1_14TypeExpressionERKSt6vectorIPKNS1_4TypeESaIS8_EE.cold
.LCOLDE55:
	.section	.text._ZN2v88internal6torque11TypeVisitor30ComputeTypeForStructExpressionEPNS1_14TypeExpressionERKSt6vectorIPKNS1_4TypeESaIS8_EE
.LHOTE55:
	.section	.text._ZN2v88internal6torque18BuiltinPointerTypeD0Ev,"axG",@progbits,_ZN2v88internal6torque18BuiltinPointerTypeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque18BuiltinPointerTypeD0Ev
	.type	_ZN2v88internal6torque18BuiltinPointerTypeD0Ev, @function
_ZN2v88internal6torque18BuiltinPointerTypeD0Ev:
.LFB12564:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal6torque18BuiltinPointerTypeE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rax, (%rdi)
	movq	72(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L2919
	call	_ZdlPv@PLT
.L2919:
	movq	40(%r12), %r13
	leaq	16+_ZTVN2v88internal6torque4TypeE(%rip), %rax
	leaq	24(%r12), %r14
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L2920
.L2923:
	movq	24(%r13), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r13), %rdi
	leaq	48(%r13), %rax
	movq	16(%r13), %rbx
	cmpq	%rax, %rdi
	je	.L2921
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L2920
.L2922:
	movq	%rbx, %r13
	jmp	.L2923
	.p2align 4,,10
	.p2align 3
.L2921:
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L2922
.L2920:
	popq	%rbx
	movq	%r12, %rdi
	movl	$112, %esi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE12564:
	.size	_ZN2v88internal6torque18BuiltinPointerTypeD0Ev, .-_ZN2v88internal6torque18BuiltinPointerTypeD0Ev
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_15TypeDeclarationE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_15TypeDeclarationE, @function
_GLOBAL__sub_I__ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_15TypeDeclarationE:
.LFB12579:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE12579:
	.size	_GLOBAL__sub_I__ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_15TypeDeclarationE, .-_GLOBAL__sub_I__ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_15TypeDeclarationE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_15TypeDeclarationE
	.weak	_ZTVN2v88internal6torque10IdentifierE
	.section	.data.rel.ro.local._ZTVN2v88internal6torque10IdentifierE,"awG",@progbits,_ZTVN2v88internal6torque10IdentifierE,comdat
	.align 8
	.type	_ZTVN2v88internal6torque10IdentifierE, @object
	.size	_ZTVN2v88internal6torque10IdentifierE, 32
_ZTVN2v88internal6torque10IdentifierE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal6torque10IdentifierD1Ev
	.quad	_ZN2v88internal6torque10IdentifierD0Ev
	.weak	_ZTVN2v88internal6torque13AggregateTypeE
	.section	.data.rel.ro._ZTVN2v88internal6torque13AggregateTypeE,"awG",@progbits,_ZTVN2v88internal6torque13AggregateTypeE,comdat
	.align 8
	.type	_ZTVN2v88internal6torque13AggregateTypeE, @object
	.size	_ZTVN2v88internal6torque13AggregateTypeE, 136
_ZTVN2v88internal6torque13AggregateTypeE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	_ZNK2v88internal6torque4Type11IsSubtypeOfEPKS2_
	.quad	_ZNK2v88internal6torque13AggregateType11MangledNameB5cxx11Ev
	.quad	_ZNK2v88internal6torque4Type11IsConstexprEv
	.quad	_ZNK2v88internal6torque4Type11IsTransientEv
	.quad	_ZNK2v88internal6torque4Type19NonConstexprVersionEv
	.quad	_ZNK2v88internal6torque4Type16ConstexprVersionEv
	.quad	_ZNK2v88internal6torque13AggregateType15GetRuntimeTypesB5cxx11Ev
	.quad	__cxa_pure_virtual
	.quad	_ZNK2v88internal6torque13AggregateType24GetGeneratedTypeNameImplB5cxx11Ev
	.quad	_ZNK2v88internal6torque13AggregateType29GetGeneratedTNodeTypeNameImplB5cxx11Ev
	.quad	__cxa_pure_virtual
	.quad	_ZNK2v88internal6torque13AggregateType15HasIndexedFieldEv
	.quad	_ZN2v88internal6torque13AggregateType13RegisterFieldENS1_5FieldE
	.weak	_ZTVN2v88internal6torque10DeclarableE
	.section	.data.rel.ro.local._ZTVN2v88internal6torque10DeclarableE,"awG",@progbits,_ZTVN2v88internal6torque10DeclarableE,comdat
	.align 8
	.type	_ZTVN2v88internal6torque10DeclarableE, @object
	.size	_ZTVN2v88internal6torque10DeclarableE, 40
_ZTVN2v88internal6torque10DeclarableE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal6torque10DeclarableD1Ev
	.quad	_ZN2v88internal6torque10DeclarableD0Ev
	.quad	_ZNK2v88internal6torque10Declarable9type_nameEv
	.weak	_ZTVN2v88internal6torque5ScopeE
	.section	.data.rel.ro.local._ZTVN2v88internal6torque5ScopeE,"awG",@progbits,_ZTVN2v88internal6torque5ScopeE,comdat
	.align 8
	.type	_ZTVN2v88internal6torque5ScopeE, @object
	.size	_ZTVN2v88internal6torque5ScopeE, 40
_ZTVN2v88internal6torque5ScopeE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal6torque5ScopeD1Ev
	.quad	_ZN2v88internal6torque5ScopeD0Ev
	.quad	_ZNK2v88internal6torque5Scope9type_nameEv
	.weak	_ZTVN2v88internal6torque9NamespaceE
	.section	.data.rel.ro.local._ZTVN2v88internal6torque9NamespaceE,"awG",@progbits,_ZTVN2v88internal6torque9NamespaceE,comdat
	.align 8
	.type	_ZTVN2v88internal6torque9NamespaceE, @object
	.size	_ZTVN2v88internal6torque9NamespaceE, 40
_ZTVN2v88internal6torque9NamespaceE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal6torque9NamespaceD1Ev
	.quad	_ZN2v88internal6torque9NamespaceD0Ev
	.quad	_ZNK2v88internal6torque9Namespace9type_nameEv
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	DW.ref.__gxx_personality_v0
	.weak	DW.ref.__gxx_personality_v0
	.section	.data.rel.local.DW.ref.__gxx_personality_v0,"awG",@progbits,DW.ref.__gxx_personality_v0,comdat
	.align 8
	.type	DW.ref.__gxx_personality_v0, @object
	.size	DW.ref.__gxx_personality_v0, 8
DW.ref.__gxx_personality_v0:
	.quad	__gxx_personality_v0
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
