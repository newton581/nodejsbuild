	.file	"utils.cc"
	.text
	.section	.rodata._ZN2v88internal6torque16IsUpperCamelCaseERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.part.0.str1.1,"aMS",@progbits,1
.LC0:
	.string	"basic_string::substr"
	.section	.rodata._ZN2v88internal6torque16IsUpperCamelCaseERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.part.0.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"%s: __pos (which is %zu) > this->size() (which is %zu)"
	.section	.rodata._ZN2v88internal6torque16IsUpperCamelCaseERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.part.0.str1.1
.LC2:
	.string	"_"
	.section	.text._ZN2v88internal6torque16IsUpperCamelCaseERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.part.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal6torque16IsUpperCamelCaseERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.part.0, @function
_ZN2v88internal6torque16IsUpperCamelCaseERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.part.0:
.LFB7866:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$64, %rsp
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	xorl	%eax, %eax
	cmpb	$95, (%r12)
	sete	%al
	xorl	%r13d, %r13d
	movsbl	(%r12,%rax), %edi
	call	isupper@PLT
	testl	%eax, %eax
	jne	.L16
.L1:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L17
	addq	$64, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	.cfi_restore_state
	movq	8(%rbx), %r13
	testq	%r13, %r13
	je	.L18
	leaq	-64(%rbp), %rbx
	subq	$1, %r13
	leaq	-80(%rbp), %r14
	movq	%rbx, -80(%rbp)
	movq	%r13, -88(%rbp)
	cmpq	$15, %r13
	ja	.L19
	cmpq	$1, %r13
	jne	.L6
	movzbl	1(%r12), %eax
	movb	%al, -64(%rbp)
	movq	%rbx, %rax
.L7:
	movq	%r13, -72(%rbp)
	movb	$0, (%rax,%r13)
	cmpq	$0, -72(%rbp)
	movl	$1, %r13d
	je	.L8
	xorl	%edx, %edx
	movl	$1, %ecx
	leaq	.LC2(%rip), %rsi
	movq	%r14, %rdi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE4findEPKcmm@PLT
	cmpq	$-1, %rax
	sete	%r13b
.L8:
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1
	call	_ZdlPv@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L6:
	testq	%r13, %r13
	jne	.L20
	movq	%rbx, %rax
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L19:
	movq	%r14, %rdi
	leaq	-88(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -80(%rbp)
	movq	%rax, %rdi
	movq	-88(%rbp), %rax
	movq	%rax, -64(%rbp)
.L5:
	movq	%r13, %rdx
	leaq	1(%r12), %rsi
	call	memcpy@PLT
	movq	-88(%rbp), %r13
	movq	-80(%rbp), %rax
	jmp	.L7
.L17:
	call	__stack_chk_fail@PLT
.L18:
	xorl	%ecx, %ecx
	movl	$1, %edx
	leaq	.LC0(%rip), %rsi
	xorl	%eax, %eax
	leaq	.LC1(%rip), %rdi
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L20:
	movq	%rbx, %rdi
	jmp	.L5
	.cfi_endproc
.LFE7866:
	.size	_ZN2v88internal6torque16IsUpperCamelCaseERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.part.0, .-_ZN2v88internal6torque16IsUpperCamelCaseERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.part.0
	.section	.rodata._ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0.str1.1,"aMS",@progbits,1
.LC3:
	.string	"%d"
	.section	.text._ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0,"ax",@progbits
	.p2align 4
	.type	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0, @function
_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0:
.LFB7905:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	subq	$208, %rsp
	.cfi_offset 3, -32
	movq	%r8, -160(%rbp)
	movq	%r9, -152(%rbp)
	testb	%al, %al
	je	.L22
	movaps	%xmm0, -144(%rbp)
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm2, -112(%rbp)
	movaps	%xmm3, -96(%rbp)
	movaps	%xmm4, -80(%rbp)
	movaps	%xmm5, -64(%rbp)
	movaps	%xmm6, -48(%rbp)
	movaps	%xmm7, -32(%rbp)
.L22:
	movq	%fs:40, %rax
	movq	%rax, -200(%rbp)
	xorl	%eax, %eax
	movq	%rsp, %rax
	cmpq	%rax, %rsp
	je	.L24
.L43:
	subq	$4096, %rsp
	orq	$0, 4088(%rsp)
	cmpq	%rax, %rsp
	jne	.L43
.L24:
	subq	$32, %rsp
	orq	$0, 24(%rsp)
	movl	$16, %ecx
	movl	$16, %esi
	movl	$1, %edx
	leaq	.LC3(%rip), %r8
	leaq	15(%rsp), %rbx
	leaq	16(%rbp), %rax
	movl	$32, -224(%rbp)
	andq	$-16, %rbx
	movq	%rax, -216(%rbp)
	leaq	-224(%rbp), %r9
	leaq	-192(%rbp), %rax
	movq	%rbx, %rdi
	movq	%rax, -208(%rbp)
	movl	$48, -220(%rbp)
	call	__vsnprintf_chk@PLT
	leaq	16(%r12), %rcx
	movq	%rcx, (%r12)
	movslq	%eax, %rsi
	cmpl	$1, %eax
	jne	.L26
	movzbl	(%rbx), %eax
	movl	$1, %esi
	movb	%al, 16(%r12)
.L27:
	movq	%rsi, 8(%r12)
	movb	$0, (%rcx,%rsi)
	movq	-200(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L44
	leaq	-16(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	.cfi_restore_state
	cmpl	$8, %eax
	jnb	.L28
	testb	$4, %al
	jne	.L45
	testl	%eax, %eax
	je	.L29
	movzbl	(%rbx), %edx
	movb	%dl, 16(%r12)
	testb	$2, %al
	jne	.L46
.L29:
	movq	(%r12), %rcx
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L28:
	movq	(%rbx), %rdx
	movq	%rdx, 16(%r12)
	movl	%eax, %edx
	movq	-8(%rbx,%rdx), %rdi
	movq	%rdi, -8(%rcx,%rdx)
	leaq	24(%r12), %rdi
	andq	$-8, %rdi
	subq	%rdi, %rcx
	addl	%ecx, %eax
	subq	%rcx, %rbx
	andl	$-8, %eax
	cmpl	$8, %eax
	jb	.L29
	andl	$-8, %eax
	xorl	%edx, %edx
.L32:
	movl	%edx, %ecx
	addl	$8, %edx
	movq	(%rbx,%rcx), %r8
	movq	%r8, (%rdi,%rcx)
	cmpl	%eax, %edx
	jb	.L32
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L45:
	movl	(%rbx), %edx
	movl	%edx, 16(%r12)
	movl	%eax, %edx
	movl	-4(%rbx,%rdx), %eax
	movl	%eax, -4(%rcx,%rdx)
	jmp	.L29
.L46:
	movl	%eax, %edx
	movzwl	-2(%rbx,%rdx), %eax
	movw	%ax, -2(%rcx,%rdx)
	jmp	.L29
.L44:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7905:
	.size	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0, .-_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0
	.section	.text._ZN2v88internal6torque21ContextualVariableTopINS1_14TorqueMessagesEEERPNT_5ScopeEv,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal6torque21ContextualVariableTopINS1_14TorqueMessagesEEERPNT_5ScopeEv
	.type	_ZN2v88internal6torque21ContextualVariableTopINS1_14TorqueMessagesEEERPNT_5ScopeEv, @function
_ZN2v88internal6torque21ContextualVariableTopINS1_14TorqueMessagesEEERPNT_5ScopeEv:
.LFB5538:
	.cfi_startproc
	endbr64
	movq	%fs:0, %rax
	addq	$_ZZN2v88internal6torque21ContextualVariableTopINS1_14TorqueMessagesEEERPNT_5ScopeEvE3top@tpoff, %rax
	ret
	.cfi_endproc
.LFE5538:
	.size	_ZN2v88internal6torque21ContextualVariableTopINS1_14TorqueMessagesEEERPNT_5ScopeEv, .-_ZN2v88internal6torque21ContextualVariableTopINS1_14TorqueMessagesEEERPNT_5ScopeEv
	.section	.rodata._ZN2v88internal6torque23CurrentPositionAsStringB5cxx11Ev.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"basic_string::_M_construct null not valid"
	.section	.rodata._ZN2v88internal6torque23CurrentPositionAsStringB5cxx11Ev.str1.1,"aMS",@progbits,1
.LC5:
	.string	"basic_string::append"
.LC6:
	.string	":"
	.section	.text.unlikely._ZN2v88internal6torque23CurrentPositionAsStringB5cxx11Ev,"ax",@progbits
.LCOLDB7:
	.section	.text._ZN2v88internal6torque23CurrentPositionAsStringB5cxx11Ev,"ax",@progbits
.LHOTB7:
	.p2align 4
	.globl	_ZN2v88internal6torque23CurrentPositionAsStringB5cxx11Ev
	.type	_ZN2v88internal6torque23CurrentPositionAsStringB5cxx11Ev, @function
_ZN2v88internal6torque23CurrentPositionAsStringB5cxx11Ev:
.LFB5543:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA5543
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-192(%rbp), %r15
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$248, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
.LEHB0:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
.LEHE0:
	movq	vsnprintf@GOTPCREL(%rip), %r13
	leaq	-96(%rbp), %rdi
	leaq	.LC3(%rip), %rcx
	movq	(%rax), %rax
	movq	%rdi, -288(%rbp)
	movq	%r13, %rsi
	movl	16(%rax), %edx
	movl	8(%rax), %r8d
	movdqu	(%rax), %xmm0
	movl	4(%rax), %ebx
	xorl	%eax, %eax
	movl	%edx, -240(%rbp)
	addl	$1, %r8d
	movl	$16, %edx
	movaps	%xmm0, -256(%rbp)
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0
	movq	%r15, %rdi
	leal	1(%rbx), %r8d
	movq	%r13, %rsi
	leaq	.LC3(%rip), %rcx
	movl	$16, %edx
	xorl	%eax, %eax
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0
	movl	-256(%rbp), %edi
.LEHB1:
	call	_ZN2v88internal6torque13SourceFileMap14PathFromV8RootB5cxx11ENS1_8SourceIdE@PLT
.LEHE1:
	leaq	-208(%rbp), %rbx
	movq	8(%rax), %r13
	movq	%rbx, -224(%rbp)
	movq	(%rax), %r14
	movq	%r14, %rax
	addq	%r13, %rax
	je	.L49
	testq	%r14, %r14
	je	.L109
.L49:
	movq	%r13, -264(%rbp)
	cmpq	$15, %r13
	ja	.L110
	cmpq	$1, %r13
	jne	.L52
	movzbl	(%r14), %eax
	movb	%al, -208(%rbp)
	movq	%rbx, %rax
.L53:
	movq	%r13, -216(%rbp)
	movb	$0, (%rax,%r13)
	movabsq	$4611686018427387903, %rax
	cmpq	%rax, -216(%rbp)
	je	.L111
	leaq	-224(%rbp), %r13
	movl	$1, %edx
	leaq	.LC6(%rip), %rsi
	movq	%r13, %rdi
.LEHB2:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.LEHE2:
	movq	-224(%rbp), %r9
	movq	-216(%rbp), %r8
	movl	$15, %eax
	movq	-184(%rbp), %rdx
	movq	%rax, %rdi
	movq	-192(%rbp), %rsi
	cmpq	%rbx, %r9
	cmovne	-208(%rbp), %rdi
	leaq	(%r8,%rdx), %rcx
	cmpq	%rdi, %rcx
	jbe	.L112
	leaq	-176(%rbp), %rdi
	cmpq	%rdi, %rsi
	cmovne	-176(%rbp), %rax
	movq	%rdi, -280(%rbp)
	cmpq	%rax, %rcx
	jbe	.L113
.L60:
	movq	%r13, %rdi
.LEHB3:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.LEHE3:
.L62:
	leaq	-144(%rbp), %r14
	leaq	16(%rax), %rdx
	movq	%r14, -160(%rbp)
	movq	(%rax), %rcx
	cmpq	%rdx, %rcx
	je	.L114
	movq	%rcx, -160(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -144(%rbp)
.L64:
	movq	8(%rax), %rcx
	movb	$0, 16(%rax)
	movq	%rcx, -152(%rbp)
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movabsq	$4611686018427387903, %rax
	cmpq	%rax, -152(%rbp)
	je	.L115
	leaq	-160(%rbp), %rdi
	movl	$1, %edx
	leaq	.LC6(%rip), %rsi
.LEHB4:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.LEHE4:
	leaq	-112(%rbp), %r13
	leaq	16(%rax), %rdx
	movq	%r13, -128(%rbp)
	movq	(%rax), %rcx
	cmpq	%rdx, %rcx
	je	.L116
	movq	%rcx, -128(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -112(%rbp)
.L67:
	movq	8(%rax), %rcx
	movb	$0, 16(%rax)
	leaq	-80(%rbp), %r15
	movq	%rcx, -120(%rbp)
	movq	%rdx, (%rax)
	movq	-128(%rbp), %r9
	movq	$0, 8(%rax)
	movl	$15, %eax
	movq	-120(%rbp), %r8
	movq	-88(%rbp), %rdx
	cmpq	%r13, %r9
	movq	%rax, %rdi
	cmovne	-112(%rbp), %rdi
	movq	-96(%rbp), %rsi
	leaq	(%r8,%rdx), %rcx
	cmpq	%rdi, %rcx
	jbe	.L69
	cmpq	%r15, %rsi
	cmovne	-80(%rbp), %rax
	cmpq	%rax, %rcx
	jbe	.L117
.L69:
	leaq	-128(%rbp), %rdi
.LEHB5:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.LEHE5:
.L71:
	leaq	16(%r12), %rdx
	movq	%rdx, (%r12)
	movq	(%rax), %rcx
	leaq	16(%rax), %rdx
	cmpq	%rdx, %rcx
	je	.L118
	movq	%rcx, (%r12)
	movq	16(%rax), %rcx
	movq	%rcx, 16(%r12)
.L73:
	movq	8(%rax), %rcx
	movq	%rdx, (%rax)
	movq	-128(%rbp), %rdi
	movq	$0, 8(%rax)
	movq	%rcx, 8(%r12)
	movb	$0, 16(%rax)
	cmpq	%r13, %rdi
	je	.L74
	call	_ZdlPv@PLT
.L74:
	movq	-160(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L75
	call	_ZdlPv@PLT
.L75:
	movq	-224(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L76
	call	_ZdlPv@PLT
.L76:
	movq	-192(%rbp), %rdi
	cmpq	-280(%rbp), %rdi
	je	.L77
	call	_ZdlPv@PLT
.L77:
	movq	-96(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L48
	call	_ZdlPv@PLT
.L48:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L119
	addq	$248, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L52:
	.cfi_restore_state
	testq	%r13, %r13
	jne	.L120
	movq	%rbx, %rax
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L110:
	leaq	-264(%rbp), %rsi
	leaq	-224(%rbp), %rdi
	xorl	%edx, %edx
.LEHB6:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE6:
	movq	%rax, -224(%rbp)
	movq	%rax, %rdi
	movq	-264(%rbp), %rax
	movq	%rax, -208(%rbp)
.L51:
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-264(%rbp), %r13
	movq	-224(%rbp), %rax
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L112:
	leaq	-176(%rbp), %rax
	movq	%rax, -280(%rbp)
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L114:
	movdqu	16(%rax), %xmm1
	movaps	%xmm1, -144(%rbp)
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L118:
	movdqu	16(%rax), %xmm3
	movups	%xmm3, 16(%r12)
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L116:
	movdqu	16(%rax), %xmm2
	movaps	%xmm2, -112(%rbp)
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L113:
	movq	%r9, %rcx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
.LEHB7:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.LEHE7:
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L117:
	movq	-288(%rbp), %rdi
	movq	%r9, %rcx
	xorl	%edx, %edx
	xorl	%esi, %esi
.LEHB8:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.LEHE8:
	jmp	.L71
.L109:
	leaq	.LC4(%rip), %rdi
.LEHB9:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE9:
.L111:
	leaq	.LC5(%rip), %rdi
.LEHB10:
	call	_ZSt20__throw_length_errorPKc@PLT
.LEHE10:
.L115:
	leaq	.LC5(%rip), %rdi
.LEHB11:
	call	_ZSt20__throw_length_errorPKc@PLT
.LEHE11:
.L119:
	call	__stack_chk_fail@PLT
.L120:
	movq	%rbx, %rdi
	jmp	.L51
.L97:
	endbr64
	movq	%rax, %r12
	jmp	.L57
.L94:
	endbr64
	movq	%rax, %r12
	leaq	-80(%rbp), %r15
	jmp	.L83
.L95:
	endbr64
	movq	%rax, %r12
	leaq	-80(%rbp), %r15
	jmp	.L81
.L93:
	endbr64
	movq	%rax, %r12
	leaq	-176(%rbp), %rax
	leaq	-80(%rbp), %r15
	movq	%rax, -280(%rbp)
	jmp	.L59
.L96:
	endbr64
	movq	%rax, %r12
	jmp	.L79
	.globl	__gxx_personality_v0
	.section	.gcc_except_table._ZN2v88internal6torque23CurrentPositionAsStringB5cxx11Ev,"a",@progbits
.LLSDA5543:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE5543-.LLSDACSB5543
.LLSDACSB5543:
	.uleb128 .LEHB0-.LFB5543
	.uleb128 .LEHE0-.LEHB0
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB1-.LFB5543
	.uleb128 .LEHE1-.LEHB1
	.uleb128 .L93-.LFB5543
	.uleb128 0
	.uleb128 .LEHB2-.LFB5543
	.uleb128 .LEHE2-.LEHB2
	.uleb128 .L97-.LFB5543
	.uleb128 0
	.uleb128 .LEHB3-.LFB5543
	.uleb128 .LEHE3-.LEHB3
	.uleb128 .L94-.LFB5543
	.uleb128 0
	.uleb128 .LEHB4-.LFB5543
	.uleb128 .LEHE4-.LEHB4
	.uleb128 .L95-.LFB5543
	.uleb128 0
	.uleb128 .LEHB5-.LFB5543
	.uleb128 .LEHE5-.LEHB5
	.uleb128 .L96-.LFB5543
	.uleb128 0
	.uleb128 .LEHB6-.LFB5543
	.uleb128 .LEHE6-.LEHB6
	.uleb128 .L93-.LFB5543
	.uleb128 0
	.uleb128 .LEHB7-.LFB5543
	.uleb128 .LEHE7-.LEHB7
	.uleb128 .L94-.LFB5543
	.uleb128 0
	.uleb128 .LEHB8-.LFB5543
	.uleb128 .LEHE8-.LEHB8
	.uleb128 .L96-.LFB5543
	.uleb128 0
	.uleb128 .LEHB9-.LFB5543
	.uleb128 .LEHE9-.LEHB9
	.uleb128 .L93-.LFB5543
	.uleb128 0
	.uleb128 .LEHB10-.LFB5543
	.uleb128 .LEHE10-.LEHB10
	.uleb128 .L97-.LFB5543
	.uleb128 0
	.uleb128 .LEHB11-.LFB5543
	.uleb128 .LEHE11-.LEHB11
	.uleb128 .L95-.LFB5543
	.uleb128 0
.LLSDACSE5543:
	.section	.text._ZN2v88internal6torque23CurrentPositionAsStringB5cxx11Ev
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque23CurrentPositionAsStringB5cxx11Ev
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC5543
	.type	_ZN2v88internal6torque23CurrentPositionAsStringB5cxx11Ev.cold, @function
_ZN2v88internal6torque23CurrentPositionAsStringB5cxx11Ev.cold:
.LFSB5543:
.L57:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-224(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L58
	call	_ZdlPv@PLT
.L58:
	leaq	-176(%rbp), %rax
	leaq	-80(%rbp), %r15
	movq	%rax, -280(%rbp)
.L59:
	movq	-192(%rbp), %rdi
	cmpq	-280(%rbp), %rdi
	je	.L85
	call	_ZdlPv@PLT
.L85:
	movq	-96(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L86
	call	_ZdlPv@PLT
.L86:
	movq	%r12, %rdi
.LEHB12:
	call	_Unwind_Resume@PLT
.LEHE12:
.L79:
	movq	-128(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L81
	call	_ZdlPv@PLT
.L81:
	movq	-160(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L83
	call	_ZdlPv@PLT
.L83:
	movq	-224(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L59
	call	_ZdlPv@PLT
	jmp	.L59
	.cfi_endproc
.LFE5543:
	.section	.gcc_except_table._ZN2v88internal6torque23CurrentPositionAsStringB5cxx11Ev
.LLSDAC5543:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC5543-.LLSDACSBC5543
.LLSDACSBC5543:
	.uleb128 .LEHB12-.LCOLDB7
	.uleb128 .LEHE12-.LEHB12
	.uleb128 0
	.uleb128 0
.LLSDACSEC5543:
	.section	.text.unlikely._ZN2v88internal6torque23CurrentPositionAsStringB5cxx11Ev
	.section	.text._ZN2v88internal6torque23CurrentPositionAsStringB5cxx11Ev
	.size	_ZN2v88internal6torque23CurrentPositionAsStringB5cxx11Ev, .-_ZN2v88internal6torque23CurrentPositionAsStringB5cxx11Ev
	.section	.text.unlikely._ZN2v88internal6torque23CurrentPositionAsStringB5cxx11Ev
	.size	_ZN2v88internal6torque23CurrentPositionAsStringB5cxx11Ev.cold, .-_ZN2v88internal6torque23CurrentPositionAsStringB5cxx11Ev.cold
.LCOLDE7:
	.section	.text._ZN2v88internal6torque23CurrentPositionAsStringB5cxx11Ev
.LHOTE7:
	.section	.text.unlikely._ZN2v88internal6torque14MessageBuilderC2ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_13TorqueMessage4KindE,"ax",@progbits
	.align 2
.LCOLDB8:
	.section	.text._ZN2v88internal6torque14MessageBuilderC2ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_13TorqueMessage4KindE,"ax",@progbits
.LHOTB8:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque14MessageBuilderC2ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_13TorqueMessage4KindE
	.type	_ZN2v88internal6torque14MessageBuilderC2ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_13TorqueMessage4KindE, @function
_ZN2v88internal6torque14MessageBuilderC2ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_13TorqueMessage4KindE:
.LFB5562:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA5562
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	16(%rdi), %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$152, %rsp
	movl	%edx, -184(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r15, (%rdi)
	movq	$0, 8(%rdi)
	movb	$0, 16(%rdi)
	movb	$0, 32(%rdi)
	movb	$0, 36(%rdi)
	movq	$0, -144(%rbp)
	movaps	%xmm0, -160(%rbp)
.LEHB13:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
	cmpq	$0, (%rax)
	je	.L141
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rax
	movb	$1, -177(%rbp)
	movdqu	(%rax), %xmm2
	movl	16(%rax), %eax
	movl	%eax, -140(%rbp)
	movups	%xmm2, -156(%rbp)
.L122:
	movq	(%r12), %r14
	movq	8(%r12), %r12
	leaq	-112(%rbp), %r13
	movq	%r13, -128(%rbp)
	movq	%r14, %rax
	addq	%r12, %rax
	je	.L123
	testq	%r14, %r14
	je	.L157
.L123:
	movq	%r12, -168(%rbp)
	cmpq	$15, %r12
	ja	.L158
	cmpq	$1, %r12
	jne	.L126
	movzbl	(%r14), %eax
	movb	%al, -112(%rbp)
	movq	%r13, %rax
.L127:
	movq	%r12, -120(%rbp)
	movb	$0, (%rax,%r12)
	movzbl	-177(%rbp), %eax
	movq	-128(%rbp), %rdx
	movq	(%rbx), %rdi
	movb	%al, -160(%rbp)
	movq	-144(%rbp), %rax
	movdqa	-160(%rbp), %xmm1
	movq	%rax, -80(%rbp)
	movl	-184(%rbp), %eax
	movaps	%xmm1, -96(%rbp)
	movl	%eax, -72(%rbp)
	cmpq	%r13, %rdx
	je	.L159
	movq	-120(%rbp), %rax
	movq	-112(%rbp), %rcx
	cmpq	%rdi, %r15
	je	.L160
	movq	%rax, %xmm0
	movq	%rcx, %xmm3
	movq	16(%rbx), %rsi
	movq	%rdx, (%rbx)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 8(%rbx)
	testq	%rdi, %rdi
	je	.L133
	movq	%rdi, -128(%rbp)
	movq	%rsi, -112(%rbp)
.L131:
	movq	$0, -120(%rbp)
	movb	$0, (%rdi)
	cmpb	$0, -96(%rbp)
	je	.L134
	cmpb	$0, 32(%rbx)
	je	.L135
	movl	-76(%rbp), %eax
	movdqu	-92(%rbp), %xmm4
	movl	%eax, 52(%rbx)
	movups	%xmm4, 36(%rbx)
.L136:
	movl	-72(%rbp), %eax
	movq	-128(%rbp), %rdi
	movl	%eax, 56(%rbx)
	cmpq	%r13, %rdi
	je	.L121
	call	_ZdlPv@PLT
.L121:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L161
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L126:
	.cfi_restore_state
	testq	%r12, %r12
	jne	.L162
	movq	%r13, %rax
	jmp	.L127
	.p2align 4,,10
	.p2align 3
.L134:
	cmpb	$0, 32(%rbx)
	je	.L136
	movb	$0, 32(%rbx)
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L158:
	leaq	-168(%rbp), %rsi
	leaq	-128(%rbp), %rdi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -128(%rbp)
	movq	%rax, %rdi
	movq	-168(%rbp), %rax
	movq	%rax, -112(%rbp)
.L125:
	movq	%r12, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-168(%rbp), %r12
	movq	-128(%rbp), %rax
	jmp	.L127
	.p2align 4,,10
	.p2align 3
.L141:
	movb	$0, -177(%rbp)
	jmp	.L122
	.p2align 4,,10
	.p2align 3
.L159:
	movq	-120(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L129
	cmpq	$1, %rdx
	je	.L163
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	-120(%rbp), %rdx
	movq	(%rbx), %rdi
.L129:
	movq	%rdx, 8(%rbx)
	movb	$0, (%rdi,%rdx)
	movq	-128(%rbp), %rdi
	jmp	.L131
	.p2align 4,,10
	.p2align 3
.L135:
	movdqu	-92(%rbp), %xmm5
	movl	-76(%rbp), %eax
	movb	$1, 32(%rbx)
	movl	%eax, 52(%rbx)
	movups	%xmm5, 36(%rbx)
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L160:
	movq	%rax, %xmm0
	movq	%rcx, %xmm6
	movq	%rdx, (%rbx)
	punpcklqdq	%xmm6, %xmm0
	movups	%xmm0, 8(%rbx)
.L133:
	movq	%r13, -128(%rbp)
	leaq	-112(%rbp), %r13
	movq	%r13, %rdi
	jmp	.L131
	.p2align 4,,10
	.p2align 3
.L163:
	movzbl	-112(%rbp), %eax
	movb	%al, (%rdi)
	movq	-120(%rbp), %rdx
	movq	(%rbx), %rdi
	jmp	.L129
.L157:
	leaq	.LC4(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE13:
.L161:
	call	__stack_chk_fail@PLT
.L162:
	movq	%r13, %rdi
	jmp	.L125
.L143:
	endbr64
	movq	%rax, %r12
	jmp	.L138
	.section	.gcc_except_table._ZN2v88internal6torque14MessageBuilderC2ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_13TorqueMessage4KindE,"a",@progbits
.LLSDA5562:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE5562-.LLSDACSB5562
.LLSDACSB5562:
	.uleb128 .LEHB13-.LFB5562
	.uleb128 .LEHE13-.LEHB13
	.uleb128 .L143-.LFB5562
	.uleb128 0
.LLSDACSE5562:
	.section	.text._ZN2v88internal6torque14MessageBuilderC2ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_13TorqueMessage4KindE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque14MessageBuilderC2ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_13TorqueMessage4KindE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC5562
	.type	_ZN2v88internal6torque14MessageBuilderC2ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_13TorqueMessage4KindE.cold, @function
_ZN2v88internal6torque14MessageBuilderC2ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_13TorqueMessage4KindE.cold:
.LFSB5562:
.L138:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	(%rbx), %rdi
	cmpq	%rdi, %r15
	je	.L139
	call	_ZdlPv@PLT
.L139:
	movq	%r12, %rdi
.LEHB14:
	call	_Unwind_Resume@PLT
.LEHE14:
	.cfi_endproc
.LFE5562:
	.section	.gcc_except_table._ZN2v88internal6torque14MessageBuilderC2ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_13TorqueMessage4KindE
.LLSDAC5562:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC5562-.LLSDACSBC5562
.LLSDACSBC5562:
	.uleb128 .LEHB14-.LCOLDB8
	.uleb128 .LEHE14-.LEHB14
	.uleb128 0
	.uleb128 0
.LLSDACSEC5562:
	.section	.text.unlikely._ZN2v88internal6torque14MessageBuilderC2ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_13TorqueMessage4KindE
	.section	.text._ZN2v88internal6torque14MessageBuilderC2ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_13TorqueMessage4KindE
	.size	_ZN2v88internal6torque14MessageBuilderC2ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_13TorqueMessage4KindE, .-_ZN2v88internal6torque14MessageBuilderC2ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_13TorqueMessage4KindE
	.section	.text.unlikely._ZN2v88internal6torque14MessageBuilderC2ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_13TorqueMessage4KindE
	.size	_ZN2v88internal6torque14MessageBuilderC2ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_13TorqueMessage4KindE.cold, .-_ZN2v88internal6torque14MessageBuilderC2ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_13TorqueMessage4KindE.cold
.LCOLDE8:
	.section	.text._ZN2v88internal6torque14MessageBuilderC2ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_13TorqueMessage4KindE
.LHOTE8:
	.globl	_ZN2v88internal6torque14MessageBuilderC1ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_13TorqueMessage4KindE
	.set	_ZN2v88internal6torque14MessageBuilderC1ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_13TorqueMessage4KindE,_ZN2v88internal6torque14MessageBuilderC2ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_13TorqueMessage4KindE
	.section	.text.unlikely._ZNK2v88internal6torque14MessageBuilder5ThrowEv,"ax",@progbits
	.align 2
	.globl	_ZNK2v88internal6torque14MessageBuilder5ThrowEv
	.type	_ZNK2v88internal6torque14MessageBuilder5ThrowEv, @function
_ZNK2v88internal6torque14MessageBuilder5ThrowEv:
.LFB5565:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	__cxa_allocate_exception@PLT
	xorl	%edx, %edx
	leaq	_ZTIN2v88internal6torque22TorqueAbortCompilationE(%rip), %rsi
	movq	%rax, %rdi
	call	__cxa_throw@PLT
	.cfi_endproc
.LFE5565:
	.size	_ZNK2v88internal6torque14MessageBuilder5ThrowEv, .-_ZNK2v88internal6torque14MessageBuilder5ThrowEv
	.section	.text._ZN2v88internal6torque16IsLowerCamelCaseERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal6torque16IsLowerCamelCaseERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN2v88internal6torque16IsLowerCamelCaseERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN2v88internal6torque16IsLowerCamelCaseERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB5577:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	xorl	%r13d, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%r12, %r12
	je	.L166
	movq	(%rdi), %r14
	movsbl	(%r14), %edi
	cmpb	$95, %dil
	je	.L184
	call	islower@PLT
	testl	%eax, %eax
	jne	.L175
.L166:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L185
	addq	$72, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L184:
	.cfi_restore_state
	movsbl	1(%r14), %edi
	call	islower@PLT
	testl	%eax, %eax
	je	.L166
	addq	$1, %r14
	subq	$1, %r12
.L175:
	leaq	-80(%rbp), %rbx
	movq	%r12, -104(%rbp)
	leaq	-96(%rbp), %r15
	movq	%rbx, -96(%rbp)
	cmpq	$15, %r12
	ja	.L186
	cmpq	$1, %r12
	jne	.L172
	movzbl	(%r14), %eax
	movb	%al, -80(%rbp)
	movq	%rbx, %rax
.L173:
	movq	%r12, -88(%rbp)
	movl	$1, %r13d
	movb	$0, (%rax,%r12)
	cmpq	$0, -88(%rbp)
	je	.L174
	xorl	%edx, %edx
	movl	$1, %ecx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE4findEPKcmm@PLT
	cmpq	$-1, %rax
	sete	%r13b
.L174:
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L166
	call	_ZdlPv@PLT
	jmp	.L166
	.p2align 4,,10
	.p2align 3
.L186:
	movq	%r15, %rdi
	leaq	-104(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -96(%rbp)
	movq	%rax, %rdi
	movq	-104(%rbp), %rax
	movq	%rax, -80(%rbp)
.L171:
	movq	%r12, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-104(%rbp), %r12
	movq	-96(%rbp), %rax
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L172:
	testq	%r12, %r12
	jne	.L187
	movq	%rbx, %rax
	jmp	.L173
.L185:
	call	__stack_chk_fail@PLT
.L187:
	movq	%rbx, %rdi
	jmp	.L171
	.cfi_endproc
.LFE5577:
	.size	_ZN2v88internal6torque16IsLowerCamelCaseERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN2v88internal6torque16IsLowerCamelCaseERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.text._ZN2v88internal6torque16IsUpperCamelCaseERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal6torque16IsUpperCamelCaseERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN2v88internal6torque16IsUpperCamelCaseERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN2v88internal6torque16IsUpperCamelCaseERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB5578:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	8(%rdi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.L188
	movq	(%rdi), %r13
	xorl	%eax, %eax
	cmpb	$95, 0(%r13)
	sete	%al
	movsbl	0(%r13,%rax), %edi
	call	isupper@PLT
	testl	%eax, %eax
	jne	.L205
.L188:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L206
	addq	$72, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L205:
	.cfi_restore_state
	leaq	-80(%rbp), %r14
	leaq	-1(%rbx), %r12
	movq	%r14, -96(%rbp)
	leaq	-96(%rbp), %r15
	movq	%r12, -104(%rbp)
	cmpq	$15, %r12
	ja	.L207
	cmpq	$1, %r12
	jne	.L193
	movzbl	1(%r13), %eax
	movb	%al, -80(%rbp)
	movq	%r14, %rax
.L194:
	movq	%r12, -88(%rbp)
	movb	$0, (%rax,%r12)
	cmpq	$0, -88(%rbp)
	movl	$1, %r12d
	je	.L195
	xorl	%edx, %edx
	movl	$1, %ecx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE4findEPKcmm@PLT
	cmpq	$-1, %rax
	sete	%r12b
.L195:
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L188
	call	_ZdlPv@PLT
	jmp	.L188
	.p2align 4,,10
	.p2align 3
.L207:
	movq	%r15, %rdi
	leaq	-104(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -96(%rbp)
	movq	%rax, %rdi
	movq	-104(%rbp), %rax
	movq	%rax, -80(%rbp)
.L192:
	movq	%r12, %rdx
	leaq	1(%r13), %rsi
	call	memcpy@PLT
	movq	-104(%rbp), %r12
	movq	-96(%rbp), %rax
	jmp	.L194
	.p2align 4,,10
	.p2align 3
.L193:
	testq	%r12, %r12
	jne	.L208
	movq	%r14, %rax
	jmp	.L194
.L206:
	call	__stack_chk_fail@PLT
.L208:
	movq	%r14, %rdi
	jmp	.L192
	.cfi_endproc
.LFE5578:
	.size	_ZN2v88internal6torque16IsUpperCamelCaseERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN2v88internal6torque16IsUpperCamelCaseERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.text._ZN2v88internal6torque11IsSnakeCaseERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal6torque11IsSnakeCaseERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN2v88internal6torque11IsSnakeCaseERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN2v88internal6torque11IsSnakeCaseERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB5579:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	xorl	%r8d, %r8d
	testq	%rax, %rax
	jne	.L239
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L239:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rax, %rdx
	sarq	$2, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	(%rdi), %rbx
	leaq	(%rbx,%rax), %r12
	testq	%rdx, %rdx
	jle	.L211
	leaq	(%rbx,%rdx,4), %r13
	jmp	.L217
	.p2align 4,,10
	.p2align 3
.L212:
	movsbl	1(%rbx), %edi
	call	isupper@PLT
	testl	%eax, %eax
	jne	.L240
	movsbl	2(%rbx), %edi
	call	isupper@PLT
	testl	%eax, %eax
	jne	.L241
	movsbl	3(%rbx), %edi
	call	isupper@PLT
	testl	%eax, %eax
	jne	.L242
	addq	$4, %rbx
	cmpq	%rbx, %r13
	je	.L243
.L217:
	movsbl	(%rbx), %edi
	call	isupper@PLT
	testl	%eax, %eax
	je	.L212
.L238:
	cmpq	%rbx, %r12
	sete	%r8b
.L209:
	addq	$8, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L243:
	.cfi_restore_state
	movq	%r12, %rax
	subq	%rbx, %rax
.L211:
	cmpq	$2, %rax
	je	.L218
	cmpq	$3, %rax
	je	.L219
	movl	$1, %r8d
	cmpq	$1, %rax
	jne	.L209
.L220:
	movsbl	(%rbx), %edi
	call	isupper@PLT
	movl	$1, %r8d
	testl	%eax, %eax
	je	.L209
	jmp	.L238
	.p2align 4,,10
	.p2align 3
.L240:
	addq	$1, %rbx
	cmpq	%rbx, %r12
	sete	%r8b
	jmp	.L209
	.p2align 4,,10
	.p2align 3
.L241:
	addq	$2, %rbx
	cmpq	%rbx, %r12
	sete	%r8b
	jmp	.L209
	.p2align 4,,10
	.p2align 3
.L242:
	addq	$3, %rbx
	cmpq	%rbx, %r12
	sete	%r8b
	jmp	.L209
	.p2align 4,,10
	.p2align 3
.L219:
	movsbl	(%rbx), %edi
	call	isupper@PLT
	testl	%eax, %eax
	jne	.L238
	addq	$1, %rbx
.L218:
	movsbl	(%rbx), %edi
	call	isupper@PLT
	testl	%eax, %eax
	jne	.L238
	addq	$1, %rbx
	jmp	.L220
	.cfi_endproc
.LFE5579:
	.size	_ZN2v88internal6torque11IsSnakeCaseERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN2v88internal6torque11IsSnakeCaseERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.rodata._ZN2v88internal6torque25IsValidNamespaceConstNameERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.str1.1,"aMS",@progbits,1
.LC9:
	.string	"True"
.LC10:
	.string	"False"
.LC11:
	.string	"TheHole"
.LC12:
	.string	"Null"
.LC13:
	.string	"Undefined"
	.section	.text.unlikely._ZN2v88internal6torque25IsValidNamespaceConstNameERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"ax",@progbits
.LCOLDB14:
	.section	.text._ZN2v88internal6torque25IsValidNamespaceConstNameERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"ax",@progbits
.LHOTB14:
	.p2align 4
	.globl	_ZN2v88internal6torque25IsValidNamespaceConstNameERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN2v88internal6torque25IsValidNamespaceConstNameERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN2v88internal6torque25IsValidNamespaceConstNameERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB5580:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA5580
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	xorl	%r12d, %r12d
	pushq	%rbx
	subq	$64, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 8(%rdi)
	jne	.L280
.L244:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L281
	addq	$64, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L280:
	.cfi_restore_state
	leaq	.LC9(%rip), %rsi
	movq	%rdi, %rbx
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	je	.L246
	leaq	.LC10(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	je	.L246
	leaq	.LC11(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	je	.L246
	leaq	.LC12(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	je	.L246
	leaq	.LC13(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	je	.L246
	movq	(%rbx), %r13
	cmpb	$107, 0(%r13)
	jne	.L244
	movq	8(%rbx), %r12
	testq	%r12, %r12
	je	.L282
	leaq	-64(%rbp), %rbx
	subq	$1, %r12
	leaq	-80(%rbp), %r14
	movq	%rbx, -80(%rbp)
	movq	%r12, -88(%rbp)
	cmpq	$15, %r12
	ja	.L283
	cmpq	$1, %r12
	jne	.L251
	movzbl	1(%r13), %eax
	movb	%al, -64(%rbp)
	movq	%rbx, %rax
.L252:
	movq	%r12, -72(%rbp)
	movb	$0, (%rax,%r12)
	cmpq	$0, -72(%rbp)
	je	.L261
	movq	%r14, %rdi
.LEHB15:
	call	_ZN2v88internal6torque16IsUpperCamelCaseERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.part.0
.LEHE15:
	movl	%eax, %r12d
.L253:
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L244
	call	_ZdlPv@PLT
	jmp	.L244
	.p2align 4,,10
	.p2align 3
.L246:
	movl	$1, %r12d
	jmp	.L244
	.p2align 4,,10
	.p2align 3
.L261:
	xorl	%r12d, %r12d
	jmp	.L253
	.p2align 4,,10
	.p2align 3
.L251:
	testq	%r12, %r12
	jne	.L284
	movq	%rbx, %rax
	jmp	.L252
	.p2align 4,,10
	.p2align 3
.L283:
	movq	%r14, %rdi
	leaq	-88(%rbp), %rsi
	xorl	%edx, %edx
.LEHB16:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -80(%rbp)
	movq	%rax, %rdi
	movq	-88(%rbp), %rax
	movq	%rax, -64(%rbp)
.L250:
	movq	%r12, %rdx
	leaq	1(%r13), %rsi
	call	memcpy@PLT
	movq	-88(%rbp), %r12
	movq	-80(%rbp), %rax
	jmp	.L252
.L281:
	call	__stack_chk_fail@PLT
.L282:
	xorl	%ecx, %ecx
	movl	$1, %edx
	leaq	.LC0(%rip), %rsi
	xorl	%eax, %eax
	leaq	.LC1(%rip), %rdi
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.LEHE16:
.L284:
	movq	%rbx, %rdi
	jmp	.L250
.L262:
	endbr64
	movq	%rax, %r12
	jmp	.L255
	.section	.gcc_except_table._ZN2v88internal6torque25IsValidNamespaceConstNameERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"a",@progbits
.LLSDA5580:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE5580-.LLSDACSB5580
.LLSDACSB5580:
	.uleb128 .LEHB15-.LFB5580
	.uleb128 .LEHE15-.LEHB15
	.uleb128 .L262-.LFB5580
	.uleb128 0
	.uleb128 .LEHB16-.LFB5580
	.uleb128 .LEHE16-.LEHB16
	.uleb128 0
	.uleb128 0
.LLSDACSE5580:
	.section	.text._ZN2v88internal6torque25IsValidNamespaceConstNameERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque25IsValidNamespaceConstNameERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC5580
	.type	_ZN2v88internal6torque25IsValidNamespaceConstNameERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.cold, @function
_ZN2v88internal6torque25IsValidNamespaceConstNameERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.cold:
.LFSB5580:
.L255:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L256
	call	_ZdlPv@PLT
.L256:
	movq	%r12, %rdi
.LEHB17:
	call	_Unwind_Resume@PLT
.LEHE17:
	.cfi_endproc
.LFE5580:
	.section	.gcc_except_table._ZN2v88internal6torque25IsValidNamespaceConstNameERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
.LLSDAC5580:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC5580-.LLSDACSBC5580
.LLSDACSBC5580:
	.uleb128 .LEHB17-.LCOLDB14
	.uleb128 .LEHE17-.LEHB17
	.uleb128 0
	.uleb128 0
.LLSDACSEC5580:
	.section	.text.unlikely._ZN2v88internal6torque25IsValidNamespaceConstNameERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.text._ZN2v88internal6torque25IsValidNamespaceConstNameERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.size	_ZN2v88internal6torque25IsValidNamespaceConstNameERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN2v88internal6torque25IsValidNamespaceConstNameERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.text.unlikely._ZN2v88internal6torque25IsValidNamespaceConstNameERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.size	_ZN2v88internal6torque25IsValidNamespaceConstNameERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.cold, .-_ZN2v88internal6torque25IsValidNamespaceConstNameERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.cold
.LCOLDE14:
	.section	.text._ZN2v88internal6torque25IsValidNamespaceConstNameERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
.LHOTE14:
	.section	.text._ZN2v88internal6torque15IsValidTypeNameERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal6torque15IsValidTypeNameERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN2v88internal6torque15IsValidTypeNameERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN2v88internal6torque15IsValidTypeNameERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB5581:
	.cfi_startproc
	endbr64
	cmpq	$0, 8(%rdi)
	jne	.L295
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L295:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	leaq	_ZZN2v88internal6torque12_GLOBAL__N_113IsMachineTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE13machine_types(%rip), %rbx
	leaq	160(%rbx), %r13
	subq	$8, %rsp
	.p2align 4,,10
	.p2align 3
.L286:
	movq	(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	je	.L302
	movq	8(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	je	.L303
	movq	16(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	je	.L304
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	je	.L305
	addq	$32, %rbx
	cmpq	%r13, %rbx
	jne	.L286
.L293:
	cmpq	$0, 8(%r12)
	je	.L294
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal6torque16IsUpperCamelCaseERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.part.0
	.p2align 4,,10
	.p2align 3
.L294:
	.cfi_restore_state
	xorl	%eax, %eax
.L285:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L302:
	.cfi_restore_state
	movl	$1, %eax
	jmp	.L285
	.p2align 4,,10
	.p2align 3
.L303:
	addq	$8, %rbx
.L290:
	leaq	160+_ZZN2v88internal6torque12_GLOBAL__N_113IsMachineTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE13machine_types(%rip), %rdx
	movl	$1, %eax
	cmpq	%rdx, %rbx
	je	.L293
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L304:
	.cfi_restore_state
	addq	$16, %rbx
	jmp	.L290
	.p2align 4,,10
	.p2align 3
.L305:
	addq	$24, %rbx
	jmp	.L290
	.cfi_endproc
.LFE5581:
	.size	_ZN2v88internal6torque15IsValidTypeNameERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN2v88internal6torque15IsValidTypeNameERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.text.unlikely._ZN2v88internal6torque27CapifyStringWithUnderscoresERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"ax",@progbits
.LCOLDB15:
	.section	.text._ZN2v88internal6torque27CapifyStringWithUnderscoresERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"ax",@progbits
.LHOTB15:
	.p2align 4
	.globl	_ZN2v88internal6torque27CapifyStringWithUnderscoresERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN2v88internal6torque27CapifyStringWithUnderscoresERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN2v88internal6torque27CapifyStringWithUnderscoresERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB5582:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA5582
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16(%rdi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%rax, (%rdi)
	movq	$0, 8(%rdi)
	movb	$0, 16(%rdi)
	movq	(%rsi), %r14
	movq	%rax, -72(%rbp)
	movq	8(%rsi), %rax
	addq	%r14, %rax
	movq	%rax, -64(%rbp)
	cmpq	%r14, %rax
	je	.L306
	movsbl	(%r14), %r13d
	jmp	.L309
	.p2align 4,,10
	.p2align 3
.L319:
	movzbl	-52(%rbp), %esi
	movl	%r13d, %edi
	movb	%sil, (%rax,%rbx)
	movq	(%r12), %rax
	movq	%r15, 8(%r12)
	movb	$0, 1(%rax,%rbx)
	leaq	1(%r14), %rbx
	call	islower@PLT
	cmpq	%rbx, -64(%rbp)
	je	.L306
	movsbl	1(%r14), %r13d
	testl	%eax, %eax
	je	.L311
	movsbl	%r13b, %edi
	call	isupper@PLT
	testl	%eax, %eax
	jne	.L327
.L311:
	movq	%rbx, %r14
.L309:
	leal	-45(%r13), %eax
	cmpb	$1, %al
	jbe	.L328
.L313:
	movl	%r13d, %edi
	call	toupper@PLT
	movq	8(%r12), %rbx
	movl	%eax, -52(%rbp)
	movq	(%r12), %rax
	leaq	1(%rbx), %r15
	cmpq	%rax, -72(%rbp)
	je	.L323
	movq	16(%r12), %rdx
.L318:
	cmpq	%rdx, %r15
	jbe	.L319
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
.LEHB18:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%r12), %rax
	jmp	.L319
	.p2align 4,,10
	.p2align 3
.L328:
	leaq	1(%r14), %rbx
.L317:
	movabsq	$4611686018427387903, %rax
	cmpq	%rax, 8(%r12)
	je	.L329
	movl	$1, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	%rbx, %r14
	cmpq	%rbx, -64(%rbp)
	je	.L306
	movsbl	(%rbx), %r13d
	addq	$1, %rbx
	leal	-45(%r13), %eax
	cmpb	$1, %al
	ja	.L313
	jmp	.L317
	.p2align 4,,10
	.p2align 3
.L327:
	movabsq	$4611686018427387903, %rax
	cmpq	%rax, 8(%r12)
	je	.L330
	movl	$1, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	jmp	.L311
	.p2align 4,,10
	.p2align 3
.L323:
	movl	$15, %edx
	jmp	.L318
	.p2align 4,,10
	.p2align 3
.L306:
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L329:
	.cfi_restore_state
	leaq	.LC5(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L330:
	leaq	.LC5(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.LEHE18:
.L324:
	endbr64
	movq	%rax, %r13
	jmp	.L321
	.section	.gcc_except_table._ZN2v88internal6torque27CapifyStringWithUnderscoresERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"a",@progbits
.LLSDA5582:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE5582-.LLSDACSB5582
.LLSDACSB5582:
	.uleb128 .LEHB18-.LFB5582
	.uleb128 .LEHE18-.LEHB18
	.uleb128 .L324-.LFB5582
	.uleb128 0
.LLSDACSE5582:
	.section	.text._ZN2v88internal6torque27CapifyStringWithUnderscoresERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque27CapifyStringWithUnderscoresERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC5582
	.type	_ZN2v88internal6torque27CapifyStringWithUnderscoresERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.cold, @function
_ZN2v88internal6torque27CapifyStringWithUnderscoresERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.cold:
.LFSB5582:
.L321:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	(%r12), %rdi
	cmpq	%rdi, -72(%rbp)
	je	.L322
	call	_ZdlPv@PLT
.L322:
	movq	%r13, %rdi
.LEHB19:
	call	_Unwind_Resume@PLT
.LEHE19:
	.cfi_endproc
.LFE5582:
	.section	.gcc_except_table._ZN2v88internal6torque27CapifyStringWithUnderscoresERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
.LLSDAC5582:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC5582-.LLSDACSBC5582
.LLSDACSBC5582:
	.uleb128 .LEHB19-.LCOLDB15
	.uleb128 .LEHE19-.LEHB19
	.uleb128 0
	.uleb128 0
.LLSDACSEC5582:
	.section	.text.unlikely._ZN2v88internal6torque27CapifyStringWithUnderscoresERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.text._ZN2v88internal6torque27CapifyStringWithUnderscoresERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.size	_ZN2v88internal6torque27CapifyStringWithUnderscoresERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN2v88internal6torque27CapifyStringWithUnderscoresERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.text.unlikely._ZN2v88internal6torque27CapifyStringWithUnderscoresERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.size	_ZN2v88internal6torque27CapifyStringWithUnderscoresERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.cold, .-_ZN2v88internal6torque27CapifyStringWithUnderscoresERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.cold
.LCOLDE15:
	.section	.text._ZN2v88internal6torque27CapifyStringWithUnderscoresERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
.LHOTE15:
	.section	.text.unlikely._ZN2v88internal6torque14CamelifyStringERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"ax",@progbits
.LCOLDB16:
	.section	.text._ZN2v88internal6torque14CamelifyStringERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"ax",@progbits
.LHOTB16:
	.p2align 4
	.globl	_ZN2v88internal6torque14CamelifyStringERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN2v88internal6torque14CamelifyStringERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN2v88internal6torque14CamelifyStringERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB5583:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA5583
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16(%rdi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	%rax, (%rdi)
	movq	$0, 8(%rdi)
	movb	$0, 16(%rdi)
	movq	(%rsi), %r13
	movq	%rax, -64(%rbp)
	movq	8(%rsi), %rax
	addq	%r13, %rax
	movq	%rax, -56(%rbp)
	cmpq	%r13, %rax
	je	.L331
	movl	$1, %ecx
	jmp	.L337
	.p2align 4,,10
	.p2align 3
.L351:
	testb	%cl, %cl
	je	.L334
	movsbl	%bl, %edi
	call	toupper@PLT
	movl	%eax, %ebx
.L334:
	movq	8(%r12), %r14
	movq	(%r12), %rax
	leaq	1(%r14), %r15
	cmpq	%rax, -64(%rbp)
	je	.L342
	movq	16(%r12), %rdx
.L335:
	cmpq	%rdx, %r15
	ja	.L350
.L336:
	movb	%bl, (%rax,%r14)
	movq	(%r12), %rax
	xorl	%ecx, %ecx
	addq	$1, %r13
	movq	%r15, 8(%r12)
	movb	$0, 1(%rax,%r14)
	cmpq	%r13, -56(%rbp)
	je	.L331
.L337:
	movzbl	0(%r13), %ebx
	cmpb	$95, %bl
	sete	%al
	cmpb	$45, %bl
	sete	%dl
	orb	%dl, %al
	je	.L351
	movl	%eax, %ecx
	addq	$1, %r13
	cmpq	%r13, -56(%rbp)
	jne	.L337
.L331:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L350:
	.cfi_restore_state
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
.LEHB20:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
.LEHE20:
	movq	(%r12), %rax
	jmp	.L336
	.p2align 4,,10
	.p2align 3
.L342:
	movl	$15, %edx
	jmp	.L335
.L343:
	endbr64
	movq	%rax, %r13
	jmp	.L339
	.section	.gcc_except_table._ZN2v88internal6torque14CamelifyStringERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"a",@progbits
.LLSDA5583:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE5583-.LLSDACSB5583
.LLSDACSB5583:
	.uleb128 .LEHB20-.LFB5583
	.uleb128 .LEHE20-.LEHB20
	.uleb128 .L343-.LFB5583
	.uleb128 0
.LLSDACSE5583:
	.section	.text._ZN2v88internal6torque14CamelifyStringERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque14CamelifyStringERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC5583
	.type	_ZN2v88internal6torque14CamelifyStringERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.cold, @function
_ZN2v88internal6torque14CamelifyStringERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.cold:
.LFSB5583:
.L339:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	(%r12), %rdi
	cmpq	%rdi, -64(%rbp)
	je	.L340
	call	_ZdlPv@PLT
.L340:
	movq	%r13, %rdi
.LEHB21:
	call	_Unwind_Resume@PLT
.LEHE21:
	.cfi_endproc
.LFE5583:
	.section	.gcc_except_table._ZN2v88internal6torque14CamelifyStringERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
.LLSDAC5583:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC5583-.LLSDACSBC5583
.LLSDACSBC5583:
	.uleb128 .LEHB21-.LCOLDB16
	.uleb128 .LEHE21-.LEHB21
	.uleb128 0
	.uleb128 0
.LLSDACSEC5583:
	.section	.text.unlikely._ZN2v88internal6torque14CamelifyStringERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.text._ZN2v88internal6torque14CamelifyStringERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.size	_ZN2v88internal6torque14CamelifyStringERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN2v88internal6torque14CamelifyStringERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.text.unlikely._ZN2v88internal6torque14CamelifyStringERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.size	_ZN2v88internal6torque14CamelifyStringERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.cold, .-_ZN2v88internal6torque14CamelifyStringERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.cold
.LCOLDE16:
	.section	.text._ZN2v88internal6torque14CamelifyStringERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
.LHOTE16:
	.section	.text.unlikely._ZN2v88internal6torque14SnakeifyStringERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"ax",@progbits
.LCOLDB17:
	.section	.text._ZN2v88internal6torque14SnakeifyStringERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"ax",@progbits
.LHOTB17:
	.p2align 4
	.globl	_ZN2v88internal6torque14SnakeifyStringERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN2v88internal6torque14SnakeifyStringERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN2v88internal6torque14SnakeifyStringERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB5584:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA5584
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16(%rdi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%rax, (%rdi)
	movq	$0, 8(%rdi)
	movb	$0, 16(%rdi)
	movq	8(%rsi), %rdx
	movq	%rax, -64(%rbp)
	movq	(%rsi), %rax
	addq	%rax, %rdx
	movq	%rdx, -72(%rbp)
	cmpq	%rax, %rdx
	je	.L352
	movsbl	(%rax), %r14d
	leaq	1(%rax), %r13
	jmp	.L362
	.p2align 4,,10
	.p2align 3
.L357:
	movzbl	-52(%rbp), %esi
	movl	%r14d, %edi
	movb	%sil, (%rax,%rbx)
	movq	(%r12), %rax
	movq	%r15, 8(%r12)
	movb	$0, 1(%rax,%rbx)
	call	islower@PLT
	cmpq	%r13, -72(%rbp)
	je	.L352
	movsbl	0(%r13), %r14d
	testl	%eax, %eax
	je	.L354
	movl	%r14d, %edi
	call	isupper@PLT
	testl	%eax, %eax
	jne	.L372
.L354:
	addq	$1, %r13
.L362:
	movl	%r14d, %edi
	call	tolower@PLT
	movq	8(%r12), %rbx
	movl	%eax, -52(%rbp)
	movq	(%r12), %rax
	leaq	1(%rbx), %r15
	cmpq	%rax, -64(%rbp)
	je	.L363
	movq	16(%r12), %rdx
.L356:
	cmpq	%rdx, %r15
	jbe	.L357
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
.LEHB22:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%r12), %rax
	jmp	.L357
	.p2align 4,,10
	.p2align 3
.L372:
	movabsq	$4611686018427387903, %rax
	cmpq	%rax, 8(%r12)
	je	.L373
	movl	$1, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	jmp	.L354
	.p2align 4,,10
	.p2align 3
.L363:
	movl	$15, %edx
	jmp	.L356
	.p2align 4,,10
	.p2align 3
.L352:
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L373:
	.cfi_restore_state
	leaq	.LC5(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.LEHE22:
.L364:
	endbr64
	movq	%rax, %r13
	jmp	.L360
	.section	.gcc_except_table._ZN2v88internal6torque14SnakeifyStringERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"a",@progbits
.LLSDA5584:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE5584-.LLSDACSB5584
.LLSDACSB5584:
	.uleb128 .LEHB22-.LFB5584
	.uleb128 .LEHE22-.LEHB22
	.uleb128 .L364-.LFB5584
	.uleb128 0
.LLSDACSE5584:
	.section	.text._ZN2v88internal6torque14SnakeifyStringERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque14SnakeifyStringERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC5584
	.type	_ZN2v88internal6torque14SnakeifyStringERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.cold, @function
_ZN2v88internal6torque14SnakeifyStringERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.cold:
.LFSB5584:
.L360:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	(%r12), %rdi
	cmpq	%rdi, -64(%rbp)
	je	.L361
	call	_ZdlPv@PLT
.L361:
	movq	%r13, %rdi
.LEHB23:
	call	_Unwind_Resume@PLT
.LEHE23:
	.cfi_endproc
.LFE5584:
	.section	.gcc_except_table._ZN2v88internal6torque14SnakeifyStringERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
.LLSDAC5584:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC5584-.LLSDACSBC5584
.LLSDACSBC5584:
	.uleb128 .LEHB23-.LCOLDB17
	.uleb128 .LEHE23-.LEHB23
	.uleb128 0
	.uleb128 0
.LLSDACSEC5584:
	.section	.text.unlikely._ZN2v88internal6torque14SnakeifyStringERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.text._ZN2v88internal6torque14SnakeifyStringERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.size	_ZN2v88internal6torque14SnakeifyStringERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN2v88internal6torque14SnakeifyStringERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.text.unlikely._ZN2v88internal6torque14SnakeifyStringERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.size	_ZN2v88internal6torque14SnakeifyStringERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.cold, .-_ZN2v88internal6torque14SnakeifyStringERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.cold
.LCOLDE17:
	.section	.text._ZN2v88internal6torque14SnakeifyStringERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
.LHOTE17:
	.section	.text._ZN2v88internal6torque13DashifyStringERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal6torque13DashifyStringERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN2v88internal6torque13DashifyStringERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN2v88internal6torque13DashifyStringERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB5585:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	addq	$16, %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%rdi, (%r12)
	movq	(%rsi), %r14
	movq	8(%rsi), %r13
	movq	%r14, %rax
	addq	%r13, %rax
	je	.L375
	testq	%r14, %r14
	je	.L397
.L375:
	movq	%r13, -48(%rbp)
	cmpq	$15, %r13
	ja	.L398
	cmpq	$1, %r13
	jne	.L378
	movzbl	(%r14), %eax
	movb	%al, 16(%r12)
.L379:
	movq	%r13, 8(%r12)
	movb	$0, (%rdi,%r13)
	movq	(%r12), %rax
	movq	8(%r12), %rdx
	addq	%rax, %rdx
	cmpq	%rdx, %rax
	je	.L374
	.p2align 4,,10
	.p2align 3
.L382:
	cmpb	$95, (%rax)
	jne	.L381
	movb	$45, (%rax)
.L381:
	addq	$1, %rax
	cmpq	%rax, %rdx
	jne	.L382
.L374:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L399
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L378:
	.cfi_restore_state
	testq	%r13, %r13
	je	.L379
	jmp	.L377
	.p2align 4,,10
	.p2align 3
.L398:
	movq	%r12, %rdi
	leaq	-48(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%r12)
	movq	%rax, %rdi
	movq	-48(%rbp), %rax
	movq	%rax, 16(%r12)
.L377:
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-48(%rbp), %r13
	movq	(%r12), %rdi
	jmp	.L379
.L399:
	call	__stack_chk_fail@PLT
.L397:
	leaq	.LC4(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
	.cfi_endproc
.LFE5585:
	.size	_ZN2v88internal6torque13DashifyStringERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN2v88internal6torque13DashifyStringERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.text._ZN2v88internal6torque15UnderlinifyPathENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal6torque15UnderlinifyPathENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN2v88internal6torque15UnderlinifyPathENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN2v88internal6torque15UnderlinifyPathENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB5586:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rax
	movq	8(%rsi), %rdx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	%rax, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	cmpq	%rdx, %rax
	je	.L412
	.p2align 4,,10
	.p2align 3
.L403:
	cmpb	$45, (%rax)
	jne	.L402
	movb	$95, (%rax)
.L402:
	addq	$1, %rax
	cmpq	%rax, %rdx
	jne	.L403
	movq	0(%r13), %rax
	movq	8(%r13), %rdx
	addq	%rax, %rdx
	cmpq	%rdx, %rax
	je	.L412
	.p2align 4,,10
	.p2align 3
.L405:
	cmpb	$47, (%rax)
	jne	.L404
	movb	$95, (%rax)
.L404:
	addq	$1, %rax
	cmpq	%rax, %rdx
	jne	.L405
	movq	0(%r13), %rax
	movq	8(%r13), %rdx
	addq	%rax, %rdx
	cmpq	%rdx, %rax
	je	.L412
	.p2align 4,,10
	.p2align 3
.L408:
	cmpb	$92, (%rax)
	jne	.L407
	movb	$95, (%rax)
.L407:
	addq	$1, %rax
	cmpq	%rax, %rdx
	jne	.L408
	movq	0(%r13), %rax
	movq	8(%r13), %rdx
	addq	%rax, %rdx
	cmpq	%rdx, %rax
	je	.L412
	.p2align 4,,10
	.p2align 3
.L411:
	cmpb	$46, (%rax)
	jne	.L410
	movb	$95, (%rax)
.L410:
	addq	$1, %rax
	cmpq	%rax, %rdx
	jne	.L411
	movq	0(%r13), %rbx
	movq	8(%r13), %r14
	addq	%rbx, %r14
	cmpq	%rbx, %r14
	je	.L412
	.p2align 4,,10
	.p2align 3
.L413:
	movsbl	(%rbx), %edi
	addq	$1, %rbx
	call	toupper@PLT
	movb	%al, -1(%rbx)
	cmpq	%rbx, %r14
	jne	.L413
.L412:
	leaq	16(%r12), %rax
	movq	%rax, (%r12)
	movq	0(%r13), %rdx
	leaq	16(%r13), %rax
	cmpq	%rax, %rdx
	je	.L425
	movq	%rdx, (%r12)
	movq	16(%r13), %rdx
	movq	%rdx, 16(%r12)
.L414:
	movq	8(%r13), %rdx
	popq	%rbx
	movq	%rax, 0(%r13)
	movq	%r12, %rax
	movq	$0, 8(%r13)
	movq	%rdx, 8(%r12)
	popq	%r12
	movb	$0, 16(%r13)
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L425:
	.cfi_restore_state
	movdqu	16(%r13), %xmm0
	movups	%xmm0, 16(%r12)
	jmp	.L414
	.cfi_endproc
.LFE5586:
	.size	_ZN2v88internal6torque15UnderlinifyPathENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN2v88internal6torque15UnderlinifyPathENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.rodata._ZN2v88internal6torque10IfDefScopeC2ERSoNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.str1.1,"aMS",@progbits,1
.LC18:
	.string	"#ifdef "
.LC19:
	.string	"\n"
	.section	.text.unlikely._ZN2v88internal6torque10IfDefScopeC2ERSoNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"ax",@progbits
	.align 2
.LCOLDB20:
	.section	.text._ZN2v88internal6torque10IfDefScopeC2ERSoNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"ax",@progbits
.LHOTB20:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque10IfDefScopeC2ERSoNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN2v88internal6torque10IfDefScopeC2ERSoNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN2v88internal6torque10IfDefScopeC2ERSoNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB5592:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA5592
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16(%rdx), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	24(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdx), %rcx
	movq	%rsi, (%rdi)
	movq	%r13, 8(%rdi)
	cmpq	%rax, %rcx
	je	.L434
	movq	%rcx, 8(%rdi)
	movq	16(%rdx), %rcx
	movq	%rcx, 24(%rdi)
.L428:
	movq	8(%rdx), %rcx
	movq	%rax, (%rdx)
	leaq	.LC18(%rip), %rsi
	movq	%r12, %rdi
	movq	$0, 8(%rdx)
	movb	$0, 16(%rdx)
	movl	$7, %edx
	movq	%rcx, 16(%rbx)
.LEHB24:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	16(%rbx), %rdx
	movq	8(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rax, %rdi
	movl	$1, %edx
	leaq	.LC19(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE24:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L434:
	.cfi_restore_state
	movdqu	16(%rdx), %xmm0
	movups	%xmm0, 24(%rdi)
	jmp	.L428
.L431:
	endbr64
	movq	%rax, %r12
	jmp	.L429
	.section	.gcc_except_table._ZN2v88internal6torque10IfDefScopeC2ERSoNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"a",@progbits
.LLSDA5592:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE5592-.LLSDACSB5592
.LLSDACSB5592:
	.uleb128 .LEHB24-.LFB5592
	.uleb128 .LEHE24-.LEHB24
	.uleb128 .L431-.LFB5592
	.uleb128 0
.LLSDACSE5592:
	.section	.text._ZN2v88internal6torque10IfDefScopeC2ERSoNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque10IfDefScopeC2ERSoNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC5592
	.type	_ZN2v88internal6torque10IfDefScopeC2ERSoNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.cold, @function
_ZN2v88internal6torque10IfDefScopeC2ERSoNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.cold:
.LFSB5592:
.L429:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movq	8(%rbx), %rdi
	cmpq	%rdi, %r13
	je	.L430
	call	_ZdlPv@PLT
.L430:
	movq	%r12, %rdi
.LEHB25:
	call	_Unwind_Resume@PLT
.LEHE25:
	.cfi_endproc
.LFE5592:
	.section	.gcc_except_table._ZN2v88internal6torque10IfDefScopeC2ERSoNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
.LLSDAC5592:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC5592-.LLSDACSBC5592
.LLSDACSBC5592:
	.uleb128 .LEHB25-.LCOLDB20
	.uleb128 .LEHE25-.LEHB25
	.uleb128 0
	.uleb128 0
.LLSDACSEC5592:
	.section	.text.unlikely._ZN2v88internal6torque10IfDefScopeC2ERSoNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.text._ZN2v88internal6torque10IfDefScopeC2ERSoNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.size	_ZN2v88internal6torque10IfDefScopeC2ERSoNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN2v88internal6torque10IfDefScopeC2ERSoNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.text.unlikely._ZN2v88internal6torque10IfDefScopeC2ERSoNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.size	_ZN2v88internal6torque10IfDefScopeC2ERSoNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.cold, .-_ZN2v88internal6torque10IfDefScopeC2ERSoNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.cold
.LCOLDE20:
	.section	.text._ZN2v88internal6torque10IfDefScopeC2ERSoNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
.LHOTE20:
	.globl	_ZN2v88internal6torque10IfDefScopeC1ERSoNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.set	_ZN2v88internal6torque10IfDefScopeC1ERSoNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,_ZN2v88internal6torque10IfDefScopeC2ERSoNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.rodata._ZN2v88internal6torque10IfDefScopeD2Ev.str1.1,"aMS",@progbits,1
.LC21:
	.string	"#endif  // "
	.section	.text._ZN2v88internal6torque10IfDefScopeD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque10IfDefScopeD2Ev
	.type	_ZN2v88internal6torque10IfDefScopeD2Ev, @function
_ZN2v88internal6torque10IfDefScopeD2Ev:
.LFB5595:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA5595
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$11, %edx
	leaq	.LC21(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %r12
	movq	%rdi, %rbx
	addq	$24, %rbx
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-8(%rbx), %rdx
	movq	-16(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	leaq	.LC19(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-16(%rbx), %rdi
	cmpq	%rbx, %rdi
	je	.L435
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L435:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5595:
	.section	.gcc_except_table._ZN2v88internal6torque10IfDefScopeD2Ev,"a",@progbits
.LLSDA5595:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE5595-.LLSDACSB5595
.LLSDACSB5595:
.LLSDACSE5595:
	.section	.text._ZN2v88internal6torque10IfDefScopeD2Ev
	.size	_ZN2v88internal6torque10IfDefScopeD2Ev, .-_ZN2v88internal6torque10IfDefScopeD2Ev
	.globl	_ZN2v88internal6torque10IfDefScopeD1Ev
	.set	_ZN2v88internal6torque10IfDefScopeD1Ev,_ZN2v88internal6torque10IfDefScopeD2Ev
	.section	.rodata._ZN2v88internal6torque14NamespaceScopeC2ERSoSt16initializer_listINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC22:
	.string	"cannot create std::vector larger than max_size()"
	.section	.rodata._ZN2v88internal6torque14NamespaceScopeC2ERSoSt16initializer_listINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE.str1.1,"aMS",@progbits,1
.LC23:
	.string	"namespace "
.LC24:
	.string	" {\n"
	.section	.text.unlikely._ZN2v88internal6torque14NamespaceScopeC2ERSoSt16initializer_listINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE,"ax",@progbits
	.align 2
.LCOLDB25:
	.section	.text._ZN2v88internal6torque14NamespaceScopeC2ERSoSt16initializer_listINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE,"ax",@progbits
.LHOTB25:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque14NamespaceScopeC2ERSoSt16initializer_listINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE
	.type	_ZN2v88internal6torque14NamespaceScopeC2ERSoSt16initializer_listINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE, @function
_ZN2v88internal6torque14NamespaceScopeC2ERSoSt16initializer_listINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE:
.LFB5599:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA5599
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	salq	$5, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	(%rdx,%rcx), %rax
	movq	%rsi, (%rdi)
	movabsq	$288230376151711743, %rdx
	movq	%rax, -72(%rbp)
	movq	%rcx, %rax
	sarq	$5, %rax
	movq	$0, 8(%rdi)
	movq	$0, 16(%rdi)
	movq	$0, 24(%rdi)
	cmpq	%rdx, %rax
	ja	.L487
	movq	%rcx, %rbx
	testq	%rax, %rax
	je	.L464
	movq	%rcx, %rdi
.LEHB26:
	call	_Znwm@PLT
.LEHE26:
	movq	%rax, -88(%rbp)
.L440:
	movq	-88(%rbp), %rax
	addq	%rax, %rbx
	movq	%rax, 8(%r15)
	movq	%rbx, 24(%r15)
	cmpq	-72(%rbp), %r12
	je	.L441
	movq	%rax, %rbx
	leaq	-64(%rbp), %rax
	movq	%rax, -80(%rbp)
	jmp	.L447
	.p2align 4,,10
	.p2align 3
.L443:
	cmpq	$1, %r12
	jne	.L445
	movzbl	(%r14), %eax
	movb	%al, 16(%rbx)
.L446:
	movq	%r12, 8(%rbx)
	addq	$32, %r13
	addq	$32, %rbx
	movb	$0, (%rdi,%r12)
	cmpq	%r13, -72(%rbp)
	je	.L488
.L447:
	movq	0(%r13), %r14
	movq	8(%r13), %r12
	leaq	16(%rbx), %rdi
	movq	%rdi, (%rbx)
	movq	%r14, %rax
	addq	%r12, %rax
	je	.L442
	testq	%r14, %r14
	je	.L489
.L442:
	movq	%r12, -64(%rbp)
	cmpq	$15, %r12
	jbe	.L443
	movq	-80(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rbx, %rdi
.LEHB27:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE27:
	movq	%rax, (%rbx)
	movq	%rax, %rdi
	movq	-64(%rbp), %rax
	movq	%rax, 16(%rbx)
.L444:
	movq	%r12, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-64(%rbp), %r12
	movq	(%rbx), %rdi
	jmp	.L446
	.p2align 4,,10
	.p2align 3
.L445:
	testq	%r12, %r12
	je	.L446
	jmp	.L444
	.p2align 4,,10
	.p2align 3
.L488:
	movq	8(%r15), %r12
	movq	%rbx, 16(%r15)
	cmpq	%rbx, %r12
	je	.L438
	leaq	.LC23(%rip), %r13
	.p2align 4,,10
	.p2align 3
.L456:
	movq	(%r15), %r14
	movl	$10, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
.LEHB28:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%r12), %rdx
	movq	(%r12), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rax, %rdi
	movl	$3, %edx
	leaq	.LC24(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE28:
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L456
.L438:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L490
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L464:
	.cfi_restore_state
	movq	$0, -88(%rbp)
	jmp	.L440
	.p2align 4,,10
	.p2align 3
.L441:
	movq	%rax, 16(%r15)
	jmp	.L438
.L489:
	leaq	.LC4(%rip), %rdi
.LEHB29:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE29:
.L487:
	leaq	.LC22(%rip), %rdi
.LEHB30:
	call	_ZSt20__throw_length_errorPKc@PLT
.LEHE30:
.L490:
	call	__stack_chk_fail@PLT
.L468:
	endbr64
	movq	%rax, %rdi
	jmp	.L449
.L465:
	endbr64
	movq	%rax, %r12
	jmp	.L458
.L466:
	endbr64
	movq	%rax, %r12
	jmp	.L454
	.section	.gcc_except_table._ZN2v88internal6torque14NamespaceScopeC2ERSoSt16initializer_listINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE,"a",@progbits
	.align 4
.LLSDA5599:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATT5599-.LLSDATTD5599
.LLSDATTD5599:
	.byte	0x1
	.uleb128 .LLSDACSE5599-.LLSDACSB5599
.LLSDACSB5599:
	.uleb128 .LEHB26-.LFB5599
	.uleb128 .LEHE26-.LEHB26
	.uleb128 .L466-.LFB5599
	.uleb128 0
	.uleb128 .LEHB27-.LFB5599
	.uleb128 .LEHE27-.LEHB27
	.uleb128 .L468-.LFB5599
	.uleb128 0x1
	.uleb128 .LEHB28-.LFB5599
	.uleb128 .LEHE28-.LEHB28
	.uleb128 .L465-.LFB5599
	.uleb128 0
	.uleb128 .LEHB29-.LFB5599
	.uleb128 .LEHE29-.LEHB29
	.uleb128 .L468-.LFB5599
	.uleb128 0x1
	.uleb128 .LEHB30-.LFB5599
	.uleb128 .LEHE30-.LEHB30
	.uleb128 .L466-.LFB5599
	.uleb128 0
.LLSDACSE5599:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATT5599:
	.section	.text._ZN2v88internal6torque14NamespaceScopeC2ERSoSt16initializer_listINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque14NamespaceScopeC2ERSoSt16initializer_listINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC5599
	.type	_ZN2v88internal6torque14NamespaceScopeC2ERSoSt16initializer_listINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE.cold, @function
_ZN2v88internal6torque14NamespaceScopeC2ERSoSt16initializer_listINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE.cold:
.LFSB5599:
.L449:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	call	__cxa_begin_catch@PLT
.L452:
	cmpq	%rbx, -88(%rbp)
	jne	.L491
.LEHB31:
	call	__cxa_rethrow@PLT
.LEHE31:
.L458:
	movq	16(%r15), %r13
	movq	8(%r15), %rbx
.L461:
	cmpq	%rbx, %r13
	jne	.L492
	movq	8(%r15), %rdi
	testq	%rdi, %rdi
	je	.L462
	call	_ZdlPv@PLT
.L462:
	movq	%r12, %rdi
.LEHB32:
	call	_Unwind_Resume@PLT
.LEHE32:
.L467:
	endbr64
	movq	%rax, %r12
	call	__cxa_end_catch@PLT
.L454:
	movq	8(%r15), %rdi
	testq	%rdi, %rdi
	je	.L455
	call	_ZdlPv@PLT
.L455:
	movq	%r12, %rdi
.LEHB33:
	call	_Unwind_Resume@PLT
.LEHE33:
.L491:
	movq	-88(%rbp), %rax
	movq	(%rax), %rdi
	addq	$16, %rax
	cmpq	%rax, %rdi
	je	.L451
	call	_ZdlPv@PLT
.L451:
	addq	$32, -88(%rbp)
	jmp	.L452
.L492:
	movq	(%rbx), %rdi
	leaq	16(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L460
	call	_ZdlPv@PLT
.L460:
	addq	$32, %rbx
	jmp	.L461
	.cfi_endproc
.LFE5599:
	.section	.gcc_except_table._ZN2v88internal6torque14NamespaceScopeC2ERSoSt16initializer_listINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE
	.align 4
.LLSDAC5599:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATTC5599-.LLSDATTDC5599
.LLSDATTDC5599:
	.byte	0x1
	.uleb128 .LLSDACSEC5599-.LLSDACSBC5599
.LLSDACSBC5599:
	.uleb128 .LEHB31-.LCOLDB25
	.uleb128 .LEHE31-.LEHB31
	.uleb128 .L467-.LCOLDB25
	.uleb128 0
	.uleb128 .LEHB32-.LCOLDB25
	.uleb128 .LEHE32-.LEHB32
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB33-.LCOLDB25
	.uleb128 .LEHE33-.LEHB33
	.uleb128 0
	.uleb128 0
.LLSDACSEC5599:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATTC5599:
	.section	.text.unlikely._ZN2v88internal6torque14NamespaceScopeC2ERSoSt16initializer_listINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE
	.section	.text._ZN2v88internal6torque14NamespaceScopeC2ERSoSt16initializer_listINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE
	.size	_ZN2v88internal6torque14NamespaceScopeC2ERSoSt16initializer_listINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE, .-_ZN2v88internal6torque14NamespaceScopeC2ERSoSt16initializer_listINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE
	.section	.text.unlikely._ZN2v88internal6torque14NamespaceScopeC2ERSoSt16initializer_listINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE
	.size	_ZN2v88internal6torque14NamespaceScopeC2ERSoSt16initializer_listINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE.cold, .-_ZN2v88internal6torque14NamespaceScopeC2ERSoSt16initializer_listINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE.cold
.LCOLDE25:
	.section	.text._ZN2v88internal6torque14NamespaceScopeC2ERSoSt16initializer_listINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE
.LHOTE25:
	.globl	_ZN2v88internal6torque14NamespaceScopeC1ERSoSt16initializer_listINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE
	.set	_ZN2v88internal6torque14NamespaceScopeC1ERSoSt16initializer_listINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE,_ZN2v88internal6torque14NamespaceScopeC2ERSoSt16initializer_listINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE
	.section	.rodata._ZN2v88internal6torque14NamespaceScopeD2Ev.str1.1,"aMS",@progbits,1
.LC26:
	.string	"}  // namespace "
	.section	.text._ZN2v88internal6torque14NamespaceScopeD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque14NamespaceScopeD2Ev
	.type	_ZN2v88internal6torque14NamespaceScopeD2Ev, @function
_ZN2v88internal6torque14NamespaceScopeD2Ev:
.LFB5602:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA5602
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	16(%rdi), %rbx
	movq	8(%rdi), %rdi
	cmpq	%rdi, %rbx
	je	.L494
	leaq	.LC26(%rip), %r13
	.p2align 4,,10
	.p2align 3
.L495:
	movq	(%r12), %r14
	movl	$16, %edx
	movq	%r13, %rsi
	subq	$32, %rbx
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%rbx), %rdx
	movq	(%rbx), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	leaq	.LC19(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpq	%rbx, 8(%r12)
	jne	.L495
	movq	16(%r12), %r13
	cmpq	%r13, %rbx
	je	.L502
	.p2align 4,,10
	.p2align 3
.L500:
	movq	(%rbx), %rdi
	leaq	16(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L497
	call	_ZdlPv@PLT
	addq	$32, %rbx
	cmpq	%rbx, %r13
	jne	.L500
.L498:
	movq	8(%r12), %rdi
.L494:
	testq	%rdi, %rdi
	je	.L505
.L496:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L497:
	.cfi_restore_state
	addq	$32, %rbx
	cmpq	%r13, %rbx
	jne	.L500
	jmp	.L498
	.p2align 4,,10
	.p2align 3
.L505:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L502:
	.cfi_restore_state
	movq	%rbx, %rdi
	jmp	.L496
	.cfi_endproc
.LFE5602:
	.section	.gcc_except_table._ZN2v88internal6torque14NamespaceScopeD2Ev,"a",@progbits
.LLSDA5602:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE5602-.LLSDACSB5602
.LLSDACSB5602:
.LLSDACSE5602:
	.section	.text._ZN2v88internal6torque14NamespaceScopeD2Ev
	.size	_ZN2v88internal6torque14NamespaceScopeD2Ev, .-_ZN2v88internal6torque14NamespaceScopeD2Ev
	.globl	_ZN2v88internal6torque14NamespaceScopeD1Ev
	.set	_ZN2v88internal6torque14NamespaceScopeD1Ev,_ZN2v88internal6torque14NamespaceScopeD2Ev
	.section	.rodata._ZN2v88internal6torque17IncludeGuardScopeC2ERSoNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.str1.1,"aMS",@progbits,1
.LC27:
	.string	"V8_GEN_TORQUE_GENERATED_"
.LC28:
	.string	"#ifndef "
.LC29:
	.string	"#define "
.LC30:
	.string	"\n\n"
	.section	.text.unlikely._ZN2v88internal6torque17IncludeGuardScopeC2ERSoNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"ax",@progbits
	.align 2
.LCOLDB31:
	.section	.text._ZN2v88internal6torque17IncludeGuardScopeC2ERSoNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"ax",@progbits
.LHOTB31:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque17IncludeGuardScopeC2ERSoNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN2v88internal6torque17IncludeGuardScopeC2ERSoNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN2v88internal6torque17IncludeGuardScopeC2ERSoNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB5605:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA5605
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rdx, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-80(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%r8, (%rdi)
	movq	%r12, %rdi
.LEHB34:
	call	_ZN2v88internal6torque27CapifyStringWithUnderscoresERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
.LEHE34:
	movl	$24, %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
	leaq	.LC27(%rip), %rcx
	movq	%r12, %rdi
.LEHB35:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.LEHE35:
	leaq	-96(%rbp), %r12
	leaq	16(%rax), %rdx
	movq	%r12, -112(%rbp)
	movq	(%rax), %rcx
	cmpq	%rdx, %rcx
	je	.L526
	movq	%rcx, -112(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -96(%rbp)
.L508:
	movq	8(%rax), %rcx
	movb	$0, 16(%rax)
	movq	%rcx, -104(%rbp)
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movabsq	$4611686018427387903, %rax
	cmpq	%rax, -104(%rbp)
	je	.L527
	leaq	-112(%rbp), %rdi
	movl	$1, %edx
	leaq	.LC2(%rip), %rsi
.LEHB36:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.LEHE36:
	leaq	24(%rbx), %r13
	leaq	16(%rax), %rdx
	movq	%r13, 8(%rbx)
	movq	(%rax), %rcx
	cmpq	%rdx, %rcx
	je	.L528
	movq	%rcx, 8(%rbx)
	movq	16(%rax), %rcx
	movq	%rcx, 24(%rbx)
.L511:
	movq	8(%rax), %rcx
	movq	%rdx, (%rax)
	movq	-112(%rbp), %rdi
	movq	$0, 8(%rax)
	movq	%rcx, 16(%rbx)
	movb	$0, 16(%rax)
	cmpq	%r12, %rdi
	je	.L512
	call	_ZdlPv@PLT
.L512:
	movq	-80(%rbp), %rdi
	leaq	-64(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L513
	call	_ZdlPv@PLT
.L513:
	movq	(%rbx), %r12
	movl	$8, %edx
	leaq	.LC28(%rip), %rsi
	movq	%r12, %rdi
.LEHB37:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	16(%rbx), %rdx
	movq	8(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rax, %rdi
	movl	$1, %edx
	leaq	.LC19(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %r12
	movl	$8, %edx
	leaq	.LC29(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	16(%rbx), %rdx
	movq	8(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rax, %rdi
	movl	$2, %edx
	leaq	.LC30(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE37:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L529
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L526:
	.cfi_restore_state
	movdqu	16(%rax), %xmm0
	movaps	%xmm0, -96(%rbp)
	jmp	.L508
	.p2align 4,,10
	.p2align 3
.L528:
	movdqu	16(%rax), %xmm1
	movups	%xmm1, 24(%rbx)
	jmp	.L511
.L529:
	call	__stack_chk_fail@PLT
.L527:
	leaq	.LC5(%rip), %rdi
.LEHB38:
	call	_ZSt20__throw_length_errorPKc@PLT
.LEHE38:
.L522:
	endbr64
	movq	%rax, %rbx
	jmp	.L514
.L523:
	endbr64
	movq	%rax, %r12
	jmp	.L518
.L521:
	endbr64
	movq	%rax, %r12
	jmp	.L516
	.section	.gcc_except_table._ZN2v88internal6torque17IncludeGuardScopeC2ERSoNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"a",@progbits
.LLSDA5605:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE5605-.LLSDACSB5605
.LLSDACSB5605:
	.uleb128 .LEHB34-.LFB5605
	.uleb128 .LEHE34-.LEHB34
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB35-.LFB5605
	.uleb128 .LEHE35-.LEHB35
	.uleb128 .L521-.LFB5605
	.uleb128 0
	.uleb128 .LEHB36-.LFB5605
	.uleb128 .LEHE36-.LEHB36
	.uleb128 .L522-.LFB5605
	.uleb128 0
	.uleb128 .LEHB37-.LFB5605
	.uleb128 .LEHE37-.LEHB37
	.uleb128 .L523-.LFB5605
	.uleb128 0
	.uleb128 .LEHB38-.LFB5605
	.uleb128 .LEHE38-.LEHB38
	.uleb128 .L522-.LFB5605
	.uleb128 0
.LLSDACSE5605:
	.section	.text._ZN2v88internal6torque17IncludeGuardScopeC2ERSoNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque17IncludeGuardScopeC2ERSoNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC5605
	.type	_ZN2v88internal6torque17IncludeGuardScopeC2ERSoNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.cold, @function
_ZN2v88internal6torque17IncludeGuardScopeC2ERSoNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.cold:
.LFSB5605:
.L514:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movq	-112(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L515
	call	_ZdlPv@PLT
.L515:
	movq	%rbx, %r12
.L516:
	movq	-80(%rbp), %rdi
	leaq	-64(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L517
	call	_ZdlPv@PLT
.L517:
	movq	%r12, %rdi
.LEHB39:
	call	_Unwind_Resume@PLT
.L518:
	movq	8(%rbx), %rdi
	cmpq	%r13, %rdi
	je	.L519
	call	_ZdlPv@PLT
.L519:
	movq	%r12, %rdi
	call	_Unwind_Resume@PLT
.LEHE39:
	.cfi_endproc
.LFE5605:
	.section	.gcc_except_table._ZN2v88internal6torque17IncludeGuardScopeC2ERSoNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
.LLSDAC5605:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC5605-.LLSDACSBC5605
.LLSDACSBC5605:
	.uleb128 .LEHB39-.LCOLDB31
	.uleb128 .LEHE39-.LEHB39
	.uleb128 0
	.uleb128 0
.LLSDACSEC5605:
	.section	.text.unlikely._ZN2v88internal6torque17IncludeGuardScopeC2ERSoNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.text._ZN2v88internal6torque17IncludeGuardScopeC2ERSoNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.size	_ZN2v88internal6torque17IncludeGuardScopeC2ERSoNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN2v88internal6torque17IncludeGuardScopeC2ERSoNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.text.unlikely._ZN2v88internal6torque17IncludeGuardScopeC2ERSoNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.size	_ZN2v88internal6torque17IncludeGuardScopeC2ERSoNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.cold, .-_ZN2v88internal6torque17IncludeGuardScopeC2ERSoNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.cold
.LCOLDE31:
	.section	.text._ZN2v88internal6torque17IncludeGuardScopeC2ERSoNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
.LHOTE31:
	.globl	_ZN2v88internal6torque17IncludeGuardScopeC1ERSoNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.set	_ZN2v88internal6torque17IncludeGuardScopeC1ERSoNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,_ZN2v88internal6torque17IncludeGuardScopeC2ERSoNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.text._ZN2v88internal6torque17IncludeGuardScopeD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque17IncludeGuardScopeD2Ev
	.type	_ZN2v88internal6torque17IncludeGuardScopeD2Ev, @function
_ZN2v88internal6torque17IncludeGuardScopeD2Ev:
.LFB5608:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA5608
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$11, %edx
	leaq	.LC21(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %r12
	movq	%rdi, %rbx
	addq	$24, %rbx
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-8(%rbx), %rdx
	movq	-16(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	leaq	.LC19(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-16(%rbx), %rdi
	cmpq	%rbx, %rdi
	je	.L530
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L530:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5608:
	.section	.gcc_except_table._ZN2v88internal6torque17IncludeGuardScopeD2Ev,"a",@progbits
.LLSDA5608:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE5608-.LLSDACSB5608
.LLSDACSB5608:
.LLSDACSE5608:
	.section	.text._ZN2v88internal6torque17IncludeGuardScopeD2Ev
	.size	_ZN2v88internal6torque17IncludeGuardScopeD2Ev, .-_ZN2v88internal6torque17IncludeGuardScopeD2Ev
	.globl	_ZN2v88internal6torque17IncludeGuardScopeD1Ev
	.set	_ZN2v88internal6torque17IncludeGuardScopeD1Ev,_ZN2v88internal6torque17IncludeGuardScopeD2Ev
	.section	.rodata._ZN2v88internal6torque24IncludeObjectMacrosScopeC2ERSo.str1.8,"aMS",@progbits,1
	.align 8
.LC32:
	.string	"\n// Has to be the last include (doesn't have include guards):\n#include \"src/objects/object-macros.h\"\n"
	.section	.text._ZN2v88internal6torque24IncludeObjectMacrosScopeC2ERSo,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque24IncludeObjectMacrosScopeC2ERSo
	.type	_ZN2v88internal6torque24IncludeObjectMacrosScopeC2ERSo, @function
_ZN2v88internal6torque24IncludeObjectMacrosScopeC2ERSo:
.LFB5611:
	.cfi_startproc
	endbr64
	movq	%rdi, %r8
	movl	$101, %edx
	movq	%rsi, %rdi
	movq	%rsi, (%r8)
	leaq	.LC32(%rip), %rsi
	jmp	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	.cfi_endproc
.LFE5611:
	.size	_ZN2v88internal6torque24IncludeObjectMacrosScopeC2ERSo, .-_ZN2v88internal6torque24IncludeObjectMacrosScopeC2ERSo
	.globl	_ZN2v88internal6torque24IncludeObjectMacrosScopeC1ERSo
	.set	_ZN2v88internal6torque24IncludeObjectMacrosScopeC1ERSo,_ZN2v88internal6torque24IncludeObjectMacrosScopeC2ERSo
	.section	.rodata._ZN2v88internal6torque24IncludeObjectMacrosScopeD2Ev.str1.8,"aMS",@progbits,1
	.align 8
.LC33:
	.string	"\n#include \"src/objects/object-macros-undef.h\"\n"
	.section	.text._ZN2v88internal6torque24IncludeObjectMacrosScopeD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque24IncludeObjectMacrosScopeD2Ev
	.type	_ZN2v88internal6torque24IncludeObjectMacrosScopeD2Ev, @function
_ZN2v88internal6torque24IncludeObjectMacrosScopeD2Ev:
.LFB5614:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA5614
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rdi), %rdi
	movl	$46, %edx
	leaq	.LC33(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5614:
	.section	.gcc_except_table._ZN2v88internal6torque24IncludeObjectMacrosScopeD2Ev,"a",@progbits
.LLSDA5614:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE5614-.LLSDACSB5614
.LLSDACSB5614:
.LLSDACSE5614:
	.section	.text._ZN2v88internal6torque24IncludeObjectMacrosScopeD2Ev
	.size	_ZN2v88internal6torque24IncludeObjectMacrosScopeD2Ev, .-_ZN2v88internal6torque24IncludeObjectMacrosScopeD2Ev
	.globl	_ZN2v88internal6torque24IncludeObjectMacrosScopeD1Ev
	.set	_ZN2v88internal6torque24IncludeObjectMacrosScopeD1Ev,_ZN2v88internal6torque24IncludeObjectMacrosScopeD2Ev
	.section	.text._ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED2Ev,"axG",@progbits,_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED2Ev
	.type	_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED2Ev, @function
_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED2Ev:
.LFB6242:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	72(%rdi), %rdi
	leaq	88(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L537
	call	_ZdlPv@PLT
.L537:
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	56(%rbx), %rdi
	movq	%rax, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt6localeD1Ev@PLT
	.cfi_endproc
.LFE6242:
	.size	_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED2Ev, .-_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED2Ev
	.weak	_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED1Ev
	.set	_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED1Ev,_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED2Ev
	.section	.text._ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED0Ev,"axG",@progbits,_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED0Ev
	.type	_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED0Ev, @function
_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED0Ev:
.LFB6244:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	72(%rdi), %rdi
	leaq	88(%r12), %rax
	cmpq	%rax, %rdi
	je	.L540
	call	_ZdlPv@PLT
.L540:
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	56(%r12), %rdi
	movq	%rax, (%r12)
	call	_ZNSt6localeD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$104, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE6244:
	.size	_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED0Ev, .-_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED0Ev
	.section	.rodata._ZN2v88internal6torque20StringLiteralUnquoteERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.str1.1,"aMS",@progbits,1
.LC34:
	.string	"unreachable code"
	.section	.text.unlikely._ZN2v88internal6torque20StringLiteralUnquoteERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"ax",@progbits
.LCOLDB38:
	.section	.text._ZN2v88internal6torque20StringLiteralUnquoteERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"ax",@progbits
.LHOTB38:
	.p2align 4
	.globl	_ZN2v88internal6torque20StringLiteralUnquoteERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN2v88internal6torque20StringLiteralUnquoteERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN2v88internal6torque20StringLiteralUnquoteERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB5539:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA5539
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-448(%rbp), %rbx
	subq	$456, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-320(%rbp), %rax
	movq	%rbx, -496(%rbp)
	movq	%rax, %rdi
	movq	%rax, -472(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rcx
	xorl	%eax, %eax
	xorl	%esi, %esi
	movq	%rcx, -320(%rbp)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	pxor	%xmm0, %xmm0
	movq	$0, -104(%rbp)
	movq	%rcx, -448(%rbp)
	movw	%ax, -96(%rbp)
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	-24(%rcx), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rcx, -448(%rbp,%rax)
	movq	-448(%rbp), %rax
	movq	$0, -440(%rbp)
	addq	-24(%rax), %rbx
	movq	%rbx, %rdi
.LEHB40:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE40:
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	leaq	-432(%rbp), %r14
	xorl	%esi, %esi
	movq	%rcx, -432(%rbp)
	movq	-24(%rcx), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rcx, -432(%rbp,%rax)
	movq	-432(%rbp), %rax
	movq	-24(%rax), %rcx
	addq	%r14, %rcx
	movq	%rcx, %rdi
.LEHB41:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE41:
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	leaq	-424(%rbp), %r13
	movq	.LC35(%rip), %xmm0
	movq	%rcx, -448(%rbp)
	movq	-24(%rcx), %rax
	movhps	.LC36(%rip), %xmm0
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rcx, -448(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rcx, -448(%rbp)
	addq	$80, %rcx
	movq	%rcx, -320(%rbp)
	leaq	-368(%rbp), %rcx
	movq	%rcx, %rdi
	movaps	%xmm0, -432(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rcx, -480(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%r13, %rsi
	movq	-472(%rbp), %rdi
	movq	%rcx, -424(%rbp)
	leaq	-336(%rbp), %rcx
	movl	$24, -360(%rbp)
	movq	%rcx, -488(%rbp)
	movq	%rcx, -352(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
.LEHB42:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE42:
	movq	8(%r12), %rax
	movl	$1, %ebx
	movabsq	$288230376151711777, %r13
	subq	$1, %rax
	cmpq	$1, %rax
	jbe	.L559
	.p2align 4,,10
	.p2align 3
.L547:
	movq	(%r12), %rax
	movzbl	(%rax,%rbx), %edx
	cmpb	$92, %dl
	jne	.L550
	addq	$1, %rbx
	movzbl	(%rax,%rbx), %eax
	cmpb	$114, %al
	je	.L551
	jg	.L552
	cmpb	$92, %al
	jg	.L553
	cmpb	$33, %al
	jle	.L554
	leal	-34(%rax), %edx
	btq	%rdx, %r13
	jc	.L580
.L554:
	leaq	.LC34(%rip), %rdi
	xorl	%eax, %eax
.LEHB43:
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L553:
	cmpb	$110, %al
	jne	.L554
	leaq	-453(%rbp), %rsi
	movl	$1, %edx
	movq	%r14, %rdi
	movb	$10, -453(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE43:
	.p2align 4,,10
	.p2align 3
.L558:
	movq	8(%r12), %rax
	addq	$1, %rbx
	subq	$1, %rax
	cmpq	%rbx, %rax
	ja	.L547
.L559:
	movq	-384(%rbp), %rax
	leaq	16(%r15), %rbx
	movb	$0, 16(%r15)
	movq	%rbx, (%r15)
	movq	$0, 8(%r15)
	testq	%rax, %rax
	je	.L581
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	ja	.L582
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
.LEHB44:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.LEHE44:
.L561:
	movq	.LC35(%rip), %xmm0
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-352(%rbp), %rdi
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movhps	.LC37(%rip), %xmm0
	movq	%rax, -320(%rbp)
	movaps	%xmm0, -432(%rbp)
	cmpq	-488(%rbp), %rdi
	je	.L563
	call	_ZdlPv@PLT
.L563:
	movq	-480(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdi
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rdi, -448(%rbp,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdi
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rdi, -432(%rbp,%rax)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdi
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rdi, -448(%rbp,%rax)
	movq	-472(%rbp), %rdi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	movq	$0, -440(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L583
	addq	$456, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L552:
	.cfi_restore_state
	cmpb	$116, %al
	jne	.L554
	leaq	-451(%rbp), %rsi
	movl	$1, %edx
	movq	%r14, %rdi
	movb	$9, -451(%rbp)
.LEHB45:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L558
	.p2align 4,,10
	.p2align 3
.L550:
	movb	%dl, -449(%rbp)
	leaq	-449(%rbp), %rsi
	movl	$1, %edx
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L558
.L580:
	leaq	-450(%rbp), %rsi
	movl	$1, %edx
	movq	%r14, %rdi
	movb	%al, -450(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L558
	.p2align 4,,10
	.p2align 3
.L551:
	leaq	-452(%rbp), %rsi
	movl	$1, %edx
	movq	%r14, %rdi
	movb	$13, -452(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE45:
	jmp	.L558
	.p2align 4,,10
	.p2align 3
.L582:
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%rax, %r8
.LEHB46:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L561
.L581:
	leaq	-352(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
.LEHE46:
	jmp	.L561
.L583:
	call	__stack_chk_fail@PLT
.L569:
	endbr64
	movq	%rax, %r12
	jmp	.L566
.L571:
	endbr64
	movq	%rax, %rbx
	jmp	.L546
.L570:
	endbr64
	movq	%rax, %rbx
	jmp	.L545
.L572:
	endbr64
	movq	%rax, %rbx
	jmp	.L544
.L573:
	endbr64
	movq	%rax, %r12
	jmp	.L564
	.section	.gcc_except_table._ZN2v88internal6torque20StringLiteralUnquoteERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"a",@progbits
.LLSDA5539:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE5539-.LLSDACSB5539
.LLSDACSB5539:
	.uleb128 .LEHB40-.LFB5539
	.uleb128 .LEHE40-.LEHB40
	.uleb128 .L570-.LFB5539
	.uleb128 0
	.uleb128 .LEHB41-.LFB5539
	.uleb128 .LEHE41-.LEHB41
	.uleb128 .L572-.LFB5539
	.uleb128 0
	.uleb128 .LEHB42-.LFB5539
	.uleb128 .LEHE42-.LEHB42
	.uleb128 .L571-.LFB5539
	.uleb128 0
	.uleb128 .LEHB43-.LFB5539
	.uleb128 .LEHE43-.LEHB43
	.uleb128 .L569-.LFB5539
	.uleb128 0
	.uleb128 .LEHB44-.LFB5539
	.uleb128 .LEHE44-.LEHB44
	.uleb128 .L573-.LFB5539
	.uleb128 0
	.uleb128 .LEHB45-.LFB5539
	.uleb128 .LEHE45-.LEHB45
	.uleb128 .L569-.LFB5539
	.uleb128 0
	.uleb128 .LEHB46-.LFB5539
	.uleb128 .LEHE46-.LEHB46
	.uleb128 .L573-.LFB5539
	.uleb128 0
.LLSDACSE5539:
	.section	.text._ZN2v88internal6torque20StringLiteralUnquoteERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque20StringLiteralUnquoteERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC5539
	.type	_ZN2v88internal6torque20StringLiteralUnquoteERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.cold, @function
_ZN2v88internal6torque20StringLiteralUnquoteERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.cold:
.LFSB5539:
.L564:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	(%r15), %rdi
	cmpq	%rdi, %rbx
	je	.L566
	call	_ZdlPv@PLT
.L566:
	movq	-496(%rbp), %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r12, %rdi
.LEHB47:
	call	_Unwind_Resume@PLT
.L546:
	movq	%r13, %rdi
	call	_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED1Ev
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdi
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rdi, -448(%rbp,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdi
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rdi, -432(%rbp,%rax)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdi
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rdi, -448(%rbp,%rax)
	movq	$0, -440(%rbp)
.L545:
	movq	-472(%rbp), %rdi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	%rbx, %rdi
	call	_Unwind_Resume@PLT
.LEHE47:
.L544:
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdi
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rdi, -448(%rbp,%rax)
	movq	$0, -440(%rbp)
	jmp	.L545
	.cfi_endproc
.LFE5539:
	.section	.gcc_except_table._ZN2v88internal6torque20StringLiteralUnquoteERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
.LLSDAC5539:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC5539-.LLSDACSBC5539
.LLSDACSBC5539:
	.uleb128 .LEHB47-.LCOLDB38
	.uleb128 .LEHE47-.LEHB47
	.uleb128 0
	.uleb128 0
.LLSDACSEC5539:
	.section	.text.unlikely._ZN2v88internal6torque20StringLiteralUnquoteERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.text._ZN2v88internal6torque20StringLiteralUnquoteERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.size	_ZN2v88internal6torque20StringLiteralUnquoteERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN2v88internal6torque20StringLiteralUnquoteERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.text.unlikely._ZN2v88internal6torque20StringLiteralUnquoteERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.size	_ZN2v88internal6torque20StringLiteralUnquoteERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.cold, .-_ZN2v88internal6torque20StringLiteralUnquoteERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.cold
.LCOLDE38:
	.section	.text._ZN2v88internal6torque20StringLiteralUnquoteERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
.LHOTE38:
	.section	.rodata._ZN2v88internal6torque18StringLiteralQuoteERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.str1.1,"aMS",@progbits,1
.LC39:
	.string	"\\n"
.LC40:
	.string	"\\r"
.LC41:
	.string	"\\t"
.LC42:
	.string	"\\"
	.section	.text.unlikely._ZN2v88internal6torque18StringLiteralQuoteERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"ax",@progbits
.LCOLDB43:
	.section	.text._ZN2v88internal6torque18StringLiteralQuoteERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"ax",@progbits
.LHOTB43:
	.p2align 4
	.globl	_ZN2v88internal6torque18StringLiteralQuoteERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN2v88internal6torque18StringLiteralQuoteERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN2v88internal6torque18StringLiteralQuoteERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB5540:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA5540
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-448(%rbp), %rbx
	subq	$456, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-320(%rbp), %rax
	movq	%rbx, -496(%rbp)
	movq	%rax, %rdi
	movq	%rax, -472(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rdx
	xorl	%eax, %eax
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	movw	%ax, -96(%rbp)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rdx, -320(%rbp)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	$0, -104(%rbp)
	movq	%rdx, -448(%rbp)
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	-24(%rdx), %rax
	movq	%rcx, -448(%rbp,%rax)
	movq	-448(%rbp), %rax
	movq	$0, -440(%rbp)
	addq	-24(%rax), %rbx
	movq	%rbx, %rdi
.LEHB48:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE48:
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	leaq	-432(%rbp), %r13
	xorl	%esi, %esi
	movq	%rdx, -432(%rbp)
	movq	-24(%rdx), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%rdx, -432(%rbp,%rax)
	movq	-432(%rbp), %rax
	movq	-24(%rax), %rcx
	addq	%r13, %rcx
	movq	%rcx, %rdi
.LEHB49:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE49:
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	.LC35(%rip), %xmm0
	leaq	-424(%rbp), %r15
	movq	%rcx, -448(%rbp)
	movq	-24(%rcx), %rax
	movhps	.LC36(%rip), %xmm0
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rcx, -448(%rbp,%rax)
	leaq	80(%rdx), %rcx
	movq	%rcx, -320(%rbp)
	leaq	-368(%rbp), %rcx
	movq	%rcx, %rdi
	movaps	%xmm0, -432(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rcx, -488(%rbp)
	movq	%rdx, -448(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rsi
	movq	-472(%rbp), %rdi
	leaq	-336(%rbp), %rcx
	movq	%rsi, -424(%rbp)
	movq	%r15, %rsi
	movl	$24, -360(%rbp)
	movq	%rcx, -480(%rbp)
	movq	%rcx, -352(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
.LEHB50:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE50:
	leaq	-452(%rbp), %rsi
	movl	$1, %edx
	movq	%r13, %rdi
	movb	$34, -452(%rbp)
.LEHB51:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	xorl	%ebx, %ebx
	cmpq	$0, 8(%r12)
	leaq	-450(%rbp), %r15
	jne	.L589
	jmp	.L599
	.p2align 4,,10
	.p2align 3
.L627:
	cmpb	$10, %al
	jne	.L595
	movl	$2, %edx
	leaq	.LC39(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L597:
	addq	$1, %rbx
	cmpq	8(%r12), %rbx
	jnb	.L599
.L589:
	movq	(%r12), %rax
	movzbl	(%rax,%rbx), %eax
	cmpb	$13, %al
	je	.L591
	jg	.L592
	cmpb	$9, %al
	jne	.L627
	movl	$2, %edx
	leaq	.LC41(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$1, %rbx
	cmpq	8(%r12), %rbx
	jb	.L589
	.p2align 4,,10
	.p2align 3
.L599:
	leaq	-449(%rbp), %rsi
	movl	$1, %edx
	movq	%r13, %rdi
	movb	$34, -449(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE51:
	movq	-384(%rbp), %rax
	leaq	16(%r14), %rbx
	movb	$0, 16(%r14)
	movq	%rbx, (%r14)
	movq	$0, 8(%r14)
	testq	%rax, %rax
	je	.L600
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	ja	.L628
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r14, %rdi
.LEHB52:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.LEHE52:
.L602:
	movq	.LC35(%rip), %xmm0
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-352(%rbp), %rdi
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movhps	.LC37(%rip), %xmm0
	movq	%rax, -320(%rbp)
	movaps	%xmm0, -432(%rbp)
	cmpq	-480(%rbp), %rdi
	je	.L604
	call	_ZdlPv@PLT
.L604:
	movq	-488(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rsi
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	-472(%rbp), %rdi
	movq	%rdx, -448(%rbp,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -432(%rbp,%rax)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rsi, -448(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	movq	$0, -440(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L629
	addq	$456, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L592:
	.cfi_restore_state
	cmpb	$34, %al
	je	.L596
	cmpb	$92, %al
	jne	.L595
.L596:
	movl	$1, %edx
	leaq	.LC42(%rip), %rsi
	movq	%r13, %rdi
.LEHB53:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r12), %rax
	leaq	-451(%rbp), %rsi
	movl	$1, %edx
	movq	%r13, %rdi
	movzbl	(%rax,%rbx), %eax
	movb	%al, -451(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L597
	.p2align 4,,10
	.p2align 3
.L591:
	movl	$2, %edx
	leaq	.LC40(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L597
.L595:
	movl	$1, %edx
	movq	%r15, %rsi
	movq	%r13, %rdi
	movb	%al, -450(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE53:
	jmp	.L597
	.p2align 4,,10
	.p2align 3
.L628:
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r14, %rdi
	movq	%rax, %r8
.LEHB54:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L602
	.p2align 4,,10
	.p2align 3
.L600:
	leaq	-352(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
.LEHE54:
	jmp	.L602
.L629:
	call	__stack_chk_fail@PLT
.L610:
	endbr64
	movq	%rax, %r12
	jmp	.L607
.L612:
	endbr64
	movq	%rax, %rbx
	jmp	.L588
.L611:
	endbr64
	movq	%rax, %rbx
	jmp	.L587
.L613:
	endbr64
	movq	%rax, %rbx
	jmp	.L586
.L614:
	endbr64
	movq	%rax, %r12
	jmp	.L605
	.section	.gcc_except_table._ZN2v88internal6torque18StringLiteralQuoteERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"a",@progbits
.LLSDA5540:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE5540-.LLSDACSB5540
.LLSDACSB5540:
	.uleb128 .LEHB48-.LFB5540
	.uleb128 .LEHE48-.LEHB48
	.uleb128 .L611-.LFB5540
	.uleb128 0
	.uleb128 .LEHB49-.LFB5540
	.uleb128 .LEHE49-.LEHB49
	.uleb128 .L613-.LFB5540
	.uleb128 0
	.uleb128 .LEHB50-.LFB5540
	.uleb128 .LEHE50-.LEHB50
	.uleb128 .L612-.LFB5540
	.uleb128 0
	.uleb128 .LEHB51-.LFB5540
	.uleb128 .LEHE51-.LEHB51
	.uleb128 .L610-.LFB5540
	.uleb128 0
	.uleb128 .LEHB52-.LFB5540
	.uleb128 .LEHE52-.LEHB52
	.uleb128 .L614-.LFB5540
	.uleb128 0
	.uleb128 .LEHB53-.LFB5540
	.uleb128 .LEHE53-.LEHB53
	.uleb128 .L610-.LFB5540
	.uleb128 0
	.uleb128 .LEHB54-.LFB5540
	.uleb128 .LEHE54-.LEHB54
	.uleb128 .L614-.LFB5540
	.uleb128 0
.LLSDACSE5540:
	.section	.text._ZN2v88internal6torque18StringLiteralQuoteERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque18StringLiteralQuoteERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC5540
	.type	_ZN2v88internal6torque18StringLiteralQuoteERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.cold, @function
_ZN2v88internal6torque18StringLiteralQuoteERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.cold:
.LFSB5540:
.L605:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	(%r14), %rdi
	cmpq	%rdi, %rbx
	je	.L607
	call	_ZdlPv@PLT
.L607:
	movq	-496(%rbp), %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r12, %rdi
.LEHB55:
	call	_Unwind_Resume@PLT
.L588:
	movq	%r15, %rdi
	call	_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED1Ev
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rdx, -448(%rbp,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rdx, -432(%rbp,%rax)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rdx, -448(%rbp,%rax)
	movq	$0, -440(%rbp)
.L587:
	movq	-472(%rbp), %rdi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	%rbx, %rdi
	call	_Unwind_Resume@PLT
.LEHE55:
.L586:
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rdx, -448(%rbp,%rax)
	movq	$0, -440(%rbp)
	jmp	.L587
	.cfi_endproc
.LFE5540:
	.section	.gcc_except_table._ZN2v88internal6torque18StringLiteralQuoteERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
.LLSDAC5540:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC5540-.LLSDACSBC5540
.LLSDACSBC5540:
	.uleb128 .LEHB55-.LCOLDB43
	.uleb128 .LEHE55-.LEHB55
	.uleb128 0
	.uleb128 0
.LLSDACSEC5540:
	.section	.text.unlikely._ZN2v88internal6torque18StringLiteralQuoteERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.text._ZN2v88internal6torque18StringLiteralQuoteERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.size	_ZN2v88internal6torque18StringLiteralQuoteERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN2v88internal6torque18StringLiteralQuoteERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.text.unlikely._ZN2v88internal6torque18StringLiteralQuoteERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.size	_ZN2v88internal6torque18StringLiteralQuoteERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.cold, .-_ZN2v88internal6torque18StringLiteralQuoteERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.cold
.LCOLDE43:
	.section	.text._ZN2v88internal6torque18StringLiteralQuoteERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
.LHOTE43:
	.section	.text.unlikely._ZN2v88internal6torque13FileUriDecodeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"ax",@progbits
.LCOLDB45:
	.section	.text._ZN2v88internal6torque13FileUriDecodeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"ax",@progbits
.LHOTB45:
	.p2align 4
	.globl	_ZN2v88internal6torque13FileUriDecodeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN2v88internal6torque13FileUriDecodeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN2v88internal6torque13FileUriDecodeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB5542:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA5542
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$7, %ecx
	movq	$-1, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	leaq	_ZN2v88internal6torqueL14kFileUriPrefixE(%rip), %rsi
	subq	$520, %rsp
	movq	%rdi, -528(%rbp)
	movq	%rbx, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE5rfindEPKcmm@PLT
	testq	%rax, %rax
	je	.L631
	movb	$0, (%r15)
	movb	$0, 8(%r15)
.L630:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L683
	movq	-528(%rbp), %rax
	addq	$520, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L631:
	.cfi_restore_state
	movq	8(%rbx), %r12
	cmpq	$6, %r12
	jbe	.L684
	leaq	-480(%rbp), %rax
	subq	$7, %r12
	movq	(%rbx), %rbx
	leaq	-496(%rbp), %rdi
	movq	%rax, -536(%rbp)
	movq	%rax, -496(%rbp)
	movq	%r12, -504(%rbp)
	cmpq	$15, %r12
	ja	.L685
	cmpq	$1, %r12
	jne	.L636
	movzbl	7(%rbx), %eax
	movb	%al, -480(%rbp)
	movq	-536(%rbp), %rax
.L637:
	movq	%r12, -488(%rbp)
	leaq	-320(%rbp), %rbx
	leaq	-432(%rbp), %r14
	movb	$0, (%rax,%r12)
	movq	%rbx, %rdi
	movq	%rbx, -544(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	movq	%rax, -320(%rbp)
	xorl	%eax, %eax
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movw	%ax, -96(%rbp)
	movq	8+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	$0, -104(%rbp)
	movq	%rax, -432(%rbp)
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -432(%rbp,%rax)
	movq	-432(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r14, %rdi
.LEHB56:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE56:
	movq	.LC44(%rip), %xmm0
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	leaq	-424(%rbp), %r12
	movq	%rax, -320(%rbp)
	leaq	-368(%rbp), %rax
	movhps	.LC36(%rip), %xmm0
	movq	%rax, %rdi
	movq	%rax, -552(%rbp)
	movaps	%xmm0, -432(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movl	$16, -360(%rbp)
	movq	%rax, -560(%rbp)
	movq	%rax, -352(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
.LEHB57:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE57:
	movq	-496(%rbp), %rbx
	movq	-488(%rbp), %rax
	leaq	-504(%rbp), %r15
	addq	%rbx, %rax
	movq	%rax, -520(%rbp)
	cmpq	%rax, %rbx
	jne	.L642
	jmp	.L657
	.p2align 4,,10
	.p2align 3
.L645:
	movq	-520(%rbp), %rax
	subq	%rbx, %rax
	cmpq	$2, %rax
	jle	.L649
	movzbl	2(%rbx), %r12d
	leaq	2(%rbx), %r13
	movzbl	1(%rbx), %ebx
	movl	%ebx, %edi
	call	isxdigit@PLT
	testl	%eax, %eax
	je	.L649
	movl	%r12d, %edi
	call	isxdigit@PLT
	testl	%eax, %eax
	je	.L649
	leal	-48(%rbx), %eax
	cmpl	$9, %eax
	jbe	.L686
	movl	%ebx, %edi
	call	isupper@PLT
	leal	-55(%rbx), %edx
	movl	%eax, %r8d
	leal	-87(%rbx), %eax
	testl	%r8d, %r8d
	cmove	%eax, %edx
	movl	%edx, %ebx
.L652:
	leal	-48(%r12), %eax
	sall	$4, %ebx
	cmpl	$9, %eax
	jbe	.L655
	movl	%r12d, %edi
	call	isupper@PLT
	movl	%eax, %r8d
	leal	-55(%r12), %eax
	subl	$87, %r12d
	testl	%r8d, %r8d
	cmove	%r12d, %eax
.L655:
	addl	%eax, %ebx
	movl	$1, %edx
	movq	%r15, %rsi
	movq	%r14, %rdi
	movb	%bl, -504(%rbp)
.LEHB58:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	1(%r13), %rbx
	cmpq	%rbx, -520(%rbp)
	je	.L657
.L642:
	movzbl	(%rbx), %eax
	cmpb	$37, %al
	je	.L645
	movl	$1, %edx
	movq	%r15, %rsi
	movq	%r14, %rdi
	movb	%al, -504(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE58:
	movq	%rbx, %r13
	leaq	1(%r13), %rbx
	cmpq	%rbx, -520(%rbp)
	jne	.L642
.L657:
	movq	-384(%rbp), %rax
	leaq	-448(%rbp), %rbx
	movq	$0, -456(%rbp)
	leaq	-464(%rbp), %rdi
	movq	%rbx, -464(%rbp)
	movb	$0, -448(%rbp)
	testq	%rax, %rax
	je	.L687
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	ja	.L688
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
.LEHB59:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.LEHE59:
.L659:
	movq	-528(%rbp), %rcx
	leaq	24(%rcx), %rax
	movb	$1, (%rcx)
	movq	%rax, 8(%rcx)
	movq	-464(%rbp), %rax
	cmpq	%rbx, %rax
	je	.L689
	movq	%rax, 8(%rcx)
	movq	-448(%rbp), %rax
	movq	%rax, 24(%rcx)
.L665:
	movq	-456(%rbp), %rax
	movq	%rax, 16(%rcx)
	jmp	.L648
	.p2align 4,,10
	.p2align 3
.L636:
	testq	%r12, %r12
	je	.L637
	movq	-536(%rbp), %rdi
	jmp	.L635
	.p2align 4,,10
	.p2align 3
.L686:
	movl	%eax, %ebx
	jmp	.L652
	.p2align 4,,10
	.p2align 3
.L649:
	movq	-528(%rbp), %rax
	movb	$0, (%rax)
	movb	$0, 8(%rax)
.L648:
	movq	.LC44(%rip), %xmm0
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-352(%rbp), %rdi
	movq	%rax, -320(%rbp)
	movhps	.LC37(%rip), %xmm0
	movaps	%xmm0, -432(%rbp)
	cmpq	-560(%rbp), %rdi
	je	.L666
	call	_ZdlPv@PLT
.L666:
	movq	-552(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	-544(%rbp), %rdi
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -432(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-496(%rbp), %rdi
	cmpq	-536(%rbp), %rdi
	je	.L630
	call	_ZdlPv@PLT
	jmp	.L630
	.p2align 4,,10
	.p2align 3
.L685:
	leaq	-504(%rbp), %rsi
	xorl	%edx, %edx
.LEHB60:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE60:
	movq	%rax, -496(%rbp)
	movq	%rax, %rdi
	movq	-504(%rbp), %rax
	movq	%rax, -480(%rbp)
.L635:
	movq	%r12, %rdx
	leaq	7(%rbx), %rsi
	call	memcpy@PLT
	movq	-504(%rbp), %r12
	movq	-496(%rbp), %rax
	jmp	.L637
	.p2align 4,,10
	.p2align 3
.L688:
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r8
.LEHB61:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L659
.L689:
	movdqa	-448(%rbp), %xmm1
	movups	%xmm1, 24(%rcx)
	jmp	.L665
.L687:
	leaq	-352(%rbp), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
.LEHE61:
	jmp	.L659
.L683:
	call	__stack_chk_fail@PLT
.L684:
	movq	%r12, %rcx
	movl	$7, %edx
	leaq	.LC0(%rip), %rsi
	xorl	%eax, %eax
	leaq	.LC1(%rip), %rdi
.LEHB62:
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.LEHE62:
.L670:
	endbr64
	movq	%rax, %r12
	jmp	.L664
.L673:
	endbr64
	movq	%rax, %r12
	jmp	.L662
.L672:
	endbr64
	movq	%rax, %rbx
	jmp	.L639
.L671:
	endbr64
	movq	%rax, %rbx
	jmp	.L640
	.section	.gcc_except_table._ZN2v88internal6torque13FileUriDecodeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"a",@progbits
.LLSDA5542:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE5542-.LLSDACSB5542
.LLSDACSB5542:
	.uleb128 .LEHB56-.LFB5542
	.uleb128 .LEHE56-.LEHB56
	.uleb128 .L671-.LFB5542
	.uleb128 0
	.uleb128 .LEHB57-.LFB5542
	.uleb128 .LEHE57-.LEHB57
	.uleb128 .L672-.LFB5542
	.uleb128 0
	.uleb128 .LEHB58-.LFB5542
	.uleb128 .LEHE58-.LEHB58
	.uleb128 .L670-.LFB5542
	.uleb128 0
	.uleb128 .LEHB59-.LFB5542
	.uleb128 .LEHE59-.LEHB59
	.uleb128 .L673-.LFB5542
	.uleb128 0
	.uleb128 .LEHB60-.LFB5542
	.uleb128 .LEHE60-.LEHB60
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB61-.LFB5542
	.uleb128 .LEHE61-.LEHB61
	.uleb128 .L673-.LFB5542
	.uleb128 0
	.uleb128 .LEHB62-.LFB5542
	.uleb128 .LEHE62-.LEHB62
	.uleb128 0
	.uleb128 0
.LLSDACSE5542:
	.section	.text._ZN2v88internal6torque13FileUriDecodeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque13FileUriDecodeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC5542
	.type	_ZN2v88internal6torque13FileUriDecodeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.cold, @function
_ZN2v88internal6torque13FileUriDecodeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.cold:
.LFSB5542:
.L662:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-464(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L664
	call	_ZdlPv@PLT
.L664:
	movq	%r14, %rdi
	call	_ZNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
.L641:
	movq	-496(%rbp), %rdi
	cmpq	-536(%rbp), %rdi
	je	.L667
	call	_ZdlPv@PLT
.L667:
	movq	%r12, %rdi
.LEHB63:
	call	_Unwind_Resume@PLT
.LEHE63:
.L639:
	movq	%r12, %rdi
	call	_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED1Ev
	movq	8+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -432(%rbp,%rax)
.L640:
	movq	-544(%rbp), %rdi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rbx, %r12
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	jmp	.L641
	.cfi_endproc
.LFE5542:
	.section	.gcc_except_table._ZN2v88internal6torque13FileUriDecodeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
.LLSDAC5542:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC5542-.LLSDACSBC5542
.LLSDACSBC5542:
	.uleb128 .LEHB63-.LCOLDB45
	.uleb128 .LEHE63-.LEHB63
	.uleb128 0
	.uleb128 0
.LLSDACSEC5542:
	.section	.text.unlikely._ZN2v88internal6torque13FileUriDecodeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.text._ZN2v88internal6torque13FileUriDecodeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.size	_ZN2v88internal6torque13FileUriDecodeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN2v88internal6torque13FileUriDecodeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.text.unlikely._ZN2v88internal6torque13FileUriDecodeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.size	_ZN2v88internal6torque13FileUriDecodeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.cold, .-_ZN2v88internal6torque13FileUriDecodeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.cold
.LCOLDE45:
	.section	.text._ZN2v88internal6torque13FileUriDecodeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
.LHOTE45:
	.section	.rodata._ZNSt6vectorIN2v88internal6torque13TorqueMessageESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_.str1.1,"aMS",@progbits,1
.LC46:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorIN2v88internal6torque13TorqueMessageESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal6torque13TorqueMessageESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal6torque13TorqueMessageESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.type	_ZNSt6vectorIN2v88internal6torque13TorqueMessageESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, @function
_ZNSt6vectorIN2v88internal6torque13TorqueMessageESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_:
.LFB6781:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6781
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	movabsq	$144115188075855871, %rdx
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rbx
	movq	(%rdi), %r15
	movq	%rdi, -104(%rbp)
	movq	%rsi, -80(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rbx, %rax
	subq	%r15, %rax
	sarq	$6, %rax
	cmpq	%rdx, %rax
	je	.L738
	movq	%rsi, %r12
	subq	%r15, %r12
	testq	%rax, %rax
	je	.L719
	leaq	(%rax,%rax), %rsi
	movq	%rsi, -88(%rbp)
	cmpq	%rsi, %rax
	jbe	.L739
	movabsq	$9223372036854775744, %rax
	movq	%rax, -88(%rbp)
.L692:
	movq	-88(%rbp), %rdi
.LEHB64:
	call	_Znwm@PLT
.LEHE64:
	movq	%rax, -72(%rbp)
.L717:
	addq	-72(%rbp), %r12
	movq	8(%r13), %r14
	leaq	16(%r12), %rax
	movq	%rax, -96(%rbp)
	movq	%rax, (%r12)
	movq	0(%r13), %rax
	movq	%rax, %rsi
	movq	%rax, -112(%rbp)
	addq	%r14, %rsi
	je	.L694
	testq	%rax, %rax
	je	.L740
.L694:
	movq	%r14, -64(%rbp)
	cmpq	$15, %r14
	ja	.L741
	cmpq	$1, %r14
	jne	.L697
	movq	-112(%rbp), %rax
	movzbl	(%rax), %eax
	movb	%al, 16(%r12)
.L698:
	movq	-96(%rbp), %rax
	movq	%r14, 8(%r12)
	movb	$0, (%rax,%r14)
	movq	48(%r13), %rax
	movdqu	32(%r13), %xmm6
	movq	%rax, 48(%r12)
	movl	56(%r13), %eax
	movups	%xmm6, 32(%r12)
	movl	%eax, 56(%r12)
	movq	-80(%rbp), %rax
	cmpq	%r15, %rax
	je	.L721
	leaq	-64(%rax), %r13
	leaq	16(%r15), %r14
	subq	%r15, %r13
	leaq	16(%rax), %r12
	movq	%r13, -96(%rbp)
	movq	-72(%rbp), %r13
	.p2align 4,,10
	.p2align 3
.L705:
	leaq	16(%r13), %rdx
	movq	%rdx, 0(%r13)
	movq	-16(%r14), %rdx
	cmpq	%rdx, %r14
	je	.L742
	movq	%rdx, 0(%r13)
	movq	(%r14), %rdx
	movq	%rdx, 16(%r13)
.L701:
	movq	-8(%r14), %rdx
	movq	%rdx, 8(%r13)
	movdqu	16(%r14), %xmm1
	movq	%r14, -16(%r14)
	movq	$0, -8(%r14)
	movb	$0, (%r14)
	movups	%xmm1, 32(%r13)
	movq	32(%r14), %rdx
	movq	%rdx, 48(%r13)
	movl	40(%r14), %edx
	movl	%edx, 56(%r13)
	movq	-16(%r14), %rdi
	cmpq	%rdi, %r14
	je	.L702
	call	_ZdlPv@PLT
	addq	$64, %r14
	addq	$64, %r13
	cmpq	%r14, %r12
	jne	.L705
.L703:
	movq	-72(%rbp), %rax
	movq	-96(%rbp), %rsi
	leaq	64(%rax,%rsi), %r12
.L699:
	movq	-80(%rbp), %rax
	addq	$64, %r12
	cmpq	%rbx, %rax
	je	.L706
	movq	%r12, %rdx
	.p2align 4,,10
	.p2align 3
.L710:
	leaq	16(%rdx), %rcx
	leaq	16(%rax), %rsi
	movq	%rcx, (%rdx)
	movq	(%rax), %rcx
	cmpq	%rsi, %rcx
	je	.L743
	movq	%rcx, (%rdx)
	movq	16(%rax), %rcx
	addq	$64, %rax
	addq	$64, %rdx
	movdqu	-32(%rax), %xmm2
	movq	%rcx, -48(%rdx)
	movq	-56(%rax), %rcx
	movups	%xmm2, -32(%rdx)
	movq	%rcx, -56(%rdx)
	movq	-16(%rax), %rcx
	movq	%rcx, -16(%rdx)
	movl	-8(%rax), %ecx
	movl	%ecx, -8(%rdx)
	cmpq	%rbx, %rax
	jne	.L710
.L708:
	subq	-80(%rbp), %rbx
	addq	%rbx, %r12
.L706:
	testq	%r15, %r15
	je	.L711
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L711:
	movq	-72(%rbp), %rax
	movq	-104(%rbp), %rbx
	movq	%r12, %xmm7
	movq	%rax, %xmm0
	addq	-88(%rbp), %rax
	movq	%rax, 16(%rbx)
	punpcklqdq	%xmm7, %xmm0
	movups	%xmm0, (%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L744
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L739:
	.cfi_restore_state
	testq	%rsi, %rsi
	jne	.L693
	movq	$0, -72(%rbp)
	jmp	.L717
	.p2align 4,,10
	.p2align 3
.L702:
	addq	$64, %r14
	addq	$64, %r13
	cmpq	%r12, %r14
	jne	.L705
	jmp	.L703
	.p2align 4,,10
	.p2align 3
.L743:
	movq	8(%rax), %rcx
	movdqu	16(%rax), %xmm4
	addq	$64, %rax
	addq	$64, %rdx
	movdqu	-32(%rax), %xmm5
	movq	%rcx, -56(%rdx)
	movq	-16(%rax), %rcx
	movups	%xmm4, -48(%rdx)
	movq	%rcx, -16(%rdx)
	movl	-8(%rax), %ecx
	movups	%xmm5, -32(%rdx)
	movl	%ecx, -8(%rdx)
	cmpq	%rax, %rbx
	jne	.L710
	jmp	.L708
	.p2align 4,,10
	.p2align 3
.L742:
	movdqu	(%r14), %xmm3
	movups	%xmm3, 16(%r13)
	jmp	.L701
	.p2align 4,,10
	.p2align 3
.L719:
	movq	$64, -88(%rbp)
	jmp	.L692
	.p2align 4,,10
	.p2align 3
.L697:
	testq	%r14, %r14
	je	.L698
	movq	-96(%rbp), %rdi
	jmp	.L696
	.p2align 4,,10
	.p2align 3
.L741:
	leaq	-64(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
.LEHB65:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%r12)
	movq	%rax, %rdi
	movq	-64(%rbp), %rax
	movq	%rax, 16(%r12)
.L696:
	movq	-112(%rbp), %rsi
	movq	%r14, %rdx
	call	memcpy@PLT
	movq	(%r12), %rax
	movq	-64(%rbp), %r14
	movq	%rax, -96(%rbp)
	jmp	.L698
	.p2align 4,,10
	.p2align 3
.L721:
	movq	-72(%rbp), %r12
	jmp	.L699
.L740:
	leaq	.LC4(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE65:
.L744:
	call	__stack_chk_fail@PLT
.L693:
	movq	-88(%rbp), %rax
	cmpq	%rdx, %rax
	cmovbe	%rax, %rdx
	salq	$6, %rdx
	movq	%rdx, -88(%rbp)
	jmp	.L692
.L738:
	leaq	.LC46(%rip), %rdi
.LEHB66:
	call	_ZSt20__throw_length_errorPKc@PLT
.LEHE66:
.L722:
	endbr64
	movq	%rax, %rdi
.L712:
	call	__cxa_begin_catch@PLT
	cmpq	$0, -72(%rbp)
	je	.L745
	movq	-72(%rbp), %rdi
	call	_ZdlPv@PLT
.L716:
.LEHB67:
	call	__cxa_rethrow@PLT
.LEHE67:
.L745:
	movq	(%r12), %rdi
	cmpq	-96(%rbp), %rdi
	je	.L716
	call	_ZdlPv@PLT
	jmp	.L716
.L723:
	endbr64
	movq	%rax, %r12
.L715:
	call	__cxa_end_catch@PLT
	movq	%r12, %rdi
.LEHB68:
	call	_Unwind_Resume@PLT
.LEHE68:
	.cfi_endproc
.LFE6781:
	.section	.gcc_except_table._ZNSt6vectorIN2v88internal6torque13TorqueMessageESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,"aG",@progbits,_ZNSt6vectorIN2v88internal6torque13TorqueMessageESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,comdat
	.align 4
.LLSDA6781:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATT6781-.LLSDATTD6781
.LLSDATTD6781:
	.byte	0x1
	.uleb128 .LLSDACSE6781-.LLSDACSB6781
.LLSDACSB6781:
	.uleb128 .LEHB64-.LFB6781
	.uleb128 .LEHE64-.LEHB64
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB65-.LFB6781
	.uleb128 .LEHE65-.LEHB65
	.uleb128 .L722-.LFB6781
	.uleb128 0x1
	.uleb128 .LEHB66-.LFB6781
	.uleb128 .LEHE66-.LEHB66
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB67-.LFB6781
	.uleb128 .LEHE67-.LEHB67
	.uleb128 .L723-.LFB6781
	.uleb128 0
	.uleb128 .LEHB68-.LFB6781
	.uleb128 .LEHE68-.LEHB68
	.uleb128 0
	.uleb128 0
.LLSDACSE6781:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATT6781:
	.section	.text._ZNSt6vectorIN2v88internal6torque13TorqueMessageESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal6torque13TorqueMessageESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,comdat
	.size	_ZNSt6vectorIN2v88internal6torque13TorqueMessageESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, .-_ZNSt6vectorIN2v88internal6torque13TorqueMessageESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.section	.text._ZNK2v88internal6torque14MessageBuilder6ReportEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal6torque14MessageBuilder6ReportEv
	.type	_ZNK2v88internal6torque14MessageBuilder6ReportEv, @function
_ZNK2v88internal6torque14MessageBuilder6ReportEv:
.LFB5564:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%fs:_ZZN2v88internal6torque21ContextualVariableTopINS1_14TorqueMessagesEEERPNT_5ScopeEvE3top@tpoff, %rbx
	movq	8(%rbx), %r12
	cmpq	16(%rbx), %r12
	je	.L747
	leaq	16(%r12), %rax
	movq	%rax, (%r12)
	movq	(%rdi), %r15
	movq	8(%rdi), %r14
	movq	%r15, %rax
	addq	%r14, %rax
	je	.L748
	testq	%r15, %r15
	je	.L765
.L748:
	movq	%r14, -64(%rbp)
	cmpq	$15, %r14
	ja	.L766
	movq	(%r12), %rdi
	cmpq	$1, %r14
	jne	.L751
	movzbl	(%r15), %eax
	movb	%al, (%rdi)
	movq	-64(%rbp), %r14
	movq	(%r12), %rdi
.L752:
	movq	%r14, 8(%r12)
	movb	$0, (%rdi,%r14)
	movq	48(%r13), %rax
	movdqu	32(%r13), %xmm0
	movq	%rax, 48(%r12)
	movl	56(%r13), %eax
	movups	%xmm0, 32(%r12)
	movl	%eax, 56(%r12)
	addq	$64, 8(%rbx)
.L746:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L767
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L766:
	.cfi_restore_state
	movq	%r12, %rdi
	leaq	-64(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%r12)
	movq	%rax, %rdi
	movq	-64(%rbp), %rax
	movq	%rax, 16(%r12)
.L750:
	movq	%r14, %rdx
	movq	%r15, %rsi
	call	memcpy@PLT
	movq	-64(%rbp), %r14
	movq	(%r12), %rdi
	jmp	.L752
	.p2align 4,,10
	.p2align 3
.L747:
	movq	%rdi, %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZNSt6vectorIN2v88internal6torque13TorqueMessageESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	jmp	.L746
	.p2align 4,,10
	.p2align 3
.L751:
	testq	%r14, %r14
	je	.L752
	jmp	.L750
.L765:
	leaq	.LC4(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L767:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5564:
	.size	_ZNK2v88internal6torque14MessageBuilder6ReportEv, .-_ZNK2v88internal6torque14MessageBuilder6ReportEv
	.section	.text._ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructISt19istreambuf_iteratorIcS2_EEEvT_S8_St18input_iterator_tag,"axG",@progbits,_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructISt19istreambuf_iteratorIcS2_EEEvT_S8_St18input_iterator_tag,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructISt19istreambuf_iteratorIcS2_EEEvT_S8_St18input_iterator_tag
	.type	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructISt19istreambuf_iteratorIcS2_EEEvT_S8_St18input_iterator_tag, @function
_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructISt19istreambuf_iteratorIcS2_EEEvT_S8_St18input_iterator_tag:
.LFB7225:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA7225
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$-1, %r8d
	movq	$15, -64(%rbp)
	sete	-73(%rbp)
	xorl	%r12d, %r12d
	jmp	.L778
	.p2align 4,,10
	.p2align 3
.L801:
	movzbl	-73(%rbp), %edx
.L770:
	cmpb	%cl, %dl
	je	.L794
	cmpq	%r12, -64(%rbp)
	jbe	.L794
	leaq	1(%r12), %rdx
	addq	(%r14), %r12
	testb	%sil, %sil
	jne	.L824
.L773:
	movb	%r13b, (%r12)
	movq	16(%rbx), %rax
	cmpq	24(%rbx), %rax
	jnb	.L776
.L836:
	addq	$1, %rax
	movq	%rax, 16(%rbx)
.L777:
	movq	%rdx, %r12
	movl	$-1, %r13d
.L778:
	cmpl	$-1, %r13d
	sete	%cl
	testq	%rbx, %rbx
	setne	%al
	andb	%cl, %al
	movl	%eax, %esi
	jne	.L825
.L769:
	testq	%r15, %r15
	setne	%dl
	andb	-73(%rbp), %dl
	je	.L801
	movq	24(%r15), %rax
	cmpq	%rax, 16(%r15)
	jnb	.L826
	xorl	%edx, %edx
	jmp	.L770
	.p2align 4,,10
	.p2align 3
.L830:
	movq	(%r14), %rcx
.L783:
	addq	%rcx, %r12
	testq	%rbx, %rbx
	je	.L789
	cmpb	$0, -72(%rbp)
	jne	.L827
.L789:
	movb	%r13b, (%r12)
	movq	16(%rbx), %rax
	cmpq	24(%rbx), %rax
	jnb	.L792
.L837:
	addq	$1, %rax
	movq	%rax, 16(%rbx)
.L793:
	movq	-88(%rbp), %r12
	movl	$-1, %r13d
.L794:
	cmpl	$-1, %r13d
	sete	-72(%rbp)
	testq	%rbx, %rbx
	movzbl	-72(%rbp), %edi
	setne	%al
	andb	%dil, %al
	movb	%al, -88(%rbp)
	jne	.L828
	movb	%dil, -88(%rbp)
.L779:
	testq	%r15, %r15
	setne	%al
	andb	-73(%rbp), %al
	movb	%al, -96(%rbp)
	jne	.L829
	movzbl	-73(%rbp), %eax
	movb	%al, -96(%rbp)
	movl	%eax, %esi
.L780:
	cmpb	%sil, -88(%rbp)
	je	.L781
.L833:
	leaq	1(%r12), %rax
	movq	%rax, -88(%rbp)
	cmpq	%r12, -64(%rbp)
	jne	.L830
	leaq	1(%r12), %rax
	leaq	-64(%rbp), %rsi
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%rax, -64(%rbp)
.LEHB69:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE69:
	movq	(%r14), %rdi
	movq	%rax, %rcx
	cmpq	$1, %r12
	je	.L831
	testq	%r12, %r12
	je	.L785
	movq	%rdi, %rsi
	movq	%r12, %rdx
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	(%r14), %rdi
	movq	%rax, %rcx
.L785:
	leaq	16(%r14), %rax
	cmpq	%rax, %rdi
	je	.L786
	movq	%rcx, -96(%rbp)
	call	_ZdlPv@PLT
	movq	-96(%rbp), %rcx
.L786:
	movq	-64(%rbp), %rax
	movq	%rcx, (%r14)
	movq	%rax, 16(%r14)
	jmp	.L783
	.p2align 4,,10
	.p2align 3
.L825:
	movq	24(%rbx), %rax
	xorl	%ecx, %ecx
	cmpq	%rax, 16(%rbx)
	jb	.L769
	movq	(%rbx), %rax
	movb	%cl, -88(%rbp)
	movq	%rbx, %rdi
	movb	%sil, -72(%rbp)
.LEHB70:
	call	*72(%rax)
	movzbl	-72(%rbp), %esi
	movzbl	-88(%rbp), %ecx
	cmpl	$-1, %eax
	jne	.L769
	movl	%esi, %ecx
	xorl	%ebx, %ebx
	xorl	%esi, %esi
	jmp	.L769
	.p2align 4,,10
	.p2align 3
.L829:
	movq	24(%r15), %rax
	cmpq	%rax, 16(%r15)
	jnb	.L832
	movb	$0, -96(%rbp)
	movzbl	-96(%rbp), %esi
	cmpb	%sil, -88(%rbp)
	jne	.L833
.L781:
	movq	(%r14), %rax
	movq	%r12, 8(%r14)
	movb	$0, (%rax,%r12)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L834
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L828:
	.cfi_restore_state
	movq	24(%rbx), %rax
	cmpq	%rax, 16(%rbx)
	jnb	.L835
	movb	$0, -88(%rbp)
	jmp	.L779
	.p2align 4,,10
	.p2align 3
.L824:
	movq	16(%rbx), %rax
	cmpq	24(%rbx), %rax
	jnb	.L774
	movzbl	(%rax), %r13d
	movb	%r13b, (%r12)
	movq	16(%rbx), %rax
	cmpq	24(%rbx), %rax
	jb	.L836
.L776:
	movq	(%rbx), %rax
	movq	%rdx, -72(%rbp)
	movq	%rbx, %rdi
	call	*80(%rax)
.LEHE70:
	movq	-72(%rbp), %rdx
	jmp	.L777
	.p2align 4,,10
	.p2align 3
.L827:
	movq	16(%rbx), %rax
	cmpq	24(%rbx), %rax
	jnb	.L790
	movzbl	(%rax), %r13d
	movb	%r13b, (%r12)
	movq	16(%rbx), %rax
	cmpq	24(%rbx), %rax
	jb	.L837
.L792:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
.LEHB71:
	call	*80(%rax)
.LEHE71:
	jmp	.L793
	.p2align 4,,10
	.p2align 3
.L831:
	movzbl	(%rdi), %eax
	movb	%al, (%rcx)
	movq	(%r14), %rdi
	jmp	.L785
	.p2align 4,,10
	.p2align 3
.L826:
	movq	(%r15), %rax
	movb	%cl, -96(%rbp)
	movq	%r15, %rdi
	movb	%sil, -88(%rbp)
	movb	%dl, -72(%rbp)
.LEHB72:
	call	*72(%rax)
.LEHE72:
	movzbl	-72(%rbp), %edx
	movl	$0, %ecx
	movzbl	-88(%rbp), %esi
	cmpl	$-1, %eax
	movl	$0, %eax
	cmovne	%ecx, %edx
	cmove	%rax, %r15
	movzbl	-96(%rbp), %ecx
	jmp	.L770
	.p2align 4,,10
	.p2align 3
.L835:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
.LEHB73:
	call	*72(%rax)
	cmpl	$-1, %eax
	movl	$0, %ecx
	movl	$0, %eax
	cmove	%rcx, %rbx
	movzbl	-88(%rbp), %ecx
	cmovne	%eax, %ecx
	movb	%cl, -88(%rbp)
	jmp	.L779
	.p2align 4,,10
	.p2align 3
.L832:
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*72(%rax)
.LEHE73:
	cmpl	$-1, %eax
	movl	$0, %ecx
	movl	$0, %eax
	cmove	%rcx, %r15
	movzbl	-96(%rbp), %ecx
	cmovne	%eax, %ecx
	movb	%cl, -96(%rbp)
	movl	%ecx, %esi
	jmp	.L780
	.p2align 4,,10
	.p2align 3
.L774:
	movq	(%rbx), %rax
	movq	%rdx, -72(%rbp)
	movq	%rbx, %rdi
.LEHB74:
	call	*72(%rax)
.LEHE74:
	movl	%eax, %r13d
	cmpl	$-1, %eax
	je	.L791
	movq	-72(%rbp), %rdx
	jmp	.L773
	.p2align 4,,10
	.p2align 3
.L790:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
.LEHB75:
	call	*72(%rax)
.LEHE75:
	movl	%eax, %r13d
	cmpl	$-1, %eax
	jne	.L789
.L791:
	movb	$-1, (%r12)
	movq	16, %rax
	ud2
.L834:
	call	__stack_chk_fail@PLT
.L810:
	endbr64
	movq	%rax, %rdi
.L795:
	call	__cxa_begin_catch@PLT
	movq	(%r14), %rdi
	addq	$16, %r14
	cmpq	%r14, %rdi
	je	.L796
	call	_ZdlPv@PLT
.L796:
.LEHB76:
	call	__cxa_rethrow@PLT
.LEHE76:
.L811:
	endbr64
	movq	%rax, %r12
.L797:
	call	__cxa_end_catch@PLT
	movq	%r12, %rdi
.LEHB77:
	call	_Unwind_Resume@PLT
.LEHE77:
	.cfi_endproc
.LFE7225:
	.section	.gcc_except_table._ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructISt19istreambuf_iteratorIcS2_EEEvT_S8_St18input_iterator_tag,"aG",@progbits,_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructISt19istreambuf_iteratorIcS2_EEEvT_S8_St18input_iterator_tag,comdat
	.align 4
.LLSDA7225:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATT7225-.LLSDATTD7225
.LLSDATTD7225:
	.byte	0x1
	.uleb128 .LLSDACSE7225-.LLSDACSB7225
.LLSDACSB7225:
	.uleb128 .LEHB69-.LFB7225
	.uleb128 .LEHE69-.LEHB69
	.uleb128 .L810-.LFB7225
	.uleb128 0x1
	.uleb128 .LEHB70-.LFB7225
	.uleb128 .LEHE70-.LEHB70
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB71-.LFB7225
	.uleb128 .LEHE71-.LEHB71
	.uleb128 .L810-.LFB7225
	.uleb128 0x1
	.uleb128 .LEHB72-.LFB7225
	.uleb128 .LEHE72-.LEHB72
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB73-.LFB7225
	.uleb128 .LEHE73-.LEHB73
	.uleb128 .L810-.LFB7225
	.uleb128 0x1
	.uleb128 .LEHB74-.LFB7225
	.uleb128 .LEHE74-.LEHB74
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB75-.LFB7225
	.uleb128 .LEHE75-.LEHB75
	.uleb128 .L810-.LFB7225
	.uleb128 0x1
	.uleb128 .LEHB76-.LFB7225
	.uleb128 .LEHE76-.LEHB76
	.uleb128 .L811-.LFB7225
	.uleb128 0
	.uleb128 .LEHB77-.LFB7225
	.uleb128 .LEHE77-.LEHB77
	.uleb128 0
	.uleb128 0
.LLSDACSE7225:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATT7225:
	.section	.text._ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructISt19istreambuf_iteratorIcS2_EEEvT_S8_St18input_iterator_tag,"axG",@progbits,_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructISt19istreambuf_iteratorIcS2_EEEvT_S8_St18input_iterator_tag,comdat
	.size	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructISt19istreambuf_iteratorIcS2_EEEvT_S8_St18input_iterator_tag, .-_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructISt19istreambuf_iteratorIcS2_EEEvT_S8_St18input_iterator_tag
	.section	.text.unlikely._ZN2v88internal6torque30ReplaceFileContentsIfDifferentERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES9_,"ax",@progbits
.LCOLDB48:
	.section	.text._ZN2v88internal6torque30ReplaceFileContentsIfDifferentERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES9_,"ax",@progbits
.LHOTB48:
	.p2align 4
	.globl	_ZN2v88internal6torque30ReplaceFileContentsIfDifferentERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES9_
	.type	_ZN2v88internal6torque30ReplaceFileContentsIfDifferentERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES9_, @function
_ZN2v88internal6torque30ReplaceFileContentsIfDifferentERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES9_:
.LFB5587:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA5587
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-320(%rbp), %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	-576(%rbp), %rbx
	subq	$1112, %rsp
	movq	%rdi, -1144(%rbp)
	movq	(%rdi), %r13
	movq	%r14, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rbx, -1152(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	movq	%rax, -320(%rbp)
	movq	8+_ZTTSt14basic_ifstreamIcSt11char_traitsIcEE(%rip), %rax
	pxor	%xmm0, %xmm0
	movw	%cx, -96(%rbp)
	movq	16+_ZTTSt14basic_ifstreamIcSt11char_traitsIcEE(%rip), %rcx
	movq	%rax, -576(%rbp)
	movq	$0, -104(%rbp)
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -576(%rbp,%rax)
	movq	-576(%rbp), %rax
	movq	$0, -568(%rbp)
	movq	-24(%rax), %rdi
	addq	%rbx, %rdi
.LEHB78:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE78:
	leaq	24+_ZTVSt14basic_ifstreamIcSt11char_traitsIcEE(%rip), %rax
	leaq	-560(%rbp), %r12
	movq	%rax, -576(%rbp)
	movq	%r12, %rdi
	addq	$40, %rax
	movq	%rax, -320(%rbp)
.LEHB79:
	call	_ZNSt13basic_filebufIcSt11char_traitsIcEEC1Ev@PLT
.LEHE79:
	movq	%r12, %rsi
	movq	%r14, %rdi
.LEHB80:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movl	$8, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNSt13basic_filebufIcSt11char_traitsIcEE4openEPKcSt13_Ios_Openmode@PLT
	movq	-576(%rbp), %rdx
	addq	-24(%rdx), %rbx
	movq	%rbx, %rdi
	testq	%rax, %rax
	je	.L897
	xorl	%esi, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
.LEHE80:
.L840:
	movl	-288(%rbp), %edx
	leaq	-1104(%rbp), %rax
	movq	$0, -1112(%rbp)
	movq	%rax, -1136(%rbp)
	movq	%rax, -1120(%rbp)
	movb	$0, -1104(%rbp)
	testl	%edx, %edx
	je	.L841
	leaq	-1088(%rbp), %rbx
.L854:
	leaq	-840(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -1128(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	movq	%rax, -840(%rbp)
	xorl	%eax, %eax
	movq	16+_ZTTSt14basic_ofstreamIcSt11char_traitsIcEE(%rip), %rcx
	movw	%ax, -616(%rbp)
	movq	8+_ZTTSt14basic_ofstreamIcSt11char_traitsIcEE(%rip), %rax
	movq	$0, -624(%rbp)
	movq	%rax, -1088(%rbp)
	movaps	%xmm0, -608(%rbp)
	movaps	%xmm0, -592(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -1088(%rbp,%rax)
	movq	-1088(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%rbx, %rdi
.LEHB81:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE81:
	leaq	24+_ZTVSt14basic_ofstreamIcSt11char_traitsIcEE(%rip), %rax
	leaq	-1080(%rbp), %r13
	movq	%rax, -1088(%rbp)
	movq	%r13, %rdi
	addq	$40, %rax
	movq	%rax, -840(%rbp)
.LEHB82:
	call	_ZNSt13basic_filebufIcSt11char_traitsIcEEC1Ev@PLT
.LEHE82:
	movq	-1128(%rbp), %rdi
	movq	%r13, %rsi
.LEHB83:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE83:
	movq	-1144(%rbp), %rax
	movl	$16, %edx
	movq	%r13, %rdi
	movq	(%rax), %rsi
.LEHB84:
	call	_ZNSt13basic_filebufIcSt11char_traitsIcEE4openEPKcSt13_Ios_Openmode@PLT
	movq	-1088(%rbp), %rdx
	movq	-24(%rdx), %rdi
	addq	%rbx, %rdi
	testq	%rax, %rax
	je	.L898
	xorl	%esi, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
.L862:
	movq	8(%r15), %rdx
	movq	(%r15), %rsi
	movq	%rbx, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r13, %rdi
	call	_ZNSt13basic_filebufIcSt11char_traitsIcEE5closeEv@PLT
.LEHE84:
	testq	%rax, %rax
	je	.L899
.L863:
	movq	.LC47(%rip), %xmm0
	leaq	16+_ZTVSt13basic_filebufIcSt11char_traitsIcEE(%rip), %r15
	leaq	64+_ZTVSt14basic_ofstreamIcSt11char_traitsIcEE(%rip), %rax
	movq	%r13, %rdi
	movq	%r15, %xmm1
	movq	%rax, -840(%rbp)
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -1088(%rbp)
.LEHB85:
	call	_ZNSt13basic_filebufIcSt11char_traitsIcEE5closeEv@PLT
.LEHE85:
.L865:
	leaq	-976(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rbx
	call	_ZNSt12__basic_fileIcED1Ev@PLT
	leaq	-1024(%rbp), %rdi
	movq	%rbx, -1080(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTSt14basic_ofstreamIcSt11char_traitsIcEE(%rip), %rax
	movq	16+_ZTTSt14basic_ofstreamIcSt11char_traitsIcEE(%rip), %rcx
	movq	-1128(%rbp), %rdi
	movq	%rax, -1088(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -1088(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -840(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-1120(%rbp), %r13
.L855:
	cmpq	-1136(%rbp), %r13
	je	.L866
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L866:
	leaq	24+_ZTVSt14basic_ifstreamIcSt11char_traitsIcEE(%rip), %rax
	movq	%r12, %rdi
	movq	%r15, -560(%rbp)
	movq	%rax, -576(%rbp)
	addq	$40, %rax
	movq	%rax, -320(%rbp)
.LEHB86:
	call	_ZNSt13basic_filebufIcSt11char_traitsIcEE5closeEv@PLT
.LEHE86:
.L868:
	leaq	-456(%rbp), %rdi
	call	_ZNSt12__basic_fileIcED1Ev@PLT
	leaq	-504(%rbp), %rdi
	movq	%rbx, -560(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTSt14basic_ifstreamIcSt11char_traitsIcEE(%rip), %rax
	movq	16+_ZTTSt14basic_ifstreamIcSt11char_traitsIcEE(%rip), %rcx
	movq	%r14, %rdi
	movq	%rax, -576(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -576(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	movq	$0, -568(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L900
	addq	$1112, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L841:
	.cfi_restore_state
	movq	-576(%rbp), %rax
	leaq	-1088(%rbp), %rbx
	movl	$4294967295, %edx
	xorl	%ecx, %ecx
	leaq	-1072(%rbp), %r13
	movq	%rdx, %r8
	movq	%rbx, %rdi
	movq	-24(%rax), %rax
	movq	%r13, -1088(%rbp)
	movq	-344(%rbp,%rax), %rsi
.LEHB87:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructISt19istreambuf_iteratorIcS2_EEEvT_S8_St18input_iterator_tag
	movq	-1088(%rbp), %rax
	movq	-1120(%rbp), %rdi
	movq	-1080(%rbp), %rdx
	cmpq	%r13, %rax
	je	.L901
	movq	-1072(%rbp), %rcx
	cmpq	-1136(%rbp), %rdi
	je	.L902
	movq	%rdx, %xmm0
	movq	%rcx, %xmm2
	movq	-1104(%rbp), %rsi
	movq	%rax, -1120(%rbp)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, -1112(%rbp)
	testq	%rdi, %rdi
	je	.L851
	movq	%rdi, -1088(%rbp)
	movq	%rsi, -1072(%rbp)
.L849:
	movq	$0, -1080(%rbp)
	movb	$0, (%rdi)
	movq	-1088(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L852
	call	_ZdlPv@PLT
.L852:
	movq	%r12, %rdi
	call	_ZNSt13basic_filebufIcSt11char_traitsIcEE5closeEv@PLT
.LEHE87:
	testq	%rax, %rax
	je	.L903
.L853:
	movq	-1112(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L854
	cmpq	8(%r15), %rdx
	jne	.L854
	movq	-1120(%rbp), %r13
	movq	(%r15), %rsi
	movq	%r13, %rdi
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L854
	leaq	16+_ZTVSt13basic_filebufIcSt11char_traitsIcEE(%rip), %r15
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rbx
	jmp	.L855
	.p2align 4,,10
	.p2align 3
.L897:
	movl	32(%rbx), %esi
	orl	$4, %esi
.LEHB88:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
.LEHE88:
	jmp	.L840
	.p2align 4,,10
	.p2align 3
.L898:
	movl	32(%rdi), %esi
	orl	$4, %esi
.LEHB89:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L862
	.p2align 4,,10
	.p2align 3
.L902:
	movq	%rdx, %xmm0
	movq	%rcx, %xmm3
	movq	%rax, -1120(%rbp)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, -1112(%rbp)
.L851:
	movq	%r13, -1088(%rbp)
	leaq	-1072(%rbp), %r13
	movq	%r13, %rdi
	jmp	.L849
	.p2align 4,,10
	.p2align 3
.L899:
	movq	-1088(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%rbx, %rdi
	movl	32(%rdi), %esi
	orl	$4, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
.LEHE89:
	jmp	.L863
	.p2align 4,,10
	.p2align 3
.L901:
	testq	%rdx, %rdx
	je	.L847
	cmpq	$1, %rdx
	je	.L904
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	-1080(%rbp), %rdx
	movq	-1120(%rbp), %rdi
.L847:
	movq	%rdx, -1112(%rbp)
	movb	$0, (%rdi,%rdx)
	movq	-1088(%rbp), %rdi
	jmp	.L849
	.p2align 4,,10
	.p2align 3
.L903:
	movq	-576(%rbp), %rax
	movq	-1152(%rbp), %rdi
	addq	-24(%rax), %rdi
	movl	32(%rdi), %esi
	orl	$4, %esi
.LEHB90:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
.LEHE90:
	jmp	.L853
	.p2align 4,,10
	.p2align 3
.L904:
	movzbl	-1072(%rbp), %eax
	movb	%al, (%rdi)
	movq	-1080(%rbp), %rdx
	movq	-1120(%rbp), %rdi
	jmp	.L847
.L900:
	call	__stack_chk_fail@PLT
.L875:
	endbr64
	movq	%rax, %rbx
	jmp	.L844
.L874:
	endbr64
	movq	%rax, %r12
	jmp	.L845
.L876:
	endbr64
	movq	%rax, %rbx
	jmp	.L843
.L878:
	endbr64
	movq	%rax, %r12
	jmp	.L858
.L881:
	endbr64
	movq	%rax, %rdi
	jmp	.L867
.L872:
	endbr64
	movq	%rax, %r12
	jmp	.L860
.L873:
	endbr64
	movq	%rax, %r12
	jmp	.L869
.L877:
	endbr64
	movq	%rax, %r12
	jmp	.L859
.L879:
	endbr64
	movq	%rax, %rbx
	jmp	.L857
.L880:
	endbr64
	movq	%rax, %rdi
	jmp	.L864
	.section	.gcc_except_table._ZN2v88internal6torque30ReplaceFileContentsIfDifferentERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES9_,"a",@progbits
	.align 4
.LLSDA5587:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATT5587-.LLSDATTD5587
.LLSDATTD5587:
	.byte	0x1
	.uleb128 .LLSDACSE5587-.LLSDACSB5587
.LLSDACSB5587:
	.uleb128 .LEHB78-.LFB5587
	.uleb128 .LEHE78-.LEHB78
	.uleb128 .L874-.LFB5587
	.uleb128 0
	.uleb128 .LEHB79-.LFB5587
	.uleb128 .LEHE79-.LEHB79
	.uleb128 .L875-.LFB5587
	.uleb128 0
	.uleb128 .LEHB80-.LFB5587
	.uleb128 .LEHE80-.LEHB80
	.uleb128 .L876-.LFB5587
	.uleb128 0
	.uleb128 .LEHB81-.LFB5587
	.uleb128 .LEHE81-.LEHB81
	.uleb128 .L877-.LFB5587
	.uleb128 0
	.uleb128 .LEHB82-.LFB5587
	.uleb128 .LEHE82-.LEHB82
	.uleb128 .L878-.LFB5587
	.uleb128 0
	.uleb128 .LEHB83-.LFB5587
	.uleb128 .LEHE83-.LEHB83
	.uleb128 .L879-.LFB5587
	.uleb128 0
	.uleb128 .LEHB84-.LFB5587
	.uleb128 .LEHE84-.LEHB84
	.uleb128 .L873-.LFB5587
	.uleb128 0
	.uleb128 .LEHB85-.LFB5587
	.uleb128 .LEHE85-.LEHB85
	.uleb128 .L880-.LFB5587
	.uleb128 0x1
	.uleb128 .LEHB86-.LFB5587
	.uleb128 .LEHE86-.LEHB86
	.uleb128 .L881-.LFB5587
	.uleb128 0x1
	.uleb128 .LEHB87-.LFB5587
	.uleb128 .LEHE87-.LEHB87
	.uleb128 .L872-.LFB5587
	.uleb128 0
	.uleb128 .LEHB88-.LFB5587
	.uleb128 .LEHE88-.LEHB88
	.uleb128 .L876-.LFB5587
	.uleb128 0
	.uleb128 .LEHB89-.LFB5587
	.uleb128 .LEHE89-.LEHB89
	.uleb128 .L873-.LFB5587
	.uleb128 0
	.uleb128 .LEHB90-.LFB5587
	.uleb128 .LEHE90-.LEHB90
	.uleb128 .L872-.LFB5587
	.uleb128 0
.LLSDACSE5587:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATT5587:
	.section	.text._ZN2v88internal6torque30ReplaceFileContentsIfDifferentERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES9_
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque30ReplaceFileContentsIfDifferentERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES9_
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC5587
	.type	_ZN2v88internal6torque30ReplaceFileContentsIfDifferentERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES9_.cold, @function
_ZN2v88internal6torque30ReplaceFileContentsIfDifferentERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES9_.cold:
.LFSB5587:
.L843:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	%r12, %rdi
	call	_ZNSt13basic_filebufIcSt11char_traitsIcEED1Ev@PLT
.L844:
	movq	8+_ZTTSt14basic_ifstreamIcSt11char_traitsIcEE(%rip), %rax
	movq	16+_ZTTSt14basic_ifstreamIcSt11char_traitsIcEE(%rip), %rcx
	movq	%rbx, %r12
	movq	%rax, -576(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -576(%rbp,%rax)
	movq	$0, -568(%rbp)
.L845:
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%r14, %rdi
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	%r12, %rdi
.LEHB91:
	call	_Unwind_Resume@PLT
.L857:
	movq	%r13, %rdi
	movq	%rbx, %r12
	call	_ZNSt13basic_filebufIcSt11char_traitsIcEED1Ev@PLT
.L858:
	movq	8+_ZTTSt14basic_ofstreamIcSt11char_traitsIcEE(%rip), %rax
	movq	16+_ZTTSt14basic_ofstreamIcSt11char_traitsIcEE(%rip), %rcx
	movq	%rax, -1088(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -1088(%rbp,%rax)
.L859:
	movq	-1128(%rbp), %rdi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -840(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
.L860:
	movq	-1120(%rbp), %rdi
	cmpq	-1136(%rbp), %rdi
	je	.L870
	call	_ZdlPv@PLT
.L870:
	movq	-1152(%rbp), %rdi
	call	_ZNSt14basic_ifstreamIcSt11char_traitsIcEED1Ev@PLT
	movq	%r12, %rdi
	call	_Unwind_Resume@PLT
.LEHE91:
.L867:
	call	__cxa_begin_catch@PLT
	call	__cxa_end_catch@PLT
	jmp	.L868
.L869:
	movq	%rbx, %rdi
	call	_ZNSt14basic_ofstreamIcSt11char_traitsIcEED1Ev@PLT
	jmp	.L860
.L864:
	call	__cxa_begin_catch@PLT
	call	__cxa_end_catch@PLT
	jmp	.L865
	.cfi_endproc
.LFE5587:
	.section	.gcc_except_table._ZN2v88internal6torque30ReplaceFileContentsIfDifferentERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES9_
	.align 4
.LLSDAC5587:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATTC5587-.LLSDATTDC5587
.LLSDATTDC5587:
	.byte	0x1
	.uleb128 .LLSDACSEC5587-.LLSDACSBC5587
.LLSDACSBC5587:
	.uleb128 .LEHB91-.LCOLDB48
	.uleb128 .LEHE91-.LEHB91
	.uleb128 0
	.uleb128 0
.LLSDACSEC5587:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATTC5587:
	.section	.text.unlikely._ZN2v88internal6torque30ReplaceFileContentsIfDifferentERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES9_
	.section	.text._ZN2v88internal6torque30ReplaceFileContentsIfDifferentERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES9_
	.size	_ZN2v88internal6torque30ReplaceFileContentsIfDifferentERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES9_, .-_ZN2v88internal6torque30ReplaceFileContentsIfDifferentERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES9_
	.section	.text.unlikely._ZN2v88internal6torque30ReplaceFileContentsIfDifferentERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES9_
	.size	_ZN2v88internal6torque30ReplaceFileContentsIfDifferentERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES9_.cold, .-_ZN2v88internal6torque30ReplaceFileContentsIfDifferentERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES9_.cold
.LCOLDE48:
	.section	.text._ZN2v88internal6torque30ReplaceFileContentsIfDifferentERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES9_
.LHOTE48:
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal6torque21ContextualVariableTopINS1_14TorqueMessagesEEERPNT_5ScopeEv,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal6torque21ContextualVariableTopINS1_14TorqueMessagesEEERPNT_5ScopeEv, @function
_GLOBAL__sub_I__ZN2v88internal6torque21ContextualVariableTopINS1_14TorqueMessagesEEERPNT_5ScopeEv:
.LFB7797:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE7797:
	.size	_GLOBAL__sub_I__ZN2v88internal6torque21ContextualVariableTopINS1_14TorqueMessagesEEERPNT_5ScopeEv, .-_GLOBAL__sub_I__ZN2v88internal6torque21ContextualVariableTopINS1_14TorqueMessagesEEERPNT_5ScopeEv
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal6torque21ContextualVariableTopINS1_14TorqueMessagesEEERPNT_5ScopeEv
	.weak	_ZTSN2v88internal6torque22TorqueAbortCompilationE
	.section	.rodata._ZTSN2v88internal6torque22TorqueAbortCompilationE,"aG",@progbits,_ZTSN2v88internal6torque22TorqueAbortCompilationE,comdat
	.align 32
	.type	_ZTSN2v88internal6torque22TorqueAbortCompilationE, @object
	.size	_ZTSN2v88internal6torque22TorqueAbortCompilationE, 46
_ZTSN2v88internal6torque22TorqueAbortCompilationE:
	.string	"N2v88internal6torque22TorqueAbortCompilationE"
	.weak	_ZTIN2v88internal6torque22TorqueAbortCompilationE
	.section	.data.rel.ro._ZTIN2v88internal6torque22TorqueAbortCompilationE,"awG",@progbits,_ZTIN2v88internal6torque22TorqueAbortCompilationE,comdat
	.align 8
	.type	_ZTIN2v88internal6torque22TorqueAbortCompilationE, @object
	.size	_ZTIN2v88internal6torque22TorqueAbortCompilationE, 16
_ZTIN2v88internal6torque22TorqueAbortCompilationE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN2v88internal6torque22TorqueAbortCompilationE
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC49:
	.string	"void"
.LC50:
	.string	"never"
.LC51:
	.string	"int8"
.LC52:
	.string	"uint8"
.LC53:
	.string	"int16"
.LC54:
	.string	"uint16"
.LC55:
	.string	"int31"
.LC56:
	.string	"uint31"
.LC57:
	.string	"int32"
.LC58:
	.string	"uint32"
.LC59:
	.string	"int64"
.LC60:
	.string	"intptr"
.LC61:
	.string	"uintptr"
.LC62:
	.string	"float32"
.LC63:
	.string	"float64"
.LC64:
	.string	"bool"
.LC65:
	.string	"string"
.LC66:
	.string	"bint"
.LC67:
	.string	"char8"
.LC68:
	.string	"char16"
	.section	.data.rel.ro.local._ZZN2v88internal6torque12_GLOBAL__N_113IsMachineTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE13machine_types,"aw"
	.align 32
	.type	_ZZN2v88internal6torque12_GLOBAL__N_113IsMachineTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE13machine_types, @object
	.size	_ZZN2v88internal6torque12_GLOBAL__N_113IsMachineTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE13machine_types, 160
_ZZN2v88internal6torque12_GLOBAL__N_113IsMachineTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE13machine_types:
	.quad	.LC49
	.quad	.LC50
	.quad	.LC51
	.quad	.LC52
	.quad	.LC53
	.quad	.LC54
	.quad	.LC55
	.quad	.LC56
	.quad	.LC57
	.quad	.LC58
	.quad	.LC59
	.quad	.LC60
	.quad	.LC61
	.quad	.LC62
	.quad	.LC63
	.quad	.LC64
	.quad	.LC65
	.quad	.LC66
	.quad	.LC67
	.quad	.LC68
	.section	.rodata._ZN2v88internal6torqueL14kFileUriPrefixE,"a"
	.align 8
	.type	_ZN2v88internal6torqueL14kFileUriPrefixE, @object
	.size	_ZN2v88internal6torqueL14kFileUriPrefixE, 8
_ZN2v88internal6torqueL14kFileUriPrefixE:
	.string	"file://"
	.section	.tbss._ZZN2v88internal6torque21ContextualVariableTopINS1_14TorqueMessagesEEERPNT_5ScopeEvE3top,"awT",@nobits
	.align 8
	.type	_ZZN2v88internal6torque21ContextualVariableTopINS1_14TorqueMessagesEEERPNT_5ScopeEvE3top, @object
	.size	_ZZN2v88internal6torque21ContextualVariableTopINS1_14TorqueMessagesEEERPNT_5ScopeEvE3top, 8
_ZZN2v88internal6torque21ContextualVariableTopINS1_14TorqueMessagesEEERPNT_5ScopeEvE3top:
	.zero	8
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.data.rel.ro,"aw"
	.align 8
.LC35:
	.quad	_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE+64
	.align 8
.LC36:
	.quad	_ZTVSt15basic_streambufIcSt11char_traitsIcEE+16
	.align 8
.LC37:
	.quad	_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE+16
	.align 8
.LC44:
	.quad	_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE+24
	.align 8
.LC47:
	.quad	_ZTVSt14basic_ofstreamIcSt11char_traitsIcEE+24
	.hidden	DW.ref.__gxx_personality_v0
	.weak	DW.ref.__gxx_personality_v0
	.section	.data.rel.local.DW.ref.__gxx_personality_v0,"awG",@progbits,DW.ref.__gxx_personality_v0,comdat
	.align 8
	.type	DW.ref.__gxx_personality_v0, @object
	.size	DW.ref.__gxx_personality_v0, 8
DW.ref.__gxx_personality_v0:
	.quad	__gxx_personality_v0
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
