	.file	"declarable.cc"
	.text
	.section	.text._ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv
	.type	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv, @function
_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv:
.LFB6563:
	.cfi_startproc
	endbr64
	movq	%fs:0, %rax
	addq	$_ZZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEvE3top@tpoff, %rax
	ret
	.cfi_endproc
.LFE6563:
	.size	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv, .-_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv
	.section	.rodata._ZN2v88internal6torquelsERSoRKNS1_13QualifiedNameE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"::"
	.section	.text._ZN2v88internal6torquelsERSoRKNS1_13QualifiedNameE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal6torquelsERSoRKNS1_13QualifiedNameE
	.type	_ZN2v88internal6torquelsERSoRKNS1_13QualifiedNameE, @function
_ZN2v88internal6torquelsERSoRKNS1_13QualifiedNameE:
.LFB6564:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rsi), %rbx
	movq	8(%rsi), %r12
	cmpq	%r12, %rbx
	je	.L4
	leaq	.LC0(%rip), %r14
	.p2align 4,,10
	.p2align 3
.L5:
	movq	8(%rbx), %rdx
	movq	(%rbx), %rsi
	movq	%r15, %rdi
	addq	$32, %rbx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$2, %edx
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpq	%rbx, %r12
	jne	.L5
.L4:
	movq	32(%r13), %rdx
	movq	24(%r13), %rsi
	addq	$8, %rsp
	movq	%r15, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	.cfi_endproc
.LFE6564:
	.size	_ZN2v88internal6torquelsERSoRKNS1_13QualifiedNameE, .-_ZN2v88internal6torquelsERSoRKNS1_13QualifiedNameE
	.section	.rodata._ZN2v88internal6torquelsERSoRKNS1_8CallableE.str1.1,"aMS",@progbits,1
.LC1:
	.string	"callable "
.LC2:
	.string	"("
.LC3:
	.string	"implicit "
	.section	.rodata._ZN2v88internal6torquelsERSoRKNS1_8CallableE.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"cannot create std::vector larger than max_size()"
	.section	.rodata._ZN2v88internal6torquelsERSoRKNS1_8CallableE.str1.1
.LC5:
	.string	")("
.LC6:
	.string	"): "
	.section	.text.unlikely._ZN2v88internal6torquelsERSoRKNS1_8CallableE,"ax",@progbits
.LCOLDB7:
	.section	.text._ZN2v88internal6torquelsERSoRKNS1_8CallableE,"ax",@progbits
.LHOTB7:
	.p2align 4
	.globl	_ZN2v88internal6torquelsERSoRKNS1_8CallableE
	.type	_ZN2v88internal6torquelsERSoRKNS1_8CallableE, @function
_ZN2v88internal6torquelsERSoRKNS1_8CallableE:
.LFB6565:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6565
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$9, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	leaq	.LC1(%rip), %rsi
	subq	$136, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
.LEHB0:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	168(%rbx), %rdx
	movq	160(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	leaq	.LC2(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpq	$0, 288(%rbx)
	je	.L9
	movl	$9, %edx
	leaq	.LC3(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE0:
	movq	288(%rbx), %rax
	pxor	%xmm0, %xmm0
	movabsq	$1152921504606846975, %rdx
	movq	256(%rbx), %r14
	movaps	%xmm0, -160(%rbp)
	leaq	0(,%rax,8), %r13
	movq	$0, -144(%rbp)
	movq	%r13, %rax
	sarq	$3, %rax
	cmpq	%rdx, %rax
	ja	.L65
	testq	%rax, %rax
	je	.L33
	movq	%r13, %rdi
.LEHB1:
	call	_Znwm@PLT
.LEHE1:
	movq	%rax, %rdi
.L11:
	leaq	(%rdi,%r13), %r15
	movq	%rdi, -160(%rbp)
	movq	%r15, -144(%rbp)
	testq	%r13, %r13
	je	.L12
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
.L12:
	leaq	-160(%rbp), %rsi
	movq	%r12, %rdi
	movq	%r15, -152(%rbp)
.LEHB2:
	call	_ZN2v88internal6torquelsERSoRKSt6vectorIPKNS1_4TypeESaIS6_EE@PLT
	movq	%rax, %rdi
	movl	$2, %edx
	leaq	.LC5(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE2:
	movq	256(%rbx), %rax
	movq	264(%rbx), %r15
	pxor	%xmm0, %xmm0
	movq	$0, -112(%rbp)
	movq	288(%rbx), %rdx
	movaps	%xmm0, -128(%rbp)
	movq	%r15, %r13
	leaq	(%rax,%rdx,8), %r14
	subq	%r14, %r13
	movq	%r13, %rax
	sarq	$3, %rax
	testq	%r13, %r13
	js	.L66
	testq	%rax, %rax
	je	.L34
	movq	%r13, %rdi
.LEHB3:
	call	_Znwm@PLT
.LEHE3:
	movq	%rax, %rdi
.L17:
	leaq	(%rdi,%r13), %rcx
	movq	%rdi, -128(%rbp)
	movq	%rcx, -112(%rbp)
	cmpq	%r14, %r15
	je	.L18
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%rcx, -168(%rbp)
	call	memcpy@PLT
	movq	-168(%rbp), %rcx
.L18:
	leaq	-128(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rcx, -120(%rbp)
.LEHB4:
	call	_ZN2v88internal6torquelsERSoRKSt6vectorIPKNS1_4TypeESaIS6_EE@PLT
.LEHE4:
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L23
	call	_ZdlPv@PLT
.L23:
	movq	-160(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L25
	call	_ZdlPv@PLT
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L9:
	leaq	256(%rbx), %rsi
	movq	%r12, %rdi
.LEHB5:
	call	_ZN2v88internal6torquelsERSoRKNS1_14ParameterTypesE@PLT
.L25:
	movl	$3, %edx
	leaq	.LC6(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	296(%rbx), %rsi
	leaq	-96(%rbp), %rdi
	call	_ZNK2v88internal6torque4Type8ToStringB5cxx11Ev@PLT
.LEHE5:
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r12, %rdi
.LEHB6:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE6:
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L26
	call	_ZdlPv@PLT
.L26:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L67
	addq	$136, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L33:
	.cfi_restore_state
	xorl	%edi, %edi
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L34:
	xorl	%edi, %edi
	jmp	.L17
.L65:
	leaq	.LC4(%rip), %rdi
.LEHB7:
	call	_ZSt20__throw_length_errorPKc@PLT
.LEHE7:
.L67:
	call	__stack_chk_fail@PLT
.L66:
	leaq	.LC4(%rip), %rdi
.LEHB8:
	call	_ZSt20__throw_length_errorPKc@PLT
.LEHE8:
.L37:
	endbr64
	movq	%rax, %r12
	jmp	.L14
.L36:
	endbr64
	movq	%rax, %r12
	jmp	.L29
.L38:
	endbr64
	movq	%rax, %r12
	jmp	.L20
.L39:
	endbr64
	movq	%rax, %r12
	jmp	.L27
.L35:
	endbr64
	movq	%rax, %r12
	jmp	.L22
	.globl	__gxx_personality_v0
	.section	.gcc_except_table._ZN2v88internal6torquelsERSoRKNS1_8CallableE,"a",@progbits
.LLSDA6565:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6565-.LLSDACSB6565
.LLSDACSB6565:
	.uleb128 .LEHB0-.LFB6565
	.uleb128 .LEHE0-.LEHB0
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB1-.LFB6565
	.uleb128 .LEHE1-.LEHB1
	.uleb128 .L37-.LFB6565
	.uleb128 0
	.uleb128 .LEHB2-.LFB6565
	.uleb128 .LEHE2-.LEHB2
	.uleb128 .L35-.LFB6565
	.uleb128 0
	.uleb128 .LEHB3-.LFB6565
	.uleb128 .LEHE3-.LEHB3
	.uleb128 .L38-.LFB6565
	.uleb128 0
	.uleb128 .LEHB4-.LFB6565
	.uleb128 .LEHE4-.LEHB4
	.uleb128 .L36-.LFB6565
	.uleb128 0
	.uleb128 .LEHB5-.LFB6565
	.uleb128 .LEHE5-.LEHB5
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB6-.LFB6565
	.uleb128 .LEHE6-.LEHB6
	.uleb128 .L39-.LFB6565
	.uleb128 0
	.uleb128 .LEHB7-.LFB6565
	.uleb128 .LEHE7-.LEHB7
	.uleb128 .L37-.LFB6565
	.uleb128 0
	.uleb128 .LEHB8-.LFB6565
	.uleb128 .LEHE8-.LEHB8
	.uleb128 .L38-.LFB6565
	.uleb128 0
.LLSDACSE6565:
	.section	.text._ZN2v88internal6torquelsERSoRKNS1_8CallableE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torquelsERSoRKNS1_8CallableE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6565
	.type	_ZN2v88internal6torquelsERSoRKNS1_8CallableE.cold, @function
_ZN2v88internal6torquelsERSoRKNS1_8CallableE.cold:
.LFSB6565:
.L14:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-160(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L15
	call	_ZdlPv@PLT
.L15:
	movq	%r12, %rdi
.LEHB9:
	call	_Unwind_Resume@PLT
.L29:
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L22
	call	_ZdlPv@PLT
.L22:
	movq	-160(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L31
	call	_ZdlPv@PLT
.L31:
	movq	%r12, %rdi
	call	_Unwind_Resume@PLT
.L27:
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L28
	call	_ZdlPv@PLT
.L28:
	movq	%r12, %rdi
	call	_Unwind_Resume@PLT
.LEHE9:
.L20:
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L22
	call	_ZdlPv@PLT
	jmp	.L22
	.cfi_endproc
.LFE6565:
	.section	.gcc_except_table._ZN2v88internal6torquelsERSoRKNS1_8CallableE
.LLSDAC6565:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6565-.LLSDACSBC6565
.LLSDACSBC6565:
	.uleb128 .LEHB9-.LCOLDB7
	.uleb128 .LEHE9-.LEHB9
	.uleb128 0
	.uleb128 0
.LLSDACSEC6565:
	.section	.text.unlikely._ZN2v88internal6torquelsERSoRKNS1_8CallableE
	.section	.text._ZN2v88internal6torquelsERSoRKNS1_8CallableE
	.size	_ZN2v88internal6torquelsERSoRKNS1_8CallableE, .-_ZN2v88internal6torquelsERSoRKNS1_8CallableE
	.section	.text.unlikely._ZN2v88internal6torquelsERSoRKNS1_8CallableE
	.size	_ZN2v88internal6torquelsERSoRKNS1_8CallableE.cold, .-_ZN2v88internal6torquelsERSoRKNS1_8CallableE.cold
.LCOLDE7:
	.section	.text._ZN2v88internal6torquelsERSoRKNS1_8CallableE
.LHOTE7:
	.section	.rodata._ZN2v88internal6torquelsERSoRKNS1_7BuiltinE.str1.1,"aMS",@progbits,1
.LC8:
	.string	"builtin "
.LC9:
	.string	" "
	.section	.text.unlikely._ZN2v88internal6torquelsERSoRKNS1_7BuiltinE,"ax",@progbits
.LCOLDB10:
	.section	.text._ZN2v88internal6torquelsERSoRKNS1_7BuiltinE,"ax",@progbits
.LHOTB10:
	.p2align 4
	.globl	_ZN2v88internal6torquelsERSoRKNS1_7BuiltinE
	.type	_ZN2v88internal6torquelsERSoRKNS1_7BuiltinE, @function
_ZN2v88internal6torquelsERSoRKNS1_7BuiltinE:
.LFB6566:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6566
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$8, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	leaq	.LC8(%rip), %rsi
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
.LEHB10:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	296(%rbx), %rsi
	leaq	-64(%rbp), %rdi
	call	_ZNK2v88internal6torque4Type8ToStringB5cxx11Ev@PLT
.LEHE10:
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rsi
	movq	%r12, %rdi
.LEHB11:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE11:
	movq	-64(%rbp), %rdi
	leaq	-48(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L69
	call	_ZdlPv@PLT
.L69:
	movl	$1, %edx
	leaq	.LC9(%rip), %rsi
	movq	%r12, %rdi
.LEHB12:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	168(%rbx), %rdx
	movq	160(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	256(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal6torquelsERSoRKNS1_14ParameterTypesE@PLT
.LEHE12:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L76
	addq	$48, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L76:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
.L73:
	endbr64
	movq	%rax, %r12
	jmp	.L70
	.section	.gcc_except_table._ZN2v88internal6torquelsERSoRKNS1_7BuiltinE,"a",@progbits
.LLSDA6566:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6566-.LLSDACSB6566
.LLSDACSB6566:
	.uleb128 .LEHB10-.LFB6566
	.uleb128 .LEHE10-.LEHB10
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB11-.LFB6566
	.uleb128 .LEHE11-.LEHB11
	.uleb128 .L73-.LFB6566
	.uleb128 0
	.uleb128 .LEHB12-.LFB6566
	.uleb128 .LEHE12-.LEHB12
	.uleb128 0
	.uleb128 0
.LLSDACSE6566:
	.section	.text._ZN2v88internal6torquelsERSoRKNS1_7BuiltinE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torquelsERSoRKNS1_7BuiltinE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6566
	.type	_ZN2v88internal6torquelsERSoRKNS1_7BuiltinE.cold, @function
_ZN2v88internal6torquelsERSoRKNS1_7BuiltinE.cold:
.LFSB6566:
.L70:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	movq	-64(%rbp), %rdi
	leaq	-48(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L71
	call	_ZdlPv@PLT
.L71:
	movq	%r12, %rdi
.LEHB13:
	call	_Unwind_Resume@PLT
.LEHE13:
	.cfi_endproc
.LFE6566:
	.section	.gcc_except_table._ZN2v88internal6torquelsERSoRKNS1_7BuiltinE
.LLSDAC6566:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6566-.LLSDACSBC6566
.LLSDACSBC6566:
	.uleb128 .LEHB13-.LCOLDB10
	.uleb128 .LEHE13-.LEHB13
	.uleb128 0
	.uleb128 0
.LLSDACSEC6566:
	.section	.text.unlikely._ZN2v88internal6torquelsERSoRKNS1_7BuiltinE
	.section	.text._ZN2v88internal6torquelsERSoRKNS1_7BuiltinE
	.size	_ZN2v88internal6torquelsERSoRKNS1_7BuiltinE, .-_ZN2v88internal6torquelsERSoRKNS1_7BuiltinE
	.section	.text.unlikely._ZN2v88internal6torquelsERSoRKNS1_7BuiltinE
	.size	_ZN2v88internal6torquelsERSoRKNS1_7BuiltinE.cold, .-_ZN2v88internal6torquelsERSoRKNS1_7BuiltinE.cold
.LCOLDE10:
	.section	.text._ZN2v88internal6torquelsERSoRKNS1_7BuiltinE
.LHOTE10:
	.section	.rodata._ZN2v88internal6torquelsERSoRKNS1_15RuntimeFunctionE.str1.1,"aMS",@progbits,1
.LC11:
	.string	"runtime function "
	.section	.text.unlikely._ZN2v88internal6torquelsERSoRKNS1_15RuntimeFunctionE,"ax",@progbits
.LCOLDB12:
	.section	.text._ZN2v88internal6torquelsERSoRKNS1_15RuntimeFunctionE,"ax",@progbits
.LHOTB12:
	.p2align 4
	.globl	_ZN2v88internal6torquelsERSoRKNS1_15RuntimeFunctionE
	.type	_ZN2v88internal6torquelsERSoRKNS1_15RuntimeFunctionE, @function
_ZN2v88internal6torquelsERSoRKNS1_15RuntimeFunctionE:
.LFB6567:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6567
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$17, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	leaq	.LC11(%rip), %rsi
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
.LEHB14:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	296(%rbx), %rsi
	leaq	-64(%rbp), %rdi
	call	_ZNK2v88internal6torque4Type8ToStringB5cxx11Ev@PLT
.LEHE14:
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rsi
	movq	%r12, %rdi
.LEHB15:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE15:
	movq	-64(%rbp), %rdi
	leaq	-48(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L78
	call	_ZdlPv@PLT
.L78:
	movl	$1, %edx
	leaq	.LC9(%rip), %rsi
	movq	%r12, %rdi
.LEHB16:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	168(%rbx), %rdx
	movq	160(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	256(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal6torquelsERSoRKNS1_14ParameterTypesE@PLT
.LEHE16:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L85
	addq	$48, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L85:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
.L82:
	endbr64
	movq	%rax, %r12
	jmp	.L79
	.section	.gcc_except_table._ZN2v88internal6torquelsERSoRKNS1_15RuntimeFunctionE,"a",@progbits
.LLSDA6567:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6567-.LLSDACSB6567
.LLSDACSB6567:
	.uleb128 .LEHB14-.LFB6567
	.uleb128 .LEHE14-.LEHB14
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB15-.LFB6567
	.uleb128 .LEHE15-.LEHB15
	.uleb128 .L82-.LFB6567
	.uleb128 0
	.uleb128 .LEHB16-.LFB6567
	.uleb128 .LEHE16-.LEHB16
	.uleb128 0
	.uleb128 0
.LLSDACSE6567:
	.section	.text._ZN2v88internal6torquelsERSoRKNS1_15RuntimeFunctionE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torquelsERSoRKNS1_15RuntimeFunctionE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6567
	.type	_ZN2v88internal6torquelsERSoRKNS1_15RuntimeFunctionE.cold, @function
_ZN2v88internal6torquelsERSoRKNS1_15RuntimeFunctionE.cold:
.LFSB6567:
.L79:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	movq	-64(%rbp), %rdi
	leaq	-48(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L80
	call	_ZdlPv@PLT
.L80:
	movq	%r12, %rdi
.LEHB17:
	call	_Unwind_Resume@PLT
.LEHE17:
	.cfi_endproc
.LFE6567:
	.section	.gcc_except_table._ZN2v88internal6torquelsERSoRKNS1_15RuntimeFunctionE
.LLSDAC6567:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6567-.LLSDACSBC6567
.LLSDACSBC6567:
	.uleb128 .LEHB17-.LCOLDB12
	.uleb128 .LEHE17-.LEHB17
	.uleb128 0
	.uleb128 0
.LLSDACSEC6567:
	.section	.text.unlikely._ZN2v88internal6torquelsERSoRKNS1_15RuntimeFunctionE
	.section	.text._ZN2v88internal6torquelsERSoRKNS1_15RuntimeFunctionE
	.size	_ZN2v88internal6torquelsERSoRKNS1_15RuntimeFunctionE, .-_ZN2v88internal6torquelsERSoRKNS1_15RuntimeFunctionE
	.section	.text.unlikely._ZN2v88internal6torquelsERSoRKNS1_15RuntimeFunctionE
	.size	_ZN2v88internal6torquelsERSoRKNS1_15RuntimeFunctionE.cold, .-_ZN2v88internal6torquelsERSoRKNS1_15RuntimeFunctionE.cold
.LCOLDE12:
	.section	.text._ZN2v88internal6torquelsERSoRKNS1_15RuntimeFunctionE
.LHOTE12:
	.section	.rodata._ZN2v88internal6torquelsERSoRKNS1_7GenericE.str1.1,"aMS",@progbits,1
.LC13:
	.string	"generic "
.LC14:
	.string	"<"
.LC15:
	.string	", "
	.section	.rodata._ZN2v88internal6torquelsERSoRKNS1_7GenericE.str1.8,"aMS",@progbits,1
	.align 8
.LC16:
	.string	"basic_string::_M_construct null not valid"
	.section	.rodata._ZN2v88internal6torquelsERSoRKNS1_7GenericE.str1.1
.LC17:
	.string	">"
	.section	.text.unlikely._ZN2v88internal6torquelsERSoRKNS1_7GenericE,"ax",@progbits
.LCOLDB18:
	.section	.text._ZN2v88internal6torquelsERSoRKNS1_7GenericE,"ax",@progbits
.LHOTB18:
	.p2align 4
	.globl	_ZN2v88internal6torquelsERSoRKNS1_7GenericE
	.type	_ZN2v88internal6torquelsERSoRKNS1_7GenericE, @function
_ZN2v88internal6torquelsERSoRKNS1_7GenericE:
.LFB6568:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6568
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$8, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	leaq	.LC13(%rip), %rsi
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
.LEHB18:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	80(%rbx), %rdx
	movq	72(%rbx), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	leaq	.LC14(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	104(%rbx), %rbx
	movq	40(%rbx), %rax
	movq	32(%rbx), %rsi
	movq	%rax, %rdi
	subq	%rsi, %rdi
	movq	%rdi, %rdx
	sarq	$3, %rdx
	je	.L107
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L122
	call	_Znwm@PLT
.LEHE18:
	movq	32(%rbx), %rsi
	movq	%rax, %r14
	movq	40(%rbx), %rax
	movq	%rax, %rdi
	subq	%rsi, %rdi
.L87:
	leaq	(%r14,%rdi), %rbx
	movq	%rbx, -128(%rbp)
	cmpq	%rsi, %rax
	je	.L89
	movq	%rdi, %rdx
	movq	%r14, %rdi
	call	memmove@PLT
	cmpq	%rbx, %r14
	je	.L104
.L90:
	leaq	-80(%rbp), %rax
	movq	%r14, %rbx
	movq	%rax, -120(%rbp)
	leaq	-104(%rbp), %rax
	movq	%rax, -136(%rbp)
.L92:
	movq	(%rbx), %rax
	movq	-120(%rbp), %rcx
	movq	32(%rax), %r15
	movq	40(%rax), %r12
	movq	%rcx, -96(%rbp)
	movq	%r15, %rax
	addq	%r12, %rax
	je	.L94
	testq	%r15, %r15
	je	.L123
.L94:
	movq	%r12, -104(%rbp)
	cmpq	$15, %r12
	ja	.L124
	cmpq	$1, %r12
	jne	.L97
	movzbl	(%r15), %eax
	movb	%al, -80(%rbp)
	movq	-120(%rbp), %rax
.L98:
	movq	%r12, -88(%rbp)
	movq	%r13, %rdi
	movb	$0, (%rax,%r12)
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
.LEHB19:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE19:
	movq	-96(%rbp), %rdi
	cmpq	-120(%rbp), %rdi
	je	.L99
	call	_ZdlPv@PLT
.L99:
	addq	$8, %rbx
	cmpq	-128(%rbp), %rbx
	je	.L93
	movl	$2, %edx
	leaq	.LC15(%rip), %rsi
	movq	%r13, %rdi
.LEHB20:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE20:
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L89:
	cmpq	-128(%rbp), %r14
	jne	.L90
	.p2align 4,,10
	.p2align 3
.L93:
	testq	%r14, %r14
	je	.L91
.L104:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L91:
	movl	$1, %edx
	leaq	.LC17(%rip), %rsi
	movq	%r13, %rdi
.LEHB21:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE21:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L125
	addq	$104, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L97:
	.cfi_restore_state
	testq	%r12, %r12
	jne	.L126
	movq	-120(%rbp), %rax
	jmp	.L98
	.p2align 4,,10
	.p2align 3
.L124:
	movq	-136(%rbp), %rsi
	leaq	-96(%rbp), %rdi
	xorl	%edx, %edx
.LEHB22:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -96(%rbp)
	movq	%rax, %rdi
	movq	-104(%rbp), %rax
	movq	%rax, -80(%rbp)
.L96:
	movq	%r12, %rdx
	movq	%r15, %rsi
	call	memcpy@PLT
	movq	-104(%rbp), %r12
	movq	-96(%rbp), %rax
	jmp	.L98
	.p2align 4,,10
	.p2align 3
.L107:
	xorl	%r14d, %r14d
	jmp	.L87
.L123:
	leaq	.LC16(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE22:
.L125:
	call	__stack_chk_fail@PLT
.L122:
.LEHB23:
	call	_ZSt17__throw_bad_allocv@PLT
.LEHE23:
.L126:
	movq	-120(%rbp), %rdi
	jmp	.L96
.L110:
	endbr64
	movq	%rax, %r12
	jmp	.L101
.L109:
	endbr64
	movq	%rax, %r12
	jmp	.L103
	.section	.gcc_except_table._ZN2v88internal6torquelsERSoRKNS1_7GenericE,"a",@progbits
.LLSDA6568:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6568-.LLSDACSB6568
.LLSDACSB6568:
	.uleb128 .LEHB18-.LFB6568
	.uleb128 .LEHE18-.LEHB18
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB19-.LFB6568
	.uleb128 .LEHE19-.LEHB19
	.uleb128 .L110-.LFB6568
	.uleb128 0
	.uleb128 .LEHB20-.LFB6568
	.uleb128 .LEHE20-.LEHB20
	.uleb128 .L109-.LFB6568
	.uleb128 0
	.uleb128 .LEHB21-.LFB6568
	.uleb128 .LEHE21-.LEHB21
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB22-.LFB6568
	.uleb128 .LEHE22-.LEHB22
	.uleb128 .L109-.LFB6568
	.uleb128 0
	.uleb128 .LEHB23-.LFB6568
	.uleb128 .LEHE23-.LEHB23
	.uleb128 0
	.uleb128 0
.LLSDACSE6568:
	.section	.text._ZN2v88internal6torquelsERSoRKNS1_7GenericE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torquelsERSoRKNS1_7GenericE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6568
	.type	_ZN2v88internal6torquelsERSoRKNS1_7GenericE.cold, @function
_ZN2v88internal6torquelsERSoRKNS1_7GenericE.cold:
.LFSB6568:
.L101:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-96(%rbp), %rdi
	cmpq	-120(%rbp), %rdi
	je	.L103
	call	_ZdlPv@PLT
.L103:
	testq	%r14, %r14
	je	.L105
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L105:
	movq	%r12, %rdi
.LEHB24:
	call	_Unwind_Resume@PLT
.LEHE24:
	.cfi_endproc
.LFE6568:
	.section	.gcc_except_table._ZN2v88internal6torquelsERSoRKNS1_7GenericE
.LLSDAC6568:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6568-.LLSDACSBC6568
.LLSDACSBC6568:
	.uleb128 .LEHB24-.LCOLDB18
	.uleb128 .LEHE24-.LEHB24
	.uleb128 0
	.uleb128 0
.LLSDACSEC6568:
	.section	.text.unlikely._ZN2v88internal6torquelsERSoRKNS1_7GenericE
	.section	.text._ZN2v88internal6torquelsERSoRKNS1_7GenericE
	.size	_ZN2v88internal6torquelsERSoRKNS1_7GenericE, .-_ZN2v88internal6torquelsERSoRKNS1_7GenericE
	.section	.text.unlikely._ZN2v88internal6torquelsERSoRKNS1_7GenericE
	.size	_ZN2v88internal6torquelsERSoRKNS1_7GenericE.cold, .-_ZN2v88internal6torquelsERSoRKNS1_7GenericE.cold
.LCOLDE18:
	.section	.text._ZN2v88internal6torquelsERSoRKNS1_7GenericE
.LHOTE18:
	.section	.text.unlikely._ZN2v88internal6torque7Generic24InferSpecializationTypesERKSt6vectorIPKNS1_4TypeESaIS6_EESA_,"ax",@progbits
	.align 2
.LCOLDB19:
	.section	.text._ZN2v88internal6torque7Generic24InferSpecializationTypesERKSt6vectorIPKNS1_4TypeESaIS6_EESA_,"ax",@progbits
.LHOTB19:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque7Generic24InferSpecializationTypesERKSt6vectorIPKNS1_4TypeESaIS6_EESA_
	.type	_ZN2v88internal6torque7Generic24InferSpecializationTypesERKSt6vectorIPKNS1_4TypeESaIS6_EESA_, @function
_ZN2v88internal6torque7Generic24InferSpecializationTypesERKSt6vectorIPKNS1_4TypeESaIS6_EESA_:
.LFB6572:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6572
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%rdx, -144(%rbp)
	movq	104(%rsi), %rbx
	movq	%rcx, -152(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	56(%rbx), %rax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -112(%rbp)
	movq	80(%rax), %rsi
	movq	120(%rax), %rdx
	movq	72(%rax), %rax
	movq	%rsi, -136(%rbp)
	leaq	(%rax,%rdx,8), %r15
	subq	%r15, %rsi
	movq	%rsi, %rax
	sarq	$3, %rax
	testq	%rsi, %rsi
	js	.L164
	movq	%rdi, %r14
	movq	%rsi, %r13
	testq	%rax, %rax
	je	.L144
	movq	%rsi, %rdi
.LEHB25:
	call	_Znwm@PLT
.LEHE25:
	movq	104(%r12), %rbx
	movq	%rax, %rdi
.L129:
	leaq	(%rdi,%r13), %rcx
	movq	%rdi, -112(%rbp)
	movq	%rcx, -96(%rbp)
	cmpq	-136(%rbp), %r15
	je	.L130
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%rcx, -136(%rbp)
	call	memcpy@PLT
	movq	-136(%rbp), %rcx
.L130:
	movq	%rcx, -104(%rbp)
	movq	16(%r12), %xmm0
	leaq	-128(%rbp), %rax
	movhps	%fs:_ZZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEvE3top@tpoff, %xmm0
	movaps	%xmm0, -128(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rax, %fs:_ZZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEvE3top@tpoff
	movq	40(%rbx), %rax
	movq	32(%rbx), %rsi
	movq	$0, -64(%rbp)
	movq	%rax, %r12
	movaps	%xmm0, -80(%rbp)
	subq	%rsi, %r12
	movq	%r12, %rdx
	sarq	$3, %rdx
	je	.L165
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L166
	movq	%r12, %rdi
.LEHB26:
	call	_Znwm@PLT
.LEHE26:
	movq	%rax, %rcx
	movq	40(%rbx), %rax
	movq	32(%rbx), %rsi
	movq	%rax, %rbx
	subq	%rsi, %rbx
.L132:
	movq	%rcx, %xmm0
	addq	%rcx, %r12
	punpcklqdq	%xmm0, %xmm0
	movq	%r12, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	cmpq	%rsi, %rax
	je	.L136
	movq	%rcx, %rdi
	movq	%rbx, %rdx
	call	memmove@PLT
	movq	%rax, %rcx
.L136:
	addq	%rbx, %rcx
	movq	-152(%rbp), %r8
	leaq	-80(%rbp), %rsi
	movq	%r14, %rdi
	movq	-144(%rbp), %rdx
	movq	%rcx, -72(%rbp)
	leaq	-112(%rbp), %rcx
.LEHB27:
	call	_ZN2v88internal6torque21TypeArgumentInferenceC1ERKSt6vectorIPNS1_10IdentifierESaIS5_EERKS3_IPKNS1_4TypeESaISC_EERKS3_IPNS1_14TypeExpressionESaISI_EESG_@PLT
.LEHE27:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L137
	call	_ZdlPv@PLT
.L137:
	movq	-120(%rbp), %rax
	movq	%rax, %fs:_ZZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEvE3top@tpoff
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L127
	call	_ZdlPv@PLT
.L127:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L167
	addq	$120, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L144:
	.cfi_restore_state
	xorl	%edi, %edi
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L165:
	movq	%r12, %rbx
	xorl	%ecx, %ecx
	jmp	.L132
.L167:
	call	__stack_chk_fail@PLT
.L164:
	leaq	.LC4(%rip), %rdi
.LEHB28:
	call	_ZSt20__throw_length_errorPKc@PLT
.LEHE28:
.L166:
.LEHB29:
	call	_ZSt17__throw_bad_allocv@PLT
.LEHE29:
.L145:
	endbr64
	movq	%rax, %r12
	jmp	.L141
.L146:
	endbr64
	movq	%rax, %r12
	jmp	.L139
.L147:
	endbr64
	movq	%rax, %r12
	jmp	.L133
	.section	.gcc_except_table._ZN2v88internal6torque7Generic24InferSpecializationTypesERKSt6vectorIPKNS1_4TypeESaIS6_EESA_,"a",@progbits
.LLSDA6572:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6572-.LLSDACSB6572
.LLSDACSB6572:
	.uleb128 .LEHB25-.LFB6572
	.uleb128 .LEHE25-.LEHB25
	.uleb128 .L147-.LFB6572
	.uleb128 0
	.uleb128 .LEHB26-.LFB6572
	.uleb128 .LEHE26-.LEHB26
	.uleb128 .L145-.LFB6572
	.uleb128 0
	.uleb128 .LEHB27-.LFB6572
	.uleb128 .LEHE27-.LEHB27
	.uleb128 .L146-.LFB6572
	.uleb128 0
	.uleb128 .LEHB28-.LFB6572
	.uleb128 .LEHE28-.LEHB28
	.uleb128 .L147-.LFB6572
	.uleb128 0
	.uleb128 .LEHB29-.LFB6572
	.uleb128 .LEHE29-.LEHB29
	.uleb128 .L145-.LFB6572
	.uleb128 0
.LLSDACSE6572:
	.section	.text._ZN2v88internal6torque7Generic24InferSpecializationTypesERKSt6vectorIPKNS1_4TypeESaIS6_EESA_
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque7Generic24InferSpecializationTypesERKSt6vectorIPKNS1_4TypeESaIS6_EESA_
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6572
	.type	_ZN2v88internal6torque7Generic24InferSpecializationTypesERKSt6vectorIPKNS1_4TypeESaIS6_EESA_.cold, @function
_ZN2v88internal6torque7Generic24InferSpecializationTypesERKSt6vectorIPKNS1_4TypeESaIS6_EESA_.cold:
.LFSB6572:
.L139:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L141
	call	_ZdlPv@PLT
.L141:
	movq	-120(%rbp), %rax
	movq	%rax, %fs:_ZZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEvE3top@tpoff
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L142
	call	_ZdlPv@PLT
.L142:
	movq	%r12, %rdi
.LEHB30:
	call	_Unwind_Resume@PLT
.L133:
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L134
	call	_ZdlPv@PLT
.L134:
	movq	%r12, %rdi
	call	_Unwind_Resume@PLT
.LEHE30:
	.cfi_endproc
.LFE6572:
	.section	.gcc_except_table._ZN2v88internal6torque7Generic24InferSpecializationTypesERKSt6vectorIPKNS1_4TypeESaIS6_EESA_
.LLSDAC6572:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6572-.LLSDACSBC6572
.LLSDACSBC6572:
	.uleb128 .LEHB30-.LCOLDB19
	.uleb128 .LEHE30-.LEHB30
	.uleb128 0
	.uleb128 0
.LLSDACSEC6572:
	.section	.text.unlikely._ZN2v88internal6torque7Generic24InferSpecializationTypesERKSt6vectorIPKNS1_4TypeESaIS6_EESA_
	.section	.text._ZN2v88internal6torque7Generic24InferSpecializationTypesERKSt6vectorIPKNS1_4TypeESaIS6_EESA_
	.size	_ZN2v88internal6torque7Generic24InferSpecializationTypesERKSt6vectorIPKNS1_4TypeESaIS6_EESA_, .-_ZN2v88internal6torque7Generic24InferSpecializationTypesERKSt6vectorIPKNS1_4TypeESaIS6_EESA_
	.section	.text.unlikely._ZN2v88internal6torque7Generic24InferSpecializationTypesERKSt6vectorIPKNS1_4TypeESaIS6_EESA_
	.size	_ZN2v88internal6torque7Generic24InferSpecializationTypesERKSt6vectorIPKNS1_4TypeESaIS6_EESA_.cold, .-_ZN2v88internal6torque7Generic24InferSpecializationTypesERKSt6vectorIPKNS1_4TypeESaIS6_EESA_.cold
.LCOLDE19:
	.section	.text._ZN2v88internal6torque7Generic24InferSpecializationTypesERKSt6vectorIPKNS1_4TypeESaIS6_EESA_
.LHOTE19:
	.section	.text._ZN2v88internal6torque7Generic12CallableBodyEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque7Generic12CallableBodyEv
	.type	_ZN2v88internal6torque7Generic12CallableBodyEv, @function
_ZN2v88internal6torque7Generic12CallableBodyEv:
.LFB6594:
	.cfi_startproc
	endbr64
	movq	104(%rdi), %rax
	movq	56(%rax), %rdx
	testq	%rdx, %rdx
	je	.L169
	movl	8(%rdx), %eax
	cmpl	$46, %eax
	jne	.L170
	movq	248(%rdx), %rax
	movq	256(%rdx), %rdx
	ret
	.p2align 4,,10
	.p2align 3
.L170:
	cmpl	$47, %eax
	jne	.L169
	movq	208(%rdx), %rax
	movq	216(%rdx), %rdx
	ret
	.p2align 4,,10
	.p2align 3
.L169:
	xorl	%edx, %edx
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE6594:
	.size	_ZN2v88internal6torque7Generic12CallableBodyEv, .-_ZN2v88internal6torque7Generic12CallableBodyEv
	.section	.text._ZNK2v88internal6torque9Namespace18IsDefaultNamespaceEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal6torque9Namespace18IsDefaultNamespaceEv
	.type	_ZNK2v88internal6torque9Namespace18IsDefaultNamespaceEv, @function
_ZNK2v88internal6torque9Namespace18IsDefaultNamespaceEv:
.LFB6595:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_13GlobalContextEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rax
	cmpq	%rbx, 8(%rax)
	sete	%al
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE6595:
	.size	_ZNK2v88internal6torque9Namespace18IsDefaultNamespaceEv, .-_ZNK2v88internal6torque9Namespace18IsDefaultNamespaceEv
	.section	.rodata._ZNK2v88internal6torque9Namespace15IsTestNamespaceEv.str1.1,"aMS",@progbits,1
.LC20:
	.string	"test"
	.section	.text._ZNK2v88internal6torque9Namespace15IsTestNamespaceEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal6torque9Namespace15IsTestNamespaceEv
	.type	_ZNK2v88internal6torque9Namespace15IsTestNamespaceEv, @function
_ZNK2v88internal6torque9Namespace15IsTestNamespaceEv:
.LFB6596:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	subq	$-128, %rdi
	leaq	.LC20(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	testl	%eax, %eax
	sete	%al
	ret
	.cfi_endproc
.LFE6596:
	.size	_ZNK2v88internal6torque9Namespace15IsTestNamespaceEv, .-_ZNK2v88internal6torque9Namespace15IsTestNamespaceEv
	.section	.text._ZN2v88internal6torque11ReportErrorIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_,"axG",@progbits,_ZN2v88internal6torque11ReportErrorIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_,comdat
	.p2align 4
	.weak	_ZN2v88internal6torque11ReportErrorIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_
	.type	_ZN2v88internal6torque11ReportErrorIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_, @function
_ZN2v88internal6torque11ReportErrorIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_:
.LFB7746:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA7746
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-432(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%r13, %rdi
	subq	$496, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
.LEHB31:
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEC1Ev@PLT
.LEHE31:
	movq	8(%rbx), %rdx
	movq	(%rbx), %rsi
	leaq	-416(%rbp), %rdi
.LEHB32:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	-528(%rbp), %r14
	leaq	-408(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZNKSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEE3strEv@PLT
.LEHE32:
	leaq	-496(%rbp), %r12
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
.LEHB33:
	call	_ZN2v88internal6torque14MessageBuilderC1ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_13TorqueMessage4KindE@PLT
.LEHE33:
	movq	-528(%rbp), %rdi
	leaq	-512(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L180
	call	_ZdlPv@PLT
.L180:
	movq	%r13, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r12, %rdi
.LEHB34:
	call	_ZNK2v88internal6torque14MessageBuilder5ThrowEv@PLT
.LEHE34:
.L188:
	endbr64
	movq	%rax, %r12
	jmp	.L183
.L187:
	endbr64
	movq	%rax, %r13
	jmp	.L184
.L189:
	endbr64
	movq	%rax, %r12
.L181:
	movq	-528(%rbp), %rdi
	leaq	-512(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L183
	call	_ZdlPv@PLT
.L183:
	movq	%r13, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r12, %rdi
.LEHB35:
	call	_Unwind_Resume@PLT
.LEHE35:
.L184:
	movq	%r12, %rdi
	call	_ZNK2v88internal6torque14MessageBuilder6ReportEv@PLT
	movq	-496(%rbp), %rdi
	leaq	-480(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L185
	call	_ZdlPv@PLT
.L185:
	movq	%r13, %rdi
.LEHB36:
	call	_Unwind_Resume@PLT
.LEHE36:
	.cfi_endproc
.LFE7746:
	.section	.gcc_except_table._ZN2v88internal6torque11ReportErrorIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_,"aG",@progbits,_ZN2v88internal6torque11ReportErrorIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_,comdat
.LLSDA7746:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE7746-.LLSDACSB7746
.LLSDACSB7746:
	.uleb128 .LEHB31-.LFB7746
	.uleb128 .LEHE31-.LEHB31
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB32-.LFB7746
	.uleb128 .LEHE32-.LEHB32
	.uleb128 .L188-.LFB7746
	.uleb128 0
	.uleb128 .LEHB33-.LFB7746
	.uleb128 .LEHE33-.LEHB33
	.uleb128 .L189-.LFB7746
	.uleb128 0
	.uleb128 .LEHB34-.LFB7746
	.uleb128 .LEHE34-.LEHB34
	.uleb128 .L187-.LFB7746
	.uleb128 0
	.uleb128 .LEHB35-.LFB7746
	.uleb128 .LEHE35-.LEHB35
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB36-.LFB7746
	.uleb128 .LEHE36-.LEHB36
	.uleb128 0
	.uleb128 0
.LLSDACSE7746:
	.section	.text._ZN2v88internal6torque11ReportErrorIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_,"axG",@progbits,_ZN2v88internal6torque11ReportErrorIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_,comdat
	.size	_ZN2v88internal6torque11ReportErrorIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_, .-_ZN2v88internal6torque11ReportErrorIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_
	.section	.rodata._ZNK2v88internal6torque9TypeAlias7ResolveEv.str1.1,"aMS",@progbits,1
.LC21:
	.string	"Cannot create type "
	.section	.rodata._ZNK2v88internal6torque9TypeAlias7ResolveEv.str1.8,"aMS",@progbits,1
	.align 8
.LC22:
	.string	" due to circular dependencies."
	.section	.text.unlikely._ZNK2v88internal6torque9TypeAlias7ResolveEv,"ax",@progbits
	.align 2
.LCOLDB23:
	.section	.text._ZNK2v88internal6torque9TypeAlias7ResolveEv,"ax",@progbits
.LHOTB23:
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal6torque9TypeAlias7ResolveEv
	.type	_ZNK2v88internal6torque9TypeAlias7ResolveEv, @function
_ZNK2v88internal6torque9TypeAlias7ResolveEv:
.LFB6597:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6597
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$480, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 88(%rdi)
	je	.L208
.L192:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	movq	96(%rbx), %rax
	jne	.L209
	addq	$480, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L208:
	.cfi_restore_state
	movq	16(%rdi), %xmm0
	leaq	-512(%rbp), %rax
	movhps	%fs:_ZZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEvE3top@tpoff, %xmm0
	movaps	%xmm0, -512(%rbp)
	movq	%rax, %fs:_ZZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEvE3top@tpoff
	movl	40(%rdi), %eax
	movdqu	24(%rdi), %xmm1
	movl	%eax, -480(%rbp)
	movaps	%xmm1, -496(%rbp)
.LEHB37:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rax
	movq	%rax, -472(%rbp)
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
.LEHE37:
	leaq	-496(%rbp), %rdx
	movq	%rdx, (%rax)
	cmpb	$0, 65(%rbx)
	movq	80(%rbx), %r12
	jne	.L210
	movq	%r12, %rdi
.LEHB38:
	call	_ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_15TypeDeclarationE@PLT
.LEHE38:
	cmpb	$0, 88(%rbx)
	movq	%rax, 96(%rbx)
	je	.L194
.L195:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
	movq	-472(%rbp), %rdx
	movq	%rdx, (%rax)
	movq	-504(%rbp), %rax
	movq	%rax, %fs:_ZZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEvE3top@tpoff
	jmp	.L192
	.p2align 4,,10
	.p2align 3
.L194:
	movb	$1, 88(%rbx)
	jmp	.L195
.L209:
	call	__stack_chk_fail@PLT
.L210:
	leaq	-432(%rbp), %r13
	movq	%r13, %rdi
.LEHB39:
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEC1Ev@PLT
.LEHE39:
	leaq	-416(%rbp), %r14
	movl	$19, %edx
	leaq	.LC21(%rip), %rsi
	movq	%r14, %rdi
.LEHB40:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	32(%r12), %rax
	movq	%r14, %rdi
	movq	40(%rax), %rdx
	movq	32(%rax), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rax, %rdi
	leaq	.LC22(%rip), %rsi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	leaq	-464(%rbp), %r12
	leaq	-408(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNKSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEE3strEv@PLT
.LEHE40:
	movq	%r12, %rdi
.LEHB41:
	call	_ZN2v88internal6torque11ReportErrorIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_
.LEHE41:
.L205:
	endbr64
	movq	%rax, %rbx
	jmp	.L199
.L202:
	endbr64
	movq	%rax, %rdi
	jmp	.L200
.L204:
	endbr64
	movq	%rax, %rbx
	jmp	.L196
.L203:
	endbr64
	movq	%rax, %rbx
	jmp	.L198
	.section	.gcc_except_table._ZNK2v88internal6torque9TypeAlias7ResolveEv,"a",@progbits
.LLSDA6597:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6597-.LLSDACSB6597
.LLSDACSB6597:
	.uleb128 .LEHB37-.LFB6597
	.uleb128 .LEHE37-.LEHB37
	.uleb128 .L202-.LFB6597
	.uleb128 0
	.uleb128 .LEHB38-.LFB6597
	.uleb128 .LEHE38-.LEHB38
	.uleb128 .L205-.LFB6597
	.uleb128 0
	.uleb128 .LEHB39-.LFB6597
	.uleb128 .LEHE39-.LEHB39
	.uleb128 .L205-.LFB6597
	.uleb128 0
	.uleb128 .LEHB40-.LFB6597
	.uleb128 .LEHE40-.LEHB40
	.uleb128 .L203-.LFB6597
	.uleb128 0
	.uleb128 .LEHB41-.LFB6597
	.uleb128 .LEHE41-.LEHB41
	.uleb128 .L204-.LFB6597
	.uleb128 0
.LLSDACSE6597:
	.section	.text._ZNK2v88internal6torque9TypeAlias7ResolveEv
	.cfi_endproc
	.section	.text.unlikely._ZNK2v88internal6torque9TypeAlias7ResolveEv
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6597
	.type	_ZNK2v88internal6torque9TypeAlias7ResolveEv.cold, @function
_ZNK2v88internal6torque9TypeAlias7ResolveEv.cold:
.LFSB6597:
.L196:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	movq	-464(%rbp), %rdi
	leaq	-448(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L198
	call	_ZdlPv@PLT
.L198:
	movq	%r13, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
.L199:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
	movq	-472(%rbp), %rdx
	movq	%rbx, %rdi
	movq	%rdx, (%rax)
.L200:
	movq	-504(%rbp), %rax
	movq	%rax, %fs:_ZZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEvE3top@tpoff
.LEHB42:
	call	_Unwind_Resume@PLT
.LEHE42:
	.cfi_endproc
.LFE6597:
	.section	.gcc_except_table._ZNK2v88internal6torque9TypeAlias7ResolveEv
.LLSDAC6597:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6597-.LLSDACSBC6597
.LLSDACSBC6597:
	.uleb128 .LEHB42-.LCOLDB23
	.uleb128 .LEHE42-.LEHB42
	.uleb128 0
	.uleb128 0
.LLSDACSEC6597:
	.section	.text.unlikely._ZNK2v88internal6torque9TypeAlias7ResolveEv
	.section	.text._ZNK2v88internal6torque9TypeAlias7ResolveEv
	.size	_ZNK2v88internal6torque9TypeAlias7ResolveEv, .-_ZNK2v88internal6torque9TypeAlias7ResolveEv
	.section	.text.unlikely._ZNK2v88internal6torque9TypeAlias7ResolveEv
	.size	_ZNK2v88internal6torque9TypeAlias7ResolveEv.cold, .-_ZNK2v88internal6torque9TypeAlias7ResolveEv.cold
.LCOLDE23:
	.section	.text._ZNK2v88internal6torque9TypeAlias7ResolveEv
.LHOTE23:
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv, @function
_GLOBAL__sub_I__ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv:
.LFB10454:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE10454:
	.size	_GLOBAL__sub_I__ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv, .-_GLOBAL__sub_I__ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv
	.section	.tbss._ZZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEvE3top,"awT",@nobits
	.align 8
	.type	_ZZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEvE3top, @object
	.size	_ZZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEvE3top, 8
_ZZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEvE3top:
	.zero	8
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	DW.ref.__gxx_personality_v0
	.weak	DW.ref.__gxx_personality_v0
	.section	.data.rel.local.DW.ref.__gxx_personality_v0,"awG",@progbits,DW.ref.__gxx_personality_v0,comdat
	.align 8
	.type	DW.ref.__gxx_personality_v0, @object
	.size	DW.ref.__gxx_personality_v0, 8
DW.ref.__gxx_personality_v0:
	.quad	__gxx_personality_v0
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
