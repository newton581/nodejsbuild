	.file	"type-oracle.cc"
	.text
	.section	.text._ZN2v88internal6torque21ContextualVariableTopINS1_10TypeOracleEEERPNT_5ScopeEv,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal6torque21ContextualVariableTopINS1_10TypeOracleEEERPNT_5ScopeEv
	.type	_ZN2v88internal6torque21ContextualVariableTopINS1_10TypeOracleEEERPNT_5ScopeEv, @function
_ZN2v88internal6torque21ContextualVariableTopINS1_10TypeOracleEEERPNT_5ScopeEv:
.LFB6352:
	.cfi_startproc
	endbr64
	movq	%fs:0, %rax
	addq	$_ZZN2v88internal6torque21ContextualVariableTopINS1_10TypeOracleEEERPNT_5ScopeEvE3top@tpoff, %rax
	ret
	.cfi_endproc
.LFE6352:
	.size	_ZN2v88internal6torque21ContextualVariableTopINS1_10TypeOracleEEERPNT_5ScopeEv, .-_ZN2v88internal6torque21ContextualVariableTopINS1_10TypeOracleEEERPNT_5ScopeEv
	.section	.text._ZN2v88internal6torque10TypeOracle17GetAggregateTypesEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque10TypeOracle17GetAggregateTypesEv
	.type	_ZN2v88internal6torque10TypeOracle17GetAggregateTypesEv, @function
_ZN2v88internal6torque10TypeOracle17GetAggregateTypesEv:
.LFB6353:
	.cfi_startproc
	endbr64
	movq	%fs:_ZZN2v88internal6torque21ContextualVariableTopINS1_10TypeOracleEEERPNT_5ScopeEvE3top@tpoff, %rax
	addq	$160, %rax
	ret
	.cfi_endproc
.LFE6353:
	.size	_ZN2v88internal6torque10TypeOracle17GetAggregateTypesEv, .-_ZN2v88internal6torque10TypeOracle17GetAggregateTypesEv
	.section	.text._ZN2v88internal6torque10TypeOracle22FinalizeAggregateTypesEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque10TypeOracle22FinalizeAggregateTypesEv
	.type	_ZN2v88internal6torque10TypeOracle22FinalizeAggregateTypesEv, @function
_ZN2v88internal6torque10TypeOracle22FinalizeAggregateTypesEv:
.LFB6354:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%fs:_ZZN2v88internal6torque21ContextualVariableTopINS1_10TypeOracleEEERPNT_5ScopeEvE3top@tpoff, %rax
	movq	160(%rax), %rbx
	movq	168(%rax), %r12
	cmpq	%r12, %rbx
	je	.L4
	.p2align 4,,10
	.p2align 3
.L6:
	movq	(%rbx), %rdi
	addq	$8, %rbx
	movq	(%rdi), %rax
	call	*96(%rax)
	cmpq	%rbx, %r12
	jne	.L6
.L4:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE6354:
	.size	_ZN2v88internal6torque10TypeOracle22FinalizeAggregateTypesEv, .-_ZN2v88internal6torque10TypeOracle22FinalizeAggregateTypesEv
	.section	.text._ZN2v88internal6torque11ReportErrorIJRA22_KcmRA18_S3_mRA12_S3_EEEvDpOT_,"axG",@progbits,_ZN2v88internal6torque11ReportErrorIJRA22_KcmRA18_S3_mRA12_S3_EEEvDpOT_,comdat
	.p2align 4
	.weak	_ZN2v88internal6torque11ReportErrorIJRA22_KcmRA18_S3_mRA12_S3_EEEvDpOT_
	.type	_ZN2v88internal6torque11ReportErrorIJRA22_KcmRA18_S3_mRA12_S3_EEEvDpOT_, @function
_ZN2v88internal6torque11ReportErrorIJRA22_KcmRA18_S3_mRA12_S3_EEEvDpOT_:
.LFB7519:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA7519
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-448(%rbp), %r15
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-432(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$520, %rsp
	movq	%rdi, -560(%rbp)
	movq	%r15, %rdi
	movq	%rcx, -552(%rbp)
	movq	%fs:40, %rdx
	movq	%rdx, -56(%rbp)
	xorl	%edx, %edx
.LEHB0:
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEC1Ev@PLT
.LEHE0:
	movq	-560(%rbp), %r9
	movq	%r12, %rdi
	movq	%r9, %rsi
.LEHB1:
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	-552(%rbp), %rax
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	leaq	-544(%rbp), %r13
	leaq	-424(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNKSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEE3strEv@PLT
.LEHE1:
	leaq	-512(%rbp), %r12
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
.LEHB2:
	call	_ZN2v88internal6torque14MessageBuilderC1ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_13TorqueMessage4KindE@PLT
.LEHE2:
	movq	-544(%rbp), %rdi
	leaq	-528(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L10
	call	_ZdlPv@PLT
.L10:
	movq	%r15, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r12, %rdi
.LEHB3:
	call	_ZNK2v88internal6torque14MessageBuilder5ThrowEv@PLT
.LEHE3:
.L18:
	endbr64
	movq	%rax, %r12
	jmp	.L13
.L17:
	endbr64
	movq	%rax, %r13
	jmp	.L14
.L19:
	endbr64
	movq	%rax, %r12
.L11:
	movq	-544(%rbp), %rdi
	leaq	-528(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L13
	call	_ZdlPv@PLT
.L13:
	movq	%r15, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r12, %rdi
.LEHB4:
	call	_Unwind_Resume@PLT
.LEHE4:
.L14:
	movq	%r12, %rdi
	call	_ZNK2v88internal6torque14MessageBuilder6ReportEv@PLT
	movq	-512(%rbp), %rdi
	leaq	-496(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L15
	call	_ZdlPv@PLT
.L15:
	movq	%r13, %rdi
.LEHB5:
	call	_Unwind_Resume@PLT
.LEHE5:
	.cfi_endproc
.LFE7519:
	.globl	__gxx_personality_v0
	.section	.gcc_except_table._ZN2v88internal6torque11ReportErrorIJRA22_KcmRA18_S3_mRA12_S3_EEEvDpOT_,"aG",@progbits,_ZN2v88internal6torque11ReportErrorIJRA22_KcmRA18_S3_mRA12_S3_EEEvDpOT_,comdat
.LLSDA7519:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE7519-.LLSDACSB7519
.LLSDACSB7519:
	.uleb128 .LEHB0-.LFB7519
	.uleb128 .LEHE0-.LEHB0
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB1-.LFB7519
	.uleb128 .LEHE1-.LEHB1
	.uleb128 .L18-.LFB7519
	.uleb128 0
	.uleb128 .LEHB2-.LFB7519
	.uleb128 .LEHE2-.LEHB2
	.uleb128 .L19-.LFB7519
	.uleb128 0
	.uleb128 .LEHB3-.LFB7519
	.uleb128 .LEHE3-.LEHB3
	.uleb128 .L17-.LFB7519
	.uleb128 0
	.uleb128 .LEHB4-.LFB7519
	.uleb128 .LEHE4-.LEHB4
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB5-.LFB7519
	.uleb128 .LEHE5-.LEHB5
	.uleb128 0
	.uleb128 0
.LLSDACSE7519:
	.section	.text._ZN2v88internal6torque11ReportErrorIJRA22_KcmRA18_S3_mRA12_S3_EEEvDpOT_,"axG",@progbits,_ZN2v88internal6torque11ReportErrorIJRA22_KcmRA18_S3_mRA12_S3_EEEvDpOT_,comdat
	.size	_ZN2v88internal6torque11ReportErrorIJRA22_KcmRA18_S3_mRA12_S3_EEEvDpOT_, .-_ZN2v88internal6torque11ReportErrorIJRA22_KcmRA18_S3_mRA12_S3_EEEvDpOT_
	.section	.text._ZNSt10_HashtableISt6vectorIPKN2v88internal6torque4TypeESaIS6_EESt4pairIKS8_PKNS3_10StructTypeEESaISE_ENSt8__detail10_Select1stESt8equal_toIS8_ENS1_4base4hashIS8_EENSG_18_Mod_range_hashingENSG_20_Default_ranged_hashENSG_20_Prime_rehash_policyENSG_17_Hashtable_traitsILb1ELb0ELb1EEEE9_M_rehashEmRKm,"axG",@progbits,_ZNSt10_HashtableISt6vectorIPKN2v88internal6torque4TypeESaIS6_EESt4pairIKS8_PKNS3_10StructTypeEESaISE_ENSt8__detail10_Select1stESt8equal_toIS8_ENS1_4base4hashIS8_EENSG_18_Mod_range_hashingENSG_20_Default_ranged_hashENSG_20_Prime_rehash_policyENSG_17_Hashtable_traitsILb1ELb0ELb1EEEE9_M_rehashEmRKm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableISt6vectorIPKN2v88internal6torque4TypeESaIS6_EESt4pairIKS8_PKNS3_10StructTypeEESaISE_ENSt8__detail10_Select1stESt8equal_toIS8_ENS1_4base4hashIS8_EENSG_18_Mod_range_hashingENSG_20_Default_ranged_hashENSG_20_Prime_rehash_policyENSG_17_Hashtable_traitsILb1ELb0ELb1EEEE9_M_rehashEmRKm
	.type	_ZNSt10_HashtableISt6vectorIPKN2v88internal6torque4TypeESaIS6_EESt4pairIKS8_PKNS3_10StructTypeEESaISE_ENSt8__detail10_Select1stESt8equal_toIS8_ENS1_4base4hashIS8_EENSG_18_Mod_range_hashingENSG_20_Default_ranged_hashENSG_20_Prime_rehash_policyENSG_17_Hashtable_traitsILb1ELb0ELb1EEEE9_M_rehashEmRKm, @function
_ZNSt10_HashtableISt6vectorIPKN2v88internal6torque4TypeESaIS6_EESt4pairIKS8_PKNS3_10StructTypeEESaISE_ENSt8__detail10_Select1stESt8equal_toIS8_ENS1_4base4hashIS8_EENSG_18_Mod_range_hashingENSG_20_Default_ranged_hashENSG_20_Prime_rehash_policyENSG_17_Hashtable_traitsILb1ELb0ELb1EEEE9_M_rehashEmRKm:
.LFB10032:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA10032
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	cmpq	$1, %rsi
	je	.L43
	movabsq	$1152921504606846975, %rax
	movq	%rdx, %r13
	cmpq	%rax, %rsi
	ja	.L44
	leaq	0(,%rsi,8), %r14
	movq	%r14, %rdi
.LEHB6:
	call	_Znwm@PLT
	movq	%r14, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	memset@PLT
	leaq	48(%rbx), %r9
.L23:
	movq	16(%rbx), %rsi
	movq	$0, 16(%rbx)
	testq	%rsi, %rsi
	je	.L25
	xorl	%r8d, %r8d
	leaq	16(%rbx), %r10
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L27:
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	(%rdi), %rax
	movq	%rcx, (%rax)
.L28:
	testq	%rsi, %rsi
	je	.L25
.L26:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	40(%rcx), %rax
	divq	%r12
	leaq	0(%r13,%rdx,8), %rdi
	movq	(%rdi), %rax
	testq	%rax, %rax
	jne	.L27
	movq	16(%rbx), %rax
	movq	%rax, (%rcx)
	movq	%rcx, 16(%rbx)
	movq	%r10, (%rdi)
	cmpq	$0, (%rcx)
	je	.L32
	movq	%rcx, 0(%r13,%r8,8)
	movq	%rdx, %r8
	testq	%rsi, %rsi
	jne	.L26
	.p2align 4,,10
	.p2align 3
.L25:
	movq	(%rbx), %rdi
	cmpq	%r9, %rdi
	je	.L29
	call	_ZdlPv@PLT
.L29:
	movq	%r12, 8(%rbx)
	movq	%r13, (%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	.cfi_restore_state
	movq	%rdx, %r8
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L43:
	leaq	48(%rdi), %r13
	movq	$0, 48(%rdi)
	movq	%r13, %r9
	jmp	.L23
.L44:
	call	_ZSt17__throw_bad_allocv@PLT
.LEHE6:
.L33:
	endbr64
	movq	%rax, %rdi
.L30:
	call	__cxa_begin_catch@PLT
	movq	0(%r13), %rax
	movq	%rax, 40(%rbx)
.LEHB7:
	call	__cxa_rethrow@PLT
.LEHE7:
.L34:
	endbr64
	movq	%rax, %r12
.L31:
	call	__cxa_end_catch@PLT
	movq	%r12, %rdi
.LEHB8:
	call	_Unwind_Resume@PLT
.LEHE8:
	.cfi_endproc
.LFE10032:
	.section	.gcc_except_table._ZNSt10_HashtableISt6vectorIPKN2v88internal6torque4TypeESaIS6_EESt4pairIKS8_PKNS3_10StructTypeEESaISE_ENSt8__detail10_Select1stESt8equal_toIS8_ENS1_4base4hashIS8_EENSG_18_Mod_range_hashingENSG_20_Default_ranged_hashENSG_20_Prime_rehash_policyENSG_17_Hashtable_traitsILb1ELb0ELb1EEEE9_M_rehashEmRKm,"aG",@progbits,_ZNSt10_HashtableISt6vectorIPKN2v88internal6torque4TypeESaIS6_EESt4pairIKS8_PKNS3_10StructTypeEESaISE_ENSt8__detail10_Select1stESt8equal_toIS8_ENS1_4base4hashIS8_EENSG_18_Mod_range_hashingENSG_20_Default_ranged_hashENSG_20_Prime_rehash_policyENSG_17_Hashtable_traitsILb1ELb0ELb1EEEE9_M_rehashEmRKm,comdat
	.align 4
.LLSDA10032:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATT10032-.LLSDATTD10032
.LLSDATTD10032:
	.byte	0x1
	.uleb128 .LLSDACSE10032-.LLSDACSB10032
.LLSDACSB10032:
	.uleb128 .LEHB6-.LFB10032
	.uleb128 .LEHE6-.LEHB6
	.uleb128 .L33-.LFB10032
	.uleb128 0x1
	.uleb128 .LEHB7-.LFB10032
	.uleb128 .LEHE7-.LEHB7
	.uleb128 .L34-.LFB10032
	.uleb128 0
	.uleb128 .LEHB8-.LFB10032
	.uleb128 .LEHE8-.LEHB8
	.uleb128 0
	.uleb128 0
.LLSDACSE10032:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATT10032:
	.section	.text._ZNSt10_HashtableISt6vectorIPKN2v88internal6torque4TypeESaIS6_EESt4pairIKS8_PKNS3_10StructTypeEESaISE_ENSt8__detail10_Select1stESt8equal_toIS8_ENS1_4base4hashIS8_EENSG_18_Mod_range_hashingENSG_20_Default_ranged_hashENSG_20_Prime_rehash_policyENSG_17_Hashtable_traitsILb1ELb0ELb1EEEE9_M_rehashEmRKm,"axG",@progbits,_ZNSt10_HashtableISt6vectorIPKN2v88internal6torque4TypeESaIS6_EESt4pairIKS8_PKNS3_10StructTypeEESaISE_ENSt8__detail10_Select1stESt8equal_toIS8_ENS1_4base4hashIS8_EENSG_18_Mod_range_hashingENSG_20_Default_ranged_hashENSG_20_Prime_rehash_policyENSG_17_Hashtable_traitsILb1ELb0ELb1EEEE9_M_rehashEmRKm,comdat
	.size	_ZNSt10_HashtableISt6vectorIPKN2v88internal6torque4TypeESaIS6_EESt4pairIKS8_PKNS3_10StructTypeEESaISE_ENSt8__detail10_Select1stESt8equal_toIS8_ENS1_4base4hashIS8_EENSG_18_Mod_range_hashingENSG_20_Default_ranged_hashENSG_20_Prime_rehash_policyENSG_17_Hashtable_traitsILb1ELb0ELb1EEEE9_M_rehashEmRKm, .-_ZNSt10_HashtableISt6vectorIPKN2v88internal6torque4TypeESaIS6_EESt4pairIKS8_PKNS3_10StructTypeEESaISE_ENSt8__detail10_Select1stESt8equal_toIS8_ENS1_4base4hashIS8_EENSG_18_Mod_range_hashingENSG_20_Default_ranged_hashENSG_20_Prime_rehash_policyENSG_17_Hashtable_traitsILb1ELb0ELb1EEEE9_M_rehashEmRKm
	.section	.text._ZNSt10_HashtableISt6vectorIPKN2v88internal6torque4TypeESaIS6_EESt4pairIKS8_PKNS3_10StructTypeEESaISE_ENSt8__detail10_Select1stESt8equal_toIS8_ENS1_4base4hashIS8_EENSG_18_Mod_range_hashingENSG_20_Default_ranged_hashENSG_20_Prime_rehash_policyENSG_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSG_10_Hash_nodeISE_Lb1EEEm,"axG",@progbits,_ZNSt10_HashtableISt6vectorIPKN2v88internal6torque4TypeESaIS6_EESt4pairIKS8_PKNS3_10StructTypeEESaISE_ENSt8__detail10_Select1stESt8equal_toIS8_ENS1_4base4hashIS8_EENSG_18_Mod_range_hashingENSG_20_Default_ranged_hashENSG_20_Prime_rehash_policyENSG_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSG_10_Hash_nodeISE_Lb1EEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableISt6vectorIPKN2v88internal6torque4TypeESaIS6_EESt4pairIKS8_PKNS3_10StructTypeEESaISE_ENSt8__detail10_Select1stESt8equal_toIS8_ENS1_4base4hashIS8_EENSG_18_Mod_range_hashingENSG_20_Default_ranged_hashENSG_20_Prime_rehash_policyENSG_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSG_10_Hash_nodeISE_Lb1EEEm
	.type	_ZNSt10_HashtableISt6vectorIPKN2v88internal6torque4TypeESaIS6_EESt4pairIKS8_PKNS3_10StructTypeEESaISE_ENSt8__detail10_Select1stESt8equal_toIS8_ENS1_4base4hashIS8_EENSG_18_Mod_range_hashingENSG_20_Default_ranged_hashENSG_20_Prime_rehash_policyENSG_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSG_10_Hash_nodeISE_Lb1EEEm, @function
_ZNSt10_HashtableISt6vectorIPKN2v88internal6torque4TypeESaIS6_EESt4pairIKS8_PKNS3_10StructTypeEESaISE_ENSt8__detail10_Select1stESt8equal_toIS8_ENS1_4base4hashIS8_EENSG_18_Mod_range_hashingENSG_20_Default_ranged_hashENSG_20_Prime_rehash_policyENSG_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSG_10_Hash_nodeISE_Lb1EEEm:
.LFB9686:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA9686
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rcx, %r12
	movq	%r8, %rcx
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	addq	$32, %rdi
	subq	$16, %rsp
	movq	-8(%rdi), %rdx
	movq	-24(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	%rax, -48(%rbp)
.LEHB9:
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
.LEHE9:
	testb	%al, %al
	je	.L46
	movq	%rdx, %rsi
	movq	%rbx, %rdi
	leaq	-48(%rbp), %rdx
.LEHB10:
	call	_ZNSt10_HashtableISt6vectorIPKN2v88internal6torque4TypeESaIS6_EESt4pairIKS8_PKNS3_10StructTypeEESaISE_ENSt8__detail10_Select1stESt8equal_toIS8_ENS1_4base4hashIS8_EENSG_18_Mod_range_hashingENSG_20_Default_ranged_hashENSG_20_Prime_rehash_policyENSG_17_Hashtable_traitsILb1ELb0ELb1EEEE9_M_rehashEmRKm
.LEHE10:
	movq	%r14, %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	%rdx, %r13
.L46:
	movq	%r14, 40(%r12)
	movq	(%rbx), %rax
	leaq	0(,%r13,8), %rcx
	movq	(%rax,%r13,8), %rax
	testq	%rax, %rax
	je	.L47
	movq	(%rax), %rax
	movq	%rax, (%r12)
	movq	(%rbx), %rax
	movq	(%rax,%r13,8), %rax
	movq	%r12, (%rax)
.L48:
	addq	$1, 24(%rbx)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L64
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L47:
	.cfi_restore_state
	movq	16(%rbx), %rax
	movq	%rax, (%r12)
	movq	%r12, 16(%rbx)
	movq	(%r12), %rax
	testq	%rax, %rax
	je	.L49
	movq	40(%rax), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	(%rbx), %rax
	movq	%r12, (%rax,%rdx,8)
.L49:
	movq	(%rbx), %rax
	leaq	16(%rbx), %rdx
	movq	%rdx, (%rax,%rcx)
	jmp	.L48
.L64:
	call	__stack_chk_fail@PLT
.L54:
	endbr64
	movq	%rax, %rdi
.L50:
	call	__cxa_begin_catch@PLT
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L51
	call	_ZdlPv@PLT
.L51:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.LEHB11:
	call	__cxa_rethrow@PLT
.LEHE11:
.L55:
	endbr64
	movq	%rax, %r12
.L52:
	call	__cxa_end_catch@PLT
	movq	%r12, %rdi
.LEHB12:
	call	_Unwind_Resume@PLT
.LEHE12:
	.cfi_endproc
.LFE9686:
	.section	.gcc_except_table._ZNSt10_HashtableISt6vectorIPKN2v88internal6torque4TypeESaIS6_EESt4pairIKS8_PKNS3_10StructTypeEESaISE_ENSt8__detail10_Select1stESt8equal_toIS8_ENS1_4base4hashIS8_EENSG_18_Mod_range_hashingENSG_20_Default_ranged_hashENSG_20_Prime_rehash_policyENSG_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSG_10_Hash_nodeISE_Lb1EEEm,"aG",@progbits,_ZNSt10_HashtableISt6vectorIPKN2v88internal6torque4TypeESaIS6_EESt4pairIKS8_PKNS3_10StructTypeEESaISE_ENSt8__detail10_Select1stESt8equal_toIS8_ENS1_4base4hashIS8_EENSG_18_Mod_range_hashingENSG_20_Default_ranged_hashENSG_20_Prime_rehash_policyENSG_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSG_10_Hash_nodeISE_Lb1EEEm,comdat
	.align 4
.LLSDA9686:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATT9686-.LLSDATTD9686
.LLSDATTD9686:
	.byte	0x1
	.uleb128 .LLSDACSE9686-.LLSDACSB9686
.LLSDACSB9686:
	.uleb128 .LEHB9-.LFB9686
	.uleb128 .LEHE9-.LEHB9
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB10-.LFB9686
	.uleb128 .LEHE10-.LEHB10
	.uleb128 .L54-.LFB9686
	.uleb128 0x1
	.uleb128 .LEHB11-.LFB9686
	.uleb128 .LEHE11-.LEHB11
	.uleb128 .L55-.LFB9686
	.uleb128 0
	.uleb128 .LEHB12-.LFB9686
	.uleb128 .LEHE12-.LEHB12
	.uleb128 0
	.uleb128 0
.LLSDACSE9686:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATT9686:
	.section	.text._ZNSt10_HashtableISt6vectorIPKN2v88internal6torque4TypeESaIS6_EESt4pairIKS8_PKNS3_10StructTypeEESaISE_ENSt8__detail10_Select1stESt8equal_toIS8_ENS1_4base4hashIS8_EENSG_18_Mod_range_hashingENSG_20_Default_ranged_hashENSG_20_Prime_rehash_policyENSG_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSG_10_Hash_nodeISE_Lb1EEEm,"axG",@progbits,_ZNSt10_HashtableISt6vectorIPKN2v88internal6torque4TypeESaIS6_EESt4pairIKS8_PKNS3_10StructTypeEESaISE_ENSt8__detail10_Select1stESt8equal_toIS8_ENS1_4base4hashIS8_EENSG_18_Mod_range_hashingENSG_20_Default_ranged_hashENSG_20_Prime_rehash_policyENSG_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSG_10_Hash_nodeISE_Lb1EEEm,comdat
	.size	_ZNSt10_HashtableISt6vectorIPKN2v88internal6torque4TypeESaIS6_EESt4pairIKS8_PKNS3_10StructTypeEESaISE_ENSt8__detail10_Select1stESt8equal_toIS8_ENS1_4base4hashIS8_EENSG_18_Mod_range_hashingENSG_20_Default_ranged_hashENSG_20_Prime_rehash_policyENSG_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSG_10_Hash_nodeISE_Lb1EEEm, .-_ZNSt10_HashtableISt6vectorIPKN2v88internal6torque4TypeESaIS6_EESt4pairIKS8_PKNS3_10StructTypeEESaISE_ENSt8__detail10_Select1stESt8equal_toIS8_ENS1_4base4hashIS8_EENSG_18_Mod_range_hashingENSG_20_Default_ranged_hashENSG_20_Prime_rehash_policyENSG_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSG_10_Hash_nodeISE_Lb1EEEm
	.section	.text._ZNSt8__detail9_Map_baseISt6vectorIPKN2v88internal6torque4TypeESaIS7_EESt4pairIKS9_PKNS4_10StructTypeEESaISF_ENS_10_Select1stESt8equal_toIS9_ENS2_4base4hashIS9_EENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERSB_,"axG",@progbits,_ZNSt8__detail9_Map_baseISt6vectorIPKN2v88internal6torque4TypeESaIS7_EESt4pairIKS9_PKNS4_10StructTypeEESaISF_ENS_10_Select1stESt8equal_toIS9_ENS2_4base4hashIS9_EENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERSB_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8__detail9_Map_baseISt6vectorIPKN2v88internal6torque4TypeESaIS7_EESt4pairIKS9_PKNS4_10StructTypeEESaISF_ENS_10_Select1stESt8equal_toIS9_ENS2_4base4hashIS9_EENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERSB_
	.type	_ZNSt8__detail9_Map_baseISt6vectorIPKN2v88internal6torque4TypeESaIS7_EESt4pairIKS9_PKNS4_10StructTypeEESaISF_ENS_10_Select1stESt8equal_toIS9_ENS2_4base4hashIS9_EENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERSB_, @function
_ZNSt8__detail9_Map_baseISt6vectorIPKN2v88internal6torque4TypeESaIS7_EESt4pairIKS9_PKNS4_10StructTypeEESaISF_ENS_10_Select1stESt8equal_toIS9_ENS2_4base4hashIS9_EENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERSB_:
.LFB9191:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA9191
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	(%rsi), %r14
	movq	8(%rsi), %r15
	cmpq	%r15, %r14
	je	.L66
	.p2align 4,,10
	.p2align 3
.L67:
	movq	(%r14), %r8
	movq	%r12, %rdi
	addq	$8, %r14
	movq	%r8, -56(%rbp)
.LEHB13:
	call	_ZN2v84base10hash_valueEm@PLT
	movq	-56(%rbp), %r8
	movq	%rax, %r12
	movq	%r8, %rdi
	call	_ZN2v84base10hash_valueEm@PLT
	xorl	%edi, %edi
	movq	%rax, %rsi
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	%rax, %r12
	cmpq	%r14, %r15
	jne	.L67
.L66:
	movq	8(%r13), %r8
	movq	%r12, %rax
	xorl	%edx, %edx
	divq	%r8
	movq	0(%r13), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r15
	testq	%rax, %rax
	je	.L68
	movq	(%rax), %r14
	movq	40(%r14), %rcx
.L71:
	cmpq	%rcx, %r12
	je	.L97
.L69:
	movq	(%r14), %r14
	testq	%r14, %r14
	je	.L68
	movq	40(%r14), %rcx
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%r8
	cmpq	%rdx, %r15
	je	.L71
.L68:
	movl	$48, %edi
	call	_Znwm@PLT
.LEHE13:
	movq	8(%rbx), %rdx
	movq	(%rbx), %rsi
	pxor	%xmm0, %xmm0
	movq	%rax, %r14
	movq	%rdx, %rax
	movups	%xmm0, (%r14)
	subq	%rsi, %rax
	movups	%xmm0, 16(%r14)
	movq	%rax, -56(%rbp)
	sarq	$3, %rax
	je	.L98
	movabsq	$1152921504606846975, %rdx
	cmpq	%rdx, %rax
	ja	.L99
	movq	-56(%rbp), %rdi
.LEHB14:
	call	_Znwm@PLT
.LEHE14:
	movq	8(%rbx), %rdx
	movq	(%rbx), %rsi
	movq	%rax, %rcx
	movq	%rdx, %rbx
	subq	%rsi, %rbx
.L73:
	movq	-56(%rbp), %rax
	movq	%rcx, %xmm0
	punpcklqdq	%xmm0, %xmm0
	addq	%rcx, %rax
	movups	%xmm0, 8(%r14)
	movq	%rax, 24(%r14)
	cmpq	%rdx, %rsi
	je	.L75
	movq	%rcx, %rdi
	movq	%rbx, %rdx
	call	memmove@PLT
	movq	%rax, %rcx
.L75:
	addq	%rbx, %rcx
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rcx, 16(%r14)
	movl	$1, %r8d
	movq	%r14, %rcx
	movq	$0, 32(%r14)
.LEHB15:
	call	_ZNSt10_HashtableISt6vectorIPKN2v88internal6torque4TypeESaIS6_EESt4pairIKS8_PKNS3_10StructTypeEESaISE_ENSt8__detail10_Select1stESt8equal_toIS8_ENS1_4base4hashIS8_EENSG_18_Mod_range_hashingENSG_20_Default_ranged_hashENSG_20_Prime_rehash_policyENSG_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSG_10_Hash_nodeISE_Lb1EEEm
.LEHE15:
	addq	$24, %rsp
	popq	%rbx
	addq	$32, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L97:
	.cfi_restore_state
	movq	(%rbx), %rdi
	movq	8(%rbx), %rdx
	movq	8(%r14), %rsi
	movq	16(%r14), %rax
	subq	%rdi, %rdx
	subq	%rsi, %rax
	cmpq	%rax, %rdx
	jne	.L69
	testq	%rdx, %rdx
	jne	.L100
.L70:
	addq	$24, %rsp
	leaq	32(%r14), %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L100:
	.cfi_restore_state
	movq	%r8, -56(%rbp)
	call	memcmp@PLT
	movq	-56(%rbp), %r8
	testl	%eax, %eax
	je	.L70
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L98:
	movq	-56(%rbp), %rbx
	xorl	%ecx, %ecx
	jmp	.L73
.L99:
.LEHB16:
	call	_ZSt17__throw_bad_allocv@PLT
.LEHE16:
.L80:
	endbr64
	movq	%rax, %rdi
.L77:
	call	__cxa_begin_catch@PLT
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.LEHB17:
	call	__cxa_rethrow@PLT
.LEHE17:
.L81:
	endbr64
	movq	%rax, %r12
.L78:
	call	__cxa_end_catch@PLT
	movq	%r12, %rdi
.LEHB18:
	call	_Unwind_Resume@PLT
.LEHE18:
	.cfi_endproc
.LFE9191:
	.section	.gcc_except_table._ZNSt8__detail9_Map_baseISt6vectorIPKN2v88internal6torque4TypeESaIS7_EESt4pairIKS9_PKNS4_10StructTypeEESaISF_ENS_10_Select1stESt8equal_toIS9_ENS2_4base4hashIS9_EENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERSB_,"aG",@progbits,_ZNSt8__detail9_Map_baseISt6vectorIPKN2v88internal6torque4TypeESaIS7_EESt4pairIKS9_PKNS4_10StructTypeEESaISF_ENS_10_Select1stESt8equal_toIS9_ENS2_4base4hashIS9_EENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERSB_,comdat
	.align 4
.LLSDA9191:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATT9191-.LLSDATTD9191
.LLSDATTD9191:
	.byte	0x1
	.uleb128 .LLSDACSE9191-.LLSDACSB9191
.LLSDACSB9191:
	.uleb128 .LEHB13-.LFB9191
	.uleb128 .LEHE13-.LEHB13
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB14-.LFB9191
	.uleb128 .LEHE14-.LEHB14
	.uleb128 .L80-.LFB9191
	.uleb128 0x1
	.uleb128 .LEHB15-.LFB9191
	.uleb128 .LEHE15-.LEHB15
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB16-.LFB9191
	.uleb128 .LEHE16-.LEHB16
	.uleb128 .L80-.LFB9191
	.uleb128 0x1
	.uleb128 .LEHB17-.LFB9191
	.uleb128 .LEHE17-.LEHB17
	.uleb128 .L81-.LFB9191
	.uleb128 0
	.uleb128 .LEHB18-.LFB9191
	.uleb128 .LEHE18-.LEHB18
	.uleb128 0
	.uleb128 0
.LLSDACSE9191:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATT9191:
	.section	.text._ZNSt8__detail9_Map_baseISt6vectorIPKN2v88internal6torque4TypeESaIS7_EESt4pairIKS9_PKNS4_10StructTypeEESaISF_ENS_10_Select1stESt8equal_toIS9_ENS2_4base4hashIS9_EENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERSB_,"axG",@progbits,_ZNSt8__detail9_Map_baseISt6vectorIPKN2v88internal6torque4TypeESaIS7_EESt4pairIKS9_PKNS4_10StructTypeEESaISF_ENS_10_Select1stESt8equal_toIS9_ENS2_4base4hashIS9_EENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERSB_,comdat
	.size	_ZNSt8__detail9_Map_baseISt6vectorIPKN2v88internal6torque4TypeESaIS7_EESt4pairIKS9_PKNS4_10StructTypeEESaISF_ENS_10_Select1stESt8equal_toIS9_ENS2_4base4hashIS9_EENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERSB_, .-_ZNSt8__detail9_Map_baseISt6vectorIPKN2v88internal6torque4TypeESaIS7_EESt4pairIKS9_PKNS4_10StructTypeEESaISF_ENS_10_Select1stESt8equal_toIS9_ENS2_4base4hashIS9_EENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERSB_
	.section	.rodata._ZN2v88internal6torque10TypeOracle28GetGenericStructTypeInstanceEPNS1_17GenericStructTypeESt6vectorIPKNS1_4TypeESaIS8_EE.str1.1,"aMS",@progbits,1
.LC0:
	.string	" were given"
.LC1:
	.string	" parameters, but "
.LC2:
	.string	"Generic struct takes "
	.section	.text.unlikely._ZN2v88internal6torque10TypeOracle28GetGenericStructTypeInstanceEPNS1_17GenericStructTypeESt6vectorIPKNS1_4TypeESaIS8_EE,"ax",@progbits
	.align 2
.LCOLDB3:
	.section	.text._ZN2v88internal6torque10TypeOracle28GetGenericStructTypeInstanceEPNS1_17GenericStructTypeESt6vectorIPKNS1_4TypeESaIS8_EE,"ax",@progbits
.LHOTB3:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque10TypeOracle28GetGenericStructTypeInstanceEPNS1_17GenericStructTypeESt6vectorIPKNS1_4TypeESaIS8_EE
	.type	_ZN2v88internal6torque10TypeOracle28GetGenericStructTypeInstanceEPNS1_17GenericStructTypeESt6vectorIPKNS1_4TypeESaIS8_EE, @function
_ZN2v88internal6torque10TypeOracle28GetGenericStructTypeInstanceEPNS1_17GenericStructTypeESt6vectorIPKNS1_4TypeESaIS8_EE:
.LFB6355:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6355
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	104(%rdi), %rdx
	movq	8(%rsi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rbx
	movq	96(%rdx), %rax
	subq	88(%rdx), %rax
	movq	%r15, %rdx
	subq	%rbx, %rdx
	cmpq	%rdx, %rax
	jne	.L143
	movq	%rdi, %r13
	movq	%rsi, %r12
	xorl	%r14d, %r14d
	cmpq	%r15, %rbx
	je	.L103
	.p2align 4,,10
	.p2align 3
.L104:
	movq	(%rbx), %r8
	movq	%r14, %rdi
	addq	$8, %rbx
	movq	%r8, -120(%rbp)
.LEHB19:
	call	_ZN2v84base10hash_valueEm@PLT
	movq	-120(%rbp), %r8
	movq	%rax, %r14
	movq	%r8, %rdi
	call	_ZN2v84base10hash_valueEm@PLT
	xorl	%edi, %edi
	movq	%rax, %rsi
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	%rax, %r14
	cmpq	%rbx, %r15
	jne	.L104
.L103:
	movq	120(%r13), %r15
	movq	%r14, %rax
	xorl	%edx, %edx
	divq	%r15
	movq	112(%r13), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r8
	testq	%rax, %rax
	je	.L105
	movq	(%rax), %rbx
	movq	40(%rbx), %rcx
.L108:
	cmpq	%r14, %rcx
	je	.L144
.L106:
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L105
	movq	40(%rbx), %rcx
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%r15
	cmpq	%rdx, %r8
	je	.L108
.L105:
	movq	16(%r13), %rax
	movq	%rax, -112(%rbp)
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rax
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
.LEHE19:
	leaq	-112(%rbp), %rdx
	movq	%rdx, (%rax)
	movq	8(%r12), %rax
	movq	(%r12), %rsi
	movq	%rax, %rbx
	subq	%rsi, %rbx
	movq	%rbx, %rdx
	sarq	$3, %rdx
	je	.L145
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L146
	movq	%rbx, %rdi
.LEHB20:
	call	_Znwm@PLT
.LEHE20:
	movq	%rax, %rcx
	movq	8(%r12), %rax
	movq	(%r12), %rsi
	movq	%rax, %r14
	subq	%rsi, %r14
.L110:
	addq	%rcx, %rbx
	cmpq	%rsi, %rax
	je	.L112
	movq	%rcx, %rdi
	movq	%r14, %rdx
	call	memmove@PLT
	movq	%rax, %rcx
.L112:
	movq	%rcx, %xmm1
	movq	%r13, %xmm0
	movq	104(%r13), %rdi
	addq	%r14, %rcx
	punpcklqdq	%xmm1, %xmm0
	leaq	-96(%rbp), %rsi
	movb	$1, -96(%rbp)
	movq	%rcx, -72(%rbp)
	movq	%rbx, -64(%rbp)
	movups	%xmm0, -88(%rbp)
.LEHB21:
	call	_ZN2v88internal6torque11TypeVisitor11ComputeTypeEPNS1_17StructDeclarationENS_4base8OptionalINS1_17SpecializationKeyINS1_17GenericStructTypeEEEEE@PLT
.LEHE21:
	cmpb	$0, -96(%rbp)
	movq	%rax, %r14
	je	.L113
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L113
	call	_ZdlPv@PLT
.L113:
	leaq	112(%r13), %rdi
	movq	%r12, %rsi
.LEHB22:
	call	_ZNSt8__detail9_Map_baseISt6vectorIPKN2v88internal6torque4TypeESaIS7_EESt4pairIKS9_PKNS4_10StructTypeEESaISF_ENS_10_Select1stESt8equal_toIS9_ENS2_4base4hashIS9_EENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERSB_
.LEHE22:
	movq	%r14, (%rax)
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
	movq	-104(%rbp), %rdx
	movq	%rdx, (%rax)
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L144:
	movq	(%r12), %rdi
	movq	8(%r12), %rdx
	movq	8(%rbx), %rsi
	movq	16(%rbx), %rax
	subq	%rdi, %rdx
	subq	%rsi, %rax
	cmpq	%rax, %rdx
	jne	.L106
	testq	%rdx, %rdx
	jne	.L147
.L107:
	movq	32(%rbx), %r14
.L101:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L148
	addq	$88, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L147:
	.cfi_restore_state
	movq	%r8, -120(%rbp)
	call	memcmp@PLT
	movq	-120(%rbp), %r8
	testl	%eax, %eax
	je	.L107
	jmp	.L106
	.p2align 4,,10
	.p2align 3
.L145:
	movq	%rbx, %r14
	xorl	%ecx, %ecx
	jmp	.L110
.L146:
.LEHB23:
	call	_ZSt17__throw_bad_allocv@PLT
.LEHE23:
.L148:
	call	__stack_chk_fail@PLT
.L143:
	sarq	$3, %rdx
	sarq	$3, %rax
	leaq	-96(%rbp), %rcx
	leaq	-112(%rbp), %rsi
	movq	%rdx, -96(%rbp)
	leaq	.LC0(%rip), %r8
	leaq	.LC1(%rip), %rdx
	leaq	.LC2(%rip), %rdi
	movq	%rax, -112(%rbp)
.LEHB24:
	call	_ZN2v88internal6torque11ReportErrorIJRA22_KcmRA18_S3_mRA12_S3_EEEvDpOT_
.LEHE24:
.L121:
	endbr64
	movq	%rax, %r12
	jmp	.L114
.L120:
	endbr64
	movq	%rax, %r12
	jmp	.L116
	.section	.gcc_except_table._ZN2v88internal6torque10TypeOracle28GetGenericStructTypeInstanceEPNS1_17GenericStructTypeESt6vectorIPKNS1_4TypeESaIS8_EE,"a",@progbits
.LLSDA6355:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6355-.LLSDACSB6355
.LLSDACSB6355:
	.uleb128 .LEHB19-.LFB6355
	.uleb128 .LEHE19-.LEHB19
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB20-.LFB6355
	.uleb128 .LEHE20-.LEHB20
	.uleb128 .L120-.LFB6355
	.uleb128 0
	.uleb128 .LEHB21-.LFB6355
	.uleb128 .LEHE21-.LEHB21
	.uleb128 .L121-.LFB6355
	.uleb128 0
	.uleb128 .LEHB22-.LFB6355
	.uleb128 .LEHE22-.LEHB22
	.uleb128 .L120-.LFB6355
	.uleb128 0
	.uleb128 .LEHB23-.LFB6355
	.uleb128 .LEHE23-.LEHB23
	.uleb128 .L120-.LFB6355
	.uleb128 0
	.uleb128 .LEHB24-.LFB6355
	.uleb128 .LEHE24-.LEHB24
	.uleb128 0
	.uleb128 0
.LLSDACSE6355:
	.section	.text._ZN2v88internal6torque10TypeOracle28GetGenericStructTypeInstanceEPNS1_17GenericStructTypeESt6vectorIPKNS1_4TypeESaIS8_EE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque10TypeOracle28GetGenericStructTypeInstanceEPNS1_17GenericStructTypeESt6vectorIPKNS1_4TypeESaIS8_EE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6355
	.type	_ZN2v88internal6torque10TypeOracle28GetGenericStructTypeInstanceEPNS1_17GenericStructTypeESt6vectorIPKNS1_4TypeESaIS8_EE.cold, @function
_ZN2v88internal6torque10TypeOracle28GetGenericStructTypeInstanceEPNS1_17GenericStructTypeESt6vectorIPKNS1_4TypeESaIS8_EE.cold:
.LFSB6355:
.L114:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	cmpb	$0, -96(%rbp)
	je	.L116
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L116
	call	_ZdlPv@PLT
.L116:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
	movq	-104(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rdx, (%rax)
.LEHB25:
	call	_Unwind_Resume@PLT
.LEHE25:
	.cfi_endproc
.LFE6355:
	.section	.gcc_except_table._ZN2v88internal6torque10TypeOracle28GetGenericStructTypeInstanceEPNS1_17GenericStructTypeESt6vectorIPKNS1_4TypeESaIS8_EE
.LLSDAC6355:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6355-.LLSDACSBC6355
.LLSDACSBC6355:
	.uleb128 .LEHB25-.LCOLDB3
	.uleb128 .LEHE25-.LEHB25
	.uleb128 0
	.uleb128 0
.LLSDACSEC6355:
	.section	.text.unlikely._ZN2v88internal6torque10TypeOracle28GetGenericStructTypeInstanceEPNS1_17GenericStructTypeESt6vectorIPKNS1_4TypeESaIS8_EE
	.section	.text._ZN2v88internal6torque10TypeOracle28GetGenericStructTypeInstanceEPNS1_17GenericStructTypeESt6vectorIPKNS1_4TypeESaIS8_EE
	.size	_ZN2v88internal6torque10TypeOracle28GetGenericStructTypeInstanceEPNS1_17GenericStructTypeESt6vectorIPKNS1_4TypeESaIS8_EE, .-_ZN2v88internal6torque10TypeOracle28GetGenericStructTypeInstanceEPNS1_17GenericStructTypeESt6vectorIPKNS1_4TypeESaIS8_EE
	.section	.text.unlikely._ZN2v88internal6torque10TypeOracle28GetGenericStructTypeInstanceEPNS1_17GenericStructTypeESt6vectorIPKNS1_4TypeESaIS8_EE
	.size	_ZN2v88internal6torque10TypeOracle28GetGenericStructTypeInstanceEPNS1_17GenericStructTypeESt6vectorIPKNS1_4TypeESaIS8_EE.cold, .-_ZN2v88internal6torque10TypeOracle28GetGenericStructTypeInstanceEPNS1_17GenericStructTypeESt6vectorIPKNS1_4TypeESaIS8_EE.cold
.LCOLDE3:
	.section	.text._ZN2v88internal6torque10TypeOracle28GetGenericStructTypeInstanceEPNS1_17GenericStructTypeESt6vectorIPKNS1_4TypeESaIS8_EE
.LHOTE3:
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal6torque21ContextualVariableTopINS1_10TypeOracleEEERPNT_5ScopeEv,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal6torque21ContextualVariableTopINS1_10TypeOracleEEERPNT_5ScopeEv, @function
_GLOBAL__sub_I__ZN2v88internal6torque21ContextualVariableTopINS1_10TypeOracleEEERPNT_5ScopeEv:
.LFB11050:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE11050:
	.size	_GLOBAL__sub_I__ZN2v88internal6torque21ContextualVariableTopINS1_10TypeOracleEEERPNT_5ScopeEv, .-_GLOBAL__sub_I__ZN2v88internal6torque21ContextualVariableTopINS1_10TypeOracleEEERPNT_5ScopeEv
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal6torque21ContextualVariableTopINS1_10TypeOracleEEERPNT_5ScopeEv
	.section	.tbss._ZZN2v88internal6torque21ContextualVariableTopINS1_10TypeOracleEEERPNT_5ScopeEvE3top,"awT",@nobits
	.align 8
	.type	_ZZN2v88internal6torque21ContextualVariableTopINS1_10TypeOracleEEERPNT_5ScopeEvE3top, @object
	.size	_ZZN2v88internal6torque21ContextualVariableTopINS1_10TypeOracleEEERPNT_5ScopeEvE3top, 8
_ZZN2v88internal6torque21ContextualVariableTopINS1_10TypeOracleEEERPNT_5ScopeEvE3top:
	.zero	8
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	DW.ref.__gxx_personality_v0
	.weak	DW.ref.__gxx_personality_v0
	.section	.data.rel.local.DW.ref.__gxx_personality_v0,"awG",@progbits,DW.ref.__gxx_personality_v0,comdat
	.align 8
	.type	DW.ref.__gxx_personality_v0, @object
	.size	DW.ref.__gxx_personality_v0, 8
DW.ref.__gxx_personality_v0:
	.quad	__gxx_personality_v0
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
