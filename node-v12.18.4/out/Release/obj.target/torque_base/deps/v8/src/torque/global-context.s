	.file	"global-context.cc"
	.text
	.section	.rodata._ZNK2v88internal6torque10Declarable9type_nameEv.str1.1,"aMS",@progbits,1
.LC0:
	.string	"<<unknown>>"
	.section	.text._ZNK2v88internal6torque10Declarable9type_nameEv,"axG",@progbits,_ZNK2v88internal6torque10Declarable9type_nameEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal6torque10Declarable9type_nameEv
	.type	_ZNK2v88internal6torque10Declarable9type_nameEv, @function
_ZNK2v88internal6torque10Declarable9type_nameEv:
.LFB6076:
	.cfi_startproc
	endbr64
	leaq	.LC0(%rip), %rax
	ret
	.cfi_endproc
.LFE6076:
	.size	_ZNK2v88internal6torque10Declarable9type_nameEv, .-_ZNK2v88internal6torque10Declarable9type_nameEv
	.section	.rodata._ZNK2v88internal6torque5Scope9type_nameEv.str1.1,"aMS",@progbits,1
.LC1:
	.string	"scope"
	.section	.text._ZNK2v88internal6torque5Scope9type_nameEv,"axG",@progbits,_ZNK2v88internal6torque5Scope9type_nameEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal6torque5Scope9type_nameEv
	.type	_ZNK2v88internal6torque5Scope9type_nameEv, @function
_ZNK2v88internal6torque5Scope9type_nameEv:
.LFB6089:
	.cfi_startproc
	endbr64
	leaq	.LC1(%rip), %rax
	ret
	.cfi_endproc
.LFE6089:
	.size	_ZNK2v88internal6torque5Scope9type_nameEv, .-_ZNK2v88internal6torque5Scope9type_nameEv
	.section	.text._ZN2v88internal6torque10DeclarableD2Ev,"axG",@progbits,_ZN2v88internal6torque10DeclarableD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque10DeclarableD2Ev
	.type	_ZN2v88internal6torque10DeclarableD2Ev, @function
_ZN2v88internal6torque10DeclarableD2Ev:
.LFB6094:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE6094:
	.size	_ZN2v88internal6torque10DeclarableD2Ev, .-_ZN2v88internal6torque10DeclarableD2Ev
	.weak	_ZN2v88internal6torque10DeclarableD1Ev
	.set	_ZN2v88internal6torque10DeclarableD1Ev,_ZN2v88internal6torque10DeclarableD2Ev
	.section	.rodata._ZNK2v88internal6torque9Namespace9type_nameEv.str1.1,"aMS",@progbits,1
.LC2:
	.string	"namespace"
	.section	.text._ZNK2v88internal6torque9Namespace9type_nameEv,"axG",@progbits,_ZNK2v88internal6torque9Namespace9type_nameEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal6torque9Namespace9type_nameEv
	.type	_ZNK2v88internal6torque9Namespace9type_nameEv, @function
_ZNK2v88internal6torque9Namespace9type_nameEv:
.LFB6143:
	.cfi_startproc
	endbr64
	leaq	.LC2(%rip), %rax
	ret
	.cfi_endproc
.LFE6143:
	.size	_ZNK2v88internal6torque9Namespace9type_nameEv, .-_ZNK2v88internal6torque9Namespace9type_nameEv
	.section	.text._ZN2v88internal6torque10DeclarableD0Ev,"axG",@progbits,_ZN2v88internal6torque10DeclarableD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque10DeclarableD0Ev
	.type	_ZN2v88internal6torque10DeclarableD0Ev, @function
_ZN2v88internal6torque10DeclarableD0Ev:
.LFB6096:
	.cfi_startproc
	endbr64
	movl	$72, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE6096:
	.size	_ZN2v88internal6torque10DeclarableD0Ev, .-_ZN2v88internal6torque10DeclarableD0Ev
	.section	.text._ZN2v88internal6torque5ScopeD2Ev,"axG",@progbits,_ZN2v88internal6torque5ScopeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque5ScopeD2Ev
	.type	_ZN2v88internal6torque5ScopeD2Ev, @function
_ZN2v88internal6torque5ScopeD2Ev:
.LFB6148:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal6torque5ScopeE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	88(%rdi), %r12
	movq	%rax, (%rdi)
	testq	%r12, %r12
	jne	.L12
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L27:
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	je	.L8
.L11:
	movq	%r13, %r12
.L12:
	movq	40(%r12), %rdi
	movq	(%r12), %r13
	testq	%rdi, %rdi
	je	.L9
	call	_ZdlPv@PLT
.L9:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	jne	.L27
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L11
.L8:
	movq	80(%rbx), %rax
	movq	72(%rbx), %rdi
	xorl	%esi, %esi
	addq	$120, %rbx
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	-48(%rbx), %rdi
	movq	$0, -24(%rbx)
	movq	$0, -32(%rbx)
	cmpq	%rbx, %rdi
	je	.L7
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L7:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE6148:
	.size	_ZN2v88internal6torque5ScopeD2Ev, .-_ZN2v88internal6torque5ScopeD2Ev
	.weak	_ZN2v88internal6torque5ScopeD1Ev
	.set	_ZN2v88internal6torque5ScopeD1Ev,_ZN2v88internal6torque5ScopeD2Ev
	.section	.text._ZN2v88internal6torque9NamespaceD0Ev,"axG",@progbits,_ZN2v88internal6torque9NamespaceD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque9NamespaceD0Ev
	.type	_ZN2v88internal6torque9NamespaceD0Ev, @function
_ZN2v88internal6torque9NamespaceD0Ev:
.LFB8426:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal6torque9NamespaceE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	%rax, (%rdi)
	movq	128(%rdi), %rdi
	leaq	144(%r12), %rax
	cmpq	%rax, %rdi
	je	.L29
	call	_ZdlPv@PLT
.L29:
	movq	88(%r12), %r13
	leaq	16+_ZTVN2v88internal6torque5ScopeE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	jne	.L34
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L49:
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L30
.L33:
	movq	%rbx, %r13
.L34:
	movq	40(%r13), %rdi
	movq	0(%r13), %rbx
	testq	%rdi, %rdi
	je	.L31
	call	_ZdlPv@PLT
.L31:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	jne	.L49
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L33
.L30:
	movq	80(%r12), %rax
	movq	72(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	72(%r12), %rdi
	leaq	120(%r12), %rax
	movq	$0, 96(%r12)
	movq	$0, 88(%r12)
	cmpq	%rax, %rdi
	je	.L35
	call	_ZdlPv@PLT
.L35:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$160, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE8426:
	.size	_ZN2v88internal6torque9NamespaceD0Ev, .-_ZN2v88internal6torque9NamespaceD0Ev
	.section	.text._ZN2v88internal6torque5ScopeD0Ev,"axG",@progbits,_ZN2v88internal6torque5ScopeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque5ScopeD0Ev
	.type	_ZN2v88internal6torque5ScopeD0Ev, @function
_ZN2v88internal6torque5ScopeD0Ev:
.LFB6150:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal6torque5ScopeE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	88(%rdi), %r13
	movq	%rax, (%rdi)
	testq	%r13, %r13
	jne	.L55
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L70:
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L51
.L54:
	movq	%rbx, %r13
.L55:
	movq	40(%r13), %rdi
	movq	0(%r13), %rbx
	testq	%rdi, %rdi
	je	.L52
	call	_ZdlPv@PLT
.L52:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	jne	.L70
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L54
.L51:
	movq	80(%r12), %rax
	movq	72(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	72(%r12), %rdi
	leaq	120(%r12), %rax
	movq	$0, 96(%r12)
	movq	$0, 88(%r12)
	cmpq	%rax, %rdi
	je	.L56
	call	_ZdlPv@PLT
.L56:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$128, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE6150:
	.size	_ZN2v88internal6torque5ScopeD0Ev, .-_ZN2v88internal6torque5ScopeD0Ev
	.section	.text._ZN2v88internal6torque9NamespaceD2Ev,"axG",@progbits,_ZN2v88internal6torque9NamespaceD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque9NamespaceD2Ev
	.type	_ZN2v88internal6torque9NamespaceD2Ev, @function
_ZN2v88internal6torque9NamespaceD2Ev:
.LFB8424:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal6torque9NamespaceE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	128(%rdi), %rdi
	leaq	144(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L72
	call	_ZdlPv@PLT
.L72:
	movq	88(%rbx), %r12
	leaq	16+_ZTVN2v88internal6torque5ScopeE(%rip), %rax
	movq	%rax, (%rbx)
	testq	%r12, %r12
	jne	.L77
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L92:
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	je	.L73
.L76:
	movq	%r13, %r12
.L77:
	movq	40(%r12), %rdi
	movq	(%r12), %r13
	testq	%rdi, %rdi
	je	.L74
	call	_ZdlPv@PLT
.L74:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	jne	.L92
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L76
.L73:
	movq	80(%rbx), %rax
	movq	72(%rbx), %rdi
	xorl	%esi, %esi
	addq	$120, %rbx
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	-48(%rbx), %rdi
	movq	$0, -24(%rbx)
	movq	$0, -32(%rbx)
	cmpq	%rbx, %rdi
	je	.L71
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L71:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8424:
	.size	_ZN2v88internal6torque9NamespaceD2Ev, .-_ZN2v88internal6torque9NamespaceD2Ev
	.weak	_ZN2v88internal6torque9NamespaceD1Ev
	.set	_ZN2v88internal6torque9NamespaceD1Ev,_ZN2v88internal6torque9NamespaceD2Ev
	.section	.text._ZN2v88internal6torque21ContextualVariableTopINS1_13GlobalContextEEERPNT_5ScopeEv,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal6torque21ContextualVariableTopINS1_13GlobalContextEEERPNT_5ScopeEv
	.type	_ZN2v88internal6torque21ContextualVariableTopINS1_13GlobalContextEEERPNT_5ScopeEv, @function
_ZN2v88internal6torque21ContextualVariableTopINS1_13GlobalContextEEERPNT_5ScopeEv:
.LFB6432:
	.cfi_startproc
	endbr64
	movq	%fs:0, %rax
	addq	$_ZZN2v88internal6torque21ContextualVariableTopINS1_13GlobalContextEEERPNT_5ScopeEvE3top@tpoff, %rax
	ret
	.cfi_endproc
.LFE6432:
	.size	_ZN2v88internal6torque21ContextualVariableTopINS1_13GlobalContextEEERPNT_5ScopeEv, .-_ZN2v88internal6torque21ContextualVariableTopINS1_13GlobalContextEEERPNT_5ScopeEv
	.section	.text._ZN2v88internal6torque21ContextualVariableTopINS1_18TargetArchitectureEEERPNT_5ScopeEv,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal6torque21ContextualVariableTopINS1_18TargetArchitectureEEERPNT_5ScopeEv
	.type	_ZN2v88internal6torque21ContextualVariableTopINS1_18TargetArchitectureEEERPNT_5ScopeEv, @function
_ZN2v88internal6torque21ContextualVariableTopINS1_18TargetArchitectureEEERPNT_5ScopeEv:
.LFB6433:
	.cfi_startproc
	endbr64
	movq	%fs:0, %rax
	addq	$_ZZN2v88internal6torque21ContextualVariableTopINS1_18TargetArchitectureEEERPNT_5ScopeEvE3top@tpoff, %rax
	ret
	.cfi_endproc
.LFE6433:
	.size	_ZN2v88internal6torque21ContextualVariableTopINS1_18TargetArchitectureEEERPNT_5ScopeEv, .-_ZN2v88internal6torque21ContextualVariableTopINS1_18TargetArchitectureEEERPNT_5ScopeEv
	.section	.text._ZN2v88internal6torque18TargetArchitectureC2Eb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque18TargetArchitectureC2Eb
	.type	_ZN2v88internal6torque18TargetArchitectureC2Eb, @function
_ZN2v88internal6torque18TargetArchitectureC2Eb:
.LFB6492:
	.cfi_startproc
	endbr64
	cmpb	$1, %sil
	sbbl	%eax, %eax
	andl	$4, %eax
	addl	$4, %eax
	movl	%eax, (%rdi)
	movl	%eax, 4(%rdi)
	ret
	.cfi_endproc
.LFE6492:
	.size	_ZN2v88internal6torque18TargetArchitectureC2Eb, .-_ZN2v88internal6torque18TargetArchitectureC2Eb
	.globl	_ZN2v88internal6torque18TargetArchitectureC1Eb
	.set	_ZN2v88internal6torque18TargetArchitectureC1Eb,_ZN2v88internal6torque18TargetArchitectureC2Eb
	.section	.rodata._ZN2v84base11make_uniqueINS_8internal6torque9NamespaceEJRKPKcEEESt10unique_ptrIT_St14default_deleteISA_EEDpOT0_.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"basic_string::_M_construct null not valid"
	.section	.text._ZN2v84base11make_uniqueINS_8internal6torque9NamespaceEJRKPKcEEESt10unique_ptrIT_St14default_deleteISA_EEDpOT0_,"axG",@progbits,_ZN2v84base11make_uniqueINS_8internal6torque9NamespaceEJRKPKcEEESt10unique_ptrIT_St14default_deleteISA_EEDpOT0_,comdat
	.p2align 4
	.weak	_ZN2v84base11make_uniqueINS_8internal6torque9NamespaceEJRKPKcEEESt10unique_ptrIT_St14default_deleteISA_EEDpOT0_
	.type	_ZN2v84base11make_uniqueINS_8internal6torque9NamespaceEJRKPKcEEESt10unique_ptrIT_St14default_deleteISA_EEDpOT0_, @function
_ZN2v84base11make_uniqueINS_8internal6torque9NamespaceEJRKPKcEEESt10unique_ptrIT_St14default_deleteISA_EEDpOT0_:
.LFB7604:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA7604
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	-80(%rbp), %rbx
	subq	$88, %rsp
	movq	(%rsi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rbx, -96(%rbp)
	testq	%r15, %r15
	je	.L138
	movq	%rdi, %r14
	movq	%r15, %rdi
	call	strlen@PLT
	movq	%rax, -104(%rbp)
	movq	%rax, %r12
	cmpq	$15, %rax
	ja	.L139
	cmpq	$1, %rax
	jne	.L102
	movzbl	(%r15), %edx
	movb	%dl, -80(%rbp)
	movq	%rbx, %rdx
.L103:
	movq	%rax, -88(%rbp)
	movl	$160, %edi
	movb	$0, (%rdx,%rax)
.LEHB0:
	call	_Znwm@PLT
.LEHE0:
	movq	%rax, %r12
	leaq	16+_ZTVN2v88internal6torque10DeclarableE(%rip), %rax
	movq	%rax, (%r12)
	movl	$0, 8(%r12)
.LEHB1:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rax
	movq	(%rax), %rax
	movq	%rax, 16(%r12)
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
.LEHE1:
	movq	(%rax), %rax
	movq	-96(%rbp), %r15
	pcmpeqd	%xmm0, %xmm0
	leaq	144(%r12), %rdi
	movq	-88(%rbp), %r13
	movb	$1, 64(%r12)
	movdqu	(%rax), %xmm1
	movl	16(%rax), %eax
	movups	%xmm0, 44(%r12)
	movl	$-1, 60(%r12)
	movl	%eax, 40(%r12)
	leaq	120(%r12), %rax
	movq	%rax, -120(%rbp)
	movq	%rax, 72(%r12)
	leaq	16+_ZTVN2v88internal6torque9NamespaceE(%rip), %rax
	movq	%rax, (%r12)
	movq	%r15, %rax
	addq	%r13, %rax
	movups	%xmm1, 24(%r12)
	movq	$1, 80(%r12)
	movq	$0, 88(%r12)
	movq	$0, 96(%r12)
	movl	$0x3f800000, 104(%r12)
	movq	$0, 112(%r12)
	movq	$0, 120(%r12)
	movq	%rdi, 128(%r12)
	je	.L104
	testq	%r15, %r15
	je	.L140
.L104:
	movq	%r13, -104(%rbp)
	cmpq	$15, %r13
	ja	.L141
	cmpq	$1, %r13
	jne	.L107
	movzbl	(%r15), %eax
	movb	%al, 144(%r12)
.L108:
	movq	%r13, 136(%r12)
	movb	$0, (%rdi,%r13)
	movq	-96(%rbp), %rdi
	movq	%r12, (%r14)
	cmpq	%rbx, %rdi
	je	.L98
	call	_ZdlPv@PLT
.L98:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L142
	addq	$88, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L102:
	.cfi_restore_state
	testq	%rax, %rax
	jne	.L143
	movq	%rbx, %rdx
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L107:
	testq	%r13, %r13
	je	.L108
	jmp	.L106
	.p2align 4,,10
	.p2align 3
.L139:
	leaq	-96(%rbp), %r13
	leaq	-104(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r13, %rdi
.LEHB2:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE2:
	movq	%rax, -96(%rbp)
	movq	%rax, %rdi
	movq	-104(%rbp), %rax
	movq	%rax, -80(%rbp)
.L101:
	movq	%r12, %rdx
	movq	%r15, %rsi
	call	memcpy@PLT
	movq	-104(%rbp), %rax
	movq	-96(%rbp), %rdx
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L141:
	leaq	-104(%rbp), %rsi
	leaq	128(%r12), %rdi
	xorl	%edx, %edx
.LEHB3:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE3:
	movq	%rax, 128(%r12)
	movq	%rax, %rdi
	movq	-104(%rbp), %rax
	movq	%rax, 144(%r12)
.L106:
	movq	%r13, %rdx
	movq	%r15, %rsi
	call	memcpy@PLT
	movq	-104(%rbp), %r13
	movq	128(%r12), %rdi
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L138:
	leaq	.LC3(%rip), %rdi
.LEHB4:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE4:
.L142:
	call	__stack_chk_fail@PLT
.L140:
	leaq	.LC3(%rip), %rdi
.LEHB5:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE5:
.L143:
	movq	%rbx, %rdi
	jmp	.L101
.L122:
	endbr64
	movq	%rax, %r13
	jmp	.L118
.L123:
	endbr64
	movq	%rax, %r13
	jmp	.L117
.L124:
	endbr64
	movq	%rax, %r13
	jmp	.L111
.L144:
	movq	40(%r15), %rdi
	movq	(%r15), %r14
	testq	%rdi, %rdi
	je	.L113
	call	_ZdlPv@PLT
.L113:
	movq	8(%r15), %rdi
	leaq	24(%r15), %rax
	cmpq	%rax, %rdi
	je	.L114
	call	_ZdlPv@PLT
.L114:
	movq	%r15, %rdi
	movq	%r14, %r15
	call	_ZdlPv@PLT
.L115:
	testq	%r15, %r15
	jne	.L144
	movq	80(%r12), %rax
	movq	72(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	72(%r12), %rdi
	movq	$0, 96(%r12)
	movq	$0, 88(%r12)
	cmpq	%rdi, -120(%rbp)
	je	.L117
	call	_ZdlPv@PLT
.L117:
	movl	$160, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L118:
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L119
	call	_ZdlPv@PLT
.L119:
	movq	%r13, %rdi
.LEHB6:
	call	_Unwind_Resume@PLT
.LEHE6:
.L111:
	leaq	16+_ZTVN2v88internal6torque5ScopeE(%rip), %rax
	movq	88(%r12), %r15
	movq	%rax, (%r12)
	jmp	.L115
	.cfi_endproc
.LFE7604:
	.globl	__gxx_personality_v0
	.section	.gcc_except_table._ZN2v84base11make_uniqueINS_8internal6torque9NamespaceEJRKPKcEEESt10unique_ptrIT_St14default_deleteISA_EEDpOT0_,"aG",@progbits,_ZN2v84base11make_uniqueINS_8internal6torque9NamespaceEJRKPKcEEESt10unique_ptrIT_St14default_deleteISA_EEDpOT0_,comdat
.LLSDA7604:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE7604-.LLSDACSB7604
.LLSDACSB7604:
	.uleb128 .LEHB0-.LFB7604
	.uleb128 .LEHE0-.LEHB0
	.uleb128 .L122-.LFB7604
	.uleb128 0
	.uleb128 .LEHB1-.LFB7604
	.uleb128 .LEHE1-.LEHB1
	.uleb128 .L123-.LFB7604
	.uleb128 0
	.uleb128 .LEHB2-.LFB7604
	.uleb128 .LEHE2-.LEHB2
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB3-.LFB7604
	.uleb128 .LEHE3-.LEHB3
	.uleb128 .L124-.LFB7604
	.uleb128 0
	.uleb128 .LEHB4-.LFB7604
	.uleb128 .LEHE4-.LEHB4
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB5-.LFB7604
	.uleb128 .LEHE5-.LEHB5
	.uleb128 .L124-.LFB7604
	.uleb128 0
	.uleb128 .LEHB6-.LFB7604
	.uleb128 .LEHE6-.LEHB6
	.uleb128 0
	.uleb128 0
.LLSDACSE7604:
	.section	.text._ZN2v84base11make_uniqueINS_8internal6torque9NamespaceEJRKPKcEEESt10unique_ptrIT_St14default_deleteISA_EEDpOT0_,"axG",@progbits,_ZN2v84base11make_uniqueINS_8internal6torque9NamespaceEJRKPKcEEESt10unique_ptrIT_St14default_deleteISA_EEDpOT0_,comdat
	.size	_ZN2v84base11make_uniqueINS_8internal6torque9NamespaceEJRKPKcEEESt10unique_ptrIT_St14default_deleteISA_EEDpOT0_, .-_ZN2v84base11make_uniqueINS_8internal6torque9NamespaceEJRKPKcEEESt10unique_ptrIT_St14default_deleteISA_EEDpOT0_
	.section	.text._ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_NS2_13GlobalContext14PerFileStreamsEESt10_Select1stIS8_ESt4lessIS3_ESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E,"axG",@progbits,_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_NS2_13GlobalContext14PerFileStreamsEESt10_Select1stIS8_ESt4lessIS3_ESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_NS2_13GlobalContext14PerFileStreamsEESt10_Select1stIS8_ESt4lessIS3_ESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E
	.type	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_NS2_13GlobalContext14PerFileStreamsEESt10_Select1stIS8_ESt4lessIS3_ESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E, @function
_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_NS2_13GlobalContext14PerFileStreamsEESt10_Select1stIS8_ESt4lessIS3_ESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E:
.LFB8401:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -72(%rbp)
	testq	%rsi, %rsi
	je	.L145
	leaq	64+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %r14
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %r13
	movq	%rdx, %xmm2
	movq	%rax, %xmm3
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %r12
	movq	%rsi, %rbx
	punpcklqdq	%xmm3, %xmm2
	movaps	%xmm2, -64(%rbp)
.L150:
	movq	24(%rbx), %rsi
	movq	-72(%rbp), %rdi
	movq	%rbx, %r15
	call	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_NS2_13GlobalContext14PerFileStreamsEESt10_Select1stIS8_ESt4lessIS3_ESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	16(%rbx), %rbx
	movdqa	-64(%rbp), %xmm0
	movq	%rax, 432(%r15)
	movq	528(%r15), %rdi
	addq	$80, %rax
	movq	%rax, 560(%r15)
	leaq	544(%r15), %rax
	movups	%xmm0, 448(%r15)
	cmpq	%rax, %rdi
	je	.L147
	call	_ZdlPv@PLT
.L147:
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	512(%r15), %rdi
	movq	%rax, 456(%r15)
	call	_ZNSt6localeD1Ev@PLT
	movq	%r14, 432(%r15)
	movq	-24(%r14), %rax
	leaq	560(%r15), %rdi
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%rcx, 432(%r15,%rax)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%r13, 448(%r15)
	movq	-24(%r13), %rax
	movq	%rdx, 448(%r15,%rax)
	movq	%r12, 432(%r15)
	movq	-24(%r12), %rax
	movq	%rcx, 432(%r15,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 560(%r15)
	movq	$0, 440(%r15)
	call	_ZNSt8ios_baseD2Ev@PLT
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movdqa	-64(%rbp), %xmm1
	movq	136(%r15), %rdi
	movq	%rax, 40(%r15)
	addq	$80, %rax
	movq	%rax, 168(%r15)
	leaq	152(%r15), %rax
	movups	%xmm1, 56(%r15)
	cmpq	%rax, %rdi
	je	.L148
	call	_ZdlPv@PLT
.L148:
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	120(%r15), %rdi
	movq	%rax, 64(%r15)
	call	_ZNSt6localeD1Ev@PLT
	movq	%r14, 40(%r15)
	movq	-24(%r14), %rax
	leaq	168(%r15), %rdi
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rsi
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rdx, 40(%r15,%rax)
	movq	%r13, 56(%r15)
	movq	-24(%r13), %rax
	movq	%rsi, 56(%r15,%rax)
	movq	%r12, 40(%r15)
	movq	-24(%r12), %rax
	movq	%rcx, 40(%r15,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	$0, 48(%r15)
	movq	%rax, 168(%r15)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L150
.L145:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8401:
	.size	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_NS2_13GlobalContext14PerFileStreamsEESt10_Select1stIS8_ESt4lessIS3_ESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E, .-_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_NS2_13GlobalContext14PerFileStreamsEESt10_Select1stIS8_ESt4lessIS3_ESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E
	.section	.rodata._ZNSt6vectorISt10unique_ptrIN2v88internal6torque10DeclarableESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_.str1.1,"aMS",@progbits,1
.LC5:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorISt10unique_ptrIN2v88internal6torque10DeclarableESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_,"axG",@progbits,_ZNSt6vectorISt10unique_ptrIN2v88internal6torque10DeclarableESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorISt10unique_ptrIN2v88internal6torque10DeclarableESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	.type	_ZNSt6vectorISt10unique_ptrIN2v88internal6torque10DeclarableESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_, @function
_ZNSt6vectorISt10unique_ptrIN2v88internal6torque10DeclarableESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_:
.LFB9412:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rax
	movq	(%rdi), %r12
	movq	%rdi, -80(%rbp)
	movq	%rax, -72(%rbp)
	subq	%r12, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L179
	movq	%rsi, %r9
	movq	%rsi, %r15
	movq	%rsi, %rbx
	subq	%r12, %r9
	testq	%rax, %rax
	je	.L170
	movabsq	$9223372036854775800, %r13
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L180
.L157:
	movq	%r13, %rdi
	movq	%rdx, -96(%rbp)
	movq	%r9, -88(%rbp)
	call	_Znwm@PLT
	movq	-88(%rbp), %r9
	movq	-96(%rbp), %rdx
	addq	%rax, %r13
	movq	%rax, -56(%rbp)
	leaq	8(%rax), %rcx
	movq	%r13, -64(%rbp)
.L169:
	movq	(%rdx), %rax
	movq	-56(%rbp), %rsi
	movq	$0, (%rdx)
	movq	%rax, (%rsi,%r9)
	cmpq	%r12, %r15
	je	.L159
	movq	%rsi, %r13
	movq	%r12, %r14
	.p2align 4,,10
	.p2align 3
.L163:
	movq	(%r14), %rcx
	movq	$0, (%r14)
	movq	%rcx, 0(%r13)
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L160
	movq	(%rdi), %rcx
	addq	$8, %r14
	addq	$8, %r13
	call	*8(%rcx)
	cmpq	%r14, %r15
	jne	.L163
.L161:
	movq	-56(%rbp), %rsi
	movq	%r15, %rax
	subq	%r12, %rax
	leaq	8(%rsi,%rax), %rcx
.L159:
	movq	-72(%rbp), %rax
	cmpq	%rax, %r15
	je	.L164
	movq	%rax, %r14
	subq	%r15, %r14
	leaq	-8(%r14), %r9
	movq	%r9, %rdi
	shrq	$3, %rdi
	addq	$1, %rdi
	testq	%r9, %r9
	je	.L172
	movq	%rdi, %rdx
	xorl	%eax, %eax
	shrq	%rdx
	salq	$4, %rdx
	.p2align 4,,10
	.p2align 3
.L166:
	movdqu	(%r15,%rax), %xmm1
	movups	%xmm1, (%rcx,%rax)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L166
	movq	%rdi, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %r8
	leaq	(%rcx,%r8), %rdx
	leaq	(%r15,%r8), %rbx
	cmpq	%rax, %rdi
	je	.L167
.L165:
	movq	(%rbx), %rax
	movq	%rax, (%rdx)
.L167:
	leaq	8(%rcx,%r9), %rcx
.L164:
	testq	%r12, %r12
	je	.L168
	movq	%r12, %rdi
	movq	%rcx, -72(%rbp)
	call	_ZdlPv@PLT
	movq	-72(%rbp), %rcx
.L168:
	movq	-56(%rbp), %xmm0
	movq	-80(%rbp), %rax
	movq	%rcx, %xmm2
	movq	-64(%rbp), %rbx
	punpcklqdq	%xmm2, %xmm0
	movq	%rbx, 16(%rax)
	movups	%xmm0, (%rax)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L160:
	.cfi_restore_state
	addq	$8, %r14
	addq	$8, %r13
	cmpq	%r14, %r15
	jne	.L163
	jmp	.L161
	.p2align 4,,10
	.p2align 3
.L180:
	testq	%rdi, %rdi
	jne	.L158
	movq	$0, -64(%rbp)
	movl	$8, %ecx
	movq	$0, -56(%rbp)
	jmp	.L169
	.p2align 4,,10
	.p2align 3
.L170:
	movl	$8, %r13d
	jmp	.L157
.L172:
	movq	%rcx, %rdx
	jmp	.L165
.L158:
	cmpq	%rcx, %rdi
	movq	%rcx, %rax
	cmovbe	%rdi, %rax
	leaq	0(,%rax,8), %r13
	jmp	.L157
.L179:
	leaq	.LC5(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE9412:
	.size	_ZNSt6vectorISt10unique_ptrIN2v88internal6torque10DeclarableESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_, .-_ZNSt6vectorISt10unique_ptrIN2v88internal6torque10DeclarableESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	.section	.text._ZNSt8_Rb_treeIN2v88internal6torque8SourceIdES3_St9_IdentityIS3_ESt4lessIS3_ESaIS3_EE8_M_eraseEPSt13_Rb_tree_nodeIS3_E,"axG",@progbits,_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdES3_St9_IdentityIS3_ESt4lessIS3_ESaIS3_EE8_M_eraseEPSt13_Rb_tree_nodeIS3_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdES3_St9_IdentityIS3_ESt4lessIS3_ESaIS3_EE8_M_eraseEPSt13_Rb_tree_nodeIS3_E
	.type	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdES3_St9_IdentityIS3_ESt4lessIS3_ESaIS3_EE8_M_eraseEPSt13_Rb_tree_nodeIS3_E, @function
_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdES3_St9_IdentityIS3_ESt4lessIS3_ESaIS3_EE8_M_eraseEPSt13_Rb_tree_nodeIS3_E:
.LFB10008:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L189
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
.L183:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdES3_St9_IdentityIS3_ESt4lessIS3_ESaIS3_EE8_M_eraseEPSt13_Rb_tree_nodeIS3_E
	movq	%rbx, %rdi
	movq	16(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L183
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L189:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE10008:
	.size	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdES3_St9_IdentityIS3_ESt4lessIS3_ESaIS3_EE8_M_eraseEPSt13_Rb_tree_nodeIS3_E, .-_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdES3_St9_IdentityIS3_ESt4lessIS3_ESaIS3_EE8_M_eraseEPSt13_Rb_tree_nodeIS3_E
	.section	.text._ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_St3setIS3_St4lessIS3_ESaIS3_EEESt10_Select1stISB_ES8_SaISB_EE8_M_eraseEPSt13_Rb_tree_nodeISB_E,"axG",@progbits,_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_St3setIS3_St4lessIS3_ESaIS3_EEESt10_Select1stISB_ES8_SaISB_EE8_M_eraseEPSt13_Rb_tree_nodeISB_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_St3setIS3_St4lessIS3_ESaIS3_EEESt10_Select1stISB_ES8_SaISB_EE8_M_eraseEPSt13_Rb_tree_nodeISB_E
	.type	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_St3setIS3_St4lessIS3_ESaIS3_EEESt10_Select1stISB_ES8_SaISB_EE8_M_eraseEPSt13_Rb_tree_nodeISB_E, @function
_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_St3setIS3_St4lessIS3_ESaIS3_EEESt10_Select1stISB_ES8_SaISB_EE8_M_eraseEPSt13_Rb_tree_nodeISB_E:
.LFB7792:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L206
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
.L196:
	movq	24(%r12), %rsi
	movq	%r12, %r13
	movq	%r14, %rdi
	leaq	40(%r13), %r15
	call	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_St3setIS3_St4lessIS3_ESaIS3_EEESt10_Select1stISB_ES8_SaISB_EE8_M_eraseEPSt13_Rb_tree_nodeISB_E
	movq	56(%r13), %rbx
	movq	16(%r12), %r12
	testq	%rbx, %rbx
	je	.L194
.L195:
	movq	24(%rbx), %rsi
	movq	%r15, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdES3_St9_IdentityIS3_ESt4lessIS3_ESaIS3_EE8_M_eraseEPSt13_Rb_tree_nodeIS3_E
	movq	%rbx, %rdi
	movq	16(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L195
.L194:
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L196
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L206:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.cfi_endproc
.LFE7792:
	.size	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_St3setIS3_St4lessIS3_ESaIS3_EEESt10_Select1stISB_ES8_SaISB_EE8_M_eraseEPSt13_Rb_tree_nodeISB_E, .-_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_St3setIS3_St4lessIS3_ESaIS3_EEESt10_Select1stISB_ES8_SaISB_EE8_M_eraseEPSt13_Rb_tree_nodeISB_E
	.section	.text._ZN2v88internal6torque3AstD2Ev,"axG",@progbits,_ZN2v88internal6torque3AstD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque3AstD2Ev
	.type	_ZN2v88internal6torque3AstD2Ev, @function
_ZN2v88internal6torque3AstD2Ev:
.LFB6454:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	48(%rdi), %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	64(%rdi), %r12
	testq	%r12, %r12
	je	.L214
.L210:
	movq	24(%r12), %rsi
	movq	%r12, %r14
	movq	%r15, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_St3setIS3_St4lessIS3_ESaIS3_EEESt10_Select1stISB_ES8_SaISB_EE8_M_eraseEPSt13_Rb_tree_nodeISB_E
	movq	56(%r14), %r13
	movq	16(%r12), %r12
	leaq	40(%r14), %rax
	testq	%r13, %r13
	je	.L215
.L213:
	movq	24(%r13), %rsi
	movq	%rax, %rdi
	movq	%rax, -56(%rbp)
	call	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdES3_St9_IdentityIS3_ESt4lessIS3_ESaIS3_EE8_M_eraseEPSt13_Rb_tree_nodeIS3_E
	movq	%r13, %rdi
	movq	16(%r13), %r13
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rax
	testq	%r13, %r13
	jne	.L213
.L215:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L210
.L214:
	movq	32(%rbx), %r13
	movq	24(%rbx), %r12
	cmpq	%r12, %r13
	je	.L211
	.p2align 4,,10
	.p2align 3
.L212:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L216
	movq	(%rdi), %rax
	addq	$8, %r12
	call	*8(%rax)
	cmpq	%r12, %r13
	jne	.L212
.L217:
	movq	24(%rbx), %r12
.L211:
	testq	%r12, %r12
	je	.L219
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L219:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L209
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L216:
	.cfi_restore_state
	addq	$8, %r12
	cmpq	%r12, %r13
	jne	.L212
	jmp	.L217
	.p2align 4,,10
	.p2align 3
.L209:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE6454:
	.size	_ZN2v88internal6torque3AstD2Ev, .-_ZN2v88internal6torque3AstD2Ev
	.weak	_ZN2v88internal6torque3AstD1Ev
	.set	_ZN2v88internal6torque3AstD1Ev,_ZN2v88internal6torque3AstD2Ev
	.section	.text.unlikely._ZN2v88internal6torque13GlobalContextC2ENS1_3AstE,"ax",@progbits
	.align 2
.LCOLDB6:
	.section	.text._ZN2v88internal6torque13GlobalContextC2ENS1_3AstE,"ax",@progbits
.LHOTB6:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque13GlobalContextC2ENS1_3AstE
	.type	_ZN2v88internal6torque13GlobalContextC2ENS1_3AstE, @function
_ZN2v88internal6torque13GlobalContextC2ENS1_3AstE:
.LFB6489:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6489
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	leaq	72(%rdi), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$80, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	xorl	%eax, %eax
	movw	%ax, (%rdi)
	movq	(%rsi), %rax
	movq	%rax, 16(%rdi)
	movq	8(%rsi), %rax
	movups	%xmm0, (%rsi)
	movq	%rax, 24(%rdi)
	movq	16(%rsi), %rax
	movq	%rax, 32(%rdi)
	movq	24(%rsi), %rax
	movups	%xmm0, 16(%rsi)
	movq	%rax, 40(%rdi)
	movq	32(%rsi), %rax
	movq	%rax, 48(%rdi)
	movq	40(%rsi), %rax
	movups	%xmm0, 32(%rsi)
	movq	%rax, 56(%rdi)
	movq	64(%rsi), %rax
	testq	%rax, %rax
	je	.L235
	movl	56(%rsi), %ecx
	movq	%rax, 80(%rdi)
	movl	%ecx, 72(%rdi)
	movq	72(%rsi), %rcx
	movq	%rcx, 88(%rdi)
	movq	80(%rsi), %rcx
	movq	%rcx, 96(%rdi)
	movq	%rdx, 8(%rax)
	movq	88(%rsi), %rax
	movq	$0, 64(%rsi)
	movq	%rax, 104(%rdi)
	leaq	56(%rsi), %rax
	movq	%rax, 72(%rsi)
	movq	%rax, 80(%rsi)
	movq	$0, 88(%rsi)
.L236:
	leaq	168(%rbx), %rax
	pxor	%xmm0, %xmm0
	movl	$0, 168(%rbx)
	movq	$0, 176(%rbx)
	movq	%rax, 184(%rbx)
	movq	%rax, 192(%rbx)
	movq	$0, 200(%rbx)
	movq	$0, 224(%rbx)
	movq	$0, 232(%rbx)
	movups	%xmm0, 112(%rbx)
	movups	%xmm0, 128(%rbx)
	movups	%xmm0, 144(%rbx)
	movups	%xmm0, 208(%rbx)
	movq	$0, -96(%rbp)
.LEHB7:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rax
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
.LEHE7:
	leaq	-96(%rbp), %rdx
	movq	%rdx, (%rax)
.LEHB8:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_17CurrentSourceFileEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rax
	movl	(%rax), %eax
	movq	$-1, -76(%rbp)
	movq	$-1, -68(%rbp)
	movl	%eax, -80(%rbp)
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rax
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
.LEHE8:
	leaq	-80(%rbp), %rdx
	leaq	-112(%rbp), %rdi
	movq	%rdx, (%rax)
	leaq	_ZN2v88internal6torqueL18kBaseNamespaceNameE(%rip), %rsi
.LEHB9:
	call	_ZN2v84base11make_uniqueINS_8internal6torque9NamespaceEJRKPKcEEESt10unique_ptrIT_St14default_deleteISA_EEDpOT0_
.LEHE9:
	movq	-112(%rbp), %r12
	movq	120(%rbx), %rsi
	movq	$0, -112(%rbp)
	movq	%r12, -104(%rbp)
	cmpq	128(%rbx), %rsi
	je	.L237
	movq	$0, -104(%rbp)
	movq	%r12, (%rsi)
	addq	$8, 120(%rbx)
.L238:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L239
	movq	(%rdi), %rax
	call	*8(%rax)
.L239:
	movq	-112(%rbp), %rdi
	movq	%r12, 8(%rbx)
	testq	%rdi, %rdi
	je	.L241
	movq	(%rdi), %rax
	call	*8(%rax)
.L241:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
	movq	-56(%rbp), %rdx
	movq	%rdx, (%rax)
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
	movq	-88(%rbp), %rdx
	movq	%rdx, (%rax)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L283
	addq	$80, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L235:
	.cfi_restore_state
	movl	$0, 72(%rdi)
	movq	$0, 80(%rdi)
	movq	%rdx, 88(%rdi)
	movq	%rdx, 96(%rdi)
	movq	$0, 104(%rdi)
	jmp	.L236
	.p2align 4,,10
	.p2align 3
.L237:
	leaq	-104(%rbp), %rdx
	leaq	112(%rbx), %rdi
.LEHB10:
	call	_ZNSt6vectorISt10unique_ptrIN2v88internal6torque10DeclarableESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
.LEHE10:
	jmp	.L238
.L283:
	call	__stack_chk_fail@PLT
.L262:
	endbr64
	movq	%rax, %r12
	jmp	.L242
.L260:
	endbr64
	movq	%rax, %r12
	jmp	.L247
.L259:
	endbr64
	movq	%rax, %r12
	jmp	.L248
.L261:
	endbr64
	movq	%rax, %r12
	jmp	.L246
	.section	.gcc_except_table._ZN2v88internal6torque13GlobalContextC2ENS1_3AstE,"a",@progbits
.LLSDA6489:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6489-.LLSDACSB6489
.LLSDACSB6489:
	.uleb128 .LEHB7-.LFB6489
	.uleb128 .LEHE7-.LEHB7
	.uleb128 .L259-.LFB6489
	.uleb128 0
	.uleb128 .LEHB8-.LFB6489
	.uleb128 .LEHE8-.LEHB8
	.uleb128 .L260-.LFB6489
	.uleb128 0
	.uleb128 .LEHB9-.LFB6489
	.uleb128 .LEHE9-.LEHB9
	.uleb128 .L261-.LFB6489
	.uleb128 0
	.uleb128 .LEHB10-.LFB6489
	.uleb128 .LEHE10-.LEHB10
	.uleb128 .L262-.LFB6489
	.uleb128 0
.LLSDACSE6489:
	.section	.text._ZN2v88internal6torque13GlobalContextC2ENS1_3AstE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque13GlobalContextC2ENS1_3AstE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6489
	.type	_ZN2v88internal6torque13GlobalContextC2ENS1_3AstE.cold, @function
_ZN2v88internal6torque13GlobalContextC2ENS1_3AstE.cold:
.LFSB6489:
.L242:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L243
	movq	(%rdi), %rax
	call	*8(%rax)
.L243:
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L246
	movq	(%rdi), %rax
	call	*8(%rax)
.L246:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
	movq	-56(%rbp), %rdx
	movq	%rdx, (%rax)
.L247:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
	movq	-88(%rbp), %rdx
	movq	%rdx, (%rax)
.L248:
	movq	208(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L249
	call	_ZdlPv@PLT
.L249:
	movq	176(%rbx), %rsi
	leaq	160(%rbx), %rdi
	call	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_NS2_13GlobalContext14PerFileStreamsEESt10_Select1stIS8_ESt4lessIS3_ESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E
	movq	144(%rbx), %r14
	movq	136(%rbx), %r13
.L252:
	cmpq	%r13, %r14
	jne	.L284
	movq	136(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L253
	call	_ZdlPv@PLT
.L253:
	movq	120(%rbx), %r14
	movq	112(%rbx), %r13
.L256:
	cmpq	%r13, %r14
	jne	.L285
	movq	112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L257
	call	_ZdlPv@PLT
.L257:
	leaq	16(%rbx), %rdi
	call	_ZN2v88internal6torque3AstD1Ev
	movq	%r12, %rdi
.LEHB11:
	call	_Unwind_Resume@PLT
.LEHE11:
.L284:
	movq	0(%r13), %rdi
	leaq	16(%r13), %rax
	cmpq	%rax, %rdi
	je	.L251
	call	_ZdlPv@PLT
.L251:
	addq	$32, %r13
	jmp	.L252
.L285:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L255
	movq	(%rdi), %rax
	call	*8(%rax)
.L255:
	addq	$8, %r13
	jmp	.L256
	.cfi_endproc
.LFE6489:
	.section	.gcc_except_table._ZN2v88internal6torque13GlobalContextC2ENS1_3AstE
.LLSDAC6489:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6489-.LLSDACSBC6489
.LLSDACSBC6489:
	.uleb128 .LEHB11-.LCOLDB6
	.uleb128 .LEHE11-.LEHB11
	.uleb128 0
	.uleb128 0
.LLSDACSEC6489:
	.section	.text.unlikely._ZN2v88internal6torque13GlobalContextC2ENS1_3AstE
	.section	.text._ZN2v88internal6torque13GlobalContextC2ENS1_3AstE
	.size	_ZN2v88internal6torque13GlobalContextC2ENS1_3AstE, .-_ZN2v88internal6torque13GlobalContextC2ENS1_3AstE
	.section	.text.unlikely._ZN2v88internal6torque13GlobalContextC2ENS1_3AstE
	.size	_ZN2v88internal6torque13GlobalContextC2ENS1_3AstE.cold, .-_ZN2v88internal6torque13GlobalContextC2ENS1_3AstE.cold
.LCOLDE6:
	.section	.text._ZN2v88internal6torque13GlobalContextC2ENS1_3AstE
.LHOTE6:
	.globl	_ZN2v88internal6torque13GlobalContextC1ENS1_3AstE
	.set	_ZN2v88internal6torque13GlobalContextC1ENS1_3AstE,_ZN2v88internal6torque13GlobalContextC2ENS1_3AstE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal6torque21ContextualVariableTopINS1_13GlobalContextEEERPNT_5ScopeEv,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal6torque21ContextualVariableTopINS1_13GlobalContextEEERPNT_5ScopeEv, @function
_GLOBAL__sub_I__ZN2v88internal6torque21ContextualVariableTopINS1_13GlobalContextEEERPNT_5ScopeEv:
.LFB10468:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE10468:
	.size	_GLOBAL__sub_I__ZN2v88internal6torque21ContextualVariableTopINS1_13GlobalContextEEERPNT_5ScopeEv, .-_GLOBAL__sub_I__ZN2v88internal6torque21ContextualVariableTopINS1_13GlobalContextEEERPNT_5ScopeEv
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal6torque21ContextualVariableTopINS1_13GlobalContextEEERPNT_5ScopeEv
	.weak	_ZTVN2v88internal6torque10DeclarableE
	.section	.data.rel.ro.local._ZTVN2v88internal6torque10DeclarableE,"awG",@progbits,_ZTVN2v88internal6torque10DeclarableE,comdat
	.align 8
	.type	_ZTVN2v88internal6torque10DeclarableE, @object
	.size	_ZTVN2v88internal6torque10DeclarableE, 40
_ZTVN2v88internal6torque10DeclarableE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal6torque10DeclarableD1Ev
	.quad	_ZN2v88internal6torque10DeclarableD0Ev
	.quad	_ZNK2v88internal6torque10Declarable9type_nameEv
	.weak	_ZTVN2v88internal6torque5ScopeE
	.section	.data.rel.ro.local._ZTVN2v88internal6torque5ScopeE,"awG",@progbits,_ZTVN2v88internal6torque5ScopeE,comdat
	.align 8
	.type	_ZTVN2v88internal6torque5ScopeE, @object
	.size	_ZTVN2v88internal6torque5ScopeE, 40
_ZTVN2v88internal6torque5ScopeE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal6torque5ScopeD1Ev
	.quad	_ZN2v88internal6torque5ScopeD0Ev
	.quad	_ZNK2v88internal6torque5Scope9type_nameEv
	.weak	_ZTVN2v88internal6torque9NamespaceE
	.section	.data.rel.ro.local._ZTVN2v88internal6torque9NamespaceE,"awG",@progbits,_ZTVN2v88internal6torque9NamespaceE,comdat
	.align 8
	.type	_ZTVN2v88internal6torque9NamespaceE, @object
	.size	_ZTVN2v88internal6torque9NamespaceE, 40
_ZTVN2v88internal6torque9NamespaceE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal6torque9NamespaceD1Ev
	.quad	_ZN2v88internal6torque9NamespaceD0Ev
	.quad	_ZNK2v88internal6torque9Namespace9type_nameEv
	.section	.tbss._ZZN2v88internal6torque21ContextualVariableTopINS1_18TargetArchitectureEEERPNT_5ScopeEvE3top,"awT",@nobits
	.align 8
	.type	_ZZN2v88internal6torque21ContextualVariableTopINS1_18TargetArchitectureEEERPNT_5ScopeEvE3top, @object
	.size	_ZZN2v88internal6torque21ContextualVariableTopINS1_18TargetArchitectureEEERPNT_5ScopeEvE3top, 8
_ZZN2v88internal6torque21ContextualVariableTopINS1_18TargetArchitectureEEERPNT_5ScopeEvE3top:
	.zero	8
	.section	.tbss._ZZN2v88internal6torque21ContextualVariableTopINS1_13GlobalContextEEERPNT_5ScopeEvE3top,"awT",@nobits
	.align 8
	.type	_ZZN2v88internal6torque21ContextualVariableTopINS1_13GlobalContextEEERPNT_5ScopeEvE3top, @object
	.size	_ZZN2v88internal6torque21ContextualVariableTopINS1_13GlobalContextEEERPNT_5ScopeEvE3top, 8
_ZZN2v88internal6torque21ContextualVariableTopINS1_13GlobalContextEEERPNT_5ScopeEvE3top:
	.zero	8
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC7:
	.string	"base"
	.section	.data.rel.ro.local._ZN2v88internal6torqueL18kBaseNamespaceNameE,"aw"
	.align 8
	.type	_ZN2v88internal6torqueL18kBaseNamespaceNameE, @object
	.size	_ZN2v88internal6torqueL18kBaseNamespaceNameE, 8
_ZN2v88internal6torqueL18kBaseNamespaceNameE:
	.quad	.LC7
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	DW.ref.__gxx_personality_v0
	.weak	DW.ref.__gxx_personality_v0
	.section	.data.rel.local.DW.ref.__gxx_personality_v0,"awG",@progbits,DW.ref.__gxx_personality_v0,comdat
	.align 8
	.type	DW.ref.__gxx_personality_v0, @object
	.size	DW.ref.__gxx_personality_v0, 8
DW.ref.__gxx_personality_v0:
	.quad	__gxx_personality_v0
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
