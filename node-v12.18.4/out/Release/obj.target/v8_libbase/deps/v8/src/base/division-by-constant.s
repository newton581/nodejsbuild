	.file	"division-by-constant.cc"
	.text
	.section	.text._ZN2v84base23MagicNumbersForDivisionIjEC2Ejjb,"axG",@progbits,_ZN2v84base23MagicNumbersForDivisionIjEC5Ejjb,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v84base23MagicNumbersForDivisionIjEC2Ejjb
	.type	_ZN2v84base23MagicNumbersForDivisionIjEC2Ejjb, @function
_ZN2v84base23MagicNumbersForDivisionIjEC2Ejjb:
.LFB3000:
	.cfi_startproc
	endbr64
	movl	%esi, (%rdi)
	movl	%edx, 4(%rdi)
	movb	%cl, 8(%rdi)
	ret
	.cfi_endproc
.LFE3000:
	.size	_ZN2v84base23MagicNumbersForDivisionIjEC2Ejjb, .-_ZN2v84base23MagicNumbersForDivisionIjEC2Ejjb
	.weak	_ZN2v84base23MagicNumbersForDivisionIjEC1Ejjb
	.set	_ZN2v84base23MagicNumbersForDivisionIjEC1Ejjb,_ZN2v84base23MagicNumbersForDivisionIjEC2Ejjb
	.section	.text._ZNK2v84base23MagicNumbersForDivisionIjEeqERKS2_,"axG",@progbits,_ZNK2v84base23MagicNumbersForDivisionIjEeqERKS2_,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v84base23MagicNumbersForDivisionIjEeqERKS2_
	.type	_ZNK2v84base23MagicNumbersForDivisionIjEeqERKS2_, @function
_ZNK2v84base23MagicNumbersForDivisionIjEeqERKS2_:
.LFB3002:
	.cfi_startproc
	endbr64
	movl	(%rsi), %edx
	xorl	%eax, %eax
	cmpl	%edx, (%rdi)
	je	.L7
.L3:
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	movl	4(%rsi), %ecx
	cmpl	%ecx, 4(%rdi)
	jne	.L3
	movzbl	8(%rsi), %eax
	cmpb	%al, 8(%rdi)
	sete	%al
	ret
	.cfi_endproc
.LFE3002:
	.size	_ZNK2v84base23MagicNumbersForDivisionIjEeqERKS2_, .-_ZNK2v84base23MagicNumbersForDivisionIjEeqERKS2_
	.section	.text._ZN2v84base23MagicNumbersForDivisionImEC2Emjb,"axG",@progbits,_ZN2v84base23MagicNumbersForDivisionImEC5Emjb,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v84base23MagicNumbersForDivisionImEC2Emjb
	.type	_ZN2v84base23MagicNumbersForDivisionImEC2Emjb, @function
_ZN2v84base23MagicNumbersForDivisionImEC2Emjb:
.LFB3004:
	.cfi_startproc
	endbr64
	movq	%rsi, (%rdi)
	movl	%edx, 8(%rdi)
	movb	%cl, 12(%rdi)
	ret
	.cfi_endproc
.LFE3004:
	.size	_ZN2v84base23MagicNumbersForDivisionImEC2Emjb, .-_ZN2v84base23MagicNumbersForDivisionImEC2Emjb
	.weak	_ZN2v84base23MagicNumbersForDivisionImEC1Emjb
	.set	_ZN2v84base23MagicNumbersForDivisionImEC1Emjb,_ZN2v84base23MagicNumbersForDivisionImEC2Emjb
	.section	.text._ZNK2v84base23MagicNumbersForDivisionImEeqERKS2_,"axG",@progbits,_ZNK2v84base23MagicNumbersForDivisionImEeqERKS2_,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v84base23MagicNumbersForDivisionImEeqERKS2_
	.type	_ZNK2v84base23MagicNumbersForDivisionImEeqERKS2_, @function
_ZNK2v84base23MagicNumbersForDivisionImEeqERKS2_:
.LFB3006:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rcx
	xorl	%eax, %eax
	cmpq	%rcx, (%rdi)
	je	.L12
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	movabsq	$1099511627775, %rdx
	movq	8(%rdi), %rax
	xorq	8(%rsi), %rax
	testq	%rdx, %rax
	sete	%al
	ret
	.cfi_endproc
.LFE3006:
	.size	_ZNK2v84base23MagicNumbersForDivisionImEeqERKS2_, .-_ZNK2v84base23MagicNumbersForDivisionImEeqERKS2_
	.section	.text._ZN2v84base24SignedDivisionByConstantIjEENS0_23MagicNumbersForDivisionIT_EES3_,"axG",@progbits,_ZN2v84base24SignedDivisionByConstantIjEENS0_23MagicNumbersForDivisionIT_EES3_,comdat
	.p2align 4
	.weak	_ZN2v84base24SignedDivisionByConstantIjEENS0_23MagicNumbersForDivisionIT_EES3_
	.type	_ZN2v84base24SignedDivisionByConstantIjEENS0_23MagicNumbersForDivisionIT_EES3_, @function
_ZN2v84base24SignedDivisionByConstantIjEENS0_23MagicNumbersForDivisionIT_EES3_:
.LFB3007:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edi, %eax
	xorl	%edx, %edx
	movl	$-2147483648, %esi
	sarl	$31, %eax
	movl	%eax, %r9d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	xorl	%edi, %r9d
	pushq	%r12
	subl	%eax, %r9d
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movl	%edi, %ebx
	shrl	$31, %ebx
	leal	-2147483648(%rbx), %eax
	addl	$2147483647, %ebx
	divl	%r9d
	movl	%ebx, %r11d
	movl	%esi, %eax
	subl	%edx, %r11d
	movl	%edx, %r12d
	xorl	%edx, %edx
	divl	%r11d
	movl	%edx, %ecx
	movl	%eax, %r8d
	xorl	%edx, %edx
	movl	%esi, %eax
	divl	%r9d
	movl	$31, %esi
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L23:
	xorl	%ecx, %ecx
.L18:
	movl	%r13d, %esi
.L15:
	addl	%ecx, %ecx
	leal	1(%rsi), %r13d
	addl	%r8d, %r8d
	cmpl	%ecx, %r11d
	ja	.L16
	subl	%ebx, %ecx
	addl	$1, %r8d
	addl	%r12d, %ecx
.L16:
	addl	%edx, %edx
	addl	%eax, %eax
	cmpl	%edx, %r9d
	ja	.L17
	addl	$1, %eax
	subl	%r9d, %edx
.L17:
	movl	%r9d, %r10d
	subl	%edx, %r10d
	cmpl	%r10d, %r8d
	jb	.L18
	jne	.L24
	testl	%ecx, %ecx
	je	.L23
.L24:
	leal	1(%rax), %edx
	subl	$31, %esi
	notl	%eax
	testl	%edi, %edi
	cmovns	%edx, %eax
	movl	%esi, -40(%rbp)
	movb	$0, -36(%rbp)
	movl	-36(%rbp), %edx
	movl	%eax, -44(%rbp)
	movq	-44(%rbp), %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3007:
	.size	_ZN2v84base24SignedDivisionByConstantIjEENS0_23MagicNumbersForDivisionIT_EES3_, .-_ZN2v84base24SignedDivisionByConstantIjEENS0_23MagicNumbersForDivisionIT_EES3_
	.section	.text._ZN2v84base24SignedDivisionByConstantImEENS0_23MagicNumbersForDivisionIT_EES3_,"axG",@progbits,_ZN2v84base24SignedDivisionByConstantImEENS0_23MagicNumbersForDivisionIT_EES3_,comdat
	.p2align 4
	.weak	_ZN2v84base24SignedDivisionByConstantImEENS0_23MagicNumbersForDivisionIT_EES3_
	.type	_ZN2v84base24SignedDivisionByConstantImEENS0_23MagicNumbersForDivisionIT_EES3_, @function
_ZN2v84base24SignedDivisionByConstantImEENS0_23MagicNumbersForDivisionIT_EES3_:
.LFB3008:
	.cfi_startproc
	endbr64
	movabsq	$-9223372036854775808, %rsi
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rax
	xorl	%edx, %edx
	sarq	$63, %rax
	movq	%rax, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	xorq	%rdi, %r9
	pushq	%r13
	pushq	%r12
	subq	%rax, %r9
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	shrq	$63, %rdi
	leaq	(%rdi,%rsi), %rax
	divq	%r9
	movabsq	$9223372036854775807, %rax
	addq	%rax, %rdi
	movq	%rsi, %rax
	movq	%rdi, %r11
	subq	%rdx, %r11
	movq	%rdx, %r12
	xorl	%edx, %edx
	divq	%r11
	movq	%rdx, %rcx
	movq	%rax, %r8
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%r9
	movl	$63, %esi
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L36:
	xorl	%ecx, %ecx
.L31:
	movl	%r13d, %esi
.L28:
	addq	%rcx, %rcx
	leal	1(%rsi), %r13d
	addq	%r8, %r8
	cmpq	%rcx, %r11
	ja	.L29
	subq	%rdi, %rcx
	addq	$1, %r8
	addq	%r12, %rcx
.L29:
	addq	%rdx, %rdx
	addq	%rax, %rax
	cmpq	%rdx, %r9
	ja	.L30
	addq	$1, %rax
	subq	%r9, %rdx
.L30:
	movq	%r9, %r10
	subq	%rdx, %r10
	cmpq	%r10, %r8
	jb	.L31
	jne	.L37
	testq	%rcx, %rcx
	je	.L36
.L37:
	leaq	1(%rax), %rdx
	testq	%rbx, %rbx
	notq	%rax
	popq	%rbx
	cmovns	%rdx, %rax
	popq	%r12
	leal	-63(%rsi), %edx
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3008:
	.size	_ZN2v84base24SignedDivisionByConstantImEENS0_23MagicNumbersForDivisionIT_EES3_, .-_ZN2v84base24SignedDivisionByConstantImEENS0_23MagicNumbersForDivisionIT_EES3_
	.section	.text._ZN2v84base26UnsignedDivisionByConstantIjEENS0_23MagicNumbersForDivisionIT_EES3_j,"axG",@progbits,_ZN2v84base26UnsignedDivisionByConstantIjEENS0_23MagicNumbersForDivisionIT_EES3_j,comdat
	.p2align 4
	.weak	_ZN2v84base26UnsignedDivisionByConstantIjEENS0_23MagicNumbersForDivisionIT_EES3_j
	.type	_ZN2v84base26UnsignedDivisionByConstantIjEENS0_23MagicNumbersForDivisionIT_EES3_j, @function
_ZN2v84base26UnsignedDivisionByConstantIjEENS0_23MagicNumbersForDivisionIT_EES3_j:
.LFB3009:
	.cfi_startproc
	endbr64
	movl	%esi, %ecx
	movl	$-1, %r8d
	xorl	%edx, %edx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	shrl	%cl, %r8d
	movl	$31, %r10d
	xorl	%r11d, %r11d
	movl	%r8d, %eax
	subl	%edi, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	divl	%edi
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%r8d, %ebx
	movl	$-2147483648, %eax
	movl	$1, %r14d
	movl	$1, %r12d
	leal	-1(%rdi), %r13d
	subl	%edi, %r14d
	movl	%edx, %r9d
	subl	%edx, %ebx
	xorl	%edx, %edx
	divl	%ebx
	subl	%r8d, %r9d
	movl	%r9d, -84(%rbp)
	movl	%edx, %ecx
	movl	%eax, %esi
	xorl	%edx, %edx
	movl	$2147483647, %eax
	divl	%edi
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L53:
	cmpl	$2147483647, %eax
	leal	(%r14,%rdx,2), %edx
	leal	1(%rax,%rax), %eax
	movl	%r13d, %ecx
	cmovnb	%r12d, %r11d
	subl	%edx, %ecx
	cmpl	$64, %r9d
	je	.L49
.L54:
	cmpl	%ecx, %esi
	jb	.L47
	jne	.L51
	testl	%r8d, %r8d
	jne	.L51
	xorl	%r8d, %r8d
.L47:
	movl	%r8d, %ecx
	movl	%r9d, %r10d
.L40:
	movl	%ebx, %r15d
	leal	1(%r10), %r9d
	leal	(%rcx,%rcx), %r8d
	addl	%esi, %esi
	subl	%ecx, %r15d
	cmpl	%ecx, %r15d
	ja	.L41
	addl	$1, %esi
	addl	-84(%rbp), %r8d
.L41:
	movl	%edi, %ecx
	leal	1(%rdx), %r15d
	subl	%edx, %ecx
	cmpl	%ecx, %r15d
	jnb	.L53
	testl	%eax, %eax
	leal	1(%rdx,%rdx), %edx
	movl	%r13d, %ecx
	cmovs	%r12d, %r11d
	addl	%eax, %eax
	subl	%edx, %ecx
	cmpl	$64, %r9d
	jne	.L54
.L49:
	movl	$32, %r10d
.L46:
	addl	$1, %eax
	movl	%r10d, -56(%rbp)
	movl	%eax, -60(%rbp)
	movq	-60(%rbp), %rax
	movb	%r11b, -52(%rbp)
	movl	-52(%rbp), %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L51:
	.cfi_restore_state
	subl	$31, %r10d
	jmp	.L46
	.cfi_endproc
.LFE3009:
	.size	_ZN2v84base26UnsignedDivisionByConstantIjEENS0_23MagicNumbersForDivisionIT_EES3_j, .-_ZN2v84base26UnsignedDivisionByConstantIjEENS0_23MagicNumbersForDivisionIT_EES3_j
	.section	.text._ZN2v84base26UnsignedDivisionByConstantImEENS0_23MagicNumbersForDivisionIT_EES3_j,"axG",@progbits,_ZN2v84base26UnsignedDivisionByConstantImEENS0_23MagicNumbersForDivisionIT_EES3_j,comdat
	.p2align 4
	.weak	_ZN2v84base26UnsignedDivisionByConstantImEENS0_23MagicNumbersForDivisionIT_EES3_j
	.type	_ZN2v84base26UnsignedDivisionByConstantImEENS0_23MagicNumbersForDivisionIT_EES3_j, @function
_ZN2v84base26UnsignedDivisionByConstantImEENS0_23MagicNumbersForDivisionIT_EES3_j:
.LFB3010:
	.cfi_startproc
	endbr64
	movl	%esi, %ecx
	movq	$-1, %r8
	xorl	%edx, %edx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	shrq	%cl, %r8
	movl	$63, %r10d
	xorl	%r11d, %r11d
	movq	%r8, %rax
	subq	%rdi, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	divq	%rdi
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%r8, %rbx
	movl	$1, %r14d
	leaq	-1(%rdi), %r13
	movabsq	$-9223372036854775808, %rax
	movl	$1, %r12d
	subq	%rdi, %r14
	movq	%rdx, %r9
	subq	%rdx, %rbx
	xorl	%edx, %edx
	divq	%rbx
	subq	%r8, %r9
	movq	%r9, -48(%rbp)
	movq	%rdx, %rcx
	movq	%rax, %rsi
	xorl	%edx, %edx
	movabsq	$9223372036854775807, %rax
	divq	%rdi
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L69:
	movabsq	$9223372036854775806, %rcx
	leaq	(%r14,%rdx,2), %rdx
	cmpq	%rcx, %rax
	movq	%r13, %rcx
	leaq	1(%rax,%rax), %rax
	cmova	%r12d, %r11d
	subq	%rdx, %rcx
	cmpl	$128, %r9d
	je	.L65
.L70:
	cmpq	%rcx, %rsi
	jb	.L63
	jne	.L67
	testq	%r8, %r8
	jne	.L67
	xorl	%r8d, %r8d
.L63:
	movq	%r8, %rcx
	movl	%r9d, %r10d
.L56:
	movq	%rbx, %r15
	leal	1(%r10), %r9d
	leaq	(%rcx,%rcx), %r8
	addq	%rsi, %rsi
	subq	%rcx, %r15
	cmpq	%rcx, %r15
	ja	.L57
	addq	$1, %rsi
	addq	-48(%rbp), %r8
.L57:
	movq	%rdi, %rcx
	leaq	1(%rdx), %r15
	subq	%rdx, %rcx
	cmpq	%rcx, %r15
	jnb	.L69
	testq	%rax, %rax
	leaq	1(%rdx,%rdx), %rdx
	movq	%r13, %rcx
	cmovs	%r12d, %r11d
	addq	%rax, %rax
	subq	%rdx, %rcx
	cmpl	$128, %r9d
	jne	.L70
.L65:
	movl	$64, %r10d
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L67:
	subl	$63, %r10d
.L62:
	movzbl	%r11b, %edx
	addq	$1, %rax
	salq	$32, %rdx
	movq	%rdx, %r11
	movl	%r10d, %edx
	popq	%rbx
	popq	%r12
	orq	%r11, %rdx
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3010:
	.size	_ZN2v84base26UnsignedDivisionByConstantImEENS0_23MagicNumbersForDivisionIT_EES3_j, .-_ZN2v84base26UnsignedDivisionByConstantImEENS0_23MagicNumbersForDivisionIT_EES3_j
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
