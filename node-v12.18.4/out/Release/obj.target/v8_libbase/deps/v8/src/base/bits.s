	.file	"bits.cc"
	.text
	.section	.text._ZN2v84base4bits21RoundUpToPowerOfTwo32Ej,"ax",@progbits
	.p2align 4
	.globl	_ZN2v84base4bits21RoundUpToPowerOfTwo32Ej
	.type	_ZN2v84base4bits21RoundUpToPowerOfTwo32Ej, @function
_ZN2v84base4bits21RoundUpToPowerOfTwo32Ej:
.LFB2784:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	testl	%edi, %edi
	je	.L1
	subl	$1, %edi
	je	.L1
	bsrl	%edi, %edi
	movl	$32, %ecx
	xorl	$31, %edi
	subl	%edi, %ecx
	sall	%cl, %eax
.L1:
	ret
	.cfi_endproc
.LFE2784:
	.size	_ZN2v84base4bits21RoundUpToPowerOfTwo32Ej, .-_ZN2v84base4bits21RoundUpToPowerOfTwo32Ej
	.section	.text._ZN2v84base4bits21RoundUpToPowerOfTwo64Em,"ax",@progbits
	.p2align 4
	.globl	_ZN2v84base4bits21RoundUpToPowerOfTwo64Em
	.type	_ZN2v84base4bits21RoundUpToPowerOfTwo64Em, @function
_ZN2v84base4bits21RoundUpToPowerOfTwo64Em:
.LFB2785:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	testq	%rdi, %rdi
	je	.L7
	subq	$1, %rdi
	je	.L7
	bsrq	%rdi, %rdi
	movl	$64, %ecx
	xorq	$63, %rdi
	subl	%edi, %ecx
	salq	%cl, %rax
.L7:
	ret
	.cfi_endproc
.LFE2785:
	.size	_ZN2v84base4bits21RoundUpToPowerOfTwo64Em, .-_ZN2v84base4bits21RoundUpToPowerOfTwo64Em
	.section	.text._ZN2v84base4bits15SignedMulHigh32Eii,"ax",@progbits
	.p2align 4
	.globl	_ZN2v84base4bits15SignedMulHigh32Eii
	.type	_ZN2v84base4bits15SignedMulHigh32Eii, @function
_ZN2v84base4bits15SignedMulHigh32Eii:
.LFB2786:
	.cfi_startproc
	endbr64
	movslq	%esi, %rax
	movslq	%edi, %rdi
	imulq	%rax, %rdi
	movq	%rdi, %rax
	shrq	$32, %rax
	ret
	.cfi_endproc
.LFE2786:
	.size	_ZN2v84base4bits15SignedMulHigh32Eii, .-_ZN2v84base4bits15SignedMulHigh32Eii
	.section	.text._ZN2v84base4bits21SignedMulHighAndAdd32Eiii,"ax",@progbits
	.p2align 4
	.globl	_ZN2v84base4bits21SignedMulHighAndAdd32Eiii
	.type	_ZN2v84base4bits21SignedMulHighAndAdd32Eiii, @function
_ZN2v84base4bits21SignedMulHighAndAdd32Eiii:
.LFB2787:
	.cfi_startproc
	endbr64
	movslq	%esi, %rsi
	movslq	%edi, %rdi
	imulq	%rsi, %rdi
	shrq	$32, %rdi
	leal	(%rdx,%rdi), %eax
	ret
	.cfi_endproc
.LFE2787:
	.size	_ZN2v84base4bits21SignedMulHighAndAdd32Eiii, .-_ZN2v84base4bits21SignedMulHighAndAdd32Eiii
	.section	.text._ZN2v84base4bits11SignedDiv32Eii,"ax",@progbits
	.p2align 4
	.globl	_ZN2v84base4bits11SignedDiv32Eii
	.type	_ZN2v84base4bits11SignedDiv32Eii, @function
_ZN2v84base4bits11SignedDiv32Eii:
.LFB2788:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testl	%esi, %esi
	je	.L14
	movl	%edi, %eax
	cmpl	$-1, %esi
	je	.L20
	cltd
	idivl	%esi
.L14:
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	negl	%eax
	movl	$-2147483648, %edx
	cmpl	$-2147483648, %edi
	cmove	%edx, %eax
	ret
	.cfi_endproc
.LFE2788:
	.size	_ZN2v84base4bits11SignedDiv32Eii, .-_ZN2v84base4bits11SignedDiv32Eii
	.section	.text._ZN2v84base4bits11SignedMod32Eii,"ax",@progbits
	.p2align 4
	.globl	_ZN2v84base4bits11SignedMod32Eii
	.type	_ZN2v84base4bits11SignedMod32Eii, @function
_ZN2v84base4bits11SignedMod32Eii:
.LFB2789:
	.cfi_startproc
	endbr64
	leal	1(%rsi), %edx
	movl	%edi, %eax
	xorl	%r8d, %r8d
	cmpl	$1, %edx
	jbe	.L21
	cltd
	idivl	%esi
	movl	%edx, %r8d
.L21:
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE2789:
	.size	_ZN2v84base4bits11SignedMod32Eii, .-_ZN2v84base4bits11SignedMod32Eii
	.section	.text._ZN2v84base4bits20SignedSaturatedAdd64Ell,"ax",@progbits
	.p2align 4
	.globl	_ZN2v84base4bits20SignedSaturatedAdd64Ell
	.type	_ZN2v84base4bits20SignedSaturatedAdd64Ell, @function
_ZN2v84base4bits20SignedSaturatedAdd64Ell:
.LFB2790:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	js	.L30
	movabsq	$9223372036854775807, %rax
	movq	%rax, %rdx
	subq	%rsi, %rdx
	cmpq	%rdi, %rdx
	jl	.L24
.L27:
	leaq	(%rsi,%rdi), %rax
.L24:
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	movabsq	$-9223372036854775808, %rax
	movq	%rax, %rdx
	subq	%rsi, %rdx
	cmpq	%rdi, %rdx
	jle	.L27
	ret
	.cfi_endproc
.LFE2790:
	.size	_ZN2v84base4bits20SignedSaturatedAdd64Ell, .-_ZN2v84base4bits20SignedSaturatedAdd64Ell
	.section	.text._ZN2v84base4bits20SignedSaturatedSub64Ell,"ax",@progbits
	.p2align 4
	.globl	_ZN2v84base4bits20SignedSaturatedSub64Ell
	.type	_ZN2v84base4bits20SignedSaturatedSub64Ell, @function
_ZN2v84base4bits20SignedSaturatedSub64Ell:
.LFB2791:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	jle	.L32
	movabsq	$-9223372036854775808, %rax
	leaq	(%rsi,%rax), %rdx
	cmpq	%rdi, %rdx
	jg	.L38
.L34:
	movq	%rdi, %rax
	subq	%rsi, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	movabsq	$9223372036854775807, %rax
	leaq	(%rsi,%rax), %rdx
	cmpq	%rdi, %rdx
	jge	.L34
	ret
	.p2align 4,,10
	.p2align 3
.L38:
	ret
	.cfi_endproc
.LFE2791:
	.size	_ZN2v84base4bits20SignedSaturatedSub64Ell, .-_ZN2v84base4bits20SignedSaturatedSub64Ell
	.section	.text._ZN2v84base4bits19SignedMulOverflow32EiiPi,"ax",@progbits
	.p2align 4
	.globl	_ZN2v84base4bits19SignedMulOverflow32EiiPi
	.type	_ZN2v84base4bits19SignedMulOverflow32EiiPi, @function
_ZN2v84base4bits19SignedMulOverflow32EiiPi:
.LFB2792:
	.cfi_startproc
	endbr64
	movslq	%esi, %r8
	movslq	%edi, %rsi
	movl	$2147483648, %edi
	movl	$4294967295, %eax
	imulq	%r8, %rsi
	movl	%esi, (%rdx)
	addq	%rdi, %rsi
	cmpq	%rax, %rsi
	seta	%al
	ret
	.cfi_endproc
.LFE2792:
	.size	_ZN2v84base4bits19SignedMulOverflow32EiiPi, .-_ZN2v84base4bits19SignedMulOverflow32EiiPi
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
