	.file	"file-utils.cc"
	.text
	.section	.text._ZN2v84base12RelativePathEPPcPKcS4_,"ax",@progbits
	.p2align 4
	.globl	_ZN2v84base12RelativePathEPPcPKcS4_
	.type	_ZN2v84base12RelativePathEPPcPKcS4_, @function
_ZN2v84base12RelativePathEPPcPKcS4_:
.LFB3429:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movq	%rsi, %rdi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	call	strlen@PLT
	subl	$1, %eax
	js	.L2
	movslq	%eax, %rbx
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L9:
	subq	$1, %rbx
	testl	%ebx, %ebx
	js	.L2
.L4:
	movsbl	(%r12,%rbx), %edi
	movl	%ebx, %r15d
	call	_ZN2v84base2OS20isDirectorySeparatorEc@PLT
	testb	%al, %al
	je	.L9
	movq	%r13, %rdi
	call	strlen@PLT
	movl	$1, %esi
	leal	2(%r15,%rax), %edi
	movq	%rax, %rbx
	movslq	%edi, %rdi
	call	calloc@PLT
	leal	1(%r15), %edx
	movq	%r12, %rsi
	movq	%rax, (%r14)
	movslq	%edx, %rdx
	movq	%rax, %rdi
	call	strncat@PLT
	movslq	%ebx, %rdx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	strncat@PLT
	movq	%rax, %r8
.L1:
	addq	$8, %rsp
	movq	%r8, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	.cfi_restore_state
	movq	%r13, %rdi
	call	strdup@PLT
	movq	%rax, (%r14)
	movq	%rax, %r8
	jmp	.L1
	.cfi_endproc
.LFE3429:
	.size	_ZN2v84base12RelativePathEPPcPKcS4_, .-_ZN2v84base12RelativePathEPPcPKcS4_
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
