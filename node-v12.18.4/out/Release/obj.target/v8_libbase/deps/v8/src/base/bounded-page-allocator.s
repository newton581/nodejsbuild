	.file	"bounded-page-allocator.cc"
	.text
	.section	.text._ZN2v84base20BoundedPageAllocator16AllocatePageSizeEv,"axG",@progbits,_ZN2v84base20BoundedPageAllocator16AllocatePageSizeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v84base20BoundedPageAllocator16AllocatePageSizeEv
	.type	_ZN2v84base20BoundedPageAllocator16AllocatePageSizeEv, @function
_ZN2v84base20BoundedPageAllocator16AllocatePageSizeEv:
.LFB4213:
	.cfi_startproc
	endbr64
	movq	48(%rdi), %rax
	ret
	.cfi_endproc
.LFE4213:
	.size	_ZN2v84base20BoundedPageAllocator16AllocatePageSizeEv, .-_ZN2v84base20BoundedPageAllocator16AllocatePageSizeEv
	.section	.text._ZN2v84base20BoundedPageAllocator14CommitPageSizeEv,"axG",@progbits,_ZN2v84base20BoundedPageAllocator14CommitPageSizeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v84base20BoundedPageAllocator14CommitPageSizeEv
	.type	_ZN2v84base20BoundedPageAllocator14CommitPageSizeEv, @function
_ZN2v84base20BoundedPageAllocator14CommitPageSizeEv:
.LFB4214:
	.cfi_startproc
	endbr64
	movq	56(%rdi), %rax
	ret
	.cfi_endproc
.LFE4214:
	.size	_ZN2v84base20BoundedPageAllocator14CommitPageSizeEv, .-_ZN2v84base20BoundedPageAllocator14CommitPageSizeEv
	.section	.text._ZN2v84base20BoundedPageAllocator17SetRandomMmapSeedEl,"axG",@progbits,_ZN2v84base20BoundedPageAllocator17SetRandomMmapSeedEl,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v84base20BoundedPageAllocator17SetRandomMmapSeedEl
	.type	_ZN2v84base20BoundedPageAllocator17SetRandomMmapSeedEl, @function
_ZN2v84base20BoundedPageAllocator17SetRandomMmapSeedEl:
.LFB4215:
	.cfi_startproc
	endbr64
	movq	64(%rdi), %rdi
	movq	(%rdi), %rax
	jmp	*32(%rax)
	.cfi_endproc
.LFE4215:
	.size	_ZN2v84base20BoundedPageAllocator17SetRandomMmapSeedEl, .-_ZN2v84base20BoundedPageAllocator17SetRandomMmapSeedEl
	.section	.text._ZN2v84base20BoundedPageAllocator17GetRandomMmapAddrEv,"axG",@progbits,_ZN2v84base20BoundedPageAllocator17GetRandomMmapAddrEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v84base20BoundedPageAllocator17GetRandomMmapAddrEv
	.type	_ZN2v84base20BoundedPageAllocator17GetRandomMmapAddrEv, @function
_ZN2v84base20BoundedPageAllocator17GetRandomMmapAddrEv:
.LFB4216:
	.cfi_startproc
	endbr64
	movq	64(%rdi), %rdi
	movq	(%rdi), %rax
	jmp	*40(%rax)
	.cfi_endproc
.LFE4216:
	.size	_ZN2v84base20BoundedPageAllocator17GetRandomMmapAddrEv, .-_ZN2v84base20BoundedPageAllocator17GetRandomMmapAddrEv
	.section	.text._ZN2v84base20BoundedPageAllocator14SetPermissionsEPvmNS_13PageAllocator10PermissionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base20BoundedPageAllocator14SetPermissionsEPvmNS_13PageAllocator10PermissionE
	.type	_ZN2v84base20BoundedPageAllocator14SetPermissionsEPvmNS_13PageAllocator10PermissionE, @function
_ZN2v84base20BoundedPageAllocator14SetPermissionsEPvmNS_13PageAllocator10PermissionE:
.LFB4233:
	.cfi_startproc
	endbr64
	movq	64(%rdi), %rdi
	movq	(%rdi), %rax
	jmp	*72(%rax)
	.cfi_endproc
.LFE4233:
	.size	_ZN2v84base20BoundedPageAllocator14SetPermissionsEPvmNS_13PageAllocator10PermissionE, .-_ZN2v84base20BoundedPageAllocator14SetPermissionsEPvmNS_13PageAllocator10PermissionE
	.section	.text._ZN2v84base20BoundedPageAllocator18DiscardSystemPagesEPvm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base20BoundedPageAllocator18DiscardSystemPagesEPvm
	.type	_ZN2v84base20BoundedPageAllocator18DiscardSystemPagesEPvm, @function
_ZN2v84base20BoundedPageAllocator18DiscardSystemPagesEPvm:
.LFB4234:
	.cfi_startproc
	endbr64
	movq	64(%rdi), %rdi
	movq	(%rdi), %rax
	jmp	*80(%rax)
	.cfi_endproc
.LFE4234:
	.size	_ZN2v84base20BoundedPageAllocator18DiscardSystemPagesEPvm, .-_ZN2v84base20BoundedPageAllocator18DiscardSystemPagesEPvm
	.section	.text._ZN2v84base20BoundedPageAllocatorD2Ev,"axG",@progbits,_ZN2v84base20BoundedPageAllocatorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v84base20BoundedPageAllocatorD2Ev
	.type	_ZN2v84base20BoundedPageAllocatorD2Ev, @function
_ZN2v84base20BoundedPageAllocatorD2Ev:
.LFB4888:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v84base20BoundedPageAllocatorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	72(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -72(%rdi)
	call	_ZN2v84base15RegionAllocatorD1Ev@PLT
	addq	$8, %rsp
	leaq	8(%rbx), %rdi
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base5MutexD1Ev@PLT
	.cfi_endproc
.LFE4888:
	.size	_ZN2v84base20BoundedPageAllocatorD2Ev, .-_ZN2v84base20BoundedPageAllocatorD2Ev
	.weak	_ZN2v84base20BoundedPageAllocatorD1Ev
	.set	_ZN2v84base20BoundedPageAllocatorD1Ev,_ZN2v84base20BoundedPageAllocatorD2Ev
	.section	.text._ZN2v84base20BoundedPageAllocatorD0Ev,"axG",@progbits,_ZN2v84base20BoundedPageAllocatorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v84base20BoundedPageAllocatorD0Ev
	.type	_ZN2v84base20BoundedPageAllocatorD0Ev, @function
_ZN2v84base20BoundedPageAllocatorD0Ev:
.LFB4890:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v84base20BoundedPageAllocatorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	72(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -72(%rdi)
	call	_ZN2v84base15RegionAllocatorD1Ev@PLT
	leaq	8(%r12), %rdi
	call	_ZN2v84base5MutexD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$224, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE4890:
	.size	_ZN2v84base20BoundedPageAllocatorD0Ev, .-_ZN2v84base20BoundedPageAllocatorD0Ev
	.section	.rodata._ZN2v84base20BoundedPageAllocator12ReleasePagesEPvmm.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"IsAligned(address, allocate_page_size_)"
	.section	.rodata._ZN2v84base20BoundedPageAllocator12ReleasePagesEPvmm.str1.1,"aMS",@progbits,1
.LC1:
	.string	"Check failed: %s."
	.section	.text._ZN2v84base20BoundedPageAllocator12ReleasePagesEPvmm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base20BoundedPageAllocator12ReleasePagesEPvmm
	.type	_ZN2v84base20BoundedPageAllocator12ReleasePagesEPvmm, @function
_ZN2v84base20BoundedPageAllocator12ReleasePagesEPvmm:
.LFB4232:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	48(%rdi), %rax
	leaq	-1(%rax), %rdx
	testq	%rdx, %rsi
	jne	.L20
	movq	%rax, %rdx
	leaq	-1(%rax,%rcx), %r15
	leaq	-1(%rax,%r12), %rax
	movq	%rdi, %r13
	negq	%rdx
	movq	%rsi, %r14
	movq	%rcx, %rbx
	andq	%rdx, %r15
	andq	%rdx, %rax
	cmpq	%rax, %r15
	jb	.L21
.L14:
	movq	64(%r13), %rdi
	movq	%r12, %rdx
	leaq	_ZN2v84base20BoundedPageAllocator14SetPermissionsEPvmNS_13PageAllocator10PermissionE(%rip), %rcx
	leaq	(%r14,%rbx), %rsi
	subq	%rbx, %rdx
	movq	(%rdi), %rax
	movq	72(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L15
	movq	64(%rdi), %rdi
	movq	(%rdi), %rcx
	movq	72(%rcx), %r8
	cmpq	%rax, %r8
	jne	.L16
	movq	64(%rdi), %rdi
	movq	(%rdi), %rax
	movq	72(%rax), %rax
	cmpq	%r8, %rax
	jne	.L15
	movq	64(%rdi), %rdi
	movq	(%rdi), %rcx
	movq	72(%rcx), %r8
	cmpq	%rax, %r8
	jne	.L16
	movq	64(%rdi), %rdi
	movq	(%rdi), %rax
	movq	72(%rax), %rax
.L15:
	addq	$24, %rsp
	xorl	%ecx, %ecx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L21:
	.cfi_restore_state
	leaq	8(%rdi), %r8
	movq	%r8, %rdi
	movq	%r8, -56(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	leaq	72(%r13), %rdi
	movq	%r15, %rdx
	movq	%r14, %rsi
	call	_ZN2v84base15RegionAllocator10TrimRegionEmm@PLT
	movq	-56(%rbp), %r8
	movq	%r8, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L16:
	addq	$24, %rsp
	xorl	%ecx, %ecx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%r8
	.p2align 4,,10
	.p2align 3
.L20:
	.cfi_restore_state
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE4232:
	.size	_ZN2v84base20BoundedPageAllocator12ReleasePagesEPvmm, .-_ZN2v84base20BoundedPageAllocator12ReleasePagesEPvmm
	.section	.rodata._ZN2v84base20BoundedPageAllocator13AllocatePagesEPvmmNS_13PageAllocator10PermissionE.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"IsAligned(alignment, region_allocator_.page_size())"
	.align 8
.LC3:
	.string	"alignment <= allocate_page_size_"
	.align 8
.LC4:
	.string	"page_allocator_->SetPermissions(reinterpret_cast<void*>(address), size, access)"
	.section	.text._ZN2v84base20BoundedPageAllocator13AllocatePagesEPvmmNS_13PageAllocator10PermissionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base20BoundedPageAllocator13AllocatePagesEPvmmNS_13PageAllocator10PermissionE
	.type	_ZN2v84base20BoundedPageAllocator13AllocatePagesEPvmmNS_13PageAllocator10PermissionE, @function
_ZN2v84base20BoundedPageAllocator13AllocatePagesEPvmmNS_13PageAllocator10PermissionE:
.LFB4229:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	8(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%r8d, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r15, %rdi
	subq	$8, %rsp
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	120(%rbx), %rax
	subq	$1, %rax
	testq	%r12, %rax
	jne	.L33
	cmpq	%r12, 48(%rbx)
	jb	.L34
	leaq	72(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v84base15RegionAllocator14AllocateRegionEm@PLT
	cmpq	$-1, %rax
	je	.L31
	movq	64(%rbx), %rdi
	movq	%rax, %r12
	leaq	_ZN2v84base20BoundedPageAllocator14SetPermissionsEPvmNS_13PageAllocator10PermissionE(%rip), %rax
	movq	(%rdi), %rdx
	movq	72(%rdx), %r8
	cmpq	%rax, %r8
	jne	.L26
	movq	64(%rdi), %rdi
	movq	(%rdi), %rax
	movq	72(%rax), %rax
	cmpq	%r8, %rax
	jne	.L27
	movq	64(%rdi), %rdi
	movq	(%rdi), %rdx
	movq	72(%rdx), %r8
	cmpq	%rax, %r8
	jne	.L26
	movq	64(%rdi), %rdi
	movq	(%rdi), %rax
	movq	72(%rax), %rax
	cmpq	%r8, %rax
	jne	.L27
	movq	64(%rdi), %rdi
	movl	%r14d, %ecx
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	(%rdi), %rax
	call	*72(%rax)
.L30:
	testb	%al, %al
	je	.L35
.L25:
	movq	%r15, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	.cfi_restore_state
	movl	%r14d, %ecx
	movq	%r13, %rdx
	movq	%r12, %rsi
	call	*%r8
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L27:
	movl	%r14d, %ecx
	movq	%r13, %rdx
	movq	%r12, %rsi
	call	*%rax
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L33:
	leaq	.LC2(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L34:
	leaq	.LC3(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L31:
	xorl	%r12d, %r12d
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L35:
	leaq	.LC4(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE4229:
	.size	_ZN2v84base20BoundedPageAllocator13AllocatePagesEPvmmNS_13PageAllocator10PermissionE, .-_ZN2v84base20BoundedPageAllocator13AllocatePagesEPvmmNS_13PageAllocator10PermissionE
	.section	.rodata._ZN2v84base20BoundedPageAllocator9FreePagesEPvm.str1.8,"aMS",@progbits,1
	.align 8
.LC5:
	.string	"page_allocator_->SetPermissions(raw_address, size, PageAllocator::kNoAccess)"
	.section	.text._ZN2v84base20BoundedPageAllocator9FreePagesEPvm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base20BoundedPageAllocator9FreePagesEPvm
	.type	_ZN2v84base20BoundedPageAllocator9FreePagesEPvm, @function
_ZN2v84base20BoundedPageAllocator9FreePagesEPvm:
.LFB4231:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	leaq	8(%rdi), %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%r14, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	xorl	%edx, %edx
	leaq	72(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v84base15RegionAllocator10TrimRegionEmm@PLT
	cmpq	%rax, %r13
	jne	.L43
	movq	64(%rbx), %rdi
	leaq	_ZN2v84base20BoundedPageAllocator14SetPermissionsEPvmNS_13PageAllocator10PermissionE(%rip), %rdx
	movq	(%rdi), %rax
	movq	72(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L38
	movq	64(%rdi), %rdi
	movq	(%rdi), %rdx
	movq	72(%rdx), %r8
	cmpq	%rax, %r8
	jne	.L39
	movq	64(%rdi), %rdi
	movq	(%rdi), %rax
	movq	72(%rax), %rax
	cmpq	%r8, %rax
	jne	.L38
	movq	64(%rdi), %rdi
	movq	(%rdi), %rdx
	movq	72(%rdx), %r8
	cmpq	%rax, %r8
	jne	.L39
	movq	64(%rdi), %rdi
	movq	%r12, %rsi
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movq	(%rdi), %rax
	call	*72(%rax)
	movl	%eax, %r12d
.L42:
	testb	%r12b, %r12b
	jne	.L37
	leaq	.LC5(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L43:
	xorl	%r12d, %r12d
.L37:
	movq	%r14, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	popq	%rbx
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L38:
	.cfi_restore_state
	movq	%r12, %rsi
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	call	*%rax
	movl	%eax, %r12d
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L39:
	movq	%r12, %rsi
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	call	*%r8
	movl	%eax, %r12d
	jmp	.L42
	.cfi_endproc
.LFE4231:
	.size	_ZN2v84base20BoundedPageAllocator9FreePagesEPvm, .-_ZN2v84base20BoundedPageAllocator9FreePagesEPvm
	.section	.rodata._ZN2v84base20BoundedPageAllocatorC2EPNS_13PageAllocatorEmmm.str1.8,"aMS",@progbits,1
	.align 8
.LC6:
	.string	"IsAligned(allocate_page_size, page_allocator->AllocatePageSize())"
	.align 8
.LC7:
	.string	"IsAligned(allocate_page_size_, commit_page_size_)"
	.section	.text._ZN2v84base20BoundedPageAllocatorC2EPNS_13PageAllocatorEmmm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base20BoundedPageAllocatorC2EPNS_13PageAllocatorEmmm
	.type	_ZN2v84base20BoundedPageAllocatorC2EPNS_13PageAllocatorEmmm, @function
_ZN2v84base20BoundedPageAllocatorC2EPNS_13PageAllocatorEmmm:
.LFB4225:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v84base20BoundedPageAllocatorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$8, %rdi
	subq	$8, %rsp
	movq	%rax, -8(%rdi)
	call	_ZN2v84base5MutexC1Ev@PLT
	movq	(%r12), %rax
	movq	%r13, 48(%rbx)
	leaq	_ZN2v84base20BoundedPageAllocator14CommitPageSizeEv(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L46
	movq	56(%r12), %rax
	movq	%r13, %rcx
.L47:
	movq	%rax, 56(%rbx)
	movq	%r15, %rdx
	leaq	72(%rbx), %rdi
	movq	%r14, %rsi
	movq	%r12, 64(%rbx)
	call	_ZN2v84base15RegionAllocatorC1Emmm@PLT
	movq	(%r12), %rax
	leaq	_ZN2v84base20BoundedPageAllocator16AllocatePageSizeEv(%rip), %rdx
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L48
	movq	48(%r12), %rax
	subq	$1, %rax
	testq	%r13, %rax
	jne	.L53
.L50:
	movq	56(%rbx), %rax
	subq	$1, %rax
	testq	%rax, 48(%rbx)
	jne	.L54
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L46:
	.cfi_restore_state
	movq	%r12, %rdi
	call	*%rax
	movq	48(%rbx), %rcx
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L48:
	movq	%r12, %rdi
	call	*%rax
	subq	$1, %rax
	testq	%r13, %rax
	je	.L50
.L53:
	leaq	.LC6(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L54:
	leaq	.LC7(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE4225:
	.size	_ZN2v84base20BoundedPageAllocatorC2EPNS_13PageAllocatorEmmm, .-_ZN2v84base20BoundedPageAllocatorC2EPNS_13PageAllocatorEmmm
	.globl	_ZN2v84base20BoundedPageAllocatorC1EPNS_13PageAllocatorEmmm
	.set	_ZN2v84base20BoundedPageAllocatorC1EPNS_13PageAllocatorEmmm,_ZN2v84base20BoundedPageAllocatorC2EPNS_13PageAllocatorEmmm
	.section	.text._ZNK2v84base20BoundedPageAllocator5beginEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v84base20BoundedPageAllocator5beginEv
	.type	_ZNK2v84base20BoundedPageAllocator5beginEv, @function
_ZNK2v84base20BoundedPageAllocator5beginEv:
.LFB4227:
	.cfi_startproc
	endbr64
	movq	72(%rdi), %rax
	ret
	.cfi_endproc
.LFE4227:
	.size	_ZNK2v84base20BoundedPageAllocator5beginEv, .-_ZNK2v84base20BoundedPageAllocator5beginEv
	.section	.text._ZNK2v84base20BoundedPageAllocator4sizeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v84base20BoundedPageAllocator4sizeEv
	.type	_ZNK2v84base20BoundedPageAllocator4sizeEv, @function
_ZNK2v84base20BoundedPageAllocator4sizeEv:
.LFB4228:
	.cfi_startproc
	endbr64
	movq	80(%rdi), %rax
	ret
	.cfi_endproc
.LFE4228:
	.size	_ZNK2v84base20BoundedPageAllocator4sizeEv, .-_ZNK2v84base20BoundedPageAllocator4sizeEv
	.section	.rodata._ZN2v84base20BoundedPageAllocator15AllocatePagesAtEmmNS_13PageAllocator10PermissionE.str1.8,"aMS",@progbits,1
	.align 8
.LC8:
	.string	"IsAligned(size, allocate_page_size_)"
	.align 8
.LC9:
	.string	"region_allocator_.contains(address, size)"
	.section	.text._ZN2v84base20BoundedPageAllocator15AllocatePagesAtEmmNS_13PageAllocator10PermissionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base20BoundedPageAllocator15AllocatePagesAtEmmNS_13PageAllocator10PermissionE
	.type	_ZN2v84base20BoundedPageAllocator15AllocatePagesAtEmmNS_13PageAllocator10PermissionE, @function
_ZN2v84base20BoundedPageAllocator15AllocatePagesAtEmmNS_13PageAllocator10PermissionE:
.LFB4230:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	48(%rdi), %rax
	subq	$1, %rax
	testq	%rax, %rsi
	jne	.L69
	movq	%rdx, %r13
	testq	%rax, %rdx
	jne	.L70
	movq	80(%rdi), %rdx
	movq	%rsi, %rax
	subq	72(%rdi), %rax
	movq	%rdi, %rbx
	movq	%rsi, %r12
	cmpq	%rdx, %rax
	jnb	.L60
	addq	%r13, %rax
	cmpq	%rax, %rdx
	jb	.L60
	leaq	72(%rdi), %rdi
	movq	%r13, %rdx
	movl	%ecx, %r14d
	call	_ZN2v84base15RegionAllocator16AllocateRegionAtEmm@PLT
	testb	%al, %al
	je	.L57
	movq	64(%rbx), %rdi
	leaq	_ZN2v84base20BoundedPageAllocator14SetPermissionsEPvmNS_13PageAllocator10PermissionE(%rip), %rdx
	movq	(%rdi), %rax
	movq	72(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L63
	movq	64(%rdi), %rdi
	movq	(%rdi), %rdx
	movq	72(%rdx), %r8
	cmpq	%rax, %r8
	jne	.L64
	movq	64(%rdi), %rdi
	movq	(%rdi), %rax
	movq	72(%rax), %rax
	cmpq	%r8, %rax
	jne	.L63
	movq	64(%rdi), %rdi
	movq	(%rdi), %rdx
	movq	72(%rdx), %r8
	cmpq	%rax, %r8
	jne	.L64
	movq	64(%rdi), %rdi
	movl	%r14d, %ecx
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	(%rdi), %rax
	call	*72(%rax)
.L67:
	testb	%al, %al
	je	.L71
.L57:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L60:
	.cfi_restore_state
	leaq	.LC9(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L64:
	movl	%r14d, %ecx
	movq	%r13, %rdx
	movq	%r12, %rsi
	call	*%r8
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L63:
	movl	%r14d, %ecx
	movq	%r13, %rdx
	movq	%r12, %rsi
	call	*%rax
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L69:
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L70:
	leaq	.LC8(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L71:
	leaq	.LC4(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE4230:
	.size	_ZN2v84base20BoundedPageAllocator15AllocatePagesAtEmmNS_13PageAllocator10PermissionE, .-_ZN2v84base20BoundedPageAllocator15AllocatePagesAtEmmNS_13PageAllocator10PermissionE
	.section	.text.startup._GLOBAL__sub_I__ZN2v84base20BoundedPageAllocatorC2EPNS_13PageAllocatorEmmm,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v84base20BoundedPageAllocatorC2EPNS_13PageAllocatorEmmm, @function
_GLOBAL__sub_I__ZN2v84base20BoundedPageAllocatorC2EPNS_13PageAllocatorEmmm:
.LFB4892:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE4892:
	.size	_GLOBAL__sub_I__ZN2v84base20BoundedPageAllocatorC2EPNS_13PageAllocatorEmmm, .-_GLOBAL__sub_I__ZN2v84base20BoundedPageAllocatorC2EPNS_13PageAllocatorEmmm
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v84base20BoundedPageAllocatorC2EPNS_13PageAllocatorEmmm
	.weak	_ZTVN2v84base20BoundedPageAllocatorE
	.section	.data.rel.ro.local._ZTVN2v84base20BoundedPageAllocatorE,"awG",@progbits,_ZTVN2v84base20BoundedPageAllocatorE,comdat
	.align 8
	.type	_ZTVN2v84base20BoundedPageAllocatorE, @object
	.size	_ZTVN2v84base20BoundedPageAllocatorE, 104
_ZTVN2v84base20BoundedPageAllocatorE:
	.quad	0
	.quad	0
	.quad	_ZN2v84base20BoundedPageAllocatorD1Ev
	.quad	_ZN2v84base20BoundedPageAllocatorD0Ev
	.quad	_ZN2v84base20BoundedPageAllocator16AllocatePageSizeEv
	.quad	_ZN2v84base20BoundedPageAllocator14CommitPageSizeEv
	.quad	_ZN2v84base20BoundedPageAllocator17SetRandomMmapSeedEl
	.quad	_ZN2v84base20BoundedPageAllocator17GetRandomMmapAddrEv
	.quad	_ZN2v84base20BoundedPageAllocator13AllocatePagesEPvmmNS_13PageAllocator10PermissionE
	.quad	_ZN2v84base20BoundedPageAllocator9FreePagesEPvm
	.quad	_ZN2v84base20BoundedPageAllocator12ReleasePagesEPvmm
	.quad	_ZN2v84base20BoundedPageAllocator14SetPermissionsEPvmNS_13PageAllocator10PermissionE
	.quad	_ZN2v84base20BoundedPageAllocator18DiscardSystemPagesEPvm
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
