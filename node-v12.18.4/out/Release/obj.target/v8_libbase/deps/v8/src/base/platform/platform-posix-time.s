	.file	"platform-posix-time.cc"
	.text
	.section	.text._ZN2v84base18PosixTimezoneCache5ClearENS0_13TimezoneCache17TimeZoneDetectionE,"axG",@progbits,_ZN2v84base18PosixTimezoneCache5ClearENS0_13TimezoneCache17TimeZoneDetectionE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v84base18PosixTimezoneCache5ClearENS0_13TimezoneCache17TimeZoneDetectionE
	.type	_ZN2v84base18PosixTimezoneCache5ClearENS0_13TimezoneCache17TimeZoneDetectionE, @function
_ZN2v84base18PosixTimezoneCache5ClearENS0_13TimezoneCache17TimeZoneDetectionE:
.LFB3658:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3658:
	.size	_ZN2v84base18PosixTimezoneCache5ClearENS0_13TimezoneCache17TimeZoneDetectionE, .-_ZN2v84base18PosixTimezoneCache5ClearENS0_13TimezoneCache17TimeZoneDetectionE
	.section	.text._ZN2v84base25PosixDefaultTimezoneCacheD2Ev,"axG",@progbits,_ZN2v84base25PosixDefaultTimezoneCacheD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v84base25PosixDefaultTimezoneCacheD2Ev
	.type	_ZN2v84base25PosixDefaultTimezoneCacheD2Ev, @function
_ZN2v84base25PosixDefaultTimezoneCacheD2Ev:
.LFB4178:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE4178:
	.size	_ZN2v84base25PosixDefaultTimezoneCacheD2Ev, .-_ZN2v84base25PosixDefaultTimezoneCacheD2Ev
	.weak	_ZN2v84base25PosixDefaultTimezoneCacheD1Ev
	.set	_ZN2v84base25PosixDefaultTimezoneCacheD1Ev,_ZN2v84base25PosixDefaultTimezoneCacheD2Ev
	.section	.rodata._ZN2v84base25PosixDefaultTimezoneCache13LocalTimezoneEd.str1.1,"aMS",@progbits,1
.LC0:
	.string	""
	.section	.text._ZN2v84base25PosixDefaultTimezoneCache13LocalTimezoneEd,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base25PosixDefaultTimezoneCache13LocalTimezoneEd
	.type	_ZN2v84base25PosixDefaultTimezoneCache13LocalTimezoneEd, @function
_ZN2v84base25PosixDefaultTimezoneCache13LocalTimezoneEd:
.LFB3659:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$80, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	ucomisd	%xmm0, %xmm0
	jp	.L7
	divsd	.LC1(%rip), %xmm0
	call	floor@PLT
	leaq	-64(%rbp), %rsi
	leaq	-72(%rbp), %rdi
	cvttsd2siq	%xmm0, %rax
	movq	%rax, -72(%rbp)
	call	localtime_r@PLT
	testq	%rax, %rax
	je	.L7
	movq	48(%rax), %rax
	testq	%rax, %rax
	je	.L7
.L4:
	movq	-8(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L16
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	.cfi_restore_state
	leaq	.LC0(%rip), %rax
	jmp	.L4
.L16:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3659:
	.size	_ZN2v84base25PosixDefaultTimezoneCache13LocalTimezoneEd, .-_ZN2v84base25PosixDefaultTimezoneCache13LocalTimezoneEd
	.section	.text._ZN2v84base25PosixDefaultTimezoneCache15LocalTimeOffsetEdb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base25PosixDefaultTimezoneCache15LocalTimeOffsetEdb
	.type	_ZN2v84base25PosixDefaultTimezoneCache15LocalTimeOffsetEdb, @function
_ZN2v84base25PosixDefaultTimezoneCache15LocalTimeOffsetEdb:
.LFB3660:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edi, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$80, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	call	time@PLT
	leaq	-64(%rbp), %rsi
	leaq	-72(%rbp), %rdi
	movq	%rax, -72(%rbp)
	call	localtime_r@PLT
	imulq	$1000, 40(%rax), %rdx
	movl	32(%rax), %eax
	testl	%eax, %eax
	jle	.L18
	subq	$3600000, %rdx
.L18:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
	jne	.L21
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L21:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3660:
	.size	_ZN2v84base25PosixDefaultTimezoneCache15LocalTimeOffsetEdb, .-_ZN2v84base25PosixDefaultTimezoneCache15LocalTimeOffsetEdb
	.section	.text._ZN2v84base25PosixDefaultTimezoneCacheD0Ev,"axG",@progbits,_ZN2v84base25PosixDefaultTimezoneCacheD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v84base25PosixDefaultTimezoneCacheD0Ev
	.type	_ZN2v84base25PosixDefaultTimezoneCacheD0Ev, @function
_ZN2v84base25PosixDefaultTimezoneCacheD0Ev:
.LFB4180:
	.cfi_startproc
	endbr64
	movl	$8, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE4180:
	.size	_ZN2v84base25PosixDefaultTimezoneCacheD0Ev, .-_ZN2v84base25PosixDefaultTimezoneCacheD0Ev
	.weak	_ZTVN2v84base25PosixDefaultTimezoneCacheE
	.section	.data.rel.ro._ZTVN2v84base25PosixDefaultTimezoneCacheE,"awG",@progbits,_ZTVN2v84base25PosixDefaultTimezoneCacheE,comdat
	.align 8
	.type	_ZTVN2v84base25PosixDefaultTimezoneCacheE, @object
	.size	_ZTVN2v84base25PosixDefaultTimezoneCacheE, 64
_ZTVN2v84base25PosixDefaultTimezoneCacheE:
	.quad	0
	.quad	0
	.quad	_ZN2v84base25PosixDefaultTimezoneCache13LocalTimezoneEd
	.quad	_ZN2v84base18PosixTimezoneCache21DaylightSavingsOffsetEd
	.quad	_ZN2v84base25PosixDefaultTimezoneCache15LocalTimeOffsetEdb
	.quad	_ZN2v84base18PosixTimezoneCache5ClearENS0_13TimezoneCache17TimeZoneDetectionE
	.quad	_ZN2v84base25PosixDefaultTimezoneCacheD1Ev
	.quad	_ZN2v84base25PosixDefaultTimezoneCacheD0Ev
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC1:
	.long	0
	.long	1083129856
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
