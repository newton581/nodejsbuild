	.file	"vlq-base64.cc"
	.text
	.section	.text._ZN2v84base27charToDigitDecodeForTestingEh,"ax",@progbits
	.p2align 4
	.globl	_ZN2v84base27charToDigitDecodeForTestingEh
	.type	_ZN2v84base27charToDigitDecodeForTestingEh, @function
_ZN2v84base27charToDigitDecodeForTestingEh:
.LFB2749:
	.cfi_startproc
	endbr64
	movzbl	%dil, %eax
	testb	%dil, %dil
	js	.L3
	leaq	_ZN2v84base12_GLOBAL__N_1L12kCharToDigitE(%rip), %rdx
	movzbl	(%rdx,%rax), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE2749:
	.size	_ZN2v84base27charToDigitDecodeForTestingEh, .-_ZN2v84base27charToDigitDecodeForTestingEh
	.section	.text._ZN2v84base15VLQBase64DecodeEPKcmPm,"ax",@progbits
	.p2align 4
	.globl	_ZN2v84base15VLQBase64DecodeEPKcmPm
	.type	_ZN2v84base15VLQBase64DecodeEPKcmPm, @function
_ZN2v84base15VLQBase64DecodeEPKcmPm:
.LFB2750:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r9d, %r9d
	xorl	%r11d, %r11d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	leaq	_ZN2v84base12_GLOBAL__N_1L12kCharToDigitE(%rip), %rbx
	movq	(%rdx), %rax
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L18:
	movzbl	(%rdi,%rax), %ecx
	testb	%cl, %cl
	js	.L12
	movslq	%ecx, %rcx
	addq	$5, %r9
	movsbl	(%rbx,%rcx), %ecx
	movl	%ecx, %r8d
	cmpb	$-1, %cl
	je	.L12
	cmpq	$31, %r9
	jbe	.L7
	shrl	$2, %ecx
	jne	.L12
.L7:
	movl	%r8d, %r10d
	leal	-5(%r9), %ecx
	addq	$1, %rax
	andl	$31, %r10d
	movq	%rax, (%rdx)
	sall	%cl, %r10d
	addl	%r10d, %r11d
	andl	$32, %r8d
	je	.L17
.L8:
	cmpq	%rax, %rsi
	ja	.L18
.L12:
	movl	$-2147483648, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	.cfi_restore_state
	movl	%r11d, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	shrl	%eax
	movl	%eax, %edx
	negl	%edx
	andl	$1, %r11d
	cmovne	%edx, %eax
	ret
	.cfi_endproc
.LFE2750:
	.size	_ZN2v84base15VLQBase64DecodeEPKcmPm, .-_ZN2v84base15VLQBase64DecodeEPKcmPm
	.section	.rodata._ZN2v84base12_GLOBAL__N_1L12kCharToDigitE,"a"
	.align 32
	.type	_ZN2v84base12_GLOBAL__N_1L12kCharToDigitE, @object
	.size	_ZN2v84base12_GLOBAL__N_1L12kCharToDigitE, 128
_ZN2v84base12_GLOBAL__N_1L12kCharToDigitE:
	.string	"\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377>\377\377\377?456789:;<=\377\377\377\377\377\377\377"
	.ascii	"\001\002\003\004\005\006\007\b\t\n\013\f\r\016\017\020\021\022"
	.ascii	"\023\024\025\026\027\030\031\377\377\377\377\377\377\032\033"
	.ascii	"\034\035\036\037 !\"#$%&'()*+,-./0123\377\377\377\377\377"
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
