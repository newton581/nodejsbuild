	.file	"mutex.cc"
	.text
	.section	.text._ZN2v84base5MutexC2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base5MutexC2Ev
	.type	_ZN2v84base5MutexC2Ev, @function
_ZN2v84base5MutexC2Ev:
.LFB3110:
	.cfi_startproc
	endbr64
	xorl	%esi, %esi
	jmp	pthread_mutex_init@PLT
	.cfi_endproc
.LFE3110:
	.size	_ZN2v84base5MutexC2Ev, .-_ZN2v84base5MutexC2Ev
	.globl	_ZN2v84base5MutexC1Ev
	.set	_ZN2v84base5MutexC1Ev,_ZN2v84base5MutexC2Ev
	.section	.text._ZN2v84base5MutexD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base5MutexD2Ev
	.type	_ZN2v84base5MutexD2Ev, @function
_ZN2v84base5MutexD2Ev:
.LFB3113:
	.cfi_startproc
	endbr64
	jmp	pthread_mutex_destroy@PLT
	.cfi_endproc
.LFE3113:
	.size	_ZN2v84base5MutexD2Ev, .-_ZN2v84base5MutexD2Ev
	.globl	_ZN2v84base5MutexD1Ev
	.set	_ZN2v84base5MutexD1Ev,_ZN2v84base5MutexD2Ev
	.section	.text._ZN2v84base5Mutex4LockEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base5Mutex4LockEv
	.type	_ZN2v84base5Mutex4LockEv, @function
_ZN2v84base5Mutex4LockEv:
.LFB3115:
	.cfi_startproc
	endbr64
	jmp	pthread_mutex_lock@PLT
	.cfi_endproc
.LFE3115:
	.size	_ZN2v84base5Mutex4LockEv, .-_ZN2v84base5Mutex4LockEv
	.section	.text._ZN2v84base5Mutex6UnlockEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base5Mutex6UnlockEv
	.type	_ZN2v84base5Mutex6UnlockEv, @function
_ZN2v84base5Mutex6UnlockEv:
.LFB3116:
	.cfi_startproc
	endbr64
	jmp	pthread_mutex_unlock@PLT
	.cfi_endproc
.LFE3116:
	.size	_ZN2v84base5Mutex6UnlockEv, .-_ZN2v84base5Mutex6UnlockEv
	.section	.text._ZN2v84base5Mutex7TryLockEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base5Mutex7TryLockEv
	.type	_ZN2v84base5Mutex7TryLockEv, @function
_ZN2v84base5Mutex7TryLockEv:
.LFB3117:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	pthread_mutex_trylock@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	cmpl	$16, %eax
	setne	%al
	ret
	.cfi_endproc
.LFE3117:
	.size	_ZN2v84base5Mutex7TryLockEv, .-_ZN2v84base5Mutex7TryLockEv
	.section	.text._ZN2v84base14RecursiveMutexC2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base14RecursiveMutexC2Ev
	.type	_ZN2v84base14RecursiveMutexC2Ev, @function
_ZN2v84base14RecursiveMutexC2Ev:
.LFB3119:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-28(%rbp), %r12
	movq	%r12, %rdi
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	pthread_mutexattr_init@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	call	pthread_mutexattr_settype@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	pthread_mutex_init@PLT
	movq	%r12, %rdi
	call	pthread_mutexattr_destroy@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L11
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L11:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3119:
	.size	_ZN2v84base14RecursiveMutexC2Ev, .-_ZN2v84base14RecursiveMutexC2Ev
	.globl	_ZN2v84base14RecursiveMutexC1Ev
	.set	_ZN2v84base14RecursiveMutexC1Ev,_ZN2v84base14RecursiveMutexC2Ev
	.section	.text._ZN2v84base14RecursiveMutexD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base14RecursiveMutexD2Ev
	.type	_ZN2v84base14RecursiveMutexD2Ev, @function
_ZN2v84base14RecursiveMutexD2Ev:
.LFB3122:
	.cfi_startproc
	endbr64
	jmp	pthread_mutex_destroy@PLT
	.cfi_endproc
.LFE3122:
	.size	_ZN2v84base14RecursiveMutexD2Ev, .-_ZN2v84base14RecursiveMutexD2Ev
	.globl	_ZN2v84base14RecursiveMutexD1Ev
	.set	_ZN2v84base14RecursiveMutexD1Ev,_ZN2v84base14RecursiveMutexD2Ev
	.section	.text._ZN2v84base14RecursiveMutex4LockEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base14RecursiveMutex4LockEv
	.type	_ZN2v84base14RecursiveMutex4LockEv, @function
_ZN2v84base14RecursiveMutex4LockEv:
.LFB3124:
	.cfi_startproc
	endbr64
	jmp	pthread_mutex_lock@PLT
	.cfi_endproc
.LFE3124:
	.size	_ZN2v84base14RecursiveMutex4LockEv, .-_ZN2v84base14RecursiveMutex4LockEv
	.section	.text._ZN2v84base14RecursiveMutex6UnlockEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base14RecursiveMutex6UnlockEv
	.type	_ZN2v84base14RecursiveMutex6UnlockEv, @function
_ZN2v84base14RecursiveMutex6UnlockEv:
.LFB3125:
	.cfi_startproc
	endbr64
	jmp	pthread_mutex_unlock@PLT
	.cfi_endproc
.LFE3125:
	.size	_ZN2v84base14RecursiveMutex6UnlockEv, .-_ZN2v84base14RecursiveMutex6UnlockEv
	.section	.text._ZN2v84base14RecursiveMutex7TryLockEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base14RecursiveMutex7TryLockEv
	.type	_ZN2v84base14RecursiveMutex7TryLockEv, @function
_ZN2v84base14RecursiveMutex7TryLockEv:
.LFB3126:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	pthread_mutex_trylock@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	cmpl	$16, %eax
	setne	%al
	ret
	.cfi_endproc
.LFE3126:
	.size	_ZN2v84base14RecursiveMutex7TryLockEv, .-_ZN2v84base14RecursiveMutex7TryLockEv
	.section	.text._ZN2v84base11SharedMutexC2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base11SharedMutexC2Ev
	.type	_ZN2v84base11SharedMutexC2Ev, @function
_ZN2v84base11SharedMutexC2Ev:
.LFB3128:
	.cfi_startproc
	endbr64
	xorl	%esi, %esi
	jmp	pthread_rwlock_init@PLT
	.cfi_endproc
.LFE3128:
	.size	_ZN2v84base11SharedMutexC2Ev, .-_ZN2v84base11SharedMutexC2Ev
	.globl	_ZN2v84base11SharedMutexC1Ev
	.set	_ZN2v84base11SharedMutexC1Ev,_ZN2v84base11SharedMutexC2Ev
	.section	.text._ZN2v84base11SharedMutexD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base11SharedMutexD2Ev
	.type	_ZN2v84base11SharedMutexD2Ev, @function
_ZN2v84base11SharedMutexD2Ev:
.LFB3131:
	.cfi_startproc
	endbr64
	jmp	pthread_rwlock_destroy@PLT
	.cfi_endproc
.LFE3131:
	.size	_ZN2v84base11SharedMutexD2Ev, .-_ZN2v84base11SharedMutexD2Ev
	.globl	_ZN2v84base11SharedMutexD1Ev
	.set	_ZN2v84base11SharedMutexD1Ev,_ZN2v84base11SharedMutexD2Ev
	.section	.text._ZN2v84base11SharedMutex10LockSharedEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base11SharedMutex10LockSharedEv
	.type	_ZN2v84base11SharedMutex10LockSharedEv, @function
_ZN2v84base11SharedMutex10LockSharedEv:
.LFB3133:
	.cfi_startproc
	endbr64
	jmp	pthread_rwlock_rdlock@PLT
	.cfi_endproc
.LFE3133:
	.size	_ZN2v84base11SharedMutex10LockSharedEv, .-_ZN2v84base11SharedMutex10LockSharedEv
	.section	.text._ZN2v84base11SharedMutex13LockExclusiveEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base11SharedMutex13LockExclusiveEv
	.type	_ZN2v84base11SharedMutex13LockExclusiveEv, @function
_ZN2v84base11SharedMutex13LockExclusiveEv:
.LFB3134:
	.cfi_startproc
	endbr64
	jmp	pthread_rwlock_wrlock@PLT
	.cfi_endproc
.LFE3134:
	.size	_ZN2v84base11SharedMutex13LockExclusiveEv, .-_ZN2v84base11SharedMutex13LockExclusiveEv
	.section	.text._ZN2v84base11SharedMutex12UnlockSharedEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base11SharedMutex12UnlockSharedEv
	.type	_ZN2v84base11SharedMutex12UnlockSharedEv, @function
_ZN2v84base11SharedMutex12UnlockSharedEv:
.LFB3135:
	.cfi_startproc
	endbr64
	jmp	pthread_rwlock_unlock@PLT
	.cfi_endproc
.LFE3135:
	.size	_ZN2v84base11SharedMutex12UnlockSharedEv, .-_ZN2v84base11SharedMutex12UnlockSharedEv
	.section	.text._ZN2v84base11SharedMutex15UnlockExclusiveEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base11SharedMutex15UnlockExclusiveEv
	.type	_ZN2v84base11SharedMutex15UnlockExclusiveEv, @function
_ZN2v84base11SharedMutex15UnlockExclusiveEv:
.LFB3651:
	.cfi_startproc
	endbr64
	jmp	pthread_rwlock_unlock@PLT
	.cfi_endproc
.LFE3651:
	.size	_ZN2v84base11SharedMutex15UnlockExclusiveEv, .-_ZN2v84base11SharedMutex15UnlockExclusiveEv
	.section	.text._ZN2v84base11SharedMutex13TryLockSharedEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base11SharedMutex13TryLockSharedEv
	.type	_ZN2v84base11SharedMutex13TryLockSharedEv, @function
_ZN2v84base11SharedMutex13TryLockSharedEv:
.LFB3137:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	pthread_rwlock_tryrdlock@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	testl	%eax, %eax
	sete	%al
	ret
	.cfi_endproc
.LFE3137:
	.size	_ZN2v84base11SharedMutex13TryLockSharedEv, .-_ZN2v84base11SharedMutex13TryLockSharedEv
	.section	.text._ZN2v84base11SharedMutex16TryLockExclusiveEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base11SharedMutex16TryLockExclusiveEv
	.type	_ZN2v84base11SharedMutex16TryLockExclusiveEv, @function
_ZN2v84base11SharedMutex16TryLockExclusiveEv:
.LFB3138:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	pthread_rwlock_trywrlock@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	testl	%eax, %eax
	sete	%al
	ret
	.cfi_endproc
.LFE3138:
	.size	_ZN2v84base11SharedMutex16TryLockExclusiveEv, .-_ZN2v84base11SharedMutex16TryLockExclusiveEv
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
