	.file	"random-number-generator.cc"
	.text
	.section	.text._ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data,"axG",@progbits,_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data,comdat
	.p2align 4
	.weak	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data
	.type	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data, @function
_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data:
.LFB5046:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %r8
	movq	(%rdi), %rax
	movq	%r8, %rdi
	jmp	*%rax
	.cfi_endproc
.LFE5046:
	.size	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data, .-_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation,"axG",@progbits,_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation,comdat
	.p2align 4
	.weak	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation
	.type	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation:
.LFB5047:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L4
	cmpl	$3, %edx
	je	.L5
	cmpl	$1, %edx
	je	.L9
.L5:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	movdqu	(%rsi), %xmm0
	xorl	%eax, %eax
	movups	%xmm0, (%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE5047:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation
	.section	.text._ZN2v84base16LazyInstanceImplINS0_5MutexENS0_32StaticallyAllocatedInstanceTraitIS2_EENS0_21DefaultConstructTraitIS2_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS2_EEE12InitInstanceEPv,"axG",@progbits,_ZN2v84base16LazyInstanceImplINS0_5MutexENS0_32StaticallyAllocatedInstanceTraitIS2_EENS0_21DefaultConstructTraitIS2_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS2_EEE12InitInstanceEPv,comdat
	.p2align 4
	.weak	_ZN2v84base16LazyInstanceImplINS0_5MutexENS0_32StaticallyAllocatedInstanceTraitIS2_EENS0_21DefaultConstructTraitIS2_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS2_EEE12InitInstanceEPv
	.type	_ZN2v84base16LazyInstanceImplINS0_5MutexENS0_32StaticallyAllocatedInstanceTraitIS2_EENS0_21DefaultConstructTraitIS2_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS2_EEE12InitInstanceEPv, @function
_ZN2v84base16LazyInstanceImplINS0_5MutexENS0_32StaticallyAllocatedInstanceTraitIS2_EENS0_21DefaultConstructTraitIS2_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS2_EEE12InitInstanceEPv:
.LFB4782:
	.cfi_startproc
	endbr64
	jmp	_ZN2v84base5MutexC1Ev@PLT
	.cfi_endproc
.LFE4782:
	.size	_ZN2v84base16LazyInstanceImplINS0_5MutexENS0_32StaticallyAllocatedInstanceTraitIS2_EENS0_21DefaultConstructTraitIS2_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS2_EEE12InitInstanceEPv, .-_ZN2v84base16LazyInstanceImplINS0_5MutexENS0_32StaticallyAllocatedInstanceTraitIS2_EENS0_21DefaultConstructTraitIS2_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS2_EEE12InitInstanceEPv
	.section	.text._ZN2v84base21RandomNumberGenerator16SetEntropySourceEPFbPhmE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base21RandomNumberGenerator16SetEntropySourceEPFbPhmE
	.type	_ZN2v84base21RandomNumberGenerator16SetEntropySourceEPFbPhmE, @function
_ZN2v84base21RandomNumberGenerator16SetEntropySourceEPFbPhmE:
.LFB4196:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movzbl	_ZN2v84baseL13entropy_mutexE(%rip), %eax
	cmpb	$2, %al
	jne	.L22
.L12:
	leaq	8+_ZN2v84baseL13entropy_mutexE(%rip), %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	leaq	8+_ZN2v84baseL13entropy_mutexE(%rip), %rdi
	movq	%rbx, _ZN2v84baseL14entropy_sourceE(%rip)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L23
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	.cfi_restore_state
	leaq	_ZN2v84base16LazyInstanceImplINS0_5MutexENS0_32StaticallyAllocatedInstanceTraitIS2_EENS0_21DefaultConstructTraitIS2_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS2_EEE12InitInstanceEPv(%rip), %rax
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation(%rip), %rcx
	movq	%rax, -64(%rbp)
	movq	%rcx, %xmm0
	leaq	8+_ZN2v84baseL13entropy_mutexE(%rip), %rax
	leaq	-64(%rbp), %r12
	movq	%rax, -56(%rbp)
	leaq	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data(%rip), %rax
	movq	%r12, %rsi
	leaq	_ZN2v84baseL13entropy_mutexE(%rip), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v84base12CallOnceImplEPSt6atomicIhESt8functionIFvvEE@PLT
	movq	-48(%rbp), %rax
	testq	%rax, %rax
	je	.L12
	movl	$3, %edx
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	*%rax
	jmp	.L12
.L23:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4196:
	.size	_ZN2v84base21RandomNumberGenerator16SetEntropySourceEPFbPhmE, .-_ZN2v84base21RandomNumberGenerator16SetEntropySourceEPFbPhmE
	.section	.rodata._ZN2v84base21RandomNumberGeneratorC2Ev.str1.1,"aMS",@progbits,1
.LC0:
	.string	"state0_ != 0 || state1_ != 0"
.LC1:
	.string	"Check failed: %s."
.LC2:
	.string	"rb"
.LC3:
	.string	"/dev/urandom"
	.section	.text._ZN2v84base21RandomNumberGeneratorC2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base21RandomNumberGeneratorC2Ev
	.type	_ZN2v84base21RandomNumberGeneratorC2Ev, @function
_ZN2v84base21RandomNumberGeneratorC2Ev:
.LFB4198:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movzbl	_ZN2v84baseL13entropy_mutexE(%rip), %eax
	cmpb	$2, %al
	jne	.L69
.L25:
	leaq	8+_ZN2v84baseL13entropy_mutexE(%rip), %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	_ZN2v84baseL14entropy_sourceE(%rip), %rax
	testq	%rax, %rax
	je	.L27
	leaq	-96(%rbp), %rdi
	movl	$8, %esi
	call	*%rax
	testb	%al, %al
	jne	.L70
.L27:
	leaq	8+_ZN2v84baseL13entropy_mutexE(%rip), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	call	fopen@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L31
	leaq	-88(%rbp), %rdi
	movq	%rax, %rcx
	movl	$1, %edx
	movl	$8, %esi
	call	fread@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	fclose@PLT
	movq	-88(%rbp), %rax
	cmpq	$1, %r13
	je	.L68
.L31:
	call	_ZN2v84base4Time17NowFromSystemTimeEv@PLT
	salq	$24, %rax
	movq	%rax, %r12
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	salq	$16, %rax
	xorq	%rax, %r12
	call	_ZN2v84base9TimeTicks3NowEv@PLT
	salq	$8, %rax
	xorq	%r12, %rax
.L68:
	movabsq	$-4265267296055464877, %rdi
	movq	%rax, %rcx
	movq	%rax, (%rbx)
	shrq	$33, %rcx
	xorq	%rax, %rcx
	movabsq	$-49064778989728563, %rax
	imulq	%rax, %rcx
	movq	%rcx, %rdx
	shrq	$33, %rdx
	xorq	%rdx, %rcx
	imulq	%rdi, %rcx
	movq	%rcx, %r8
	movq	%rcx, %rdx
	shrq	$33, %r8
	xorq	%r8, %rdx
	movq	%rdx, %rsi
	movq	%rdx, 8(%rbx)
	notq	%rsi
	movq	%rsi, %rdx
	shrq	$33, %rdx
	xorq	%rsi, %rdx
	imulq	%rax, %rdx
	movq	%rdx, %rax
	shrq	$33, %rax
	xorq	%rdx, %rax
	imulq	%rdi, %rax
	movq	%rax, %rdx
	movq	%rax, %rsi
	shrq	$33, %rdx
	xorq	%rdx, %rsi
	movq	%rsi, 16(%rbx)
	cmpq	%rdx, %rax
	jne	.L24
	cmpq	%r8, %rcx
	je	.L33
.L24:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L71
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L69:
	.cfi_restore_state
	leaq	_ZN2v84base16LazyInstanceImplINS0_5MutexENS0_32StaticallyAllocatedInstanceTraitIS2_EENS0_21DefaultConstructTraitIS2_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS2_EEE12InitInstanceEPv(%rip), %rax
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation(%rip), %rsi
	movq	%rax, -80(%rbp)
	movq	%rsi, %xmm0
	leaq	8+_ZN2v84baseL13entropy_mutexE(%rip), %rax
	leaq	-80(%rbp), %r12
	movq	%rax, -72(%rbp)
	leaq	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data(%rip), %rax
	movq	%r12, %rsi
	leaq	_ZN2v84baseL13entropy_mutexE(%rip), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v84base12CallOnceImplEPSt6atomicIhESt8functionIFvvEE@PLT
	movq	-64(%rbp), %rax
	testq	%rax, %rax
	je	.L25
	movl	$3, %edx
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	*%rax
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L70:
	movabsq	$-4265267296055464877, %rdi
	movq	-96(%rbp), %rax
	movq	%rax, %rcx
	movq	%rax, (%rbx)
	shrq	$33, %rcx
	xorq	%rax, %rcx
	movabsq	$-49064778989728563, %rax
	imulq	%rax, %rcx
	movq	%rcx, %rdx
	shrq	$33, %rdx
	xorq	%rdx, %rcx
	imulq	%rdi, %rcx
	movq	%rcx, %r8
	movq	%rcx, %rdx
	shrq	$33, %r8
	xorq	%r8, %rdx
	movq	%rdx, %rsi
	movq	%rdx, 8(%rbx)
	notq	%rsi
	movq	%rsi, %rdx
	shrq	$33, %rdx
	xorq	%rsi, %rdx
	imulq	%rax, %rdx
	movq	%rdx, %rax
	shrq	$33, %rax
	xorq	%rdx, %rax
	imulq	%rdi, %rax
	movq	%rax, %rdx
	movq	%rax, %rsi
	shrq	$33, %rdx
	xorq	%rdx, %rsi
	movq	%rsi, 16(%rbx)
	cmpq	%rdx, %rax
	jne	.L29
	cmpq	%r8, %rcx
	je	.L33
.L29:
	leaq	8+_ZN2v84baseL13entropy_mutexE(%rip), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	jmp	.L24
.L33:
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L71:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4198:
	.size	_ZN2v84base21RandomNumberGeneratorC2Ev, .-_ZN2v84base21RandomNumberGeneratorC2Ev
	.globl	_ZN2v84base21RandomNumberGeneratorC1Ev
	.set	_ZN2v84base21RandomNumberGeneratorC1Ev,_ZN2v84base21RandomNumberGeneratorC2Ev
	.section	.text._ZN2v84base21RandomNumberGenerator7NextIntEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base21RandomNumberGenerator7NextIntEi
	.type	_ZN2v84base21RandomNumberGenerator7NextIntEi, @function
_ZN2v84base21RandomNumberGenerator7NextIntEi:
.LFB4200:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %r11
	movq	16(%rdi), %r8
	leal	-1(%rsi), %r9d
	testl	%esi, %esi
	jle	.L73
	testl	%r9d, %esi
	je	.L77
.L73:
	movl	$2147483647, %r10d
	.p2align 4,,10
	.p2align 3
.L75:
	movq	%r11, %rax
	salq	$23, %rax
	xorq	%r11, %rax
	movq	%r8, %r11
	movq	%rax, %rdx
	movq	%rax, %rcx
	movq	%r8, %rax
	shrq	$26, %rax
	shrq	$17, %rcx
	xorq	%rdx, %rax
	xorq	%r8, %rax
	xorq	%rax, %rcx
	movq	%rcx, %r8
	leaq	(%r11,%rcx), %rcx
	shrq	$33, %rcx
	movl	%ecx, %eax
	cltd
	idivl	%esi
	movl	%r10d, %eax
	subl	%edx, %ecx
	subl	%ecx, %eax
	cmpl	%r9d, %eax
	jl	.L75
	movq	%r11, %xmm0
	movq	%r8, %xmm1
	movl	%edx, %eax
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 8(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L77:
	movq	%r11, %rdx
	movq	%r8, %rcx
	movslq	%esi, %rsi
	movq	%r8, 8(%rdi)
	salq	$23, %rdx
	shrq	$26, %rcx
	xorq	%r11, %rdx
	xorq	%r8, %rcx
	xorq	%rdx, %rcx
	shrq	$17, %rdx
	xorq	%rcx, %rdx
	movq	%rdx, 16(%rdi)
	addq	%r8, %rdx
	shrq	$33, %rdx
	imulq	%rsi, %rdx
	sarq	$31, %rdx
	movl	%edx, %eax
	ret
	.cfi_endproc
.LFE4200:
	.size	_ZN2v84base21RandomNumberGenerator7NextIntEi, .-_ZN2v84base21RandomNumberGenerator7NextIntEi
	.section	.text._ZN2v84base21RandomNumberGenerator10NextDoubleEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base21RandomNumberGenerator10NextDoubleEv
	.type	_ZN2v84base21RandomNumberGenerator10NextDoubleEv, @function
_ZN2v84base21RandomNumberGenerator10NextDoubleEv:
.LFB4202:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdx
	movq	16(%rdi), %rax
	movq	%rdx, %rcx
	movq	%rax, 8(%rdi)
	salq	$23, %rcx
	xorq	%rdx, %rcx
	movq	%rax, %rdx
	shrq	$26, %rdx
	movq	%rcx, %rsi
	xorq	%rax, %rdx
	shrq	$17, %rsi
	xorq	%rcx, %rdx
	shrq	$12, %rax
	xorq	%rsi, %rdx
	movq	%rdx, 16(%rdi)
	movabsq	$4607182418800017408, %rdx
	orq	%rdx, %rax
	movq	%rax, %xmm0
	subsd	.LC4(%rip), %xmm0
	ret
	.cfi_endproc
.LFE4202:
	.size	_ZN2v84base21RandomNumberGenerator10NextDoubleEv, .-_ZN2v84base21RandomNumberGenerator10NextDoubleEv
	.section	.text._ZN2v84base21RandomNumberGenerator9NextInt64Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base21RandomNumberGenerator9NextInt64Ev
	.type	_ZN2v84base21RandomNumberGenerator9NextInt64Ev, @function
_ZN2v84base21RandomNumberGenerator9NextInt64Ev:
.LFB4203:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	movq	16(%rdi), %rdx
	movq	%rax, %rcx
	movq	%rdx, 8(%rdi)
	salq	$23, %rcx
	xorq	%rax, %rcx
	movq	%rdx, %rax
	shrq	$26, %rax
	movq	%rcx, %rsi
	xorq	%rdx, %rax
	shrq	$17, %rsi
	xorq	%rcx, %rax
	xorq	%rsi, %rax
	movq	%rax, 16(%rdi)
	addq	%rdx, %rax
	ret
	.cfi_endproc
.LFE4203:
	.size	_ZN2v84base21RandomNumberGenerator9NextInt64Ev, .-_ZN2v84base21RandomNumberGenerator9NextInt64Ev
	.section	.text._ZN2v84base21RandomNumberGenerator9NextBytesEPvm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base21RandomNumberGenerator9NextBytesEPvm
	.type	_ZN2v84base21RandomNumberGenerator9NextBytesEPvm, @function
_ZN2v84base21RandomNumberGenerator9NextBytesEPvm:
.LFB4204:
	.cfi_startproc
	endbr64
	testq	%rdx, %rdx
	je	.L80
	leaq	(%rsi,%rdx), %r9
	.p2align 4,,10
	.p2align 3
.L82:
	movq	8(%rdi), %rdx
	movq	16(%rdi), %rax
	addq	$1, %rsi
	movq	%rdx, %rcx
	movq	%rax, 8(%rdi)
	salq	$23, %rcx
	xorq	%rdx, %rcx
	movq	%rax, %rdx
	shrq	$26, %rdx
	movq	%rcx, %r8
	xorq	%rax, %rdx
	shrq	$17, %r8
	xorq	%rcx, %rdx
	xorq	%r8, %rdx
	addq	%rdx, %rax
	movq	%rdx, 16(%rdi)
	shrq	$56, %rax
	movb	%al, -1(%rsi)
	cmpq	%rsi, %r9
	jne	.L82
.L80:
	ret
	.cfi_endproc
.LFE4204:
	.size	_ZN2v84base21RandomNumberGenerator9NextBytesEPvm, .-_ZN2v84base21RandomNumberGenerator9NextBytesEPvm
	.section	.text._ZN2v84base21RandomNumberGenerator4NextEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base21RandomNumberGenerator4NextEi
	.type	_ZN2v84base21RandomNumberGenerator4NextEi, @function
_ZN2v84base21RandomNumberGenerator4NextEi:
.LFB4224:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	movq	16(%rdi), %rdx
	movl	%esi, %r8d
	movq	%rax, %rcx
	movq	%rdx, 8(%rdi)
	salq	$23, %rcx
	xorq	%rax, %rcx
	movq	%rdx, %rax
	shrq	$26, %rax
	movq	%rcx, %rsi
	xorq	%rdx, %rax
	shrq	$17, %rsi
	xorq	%rcx, %rax
	movl	$64, %ecx
	xorq	%rsi, %rax
	subl	%r8d, %ecx
	movq	%rax, 16(%rdi)
	addq	%rdx, %rax
	shrq	%cl, %rax
	ret
	.cfi_endproc
.LFE4224:
	.size	_ZN2v84base21RandomNumberGenerator4NextEi, .-_ZN2v84base21RandomNumberGenerator4NextEi
	.section	.text._ZN2v84base21RandomNumberGenerator7SetSeedEl,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base21RandomNumberGenerator7SetSeedEl
	.type	_ZN2v84base21RandomNumberGenerator7SetSeedEl, @function
_ZN2v84base21RandomNumberGenerator7SetSeedEl:
.LFB4225:
	.cfi_startproc
	endbr64
	movabsq	$-4265267296055464877, %r8
	movq	%rsi, %rax
	movq	%rsi, (%rdi)
	shrq	$33, %rsi
	xorq	%rax, %rsi
	movabsq	$-49064778989728563, %rax
	imulq	%rax, %rsi
	movq	%rsi, %rdx
	shrq	$33, %rdx
	xorq	%rdx, %rsi
	imulq	%r8, %rsi
	movq	%rsi, %r9
	movq	%rsi, %rdx
	shrq	$33, %r9
	xorq	%r9, %rdx
	movq	%rdx, %rcx
	movq	%rdx, 8(%rdi)
	notq	%rcx
	movq	%rcx, %rdx
	shrq	$33, %rdx
	xorq	%rcx, %rdx
	imulq	%rax, %rdx
	movq	%rdx, %rax
	shrq	$33, %rax
	xorq	%rdx, %rax
	imulq	%r8, %rax
	movq	%rax, %rdx
	movq	%rax, %rcx
	shrq	$33, %rdx
	xorq	%rdx, %rcx
	movq	%rcx, 16(%rdi)
	cmpq	%rdx, %rax
	jne	.L88
	cmpq	%r9, %rsi
	je	.L99
.L88:
	ret
.L99:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE4225:
	.size	_ZN2v84base21RandomNumberGenerator7SetSeedEl, .-_ZN2v84base21RandomNumberGenerator7SetSeedEl
	.section	.text._ZN2v84base21RandomNumberGenerator11MurmurHash3Em,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base21RandomNumberGenerator11MurmurHash3Em
	.type	_ZN2v84base21RandomNumberGenerator11MurmurHash3Em, @function
_ZN2v84base21RandomNumberGenerator11MurmurHash3Em:
.LFB4226:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	shrq	$33, %rax
	xorq	%rdi, %rax
	movabsq	$-49064778989728563, %rdi
	imulq	%rax, %rdi
	movq	%rdi, %rax
	shrq	$33, %rax
	xorq	%rdi, %rax
	movabsq	$-4265267296055464877, %rdi
	imulq	%rdi, %rax
	movq	%rax, %rdi
	shrq	$33, %rdi
	xorq	%rdi, %rax
	ret
	.cfi_endproc
.LFE4226:
	.size	_ZN2v84base21RandomNumberGenerator11MurmurHash3Em, .-_ZN2v84base21RandomNumberGenerator11MurmurHash3Em
	.section	.rodata._ZNSt6vectorImSaImEE7reserveEm.str1.1,"aMS",@progbits,1
.LC5:
	.string	"vector::reserve"
	.section	.text._ZNSt6vectorImSaImEE7reserveEm,"axG",@progbits,_ZNSt6vectorImSaImEE7reserveEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorImSaImEE7reserveEm
	.type	_ZNSt6vectorImSaImEE7reserveEm, @function
_ZNSt6vectorImSaImEE7reserveEm:
.LFB4535:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rax
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	cmpq	%rax, %rsi
	ja	.L113
	movq	(%rdi), %r12
	movq	16(%rdi), %rax
	movq	%rdi, %rbx
	subq	%r12, %rax
	sarq	$3, %rax
	cmpq	%rax, %rsi
	ja	.L114
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L114:
	.cfi_restore_state
	movq	8(%rdi), %r13
	leaq	0(,%rsi,8), %r14
	subq	%r12, %r13
	testq	%rsi, %rsi
	je	.L108
	movq	%r14, %rdi
	call	_Znwm@PLT
	movq	(%rbx), %r12
	movq	8(%rbx), %rdx
	movq	%rax, %r15
	subq	%r12, %rdx
.L104:
	testq	%rdx, %rdx
	jg	.L115
	testq	%r12, %r12
	jne	.L106
.L107:
	addq	%r15, %r13
	addq	%r15, %r14
	movq	%r15, (%rbx)
	movq	%r13, 8(%rbx)
	movq	%r14, 16(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L115:
	.cfi_restore_state
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	memmove@PLT
.L106:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L108:
	movq	%r13, %rdx
	xorl	%r15d, %r15d
	jmp	.L104
.L113:
	leaq	.LC5(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE4535:
	.size	_ZNSt6vectorImSaImEE7reserveEm, .-_ZNSt6vectorImSaImEE7reserveEm
	.section	.rodata._ZNSt6vectorImSaImEE17_M_realloc_insertIJRKmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_.str1.1,"aMS",@progbits,1
.LC6:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorImSaImEE17_M_realloc_insertIJRKmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_,"axG",@progbits,_ZNSt6vectorImSaImEE17_M_realloc_insertIJRKmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorImSaImEE17_M_realloc_insertIJRKmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	.type	_ZNSt6vectorImSaImEE17_M_realloc_insertIJRKmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_, @function
_ZNSt6vectorImSaImEE17_M_realloc_insertIJRKmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_:
.LFB4719:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r9
	movq	%r13, %rax
	subq	%r9, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L130
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L126
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L131
.L118:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L125:
	movq	(%r15), %rax
	subq	%r8, %r13
	leaq	8(%rbx,%rdx), %r15
	movq	%rax, (%rbx,%rdx)
	leaq	(%r15,%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L132
	testq	%r13, %r13
	jg	.L121
	testq	%r9, %r9
	jne	.L124
.L122:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L132:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jg	.L121
.L124:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L122
	.p2align 4,,10
	.p2align 3
.L131:
	testq	%rsi, %rsi
	jne	.L119
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L125
	.p2align 4,,10
	.p2align 3
.L121:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L122
	jmp	.L124
	.p2align 4,,10
	.p2align 3
.L126:
	movl	$8, %r14d
	jmp	.L118
.L130:
	leaq	.LC6(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L119:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$3, %r14
	jmp	.L118
	.cfi_endproc
.LFE4719:
	.size	_ZNSt6vectorImSaImEE17_M_realloc_insertIJRKmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_, .-_ZNSt6vectorImSaImEE17_M_realloc_insertIJRKmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	.section	.text._ZN2v84baseL16ComplementSampleERKSt13unordered_setImSt4hashImESt8equal_toImESaImEEm,"ax",@progbits
	.p2align 4
	.type	_ZN2v84baseL16ComplementSampleERKSt13unordered_setImSt4hashImESt8equal_toImESaImEEm, @function
_ZN2v84baseL16ComplementSampleERKSt13unordered_setImSt4hashImESt8equal_toImESaImEEm:
.LFB4205:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	movq	%rdx, %rsi
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, 16(%rdi)
	movups	%xmm0, (%rdi)
	subq	24(%rbx), %rsi
	call	_ZNSt6vectorImSaImEE7reserveEm
	movq	$0, -48(%rbp)
	testq	%r12, %r12
	je	.L133
	xorl	%ecx, %ecx
	leaq	-48(%rbp), %r13
	.p2align 4,,10
	.p2align 3
.L134:
	movq	8(%rbx), %r8
	movq	%rcx, %rax
	xorl	%edx, %edx
	divq	%r8
	movq	(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r11
	testq	%rax, %rax
	je	.L135
	movq	(%rax), %rsi
	testq	%rsi, %rsi
	je	.L135
	movq	8(%rsi), %r9
	xorl	%r10d, %r10d
	jmp	.L140
	.p2align 4,,10
	.p2align 3
.L136:
	testq	%r10, %r10
	jne	.L138
	movq	(%rsi), %rsi
	testq	%rsi, %rsi
	je	.L139
.L157:
	movq	8(%rsi), %r9
	xorl	%edx, %edx
	movq	%r9, %rax
	divq	%r8
	cmpq	%rdx, %r11
	jne	.L139
.L140:
	cmpq	%r9, %rcx
	jne	.L136
	movq	(%rsi), %rsi
	addq	$1, %r10
	testq	%rsi, %rsi
	jne	.L157
	.p2align 4,,10
	.p2align 3
.L139:
	testq	%r10, %r10
	je	.L135
.L138:
	addq	$1, %rcx
	movq	%rcx, -48(%rbp)
	cmpq	%r12, %rcx
	jb	.L134
.L133:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L158
	addq	$16, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L135:
	.cfi_restore_state
	movq	8(%r14), %rsi
	cmpq	16(%r14), %rsi
	je	.L141
	movq	%rcx, (%rsi)
	movq	-48(%rbp), %rcx
	addq	$8, 8(%r14)
	addq	$1, %rcx
	movq	%rcx, -48(%rbp)
	cmpq	%r12, %rcx
	jb	.L134
	jmp	.L133
	.p2align 4,,10
	.p2align 3
.L141:
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJRKmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	movq	-48(%rbp), %rcx
	addq	$1, %rcx
	movq	%rcx, -48(%rbp)
	cmpq	%r12, %rcx
	jb	.L134
	jmp	.L133
.L158:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4205:
	.size	_ZN2v84baseL16ComplementSampleERKSt13unordered_setImSt4hashImESt8equal_toImESaImEEm, .-_ZN2v84baseL16ComplementSampleERKSt13unordered_setImSt4hashImESt8equal_toImESaImEEm
	.section	.text._ZNSt10_HashtableImmSaImENSt8__detail9_IdentityESt8equal_toImESt4hashImENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS1_10_Hash_nodeImLb0EEEm,"axG",@progbits,_ZNSt10_HashtableImmSaImENSt8__detail9_IdentityESt8equal_toImESt4hashImENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS1_10_Hash_nodeImLb0EEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableImmSaImENSt8__detail9_IdentityESt8equal_toImESt4hashImENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS1_10_Hash_nodeImLb0EEEm
	.type	_ZNSt10_HashtableImmSaImENSt8__detail9_IdentityESt8equal_toImESt4hashImENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS1_10_Hash_nodeImLb0EEEm, @function
_ZNSt10_HashtableImmSaImENSt8__detail9_IdentityESt8equal_toImESt4hashImENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS1_10_Hash_nodeImLb0EEEm:
.LFB4897:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	movq	%r8, %rcx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$32, %rdi
	subq	$24, %rsp
	movq	-8(%rdi), %rdx
	movq	-24(%rdi), %rsi
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	testb	%al, %al
	jne	.L160
	movq	(%rbx), %r8
.L161:
	movq	(%r8,%r15,8), %rax
	leaq	0(,%r15,8), %rcx
	testq	%rax, %rax
	je	.L170
	movq	(%rax), %rax
	movq	%rax, 0(%r13)
	movq	(%rbx), %rax
	movq	(%rax,%r15,8), %rax
	movq	%r13, (%rax)
.L171:
	addq	$1, 24(%rbx)
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L160:
	.cfi_restore_state
	movq	%rdx, %r12
	cmpq	$1, %rdx
	je	.L184
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L185
	leaq	0(,%rdx,8), %r15
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	%r15, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	leaq	48(%rbx), %r10
	movq	%rax, %r8
.L163:
	movq	16(%rbx), %rsi
	movq	$0, 16(%rbx)
	testq	%rsi, %rsi
	je	.L165
	xorl	%edi, %edi
	leaq	16(%rbx), %r9
	jmp	.L166
	.p2align 4,,10
	.p2align 3
.L167:
	movq	(%r11), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L168:
	testq	%rsi, %rsi
	je	.L165
.L166:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	8(%rcx), %rax
	divq	%r12
	leaq	(%r8,%rdx,8), %rax
	movq	(%rax), %r11
	testq	%r11, %r11
	jne	.L167
	movq	16(%rbx), %r11
	movq	%r11, (%rcx)
	movq	%rcx, 16(%rbx)
	movq	%r9, (%rax)
	cmpq	$0, (%rcx)
	je	.L173
	movq	%rcx, (%r8,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L166
	.p2align 4,,10
	.p2align 3
.L165:
	movq	(%rbx), %rdi
	cmpq	%rdi, %r10
	je	.L169
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L169:
	movq	%r14, %rax
	xorl	%edx, %edx
	movq	%r12, 8(%rbx)
	divq	%r12
	movq	%r8, (%rbx)
	movq	%rdx, %r15
	jmp	.L161
	.p2align 4,,10
	.p2align 3
.L170:
	movq	16(%rbx), %rax
	movq	%rax, 0(%r13)
	movq	%r13, 16(%rbx)
	movq	0(%r13), %rax
	testq	%rax, %rax
	je	.L172
	movq	8(%rax), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	(%rbx), %rax
	movq	%r13, (%rax,%rdx,8)
.L172:
	movq	(%rbx), %rax
	leaq	16(%rbx), %rdx
	movq	%rdx, (%rax,%rcx)
	jmp	.L171
	.p2align 4,,10
	.p2align 3
.L173:
	movq	%rdx, %rdi
	jmp	.L168
	.p2align 4,,10
	.p2align 3
.L184:
	leaq	48(%rbx), %r8
	movq	$0, 48(%rbx)
	movq	%r8, %r10
	jmp	.L163
.L185:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE4897:
	.size	_ZNSt10_HashtableImmSaImENSt8__detail9_IdentityESt8equal_toImESt4hashImENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS1_10_Hash_nodeImLb0EEEm, .-_ZNSt10_HashtableImmSaImENSt8__detail9_IdentityESt8equal_toImESt4hashImENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS1_10_Hash_nodeImLb0EEEm
	.section	.text._ZNSt10_HashtableImmSaImENSt8__detail9_IdentityESt8equal_toImESt4hashImENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE9_M_insertIRKmNS1_10_AllocNodeISaINS1_10_Hash_nodeImLb0EEEEEEEESt4pairINS1_14_Node_iteratorImLb1ELb0EEEbEOT_RKT0_St17integral_constantIbLb1EEm.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNSt10_HashtableImmSaImENSt8__detail9_IdentityESt8equal_toImESt4hashImENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE9_M_insertIRKmNS1_10_AllocNodeISaINS1_10_Hash_nodeImLb0EEEEEEEESt4pairINS1_14_Node_iteratorImLb1ELb0EEEbEOT_RKT0_St17integral_constantIbLb1EEm.constprop.0, @function
_ZNSt10_HashtableImmSaImENSt8__detail9_IdentityESt8equal_toImESt4hashImENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE9_M_insertIRKmNS1_10_AllocNodeISaINS1_10_Hash_nodeImLb0EEEEEEEESt4pairINS1_14_Node_iteratorImLb1ELb0EEEbEOT_RKT0_St17integral_constantIbLb1EEm.constprop.0:
.LFB5231:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	(%rsi), %r13
	movq	%rsi, %rbx
	movq	8(%rdi), %rdi
	movq	%r13, %rax
	divq	%rdi
	movq	(%r12), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r14
	testq	%rax, %rax
	je	.L187
	movq	(%rax), %rcx
	movq	8(%rcx), %r8
	jmp	.L189
	.p2align 4,,10
	.p2align 3
.L200:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L187
	movq	8(%rcx), %r8
	xorl	%edx, %edx
	movq	%r8, %rax
	divq	%rdi
	cmpq	%rdx, %r14
	jne	.L187
.L189:
	cmpq	%r8, %r13
	jne	.L200
	popq	%rbx
	movq	%rcx, %rax
	popq	%r12
	xorl	%edx, %edx
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L187:
	.cfi_restore_state
	movl	$16, %edi
	call	_Znwm@PLT
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	$0, (%rax)
	movq	%rax, %rcx
	movq	(%rbx), %rax
	movl	$1, %r8d
	movq	%rax, 8(%rcx)
	call	_ZNSt10_HashtableImmSaImENSt8__detail9_IdentityESt8equal_toImESt4hashImENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS1_10_Hash_nodeImLb0EEEm
	popq	%rbx
	movl	$1, %edx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5231:
	.size	_ZNSt10_HashtableImmSaImENSt8__detail9_IdentityESt8equal_toImESt4hashImENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE9_M_insertIRKmNS1_10_AllocNodeISaINS1_10_Hash_nodeImLb0EEEEEEEESt4pairINS1_14_Node_iteratorImLb1ELb0EEEbEOT_RKT0_St17integral_constantIbLb1EEm.constprop.0, .-_ZNSt10_HashtableImmSaImENSt8__detail9_IdentityESt8equal_toImESt4hashImENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE9_M_insertIRKmNS1_10_AllocNodeISaINS1_10_Hash_nodeImLb0EEEEEEEESt4pairINS1_14_Node_iteratorImLb1ELb0EEEbEOT_RKT0_St17integral_constantIbLb1EEm.constprop.0
	.section	.rodata._ZN2v84base21RandomNumberGenerator14NextSampleSlowEmmRKSt13unordered_setImSt4hashImESt8equal_toImESaImEE.str1.1,"aMS",@progbits,1
.LC7:
	.string	"max - excluded.size() >= n"
.LC9:
	.string	"x < result.size()"
	.section	.text._ZN2v84base21RandomNumberGenerator14NextSampleSlowEmmRKSt13unordered_setImSt4hashImESt8equal_toImESaImEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base21RandomNumberGenerator14NextSampleSlowEmmRKSt13unordered_setImSt4hashImESt8equal_toImESaImEE
	.type	_ZN2v84base21RandomNumberGenerator14NextSampleSlowEmmRKSt13unordered_setImSt4hashImESt8equal_toImESaImEE, @function
_ZN2v84base21RandomNumberGenerator14NextSampleSlowEmmRKSt13unordered_setImSt4hashImESt8equal_toImESaImEE:
.LFB4222:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movq	%rdx, %rsi
	subq	$120, %rsp
	movq	%rdi, -152(%rbp)
	subq	24(%r8), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	%rcx, %rsi
	jb	.L277
	leaq	-144(%rbp), %rax
	movq	%rdx, %r13
	movq	%rcx, %r14
	movq	%r8, %r15
	pxor	%xmm0, %xmm0
	movq	%rax, %rdi
	movq	$0, -128(%rbp)
	movq	%rax, -160(%rbp)
	movaps	%xmm0, -144(%rbp)
	call	_ZNSt6vectorImSaImEE7reserveEm
	movq	$0, -112(%rbp)
	movq	-136(%rbp), %r12
	testq	%r13, %r13
	je	.L211
	leaq	-112(%rbp), %rcx
	movq	%r12, %r11
	xorl	%esi, %esi
	movq	%rbx, %r12
	movq	%rcx, %rbx
	.p2align 4,,10
	.p2align 3
.L212:
	movq	8(%r15), %r8
	movq	%rsi, %rax
	xorl	%edx, %edx
	divq	%r8
	movq	(%r15), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %rcx
	testq	%rax, %rax
	je	.L204
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L204
	movq	8(%rdi), %r9
	xorl	%r10d, %r10d
	jmp	.L209
	.p2align 4,,10
	.p2align 3
.L205:
	testq	%r10, %r10
	jne	.L207
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L208
.L278:
	movq	8(%rdi), %r9
	xorl	%edx, %edx
	movq	%r9, %rax
	divq	%r8
	cmpq	%rdx, %rcx
	jne	.L208
.L209:
	cmpq	%r9, %rsi
	jne	.L205
	movq	(%rdi), %rdi
	addq	$1, %r10
	testq	%rdi, %rdi
	jne	.L278
	.p2align 4,,10
	.p2align 3
.L208:
	testq	%r10, %r10
	je	.L204
.L207:
	addq	$1, %rsi
	movq	%rsi, -112(%rbp)
	cmpq	%r13, %rsi
	jb	.L212
.L281:
	movq	%r12, %rbx
	movq	%r11, %r12
.L211:
	movq	%r13, %rcx
	movq	-144(%rbp), %r15
	movq	%r12, %rdx
	subq	%r14, %rcx
	cmpq	%r14, %rcx
	cmovb	%r14, %rcx
	subq	%r15, %rdx
	sarq	$3, %rdx
	cmpq	%rcx, %rdx
	je	.L213
	cmpq	%r14, %rdx
	jbe	.L213
	movsd	.LC4(%rip), %xmm3
	movsd	.LC8(%rip), %xmm2
	movabsq	$4607182418800017408, %rsi
	movabsq	$-9223372036854775808, %rdi
	jmp	.L222
	.p2align 4,,10
	.p2align 3
.L280:
	cvttsd2siq	%xmm0, %rax
	cmpq	%rdx, %rax
	jnb	.L279
.L269:
	leaq	(%r15,%rax,8), %rax
	movq	-8(%r12), %r8
	movq	(%rax), %rdx
	movq	%r8, (%rax)
	movq	%rdx, -8(%r12)
	movq	-136(%rbp), %rax
	movq	-144(%rbp), %r15
	leaq	-8(%rax), %r12
	movq	%r12, %rdx
	movq	%r12, -136(%rbp)
	subq	%r15, %rdx
	sarq	$3, %rdx
	cmpq	%rcx, %rdx
	je	.L213
	cmpq	%rdx, %r14
	jnb	.L213
.L222:
	movq	8(%rbx), %r8
	movq	16(%rbx), %rax
	movq	%r8, %r10
	movq	%rax, 8(%rbx)
	salq	$23, %r10
	xorq	%r8, %r10
	movq	%rax, %r8
	shrq	$26, %r8
	movq	%r10, %r9
	xorq	%rax, %r8
	shrq	$12, %rax
	shrq	$17, %r9
	xorq	%r10, %r8
	orq	%rsi, %rax
	xorq	%r9, %r8
	movq	%rax, %xmm0
	movq	%r8, 16(%rbx)
	subsd	%xmm3, %xmm0
	testq	%rdx, %rdx
	js	.L214
	pxor	%xmm1, %xmm1
	cvtsi2sdq	%rdx, %xmm1
.L215:
	mulsd	%xmm1, %xmm0
	comisd	%xmm2, %xmm0
	jb	.L280
	subsd	%xmm2, %xmm0
	cvttsd2siq	%xmm0, %rax
	xorq	%rdi, %rax
	cmpq	%rdx, %rax
	jb	.L269
.L279:
	leaq	.LC9(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L204:
	cmpq	%r11, -128(%rbp)
	je	.L210
	movq	%rsi, (%r11)
	movq	-136(%rbp), %rax
	movq	-112(%rbp), %rsi
	leaq	8(%rax), %r11
	addq	$1, %rsi
	movq	%r11, -136(%rbp)
	movq	%rsi, -112(%rbp)
	cmpq	%r13, %rsi
	jb	.L212
	jmp	.L281
	.p2align 4,,10
	.p2align 3
.L214:
	movq	%rdx, %rax
	movq	%rdx, %r8
	pxor	%xmm1, %xmm1
	shrq	%rax
	andl	$1, %r8d
	orq	%r8, %rax
	cvtsi2sdq	%rax, %xmm1
	addsd	%xmm1, %xmm1
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L210:
	movq	-160(%rbp), %rdi
	movq	%r11, %rsi
	movq	%rbx, %rdx
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJRKmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	movq	-112(%rbp), %rsi
	movq	-136(%rbp), %r11
	addq	$1, %rsi
	movq	%rsi, -112(%rbp)
	cmpq	%r13, %rsi
	jb	.L212
	jmp	.L281
	.p2align 4,,10
	.p2align 3
.L213:
	cmpq	%rdx, %r14
	jne	.L282
	movq	-152(%rbp), %rbx
	movq	-128(%rbp), %rax
	movq	%r15, %xmm0
	movq	%r12, %xmm4
	punpcklqdq	%xmm4, %xmm0
	movq	%rax, 16(%rbx)
	movups	%xmm0, (%rbx)
.L201:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L283
	movq	-152(%rbp), %rax
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L282:
	.cfi_restore_state
	leaq	-64(%rbp), %rbx
	movq	%rdx, -160(%rbp)
	fildq	-160(%rbp)
	movq	%rbx, -112(%rbp)
	movq	$1, -104(%rbp)
	movq	$0, -96(%rbp)
	movq	$0, -88(%rbp)
	movl	$0x3f800000, -80(%rbp)
	movq	$0, -72(%rbp)
	movq	$0, -64(%rbp)
	testq	%rdx, %rdx
	jns	.L224
	fadds	.LC11(%rip)
.L224:
	movsd	.LC8(%rip), %xmm1
	fstpl	-160(%rbp)
	movsd	-160(%rbp), %xmm0
	comisd	%xmm1, %xmm0
	jnb	.L225
	cvttsd2siq	%xmm0, %rsi
.L226:
	leaq	-80(%rbp), %rdi
	call	_ZNKSt8__detail20_Prime_rehash_policy11_M_next_bktEm@PLT
	movq	%rax, %r14
	cmpq	-104(%rbp), %rax
	jbe	.L227
	cmpq	$1, %rax
	je	.L284
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %r14
	ja	.L285
	leaq	0(,%r14,8), %rdx
	movq	%rdx, %rdi
	movq	%rdx, -160(%rbp)
	call	_Znwm@PLT
	movq	-160(%rbp), %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	movq	%rax, %rcx
.L229:
	movq	%rcx, -112(%rbp)
	movq	%r14, -104(%rbp)
.L227:
	leaq	-112(%rbp), %r14
	cmpq	%r15, %r12
	je	.L235
	.p2align 4,,10
	.p2align 3
.L234:
	movq	%r15, %rsi
	movq	%r14, %rdi
	addq	$8, %r15
	call	_ZNSt10_HashtableImmSaImENSt8__detail9_IdentityESt8equal_toImESt4hashImENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE9_M_insertIRKmNS1_10_AllocNodeISaINS1_10_Hash_nodeImLb0EEEEEEEESt4pairINS1_14_Node_iteratorImLb1ELb0EEEbEOT_RKT0_St17integral_constantIbLb1EEm.constprop.0
	cmpq	%r15, %r12
	jne	.L234
.L235:
	movq	-152(%rbp), %rdi
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	_ZN2v84baseL16ComplementSampleERKSt13unordered_setImSt4hashImESt8equal_toImESaImEEm
	movq	-96(%rbp), %r12
	testq	%r12, %r12
	je	.L232
	.p2align 4,,10
	.p2align 3
.L233:
	movq	%r12, %rdi
	movq	(%r12), %r12
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L233
.L232:
	movq	-104(%rbp), %rax
	movq	-112(%rbp), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	-112(%rbp), %rdi
	movq	$0, -88(%rbp)
	movq	$0, -96(%rbp)
	cmpq	%rbx, %rdi
	je	.L236
	call	_ZdlPv@PLT
.L236:
	movq	-144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L201
	call	_ZdlPv@PLT
	jmp	.L201
	.p2align 4,,10
	.p2align 3
.L277:
	leaq	.LC7(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L225:
	subsd	%xmm1, %xmm0
	cvttsd2siq	%xmm0, %rsi
	btcq	$63, %rsi
	jmp	.L226
.L284:
	movq	$0, -64(%rbp)
	movq	%rbx, %rcx
	jmp	.L229
.L283:
	call	__stack_chk_fail@PLT
.L285:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE4222:
	.size	_ZN2v84base21RandomNumberGenerator14NextSampleSlowEmmRKSt13unordered_setImSt4hashImESt8equal_toImESaImEE, .-_ZN2v84base21RandomNumberGenerator14NextSampleSlowEmmRKSt13unordered_setImSt4hashImESt8equal_toImESaImEE
	.section	.rodata._ZN2v84base21RandomNumberGenerator10NextSampleEmm.str1.1,"aMS",@progbits,1
.LC13:
	.string	"n <= max"
.LC14:
	.string	"x < max"
	.section	.rodata._ZN2v84base21RandomNumberGenerator10NextSampleEmm.str1.8,"aMS",@progbits,1
	.align 8
.LC15:
	.string	"cannot create std::vector larger than max_size()"
	.section	.text._ZN2v84base21RandomNumberGenerator10NextSampleEmm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base21RandomNumberGenerator10NextSampleEmm
	.type	_ZN2v84base21RandomNumberGenerator10NextSampleEmm, @function
_ZN2v84base21RandomNumberGenerator10NextSampleEmm:
.LFB4221:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -160(%rbp)
	movq	%rcx, -152(%rbp)
	movq	%fs:40, %rbx
	movq	%rbx, -56(%rbp)
	xorl	%ebx, %ebx
	cmpq	%rdx, %rcx
	ja	.L326
	cmpq	$0, -152(%rbp)
	je	.L327
	movq	%rdx, %rax
	movq	%rcx, %r14
	movl	$0x3f800000, -80(%rbp)
	movq	%rdx, %r15
	subq	%rcx, %rax
	movq	$1, -104(%rbp)
	leaq	-112(%rbp), %rbx
	cmpq	%rcx, %rax
	movq	%rax, -176(%rbp)
	cmovbe	%rax, %r14
	leaq	-64(%rbp), %rax
	movq	$0, -96(%rbp)
	movq	%rax, -168(%rbp)
	movq	%rax, -112(%rbp)
	movq	$0, -88(%rbp)
	movq	$0, -72(%rbp)
	movq	$0, -64(%rbp)
	testq	%r14, %r14
	je	.L290
	movq	%rsi, %r13
	testq	%rdx, %rdx
	js	.L291
	pxor	%xmm2, %xmm2
	cvtsi2sdq	%rdx, %xmm2
	movsd	%xmm2, -144(%rbp)
.L292:
	movq	%r15, -136(%rbp)
	xorl	%r12d, %r12d
	leaq	-120(%rbp), %rbx
	jmp	.L293
	.p2align 4,,10
	.p2align 3
.L330:
	cvttsd2siq	%xmm0, %rax
	movq	%rax, -120(%rbp)
	cmpq	-136(%rbp), %rax
	jnb	.L328
.L297:
	leaq	-112(%rbp), %r15
	movq	%rbx, %rsi
	addq	$1, %r12
	movq	%r15, %rdi
	call	_ZNSt10_HashtableImmSaImENSt8__detail9_IdentityESt8equal_toImESt4hashImENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE9_M_insertIRKmNS1_10_AllocNodeISaINS1_10_Hash_nodeImLb0EEEEEEEESt4pairINS1_14_Node_iteratorImLb1ELb0EEEbEOT_RKT0_St17integral_constantIbLb1EEm.constprop.0
	cmpq	-88(%rbp), %r14
	je	.L329
	movabsq	$-6148914691236517205, %rax
	mulq	%r12
	shrq	%rdx
	cmpq	%rdx, %r14
	jbe	.L294
.L293:
	movq	8(%r13), %rdx
	movq	16(%r13), %rax
	movabsq	$4607182418800017408, %rdi
	movq	%rdx, %rcx
	movq	%rax, 8(%r13)
	salq	$23, %rcx
	xorq	%rdx, %rcx
	movq	%rax, %rdx
	shrq	$26, %rdx
	movq	%rcx, %rsi
	xorq	%rax, %rdx
	shrq	$12, %rax
	orq	%rdi, %rax
	shrq	$17, %rsi
	xorq	%rcx, %rdx
	movq	%rax, %xmm0
	subsd	.LC4(%rip), %xmm0
	xorq	%rsi, %rdx
	mulsd	-144(%rbp), %xmm0
	movq	%rdx, 16(%r13)
	comisd	.LC8(%rip), %xmm0
	jb	.L330
	subsd	.LC8(%rip), %xmm0
	movabsq	$-9223372036854775808, %rsi
	cvttsd2siq	%xmm0, %rax
	xorq	%rsi, %rax
	movq	%rax, -120(%rbp)
	cmpq	-136(%rbp), %rax
	jb	.L297
.L328:
	leaq	.LC14(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L327:
	movq	$0, 16(%rdi)
	pxor	%xmm0, %xmm0
	movups	%xmm0, (%rdi)
.L286:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L331
	movq	-160(%rbp), %rax
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L291:
	.cfi_restore_state
	movq	%rdx, %rax
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rax
	orq	%rdx, %rax
	cvtsi2sdq	%rax, %xmm0
	addsd	%xmm0, %xmm0
	movsd	%xmm0, -144(%rbp)
	jmp	.L292
	.p2align 4,,10
	.p2align 3
.L326:
	leaq	.LC13(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L294:
	movq	%r15, %rbx
	movq	-136(%rbp), %r15
	movq	-152(%rbp), %rcx
	movq	%r13, %rsi
	movq	-160(%rbp), %rdi
	movq	%rbx, %r8
	movq	%r15, %rdx
	call	_ZN2v84base21RandomNumberGenerator14NextSampleSlowEmmRKSt13unordered_setImSt4hashImESt8equal_toImESaImEE
	movq	-96(%rbp), %rbx
.L300:
	testq	%rbx, %rbx
	je	.L308
	.p2align 4,,10
	.p2align 3
.L305:
	movq	%rbx, %rdi
	movq	(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L305
.L308:
	movq	-104(%rbp), %rax
	movq	-112(%rbp), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	$0, -88(%rbp)
	movq	-112(%rbp), %rdi
	movq	$0, -96(%rbp)
	cmpq	-168(%rbp), %rdi
	je	.L286
	call	_ZdlPv@PLT
	jmp	.L286
	.p2align 4,,10
	.p2align 3
.L329:
	movq	%r15, %rbx
	movq	-152(%rbp), %rsi
	movq	-136(%rbp), %r15
	cmpq	%rsi, -176(%rbp)
	jnb	.L332
.L290:
	movq	-160(%rbp), %rdi
	movq	%rbx, %rsi
	movq	%r15, %rdx
	call	_ZN2v84baseL16ComplementSampleERKSt13unordered_setImSt4hashImESt8equal_toImESaImEEm
	movq	-96(%rbp), %rbx
	jmp	.L300
	.p2align 4,,10
	.p2align 3
.L332:
	movq	-160(%rbp), %rax
	movq	-96(%rbp), %r12
	pxor	%xmm0, %xmm0
	movq	$0, 16(%rax)
	movups	%xmm0, (%rax)
	testq	%r12, %r12
	je	.L301
	movq	%r12, %rax
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L302:
	movq	(%rax), %rax
	addq	$1, %rdx
	testq	%rax, %rax
	jne	.L302
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	jg	.L333
	leaq	0(,%rdx,8), %r14
	movq	%r14, %rdi
	call	_Znwm@PLT
	movq	-160(%rbp), %rsi
	movq	-96(%rbp), %rbx
	leaq	(%rax,%r14), %rdx
	movq	%rax, (%rsi)
	movq	%rdx, 16(%rsi)
	.p2align 4,,10
	.p2align 3
.L304:
	movq	8(%r12), %rdx
	addq	$8, %rax
	movq	%rdx, -8(%rax)
	movq	(%r12), %r12
	testq	%r12, %r12
	jne	.L304
	movq	-160(%rbp), %rsi
	movq	%rax, 8(%rsi)
	jmp	.L300
.L301:
	movq	$0, (%rax)
	movq	$0, 8(%rax)
	jmp	.L308
.L331:
	call	__stack_chk_fail@PLT
.L333:
	leaq	.LC15(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE4221:
	.size	_ZN2v84base21RandomNumberGenerator10NextSampleEmm, .-_ZN2v84base21RandomNumberGenerator10NextSampleEmm
	.section	.bss._ZN2v84baseL14entropy_sourceE,"aw",@nobits
	.align 8
	.type	_ZN2v84baseL14entropy_sourceE, @object
	.size	_ZN2v84baseL14entropy_sourceE, 8
_ZN2v84baseL14entropy_sourceE:
	.zero	8
	.section	.bss._ZN2v84baseL13entropy_mutexE,"aw",@nobits
	.align 32
	.type	_ZN2v84baseL13entropy_mutexE, @object
	.size	_ZN2v84baseL13entropy_mutexE, 48
_ZN2v84baseL13entropy_mutexE:
	.zero	48
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC4:
	.long	0
	.long	1072693248
	.align 8
.LC8:
	.long	0
	.long	1138753536
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC11:
	.long	1602224128
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
