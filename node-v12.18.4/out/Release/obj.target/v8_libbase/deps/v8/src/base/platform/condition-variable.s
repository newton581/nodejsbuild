	.file	"condition-variable.cc"
	.text
	.section	.text._ZN2v84base17ConditionVariableC2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base17ConditionVariableC2Ev
	.type	_ZN2v84base17ConditionVariableC2Ev, @function
_ZN2v84base17ConditionVariableC2Ev:
.LFB3219:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-28(%rbp), %r12
	movq	%r12, %rdi
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	pthread_condattr_init@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	call	pthread_condattr_setclock@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	pthread_cond_init@PLT
	movq	%r12, %rdi
	call	pthread_condattr_destroy@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L5:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3219:
	.size	_ZN2v84base17ConditionVariableC2Ev, .-_ZN2v84base17ConditionVariableC2Ev
	.globl	_ZN2v84base17ConditionVariableC1Ev
	.set	_ZN2v84base17ConditionVariableC1Ev,_ZN2v84base17ConditionVariableC2Ev
	.section	.text._ZN2v84base17ConditionVariableD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base17ConditionVariableD2Ev
	.type	_ZN2v84base17ConditionVariableD2Ev, @function
_ZN2v84base17ConditionVariableD2Ev:
.LFB3222:
	.cfi_startproc
	endbr64
	jmp	pthread_cond_destroy@PLT
	.cfi_endproc
.LFE3222:
	.size	_ZN2v84base17ConditionVariableD2Ev, .-_ZN2v84base17ConditionVariableD2Ev
	.globl	_ZN2v84base17ConditionVariableD1Ev
	.set	_ZN2v84base17ConditionVariableD1Ev,_ZN2v84base17ConditionVariableD2Ev
	.section	.text._ZN2v84base17ConditionVariable9NotifyOneEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base17ConditionVariable9NotifyOneEv
	.type	_ZN2v84base17ConditionVariable9NotifyOneEv, @function
_ZN2v84base17ConditionVariable9NotifyOneEv:
.LFB3224:
	.cfi_startproc
	endbr64
	jmp	pthread_cond_signal@PLT
	.cfi_endproc
.LFE3224:
	.size	_ZN2v84base17ConditionVariable9NotifyOneEv, .-_ZN2v84base17ConditionVariable9NotifyOneEv
	.section	.text._ZN2v84base17ConditionVariable9NotifyAllEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base17ConditionVariable9NotifyAllEv
	.type	_ZN2v84base17ConditionVariable9NotifyAllEv, @function
_ZN2v84base17ConditionVariable9NotifyAllEv:
.LFB3225:
	.cfi_startproc
	endbr64
	jmp	pthread_cond_broadcast@PLT
	.cfi_endproc
.LFE3225:
	.size	_ZN2v84base17ConditionVariable9NotifyAllEv, .-_ZN2v84base17ConditionVariable9NotifyAllEv
	.section	.text._ZN2v84base17ConditionVariable4WaitEPNS0_5MutexE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base17ConditionVariable4WaitEPNS0_5MutexE
	.type	_ZN2v84base17ConditionVariable4WaitEPNS0_5MutexE, @function
_ZN2v84base17ConditionVariable4WaitEPNS0_5MutexE:
.LFB3226:
	.cfi_startproc
	endbr64
	jmp	pthread_cond_wait@PLT
	.cfi_endproc
.LFE3226:
	.size	_ZN2v84base17ConditionVariable4WaitEPNS0_5MutexE, .-_ZN2v84base17ConditionVariable4WaitEPNS0_5MutexE
	.section	.text._ZN2v84base17ConditionVariable7WaitForEPNS0_5MutexERKNS0_9TimeDeltaE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base17ConditionVariable7WaitForEPNS0_5MutexERKNS0_9TimeDeltaE
	.type	_ZN2v84base17ConditionVariable7WaitForEPNS0_5MutexERKNS0_9TimeDeltaE, @function
_ZN2v84base17ConditionVariable7WaitForEPNS0_5MutexERKNS0_9TimeDeltaE:
.LFB3227:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-64(%rbp), %r14
	movq	%rsi, %r13
	pushq	%r12
	movq	%r14, %rsi
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movl	$1, %edi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdx, %rbx
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	clock_gettime@PLT
	movq	-64(%rbp), %rdi
	movq	-56(%rbp), %rsi
	call	_ZN2v84base4Time12FromTimespecE8timespec@PLT
	movq	(%rbx), %rdi
	movq	%rax, %rsi
	call	_ZN2v84base4bits20SignedSaturatedAdd64Ell@PLT
	leaq	-72(%rbp), %rdi
	movq	%rax, -72(%rbp)
	call	_ZNK2v84base4Time10ToTimespecEv@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rdx, -56(%rbp)
	movq	%r14, %rdx
	movq	%rax, -64(%rbp)
	call	pthread_cond_timedwait@PLT
	cmpl	$110, %eax
	setne	%al
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L13
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L13:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3227:
	.size	_ZN2v84base17ConditionVariable7WaitForEPNS0_5MutexERKNS0_9TimeDeltaE, .-_ZN2v84base17ConditionVariable7WaitForEPNS0_5MutexERKNS0_9TimeDeltaE
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
