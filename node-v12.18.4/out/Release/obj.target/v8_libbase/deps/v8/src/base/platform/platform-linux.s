	.file	"platform-linux.cc"
	.text
	.section	.text._ZN2v84base2OS19CreateTimezoneCacheEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base2OS19CreateTimezoneCacheEv
	.type	_ZN2v84base2OS19CreateTimezoneCacheEv, @function
_ZN2v84base2OS19CreateTimezoneCacheEv:
.LFB3686:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$8, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Znwm@PLT
	leaq	16+_ZTVN2v84base25PosixDefaultTimezoneCacheE(%rip), %rdx
	movq	%rdx, (%rax)
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3686:
	.size	_ZN2v84base2OS19CreateTimezoneCacheEv, .-_ZN2v84base2OS19CreateTimezoneCacheEv
	.section	.rodata._ZN2v84base2OS18SignalCodeMovingGCEv.str1.1,"aMS",@progbits,1
.LC0:
	.string	"w+"
.LC1:
	.string	"Failed to open %s\n"
.LC2:
	.string	"Free(addr, size)"
.LC3:
	.string	"Check failed: %s."
	.section	.text._ZN2v84base2OS18SignalCodeMovingGCEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base2OS18SignalCodeMovingGCEv
	.type	_ZN2v84base2OS18SignalCodeMovingGCEv, @function
_ZN2v84base2OS18SignalCodeMovingGCEv:
.LFB3723:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$30, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	call	sysconf@PLT
	movq	%rax, %r13
	call	_ZN2v84base2OS17GetGCFakeMMapFileEv@PLT
	leaq	.LC0(%rip), %rsi
	movq	%rax, %rdi
	call	fopen@PLT
	testq	%rax, %rax
	je	.L8
	movq	%rax, %rdi
	movq	%rax, %r12
	call	fileno@PLT
	movl	%eax, %ebx
	call	_ZN2v84base2OS17GetRandomMmapAddrEv@PLT
	xorl	%r9d, %r9d
	movq	%r13, %rsi
	movl	%ebx, %r8d
	movq	%rax, %rdi
	movl	$2, %ecx
	movl	$5, %edx
	call	mmap@PLT
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN2v84base2OS4FreeEPvm@PLT
	testb	%al, %al
	je	.L9
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	fclose@PLT
	.p2align 4,,10
	.p2align 3
.L9:
	.cfi_restore_state
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
.L8:
	call	_ZN2v84base2OS17GetGCFakeMMapFileEv@PLT
	leaq	.LC1(%rip), %rdi
	movq	%rax, %rsi
	xorl	%eax, %eax
	call	_ZN2v84base2OS10PrintErrorEPKcz@PLT
	call	_ZN2v84base2OS5AbortEv@PLT
	.cfi_endproc
.LFE3723:
	.size	_ZN2v84base2OS18SignalCodeMovingGCEv, .-_ZN2v84base2OS18SignalCodeMovingGCEv
	.section	.text._ZN2v84base2OS22AdjustSchedulingParamsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base2OS22AdjustSchedulingParamsEv
	.type	_ZN2v84base2OS22AdjustSchedulingParamsEv, @function
_ZN2v84base2OS22AdjustSchedulingParamsEv:
.LFB3724:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3724:
	.size	_ZN2v84base2OS22AdjustSchedulingParamsEv, .-_ZN2v84base2OS22AdjustSchedulingParamsEv
	.section	.rodata._ZNSt6vectorIN2v84base2OS20SharedLibraryAddressESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_.str1.1,"aMS",@progbits,1
.LC4:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorIN2v84base2OS20SharedLibraryAddressESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v84base2OS20SharedLibraryAddressESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v84base2OS20SharedLibraryAddressESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.type	_ZNSt6vectorIN2v84base2OS20SharedLibraryAddressESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, @function
_ZNSt6vectorIN2v84base2OS20SharedLibraryAddressESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_:
.LFB4184:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	movabsq	$7905747460161236407, %rdx
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	8(%rdi), %rbx
	movq	(%rdi), %r14
	movq	%rdi, -80(%rbp)
	movabsq	$164703072086692425, %rdi
	movq	%rsi, -56(%rbp)
	movq	%rbx, %rax
	subq	%r14, %rax
	sarq	$3, %rax
	imulq	%rdx, %rax
	cmpq	%rdi, %rax
	je	.L39
	movq	%rsi, %rdx
	subq	%r14, %rdx
	testq	%rax, %rax
	je	.L31
	movabsq	$9223372036854775800, %rcx
	leaq	(%rax,%rax), %r8
	cmpq	%r8, %rax
	jbe	.L40
.L13:
	movq	%rcx, %rdi
	movq	%rdx, -88(%rbp)
	movq	%rcx, -72(%rbp)
	call	_Znwm@PLT
	movq	-72(%rbp), %rcx
	movq	-88(%rbp), %rdx
	movq	%rax, -64(%rbp)
	leaq	56(%rax), %r8
	addq	%rax, %rcx
	movq	%rcx, -72(%rbp)
.L30:
	movq	-64(%rbp), %rax
	movq	(%r12), %rcx
	addq	%rdx, %rax
	leaq	16(%rax), %rdx
	movq	%rdx, (%rax)
	leaq	16(%r12), %rdx
	cmpq	%rdx, %rcx
	je	.L41
	movq	%rcx, (%rax)
	movq	16(%r12), %rcx
	movq	%rcx, 16(%rax)
.L16:
	movq	8(%r12), %rcx
	movq	%rdx, (%r12)
	movdqu	32(%r12), %xmm6
	movq	48(%r12), %rdx
	movq	$0, 8(%r12)
	movq	%rcx, 8(%rax)
	movq	%rdx, 48(%rax)
	movups	%xmm6, 32(%rax)
	movq	-56(%rbp), %rax
	movb	$0, 16(%r12)
	cmpq	%r14, %rax
	je	.L17
	leaq	-56(%rax), %rdx
	movq	-64(%rbp), %r15
	leaq	16(%r14), %r13
	movabsq	$988218432520154551, %rcx
	subq	%r14, %rdx
	shrq	$3, %rdx
	imulq	%rcx, %rdx
	movabsq	$2305843009213693951, %rcx
	andq	%rcx, %rdx
	movq	%rdx, %rax
	movq	%rdx, -88(%rbp)
	leaq	0(,%rdx,8), %rdx
	subq	%rax, %rdx
	leaq	72(%r14,%rdx,8), %r12
	.p2align 4,,10
	.p2align 3
.L23:
	leaq	16(%r15), %rcx
	movq	%rcx, (%r15)
	movq	-16(%r13), %rcx
	cmpq	%r13, %rcx
	je	.L42
.L18:
	movq	%rcx, (%r15)
	movq	0(%r13), %rcx
	movq	%rcx, 16(%r15)
.L19:
	movq	-8(%r13), %rcx
	movq	%rcx, 8(%r15)
	movdqu	16(%r13), %xmm1
	movq	%r13, -16(%r13)
	movq	$0, -8(%r13)
	movb	$0, 0(%r13)
	movups	%xmm1, 32(%r15)
	movq	32(%r13), %rcx
	movq	%rcx, 48(%r15)
	movq	-16(%r13), %rdi
	cmpq	%r13, %rdi
	je	.L20
	call	_ZdlPv@PLT
	addq	$56, %r13
	addq	$56, %r15
	cmpq	%r13, %r12
	jne	.L23
.L21:
	movq	-88(%rbp), %rax
	movq	-64(%rbp), %rsi
	addq	$2, %rax
	leaq	0(,%rax,8), %rdx
	subq	%rax, %rdx
	leaq	(%rsi,%rdx,8), %r8
.L17:
	movq	-56(%rbp), %rax
	cmpq	%rbx, %rax
	je	.L24
	movq	%r8, %rdx
	.p2align 4,,10
	.p2align 3
.L28:
	leaq	16(%rdx), %rcx
	leaq	16(%rax), %rdi
	movq	%rcx, (%rdx)
	movq	(%rax), %rcx
	cmpq	%rdi, %rcx
	je	.L43
	movq	%rcx, (%rdx)
	movq	16(%rax), %rcx
	addq	$56, %rax
	addq	$56, %rdx
	movdqu	-24(%rax), %xmm2
	movq	%rcx, -40(%rdx)
	movq	-48(%rax), %rcx
	movups	%xmm2, -24(%rdx)
	movq	%rcx, -48(%rdx)
	movq	-8(%rax), %rcx
	movq	%rcx, -8(%rdx)
	cmpq	%rbx, %rax
	jne	.L28
.L26:
	movabsq	$988218432520154551, %rdx
	movq	%rbx, %rsi
	subq	-56(%rbp), %rsi
	leaq	-56(%rsi), %rax
	shrq	$3, %rax
	imulq	%rdx, %rax
	movabsq	$2305843009213693951, %rdx
	andq	%rdx, %rax
	addq	$1, %rax
	leaq	0(,%rax,8), %rdx
	subq	%rax, %rdx
	leaq	(%r8,%rdx,8), %r8
.L24:
	testq	%r14, %r14
	je	.L29
	movq	%r14, %rdi
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L29:
	movq	-64(%rbp), %xmm0
	movq	-80(%rbp), %rax
	movq	%r8, %xmm7
	movq	-72(%rbp), %rsi
	punpcklqdq	%xmm7, %xmm0
	movq	%rsi, 16(%rax)
	movups	%xmm0, (%rax)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	.cfi_restore_state
	addq	$56, %r13
	addq	$56, %r15
	cmpq	%r12, %r13
	je	.L21
	leaq	16(%r15), %rcx
	movq	%rcx, (%r15)
	movq	-16(%r13), %rcx
	cmpq	%r13, %rcx
	jne	.L18
.L42:
	movdqu	0(%r13), %xmm3
	movups	%xmm3, 16(%r15)
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L43:
	movq	8(%rax), %rcx
	movdqu	16(%rax), %xmm4
	addq	$56, %rax
	addq	$56, %rdx
	movdqu	-24(%rax), %xmm5
	movq	%rcx, -48(%rdx)
	movq	-8(%rax), %rcx
	movups	%xmm4, -40(%rdx)
	movups	%xmm5, -24(%rdx)
	movq	%rcx, -8(%rdx)
	cmpq	%rax, %rbx
	jne	.L28
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L40:
	testq	%r8, %r8
	jne	.L14
	movq	$0, -72(%rbp)
	movl	$56, %r8d
	movq	$0, -64(%rbp)
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L31:
	movl	$56, %ecx
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L41:
	movdqu	16(%r12), %xmm6
	movups	%xmm6, 16(%rax)
	jmp	.L16
.L14:
	cmpq	%rdi, %r8
	cmovbe	%r8, %rdi
	imulq	$56, %rdi, %rcx
	jmp	.L13
.L39:
	leaq	.LC4(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE4184:
	.size	_ZNSt6vectorIN2v84base2OS20SharedLibraryAddressESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, .-_ZNSt6vectorIN2v84base2OS20SharedLibraryAddressESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.section	.rodata._ZN2v84base2OS25GetSharedLibraryAddressesEv.str1.1,"aMS",@progbits,1
.LC5:
	.string	"r"
.LC6:
	.string	"/proc/self/maps"
.LC7:
	.string	"%lx-%lx"
.LC8:
	.string	" %c%c%c%c"
.LC9:
	.string	"%lx"
.LC10:
	.string	"%08lx-%08lx"
	.section	.rodata._ZN2v84base2OS25GetSharedLibraryAddressesEv.str1.8,"aMS",@progbits,1
	.align 8
.LC11:
	.string	"basic_string::_M_construct null not valid"
	.section	.text._ZN2v84base2OS25GetSharedLibraryAddressesEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base2OS25GetSharedLibraryAddressesEv
	.type	_ZN2v84base2OS25GetSharedLibraryAddressesEv, @function
_ZN2v84base2OS25GetSharedLibraryAddressesEv:
.LFB3704:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	leaq	.LC5(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$216, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, 16(%rdi)
	movups	%xmm0, (%rdi)
	leaq	.LC6(%rip), %rdi
	call	fopen@PLT
	testq	%rax, %rax
	je	.L44
	movl	$4097, %edi
	movq	%rax, %r12
	leaq	-168(%rbp), %r14
	call	malloc@PLT
	leaq	-176(%rbp), %r13
	leaq	-177(%rbp), %r15
	movq	%rax, -200(%rbp)
	.p2align 4,,10
	.p2align 3
.L77:
	xorl	%eax, %eax
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r12, %rdi
	leaq	.LC7(%rip), %rsi
	call	__isoc99_fscanf@PLT
	cmpl	$2, %eax
	jne	.L46
	xorl	%eax, %eax
	leaq	-179(%rbp), %rcx
	movq	%r15, %r9
	movq	%r12, %rdi
	leaq	-180(%rbp), %rdx
	leaq	-178(%rbp), %r8
	leaq	.LC8(%rip), %rsi
	call	__isoc99_fscanf@PLT
	cmpl	$4, %eax
	jne	.L46
	xorl	%eax, %eax
	leaq	-160(%rbp), %rdx
	leaq	.LC9(%rip), %rsi
	movq	%r12, %rdi
	call	__isoc99_fscanf@PLT
	cmpl	$1, %eax
	jne	.L46
	cmpb	$114, -180(%rbp)
	jne	.L75
	cmpb	$119, -179(%rbp)
	je	.L75
	cmpb	$120, -178(%rbp)
	je	.L50
	.p2align 4,,10
	.p2align 3
.L75:
	movq	%r12, %rdi
	call	getc@PLT
	cmpl	$-1, %eax
	je	.L46
	cmpl	$10, %eax
	jne	.L75
	cmpl	$-1, %eax
	jne	.L77
	.p2align 4,,10
	.p2align 3
.L46:
	movq	-200(%rbp), %rdi
	call	free@PLT
	movq	%r12, %rdi
	call	fclose@PLT
.L44:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L121
	leaq	-40(%rbp), %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L122:
	.cfi_restore_state
	cmpl	$10, %eax
	je	.L48
	cmpl	$47, %eax
	je	.L49
	cmpl	$91, %eax
	je	.L49
.L50:
	movq	%r12, %rdi
	call	getc@PLT
	cmpl	$-1, %eax
	jne	.L122
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L49:
	movq	%r12, %rsi
	movl	%eax, %edi
	call	ungetc@PLT
	movq	-200(%rbp), %rdi
	movq	%r12, %rdx
	movl	$4097, %esi
	call	fgets@PLT
	testq	%rax, %rax
	je	.L46
	movq	-200(%rbp), %rdx
.L53:
	movl	(%rdx), %ecx
	addq	$4, %rdx
	leal	-16843009(%rcx), %eax
	notl	%ecx
	andl	%ecx, %eax
	andl	$-2139062144, %eax
	je	.L53
	movl	%eax, %ecx
	shrl	$16, %ecx
	testl	$32896, %eax
	cmove	%ecx, %eax
	leaq	2(%rdx), %rcx
	cmove	%rcx, %rdx
	movl	%eax, %esi
	addb	%al, %sil
	movq	-176(%rbp), %rax
	sbbq	$3, %rdx
	subq	-160(%rbp), %rax
	movq	%rax, -216(%rbp)
	movq	%rax, -176(%rbp)
	movq	-168(%rbp), %rax
	movb	$0, -1(%rdx)
	movq	%rax, -224(%rbp)
	leaq	-128(%rbp), %rax
	movq	%rax, -208(%rbp)
	movq	%rax, -144(%rbp)
	movq	-200(%rbp), %rax
.L55:
	movl	(%rax), %ecx
	addq	$4, %rax
	leal	-16843009(%rcx), %edx
	notl	%ecx
	andl	%ecx, %edx
	andl	$-2139062144, %edx
	je	.L55
.L120:
	movl	%edx, %ecx
	shrl	$16, %ecx
	testl	$32896, %edx
	cmove	%ecx, %edx
	leaq	2(%rax), %rcx
	cmove	%rcx, %rax
	movl	%edx, %esi
	addb	%dl, %sil
	sbbq	$3, %rax
	subq	-200(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	%rax, %r8
	cmpq	$15, %rax
	ja	.L123
	cmpq	$1, %rax
	jne	.L61
	movq	-200(%rbp), %rax
	movzbl	(%rax), %eax
	movb	%al, -128(%rbp)
	movq	-144(%rbp), %rax
.L62:
	movq	%r8, -136(%rbp)
	leaq	-96(%rbp), %rcx
	movb	$0, (%rax,%r8)
	movq	-144(%rbp), %r9
	movq	-136(%rbp), %r8
	movq	%rcx, -112(%rbp)
	movq	%r9, %rax
	addq	%r8, %rax
	je	.L83
	testq	%r9, %r9
	je	.L63
.L83:
	movq	%r8, -152(%rbp)
	cmpq	$15, %r8
	ja	.L124
	cmpq	$1, %r8
	jne	.L67
	movzbl	(%r9), %eax
	movb	%al, -96(%rbp)
	movq	%rcx, %rax
.L68:
	movq	-216(%rbp), %xmm0
	movq	%r8, -104(%rbp)
	movb	$0, (%rax,%r8)
	movq	8(%rbx), %rsi
	movhps	-224(%rbp), %xmm0
	movq	$0, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	cmpq	16(%rbx), %rsi
	je	.L69
	leaq	16(%rsi), %rax
	movq	%rax, (%rsi)
	movq	-112(%rbp), %rax
	cmpq	%rcx, %rax
	je	.L125
	movq	%rax, (%rsi)
	movq	-96(%rbp), %rax
	movq	%rax, 16(%rsi)
.L71:
	movq	-104(%rbp), %rax
	movq	%rax, 8(%rsi)
	movq	-80(%rbp), %rax
	movq	%rax, 32(%rsi)
	movq	-72(%rbp), %rax
	movq	%rax, 40(%rsi)
	movq	-64(%rbp), %rax
	movq	%rax, 48(%rsi)
	addq	$56, 8(%rbx)
.L72:
	movq	-144(%rbp), %rdi
	cmpq	-208(%rbp), %rdi
	je	.L77
	call	_ZdlPv@PLT
	jmp	.L77
.L61:
	testq	%rax, %rax
	jne	.L126
	movq	-208(%rbp), %rax
	jmp	.L62
.L69:
	movq	%rbx, %rdi
	leaq	-112(%rbp), %rdx
	movq	%rcx, -216(%rbp)
	call	_ZNSt6vectorIN2v84base2OS20SharedLibraryAddressESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	movq	-112(%rbp), %rdi
	movq	-216(%rbp), %rcx
	cmpq	%rcx, %rdi
	je	.L72
	call	_ZdlPv@PLT
	jmp	.L72
.L67:
	testq	%r8, %r8
	jne	.L127
	movq	%rcx, %rax
	jmp	.L68
.L124:
	leaq	-112(%rbp), %rdi
	leaq	-152(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rcx, -248(%rbp)
	movq	%r8, -240(%rbp)
	movq	%r9, -232(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-232(%rbp), %r9
	movq	-240(%rbp), %r8
	movq	%rax, -112(%rbp)
	movq	%rax, %rdi
	movq	-152(%rbp), %rax
	movq	-248(%rbp), %rcx
	movq	%rax, -96(%rbp)
.L66:
	movq	%r8, %rdx
	movq	%r9, %rsi
	movq	%rcx, -232(%rbp)
	call	memcpy@PLT
	movq	-152(%rbp), %r8
	movq	-112(%rbp), %rax
	movq	-232(%rbp), %rcx
	jmp	.L68
.L123:
	leaq	-144(%rbp), %rdi
	leaq	-152(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rax, -232(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-232(%rbp), %r8
	movq	%rax, -144(%rbp)
	movq	%rax, %rdi
	movq	-152(%rbp), %rax
	movq	%rax, -128(%rbp)
.L60:
	movq	-200(%rbp), %rsi
	movq	%r8, %rdx
	call	memcpy@PLT
	movq	-152(%rbp), %r8
	movq	-144(%rbp), %rax
	jmp	.L62
.L63:
	leaq	.LC11(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L48:
	cmpl	$-1, %eax
	je	.L46
	cmpl	$91, %eax
	je	.L49
	cmpl	$47, %eax
	je	.L49
	subq	$8, %rsp
	movl	$1, %edx
	movl	$4097, %esi
	xorl	%eax, %eax
	pushq	-168(%rbp)
	movq	-176(%rbp), %r9
	leaq	.LC10(%rip), %r8
	movl	$4097, %ecx
	movq	-200(%rbp), %rdi
	call	__snprintf_chk@PLT
	movq	-176(%rbp), %rax
	movq	-168(%rbp), %rsi
	subq	-160(%rbp), %rax
	cmpq	$0, -200(%rbp)
	movq	%rax, -216(%rbp)
	movq	%rax, -176(%rbp)
	leaq	-128(%rbp), %rax
	movq	%rax, -208(%rbp)
	movq	%rax, -144(%rbp)
	popq	%rax
	movq	%rsi, -224(%rbp)
	popq	%rdx
	je	.L63
	movq	-200(%rbp), %rax
.L78:
	movl	(%rax), %ecx
	addq	$4, %rax
	leal	-16843009(%rcx), %edx
	notl	%ecx
	andl	%ecx, %edx
	andl	$-2139062144, %edx
	je	.L78
	jmp	.L120
.L125:
	movdqa	-96(%rbp), %xmm1
	movups	%xmm1, 16(%rsi)
	jmp	.L71
.L126:
	movq	-208(%rbp), %rdi
	jmp	.L60
.L127:
	movq	%rcx, %rdi
	jmp	.L66
.L121:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3704:
	.size	_ZN2v84base2OS25GetSharedLibraryAddressesEv, .-_ZN2v84base2OS25GetSharedLibraryAddressesEv
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
