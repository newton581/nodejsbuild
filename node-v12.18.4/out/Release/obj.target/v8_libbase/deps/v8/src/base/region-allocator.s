	.file	"region-allocator.cc"
	.text
	.section	.text._ZN2v84base15RegionAllocator10FindRegionEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base15RegionAllocator10FindRegionEm
	.type	_ZN2v84base15RegionAllocator10FindRegionEm, @function
_ZN2v84base15RegionAllocator10FindRegionEm:
.LFB3899:
	.cfi_startproc
	endbr64
	movq	%rsi, %rax
	leaq	64(%rdi), %r8
	subq	(%rdi), %rax
	cmpq	8(%rdi), %rax
	jnb	.L3
	movq	72(%rdi), %rax
	testq	%rax, %rax
	jne	.L5
.L3:
	movq	%r8, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	movq	%rax, %r8
	movq	16(%rax), %rax
.L7:
	testq	%rax, %rax
	je	.L3
.L5:
	movq	32(%rax), %rcx
	movq	8(%rcx), %rdx
	addq	(%rcx), %rdx
	cmpq	%rdx, %rsi
	jb	.L15
	movq	24(%rax), %rax
	jmp	.L7
	.cfi_endproc
.LFE3899:
	.size	_ZN2v84base15RegionAllocator10FindRegionEm, .-_ZN2v84base15RegionAllocator10FindRegionEm
	.section	.text._ZN2v84base15RegionAllocator18FreeListFindRegionEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base15RegionAllocator18FreeListFindRegionEm
	.type	_ZN2v84base15RegionAllocator18FreeListFindRegionEm, @function
_ZN2v84base15RegionAllocator18FreeListFindRegionEm:
.LFB3901:
	.cfi_startproc
	endbr64
	movq	120(%rdi), %rax
	leaq	112(%rdi), %r8
	testq	%rax, %rax
	je	.L16
	movq	%r8, %rcx
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L20:
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L19
.L18:
	movq	32(%rax), %rdx
	cmpq	8(%rdx), %rsi
	ja	.L20
	movq	%rax, %rcx
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L18
.L19:
	cmpq	%rcx, %r8
	je	.L16
	movq	32(%rcx), %rax
.L16:
	ret
	.cfi_endproc
.LFE3901:
	.size	_ZN2v84base15RegionAllocator18FreeListFindRegionEm, .-_ZN2v84base15RegionAllocator18FreeListFindRegionEm
	.section	.text._ZN2v84base15RegionAllocator20FreeListRemoveRegionEPNS1_6RegionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base15RegionAllocator20FreeListRemoveRegionEPNS1_6RegionE
	.type	_ZN2v84base15RegionAllocator20FreeListRemoveRegionEPNS1_6RegionE, @function
_ZN2v84base15RegionAllocator20FreeListRemoveRegionEPNS1_6RegionE:
.LFB3902:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	112(%rdi), %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	120(%rdi), %rax
	movq	8(%rsi), %rdx
	movq	%r8, %rdi
	testq	%rax, %rax
	jne	.L27
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L39:
	movq	%rax, %rdi
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L28
.L27:
	movq	32(%rax), %rcx
	cmpq	8(%rcx), %rdx
	je	.L29
	seta	%cl
.L30:
	testb	%cl, %cl
	je	.L39
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L27
.L28:
	cmpq	%rdi, %r8
	je	.L26
	movq	32(%rdi), %rax
	cmpq	8(%rax), %rdx
	je	.L33
	setb	%al
	testb	%al, %al
	cmovne	%r8, %rdi
.L26:
	subq	%rdx, 40(%rbx)
	movq	%r8, %rsi
	call	_ZSt28_Rb_tree_rebalance_for_erasePSt18_Rb_tree_node_baseRS_@PLT
	movq	%rax, %rdi
	call	_ZdlPv@PLT
	subq	$1, 144(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	.cfi_restore_state
	movq	(%rsi), %r9
	cmpq	%r9, (%rcx)
	setb	%cl
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L33:
	movq	(%rax), %rax
	cmpq	%rax, (%rsi)
	setb	%al
	testb	%al, %al
	cmovne	%r8, %rdi
	jmp	.L26
	.cfi_endproc
.LFE3902:
	.size	_ZN2v84base15RegionAllocator20FreeListRemoveRegionEPNS1_6RegionE, .-_ZN2v84base15RegionAllocator20FreeListRemoveRegionEPNS1_6RegionE
	.section	.text._ZN2v84base15RegionAllocator5MergeESt23_Rb_tree_const_iteratorIPNS1_6RegionEES5_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base15RegionAllocator5MergeESt23_Rb_tree_const_iteratorIPNS1_6RegionEES5_
	.type	_ZN2v84base15RegionAllocator5MergeESt23_Rb_tree_const_iteratorIPNS1_6RegionEES5_, @function
_ZN2v84base15RegionAllocator5MergeESt23_Rb_tree_const_iteratorIPNS1_6RegionEES5_:
.LFB3904:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	32(%rdx), %r12
	movq	%rdi, %rbx
	movq	%rdx, %rdi
	movq	32(%rsi), %rax
	leaq	64(%rbx), %rsi
	movq	8(%r12), %rdx
	addq	%rdx, 8(%rax)
	call	_ZSt28_Rb_tree_rebalance_for_erasePSt18_Rb_tree_node_baseRS_@PLT
	movq	%rax, %rdi
	call	_ZdlPv@PLT
	subq	$1, 96(%rbx)
	movq	%r12, %rdi
	popq	%rbx
	movl	$24, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE3904:
	.size	_ZN2v84base15RegionAllocator5MergeESt23_Rb_tree_const_iteratorIPNS1_6RegionEES5_, .-_ZN2v84base15RegionAllocator5MergeESt23_Rb_tree_const_iteratorIPNS1_6RegionEES5_
	.section	.text._ZN2v84base15RegionAllocator11CheckRegionEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base15RegionAllocator11CheckRegionEm
	.type	_ZN2v84base15RegionAllocator11CheckRegionEm, @function
_ZN2v84base15RegionAllocator11CheckRegionEm:
.LFB3909:
	.cfi_startproc
	endbr64
	movq	%rsi, %rax
	xorl	%r8d, %r8d
	subq	(%rdi), %rax
	cmpq	8(%rdi), %rax
	jnb	.L42
	movq	72(%rdi), %rax
	leaq	64(%rdi), %r9
	testq	%rax, %rax
	je	.L42
	movq	%r9, %rcx
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L53:
	movq	%rax, %rcx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L45
.L44:
	movq	32(%rax), %rdi
	movq	8(%rdi), %rdx
	addq	(%rdi), %rdx
	cmpq	%rdx, %rsi
	jb	.L53
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L44
.L45:
	cmpq	%rcx, %r9
	je	.L50
	movq	32(%rcx), %rax
	xorl	%r8d, %r8d
	cmpq	(%rax), %rsi
	jne	.L42
	cmpb	$0, 16(%rax)
	je	.L42
	movq	8(%rax), %r8
.L42:
	movq	%r8, %rax
	ret
.L50:
	xorl	%r8d, %r8d
	jmp	.L42
	.cfi_endproc
.LFE3909:
	.size	_ZN2v84base15RegionAllocator11CheckRegionEm, .-_ZN2v84base15RegionAllocator11CheckRegionEm
	.section	.rodata._ZN2v84base15RegionAllocator6IsFreeEmm.str1.1,"aMS",@progbits,1
.LC0:
	.string	"contains(address, size)"
.LC1:
	.string	"Check failed: %s."
	.section	.text._ZN2v84base15RegionAllocator6IsFreeEmm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base15RegionAllocator6IsFreeEmm
	.type	_ZN2v84base15RegionAllocator6IsFreeEmm, @function
_ZN2v84base15RegionAllocator6IsFreeEmm:
.LFB3910:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rcx
	movq	%rsi, %rax
	subq	(%rdi), %rax
	cmpq	%rax, %rcx
	jbe	.L55
	addq	%rdx, %rax
	cmpq	%rcx, %rax
	ja	.L55
	movq	72(%rdi), %rax
	leaq	64(%rdi), %r9
	testq	%rax, %rax
	je	.L63
	movq	%r9, %r8
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L67:
	movq	%rax, %r8
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L59
.L58:
	movq	32(%rax), %rdi
	movq	8(%rdi), %rcx
	addq	(%rdi), %rcx
	cmpq	%rcx, %rsi
	jb	.L67
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L58
.L59:
	cmpq	%r9, %r8
	je	.L63
	movq	32(%r8), %rcx
	movzbl	16(%rcx), %eax
	testb	%al, %al
	jne	.L64
	subq	(%rcx), %rsi
	movq	8(%rcx), %rcx
	cmpq	%rcx, %rsi
	jnb	.L54
	addq	%rdx, %rsi
	cmpq	%rsi, %rcx
	setnb	%al
	ret
	.p2align 4,,10
	.p2align 3
.L55:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L64:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	xorl	%eax, %eax
.L54:
	ret
	.p2align 4,,10
	.p2align 3
.L63:
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE3910:
	.size	_ZN2v84base15RegionAllocator6IsFreeEmm, .-_ZN2v84base15RegionAllocator6IsFreeEmm
	.section	.rodata._ZNK2v84base15RegionAllocator6Region5PrintERSo.str1.1,"aMS",@progbits,1
.LC2:
	.string	"used"
.LC3:
	.string	"free"
.LC4:
	.string	"["
.LC5:
	.string	", "
.LC6:
	.string	"), size: "
	.section	.text._ZNK2v84base15RegionAllocator6Region5PrintERSo,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v84base15RegionAllocator6Region5PrintERSo
	.type	_ZNK2v84base15RegionAllocator6Region5PrintERSo, @function
_ZNK2v84base15RegionAllocator6Region5PrintERSo:
.LFB3911:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	(%rsi), %rax
	movq	%rsi, %rbx
	movq	%rbx, %rdi
	movq	-24(%rax), %rcx
	addq	%rsi, %rcx
	leaq	.LC4(%rip), %rsi
	movl	24(%rcx), %r14d
	movl	$520, 24(%rcx)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r12), %rsi
	movq	%rbx, %rdi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$2, %edx
	leaq	.LC5(%rip), %rsi
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%r12), %rsi
	movq	%r13, %rdi
	addq	(%r12), %rsi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$9, %edx
	leaq	.LC6(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%r12), %rsi
	movq	%r13, %rdi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movq	%rbx, %rdi
	movl	$2, %edx
	leaq	.LC5(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 16(%r12)
	movq	%rbx, %rdi
	movl	$4, %edx
	leaq	.LC3(%rip), %rax
	leaq	.LC2(%rip), %rsi
	cmove	%rax, %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rax
	movq	-24(%rax), %rax
	movl	%r14d, 24(%rbx,%rax)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3911:
	.size	_ZNK2v84base15RegionAllocator6Region5PrintERSo, .-_ZNK2v84base15RegionAllocator6Region5PrintERSo
	.section	.rodata._ZNK2v84base15RegionAllocator5PrintERSo.str1.1,"aMS",@progbits,1
.LC7:
	.string	"RegionAllocator: ["
.LC8:
	.string	")"
.LC9:
	.string	"\nsize: "
.LC10:
	.string	"\nfree_size: "
.LC11:
	.string	"\npage_size: "
.LC12:
	.string	"\nall regions: "
.LC13:
	.string	"\n  "
.LC14:
	.string	"\nfree regions: "
.LC15:
	.string	"\n"
	.section	.text._ZNK2v84base15RegionAllocator5PrintERSo,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v84base15RegionAllocator5PrintERSo
	.type	_ZNK2v84base15RegionAllocator5PrintERSo, @function
_ZNK2v84base15RegionAllocator5PrintERSo:
.LFB3912:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$18, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movq	%rbx, %rdi
	subq	$24, %rsp
	movq	(%rsi), %rax
	movq	-24(%rax), %rcx
	addq	%rsi, %rcx
	leaq	.LC7(%rip), %rsi
	movq	%rcx, %rax
	movl	24(%rcx), %ecx
	movl	$520, 24(%rax)
	movl	%ecx, -60(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	0(%r13), %rsi
	movq	%rbx, %rdi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$2, %edx
	leaq	.LC5(%rip), %rsi
	movq	%rax, %r12
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%r13), %rsi
	movq	%r12, %rdi
	addq	0(%r13), %rsi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$1, %edx
	leaq	.LC8(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$7, %edx
	leaq	.LC9(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%r13), %rsi
	movq	%rbx, %rdi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$12, %edx
	leaq	.LC10(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	40(%r13), %rsi
	movq	%rbx, %rdi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$12, %edx
	leaq	.LC11(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	48(%r13), %rsi
	movq	%rbx, %rdi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$14, %edx
	leaq	.LC12(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	80(%r13), %r15
	leaq	64(%r13), %rax
	movq	%rax, -56(%rbp)
	cmpq	%rax, %r15
	je	.L73
	leaq	.LC13(%rip), %r14
	.p2align 4,,10
	.p2align 3
.L74:
	movq	32(%r15), %r12
	movl	$3, %edx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZNK2v84base15RegionAllocator6Region5PrintERSo
	movq	%r15, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r15
	cmpq	%rax, -56(%rbp)
	jne	.L74
.L73:
	movl	$15, %edx
	leaq	.LC14(%rip), %rsi
	movq	%rbx, %rdi
	addq	$112, %r13
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	16(%r13), %r12
	cmpq	%r12, %r13
	je	.L75
	leaq	.LC13(%rip), %r14
	.p2align 4,,10
	.p2align 3
.L76:
	movq	32(%r12), %r15
	movl	$3, %edx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZNK2v84base15RegionAllocator6Region5PrintERSo
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r12
	cmpq	%rax, %r13
	jne	.L76
.L75:
	movq	%rbx, %rdi
	movl	$1, %edx
	leaq	.LC15(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rax
	movl	-60(%rbp), %ecx
	movq	-24(%rax), %rax
	movl	%ecx, 24(%rbx,%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3912:
	.size	_ZNK2v84base15RegionAllocator5PrintERSo, .-_ZNK2v84base15RegionAllocator5PrintERSo
	.section	.text._ZNSt8_Rb_treeIPN2v84base15RegionAllocator6RegionES4_St9_IdentityIS4_ENS2_15AddressEndOrderESaIS4_EE8_M_eraseEPSt13_Rb_tree_nodeIS4_E,"axG",@progbits,_ZNSt8_Rb_treeIPN2v84base15RegionAllocator6RegionES4_St9_IdentityIS4_ENS2_15AddressEndOrderESaIS4_EE8_M_eraseEPSt13_Rb_tree_nodeIS4_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIPN2v84base15RegionAllocator6RegionES4_St9_IdentityIS4_ENS2_15AddressEndOrderESaIS4_EE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	.type	_ZNSt8_Rb_treeIPN2v84base15RegionAllocator6RegionES4_St9_IdentityIS4_ENS2_15AddressEndOrderESaIS4_EE8_M_eraseEPSt13_Rb_tree_nodeIS4_E, @function
_ZNSt8_Rb_treeIPN2v84base15RegionAllocator6RegionES4_St9_IdentityIS4_ENS2_15AddressEndOrderESaIS4_EE8_M_eraseEPSt13_Rb_tree_nodeIS4_E:
.LFB4363:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L88
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
.L82:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeIPN2v84base15RegionAllocator6RegionES4_St9_IdentityIS4_ENS2_15AddressEndOrderESaIS4_EE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	movq	%rbx, %rdi
	movq	16(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L82
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L88:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE4363:
	.size	_ZNSt8_Rb_treeIPN2v84base15RegionAllocator6RegionES4_St9_IdentityIS4_ENS2_15AddressEndOrderESaIS4_EE8_M_eraseEPSt13_Rb_tree_nodeIS4_E, .-_ZNSt8_Rb_treeIPN2v84base15RegionAllocator6RegionES4_St9_IdentityIS4_ENS2_15AddressEndOrderESaIS4_EE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	.section	.text._ZNSt8_Rb_treeIPN2v84base15RegionAllocator6RegionES4_St9_IdentityIS4_ENS2_16SizeAddressOrderESaIS4_EE8_M_eraseEPSt13_Rb_tree_nodeIS4_E,"axG",@progbits,_ZNSt8_Rb_treeIPN2v84base15RegionAllocator6RegionES4_St9_IdentityIS4_ENS2_16SizeAddressOrderESaIS4_EE8_M_eraseEPSt13_Rb_tree_nodeIS4_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIPN2v84base15RegionAllocator6RegionES4_St9_IdentityIS4_ENS2_16SizeAddressOrderESaIS4_EE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	.type	_ZNSt8_Rb_treeIPN2v84base15RegionAllocator6RegionES4_St9_IdentityIS4_ENS2_16SizeAddressOrderESaIS4_EE8_M_eraseEPSt13_Rb_tree_nodeIS4_E, @function
_ZNSt8_Rb_treeIPN2v84base15RegionAllocator6RegionES4_St9_IdentityIS4_ENS2_16SizeAddressOrderESaIS4_EE8_M_eraseEPSt13_Rb_tree_nodeIS4_E:
.LFB4368:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L99
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
.L93:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeIPN2v84base15RegionAllocator6RegionES4_St9_IdentityIS4_ENS2_16SizeAddressOrderESaIS4_EE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	movq	%rbx, %rdi
	movq	16(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L93
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L99:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE4368:
	.size	_ZNSt8_Rb_treeIPN2v84base15RegionAllocator6RegionES4_St9_IdentityIS4_ENS2_16SizeAddressOrderESaIS4_EE8_M_eraseEPSt13_Rb_tree_nodeIS4_E, .-_ZNSt8_Rb_treeIPN2v84base15RegionAllocator6RegionES4_St9_IdentityIS4_ENS2_16SizeAddressOrderESaIS4_EE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	.section	.text._ZN2v84base15RegionAllocatorD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base15RegionAllocatorD2Ev
	.type	_ZN2v84base15RegionAllocatorD2Ev, @function
_ZN2v84base15RegionAllocatorD2Ev:
.LFB3897:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	leaq	64(%rdi), %rbx
	subq	$8, %rsp
	movq	80(%rdi), %r12
	cmpq	%rbx, %r12
	je	.L103
	.p2align 4,,10
	.p2align 3
.L106:
	movq	32(%r12), %rdi
	testq	%rdi, %rdi
	je	.L104
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L104:
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r12
	cmpq	%rax, %rbx
	jne	.L106
.L103:
	movq	120(%r13), %rbx
	leaq	104(%r13), %r12
	testq	%rbx, %rbx
	je	.L107
.L108:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeIPN2v84base15RegionAllocator6RegionES4_St9_IdentityIS4_ENS2_16SizeAddressOrderESaIS4_EE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	movq	%rbx, %rdi
	movq	16(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L108
.L107:
	movq	72(%r13), %rbx
	leaq	56(%r13), %r12
	testq	%rbx, %rbx
	je	.L102
.L110:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeIPN2v84base15RegionAllocator6RegionES4_St9_IdentityIS4_ENS2_15AddressEndOrderESaIS4_EE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	movq	%rbx, %rdi
	movq	16(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L110
.L102:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3897:
	.size	_ZN2v84base15RegionAllocatorD2Ev, .-_ZN2v84base15RegionAllocatorD2Ev
	.globl	_ZN2v84base15RegionAllocatorD1Ev
	.set	_ZN2v84base15RegionAllocatorD1Ev,_ZN2v84base15RegionAllocatorD2Ev
	.section	.text._ZNSt8_Rb_treeIPN2v84base15RegionAllocator6RegionES4_St9_IdentityIS4_ENS2_15AddressEndOrderESaIS4_EE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_,"axG",@progbits,_ZNSt8_Rb_treeIPN2v84base15RegionAllocator6RegionES4_St9_IdentityIS4_ENS2_15AddressEndOrderESaIS4_EE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIPN2v84base15RegionAllocator6RegionES4_St9_IdentityIS4_ENS2_15AddressEndOrderESaIS4_EE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_
	.type	_ZNSt8_Rb_treeIPN2v84base15RegionAllocator6RegionES4_St9_IdentityIS4_ENS2_15AddressEndOrderESaIS4_EE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_, @function
_ZNSt8_Rb_treeIPN2v84base15RegionAllocator6RegionES4_St9_IdentityIS4_ENS2_15AddressEndOrderESaIS4_EE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_:
.LFB4370:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	8(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	16(%rdi), %r12
	testq	%r12, %r12
	je	.L121
	movq	(%rsi), %r13
	movq	8(%r13), %rdx
	addq	0(%r13), %rdx
	jmp	.L122
	.p2align 4,,10
	.p2align 3
.L142:
	movq	16(%r12), %rcx
	movl	$1, %esi
	testq	%rcx, %rcx
	je	.L123
.L143:
	movq	%rcx, %r12
.L122:
	movq	32(%r12), %rcx
	movq	8(%rcx), %rax
	addq	(%rcx), %rax
	cmpq	%rax, %rdx
	jb	.L142
	movq	24(%r12), %rcx
	xorl	%esi, %esi
	testq	%rcx, %rcx
	jne	.L143
.L123:
	testb	%sil, %sil
	jne	.L144
	cmpq	%rax, %rdx
	jbe	.L128
.L131:
	movl	$1, %r8d
	cmpq	%r12, %r15
	jne	.L145
.L129:
	movl	$40, %edi
	movl	%r8d, -52(%rbp)
	call	_Znwm@PLT
	movl	-52(%rbp), %r8d
	movq	%r12, %rdx
	movq	%r15, %rcx
	movq	%rax, %r13
	movq	(%r14), %rax
	movq	%r13, %rsi
	movl	%r8d, %edi
	movq	%rax, 32(%r13)
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 40(%rbx)
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	movl	$1, %edx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L144:
	.cfi_restore_state
	cmpq	%r12, 24(%rbx)
	je	.L131
.L132:
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	0(%r13), %rcx
	addq	8(%r13), %rcx
	movq	32(%rax), %rsi
	movq	8(%rsi), %rdx
	addq	(%rsi), %rdx
	cmpq	%rdx, %rcx
	ja	.L146
	movq	%rax, %r12
.L128:
	addq	$24, %rsp
	movq	%r12, %rax
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L146:
	.cfi_restore_state
	testq	%r12, %r12
	je	.L128
	movl	$1, %r8d
	cmpq	%r12, %r15
	je	.L129
.L145:
	movq	32(%r12), %rcx
	movq	8(%r13), %rdx
	xorl	%r8d, %r8d
	addq	0(%r13), %rdx
	movq	8(%rcx), %rax
	addq	(%rcx), %rax
	cmpq	%rax, %rdx
	setb	%r8b
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L121:
	movq	%r15, %r12
	cmpq	24(%rdi), %r15
	je	.L135
	movq	(%rsi), %r13
	jmp	.L132
	.p2align 4,,10
	.p2align 3
.L135:
	movl	$1, %r8d
	jmp	.L129
	.cfi_endproc
.LFE4370:
	.size	_ZNSt8_Rb_treeIPN2v84base15RegionAllocator6RegionES4_St9_IdentityIS4_ENS2_15AddressEndOrderESaIS4_EE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_, .-_ZNSt8_Rb_treeIPN2v84base15RegionAllocator6RegionES4_St9_IdentityIS4_ENS2_15AddressEndOrderESaIS4_EE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_
	.section	.text._ZNSt8_Rb_treeIPN2v84base15RegionAllocator6RegionES4_St9_IdentityIS4_ENS2_16SizeAddressOrderESaIS4_EE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_,"axG",@progbits,_ZNSt8_Rb_treeIPN2v84base15RegionAllocator6RegionES4_St9_IdentityIS4_ENS2_16SizeAddressOrderESaIS4_EE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIPN2v84base15RegionAllocator6RegionES4_St9_IdentityIS4_ENS2_16SizeAddressOrderESaIS4_EE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_
	.type	_ZNSt8_Rb_treeIPN2v84base15RegionAllocator6RegionES4_St9_IdentityIS4_ENS2_16SizeAddressOrderESaIS4_EE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_, @function
_ZNSt8_Rb_treeIPN2v84base15RegionAllocator6RegionES4_St9_IdentityIS4_ENS2_16SizeAddressOrderESaIS4_EE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_:
.LFB4396:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	8(%rdi), %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %rbx
	testq	%rbx, %rbx
	je	.L148
	movq	(%rsi), %r14
	movq	8(%r14), %rdi
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L178:
	movq	16(%rbx), %rax
	testq	%rax, %rax
	je	.L150
.L179:
	movq	%rax, %rbx
.L149:
	movq	32(%rbx), %rdx
	movq	8(%rdx), %rsi
	cmpq	%rsi, %rdi
	jne	.L175
	movq	(%rdx), %rax
	cmpq	%rax, (%r14)
.L175:
	setb	%cl
	testb	%cl, %cl
	jne	.L178
	movq	24(%rbx), %rax
	testq	%rax, %rax
	jne	.L179
.L150:
	movq	%rbx, %r8
	testb	%cl, %cl
	jne	.L180
.L156:
	cmpq	%rdi, %rsi
	jne	.L176
	movq	(%r14), %rax
	cmpq	%rax, (%rdx)
.L176:
	setb	%al
	testb	%al, %al
	je	.L159
	testq	%r8, %r8
	je	.L181
	movl	$1, %r9d
	cmpq	%r8, %r15
	jne	.L182
.L160:
	movl	$40, %edi
	movl	%r9d, -60(%rbp)
	movq	%r8, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r8
	movl	-60(%rbp), %r9d
	movq	%r15, %rcx
	movq	%rax, %rbx
	movq	0(%r13), %rax
	movq	%r8, %rdx
	movq	%rbx, %rsi
	movl	%r9d, %edi
	movq	%rax, 32(%rbx)
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 40(%r12)
	addq	$24, %rsp
	movq	%rbx, %rax
	movl	$1, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L181:
	.cfi_restore_state
	xorl	%ebx, %ebx
.L159:
	addq	$24, %rsp
	movq	%rbx, %rax
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L180:
	.cfi_restore_state
	cmpq	%rbx, 24(%r12)
	je	.L183
.L164:
	movq	%rbx, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	%rbx, %r8
	movq	8(%r14), %rdi
	movq	32(%rax), %rdx
	movq	%rax, %rbx
	movq	8(%rdx), %rsi
	jmp	.L156
	.p2align 4,,10
	.p2align 3
.L183:
	movq	%rbx, %r8
	movl	$1, %r9d
	cmpq	%r8, %r15
	je	.L160
.L182:
	movq	32(%r8), %rax
	movq	8(%rax), %rcx
	cmpq	%rcx, 8(%r14)
	jne	.L177
	movq	(%rax), %rax
	cmpq	%rax, (%r14)
.L177:
	setb	%r9b
	movzbl	%r9b, %r9d
	jmp	.L160
	.p2align 4,,10
	.p2align 3
.L148:
	cmpq	24(%rdi), %r15
	je	.L168
	movq	(%rsi), %r14
	movq	%r15, %rbx
	jmp	.L164
	.p2align 4,,10
	.p2align 3
.L168:
	movq	%r15, %r8
	movl	$1, %r9d
	jmp	.L160
	.cfi_endproc
.LFE4396:
	.size	_ZNSt8_Rb_treeIPN2v84base15RegionAllocator6RegionES4_St9_IdentityIS4_ENS2_16SizeAddressOrderESaIS4_EE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_, .-_ZNSt8_Rb_treeIPN2v84base15RegionAllocator6RegionES4_St9_IdentityIS4_ENS2_16SizeAddressOrderESaIS4_EE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_
	.section	.text._ZN2v84base15RegionAllocator17FreeListAddRegionEPNS1_6RegionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base15RegionAllocator17FreeListAddRegionEPNS1_6RegionE
	.type	_ZN2v84base15RegionAllocator17FreeListAddRegionEPNS1_6RegionE, @function
_ZN2v84base15RegionAllocator17FreeListAddRegionEPNS1_6RegionE:
.LFB3900:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	8(%rsi), %rax
	addq	%rax, 40(%rdi)
	movq	%rsi, -8(%rbp)
	addq	$104, %rdi
	leaq	-8(%rbp), %rsi
	call	_ZNSt8_Rb_treeIPN2v84base15RegionAllocator6RegionES4_St9_IdentityIS4_ENS2_16SizeAddressOrderESaIS4_EE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3900:
	.size	_ZN2v84base15RegionAllocator17FreeListAddRegionEPNS1_6RegionE, .-_ZN2v84base15RegionAllocator17FreeListAddRegionEPNS1_6RegionE
	.section	.rodata._ZN2v84base15RegionAllocatorC2Emmm.str1.1,"aMS",@progbits,1
.LC18:
	.string	"begin() < end()"
	.section	.rodata._ZN2v84base15RegionAllocatorC2Emmm.str1.8,"aMS",@progbits,1
	.align 8
.LC19:
	.string	"base::bits::IsPowerOfTwo(page_size_)"
	.section	.rodata._ZN2v84base15RegionAllocatorC2Emmm.str1.1
.LC20:
	.string	"IsAligned(size(), page_size_)"
	.section	.rodata._ZN2v84base15RegionAllocatorC2Emmm.str1.8
	.align 8
.LC21:
	.string	"IsAligned(begin(), page_size_)"
	.section	.text._ZN2v84base15RegionAllocatorC2Emmm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base15RegionAllocatorC2Emmm
	.type	_ZN2v84base15RegionAllocatorC2Emmm, @function
_ZN2v84base15RegionAllocatorC2Emmm:
.LFB3894:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %xmm2
	movq	%rsi, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movq	%rdx, %rdi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	%rdx, %rax
	xorl	%edx, %edx
	movb	$0, 16(%rbx)
	divq	%rcx
	movups	%xmm0, (%rbx)
	movq	%rax, 24(%rbx)
	testq	%rdi, %rdi
	js	.L187
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdi, %xmm0
.L188:
	mulsd	.LC16(%rip), %xmm0
	movsd	.LC17(%rip), %xmm1
	comisd	%xmm1, %xmm0
	jnb	.L189
	cvttsd2siq	%xmm0, %rax
	movq	%rax, 32(%rbx)
.L190:
	leaq	64(%rbx), %rax
	movq	$0, 40(%rbx)
	movq	%rax, 80(%rbx)
	movq	%rax, 88(%rbx)
	leaq	112(%rbx), %rax
	movq	%rax, 128(%rbx)
	movq	%rax, 136(%rbx)
	leaq	(%rsi,%rdi), %rax
	movq	%rcx, 48(%rbx)
	movl	$0, 64(%rbx)
	movq	$0, 72(%rbx)
	movq	$0, 96(%rbx)
	movl	$0, 112(%rbx)
	movq	$0, 120(%rbx)
	movq	$0, 144(%rbx)
	cmpq	%rax, %rsi
	jnb	.L202
	testq	%rcx, %rcx
	je	.L192
	leaq	-1(%rcx), %rax
	testq	%rcx, %rax
	jne	.L192
	testq	%rax, %rdi
	jne	.L203
	testq	%rax, %rsi
	jne	.L204
	movl	$24, %edi
	call	_Znwm@PLT
	movq	16(%rbx), %rdx
	movdqu	(%rbx), %xmm3
	leaq	-40(%rbp), %rsi
	leaq	56(%rbx), %rdi
	movq	%rax, -40(%rbp)
	movq	%rdx, 16(%rax)
	movups	%xmm3, (%rax)
	call	_ZNSt8_Rb_treeIPN2v84base15RegionAllocator6RegionES4_St9_IdentityIS4_ENS2_15AddressEndOrderESaIS4_EE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_
	movq	-40(%rbp), %rax
	leaq	-32(%rbp), %rsi
	leaq	104(%rbx), %rdi
	movq	%rax, -32(%rbp)
	movq	8(%rax), %rax
	addq	%rax, 40(%rbx)
	call	_ZNSt8_Rb_treeIPN2v84base15RegionAllocator6RegionES4_St9_IdentityIS4_ENS2_16SizeAddressOrderESaIS4_EE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L205
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L192:
	.cfi_restore_state
	leaq	.LC19(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L189:
	subsd	%xmm1, %xmm0
	cvttsd2siq	%xmm0, %rax
	movq	%rax, 32(%rbx)
	btcq	$63, 32(%rbx)
	jmp	.L190
	.p2align 4,,10
	.p2align 3
.L187:
	movq	%rdi, %rax
	movq	%rdi, %rdx
	pxor	%xmm0, %xmm0
	shrq	%rax
	andl	$1, %edx
	orq	%rdx, %rax
	cvtsi2sdq	%rax, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L188
	.p2align 4,,10
	.p2align 3
.L202:
	leaq	.LC18(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L203:
	leaq	.LC20(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L204:
	leaq	.LC21(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L205:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3894:
	.size	_ZN2v84base15RegionAllocatorC2Emmm, .-_ZN2v84base15RegionAllocatorC2Emmm
	.globl	_ZN2v84base15RegionAllocatorC1Emmm
	.set	_ZN2v84base15RegionAllocatorC1Emmm,_ZN2v84base15RegionAllocatorC2Emmm
	.section	.text._ZN2v84base15RegionAllocator5SplitEPNS1_6RegionEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base15RegionAllocator5SplitEPNS1_6RegionEm
	.type	_ZN2v84base15RegionAllocator5SplitEPNS1_6RegionEm, @function
_ZN2v84base15RegionAllocator5SplitEPNS1_6RegionEm:
.LFB3903:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movl	$24, %edi
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$56, %rsp
	movq	(%rsi), %r13
	movzbl	16(%rsi), %r15d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8(%rsi), %rax
	addq	%rdx, %r13
	subq	%rdx, %rax
	movq	%rax, -88(%rbp)
	call	_Znwm@PLT
	movq	%r13, %xmm0
	leaq	56(%r14), %r13
	movhps	-88(%rbp), %xmm0
	movb	%r15b, 16(%rax)
	movq	%rax, -72(%rbp)
	movups	%xmm0, (%rax)
	testb	%r15b, %r15b
	je	.L212
	movq	%r12, 8(%rbx)
	leaq	-72(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeIPN2v84base15RegionAllocator6RegionES4_St9_IdentityIS4_ENS2_15AddressEndOrderESaIS4_EE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_
.L208:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	movq	-72(%rbp), %rax
	jne	.L213
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L212:
	.cfi_restore_state
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v84base15RegionAllocator20FreeListRemoveRegionEPNS1_6RegionE
	movq	%r12, 8(%rbx)
	leaq	-72(%rbp), %rsi
	movq	%r13, %rdi
	leaq	104(%r14), %r12
	leaq	-64(%rbp), %r13
	call	_ZNSt8_Rb_treeIPN2v84base15RegionAllocator6RegionES4_St9_IdentityIS4_ENS2_15AddressEndOrderESaIS4_EE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_
	movq	8(%rbx), %rax
	addq	%rax, 40(%r14)
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rbx, -64(%rbp)
	call	_ZNSt8_Rb_treeIPN2v84base15RegionAllocator6RegionES4_St9_IdentityIS4_ENS2_16SizeAddressOrderESaIS4_EE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_
	movq	-72(%rbp), %rax
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	movq	8(%rax), %rax
	addq	%rax, 40(%r14)
	call	_ZNSt8_Rb_treeIPN2v84base15RegionAllocator6RegionES4_St9_IdentityIS4_ENS2_16SizeAddressOrderESaIS4_EE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_
	jmp	.L208
.L213:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3903:
	.size	_ZN2v84base15RegionAllocator5SplitEPNS1_6RegionEm, .-_ZN2v84base15RegionAllocator5SplitEPNS1_6RegionEm
	.section	.text._ZN2v84base15RegionAllocator14AllocateRegionEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base15RegionAllocator14AllocateRegionEm
	.type	_ZN2v84base15RegionAllocator14AllocateRegionEm, @function
_ZN2v84base15RegionAllocator14AllocateRegionEm:
.LFB3905:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	120(%rdi), %rax
	testq	%rax, %rax
	je	.L223
	addq	$112, %rdi
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	jmp	.L216
	.p2align 4,,10
	.p2align 3
.L225:
	movq	%rax, %rsi
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L217
.L216:
	movq	32(%rax), %rcx
	cmpq	8(%rcx), %rdx
	jbe	.L225
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L216
.L217:
	cmpq	%rsi, %rdi
	je	.L223
	movq	32(%rsi), %rbx
	testq	%rbx, %rbx
	je	.L223
	cmpq	8(%rbx), %rdx
	jne	.L226
.L220:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v84base15RegionAllocator20FreeListRemoveRegionEPNS1_6RegionE
	movb	$1, 16(%rbx)
	movq	(%rbx), %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L226:
	.cfi_restore_state
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v84base15RegionAllocator5SplitEPNS1_6RegionEm
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L223:
	popq	%rbx
	movq	$-1, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3905:
	.size	_ZN2v84base15RegionAllocator14AllocateRegionEm, .-_ZN2v84base15RegionAllocator14AllocateRegionEm
	.section	.text._ZN2v84base15RegionAllocator16AllocateRegionAtEmm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base15RegionAllocator16AllocateRegionAtEmm
	.type	_ZN2v84base15RegionAllocator16AllocateRegionAtEmm, @function
_ZN2v84base15RegionAllocator16AllocateRegionAtEmm:
.LFB3907:
	.cfi_startproc
	endbr64
	movq	%rsi, %rax
	xorl	%r8d, %r8d
	subq	(%rdi), %rax
	cmpq	%rax, 8(%rdi)
	jbe	.L240
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	64(%rdi), %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	72(%rdi), %rax
	testq	%rax, %rax
	je	.L227
	movq	%rdx, %r14
	movq	%r9, %rcx
	jmp	.L229
	.p2align 4,,10
	.p2align 3
.L243:
	movq	%rax, %rcx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L230
.L229:
	movq	32(%rax), %rdi
	movq	8(%rdi), %rdx
	addq	(%rdi), %rdx
	cmpq	%rdx, %rsi
	jb	.L243
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L229
.L230:
	cmpq	%rcx, %r9
	je	.L238
	movq	32(%rcx), %r13
	movzbl	16(%r13), %r8d
	testb	%r8b, %r8b
	jne	.L238
	movq	0(%r13), %rdx
	movq	8(%r13), %rax
	leaq	(%rsi,%r14), %rbx
	addq	%rdx, %rax
	cmpq	%rax, %rbx
	ja	.L227
	cmpq	%rdx, %rsi
	jne	.L244
	cmpq	%rax, %rbx
	jne	.L245
.L234:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v84base15RegionAllocator20FreeListRemoveRegionEPNS1_6RegionE
	movb	$1, 16(%r13)
	movl	$1, %r8d
.L227:
	popq	%rbx
	movl	%r8d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L240:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L238:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	xorl	%r8d, %r8d
	popq	%rbx
	popq	%r12
	movl	%r8d, %eax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L245:
	.cfi_restore_state
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v84base15RegionAllocator5SplitEPNS1_6RegionEm
	jmp	.L234
	.p2align 4,,10
	.p2align 3
.L244:
	subq	%rdx, %rsi
	movq	%r12, %rdi
	movq	%rsi, %rdx
	movq	%r13, %rsi
	call	_ZN2v84base15RegionAllocator5SplitEPNS1_6RegionEm
	movq	%rax, %r13
	movq	8(%rax), %rax
	addq	0(%r13), %rax
	cmpq	%rax, %rbx
	je	.L234
	jmp	.L245
	.cfi_endproc
.LFE3907:
	.size	_ZN2v84base15RegionAllocator16AllocateRegionAtEmm, .-_ZN2v84base15RegionAllocator16AllocateRegionAtEmm
	.section	.text._ZN2v84base15RegionAllocator14AllocateRegionEPNS0_21RandomNumberGeneratorEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base15RegionAllocator14AllocateRegionEPNS0_21RandomNumberGeneratorEm
	.type	_ZN2v84base15RegionAllocator14AllocateRegionEPNS0_21RandomNumberGeneratorEm, @function
_ZN2v84base15RegionAllocator14AllocateRegionEPNS0_21RandomNumberGeneratorEm:
.LFB3906:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	40(%rdi), %rax
	cmpq	%rax, 32(%rdi)
	ja	.L247
	movq	$0, -64(%rbp)
	movq	%rsi, %r15
	leaq	-64(%rbp), %rbx
	movl	$3, -68(%rbp)
.L250:
	movl	$8, %edx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v84base21RandomNumberGenerator9NextBytesEPvm@PLT
	movq	-64(%rbp), %rax
	xorl	%edx, %edx
	movq	%r12, %rdi
	divq	24(%r12)
	imulq	48(%r12), %rdx
	addq	(%r12), %rdx
	movq	%rdx, %r13
	movq	%r14, %rdx
	movq	%r13, %rsi
	call	_ZN2v84base15RegionAllocator16AllocateRegionAtEmm
	testb	%al, %al
	jne	.L246
	subl	$1, -68(%rbp)
	jne	.L250
.L247:
	movq	120(%r12), %rax
	leaq	112(%r12), %rsi
	testq	%rax, %rax
	je	.L259
	movq	%rsi, %rdx
	jmp	.L251
	.p2align 4,,10
	.p2align 3
.L262:
	movq	%rax, %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L252
.L251:
	movq	32(%rax), %rcx
	cmpq	8(%rcx), %r14
	jbe	.L262
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L251
.L252:
	cmpq	%rdx, %rsi
	je	.L259
	movq	32(%rdx), %rbx
	testq	%rbx, %rbx
	je	.L259
	cmpq	8(%rbx), %r14
	jne	.L263
.L255:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v84base15RegionAllocator20FreeListRemoveRegionEPNS1_6RegionE
	movb	$1, 16(%rbx)
	movq	(%rbx), %r13
.L246:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L264
	addq	$40, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L263:
	.cfi_restore_state
	movq	%r14, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v84base15RegionAllocator5SplitEPNS1_6RegionEm
	jmp	.L255
	.p2align 4,,10
	.p2align 3
.L259:
	movq	$-1, %r13
	jmp	.L246
.L264:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3906:
	.size	_ZN2v84base15RegionAllocator14AllocateRegionEPNS0_21RandomNumberGeneratorEm, .-_ZN2v84base15RegionAllocator14AllocateRegionEPNS0_21RandomNumberGeneratorEm
	.section	.text._ZN2v84base15RegionAllocator10TrimRegionEmm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base15RegionAllocator10TrimRegionEmm
	.type	_ZN2v84base15RegionAllocator10TrimRegionEmm, @function
_ZN2v84base15RegionAllocator10TrimRegionEmm:
.LFB3908:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rsi, %rax
	subq	(%rdi), %rax
	cmpq	%rax, 8(%rdi)
	jbe	.L265
	movq	72(%rdi), %rax
	movq	%rdi, %rbx
	leaq	64(%rdi), %r8
	testq	%rax, %rax
	je	.L265
	movq	%rdx, %r14
	movq	%r8, %r12
	jmp	.L267
	.p2align 4,,10
	.p2align 3
.L288:
	movq	%rax, %r12
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L268
.L267:
	movq	32(%rax), %rdx
	movq	8(%rdx), %rcx
	addq	(%rdx), %rcx
	cmpq	%rcx, %rsi
	jb	.L288
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L267
.L268:
	cmpq	%r12, %r8
	je	.L282
	movq	32(%r12), %r13
	xorl	%r15d, %r15d
	movq	0(%r13), %rax
	cmpq	%rax, %rsi
	jne	.L265
	cmpb	$0, 16(%r13)
	je	.L265
	testq	%r14, %r14
	jne	.L289
	movb	$0, 16(%r13)
	movq	8(%r13), %r15
	movq	(%rbx), %rdx
	movq	8(%rbx), %rcx
	leaq	(%r15,%rax), %rsi
	addq	%rdx, %rcx
	cmpq	%rcx, %rsi
	je	.L278
.L277:
	movq	%r12, %rdi
	movq	%r8, -72(%rbp)
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	-72(%rbp), %r8
	movq	32(%rax), %rsi
	cmpb	$0, 16(%rsi)
	je	.L290
.L273:
	testq	%r14, %r14
	je	.L274
.L287:
	movq	8(%r13), %rax
.L272:
	addq	%rax, 40(%rbx)
	leaq	-64(%rbp), %rsi
	leaq	104(%rbx), %rdi
	movq	%r13, -64(%rbp)
	call	_ZNSt8_Rb_treeIPN2v84base15RegionAllocator6RegionES4_St9_IdentityIS4_ENS2_16SizeAddressOrderESaIS4_EE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_
	.p2align 4,,10
	.p2align 3
.L265:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L291
	addq	$40, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L289:
	.cfi_restore_state
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	call	_ZN2v84base15RegionAllocator5SplitEPNS1_6RegionEm
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	8(%r13), %r15
	movq	0(%r13), %rcx
	movb	$0, 16(%r13)
	movq	8(%rbx), %rdx
	addq	(%rbx), %rdx
	movq	%rax, %r12
	addq	%r15, %rcx
	movq	-72(%rbp), %r8
	movq	%r15, %rax
	cmpq	%rdx, %rcx
	je	.L272
	jmp	.L277
	.p2align 4,,10
	.p2align 3
.L274:
	movq	0(%r13), %rax
	movq	(%rbx), %rdx
.L278:
	cmpq	%rax, %rdx
	je	.L287
	movq	%r12, %rdi
	movq	%r8, -72(%rbp)
	call	_ZSt18_Rb_tree_decrementPKSt18_Rb_tree_node_base@PLT
	movq	-72(%rbp), %r8
	movq	32(%rax), %rsi
	movq	%rax, %r14
	cmpb	$0, 16(%rsi)
	jne	.L287
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	call	_ZN2v84base15RegionAllocator20FreeListRemoveRegionEPNS1_6RegionE
	movq	32(%r14), %rax
	movq	-72(%rbp), %r8
	movq	%r12, %rdi
	movq	32(%r12), %r13
	movq	%r8, %rsi
	movq	8(%r13), %rdx
	addq	%rdx, 8(%rax)
	call	_ZSt28_Rb_tree_rebalance_for_erasePSt18_Rb_tree_node_baseRS_@PLT
	movq	%rax, %rdi
	call	_ZdlPv@PLT
	subq	$1, 96(%rbx)
	movq	%r13, %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
	movq	32(%r14), %r13
	jmp	.L287
	.p2align 4,,10
	.p2align 3
.L290:
	movq	%rbx, %rdi
	movq	%rax, -80(%rbp)
	call	_ZN2v84base15RegionAllocator20FreeListRemoveRegionEPNS1_6RegionE
	movq	-80(%rbp), %r9
	movq	32(%r12), %rax
	movq	-72(%rbp), %r8
	movq	32(%r9), %r10
	movq	%r9, %rdi
	movq	%r8, %rsi
	movq	8(%r10), %rdx
	addq	%rdx, 8(%rax)
	movq	%r10, -80(%rbp)
	call	_ZSt28_Rb_tree_rebalance_for_erasePSt18_Rb_tree_node_baseRS_@PLT
	movq	%rax, %rdi
	call	_ZdlPv@PLT
	subq	$1, 96(%rbx)
	movq	-80(%rbp), %r10
	movl	$24, %esi
	movq	%r10, %rdi
	call	_ZdlPvm@PLT
	movq	-72(%rbp), %r8
	jmp	.L273
.L291:
	call	__stack_chk_fail@PLT
.L282:
	xorl	%r15d, %r15d
	jmp	.L265
	.cfi_endproc
.LFE3908:
	.size	_ZN2v84base15RegionAllocator10TrimRegionEmm, .-_ZN2v84base15RegionAllocator10TrimRegionEmm
	.section	.text.startup._GLOBAL__sub_I__ZN2v84base15RegionAllocatorC2Emmm,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v84base15RegionAllocatorC2Emmm, @function
_GLOBAL__sub_I__ZN2v84base15RegionAllocatorC2Emmm:
.LFB4679:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE4679:
	.size	_GLOBAL__sub_I__ZN2v84base15RegionAllocatorC2Emmm, .-_GLOBAL__sub_I__ZN2v84base15RegionAllocatorC2Emmm
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v84base15RegionAllocatorC2Emmm
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC16:
	.long	2576980378
	.long	1071225241
	.align 8
.LC17:
	.long	0
	.long	1138753536
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
