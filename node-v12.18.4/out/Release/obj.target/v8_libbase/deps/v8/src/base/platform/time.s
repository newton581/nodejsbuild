	.file	"time.cc"
	.text
	.section	.text._ZNK2v84base9TimeDelta6InDaysEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v84base9TimeDelta6InDaysEv
	.type	_ZNK2v84base9TimeDelta6InDaysEv, @function
_ZNK2v84base9TimeDelta6InDaysEv:
.LFB3601:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rcx
	movl	$2147483647, %eax
	movabsq	$9223372036854775807, %rdx
	cmpq	%rdx, %rcx
	je	.L1
	movabsq	$3667970486771497111, %rdx
	movq	%rcx, %rax
	sarq	$63, %rcx
	imulq	%rdx
	movq	%rdx, %rax
	sarq	$34, %rax
	subl	%ecx, %eax
.L1:
	ret
	.cfi_endproc
.LFE3601:
	.size	_ZNK2v84base9TimeDelta6InDaysEv, .-_ZNK2v84base9TimeDelta6InDaysEv
	.section	.text._ZNK2v84base9TimeDelta7InHoursEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v84base9TimeDelta7InHoursEv
	.type	_ZNK2v84base9TimeDelta7InHoursEv, @function
_ZNK2v84base9TimeDelta7InHoursEv:
.LFB3602:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rcx
	movl	$2147483647, %eax
	movabsq	$9223372036854775807, %rdx
	cmpq	%rdx, %rcx
	je	.L5
	movabsq	$-7442832613395060283, %rdx
	movq	%rcx, %rax
	imulq	%rdx
	leaq	(%rdx,%rcx), %rax
	sarq	$63, %rcx
	sarq	$31, %rax
	subl	%ecx, %eax
.L5:
	ret
	.cfi_endproc
.LFE3602:
	.size	_ZNK2v84base9TimeDelta7InHoursEv, .-_ZNK2v84base9TimeDelta7InHoursEv
	.section	.text._ZNK2v84base9TimeDelta9InMinutesEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v84base9TimeDelta9InMinutesEv
	.type	_ZNK2v84base9TimeDelta9InMinutesEv, @function
_ZNK2v84base9TimeDelta9InMinutesEv:
.LFB3603:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rcx
	movl	$2147483647, %eax
	movabsq	$9223372036854775807, %rdx
	cmpq	%rdx, %rcx
	je	.L8
	movabsq	$-8130577079664715991, %rdx
	movq	%rcx, %rax
	imulq	%rdx
	leaq	(%rdx,%rcx), %rax
	sarq	$63, %rcx
	sarq	$25, %rax
	subl	%ecx, %eax
.L8:
	ret
	.cfi_endproc
.LFE3603:
	.size	_ZNK2v84base9TimeDelta9InMinutesEv, .-_ZNK2v84base9TimeDelta9InMinutesEv
	.section	.text._ZNK2v84base9TimeDelta10InSecondsFEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v84base9TimeDelta10InSecondsFEv
	.type	_ZNK2v84base9TimeDelta10InSecondsFEv, @function
_ZNK2v84base9TimeDelta10InSecondsFEv:
.LFB3604:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movsd	.LC0(%rip), %xmm0
	movabsq	$9223372036854775807, %rdx
	cmpq	%rdx, %rax
	je	.L11
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
	divsd	.LC1(%rip), %xmm0
.L11:
	ret
	.cfi_endproc
.LFE3604:
	.size	_ZNK2v84base9TimeDelta10InSecondsFEv, .-_ZNK2v84base9TimeDelta10InSecondsFEv
	.section	.text._ZNK2v84base9TimeDelta9InSecondsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v84base9TimeDelta9InSecondsEv
	.type	_ZNK2v84base9TimeDelta9InSecondsEv, @function
_ZNK2v84base9TimeDelta9InSecondsEv:
.LFB3605:
	.cfi_startproc
	endbr64
	movabsq	$9223372036854775807, %rax
	movq	(%rdi), %r8
	cmpq	%rax, %r8
	je	.L14
	movabsq	$4835703278458516699, %rdx
	movq	%r8, %rax
	sarq	$63, %r8
	imulq	%rdx
	sarq	$18, %rdx
	subq	%r8, %rdx
	movq	%rdx, %r8
.L14:
	movq	%r8, %rax
	ret
	.cfi_endproc
.LFE3605:
	.size	_ZNK2v84base9TimeDelta9InSecondsEv, .-_ZNK2v84base9TimeDelta9InSecondsEv
	.section	.text._ZNK2v84base9TimeDelta15InMillisecondsFEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v84base9TimeDelta15InMillisecondsFEv
	.type	_ZNK2v84base9TimeDelta15InMillisecondsFEv, @function
_ZNK2v84base9TimeDelta15InMillisecondsFEv:
.LFB3606:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movsd	.LC0(%rip), %xmm0
	movabsq	$9223372036854775807, %rdx
	cmpq	%rdx, %rax
	je	.L16
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
	divsd	.LC2(%rip), %xmm0
.L16:
	ret
	.cfi_endproc
.LFE3606:
	.size	_ZNK2v84base9TimeDelta15InMillisecondsFEv, .-_ZNK2v84base9TimeDelta15InMillisecondsFEv
	.section	.text._ZNK2v84base9TimeDelta14InMillisecondsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v84base9TimeDelta14InMillisecondsEv
	.type	_ZNK2v84base9TimeDelta14InMillisecondsEv, @function
_ZNK2v84base9TimeDelta14InMillisecondsEv:
.LFB3607:
	.cfi_startproc
	endbr64
	movabsq	$9223372036854775807, %rax
	movq	(%rdi), %r8
	cmpq	%rax, %r8
	je	.L19
	movabsq	$2361183241434822607, %rdx
	movq	%r8, %rax
	sarq	$63, %r8
	imulq	%rdx
	sarq	$7, %rdx
	subq	%r8, %rdx
	movq	%rdx, %r8
.L19:
	movq	%r8, %rax
	ret
	.cfi_endproc
.LFE3607:
	.size	_ZNK2v84base9TimeDelta14InMillisecondsEv, .-_ZNK2v84base9TimeDelta14InMillisecondsEv
	.section	.text._ZNK2v84base9TimeDelta23InMillisecondsRoundedUpEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v84base9TimeDelta23InMillisecondsRoundedUpEv
	.type	_ZNK2v84base9TimeDelta23InMillisecondsRoundedUpEv, @function
_ZNK2v84base9TimeDelta23InMillisecondsRoundedUpEv:
.LFB3608:
	.cfi_startproc
	endbr64
	movabsq	$9223372036854775807, %rdx
	movq	(%rdi), %rax
	cmpq	%rdx, %rax
	je	.L21
	leaq	999(%rax), %rcx
	movabsq	$2361183241434822607, %rdx
	movq	%rcx, %rax
	imulq	%rdx
	movq	%rcx, %rax
	sarq	$63, %rax
	sarq	$7, %rdx
	subq	%rax, %rdx
	movq	%rdx, %rax
.L21:
	ret
	.cfi_endproc
.LFE3608:
	.size	_ZNK2v84base9TimeDelta23InMillisecondsRoundedUpEv, .-_ZNK2v84base9TimeDelta23InMillisecondsRoundedUpEv
	.section	.text._ZNK2v84base9TimeDelta14InMicrosecondsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v84base9TimeDelta14InMicrosecondsEv
	.type	_ZNK2v84base9TimeDelta14InMicrosecondsEv, @function
_ZNK2v84base9TimeDelta14InMicrosecondsEv:
.LFB3609:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE3609:
	.size	_ZNK2v84base9TimeDelta14InMicrosecondsEv, .-_ZNK2v84base9TimeDelta14InMicrosecondsEv
	.section	.text._ZNK2v84base9TimeDelta13InNanosecondsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v84base9TimeDelta13InNanosecondsEv
	.type	_ZNK2v84base9TimeDelta13InNanosecondsEv, @function
_ZNK2v84base9TimeDelta13InNanosecondsEv:
.LFB3610:
	.cfi_startproc
	endbr64
	movabsq	$9223372036854775807, %rdx
	movq	(%rdi), %rax
	cmpq	%rdx, %rax
	je	.L24
	imulq	$1000, %rax, %rax
.L24:
	ret
	.cfi_endproc
.LFE3610:
	.size	_ZNK2v84base9TimeDelta13InNanosecondsEv, .-_ZNK2v84base9TimeDelta13InNanosecondsEv
	.section	.text._ZN2v84base9TimeDelta12FromTimespecE8timespec,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base9TimeDelta12FromTimespecE8timespec
	.type	_ZN2v84base9TimeDelta12FromTimespecE8timespec, @function
_ZN2v84base9TimeDelta12FromTimespecE8timespec:
.LFB3611:
	.cfi_startproc
	endbr64
	movq	%rsi, %rax
	imulq	$1000000, %rdi, %rdi
	sarq	$63, %rsi
	movabsq	$2361183241434822607, %rdx
	imulq	%rdx
	sarq	$7, %rdx
	subq	%rsi, %rdx
	leaq	(%rdi,%rdx), %rax
	ret
	.cfi_endproc
.LFE3611:
	.size	_ZN2v84base9TimeDelta12FromTimespecE8timespec, .-_ZN2v84base9TimeDelta12FromTimespecE8timespec
	.section	.text._ZNK2v84base9TimeDelta10ToTimespecEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v84base9TimeDelta10ToTimespecEv
	.type	_ZNK2v84base9TimeDelta10ToTimespecEv, @function
_ZNK2v84base9TimeDelta10ToTimespecEv:
.LFB3612:
	.cfi_startproc
	endbr64
	movabsq	$4835703278458516699, %rdx
	movq	(%rdi), %rcx
	movq	%rcx, %rax
	imulq	%rdx
	movq	%rdx, %rax
	movq	%rcx, %rdx
	sarq	$63, %rdx
	sarq	$18, %rax
	subq	%rdx, %rax
	imulq	$1000000, %rax, %rdx
	subq	%rdx, %rcx
	imulq	$1000, %rcx, %rdx
	ret
	.cfi_endproc
.LFE3612:
	.size	_ZNK2v84base9TimeDelta10ToTimespecEv, .-_ZNK2v84base9TimeDelta10ToTimespecEv
	.section	.text._ZN2v84base4Time3NowEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base4Time3NowEv
	.type	_ZN2v84base4Time3NowEv, @function
_ZN2v84base4Time3NowEv:
.LFB3613:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-32(%rbp), %rdi
	call	gettimeofday@PLT
	movq	-24(%rbp), %rdx
	movq	-32(%rbp), %rax
	testq	%rdx, %rdx
	jne	.L29
	testq	%rax, %rax
	jne	.L31
.L30:
	movq	-8(%rbp), %rsi
	xorq	%fs:40, %rsi
	jne	.L42
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	.cfi_restore_state
	movabsq	$9223372036854775807, %rcx
	cmpq	%rcx, %rax
	jne	.L31
	cmpq	$999999, %rdx
	je	.L34
.L31:
	imulq	$1000000, %rax, %rax
	addq	%rdx, %rax
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L34:
	movq	%rcx, %rax
	jmp	.L30
.L42:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3613:
	.size	_ZN2v84base4Time3NowEv, .-_ZN2v84base4Time3NowEv
	.section	.text._ZN2v84base4Time17NowFromSystemTimeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base4Time17NowFromSystemTimeEv
	.type	_ZN2v84base4Time17NowFromSystemTimeEv, @function
_ZN2v84base4Time17NowFromSystemTimeEv:
.LFB3614:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-32(%rbp), %rdi
	call	gettimeofday@PLT
	movq	-24(%rbp), %rdx
	movq	-32(%rbp), %rax
	testq	%rdx, %rdx
	jne	.L44
	testq	%rax, %rax
	jne	.L46
.L45:
	movq	-8(%rbp), %rsi
	xorq	%fs:40, %rsi
	jne	.L57
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L44:
	.cfi_restore_state
	movabsq	$9223372036854775807, %rcx
	cmpq	%rcx, %rax
	jne	.L46
	cmpq	$999999, %rdx
	je	.L49
.L46:
	imulq	$1000000, %rax, %rax
	addq	%rdx, %rax
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L49:
	movq	%rcx, %rax
	jmp	.L45
.L57:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3614:
	.size	_ZN2v84base4Time17NowFromSystemTimeEv, .-_ZN2v84base4Time17NowFromSystemTimeEv
	.section	.text._ZN2v84base4Time12FromTimespecE8timespec,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base4Time12FromTimespecE8timespec
	.type	_ZN2v84base4Time12FromTimespecE8timespec, @function
_ZN2v84base4Time12FromTimespecE8timespec:
.LFB3615:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	jne	.L59
	testq	%rdi, %rdi
	jne	.L60
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L59:
	movabsq	$9223372036854775807, %rax
	cmpq	%rax, %rdi
	jne	.L60
	cmpq	$999999999, %rsi
	je	.L61
.L60:
	movq	%rsi, %rax
	imulq	$1000000, %rdi, %rdi
	sarq	$63, %rsi
	movabsq	$2361183241434822607, %rdx
	imulq	%rdx
	sarq	$7, %rdx
	subq	%rsi, %rdx
	leaq	(%rdi,%rdx), %rax
.L61:
	ret
	.cfi_endproc
.LFE3615:
	.size	_ZN2v84base4Time12FromTimespecE8timespec, .-_ZN2v84base4Time12FromTimespecE8timespec
	.section	.text._ZNK2v84base4Time10ToTimespecEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v84base4Time10ToTimespecEv
	.type	_ZNK2v84base4Time10ToTimespecEv, @function
_ZNK2v84base4Time10ToTimespecEv:
.LFB3616:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rcx
	testq	%rcx, %rcx
	jne	.L69
	xorl	%eax, %eax
	xorl	%edx, %edx
	ret
	.p2align 4,,10
	.p2align 3
.L69:
	movabsq	$9223372036854775807, %rax
	cmpq	%rax, %rcx
	je	.L72
	movabsq	$4835703278458516699, %rdx
	movq	%rcx, %rax
	imulq	%rdx
	movq	%rdx, %rax
	movq	%rcx, %rdx
	sarq	$63, %rdx
	sarq	$18, %rax
	subq	%rdx, %rax
	imulq	$1000000, %rax, %rdx
	subq	%rdx, %rcx
	imulq	$1000, %rcx, %rdx
	ret
	.p2align 4,,10
	.p2align 3
.L72:
	movq	%rcx, %rax
	movl	$999999999, %edx
	ret
	.cfi_endproc
.LFE3616:
	.size	_ZNK2v84base4Time10ToTimespecEv, .-_ZNK2v84base4Time10ToTimespecEv
	.section	.text._ZN2v84base4Time11FromTimevalE7timeval,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base4Time11FromTimevalE7timeval
	.type	_ZN2v84base4Time11FromTimevalE7timeval, @function
_ZN2v84base4Time11FromTimevalE7timeval:
.LFB3617:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	jne	.L74
	testq	%rdi, %rdi
	jne	.L75
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L74:
	movabsq	$9223372036854775807, %rax
	cmpq	%rax, %rdi
	jne	.L75
	cmpq	$999999, %rsi
	je	.L76
.L75:
	imulq	$1000000, %rdi, %rdi
	leaq	(%rdi,%rsi), %rax
.L76:
	ret
	.cfi_endproc
.LFE3617:
	.size	_ZN2v84base4Time11FromTimevalE7timeval, .-_ZN2v84base4Time11FromTimevalE7timeval
	.section	.text._ZNK2v84base4Time9ToTimevalEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v84base4Time9ToTimevalEv
	.type	_ZNK2v84base4Time9ToTimevalEv, @function
_ZNK2v84base4Time9ToTimevalEv:
.LFB3618:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rcx
	testq	%rcx, %rcx
	jne	.L84
	xorl	%eax, %eax
	xorl	%edx, %edx
	ret
	.p2align 4,,10
	.p2align 3
.L84:
	movabsq	$9223372036854775807, %rax
	cmpq	%rax, %rcx
	je	.L87
	movabsq	$4835703278458516699, %rdx
	movq	%rcx, %rax
	imulq	%rdx
	movq	%rdx, %rax
	movq	%rcx, %rdx
	sarq	$63, %rdx
	sarq	$18, %rax
	subq	%rdx, %rax
	imulq	$1000000, %rax, %rdx
	subq	%rdx, %rcx
	movq	%rcx, %rdx
	ret
	.p2align 4,,10
	.p2align 3
.L87:
	movq	%rcx, %rax
	movl	$999999, %edx
	ret
	.cfi_endproc
.LFE3618:
	.size	_ZNK2v84base4Time9ToTimevalEv, .-_ZNK2v84base4Time9ToTimevalEv
	.section	.rodata._ZN2v84base9TimeTicks17HighResolutionNowEv.str1.1,"aMS",@progbits,1
.LC3:
	.string	"unreachable code"
.LC4:
	.string	"kSecondsLimit > ts.tv_sec"
.LC5:
	.string	"Check failed: %s."
	.section	.text._ZN2v84base9TimeTicks17HighResolutionNowEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base9TimeTicks17HighResolutionNowEv
	.type	_ZN2v84base9TimeTicks17HighResolutionNowEv, @function
_ZN2v84base9TimeTicks17HighResolutionNowEv:
.LFB3619:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-32(%rbp), %rsi
	call	clock_gettime@PLT
	testl	%eax, %eax
	jne	.L93
	movabsq	$9223372036852, %rdx
	movq	-32(%rbp), %rax
	cmpq	%rdx, %rax
	jg	.L94
	movq	-24(%rbp), %rsi
	imulq	$1000000, %rax, %rcx
	movabsq	$2361183241434822607, %rdx
	movq	%rsi, %rax
	sarq	$63, %rsi
	imulq	%rdx
	sarq	$7, %rdx
	subq	%rsi, %rdx
	movq	-8(%rbp), %rdi
	xorq	%fs:40, %rdi
	leaq	1(%rcx,%rdx), %rax
	jne	.L95
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L94:
	.cfi_restore_state
	leaq	.LC4(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L95:
	call	__stack_chk_fail@PLT
.L93:
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE3619:
	.size	_ZN2v84base9TimeTicks17HighResolutionNowEv, .-_ZN2v84base9TimeTicks17HighResolutionNowEv
	.section	.text._ZN2v84base4Time10FromJsTimeEd,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base4Time10FromJsTimeEd
	.type	_ZN2v84base4Time10FromJsTimeEd, @function
_ZN2v84base4Time10FromJsTimeEd:
.LFB3620:
	.cfi_startproc
	endbr64
	ucomisd	.LC6(%rip), %xmm0
	jp	.L100
	jne	.L100
	movabsq	$9223372036854775807, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L100:
	mulsd	.LC2(%rip), %xmm0
	cvttsd2siq	%xmm0, %rax
	ret
	.cfi_endproc
.LFE3620:
	.size	_ZN2v84base4Time10FromJsTimeEd, .-_ZN2v84base4Time10FromJsTimeEd
	.section	.text._ZNK2v84base4Time8ToJsTimeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v84base4Time8ToJsTimeEv
	.type	_ZNK2v84base4Time8ToJsTimeEv, @function
_ZNK2v84base4Time8ToJsTimeEv:
.LFB3621:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	pxor	%xmm0, %xmm0
	testq	%rax, %rax
	je	.L101
	movabsq	$9223372036854775807, %rdx
	movsd	.LC6(%rip), %xmm0
	cmpq	%rdx, %rax
	je	.L101
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
	divsd	.LC2(%rip), %xmm0
.L101:
	ret
	.cfi_endproc
.LFE3621:
	.size	_ZNK2v84base4Time8ToJsTimeEv, .-_ZNK2v84base4Time8ToJsTimeEv
	.section	.text._ZN2v84baselsERSoRKNS0_4TimeE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v84baselsERSoRKNS0_4TimeE
	.type	_ZN2v84baselsERSoRKNS0_4TimeE, @function
_ZN2v84baselsERSoRKNS0_4TimeE:
.LFB3622:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rax
	pxor	%xmm0, %xmm0
	testq	%rax, %rax
	je	.L107
	movabsq	$9223372036854775807, %rdx
	movsd	.LC6(%rip), %xmm0
	cmpq	%rdx, %rax
	je	.L107
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
	divsd	.LC2(%rip), %xmm0
.L107:
	jmp	_ZNSo9_M_insertIdEERSoT_@PLT
	.cfi_endproc
.LFE3622:
	.size	_ZN2v84baselsERSoRKNS0_4TimeE, .-_ZN2v84baselsERSoRKNS0_4TimeE
	.section	.text._ZN2v84base9TimeTicks3NowEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base9TimeTicks3NowEv
	.type	_ZN2v84base9TimeTicks3NowEv, @function
_ZN2v84base9TimeTicks3NowEv:
.LFB3623:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-32(%rbp), %rsi
	call	clock_gettime@PLT
	testl	%eax, %eax
	jne	.L116
	movabsq	$9223372036852, %rdx
	movq	-32(%rbp), %rax
	cmpq	%rdx, %rax
	jg	.L117
	movq	-24(%rbp), %rsi
	imulq	$1000000, %rax, %rcx
	movabsq	$2361183241434822607, %rdx
	movq	%rsi, %rax
	sarq	$63, %rsi
	imulq	%rdx
	sarq	$7, %rdx
	subq	%rsi, %rdx
	movq	-8(%rbp), %rdi
	xorq	%fs:40, %rdi
	leaq	1(%rcx,%rdx), %rax
	jne	.L118
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L117:
	.cfi_restore_state
	leaq	.LC4(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L118:
	call	__stack_chk_fail@PLT
.L116:
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE3623:
	.size	_ZN2v84base9TimeTicks3NowEv, .-_ZN2v84base9TimeTicks3NowEv
	.section	.text._ZN2v84base9TimeTicks16IsHighResolutionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base9TimeTicks16IsHighResolutionEv
	.type	_ZN2v84base9TimeTicks16IsHighResolutionEv, @function
_ZN2v84base9TimeTicks16IsHighResolutionEv:
.LFB3624:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	_ZGVZN2v84base9TimeTicks16IsHighResolutionEvE18is_high_resolution(%rip), %eax
	testb	%al, %al
	je	.L140
.L121:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	movzbl	_ZZN2v84base9TimeTicks16IsHighResolutionEvE18is_high_resolution(%rip), %eax
	jne	.L141
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L140:
	.cfi_restore_state
	leaq	_ZGVZN2v84base9TimeTicks16IsHighResolutionEvE18is_high_resolution(%rip), %rdi
	call	__cxa_guard_acquire@PLT
	testl	%eax, %eax
	je	.L121
	leaq	-80(%rbp), %rsi
	movl	$1, %edi
	call	clock_gettime@PLT
	testl	%eax, %eax
	jne	.L125
	movabsq	$9223372036852, %r12
	movq	-80(%rbp), %rax
	cmpq	%r12, %rax
	jg	.L126
	movq	-72(%rbp), %rsi
	imulq	$1000000, %rax, %rcx
	leaq	-96(%rbp), %r15
	movabsq	$2361183241434822607, %r13
	leaq	-112(%rbp), %r14
	movq	%rsi, %rax
	sarq	$63, %rsi
	imulq	%r13
	sarq	$7, %rdx
	subq	%rsi, %rdx
	leaq	100000(%rcx,%rdx), %rax
	movq	%rax, -120(%rbp)
.L128:
	movq	%r15, %rsi
	movl	$1, %edi
	call	clock_gettime@PLT
	testl	%eax, %eax
	jne	.L125
	movq	-96(%rbp), %rbx
	cmpq	%r12, %rbx
	jg	.L126
	movq	-88(%rbp), %rcx
	imulq	$1000000, %rbx, %rbx
	movq	%rcx, %rax
	sarq	$63, %rcx
	imulq	%r13
	sarq	$7, %rdx
	subq	%rcx, %rdx
	addq	%rdx, %rbx
	jmp	.L127
	.p2align 4,,10
	.p2align 3
.L143:
	movq	-104(%rbp), %rsi
	imulq	$1000000, %rdx, %rcx
	movq	%rsi, %rax
	sarq	$63, %rsi
	imulq	%r13
	sarq	$7, %rdx
	subq	%rsi, %rdx
	addq	%rcx, %rdx
	subq	%rbx, %rdx
	jne	.L142
.L127:
	movq	%r14, %rsi
	movl	$1, %edi
	call	clock_gettime@PLT
	testl	%eax, %eax
	jne	.L125
	movq	-112(%rbp), %rdx
	cmpq	%r12, %rdx
	jle	.L143
.L126:
	leaq	.LC4(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L142:
	cmpq	$1, %rdx
	setle	%al
	cmpq	%rbx, -120(%rbp)
	jle	.L131
	testb	%al, %al
	je	.L128
.L131:
	leaq	_ZGVZN2v84base9TimeTicks16IsHighResolutionEvE18is_high_resolution(%rip), %rdi
	movb	%al, _ZZN2v84base9TimeTicks16IsHighResolutionEvE18is_high_resolution(%rip)
	call	__cxa_guard_release@PLT
	jmp	.L121
.L125:
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L141:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3624:
	.size	_ZN2v84base9TimeTicks16IsHighResolutionEv, .-_ZN2v84base9TimeTicks16IsHighResolutionEv
	.section	.text._ZN2v84base11ThreadTicks11IsSupportedEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base11ThreadTicks11IsSupportedEv
	.type	_ZN2v84base11ThreadTicks11IsSupportedEv, @function
_ZN2v84base11ThreadTicks11IsSupportedEv:
.LFB3625:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE3625:
	.size	_ZN2v84base11ThreadTicks11IsSupportedEv, .-_ZN2v84base11ThreadTicks11IsSupportedEv
	.section	.text._ZN2v84base11ThreadTicks3NowEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base11ThreadTicks3NowEv
	.type	_ZN2v84base11ThreadTicks3NowEv, @function
_ZN2v84base11ThreadTicks3NowEv:
.LFB3626:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$3, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-32(%rbp), %rsi
	call	clock_gettime@PLT
	testl	%eax, %eax
	jne	.L150
	movabsq	$9223372036852, %rdx
	movq	-32(%rbp), %rax
	cmpq	%rdx, %rax
	jg	.L151
	movq	-24(%rbp), %rsi
	imulq	$1000000, %rax, %rcx
	movabsq	$2361183241434822607, %rdx
	movq	%rsi, %rax
	sarq	$63, %rsi
	imulq	%rdx
	sarq	$7, %rdx
	subq	%rsi, %rdx
	movq	-8(%rbp), %rdi
	xorq	%fs:40, %rdi
	leaq	(%rdx,%rcx), %rax
	jne	.L152
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L151:
	.cfi_restore_state
	leaq	.LC4(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L152:
	call	__stack_chk_fail@PLT
.L150:
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE3626:
	.size	_ZN2v84base11ThreadTicks3NowEv, .-_ZN2v84base11ThreadTicks3NowEv
	.section	.bss._ZGVZN2v84base9TimeTicks16IsHighResolutionEvE18is_high_resolution,"aw",@nobits
	.align 8
	.type	_ZGVZN2v84base9TimeTicks16IsHighResolutionEvE18is_high_resolution, @object
	.size	_ZGVZN2v84base9TimeTicks16IsHighResolutionEvE18is_high_resolution, 8
_ZGVZN2v84base9TimeTicks16IsHighResolutionEvE18is_high_resolution:
	.zero	8
	.section	.bss._ZZN2v84base9TimeTicks16IsHighResolutionEvE18is_high_resolution,"aw",@nobits
	.type	_ZZN2v84base9TimeTicks16IsHighResolutionEvE18is_high_resolution, @object
	.size	_ZZN2v84base9TimeTicks16IsHighResolutionEvE18is_high_resolution, 1
_ZZN2v84base9TimeTicks16IsHighResolutionEvE18is_high_resolution:
	.zero	1
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC0:
	.long	0
	.long	2146435072
	.align 8
.LC1:
	.long	0
	.long	1093567616
	.align 8
.LC2:
	.long	0
	.long	1083129856
	.align 8
.LC6:
	.long	4294967295
	.long	2146435071
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
