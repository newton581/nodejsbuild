	.file	"functional.cc"
	.text
	.section	.text._ZN2v84base12hash_combineEmm,"ax",@progbits
	.p2align 4
	.globl	_ZN2v84base12hash_combineEmm
	.type	_ZN2v84base12hash_combineEmm, @function
_ZN2v84base12hash_combineEmm:
.LFB2915:
	.cfi_startproc
	endbr64
	movabsq	$-4132994306676758123, %rax
	movq	%rdi, %r8
	imulq	%rax, %rsi
	movq	%rsi, %rdi
	shrq	$47, %rdi
	xorq	%rsi, %rdi
	imulq	%rax, %rdi
	xorq	%r8, %rdi
	imulq	%rdi, %rax
	ret
	.cfi_endproc
.LFE2915:
	.size	_ZN2v84base12hash_combineEmm, .-_ZN2v84base12hash_combineEmm
	.section	.text._ZN2v84base10hash_valueEj,"ax",@progbits
	.p2align 4
	.globl	_ZN2v84base10hash_valueEj
	.type	_ZN2v84base10hash_valueEj, @function
_ZN2v84base10hash_valueEj:
.LFB2916:
	.cfi_startproc
	endbr64
	movl	%edi, %eax
	sall	$15, %eax
	subl	%edi, %eax
	subl	$1, %eax
	movl	%eax, %edx
	shrl	$12, %edx
	xorl	%edx, %eax
	leal	(%rax,%rax,4), %eax
	movl	%eax, %edx
	shrl	$4, %edx
	xorl	%edx, %eax
	imull	$2057, %eax, %eax
	movl	%eax, %edx
	shrl	$16, %edx
	xorl	%edx, %eax
	ret
	.cfi_endproc
.LFE2916:
	.size	_ZN2v84base10hash_valueEj, .-_ZN2v84base10hash_valueEj
	.section	.text._ZN2v84base10hash_valueEm,"ax",@progbits
	.p2align 4
	.globl	_ZN2v84base10hash_valueEm
	.type	_ZN2v84base10hash_valueEm, @function
_ZN2v84base10hash_valueEm:
.LFB2917:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	salq	$21, %rax
	subq	%rdi, %rax
	subq	$1, %rax
	movq	%rax, %rdx
	shrq	$24, %rdx
	xorq	%rdx, %rax
	movq	%rax, %rdx
	salq	$8, %rdx
	leaq	(%rdx,%rax,8), %rdx
	addq	%rdx, %rax
	movq	%rax, %rdx
	shrq	$14, %rdx
	xorq	%rdx, %rax
	movq	%rax, %rdx
	salq	$4, %rdx
	leaq	(%rdx,%rax,4), %rdx
	addq	%rdx, %rax
	movq	%rax, %rdx
	shrq	$28, %rdx
	xorq	%rdx, %rax
	movq	%rax, %rdx
	salq	$31, %rdx
	addq	%rdx, %rax
	ret
	.cfi_endproc
.LFE2917:
	.size	_ZN2v84base10hash_valueEm, .-_ZN2v84base10hash_valueEm
	.section	.text._ZN2v84base10hash_valueEy,"ax",@progbits
	.p2align 4
	.globl	_ZN2v84base10hash_valueEy
	.type	_ZN2v84base10hash_valueEy, @function
_ZN2v84base10hash_valueEy:
.LFB2918:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	salq	$21, %rax
	subq	%rdi, %rax
	subq	$1, %rax
	movq	%rax, %rdx
	shrq	$24, %rdx
	xorq	%rdx, %rax
	movq	%rax, %rdx
	salq	$8, %rdx
	leaq	(%rdx,%rax,8), %rdx
	addq	%rdx, %rax
	movq	%rax, %rdx
	shrq	$14, %rdx
	xorq	%rdx, %rax
	movq	%rax, %rdx
	salq	$4, %rdx
	leaq	(%rdx,%rax,4), %rdx
	addq	%rdx, %rax
	movq	%rax, %rdx
	shrq	$28, %rdx
	xorq	%rdx, %rax
	movq	%rax, %rdx
	salq	$31, %rdx
	addq	%rdx, %rax
	ret
	.cfi_endproc
.LFE2918:
	.size	_ZN2v84base10hash_valueEy, .-_ZN2v84base10hash_valueEy
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
