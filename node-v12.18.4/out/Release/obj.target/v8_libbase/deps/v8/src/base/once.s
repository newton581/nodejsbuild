	.file	"once.cc"
	.text
	.section	.text._ZN2v84base12CallOnceImplEPSt6atomicIhESt8functionIFvvEE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v84base12CallOnceImplEPSt6atomicIhESt8functionIFvvEE
	.type	_ZN2v84base12CallOnceImplEPSt6atomicIhESt8functionIFvvEE, @function
_ZN2v84base12CallOnceImplEPSt6atomicIhESt8functionIFvvEE:
.LFB1750:
	.cfi_startproc
	endbr64
	movzbl	(%rdi), %eax
	cmpb	$2, %al
	je	.L12
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movl	$1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	lock cmpxchgb	%dl, (%rdi)
	jne	.L3
	cmpq	$0, 16(%rsi)
	je	.L16
	movq	%rsi, %rdi
	call	*24(%rsi)
	movb	$2, (%rbx)
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L17:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	call	sched_yield@PLT
.L3:
	movzbl	(%rbx), %eax
	cmpb	$1, %al
	je	.L17
	jmp	.L1
.L16:
	call	_ZSt25__throw_bad_function_callv@PLT
	.cfi_endproc
.LFE1750:
	.size	_ZN2v84base12CallOnceImplEPSt6atomicIhESt8functionIFvvEE, .-_ZN2v84base12CallOnceImplEPSt6atomicIhESt8functionIFvvEE
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
