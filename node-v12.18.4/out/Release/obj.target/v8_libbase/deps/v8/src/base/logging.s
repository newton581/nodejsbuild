	.file	"logging.cc"
	.text
	.section	.text._ZN2v84base18SetPrintStackTraceEPFvvE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v84base18SetPrintStackTraceEPFvvE
	.type	_ZN2v84base18SetPrintStackTraceEPFvvE, @function
_ZN2v84base18SetPrintStackTraceEPFvvE:
.LFB3444:
	.cfi_startproc
	endbr64
	movq	%rdi, _ZN2v84base12_GLOBAL__N_119g_print_stack_traceE(%rip)
	ret
	.cfi_endproc
.LFE3444:
	.size	_ZN2v84base18SetPrintStackTraceEPFvvE, .-_ZN2v84base18SetPrintStackTraceEPFvvE
	.section	.text._ZN2v84base17SetDcheckFunctionEPFvPKciS2_E,"ax",@progbits
	.p2align 4
	.globl	_ZN2v84base17SetDcheckFunctionEPFvPKciS2_E
	.type	_ZN2v84base17SetDcheckFunctionEPFvPKciS2_E, @function
_ZN2v84base17SetDcheckFunctionEPFvPKciS2_E:
.LFB3445:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	leaq	_ZN2v84base12_GLOBAL__N_120DefaultDcheckHandlerEPKciS3_(%rip), %rax
	cmove	%rax, %rdi
	movq	%rdi, _ZN2v84base12_GLOBAL__N_117g_dcheck_functionE(%rip)
	ret
	.cfi_endproc
.LFE3445:
	.size	_ZN2v84base17SetDcheckFunctionEPFvPKciS2_E, .-_ZN2v84base17SetDcheckFunctionEPFvPKciS2_E
	.section	.rodata._ZN2v84base17PrintCheckOperandIcEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS5_vEE5valueEvE4typeERSoS5_.str1.1,"aMS",@progbits,1
.LC0:
	.string	"'\\0'"
.LC1:
	.string	"'\\''"
.LC2:
	.string	"'\\\\'"
.LC3:
	.string	"'\\a'"
.LC4:
	.string	"'\\b'"
.LC5:
	.string	"'\\f'"
.LC6:
	.string	"'\\n'"
.LC7:
	.string	"'\\r'"
.LC8:
	.string	"'\\t'"
.LC9:
	.string	"'\\v'"
.LC10:
	.string	"\\x"
	.section	.text._ZN2v84base17PrintCheckOperandIcEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS5_vEE5valueEvE4typeERSoS5_,"ax",@progbits
	.p2align 4
	.globl	_ZN2v84base17PrintCheckOperandIcEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS5_vEE5valueEvE4typeERSoS5_
	.type	_ZN2v84base17PrintCheckOperandIcEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS5_vEE5valueEvE4typeERSoS5_, @function
_ZN2v84base17PrintCheckOperandIcEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS5_vEE5valueEvE4typeERSoS5_:
.LFB3446:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	subq	$24, %rsp
	.cfi_offset 12, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$39, %sil
	jg	.L6
	testb	%sil, %sil
	js	.L7
	cmpb	$39, %sil
	ja	.L7
	leaq	.L9(%rip), %rcx
	movzbl	%sil, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v84base17PrintCheckOperandIcEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS5_vEE5valueEvE4typeERSoS5_,"a",@progbits
	.align 4
	.align 4
.L9:
	.long	.L17-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L16-.L9
	.long	.L15-.L9
	.long	.L14-.L9
	.long	.L13-.L9
	.long	.L12-.L9
	.long	.L11-.L9
	.long	.L10-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L8-.L9
	.section	.text._ZN2v84base17PrintCheckOperandIcEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS5_vEE5valueEvE4typeERSoS5_
	.p2align 4,,10
	.p2align 3
.L6:
	cmpb	$92, %sil
	jne	.L7
	movl	$4, %edx
	leaq	.LC2(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	.p2align 4,,10
	.p2align 3
.L5:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L23
	addq	$24, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L8:
	.cfi_restore_state
	movl	$4, %edx
	leaq	.LC1(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L5
.L7:
	movsbl	%sil, %r12d
	movl	%r12d, %edi
	call	isprint@PLT
	testl	%eax, %eax
	je	.L20
	leaq	-41(%rbp), %r14
	movl	$1, %edx
	movq	%r13, %rdi
	movb	$39, -41(%rbp)
	movq	%r14, %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	%r12d, %esi
	movq	%rax, %rdi
	call	_ZNSolsEi@PLT
	movl	$1, %edx
	movq	%r14, %rsi
	movb	$39, -41(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L5
.L17:
	movl	$4, %edx
	leaq	.LC0(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L5
.L16:
	movl	$4, %edx
	leaq	.LC3(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L5
.L15:
	movl	$4, %edx
	leaq	.LC4(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L5
.L14:
	movl	$4, %edx
	leaq	.LC8(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L5
.L13:
	movl	$4, %edx
	leaq	.LC6(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L5
.L12:
	movl	$4, %edx
	leaq	.LC9(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L5
.L11:
	movl	$4, %edx
	leaq	.LC5(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L5
.L10:
	movl	$4, %edx
	leaq	.LC7(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L20:
	movq	0(%r13), %rax
	movl	$2, %edx
	leaq	.LC10(%rip), %rsi
	movq	%r13, %rdi
	movq	-24(%rax), %rcx
	addq	%r13, %rcx
	movl	24(%rcx), %r14d
	movl	$8, 24(%rcx)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	%r12d, %esi
	movq	%r13, %rdi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movq	0(%r13), %rax
	movq	-24(%rax), %rax
	movl	%r14d, 24(%r13,%rax)
	jmp	.L5
.L23:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3446:
	.size	_ZN2v84base17PrintCheckOperandIcEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS5_vEE5valueEvE4typeERSoS5_, .-_ZN2v84base17PrintCheckOperandIcEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS5_vEE5valueEvE4typeERSoS5_
	.section	.text._ZN2v84base17PrintCheckOperandIPcEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS6_vEE5valueEvE4typeERSoS6_,"ax",@progbits
	.p2align 4
	.globl	_ZN2v84base17PrintCheckOperandIPcEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS6_vEE5valueEvE4typeERSoS6_
	.type	_ZN2v84base17PrintCheckOperandIPcEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS6_vEE5valueEvE4typeERSoS6_, @function
_ZN2v84base17PrintCheckOperandIPcEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS6_vEE5valueEvE4typeERSoS6_:
.LFB3447:
	.cfi_startproc
	endbr64
	jmp	_ZNSo9_M_insertIPKvEERSoT_@PLT
	.cfi_endproc
.LFE3447:
	.size	_ZN2v84base17PrintCheckOperandIPcEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS6_vEE5valueEvE4typeERSoS6_, .-_ZN2v84base17PrintCheckOperandIPcEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS6_vEE5valueEvE4typeERSoS6_
	.section	.text._ZN2v84base17PrintCheckOperandIPKcEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS7_vEE5valueEvE4typeERSoS7_,"ax",@progbits
	.p2align 4
	.globl	_ZN2v84base17PrintCheckOperandIPKcEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS7_vEE5valueEvE4typeERSoS7_
	.type	_ZN2v84base17PrintCheckOperandIPKcEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS7_vEE5valueEvE4typeERSoS7_, @function
_ZN2v84base17PrintCheckOperandIPKcEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS7_vEE5valueEvE4typeERSoS7_:
.LFB4124:
	.cfi_startproc
	endbr64
	jmp	_ZNSo9_M_insertIPKvEERSoT_@PLT
	.cfi_endproc
.LFE4124:
	.size	_ZN2v84base17PrintCheckOperandIPKcEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS7_vEE5valueEvE4typeERSoS7_, .-_ZN2v84base17PrintCheckOperandIPKcEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS7_vEE5valueEvE4typeERSoS7_
	.section	.text._ZN2v84base17PrintCheckOperandIaEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS5_vEE5valueEvE4typeERSoS5_,"ax",@progbits
	.p2align 4
	.globl	_ZN2v84base17PrintCheckOperandIaEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS5_vEE5valueEvE4typeERSoS5_
	.type	_ZN2v84base17PrintCheckOperandIaEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS5_vEE5valueEvE4typeERSoS5_, @function
_ZN2v84base17PrintCheckOperandIaEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS5_vEE5valueEvE4typeERSoS5_:
.LFB4122:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	subq	$24, %rsp
	.cfi_offset 12, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$39, %sil
	jg	.L27
	testb	%sil, %sil
	js	.L28
	cmpb	$39, %sil
	ja	.L28
	leaq	.L30(%rip), %rcx
	movzbl	%sil, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v84base17PrintCheckOperandIaEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS5_vEE5valueEvE4typeERSoS5_,"a",@progbits
	.align 4
	.align 4
.L30:
	.long	.L38-.L30
	.long	.L28-.L30
	.long	.L28-.L30
	.long	.L28-.L30
	.long	.L28-.L30
	.long	.L28-.L30
	.long	.L28-.L30
	.long	.L37-.L30
	.long	.L36-.L30
	.long	.L35-.L30
	.long	.L34-.L30
	.long	.L33-.L30
	.long	.L32-.L30
	.long	.L31-.L30
	.long	.L28-.L30
	.long	.L28-.L30
	.long	.L28-.L30
	.long	.L28-.L30
	.long	.L28-.L30
	.long	.L28-.L30
	.long	.L28-.L30
	.long	.L28-.L30
	.long	.L28-.L30
	.long	.L28-.L30
	.long	.L28-.L30
	.long	.L28-.L30
	.long	.L28-.L30
	.long	.L28-.L30
	.long	.L28-.L30
	.long	.L28-.L30
	.long	.L28-.L30
	.long	.L28-.L30
	.long	.L28-.L30
	.long	.L28-.L30
	.long	.L28-.L30
	.long	.L28-.L30
	.long	.L28-.L30
	.long	.L28-.L30
	.long	.L28-.L30
	.long	.L29-.L30
	.section	.text._ZN2v84base17PrintCheckOperandIaEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS5_vEE5valueEvE4typeERSoS5_
	.p2align 4,,10
	.p2align 3
.L27:
	cmpb	$92, %sil
	jne	.L28
	movl	$4, %edx
	leaq	.LC2(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	.p2align 4,,10
	.p2align 3
.L26:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L44
	addq	$24, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L29:
	.cfi_restore_state
	movl	$4, %edx
	leaq	.LC1(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L26
.L28:
	movsbl	%sil, %r12d
	movl	%r12d, %edi
	call	isprint@PLT
	testl	%eax, %eax
	je	.L41
	leaq	-41(%rbp), %r14
	movl	$1, %edx
	movq	%r13, %rdi
	movb	$39, -41(%rbp)
	movq	%r14, %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	%r12d, %esi
	movq	%rax, %rdi
	call	_ZNSolsEi@PLT
	movl	$1, %edx
	movq	%r14, %rsi
	movb	$39, -41(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L26
.L38:
	movl	$4, %edx
	leaq	.LC0(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L26
.L37:
	movl	$4, %edx
	leaq	.LC3(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L26
.L36:
	movl	$4, %edx
	leaq	.LC4(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L26
.L35:
	movl	$4, %edx
	leaq	.LC8(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L26
.L34:
	movl	$4, %edx
	leaq	.LC6(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L26
.L33:
	movl	$4, %edx
	leaq	.LC9(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L26
.L32:
	movl	$4, %edx
	leaq	.LC5(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L26
.L31:
	movl	$4, %edx
	leaq	.LC7(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L41:
	movq	0(%r13), %rax
	movl	$2, %edx
	leaq	.LC10(%rip), %rsi
	movq	%r13, %rdi
	movq	-24(%rax), %rcx
	addq	%r13, %rcx
	movl	24(%rcx), %r14d
	movl	$8, 24(%rcx)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	%r12d, %esi
	movq	%r13, %rdi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movq	0(%r13), %rax
	movq	-24(%rax), %rax
	movl	%r14d, 24(%r13,%rax)
	jmp	.L26
.L44:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4122:
	.size	_ZN2v84base17PrintCheckOperandIaEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS5_vEE5valueEvE4typeERSoS5_, .-_ZN2v84base17PrintCheckOperandIaEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS5_vEE5valueEvE4typeERSoS5_
	.section	.text._ZN2v84base17PrintCheckOperandIPaEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS6_vEE5valueEvE4typeERSoS6_,"ax",@progbits
	.p2align 4
	.globl	_ZN2v84base17PrintCheckOperandIPaEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS6_vEE5valueEvE4typeERSoS6_
	.type	_ZN2v84base17PrintCheckOperandIPaEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS6_vEE5valueEvE4typeERSoS6_, @function
_ZN2v84base17PrintCheckOperandIPaEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS6_vEE5valueEvE4typeERSoS6_:
.LFB3450:
	.cfi_startproc
	endbr64
	jmp	_ZNSo9_M_insertIPKvEERSoT_@PLT
	.cfi_endproc
.LFE3450:
	.size	_ZN2v84base17PrintCheckOperandIPaEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS6_vEE5valueEvE4typeERSoS6_, .-_ZN2v84base17PrintCheckOperandIPaEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS6_vEE5valueEvE4typeERSoS6_
	.section	.text._ZN2v84base17PrintCheckOperandIPKaEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS7_vEE5valueEvE4typeERSoS7_,"ax",@progbits
	.p2align 4
	.globl	_ZN2v84base17PrintCheckOperandIPKaEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS7_vEE5valueEvE4typeERSoS7_
	.type	_ZN2v84base17PrintCheckOperandIPKaEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS7_vEE5valueEvE4typeERSoS7_, @function
_ZN2v84base17PrintCheckOperandIPKaEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS7_vEE5valueEvE4typeERSoS7_:
.LFB4126:
	.cfi_startproc
	endbr64
	jmp	_ZNSo9_M_insertIPKvEERSoT_@PLT
	.cfi_endproc
.LFE4126:
	.size	_ZN2v84base17PrintCheckOperandIPKaEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS7_vEE5valueEvE4typeERSoS7_, .-_ZN2v84base17PrintCheckOperandIPKaEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS7_vEE5valueEvE4typeERSoS7_
	.section	.text._ZN2v84base17PrintCheckOperandIhEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS5_vEE5valueEvE4typeERSoS5_,"ax",@progbits
	.p2align 4
	.globl	_ZN2v84base17PrintCheckOperandIhEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS5_vEE5valueEvE4typeERSoS5_
	.type	_ZN2v84base17PrintCheckOperandIhEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS5_vEE5valueEvE4typeERSoS5_, @function
_ZN2v84base17PrintCheckOperandIhEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS5_vEE5valueEvE4typeERSoS5_:
.LFB3452:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movl	%esi, %ebx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$39, %sil
	ja	.L48
	leaq	.L51(%rip), %rcx
	movzbl	%sil, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v84base17PrintCheckOperandIhEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS5_vEE5valueEvE4typeERSoS5_,"a",@progbits
	.align 4
	.align 4
.L51:
	.long	.L59-.L51
	.long	.L49-.L51
	.long	.L49-.L51
	.long	.L49-.L51
	.long	.L49-.L51
	.long	.L49-.L51
	.long	.L49-.L51
	.long	.L58-.L51
	.long	.L57-.L51
	.long	.L56-.L51
	.long	.L55-.L51
	.long	.L54-.L51
	.long	.L53-.L51
	.long	.L52-.L51
	.long	.L49-.L51
	.long	.L49-.L51
	.long	.L49-.L51
	.long	.L49-.L51
	.long	.L49-.L51
	.long	.L49-.L51
	.long	.L49-.L51
	.long	.L49-.L51
	.long	.L49-.L51
	.long	.L49-.L51
	.long	.L49-.L51
	.long	.L49-.L51
	.long	.L49-.L51
	.long	.L49-.L51
	.long	.L49-.L51
	.long	.L49-.L51
	.long	.L49-.L51
	.long	.L49-.L51
	.long	.L49-.L51
	.long	.L49-.L51
	.long	.L49-.L51
	.long	.L49-.L51
	.long	.L49-.L51
	.long	.L49-.L51
	.long	.L49-.L51
	.long	.L50-.L51
	.section	.text._ZN2v84base17PrintCheckOperandIhEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS5_vEE5valueEvE4typeERSoS5_
	.p2align 4,,10
	.p2align 3
.L48:
	cmpb	$92, %sil
	jne	.L49
	movl	$4, %edx
	leaq	.LC2(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	.p2align 4,,10
	.p2align 3
.L47:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L65
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L50:
	.cfi_restore_state
	movl	$4, %edx
	leaq	.LC1(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L47
.L49:
	movzbl	%sil, %r12d
	movl	%r12d, %edi
	call	isprint@PLT
	testl	%eax, %eax
	je	.L62
	leaq	-41(%rbp), %r14
	movl	$1, %edx
	movq	%r13, %rdi
	movb	$39, -41(%rbp)
	movq	%r14, %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	%r12d, %esi
	movq	%rax, %rdi
	call	_ZNSolsEi@PLT
	movl	$1, %edx
	movq	%r14, %rsi
	movb	$39, -41(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L47
.L59:
	movl	$4, %edx
	leaq	.LC0(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L47
.L58:
	movl	$4, %edx
	leaq	.LC3(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L47
.L57:
	movl	$4, %edx
	leaq	.LC4(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L47
.L56:
	movl	$4, %edx
	leaq	.LC8(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L47
.L55:
	movl	$4, %edx
	leaq	.LC6(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L47
.L54:
	movl	$4, %edx
	leaq	.LC9(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L47
.L53:
	movl	$4, %edx
	leaq	.LC5(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L47
.L52:
	movl	$4, %edx
	leaq	.LC7(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L62:
	movq	0(%r13), %rax
	movl	$2, %edx
	leaq	.LC10(%rip), %rsi
	movq	%r13, %rdi
	movq	-24(%rax), %rcx
	addq	%r13, %rcx
	movl	24(%rcx), %r12d
	movl	$8, 24(%rcx)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movzbl	%bl, %esi
	movq	%r13, %rdi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movq	0(%r13), %rax
	movq	-24(%rax), %rax
	movl	%r12d, 24(%r13,%rax)
	jmp	.L47
.L65:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3452:
	.size	_ZN2v84base17PrintCheckOperandIhEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS5_vEE5valueEvE4typeERSoS5_, .-_ZN2v84base17PrintCheckOperandIhEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS5_vEE5valueEvE4typeERSoS5_
	.section	.text._ZN2v84base17PrintCheckOperandIPhEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS6_vEE5valueEvE4typeERSoS6_,"ax",@progbits
	.p2align 4
	.globl	_ZN2v84base17PrintCheckOperandIPhEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS6_vEE5valueEvE4typeERSoS6_
	.type	_ZN2v84base17PrintCheckOperandIPhEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS6_vEE5valueEvE4typeERSoS6_, @function
_ZN2v84base17PrintCheckOperandIPhEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS6_vEE5valueEvE4typeERSoS6_:
.LFB3453:
	.cfi_startproc
	endbr64
	jmp	_ZNSo9_M_insertIPKvEERSoT_@PLT
	.cfi_endproc
.LFE3453:
	.size	_ZN2v84base17PrintCheckOperandIPhEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS6_vEE5valueEvE4typeERSoS6_, .-_ZN2v84base17PrintCheckOperandIPhEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS6_vEE5valueEvE4typeERSoS6_
	.section	.text._ZN2v84base17PrintCheckOperandIPKhEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS7_vEE5valueEvE4typeERSoS7_,"ax",@progbits
	.p2align 4
	.globl	_ZN2v84base17PrintCheckOperandIPKhEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS7_vEE5valueEvE4typeERSoS7_
	.type	_ZN2v84base17PrintCheckOperandIPKhEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS7_vEE5valueEvE4typeERSoS7_, @function
_ZN2v84base17PrintCheckOperandIPKhEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS7_vEE5valueEvE4typeERSoS7_:
.LFB4128:
	.cfi_startproc
	endbr64
	jmp	_ZNSo9_M_insertIPKvEERSoT_@PLT
	.cfi_endproc
.LFE4128:
	.size	_ZN2v84base17PrintCheckOperandIPKhEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS7_vEE5valueEvE4typeERSoS7_, .-_ZN2v84base17PrintCheckOperandIPKhEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS7_vEE5valueEvE4typeERSoS7_
	.section	.text._ZN2v84base17PrintCheckOperandIiEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS5_vEE5valueEvE4typeERSoS5_,"axG",@progbits,_ZN2v84base17PrintCheckOperandIiEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS5_vEE5valueEvE4typeERSoS5_,comdat
	.p2align 4
	.weak	_ZN2v84base17PrintCheckOperandIiEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS5_vEE5valueEvE4typeERSoS5_
	.type	_ZN2v84base17PrintCheckOperandIiEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS5_vEE5valueEvE4typeERSoS5_, @function
_ZN2v84base17PrintCheckOperandIiEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS5_vEE5valueEvE4typeERSoS5_:
.LFB3727:
	.cfi_startproc
	endbr64
	jmp	_ZNSolsEi@PLT
	.cfi_endproc
.LFE3727:
	.size	_ZN2v84base17PrintCheckOperandIiEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS5_vEE5valueEvE4typeERSoS5_, .-_ZN2v84base17PrintCheckOperandIiEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS5_vEE5valueEvE4typeERSoS5_
	.section	.rodata._ZN2v84base17MakeCheckOpStringIiiEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc.str1.1,"aMS",@progbits,1
.LC11:
	.string	" ("
.LC12:
	.string	" vs. "
.LC13:
	.string	")"
	.section	.text._ZN2v84base17MakeCheckOpStringIiiEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc,"axG",@progbits,_ZN2v84base17MakeCheckOpStringIiiEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc,comdat
	.p2align 4
	.weak	_ZN2v84base17MakeCheckOpStringIiiEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc
	.type	_ZN2v84base17MakeCheckOpStringIiiEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc, @function
_ZN2v84base17MakeCheckOpStringIiiEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc:
.LFB3726:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-320(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-432(%rbp), %r12
	pushq	%rbx
	subq	$424, %rsp
	.cfi_offset 3, -56
	movl	%edi, -436(%rbp)
	movq	%r13, %rdi
	movq	.LC14(%rip), %xmm1
	movl	%esi, -440(%rbp)
	movhps	.LC15(%rip), %xmm1
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm1, -464(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	movq	8+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %r15
	xorl	%esi, %esi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	pxor	%xmm0, %xmm0
	xorl	%eax, %eax
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	movq	%r15, -432(%rbp)
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	$0, -104(%rbp)
	movw	%ax, -96(%rbp)
	movq	-24(%r15), %rax
	movq	%rbx, -432(%rbp,%rax)
	movq	-432(%rbp), %rax
	leaq	-368(%rbp), %rbx
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movdqa	-464(%rbp), %xmm1
	pxor	%xmm0, %xmm0
	movq	%rbx, %rdi
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movaps	%xmm0, -416(%rbp)
	movq	%rax, -320(%rbp)
	movaps	%xmm1, -432(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r13, %rdi
	leaq	-424(%rbp), %rsi
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movl	$16, -360(%rbp)
	movq	%rax, -464(%rbp)
	movq	%rax, -352(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	testq	%r14, %r14
	je	.L78
	movq	%r14, %rdi
	call	strlen@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L71:
	movl	$2, %edx
	movq	%r12, %rdi
	leaq	.LC11(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	-436(%rbp), %esi
	movq	%r12, %rdi
	call	_ZN2v84base17PrintCheckOperandIiEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS5_vEE5valueEvE4typeERSoS5_
	movl	$5, %edx
	movq	%r12, %rdi
	leaq	.LC12(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	-440(%rbp), %esi
	movq	%r12, %rdi
	call	_ZN2v84base17PrintCheckOperandIiEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS5_vEE5valueEvE4typeERSoS5_
	movq	%r12, %rdi
	movl	$1, %edx
	leaq	.LC13(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$32, %edi
	call	_Znwm@PLT
	movq	%rax, %r12
	leaq	16(%rax), %rax
	movq	%rax, (%r12)
	movq	-384(%rbp), %rax
	movq	$0, 8(%r12)
	movb	$0, 16(%r12)
	testq	%rax, %rax
	je	.L72
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	ja	.L79
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L74:
	movq	.LC14(%rip), %xmm0
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-352(%rbp), %rdi
	movq	%rax, -320(%rbp)
	movhps	.LC16(%rip), %xmm0
	movaps	%xmm0, -432(%rbp)
	cmpq	-464(%rbp), %rdi
	je	.L75
	call	_ZdlPv@PLT
.L75:
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rbx, %rdi
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	%r15, -432(%rbp)
	movq	-24(%r15), %rax
	movq	%r13, %rdi
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rcx, -432(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L80
	addq	$424, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L79:
	.cfi_restore_state
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L78:
	movq	-432(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L72:
	leaq	-352(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L74
.L80:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3726:
	.size	_ZN2v84base17MakeCheckOpStringIiiEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc, .-_ZN2v84base17MakeCheckOpStringIiiEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc
	.section	.text._ZN2v84base17PrintCheckOperandIlEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS5_vEE5valueEvE4typeERSoS5_,"axG",@progbits,_ZN2v84base17PrintCheckOperandIlEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS5_vEE5valueEvE4typeERSoS5_,comdat
	.p2align 4
	.weak	_ZN2v84base17PrintCheckOperandIlEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS5_vEE5valueEvE4typeERSoS5_
	.type	_ZN2v84base17PrintCheckOperandIlEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS5_vEE5valueEvE4typeERSoS5_, @function
_ZN2v84base17PrintCheckOperandIlEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS5_vEE5valueEvE4typeERSoS5_:
.LFB3730:
	.cfi_startproc
	endbr64
	jmp	_ZNSo9_M_insertIlEERSoT_@PLT
	.cfi_endproc
.LFE3730:
	.size	_ZN2v84base17PrintCheckOperandIlEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS5_vEE5valueEvE4typeERSoS5_, .-_ZN2v84base17PrintCheckOperandIlEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS5_vEE5valueEvE4typeERSoS5_
	.section	.text._ZN2v84base17MakeCheckOpStringIllEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc,"axG",@progbits,_ZN2v84base17MakeCheckOpStringIllEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc,comdat
	.p2align 4
	.weak	_ZN2v84base17MakeCheckOpStringIllEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc
	.type	_ZN2v84base17MakeCheckOpStringIllEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc, @function
_ZN2v84base17MakeCheckOpStringIllEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc:
.LFB3729:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-320(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-432(%rbp), %r12
	pushq	%rbx
	subq	$424, %rsp
	.cfi_offset 3, -56
	movq	%rdi, -440(%rbp)
	movq	.LC14(%rip), %xmm1
	movq	%r13, %rdi
	movq	%rsi, -448(%rbp)
	movhps	.LC15(%rip), %xmm1
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm1, -464(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	movq	8+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %r15
	xorl	%esi, %esi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	pxor	%xmm0, %xmm0
	xorl	%eax, %eax
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	movq	%r15, -432(%rbp)
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	$0, -104(%rbp)
	movw	%ax, -96(%rbp)
	movq	-24(%r15), %rax
	movq	%rbx, -432(%rbp,%rax)
	movq	-432(%rbp), %rax
	leaq	-368(%rbp), %rbx
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movdqa	-464(%rbp), %xmm1
	pxor	%xmm0, %xmm0
	movq	%rbx, %rdi
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movaps	%xmm0, -416(%rbp)
	movq	%rax, -320(%rbp)
	movaps	%xmm1, -432(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r13, %rdi
	leaq	-424(%rbp), %rsi
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movl	$16, -360(%rbp)
	movq	%rax, -464(%rbp)
	movq	%rax, -352(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	testq	%r14, %r14
	je	.L91
	movq	%r14, %rdi
	call	strlen@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L84:
	movl	$2, %edx
	movq	%r12, %rdi
	leaq	.LC11(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-440(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v84base17PrintCheckOperandIlEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS5_vEE5valueEvE4typeERSoS5_
	movl	$5, %edx
	movq	%r12, %rdi
	leaq	.LC12(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-448(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v84base17PrintCheckOperandIlEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS5_vEE5valueEvE4typeERSoS5_
	movq	%r12, %rdi
	movl	$1, %edx
	leaq	.LC13(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$32, %edi
	call	_Znwm@PLT
	movq	%rax, %r12
	leaq	16(%rax), %rax
	movq	%rax, (%r12)
	movq	-384(%rbp), %rax
	movq	$0, 8(%r12)
	movb	$0, 16(%r12)
	testq	%rax, %rax
	je	.L85
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	ja	.L92
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L87:
	movq	.LC14(%rip), %xmm0
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-352(%rbp), %rdi
	movq	%rax, -320(%rbp)
	movhps	.LC16(%rip), %xmm0
	movaps	%xmm0, -432(%rbp)
	cmpq	-464(%rbp), %rdi
	je	.L88
	call	_ZdlPv@PLT
.L88:
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rbx, %rdi
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	%r15, -432(%rbp)
	movq	-24(%r15), %rax
	movq	%r13, %rdi
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rcx, -432(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L93
	addq	$424, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L92:
	.cfi_restore_state
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L91:
	movq	-432(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L85:
	leaq	-352(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L87
.L93:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3729:
	.size	_ZN2v84base17MakeCheckOpStringIllEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc, .-_ZN2v84base17MakeCheckOpStringIllEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc
	.section	.text._ZN2v84base17PrintCheckOperandIxEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS5_vEE5valueEvE4typeERSoS5_,"axG",@progbits,_ZN2v84base17PrintCheckOperandIxEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS5_vEE5valueEvE4typeERSoS5_,comdat
	.p2align 4
	.weak	_ZN2v84base17PrintCheckOperandIxEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS5_vEE5valueEvE4typeERSoS5_
	.type	_ZN2v84base17PrintCheckOperandIxEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS5_vEE5valueEvE4typeERSoS5_, @function
_ZN2v84base17PrintCheckOperandIxEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS5_vEE5valueEvE4typeERSoS5_:
.LFB3733:
	.cfi_startproc
	endbr64
	jmp	_ZNSo9_M_insertIxEERSoT_@PLT
	.cfi_endproc
.LFE3733:
	.size	_ZN2v84base17PrintCheckOperandIxEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS5_vEE5valueEvE4typeERSoS5_, .-_ZN2v84base17PrintCheckOperandIxEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS5_vEE5valueEvE4typeERSoS5_
	.section	.text._ZN2v84base17MakeCheckOpStringIxxEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc,"axG",@progbits,_ZN2v84base17MakeCheckOpStringIxxEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc,comdat
	.p2align 4
	.weak	_ZN2v84base17MakeCheckOpStringIxxEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc
	.type	_ZN2v84base17MakeCheckOpStringIxxEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc, @function
_ZN2v84base17MakeCheckOpStringIxxEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc:
.LFB3732:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-320(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-432(%rbp), %r12
	pushq	%rbx
	subq	$424, %rsp
	.cfi_offset 3, -56
	movq	%rdi, -440(%rbp)
	movq	.LC14(%rip), %xmm1
	movq	%r13, %rdi
	movq	%rsi, -448(%rbp)
	movhps	.LC15(%rip), %xmm1
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm1, -464(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	movq	8+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %r15
	xorl	%esi, %esi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	pxor	%xmm0, %xmm0
	xorl	%eax, %eax
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	movq	%r15, -432(%rbp)
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	$0, -104(%rbp)
	movw	%ax, -96(%rbp)
	movq	-24(%r15), %rax
	movq	%rbx, -432(%rbp,%rax)
	movq	-432(%rbp), %rax
	leaq	-368(%rbp), %rbx
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movdqa	-464(%rbp), %xmm1
	pxor	%xmm0, %xmm0
	movq	%rbx, %rdi
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movaps	%xmm0, -416(%rbp)
	movq	%rax, -320(%rbp)
	movaps	%xmm1, -432(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r13, %rdi
	leaq	-424(%rbp), %rsi
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movl	$16, -360(%rbp)
	movq	%rax, -464(%rbp)
	movq	%rax, -352(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	testq	%r14, %r14
	je	.L104
	movq	%r14, %rdi
	call	strlen@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L97:
	movl	$2, %edx
	movq	%r12, %rdi
	leaq	.LC11(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-440(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v84base17PrintCheckOperandIxEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS5_vEE5valueEvE4typeERSoS5_
	movl	$5, %edx
	movq	%r12, %rdi
	leaq	.LC12(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-448(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v84base17PrintCheckOperandIxEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS5_vEE5valueEvE4typeERSoS5_
	movq	%r12, %rdi
	movl	$1, %edx
	leaq	.LC13(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$32, %edi
	call	_Znwm@PLT
	movq	%rax, %r12
	leaq	16(%rax), %rax
	movq	%rax, (%r12)
	movq	-384(%rbp), %rax
	movq	$0, 8(%r12)
	movb	$0, 16(%r12)
	testq	%rax, %rax
	je	.L98
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	ja	.L105
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L100:
	movq	.LC14(%rip), %xmm0
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-352(%rbp), %rdi
	movq	%rax, -320(%rbp)
	movhps	.LC16(%rip), %xmm0
	movaps	%xmm0, -432(%rbp)
	cmpq	-464(%rbp), %rdi
	je	.L101
	call	_ZdlPv@PLT
.L101:
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rbx, %rdi
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	%r15, -432(%rbp)
	movq	-24(%r15), %rax
	movq	%r13, %rdi
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rcx, -432(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L106
	addq	$424, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L105:
	.cfi_restore_state
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L104:
	movq	-432(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L98:
	leaq	-352(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L100
.L106:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3732:
	.size	_ZN2v84base17MakeCheckOpStringIxxEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc, .-_ZN2v84base17MakeCheckOpStringIxxEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc
	.section	.text._ZN2v84base17PrintCheckOperandIjEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS5_vEE5valueEvE4typeERSoS5_,"axG",@progbits,_ZN2v84base17PrintCheckOperandIjEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS5_vEE5valueEvE4typeERSoS5_,comdat
	.p2align 4
	.weak	_ZN2v84base17PrintCheckOperandIjEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS5_vEE5valueEvE4typeERSoS5_
	.type	_ZN2v84base17PrintCheckOperandIjEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS5_vEE5valueEvE4typeERSoS5_, @function
_ZN2v84base17PrintCheckOperandIjEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS5_vEE5valueEvE4typeERSoS5_:
.LFB3736:
	.cfi_startproc
	endbr64
	movl	%esi, %esi
	jmp	_ZNSo9_M_insertImEERSoT_@PLT
	.cfi_endproc
.LFE3736:
	.size	_ZN2v84base17PrintCheckOperandIjEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS5_vEE5valueEvE4typeERSoS5_, .-_ZN2v84base17PrintCheckOperandIjEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS5_vEE5valueEvE4typeERSoS5_
	.section	.text._ZN2v84base17MakeCheckOpStringIjjEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc,"axG",@progbits,_ZN2v84base17MakeCheckOpStringIjjEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc,comdat
	.p2align 4
	.weak	_ZN2v84base17MakeCheckOpStringIjjEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc
	.type	_ZN2v84base17MakeCheckOpStringIjjEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc, @function
_ZN2v84base17MakeCheckOpStringIjjEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc:
.LFB3735:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-320(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-432(%rbp), %r12
	pushq	%rbx
	subq	$424, %rsp
	.cfi_offset 3, -56
	movl	%edi, -436(%rbp)
	movq	%r13, %rdi
	movq	.LC14(%rip), %xmm1
	movl	%esi, -440(%rbp)
	movhps	.LC15(%rip), %xmm1
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm1, -464(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	movq	8+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %r15
	xorl	%esi, %esi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	pxor	%xmm0, %xmm0
	xorl	%eax, %eax
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	movq	%r15, -432(%rbp)
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	$0, -104(%rbp)
	movw	%ax, -96(%rbp)
	movq	-24(%r15), %rax
	movq	%rbx, -432(%rbp,%rax)
	movq	-432(%rbp), %rax
	leaq	-368(%rbp), %rbx
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movdqa	-464(%rbp), %xmm1
	pxor	%xmm0, %xmm0
	movq	%rbx, %rdi
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movaps	%xmm0, -416(%rbp)
	movq	%rax, -320(%rbp)
	movaps	%xmm1, -432(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r13, %rdi
	leaq	-424(%rbp), %rsi
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movl	$16, -360(%rbp)
	movq	%rax, -464(%rbp)
	movq	%rax, -352(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	testq	%r14, %r14
	je	.L117
	movq	%r14, %rdi
	call	strlen@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L110:
	movl	$2, %edx
	movq	%r12, %rdi
	leaq	.LC11(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	-436(%rbp), %esi
	movq	%r12, %rdi
	call	_ZN2v84base17PrintCheckOperandIjEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS5_vEE5valueEvE4typeERSoS5_
	movl	$5, %edx
	movq	%r12, %rdi
	leaq	.LC12(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	-440(%rbp), %esi
	movq	%r12, %rdi
	call	_ZN2v84base17PrintCheckOperandIjEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS5_vEE5valueEvE4typeERSoS5_
	movq	%r12, %rdi
	movl	$1, %edx
	leaq	.LC13(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$32, %edi
	call	_Znwm@PLT
	movq	%rax, %r12
	leaq	16(%rax), %rax
	movq	%rax, (%r12)
	movq	-384(%rbp), %rax
	movq	$0, 8(%r12)
	movb	$0, 16(%r12)
	testq	%rax, %rax
	je	.L111
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	ja	.L118
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L113:
	movq	.LC14(%rip), %xmm0
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-352(%rbp), %rdi
	movq	%rax, -320(%rbp)
	movhps	.LC16(%rip), %xmm0
	movaps	%xmm0, -432(%rbp)
	cmpq	-464(%rbp), %rdi
	je	.L114
	call	_ZdlPv@PLT
.L114:
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rbx, %rdi
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	%r15, -432(%rbp)
	movq	-24(%r15), %rax
	movq	%r13, %rdi
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rcx, -432(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L119
	addq	$424, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L118:
	.cfi_restore_state
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L117:
	movq	-432(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L110
	.p2align 4,,10
	.p2align 3
.L111:
	leaq	-352(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L113
.L119:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3735:
	.size	_ZN2v84base17MakeCheckOpStringIjjEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc, .-_ZN2v84base17MakeCheckOpStringIjjEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc
	.section	.text._ZN2v84base17PrintCheckOperandImEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS5_vEE5valueEvE4typeERSoS5_,"axG",@progbits,_ZN2v84base17PrintCheckOperandImEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS5_vEE5valueEvE4typeERSoS5_,comdat
	.p2align 4
	.weak	_ZN2v84base17PrintCheckOperandImEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS5_vEE5valueEvE4typeERSoS5_
	.type	_ZN2v84base17PrintCheckOperandImEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS5_vEE5valueEvE4typeERSoS5_, @function
_ZN2v84base17PrintCheckOperandImEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS5_vEE5valueEvE4typeERSoS5_:
.LFB3739:
	.cfi_startproc
	endbr64
	jmp	_ZNSo9_M_insertImEERSoT_@PLT
	.cfi_endproc
.LFE3739:
	.size	_ZN2v84base17PrintCheckOperandImEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS5_vEE5valueEvE4typeERSoS5_, .-_ZN2v84base17PrintCheckOperandImEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS5_vEE5valueEvE4typeERSoS5_
	.section	.text._ZN2v84base17MakeCheckOpStringImmEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc,"axG",@progbits,_ZN2v84base17MakeCheckOpStringImmEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc,comdat
	.p2align 4
	.weak	_ZN2v84base17MakeCheckOpStringImmEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc
	.type	_ZN2v84base17MakeCheckOpStringImmEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc, @function
_ZN2v84base17MakeCheckOpStringImmEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc:
.LFB3738:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-320(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-432(%rbp), %r12
	pushq	%rbx
	subq	$424, %rsp
	.cfi_offset 3, -56
	movq	%rdi, -440(%rbp)
	movq	.LC14(%rip), %xmm1
	movq	%r13, %rdi
	movq	%rsi, -448(%rbp)
	movhps	.LC15(%rip), %xmm1
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm1, -464(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	movq	8+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %r15
	xorl	%esi, %esi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	pxor	%xmm0, %xmm0
	xorl	%eax, %eax
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	movq	%r15, -432(%rbp)
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	$0, -104(%rbp)
	movw	%ax, -96(%rbp)
	movq	-24(%r15), %rax
	movq	%rbx, -432(%rbp,%rax)
	movq	-432(%rbp), %rax
	leaq	-368(%rbp), %rbx
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movdqa	-464(%rbp), %xmm1
	pxor	%xmm0, %xmm0
	movq	%rbx, %rdi
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movaps	%xmm0, -416(%rbp)
	movq	%rax, -320(%rbp)
	movaps	%xmm1, -432(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r13, %rdi
	leaq	-424(%rbp), %rsi
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movl	$16, -360(%rbp)
	movq	%rax, -464(%rbp)
	movq	%rax, -352(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	testq	%r14, %r14
	je	.L130
	movq	%r14, %rdi
	call	strlen@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L123:
	movl	$2, %edx
	movq	%r12, %rdi
	leaq	.LC11(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-440(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v84base17PrintCheckOperandImEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS5_vEE5valueEvE4typeERSoS5_
	movl	$5, %edx
	movq	%r12, %rdi
	leaq	.LC12(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-448(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v84base17PrintCheckOperandImEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS5_vEE5valueEvE4typeERSoS5_
	movq	%r12, %rdi
	movl	$1, %edx
	leaq	.LC13(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$32, %edi
	call	_Znwm@PLT
	movq	%rax, %r12
	leaq	16(%rax), %rax
	movq	%rax, (%r12)
	movq	-384(%rbp), %rax
	movq	$0, 8(%r12)
	movb	$0, 16(%r12)
	testq	%rax, %rax
	je	.L124
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	ja	.L131
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L126:
	movq	.LC14(%rip), %xmm0
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-352(%rbp), %rdi
	movq	%rax, -320(%rbp)
	movhps	.LC16(%rip), %xmm0
	movaps	%xmm0, -432(%rbp)
	cmpq	-464(%rbp), %rdi
	je	.L127
	call	_ZdlPv@PLT
.L127:
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rbx, %rdi
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	%r15, -432(%rbp)
	movq	-24(%r15), %rax
	movq	%r13, %rdi
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rcx, -432(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L132
	addq	$424, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L131:
	.cfi_restore_state
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L130:
	movq	-432(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L124:
	leaq	-352(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L126
.L132:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3738:
	.size	_ZN2v84base17MakeCheckOpStringImmEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc, .-_ZN2v84base17MakeCheckOpStringImmEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc
	.section	.text._ZN2v84base17PrintCheckOperandIyEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS5_vEE5valueEvE4typeERSoS5_,"axG",@progbits,_ZN2v84base17PrintCheckOperandIyEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS5_vEE5valueEvE4typeERSoS5_,comdat
	.p2align 4
	.weak	_ZN2v84base17PrintCheckOperandIyEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS5_vEE5valueEvE4typeERSoS5_
	.type	_ZN2v84base17PrintCheckOperandIyEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS5_vEE5valueEvE4typeERSoS5_, @function
_ZN2v84base17PrintCheckOperandIyEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS5_vEE5valueEvE4typeERSoS5_:
.LFB3742:
	.cfi_startproc
	endbr64
	jmp	_ZNSo9_M_insertIyEERSoT_@PLT
	.cfi_endproc
.LFE3742:
	.size	_ZN2v84base17PrintCheckOperandIyEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS5_vEE5valueEvE4typeERSoS5_, .-_ZN2v84base17PrintCheckOperandIyEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS5_vEE5valueEvE4typeERSoS5_
	.section	.text._ZN2v84base17MakeCheckOpStringIyyEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc,"axG",@progbits,_ZN2v84base17MakeCheckOpStringIyyEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc,comdat
	.p2align 4
	.weak	_ZN2v84base17MakeCheckOpStringIyyEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc
	.type	_ZN2v84base17MakeCheckOpStringIyyEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc, @function
_ZN2v84base17MakeCheckOpStringIyyEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc:
.LFB3741:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-320(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-432(%rbp), %r12
	pushq	%rbx
	subq	$424, %rsp
	.cfi_offset 3, -56
	movq	%rdi, -440(%rbp)
	movq	.LC14(%rip), %xmm1
	movq	%r13, %rdi
	movq	%rsi, -448(%rbp)
	movhps	.LC15(%rip), %xmm1
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm1, -464(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	movq	8+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %r15
	xorl	%esi, %esi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	pxor	%xmm0, %xmm0
	xorl	%eax, %eax
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	movq	%r15, -432(%rbp)
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	$0, -104(%rbp)
	movw	%ax, -96(%rbp)
	movq	-24(%r15), %rax
	movq	%rbx, -432(%rbp,%rax)
	movq	-432(%rbp), %rax
	leaq	-368(%rbp), %rbx
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movdqa	-464(%rbp), %xmm1
	pxor	%xmm0, %xmm0
	movq	%rbx, %rdi
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movaps	%xmm0, -416(%rbp)
	movq	%rax, -320(%rbp)
	movaps	%xmm1, -432(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r13, %rdi
	leaq	-424(%rbp), %rsi
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movl	$16, -360(%rbp)
	movq	%rax, -464(%rbp)
	movq	%rax, -352(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	testq	%r14, %r14
	je	.L143
	movq	%r14, %rdi
	call	strlen@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L136:
	movl	$2, %edx
	movq	%r12, %rdi
	leaq	.LC11(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-440(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v84base17PrintCheckOperandIyEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS5_vEE5valueEvE4typeERSoS5_
	movl	$5, %edx
	movq	%r12, %rdi
	leaq	.LC12(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-448(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v84base17PrintCheckOperandIyEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS5_vEE5valueEvE4typeERSoS5_
	movq	%r12, %rdi
	movl	$1, %edx
	leaq	.LC13(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$32, %edi
	call	_Znwm@PLT
	movq	%rax, %r12
	leaq	16(%rax), %rax
	movq	%rax, (%r12)
	movq	-384(%rbp), %rax
	movq	$0, 8(%r12)
	movb	$0, 16(%r12)
	testq	%rax, %rax
	je	.L137
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	ja	.L144
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L139:
	movq	.LC14(%rip), %xmm0
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-352(%rbp), %rdi
	movq	%rax, -320(%rbp)
	movhps	.LC16(%rip), %xmm0
	movaps	%xmm0, -432(%rbp)
	cmpq	-464(%rbp), %rdi
	je	.L140
	call	_ZdlPv@PLT
.L140:
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rbx, %rdi
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	%r15, -432(%rbp)
	movq	-24(%r15), %rax
	movq	%r13, %rdi
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rcx, -432(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L145
	addq	$424, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L144:
	.cfi_restore_state
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L139
	.p2align 4,,10
	.p2align 3
.L143:
	movq	-432(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L137:
	leaq	-352(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L139
.L145:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3741:
	.size	_ZN2v84base17MakeCheckOpStringIyyEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc, .-_ZN2v84base17MakeCheckOpStringIyyEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc
	.section	.text._ZN2v84base17PrintCheckOperandIPKvEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS7_vEE5valueEvE4typeERSoS7_,"axG",@progbits,_ZN2v84base17PrintCheckOperandIPKvEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS7_vEE5valueEvE4typeERSoS7_,comdat
	.p2align 4
	.weak	_ZN2v84base17PrintCheckOperandIPKvEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS7_vEE5valueEvE4typeERSoS7_
	.type	_ZN2v84base17PrintCheckOperandIPKvEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS7_vEE5valueEvE4typeERSoS7_, @function
_ZN2v84base17PrintCheckOperandIPKvEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS7_vEE5valueEvE4typeERSoS7_:
.LFB3745:
	.cfi_startproc
	endbr64
	jmp	_ZNSo9_M_insertIPKvEERSoT_@PLT
	.cfi_endproc
.LFE3745:
	.size	_ZN2v84base17PrintCheckOperandIPKvEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS7_vEE5valueEvE4typeERSoS7_, .-_ZN2v84base17PrintCheckOperandIPKvEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS7_vEE5valueEvE4typeERSoS7_
	.section	.text._ZN2v84base17MakeCheckOpStringIPKvS3_EEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc,"axG",@progbits,_ZN2v84base17MakeCheckOpStringIPKvS3_EEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc,comdat
	.p2align 4
	.weak	_ZN2v84base17MakeCheckOpStringIPKvS3_EEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc
	.type	_ZN2v84base17MakeCheckOpStringIPKvS3_EEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc, @function
_ZN2v84base17MakeCheckOpStringIPKvS3_EEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc:
.LFB3744:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-320(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-432(%rbp), %r12
	pushq	%rbx
	subq	$424, %rsp
	.cfi_offset 3, -56
	movq	%rdi, -440(%rbp)
	movq	.LC14(%rip), %xmm1
	movq	%r13, %rdi
	movq	%rsi, -448(%rbp)
	movhps	.LC15(%rip), %xmm1
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm1, -464(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	movq	8+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %r15
	xorl	%esi, %esi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	pxor	%xmm0, %xmm0
	xorl	%eax, %eax
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	movq	%r15, -432(%rbp)
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	$0, -104(%rbp)
	movw	%ax, -96(%rbp)
	movq	-24(%r15), %rax
	movq	%rbx, -432(%rbp,%rax)
	movq	-432(%rbp), %rax
	leaq	-368(%rbp), %rbx
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movdqa	-464(%rbp), %xmm1
	pxor	%xmm0, %xmm0
	movq	%rbx, %rdi
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movaps	%xmm0, -416(%rbp)
	movq	%rax, -320(%rbp)
	movaps	%xmm1, -432(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r13, %rdi
	leaq	-424(%rbp), %rsi
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movl	$16, -360(%rbp)
	movq	%rax, -464(%rbp)
	movq	%rax, -352(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	testq	%r14, %r14
	je	.L156
	movq	%r14, %rdi
	call	strlen@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L149:
	movl	$2, %edx
	movq	%r12, %rdi
	leaq	.LC11(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-440(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v84base17PrintCheckOperandIPKvEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS7_vEE5valueEvE4typeERSoS7_
	movl	$5, %edx
	movq	%r12, %rdi
	leaq	.LC12(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-448(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v84base17PrintCheckOperandIPKvEENSt9enable_ifIXaantsrSt11is_functionINSt14remove_pointerIT_E4typeEE5valuesrNS0_19has_output_operatorIS7_vEE5valueEvE4typeERSoS7_
	movq	%r12, %rdi
	movl	$1, %edx
	leaq	.LC13(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$32, %edi
	call	_Znwm@PLT
	movq	%rax, %r12
	leaq	16(%rax), %rax
	movq	%rax, (%r12)
	movq	-384(%rbp), %rax
	movq	$0, 8(%r12)
	movb	$0, 16(%r12)
	testq	%rax, %rax
	je	.L150
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	ja	.L157
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L152:
	movq	.LC14(%rip), %xmm0
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-352(%rbp), %rdi
	movq	%rax, -320(%rbp)
	movhps	.LC16(%rip), %xmm0
	movaps	%xmm0, -432(%rbp)
	cmpq	-464(%rbp), %rdi
	je	.L153
	call	_ZdlPv@PLT
.L153:
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rbx, %rdi
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	%r15, -432(%rbp)
	movq	-24(%r15), %rax
	movq	%r13, %rdi
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rcx, -432(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L158
	addq	$424, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L157:
	.cfi_restore_state
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L152
	.p2align 4,,10
	.p2align 3
.L156:
	movq	-432(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L150:
	leaq	-352(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L152
.L158:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3744:
	.size	_ZN2v84base17MakeCheckOpStringIPKvS3_EEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc, .-_ZN2v84base17MakeCheckOpStringIPKvS3_EEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc
	.section	.text._ZN2v84base11CheckEQImplIffEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc,"axG",@progbits,_ZN2v84base11CheckEQImplIffEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc,comdat
	.p2align 4
	.weak	_ZN2v84base11CheckEQImplIffEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc
	.type	_ZN2v84base11CheckEQImplIffEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc, @function
_ZN2v84base11CheckEQImplIffEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc:
.LFB3747:
	.cfi_startproc
	endbr64
	ucomiss	%xmm1, %xmm0
	jp	.L162
	jne	.L162
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L162:
	jmp	_ZN2v84base17MakeCheckOpStringIffEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc
	.cfi_endproc
.LFE3747:
	.size	_ZN2v84base11CheckEQImplIffEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc, .-_ZN2v84base11CheckEQImplIffEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc
	.section	.text._ZN2v84base11CheckEQImplIddEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc,"axG",@progbits,_ZN2v84base11CheckEQImplIddEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc,comdat
	.p2align 4
	.weak	_ZN2v84base11CheckEQImplIddEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc
	.type	_ZN2v84base11CheckEQImplIddEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc, @function
_ZN2v84base11CheckEQImplIddEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc:
.LFB3748:
	.cfi_startproc
	endbr64
	ucomisd	%xmm1, %xmm0
	jp	.L167
	jne	.L167
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L167:
	jmp	_ZN2v84base17MakeCheckOpStringIddEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc
	.cfi_endproc
.LFE3748:
	.size	_ZN2v84base11CheckEQImplIddEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc, .-_ZN2v84base11CheckEQImplIddEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc
	.section	.text._ZN2v84base11CheckNEImplIffEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc,"axG",@progbits,_ZN2v84base11CheckNEImplIffEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc,comdat
	.p2align 4
	.weak	_ZN2v84base11CheckNEImplIffEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc
	.type	_ZN2v84base11CheckNEImplIffEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc, @function
_ZN2v84base11CheckNEImplIffEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc:
.LFB3749:
	.cfi_startproc
	endbr64
	ucomiss	%xmm1, %xmm0
	jnp	.L173
.L170:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L173:
	jne	.L170
	jmp	_ZN2v84base17MakeCheckOpStringIffEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc
	.cfi_endproc
.LFE3749:
	.size	_ZN2v84base11CheckNEImplIffEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc, .-_ZN2v84base11CheckNEImplIffEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc
	.section	.text._ZN2v84base11CheckNEImplIddEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc,"axG",@progbits,_ZN2v84base11CheckNEImplIddEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc,comdat
	.p2align 4
	.weak	_ZN2v84base11CheckNEImplIddEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc
	.type	_ZN2v84base11CheckNEImplIddEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc, @function
_ZN2v84base11CheckNEImplIddEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc:
.LFB3750:
	.cfi_startproc
	endbr64
	ucomisd	%xmm1, %xmm0
	jnp	.L178
.L175:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L178:
	jne	.L175
	jmp	_ZN2v84base17MakeCheckOpStringIddEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc
	.cfi_endproc
.LFE3750:
	.size	_ZN2v84base11CheckNEImplIddEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc, .-_ZN2v84base11CheckNEImplIddEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc
	.section	.text._ZN2v84base11CheckLEImplIffEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc,"axG",@progbits,_ZN2v84base11CheckLEImplIffEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc,comdat
	.p2align 4
	.weak	_ZN2v84base11CheckLEImplIffEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc
	.type	_ZN2v84base11CheckLEImplIffEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc, @function
_ZN2v84base11CheckLEImplIffEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc:
.LFB3751:
	.cfi_startproc
	endbr64
	comiss	%xmm0, %xmm1
	jb	.L181
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L181:
	jmp	_ZN2v84base17MakeCheckOpStringIffEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc
	.cfi_endproc
.LFE3751:
	.size	_ZN2v84base11CheckLEImplIffEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc, .-_ZN2v84base11CheckLEImplIffEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc
	.section	.text._ZN2v84base11CheckLEImplIddEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc,"axG",@progbits,_ZN2v84base11CheckLEImplIddEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc,comdat
	.p2align 4
	.weak	_ZN2v84base11CheckLEImplIddEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc
	.type	_ZN2v84base11CheckLEImplIddEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc, @function
_ZN2v84base11CheckLEImplIddEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc:
.LFB3752:
	.cfi_startproc
	endbr64
	comisd	%xmm0, %xmm1
	jb	.L184
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L184:
	jmp	_ZN2v84base17MakeCheckOpStringIddEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc
	.cfi_endproc
.LFE3752:
	.size	_ZN2v84base11CheckLEImplIddEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc, .-_ZN2v84base11CheckLEImplIddEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc
	.section	.text._ZN2v84base11CheckLTImplIffEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc,"axG",@progbits,_ZN2v84base11CheckLTImplIffEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc,comdat
	.p2align 4
	.weak	_ZN2v84base11CheckLTImplIffEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc
	.type	_ZN2v84base11CheckLTImplIffEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc, @function
_ZN2v84base11CheckLTImplIffEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc:
.LFB3753:
	.cfi_startproc
	endbr64
	comiss	%xmm0, %xmm1
	jbe	.L187
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L187:
	jmp	_ZN2v84base17MakeCheckOpStringIffEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc
	.cfi_endproc
.LFE3753:
	.size	_ZN2v84base11CheckLTImplIffEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc, .-_ZN2v84base11CheckLTImplIffEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc
	.section	.text._ZN2v84base11CheckLTImplIddEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc,"axG",@progbits,_ZN2v84base11CheckLTImplIddEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc,comdat
	.p2align 4
	.weak	_ZN2v84base11CheckLTImplIddEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc
	.type	_ZN2v84base11CheckLTImplIddEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc, @function
_ZN2v84base11CheckLTImplIddEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc:
.LFB3754:
	.cfi_startproc
	endbr64
	comisd	%xmm0, %xmm1
	jbe	.L190
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L190:
	jmp	_ZN2v84base17MakeCheckOpStringIddEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc
	.cfi_endproc
.LFE3754:
	.size	_ZN2v84base11CheckLTImplIddEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc, .-_ZN2v84base11CheckLTImplIddEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc
	.section	.text._ZN2v84base11CheckGEImplIffEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc,"axG",@progbits,_ZN2v84base11CheckGEImplIffEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc,comdat
	.p2align 4
	.weak	_ZN2v84base11CheckGEImplIffEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc
	.type	_ZN2v84base11CheckGEImplIffEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc, @function
_ZN2v84base11CheckGEImplIffEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc:
.LFB3755:
	.cfi_startproc
	endbr64
	comiss	%xmm1, %xmm0
	jb	.L193
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L193:
	jmp	_ZN2v84base17MakeCheckOpStringIffEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc
	.cfi_endproc
.LFE3755:
	.size	_ZN2v84base11CheckGEImplIffEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc, .-_ZN2v84base11CheckGEImplIffEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc
	.section	.text._ZN2v84base11CheckGEImplIddEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc,"axG",@progbits,_ZN2v84base11CheckGEImplIddEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc,comdat
	.p2align 4
	.weak	_ZN2v84base11CheckGEImplIddEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc
	.type	_ZN2v84base11CheckGEImplIddEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc, @function
_ZN2v84base11CheckGEImplIddEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc:
.LFB3756:
	.cfi_startproc
	endbr64
	comisd	%xmm1, %xmm0
	jb	.L196
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L196:
	jmp	_ZN2v84base17MakeCheckOpStringIddEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc
	.cfi_endproc
.LFE3756:
	.size	_ZN2v84base11CheckGEImplIddEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc, .-_ZN2v84base11CheckGEImplIddEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc
	.section	.text._ZN2v84base11CheckGTImplIffEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc,"axG",@progbits,_ZN2v84base11CheckGTImplIffEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc,comdat
	.p2align 4
	.weak	_ZN2v84base11CheckGTImplIffEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc
	.type	_ZN2v84base11CheckGTImplIffEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc, @function
_ZN2v84base11CheckGTImplIffEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc:
.LFB3757:
	.cfi_startproc
	endbr64
	comiss	%xmm1, %xmm0
	jbe	.L199
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L199:
	jmp	_ZN2v84base17MakeCheckOpStringIffEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc
	.cfi_endproc
.LFE3757:
	.size	_ZN2v84base11CheckGTImplIffEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc, .-_ZN2v84base11CheckGTImplIffEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc
	.section	.text._ZN2v84base11CheckGTImplIddEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc,"axG",@progbits,_ZN2v84base11CheckGTImplIddEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc,comdat
	.p2align 4
	.weak	_ZN2v84base11CheckGTImplIddEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc
	.type	_ZN2v84base11CheckGTImplIddEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc, @function
_ZN2v84base11CheckGTImplIddEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc:
.LFB3758:
	.cfi_startproc
	endbr64
	comisd	%xmm1, %xmm0
	jbe	.L202
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L202:
	jmp	_ZN2v84base17MakeCheckOpStringIddEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc
	.cfi_endproc
.LFE3758:
	.size	_ZN2v84base11CheckGTImplIddEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc, .-_ZN2v84base11CheckGTImplIddEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc
	.section	.rodata._Z8V8_FatalPKcz.str1.1,"aMS",@progbits,1
.LC17:
	.string	""
	.section	.rodata._Z8V8_FatalPKcz.str1.8,"aMS",@progbits,1
	.align 8
.LC18:
	.string	"\n\n#\n# Fatal error in %s, line %d\n# "
	.align 8
.LC19:
	.string	"\n#\n#\n#\n#FailureMessage Object: %p"
	.section	.text._Z8V8_FatalPKcz,"ax",@progbits
	.p2align 4
	.globl	_Z8V8_FatalPKcz
	.type	_Z8V8_FatalPKcz, @function
_Z8V8_FatalPKcz:
.LFB3458:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$760, %rsp
	movq	%rsi, -200(%rbp)
	movq	%rdx, -192(%rbp)
	movq	%rcx, -184(%rbp)
	movq	%r8, -176(%rbp)
	movq	%r9, -168(%rbp)
	testb	%al, %al
	je	.L204
	movaps	%xmm0, -160(%rbp)
	movaps	%xmm1, -144(%rbp)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm3, -112(%rbp)
	movaps	%xmm4, -96(%rbp)
	movaps	%xmm5, -80(%rbp)
	movaps	%xmm6, -64(%rbp)
	movaps	%xmm7, -48(%rbp)
.L204:
	movq	%fs:40, %rax
	movq	%rax, -216(%rbp)
	xorl	%eax, %eax
	leaq	16(%rbp), %rax
	leaq	-744(%rbp), %r8
	movq	%r12, %rdx
	movq	%rax, -768(%rbp)
	movq	%r8, %rdi
	leaq	-208(%rbp), %rax
	movl	$64, %ecx
	movq	%rax, -760(%rbp)
	movl	$3737837072, %eax
	leaq	-776(%rbp), %r13
	movl	$512, %esi
	movq	%rax, -752(%rbp)
	addq	$1, %rax
	leaq	-752(%rbp), %r14
	movq	%rax, -232(%rbp)
	xorl	%eax, %eax
	rep stosq
	movq	%r8, %rdi
	movq	%r13, %rcx
	movl	$8, -776(%rbp)
	movl	$48, -772(%rbp)
	call	_ZN2v84base2OS9VSNPrintFEPciPKcP13__va_list_tag@PLT
	movq	stdout(%rip), %rdi
	call	fflush@PLT
	movq	stderr(%rip), %rdi
	call	fflush@PLT
	xorl	%edx, %edx
	leaq	.LC17(%rip), %rsi
	xorl	%eax, %eax
	leaq	.LC18(%rip), %rdi
	call	_ZN2v84base2OS10PrintErrorEPKcz@PLT
	leaq	16(%rbp), %rax
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -768(%rbp)
	leaq	-208(%rbp), %rax
	movq	%rax, -760(%rbp)
	movl	$8, -776(%rbp)
	movl	$48, -772(%rbp)
	call	_ZN2v84base2OS11VPrintErrorEPKcP13__va_list_tag@PLT
	xorl	%eax, %eax
	movq	%r14, %rsi
	leaq	.LC19(%rip), %rdi
	call	_ZN2v84base2OS10PrintErrorEPKcz@PLT
	movq	_ZN2v84base12_GLOBAL__N_119g_print_stack_traceE(%rip), %rax
	testq	%rax, %rax
	je	.L205
	call	*%rax
.L205:
	movq	stderr(%rip), %rdi
	call	fflush@PLT
	call	_ZN2v84base2OS5AbortEv@PLT
	.cfi_endproc
.LFE3458:
	.size	_Z8V8_FatalPKcz, .-_Z8V8_FatalPKcz
	.section	.rodata._ZN2v84base12_GLOBAL__N_120DefaultDcheckHandlerEPKciS3_.str1.1,"aMS",@progbits,1
.LC20:
	.string	"Debug check failed: %s."
	.section	.text._ZN2v84base12_GLOBAL__N_120DefaultDcheckHandlerEPKciS3_,"ax",@progbits
	.p2align 4
	.type	_ZN2v84base12_GLOBAL__N_120DefaultDcheckHandlerEPKciS3_, @function
_ZN2v84base12_GLOBAL__N_120DefaultDcheckHandlerEPKciS3_:
.LFB3443:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %rsi
	leaq	.LC20(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz
	.cfi_endproc
.LFE3443:
	.size	_ZN2v84base12_GLOBAL__N_120DefaultDcheckHandlerEPKciS3_, .-_ZN2v84base12_GLOBAL__N_120DefaultDcheckHandlerEPKciS3_
	.section	.text._Z9V8_DcheckPKciS0_,"ax",@progbits
	.p2align 4
	.globl	_Z9V8_DcheckPKciS0_
	.type	_Z9V8_DcheckPKciS0_, @function
_Z9V8_DcheckPKciS0_:
.LFB3459:
	.cfi_startproc
	endbr64
	jmp	*_ZN2v84base12_GLOBAL__N_117g_dcheck_functionE(%rip)
	.cfi_endproc
.LFE3459:
	.size	_Z9V8_DcheckPKciS0_, .-_Z9V8_DcheckPKciS0_
	.section	.text._ZN2v84base17MakeCheckOpStringIffEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc,"axG",@progbits,_ZN2v84base17MakeCheckOpStringIffEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc,comdat
	.p2align 4
	.weak	_ZN2v84base17MakeCheckOpStringIffEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc
	.type	_ZN2v84base17MakeCheckOpStringIffEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc, @function
_ZN2v84base17MakeCheckOpStringIffEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc:
.LFB3890:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-368(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-320(%rbp), %r13
	pushq	%r12
	movq	%r13, %rdi
	.cfi_offset 12, -48
	leaq	-432(%rbp), %r12
	pushq	%rbx
	subq	$424, %rsp
	.cfi_offset 3, -56
	movss	%xmm1, -440(%rbp)
	movq	.LC14(%rip), %xmm1
	movss	%xmm0, -436(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movhps	.LC15(%rip), %xmm1
	movaps	%xmm1, -464(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	movq	8+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	xorl	%esi, %esi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	pxor	%xmm0, %xmm0
	xorl	%eax, %eax
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rbx, -432(%rbp)
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	$0, -104(%rbp)
	movw	%ax, -96(%rbp)
	movq	-24(%rbx), %rax
	movq	%rcx, -432(%rbp,%rax)
	movq	-432(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movdqa	-464(%rbp), %xmm1
	pxor	%xmm0, %xmm0
	movq	%r14, %rdi
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movaps	%xmm0, -416(%rbp)
	movq	%rax, -320(%rbp)
	movaps	%xmm1, -432(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r13, %rdi
	leaq	-424(%rbp), %rsi
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movl	$16, -360(%rbp)
	movq	%rax, -464(%rbp)
	movq	%rax, -352(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	testq	%r15, %r15
	je	.L223
	movq	%r15, %rdi
	call	strlen@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L216:
	movl	$2, %edx
	leaq	.LC11(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r12, %rdi
	pxor	%xmm0, %xmm0
	cvtss2sd	-436(%rbp), %xmm0
	call	_ZNSo9_M_insertIdEERSoT_@PLT
	movq	%r12, %rdi
	movl	$5, %edx
	leaq	.LC12(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r12, %rdi
	pxor	%xmm0, %xmm0
	cvtss2sd	-440(%rbp), %xmm0
	call	_ZNSo9_M_insertIdEERSoT_@PLT
	movq	%r12, %rdi
	movl	$1, %edx
	leaq	.LC13(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$32, %edi
	call	_Znwm@PLT
	movq	%rax, %r12
	leaq	16(%rax), %rax
	movq	%rax, (%r12)
	movq	-384(%rbp), %rax
	movq	$0, 8(%r12)
	movb	$0, 16(%r12)
	testq	%rax, %rax
	je	.L217
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	ja	.L224
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L219:
	movq	.LC14(%rip), %xmm0
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-352(%rbp), %rdi
	movq	%rax, -320(%rbp)
	movhps	.LC16(%rip), %xmm0
	movaps	%xmm0, -432(%rbp)
	cmpq	-464(%rbp), %rdi
	je	.L220
	call	_ZdlPv@PLT
.L220:
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%r14, %rdi
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	%rbx, -432(%rbp)
	movq	-24(%rbx), %rax
	movq	%r13, %rdi
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	movq	%rbx, -432(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L225
	addq	$424, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L224:
	.cfi_restore_state
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L219
	.p2align 4,,10
	.p2align 3
.L223:
	movq	-432(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L216
	.p2align 4,,10
	.p2align 3
.L217:
	leaq	-352(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L219
.L225:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3890:
	.size	_ZN2v84base17MakeCheckOpStringIffEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc, .-_ZN2v84base17MakeCheckOpStringIffEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc
	.section	.text._ZN2v84base17MakeCheckOpStringIddEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc,"axG",@progbits,_ZN2v84base17MakeCheckOpStringIddEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc,comdat
	.p2align 4
	.weak	_ZN2v84base17MakeCheckOpStringIddEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc
	.type	_ZN2v84base17MakeCheckOpStringIddEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc, @function
_ZN2v84base17MakeCheckOpStringIddEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc:
.LFB3892:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-368(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-320(%rbp), %r13
	pushq	%r12
	movq	%r13, %rdi
	.cfi_offset 12, -48
	leaq	-432(%rbp), %r12
	pushq	%rbx
	subq	$424, %rsp
	.cfi_offset 3, -56
	movsd	%xmm1, -448(%rbp)
	movq	.LC14(%rip), %xmm1
	movsd	%xmm0, -440(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movhps	.LC15(%rip), %xmm1
	movaps	%xmm1, -464(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	movq	8+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	xorl	%esi, %esi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	pxor	%xmm0, %xmm0
	xorl	%eax, %eax
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rbx, -432(%rbp)
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	$0, -104(%rbp)
	movw	%ax, -96(%rbp)
	movq	-24(%rbx), %rax
	movq	%rcx, -432(%rbp,%rax)
	movq	-432(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movdqa	-464(%rbp), %xmm1
	pxor	%xmm0, %xmm0
	movq	%r14, %rdi
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movaps	%xmm0, -416(%rbp)
	movq	%rax, -320(%rbp)
	movaps	%xmm1, -432(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r13, %rdi
	leaq	-424(%rbp), %rsi
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movl	$16, -360(%rbp)
	movq	%rax, -464(%rbp)
	movq	%rax, -352(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	testq	%r15, %r15
	je	.L235
	movq	%r15, %rdi
	call	strlen@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L228:
	movl	$2, %edx
	leaq	.LC11(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movsd	-440(%rbp), %xmm0
	movq	%r12, %rdi
	call	_ZNSo9_M_insertIdEERSoT_@PLT
	movl	$5, %edx
	leaq	.LC12(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movsd	-448(%rbp), %xmm0
	movq	%r12, %rdi
	call	_ZNSo9_M_insertIdEERSoT_@PLT
	movq	%r12, %rdi
	movl	$1, %edx
	leaq	.LC13(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$32, %edi
	call	_Znwm@PLT
	movq	%rax, %r12
	leaq	16(%rax), %rax
	movq	%rax, (%r12)
	movq	-384(%rbp), %rax
	movq	$0, 8(%r12)
	movb	$0, 16(%r12)
	testq	%rax, %rax
	je	.L229
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	ja	.L236
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L231:
	movq	.LC14(%rip), %xmm0
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-352(%rbp), %rdi
	movq	%rax, -320(%rbp)
	movhps	.LC16(%rip), %xmm0
	movaps	%xmm0, -432(%rbp)
	cmpq	-464(%rbp), %rdi
	je	.L232
	call	_ZdlPv@PLT
.L232:
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%r14, %rdi
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	%rbx, -432(%rbp)
	movq	-24(%rbx), %rax
	movq	%r13, %rdi
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	movq	%rbx, -432(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L237
	addq	$424, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L236:
	.cfi_restore_state
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L231
	.p2align 4,,10
	.p2align 3
.L235:
	movq	-432(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L228
	.p2align 4,,10
	.p2align 3
.L229:
	leaq	-352(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L231
.L237:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3892:
	.size	_ZN2v84base17MakeCheckOpStringIddEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc, .-_ZN2v84base17MakeCheckOpStringIddEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET_T0_PKc
	.section	.data.rel.local._ZN2v84base12_GLOBAL__N_117g_dcheck_functionE,"aw"
	.align 8
	.type	_ZN2v84base12_GLOBAL__N_117g_dcheck_functionE, @object
	.size	_ZN2v84base12_GLOBAL__N_117g_dcheck_functionE, 8
_ZN2v84base12_GLOBAL__N_117g_dcheck_functionE:
	.quad	_ZN2v84base12_GLOBAL__N_120DefaultDcheckHandlerEPKciS3_
	.section	.bss._ZN2v84base12_GLOBAL__N_119g_print_stack_traceE,"aw",@nobits
	.align 8
	.type	_ZN2v84base12_GLOBAL__N_119g_print_stack_traceE, @object
	.size	_ZN2v84base12_GLOBAL__N_119g_print_stack_traceE, 8
_ZN2v84base12_GLOBAL__N_119g_print_stack_traceE:
	.zero	8
	.section	.data.rel.ro,"aw"
	.align 8
.LC14:
	.quad	_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE+24
	.align 8
.LC15:
	.quad	_ZTVSt15basic_streambufIcSt11char_traitsIcEE+16
	.align 8
.LC16:
	.quad	_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE+16
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
