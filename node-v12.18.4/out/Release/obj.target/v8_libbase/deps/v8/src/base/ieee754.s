	.file	"ieee754.cc"
	.text
	.section	.text._ZN2v84base7ieee75412_GLOBAL__N_117__kernel_rem_pio2EPdS3_iiiPKi.constprop.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v84base7ieee75412_GLOBAL__N_117__kernel_rem_pio2EPdS3_iiiPKi.constprop.0, @function
_ZN2v84base7ieee75412_GLOBAL__N_117__kernel_rem_pio2EPdS3_iiiPKi.constprop.0:
.LFB3513:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leal	-1(%rcx), %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%ecx, %ebx
	subq	$680, %rsp
	movq	%rsi, -696(%rbp)
	movl	%edx, %esi
	subl	$3, %edx
	movl	%ecx, -640(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movslq	%edx, %rax
	sarl	$31, %edx
	imulq	$715827883, %rax, %rax
	sarq	$34, %rax
	subl	%edx, %eax
	movl	%eax, %edx
	addl	$1, %eax
	leal	0(,%rax,4), %ecx
	movl	%edx, %edi
	subl	%ecx, %eax
	leal	(%rsi,%rax,8), %r8d
	leal	3(%rbx), %esi
	subl	%r13d, %edi
	jns	.L70
	movq	$0x000000000, -544(%rbp)
	cmpl	$-1, %edi
	je	.L71
	movq	$0x000000000, -536(%rbp)
	movl	$2, %eax
.L3:
	xorl	%edi, %edi
.L2:
	movl	-640(%rbp), %ebx
	movl	$1, %ecx
	leal	4(%rbx), %r10d
	subl	%eax, %r10d
	cmpl	%eax, %esi
	cmovl	%ecx, %r10d
	movl	%esi, %ecx
	subl	%eax, %ecx
	cmpl	$2, %ecx
	jbe	.L4
	leaq	-544(%rbp), %rbx
	movslq	%eax, %rcx
	movslq	%edi, %r9
	leaq	(%rbx,%rcx,8), %r11
	leaq	_ZZN2v84base7ieee75412_GLOBAL__N_118__ieee754_rem_pio2EdPdE11two_over_pi(%rip), %rcx
	movq	%rbx, -688(%rbp)
	movdqu	(%rcx,%r9,4), %xmm0
	leal	4(%rax), %r9d
	cvtdq2pd	%xmm0, %xmm1
	pshufd	$238, %xmm0, %xmm0
	movups	%xmm1, (%r11)
	cvtdq2pd	%xmm0, %xmm0
	movups	%xmm0, 16(%r11)
	leal	4(%rdi), %r11d
	cmpl	$4, %r10d
	je	.L8
	movslq	%r11d, %r11
	pxor	%xmm0, %xmm0
	movslq	%r9d, %r9
	addl	$5, %eax
	cvtsi2sdl	(%rcx,%r11,4), %xmm0
	addl	$5, %edi
	movsd	%xmm0, -544(%rbp,%r9,8)
	cmpl	%eax, %esi
	jl	.L8
.L68:
	movslq	%edi, %rdi
	pxor	%xmm0, %xmm0
	movslq	%eax, %r9
	addl	$1, %eax
	cvtsi2sdl	(%rcx,%rdi,4), %xmm0
	movsd	%xmm0, -544(%rbp,%r9,8)
	cmpl	%eax, %esi
	jl	.L8
	leaq	4+_ZZN2v84base7ieee75412_GLOBAL__N_118__ieee754_rem_pio2EdPdE11two_over_pi(%rip), %r9
	pxor	%xmm0, %xmm0
	cltq
	cvtsi2sdl	(%r9,%rdi,4), %xmm0
	movsd	%xmm0, -544(%rbp,%rax,8)
.L8:
	movslq	-640(%rbp), %rax
	movq	-688(%rbp), %rbx
	pxor	%xmm3, %xmm3
	movsd	(%r14), %xmm0
	leaq	-8(%rbx,%rax,8), %rax
	movupd	(%rax), %xmm1
	movapd	%xmm0, %xmm4
	unpcklpd	%xmm4, %xmm4
	mulpd	%xmm4, %xmm1
	movapd	%xmm1, %xmm2
	addpd	%xmm3, %xmm2
	testl	%r13d, %r13d
	je	.L153
	movsd	8(%r14), %xmm3
	movupd	-8(%rax), %xmm7
	movapd	%xmm3, %xmm1
	unpcklpd	%xmm1, %xmm1
	mulpd	%xmm7, %xmm1
	addpd	%xmm2, %xmm1
	cmpl	$1, %r13d
	je	.L9
	movsd	16(%r14), %xmm2
	movupd	-16(%rax), %xmm6
	unpcklpd	%xmm2, %xmm2
	mulpd	%xmm6, %xmm2
	addpd	%xmm2, %xmm1
.L9:
	movaps	%xmm1, -224(%rbp)
	movupd	16(%rax), %xmm1
	pxor	%xmm2, %xmm2
	movupd	8(%rax), %xmm7
	mulpd	%xmm4, %xmm1
	addpd	%xmm1, %xmm2
	movapd	%xmm3, %xmm1
	unpcklpd	%xmm1, %xmm1
	mulpd	%xmm7, %xmm1
	addpd	%xmm2, %xmm1
	cmpl	$1, %r13d
	je	.L10
	movsd	16(%r14), %xmm3
	movupd	(%rax), %xmm2
	unpcklpd	%xmm3, %xmm3
	mulpd	%xmm3, %xmm2
	addpd	%xmm2, %xmm1
.L10:
	movslq	%esi, %rax
	movaps	%xmm1, -208(%rbp)
	pxor	%xmm3, %xmm3
	mulsd	-544(%rbp,%rax,8), %xmm0
	movl	-640(%rbp), %eax
	addl	$2, %eax
	cltq
	movapd	%xmm0, %xmm1
	movsd	-544(%rbp,%rax,8), %xmm0
	mulsd	8(%r14), %xmm0
	addsd	%xmm3, %xmm1
	addsd	%xmm1, %xmm0
	cmpl	$1, %r13d
	je	.L12
	movl	-640(%rbp), %eax
	addl	$1, %eax
	cltq
	movsd	-544(%rbp,%rax,8), %xmm1
	mulsd	16(%r14), %xmm1
	addsd	%xmm1, %xmm0
.L12:
	movslq	%edx, %rax
	movl	$24, %edx
	leaq	-624(%rbp), %rsi
	movsd	.LC1(%rip), %xmm4
	subl	%r8d, %edx
	movq	%rsi, -632(%rbp)
	leaq	(%rcx,%rax,4), %rax
	movsd	.LC2(%rip), %xmm5
	movl	%edx, -676(%rbp)
	movl	$4, %r12d
	leaq	-224(%rbp), %r15
	movsd	%xmm0, -192(%rbp)
.L11:
	movslq	%r12d, %r9
	movq	-632(%rbp), %rsi
	movl	%r12d, %ebx
	movsd	-224(%rbp,%r9,8), %xmm0
	leaq	(%r15,%r9,8), %rcx
	.p2align 4,,10
	.p2align 3
.L13:
	movapd	%xmm0, %xmm1
	addq	$4, %rsi
	subq	$8, %rcx
	mulsd	%xmm4, %xmm1
	cvttsd2sil	%xmm1, %edx
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%edx, %xmm1
	movapd	%xmm1, %xmm2
	mulsd	%xmm5, %xmm2
	subsd	%xmm2, %xmm0
	cvttsd2sil	%xmm0, %edx
	movsd	(%rcx), %xmm0
	addsd	%xmm1, %xmm0
	movl	%edx, -4(%rsi)
	subl	$1, %ebx
	jne	.L13
	movl	%r8d, %edi
	movq	%rax, -664(%rbp)
	movq	%r9, -656(%rbp)
	movl	%r8d, -648(%rbp)
	movsd	%xmm3, -672(%rbp)
	call	scalbn@PLT
	movsd	.LC3(%rip), %xmm1
	movsd	.LC5(%rip), %xmm6
	movsd	.LC4(%rip), %xmm5
	movq	.LC1(%rip), %rdi
	mulsd	%xmm0, %xmm1
	movl	-648(%rbp), %r8d
	movq	-656(%rbp), %r9
	movq	%rdi, %xmm4
	movq	.LC2(%rip), %rdi
	movq	-664(%rbp), %rax
	movsd	-672(%rbp), %xmm3
	movapd	%xmm1, %xmm2
	movapd	%xmm1, %xmm7
	andpd	%xmm6, %xmm2
	ucomisd	%xmm2, %xmm5
	movq	%rdi, %xmm5
	jbe	.L14
	cvttsd2siq	%xmm1, %rdx
	pxor	%xmm2, %xmm2
	andnpd	%xmm7, %xmm6
	movsd	.LC6(%rip), %xmm9
	cvtsi2sdq	%rdx, %xmm2
	movapd	%xmm2, %xmm8
	cmpnlesd	%xmm1, %xmm8
	movapd	%xmm8, %xmm1
	andpd	%xmm9, %xmm1
	subsd	%xmm1, %xmm2
	movapd	%xmm2, %xmm1
	orpd	%xmm6, %xmm1
.L14:
	mulsd	.LC7(%rip), %xmm1
	subsd	%xmm1, %xmm0
	pxor	%xmm1, %xmm1
	cvttsd2sil	%xmm0, %r11d
	cvtsi2sdl	%r11d, %xmm1
	subsd	%xmm1, %xmm0
	movapd	%xmm0, %xmm1
	testl	%r8d, %r8d
	jle	.L15
	leal	-1(%r12), %esi
	movl	-676(%rbp), %r10d
	movslq	%esi, %rsi
	movl	-624(%rbp,%rsi,4), %edx
	movl	%r10d, %ecx
	movl	%edx, %edi
	sarl	%cl, %edi
	addl	%edi, %r11d
	sall	%cl, %edi
	movl	$23, %ecx
	subl	%edi, %edx
	subl	%r8d, %ecx
	movl	%edx, -624(%rbp,%rsi,4)
	sarl	%cl, %edx
	movl	%edx, %r10d
	testl	%r10d, %r10d
	jg	.L18
.L19:
	ucomisd	%xmm3, %xmm1
	jp	.L33
.L158:
	jne	.L33
	leal	-1(%r12), %ecx
	cmpl	$4, %r12d
	je	.L35
	leal	-5(%r12), %edx
	leal	-4(%r12), %esi
	cmpl	$3, %edx
	jbe	.L73
	movq	-632(%rbp), %rbx
	movl	%esi, %edx
	shrl	$2, %edx
	leaq	-16(%rbx,%r9,4), %rdi
	movdqu	(%rdi), %xmm7
	pshufd	$27, %xmm7, %xmm0
	cmpl	$1, %edx
	je	.L37
	movdqu	-16(%rdi), %xmm7
	pshufd	$27, %xmm7, %xmm1
	por	%xmm1, %xmm0
	cmpl	$2, %edx
	je	.L37
	movdqu	-32(%rdi), %xmm7
	pshufd	$27, %xmm7, %xmm1
	por	%xmm1, %xmm0
	cmpl	$3, %edx
	je	.L37
	movdqu	-48(%rdi), %xmm6
	pshufd	$27, %xmm6, %xmm1
	por	%xmm1, %xmm0
	cmpl	$4, %edx
	je	.L37
	movdqu	-64(%rdi), %xmm1
	pshufd	$27, %xmm1, %xmm1
	por	%xmm1, %xmm0
.L37:
	movdqa	%xmm0, %xmm1
	movl	%esi, %edi
	movl	%ecx, %edx
	psrldq	$8, %xmm1
	andl	$-4, %edi
	por	%xmm1, %xmm0
	subl	%edi, %edx
	movdqa	%xmm0, %xmm1
	psrldq	$4, %xmm1
	por	%xmm1, %xmm0
	movd	%xmm0, %ebx
	cmpl	%esi, %edi
	je	.L38
.L36:
	movslq	%edx, %rsi
	orl	-624(%rbp,%rsi,4), %ebx
	leal	-1(%rdx), %esi
	cmpl	$4, %edx
	je	.L38
	movslq	%esi, %rsi
	orl	-624(%rbp,%rsi,4), %ebx
	leal	-2(%rdx), %esi
	cmpl	$5, %edx
	je	.L38
	movslq	%esi, %rsi
	orl	-624(%rbp,%rsi,4), %ebx
	leal	-3(%rdx), %esi
	cmpl	$6, %edx
	je	.L38
	movslq	%esi, %rdx
	orl	-624(%rbp,%rdx,4), %ebx
.L38:
	testl	%ebx, %ebx
	jne	.L41
	movl	-612(%rbp), %ebx
	leal	1(%r12), %ecx
	testl	%ebx, %ebx
	jne	.L75
	movl	-616(%rbp), %r11d
	testl	%r11d, %r11d
	jne	.L154
.L42:
	movl	-620(%rbp), %r10d
	testl	%r10d, %r10d
	jne	.L155
	movl	-624(%rbp), %r9d
	testl	%r9d, %r9d
	jne	.L156
	addl	$5, %r12d
.L39:
	movslq	%ecx, %rdx
	addl	%r13d, %ecx
	movq	-688(%rbp), %rbx
	movsd	(%r14), %xmm2
	movslq	%ecx, %rcx
	subq	%rdx, %rcx
	leaq	(%rbx,%rcx,8), %rsi
	movl	-640(%rbp), %ebx
	leal	-2(%rbx), %edi
	leal	-3(%rbx), %r9d
	.p2align 4,,10
	.p2align 3
.L46:
	pxor	%xmm0, %xmm0
	cvtsi2sdl	(%rax,%rdx,4), %xmm0
	movsd	%xmm0, (%rsi,%rdx,8)
	mulsd	%xmm2, %xmm0
	addsd	%xmm3, %xmm0
	testl	%r13d, %r13d
	je	.L45
	leal	(%rdi,%rdx), %ecx
	movslq	%ecx, %rcx
	movsd	-544(%rbp,%rcx,8), %xmm1
	mulsd	8(%r14), %xmm1
	addsd	%xmm1, %xmm0
	cmpl	$1, %r13d
	je	.L45
	leal	(%r9,%rdx), %ecx
	movslq	%ecx, %rcx
	movsd	-544(%rbp,%rcx,8), %xmm1
	mulsd	16(%r14), %xmm1
	addsd	%xmm1, %xmm0
.L45:
	movsd	%xmm0, (%r15,%rdx,8)
	addq	$1, %rdx
	cmpl	%edx, %r12d
	jge	.L46
	jmp	.L11
.L15:
	jne	.L17
	leal	-1(%r12), %edx
	movslq	%edx, %rdx
	movl	-624(%rbp,%rdx,4), %edx
	sarl	$23, %edx
	movl	%edx, %r10d
	testl	%r10d, %r10d
	jle	.L19
.L18:
	movl	-624(%rbp), %edx
	addl	$1, %r11d
	testl	%edx, %edx
	jne	.L20
.L159:
	movl	-620(%rbp), %esi
	movl	$1, %edx
	leaq	-620(%rbp), %rdi
	leal	1(%rdx), %ecx
	testl	%esi, %esi
	jne	.L157
.L23:
	addq	$4, %rdi
	cmpl	%ecx, %r12d
	je	.L22
	movl	(%rdi), %esi
	movl	%ecx, %edx
	leal	1(%rdx), %ecx
	testl	%esi, %esi
	je	.L23
.L157:
	movl	$16777216, %edi
	subl	%esi, %edi
	movslq	%edx, %rsi
	movl	%edi, -624(%rbp,%rsi,4)
	cmpl	%ecx, %r12d
	jle	.L25
	movslq	%ecx, %rdi
	movl	$16777215, %esi
	addl	$2, %edx
	subl	-624(%rbp,%rdi,4), %esi
	movl	%esi, -624(%rbp,%rdi,4)
	cmpl	%edx, %r12d
	jle	.L25
.L63:
	movl	%r12d, %esi
	subl	%ecx, %esi
	leal	-1(%rsi), %edi
	subl	$2, %esi
	cmpl	$2, %esi
	jbe	.L26
	movl	%edi, %esi
	movslq	%ecx, %rcx
	movdqa	.LC9(%rip), %xmm0
	shrl	$2, %esi
	movl	%esi, -648(%rbp)
	movq	-632(%rbp), %rsi
	leaq	4(%rsi,%rcx,4), %rcx
	movl	-648(%rbp), %esi
	movdqu	(%rcx), %xmm6
	psubd	%xmm6, %xmm0
	movups	%xmm0, (%rcx)
	cmpl	$1, %esi
	je	.L27
	movdqu	16(%rcx), %xmm6
	movdqa	.LC9(%rip), %xmm0
	psubd	%xmm6, %xmm0
	movups	%xmm0, 16(%rcx)
	cmpl	$2, %esi
	je	.L27
	movdqu	32(%rcx), %xmm7
	movdqa	.LC9(%rip), %xmm0
	psubd	%xmm7, %xmm0
	movups	%xmm0, 32(%rcx)
	cmpl	$3, %esi
	je	.L27
	movdqu	48(%rcx), %xmm6
	movdqa	.LC9(%rip), %xmm0
	psubd	%xmm6, %xmm0
	movups	%xmm0, 48(%rcx)
.L27:
	movl	%edi, %ecx
	andl	$-4, %ecx
	addl	%ecx, %edx
	cmpl	%edi, %ecx
	je	.L25
.L26:
	movslq	%edx, %rcx
	movl	$16777215, %esi
	movl	%esi, %edi
	subl	-624(%rbp,%rcx,4), %edi
	movl	%edi, -624(%rbp,%rcx,4)
	leal	1(%rdx), %ecx
	cmpl	%ecx, %r12d
	jle	.L25
	movslq	%ecx, %rcx
	movl	%esi, %edi
	addl	$2, %edx
	subl	-624(%rbp,%rcx,4), %edi
	movl	%edi, -624(%rbp,%rcx,4)
	cmpl	%edx, %r12d
	jle	.L25
	movslq	%edx, %rdx
	subl	-624(%rbp,%rdx,4), %esi
	movl	%esi, -624(%rbp,%rdx,4)
.L25:
	movl	$1, %esi
.L22:
	testl	%r8d, %r8d
	jle	.L30
	cmpl	$1, %r8d
	je	.L31
	cmpl	$2, %r8d
	jne	.L30
	leal	-1(%r12), %edx
	movslq	%edx, %rdx
	andl	$4194303, -624(%rbp,%rdx,4)
.L30:
	cmpl	$2, %r10d
	jne	.L19
	movsd	.LC6(%rip), %xmm7
	subsd	%xmm1, %xmm7
	movapd	%xmm7, %xmm1
	testl	%esi, %esi
	je	.L19
	movq	%rax, -672(%rbp)
	movq	.LC6(%rip), %rax
	movl	%r8d, %edi
	movq	%r9, -712(%rbp)
	movq	%rax, %xmm0
	movl	%r11d, -700(%rbp)
	movl	%r10d, -680(%rbp)
	movl	%r8d, -648(%rbp)
	movsd	%xmm3, -664(%rbp)
	movsd	%xmm7, -656(%rbp)
	call	scalbn@PLT
	movsd	-656(%rbp), %xmm1
	movsd	-664(%rbp), %xmm3
	movq	.LC2(%rip), %rdi
	movq	-712(%rbp), %r9
	subsd	%xmm0, %xmm1
	movl	-700(%rbp), %r11d
	movl	-680(%rbp), %r10d
	movq	%rdi, %xmm5
	movq	.LC1(%rip), %rdi
	movq	-672(%rbp), %rax
	movl	-648(%rbp), %r8d
	ucomisd	%xmm3, %xmm1
	movq	%rdi, %xmm4
	jnp	.L158
.L33:
	movq	-632(%rbp), %rsi
	movl	%r8d, %edi
	movapd	%xmm1, %xmm0
	movl	%r11d, -656(%rbp)
	negl	%edi
	movq	%r9, -640(%rbp)
	movl	%r10d, %r14d
	movq	%rsi, -648(%rbp)
	movl	%r8d, -632(%rbp)
	movsd	%xmm3, -664(%rbp)
	call	scalbn@PLT
	movq	.LC2(%rip), %rax
	movl	-632(%rbp), %r8d
	movq	-640(%rbp), %r9
	movq	-648(%rbp), %rsi
	movq	%rax, %xmm5
	movq	.LC1(%rip), %rax
	movl	-656(%rbp), %r11d
	comisd	%xmm5, %xmm0
	movsd	-664(%rbp), %xmm3
	movq	%rax, %xmm4
	jb	.L145
	movapd	%xmm0, %xmm1
	leal	1(%r12), %ecx
	addl	$24, %r8d
	mulsd	%xmm4, %xmm1
	movslq	%ecx, %rbx
	cvttsd2sil	%xmm1, %edx
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%edx, %xmm1
	mulsd	%xmm1, %xmm5
	subsd	%xmm5, %xmm0
	cvttsd2sil	%xmm0, %eax
	movl	%eax, -624(%rbp,%r9,4)
	movl	%edx, -624(%rbp,%rbx,4)
.L47:
	movq	.LC6(%rip), %rax
	movl	%r8d, %edi
	movq	%rsi, -648(%rbp)
	movl	%r11d, -640(%rbp)
	movq	%rax, %xmm0
	movl	%ecx, -632(%rbp)
	movsd	%xmm3, -656(%rbp)
	call	scalbn@PLT
	movq	.LC1(%rip), %rax
	movl	-632(%rbp), %ecx
	movl	-640(%rbp), %r11d
	movq	-648(%rbp), %rsi
	movapd	%xmm0, %xmm1
	movsd	-656(%rbp), %xmm3
	movq	%rax, %xmm4
	jmp	.L62
.L17:
	xorl	%r10d, %r10d
	comisd	.LC8(%rip), %xmm0
	jb	.L19
	movl	-624(%rbp), %edx
	movl	$2, %r10d
	addl	$1, %r11d
	testl	%edx, %edx
	je	.L159
.L20:
	movl	$16777216, %ecx
	subl	%edx, %ecx
	movl	$16777215, %edx
	subl	-620(%rbp), %edx
	movl	%ecx, -624(%rbp)
	movl	$1, %ecx
	movl	%edx, -620(%rbp)
	movl	$2, %edx
	jmp	.L63
.L70:
	xorl	%eax, %eax
	jmp	.L2
.L35:
	movl	-612(%rbp), %edx
	testl	%edx, %edx
	jne	.L74
	movl	-616(%rbp), %r11d
	movl	$5, %ecx
	testl	%r11d, %r11d
	je	.L42
.L154:
	addl	$2, %r12d
	jmp	.L39
.L73:
	movl	%ecx, %edx
	jmp	.L36
.L41:
	movslq	%ecx, %rbx
	movq	-632(%rbp), %rsi
	movl	%r10d, %r14d
	subl	$24, %r8d
	movl	-624(%rbp,%rbx,4), %edi
	testl	%edi, %edi
	jne	.L47
	leaq	(%rsi,%rbx,4), %rax
.L49:
	movl	-4(%rax), %edx
	subq	$4, %rax
	subl	$1, %ecx
	subl	$24, %r8d
	testl	%edx, %edx
	je	.L49
	movq	.LC6(%rip), %rax
	movl	%r8d, %edi
	movl	%r11d, -640(%rbp)
	movl	%ecx, -632(%rbp)
	movq	%rax, %xmm0
	movq	%rsi, -648(%rbp)
	movsd	%xmm3, -656(%rbp)
	call	scalbn@PLT
	movl	-632(%rbp), %ecx
	movl	-640(%rbp), %r11d
	movapd	%xmm0, %xmm1
	testl	%ecx, %ecx
	js	.L52
	movq	.LC1(%rip), %rax
	movsd	-656(%rbp), %xmm3
	movslq	%ecx, %rbx
	movq	-648(%rbp), %rsi
	movq	%rax, %xmm4
.L62:
	movq	%rbx, %rdi
	movq	%rbx, %rax
	.p2align 4,,10
	.p2align 3
.L53:
	pxor	%xmm0, %xmm0
	cvtsi2sdl	(%rsi,%rax,4), %xmm0
	mulsd	%xmm1, %xmm0
	mulsd	%xmm4, %xmm1
	movsd	%xmm0, (%r15,%rax,8)
	subq	$1, %rax
	testl	%eax, %eax
	jns	.L53
	movsd	.LC10(%rip), %xmm2
	movsd	.LC11(%rip), %xmm5
	movq	%rbx, %rax
	xorl	%edx, %edx
	movsd	.LC12(%rip), %xmm4
	movsd	.LC13(%rip), %xmm7
	leaq	-384(%rbp), %r9
	movsd	.LC14(%rip), %xmm6
	.p2align 4,,10
	.p2align 3
.L55:
	movsd	(%r15,%rax,8), %xmm0
	mulsd	%xmm2, %xmm0
	addsd	%xmm3, %xmm0
	testq	%rdx, %rdx
	je	.L54
	leal	1(%rax), %r8d
	movl	%eax, %esi
	movslq	%r8d, %r8
	movsd	-224(%rbp,%r8,8), %xmm1
	mulsd	%xmm5, %xmm1
	addsd	%xmm1, %xmm0
	cmpl	$1, %edx
	je	.L54
	leal	2(%rax), %r8d
	movslq	%r8d, %r8
	movsd	-224(%rbp,%r8,8), %xmm1
	mulsd	%xmm4, %xmm1
	addsd	%xmm1, %xmm0
	cmpl	$2, %edx
	je	.L54
	leal	3(%rax), %r8d
	movslq	%r8d, %r8
	movsd	-224(%rbp,%r8,8), %xmm1
	mulsd	%xmm7, %xmm1
	addsd	%xmm1, %xmm0
	cmpl	$3, %edx
	je	.L54
	addl	$4, %esi
	movslq	%esi, %rsi
	movsd	-224(%rbp,%rsi,8), %xmm1
	mulsd	%xmm6, %xmm1
	addsd	%xmm1, %xmm0
.L54:
	subq	$1, %rax
	movsd	%xmm0, (%r9,%rdx,8)
	addq	$1, %rdx
	testl	%eax, %eax
	jns	.L55
.L56:
	addsd	(%r9,%rdi,8), %xmm3
	subq	$1, %rdi
	testl	%edi, %edi
	jns	.L56
	movsd	-384(%rbp), %xmm0
	subsd	%xmm3, %xmm0
	testl	%r14d, %r14d
	je	.L57
	movq	.LC15(%rip), %xmm1
	movq	-696(%rbp), %rax
	xorpd	%xmm1, %xmm3
	movsd	%xmm3, (%rax)
	testl	%ecx, %ecx
	je	.L58
.L61:
	movl	$1, %eax
.L59:
	addsd	(%r9,%rax,8), %xmm0
	addq	$1, %rax
	cmpl	%eax, %ecx
	jge	.L59
	testl	%r14d, %r14d
	je	.L60
	movq	.LC15(%rip), %xmm1
.L58:
	xorpd	%xmm1, %xmm0
.L60:
	movq	-696(%rbp), %rax
	movsd	%xmm0, 8(%rax)
	movl	%r11d, %eax
	andl	$7, %eax
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L160
	addq	$680, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L31:
	.cfi_restore_state
	leal	-1(%r12), %edx
	movslq	%edx, %rdx
	andl	$8388607, -624(%rbp,%rdx,4)
	jmp	.L30
.L155:
	addl	$3, %r12d
	jmp	.L39
.L75:
	movl	%ecx, %r12d
	jmp	.L39
.L156:
	addl	$4, %r12d
	jmp	.L39
.L145:
	cvttsd2sil	%xmm0, %eax
	movslq	%r12d, %rbx
	movq	%rbx, %rcx
	movl	%eax, -624(%rbp,%r9,4)
	jmp	.L47
.L52:
	movsd	-384(%rbp), %xmm0
	testl	%r14d, %r14d
	jne	.L161
	movq	-696(%rbp), %rax
	movq	$0x000000000, (%rax)
	jmp	.L60
.L57:
	movq	-696(%rbp), %rax
	movsd	%xmm3, (%rax)
	testl	%ecx, %ecx
	jg	.L61
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L71:
	movl	$1, %eax
	jmp	.L3
.L4:
	movslq	%edi, %r10
	leaq	_ZZN2v84base7ieee75412_GLOBAL__N_118__ieee754_rem_pio2EdPdE11two_over_pi(%rip), %rcx
	movslq	%eax, %r9
	addl	$1, %edi
	pxor	%xmm0, %xmm0
	leaq	-544(%rbp), %rbx
	addl	$1, %eax
	cvtsi2sdl	(%rcx,%r10,4), %xmm0
	movq	%rbx, -688(%rbp)
	movsd	%xmm0, -544(%rbp,%r9,8)
	jmp	.L68
.L153:
	movupd	16(%rax), %xmm1
	mulsd	-512(%rbp), %xmm0
	movaps	%xmm2, -224(%rbp)
	mulpd	%xmm4, %xmm1
	addpd	%xmm3, %xmm1
	pxor	%xmm3, %xmm3
	addsd	%xmm3, %xmm0
	movaps	%xmm1, -208(%rbp)
	jmp	.L12
.L74:
	movl	$5, %r12d
	movl	$5, %ecx
	jmp	.L39
.L160:
	call	__stack_chk_fail@PLT
	.p2align 4,,10
	.p2align 3
.L161:
	movq	-696(%rbp), %rax
	movq	.LC16(%rip), %rbx
	movq	.LC15(%rip), %xmm1
	movq	%rbx, (%rax)
	jmp	.L58
	.cfi_endproc
.LFE3513:
	.size	_ZN2v84base7ieee75412_GLOBAL__N_117__kernel_rem_pio2EPdS3_iiiPKi.constprop.0, .-_ZN2v84base7ieee75412_GLOBAL__N_117__kernel_rem_pio2EPdS3_iiiPKi.constprop.0
	.section	.text._ZN2v84base7ieee75412_GLOBAL__N_118__ieee754_rem_pio2EdPd,"ax",@progbits
	.p2align 4
	.type	_ZN2v84base7ieee75412_GLOBAL__N_118__ieee754_rem_pio2EdPd, @function
_ZN2v84base7ieee75412_GLOBAL__N_118__ieee754_rem_pio2EdPd:
.LFB2996:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%xmm0, %r12
	pushq	%rbx
	shrq	$32, %r12
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movl	%r12d, %ecx
	andl	$2147483647, %ecx
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpl	$1072243195, %ecx
	jle	.L183
	cmpl	$1073928571, %ecx
	jg	.L165
	testl	%r12d, %r12d
	jle	.L166
	subsd	.LC17(%rip), %xmm0
	movsd	.LC18(%rip), %xmm1
	cmpl	$1073291771, %ecx
	jne	.L181
	subsd	.LC19(%rip), %xmm0
	movsd	.LC20(%rip), %xmm1
.L181:
	movapd	%xmm0, %xmm2
	movl	$1, %eax
	subsd	%xmm1, %xmm2
	subsd	%xmm2, %xmm0
	movsd	%xmm2, (%rbx)
	subsd	%xmm1, %xmm0
	movsd	%xmm0, 8(%rbx)
.L162:
	movq	-24(%rbp), %rsi
	xorq	%fs:40, %rsi
	jne	.L184
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L165:
	.cfi_restore_state
	cmpl	$1094263291, %ecx
	jg	.L169
	andpd	.LC5(%rip), %xmm0
	movsd	.LC21(%rip), %xmm1
	pxor	%xmm3, %xmm3
	movsd	.LC18(%rip), %xmm2
	mulsd	%xmm0, %xmm1
	addsd	.LC8(%rip), %xmm1
	cvttsd2sil	%xmm1, %eax
	movsd	.LC17(%rip), %xmm1
	cvtsi2sdl	%eax, %xmm3
	mulsd	%xmm3, %xmm1
	mulsd	%xmm3, %xmm2
	subsd	%xmm1, %xmm0
	movapd	%xmm0, %xmm1
	subsd	%xmm2, %xmm1
	cmpl	$31, %eax
	jg	.L170
	leal	-1(%rax), %edx
	leaq	_ZZN2v84base7ieee75412_GLOBAL__N_118__ieee754_rem_pio2EdPdE8npio2_hw(%rip), %rsi
	movslq	%edx, %rdx
	cmpl	%ecx, (%rsi,%rdx,4)
	je	.L170
.L180:
	movsd	%xmm1, (%rbx)
.L171:
	subsd	%xmm1, %xmm0
	subsd	%xmm2, %xmm0
	movsd	%xmm0, 8(%rbx)
	testl	%r12d, %r12d
	jns	.L162
	unpcklpd	%xmm0, %xmm1
	xorpd	.LC24(%rip), %xmm1
	negl	%eax
	movaps	%xmm1, (%rbx)
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L183:
	movq	$0x000000000, 8(%rdi)
	xorl	%eax, %eax
	movsd	%xmm0, (%rdi)
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L169:
	cmpl	$2146435071, %ecx
	jg	.L185
	movl	%ecx, %edx
	movq	%xmm0, %rax
	pxor	%xmm0, %xmm0
	sarl	$20, %edx
	movl	%eax, %eax
	pxor	%xmm2, %xmm2
	subl	$1046, %edx
	movl	%edx, %esi
	sall	$20, %esi
	subl	%esi, %ecx
	salq	$32, %rcx
	orq	%rcx, %rax
	movq	%rax, %xmm1
	cvttsd2sil	%xmm1, %eax
	cvtsi2sdl	%eax, %xmm0
	subsd	%xmm0, %xmm1
	movsd	%xmm0, -48(%rbp)
	movapd	%xmm1, %xmm0
	movsd	.LC2(%rip), %xmm1
	mulsd	%xmm1, %xmm0
	cvttsd2sil	%xmm0, %eax
	cvtsi2sdl	%eax, %xmm2
	subsd	%xmm2, %xmm0
	movsd	%xmm2, -40(%rbp)
	mulsd	%xmm1, %xmm0
	ucomisd	.LC0(%rip), %xmm0
	movsd	%xmm0, -32(%rbp)
	jp	.L177
	jne	.L177
	xorl	%ecx, %ecx
	testl	%eax, %eax
	setne	%cl
	addl	$1, %ecx
.L175:
	leaq	-48(%rbp), %rdi
	leaq	_ZZN2v84base7ieee75412_GLOBAL__N_118__ieee754_rem_pio2EdPdE11two_over_pi(%rip), %r9
	movl	$2, %r8d
	movq	%rbx, %rsi
	call	_ZN2v84base7ieee75412_GLOBAL__N_117__kernel_rem_pio2EPdS3_iiiPKi.constprop.0
	testl	%r12d, %r12d
	jns	.L162
	movapd	(%rbx), %xmm0
	xorpd	.LC24(%rip), %xmm0
	negl	%eax
	movaps	%xmm0, (%rbx)
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L170:
	movq	%xmm1, %rdx
	sarl	$20, %ecx
	shrq	$52, %rdx
	movl	%ecx, %esi
	andl	$2047, %edx
	subl	%edx, %esi
	cmpl	$16, %esi
	jle	.L180
	movsd	.LC19(%rip), %xmm1
	movapd	%xmm0, %xmm4
	movsd	.LC20(%rip), %xmm2
	mulsd	%xmm3, %xmm1
	mulsd	%xmm3, %xmm2
	subsd	%xmm1, %xmm4
	subsd	%xmm4, %xmm0
	subsd	%xmm1, %xmm0
	movapd	%xmm4, %xmm1
	subsd	%xmm0, %xmm2
	subsd	%xmm2, %xmm1
	movq	%xmm1, %rdx
	shrq	$52, %rdx
	andl	$2047, %edx
	subl	%edx, %ecx
	cmpl	$49, %ecx
	jle	.L186
	movsd	.LC22(%rip), %xmm1
	movapd	%xmm4, %xmm0
	mulsd	%xmm3, %xmm1
	mulsd	.LC23(%rip), %xmm3
	subsd	%xmm1, %xmm0
	movapd	%xmm3, %xmm2
	subsd	%xmm0, %xmm4
	subsd	%xmm1, %xmm4
	movapd	%xmm0, %xmm1
	subsd	%xmm4, %xmm2
	subsd	%xmm2, %xmm1
	jmp	.L180
	.p2align 4,,10
	.p2align 3
.L185:
	unpcklpd	%xmm0, %xmm0
	xorl	%eax, %eax
	subpd	%xmm0, %xmm0
	movaps	%xmm0, (%rdi)
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L186:
	movsd	%xmm1, (%rbx)
	movapd	%xmm4, %xmm0
	jmp	.L171
	.p2align 4,,10
	.p2align 3
.L177:
	movl	$3, %ecx
	jmp	.L175
.L166:
	addsd	.LC17(%rip), %xmm0
	movsd	.LC18(%rip), %xmm1
	cmpl	$1073291771, %ecx
	jne	.L182
	addsd	.LC19(%rip), %xmm0
	movsd	.LC20(%rip), %xmm1
.L182:
	movapd	%xmm0, %xmm2
	movl	$-1, %eax
	addsd	%xmm1, %xmm2
	subsd	%xmm2, %xmm0
	movsd	%xmm2, (%rbx)
	addsd	%xmm1, %xmm0
	movsd	%xmm0, 8(%rbx)
	jmp	.L162
.L184:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2996:
	.size	_ZN2v84base7ieee75412_GLOBAL__N_118__ieee754_rem_pio2EdPd, .-_ZN2v84base7ieee75412_GLOBAL__N_118__ieee754_rem_pio2EdPd
	.section	.text._ZN2v84base7ieee7544acosEd,"ax",@progbits
	.p2align 4
	.globl	_ZN2v84base7ieee7544acosEd
	.type	_ZN2v84base7ieee7544acosEd, @function
_ZN2v84base7ieee7544acosEd:
.LFB3001:
	.cfi_startproc
	endbr64
	movq	%xmm0, %rdx
	shrq	$32, %rdx
	movl	%edx, %eax
	andl	$2147483647, %eax
	cmpl	$1072693247, %eax
	jle	.L188
	movq	%xmm0, %rcx
	subl	$1072693248, %eax
	movsd	.LC26(%rip), %xmm0
	orl	%ecx, %eax
	jne	.L198
	pxor	%xmm0, %xmm0
	testl	%edx, %edx
	jle	.L200
.L198:
	ret
	.p2align 4,,10
	.p2align 3
.L188:
	movapd	%xmm0, %xmm1
	cmpl	$1071644671, %eax
	jg	.L190
	movsd	.LC27(%rip), %xmm0
	cmpl	$1012924416, %eax
	jle	.L198
	movapd	%xmm1, %xmm4
	movsd	.LC28(%rip), %xmm2
	movsd	.LC34(%rip), %xmm3
	mulsd	%xmm1, %xmm4
	mulsd	%xmm4, %xmm2
	addsd	.LC29(%rip), %xmm2
	mulsd	%xmm4, %xmm3
	subsd	.LC35(%rip), %xmm3
	mulsd	%xmm4, %xmm2
	subsd	.LC30(%rip), %xmm2
	mulsd	%xmm4, %xmm3
	addsd	.LC36(%rip), %xmm3
	mulsd	%xmm4, %xmm2
	addsd	.LC31(%rip), %xmm2
	mulsd	%xmm4, %xmm3
	subsd	.LC37(%rip), %xmm3
	mulsd	%xmm4, %xmm2
	subsd	.LC32(%rip), %xmm2
	mulsd	%xmm4, %xmm3
	addsd	.LC6(%rip), %xmm3
	mulsd	%xmm4, %xmm2
	addsd	.LC33(%rip), %xmm2
	mulsd	%xmm4, %xmm2
	divsd	%xmm3, %xmm2
	movsd	.LC38(%rip), %xmm3
	mulsd	%xmm1, %xmm2
	subsd	%xmm2, %xmm3
	subsd	%xmm3, %xmm1
	subsd	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L190:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movsd	.LC6(%rip), %xmm4
	testl	%edx, %edx
	js	.L201
	movapd	%xmm4, %xmm6
	movsd	.LC8(%rip), %xmm1
	subsd	%xmm0, %xmm6
	pxor	%xmm0, %xmm0
	mulsd	%xmm6, %xmm1
	ucomisd	%xmm1, %xmm0
	movapd	%xmm1, %xmm3
	sqrtsd	%xmm3, %xmm3
	ja	.L202
.L193:
	movsd	.LC28(%rip), %xmm0
	movq	%xmm3, %rdx
	movsd	.LC34(%rip), %xmm5
	movabsq	$-4294967296, %rax
	andq	%rax, %rdx
	mulsd	%xmm1, %xmm0
	movq	%rdx, %xmm2
	addsd	.LC29(%rip), %xmm0
	mulsd	%xmm1, %xmm5
	subsd	.LC35(%rip), %xmm5
	mulsd	%xmm1, %xmm0
	subsd	.LC30(%rip), %xmm0
	mulsd	%xmm1, %xmm5
	addsd	.LC36(%rip), %xmm5
	mulsd	%xmm1, %xmm0
	addsd	.LC31(%rip), %xmm0
	mulsd	%xmm1, %xmm5
	subsd	.LC37(%rip), %xmm5
	mulsd	%xmm1, %xmm0
	subsd	.LC32(%rip), %xmm0
	mulsd	%xmm1, %xmm5
	mulsd	%xmm1, %xmm0
	addsd	.LC33(%rip), %xmm0
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	addsd	%xmm5, %xmm4
	mulsd	%xmm1, %xmm0
	divsd	%xmm4, %xmm0
	movq	%rdx, %xmm4
	mulsd	%xmm2, %xmm4
	subsd	%xmm4, %xmm1
	mulsd	%xmm3, %xmm0
	addsd	%xmm2, %xmm3
	divsd	%xmm3, %xmm1
	addsd	%xmm1, %xmm0
	addsd	%xmm2, %xmm0
	addsd	%xmm0, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L201:
	.cfi_restore_state
	addsd	%xmm4, %xmm1
	mulsd	.LC8(%rip), %xmm1
	movsd	.LC28(%rip), %xmm2
	pxor	%xmm0, %xmm0
	movsd	.LC34(%rip), %xmm3
	mulsd	%xmm1, %xmm2
	ucomisd	%xmm1, %xmm0
	addsd	.LC29(%rip), %xmm2
	mulsd	%xmm1, %xmm3
	subsd	.LC35(%rip), %xmm3
	mulsd	%xmm1, %xmm2
	subsd	.LC30(%rip), %xmm2
	mulsd	%xmm1, %xmm3
	addsd	.LC36(%rip), %xmm3
	mulsd	%xmm1, %xmm2
	addsd	.LC31(%rip), %xmm2
	mulsd	%xmm1, %xmm3
	subsd	.LC37(%rip), %xmm3
	mulsd	%xmm1, %xmm2
	subsd	.LC32(%rip), %xmm2
	mulsd	%xmm1, %xmm3
	mulsd	%xmm1, %xmm2
	addsd	.LC33(%rip), %xmm2
	addsd	%xmm4, %xmm3
	movapd	%xmm1, %xmm4
	sqrtsd	%xmm4, %xmm4
	mulsd	%xmm1, %xmm2
	ja	.L203
.L192:
	divsd	%xmm3, %xmm2
	movsd	.LC25(%rip), %xmm1
	movapd	%xmm2, %xmm0
	mulsd	%xmm4, %xmm0
	subsd	.LC38(%rip), %xmm0
	leave
	.cfi_def_cfa 7, 8
	addsd	%xmm4, %xmm0
	addsd	%xmm0, %xmm0
	subsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L200:
	.cfi_restore 6
	movsd	.LC25(%rip), %xmm0
	ret
.L202:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	movapd	%xmm1, %xmm0
	movsd	%xmm3, -16(%rbp)
	movsd	%xmm1, -8(%rbp)
	call	sqrt@PLT
	movsd	-16(%rbp), %xmm3
	movsd	-8(%rbp), %xmm1
	movq	.LC6(%rip), %rax
	movq	%rax, %xmm4
	jmp	.L193
.L203:
	movapd	%xmm1, %xmm0
	movsd	%xmm4, -24(%rbp)
	movsd	%xmm3, -16(%rbp)
	movsd	%xmm2, -8(%rbp)
	call	sqrt@PLT
	movsd	-24(%rbp), %xmm4
	movsd	-16(%rbp), %xmm3
	movsd	-8(%rbp), %xmm2
	jmp	.L192
	.cfi_endproc
.LFE3001:
	.size	_ZN2v84base7ieee7544acosEd, .-_ZN2v84base7ieee7544acosEd
	.section	.text._ZN2v84base7ieee7544asinEd,"ax",@progbits
	.p2align 4
	.globl	_ZN2v84base7ieee7544asinEd
	.type	_ZN2v84base7ieee7544asinEd, @function
_ZN2v84base7ieee7544asinEd:
.LFB3003:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movapd	%xmm0, %xmm1
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%xmm0, %r12
	pushq	%rbx
	shrq	$32, %r12
	.cfi_offset 3, -32
	movl	%r12d, %ebx
	andl	$2147483647, %ebx
	subq	$32, %rsp
	cmpl	$1072693247, %ebx
	jle	.L205
	movq	%xmm0, %rax
	subl	$1072693248, %ebx
	movsd	.LC26(%rip), %xmm0
	orl	%eax, %ebx
	je	.L216
.L204:
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L205:
	.cfi_restore_state
	cmpl	$1071644671, %ebx
	jg	.L207
	cmpl	$1044381695, %ebx
	jg	.L208
	movsd	.LC39(%rip), %xmm2
	addsd	%xmm0, %xmm2
	comisd	.LC6(%rip), %xmm2
	ja	.L204
	pxor	%xmm0, %xmm0
.L209:
	mulsd	%xmm1, %xmm0
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	addsd	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L216:
	.cfi_restore_state
	movsd	.LC27(%rip), %xmm0
	mulsd	%xmm1, %xmm0
	mulsd	.LC38(%rip), %xmm1
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	addsd	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L207:
	.cfi_restore_state
	movsd	.LC6(%rip), %xmm0
	andpd	.LC5(%rip), %xmm1
	movsd	.LC34(%rip), %xmm3
	movapd	%xmm0, %xmm2
	subsd	%xmm1, %xmm2
	mulsd	.LC8(%rip), %xmm2
	movsd	.LC28(%rip), %xmm1
	mulsd	%xmm2, %xmm3
	movapd	%xmm2, %xmm4
	mulsd	%xmm2, %xmm1
	sqrtsd	%xmm4, %xmm4
	subsd	.LC35(%rip), %xmm3
	addsd	.LC29(%rip), %xmm1
	mulsd	%xmm2, %xmm3
	mulsd	%xmm2, %xmm1
	addsd	.LC36(%rip), %xmm3
	subsd	.LC30(%rip), %xmm1
	mulsd	%xmm2, %xmm3
	mulsd	%xmm2, %xmm1
	subsd	.LC37(%rip), %xmm3
	addsd	.LC31(%rip), %xmm1
	mulsd	%xmm2, %xmm3
	mulsd	%xmm2, %xmm1
	subsd	.LC32(%rip), %xmm1
	addsd	%xmm0, %xmm3
	mulsd	%xmm2, %xmm1
	pxor	%xmm0, %xmm0
	addsd	.LC33(%rip), %xmm1
	ucomisd	%xmm2, %xmm0
	mulsd	%xmm2, %xmm1
	ja	.L217
.L210:
	divsd	%xmm3, %xmm1
	cmpl	$1072640818, %ebx
	jle	.L211
	mulsd	%xmm4, %xmm1
	movsd	.LC27(%rip), %xmm0
	addsd	%xmm4, %xmm1
	addsd	%xmm1, %xmm1
	subsd	.LC38(%rip), %xmm1
	subsd	%xmm1, %xmm0
.L212:
	testl	%r12d, %r12d
	jg	.L204
	xorpd	.LC15(%rip), %xmm0
	jmp	.L204
	.p2align 4,,10
	.p2align 3
.L208:
	movapd	%xmm0, %xmm3
	movsd	.LC34(%rip), %xmm2
	mulsd	%xmm0, %xmm3
	movsd	.LC28(%rip), %xmm0
	mulsd	%xmm3, %xmm0
	addsd	.LC29(%rip), %xmm0
	mulsd	%xmm3, %xmm2
	subsd	.LC35(%rip), %xmm2
	mulsd	%xmm3, %xmm0
	subsd	.LC30(%rip), %xmm0
	mulsd	%xmm3, %xmm2
	addsd	.LC36(%rip), %xmm2
	mulsd	%xmm3, %xmm0
	addsd	.LC31(%rip), %xmm0
	mulsd	%xmm3, %xmm2
	subsd	.LC37(%rip), %xmm2
	mulsd	%xmm3, %xmm0
	subsd	.LC32(%rip), %xmm0
	mulsd	%xmm3, %xmm2
	addsd	.LC6(%rip), %xmm2
	mulsd	%xmm3, %xmm0
	addsd	.LC33(%rip), %xmm0
	mulsd	%xmm3, %xmm0
	divsd	%xmm2, %xmm0
	jmp	.L209
	.p2align 4,,10
	.p2align 3
.L211:
	movapd	%xmm4, %xmm3
	movq	%xmm4, %rax
	movabsq	$-4294967296, %rdx
	addsd	%xmm4, %xmm3
	andq	%rdx, %rax
	movq	%rax, %xmm0
	addsd	%xmm0, %xmm4
	mulsd	%xmm3, %xmm1
	movq	%rax, %xmm3
	mulsd	%xmm0, %xmm3
	subsd	%xmm3, %xmm2
	movsd	.LC38(%rip), %xmm3
	divsd	%xmm4, %xmm2
	addsd	%xmm2, %xmm2
	subsd	%xmm2, %xmm3
	movq	%rax, %xmm2
	addsd	%xmm0, %xmm2
	movsd	.LC40(%rip), %xmm0
	movapd	%xmm0, %xmm5
	subsd	%xmm3, %xmm1
	subsd	%xmm2, %xmm5
	subsd	%xmm5, %xmm1
	subsd	%xmm1, %xmm0
	jmp	.L212
.L217:
	movapd	%xmm2, %xmm0
	movsd	%xmm4, -48(%rbp)
	movsd	%xmm3, -40(%rbp)
	movsd	%xmm1, -32(%rbp)
	movsd	%xmm2, -24(%rbp)
	call	sqrt@PLT
	movsd	-48(%rbp), %xmm4
	movsd	-40(%rbp), %xmm3
	movsd	-32(%rbp), %xmm1
	movsd	-24(%rbp), %xmm2
	jmp	.L210
	.cfi_endproc
.LFE3003:
	.size	_ZN2v84base7ieee7544asinEd, .-_ZN2v84base7ieee7544asinEd
	.section	.text._ZN2v84base7ieee7544atanEd,"ax",@progbits
	.p2align 4
	.globl	_ZN2v84base7ieee7544atanEd
	.type	_ZN2v84base7ieee7544atanEd, @function
_ZN2v84base7ieee7544atanEd:
.LFB3005:
	.cfi_startproc
	endbr64
	movq	%xmm0, %rdx
	movapd	%xmm0, %xmm2
	movq	%xmm0, %rcx
	shrq	$32, %rdx
	movl	%edx, %eax
	andl	$2147483647, %eax
	cmpl	$1141899263, %eax
	jle	.L219
	cmpl	$2146435072, %eax
	jg	.L220
	jne	.L221
	testl	%ecx, %ecx
	je	.L221
.L220:
	addsd	%xmm2, %xmm2
	movapd	%xmm2, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L221:
	testl	%edx, %edx
	jle	.L223
	movsd	24+_ZZN2v84base7ieee7544atanEdE6atanlo(%rip), %xmm0
	addsd	.LC27(%rip), %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L219:
	cmpl	$1071382527, %eax
	jg	.L224
	cmpl	$1044381695, %eax
	jle	.L239
.L230:
	pxor	%xmm3, %xmm3
	movl	$-1, %eax
	movapd	%xmm3, %xmm0
.L225:
	movapd	%xmm2, %xmm4
	movsd	.LC50(%rip), %xmm1
	mulsd	%xmm2, %xmm4
	movapd	%xmm4, %xmm5
	mulsd	%xmm4, %xmm5
	mulsd	%xmm5, %xmm1
	addsd	.LC51(%rip), %xmm1
	mulsd	%xmm5, %xmm1
	addsd	.LC52(%rip), %xmm1
	mulsd	%xmm5, %xmm1
	addsd	.LC53(%rip), %xmm1
	mulsd	%xmm5, %xmm1
	addsd	.LC54(%rip), %xmm1
	mulsd	%xmm5, %xmm1
	addsd	.LC55(%rip), %xmm1
	mulsd	%xmm4, %xmm1
	movsd	.LC56(%rip), %xmm4
	mulsd	%xmm5, %xmm4
	subsd	.LC57(%rip), %xmm4
	mulsd	%xmm5, %xmm4
	subsd	.LC58(%rip), %xmm4
	mulsd	%xmm5, %xmm4
	subsd	.LC59(%rip), %xmm4
	mulsd	%xmm5, %xmm4
	subsd	.LC60(%rip), %xmm4
	mulsd	%xmm5, %xmm4
	addsd	%xmm4, %xmm1
	mulsd	%xmm2, %xmm1
	cmpl	$-1, %eax
	je	.L240
	subsd	%xmm3, %xmm1
	subsd	%xmm2, %xmm1
	subsd	%xmm1, %xmm0
	testl	%edx, %edx
	js	.L241
	ret
	.p2align 4,,10
	.p2align 3
.L224:
	andpd	.LC5(%rip), %xmm2
	movapd	%xmm2, %xmm1
	cmpl	$1072889855, %eax
	jg	.L226
	cmpl	$1072037887, %eax
	jg	.L227
	addsd	%xmm2, %xmm2
	addsd	.LC47(%rip), %xmm1
	movsd	.LC44(%rip), %xmm3
	xorl	%eax, %eax
	subsd	.LC6(%rip), %xmm2
	movsd	.LC45(%rip), %xmm0
	divsd	%xmm1, %xmm2
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L226:
	cmpl	$1073971199, %eax
	jg	.L228
	movsd	.LC48(%rip), %xmm3
	movsd	.LC43(%rip), %xmm0
	movl	$2, %eax
	mulsd	%xmm3, %xmm1
	subsd	%xmm3, %xmm2
	movsd	.LC42(%rip), %xmm3
	addsd	.LC6(%rip), %xmm1
	divsd	%xmm1, %xmm2
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L239:
	movsd	.LC39(%rip), %xmm1
	addsd	%xmm0, %xmm1
	comisd	.LC6(%rip), %xmm1
	jbe	.L230
	ret
	.p2align 4,,10
	.p2align 3
.L241:
	xorpd	.LC15(%rip), %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L223:
	movsd	24+_ZZN2v84base7ieee7544atanEdE6atanlo(%rip), %xmm1
	movsd	.LC46(%rip), %xmm0
	subsd	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L240:
	subsd	%xmm1, %xmm2
	movapd	%xmm2, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L227:
	movsd	.LC6(%rip), %xmm3
	movsd	.LC40(%rip), %xmm0
	movl	$1, %eax
	subsd	%xmm3, %xmm2
	addsd	%xmm3, %xmm1
	movsd	.LC41(%rip), %xmm3
	divsd	%xmm1, %xmm2
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L228:
	movsd	.LC49(%rip), %xmm2
	movsd	.LC38(%rip), %xmm3
	movl	$3, %eax
	movsd	.LC27(%rip), %xmm0
	divsd	%xmm1, %xmm2
	jmp	.L225
	.cfi_endproc
.LFE3005:
	.size	_ZN2v84base7ieee7544atanEd, .-_ZN2v84base7ieee7544atanEd
	.section	.text._ZN2v84base7ieee7545atan2Edd,"ax",@progbits
	.p2align 4
	.globl	_ZN2v84base7ieee7545atan2Edd
	.type	_ZN2v84base7ieee7545atan2Edd, @function
_ZN2v84base7ieee7545atan2Edd:
.LFB3006:
	.cfi_startproc
	endbr64
	movq	%xmm1, %rsi
	movq	%xmm1, %rdx
	shrq	$32, %rsi
	movl	%esi, %edi
	andl	$2147483647, %edi
	cmpl	$-2147483648, %edx
	je	.L269
	movd	%xmm1, %eax
	negl	%eax
.L243:
	orl	%edx, %eax
	shrl	$31, %eax
	orl	%edi, %eax
	cmpl	$2146435072, %eax
	ja	.L244
	movq	%xmm0, %r8
	cmpl	$-2147483648, %r8d
	je	.L270
	movd	%xmm0, %eax
	negl	%eax
.L245:
	movq	%xmm0, %rcx
	orl	%r8d, %eax
	shrq	$32, %rcx
	shrl	$31, %eax
	movl	%ecx, %r10d
	andl	$2147483647, %r10d
	orl	%r10d, %eax
	cmpl	$2146435072, %eax
	ja	.L244
	leal	-1072693248(%rsi), %eax
	orl	%edx, %eax
	je	.L288
	movl	%esi, %r9d
	movl	%ecx, %eax
	sarl	$30, %r9d
	shrl	$31, %eax
	andl	$2, %r9d
	orl	%eax, %r9d
	movl	%r8d, %eax
	orl	%r10d, %eax
	jne	.L249
	cmpl	$2, %r9d
	je	.L259
	movapd	%xmm0, %xmm2
	cmpl	$3, %r9d
	jne	.L282
.L260:
	movsd	_ZZN2v84base7ieee7545atan2EddE4tiny(%rip), %xmm0
	movsd	.LC61(%rip), %xmm2
	subsd	%xmm0, %xmm2
	jmp	.L282
	.p2align 4,,10
	.p2align 3
.L244:
	movapd	%xmm1, %xmm2
	addsd	%xmm0, %xmm2
.L282:
	movapd	%xmm2, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L249:
	movl	%edx, %eax
	orl	%edi, %eax
	je	.L287
	cmpl	$2146435072, %edi
	je	.L289
	cmpl	$2146435072, %r10d
	je	.L287
	subl	%edi, %r10d
	movl	%r10d, %eax
	sarl	$20, %eax
	cmpl	$63963135, %r10d
	jle	.L263
	movsd	_ZZN2v84base7ieee7545atan2EddE5pi_lo(%rip), %xmm2
	andl	$1, %r9d
	mulsd	.LC8(%rip), %xmm2
	addsd	.LC27(%rip), %xmm2
	je	.L282
	xorpd	.LC15(%rip), %xmm2
	jmp	.L282
	.p2align 4,,10
	.p2align 3
.L288:
	jmp	_ZN2v84base7ieee7544atanEd
	.p2align 4,,10
	.p2align 3
.L287:
	testl	%ecx, %ecx
	js	.L290
	movsd	_ZZN2v84base7ieee7545atan2EddE4tiny(%rip), %xmm2
	addsd	.LC27(%rip), %xmm2
	jmp	.L282
	.p2align 4,,10
	.p2align 3
.L259:
	movsd	_ZZN2v84base7ieee7545atan2EddE4tiny(%rip), %xmm2
	addsd	.LC25(%rip), %xmm2
	jmp	.L282
	.p2align 4,,10
	.p2align 3
.L289:
	cmpl	$2146435072, %r10d
	je	.L291
	cmpl	$2, %r9d
	je	.L259
	cmpl	$3, %r9d
	je	.L260
	pxor	%xmm2, %xmm2
	cmpl	$1, %r9d
	jne	.L282
	movsd	.LC16(%rip), %xmm2
	jmp	.L282
	.p2align 4,,10
	.p2align 3
.L290:
	movsd	_ZZN2v84base7ieee7545atan2EddE4tiny(%rip), %xmm0
	movsd	.LC46(%rip), %xmm2
	subsd	%xmm0, %xmm2
	jmp	.L282
	.p2align 4,,10
	.p2align 3
.L263:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	shrq	$31, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	je	.L273
	pxor	%xmm2, %xmm2
	cmpl	$-60, %eax
	jge	.L273
.L265:
	cmpl	$1, %r9d
	je	.L264
	cmpl	$2, %r9d
	jne	.L292
	movsd	_ZZN2v84base7ieee7545atan2EddE5pi_lo(%rip), %xmm0
	subsd	%xmm0, %xmm2
	movsd	.LC25(%rip), %xmm0
	subsd	%xmm2, %xmm0
	movapd	%xmm0, %xmm2
.L242:
	movapd	%xmm2, %xmm0
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L292:
	.cfi_restore_state
	testl	%r9d, %r9d
	je	.L242
	movsd	_ZZN2v84base7ieee7545atan2EddE5pi_lo(%rip), %xmm0
	subsd	%xmm0, %xmm2
	subsd	.LC25(%rip), %xmm2
	jmp	.L242
.L291:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	cmpl	$2, %r9d
	je	.L256
	cmpl	$3, %r9d
	je	.L257
	cmpl	$1, %r9d
	je	.L258
	movsd	_ZZN2v84base7ieee7545atan2EddE4tiny(%rip), %xmm2
	addsd	.LC40(%rip), %xmm2
	jmp	.L282
.L269:
	movl	$-2147483648, %eax
	jmp	.L243
.L273:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	divsd	%xmm1, %xmm0
	andpd	.LC5(%rip), %xmm0
	call	_ZN2v84base7ieee7544atanEd
	movapd	%xmm0, %xmm2
	jmp	.L265
.L264:
	xorpd	.LC15(%rip), %xmm2
	jmp	.L242
.L270:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	movl	$-2147483648, %eax
	jmp	.L245
.L258:
	movsd	_ZZN2v84base7ieee7545atan2EddE4tiny(%rip), %xmm0
	movsd	.LC62(%rip), %xmm2
	subsd	%xmm0, %xmm2
	jmp	.L282
.L257:
	movsd	_ZZN2v84base7ieee7545atan2EddE4tiny(%rip), %xmm0
	movsd	.LC64(%rip), %xmm2
	subsd	%xmm0, %xmm2
	jmp	.L282
.L256:
	movsd	_ZZN2v84base7ieee7545atan2EddE4tiny(%rip), %xmm2
	addsd	.LC63(%rip), %xmm2
	jmp	.L282
	.cfi_endproc
.LFE3006:
	.size	_ZN2v84base7ieee7545atan2Edd, .-_ZN2v84base7ieee7545atan2Edd
	.section	.text._ZN2v84base7ieee7543cosEd,"ax",@progbits
	.p2align 4
	.globl	_ZN2v84base7ieee7543cosEd
	.type	_ZN2v84base7ieee7543cosEd, @function
_ZN2v84base7ieee7543cosEd:
.LFB3007:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	%xmm0, %rax
	shrq	$32, %rax
	andl	$2147483647, %eax
	cmpl	$1072243195, %eax
	jg	.L294
	cmpl	$1044381695, %eax
	jle	.L338
	movapd	%xmm0, %xmm3
	movsd	.LC67(%rip), %xmm1
	mulsd	%xmm0, %xmm3
	mulsd	%xmm3, %xmm1
	addsd	.LC68(%rip), %xmm1
	mulsd	%xmm3, %xmm1
	subsd	.LC69(%rip), %xmm1
	mulsd	%xmm3, %xmm1
	addsd	.LC70(%rip), %xmm1
	mulsd	%xmm3, %xmm1
	subsd	.LC71(%rip), %xmm1
	mulsd	%xmm3, %xmm1
	addsd	.LC72(%rip), %xmm1
	mulsd	%xmm3, %xmm1
	cmpl	$1070805810, %eax
	jle	.L319
	cmpl	$1072234496, %eax
	jg	.L321
	subl	$2097152, %eax
	movsd	.LC6(%rip), %xmm5
	salq	$32, %rax
	movq	%rax, %xmm4
	subsd	%xmm4, %xmm5
.L299:
	movsd	.LC8(%rip), %xmm2
	mulsd	.LC0(%rip), %xmm0
	mulsd	%xmm3, %xmm1
	mulsd	%xmm3, %xmm2
	subsd	%xmm0, %xmm1
	subsd	%xmm4, %xmm2
	subsd	%xmm1, %xmm2
	subsd	%xmm2, %xmm5
	movapd	%xmm5, %xmm0
	jmp	.L293
	.p2align 4,,10
	.p2align 3
.L294:
	cmpl	$2146435071, %eax
	jle	.L300
	subsd	%xmm0, %xmm0
.L293:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L339
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L338:
	.cfi_restore_state
	cvttsd2sil	%xmm0, %eax
	testl	%eax, %eax
	je	.L305
	movapd	%xmm0, %xmm3
	movsd	.LC67(%rip), %xmm2
	mulsd	%xmm0, %xmm3
	mulsd	%xmm3, %xmm2
	addsd	.LC68(%rip), %xmm2
	mulsd	%xmm3, %xmm2
	subsd	.LC69(%rip), %xmm2
	mulsd	%xmm3, %xmm2
	addsd	.LC70(%rip), %xmm2
	mulsd	%xmm3, %xmm2
	subsd	.LC71(%rip), %xmm2
	mulsd	%xmm3, %xmm2
	addsd	.LC72(%rip), %xmm2
	mulsd	%xmm3, %xmm2
	movapd	%xmm2, %xmm1
.L319:
	mulsd	.LC0(%rip), %xmm0
	movsd	.LC8(%rip), %xmm2
	mulsd	%xmm3, %xmm1
	mulsd	%xmm3, %xmm2
	subsd	%xmm0, %xmm1
	movsd	.LC6(%rip), %xmm0
	subsd	%xmm1, %xmm2
	subsd	%xmm2, %xmm0
	jmp	.L293
	.p2align 4,,10
	.p2align 3
.L300:
	leaq	-32(%rbp), %rdi
	call	_ZN2v84base7ieee75412_GLOBAL__N_118__ieee754_rem_pio2EdPd
	andl	$3, %eax
	cmpl	$1, %eax
	je	.L301
	cmpl	$2, %eax
	je	.L302
	testl	%eax, %eax
	je	.L340
	movsd	-32(%rbp), %xmm0
	movsd	-24(%rbp), %xmm5
	movq	%xmm0, %rax
	shrq	$32, %rax
	andl	$2147483647, %eax
	cmpl	$1044381695, %eax
	jg	.L316
	cvttsd2sil	%xmm0, %eax
	testl	%eax, %eax
	je	.L293
.L316:
	movapd	%xmm0, %xmm3
	movsd	.LC73(%rip), %xmm1
	movapd	%xmm0, %xmm4
	movsd	.LC8(%rip), %xmm2
	mulsd	%xmm0, %xmm3
	mulsd	%xmm5, %xmm2
	mulsd	%xmm3, %xmm1
	subsd	.LC74(%rip), %xmm1
	mulsd	%xmm3, %xmm4
	mulsd	%xmm3, %xmm1
	addsd	.LC75(%rip), %xmm1
	mulsd	%xmm3, %xmm1
	subsd	.LC76(%rip), %xmm1
	mulsd	%xmm3, %xmm1
	addsd	.LC77(%rip), %xmm1
	mulsd	%xmm4, %xmm1
	mulsd	.LC78(%rip), %xmm4
	subsd	%xmm1, %xmm2
	movapd	%xmm2, %xmm1
	mulsd	%xmm3, %xmm1
	subsd	%xmm5, %xmm1
	addsd	%xmm4, %xmm1
	subsd	%xmm1, %xmm0
	jmp	.L293
	.p2align 4,,10
	.p2align 3
.L321:
	movsd	.LC65(%rip), %xmm5
	movsd	.LC66(%rip), %xmm4
	jmp	.L299
	.p2align 4,,10
	.p2align 3
.L305:
	movsd	.LC6(%rip), %xmm0
	jmp	.L293
	.p2align 4,,10
	.p2align 3
.L302:
	movsd	-32(%rbp), %xmm2
	movsd	-24(%rbp), %xmm3
	movq	%xmm2, %rax
	shrq	$32, %rax
	andl	$2147483647, %eax
	cmpl	$1044381695, %eax
	jle	.L341
	movapd	%xmm2, %xmm4
	movsd	.LC67(%rip), %xmm1
	mulsd	%xmm2, %xmm4
	mulsd	%xmm4, %xmm1
	addsd	.LC68(%rip), %xmm1
	mulsd	%xmm4, %xmm1
	subsd	.LC69(%rip), %xmm1
	mulsd	%xmm4, %xmm1
	addsd	.LC70(%rip), %xmm1
	mulsd	%xmm4, %xmm1
	subsd	.LC71(%rip), %xmm1
	mulsd	%xmm4, %xmm1
	addsd	.LC72(%rip), %xmm1
	mulsd	%xmm4, %xmm1
	cmpl	$1070805810, %eax
	jle	.L318
	cmpl	$1072234496, %eax
	jg	.L324
	subl	$2097152, %eax
	movsd	.LC6(%rip), %xmm0
	salq	$32, %rax
	movq	%rax, %xmm6
	subsd	%xmm6, %xmm0
.L315:
	movsd	.LC8(%rip), %xmm5
	mulsd	%xmm4, %xmm1
	mulsd	%xmm3, %xmm2
	mulsd	%xmm4, %xmm5
	subsd	%xmm2, %xmm1
	subsd	%xmm6, %xmm5
	subsd	%xmm1, %xmm5
	subsd	%xmm5, %xmm0
	xorpd	.LC15(%rip), %xmm0
	jmp	.L293
	.p2align 4,,10
	.p2align 3
.L340:
	movsd	-32(%rbp), %xmm2
	movsd	-24(%rbp), %xmm3
	movq	%xmm2, %rax
	shrq	$32, %rax
	andl	$2147483647, %eax
	cmpl	$1044381695, %eax
	jle	.L342
	movapd	%xmm2, %xmm4
	movsd	.LC67(%rip), %xmm1
	mulsd	%xmm2, %xmm4
	mulsd	%xmm4, %xmm1
	addsd	.LC68(%rip), %xmm1
	mulsd	%xmm4, %xmm1
	subsd	.LC69(%rip), %xmm1
	mulsd	%xmm4, %xmm1
	addsd	.LC70(%rip), %xmm1
	mulsd	%xmm4, %xmm1
	subsd	.LC71(%rip), %xmm1
	mulsd	%xmm4, %xmm1
	addsd	.LC72(%rip), %xmm1
	mulsd	%xmm4, %xmm1
	cmpl	$1070805810, %eax
	jle	.L317
	cmpl	$1072234496, %eax
	jg	.L322
	subl	$2097152, %eax
	movsd	.LC6(%rip), %xmm0
	salq	$32, %rax
	movq	%rax, %xmm6
	subsd	%xmm6, %xmm0
.L308:
	movsd	.LC8(%rip), %xmm5
	mulsd	%xmm4, %xmm1
	mulsd	%xmm3, %xmm2
	mulsd	%xmm4, %xmm5
	subsd	%xmm2, %xmm1
	subsd	%xmm6, %xmm5
	subsd	%xmm1, %xmm5
	subsd	%xmm5, %xmm0
	jmp	.L293
	.p2align 4,,10
	.p2align 3
.L301:
	movsd	-32(%rbp), %xmm0
	movsd	-24(%rbp), %xmm5
	movq	%xmm0, %rax
	shrq	$32, %rax
	andl	$2147483647, %eax
	cmpl	$1044381695, %eax
	jle	.L343
.L309:
	movapd	%xmm0, %xmm3
	movsd	.LC73(%rip), %xmm1
	movapd	%xmm0, %xmm4
	movsd	.LC8(%rip), %xmm2
	mulsd	%xmm0, %xmm3
	mulsd	%xmm5, %xmm2
	mulsd	%xmm3, %xmm1
	subsd	.LC74(%rip), %xmm1
	mulsd	%xmm3, %xmm4
	mulsd	%xmm3, %xmm1
	addsd	.LC75(%rip), %xmm1
	mulsd	%xmm3, %xmm1
	subsd	.LC76(%rip), %xmm1
	mulsd	%xmm3, %xmm1
	addsd	.LC77(%rip), %xmm1
	mulsd	%xmm4, %xmm1
	mulsd	.LC78(%rip), %xmm4
	subsd	%xmm1, %xmm2
	movapd	%xmm2, %xmm1
	mulsd	%xmm3, %xmm1
	subsd	%xmm5, %xmm1
	addsd	%xmm4, %xmm1
	subsd	%xmm1, %xmm0
.L310:
	xorpd	.LC15(%rip), %xmm0
	jmp	.L293
	.p2align 4,,10
	.p2align 3
.L343:
	cvttsd2sil	%xmm0, %eax
	testl	%eax, %eax
	jne	.L309
	jmp	.L310
	.p2align 4,,10
	.p2align 3
.L342:
	cvttsd2sil	%xmm2, %eax
	testl	%eax, %eax
	je	.L305
	movapd	%xmm2, %xmm4
	movsd	.LC67(%rip), %xmm0
	mulsd	%xmm2, %xmm4
	mulsd	%xmm4, %xmm0
	addsd	.LC68(%rip), %xmm0
	mulsd	%xmm4, %xmm0
	subsd	.LC69(%rip), %xmm0
	mulsd	%xmm4, %xmm0
	addsd	.LC70(%rip), %xmm0
	mulsd	%xmm4, %xmm0
	subsd	.LC71(%rip), %xmm0
	mulsd	%xmm4, %xmm0
	addsd	.LC72(%rip), %xmm0
	movapd	%xmm0, %xmm1
	mulsd	%xmm4, %xmm1
.L317:
	mulsd	%xmm4, %xmm1
	movsd	.LC8(%rip), %xmm0
	mulsd	%xmm3, %xmm2
	mulsd	%xmm4, %xmm0
	subsd	%xmm2, %xmm1
	subsd	%xmm1, %xmm0
	movapd	%xmm0, %xmm1
	movsd	.LC6(%rip), %xmm0
	subsd	%xmm1, %xmm0
	jmp	.L293
	.p2align 4,,10
	.p2align 3
.L341:
	cvttsd2sil	%xmm2, %eax
	movsd	.LC49(%rip), %xmm0
	testl	%eax, %eax
	je	.L293
	movapd	%xmm2, %xmm4
	movsd	.LC67(%rip), %xmm0
	mulsd	%xmm2, %xmm4
	mulsd	%xmm4, %xmm0
	addsd	.LC68(%rip), %xmm0
	mulsd	%xmm4, %xmm0
	subsd	.LC69(%rip), %xmm0
	mulsd	%xmm4, %xmm0
	addsd	.LC70(%rip), %xmm0
	mulsd	%xmm4, %xmm0
	subsd	.LC71(%rip), %xmm0
	mulsd	%xmm4, %xmm0
	addsd	.LC72(%rip), %xmm0
	movapd	%xmm0, %xmm1
	mulsd	%xmm4, %xmm1
.L318:
	mulsd	%xmm4, %xmm1
	movsd	.LC8(%rip), %xmm0
	mulsd	%xmm3, %xmm2
	mulsd	%xmm4, %xmm0
	subsd	%xmm2, %xmm1
	subsd	%xmm1, %xmm0
	movapd	%xmm0, %xmm1
	movsd	.LC6(%rip), %xmm0
	subsd	%xmm1, %xmm0
	xorpd	.LC15(%rip), %xmm0
	jmp	.L293
	.p2align 4,,10
	.p2align 3
.L322:
	movsd	.LC65(%rip), %xmm0
	movsd	.LC66(%rip), %xmm6
	jmp	.L308
	.p2align 4,,10
	.p2align 3
.L324:
	movsd	.LC65(%rip), %xmm0
	movsd	.LC66(%rip), %xmm6
	jmp	.L315
.L339:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3007:
	.size	_ZN2v84base7ieee7543cosEd, .-_ZN2v84base7ieee7543cosEd
	.section	.text._ZN2v84base7ieee7543expEd,"ax",@progbits
	.p2align 4
	.globl	_ZN2v84base7ieee7543expEd
	.type	_ZN2v84base7ieee7543expEd, @function
_ZN2v84base7ieee7543expEd:
.LFB3008:
	.cfi_startproc
	endbr64
	movq	%xmm0, %rcx
	movq	%xmm0, %rax
	movq	%xmm0, %rdx
	shrq	$63, %rax
	shrq	$32, %rcx
	movq	%rax, %rsi
	movl	%ecx, %eax
	andl	$2147483647, %eax
	cmpl	$1082535489, %eax
	jbe	.L345
	cmpl	$2146435071, %eax
	jbe	.L346
	andl	$1048575, %ecx
	orl	%edx, %ecx
	jne	.L376
	testl	%esi, %esi
	je	.L344
	pxor	%xmm0, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L345:
	cmpl	$1071001154, %eax
	ja	.L377
	movsd	.LC6(%rip), %xmm2
	cmpl	$1043333119, %eax
	jbe	.L378
.L357:
	movapd	%xmm0, %xmm5
	pxor	%xmm3, %xmm3
	movl	$1072693248, %edx
	xorl	%eax, %eax
	mulsd	%xmm0, %xmm5
	movapd	%xmm3, %xmm4
.L375:
	movsd	.LC85(%rip), %xmm1
	movsd	.LC88(%rip), %xmm7
	movsd	.LC86(%rip), %xmm9
	movsd	.LC89(%rip), %xmm6
	movsd	.LC87(%rip), %xmm8
.L355:
	mulsd	%xmm5, %xmm1
	salq	$32, %rdx
	subsd	%xmm9, %xmm1
	mulsd	%xmm5, %xmm1
	addsd	%xmm8, %xmm1
	mulsd	%xmm5, %xmm1
	subsd	%xmm7, %xmm1
	mulsd	%xmm5, %xmm1
	addsd	%xmm6, %xmm1
	mulsd	%xmm5, %xmm1
	movapd	%xmm0, %xmm5
	subsd	%xmm1, %xmm5
	movapd	%xmm5, %xmm1
	mulsd	%xmm0, %xmm1
	testl	%eax, %eax
	jne	.L379
	subsd	.LC47(%rip), %xmm5
	divsd	%xmm5, %xmm1
	subsd	%xmm0, %xmm1
	subsd	%xmm1, %xmm2
	movapd	%xmm2, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L346:
	comisd	.LC80(%rip), %xmm0
	ja	.L380
	movsd	.LC81(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jbe	.L351
	movsd	_ZZN2v84base7ieee7543expEdE8twom1000(%rip), %xmm0
	movsd	_ZZN2v84base7ieee7543expEdE8twom1000(%rip), %xmm1
	mulsd	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L377:
	cmpl	$1072734897, %eax
	ja	.L351
	movsd	.LC6(%rip), %xmm2
	ucomisd	%xmm2, %xmm0
	jnp	.L381
.L367:
	movslq	%esi, %rax
	leaq	_ZZN2v84base7ieee7543expEdE5ln2HI(%rip), %rdx
	movapd	%xmm0, %xmm4
	subsd	(%rdx,%rax,8), %xmm4
	leaq	_ZZN2v84base7ieee7543expEdE5ln2LO(%rip), %rdx
	movsd	(%rdx,%rax,8), %xmm3
	movl	$1, %eax
	subl	%esi, %eax
	movapd	%xmm4, %xmm0
	subl	%esi, %eax
	subsd	%xmm3, %xmm0
	movl	%eax, %edx
	sall	$20, %edx
	addl	$1072693248, %edx
	movapd	%xmm0, %xmm5
	mulsd	%xmm0, %xmm5
	jmp	.L375
	.p2align 4,,10
	.p2align 3
.L351:
	movsd	.LC82(%rip), %xmm1
	movslq	%esi, %rsi
	pxor	%xmm3, %xmm3
	movapd	%xmm0, %xmm4
	leaq	_ZZN2v84base7ieee7543expEdE4halF(%rip), %rax
	movsd	.LC88(%rip), %xmm7
	movsd	.LC86(%rip), %xmm9
	mulsd	%xmm0, %xmm1
	movsd	.LC89(%rip), %xmm6
	movsd	.LC87(%rip), %xmm8
	addsd	(%rax,%rsi,8), %xmm1
	cvttsd2sil	%xmm1, %eax
	movsd	.LC83(%rip), %xmm1
	cvtsi2sdl	%eax, %xmm3
	mulsd	%xmm3, %xmm1
	mulsd	.LC84(%rip), %xmm3
	subsd	%xmm1, %xmm4
	movsd	.LC85(%rip), %xmm1
	movapd	%xmm4, %xmm0
	subsd	%xmm3, %xmm0
	movapd	%xmm0, %xmm5
	movapd	%xmm0, %xmm10
	mulsd	%xmm0, %xmm5
	movapd	%xmm5, %xmm2
	mulsd	%xmm1, %xmm2
	subsd	%xmm9, %xmm2
	mulsd	%xmm5, %xmm2
	addsd	%xmm8, %xmm2
	mulsd	%xmm5, %xmm2
	subsd	%xmm7, %xmm2
	mulsd	%xmm5, %xmm2
	addsd	%xmm6, %xmm2
	mulsd	%xmm5, %xmm2
	subsd	%xmm2, %xmm10
	movapd	%xmm0, %xmm2
	mulsd	%xmm10, %xmm2
	cmpl	$-1021, %eax
	jge	.L382
	movsd	.LC47(%rip), %xmm1
	sall	$20, %eax
	movsd	_ZZN2v84base7ieee7543expEdE8twom1000(%rip), %xmm5
	addl	$2121269248, %eax
	subsd	%xmm10, %xmm1
	salq	$32, %rax
	movq	%rax, %xmm0
	divsd	%xmm1, %xmm2
	movsd	.LC6(%rip), %xmm1
	subsd	%xmm2, %xmm3
	subsd	%xmm4, %xmm3
	subsd	%xmm3, %xmm1
	mulsd	%xmm1, %xmm0
	mulsd	%xmm5, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L378:
	movsd	_ZZN2v84base7ieee7543expEdE4huge(%rip), %xmm1
	movsd	.LC6(%rip), %xmm2
	addsd	%xmm0, %xmm1
	comisd	%xmm2, %xmm1
	jbe	.L357
	addsd	%xmm2, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L376:
	addsd	%xmm0, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L380:
	movsd	_ZZN2v84base7ieee7543expEdE4huge(%rip), %xmm0
	movsd	_ZZN2v84base7ieee7543expEdE4huge(%rip), %xmm1
	mulsd	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L379:
	movsd	.LC47(%rip), %xmm0
	subsd	%xmm5, %xmm0
	divsd	%xmm0, %xmm1
	subsd	%xmm1, %xmm3
	subsd	%xmm4, %xmm3
	subsd	%xmm3, %xmm2
	movapd	%xmm2, %xmm0
	cmpl	$1024, %eax
	jne	.L383
	addsd	%xmm0, %xmm0
	movsd	_ZZN2v84base7ieee7543expEdE7two1023(%rip), %xmm1
	mulsd	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L344:
	ret
	.p2align 4,,10
	.p2align 3
.L383:
	movq	%rdx, %xmm6
	mulsd	%xmm6, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L381:
	jne	.L367
	movsd	.LC79(%rip), %xmm0
	ret
.L382:
	movl	%eax, %edx
	movsd	.LC6(%rip), %xmm2
	sall	$20, %edx
	addl	$1072693248, %edx
	jmp	.L355
	.cfi_endproc
.LFE3008:
	.size	_ZN2v84base7ieee7543expEd, .-_ZN2v84base7ieee7543expEd
	.section	.text._ZN2v84base7ieee7543logEd,"ax",@progbits
	.p2align 4
	.globl	_ZN2v84base7ieee7543logEd
	.type	_ZN2v84base7ieee7543logEd, @function
_ZN2v84base7ieee7543logEd:
.LFB3010:
	.cfi_startproc
	endbr64
	movq	%xmm0, %rax
	movq	%xmm0, %rcx
	shrq	$32, %rax
	movl	%eax, %edx
	cmpl	$1048575, %eax
	jg	.L395
	andl	$2147483647, %edx
	movsd	.LC90(%rip), %xmm1
	orl	%ecx, %edx
	jne	.L403
.L384:
	movapd	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L395:
	xorl	%eax, %eax
	cmpl	$2146435071, %edx
	jg	.L404
.L387:
	movl	%edx, %ecx
	movl	%edx, %esi
	movq	%xmm0, %rdx
	andl	$1048575, %esi
	sarl	$20, %ecx
	leal	-1023(%rax,%rcx), %r8d
	leal	614244(%rsi), %ecx
	andl	$1048576, %ecx
	movl	%ecx, %eax
	sarl	$20, %ecx
	xorl	$1072693248, %eax
	addl	%r8d, %ecx
	orl	%esi, %eax
	movq	%rax, %rdi
	movl	%edx, %eax
	salq	$32, %rdi
	orq	%rdi, %rax
	movq	%rax, %xmm0
	leal	2(%rsi), %eax
	subsd	.LC6(%rip), %xmm0
	andl	$1048575, %eax
	cmpl	$2, %eax
	jg	.L388
	pxor	%xmm6, %xmm6
	ucomisd	%xmm6, %xmm0
	jp	.L389
	jne	.L389
	pxor	%xmm1, %xmm1
	testl	%ecx, %ecx
	je	.L384
	pxor	%xmm0, %xmm0
	movsd	.LC83(%rip), %xmm1
	cvtsi2sdl	%ecx, %xmm0
	mulsd	%xmm0, %xmm1
	mulsd	.LC84(%rip), %xmm0
	addsd	%xmm0, %xmm1
	jmp	.L384
	.p2align 4,,10
	.p2align 3
.L403:
	movsd	.LC26(%rip), %xmm1
	testl	%eax, %eax
	js	.L384
	mulsd	.LC91(%rip), %xmm0
	movl	$-54, %eax
	movq	%xmm0, %rdx
	shrq	$32, %rdx
	cmpl	$2146435071, %edx
	jle	.L387
.L404:
	movapd	%xmm0, %xmm1
	addsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L388:
	movsd	.LC47(%rip), %xmm1
	movapd	%xmm0, %xmm4
	movl	$440401, %eax
	movsd	.LC93(%rip), %xmm2
	leal	-398458(%rsi), %edx
	subl	%esi, %eax
	pxor	%xmm3, %xmm3
	orl	%edx, %eax
	cvtsi2sdl	%ecx, %xmm3
	addsd	%xmm0, %xmm1
	divsd	%xmm1, %xmm4
	movapd	%xmm4, %xmm1
	mulsd	%xmm4, %xmm1
	movapd	%xmm1, %xmm5
	mulsd	%xmm1, %xmm5
	mulsd	%xmm5, %xmm2
	addsd	.LC94(%rip), %xmm2
	mulsd	%xmm5, %xmm2
	addsd	.LC95(%rip), %xmm2
	mulsd	%xmm5, %xmm2
	addsd	.LC96(%rip), %xmm2
	mulsd	%xmm1, %xmm2
	movsd	.LC97(%rip), %xmm1
	mulsd	%xmm5, %xmm1
	addsd	.LC98(%rip), %xmm1
	mulsd	%xmm5, %xmm1
	addsd	.LC99(%rip), %xmm1
	mulsd	%xmm5, %xmm1
	addsd	%xmm1, %xmm2
	jle	.L392
	movsd	.LC8(%rip), %xmm5
	mulsd	%xmm0, %xmm5
	mulsd	%xmm0, %xmm5
	addsd	%xmm5, %xmm2
	mulsd	%xmm4, %xmm2
	testl	%ecx, %ecx
	jne	.L393
	subsd	%xmm2, %xmm5
	movapd	%xmm0, %xmm1
	subsd	%xmm5, %xmm1
	jmp	.L384
	.p2align 4,,10
	.p2align 3
.L389:
	movsd	.LC92(%rip), %xmm1
	movsd	.LC8(%rip), %xmm2
	mulsd	%xmm0, %xmm1
	subsd	%xmm1, %xmm2
	movapd	%xmm0, %xmm1
	mulsd	%xmm0, %xmm1
	mulsd	%xmm1, %xmm2
	testl	%ecx, %ecx
	je	.L402
	pxor	%xmm1, %xmm1
	movsd	.LC83(%rip), %xmm3
	cvtsi2sdl	%ecx, %xmm1
	mulsd	%xmm1, %xmm3
	mulsd	.LC84(%rip), %xmm1
	subsd	%xmm1, %xmm2
	subsd	%xmm0, %xmm2
	subsd	%xmm2, %xmm3
	movapd	%xmm3, %xmm1
	jmp	.L384
	.p2align 4,,10
	.p2align 3
.L392:
	movapd	%xmm0, %xmm7
	subsd	%xmm2, %xmm7
	movapd	%xmm7, %xmm2
	mulsd	%xmm4, %xmm2
	testl	%ecx, %ecx
	jne	.L394
.L402:
	movapd	%xmm0, %xmm1
	subsd	%xmm2, %xmm1
	jmp	.L384
	.p2align 4,,10
	.p2align 3
.L393:
	movsd	.LC83(%rip), %xmm1
	mulsd	%xmm3, %xmm1
	mulsd	.LC84(%rip), %xmm3
	addsd	%xmm3, %xmm2
	subsd	%xmm2, %xmm5
	subsd	%xmm0, %xmm5
	subsd	%xmm5, %xmm1
	jmp	.L384
	.p2align 4,,10
	.p2align 3
.L394:
	movsd	.LC83(%rip), %xmm1
	mulsd	%xmm3, %xmm1
	mulsd	.LC84(%rip), %xmm3
	subsd	%xmm3, %xmm2
	subsd	%xmm0, %xmm2
	subsd	%xmm2, %xmm1
	jmp	.L384
	.cfi_endproc
.LFE3010:
	.size	_ZN2v84base7ieee7543logEd, .-_ZN2v84base7ieee7543logEd
	.section	.text._ZN2v84base7ieee7545log1pEd,"ax",@progbits
	.p2align 4
	.globl	_ZN2v84base7ieee7545log1pEd
	.type	_ZN2v84base7ieee7545log1pEd, @function
_ZN2v84base7ieee7545log1pEd:
.LFB3011:
	.cfi_startproc
	endbr64
	movq	%xmm0, %rax
	shrq	$32, %rax
	movl	%eax, %edx
	cmpl	$1071284857, %eax
	jle	.L406
	cmpl	$2146435071, %eax
	jg	.L437
	cmpl	$1128267775, %eax
	jle	.L414
	sarl	$20, %eax
	movsd	.LC6(%rip), %xmm5
	movapd	%xmm0, %xmm4
	pxor	%xmm2, %xmm2
	subl	$1023, %eax
.L419:
	andl	$1048575, %edx
	cmpl	$434333, %edx
	jg	.L420
	movl	%edx, %esi
	movq	%xmm4, %rcx
	orl	$1072693248, %esi
	movl	%ecx, %ecx
	salq	$32, %rsi
	orq	%rsi, %rcx
	movq	%rcx, %xmm0
.L421:
	subsd	%xmm5, %xmm0
	movsd	.LC8(%rip), %xmm3
	mulsd	%xmm0, %xmm3
	mulsd	%xmm0, %xmm3
	testl	%edx, %edx
	jne	.L415
	pxor	%xmm6, %xmm6
	ucomisd	%xmm6, %xmm0
	jp	.L422
	jne	.L422
	pxor	%xmm4, %xmm4
	testl	%eax, %eax
	je	.L405
	pxor	%xmm0, %xmm0
	movsd	.LC84(%rip), %xmm1
	cvtsi2sdl	%eax, %xmm0
	mulsd	%xmm0, %xmm1
	mulsd	.LC83(%rip), %xmm0
	addsd	%xmm1, %xmm2
	addsd	%xmm0, %xmm2
	movapd	%xmm2, %xmm4
	jmp	.L405
	.p2align 4,,10
	.p2align 3
.L437:
	movapd	%xmm0, %xmm4
	addsd	%xmm0, %xmm4
.L405:
	movapd	%xmm4, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L406:
	andl	$2147483647, %edx
	cmpl	$1072693247, %edx
	jle	.L409
	ucomisd	.LC49(%rip), %xmm0
	jp	.L429
	jne	.L429
	movsd	.LC90(%rip), %xmm4
	movapd	%xmm4, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L409:
	cmpl	$1042284543, %edx
	jle	.L438
	addl	$1076707643, %eax
	cmpl	$1076707643, %eax
	jbe	.L414
	movsd	.LC8(%rip), %xmm3
	xorl	%eax, %eax
	mulsd	%xmm0, %xmm3
	mulsd	%xmm0, %xmm3
.L415:
	movsd	.LC47(%rip), %xmm1
	movapd	%xmm0, %xmm4
	addsd	%xmm0, %xmm1
	divsd	%xmm1, %xmm4
	movsd	.LC93(%rip), %xmm1
	movapd	%xmm4, %xmm5
	mulsd	%xmm4, %xmm5
	mulsd	%xmm5, %xmm1
	addsd	.LC97(%rip), %xmm1
	mulsd	%xmm5, %xmm1
	addsd	.LC94(%rip), %xmm1
	mulsd	%xmm5, %xmm1
	addsd	.LC98(%rip), %xmm1
	mulsd	%xmm5, %xmm1
	addsd	.LC95(%rip), %xmm1
	mulsd	%xmm5, %xmm1
	addsd	.LC99(%rip), %xmm1
	mulsd	%xmm5, %xmm1
	addsd	.LC96(%rip), %xmm1
	mulsd	%xmm5, %xmm1
	addsd	%xmm3, %xmm1
	mulsd	%xmm4, %xmm1
	testl	%eax, %eax
	jne	.L425
	subsd	%xmm1, %xmm3
	movapd	%xmm0, %xmm4
	subsd	%xmm3, %xmm4
	movapd	%xmm4, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L438:
	movsd	.LC91(%rip), %xmm1
	addsd	%xmm0, %xmm1
	comisd	.LC0(%rip), %xmm1
	jbe	.L430
	movapd	%xmm0, %xmm4
	cmpl	$1016070143, %edx
	jle	.L405
.L430:
	movapd	%xmm0, %xmm1
	movapd	%xmm0, %xmm4
	mulsd	%xmm0, %xmm1
	mulsd	.LC8(%rip), %xmm1
	subsd	%xmm1, %xmm4
	jmp	.L405
	.p2align 4,,10
	.p2align 3
.L429:
	movsd	.LC26(%rip), %xmm4
	movapd	%xmm4, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L422:
	movsd	.LC100(%rip), %xmm1
	mulsd	%xmm0, %xmm1
	subsd	%xmm1, %xmm5
	mulsd	%xmm3, %xmm5
	testl	%eax, %eax
	jne	.L424
	movapd	%xmm0, %xmm4
	subsd	%xmm5, %xmm4
	jmp	.L405
	.p2align 4,,10
	.p2align 3
.L425:
	pxor	%xmm5, %xmm5
	movsd	.LC83(%rip), %xmm4
	cvtsi2sdl	%eax, %xmm5
	mulsd	%xmm5, %xmm4
	mulsd	.LC84(%rip), %xmm5
	addsd	%xmm5, %xmm2
	addsd	%xmm2, %xmm1
	subsd	%xmm1, %xmm3
	subsd	%xmm0, %xmm3
	subsd	%xmm3, %xmm4
	movapd	%xmm4, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L420:
	movl	%edx, %esi
	movq	%xmm4, %rcx
	addl	$1, %eax
	orl	$1071644672, %esi
	movl	%ecx, %ecx
	salq	$32, %rsi
	orq	%rsi, %rcx
	movq	%rcx, %xmm0
	movl	$1048576, %ecx
	subl	%edx, %ecx
	movl	%ecx, %edx
	sarl	$2, %edx
	jmp	.L421
	.p2align 4,,10
	.p2align 3
.L414:
	movsd	.LC6(%rip), %xmm5
	movapd	%xmm0, %xmm4
	addsd	%xmm5, %xmm4
	movq	%xmm4, %rax
	shrq	$32, %rax
	movl	%eax, %edx
	sarl	$20, %eax
	subl	$1023, %eax
	testl	%eax, %eax
	jle	.L417
	movapd	%xmm4, %xmm7
	movapd	%xmm5, %xmm2
	subsd	%xmm0, %xmm7
	subsd	%xmm7, %xmm2
.L418:
	divsd	%xmm4, %xmm2
	jmp	.L419
	.p2align 4,,10
	.p2align 3
.L424:
	pxor	%xmm3, %xmm3
	movsd	.LC83(%rip), %xmm1
	cvtsi2sdl	%eax, %xmm3
	mulsd	%xmm3, %xmm1
	mulsd	.LC84(%rip), %xmm3
	movapd	%xmm1, %xmm4
	addsd	%xmm3, %xmm2
	subsd	%xmm2, %xmm5
	subsd	%xmm0, %xmm5
	subsd	%xmm5, %xmm4
	jmp	.L405
	.p2align 4,,10
	.p2align 3
.L417:
	movapd	%xmm4, %xmm1
	movapd	%xmm0, %xmm2
	subsd	%xmm5, %xmm1
	subsd	%xmm1, %xmm2
	jmp	.L418
	.cfi_endproc
.LFE3011:
	.size	_ZN2v84base7ieee7545log1pEd, .-_ZN2v84base7ieee7545log1pEd
	.section	.text._ZN2v84base7ieee7545acoshEd,"ax",@progbits
	.p2align 4
	.globl	_ZN2v84base7ieee7545acoshEd
	.type	_ZN2v84base7ieee7545acoshEd, @function
_ZN2v84base7ieee7545acoshEd:
.LFB3002:
	.cfi_startproc
	endbr64
	movq	%xmm0, %rax
	shrq	$32, %rax
	cmpl	$1072693247, %eax
	jle	.L446
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movapd	%xmm0, %xmm1
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	cmpl	$1102053375, %eax
	jle	.L441
	cmpl	$2146435071, %eax
	jg	.L452
	call	_ZN2v84base7ieee7543logEd
	addsd	.LC101(%rip), %xmm0
	leave
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L446:
	.cfi_restore 6
	movsd	.LC26(%rip), %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L441:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	movq	%xmm0, %rdx
	leal	-1072693248(%rax), %ecx
	orl	%edx, %ecx
	je	.L447
	cmpl	$1073741824, %eax
	jle	.L443
	movapd	%xmm0, %xmm3
	movsd	.LC6(%rip), %xmm2
	pxor	%xmm5, %xmm5
	addsd	%xmm0, %xmm3
	mulsd	%xmm0, %xmm0
	subsd	%xmm2, %xmm0
	ucomisd	%xmm0, %xmm5
	movapd	%xmm0, %xmm4
	sqrtsd	%xmm4, %xmm4
	ja	.L453
.L444:
	leave
	.cfi_remember_state
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	addsd	%xmm4, %xmm1
	divsd	%xmm1, %xmm2
	subsd	%xmm2, %xmm3
	movapd	%xmm3, %xmm0
	jmp	_ZN2v84base7ieee7543logEd
	.p2align 4,,10
	.p2align 3
.L452:
	.cfi_restore_state
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	addsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L447:
	.cfi_restore_state
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	pxor	%xmm0, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L443:
	.cfi_restore_state
	subsd	.LC6(%rip), %xmm1
	pxor	%xmm3, %xmm3
	movapd	%xmm1, %xmm2
	movapd	%xmm1, %xmm0
	mulsd	%xmm1, %xmm2
	addsd	%xmm1, %xmm0
	addsd	%xmm2, %xmm0
	ucomisd	%xmm0, %xmm3
	movapd	%xmm0, %xmm2
	sqrtsd	%xmm2, %xmm2
	ja	.L454
.L445:
	leave
	.cfi_remember_state
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	addsd	%xmm1, %xmm2
	movapd	%xmm2, %xmm0
	jmp	_ZN2v84base7ieee7545log1pEd
.L453:
	.cfi_restore_state
	movsd	%xmm1, -24(%rbp)
	movsd	%xmm4, -16(%rbp)
	movsd	%xmm3, -8(%rbp)
	call	sqrt@PLT
	movsd	-24(%rbp), %xmm1
	movsd	-16(%rbp), %xmm4
	movq	.LC6(%rip), %rax
	movsd	-8(%rbp), %xmm3
	movq	%rax, %xmm2
	jmp	.L444
.L454:
	movsd	%xmm1, -16(%rbp)
	movsd	%xmm2, -8(%rbp)
	call	sqrt@PLT
	movsd	-16(%rbp), %xmm1
	movsd	-8(%rbp), %xmm2
	jmp	.L445
	.cfi_endproc
.LFE3002:
	.size	_ZN2v84base7ieee7545acoshEd, .-_ZN2v84base7ieee7545acoshEd
	.section	.text._ZN2v84base7ieee7545asinhEd,"ax",@progbits
	.p2align 4
	.globl	_ZN2v84base7ieee7545asinhEd
	.type	_ZN2v84base7ieee7545asinhEd, @function
_ZN2v84base7ieee7545asinhEd:
.LFB3004:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%xmm0, %rbx
	shrq	$32, %rbx
	movl	%ebx, %eax
	andl	$2147483647, %eax
	subq	$40, %rsp
	cmpl	$2146435071, %eax
	jg	.L466
	cmpl	$1043333119, %eax
	jle	.L467
	movq	.LC5(%rip), %xmm1
	movapd	%xmm0, %xmm4
	andpd	%xmm1, %xmm4
	cmpl	$1102053376, %eax
	jle	.L460
	movapd	%xmm4, %xmm0
	call	_ZN2v84base7ieee7543logEd
	movsd	.LC101(%rip), %xmm1
	addsd	%xmm0, %xmm1
.L461:
	testl	%ebx, %ebx
	jg	.L455
	xorpd	.LC15(%rip), %xmm1
.L455:
	addq	$40, %rsp
	movapd	%xmm1, %xmm0
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L467:
	.cfi_restore_state
	movsd	.LC39(%rip), %xmm3
	movsd	.LC6(%rip), %xmm2
	movapd	%xmm0, %xmm1
	addsd	%xmm0, %xmm3
	comisd	%xmm2, %xmm3
	ja	.L455
	movapd	%xmm0, %xmm3
	movq	.LC5(%rip), %xmm1
	mulsd	%xmm0, %xmm3
.L459:
	andpd	%xmm0, %xmm1
	movapd	%xmm3, %xmm0
	pxor	%xmm5, %xmm5
	addsd	%xmm2, %xmm0
	ucomisd	%xmm0, %xmm5
	movapd	%xmm0, %xmm4
	sqrtsd	%xmm4, %xmm4
	ja	.L468
.L463:
	addsd	%xmm4, %xmm2
	divsd	%xmm2, %xmm3
	movapd	%xmm3, %xmm0
	addsd	%xmm1, %xmm0
	call	_ZN2v84base7ieee7545log1pEd
	movapd	%xmm0, %xmm1
	jmp	.L461
	.p2align 4,,10
	.p2align 3
.L466:
	movapd	%xmm0, %xmm1
	addq	$40, %rsp
	addsd	%xmm0, %xmm1
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movapd	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L460:
	.cfi_restore_state
	movapd	%xmm0, %xmm3
	movsd	.LC6(%rip), %xmm2
	mulsd	%xmm0, %xmm3
	movapd	%xmm3, %xmm5
	addsd	%xmm2, %xmm5
	cmpl	$1073741824, %eax
	jle	.L459
	pxor	%xmm0, %xmm0
	movapd	%xmm4, %xmm3
	movapd	%xmm5, %xmm1
	ucomisd	%xmm5, %xmm0
	sqrtsd	%xmm1, %xmm1
	addsd	%xmm4, %xmm3
	ja	.L469
.L462:
	addsd	%xmm1, %xmm4
	divsd	%xmm4, %xmm2
	movapd	%xmm2, %xmm0
	addsd	%xmm3, %xmm0
	call	_ZN2v84base7ieee7543logEd
	movapd	%xmm0, %xmm1
	jmp	.L461
.L468:
	movsd	%xmm3, -48(%rbp)
	movsd	%xmm2, -40(%rbp)
	movsd	%xmm4, -32(%rbp)
	movsd	%xmm1, -24(%rbp)
	call	sqrt@PLT
	movsd	-48(%rbp), %xmm3
	movsd	-40(%rbp), %xmm2
	movsd	-32(%rbp), %xmm4
	movsd	-24(%rbp), %xmm1
	jmp	.L463
.L469:
	movapd	%xmm5, %xmm0
	movsd	%xmm2, -48(%rbp)
	movsd	%xmm4, -40(%rbp)
	movsd	%xmm1, -32(%rbp)
	movsd	%xmm3, -24(%rbp)
	call	sqrt@PLT
	movsd	-48(%rbp), %xmm2
	movsd	-40(%rbp), %xmm4
	movsd	-32(%rbp), %xmm1
	movsd	-24(%rbp), %xmm3
	jmp	.L462
	.cfi_endproc
.LFE3004:
	.size	_ZN2v84base7ieee7545asinhEd, .-_ZN2v84base7ieee7545asinhEd
	.section	.text._ZN2v84base7ieee7545atanhEd,"ax",@progbits
	.p2align 4
	.globl	_ZN2v84base7ieee7545atanhEd
	.type	_ZN2v84base7ieee7545atanhEd, @function
_ZN2v84base7ieee7545atanhEd:
.LFB3009:
	.cfi_startproc
	endbr64
	movq	%xmm0, %rdi
	movq	%xmm0, %rdx
	shrq	$32, %rdi
	movl	%edi, %ecx
	andl	$2147483647, %ecx
	cmpl	$-2147483648, %edx
	je	.L479
	movd	%xmm0, %eax
	negl	%eax
.L471:
	orl	%edx, %eax
	movsd	.LC26(%rip), %xmm1
	shrl	$31, %eax
	orl	%ecx, %eax
	cmpl	$1072693248, %eax
	jbe	.L487
.L485:
	movapd	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L487:
	cmpl	$1072693248, %ecx
	je	.L488
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	cmpl	$1043333119, %ecx
	jle	.L489
	movq	%xmm0, %rax
	movq	%rcx, %rdx
	salq	$32, %rdx
	movl	%eax, %eax
	orq	%rdx, %rax
	movq	%rax, %xmm1
	cmpl	$1071644671, %ecx
	jle	.L478
	movsd	.LC6(%rip), %xmm2
	movq	%rax, %xmm0
	addsd	%xmm1, %xmm0
	subsd	%xmm1, %xmm2
	divsd	%xmm2, %xmm0
	call	_ZN2v84base7ieee7545log1pEd
	movsd	.LC8(%rip), %xmm1
	mulsd	%xmm0, %xmm1
.L477:
	testl	%edi, %edi
	jns	.L470
	xorpd	.LC15(%rip), %xmm1
.L470:
	movapd	%xmm1, %xmm0
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L488:
	.cfi_restore 6
	comisd	.LC0(%rip), %xmm0
	movsd	.LC102(%rip), %xmm1
	ja	.L485
	movsd	.LC90(%rip), %xmm1
	jmp	.L485
	.p2align 4,,10
	.p2align 3
.L489:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	movsd	.LC39(%rip), %xmm2
	movapd	%xmm0, %xmm1
	addsd	%xmm0, %xmm2
	comisd	.LC0(%rip), %xmm2
	ja	.L470
	movq	%xmm0, %rax
	salq	$32, %rcx
	movl	%eax, %eax
	orq	%rcx, %rax
	movq	%rax, %xmm1
.L478:
	movapd	%xmm1, %xmm3
	movsd	.LC6(%rip), %xmm2
	addsd	%xmm1, %xmm3
	subsd	%xmm1, %xmm2
	movapd	%xmm3, %xmm0
	mulsd	%xmm1, %xmm0
	divsd	%xmm2, %xmm0
	addsd	%xmm3, %xmm0
	call	_ZN2v84base7ieee7545log1pEd
	movsd	.LC8(%rip), %xmm1
	mulsd	%xmm0, %xmm1
	jmp	.L477
.L479:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	movl	$-2147483648, %eax
	jmp	.L471
	.cfi_endproc
.LFE3009:
	.size	_ZN2v84base7ieee7545atanhEd, .-_ZN2v84base7ieee7545atanhEd
	.section	.text._ZN2v84base7ieee7544log2Ed,"ax",@progbits
	.p2align 4
	.globl	_ZN2v84base7ieee7544log2Ed
	.type	_ZN2v84base7ieee7544log2Ed, @function
_ZN2v84base7ieee7544log2Ed:
.LFB3013:
	.cfi_startproc
	endbr64
	movq	%xmm0, %rdx
	movq	%xmm0, %rcx
	shrq	$32, %rdx
	movl	%edx, %eax
	cmpl	$1048575, %edx
	jg	.L495
	andl	$2147483647, %eax
	movsd	.LC90(%rip), %xmm1
	orl	%ecx, %eax
	jne	.L504
.L490:
	movapd	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L495:
	xorl	%edx, %edx
	cmpl	$2146435071, %eax
	jg	.L505
.L493:
	cmpl	$1072693248, %eax
	jne	.L499
	pxor	%xmm1, %xmm1
	testl	%ecx, %ecx
	je	.L490
.L499:
	movl	%eax, %ecx
	andl	$1048575, %eax
	movsd	.LC47(%rip), %xmm1
	pxor	%xmm2, %xmm2
	sarl	$20, %ecx
	movsd	.LC8(%rip), %xmm5
	leal	-1023(%rdx,%rcx), %edi
	leal	614244(%rax), %ecx
	movq	%xmm0, %rdx
	andl	$1048576, %ecx
	movl	%ecx, %esi
	sarl	$20, %ecx
	xorl	$1072693248, %esi
	addl	%edi, %ecx
	orl	%esi, %eax
	cvtsi2sdl	%ecx, %xmm2
	movq	%rax, %rsi
	movl	%edx, %eax
	salq	$32, %rsi
	orq	%rsi, %rax
	movq	%rax, %xmm0
	subsd	.LC6(%rip), %xmm0
	movabsq	$-4294967296, %rax
	addsd	%xmm0, %xmm1
	movapd	%xmm0, %xmm6
	movapd	%xmm0, %xmm3
	mulsd	%xmm0, %xmm5
	divsd	%xmm1, %xmm6
	movsd	.LC93(%rip), %xmm1
	mulsd	%xmm0, %xmm5
	subsd	%xmm5, %xmm3
	movq	%xmm3, %rdi
	andq	%rax, %rdi
	movq	%rdi, %xmm3
	subsd	%xmm3, %xmm0
	subsd	%xmm5, %xmm0
	movapd	%xmm6, %xmm4
	mulsd	%xmm6, %xmm4
	movapd	%xmm4, %xmm7
	mulsd	%xmm4, %xmm7
	mulsd	%xmm7, %xmm1
	addsd	.LC94(%rip), %xmm1
	mulsd	%xmm7, %xmm1
	addsd	.LC95(%rip), %xmm1
	mulsd	%xmm7, %xmm1
	addsd	.LC96(%rip), %xmm1
	mulsd	%xmm4, %xmm1
	movsd	.LC97(%rip), %xmm4
	mulsd	%xmm7, %xmm4
	addsd	.LC98(%rip), %xmm4
	mulsd	%xmm7, %xmm4
	addsd	.LC99(%rip), %xmm4
	mulsd	%xmm7, %xmm4
	addsd	%xmm4, %xmm1
	movq	%rdi, %xmm4
	addsd	%xmm5, %xmm1
	movsd	.LC103(%rip), %xmm5
	mulsd	%xmm5, %xmm4
	mulsd	%xmm6, %xmm1
	addsd	%xmm0, %xmm1
	movsd	.LC104(%rip), %xmm0
	addsd	%xmm1, %xmm3
	mulsd	%xmm5, %xmm1
	mulsd	%xmm3, %xmm0
	movapd	%xmm2, %xmm3
	addsd	%xmm4, %xmm3
	subsd	%xmm3, %xmm2
	addsd	%xmm1, %xmm0
	movapd	%xmm2, %xmm1
	addsd	%xmm4, %xmm1
	addsd	%xmm0, %xmm1
	addsd	%xmm3, %xmm1
	movapd	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L504:
	movsd	.LC26(%rip), %xmm1
	testl	%edx, %edx
	js	.L490
	mulsd	.LC91(%rip), %xmm0
	movl	$-54, %edx
	movq	%xmm0, %rax
	shrq	$32, %rax
	cmpl	$2146435071, %eax
	jle	.L493
.L505:
	movapd	%xmm0, %xmm1
	addsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	ret
	.cfi_endproc
.LFE3013:
	.size	_ZN2v84base7ieee7544log2Ed, .-_ZN2v84base7ieee7544log2Ed
	.section	.text._ZN2v84base7ieee7545log10Ed,"ax",@progbits
	.p2align 4
	.globl	_ZN2v84base7ieee7545log10Ed
	.type	_ZN2v84base7ieee7545log10Ed, @function
_ZN2v84base7ieee7545log10Ed:
.LFB3014:
	.cfi_startproc
	endbr64
	movq	%xmm0, %rax
	shrq	$32, %rax
	movl	%eax, %ecx
	cmpl	$1048575, %eax
	jg	.L511
	movq	%xmm0, %rdx
	andl	$2147483647, %ecx
	movsd	.LC90(%rip), %xmm1
	orl	%edx, %ecx
	jne	.L523
.L521:
	movapd	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L511:
	movd	%xmm0, %esi
	xorl	%eax, %eax
	cmpl	$2146435071, %ecx
	jg	.L524
.L509:
	cmpl	$1072693248, %ecx
	jne	.L515
	pxor	%xmm1, %xmm1
	testl	%esi, %esi
	je	.L521
.L515:
	movl	%ecx, %edx
	andl	$1048575, %ecx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %esi
	sarl	$20, %edx
	pxor	%xmm8, %xmm8
	leal	-1023(%rax,%rdx), %edx
	movl	$1023, %eax
	movl	%edx, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	shrl	$31, %edi
	subl	%edi, %eax
	addl	%edi, %edx
	sall	$20, %eax
	cvtsi2sdl	%edx, %xmm8
	orl	%ecx, %eax
	salq	$32, %rax
	orq	%rsi, %rax
	movq	%rax, %xmm0
	call	_ZN2v84base7ieee7543logEd
	movsd	.LC106(%rip), %xmm2
	movsd	.LC107(%rip), %xmm1
	popq	%rbp
	.cfi_def_cfa 7, 8
	mulsd	%xmm0, %xmm1
	mulsd	%xmm8, %xmm2
	addsd	%xmm1, %xmm2
	movsd	.LC108(%rip), %xmm1
	mulsd	%xmm8, %xmm1
	addsd	%xmm2, %xmm1
	movapd	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L523:
	.cfi_restore 6
	movsd	.LC105(%rip), %xmm1
	testl	%eax, %eax
	js	.L521
	mulsd	.LC91(%rip), %xmm0
	movq	%xmm0, %rax
	movq	%xmm0, %rsi
	shrq	$32, %rax
	movq	%rax, %rcx
	movl	$-54, %eax
	cmpl	$2146435071, %ecx
	jle	.L509
.L524:
	movapd	%xmm0, %xmm1
	addsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	ret
	.cfi_endproc
.LFE3014:
	.size	_ZN2v84base7ieee7545log10Ed, .-_ZN2v84base7ieee7545log10Ed
	.section	.text._ZN2v84base7ieee7545expm1Ed,"ax",@progbits
	.p2align 4
	.globl	_ZN2v84base7ieee7545expm1Ed
	.type	_ZN2v84base7ieee7545expm1Ed, @function
_ZN2v84base7ieee7545expm1Ed:
.LFB3015:
	.cfi_startproc
	endbr64
	movq	%xmm0, %rdx
	movq	%xmm0, %rcx
	shrq	$32, %rdx
	movl	%edx, %eax
	andl	$2147483647, %eax
	cmpl	$1078159481, %eax
	jbe	.L526
	cmpl	$1082535489, %eax
	jbe	.L527
	cmpl	$2146435071, %eax
	jbe	.L528
	movl	%edx, %eax
	andl	$1048575, %eax
	orl	%ecx, %eax
	je	.L529
	addsd	%xmm0, %xmm0
	movapd	%xmm0, %xmm1
	jmp	.L525
	.p2align 4,,10
	.p2align 3
.L526:
	cmpl	$1071001154, %eax
	ja	.L559
	cmpl	$1016070143, %eax
	jbe	.L560
	movsd	.LC8(%rip), %xmm4
	xorl	%ecx, %ecx
.L539:
	movapd	%xmm0, %xmm6
	movapd	%xmm0, %xmm2
	movsd	.LC112(%rip), %xmm1
	movsd	.LC6(%rip), %xmm5
	mulsd	%xmm4, %xmm6
	mulsd	%xmm6, %xmm2
	movapd	%xmm6, %xmm7
	movsd	.LC117(%rip), %xmm6
	mulsd	%xmm2, %xmm1
	addsd	.LC113(%rip), %xmm1
	mulsd	%xmm2, %xmm1
	subsd	.LC114(%rip), %xmm1
	mulsd	%xmm2, %xmm1
	addsd	.LC115(%rip), %xmm1
	mulsd	%xmm2, %xmm1
	subsd	.LC116(%rip), %xmm1
	mulsd	%xmm2, %xmm1
	addsd	%xmm5, %xmm1
	mulsd	%xmm1, %xmm7
	subsd	%xmm7, %xmm6
	movsd	.LC118(%rip), %xmm7
	subsd	%xmm6, %xmm1
	mulsd	%xmm0, %xmm6
	subsd	%xmm6, %xmm7
	divsd	%xmm7, %xmm1
	mulsd	%xmm2, %xmm1
	testl	%ecx, %ecx
	je	.L561
	subsd	%xmm3, %xmm1
	movl	%ecx, %eax
	sall	$20, %eax
	addl	$1072693248, %eax
	mulsd	%xmm0, %xmm1
	salq	$32, %rax
	subsd	%xmm3, %xmm1
	subsd	%xmm2, %xmm1
	cmpl	$-1, %ecx
	je	.L562
	cmpl	$1, %ecx
	je	.L563
	leal	1(%rcx), %edx
	cmpl	$57, %edx
	ja	.L564
	cmpl	$19, %ecx
	jg	.L548
	movl	$2097152, %edx
	subsd	%xmm0, %xmm1
	sarl	%cl, %edx
	movl	%edx, %ecx
	movl	$1072693248, %edx
	subl	%ecx, %edx
	salq	$32, %rdx
	movq	%rdx, %xmm5
	subsd	%xmm1, %xmm5
	movapd	%xmm5, %xmm1
	movq	%rax, %xmm5
	mulsd	%xmm1, %xmm5
	movapd	%xmm5, %xmm1
.L525:
	movapd	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L527:
	testl	%edx, %edx
	js	.L565
	movsd	.LC82(%rip), %xmm2
	movsd	.LC8(%rip), %xmm4
	mulsd	%xmm0, %xmm2
	movapd	%xmm4, %xmm1
.L538:
	addsd	%xmm2, %xmm1
	movsd	.LC83(%rip), %xmm2
	movapd	%xmm0, %xmm3
	cvttsd2sil	%xmm1, %ecx
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	mulsd	%xmm1, %xmm2
	mulsd	.LC84(%rip), %xmm1
	subsd	%xmm2, %xmm3
.L537:
	movapd	%xmm3, %xmm0
	subsd	%xmm1, %xmm0
	subsd	%xmm0, %xmm3
	subsd	%xmm1, %xmm3
	jmp	.L539
	.p2align 4,,10
	.p2align 3
.L560:
	movsd	_ZZN2v84base7ieee7545expm1EdE4huge(%rip), %xmm1
	movsd	_ZZN2v84base7ieee7545expm1EdE4huge(%rip), %xmm2
	addsd	%xmm0, %xmm1
	addsd	%xmm0, %xmm2
	subsd	%xmm2, %xmm1
	subsd	%xmm1, %xmm0
.L558:
	movapd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L565:
	movsd	.LC111(%rip), %xmm1
	pxor	%xmm2, %xmm2
	addsd	%xmm0, %xmm1
	comisd	%xmm1, %xmm2
	ja	.L566
	movsd	.LC82(%rip), %xmm2
	mulsd	%xmm0, %xmm2
.L557:
	movsd	.LC109(%rip), %xmm1
	movsd	.LC8(%rip), %xmm4
	jmp	.L538
	.p2align 4,,10
	.p2align 3
.L528:
	comisd	.LC80(%rip), %xmm0
	jbe	.L527
	movsd	_ZZN2v84base7ieee7545expm1EdE4huge(%rip), %xmm1
	movsd	_ZZN2v84base7ieee7545expm1EdE4huge(%rip), %xmm0
	mulsd	%xmm0, %xmm1
	jmp	.L525
	.p2align 4,,10
	.p2align 3
.L559:
	cmpl	$1072734897, %eax
	ja	.L535
	movapd	%xmm0, %xmm3
	testl	%edx, %edx
	js	.L536
	subsd	.LC83(%rip), %xmm3
	movsd	.LC84(%rip), %xmm1
	movl	$1, %ecx
	movsd	.LC8(%rip), %xmm4
	jmp	.L537
	.p2align 4,,10
	.p2align 3
.L561:
	mulsd	%xmm0, %xmm1
	subsd	%xmm2, %xmm1
	subsd	%xmm1, %xmm0
	movapd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L563:
	movsd	.LC119(%rip), %xmm2
	comisd	%xmm0, %xmm2
	jbe	.L556
	addsd	%xmm4, %xmm0
	subsd	%xmm0, %xmm1
	mulsd	.LC120(%rip), %xmm1
	jmp	.L525
	.p2align 4,,10
	.p2align 3
.L562:
	subsd	%xmm1, %xmm0
	movapd	%xmm0, %xmm1
	mulsd	%xmm4, %xmm1
	subsd	%xmm4, %xmm1
	jmp	.L525
	.p2align 4,,10
	.p2align 3
.L536:
	addsd	.LC83(%rip), %xmm3
	movsd	.LC110(%rip), %xmm1
	movl	$-1, %ecx
	movsd	.LC8(%rip), %xmm4
	jmp	.L537
	.p2align 4,,10
	.p2align 3
.L564:
	subsd	%xmm0, %xmm1
	movapd	%xmm5, %xmm4
	subsd	%xmm1, %xmm4
	movapd	%xmm4, %xmm1
	cmpl	$1024, %ecx
	je	.L567
	movq	%rax, %xmm4
	mulsd	%xmm1, %xmm4
	movapd	%xmm4, %xmm1
.L547:
	subsd	%xmm5, %xmm1
	jmp	.L525
	.p2align 4,,10
	.p2align 3
.L548:
	movl	$1023, %edx
	subl	%ecx, %edx
	sall	$20, %edx
	salq	$32, %rdx
	movq	%rdx, %xmm4
	addsd	%xmm4, %xmm1
	movq	%rax, %xmm4
	subsd	%xmm1, %xmm0
	movapd	%xmm0, %xmm1
	addsd	%xmm5, %xmm1
	mulsd	%xmm1, %xmm4
	movapd	%xmm4, %xmm1
	jmp	.L525
	.p2align 4,,10
	.p2align 3
.L529:
	movsd	.LC49(%rip), %xmm1
	testl	%edx, %edx
	jns	.L558
	jmp	.L525
	.p2align 4,,10
	.p2align 3
.L556:
	subsd	%xmm1, %xmm0
	movapd	%xmm0, %xmm1
	addsd	%xmm0, %xmm1
	addsd	%xmm5, %xmm1
	jmp	.L525
	.p2align 4,,10
	.p2align 3
.L567:
	addsd	%xmm4, %xmm1
	mulsd	.LC121(%rip), %xmm1
	jmp	.L547
	.p2align 4,,10
	.p2align 3
.L566:
	movsd	.LC49(%rip), %xmm1
	jmp	.L525
.L535:
	movsd	.LC82(%rip), %xmm2
	mulsd	%xmm0, %xmm2
	testl	%edx, %edx
	js	.L557
	movsd	.LC8(%rip), %xmm4
	movapd	%xmm4, %xmm1
	jmp	.L538
	.cfi_endproc
.LFE3015:
	.size	_ZN2v84base7ieee7545expm1Ed, .-_ZN2v84base7ieee7545expm1Ed
	.section	.text._ZN2v84base7ieee7544cbrtEd,"ax",@progbits
	.p2align 4
	.globl	_ZN2v84base7ieee7544cbrtEd
	.type	_ZN2v84base7ieee7544cbrtEd, @function
_ZN2v84base7ieee7544cbrtEd:
.LFB3016:
	.cfi_startproc
	endbr64
	movq	%xmm0, %rdx
	movapd	%xmm0, %xmm3
	movq	%xmm0, %rsi
	shrq	$32, %rdx
	movl	%edx, %eax
	andl	$2147483647, %eax
	cmpl	$2146435071, %eax
	ja	.L575
	movl	%edx, %ecx
	andl	$-2147483648, %ecx
	testl	$2146435072, %edx
	jne	.L571
	orl	%esi, %eax
	je	.L568
	movabsq	$4850376798678024192, %rdx
	movl	$2863311531, %eax
	movq	%rdx, %xmm5
	mulsd	%xmm0, %xmm5
	movq	%xmm5, %rdx
	shrq	$32, %rdx
	andl	$2147483647, %edx
	imulq	%rdx, %rax
	shrq	$33, %rax
	leal	696219795(%rax), %eax
	orl	%ecx, %eax
	salq	$32, %rax
	movq	%rax, %xmm1
	jmp	.L572
	.p2align 4,,10
	.p2align 3
.L571:
	movl	$2863311531, %edx
	imulq	%rdx, %rax
	shrq	$33, %rax
	addl	$715094163, %eax
	orl	%ecx, %eax
	salq	$32, %rax
	movq	%rax, %xmm1
.L572:
	movapd	%xmm1, %xmm2
	movapd	%xmm1, %xmm0
	movl	$2147483648, %eax
	movsd	.LC122(%rip), %xmm4
	divsd	%xmm3, %xmm2
	mulsd	%xmm1, %xmm0
	mulsd	%xmm2, %xmm0
	mulsd	%xmm0, %xmm4
	movapd	%xmm0, %xmm2
	mulsd	%xmm0, %xmm2
	subsd	.LC123(%rip), %xmm4
	mulsd	%xmm0, %xmm2
	mulsd	%xmm4, %xmm2
	movsd	.LC124(%rip), %xmm4
	mulsd	%xmm0, %xmm4
	subsd	.LC125(%rip), %xmm4
	mulsd	%xmm4, %xmm0
	addsd	.LC126(%rip), %xmm0
	addsd	%xmm0, %xmm2
	mulsd	%xmm2, %xmm1
	movq	%xmm1, %rdi
	addq	%rax, %rdi
	movq	%rdi, %rax
	andq	$-1073741824, %rax
	movq	%rax, %xmm0
	movq	%rax, %xmm1
	movq	%rax, %xmm2
	mulsd	%xmm0, %xmm1
	addsd	%xmm0, %xmm2
	divsd	%xmm1, %xmm3
	movapd	%xmm3, %xmm1
	addsd	%xmm2, %xmm3
	subsd	%xmm0, %xmm1
	divsd	%xmm3, %xmm1
	mulsd	%xmm0, %xmm1
	addsd	%xmm1, %xmm0
.L568:
	ret
	.p2align 4,,10
	.p2align 3
.L575:
	addsd	%xmm0, %xmm3
	movapd	%xmm3, %xmm0
	ret
	.cfi_endproc
.LFE3016:
	.size	_ZN2v84base7ieee7544cbrtEd, .-_ZN2v84base7ieee7544cbrtEd
	.section	.text._ZN2v84base7ieee7543sinEd,"ax",@progbits
	.p2align 4
	.globl	_ZN2v84base7ieee7543sinEd
	.type	_ZN2v84base7ieee7543sinEd, @function
_ZN2v84base7ieee7543sinEd:
.LFB3017:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movapd	%xmm0, %xmm1
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	%xmm0, %rax
	shrq	$32, %rax
	andl	$2147483647, %eax
	cmpl	$1072243195, %eax
	jg	.L577
	cmpl	$1044381695, %eax
	jg	.L578
	cvttsd2sil	%xmm0, %eax
	testl	%eax, %eax
	je	.L576
.L578:
	movapd	%xmm1, %xmm2
	movsd	.LC73(%rip), %xmm0
	mulsd	%xmm1, %xmm2
	mulsd	%xmm2, %xmm0
	subsd	.LC74(%rip), %xmm0
	mulsd	%xmm2, %xmm0
	addsd	.LC75(%rip), %xmm0
	mulsd	%xmm2, %xmm0
	subsd	.LC76(%rip), %xmm0
	mulsd	%xmm2, %xmm0
	addsd	.LC77(%rip), %xmm0
	mulsd	%xmm2, %xmm0
	mulsd	%xmm1, %xmm2
	subsd	.LC78(%rip), %xmm0
	mulsd	%xmm2, %xmm0
	addsd	%xmm1, %xmm0
	jmp	.L576
	.p2align 4,,10
	.p2align 3
.L577:
	cmpl	$2146435071, %eax
	jle	.L580
	subsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
.L576:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L616
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L580:
	.cfi_restore_state
	leaq	-32(%rbp), %rdi
	call	_ZN2v84base7ieee75412_GLOBAL__N_118__ieee754_rem_pio2EdPd
	andl	$3, %eax
	cmpl	$1, %eax
	je	.L581
	cmpl	$2, %eax
	je	.L582
	testl	%eax, %eax
	je	.L617
	movsd	-32(%rbp), %xmm2
	movsd	-24(%rbp), %xmm3
	movq	%xmm2, %rax
	shrq	$32, %rax
	andl	$2147483647, %eax
	cmpl	$1044381695, %eax
	jle	.L618
	movapd	%xmm2, %xmm4
	movsd	.LC67(%rip), %xmm1
	mulsd	%xmm2, %xmm4
	mulsd	%xmm4, %xmm1
	addsd	.LC68(%rip), %xmm1
	mulsd	%xmm4, %xmm1
	subsd	.LC69(%rip), %xmm1
	mulsd	%xmm4, %xmm1
	addsd	.LC70(%rip), %xmm1
	mulsd	%xmm4, %xmm1
	subsd	.LC71(%rip), %xmm1
	mulsd	%xmm4, %xmm1
	addsd	.LC72(%rip), %xmm1
	mulsd	%xmm4, %xmm1
	cmpl	$1070805810, %eax
	jle	.L596
	cmpl	$1072234496, %eax
	jg	.L603
	subl	$2097152, %eax
	movsd	.LC6(%rip), %xmm0
	salq	$32, %rax
	movq	%rax, %xmm6
	subsd	%xmm6, %xmm0
.L595:
	movsd	.LC8(%rip), %xmm5
	mulsd	%xmm4, %xmm1
	mulsd	%xmm3, %xmm2
	mulsd	%xmm4, %xmm5
	subsd	%xmm2, %xmm1
	subsd	%xmm6, %xmm5
	subsd	%xmm1, %xmm5
	subsd	%xmm5, %xmm0
	xorpd	.LC15(%rip), %xmm0
	jmp	.L576
	.p2align 4,,10
	.p2align 3
.L582:
	movsd	-32(%rbp), %xmm0
	movsd	-24(%rbp), %xmm5
	movq	%xmm0, %rax
	shrq	$32, %rax
	andl	$2147483647, %eax
	cmpl	$1044381695, %eax
	jle	.L619
.L589:
	movapd	%xmm0, %xmm3
	movsd	.LC73(%rip), %xmm1
	movapd	%xmm0, %xmm4
	movsd	.LC8(%rip), %xmm2
	mulsd	%xmm0, %xmm3
	mulsd	%xmm5, %xmm2
	mulsd	%xmm3, %xmm1
	subsd	.LC74(%rip), %xmm1
	mulsd	%xmm3, %xmm4
	mulsd	%xmm3, %xmm1
	addsd	.LC75(%rip), %xmm1
	mulsd	%xmm3, %xmm1
	subsd	.LC76(%rip), %xmm1
	mulsd	%xmm3, %xmm1
	addsd	.LC77(%rip), %xmm1
	mulsd	%xmm4, %xmm1
	mulsd	.LC78(%rip), %xmm4
	subsd	%xmm1, %xmm2
	movapd	%xmm2, %xmm1
	mulsd	%xmm3, %xmm1
	subsd	%xmm5, %xmm1
	addsd	%xmm4, %xmm1
	subsd	%xmm1, %xmm0
.L590:
	xorpd	.LC15(%rip), %xmm0
	jmp	.L576
	.p2align 4,,10
	.p2align 3
.L617:
	movsd	-32(%rbp), %xmm0
	movsd	-24(%rbp), %xmm5
	movq	%xmm0, %rax
	shrq	$32, %rax
	andl	$2147483647, %eax
	cmpl	$1044381695, %eax
	jg	.L584
	cvttsd2sil	%xmm0, %eax
	testl	%eax, %eax
	je	.L576
.L584:
	movapd	%xmm0, %xmm3
	movsd	.LC73(%rip), %xmm1
	movapd	%xmm0, %xmm4
	movsd	.LC8(%rip), %xmm2
	mulsd	%xmm0, %xmm3
	mulsd	%xmm5, %xmm2
	mulsd	%xmm3, %xmm1
	subsd	.LC74(%rip), %xmm1
	mulsd	%xmm3, %xmm4
	mulsd	%xmm3, %xmm1
	addsd	.LC75(%rip), %xmm1
	mulsd	%xmm3, %xmm1
	subsd	.LC76(%rip), %xmm1
	mulsd	%xmm3, %xmm1
	addsd	.LC77(%rip), %xmm1
	mulsd	%xmm4, %xmm1
	mulsd	.LC78(%rip), %xmm4
	subsd	%xmm1, %xmm2
	movapd	%xmm2, %xmm1
	mulsd	%xmm3, %xmm1
	subsd	%xmm5, %xmm1
	addsd	%xmm4, %xmm1
	subsd	%xmm1, %xmm0
	jmp	.L576
	.p2align 4,,10
	.p2align 3
.L581:
	movsd	-32(%rbp), %xmm2
	movsd	-24(%rbp), %xmm3
	movq	%xmm2, %rax
	shrq	$32, %rax
	andl	$2147483647, %eax
	cmpl	$1044381695, %eax
	jle	.L620
	movapd	%xmm2, %xmm4
	movsd	.LC67(%rip), %xmm1
	mulsd	%xmm2, %xmm4
	mulsd	%xmm4, %xmm1
	addsd	.LC68(%rip), %xmm1
	mulsd	%xmm4, %xmm1
	subsd	.LC69(%rip), %xmm1
	mulsd	%xmm4, %xmm1
	addsd	.LC70(%rip), %xmm1
	mulsd	%xmm4, %xmm1
	subsd	.LC71(%rip), %xmm1
	mulsd	%xmm4, %xmm1
	addsd	.LC72(%rip), %xmm1
	mulsd	%xmm4, %xmm1
	cmpl	$1070805810, %eax
	jle	.L597
	cmpl	$1072234496, %eax
	jg	.L601
	subl	$2097152, %eax
	movsd	.LC6(%rip), %xmm0
	salq	$32, %rax
	movq	%rax, %xmm6
	subsd	%xmm6, %xmm0
.L588:
	movsd	.LC8(%rip), %xmm5
	mulsd	%xmm4, %xmm1
	mulsd	%xmm3, %xmm2
	mulsd	%xmm4, %xmm5
	subsd	%xmm2, %xmm1
	subsd	%xmm6, %xmm5
	subsd	%xmm1, %xmm5
	subsd	%xmm5, %xmm0
	jmp	.L576
	.p2align 4,,10
	.p2align 3
.L620:
	cvttsd2sil	%xmm2, %eax
	movsd	.LC6(%rip), %xmm0
	testl	%eax, %eax
	je	.L576
	movapd	%xmm2, %xmm4
	movsd	.LC67(%rip), %xmm0
	mulsd	%xmm2, %xmm4
	mulsd	%xmm4, %xmm0
	addsd	.LC68(%rip), %xmm0
	mulsd	%xmm4, %xmm0
	subsd	.LC69(%rip), %xmm0
	mulsd	%xmm4, %xmm0
	addsd	.LC70(%rip), %xmm0
	mulsd	%xmm4, %xmm0
	subsd	.LC71(%rip), %xmm0
	mulsd	%xmm4, %xmm0
	addsd	.LC72(%rip), %xmm0
	movapd	%xmm0, %xmm1
	mulsd	%xmm4, %xmm1
.L597:
	mulsd	%xmm4, %xmm1
	movsd	.LC8(%rip), %xmm0
	mulsd	%xmm3, %xmm2
	mulsd	%xmm4, %xmm0
	subsd	%xmm2, %xmm1
	subsd	%xmm1, %xmm0
	movapd	%xmm0, %xmm1
	movsd	.LC6(%rip), %xmm0
	subsd	%xmm1, %xmm0
	jmp	.L576
	.p2align 4,,10
	.p2align 3
.L619:
	cvttsd2sil	%xmm0, %eax
	testl	%eax, %eax
	jne	.L589
	jmp	.L590
	.p2align 4,,10
	.p2align 3
.L618:
	cvttsd2sil	%xmm2, %eax
	movsd	.LC49(%rip), %xmm0
	testl	%eax, %eax
	je	.L576
	movapd	%xmm2, %xmm4
	movsd	.LC67(%rip), %xmm0
	mulsd	%xmm2, %xmm4
	mulsd	%xmm4, %xmm0
	addsd	.LC68(%rip), %xmm0
	mulsd	%xmm4, %xmm0
	subsd	.LC69(%rip), %xmm0
	mulsd	%xmm4, %xmm0
	addsd	.LC70(%rip), %xmm0
	mulsd	%xmm4, %xmm0
	subsd	.LC71(%rip), %xmm0
	mulsd	%xmm4, %xmm0
	addsd	.LC72(%rip), %xmm0
	movapd	%xmm0, %xmm1
	mulsd	%xmm4, %xmm1
.L596:
	mulsd	%xmm4, %xmm1
	movsd	.LC8(%rip), %xmm0
	mulsd	%xmm3, %xmm2
	mulsd	%xmm4, %xmm0
	subsd	%xmm2, %xmm1
	subsd	%xmm1, %xmm0
	movapd	%xmm0, %xmm1
	movsd	.LC6(%rip), %xmm0
	subsd	%xmm1, %xmm0
	xorpd	.LC15(%rip), %xmm0
	jmp	.L576
	.p2align 4,,10
	.p2align 3
.L603:
	movsd	.LC65(%rip), %xmm0
	movsd	.LC66(%rip), %xmm6
	jmp	.L595
	.p2align 4,,10
	.p2align 3
.L601:
	movsd	.LC65(%rip), %xmm0
	movsd	.LC66(%rip), %xmm6
	jmp	.L588
.L616:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3017:
	.size	_ZN2v84base7ieee7543sinEd, .-_ZN2v84base7ieee7543sinEd
	.section	.text._ZN2v84base7ieee7543tanEd,"ax",@progbits
	.p2align 4
	.globl	_ZN2v84base7ieee7543tanEd
	.type	_ZN2v84base7ieee7543tanEd, @function
_ZN2v84base7ieee7543tanEd:
.LFB3018:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	%xmm0, %rax
	shrq	$32, %rax
	movl	%eax, %edx
	andl	$2147483647, %edx
	cmpl	$1072243195, %edx
	jg	.L622
	cmpl	$1043333119, %edx
	jg	.L623
	cvttsd2sil	%xmm0, %ecx
	movapd	%xmm0, %xmm4
	testl	%ecx, %ecx
	je	.L621
.L625:
	movapd	%xmm0, %xmm3
	movsd	.LC127(%rip), %xmm1
	movsd	.LC133(%rip), %xmm4
	movapd	%xmm0, %xmm2
	mulsd	%xmm0, %xmm3
	mulsd	%xmm3, %xmm2
	movapd	%xmm3, %xmm5
	mulsd	%xmm3, %xmm5
	mulsd	%xmm5, %xmm1
	addsd	.LC128(%rip), %xmm1
	mulsd	%xmm5, %xmm4
	addsd	.LC134(%rip), %xmm4
	mulsd	%xmm5, %xmm1
	addsd	.LC129(%rip), %xmm1
	mulsd	%xmm5, %xmm4
	addsd	.LC135(%rip), %xmm4
	mulsd	%xmm5, %xmm1
	addsd	.LC130(%rip), %xmm1
	mulsd	%xmm5, %xmm4
	addsd	.LC136(%rip), %xmm4
	mulsd	%xmm5, %xmm1
	addsd	.LC131(%rip), %xmm1
	mulsd	%xmm5, %xmm4
	addsd	.LC137(%rip), %xmm4
	mulsd	%xmm5, %xmm1
	addsd	.LC132(%rip), %xmm1
	mulsd	%xmm5, %xmm4
	addsd	.LC138(%rip), %xmm4
	mulsd	%xmm3, %xmm1
	addsd	%xmm4, %xmm1
	pxor	%xmm4, %xmm4
	mulsd	%xmm2, %xmm1
	mulsd	.LC139(%rip), %xmm2
	addsd	%xmm4, %xmm1
	mulsd	%xmm3, %xmm1
	addsd	%xmm4, %xmm1
	movapd	%xmm0, %xmm4
	addsd	%xmm2, %xmm1
	addsd	%xmm1, %xmm4
	cmpl	$1072010279, %edx
	jle	.L621
	movapd	%xmm4, %xmm2
	movsd	.LC6(%rip), %xmm3
	sarl	$30, %eax
	movl	$1, %edx
	mulsd	%xmm4, %xmm2
	andl	$2, %eax
	addsd	%xmm3, %xmm4
	subl	%eax, %edx
	divsd	%xmm4, %xmm2
	pxor	%xmm4, %xmm4
	cvtsi2sdl	%edx, %xmm4
	subsd	%xmm1, %xmm2
	subsd	%xmm2, %xmm0
	addsd	%xmm0, %xmm0
	subsd	%xmm0, %xmm3
	mulsd	%xmm3, %xmm4
	jmp	.L621
	.p2align 4,,10
	.p2align 3
.L622:
	cmpl	$2146435071, %edx
	jle	.L627
	movapd	%xmm0, %xmm4
	subsd	%xmm0, %xmm4
.L621:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L642
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movapd	%xmm4, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L623:
	.cfi_restore_state
	cmpl	$1072010279, %edx
	jle	.L625
	testl	%eax, %eax
	jns	.L626
	xorpd	.LC15(%rip), %xmm0
.L626:
	movsd	.LC40(%rip), %xmm1
	subsd	%xmm0, %xmm1
	movsd	.LC41(%rip), %xmm0
	addsd	%xmm1, %xmm0
	jmp	.L625
	.p2align 4,,10
	.p2align 3
.L627:
	leaq	-32(%rbp), %rdi
	call	_ZN2v84base7ieee75412_GLOBAL__N_118__ieee754_rem_pio2EdPd
	movsd	-32(%rbp), %xmm1
	movl	$1, %edx
	movsd	-24(%rbp), %xmm3
	addl	%eax, %eax
	movl	%eax, %ecx
	movq	%xmm1, %rsi
	movq	%xmm1, %rdi
	andl	$2, %ecx
	shrq	$32, %rsi
	subl	%ecx, %edx
	movl	%esi, %ecx
	andl	$2147483647, %ecx
	cmpl	$1043333119, %ecx
	jle	.L643
	cmpl	$1072010279, %ecx
	jle	.L629
	testl	%esi, %esi
	jns	.L631
	movq	.LC15(%rip), %xmm0
	xorpd	%xmm0, %xmm1
	xorpd	%xmm0, %xmm3
.L631:
	movsd	.LC40(%rip), %xmm0
	subsd	%xmm1, %xmm0
	movsd	.LC41(%rip), %xmm1
	subsd	%xmm3, %xmm1
	pxor	%xmm3, %xmm3
	addsd	%xmm0, %xmm1
.L629:
	movapd	%xmm1, %xmm4
	movsd	.LC127(%rip), %xmm0
	movsd	.LC133(%rip), %xmm5
	movapd	%xmm1, %xmm2
	mulsd	%xmm1, %xmm4
	mulsd	%xmm4, %xmm2
	movapd	%xmm4, %xmm6
	mulsd	%xmm4, %xmm6
	mulsd	%xmm6, %xmm0
	addsd	.LC128(%rip), %xmm0
	mulsd	%xmm6, %xmm5
	addsd	.LC134(%rip), %xmm5
	mulsd	%xmm6, %xmm0
	addsd	.LC129(%rip), %xmm0
	mulsd	%xmm6, %xmm5
	addsd	.LC135(%rip), %xmm5
	mulsd	%xmm6, %xmm0
	addsd	.LC130(%rip), %xmm0
	mulsd	%xmm6, %xmm5
	addsd	.LC136(%rip), %xmm5
	mulsd	%xmm6, %xmm0
	addsd	.LC131(%rip), %xmm0
	mulsd	%xmm6, %xmm5
	addsd	.LC137(%rip), %xmm5
	mulsd	%xmm6, %xmm0
	addsd	.LC132(%rip), %xmm0
	mulsd	%xmm6, %xmm5
	addsd	.LC138(%rip), %xmm5
	mulsd	%xmm4, %xmm0
	addsd	%xmm5, %xmm0
	mulsd	%xmm2, %xmm0
	mulsd	.LC139(%rip), %xmm2
	addsd	%xmm3, %xmm0
	mulsd	%xmm4, %xmm0
	movapd	%xmm1, %xmm4
	addsd	%xmm3, %xmm0
	addsd	%xmm0, %xmm2
	addsd	%xmm2, %xmm4
	cmpl	$1072010279, %ecx
	jg	.L644
	cmpl	$1, %edx
	je	.L621
	movsd	.LC49(%rip), %xmm3
	movq	%xmm4, %rax
	movabsq	$-4294967296, %rdx
	andq	%rdx, %rax
	divsd	%xmm4, %xmm3
	movq	%rax, %xmm0
	movapd	%xmm0, %xmm7
	subsd	%xmm1, %xmm7
	subsd	%xmm7, %xmm2
	movq	%xmm3, %rax
	andq	%rdx, %rax
	movq	%rax, %xmm4
	mulsd	%xmm4, %xmm2
	mulsd	%xmm4, %xmm0
	addsd	.LC6(%rip), %xmm0
	addsd	%xmm2, %xmm0
	mulsd	%xmm3, %xmm0
	addsd	%xmm0, %xmm4
	jmp	.L621
	.p2align 4,,10
	.p2align 3
.L643:
	cvttsd2sil	%xmm1, %r8d
	testl	%r8d, %r8d
	jne	.L629
	notl	%eax
	orl	%edi, %ecx
	andl	$2, %eax
	orl	%ecx, %eax
	je	.L645
	movapd	%xmm1, %xmm4
	cmpl	$1, %edx
	je	.L621
	movapd	%xmm3, %xmm2
	movsd	.LC49(%rip), %xmm0
	movabsq	$-4294967296, %rdx
	addsd	%xmm1, %xmm2
	divsd	%xmm2, %xmm0
	movq	%xmm2, %rax
	andq	%rdx, %rax
	movq	%rax, %xmm1
	movapd	%xmm1, %xmm7
	subsd	%xmm4, %xmm7
	subsd	%xmm7, %xmm3
	movapd	%xmm3, %xmm4
	movq	%xmm0, %rax
	andq	%rdx, %rax
	movq	%rax, %xmm2
	mulsd	%xmm2, %xmm4
	mulsd	%xmm2, %xmm1
	addsd	.LC6(%rip), %xmm1
	addsd	%xmm1, %xmm4
	mulsd	%xmm0, %xmm4
	addsd	%xmm2, %xmm4
	jmp	.L621
	.p2align 4,,10
	.p2align 3
.L644:
	pxor	%xmm0, %xmm0
	movapd	%xmm4, %xmm3
	sarl	$30, %esi
	movl	$1, %eax
	cvtsi2sdl	%edx, %xmm0
	mulsd	%xmm4, %xmm3
	andl	$2, %esi
	subl	%esi, %eax
	addsd	%xmm0, %xmm4
	divsd	%xmm4, %xmm3
	pxor	%xmm4, %xmm4
	cvtsi2sdl	%eax, %xmm4
	subsd	%xmm2, %xmm3
	subsd	%xmm3, %xmm1
	addsd	%xmm1, %xmm1
	subsd	%xmm1, %xmm0
	mulsd	%xmm0, %xmm4
	jmp	.L621
	.p2align 4,,10
	.p2align 3
.L645:
	andpd	.LC5(%rip), %xmm1
	movsd	.LC6(%rip), %xmm4
	divsd	%xmm1, %xmm4
	jmp	.L621
.L642:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3018:
	.size	_ZN2v84base7ieee7543tanEd, .-_ZN2v84base7ieee7543tanEd
	.section	.text._ZN2v84base7ieee7544coshEd,"ax",@progbits
	.p2align 4
	.globl	_ZN2v84base7ieee7544coshEd
	.type	_ZN2v84base7ieee7544coshEd, @function
_ZN2v84base7ieee7544coshEd:
.LFB3019:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%xmm0, %rsi
	movapd	%xmm0, %xmm1
	andpd	.LC5(%rip), %xmm1
	shrq	$32, %rsi
	andl	$2147483647, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	cmpl	$1071001154, %esi
	jle	.L657
	cmpl	$1077280767, %esi
	jle	.L658
	cmpl	$1082535489, %esi
	jle	.L659
	movsd	.LC140(%rip), %xmm2
	comisd	%xmm1, %xmm2
	jnb	.L660
	cmpl	$2146435071, %esi
	jg	.L661
	movsd	_ZZN2v84base7ieee7544coshEdE4huge(%rip), %xmm0
	movsd	_ZZN2v84base7ieee7544coshEdE4huge(%rip), %xmm1
	mulsd	%xmm1, %xmm0
.L646:
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L657:
	.cfi_restore_state
	movapd	%xmm1, %xmm0
	call	_ZN2v84base7ieee7545expm1Ed
	movsd	.LC6(%rip), %xmm2
	movapd	%xmm0, %xmm1
	addsd	%xmm2, %xmm0
	cmpl	$1015021567, %esi
	jle	.L646
	mulsd	%xmm1, %xmm1
	addsd	%xmm0, %xmm0
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	divsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	addsd	%xmm2, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L659:
	.cfi_restore_state
	movapd	%xmm1, %xmm0
	call	_ZN2v84base7ieee7543expEd
	mulsd	.LC8(%rip), %xmm0
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L661:
	.cfi_restore_state
	mulsd	%xmm0, %xmm0
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L658:
	.cfi_restore_state
	movapd	%xmm1, %xmm0
	call	_ZN2v84base7ieee7543expEd
	movsd	.LC8(%rip), %xmm1
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movapd	%xmm0, %xmm2
	mulsd	%xmm1, %xmm0
	divsd	%xmm2, %xmm1
	addsd	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L660:
	.cfi_restore_state
	movsd	.LC8(%rip), %xmm11
	mulsd	%xmm11, %xmm1
	movapd	%xmm1, %xmm0
	call	_ZN2v84base7ieee7543expEd
	popq	%rbp
	.cfi_def_cfa 7, 8
	movapd	%xmm0, %xmm1
	movapd	%xmm11, %xmm0
	mulsd	%xmm1, %xmm0
	mulsd	%xmm1, %xmm0
	ret
	.cfi_endproc
.LFE3019:
	.size	_ZN2v84base7ieee7544coshEd, .-_ZN2v84base7ieee7544coshEd
	.section	.text._ZN2v84base7ieee7543powEdd,"ax",@progbits
	.p2align 4
	.globl	_ZN2v84base7ieee7543powEdd
	.type	_ZN2v84base7ieee7543powEdd, @function
_ZN2v84base7ieee7543powEdd:
.LFB3020:
	.cfi_startproc
	endbr64
	movq	%xmm1, %rdi
	movq	%xmm1, %rax
	shrq	$32, %rdi
	movl	%edi, %edx
	andl	$2147483647, %edx
	movl	%edx, %esi
	orl	%eax, %esi
	je	.L710
	movq	%xmm0, %r9
	movq	%xmm0, %r8
	shrq	$32, %r9
	movl	%r9d, %r10d
	andl	$2147483647, %r10d
	cmpl	$2146435072, %r10d
	jg	.L664
	sete	%sil
	testl	%r8d, %r8d
	setne	%cl
	testb	%cl, %sil
	jne	.L664
	cmpl	$2146435072, %edx
	jg	.L664
	jne	.L665
	testl	%eax, %eax
	jne	.L664
.L665:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	testl	%r9d, %r9d
	js	.L770
.L666:
	testl	%eax, %eax
	jne	.L670
	cmpl	$2146435072, %edx
	je	.L771
.L671:
	cmpl	$1072693248, %edx
	je	.L772
	cmpl	$1073741824, %edi
	je	.L773
	cmpq	$1071644672, %rdi
	jne	.L670
	testl	%r9d, %r9d
	js	.L670
	pxor	%xmm1, %xmm1
	movapd	%xmm0, %xmm2
	ucomisd	%xmm0, %xmm1
	sqrtsd	%xmm2, %xmm2
	jbe	.L662
	movsd	%xmm2, -24(%rbp)
	call	sqrt@PLT
	movsd	-24(%rbp), %xmm2
	jmp	.L662
	.p2align 4,,10
	.p2align 3
.L664:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	movapd	%xmm0, %xmm2
	addsd	%xmm1, %xmm2
	movapd	%xmm2, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L710:
	movsd	.LC6(%rip), %xmm2
	movapd	%xmm2, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L670:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movapd	%xmm0, %xmm2
	andpd	.LC5(%rip), %xmm2
	testl	%r8d, %r8d
	je	.L774
.L678:
	movl	%r9d, %ecx
	sarl	$31, %ecx
	addl	$1, %ecx
	movl	%ecx, %eax
	orl	%esi, %eax
	je	.L766
	leal	-1(%rsi), %eax
	orl	%ecx, %eax
	jne	.L723
	movsd	.LC16(%rip), %xmm4
	movsd	.LC141(%rip), %xmm5
	movsd	.LC49(%rip), %xmm3
.L682:
	cmpl	$1105199104, %edx
	jle	.L683
	cmpl	$1139802112, %edx
	jle	.L684
	cmpl	$1072693247, %r10d
	jg	.L685
	pxor	%xmm2, %xmm2
	testl	%edi, %edi
	jns	.L662
.L686:
	movsd	.LC39(%rip), %xmm2
	mulsd	%xmm2, %xmm2
	jmp	.L662
	.p2align 4,,10
	.p2align 3
.L770:
	cmpl	$1128267775, %edx
	jg	.L712
	cmpl	$1072693247, %edx
	jle	.L666
	movl	%edx, %r11d
	sarl	$20, %r11d
	subl	$1023, %r11d
	cmpl	$20, %r11d
	jle	.L667
	movl	$52, %ecx
	subl	%r11d, %ecx
	movl	%eax, %r11d
	shrl	%cl, %r11d
	movl	%r11d, %ebx
	sall	%cl, %ebx
	cmpl	%eax, %ebx
	jne	.L666
	andl	$1, %r11d
	movl	$2, %esi
	subl	%r11d, %esi
	jmp	.L666
	.p2align 4,,10
	.p2align 3
.L774:
	movl	%r9d, %ecx
	andl	$1073741823, %ecx
	cmpl	$1072693248, %ecx
	je	.L709
	testl	%r10d, %r10d
	jne	.L678
.L709:
	testl	%edi, %edi
	js	.L775
.L680:
	testl	%r9d, %r9d
	js	.L776
.L662:
	addq	$24, %rsp
	movapd	%xmm2, %xmm0
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L668:
	.cfi_restore_state
	movsd	.LC26(%rip), %xmm2
	testl	%r8d, %r8d
	jne	.L662
	movl	%r9d, %eax
	movapd	%xmm0, %xmm2
	andpd	.LC5(%rip), %xmm2
	andl	$1073741823, %eax
	cmpl	$1072693248, %eax
	je	.L737
	testl	%r10d, %r10d
	je	.L737
	.p2align 4,,10
	.p2align 3
.L766:
	movsd	.LC26(%rip), %xmm2
	jmp	.L662
	.p2align 4,,10
	.p2align 3
.L712:
	movl	$2, %esi
	jmp	.L666
	.p2align 4,,10
	.p2align 3
.L771:
	leal	-1072693248(%r10), %eax
	orl	%r8d, %eax
	je	.L777
	pxor	%xmm2, %xmm2
	cmpl	$1072693247, %r10d
	jle	.L673
	testl	%edi, %edi
	js	.L662
	movapd	%xmm1, %xmm2
	jmp	.L662
	.p2align 4,,10
	.p2align 3
.L683:
	xorl	%eax, %eax
	testl	$2146435072, %r9d
	jne	.L690
	mulsd	.LC147(%rip), %xmm2
	movl	$-53, %eax
	movq	%xmm2, %r10
	shrq	$32, %r10
.L690:
	movl	%r10d, %edx
	andl	$1048575, %r10d
	sarl	$20, %edx
	leal	-1023(%rax,%rdx), %ecx
	movl	%r10d, %eax
	orl	$1072693248, %eax
	cmpl	$235662, %r10d
	jle	.L728
	cmpl	$767609, %r10d
	jle	.L729
	addl	$1, %ecx
	subl	$1048576, %eax
.L728:
	movsd	.LC6(%rip), %xmm12
	pxor	%xmm6, %xmm6
	xorl	%edi, %edi
	movapd	%xmm6, %xmm7
	movapd	%xmm6, %xmm8
.L691:
	movq	%xmm2, %rdx
	movq	%rax, %rsi
	salq	$32, %rsi
	movl	%edx, %edx
	orq	%rsi, %rdx
	movq	%rdx, %xmm9
	movq	%rdx, %xmm2
	movq	%rdx, %xmm0
	addsd	%xmm12, %xmm9
	subsd	%xmm12, %xmm2
	ucomisd	%xmm6, %xmm9
	jp	.L740
	jne	.L740
	movsd	.LC102(%rip), %xmm11
	movmskpd	%xmm9, %edx
	andl	$1, %edx
	je	.L694
	movsd	.LC90(%rip), %xmm11
.L694:
	movapd	%xmm2, %xmm9
	sarl	%eax
	movabsq	$-4294967296, %rdx
	mulsd	%xmm11, %xmm9
	orl	$536870912, %eax
	leal	524288(%rdi,%rax), %eax
	salq	$32, %rax
	movq	%rax, %xmm6
	movq	%xmm9, %rsi
	andq	%rdx, %rsi
	movq	%rsi, %xmm13
	movq	%rsi, %xmm10
	mulsd	%xmm6, %xmm13
	subsd	%xmm12, %xmm6
	movq	%rsi, %xmm12
	mulsd	%xmm10, %xmm12
	subsd	%xmm6, %xmm0
	subsd	%xmm13, %xmm2
	movsd	.LC117(%rip), %xmm13
	movapd	%xmm0, %xmm6
	mulsd	%xmm10, %xmm6
	subsd	%xmm6, %xmm2
	movsd	.LC148(%rip), %xmm6
	movapd	%xmm2, %xmm0
	movapd	%xmm9, %xmm2
	mulsd	%xmm9, %xmm2
	mulsd	%xmm11, %xmm0
	movapd	%xmm12, %xmm11
	addsd	%xmm13, %xmm11
	mulsd	%xmm2, %xmm6
	addsd	.LC149(%rip), %xmm6
	mulsd	%xmm2, %xmm6
	addsd	.LC150(%rip), %xmm6
	mulsd	%xmm2, %xmm6
	addsd	.LC151(%rip), %xmm6
	mulsd	%xmm2, %xmm6
	addsd	.LC152(%rip), %xmm6
	mulsd	%xmm2, %xmm6
	mulsd	%xmm2, %xmm2
	addsd	.LC153(%rip), %xmm6
	mulsd	%xmm2, %xmm6
	movapd	%xmm9, %xmm2
	addsd	%xmm10, %xmm2
	mulsd	%xmm0, %xmm2
	addsd	%xmm6, %xmm2
	addsd	%xmm2, %xmm11
	movq	%xmm11, %rax
	andq	%rdx, %rax
	movq	%rax, %xmm11
	movq	%rax, %xmm6
	subsd	%xmm13, %xmm11
	mulsd	%xmm6, %xmm0
	mulsd	%xmm6, %xmm10
	subsd	%xmm12, %xmm11
	subsd	%xmm11, %xmm2
	movapd	%xmm10, %xmm6
	mulsd	%xmm9, %xmm2
	movsd	.LC154(%rip), %xmm9
	addsd	%xmm0, %xmm2
	addsd	%xmm2, %xmm6
	movq	%xmm6, %rax
	andq	%rdx, %rax
	movq	%rax, %xmm0
	movq	%rax, %xmm6
	subsd	%xmm10, %xmm0
	mulsd	%xmm6, %xmm9
	mulsd	.LC156(%rip), %xmm6
	subsd	%xmm0, %xmm2
	movsd	.LC155(%rip), %xmm0
	mulsd	%xmm2, %xmm0
	pxor	%xmm2, %xmm2
	cvtsi2sdl	%ecx, %xmm2
	addsd	%xmm6, %xmm0
	movapd	%xmm9, %xmm6
	addsd	%xmm8, %xmm0
	addsd	%xmm0, %xmm6
	addsd	%xmm7, %xmm6
	addsd	%xmm2, %xmm6
	movq	%xmm6, %rax
	andq	%rdx, %rax
	movq	%rax, %xmm14
	movq	%rax, %xmm6
	subsd	%xmm2, %xmm14
	movapd	%xmm14, %xmm2
	subsd	%xmm7, %xmm2
	subsd	%xmm9, %xmm2
	subsd	%xmm2, %xmm0
.L689:
	movq	%xmm1, %rax
	movapd	%xmm1, %xmm2
	movabsq	$-4294967296, %rdx
	mulsd	%xmm0, %xmm1
	andq	%rdx, %rax
	movq	%rax, %xmm7
	subsd	%xmm7, %xmm2
	mulsd	%xmm6, %xmm2
	mulsd	%xmm7, %xmm6
	addsd	%xmm2, %xmm1
	movapd	%xmm1, %xmm0
	addsd	%xmm6, %xmm0
	movq	%xmm0, %rdx
	movq	%xmm0, %rcx
	shrq	$32, %rdx
	cmpl	$1083179007, %edx
	jle	.L695
	leal	-1083179008(%rdx), %eax
	orl	%ecx, %eax
	jne	.L769
	movsd	.LC157(%rip), %xmm2
	subsd	%xmm6, %xmm0
	addsd	%xmm1, %xmm2
	comisd	%xmm0, %xmm2
	ja	.L769
.L697:
	movl	%edx, %eax
	sarl	$20, %eax
	andl	$2047, %eax
.L708:
	leal	-1022(%rax), %ecx
	movl	$1048576, %eax
	movl	$1048575, %esi
	movl	$20, %edi
	sarl	%cl, %eax
	movapd	%xmm1, %xmm0
	addl	%edx, %eax
	movl	%eax, %ecx
	sarl	$20, %ecx
	andl	$2047, %ecx
	subl	$1023, %ecx
	sarl	%cl, %esi
	subl	%ecx, %edi
	notl	%esi
	movl	%edi, %ecx
	andl	%eax, %esi
	andl	$1048575, %eax
	salq	$32, %rsi
	orl	$1048576, %eax
	movq	%rsi, %xmm7
	sarl	%cl, %eax
	subsd	%xmm7, %xmm6
	movl	%eax, %edi
	negl	%eax
	testl	%edx, %edx
	cmovs	%eax, %edi
	addsd	%xmm6, %xmm0
	movl	%edi, %edx
	sall	$20, %edx
.L700:
	movq	%xmm0, %rax
	movsd	.LC158(%rip), %xmm4
	movabsq	$-4294967296, %rcx
	andq	%rcx, %rax
	movq	%rax, %xmm0
	movq	%rax, %xmm2
	subsd	%xmm6, %xmm0
	mulsd	%xmm2, %xmm4
	pxor	%xmm6, %xmm6
	mulsd	.LC159(%rip), %xmm2
	subsd	%xmm0, %xmm1
	mulsd	.LC101(%rip), %xmm1
	movapd	%xmm1, %xmm0
	addsd	%xmm2, %xmm0
	movapd	%xmm4, %xmm2
	addsd	%xmm0, %xmm2
	movapd	%xmm2, %xmm1
	movapd	%xmm2, %xmm5
	subsd	%xmm4, %xmm1
	movapd	%xmm2, %xmm4
	mulsd	%xmm2, %xmm4
	subsd	%xmm1, %xmm0
	movsd	.LC85(%rip), %xmm1
	mulsd	%xmm4, %xmm1
	subsd	.LC86(%rip), %xmm1
	mulsd	%xmm4, %xmm1
	addsd	.LC87(%rip), %xmm1
	mulsd	%xmm4, %xmm1
	subsd	.LC88(%rip), %xmm1
	mulsd	%xmm4, %xmm1
	addsd	.LC89(%rip), %xmm1
	mulsd	%xmm4, %xmm1
	subsd	%xmm1, %xmm5
	movapd	%xmm5, %xmm1
	movapd	%xmm5, %xmm4
	movapd	%xmm2, %xmm5
	mulsd	%xmm0, %xmm5
	subsd	.LC47(%rip), %xmm4
	mulsd	%xmm2, %xmm1
	addsd	%xmm5, %xmm0
	subsd	%xmm0, %xmm4
	movapd	%xmm1, %xmm0
	ucomisd	%xmm6, %xmm4
	jp	.L742
	jne	.L742
	ucomisd	%xmm6, %xmm1
	movl	$0, %eax
	setnp	%cl
	cmovne	%eax, %ecx
	testb	%cl, %cl
	jne	.L734
	ucomisd	%xmm1, %xmm1
	jp	.L734
	jne	.L734
	comisd	%xmm6, %xmm1
	movmskpd	%xmm4, %ecx
	movsd	.LC102(%rip), %xmm0
	setnb	%sil
	andl	$1, %ecx
	sete	%al
	cmpb	%al, %sil
	je	.L704
	movsd	.LC90(%rip), %xmm0
.L704:
	subsd	%xmm2, %xmm0
	movsd	.LC6(%rip), %xmm1
	subsd	%xmm0, %xmm1
	movq	%xmm1, %rcx
	movq	%xmm1, %rax
	shrq	$32, %rcx
	movl	%eax, %eax
	addl	%ecx, %edx
	movq	%rdx, %rcx
	salq	$32, %rcx
	orq	%rcx, %rax
	movq	%rax, %xmm2
	cmpl	$1048575, %edx
	jle	.L778
.L707:
	mulsd	%xmm3, %xmm2
	jmp	.L662
	.p2align 4,,10
	.p2align 3
.L723:
	movsd	.LC39(%rip), %xmm5
	movsd	.LC6(%rip), %xmm3
	pxor	%xmm4, %xmm4
	jmp	.L682
	.p2align 4,,10
	.p2align 3
.L772:
	movapd	%xmm0, %xmm2
	testl	%edi, %edi
	jns	.L662
	ucomisd	.LC0(%rip), %xmm0
	jp	.L738
	jne	.L738
	movmskpd	%xmm0, %eax
	movsd	.LC102(%rip), %xmm2
	testb	$1, %al
	je	.L662
	movsd	.LC90(%rip), %xmm2
	jmp	.L662
.L695:
	movl	%edx, %eax
	andl	$2147483647, %eax
	cmpl	$1083231231, %eax
	jle	.L699
	leal	1064252416(%rdx), %eax
	movapd	%xmm4, %xmm2
	orl	%ecx, %eax
	jne	.L662
	subsd	%xmm6, %xmm0
	comisd	%xmm1, %xmm0
	jb	.L697
	jmp	.L662
.L776:
	subl	$1072693248, %r10d
	orl	%esi, %r10d
	je	.L766
	cmpl	$1, %esi
	jne	.L662
	xorpd	.LC15(%rip), %xmm2
	jmp	.L662
.L775:
	ucomisd	.LC0(%rip), %xmm0
	jp	.L739
	jne	.L739
	movsd	.LC102(%rip), %xmm2
	jmp	.L680
.L684:
	cmpl	$1072693246, %r10d
	jle	.L779
	cmpl	$1072693248, %r10d
	jg	.L780
	subsd	.LC6(%rip), %xmm2
	movsd	.LC146(%rip), %xmm6
	movabsq	$-4294967296, %rax
	movsd	.LC92(%rip), %xmm7
	movsd	.LC145(%rip), %xmm0
	movsd	.LC144(%rip), %xmm8
	mulsd	%xmm2, %xmm6
	mulsd	%xmm2, %xmm8
	mulsd	%xmm2, %xmm0
	subsd	%xmm6, %xmm7
	movsd	.LC8(%rip), %xmm6
	mulsd	%xmm2, %xmm7
	mulsd	%xmm2, %xmm2
	subsd	%xmm7, %xmm6
	movapd	%xmm8, %xmm7
	mulsd	%xmm6, %xmm2
	mulsd	.LC82(%rip), %xmm2
	subsd	%xmm2, %xmm0
	addsd	%xmm0, %xmm7
	movq	%xmm7, %rbx
	andq	%rax, %rbx
	movq	%rbx, %xmm2
	movq	%rbx, %xmm6
	subsd	%xmm8, %xmm2
	subsd	%xmm2, %xmm0
	jmp	.L689
.L780:
	movapd	%xmm4, %xmm2
	testl	%edi, %edi
	jle	.L662
.L769:
	mulsd	.LC39(%rip), %xmm5
	movapd	%xmm5, %xmm2
	jmp	.L662
.L777:
	movapd	%xmm1, %xmm2
	subsd	%xmm1, %xmm2
	jmp	.L662
.L673:
	testl	%edi, %edi
	jns	.L662
	movapd	%xmm1, %xmm2
	xorpd	.LC15(%rip), %xmm2
	jmp	.L662
.L667:
	testl	%eax, %eax
	jne	.L668
	movl	$20, %ecx
	movl	%edx, %eax
	subl	%r11d, %ecx
	sarl	%cl, %eax
	movl	%eax, %ebx
	sall	%cl, %ebx
	cmpl	%edx, %ebx
	jne	.L671
	andl	$1, %eax
	movl	$2, %esi
	subl	%eax, %esi
	jmp	.L671
.L773:
	movapd	%xmm0, %xmm2
	mulsd	%xmm0, %xmm2
	jmp	.L662
.L699:
	cmpl	$1071644672, %eax
	jg	.L781
	xorl	%edx, %edx
	xorl	%edi, %edi
	jmp	.L700
.L740:
	movsd	.LC6(%rip), %xmm11
	divsd	%xmm9, %xmm11
	jmp	.L694
.L685:
	pxor	%xmm2, %xmm2
	testl	%edi, %edi
	jg	.L686
	jmp	.L662
.L729:
	movsd	.LC142(%rip), %xmm7
	movl	$262144, %edi
	pxor	%xmm6, %xmm6
	movsd	.LC143(%rip), %xmm8
	movsd	.LC48(%rip), %xmm12
	jmp	.L691
.L742:
	divsd	%xmm4, %xmm0
	jmp	.L704
.L734:
	movsd	.LC105(%rip), %xmm0
	jmp	.L704
.L737:
	xorl	%esi, %esi
	jmp	.L709
.L778:
	movapd	%xmm1, %xmm0
	movsd	%xmm3, -24(%rbp)
	call	scalbn@PLT
	movsd	-24(%rbp), %xmm3
	movapd	%xmm0, %xmm2
	jmp	.L707
.L738:
	movsd	.LC6(%rip), %xmm2
	divsd	%xmm0, %xmm2
	jmp	.L662
.L739:
	movsd	.LC6(%rip), %xmm0
	divsd	%xmm2, %xmm0
	movapd	%xmm0, %xmm2
	jmp	.L680
.L779:
	movapd	%xmm4, %xmm2
	testl	%edi, %edi
	jns	.L662
	jmp	.L769
.L781:
	sarl	$20, %eax
	jmp	.L708
	.cfi_endproc
.LFE3020:
	.size	_ZN2v84base7ieee7543powEdd, .-_ZN2v84base7ieee7543powEdd
	.section	.text._ZN2v84base7ieee7544sinhEd,"ax",@progbits
	.p2align 4
	.globl	_ZN2v84base7ieee7544sinhEd
	.type	_ZN2v84base7ieee7544sinhEd, @function
_ZN2v84base7ieee7544sinhEd:
.LFB3021:
	.cfi_startproc
	endbr64
	movsd	.LC109(%rip), %xmm11
	pxor	%xmm1, %xmm1
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	comisd	%xmm0, %xmm1
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	ja	.L783
	movsd	.LC8(%rip), %xmm11
.L783:
	movsd	.LC160(%rip), %xmm1
	movapd	%xmm0, %xmm8
	andpd	.LC5(%rip), %xmm8
	comisd	%xmm8, %xmm1
	jbe	.L799
	movsd	.LC161(%rip), %xmm1
	comisd	%xmm8, %xmm1
	jbe	.L804
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L799:
	.cfi_restore_state
	movsd	.LC162(%rip), %xmm1
	comisd	%xmm8, %xmm1
	ja	.L805
	movsd	.LC140(%rip), %xmm1
	comisd	%xmm8, %xmm1
	jnb	.L806
	mulsd	.LC163(%rip), %xmm0
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L804:
	.cfi_restore_state
	movapd	%xmm8, %xmm0
	call	_ZN2v84base7ieee7545expm1Ed
	movsd	.LC6(%rip), %xmm2
	movapd	%xmm0, %xmm1
	comisd	%xmm8, %xmm2
	ja	.L807
	addsd	%xmm0, %xmm2
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	divsd	%xmm2, %xmm0
	addsd	%xmm1, %xmm0
	mulsd	%xmm11, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L805:
	.cfi_restore_state
	movapd	%xmm8, %xmm0
	call	_ZN2v84base7ieee7543expEd
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	mulsd	%xmm11, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L807:
	.cfi_restore_state
	movapd	%xmm1, %xmm3
	addsd	%xmm0, %xmm0
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	mulsd	%xmm1, %xmm3
	addsd	%xmm2, %xmm1
	divsd	%xmm1, %xmm3
	subsd	%xmm3, %xmm0
	mulsd	%xmm11, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L806:
	.cfi_restore_state
	movsd	.LC8(%rip), %xmm0
	mulsd	%xmm8, %xmm0
	call	_ZN2v84base7ieee7543expEd
	popq	%rbp
	.cfi_def_cfa 7, 8
	mulsd	%xmm0, %xmm11
	mulsd	%xmm11, %xmm0
	ret
	.cfi_endproc
.LFE3021:
	.size	_ZN2v84base7ieee7544sinhEd, .-_ZN2v84base7ieee7544sinhEd
	.section	.text._ZN2v84base7ieee7544tanhEd,"ax",@progbits
	.p2align 4
	.globl	_ZN2v84base7ieee7544tanhEd
	.type	_ZN2v84base7ieee7544tanhEd, @function
_ZN2v84base7ieee7544tanhEd:
.LFB3022:
	.cfi_startproc
	endbr64
	movq	%xmm0, %rsi
	shrq	$32, %rsi
	movl	%esi, %eax
	andl	$2147483647, %eax
	cmpl	$2146435071, %eax
	jle	.L809
	movsd	.LC6(%rip), %xmm2
	movapd	%xmm2, %xmm1
	divsd	%xmm0, %xmm1
	testl	%esi, %esi
	js	.L810
	addsd	%xmm2, %xmm1
.L820:
	movapd	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L809:
	cmpl	$1077280767, %eax
	jg	.L812
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	cmpl	$1043333119, %eax
	jle	.L825
	andpd	.LC5(%rip), %xmm0
	cmpl	$1072693247, %eax
	jle	.L815
	addsd	%xmm0, %xmm0
	call	_ZN2v84base7ieee7545expm1Ed
	movsd	.LC47(%rip), %xmm1
	addsd	%xmm1, %xmm0
	divsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	movsd	.LC6(%rip), %xmm1
	subsd	%xmm0, %xmm1
.L816:
	testl	%esi, %esi
	jns	.L808
	xorpd	.LC15(%rip), %xmm1
.L808:
	movapd	%xmm1, %xmm0
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L812:
	.cfi_restore 6
	movsd	_ZZN2v84base7ieee7544tanhEdE4tiny(%rip), %xmm0
	movsd	.LC6(%rip), %xmm1
	subsd	%xmm0, %xmm1
	testl	%esi, %esi
	jns	.L820
	xorpd	.LC15(%rip), %xmm1
	jmp	.L820
	.p2align 4,,10
	.p2align 3
.L810:
	subsd	%xmm2, %xmm1
	movapd	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L825:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	movsd	.LC39(%rip), %xmm2
	movapd	%xmm0, %xmm1
	addsd	%xmm0, %xmm2
	comisd	.LC6(%rip), %xmm2
	ja	.L808
	andpd	.LC5(%rip), %xmm0
.L815:
	mulsd	.LC120(%rip), %xmm0
	call	_ZN2v84base7ieee7545expm1Ed
	movapd	%xmm0, %xmm1
	addsd	.LC47(%rip), %xmm0
	xorpd	.LC15(%rip), %xmm1
	divsd	%xmm0, %xmm1
	jmp	.L816
	.cfi_endproc
.LFE3022:
	.size	_ZN2v84base7ieee7544tanhEd, .-_ZN2v84base7ieee7544tanhEd
	.section	.data._ZZN2v84base7ieee7544tanhEdE4tiny,"aw"
	.align 8
	.type	_ZZN2v84base7ieee7544tanhEdE4tiny, @object
	.size	_ZZN2v84base7ieee7544tanhEdE4tiny, 8
_ZZN2v84base7ieee7544tanhEdE4tiny:
	.long	3271095129
	.long	27618847
	.section	.data._ZZN2v84base7ieee7544coshEdE4huge,"aw"
	.align 8
	.type	_ZZN2v84base7ieee7544coshEdE4huge, @object
	.size	_ZZN2v84base7ieee7544coshEdE4huge, 8
_ZZN2v84base7ieee7544coshEdE4huge:
	.long	2281731484
	.long	2117592124
	.section	.data._ZZN2v84base7ieee7545expm1EdE4huge,"aw"
	.align 8
	.type	_ZZN2v84base7ieee7545expm1EdE4huge, @object
	.size	_ZZN2v84base7ieee7545expm1EdE4huge, 8
_ZZN2v84base7ieee7545expm1EdE4huge:
	.long	2281731484
	.long	2117592124
	.section	.data._ZZN2v84base7ieee7543expEdE7two1023,"aw"
	.align 8
	.type	_ZZN2v84base7ieee7543expEdE7two1023, @object
	.size	_ZZN2v84base7ieee7543expEdE7two1023, 8
_ZZN2v84base7ieee7543expEdE7two1023:
	.long	0
	.long	2145386496
	.section	.data._ZZN2v84base7ieee7543expEdE8twom1000,"aw"
	.align 8
	.type	_ZZN2v84base7ieee7543expEdE8twom1000, @object
	.size	_ZZN2v84base7ieee7543expEdE8twom1000, 8
_ZZN2v84base7ieee7543expEdE8twom1000:
	.long	0
	.long	24117248
	.section	.data._ZZN2v84base7ieee7543expEdE4huge,"aw"
	.align 8
	.type	_ZZN2v84base7ieee7543expEdE4huge, @object
	.size	_ZZN2v84base7ieee7543expEdE4huge, 8
_ZZN2v84base7ieee7543expEdE4huge:
	.long	2281731484
	.long	2117592124
	.section	.rodata._ZZN2v84base7ieee7543expEdE5ln2LO,"a"
	.align 16
	.type	_ZZN2v84base7ieee7543expEdE5ln2LO, @object
	.size	_ZZN2v84base7ieee7543expEdE5ln2LO, 16
_ZZN2v84base7ieee7543expEdE5ln2LO:
	.long	897137782
	.long	1038760431
	.long	897137782
	.long	-1108723217
	.section	.rodata._ZZN2v84base7ieee7543expEdE5ln2HI,"a"
	.align 16
	.type	_ZZN2v84base7ieee7543expEdE5ln2HI, @object
	.size	_ZZN2v84base7ieee7543expEdE5ln2HI, 16
_ZZN2v84base7ieee7543expEdE5ln2HI:
	.long	4276092928
	.long	1072049730
	.long	4276092928
	.long	-1075433918
	.section	.rodata._ZZN2v84base7ieee7543expEdE4halF,"a"
	.align 16
	.type	_ZZN2v84base7ieee7543expEdE4halF, @object
	.size	_ZZN2v84base7ieee7543expEdE4halF, 16
_ZZN2v84base7ieee7543expEdE4halF:
	.long	0
	.long	1071644672
	.long	0
	.long	-1075838976
	.section	.data._ZZN2v84base7ieee7545atan2EddE5pi_lo,"aw"
	.align 8
	.type	_ZZN2v84base7ieee7545atan2EddE5pi_lo, @object
	.size	_ZZN2v84base7ieee7545atan2EddE5pi_lo, 8
_ZZN2v84base7ieee7545atan2EddE5pi_lo:
	.long	856972295
	.long	1017226790
	.section	.data._ZZN2v84base7ieee7545atan2EddE4tiny,"aw"
	.align 8
	.type	_ZZN2v84base7ieee7545atan2EddE4tiny, @object
	.size	_ZZN2v84base7ieee7545atan2EddE4tiny, 8
_ZZN2v84base7ieee7545atan2EddE4tiny:
	.long	3271095129
	.long	27618847
	.section	.rodata._ZZN2v84base7ieee7544atanEdE6atanlo,"a"
	.align 32
	.type	_ZZN2v84base7ieee7544atanEdE6atanlo, @object
	.size	_ZZN2v84base7ieee7544atanEdE6atanlo, 32
_ZZN2v84base7ieee7544atanEdE6atanlo:
	.long	573531618
	.long	1014639487
	.long	856972295
	.long	1015129638
	.long	2062601149
	.long	1013974920
	.long	856972295
	.long	1016178214
	.section	.rodata._ZZN2v84base7ieee75412_GLOBAL__N_118__ieee754_rem_pio2EdPdE8npio2_hw,"a"
	.align 32
	.type	_ZZN2v84base7ieee75412_GLOBAL__N_118__ieee754_rem_pio2EdPdE8npio2_hw, @object
	.size	_ZZN2v84base7ieee75412_GLOBAL__N_118__ieee754_rem_pio2EdPdE8npio2_hw, 128
_ZZN2v84base7ieee75412_GLOBAL__N_118__ieee754_rem_pio2EdPdE8npio2_hw:
	.long	1073291771
	.long	1074340347
	.long	1074977148
	.long	1075388923
	.long	1075800698
	.long	1076025724
	.long	1076231611
	.long	1076437499
	.long	1076643386
	.long	1076849274
	.long	1076971356
	.long	1077074300
	.long	1077177244
	.long	1077280187
	.long	1077383131
	.long	1077486075
	.long	1077589019
	.long	1077691962
	.long	1077794906
	.long	1077897850
	.long	1077968460
	.long	1078019932
	.long	1078071404
	.long	1078122876
	.long	1078174348
	.long	1078225820
	.long	1078277292
	.long	1078328763
	.long	1078380235
	.long	1078431707
	.long	1078483179
	.long	1078534651
	.section	.rodata._ZZN2v84base7ieee75412_GLOBAL__N_118__ieee754_rem_pio2EdPdE11two_over_pi,"a"
	.align 32
	.type	_ZZN2v84base7ieee75412_GLOBAL__N_118__ieee754_rem_pio2EdPdE11two_over_pi, @object
	.size	_ZZN2v84base7ieee75412_GLOBAL__N_118__ieee754_rem_pio2EdPdE11two_over_pi, 264
_ZZN2v84base7ieee75412_GLOBAL__N_118__ieee754_rem_pio2EdPdE11two_over_pi:
	.long	10680707
	.long	7228996
	.long	1387004
	.long	2578385
	.long	16069853
	.long	12639074
	.long	9804092
	.long	4427841
	.long	16666979
	.long	11263675
	.long	12935607
	.long	2387514
	.long	4345298
	.long	14681673
	.long	3074569
	.long	13734428
	.long	16653803
	.long	1880361
	.long	10960616
	.long	8533493
	.long	3062596
	.long	8710556
	.long	7349940
	.long	6258241
	.long	3772886
	.long	3769171
	.long	3798172
	.long	8675211
	.long	12450088
	.long	3874808
	.long	9961438
	.long	366607
	.long	15675153
	.long	9132554
	.long	7151469
	.long	3571407
	.long	2607881
	.long	12013382
	.long	4155038
	.long	6285869
	.long	7677882
	.long	13102053
	.long	15825725
	.long	473591
	.long	9065106
	.long	15363067
	.long	6271263
	.long	9264392
	.long	5636912
	.long	4652155
	.long	7056368
	.long	13614112
	.long	10155062
	.long	1944035
	.long	9527646
	.long	15080200
	.long	6658437
	.long	6231200
	.long	6832269
	.long	16767104
	.long	5075751
	.long	3212806
	.long	1398474
	.long	7579849
	.long	6349435
	.long	12618859
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC0:
	.long	0
	.long	0
	.align 8
.LC1:
	.long	0
	.long	1047527424
	.align 8
.LC2:
	.long	0
	.long	1097859072
	.align 8
.LC3:
	.long	0
	.long	1069547520
	.align 8
.LC4:
	.long	0
	.long	1127219200
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC5:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC6:
	.long	0
	.long	1072693248
	.align 8
.LC7:
	.long	0
	.long	1075838976
	.align 8
.LC8:
	.long	0
	.long	1071644672
	.section	.rodata.cst16
	.align 16
.LC9:
	.long	16777215
	.long	16777215
	.long	16777215
	.long	16777215
	.section	.rodata.cst8
	.align 8
.LC10:
	.long	1073741824
	.long	1073291771
	.align 8
.LC11:
	.long	0
	.long	1047807021
	.align 8
.LC12:
	.long	2147483648
	.long	1022903960
	.align 8
.LC13:
	.long	1610612736
	.long	997772369
	.align 8
.LC14:
	.long	2147483648
	.long	972036995
	.section	.rodata.cst16
	.align 16
.LC15:
	.long	0
	.long	-2147483648
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC16:
	.long	0
	.long	-2147483648
	.align 8
.LC17:
	.long	1413480448
	.long	1073291771
	.align 8
.LC18:
	.long	442655537
	.long	1037087841
	.align 8
.LC19:
	.long	442499072
	.long	1037087841
	.align 8
.LC20:
	.long	771977331
	.long	1000544650
	.align 8
.LC21:
	.long	1841940611
	.long	1071931184
	.align 8
.LC22:
	.long	771751936
	.long	1000544650
	.align 8
.LC23:
	.long	622873025
	.long	964395930
	.section	.rodata.cst16
	.align 16
.LC24:
	.long	0
	.long	-2147483648
	.long	0
	.long	-2147483648
	.section	.rodata.cst8
	.align 8
.LC25:
	.long	1413754136
	.long	1074340347
	.align 8
.LC26:
	.long	0
	.long	2146697216
	.align 8
.LC27:
	.long	1413754136
	.long	1073291771
	.align 8
.LC28:
	.long	234747657
	.long	1057111521
	.align 8
.LC29:
	.long	1963045512
	.long	1061810144
	.align 8
.LC30:
	.long	3043528507
	.long	1067745832
	.align 8
.LC31:
	.long	243811413
	.long	1070186837
	.align 8
.LC32:
	.long	65761149
	.long	1070913042
	.align 8
.LC33:
	.long	1431655765
	.long	1069897045
	.align 8
.LC34:
	.long	2972619394
	.long	1068742853
	.align 8
.LC35:
	.long	462225753
	.long	1072039532
	.align 8
.LC36:
	.long	2623113928
	.long	1073752805
	.align 8
.LC37:
	.long	478817611
	.long	1073953319
	.align 8
.LC38:
	.long	856972295
	.long	1016178214
	.align 8
.LC39:
	.long	2281731484
	.long	2117592124
	.align 8
.LC40:
	.long	1413754136
	.long	1072243195
	.align 8
.LC41:
	.long	856972295
	.long	1015129638
	.align 8
.LC42:
	.long	2062601149
	.long	1013974920
	.align 8
.LC43:
	.long	3531732635
	.long	1072657163
	.align 8
.LC44:
	.long	573531618
	.long	1014639487
	.align 8
.LC45:
	.long	90291023
	.long	1071492199
	.align 8
.LC46:
	.long	1413754136
	.long	-1074191877
	.align 8
.LC47:
	.long	0
	.long	1073741824
	.align 8
.LC48:
	.long	0
	.long	1073217536
	.align 8
.LC49:
	.long	0
	.long	-1074790400
	.align 8
.LC50:
	.long	3810712081
	.long	1066446138
	.align 8
.LC51:
	.long	611716587
	.long	1068071755
	.align 8
.LC52:
	.long	2698001745
	.long	1068567910
	.align 8
.LC53:
	.long	3310100590
	.long	1068975565
	.align 8
.LC54:
	.long	2449507327
	.long	1069697316
	.align 8
.LC55:
	.long	1431655693
	.long	1070945621
	.align 8
.LC56:
	.long	745172015
	.long	-1079856060
	.align 8
.LC57:
	.long	1390345626
	.long	1068359213
	.align 8
.LC58:
	.long	2943654509
	.long	1068740850
	.align 8
.LC59:
	.long	4263712369
	.long	1069314502
	.align 8
.LC60:
	.long	2576935876
	.long	1070176665
	.align 8
.LC61:
	.long	1413754136
	.long	-1073143301
	.align 8
.LC62:
	.long	1413754136
	.long	-1075240453
	.align 8
.LC63:
	.long	2134057426
	.long	1073928572
	.align 8
.LC64:
	.long	2134057426
	.long	-1073555076
	.align 8
.LC65:
	.long	0
	.long	1072103424
	.align 8
.LC66:
	.long	0
	.long	1070727168
	.align 8
.LC67:
	.long	3196598484
	.long	-1112999191
	.align 8
.LC68:
	.long	3182735812
	.long	1042411166
	.align 8
.LC69:
	.long	2157728429
	.long	1049787983
	.align 8
.LC70:
	.long	432739728
	.long	1056571808
	.align 8
.LC71:
	.long	381768055
	.long	1062650220
	.align 8
.LC72:
	.long	1431655756
	.long	1067799893
	.align 8
.LC73:
	.long	1523570044
	.long	1038473530
	.align 8
.LC74:
	.long	2318114027
	.long	1046144486
	.align 8
.LC75:
	.long	1471282813
	.long	1053236707
	.align 8
.LC76:
	.long	432103893
	.long	1059717536
	.align 8
.LC77:
	.long	286324902
	.long	1065423121
	.align 8
.LC78:
	.long	1431655753
	.long	1069897045
	.align 8
.LC79:
	.long	2333366121
	.long	1074118410
	.align 8
.LC80:
	.long	4277811695
	.long	1082535490
	.align 8
.LC81:
	.long	3576508497
	.long	-1064875760
	.align 8
.LC82:
	.long	1697350398
	.long	1073157447
	.align 8
.LC83:
	.long	4276092928
	.long	1072049730
	.align 8
.LC84:
	.long	897137782
	.long	1038760431
	.align 8
.LC85:
	.long	1925096656
	.long	1046886249
	.align 8
.LC86:
	.long	3318901745
	.long	1052491073
	.align 8
.LC87:
	.long	2938494508
	.long	1058100842
	.align 8
.LC88:
	.long	381599123
	.long	1063698796
	.align 8
.LC89:
	.long	1431655742
	.long	1069897045
	.align 8
.LC90:
	.long	0
	.long	-1048576
	.align 8
.LC91:
	.long	0
	.long	1129316352
	.align 8
.LC92:
	.long	1431655765
	.long	1070945621
	.align 8
.LC93:
	.long	3745403460
	.long	1069740306
	.align 8
.LC94:
	.long	2529887198
	.long	1070024292
	.align 8
.LC95:
	.long	2485293913
	.long	1070745892
	.align 8
.LC96:
	.long	1431655827
	.long	1071994197
	.align 8
.LC97:
	.long	3497576095
	.long	1069783561
	.align 8
.LC98:
	.long	495876271
	.long	1070363077
	.align 8
.LC99:
	.long	2576873988
	.long	1071225241
	.align 8
.LC100:
	.long	1431655765
	.long	1071994197
	.align 8
.LC101:
	.long	4277811695
	.long	1072049730
	.align 8
.LC102:
	.long	0
	.long	2146435072
	.align 8
.LC103:
	.long	1696595968
	.long	1073157447
	.align 8
.LC104:
	.long	787456512
	.long	1038550524
	.align 8
.LC105:
	.long	0
	.long	2146959360
	.align 8
.LC106:
	.long	301017910
	.long	1029308147
	.align 8
.LC107:
	.long	354870542
	.long	1071369083
	.align 8
.LC108:
	.long	1352622080
	.long	1070810131
	.align 8
.LC109:
	.long	0
	.long	-1075838976
	.align 8
.LC110:
	.long	897137782
	.long	-1108723217
	.align 8
.LC111:
	.long	3271095129
	.long	27618847
	.align 8
.LC112:
	.long	1846133549
	.long	-1098187337
	.align 8
.LC113:
	.long	2263241273
	.long	1053872074
	.align 8
.LC114:
	.long	2661997495
	.long	1058328089
	.align 8
.LC115:
	.long	436098437
	.long	1062863264
	.align 8
.LC116:
	.long	286331124
	.long	1067520273
	.align 8
.LC117:
	.long	0
	.long	1074266112
	.align 8
.LC118:
	.long	0
	.long	1075314688
	.align 8
.LC119:
	.long	0
	.long	-1076887552
	.align 8
.LC120:
	.long	0
	.long	-1073741824
	.align 8
.LC121:
	.long	0
	.long	2145386496
	.align 8
.LC122:
	.long	3571772887
	.long	1069723648
	.align 8
.LC123:
	.long	3202830809
	.long	1072186571
	.align 8
.LC124:
	.long	1246353090
	.long	1073344864
	.align 8
.LC125:
	.long	2465211424
	.long	1073621216
	.align 8
.LC126:
	.long	258074258
	.long	1073611750
	.align 8
.LC127:
	.long	1958705876
	.long	1056647792
	.align 8
.LC128:
	.long	854632425
	.long	1058191375
	.align 8
.LC129:
	.long	445452392
	.long	1060120311
	.align 8
.LC130:
	.long	4276126485
	.long	1062722504
	.align 8
.LC131:
	.long	3916334227
	.long	1065494243
	.align 8
.LC132:
	.long	464732670
	.long	1068212666
	.align 8
.LC133:
	.long	3680523123
	.long	-1091340853
	.align 8
.LC134:
	.long	2687996582
	.long	1058307720
	.align 8
.LC135:
	.long	4075971841
	.long	1061373144
	.align 8
.LC136:
	.long	3377857320
	.long	1064135970
	.align 8
.LC137:
	.long	2215040567
	.long	1066820852
	.align 8
.LC138:
	.long	286326394
	.long	1069617425
	.align 8
.LC139:
	.long	1431655779
	.long	1070945621
	.align 8
.LC140:
	.long	2411329661
	.long	1082536910
	.align 8
.LC141:
	.long	2281731484
	.long	-29891524
	.align 8
.LC142:
	.long	1073741824
	.long	1071822851
	.align 8
.LC143:
	.long	1137692678
	.long	1045233131
	.align 8
.LC144:
	.long	1610612736
	.long	1073157447
	.align 8
.LC145:
	.long	4166901572
	.long	1045736971
	.align 8
.LC146:
	.long	0
	.long	1070596096
	.align 8
.LC147:
	.long	0
	.long	1128267776
	.align 8
.LC148:
	.long	1246056175
	.long	1070235176
	.align 8
.LC149:
	.long	2479479653
	.long	1070433866
	.align 8
.LC150:
	.long	2837266689
	.long	1070691424
	.align 8
.LC151:
	.long	1368335949
	.long	1070945621
	.align 8
.LC152:
	.long	3681528831
	.long	1071345078
	.align 8
.LC153:
	.long	858993411
	.long	1071854387
	.align 8
.LC154:
	.long	3758096384
	.long	1072613129
	.align 8
.LC155:
	.long	3694789629
	.long	1072613129
	.align 8
.LC156:
	.long	341508597
	.long	-1103220768
	.align 8
.LC157:
	.long	1697350398
	.long	1016534343
	.align 8
.LC158:
	.long	0
	.long	1072049731
	.align 8
.LC159:
	.long	212364345
	.long	-1105175455
	.align 8
.LC160:
	.long	0
	.long	1077280768
	.align 8
.LC161:
	.long	0
	.long	1043333120
	.align 8
.LC162:
	.long	0
	.long	1082535490
	.align 8
.LC163:
	.long	1017934899
	.long	2142010143
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
