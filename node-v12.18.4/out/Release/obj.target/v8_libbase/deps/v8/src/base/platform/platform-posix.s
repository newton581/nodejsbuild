	.file	"platform-posix.cc"
	.text
	.section	.text._ZN2v84base18PosixTimezoneCache5ClearENS0_13TimezoneCache17TimeZoneDetectionE,"axG",@progbits,_ZN2v84base18PosixTimezoneCache5ClearENS0_13TimezoneCache17TimeZoneDetectionE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v84base18PosixTimezoneCache5ClearENS0_13TimezoneCache17TimeZoneDetectionE
	.type	_ZN2v84base18PosixTimezoneCache5ClearENS0_13TimezoneCache17TimeZoneDetectionE, @function
_ZN2v84base18PosixTimezoneCache5ClearENS0_13TimezoneCache17TimeZoneDetectionE:
.LFB3681:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3681:
	.size	_ZN2v84base18PosixTimezoneCache5ClearENS0_13TimezoneCache17TimeZoneDetectionE, .-_ZN2v84base18PosixTimezoneCache5ClearENS0_13TimezoneCache17TimeZoneDetectionE
	.section	.text._ZNK2v84base21PosixMemoryMappedFile6memoryEv,"axG",@progbits,_ZNK2v84base21PosixMemoryMappedFile6memoryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v84base21PosixMemoryMappedFile6memoryEv
	.type	_ZNK2v84base21PosixMemoryMappedFile6memoryEv, @function
_ZNK2v84base21PosixMemoryMappedFile6memoryEv:
.LFB4276:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	ret
	.cfi_endproc
.LFE4276:
	.size	_ZNK2v84base21PosixMemoryMappedFile6memoryEv, .-_ZNK2v84base21PosixMemoryMappedFile6memoryEv
	.section	.text._ZNK2v84base21PosixMemoryMappedFile4sizeEv,"axG",@progbits,_ZNK2v84base21PosixMemoryMappedFile4sizeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v84base21PosixMemoryMappedFile4sizeEv
	.type	_ZNK2v84base21PosixMemoryMappedFile4sizeEv, @function
_ZNK2v84base21PosixMemoryMappedFile4sizeEv:
.LFB4277:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	ret
	.cfi_endproc
.LFE4277:
	.size	_ZNK2v84base21PosixMemoryMappedFile4sizeEv, .-_ZNK2v84base21PosixMemoryMappedFile4sizeEv
	.section	.text._ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data,"axG",@progbits,_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data,comdat
	.p2align 4
	.weak	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data
	.type	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data, @function
_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data:
.LFB4938:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %r8
	movq	(%rdi), %rax
	movq	%r8, %rdi
	jmp	*%rax
	.cfi_endproc
.LFE4938:
	.size	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data, .-_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation,"axG",@progbits,_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation,comdat
	.p2align 4
	.weak	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation
	.type	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation:
.LFB4939:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L7
	cmpl	$3, %edx
	je	.L8
	cmpl	$1, %edx
	je	.L12
.L8:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	movdqu	(%rsi), %xmm0
	xorl	%eax, %eax
	movups	%xmm0, (%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4939:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation
	.section	.text._ZN2v84base16LazyInstanceImplINS0_5MutexENS0_32StaticallyAllocatedInstanceTraitIS2_EENS0_21DefaultConstructTraitIS2_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS2_EEE12InitInstanceEPv,"axG",@progbits,_ZN2v84base16LazyInstanceImplINS0_5MutexENS0_32StaticallyAllocatedInstanceTraitIS2_EENS0_21DefaultConstructTraitIS2_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS2_EEE12InitInstanceEPv,comdat
	.p2align 4
	.weak	_ZN2v84base16LazyInstanceImplINS0_5MutexENS0_32StaticallyAllocatedInstanceTraitIS2_EENS0_21DefaultConstructTraitIS2_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS2_EEE12InitInstanceEPv
	.type	_ZN2v84base16LazyInstanceImplINS0_5MutexENS0_32StaticallyAllocatedInstanceTraitIS2_EENS0_21DefaultConstructTraitIS2_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS2_EEE12InitInstanceEPv, @function
_ZN2v84base16LazyInstanceImplINS0_5MutexENS0_32StaticallyAllocatedInstanceTraitIS2_EENS0_21DefaultConstructTraitIS2_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS2_EEE12InitInstanceEPv:
.LFB4801:
	.cfi_startproc
	endbr64
	jmp	_ZN2v84base5MutexC1Ev@PLT
	.cfi_endproc
.LFE4801:
	.size	_ZN2v84base16LazyInstanceImplINS0_5MutexENS0_32StaticallyAllocatedInstanceTraitIS2_EENS0_21DefaultConstructTraitIS2_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS2_EEE12InitInstanceEPv, .-_ZN2v84base16LazyInstanceImplINS0_5MutexENS0_32StaticallyAllocatedInstanceTraitIS2_EENS0_21DefaultConstructTraitIS2_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS2_EEE12InitInstanceEPv
	.section	.text._ZN2v84base18PosixTimezoneCache21DaylightSavingsOffsetEd,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base18PosixTimezoneCache21DaylightSavingsOffsetEd
	.type	_ZN2v84base18PosixTimezoneCache21DaylightSavingsOffsetEd, @function
_ZN2v84base18PosixTimezoneCache21DaylightSavingsOffsetEd:
.LFB4289:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$80, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	ucomisd	%xmm0, %xmm0
	jp	.L17
	divsd	.LC3(%rip), %xmm0
	call	floor@PLT
	leaq	-64(%rbp), %rsi
	leaq	-72(%rbp), %rdi
	cvttsd2siq	%xmm0, %rax
	movq	%rax, -72(%rbp)
	call	localtime_r@PLT
	testq	%rax, %rax
	je	.L17
	movl	32(%rax), %eax
	movsd	.LC0(%rip), %xmm0
	testl	%eax, %eax
	jle	.L24
.L14:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L25
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L17:
	movsd	.LC1(%rip), %xmm0
	jmp	.L14
.L25:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4289:
	.size	_ZN2v84base18PosixTimezoneCache21DaylightSavingsOffsetEd, .-_ZN2v84base18PosixTimezoneCache21DaylightSavingsOffsetEd
	.section	.text._ZN2v84baseL11ThreadEntryEPv,"ax",@progbits
	.p2align 4
	.type	_ZN2v84baseL11ThreadEntryEPv, @function
_ZN2v84baseL11ThreadEntryEPv:
.LFB4319:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	8(%rdi), %rax
	movq	%rdi, %r12
	leaq	8(%rax), %r13
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$15, %edi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	leaq	16(%r12), %rsi
	xorl	%edx, %edx
	xorl	%eax, %eax
	call	prctl@PLT
	movq	40(%r12), %rdi
	testq	%rdi, %rdi
	je	.L27
	call	_ZN2v84base9Semaphore6SignalEv@PLT
.L27:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	popq	%r12
	xorl	%eax, %eax
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4319:
	.size	_ZN2v84baseL11ThreadEntryEPv, .-_ZN2v84baseL11ThreadEntryEPv
	.section	.rodata._ZN2v84base21PosixMemoryMappedFileD2Ev.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"OS::Free(memory_, RoundUp(size_, OS::AllocatePageSize()))"
	.section	.rodata._ZN2v84base21PosixMemoryMappedFileD2Ev.str1.1,"aMS",@progbits,1
.LC5:
	.string	"Check failed: %s."
	.section	.text._ZN2v84base21PosixMemoryMappedFileD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base21PosixMemoryMappedFileD2Ev
	.type	_ZN2v84base21PosixMemoryMappedFileD2Ev, @function
_ZN2v84base21PosixMemoryMappedFileD2Ev:
.LFB4281:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v84base21PosixMemoryMappedFileE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpq	$0, 16(%rdi)
	movq	%rax, (%rdi)
	je	.L33
	movl	$30, %edi
	call	sysconf@PLT
	movq	24(%rbx), %rdx
	movq	16(%rbx), %rdi
	leaq	-1(%rax,%rdx), %rsi
	negq	%rax
	andq	%rax, %rsi
	call	munmap@PLT
	testl	%eax, %eax
	jne	.L38
.L33:
	movq	8(%rbx), %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	fclose@PLT
	.p2align 4,,10
	.p2align 3
.L38:
	.cfi_restore_state
	leaq	.LC4(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE4281:
	.size	_ZN2v84base21PosixMemoryMappedFileD2Ev, .-_ZN2v84base21PosixMemoryMappedFileD2Ev
	.globl	_ZN2v84base21PosixMemoryMappedFileD1Ev
	.set	_ZN2v84base21PosixMemoryMappedFileD1Ev,_ZN2v84base21PosixMemoryMappedFileD2Ev
	.section	.text._ZN2v84base21PosixMemoryMappedFileD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base21PosixMemoryMappedFileD0Ev
	.type	_ZN2v84base21PosixMemoryMappedFileD0Ev, @function
_ZN2v84base21PosixMemoryMappedFileD0Ev:
.LFB4283:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v84base21PosixMemoryMappedFileE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	cmpq	$0, 16(%rdi)
	movq	%rax, (%rdi)
	je	.L40
	movl	$30, %edi
	call	sysconf@PLT
	movq	24(%r12), %rdx
	movq	16(%r12), %rdi
	leaq	-1(%rax,%rdx), %rsi
	negq	%rax
	andq	%rax, %rsi
	call	munmap@PLT
	testl	%eax, %eax
	jne	.L45
.L40:
	movq	8(%r12), %rdi
	call	fclose@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$32, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L45:
	.cfi_restore_state
	leaq	.LC4(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE4283:
	.size	_ZN2v84base21PosixMemoryMappedFileD0Ev, .-_ZN2v84base21PosixMemoryMappedFileD0Ev
	.section	.text._ZN2v84base2OS10InitializeEbPKc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base2OS10InitializeEbPKc
	.type	_ZN2v84base2OS10InitializeEbPKc, @function
_ZN2v84base2OS10InitializeEbPKc:
.LFB4250:
	.cfi_startproc
	endbr64
	movb	%dil, _ZN2v84base12_GLOBAL__N_112g_hard_abortE(%rip)
	movq	%rsi, _ZN2v84base12_GLOBAL__N_114g_gc_fake_mmapE(%rip)
	ret
	.cfi_endproc
.LFE4250:
	.size	_ZN2v84base2OS10InitializeEbPKc, .-_ZN2v84base2OS10InitializeEbPKc
	.section	.text._ZN2v84base2OS24ActivationFrameAlignmentEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base2OS24ActivationFrameAlignmentEv
	.type	_ZN2v84base2OS24ActivationFrameAlignmentEv, @function
_ZN2v84base2OS24ActivationFrameAlignmentEv:
.LFB4251:
	.cfi_startproc
	endbr64
	movl	$16, %eax
	ret
	.cfi_endproc
.LFE4251:
	.size	_ZN2v84base2OS24ActivationFrameAlignmentEv, .-_ZN2v84base2OS24ActivationFrameAlignmentEv
	.section	.text._ZN2v84base2OS16AllocatePageSizeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base2OS16AllocatePageSizeEv
	.type	_ZN2v84base2OS16AllocatePageSizeEv, @function
_ZN2v84base2OS16AllocatePageSizeEv:
.LFB4252:
	.cfi_startproc
	endbr64
	movl	$30, %edi
	jmp	sysconf@PLT
	.cfi_endproc
.LFE4252:
	.size	_ZN2v84base2OS16AllocatePageSizeEv, .-_ZN2v84base2OS16AllocatePageSizeEv
	.section	.text._ZN2v84base2OS14CommitPageSizeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base2OS14CommitPageSizeEv
	.type	_ZN2v84base2OS14CommitPageSizeEv, @function
_ZN2v84base2OS14CommitPageSizeEv:
.LFB4253:
	.cfi_startproc
	endbr64
	movzbl	_ZGVZN2v84base2OS14CommitPageSizeEvE9page_size(%rip), %eax
	testb	%al, %al
	je	.L60
	movq	_ZZN2v84base2OS14CommitPageSizeEvE9page_size(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L60:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZGVZN2v84base2OS14CommitPageSizeEvE9page_size(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	__cxa_guard_acquire@PLT
	testl	%eax, %eax
	jne	.L61
	movq	_ZZN2v84base2OS14CommitPageSizeEvE9page_size(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L61:
	.cfi_restore_state
	call	getpagesize@PLT
	leaq	_ZGVZN2v84base2OS14CommitPageSizeEvE9page_size(%rip), %rdi
	cltq
	movq	%rax, _ZZN2v84base2OS14CommitPageSizeEvE9page_size(%rip)
	call	__cxa_guard_release@PLT
	movq	_ZZN2v84base2OS14CommitPageSizeEvE9page_size(%rip), %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4253:
	.size	_ZN2v84base2OS14CommitPageSizeEv, .-_ZN2v84base2OS14CommitPageSizeEv
	.section	.text._ZN2v84base2OS17SetRandomMmapSeedEl,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base2OS17SetRandomMmapSeedEl
	.type	_ZN2v84base2OS17SetRandomMmapSeedEl, @function
_ZN2v84base2OS17SetRandomMmapSeedEl:
.LFB4254:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	subq	$48, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	jne	.L83
.L62:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L84
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L83:
	.cfi_restore_state
	movzbl	_ZN2v84base12_GLOBAL__N_1L9rng_mutexE(%rip), %eax
	movq	%rdi, %r12
	cmpb	$2, %al
	jne	.L85
.L64:
	leaq	8+_ZN2v84base12_GLOBAL__N_1L9rng_mutexE(%rip), %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movzbl	_ZGVZN2v84base12_GLOBAL__N_132GetPlatformRandomNumberGeneratorEvE6object(%rip), %eax
	testb	%al, %al
	je	.L86
.L67:
	movq	%r12, %rsi
	leaq	_ZZN2v84base12_GLOBAL__N_132GetPlatformRandomNumberGeneratorEvE6object(%rip), %rdi
	call	_ZN2v84base21RandomNumberGenerator7SetSeedEl@PLT
	leaq	8+_ZN2v84base12_GLOBAL__N_1L9rng_mutexE(%rip), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L85:
	leaq	_ZN2v84base16LazyInstanceImplINS0_5MutexENS0_32StaticallyAllocatedInstanceTraitIS2_EENS0_21DefaultConstructTraitIS2_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS2_EEE12InitInstanceEPv(%rip), %rax
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation(%rip), %rcx
	movq	%rax, -64(%rbp)
	movq	%rcx, %xmm0
	leaq	8+_ZN2v84base12_GLOBAL__N_1L9rng_mutexE(%rip), %rax
	leaq	-64(%rbp), %r13
	movq	%rax, -56(%rbp)
	leaq	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data(%rip), %rax
	movq	%r13, %rsi
	leaq	_ZN2v84base12_GLOBAL__N_1L9rng_mutexE(%rip), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v84base12CallOnceImplEPSt6atomicIhESt8functionIFvvEE@PLT
	movq	-48(%rbp), %rax
	testq	%rax, %rax
	je	.L64
	movl	$3, %edx
	movq	%r13, %rsi
	movq	%r13, %rdi
	call	*%rax
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L86:
	leaq	_ZGVZN2v84base12_GLOBAL__N_132GetPlatformRandomNumberGeneratorEvE6object(%rip), %rdi
	call	__cxa_guard_acquire@PLT
	testl	%eax, %eax
	je	.L67
	leaq	_ZZN2v84base12_GLOBAL__N_132GetPlatformRandomNumberGeneratorEvE6object(%rip), %rdi
	call	_ZN2v84base21RandomNumberGeneratorC1Ev@PLT
	leaq	_ZGVZN2v84base12_GLOBAL__N_132GetPlatformRandomNumberGeneratorEvE6object(%rip), %rdi
	call	__cxa_guard_release@PLT
	jmp	.L67
.L84:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4254:
	.size	_ZN2v84base2OS17SetRandomMmapSeedEl, .-_ZN2v84base2OS17SetRandomMmapSeedEl
	.section	.text._ZN2v84base2OS17GetRandomMmapAddrEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base2OS17GetRandomMmapAddrEv
	.type	_ZN2v84base2OS17GetRandomMmapAddrEv, @function
_ZN2v84base2OS17GetRandomMmapAddrEv:
.LFB4255:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$72, %rsp
	.cfi_offset 12, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movzbl	_ZN2v84base12_GLOBAL__N_1L9rng_mutexE(%rip), %eax
	cmpb	$2, %al
	jne	.L104
.L88:
	leaq	8+_ZN2v84base12_GLOBAL__N_1L9rng_mutexE(%rip), %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movzbl	_ZGVZN2v84base12_GLOBAL__N_132GetPlatformRandomNumberGeneratorEvE6object(%rip), %eax
	testb	%al, %al
	je	.L105
.L91:
	leaq	-72(%rbp), %rsi
	movl	$8, %edx
	leaq	_ZZN2v84base12_GLOBAL__N_132GetPlatformRandomNumberGeneratorEvE6object(%rip), %rdi
	call	_ZN2v84base21RandomNumberGenerator9NextBytesEPvm@PLT
	leaq	8+_ZN2v84base12_GLOBAL__N_1L9rng_mutexE(%rip), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movabsq	$70368744173568, %rax
	andq	-72(%rbp), %rax
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L106
	addq	$72, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L104:
	.cfi_restore_state
	leaq	_ZN2v84base16LazyInstanceImplINS0_5MutexENS0_32StaticallyAllocatedInstanceTraitIS2_EENS0_21DefaultConstructTraitIS2_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS2_EEE12InitInstanceEPv(%rip), %rax
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation(%rip), %rcx
	movq	%rax, -64(%rbp)
	movq	%rcx, %xmm0
	leaq	8+_ZN2v84base12_GLOBAL__N_1L9rng_mutexE(%rip), %rax
	leaq	-64(%rbp), %r12
	movq	%rax, -56(%rbp)
	leaq	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data(%rip), %rax
	movq	%r12, %rsi
	leaq	_ZN2v84base12_GLOBAL__N_1L9rng_mutexE(%rip), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v84base12CallOnceImplEPSt6atomicIhESt8functionIFvvEE@PLT
	movq	-48(%rbp), %rax
	testq	%rax, %rax
	je	.L88
	movl	$3, %edx
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	*%rax
	jmp	.L88
	.p2align 4,,10
	.p2align 3
.L105:
	leaq	_ZGVZN2v84base12_GLOBAL__N_132GetPlatformRandomNumberGeneratorEvE6object(%rip), %rdi
	call	__cxa_guard_acquire@PLT
	testl	%eax, %eax
	je	.L91
	leaq	_ZZN2v84base12_GLOBAL__N_132GetPlatformRandomNumberGeneratorEvE6object(%rip), %rdi
	call	_ZN2v84base21RandomNumberGeneratorC1Ev@PLT
	leaq	_ZGVZN2v84base12_GLOBAL__N_132GetPlatformRandomNumberGeneratorEvE6object(%rip), %rdi
	call	__cxa_guard_release@PLT
	jmp	.L91
.L106:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4255:
	.size	_ZN2v84base2OS17GetRandomMmapAddrEv, .-_ZN2v84base2OS17GetRandomMmapAddrEv
	.section	.rodata._ZN2v84base2OS8AllocateEPvmmNS1_16MemoryPermissionE.str1.1,"aMS",@progbits,1
.LC6:
	.string	"unreachable code"
.LC7:
	.string	"Free(base, prefix_size)"
	.section	.rodata._ZN2v84base2OS8AllocateEPvmmNS1_16MemoryPermissionE.str1.8,"aMS",@progbits,1
	.align 8
.LC8:
	.string	"Free(aligned_base + size, suffix_size)"
	.section	.text._ZN2v84base2OS8AllocateEPvmmNS1_16MemoryPermissionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base2OS8AllocateEPvmmNS1_16MemoryPermissionE
	.type	_ZN2v84base2OS8AllocateEPvmmNS1_16MemoryPermissionE, @function
_ZN2v84base2OS8AllocateEPvmmNS1_16MemoryPermissionE:
.LFB4256:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	movq	%r14, %r15
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$30, %edi
	negq	%r15
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movl	%ecx, -52(%rbp)
	call	sysconf@PLT
	movq	%r12, %r10
	movl	$30, %edi
	leaq	-1(%r13,%r14), %r12
	andq	%r15, %r10
	movq	%rax, %rbx
	movq	%r10, -64(%rbp)
	call	sysconf@PLT
	movl	-52(%rbp), %ecx
	addq	%rax, %r12
	negq	%rax
	subq	%rbx, %r12
	movq	%r12, %rbx
	andq	%rax, %rbx
	cmpl	$4, %ecx
	ja	.L108
	cmpl	$1, %ecx
	movq	-64(%rbp), %r10
	movl	%ecx, %edx
	leaq	CSWTCH.55(%rip), %rax
	sbbl	%ecx, %ecx
	movl	(%rax,%rdx,4), %edx
	xorl	%r9d, %r9d
	movl	$-1, %r8d
	andl	$16384, %ecx
	movq	%r10, %rdi
	movq	%rbx, %rsi
	addl	$34, %ecx
	call	mmap@PLT
	movq	%rax, %rdi
	leaq	-1(%rax), %rax
	cmpq	$-3, %rax
	ja	.L121
	leaq	-1(%rdi,%r14), %rax
	andq	%rax, %r15
	cmpq	%rdi, %r15
	je	.L112
	movq	%r15, %r12
	subq	%rdi, %r12
	movq	%r12, %rsi
	subq	%r12, %rbx
	call	munmap@PLT
	testl	%eax, %eax
	jne	.L122
.L112:
	cmpq	%r13, %rbx
	jne	.L123
.L107:
	addq	$24, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L123:
	.cfi_restore_state
	movq	%rbx, %rsi
	leaq	(%r15,%r13), %rdi
	subq	%r13, %rsi
	call	munmap@PLT
	testl	%eax, %eax
	je	.L107
	leaq	.LC8(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L121:
	xorl	%r15d, %r15d
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L122:
	leaq	.LC7(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L108:
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE4256:
	.size	_ZN2v84base2OS8AllocateEPvmmNS1_16MemoryPermissionE, .-_ZN2v84base2OS8AllocateEPvmmNS1_16MemoryPermissionE
	.section	.text._ZN2v84base2OS4FreeEPvm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base2OS4FreeEPvm
	.type	_ZN2v84base2OS4FreeEPvm, @function
_ZN2v84base2OS4FreeEPvm:
.LFB4257:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	munmap@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	testl	%eax, %eax
	sete	%al
	ret
	.cfi_endproc
.LFE4257:
	.size	_ZN2v84base2OS4FreeEPvm, .-_ZN2v84base2OS4FreeEPvm
	.section	.text._ZN2v84base2OS7ReleaseEPvm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base2OS7ReleaseEPvm
	.type	_ZN2v84base2OS7ReleaseEPvm, @function
_ZN2v84base2OS7ReleaseEPvm:
.LFB4989:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	munmap@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	testl	%eax, %eax
	sete	%al
	ret
	.cfi_endproc
.LFE4989:
	.size	_ZN2v84base2OS7ReleaseEPvm, .-_ZN2v84base2OS7ReleaseEPvm
	.section	.text._ZN2v84base2OS14SetPermissionsEPvmNS1_16MemoryPermissionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base2OS14SetPermissionsEPvmNS1_16MemoryPermissionE
	.type	_ZN2v84base2OS14SetPermissionsEPvmNS1_16MemoryPermissionE, @function
_ZN2v84base2OS14SetPermissionsEPvmNS1_16MemoryPermissionE:
.LFB4259:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	cmpl	$4, %edx
	ja	.L129
	movl	%edx, %ebx
	leaq	CSWTCH.55(%rip), %rax
	movl	%edx, %edx
	movq	%rdi, %r12
	movl	(%rax,%rdx,4), %edx
	movq	%rsi, %r13
	call	mprotect@PLT
	testl	%eax, %eax
	sete	%r14b
	orl	%eax, %ebx
	je	.L137
.L128:
	popq	%rbx
	movl	%r14d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L137:
	.cfi_restore_state
	movl	$8, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	madvise@PLT
	testl	%eax, %eax
	je	.L128
	call	__errno_location@PLT
	cmpl	$22, (%rax)
	jne	.L128
	movl	$4, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	madvise@PLT
	jmp	.L128
.L129:
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE4259:
	.size	_ZN2v84base2OS14SetPermissionsEPvmNS1_16MemoryPermissionE, .-_ZN2v84base2OS14SetPermissionsEPvmNS1_16MemoryPermissionE
	.section	.text._ZN2v84base2OS18DiscardSystemPagesEPvm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base2OS18DiscardSystemPagesEPvm
	.type	_ZN2v84base2OS18DiscardSystemPagesEPvm, @function
_ZN2v84base2OS18DiscardSystemPagesEPvm:
.LFB4260:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$8, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	$1, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$8, %rsp
	call	madvise@PLT
	testl	%eax, %eax
	je	.L138
	call	__errno_location@PLT
	movl	(%rax), %eax
	cmpl	$38, %eax
	je	.L138
	xorl	%r14d, %r14d
	cmpl	$22, %eax
	je	.L147
.L138:
	addq	$8, %rsp
	movl	%r14d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L147:
	.cfi_restore_state
	movl	$4, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	madvise@PLT
	testl	%eax, %eax
	sete	%r14b
	jmp	.L138
	.cfi_endproc
.LFE4260:
	.size	_ZN2v84base2OS18DiscardSystemPagesEPvm, .-_ZN2v84base2OS18DiscardSystemPagesEPvm
	.section	.text._ZN2v84base2OS14HasLazyCommitsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base2OS14HasLazyCommitsEv
	.type	_ZN2v84base2OS14HasLazyCommitsEv, @function
_ZN2v84base2OS14HasLazyCommitsEv:
.LFB4261:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE4261:
	.size	_ZN2v84base2OS14HasLazyCommitsEv, .-_ZN2v84base2OS14HasLazyCommitsEv
	.section	.text._ZN2v84base2OS17GetGCFakeMMapFileEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base2OS17GetGCFakeMMapFileEv
	.type	_ZN2v84base2OS17GetGCFakeMMapFileEv, @function
_ZN2v84base2OS17GetGCFakeMMapFileEv:
.LFB4262:
	.cfi_startproc
	endbr64
	movq	_ZN2v84base12_GLOBAL__N_114g_gc_fake_mmapE(%rip), %rax
	ret
	.cfi_endproc
.LFE4262:
	.size	_ZN2v84base2OS17GetGCFakeMMapFileEv, .-_ZN2v84base2OS17GetGCFakeMMapFileEv
	.section	.text._ZN2v84base2OS5SleepENS0_9TimeDeltaE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base2OS5SleepENS0_9TimeDeltaE
	.type	_ZN2v84base2OS5SleepENS0_9TimeDeltaE, @function
_ZN2v84base2OS5SleepENS0_9TimeDeltaE:
.LFB4263:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	leaq	-8(%rbp), %rdi
	call	_ZNK2v84base9TimeDelta14InMicrosecondsEv@PLT
	movq	%rax, %rdi
	call	usleep@PLT
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4263:
	.size	_ZN2v84base2OS5SleepENS0_9TimeDeltaE, .-_ZN2v84base2OS5SleepENS0_9TimeDeltaE
	.section	.text.unlikely._ZN2v84base2OS5AbortEv,"ax",@progbits
	.align 2
	.globl	_ZN2v84base2OS5AbortEv
	.type	_ZN2v84base2OS5AbortEv, @function
_ZN2v84base2OS5AbortEv:
.LFB4264:
	.cfi_startproc
	endbr64
	cmpb	$0, _ZN2v84base12_GLOBAL__N_112g_hard_abortE(%rip)
	je	.L153
	ud2
.L153:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	abort@PLT
	.cfi_endproc
.LFE4264:
	.size	_ZN2v84base2OS5AbortEv, .-_ZN2v84base2OS5AbortEv
	.section	.text._ZN2v84base2OS10DebugBreakEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base2OS10DebugBreakEv
	.type	_ZN2v84base2OS10DebugBreakEv, @function
_ZN2v84base2OS10DebugBreakEv:
.LFB4265:
	.cfi_startproc
	endbr64
#APP
# 427 "../deps/v8/src/base/platform/platform-posix.cc" 1
	int $3
# 0 "" 2
#NO_APP
	ret
	.cfi_endproc
.LFE4265:
	.size	_ZN2v84base2OS10DebugBreakEv, .-_ZN2v84base2OS10DebugBreakEv
	.section	.rodata._ZN2v84base2OS16MemoryMappedFile4openEPKcNS2_8FileModeE.str1.1,"aMS",@progbits,1
.LC9:
	.string	"r"
.LC10:
	.string	"r+"
	.section	.text._ZN2v84base2OS16MemoryMappedFile4openEPKcNS2_8FileModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base2OS16MemoryMappedFile4openEPKcNS2_8FileModeE
	.type	_ZN2v84base2OS16MemoryMappedFile4openEPKcNS2_8FileModeE, @function
_ZN2v84base2OS16MemoryMappedFile4openEPKcNS2_8FileModeE:
.LFB4278:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC10(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testl	%esi, %esi
	leaq	.LC9(%rip), %rsi
	cmovne	%rax, %rsi
	call	fopen@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L157
	xorl	%esi, %esi
	movl	$2, %edx
	movq	%rax, %rdi
	call	fseek@PLT
	testl	%eax, %eax
	jne	.L160
	movq	%r12, %rdi
	call	ftell@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L172
	jle	.L160
	movl	$3, %r15d
	cmpl	$1, %r13d
	je	.L162
	movl	$2, %r13d
	movl	$1, %r15d
.L162:
	movq	%r12, %rdi
	call	fileno@PLT
	movl	%eax, %r14d
	call	_ZN2v84base2OS17GetRandomMmapAddrEv
	movl	%r13d, %ecx
	xorl	%r9d, %r9d
	movl	%r14d, %r8d
	movq	%rax, %rdi
	movl	%r15d, %edx
	movq	%rbx, %rsi
	call	mmap@PLT
	movq	%rax, %r13
	cmpq	$-1, %rax
	je	.L160
	movl	$32, %edi
	call	_Znwm@PLT
	leaq	16+_ZTVN2v84base21PosixMemoryMappedFileE(%rip), %rcx
	movq	%rcx, (%rax)
	movq	%r13, 16(%rax)
	movq	%rbx, 24(%rax)
	movq	%r12, 8(%rax)
	movq	%rax, %r12
.L157:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L160:
	.cfi_restore_state
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	fclose@PLT
	jmp	.L157
	.p2align 4,,10
	.p2align 3
.L172:
	movl	$32, %edi
	call	_Znwm@PLT
	leaq	16+_ZTVN2v84base21PosixMemoryMappedFileE(%rip), %rcx
	movq	%r12, 8(%rax)
	movq	%rax, %r12
	movq	%rcx, (%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	jmp	.L157
	.cfi_endproc
.LFE4278:
	.size	_ZN2v84base2OS16MemoryMappedFile4openEPKcNS2_8FileModeE, .-_ZN2v84base2OS16MemoryMappedFile4openEPKcNS2_8FileModeE
	.section	.rodata._ZN2v84base2OS16MemoryMappedFile6createEPKcmPv.str1.1,"aMS",@progbits,1
.LC11:
	.string	"w+"
	.section	.text._ZN2v84base2OS16MemoryMappedFile6createEPKcmPv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base2OS16MemoryMappedFile6createEPKcmPv
	.type	_ZN2v84base2OS16MemoryMappedFile6createEPKcmPv, @function
_ZN2v84base2OS16MemoryMappedFile6createEPKcmPv:
.LFB4279:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	leaq	.LC11(%rip), %rsi
	subq	$8, %rsp
	call	fopen@PLT
	testq	%rax, %rax
	je	.L177
	movq	%rax, %r12
	testq	%rbx, %rbx
	jne	.L175
	movl	$32, %edi
	call	_Znwm@PLT
	leaq	16+_ZTVN2v84base21PosixMemoryMappedFileE(%rip), %rcx
	movq	%rcx, (%rax)
	movq	%r12, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
.L173:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L175:
	.cfi_restore_state
	movq	%rax, %rcx
	movq	%rbx, %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	call	fwrite@PLT
	cmpq	%rax, %rbx
	jne	.L176
	movq	%r12, %rdi
	call	ferror@PLT
	testl	%eax, %eax
	jne	.L176
	movq	%r12, %rdi
	call	fileno@PLT
	movl	%eax, %r13d
	call	_ZN2v84base2OS17GetRandomMmapAddrEv
	movl	%r13d, %r8d
	xorl	%r9d, %r9d
	movl	$1, %ecx
	movq	%rax, %rdi
	movl	$3, %edx
	movq	%rbx, %rsi
	call	mmap@PLT
	movq	%rax, %r13
	cmpq	$-1, %rax
	je	.L176
	movl	$32, %edi
	call	_Znwm@PLT
	leaq	16+_ZTVN2v84base21PosixMemoryMappedFileE(%rip), %rcx
	movq	%rcx, (%rax)
	movq	%r12, 8(%rax)
	movq	%r13, 16(%rax)
	movq	%rbx, 24(%rax)
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L176:
	movq	%r12, %rdi
	call	fclose@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L177:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4279:
	.size	_ZN2v84base2OS16MemoryMappedFile6createEPKcmPv, .-_ZN2v84base2OS16MemoryMappedFile6createEPKcmPv
	.section	.text._ZN2v84base2OS19GetCurrentProcessIdEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base2OS19GetCurrentProcessIdEv
	.type	_ZN2v84base2OS19GetCurrentProcessIdEv, @function
_ZN2v84base2OS19GetCurrentProcessIdEv:
.LFB4284:
	.cfi_startproc
	endbr64
	jmp	getpid@PLT
	.cfi_endproc
.LFE4284:
	.size	_ZN2v84base2OS19GetCurrentProcessIdEv, .-_ZN2v84base2OS19GetCurrentProcessIdEv
	.section	.text._ZN2v84base2OS18GetCurrentThreadIdEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base2OS18GetCurrentThreadIdEv
	.type	_ZN2v84base2OS18GetCurrentThreadIdEv, @function
_ZN2v84base2OS18GetCurrentThreadIdEv:
.LFB4285:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$186, %edi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	syscall@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4285:
	.size	_ZN2v84base2OS18GetCurrentThreadIdEv, .-_ZN2v84base2OS18GetCurrentThreadIdEv
	.section	.text._ZN2v84base2OS11ExitProcessEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base2OS11ExitProcessEi
	.type	_ZN2v84base2OS11ExitProcessEi, @function
_ZN2v84base2OS11ExitProcessEi:
.LFB4286:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%edi, %r12d
	subq	$8, %rsp
	movq	stdout(%rip), %rdi
	call	fflush@PLT
	movq	stderr(%rip), %rdi
	call	fflush@PLT
	movl	%r12d, %edi
	call	_exit@PLT
	.cfi_endproc
.LFE4286:
	.size	_ZN2v84base2OS11ExitProcessEi, .-_ZN2v84base2OS11ExitProcessEi
	.section	.text._ZN2v84base2OS11GetUserTimeEPjS2_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base2OS11GetUserTimeEPjS2_
	.type	_ZN2v84base2OS11GetUserTimeEPjS2_, @function
_ZN2v84base2OS11GetUserTimeEPjS2_:
.LFB4287:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	xorl	%edi, %edi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	leaq	-176(%rbp), %rsi
	subq	$160, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	getrusage@PLT
	testl	%eax, %eax
	js	.L190
	movq	-176(%rbp), %rax
	movl	%eax, (%r12)
	movq	-168(%rbp), %rax
	movl	%eax, (%rbx)
	xorl	%eax, %eax
.L187:
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L192
	addq	$160, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L190:
	.cfi_restore_state
	movl	$-1, %eax
	jmp	.L187
.L192:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4287:
	.size	_ZN2v84base2OS11GetUserTimeEPjS2_, .-_ZN2v84base2OS11GetUserTimeEPjS2_
	.section	.text._ZN2v84base2OS17TimeCurrentMillisEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base2OS17TimeCurrentMillisEv
	.type	_ZN2v84base2OS17TimeCurrentMillisEv, @function
_ZN2v84base2OS17TimeCurrentMillisEv:
.LFB4288:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	call	_ZN2v84base4Time3NowEv@PLT
	leaq	-16(%rbp), %rdi
	movq	%rax, -16(%rbp)
	call	_ZNK2v84base4Time8ToJsTimeEv@PLT
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L196
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L196:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4288:
	.size	_ZN2v84base2OS17TimeCurrentMillisEv, .-_ZN2v84base2OS17TimeCurrentMillisEv
	.section	.text._ZN2v84base2OS12GetLastErrorEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base2OS12GetLastErrorEv
	.type	_ZN2v84base2OS12GetLastErrorEv, @function
_ZN2v84base2OS12GetLastErrorEv:
.LFB4290:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	__errno_location@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	movl	(%rax), %eax
	ret
	.cfi_endproc
.LFE4290:
	.size	_ZN2v84base2OS12GetLastErrorEv, .-_ZN2v84base2OS12GetLastErrorEv
	.section	.text._ZN2v84base2OS5FOpenEPKcS3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base2OS5FOpenEPKcS3_
	.type	_ZN2v84base2OS5FOpenEPKcS3_, @function
_ZN2v84base2OS5FOpenEPKcS3_:
.LFB4291:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$168, %rsp
	.cfi_offset 12, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	fopen@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L199
	movq	%rax, %rdi
	call	fileno@PLT
	leaq	-176(%rbp), %rdx
	movl	$1, %edi
	movl	%eax, %esi
	call	__fxstat@PLT
	testl	%eax, %eax
	jne	.L207
	testb	$-128, -151(%rbp)
	je	.L207
.L199:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L208
	addq	$168, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L207:
	.cfi_restore_state
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	fclose@PLT
	jmp	.L199
.L208:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4291:
	.size	_ZN2v84base2OS5FOpenEPKcS3_, .-_ZN2v84base2OS5FOpenEPKcS3_
	.section	.text._ZN2v84base2OS6RemoveEPKc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base2OS6RemoveEPKc
	.type	_ZN2v84base2OS6RemoveEPKc, @function
_ZN2v84base2OS6RemoveEPKc:
.LFB4292:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	remove@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	testl	%eax, %eax
	sete	%al
	ret
	.cfi_endproc
.LFE4292:
	.size	_ZN2v84base2OS6RemoveEPKc, .-_ZN2v84base2OS6RemoveEPKc
	.section	.text._ZN2v84base2OS18DirectorySeparatorEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base2OS18DirectorySeparatorEv
	.type	_ZN2v84base2OS18DirectorySeparatorEv, @function
_ZN2v84base2OS18DirectorySeparatorEv:
.LFB4293:
	.cfi_startproc
	endbr64
	movl	$47, %eax
	ret
	.cfi_endproc
.LFE4293:
	.size	_ZN2v84base2OS18DirectorySeparatorEv, .-_ZN2v84base2OS18DirectorySeparatorEv
	.section	.text._ZN2v84base2OS20isDirectorySeparatorEc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base2OS20isDirectorySeparatorEc
	.type	_ZN2v84base2OS20isDirectorySeparatorEc, @function
_ZN2v84base2OS20isDirectorySeparatorEc:
.LFB4294:
	.cfi_startproc
	endbr64
	cmpb	$47, %dil
	sete	%al
	ret
	.cfi_endproc
.LFE4294:
	.size	_ZN2v84base2OS20isDirectorySeparatorEc, .-_ZN2v84base2OS20isDirectorySeparatorEc
	.section	.text._ZN2v84base2OS17OpenTemporaryFileEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base2OS17OpenTemporaryFileEv
	.type	_ZN2v84base2OS17OpenTemporaryFileEv, @function
_ZN2v84base2OS17OpenTemporaryFileEv:
.LFB4295:
	.cfi_startproc
	endbr64
	jmp	tmpfile@PLT
	.cfi_endproc
.LFE4295:
	.size	_ZN2v84base2OS17OpenTemporaryFileEv, .-_ZN2v84base2OS17OpenTemporaryFileEv
	.section	.text._ZN2v84base2OS5PrintEPKcz,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base2OS5PrintEPKcz
	.type	_ZN2v84base2OS5PrintEPKcz, @function
_ZN2v84base2OS5PrintEPKcz:
.LFB4296:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r10
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$208, %rsp
	movq	%rsi, -168(%rbp)
	movq	%rdx, -160(%rbp)
	movq	%rcx, -152(%rbp)
	movq	%r8, -144(%rbp)
	movq	%r9, -136(%rbp)
	testb	%al, %al
	je	.L215
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm2, -96(%rbp)
	movaps	%xmm3, -80(%rbp)
	movaps	%xmm4, -64(%rbp)
	movaps	%xmm5, -48(%rbp)
	movaps	%xmm6, -32(%rbp)
	movaps	%xmm7, -16(%rbp)
.L215:
	movq	%fs:40, %rax
	movq	%rax, -184(%rbp)
	xorl	%eax, %eax
	leaq	16(%rbp), %rax
	movq	%r10, %rdx
	movl	$1, %esi
	movq	stdout(%rip), %rdi
	movq	%rax, -200(%rbp)
	leaq	-208(%rbp), %rcx
	leaq	-176(%rbp), %rax
	movl	$8, -208(%rbp)
	movl	$48, -204(%rbp)
	movq	%rax, -192(%rbp)
	call	__vfprintf_chk@PLT
	movq	-184(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L218
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L218:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4296:
	.size	_ZN2v84base2OS5PrintEPKcz, .-_ZN2v84base2OS5PrintEPKcz
	.section	.text._ZN2v84base2OS6VPrintEPKcP13__va_list_tag,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base2OS6VPrintEPKcP13__va_list_tag
	.type	_ZN2v84base2OS6VPrintEPKcP13__va_list_tag, @function
_ZN2v84base2OS6VPrintEPKcP13__va_list_tag:
.LFB4297:
	.cfi_startproc
	endbr64
	movq	%rdi, %rdx
	movq	stdout(%rip), %rdi
	movq	%rsi, %rcx
	movl	$1, %esi
	jmp	__vfprintf_chk@PLT
	.cfi_endproc
.LFE4297:
	.size	_ZN2v84base2OS6VPrintEPKcP13__va_list_tag, .-_ZN2v84base2OS6VPrintEPKcP13__va_list_tag
	.section	.text._ZN2v84base2OS6FPrintEP8_IO_FILEPKcz,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base2OS6FPrintEP8_IO_FILEPKcz
	.type	_ZN2v84base2OS6FPrintEP8_IO_FILEPKcz, @function
_ZN2v84base2OS6FPrintEP8_IO_FILEPKcz:
.LFB4298:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r10
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$208, %rsp
	movq	%rdx, -160(%rbp)
	movq	%rcx, -152(%rbp)
	movq	%r8, -144(%rbp)
	movq	%r9, -136(%rbp)
	testb	%al, %al
	je	.L221
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm2, -96(%rbp)
	movaps	%xmm3, -80(%rbp)
	movaps	%xmm4, -64(%rbp)
	movaps	%xmm5, -48(%rbp)
	movaps	%xmm6, -32(%rbp)
	movaps	%xmm7, -16(%rbp)
.L221:
	movq	%fs:40, %rax
	movq	%rax, -184(%rbp)
	xorl	%eax, %eax
	leaq	16(%rbp), %rax
	movq	%r10, %rdx
	leaq	-208(%rbp), %rcx
	movq	%rax, -200(%rbp)
	movl	$1, %esi
	leaq	-176(%rbp), %rax
	movl	$16, -208(%rbp)
	movl	$48, -204(%rbp)
	movq	%rax, -192(%rbp)
	call	__vfprintf_chk@PLT
	movq	-184(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L224
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L224:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4298:
	.size	_ZN2v84base2OS6FPrintEP8_IO_FILEPKcz, .-_ZN2v84base2OS6FPrintEP8_IO_FILEPKcz
	.section	.text._ZN2v84base2OS7VFPrintEP8_IO_FILEPKcP13__va_list_tag,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base2OS7VFPrintEP8_IO_FILEPKcP13__va_list_tag
	.type	_ZN2v84base2OS7VFPrintEP8_IO_FILEPKcP13__va_list_tag, @function
_ZN2v84base2OS7VFPrintEP8_IO_FILEPKcP13__va_list_tag:
.LFB4299:
	.cfi_startproc
	endbr64
	movq	%rdx, %rcx
	movq	%rsi, %rdx
	movl	$1, %esi
	jmp	__vfprintf_chk@PLT
	.cfi_endproc
.LFE4299:
	.size	_ZN2v84base2OS7VFPrintEP8_IO_FILEPKcP13__va_list_tag, .-_ZN2v84base2OS7VFPrintEP8_IO_FILEPKcP13__va_list_tag
	.section	.text._ZN2v84base2OS10PrintErrorEPKcz,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base2OS10PrintErrorEPKcz
	.type	_ZN2v84base2OS10PrintErrorEPKcz, @function
_ZN2v84base2OS10PrintErrorEPKcz:
.LFB4300:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r10
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$208, %rsp
	movq	%rsi, -168(%rbp)
	movq	%rdx, -160(%rbp)
	movq	%rcx, -152(%rbp)
	movq	%r8, -144(%rbp)
	movq	%r9, -136(%rbp)
	testb	%al, %al
	je	.L227
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm2, -96(%rbp)
	movaps	%xmm3, -80(%rbp)
	movaps	%xmm4, -64(%rbp)
	movaps	%xmm5, -48(%rbp)
	movaps	%xmm6, -32(%rbp)
	movaps	%xmm7, -16(%rbp)
.L227:
	movq	%fs:40, %rax
	movq	%rax, -184(%rbp)
	xorl	%eax, %eax
	leaq	16(%rbp), %rax
	movq	%r10, %rdx
	movl	$1, %esi
	movq	stderr(%rip), %rdi
	movq	%rax, -200(%rbp)
	leaq	-208(%rbp), %rcx
	leaq	-176(%rbp), %rax
	movl	$8, -208(%rbp)
	movl	$48, -204(%rbp)
	movq	%rax, -192(%rbp)
	call	__vfprintf_chk@PLT
	movq	-184(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L230
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L230:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4300:
	.size	_ZN2v84base2OS10PrintErrorEPKcz, .-_ZN2v84base2OS10PrintErrorEPKcz
	.section	.text._ZN2v84base2OS11VPrintErrorEPKcP13__va_list_tag,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base2OS11VPrintErrorEPKcP13__va_list_tag
	.type	_ZN2v84base2OS11VPrintErrorEPKcP13__va_list_tag, @function
_ZN2v84base2OS11VPrintErrorEPKcP13__va_list_tag:
.LFB4301:
	.cfi_startproc
	endbr64
	movq	%rdi, %rdx
	movq	stderr(%rip), %rdi
	movq	%rsi, %rcx
	movl	$1, %esi
	jmp	__vfprintf_chk@PLT
	.cfi_endproc
.LFE4301:
	.size	_ZN2v84base2OS11VPrintErrorEPKcP13__va_list_tag, .-_ZN2v84base2OS11VPrintErrorEPKcP13__va_list_tag
	.section	.text._ZN2v84base2OS8SNPrintFEPciPKcz,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base2OS8SNPrintFEPciPKcz
	.type	_ZN2v84base2OS8SNPrintFEPciPKcz, @function
_ZN2v84base2OS8SNPrintFEPciPKcz:
.LFB4302:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movl	%esi, %ebx
	subq	$216, %rsp
	movq	%rcx, -184(%rbp)
	movq	%r8, -176(%rbp)
	movq	%r9, -168(%rbp)
	testb	%al, %al
	je	.L233
	movaps	%xmm0, -160(%rbp)
	movaps	%xmm1, -144(%rbp)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm3, -112(%rbp)
	movaps	%xmm4, -96(%rbp)
	movaps	%xmm5, -80(%rbp)
	movaps	%xmm6, -64(%rbp)
	movaps	%xmm7, -48(%rbp)
.L233:
	movq	%fs:40, %rax
	movq	%rax, -216(%rbp)
	xorl	%eax, %eax
	movslq	%ebx, %r13
	leaq	16(%rbp), %rax
	movq	%rdx, %r8
	movq	%rax, -232(%rbp)
	leaq	-240(%rbp), %r9
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	-208(%rbp), %rax
	movl	$1, %edx
	movq	$-1, %rcx
	movl	$24, -240(%rbp)
	movl	$48, -236(%rbp)
	movq	%rax, -224(%rbp)
	call	__vsnprintf_chk@PLT
	testl	%eax, %eax
	js	.L238
	cmpl	%eax, %ebx
	jle	.L238
.L232:
	movq	-216(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L243
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L238:
	.cfi_restore_state
	movl	$-1, %eax
	testl	%ebx, %ebx
	jle	.L232
	movb	$0, -1(%r12,%r13)
	jmp	.L232
.L243:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4302:
	.size	_ZN2v84base2OS8SNPrintFEPciPKcz, .-_ZN2v84base2OS8SNPrintFEPciPKcz
	.section	.text._ZN2v84base2OS9VSNPrintFEPciPKcP13__va_list_tag,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base2OS9VSNPrintFEPciPKcP13__va_list_tag
	.type	_ZN2v84base2OS9VSNPrintFEPciPKcP13__va_list_tag, @function
_ZN2v84base2OS9VSNPrintFEPciPKcP13__va_list_tag:
.LFB4303:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r8
	movq	%rcx, %r9
	movl	$1, %edx
	movq	$-1, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movslq	%esi, %r13
	pushq	%r12
	movq	%r13, %rsi
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%r13, %rbx
	subq	$8, %rsp
	call	__vsnprintf_chk@PLT
	testl	%eax, %eax
	js	.L248
	cmpl	%eax, %r13d
	jle	.L248
.L244:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L248:
	.cfi_restore_state
	movl	$-1, %eax
	testl	%ebx, %ebx
	jle	.L244
	movb	$0, -1(%r12,%r13)
	jmp	.L244
	.cfi_endproc
.LFE4303:
	.size	_ZN2v84base2OS9VSNPrintFEPciPKcP13__va_list_tag, .-_ZN2v84base2OS9VSNPrintFEPciPKcP13__va_list_tag
	.section	.text._ZN2v84base2OS7StrNCpyEPciPKcm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base2OS7StrNCpyEPciPKcm
	.type	_ZN2v84base2OS7StrNCpyEPciPKcm, @function
_ZN2v84base2OS7StrNCpyEPciPKcm:
.LFB4304:
	.cfi_startproc
	endbr64
	movq	%rdx, %rsi
	movq	%rcx, %rdx
	jmp	strncpy@PLT
	.cfi_endproc
.LFE4304:
	.size	_ZN2v84base2OS7StrNCpyEPciPKcm, .-_ZN2v84base2OS7StrNCpyEPciPKcm
	.section	.text._ZN2v84base6ThreadC2ERKNS1_7OptionsE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base6ThreadC2ERKNS1_7OptionsE
	.type	_ZN2v84base6ThreadC2ERKNS1_7OptionsE, @function
_ZN2v84base6ThreadC2ERKNS1_7OptionsE:
.LFB4309:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v84base6ThreadE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movl	$48, %edi
	call	_Znwm@PLT
	movq	$0, (%rax)
	leaq	8(%rax), %rdi
	movq	%rax, %r12
	call	_ZN2v84base5MutexC1Ev@PLT
	movl	8(%r13), %eax
	movq	%r12, 8(%rbx)
	leaq	16(%rbx), %rdi
	movq	$0, 40(%rbx)
	movq	0(%r13), %rsi
	leal	-1(%rax), %edx
	cmpl	$16382, %edx
	movl	$16384, %edx
	cmovbe	%edx, %eax
	movl	$16, %edx
	movl	%eax, 32(%rbx)
	call	strncpy@PLT
	movb	$0, 31(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4309:
	.size	_ZN2v84base6ThreadC2ERKNS1_7OptionsE, .-_ZN2v84base6ThreadC2ERKNS1_7OptionsE
	.globl	_ZN2v84base6ThreadC1ERKNS1_7OptionsE
	.set	_ZN2v84base6ThreadC1ERKNS1_7OptionsE,_ZN2v84base6ThreadC2ERKNS1_7OptionsE
	.section	.text._ZN2v84base6ThreadD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base6ThreadD2Ev
	.type	_ZN2v84base6ThreadD2Ev, @function
_ZN2v84base6ThreadD2Ev:
.LFB4315:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v84base6ThreadE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	8(%rdi), %r12
	movq	%rax, (%rdi)
	testq	%r12, %r12
	je	.L257
	leaq	8(%r12), %rdi
	call	_ZN2v84base5MutexD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$48, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L257:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4315:
	.size	_ZN2v84base6ThreadD2Ev, .-_ZN2v84base6ThreadD2Ev
	.globl	_ZN2v84base6ThreadD1Ev
	.set	_ZN2v84base6ThreadD1Ev,_ZN2v84base6ThreadD2Ev
	.section	.text._ZN2v84base6ThreadD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base6ThreadD0Ev
	.type	_ZN2v84base6ThreadD0Ev, @function
_ZN2v84base6ThreadD0Ev:
.LFB4317:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v84base6ThreadE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	8(%rdi), %r13
	movq	%rdi, %r12
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L261
	leaq	8(%r13), %rdi
	call	_ZN2v84base5MutexD1Ev@PLT
	movl	$48, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L261:
	movq	%r12, %rdi
	movl	$48, %esi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE4317:
	.size	_ZN2v84base6ThreadD0Ev, .-_ZN2v84base6ThreadD0Ev
	.section	.text._ZN2v84base6Thread8set_nameEPKc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base6Thread8set_nameEPKc
	.type	_ZN2v84base6Thread8set_nameEPKc, @function
_ZN2v84base6Thread8set_nameEPKc:
.LFB4320:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$16, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	16(%rdi), %rdi
	subq	$8, %rsp
	call	strncpy@PLT
	movb	$0, 31(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4320:
	.size	_ZN2v84base6Thread8set_nameEPKc, .-_ZN2v84base6Thread8set_nameEPKc
	.section	.text._ZN2v84base6Thread5StartEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base6Thread5StartEv
	.type	_ZN2v84base6Thread5StartEv, @function
_ZN2v84base6Thread5StartEv:
.LFB4321:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	xorl	%r13d, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-96(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -48(%rbp)
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	movaps	%xmm0, -64(%rbp)
	call	pthread_attr_init@PLT
	testl	%eax, %eax
	jne	.L268
	movslq	32(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L270
	movq	%r12, %rdi
	call	pthread_attr_setstacksize@PLT
	testl	%eax, %eax
	jne	.L282
.L270:
	movq	8(%rbx), %rax
	leaq	8(%rax), %r13
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	8(%rbx), %rdi
	movq	%rbx, %rcx
	movq	%r12, %rsi
	leaq	_ZN2v84baseL11ThreadEntryEPv(%rip), %rdx
	call	pthread_create@PLT
	testl	%eax, %eax
	jne	.L271
	movq	8(%rbx), %rax
	cmpq	$0, (%rax)
	jne	.L272
.L271:
	movq	%r12, %rdi
	call	pthread_attr_destroy@PLT
	movq	%r13, %rdi
	xorl	%r13d, %r13d
	call	_ZN2v84base5Mutex6UnlockEv@PLT
.L268:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L283
	addq	$72, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L282:
	.cfi_restore_state
	movq	%r12, %rdi
	call	pthread_attr_destroy@PLT
	jmp	.L268
	.p2align 4,,10
	.p2align 3
.L272:
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	%r12, %rdi
	call	pthread_attr_destroy@PLT
	testl	%eax, %eax
	sete	%r13b
	jmp	.L268
.L283:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4321:
	.size	_ZN2v84base6Thread5StartEv, .-_ZN2v84base6Thread5StartEv
	.section	.text._ZN2v84base6Thread4JoinEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base6Thread4JoinEv
	.type	_ZN2v84base6Thread4JoinEv, @function
_ZN2v84base6Thread4JoinEv:
.LFB4322:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	xorl	%esi, %esi
	movq	(%rax), %rdi
	jmp	pthread_join@PLT
	.cfi_endproc
.LFE4322:
	.size	_ZN2v84base6Thread4JoinEv, .-_ZN2v84base6Thread4JoinEv
	.section	.text._ZN2v84base6Thread20CreateThreadLocalKeyEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base6Thread20CreateThreadLocalKeyEv
	.type	_ZN2v84base6Thread20CreateThreadLocalKeyEv, @function
_ZN2v84base6Thread20CreateThreadLocalKeyEv:
.LFB4325:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-12(%rbp), %rdi
	call	pthread_key_create@PLT
	movl	-12(%rbp), %eax
	movq	-8(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L288
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L288:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4325:
	.size	_ZN2v84base6Thread20CreateThreadLocalKeyEv, .-_ZN2v84base6Thread20CreateThreadLocalKeyEv
	.section	.text._ZN2v84base6Thread20DeleteThreadLocalKeyEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base6Thread20DeleteThreadLocalKeyEi
	.type	_ZN2v84base6Thread20DeleteThreadLocalKeyEi, @function
_ZN2v84base6Thread20DeleteThreadLocalKeyEi:
.LFB4326:
	.cfi_startproc
	endbr64
	jmp	pthread_key_delete@PLT
	.cfi_endproc
.LFE4326:
	.size	_ZN2v84base6Thread20DeleteThreadLocalKeyEi, .-_ZN2v84base6Thread20DeleteThreadLocalKeyEi
	.section	.text._ZN2v84base6Thread14GetThreadLocalEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base6Thread14GetThreadLocalEi
	.type	_ZN2v84base6Thread14GetThreadLocalEi, @function
_ZN2v84base6Thread14GetThreadLocalEi:
.LFB4327:
	.cfi_startproc
	endbr64
	jmp	pthread_getspecific@PLT
	.cfi_endproc
.LFE4327:
	.size	_ZN2v84base6Thread14GetThreadLocalEi, .-_ZN2v84base6Thread14GetThreadLocalEi
	.section	.text._ZN2v84base6Thread14SetThreadLocalEiPv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base6Thread14SetThreadLocalEiPv
	.type	_ZN2v84base6Thread14SetThreadLocalEiPv, @function
_ZN2v84base6Thread14SetThreadLocalEiPv:
.LFB4328:
	.cfi_startproc
	endbr64
	jmp	pthread_setspecific@PLT
	.cfi_endproc
.LFE4328:
	.size	_ZN2v84base6Thread14SetThreadLocalEiPv, .-_ZN2v84base6Thread14SetThreadLocalEiPv
	.section	.rodata.CSWTCH.55,"a"
	.align 16
	.type	CSWTCH.55, @object
	.size	CSWTCH.55, 20
CSWTCH.55:
	.long	0
	.long	1
	.long	3
	.long	7
	.long	5
	.weak	_ZTVN2v84base21PosixMemoryMappedFileE
	.section	.data.rel.ro.local._ZTVN2v84base21PosixMemoryMappedFileE,"awG",@progbits,_ZTVN2v84base21PosixMemoryMappedFileE,comdat
	.align 8
	.type	_ZTVN2v84base21PosixMemoryMappedFileE, @object
	.size	_ZTVN2v84base21PosixMemoryMappedFileE, 48
_ZTVN2v84base21PosixMemoryMappedFileE:
	.quad	0
	.quad	0
	.quad	_ZN2v84base21PosixMemoryMappedFileD1Ev
	.quad	_ZN2v84base21PosixMemoryMappedFileD0Ev
	.quad	_ZNK2v84base21PosixMemoryMappedFile6memoryEv
	.quad	_ZNK2v84base21PosixMemoryMappedFile4sizeEv
	.weak	_ZTVN2v84base18PosixTimezoneCacheE
	.section	.data.rel.ro._ZTVN2v84base18PosixTimezoneCacheE,"awG",@progbits,_ZTVN2v84base18PosixTimezoneCacheE,comdat
	.align 8
	.type	_ZTVN2v84base18PosixTimezoneCacheE, @object
	.size	_ZTVN2v84base18PosixTimezoneCacheE, 64
_ZTVN2v84base18PosixTimezoneCacheE:
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	_ZN2v84base18PosixTimezoneCache21DaylightSavingsOffsetEd
	.quad	__cxa_pure_virtual
	.quad	_ZN2v84base18PosixTimezoneCache5ClearENS0_13TimezoneCache17TimeZoneDetectionE
	.quad	0
	.quad	0
	.weak	_ZTVN2v84base6ThreadE
	.section	.data.rel.ro._ZTVN2v84base6ThreadE,"awG",@progbits,_ZTVN2v84base6ThreadE,comdat
	.align 8
	.type	_ZTVN2v84base6ThreadE, @object
	.size	_ZTVN2v84base6ThreadE, 40
_ZTVN2v84base6ThreadE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.globl	_ZN2v84base2OS15LogFileOpenModeE
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC12:
	.string	"w"
	.section	.data.rel.ro.local._ZN2v84base2OS15LogFileOpenModeE,"aw"
	.align 8
	.type	_ZN2v84base2OS15LogFileOpenModeE, @object
	.size	_ZN2v84base2OS15LogFileOpenModeE, 8
_ZN2v84base2OS15LogFileOpenModeE:
	.quad	.LC12
	.section	.bss._ZGVZN2v84base2OS14CommitPageSizeEvE9page_size,"aw",@nobits
	.align 8
	.type	_ZGVZN2v84base2OS14CommitPageSizeEvE9page_size, @object
	.size	_ZGVZN2v84base2OS14CommitPageSizeEvE9page_size, 8
_ZGVZN2v84base2OS14CommitPageSizeEvE9page_size:
	.zero	8
	.section	.bss._ZZN2v84base2OS14CommitPageSizeEvE9page_size,"aw",@nobits
	.align 8
	.type	_ZZN2v84base2OS14CommitPageSizeEvE9page_size, @object
	.size	_ZZN2v84base2OS14CommitPageSizeEvE9page_size, 8
_ZZN2v84base2OS14CommitPageSizeEvE9page_size:
	.zero	8
	.section	.bss._ZN2v84base12_GLOBAL__N_1L9rng_mutexE,"aw",@nobits
	.align 32
	.type	_ZN2v84base12_GLOBAL__N_1L9rng_mutexE, @object
	.size	_ZN2v84base12_GLOBAL__N_1L9rng_mutexE, 48
_ZN2v84base12_GLOBAL__N_1L9rng_mutexE:
	.zero	48
	.section	.bss._ZGVZN2v84base12_GLOBAL__N_132GetPlatformRandomNumberGeneratorEvE6object,"aw",@nobits
	.align 8
	.type	_ZGVZN2v84base12_GLOBAL__N_132GetPlatformRandomNumberGeneratorEvE6object, @object
	.size	_ZGVZN2v84base12_GLOBAL__N_132GetPlatformRandomNumberGeneratorEvE6object, 8
_ZGVZN2v84base12_GLOBAL__N_132GetPlatformRandomNumberGeneratorEvE6object:
	.zero	8
	.section	.bss._ZZN2v84base12_GLOBAL__N_132GetPlatformRandomNumberGeneratorEvE6object,"aw",@nobits
	.align 16
	.type	_ZZN2v84base12_GLOBAL__N_132GetPlatformRandomNumberGeneratorEvE6object, @object
	.size	_ZZN2v84base12_GLOBAL__N_132GetPlatformRandomNumberGeneratorEvE6object, 24
_ZZN2v84base12_GLOBAL__N_132GetPlatformRandomNumberGeneratorEvE6object:
	.zero	24
	.section	.bss._ZN2v84base12_GLOBAL__N_114g_gc_fake_mmapE,"aw",@nobits
	.align 8
	.type	_ZN2v84base12_GLOBAL__N_114g_gc_fake_mmapE, @object
	.size	_ZN2v84base12_GLOBAL__N_114g_gc_fake_mmapE, 8
_ZN2v84base12_GLOBAL__N_114g_gc_fake_mmapE:
	.zero	8
	.section	.bss._ZN2v84base12_GLOBAL__N_112g_hard_abortE,"aw",@nobits
	.type	_ZN2v84base12_GLOBAL__N_112g_hard_abortE, @object
	.size	_ZN2v84base12_GLOBAL__N_112g_hard_abortE, 1
_ZN2v84base12_GLOBAL__N_112g_hard_abortE:
	.zero	1
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC0:
	.long	0
	.long	1095464768
	.align 8
.LC1:
	.long	0
	.long	2146959360
	.align 8
.LC3:
	.long	0
	.long	1083129856
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
