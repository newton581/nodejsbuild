	.file	"cpu.cc"
	.text
	.section	.text._ZN2v84base3CPUC2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base3CPUC2Ev
	.type	_ZN2v84base3CPUC2Ev, @function
_ZN2v84base3CPUC2Ev:
.LFB3042:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	movl	%esi, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movups	%xmm0, 16(%rdi)
	xorl	%eax, %eax
	movups	%xmm0, 32(%rdi)
	movdqa	.LC0(%rip), %xmm0
	movw	%ax, 88(%rdi)
	movl	%esi, %eax
	movups	%xmm0, 48(%rdi)
	pxor	%xmm0, %xmm0
	movq	$0, 80(%rdi)
	movb	$0, 90(%rdi)
	movups	%xmm0, 64(%rdi)
#APP
# 72 "../deps/v8/src/base/cpu.cc" 1
	cpuid 
	
# 0 "" 2
#NO_APP
	movl	%ebx, -44(%rbp)
	movl	%eax, %r9d
	movl	%edx, -40(%rbp)
	movq	-44(%rbp), %rax
	movl	%ecx, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	testl	%r9d, %r9d
	je	.L3
	movl	$1, %eax
	movl	%esi, %ecx
#APP
# 72 "../deps/v8/src/base/cpu.cc" 1
	cpuid 
	
# 0 "" 2
#NO_APP
	movl	%ecx, %esi
	movl	%eax, %r10d
	movl	%eax, %ecx
	movl	%eax, %r8d
	sarl	$12, %r10d
	andl	$15, %ecx
	sarl	$4, %r8d
	movb	%dl, 64(%rdi)
	movl	%ecx, 16(%rdi)
	movl	%r10d, %ecx
	andl	$15, %r8d
	andl	$3, %r10d
	andl	$240, %ecx
	movl	%r10d, 36(%rdi)
	movl	%eax, %r10d
	orl	%ecx, %r8d
	movl	%eax, %ecx
	sarl	$20, %eax
	movb	%sil, 70(%rdi)
	sarl	$8, %ecx
	andl	$255, %eax
	sarl	$16, %r10d
	andb	$1, 64(%rdi)
	movl	%eax, 32(%rdi)
	movl	%edx, %eax
	andl	$15, %ecx
	andl	$15, %r10d
	shrl	$15, %eax
	andb	$1, 70(%rdi)
	andl	$1, %eax
	movl	%r8d, 20(%rdi)
	movb	%al, 65(%rdi)
	movl	%edx, %eax
	shrl	$23, %eax
	movl	%ecx, 28(%rdi)
	andl	$1, %eax
	movl	%r10d, 24(%rdi)
	movb	%al, 67(%rdi)
	movl	%edx, %eax
	shrl	$26, %edx
	shrl	$25, %eax
	andl	$1, %edx
	andl	$1, %eax
	movb	%dl, 69(%rdi)
	movb	%al, 68(%rdi)
	movl	%esi, %eax
	shrl	$9, %eax
	andl	$1, %eax
	movb	%al, 71(%rdi)
	movl	%esi, %eax
	shrl	$19, %eax
	andl	$1, %eax
	movb	%al, 72(%rdi)
	movl	%esi, %eax
	shrl	$20, %eax
	andl	$1, %eax
	movb	%al, 73(%rdi)
	movl	%esi, %eax
	shrl	$23, %eax
	andl	$1, %eax
	movb	%al, 81(%rdi)
	movl	%esi, %eax
	shrl	$27, %eax
	andl	$1, %eax
	movb	%al, 75(%rdi)
	movl	%esi, %eax
	shrl	$12, %esi
	shrl	$28, %eax
	andl	$1, %esi
	andl	$1, %eax
	movb	%sil, 77(%rdi)
	movb	%al, 76(%rdi)
	cmpl	$6, %ecx
	je	.L15
.L4:
	cmpl	$6, %r9d
	jbe	.L3
	movl	$7, %eax
	xorl	%ecx, %ecx
#APP
# 72 "../deps/v8/src/base/cpu.cc" 1
	cpuid 
	
# 0 "" 2
#NO_APP
	movl	%ebx, %eax
	shrl	$8, %ebx
	shrl	$3, %eax
	andl	$1, %ebx
	andl	$1, %eax
	movb	%bl, 79(%rdi)
	movb	%al, 78(%rdi)
.L3:
	xorl	%esi, %esi
	movl	$-2147483648, %eax
	movl	%esi, %ecx
#APP
# 72 "../deps/v8/src/base/cpu.cc" 1
	cpuid 
	
# 0 "" 2
#NO_APP
	movl	%eax, -48(%rbp)
	movl	%eax, %r8d
	movl	%ebx, -44(%rbp)
	movl	%ecx, -40(%rbp)
	movl	%edx, -36(%rbp)
	cmpl	$-2147483648, %eax
	jbe	.L1
	movl	$-2147483647, %eax
	movl	%esi, %ecx
#APP
# 72 "../deps/v8/src/base/cpu.cc" 1
	cpuid 
	
# 0 "" 2
#NO_APP
	movl	%eax, -48(%rbp)
	movl	%ecx, %eax
	shrl	$5, %eax
	movb	%cl, 66(%rdi)
	andl	$1, %eax
	andb	$1, 66(%rdi)
	movl	%ebx, -44(%rbp)
	movl	%ecx, -40(%rbp)
	movl	%edx, -36(%rbp)
	movb	%al, 80(%rdi)
	cmpl	$-2147483642, %r8d
	jbe	.L1
	movl	$-2147483641, %eax
	movl	%esi, %ecx
#APP
# 72 "../deps/v8/src/base/cpu.cc" 1
	cpuid 
	
# 0 "" 2
#NO_APP
	movl	%edx, %esi
	movl	%eax, -48(%rbp)
	shrl	$8, %esi
	movl	%ebx, -44(%rbp)
	andl	$1, %esi
	movl	%ecx, -40(%rbp)
	movl	%edx, -36(%rbp)
	movb	%sil, 89(%rdi)
.L1:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L16
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	.cfi_restore_state
	cmpl	$77, %r8d
	jg	.L5
	cmpl	$27, %r8d
	jle	.L4
	leal	-28(%r8), %eax
	cmpl	$49, %eax
	ja	.L4
	leaq	.L7(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v84base3CPUC2Ev,"a",@progbits
	.align 4
	.align 4
.L7:
	.long	.L6-.L7
	.long	.L4-.L7
	.long	.L4-.L7
	.long	.L4-.L7
	.long	.L4-.L7
	.long	.L4-.L7
	.long	.L4-.L7
	.long	.L4-.L7
	.long	.L4-.L7
	.long	.L4-.L7
	.long	.L6-.L7
	.long	.L6-.L7
	.long	.L4-.L7
	.long	.L4-.L7
	.long	.L4-.L7
	.long	.L4-.L7
	.long	.L4-.L7
	.long	.L4-.L7
	.long	.L4-.L7
	.long	.L4-.L7
	.long	.L4-.L7
	.long	.L4-.L7
	.long	.L4-.L7
	.long	.L4-.L7
	.long	.L4-.L7
	.long	.L6-.L7
	.long	.L6-.L7
	.long	.L6-.L7
	.long	.L4-.L7
	.long	.L4-.L7
	.long	.L4-.L7
	.long	.L4-.L7
	.long	.L4-.L7
	.long	.L4-.L7
	.long	.L4-.L7
	.long	.L4-.L7
	.long	.L4-.L7
	.long	.L4-.L7
	.long	.L4-.L7
	.long	.L4-.L7
	.long	.L4-.L7
	.long	.L4-.L7
	.long	.L4-.L7
	.long	.L4-.L7
	.long	.L4-.L7
	.long	.L4-.L7
	.long	.L6-.L7
	.long	.L4-.L7
	.long	.L6-.L7
	.long	.L6-.L7
	.section	.text._ZN2v84base3CPUC2Ev
	.p2align 4,,10
	.p2align 3
.L5:
	cmpl	$110, %r8d
	jne	.L4
.L6:
	movb	$1, 74(%rdi)
	jmp	.L4
.L16:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3042:
	.size	_ZN2v84base3CPUC2Ev, .-_ZN2v84base3CPUC2Ev
	.globl	_ZN2v84base3CPUC1Ev
	.set	_ZN2v84base3CPUC1Ev,_ZN2v84base3CPUC2Ev
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	-1
	.long	0
	.long	0
	.long	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
