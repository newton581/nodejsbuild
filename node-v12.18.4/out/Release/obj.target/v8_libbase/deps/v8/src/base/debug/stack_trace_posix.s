	.file	"stack_trace_posix.cc"
	.text
	.section	.text._ZN2v84base5debug12_GLOBAL__N_128StreamBacktraceOutputHandlerD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v84base5debug12_GLOBAL__N_128StreamBacktraceOutputHandlerD2Ev, @function
_ZN2v84base5debug12_GLOBAL__N_128StreamBacktraceOutputHandlerD2Ev:
.LFB4065:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE4065:
	.size	_ZN2v84base5debug12_GLOBAL__N_128StreamBacktraceOutputHandlerD2Ev, .-_ZN2v84base5debug12_GLOBAL__N_128StreamBacktraceOutputHandlerD2Ev
	.set	_ZN2v84base5debug12_GLOBAL__N_128StreamBacktraceOutputHandlerD1Ev,_ZN2v84base5debug12_GLOBAL__N_128StreamBacktraceOutputHandlerD2Ev
	.section	.text._ZN2v84base5debug12_GLOBAL__N_127PrintBacktraceOutputHandlerD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v84base5debug12_GLOBAL__N_127PrintBacktraceOutputHandlerD2Ev, @function
_ZN2v84base5debug12_GLOBAL__N_127PrintBacktraceOutputHandlerD2Ev:
.LFB4069:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE4069:
	.size	_ZN2v84base5debug12_GLOBAL__N_127PrintBacktraceOutputHandlerD2Ev, .-_ZN2v84base5debug12_GLOBAL__N_127PrintBacktraceOutputHandlerD2Ev
	.set	_ZN2v84base5debug12_GLOBAL__N_127PrintBacktraceOutputHandlerD1Ev,_ZN2v84base5debug12_GLOBAL__N_127PrintBacktraceOutputHandlerD2Ev
	.section	.text._ZN2v84base5debug12_GLOBAL__N_127PrintBacktraceOutputHandlerD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v84base5debug12_GLOBAL__N_127PrintBacktraceOutputHandlerD0Ev, @function
_ZN2v84base5debug12_GLOBAL__N_127PrintBacktraceOutputHandlerD0Ev:
.LFB4071:
	.cfi_startproc
	endbr64
	movl	$8, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE4071:
	.size	_ZN2v84base5debug12_GLOBAL__N_127PrintBacktraceOutputHandlerD0Ev, .-_ZN2v84base5debug12_GLOBAL__N_127PrintBacktraceOutputHandlerD0Ev
	.section	.text._ZN2v84base5debug12_GLOBAL__N_128StreamBacktraceOutputHandlerD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v84base5debug12_GLOBAL__N_128StreamBacktraceOutputHandlerD0Ev, @function
_ZN2v84base5debug12_GLOBAL__N_128StreamBacktraceOutputHandlerD0Ev:
.LFB4067:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE4067:
	.size	_ZN2v84base5debug12_GLOBAL__N_128StreamBacktraceOutputHandlerD0Ev, .-_ZN2v84base5debug12_GLOBAL__N_128StreamBacktraceOutputHandlerD0Ev
	.section	.text._ZN2v84base5debug12_GLOBAL__N_128StreamBacktraceOutputHandler12HandleOutputEPKc,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v84base5debug12_GLOBAL__N_128StreamBacktraceOutputHandler12HandleOutputEPKc, @function
_ZN2v84base5debug12_GLOBAL__N_128StreamBacktraceOutputHandler12HandleOutputEPKc:
.LFB3467:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	8(%rdi), %r13
	testq	%rsi, %rsi
	je	.L9
	movq	%rsi, %rdi
	movq	%rsi, %r12
	call	strlen@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%r12
	movq	%rax, %rdx
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	.p2align 4,,10
	.p2align 3
.L9:
	.cfi_restore_state
	movq	0(%r13), %rax
	popq	%r12
	movq	-24(%rax), %rdi
	addq	%r13, %rdi
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	movl	32(%rdi), %esi
	orl	$1, %esi
	jmp	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	.cfi_endproc
.LFE3467:
	.size	_ZN2v84base5debug12_GLOBAL__N_128StreamBacktraceOutputHandler12HandleOutputEPKc, .-_ZN2v84base5debug12_GLOBAL__N_128StreamBacktraceOutputHandler12HandleOutputEPKc
	.section	.rodata._ZN2v84base5debug12_GLOBAL__N_115DemangleSymbolsEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"basic_string::substr"
	.section	.rodata._ZN2v84base5debug12_GLOBAL__N_115DemangleSymbolsEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"%s: __pos (which is %zu) > this->size() (which is %zu)"
	.align 8
.LC2:
	.string	"basic_string::_M_construct null not valid"
	.section	.rodata._ZN2v84base5debug12_GLOBAL__N_115DemangleSymbolsEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.str1.1
.LC3:
	.string	"basic_string::erase"
.LC4:
	.string	"basic_string::replace"
	.section	.text._ZN2v84base5debug12_GLOBAL__N_115DemangleSymbolsEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"ax",@progbits
	.p2align 4
	.type	_ZN2v84base5debug12_GLOBAL__N_115DemangleSymbolsEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN2v84base5debug12_GLOBAL__N_115DemangleSymbolsEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB3447:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 8(%rdi)
	je	.L10
	.p2align 4,,10
	.p2align 3
.L11:
	movq	%r15, %rdx
	movl	$2, %ecx
	leaq	_ZN2v84base5debug12_GLOBAL__N_1L20kMangledSymbolPrefixE(%rip), %rsi
	movq	%r13, %rdi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE4findEPKcmm@PLT
	movq	%rax, %r15
	cmpq	$-1, %rax
	je	.L10
	movl	$63, %ecx
	movq	%rax, %rdx
	leaq	_ZN2v84base5debug12_GLOBAL__N_1L17kSymbolCharactersE(%rip), %rsi
	movq	%r13, %rdi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE17find_first_not_ofEPKcmm@PLT
	movq	%rax, %rbx
	cmpq	$-1, %rax
	je	.L14
	movq	8(%r13), %r12
.L15:
	subq	%r15, %rbx
	cmpq	%r12, %r15
	ja	.L50
	movq	0(%r13), %r8
	subq	%r15, %r12
	movq	%r14, -96(%rbp)
	addq	%r15, %r8
	cmpq	%rbx, %r12
	cmova	%rbx, %r12
	movq	%r8, %rax
	addq	%r12, %rax
	je	.L17
	testq	%r8, %r8
	je	.L51
.L17:
	movq	%r12, -104(%rbp)
	cmpq	$15, %r12
	ja	.L52
	cmpq	$1, %r12
	jne	.L20
	movzbl	(%r8), %eax
	leaq	-104(%rbp), %rcx
	movb	%al, -80(%rbp)
	movq	%r14, %rax
.L21:
	movq	%r12, -88(%rbp)
	xorl	%edx, %edx
	xorl	%esi, %esi
	movb	$0, (%rax,%r12)
	movq	-96(%rbp), %rdi
	movl	$0, -104(%rbp)
	call	__cxa_demangle@PLT
	movq	%rax, %r12
	movl	-104(%rbp), %eax
	testl	%eax, %eax
	je	.L53
	addq	$2, %r15
	testq	%r12, %r12
	jne	.L28
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L30
.L58:
	call	_ZdlPv@PLT
	cmpq	%r15, 8(%r13)
	ja	.L11
.L10:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L54
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L53:
	.cfi_restore_state
	movq	8(%r13), %rdx
	cmpq	%rdx, %r15
	ja	.L55
	cmpq	$-1, %rbx
	je	.L56
	testq	%rbx, %rbx
	jne	.L57
	movq	%r12, %rdi
	call	strlen@PLT
	movq	%rax, %r8
.L27:
	movq	%r15, %rsi
	movq	%r12, %rcx
	xorl	%edx, %edx
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	%r12, %rdi
	call	strlen@PLT
	addq	%rax, %r15
.L28:
	movq	%r12, %rdi
	call	free@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	jne	.L58
.L30:
	cmpq	%r15, 8(%r13)
	ja	.L11
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L14:
	movq	8(%r13), %rbx
	movq	%rbx, %r12
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L20:
	testq	%r12, %r12
	jne	.L59
	movq	%r14, %rax
	leaq	-104(%rbp), %rcx
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L52:
	leaq	-104(%rbp), %rcx
	leaq	-96(%rbp), %rdi
	xorl	%edx, %edx
	movq	%r8, -128(%rbp)
	movq	%rcx, %rsi
	movq	%rcx, -120(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-120(%rbp), %rcx
	movq	-128(%rbp), %r8
	movq	%rax, -96(%rbp)
	movq	%rax, %rdi
	movq	-104(%rbp), %rax
	movq	%rax, -80(%rbp)
.L19:
	movq	%r12, %rdx
	movq	%r8, %rsi
	movq	%rcx, -120(%rbp)
	call	memcpy@PLT
	movq	-104(%rbp), %r12
	movq	-96(%rbp), %rax
	movq	-120(%rbp), %rcx
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L56:
	movq	0(%r13), %rax
	movq	%r15, 8(%r13)
	movb	$0, (%rax,%r15)
.L25:
	movq	%r12, %rdi
	call	strlen@PLT
	movq	8(%r13), %rcx
	movq	%rax, %r8
	cmpq	%rcx, %r15
	jbe	.L27
	movq	%r15, %rdx
	leaq	.LC4(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L57:
	subq	%r15, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	cmpq	%rbx, %rdx
	cmova	%rbx, %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE8_M_eraseEmm@PLT
	jmp	.L25
.L50:
	movq	%r12, %rcx
	movq	%r15, %rdx
	leaq	.LC0(%rip), %rsi
	xorl	%eax, %eax
	leaq	.LC1(%rip), %rdi
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L55:
	movq	%rdx, %rcx
	leaq	.LC3(%rip), %rsi
	movq	%r15, %rdx
	xorl	%eax, %eax
	leaq	.LC1(%rip), %rdi
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L51:
	leaq	.LC2(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L59:
	movq	%r14, %rdi
	leaq	-104(%rbp), %rcx
	jmp	.L19
.L54:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3447:
	.size	_ZN2v84base5debug12_GLOBAL__N_115DemangleSymbolsEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN2v84base5debug12_GLOBAL__N_115DemangleSymbolsEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.text._ZN2v84base5debug12_GLOBAL__N_127PrintBacktraceOutputHandler12HandleOutputEPKc,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v84base5debug12_GLOBAL__N_127PrintBacktraceOutputHandler12HandleOutputEPKc, @function
_ZN2v84base5debug12_GLOBAL__N_127PrintBacktraceOutputHandler12HandleOutputEPKc:
.LFB3456:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	subq	$8, %rsp
	call	strlen@PLT
	addq	$8, %rsp
	movq	%r12, %rsi
	movl	$2, %edi
	popq	%r12
	movq	%rax, %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	write@PLT
	.cfi_endproc
.LFE3456:
	.size	_ZN2v84base5debug12_GLOBAL__N_127PrintBacktraceOutputHandler12HandleOutputEPKc, .-_ZN2v84base5debug12_GLOBAL__N_127PrintBacktraceOutputHandler12HandleOutputEPKc
	.section	.rodata._ZN2v84base5debug12_GLOBAL__N_116ProcessBacktraceEPKPvmPNS2_22BacktraceOutputHandlerE.constprop.0.str1.1,"aMS",@progbits,1
.LC5:
	.string	"\n"
	.section	.rodata._ZN2v84base5debug12_GLOBAL__N_116ProcessBacktraceEPKPvmPNS2_22BacktraceOutputHandlerE.constprop.0.str1.8,"aMS",@progbits,1
	.align 8
.LC6:
	.string	"==== C stack trace ===============================\n"
	.section	.rodata._ZN2v84base5debug12_GLOBAL__N_116ProcessBacktraceEPKPvmPNS2_22BacktraceOutputHandlerE.constprop.0.str1.1
.LC7:
	.string	"    "
.LC8:
	.string	" ["
.LC9:
	.string	"0x"
.LC11:
	.string	"]\n"
.LC10:
	.string	"0123456789abcdef"
	.section	.text._ZN2v84base5debug12_GLOBAL__N_116ProcessBacktraceEPKPvmPNS2_22BacktraceOutputHandlerE.constprop.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v84base5debug12_GLOBAL__N_116ProcessBacktraceEPKPvmPNS2_22BacktraceOutputHandlerE.constprop.0, @function
_ZN2v84base5debug12_GLOBAL__N_116ProcessBacktraceEPKPvmPNS2_22BacktraceOutputHandlerE.constprop.0:
.LFB4098:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	leaq	.LC5(%rip), %rsi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movl	$2, %edi
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	write@PLT
	movl	$51, %edx
	movl	$2, %edi
	leaq	.LC6(%rip), %rsi
	call	write@PLT
	movl	$1, %edx
	movl	$2, %edi
	leaq	.LC5(%rip), %rsi
	call	write@PLT
	movl	_ZN2v84base5debug12_GLOBAL__N_117in_signal_handlerE(%rip), %eax
	testl	%eax, %eax
	je	.L63
.L66:
	testq	%r12, %r12
	je	.L62
	leaq	(%rbx,%r12,8), %r13
	leaq	.LC10(%rip), %r15
	leaq	-80(%rbp), %r12
	.p2align 4,,10
	.p2align 3
.L81:
	movl	$2, %edx
	leaq	.LC8(%rip), %rsi
	movl	$2, %edi
	call	write@PLT
	movq	(%rbx), %r14
	pxor	%xmm0, %xmm0
	movb	$0, -64(%rbp)
	movl	$2, %edx
	leaq	.LC9(%rip), %rsi
	movl	$2, %edi
	movaps	%xmm0, -80(%rbp)
	call	write@PLT
	movq	%r14, %rax
	andl	$15, %eax
	movzbl	(%r15,%rax), %eax
	movb	%al, -80(%rbp)
	movq	%r14, %rax
	shrq	$4, %rax
	andl	$15, %eax
	movzbl	(%r15,%rax), %eax
	movb	%al, -79(%rbp)
	movq	%r14, %rax
	shrq	$8, %rax
	andl	$15, %eax
	movzbl	(%r15,%rax), %eax
	movb	%al, -78(%rbp)
	movq	%r14, %rax
	shrq	$12, %rax
	andl	$15, %eax
	movzbl	(%r15,%rax), %eax
	movb	%al, -77(%rbp)
	movq	%r14, %rax
	shrq	$16, %rax
	andl	$15, %eax
	movzbl	(%r15,%rax), %eax
	movb	%al, -76(%rbp)
	movq	%r14, %rax
	shrq	$20, %rax
	andl	$15, %eax
	movzbl	(%r15,%rax), %eax
	movb	%al, -75(%rbp)
	movq	%r14, %rax
	shrq	$24, %rax
	andl	$15, %eax
	movzbl	(%r15,%rax), %eax
	movb	%al, -74(%rbp)
	movq	%r14, %rax
	shrq	$28, %rax
	andl	$15, %eax
	movzbl	(%r15,%rax), %eax
	movb	%al, -73(%rbp)
	movq	%r14, %rax
	shrq	$32, %rax
	andl	$15, %eax
	movzbl	(%r15,%rax), %eax
	movb	%al, -72(%rbp)
	movq	%r14, %rax
	shrq	$36, %rax
	andl	$15, %eax
	movzbl	(%r15,%rax), %eax
	movb	%al, -71(%rbp)
	movq	%r14, %rax
	shrq	$40, %rax
	andl	$15, %eax
	movzbl	(%r15,%rax), %eax
	movb	%al, -70(%rbp)
	movq	%r14, %rax
	shrq	$44, %rax
	andl	$15, %eax
	movzbl	(%r15,%rax), %eax
	movb	%al, -69(%rbp)
	movabsq	$281474976710655, %rax
	cmpq	%rax, %r14
	jbe	.L84
	movabsq	$4503599627370495, %rsi
	movq	%r14, %rax
	shrq	$48, %rax
	andl	$15, %eax
	movzbl	(%r15,%rax), %eax
	movb	%al, -68(%rbp)
	movq	%r14, %rax
	shrq	$52, %rax
	cmpq	%rsi, %r14
	jbe	.L85
	movabsq	$72057594037927935, %rdi
	andl	$15, %eax
	movzbl	(%r15,%rax), %eax
	movb	%al, -67(%rbp)
	movq	%r14, %rax
	shrq	$56, %rax
	cmpq	%rdi, %r14
	jbe	.L86
	movabsq	$1152921504606846975, %rsi
	andl	$15, %eax
	movzbl	(%r15,%rax), %eax
	movb	%al, -66(%rbp)
	movq	%r14, %rax
	shrq	$60, %rax
	cmpq	%rsi, %r14
	jbe	.L87
	movzbl	(%r15,%rax), %eax
	movb	%al, -65(%rbp)
	leaq	-64(%rbp), %rax
.L76:
	movb	$0, (%rax)
	movq	%r12, %rdx
	subq	$1, %rax
	.p2align 4,,10
	.p2align 3
.L77:
	movzbl	(%rdx), %esi
	movzbl	(%rax), %ecx
	addq	$1, %rdx
	subq	$1, %rax
	movb	%sil, 1(%rax)
	movb	%cl, -1(%rdx)
	cmpq	%rdx, %rax
	ja	.L77
	movq	%r12, %rdx
.L78:
	movl	(%rdx), %ecx
	addq	$4, %rdx
	leal	-16843009(%rcx), %eax
	notl	%ecx
	andl	%ecx, %eax
	andl	$-2139062144, %eax
	je	.L78
	movl	%eax, %ecx
	movq	%r12, %rsi
	shrl	$16, %ecx
	testl	$32896, %eax
	cmove	%ecx, %eax
	leaq	2(%rdx), %rcx
	cmove	%rcx, %rdx
	movl	%eax, %edi
	addb	%al, %dil
	movl	$2, %edi
	sbbq	$3, %rdx
	addq	$8, %rbx
	subq	%r12, %rdx
	call	write@PLT
	movl	$2, %edx
	movl	$2, %edi
	leaq	.LC11(%rip), %rsi
	call	write@PLT
	cmpq	%r13, %rbx
	jne	.L81
.L62:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L98
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L84:
	.cfi_restore_state
	leaq	-68(%rbp), %rax
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L63:
	movl	%r12d, %esi
	movq	%rbx, %rdi
	call	backtrace_symbols@PLT
	movq	%rax, -144(%rbp)
	testq	%rax, %rax
	je	.L66
	testq	%r12, %r12
	je	.L67
	leaq	-120(%rbp), %rax
	xorl	%ebx, %ebx
	leaq	-112(%rbp), %r15
	movq	%rax, -152(%rbp)
	leaq	-96(%rbp), %r13
	.p2align 4,,10
	.p2align 3
.L75:
	movq	-144(%rbp), %rax
	movq	(%rax,%rbx,8), %r14
	movq	%r13, -112(%rbp)
	testq	%r14, %r14
	je	.L99
	movq	%r14, %rdi
	call	strlen@PLT
	movq	%rax, -120(%rbp)
	movq	%rax, %r8
	cmpq	$15, %rax
	ja	.L100
	cmpq	$1, %rax
	jne	.L71
	movzbl	(%r14), %edx
	movb	%dl, -96(%rbp)
	movq	%r13, %rdx
.L72:
	movq	%rax, -104(%rbp)
	movq	%r15, %rdi
	movb	$0, (%rdx,%rax)
	call	_ZN2v84base5debug12_GLOBAL__N_115DemangleSymbolsEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movl	$4, %edx
	movl	$2, %edi
	leaq	.LC7(%rip), %rsi
	call	write@PLT
	movq	-112(%rbp), %rsi
	movq	%rsi, %rdi
	movq	%rsi, -136(%rbp)
	call	strlen@PLT
	movq	-136(%rbp), %rsi
	movl	$2, %edi
	movq	%rax, %rdx
	call	write@PLT
	movl	$2, %edi
	movl	$1, %edx
	leaq	.LC5(%rip), %rsi
	call	write@PLT
	movq	-112(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L73
	call	_ZdlPv@PLT
	addq	$1, %rbx
	cmpq	%rbx, %r12
	jne	.L75
.L67:
	movq	-144(%rbp), %rdi
	call	free@PLT
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L73:
	addq	$1, %rbx
	cmpq	%rbx, %r12
	jne	.L75
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L71:
	testq	%rax, %rax
	jne	.L101
	movq	%r13, %rdx
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L100:
	movq	-152(%rbp), %rsi
	movq	%r15, %rdi
	xorl	%edx, %edx
	movq	%rax, -136(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-136(%rbp), %r8
	movq	%rax, -112(%rbp)
	movq	%rax, %rdi
	movq	-120(%rbp), %rax
	movq	%rax, -96(%rbp)
.L70:
	movq	%r8, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-120(%rbp), %rax
	movq	-112(%rbp), %rdx
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L99:
	leaq	.LC2(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
	.p2align 4,,10
	.p2align 3
.L85:
	leaq	-67(%rbp), %rax
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L86:
	leaq	-66(%rbp), %rax
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L87:
	leaq	-65(%rbp), %rax
	jmp	.L76
.L98:
	call	__stack_chk_fail@PLT
.L101:
	movq	%r13, %rdi
	jmp	.L70
	.cfi_endproc
.LFE4098:
	.size	_ZN2v84base5debug12_GLOBAL__N_116ProcessBacktraceEPKPvmPNS2_22BacktraceOutputHandlerE.constprop.0, .-_ZN2v84base5debug12_GLOBAL__N_116ProcessBacktraceEPKPvmPNS2_22BacktraceOutputHandlerE.constprop.0
	.section	.rodata._ZN2v84base5debug12_GLOBAL__N_122StackDumpSignalHandlerEiP9siginfo_tPv.str1.1,"aMS",@progbits,1
.LC12:
	.string	"Received signal "
.LC13:
	.string	" BUS_ADRALN "
.LC14:
	.string	" BUS_ADRERR "
.LC15:
	.string	" BUS_OBJERR "
.LC16:
	.string	" <unknown> "
.LC17:
	.string	" FPE_FLTDIV "
.LC18:
	.string	" FPE_FLTINV "
.LC19:
	.string	" FPE_FLTOVF "
.LC20:
	.string	" FPE_FLTRES "
.LC21:
	.string	" FPE_FLTSUB "
.LC22:
	.string	" FPE_FLTUND "
.LC23:
	.string	" FPE_INTDIV "
.LC24:
	.string	" FPE_INTOVF "
.LC25:
	.string	" ILL_BADSTK "
.LC26:
	.string	" ILL_COPROC "
.LC27:
	.string	" ILL_ILLOPN "
.LC28:
	.string	" ILL_ILLADR "
.LC29:
	.string	" ILL_ILLTRP "
.LC30:
	.string	" ILL_PRVOPC "
.LC31:
	.string	" ILL_PRVREG "
.LC32:
	.string	" SEGV_MAPERR "
.LC33:
	.string	" SEGV_ACCERR "
.LC34:
	.string	"[end of stack trace]\n"
	.section	.text._ZN2v84base5debug12_GLOBAL__N_122StackDumpSignalHandlerEiP9siginfo_tPv,"ax",@progbits
	.p2align 4
	.type	_ZN2v84base5debug12_GLOBAL__N_122StackDumpSignalHandlerEiP9siginfo_tPv, @function
_ZN2v84base5debug12_GLOBAL__N_122StackDumpSignalHandlerEiP9siginfo_tPv:
.LFB3455:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$16, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movl	%edi, %r12d
	movl	$2, %edi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	leaq	.LC12(%rip), %rsi
	subq	$1568, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	$1, _ZN2v84base5debug12_GLOBAL__N_117in_signal_handlerE(%rip)
	call	write@PLT
	pxor	%xmm0, %xmm0
	movl	$126, %ecx
	xorl	%eax, %eax
	leaq	-1056(%rbp), %rdi
	movaps	%xmm0, -1072(%rbp)
	rep stosq
	movslq	%r12d, %rdi
	testl	%r12d, %r12d
	jns	.L144
	movb	$45, -1072(%rbp)
	negq	%rdi
	movl	$3, %r9d
	leaq	-1071(%rbp), %rcx
	leaq	-1072(%rbp), %r13
.L103:
	movq	%rcx, %r8
	leaq	.LC10(%rip), %r14
	subq	%rcx, %r9
	movabsq	$-3689348814741910323, %r10
	.p2align 4,,10
	.p2align 3
.L104:
	movq	%rdi, %rax
	movq	%rdi, %r11
	movq	%r8, %rsi
	mulq	%r10
	leaq	1(%r8), %r8
	shrq	$3, %rdx
	leaq	(%rdx,%rdx,4), %rax
	addq	%rax, %rax
	subq	%rax, %r11
	movzbl	(%r14,%r11), %eax
	movb	%al, -1(%r8)
	movq	%rdi, %rax
	movq	%rdx, %rdi
	cmpq	$9, %rax
	jbe	.L165
	leaq	(%r8,%r9), %rax
	cmpq	$1024, %rax
	jbe	.L104
	movb	$0, -1072(%rbp)
.L105:
	movq	%r13, %rdx
.L109:
	movl	(%rdx), %ecx
	addq	$4, %rdx
	leal	-16843009(%rcx), %eax
	notl	%ecx
	andl	%ecx, %eax
	andl	$-2139062144, %eax
	je	.L109
	movl	%eax, %ecx
	movl	$2, %edi
	shrl	$16, %ecx
	testl	$32896, %eax
	cmove	%ecx, %eax
	leaq	2(%rdx), %rcx
	cmove	%rcx, %rdx
	movl	%eax, %esi
	addb	%al, %sil
	movq	%r13, %rsi
	sbbq	$3, %rdx
	subq	%r13, %rdx
	call	write@PLT
	cmpl	$7, %r12d
	je	.L166
	cmpl	$8, %r12d
	je	.L167
	cmpl	$4, %r12d
	je	.L168
	cmpl	$11, %r12d
	je	.L169
	.p2align 4,,10
	.p2align 3
.L131:
	movl	$1, %edx
	leaq	.LC5(%rip), %rsi
	movl	$2, %edi
	call	write@PLT
	cmpb	$0, _ZN2v84base5debug12_GLOBAL__N_128dump_stack_in_signal_handlerE(%rip)
	jne	.L170
.L141:
	xorl	%esi, %esi
	movl	%r12d, %edi
	call	signal@PLT
	cmpq	$-1, %rax
	je	.L171
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L172
	addq	$1568, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L144:
	.cfi_restore_state
	leaq	-1072(%rbp), %r13
	movl	$2, %r9d
	movq	%r13, %rcx
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L167:
	movl	8(%rbx), %eax
	cmpl	$3, %eax
	je	.L173
	cmpl	$7, %eax
	je	.L174
	cmpl	$4, %eax
	je	.L175
	cmpl	$6, %eax
	je	.L176
	cmpl	$8, %eax
	je	.L177
	cmpl	$5, %eax
	je	.L178
	cmpl	$1, %eax
	je	.L179
	cmpl	$2, %eax
	jne	.L115
	movl	$12, %edx
	leaq	.LC24(%rip), %rsi
	movl	$2, %edi
	call	write@PLT
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L165:
	movb	$0, (%r8)
	cmpq	%rcx, %rsi
	jbe	.L105
	.p2align 4,,10
	.p2align 3
.L107:
	movzbl	(%rcx), %edx
	movzbl	(%rsi), %eax
	addq	$1, %rcx
	subq	$1, %rsi
	movb	%dl, 1(%rsi)
	movb	%al, -1(%rcx)
	cmpq	%rcx, %rsi
	ja	.L107
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L166:
	movl	8(%rbx), %eax
	cmpl	$1, %eax
	je	.L180
	cmpl	$2, %eax
	je	.L181
	cmpl	$3, %eax
	je	.L182
.L115:
	movl	$11, %edx
	leaq	.LC16(%rip), %rsi
	movl	$2, %edi
	call	write@PLT
.L113:
	movq	16(%rbx), %rsi
	movl	$12, %ecx
	movq	%r13, %rdx
	leaq	-49(%rbp), %r9
	.p2align 4,,10
	.p2align 3
.L133:
	movq	%rsi, %rdi
	movq	%rdx, %rax
	addq	$1, %rdx
	movq	%rsi, %r8
	andl	$15, %edi
	shrq	$4, %rsi
	movzbl	(%r14,%rdi), %edi
	movb	%dil, -1(%rdx)
	movl	$1, %edi
	testq	%rcx, %rcx
	je	.L135
	subq	$1, %rcx
	sete	%dil
.L135:
	cmpq	$15, %r8
	ja	.L136
	testb	%dil, %dil
	jne	.L183
.L136:
	cmpq	%r9, %rdx
	jne	.L133
	movb	$0, -1072(%rbp)
.L134:
	movq	%r13, %rdx
.L139:
	movl	(%rdx), %ecx
	addq	$4, %rdx
	leal	-16843009(%rcx), %eax
	notl	%ecx
	andl	%ecx, %eax
	andl	$-2139062144, %eax
	je	.L139
	movl	%eax, %ecx
	movq	%r13, %rsi
	movl	$2, %edi
	shrl	$16, %ecx
	testl	$32896, %eax
	cmove	%ecx, %eax
	leaq	2(%rdx), %rcx
	cmove	%rcx, %rdx
	movl	%eax, %ebx
	addb	%al, %bl
	sbbq	$3, %rdx
	subq	%r13, %rdx
	call	write@PLT
	jmp	.L131
	.p2align 4,,10
	.p2align 3
.L170:
	leaq	-1584(%rbp), %r13
	movl	$62, %esi
	movq	%r13, %rdi
	call	backtrace@PLT
	leaq	-1592(%rbp), %rdx
	movq	%r13, %rdi
	movslq	%eax, %rsi
	leaq	16+_ZTVN2v84base5debug12_GLOBAL__N_127PrintBacktraceOutputHandlerE(%rip), %rax
	movq	%rsi, -1088(%rbp)
	movq	%rax, -1592(%rbp)
	call	_ZN2v84base5debug12_GLOBAL__N_116ProcessBacktraceEPKPvmPNS2_22BacktraceOutputHandlerE.constprop.0
	movq	%r13, %rdi
	call	_ZN2v84base5debug10StackTraceD1Ev@PLT
	movl	$21, %edx
	movl	$2, %edi
	leaq	.LC34(%rip), %rsi
	call	write@PLT
	jmp	.L141
	.p2align 4,,10
	.p2align 3
.L183:
	movb	$0, (%rdx)
	movq	%r13, %rdx
	cmpq	%r13, %rax
	jbe	.L134
	.p2align 4,,10
	.p2align 3
.L137:
	movzbl	(%rdx), %esi
	movzbl	(%rax), %ecx
	addq	$1, %rdx
	subq	$1, %rax
	movb	%sil, 1(%rax)
	movb	%cl, -1(%rdx)
	cmpq	%rdx, %rax
	ja	.L137
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L169:
	movl	8(%rbx), %eax
	cmpl	$1, %eax
	je	.L184
	cmpl	$2, %eax
	jne	.L115
	movl	$13, %edx
	leaq	.LC33(%rip), %rsi
	movl	$2, %edi
	call	write@PLT
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L182:
	movl	$12, %edx
	leaq	.LC15(%rip), %rsi
	movl	$2, %edi
	call	write@PLT
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L168:
	movl	8(%rbx), %eax
	cmpl	$8, %eax
	je	.L185
	cmpl	$7, %eax
	je	.L186
	cmpl	$2, %eax
	je	.L187
	cmpl	$3, %eax
	je	.L188
	cmpl	$4, %eax
	je	.L189
	cmpl	$5, %eax
	je	.L190
	cmpl	$6, %eax
	jne	.L115
	movl	$12, %edx
	leaq	.LC31(%rip), %rsi
	movl	$2, %edi
	call	write@PLT
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L180:
	movl	$12, %edx
	leaq	.LC13(%rip), %rsi
	movl	$2, %edi
	call	write@PLT
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L181:
	movl	$12, %edx
	leaq	.LC14(%rip), %rsi
	movl	$2, %edi
	call	write@PLT
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L173:
	movl	$12, %edx
	leaq	.LC17(%rip), %rsi
	movl	$2, %edi
	call	write@PLT
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L189:
	movl	$12, %edx
	leaq	.LC29(%rip), %rsi
	movl	$2, %edi
	call	write@PLT
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L179:
	movl	$12, %edx
	leaq	.LC23(%rip), %rsi
	movl	$2, %edi
	call	write@PLT
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L174:
	movl	$12, %edx
	leaq	.LC18(%rip), %rsi
	movl	$2, %edi
	call	write@PLT
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L185:
	movl	$12, %edx
	leaq	.LC25(%rip), %rsi
	movl	$2, %edi
	call	write@PLT
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L175:
	movl	$12, %edx
	leaq	.LC19(%rip), %rsi
	movl	$2, %edi
	call	write@PLT
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L186:
	movl	$12, %edx
	leaq	.LC26(%rip), %rsi
	movl	$2, %edi
	call	write@PLT
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L176:
	movl	$12, %edx
	leaq	.LC20(%rip), %rsi
	movl	$2, %edi
	call	write@PLT
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L184:
	movl	$13, %edx
	leaq	.LC32(%rip), %rsi
	movl	$2, %edi
	call	write@PLT
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L187:
	movl	$12, %edx
	leaq	.LC27(%rip), %rsi
	movl	$2, %edi
	call	write@PLT
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L177:
	movl	$12, %edx
	leaq	.LC21(%rip), %rsi
	movl	$2, %edi
	call	write@PLT
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L188:
	movl	$12, %edx
	leaq	.LC28(%rip), %rsi
	movl	$2, %edi
	call	write@PLT
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L178:
	movl	$12, %edx
	leaq	.LC22(%rip), %rsi
	movl	$2, %edi
	call	write@PLT
	jmp	.L113
.L190:
	movl	$12, %edx
	leaq	.LC30(%rip), %rsi
	movl	$2, %edi
	call	write@PLT
	jmp	.L113
.L171:
	movl	$1, %edi
	call	_exit@PLT
.L172:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3455:
	.size	_ZN2v84base5debug12_GLOBAL__N_122StackDumpSignalHandlerEiP9siginfo_tPv, .-_ZN2v84base5debug12_GLOBAL__N_122StackDumpSignalHandlerEiP9siginfo_tPv
	.section	.text._ZN2v84base5debug27EnableInProcessStackDumpingEv,"ax",@progbits
	.p2align 4
	.globl	_ZN2v84base5debug27EnableInProcessStackDumpingEv
	.type	_ZN2v84base5debug27EnableInProcessStackDumpingEv, @function
_ZN2v84base5debug27EnableInProcessStackDumpingEv:
.LFB3469:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$18, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	xorl	%r13d, %r13d
	pushq	%r12
	leaq	-696(%rbp), %r8
	.cfi_offset 12, -32
	leaq	-704(%rbp), %r12
	pushq	%rbx
	movq	%r8, %rdi
	subq	$680, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$1, -704(%rbp)
	rep stosq
	movq	%r8, %rdi
	call	sigemptyset@PLT
	xorl	%edx, %edx
	movq	%r12, %rsi
	movl	$13, %edi
	call	sigaction@PLT
	leaq	-544(%rbp), %r12
	movl	$62, %esi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	backtrace@PLT
	movq	%r12, %rdi
	cltq
	movq	%rax, -48(%rbp)
	call	_ZN2v84base5debug10StackTraceD1Ev@PLT
	leaq	-536(%rbp), %r8
	movq	%r13, %rax
	movl	$18, %ecx
	movq	%r8, %rdi
	rep stosq
	movq	%r8, %rdi
	leaq	_ZN2v84base5debug12_GLOBAL__N_122StackDumpSignalHandlerEiP9siginfo_tPv(%rip), %rax
	movl	$-2147483644, -408(%rbp)
	movq	%rax, -544(%rbp)
	call	sigemptyset@PLT
	xorl	%edx, %edx
	movq	%r12, %rsi
	movl	$4, %edi
	call	sigaction@PLT
	xorl	%edx, %edx
	movq	%r12, %rsi
	movl	$6, %edi
	orl	%eax, %ebx
	call	sigaction@PLT
	xorl	%edx, %edx
	movq	%r12, %rsi
	movl	$8, %edi
	orl	%eax, %ebx
	call	sigaction@PLT
	xorl	%edx, %edx
	movq	%r12, %rsi
	movl	$7, %edi
	orl	%eax, %ebx
	call	sigaction@PLT
	xorl	%edx, %edx
	movq	%r12, %rsi
	movl	$11, %edi
	orl	%eax, %ebx
	call	sigaction@PLT
	xorl	%edx, %edx
	movq	%r12, %rsi
	movl	$31, %edi
	orl	%eax, %ebx
	call	sigaction@PLT
	movb	$1, _ZN2v84base5debug12_GLOBAL__N_128dump_stack_in_signal_handlerE(%rip)
	orl	%ebx, %eax
	sete	%al
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L194
	addq	$680, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L194:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3469:
	.size	_ZN2v84base5debug27EnableInProcessStackDumpingEv, .-_ZN2v84base5debug27EnableInProcessStackDumpingEv
	.section	.text._ZN2v84base5debug22DisableSignalStackDumpEv,"ax",@progbits
	.p2align 4
	.globl	_ZN2v84base5debug22DisableSignalStackDumpEv
	.type	_ZN2v84base5debug22DisableSignalStackDumpEv, @function
_ZN2v84base5debug22DisableSignalStackDumpEv:
.LFB3470:
	.cfi_startproc
	endbr64
	movb	$0, _ZN2v84base5debug12_GLOBAL__N_128dump_stack_in_signal_handlerE(%rip)
	ret
	.cfi_endproc
.LFE3470:
	.size	_ZN2v84base5debug22DisableSignalStackDumpEv, .-_ZN2v84base5debug22DisableSignalStackDumpEv
	.section	.text._ZN2v84base5debug10StackTraceC2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base5debug10StackTraceC2Ev
	.type	_ZN2v84base5debug10StackTraceC2Ev, @function
_ZN2v84base5debug10StackTraceC2Ev:
.LFB3472:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$62, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	backtrace@PLT
	cltq
	movq	%rax, 496(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3472:
	.size	_ZN2v84base5debug10StackTraceC2Ev, .-_ZN2v84base5debug10StackTraceC2Ev
	.globl	_ZN2v84base5debug10StackTraceC1Ev
	.set	_ZN2v84base5debug10StackTraceC1Ev,_ZN2v84base5debug10StackTraceC2Ev
	.section	.text._ZNK2v84base5debug10StackTrace5PrintEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v84base5debug10StackTrace5PrintEv
	.type	_ZNK2v84base5debug10StackTrace5PrintEv, @function
_ZNK2v84base5debug10StackTrace5PrintEv:
.LFB3474:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	496(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-16(%rbp), %rdx
	leaq	16+_ZTVN2v84base5debug12_GLOBAL__N_127PrintBacktraceOutputHandlerE(%rip), %rax
	movq	%rax, -16(%rbp)
	call	_ZN2v84base5debug12_GLOBAL__N_116ProcessBacktraceEPKPvmPNS2_22BacktraceOutputHandlerE.constprop.0
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L201
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L201:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3474:
	.size	_ZNK2v84base5debug10StackTrace5PrintEv, .-_ZNK2v84base5debug10StackTrace5PrintEv
	.section	.text._ZNK2v84base5debug10StackTrace14OutputToStreamEPSo,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v84base5debug10StackTrace14OutputToStreamEPSo
	.type	_ZNK2v84base5debug10StackTrace14OutputToStreamEPSo, @function
_ZNK2v84base5debug10StackTrace14OutputToStreamEPSo:
.LFB3478:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	leaq	.LC5(%rip), %rsi
	subq	$120, %rsp
	movq	496(%rdi), %r13
	movq	%rbx, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$51, %edx
	leaq	.LC6(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	leaq	.LC5(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	_ZN2v84base5debug12_GLOBAL__N_117in_signal_handlerE(%rip), %eax
	testl	%eax, %eax
	je	.L203
.L206:
	testq	%r13, %r13
	je	.L202
	leaq	(%r12,%r13,8), %rax
	leaq	.LC10(%rip), %r14
	movq	%rax, -136(%rbp)
	leaq	-80(%rbp), %r13
	.p2align 4,,10
	.p2align 3
.L223:
	movl	$2, %edx
	leaq	.LC8(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r12), %r15
	pxor	%xmm0, %xmm0
	movq	%rbx, %rdi
	movl	$2, %edx
	leaq	.LC9(%rip), %rsi
	movb	$0, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r15, %rax
	andl	$15, %eax
	movzbl	(%r14,%rax), %eax
	movb	%al, -80(%rbp)
	movq	%r15, %rax
	shrq	$4, %rax
	andl	$15, %eax
	movzbl	(%r14,%rax), %eax
	movb	%al, -79(%rbp)
	movq	%r15, %rax
	shrq	$8, %rax
	andl	$15, %eax
	movzbl	(%r14,%rax), %eax
	movb	%al, -78(%rbp)
	movq	%r15, %rax
	shrq	$12, %rax
	andl	$15, %eax
	movzbl	(%r14,%rax), %eax
	movb	%al, -77(%rbp)
	movq	%r15, %rax
	shrq	$16, %rax
	andl	$15, %eax
	movzbl	(%r14,%rax), %eax
	movb	%al, -76(%rbp)
	movq	%r15, %rax
	shrq	$20, %rax
	andl	$15, %eax
	movzbl	(%r14,%rax), %eax
	movb	%al, -75(%rbp)
	movq	%r15, %rax
	shrq	$24, %rax
	andl	$15, %eax
	movzbl	(%r14,%rax), %eax
	movb	%al, -74(%rbp)
	movq	%r15, %rax
	shrq	$28, %rax
	andl	$15, %eax
	movzbl	(%r14,%rax), %eax
	movb	%al, -73(%rbp)
	movq	%r15, %rax
	shrq	$32, %rax
	andl	$15, %eax
	movzbl	(%r14,%rax), %eax
	movb	%al, -72(%rbp)
	movq	%r15, %rax
	shrq	$36, %rax
	andl	$15, %eax
	movzbl	(%r14,%rax), %eax
	movb	%al, -71(%rbp)
	movq	%r15, %rax
	shrq	$40, %rax
	andl	$15, %eax
	movzbl	(%r14,%rax), %eax
	movb	%al, -70(%rbp)
	movq	%r15, %rax
	shrq	$44, %rax
	andl	$15, %eax
	movzbl	(%r14,%rax), %eax
	movb	%al, -69(%rbp)
	movabsq	$281474976710655, %rax
	cmpq	%rax, %r15
	jbe	.L226
	movabsq	$4503599627370495, %rcx
	movq	%r15, %rax
	shrq	$48, %rax
	andl	$15, %eax
	movzbl	(%r14,%rax), %eax
	movb	%al, -68(%rbp)
	movq	%r15, %rax
	shrq	$52, %rax
	cmpq	%rcx, %r15
	jbe	.L227
	movabsq	$72057594037927935, %rcx
	andl	$15, %eax
	movzbl	(%r14,%rax), %eax
	movb	%al, -67(%rbp)
	movq	%r15, %rax
	shrq	$56, %rax
	cmpq	%rcx, %r15
	jbe	.L228
	movabsq	$1152921504606846975, %rcx
	andl	$15, %eax
	movzbl	(%r14,%rax), %eax
	movb	%al, -66(%rbp)
	movq	%r15, %rax
	shrq	$60, %rax
	cmpq	%rcx, %r15
	jbe	.L229
	movzbl	(%r14,%rax), %eax
	movb	%al, -65(%rbp)
	leaq	-64(%rbp), %rax
.L218:
	movb	$0, (%rax)
	movq	%r13, %rdx
	subq	$1, %rax
	.p2align 4,,10
	.p2align 3
.L219:
	movzbl	(%rdx), %esi
	movzbl	(%rax), %ecx
	addq	$1, %rdx
	subq	$1, %rax
	movb	%sil, 1(%rax)
	movb	%cl, -1(%rdx)
	cmpq	%rdx, %rax
	ja	.L219
	movq	%r13, %rdx
.L220:
	movl	(%rdx), %ecx
	addq	$4, %rdx
	leal	-16843009(%rcx), %eax
	notl	%ecx
	andl	%ecx, %eax
	andl	$-2139062144, %eax
	je	.L220
	movl	%eax, %ecx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	shrl	$16, %ecx
	testl	$32896, %eax
	cmove	%ecx, %eax
	leaq	2(%rdx), %rcx
	cmove	%rcx, %rdx
	movl	%eax, %ecx
	addb	%al, %cl
	sbbq	$3, %rdx
	addq	$8, %r12
	subq	%r13, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$2, %edx
	leaq	.LC11(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpq	-136(%rbp), %r12
	jne	.L223
.L202:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L240
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L226:
	.cfi_restore_state
	leaq	-68(%rbp), %rax
	jmp	.L218
	.p2align 4,,10
	.p2align 3
.L203:
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	backtrace_symbols@PLT
	movq	%rax, -152(%rbp)
	testq	%rax, %rax
	je	.L206
	testq	%r13, %r13
	je	.L207
	leaq	-112(%rbp), %rax
	xorl	%r12d, %r12d
	leaq	-96(%rbp), %r14
	movq	%rax, -144(%rbp)
	leaq	-120(%rbp), %rax
	movq	%rax, -160(%rbp)
	.p2align 4,,10
	.p2align 3
.L217:
	movq	-152(%rbp), %rax
	movq	(%rax,%r12,8), %r15
	movq	%r14, -112(%rbp)
	testq	%r15, %r15
	je	.L241
	movq	%r15, %rdi
	call	strlen@PLT
	movq	%rax, -120(%rbp)
	movq	%rax, %r8
	cmpq	$15, %rax
	ja	.L242
	cmpq	$1, %rax
	jne	.L211
	movzbl	(%r15), %edx
	movb	%dl, -96(%rbp)
	movq	%r14, %rdx
.L212:
	movq	%rax, -104(%rbp)
	movq	-144(%rbp), %rdi
	movb	$0, (%rdx,%rax)
	call	_ZN2v84base5debug12_GLOBAL__N_115DemangleSymbolsEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	leaq	.LC7(%rip), %rsi
	movl	$4, %edx
	movq	%rbx, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-112(%rbp), %rsi
	testq	%rsi, %rsi
	je	.L243
	movq	%rsi, %rdi
	movq	%rsi, -136(%rbp)
	call	strlen@PLT
	movq	-136(%rbp), %rsi
	movq	%rbx, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L214:
	movq	%rbx, %rdi
	movl	$1, %edx
	leaq	.LC5(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-112(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L215
	call	_ZdlPv@PLT
	addq	$1, %r12
	cmpq	%r12, %r13
	jne	.L217
.L207:
	movq	-152(%rbp), %rdi
	call	free@PLT
	jmp	.L202
	.p2align 4,,10
	.p2align 3
.L215:
	addq	$1, %r12
	cmpq	%r12, %r13
	jne	.L217
	jmp	.L207
	.p2align 4,,10
	.p2align 3
.L211:
	testq	%rax, %rax
	jne	.L244
	movq	%r14, %rdx
	jmp	.L212
	.p2align 4,,10
	.p2align 3
.L242:
	movq	-144(%rbp), %rdi
	movq	-160(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rax, -136(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-136(%rbp), %r8
	movq	%rax, -112(%rbp)
	movq	%rax, %rdi
	movq	-120(%rbp), %rax
	movq	%rax, -96(%rbp)
.L210:
	movq	%r8, %rdx
	movq	%r15, %rsi
	call	memcpy@PLT
	movq	-120(%rbp), %rax
	movq	-112(%rbp), %rdx
	jmp	.L212
	.p2align 4,,10
	.p2align 3
.L241:
	leaq	.LC2(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
	.p2align 4,,10
	.p2align 3
.L243:
	movq	(%rbx), %rax
	movq	-24(%rax), %rdi
	addq	%rbx, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L214
	.p2align 4,,10
	.p2align 3
.L227:
	leaq	-67(%rbp), %rax
	jmp	.L218
	.p2align 4,,10
	.p2align 3
.L228:
	leaq	-66(%rbp), %rax
	jmp	.L218
	.p2align 4,,10
	.p2align 3
.L229:
	leaq	-65(%rbp), %rax
	jmp	.L218
.L240:
	call	__stack_chk_fail@PLT
.L244:
	movq	%r14, %rdi
	jmp	.L210
	.cfi_endproc
.LFE3478:
	.size	_ZNK2v84base5debug10StackTrace14OutputToStreamEPSo, .-_ZNK2v84base5debug10StackTrace14OutputToStreamEPSo
	.section	.text._ZN2v84base5debug8internal6itoa_rElPcmim,"ax",@progbits
	.p2align 4
	.globl	_ZN2v84base5debug8internal6itoa_rElPcmim
	.type	_ZN2v84base5debug8internal6itoa_rElPcmim, @function
_ZN2v84base5debug8internal6itoa_rElPcmim:
.LFB3479:
	.cfi_startproc
	endbr64
	testq	%rdx, %rdx
	je	.L271
	leal	-2(%rcx), %eax
	cmpl	$14, %eax
	ja	.L272
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdx, %rbx
	testq	%rdi, %rdi
	jns	.L258
	cmpl	$10, %ecx
	jne	.L258
	negq	%rdi
	cmpq	$1, %rdx
	jbe	.L249
	movb	$45, (%rsi)
	leaq	1(%rsi), %r11
	movl	$2, %eax
.L248:
	leaq	1(%rax), %r12
	movq	%r11, %r9
	movslq	%ecx, %rcx
	subq	%r11, %r12
	leaq	.LC10(%rip), %r13
	jmp	.L254
	.p2align 4,,10
	.p2align 3
.L273:
	subq	$1, %r8
	cmpq	%rdi, %rcx
	jbe	.L253
	testq	%r8, %r8
	je	.L251
.L253:
	movq	%r10, %r9
	movq	%rax, %rdi
.L254:
	leaq	(%r9,%r12), %rax
	cmpq	%rax, %rbx
	jb	.L249
	movq	%rdi, %rax
	xorl	%edx, %edx
	leaq	1(%r9), %r10
	divq	%rcx
	movzbl	0(%r13,%rdx), %edx
	movb	%dl, -1(%r10)
	testq	%r8, %r8
	jne	.L273
	cmpq	%rdi, %rcx
	jbe	.L253
.L251:
	movb	$0, (%r10)
	cmpq	%r11, %r9
	jbe	.L256
	.p2align 4,,10
	.p2align 3
.L255:
	movzbl	(%r11), %edx
	movzbl	(%r9), %eax
	addq	$1, %r11
	subq	$1, %r9
	movb	%dl, 1(%r9)
	movb	%al, -1(%r11)
	cmpq	%r11, %r9
	ja	.L255
.L256:
	popq	%rbx
	movq	%rsi, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L258:
	.cfi_restore_state
	movq	%rsi, %r11
	movl	$1, %eax
	jmp	.L248
	.p2align 4,,10
	.p2align 3
.L249:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	movb	$0, (%rsi)
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L272:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	movb	$0, (%rsi)
.L271:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3479:
	.size	_ZN2v84base5debug8internal6itoa_rElPcmim, .-_ZN2v84base5debug8internal6itoa_rElPcmim
	.section	.data.rel.ro.local._ZTVN2v84base5debug12_GLOBAL__N_127PrintBacktraceOutputHandlerE,"aw"
	.align 8
	.type	_ZTVN2v84base5debug12_GLOBAL__N_127PrintBacktraceOutputHandlerE, @object
	.size	_ZTVN2v84base5debug12_GLOBAL__N_127PrintBacktraceOutputHandlerE, 40
_ZTVN2v84base5debug12_GLOBAL__N_127PrintBacktraceOutputHandlerE:
	.quad	0
	.quad	0
	.quad	_ZN2v84base5debug12_GLOBAL__N_127PrintBacktraceOutputHandler12HandleOutputEPKc
	.quad	_ZN2v84base5debug12_GLOBAL__N_127PrintBacktraceOutputHandlerD1Ev
	.quad	_ZN2v84base5debug12_GLOBAL__N_127PrintBacktraceOutputHandlerD0Ev
	.section	.rodata._ZN2v84base5debug12_GLOBAL__N_1L17kSymbolCharactersE,"a"
	.align 32
	.type	_ZN2v84base5debug12_GLOBAL__N_1L17kSymbolCharactersE, @object
	.size	_ZN2v84base5debug12_GLOBAL__N_1L17kSymbolCharactersE, 64
_ZN2v84base5debug12_GLOBAL__N_1L17kSymbolCharactersE:
	.string	"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_"
	.section	.rodata._ZN2v84base5debug12_GLOBAL__N_1L20kMangledSymbolPrefixE,"a"
	.type	_ZN2v84base5debug12_GLOBAL__N_1L20kMangledSymbolPrefixE, @object
	.size	_ZN2v84base5debug12_GLOBAL__N_1L20kMangledSymbolPrefixE, 3
_ZN2v84base5debug12_GLOBAL__N_1L20kMangledSymbolPrefixE:
	.string	"_Z"
	.section	.data._ZN2v84base5debug12_GLOBAL__N_128dump_stack_in_signal_handlerE,"aw"
	.type	_ZN2v84base5debug12_GLOBAL__N_128dump_stack_in_signal_handlerE, @object
	.size	_ZN2v84base5debug12_GLOBAL__N_128dump_stack_in_signal_handlerE, 1
_ZN2v84base5debug12_GLOBAL__N_128dump_stack_in_signal_handlerE:
	.byte	1
	.section	.bss._ZN2v84base5debug12_GLOBAL__N_117in_signal_handlerE,"aw",@nobits
	.align 4
	.type	_ZN2v84base5debug12_GLOBAL__N_117in_signal_handlerE, @object
	.size	_ZN2v84base5debug12_GLOBAL__N_117in_signal_handlerE, 4
_ZN2v84base5debug12_GLOBAL__N_117in_signal_handlerE:
	.zero	4
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
