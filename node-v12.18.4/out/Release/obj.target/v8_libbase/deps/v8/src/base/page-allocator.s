	.file	"page-allocator.cc"
	.text
	.section	.text._ZN2v84base13PageAllocator16AllocatePageSizeEv,"axG",@progbits,_ZN2v84base13PageAllocator16AllocatePageSizeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v84base13PageAllocator16AllocatePageSizeEv
	.type	_ZN2v84base13PageAllocator16AllocatePageSizeEv, @function
_ZN2v84base13PageAllocator16AllocatePageSizeEv:
.LFB1953:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	ret
	.cfi_endproc
.LFE1953:
	.size	_ZN2v84base13PageAllocator16AllocatePageSizeEv, .-_ZN2v84base13PageAllocator16AllocatePageSizeEv
	.section	.text._ZN2v84base13PageAllocator14CommitPageSizeEv,"axG",@progbits,_ZN2v84base13PageAllocator14CommitPageSizeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v84base13PageAllocator14CommitPageSizeEv
	.type	_ZN2v84base13PageAllocator14CommitPageSizeEv, @function
_ZN2v84base13PageAllocator14CommitPageSizeEv:
.LFB1954:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	ret
	.cfi_endproc
.LFE1954:
	.size	_ZN2v84base13PageAllocator14CommitPageSizeEv, .-_ZN2v84base13PageAllocator14CommitPageSizeEv
	.section	.text._ZN2v84base13PageAllocatorD2Ev,"axG",@progbits,_ZN2v84base13PageAllocatorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v84base13PageAllocatorD2Ev
	.type	_ZN2v84base13PageAllocatorD2Ev, @function
_ZN2v84base13PageAllocatorD2Ev:
.LFB4037:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE4037:
	.size	_ZN2v84base13PageAllocatorD2Ev, .-_ZN2v84base13PageAllocatorD2Ev
	.weak	_ZN2v84base13PageAllocatorD1Ev
	.set	_ZN2v84base13PageAllocatorD1Ev,_ZN2v84base13PageAllocatorD2Ev
	.section	.text._ZN2v84base13PageAllocator17SetRandomMmapSeedEl,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base13PageAllocator17SetRandomMmapSeedEl
	.type	_ZN2v84base13PageAllocator17SetRandomMmapSeedEl, @function
_ZN2v84base13PageAllocator17SetRandomMmapSeedEl:
.LFB3465:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdi
	jmp	_ZN2v84base2OS17SetRandomMmapSeedEl@PLT
	.cfi_endproc
.LFE3465:
	.size	_ZN2v84base13PageAllocator17SetRandomMmapSeedEl, .-_ZN2v84base13PageAllocator17SetRandomMmapSeedEl
	.section	.text._ZN2v84base13PageAllocator17GetRandomMmapAddrEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base13PageAllocator17GetRandomMmapAddrEv
	.type	_ZN2v84base13PageAllocator17GetRandomMmapAddrEv, @function
_ZN2v84base13PageAllocator17GetRandomMmapAddrEv:
.LFB3466:
	.cfi_startproc
	endbr64
	jmp	_ZN2v84base2OS17GetRandomMmapAddrEv@PLT
	.cfi_endproc
.LFE3466:
	.size	_ZN2v84base13PageAllocator17GetRandomMmapAddrEv, .-_ZN2v84base13PageAllocator17GetRandomMmapAddrEv
	.section	.text._ZN2v84base13PageAllocator13AllocatePagesEPvmmNS_13PageAllocator10PermissionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base13PageAllocator13AllocatePagesEPvmmNS_13PageAllocator10PermissionE
	.type	_ZN2v84base13PageAllocator13AllocatePagesEPvmmNS_13PageAllocator10PermissionE, @function
_ZN2v84base13PageAllocator13AllocatePagesEPvmmNS_13PageAllocator10PermissionE:
.LFB3467:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdi
	movq	%rdx, %rsi
	movq	%rcx, %rdx
	movl	%r8d, %ecx
	jmp	_ZN2v84base2OS8AllocateEPvmmNS1_16MemoryPermissionE@PLT
	.cfi_endproc
.LFE3467:
	.size	_ZN2v84base13PageAllocator13AllocatePagesEPvmmNS_13PageAllocator10PermissionE, .-_ZN2v84base13PageAllocator13AllocatePagesEPvmmNS_13PageAllocator10PermissionE
	.section	.text._ZN2v84base13PageAllocator9FreePagesEPvm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base13PageAllocator9FreePagesEPvm
	.type	_ZN2v84base13PageAllocator9FreePagesEPvm, @function
_ZN2v84base13PageAllocator9FreePagesEPvm:
.LFB3468:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdi
	movq	%rdx, %rsi
	jmp	_ZN2v84base2OS4FreeEPvm@PLT
	.cfi_endproc
.LFE3468:
	.size	_ZN2v84base13PageAllocator9FreePagesEPvm, .-_ZN2v84base13PageAllocator9FreePagesEPvm
	.section	.text._ZN2v84base13PageAllocator12ReleasePagesEPvmm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base13PageAllocator12ReleasePagesEPvmm
	.type	_ZN2v84base13PageAllocator12ReleasePagesEPvmm, @function
_ZN2v84base13PageAllocator12ReleasePagesEPvmm:
.LFB3469:
	.cfi_startproc
	endbr64
	subq	%rcx, %rdx
	leaq	(%rsi,%rcx), %rdi
	movq	%rdx, %rsi
	jmp	_ZN2v84base2OS7ReleaseEPvm@PLT
	.cfi_endproc
.LFE3469:
	.size	_ZN2v84base13PageAllocator12ReleasePagesEPvmm, .-_ZN2v84base13PageAllocator12ReleasePagesEPvmm
	.section	.text._ZN2v84base13PageAllocator14SetPermissionsEPvmNS_13PageAllocator10PermissionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base13PageAllocator14SetPermissionsEPvmNS_13PageAllocator10PermissionE
	.type	_ZN2v84base13PageAllocator14SetPermissionsEPvmNS_13PageAllocator10PermissionE, @function
_ZN2v84base13PageAllocator14SetPermissionsEPvmNS_13PageAllocator10PermissionE:
.LFB3470:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdi
	movq	%rdx, %rsi
	movl	%ecx, %edx
	jmp	_ZN2v84base2OS14SetPermissionsEPvmNS1_16MemoryPermissionE@PLT
	.cfi_endproc
.LFE3470:
	.size	_ZN2v84base13PageAllocator14SetPermissionsEPvmNS_13PageAllocator10PermissionE, .-_ZN2v84base13PageAllocator14SetPermissionsEPvmNS_13PageAllocator10PermissionE
	.section	.text._ZN2v84base13PageAllocator18DiscardSystemPagesEPvm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base13PageAllocator18DiscardSystemPagesEPvm
	.type	_ZN2v84base13PageAllocator18DiscardSystemPagesEPvm, @function
_ZN2v84base13PageAllocator18DiscardSystemPagesEPvm:
.LFB3471:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdi
	movq	%rdx, %rsi
	jmp	_ZN2v84base2OS18DiscardSystemPagesEPvm@PLT
	.cfi_endproc
.LFE3471:
	.size	_ZN2v84base13PageAllocator18DiscardSystemPagesEPvm, .-_ZN2v84base13PageAllocator18DiscardSystemPagesEPvm
	.section	.text._ZN2v84base13PageAllocatorD0Ev,"axG",@progbits,_ZN2v84base13PageAllocatorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v84base13PageAllocatorD0Ev
	.type	_ZN2v84base13PageAllocatorD0Ev, @function
_ZN2v84base13PageAllocatorD0Ev:
.LFB4039:
	.cfi_startproc
	endbr64
	movl	$24, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE4039:
	.size	_ZN2v84base13PageAllocatorD0Ev, .-_ZN2v84base13PageAllocatorD0Ev
	.section	.text._ZN2v84base13PageAllocatorC2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base13PageAllocatorC2Ev
	.type	_ZN2v84base13PageAllocatorC2Ev, @function
_ZN2v84base13PageAllocatorC2Ev:
.LFB3463:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v84base13PageAllocatorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN2v84base2OS16AllocatePageSizeEv@PLT
	movq	%rax, 8(%rbx)
	call	_ZN2v84base2OS14CommitPageSizeEv@PLT
	movq	%rax, 16(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3463:
	.size	_ZN2v84base13PageAllocatorC2Ev, .-_ZN2v84base13PageAllocatorC2Ev
	.globl	_ZN2v84base13PageAllocatorC1Ev
	.set	_ZN2v84base13PageAllocatorC1Ev,_ZN2v84base13PageAllocatorC2Ev
	.weak	_ZTVN2v84base13PageAllocatorE
	.section	.data.rel.ro.local._ZTVN2v84base13PageAllocatorE,"awG",@progbits,_ZTVN2v84base13PageAllocatorE,comdat
	.align 8
	.type	_ZTVN2v84base13PageAllocatorE, @object
	.size	_ZTVN2v84base13PageAllocatorE, 104
_ZTVN2v84base13PageAllocatorE:
	.quad	0
	.quad	0
	.quad	_ZN2v84base13PageAllocatorD1Ev
	.quad	_ZN2v84base13PageAllocatorD0Ev
	.quad	_ZN2v84base13PageAllocator16AllocatePageSizeEv
	.quad	_ZN2v84base13PageAllocator14CommitPageSizeEv
	.quad	_ZN2v84base13PageAllocator17SetRandomMmapSeedEl
	.quad	_ZN2v84base13PageAllocator17GetRandomMmapAddrEv
	.quad	_ZN2v84base13PageAllocator13AllocatePagesEPvmmNS_13PageAllocator10PermissionE
	.quad	_ZN2v84base13PageAllocator9FreePagesEPvm
	.quad	_ZN2v84base13PageAllocator12ReleasePagesEPvmm
	.quad	_ZN2v84base13PageAllocator14SetPermissionsEPvmNS_13PageAllocator10PermissionE
	.quad	_ZN2v84base13PageAllocator18DiscardSystemPagesEPvm
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
