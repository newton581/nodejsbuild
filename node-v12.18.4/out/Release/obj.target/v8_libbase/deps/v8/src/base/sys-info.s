	.file	"sys-info.cc"
	.text
	.section	.text._ZN2v84base7SysInfo18NumberOfProcessorsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base7SysInfo18NumberOfProcessorsEv
	.type	_ZN2v84base7SysInfo18NumberOfProcessorsEv, @function
_ZN2v84base7SysInfo18NumberOfProcessorsEv:
.LFB2781:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$84, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	sysconf@PLT
	movl	$1, %edx
	popq	%rbp
	.cfi_def_cfa 7, 8
	cmpq	$-1, %rax
	cmove	%rdx, %rax
	ret
	.cfi_endproc
.LFE2781:
	.size	_ZN2v84base7SysInfo18NumberOfProcessorsEv, .-_ZN2v84base7SysInfo18NumberOfProcessorsEv
	.section	.text._ZN2v84base7SysInfo22AmountOfPhysicalMemoryEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base7SysInfo22AmountOfPhysicalMemoryEv
	.type	_ZN2v84base7SysInfo22AmountOfPhysicalMemoryEv, @function
_ZN2v84base7SysInfo22AmountOfPhysicalMemoryEv:
.LFB2782:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$85, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	call	sysconf@PLT
	movl	$30, %edi
	movq	%rax, %rbx
	call	sysconf@PLT
	cmpq	$-1, %rbx
	je	.L7
	cmpq	$-1, %rax
	je	.L7
	addq	$8, %rsp
	imulq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2782:
	.size	_ZN2v84base7SysInfo22AmountOfPhysicalMemoryEv, .-_ZN2v84base7SysInfo22AmountOfPhysicalMemoryEv
	.section	.text._ZN2v84base7SysInfo21AmountOfVirtualMemoryEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base7SysInfo21AmountOfVirtualMemoryEv
	.type	_ZN2v84base7SysInfo21AmountOfVirtualMemoryEv, @function
_ZN2v84base7SysInfo21AmountOfVirtualMemoryEv:
.LFB2783:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-32(%rbp), %rsi
	call	getrlimit@PLT
	movl	%eax, %r8d
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jne	.L9
	movq	-32(%rbp), %rdx
	cmpq	$-1, %rdx
	cmovne	%rdx, %rax
.L9:
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L15
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L15:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2783:
	.size	_ZN2v84base7SysInfo21AmountOfVirtualMemoryEv, .-_ZN2v84base7SysInfo21AmountOfVirtualMemoryEv
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
