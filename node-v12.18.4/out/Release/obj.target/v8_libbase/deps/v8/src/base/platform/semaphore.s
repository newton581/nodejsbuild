	.file	"semaphore.cc"
	.text
	.section	.text._ZN2v84base9SemaphoreC2Ei,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base9SemaphoreC2Ei
	.type	_ZN2v84base9SemaphoreC2Ei, @function
_ZN2v84base9SemaphoreC2Ei:
.LFB3216:
	.cfi_startproc
	endbr64
	movl	%esi, %edx
	xorl	%esi, %esi
	jmp	sem_init@PLT
	.cfi_endproc
.LFE3216:
	.size	_ZN2v84base9SemaphoreC2Ei, .-_ZN2v84base9SemaphoreC2Ei
	.globl	_ZN2v84base9SemaphoreC1Ei
	.set	_ZN2v84base9SemaphoreC1Ei,_ZN2v84base9SemaphoreC2Ei
	.section	.text._ZN2v84base9SemaphoreD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base9SemaphoreD2Ev
	.type	_ZN2v84base9SemaphoreD2Ev, @function
_ZN2v84base9SemaphoreD2Ev:
.LFB3219:
	.cfi_startproc
	endbr64
	jmp	sem_destroy@PLT
	.cfi_endproc
.LFE3219:
	.size	_ZN2v84base9SemaphoreD2Ev, .-_ZN2v84base9SemaphoreD2Ev
	.globl	_ZN2v84base9SemaphoreD1Ev
	.set	_ZN2v84base9SemaphoreD1Ev,_ZN2v84base9SemaphoreD2Ev
	.section	.rodata._ZN2v84base9Semaphore6SignalEv.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"Error when signaling semaphore, errno: %d"
	.section	.text._ZN2v84base9Semaphore6SignalEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base9Semaphore6SignalEv
	.type	_ZN2v84base9Semaphore6SignalEv, @function
_ZN2v84base9Semaphore6SignalEv:
.LFB3221:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	sem_post@PLT
	testl	%eax, %eax
	jne	.L7
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L7:
	.cfi_restore_state
	call	__errno_location@PLT
	leaq	.LC0(%rip), %rdi
	movl	(%rax), %esi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE3221:
	.size	_ZN2v84base9Semaphore6SignalEv, .-_ZN2v84base9Semaphore6SignalEv
	.section	.text._ZN2v84base9Semaphore4WaitEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base9Semaphore4WaitEv
	.type	_ZN2v84base9Semaphore4WaitEv, @function
_ZN2v84base9Semaphore4WaitEv:
.LFB3222:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	.p2align 4,,10
	.p2align 3
.L9:
	movq	%rbx, %rdi
	call	sem_wait@PLT
	testl	%eax, %eax
	jne	.L9
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3222:
	.size	_ZN2v84base9Semaphore4WaitEv, .-_ZN2v84base9Semaphore4WaitEv
	.section	.text._ZN2v84base9Semaphore7WaitForERKNS0_9TimeDeltaE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base9Semaphore7WaitForERKNS0_9TimeDeltaE
	.type	_ZN2v84base9Semaphore7WaitForERKNS0_9TimeDeltaE, @function
_ZN2v84base9Semaphore7WaitForERKNS0_9TimeDeltaE:
.LFB3223:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v84base4Time17NowFromSystemTimeEv@PLT
	movq	(%r12), %rdi
	leaq	-48(%rbp), %r12
	movq	%rax, %rsi
	call	_ZN2v84base4bits20SignedSaturatedAdd64Ell@PLT
	leaq	-56(%rbp), %rdi
	movq	%rax, -56(%rbp)
	call	_ZNK2v84base4Time10ToTimespecEv@PLT
	movq	%rax, -48(%rbp)
	movq	%rdx, -40(%rbp)
	.p2align 4,,10
	.p2align 3
.L17:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	sem_timedwait@PLT
	testl	%eax, %eax
	je	.L16
	cmpl	$-1, %eax
	jne	.L17
	call	__errno_location@PLT
	cmpl	$110, (%rax)
	jne	.L17
	xorl	%eax, %eax
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L16:
	movl	$1, %eax
.L12:
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L19
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L19:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3223:
	.size	_ZN2v84base9Semaphore7WaitForERKNS0_9TimeDeltaE, .-_ZN2v84base9Semaphore7WaitForERKNS0_9TimeDeltaE
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
