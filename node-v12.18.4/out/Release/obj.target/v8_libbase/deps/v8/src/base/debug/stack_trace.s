	.file	"stack_trace.cc"
	.text
	.section	.text._ZN2v84base5debug10StackTraceC2EPKPKvm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base5debug10StackTraceC2EPKPKvm
	.type	_ZN2v84base5debug10StackTraceC2EPKPKvm, @function
_ZN2v84base5debug10StackTraceC2EPKPKvm:
.LFB3002:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	cmpq	$62, %rdx
	ja	.L4
	testq	%rdx, %rdx
	jne	.L9
	movq	%rdx, 496(%rax)
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	movl	$496, %ecx
	movl	$62, %edx
.L2:
	movq	(%rsi), %rdi
	movq	%rdi, (%rax)
	movl	%ecx, %edi
	movq	-8(%rsi,%rdi), %r8
	movq	%r8, -8(%rax,%rdi)
	leaq	8(%rax), %rdi
	movq	%rax, %r8
	andq	$-8, %rdi
	subq	%rdi, %r8
	addl	%r8d, %ecx
	subq	%r8, %rsi
	shrl	$3, %ecx
	rep movsq
	movq	%rdx, 496(%rax)
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	leaq	0(,%rdx,8), %rcx
	jmp	.L2
	.cfi_endproc
.LFE3002:
	.size	_ZN2v84base5debug10StackTraceC2EPKPKvm, .-_ZN2v84base5debug10StackTraceC2EPKPKvm
	.globl	_ZN2v84base5debug10StackTraceC1EPKPKvm
	.set	_ZN2v84base5debug10StackTraceC1EPKPKvm,_ZN2v84base5debug10StackTraceC2EPKPKvm
	.section	.text._ZN2v84base5debug10StackTraceD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base5debug10StackTraceD2Ev
	.type	_ZN2v84base5debug10StackTraceD2Ev, @function
_ZN2v84base5debug10StackTraceD2Ev:
.LFB3005:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3005:
	.size	_ZN2v84base5debug10StackTraceD2Ev, .-_ZN2v84base5debug10StackTraceD2Ev
	.globl	_ZN2v84base5debug10StackTraceD1Ev
	.set	_ZN2v84base5debug10StackTraceD1Ev,_ZN2v84base5debug10StackTraceD2Ev
	.section	.text._ZNK2v84base5debug10StackTrace9AddressesEPm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v84base5debug10StackTrace9AddressesEPm
	.type	_ZNK2v84base5debug10StackTrace9AddressesEPm, @function
_ZNK2v84base5debug10StackTrace9AddressesEPm:
.LFB3007:
	.cfi_startproc
	endbr64
	movq	496(%rdi), %rax
	testq	%rax, %rax
	movq	%rax, (%rsi)
	movl	$0, %eax
	cmovne	%rdi, %rax
	ret
	.cfi_endproc
.LFE3007:
	.size	_ZNK2v84base5debug10StackTrace9AddressesEPm, .-_ZNK2v84base5debug10StackTrace9AddressesEPm
	.section	.text._ZNK2v84base5debug10StackTrace8ToStringB5cxx11Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v84base5debug10StackTrace8ToStringB5cxx11Ev
	.type	_ZNK2v84base5debug10StackTrace8ToStringB5cxx11Ev, @function
_ZNK2v84base5debug10StackTrace8ToStringB5cxx11Ev:
.LFB3008:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-320(%rbp), %r14
	leaq	-368(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%r14, %rdi
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-448(%rbp), %r12
	pushq	%rbx
	subq	$440, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -480(%rbp)
	movq	.LC0(%rip), %xmm1
	movhps	.LC1(%rip), %xmm1
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm1, -464(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	xorl	%esi, %esi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	pxor	%xmm0, %xmm0
	xorl	%eax, %eax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	%rbx, -448(%rbp)
	movq	$0, -104(%rbp)
	movw	%ax, -96(%rbp)
	movq	-24(%rbx), %rax
	movq	%rdx, -448(%rbp,%rax)
	movq	-448(%rbp), %rax
	movq	$0, -440(%rbp)
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %r12
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	xorl	%esi, %esi
	leaq	-432(%rbp), %r10
	movq	%r12, -432(%rbp)
	movq	-24(%r12), %rax
	movq	%r10, %rdi
	movq	%r10, -472(%rbp)
	movq	%rdx, -432(%rbp,%rax)
	movq	-432(%rbp), %rax
	addq	-24(%rax), %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	%r15, %rdi
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movdqa	-464(%rbp), %xmm1
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movaps	%xmm1, -432(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	movq	%rax, -320(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r14, %rdi
	leaq	-424(%rbp), %rsi
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movq	%rax, -464(%rbp)
	movq	%rax, -352(%rbp)
	movl	$24, -360(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	-472(%rbp), %r10
	movq	-480(%rbp), %r8
	movq	%r10, %rsi
	movq	%r8, %rdi
	call	_ZNK2v84base5debug10StackTrace14OutputToStreamEPSo@PLT
	leaq	16(%r13), %rax
	movb	$0, 16(%r13)
	movq	%rax, 0(%r13)
	movq	-384(%rbp), %rax
	movq	$0, 8(%r13)
	testq	%rax, %rax
	je	.L15
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	ja	.L21
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L17:
	movq	.LC0(%rip), %xmm0
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-352(%rbp), %rdi
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movhps	.LC2(%rip), %xmm0
	movq	%rax, -320(%rbp)
	movaps	%xmm0, -432(%rbp)
	cmpq	-464(%rbp), %rdi
	je	.L18
	call	_ZdlPv@PLT
.L18:
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%r15, %rdi
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%r14, %rdi
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rdx, -448(%rbp,%rax)
	movq	%r12, -432(%rbp)
	movq	-24(%r12), %rax
	movq	%rcx, -432(%rbp,%rax)
	movq	%rbx, -448(%rbp)
	movq	-24(%rbx), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	movq	%rbx, -448(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	movq	$0, -440(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L22
	addq	$440, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	.cfi_restore_state
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r13, %rdi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L15:
	leaq	-352(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L17
.L22:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3008:
	.size	_ZNK2v84base5debug10StackTrace8ToStringB5cxx11Ev, .-_ZNK2v84base5debug10StackTrace8ToStringB5cxx11Ev
	.section	.data.rel.ro,"aw"
	.align 8
.LC0:
	.quad	_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE+64
	.align 8
.LC1:
	.quad	_ZTVSt15basic_streambufIcSt11char_traitsIcEE+16
	.align 8
.LC2:
	.quad	_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE+16
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
