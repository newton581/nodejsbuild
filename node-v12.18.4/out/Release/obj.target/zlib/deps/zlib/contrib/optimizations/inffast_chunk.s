	.file	"inffast_chunk.c"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"invalid distance too far back"
.LC1:
	.string	"invalid distance code"
.LC2:
	.string	"invalid literal/length code"
	.text
	.p2align 4
	.globl	inflate_fast_chunk_
	.hidden	inflate_fast_chunk_
	.type	inflate_fast_chunk_, @function
inflate_fast_chunk_:
.LFB571:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movl	8(%rbx), %eax
	movq	56(%rdi), %r11
	movq	(%rdi), %rdi
	movq	24(%rbx), %r10
	subl	$7, %eax
	movl	68(%r11), %r15d
	addq	%rdi, %rax
	movq	%r10, %rcx
	movq	%rax, -56(%rbp)
	movl	32(%rbx), %eax
	subl	%eax, %esi
	leal	-257(%rax), %edx
	addq	%r10, %rax
	subq	%rsi, %rcx
	leaq	(%r10,%rdx), %rsi
	movq	%rax, -96(%rbp)
	movl	60(%r11), %eax
	movq	%rsi, -64(%rbp)
	movl	64(%r11), %esi
	movq	%rcx, -104(%rbp)
	movl	%eax, -116(%rbp)
	movl	%esi, -88(%rbp)
	cmpl	%esi, %eax
	ja	.L2
	testl	%r15d, %r15d
	cmove	%eax, %r15d
.L2:
	movl	124(%r11), %ecx
	movl	$1, %r8d
	movq	72(%r11), %rax
	movl	%r15d, -84(%rbp)
	movl	%r8d, %edx
	movl	88(%r11), %esi
	movq	104(%r11), %r9
	sall	%cl, %edx
	movl	120(%r11), %ecx
	movq	112(%r11), %r14
	movq	%rax, -112(%rbp)
	movq	80(%r11), %rax
	sall	%cl, %r8d
	leal	-1(%r8), %ecx
	movl	$1, %r8d
	movq	%rcx, -72(%rbp)
	leal	-1(%rdx), %ecx
	movq	%rcx, -80(%rbp)
.L69:
	cmpl	$14, %esi
	ja	.L4
	movq	(%rdi), %rdx
	movl	%esi, %ecx
	addq	$6, %rdi
	addl	$48, %esi
	salq	%cl, %rdx
	orq	%rdx, %rax
.L4:
	movq	-72(%rbp), %rdx
	andq	%rax, %rdx
	leaq	(%r9,%rdx,4), %rdx
	movzbl	1(%rdx), %ecx
	movzbl	(%rdx), %r12d
	movzwl	2(%rdx), %r13d
	shrq	%cl, %rax
	subl	%ecx, %esi
	movzbl	%r12b, %ecx
	testl	%ecx, %ecx
	je	.L5
	testb	$16, %r12b
	jne	.L6
	testb	$64, %r12b
	jne	.L8
	movl	%r13d, %r15d
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L192:
	testb	$16, %r12b
	jne	.L201
	testb	$64, %r12b
	jne	.L8
.L7:
	movl	%r8d, %edx
	sall	%cl, %edx
	leal	-1(%rdx), %r13d
	movq	%r13, %rdx
	movzwl	%r15w, %r13d
	andq	%rax, %rdx
	addq	%rdx, %r13
	leaq	(%r9,%r13,4), %rdx
	movzbl	1(%rdx), %ecx
	movzbl	(%rdx), %r12d
	movzwl	2(%rdx), %r15d
	shrq	%cl, %rax
	subl	%ecx, %esi
	movzbl	%r12b, %ecx
	testl	%ecx, %ecx
	jne	.L192
	movl	%r15d, %r13d
.L5:
	movb	%r13b, (%r10)
	addq	$1, %r10
.L9:
	cmpq	-56(%rbp), %rdi
	jnb	.L20
	cmpq	-64(%rbp), %r10
	jb	.L69
	.p2align 4,,10
	.p2align 3
.L20:
	movl	%esi, %edx
	andl	$7, %esi
	movq	%r10, 24(%rbx)
	shrl	$3, %edx
	movl	%esi, %ecx
	movq	-56(%rbp), %rsi
	subq	%rdx, %rdi
	movl	$1, %edx
	sall	%cl, %edx
	movq	%rdi, (%rbx)
	subl	$1, %edx
	andq	%rdx, %rax
	cmpq	%rdi, %rsi
	jbe	.L71
	subq	%rdi, %rsi
	movq	%rsi, %rdx
	addl	$7, %edx
.L72:
	movq	-64(%rbp), %rdi
	movl	%edx, 8(%rbx)
	cmpq	%rdi, %r10
	jnb	.L73
	subq	%r10, %rdi
	movq	%rdi, %rdx
	addl	$257, %edx
.L74:
	movl	%edx, 32(%rbx)
	movq	%rax, 80(%r11)
	movl	%ecx, 88(%r11)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L201:
	.cfi_restore_state
	movzwl	%r15w, %r13d
.L6:
	movl	%r12d, %edx
	andl	$15, %edx
	je	.L11
	movzbl	%dl, %r12d
	cmpl	%esi, %r12d
	jbe	.L12
	movq	(%rdi), %r15
	movl	%esi, %ecx
	addq	$6, %rdi
	addl	$48, %esi
	salq	%cl, %r15
	orq	%r15, %rax
.L12:
	movl	%edx, %ecx
	movl	$-1, %r15d
	subl	%r12d, %esi
	sall	%cl, %r15d
	movl	%r15d, %ecx
	notl	%ecx
	andl	%eax, %ecx
	addl	%ecx, %r13d
	movl	%edx, %ecx
	shrq	%cl, %rax
.L11:
	cmpl	$14, %esi
	jbe	.L202
.L13:
	movq	-80(%rbp), %rdx
	andq	%rax, %rdx
	leaq	(%r14,%rdx,4), %rcx
	movzbl	(%rcx), %r12d
	movzwl	2(%rcx), %edx
	movzbl	1(%rcx), %ecx
	shrq	%cl, %rax
	subl	%ecx, %esi
	movzbl	%r12b, %ecx
	testb	$16, %r12b
	jne	.L14
	andl	$64, %r12d
	jne	.L16
	movl	%edx, %r15d
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L191:
	andl	$64, %r12d
	jne	.L16
.L15:
	movl	%r8d, %edx
	sall	%cl, %edx
	subl	$1, %edx
	movq	%rdx, %rcx
	movzwl	%r15w, %edx
	andq	%rax, %rcx
	addq	%rcx, %rdx
	leaq	(%r14,%rdx,4), %rdx
	movzbl	1(%rdx), %ecx
	movzbl	(%rdx), %r12d
	movzwl	2(%rdx), %r15d
	shrq	%cl, %rax
	subl	%ecx, %esi
	movzbl	%r12b, %ecx
	testb	$16, %r12b
	je	.L191
	movl	%r15d, %edx
.L14:
	movl	%r12d, %r15d
	movzwl	%dx, %ecx
	andl	$15, %r12d
	movl	%ecx, -128(%rbp)
	andl	$15, %r15d
	cmpl	%esi, %r12d
	ja	.L203
.L17:
	movl	%r15d, %ecx
	movl	$-1, %edx
	subl	%r12d, %esi
	sall	%cl, %edx
	notl	%edx
	andl	%eax, %edx
	shrq	%cl, %rax
	addl	-128(%rbp), %edx
	movq	%r10, %rcx
	subq	-104(%rbp), %rcx
	cmpl	%ecx, %edx
	jbe	.L18
	movl	%edx, %r12d
	subl	%ecx, %r12d
	cmpl	%r12d, -88(%rbp)
	jnb	.L19
	movl	7144(%r11), %r15d
	testl	%r15d, %r15d
	jne	.L204
.L19:
	movl	-84(%rbp), %r15d
	addl	%r15d, %ecx
	subl	%edx, %ecx
	movl	%ecx, -120(%rbp)
	movq	-96(%rbp), %rcx
	subq	%r10, %rcx
	movq	%rcx, -136(%rbp)
	cmpl	%r12d, %r15d
	jnb	.L205
	movl	-116(%rbp), %ecx
	movl	-120(%rbp), %r15d
	subl	-84(%rbp), %r12d
	addl	%ecx, %r15d
	addq	-112(%rbp), %r15
	movq	%r15, -128(%rbp)
	cmpl	%r13d, %r12d
	jb	.L206
.L23:
	cmpq	$15, -136(%rbp)
	jle	.L207
	movq	-128(%rbp), %r15
	leal	-1(%r13), %edx
	movl	%edx, %ecx
	movdqu	(%r15), %xmm1
	andl	$15, %ecx
	addq	$1, %rcx
	addq	%rcx, %r15
	movups	%xmm1, (%r10)
	addq	%rcx, %r10
	shrl	$4, %edx
	leal	-1(%rdx), %ecx
	je	.L9
	addq	$1, %rcx
	xorl	%edx, %edx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L50:
	movdqu	(%r15,%rdx), %xmm2
	movups	%xmm2, (%r10,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rcx
	jne	.L50
.L199:
	addq	%rcx, %r10
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L8:
	andl	$32, %r12d
	je	.L68
	movl	$16191, 8(%r11)
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L73:
	movl	-64(%rbp), %edx
	subl	%r10d, %edx
	addl	$257, %edx
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L71:
	movl	-56(%rbp), %edx
	subl	%edi, %edx
	addl	$7, %edx
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L202:
	movq	(%rdi), %rdx
	movl	%esi, %ecx
	addq	$6, %rdi
	addl	$48, %esi
	salq	%cl, %rdx
	orq	%rdx, %rax
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L68:
	leaq	.LC2(%rip), %rcx
	movq	%rcx, 48(%rbx)
	movl	$16209, 8(%r11)
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L16:
	leaq	.LC1(%rip), %rcx
	movq	%rcx, 48(%rbx)
	movl	$16209, 8(%r11)
	jmp	.L20
.L203:
	movq	(%rdi), %rdx
	movl	%esi, %ecx
	addq	$6, %rdi
	addl	$48, %esi
	salq	%cl, %rdx
	orq	%rdx, %rax
	jmp	.L17
.L205:
	movl	-120(%rbp), %ecx
	movq	-112(%rbp), %r15
	addq	%r15, %rcx
	movq	%rcx, -128(%rbp)
.L22:
	cmpl	%r13d, %r12d
	jnb	.L23
	cmpq	$15, -136(%rbp)
	jle	.L208
	movq	-128(%rbp), %r15
	leal	-1(%r12), %ecx
	movl	%ecx, -136(%rbp)
	movdqu	(%r15), %xmm7
	movl	%ecx, %r15d
	movq	-128(%rbp), %rcx
	andl	$15, %r15d
	addq	$1, %r15
	movups	%xmm7, (%r10)
	addq	%r15, %rcx
	addq	%r15, %r10
	movq	%rcx, -128(%rbp)
	leal	-1(%r12), %ecx
	shrl	$4, %ecx
	leal	-1(%rcx), %r15d
	je	.L35
	movl	%r15d, %ecx
	movl	%r13d, -120(%rbp)
	movq	-128(%rbp), %r15
	addq	$1, %rcx
	salq	$4, %rcx
	movq	%rcx, -136(%rbp)
	movq	-136(%rbp), %r13
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L36:
	movdqu	(%r15,%rcx), %xmm5
	movups	%xmm5, (%r10,%rcx)
	addq	$16, %rcx
	cmpq	%rcx, %r13
	jne	.L36
	movl	-120(%rbp), %r13d
	addq	-136(%rbp), %r10
.L35:
	movl	%r13d, %ecx
	movq	%r10, %r13
	subl	%r12d, %ecx
	movl	%edx, %r12d
	subq	%r12, %r13
	cmpl	$15, %edx
	ja	.L37
	cmpl	%ecx, %edx
	jb	.L38
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L209:
	cmpl	$15, %edx
	ja	.L80
	movl	%edx, %r12d
.L38:
	movdqu	0(%r13), %xmm4
	subl	%edx, %ecx
	addl	%edx, %edx
	movups	%xmm4, (%r10)
	addq	%r12, %r10
	cmpl	%edx, %ecx
	ja	.L209
.L80:
	movq	%r10, %r13
	subq	%rdx, %r13
.L37:
	movq	-96(%rbp), %rdx
	subq	%r10, %rdx
	cmpq	$15, %rdx
	jle	.L210
	subl	$1, %ecx
	movdqu	0(%r13), %xmm1
	movl	%ecx, %edx
	andl	$15, %edx
	movups	%xmm1, (%r10)
	addq	$1, %rdx
	addq	%rdx, %r10
	addq	%rdx, %r13
	shrl	$4, %ecx
	leal	-1(%rcx), %edx
	je	.L9
	movl	%edx, %ecx
	xorl	%edx, %edx
	addq	$1, %rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L44:
	movdqu	0(%r13,%rdx), %xmm6
	movups	%xmm6, (%r10,%rdx)
	addq	$16, %rdx
	cmpq	%rcx, %rdx
	jne	.L44
	addq	%rdx, %r10
	jmp	.L9
.L18:
	leal	-1(%r13), %r12d
	movl	%r12d, %ecx
	andl	$15, %ecx
	addl	$1, %ecx
	cmpl	%r13d, %edx
	jnb	.L51
	cmpl	$15, %edx
	jbe	.L211
.L51:
	movq	%r10, %r15
	subq	%rdx, %r15
	movdqu	(%r15), %xmm1
	movups	%xmm1, (%r10)
	addq	%rcx, %r10
	addq	%r15, %rcx
	shrl	$4, %r12d
	leal	-1(%r12), %edx
	je	.L9
	movl	%edx, %r12d
	xorl	%edx, %edx
	addq	$1, %r12
	salq	$4, %r12
	.p2align 4,,10
	.p2align 3
.L65:
	movdqu	(%rcx,%rdx), %xmm3
	movups	%xmm3, (%r10,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %r12
	jne	.L65
	addq	%r12, %r10
	jmp	.L9
.L207:
	testb	$8, %r13b
	je	.L47
	movq	-128(%rbp), %rcx
	addq	$8, %r10
	movq	(%rcx), %rdx
	addq	$8, %rcx
	movq	%rcx, -128(%rbp)
	movq	%rdx, -8(%r10)
.L47:
	testb	$4, %r13b
	je	.L48
	movq	-128(%rbp), %rcx
	addq	$4, %r10
	movl	(%rcx), %edx
	addq	$4, %rcx
	movq	%rcx, -128(%rbp)
	movl	%edx, -4(%r10)
.L48:
	testb	$2, %r13b
	je	.L49
	movq	-128(%rbp), %rcx
	addq	$2, %r10
	movzwl	(%rcx), %edx
	addq	$2, %rcx
	movq	%rcx, -128(%rbp)
	movw	%dx, -2(%r10)
.L49:
	andl	$1, %r13d
	je	.L9
	movq	-128(%rbp), %rcx
	addq	$1, %r10
	movzbl	(%rcx), %edx
	movb	%dl, -1(%r10)
	jmp	.L9
.L211:
	cmpl	$4, %edx
	je	.L52
	ja	.L53
	cmpl	$1, %edx
	je	.L54
	cmpl	$2, %edx
	jne	.L56
	movzwl	-2(%r10), %edx
	movd	%edx, %xmm0
	movl	%ecx, %edx
	punpcklwd	%xmm0, %xmm0
	addq	%r10, %rdx
	pshufd	$0, %xmm0, %xmm0
	movups	%xmm0, (%r10)
	movq	%rdx, %r10
	subl	%ecx, %r13d
	je	.L9
	movzwl	-2(%rdx), %edx
	movd	%edx, %xmm0
	leal	0(%r13,%r10), %edx
	punpcklwd	%xmm0, %xmm0
	pshufd	$0, %xmm0, %xmm0
.L59:
	movups	%xmm0, (%r10)
	addq	$16, %r10
	cmpl	%r10d, %edx
	jne	.L59
	jmp	.L9
.L208:
	testb	$8, %r12b
	je	.L32
	movq	-128(%rbp), %r15
	addq	$8, %r10
	movq	(%r15), %rcx
	addq	$8, %r15
	movq	%r15, -128(%rbp)
	movq	%rcx, -8(%r10)
.L32:
	testb	$4, %r12b
	je	.L33
	movq	-128(%rbp), %r15
	addq	$4, %r10
	movl	(%r15), %ecx
	addq	$4, %r15
	movq	%r15, -128(%rbp)
	movl	%ecx, -4(%r10)
.L33:
	testb	$2, %r12b
	je	.L34
	movq	-128(%rbp), %r15
	addq	$2, %r10
	movzwl	(%r15), %ecx
	addq	$2, %r15
	movq	%r15, -128(%rbp)
	movw	%cx, -2(%r10)
.L34:
	testb	$1, %r12b
	je	.L35
	movq	-128(%rbp), %rcx
	addq	$1, %r10
	movzbl	(%rcx), %ecx
	movb	%cl, -1(%r10)
	jmp	.L35
.L204:
	leaq	.LC0(%rip), %rcx
	movq	%rcx, 48(%rbx)
	movl	$16209, 8(%r11)
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L206:
	addl	-120(%rbp), %r13d
	cmpq	$15, -136(%rbp)
	jle	.L212
	subl	$1, %r12d
	movdqu	(%r15), %xmm7
	movl	%r12d, %ecx
	andl	$15, %ecx
	movups	%xmm7, (%r10)
	addq	$1, %rcx
	addq	%rcx, %r10
	addq	%rcx, %r15
	shrl	$4, %r12d
	leal	-1(%r12), %ecx
	je	.L28
	movl	%ecx, %r12d
	xorl	%ecx, %ecx
	addq	$1, %r12
	salq	$4, %r12
	.p2align 4,,10
	.p2align 3
.L30:
	movdqu	(%r15,%rcx), %xmm7
	movups	%xmm7, (%r10,%rcx)
	addq	$16, %rcx
	cmpq	%rcx, %r12
	jne	.L30
	addq	%r12, %r10
.L28:
	movq	-96(%rbp), %rcx
	movl	-84(%rbp), %r12d
	subq	%r10, %rcx
	movq	%rcx, -136(%rbp)
	movq	-112(%rbp), %rcx
	movq	%rcx, -128(%rbp)
	jmp	.L22
.L53:
	cmpl	$8, %edx
	jne	.L56
	movq	-8(%r10), %xmm0
	movl	%ecx, %edx
	addq	%r10, %rdx
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, (%r10)
	movq	%rdx, %r10
	subl	%ecx, %r13d
	je	.L9
	movq	-8(%rdx), %xmm0
	leal	0(%r13,%rdx), %edx
	punpcklqdq	%xmm0, %xmm0
.L61:
	movups	%xmm0, (%r10)
	addq	$16, %r10
	cmpl	%r10d, %edx
	jne	.L61
	jmp	.L9
.L210:
	testb	$8, %cl
	je	.L41
	movq	0(%r13), %rdx
	addq	$8, %r10
	addq	$8, %r13
	movq	%rdx, -8(%r10)
.L41:
	testb	$4, %cl
	je	.L42
	movl	0(%r13), %edx
	addq	$4, %r10
	addq	$4, %r13
	movl	%edx, -4(%r10)
.L42:
	testb	$2, %cl
	je	.L43
	movzwl	0(%r13), %edx
	addq	$2, %r10
	addq	$2, %r13
	movw	%dx, -2(%r10)
.L43:
	andl	$1, %ecx
	je	.L9
	movzbl	0(%r13), %edx
	addq	$1, %r10
	movb	%dl, -1(%r10)
	jmp	.L9
.L212:
	testb	$8, %r12b
	je	.L25
	movq	(%r15), %rcx
	addq	$8, %r15
	addq	$8, %r10
	movq	%r15, -128(%rbp)
	movq	%rcx, -8(%r10)
.L25:
	testb	$4, %r12b
	je	.L26
	movq	-128(%rbp), %r15
	addq	$4, %r10
	movl	(%r15), %ecx
	addq	$4, %r15
	movq	%r15, -128(%rbp)
	movl	%ecx, -4(%r10)
.L26:
	testb	$2, %r12b
	je	.L27
	movq	-128(%rbp), %r15
	addq	$2, %r10
	movzwl	(%r15), %ecx
	addq	$2, %r15
	movq	%r15, -128(%rbp)
	movw	%cx, -2(%r10)
.L27:
	andl	$1, %r12d
	je	.L28
	movq	-128(%rbp), %r15
	leaq	1(%r10), %rcx
	movzbl	(%r15), %r12d
	movb	%r12b, (%r10)
	movq	-96(%rbp), %r10
	movl	-84(%rbp), %r12d
	subq	%rcx, %r10
	movq	%r10, -136(%rbp)
	movq	-112(%rbp), %r10
	movq	%r10, -128(%rbp)
	movq	%rcx, %r10
	jmp	.L22
.L52:
	movd	-4(%r10), %xmm1
	movl	%ecx, %edx
	addq	%r10, %rdx
	pshufd	$0, %xmm1, %xmm0
	movups	%xmm0, (%r10)
	movq	%rdx, %r10
	subl	%ecx, %r13d
	je	.L9
	movd	-4(%rdx), %xmm7
	leal	0(%r13,%rdx), %edx
	pshufd	$0, %xmm7, %xmm0
.L60:
	movups	%xmm0, (%r10)
	addq	$16, %r10
	cmpl	%r10d, %edx
	jne	.L60
	jmp	.L9
.L56:
	movl	%edx, %r12d
	movq	%r10, %rcx
	subq	%r12, %rcx
	jmp	.L62
.L213:
	cmpl	%edx, %r13d
	jbe	.L81
	movl	%edx, %r12d
.L62:
	movdqu	(%rcx), %xmm7
	subl	%edx, %r13d
	addl	%edx, %edx
	movups	%xmm7, (%r10)
	addq	%r12, %r10
	cmpl	$15, %edx
	jbe	.L213
.L81:
	movq	%r10, %r12
	subq	%rdx, %r12
	leal	-1(%r13), %edx
	movl	%edx, %ecx
	movdqu	(%r12), %xmm1
	andl	$15, %ecx
	addq	$1, %rcx
	movups	%xmm1, (%r10)
	addq	%rcx, %r12
	addq	%rcx, %r10
	shrl	$4, %edx
	leal	-1(%rdx), %ecx
	je	.L9
	addq	$1, %rcx
	xorl	%edx, %edx
	salq	$4, %rcx
.L64:
	movdqu	(%r12,%rdx), %xmm1
	movups	%xmm1, (%r10,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rcx
	jne	.L64
	jmp	.L199
.L54:
	movzbl	-1(%r10), %edx
	movd	%edx, %xmm0
	movl	%ecx, %edx
	punpcklbw	%xmm0, %xmm0
	punpcklwd	%xmm0, %xmm0
	pshufd	$0, %xmm0, %xmm0
	movups	%xmm0, (%r10)
	addq	%rdx, %r10
	movl	%r13d, %edx
	movdqa	%xmm0, %xmm1
	subl	%ecx, %edx
	je	.L9
	addl	%r10d, %edx
.L58:
	movups	%xmm1, (%r10)
	addq	$16, %r10
	cmpl	%r10d, %edx
	jne	.L58
	jmp	.L9
	.cfi_endproc
.LFE571:
	.size	inflate_fast_chunk_, .-inflate_fast_chunk_
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
