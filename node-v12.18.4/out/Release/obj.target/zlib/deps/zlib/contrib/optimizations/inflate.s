	.file	"inflate.c"
	.text
	.p2align 4
	.globl	inflateResetKeep
	.type	inflateResetKeep, @function
inflateResetKeep:
.LFB572:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L6
	cmpq	$0, 64(%rdi)
	je	.L6
	cmpq	$0, 72(%rdi)
	je	.L6
	movq	56(%rdi), %rax
	movl	$-2, %r8d
	testq	%rax, %rax
	je	.L1
	cmpq	(%rax), %rdi
	jne	.L1
	movl	8(%rax), %esi
	leal	-16180(%rsi), %edx
	cmpl	$31, %edx
	jbe	.L14
.L1:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	movl	16(%rax), %edx
	movq	$0, 40(%rax)
	movq	$0, 40(%rdi)
	movq	$0, 16(%rdi)
	movq	$0, 48(%rdi)
	testl	%edx, %edx
	je	.L3
	andl	$1, %edx
	movq	%rdx, 96(%rdi)
.L3:
	leaq	1368(%rax), %rdx
	xorl	%r8d, %r8d
	movabsq	$-4294967295, %rcx
	movq	$16180, 8(%rax)
	movq	%rdx, %xmm0
	movl	$0, 20(%rax)
	punpcklqdq	%xmm0, %xmm0
	movl	$32768, 28(%rax)
	movq	$0, 48(%rax)
	movq	$0, 80(%rax)
	movl	$0, 88(%rax)
	movq	%rdx, 144(%rax)
	movq	%rcx, 7144(%rax)
	movups	%xmm0, 104(%rax)
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	movl	$-2, %r8d
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE572:
	.size	inflateResetKeep, .-inflateResetKeep
	.p2align 4
	.globl	inflateReset
	.type	inflateReset, @function
inflateReset:
.LFB573:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L20
	cmpq	$0, 64(%rdi)
	je	.L20
	cmpq	$0, 72(%rdi)
	je	.L20
	movq	56(%rdi), %rax
	movl	$-2, %r8d
	testq	%rax, %rax
	je	.L15
	cmpq	(%rax), %rdi
	jne	.L15
	movl	8(%rax), %esi
	leal	-16180(%rsi), %edx
	cmpl	$31, %edx
	jbe	.L27
.L15:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L27:
	movl	16(%rax), %edx
	movl	$0, 60(%rax)
	movq	$0, 64(%rax)
	movq	$0, 40(%rax)
	movq	$0, 40(%rdi)
	movq	$0, 16(%rdi)
	movq	$0, 48(%rdi)
	testl	%edx, %edx
	je	.L17
	andl	$1, %edx
	movq	%rdx, 96(%rdi)
.L17:
	leaq	1368(%rax), %rdx
	xorl	%r8d, %r8d
	movabsq	$-4294967295, %rcx
	movq	$16180, 8(%rax)
	movq	%rdx, %xmm0
	movl	$0, 20(%rax)
	punpcklqdq	%xmm0, %xmm0
	movl	$32768, 28(%rax)
	movq	$0, 48(%rax)
	movq	$0, 80(%rax)
	movl	$0, 88(%rax)
	movq	%rdx, 144(%rax)
	movq	%rcx, 7144(%rax)
	movups	%xmm0, 104(%rax)
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	movl	$-2, %r8d
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE573:
	.size	inflateReset, .-inflateReset
	.p2align 4
	.globl	inflateReset2
	.type	inflateReset2, @function
inflateReset2:
.LFB574:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L63
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	cmpq	$0, 64(%rdi)
	movq	%rdi, %rbx
	je	.L31
	movq	72(%rdi), %rax
	testq	%rax, %rax
	je	.L31
	movq	56(%rdi), %r13
	testq	%r13, %r13
	je	.L31
	cmpq	0(%r13), %rdi
	jne	.L31
	movl	8(%r13), %ecx
	leal	-16180(%rcx), %edx
	cmpl	$31, %edx
	jbe	.L64
.L31:
	popq	%rbx
	movl	$-2, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L64:
	.cfi_restore_state
	movl	%esi, %r12d
	testl	%esi, %esi
	js	.L65
	movl	%esi, %r14d
	movl	%esi, %edx
	sarl	$4, %r14d
	andl	$15, %edx
	addl	$5, %r14d
	cmpl	$48, %esi
	cmovl	%edx, %r12d
.L33:
	leal	-8(%r12), %edx
	cmpl	$7, %edx
	jbe	.L38
	testl	%r12d, %r12d
	jne	.L31
.L38:
	movq	72(%r13), %rsi
	testq	%rsi, %rsi
	je	.L35
	cmpl	56(%r13), %r12d
	je	.L35
	movq	80(%rbx), %rdi
	call	*%rax
	movq	64(%rbx), %rax
	movq	$0, 72(%r13)
	movl	%r14d, 16(%r13)
	movl	%r12d, 56(%r13)
	testq	%rax, %rax
	je	.L31
.L37:
	cmpq	$0, 72(%rbx)
	je	.L31
	movq	56(%rbx), %rax
	testq	%rax, %rax
	je	.L31
	cmpq	(%rax), %rbx
	jne	.L31
	movl	8(%rax), %ecx
	leal	-16180(%rcx), %edx
	cmpl	$31, %edx
	ja	.L31
	movl	16(%rax), %edx
	movl	$0, 60(%rax)
	movq	$0, 64(%rax)
	movq	$0, 40(%rax)
	movq	$0, 40(%rbx)
	movq	$0, 16(%rbx)
	movq	$0, 48(%rbx)
	testl	%edx, %edx
	je	.L36
	andl	$1, %edx
	movq	%rdx, 96(%rbx)
.L36:
	leaq	1368(%rax), %rdx
	movq	$16180, 8(%rax)
	movabsq	$-4294967295, %rcx
	movq	%rdx, %xmm0
	movl	$0, 20(%rax)
	punpcklqdq	%xmm0, %xmm0
	movl	$32768, 28(%rax)
	movq	$0, 48(%rax)
	movq	$0, 80(%rax)
	movl	$0, 88(%rax)
	movq	%rdx, 144(%rax)
	movq	%rcx, 7144(%rax)
	movups	%xmm0, 104(%rax)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L35:
	.cfi_restore_state
	movl	%r14d, 16(%r13)
	movl	%r12d, 56(%r13)
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L63:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	movl	$-2, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L65:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	negl	%r12d
	xorl	%r14d, %r14d
	jmp	.L33
	.cfi_endproc
.LFE574:
	.size	inflateReset2, .-inflateReset2
	.p2align 4
	.globl	inflateInit2_
	.type	inflateInit2_, @function
inflateInit2_:
.LFB575:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	testq	%rdx, %rdx
	je	.L72
	cmpb	$49, (%rdx)
	jne	.L72
	cmpl	$112, %ecx
	jne	.L72
	movq	%rdi, %rbx
	testq	%rdi, %rdi
	je	.L73
	movq	64(%rdi), %rax
	movq	$0, 48(%rdi)
	movl	%esi, %r13d
	testq	%rax, %rax
	je	.L68
	movq	80(%rdi), %rdi
.L69:
	cmpq	$0, 72(%rbx)
	je	.L79
.L70:
	movl	$7160, %edx
	movl	$1, %esi
	call	*%rax
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L74
	movq	%rax, 56(%rbx)
	movl	%r13d, %esi
	movq	%rbx, %rdi
	movq	%rbx, (%rax)
	movq	$0, 72(%rax)
	movl	$16180, 8(%rax)
	movq	$1, 32(%rax)
	call	inflateReset2
	movl	%eax, %r13d
	testl	%eax, %eax
	je	.L66
	movq	80(%rbx), %rdi
	movq	%r12, %rsi
	call	*72(%rbx)
	movq	$0, 56(%rbx)
.L66:
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L79:
	.cfi_restore_state
	leaq	zcfree(%rip), %rcx
	movq	%rcx, 72(%rbx)
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L68:
	leaq	zcalloc(%rip), %rax
	movq	$0, 80(%rdi)
	movq	%rax, 64(%rdi)
	xorl	%edi, %edi
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L72:
	movl	$-6, %r13d
	jmp	.L66
.L74:
	movl	$-4, %r13d
	jmp	.L66
.L73:
	movl	$-2, %r13d
	jmp	.L66
	.cfi_endproc
.LFE575:
	.size	inflateInit2_, .-inflateInit2_
	.p2align 4
	.globl	inflateInit_
	.type	inflateInit_, @function
inflateInit_:
.LFB576:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	testq	%rsi, %rsi
	je	.L87
	cmpb	$49, (%rsi)
	jne	.L87
	cmpl	$112, %edx
	jne	.L87
	movq	%rdi, %rbx
	testq	%rdi, %rdi
	je	.L88
	movq	64(%rdi), %rax
	movq	$0, 48(%rdi)
	testq	%rax, %rax
	je	.L82
	cmpq	$0, 72(%rbx)
	movq	80(%rdi), %rdi
	je	.L96
.L84:
	movl	$7160, %edx
	movl	$1, %esi
	call	*%rax
	movq	%rax, %r9
	testq	%rax, %rax
	je	.L89
	cmpq	$0, 64(%rbx)
	movq	%rax, 56(%rbx)
	movq	%rbx, (%rax)
	movq	$0, 72(%rax)
	movl	$16180, 8(%rax)
	movq	$1, 32(%rax)
	movq	72(%rbx), %rax
	je	.L91
	testq	%rax, %rax
	je	.L91
	movl	$5, 16(%r9)
	movq	%rbx, %rdi
	movl	$15, 56(%r9)
	call	inflateReset
	movl	%eax, %r12d
	testl	%eax, %eax
	jne	.L97
.L80:
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L82:
	.cfi_restore_state
	leaq	zcalloc(%rip), %rax
	movq	$0, 80(%rdi)
	movq	%rax, 64(%rdi)
	xorl	%edi, %edi
	cmpq	$0, 72(%rbx)
	jne	.L84
.L96:
	leaq	zcfree(%rip), %rcx
	movq	%rcx, 72(%rbx)
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L91:
	movl	$-2, %r12d
.L85:
	movq	80(%rbx), %rdi
	movq	%r9, %rsi
	call	*%rax
	movq	$0, 56(%rbx)
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L97:
	.cfi_restore_state
	movq	72(%rbx), %rax
	jmp	.L85
	.p2align 4,,10
	.p2align 3
.L87:
	movl	$-6, %r12d
	jmp	.L80
.L89:
	movl	$-4, %r12d
	jmp	.L80
.L88:
	movl	$-2, %r12d
	jmp	.L80
	.cfi_endproc
.LFE576:
	.size	inflateInit_, .-inflateInit_
	.p2align 4
	.globl	inflatePrime
	.type	inflatePrime, @function
inflatePrime:
.LFB577:
	.cfi_startproc
	endbr64
	movl	%esi, %ecx
	movl	%edx, %r8d
	testq	%rdi, %rdi
	je	.L103
	cmpq	$0, 64(%rdi)
	je	.L103
	cmpq	$0, 72(%rdi)
	je	.L103
	movq	56(%rdi), %rsi
	movl	$-2, %eax
	testq	%rsi, %rsi
	je	.L98
	cmpq	(%rsi), %rdi
	jne	.L98
	movl	8(%rsi), %edx
	subl	$16180, %edx
	cmpl	$31, %edx
	jbe	.L109
.L98:
	ret
	.p2align 4,,10
	.p2align 3
.L109:
	testl	%ecx, %ecx
	js	.L110
	cmpl	$16, %ecx
	jg	.L98
	movl	88(%rsi), %r9d
	leal	(%rcx,%r9), %edi
	cmpl	$32, %edi
	ja	.L98
	movl	$1, %eax
	movl	%edi, 88(%rsi)
	salq	%cl, %rax
	movl	%r9d, %ecx
	leal	-1(%rax), %edx
	xorl	%eax, %eax
	andl	%r8d, %edx
	sall	%cl, %edx
	addq	%rdx, 80(%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L103:
	movl	$-2, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L110:
	movq	$0, 80(%rsi)
	xorl	%eax, %eax
	movl	$0, 88(%rsi)
	ret
	.cfi_endproc
.LFE577:
	.size	inflatePrime, .-inflatePrime
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"incorrect header check"
.LC1:
	.string	"unknown compression method"
.LC2:
	.string	"invalid window size"
.LC3:
	.string	"unknown header flags set"
.LC4:
	.string	"header crc mismatch"
.LC5:
	.string	"invalid block type"
.LC6:
	.string	"invalid stored block lengths"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC7:
	.string	"too many length or distance symbols"
	.section	.rodata.str1.1
.LC8:
	.string	"invalid code lengths set"
.LC9:
	.string	"invalid bit length repeat"
	.section	.rodata.str1.8
	.align 8
.LC10:
	.string	"invalid code -- missing end-of-block"
	.section	.rodata.str1.1
.LC11:
	.string	"invalid literal/lengths set"
.LC12:
	.string	"invalid distances set"
.LC13:
	.string	"invalid literal/length code"
.LC14:
	.string	"invalid distance code"
.LC15:
	.string	"invalid distance too far back"
.LC16:
	.string	"incorrect data check"
.LC17:
	.string	"incorrect length check"
	.text
	.p2align 4
	.globl	inflate
	.type	inflate, @function
inflate:
.LFB580:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%esi, -84(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L381
	cmpq	$0, 64(%rdi)
	movq	%rdi, %r11
	je	.L381
	cmpq	$0, 72(%rdi)
	je	.L381
	movq	56(%rdi), %r13
	movl	$-2, %r8d
	testq	%r13, %r13
	je	.L111
	cmpq	0(%r13), %rdi
	jne	.L111
	movl	8(%r13), %edx
	leal	-16180(%rdx), %eax
	cmpl	$31, %eax
	jbe	.L773
	.p2align 4,,10
	.p2align 3
.L111:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L774
	addq	$120, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L773:
	.cfi_restore_state
	movq	24(%rdi), %rbx
	movq	%rbx, -72(%rbp)
	testq	%rbx, %rbx
	je	.L111
	movq	(%rdi), %r12
	movl	8(%rdi), %edi
	movl	%edi, -92(%rbp)
	testq	%r12, %r12
	je	.L775
.L113:
	cmpl	$16191, %edx
	jne	.L114
	movl	$16192, 8(%r13)
	movl	$12, %eax
.L114:
	movl	-84(%rbp), %edi
	movl	32(%r11), %esi
	leaq	distfix.6368(%rip), %rdx
	leaq	lenfix.6367(%rip), %rcx
	movl	-92(%rbp), %r10d
	movq	%rcx, %xmm2
	movq	%rdx, %xmm3
	movq	80(%r13), %r15
	subl	$5, %edi
	movl	%esi, -88(%rbp)
	punpcklqdq	%xmm3, %xmm2
	movl	88(%r13), %ebx
	movl	%edi, -104(%rbp)
	movl	%r10d, %r14d
	movl	%esi, -80(%rbp)
	movaps	%xmm2, -128(%rbp)
.L115:
	cmpl	$30, %eax
	ja	.L381
	leaq	.L117(%rip), %rdi
	movslq	(%rdi,%rax,4), %rax
	addq	%rdi, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L117:
	.long	.L147-.L117
	.long	.L146-.L117
	.long	.L145-.L117
	.long	.L144-.L117
	.long	.L143-.L117
	.long	.L142-.L117
	.long	.L141-.L117
	.long	.L140-.L117
	.long	.L139-.L117
	.long	.L138-.L117
	.long	.L137-.L117
	.long	.L136-.L117
	.long	.L135-.L117
	.long	.L134-.L117
	.long	.L133-.L117
	.long	.L132-.L117
	.long	.L131-.L117
	.long	.L130-.L117
	.long	.L129-.L117
	.long	.L128-.L117
	.long	.L127-.L117
	.long	.L126-.L117
	.long	.L125-.L117
	.long	.L124-.L117
	.long	.L123-.L117
	.long	.L122-.L117
	.long	.L739-.L117
	.long	.L120-.L117
	.long	.L387-.L117
	.long	.L749-.L117
	.long	.L388-.L117
	.text
	.p2align 4,,10
	.p2align 3
.L381:
	movl	$-2, %r8d
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L147:
	movl	16(%r13), %edx
	testl	%edx, %edx
	jne	.L776
	movl	$16192, 8(%r13)
.L135:
	movl	12(%r13), %r8d
	testl	%r8d, %r8d
	je	.L777
	movl	%ebx, %ecx
	movl	$16206, 8(%r13)
	movl	%r14d, %r10d
	andl	$-8, %ebx
	andl	$7, %ecx
	shrq	%cl, %r15
.L121:
	movl	16(%r13), %esi
	testl	%esi, %esi
	je	.L344
	cmpl	$31, %ebx
	ja	.L468
	testl	%r10d, %r10d
	je	.L768
	movzbl	(%r12), %eax
	movl	%ebx, %ecx
	leal	-1(%r10), %edx
	leaq	1(%r12), %r14
	salq	%cl, %rax
	leal	8(%rbx), %ecx
	addq	%rax, %r15
	cmpl	$31, %ecx
	ja	.L474
	testl	%edx, %edx
	je	.L475
	movzbl	1(%r12), %eax
	leal	-2(%r10), %edx
	leaq	2(%r12), %r14
	salq	%cl, %rax
	leal	16(%rbx), %ecx
	addq	%rax, %r15
	cmpl	$31, %ecx
	ja	.L474
	testl	%edx, %edx
	je	.L475
	movzbl	2(%r12), %eax
	leal	-3(%r10), %edx
	leaq	3(%r12), %r14
	salq	%cl, %rax
	leal	24(%rbx), %ecx
	addq	%rax, %r15
	cmpl	$31, %ecx
	ja	.L474
	testl	%edx, %edx
	je	.L475
	movzbl	3(%r12), %eax
	subl	$4, %r10d
	leaq	4(%r12), %r14
	addl	$32, %ebx
	salq	%cl, %rax
	addq	%rax, %r15
.L345:
	movl	-88(%rbp), %edx
	movl	%esi, %ecx
	subl	-80(%rbp), %edx
	movl	%edx, %eax
	addq	%rax, 40(%r11)
	addq	%rax, 40(%r13)
	andl	$4, %ecx
	je	.L478
	testl	%edx, %edx
	jne	.L778
.L346:
	testl	%ecx, %ecx
	je	.L478
	movl	24(%r13), %r9d
	movq	%r15, %rax
	testl	%r9d, %r9d
	jne	.L349
	shrq	$24, %rax
	movq	%r15, %rcx
	movzbl	%al, %edx
	movq	%r15, %rax
	salq	$24, %rcx
	shrq	$8, %rax
	movl	%ecx, %ecx
	andl	$65280, %eax
	orq	%rdx, %rax
	movq	%r15, %rdx
	salq	$8, %rdx
	andl	$16711680, %edx
	addq	%rcx, %rdx
	addq	%rdx, %rax
.L349:
	cmpq	%rax, 32(%r13)
	je	.L478
	leaq	.LC16(%rip), %rax
	movq	%r14, %r12
	movq	%rax, 48(%r11)
	movl	-80(%rbp), %eax
	movl	$16209, 8(%r13)
	movl	%eax, -88(%rbp)
	jmp	.L118
.L278:
	movl	%eax, 7148(%r13)
	shrq	%cl, %r15
	subl	%eax, %ebx
	movl	%esi, 92(%r13)
.L377:
	movl	$16205, 8(%r13)
.L122:
	movl	-80(%rbp), %edx
	testl	%edx, %edx
	je	.L467
	movq	-72(%rbp), %rsi
	movl	92(%r13), %eax
	subl	$1, %edx
	movl	%edx, -80(%rbp)
	movb	%al, (%rsi)
	leaq	1(%rsi), %rax
	movl	$16200, 8(%r13)
	movq	%rax, -72(%rbp)
.L127:
	cmpl	$7, %r14d
	jbe	.L274
	movl	-80(%rbp), %eax
	cmpl	$257, %eax
	jbe	.L274
	movq	-72(%rbp), %rdx
	movl	-88(%rbp), %esi
	movl	%eax, 32(%r11)
	movq	%r11, %rdi
	movq	%r12, (%r11)
	movl	%r14d, 8(%r11)
	movq	%rdx, 24(%r11)
	movq	%r15, 80(%r13)
	movl	%ebx, 88(%r13)
	movq	%r11, -72(%rbp)
	call	inflate_fast_chunk_
	movq	-72(%rbp), %r11
	movq	80(%r13), %r15
	movl	88(%r13), %ebx
	movq	24(%r11), %rax
	movq	(%r11), %r12
	movl	8(%r11), %r14d
	movq	%rax, -72(%rbp)
	movl	32(%r11), %eax
	movl	%eax, -80(%rbp)
	movl	8(%r13), %eax
	cmpl	$16191, %eax
	je	.L779
.L234:
	subl	$16180, %eax
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L124:
	movl	100(%r13), %esi
.L149:
	testl	%esi, %esi
	jne	.L780
.L300:
	movl	$16204, 8(%r13)
.L123:
	movl	-80(%rbp), %edi
	testl	%edi, %edi
	je	.L467
	movl	-88(%rbp), %ecx
	movl	96(%r13), %eax
	movl	%ecx, %edx
	subl	%edi, %edx
	cmpl	%edx, %eax
	jbe	.L305
	addl	%edi, %eax
	movl	%eax, %esi
	subl	%ecx, %esi
	cmpl	%esi, 64(%r13)
	jb	.L781
.L306:
	movl	-88(%rbp), %edi
	movl	68(%r13), %ecx
	movq	72(%r13), %rdx
	subl	%eax, %edi
	movl	%edi, %eax
	cmpl	%esi, %ecx
	jnb	.L307
	subl	%ecx, %esi
	addl	60(%r13), %ecx
	addl	%ecx, %eax
	addq	%rax, %rdx
.L308:
	movl	-80(%rbp), %edi
	cmpl	%edi, 92(%r13)
	movl	%edi, %eax
	cmovbe	92(%r13), %eax
	cmpl	%esi, %eax
	cmovbe	%eax, %esi
	movl	%edi, %eax
	subl	%esi, %edi
	cmpl	$15, %eax
	jbe	.L782
	leal	-1(%rsi), %ecx
	movq	-72(%rbp), %r9
	movdqu	(%rdx), %xmm7
	movl	%ecx, %eax
	andl	$15, %eax
	movups	%xmm7, (%r9)
	addq	$1, %rax
	addq	%rax, %r9
	addq	%rax, %rdx
	shrl	$4, %ecx
	movq	%r9, -72(%rbp)
	leal	-1(%rcx), %eax
	je	.L313
	movl	%eax, %ecx
	movq	%r9, %r8
	xorl	%eax, %eax
	addq	$1, %rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L316:
	movdqu	(%rdx,%rax), %xmm4
	movups	%xmm4, (%r8,%rax)
	addq	$16, %rax
	cmpq	%rcx, %rax
	jne	.L316
	addq	%rax, -72(%rbp)
	movl	92(%r13), %ecx
	subl	%esi, %ecx
.L314:
	movl	%ecx, 92(%r13)
	testl	%ecx, %ecx
	je	.L343
	movl	%edi, -80(%rbp)
	movl	8(%r13), %eax
	jmp	.L234
	.p2align 4,,10
	.p2align 3
.L126:
	movl	100(%r13), %edx
.L150:
	testl	%edx, %edx
	jne	.L286
	movl	92(%r13), %eax
.L287:
	movl	%eax, 7152(%r13)
	movl	$16202, 8(%r13)
.L125:
	movl	124(%r13), %ecx
	movl	$-1, %edi
	movq	112(%r13), %r9
	movl	%r15d, -112(%rbp)
	sall	%cl, %edi
	notl	%edi
	movl	%edi, %eax
	andl	%r15d, %eax
	leaq	(%r9,%rax,4), %rax
	movzbl	1(%rax), %edx
	movzbl	(%rax), %esi
	movzwl	2(%rax), %r10d
	movzbl	%dl, %eax
	movl	%eax, %r8d
	cmpl	%eax, %ebx
	jnb	.L291
	testl	%r14d, %r14d
	je	.L461
	movl	%ebx, %ecx
	jmp	.L292
	.p2align 4,,10
	.p2align 3
.L293:
	testl	%r14d, %r14d
	je	.L463
.L292:
	movzbl	(%r12), %eax
	addq	$1, %r12
	subl	$1, %r14d
	salq	%cl, %rax
	addl	$8, %ecx
	addq	%rax, %r15
	movl	%edi, %eax
	andl	%r15d, %eax
	leaq	(%r9,%rax,4), %rax
	movzbl	1(%rax), %edx
	movzbl	(%rax), %esi
	movzwl	2(%rax), %r10d
	movzbl	%dl, %eax
	movl	%eax, %r8d
	cmpl	%ecx, %eax
	ja	.L293
	movl	%r15d, -112(%rbp)
	movl	%ecx, %ebx
.L291:
	movl	%eax, %ecx
	testb	$-16, %sil
	je	.L294
	movl	7148(%r13), %eax
.L295:
	addl	%ecx, %eax
	shrq	%cl, %r15
	subl	%r8d, %ebx
	movl	%eax, 7148(%r13)
	testb	$64, %sil
	je	.L299
	leaq	.LC14(%rip), %rax
	movl	%r14d, %r10d
	movq	%rax, 48(%r11)
	movl	$16209, 8(%r13)
	.p2align 4,,10
	.p2align 3
.L118:
	movl	$-3, %r8d
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L138:
	cmpl	$31, %ebx
	ja	.L393
.L177:
	testl	%r14d, %r14d
	je	.L461
	movzbl	(%r12), %eax
	movl	%ebx, %ecx
	leal	-1(%r14), %esi
	leaq	1(%r12), %rdx
	salq	%cl, %rax
	leal	8(%rbx), %ecx
	addq	%rax, %r15
	cmpl	$31, %ecx
	ja	.L399
	testl	%esi, %esi
	je	.L434
	movzbl	1(%r12), %eax
	leal	-2(%r14), %esi
	leaq	2(%r12), %rdx
	salq	%cl, %rax
	leal	16(%rbx), %ecx
	addq	%rax, %r15
	cmpl	$31, %ecx
	ja	.L399
	testl	%esi, %esi
	je	.L434
	movzbl	2(%r12), %eax
	addl	$24, %ebx
	leal	-3(%r14), %esi
	leaq	3(%r12), %rdx
	salq	%cl, %rax
	addq	%rax, %r15
	cmpl	$31, %ebx
	ja	.L399
	testl	%esi, %esi
	je	.L767
	movzbl	3(%r12), %eax
	movl	%ebx, %ecx
	subl	$4, %r14d
	leaq	4(%r12), %rdx
	salq	%cl, %rax
	addq	%rax, %r15
.L159:
	movq	%r15, %rax
	movq	%r15, %rcx
	movq	%rdx, %r12
	xorl	%ebx, %ebx
	shrq	$24, %rax
	shrq	$8, %rcx
	andl	$65280, %ecx
	movzbl	%al, %eax
	orq	%rcx, %rax
	movq	%r15, %rcx
	salq	$24, %r15
	salq	$8, %rcx
	movl	%r15d, %r15d
	andl	$16711680, %ecx
	addq	%r15, %rcx
	xorl	%r15d, %r15d
	addq	%rcx, %rax
	movq	%rax, 32(%r13)
	movq	%rax, 96(%r11)
	movl	$16190, 8(%r13)
.L137:
	movl	20(%r13), %edi
	testl	%edi, %edi
	je	.L783
	xorl	%edx, %edx
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	%r11, -112(%rbp)
	call	adler32@PLT
	movq	-112(%rbp), %r11
	movq	%rax, 32(%r13)
	movq	%rax, 96(%r11)
.L761:
	movl	$16191, 8(%r13)
.L136:
	cmpl	$1, -104(%rbp)
	ja	.L135
	.p2align 4,,10
	.p2align 3
.L461:
	movl	%r14d, %r10d
	xorl	%r8d, %r8d
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L775:
	testl	%edi, %edi
	je	.L113
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L463:
	movl	%r14d, %r10d
	movl	%ecx, %ebx
	xorl	%r8d, %r8d
.L119:
	movl	-80(%rbp), %eax
	movl	-88(%rbp), %r14d
	movl	%eax, %esi
	subl	%eax, %r14d
	cmpl	$15, %eax
	jbe	.L304
	movdqa	.LC18(%rip), %xmm0
	movq	-72(%rbp), %rax
	movups	%xmm0, (%rax)
.L353:
	movq	-72(%rbp), %rax
	movl	60(%r13), %esi
	movq	%r12, (%r11)
	movl	%r10d, 8(%r11)
	movq	%rax, 24(%r11)
	movl	-80(%rbp), %eax
	movl	%eax, 32(%r11)
	movq	%r15, 80(%r13)
	movl	%ebx, 88(%r13)
	testl	%esi, %esi
	jne	.L360
	cmpl	%eax, -88(%rbp)
	je	.L361
	movl	8(%r13), %eax
	cmpl	$16208, %eax
	ja	.L361
	cmpl	$4, -84(%rbp)
	jne	.L360
	cmpl	$16205, %eax
	ja	.L361
	.p2align 4,,10
	.p2align 3
.L360:
	movq	56(%r11), %rbx
	movq	72(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L784
.L363:
	movl	60(%rbx), %r12d
	testl	%r12d, %r12d
	jne	.L365
	movl	56(%rbx), %ecx
	movl	$1, %r12d
	movq	$0, 64(%rbx)
	sall	%cl, %r12d
	movl	%r12d, 60(%rbx)
.L365:
	movq	%r11, -104(%rbp)
	movl	%r8d, -80(%rbp)
	cmpl	%r14d, %r12d
	ja	.L366
	movq	-72(%rbp), %rsi
	movl	%r12d, %edx
	subq	%rdx, %rsi
	call	memcpy@PLT
	movl	$0, 68(%rbx)
.L771:
	movl	60(%rbx), %eax
	movq	-104(%rbp), %r11
	movl	-80(%rbp), %r8d
	movl	-88(%rbp), %r14d
	movl	8(%r11), %r10d
	subl	32(%r11), %r14d
	movl	%eax, 64(%rbx)
.L361:
	movl	-92(%rbp), %ebx
	subl	%r10d, %ebx
	movl	%ebx, %eax
	addq	%rax, 16(%r11)
	movl	%r14d, %eax
	addq	%rax, 40(%r11)
	addq	%rax, 40(%r13)
	testb	$4, 16(%r13)
	je	.L369
	testl	%r14d, %r14d
	jne	.L785
.L369:
	movl	12(%r13), %edx
	movl	88(%r13), %eax
	testl	%edx, %edx
	je	.L372
	addl	$64, %eax
.L372:
	movl	8(%r13), %edx
	cmpl	$16191, %edx
	je	.L373
	cmpl	$16199, %edx
	je	.L492
	cmpl	$16194, %edx
	jne	.L374
.L492:
	addl	$256, %eax
.L374:
	orl	%r14d, %ebx
	movl	%eax, 88(%r11)
	je	.L493
	cmpl	$4, -84(%rbp)
	jne	.L111
.L493:
	testl	%r8d, %r8d
	jne	.L111
	movl	$-5, %r8d
	jmp	.L111
.L467:
	movl	%r14d, %r10d
	movl	-88(%rbp), %r14d
	xorl	%esi, %esi
	xorl	%r8d, %r8d
	.p2align 4,,10
	.p2align 3
.L304:
	movabsq	$6148914691236517205, %rcx
	movl	%esi, %eax
	cmpl	$8, %esi
	jnb	.L354
	andl	$4, %esi
	jne	.L786
	testl	%eax, %eax
	je	.L353
	movq	-72(%rbp), %rdx
	movb	$85, (%rdx)
	testb	$2, %al
	je	.L353
	movq	-72(%rbp), %rdx
	movl	$21845, %edi
	movw	%di, -2(%rdx,%rax)
	jmp	.L353
	.p2align 4,,10
	.p2align 3
.L373:
	subl	$-128, %eax
	jmp	.L374
	.p2align 4,,10
	.p2align 3
.L354:
	movq	-72(%rbp), %rax
	leaq	8(%rax), %rdx
	movq	%rcx, (%rax)
	andq	$-8, %rdx
	movq	%rcx, -8(%rax,%rsi)
	subq	%rdx, %rax
	addl	%esi, %eax
	andl	$-8, %eax
	cmpl	$8, %eax
	jb	.L353
	andl	$-8, %eax
	xorl	%esi, %esi
.L358:
	movl	%esi, %edi
	addl	$8, %esi
	movq	%rcx, (%rdx,%rdi)
	cmpl	%eax, %esi
	jb	.L358
	jmp	.L353
	.p2align 4,,10
	.p2align 3
.L785:
	movq	24(%r11), %rsi
	movl	24(%r13), %ecx
	movq	%r11, -80(%rbp)
	movl	%r14d, %edx
	movl	%r8d, -72(%rbp)
	movq	32(%r13), %rdi
	subq	%rax, %rsi
	testl	%ecx, %ecx
	je	.L370
	call	crc32@PLT
	movl	-72(%rbp), %r8d
	movq	-80(%rbp), %r11
.L371:
	movq	%rax, 32(%r13)
	movq	%rax, 96(%r11)
	jmp	.L369
	.p2align 4,,10
	.p2align 3
.L366:
	movl	68(%rbx), %eax
	movq	-72(%rbp), %rsi
	subl	%eax, %r12d
	cmpl	%r14d, %r12d
	cmova	%r14d, %r12d
	addq	%rax, %rdi
	movl	%r14d, %eax
	subq	%rax, %rsi
	movl	%r12d, %edx
	call	memcpy@PLT
	subl	%r12d, %r14d
	movl	-80(%rbp), %r8d
	movq	-104(%rbp), %r11
	jne	.L787
	movl	68(%rbx), %eax
	movl	60(%rbx), %edx
	movl	8(%r11), %r10d
	addl	%r12d, %eax
	cmpl	%edx, %eax
	cmovne	%eax, %r14d
	movl	64(%rbx), %eax
	movl	%r14d, 68(%rbx)
	movl	-88(%rbp), %r14d
	subl	32(%r11), %r14d
	cmpl	%eax, %edx
	jbe	.L361
	addl	%eax, %r12d
	movl	%r12d, 64(%rbx)
	jmp	.L361
	.p2align 4,,10
	.p2align 3
.L784:
	movl	56(%rbx), %ecx
	movl	$1, %esi
	movq	80(%r11), %rdi
	movl	%r8d, -104(%rbp)
	movq	%r11, -80(%rbp)
	movl	$1, %edx
	sall	%cl, %esi
	addl	$16, %esi
	call	*64(%r11)
	movq	-80(%rbp), %r11
	movl	-104(%rbp), %r8d
	testq	%rax, %rax
	movq	%rax, 72(%rbx)
	movq	%rax, %rdi
	jne	.L363
	movl	$16210, 8(%r13)
	movl	$-4, %r8d
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L370:
	call	adler32@PLT
	movq	-80(%rbp), %r11
	movl	-72(%rbp), %r8d
	jmp	.L371
	.p2align 4,,10
	.p2align 3
.L781:
	movl	7144(%r13), %edx
	testl	%edx, %edx
	je	.L306
	leaq	.LC15(%rip), %rax
	movl	%r14d, %r10d
	movq	%rax, 48(%r11)
	movl	$16209, 8(%r13)
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L143:
	movl	24(%r13), %edx
	movl	%edx, %esi
	testb	$4, %dh
	jne	.L788
.L195:
	movq	48(%r13), %rdx
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L201
	movq	$0, 24(%rdx)
.L201:
	movl	$16185, 8(%r13)
.L156:
	testl	%eax, %eax
	je	.L203
	movl	92(%r13), %eax
	cmpl	%eax, %r14d
	movl	%eax, %ecx
	cmovbe	%r14d, %ecx
	testl	%ecx, %ecx
	je	.L204
	movq	48(%r13), %rdi
	movl	%ecx, %r10d
	testq	%rdi, %rdi
	je	.L205
	movq	24(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L205
	movl	32(%rdi), %r9d
	movl	36(%rdi), %edi
	movl	%r9d, %esi
	subl	%eax, %esi
	leal	(%rcx,%rsi), %r8d
	movl	%r8d, -112(%rbp)
	movq	%r10, %r8
	cmpl	%edi, -112(%rbp)
	jbe	.L206
	leal	(%rdi,%rax), %r8d
	subl	%r9d, %r8d
.L206:
	leaq	(%rdx,%rsi), %rdi
	movq	%r12, %rsi
	movq	%r8, %rdx
	movq	%r11, -144(%rbp)
	movq	%r10, -136(%rbp)
	movl	%ecx, -112(%rbp)
	call	memcpy@PLT
	movl	24(%r13), %esi
	movq	-144(%rbp), %r11
	movq	-136(%rbp), %r10
	movl	-112(%rbp), %ecx
.L205:
	andl	$512, %esi
	je	.L207
	testb	$4, 16(%r13)
	jne	.L789
.L207:
	movl	92(%r13), %eax
	subl	%ecx, %r14d
	addq	%r10, %r12
	subl	%ecx, %eax
	movl	%eax, 92(%r13)
.L204:
	testl	%eax, %eax
	jne	.L461
	movl	24(%r13), %esi
.L203:
	movl	$0, 92(%r13)
	movl	$16186, 8(%r13)
.L155:
	testl	$2048, %esi
	jne	.L790
	movq	48(%r13), %rax
	testq	%rax, %rax
	je	.L213
	movq	$0, 40(%rax)
.L213:
	movl	$0, 92(%r13)
	movl	$16187, 8(%r13)
.L154:
	testl	$4096, %esi
	jne	.L791
	movq	48(%r13), %rax
	testq	%rax, %rax
	je	.L219
	movq	$0, 56(%rax)
.L219:
	movl	$16188, 8(%r13)
.L153:
	testl	$512, %esi
	jne	.L792
.L220:
	movq	48(%r13), %rax
	testq	%rax, %rax
	je	.L222
	sarl	$9, %esi
	movl	$1, 72(%rax)
	andl	$1, %esi
	movl	%esi, 68(%rax)
.L222:
	xorl	%edx, %edx
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	%r11, -112(%rbp)
	call	crc32@PLT
	movq	-112(%rbp), %r11
	movq	%rax, 32(%r13)
	movq	%rax, 96(%r11)
	movl	$16191, 8(%r13)
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L142:
	movl	24(%r13), %esi
	movl	%esi, %eax
	andl	$1024, %eax
	jmp	.L156
	.p2align 4,,10
	.p2align 3
.L139:
	movl	24(%r13), %esi
	jmp	.L153
	.p2align 4,,10
	.p2align 3
.L141:
	movl	24(%r13), %esi
	jmp	.L155
	.p2align 4,,10
	.p2align 3
.L140:
	movl	24(%r13), %esi
	jmp	.L154
	.p2align 4,,10
	.p2align 3
.L145:
	cmpl	$31, %ebx
	ja	.L183
.L182:
	testl	%r14d, %r14d
	je	.L461
	movl	%ebx, %ecx
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L185:
	testl	%r14d, %r14d
	je	.L463
.L184:
	movzbl	(%r12), %eax
	addq	$1, %r12
	subl	$1, %r14d
	salq	%cl, %rax
	addl	$8, %ecx
	addq	%rax, %r15
	cmpl	$31, %ecx
	jbe	.L185
.L183:
	movq	48(%r13), %rax
	testq	%rax, %rax
	je	.L186
	movq	%r15, 8(%rax)
.L186:
	testb	$2, 25(%r13)
	je	.L187
	testb	$4, 16(%r13)
	jne	.L793
.L187:
	movl	$16183, 8(%r13)
	xorl	%r15d, %r15d
	xorl	%ebx, %ebx
	jmp	.L188
	.p2align 4,,10
	.p2align 3
.L144:
	cmpl	$15, %ebx
	ja	.L189
.L188:
	testl	%r14d, %r14d
	je	.L461
	movl	%ebx, %ecx
	jmp	.L190
	.p2align 4,,10
	.p2align 3
.L191:
	testl	%r14d, %r14d
	je	.L463
.L190:
	movzbl	(%r12), %eax
	addq	$1, %r12
	subl	$1, %r14d
	salq	%cl, %rax
	addl	$8, %ecx
	addq	%rax, %r15
	cmpl	$15, %ecx
	jbe	.L191
.L189:
	movq	48(%r13), %rax
	testq	%rax, %rax
	je	.L192
	movzbl	%r15b, %edx
	movl	%edx, 16(%rax)
	movq	%r15, %rdx
	shrq	$8, %rdx
	movl	%edx, 20(%rax)
.L192:
	movl	24(%r13), %edx
	movl	%edx, %esi
	testb	$2, %dh
	je	.L193
	testb	$4, 16(%r13)
	jne	.L794
.L193:
	movl	$16184, 8(%r13)
	testb	$4, %dh
	jne	.L413
	xorl	%ebx, %ebx
	xorl	%r15d, %r15d
	jmp	.L195
	.p2align 4,,10
	.p2align 3
.L129:
	movl	140(%r13), %r8d
.L245:
	movl	132(%r13), %eax
	movl	136(%r13), %r9d
	addl	%eax, %r9d
	movl	%eax, -112(%rbp)
	cmpl	%r8d, %r9d
	jbe	.L267
	movl	120(%r13), %ecx
	movl	$-1, %edx
	movq	104(%r13), %rsi
	sall	%cl, %edx
	notl	%edx
.L268:
	movl	%edx, %eax
	andl	%r15d, %eax
	leaq	(%rsi,%rax,4), %rax
	movzbl	1(%rax), %ecx
	movzwl	2(%rax), %edi
	movzbl	%cl, %eax
	cmpl	%ebx, %eax
	jbe	.L269
	testl	%r14d, %r14d
	je	.L461
	movl	%ebx, %ecx
	jmp	.L246
	.p2align 4,,10
	.p2align 3
.L247:
	testl	%r14d, %r14d
	je	.L463
.L246:
	movzbl	(%r12), %eax
	addq	$1, %r12
	subl	$1, %r14d
	salq	%cl, %rax
	addl	$8, %ecx
	addq	%rax, %r15
	movl	%edx, %eax
	andl	%r15d, %eax
	leaq	(%rsi,%rax,4), %rax
	movzbl	1(%rax), %r10d
	movzwl	2(%rax), %edi
	movzbl	%r10b, %eax
	cmpl	%ecx, %eax
	ja	.L247
	movl	%ecx, %ebx
	movl	%r10d, %ecx
.L269:
	cmpw	$15, %di
	ja	.L248
	subl	%eax, %ebx
	leal	1(%r8), %eax
	shrq	%cl, %r15
	movl	%eax, 140(%r13)
	movw	%di, 152(%r13,%r8,2)
	movl	%eax, %r8d
.L249:
	cmpl	%r8d, %r9d
	ja	.L268
.L267:
	cmpl	$16209, 8(%r13)
	je	.L749
	cmpw	$0, 664(%r13)
	jne	.L271
	leaq	.LC10(%rip), %rax
	movl	%r14d, %r10d
	movq	%rax, 48(%r11)
	movl	$16209, 8(%r13)
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L132:
	movl	92(%r13), %eax
.L151:
	testl	%eax, %eax
	je	.L761
	cmpl	%eax, %r14d
	movl	-80(%rbp), %edx
	cmovbe	%r14d, %eax
	cmpl	%edx, %eax
	movl	%eax, %ecx
	cmova	%edx, %ecx
	testl	%ecx, %ecx
	je	.L461
	movq	-72(%rbp), %rdi
	movl	%ecx, %edx
	movq	%r12, %rsi
	movq	%r11, -144(%rbp)
	movl	%ecx, -136(%rbp)
	movq	%rdx, -112(%rbp)
	call	memcpy@PLT
	movl	-136(%rbp), %ecx
	movq	-112(%rbp), %rdx
	subl	%ecx, -80(%rbp)
	movl	8(%r13), %eax
	addq	%rdx, -72(%rbp)
	movq	-144(%rbp), %r11
	subl	%ecx, %r14d
	addq	%rdx, %r12
	subl	%ecx, 92(%r13)
	jmp	.L234
	.p2align 4,,10
	.p2align 3
.L120:
	movl	16(%r13), %esi
	movl	%r14d, %r10d
.L148:
	testl	%esi, %esi
	je	.L350
	movl	24(%r13), %r8d
	testl	%r8d, %r8d
	je	.L350
	cmpl	$31, %ebx
	ja	.L479
	testl	%r10d, %r10d
	je	.L768
	movzbl	(%r12), %edx
	movl	%ebx, %ecx
	leal	-1(%r10), %esi
	leaq	1(%r12), %rax
	salq	%cl, %rdx
	leal	8(%rbx), %ecx
	addq	%rdx, %r15
	cmpl	$31, %ecx
	ja	.L485
	testl	%esi, %esi
	je	.L484
	movzbl	1(%r12), %edx
	leal	-2(%r10), %esi
	leaq	2(%r12), %rax
	salq	%cl, %rdx
	leal	16(%rbx), %ecx
	addq	%rdx, %r15
	cmpl	$31, %ecx
	ja	.L485
	testl	%esi, %esi
	je	.L484
	movzbl	2(%r12), %edx
	leal	-3(%r10), %esi
	leaq	3(%r12), %rax
	salq	%cl, %rdx
	leal	24(%rbx), %ecx
	addq	%rdx, %r15
	cmpl	$31, %ecx
	ja	.L485
	testl	%esi, %esi
	je	.L486
	movzbl	3(%r12), %edx
	subl	$4, %r10d
	leaq	4(%r12), %rax
	addl	$32, %ebx
	salq	%cl, %rdx
	addq	%rdx, %r15
.L351:
	movl	40(%r13), %edx
	cmpq	%r15, %rdx
	je	.L487
	leaq	.LC17(%rip), %rdx
	movq	%rax, %r12
	movq	%rdx, 48(%r11)
	movl	$16209, 8(%r13)
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L130:
	movl	140(%r13), %esi
	movl	128(%r13), %edi
	cmpl	%esi, %edi
	jbe	.L238
.L237:
	movl	%ebx, %ecx
	leaq	order.6397(%rip), %r8
	cmpl	$2, %ecx
	ja	.L442
.L796:
	testl	%r14d, %r14d
	je	.L463
	movzbl	(%r12), %eax
	subl	$1, %r14d
	leaq	1(%r12), %rdx
	salq	%cl, %rax
	addl	$8, %ecx
	addq	%rax, %r15
.L240:
	leal	1(%rsi), %eax
	movl	%r15d, %r9d
	movzwl	(%r8,%rsi,2), %esi
	shrq	$3, %r15
	andl	$7, %r9d
	movl	%eax, 140(%r13)
	subl	$3, %ecx
	movw	%r9w, 152(%r13,%rsi,2)
	cmpl	%edi, %eax
	jnb	.L795
	movq	%rdx, %r12
	movl	%eax, %esi
	cmpl	$2, %ecx
	jbe	.L796
.L442:
	movq	%r12, %rdx
	jmp	.L240
	.p2align 4,,10
	.p2align 3
.L133:
	movl	92(%r13), %eax
.L152:
	movl	$16195, 8(%r13)
	jmp	.L151
	.p2align 4,,10
	.p2align 3
.L388:
	movl	$-4, %r8d
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L387:
	movl	%r14d, %r10d
	movl	$1, %r8d
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L146:
	cmpl	$15, %ebx
	ja	.L401
.L168:
	testl	%r14d, %r14d
	je	.L461
	movzbl	(%r12), %eax
	movl	%ebx, %ecx
	leal	-1(%r14), %edx
	leaq	1(%r12), %r8
	salq	%cl, %rax
	leal	8(%rbx), %ecx
	addq	%rax, %r15
	cmpl	$15, %ecx
	ja	.L403
	testl	%edx, %edx
	je	.L769
	movzbl	1(%r12), %eax
	subl	$2, %r14d
	leaq	2(%r12), %r8
	addl	$16, %ebx
	salq	%cl, %rax
	addq	%rax, %r15
.L161:
	movl	%r15d, 24(%r13)
	movl	%r15d, %eax
	cmpb	$8, %r15b
	je	.L178
.L759:
	leaq	.LC1(%rip), %rax
	movl	%r14d, %r10d
	movq	%r8, %r12
	movq	%rax, 48(%r11)
	movl	$16209, 8(%r13)
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L777:
	cmpl	$2, %ebx
	ja	.L430
	testl	%r14d, %r14d
	je	.L750
	movzbl	(%r12), %eax
	movl	%ebx, %ecx
	subl	$1, %r14d
	leaq	1(%r12), %rdx
	addl	$8, %ebx
	salq	%cl, %rax
	addq	%rax, %r15
.L225:
	movl	%r15d, %eax
	andl	$1, %eax
	movl	%eax, 12(%r13)
	movq	%r15, %rax
	shrq	%rax
	andl	$3, %eax
	cmpl	$2, %eax
	je	.L227
	cmpl	$3, %eax
	je	.L228
	cmpl	$1, %eax
	je	.L229
	movl	$16193, 8(%r13)
	shrq	$3, %r15
	subl	$3, %ebx
	movq	%rdx, %r12
	.p2align 4,,10
	.p2align 3
.L134:
	movl	%ebx, %ecx
	andl	$-8, %ebx
	andl	$7, %ecx
	shrq	%cl, %r15
	cmpl	$31, %ebx
	ja	.L431
	testl	%r14d, %r14d
	je	.L461
	movzbl	(%r12), %eax
	movl	%ebx, %ecx
	leal	-1(%r14), %esi
	leaq	1(%r12), %rdx
	salq	%cl, %rax
	leal	8(%rbx), %ecx
	addq	%rax, %r15
	cmpl	$31, %ecx
	ja	.L437
	testl	%esi, %esi
	je	.L434
	movzbl	1(%r12), %eax
	leal	-2(%r14), %esi
	leaq	2(%r12), %rdx
	salq	%cl, %rax
	leal	16(%rbx), %ecx
	addq	%rax, %r15
	cmpl	$31, %ecx
	ja	.L437
	testl	%esi, %esi
	je	.L434
	movzbl	2(%r12), %eax
	leal	-3(%r14), %esi
	leaq	3(%r12), %rdx
	salq	%cl, %rax
	leal	24(%rbx), %ecx
	addq	%rax, %r15
	cmpl	$31, %ecx
	ja	.L437
	testl	%esi, %esi
	je	.L434
	movzbl	3(%r12), %eax
	subl	$4, %r14d
	leaq	4(%r12), %rdx
	addl	$32, %ebx
	salq	%cl, %rax
	addq	%rax, %r15
.L231:
	movq	%r15, %rax
	movzwl	%r15w, %ecx
	shrq	$16, %rax
	xorq	$65535, %rax
	cmpq	%rax, %rcx
	je	.L232
	leaq	.LC6(%rip), %rax
	movl	%r14d, %r10d
	movq	%rdx, %r12
	movq	%rax, 48(%r11)
	movl	$16209, 8(%r13)
	jmp	.L118
.L227:
	movl	$16196, 8(%r13)
	shrq	$3, %r15
	subl	$3, %ebx
	movq	%rdx, %r12
	.p2align 4,,10
	.p2align 3
.L131:
	cmpl	$13, %ebx
	ja	.L389
	testl	%r14d, %r14d
	je	.L461
	movzbl	(%r12), %eax
	movl	%ebx, %ecx
	leal	-1(%r14), %esi
	leaq	1(%r12), %rdx
	salq	%cl, %rax
	leal	8(%rbx), %ecx
	addq	%rax, %r15
	cmpl	$13, %ecx
	ja	.L391
	testl	%esi, %esi
	je	.L434
	movzbl	1(%r12), %eax
	subl	$2, %r14d
	leaq	2(%r12), %rdx
	addl	$16, %ebx
	salq	%cl, %rax
	addq	%rax, %r15
.L157:
	movq	%r15, %rax
	movq	%r15, %rdi
	movl	%r15d, %ecx
	subl	$14, %ebx
	shrq	$5, %rax
	shrq	$10, %rdi
	andl	$31, %ecx
	andl	$31, %eax
	andl	$15, %edi
	addl	$257, %ecx
	shrq	$14, %r15
	addl	$1, %eax
	addl	$4, %edi
	movl	%ecx, 132(%r13)
	movl	%eax, 136(%r13)
	movl	%edi, 128(%r13)
	cmpl	$286, %ecx
	ja	.L490
	cmpl	$30, %eax
	jbe	.L235
.L490:
	leaq	.LC7(%rip), %rax
	movl	%r14d, %r10d
	movq	%rdx, %r12
	movq	%rax, 48(%r11)
	movl	$16209, 8(%r13)
	jmp	.L118
.L229:
	movdqa	-128(%rbp), %xmm1
	cmpl	$6, -84(%rbp)
	movabsq	$21474836489, %rax
	movl	$16199, 8(%r13)
	movq	%rax, 120(%r13)
	movups	%xmm1, 104(%r13)
	je	.L230
	shrq	$3, %r15
	subl	$3, %ebx
	movq	%rdx, %r12
	.p2align 4,,10
	.p2align 3
.L128:
	movl	$16200, 8(%r13)
	jmp	.L127
.L478:
	movl	-80(%rbp), %eax
	movq	%r14, %r12
	xorl	%ebx, %ebx
	xorl	%r15d, %r15d
	movl	%eax, -88(%rbp)
	.p2align 4,,10
	.p2align 3
.L344:
	movl	$16207, 8(%r13)
	jmp	.L148
	.p2align 4,,10
	.p2align 3
.L274:
	movl	120(%r13), %ecx
	movq	104(%r13), %rdi
	movl	$-1, %r9d
	movl	$0, 7148(%r13)
	movl	%r15d, %r8d
	sall	%cl, %r9d
	notl	%r9d
	movl	%r9d, %eax
	andl	%r15d, %eax
	leaq	(%rdi,%rax,4), %rax
	movzbl	1(%rax), %ecx
	movzbl	(%rax), %edx
	movzwl	2(%rax), %esi
	movzbl	%cl, %eax
	cmpl	%eax, %ebx
	jnb	.L275
	testl	%r14d, %r14d
	je	.L461
	movl	%ebx, %ecx
	jmp	.L276
	.p2align 4,,10
	.p2align 3
.L277:
	testl	%r14d, %r14d
	je	.L463
.L276:
	movzbl	(%r12), %eax
	addq	$1, %r12
	subl	$1, %r14d
	salq	%cl, %rax
	addl	$8, %ecx
	addq	%rax, %r15
	movl	%r9d, %eax
	andl	%r15d, %eax
	movl	%r15d, %r8d
	leaq	(%rdi,%rax,4), %rax
	movzbl	1(%rax), %r10d
	movzbl	(%rax), %edx
	movzwl	2(%rax), %esi
	movzbl	%r10b, %eax
	cmpl	%ecx, %eax
	ja	.L277
	movl	%ecx, %ebx
	movl	%r10d, %ecx
.L275:
	testb	%dl, %dl
	je	.L278
	testb	$-16, %dl
	je	.L797
	movl	%eax, 7148(%r13)
	movl	%eax, %ecx
	subl	%eax, %ebx
	movl	%esi, 92(%r13)
	shrq	%cl, %r15
.L283:
	testb	$32, %dl
	je	.L284
	movl	$-1, 7148(%r13)
	movl	$16191, 8(%r13)
	jmp	.L136
.L739:
	movl	%r14d, %r10d
	jmp	.L121
	.p2align 4,,10
	.p2align 3
.L792:
	cmpl	$15, %ebx
	ja	.L423
	testl	%r14d, %r14d
	je	.L461
	movzbl	(%r12), %eax
	movl	%ebx, %ecx
	leal	-1(%r14), %edi
	leaq	1(%r12), %rdx
	salq	%cl, %rax
	leal	8(%rbx), %ecx
	addq	%rax, %r15
	cmpl	$15, %ecx
	ja	.L425
	testl	%edi, %edi
	je	.L434
	movzbl	1(%r12), %eax
	subl	$2, %r14d
	leaq	2(%r12), %rdx
	addl	$16, %ebx
	salq	%cl, %rax
	addq	%rax, %r15
.L221:
	testb	$4, 16(%r13)
	je	.L428
	movzwl	32(%r13), %eax
	cmpq	%r15, %rax
	je	.L428
	leaq	.LC4(%rip), %rax
	movl	%r14d, %r10d
	movq	%rdx, %r12
	movq	%rax, 48(%r11)
	movl	$16209, 8(%r13)
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L791:
	testl	%r14d, %r14d
	je	.L461
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L216:
	movl	%edx, %eax
	addl	$1, %edx
	movzbl	(%r12,%rax), %ecx
	movq	48(%r13), %rax
	testq	%rax, %rax
	je	.L215
	movq	56(%rax), %rdi
	testq	%rdi, %rdi
	je	.L215
	movl	92(%r13), %esi
	cmpl	64(%rax), %esi
	jnb	.L215
	leal	1(%rsi), %eax
	movl	%eax, 92(%r13)
	movb	%cl, (%rdi,%rsi)
.L215:
	testb	%cl, %cl
	je	.L489
	cmpl	%edx, %r14d
	ja	.L216
.L489:
	testb	$2, 25(%r13)
	je	.L218
	testb	$4, 16(%r13)
	jne	.L798
.L218:
	movl	%edx, %eax
	subl	%edx, %r14d
	addq	%rax, %r12
	testb	%cl, %cl
	jne	.L461
	movl	24(%r13), %esi
	jmp	.L219
	.p2align 4,,10
	.p2align 3
.L790:
	testl	%r14d, %r14d
	je	.L461
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L210:
	movl	%edx, %eax
	addl	$1, %edx
	movzbl	(%r12,%rax), %ecx
	movq	48(%r13), %rax
	testq	%rax, %rax
	je	.L209
	movq	40(%rax), %rdi
	testq	%rdi, %rdi
	je	.L209
	movl	92(%r13), %esi
	cmpl	48(%rax), %esi
	jnb	.L209
	leal	1(%rsi), %eax
	movl	%eax, 92(%r13)
	movb	%cl, (%rdi,%rsi)
.L209:
	testb	%cl, %cl
	je	.L488
	cmpl	%edx, %r14d
	ja	.L210
.L488:
	testb	$2, 25(%r13)
	je	.L212
	testb	$4, 16(%r13)
	jne	.L799
.L212:
	movl	%edx, %eax
	subl	%edx, %r14d
	addq	%rax, %r12
	testb	%cl, %cl
	jne	.L461
	movl	24(%r13), %esi
	jmp	.L213
	.p2align 4,,10
	.p2align 3
.L787:
	movq	-72(%rbp), %rsi
	movl	%r14d, %edx
	movq	72(%rbx), %rdi
	subq	%rdx, %rsi
	call	memcpy@PLT
	movl	%r14d, 68(%rbx)
	jmp	.L771
	.p2align 4,,10
	.p2align 3
.L343:
	movl	$16200, 8(%r13)
	movl	%edi, -80(%rbp)
	jmp	.L127
	.p2align 4,,10
	.p2align 3
.L305:
	movl	92(%r13), %ecx
	movl	%edi, %edx
	cmpl	%ecx, %edi
	cmova	%ecx, %edx
	leal	-1(%rdx), %esi
	cmpl	$47, %edi
	ja	.L317
	testl	%edx, %edx
	je	.L314
	movq	-72(%rbp), %r10
	movl	$16, %ecx
	movq	%rax, %rdi
	subq	%rax, %rcx
	negq	%rdi
	movq	%r10, %r9
	subq	%rax, %r9
	testq	%rcx, %rcx
	leaq	16(%r10), %rcx
	setle	%r8b
	cmpq	%rcx, %r9
	setnb	%cl
	orb	%cl, %r8b
	je	.L318
	cmpl	$14, %esi
	jbe	.L318
	movl	%edx, %r8d
	xorl	%ecx, %ecx
	shrl	$4, %r8d
	salq	$4, %r8
	.p2align 4,,10
	.p2align 3
.L319:
	movdqu	(%r9,%rcx), %xmm6
	movups	%xmm6, (%r10,%rcx)
	addq	$16, %rcx
	cmpq	%r8, %rcx
	jne	.L319
	movl	%edx, %r9d
	movl	%esi, %r8d
	movl	%esi, %r10d
	andl	$-16, %r9d
	movl	%r9d, %ecx
	subl	%r9d, %r8d
	addq	-72(%rbp), %rcx
	cmpl	%r9d, %edx
	je	.L322
	movzbl	(%rcx,%rdi), %r9d
	movb	%r9b, (%rcx)
	testl	%r8d, %r8d
	je	.L322
	movq	%rcx, %r9
	subq	%rax, %r9
	movzbl	1(%r9), %eax
	movb	%al, 1(%rcx)
	cmpl	$1, %r8d
	je	.L322
	movzbl	2(%rcx,%rdi), %eax
	movb	%al, 2(%rcx)
	cmpl	$2, %r8d
	je	.L322
	movzbl	3(%rcx,%rdi), %eax
	movb	%al, 3(%rcx)
	cmpl	$3, %r8d
	je	.L322
	movzbl	4(%rcx,%rdi), %eax
	movb	%al, 4(%rcx)
	cmpl	$4, %r8d
	je	.L322
	movzbl	5(%rcx,%rdi), %eax
	movb	%al, 5(%rcx)
	cmpl	$5, %r8d
	je	.L322
	movzbl	6(%rcx,%rdi), %eax
	movb	%al, 6(%rcx)
	cmpl	$6, %r8d
	je	.L322
	movzbl	7(%rcx,%rdi), %eax
	movb	%al, 7(%rcx)
	cmpl	$7, %r8d
	je	.L322
	movzbl	8(%rcx,%rdi), %eax
	movb	%al, 8(%rcx)
	cmpl	$8, %r8d
	je	.L322
	movzbl	9(%rcx,%rdi), %eax
	movb	%al, 9(%rcx)
	cmpl	$9, %r8d
	je	.L322
	movzbl	10(%rcx,%rdi), %eax
	movb	%al, 10(%rcx)
	cmpl	$10, %r8d
	je	.L322
	movzbl	11(%rcx,%rdi), %eax
	movb	%al, 11(%rcx)
	cmpl	$11, %r8d
	je	.L322
	movzbl	12(%rcx,%rdi), %eax
	movb	%al, 12(%rcx)
	cmpl	$12, %r8d
	je	.L322
	movzbl	13(%rcx,%rdi), %eax
	movb	%al, 13(%rcx)
	cmpl	$13, %r8d
	je	.L322
	movzbl	14(%rcx,%rdi), %eax
	movb	%al, 14(%rcx)
	.p2align 4,,10
	.p2align 3
.L322:
	movq	-72(%rbp), %rax
	movl	92(%r13), %ecx
	movl	-80(%rbp), %edi
	leaq	1(%rax,%r10), %rax
	subl	%edx, %ecx
	movq	%rax, -72(%rbp)
	subl	%edx, %edi
	jmp	.L314
	.p2align 4,,10
	.p2align 3
.L307:
	addl	%ecx, %eax
	addq	%rax, %rdx
	jmp	.L308
	.p2align 4,,10
	.p2align 3
.L286:
	cmpl	%edx, %ebx
	jnb	.L288
	testl	%r14d, %r14d
	je	.L461
	movl	%ebx, %ecx
	jmp	.L289
	.p2align 4,,10
	.p2align 3
.L290:
	testl	%r14d, %r14d
	je	.L463
.L289:
	movzbl	(%r12), %eax
	addq	$1, %r12
	subl	$1, %r14d
	salq	%cl, %rax
	addl	$8, %ecx
	addq	%rax, %r15
	cmpl	%edx, %ecx
	jb	.L290
	movl	%ecx, %ebx
.L288:
	movl	%edx, %ecx
	movl	$-1, %eax
	addl	%edx, 7148(%r13)
	subl	%edx, %ebx
	sall	%cl, %eax
	notl	%eax
	andl	%r15d, %eax
	addl	92(%r13), %eax
	shrq	%cl, %r15
	movl	%eax, 92(%r13)
	jmp	.L287
	.p2align 4,,10
	.p2align 3
.L434:
	movl	%ecx, %ebx
.L767:
	xorl	%r10d, %r10d
	movq	%rdx, %r12
.L768:
	xorl	%r8d, %r8d
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L428:
	movq	%rdx, %r12
	xorl	%ebx, %ebx
	xorl	%r15d, %r15d
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L248:
	cmpw	$16, %di
	je	.L800
	cmpw	$17, %di
	je	.L257
	leal	7(%rax), %r10d
	cmpl	%r10d, %ebx
	jnb	.L258
	testl	%r14d, %r14d
	je	.L461
	movl	%ebx, %ecx
	jmp	.L259
	.p2align 4,,10
	.p2align 3
.L263:
	testl	%r14d, %r14d
	je	.L463
.L259:
	movzbl	(%r12), %edi
	addq	$1, %r12
	subl	$1, %r14d
	salq	%cl, %rdi
	addl	$8, %ecx
	addq	%rdi, %r15
	cmpl	%r10d, %ecx
	jb	.L263
	movl	%ecx, %ebx
.L258:
	movl	%eax, %ecx
	movl	$-7, %edi
	shrq	%cl, %r15
	subl	%eax, %edi
	movl	%r15d, %ecx
	addl	%edi, %ebx
	shrq	$7, %r15
	xorl	%edi, %edi
	andl	$127, %ecx
	addl	$11, %ecx
.L256:
	addl	%ecx, %r8d
	cmpl	%r9d, %r8d
	ja	.L264
	movl	140(%r13), %eax
	leal	(%rcx,%rax), %r8d
	.p2align 4,,10
	.p2align 3
.L265:
	movl	%eax, %ecx
	addl	$1, %eax
	movw	%di, 152(%r13,%rcx,2)
	cmpl	%r8d, %eax
	jne	.L265
	movl	%r8d, 140(%r13)
	jmp	.L249
	.p2align 4,,10
	.p2align 3
.L299:
	andl	$15, %esi
	movl	%r10d, 96(%r13)
	movl	%esi, 100(%r13)
	movl	$16203, 8(%r13)
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L779:
	movl	$-1, 7148(%r13)
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L780:
	cmpl	%esi, %ebx
	jnb	.L301
	testl	%r14d, %r14d
	je	.L461
	movl	%ebx, %ecx
	jmp	.L302
	.p2align 4,,10
	.p2align 3
.L303:
	testl	%r14d, %r14d
	je	.L463
.L302:
	movzbl	(%r12), %eax
	addq	$1, %r12
	subl	$1, %r14d
	salq	%cl, %rax
	addl	$8, %ecx
	addq	%rax, %r15
	cmpl	%esi, %ecx
	jb	.L303
	movl	%ecx, %ebx
.L301:
	movl	%esi, %ecx
	movl	$-1, %eax
	addl	%esi, 7148(%r13)
	subl	%esi, %ebx
	sall	%cl, %eax
	notl	%eax
	andl	%r15d, %eax
	shrq	%cl, %r15
	addl	%eax, 96(%r13)
	jmp	.L300
	.p2align 4,,10
	.p2align 3
.L782:
	testb	$8, %sil
	je	.L310
	movq	-72(%rbp), %rcx
	movq	(%rdx), %rax
	addq	$8, %rdx
	movq	%rax, (%rcx)
	addq	$8, %rcx
	movq	%rcx, -72(%rbp)
.L310:
	testb	$4, %sil
	je	.L311
	movq	-72(%rbp), %rcx
	movl	(%rdx), %eax
	addq	$4, %rdx
	movl	%eax, (%rcx)
	addq	$4, %rcx
	movq	%rcx, -72(%rbp)
.L311:
	testb	$2, %sil
	je	.L312
	movq	-72(%rbp), %rcx
	movzwl	(%rdx), %eax
	addq	$2, %rdx
	movw	%ax, (%rcx)
	addq	$2, %rcx
	movq	%rcx, -72(%rbp)
.L312:
	testb	$1, %sil
	je	.L313
	movzbl	(%rdx), %eax
	movq	-72(%rbp), %rdx
	movb	%al, (%rdx)
	movl	92(%r13), %ecx
	leaq	1(%rdx), %rax
	movq	%rax, -72(%rbp)
	subl	%esi, %ecx
	jmp	.L314
	.p2align 4,,10
	.p2align 3
.L317:
	movl	%esi, %ecx
	subl	%edx, %edi
	andl	$15, %ecx
	addl	$1, %ecx
	cmpl	%edx, %eax
	jnb	.L323
	cmpl	$15, %eax
	jbe	.L801
.L323:
	movq	-72(%rbp), %r9
	movq	%r9, %r8
	subq	%rax, %r8
	movq	%r9, %rax
	movdqu	(%r8), %xmm1
	addq	%rcx, %rax
	addq	%rcx, %r8
	shrl	$4, %esi
	movq	%rax, -72(%rbp)
	leal	-1(%rsi), %eax
	movups	%xmm1, (%r9)
	je	.L763
	movl	%eax, %ecx
	movq	-72(%rbp), %rsi
	xorl	%eax, %eax
	addq	$1, %rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L342:
	movdqu	(%r8,%rax), %xmm5
	movups	%xmm5, (%rsi,%rax)
	addq	$16, %rax
	cmpq	%rax, %rcx
	jne	.L342
	addq	%rcx, -72(%rbp)
.L763:
	movl	92(%r13), %ecx
	subl	%edx, %ecx
	jmp	.L314
.L786:
	movq	-72(%rbp), %rdx
	movl	$1431655765, (%rdx)
	movl	$1431655765, -4(%rdx,%rax)
	jmp	.L353
.L788:
	cmpl	$15, %ebx
	ja	.L196
.L194:
	testl	%r14d, %r14d
	je	.L461
	movl	%ebx, %ecx
	jmp	.L197
	.p2align 4,,10
	.p2align 3
.L198:
	testl	%r14d, %r14d
	je	.L463
.L197:
	movzbl	(%r12), %eax
	addq	$1, %r12
	subl	$1, %r14d
	salq	%cl, %rax
	addl	$8, %ecx
	addq	%rax, %r15
	cmpl	$15, %ecx
	jbe	.L198
.L196:
	movq	48(%r13), %rax
	movl	%r15d, 92(%r13)
	testq	%rax, %rax
	je	.L199
	movl	%r15d, 32(%rax)
.L199:
	testb	$2, %dh
	je	.L760
	testb	$4, 16(%r13)
	jne	.L202
.L760:
	movl	%edx, %eax
	movl	%edx, %esi
	xorl	%ebx, %ebx
	xorl	%r15d, %r15d
	andl	$1024, %eax
	jmp	.L201
.L776:
	cmpl	$15, %ebx
	ja	.L405
	testl	%r14d, %r14d
	je	.L461
	movzbl	(%r12), %eax
	movl	%ebx, %ecx
	leal	-1(%r14), %esi
	leaq	1(%r12), %r8
	salq	%cl, %rax
	leal	8(%rbx), %ecx
	addq	%rax, %r15
	cmpl	$15, %ecx
	ja	.L407
	testl	%esi, %esi
	je	.L769
	movzbl	1(%r12), %eax
	subl	$2, %r14d
	leaq	2(%r12), %r8
	addl	$16, %ebx
	salq	%cl, %rax
	addq	%rax, %r15
.L164:
	testb	$2, %dl
	je	.L166
	cmpq	$35615, %r15
	je	.L802
.L166:
	movq	48(%r13), %rax
	movl	$0, 24(%r13)
	testq	%rax, %rax
	je	.L169
	movl	$-1, 72(%rax)
.L169:
	andl	$1, %edx
	je	.L170
	movabsq	$595056260442243601, %rdx
	movl	%r15d, %ecx
	movq	%r15, %rax
	sall	$8, %ecx
	shrq	$8, %rax
	andl	$65280, %ecx
	addq	%rax, %rcx
	movq	%rcx, %rax
	mulq	%rdx
	movq	%rcx, %rax
	subq	%rdx, %rax
	shrq	%rax
	addq	%rdx, %rax
	shrq	$4, %rax
	movq	%rax, %rdx
	salq	$5, %rdx
	subq	%rax, %rdx
	cmpq	%rdx, %rcx
	jne	.L170
	movl	%r15d, %eax
	andl	$15, %eax
	cmpl	$8, %eax
	jne	.L759
	shrq	$4, %r15
	movl	56(%r13), %eax
	subl	$4, %ebx
	movl	%r15d, %ecx
	andl	$15, %ecx
	addl	$8, %ecx
	testl	%eax, %eax
	jne	.L173
	movl	%ecx, 56(%r13)
	cmpl	$15, %ecx
	jbe	.L174
.L175:
	leaq	.LC2(%rip), %rax
	movl	%r14d, %r10d
	movq	%r8, %r12
	movq	%rax, 48(%r11)
	movl	$16209, 8(%r13)
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L313:
	movl	92(%r13), %ecx
	subl	%esi, %ecx
	jmp	.L314
.L294:
	leal	(%rsi,%rax), %ecx
	movl	$-1, %edi
	movl	-112(%rbp), %esi
	movzwl	%r10w, %r8d
	sall	%cl, %edi
	movl	%edx, %ecx
	notl	%edi
	andl	%edi, %esi
	shrl	%cl, %esi
	movl	%esi, %edx
	addl	%r8d, %edx
	leaq	(%r9,%rdx,4), %rdx
	movzbl	(%rdx), %esi
	movzwl	2(%rdx), %r10d
	movzbl	1(%rdx), %edx
	leal	(%rdx,%rax), %ecx
	cmpl	%ecx, %ebx
	jb	.L298
	jmp	.L296
	.p2align 4,,10
	.p2align 3
.L803:
	movzbl	(%r12), %edx
	movl	%ebx, %ecx
	addl	$8, %ebx
	addq	$1, %r12
	subl	$1, %r14d
	salq	%cl, %rdx
	movl	%eax, %ecx
	addq	%rdx, %r15
	movl	%edi, %edx
	andl	%r15d, %edx
	shrl	%cl, %edx
	addl	%r8d, %edx
	leaq	(%r9,%rdx,4), %rdx
	movzbl	(%rdx), %esi
	movzwl	2(%rdx), %r10d
	movzbl	1(%rdx), %edx
	leal	(%rdx,%rax), %ecx
	cmpl	%ebx, %ecx
	jbe	.L296
.L298:
	testl	%r14d, %r14d
	jne	.L803
	jmp	.L461
.L413:
	xorl	%r15d, %r15d
	xorl	%ebx, %ebx
	jmp	.L194
.L485:
	movl	%ecx, %ebx
	movl	%esi, %r10d
	jmp	.L351
.L798:
	movq	32(%r13), %rdi
	movq	%r12, %rsi
	movq	%r11, -144(%rbp)
	movb	%cl, -136(%rbp)
	movl	%edx, -112(%rbp)
	call	crc32@PLT
	movq	-144(%rbp), %r11
	movzbl	-136(%rbp), %ecx
	movq	%rax, 32(%r13)
	movl	-112(%rbp), %edx
	jmp	.L218
.L178:
	testl	$57344, %r15d
	je	.L179
	leaq	.LC3(%rip), %rax
	movl	%r14d, %r10d
	movq	%r8, %r12
	movq	%rax, 48(%r11)
	movl	$16209, 8(%r13)
	jmp	.L118
.L232:
	movzwl	%r15w, %eax
	cmpl	$6, -84(%rbp)
	movl	$16194, 8(%r13)
	movl	%eax, 92(%r13)
	je	.L439
	movq	%rdx, %r12
	xorl	%ebx, %ebx
	xorl	%r15d, %r15d
	jmp	.L152
.L799:
	movq	32(%r13), %rdi
	movq	%r12, %rsi
	movq	%r11, -144(%rbp)
	movb	%cl, -136(%rbp)
	movl	%edx, -112(%rbp)
	call	crc32@PLT
	movq	-144(%rbp), %r11
	movzbl	-136(%rbp), %ecx
	movq	%rax, 32(%r13)
	movl	-112(%rbp), %edx
	jmp	.L212
.L484:
	movl	%ecx, %ebx
	xorl	%r10d, %r10d
	movq	%rax, %r12
	xorl	%r8d, %r8d
	jmp	.L119
.L437:
	movl	%ecx, %ebx
	movl	%esi, %r14d
	jmp	.L231
.L794:
	movq	32(%r13), %rdi
	leaq	-60(%rbp), %rsi
	movl	$2, %edx
	movq	%r11, -112(%rbp)
	movw	%r15w, -60(%rbp)
	call	crc32@PLT
	movl	24(%r13), %edx
	movq	-112(%rbp), %r11
	movq	%rax, 32(%r13)
	movl	%edx, %esi
	jmp	.L193
.L399:
	movl	%esi, %r14d
	jmp	.L159
.L296:
	movl	%eax, %ecx
	subl	%eax, %ebx
	movl	%edx, %r8d
	addl	7148(%r13), %eax
	shrq	%cl, %r15
	movl	%edx, %ecx
	jmp	.L295
.L257:
	leal	3(%rax), %r10d
	cmpl	%r10d, %ebx
	jnb	.L260
	testl	%r14d, %r14d
	je	.L461
	movl	%ebx, %ecx
	jmp	.L261
	.p2align 4,,10
	.p2align 3
.L262:
	testl	%r14d, %r14d
	je	.L463
.L261:
	movzbl	(%r12), %edi
	addq	$1, %r12
	subl	$1, %r14d
	salq	%cl, %rdi
	addl	$8, %ecx
	addq	%rdi, %r15
	cmpl	%r10d, %ecx
	jb	.L262
	movl	%ecx, %ebx
.L260:
	movl	%eax, %ecx
	movl	$-3, %edi
	shrq	%cl, %r15
	subl	%eax, %edi
	movl	%r15d, %ecx
	addl	%edi, %ebx
	shrq	$3, %r15
	xorl	%edi, %edi
	andl	$7, %ecx
	addl	$3, %ecx
	jmp	.L256
.L228:
	leaq	.LC5(%rip), %rax
	movl	%r14d, %r10d
	subl	$3, %ebx
	movq	%rdx, %r12
	movq	%rax, 48(%r11)
	shrq	$3, %r15
	movl	$16209, 8(%r13)
	jmp	.L118
.L795:
	movl	%ecx, %ebx
	movl	%eax, %esi
	movq	%rdx, %r12
.L238:
	cmpl	$18, %esi
	ja	.L242
	movl	$18, %edx
	movl	%esi, %ecx
	leaq	order.6397(%rip), %rax
	subl	%esi, %edx
	leaq	(%rax,%rcx,2), %rax
	addq	%rcx, %rdx
	leaq	2+order.6397(%rip), %rcx
	leaq	(%rcx,%rdx,2), %rcx
	.p2align 4,,10
	.p2align 3
.L243:
	movzwl	(%rax), %edx
	xorl	%esi, %esi
	addq	$2, %rax
	movw	%si, 152(%r13,%rdx,2)
	cmpq	%rcx, %rax
	jne	.L243
	movl	$19, 140(%r13)
.L242:
	leaq	1368(%r13), %rax
	xorl	%edi, %edi
	movl	$7, 120(%r13)
	leaq	144(%r13), %rcx
	movq	%rax, 144(%r13)
	leaq	120(%r13), %r8
	leaq	152(%r13), %rsi
	movl	$19, %edx
	movq	%rax, 104(%r13)
	leaq	792(%r13), %r9
	movq	%r11, -112(%rbp)
	call	inflate_table
	movq	-112(%rbp), %r11
	testl	%eax, %eax
	je	.L244
	leaq	.LC8(%rip), %rax
	movl	%r14d, %r10d
	movq	%rax, 48(%r11)
	movl	$16209, 8(%r13)
	jmp	.L118
.L789:
	movq	32(%r13), %rdi
	movl	%ecx, %edx
	movq	%r12, %rsi
	movq	%r11, -144(%rbp)
	movq	%r10, -136(%rbp)
	movl	%ecx, -112(%rbp)
	call	crc32@PLT
	movq	-144(%rbp), %r11
	movq	-136(%rbp), %r10
	movq	%rax, 32(%r13)
	movl	-112(%rbp), %ecx
	jmp	.L207
.L284:
	testb	$64, %dl
	je	.L285
	leaq	.LC13(%rip), %rax
	movl	%r14d, %r10d
	movq	%rax, 48(%r11)
	movl	$16209, 8(%r13)
	jmp	.L118
.L793:
	movq	32(%r13), %rdi
	leaq	-60(%rbp), %rsi
	movl	$4, %edx
	movq	%r11, -112(%rbp)
	movl	%r15d, -60(%rbp)
	call	crc32@PLT
	movq	-112(%rbp), %r11
	movq	%rax, 32(%r13)
	jmp	.L187
.L170:
	leaq	.LC0(%rip), %rax
	movl	%r14d, %r10d
	movq	%r8, %r12
	movq	%rax, 48(%r11)
	movl	$16209, 8(%r13)
	jmp	.L118
.L479:
	movq	%r12, %rax
	jmp	.L351
.L487:
	movq	%rax, %r12
	xorl	%ebx, %ebx
	xorl	%r15d, %r15d
.L350:
	movl	$16208, 8(%r13)
	movl	$1, %r8d
	jmp	.L119
.L423:
	movq	%r12, %rdx
	jmp	.L221
.L425:
	movl	%ecx, %ebx
	movl	%edi, %r14d
	jmp	.L221
.L801:
	cmpl	$4, %eax
	je	.L324
	ja	.L325
	cmpl	$1, %eax
	je	.L326
	cmpl	$2, %eax
	jne	.L328
	movq	-72(%rbp), %rsi
	movzwl	-2(%rsi), %eax
	movd	%eax, %xmm0
	movl	%ecx, %eax
	punpcklwd	%xmm0, %xmm0
	addq	%rsi, %rax
	pshufd	$0, %xmm0, %xmm0
	movups	%xmm0, (%rsi)
	movl	%edx, %esi
	subl	%ecx, %esi
	movl	%esi, %ecx
	je	.L765
	movzwl	-2(%rax), %esi
	movd	%esi, %xmm0
	movq	%rax, %rsi
	leal	(%rcx,%rax), %eax
	punpcklwd	%xmm0, %xmm0
	movq	%rsi, %rcx
	pshufd	$0, %xmm0, %xmm0
.L332:
	movups	%xmm0, (%rcx)
	addq	$16, %rcx
	cmpl	%ecx, %eax
	jne	.L332
.L764:
	movq	%rcx, -72(%rbp)
	movl	92(%r13), %ecx
	subl	%edx, %ecx
	jmp	.L314
.L325:
	cmpl	$8, %eax
	jne	.L328
	movq	-72(%rbp), %rsi
	movl	%ecx, %eax
	movq	-8(%rsi), %xmm0
	addq	%rsi, %rax
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, (%rsi)
	movl	%edx, %esi
	subl	%ecx, %esi
	movl	%esi, %ecx
	je	.L765
	movq	-8(%rax), %xmm0
	movq	%rax, %rsi
	leal	(%rcx,%rax), %eax
	movq	%rsi, %rcx
	punpcklqdq	%xmm0, %xmm0
.L336:
	movups	%xmm0, (%rcx)
	addq	$16, %rcx
	cmpl	%ecx, %eax
	jne	.L336
	jmp	.L764
.L235:
	movl	$0, 140(%r13)
	movq	%rdx, %r12
	xorl	%esi, %esi
	movl	$16197, 8(%r13)
	jmp	.L237
.L800:
	leal	2(%rax), %r10d
	cmpl	%r10d, %ebx
	jnb	.L251
	testl	%r14d, %r14d
	je	.L461
	movl	%ebx, %ecx
	jmp	.L252
	.p2align 4,,10
	.p2align 3
.L253:
	testl	%r14d, %r14d
	je	.L463
.L252:
	movzbl	(%r12), %edi
	addq	$1, %r12
	subl	$1, %r14d
	salq	%cl, %rdi
	addl	$8, %ecx
	addq	%rdi, %r15
	cmpl	%r10d, %ecx
	jb	.L253
	movl	%ecx, %ebx
.L251:
	movl	%eax, %ecx
	subl	%eax, %ebx
	shrq	%cl, %r15
	testl	%r8d, %r8d
	je	.L264
	movl	%r15d, %ecx
	leal	-1(%r8), %eax
	shrq	$2, %r15
	subl	$2, %ebx
	movzwl	152(%r13,%rax,2), %edi
	andl	$3, %ecx
	addl	$3, %ecx
	jmp	.L256
.L474:
	movl	%ecx, %ebx
	movl	%edx, %r10d
	jmp	.L345
.L778:
	movq	-72(%rbp), %rsi
	movl	24(%r13), %r12d
	movq	%r11, -104(%rbp)
	movl	%r10d, -88(%rbp)
	movq	32(%r13), %rdi
	subq	%rax, %rsi
	testl	%r12d, %r12d
	je	.L347
	call	crc32@PLT
	movl	-88(%rbp), %r10d
	movq	-104(%rbp), %r11
.L348:
	movl	16(%r13), %esi
	movq	%rax, 32(%r13)
	movq	%rax, 96(%r11)
	movl	%esi, %ecx
	andl	$4, %ecx
	jmp	.L346
.L318:
	movq	-72(%rbp), %rax
	movl	%esi, %r10d
	leaq	1(%rax,%r10), %rsi
	.p2align 4,,10
	.p2align 3
.L321:
	movzbl	(%rax,%rdi), %ecx
	addq	$1, %rax
	movb	%cl, -1(%rax)
	cmpq	%rax, %rsi
	jne	.L321
	jmp	.L322
.L475:
	movl	%ecx, %ebx
	xorl	%r10d, %r10d
	movq	%r14, %r12
	xorl	%r8d, %r8d
	jmp	.L119
.L797:
	addl	%eax, %edx
	movl	$-1, %r9d
	movl	%edx, %ecx
	sall	%cl, %r9d
	movl	%eax, %ecx
	notl	%r9d
	andl	%r9d, %r8d
	movl	%r9d, -112(%rbp)
	shrl	%cl, %r8d
	leal	(%r8,%rsi), %edx
	leaq	(%rdi,%rdx,4), %rcx
	movzbl	1(%rcx), %r8d
	movzbl	(%rcx), %edx
	movzwl	2(%rcx), %r10d
	leal	(%r8,%rax), %r9d
	cmpl	%r9d, %ebx
	jnb	.L280
	testl	%r14d, %r14d
	je	.L461
	movl	%esi, %r8d
	movl	-112(%rbp), %esi
	jmp	.L281
	.p2align 4,,10
	.p2align 3
.L282:
	testl	%r14d, %r14d
	je	.L461
.L281:
	movzbl	(%r12), %edx
	movl	%ebx, %ecx
	addl	$8, %ebx
	addq	$1, %r12
	subl	$1, %r14d
	salq	%cl, %rdx
	movl	%eax, %ecx
	addq	%rdx, %r15
	movl	%esi, %edx
	andl	%r15d, %edx
	shrl	%cl, %edx
	addl	%r8d, %edx
	leaq	(%rdi,%rdx,4), %rcx
	movzbl	(%rcx), %edx
	movzwl	2(%rcx), %r10d
	movzbl	1(%rcx), %ecx
	leal	(%rcx,%rax), %r9d
	cmpl	%ebx, %r9d
	ja	.L282
	movl	%ecx, %r8d
.L280:
	movl	%eax, %ecx
	subl	%eax, %ebx
	movl	%r9d, 7148(%r13)
	shrq	%cl, %r15
	movl	%r8d, %ecx
	movl	%r10d, 92(%r13)
	subl	%r8d, %ebx
	shrq	%cl, %r15
	testb	%dl, %dl
	jne	.L283
	jmp	.L377
	.p2align 4,,10
	.p2align 3
.L486:
	movl	%ecx, %ebx
	xorl	%r10d, %r10d
	movq	%rax, %r12
	jmp	.L768
.L769:
	movq	%r8, %r12
	movl	%ecx, %ebx
	xorl	%r10d, %r10d
	xorl	%r8d, %r8d
	jmp	.L119
.L431:
	movq	%r12, %rdx
	jmp	.L231
.L401:
	movq	%r12, %r8
	jmp	.L161
.L389:
	movq	%r12, %rdx
	jmp	.L157
.L393:
	movq	%r12, %rdx
	jmp	.L159
.L285:
	andl	$15, %edx
	movl	$16201, 8(%r13)
	movl	%edx, 100(%r13)
	jmp	.L150
.L430:
	movq	%r12, %rdx
	jmp	.L225
.L273:
	cmpl	$6, -84(%rbp)
	movl	$16199, 8(%r13)
	jne	.L128
.L750:
	movl	%r14d, %r10d
	jmp	.L119
.L179:
	movq	48(%r13), %rcx
	testq	%rcx, %rcx
	je	.L180
	movq	%r15, %rdx
	shrq	$8, %rdx
	andl	$1, %edx
	movl	%edx, (%rcx)
.L180:
	testb	$2, %ah
	je	.L181
	testb	$4, 16(%r13)
	jne	.L804
.L181:
	movl	$16182, 8(%r13)
	movq	%r8, %r12
	xorl	%r15d, %r15d
	xorl	%ebx, %ebx
	jmp	.L182
.L403:
	movl	%ecx, %ebx
	movl	%edx, %r14d
	jmp	.L161
.L391:
	movl	%ecx, %ebx
	movl	%esi, %r14d
	jmp	.L157
.L202:
	movq	32(%r13), %rdi
	leaq	-60(%rbp), %rsi
	movl	$2, %edx
	xorl	%ebx, %ebx
	movq	%r11, -112(%rbp)
	movw	%r15w, -60(%rbp)
	xorl	%r15d, %r15d
	call	crc32@PLT
	movl	24(%r13), %esi
	movq	-112(%rbp), %r11
	movq	%rax, 32(%r13)
	movl	%esi, %eax
	andl	$1024, %eax
	jmp	.L201
.L244:
	movl	$0, 140(%r13)
	xorl	%r8d, %r8d
	movl	$16198, 8(%r13)
	jmp	.L245
.L347:
	call	adler32@PLT
	movq	-104(%rbp), %r11
	movl	-88(%rbp), %r10d
	jmp	.L348
.L783:
	movq	-72(%rbp), %rax
	movq	%r12, (%r11)
	movl	$2, %r8d
	movl	%r14d, 8(%r11)
	movq	%rax, 24(%r11)
	movl	-80(%rbp), %eax
	movl	%eax, 32(%r11)
	movq	%r15, 80(%r13)
	movl	%ebx, 88(%r13)
	jmp	.L111
.L468:
	movq	%r12, %r14
	jmp	.L345
.L765:
	movl	92(%r13), %ecx
	movq	%rax, -72(%rbp)
	subl	%edx, %ecx
	jmp	.L314
.L802:
	cmpl	$0, 56(%r13)
	jne	.L167
	movl	$15, 56(%r13)
.L167:
	xorl	%edx, %edx
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	%r8, -136(%rbp)
	movq	%r11, -112(%rbp)
	xorl	%r15d, %r15d
	xorl	%ebx, %ebx
	call	crc32@PLT
	leaq	-60(%rbp), %rsi
	movl	$2, %edx
	movw	$-29921, -60(%rbp)
	movq	%rax, 32(%r13)
	movq	%rax, %rdi
	call	crc32@PLT
	movq	-136(%rbp), %r8
	movq	-112(%rbp), %r11
	movl	$16181, 8(%r13)
	movq	%rax, 32(%r13)
	movq	%r8, %r12
	jmp	.L168
.L405:
	movq	%r12, %r8
	jmp	.L164
.L326:
	movq	-72(%rbp), %rsi
	movl	%edx, %r9d
	subl	%ecx, %r9d
	movzbl	-1(%rsi), %eax
	movd	%eax, %xmm0
	movl	%ecx, %eax
	punpcklbw	%xmm0, %xmm0
	leaq	(%rsi,%rax), %rax
	punpcklwd	%xmm0, %xmm0
	movq	%rax, -72(%rbp)
	leal	(%r9,%rax), %eax
	movq	-72(%rbp), %rcx
	pshufd	$0, %xmm0, %xmm0
	movdqa	%xmm0, %xmm1
	movups	%xmm0, (%rsi)
	je	.L763
.L330:
	movups	%xmm1, (%rcx)
	addq	$16, %rcx
	cmpl	%ecx, %eax
	jne	.L330
	jmp	.L764
.L407:
	movl	%ecx, %ebx
	movl	%esi, %r14d
	jmp	.L164
.L324:
	movq	-72(%rbp), %rsi
	movl	%ecx, %eax
	movd	-4(%rsi), %xmm1
	addq	%rsi, %rax
	pshufd	$0, %xmm1, %xmm0
	movups	%xmm0, (%rsi)
	movl	%edx, %esi
	subl	%ecx, %esi
	movl	%esi, %ecx
	je	.L765
	movd	-4(%rax), %xmm1
	movq	%rax, %rsi
	leal	(%rcx,%rax), %eax
	movq	%rsi, %rcx
	pshufd	$0, %xmm1, %xmm0
.L334:
	movups	%xmm0, (%rcx)
	addq	$16, %rcx
	cmpl	%ecx, %eax
	jne	.L334
	jmp	.L764
.L328:
	movq	-72(%rbp), %r9
	movl	%eax, %esi
	movl	%edx, %ecx
	movq	%r9, %r8
	subq	%rsi, %r8
	jmp	.L337
.L805:
	cmpl	$15, %eax
	ja	.L491
	movl	%eax, %esi
.L337:
	movdqu	(%r8), %xmm1
	subl	%eax, %ecx
	addl	%eax, %eax
	movups	%xmm1, (%r9)
	addq	%rsi, %r9
	cmpl	%eax, %ecx
	ja	.L805
.L491:
	movq	%r9, %rsi
	subq	%rax, %rsi
	leal	-1(%rcx), %eax
	movl	%eax, %ecx
	movdqu	(%rsi), %xmm1
	andl	$15, %ecx
	addq	$1, %rcx
	movups	%xmm1, (%r9)
	addq	%rcx, %rsi
	addq	%rcx, %r9
	shrl	$4, %eax
	movq	%r9, -72(%rbp)
	leal	-1(%rax), %ecx
	je	.L763
	addq	$1, %rcx
	movq	-72(%rbp), %r8
	xorl	%eax, %eax
	salq	$4, %rcx
.L340:
	movdqu	(%rsi,%rax), %xmm1
	movups	%xmm1, (%r8,%rax)
	addq	$16, %rax
	cmpq	%rcx, %rax
	jne	.L340
	addq	%rax, -72(%rbp)
	jmp	.L763
.L271:
	leaq	1368(%r13), %rax
	movl	-112(%rbp), %edx
	movl	$9, 120(%r13)
	leaq	792(%r13), %r9
	movq	%rax, 144(%r13)
	leaq	120(%r13), %r8
	leaq	144(%r13), %rcx
	movl	$1, %edi
	movq	%rax, 104(%r13)
	leaq	152(%r13), %rsi
	movq	%r11, -152(%rbp)
	movq	%r9, -144(%rbp)
	movq	%rcx, -136(%rbp)
	movq	%rsi, -112(%rbp)
	call	inflate_table
	movq	-112(%rbp), %rsi
	movq	-136(%rbp), %rcx
	testl	%eax, %eax
	movq	-144(%rbp), %r9
	movq	-152(%rbp), %r11
	je	.L272
	leaq	.LC11(%rip), %rax
	movl	%r14d, %r10d
	movq	%rax, 48(%r11)
	movl	$16209, 8(%r13)
	jmp	.L118
.L749:
	movl	%r14d, %r10d
	jmp	.L118
.L774:
	call	__stack_chk_fail@PLT
.L264:
	leaq	.LC9(%rip), %rax
	movl	%r14d, %r10d
	movq	%rax, 48(%r11)
	movl	$16209, 8(%r13)
	jmp	.L118
.L804:
	movq	32(%r13), %rdi
	leaq	-60(%rbp), %rsi
	movl	$2, %edx
	movq	%r8, -136(%rbp)
	movq	%r11, -112(%rbp)
	movw	%r15w, -60(%rbp)
	call	crc32@PLT
	movq	-136(%rbp), %r8
	movq	-112(%rbp), %r11
	movq	%rax, 32(%r13)
	jmp	.L181
.L173:
	cmpl	$15, %ecx
	ja	.L175
	cmpl	%ecx, %eax
	jb	.L175
.L174:
	movl	$1, %eax
	xorl	%edx, %edx
	xorl	%esi, %esi
	xorl	%edi, %edi
	sall	%cl, %eax
	movq	%r8, -136(%rbp)
	movl	%eax, 28(%r13)
	movq	%r11, -112(%rbp)
	call	adler32@PLT
	movq	-112(%rbp), %r11
	andl	$512, %r15d
	movq	-136(%rbp), %r8
	movq	%rax, 32(%r13)
	movq	%rax, 96(%r11)
	jne	.L176
	movl	$16191, 8(%r13)
	movq	%r8, %r12
	xorl	%ebx, %ebx
	jmp	.L136
.L230:
	movl	%r14d, %r10d
	shrq	$3, %r15
	subl	$3, %ebx
	movq	%rdx, %r12
	jmp	.L119
.L272:
	movq	144(%r13), %rax
	movl	$6, 124(%r13)
	leaq	124(%r13), %r8
	movl	$2, %edi
	movl	136(%r13), %edx
	movq	%r11, -112(%rbp)
	movq	%rax, 112(%r13)
	movl	132(%r13), %eax
	addq	%rax, %rax
	addq	%rax, %rsi
	call	inflate_table
	movq	-112(%rbp), %r11
	testl	%eax, %eax
	movl	%eax, %r8d
	je	.L273
	leaq	.LC12(%rip), %rax
	movl	%r14d, %r10d
	movq	%rax, 48(%r11)
	movl	$16209, 8(%r13)
	jmp	.L118
.L439:
	movl	%r14d, %r10d
	movq	%rdx, %r12
	xorl	%r8d, %r8d
	xorl	%ebx, %ebx
	xorl	%r15d, %r15d
	jmp	.L119
.L176:
	movl	$16189, 8(%r13)
	movq	%r8, %r12
	xorl	%r15d, %r15d
	xorl	%ebx, %ebx
	jmp	.L177
	.cfi_endproc
.LFE580:
	.size	inflate, .-inflate
	.p2align 4
	.globl	inflateEnd
	.type	inflateEnd, @function
inflateEnd:
.LFB581:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L809
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpq	$0, 64(%rdi)
	je	.L811
	movq	72(%rdi), %rax
	testq	%rax, %rax
	je	.L811
	movq	56(%rdi), %rsi
	movl	$-2, %r8d
	testq	%rsi, %rsi
	je	.L806
	cmpq	(%rsi), %rdi
	jne	.L806
	movl	8(%rsi), %ecx
	leal	-16180(%rcx), %edx
	cmpl	$31, %edx
	jbe	.L822
.L806:
	addq	$8, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L822:
	.cfi_restore_state
	movq	72(%rsi), %r8
	movq	80(%rdi), %rdi
	testq	%r8, %r8
	je	.L808
	movq	%r8, %rsi
	call	*%rax
	movq	72(%rbx), %rax
	movq	56(%rbx), %rsi
	movq	80(%rbx), %rdi
.L808:
	call	*%rax
	movq	$0, 56(%rbx)
	xorl	%r8d, %r8d
	addq	$8, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L811:
	.cfi_restore_state
	addq	$8, %rsp
	movl	$-2, %r8d
	movl	%r8d, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L809:
	.cfi_restore 3
	.cfi_restore 6
	movl	$-2, %r8d
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE581:
	.size	inflateEnd, .-inflateEnd
	.p2align 4
	.globl	inflateGetDictionary
	.type	inflateGetDictionary, @function
inflateGetDictionary:
.LFB582:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L828
	cmpq	$0, 64(%rdi)
	je	.L828
	cmpq	$0, 72(%rdi)
	je	.L828
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$-2, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	56(%rdi), %r12
	testq	%r12, %r12
	je	.L823
	cmpq	(%r12), %rdi
	jne	.L823
	movq	%rsi, %rcx
	movl	8(%r12), %esi
	movq	%rdx, %rbx
	leal	-16180(%rsi), %edx
	cmpl	$31, %edx
	jbe	.L844
.L823:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L844:
	.cfi_restore_state
	movl	64(%r12), %edx
	testq	%rcx, %rcx
	je	.L825
	testl	%edx, %edx
	jne	.L845
.L825:
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.L823
	movl	64(%r12), %edx
	movl	%edx, (%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L828:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	movl	$-2, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L845:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	movl	68(%r12), %esi
	movq	%rcx, %rdi
	subl	%esi, %edx
	addq	72(%r12), %rsi
	call	memcpy@PLT
	movl	68(%r12), %edx
	movl	64(%r12), %edi
	movq	72(%r12), %rsi
	subq	%rdx, %rdi
	addq	%rax, %rdi
	call	memcpy@PLT
	jmp	.L825
	.cfi_endproc
.LFE582:
	.size	inflateGetDictionary, .-inflateGetDictionary
	.p2align 4
	.globl	inflateSetDictionary
	.type	inflateSetDictionary, @function
inflateSetDictionary:
.LFB583:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L858
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpq	$0, 64(%rdi)
	je	.L860
	cmpq	$0, 72(%rdi)
	je	.L860
	movq	56(%rdi), %r15
	movl	$-2, %eax
	testq	%r15, %r15
	je	.L846
	cmpq	(%r15), %rdi
	jne	.L846
	movl	%edx, %r13d
	movl	8(%r15), %edx
	leal	-16180(%rdx), %ecx
	cmpl	$31, %ecx
	jbe	.L871
.L846:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L871:
	.cfi_restore_state
	movl	16(%r15), %ecx
	movq	%rsi, %r8
	testl	%ecx, %ecx
	je	.L848
	cmpl	$16190, %edx
	jne	.L846
.L849:
	xorl	%edx, %edx
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	%r8, -56(%rbp)
	call	adler32@PLT
	movq	-56(%rbp), %r8
	movl	%r13d, %edx
	movq	%rax, %rdi
	movq	%r8, %rsi
	call	adler32@PLT
	cmpq	%rax, 32(%r15)
	jne	.L866
	movq	56(%rbx), %r14
	movq	-56(%rbp), %r8
	movl	%r13d, %r12d
	movq	72(%r14), %rdi
	addq	%r8, %r12
	testq	%rdi, %rdi
	jne	.L851
.L872:
	movl	56(%r14), %ecx
	movl	$1, %esi
	movq	%r8, -56(%rbp)
	movl	$1, %edx
	movq	80(%rbx), %rdi
	sall	%cl, %esi
	addl	$16, %esi
	call	*64(%rbx)
	movq	-56(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, 72(%r14)
	movq	%rax, %rdi
	jne	.L851
	movl	$16210, 8(%r15)
	movl	$-4, %eax
	jmp	.L846
	.p2align 4,,10
	.p2align 3
.L848:
	cmpl	$16190, %edx
	je	.L849
	movq	%r15, %r14
	movl	%r13d, %r12d
	movq	72(%r14), %rdi
	addq	%r8, %r12
	testq	%rdi, %rdi
	je	.L872
.L851:
	movl	60(%r14), %edx
	testl	%edx, %edx
	jne	.L853
	movl	56(%r14), %ecx
	movl	$1, %edx
	movq	$0, 64(%r14)
	sall	%cl, %edx
	movl	%edx, 60(%r14)
.L853:
	cmpl	%edx, %r13d
	jb	.L854
	movq	%r12, %rsi
	subq	%rdx, %rsi
	call	memcpy@PLT
	movl	60(%r14), %eax
	movl	$0, 68(%r14)
	movl	%eax, 64(%r14)
.L855:
	movl	$1, 20(%r15)
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L860:
	.cfi_restore_state
	addq	$24, %rsp
	movl	$-2, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L854:
	.cfi_restore_state
	movl	68(%r14), %eax
	movq	%r8, %rsi
	subl	%eax, %edx
	cmpl	%edx, %r13d
	movl	%edx, %ebx
	cmovbe	%r13d, %ebx
	addq	%rax, %rdi
	movl	%ebx, %edx
	call	memcpy@PLT
	subl	%ebx, %r13d
	jne	.L873
	movl	68(%r14), %eax
	movl	60(%r14), %edx
	addl	%ebx, %eax
	cmpl	%edx, %eax
	cmovne	%eax, %r13d
	movl	64(%r14), %eax
	movl	%r13d, 68(%r14)
	cmpl	%eax, %edx
	jbe	.L855
	addl	%eax, %ebx
	movl	%ebx, 64(%r14)
	jmp	.L855
	.p2align 4,,10
	.p2align 3
.L873:
	movl	%r13d, %edx
	movq	72(%r14), %rdi
	movq	%r12, %rsi
	subq	%rdx, %rsi
	call	memcpy@PLT
	movl	60(%r14), %eax
	movl	%r13d, 68(%r14)
	movl	%eax, 64(%r14)
	jmp	.L855
	.p2align 4,,10
	.p2align 3
.L858:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	movl	$-2, %eax
	ret
.L866:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movl	$-3, %eax
	jmp	.L846
	.cfi_endproc
.LFE583:
	.size	inflateSetDictionary, .-inflateSetDictionary
	.p2align 4
	.globl	inflateGetHeader
	.type	inflateGetHeader, @function
inflateGetHeader:
.LFB584:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L878
	cmpq	$0, 64(%rdi)
	je	.L878
	cmpq	$0, 72(%rdi)
	je	.L878
	movq	56(%rdi), %rax
	movl	$-2, %r8d
	testq	%rax, %rax
	je	.L874
	cmpq	(%rax), %rdi
	jne	.L874
	movl	8(%rax), %ecx
	leal	-16180(%rcx), %edx
	cmpl	$31, %edx
	jbe	.L883
.L874:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L883:
	testb	$2, 16(%rax)
	je	.L874
	xorl	%r8d, %r8d
	movq	%rsi, 48(%rax)
	movl	$0, 72(%rsi)
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L878:
	movl	$-2, %r8d
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE584:
	.size	inflateGetHeader, .-inflateGetHeader
	.p2align 4
	.globl	inflateSync
	.type	inflateSync, @function
inflateSync:
.LFB586:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L907
	cmpq	$0, 64(%rdi)
	je	.L907
	cmpq	$0, 72(%rdi)
	je	.L907
	movq	56(%rdi), %r9
	movl	$-2, %eax
	testq	%r9, %r9
	je	.L884
	cmpq	(%r9), %rdi
	jne	.L884
	movl	8(%r9), %edx
	leal	-16180(%rdx), %ecx
	cmpl	$31, %ecx
	jbe	.L927
	.p2align 4,,10
	.p2align 3
.L884:
	movq	-24(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L928
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L927:
	.cfi_restore_state
	movl	8(%rdi), %r10d
	testl	%r10d, %r10d
	jne	.L886
	movl	88(%r9), %eax
	cmpl	$7, %eax
	jbe	.L929
	movq	(%rdi), %r8
	cmpl	$16211, %edx
	je	.L902
	movq	80(%r9), %rdx
	movl	%eax, %ecx
	andl	$-8, %eax
	movl	$16211, 8(%r9)
	andl	$7, %ecx
	movl	%eax, 88(%r9)
	salq	%cl, %rdx
	movq	%rdx, 80(%r9)
.L903:
	movq	%rdx, %rcx
	leal	-8(%rax), %r10d
	movb	%dl, -28(%rbp)
	shrq	$8, %rcx
	cmpl	$7, %r10d
	jbe	.L891
	movb	%cl, -27(%rbp)
	leal	-16(%rax), %esi
	movq	%rdx, %rcx
	shrq	$16, %rcx
	cmpl	$7, %esi
	jbe	.L891
	movb	%cl, -26(%rbp)
	leal	-24(%rax), %esi
	movq	%rdx, %rcx
	shrq	$24, %rcx
	cmpl	$7, %esi
	jbe	.L891
	shrq	$32, %rdx
	movb	%cl, -25(%rbp)
	movq	%rdx, %rcx
.L891:
	andl	$7, %eax
	shrl	$3, %r10d
	leaq	-28(%rbp), %rdx
	movq	%rcx, 80(%r9)
	movl	$1, %r11d
	movl	%eax, 88(%r9)
	addl	$1, %r10d
	xorl	%eax, %eax
	movl	$4, %ebx
	subl	%edx, %r11d
	jmp	.L895
	.p2align 4,,10
	.p2align 3
.L893:
	testb	%sil, %sil
	jne	.L914
	movl	%ebx, %esi
	subl	%eax, %esi
	cmpl	$3, %esi
	movl	%esi, %eax
	setbe	%cl
.L894:
	leal	(%r11,%rdx), %esi
	cmpl	%r10d, %esi
	setb	%sil
	addq	$1, %rdx
	testb	%cl, %sil
	je	.L890
.L895:
	cmpl	$2, %eax
	movzbl	(%rdx), %esi
	sbbl	%ecx, %ecx
	notl	%ecx
	cmpb	%cl, %sil
	jne	.L893
	addl	$1, %eax
	cmpl	$3, %eax
	setbe	%cl
	jmp	.L894
	.p2align 4,,10
	.p2align 3
.L914:
	movl	$1, %ecx
	xorl	%eax, %eax
	jmp	.L894
	.p2align 4,,10
	.p2align 3
.L907:
	movl	$-2, %eax
	jmp	.L884
	.p2align 4,,10
	.p2align 3
.L886:
	movq	(%rdi), %r8
	cmpl	$16211, %edx
	je	.L930
	movl	88(%r9), %eax
	movq	80(%r9), %rdx
	movl	$16211, 8(%r9)
	movl	%eax, %ecx
	andl	$-8, %eax
	andl	$7, %ecx
	movl	%eax, 88(%r9)
	salq	%cl, %rdx
	movq	%rdx, 80(%r9)
	cmpl	$7, %eax
	ja	.L903
	movl	$1, %ecx
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L890:
	movl	%eax, 140(%r9)
	movl	8(%rdi), %r10d
.L889:
	testl	%r10d, %r10d
	setne	%r11b
	andb	%cl, %r11b
	je	.L915
	xorl	%edx, %edx
	movl	$4, %ebx
	jmp	.L900
	.p2align 4,,10
	.p2align 3
.L898:
	testb	%sil, %sil
	jne	.L917
	movl	%ebx, %esi
	subl	%eax, %esi
	cmpl	$3, %esi
	movl	%esi, %eax
	setbe	%cl
.L899:
	addl	$1, %edx
	cmpl	%edx, %r10d
	jbe	.L919
.L931:
	testb	%cl, %cl
	je	.L919
.L900:
	movl	%edx, %ecx
	cmpl	$2, %eax
	movzbl	(%r8,%rcx), %esi
	sbbl	%ecx, %ecx
	notl	%ecx
	cmpb	%cl, %sil
	jne	.L898
	addl	$1, %eax
	cmpl	$3, %eax
	setbe	%cl
	addl	$1, %edx
	cmpl	%edx, %r10d
	ja	.L931
	.p2align 4,,10
	.p2align 3
.L919:
	movl	%edx, %r10d
	addq	%r10, %r8
.L896:
	addq	16(%rdi), %r10
	movl	%eax, 140(%r9)
	movq	%r8, (%rdi)
	subl	%edx, 8(%rdi)
	movq	%r10, 16(%rdi)
	cmpl	$4, %eax
	jne	.L918
	movq	40(%rdi), %r11
	call	inflateReset
	xorl	%eax, %eax
	movq	%r10, 16(%rdi)
	movq	%r11, 40(%rdi)
	movl	$16191, 8(%r9)
	jmp	.L884
	.p2align 4,,10
	.p2align 3
.L917:
	movl	%r11d, %ecx
	xorl	%eax, %eax
	jmp	.L899
	.p2align 4,,10
	.p2align 3
.L930:
	movl	140(%r9), %eax
	cmpl	$3, %eax
	setbe	%cl
	jmp	.L889
	.p2align 4,,10
	.p2align 3
.L902:
	movl	140(%r9), %eax
	xorl	%edx, %edx
	xorl	%r10d, %r10d
	jmp	.L896
.L918:
	movl	$-3, %eax
	jmp	.L884
.L929:
	movl	$-5, %eax
	jmp	.L884
.L928:
	call	__stack_chk_fail@PLT
.L915:
	xorl	%r10d, %r10d
	xorl	%edx, %edx
	jmp	.L896
	.cfi_endproc
.LFE586:
	.size	inflateSync, .-inflateSync
	.p2align 4
	.globl	inflateSyncPoint
	.type	inflateSyncPoint, @function
inflateSyncPoint:
.LFB587:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L936
	cmpq	$0, 64(%rdi)
	je	.L936
	cmpq	$0, 72(%rdi)
	je	.L936
	movq	56(%rdi), %rdx
	movl	$-2, %eax
	testq	%rdx, %rdx
	je	.L932
	cmpq	(%rdx), %rdi
	jne	.L932
	movl	8(%rdx), %ecx
	leal	-16180(%rcx), %esi
	cmpl	$31, %esi
	jbe	.L941
.L932:
	ret
	.p2align 4,,10
	.p2align 3
.L941:
	xorl	%eax, %eax
	cmpl	$16193, %ecx
	jne	.L932
	movl	88(%rdx), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L936:
	movl	$-2, %eax
	ret
	.cfi_endproc
.LFE587:
	.size	inflateSyncPoint, .-inflateSyncPoint
	.p2align 4
	.globl	inflateCopy
	.type	inflateCopy, @function
inflateCopy:
.LFB588:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L952
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	64(%rsi), %rcx
	movq	%rsi, %rbx
	testq	%rcx, %rcx
	je	.L957
	cmpq	$0, 72(%rsi)
	je	.L957
	movq	56(%rsi), %r14
	movl	$-2, %eax
	testq	%r14, %r14
	je	.L942
	cmpq	(%r14), %rsi
	jne	.L942
	movl	8(%r14), %eax
	subl	$16180, %eax
	cmpl	$31, %eax
	ja	.L957
	movq	%rdi, %r12
	testq	%rdi, %rdi
	je	.L957
	movq	80(%rsi), %rdi
	movl	$7160, %edx
	movl	$1, %esi
	call	*%rcx
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L958
	movq	72(%r14), %r8
	testq	%r8, %r8
	je	.L944
	movl	56(%r14), %ecx
	movl	$1, %esi
	movq	80(%rbx), %rdi
	movl	$1, %edx
	sall	%cl, %esi
	call	*64(%rbx)
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L968
	movdqu	(%rbx), %xmm0
	leaq	8(%r13), %rdi
	movq	%r13, %rcx
	movq	%r14, %rsi
	andq	$-8, %rdi
	movups	%xmm0, (%r12)
	movdqu	16(%rbx), %xmm1
	subq	%rdi, %rcx
	subq	%rcx, %rsi
	addl	$7160, %ecx
	movups	%xmm1, 16(%r12)
	movdqu	32(%rbx), %xmm2
	shrl	$3, %ecx
	movups	%xmm2, 32(%r12)
	movdqu	48(%rbx), %xmm3
	movups	%xmm3, 48(%r12)
	movdqu	64(%rbx), %xmm4
	movups	%xmm4, 64(%r12)
	movdqu	80(%rbx), %xmm5
	movups	%xmm5, 80(%r12)
	movdqu	96(%rbx), %xmm6
	movups	%xmm6, 96(%r12)
	movq	(%r14), %rax
	movq	%rax, 0(%r13)
	movq	7152(%r14), %rax
	movq	%rax, 7152(%r13)
	leaq	1368(%r13), %rax
	rep movsq
	leaq	1368(%r14), %rcx
	movq	%r12, 0(%r13)
	movq	104(%r14), %rdx
	cmpq	%rcx, %rdx
	jnb	.L949
	addq	144(%r14), %rax
	subq	%rcx, %rax
	movq	%rax, 144(%r13)
.L948:
	movl	56(%r14), %ecx
	movq	72(%r14), %rsi
	movl	$1, %edx
	movq	%r8, %rdi
	sall	%cl, %edx
	call	memcpy@PLT
	movq	%rax, %r8
.L947:
	movq	%r8, 72(%r13)
	xorl	%eax, %eax
	movq	%r13, 56(%r12)
.L942:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L957:
	.cfi_restore_state
	popq	%rbx
	movl	$-2, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L944:
	.cfi_restore_state
	movdqu	(%rbx), %xmm7
	leaq	8(%r13), %rdi
	movq	%r13, %rcx
	movq	%r14, %rsi
	andq	$-8, %rdi
	movups	%xmm7, (%r12)
	movdqu	16(%rbx), %xmm7
	subq	%rdi, %rcx
	subq	%rcx, %rsi
	addl	$7160, %ecx
	movups	%xmm7, 16(%r12)
	movdqu	32(%rbx), %xmm7
	shrl	$3, %ecx
	movups	%xmm7, 32(%r12)
	movdqu	48(%rbx), %xmm7
	movups	%xmm7, 48(%r12)
	movdqu	64(%rbx), %xmm7
	movups	%xmm7, 64(%r12)
	movdqu	80(%rbx), %xmm0
	movups	%xmm0, 80(%r12)
	movdqu	96(%rbx), %xmm1
	movups	%xmm1, 96(%r12)
	movq	(%r14), %rax
	movq	%rax, 0(%r13)
	movq	7152(%r14), %rax
	movq	%rax, 7152(%r13)
	leaq	1368(%r13), %rax
	rep movsq
	leaq	1368(%r14), %rcx
	movq	%r12, 0(%r13)
	movq	104(%r14), %rdx
	cmpq	%rcx, %rdx
	jb	.L963
	.p2align 4,,10
	.p2align 3
.L949:
	leaq	7140(%r14), %rsi
	cmpq	%rsi, %rdx
	ja	.L946
	subq	%rcx, %rdx
	addq	%rax, %rdx
	movq	%rdx, 104(%r13)
	movq	112(%r14), %rdx
	addq	%rax, %rdx
	subq	%rcx, %rdx
	movq	%rdx, 112(%r13)
.L946:
	addq	144(%r14), %rax
	subq	%rcx, %rax
	movq	%rax, 144(%r13)
	testq	%r8, %r8
	je	.L947
	jmp	.L948
	.p2align 4,,10
	.p2align 3
.L952:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	movl	$-2, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L963:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	addq	144(%r14), %rax
	subq	%rcx, %rax
	movq	%rax, 144(%r13)
	jmp	.L947
.L958:
	movl	$-4, %eax
	jmp	.L942
.L968:
	movq	80(%rbx), %rdi
	movq	%r13, %rsi
	call	*72(%rbx)
	movl	$-4, %eax
	jmp	.L942
	.cfi_endproc
.LFE588:
	.size	inflateCopy, .-inflateCopy
	.p2align 4
	.globl	inflateUndermine
	.type	inflateUndermine, @function
inflateUndermine:
.LFB589:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L973
	cmpq	$0, 64(%rdi)
	je	.L973
	cmpq	$0, 72(%rdi)
	je	.L973
	movq	56(%rdi), %rdx
	movl	$-2, %eax
	testq	%rdx, %rdx
	je	.L969
	cmpq	(%rdx), %rdi
	jne	.L969
	movl	8(%rdx), %esi
	leal	-16180(%rsi), %ecx
	cmpl	$31, %ecx
	jbe	.L977
.L969:
	ret
	.p2align 4,,10
	.p2align 3
.L977:
	movl	$1, 7144(%rdx)
	movl	$-3, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L973:
	movl	$-2, %eax
	ret
	.cfi_endproc
.LFE589:
	.size	inflateUndermine, .-inflateUndermine
	.p2align 4
	.globl	inflateValidate
	.type	inflateValidate, @function
inflateValidate:
.LFB590:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L983
	cmpq	$0, 64(%rdi)
	je	.L983
	cmpq	$0, 72(%rdi)
	je	.L983
	movq	56(%rdi), %rax
	movl	$-2, %r8d
	testq	%rax, %rax
	je	.L978
	cmpq	(%rax), %rdi
	jne	.L978
	movl	8(%rax), %ecx
	leal	-16180(%rcx), %edx
	cmpl	$31, %edx
	jbe	.L987
.L978:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L987:
	movl	16(%rax), %edx
	testl	%esi, %esi
	je	.L980
	orl	$4, %edx
	xorl	%r8d, %r8d
	movl	%edx, 16(%rax)
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L983:
	movl	$-2, %r8d
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L980:
	andl	$-5, %edx
	xorl	%r8d, %r8d
	movl	%edx, 16(%rax)
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE590:
	.size	inflateValidate, .-inflateValidate
	.p2align 4
	.globl	inflateMark
	.type	inflateMark, @function
inflateMark:
.LFB591:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L994
	cmpq	$0, 64(%rdi)
	je	.L994
	cmpq	$0, 72(%rdi)
	je	.L994
	movq	56(%rdi), %rdx
	movq	$-65536, %rax
	testq	%rdx, %rdx
	je	.L988
	cmpq	(%rdx), %rdi
	jne	.L988
	movl	8(%rdx), %ecx
	leal	-16180(%rcx), %esi
	cmpl	$31, %esi
	jbe	.L998
.L988:
	ret
	.p2align 4,,10
	.p2align 3
.L998:
	movslq	7148(%rdx), %rax
	salq	$16, %rax
	cmpl	$16195, %ecx
	je	.L999
	cmpl	$16204, %ecx
	jne	.L988
	movl	7152(%rdx), %ecx
	movl	%ecx, %esi
	subl	92(%rdx), %esi
	addq	%rsi, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L994:
	movq	$-65536, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L999:
	movl	92(%rdx), %edx
	addq	%rdx, %rax
	ret
	.cfi_endproc
.LFE591:
	.size	inflateMark, .-inflateMark
	.p2align 4
	.globl	inflateCodesUsed
	.type	inflateCodesUsed, @function
inflateCodesUsed:
.LFB592:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L1004
	cmpq	$0, 64(%rdi)
	je	.L1004
	cmpq	$0, 72(%rdi)
	je	.L1004
	movq	56(%rdi), %rdx
	movq	$-1, %rax
	testq	%rdx, %rdx
	je	.L1000
	cmpq	(%rdx), %rdi
	jne	.L1000
	movl	8(%rdx), %esi
	leal	-16180(%rsi), %ecx
	cmpl	$31, %ecx
	jbe	.L1008
.L1000:
	ret
	.p2align 4,,10
	.p2align 3
.L1008:
	movq	144(%rdx), %rax
	leaq	1368(%rdx), %rcx
	subq	%rcx, %rax
	sarq	$2, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1004:
	movq	$-1, %rax
	ret
	.cfi_endproc
.LFE592:
	.size	inflateCodesUsed, .-inflateCodesUsed
	.section	.rodata
	.align 32
	.type	distfix.6368, @object
	.size	distfix.6368, 128
distfix.6368:
	.byte	16
	.byte	5
	.value	1
	.byte	23
	.byte	5
	.value	257
	.byte	19
	.byte	5
	.value	17
	.byte	27
	.byte	5
	.value	4097
	.byte	17
	.byte	5
	.value	5
	.byte	25
	.byte	5
	.value	1025
	.byte	21
	.byte	5
	.value	65
	.byte	29
	.byte	5
	.value	16385
	.byte	16
	.byte	5
	.value	3
	.byte	24
	.byte	5
	.value	513
	.byte	20
	.byte	5
	.value	33
	.byte	28
	.byte	5
	.value	8193
	.byte	18
	.byte	5
	.value	9
	.byte	26
	.byte	5
	.value	2049
	.byte	22
	.byte	5
	.value	129
	.byte	64
	.byte	5
	.value	0
	.byte	16
	.byte	5
	.value	2
	.byte	23
	.byte	5
	.value	385
	.byte	19
	.byte	5
	.value	25
	.byte	27
	.byte	5
	.value	6145
	.byte	17
	.byte	5
	.value	7
	.byte	25
	.byte	5
	.value	1537
	.byte	21
	.byte	5
	.value	97
	.byte	29
	.byte	5
	.value	24577
	.byte	16
	.byte	5
	.value	4
	.byte	24
	.byte	5
	.value	769
	.byte	20
	.byte	5
	.value	49
	.byte	28
	.byte	5
	.value	12289
	.byte	18
	.byte	5
	.value	13
	.byte	26
	.byte	5
	.value	3073
	.byte	22
	.byte	5
	.value	193
	.byte	64
	.byte	5
	.value	0
	.align 32
	.type	lenfix.6367, @object
	.size	lenfix.6367, 2048
lenfix.6367:
	.byte	96
	.byte	7
	.value	0
	.byte	0
	.byte	8
	.value	80
	.byte	0
	.byte	8
	.value	16
	.byte	20
	.byte	8
	.value	115
	.byte	18
	.byte	7
	.value	31
	.byte	0
	.byte	8
	.value	112
	.byte	0
	.byte	8
	.value	48
	.byte	0
	.byte	9
	.value	192
	.byte	16
	.byte	7
	.value	10
	.byte	0
	.byte	8
	.value	96
	.byte	0
	.byte	8
	.value	32
	.byte	0
	.byte	9
	.value	160
	.byte	0
	.byte	8
	.value	0
	.byte	0
	.byte	8
	.value	128
	.byte	0
	.byte	8
	.value	64
	.byte	0
	.byte	9
	.value	224
	.byte	16
	.byte	7
	.value	6
	.byte	0
	.byte	8
	.value	88
	.byte	0
	.byte	8
	.value	24
	.byte	0
	.byte	9
	.value	144
	.byte	19
	.byte	7
	.value	59
	.byte	0
	.byte	8
	.value	120
	.byte	0
	.byte	8
	.value	56
	.byte	0
	.byte	9
	.value	208
	.byte	17
	.byte	7
	.value	17
	.byte	0
	.byte	8
	.value	104
	.byte	0
	.byte	8
	.value	40
	.byte	0
	.byte	9
	.value	176
	.byte	0
	.byte	8
	.value	8
	.byte	0
	.byte	8
	.value	136
	.byte	0
	.byte	8
	.value	72
	.byte	0
	.byte	9
	.value	240
	.byte	16
	.byte	7
	.value	4
	.byte	0
	.byte	8
	.value	84
	.byte	0
	.byte	8
	.value	20
	.byte	21
	.byte	8
	.value	227
	.byte	19
	.byte	7
	.value	43
	.byte	0
	.byte	8
	.value	116
	.byte	0
	.byte	8
	.value	52
	.byte	0
	.byte	9
	.value	200
	.byte	17
	.byte	7
	.value	13
	.byte	0
	.byte	8
	.value	100
	.byte	0
	.byte	8
	.value	36
	.byte	0
	.byte	9
	.value	168
	.byte	0
	.byte	8
	.value	4
	.byte	0
	.byte	8
	.value	132
	.byte	0
	.byte	8
	.value	68
	.byte	0
	.byte	9
	.value	232
	.byte	16
	.byte	7
	.value	8
	.byte	0
	.byte	8
	.value	92
	.byte	0
	.byte	8
	.value	28
	.byte	0
	.byte	9
	.value	152
	.byte	20
	.byte	7
	.value	83
	.byte	0
	.byte	8
	.value	124
	.byte	0
	.byte	8
	.value	60
	.byte	0
	.byte	9
	.value	216
	.byte	18
	.byte	7
	.value	23
	.byte	0
	.byte	8
	.value	108
	.byte	0
	.byte	8
	.value	44
	.byte	0
	.byte	9
	.value	184
	.byte	0
	.byte	8
	.value	12
	.byte	0
	.byte	8
	.value	140
	.byte	0
	.byte	8
	.value	76
	.byte	0
	.byte	9
	.value	248
	.byte	16
	.byte	7
	.value	3
	.byte	0
	.byte	8
	.value	82
	.byte	0
	.byte	8
	.value	18
	.byte	21
	.byte	8
	.value	163
	.byte	19
	.byte	7
	.value	35
	.byte	0
	.byte	8
	.value	114
	.byte	0
	.byte	8
	.value	50
	.byte	0
	.byte	9
	.value	196
	.byte	17
	.byte	7
	.value	11
	.byte	0
	.byte	8
	.value	98
	.byte	0
	.byte	8
	.value	34
	.byte	0
	.byte	9
	.value	164
	.byte	0
	.byte	8
	.value	2
	.byte	0
	.byte	8
	.value	130
	.byte	0
	.byte	8
	.value	66
	.byte	0
	.byte	9
	.value	228
	.byte	16
	.byte	7
	.value	7
	.byte	0
	.byte	8
	.value	90
	.byte	0
	.byte	8
	.value	26
	.byte	0
	.byte	9
	.value	148
	.byte	20
	.byte	7
	.value	67
	.byte	0
	.byte	8
	.value	122
	.byte	0
	.byte	8
	.value	58
	.byte	0
	.byte	9
	.value	212
	.byte	18
	.byte	7
	.value	19
	.byte	0
	.byte	8
	.value	106
	.byte	0
	.byte	8
	.value	42
	.byte	0
	.byte	9
	.value	180
	.byte	0
	.byte	8
	.value	10
	.byte	0
	.byte	8
	.value	138
	.byte	0
	.byte	8
	.value	74
	.byte	0
	.byte	9
	.value	244
	.byte	16
	.byte	7
	.value	5
	.byte	0
	.byte	8
	.value	86
	.byte	0
	.byte	8
	.value	22
	.byte	64
	.byte	8
	.value	0
	.byte	19
	.byte	7
	.value	51
	.byte	0
	.byte	8
	.value	118
	.byte	0
	.byte	8
	.value	54
	.byte	0
	.byte	9
	.value	204
	.byte	17
	.byte	7
	.value	15
	.byte	0
	.byte	8
	.value	102
	.byte	0
	.byte	8
	.value	38
	.byte	0
	.byte	9
	.value	172
	.byte	0
	.byte	8
	.value	6
	.byte	0
	.byte	8
	.value	134
	.byte	0
	.byte	8
	.value	70
	.byte	0
	.byte	9
	.value	236
	.byte	16
	.byte	7
	.value	9
	.byte	0
	.byte	8
	.value	94
	.byte	0
	.byte	8
	.value	30
	.byte	0
	.byte	9
	.value	156
	.byte	20
	.byte	7
	.value	99
	.byte	0
	.byte	8
	.value	126
	.byte	0
	.byte	8
	.value	62
	.byte	0
	.byte	9
	.value	220
	.byte	18
	.byte	7
	.value	27
	.byte	0
	.byte	8
	.value	110
	.byte	0
	.byte	8
	.value	46
	.byte	0
	.byte	9
	.value	188
	.byte	0
	.byte	8
	.value	14
	.byte	0
	.byte	8
	.value	142
	.byte	0
	.byte	8
	.value	78
	.byte	0
	.byte	9
	.value	252
	.byte	96
	.byte	7
	.value	0
	.byte	0
	.byte	8
	.value	81
	.byte	0
	.byte	8
	.value	17
	.byte	21
	.byte	8
	.value	131
	.byte	18
	.byte	7
	.value	31
	.byte	0
	.byte	8
	.value	113
	.byte	0
	.byte	8
	.value	49
	.byte	0
	.byte	9
	.value	194
	.byte	16
	.byte	7
	.value	10
	.byte	0
	.byte	8
	.value	97
	.byte	0
	.byte	8
	.value	33
	.byte	0
	.byte	9
	.value	162
	.byte	0
	.byte	8
	.value	1
	.byte	0
	.byte	8
	.value	129
	.byte	0
	.byte	8
	.value	65
	.byte	0
	.byte	9
	.value	226
	.byte	16
	.byte	7
	.value	6
	.byte	0
	.byte	8
	.value	89
	.byte	0
	.byte	8
	.value	25
	.byte	0
	.byte	9
	.value	146
	.byte	19
	.byte	7
	.value	59
	.byte	0
	.byte	8
	.value	121
	.byte	0
	.byte	8
	.value	57
	.byte	0
	.byte	9
	.value	210
	.byte	17
	.byte	7
	.value	17
	.byte	0
	.byte	8
	.value	105
	.byte	0
	.byte	8
	.value	41
	.byte	0
	.byte	9
	.value	178
	.byte	0
	.byte	8
	.value	9
	.byte	0
	.byte	8
	.value	137
	.byte	0
	.byte	8
	.value	73
	.byte	0
	.byte	9
	.value	242
	.byte	16
	.byte	7
	.value	4
	.byte	0
	.byte	8
	.value	85
	.byte	0
	.byte	8
	.value	21
	.byte	16
	.byte	8
	.value	258
	.byte	19
	.byte	7
	.value	43
	.byte	0
	.byte	8
	.value	117
	.byte	0
	.byte	8
	.value	53
	.byte	0
	.byte	9
	.value	202
	.byte	17
	.byte	7
	.value	13
	.byte	0
	.byte	8
	.value	101
	.byte	0
	.byte	8
	.value	37
	.byte	0
	.byte	9
	.value	170
	.byte	0
	.byte	8
	.value	5
	.byte	0
	.byte	8
	.value	133
	.byte	0
	.byte	8
	.value	69
	.byte	0
	.byte	9
	.value	234
	.byte	16
	.byte	7
	.value	8
	.byte	0
	.byte	8
	.value	93
	.byte	0
	.byte	8
	.value	29
	.byte	0
	.byte	9
	.value	154
	.byte	20
	.byte	7
	.value	83
	.byte	0
	.byte	8
	.value	125
	.byte	0
	.byte	8
	.value	61
	.byte	0
	.byte	9
	.value	218
	.byte	18
	.byte	7
	.value	23
	.byte	0
	.byte	8
	.value	109
	.byte	0
	.byte	8
	.value	45
	.byte	0
	.byte	9
	.value	186
	.byte	0
	.byte	8
	.value	13
	.byte	0
	.byte	8
	.value	141
	.byte	0
	.byte	8
	.value	77
	.byte	0
	.byte	9
	.value	250
	.byte	16
	.byte	7
	.value	3
	.byte	0
	.byte	8
	.value	83
	.byte	0
	.byte	8
	.value	19
	.byte	21
	.byte	8
	.value	195
	.byte	19
	.byte	7
	.value	35
	.byte	0
	.byte	8
	.value	115
	.byte	0
	.byte	8
	.value	51
	.byte	0
	.byte	9
	.value	198
	.byte	17
	.byte	7
	.value	11
	.byte	0
	.byte	8
	.value	99
	.byte	0
	.byte	8
	.value	35
	.byte	0
	.byte	9
	.value	166
	.byte	0
	.byte	8
	.value	3
	.byte	0
	.byte	8
	.value	131
	.byte	0
	.byte	8
	.value	67
	.byte	0
	.byte	9
	.value	230
	.byte	16
	.byte	7
	.value	7
	.byte	0
	.byte	8
	.value	91
	.byte	0
	.byte	8
	.value	27
	.byte	0
	.byte	9
	.value	150
	.byte	20
	.byte	7
	.value	67
	.byte	0
	.byte	8
	.value	123
	.byte	0
	.byte	8
	.value	59
	.byte	0
	.byte	9
	.value	214
	.byte	18
	.byte	7
	.value	19
	.byte	0
	.byte	8
	.value	107
	.byte	0
	.byte	8
	.value	43
	.byte	0
	.byte	9
	.value	182
	.byte	0
	.byte	8
	.value	11
	.byte	0
	.byte	8
	.value	139
	.byte	0
	.byte	8
	.value	75
	.byte	0
	.byte	9
	.value	246
	.byte	16
	.byte	7
	.value	5
	.byte	0
	.byte	8
	.value	87
	.byte	0
	.byte	8
	.value	23
	.byte	64
	.byte	8
	.value	0
	.byte	19
	.byte	7
	.value	51
	.byte	0
	.byte	8
	.value	119
	.byte	0
	.byte	8
	.value	55
	.byte	0
	.byte	9
	.value	206
	.byte	17
	.byte	7
	.value	15
	.byte	0
	.byte	8
	.value	103
	.byte	0
	.byte	8
	.value	39
	.byte	0
	.byte	9
	.value	174
	.byte	0
	.byte	8
	.value	7
	.byte	0
	.byte	8
	.value	135
	.byte	0
	.byte	8
	.value	71
	.byte	0
	.byte	9
	.value	238
	.byte	16
	.byte	7
	.value	9
	.byte	0
	.byte	8
	.value	95
	.byte	0
	.byte	8
	.value	31
	.byte	0
	.byte	9
	.value	158
	.byte	20
	.byte	7
	.value	99
	.byte	0
	.byte	8
	.value	127
	.byte	0
	.byte	8
	.value	63
	.byte	0
	.byte	9
	.value	222
	.byte	18
	.byte	7
	.value	27
	.byte	0
	.byte	8
	.value	111
	.byte	0
	.byte	8
	.value	47
	.byte	0
	.byte	9
	.value	190
	.byte	0
	.byte	8
	.value	15
	.byte	0
	.byte	8
	.value	143
	.byte	0
	.byte	8
	.value	79
	.byte	0
	.byte	9
	.value	254
	.byte	96
	.byte	7
	.value	0
	.byte	0
	.byte	8
	.value	80
	.byte	0
	.byte	8
	.value	16
	.byte	20
	.byte	8
	.value	115
	.byte	18
	.byte	7
	.value	31
	.byte	0
	.byte	8
	.value	112
	.byte	0
	.byte	8
	.value	48
	.byte	0
	.byte	9
	.value	193
	.byte	16
	.byte	7
	.value	10
	.byte	0
	.byte	8
	.value	96
	.byte	0
	.byte	8
	.value	32
	.byte	0
	.byte	9
	.value	161
	.byte	0
	.byte	8
	.value	0
	.byte	0
	.byte	8
	.value	128
	.byte	0
	.byte	8
	.value	64
	.byte	0
	.byte	9
	.value	225
	.byte	16
	.byte	7
	.value	6
	.byte	0
	.byte	8
	.value	88
	.byte	0
	.byte	8
	.value	24
	.byte	0
	.byte	9
	.value	145
	.byte	19
	.byte	7
	.value	59
	.byte	0
	.byte	8
	.value	120
	.byte	0
	.byte	8
	.value	56
	.byte	0
	.byte	9
	.value	209
	.byte	17
	.byte	7
	.value	17
	.byte	0
	.byte	8
	.value	104
	.byte	0
	.byte	8
	.value	40
	.byte	0
	.byte	9
	.value	177
	.byte	0
	.byte	8
	.value	8
	.byte	0
	.byte	8
	.value	136
	.byte	0
	.byte	8
	.value	72
	.byte	0
	.byte	9
	.value	241
	.byte	16
	.byte	7
	.value	4
	.byte	0
	.byte	8
	.value	84
	.byte	0
	.byte	8
	.value	20
	.byte	21
	.byte	8
	.value	227
	.byte	19
	.byte	7
	.value	43
	.byte	0
	.byte	8
	.value	116
	.byte	0
	.byte	8
	.value	52
	.byte	0
	.byte	9
	.value	201
	.byte	17
	.byte	7
	.value	13
	.byte	0
	.byte	8
	.value	100
	.byte	0
	.byte	8
	.value	36
	.byte	0
	.byte	9
	.value	169
	.byte	0
	.byte	8
	.value	4
	.byte	0
	.byte	8
	.value	132
	.byte	0
	.byte	8
	.value	68
	.byte	0
	.byte	9
	.value	233
	.byte	16
	.byte	7
	.value	8
	.byte	0
	.byte	8
	.value	92
	.byte	0
	.byte	8
	.value	28
	.byte	0
	.byte	9
	.value	153
	.byte	20
	.byte	7
	.value	83
	.byte	0
	.byte	8
	.value	124
	.byte	0
	.byte	8
	.value	60
	.byte	0
	.byte	9
	.value	217
	.byte	18
	.byte	7
	.value	23
	.byte	0
	.byte	8
	.value	108
	.byte	0
	.byte	8
	.value	44
	.byte	0
	.byte	9
	.value	185
	.byte	0
	.byte	8
	.value	12
	.byte	0
	.byte	8
	.value	140
	.byte	0
	.byte	8
	.value	76
	.byte	0
	.byte	9
	.value	249
	.byte	16
	.byte	7
	.value	3
	.byte	0
	.byte	8
	.value	82
	.byte	0
	.byte	8
	.value	18
	.byte	21
	.byte	8
	.value	163
	.byte	19
	.byte	7
	.value	35
	.byte	0
	.byte	8
	.value	114
	.byte	0
	.byte	8
	.value	50
	.byte	0
	.byte	9
	.value	197
	.byte	17
	.byte	7
	.value	11
	.byte	0
	.byte	8
	.value	98
	.byte	0
	.byte	8
	.value	34
	.byte	0
	.byte	9
	.value	165
	.byte	0
	.byte	8
	.value	2
	.byte	0
	.byte	8
	.value	130
	.byte	0
	.byte	8
	.value	66
	.byte	0
	.byte	9
	.value	229
	.byte	16
	.byte	7
	.value	7
	.byte	0
	.byte	8
	.value	90
	.byte	0
	.byte	8
	.value	26
	.byte	0
	.byte	9
	.value	149
	.byte	20
	.byte	7
	.value	67
	.byte	0
	.byte	8
	.value	122
	.byte	0
	.byte	8
	.value	58
	.byte	0
	.byte	9
	.value	213
	.byte	18
	.byte	7
	.value	19
	.byte	0
	.byte	8
	.value	106
	.byte	0
	.byte	8
	.value	42
	.byte	0
	.byte	9
	.value	181
	.byte	0
	.byte	8
	.value	10
	.byte	0
	.byte	8
	.value	138
	.byte	0
	.byte	8
	.value	74
	.byte	0
	.byte	9
	.value	245
	.byte	16
	.byte	7
	.value	5
	.byte	0
	.byte	8
	.value	86
	.byte	0
	.byte	8
	.value	22
	.byte	64
	.byte	8
	.value	0
	.byte	19
	.byte	7
	.value	51
	.byte	0
	.byte	8
	.value	118
	.byte	0
	.byte	8
	.value	54
	.byte	0
	.byte	9
	.value	205
	.byte	17
	.byte	7
	.value	15
	.byte	0
	.byte	8
	.value	102
	.byte	0
	.byte	8
	.value	38
	.byte	0
	.byte	9
	.value	173
	.byte	0
	.byte	8
	.value	6
	.byte	0
	.byte	8
	.value	134
	.byte	0
	.byte	8
	.value	70
	.byte	0
	.byte	9
	.value	237
	.byte	16
	.byte	7
	.value	9
	.byte	0
	.byte	8
	.value	94
	.byte	0
	.byte	8
	.value	30
	.byte	0
	.byte	9
	.value	157
	.byte	20
	.byte	7
	.value	99
	.byte	0
	.byte	8
	.value	126
	.byte	0
	.byte	8
	.value	62
	.byte	0
	.byte	9
	.value	221
	.byte	18
	.byte	7
	.value	27
	.byte	0
	.byte	8
	.value	110
	.byte	0
	.byte	8
	.value	46
	.byte	0
	.byte	9
	.value	189
	.byte	0
	.byte	8
	.value	14
	.byte	0
	.byte	8
	.value	142
	.byte	0
	.byte	8
	.value	78
	.byte	0
	.byte	9
	.value	253
	.byte	96
	.byte	7
	.value	0
	.byte	0
	.byte	8
	.value	81
	.byte	0
	.byte	8
	.value	17
	.byte	21
	.byte	8
	.value	131
	.byte	18
	.byte	7
	.value	31
	.byte	0
	.byte	8
	.value	113
	.byte	0
	.byte	8
	.value	49
	.byte	0
	.byte	9
	.value	195
	.byte	16
	.byte	7
	.value	10
	.byte	0
	.byte	8
	.value	97
	.byte	0
	.byte	8
	.value	33
	.byte	0
	.byte	9
	.value	163
	.byte	0
	.byte	8
	.value	1
	.byte	0
	.byte	8
	.value	129
	.byte	0
	.byte	8
	.value	65
	.byte	0
	.byte	9
	.value	227
	.byte	16
	.byte	7
	.value	6
	.byte	0
	.byte	8
	.value	89
	.byte	0
	.byte	8
	.value	25
	.byte	0
	.byte	9
	.value	147
	.byte	19
	.byte	7
	.value	59
	.byte	0
	.byte	8
	.value	121
	.byte	0
	.byte	8
	.value	57
	.byte	0
	.byte	9
	.value	211
	.byte	17
	.byte	7
	.value	17
	.byte	0
	.byte	8
	.value	105
	.byte	0
	.byte	8
	.value	41
	.byte	0
	.byte	9
	.value	179
	.byte	0
	.byte	8
	.value	9
	.byte	0
	.byte	8
	.value	137
	.byte	0
	.byte	8
	.value	73
	.byte	0
	.byte	9
	.value	243
	.byte	16
	.byte	7
	.value	4
	.byte	0
	.byte	8
	.value	85
	.byte	0
	.byte	8
	.value	21
	.byte	16
	.byte	8
	.value	258
	.byte	19
	.byte	7
	.value	43
	.byte	0
	.byte	8
	.value	117
	.byte	0
	.byte	8
	.value	53
	.byte	0
	.byte	9
	.value	203
	.byte	17
	.byte	7
	.value	13
	.byte	0
	.byte	8
	.value	101
	.byte	0
	.byte	8
	.value	37
	.byte	0
	.byte	9
	.value	171
	.byte	0
	.byte	8
	.value	5
	.byte	0
	.byte	8
	.value	133
	.byte	0
	.byte	8
	.value	69
	.byte	0
	.byte	9
	.value	235
	.byte	16
	.byte	7
	.value	8
	.byte	0
	.byte	8
	.value	93
	.byte	0
	.byte	8
	.value	29
	.byte	0
	.byte	9
	.value	155
	.byte	20
	.byte	7
	.value	83
	.byte	0
	.byte	8
	.value	125
	.byte	0
	.byte	8
	.value	61
	.byte	0
	.byte	9
	.value	219
	.byte	18
	.byte	7
	.value	23
	.byte	0
	.byte	8
	.value	109
	.byte	0
	.byte	8
	.value	45
	.byte	0
	.byte	9
	.value	187
	.byte	0
	.byte	8
	.value	13
	.byte	0
	.byte	8
	.value	141
	.byte	0
	.byte	8
	.value	77
	.byte	0
	.byte	9
	.value	251
	.byte	16
	.byte	7
	.value	3
	.byte	0
	.byte	8
	.value	83
	.byte	0
	.byte	8
	.value	19
	.byte	21
	.byte	8
	.value	195
	.byte	19
	.byte	7
	.value	35
	.byte	0
	.byte	8
	.value	115
	.byte	0
	.byte	8
	.value	51
	.byte	0
	.byte	9
	.value	199
	.byte	17
	.byte	7
	.value	11
	.byte	0
	.byte	8
	.value	99
	.byte	0
	.byte	8
	.value	35
	.byte	0
	.byte	9
	.value	167
	.byte	0
	.byte	8
	.value	3
	.byte	0
	.byte	8
	.value	131
	.byte	0
	.byte	8
	.value	67
	.byte	0
	.byte	9
	.value	231
	.byte	16
	.byte	7
	.value	7
	.byte	0
	.byte	8
	.value	91
	.byte	0
	.byte	8
	.value	27
	.byte	0
	.byte	9
	.value	151
	.byte	20
	.byte	7
	.value	67
	.byte	0
	.byte	8
	.value	123
	.byte	0
	.byte	8
	.value	59
	.byte	0
	.byte	9
	.value	215
	.byte	18
	.byte	7
	.value	19
	.byte	0
	.byte	8
	.value	107
	.byte	0
	.byte	8
	.value	43
	.byte	0
	.byte	9
	.value	183
	.byte	0
	.byte	8
	.value	11
	.byte	0
	.byte	8
	.value	139
	.byte	0
	.byte	8
	.value	75
	.byte	0
	.byte	9
	.value	247
	.byte	16
	.byte	7
	.value	5
	.byte	0
	.byte	8
	.value	87
	.byte	0
	.byte	8
	.value	23
	.byte	64
	.byte	8
	.value	0
	.byte	19
	.byte	7
	.value	51
	.byte	0
	.byte	8
	.value	119
	.byte	0
	.byte	8
	.value	55
	.byte	0
	.byte	9
	.value	207
	.byte	17
	.byte	7
	.value	15
	.byte	0
	.byte	8
	.value	103
	.byte	0
	.byte	8
	.value	39
	.byte	0
	.byte	9
	.value	175
	.byte	0
	.byte	8
	.value	7
	.byte	0
	.byte	8
	.value	135
	.byte	0
	.byte	8
	.value	71
	.byte	0
	.byte	9
	.value	239
	.byte	16
	.byte	7
	.value	9
	.byte	0
	.byte	8
	.value	95
	.byte	0
	.byte	8
	.value	31
	.byte	0
	.byte	9
	.value	159
	.byte	20
	.byte	7
	.value	99
	.byte	0
	.byte	8
	.value	127
	.byte	0
	.byte	8
	.value	63
	.byte	0
	.byte	9
	.value	223
	.byte	18
	.byte	7
	.value	27
	.byte	0
	.byte	8
	.value	111
	.byte	0
	.byte	8
	.value	47
	.byte	0
	.byte	9
	.value	191
	.byte	0
	.byte	8
	.value	15
	.byte	0
	.byte	8
	.value	143
	.byte	0
	.byte	8
	.value	79
	.byte	0
	.byte	9
	.value	255
	.align 32
	.type	order.6397, @object
	.size	order.6397, 38
order.6397:
	.value	16
	.value	17
	.value	18
	.value	0
	.value	8
	.value	7
	.value	9
	.value	6
	.value	10
	.value	5
	.value	11
	.value	4
	.value	12
	.value	3
	.value	13
	.value	2
	.value	14
	.value	1
	.value	15
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC18:
	.quad	6148914691236517205
	.quad	6148914691236517205
	.hidden	inflate_table
	.hidden	inflate_fast_chunk_
	.hidden	zcalloc
	.hidden	zcfree
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
