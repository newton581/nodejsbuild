	.file	"gzread.c"
	.text
	.p2align 4
	.type	gz_load, @function
gz_load:
.LFB63:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	$1073741824, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	xorl	%esi, %esi
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$8, %rsp
	movl	$0, (%rcx)
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L10:
	movl	%eax, %esi
	addl	(%r15), %esi
	movl	%esi, (%r15)
	cmpl	%ebx, %esi
	jnb	.L9
.L3:
	movl	%ebx, %edx
	movl	28(%r14), %edi
	subl	%esi, %edx
	cmpl	$1073741824, %edx
	cmova	%r13, %rdx
	addq	%r12, %rsi
	call	read@PLT
	movl	%eax, %r8d
	testl	%eax, %eax
	jg	.L10
	testl	%eax, %eax
	jne	.L11
	movl	$1, 80(%r14)
.L1:
	addq	$8, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	.cfi_restore_state
	xorl	%r8d, %r8d
	jmp	.L1
.L11:
	call	__errno_location@PLT
	movl	(%rax), %edi
	call	strerror@PLT
	movl	$-1, %esi
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	gz_error
	movl	$-1, %r8d
	jmp	.L1
	.cfi_endproc
.LFE63:
	.size	gz_load, .-gz_load
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"out of memory"
.LC1:
	.string	"1.2.11"
	.text
	.p2align 4
	.type	gz_look, @function
gz_look:
.LFB65:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	120(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movl	40(%rdi), %edi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testl	%edi, %edi
	je	.L58
.L13:
	movl	128(%r12), %edx
	cmpl	$1, %edx
	jbe	.L59
.L17:
	movq	120(%r12), %rsi
	cmpb	$31, (%rsi)
	je	.L60
.L24:
	movl	64(%r12), %ecx
	testl	%ecx, %ecx
	jne	.L61
.L26:
	movl	$0, 128(%r12)
	xorl	%eax, %eax
	movl	$1, 80(%r12)
	movl	$0, (%r12)
.L12:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L62
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L58:
	.cfi_restore_state
	movl	44(%r12), %edi
	movq	%rdi, %rbx
	call	malloc@PLT
	leal	(%rbx,%rbx), %edi
	movq	%rax, 48(%r12)
	movq	%rax, %r14
	call	malloc@PLT
	movq	%rax, 56(%r12)
	testq	%rax, %rax
	je	.L32
	testq	%r14, %r14
	je	.L32
	movl	%ebx, 40(%r12)
	pxor	%xmm0, %xmm0
	movl	$112, %ecx
	leaq	.LC1(%rip), %rdx
	movq	$0, 200(%r12)
	movl	$31, %esi
	movq	%r13, %rdi
	movl	$0, 128(%r12)
	movq	$0, 120(%r12)
	movups	%xmm0, 184(%r12)
	call	inflateInit2_@PLT
	testl	%eax, %eax
	je	.L13
	movq	56(%r12), %rdi
	call	free@PLT
	movq	48(%r12), %rdi
	call	free@PLT
	movl	$0, 40(%r12)
.L55:
	leaq	.LC0(%rip), %rdx
	movl	$-4, %esi
	movq	%r12, %rdi
	call	gz_error
.L56:
	movl	$-1, %eax
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L59:
	movl	108(%r12), %eax
	testl	%eax, %eax
	je	.L33
	cmpl	$-5, %eax
	jne	.L56
.L33:
	movl	80(%r12), %esi
	testl	%esi, %esi
	jne	.L19
	movq	48(%r12), %rsi
	testl	%edx, %edx
	je	.L20
	movq	120(%r12), %rax
	movzbl	(%rax), %eax
	movb	%al, (%rsi)
	movl	128(%r12), %edx
	movq	48(%r12), %rsi
.L20:
	movl	40(%r12), %r8d
	addq	%rdx, %rsi
	leaq	-44(%rbp), %rcx
	movq	%r12, %rdi
	subl	%edx, %r8d
	movl	%r8d, %edx
	call	gz_load
	cmpl	$-1, %eax
	je	.L12
	movq	48(%r12), %rax
	movl	-44(%rbp), %edx
	addl	128(%r12), %edx
	movl	%edx, 128(%r12)
	movq	%rax, 120(%r12)
	je	.L57
	cmpl	$1, %edx
	je	.L23
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L61:
	movq	56(%r12), %rdi
	movq	%rdi, 8(%r12)
.L25:
	call	memcpy@PLT
	movl	128(%r12), %eax
	movl	$0, 128(%r12)
	movl	%eax, (%r12)
	movabsq	$4294967297, %rax
	movq	%rax, 64(%r12)
.L57:
	xorl	%eax, %eax
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L60:
	cmpb	$-117, 1(%rsi)
	jne	.L24
	movq	%r13, %rdi
	call	inflateReset@PLT
	movabsq	$8589934592, %rax
	movq	%rax, 64(%r12)
	xorl	%eax, %eax
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L19:
	testl	%edx, %edx
	je	.L57
.L23:
	movl	64(%r12), %eax
	testl	%eax, %eax
	je	.L26
	movq	56(%r12), %rdi
	movq	120(%r12), %rsi
	movl	$1, %edx
	movq	%rdi, 8(%r12)
	jmp	.L25
.L62:
	call	__stack_chk_fail@PLT
.L32:
	movq	%rax, %rdi
	call	free@PLT
	movq	%r14, %rdi
	call	free@PLT
	jmp	.L55
	.cfi_endproc
.LFE65:
	.size	gz_look, .-gz_look
	.section	.rodata.str1.1
.LC2:
	.string	"compressed data error"
.LC3:
	.string	"unexpected end of file"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"internal error: inflate stream corrupt"
	.text
	.p2align 4
	.type	gz_decomp, @function
gz_decomp:
.LFB66:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	leaq	-44(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	leaq	120(%rdi), %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movl	152(%rdi), %ebx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L64:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	inflate@PLT
	leal	2(%rax), %edx
	andl	$-5, %edx
	je	.L94
	cmpl	$-4, %eax
	je	.L95
	cmpl	$-3, %eax
	je	.L96
	movl	152(%r14), %edx
	testl	%edx, %edx
	je	.L73
	cmpl	$1, %eax
	je	.L97
.L74:
	movl	128(%r14), %edx
	testl	%edx, %edx
	jne	.L64
	movl	108(%r14), %eax
	testl	%eax, %eax
	je	.L81
	cmpl	$-5, %eax
	jne	.L78
.L81:
	movl	80(%r14), %eax
	testl	%eax, %eax
	jne	.L67
	movl	40(%r14), %edx
	movq	48(%r14), %rsi
	movq	%r13, %rcx
	movq	%r14, %rdi
	call	gz_load
	movl	%eax, %r8d
	cmpl	$-1, %eax
	je	.L63
	movl	-44(%rbp), %eax
	addl	%eax, 128(%r14)
	movq	48(%r14), %rdx
	movq	%rdx, 120(%r14)
	jne	.L64
	.p2align 4,,10
	.p2align 3
.L67:
	leaq	.LC3(%rip), %rdx
	movl	$-5, %esi
	movq	%r14, %rdi
	call	gz_error
	movq	144(%r14), %rax
	subl	152(%r14), %ebx
	xorl	%r8d, %r8d
	movl	%ebx, (%r14)
	subq	%rbx, %rax
	movq	%rax, 8(%r14)
.L63:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L98
	addq	$16, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L94:
	.cfi_restore_state
	leaq	.LC4(%rip), %rdx
	movl	$-2, %esi
	movq	%r14, %rdi
	call	gz_error
	movl	$-1, %r8d
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L95:
	leaq	.LC0(%rip), %rdx
	movl	$-4, %esi
	movq	%r14, %rdi
	call	gz_error
	movl	$-1, %r8d
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L96:
	movq	168(%r14), %rdx
	leaq	.LC2(%rip), %rax
	movl	$-3, %esi
	movq	%r14, %rdi
	testq	%rdx, %rdx
	cmove	%rax, %rdx
	call	gz_error
	movl	$-1, %r8d
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L73:
	movq	144(%r14), %rdx
	movl	%ebx, (%r14)
	xorl	%r8d, %r8d
	subq	%rbx, %rdx
	movq	%rdx, 8(%r14)
	cmpl	$1, %eax
	jne	.L63
	movl	$0, 68(%r14)
	xorl	%r8d, %r8d
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L97:
	movq	144(%r14), %rax
	subl	%edx, %ebx
	movl	$0, 68(%r14)
	xorl	%r8d, %r8d
	movl	%ebx, (%r14)
	subq	%rbx, %rax
	movq	%rax, 8(%r14)
	jmp	.L63
.L78:
	movl	$-1, %r8d
	jmp	.L63
.L98:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE66:
	.size	gz_decomp, .-gz_decomp
	.p2align 4
	.type	gz_fetch, @function
gz_fetch:
.LFB67:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
.L113:
	movl	68(%rbx), %eax
	cmpl	$1, %eax
	je	.L100
	cmpl	$2, %eax
	je	.L101
	testl	%eax, %eax
	je	.L118
.L102:
	movl	(%rbx), %ecx
	testl	%ecx, %ecx
	jne	.L117
	movl	80(%rbx), %edx
	testl	%edx, %edx
	je	.L113
	movl	128(%rbx), %eax
	testl	%eax, %eax
	jne	.L113
	.p2align 4,,10
	.p2align 3
.L117:
	xorl	%eax, %eax
.L99:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L118:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	gz_look
	cmpl	$-1, %eax
	je	.L106
	movl	68(%rbx), %esi
	testl	%esi, %esi
	jne	.L102
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L101:
	movl	40(%rbx), %eax
	movq	%rbx, %rdi
	addl	%eax, %eax
	movl	%eax, 152(%rbx)
	movq	56(%rbx), %rax
	movq	%rax, 144(%rbx)
	call	gz_decomp
	cmpl	$-1, %eax
	jne	.L102
.L106:
	movl	$-1, %eax
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L100:
	movl	40(%rbx), %eax
	movq	56(%rbx), %rsi
	movq	%rbx, %rcx
	movq	%rbx, %rdi
	leal	(%rax,%rax), %edx
	call	gz_load
	cmpl	$-1, %eax
	je	.L106
	movq	56(%rbx), %rax
	movq	%rax, 8(%rbx)
	jmp	.L117
	.cfi_endproc
.LFE67:
	.size	gz_fetch, .-gz_fetch
	.p2align 4
	.type	gz_read.part.0, @function
gz_read.part.0:
.LFB79:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	xorl	%r14d, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	jmp	.L132
	.p2align 4,,10
	.p2align 3
.L145:
	movl	(%rbx), %eax
	movl	$-1, -60(%rbp)
	movl	$-1, %r8d
	testl	%eax, %eax
	je	.L122
.L146:
	cmpl	%r8d, %eax
	jnb	.L123
	movl	%eax, -60(%rbp)
	movl	%eax, %r8d
.L123:
	movl	%r8d, %r13d
	movq	8(%rbx), %rsi
	movq	%r15, %rdi
	movl	%r8d, -68(%rbp)
	movq	%r13, %rdx
	call	memcpy@PLT
	movl	-68(%rbp), %r8d
	addq	%r13, 8(%rbx)
	subl	%r8d, (%rbx)
.L124:
	addq	%r13, 16(%rbx)
	subq	%r13, %r12
	addq	%r13, %r15
	addq	%r13, %r14
.L129:
	testq	%r12, %r12
	je	.L119
.L132:
	movl	$4294967294, %eax
	cmpq	%rax, %r12
	ja	.L145
	movl	(%rbx), %eax
	movl	%r12d, -60(%rbp)
	movl	%r12d, %r8d
	testl	%eax, %eax
	jne	.L146
.L122:
	movl	80(%rbx), %edx
	testl	%edx, %edx
	je	.L125
	movl	128(%rbx), %eax
	testl	%eax, %eax
	je	.L147
.L125:
	movl	68(%rbx), %edx
	testl	%edx, %edx
	je	.L127
	movl	40(%rbx), %eax
	addl	%eax, %eax
	cmpl	%r8d, %eax
	ja	.L127
	cmpl	$1, %edx
	je	.L148
	movl	%r8d, 152(%rbx)
	movq	%rbx, %rdi
	movq	%r15, 144(%rbx)
	call	gz_decomp
	cmpl	$-1, %eax
	je	.L131
	movl	(%rbx), %r13d
	movl	$0, (%rbx)
	movl	%r13d, -60(%rbp)
	jmp	.L124
	.p2align 4,,10
	.p2align 3
.L127:
	movq	%rbx, %rdi
	call	gz_fetch
	cmpl	$-1, %eax
	jne	.L129
.L131:
	xorl	%r14d, %r14d
.L119:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L149
	addq	$40, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L148:
	.cfi_restore_state
	leaq	-60(%rbp), %rcx
	movl	%r8d, %edx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	gz_load
	cmpl	$-1, %eax
	je	.L131
	movl	-60(%rbp), %r13d
	jmp	.L124
.L147:
	movl	$1, 84(%rbx)
	jmp	.L119
.L149:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE79:
	.size	gz_read.part.0, .-gz_read.part.0
	.p2align 4
	.type	gz_skip, @function
gz_skip:
.LFB68:
	.cfi_startproc
	testq	%rsi, %rsi
	je	.L180
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	(%rdi), %eax
	movq	%rdi, %rbx
	.p2align 4,,10
	.p2align 3
.L151:
	testl	%eax, %eax
	je	.L153
.L177:
	movl	%eax, %ecx
	xorl	%esi, %esi
	movq	%rcx, %rdx
	cmpq	%r12, %rcx
	jle	.L154
	subl	%r12d, %eax
	movl	%r12d, %edx
	movl	%r12d, %ecx
	movl	%eax, %esi
.L154:
	addq	%rcx, 8(%rbx)
	addq	%rdx, 16(%rbx)
	movl	%esi, (%rbx)
	subq	%rdx, %r12
	je	.L156
	movl	%esi, %eax
	testl	%eax, %eax
	jne	.L177
.L153:
	movl	80(%rbx), %edi
	testl	%edi, %edi
	je	.L174
	movl	128(%rbx), %esi
	testl	%esi, %esi
	je	.L156
.L174:
	movl	68(%rbx), %eax
	cmpl	$1, %eax
	je	.L158
.L187:
	cmpl	$2, %eax
	je	.L159
	testl	%eax, %eax
	je	.L186
.L160:
	movl	80(%rbx), %edx
	testl	%edx, %edx
	je	.L174
	movl	128(%rbx), %eax
	testl	%eax, %eax
	je	.L153
	movl	68(%rbx), %eax
	cmpl	$1, %eax
	jne	.L187
.L158:
	movl	40(%rbx), %eax
	movq	56(%rbx), %rsi
	movq	%rbx, %rcx
	movq	%rbx, %rdi
	leal	(%rax,%rax), %edx
	call	gz_load
	cmpl	$-1, %eax
	je	.L164
	movq	56(%rbx), %rax
	movq	%rax, 8(%rbx)
.L183:
	movl	(%rbx), %eax
	jmp	.L151
	.p2align 4,,10
	.p2align 3
.L186:
	movq	%rbx, %rdi
	call	gz_look
	cmpl	$-1, %eax
	je	.L164
	movl	68(%rbx), %ecx
	testl	%ecx, %ecx
	je	.L183
.L184:
	movl	(%rbx), %eax
	testl	%eax, %eax
	je	.L160
	jmp	.L177
	.p2align 4,,10
	.p2align 3
.L159:
	movl	40(%rbx), %eax
	movq	%rbx, %rdi
	addl	%eax, %eax
	movl	%eax, 152(%rbx)
	movq	56(%rbx), %rax
	movq	%rax, 144(%rbx)
	call	gz_decomp
	cmpl	$-1, %eax
	jne	.L184
.L164:
	popq	%rbx
	movl	$-1, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L156:
	.cfi_restore_state
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
.L180:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE68:
	.size	gz_skip, .-gz_skip
	.section	.rodata.str1.8
	.align 8
.LC5:
	.string	"request does not fit in an int"
	.text
	.p2align 4
	.globl	gzread
	.type	gzread, @function
gzread:
.LFB70:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L248
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	cmpl	$7247, 24(%rdi)
	jne	.L246
	movl	108(%rdi), %eax
	movq	%rsi, %r14
	movl	%edx, %r13d
	testl	%eax, %eax
	je	.L214
	cmpl	$-5, %eax
	jne	.L246
.L214:
	testl	%r13d, %r13d
	js	.L249
	movl	$0, %eax
	je	.L188
	movl	104(%r12), %r8d
	testl	%r8d, %r8d
	jne	.L195
.L198:
	movl	%r13d, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	gz_read.part.0
	testl	%eax, %eax
	je	.L196
.L188:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L195:
	.cfi_restore_state
	movl	$0, 104(%r12)
	movq	96(%r12), %rbx
	testq	%rbx, %rbx
	je	.L198
.L244:
	movl	(%r12), %eax
	.p2align 4,,10
	.p2align 3
.L210:
	testl	%eax, %eax
	je	.L199
.L238:
	movl	%eax, %ecx
	xorl	%esi, %esi
	movq	%rcx, %rdx
	cmpq	%rbx, %rcx
	jle	.L200
	subl	%ebx, %eax
	movl	%ebx, %edx
	movl	%ebx, %ecx
	movl	%eax, %esi
.L200:
	addq	%rcx, 8(%r12)
	addq	%rdx, 16(%r12)
	movl	%esi, (%r12)
	subq	%rdx, %rbx
	je	.L198
	movl	%esi, %eax
	testl	%eax, %eax
	jne	.L238
.L199:
	movl	80(%r12), %edi
	testl	%edi, %edi
	je	.L230
	movl	128(%r12), %esi
	testl	%esi, %esi
	je	.L198
.L230:
	movl	68(%r12), %eax
	cmpl	$1, %eax
	je	.L203
.L251:
	cmpl	$2, %eax
	je	.L204
	testl	%eax, %eax
	je	.L250
.L205:
	movl	80(%r12), %edx
	testl	%edx, %edx
	je	.L230
	movl	128(%r12), %eax
	testl	%eax, %eax
	je	.L199
	movl	68(%r12), %eax
	cmpl	$1, %eax
	jne	.L251
.L203:
	movl	40(%r12), %eax
	movq	56(%r12), %rsi
	movq	%r12, %rcx
	movq	%r12, %rdi
	leal	(%rax,%rax), %edx
	call	gz_load
	cmpl	$-1, %eax
	je	.L206
	movq	56(%r12), %rax
	movq	%rax, 8(%r12)
	movl	(%r12), %eax
	jmp	.L210
	.p2align 4,,10
	.p2align 3
.L250:
	movq	%r12, %rdi
	call	gz_look
	cmpl	$-1, %eax
	je	.L206
	movl	68(%r12), %ecx
	testl	%ecx, %ecx
	je	.L244
.L245:
	movl	(%r12), %eax
	testl	%eax, %eax
	je	.L205
	jmp	.L238
	.p2align 4,,10
	.p2align 3
.L204:
	movl	40(%r12), %eax
	movq	%r12, %rdi
	addl	%eax, %eax
	movl	%eax, 152(%r12)
	movq	56(%r12), %rax
	movq	%rax, 144(%r12)
	call	gz_decomp
	cmpl	$-1, %eax
	jne	.L245
.L206:
	xorl	%eax, %eax
.L196:
	movl	108(%r12), %edx
	cmpl	$-5, %edx
	je	.L188
	testl	%edx, %edx
	je	.L188
	.p2align 4,,10
	.p2align 3
.L246:
	movl	$-1, %eax
	jmp	.L188
.L249:
	leaq	.LC5(%rip), %rdx
	movl	$-2, %esi
	movq	%r12, %rdi
	call	gz_error
	jmp	.L246
.L248:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	orl	$-1, %eax
	ret
	.cfi_endproc
.LFE70:
	.size	gzread, .-gzread
	.section	.rodata.str1.8
	.align 8
.LC6:
	.string	"request does not fit in a size_t"
	.text
	.p2align 4
	.globl	gzfread
	.type	gzfread, @function
gzfread:
.LFB71:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%rcx, %rcx
	je	.L264
	xorl	%r13d, %r13d
	cmpl	$7247, 24(%rcx)
	movq	%rcx, %r12
	jne	.L252
	movl	108(%rcx), %eax
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movq	%rdx, %rsi
	testl	%eax, %eax
	je	.L270
	cmpl	$-5, %eax
	je	.L270
.L252:
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L270:
	.cfi_restore_state
	xorl	%r13d, %r13d
	testq	%rbx, %rbx
	je	.L252
	movq	%rsi, %r13
	xorl	%edx, %edx
	imulq	%rbx, %r13
	movq	%r13, %rax
	divq	%rbx
	cmpq	%rsi, %rax
	jne	.L282
	testq	%r13, %r13
	je	.L252
	movl	104(%r12), %ecx
	testl	%ecx, %ecx
	jne	.L256
.L258:
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	gz_read.part.0
	xorl	%edx, %edx
	divq	%rbx
	movq	%rax, %r13
	jmp	.L252
	.p2align 4,,10
	.p2align 3
.L259:
	movl	80(%r12), %edx
	testl	%edx, %edx
	je	.L262
	movl	128(%r12), %eax
	testl	%eax, %eax
	je	.L258
.L262:
	movq	%r12, %rdi
	call	gz_fetch
	cmpl	$-1, %eax
	jne	.L263
	.p2align 4,,10
	.p2align 3
.L264:
	xorl	%r13d, %r13d
	jmp	.L252
	.p2align 4,,10
	.p2align 3
.L256:
	movl	$0, 104(%r12)
	movq	96(%r12), %r15
	testq	%r15, %r15
	je	.L258
	.p2align 4,,10
	.p2align 3
.L263:
	movl	(%r12), %eax
	testl	%eax, %eax
	je	.L259
	movl	%eax, %ecx
	xorl	%esi, %esi
	movq	%rcx, %rdx
	cmpq	%r15, %rcx
	jle	.L260
	subl	%r15d, %eax
	movl	%r15d, %edx
	movl	%r15d, %ecx
	movl	%eax, %esi
.L260:
	addq	%rcx, 8(%r12)
	addq	%rdx, 16(%r12)
	movl	%esi, (%r12)
	subq	%rdx, %r15
	jne	.L263
	jmp	.L258
	.p2align 4,,10
	.p2align 3
.L282:
	leaq	.LC6(%rip), %rdx
	movl	$-2, %esi
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	call	gz_error
	jmp	.L252
	.cfi_endproc
.LFE71:
	.size	gzfread, .-gzfread
	.p2align 4
	.globl	gzgetc
	.type	gzgetc, @function
gzgetc:
.LFB72:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L295
	cmpl	$7247, 24(%rdi)
	jne	.L295
	movl	108(%rdi), %eax
	testl	%eax, %eax
	je	.L296
	cmpl	$-5, %eax
	jne	.L295
.L296:
	movl	(%rdi), %eax
	testl	%eax, %eax
	je	.L286
	subl	$1, %eax
	addq	$1, 16(%rdi)
	movl	%eax, (%rdi)
	movq	8(%rdi), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 8(%rdi)
	movzbl	(%rax), %eax
.L283:
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L301
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L286:
	.cfi_restore_state
	movl	104(%rdi), %eax
	testl	%eax, %eax
	jne	.L287
.L290:
	leaq	-9(%rbp), %rsi
	movl	$1, %edx
	call	gz_read.part.0
	testl	%eax, %eax
	jle	.L295
	movzbl	-9(%rbp), %eax
	jmp	.L283
	.p2align 4,,10
	.p2align 3
.L287:
	movl	$0, 104(%rdi)
	movq	96(%rdi), %rsi
	movq	%rdi, -24(%rbp)
	call	gz_skip
	movq	-24(%rbp), %rdi
	cmpl	$-1, %eax
	jne	.L290
.L295:
	movl	$-1, %eax
	jmp	.L283
.L301:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE72:
	.size	gzgetc, .-gzgetc
	.p2align 4
	.globl	gzgetc_
	.type	gzgetc_, @function
gzgetc_:
.LFB73:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L317
	cmpl	$7247, 24(%rdi)
	movq	%rdi, %r12
	jne	.L317
	movl	108(%rdi), %eax
	testl	%eax, %eax
	je	.L319
	cmpl	$-5, %eax
	jne	.L317
.L319:
	movl	(%r12), %eax
	testl	%eax, %eax
	je	.L305
	subl	$1, %eax
	addq	$1, 16(%r12)
	movl	%eax, (%r12)
	movq	8(%r12), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 8(%r12)
	movzbl	(%rax), %eax
.L302:
	movq	-24(%rbp), %rsi
	xorq	%fs:40, %rsi
	jne	.L327
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L305:
	.cfi_restore_state
	movl	104(%r12), %ecx
	testl	%ecx, %ecx
	jne	.L328
.L306:
	leaq	-25(%rbp), %rsi
	movl	$1, %edx
	movq	%r12, %rdi
	call	gz_read.part.0
	testl	%eax, %eax
	jle	.L317
	movzbl	-25(%rbp), %eax
	jmp	.L302
	.p2align 4,,10
	.p2align 3
.L328:
	movl	$0, 104(%r12)
	movq	96(%r12), %rbx
	testq	%rbx, %rbx
	je	.L306
	xorl	%ecx, %ecx
	jmp	.L312
	.p2align 4,,10
	.p2align 3
.L329:
	movl	%ecx, %edx
	xorl	%esi, %esi
	movq	%rdx, %rax
	cmpq	%rbx, %rdx
	jle	.L308
	subl	%ebx, %ecx
	movl	%ebx, %eax
	movl	%ebx, %edx
	movl	%ecx, %esi
.L308:
	addq	%rdx, 8(%r12)
	addq	%rax, 16(%r12)
	movl	%esi, (%r12)
	subq	%rax, %rbx
	je	.L306
.L309:
	movl	(%r12), %ecx
.L312:
	testl	%ecx, %ecx
	jne	.L329
	movl	80(%r12), %edx
	testl	%edx, %edx
	je	.L310
	movl	128(%r12), %eax
	testl	%eax, %eax
	je	.L306
.L310:
	movq	%r12, %rdi
	call	gz_fetch
	cmpl	$-1, %eax
	jne	.L309
.L317:
	movl	$-1, %eax
	jmp	.L302
.L327:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE73:
	.size	gzgetc_, .-gzgetc_
	.section	.rodata.str1.8
	.align 8
.LC7:
	.string	"out of room to push characters"
	.text
	.p2align 4
	.globl	gzungetc
	.type	gzungetc, @function
gzungetc:
.LFB74:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L356
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpl	$7247, 24(%rsi)
	jne	.L355
	movl	108(%rsi), %eax
	movl	%edi, %ebx
	testl	%eax, %eax
	je	.L343
	cmpl	$-5, %eax
	jne	.L355
.L343:
	movl	104(%r12), %eax
	testl	%eax, %eax
	jne	.L335
.L337:
	testl	%ebx, %ebx
	js	.L355
	movl	40(%r12), %eax
	movl	(%r12), %esi
	leal	(%rax,%rax), %ecx
	testl	%esi, %esi
	je	.L357
	cmpl	%ecx, %esi
	je	.L358
	movq	8(%r12), %rax
	movq	56(%r12), %rdi
	cmpq	%rdi, %rax
	je	.L359
.L340:
	leaq	-1(%rax), %rdx
	addl	$1, %esi
	movl	%esi, (%r12)
	movq	%rdx, 8(%r12)
	movb	%bl, -1(%rax)
	movl	%ebx, %eax
	subq	$1, 16(%r12)
	movl	$0, 84(%r12)
.L330:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L357:
	.cfi_restore_state
	movq	56(%r12), %rax
	movl	$1, (%r12)
	leaq	-1(%rax,%rcx), %rax
	movq	%rax, 8(%r12)
	movb	%bl, (%rax)
	movl	%ebx, %eax
	subq	$1, 16(%r12)
	movl	$0, 84(%r12)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L335:
	.cfi_restore_state
	movl	$0, 104(%r12)
	movq	96(%r12), %rsi
	movq	%r12, %rdi
	call	gz_skip
	cmpl	$-1, %eax
	jne	.L337
	.p2align 4,,10
	.p2align 3
.L355:
	movl	$-1, %eax
	jmp	.L330
	.p2align 4,,10
	.p2align 3
.L359:
	movl	%esi, %edx
	addq	%rax, %rdx
	addq	%rcx, %rax
	cmpq	%rdx, %rdi
	jnb	.L340
	.p2align 4,,10
	.p2align 3
.L342:
	movzbl	-1(%rdx), %ecx
	subq	$1, %rdx
	subq	$1, %rax
	movb	%cl, (%rax)
	cmpq	%rdx, 56(%r12)
	jb	.L342
	movl	(%r12), %esi
	jmp	.L340
.L358:
	leaq	.LC7(%rip), %rdx
	movl	$-3, %esi
	movq	%r12, %rdi
	call	gz_error
	jmp	.L355
.L356:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	orl	$-1, %eax
	ret
	.cfi_endproc
.LFE74:
	.size	gzungetc, .-gzungetc
	.p2align 4
	.globl	gzgets
	.type	gzgets, @function
gzgets:
.LFB75:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%edx, %r12d
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	testq	%rsi, %rsi
	movq	%rsi, -64(%rbp)
	sete	%dl
	testl	%r12d, %r12d
	setle	%al
	orb	%al, %dl
	jne	.L364
	movq	%rdi, %rbx
	testq	%rdi, %rdi
	je	.L364
	cmpl	$7247, 24(%rdi)
	jne	.L364
	movl	108(%rdi), %eax
	testl	%eax, %eax
	je	.L373
	cmpl	$-5, %eax
	jne	.L364
.L373:
	movl	104(%rbx), %eax
	testl	%eax, %eax
	jne	.L366
.L368:
	subl	$1, %r12d
	movl	%r12d, -52(%rbp)
	je	.L364
	movl	(%rbx), %eax
	movq	-64(%rbp), %r13
	testl	%eax, %eax
	je	.L386
	.p2align 4,,10
	.p2align 3
.L369:
	movl	-52(%rbp), %ecx
	movq	8(%rbx), %r15
	movl	$10, %esi
	cmpl	%eax, %ecx
	movq	%r15, %rdi
	cmovbe	%ecx, %eax
	movl	%eax, %r12d
	movq	%r12, %rdx
	call	memchr@PLT
	testq	%rax, %rax
	je	.L371
	subq	%r15, %rax
	movq	%r13, %rdi
	movq	%r15, %rsi
	leal	1(%rax), %r14d
	movq	%r14, %rdx
	addq	%r14, %r13
	call	memcpy@PLT
	subl	%r14d, (%rbx)
	addq	%r14, 8(%rbx)
	addq	%r14, 16(%rbx)
.L370:
	movq	-64(%rbp), %rax
	cmpq	%rax, %r13
	je	.L364
	movb	$0, 0(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L366:
	.cfi_restore_state
	movl	$0, 104(%rbx)
	movq	96(%rbx), %rsi
	movq	%rbx, %rdi
	call	gz_skip
	cmpl	$-1, %eax
	jne	.L368
.L364:
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L371:
	.cfi_restore_state
	movq	%r13, %rdi
	movq	%r12, %rdx
	movq	%r15, %rsi
	addq	%r12, %r13
	call	memcpy@PLT
	movl	(%rbx), %eax
	addq	%r12, 8(%rbx)
	addq	%r12, 16(%rbx)
	subl	%r12d, %eax
	subl	%r12d, -52(%rbp)
	movl	%eax, (%rbx)
	je	.L370
	testl	%eax, %eax
	jne	.L369
.L386:
	movq	%rbx, %rdi
	call	gz_fetch
	cmpl	$-1, %eax
	je	.L364
	movl	(%rbx), %eax
	testl	%eax, %eax
	jne	.L369
	movl	$1, 84(%rbx)
	jmp	.L370
	.cfi_endproc
.LFE75:
	.size	gzgets, .-gzgets
	.p2align 4
	.globl	gzdirect
	.type	gzdirect, @function
gzdirect:
.LFB76:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L390
	cmpl	$7247, 24(%rdi)
	je	.L396
.L392:
	movl	64(%rdi), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L396:
	movl	68(%rdi), %edx
	testl	%edx, %edx
	jne	.L392
	movl	(%rdi), %eax
	testl	%eax, %eax
	jne	.L392
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	call	gz_look
	movq	-8(%rbp), %rdi
	movl	64(%rdi), %eax
	leave
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L390:
	.cfi_restore 6
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE76:
	.size	gzdirect, .-gzdirect
	.p2align 4
	.globl	gzclose_r
	.type	gzclose_r, @function
gzclose_r:
.LFB77:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	testq	%rdi, %rdi
	je	.L402
	cmpl	$7247, 24(%rdi)
	movq	%rdi, %r12
	jne	.L402
	movl	40(%rdi), %eax
	testl	%eax, %eax
	jne	.L407
.L399:
	movl	108(%r12), %r13d
	movl	$0, %eax
	movq	%r12, %rdi
	cmpl	$-5, %r13d
	cmovne	%eax, %r13d
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	gz_error
	movq	32(%r12), %rdi
	call	free@PLT
	movl	28(%r12), %edi
	call	close@PLT
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	free@PLT
	testl	%ebx, %ebx
	movl	$-1, %eax
	cmovne	%eax, %r13d
.L397:
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L407:
	.cfi_restore_state
	leaq	120(%rdi), %rdi
	call	inflateEnd@PLT
	movq	56(%r12), %rdi
	call	free@PLT
	movq	48(%r12), %rdi
	call	free@PLT
	jmp	.L399
	.p2align 4,,10
	.p2align 3
.L402:
	movl	$-2, %r13d
	jmp	.L397
	.cfi_endproc
.LFE77:
	.size	gzclose_r, .-gzclose_r
	.hidden	gz_error
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
