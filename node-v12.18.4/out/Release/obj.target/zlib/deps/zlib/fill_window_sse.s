	.file	"fill_window_sse.c"
	.text
	.p2align 4
	.globl	fill_window_sse
	.hidden	fill_window_sse
	.type	fill_window_sse, @function
fill_window_sse:
.LFB5306:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movl	160(%rdi), %r14d
	movl	260(%rdi), %eax
	movd	%r14d, %xmm1
	movq	%r14, %r12
	leaq	-16(%r14,%r14), %r15
	movl	%r14d, %edx
	punpcklwd	%xmm1, %xmm1
	leal	-262(%r14), %r13d
	pshufd	$0, %xmm1, %xmm1
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L2:
	movq	(%rbx), %rdi
	movaps	%xmm1, -64(%rbp)
	movl	8(%rdi), %r9d
	testl	%r9d, %r9d
	je	.L5
.L28:
	movl	252(%rbx), %esi
	movl	260(%rbx), %eax
	movl	%r8d, %edx
	addq	%rax, %rsi
	addq	176(%rbx), %rsi
	call	deflate_read_buf@PLT
	addl	260(%rbx), %eax
	movdqa	-64(%rbp), %xmm1
	cmpl	$2, %eax
	movl	%eax, 260(%rbx)
	jbe	.L10
	movl	252(%rbx), %ecx
	movq	176(%rbx), %r9
	movq	%rcx, %rdi
	addq	%r9, %rcx
	movzbl	(%rcx), %edx
	movl	%edx, 208(%rbx)
	testl	%edi, %edi
	je	.L8
	movzbl	(%rcx), %r10d
	leal	-1(%rdi), %edx
	leal	1(%rdi), %ecx
	cmpl	$5, 276(%rbx)
	movzbl	(%r9,%rdx), %edx
	movzbl	(%r9,%rcx), %r8d
	movl	220(%rbx), %esi
	jg	.L9
	imull	$3483, %edx, %ecx
	imull	$23081, %r10d, %edx
	addl	%edx, %ecx
	imull	$6954, %r8d, %edx
	addl	%edx, %ecx
	leal	2(%rdi), %edx
	movzbl	(%r9,%rdx), %edx
	imull	$20947, %edx, %edx
	addl	%ecx, %edx
	andl	%esi, %edx
	movl	%edx, 208(%rbx)
.L8:
	cmpl	$261, %eax
	ja	.L5
.L10:
	movq	(%rbx), %rdx
	movl	8(%rdx), %r8d
	testl	%r8d, %r8d
	je	.L5
	movl	160(%rbx), %edx
.L11:
	movl	252(%rbx), %ecx
	movl	184(%rbx), %r8d
	addl	%r13d, %edx
	subl	%ecx, %r8d
	subl	%eax, %r8d
	cmpl	%edx, %ecx
	jb	.L2
	movq	176(%rbx), %rdi
	movq	%r14, %rdx
	movl	%r8d, -64(%rbp)
	movaps	%xmm1, -80(%rbp)
	leaq	(%rdi,%r14), %rsi
	call	memcpy@PLT
	subl	%r12d, 256(%rbx)
	movdqa	-80(%rbp), %xmm1
	subl	%r12d, 252(%rbx)
	movl	212(%rbx), %ecx
	subq	%r14, 232(%rbx)
	movq	200(%rbx), %rax
	movl	-64(%rbp), %r8d
	movq	%rcx, %rdx
	leaq	-16(%rax,%rcx,2), %rax
	.p2align 4,,10
	.p2align 3
.L3:
	movdqu	(%rax), %xmm0
	subq	$16, %rax
	psubusw	%xmm1, %xmm0
	movups	%xmm0, 16(%rax)
	subl	$8, %edx
	jne	.L3
	movq	192(%rbx), %rax
	movl	%r12d, %edx
	addq	%r15, %rax
	.p2align 4,,10
	.p2align 3
.L4:
	movdqu	(%rax), %xmm0
	subq	$16, %rax
	psubusw	%xmm1, %xmm0
	movups	%xmm0, 16(%rax)
	subl	$8, %edx
	jne	.L4
	movq	(%rbx), %rdi
	addl	%r12d, %r8d
	movaps	%xmm1, -64(%rbp)
	movl	8(%rdi), %r9d
	testl	%r9d, %r9d
	jne	.L28
	.p2align 4,,10
	.p2align 3
.L5:
	movq	6024(%rbx), %rdx
	movq	184(%rbx), %rax
	cmpq	%rax, %rdx
	jnb	.L1
	movl	252(%rbx), %esi
	movl	260(%rbx), %ecx
	addq	%rcx, %rsi
	cmpq	%rsi, %rdx
	jb	.L29
	leaq	258(%rsi), %rcx
	cmpq	%rcx, %rdx
	jb	.L30
.L1:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	.cfi_restore_state
	imull	$25881, %edx, %ecx
	imull	$24674, %r10d, %edx
	addl	%edx, %ecx
	imull	$25811, %r8d, %edx
	addl	%ecx, %edx
	andl	%esi, %edx
	movl	%edx, 208(%rbx)
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L29:
	subq	%rsi, %rax
	movl	$258, %edx
	cmpq	$258, %rax
	cmovbe	%rax, %rdx
	movq	176(%rbx), %rax
	addq	%rsi, %rax
	cmpl	$8, %edx
	jnb	.L14
	testb	$4, %dl
	jne	.L31
	testl	%edx, %edx
	je	.L15
	movb	$0, (%rax)
	testb	$2, %dl
	je	.L15
	movl	%edx, %ecx
	xorl	%edi, %edi
	movw	%di, -2(%rax,%rcx)
.L15:
	addq	%rsi, %rdx
	movq	%rdx, 6024(%rbx)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L30:
	.cfi_restore_state
	movq	%rsi, %rcx
	subq	%rdx, %rax
	movq	176(%rbx), %rdi
	subq	%rdx, %rcx
	leaq	258(%rcx), %r12
	cmpq	%r12, %rax
	cmovbe	%rax, %r12
	addq	%rdx, %rdi
	xorl	%esi, %esi
	movl	%r12d, %r8d
	movq	%r8, %rdx
	call	memset@PLT
	addq	%r12, 6024(%rbx)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L14:
	.cfi_restore_state
	leaq	8(%rax), %rdi
	movl	%edx, %ecx
	movq	$0, (%rax)
	movq	$0, -8(%rax,%rcx)
	andq	$-8, %rdi
	subq	%rdi, %rax
	leal	(%rdx,%rax), %ecx
	xorl	%eax, %eax
	shrl	$3, %ecx
	rep stosq
	jmp	.L15
.L31:
	movl	%edx, %ecx
	movl	$0, (%rax)
	movl	$0, -4(%rax,%rcx)
	jmp	.L15
	.cfi_endproc
.LFE5306:
	.size	fill_window_sse, .-fill_window_sse
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
