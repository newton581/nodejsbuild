	.file	"inffast.c"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"invalid distance too far back"
.LC1:
	.string	"invalid distance code"
.LC2:
	.string	"invalid literal/length code"
	.text
	.p2align 4
	.globl	inflate_fast
	.hidden	inflate_fast
	.type	inflate_fast, @function
inflate_fast:
.LFB38:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %edx
	movl	%edx, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	56(%rdi), %r10
	movl	8(%rdi), %eax
	movq	(%rdi), %r15
	movq	24(%rdi), %rdi
	movl	124(%r10), %ecx
	subl	$5, %eax
	movl	68(%r10), %r13d
	addq	%r15, %rax
	movq	%rdi, %rbx
	movq	104(%r10), %r9
	movq	112(%r10), %r12
	sall	%cl, %r8d
	movl	120(%r10), %ecx
	movq	%rax, -56(%rbp)
	leal	-1(%r13), %r11d
	movl	32(%r14), %eax
	subl	$1, %r8d
	sall	%cl, %edx
	movl	%r13d, %ecx
	subl	$1, %edx
	subl	%eax, %esi
	subl	$257, %eax
	andl	$-16, %ecx
	movq	%rdx, -72(%rbp)
	movl	%r13d, %edx
	subq	%rsi, %rbx
	addq	%rdi, %rax
	shrl	$4, %edx
	movl	64(%r10), %esi
	movq	%rbx, -104(%rbp)
	movq	%rax, -64(%rbp)
	movq	72(%r10), %rbx
	salq	$4, %rdx
	movl	60(%r10), %eax
	movq	%rdx, -160(%rbp)
	movl	%ecx, %edx
	movl	%esi, -116(%rbp)
	movl	88(%r10), %esi
	movl	%eax, -76(%rbp)
	movq	80(%r10), %rax
	movq	%rbx, -112(%rbp)
	movl	%ecx, -144(%rbp)
	movq	%rdx, -88(%rbp)
	movq	-88(%rbp), %rdx
	movl	%r11d, -140(%rbp)
	addq	%rbx, %rdx
	addq	$16, %rbx
	movl	%r13d, -80(%rbp)
	movq	%rdx, -152(%rbp)
	movl	%r11d, %edx
	subl	%ecx, %r11d
	movl	-76(%rbp), %ecx
	movq	%rbx, -168(%rbp)
	movl	%edx, %ebx
	addl	%r13d, %ecx
	movl	%r11d, -128(%rbp)
	movl	%ecx, -124(%rbp)
	movq	%rbx, -136(%rbp)
	movq	%r8, -96(%rbp)
	movl	$1, %r8d
.L51:
	cmpl	$14, %esi
	ja	.L2
	movzbl	1(%r15), %edx
	leal	8(%rsi), %ecx
	movzbl	(%r15), %r11d
	addq	$2, %r15
	salq	%cl, %rdx
	movl	%esi, %ecx
	addl	$16, %esi
	salq	%cl, %r11
	addq	%r11, %rdx
	addq	%rdx, %rax
.L2:
	movq	-72(%rbp), %rdx
	andq	%rax, %rdx
	leaq	(%r9,%rdx,4), %rdx
	movzbl	1(%rdx), %ecx
	movzbl	(%rdx), %r11d
	movzwl	2(%rdx), %ebx
	shrq	%cl, %rax
	subl	%ecx, %esi
	movzbl	%r11b, %ecx
	testl	%ecx, %ecx
	je	.L3
	testb	$16, %r11b
	jne	.L4
	testb	$64, %r11b
	jne	.L6
	movl	%ebx, %r13d
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L286:
	testb	$16, %r11b
	jne	.L298
	testb	$64, %r11b
	jne	.L6
.L5:
	movl	%r8d, %edx
	sall	%cl, %edx
	leal	-1(%rdx), %ebx
	andq	%rax, %rbx
	movq	%rbx, %rdx
	movzwl	%r13w, %ebx
	addq	%rdx, %rbx
	leaq	(%r9,%rbx,4), %rdx
	movzbl	1(%rdx), %ecx
	movzbl	(%rdx), %r11d
	movzwl	2(%rdx), %r13d
	shrq	%cl, %rax
	subl	%ecx, %esi
	movzbl	%r11b, %ecx
	testl	%ecx, %ecx
	jne	.L286
	movl	%r13d, %ebx
.L3:
	movb	%bl, (%rdi)
	addq	$1, %rdi
.L7:
	cmpq	-56(%rbp), %r15
	jnb	.L19
	cmpq	-64(%rbp), %rdi
	jb	.L51
	.p2align 4,,10
	.p2align 3
.L19:
	movl	%esi, %edx
	andl	$7, %esi
	movl	$1, %r8d
	movq	%rdi, 24(%r14)
	movl	%esi, %ecx
	shrl	$3, %edx
	movq	-56(%rbp), %rsi
	sall	%cl, %r8d
	subq	%rdx, %r15
	subl	$1, %r8d
	movq	%r15, (%r14)
	andq	%r8, %rax
	cmpq	%r15, %rsi
	jbe	.L53
	subq	%r15, %rsi
	movq	%rsi, %rdx
	addl	$5, %edx
.L54:
	movq	-64(%rbp), %rsi
	movl	%edx, 8(%r14)
	cmpq	%rsi, %rdi
	jnb	.L55
	subq	%rdi, %rsi
	movq	%rsi, %rdx
	addl	$257, %edx
.L56:
	movl	%edx, 32(%r14)
	movq	%rax, 80(%r10)
	movl	%ecx, 88(%r10)
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L298:
	.cfi_restore_state
	movl	%r13d, %ebx
.L4:
	movzwl	%bx, %edx
	andl	$15, %r11d
	je	.L9
	movzbl	%r11b, %ebx
	cmpl	%esi, %ebx
	jbe	.L10
	movzbl	(%r15), %r13d
	movl	%esi, %ecx
	addq	$1, %r15
	addl	$8, %esi
	salq	%cl, %r13
	addq	%r13, %rax
.L10:
	movl	%r11d, %ecx
	movl	$-1, %r13d
	subl	%ebx, %esi
	sall	%cl, %r13d
	movl	%r13d, %ecx
	notl	%ecx
	andl	%eax, %ecx
	addl	%ecx, %edx
	movl	%r11d, %ecx
	shrq	%cl, %rax
.L9:
	cmpl	$14, %esi
	jbe	.L299
.L11:
	movq	-96(%rbp), %rcx
	andq	%rax, %rcx
	jmp	.L296
	.p2align 4,,10
	.p2align 3
.L285:
	andl	$64, %r11d
	jne	.L14
	movl	%r8d, %r11d
	sall	%cl, %r11d
	movl	%r11d, %ecx
	subl	$1, %ecx
	andq	%rax, %rcx
	addq	%rbx, %rcx
.L296:
	leaq	(%r12,%rcx,4), %rcx
	movzbl	(%rcx), %r11d
	movzwl	2(%rcx), %ebx
	movzbl	1(%rcx), %ecx
	shrq	%cl, %rax
	subl	%ecx, %esi
	movzbl	%r11b, %ecx
	testb	$16, %r11b
	je	.L285
	movl	%r11d, %r13d
	andl	$15, %r11d
	movl	%ebx, -120(%rbp)
	andl	$15, %r13d
	cmpl	%esi, %r11d
	jbe	.L15
	movzbl	(%r15), %ebx
	movl	%esi, %ecx
	salq	%cl, %rbx
	leal	8(%rsi), %ecx
	addq	%rbx, %rax
	cmpl	%ecx, %r11d
	ja	.L16
	addq	$1, %r15
	movl	%ecx, %esi
.L15:
	movl	%r13d, %ecx
	movl	$-1, %ebx
	subl	%r11d, %esi
	sall	%cl, %ebx
	movl	%ebx, %ecx
	movq	%rdi, %rbx
	subq	-104(%rbp), %rbx
	notl	%ecx
	andl	%eax, %ecx
	addl	-120(%rbp), %ecx
	movl	%ecx, -120(%rbp)
	movl	%r13d, %ecx
	shrq	%cl, %rax
	movl	-120(%rbp), %ecx
	cmpl	%ebx, %ecx
	jbe	.L17
	subl	%ebx, %ecx
	movl	%ecx, %r11d
	cmpl	%ecx, -116(%rbp)
	jnb	.L18
	movl	7144(%r10), %r13d
	testl	%r13d, %r13d
	jne	.L300
.L18:
	movl	-80(%rbp), %ecx
	subl	-120(%rbp), %ebx
	testl	%ecx, %ecx
	je	.L301
	cmpl	%r11d, -80(%rbp)
	jnb	.L27
	movl	-124(%rbp), %r13d
	addl	%ebx, %r13d
	movq	%r13, %rcx
	movq	%r13, -184(%rbp)
	movq	-112(%rbp), %r13
	addq	%r13, %rcx
	movl	%r11d, %r13d
	subl	-80(%rbp), %r13d
	movl	%r13d, -176(%rbp)
	cmpl	%r13d, %edx
	jbe	.L294
	addl	-80(%rbp), %edx
	movq	-112(%rbp), %r11
	addl	%ebx, %edx
	leal	-1(%r13), %ebx
	movl	%ebx, -192(%rbp)
	movq	-184(%rbp), %rbx
	leaq	16(%r11,%rbx), %r11
	cmpq	%r11, %rdi
	leaq	16(%rdi), %r11
	setnb	%bl
	cmpq	%r11, %rcx
	setnb	%r11b
	orb	%r11b, %bl
	je	.L28
	cmpl	$14, -192(%rbp)
	jbe	.L28
	movl	%r13d, %ebx
	xorl	%r11d, %r11d
	shrl	$4, %ebx
	salq	$4, %rbx
.L29:
	movdqu	(%rcx,%r11), %xmm1
	movups	%xmm1, (%rdi,%r11)
	addq	$16, %r11
	cmpq	%rbx, %r11
	jne	.L29
	movl	-176(%rbp), %ebx
	andl	$-16, %ebx
	movl	%ebx, %r11d
	leaq	(%rdi,%r11), %r13
	addq	%r11, %rcx
	cmpl	%ebx, -176(%rbp)
	je	.L32
	movzbl	(%rcx), %r11d
	movb	%r11b, 0(%r13)
	movl	-192(%rbp), %r11d
	subl	%ebx, %r11d
	je	.L32
	movzbl	1(%rcx), %ebx
	movb	%bl, 1(%r13)
	cmpl	$1, %r11d
	je	.L32
	movzbl	2(%rcx), %ebx
	movb	%bl, 2(%r13)
	cmpl	$2, %r11d
	je	.L32
	movzbl	3(%rcx), %ebx
	movb	%bl, 3(%r13)
	cmpl	$3, %r11d
	je	.L32
	movzbl	4(%rcx), %ebx
	movb	%bl, 4(%r13)
	cmpl	$4, %r11d
	je	.L32
	movzbl	5(%rcx), %ebx
	movb	%bl, 5(%r13)
	cmpl	$5, %r11d
	je	.L32
	movzbl	6(%rcx), %ebx
	movb	%bl, 6(%r13)
	cmpl	$6, %r11d
	je	.L32
	movzbl	7(%rcx), %ebx
	movb	%bl, 7(%r13)
	cmpl	$7, %r11d
	je	.L32
	movzbl	8(%rcx), %ebx
	movb	%bl, 8(%r13)
	cmpl	$8, %r11d
	je	.L32
	movzbl	9(%rcx), %ebx
	movb	%bl, 9(%r13)
	cmpl	$9, %r11d
	je	.L32
	movzbl	10(%rcx), %ebx
	movb	%bl, 10(%r13)
	cmpl	$10, %r11d
	je	.L32
	movzbl	11(%rcx), %ebx
	movb	%bl, 11(%r13)
	cmpl	$11, %r11d
	je	.L32
	movzbl	12(%rcx), %ebx
	movb	%bl, 12(%r13)
	cmpl	$12, %r11d
	je	.L32
	movzbl	13(%rcx), %ebx
	movb	%bl, 13(%r13)
	cmpl	$13, %r11d
	je	.L32
	movzbl	14(%rcx), %ecx
	movb	%cl, 14(%r13)
.L32:
	movl	-192(%rbp), %ecx
	leaq	1(%rdi,%rcx), %r11
	cmpl	%edx, -80(%rbp)
	jb	.L302
	movq	-112(%rbp), %rcx
	movq	%r11, %rdi
	jmp	.L294
	.p2align 4,,10
	.p2align 3
.L6:
	andl	$32, %r11d
	je	.L50
	movl	$16191, 8(%r10)
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L55:
	movl	-64(%rbp), %edx
	subl	%edi, %edx
	addl	$257, %edx
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L53:
	movl	-56(%rbp), %esi
	subl	%r15d, %esi
	movl	%esi, %edx
	addl	$5, %edx
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L299:
	movzbl	1(%r15), %r11d
	leal	8(%rsi), %ecx
	movzbl	(%r15), %ebx
	addq	$2, %r15
	salq	%cl, %r11
	movl	%esi, %ecx
	addl	$16, %esi
	salq	%cl, %rbx
	addq	%rbx, %r11
	addq	%r11, %rax
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L50:
	leaq	.LC2(%rip), %rbx
	movq	%rbx, 48(%r14)
	movl	$16209, 8(%r10)
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L14:
	leaq	.LC1(%rip), %rbx
	movq	%rbx, 48(%r14)
	movl	$16209, 8(%r10)
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L38:
	movzbl	(%rcx), %r11d
	addq	$3, %rcx
	addq	$3, %rdi
	subl	$3, %edx
	movb	%r11b, -3(%rdi)
	movzbl	-2(%rcx), %r11d
	movb	%r11b, -2(%rdi)
	movzbl	-1(%rcx), %r11d
	movb	%r11b, -1(%rdi)
.L294:
	cmpl	$2, %edx
	ja	.L38
	testl	%edx, %edx
	je	.L7
	movzbl	(%rcx), %r11d
	movb	%r11b, (%rdi)
	cmpl	$2, %edx
	je	.L45
	addq	$1, %rdi
	jmp	.L7
.L301:
	movl	-76(%rbp), %ecx
	leal	(%rcx,%rbx), %r13d
	movq	-112(%rbp), %rcx
	addq	%r13, %rcx
	cmpl	%r11d, %edx
	jbe	.L294
	addl	%ebx, %edx
	leal	-1(%r11), %ebx
	movl	%ebx, -176(%rbp)
	movq	-112(%rbp), %rbx
	leaq	16(%rbx,%r13), %rbx
	cmpq	%rbx, %rdi
	leaq	16(%rdi), %rbx
	setnb	%r13b
	cmpq	%rbx, %rcx
	setnb	%bl
	orb	%bl, %r13b
	je	.L22
	cmpl	$14, -176(%rbp)
	jbe	.L22
	leal	-16(%r11), %ebx
	movq	%rax, -192(%rbp)
	xorl	%r13d, %r13d
	shrl	$4, %ebx
	addl	$1, %ebx
	movl	%ebx, -184(%rbp)
	movl	-184(%rbp), %eax
	xorl	%ebx, %ebx
.L23:
	movdqu	(%rcx,%rbx), %xmm0
	addl	$1, %r13d
	movups	%xmm0, (%rdi,%rbx)
	addq	$16, %rbx
	cmpl	%r13d, %eax
	ja	.L23
	movl	-184(%rbp), %ebx
	movq	-192(%rbp), %rax
	sall	$4, %ebx
	movl	%ebx, -196(%rbp)
	leaq	(%rdi,%rbx), %r13
	addq	%rbx, %rcx
	movl	-176(%rbp), %ebx
	movl	%ebx, -192(%rbp)
	movq	%rbx, -184(%rbp)
	cmpl	-196(%rbp), %r11d
	je	.L26
	movzbl	(%rcx), %r11d
	movl	-192(%rbp), %ebx
	subl	-196(%rbp), %ebx
	movb	%r11b, 0(%r13)
	movl	%ebx, %r11d
	je	.L26
	movzbl	1(%rcx), %ebx
	movb	%bl, 1(%r13)
	cmpl	$1, %r11d
	je	.L26
	movzbl	2(%rcx), %ebx
	movb	%bl, 2(%r13)
	cmpl	$2, %r11d
	je	.L26
	movzbl	3(%rcx), %ebx
	movb	%bl, 3(%r13)
	cmpl	$3, %r11d
	je	.L26
	movzbl	4(%rcx), %ebx
	movb	%bl, 4(%r13)
	cmpl	$4, %r11d
	je	.L26
	movzbl	5(%rcx), %ebx
	movb	%bl, 5(%r13)
	cmpl	$5, %r11d
	je	.L26
	movzbl	6(%rcx), %ebx
	movb	%bl, 6(%r13)
	cmpl	$6, %r11d
	je	.L26
	movzbl	7(%rcx), %ebx
	movb	%bl, 7(%r13)
	cmpl	$7, %r11d
	je	.L26
	movzbl	8(%rcx), %ebx
	movb	%bl, 8(%r13)
	cmpl	$8, %r11d
	je	.L26
	movzbl	9(%rcx), %ebx
	movb	%bl, 9(%r13)
	cmpl	$9, %r11d
	je	.L26
	movzbl	10(%rcx), %ebx
	movb	%bl, 10(%r13)
	cmpl	$10, %r11d
	je	.L26
	movzbl	11(%rcx), %ebx
	movb	%bl, 11(%r13)
	cmpl	$11, %r11d
	je	.L26
	movzbl	12(%rcx), %ebx
	movb	%bl, 12(%r13)
	cmpl	$12, %r11d
	je	.L26
	movzbl	13(%rcx), %ebx
	movb	%bl, 13(%r13)
	cmpl	$13, %r11d
	je	.L26
	movzbl	14(%rcx), %ecx
	movb	%cl, 14(%r13)
.L26:
	movq	-184(%rbp), %rbx
	leaq	1(%rdi,%rbx), %rdi
	movl	-120(%rbp), %ebx
	movq	%rdi, %rcx
	subq	%rbx, %rcx
	jmp	.L294
.L17:
	movl	-120(%rbp), %ebx
	movq	%rdi, %rcx
	subq	%rbx, %rcx
	.p2align 4,,10
	.p2align 3
.L46:
	movzbl	(%rcx), %r11d
	movq	%rcx, %rbx
	addq	$3, %rcx
	subl	$3, %edx
	movb	%r11b, (%rdi)
	movzbl	-2(%rcx), %r11d
	movb	%r11b, 1(%rdi)
	movzbl	-1(%rcx), %r13d
	movq	%rdi, %r11
	addq	$3, %rdi
	movb	%r13b, -1(%rdi)
	cmpl	$2, %edx
	ja	.L46
	testl	%edx, %edx
	je	.L7
	movzbl	3(%rbx), %ecx
	leaq	4(%r11), %rdi
	movb	%cl, 3(%r11)
	cmpl	$2, %edx
	jne	.L7
	movzbl	4(%rbx), %edx
	leaq	5(%r11), %rdi
	movb	%dl, 4(%r11)
	jmp	.L7
.L16:
	movzbl	1(%r15), %ebx
	addl	$16, %esi
	addq	$2, %r15
	salq	%cl, %rbx
	addq	%rbx, %rax
	jmp	.L15
.L27:
	movl	-80(%rbp), %ecx
	leal	(%rcx,%rbx), %r13d
	movq	-112(%rbp), %rcx
	addq	%r13, %rcx
	cmpl	%r11d, %edx
	jbe	.L294
	addl	%ebx, %edx
	leal	-1(%r11), %ebx
	movl	%ebx, -184(%rbp)
	movq	-112(%rbp), %rbx
	leaq	16(%rbx,%r13), %rbx
	cmpq	%rbx, %rdi
	leaq	16(%rdi), %rbx
	setnb	%r13b
	cmpq	%rbx, %rcx
	setnb	%bl
	orb	%bl, %r13b
	je	.L40
	cmpl	$14, -184(%rbp)
	jbe	.L40
	leal	-16(%r11), %r13d
	shrl	$4, %r13d
	leal	1(%r13), %ebx
	xorl	%r13d, %r13d
	movl	%ebx, -176(%rbp)
	xorl	%ebx, %ebx
.L41:
	movdqu	(%rcx,%rbx), %xmm2
	addl	$1, %r13d
	movups	%xmm2, (%rdi,%rbx)
	addq	$16, %rbx
	cmpl	%r13d, -176(%rbp)
	ja	.L41
	movl	-176(%rbp), %ebx
	sall	$4, %ebx
	movl	%ebx, -196(%rbp)
	leaq	(%rdi,%rbx), %r13
	addq	%rbx, %rcx
	movl	-184(%rbp), %ebx
	movl	%ebx, -192(%rbp)
	movq	%rbx, -176(%rbp)
	cmpl	-196(%rbp), %r11d
	je	.L44
	movzbl	(%rcx), %r11d
	movl	-192(%rbp), %ebx
	subl	-196(%rbp), %ebx
	movb	%r11b, 0(%r13)
	movl	%ebx, %r11d
	je	.L44
	movzbl	1(%rcx), %ebx
	movb	%bl, 1(%r13)
	cmpl	$1, %r11d
	je	.L44
	movzbl	2(%rcx), %ebx
	movb	%bl, 2(%r13)
	cmpl	$2, %r11d
	je	.L44
	movzbl	3(%rcx), %ebx
	movb	%bl, 3(%r13)
	cmpl	$3, %r11d
	je	.L44
	movzbl	4(%rcx), %ebx
	movb	%bl, 4(%r13)
	cmpl	$4, %r11d
	je	.L44
	movzbl	5(%rcx), %ebx
	movb	%bl, 5(%r13)
	cmpl	$5, %r11d
	je	.L44
	movzbl	6(%rcx), %ebx
	movb	%bl, 6(%r13)
	cmpl	$6, %r11d
	je	.L44
	movzbl	7(%rcx), %ebx
	movb	%bl, 7(%r13)
	cmpl	$7, %r11d
	je	.L44
	movzbl	8(%rcx), %ebx
	movb	%bl, 8(%r13)
	cmpl	$8, %r11d
	je	.L44
	movzbl	9(%rcx), %ebx
	movb	%bl, 9(%r13)
	cmpl	$9, %r11d
	je	.L44
	movzbl	10(%rcx), %ebx
	movb	%bl, 10(%r13)
	cmpl	$10, %r11d
	je	.L44
	movzbl	11(%rcx), %ebx
	movb	%bl, 11(%r13)
	cmpl	$11, %r11d
	je	.L44
	movzbl	12(%rcx), %ebx
	movb	%bl, 12(%r13)
	cmpl	$12, %r11d
	je	.L44
	movzbl	13(%rcx), %ebx
	movb	%bl, 13(%r13)
	cmpl	$13, %r11d
	je	.L44
	movzbl	14(%rcx), %ecx
	movb	%cl, 14(%r13)
.L44:
	movq	-176(%rbp), %rbx
	leaq	1(%rdi,%rbx), %rdi
	movl	-120(%rbp), %ebx
	movq	%rdi, %rcx
	subq	%rbx, %rcx
	jmp	.L294
.L300:
	leaq	.LC0(%rip), %rbx
	movq	%rbx, 48(%r14)
	movl	$16209, 8(%r10)
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L45:
	movzbl	1(%rcx), %edx
	addq	$2, %rdi
	movb	%dl, -1(%rdi)
	jmp	.L7
.L22:
	movl	-176(%rbp), %ebx
	xorl	%r11d, %r11d
.L25:
	movzbl	(%rcx,%r11), %r13d
	movb	%r13b, (%rdi,%r11)
	movq	%r11, %r13
	addq	$1, %r11
	cmpq	%rbx, %r13
	jne	.L25
	movq	%rbx, -184(%rbp)
	jmp	.L26
.L28:
	movl	-176(%rbp), %ebx
	xorl	%r11d, %r11d
.L31:
	movzbl	(%rcx,%r11), %r13d
	movb	%r13b, (%rdi,%r11)
	addq	$1, %r11
	cmpq	%rbx, %r11
	jne	.L31
	jmp	.L32
.L40:
	movl	-184(%rbp), %ebx
	xorl	%r11d, %r11d
.L43:
	movzbl	(%rcx,%r11), %r13d
	movb	%r13b, (%rdi,%r11)
	movq	%r11, %r13
	addq	$1, %r11
	cmpq	%rbx, %r13
	jne	.L43
	movq	%rbx, -176(%rbp)
	jmp	.L44
.L302:
	movq	-112(%rbp), %rbx
	leaq	17(%rdi,%rcx), %rcx
	subl	-80(%rbp), %edx
	cmpq	%rcx, %rbx
	setnb	%dil
	cmpq	-168(%rbp), %r11
	setnb	%cl
	orb	%cl, %dil
	movl	$0, %ecx
	je	.L33
	cmpl	$14, -140(%rbp)
	jbe	.L33
	movq	%rbx, %rdi
.L34:
	movdqu	(%rdi,%rcx), %xmm3
	movups	%xmm3, (%r11,%rcx)
	addq	$16, %rcx
	cmpq	-160(%rbp), %rcx
	jne	.L34
	movq	-88(%rbp), %rdi
	movl	-144(%rbp), %ebx
	leaq	(%r11,%rdi), %rcx
	cmpl	%ebx, -80(%rbp)
	je	.L37
	movq	-152(%rbp), %rbx
	movl	-128(%rbp), %r13d
	movzbl	(%rbx), %edi
	movb	%dil, (%rcx)
	testl	%r13d, %r13d
	je	.L37
	movzbl	1(%rbx), %edi
	movb	%dil, 1(%rcx)
	cmpl	$1, %r13d
	je	.L37
	movzbl	2(%rbx), %edi
	movb	%dil, 2(%rcx)
	cmpl	$2, %r13d
	je	.L37
	movzbl	3(%rbx), %edi
	movb	%dil, 3(%rcx)
	cmpl	$3, %r13d
	je	.L37
	movzbl	4(%rbx), %edi
	movb	%dil, 4(%rcx)
	cmpl	$4, %r13d
	je	.L37
	movzbl	5(%rbx), %edi
	movb	%dil, 5(%rcx)
	cmpl	$5, %r13d
	je	.L37
	movzbl	6(%rbx), %edi
	movb	%dil, 6(%rcx)
	cmpl	$6, %r13d
	je	.L37
	movzbl	7(%rbx), %edi
	movb	%dil, 7(%rcx)
	cmpl	$7, %r13d
	je	.L37
	movzbl	8(%rbx), %edi
	movb	%dil, 8(%rcx)
	cmpl	$8, %r13d
	je	.L37
	movzbl	9(%rbx), %edi
	movb	%dil, 9(%rcx)
	cmpl	$9, %r13d
	je	.L37
	movzbl	10(%rbx), %edi
	movb	%dil, 10(%rcx)
	cmpl	$10, %r13d
	je	.L37
	movzbl	11(%rbx), %edi
	movb	%dil, 11(%rcx)
	cmpl	$11, %r13d
	je	.L37
	movzbl	12(%rbx), %edi
	movb	%dil, 12(%rcx)
	cmpl	$12, %r13d
	je	.L37
	movzbl	13(%rbx), %edi
	movb	%dil, 13(%rcx)
	cmpl	$13, %r13d
	je	.L37
	movzbl	14(%rbx), %edi
	movb	%dil, 14(%rcx)
.L37:
	movq	-136(%rbp), %rdi
	movl	-120(%rbp), %ebx
	leaq	1(%r11,%rdi), %rdi
	movq	%rdi, %rcx
	subq	%rbx, %rcx
	jmp	.L294
.L33:
	movq	-112(%rbp), %rbx
.L36:
	movzbl	(%rbx,%rcx), %edi
	movb	%dil, (%r11,%rcx)
	movq	%rcx, %rdi
	addq	$1, %rcx
	cmpq	-136(%rbp), %rdi
	jne	.L36
	jmp	.L37
	.cfi_endproc
.LFE38:
	.size	inflate_fast, .-inflate_fast
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
