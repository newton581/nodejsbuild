	.file	"infback.c"
	.text
	.p2align 4
	.globl	inflateBackInit_
	.type	inflateBackInit_, @function
inflateBackInit_:
.LFB38:
	.cfi_startproc
	endbr64
	testq	%rcx, %rcx
	je	.L7
	cmpb	$49, (%rcx)
	jne	.L7
	cmpl	$112, %r8d
	jne	.L7
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	testq	%rdi, %rdi
	je	.L9
	movq	%rdx, %r13
	testq	%rdx, %rdx
	je	.L9
	leal	-8(%rsi), %eax
	movl	%esi, %r12d
	cmpl	$7, %eax
	ja	.L9
	movq	64(%rdi), %rax
	movq	$0, 48(%rdi)
	testq	%rax, %rax
	je	.L3
	cmpq	$0, 72(%rbx)
	movq	80(%rdi), %rdi
	je	.L16
.L5:
	movl	$7160, %edx
	movl	$1, %esi
	call	*%rax
	testq	%rax, %rax
	je	.L10
	movl	$1, %edx
	movl	%r12d, %ecx
	movq	%rax, 56(%rbx)
	sall	%cl, %edx
	movl	$32768, 28(%rax)
	movl	%r12d, 56(%rax)
	movl	%edx, 60(%rax)
	movq	%r13, 72(%rax)
	movq	$0, 64(%rax)
	xorl	%eax, %eax
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	.cfi_restore_state
	leaq	zcalloc(%rip), %rax
	movq	$0, 80(%rdi)
	movq	%rax, 64(%rdi)
	xorl	%edi, %edi
	cmpq	$0, 72(%rbx)
	jne	.L5
.L16:
	leaq	zcfree(%rip), %rsi
	movq	%rsi, 72(%rbx)
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L7:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	movl	$-6, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movl	$-2, %eax
	jmp	.L1
.L10:
	movl	$-4, %eax
	jmp	.L1
	.cfi_endproc
.LFE38:
	.size	inflateBackInit_, .-inflateBackInit_
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"invalid block type"
.LC1:
	.string	"invalid stored block lengths"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"too many length or distance symbols"
	.section	.rodata.str1.1
.LC3:
	.string	"invalid code lengths set"
.LC4:
	.string	"invalid bit length repeat"
	.section	.rodata.str1.8
	.align 8
.LC5:
	.string	"invalid code -- missing end-of-block"
	.section	.rodata.str1.1
.LC6:
	.string	"invalid literal/lengths set"
.LC7:
	.string	"invalid distances set"
.LC8:
	.string	"invalid literal/length code"
.LC9:
	.string	"invalid distance code"
.LC10:
	.string	"invalid distance too far back"
	.text
	.p2align 4
	.globl	inflateBack
	.type	inflateBack, @function
inflateBack:
.LFB40:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -72(%rbp)
	movq	%rcx, -104(%rbp)
	movq	%r8, -112(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L138
	movq	56(%rdi), %r14
	movq	%rdi, %r15
	testq	%r14, %r14
	je	.L138
	movq	$0, 48(%rdi)
	movq	%rdx, %rbx
	xorl	%r10d, %r10d
	movq	$16191, 8(%r14)
	movq	(%rdi), %rax
	movl	$0, 64(%r14)
	movq	%rax, -64(%rbp)
	testq	%rax, %rax
	je	.L19
	movl	8(%rdi), %r10d
.L19:
	movq	72(%r14), %rax
	movq	%r15, -80(%rbp)
	xorl	%r12d, %r12d
	xorl	%r13d, %r13d
	leaq	distfix.4103(%rip), %rdx
	leaq	lenfix.4102(%rip), %rcx
	movl	%r10d, %r15d
	movq	%rax, -96(%rbp)
	movq	%rcx, %xmm0
	movl	60(%r14), %eax
	movq	%rdx, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movl	%eax, -88(%rbp)
	movl	$16191, %eax
	movaps	%xmm0, -128(%rbp)
.L20:
	subl	$16191, %eax
	cmpl	$18, %eax
	ja	.L21
	leaq	.L23(%rip), %rdi
	movslq	(%rdi,%rax,4), %rax
	addq	%rdi, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L23:
	.long	.L28-.L23
	.long	.L21-.L23
	.long	.L27-.L23
	.long	.L21-.L23
	.long	.L21-.L23
	.long	.L26-.L23
	.long	.L21-.L23
	.long	.L21-.L23
	.long	.L21-.L23
	.long	.L25-.L23
	.long	.L21-.L23
	.long	.L21-.L23
	.long	.L21-.L23
	.long	.L21-.L23
	.long	.L21-.L23
	.long	.L21-.L23
	.long	.L21-.L23
	.long	.L276-.L23
	.long	.L290-.L23
	.text
	.p2align 4,,10
	.p2align 3
.L21:
	movl	%r15d, %r10d
	movq	-64(%rbp), %rdx
	movq	-80(%rbp), %r15
	movl	$-2, %eax
.L29:
	movq	%rdx, (%r15)
	movl	%r10d, 8(%r15)
.L17:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L303
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L86:
	.cfi_restore_state
	movl	120(%rbx), %eax
	movl	$-1, %edx
	movq	104(%rbx), %rsi
	movl	%r14d, %edi
	movl	%eax, %ecx
	sall	%cl, %edx
	notl	%edx
	andl	%r14d, %edx
	leaq	(%rsi,%rdx,4), %r8
	movzbl	1(%r8), %edx
	movl	%edx, %ecx
	cmpl	%edx, %r12d
	jnb	.L89
	.p2align 4,,10
	.p2align 3
.L91:
	testl	%r15d, %r15d
	jne	.L90
	movq	-136(%rbp), %rsi
	movq	-72(%rbp), %rax
	movq	%r13, %rdi
	call	*%rax
	movl	%eax, %r15d
	testl	%eax, %eax
	je	.L297
	movq	104(%rbx), %rsi
	movl	120(%rbx), %eax
.L90:
	movq	-64(%rbp), %rdx
	subl	$1, %r15d
	leaq	1(%rdx), %rcx
	movq	%rcx, -64(%rbp)
	movzbl	(%rdx), %edx
	movl	%r12d, %ecx
	addl	$8, %r12d
	salq	%cl, %rdx
	movl	%eax, %ecx
	addq	%rdx, %r14
	movl	$-1, %edx
	sall	%cl, %edx
	movl	%r14d, %edi
	notl	%edx
	andl	%r14d, %edx
	leaq	(%rsi,%rdx,4), %r8
	movzbl	1(%r8), %edx
	movl	%edx, %ecx
	cmpl	%r12d, %edx
	ja	.L91
.L89:
	movzbl	(%r8), %eax
	movzwl	2(%r8), %r8d
	testb	%al, %al
	je	.L93
	testb	$-16, %al
	je	.L304
	movq	%r13, %rsi
	movq	%r14, %r13
	movq	%rbx, %r14
	movl	%edx, %ecx
	movl	%r8d, 92(%r14)
	movq	%rsi, %rbx
	shrq	%cl, %r13
	subl	%edx, %r12d
.L99:
	testb	$32, %al
	je	.L102
	movl	$16191, 8(%r14)
	.p2align 4,,10
	.p2align 3
.L28:
	movl	12(%r14), %r8d
	testl	%r8d, %r8d
	jne	.L32
	cmpl	$2, %r12d
	ja	.L34
.L33:
	testl	%r15d, %r15d
	jne	.L35
	movq	-72(%rbp), %rax
	leaq	-64(%rbp), %rsi
	movq	%rbx, %rdi
	call	*%rax
	movl	%eax, %r15d
	testl	%eax, %eax
	je	.L297
.L35:
	movq	-64(%rbp), %rax
	movl	%r12d, %ecx
	subl	$1, %r15d
	addl	$8, %r12d
	leaq	1(%rax), %rdx
	movq	%rdx, -64(%rbp)
	movzbl	(%rax), %eax
	salq	%cl, %rax
	addq	%rax, %r13
.L34:
	movl	%r13d, %eax
	andl	$1, %eax
	movl	%eax, 12(%r14)
	movq	%r13, %rax
	shrq	%rax
	andl	$3, %eax
	cmpl	$2, %eax
	jne	.L305
	movl	$16196, 8(%r14)
	shrq	$3, %r13
	subl	$3, %r12d
	.p2align 4,,10
	.p2align 3
.L26:
	leaq	-64(%rbp), %rax
	movq	%rax, %rdi
	cmpl	$13, %r12d
	ja	.L31
	movq	%rbx, %rax
	movq	%r14, -136(%rbp)
	movl	%r12d, %ebx
	movq	%r13, %r14
	movq	%rax, %r12
	movq	%rdi, %r13
	.p2align 4,,10
	.p2align 3
.L30:
	testl	%r15d, %r15d
	jne	.L49
	movq	-72(%rbp), %rax
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	*%rax
	movl	%eax, %r15d
	testl	%eax, %eax
	je	.L297
.L49:
	movq	-64(%rbp), %rax
	movl	%ebx, %ecx
	addl	$8, %ebx
	subl	$1, %r15d
	leaq	1(%rax), %rsi
	movq	%rsi, -64(%rbp)
	movzbl	(%rax), %eax
	salq	%cl, %rax
	addq	%rax, %r14
	cmpl	$13, %ebx
	jbe	.L30
	movq	%r12, %rax
	movq	%r14, %r13
	movq	-136(%rbp), %r14
	movl	%ebx, %r12d
	movq	%rax, %rbx
.L31:
	movq	%r13, %rax
	movq	%r13, %rdx
	movl	%r13d, %ecx
	subl	$14, %r12d
	shrq	$5, %rax
	shrq	$10, %rdx
	andl	$31, %ecx
	andl	$31, %eax
	andl	$15, %edx
	addl	$257, %ecx
	shrq	$14, %r13
	addl	$1, %eax
	addl	$4, %edx
	movl	%ecx, 132(%r14)
	movl	%eax, 136(%r14)
	movl	%edx, 128(%r14)
	cmpl	$30, %eax
	ja	.L141
	cmpl	$286, %ecx
	jbe	.L50
.L141:
	movl	%r15d, %r10d
	movq	-80(%rbp), %r15
	leaq	.LC2(%rip), %rax
	movq	%rax, 48(%r15)
	movl	$16209, 8(%r14)
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L305:
	cmpl	$3, %eax
	je	.L37
	cmpl	$1, %eax
	je	.L38
	movl	$16193, 8(%r14)
	shrq	$3, %r13
	subl	$3, %r12d
	.p2align 4,,10
	.p2align 3
.L27:
	movl	%r12d, %ecx
	andl	$-8, %r12d
	andl	$7, %ecx
	shrq	%cl, %r13
	cmpl	$31, %r12d
	ja	.L40
	leaq	-64(%rbp), %rax
	movq	%r14, -136(%rbp)
	movq	%r13, %r14
	movq	%rax, %r13
	.p2align 4,,10
	.p2align 3
.L43:
	testl	%r15d, %r15d
	jne	.L41
	movq	-72(%rbp), %rax
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	*%rax
	movl	%eax, %r15d
	testl	%eax, %eax
	je	.L297
.L41:
	movq	-64(%rbp), %rax
	movl	%r12d, %ecx
	addl	$8, %r12d
	subl	$1, %r15d
	leaq	1(%rax), %rsi
	movq	%rsi, -64(%rbp)
	movzbl	(%rax), %eax
	salq	%cl, %rax
	addq	%rax, %r14
	cmpl	$31, %r12d
	jbe	.L43
	movq	%r14, %r13
	movq	-136(%rbp), %r14
.L40:
	movq	%r13, %rax
	movzwl	%r13w, %edx
	shrq	$16, %rax
	xorq	$65535, %rax
	cmpq	%rax, %rdx
	je	.L44
	movl	%r15d, %r10d
	movq	-80(%rbp), %r15
	leaq	.LC1(%rip), %rax
	movq	%rax, 48(%r15)
	movl	$16209, 8(%r14)
.L22:
	movq	-64(%rbp), %rdx
	movl	$-3, %eax
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L297:
	movq	-80(%rbp), %r15
	xorl	%edx, %edx
	movl	$-5, %eax
	xorl	%r10d, %r10d
	jmp	.L29
.L44:
	movzwl	%r13w, %r13d
	movl	%r13d, 92(%r14)
	testl	%r13d, %r13d
	je	.L45
	leaq	-64(%rbp), %rax
	movq	%rbx, -136(%rbp)
	movl	%r13d, %r12d
	movq	%r14, %rbx
	movq	%rax, -144(%rbp)
	movl	%r15d, %r14d
	movl	-88(%rbp), %r13d
	movq	-96(%rbp), %r15
	.p2align 4,,10
	.p2align 3
.L48:
	testl	%r14d, %r14d
	jne	.L46
	movq	-144(%rbp), %rsi
	movq	-136(%rbp), %rdi
	movq	-72(%rbp), %rax
	call	*%rax
	movl	%eax, %r14d
	testl	%eax, %eax
	je	.L297
.L46:
	testl	%r13d, %r13d
	jne	.L47
	movl	60(%rbx), %r13d
	movq	72(%rbx), %r15
	movq	-112(%rbp), %rdi
	movq	-104(%rbp), %rax
	movl	%r13d, 64(%rbx)
	movl	%r13d, %edx
	movq	%r15, %rsi
	call	*%rax
	testl	%eax, %eax
	jne	.L306
.L47:
	cmpl	%r13d, %r14d
	movl	%r13d, %eax
	movq	-64(%rbp), %rsi
	movq	%r15, %rdi
	cmovbe	%r14d, %eax
	cmpl	%r12d, %eax
	cmovbe	%eax, %r12d
	movl	%r12d, %edx
	subl	%r12d, %r14d
	subl	%r12d, %r13d
	movq	%rdx, -88(%rbp)
	call	memcpy@PLT
	movq	-88(%rbp), %rdx
	movl	92(%rbx), %eax
	addq	%rdx, -64(%rbp)
	addq	%rdx, %r15
	subl	%r12d, %eax
	movl	%eax, 92(%rbx)
	movl	%eax, %r12d
	jne	.L48
	movl	%r13d, -88(%rbp)
	movq	%r15, -96(%rbp)
	movl	%r14d, %r15d
	movq	%rbx, %r14
	movq	-136(%rbp), %rbx
.L45:
	movl	12(%r14), %edi
	movl	$16191, 8(%r14)
	xorl	%r13d, %r13d
	xorl	%r12d, %r12d
	testl	%edi, %edi
	je	.L33
	.p2align 4,,10
	.p2align 3
.L32:
	movl	$16208, 8(%r14)
	movl	%r15d, %r10d
	movq	-80(%rbp), %r15
.L24:
	movl	60(%r14), %edx
	cmpl	-88(%rbp), %edx
	ja	.L133
	movq	-64(%rbp), %rdx
	movl	$1, %eax
	jmp	.L29
.L38:
	movdqa	-128(%rbp), %xmm3
	movl	$16200, 8(%r14)
	shrq	$3, %r13
	subl	$3, %r12d
	movabsq	$21474836489, %rax
	movq	%rax, 120(%r14)
	movups	%xmm3, 104(%r14)
.L25:
	leaq	-64(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	%rbx, %rax
	movq	%r14, %rbx
	movq	%r13, %r14
	movq	%rax, %r13
.L39:
	cmpl	$5, %r15d
	jbe	.L86
	movl	-88(%rbp), %ecx
	cmpl	$257, %ecx
	jbe	.L86
	movq	%r13, %rsi
	movq	-80(%rbp), %rdi
	movq	%r14, %r13
	movq	-64(%rbp), %rax
	movq	%rbx, %r14
	movq	%rsi, %rbx
	movq	-96(%rbp), %rsi
	movl	%ecx, 32(%rdi)
	movq	%rsi, 24(%rdi)
	movl	60(%r14), %esi
	movq	%rax, (%rdi)
	movl	%r15d, 8(%rdi)
	movq	%r13, 80(%r14)
	movl	%r12d, 88(%r14)
	cmpl	%esi, 64(%r14)
	jnb	.L87
	movl	%esi, %eax
	subl	%ecx, %eax
	movl	%eax, 64(%r14)
.L87:
	movq	-80(%rbp), %r15
	movq	%r15, %rdi
	call	inflate_fast
	movq	24(%r15), %rdi
	movq	(%r15), %rax
	movq	80(%r14), %r13
	movl	88(%r14), %r12d
	movq	%rdi, -96(%rbp)
	movl	32(%r15), %edi
	movq	%rax, -64(%rbp)
	movl	8(%r15), %r15d
	movl	%edi, -88(%rbp)
	movl	8(%r14), %eax
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L133:
	subl	-88(%rbp), %edx
	movq	72(%r14), %rsi
	movl	%r10d, -72(%rbp)
	movq	-112(%rbp), %rdi
	movq	-104(%rbp), %rax
	call	*%rax
	movq	-64(%rbp), %rdx
	movl	-72(%rbp), %r10d
	testl	%eax, %eax
	jne	.L134
	movl	$1, %eax
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L134:
	movl	$-5, %eax
	jmp	.L29
.L276:
	movl	%r15d, %r10d
	movq	-80(%rbp), %r15
	jmp	.L24
.L290:
	movl	%r15d, %r10d
	movq	-80(%rbp), %r15
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L50:
	movl	$0, 140(%r14)
	movl	%r15d, %eax
	xorl	%esi, %esi
	movq	-72(%rbp), %r15
	cmpl	$2, %r12d
	ja	.L56
.L55:
	testl	%eax, %eax
	jne	.L53
	leaq	-64(%rbp), %rsi
	movq	%rbx, %rdi
	call	*%r15
	testl	%eax, %eax
	je	.L297
	movl	140(%r14), %esi
	movl	128(%r14), %edx
.L53:
	movq	-64(%rbp), %rdi
	movl	%r12d, %ecx
	subl	$1, %eax
	addl	$8, %r12d
	leaq	1(%rdi), %r8
	movq	%r8, -64(%rbp)
	movzbl	(%rdi), %edi
	salq	%cl, %rdi
	addq	%rdi, %r13
.L56:
	leaq	order.4124(%rip), %rdi
	leal	1(%rsi), %ecx
	subl	$3, %r12d
	movzwl	(%rdi,%rsi,2), %esi
	movl	%r13d, %edi
	movl	%ecx, 140(%r14)
	shrq	$3, %r13
	andl	$7, %edi
	movw	%di, 152(%r14,%rsi,2)
	cmpl	%edx, %ecx
	jnb	.L54
	movl	%ecx, %esi
	cmpl	$2, %r12d
	jbe	.L55
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L93:
	movl	%r8d, 92(%rbx)
	shrq	%cl, %r14
	subl	%edx, %r12d
.L135:
	movl	-88(%rbp), %edx
	testl	%edx, %edx
	jne	.L100
	movl	60(%rbx), %eax
	movq	72(%rbx), %rsi
	movq	-112(%rbp), %rdi
	movl	%eax, -88(%rbp)
	movl	%eax, %edx
	movl	%eax, 64(%rbx)
	movq	-104(%rbp), %rax
	movq	%rsi, -96(%rbp)
	call	*%rax
	testl	%eax, %eax
	jne	.L302
	movl	92(%rbx), %r8d
.L100:
	movq	-96(%rbp), %rax
	subl	$1, -88(%rbp)
	movb	%r8b, (%rax)
	addq	$1, %rax
	movl	$16200, 8(%rbx)
	movq	%rax, -96(%rbp)
	jmp	.L39
.L37:
	movl	%r15d, %r10d
	movq	-80(%rbp), %r15
	leaq	.LC0(%rip), %rax
	movq	%rax, 48(%r15)
	movl	$16209, 8(%r14)
	jmp	.L22
.L102:
	testb	$64, %al
	je	.L103
	movl	%r15d, %r10d
	movq	-80(%rbp), %r15
	leaq	.LC8(%rip), %rax
	movq	%rax, 48(%r15)
	movl	$16209, 8(%r14)
	jmp	.L22
.L138:
	movl	$-2, %eax
	jmp	.L17
.L306:
	movq	-80(%rbp), %r15
	movq	-64(%rbp), %rdx
	movl	%r14d, %r10d
	movl	$-5, %eax
	jmp	.L29
.L304:
	leal	(%rax,%rdx), %ecx
	movl	$-1, %eax
	sall	%cl, %eax
	movl	%edx, %ecx
	notl	%eax
	andl	%eax, %edi
	movl	%eax, -144(%rbp)
	shrl	%cl, %edi
	leal	(%rdi,%r8), %eax
	leaq	(%rsi,%rax,4), %rcx
	movzbl	1(%rcx), %edi
	leal	(%rdi,%rdx), %eax
	cmpl	%r12d, %eax
	jbe	.L95
	movq	%r13, %rdi
	movq	%rbx, -152(%rbp)
	movl	%r15d, %eax
	movq	%r14, %r13
	movl	%r8d, %ebx
	movl	%edx, %r15d
	movq	%rdi, %r14
	.p2align 4,,10
	.p2align 3
.L97:
	testl	%eax, %eax
	jne	.L96
	movq	-136(%rbp), %rsi
	movq	-72(%rbp), %rax
	movq	%r14, %rdi
	call	*%rax
	testl	%eax, %eax
	je	.L297
	movq	-152(%rbp), %rsi
	movq	104(%rsi), %rsi
.L96:
	movq	-64(%rbp), %rcx
	subl	$1, %eax
	leaq	1(%rcx), %rdi
	movq	%rdi, -64(%rbp)
	movzbl	(%rcx), %edi
	movl	%r12d, %ecx
	addl	$8, %r12d
	salq	%cl, %rdi
	movl	%r15d, %ecx
	addq	%rdi, %r13
	movl	-144(%rbp), %edi
	andl	%r13d, %edi
	shrl	%cl, %edi
	leal	(%rdi,%rbx), %ecx
	leaq	(%rsi,%rcx,4), %rcx
	movzbl	1(%rcx), %edi
	leal	(%rdi,%r15), %r11d
	cmpl	%r12d, %r11d
	ja	.L97
	movq	%r14, %rsi
	movl	%r15d, %edx
	movzwl	2(%rcx), %r8d
	movl	%eax, %r15d
	movq	-152(%rbp), %rbx
	movzbl	(%rcx), %eax
	movq	%r13, %r14
	movq	%rsi, %r13
.L98:
	movl	%edx, %ecx
	subl	%edx, %r12d
	movl	%r8d, 92(%rbx)
	shrq	%cl, %r14
	movl	%edi, %ecx
	subl	%edi, %r12d
	shrq	%cl, %r14
	testb	%al, %al
	je	.L135
	movq	%r13, %rdi
	movq	%r14, %r13
	movq	%rbx, %r14
	movq	%rdi, %rbx
	jmp	.L99
.L103:
	movl	%eax, %edx
	andl	$15, %edx
	movl	%edx, 100(%r14)
	testb	$15, %al
	je	.L104
	cmpl	%r12d, %edx
	jbe	.L105
	leaq	-64(%rbp), %rax
	movq	%r14, -136(%rbp)
	movq	%rbx, %r14
	movq	%r13, %rbx
	movl	%r12d, %r13d
	movq	%rax, %r12
	.p2align 4,,10
	.p2align 3
.L107:
	testl	%r15d, %r15d
	jne	.L106
	movq	-72(%rbp), %rax
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	*%rax
	movl	%eax, %r15d
	testl	%eax, %eax
	je	.L297
	movq	-136(%rbp), %rax
	movl	100(%rax), %edx
.L106:
	movq	-64(%rbp), %rax
	subl	$1, %r15d
	leaq	1(%rax), %rcx
	movq	%rcx, -64(%rbp)
	movzbl	(%rax), %eax
	movl	%r13d, %ecx
	addl	$8, %r13d
	salq	%cl, %rax
	addq	%rax, %rbx
	cmpl	%edx, %r13d
	jb	.L107
	movl	%r13d, %r12d
	movq	%rbx, %r13
	movq	%r14, %rbx
	movq	-136(%rbp), %r14
.L105:
	movl	%edx, %ecx
	movl	$-1, %eax
	subl	%edx, %r12d
	sall	%cl, %eax
	notl	%eax
	andl	%r13d, %eax
	addl	%eax, 92(%r14)
	shrq	%cl, %r13
.L104:
	movl	124(%r14), %esi
	movl	$-1, %edx
	movq	112(%r14), %rax
	movl	%esi, %ecx
	sall	%cl, %edx
	notl	%edx
	andl	%r13d, %edx
	leaq	(%rax,%rdx,4), %rcx
	movzbl	1(%rcx), %edx
	movl	%edx, %r9d
	movl	%edx, %r8d
	cmpl	%r12d, %edx
	jbe	.L108
	movq	%rbx, %rcx
	leaq	-64(%rbp), %rdx
	movq	%r14, %rbx
	movq	%rax, %rdi
	movl	%r12d, %r14d
	movl	%r15d, %eax
	movq	%rcx, %r12
	movq	%r13, %r15
	movq	%rdx, %r13
	.p2align 4,,10
	.p2align 3
.L110:
	testl	%eax, %eax
	jne	.L109
	movq	-72(%rbp), %rax
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	*%rax
	testl	%eax, %eax
	je	.L297
	movq	112(%rbx), %rdi
	movl	124(%rbx), %esi
.L109:
	movq	-64(%rbp), %rdx
	subl	$1, %eax
	leaq	1(%rdx), %rcx
	movq	%rcx, -64(%rbp)
	movzbl	(%rdx), %edx
	movl	%r14d, %ecx
	addl	$8, %r14d
	salq	%cl, %rdx
	movl	%esi, %ecx
	addq	%rdx, %r15
	movl	$-1, %edx
	sall	%cl, %edx
	notl	%edx
	andl	%r15d, %edx
	leaq	(%rdi,%rdx,4), %rcx
	movzbl	1(%rcx), %edx
	movl	%edx, %r9d
	movl	%edx, %r8d
	cmpl	%r14d, %edx
	ja	.L110
	movq	%r12, %rsi
	movq	%r15, %r13
	movzwl	2(%rcx), %r10d
	movl	%eax, %r15d
	movq	%rdi, %rax
	movzbl	(%rcx), %edi
	movl	%r14d, %r12d
	movq	%rbx, %r14
	movq	%rsi, %rbx
.L111:
	movl	%edx, %ecx
	testb	$-16, %dil
	je	.L307
.L112:
	shrq	%cl, %r13
	subl	%r8d, %r12d
	testb	$64, %dil
	je	.L117
	movl	%r15d, %r10d
	movq	-80(%rbp), %r15
	leaq	.LC9(%rip), %rax
	movq	%rax, 48(%r15)
	movl	$16209, 8(%r14)
	jmp	.L22
.L54:
	movl	%eax, %r15d
	movl	%ecx, %eax
	cmpl	$18, %ecx
	ja	.L57
	movl	%ecx, %esi
	leaq	order.4124(%rip), %rdi
	movl	$18, %ecx
	leaq	(%rdi,%rsi,2), %rdx
	movl	%ecx, %edi
	leaq	2+order.4124(%rip), %rcx
	subl	%eax, %edi
	movq	%rdi, %rax
	addq	%rsi, %rax
	leaq	(%rcx,%rax,2), %rcx
.L58:
	movzwl	(%rdx), %eax
	xorl	%esi, %esi
	addq	$2, %rdx
	movw	%si, 152(%r14,%rax,2)
	cmpq	%rdx, %rcx
	jne	.L58
	movl	$19, 140(%r14)
.L57:
	leaq	1368(%r14), %rax
	leaq	120(%r14), %r8
	xorl	%edi, %edi
	movl	$19, %edx
	movq	%rax, 144(%r14)
	leaq	792(%r14), %r9
	leaq	144(%r14), %rcx
	movq	%rax, 104(%r14)
	movl	$7, 120(%r14)
	movq	%rax, -168(%rbp)
	leaq	152(%r14), %rax
	movq	%rax, %rsi
	movq	%r9, -144(%rbp)
	movq	%r8, -176(%rbp)
	movq	%rcx, -152(%rbp)
	movq	%rax, -160(%rbp)
	call	inflate_table
	testl	%eax, %eax
	je	.L59
	movl	%r15d, %r10d
	movq	-80(%rbp), %r15
	leaq	.LC3(%rip), %rax
	movq	%rax, 48(%r15)
	movl	$16209, 8(%r14)
	jmp	.L22
.L117:
	movl	%edi, %edx
	movzwl	%r10w, %esi
	andl	$15, %edx
	andl	$15, %edi
	movl	%esi, 96(%r14)
	movl	%edx, 100(%r14)
	jne	.L308
.L118:
	movl	60(%r14), %edx
	movl	$0, %ecx
	cmpl	64(%r14), %edx
	cmova	-88(%rbp), %ecx
	movl	%edx, %eax
	subl	%ecx, %eax
	cmpl	%esi, %eax
	jnb	.L298
	movl	%r15d, %r10d
	movq	-80(%rbp), %r15
	leaq	.LC10(%rip), %rax
	movq	%rax, 48(%r15)
	movl	$16209, 8(%r14)
	jmp	.L22
.L307:
	leal	(%rdi,%rdx), %ecx
	orl	$-1, %esi
	sall	%cl, %esi
	movl	%r9d, %ecx
	notl	%esi
	movl	%esi, %r8d
	andl	%r13d, %esi
	shrl	%cl, %esi
	leal	(%rsi,%r10), %ecx
	leaq	(%rax,%rcx,4), %rcx
	movzbl	1(%rcx), %esi
	leal	(%rsi,%rdx), %edi
	cmpl	%r12d, %edi
	jbe	.L113
	movq	%rbx, %rdi
	leaq	-64(%rbp), %r9
	movl	%edx, %ebx
	movq	%r14, %rdx
	movl	%r10d, %r14d
.L115:
	testl	%r15d, %r15d
	jne	.L114
	movl	%r8d, -160(%rbp)
	movq	-72(%rbp), %rax
	movq	%r9, %rsi
	movq	%rdx, -152(%rbp)
	movq	%r9, -144(%rbp)
	movq	%rdi, -136(%rbp)
	call	*%rax
	movl	%eax, %r15d
	testl	%eax, %eax
	je	.L297
	movq	-152(%rbp), %rdx
	movl	-160(%rbp), %r8d
	movq	-144(%rbp), %r9
	movq	-136(%rbp), %rdi
	movq	112(%rdx), %rax
.L114:
	movq	-64(%rbp), %rcx
	subl	$1, %r15d
	leaq	1(%rcx), %rsi
	movq	%rsi, -64(%rbp)
	movzbl	(%rcx), %esi
	movl	%r12d, %ecx
	addl	$8, %r12d
	salq	%cl, %rsi
	movl	%ebx, %ecx
	addq	%rsi, %r13
	movl	%r8d, %esi
	andl	%r13d, %esi
	shrl	%cl, %esi
	leal	(%rsi,%r14), %ecx
	leaq	(%rax,%rcx,4), %rcx
	movzbl	1(%rcx), %esi
	leal	(%rsi,%rbx), %r10d
	cmpl	%r12d, %r10d
	ja	.L115
	movq	%rdx, %r14
	movzwl	2(%rcx), %r10d
	movl	%ebx, %edx
	movq	%rdi, %rbx
	movzbl	(%rcx), %edi
.L116:
	movl	%edx, %ecx
	subl	%edx, %r12d
	movl	%esi, %r8d
	shrq	%cl, %r13
	movl	%esi, %ecx
	jmp	.L112
.L59:
	movl	132(%r14), %edx
	movl	$0, 140(%r14)
	movl	%edx, %eax
	addl	136(%r14), %eax
	je	.L61
	leaq	-64(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	%rbx, %rax
	movq	%r14, %rbx
	movq	%rax, %r14
.L60:
	movl	120(%rbx), %esi
	movl	$-1, %eax
	movq	104(%rbx), %rdx
	movl	%esi, %ecx
	sall	%cl, %eax
	notl	%eax
	andl	%r13d, %eax
	leaq	(%rdx,%rax,4), %rax
	movzbl	1(%rax), %r9d
	movl	%r9d, %ecx
	cmpl	%r12d, %r9d
	jbe	.L301
	movq	%r14, %rax
	movq	%r13, %r14
	movl	%r12d, %r13d
	movq	%rax, %r12
.L63:
	testl	%r15d, %r15d
	jne	.L62
	movq	-136(%rbp), %rsi
	movq	-72(%rbp), %rax
	movq	%r12, %rdi
	call	*%rax
	movl	%eax, %r15d
	testl	%eax, %eax
	je	.L297
	movq	104(%rbx), %rdx
	movl	120(%rbx), %esi
.L62:
	movq	-64(%rbp), %rax
	subl	$1, %r15d
	leaq	1(%rax), %rcx
	movq	%rcx, -64(%rbp)
	movzbl	(%rax), %eax
	movl	%r13d, %ecx
	addl	$8, %r13d
	salq	%cl, %rax
	movl	%esi, %ecx
	addq	%rax, %r14
	movl	$-1, %eax
	sall	%cl, %eax
	notl	%eax
	andl	%r14d, %eax
	leaq	(%rdx,%rax,4), %rax
	movzbl	1(%rax), %r9d
	movl	%r9d, %ecx
	cmpl	%r13d, %r9d
	ja	.L63
	movq	%r12, %rdi
	movl	%r13d, %r12d
	movq	%r14, %r13
	movq	%rdi, %r14
.L301:
	movzwl	2(%rax), %eax
	cmpw	$15, %ax
	ja	.L64
	movl	140(%rbx), %edx
	shrq	%cl, %r13
	subl	%r9d, %r12d
	leal	1(%rdx), %ecx
	movl	%ecx, 140(%rbx)
	movl	136(%rbx), %ecx
	movw	%ax, 152(%rbx,%rdx,2)
	movl	132(%rbx), %edx
.L65:
	addl	%edx, %ecx
	cmpl	%ecx, 140(%rbx)
	jb	.L60
	movq	%r14, %rax
	movq	%rbx, %r14
	movq	%rax, %rbx
.L61:
	cmpl	$16209, 8(%r14)
	je	.L290
	cmpw	$0, 664(%r14)
	jne	.L83
	movl	%r15d, %r10d
	movq	-80(%rbp), %r15
	leaq	.LC5(%rip), %rax
	movq	%rax, 48(%r15)
	movl	$16209, 8(%r14)
	jmp	.L22
.L64:
	cmpw	$16, %ax
	je	.L309
	cmpw	$17, %ax
	je	.L73
	leal	7(%r9), %edx
	cmpl	%r12d, %edx
	jbe	.L75
	movq	%r14, %rax
	movq	%rbx, %r14
	movl	%edx, %ebx
	movq	%rax, %rdx
.L74:
	testl	%r15d, %r15d
	jne	.L79
	movl	%r9d, -188(%rbp)
	movq	%rdx, %rdi
	movq	-72(%rbp), %rax
	movq	%rdx, -184(%rbp)
	movq	-136(%rbp), %rsi
	call	*%rax
	movq	-184(%rbp), %rdx
	movl	-188(%rbp), %r9d
	testl	%eax, %eax
	movl	%eax, %r15d
	je	.L297
.L79:
	movq	-64(%rbp), %rax
	subl	$1, %r15d
	leaq	1(%rax), %rcx
	movq	%rcx, -64(%rbp)
	movzbl	(%rax), %eax
	movl	%r12d, %ecx
	addl	$8, %r12d
	salq	%cl, %rax
	addq	%rax, %r13
	cmpl	%ebx, %r12d
	jb	.L74
	movq	%r14, %rbx
	movq	%rdx, %r14
.L75:
	movl	%r9d, %ecx
	subl	%r9d, %r12d
	movl	140(%rbx), %eax
	xorl	%esi, %esi
	shrq	%cl, %r13
	subl	$7, %r12d
	movl	%r13d, %edx
	shrq	$7, %r13
	andl	$127, %edx
	addl	$11, %edx
.L72:
	movl	136(%rbx), %ecx
	leal	(%rax,%rdx), %r9d
	movl	132(%rbx), %edx
	leal	(%rdx,%rcx), %r10d
	cmpl	%r10d, %r9d
	ja	.L300
.L81:
	movl	%eax, %edi
	addl	$1, %eax
	movw	%si, 152(%rbx,%rdi,2)
	cmpl	%eax, %r9d
	jne	.L81
	movl	%r9d, 140(%rbx)
	jmp	.L65
.L73:
	leal	3(%r9), %edx
	cmpl	%r12d, %edx
	jbe	.L76
	movq	%r14, %rax
	movq	%rbx, %r14
	movl	%edx, %ebx
	movq	%rax, %rdx
.L78:
	testl	%r15d, %r15d
	jne	.L77
	movl	%r9d, -188(%rbp)
	movq	%rdx, %rdi
	movq	-72(%rbp), %rax
	movq	%rdx, -184(%rbp)
	movq	-136(%rbp), %rsi
	call	*%rax
	movq	-184(%rbp), %rdx
	movl	-188(%rbp), %r9d
	testl	%eax, %eax
	movl	%eax, %r15d
	je	.L297
.L77:
	movq	-64(%rbp), %rax
	subl	$1, %r15d
	leaq	1(%rax), %rcx
	movq	%rcx, -64(%rbp)
	movzbl	(%rax), %eax
	movl	%r12d, %ecx
	addl	$8, %r12d
	salq	%cl, %rax
	addq	%rax, %r13
	cmpl	%ebx, %r12d
	jb	.L78
	movq	%r14, %rbx
	movq	%rdx, %r14
.L76:
	movl	%r9d, %ecx
	movl	$-3, %eax
	xorl	%esi, %esi
	shrq	%cl, %r13
	subl	%r9d, %eax
	movl	%r13d, %edx
	addl	%eax, %r12d
	shrq	$3, %r13
	movl	140(%rbx), %eax
	andl	$7, %edx
	addl	$3, %edx
	jmp	.L72
.L108:
	movzbl	(%rcx), %edi
	movzwl	2(%rcx), %r10d
	jmp	.L111
.L302:
	movl	%r15d, %r10d
	movq	-64(%rbp), %rdx
	movq	-80(%rbp), %r15
	movl	$-5, %eax
	jmp	.L29
.L95:
	movzbl	(%rcx), %eax
	movzwl	2(%rcx), %r8d
	jmp	.L98
.L303:
	call	__stack_chk_fail@PLT
.L298:
	movq	-96(%rbp), %r8
	movl	-88(%rbp), %edi
.L123:
	movl	%edx, %r9d
	testl	%edi, %edi
	jne	.L124
	movq	72(%r14), %r8
	movq	-112(%rbp), %rdi
	movl	%edx, -96(%rbp)
	movl	%edx, 64(%r14)
	movq	-104(%rbp), %rax
	movq	%r8, -88(%rbp)
	movq	%r8, %rsi
	call	*%rax
	movq	-88(%rbp), %r8
	movl	-96(%rbp), %r9d
	testl	%eax, %eax
	jne	.L302
	movl	%r9d, %edi
	movl	60(%r14), %r9d
.L124:
	movl	96(%r14), %edx
	movl	%r9d, %eax
	subl	%edx, %eax
	cmpl	%eax, %edi
	jbe	.L126
	leal	(%rdx,%rdi), %ecx
	leaq	(%r8,%rax), %rsi
	subl	%r9d, %ecx
.L127:
	movl	92(%r14), %eax
	cmpl	%ecx, %eax
	cmovbe	%eax, %ecx
	subl	%ecx, %eax
	subl	%ecx, %edi
	leal	-1(%rcx), %r9d
	movl	%eax, 92(%r14)
	leaq	15(%r8), %rax
	subq	%rsi, %rax
	cmpq	$30, %rax
	jbe	.L128
	cmpl	$14, %r9d
	jbe	.L128
	leal	-16(%rcx), %r10d
	xorl	%eax, %eax
	xorl	%edx, %edx
	shrl	$4, %r10d
	addl	$1, %r10d
.L129:
	movdqu	(%rsi,%rax), %xmm2
	addl	$1, %edx
	movups	%xmm2, (%r8,%rax)
	addq	$16, %rax
	cmpl	%r10d, %edx
	jb	.L129
	movl	%r10d, %edx
	movl	%r9d, %r10d
	sall	$4, %edx
	movl	%edx, %eax
	leaq	(%r8,%rax), %r11
	addq	%rsi, %rax
	cmpl	%edx, %ecx
	je	.L132
	movzbl	(%rax), %ecx
	movl	%r9d, %esi
	movb	%cl, (%r11)
	subl	%edx, %esi
	je	.L132
	movzbl	1(%rax), %ecx
	movb	%cl, 1(%r11)
	cmpl	$1, %esi
	je	.L132
	movzbl	2(%rax), %ecx
	movb	%cl, 2(%r11)
	cmpl	$2, %esi
	je	.L132
	movzbl	3(%rax), %ecx
	movb	%cl, 3(%r11)
	cmpl	$3, %esi
	je	.L132
	movzbl	4(%rax), %ecx
	movb	%cl, 4(%r11)
	cmpl	$4, %esi
	je	.L132
	movzbl	5(%rax), %ecx
	movb	%cl, 5(%r11)
	cmpl	$5, %esi
	je	.L132
	movzbl	6(%rax), %ecx
	movb	%cl, 6(%r11)
	cmpl	$6, %esi
	je	.L132
	movzbl	7(%rax), %ecx
	movb	%cl, 7(%r11)
	cmpl	$7, %esi
	je	.L132
	movzbl	8(%rax), %ecx
	movb	%cl, 8(%r11)
	cmpl	$8, %esi
	je	.L132
	movzbl	9(%rax), %ecx
	movb	%cl, 9(%r11)
	cmpl	$9, %esi
	je	.L132
	movzbl	10(%rax), %ecx
	movb	%cl, 10(%r11)
	cmpl	$10, %esi
	je	.L132
	movzbl	11(%rax), %ecx
	movb	%cl, 11(%r11)
	cmpl	$11, %esi
	je	.L132
	movzbl	12(%rax), %ecx
	movb	%cl, 12(%r11)
	cmpl	$12, %esi
	je	.L132
	movzbl	13(%rax), %ecx
	movb	%cl, 13(%r11)
	cmpl	$13, %esi
	je	.L132
	movzbl	14(%rax), %eax
	movb	%al, 14(%r11)
.L132:
	movl	92(%r14), %eax
	leaq	1(%r8,%r10), %r8
	testl	%eax, %eax
	je	.L275
	movl	60(%r14), %edx
	jmp	.L123
.L126:
	movq	%r8, %rsi
	movl	%edi, %ecx
	subq	%rdx, %rsi
	jmp	.L127
.L128:
	movl	%r9d, %r10d
	xorl	%eax, %eax
.L131:
	movzbl	(%rsi,%rax), %edx
	movb	%dl, (%r8,%rax)
	movq	%rax, %rdx
	addq	$1, %rax
	cmpq	%r10, %rdx
	jne	.L131
	jmp	.L132
.L308:
	cmpl	%r12d, %edx
	jbe	.L119
	leaq	-64(%rbp), %rsi
.L121:
	testl	%r15d, %r15d
	jne	.L120
	movq	%rsi, -136(%rbp)
	movq	-72(%rbp), %rax
	movq	%rbx, %rdi
	call	*%rax
	movl	%eax, %r15d
	testl	%eax, %eax
	je	.L297
	movl	100(%r14), %edx
	movq	-136(%rbp), %rsi
.L120:
	movq	-64(%rbp), %rax
	subl	$1, %r15d
	leaq	1(%rax), %rcx
	movq	%rcx, -64(%rbp)
	movzbl	(%rax), %eax
	movl	%r12d, %ecx
	addl	$8, %r12d
	salq	%cl, %rax
	addq	%rax, %r13
	cmpl	%edx, %r12d
	jb	.L121
.L119:
	movl	%edx, %ecx
	orl	$-1, %eax
	subl	%edx, %r12d
	sall	%cl, %eax
	notl	%eax
	andl	%r13d, %eax
	addl	96(%r14), %eax
	shrq	%cl, %r13
	movl	%eax, 96(%r14)
	movl	%eax, %esi
	jmp	.L118
.L275:
	movq	%r8, -96(%rbp)
	movl	8(%r14), %eax
	movl	%edi, -88(%rbp)
	jmp	.L20
.L300:
	movl	%r15d, %r10d
	movq	-80(%rbp), %r15
	leaq	.LC4(%rip), %rax
	movq	%rax, 48(%r15)
	movl	$16209, 8(%rbx)
	jmp	.L22
.L309:
	leal	2(%r9), %edx
	cmpl	%r12d, %edx
	jbe	.L67
	movq	%r14, %rax
	movq	%rbx, %r14
	movl	%edx, %ebx
	movq	%rax, %rdx
.L69:
	testl	%r15d, %r15d
	jne	.L68
	movl	%r9d, -188(%rbp)
	movq	%rdx, %rdi
	movq	-72(%rbp), %rax
	movq	%rdx, -184(%rbp)
	movq	-136(%rbp), %rsi
	call	*%rax
	movq	-184(%rbp), %rdx
	movl	-188(%rbp), %r9d
	testl	%eax, %eax
	movl	%eax, %r15d
	je	.L297
.L68:
	movq	-64(%rbp), %rax
	subl	$1, %r15d
	leaq	1(%rax), %rcx
	movq	%rcx, -64(%rbp)
	movzbl	(%rax), %eax
	movl	%r12d, %ecx
	addl	$8, %r12d
	salq	%cl, %rax
	addq	%rax, %r13
	cmpl	%ebx, %r12d
	jb	.L69
	movq	%r14, %rbx
	movq	%rdx, %r14
.L67:
	movl	140(%rbx), %eax
	movl	%r9d, %ecx
	subl	%r9d, %r12d
	shrq	%cl, %r13
	testl	%eax, %eax
	je	.L300
	leal	-1(%rax), %edx
	subl	$2, %r12d
	movzwl	152(%rbx,%rdx,2), %esi
	movl	%r13d, %edx
	shrq	$2, %r13
	andl	$3, %edx
	addl	$3, %edx
	jmp	.L72
.L113:
	movzbl	(%rcx), %edi
	movzwl	2(%rcx), %r10d
	jmp	.L116
.L83:
	movq	-168(%rbp), %rax
	movl	$9, 120(%r14)
	movl	$1, %edi
	movq	-144(%rbp), %r9
	movq	-176(%rbp), %r8
	movq	%rax, 144(%r14)
	movq	-152(%rbp), %rcx
	movq	%rax, 104(%r14)
	movq	-160(%rbp), %rsi
	call	inflate_table
	testl	%eax, %eax
	je	.L84
	movl	%r15d, %r10d
	movq	-80(%rbp), %r15
	leaq	.LC6(%rip), %rax
	movq	%rax, 48(%r15)
	movl	$16209, 8(%r14)
	jmp	.L22
.L84:
	movq	144(%r14), %rax
	movl	132(%r14), %esi
	movl	$6, 124(%r14)
	leaq	124(%r14), %r8
	movl	136(%r14), %edx
	movq	-144(%rbp), %r9
	movl	$2, %edi
	movq	%rax, 112(%r14)
	movq	-152(%rbp), %rcx
	addq	%rsi, %rsi
	addq	-160(%rbp), %rsi
	call	inflate_table
	testl	%eax, %eax
	je	.L85
	movl	%r15d, %r10d
	movq	-80(%rbp), %r15
	leaq	.LC7(%rip), %rax
	movq	%rax, 48(%r15)
	movl	$16209, 8(%r14)
	jmp	.L22
.L85:
	movl	$16200, 8(%r14)
	jmp	.L25
	.cfi_endproc
.LFE40:
	.size	inflateBack, .-inflateBack
	.p2align 4
	.globl	inflateBackEnd
	.type	inflateBackEnd, @function
inflateBackEnd:
.LFB41:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L312
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	56(%rdi), %rsi
	testq	%rsi, %rsi
	je	.L314
	movq	72(%rdi), %rax
	testq	%rax, %rax
	je	.L314
	movq	80(%rdi), %rdi
	call	*%rax
	movq	$0, 56(%rbx)
	xorl	%eax, %eax
.L310:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L314:
	.cfi_restore_state
	movl	$-2, %eax
	jmp	.L310
.L312:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	movl	$-2, %eax
	ret
	.cfi_endproc
.LFE41:
	.size	inflateBackEnd, .-inflateBackEnd
	.section	.rodata
	.align 32
	.type	distfix.4103, @object
	.size	distfix.4103, 128
distfix.4103:
	.byte	16
	.byte	5
	.value	1
	.byte	23
	.byte	5
	.value	257
	.byte	19
	.byte	5
	.value	17
	.byte	27
	.byte	5
	.value	4097
	.byte	17
	.byte	5
	.value	5
	.byte	25
	.byte	5
	.value	1025
	.byte	21
	.byte	5
	.value	65
	.byte	29
	.byte	5
	.value	16385
	.byte	16
	.byte	5
	.value	3
	.byte	24
	.byte	5
	.value	513
	.byte	20
	.byte	5
	.value	33
	.byte	28
	.byte	5
	.value	8193
	.byte	18
	.byte	5
	.value	9
	.byte	26
	.byte	5
	.value	2049
	.byte	22
	.byte	5
	.value	129
	.byte	64
	.byte	5
	.value	0
	.byte	16
	.byte	5
	.value	2
	.byte	23
	.byte	5
	.value	385
	.byte	19
	.byte	5
	.value	25
	.byte	27
	.byte	5
	.value	6145
	.byte	17
	.byte	5
	.value	7
	.byte	25
	.byte	5
	.value	1537
	.byte	21
	.byte	5
	.value	97
	.byte	29
	.byte	5
	.value	24577
	.byte	16
	.byte	5
	.value	4
	.byte	24
	.byte	5
	.value	769
	.byte	20
	.byte	5
	.value	49
	.byte	28
	.byte	5
	.value	12289
	.byte	18
	.byte	5
	.value	13
	.byte	26
	.byte	5
	.value	3073
	.byte	22
	.byte	5
	.value	193
	.byte	64
	.byte	5
	.value	0
	.align 32
	.type	lenfix.4102, @object
	.size	lenfix.4102, 2048
lenfix.4102:
	.byte	96
	.byte	7
	.value	0
	.byte	0
	.byte	8
	.value	80
	.byte	0
	.byte	8
	.value	16
	.byte	20
	.byte	8
	.value	115
	.byte	18
	.byte	7
	.value	31
	.byte	0
	.byte	8
	.value	112
	.byte	0
	.byte	8
	.value	48
	.byte	0
	.byte	9
	.value	192
	.byte	16
	.byte	7
	.value	10
	.byte	0
	.byte	8
	.value	96
	.byte	0
	.byte	8
	.value	32
	.byte	0
	.byte	9
	.value	160
	.byte	0
	.byte	8
	.value	0
	.byte	0
	.byte	8
	.value	128
	.byte	0
	.byte	8
	.value	64
	.byte	0
	.byte	9
	.value	224
	.byte	16
	.byte	7
	.value	6
	.byte	0
	.byte	8
	.value	88
	.byte	0
	.byte	8
	.value	24
	.byte	0
	.byte	9
	.value	144
	.byte	19
	.byte	7
	.value	59
	.byte	0
	.byte	8
	.value	120
	.byte	0
	.byte	8
	.value	56
	.byte	0
	.byte	9
	.value	208
	.byte	17
	.byte	7
	.value	17
	.byte	0
	.byte	8
	.value	104
	.byte	0
	.byte	8
	.value	40
	.byte	0
	.byte	9
	.value	176
	.byte	0
	.byte	8
	.value	8
	.byte	0
	.byte	8
	.value	136
	.byte	0
	.byte	8
	.value	72
	.byte	0
	.byte	9
	.value	240
	.byte	16
	.byte	7
	.value	4
	.byte	0
	.byte	8
	.value	84
	.byte	0
	.byte	8
	.value	20
	.byte	21
	.byte	8
	.value	227
	.byte	19
	.byte	7
	.value	43
	.byte	0
	.byte	8
	.value	116
	.byte	0
	.byte	8
	.value	52
	.byte	0
	.byte	9
	.value	200
	.byte	17
	.byte	7
	.value	13
	.byte	0
	.byte	8
	.value	100
	.byte	0
	.byte	8
	.value	36
	.byte	0
	.byte	9
	.value	168
	.byte	0
	.byte	8
	.value	4
	.byte	0
	.byte	8
	.value	132
	.byte	0
	.byte	8
	.value	68
	.byte	0
	.byte	9
	.value	232
	.byte	16
	.byte	7
	.value	8
	.byte	0
	.byte	8
	.value	92
	.byte	0
	.byte	8
	.value	28
	.byte	0
	.byte	9
	.value	152
	.byte	20
	.byte	7
	.value	83
	.byte	0
	.byte	8
	.value	124
	.byte	0
	.byte	8
	.value	60
	.byte	0
	.byte	9
	.value	216
	.byte	18
	.byte	7
	.value	23
	.byte	0
	.byte	8
	.value	108
	.byte	0
	.byte	8
	.value	44
	.byte	0
	.byte	9
	.value	184
	.byte	0
	.byte	8
	.value	12
	.byte	0
	.byte	8
	.value	140
	.byte	0
	.byte	8
	.value	76
	.byte	0
	.byte	9
	.value	248
	.byte	16
	.byte	7
	.value	3
	.byte	0
	.byte	8
	.value	82
	.byte	0
	.byte	8
	.value	18
	.byte	21
	.byte	8
	.value	163
	.byte	19
	.byte	7
	.value	35
	.byte	0
	.byte	8
	.value	114
	.byte	0
	.byte	8
	.value	50
	.byte	0
	.byte	9
	.value	196
	.byte	17
	.byte	7
	.value	11
	.byte	0
	.byte	8
	.value	98
	.byte	0
	.byte	8
	.value	34
	.byte	0
	.byte	9
	.value	164
	.byte	0
	.byte	8
	.value	2
	.byte	0
	.byte	8
	.value	130
	.byte	0
	.byte	8
	.value	66
	.byte	0
	.byte	9
	.value	228
	.byte	16
	.byte	7
	.value	7
	.byte	0
	.byte	8
	.value	90
	.byte	0
	.byte	8
	.value	26
	.byte	0
	.byte	9
	.value	148
	.byte	20
	.byte	7
	.value	67
	.byte	0
	.byte	8
	.value	122
	.byte	0
	.byte	8
	.value	58
	.byte	0
	.byte	9
	.value	212
	.byte	18
	.byte	7
	.value	19
	.byte	0
	.byte	8
	.value	106
	.byte	0
	.byte	8
	.value	42
	.byte	0
	.byte	9
	.value	180
	.byte	0
	.byte	8
	.value	10
	.byte	0
	.byte	8
	.value	138
	.byte	0
	.byte	8
	.value	74
	.byte	0
	.byte	9
	.value	244
	.byte	16
	.byte	7
	.value	5
	.byte	0
	.byte	8
	.value	86
	.byte	0
	.byte	8
	.value	22
	.byte	64
	.byte	8
	.value	0
	.byte	19
	.byte	7
	.value	51
	.byte	0
	.byte	8
	.value	118
	.byte	0
	.byte	8
	.value	54
	.byte	0
	.byte	9
	.value	204
	.byte	17
	.byte	7
	.value	15
	.byte	0
	.byte	8
	.value	102
	.byte	0
	.byte	8
	.value	38
	.byte	0
	.byte	9
	.value	172
	.byte	0
	.byte	8
	.value	6
	.byte	0
	.byte	8
	.value	134
	.byte	0
	.byte	8
	.value	70
	.byte	0
	.byte	9
	.value	236
	.byte	16
	.byte	7
	.value	9
	.byte	0
	.byte	8
	.value	94
	.byte	0
	.byte	8
	.value	30
	.byte	0
	.byte	9
	.value	156
	.byte	20
	.byte	7
	.value	99
	.byte	0
	.byte	8
	.value	126
	.byte	0
	.byte	8
	.value	62
	.byte	0
	.byte	9
	.value	220
	.byte	18
	.byte	7
	.value	27
	.byte	0
	.byte	8
	.value	110
	.byte	0
	.byte	8
	.value	46
	.byte	0
	.byte	9
	.value	188
	.byte	0
	.byte	8
	.value	14
	.byte	0
	.byte	8
	.value	142
	.byte	0
	.byte	8
	.value	78
	.byte	0
	.byte	9
	.value	252
	.byte	96
	.byte	7
	.value	0
	.byte	0
	.byte	8
	.value	81
	.byte	0
	.byte	8
	.value	17
	.byte	21
	.byte	8
	.value	131
	.byte	18
	.byte	7
	.value	31
	.byte	0
	.byte	8
	.value	113
	.byte	0
	.byte	8
	.value	49
	.byte	0
	.byte	9
	.value	194
	.byte	16
	.byte	7
	.value	10
	.byte	0
	.byte	8
	.value	97
	.byte	0
	.byte	8
	.value	33
	.byte	0
	.byte	9
	.value	162
	.byte	0
	.byte	8
	.value	1
	.byte	0
	.byte	8
	.value	129
	.byte	0
	.byte	8
	.value	65
	.byte	0
	.byte	9
	.value	226
	.byte	16
	.byte	7
	.value	6
	.byte	0
	.byte	8
	.value	89
	.byte	0
	.byte	8
	.value	25
	.byte	0
	.byte	9
	.value	146
	.byte	19
	.byte	7
	.value	59
	.byte	0
	.byte	8
	.value	121
	.byte	0
	.byte	8
	.value	57
	.byte	0
	.byte	9
	.value	210
	.byte	17
	.byte	7
	.value	17
	.byte	0
	.byte	8
	.value	105
	.byte	0
	.byte	8
	.value	41
	.byte	0
	.byte	9
	.value	178
	.byte	0
	.byte	8
	.value	9
	.byte	0
	.byte	8
	.value	137
	.byte	0
	.byte	8
	.value	73
	.byte	0
	.byte	9
	.value	242
	.byte	16
	.byte	7
	.value	4
	.byte	0
	.byte	8
	.value	85
	.byte	0
	.byte	8
	.value	21
	.byte	16
	.byte	8
	.value	258
	.byte	19
	.byte	7
	.value	43
	.byte	0
	.byte	8
	.value	117
	.byte	0
	.byte	8
	.value	53
	.byte	0
	.byte	9
	.value	202
	.byte	17
	.byte	7
	.value	13
	.byte	0
	.byte	8
	.value	101
	.byte	0
	.byte	8
	.value	37
	.byte	0
	.byte	9
	.value	170
	.byte	0
	.byte	8
	.value	5
	.byte	0
	.byte	8
	.value	133
	.byte	0
	.byte	8
	.value	69
	.byte	0
	.byte	9
	.value	234
	.byte	16
	.byte	7
	.value	8
	.byte	0
	.byte	8
	.value	93
	.byte	0
	.byte	8
	.value	29
	.byte	0
	.byte	9
	.value	154
	.byte	20
	.byte	7
	.value	83
	.byte	0
	.byte	8
	.value	125
	.byte	0
	.byte	8
	.value	61
	.byte	0
	.byte	9
	.value	218
	.byte	18
	.byte	7
	.value	23
	.byte	0
	.byte	8
	.value	109
	.byte	0
	.byte	8
	.value	45
	.byte	0
	.byte	9
	.value	186
	.byte	0
	.byte	8
	.value	13
	.byte	0
	.byte	8
	.value	141
	.byte	0
	.byte	8
	.value	77
	.byte	0
	.byte	9
	.value	250
	.byte	16
	.byte	7
	.value	3
	.byte	0
	.byte	8
	.value	83
	.byte	0
	.byte	8
	.value	19
	.byte	21
	.byte	8
	.value	195
	.byte	19
	.byte	7
	.value	35
	.byte	0
	.byte	8
	.value	115
	.byte	0
	.byte	8
	.value	51
	.byte	0
	.byte	9
	.value	198
	.byte	17
	.byte	7
	.value	11
	.byte	0
	.byte	8
	.value	99
	.byte	0
	.byte	8
	.value	35
	.byte	0
	.byte	9
	.value	166
	.byte	0
	.byte	8
	.value	3
	.byte	0
	.byte	8
	.value	131
	.byte	0
	.byte	8
	.value	67
	.byte	0
	.byte	9
	.value	230
	.byte	16
	.byte	7
	.value	7
	.byte	0
	.byte	8
	.value	91
	.byte	0
	.byte	8
	.value	27
	.byte	0
	.byte	9
	.value	150
	.byte	20
	.byte	7
	.value	67
	.byte	0
	.byte	8
	.value	123
	.byte	0
	.byte	8
	.value	59
	.byte	0
	.byte	9
	.value	214
	.byte	18
	.byte	7
	.value	19
	.byte	0
	.byte	8
	.value	107
	.byte	0
	.byte	8
	.value	43
	.byte	0
	.byte	9
	.value	182
	.byte	0
	.byte	8
	.value	11
	.byte	0
	.byte	8
	.value	139
	.byte	0
	.byte	8
	.value	75
	.byte	0
	.byte	9
	.value	246
	.byte	16
	.byte	7
	.value	5
	.byte	0
	.byte	8
	.value	87
	.byte	0
	.byte	8
	.value	23
	.byte	64
	.byte	8
	.value	0
	.byte	19
	.byte	7
	.value	51
	.byte	0
	.byte	8
	.value	119
	.byte	0
	.byte	8
	.value	55
	.byte	0
	.byte	9
	.value	206
	.byte	17
	.byte	7
	.value	15
	.byte	0
	.byte	8
	.value	103
	.byte	0
	.byte	8
	.value	39
	.byte	0
	.byte	9
	.value	174
	.byte	0
	.byte	8
	.value	7
	.byte	0
	.byte	8
	.value	135
	.byte	0
	.byte	8
	.value	71
	.byte	0
	.byte	9
	.value	238
	.byte	16
	.byte	7
	.value	9
	.byte	0
	.byte	8
	.value	95
	.byte	0
	.byte	8
	.value	31
	.byte	0
	.byte	9
	.value	158
	.byte	20
	.byte	7
	.value	99
	.byte	0
	.byte	8
	.value	127
	.byte	0
	.byte	8
	.value	63
	.byte	0
	.byte	9
	.value	222
	.byte	18
	.byte	7
	.value	27
	.byte	0
	.byte	8
	.value	111
	.byte	0
	.byte	8
	.value	47
	.byte	0
	.byte	9
	.value	190
	.byte	0
	.byte	8
	.value	15
	.byte	0
	.byte	8
	.value	143
	.byte	0
	.byte	8
	.value	79
	.byte	0
	.byte	9
	.value	254
	.byte	96
	.byte	7
	.value	0
	.byte	0
	.byte	8
	.value	80
	.byte	0
	.byte	8
	.value	16
	.byte	20
	.byte	8
	.value	115
	.byte	18
	.byte	7
	.value	31
	.byte	0
	.byte	8
	.value	112
	.byte	0
	.byte	8
	.value	48
	.byte	0
	.byte	9
	.value	193
	.byte	16
	.byte	7
	.value	10
	.byte	0
	.byte	8
	.value	96
	.byte	0
	.byte	8
	.value	32
	.byte	0
	.byte	9
	.value	161
	.byte	0
	.byte	8
	.value	0
	.byte	0
	.byte	8
	.value	128
	.byte	0
	.byte	8
	.value	64
	.byte	0
	.byte	9
	.value	225
	.byte	16
	.byte	7
	.value	6
	.byte	0
	.byte	8
	.value	88
	.byte	0
	.byte	8
	.value	24
	.byte	0
	.byte	9
	.value	145
	.byte	19
	.byte	7
	.value	59
	.byte	0
	.byte	8
	.value	120
	.byte	0
	.byte	8
	.value	56
	.byte	0
	.byte	9
	.value	209
	.byte	17
	.byte	7
	.value	17
	.byte	0
	.byte	8
	.value	104
	.byte	0
	.byte	8
	.value	40
	.byte	0
	.byte	9
	.value	177
	.byte	0
	.byte	8
	.value	8
	.byte	0
	.byte	8
	.value	136
	.byte	0
	.byte	8
	.value	72
	.byte	0
	.byte	9
	.value	241
	.byte	16
	.byte	7
	.value	4
	.byte	0
	.byte	8
	.value	84
	.byte	0
	.byte	8
	.value	20
	.byte	21
	.byte	8
	.value	227
	.byte	19
	.byte	7
	.value	43
	.byte	0
	.byte	8
	.value	116
	.byte	0
	.byte	8
	.value	52
	.byte	0
	.byte	9
	.value	201
	.byte	17
	.byte	7
	.value	13
	.byte	0
	.byte	8
	.value	100
	.byte	0
	.byte	8
	.value	36
	.byte	0
	.byte	9
	.value	169
	.byte	0
	.byte	8
	.value	4
	.byte	0
	.byte	8
	.value	132
	.byte	0
	.byte	8
	.value	68
	.byte	0
	.byte	9
	.value	233
	.byte	16
	.byte	7
	.value	8
	.byte	0
	.byte	8
	.value	92
	.byte	0
	.byte	8
	.value	28
	.byte	0
	.byte	9
	.value	153
	.byte	20
	.byte	7
	.value	83
	.byte	0
	.byte	8
	.value	124
	.byte	0
	.byte	8
	.value	60
	.byte	0
	.byte	9
	.value	217
	.byte	18
	.byte	7
	.value	23
	.byte	0
	.byte	8
	.value	108
	.byte	0
	.byte	8
	.value	44
	.byte	0
	.byte	9
	.value	185
	.byte	0
	.byte	8
	.value	12
	.byte	0
	.byte	8
	.value	140
	.byte	0
	.byte	8
	.value	76
	.byte	0
	.byte	9
	.value	249
	.byte	16
	.byte	7
	.value	3
	.byte	0
	.byte	8
	.value	82
	.byte	0
	.byte	8
	.value	18
	.byte	21
	.byte	8
	.value	163
	.byte	19
	.byte	7
	.value	35
	.byte	0
	.byte	8
	.value	114
	.byte	0
	.byte	8
	.value	50
	.byte	0
	.byte	9
	.value	197
	.byte	17
	.byte	7
	.value	11
	.byte	0
	.byte	8
	.value	98
	.byte	0
	.byte	8
	.value	34
	.byte	0
	.byte	9
	.value	165
	.byte	0
	.byte	8
	.value	2
	.byte	0
	.byte	8
	.value	130
	.byte	0
	.byte	8
	.value	66
	.byte	0
	.byte	9
	.value	229
	.byte	16
	.byte	7
	.value	7
	.byte	0
	.byte	8
	.value	90
	.byte	0
	.byte	8
	.value	26
	.byte	0
	.byte	9
	.value	149
	.byte	20
	.byte	7
	.value	67
	.byte	0
	.byte	8
	.value	122
	.byte	0
	.byte	8
	.value	58
	.byte	0
	.byte	9
	.value	213
	.byte	18
	.byte	7
	.value	19
	.byte	0
	.byte	8
	.value	106
	.byte	0
	.byte	8
	.value	42
	.byte	0
	.byte	9
	.value	181
	.byte	0
	.byte	8
	.value	10
	.byte	0
	.byte	8
	.value	138
	.byte	0
	.byte	8
	.value	74
	.byte	0
	.byte	9
	.value	245
	.byte	16
	.byte	7
	.value	5
	.byte	0
	.byte	8
	.value	86
	.byte	0
	.byte	8
	.value	22
	.byte	64
	.byte	8
	.value	0
	.byte	19
	.byte	7
	.value	51
	.byte	0
	.byte	8
	.value	118
	.byte	0
	.byte	8
	.value	54
	.byte	0
	.byte	9
	.value	205
	.byte	17
	.byte	7
	.value	15
	.byte	0
	.byte	8
	.value	102
	.byte	0
	.byte	8
	.value	38
	.byte	0
	.byte	9
	.value	173
	.byte	0
	.byte	8
	.value	6
	.byte	0
	.byte	8
	.value	134
	.byte	0
	.byte	8
	.value	70
	.byte	0
	.byte	9
	.value	237
	.byte	16
	.byte	7
	.value	9
	.byte	0
	.byte	8
	.value	94
	.byte	0
	.byte	8
	.value	30
	.byte	0
	.byte	9
	.value	157
	.byte	20
	.byte	7
	.value	99
	.byte	0
	.byte	8
	.value	126
	.byte	0
	.byte	8
	.value	62
	.byte	0
	.byte	9
	.value	221
	.byte	18
	.byte	7
	.value	27
	.byte	0
	.byte	8
	.value	110
	.byte	0
	.byte	8
	.value	46
	.byte	0
	.byte	9
	.value	189
	.byte	0
	.byte	8
	.value	14
	.byte	0
	.byte	8
	.value	142
	.byte	0
	.byte	8
	.value	78
	.byte	0
	.byte	9
	.value	253
	.byte	96
	.byte	7
	.value	0
	.byte	0
	.byte	8
	.value	81
	.byte	0
	.byte	8
	.value	17
	.byte	21
	.byte	8
	.value	131
	.byte	18
	.byte	7
	.value	31
	.byte	0
	.byte	8
	.value	113
	.byte	0
	.byte	8
	.value	49
	.byte	0
	.byte	9
	.value	195
	.byte	16
	.byte	7
	.value	10
	.byte	0
	.byte	8
	.value	97
	.byte	0
	.byte	8
	.value	33
	.byte	0
	.byte	9
	.value	163
	.byte	0
	.byte	8
	.value	1
	.byte	0
	.byte	8
	.value	129
	.byte	0
	.byte	8
	.value	65
	.byte	0
	.byte	9
	.value	227
	.byte	16
	.byte	7
	.value	6
	.byte	0
	.byte	8
	.value	89
	.byte	0
	.byte	8
	.value	25
	.byte	0
	.byte	9
	.value	147
	.byte	19
	.byte	7
	.value	59
	.byte	0
	.byte	8
	.value	121
	.byte	0
	.byte	8
	.value	57
	.byte	0
	.byte	9
	.value	211
	.byte	17
	.byte	7
	.value	17
	.byte	0
	.byte	8
	.value	105
	.byte	0
	.byte	8
	.value	41
	.byte	0
	.byte	9
	.value	179
	.byte	0
	.byte	8
	.value	9
	.byte	0
	.byte	8
	.value	137
	.byte	0
	.byte	8
	.value	73
	.byte	0
	.byte	9
	.value	243
	.byte	16
	.byte	7
	.value	4
	.byte	0
	.byte	8
	.value	85
	.byte	0
	.byte	8
	.value	21
	.byte	16
	.byte	8
	.value	258
	.byte	19
	.byte	7
	.value	43
	.byte	0
	.byte	8
	.value	117
	.byte	0
	.byte	8
	.value	53
	.byte	0
	.byte	9
	.value	203
	.byte	17
	.byte	7
	.value	13
	.byte	0
	.byte	8
	.value	101
	.byte	0
	.byte	8
	.value	37
	.byte	0
	.byte	9
	.value	171
	.byte	0
	.byte	8
	.value	5
	.byte	0
	.byte	8
	.value	133
	.byte	0
	.byte	8
	.value	69
	.byte	0
	.byte	9
	.value	235
	.byte	16
	.byte	7
	.value	8
	.byte	0
	.byte	8
	.value	93
	.byte	0
	.byte	8
	.value	29
	.byte	0
	.byte	9
	.value	155
	.byte	20
	.byte	7
	.value	83
	.byte	0
	.byte	8
	.value	125
	.byte	0
	.byte	8
	.value	61
	.byte	0
	.byte	9
	.value	219
	.byte	18
	.byte	7
	.value	23
	.byte	0
	.byte	8
	.value	109
	.byte	0
	.byte	8
	.value	45
	.byte	0
	.byte	9
	.value	187
	.byte	0
	.byte	8
	.value	13
	.byte	0
	.byte	8
	.value	141
	.byte	0
	.byte	8
	.value	77
	.byte	0
	.byte	9
	.value	251
	.byte	16
	.byte	7
	.value	3
	.byte	0
	.byte	8
	.value	83
	.byte	0
	.byte	8
	.value	19
	.byte	21
	.byte	8
	.value	195
	.byte	19
	.byte	7
	.value	35
	.byte	0
	.byte	8
	.value	115
	.byte	0
	.byte	8
	.value	51
	.byte	0
	.byte	9
	.value	199
	.byte	17
	.byte	7
	.value	11
	.byte	0
	.byte	8
	.value	99
	.byte	0
	.byte	8
	.value	35
	.byte	0
	.byte	9
	.value	167
	.byte	0
	.byte	8
	.value	3
	.byte	0
	.byte	8
	.value	131
	.byte	0
	.byte	8
	.value	67
	.byte	0
	.byte	9
	.value	231
	.byte	16
	.byte	7
	.value	7
	.byte	0
	.byte	8
	.value	91
	.byte	0
	.byte	8
	.value	27
	.byte	0
	.byte	9
	.value	151
	.byte	20
	.byte	7
	.value	67
	.byte	0
	.byte	8
	.value	123
	.byte	0
	.byte	8
	.value	59
	.byte	0
	.byte	9
	.value	215
	.byte	18
	.byte	7
	.value	19
	.byte	0
	.byte	8
	.value	107
	.byte	0
	.byte	8
	.value	43
	.byte	0
	.byte	9
	.value	183
	.byte	0
	.byte	8
	.value	11
	.byte	0
	.byte	8
	.value	139
	.byte	0
	.byte	8
	.value	75
	.byte	0
	.byte	9
	.value	247
	.byte	16
	.byte	7
	.value	5
	.byte	0
	.byte	8
	.value	87
	.byte	0
	.byte	8
	.value	23
	.byte	64
	.byte	8
	.value	0
	.byte	19
	.byte	7
	.value	51
	.byte	0
	.byte	8
	.value	119
	.byte	0
	.byte	8
	.value	55
	.byte	0
	.byte	9
	.value	207
	.byte	17
	.byte	7
	.value	15
	.byte	0
	.byte	8
	.value	103
	.byte	0
	.byte	8
	.value	39
	.byte	0
	.byte	9
	.value	175
	.byte	0
	.byte	8
	.value	7
	.byte	0
	.byte	8
	.value	135
	.byte	0
	.byte	8
	.value	71
	.byte	0
	.byte	9
	.value	239
	.byte	16
	.byte	7
	.value	9
	.byte	0
	.byte	8
	.value	95
	.byte	0
	.byte	8
	.value	31
	.byte	0
	.byte	9
	.value	159
	.byte	20
	.byte	7
	.value	99
	.byte	0
	.byte	8
	.value	127
	.byte	0
	.byte	8
	.value	63
	.byte	0
	.byte	9
	.value	223
	.byte	18
	.byte	7
	.value	27
	.byte	0
	.byte	8
	.value	111
	.byte	0
	.byte	8
	.value	47
	.byte	0
	.byte	9
	.value	191
	.byte	0
	.byte	8
	.value	15
	.byte	0
	.byte	8
	.value	143
	.byte	0
	.byte	8
	.value	79
	.byte	0
	.byte	9
	.value	255
	.align 32
	.type	order.4124, @object
	.size	order.4124, 38
order.4124:
	.value	16
	.value	17
	.value	18
	.value	0
	.value	8
	.value	7
	.value	9
	.value	6
	.value	10
	.value	5
	.value	11
	.value	4
	.value	12
	.value	3
	.value	13
	.value	2
	.value	14
	.value	1
	.value	15
	.hidden	inflate_table
	.hidden	inflate_fast
	.hidden	zcfree
	.hidden	zcalloc
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
