	.file	"compress.c"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"1.2.11"
	.text
	.p2align 4
	.globl	compress2
	.type	compress2, @function
compress2:
.LFB17:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rax
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	leaq	-176(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%r15, %rdi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	leaq	.LC0(%rip), %rdx
	subq	$152, %rsp
	movq	%rsi, -192(%rbp)
	movq	(%rax), %r12
	movl	%r8d, %esi
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	movq	$0, (%rax)
	movl	$112, %ecx
	movq	$0, -96(%rbp)
	movaps	%xmm0, -112(%rbp)
	call	deflateInit_@PLT
	movl	%eax, -180(%rbp)
	testl	%eax, %eax
	jne	.L1
	movq	%r13, -152(%rbp)
	xorl	%edx, %edx
	movl	$4294967295, %r13d
	movl	$0, -144(%rbp)
	movq	%rbx, -176(%rbp)
	movl	$0, -168(%rbp)
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L17:
	movl	-144(%rbp), %eax
	movl	-168(%rbp), %edx
.L8:
	testl	%eax, %eax
	jne	.L3
	cmpq	%r13, %r12
	movq	%r13, %rax
	cmovbe	%r12, %rax
	movl	%eax, -144(%rbp)
	subq	%rax, %r12
.L3:
	testl	%edx, %edx
	jne	.L4
	cmpq	%r13, %r14
	movq	%r13, %rax
	cmovbe	%r14, %rax
	movl	%eax, -168(%rbp)
	subq	%rax, %r14
.L4:
	movl	$4, %esi
	testq	%r14, %r14
	je	.L15
	xorl	%esi, %esi
.L15:
	movq	%r15, %rdi
	call	deflate@PLT
	movl	%eax, %ebx
	testl	%eax, %eax
	je	.L17
	movq	-136(%rbp), %rax
	movq	-192(%rbp), %rcx
	movq	%r15, %rdi
	movq	%rax, (%rcx)
	call	deflateEnd@PLT
	cmpl	$1, %ebx
	cmove	-180(%rbp), %ebx
	movl	%ebx, -180(%rbp)
.L1:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L18
	movl	-180(%rbp), %eax
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L18:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17:
	.size	compress2, .-compress2
	.p2align 4
	.globl	compress
	.type	compress, @function
compress:
.LFB18:
	.cfi_startproc
	endbr64
	movl	$-1, %r8d
	jmp	compress2
	.cfi_endproc
.LFE18:
	.size	compress, .-compress
	.p2align 4
	.globl	compressBound
	.type	compressBound, @function
compressBound:
.LFB19:
	.cfi_startproc
	endbr64
	movq	%rdi, %rdx
	movq	%rdi, %rax
	shrq	$12, %rdx
	shrq	$25, %rax
	leaq	13(%rdi,%rdx), %rdx
	shrq	$14, %rdi
	addq	%rdx, %rdi
	addq	%rax, %rdi
	movq	%rdi, %rax
	shrq	$7, %rax
	addq	%rdi, %rax
	ret
	.cfi_endproc
.LFE19:
	.size	compressBound, .-compressBound
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
