	.file	"deflate.c"
	.text
	.p2align 4
	.type	longest_match, @function
longest_match:
.LFB695:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$0, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	252(%rdi), %r8d
	movq	176(%rdi), %r10
	movl	288(%rdi), %ebx
	movl	264(%rdi), %eax
	movl	268(%rdi), %ecx
	movl	160(%rdi), %edi
	leal	262(%r8), %r9d
	leaq	(%r10,%r8), %r13
	movl	%ebx, -44(%rbp)
	movl	168(%r12), %ebx
	leal	-262(%rdi), %r11d
	subl	%edi, %r9d
	movq	192(%r12), %rdi
	cmpl	%r11d, %r8d
	cmovbe	%edx, %r9d
	leaq	258(%r10,%r8), %rdx
	movq	%rdx, %r8
	movslq	%eax, %rdx
	movzbl	-1(%r13,%rdx), %r14d
	movzbl	0(%r13,%rdx), %r11d
	cmpl	284(%r12), %eax
	jb	.L3
	shrl	$2, %ecx
.L3:
	movl	260(%r12), %r15d
	movl	-44(%rbp), %edx
	movq	%r12, -56(%rbp)
	movq	%r8, %r12
	cmpl	%edx, %r15d
	cmovb	%r15d, %edx
	movl	%edx, -44(%rbp)
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L5:
	andl	%ebx, %esi
	movzwl	(%rdi,%rsi,2), %esi
	cmpl	%esi, %r9d
	jnb	.L16
.L36:
	subl	$1, %ecx
	je	.L16
.L17:
	movl	%esi, %edx
	movslq	%eax, %r8
	addq	%r10, %rdx
	cmpb	%r11b, (%rdx,%r8)
	jne	.L5
	cmpb	%r14b, -1(%rdx,%r8)
	jne	.L5
	movzbl	0(%r13), %r8d
	cmpb	%r8b, (%rdx)
	jne	.L5
	movzbl	1(%r13), %r8d
	cmpb	%r8b, 1(%rdx)
	jne	.L5
	movl	%eax, -48(%rbp)
	leaq	2(%r13), %r8
	addq	$2, %rdx
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L35:
	movzbl	2(%rdx), %eax
	cmpb	%al, 2(%r8)
	jne	.L29
	movzbl	3(%rdx), %eax
	cmpb	%al, 3(%r8)
	jne	.L30
	movzbl	4(%rdx), %eax
	cmpb	%al, 4(%r8)
	jne	.L31
	movzbl	5(%rdx), %eax
	cmpb	%al, 5(%r8)
	jne	.L32
	movzbl	6(%rdx), %eax
	cmpb	%al, 6(%r8)
	jne	.L33
	movzbl	7(%rdx), %eax
	cmpb	%al, 7(%r8)
	jne	.L34
	addq	$8, %r8
	addq	$8, %rdx
	movzbl	(%rdx), %eax
	cmpb	%al, (%r8)
	jne	.L25
	cmpq	%r8, %r12
	jbe	.L25
.L14:
	movzbl	1(%rdx), %eax
	cmpb	%al, 1(%r8)
	je	.L35
	movl	-48(%rbp), %eax
	addq	$1, %r8
	.p2align 4,,10
	.p2align 3
.L8:
	movq	%r12, %rdx
	subq	%r8, %rdx
	movl	$258, %r8d
	subl	%edx, %r8d
	cmpl	%r8d, %eax
	jge	.L5
	movq	-56(%rbp), %rax
	movl	%esi, 256(%rax)
	cmpl	-44(%rbp), %r8d
	jge	.L19
	andl	%ebx, %esi
	movslq	%r8d, %rax
	movzwl	(%rdi,%rsi,2), %esi
	movzbl	-1(%r13,%rax), %r14d
	movzbl	0(%r13,%rax), %r11d
	movl	%r8d, %eax
	cmpl	%esi, %r9d
	jb	.L36
.L16:
	cmpl	%r15d, %eax
	cmova	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L25:
	.cfi_restore_state
	movl	-48(%rbp), %eax
	jmp	.L8
.L29:
	movl	-48(%rbp), %eax
	addq	$2, %r8
	jmp	.L8
.L30:
	movl	-48(%rbp), %eax
	addq	$3, %r8
	jmp	.L8
.L31:
	movl	-48(%rbp), %eax
	addq	$4, %r8
	jmp	.L8
.L33:
	movl	-48(%rbp), %eax
	addq	$6, %r8
	jmp	.L8
.L32:
	movl	-48(%rbp), %eax
	addq	$5, %r8
	jmp	.L8
.L34:
	movl	-48(%rbp), %eax
	addq	$7, %r8
	jmp	.L8
.L19:
	movl	%r8d, %eax
	jmp	.L16
	.cfi_endproc
.LFE695:
	.size	longest_match, .-longest_match
	.p2align 4
	.type	insert_string_optimized, @function
insert_string_optimized:
.LFB671:
	.cfi_startproc
	movq	176(%rdi), %rdx
	movzwl	%si, %eax
	cmpl	$5, 276(%rdi)
	movl	(%rdx,%rax), %edx
	jle	.L38
	andl	$16777215, %edx
.L38:
	xorl	%eax, %eax
	crc32l	%edx, %eax
	movq	200(%rdi), %rdx
	andl	220(%rdi), %eax
	leaq	(%rdx,%rax,2), %rdx
	movzwl	(%rdx), %eax
	movw	%si, (%rdx)
	andw	168(%rdi), %si
	movq	192(%rdi), %rdx
	movzwl	%si, %esi
	movw	%ax, (%rdx,%rsi,2)
	ret
	.cfi_endproc
.LFE671:
	.size	insert_string_optimized, .-insert_string_optimized
	.p2align 4
	.type	fill_window, @function
fill_window:
.LFB696:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movl	x86_cpu_enable_simd(%rip), %r11d
	testl	%r11d, %r11d
	jne	.L80
	movl	160(%rdi), %ebx
	movl	260(%rdi), %edx
	leal	-262(%rbx), %eax
	movl	%ebx, %ecx
	movl	%eax, -52(%rbp)
	movl	%ebx, %eax
	movq	%rax, -64(%rbp)
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L42:
	movq	(%r12), %r15
	movl	8(%r15), %ecx
	testl	%ecx, %ecx
	je	.L51
.L83:
	cmpl	%r14d, %ecx
	movl	260(%r12), %edx
	cmovbe	%ecx, %r14d
	testl	%r14d, %r14d
	je	.L52
	movl	%eax, %r8d
	movq	56(%r15), %rax
	subl	%r14d, %ecx
	movl	%r14d, %r13d
	addq	%rdx, %r8
	addq	176(%r12), %r8
	movl	%ecx, 8(%r15)
	cmpl	$2, 48(%rax)
	je	.L81
	movq	(%r15), %rsi
	movq	%r8, %rdi
	movq	%r13, %rdx
	call	memcpy@PLT
	movq	%rax, %r8
	movq	56(%r15), %rax
	cmpl	$1, 48(%rax)
	je	.L82
.L54:
	addq	%r13, (%r15)
	movl	260(%r12), %edx
	addq	%r13, 16(%r15)
	addl	%r14d, %edx
.L52:
	movl	6012(%r12), %eax
	movl	%edx, 260(%r12)
	leal	(%rax,%rdx), %ecx
	cmpl	$2, %ecx
	ja	.L55
.L58:
	cmpl	$261, %edx
	ja	.L51
	movq	(%r12), %rax
	movl	8(%rax), %r8d
	testl	%r8d, %r8d
	je	.L51
	movl	160(%r12), %ecx
.L60:
	movq	184(%r12), %rsi
	movl	252(%r12), %eax
	addl	-52(%rbp), %ecx
	movl	%esi, %r14d
	subl	%eax, %r14d
	subl	%edx, %r14d
	cmpl	%ecx, %eax
	jb	.L42
	movq	176(%r12), %rdi
	movq	-64(%rbp), %r15
	addl	%ebx, %eax
	subl	%esi, %eax
	addl	%eax, %edx
	leaq	(%rdi,%r15), %rsi
	call	memcpy@PLT
	movl	212(%r12), %edi
	movl	252(%r12), %eax
	movq	200(%r12), %rdx
	subl	%ebx, 256(%r12)
	movq	%rdi, %rsi
	subl	%ebx, %eax
	subq	%r15, 232(%r12)
	movl	160(%r12), %ecx
	subl	$1, %esi
	movl	%eax, 252(%r12)
	leaq	(%rdx,%rdi,2), %rdx
	notq	%rsi
	leaq	(%rdx,%rsi,2), %rdi
	.p2align 4,,10
	.p2align 3
.L46:
	movzwl	-2(%rdx), %esi
	subq	$2, %rdx
	cmpl	%esi, %ecx
	ja	.L43
	subl	%ecx, %esi
	movw	%si, (%rdx)
	cmpq	%rdx, %rdi
	jne	.L46
.L45:
	movq	192(%r12), %rsi
	movl	%ecx, %edx
	leaq	(%rsi,%rdx,2), %rdx
	leal	-1(%rcx), %esi
	notq	%rsi
	leaq	(%rdx,%rsi,2), %rdi
	.p2align 4,,10
	.p2align 3
.L50:
	movzwl	-2(%rdx), %esi
	subq	$2, %rdx
	cmpl	%esi, %ecx
	ja	.L47
	subl	%ecx, %esi
	movw	%si, (%rdx)
	cmpq	%rdx, %rdi
	jne	.L50
	addl	%ebx, %r14d
.L86:
	movq	(%r12), %r15
	movl	8(%r15), %ecx
	testl	%ecx, %ecx
	jne	.L83
	.p2align 4,,10
	.p2align 3
.L51:
	movq	6024(%r12), %rdi
	movq	184(%r12), %rax
	cmpq	%rax, %rdi
	jnb	.L40
	movl	252(%r12), %esi
	movl	260(%r12), %edx
	addq	%rdx, %rsi
	cmpq	%rsi, %rdi
	jb	.L84
	leaq	258(%rsi), %rdx
	cmpq	%rdx, %rdi
	jb	.L85
.L40:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L47:
	.cfi_restore_state
	xorl	%r9d, %r9d
	movw	%r9w, (%rdx)
	cmpq	%rdx, %rdi
	jne	.L50
	addl	%ebx, %r14d
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L43:
	xorl	%r10d, %r10d
	movw	%r10w, (%rdx)
	cmpq	%rdx, %rdi
	jne	.L46
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L81:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	call	copy_with_crc
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L55:
	movl	252(%r12), %edi
	movq	176(%r12), %r8
	movl	224(%r12), %ecx
	movl	220(%r12), %r9d
	movl	%edi, %esi
	leal	(%rdi,%rdx), %r11d
	subl	%eax, %esi
	movl	%esi, %eax
	movzbl	(%r8,%rax), %r10d
	leal	1(%rsi), %eax
	movl	%r10d, 208(%r12)
	movzbl	(%r8,%rax), %eax
	sall	%cl, %r10d
	xorl	%eax, %r10d
	movl	%r10d, %eax
	andl	%r9d, %eax
	movl	%eax, 208(%r12)
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L59:
	leal	2(%rsi), %r10d
	sall	%cl, %eax
	movl	%esi, %r14d
	andl	168(%r12), %r14d
	movzbl	(%r8,%r10), %r10d
	xorl	%r10d, %eax
	movq	200(%r12), %r10
	andl	%r9d, %eax
	movl	%eax, %r13d
	movl	%eax, 208(%r12)
	leaq	(%r10,%r13,2), %r10
	movq	192(%r12), %r13
	movzwl	(%r10), %r15d
	movw	%r15w, 0(%r13,%r14,2)
	movw	%si, (%r10)
	addl	$1, %esi
	movl	%edi, %r10d
	subl	%esi, %r10d
	movl	%r10d, 6012(%r12)
	movl	%r11d, %r10d
	subl	%esi, %r10d
	cmpl	$2, %r10d
	jbe	.L58
.L57:
	cmpl	%esi, %edi
	jne	.L59
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L82:
	movq	96(%r15), %rdi
	movl	%r14d, %edx
	movq	%r8, %rsi
	call	adler32@PLT
	movq	%rax, 96(%r15)
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L80:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	fill_window_sse
	.p2align 4,,10
	.p2align 3
.L85:
	.cfi_restore_state
	subq	%rdi, %rsi
	subq	%rdi, %rax
	leaq	258(%rsi), %rbx
	cmpq	%rbx, %rax
	cmovbe	%rax, %rbx
	addq	176(%r12), %rdi
	xorl	%esi, %esi
	movl	%ebx, %edx
	call	memset@PLT
	addq	%rbx, 6024(%r12)
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L84:
	subq	%rsi, %rax
	movl	$258, %edx
	cmpq	$258, %rax
	cmovbe	%rax, %rdx
	movq	176(%r12), %rax
	addq	%rsi, %rax
	cmpl	$8, %edx
	jnb	.L63
	testb	$4, %dl
	jne	.L87
	testl	%edx, %edx
	je	.L64
	movb	$0, (%rax)
	testb	$2, %dl
	je	.L64
	movl	%edx, %ecx
	xorl	%edi, %edi
	movw	%di, -2(%rax,%rcx)
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L63:
	leaq	8(%rax), %rdi
	movl	%edx, %ecx
	movq	$0, (%rax)
	movq	$0, -8(%rax,%rcx)
	andq	$-8, %rdi
	subq	%rdi, %rax
	leal	(%rdx,%rax), %ecx
	xorl	%eax, %eax
	shrl	$3, %ecx
	rep stosq
.L64:
	addq	%rsi, %rdx
	movq	%rdx, 6024(%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L87:
	.cfi_restore_state
	movl	%edx, %ecx
	movl	$0, (%rax)
	movl	$0, -4(%rax,%rcx)
	jmp	.L64
	.cfi_endproc
.LFE696:
	.size	fill_window, .-fill_window
	.p2align 4
	.type	deflate_stored, @function
deflate_stored:
.LFB698:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	24(%rdi), %rax
	movq	(%rdi), %rcx
	movl	%esi, -60(%rbp)
	leaq	-5(%rax), %r14
	movl	160(%rdi), %eax
	cmpq	%rax, %r14
	cmovbe	%r14, %rax
	movq	%rax, -72(%rbp)
	movl	8(%rcx), %eax
	movl	%eax, -84(%rbp)
	movl	%eax, %edx
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L96:
	movl	8(%rcx), %edx
	testl	%r12d, %r12d
	jne	.L173
.L97:
	testl	%r13d, %r13d
	jne	.L174
.L101:
	movl	6020(%rbx), %eax
	movl	32(%rcx), %r12d
	addl	$42, %eax
	sarl	$3, %eax
	cmpl	%eax, %r12d
	jb	.L133
	movl	252(%rbx), %r8d
	subl	%eax, %r12d
	movl	%edx, %esi
	movl	%r8d, %r15d
	subl	232(%rbx), %r15d
	movl	%r15d, %eax
	addq	%rsi, %rax
	leal	(%r15,%rdx), %esi
	cmpq	$65535, %rax
	movl	$65535, %eax
	cmovb	%esi, %eax
	cmpl	%r12d, %eax
	cmovbe	%eax, %r12d
	cmpl	%r12d, -72(%rbp)
	jbe	.L91
	movl	-60(%rbp), %r11d
	testl	%r12d, %r12d
	sete	%dil
	cmpl	$4, %r11d
	setne	%al
	testb	%al, %dil
	jne	.L133
	testl	%r11d, %r11d
	je	.L133
	cmpl	%r12d, %esi
	jne	.L133
.L91:
	xorl	%r13d, %r13d
	cmpl	$4, -60(%rbp)
	jne	.L92
	addl	%r15d, %edx
	xorl	%r13d, %r13d
	cmpl	%r12d, %edx
	sete	%r13b
.L92:
	xorl	%edx, %edx
	xorl	%esi, %esi
	movl	%r13d, %ecx
	movq	%rbx, %rdi
	call	_tr_stored_block
	movq	16(%rbx), %rdx
	movq	40(%rbx), %rax
	movl	%r12d, %ecx
	movb	%r12b, -4(%rdx,%rax)
	movq	16(%rbx), %rdx
	movq	40(%rbx), %rax
	movb	%ch, -3(%rdx,%rax)
	movq	16(%rbx), %rdx
	notl	%ecx
	movq	40(%rbx), %rax
	movb	%cl, -2(%rdx,%rax)
	movl	%r12d, %ecx
	movq	40(%rbx), %rax
	movq	16(%rbx), %rdx
	notl	%ecx
	movb	%ch, -1(%rdx,%rax)
	movq	(%rbx), %rcx
	movq	56(%rcx), %r14
	movq	%rcx, -56(%rbp)
	movq	%r14, %rdi
	call	_tr_flush_bits
	movq	40(%r14), %rax
	movq	-56(%rbp), %rcx
	movl	%eax, %r9d
	cmpl	%eax, 32(%rcx)
	cmovbe	32(%rcx), %r9d
	testl	%r9d, %r9d
	je	.L94
	movq	24(%rcx), %rdi
	movq	32(%r14), %rsi
	movl	%r9d, %edx
	movl	%r9d, -64(%rbp)
	movq	%rcx, -80(%rbp)
	movq	%rdx, -56(%rbp)
	call	memcpy@PLT
	movq	-80(%rbp), %rcx
	movq	-56(%rbp), %rdx
	movl	-64(%rbp), %r9d
	addq	%rdx, 24(%rcx)
	addq	%rdx, 32(%r14)
	addq	%rdx, 40(%rcx)
	subl	%r9d, 32(%rcx)
	subq	%rdx, 40(%r14)
	jne	.L94
	movq	16(%r14), %rax
	movq	%rax, 32(%r14)
.L94:
	movq	(%rbx), %rcx
	testl	%r15d, %r15d
	je	.L96
	cmpl	%r12d, %r15d
	movl	%r15d, %r8d
	movq	24(%rcx), %rdi
	movq	232(%rbx), %rsi
	cmova	%r12d, %r8d
	addq	176(%rbx), %rsi
	movl	%r8d, %edx
	movl	%r8d, -80(%rbp)
	movq	%rdx, -56(%rbp)
	call	memcpy@PLT
	movl	-80(%rbp), %r8d
	movq	(%rbx), %rcx
	movq	-56(%rbp), %rdx
	subl	%r8d, %r12d
	addq	%rdx, 24(%rcx)
	addq	%rdx, 40(%rcx)
	subl	%r8d, 32(%rcx)
	addq	%rdx, 232(%rbx)
	movl	8(%rcx), %edx
	testl	%r12d, %r12d
	je	.L97
.L173:
	cmpl	%edx, %r12d
	movl	%edx, %r9d
	movq	24(%rcx), %r8
	cmovbe	%r12d, %r9d
	testl	%r9d, %r9d
	je	.L98
	movq	56(%rcx), %rax
	subl	%r9d, %edx
	movl	%r9d, %r10d
	movl	%edx, 8(%rcx)
	cmpl	$2, 48(%rax)
	je	.L175
	movq	(%rcx), %rsi
	movq	%r10, %rdx
	movq	%r8, %rdi
	movl	%r9d, -64(%rbp)
	movq	%rcx, -80(%rbp)
	movq	%r10, -56(%rbp)
	call	memcpy@PLT
	movq	-80(%rbp), %rcx
	movq	-56(%rbp), %r10
	movq	%rax, %r8
	movl	-64(%rbp), %r9d
	movq	56(%rcx), %rax
	cmpl	$1, 48(%rax)
	je	.L176
.L100:
	addq	%r10, (%rcx)
	addq	%r10, 16(%rcx)
	movq	(%rbx), %rcx
	movq	24(%rcx), %r8
	movl	8(%rcx), %edx
.L98:
	movl	%r12d, %eax
	subl	%r12d, 32(%rcx)
	addq	%rax, %r8
	addq	%rax, 40(%rcx)
	movq	%r8, 24(%rcx)
	testl	%r13d, %r13d
	je	.L101
.L174:
	movl	-84(%rbp), %r12d
	movl	$1, %r14d
	subl	%edx, %r12d
	jne	.L102
.L181:
	movl	252(%rbx), %edx
.L103:
	movl	%edx, %ecx
	movq	%rcx, %rsi
	cmpq	%rcx, 6024(%rbx)
	jnb	.L108
	movq	%rcx, 6024(%rbx)
.L108:
	testl	%r14d, %r14d
	jne	.L135
	movq	(%rbx), %r15
	movl	8(%r15), %eax
	testl	$-5, -60(%rbp)
	je	.L110
	testl	%eax, %eax
	je	.L177
.L110:
	movl	%edx, %r12d
	notl	%r12d
	addl	184(%rbx), %r12d
	cmpl	%eax, %r12d
	jnb	.L112
	movq	232(%rbx), %rcx
	movl	160(%rbx), %r8d
	cmpq	%r8, %rcx
	jge	.L178
.L112:
	cmpl	%eax, %r12d
	cmova	%eax, %r12d
	testl	%r12d, %r12d
	jne	.L179
.L111:
	cmpq	%rsi, 6024(%rbx)
	jnb	.L117
	movq	%rsi, 6024(%rbx)
.L117:
	movl	6020(%rbx), %eax
	movq	24(%rbx), %rdi
	movl	$65535, %ecx
	movq	232(%rbx), %rsi
	addl	$42, %eax
	sarl	$3, %eax
	subq	%rax, %rdi
	cmpq	$65535, %rdi
	movq	%rdi, %rax
	cmova	%rcx, %rax
	subl	%esi, %edx
	cmpl	%eax, 160(%rbx)
	movl	%eax, %ecx
	cmovbe	160(%rbx), %ecx
	movl	%eax, %edi
	cmpl	%edx, %ecx
	jbe	.L118
	movl	-60(%rbp), %r10d
	testl	%edx, %edx
	setne	%cl
	cmpl	$4, %r10d
	sete	%al
	orb	%al, %cl
	je	.L88
	testl	%r10d, %r10d
	jne	.L180
.L88:
	addq	$56, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L175:
	.cfi_restore_state
	movq	%r10, %rdx
	movq	%rcx, %rdi
	movq	%r8, %rsi
	movq	%r10, -80(%rbp)
	movq	%rcx, -56(%rbp)
	call	copy_with_crc
	movq	-56(%rbp), %rcx
	movq	-80(%rbp), %r10
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L133:
	movl	-84(%rbp), %r12d
	xorl	%r14d, %r14d
	subl	%edx, %r12d
	je	.L181
.L102:
	movl	160(%rbx), %eax
	movq	176(%rbx), %rdi
	cmpl	%r12d, %eax
	jbe	.L182
	movl	252(%rbx), %r8d
	movq	184(%rbx), %rsi
	movl	%r12d, %r15d
	subq	%r8, %rsi
	movq	%r8, %rdx
	cmpq	%r15, %rsi
	jbe	.L183
.L106:
	movq	(%rcx), %rsi
	movq	%r15, %rdx
	addq	%r8, %rdi
	subq	%r15, %rsi
	call	memcpy@PLT
	movl	252(%rbx), %edx
	movl	160(%rbx), %eax
	addl	%r12d, %edx
	movl	%edx, 252(%rbx)
.L105:
	movl	6012(%rbx), %ecx
	movl	%edx, %edi
	movq	%rdi, 232(%rbx)
	subl	%ecx, %eax
	cmpl	%r12d, %eax
	cmova	%r12d, %eax
	addl	%ecx, %eax
	movl	%eax, 6012(%rbx)
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L118:
	cmpl	%edx, %eax
	cmova	%edx, %eax
	cmpl	$4, -60(%rbp)
	je	.L184
.L124:
	xorl	%r15d, %r15d
.L125:
	movl	%eax, %r12d
	addq	176(%rbx), %rsi
	movq	%rbx, %rdi
	movl	%r15d, %ecx
	movq	%r12, %rdx
	call	_tr_stored_block
	addq	%r12, 232(%rbx)
	movq	(%rbx), %rbx
	movq	56(%rbx), %r13
	movq	%r13, %rdi
	call	_tr_flush_bits
	movq	40(%r13), %rax
	cmpl	%eax, 32(%rbx)
	movl	%eax, %r12d
	cmovbe	32(%rbx), %r12d
	testl	%r12d, %r12d
	jne	.L185
.L128:
	testl	%r15d, %r15d
	movl	$2, %eax
	cmovne	%eax, %r14d
	jmp	.L88
	.p2align 4,,10
	.p2align 3
.L176:
	movq	96(%rcx), %rdi
	movl	%r9d, %edx
	movq	%r8, %rsi
	movq	%r10, -80(%rbp)
	movq	%rcx, -56(%rbp)
	call	adler32@PLT
	movq	-56(%rbp), %rcx
	movq	-80(%rbp), %r10
	movq	%rax, 96(%rcx)
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L179:
	cmpl	%eax, %r12d
	movl	%eax, %r8d
	cmovbe	%r12d, %r8d
	testl	%r8d, %r8d
	je	.L114
	subl	%r8d, %eax
	addq	176(%rbx), %rsi
	movl	%r8d, %ecx
	movl	%eax, 8(%r15)
	movq	56(%r15), %rax
	movq	%rsi, %r9
	cmpl	$2, 48(%rax)
	je	.L186
	movq	(%r15), %rsi
	movq	%rcx, %rdx
	movq	%r9, %rdi
	movl	%r8d, -72(%rbp)
	movq	%rcx, -56(%rbp)
	call	memcpy@PLT
	movq	-56(%rbp), %rcx
	movl	-72(%rbp), %r8d
	movq	%rax, %r9
	movq	56(%r15), %rax
	cmpl	$1, 48(%rax)
	je	.L187
.L116:
	addq	%rcx, (%r15)
	movl	252(%rbx), %edx
	addq	%rcx, 16(%r15)
.L114:
	leal	(%rdx,%r12), %esi
	movl	%esi, 252(%rbx)
	movq	%rsi, %rdx
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L135:
	movl	$3, %r14d
	jmp	.L88
	.p2align 4,,10
	.p2align 3
.L177:
	cmpq	232(%rbx), %rcx
	jne	.L111
	movl	$1, %r14d
	jmp	.L88
.L180:
	movq	(%rbx), %rax
	movl	8(%rax), %r8d
	testl	%r8d, %r8d
	jne	.L88
	cmpl	%edx, %edi
	jb	.L88
	movl	%edx, %eax
	movl	$1, %r15d
	cmovbe	%edi, %eax
	cmpl	$4, -60(%rbp)
	jne	.L124
	jmp	.L125
	.p2align 4,,10
	.p2align 3
.L185:
	movq	24(%rbx), %rdi
	movq	32(%r13), %rsi
	movl	%r12d, %edx
	movq	%rdx, -56(%rbp)
	call	memcpy@PLT
	movq	-56(%rbp), %rdx
	addq	%rdx, 24(%rbx)
	addq	%rdx, 32(%r13)
	addq	%rdx, 40(%rbx)
	subl	%r12d, 32(%rbx)
	subq	%rdx, 40(%r13)
	jne	.L128
	movq	16(%r13), %rax
	movq	%rax, 32(%r13)
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L182:
	movl	$2, 6008(%rbx)
	movq	(%rcx), %rsi
	movl	%eax, %edx
	subq	%rdx, %rsi
	call	memcpy@PLT
	movl	160(%rbx), %edx
	movl	%edx, 252(%rbx)
	movl	%edx, %eax
	jmp	.L105
.L184:
	movq	(%rbx), %rcx
	movl	8(%rcx), %ecx
	testl	%ecx, %ecx
	jne	.L124
	movl	$1, %r15d
	cmpl	%edx, %edi
	jb	.L124
	jmp	.L125
.L178:
	subq	%r8, %rcx
	subl	%r8d, %edx
	movq	176(%rbx), %rdi
	movq	%rcx, 232(%rbx)
	movl	%edx, 252(%rbx)
	leaq	(%rdi,%r8), %rsi
	call	memcpy@PLT
	movl	6008(%rbx), %eax
	cmpl	$1, %eax
	ja	.L113
	addl	$1, %eax
	movl	%eax, 6008(%rbx)
.L113:
	movq	(%rbx), %r15
	movl	252(%rbx), %esi
	addl	160(%rbx), %r12d
	movl	8(%r15), %eax
	movq	%rsi, %rdx
	jmp	.L112
.L183:
	subl	%eax, %edx
	leaq	(%rdi,%rax), %rsi
	movl	%edx, 252(%rbx)
	call	memcpy@PLT
	movl	6008(%rbx), %eax
	cmpl	$1, %eax
	ja	.L172
	addl	$1, %eax
	movl	%eax, 6008(%rbx)
.L172:
	movq	(%rbx), %rcx
	movq	176(%rbx), %rdi
	movl	252(%rbx), %r8d
	jmp	.L106
.L186:
	movq	%rcx, %rdx
	movq	%r15, %rdi
	movq	%rcx, -56(%rbp)
	call	copy_with_crc
	movq	-56(%rbp), %rcx
	jmp	.L116
.L187:
	movq	96(%r15), %rdi
	movl	%r8d, %edx
	movq	%r9, %rsi
	call	adler32@PLT
	movq	-56(%rbp), %rcx
	movq	%rax, 96(%r15)
	jmp	.L116
	.cfi_endproc
.LFE698:
	.size	deflate_stored, .-deflate_stored
	.p2align 4
	.type	deflate_fast, @function
deflate_fast:
.LFB699:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%esi, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	leaq	_dist_code(%rip), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	_length_code(%rip), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	jmp	.L208
	.p2align 4,,10
	.p2align 3
.L248:
	subw	256(%rbx), %dx
	movl	%esi, 5980(%rbx)
	subl	$3, %eax
	movb	%dl, (%rcx)
	movl	5980(%rbx), %ecx
	movq	5968(%rbx), %rsi
	leal	1(%rcx), %edi
	movl	%edi, 5980(%rbx)
	movl	%edx, %edi
	subl	$1, %edx
	shrw	$8, %di
	movb	%dil, (%rsi,%rcx)
	movl	5980(%rbx), %ecx
	movq	5968(%rbx), %rsi
	leal	1(%rcx), %edi
	movl	%edi, 5980(%rbx)
	movb	%al, (%rsi,%rcx)
	movzbl	%al, %eax
	movzbl	(%r12,%rax), %eax
	addw	$1, 1320(%rbx,%rax,4)
	cmpw	$255, %dx
	ja	.L200
	movzwl	%dx, %edx
	movzbl	0(%r13,%rdx), %eax
.L201:
	addw	$1, 2584(%rbx,%rax,4)
	movl	260(%rbx), %edx
	xorl	%r8d, %r8d
	movl	240(%rbx), %ecx
	movl	5984(%rbx), %eax
	cmpl	%eax, 5980(%rbx)
	movl	252(%rbx), %eax
	sete	%r8b
	subl	%ecx, %edx
	movl	%edx, 260(%rbx)
	cmpl	272(%rbx), %ecx
	ja	.L202
	cmpl	$2, %edx
	ja	.L244
.L202:
	movq	176(%rbx), %rdx
	addl	%eax, %ecx
	movl	$0, 240(%rbx)
	movl	%ecx, 252(%rbx)
	movq	%rcx, %rax
	movzbl	(%rdx,%rcx), %esi
	leal	1(%rcx), %ecx
	movl	%esi, 208(%rbx)
	movzbl	(%rdx,%rcx), %edx
	movl	224(%rbx), %ecx
	sall	%cl, %esi
	xorl	%esi, %edx
	andl	220(%rbx), %edx
	movl	%edx, 208(%rbx)
	testl	%r8d, %r8d
	jne	.L245
.L208:
	cmpl	$261, 260(%rbx)
	jbe	.L246
.L189:
	movl	252(%rbx), %edx
.L193:
	movl	x86_cpu_enable_simd(%rip), %r8d
	movzwl	%dx, %esi
	movzwl	%dx, %eax
	testl	%r8d, %r8d
	jne	.L247
	movq	176(%rbx), %rcx
	movl	208(%rbx), %edi
	andl	168(%rbx), %esi
	movzbl	2(%rcx,%rax), %eax
	movl	224(%rbx), %ecx
	sall	%cl, %edi
	movq	200(%rbx), %rcx
	xorl	%edi, %eax
	andl	220(%rbx), %eax
	movq	192(%rbx), %rdi
	leaq	(%rcx,%rax,2), %rcx
	movl	%eax, 208(%rbx)
	movzwl	(%rcx), %eax
	movw	%ax, (%rdi,%rsi,2)
	movw	%dx, (%rcx)
.L196:
	movzwl	%ax, %esi
	testl	%esi, %esi
	je	.L241
	movl	160(%rbx), %eax
	movl	%edx, %ecx
	subl	%esi, %ecx
	subl	$262, %eax
	cmpl	%eax, %ecx
	jbe	.L198
	.p2align 4,,10
	.p2align 3
.L241:
	movl	240(%rbx), %eax
.L194:
	movl	5980(%rbx), %ecx
	leal	1(%rcx), %esi
	addq	5968(%rbx), %rcx
	cmpl	$2, %eax
	ja	.L248
	movq	176(%rbx), %rax
	xorl	%r8d, %r8d
	movzbl	(%rax,%rdx), %eax
	movl	%esi, 5980(%rbx)
	movb	$0, (%rcx)
	movl	5980(%rbx), %edx
	movq	5968(%rbx), %rcx
	leal	1(%rdx), %esi
	movl	%esi, 5980(%rbx)
	movb	$0, (%rcx,%rdx)
	movl	5980(%rbx), %edx
	movq	5968(%rbx), %rcx
	leal	1(%rdx), %esi
	movl	%esi, 5980(%rbx)
	movb	%al, (%rcx,%rdx)
	addw	$1, 292(%rbx,%rax,4)
	movl	5984(%rbx), %eax
	cmpl	%eax, 5980(%rbx)
	movl	252(%rbx), %eax
	sete	%r8b
	subl	$1, 260(%rbx)
.L243:
	addl	$1, %eax
	movl	%eax, 252(%rbx)
	testl	%r8d, %r8d
	je	.L208
.L245:
	movq	232(%rbx), %rcx
	movl	%eax, %edx
	xorl	%esi, %esi
	subq	%rcx, %rdx
	testq	%rcx, %rcx
	js	.L209
	movq	176(%rbx), %rsi
	movl	%ecx, %ecx
	addq	%rcx, %rsi
.L209:
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	call	_tr_flush_block
	movq	(%rbx), %r15
	movl	252(%rbx), %eax
	movq	56(%r15), %r8
	movq	%rax, 232(%rbx)
	movq	%r8, %rdi
	movq	%r8, -56(%rbp)
	call	_tr_flush_bits
	movq	-56(%rbp), %r8
	movq	40(%r8), %rax
	cmpl	%eax, 32(%r15)
	movl	%eax, %ecx
	cmovbe	32(%r15), %ecx
	testl	%ecx, %ecx
	je	.L211
	movq	32(%r8), %rsi
	movq	24(%r15), %rdi
	movl	%ecx, %edx
	movl	%ecx, -68(%rbp)
	movq	%r8, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r8
	movq	-56(%rbp), %rdx
	movl	-68(%rbp), %ecx
	addq	%rdx, 24(%r15)
	addq	%rdx, 32(%r8)
	addq	%rdx, 40(%r15)
	subl	%ecx, 32(%r15)
	subq	%rdx, 40(%r8)
	jne	.L211
	movq	16(%r8), %rax
	movq	%rax, 32(%r8)
.L211:
	movq	(%rbx), %rax
	movl	32(%rax), %esi
	testl	%esi, %esi
	jne	.L208
.L213:
	xorl	%r8d, %r8d
	jmp	.L188
	.p2align 4,,10
	.p2align 3
.L246:
	movq	%rbx, %rdi
	call	fill_window
	movl	260(%rbx), %eax
	cmpl	$261, %eax
	ja	.L189
	testl	%r14d, %r14d
	je	.L213
	testl	%eax, %eax
	je	.L192
	movl	252(%rbx), %edx
	cmpl	$2, %eax
	jbe	.L241
	jmp	.L193
	.p2align 4,,10
	.p2align 3
.L200:
	shrw	$7, %dx
	leaq	256(%rdx), %rax
	andl	$1023, %eax
	movzbl	0(%r13,%rax), %eax
	jmp	.L201
	.p2align 4,,10
	.p2align 3
.L244:
	subl	$1, %ecx
	movl	%ecx, 240(%rbx)
	jmp	.L206
	.p2align 4,,10
	.p2align 3
.L203:
	movq	176(%rbx), %rcx
	movl	208(%rbx), %edi
	andl	168(%rbx), %esi
	movzbl	2(%rcx,%rdx), %edx
	movl	224(%rbx), %ecx
	sall	%cl, %edi
	movq	200(%rbx), %rcx
	xorl	%edi, %edx
	andl	220(%rbx), %edx
	movl	%edx, 208(%rbx)
	leaq	(%rcx,%rdx,2), %rdx
	movq	192(%rbx), %rcx
	movzwl	(%rdx), %edi
	movw	%di, (%rcx,%rsi,2)
	movw	%ax, (%rdx)
.L242:
	subl	$1, 240(%rbx)
	je	.L243
.L206:
	movl	x86_cpu_enable_simd(%rip), %edi
	addl	$1, %eax
	movl	%eax, 252(%rbx)
	movzwl	%ax, %esi
	movzwl	%ax, %edx
	testl	%edi, %edi
	je	.L203
	movq	%rbx, %rdi
	call	insert_string_optimized
	movl	252(%rbx), %eax
	jmp	.L242
	.p2align 4,,10
	.p2align 3
.L247:
	movq	%rbx, %rdi
	call	insert_string_optimized
	movl	252(%rbx), %edx
	jmp	.L196
	.p2align 4,,10
	.p2align 3
.L198:
	movq	%rbx, %rdi
	call	longest_match
	movl	252(%rbx), %edx
	movl	%eax, 240(%rbx)
	jmp	.L194
	.p2align 4,,10
	.p2align 3
.L192:
	movl	252(%rbx), %eax
	movl	$2, %edx
	cmpl	$2, %eax
	cmovbe	%eax, %edx
	movl	%edx, 6012(%rbx)
	cmpl	$4, %r14d
	je	.L249
	movl	5980(%rbx), %edx
	movl	$1, %r8d
	testl	%edx, %edx
	je	.L188
	movq	232(%rbx), %rcx
	movl	%eax, %edx
	xorl	%esi, %esi
	subq	%rcx, %rdx
	testq	%rcx, %rcx
	js	.L220
	movq	176(%rbx), %rsi
	movl	%ecx, %ecx
	addq	%rcx, %rsi
.L220:
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	call	_tr_flush_block
	movq	(%rbx), %r12
	movl	252(%rbx), %eax
	movq	56(%r12), %r14
	movq	%rax, 232(%rbx)
	movq	%r14, %rdi
	call	_tr_flush_bits
	movq	40(%r14), %rax
	cmpl	%eax, 32(%r12)
	movl	%eax, %r13d
	cmovbe	32(%r12), %r13d
	testl	%r13d, %r13d
	jne	.L250
.L222:
	movq	(%rbx), %rax
	xorl	%r8d, %r8d
	movl	32(%rax), %eax
	testl	%eax, %eax
	setne	%r8b
	jmp	.L188
.L249:
	movq	232(%rbx), %rcx
	movl	%eax, %edx
	xorl	%esi, %esi
	subq	%rcx, %rdx
	testq	%rcx, %rcx
	js	.L216
	movq	176(%rbx), %rsi
	movl	%ecx, %ecx
	addq	%rcx, %rsi
.L216:
	movl	$1, %ecx
	movq	%rbx, %rdi
	call	_tr_flush_block
	movq	(%rbx), %r12
	movl	252(%rbx), %eax
	movq	56(%r12), %r13
	movq	%rax, 232(%rbx)
	movq	%r13, %rdi
	call	_tr_flush_bits
	movq	40(%r13), %r14
	cmpl	%r14d, 32(%r12)
	cmovbe	32(%r12), %r14d
	testl	%r14d, %r14d
	jne	.L251
.L218:
	movq	(%rbx), %rax
	xorl	%r8d, %r8d
	movl	32(%rax), %ecx
	testl	%ecx, %ecx
	setne	%r8b
	addl	$2, %r8d
.L188:
	addq	$40, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L251:
	.cfi_restore_state
	movl	%r14d, %r15d
	movq	24(%r12), %rdi
	movq	32(%r13), %rsi
	movq	%r15, %rdx
	call	memcpy@PLT
	addq	%r15, 24(%r12)
	addq	%r15, 32(%r13)
	addq	%r15, 40(%r12)
	subl	%r14d, 32(%r12)
	subq	%r15, 40(%r13)
	jne	.L218
	movq	16(%r13), %rax
	movq	%rax, 32(%r13)
	jmp	.L218
.L250:
	movl	%r13d, %r15d
	movq	24(%r12), %rdi
	movq	32(%r14), %rsi
	movq	%r15, %rdx
	call	memcpy@PLT
	addq	%r15, 24(%r12)
	addq	%r15, 32(%r14)
	addq	%r15, 40(%r12)
	subl	%r13d, 32(%r12)
	subq	%r15, 40(%r14)
	jne	.L222
	movq	16(%r14), %rax
	movq	%rax, 32(%r14)
	jmp	.L222
	.cfi_endproc
.LFE699:
	.size	deflate_fast, .-deflate_fast
	.p2align 4
	.type	deflate_slow, @function
deflate_slow:
.LFB700:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	_length_code(%rip), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movl	260(%rdi), %eax
	.p2align 4,,10
	.p2align 3
.L253:
	cmpl	$261, %eax
	jbe	.L307
.L254:
	movl	252(%rbx), %edx
.L258:
	movl	x86_cpu_enable_simd(%rip), %r13d
	movzwl	%dx, %r8d
	movzwl	%dx, %eax
	testl	%r13d, %r13d
	jne	.L308
	movq	176(%rbx), %rcx
	movl	208(%rbx), %edi
	andl	168(%rbx), %r8d
	movzbl	2(%rcx,%rax), %eax
	movl	224(%rbx), %ecx
	sall	%cl, %edi
	movq	200(%rbx), %rcx
	xorl	%edi, %eax
	andl	220(%rbx), %eax
	movl	%eax, 208(%rbx)
	leaq	(%rcx,%rax,2), %rax
	movq	192(%rbx), %rcx
	movzwl	(%rax), %esi
	movw	%si, (%rcx,%r8,2)
	movw	%dx, (%rax)
.L261:
	movl	240(%rbx), %ecx
	movl	256(%rbx), %eax
	movl	$2, 240(%rbx)
	movl	%ecx, 264(%rbx)
	movl	%eax, 244(%rbx)
	testl	%esi, %esi
	je	.L294
	cmpl	272(%rbx), %ecx
	jnb	.L294
	movl	160(%rbx), %eax
	movl	%edx, %r8d
	subl	%esi, %r8d
	leal	-262(%rax), %edi
	movl	$2, %eax
	cmpl	%edi, %r8d
	jbe	.L309
	.p2align 4,,10
	.p2align 3
.L259:
	cmpl	$2, %ecx
	jbe	.L265
	cmpl	%eax, %ecx
	jnb	.L264
.L265:
	movl	248(%rbx), %r9d
	testl	%r9d, %r9d
	je	.L278
	leal	-1(%rdx), %eax
	movq	176(%rbx), %rdx
	movq	5968(%rbx), %rcx
	movzbl	(%rdx,%rax), %eax
	movl	5980(%rbx), %edx
	leal	1(%rdx), %esi
	movl	%esi, 5980(%rbx)
	movb	$0, (%rcx,%rdx)
	movl	5980(%rbx), %edx
	movq	5968(%rbx), %rcx
	leal	1(%rdx), %esi
	movl	%esi, 5980(%rbx)
	movb	$0, (%rcx,%rdx)
	movl	5980(%rbx), %edx
	movq	5968(%rbx), %rcx
	leal	1(%rdx), %esi
	movl	%esi, 5980(%rbx)
	movb	%al, (%rcx,%rdx)
	addw	$1, 292(%rbx,%rax,4)
	movl	5984(%rbx), %eax
	cmpl	%eax, 5980(%rbx)
	je	.L310
.L280:
	movq	(%rbx), %rdx
	movl	260(%rbx), %eax
	addl	$1, 252(%rbx)
	movl	32(%rdx), %r8d
	subl	$1, %eax
	movl	%eax, 260(%rbx)
	testl	%r8d, %r8d
	jne	.L253
.L276:
	xorl	%eax, %eax
.L252:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L278:
	.cfi_restore_state
	movl	260(%rbx), %eax
	addl	$1, %edx
	movl	$1, 248(%rbx)
	movl	%edx, 252(%rbx)
	subl	$1, %eax
	movl	%eax, 260(%rbx)
	cmpl	$261, %eax
	ja	.L254
.L307:
	movq	%rbx, %rdi
	call	fill_window
	movl	260(%rbx), %eax
	cmpl	$261, %eax
	ja	.L254
	testl	%r12d, %r12d
	je	.L276
	testl	%eax, %eax
	je	.L257
	movl	252(%rbx), %edx
	cmpl	$2, %eax
	ja	.L258
	movl	240(%rbx), %ecx
	movl	256(%rbx), %eax
	movl	$2, 240(%rbx)
	movl	%eax, 244(%rbx)
	movl	$2, %eax
	movl	%ecx, 264(%rbx)
	jmp	.L259
	.p2align 4,,10
	.p2align 3
.L294:
	movl	$2, %eax
	jmp	.L259
	.p2align 4,,10
	.p2align 3
.L310:
	movq	232(%rbx), %rsi
	movl	252(%rbx), %edx
	xorl	%r8d, %r8d
	subq	%rsi, %rdx
	testq	%rsi, %rsi
	js	.L281
	movl	%esi, %esi
	addq	176(%rbx), %rsi
	movq	%rsi, %r8
.L281:
	xorl	%ecx, %ecx
	movq	%r8, %rsi
	movq	%rbx, %rdi
	call	_tr_flush_block
	movq	(%rbx), %r15
	movl	252(%rbx), %eax
	movq	56(%r15), %r13
	movq	%rax, 232(%rbx)
	movq	%r13, %rdi
	call	_tr_flush_bits
	movq	40(%r13), %rax
	cmpl	%eax, 32(%r15)
	movl	%eax, %ecx
	cmovbe	32(%r15), %ecx
	testl	%ecx, %ecx
	je	.L280
	movq	24(%r15), %rdi
	movq	32(%r13), %rsi
	movl	%ecx, %edx
	movl	%ecx, -60(%rbp)
	movq	%rdx, -56(%rbp)
	call	memcpy@PLT
	movq	-56(%rbp), %rdx
	movl	-60(%rbp), %ecx
	addq	%rdx, 24(%r15)
	addq	%rdx, 32(%r13)
	addq	%rdx, 40(%r15)
	subl	%ecx, 32(%r15)
	subq	%rdx, 40(%r13)
	jne	.L280
	movq	16(%r13), %rax
	movq	%rax, 32(%r13)
	jmp	.L280
	.p2align 4,,10
	.p2align 3
.L262:
	cmpl	$1, 280(%rbx)
	movl	252(%rbx), %edx
	movl	264(%rbx), %ecx
	je	.L263
	cmpl	$3, %eax
	jne	.L259
	movl	%edx, %esi
	subl	256(%rbx), %esi
	cmpl	$4096, %esi
	jbe	.L259
.L263:
	movl	$2, 240(%rbx)
	cmpl	$2, %ecx
	jbe	.L265
	.p2align 4,,10
	.p2align 3
.L264:
	movl	260(%rbx), %eax
	movq	5968(%rbx), %rdi
	subl	$3, %ecx
	leal	-3(%rdx,%rax), %r8d
	movl	5980(%rbx), %eax
	subw	244(%rbx), %dx
	leal	-1(%rdx), %esi
	subl	$2, %edx
	leal	1(%rax), %r9d
	movl	%r9d, 5980(%rbx)
	movb	%sil, (%rdi,%rax)
	movl	5980(%rbx), %eax
	shrw	$8, %si
	movq	5968(%rbx), %rdi
	leal	1(%rax), %r9d
	movl	%r9d, 5980(%rbx)
	movb	%sil, (%rdi,%rax)
	movl	5980(%rbx), %eax
	movq	5968(%rbx), %rsi
	leal	1(%rax), %edi
	movl	%edi, 5980(%rbx)
	movb	%cl, (%rsi,%rax)
	movzbl	%cl, %ecx
	movzbl	(%r14,%rcx), %eax
	addw	$1, 1320(%rbx,%rax,4)
	cmpw	$255, %dx
	ja	.L266
	movzwl	%dx, %edx
	leaq	_dist_code(%rip), %rax
	movzbl	(%rax,%rdx), %eax
.L267:
	addw	$1, 2584(%rbx,%rax,4)
	movl	260(%rbx), %eax
	movl	264(%rbx), %edx
	movl	5980(%rbx), %r10d
	addl	$1, %eax
	movl	5984(%rbx), %r9d
	subl	%edx, %eax
	subl	$2, %edx
	movl	%eax, 260(%rbx)
	movl	252(%rbx), %eax
	movl	%edx, 264(%rbx)
	jmp	.L270
	.p2align 4,,10
	.p2align 3
.L268:
	subl	$1, %edx
	movl	%edx, 264(%rbx)
	je	.L311
.L270:
	addl	$1, %eax
	movl	%eax, 252(%rbx)
	cmpl	%r8d, %eax
	ja	.L268
	movl	x86_cpu_enable_simd(%rip), %r11d
	movzwl	%ax, %esi
	movzwl	%ax, %ecx
	testl	%r11d, %r11d
	jne	.L312
	movq	176(%rbx), %rdi
	movl	208(%rbx), %r11d
	andl	168(%rbx), %esi
	movzbl	2(%rdi,%rcx), %edi
	movl	224(%rbx), %ecx
	sall	%cl, %r11d
	movl	%r11d, %ecx
	movq	192(%rbx), %r11
	xorl	%edi, %ecx
	movq	200(%rbx), %rdi
	andl	220(%rbx), %ecx
	subl	$1, %edx
	movl	%ecx, 208(%rbx)
	leaq	(%rdi,%rcx,2), %rcx
	movzwl	(%rcx), %edi
	movw	%di, (%r11,%rsi,2)
	movw	%ax, (%rcx)
	movl	%edx, 264(%rbx)
	jne	.L270
.L311:
	movl	$0, 248(%rbx)
	addl	$1, %eax
	movl	$2, 240(%rbx)
	movl	%eax, 252(%rbx)
	cmpl	%r9d, %r10d
	je	.L313
.L271:
	movl	260(%rbx), %eax
	jmp	.L253
	.p2align 4,,10
	.p2align 3
.L309:
	movq	%rbx, %rdi
	call	longest_match
	movl	%eax, 240(%rbx)
	cmpl	$5, %eax
	jbe	.L262
	movl	264(%rbx), %ecx
	movl	252(%rbx), %edx
	jmp	.L259
	.p2align 4,,10
	.p2align 3
.L312:
	movq	%rbx, %rdi
	call	insert_string_optimized
	movl	264(%rbx), %edx
	movl	252(%rbx), %eax
	jmp	.L268
	.p2align 4,,10
	.p2align 3
.L266:
	shrw	$7, %dx
	leaq	_dist_code(%rip), %rdi
	leaq	256(%rdx), %rax
	andl	$1023, %eax
	movzbl	(%rdi,%rax), %eax
	jmp	.L267
	.p2align 4,,10
	.p2align 3
.L308:
	movl	%r8d, %esi
	movq	%rbx, %rdi
	call	insert_string_optimized
	movl	252(%rbx), %edx
	movzwl	%ax, %esi
	jmp	.L261
	.p2align 4,,10
	.p2align 3
.L313:
	movq	232(%rbx), %rcx
	movl	%eax, %edx
	xorl	%esi, %esi
	subq	%rcx, %rdx
	testq	%rcx, %rcx
	js	.L272
	movq	176(%rbx), %rsi
	movl	%ecx, %ecx
	addq	%rcx, %rsi
.L272:
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	call	_tr_flush_block
	movq	(%rbx), %r15
	movl	252(%rbx), %eax
	movq	56(%r15), %r13
	movq	%rax, 232(%rbx)
	movq	%r13, %rdi
	call	_tr_flush_bits
	movq	40(%r13), %rax
	cmpl	%eax, 32(%r15)
	movl	%eax, %ecx
	cmovbe	32(%r15), %ecx
	testl	%ecx, %ecx
	je	.L274
	movq	24(%r15), %rdi
	movq	32(%r13), %rsi
	movl	%ecx, %edx
	movl	%ecx, -60(%rbp)
	movq	%rdx, -56(%rbp)
	call	memcpy@PLT
	movq	-56(%rbp), %rdx
	movl	-60(%rbp), %ecx
	addq	%rdx, 24(%r15)
	addq	%rdx, 32(%r13)
	addq	%rdx, 40(%r15)
	subl	%ecx, 32(%r15)
	subq	%rdx, 40(%r13)
	jne	.L274
	movq	16(%r13), %rax
	movq	%rax, 32(%r13)
.L274:
	movq	(%rbx), %rax
	movl	32(%rax), %r10d
	testl	%r10d, %r10d
	jne	.L271
	jmp	.L276
.L257:
	movl	248(%rbx), %edi
	movl	252(%rbx), %edx
	testl	%edi, %edi
	jne	.L314
.L283:
	cmpl	$2, %edx
	movl	$2, %eax
	cmovbe	%edx, %eax
	movl	%eax, 6012(%rbx)
	cmpl	$4, %r12d
	je	.L315
	movl	5980(%rbx), %ecx
	movl	$1, %eax
	testl	%ecx, %ecx
	je	.L252
	movq	232(%rbx), %rax
	xorl	%esi, %esi
	subq	%rax, %rdx
	testq	%rax, %rax
	js	.L289
	movl	%eax, %eax
	addq	176(%rbx), %rax
	movq	%rax, %rsi
.L289:
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	call	_tr_flush_block
	movq	(%rbx), %r12
	movl	252(%rbx), %eax
	movq	56(%r12), %r13
	movq	%rax, 232(%rbx)
	movq	%r13, %rdi
	call	_tr_flush_bits
	movq	40(%r13), %r14
	cmpl	%r14d, 32(%r12)
	cmovbe	32(%r12), %r14d
	testl	%r14d, %r14d
	je	.L291
	movl	%r14d, %r15d
	movq	24(%r12), %rdi
	movq	32(%r13), %rsi
	movq	%r15, %rdx
	call	memcpy@PLT
	addq	%r15, 24(%r12)
	addq	%r15, 32(%r13)
	addq	%r15, 40(%r12)
	subl	%r14d, 32(%r12)
	subq	%r15, 40(%r13)
	jne	.L291
	movq	16(%r13), %rax
	movq	%rax, 32(%r13)
.L291:
	movq	(%rbx), %rax
	movl	32(%rax), %eax
	testl	%eax, %eax
	setne	%al
	addq	$24, %rsp
	popq	%rbx
	movzbl	%al, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L314:
	.cfi_restore_state
	leal	-1(%rdx), %eax
	movq	176(%rbx), %rdx
	movq	5968(%rbx), %rcx
	movzbl	(%rdx,%rax), %eax
	movl	5980(%rbx), %edx
	leal	1(%rdx), %esi
	movl	%esi, 5980(%rbx)
	movb	$0, (%rcx,%rdx)
	movl	5980(%rbx), %edx
	movq	5968(%rbx), %rcx
	leal	1(%rdx), %esi
	movl	%esi, 5980(%rbx)
	movb	$0, (%rcx,%rdx)
	movl	5980(%rbx), %edx
	movq	5968(%rbx), %rcx
	leal	1(%rdx), %esi
	movl	%esi, 5980(%rbx)
	movb	%al, (%rcx,%rdx)
	addw	$1, 292(%rbx,%rax,4)
	movl	252(%rbx), %edx
	movl	$0, 248(%rbx)
	jmp	.L283
.L315:
	movq	232(%rbx), %rax
	xorl	%esi, %esi
	subq	%rax, %rdx
	testq	%rax, %rax
	js	.L285
	movl	%eax, %eax
	addq	176(%rbx), %rax
	movq	%rax, %rsi
.L285:
	movl	$1, %ecx
	movq	%rbx, %rdi
	call	_tr_flush_block
	movq	(%rbx), %r12
	movl	252(%rbx), %eax
	movq	56(%r12), %r13
	movq	%rax, 232(%rbx)
	movq	%r13, %rdi
	call	_tr_flush_bits
	movq	40(%r13), %r14
	cmpl	%r14d, 32(%r12)
	cmovbe	32(%r12), %r14d
	testl	%r14d, %r14d
	je	.L287
	movl	%r14d, %r15d
	movq	24(%r12), %rdi
	movq	32(%r13), %rsi
	movq	%r15, %rdx
	call	memcpy@PLT
	addq	%r15, 24(%r12)
	addq	%r15, 32(%r13)
	addq	%r15, 40(%r12)
	subl	%r14d, 32(%r12)
	subq	%r15, 40(%r13)
	jne	.L287
	movq	16(%r13), %rax
	movq	%rax, 32(%r13)
.L287:
	movq	(%rbx), %rax
	movl	32(%rax), %esi
	xorl	%eax, %eax
	testl	%esi, %esi
	setne	%al
	addq	$24, %rsp
	popq	%rbx
	addl	$2, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE700:
	.size	deflate_slow, .-deflate_slow
	.p2align 4
	.type	deflate.part.0, @function
deflate.part.0:
.LFB706:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	56(%rdi), %rax
	cmpq	$0, 24(%rdi)
	movl	%esi, -64(%rbp)
	movq	%rax, -56(%rbp)
	je	.L317
	movl	8(%rdi), %edx
	testl	%edx, %edx
	jne	.L596
.L318:
	movq	-56(%rbp), %rax
	cmpl	$4, -64(%rbp)
	setne	%cl
	movl	8(%rax), %eax
	cmpl	$666, %eax
	jne	.L319
	testb	%cl, %cl
	jne	.L317
.L319:
	movl	32(%r12), %r11d
	testl	%r11d, %r11d
	je	.L477
	movq	-56(%rbp), %rdi
	movl	-64(%rbp), %ebx
	cmpq	$0, 40(%rdi)
	movl	76(%rdi), %esi
	movl	%ebx, 76(%rdi)
	jne	.L597
	testl	%edx, %edx
	je	.L598
	cmpl	$666, %eax
	je	.L477
.L329:
	cmpl	$42, %eax
	jne	.L331
	movq	-56(%rbp), %rcx
	movl	164(%rcx), %eax
	sall	$12, %eax
	subl	$30720, %eax
	cmpl	$1, 280(%rcx)
	jle	.L599
.L332:
	movq	-56(%rbp), %rcx
	movl	252(%rcx), %r9d
	testl	%r9d, %r9d
	je	.L335
	orl	$32, %eax
.L335:
	movl	%eax, %edx
	movq	-56(%rbp), %rdi
	imulq	$138547333, %rdx, %rdx
	movq	16(%rdi), %rcx
	shrq	$32, %rdx
	subl	%edx, %eax
	shrl	%eax
	addl	%edx, %eax
	shrl	$4, %eax
	movl	%eax, %edx
	sall	$5, %edx
	subl	%eax, %edx
	movl	%edx, %eax
	movq	40(%rdi), %rdx
	addl	$31, %eax
	leaq	1(%rdx), %rsi
	movq	%rsi, 40(%rdi)
	movb	%ah, (%rcx,%rdx)
	movq	40(%rdi), %rdx
	movq	16(%rdi), %rcx
	leaq	1(%rdx), %rsi
	movq	%rsi, 40(%rdi)
	movb	%al, (%rcx,%rdx)
	movl	252(%rdi), %r8d
	testl	%r8d, %r8d
	je	.L336
	movq	40(%rdi), %rdx
	movq	96(%r12), %rax
	movq	16(%rdi), %rcx
	shrq	$16, %rax
	leaq	1(%rdx), %rsi
	movq	%rsi, 40(%rdi)
	movl	%eax, %esi
	shrl	$8, %esi
	movb	%sil, (%rcx,%rdx)
	movq	40(%rdi), %rdx
	movq	16(%rdi), %rcx
	leaq	1(%rdx), %rsi
	movq	%rsi, 40(%rdi)
	movb	%al, (%rcx,%rdx)
	movq	40(%rdi), %rax
	movzwl	96(%r12), %edx
	movq	16(%rdi), %rcx
	leaq	1(%rax), %rsi
	movq	%rsi, 40(%rdi)
	movb	%dh, (%rcx,%rax)
	movq	40(%rdi), %rax
	movq	16(%rdi), %rcx
	leaq	1(%rax), %rsi
	movq	%rsi, 40(%rdi)
	movb	%dl, (%rcx,%rax)
.L336:
	xorl	%edx, %edx
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	adler32@PLT
	movq	56(%r12), %r14
	movq	%rax, 96(%r12)
	movq	-56(%rbp), %rax
	movq	%r14, %rdi
	movl	$113, 8(%rax)
	call	_tr_flush_bits
	movq	40(%r14), %rax
	cmpl	%eax, 32(%r12)
	movl	%eax, %r13d
	cmovbe	32(%r12), %r13d
	testl	%r13d, %r13d
	jne	.L600
.L338:
	movq	-56(%rbp), %rax
	cmpq	$0, 40(%rax)
	jne	.L586
	movl	8(%rax), %eax
.L331:
	cmpl	$57, %eax
	je	.L601
.L341:
	cmpl	$69, %eax
	je	.L602
	cmpl	$73, %eax
	je	.L603
	cmpl	$91, %eax
	je	.L604
	cmpl	$103, %eax
	jne	.L330
	movq	-56(%rbp), %rax
	movq	56(%rax), %rax
	movl	68(%rax), %eax
	jmp	.L391
	.p2align 4,,10
	.p2align 3
.L596:
	cmpq	$0, (%rdi)
	jne	.L318
.L317:
	movq	32+z_errmsg(%rip), %rax
	movq	%rax, 48(%r12)
	movl	$-2, %eax
	jmp	.L316
	.p2align 4,,10
	.p2align 3
.L598:
	cmpl	$5, %ebx
	movl	$1, %edx
	leal	(%rbx,%rbx), %edi
	cmove	%edx, %edi
	leal	(%rsi,%rsi), %edx
	cmpl	$4, %esi
	leal	-9(%rdx), %r9d
	cmovg	%r9d, %edx
	cmpl	%edx, %edi
	jg	.L328
	testb	%cl, %cl
	jne	.L477
.L328:
	cmpl	$666, %eax
	jne	.L329
	movl	8(%r12), %r10d
	testl	%r10d, %r10d
	jne	.L477
.L330:
	movl	8(%r12), %esi
	testl	%esi, %esi
	jne	.L399
	movq	-56(%rbp), %rax
	movl	260(%rax), %ecx
	testl	%ecx, %ecx
	je	.L605
.L399:
	movq	-56(%rbp), %rax
	movslq	276(%rax), %rax
	testl	%eax, %eax
	je	.L606
	movq	-56(%rbp), %rcx
	movl	280(%rcx), %edx
	cmpl	$2, %edx
	je	.L414
	cmpl	$3, %edx
	je	.L607
	salq	$4, %rax
	leaq	configuration_table(%rip), %rdx
	movl	-64(%rbp), %esi
	movq	%rcx, %rdi
	call	*8(%rdx,%rax)
.L403:
	movl	%eax, %edx
	leal	-2(%rax), %ecx
	andl	$-3, %edx
	cmpl	$1, %ecx
	ja	.L430
	movq	-56(%rbp), %rax
	movl	$666, 8(%rax)
	testl	%edx, %edx
	je	.L421
.L401:
	cmpl	$4, -64(%rbp)
	jne	.L588
.L478:
	movq	-56(%rbp), %rax
	movl	48(%rax), %edx
	movl	$1, %eax
	testl	%edx, %edx
	jle	.L316
	cmpl	$2, %edx
	je	.L608
	movq	-56(%rbp), %rdi
	movq	96(%r12), %rax
	movq	40(%rdi), %rdx
	shrq	$16, %rax
	movq	16(%rdi), %rcx
	leaq	1(%rdx), %rsi
	movq	%rsi, 40(%rdi)
	movl	%eax, %esi
	shrl	$8, %esi
	movb	%sil, (%rcx,%rdx)
	movq	40(%rdi), %rdx
	movq	16(%rdi), %rcx
	leaq	1(%rdx), %rsi
	movq	%rsi, 40(%rdi)
	movb	%al, (%rcx,%rdx)
	movq	40(%rdi), %rax
	movzwl	96(%r12), %edx
	movq	16(%rdi), %rcx
	leaq	1(%rax), %rsi
	movq	%rsi, 40(%rdi)
	movb	%dh, (%rcx,%rax)
	movq	40(%rdi), %rax
	movq	16(%rdi), %rcx
	leaq	1(%rax), %rsi
	movq	%rsi, 40(%rdi)
	movb	%dl, (%rcx,%rax)
.L472:
	movq	56(%r12), %r13
	movq	%r13, %rdi
	call	_tr_flush_bits
	movq	40(%r13), %rax
	cmpl	%eax, 32(%r12)
	movl	%eax, %r15d
	cmovbe	32(%r12), %r15d
	testl	%r15d, %r15d
	je	.L474
	movl	%r15d, %r14d
	movq	24(%r12), %rdi
	movq	32(%r13), %rsi
	movq	%r14, %rdx
	call	memcpy@PLT
	addq	%r14, 24(%r12)
	addq	%r14, 32(%r13)
	addq	%r14, 40(%r12)
	subl	%r15d, 32(%r12)
	subq	%r14, 40(%r13)
	je	.L609
.L474:
	movq	-56(%rbp), %rcx
	movl	48(%rcx), %eax
	testl	%eax, %eax
	jle	.L476
	negl	%eax
	movl	%eax, 48(%rcx)
.L476:
	movq	-56(%rbp), %rax
	cmpq	$0, 40(%rax)
	sete	%al
	addq	$56, %rsp
	popq	%rbx
	movzbl	%al, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L612:
	.cfi_restore_state
	movq	%rax, %rcx
	movq	232(%rax), %rax
	xorl	%esi, %esi
	movl	252(%rcx), %edx
	subq	%rax, %rdx
	testq	%rax, %rax
	js	.L416
	movl	%eax, %eax
	addq	176(%rcx), %rax
	movq	%rax, %rsi
.L416:
	movq	-56(%rbp), %rbx
	movl	$1, %ecx
	movq	%rbx, %rdi
	call	_tr_flush_block
	movq	(%rbx), %r15
	movl	252(%rbx), %eax
	movq	56(%r15), %r14
	movq	%rax, 232(%rbx)
	movq	%r14, %rdi
	call	_tr_flush_bits
	movq	40(%r14), %rax
	cmpl	%eax, 32(%r15)
	movl	%eax, %r13d
	cmovbe	32(%r15), %r13d
	testl	%r13d, %r13d
	je	.L457
	movq	24(%r15), %rdi
	movq	32(%r14), %rsi
	movl	%r13d, %edx
	movq	%rdx, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %rdx
	addq	%rdx, 24(%r15)
	addq	%rdx, 32(%r14)
	addq	%rdx, 40(%r15)
	subl	%r13d, 32(%r15)
	subq	%rdx, 40(%r14)
	jne	.L457
	movq	16(%r14), %rax
	movq	%rax, 32(%r14)
.L457:
	movq	-56(%rbp), %rcx
	movq	(%rcx), %rax
	movl	32(%rax), %r10d
	testl	%r10d, %r10d
	jne	.L459
	movl	$666, 8(%rcx)
	.p2align 4,,10
	.p2align 3
.L421:
	movl	32(%r12), %edi
	testl	%edi, %edi
	jne	.L588
.L587:
	movq	-56(%rbp), %rax
.L586:
	movl	$-1, 76(%rax)
.L588:
	xorl	%eax, %eax
.L316:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L597:
	.cfi_restore_state
	movq	%rdi, %rbx
	call	_tr_flush_bits
	movq	40(%rbx), %rax
	movl	32(%r12), %edx
	movl	%eax, %r13d
	cmpl	%edx, %eax
	cmova	%edx, %r13d
	testl	%r13d, %r13d
	jne	.L610
.L323:
	movq	-56(%rbp), %rax
	testl	%edx, %edx
	je	.L586
	movl	8(%rax), %eax
	jmp	.L328
	.p2align 4,,10
	.p2align 3
.L610:
	movl	%r13d, %r14d
	movq	24(%r12), %rdi
	movq	32(%rbx), %rsi
	movq	%r14, %rdx
	call	memcpy@PLT
	movl	32(%r12), %edx
	addq	%r14, 24(%r12)
	addq	%r14, 32(%rbx)
	subl	%r13d, %edx
	addq	%r14, 40(%r12)
	movl	%edx, 32(%r12)
	subq	%r14, 40(%rbx)
	jne	.L323
	movq	16(%rbx), %rax
	movq	%rax, 32(%rbx)
	jmp	.L323
	.p2align 4,,10
	.p2align 3
.L600:
	movl	%r13d, %r15d
	movq	24(%r12), %rdi
	movq	32(%r14), %rsi
	movq	%r15, %rdx
	call	memcpy@PLT
	addq	%r15, 24(%r12)
	addq	%r15, 32(%r14)
	addq	%r15, 40(%r12)
	subl	%r13d, 32(%r12)
	subq	%r15, 40(%r14)
	jne	.L338
	movq	16(%r14), %rax
	movq	%rax, 32(%r14)
	jmp	.L338
	.p2align 4,,10
	.p2align 3
.L611:
	movq	232(%rdi), %rax
	xorl	%esi, %esi
	subq	%rax, %rdx
	testq	%rax, %rax
	js	.L409
	movl	%eax, %eax
	addq	176(%rdi), %rax
	movq	%rax, %rsi
.L409:
	movq	-56(%rbp), %rbx
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	call	_tr_flush_block
	movq	(%rbx), %r13
	movl	252(%rbx), %eax
	movq	56(%r13), %r15
	movq	%rax, 232(%rbx)
	movq	%r15, %rdi
	call	_tr_flush_bits
	movq	40(%r15), %rax
	cmpl	%eax, 32(%r13)
	movl	%eax, %r14d
	cmovbe	32(%r13), %r14d
	testl	%r14d, %r14d
	je	.L411
	movq	24(%r13), %rdi
	movq	32(%r15), %rsi
	movl	%r14d, %edx
	movq	%rdx, -72(%rbp)
	call	memcpy@PLT
	movq	-72(%rbp), %rdx
	addq	%rdx, 24(%r13)
	addq	%rdx, 32(%r15)
	addq	%rdx, 40(%r13)
	subl	%r14d, 32(%r13)
	subq	%rdx, 40(%r15)
	jne	.L411
	movq	16(%r15), %rax
	movq	%rax, 32(%r15)
.L411:
	movq	-56(%rbp), %rax
	movq	(%rax), %rax
	movl	32(%rax), %r14d
	testl	%r14d, %r14d
	je	.L421
.L414:
	movq	-56(%rbp), %rax
	movl	260(%rax), %eax
	jmp	.L408
	.p2align 4,,10
	.p2align 3
.L405:
	movq	-56(%rbp), %rdi
	movl	252(%rdi), %eax
	movq	176(%rdi), %rdx
	movl	$0, 240(%rdi)
	movq	5968(%rdi), %rcx
	movzbl	(%rdx,%rax), %eax
	movl	5980(%rdi), %edx
	leal	1(%rdx), %esi
	movl	%esi, 5980(%rdi)
	movb	$0, (%rcx,%rdx)
	movl	5980(%rdi), %edx
	movq	5968(%rdi), %rcx
	leal	1(%rdx), %esi
	movl	%esi, 5980(%rdi)
	movb	$0, (%rcx,%rdx)
	movl	5980(%rdi), %edx
	movq	5968(%rdi), %rcx
	leal	1(%rdx), %esi
	movl	%esi, 5980(%rdi)
	movb	%al, (%rcx,%rdx)
	addw	$1, 292(%rdi,%rax,4)
	movl	252(%rdi), %ebx
	movl	260(%rdi), %eax
	leal	1(%rbx), %edx
	movl	5984(%rdi), %ebx
	subl	$1, %eax
	movl	%edx, 252(%rdi)
	movl	%eax, 260(%rdi)
	cmpl	%ebx, 5980(%rdi)
	je	.L611
.L408:
	testl	%eax, %eax
	jne	.L405
	movq	-56(%rbp), %rbx
	movq	%rbx, %rdi
	call	fill_window
	movl	260(%rbx), %eax
	testl	%eax, %eax
	jne	.L405
	movl	-64(%rbp), %r15d
	testl	%r15d, %r15d
	je	.L421
	movq	-56(%rbp), %rax
	cmpl	$4, -64(%rbp)
	movl	$0, 6012(%rax)
	je	.L612
	movl	5980(%rax), %r13d
	testl	%r13d, %r13d
	je	.L460
	movq	%rax, %rcx
.L595:
	movq	232(%rcx), %rax
	movl	252(%rcx), %edx
	xorl	%esi, %esi
	subq	%rax, %rdx
	testq	%rax, %rax
	js	.L461
	movl	%eax, %eax
	addq	176(%rcx), %rax
	movq	%rax, %rsi
.L461:
	movq	-56(%rbp), %rbx
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	call	_tr_flush_block
	movq	(%rbx), %r13
	movl	252(%rbx), %eax
	movq	56(%r13), %r14
	movq	%rax, 232(%rbx)
	movq	%r14, %rdi
	call	_tr_flush_bits
	movq	40(%r14), %r15
	cmpl	%r15d, 32(%r13)
	cmovbe	32(%r13), %r15d
	testl	%r15d, %r15d
	je	.L463
	movq	24(%r13), %rdi
	movq	32(%r14), %rsi
	movl	%r15d, %edx
	movq	%rdx, -72(%rbp)
	call	memcpy@PLT
	movq	-72(%rbp), %rdx
	addq	%rdx, 24(%r13)
	addq	%rdx, 32(%r14)
	addq	%rdx, 40(%r13)
	subl	%r15d, 32(%r13)
	subq	%rdx, 40(%r14)
	jne	.L463
	movq	16(%r14), %rax
	movq	%rax, 32(%r14)
.L463:
	movq	-56(%rbp), %rax
	movq	(%rax), %rax
	movl	32(%rax), %r8d
	xorl	%eax, %eax
	testl	%r8d, %r8d
	setne	%al
	movl	%eax, %edx
	.p2align 4,,10
	.p2align 3
.L430:
	testl	%edx, %edx
	je	.L421
	cmpl	$1, %eax
	jne	.L401
.L460:
	cmpl	$1, -64(%rbp)
	je	.L613
	movl	-64(%rbp), %ebx
	cmpl	$5, %ebx
	jne	.L614
.L467:
	movq	56(%r12), %r14
	movq	%r14, %rdi
	call	_tr_flush_bits
	movq	40(%r14), %rax
	movl	32(%r12), %edx
	movl	%eax, %r13d
	cmpl	%edx, %eax
	cmova	%edx, %r13d
	testl	%r13d, %r13d
	jne	.L615
.L470:
	testl	%edx, %edx
	jne	.L401
	jmp	.L587
	.p2align 4,,10
	.p2align 3
.L605:
	movl	-64(%rbp), %edx
	testl	%edx, %edx
	je	.L588
	cmpl	$666, 8(%rax)
	jne	.L399
	jmp	.L401
	.p2align 4,,10
	.p2align 3
.L599:
	movl	276(%rcx), %edx
	cmpl	$1, %edx
	jle	.L332
	cmpl	$5, %edx
	jg	.L333
	orl	$64, %eax
	jmp	.L332
	.p2align 4,,10
	.p2align 3
.L603:
	movq	-56(%rbp), %rax
	movq	56(%rax), %rax
.L366:
	cmpq	$0, 40(%rax)
	je	.L368
	movq	-56(%rbp), %rcx
	movq	40(%rcx), %rdi
	movq	%rdi, %rdx
	jmp	.L376
	.p2align 4,,10
	.p2align 3
.L369:
	movq	-56(%rbp), %rbx
	movq	40(%rax), %rcx
	movq	64(%rbx), %rax
	leaq	1(%rax), %rsi
	movq	%rsi, 64(%rbx)
	movzbl	(%rcx,%rax), %eax
	leaq	1(%rdx), %rsi
	movq	16(%rbx), %rcx
	movq	%rsi, 40(%rbx)
	movb	%al, (%rcx,%rdx)
	testb	%al, %al
	je	.L375
	movq	40(%rbx), %rdx
	movq	56(%rbx), %rax
	movq	%rbx, %rcx
.L376:
	cmpq	%rdx, 24(%rcx)
	jne	.L369
	movl	68(%rax), %r10d
	testl	%r10d, %r10d
	je	.L370
	cmpq	%rdx, %rdi
	jb	.L616
.L370:
	movq	56(%r12), %r14
	movq	%r14, %rdi
	call	_tr_flush_bits
	movq	40(%r14), %rax
	cmpl	%eax, 32(%r12)
	movl	%eax, %r13d
	cmovbe	32(%r12), %r13d
	testl	%r13d, %r13d
	je	.L372
	movl	%r13d, %r15d
	movq	24(%r12), %rdi
	movq	32(%r14), %rsi
	movq	%r15, %rdx
	call	memcpy@PLT
	addq	%r15, 24(%r12)
	addq	%r15, 32(%r14)
	addq	%r15, 40(%r12)
	subl	%r13d, 32(%r12)
	subq	%r15, 40(%r14)
	jne	.L372
	movq	16(%r14), %rax
	movq	%rax, 32(%r14)
.L372:
	movq	-56(%rbp), %rax
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	jne	.L586
	movq	56(%rax), %rax
	xorl	%edi, %edi
	jmp	.L369
	.p2align 4,,10
	.p2align 3
.L375:
	movq	56(%rbx), %rax
	movl	68(%rax), %r9d
	testl	%r9d, %r9d
	je	.L377
	movq	40(%rbx), %rdx
	cmpq	%rdi, %rdx
	jbe	.L377
	subl	%edi, %edx
	addq	16(%rbx), %rdi
	movq	%rdi, %rsi
	movq	96(%r12), %rdi
	call	crc32@PLT
	movq	%rax, 96(%r12)
	movq	56(%rbx), %rax
	.p2align 4,,10
	.p2align 3
.L377:
	movq	-56(%rbp), %rcx
	movq	$0, 64(%rcx)
.L368:
	movq	-56(%rbp), %rcx
	movl	$91, 8(%rcx)
.L378:
	cmpq	$0, 56(%rax)
	je	.L583
	movq	-56(%rbp), %rcx
	movq	40(%rcx), %rdi
	movq	%rdi, %rdx
	jmp	.L389
	.p2align 4,,10
	.p2align 3
.L382:
	movq	-56(%rbp), %rbx
	movq	56(%rax), %rcx
	movq	64(%rbx), %rax
	leaq	1(%rax), %rsi
	movq	%rsi, 64(%rbx)
	movzbl	(%rcx,%rax), %eax
	leaq	1(%rdx), %rsi
	movq	16(%rbx), %rcx
	movq	%rsi, 40(%rbx)
	movb	%al, (%rcx,%rdx)
	testb	%al, %al
	je	.L388
	movq	40(%rbx), %rdx
	movq	56(%rbx), %rax
	movq	%rbx, %rcx
.L389:
	cmpq	%rdx, 24(%rcx)
	jne	.L382
	movl	68(%rax), %r8d
	testl	%r8d, %r8d
	je	.L383
	cmpq	%rdx, %rdi
	jb	.L617
.L383:
	movq	56(%r12), %r14
	movq	%r14, %rdi
	call	_tr_flush_bits
	movq	40(%r14), %rax
	cmpl	%eax, 32(%r12)
	movl	%eax, %r13d
	cmovbe	32(%r12), %r13d
	testl	%r13d, %r13d
	je	.L385
	movl	%r13d, %r15d
	movq	24(%r12), %rdi
	movq	32(%r14), %rsi
	movq	%r15, %rdx
	call	memcpy@PLT
	addq	%r15, 24(%r12)
	addq	%r15, 32(%r14)
	addq	%r15, 40(%r12)
	subl	%r13d, 32(%r12)
	subq	%r15, 40(%r14)
	jne	.L385
	movq	16(%r14), %rax
	movq	%rax, 32(%r14)
.L385:
	movq	-56(%rbp), %rax
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	jne	.L586
	movq	56(%rax), %rax
	xorl	%edi, %edi
	jmp	.L382
	.p2align 4,,10
	.p2align 3
.L601:
	movq	-56(%rbp), %rbx
	movq	%rbx, %rdi
	call	crc_reset
	movq	40(%rbx), %rax
	movq	16(%rbx), %rdx
	leaq	1(%rax), %rcx
	movq	%rcx, 40(%rbx)
	movb	$31, (%rdx,%rax)
	movq	40(%rbx), %rax
	movq	16(%rbx), %rdx
	leaq	1(%rax), %rcx
	movq	%rcx, 40(%rbx)
	movb	$-117, (%rdx,%rax)
	movq	40(%rbx), %rax
	movq	16(%rbx), %rdx
	leaq	1(%rax), %rcx
	movq	%rcx, 40(%rbx)
	movb	$8, (%rdx,%rax)
	movq	56(%rbx), %rax
	testq	%rax, %rax
	je	.L618
	movl	(%rax), %ecx
	movl	68(%rax), %esi
	testl	%ecx, %ecx
	setne	%dl
	testl	%esi, %esi
	je	.L348
	addl	$2, %edx
.L348:
	cmpq	$0, 24(%rax)
	je	.L349
	addl	$4, %edx
.L349:
	cmpq	$0, 40(%rax)
	je	.L350
	addl	$8, %edx
.L350:
	cmpq	$0, 56(%rax)
	je	.L351
	addl	$16, %edx
.L351:
	movq	-56(%rbp), %rdi
	movq	40(%rdi), %rax
	movq	16(%rdi), %rcx
	leaq	1(%rax), %rsi
	movq	%rsi, 40(%rdi)
	movb	%dl, (%rcx,%rax)
	movq	56(%rdi), %rax
	movq	16(%rdi), %rdx
	movq	8(%rax), %rcx
	movq	40(%rdi), %rax
	leaq	1(%rax), %rsi
	movq	%rsi, 40(%rdi)
	movb	%cl, (%rdx,%rax)
	movq	56(%rdi), %rax
	movq	40(%rdi), %rdx
	movq	16(%rdi), %rcx
	movq	8(%rax), %rax
	leaq	1(%rdx), %rsi
	shrq	$8, %rax
	movq	%rsi, 40(%rdi)
	movb	%al, (%rcx,%rdx)
	movq	56(%rdi), %rax
	movq	40(%rdi), %rdx
	movq	16(%rdi), %rcx
	movq	8(%rax), %rax
	leaq	1(%rdx), %rsi
	shrq	$16, %rax
	movq	%rsi, 40(%rdi)
	movb	%al, (%rcx,%rdx)
	movq	56(%rdi), %rax
	movq	40(%rdi), %rdx
	movq	16(%rdi), %rcx
	movq	8(%rax), %rax
	leaq	1(%rdx), %rsi
	shrq	$24, %rax
	movq	%rsi, 40(%rdi)
	movb	%al, (%rcx,%rdx)
	movl	276(%rdi), %eax
	movl	$2, %edx
	cmpl	$9, %eax
	je	.L352
	cmpl	$1, 280(%rdi)
	jg	.L483
	xorl	%edx, %edx
	cmpl	$1, %eax
	jg	.L352
.L483:
	movl	$4, %edx
.L352:
	movq	-56(%rbp), %rdi
	movq	40(%rdi), %rax
	movq	16(%rdi), %rcx
	leaq	1(%rax), %rsi
	movq	%rsi, 40(%rdi)
	movb	%dl, (%rcx,%rax)
	movq	56(%rdi), %rax
	movq	16(%rdi), %rdx
	movl	20(%rax), %ecx
	movq	40(%rdi), %rax
	leaq	1(%rax), %rsi
	movq	%rsi, 40(%rdi)
	movb	%cl, (%rdx,%rax)
	movq	56(%rdi), %rax
	cmpq	$0, 24(%rax)
	je	.L353
	movl	32(%rax), %ecx
	movq	40(%rdi), %rax
	movq	16(%rdi), %rdx
	leaq	1(%rax), %rsi
	movq	%rsi, 40(%rdi)
	movb	%cl, (%rdx,%rax)
	movq	56(%rdi), %rax
	movq	16(%rdi), %rdx
	movl	32(%rax), %ecx
	movq	40(%rdi), %rax
	leaq	1(%rax), %rsi
	movq	%rsi, 40(%rdi)
	movb	%ch, (%rdx,%rax)
	movq	56(%rdi), %rax
.L353:
	movl	68(%rax), %edx
	testl	%edx, %edx
	jne	.L619
.L354:
	movq	-56(%rbp), %rcx
	movq	$0, 64(%rcx)
	movl	$69, 8(%rcx)
.L355:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L357
	movq	-56(%rbp), %rcx
	movzwl	32(%rax), %r14d
	movq	64(%rcx), %rsi
	movq	40(%rcx), %r15
	movq	24(%rcx), %rax
	subl	%esi, %r14d
	movl	%r14d, %r13d
	leaq	(%r15,%r13), %rcx
	cmpq	%rcx, %rax
	jb	.L364
	jmp	.L358
	.p2align 4,,10
	.p2align 3
.L359:
	movq	-56(%rbp), %rax
	movq	56(%r12), %r15
	addq	%r13, 64(%rax)
	movq	%r15, %rdi
	call	_tr_flush_bits
	movq	40(%r15), %rax
	cmpl	%eax, 32(%r12)
	movl	%eax, %ecx
	cmovbe	32(%r12), %ecx
	testl	%ecx, %ecx
	je	.L361
	movq	24(%r12), %rdi
	movq	32(%r15), %rsi
	movl	%ecx, %edx
	movl	%ecx, -84(%rbp)
	movq	%rdx, -80(%rbp)
	call	memcpy@PLT
	movq	-80(%rbp), %rdx
	movl	-84(%rbp), %ecx
	addq	%rdx, 24(%r12)
	addq	%rdx, 32(%r15)
	addq	%rdx, 40(%r12)
	subl	%ecx, 32(%r12)
	subq	%rdx, 40(%r15)
	jne	.L361
	movq	16(%r15), %rax
	movq	%rax, 32(%r15)
.L361:
	movq	-56(%rbp), %rax
	movq	40(%rax), %r15
	testq	%r15, %r15
	jne	.L586
	movq	%rax, %rcx
	movl	%ebx, %r13d
	movq	24(%rax), %rax
	subl	-72(%rbp), %r13d
	movq	56(%rcx), %rdx
	addl	%r14d, %r13d
	movq	64(%rcx), %rsi
	movq	%r13, %r14
	movq	24(%rdx), %rdx
	cmpq	%r13, %rax
	jnb	.L358
.L364:
	movl	%eax, -72(%rbp)
	movl	%eax, %r13d
	movq	-56(%rbp), %rax
	addq	%rdx, %rsi
	subl	%r15d, %r13d
	movq	%r15, %rdi
	movl	%r15d, %ebx
	movq	%r13, %rdx
	addq	16(%rax), %rdi
	call	memcpy@PLT
	movq	-56(%rbp), %rax
	movq	24(%rax), %rdx
	movq	%rdx, 40(%rax)
	movq	56(%rax), %rax
	movl	68(%rax), %eax
	testl	%eax, %eax
	je	.L359
	cmpq	%r15, %rdx
	jbe	.L359
	movq	-56(%rbp), %rax
	movq	96(%r12), %rdi
	subl	%r15d, %edx
	movq	%r15, %rsi
	addq	16(%rax), %rsi
	call	crc32@PLT
	movq	%rax, 96(%r12)
	jmp	.L359
	.p2align 4,,10
	.p2align 3
.L358:
	movq	-56(%rbp), %rbx
	addq	%rdx, %rsi
	movq	%r15, %rdi
	movq	%r13, %rdx
	addq	16(%rbx), %rdi
	call	memcpy@PLT
	movq	56(%rbx), %rax
	addq	40(%rbx), %r13
	movq	%r13, 40(%rbx)
	movl	68(%rax), %r11d
	testl	%r11d, %r11d
	je	.L365
	cmpq	%r15, %r13
	jbe	.L365
	movq	-56(%rbp), %rbx
	movq	96(%r12), %rdi
	movl	%r13d, %edx
	movq	%r15, %rsi
	subl	%r15d, %edx
	addq	16(%rbx), %rsi
	call	crc32@PLT
	movq	%rax, 96(%r12)
	movq	56(%rbx), %rax
	.p2align 4,,10
	.p2align 3
.L365:
	movq	-56(%rbp), %rcx
	movq	$0, 64(%rcx)
.L357:
	movq	-56(%rbp), %rcx
	movl	$73, 8(%rcx)
	jmp	.L366
	.p2align 4,,10
	.p2align 3
.L602:
	movq	-56(%rbp), %rax
	movq	56(%rax), %rax
	jmp	.L355
	.p2align 4,,10
	.p2align 3
.L606:
	movl	-64(%rbp), %esi
	movq	-56(%rbp), %rdi
	call	deflate_stored
	jmp	.L403
	.p2align 4,,10
	.p2align 3
.L616:
	subl	%edi, %edx
	addq	16(%rcx), %rdi
	movq	%rdi, %rsi
	movq	96(%r12), %rdi
	call	crc32@PLT
	movq	%rax, 96(%r12)
	jmp	.L370
	.p2align 4,,10
	.p2align 3
.L617:
	subl	%edi, %edx
	addq	16(%rcx), %rdi
	movq	%rdi, %rsi
	movq	96(%r12), %rdi
	call	crc32@PLT
	movq	%rax, 96(%r12)
	jmp	.L383
	.p2align 4,,10
	.p2align 3
.L388:
	movq	56(%rbx), %rax
	movl	68(%rax), %eax
	testl	%eax, %eax
	je	.L390
	movq	40(%rbx), %rdx
	cmpq	%rdi, %rdx
	ja	.L620
.L381:
	movq	-56(%rbp), %rcx
	movl	$103, 8(%rcx)
.L391:
	testl	%eax, %eax
	je	.L390
	movq	-56(%rbp), %rcx
	movq	40(%rcx), %rax
	leaq	2(%rax), %rdx
	cmpq	24(%rcx), %rdx
	jbe	.L392
	movq	56(%r12), %r14
	movq	%r14, %rdi
	call	_tr_flush_bits
	movq	40(%r14), %r13
	cmpl	%r13d, 32(%r12)
	cmovbe	32(%r12), %r13d
	testl	%r13d, %r13d
	jne	.L621
.L394:
	movq	-56(%rbp), %rax
	movq	40(%rax), %rax
	testq	%rax, %rax
	jne	.L587
.L392:
	movq	-56(%rbp), %rdi
	movq	96(%r12), %rcx
	leaq	1(%rax), %rsi
	movq	16(%rdi), %rdx
	movq	%rsi, 40(%rdi)
	movb	%cl, (%rdx,%rax)
	movq	40(%rdi), %rdx
	movq	96(%r12), %rax
	movq	16(%rdi), %rcx
	leaq	1(%rdx), %rsi
	shrq	$8, %rax
	movq	%rsi, 40(%rdi)
	xorl	%esi, %esi
	xorl	%edi, %edi
	movb	%al, (%rcx,%rdx)
	xorl	%edx, %edx
	call	crc32@PLT
	movq	%rax, 96(%r12)
.L390:
	movq	-56(%rbp), %rax
	movq	56(%r12), %r14
	movl	$113, 8(%rax)
	movq	%r14, %rdi
	call	_tr_flush_bits
	movq	40(%r14), %rax
	cmpl	%eax, 32(%r12)
	movl	%eax, %r13d
	cmovbe	32(%r12), %r13d
	testl	%r13d, %r13d
	je	.L397
	movl	%r13d, %r15d
	movq	24(%r12), %rdi
	movq	32(%r14), %rsi
	movq	%r15, %rdx
	call	memcpy@PLT
	addq	%r15, 24(%r12)
	addq	%r15, 32(%r14)
	addq	%r15, 40(%r12)
	subl	%r13d, 32(%r12)
	subq	%r15, 40(%r14)
	jne	.L397
	movq	16(%r14), %rax
	movq	%rax, 32(%r14)
.L397:
	movq	-56(%rbp), %rax
	cmpq	$0, 40(%rax)
	je	.L330
	jmp	.L586
.L620:
	subl	%edi, %edx
	addq	16(%rbx), %rdi
	movq	%rdi, %rsi
	movq	96(%r12), %rdi
	call	crc32@PLT
	movq	%rax, 96(%r12)
	movq	56(%rbx), %rax
.L583:
	movl	68(%rax), %eax
	jmp	.L381
	.p2align 4,,10
	.p2align 3
.L615:
	movl	%r13d, %r15d
	movq	24(%r12), %rdi
	movq	32(%r14), %rsi
	movq	%r15, %rdx
	call	memcpy@PLT
	movl	32(%r12), %edx
	addq	%r15, 24(%r12)
	addq	%r15, 32(%r14)
	subl	%r13d, %edx
	addq	%r15, 40(%r12)
	movl	%edx, 32(%r12)
	subq	%r15, 40(%r14)
	jne	.L470
	movq	16(%r14), %rax
	movq	%rax, 32(%r14)
	jmp	.L470
	.p2align 4,,10
	.p2align 3
.L607:
	leaq	_length_code(%rip), %r13
.L453:
	movq	-56(%rbp), %rax
	movl	260(%rax), %eax
	jmp	.L448
	.p2align 4,,10
	.p2align 3
.L432:
	movq	-56(%rbp), %rcx
	movl	252(%rcx), %edx
	movq	176(%rcx), %rsi
	movl	$0, 240(%rcx)
	movl	%edx, %ecx
.L479:
	testl	%edx, %edx
	je	.L434
	leaq	-1(%rsi,%rcx), %rdx
	movzbl	(%rdx), %edi
	cmpb	1(%rdx), %dil
	je	.L622
	.p2align 4,,10
	.p2align 3
.L434:
	movq	-56(%rbp), %rbx
	movzbl	(%rsi,%rcx), %eax
	movl	5980(%rbx), %edx
	movq	5968(%rbx), %rcx
	leal	1(%rdx), %esi
	movl	%esi, 5980(%rbx)
	movb	$0, (%rcx,%rdx)
	movl	5980(%rbx), %edx
	movq	5968(%rbx), %rcx
	leal	1(%rdx), %esi
	movl	%esi, 5980(%rbx)
	movb	$0, (%rcx,%rdx)
	movl	5980(%rbx), %edx
	movq	5968(%rbx), %rcx
	leal	1(%rdx), %esi
	movl	%esi, 5980(%rbx)
	movq	%rbx, %rsi
	movb	%al, (%rcx,%rdx)
	xorl	%ecx, %ecx
	addw	$1, 292(%rbx,%rax,4)
	movl	5984(%rbx), %eax
	cmpl	%eax, 5980(%rbx)
	movl	260(%rbx), %eax
	sete	%cl
	subl	$1, %eax
	movl	%eax, 260(%rbx)
	movl	252(%rbx), %ebx
	leal	1(%rbx), %edx
	movl	%edx, 252(%rsi)
.L447:
	testl	%ecx, %ecx
	jne	.L623
.L448:
	cmpl	$258, %eax
	ja	.L432
	movq	-56(%rbp), %rbx
	movq	%rbx, %rdi
	call	fill_window
	movl	260(%rbx), %eax
	movq	%rbx, %rcx
	cmpl	$258, %eax
	ja	.L432
	movl	-64(%rbp), %ebx
	testl	%ebx, %ebx
	je	.L421
	testl	%eax, %eax
	je	.L433
	movl	252(%rcx), %edx
	movq	176(%rcx), %rsi
	movl	$0, 240(%rcx)
	movl	%edx, %ecx
	cmpl	$2, %eax
	jbe	.L434
	jmp	.L479
	.p2align 4,,10
	.p2align 3
.L614:
	movq	-56(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_tr_stored_block
	cmpl	$3, %ebx
	jne	.L467
	movq	-56(%rbp), %rbx
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	movl	212(%rbx), %eax
	movq	200(%rbx), %rdi
	subl	$1, %eax
	movw	%cx, (%rdi,%rax,2)
	leaq	(%rax,%rax), %rdx
	call	memset@PLT
	movl	260(%rbx), %esi
	testl	%esi, %esi
	jne	.L467
	movl	$0, 252(%rbx)
	movq	$0, 232(%rbx)
	movl	$0, 6012(%rbx)
	jmp	.L467
	.p2align 4,,10
	.p2align 3
.L333:
	movl	%eax, %ecx
	orb	$-64, %al
	orb	$-128, %cl
	cmpl	$6, %edx
	cmove	%ecx, %eax
	jmp	.L332
	.p2align 4,,10
	.p2align 3
.L621:
	movl	%r13d, %r15d
	movq	24(%r12), %rdi
	movq	32(%r14), %rsi
	movq	%r15, %rdx
	call	memcpy@PLT
	addq	%r15, 24(%r12)
	addq	%r15, 32(%r14)
	addq	%r15, 40(%r12)
	subl	%r13d, 32(%r12)
	subq	%r15, 40(%r14)
	jne	.L394
	movq	16(%r14), %rax
	movq	%rax, 32(%r14)
	jmp	.L394
	.p2align 4,,10
	.p2align 3
.L609:
	movq	16(%r13), %rax
	movq	%rax, 32(%r13)
	jmp	.L474
.L477:
	movq	56+z_errmsg(%rip), %rax
	movq	%rax, 48(%r12)
	movl	$-5, %eax
	jmp	.L316
	.p2align 4,,10
	.p2align 3
.L622:
	cmpb	2(%rdx), %dil
	jne	.L434
	cmpb	3(%rdx), %dil
	jne	.L434
	addq	$3, %rdx
	leaq	258(%rsi,%rcx), %r9
	jmp	.L443
	.p2align 4,,10
	.p2align 3
.L630:
	cmpb	2(%rdx), %dil
	jne	.L624
	cmpb	3(%rdx), %dil
	jne	.L625
	cmpb	4(%rdx), %dil
	jne	.L626
	cmpb	5(%rdx), %dil
	jne	.L627
	cmpb	6(%rdx), %dil
	jne	.L628
	cmpb	7(%rdx), %dil
	jne	.L629
	addq	$8, %rdx
	cmpb	%dil, (%rdx)
	jne	.L437
	cmpq	%rdx, %r9
	jbe	.L437
.L443:
	cmpb	1(%rdx), %dil
	je	.L630
	addq	$1, %rdx
.L437:
	movq	-56(%rbp), %rdi
	subl	%r9d, %edx
	addl	$258, %edx
	movl	%edx, 240(%rdi)
	cmpl	%eax, %edx
	jbe	.L445
	movl	%eax, 240(%rdi)
	movl	%eax, %edx
	.p2align 4,,10
	.p2align 3
.L446:
	movq	-56(%rbp), %rbx
	leal	-3(%rdx), %eax
	movl	5980(%rbx), %edx
	movq	5968(%rbx), %rcx
	leal	1(%rdx), %esi
	movl	%esi, 5980(%rbx)
	movb	$1, (%rcx,%rdx)
	movl	5980(%rbx), %edx
	movq	5968(%rbx), %rcx
	leal	1(%rdx), %esi
	movl	%esi, 5980(%rbx)
	movb	$0, (%rcx,%rdx)
	movl	5980(%rbx), %edx
	movq	5968(%rbx), %rcx
	leal	1(%rdx), %esi
	movl	%esi, 5980(%rbx)
	movb	%al, (%rcx,%rdx)
	movzbl	%al, %eax
	xorl	%ecx, %ecx
	movzbl	0(%r13,%rax), %eax
	addw	$1, 1320(%rbx,%rax,4)
	movzbl	_dist_code(%rip), %eax
	addw	$1, 2584(%rbx,%rax,4)
	movl	240(%rbx), %edx
	movl	$0, 240(%rbx)
	movl	5984(%rbx), %eax
	cmpl	%eax, 5980(%rbx)
	movl	260(%rbx), %eax
	sete	%cl
	subl	%edx, %eax
	addl	252(%rbx), %edx
	movl	%eax, 260(%rbx)
	movl	%edx, 252(%rbx)
	jmp	.L447
.L445:
	cmpl	$2, %edx
	ja	.L446
	jmp	.L434
	.p2align 4,,10
	.p2align 3
.L623:
	movq	-56(%rbp), %rcx
	xorl	%esi, %esi
	movq	232(%rcx), %rax
	subq	%rax, %rdx
	testq	%rax, %rax
	js	.L449
	movl	%eax, %eax
	addq	176(%rcx), %rax
	movq	%rax, %rsi
.L449:
	movq	-56(%rbp), %rbx
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	call	_tr_flush_block
	movq	(%rbx), %r14
	movl	252(%rbx), %eax
	movq	56(%r14), %rcx
	movq	%rax, 232(%rbx)
	movq	%rcx, %rdi
	movq	%rcx, -72(%rbp)
	call	_tr_flush_bits
	movq	-72(%rbp), %rcx
	movq	40(%rcx), %r15
	cmpl	%r15d, 32(%r14)
	cmovbe	32(%r14), %r15d
	testl	%r15d, %r15d
	je	.L451
	movq	32(%rcx), %rsi
	movq	24(%r14), %rdi
	movl	%r15d, %edx
	movq	%rcx, -80(%rbp)
	movq	%rdx, -72(%rbp)
	call	memcpy@PLT
	movq	-80(%rbp), %rcx
	movq	-72(%rbp), %rdx
	addq	%rdx, 24(%r14)
	addq	%rdx, 32(%rcx)
	addq	%rdx, 40(%r14)
	subl	%r15d, 32(%r14)
	subq	%rdx, 40(%rcx)
	jne	.L451
	movq	16(%rcx), %rax
	movq	%rax, 32(%rcx)
.L451:
	movq	-56(%rbp), %rax
	movq	(%rax), %rax
	movl	32(%rax), %r11d
	testl	%r11d, %r11d
	jne	.L453
	jmp	.L421
	.p2align 4,,10
	.p2align 3
.L618:
	movq	-56(%rbp), %rsi
	movq	40(%rsi), %rax
	movq	16(%rsi), %rdx
	leaq	1(%rax), %rcx
	movq	%rcx, 40(%rsi)
	movb	$0, (%rdx,%rax)
	movq	40(%rsi), %rax
	movq	16(%rsi), %rdx
	leaq	1(%rax), %rcx
	movq	%rcx, 40(%rsi)
	movb	$0, (%rdx,%rax)
	movq	40(%rsi), %rax
	movq	16(%rsi), %rdx
	leaq	1(%rax), %rcx
	movq	%rcx, 40(%rsi)
	movb	$0, (%rdx,%rax)
	movq	40(%rsi), %rax
	movq	16(%rsi), %rdx
	leaq	1(%rax), %rcx
	movq	%rcx, 40(%rsi)
	movb	$0, (%rdx,%rax)
	movq	40(%rsi), %rax
	movq	16(%rsi), %rdx
	leaq	1(%rax), %rcx
	movq	%rcx, 40(%rsi)
	movb	$0, (%rdx,%rax)
	movl	276(%rsi), %eax
	movl	$2, %edx
	cmpl	$9, %eax
	je	.L343
	cmpl	$1, 280(%rsi)
	jg	.L481
	xorl	%edx, %edx
	cmpl	$1, %eax
	jg	.L343
.L481:
	movl	$4, %edx
.L343:
	movq	-56(%rbp), %rdi
	movq	40(%rdi), %rax
	movq	16(%rdi), %rcx
	leaq	1(%rax), %rsi
	movq	%rsi, 40(%rdi)
	movb	%dl, (%rcx,%rax)
	movq	40(%rdi), %rax
	movq	16(%rdi), %rdx
	leaq	1(%rax), %rcx
	movq	%rcx, 40(%rdi)
	movb	$3, (%rdx,%rax)
	movq	56(%r12), %r14
	movl	$113, 8(%rdi)
	movq	%r14, %rdi
	call	_tr_flush_bits
	movq	40(%r14), %rax
	cmpl	%eax, 32(%r12)
	movl	%eax, %r13d
	cmovbe	32(%r12), %r13d
	testl	%r13d, %r13d
	je	.L345
	movl	%r13d, %r15d
	movq	24(%r12), %rdi
	movq	32(%r14), %rsi
	movq	%r15, %rdx
	call	memcpy@PLT
	addq	%r15, 24(%r12)
	addq	%r15, 32(%r14)
	addq	%r15, 40(%r12)
	subl	%r13d, 32(%r12)
	subq	%r15, 40(%r14)
	jne	.L345
	movq	16(%r14), %rax
	movq	%rax, 32(%r14)
.L345:
	movq	-56(%rbp), %rax
	cmpq	$0, 40(%rax)
	jne	.L586
	movl	8(%rax), %eax
	jmp	.L341
	.p2align 4,,10
	.p2align 3
.L619:
	movq	-56(%rbp), %rbx
	movq	96(%r12), %rdi
	movq	16(%rbx), %rsi
	movl	40(%rbx), %edx
	call	crc32@PLT
	movq	%rax, 96(%r12)
	movq	56(%rbx), %rax
	jmp	.L354
	.p2align 4,,10
	.p2align 3
.L433:
	movq	-56(%rbp), %rax
	cmpl	$4, -64(%rbp)
	movl	$0, 6012(%rax)
	movq	%rax, %rcx
	je	.L631
	movl	5980(%rax), %r9d
	testl	%r9d, %r9d
	je	.L460
	jmp	.L595
	.p2align 4,,10
	.p2align 3
.L604:
	movq	-56(%rbp), %rax
	movq	56(%rax), %rax
	jmp	.L378
	.p2align 4,,10
	.p2align 3
.L608:
	movq	-56(%rbp), %rbx
	movq	%rbx, %rdi
	call	crc_finalize
	movq	40(%rbx), %rax
	movq	96(%r12), %rcx
	movq	16(%rbx), %rdx
	leaq	1(%rax), %rsi
	movq	%rsi, 40(%rbx)
	movb	%cl, (%rdx,%rax)
	movq	40(%rbx), %rdx
	movq	96(%r12), %rax
	movq	16(%rbx), %rcx
	leaq	1(%rdx), %rsi
	shrq	$8, %rax
	movq	%rsi, 40(%rbx)
	movb	%al, (%rcx,%rdx)
	movq	40(%rbx), %rdx
	movq	96(%r12), %rax
	movq	16(%rbx), %rcx
	leaq	1(%rdx), %rsi
	shrq	$16, %rax
	movq	%rsi, 40(%rbx)
	movb	%al, (%rcx,%rdx)
	movq	40(%rbx), %rdx
	movq	96(%r12), %rax
	movq	16(%rbx), %rcx
	leaq	1(%rdx), %rsi
	shrq	$24, %rax
	movq	%rsi, 40(%rbx)
	movb	%al, (%rcx,%rdx)
	movq	40(%rbx), %rax
	movq	16(%r12), %rcx
	movq	16(%rbx), %rdx
	leaq	1(%rax), %rsi
	movq	%rsi, 40(%rbx)
	movb	%cl, (%rdx,%rax)
	movq	16(%r12), %rax
	movq	40(%rbx), %rdx
	movq	16(%rbx), %rcx
	shrq	$8, %rax
	leaq	1(%rdx), %rsi
	movq	%rsi, 40(%rbx)
	movb	%al, (%rcx,%rdx)
	movq	16(%rbx), %rcx
	movq	16(%r12), %rax
	movq	40(%rbx), %rdx
	shrq	$16, %rax
	leaq	1(%rdx), %rsi
	movq	%rsi, 40(%rbx)
	movb	%al, (%rcx,%rdx)
	movq	16(%r12), %rax
	movq	40(%rbx), %rdx
	movq	16(%rbx), %rcx
	shrq	$24, %rax
	leaq	1(%rdx), %rsi
	movq	%rsi, 40(%rbx)
	movb	%al, (%rcx,%rdx)
	jmp	.L472
.L613:
	movq	-56(%rbp), %rdi
	call	_tr_align
	jmp	.L467
.L459:
	movl	$666, 8(%rcx)
	jmp	.L478
.L631:
	movq	232(%rax), %rax
	movl	252(%rcx), %edx
	xorl	%esi, %esi
	subq	%rax, %rdx
	testq	%rax, %rax
	js	.L455
	movl	%eax, %eax
	addq	176(%rcx), %rax
	movq	%rax, %rsi
.L455:
	movq	-56(%rbp), %rbx
	movl	$1, %ecx
	movq	%rbx, %rdi
	call	_tr_flush_block
	movq	(%rbx), %rcx
	movl	252(%rbx), %eax
	movq	56(%rcx), %r13
	movq	%rax, 232(%rbx)
	movq	%rcx, -64(%rbp)
	movq	%r13, %rdi
	call	_tr_flush_bits
	movq	-64(%rbp), %rcx
	movq	40(%r13), %r14
	cmpl	%r14d, 32(%rcx)
	cmovbe	32(%rcx), %r14d
	testl	%r14d, %r14d
	je	.L457
	movl	%r14d, %r15d
	movq	24(%rcx), %rdi
	movq	32(%r13), %rsi
	movq	%r15, %rdx
	call	memcpy@PLT
	movq	-64(%rbp), %rcx
	addq	%r15, 24(%rcx)
	addq	%r15, 32(%r13)
	addq	%r15, 40(%rcx)
	subl	%r14d, 32(%rcx)
	subq	%r15, 40(%r13)
	jne	.L457
	movq	16(%r13), %rax
	movq	%rax, 32(%r13)
	jmp	.L457
.L626:
	addq	$4, %rdx
	jmp	.L437
.L625:
	addq	$3, %rdx
	jmp	.L437
.L624:
	addq	$2, %rdx
	jmp	.L437
.L627:
	addq	$5, %rdx
	jmp	.L437
.L629:
	addq	$7, %rdx
	jmp	.L437
.L628:
	addq	$6, %rdx
	jmp	.L437
	.cfi_endproc
.LFE706:
	.size	deflate.part.0, .-deflate.part.0
	.p2align 4
	.globl	deflateSetDictionary
	.type	deflateSetDictionary, @function
deflateSetDictionary:
.LFB678:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L648
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	cmpq	$0, 64(%rdi)
	je	.L659
	cmpq	$0, 72(%rdi)
	je	.L659
	movq	56(%rdi), %rbx
	movl	$-2, %eax
	testq	%rbx, %rbx
	je	.L632
	cmpq	(%rbx), %rdi
	jne	.L632
	movl	8(%rbx), %eax
	movl	%edx, %r14d
	movq	%rsi, %r8
	movl	$1, %edx
	leal	-42(%rax), %ecx
	cmpl	$61, %ecx
	ja	.L634
	movabsq	$2306405961448849409, %rdx
	shrq	%cl, %rdx
	notq	%rdx
	andl	$1, %edx
.L634:
	cmpl	$666, %eax
	setne	%sil
	cmpl	$113, %eax
	setne	%cl
	testb	%cl, %sil
	je	.L660
	testb	%dl, %dl
	jne	.L659
.L660:
	testq	%r8, %r8
	je	.L659
	movl	48(%rbx), %r13d
	cmpl	$2, %r13d
	je	.L659
	cmpl	$1, %r13d
	je	.L674
	movl	260(%rbx), %eax
	testl	%eax, %eax
	jne	.L659
	movl	160(%rbx), %eax
	movl	$0, 48(%rbx)
	cmpl	%r14d, %eax
	ja	.L640
	testl	%r13d, %r13d
	je	.L675
.L638:
	subl	%eax, %r14d
	addq	%r14, %r8
.L639:
	movl	8(%r12), %r14d
	movq	(%r12), %r15
	movl	%eax, 8(%r12)
	movq	%r8, (%r12)
	.p2align 4,,10
	.p2align 3
.L673:
	movq	%rbx, %rdi
	call	fill_window
	movl	260(%rbx), %eax
	cmpl	$2, %eax
	jbe	.L641
	movl	252(%rbx), %r8d
	leal	-2(%rax,%r8), %r9d
	jmp	.L644
	.p2align 4,,10
	.p2align 3
.L642:
	movq	176(%rbx), %rdx
	movl	224(%rbx), %ecx
	andl	168(%rbx), %esi
	movzbl	2(%rdx,%rax), %eax
	movl	208(%rbx), %edx
	sall	%cl, %edx
	xorl	%edx, %eax
	movq	200(%rbx), %rdx
	andl	220(%rbx), %eax
	movl	%eax, 208(%rbx)
	leaq	(%rdx,%rax,2), %rax
	movq	192(%rbx), %rdx
	movzwl	(%rax), %ecx
	movw	%cx, (%rdx,%rsi,2)
	movw	%r8w, (%rax)
	addl	$1, %r8d
	cmpl	%r9d, %r8d
	je	.L676
.L644:
	movl	x86_cpu_enable_simd(%rip), %ecx
	movzwl	%r8w, %esi
	movzwl	%r8w, %eax
	testl	%ecx, %ecx
	je	.L642
	movq	%rbx, %rdi
	call	insert_string_optimized
	addl	$1, %r8d
	cmpl	%r9d, %r8d
	jne	.L644
.L676:
	movl	%r8d, 252(%rbx)
	movl	$2, 260(%rbx)
	jmp	.L673
.L659:
	movl	$-2, %eax
.L632:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L641:
	.cfi_restore_state
	movl	252(%rbx), %edx
	movl	%eax, 6012(%rbx)
	movl	$2, 240(%rbx)
	movl	$0, 248(%rbx)
	addl	%eax, %edx
	movabsq	$8589934592, %rax
	movl	%edx, %edi
	movq	%rax, 260(%rbx)
	xorl	%eax, %eax
	movl	%edx, 252(%rbx)
	movq	%rdi, 232(%rbx)
	movq	%r15, (%r12)
	movl	%r14d, 8(%r12)
	movl	%r13d, 48(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L674:
	.cfi_restore_state
	cmpl	$42, %eax
	jne	.L659
	movl	260(%rbx), %edx
	movl	$-2, %eax
	testl	%edx, %edx
	jne	.L632
	movq	96(%r12), %rdi
	movq	%r8, %rsi
	movl	%r14d, %edx
	movq	%r8, -56(%rbp)
	call	adler32@PLT
	movq	-56(%rbp), %r8
	movq	%rax, 96(%r12)
	movl	160(%rbx), %eax
	movl	$0, 48(%rbx)
	cmpl	%eax, %r14d
	jnb	.L638
.L640:
	movl	%r14d, %eax
	jmp	.L639
.L675:
	movl	212(%rbx), %eax
	movq	200(%rbx), %rdi
	xorl	%esi, %esi
	movq	%r8, -56(%rbp)
	subl	$1, %eax
	movw	%si, (%rdi,%rax,2)
	leaq	(%rax,%rax), %rdx
	xorl	%esi, %esi
	call	memset@PLT
	movl	160(%rbx), %eax
	movq	-56(%rbp), %r8
	movl	$0, 252(%rbx)
	movq	$0, 232(%rbx)
	movl	$0, 6012(%rbx)
	jmp	.L638
.L648:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	movl	$-2, %eax
	ret
	.cfi_endproc
.LFE678:
	.size	deflateSetDictionary, .-deflateSetDictionary
	.p2align 4
	.globl	deflateGetDictionary
	.type	deflateGetDictionary, @function
deflateGetDictionary:
.LFB679:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L684
	cmpq	$0, 64(%rdi)
	je	.L684
	cmpq	$0, 72(%rdi)
	je	.L684
	movq	56(%rdi), %rax
	movl	$-2, %r8d
	testq	%rax, %rax
	je	.L702
	cmpq	(%rax), %rdi
	jne	.L702
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movl	8(%rax), %esi
	movq	%rdx, %rbx
	movl	$1, %edx
	leal	-42(%rsi), %ecx
	cmpl	$61, %ecx
	ja	.L679
	movabsq	$2306405961448849409, %rdx
	shrq	%cl, %rdx
	notq	%rdx
	andl	$1, %edx
.L679:
	cmpl	$666, %esi
	setne	%dil
	cmpl	$113, %esi
	setne	%cl
	testb	%cl, %dil
	je	.L690
	testb	%dl, %dl
	jne	.L688
.L690:
	movl	252(%rax), %ecx
	movl	260(%rax), %edi
	leal	(%rcx,%rdi), %edx
	cmpl	%edx, 160(%rax)
	cmovbe	160(%rax), %edx
	movl	%edx, %r12d
	testq	%r9, %r9
	je	.L681
	testl	%edx, %edx
	jne	.L705
.L681:
	xorl	%r8d, %r8d
	testq	%rbx, %rbx
	je	.L677
	movl	%r12d, (%rbx)
.L677:
	popq	%rbx
	movl	%r8d, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L684:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	movl	$-2, %r8d
.L702:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L705:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	movl	%ecx, %esi
	movl	%edx, %edx
	addq	%rdi, %rsi
	movq	%r9, %rdi
	subq	%rdx, %rsi
	addq	176(%rax), %rsi
	call	memcpy@PLT
	jmp	.L681
.L688:
	movl	$-2, %r8d
	jmp	.L677
	.cfi_endproc
.LFE679:
	.size	deflateGetDictionary, .-deflateGetDictionary
	.p2align 4
	.globl	deflateResetKeep
	.type	deflateResetKeep, @function
deflateResetKeep:
.LFB680:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L715
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	cmpq	$0, 64(%rdi)
	movq	%rdi, %rbx
	je	.L721
	cmpq	$0, 72(%rdi)
	je	.L721
	movq	56(%rdi), %r12
	movl	$-2, %eax
	testq	%r12, %r12
	je	.L706
	cmpq	(%r12), %rdi
	jne	.L706
	movl	8(%r12), %edx
	movl	$1, %eax
	leal	-42(%rdx), %ecx
	cmpl	$61, %ecx
	ja	.L708
	movabsq	$2306405961448849409, %rax
	shrq	%cl, %rax
	notq	%rax
	andl	$1, %eax
.L708:
	cmpl	$666, %edx
	setne	%cl
	cmpl	$113, %edx
	setne	%dl
	testb	%dl, %cl
	je	.L722
	testb	%al, %al
	jne	.L721
.L722:
	movq	16(%r12), %rax
	movq	$0, 40(%rbx)
	movq	$0, 16(%rbx)
	movq	$0, 48(%rbx)
	movl	$2, 88(%rbx)
	movq	%rax, 32(%r12)
	movl	48(%r12), %eax
	movq	$0, 40(%r12)
	testl	%eax, %eax
	js	.L733
	cmpl	$2, %eax
	je	.L712
	testl	%eax, %eax
	jne	.L711
	movl	$113, %eax
.L713:
	movl	%eax, 8(%r12)
	xorl	%edx, %edx
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	adler32@PLT
.L714:
	movq	%rax, 96(%rbx)
	movq	%r12, %rdi
	movl	$0, 76(%r12)
	call	_tr_init
	xorl	%eax, %eax
.L706:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L721:
	.cfi_restore_state
	popq	%rbx
	movl	$-2, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L733:
	.cfi_restore_state
	movl	%eax, %edx
	negl	%edx
	movl	%edx, 48(%r12)
	cmpl	$-2, %eax
	je	.L712
.L711:
	movl	$42, %eax
	jmp	.L713
	.p2align 4,,10
	.p2align 3
.L712:
	movl	$57, 8(%r12)
	xorl	%edx, %edx
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	crc32@PLT
	jmp	.L714
	.p2align 4,,10
	.p2align 3
.L715:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	movl	$-2, %eax
	ret
	.cfi_endproc
.LFE680:
	.size	deflateResetKeep, .-deflateResetKeep
	.p2align 4
	.globl	deflateReset
	.type	deflateReset, @function
deflateReset:
.LFB681:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L743
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	cmpq	$0, 64(%rdi)
	movq	%rdi, %rbx
	je	.L749
	cmpq	$0, 72(%rdi)
	je	.L749
	movq	56(%rdi), %r12
	movl	$-2, %eax
	testq	%r12, %r12
	je	.L734
	cmpq	(%r12), %rdi
	jne	.L734
	movl	8(%r12), %edx
	movl	$1, %eax
	leal	-42(%rdx), %ecx
	cmpl	$61, %ecx
	ja	.L736
	movabsq	$2306405961448849409, %rax
	shrq	%cl, %rax
	notq	%rax
	andl	$1, %eax
.L736:
	cmpl	$666, %edx
	setne	%cl
	cmpl	$113, %edx
	setne	%dl
	testb	%dl, %cl
	je	.L750
	testb	%al, %al
	jne	.L749
.L750:
	movq	16(%r12), %rax
	movq	$0, 40(%rbx)
	movq	$0, 16(%rbx)
	movq	$0, 48(%rbx)
	movl	$2, 88(%rbx)
	movq	%rax, 32(%r12)
	movl	48(%r12), %eax
	movq	$0, 40(%r12)
	testl	%eax, %eax
	js	.L761
	cmpl	$2, %eax
	je	.L740
	testl	%eax, %eax
	jne	.L739
	movl	$113, %eax
.L741:
	movl	%eax, 8(%r12)
	xorl	%edx, %edx
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	adler32@PLT
.L742:
	movq	%rax, 96(%rbx)
	movq	%r12, %rdi
	movl	$0, 76(%r12)
	call	_tr_init
	movq	56(%rbx), %rbx
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	movl	160(%rbx), %eax
	movq	200(%rbx), %rdi
	addq	%rax, %rax
	movq	%rax, 184(%rbx)
	movl	212(%rbx), %eax
	subl	$1, %eax
	movw	%cx, (%rdi,%rax,2)
	leaq	(%rax,%rax), %rdx
	call	memset@PLT
	movslq	276(%rbx), %rax
	movq	$0, 232(%rbx)
	movl	$0, 6012(%rbx)
	movl	$2, 240(%rbx)
	salq	$4, %rax
	movq	$0, 248(%rbx)
	movq	%rax, %rdx
	leaq	configuration_table(%rip), %rax
	movl	$0, 208(%rbx)
	addq	%rdx, %rax
	movzwl	2(%rax), %edx
	movl	%edx, 272(%rbx)
	movzwl	(%rax), %edx
	movl	%edx, 284(%rbx)
	movzwl	4(%rax), %edx
	movzwl	6(%rax), %eax
	movl	%edx, 288(%rbx)
	movl	%eax, 268(%rbx)
	movabsq	$8589934592, %rax
	movq	%rax, 260(%rbx)
	xorl	%eax, %eax
.L734:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L749:
	.cfi_restore_state
	popq	%rbx
	movl	$-2, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L761:
	.cfi_restore_state
	movl	%eax, %edx
	negl	%edx
	movl	%edx, 48(%r12)
	cmpl	$-2, %eax
	je	.L740
.L739:
	movl	$42, %eax
	jmp	.L741
	.p2align 4,,10
	.p2align 3
.L740:
	movl	$57, 8(%r12)
	xorl	%edx, %edx
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	crc32@PLT
	jmp	.L742
	.p2align 4,,10
	.p2align 3
.L743:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	movl	$-2, %eax
	ret
	.cfi_endproc
.LFE681:
	.size	deflateReset, .-deflateReset
	.p2align 4
	.globl	deflateSetHeader
	.type	deflateSetHeader, @function
deflateSetHeader:
.LFB682:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L773
	cmpq	$0, 64(%rdi)
	je	.L773
	cmpq	$0, 72(%rdi)
	je	.L773
	movq	56(%rdi), %rax
	movl	$-2, %r8d
	testq	%rax, %rax
	je	.L762
	cmpq	(%rax), %rdi
	jne	.L762
	movl	8(%rax), %edi
	movl	$1, %edx
	leal	-42(%rdi), %ecx
	cmpl	$61, %ecx
	ja	.L764
	movabsq	$2306405961448849409, %rdx
	shrq	%cl, %rdx
	notq	%rdx
	andl	$1, %edx
.L764:
	cmpl	$113, %edi
	setne	%r8b
	cmpl	$666, %edi
	setne	%cl
	testb	%cl, %r8b
	je	.L774
	testb	%dl, %dl
	jne	.L773
.L774:
	cmpl	$2, 48(%rax)
	jne	.L773
	movq	%rsi, 56(%rax)
	xorl	%r8d, %r8d
.L762:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L773:
	movl	$-2, %r8d
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE682:
	.size	deflateSetHeader, .-deflateSetHeader
	.p2align 4
	.globl	deflatePending
	.type	deflatePending, @function
deflatePending:
.LFB683:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L789
	cmpq	$0, 64(%rdi)
	je	.L789
	cmpq	$0, 72(%rdi)
	je	.L789
	movq	56(%rdi), %r8
	movl	$-2, %eax
	testq	%r8, %r8
	je	.L786
	cmpq	(%r8), %rdi
	je	.L799
.L778:
	ret
	.p2align 4,,10
	.p2align 3
.L799:
	movl	8(%r8), %edi
	movl	$1, %eax
	leal	-42(%rdi), %ecx
	cmpl	$61, %ecx
	ja	.L780
	movabsq	$2306405961448849409, %rax
	shrq	%cl, %rax
	notq	%rax
	andl	$1, %eax
.L780:
	cmpl	$666, %edi
	setne	%r9b
	cmpl	$113, %edi
	setne	%cl
	testb	%cl, %r9b
	je	.L791
	testb	%al, %al
	jne	.L789
.L791:
	testq	%rsi, %rsi
	je	.L782
	movq	40(%r8), %rax
	movl	%eax, (%rsi)
.L782:
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L778
	movl	6020(%r8), %ecx
	movl	%ecx, (%rdx)
	ret
	.p2align 4,,10
	.p2align 3
.L789:
	movl	$-2, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L786:
	ret
	.cfi_endproc
.LFE683:
	.size	deflatePending, .-deflatePending
	.p2align 4
	.globl	deflatePrime
	.type	deflatePrime, @function
deflatePrime:
.LFB684:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%rdi, %rdi
	je	.L811
	cmpq	$0, 64(%rdi)
	je	.L811
	cmpq	$0, 72(%rdi)
	je	.L811
	movq	56(%rdi), %r15
	testq	%r15, %r15
	je	.L811
	cmpq	(%r15), %rdi
	jne	.L811
	movl	%edx, %r14d
	movl	8(%r15), %edx
	movl	%esi, %r13d
	movl	$1, %eax
	leal	-42(%rdx), %ecx
	cmpl	$61, %ecx
	ja	.L802
	movabsq	$2306405961448849409, %rax
	shrq	%cl, %rax
	notq	%rax
	andl	$1, %eax
.L802:
	cmpl	$666, %edx
	setne	%cl
	cmpl	$113, %edx
	setne	%dl
	testb	%dl, %cl
	je	.L813
	testb	%al, %al
	jne	.L811
.L813:
	movq	32(%r15), %rax
	addq	$2, %rax
	cmpq	%rax, 5968(%r15)
	jb	.L812
	movl	$16, %ebx
	.p2align 4,,10
	.p2align 3
.L804:
	movl	6020(%r15), %r8d
	movl	%ebx, %r12d
	movl	$1, %edi
	subl	%r8d, %r12d
	cmpl	%r12d, %r13d
	cmovle	%r13d, %r12d
	movl	%r12d, %ecx
	sall	%cl, %edi
	movl	%r8d, %ecx
	addl	%r12d, %r8d
	subl	$1, %edi
	movl	%r8d, 6020(%r15)
	andl	%r14d, %edi
	sall	%cl, %edi
	orw	%di, 6016(%r15)
	movq	%r15, %rdi
	call	_tr_flush_bits
	movl	%r12d, %ecx
	sarl	%cl, %r14d
	subl	%r12d, %r13d
	jne	.L804
	jmp	.L801
	.p2align 4,,10
	.p2align 3
.L811:
	movl	$-2, %r13d
.L801:
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L812:
	.cfi_restore_state
	movl	$-5, %r13d
	jmp	.L801
	.cfi_endproc
.LFE684:
	.size	deflatePrime, .-deflatePrime
	.p2align 4
	.globl	deflateParams
	.type	deflateParams, @function
deflateParams:
.LFB685:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L852
	cmpq	$0, 64(%rdi)
	je	.L852
	cmpq	$0, 72(%rdi)
	je	.L852
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	56(%rdi), %r12
	testq	%r12, %r12
	je	.L822
	cmpq	(%r12), %rdi
	jne	.L822
	movslq	%esi, %rbx
	movl	8(%r12), %esi
	movl	%edx, %r13d
	movl	$1, %edx
	leal	-42(%rsi), %ecx
	cmpl	$61, %ecx
	ja	.L823
	movabsq	$2306405961448849409, %rax
	shrq	%cl, %rax
	notq	%rax
	movl	%eax, %edx
	andl	$1, %edx
.L823:
	cmpl	$666, %esi
	setne	%al
	cmpl	$113, %esi
	setne	%cl
	andl	%ecx, %eax
	andb	%dl, %al
	jne	.L822
	cmpl	$-1, %ebx
	je	.L840
	cmpl	$9, %ebx
	seta	%al
.L824:
	cmpl	$4, %r13d
	ja	.L822
	testb	%al, %al
	jne	.L822
	movl	276(%r12), %eax
	cmpl	%r13d, 280(%r12)
	je	.L855
.L825:
	cmpq	$0, 6024(%r12)
	jne	.L856
.L826:
	cmpl	%eax, %ebx
	je	.L827
	testl	%eax, %eax
	jne	.L828
	movl	6008(%r12), %eax
	testl	%eax, %eax
	je	.L828
	movl	212(%r12), %esi
	movq	200(%r12), %rdi
	leal	-1(%rsi), %edx
	leaq	(%rdx,%rdx), %r8
	cmpl	$1, %eax
	je	.L857
	xorl	%eax, %eax
	movw	%ax, (%rdi,%rdx,2)
.L836:
	movq	%r8, %rdx
	xorl	%esi, %esi
	call	memset@PLT
	movl	$0, 6008(%r12)
.L828:
	leaq	configuration_table(%rip), %rax
	movl	%ebx, 276(%r12)
	salq	$4, %rbx
	addq	%rax, %rbx
	movzwl	2(%rbx), %eax
	movl	%eax, 272(%r12)
	movzwl	(%rbx), %eax
	movl	%eax, 284(%r12)
	movzwl	4(%rbx), %eax
	movl	%eax, 288(%r12)
	movzwl	6(%rbx), %eax
	movl	%eax, 268(%r12)
.L827:
	movl	%r13d, 280(%r12)
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L822:
	.cfi_restore_state
	movl	$-2, %eax
.L819:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L840:
	.cfi_restore_state
	movl	$6, %ebx
	jmp	.L824
	.p2align 4,,10
	.p2align 3
.L855:
	movslq	%ebx, %rcx
	movslq	%eax, %rdx
	leaq	configuration_table(%rip), %rsi
	salq	$4, %rdx
	salq	$4, %rcx
	movq	8(%rsi,%rdx), %rdx
	cmpq	%rdx, 8(%rsi,%rcx)
	jne	.L825
	jmp	.L826
	.p2align 4,,10
	.p2align 3
.L856:
	movl	$5, %esi
	movq	%rdi, -40(%rbp)
	call	deflate.part.0
	cmpl	$-2, %eax
	je	.L822
	movq	-40(%rbp), %rdi
	movl	32(%rdi), %r10d
	testl	%r10d, %r10d
	je	.L841
	movl	276(%r12), %eax
	jmp	.L826
	.p2align 4,,10
	.p2align 3
.L852:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	movl	$-2, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L857:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	leaq	(%rdi,%rsi,2), %rax
	notq	%rdx
	movl	160(%r12), %ecx
	leaq	(%rax,%rdx,2), %rsi
	.p2align 4,,10
	.p2align 3
.L833:
	movzwl	-2(%rax), %edx
	subq	$2, %rax
	cmpl	%ecx, %edx
	jb	.L830
	subl	%ecx, %edx
	movw	%dx, (%rax)
	cmpq	%rsi, %rax
	jne	.L833
.L832:
	movq	192(%r12), %rdx
	movl	%ecx, %eax
	leaq	(%rdx,%rax,2), %rax
	leal	-1(%rcx), %edx
	notq	%rdx
	leaq	(%rax,%rdx,2), %rsi
	.p2align 4,,10
	.p2align 3
.L837:
	movzwl	-2(%rax), %edx
	subq	$2, %rax
	cmpl	%edx, %ecx
	ja	.L834
	subl	%ecx, %edx
	movw	%dx, (%rax)
	cmpq	%rax, %rsi
	jne	.L837
	jmp	.L836
	.p2align 4,,10
	.p2align 3
.L830:
	xorl	%r9d, %r9d
	movw	%r9w, (%rax)
	cmpq	%rsi, %rax
	jne	.L833
	jmp	.L832
	.p2align 4,,10
	.p2align 3
.L834:
	xorl	%edx, %edx
	movw	%dx, (%rax)
	cmpq	%rax, %rsi
	jne	.L837
	jmp	.L836
.L841:
	movl	$-5, %eax
	jmp	.L819
	.cfi_endproc
.LFE685:
	.size	deflateParams, .-deflateParams
	.p2align 4
	.globl	deflateTune
	.type	deflateTune, @function
deflateTune:
.LFB686:
	.cfi_startproc
	endbr64
	movl	%ecx, %r10d
	testq	%rdi, %rdi
	je	.L868
	cmpq	$0, 64(%rdi)
	je	.L868
	cmpq	$0, 72(%rdi)
	je	.L868
	movq	56(%rdi), %rax
	movl	$-2, %r9d
	testq	%rax, %rax
	je	.L858
	cmpq	(%rax), %rdi
	jne	.L858
	movl	8(%rax), %r9d
	movl	$1, %edi
	leal	-42(%r9), %ecx
	cmpl	$61, %ecx
	ja	.L860
	movabsq	$2306405961448849409, %rdi
	shrq	%cl, %rdi
	notq	%rdi
	andl	$1, %edi
.L860:
	cmpl	$113, %r9d
	setne	%r11b
	cmpl	$666, %r9d
	setne	%cl
	testb	%cl, %r11b
	je	.L869
	testb	%dil, %dil
	jne	.L868
.L869:
	movl	%esi, 284(%rax)
	xorl	%r9d, %r9d
	movl	%edx, 272(%rax)
	movl	%r10d, 288(%rax)
	movl	%r8d, 268(%rax)
.L858:
	movl	%r9d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L868:
	movl	$-2, %r9d
	movl	%r9d, %eax
	ret
	.cfi_endproc
.LFE686:
	.size	deflateTune, .-deflateTune
	.p2align 4
	.globl	deflateBound
	.type	deflateBound, @function
deflateBound:
.LFB687:
	.cfi_startproc
	endbr64
	leaq	7(%rsi), %r9
	leaq	63(%rsi), %rax
	movq	%r9, %rdx
	shrq	$6, %rax
	shrq	$3, %rdx
	addq	%rdx, %rax
	addq	%rsi, %rax
	testq	%rdi, %rdi
	je	.L874
	cmpq	$0, 64(%rdi)
	je	.L874
	cmpq	$0, 72(%rdi)
	je	.L874
	movq	56(%rdi), %r8
	testq	%r8, %r8
	je	.L874
	cmpq	(%r8), %rdi
	jne	.L874
	movl	8(%r8), %edi
	movl	$1, %edx
	leal	-42(%rdi), %ecx
	cmpl	$61, %ecx
	ja	.L875
	movabsq	$2306405961448849409, %rdx
	shrq	%cl, %rdx
	notq	%rdx
	andl	$1, %edx
.L875:
	cmpl	$666, %edi
	setne	%r10b
	cmpl	$113, %edi
	setne	%cl
	testb	%cl, %r10b
	je	.L876
	testb	%dl, %dl
	jne	.L874
.L876:
	movl	48(%r8), %edx
	cmpl	$1, %edx
	je	.L878
	cmpl	$2, %edx
	je	.L879
	cmpl	$1, %edx
	sbbq	%rdx, %rdx
	notq	%rdx
	andl	$6, %edx
.L880:
	cmpl	$15, 164(%r8)
	jne	.L886
	cmpl	$15, 216(%r8)
	jne	.L886
	movq	%rsi, %rcx
	movq	%rsi, %rax
	shrq	$25, %rsi
	shrq	$12, %rcx
	shrq	$14, %rax
	addq	%r9, %rcx
	addq	%rax, %rcx
	leaq	(%rcx,%rsi), %rax
	addq	%rdx, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L874:
	addq	$11, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L886:
	leaq	5(%rax,%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L879:
	movq	56(%r8), %rdi
	movl	$18, %edx
	testq	%rdi, %rdi
	je	.L880
	cmpq	$0, 24(%rdi)
	je	.L881
	movl	32(%rdi), %edx
	leal	2(%rdx), %ecx
	leaq	18(%rcx), %rdx
.L881:
	movq	40(%rdi), %rcx
	testq	%rcx, %rcx
	je	.L882
	subq	%rdx, %rcx
	.p2align 4,,10
	.p2align 3
.L883:
	addq	$1, %rdx
	cmpb	$0, -1(%rcx,%rdx)
	jne	.L883
.L882:
	movq	56(%rdi), %rcx
	testq	%rcx, %rcx
	je	.L884
	subq	%rdx, %rcx
	.p2align 4,,10
	.p2align 3
.L885:
	addq	$1, %rdx
	cmpb	$0, -1(%rcx,%rdx)
	jne	.L885
.L884:
	movl	68(%rdi), %ecx
	testl	%ecx, %ecx
	je	.L880
	addq	$2, %rdx
	jmp	.L880
	.p2align 4,,10
	.p2align 3
.L878:
	cmpl	$1, 252(%r8)
	sbbq	%rdx, %rdx
	andq	$-4, %rdx
	addq	$10, %rdx
	jmp	.L880
	.cfi_endproc
.LFE687:
	.size	deflateBound, .-deflateBound
	.p2align 4
	.globl	deflate
	.type	deflate, @function
deflate:
.LFB690:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L912
	cmpq	$0, 64(%rdi)
	je	.L912
	cmpq	$0, 72(%rdi)
	je	.L912
	movq	56(%rdi), %rax
	testq	%rax, %rax
	je	.L912
	cmpq	(%rax), %rdi
	je	.L930
.L912:
	movl	$-2, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L930:
	movl	8(%rax), %edx
	movl	$1, %eax
	leal	-42(%rdx), %ecx
	cmpl	$61, %ecx
	ja	.L914
	movabsq	$2306405961448849409, %rax
	shrq	%cl, %rax
	notq	%rax
	andl	$1, %eax
.L914:
	cmpl	$666, %edx
	setne	%cl
	cmpl	$113, %edx
	setne	%dl
	testb	%dl, %cl
	je	.L924
	testb	%al, %al
	jne	.L912
.L924:
	cmpl	$5, %esi
	ja	.L912
	jmp	deflate.part.0
	.cfi_endproc
.LFE690:
	.size	deflate, .-deflate
	.p2align 4
	.globl	deflateEnd
	.type	deflateEnd, @function
deflateEnd:
.LFB691:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L939
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	cmpq	$0, 64(%rdi)
	movq	%rdi, %rbx
	je	.L945
	movq	72(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L945
	movq	56(%rdi), %rsi
	movl	$-2, %eax
	testq	%rsi, %rsi
	je	.L931
	cmpq	(%rsi), %rdi
	jne	.L931
	movl	8(%rsi), %r12d
	movl	$1, %eax
	leal	-42(%r12), %ecx
	cmpl	$61, %ecx
	ja	.L933
	movabsq	$2306405961448849409, %rax
	shrq	%cl, %rax
	notq	%rax
	andl	$1, %eax
.L933:
	cmpl	$666, %r12d
	setne	%dil
	cmpl	$113, %r12d
	setne	%cl
	testb	%cl, %dil
	je	.L947
	testb	%al, %al
	jne	.L945
.L947:
	movq	16(%rsi), %r8
	movq	80(%rbx), %rdi
	testq	%r8, %r8
	je	.L935
	movq	%r8, %rsi
	call	*%rdx
	movq	56(%rbx), %rsi
	movq	72(%rbx), %rdx
	movq	80(%rbx), %rdi
.L935:
	movq	200(%rsi), %r8
	testq	%r8, %r8
	je	.L936
	movq	%r8, %rsi
	call	*%rdx
	movq	56(%rbx), %rsi
	movq	72(%rbx), %rdx
	movq	80(%rbx), %rdi
.L936:
	movq	192(%rsi), %r8
	testq	%r8, %r8
	je	.L937
	movq	%r8, %rsi
	call	*%rdx
	movq	56(%rbx), %rsi
	movq	72(%rbx), %rdx
	movq	80(%rbx), %rdi
.L937:
	movq	176(%rsi), %r8
	testq	%r8, %r8
	je	.L938
	movq	%r8, %rsi
	call	*%rdx
	movq	72(%rbx), %rdx
	movq	56(%rbx), %rsi
	movq	80(%rbx), %rdi
.L938:
	call	*%rdx
	xorl	%eax, %eax
	movq	$0, 56(%rbx)
	cmpl	$113, %r12d
	setne	%al
	leal	-3(%rax,%rax,2), %eax
.L931:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L945:
	.cfi_restore_state
	popq	%rbx
	movl	$-2, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L939:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	movl	$-2, %eax
	ret
	.cfi_endproc
.LFE691:
	.size	deflateEnd, .-deflateEnd
	.p2align 4
	.globl	deflateInit2_
	.type	deflateInit2_, @function
deflateInit2_:
.LFB676:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%r8d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$24, %rsp
	movq	16(%rbp), %r15
	movl	%ecx, -56(%rbp)
	movl	%r9d, -52(%rbp)
	call	x86_check_features@PLT
	testq	%r15, %r15
	je	.L983
	cmpb	$49, (%r15)
	setne	%al
	cmpl	$112, 24(%rbp)
	setne	%dl
	orb	%dl, %al
	jne	.L983
	testq	%r14, %r14
	movl	-56(%rbp), %ecx
	je	.L975
	movq	64(%r14), %r9
	movq	$0, 48(%r14)
	testq	%r9, %r9
	je	.L993
	cmpq	$0, 72(%r14)
	je	.L994
.L971:
	cmpl	$-1, %r12d
	movl	$6, %edx
	cmove	%edx, %r12d
	testl	%ecx, %ecx
	js	.L995
	movl	$1, %r8d
	cmpl	$15, %ecx
	jg	.L996
.L974:
	leal	-1(%r13), %edx
	cmpl	$8, %edx
	ja	.L975
	leal	-8(%rcx), %edx
	cmpl	$7, %edx
	ja	.L975
	cmpl	$8, %ebx
	jne	.L975
	cmpl	$9, %r12d
	ja	.L975
	cmpl	$4, -52(%rbp)
	ja	.L975
	cmpl	$8, %ecx
	jne	.L988
	testb	%al, %al
	jne	.L975
.L988:
	cmpl	$8, %ecx
	movl	%r8d, -56(%rbp)
	movq	80(%r14), %rdi
	movl	$6032, %edx
	movl	$9, %r15d
	movl	$1, %esi
	cmovne	%ecx, %r15d
	call	*%r9
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L986
	movl	-56(%rbp), %r8d
	movq	%rax, 56(%r14)
	movl	%r15d, %ecx
	movq	%r14, (%rax)
	movl	x86_cpu_enable_simd(%rip), %edi
	movl	$42, 8(%rax)
	movl	%r8d, 48(%rax)
	movq	$0, 56(%rax)
	movl	%r15d, 164(%rax)
	movl	$1, %eax
	movl	%eax, %esi
	sall	%cl, %esi
	leal	-1(%rsi), %edx
	movl	%esi, 160(%rbx)
	movl	%edx, 168(%rbx)
	testl	%edi, %edi
	jne	.L987
	leal	7(%r13), %ecx
	leal	9(%r13), %edx
	sall	%cl, %eax
	leal	-1(%rax), %edi
	movd	%edi, %xmm1
	movl	$2863311531, %edi
	imulq	%rdi, %rdx
	shrq	$33, %rdx
	movq	%rdx, %xmm0
.L978:
	punpckldq	%xmm0, %xmm1
	movd	%ecx, %xmm2
	movd	%eax, %xmm0
	addl	$8, %esi
	punpckldq	%xmm2, %xmm0
	movq	80(%r14), %rdi
	movl	$2, %edx
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 212(%rbx)
	call	*64(%r14)
	movl	160(%rbx), %esi
	movq	80(%r14), %rdi
	movl	$2, %edx
	movq	%rax, 176(%rbx)
	call	*64(%r14)
	movl	212(%rbx), %esi
	movq	80(%r14), %rdi
	movl	$2, %edx
	movq	%rax, 192(%rbx)
	call	*64(%r14)
	leal	6(%r13), %ecx
	movl	$1, %esi
	movq	$0, 6024(%rbx)
	sall	%cl, %esi
	movq	%rax, 200(%rbx)
	movq	80(%r14), %rdi
	movl	$4, %edx
	movl	%esi, 5976(%rbx)
	call	*64(%r14)
	movl	5976(%rbx), %ecx
	cmpq	$0, 176(%rbx)
	movq	%rax, 16(%rbx)
	leaq	0(,%rcx,4), %rsi
	movq	%rsi, 24(%rbx)
	je	.L979
	cmpq	$0, 192(%rbx)
	je	.L979
	cmpq	$0, 200(%rbx)
	je	.L979
	testq	%rax, %rax
	je	.L979
	addq	%rcx, %rax
	movl	%r12d, 276(%rbx)
	movq	%r14, %rdi
	movq	%rax, 5968(%rbx)
	leal	-3(%rcx,%rcx,2), %eax
	movl	%eax, 5984(%rbx)
	movl	-52(%rbp), %eax
	movb	$8, 72(%rbx)
	movl	%eax, 280(%rbx)
	call	deflateResetKeep
	movl	%eax, %r12d
	testl	%eax, %eax
	je	.L997
.L967:
	addq	$24, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L987:
	.cfi_restore_state
	movl	$5, %eax
	movl	$15, %ecx
	movd	%eax, %xmm0
	movl	$32767, %eax
	movd	%eax, %xmm1
	movl	$32768, %eax
	jmp	.L978
	.p2align 4,,10
	.p2align 3
.L996:
	subl	$16, %ecx
	movl	$1, %eax
	movl	$2, %r8d
	jmp	.L974
	.p2align 4,,10
	.p2align 3
.L995:
	negl	%ecx
	movl	$1, %eax
	xorl	%r8d, %r8d
	jmp	.L974
	.p2align 4,,10
	.p2align 3
.L997:
	movq	56(%r14), %rbx
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	movl	160(%rbx), %eax
	movq	200(%rbx), %rdi
	addq	%rax, %rax
	movq	%rax, 184(%rbx)
	movl	212(%rbx), %eax
	subl	$1, %eax
	movw	%cx, (%rdi,%rax,2)
	leaq	(%rax,%rax), %rdx
	call	memset@PLT
	movslq	276(%rbx), %rax
	movq	$0, 232(%rbx)
	movl	$0, 6012(%rbx)
	movl	$2, 240(%rbx)
	salq	$4, %rax
	movq	$0, 248(%rbx)
	movq	%rax, %rdx
	leaq	configuration_table(%rip), %rax
	movl	$0, 208(%rbx)
	addq	%rdx, %rax
	movzwl	2(%rax), %edx
	movl	%edx, 272(%rbx)
	movzwl	(%rax), %edx
	movl	%edx, 284(%rbx)
	movzwl	4(%rax), %edx
	movzwl	6(%rax), %eax
	movl	%edx, 288(%rbx)
	movl	%eax, 268(%rbx)
	movabsq	$8589934592, %rax
	movq	%rax, 260(%rbx)
	jmp	.L967
	.p2align 4,,10
	.p2align 3
.L993:
	leaq	zcalloc(%rip), %r9
	cmpq	$0, 72(%r14)
	movq	$0, 80(%r14)
	movq	%r9, 64(%r14)
	jne	.L971
.L994:
	leaq	zcfree(%rip), %rdi
	movq	%rdi, 72(%r14)
	jmp	.L971
	.p2align 4,,10
	.p2align 3
.L975:
	movl	$-2, %r12d
	jmp	.L967
	.p2align 4,,10
	.p2align 3
.L983:
	movl	$-6, %r12d
	jmp	.L967
	.p2align 4,,10
	.p2align 3
.L979:
	movq	48+z_errmsg(%rip), %rax
	movl	$666, 8(%rbx)
	movq	%r14, %rdi
	movl	$-4, %r12d
	movq	%rax, 48(%r14)
	call	deflateEnd
	jmp	.L967
.L986:
	movl	$-4, %r12d
	jmp	.L967
	.cfi_endproc
.LFE676:
	.size	deflateInit2_, .-deflateInit2_
	.p2align 4
	.globl	deflateInit_
	.type	deflateInit_, @function
deflateInit_:
.LFB675:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%ecx, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdx, %rbx
	call	x86_check_features@PLT
	testq	%rbx, %rbx
	je	.L1009
	cmpb	$49, (%rbx)
	jne	.L1009
	cmpl	$112, %r14d
	jne	.L1009
	testq	%r12, %r12
	je	.L1004
	movq	$0, 48(%r12)
	movq	64(%r12), %rax
	testq	%rax, %rax
	je	.L1013
	cmpq	$0, 72(%r12)
	je	.L1014
.L1002:
	cmpl	$-1, %r13d
	je	.L1010
	cmpl	$9, %r13d
	ja	.L1004
.L1003:
	movq	80(%r12), %rdi
	movl	$6032, %edx
	movl	$1, %esi
	call	*%rax
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L1011
	movq	%rax, 56(%r12)
	movq	80(%r12), %rdi
	movl	$2, %edx
	movl	$32776, %esi
	movdqa	.LC0(%rip), %xmm0
	movq	%r12, (%rax)
	movl	$42, 8(%rax)
	movl	$1, 48(%rax)
	movq	$0, 56(%rax)
	movabsq	$64424542208, %rax
	movups	%xmm0, 212(%rbx)
	movq	%rax, 160(%rbx)
	movl	$32767, 168(%rbx)
	call	*64(%r12)
	movl	160(%rbx), %esi
	movq	80(%r12), %rdi
	movl	$2, %edx
	movq	%rax, 176(%rbx)
	call	*64(%r12)
	movl	212(%rbx), %esi
	movq	80(%r12), %rdi
	movl	$2, %edx
	movq	%rax, 192(%rbx)
	call	*64(%r12)
	movl	$16384, %esi
	movq	80(%r12), %rdi
	movq	$0, 6024(%rbx)
	movq	%rax, 200(%rbx)
	movl	$4, %edx
	movl	$16384, 5976(%rbx)
	call	*64(%r12)
	movl	5976(%rbx), %ecx
	cmpq	$0, 176(%rbx)
	movq	%rax, 16(%rbx)
	leaq	0(,%rcx,4), %rsi
	movq	%rsi, 24(%rbx)
	je	.L1005
	cmpq	$0, 192(%rbx)
	je	.L1005
	cmpq	$0, 200(%rbx)
	je	.L1005
	testq	%rax, %rax
	je	.L1005
	addq	%rcx, %rax
	movl	%r13d, 276(%rbx)
	movq	%r12, %rdi
	movq	%rax, 5968(%rbx)
	leal	-3(%rcx,%rcx,2), %eax
	movl	%eax, 5984(%rbx)
	movl	$0, 280(%rbx)
	movb	$8, 72(%rbx)
	call	deflateResetKeep
	movl	%eax, %r13d
	testl	%eax, %eax
	je	.L1015
.L998:
	popq	%rbx
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1010:
	.cfi_restore_state
	movl	$6, %r13d
	jmp	.L1003
	.p2align 4,,10
	.p2align 3
.L1015:
	movq	56(%r12), %rbx
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	movl	160(%rbx), %eax
	movq	200(%rbx), %rdi
	addq	%rax, %rax
	movq	%rax, 184(%rbx)
	movl	212(%rbx), %eax
	subl	$1, %eax
	movw	%cx, (%rdi,%rax,2)
	leaq	(%rax,%rax), %rdx
	call	memset@PLT
	movslq	276(%rbx), %rax
	movq	$0, 232(%rbx)
	movl	$0, 6012(%rbx)
	movl	$2, 240(%rbx)
	salq	$4, %rax
	movq	$0, 248(%rbx)
	movq	%rax, %rdx
	leaq	configuration_table(%rip), %rax
	movl	$0, 208(%rbx)
	addq	%rdx, %rax
	movzwl	2(%rax), %edx
	movl	%edx, 272(%rbx)
	movzwl	(%rax), %edx
	movl	%edx, 284(%rbx)
	movzwl	4(%rax), %edx
	movzwl	6(%rax), %eax
	movl	%edx, 288(%rbx)
	movl	%eax, 268(%rbx)
	movabsq	$8589934592, %rax
	movq	%rax, 260(%rbx)
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1013:
	.cfi_restore_state
	leaq	zcalloc(%rip), %rax
	cmpq	$0, 72(%r12)
	movq	$0, 80(%r12)
	movq	%rax, 64(%r12)
	jne	.L1002
.L1014:
	leaq	zcfree(%rip), %rcx
	movq	%rcx, 72(%r12)
	jmp	.L1002
	.p2align 4,,10
	.p2align 3
.L1009:
	movl	$-6, %r13d
	jmp	.L998
	.p2align 4,,10
	.p2align 3
.L1004:
	movl	$-2, %r13d
	jmp	.L998
	.p2align 4,,10
	.p2align 3
.L1005:
	movq	48+z_errmsg(%rip), %rax
	movl	$666, 8(%rbx)
	movq	%r12, %rdi
	movl	$-4, %r13d
	movq	%rax, 48(%r12)
	call	deflateEnd
	jmp	.L998
.L1011:
	movl	$-4, %r13d
	jmp	.L998
	.cfi_endproc
.LFE675:
	.size	deflateInit_, .-deflateInit_
	.p2align 4
	.globl	deflateCopy
	.type	deflateCopy, @function
deflateCopy:
.LFB692:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L1025
	movq	64(%rsi), %r8
	testq	%r8, %r8
	je	.L1025
	cmpq	$0, 72(%rsi)
	je	.L1025
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$-2, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	56(%rsi), %r13
	testq	%r13, %r13
	je	.L1016
	cmpq	0(%r13), %rsi
	jne	.L1016
	movl	8(%r13), %edx
	movq	%rdi, %r12
	movl	$1, %eax
	leal	-42(%rdx), %ecx
	cmpl	$61, %ecx
	ja	.L1018
	movabsq	$2306405961448849409, %rax
	shrq	%cl, %rax
	notq	%rax
	andl	$1, %eax
.L1018:
	cmpl	$113, %edx
	setne	%cl
	cmpl	$666, %edx
	setne	%dl
	testb	%dl, %cl
	je	.L1032
	testb	%al, %al
	jne	.L1030
.L1032:
	testq	%r12, %r12
	je	.L1030
	movdqu	(%rsi), %xmm0
	movl	$6032, %edx
	movups	%xmm0, (%r12)
	movdqu	16(%rsi), %xmm1
	movups	%xmm1, 16(%r12)
	movdqu	32(%rsi), %xmm2
	movups	%xmm2, 32(%r12)
	movdqu	48(%rsi), %xmm3
	movups	%xmm3, 48(%r12)
	movdqu	64(%rsi), %xmm4
	movups	%xmm4, 64(%r12)
	movdqu	80(%rsi), %xmm5
	movups	%xmm5, 80(%r12)
	movdqu	96(%rsi), %xmm6
	movq	80(%r12), %rdi
	movl	$1, %esi
	movups	%xmm6, 96(%r12)
	call	*%r8
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L1031
	movq	%rax, 56(%r12)
	leaq	8(%rbx), %rdi
	movq	0(%r13), %rax
	movq	%rbx, %rcx
	andq	$-8, %rdi
	movq	%r13, %rsi
	movl	$2, %edx
	movq	%rax, (%rbx)
	subq	%rdi, %rcx
	movq	6024(%r13), %rax
	subq	%rcx, %rsi
	addl	$6032, %ecx
	movq	%rax, 6024(%rbx)
	shrl	$3, %ecx
	rep movsq
	movq	%r12, (%rbx)
	movl	160(%rbx), %esi
	movq	80(%r12), %rdi
	call	*64(%r12)
	movl	160(%rbx), %esi
	movq	80(%r12), %rdi
	movl	$2, %edx
	movq	%rax, 176(%rbx)
	call	*64(%r12)
	movl	212(%rbx), %esi
	movq	80(%r12), %rdi
	movl	$2, %edx
	movq	%rax, 192(%rbx)
	call	*64(%r12)
	movq	80(%r12), %rdi
	movl	5976(%rbx), %esi
	movl	$4, %edx
	movq	%rax, 200(%rbx)
	call	*64(%r12)
	movq	176(%rbx), %rdi
	movq	%rax, 16(%rbx)
	testq	%rdi, %rdi
	je	.L1020
	cmpq	$0, 192(%rbx)
	je	.L1020
	cmpq	$0, 200(%rbx)
	je	.L1020
	testq	%rax, %rax
	je	.L1020
	movl	160(%rbx), %eax
	movq	176(%r13), %rsi
	leal	(%rax,%rax), %edx
	call	memcpy@PLT
	movl	160(%rbx), %edx
	movq	192(%rbx), %rdi
	movq	192(%r13), %rsi
	addq	%rdx, %rdx
	call	memcpy@PLT
	movl	212(%rbx), %edx
	movq	200(%rbx), %rdi
	movq	200(%r13), %rsi
	addq	%rdx, %rdx
	call	memcpy@PLT
	movl	24(%rbx), %edx
	movq	16(%rbx), %rdi
	movq	16(%r13), %rsi
	call	memcpy@PLT
	movq	32(%r13), %rdx
	movq	16(%rbx), %rax
	addq	%rax, %rdx
	subq	16(%r13), %rdx
	movq	%rdx, 32(%rbx)
	movl	5976(%rbx), %edx
	addq	%rdx, %rax
	movq	%rax, 5968(%rbx)
	leaq	292(%rbx), %rax
	movq	%rax, 2984(%rbx)
	leaq	2584(%rbx), %rax
	movq	%rax, 3008(%rbx)
	leaq	2828(%rbx), %rax
	movq	%rax, 3032(%rbx)
	xorl	%eax, %eax
.L1016:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1025:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	movl	$-2, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1030:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movl	$-2, %eax
	jmp	.L1016
	.p2align 4,,10
	.p2align 3
.L1020:
	movq	%r12, %rdi
	call	deflateEnd
	movl	$-4, %eax
	jmp	.L1016
.L1031:
	movl	$-4, %eax
	jmp	.L1016
	.cfi_endproc
.LFE692:
	.size	deflateCopy, .-deflateCopy
	.p2align 4
	.globl	deflate_read_buf
	.hidden	deflate_read_buf
	.type	deflate_read_buf, @function
deflate_read_buf:
.LFB693:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movl	8(%rdi), %eax
	cmpl	%edx, %eax
	cmovbe	%eax, %edx
	movl	%edx, %r12d
	testl	%edx, %edx
	je	.L1043
	subl	%edx, %eax
	movq	%rdi, %rbx
	movq	%rsi, %r8
	movl	%edx, %r13d
	movl	%eax, 8(%rdi)
	movq	56(%rdi), %rax
	cmpl	$2, 48(%rax)
	je	.L1051
	movq	(%rdi), %rsi
	movq	%r13, %rdx
	movq	%r8, %rdi
	call	memcpy@PLT
	movq	%rax, %r8
	movq	56(%rbx), %rax
	cmpl	$1, 48(%rax)
	je	.L1052
.L1046:
	addq	%r13, (%rbx)
	addq	%r13, 16(%rbx)
.L1043:
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1051:
	.cfi_restore_state
	movq	%r13, %rdx
	call	copy_with_crc
	jmp	.L1046
	.p2align 4,,10
	.p2align 3
.L1052:
	movq	96(%rbx), %rdi
	movl	%r12d, %edx
	movq	%r8, %rsi
	call	adler32@PLT
	movq	%rax, 96(%rbx)
	jmp	.L1046
	.cfi_endproc
.LFE693:
	.size	deflate_read_buf, .-deflate_read_buf
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	configuration_table, @object
	.size	configuration_table, 160
configuration_table:
	.value	0
	.value	0
	.value	0
	.value	0
	.quad	deflate_stored
	.value	4
	.value	4
	.value	8
	.value	4
	.quad	deflate_fast
	.value	4
	.value	5
	.value	16
	.value	8
	.quad	deflate_fast
	.value	4
	.value	6
	.value	32
	.value	32
	.quad	deflate_fast
	.value	4
	.value	4
	.value	16
	.value	16
	.quad	deflate_slow
	.value	8
	.value	16
	.value	32
	.value	32
	.quad	deflate_slow
	.value	8
	.value	16
	.value	128
	.value	128
	.quad	deflate_slow
	.value	8
	.value	32
	.value	128
	.value	256
	.quad	deflate_slow
	.value	32
	.value	128
	.value	258
	.value	1024
	.quad	deflate_slow
	.value	32
	.value	258
	.value	258
	.value	4096
	.quad	deflate_slow
	.globl	deflate_copyright
	.section	.rodata
	.align 32
	.type	deflate_copyright, @object
	.size	deflate_copyright, 69
deflate_copyright:
	.string	" deflate 1.2.11 Copyright 1995-2017 Jean-loup Gailly and Mark Adler "
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	32768
	.long	15
	.long	32767
	.long	5
	.hidden	zcfree
	.hidden	zcalloc
	.hidden	_tr_init
	.hidden	_tr_align
	.hidden	crc_finalize
	.hidden	crc_reset
	.hidden	_tr_flush_block
	.hidden	_length_code
	.hidden	_dist_code
	.hidden	_tr_flush_bits
	.hidden	_tr_stored_block
	.hidden	fill_window_sse
	.hidden	copy_with_crc
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
