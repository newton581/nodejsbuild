	.file	"trees.c"
	.text
	.p2align 4
	.type	scan_tree, @function
scan_tree:
.LFB45:
	.cfi_startproc
	movzwl	2(%rsi), %ecx
	movslq	%edx, %rax
	movl	$-1, %r10d
	movw	%r10w, 6(%rsi,%rax,4)
	cmpl	$1, %ecx
	sbbl	%r9d, %r9d
	addl	$4, %r9d
	cmpl	$1, %ecx
	sbbl	%r8d, %r8d
	andl	$131, %r8d
	addl	$7, %r8d
	testl	%edx, %edx
	js	.L1
	leaq	10(%rsi,%rax,4), %r11
	leaq	6(%rsi), %rdx
	xorl	%eax, %eax
	movl	$-1, %r10d
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L4:
	testl	%esi, %esi
	je	.L8
	cmpl	%r10d, %esi
	je	.L9
	movslq	%esi, %rax
	addw	$1, 2828(%rdi,%rax,4)
.L9:
	addw	$1, 2892(%rdi)
.L10:
	testl	%ecx, %ecx
	je	.L6
	cmpl	%esi, %ecx
	jne	.L7
	movl	%ecx, %r10d
	movl	$3, %r9d
	movl	$6, %r8d
	xorl	%eax, %eax
.L5:
	addq	$4, %rdx
	cmpq	%rdx, %r11
	je	.L1
.L13:
	addl	$1, %eax
	movl	%ecx, %esi
	movzwl	(%rdx), %ecx
	cmpl	%r8d, %eax
	jge	.L4
	cmpl	%esi, %ecx
	je	.L5
	cmpl	%r9d, %eax
	jge	.L4
	movslq	%esi, %r8
	addw	%ax, 2828(%rdi,%r8,4)
	testl	%ecx, %ecx
	je	.L6
.L7:
	addq	$4, %rdx
	movl	%esi, %r10d
	movl	$4, %r9d
	xorl	%eax, %eax
	movl	$7, %r8d
	cmpq	%rdx, %r11
	jne	.L13
.L1:
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	movl	%esi, %r10d
	movl	$3, %r9d
	movl	$138, %r8d
	xorl	%eax, %eax
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L8:
	cmpl	$10, %eax
	jg	.L11
	addw	$1, 2896(%rdi)
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L11:
	addw	$1, 2900(%rdi)
	jmp	.L10
	.cfi_endproc
.LFE45:
	.size	scan_tree, .-scan_tree
	.p2align 4
	.type	send_tree, @function
send_tree:
.LFB46:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movzwl	2(%rsi), %r8d
	cmpl	$1, %r8d
	sbbl	%r14d, %r14d
	addl	$4, %r14d
	cmpl	$1, %r8d
	sbbl	%ecx, %ecx
	andl	$131, %ecx
	addl	$7, %ecx
	testl	%edx, %edx
	js	.L21
	movslq	%edx, %rdx
	leaq	6(%rsi), %r9
	xorl	%r13d, %r13d
	movl	$-1, %r11d
	leaq	10(%rsi,%rdx,4), %rbx
	movl	$16, %r10d
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L67:
	cmpl	%esi, %r8d
	je	.L54
	movl	6020(%rdi), %r12d
	movzwl	6016(%rdi), %edx
	cmpl	%r14d, %eax
	jge	.L26
	movslq	%esi, %rax
	movl	%r12d, %ecx
	leaq	(%rdi,%rax,4), %r11
	movzwl	2828(%r11), %r15d
	movzwl	2830(%r11), %r14d
	movl	%r15d, %eax
	sall	%cl, %eax
	orl	%eax, %edx
	movl	%r10d, %eax
	subl	%r14d, %eax
	movw	%dx, 6016(%rdi)
	cmpl	%r12d, %eax
	jl	.L27
	leal	(%r14,%r12), %ecx
	movl	%ecx, 6020(%rdi)
.L28:
	testl	%r13d, %r13d
	je	.L29
	movzwl	2828(%r11), %r12d
	movzwl	2830(%r11), %r14d
	movl	%r12d, %eax
	sall	%cl, %eax
	orl	%edx, %eax
	movl	%r10d, %edx
	subl	%r14d, %edx
	movw	%ax, 6016(%rdi)
	cmpl	%ecx, %edx
	jl	.L30
	addl	%r14d, %ecx
	movl	%ecx, 6020(%rdi)
.L31:
	cmpl	$1, %r13d
	je	.L29
	movzwl	2830(%r11), %edx
	movzwl	2828(%r11), %r11d
	movl	%r11d, %r12d
	sall	%cl, %r12d
	orl	%r12d, %eax
	movl	%r10d, %r12d
	subl	%edx, %r12d
	movw	%ax, 6016(%rdi)
	cmpl	%ecx, %r12d
	jge	.L32
	movq	40(%rdi), %rcx
	movq	16(%rdi), %r12
	leaq	1(%rcx), %r13
	movq	%r13, 40(%rdi)
	movb	%al, (%r12,%rcx)
	movq	40(%rdi), %rax
	movzbl	6017(%rdi), %r12d
	movq	16(%rdi), %rcx
	leaq	1(%rax), %r13
	movq	%r13, 40(%rdi)
	movb	%r12b, (%rcx,%rax)
	movl	6020(%rdi), %eax
	movl	%r10d, %ecx
	subl	%eax, %ecx
	leal	-16(%rax,%rdx), %eax
	sarl	%cl, %r11d
	movl	%eax, 6020(%rdi)
	movw	%r11w, 6016(%rdi)
.L29:
	testl	%r8d, %r8d
	je	.L34
.L35:
	movl	%esi, %r11d
	movl	$4, %r14d
	movl	$7, %ecx
	xorl	%r13d, %r13d
.L25:
	addq	$4, %r9
	cmpq	%rbx, %r9
	je	.L21
.L51:
	leal	1(%r13), %eax
	movl	%r8d, %esi
	movzwl	(%r9), %r8d
	cmpl	%ecx, %eax
	jl	.L67
	movl	6020(%rdi), %r12d
	movzwl	6016(%rdi), %edx
.L26:
	testl	%esi, %esi
	je	.L36
	cmpl	%r11d, %esi
	je	.L37
	movslq	%esi, %rax
	leaq	(%rdi,%rax,4), %rcx
	movzwl	2828(%rcx), %r11d
	movzwl	2830(%rcx), %eax
	movl	%r12d, %ecx
	movl	%r11d, %r15d
	sall	%cl, %r15d
	movl	%r10d, %ecx
	subl	%eax, %ecx
	orl	%r15d, %edx
	cmpl	%r12d, %ecx
	jge	.L38
	movq	40(%rdi), %rcx
	movq	16(%rdi), %r12
	movw	%dx, 6016(%rdi)
	leaq	1(%rcx), %r14
	movq	%r14, 40(%rdi)
	movb	%dl, (%r12,%rcx)
	movq	40(%rdi), %rdx
	movzbl	6017(%rdi), %r12d
	movq	16(%rdi), %rcx
	leaq	1(%rdx), %r14
	movq	%r14, 40(%rdi)
	movb	%r12b, (%rcx,%rdx)
	movl	6020(%rdi), %edx
	movl	%r10d, %ecx
	leal	-16(%rdx,%rax), %r12d
	subl	%edx, %ecx
	movl	%r13d, %eax
	movl	%r12d, 6020(%rdi)
	sarl	%cl, %r11d
	movl	%r11d, %edx
.L37:
	movzwl	2892(%rdi), %r14d
	movl	%r12d, %ecx
	movzwl	2894(%rdi), %r11d
	movl	%r14d, %r13d
	sall	%cl, %r13d
	orl	%edx, %r13d
	movl	%r10d, %edx
	subl	%r11d, %edx
	cmpl	%r12d, %edx
	jge	.L39
	movq	40(%rdi), %rdx
	movq	16(%rdi), %rcx
	movw	%r13w, 6016(%rdi)
	movl	%r10d, %r15d
	leaq	1(%rdx), %r12
	movq	%r12, 40(%rdi)
	movb	%r13b, (%rcx,%rdx)
	movq	40(%rdi), %rdx
	movq	16(%rdi), %rcx
	movzbl	6017(%rdi), %r12d
	leaq	1(%rdx), %r13
	movq	%r13, 40(%rdi)
	movb	%r12b, (%rcx,%rdx)
	movl	6020(%rdi), %ecx
	leal	-16(%rcx,%r11), %edx
	subl	%ecx, %r15d
	movl	%r15d, %ecx
	movl	%edx, 6020(%rdi)
	sarl	%cl, %r14d
	movl	%r14d, %r13d
.L40:
	cmpl	$14, %edx
	jle	.L41
	subl	$3, %eax
	movl	%edx, %ecx
	movzwl	%ax, %r11d
	movl	%r11d, %eax
	sall	%cl, %eax
	movq	16(%rdi), %rcx
	movl	%eax, %edx
	movq	40(%rdi), %rax
	orl	%r13d, %edx
	leaq	1(%rax), %r12
	movw	%dx, 6016(%rdi)
	movq	%r12, 40(%rdi)
	movb	%dl, (%rcx,%rax)
	movq	40(%rdi), %rax
	movzbl	6017(%rdi), %ecx
	movq	16(%rdi), %rdx
	leaq	1(%rax), %r12
	movq	%r12, 40(%rdi)
	movb	%cl, (%rdx,%rax)
	movl	6020(%rdi), %edx
	movl	%r10d, %ecx
	subl	%edx, %ecx
	subl	$14, %edx
	sarl	%cl, %r11d
	movl	%edx, 6020(%rdi)
	movw	%r11w, 6016(%rdi)
.L42:
	testl	%r8d, %r8d
	je	.L34
	cmpl	%esi, %r8d
	jne	.L35
	addq	$4, %r9
	movl	%r8d, %r11d
	movl	$3, %r14d
	xorl	%r13d, %r13d
	movl	$6, %ecx
	cmpq	%rbx, %r9
	jne	.L51
.L21:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L34:
	.cfi_restore_state
	movl	%esi, %r11d
	movl	$3, %r14d
	movl	$138, %ecx
	xorl	%r13d, %r13d
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L36:
	cmpl	$10, %eax
	jg	.L43
	movzwl	2898(%rdi), %r11d
	movl	%r10d, %ecx
	movzwl	2896(%rdi), %r14d
	subl	%r11d, %ecx
	cmpl	%r12d, %ecx
	jge	.L44
	movl	%r12d, %ecx
	movl	%r14d, %r15d
	movq	16(%rdi), %r12
	sall	%cl, %r15d
	movq	40(%rdi), %rcx
	orl	%r15d, %edx
	leaq	1(%rcx), %r15
	movw	%dx, 6016(%rdi)
	movq	%r15, 40(%rdi)
	movb	%dl, (%r12,%rcx)
	movq	40(%rdi), %rdx
	movzbl	6017(%rdi), %r12d
	movq	16(%rdi), %rcx
	leaq	1(%rdx), %r15
	movq	%r15, 40(%rdi)
	movb	%r12b, (%rcx,%rdx)
	movl	6020(%rdi), %edx
	movl	%r10d, %ecx
	leal	-16(%rdx,%r11), %r12d
	subl	%edx, %ecx
	movl	%r12d, 6020(%rdi)
	sarl	%cl, %r14d
	movl	%r14d, %edx
.L45:
	cmpl	$13, %r12d
	jle	.L46
	subl	$2, %r13d
	movl	%r12d, %ecx
	movzwl	%r13w, %r13d
	movl	%r13d, %eax
	sall	%cl, %eax
	movq	16(%rdi), %rcx
	orl	%eax, %edx
	movq	40(%rdi), %rax
	movw	%dx, 6016(%rdi)
	leaq	1(%rax), %r11
	movq	%r11, 40(%rdi)
	movb	%dl, (%rcx,%rax)
	movq	40(%rdi), %rax
	movzbl	6017(%rdi), %ecx
	movq	16(%rdi), %rdx
	leaq	1(%rax), %r11
	movq	%r11, 40(%rdi)
	movb	%cl, (%rdx,%rax)
	movl	6020(%rdi), %eax
	movl	%r10d, %ecx
	subl	%eax, %ecx
	subl	$13, %eax
	sarl	%cl, %r13d
	movl	%eax, 6020(%rdi)
	movw	%r13w, 6016(%rdi)
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L41:
	leal	-3(%rax), %r11d
	movl	%edx, %ecx
	movl	%r13d, %eax
	addl	$2, %edx
	movzwl	%r11w, %r11d
	movl	%edx, 6020(%rdi)
	sall	%cl, %r11d
	orl	%r11d, %eax
	movw	%ax, 6016(%rdi)
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L39:
	leal	(%r11,%r12), %edx
	movl	%edx, 6020(%rdi)
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L43:
	movzwl	2900(%rdi), %r11d
	movl	%r12d, %ecx
	movzwl	2902(%rdi), %r14d
	movl	%r11d, %r15d
	sall	%cl, %r15d
	movl	%r10d, %ecx
	subl	%r14d, %ecx
	orl	%r15d, %edx
	cmpl	%r12d, %ecx
	jge	.L47
	movq	40(%rdi), %rcx
	movq	16(%rdi), %r12
	movw	%dx, 6016(%rdi)
	leaq	1(%rcx), %r15
	movq	%r15, 40(%rdi)
	movb	%dl, (%r12,%rcx)
	movq	40(%rdi), %rdx
	movzbl	6017(%rdi), %r12d
	movq	16(%rdi), %rcx
	leaq	1(%rdx), %r15
	movq	%r15, 40(%rdi)
	movb	%r12b, (%rcx,%rdx)
	movl	6020(%rdi), %edx
	movl	%r10d, %ecx
	leal	-16(%rdx,%r14), %r12d
	subl	%edx, %ecx
	movl	%r12d, 6020(%rdi)
	sarl	%cl, %r11d
	movl	%r11d, %edx
.L48:
	cmpl	$9, %r12d
	jle	.L49
	subl	$10, %r13d
	movl	%r12d, %ecx
	movzwl	%r13w, %r13d
	movl	%r13d, %eax
	sall	%cl, %eax
	movq	16(%rdi), %rcx
	orl	%eax, %edx
	movq	40(%rdi), %rax
	movw	%dx, 6016(%rdi)
	leaq	1(%rax), %r11
	movq	%r11, 40(%rdi)
	movb	%dl, (%rcx,%rax)
	movq	40(%rdi), %rax
	movzbl	6017(%rdi), %ecx
	movq	16(%rdi), %rdx
	leaq	1(%rax), %r11
	movq	%r11, 40(%rdi)
	movb	%cl, (%rdx,%rax)
	movl	6020(%rdi), %eax
	movl	%r10d, %ecx
	subl	%eax, %ecx
	subl	$9, %eax
	sarl	%cl, %r13d
	movl	%eax, 6020(%rdi)
	movw	%r13w, 6016(%rdi)
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L27:
	movq	40(%rdi), %rax
	movq	16(%rdi), %rcx
	leaq	1(%rax), %r12
	movq	%r12, 40(%rdi)
	movb	%dl, (%rcx,%rax)
	movq	40(%rdi), %rax
	movzbl	6017(%rdi), %ecx
	movq	16(%rdi), %rdx
	leaq	1(%rax), %r12
	movq	%r12, 40(%rdi)
	movb	%cl, (%rdx,%rax)
	movl	6020(%rdi), %eax
	movl	%r10d, %ecx
	subl	%eax, %ecx
	sarl	%cl, %r15d
	leal	-16(%rax,%r14), %ecx
	movw	%r15w, 6016(%rdi)
	movl	%r15d, %edx
	movl	%ecx, 6020(%rdi)
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L38:
	addl	%eax, %r12d
	movl	%r13d, %eax
	movl	%r12d, 6020(%rdi)
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L30:
	movq	40(%rdi), %rdx
	movq	16(%rdi), %rcx
	leaq	1(%rdx), %r15
	movq	%r15, 40(%rdi)
	movb	%al, (%rcx,%rdx)
	movq	40(%rdi), %rax
	movzbl	6017(%rdi), %ecx
	movq	16(%rdi), %rdx
	leaq	1(%rax), %r15
	movq	%r15, 40(%rdi)
	movb	%cl, (%rdx,%rax)
	movl	6020(%rdi), %edx
	movl	%r10d, %ecx
	subl	%edx, %ecx
	sarl	%cl, %r12d
	leal	-16(%rdx,%r14), %ecx
	movw	%r12w, 6016(%rdi)
	movl	%r12d, %eax
	movl	%ecx, 6020(%rdi)
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L54:
	movl	%eax, %r13d
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L32:
	addl	%edx, %ecx
	movl	%ecx, 6020(%rdi)
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L49:
	subl	$11, %eax
	movl	%r12d, %ecx
	addl	$7, %r12d
	movzwl	%ax, %eax
	movl	%r12d, 6020(%rdi)
	sall	%cl, %eax
	orl	%eax, %edx
	movw	%dx, 6016(%rdi)
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L47:
	addl	%r14d, %r12d
	movl	%r12d, 6020(%rdi)
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L46:
	subl	$3, %eax
	movl	%r12d, %ecx
	addl	$3, %r12d
	movzwl	%ax, %eax
	movl	%r12d, 6020(%rdi)
	sall	%cl, %eax
	orl	%eax, %edx
	movw	%dx, 6016(%rdi)
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L44:
	movl	%r12d, %ecx
	addl	%r11d, %r12d
	sall	%cl, %r14d
	movl	%r12d, 6020(%rdi)
	orl	%r14d, %edx
	jmp	.L45
	.cfi_endproc
.LFE46:
	.size	send_tree, .-send_tree
	.p2align 4
	.type	compress_block, @function
compress_block:
.LFB54:
	.cfi_startproc
	movl	5980(%rdi), %eax
	movl	6020(%rdi), %ecx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movzwl	6016(%rdi), %r14d
	pushq	%r13
	movl	%r14d, %r8d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testl	%eax, %eax
	je	.L83
	xorl	%r9d, %r9d
	movl	$16, %r11d
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L95:
	leaq	(%rsi,%r10,4), %rax
	movl	%r11d, %ebx
	movzwl	2(%rax), %r10d
	movzwl	(%rax), %eax
	movl	%eax, %r8d
	subl	%r10d, %ebx
	sall	%cl, %r8d
	orl	%r14d, %r8d
	movw	%r8w, 6016(%rdi)
	movl	%r8d, %r14d
	cmpl	%ecx, %ebx
	jge	.L81
	movq	40(%rdi), %rcx
	movq	16(%rdi), %r8
	leaq	1(%rcx), %rbx
	movq	%rbx, 40(%rdi)
	movb	%r14b, (%r8,%rcx)
	movq	40(%rdi), %rcx
	movzbl	6017(%rdi), %ebx
	movq	16(%rdi), %r8
	leaq	1(%rcx), %r12
	movq	%r12, 40(%rdi)
	movb	%bl, (%r8,%rcx)
	movl	6020(%rdi), %ebx
	movl	%r11d, %ecx
	subl	%ebx, %ecx
	sarl	%cl, %eax
	leal	-16(%rbx,%r10), %ecx
	movw	%ax, 6016(%rdi)
	movl	%eax, %r8d
	movl	%eax, %r14d
	movl	%ecx, 6020(%rdi)
.L72:
	cmpl	%r9d, 5980(%rdi)
	jbe	.L83
.L82:
	movq	5968(%rdi), %r10
	movl	%r9d, %eax
	leal	2(%r9), %r8d
	movzbl	(%r10,%rax), %ebx
	leal	1(%r9), %eax
	addl	$3, %r9d
	movzbl	(%r10,%rax), %eax
	movzbl	(%r10,%r8), %r10d
	sall	$8, %eax
	addl	%ebx, %eax
	je	.L95
	leaq	_length_code(%rip), %r15
	movzbl	%r10b, %ebx
	movzbl	(%r15,%r10), %r12d
	leal	257(%r12), %r8d
	leaq	(%rsi,%r8,4), %r8
	movzwl	(%r8), %r13d
	movzwl	2(%r8), %r10d
	movl	%r13d, %r8d
	sall	%cl, %r8d
	orl	%r8d, %r14d
	movl	%r11d, %r8d
	subl	%r10d, %r8d
	cmpl	%ecx, %r8d
	jge	.L73
	movq	40(%rdi), %rcx
	movq	16(%rdi), %r8
	movw	%r14w, 6016(%rdi)
	leaq	1(%rcx), %r15
	movq	%r15, 40(%rdi)
	movb	%r14b, (%r8,%rcx)
	movq	40(%rdi), %rcx
	movzbl	6017(%rdi), %r14d
	movq	16(%rdi), %r8
	leaq	1(%rcx), %r15
	movq	%r15, 40(%rdi)
	movl	%r11d, %r15d
	movb	%r14b, (%r8,%rcx)
	movl	6020(%rdi), %ecx
	leal	-16(%rcx,%r10), %r10d
	subl	%ecx, %r15d
	movl	%r15d, %ecx
	movl	%r10d, 6020(%rdi)
	sarl	%cl, %r13d
	movl	%r13d, %r14d
.L74:
	movl	%r12d, %ecx
	leaq	extra_lbits(%rip), %r15
	movl	(%r15,%rcx,4), %r8d
	testl	%r8d, %r8d
	je	.L75
	leaq	base_length(%rip), %r15
	subl	(%r15,%rcx,4), %ebx
	movl	%r10d, %ecx
	movzwl	%bx, %ebx
	movl	%ebx, %r15d
	sall	%cl, %r15d
	movl	%r11d, %ecx
	subl	%r8d, %ecx
	orl	%r15d, %r14d
	cmpl	%r10d, %ecx
	jge	.L76
	movq	40(%rdi), %rcx
	movq	16(%rdi), %r10
	movw	%r14w, 6016(%rdi)
	movl	%r11d, %r15d
	leaq	1(%rcx), %r12
	movq	%r12, 40(%rdi)
	movb	%r14b, (%r10,%rcx)
	movq	40(%rdi), %rcx
	movq	16(%rdi), %r10
	movzbl	6017(%rdi), %r12d
	leaq	1(%rcx), %r13
	movq	%r13, 40(%rdi)
	movb	%r12b, (%r10,%rcx)
	movl	6020(%rdi), %ecx
	leal	-16(%rcx,%r8), %r10d
	subl	%ecx, %r15d
	movl	%r15d, %ecx
	movl	%r10d, 6020(%rdi)
	sarl	%cl, %ebx
	movl	%ebx, %r14d
.L75:
	subl	$1, %eax
	cmpl	$255, %eax
	ja	.L77
	movl	%eax, %ecx
	leaq	_dist_code(%rip), %rbx
	movzbl	(%rbx,%rcx), %ebx
.L78:
	leaq	(%rdx,%rbx,4), %rcx
	movzwl	(%rcx), %r13d
	movzwl	2(%rcx), %r12d
	movl	%r10d, %ecx
	movl	%r13d, %r8d
	sall	%cl, %r8d
	movl	%r11d, %ecx
	orl	%r14d, %r8d
	subl	%r12d, %ecx
	movw	%r8w, 6016(%rdi)
	movl	%r8d, %r14d
	cmpl	%r10d, %ecx
	jge	.L79
	movq	40(%rdi), %rcx
	movq	16(%rdi), %r10
	leaq	1(%rcx), %r14
	movq	%r14, 40(%rdi)
	movb	%r8b, (%r10,%rcx)
	movq	40(%rdi), %rcx
	movzbl	6017(%rdi), %r10d
	movq	16(%rdi), %r8
	leaq	1(%rcx), %r14
	movq	%r14, 40(%rdi)
	movb	%r10b, (%r8,%rcx)
	movl	6020(%rdi), %r10d
	movl	%r11d, %ecx
	movl	%r13d, %r8d
	subl	%r10d, %ecx
	sarl	%cl, %r8d
	leal	-16(%r10,%r12), %ecx
	movw	%r8w, 6016(%rdi)
	movl	%r8d, %r14d
	movl	%ecx, 6020(%rdi)
.L80:
	leaq	extra_dbits(%rip), %r15
	movl	(%r15,%rbx,4), %r10d
	testl	%r10d, %r10d
	je	.L72
	leaq	base_dist(%rip), %r15
	subl	(%r15,%rbx,4), %eax
	movzwl	%ax, %eax
	movl	%eax, %ebx
	sall	%cl, %ebx
	orl	%ebx, %r8d
	movl	%r11d, %ebx
	subl	%r10d, %ebx
	movw	%r8w, 6016(%rdi)
	movl	%r8d, %r14d
	cmpl	%ecx, %ebx
	jge	.L81
	movq	40(%rdi), %rcx
	movq	16(%rdi), %rbx
	leaq	1(%rcx), %r12
	movq	%r12, 40(%rdi)
	movb	%r8b, (%rbx,%rcx)
	movq	40(%rdi), %rcx
	movzbl	6017(%rdi), %ebx
	movq	16(%rdi), %r8
	leaq	1(%rcx), %r12
	movq	%r12, 40(%rdi)
	movb	%bl, (%r8,%rcx)
	movl	6020(%rdi), %r8d
	movl	%r11d, %ecx
	subl	%r8d, %ecx
	sarl	%cl, %eax
	leal	-16(%r8,%r10), %ecx
	movw	%ax, 6016(%rdi)
	movl	%eax, %r14d
	movl	%eax, %r8d
	movl	%ecx, 6020(%rdi)
	cmpl	%r9d, 5980(%rdi)
	ja	.L82
	.p2align 4,,10
	.p2align 3
.L83:
	movzwl	1024(%rsi), %eax
	movzwl	1026(%rsi), %ebx
	movl	$16, %edx
	movl	%edx, %esi
	movl	%eax, %r13d
	subl	%ebx, %esi
	sall	%cl, %r13d
	orl	%r13d, %r8d
	movw	%r8w, 6016(%rdi)
	cmpl	%ecx, %esi
	jge	.L84
	movq	40(%rdi), %rcx
	movq	16(%rdi), %rsi
	leaq	1(%rcx), %r9
	movq	%r9, 40(%rdi)
	movb	%r8b, (%rsi,%rcx)
	movq	40(%rdi), %rcx
	movzbl	6017(%rdi), %r8d
	movq	16(%rdi), %rsi
	leaq	1(%rcx), %r9
	movq	%r9, 40(%rdi)
	movb	%r8b, (%rsi,%rcx)
	movl	6020(%rdi), %esi
	subl	%esi, %edx
	movl	%edx, %ecx
	sarl	%cl, %eax
	movw	%ax, 6016(%rdi)
	leal	-16(%rsi,%rbx), %eax
	popq	%rbx
	movl	%eax, 6020(%rdi)
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L81:
	.cfi_restore_state
	addl	%r10d, %ecx
	movl	%ecx, 6020(%rdi)
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L73:
	addl	%ecx, %r10d
	movl	%r10d, 6020(%rdi)
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L79:
	leal	(%r12,%r10), %ecx
	movl	%ecx, 6020(%rdi)
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L77:
	movl	%eax, %ecx
	leaq	_dist_code(%rip), %rbx
	shrl	$7, %ecx
	addl	$256, %ecx
	movzbl	(%rbx,%rcx), %ebx
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L76:
	addl	%r8d, %r10d
	movl	%r10d, 6020(%rdi)
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L84:
	addl	%ecx, %ebx
	movl	%ebx, 6020(%rdi)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE54:
	.size	compress_block, .-compress_block
	.p2align 4
	.type	pqdownheap.constprop.0, @function
pqdownheap.constprop.0:
.LFB60:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	3092(%rdi), %eax
	movl	5380(%rdi), %r11d
	movl	%eax, -44(%rbp)
	cmpl	$1, %r11d
	jle	.L101
	movslq	%eax, %r15
	movl	$1, %ecx
	movl	$2, %edx
	movzwl	(%rsi,%r15,4), %r13d
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L99:
	leal	(%rax,%rax), %edx
	movl	%ebx, 3088(%rdi,%rcx,4)
	movslq	%eax, %rcx
	cmpl	%r11d, %edx
	jg	.L97
.L100:
	movslq	%edx, %rax
	leaq	(%rdi,%rax,4), %r9
	movl	%edx, %eax
	movslq	3088(%r9), %r10
	movzwl	(%rsi,%r10,4), %r8d
	movq	%r10, %rbx
	cmpl	%r11d, %edx
	jge	.L98
	movslq	3092(%r9), %r14
	addl	$1, %eax
	movzwl	(%rsi,%r14,4), %r12d
	cmpw	%r8w, %r12w
	jb	.L103
	je	.L110
	movl	%edx, %eax
	.p2align 4,,10
	.p2align 3
.L98:
	cmpw	%r8w, %r13w
	jb	.L97
	jne	.L99
	movslq	%ebx, %rdx
	movzbl	5388(%rdi,%rdx), %edx
	cmpb	%dl, 5388(%rdi,%r15)
	ja	.L99
.L97:
	movl	-44(%rbp), %eax
	movl	%eax, 3088(%rdi,%rcx,4)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L103:
	.cfi_restore_state
	movl	%r12d, %r8d
	movl	%r14d, %ebx
	jmp	.L98
	.p2align 4,,10
	.p2align 3
.L110:
	movzbl	5388(%rdi,%r14), %r12d
	cmpb	5388(%rdi,%r10), %r12b
	cmovbe	%r14d, %ebx
	cmova	%edx, %eax
	jmp	.L98
.L101:
	movl	$1, %ecx
	jmp	.L97
	.cfi_endproc
.LFE60:
	.size	pqdownheap.constprop.0, .-pqdownheap.constprop.0
	.p2align 4
	.type	build_tree, @function
build_tree:
.LFB44:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -136(%rbp)
	movq	16(%rsi), %rax
	movq	(%rsi), %r15
	movq	(%rax), %r8
	movl	20(%rax), %eax
	movq	%fs:40, %rbx
	movq	%rbx, -56(%rbp)
	xorl	%ebx, %ebx
	movabsq	$2461016260608, %rbx
	movl	%eax, -128(%rbp)
	movq	%rbx, 5380(%rdi)
	testl	%eax, %eax
	jle	.L163
	leal	-1(%rax), %esi
	xorl	%edx, %edx
	xorl	%eax, %eax
	movl	$-1, %r9d
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L212:
	addl	$1, %edx
	movl	%eax, %r9d
	movslq	%edx, %rcx
	movl	%edx, 5380(%rdi)
	movl	%eax, 3088(%rdi,%rcx,4)
	leaq	1(%rax), %rcx
	movb	$0, 5388(%rdi,%rax)
	cmpq	%rax, %rsi
	je	.L211
.L164:
	movq	%rcx, %rax
.L115:
	cmpw	$0, (%r15,%rax,4)
	jne	.L212
	xorl	%r13d, %r13d
	leaq	1(%rax), %rcx
	movw	%r13w, 2(%r15,%rax,4)
	cmpq	%rax, %rsi
	jne	.L164
.L211:
	movl	%r9d, -124(%rbp)
	cmpl	$1, %edx
	jg	.L213
.L112:
	movq	5992(%rdi), %r9
	movslq	%edx, %rax
	testq	%r8, %r8
	je	.L214
	movq	6000(%rdi), %rcx
	movl	-124(%rbp), %esi
	.p2align 4,,10
	.p2align 3
.L122:
	cmpl	$1, %esi
	jle	.L215
	movl	$1, %r10d
	movl	$0, 3092(%rdi,%rax,4)
	addq	$1, %rax
	movw	%r10w, (%r15)
	movb	$0, 5388(%rdi)
	movzwl	2(%r8), %r10d
	subq	%r10, %rcx
	cmpl	$1, %eax
	jle	.L122
.L209:
	movl	%esi, -124(%rbp)
	movq	%rcx, 6000(%rdi)
.L120:
	movl	$1, %eax
	movl	$0, %ecx
	subl	%edx, %eax
	cmpl	$1, %edx
	cmovle	%eax, %ecx
	negq	%rax
	cmpl	$1, %edx
	leal	1(%rdx,%rcx), %ebx
	movl	$0, %edx
	cmovg	%rdx, %rax
	movl	%ebx, 5380(%rdi)
	leaq	-1(%r9,%rax), %rax
	movq	%rax, 5992(%rdi)
.L116:
	movq	-136(%rbp), %rax
	movl	-124(%rbp), %esi
	movl	%ebx, -104(%rbp)
	movl	%esi, 8(%rax)
	movl	%ebx, %eax
	sarl	%eax
	movslq	%eax, %r10
	leal	(%rax,%rax), %r14d
	.p2align 4,,10
	.p2align 3
.L131:
	movl	3088(%rdi,%r10,4), %ebx
	movslq	%r10d, %rdx
	movl	%r14d, %eax
	movl	%ebx, -120(%rbp)
	cmpl	%r14d, -104(%rbp)
	jl	.L127
	movslq	%ebx, %r13
	movzwl	(%r15,%r13,4), %ebx
	movw	%bx, -112(%rbp)
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L129:
	movl	%ecx, 3088(%rdi,%rdx,4)
	leal	(%rax,%rax), %ecx
	movslq	%eax, %rdx
	cmpl	%ecx, -104(%rbp)
	jl	.L127
	movl	%ecx, %eax
.L130:
	movslq	%eax, %rcx
	leaq	(%rdi,%rcx,4), %r8
	movslq	3088(%r8), %r9
	movzwl	(%r15,%r9,4), %esi
	movq	%r9, %rcx
	cmpl	%eax, -104(%rbp)
	jle	.L128
	movslq	3092(%r8), %r12
	leal	1(%rax), %ebx
	movzwl	(%r15,%r12,4), %r11d
	cmpw	%si, %r11w
	jb	.L166
	jne	.L128
	movzbl	5388(%rdi,%r12), %r11d
	cmpb	5388(%rdi,%r9), %r11b
	cmovbe	%r12d, %ecx
	cmovbe	%ebx, %eax
	.p2align 4,,10
	.p2align 3
.L128:
	cmpw	%si, -112(%rbp)
	jb	.L127
	jne	.L129
	movslq	%ecx, %rsi
	movzbl	5388(%rdi,%rsi), %ebx
	cmpb	%bl, 5388(%rdi,%r13)
	ja	.L129
	.p2align 4,,10
	.p2align 3
.L127:
	movl	-120(%rbp), %eax
	subq	$1, %r10
	movl	%eax, 3088(%rdi,%rdx,4)
	subl	$2, %r14d
	jne	.L131
	movslq	5380(%rdi), %rax
	movslq	-128(%rbp), %r12
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L134:
	leal	-1(%rax), %edx
	movl	3088(%rdi,%rax,4), %eax
	movslq	3092(%rdi), %r13
	movq	%r15, %rsi
	movl	%edx, 5380(%rdi)
	movl	%eax, 3092(%rdi)
	call	pqdownheap.constprop.0
	movslq	5384(%rdi), %rcx
	movslq	3092(%rdi), %rax
	movq	%rcx, %rdx
	leaq	(%rdi,%rcx,4), %rcx
	leaq	(%r15,%rax,4), %rsi
	subl	$2, %edx
	movl	%r13d, 3084(%rcx)
	movl	%edx, 5384(%rdi)
	movl	%eax, 3080(%rcx)
	leaq	(%r15,%r13,4), %rcx
	movzwl	(%rsi), %edx
	addw	(%rcx), %dx
	movw	%dx, (%r15,%r12,4)
	movzbl	5388(%rdi,%rax), %edx
	movzbl	5388(%rdi,%r13), %r8d
	leal	1(%rdx), %eax
	cmpb	%dl, %r8b
	leal	1(%r8), %r9d
	cmovnb	%r9d, %eax
	movb	%al, 5388(%rdi,%r12)
	leal	(%rbx,%r12), %eax
	movw	%ax, 2(%rsi)
	movq	%r15, %rsi
	movw	%ax, 2(%rcx)
	movl	%r12d, 3092(%rdi)
	addq	$1, %r12
	call	pqdownheap.constprop.0
	movslq	5380(%rdi), %rax
	cmpl	$1, %eax
	jg	.L134
	movslq	5384(%rdi), %rcx
	movq	-136(%rbp), %rbx
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	movslq	3092(%rdi), %rax
	leal	-1(%rcx), %edx
	movq	(%rbx), %r8
	movl	8(%rbx), %r13d
	movl	%edx, 5384(%rdi)
	movslq	%edx, %rdx
	movl	%eax, 3088(%rdi,%rdx,4)
	movq	16(%rbx), %rdx
	movq	8(%rdx), %rbx
	movq	(%rdx), %r14
	movl	16(%rdx), %r12d
	movl	24(%rdx), %edx
	movups	%xmm0, 3056(%rdi)
	movups	%xmm0, 3072(%rdi)
	movq	%rbx, -104(%rbp)
	movw	%si, 2(%r8,%rax,4)
	cmpl	$572, %ecx
	jg	.L149
	xorl	%r11d, %r11d
	testq	%r14, %r14
	je	.L216
	.p2align 4,,10
	.p2align 3
.L148:
	movslq	3088(%rdi,%rcx,4), %r9
	movq	%r9, %rsi
	salq	$2, %r9
	leaq	(%r8,%r9), %r10
	movzwl	2(%r10), %eax
	movzwl	2(%r8,%rax,4), %eax
	addl	$1, %eax
	cmpl	%eax, %edx
	jge	.L145
	addl	$1, %r11d
	movl	%edx, %eax
.L145:
	movw	%ax, 2(%r10)
	cmpl	%esi, %r13d
	jl	.L146
	movslq	%eax, %rbx
	addw	$1, 3056(%rdi,%rbx,2)
	xorl	%ebx, %ebx
	cmpl	%esi, %r12d
	jg	.L147
	movq	-104(%rbp), %rbx
	subl	%r12d, %esi
	movslq	%esi, %rsi
	movl	(%rbx,%rsi,4), %ebx
	addl	%ebx, %eax
.L147:
	movzwl	(%r10), %esi
	movl	%eax, %eax
	imulq	%rsi, %rax
	addq	5992(%rdi), %rax
	movq	%rax, %xmm0
	movzwl	2(%r14,%r9), %eax
	addl	%ebx, %eax
	imulq	%rax, %rsi
	addq	6000(%rdi), %rsi
	movq	%rsi, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 5992(%rdi)
.L146:
	addq	$1, %rcx
	cmpl	$572, %ecx
	jle	.L148
.L144:
	testl	%r11d, %r11d
	je	.L149
	leal	-1(%rdx), %ebx
	leal	-2(%r11), %r12d
	subl	$1, %r11d
	movslq	%edx, %r14
	movslq	%ebx, %rax
	andl	$-2, %r11d
	movl	%r12d, %esi
	movq	%r8, -120(%rbp)
	subl	%r11d, %esi
	leal	-2(%rdx), %ecx
	leaq	(%rdi,%rax,2), %r9
	movl	%ebx, -104(%rbp)
	leal	-4(%rdx), %eax
	movq	%r15, -112(%rbp)
	leaq	(%rdi,%r14,2), %r11
	movl	%esi, %r8d
	movl	%r13d, -136(%rbp)
	movl	%eax, %ebx
	movl	%ecx, %r13d
	jmp	.L152
	.p2align 4,,10
	.p2align 3
.L217:
	movzwl	3054(%r9), %eax
	movl	%r13d, %esi
	testw	%ax, %ax
	jne	.L170
	movzwl	3052(%r9), %eax
	leal	-3(%rdx), %ecx
	testw	%ax, %ax
	jne	.L171
	movzwl	3050(%r9), %eax
	movl	%ebx, %esi
	testw	%ax, %ax
	jne	.L150
	movzwl	3048(%r9), %eax
	leal	-5(%rdx), %ecx
	testw	%ax, %ax
	jne	.L172
	movzwl	3046(%r9), %eax
	leal	-6(%rdx), %esi
	testw	%ax, %ax
	jne	.L150
	movzwl	3044(%r9), %eax
	leal	-7(%rdx), %ecx
	testw	%ax, %ax
	jne	.L176
	movzwl	3042(%r9), %eax
	leal	-8(%rdx), %esi
	testw	%ax, %ax
	jne	.L150
	movzwl	3040(%r9), %eax
	leal	-9(%rdx), %ecx
	testw	%ax, %ax
	jne	.L176
	movzwl	3038(%r9), %eax
	leal	-10(%rdx), %esi
	testw	%ax, %ax
	jne	.L150
	movzwl	3036(%r9), %eax
	leal	-11(%rdx), %ecx
	testw	%ax, %ax
	jne	.L176
	movzwl	3034(%r9), %eax
	leal	-12(%rdx), %esi
	testw	%ax, %ax
	jne	.L150
	movzwl	3032(%r9), %eax
	leal	-13(%rdx), %ecx
	testw	%ax, %ax
	jne	.L176
	movzwl	3030(%r9), %eax
	leal	-14(%rdx), %esi
	testw	%ax, %ax
	jne	.L150
	movzwl	3028(%r9), %eax
	leal	-15(%rdx), %r15d
	testw	%ax, %ax
	jne	.L177
	movzwl	3026(%r9), %eax
	leal	-16(%rdx), %ecx
	testw	%ax, %ax
	jne	.L178
	movzwl	3024(%r9), %eax
	leal	-17(%rdx), %esi
	.p2align 4,,10
	.p2align 3
.L150:
	movslq	%esi, %rsi
	subl	$1, %eax
	movslq	%ecx, %rcx
	movw	%ax, 3056(%rdi,%rsi,2)
	addw	$2, 3056(%rdi,%rcx,2)
	subw	$1, 3056(%r11)
	cmpl	%r8d, %r12d
	je	.L203
	subl	$2, %r12d
.L152:
	movzwl	3054(%r11), %eax
	movq	%r14, %r10
	testw	%ax, %ax
	je	.L217
	movl	-104(%rbp), %esi
	movl	%edx, %ecx
	jmp	.L150
	.p2align 4,,10
	.p2align 3
.L166:
	movl	%r11d, %esi
	movl	%r12d, %ecx
	movl	%ebx, %eax
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L215:
	addl	$1, %esi
	movl	$1, %r11d
	movslq	%esi, %r10
	movl	%esi, 3092(%rdi,%rax,4)
	addq	$1, %rax
	movw	%r11w, (%r15,%r10,4)
	movb	$0, 5388(%rdi,%r10)
	movzwl	2(%r8,%r10,4), %r10d
	subq	%r10, %rcx
	cmpl	$1, %eax
	jle	.L122
	jmp	.L209
	.p2align 4,,10
	.p2align 3
.L149:
	movzwl	3056(%rdi), %eax
	leal	(%rax,%rax), %edx
	movzwl	3058(%rdi), %eax
	movw	%dx, -94(%rbp)
	addl	%edx, %eax
	movzwl	3060(%rdi), %edx
	addl	%eax, %eax
	addl	%eax, %edx
	movw	%ax, -92(%rbp)
	leal	(%rdx,%rdx), %eax
	movzwl	3062(%rdi), %edx
	movw	%ax, -90(%rbp)
	addl	%eax, %edx
	movzwl	3064(%rdi), %eax
	addl	%edx, %edx
	addl	%edx, %eax
	movw	%dx, -88(%rbp)
	leal	(%rax,%rax), %edx
	movzwl	3066(%rdi), %eax
	movw	%dx, -86(%rbp)
	addl	%edx, %eax
	movzwl	3068(%rdi), %edx
	addl	%eax, %eax
	addl	%eax, %edx
	movw	%ax, -84(%rbp)
	leal	(%rdx,%rdx), %eax
	movzwl	3070(%rdi), %edx
	movw	%ax, -82(%rbp)
	addl	%eax, %edx
	movzwl	3072(%rdi), %eax
	addl	%edx, %edx
	addl	%edx, %eax
	movw	%dx, -80(%rbp)
	leal	(%rax,%rax), %edx
	movzwl	3074(%rdi), %eax
	movw	%dx, -78(%rbp)
	addl	%edx, %eax
	movzwl	3076(%rdi), %edx
	addl	%eax, %eax
	addl	%eax, %edx
	movw	%ax, -76(%rbp)
	leal	(%rdx,%rdx), %eax
	movzwl	3078(%rdi), %edx
	movw	%ax, -74(%rbp)
	addl	%eax, %edx
	movzwl	3080(%rdi), %eax
	addl	%edx, %edx
	addl	%edx, %eax
	movw	%dx, -72(%rbp)
	leal	(%rax,%rax), %edx
	movzwl	3082(%rdi), %eax
	movw	%dx, -70(%rbp)
	addl	%edx, %eax
	movzwl	3084(%rdi), %edx
	addl	%eax, %eax
	movw	%ax, -68(%rbp)
	addl	%edx, %eax
	addl	%eax, %eax
	cmpl	$-1, -124(%rbp)
	movw	%ax, -66(%rbp)
	je	.L111
	movslq	-124(%rbp), %rdx
	movq	%r15, %rax
	leaq	4(%r15,%rdx,4), %r8
	jmp	.L160
	.p2align 4,,10
	.p2align 3
.L158:
	addq	$4, %rax
	cmpq	%rax, %r8
	je	.L111
.L160:
	movzwl	2(%rax), %edx
	testl	%edx, %edx
	je	.L158
	movslq	%edx, %rsi
	movzwl	-96(%rbp,%rsi,2), %ecx
	leal	1(%rcx), %edi
	movw	%di, -96(%rbp,%rsi,2)
	xorl	%edi, %edi
	.p2align 4,,10
	.p2align 3
.L159:
	movl	%ecx, %esi
	shrl	%ecx
	andl	$1, %esi
	orl	%edi, %esi
	leal	(%rsi,%rsi), %edi
	subl	$1, %edx
	jne	.L159
	movw	%si, (%rax)
	addq	$4, %rax
	cmpq	%rax, %r8
	jne	.L160
.L111:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L218
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L176:
	.cfi_restore_state
	movl	%esi, %r15d
	movl	%ecx, %esi
	movl	%r15d, %ecx
	jmp	.L150
	.p2align 4,,10
	.p2align 3
.L203:
	movq	-112(%rbp), %r15
	movq	-120(%rbp), %r8
	movl	-136(%rbp), %r13d
	movl	-104(%rbp), %ebx
	testl	%edx, %edx
	je	.L149
	movl	$573, %esi
	.p2align 4,,10
	.p2align 3
.L157:
	movzwl	3056(%rdi,%r10,2), %r9d
	testl	%r9d, %r9d
	je	.L153
	leal	-1(%rsi), %eax
	cltq
	.p2align 4,,10
	.p2align 3
.L156:
	movslq	3088(%rdi,%rax,4), %rcx
	movl	%eax, %esi
	cmpl	%ecx, %r13d
	jl	.L154
	leaq	(%r8,%rcx,4), %r11
	movzwl	2(%r11), %r12d
	cmpl	%edx, %r12d
	je	.L155
	movq	%r10, %r14
	subq	%r12, %r14
	movzwl	(%r11), %r12d
	movq	%r14, %rcx
	imulq	%r12, %rcx
	addq	%rcx, 5992(%rdi)
	movw	%dx, 2(%r11)
.L155:
	subq	$1, %rax
	subl	$1, %r9d
	jne	.L156
.L153:
	movl	%ebx, %edx
	subq	$1, %r10
	testl	%ebx, %ebx
	je	.L149
	subl	$1, %ebx
	jmp	.L157
	.p2align 4,,10
	.p2align 3
.L154:
	subq	$1, %rax
	jmp	.L156
	.p2align 4,,10
	.p2align 3
.L170:
	movl	-104(%rbp), %ecx
	jmp	.L150
	.p2align 4,,10
	.p2align 3
.L171:
	movl	%ecx, %esi
	movl	%r13d, %ecx
	jmp	.L150
	.p2align 4,,10
	.p2align 3
.L172:
	movl	%ecx, %esi
	movl	%ebx, %ecx
	jmp	.L150
	.p2align 4,,10
	.p2align 3
.L216:
	movq	%rbx, %r10
	.p2align 4,,10
	.p2align 3
.L143:
	movslq	3088(%rdi,%rcx,4), %rax
	leaq	(%r8,%rax,4), %r9
	movq	%rax, %rsi
	movzwl	2(%r9), %eax
	movzwl	2(%r8,%rax,4), %eax
	addl	$1, %eax
	cmpl	%eax, %edx
	jge	.L139
	addl	$1, %r11d
	movl	%edx, %eax
.L139:
	movw	%ax, 2(%r9)
	cmpl	%esi, %r13d
	jl	.L140
	movslq	%eax, %rbx
	addw	$1, 3056(%rdi,%rbx,2)
	cmpl	%esi, %r12d
	jg	.L142
	subl	%r12d, %esi
	movslq	%esi, %rsi
	addl	(%r10,%rsi,4), %eax
.L142:
	movzwl	(%r9), %esi
	movl	%eax, %eax
	imulq	%rsi, %rax
	addq	%rax, 5992(%rdi)
.L140:
	addq	$1, %rcx
	cmpl	$572, %ecx
	jle	.L143
	jmp	.L144
	.p2align 4,,10
	.p2align 3
.L214:
	movl	-124(%rbp), %ecx
	.p2align 4,,10
	.p2align 3
.L121:
	cmpl	$1, %ecx
	jle	.L219
	movl	$0, 3092(%rdi,%rax,4)
	movl	$1, %ebx
	addq	$1, %rax
	movw	%bx, (%r15)
	movb	$0, 5388(%rdi)
	cmpl	$1, %eax
	jle	.L121
	movl	%ecx, -124(%rbp)
	jmp	.L120
	.p2align 4,,10
	.p2align 3
.L219:
	addl	$1, %ecx
	movl	$1, %r12d
	movl	%ecx, 3092(%rdi,%rax,4)
	movslq	%ecx, %rsi
	addq	$1, %rax
	movw	%r12w, (%r15,%rsi,4)
	movb	$0, 5388(%rdi,%rsi)
	cmpl	$1, %eax
	jle	.L121
	movl	%ecx, -124(%rbp)
	jmp	.L120
	.p2align 4,,10
	.p2align 3
.L177:
	movl	%esi, %ecx
	movl	%r15d, %esi
	jmp	.L150
	.p2align 4,,10
	.p2align 3
.L178:
	movl	%ecx, %esi
	movl	%r15d, %ecx
	jmp	.L150
	.p2align 4,,10
	.p2align 3
.L213:
	movl	%edx, %ebx
	jmp	.L116
	.p2align 4,,10
	.p2align 3
.L163:
	movl	$-1, -124(%rbp)
	xorl	%edx, %edx
	jmp	.L112
.L218:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE44:
	.size	build_tree, .-build_tree
	.p2align 4
	.globl	_tr_init
	.hidden	_tr_init
	.type	_tr_init, @function
_tr_init:
.LFB39:
	.cfi_startproc
	endbr64
	leaq	static_l_desc(%rip), %rsi
	leaq	292(%rdi), %rax
	xorl	%r10d, %r10d
	movl	$0, 6020(%rdi)
	movq	%rsi, 3000(%rdi)
	leaq	static_d_desc(%rip), %rsi
	leaq	2584(%rdi), %rdx
	movq	%rsi, 3024(%rdi)
	leaq	2828(%rdi), %rsi
	leaq	static_bl_desc(%rip), %rcx
	movq	%rax, 2984(%rdi)
	movq	%rdx, 3008(%rdi)
	movq	%rsi, 3032(%rdi)
	movw	%r10w, 6016(%rdi)
	movq	%rcx, 3048(%rdi)
	leaq	1436(%rdi), %rcx
	.p2align 4,,10
	.p2align 3
.L221:
	xorl	%r9d, %r9d
	addq	$4, %rax
	movw	%r9w, -4(%rax)
	cmpq	%rcx, %rax
	jne	.L221
	movq	%rdx, %rax
	leaq	2704(%rdi), %rdx
	.p2align 4,,10
	.p2align 3
.L222:
	xorl	%r8d, %r8d
	addq	$4, %rax
	movw	%r8w, -4(%rax)
	cmpq	%rdx, %rax
	jne	.L222
	movq	%rsi, %rax
	leaq	2904(%rdi), %rdx
	.p2align 4,,10
	.p2align 3
.L223:
	xorl	%ecx, %ecx
	addq	$4, %rax
	movw	%cx, -4(%rax)
	cmpq	%rdx, %rax
	jne	.L223
	movl	$1, %eax
	pxor	%xmm0, %xmm0
	movl	$0, 6008(%rdi)
	movw	%ax, 1316(%rdi)
	movl	$0, 5980(%rdi)
	movups	%xmm0, 5992(%rdi)
	ret
	.cfi_endproc
.LFE39:
	.size	_tr_init, .-_tr_init
	.p2align 4
	.globl	_tr_stored_block
	.hidden	_tr_stored_block
	.type	_tr_stored_block, @function
_tr_stored_block:
.LFB49:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%ecx, %r9d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	movzwl	%r9w, %edx
	pushq	%rbx
	.cfi_offset 3, -32
	movq	40(%rdi), %rax
	movq	%rdi, %rbx
	movl	6020(%rdi), %ecx
	leaq	1(%rax), %r8
	addq	16(%rdi), %rax
	movl	%edx, %edi
	sall	%cl, %edi
	orw	6016(%rbx), %di
	movw	%di, 6016(%rbx)
	cmpl	$13, %ecx
	jle	.L228
	movq	%r8, 40(%rbx)
	movb	%dil, (%rax)
	movq	40(%rbx), %rax
	movzbl	6017(%rbx), %edi
	movq	16(%rbx), %rcx
	leaq	1(%rax), %r8
	movq	%r8, 40(%rbx)
	movb	%dil, (%rcx,%rax)
	movl	6020(%rbx), %eax
	movl	$16, %ecx
	subl	%eax, %ecx
	sarl	%cl, %edx
	leal	-13(%rax), %ecx
	movq	40(%rbx), %rax
	movw	%dx, 6016(%rbx)
	movl	%edx, %edi
	leaq	1(%rax), %r8
	movl	%ecx, 6020(%rbx)
	addq	16(%rbx), %rax
	cmpl	$8, %ecx
	jle	.L230
.L233:
	movq	%r8, 40(%rbx)
	movb	%dil, (%rax)
	movq	40(%rbx), %rax
	movzbl	6017(%rbx), %ecx
	movq	16(%rbx), %rdx
	leaq	1(%rax), %rdi
	movq	%rdi, 40(%rbx)
	movb	%cl, (%rdx,%rax)
	movq	40(%rbx), %rax
	leaq	1(%rax), %r8
	addq	16(%rbx), %rax
.L231:
	xorl	%edx, %edx
	movq	%r8, 40(%rbx)
	movw	%dx, 6016(%rbx)
	movl	$0, 6020(%rbx)
	movb	%r12b, (%rax)
	movq	40(%rbx), %rax
	movq	16(%rbx), %rdx
	leaq	1(%rax), %rcx
	movq	%rcx, 40(%rbx)
	movl	%r12d, %ecx
	shrw	$8, %cx
	movb	%cl, (%rdx,%rax)
	movq	40(%rbx), %rax
	movq	16(%rbx), %rdx
	leaq	1(%rax), %rcx
	movq	%rcx, 40(%rbx)
	movl	%r12d, %ecx
	notl	%ecx
	movb	%cl, (%rdx,%rax)
	movq	40(%rbx), %rdx
	movq	16(%rbx), %rcx
	leaq	1(%rdx), %rax
	movq	%rax, 40(%rbx)
	movl	%r12d, %eax
	notl	%eax
	shrw	$8, %ax
	movb	%al, (%rcx,%rdx)
	movq	%r12, %rdx
	movq	40(%rbx), %rdi
	addq	16(%rbx), %rdi
	call	memcpy@PLT
	addq	%r12, 40(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L228:
	.cfi_restore_state
	addl	$3, %ecx
	movl	%ecx, 6020(%rbx)
	cmpl	$8, %ecx
	jg	.L233
.L230:
	testl	%ecx, %ecx
	jle	.L231
	movq	%r8, 40(%rbx)
	movb	%dil, (%rax)
	movq	40(%rbx), %rax
	leaq	1(%rax), %r8
	addq	16(%rbx), %rax
	jmp	.L231
	.cfi_endproc
.LFE49:
	.size	_tr_stored_block, .-_tr_stored_block
	.p2align 4
	.globl	_tr_flush_bits
	.hidden	_tr_flush_bits
	.type	_tr_flush_bits, @function
_tr_flush_bits:
.LFB50:
	.cfi_startproc
	endbr64
	movl	6020(%rdi), %eax
	cmpl	$16, %eax
	je	.L237
	cmpl	$7, %eax
	jle	.L234
	movq	40(%rdi), %rax
	movzwl	6016(%rdi), %ecx
	movq	16(%rdi), %rdx
	leaq	1(%rax), %rsi
	movq	%rsi, 40(%rdi)
	movb	%cl, (%rdx,%rax)
	subl	$8, 6020(%rdi)
	shrw	$8, 6016(%rdi)
.L234:
	ret
	.p2align 4,,10
	.p2align 3
.L237:
	movq	40(%rdi), %rax
	movzwl	6016(%rdi), %ecx
	movq	16(%rdi), %rdx
	leaq	1(%rax), %rsi
	movq	%rsi, 40(%rdi)
	movb	%cl, (%rdx,%rax)
	movq	40(%rdi), %rax
	movzbl	6017(%rdi), %ecx
	movq	16(%rdi), %rdx
	leaq	1(%rax), %rsi
	movq	%rsi, 40(%rdi)
	movb	%cl, (%rdx,%rax)
	xorl	%eax, %eax
	movw	%ax, 6016(%rdi)
	movl	$0, 6020(%rdi)
	ret
	.cfi_endproc
.LFE50:
	.size	_tr_flush_bits, .-_tr_flush_bits
	.p2align 4
	.globl	_tr_align
	.hidden	_tr_align
	.type	_tr_align, @function
_tr_align:
.LFB51:
	.cfi_startproc
	endbr64
	movl	6020(%rdi), %ecx
	movl	$2, %esi
	movl	%esi, %edx
	sall	%cl, %edx
	orw	6016(%rdi), %dx
	movw	%dx, 6016(%rdi)
	cmpl	$13, %ecx
	jle	.L239
	movq	40(%rdi), %rax
	movq	16(%rdi), %rcx
	leaq	1(%rax), %r8
	movq	%r8, 40(%rdi)
	movb	%dl, (%rcx,%rax)
	movq	40(%rdi), %rax
	movzbl	6017(%rdi), %ecx
	movq	16(%rdi), %rdx
	leaq	1(%rax), %r8
	movq	%r8, 40(%rdi)
	movb	%cl, (%rdx,%rax)
	movl	6020(%rdi), %eax
	movl	$16, %ecx
	subl	%eax, %ecx
	sarl	%cl, %esi
	leal	-13(%rax), %ecx
	movw	%si, 6016(%rdi)
	movl	%esi, %edx
	movl	%ecx, 6020(%rdi)
.L240:
	cmpl	$9, %ecx
	jle	.L241
	movq	40(%rdi), %rax
	movq	16(%rdi), %rcx
	leaq	1(%rax), %rsi
	movq	%rsi, 40(%rdi)
	movb	%dl, (%rcx,%rax)
	movq	40(%rdi), %rax
	movzbl	6017(%rdi), %ecx
	movq	16(%rdi), %rdx
	leaq	1(%rax), %rsi
	movq	%rsi, 40(%rdi)
	movb	%cl, (%rdx,%rax)
	movl	6020(%rdi), %eax
	xorl	%edx, %edx
	movw	%dx, 6016(%rdi)
	xorl	%edx, %edx
	leal	-9(%rax), %ecx
	movl	%ecx, 6020(%rdi)
	cmpl	$16, %ecx
	je	.L245
.L243:
	cmpl	$7, %ecx
	jle	.L238
	movq	40(%rdi), %rax
	movq	16(%rdi), %rcx
	leaq	1(%rax), %rsi
	movq	%rsi, 40(%rdi)
	movb	%dl, (%rcx,%rax)
	subl	$8, 6020(%rdi)
	shrw	$8, 6016(%rdi)
.L238:
	ret
	.p2align 4,,10
	.p2align 3
.L241:
	addl	$7, %ecx
	movl	%ecx, 6020(%rdi)
	cmpl	$16, %ecx
	jne	.L243
.L245:
	movq	40(%rdi), %rax
	movq	16(%rdi), %rcx
	leaq	1(%rax), %rsi
	movq	%rsi, 40(%rdi)
	movb	%dl, (%rcx,%rax)
	movq	40(%rdi), %rax
	movzbl	6017(%rdi), %ecx
	movq	16(%rdi), %rdx
	leaq	1(%rax), %rsi
	movq	%rsi, 40(%rdi)
	movb	%cl, (%rdx,%rax)
	xorl	%eax, %eax
	movw	%ax, 6016(%rdi)
	movl	$0, 6020(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L239:
	addl	$3, %ecx
	movl	%ecx, 6020(%rdi)
	jmp	.L240
	.cfi_endproc
.LFE51:
	.size	_tr_align, .-_tr_align
	.p2align 4
	.globl	_tr_flush_block
	.hidden	_tr_flush_block
	.type	_tr_flush_block, @function
_tr_flush_block:
.LFB52:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%ecx, %r14d
	leaq	4(%rdx), %rcx
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	276(%rdi), %r10d
	testq	%rsi, %rsi
	setne	%r9b
	testl	%r10d, %r10d
	jle	.L247
	movq	(%rdi), %rdi
	leaq	292(%r13), %rbx
	cmpl	$2, 88(%rdi)
	je	.L325
.L248:
	leaq	2984(%r13), %rsi
	movq	%r13, %rdi
	movq	%r8, -72(%rbp)
	leaq	2584(%r13), %r12
	movb	%r9b, -57(%rbp)
	movq	%rcx, -56(%rbp)
	call	build_tree
	leaq	3008(%r13), %rsi
	movq	%r13, %rdi
	call	build_tree
	movl	2992(%r13), %edx
	movq	%r13, %rdi
	movq	%rbx, %rsi
	call	scan_tree
	movl	3016(%r13), %edx
	movq	%r12, %rsi
	call	scan_tree
	leaq	3032(%r13), %rsi
	call	build_tree
	movq	-56(%rbp), %rcx
	movzbl	-57(%rbp), %r9d
	cmpw	$0, 2890(%r13)
	movq	-72(%rbp), %r8
	jne	.L287
	cmpw	$0, 2834(%r13)
	jne	.L288
	cmpw	$0, 2886(%r13)
	jne	.L289
	cmpw	$0, 2838(%r13)
	jne	.L290
	cmpw	$0, 2882(%r13)
	jne	.L291
	cmpw	$0, 2842(%r13)
	jne	.L292
	cmpw	$0, 2878(%r13)
	jne	.L293
	cmpw	$0, 2846(%r13)
	jne	.L294
	cmpw	$0, 2874(%r13)
	jne	.L295
	cmpw	$0, 2850(%r13)
	jne	.L296
	cmpw	$0, 2870(%r13)
	jne	.L297
	cmpw	$0, 2854(%r13)
	jne	.L298
	cmpw	$0, 2866(%r13)
	jne	.L299
	cmpw	$0, 2858(%r13)
	jne	.L300
	cmpw	$0, 2862(%r13)
	jne	.L301
	cmpw	$0, 2830(%r13)
	movl	$9, %esi
	movl	$2, %edx
	jne	.L326
.L254:
	movq	5992(%r13), %rax
	leaq	14(%rsi,%rax), %rax
	movq	%rax, 5992(%r13)
	addq	$10, %rax
	shrq	$3, %rax
	movq	%rax, %rsi
	movq	6000(%r13), %rax
	addq	$10, %rax
	shrq	$3, %rax
	cmpq	%rax, %rsi
	movq	%rax, %rdi
	cmovbe	%rsi, %rdi
	cmpq	%rcx, %rdi
	jb	.L257
	testb	%r9b, %r9b
	jne	.L255
.L257:
	cmpl	$4, 280(%r13)
	movl	6020(%r13), %ecx
	movzwl	6016(%r13), %r8d
	je	.L259
	cmpq	%rax, %rsi
	jnb	.L259
	leal	4(%r14), %eax
	movzwl	%ax, %eax
	cmpl	$13, %ecx
	jg	.L327
	leal	3(%rcx), %esi
	sall	%cl, %eax
	movl	%r8d, %edi
	movl	%esi, 6020(%r13)
	orl	%eax, %edi
.L266:
	movl	2992(%r13), %r9d
	movl	3016(%r13), %r15d
	leal	-256(%r9), %eax
	movzwl	%ax, %eax
	cmpl	$11, %esi
	jle	.L267
	movl	%esi, %ecx
	movl	%eax, %r11d
	movq	16(%r13), %rsi
	sall	%cl, %r11d
	movq	40(%r13), %rcx
	orl	%r11d, %edi
	leaq	1(%rcx), %r8
	movw	%di, 6016(%r13)
	movq	%r8, 40(%r13)
	movb	%dil, (%rsi,%rcx)
	movq	40(%r13), %rcx
	movzbl	6017(%r13), %edi
	movq	16(%r13), %rsi
	leaq	1(%rcx), %r8
	movq	%r8, 40(%r13)
	movb	%dil, (%rsi,%rcx)
	movl	6020(%r13), %edi
	movl	$16, %ecx
	leal	-11(%rdi), %esi
	subl	%edi, %ecx
	movl	%esi, 6020(%r13)
	sarl	%cl, %eax
.L268:
	movzwl	%r15w, %edi
	cmpl	$11, %esi
	jle	.L269
	movl	%esi, %ecx
	movl	%edi, %r11d
	movq	16(%r13), %rsi
	sall	%cl, %r11d
	movq	40(%r13), %rcx
	orl	%r11d, %eax
	leaq	1(%rcx), %r8
	movw	%ax, 6016(%r13)
	movq	%r8, 40(%r13)
	movb	%al, (%rsi,%rcx)
	movq	40(%r13), %rax
	movzbl	6017(%r13), %esi
	movq	16(%r13), %rcx
	leaq	1(%rax), %r8
	movq	%r8, 40(%r13)
	movb	%sil, (%rcx,%rax)
	movl	6020(%r13), %eax
	movl	$16, %ecx
	subl	%eax, %ecx
	leal	-11(%rax), %esi
	sarl	%cl, %edi
	movl	%esi, 6020(%r13)
	movl	%edi, %eax
	leal	-3(%rdx), %edi
	movzwl	%di, %edi
	cmpl	$12, %esi
	jle	.L271
.L333:
	movl	%esi, %ecx
	movl	%edi, %r11d
	movq	16(%r13), %rsi
	sall	%cl, %r11d
	movq	40(%r13), %rcx
	orl	%r11d, %eax
	leaq	1(%rcx), %r8
	movw	%ax, 6016(%r13)
	movq	%r8, 40(%r13)
	movb	%al, (%rsi,%rcx)
	movq	40(%r13), %rax
	movzbl	6017(%r13), %esi
	movq	16(%r13), %rcx
	leaq	1(%rax), %r8
	movq	%r8, 40(%r13)
	movb	%sil, (%rcx,%rax)
	movl	6020(%r13), %esi
	movl	$16, %ecx
	subl	%esi, %ecx
	sarl	%cl, %edi
	leal	-12(%rsi), %ecx
	movl	%ecx, 6020(%r13)
	movl	%edi, %eax
.L272:
	leaq	bl_order(%rip), %rsi
	movl	$16, %r8d
	leaq	(%rdx,%rsi), %rdi
	movl	$16, %edx
	jmp	.L276
	.p2align 4,,10
	.p2align 3
.L328:
	movq	40(%r13), %rcx
	movq	16(%r13), %r10
	leaq	1(%rcx), %r11
	movq	%r11, 40(%r13)
	movb	%al, (%r10,%rcx)
	movq	40(%r13), %rax
	movzbl	6017(%r13), %r10d
	movq	16(%r13), %rcx
	leaq	1(%rax), %r11
	movq	%r11, 40(%r13)
	movb	%r10b, (%rcx,%rax)
	movl	6020(%r13), %r10d
	movl	%r8d, %ecx
	subl	%r10d, %ecx
	sarl	%cl, %edx
	leal	-13(%r10), %ecx
	movw	%dx, 6016(%r13)
	movl	%edx, %eax
	movl	%ecx, 6020(%r13)
	cmpq	%rdi, %rsi
	je	.L275
.L329:
	movzbl	1(%rsi), %edx
	addq	$1, %rsi
.L276:
	movzwl	2830(%r13,%rdx,4), %edx
	movl	%edx, %r10d
	sall	%cl, %r10d
	orl	%r10d, %eax
	movw	%ax, 6016(%r13)
	cmpl	$13, %ecx
	jg	.L328
	addl	$3, %ecx
	movl	%ecx, 6020(%r13)
	cmpq	%rdi, %rsi
	jne	.L329
.L275:
	movl	%r9d, %edx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	send_tree
	movl	%r15d, %edx
	movq	%r12, %rsi
	call	send_tree
	movq	%r12, %rdx
	movq	%rbx, %rsi
	call	compress_block
	jmp	.L260
.L325:
	movq	%rbx, %rax
	leaq	420(%r13), %rsi
	movl	$4093624447, %edx
	.p2align 4,,10
	.p2align 3
.L251:
	testb	$1, %dl
	je	.L249
	cmpw	$0, (%rax)
	jne	.L283
.L249:
	addq	$4, %rax
	shrq	%rdx
	cmpq	%rax, %rsi
	jne	.L251
	movabsq	$281470681808895, %rdx
	movl	$1, %eax
	testq	%rdx, 328(%r13)
	jne	.L250
	cmpw	$0, 344(%r13)
	jne	.L250
	leaq	1316(%r13), %rax
	jmp	.L252
	.p2align 4,,10
	.p2align 3
.L330:
	addq	$4, %rsi
	cmpq	%rax, %rsi
	je	.L283
.L252:
	cmpw	$0, (%rsi)
	je	.L330
	movl	$1, %eax
.L250:
	movl	%eax, 88(%rdi)
	jmp	.L248
.L247:
	leaq	5(%rdx), %rax
	cmpq	%rcx, %rax
	jb	.L303
	leaq	292(%rdi), %rbx
	leaq	2584(%rdi), %r12
	testb	%r9b, %r9b
	jne	.L255
.L303:
	movl	6020(%r13), %ecx
	movzwl	6016(%r13), %r8d
	leaq	292(%r13), %rbx
	leaq	2584(%r13), %r12
.L259:
	cmpl	$13, %ecx
	jle	.L263
	leal	2(%r14), %edx
	movq	16(%r13), %rsi
	movzwl	%dx, %edx
	movl	%edx, %eax
	sall	%cl, %eax
	movq	40(%r13), %rcx
	orl	%r8d, %eax
	leaq	1(%rcx), %rdi
	movw	%ax, 6016(%r13)
	movq	%rdi, 40(%r13)
	movb	%al, (%rsi,%rcx)
	movq	40(%r13), %rax
	movzbl	6017(%r13), %esi
	movq	16(%r13), %rcx
	leaq	1(%rax), %rdi
	movq	%rdi, 40(%r13)
	movb	%sil, (%rcx,%rax)
	movl	6020(%r13), %eax
	movl	$16, %ecx
	subl	%eax, %ecx
	subl	$13, %eax
	sarl	%cl, %edx
	movl	%eax, 6020(%r13)
	movw	%dx, 6016(%r13)
.L264:
	leaq	static_dtree(%rip), %rdx
	leaq	static_ltree(%rip), %rsi
	movq	%r13, %rdi
	call	compress_block
.L260:
	movq	%rbx, %rax
	leaq	1436(%r13), %rdx
	.p2align 4,,10
	.p2align 3
.L277:
	xorl	%edi, %edi
	addq	$4, %rax
	movw	%di, -4(%rax)
	cmpq	%rdx, %rax
	jne	.L277
	movq	%r12, %rax
	leaq	2704(%r13), %rdx
	.p2align 4,,10
	.p2align 3
.L278:
	xorl	%esi, %esi
	addq	$4, %rax
	movw	%si, -4(%rax)
	cmpq	%rdx, %rax
	jne	.L278
	leaq	2828(%r13), %rax
	leaq	2904(%r13), %rdx
	.p2align 4,,10
	.p2align 3
.L279:
	xorl	%ecx, %ecx
	addq	$4, %rax
	movw	%cx, -4(%rax)
	cmpq	%rdx, %rax
	jne	.L279
	movl	$1, %edx
	pxor	%xmm0, %xmm0
	movl	$0, 6008(%r13)
	movw	%dx, 1316(%r13)
	movl	$0, 5980(%r13)
	movups	%xmm0, 5992(%r13)
	testl	%r14d, %r14d
	jne	.L331
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L255:
	.cfi_restore_state
	movl	%r14d, %ecx
	movq	%r15, %rdx
	movq	%r8, %rsi
	movq	%r13, %rdi
	call	_tr_stored_block
	jmp	.L260
.L331:
	movl	6020(%r13), %eax
	cmpl	$8, %eax
	jg	.L332
	testl	%eax, %eax
	jle	.L282
	movzwl	6016(%r13), %ecx
.L324:
	movq	40(%r13), %rax
	movq	16(%r13), %rdx
	leaq	1(%rax), %rsi
	movq	%rsi, 40(%r13)
	movb	%cl, (%rdx,%rax)
.L282:
	movl	$0, 6020(%r13)
	xorl	%eax, %eax
	movw	%ax, 6016(%r13)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L263:
	.cfi_restore_state
	leal	2(%r14), %eax
	movzwl	%ax, %eax
	sall	%cl, %eax
	addl	$3, %ecx
	orl	%r8d, %eax
	movl	%ecx, 6020(%r13)
	movw	%ax, 6016(%r13)
	jmp	.L264
.L332:
	movq	40(%r13), %rax
	movzwl	6016(%r13), %ecx
	movq	16(%r13), %rdx
	leaq	1(%rax), %rsi
	movq	%rsi, 40(%r13)
	movb	%cl, (%rdx,%rax)
	movzbl	6017(%r13), %ecx
	jmp	.L324
.L327:
	movl	%eax, %edi
	movq	16(%r13), %rsi
	sall	%cl, %edi
	movq	40(%r13), %rcx
	orl	%r8d, %edi
	leaq	1(%rcx), %r8
	movw	%di, 6016(%r13)
	movq	%r8, 40(%r13)
	movb	%dil, (%rsi,%rcx)
	movq	40(%r13), %rcx
	movzbl	6017(%r13), %edi
	movq	16(%r13), %rsi
	leaq	1(%rcx), %r8
	movq	%r8, 40(%r13)
	movb	%dil, (%rsi,%rcx)
	movl	6020(%r13), %edi
	movl	$16, %ecx
	subl	%edi, %ecx
	leal	-13(%rdi), %esi
	sarl	%cl, %eax
	movl	%esi, 6020(%r13)
	movl	%eax, %edi
	jmp	.L266
.L269:
	movl	%esi, %ecx
	addl	$5, %esi
	sall	%cl, %edi
	movl	%esi, 6020(%r13)
	orl	%edi, %eax
	leal	-3(%rdx), %edi
	movzwl	%di, %edi
	cmpl	$12, %esi
	jg	.L333
.L271:
	movl	%esi, %ecx
	sall	%cl, %edi
	leal	4(%rsi), %ecx
	movl	%ecx, 6020(%r13)
	orl	%edi, %eax
	jmp	.L272
.L267:
	movl	%esi, %ecx
	addl	$5, %esi
	sall	%cl, %eax
	movl	%esi, 6020(%r13)
	orl	%edi, %eax
	jmp	.L268
.L283:
	xorl	%eax, %eax
	movl	%eax, 88(%rdi)
	jmp	.L248
.L289:
	movl	$16, %edx
.L253:
	movslq	%edx, %rax
	leaq	3(%rax,%rax,2), %rsi
	jmp	.L254
.L291:
	movl	$14, %edx
	jmp	.L253
.L287:
	movl	$18, %edx
	jmp	.L253
.L290:
	movl	$15, %edx
	jmp	.L253
.L288:
	movl	$17, %edx
	jmp	.L253
.L300:
	movl	$5, %edx
	jmp	.L253
.L298:
	movl	$7, %edx
	jmp	.L253
.L299:
	movl	$6, %edx
	jmp	.L253
.L292:
	movl	$13, %edx
	jmp	.L253
.L293:
	movl	$12, %edx
	jmp	.L253
.L294:
	movl	$11, %edx
	jmp	.L253
.L295:
	movl	$10, %edx
	jmp	.L253
.L296:
	movl	$9, %edx
	jmp	.L253
.L297:
	movl	$8, %edx
	jmp	.L253
.L301:
	movl	$4, %edx
	jmp	.L253
.L326:
	movl	$3, %edx
	jmp	.L253
	.cfi_endproc
.LFE52:
	.size	_tr_flush_block, .-_tr_flush_block
	.p2align 4
	.globl	_tr_tally
	.hidden	_tr_tally
	.type	_tr_tally, @function
_tr_tally:
.LFB53:
	.cfi_startproc
	endbr64
	movl	5980(%rdi), %ecx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %eax
	movq	5968(%rdi), %rsi
	leal	1(%rcx), %r8d
	movl	%r8d, 5980(%rdi)
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movb	%al, (%rsi,%rcx)
	movl	5980(%rdi), %ecx
	movq	5968(%rdi), %rsi
	leal	1(%rcx), %r8d
	movl	%r8d, 5980(%rdi)
	movb	%ah, (%rsi,%rcx)
	movl	5980(%rdi), %ecx
	movq	5968(%rdi), %rsi
	leal	1(%rcx), %r8d
	movl	%r8d, 5980(%rdi)
	movb	%dl, (%rsi,%rcx)
	testl	%eax, %eax
	jne	.L335
	movl	%edx, %edx
	addw	$1, 292(%rdi,%rdx,4)
.L336:
	movl	5984(%rdi), %eax
	cmpl	%eax, 5980(%rdi)
	sete	%al
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movzbl	%al, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L335:
	.cfi_restore_state
	movl	%edx, %edx
	leaq	_length_code(%rip), %rcx
	subl	$1, %eax
	addl	$1, 6008(%rdi)
	movzbl	(%rcx,%rdx), %edx
	addw	$1, 1320(%rdi,%rdx,4)
	cmpl	$255, %eax
	ja	.L337
.L340:
	leaq	_dist_code(%rip), %rdx
	movzbl	(%rdx,%rax), %eax
	addw	$1, 2584(%rdi,%rax,4)
	jmp	.L336
	.p2align 4,,10
	.p2align 3
.L337:
	shrl	$7, %eax
	addl	$256, %eax
	jmp	.L340
	.cfi_endproc
.LFE53:
	.size	_tr_tally, .-_tr_tally
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	static_bl_desc, @object
	.size	static_bl_desc, 32
static_bl_desc:
	.quad	0
	.quad	extra_blbits
	.long	0
	.long	19
	.long	7
	.zero	4
	.align 32
	.type	static_d_desc, @object
	.size	static_d_desc, 32
static_d_desc:
	.quad	static_dtree
	.quad	extra_dbits
	.long	0
	.long	30
	.long	15
	.zero	4
	.align 32
	.type	static_l_desc, @object
	.size	static_l_desc, 32
static_l_desc:
	.quad	static_ltree
	.quad	extra_lbits
	.long	257
	.long	286
	.long	15
	.zero	4
	.section	.rodata
	.align 32
	.type	base_dist, @object
	.size	base_dist, 120
base_dist:
	.long	0
	.long	1
	.long	2
	.long	3
	.long	4
	.long	6
	.long	8
	.long	12
	.long	16
	.long	24
	.long	32
	.long	48
	.long	64
	.long	96
	.long	128
	.long	192
	.long	256
	.long	384
	.long	512
	.long	768
	.long	1024
	.long	1536
	.long	2048
	.long	3072
	.long	4096
	.long	6144
	.long	8192
	.long	12288
	.long	16384
	.long	24576
	.align 32
	.type	base_length, @object
	.size	base_length, 116
base_length:
	.long	0
	.long	1
	.long	2
	.long	3
	.long	4
	.long	5
	.long	6
	.long	7
	.long	8
	.long	10
	.long	12
	.long	14
	.long	16
	.long	20
	.long	24
	.long	28
	.long	32
	.long	40
	.long	48
	.long	56
	.long	64
	.long	80
	.long	96
	.long	112
	.long	128
	.long	160
	.long	192
	.long	224
	.long	0
	.hidden	_length_code
	.globl	_length_code
	.align 32
	.type	_length_code, @object
	.size	_length_code, 256
_length_code:
	.string	""
	.ascii	"\001\002\003\004\005\006\007\b\b\t\t\n\n\013\013\f\f\f\f\r\r"
	.ascii	"\r\r\016\016\016\016\017\017\017\017\020\020\020\020\020\020"
	.ascii	"\020\020\021\021\021\021\021\021\021\021\022\022\022\022\022"
	.ascii	"\022\022\022\023\023\023\023\023\023\023\023\024\024\024\024"
	.ascii	"\024\024\024\024\024\024\024\024\024\024\024\024\025\025\025"
	.ascii	"\025\025\025\025\025\025\025\025\025\025\025\025\025\026\026"
	.ascii	"\026\026\026\026\026\026\026\026\026\026\026\026\026\026\027"
	.ascii	"\027\027\027\027\027\027\027\027\027\027\027\027\027\027\027"
	.ascii	"\030\030\030\030\030\030\030\030\030\030\030\030\030\030\030"
	.ascii	"\030\030\030\030\030\030\030\030\030\030\030\030\030\030\030"
	.ascii	"\030\030\031\031\031\031\031\031\031\031\031\031\031\031\031"
	.ascii	"\031\031\031\031\031\031\031\031\031\031\031\031\031\031\031"
	.ascii	"\031\031\031\031\032\032\032\032\032\032\032\032\032\032\032"
	.ascii	"\032\032\032\032\032\032\032\032\032\032\032\032\032\032\032"
	.ascii	"\032\032\032\032\032\032\033\033\033\033\033\033\033\033\033"
	.ascii	"\033\033\033\033\033\033\033\033\033\033\033\033\033\033\033"
	.ascii	"\033\033\033\033\033\033\033\034"
	.hidden	_dist_code
	.globl	_dist_code
	.align 32
	.type	_dist_code, @object
	.size	_dist_code, 512
_dist_code:
	.string	""
	.string	"\001\002\003\004\004\005\005\006\006\006\006\007\007\007\007\b\b\b\b\b\b\b\b\t\t\t\t\t\t\t\t\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\013\013\013\013\013\013\013\013\013\013\013\013\013\013\013\013\f\f\f\f\f\f\f\f\f\f\f\f\f\f\f\f\f\f\f\f\f\f\f\f\f\f\f\f\f\f\f\f\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\016\016\016\016\016\016\016\016\016\016\016\016\016\016\016\016\016\016\016\016\016\016\016\016\016\016\016\016\016\016\016\016\016\016\016\016\016\016\016\016\016\016\016\016\016\016\016\016\016\016\016\016\016\016\016\016\016\016\016\016\016\016\016\016\017\017\017\017\017\017\017\017\017\017\017\017\017\017\017\017\017\017\017\017\017\017\017\017\017\017\017\017\017\017\017\017\017\017\017\017\017\017\017\017\017\017\017\017\017\017\017\017\017\017\017\017\017\017\017\017\017\017\017\017\017\017\017\017"
	.string	""
	.ascii	"\020\021\022\022\023\023\024\024\024\024\025\025\025\025\026"
	.ascii	"\026\026\026\026\026\026\026\027\027\027\027\027\027\027\027"
	.ascii	"\030\030\030\030\030\030\030\030\030\030\030\030\030\030\030"
	.ascii	"\030\031\031\031\031\031\031\031\031\031\031\031\031\031\031"
	.ascii	"\031\031\032\032\032\032\032\032\032\032\032\032\032\032\032"
	.ascii	"\032\032\032\032\032\032\032\032\032\032\032\032\032\032\032"
	.ascii	"\032\032\032\032\033\033\033\033\033\033\033\033\033\033\033"
	.ascii	"\033\033\033\033\033\033\033\033\033\033\033\033\033\033\033"
	.ascii	"\033\033\033\033\033\033\034\034\034\034\034\034\034\034\034"
	.ascii	"\034\034\034\034\034\034\034\034\034\034\034\034\034\034\034"
	.ascii	"\034\034\034\034\034\034\034\034\034\034\034\034\034\034\034"
	.ascii	"\034\034\034\034\034\034\034\034\034\034\034\034\034\034\034"
	.ascii	"\034\034\034\034\034\034\034\034\034\034\035\035\035\035\035"
	.ascii	"\035\035\035\035\035\035\035\035\035\035\035\035\035\035\035"
	.ascii	"\035\035\035\035\035\035\035\035\035\035\035\035\035\035\035"
	.ascii	"\035\035\035\035\035\035\035\035\035\035\035\035\035\035\035"
	.ascii	"\035\035\035\035\035\035\035\035\035\035\035\035\035\035"
	.align 32
	.type	static_dtree, @object
	.size	static_dtree, 120
static_dtree:
	.value	0
	.value	5
	.value	16
	.value	5
	.value	8
	.value	5
	.value	24
	.value	5
	.value	4
	.value	5
	.value	20
	.value	5
	.value	12
	.value	5
	.value	28
	.value	5
	.value	2
	.value	5
	.value	18
	.value	5
	.value	10
	.value	5
	.value	26
	.value	5
	.value	6
	.value	5
	.value	22
	.value	5
	.value	14
	.value	5
	.value	30
	.value	5
	.value	1
	.value	5
	.value	17
	.value	5
	.value	9
	.value	5
	.value	25
	.value	5
	.value	5
	.value	5
	.value	21
	.value	5
	.value	13
	.value	5
	.value	29
	.value	5
	.value	3
	.value	5
	.value	19
	.value	5
	.value	11
	.value	5
	.value	27
	.value	5
	.value	7
	.value	5
	.value	23
	.value	5
	.align 32
	.type	static_ltree, @object
	.size	static_ltree, 1152
static_ltree:
	.value	12
	.value	8
	.value	140
	.value	8
	.value	76
	.value	8
	.value	204
	.value	8
	.value	44
	.value	8
	.value	172
	.value	8
	.value	108
	.value	8
	.value	236
	.value	8
	.value	28
	.value	8
	.value	156
	.value	8
	.value	92
	.value	8
	.value	220
	.value	8
	.value	60
	.value	8
	.value	188
	.value	8
	.value	124
	.value	8
	.value	252
	.value	8
	.value	2
	.value	8
	.value	130
	.value	8
	.value	66
	.value	8
	.value	194
	.value	8
	.value	34
	.value	8
	.value	162
	.value	8
	.value	98
	.value	8
	.value	226
	.value	8
	.value	18
	.value	8
	.value	146
	.value	8
	.value	82
	.value	8
	.value	210
	.value	8
	.value	50
	.value	8
	.value	178
	.value	8
	.value	114
	.value	8
	.value	242
	.value	8
	.value	10
	.value	8
	.value	138
	.value	8
	.value	74
	.value	8
	.value	202
	.value	8
	.value	42
	.value	8
	.value	170
	.value	8
	.value	106
	.value	8
	.value	234
	.value	8
	.value	26
	.value	8
	.value	154
	.value	8
	.value	90
	.value	8
	.value	218
	.value	8
	.value	58
	.value	8
	.value	186
	.value	8
	.value	122
	.value	8
	.value	250
	.value	8
	.value	6
	.value	8
	.value	134
	.value	8
	.value	70
	.value	8
	.value	198
	.value	8
	.value	38
	.value	8
	.value	166
	.value	8
	.value	102
	.value	8
	.value	230
	.value	8
	.value	22
	.value	8
	.value	150
	.value	8
	.value	86
	.value	8
	.value	214
	.value	8
	.value	54
	.value	8
	.value	182
	.value	8
	.value	118
	.value	8
	.value	246
	.value	8
	.value	14
	.value	8
	.value	142
	.value	8
	.value	78
	.value	8
	.value	206
	.value	8
	.value	46
	.value	8
	.value	174
	.value	8
	.value	110
	.value	8
	.value	238
	.value	8
	.value	30
	.value	8
	.value	158
	.value	8
	.value	94
	.value	8
	.value	222
	.value	8
	.value	62
	.value	8
	.value	190
	.value	8
	.value	126
	.value	8
	.value	254
	.value	8
	.value	1
	.value	8
	.value	129
	.value	8
	.value	65
	.value	8
	.value	193
	.value	8
	.value	33
	.value	8
	.value	161
	.value	8
	.value	97
	.value	8
	.value	225
	.value	8
	.value	17
	.value	8
	.value	145
	.value	8
	.value	81
	.value	8
	.value	209
	.value	8
	.value	49
	.value	8
	.value	177
	.value	8
	.value	113
	.value	8
	.value	241
	.value	8
	.value	9
	.value	8
	.value	137
	.value	8
	.value	73
	.value	8
	.value	201
	.value	8
	.value	41
	.value	8
	.value	169
	.value	8
	.value	105
	.value	8
	.value	233
	.value	8
	.value	25
	.value	8
	.value	153
	.value	8
	.value	89
	.value	8
	.value	217
	.value	8
	.value	57
	.value	8
	.value	185
	.value	8
	.value	121
	.value	8
	.value	249
	.value	8
	.value	5
	.value	8
	.value	133
	.value	8
	.value	69
	.value	8
	.value	197
	.value	8
	.value	37
	.value	8
	.value	165
	.value	8
	.value	101
	.value	8
	.value	229
	.value	8
	.value	21
	.value	8
	.value	149
	.value	8
	.value	85
	.value	8
	.value	213
	.value	8
	.value	53
	.value	8
	.value	181
	.value	8
	.value	117
	.value	8
	.value	245
	.value	8
	.value	13
	.value	8
	.value	141
	.value	8
	.value	77
	.value	8
	.value	205
	.value	8
	.value	45
	.value	8
	.value	173
	.value	8
	.value	109
	.value	8
	.value	237
	.value	8
	.value	29
	.value	8
	.value	157
	.value	8
	.value	93
	.value	8
	.value	221
	.value	8
	.value	61
	.value	8
	.value	189
	.value	8
	.value	125
	.value	8
	.value	253
	.value	8
	.value	19
	.value	9
	.value	275
	.value	9
	.value	147
	.value	9
	.value	403
	.value	9
	.value	83
	.value	9
	.value	339
	.value	9
	.value	211
	.value	9
	.value	467
	.value	9
	.value	51
	.value	9
	.value	307
	.value	9
	.value	179
	.value	9
	.value	435
	.value	9
	.value	115
	.value	9
	.value	371
	.value	9
	.value	243
	.value	9
	.value	499
	.value	9
	.value	11
	.value	9
	.value	267
	.value	9
	.value	139
	.value	9
	.value	395
	.value	9
	.value	75
	.value	9
	.value	331
	.value	9
	.value	203
	.value	9
	.value	459
	.value	9
	.value	43
	.value	9
	.value	299
	.value	9
	.value	171
	.value	9
	.value	427
	.value	9
	.value	107
	.value	9
	.value	363
	.value	9
	.value	235
	.value	9
	.value	491
	.value	9
	.value	27
	.value	9
	.value	283
	.value	9
	.value	155
	.value	9
	.value	411
	.value	9
	.value	91
	.value	9
	.value	347
	.value	9
	.value	219
	.value	9
	.value	475
	.value	9
	.value	59
	.value	9
	.value	315
	.value	9
	.value	187
	.value	9
	.value	443
	.value	9
	.value	123
	.value	9
	.value	379
	.value	9
	.value	251
	.value	9
	.value	507
	.value	9
	.value	7
	.value	9
	.value	263
	.value	9
	.value	135
	.value	9
	.value	391
	.value	9
	.value	71
	.value	9
	.value	327
	.value	9
	.value	199
	.value	9
	.value	455
	.value	9
	.value	39
	.value	9
	.value	295
	.value	9
	.value	167
	.value	9
	.value	423
	.value	9
	.value	103
	.value	9
	.value	359
	.value	9
	.value	231
	.value	9
	.value	487
	.value	9
	.value	23
	.value	9
	.value	279
	.value	9
	.value	151
	.value	9
	.value	407
	.value	9
	.value	87
	.value	9
	.value	343
	.value	9
	.value	215
	.value	9
	.value	471
	.value	9
	.value	55
	.value	9
	.value	311
	.value	9
	.value	183
	.value	9
	.value	439
	.value	9
	.value	119
	.value	9
	.value	375
	.value	9
	.value	247
	.value	9
	.value	503
	.value	9
	.value	15
	.value	9
	.value	271
	.value	9
	.value	143
	.value	9
	.value	399
	.value	9
	.value	79
	.value	9
	.value	335
	.value	9
	.value	207
	.value	9
	.value	463
	.value	9
	.value	47
	.value	9
	.value	303
	.value	9
	.value	175
	.value	9
	.value	431
	.value	9
	.value	111
	.value	9
	.value	367
	.value	9
	.value	239
	.value	9
	.value	495
	.value	9
	.value	31
	.value	9
	.value	287
	.value	9
	.value	159
	.value	9
	.value	415
	.value	9
	.value	95
	.value	9
	.value	351
	.value	9
	.value	223
	.value	9
	.value	479
	.value	9
	.value	63
	.value	9
	.value	319
	.value	9
	.value	191
	.value	9
	.value	447
	.value	9
	.value	127
	.value	9
	.value	383
	.value	9
	.value	255
	.value	9
	.value	511
	.value	9
	.value	0
	.value	7
	.value	64
	.value	7
	.value	32
	.value	7
	.value	96
	.value	7
	.value	16
	.value	7
	.value	80
	.value	7
	.value	48
	.value	7
	.value	112
	.value	7
	.value	8
	.value	7
	.value	72
	.value	7
	.value	40
	.value	7
	.value	104
	.value	7
	.value	24
	.value	7
	.value	88
	.value	7
	.value	56
	.value	7
	.value	120
	.value	7
	.value	4
	.value	7
	.value	68
	.value	7
	.value	36
	.value	7
	.value	100
	.value	7
	.value	20
	.value	7
	.value	84
	.value	7
	.value	52
	.value	7
	.value	116
	.value	7
	.value	3
	.value	8
	.value	131
	.value	8
	.value	67
	.value	8
	.value	195
	.value	8
	.value	35
	.value	8
	.value	163
	.value	8
	.value	99
	.value	8
	.value	227
	.value	8
	.align 16
	.type	bl_order, @object
	.size	bl_order, 19
bl_order:
	.string	"\020\021\022"
	.ascii	"\b\007\t\006\n\005\013\004\f\003\r\002\016\001\017"
	.align 32
	.type	extra_blbits, @object
	.size	extra_blbits, 76
extra_blbits:
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	2
	.long	3
	.long	7
	.align 32
	.type	extra_dbits, @object
	.size	extra_dbits, 120
extra_dbits:
	.long	0
	.long	0
	.long	0
	.long	0
	.long	1
	.long	1
	.long	2
	.long	2
	.long	3
	.long	3
	.long	4
	.long	4
	.long	5
	.long	5
	.long	6
	.long	6
	.long	7
	.long	7
	.long	8
	.long	8
	.long	9
	.long	9
	.long	10
	.long	10
	.long	11
	.long	11
	.long	12
	.long	12
	.long	13
	.long	13
	.align 32
	.type	extra_lbits, @object
	.size	extra_lbits, 116
extra_lbits:
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	1
	.long	1
	.long	1
	.long	1
	.long	2
	.long	2
	.long	2
	.long	2
	.long	3
	.long	3
	.long	3
	.long	3
	.long	4
	.long	4
	.long	4
	.long	4
	.long	5
	.long	5
	.long	5
	.long	5
	.long	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
