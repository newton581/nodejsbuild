	.file	"inftrees.c"
	.text
	.p2align 4
	.globl	inflate_table
	.hidden	inflate_table
	.type	inflate_table, @function
inflate_table:
.LFB38:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$184, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -160(%rbp)
	movq	%rcx, -192(%rbp)
	movq	%r8, -184(%rbp)
	movq	%r9, -136(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	testl	%edx, %edx
	je	.L19
	leal	-1(%rdx), %ecx
	movq	%rsi, %rax
	leaq	2(%rsi,%rcx,2), %rsi
	.p2align 4,,10
	.p2align 3
.L18:
	movzwl	(%rax), %ecx
	addq	$2, %rax
	addw	$1, -128(%rbp,%rcx,2)
	cmpq	%rax, %rsi
	jne	.L18
.L19:
	movzwl	-98(%rbp), %esi
	testw	%si, %si
	jne	.L57
	cmpw	$0, -100(%rbp)
	jne	.L58
	cmpw	$0, -102(%rbp)
	jne	.L4
	cmpw	$0, -104(%rbp)
	jne	.L5
	cmpw	$0, -106(%rbp)
	jne	.L6
	cmpw	$0, -108(%rbp)
	jne	.L7
	cmpw	$0, -110(%rbp)
	jne	.L8
	cmpw	$0, -112(%rbp)
	jne	.L9
	cmpw	$0, -114(%rbp)
	jne	.L10
	cmpw	$0, -116(%rbp)
	jne	.L11
	cmpw	$0, -118(%rbp)
	jne	.L12
	cmpw	$0, -120(%rbp)
	jne	.L13
	cmpw	$0, -122(%rbp)
	jne	.L14
	cmpw	$0, -124(%rbp)
	jne	.L15
	cmpw	$0, -126(%rbp)
	jne	.L187
	movq	-192(%rbp), %rbx
	xorl	%r9d, %r9d
	movq	(%rbx), %rax
	leaq	8(%rax), %rdx
	movl	$320, (%rax)
	movq	%rdx, (%rbx)
	movl	$320, 4(%rax)
	movq	-184(%rbp), %rax
	movl	$1, (%rax)
.L1:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L188
	addq	$184, %rsp
	movl	%r9d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L57:
	.cfi_restore_state
	movl	$15, %ebx
.L3:
	cmpw	$0, -126(%rbp)
	jne	.L189
.L53:
	cmpw	$0, -124(%rbp)
	jne	.L185
	cmpw	$0, -122(%rbp)
	jne	.L60
	cmpl	$4, %ebx
	je	.L61
	cmpw	$0, -120(%rbp)
	jne	.L62
	cmpl	$5, %ebx
	je	.L63
	cmpw	$0, -118(%rbp)
	jne	.L64
	cmpl	$6, %ebx
	je	.L65
	cmpw	$0, -116(%rbp)
	jne	.L66
	cmpl	$7, %ebx
	je	.L67
	cmpw	$0, -114(%rbp)
	jne	.L68
	cmpl	$8, %ebx
	je	.L69
	cmpw	$0, -112(%rbp)
	jne	.L70
	cmpl	$9, %ebx
	je	.L71
	cmpw	$0, -110(%rbp)
	jne	.L72
	cmpl	$10, %ebx
	je	.L73
	cmpw	$0, -108(%rbp)
	jne	.L74
	cmpl	$11, %ebx
	je	.L75
	cmpw	$0, -106(%rbp)
	jne	.L76
	cmpl	$12, %ebx
	je	.L77
	cmpw	$0, -104(%rbp)
	jne	.L78
	cmpl	$13, %ebx
	je	.L79
	cmpw	$0, -102(%rbp)
	jne	.L80
	cmpl	$15, %ebx
	jne	.L81
	cmpw	$0, -100(%rbp)
	jne	.L82
	movl	$15, -148(%rbp)
.L22:
	movzwl	-126(%rbp), %ecx
	movl	$2, %eax
	movl	%ecx, %r8d
	subl	%ecx, %eax
.L23:
	movzwl	-124(%rbp), %r9d
	addl	%eax, %eax
	movl	%r9d, %ecx
	subl	%r9d, %eax
	js	.L98
	movzwl	-122(%rbp), %r10d
	addl	%eax, %eax
	movl	%r10d, %r9d
	subl	%r10d, %eax
	js	.L98
	movzwl	-120(%rbp), %r11d
	addl	%eax, %eax
	movl	%r11d, %r10d
	subl	%r11d, %eax
	js	.L98
	movzwl	-118(%rbp), %r12d
	addl	%eax, %eax
	movl	%r12d, %r11d
	subl	%r12d, %eax
	js	.L98
	movzwl	-116(%rbp), %r12d
	addl	%eax, %eax
	movl	%r12d, %r13d
	subl	%r12d, %eax
	js	.L98
	movzwl	-114(%rbp), %r12d
	addl	%eax, %eax
	movl	%r12d, %r14d
	subl	%r12d, %eax
	js	.L98
	movzwl	-112(%rbp), %r12d
	addl	%eax, %eax
	movl	%r12d, %r15d
	subl	%r12d, %eax
	js	.L98
	movzwl	-110(%rbp), %r12d
	addl	%eax, %eax
	movw	%r12w, -144(%rbp)
	subl	%r12d, %eax
	js	.L98
	movzwl	-108(%rbp), %r12d
	addl	%eax, %eax
	movw	%r12w, -152(%rbp)
	subl	%r12d, %eax
	js	.L98
	movzwl	-106(%rbp), %r12d
	addl	%eax, %eax
	movw	%r12w, -164(%rbp)
	subl	%r12d, %eax
	js	.L98
	movzwl	-104(%rbp), %r12d
	addl	%eax, %eax
	movw	%r12w, -168(%rbp)
	subl	%r12d, %eax
	js	.L98
	movzwl	-102(%rbp), %r12d
	addl	%eax, %eax
	movw	%r12w, -172(%rbp)
	subl	%r12d, %eax
	js	.L98
	leal	(%rax,%rax), %r12d
	movzwl	-100(%rbp), %eax
	subl	%eax, %r12d
	movw	%ax, -140(%rbp)
	movl	%r12d, %eax
	js	.L98
	addl	%eax, %eax
	cmpl	%esi, %eax
	js	.L98
	je	.L24
	testl	%edi, %edi
	je	.L98
	cmpl	$1, -148(%rbp)
	jne	.L98
.L24:
	addl	%r8d, %ecx
	movq	-184(%rbp), %rax
	xorl	%esi, %esi
	movw	%r8w, -92(%rbp)
	movw	%cx, -90(%rbp)
	addl	%r9d, %ecx
	leal	-1(%rdx), %r8d
	movw	%cx, -88(%rbp)
	addl	%r10d, %ecx
	movl	(%rax), %eax
	movw	%cx, -86(%rbp)
	addl	%r11d, %ecx
	movw	%cx, -84(%rbp)
	addl	%r13d, %ecx
	movw	%cx, -82(%rbp)
	addl	%r14d, %ecx
	movw	%cx, -80(%rbp)
	addl	%r15d, %ecx
	movw	%cx, -78(%rbp)
	addw	-144(%rbp), %cx
	movw	%cx, -76(%rbp)
	addw	-152(%rbp), %cx
	movw	%cx, -74(%rbp)
	addw	-164(%rbp), %cx
	movw	%cx, -72(%rbp)
	addw	-168(%rbp), %cx
	movw	%cx, -70(%rbp)
	addw	-172(%rbp), %cx
	movw	%cx, -68(%rbp)
	addw	-140(%rbp), %cx
	movw	%cx, -66(%rbp)
	xorl	%ecx, %ecx
	movw	%si, -94(%rbp)
	testl	%edx, %edx
	je	.L31
	movq	-160(%rbp), %r9
	movq	-136(%rbp), %r11
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L100:
	movq	%rdx, %rcx
.L30:
	movzwl	(%r9,%rcx,2), %edx
	testw	%dx, %dx
	je	.L29
	movzwl	-96(%rbp,%rdx,2), %esi
	leal	1(%rsi), %r10d
	movw	%cx, (%r11,%rsi,2)
	movw	%r10w, -96(%rbp,%rdx,2)
.L29:
	leaq	1(%rcx), %rdx
	cmpq	%r8, %rcx
	jne	.L100
.L31:
	movl	-148(%rbp), %esi
	cmpl	%eax, %esi
	cmovbe	%esi, %eax
	cmpl	%ebx, %eax
	cmovb	%ebx, %eax
	movl	%eax, %esi
	movl	%eax, -140(%rbp)
	movl	$1, %eax
	movl	%esi, %ecx
	sall	%cl, %eax
	movl	%eax, -164(%rbp)
	testl	%edi, %edi
	je	.L99
	cmpl	$1, %edi
	je	.L27
	cmpl	$2, %edi
	sete	-173(%rbp)
	movzbl	-173(%rbp), %eax
	cmpl	$592, -164(%rbp)
	jbe	.L150
	testb	%al, %al
	je	.L150
.L47:
	movl	$1, %r9d
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L98:
	movl	$-1, %r9d
	jmp	.L1
.L27:
	cmpl	$852, -164(%rbp)
	ja	.L47
	leaq	lext.4047(%rip), %rax
	movb	$1, -174(%rbp)
	movq	%rax, -200(%rbp)
	leaq	lbase.4046(%rip), %rax
	movl	$257, -152(%rbp)
	movq	%rax, -208(%rbp)
	movb	$0, -173(%rbp)
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L99:
	movq	-136(%rbp), %rax
	movb	$0, -173(%rbp)
	movl	$20, -152(%rbp)
	movq	%rax, -200(%rbp)
	movq	%rax, -208(%rbp)
.L26:
	cmpl	$1, %edi
	sete	-174(%rbp)
.L32:
	movq	-192(%rbp), %rax
	xorl	%esi, %esi
	xorl	%r12d, %r12d
	xorl	%r14d, %r14d
	movl	$-1, -168(%rbp)
	movl	-140(%rbp), %r13d
	movl	$1, %r15d
	movq	(%rax), %r11
	movl	-164(%rbp), %eax
	subl	$1, %eax
	movq	%r11, -216(%rbp)
	movl	%eax, -172(%rbp)
	.p2align 4,,10
	.p2align 3
.L33:
	movq	-136(%rbp), %rdi
	movl	%r14d, %eax
	movl	%ebx, %r9d
	xorl	%r8d, %r8d
	movl	-152(%rbp), %ecx
	subl	%r12d, %r9d
	movzwl	(%rdi,%rax,2), %eax
	leal	1(%rax), %edx
	movl	%eax, %edi
	cmpl	%ecx, %edx
	jb	.L34
	cmpl	%ecx, %eax
	jb	.L103
	movq	-200(%rbp), %rdi
	subl	%ecx, %eax
	movzbl	(%rdi,%rax,2), %r8d
	movq	-208(%rbp), %rdi
	movzwl	(%rdi,%rax,2), %edi
.L34:
	movl	%ebx, %ecx
	movl	%r15d, %r10d
	movl	%r15d, %eax
	movl	%esi, %edx
	subl	%r12d, %ecx
	sall	%cl, %r10d
	movl	%r13d, %ecx
	sall	%cl, %eax
	movl	%r12d, %ecx
	movl	%eax, -144(%rbp)
	shrl	%cl, %edx
	movl	%edx, %ecx
	.p2align 4,,10
	.p2align 3
.L35:
	subl	%r10d, %eax
	leal	(%rcx,%rax), %edx
	leaq	(%r11,%rdx,4), %rdx
	movb	%r8b, (%rdx)
	movb	%r9b, 1(%rdx)
	movw	%di, 2(%rdx)
	jne	.L35
	leal	-1(%rbx), %ecx
	movl	%r15d, %eax
	sall	%cl, %eax
	testl	%eax, %esi
	je	.L36
	.p2align 4,,10
	.p2align 3
.L37:
	shrl	%eax
	testl	%eax, %esi
	jne	.L37
.L36:
	testl	%eax, %eax
	je	.L38
	leal	-1(%rax), %edx
	andl	%edx, %esi
	addl	%esi, %eax
.L38:
	movl	%ebx, %ecx
	addl	$1, %r14d
	subw	$1, -128(%rbp,%rcx,2)
	jne	.L39
	cmpl	%ebx, -148(%rbp)
	je	.L40
	movq	-136(%rbp), %rbx
	movl	%r14d, %edx
	movzwl	(%rbx,%rdx,2), %edx
	movq	-160(%rbp), %rbx
	movzwl	(%rbx,%rdx,2), %ebx
.L39:
	movl	-140(%rbp), %edi
	cmpl	%edi, %ebx
	jbe	.L41
	movl	-172(%rbp), %edx
	andl	%eax, %edx
	cmpl	-168(%rbp), %edx
	jne	.L190
.L41:
	movl	%eax, %esi
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L190:
	testl	%r12d, %r12d
	movl	-144(%rbp), %ecx
	movl	%ebx, %r13d
	movl	-148(%rbp), %r8d
	cmove	%edi, %r12d
	movl	%r15d, %edi
	leaq	(%r11,%rcx,4), %r11
	subl	%r12d, %r13d
	movl	%r13d, %ecx
	sall	%cl, %edi
	movl	%edi, %ecx
	cmpl	%ebx, %r8d
	jbe	.L43
	movl	%ebx, %esi
	movzwl	-128(%rbp,%rsi,2), %esi
	subl	%esi, %ecx
	testl	%ecx, %ecx
	jle	.L43
	leal	1(%r13), %edi
	addl	%ecx, %ecx
	leal	(%r12,%rdi), %esi
	cmpl	%esi, %r8d
	jbe	.L104
	movzwl	-128(%rbp,%rsi,2), %esi
	subl	%esi, %ecx
	testl	%ecx, %ecx
	jle	.L104
	leal	2(%r13), %esi
	addl	%ecx, %ecx
	leal	(%r12,%rsi), %edi
	cmpl	%edi, %r8d
	jbe	.L132
	movzwl	-128(%rbp,%rdi,2), %edi
	subl	%edi, %ecx
	testl	%ecx, %ecx
	jle	.L132
	leal	3(%r13), %esi
	addl	%ecx, %ecx
	leal	(%r12,%rsi), %edi
	cmpl	%edi, %r8d
	jbe	.L132
	movzwl	-128(%rbp,%rdi,2), %edi
	subl	%edi, %ecx
	testl	%ecx, %ecx
	jle	.L132
	leal	4(%r13), %esi
	addl	%ecx, %ecx
	leal	(%r12,%rsi), %edi
	cmpl	%edi, %r8d
	jbe	.L132
	movzwl	-128(%rbp,%rdi,2), %edi
	subl	%edi, %ecx
	testl	%ecx, %ecx
	jle	.L132
	leal	5(%r13), %esi
	addl	%ecx, %ecx
	leal	(%r12,%rsi), %edi
	cmpl	%edi, %r8d
	jbe	.L132
	movzwl	-128(%rbp,%rdi,2), %edi
	subl	%edi, %ecx
	testl	%ecx, %ecx
	jle	.L132
	leal	6(%r13), %esi
	addl	%ecx, %ecx
	leal	(%r12,%rsi), %edi
	cmpl	%edi, %r8d
	jbe	.L132
	movzwl	-128(%rbp,%rdi,2), %edi
	subl	%edi, %ecx
	testl	%ecx, %ecx
	jle	.L132
	leal	7(%r13), %esi
	addl	%ecx, %ecx
	leal	(%r12,%rsi), %edi
	cmpl	%edi, %r8d
	jbe	.L132
	movzwl	-128(%rbp,%rdi,2), %edi
	subl	%edi, %ecx
	testl	%ecx, %ecx
	jle	.L132
	leal	8(%r13), %esi
	addl	%ecx, %ecx
	leal	(%r12,%rsi), %edi
	cmpl	%edi, %r8d
	jbe	.L132
	movzwl	-128(%rbp,%rdi,2), %edi
	subl	%edi, %ecx
	testl	%ecx, %ecx
	jle	.L132
	leal	9(%r13), %esi
	addl	%ecx, %ecx
	leal	(%r12,%rsi), %edi
	cmpl	%edi, %r8d
	jbe	.L132
	movzwl	-128(%rbp,%rdi,2), %edi
	subl	%edi, %ecx
	testl	%ecx, %ecx
	jle	.L132
	leal	10(%r13), %esi
	addl	%ecx, %ecx
	leal	(%r12,%rsi), %edi
	cmpl	%edi, %r8d
	jbe	.L132
	movzwl	-128(%rbp,%rdi,2), %edi
	subl	%edi, %ecx
	testl	%ecx, %ecx
	jle	.L132
	leal	11(%r13), %esi
	addl	%ecx, %ecx
	leal	(%r12,%rsi), %edi
	cmpl	%edi, %r8d
	jbe	.L132
	movzwl	-128(%rbp,%rdi,2), %edi
	subl	%edi, %ecx
	testl	%ecx, %ecx
	jle	.L132
	leal	12(%r13), %esi
	addl	%ecx, %ecx
	leal	(%r12,%rsi), %edi
	cmpl	%edi, %r8d
	jbe	.L132
	movzwl	-128(%rbp,%rdi,2), %edi
	subl	%edi, %ecx
	testl	%ecx, %ecx
	jle	.L132
	leal	13(%r13), %esi
	addl	%ecx, %ecx
	leal	(%r12,%rsi), %edi
	cmpl	%edi, %r8d
	jbe	.L132
	movzwl	-128(%rbp,%rdi,2), %edi
	subl	%edi, %ecx
	testl	%ecx, %ecx
	jle	.L132
	leal	14(%r13), %esi
	addl	%ecx, %ecx
	leal	(%r12,%rsi), %edi
	cmpl	%edi, %r8d
	jbe	.L132
	movzwl	-128(%rbp,%rdi,2), %edi
	subl	%edi, %ecx
	testl	%ecx, %ecx
	jle	.L132
	leal	15(%r13), %esi
	addl	%ecx, %ecx
	leal	(%r12,%rsi), %edi
	cmpl	%edi, %r8d
	jbe	.L132
	movzwl	-128(%rbp,%rdi,2), %edi
	addl	$16, %r13d
	subl	%edi, %ecx
	testl	%ecx, %ecx
	cmovle	%esi, %r13d
	.p2align 4,,10
	.p2align 3
.L43:
	movl	%r15d, %esi
	movl	%r13d, %ecx
	sall	%cl, %esi
	addl	%esi, -164(%rbp)
	movl	-164(%rbp), %edi
	cmpl	$852, %edi
	jbe	.L148
	cmpb	$0, -174(%rbp)
	jne	.L47
.L148:
	cmpl	$592, -164(%rbp)
	jbe	.L149
	cmpb	$0, -173(%rbp)
	jne	.L47
.L149:
	movq	-216(%rbp), %rdi
	movzbl	-140(%rbp), %esi
	movl	%edx, %ecx
	movl	%edx, -168(%rbp)
	leaq	(%rdi,%rcx,4), %rcx
	movb	%sil, 1(%rcx)
	movq	%r11, %rsi
	subq	%rdi, %rsi
	movb	%r13b, (%rcx)
	sarq	$2, %rsi
	movw	%si, 2(%rcx)
	movl	%eax, %esi
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L103:
	xorl	%edi, %edi
	movl	$96, %r8d
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L132:
	movl	%esi, %r13d
	jmp	.L43
.L104:
	movl	%edi, %r13d
	jmp	.L43
.L40:
	testl	%eax, %eax
	je	.L50
	leaq	(%r11,%rax,4), %rax
	xorl	%edx, %edx
	movb	$64, (%rax)
	movb	%r9b, 1(%rax)
	movw	%dx, 2(%rax)
.L50:
	movq	-216(%rbp), %rbx
	movl	-164(%rbp), %eax
	xorl	%r9d, %r9d
	leaq	(%rbx,%rax,4), %rax
	movq	-192(%rbp), %rbx
	movq	%rax, (%rbx)
	movq	-184(%rbp), %rax
	movl	-140(%rbp), %ebx
	movl	%ebx, (%rax)
	jmp	.L1
.L58:
	movl	$14, %ebx
	jmp	.L3
.L14:
	cmpw	$0, -126(%rbp)
	jne	.L191
	cmpw	$0, -124(%rbp)
	jne	.L147
	movl	$3, -148(%rbp)
	movl	$3, %ebx
	jmp	.L22
.L4:
	cmpw	$0, -126(%rbp)
	jne	.L137
	movl	$13, %ebx
	jmp	.L53
.L5:
	cmpw	$0, -126(%rbp)
	jne	.L138
	movl	$12, %ebx
	jmp	.L53
.L6:
	cmpw	$0, -126(%rbp)
	jne	.L139
	movl	$11, %ebx
	jmp	.L53
.L7:
	cmpw	$0, -126(%rbp)
	jne	.L140
	movl	$10, %ebx
	jmp	.L53
.L8:
	cmpw	$0, -126(%rbp)
	jne	.L141
	movl	$9, %ebx
	jmp	.L53
.L150:
	leaq	dext.4049(%rip), %rax
	movl	$0, -152(%rbp)
	movq	%rax, -200(%rbp)
	leaq	dbase.4048(%rip), %rax
	movq	%rax, -208(%rbp)
	jmp	.L26
.L13:
	cmpw	$0, -126(%rbp)
	jne	.L134
	movl	$4, %ebx
	jmp	.L53
.L15:
	movl	$2, -148(%rbp)
	cmpw	$0, -126(%rbp)
	jne	.L16
	movl	$2, %ebx
	jmp	.L22
.L187:
	movl	$1, -148(%rbp)
.L16:
	movzwl	-126(%rbp), %ecx
	movl	$2, %eax
	movl	$-1, %r9d
	movl	%ecx, %r8d
	subl	%ecx, %eax
	js	.L1
	movl	$1, %ebx
	jmp	.L23
.L61:
	movl	$4, -148(%rbp)
	jmp	.L22
.L60:
	movl	$3, %ecx
.L21:
	movzwl	-126(%rbp), %r9d
	movl	$2, %eax
	movl	%ebx, -148(%rbp)
	movl	%ecx, %ebx
	movl	%r9d, %r8d
	subl	%r9d, %eax
	jmp	.L23
.L63:
	movl	$5, -148(%rbp)
	jmp	.L22
.L62:
	movl	$4, %ecx
	jmp	.L21
.L67:
	movl	$7, -148(%rbp)
	jmp	.L22
.L66:
	movl	$6, %ecx
	jmp	.L21
.L65:
	movl	$6, -148(%rbp)
	jmp	.L22
.L64:
	movl	$5, %ecx
	jmp	.L21
.L9:
	cmpw	$0, -126(%rbp)
	jne	.L142
	movl	$8, %ebx
	jmp	.L53
.L73:
	movl	$10, -148(%rbp)
	jmp	.L22
.L71:
	movl	$9, -148(%rbp)
	jmp	.L22
.L70:
	movl	$8, %ecx
	jmp	.L21
.L69:
	movl	$8, -148(%rbp)
	jmp	.L22
.L68:
	movl	$7, %ecx
	jmp	.L21
.L72:
	movl	$9, %ecx
	jmp	.L21
.L75:
	movl	$11, -148(%rbp)
	jmp	.L22
.L74:
	movl	$10, %ecx
	jmp	.L21
.L79:
	movl	$13, -148(%rbp)
	jmp	.L22
.L78:
	movl	$12, %ecx
	jmp	.L21
.L77:
	movl	$12, -148(%rbp)
	jmp	.L22
.L76:
	movl	$11, %ecx
	jmp	.L21
.L81:
	movl	$14, -148(%rbp)
	movl	$14, %ebx
	jmp	.L22
.L80:
	movl	$13, %ecx
	jmp	.L21
.L82:
	movl	$14, %ecx
	jmp	.L21
.L189:
	movl	%ebx, -148(%rbp)
	jmp	.L16
.L147:
	movl	$3, %ebx
.L185:
	movl	$2, %ecx
	jmp	.L21
.L141:
	movl	$9, -148(%rbp)
	jmp	.L16
.L137:
	movl	$13, -148(%rbp)
	jmp	.L16
.L140:
	movl	$10, -148(%rbp)
	jmp	.L16
.L138:
	movl	$12, -148(%rbp)
	jmp	.L16
.L139:
	movl	$11, -148(%rbp)
	jmp	.L16
.L11:
	cmpw	$0, -126(%rbp)
	jne	.L144
	movl	$6, %ebx
	jmp	.L53
.L142:
	movl	$8, -148(%rbp)
	jmp	.L16
.L191:
	movl	$3, -148(%rbp)
	jmp	.L16
.L144:
	movl	$6, -148(%rbp)
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L134:
	movl	$4, -148(%rbp)
	jmp	.L16
.L188:
	call	__stack_chk_fail@PLT
.L10:
	cmpw	$0, -126(%rbp)
	jne	.L143
	movl	$7, %ebx
	jmp	.L53
.L12:
	cmpw	$0, -126(%rbp)
	jne	.L145
	movl	$5, %ebx
	jmp	.L53
.L143:
	movl	$7, -148(%rbp)
	jmp	.L16
.L145:
	movl	$5, -148(%rbp)
	jmp	.L16
	.cfi_endproc
.LFE38:
	.size	inflate_table, .-inflate_table
	.section	.rodata
	.align 32
	.type	dext.4049, @object
	.size	dext.4049, 64
dext.4049:
	.value	16
	.value	16
	.value	16
	.value	16
	.value	17
	.value	17
	.value	18
	.value	18
	.value	19
	.value	19
	.value	20
	.value	20
	.value	21
	.value	21
	.value	22
	.value	22
	.value	23
	.value	23
	.value	24
	.value	24
	.value	25
	.value	25
	.value	26
	.value	26
	.value	27
	.value	27
	.value	28
	.value	28
	.value	29
	.value	29
	.value	64
	.value	64
	.align 32
	.type	dbase.4048, @object
	.size	dbase.4048, 64
dbase.4048:
	.value	1
	.value	2
	.value	3
	.value	4
	.value	5
	.value	7
	.value	9
	.value	13
	.value	17
	.value	25
	.value	33
	.value	49
	.value	65
	.value	97
	.value	129
	.value	193
	.value	257
	.value	385
	.value	513
	.value	769
	.value	1025
	.value	1537
	.value	2049
	.value	3073
	.value	4097
	.value	6145
	.value	8193
	.value	12289
	.value	16385
	.value	24577
	.value	0
	.value	0
	.align 32
	.type	lext.4047, @object
	.size	lext.4047, 62
lext.4047:
	.value	16
	.value	16
	.value	16
	.value	16
	.value	16
	.value	16
	.value	16
	.value	16
	.value	17
	.value	17
	.value	17
	.value	17
	.value	18
	.value	18
	.value	18
	.value	18
	.value	19
	.value	19
	.value	19
	.value	19
	.value	20
	.value	20
	.value	20
	.value	20
	.value	21
	.value	21
	.value	21
	.value	21
	.value	16
	.value	77
	.value	202
	.align 32
	.type	lbase.4046, @object
	.size	lbase.4046, 62
lbase.4046:
	.value	3
	.value	4
	.value	5
	.value	6
	.value	7
	.value	8
	.value	9
	.value	10
	.value	11
	.value	13
	.value	15
	.value	17
	.value	19
	.value	23
	.value	27
	.value	31
	.value	35
	.value	43
	.value	51
	.value	59
	.value	67
	.value	83
	.value	99
	.value	115
	.value	131
	.value	163
	.value	195
	.value	227
	.value	258
	.value	0
	.value	0
	.globl	inflate_copyright
	.align 32
	.type	inflate_copyright, @object
	.size	inflate_copyright, 48
inflate_copyright:
	.string	" inflate 1.2.11 Copyright 1995-2017 Mark Adler "
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
