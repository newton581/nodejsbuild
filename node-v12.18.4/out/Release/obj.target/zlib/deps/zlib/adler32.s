	.file	"adler32.c"
	.text
	.p2align 4
	.globl	adler32_z
	.type	adler32_z, @function
adler32_z:
.LFB38:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	x86_cpu_enable_ssse3(%rip), %eax
	movq	%rdx, -96(%rbp)
	testl	%eax, %eax
	je	.L2
	testq	%rsi, %rsi
	je	.L2
	cmpq	$63, %rdx
	ja	.L129
.L2:
	movq	%r15, %rax
	movzwl	%r15w, %r15d
	shrq	$16, %rax
	cmpq	$1, -96(%rbp)
	movzwl	%ax, %eax
	movq	%rax, -80(%rbp)
	je	.L130
	testq	%r9, %r9
	je	.L131
	movq	-96(%rbp), %rax
	cmpq	$15, %rax
	jbe	.L8
	leaq	5552(%r9), %rbx
	movq	%rbx, -88(%rbp)
	cmpq	$5551, %rax
	jbe	.L10
.L15:
	movq	-88(%rbp), %rax
	leaq	-5552(%rax), %r9
	movq	-96(%rbp), %rax
	movq	%rax, -104(%rbp)
	subq	$5552, %rax
	movq	%rax, -96(%rbp)
	.p2align 4,,10
	.p2align 3
.L14:
	movzbl	(%r9), %eax
	movzbl	1(%r9), %r14d
	addq	$16, %r9
	movzbl	-14(%r9), %r13d
	movzbl	-13(%r9), %r12d
	addq	%r15, %rax
	movzbl	-12(%r9), %ebx
	movzbl	-11(%r9), %r11d
	addq	%rax, %r14
	movzbl	-10(%r9), %edx
	movzbl	-9(%r9), %r10d
	addq	%r14, %r13
	addq	%r14, %rax
	movzbl	-6(%r9), %r8d
	movzbl	-1(%r9), %r15d
	addq	%r13, %r12
	addq	%r13, %rax
	addq	%r12, %rbx
	addq	%r12, %rax
	addq	%rbx, %r11
	addq	%rbx, %rax
	leaq	(%rdx,%r11), %rdi
	addq	%r11, %rax
	movzbl	-2(%r9), %edx
	addq	%rdi, %r10
	movq	%rdi, -56(%rbp)
	movzbl	-5(%r9), %edi
	movq	%r10, %rcx
	movzbl	-8(%r9), %r10d
	addq	-56(%rbp), %rax
	movq	%rcx, -64(%rbp)
	addq	-64(%rbp), %rax
	addq	%rcx, %r10
	movzbl	-3(%r9), %ecx
	movq	%r10, %rsi
	movzbl	-7(%r9), %r10d
	movq	%rsi, -72(%rbp)
	addq	-72(%rbp), %rax
	addq	%rsi, %r10
	movzbl	-4(%r9), %esi
	addq	%r10, %r8
	addq	%rax, %r10
	addq	%r8, %rdi
	addq	%r10, %r8
	addq	%rdi, %rsi
	addq	%r8, %rdi
	addq	%rsi, %rcx
	addq	%rdi, %rsi
	addq	%rcx, %rdx
	addq	%rsi, %rcx
	addq	%rdx, %r15
	addq	%rcx, %rdx
	addq	%r15, %rdx
	addq	%rdx, -80(%rbp)
	cmpq	-88(%rbp), %r9
	jne	.L14
	movabsq	$4223091239536077, %rax
	movq	-80(%rbp), %rbx
	mulq	%r15
	movq	%r15, %rax
	subq	%rdx, %rax
	shrq	%rax
	addq	%rax, %rdx
	shrq	$15, %rdx
	imulq	$65521, %rdx, %rax
	subq	%rax, %r15
	movabsq	$4223091239536077, %rax
	mulq	%rbx
	movq	%rbx, %rax
	subq	%rdx, %rax
	shrq	%rax
	addq	%rax, %rdx
	shrq	$15, %rdx
	imulq	$65521, %rdx, %rax
	subq	%rax, %rbx
	leaq	5552(%r9), %rax
	cmpq	$5551, -96(%rbp)
	movq	%rbx, -80(%rbp)
	movq	%rax, -88(%rbp)
	ja	.L15
	movq	-96(%rbp), %rax
	testq	%rax, %rax
	jne	.L132
.L16:
	movq	-80(%rbp), %rax
	salq	$16, %rax
	orq	%rax, %r15
.L1:
	addq	$72, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L8:
	.cfi_restore_state
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L11
	movzbl	(%r9), %eax
	addq	%rax, %r15
	addq	%r15, -80(%rbp)
	movq	-80(%rbp), %rbx
	cmpq	$1, %rsi
	je	.L11
	movzbl	1(%r9), %eax
	addq	%rax, %r15
	addq	%r15, %rbx
	movq	%rbx, -80(%rbp)
	cmpq	$2, %rsi
	je	.L11
	movzbl	2(%r9), %eax
	addq	%rax, %r15
	addq	%r15, %rbx
	movq	%rbx, -80(%rbp)
	cmpq	$3, %rsi
	je	.L11
	movzbl	3(%r9), %eax
	addq	%rax, %r15
	addq	%r15, %rbx
	movq	%rbx, -80(%rbp)
	cmpq	$4, %rsi
	je	.L11
	movzbl	4(%r9), %eax
	addq	%rax, %r15
	addq	%r15, %rbx
	movq	%rbx, -80(%rbp)
	cmpq	$5, %rsi
	je	.L11
	movzbl	5(%r9), %eax
	addq	%rax, %r15
	addq	%r15, %rbx
	movq	%rbx, -80(%rbp)
	cmpq	$6, %rsi
	je	.L11
	movzbl	6(%r9), %eax
	addq	%rax, %r15
	addq	%r15, %rbx
	movq	%rbx, -80(%rbp)
	cmpq	$7, %rsi
	je	.L11
	movzbl	7(%r9), %eax
	addq	%rax, %r15
	addq	%r15, %rbx
	movq	%rbx, -80(%rbp)
	cmpq	$8, %rsi
	je	.L11
	movzbl	8(%r9), %eax
	addq	%rax, %r15
	addq	%r15, %rbx
	movq	%rbx, -80(%rbp)
	cmpq	$9, %rsi
	je	.L11
	movzbl	9(%r9), %eax
	addq	%rax, %r15
	addq	%r15, %rbx
	movq	%rbx, -80(%rbp)
	cmpq	$10, %rsi
	je	.L11
	movzbl	10(%r9), %eax
	addq	%rax, %r15
	addq	%r15, %rbx
	movq	%rbx, -80(%rbp)
	cmpq	$11, %rsi
	je	.L11
	movzbl	11(%r9), %eax
	addq	%rax, %r15
	addq	%r15, %rbx
	movq	%rbx, -80(%rbp)
	cmpq	$12, %rsi
	je	.L11
	movzbl	12(%r9), %eax
	addq	%rax, %r15
	addq	%r15, %rbx
	movq	%rbx, -80(%rbp)
	cmpq	$13, %rsi
	je	.L11
	movzbl	13(%r9), %eax
	addq	%rax, %r15
	addq	%r15, %rbx
	movq	%rbx, -80(%rbp)
	cmpq	$14, %rsi
	je	.L11
	movzbl	14(%r9), %eax
	addq	%rax, %r15
	addq	%r15, %rbx
	movq	%rbx, -80(%rbp)
.L11:
	cmpq	$65520, %r15
	leaq	-65521(%r15), %rax
	movq	-80(%rbp), %rbx
	movabsq	$4223091239536077, %rdx
	cmova	%rax, %r15
	movq	-80(%rbp), %rax
	mulq	%rdx
	movq	%rbx, %rax
	subq	%rdx, %rax
	shrq	%rax
	addq	%rdx, %rax
	shrq	$15, %rax
	imulq	$65521, %rax, %rax
	subq	%rax, %rbx
	movq	%rbx, %rax
	salq	$16, %rax
	orq	%rax, %r15
	jmp	.L1
.L132:
	cmpq	$15, %rax
	jbe	.L17
.L10:
	movq	-96(%rbp), %rax
	subq	$16, %rax
	shrq	$4, %rax
	movq	%rax, -104(%rbp)
	addq	$1, %rax
	salq	$4, %rax
	addq	%r9, %rax
	movq	%rax, -88(%rbp)
.L18:
	movzbl	(%r9), %eax
	movzbl	1(%r9), %r14d
	addq	$16, %r9
	movzbl	-14(%r9), %r13d
	movzbl	-13(%r9), %r12d
	addq	%r15, %rax
	movzbl	-12(%r9), %ebx
	movzbl	-11(%r9), %r11d
	addq	%rax, %r14
	movzbl	-10(%r9), %r10d
	movzbl	-9(%r9), %edx
	addq	%r14, %r13
	addq	%r14, %rax
	movzbl	-6(%r9), %r8d
	movzbl	-5(%r9), %edi
	addq	%r13, %r12
	addq	%r13, %rax
	movzbl	-1(%r9), %r15d
	addq	%r12, %rbx
	addq	%r12, %rax
	addq	%rbx, %r11
	addq	%rbx, %rax
	addq	%r11, %r10
	addq	%r11, %rax
	leaq	(%rdx,%r10), %rsi
	movzbl	-8(%r9), %edx
	addq	%r10, %rax
	movq	%rsi, -56(%rbp)
	addq	-56(%rbp), %rax
	addq	%rsi, %rdx
	movzbl	-4(%r9), %esi
	movq	%rdx, %rcx
	movzbl	-7(%r9), %edx
	movq	%rcx, -64(%rbp)
	addq	-64(%rbp), %rax
	addq	%rcx, %rdx
	movzbl	-3(%r9), %ecx
	addq	%rdx, %r8
	movq	%rdx, -72(%rbp)
	addq	-72(%rbp), %rax
	addq	%r8, %rdi
	addq	%r8, %rax
	movzbl	-2(%r9), %edx
	addq	%rdi, %rsi
	addq	%rdi, %rax
	addq	%rsi, %rcx
	addq	%rsi, %rax
	addq	%rcx, %rdx
	addq	%rcx, %rax
	addq	%rdx, %r15
	addq	%rdx, %rax
	addq	%r15, %rax
	addq	%rax, -80(%rbp)
	cmpq	-88(%rbp), %r9
	jne	.L18
	movq	-104(%rbp), %rax
	movq	-96(%rbp), %rbx
	negq	%rax
	salq	$4, %rax
	leaq	-17(%rbx,%rax), %rax
	andl	$15, %ebx
	je	.L19
.L21:
	movq	-88(%rbp), %rcx
	movzbl	(%rcx), %edx
	addq	%rdx, %r15
	addq	%r15, -80(%rbp)
	movq	-80(%rbp), %rbx
	testq	%rax, %rax
	je	.L19
	movzbl	1(%rcx), %edx
	addq	%rdx, %r15
	addq	%r15, %rbx
	movq	%rbx, -80(%rbp)
	cmpq	$1, %rax
	je	.L19
	movzbl	2(%rcx), %edx
	addq	%rdx, %r15
	addq	%r15, %rbx
	movq	%rbx, -80(%rbp)
	cmpq	$2, %rax
	je	.L19
	movzbl	3(%rcx), %edx
	addq	%rdx, %r15
	addq	%r15, %rbx
	movq	%rbx, -80(%rbp)
	cmpq	$3, %rax
	je	.L19
	movzbl	4(%rcx), %edx
	addq	%rdx, %r15
	addq	%r15, %rbx
	movq	%rbx, -80(%rbp)
	cmpq	$4, %rax
	je	.L19
	movzbl	5(%rcx), %edx
	addq	%rdx, %r15
	addq	%r15, %rbx
	movq	%rbx, -80(%rbp)
	cmpq	$5, %rax
	je	.L19
	movzbl	6(%rcx), %edx
	addq	%rdx, %r15
	addq	%r15, %rbx
	movq	%rbx, -80(%rbp)
	cmpq	$6, %rax
	je	.L19
	movzbl	7(%rcx), %edx
	addq	%rdx, %r15
	addq	%r15, %rbx
	movq	%rbx, -80(%rbp)
	cmpq	$7, %rax
	je	.L19
	movzbl	8(%rcx), %edx
	addq	%rdx, %r15
	addq	%r15, %rbx
	movq	%rbx, -80(%rbp)
	cmpq	$8, %rax
	je	.L19
	movzbl	9(%rcx), %edx
	addq	%rdx, %r15
	addq	%r15, %rbx
	movq	%rbx, -80(%rbp)
	cmpq	$9, %rax
	je	.L19
	movzbl	10(%rcx), %edx
	addq	%rdx, %r15
	addq	%r15, %rbx
	movq	%rbx, -80(%rbp)
	cmpq	$10, %rax
	je	.L19
	movzbl	11(%rcx), %edx
	addq	%rdx, %r15
	addq	%r15, %rbx
	movq	%rbx, -80(%rbp)
	cmpq	$11, %rax
	je	.L19
	movzbl	12(%rcx), %edx
	addq	%rdx, %r15
	addq	%r15, %rbx
	movq	%rbx, -80(%rbp)
	cmpq	$12, %rax
	je	.L19
	movzbl	13(%rcx), %edx
	addq	%rdx, %r15
	addq	%r15, %rbx
	movq	%rbx, -80(%rbp)
	cmpq	$13, %rax
	je	.L19
	movzbl	14(%rcx), %eax
	addq	%rax, %r15
	addq	%r15, %rbx
	movq	%rbx, -80(%rbp)
.L19:
	movq	%r15, %rax
	movq	-80(%rbp), %rbx
	movabsq	$4223091239536077, %rcx
	mulq	%rcx
	movq	%r15, %rax
	subq	%rdx, %rax
	shrq	%rax
	addq	%rdx, %rax
	shrq	$15, %rax
	imulq	$65521, %rax, %rax
	subq	%rax, %r15
	movq	-80(%rbp), %rax
	mulq	%rcx
	movq	%rbx, %rax
	subq	%rdx, %rax
	shrq	%rax
	addq	%rdx, %rax
	shrq	$15, %rax
	imulq	$65521, %rax, %rax
	subq	%rax, %rbx
	movq	%rbx, -80(%rbp)
	jmp	.L16
.L129:
	call	adler32_simd_
	movl	%eax, %r15d
	jmp	.L1
.L130:
	movzbl	(%r9), %edx
	addq	%r15, %rdx
	cmpq	$65520, %rdx
	leaq	-65521(%rdx), %rax
	cmova	%rax, %rdx
	movq	-80(%rbp), %rax
	addq	%rdx, %rax
	cmpq	$65520, %rax
	leaq	-65521(%rax), %rcx
	cmova	%rcx, %rax
	salq	$16, %rax
	movq	%rax, %r15
	orq	%rdx, %r15
	jmp	.L1
.L131:
	cmpq	$0, -96(%rbp)
	movl	$1, %r15d
	jne	.L1
	call	x86_check_features@PLT
	jmp	.L1
.L17:
	movq	-104(%rbp), %rax
	movq	%r9, -88(%rbp)
	subq	$5553, %rax
	jmp	.L21
	.cfi_endproc
.LFE38:
	.size	adler32_z, .-adler32_z
	.p2align 4
	.globl	adler32
	.type	adler32, @function
adler32:
.LFB39:
	.cfi_startproc
	endbr64
	movl	%edx, %edx
	jmp	adler32_z
	.cfi_endproc
.LFE39:
	.size	adler32, .-adler32
	.p2align 4
	.globl	adler32_combine
	.type	adler32_combine, @function
adler32_combine:
.LFB41:
	.cfi_startproc
	endbr64
	movq	%rdx, %r8
	movl	$4294967295, %eax
	testq	%rdx, %rdx
	js	.L134
	movabsq	$-9221260491235007769, %rdx
	movq	%r8, %rax
	movzwl	%di, %ecx
	shrq	$16, %rdi
	imulq	%rdx
	movq	%r8, %rax
	movq	%rcx, %r9
	sarq	$63, %rax
	addq	%r8, %rdx
	sarq	$15, %rdx
	subq	%rax, %rdx
	imulq	$65521, %rdx, %rdx
	subq	%rdx, %r8
	imulq	%r8, %r9
	movq	%r8, %rdx
	movzwl	%si, %r8d
	shrq	$16, %rsi
	movzwl	%si, %esi
	subq	%rdx, %rsi
	movzwl	%di, %edx
	addq	%rdx, %rsi
	movq	%r9, %rax
	movabsq	$4223091239536077, %rdx
	mulq	%rdx
	movq	%r9, %rax
	subq	%rdx, %rax
	shrq	%rax
	addq	%rax, %rdx
	shrq	$15, %rdx
	imulq	$65521, %rdx, %rdx
	subq	%rdx, %r9
	leaq	(%rsi,%r9), %rdx
	movl	$65520, %esi
	leaq	65521(%rdx), %rax
	addq	%r8, %rcx
	je	.L136
	leaq	-1(%rcx), %rsi
	subq	$65522, %rcx
	cmpq	$65520, %rsi
	cmova	%rcx, %rsi
.L136:
	subq	$65521, %rdx
	cmpq	$131041, %rax
	cmova	%rdx, %rax
	cmpq	$65520, %rax
	leaq	-65521(%rax), %rdx
	cmova	%rdx, %rax
	salq	$16, %rax
	orq	%rsi, %rax
.L134:
	ret
	.cfi_endproc
.LFE41:
	.size	adler32_combine, .-adler32_combine
	.p2align 4
	.globl	adler32_combine64
	.type	adler32_combine64, @function
adler32_combine64:
.LFB44:
	.cfi_startproc
	endbr64
	movq	%rdx, %r8
	movl	$4294967295, %eax
	testq	%rdx, %rdx
	js	.L142
	movabsq	$-9221260491235007769, %rdx
	movq	%r8, %rax
	movzwl	%di, %ecx
	shrq	$16, %rdi
	imulq	%rdx
	movq	%r8, %rax
	movq	%rcx, %r9
	sarq	$63, %rax
	addq	%r8, %rdx
	sarq	$15, %rdx
	subq	%rax, %rdx
	imulq	$65521, %rdx, %rdx
	subq	%rdx, %r8
	imulq	%r8, %r9
	movq	%r8, %rdx
	movzwl	%si, %r8d
	shrq	$16, %rsi
	movzwl	%si, %esi
	subq	%rdx, %rsi
	movzwl	%di, %edx
	addq	%rdx, %rsi
	movq	%r9, %rax
	movabsq	$4223091239536077, %rdx
	mulq	%rdx
	movq	%r9, %rax
	subq	%rdx, %rax
	shrq	%rax
	addq	%rax, %rdx
	shrq	$15, %rdx
	imulq	$65521, %rdx, %rdx
	subq	%rdx, %r9
	leaq	(%rsi,%r9), %rdx
	movl	$65520, %esi
	leaq	65521(%rdx), %rax
	addq	%r8, %rcx
	je	.L144
	leaq	-1(%rcx), %rsi
	subq	$65522, %rcx
	cmpq	$65520, %rsi
	cmova	%rcx, %rsi
.L144:
	subq	$65521, %rdx
	cmpq	$131041, %rax
	cmova	%rdx, %rax
	cmpq	$65520, %rax
	leaq	-65521(%rax), %rdx
	cmova	%rdx, %rax
	salq	$16, %rax
	orq	%rsi, %rax
.L142:
	ret
	.cfi_endproc
.LFE44:
	.size	adler32_combine64, .-adler32_combine64
	.hidden	adler32_simd_
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
