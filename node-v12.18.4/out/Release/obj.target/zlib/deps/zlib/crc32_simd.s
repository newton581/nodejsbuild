	.file	"crc32_simd.c"
	.text
	.p2align 4
	.globl	crc32_sse42_simd_
	.hidden	crc32_sse42_simd_
	.type	crc32_sse42_simd_, @function
crc32_sse42_simd_:
.LFB678:
	.cfi_startproc
	endbr64
	movdqu	(%rdi), %xmm5
	movd	%edx, %xmm3
	leaq	-64(%rsi), %rax
	movdqu	16(%rdi), %xmm6
	movdqu	32(%rdi), %xmm2
	movdqu	48(%rdi), %xmm1
	leaq	64(%rdi), %rdx
	movdqa	k1k2.6869(%rip), %xmm0
	pxor	%xmm5, %xmm3
	cmpq	$63, %rax
	jbe	.L4
	addq	$-128, %rsi
	movq	%rsi, %rcx
	shrq	$6, %rcx
	leaq	2(%rcx), %rax
	salq	$6, %rax
	addq	%rax, %rdi
	movq	%rdx, %rax
	.p2align 4,,10
	.p2align 3
.L3:
	movdqa	%xmm6, %xmm7
	movdqa	%xmm2, %xmm5
	movdqa	%xmm1, %xmm4
	addq	$64, %rax
	movdqa	%xmm3, %xmm8
	pclmulqdq	$0, %xmm0, %xmm7
	pclmulqdq	$0, %xmm0, %xmm5
	movdqu	-64(%rax), %xmm9
	pclmulqdq	$0, %xmm0, %xmm4
	pclmulqdq	$17, %xmm0, %xmm6
	pclmulqdq	$17, %xmm0, %xmm2
	pclmulqdq	$17, %xmm0, %xmm1
	pxor	%xmm7, %xmm6
	pxor	%xmm5, %xmm2
	movdqu	-48(%rax), %xmm7
	pxor	%xmm4, %xmm1
	movdqu	-32(%rax), %xmm5
	movdqu	-16(%rax), %xmm4
	pclmulqdq	$0, %xmm0, %xmm8
	pclmulqdq	$17, %xmm0, %xmm3
	pxor	%xmm7, %xmm6
	pxor	%xmm8, %xmm3
	pxor	%xmm5, %xmm2
	pxor	%xmm4, %xmm1
	pxor	%xmm9, %xmm3
	cmpq	%rdi, %rax
	jne	.L3
	leaq	1(%rcx), %rax
	salq	$6, %rcx
	salq	$6, %rax
	addq	%rax, %rdx
	movq	%rsi, %rax
	subq	%rcx, %rax
.L4:
	movdqa	k3k4.6870(%rip), %xmm4
	movdqa	%xmm3, %xmm0
	pclmulqdq	$0, %xmm4, %xmm0
	pclmulqdq	$17, %xmm4, %xmm3
	pxor	%xmm0, %xmm3
	pxor	%xmm3, %xmm6
	movdqa	%xmm6, %xmm0
	pclmulqdq	$17, %xmm4, %xmm6
	pclmulqdq	$0, %xmm4, %xmm0
	movdqa	%xmm0, %xmm3
	pxor	%xmm6, %xmm3
	pxor	%xmm3, %xmm2
	movdqa	%xmm2, %xmm0
	pclmulqdq	$17, %xmm4, %xmm2
	pclmulqdq	$0, %xmm4, %xmm0
	pxor	%xmm0, %xmm2
	pxor	%xmm2, %xmm1
	cmpq	$15, %rax
	jbe	.L5
	movdqa	%xmm1, %xmm0
	movdqu	(%rdx), %xmm7
	pclmulqdq	$17, %xmm4, %xmm1
	leaq	-16(%rax), %rcx
	pclmulqdq	$0, %xmm4, %xmm0
	pxor	%xmm0, %xmm1
	pxor	%xmm7, %xmm1
	cmpq	$15, %rcx
	jbe	.L5
	movdqa	%xmm1, %xmm0
	movdqu	16(%rdx), %xmm7
	pclmulqdq	$17, %xmm4, %xmm1
	subq	$32, %rax
	pclmulqdq	$0, %xmm4, %xmm0
	pxor	%xmm0, %xmm1
	pxor	%xmm7, %xmm1
	cmpq	$15, %rax
	jbe	.L5
	movdqa	%xmm1, %xmm0
	movdqu	32(%rdx), %xmm2
	pclmulqdq	$17, %xmm4, %xmm1
	pclmulqdq	$0, %xmm4, %xmm0
	pxor	%xmm0, %xmm1
	pxor	%xmm2, %xmm1
.L5:
	movdqa	%xmm1, %xmm0
	movdqa	.LC0(%rip), %xmm2
	psrldq	$8, %xmm1
	pclmulqdq	$16, %xmm4, %xmm0
	pxor	%xmm0, %xmm1
	movdqa	%xmm1, %xmm0
	pand	%xmm2, %xmm1
	pclmulqdq	$0, .LC1(%rip), %xmm1
	psrldq	$4, %xmm0
	pxor	%xmm0, %xmm1
	movdqa	%xmm1, %xmm0
	pand	%xmm2, %xmm0
	pclmulqdq	$16, poly.6872(%rip), %xmm0
	pand	%xmm2, %xmm0
	pclmulqdq	$0, poly.6872(%rip), %xmm0
	pxor	%xmm1, %xmm0
	pextrd	$1, %xmm0, %eax
	ret
	.cfi_endproc
.LFE678:
	.size	crc32_sse42_simd_, .-crc32_sse42_simd_
	.section	.rodata
	.align 16
	.type	poly.6872, @object
	.size	poly.6872, 16
poly.6872:
	.quad	7976584769
	.quad	8439010881
	.align 16
	.type	k3k4.6870, @object
	.size	k3k4.6870, 16
k3k4.6870:
	.quad	6259578832
	.quad	3433693342
	.align 16
	.type	k1k2.6869, @object
	.size	k1k2.6869, 16
k1k2.6869:
	.quad	5708721108
	.quad	7631803798
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.quad	4294967295
	.quad	4294967295
	.align 16
.LC1:
	.quad	5969371428
	.quad	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
