	.file	"adler32_simd.c"
	.text
	.p2align 4
	.globl	adler32_simd_
	.hidden	adler32_simd_
	.type	adler32_simd_, @function
adler32_simd_:
.LFB600:
	.cfi_startproc
	endbr64
	movq	%rdx, %r10
	movq	%rdx, %r8
	movzwl	%di, %eax
	shrl	$16, %edi
	andl	$31, %r10d
	shrq	$5, %r8
	je	.L2
	movdqa	.LC0(%rip), %xmm9
	movdqa	.LC1(%rip), %xmm2
	pxor	%xmm7, %xmm7
	movl	$2147975281, %r9d
	movdqa	.LC2(%rip), %xmm8
	.p2align 4,,10
	.p2align 3
.L6:
	cmpq	$172, %r8
	ja	.L3
	movl	%r8d, %r11d
	xorl	%r8d, %r8d
.L4:
	movl	%eax, %ecx
	movd	%edi, %xmm3
	pxor	%xmm4, %xmm4
	movl	%r11d, %edi
	imull	%r11d, %ecx
	salq	$5, %rdi
	addq	%rsi, %rdi
	movd	%ecx, %xmm6
	movq	%rsi, %rcx
	.p2align 4,,10
	.p2align 3
.L5:
	movdqu	(%rcx), %xmm0
	movdqa	%xmm6, %xmm5
	addq	$32, %rcx
	paddd	%xmm4, %xmm5
	movdqa	%xmm0, %xmm11
	pmaddubsw	%xmm9, %xmm0
	movdqa	%xmm5, %xmm6
	movdqa	%xmm0, %xmm1
	movdqu	-16(%rcx), %xmm0
	psadbw	%xmm7, %xmm11
	pmaddwd	%xmm2, %xmm1
	movdqa	%xmm0, %xmm10
	pmaddubsw	%xmm8, %xmm0
	psadbw	%xmm7, %xmm10
	pmaddwd	%xmm2, %xmm0
	paddd	%xmm11, %xmm10
	paddd	%xmm1, %xmm0
	paddd	%xmm4, %xmm10
	paddd	%xmm3, %xmm0
	movdqa	%xmm10, %xmm4
	movdqa	%xmm0, %xmm3
	cmpq	%rdi, %rcx
	jne	.L5
	pslld	$5, %xmm5
	pshufd	$177, %xmm10, %xmm1
	leal	-1(%r11), %ecx
	paddd	%xmm5, %xmm0
	paddd	%xmm10, %xmm1
	addq	$1, %rcx
	pshufd	$177, %xmm0, %xmm5
	pshufd	$78, %xmm1, %xmm10
	salq	$5, %rcx
	paddd	%xmm5, %xmm0
	paddd	%xmm1, %xmm10
	addq	%rcx, %rsi
	pshufd	$78, %xmm0, %xmm5
	movd	%xmm10, %ecx
	paddd	%xmm5, %xmm0
	addl	%ecx, %eax
	movd	%xmm0, %edi
	movq	%rax, %rcx
	movq	%rdi, %r11
	imulq	%r9, %rax
	imulq	%r9, %rdi
	shrq	$47, %rax
	shrq	$47, %rdi
	imull	$65521, %eax, %eax
	imull	$65521, %edi, %edi
	subl	%eax, %ecx
	subl	%edi, %r11d
	movl	%ecx, %eax
	movl	%r11d, %edi
	testq	%r8, %r8
	jne	.L6
.L2:
	testq	%r10, %r10
	je	.L7
	andl	$16, %edx
	jne	.L27
	leaq	-1(%r10), %rcx
	movq	%rsi, %rdx
.L12:
	leaq	1(%rdx,%rcx), %rsi
	.p2align 4,,10
	.p2align 3
.L10:
	movzbl	(%rdx), %ecx
	addq	$1, %rdx
	addl	%ecx, %eax
	addl	%eax, %edi
	cmpq	%rsi, %rdx
	jne	.L10
.L9:
	cmpl	$65520, %eax
	leal	-65521(%rax), %edx
	movl	$2147975281, %ecx
	cmova	%edx, %eax
	movl	%edi, %edx
	imulq	%rcx, %rdx
	shrq	$47, %rdx
	imull	$65521, %edx, %edx
	subl	%edx, %edi
.L7:
	sall	$16, %edi
	orl	%edi, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	subq	$173, %r8
	movl	$173, %r11d
	jmp	.L4
.L27:
	movzbl	(%rsi), %edx
	movzbl	15(%rsi), %ecx
	addl	%edx, %eax
	movzbl	1(%rsi), %edx
	addl	%eax, %edi
	addl	%eax, %edx
	movzbl	2(%rsi), %eax
	addl	%edx, %edi
	addl	%edx, %eax
	movzbl	3(%rsi), %edx
	addl	%eax, %edi
	addl	%eax, %edx
	movzbl	4(%rsi), %eax
	addl	%edx, %edi
	addl	%eax, %edx
	movzbl	5(%rsi), %eax
	addl	%edx, %edi
	addl	%edx, %eax
	movzbl	6(%rsi), %edx
	addl	%eax, %edi
	addl	%eax, %edx
	movzbl	7(%rsi), %eax
	addl	%edx, %edi
	addl	%eax, %edx
	movzbl	8(%rsi), %eax
	addl	%edx, %edi
	addl	%edx, %eax
	movzbl	9(%rsi), %edx
	addl	%eax, %edi
	addl	%eax, %edx
	movzbl	10(%rsi), %eax
	addl	%edx, %edi
	addl	%eax, %edx
	movzbl	11(%rsi), %eax
	addl	%edx, %edi
	addl	%edx, %eax
	movzbl	12(%rsi), %edx
	addl	%eax, %edi
	addl	%eax, %edx
	movzbl	13(%rsi), %eax
	addl	%edx, %edi
	addl	%eax, %edx
	movzbl	14(%rsi), %eax
	addl	%edx, %edi
	addl	%edx, %eax
	leaq	16(%rsi), %rdx
	addl	%eax, %edi
	addl	%ecx, %eax
	leaq	-17(%r10), %rcx
	addl	%eax, %edi
	cmpq	$16, %r10
	jne	.L12
	jmp	.L9
	.cfi_endproc
.LFE600:
	.size	adler32_simd_, .-adler32_simd_
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.byte	32
	.byte	31
	.byte	30
	.byte	29
	.byte	28
	.byte	27
	.byte	26
	.byte	25
	.byte	24
	.byte	23
	.byte	22
	.byte	21
	.byte	20
	.byte	19
	.byte	18
	.byte	17
	.align 16
.LC1:
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.align 16
.LC2:
	.byte	16
	.byte	15
	.byte	14
	.byte	13
	.byte	12
	.byte	11
	.byte	10
	.byte	9
	.byte	8
	.byte	7
	.byte	6
	.byte	5
	.byte	4
	.byte	3
	.byte	2
	.byte	1
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
