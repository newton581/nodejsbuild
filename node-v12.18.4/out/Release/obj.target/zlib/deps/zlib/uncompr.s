	.file	"uncompr.c"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"1.2.11"
	.text
	.p2align 4
	.globl	uncompress2
	.type	uncompress2, @function
uncompress2:
.LFB17:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rsi), %rbx
	movq	(%rax), %r12
	movq	%rdi, -184(%rbp)
	movq	%rsi, -200(%rbp)
	movq	%rcx, -192(%rbp)
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	testq	%rbx, %rbx
	je	.L16
	movq	$0, (%rsi)
.L2:
	leaq	-176(%rbp), %r15
	movq	%rdx, -176(%rbp)
	pxor	%xmm0, %xmm0
	movl	$112, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r15, %rdi
	movaps	%xmm0, -112(%rbp)
	movl	$0, -168(%rbp)
	movq	$0, -96(%rbp)
	call	inflateInit_@PLT
	movl	%eax, %r14d
	testl	%eax, %eax
	jne	.L1
	movq	-184(%rbp), %rax
	movl	$4294967295, %r13d
	movl	$0, -144(%rbp)
	movq	%rax, -152(%rbp)
	xorl	%eax, %eax
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L29:
	movl	-144(%rbp), %eax
.L8:
	testl	%eax, %eax
	jne	.L4
	cmpq	%r13, %rbx
	movq	%r13, %rax
	cmovbe	%rbx, %rax
	movl	%eax, -144(%rbp)
	subq	%rax, %rbx
.L4:
	movl	-168(%rbp), %eax
	testl	%eax, %eax
	jne	.L5
	cmpq	%r13, %r12
	movq	%r13, %rax
	cmovbe	%r12, %rax
	movl	%eax, -168(%rbp)
	subq	%rax, %r12
.L5:
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	inflate@PLT
	movl	%eax, %edx
	testl	%eax, %eax
	je	.L29
	movq	-192(%rbp), %rcx
	movl	-168(%rbp), %eax
	movq	(%rcx), %rdi
	movq	%rdi, -192(%rbp)
	subq	%rax, %rdi
	movq	%rdi, %rax
	subq	%r12, %rax
	movq	%rax, (%rcx)
	movq	-136(%rbp), %rax
	leaq	-57(%rbp), %rcx
	cmpq	%rcx, -184(%rbp)
	je	.L9
	movq	-200(%rbp), %rcx
	movq	%rax, (%rcx)
.L10:
	movq	%r15, %rdi
	movl	%edx, -184(%rbp)
	call	inflateEnd@PLT
	movl	-184(%rbp), %edx
	cmpl	$1, %edx
	je	.L1
	cmpl	$2, %edx
	je	.L14
	movl	%edx, %r14d
	cmpl	$-5, %edx
	je	.L30
.L1:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L31
	addq	$168, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	.cfi_restore_state
	leaq	-57(%rbp), %rax
	movl	$1, %ebx
	movq	%rax, -184(%rbp)
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L9:
	testq	%rax, %rax
	je	.L10
	cmpl	$-5, %edx
	jne	.L10
	movq	%r15, %rdi
	call	inflateEnd@PLT
.L14:
	movl	$-3, %r14d
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L30:
	movl	-144(%rbp), %eax
	addq	%rbx, %rax
	je	.L1
	jmp	.L14
.L31:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17:
	.size	uncompress2, .-uncompress2
	.p2align 4
	.globl	uncompress
	.type	uncompress, @function
uncompress:
.LFB18:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rcx, -8(%rbp)
	leaq	-8(%rbp), %rcx
	call	uncompress2
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18:
	.size	uncompress, .-uncompress
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
