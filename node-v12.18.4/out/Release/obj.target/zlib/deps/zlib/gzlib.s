	.file	"gzlib.c"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"%s"
	.text
	.p2align 4
	.type	gz_open, @function
gz_open:
.LFB64:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%rdi, %rdi
	je	.L58
	movq	%rdi, %r15
	movl	$232, %edi
	movq	%rdx, -56(%rbp)
	movl	%esi, %r12d
	call	malloc@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L58
	movq	-56(%rbp), %rdx
	xorl	%ebx, %ebx
	xorl	%r14d, %r14d
	leaq	.L9(%rip), %rcx
	movabsq	$35184372088832, %rax
	movq	$0, 112(%r13)
	movq	%rax, 40(%r13)
	movl	$4294967295, %eax
	movq	%rax, 88(%r13)
	movsbl	(%rdx), %eax
	movl	$0, 24(%r13)
	movl	$0, 64(%r13)
	testb	%al, %al
	jne	.L5
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L61:
	subl	$48, %eax
	movl	%eax, 88(%r13)
.L7:
	movsbl	1(%rdx), %eax
	addq	$1, %rdx
	testb	%al, %al
	je	.L60
.L5:
	leal	-48(%rax), %esi
	cmpb	$9, %sil
	jbe	.L61
	subl	$43, %eax
	cmpb	$77, %al
	ja	.L7
	movzbl	%al, %eax
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L9:
	.long	.L57-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L18-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L17-.L9
	.long	.L7-.L9
	.long	.L16-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L15-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L14-.L9
	.long	.L13-.L9
	.long	.L7-.L9
	.long	.L12-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L11-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L10-.L9
	.long	.L8-.L9
	.text
.L14:
	movsbl	1(%rdx), %eax
	addq	$1, %rdx
	movl	$1, %r14d
	testb	%al, %al
	jne	.L5
.L60:
	movl	24(%r13), %eax
	testl	%eax, %eax
	je	.L57
	cmpl	$7247, %eax
	jne	.L21
	movl	64(%r13), %eax
	testl	%eax, %eax
	jne	.L57
	movl	$1, 64(%r13)
.L21:
	movq	%r15, %rdi
	call	strlen@PLT
	leaq	1(%rax), %rsi
	movq	%rsi, %rdi
	movq	%rsi, -56(%rbp)
	call	malloc@PLT
	movq	-56(%rbp), %rsi
	testq	%rax, %rax
	movq	%rax, 32(%r13)
	movq	%rax, %rdi
	je	.L57
	leaq	.LC0(%rip), %rdx
	xorl	%eax, %eax
	movq	%r15, %rcx
	call	snprintf@PLT
	movl	24(%r13), %edx
	testl	%r14d, %r14d
	movl	$524288, %eax
	cmovne	%eax, %r14d
	cmpl	$7247, %edx
	je	.L25
	cmpl	$1, %ebx
	sbbl	%eax, %eax
	andl	$-128, %eax
	addl	$1217, %eax
	cmpl	$1, %ebx
	sbbl	%ecx, %ecx
	andl	$-128, %ecx
	addl	$705, %ecx
	cmpl	$31153, %edx
	cmove	%ecx, %eax
	orl	%eax, %r14d
.L25:
	testl	%r12d, %r12d
	js	.L62
	movl	%r12d, 28(%r13)
.L37:
	cmpl	$1, %edx
	je	.L63
	cmpl	$7247, %edx
	je	.L64
	movl	$0, 0(%r13)
.L31:
	movq	112(%r13), %rdi
	movl	$0, 104(%r13)
	testq	%rdi, %rdi
	je	.L35
	cmpl	$-4, 108(%r13)
	jne	.L65
.L36:
	movq	$0, 112(%r13)
.L35:
	movl	$0, 108(%r13)
	movq	$0, 16(%r13)
	movl	$0, 128(%r13)
	jmp	.L1
.L66:
	movq	32(%r13), %rdi
	call	free@PLT
.L57:
	movq	%r13, %rdi
	call	free@PLT
.L58:
	xorl	%r13d, %r13d
.L1:
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L8:
	.cfi_restore_state
	movl	$1, %ebx
	jmp	.L7
.L10:
	movl	$31153, 24(%r13)
	jmp	.L7
.L11:
	movl	$7247, 24(%r13)
	jmp	.L7
.L12:
	movl	$2, 92(%r13)
	jmp	.L7
.L13:
	movl	$1, 92(%r13)
	jmp	.L7
.L15:
	movl	$1, 24(%r13)
	jmp	.L7
.L16:
	movl	$1, 64(%r13)
	jmp	.L7
.L17:
	movl	$3, 92(%r13)
	jmp	.L7
.L18:
	movl	$4, 92(%r13)
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L65:
	call	free@PLT
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L62:
	movl	$438, %edx
	movl	%r14d, %esi
	movq	%r15, %rdi
	xorl	%eax, %eax
	call	open@PLT
	movl	%eax, 28(%r13)
	movl	%eax, %r12d
	cmpl	$-1, %eax
	je	.L66
	movl	24(%r13), %edx
	jmp	.L37
.L64:
	movl	$1, %edx
	xorl	%esi, %esi
	movl	%r12d, %edi
	call	lseek@PLT
	movq	%rax, 72(%r13)
	cmpq	$-1, %rax
	jne	.L56
	movq	$0, 72(%r13)
.L56:
	movl	24(%r13), %eax
	movl	$0, 0(%r13)
	cmpl	$7247, %eax
	jne	.L31
	movq	$0, 80(%r13)
	movl	$0, 68(%r13)
	jmp	.L31
.L63:
	movl	$2, %edx
	xorl	%esi, %esi
	movl	%r12d, %edi
	call	lseek@PLT
	movl	$31153, 24(%r13)
	movl	$0, 0(%r13)
	jmp	.L31
	.cfi_endproc
.LFE64:
	.size	gz_open, .-gz_open
	.p2align 4
	.globl	gzopen
	.type	gzopen, @function
gzopen:
.LFB65:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdx
	movl	$-1, %esi
	jmp	gz_open
	.cfi_endproc
.LFE65:
	.size	gzopen, .-gzopen
	.p2align 4
	.globl	gzopen64
	.type	gzopen64, @function
gzopen64:
.LFB82:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdx
	movl	$-1, %esi
	jmp	gz_open
	.cfi_endproc
.LFE82:
	.size	gzopen64, .-gzopen64
	.section	.rodata.str1.1
.LC1:
	.string	"<fd:%d>"
	.text
	.p2align 4
	.globl	gzdopen
	.type	gzdopen, @function
gzdopen:
.LFB67:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	cmpl	$-1, %edi
	je	.L72
	movl	%edi, %r13d
	movl	$19, %edi
	movq	%rsi, %r14
	call	malloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L72
	movl	%r13d, %r9d
	movl	$19, %ecx
	movq	%rax, %rdi
	movl	$1, %edx
	leaq	.LC1(%rip), %r8
	movl	$19, %esi
	xorl	%eax, %eax
	call	__snprintf_chk@PLT
	movq	%r14, %rdx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	gz_open
	movq	%r12, %rdi
	movq	%rax, %r13
	call	free@PLT
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L72:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%r13d, %r13d
	popq	%r12
	movq	%r13, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE67:
	.size	gzdopen, .-gzdopen
	.p2align 4
	.globl	gzbuffer
	.type	gzbuffer, @function
gzbuffer:
.LFB68:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L83
	movl	24(%rdi), %eax
	cmpl	$7247, %eax
	je	.L84
	cmpl	$31153, %eax
	jne	.L83
.L84:
	movl	40(%rdi), %eax
	testl	%eax, %eax
	jne	.L83
	leal	(%rsi,%rsi), %eax
	cmpl	%esi, %eax
	jb	.L83
	cmpl	$2, %esi
	movl	$2, %eax
	cmovb	%eax, %esi
	xorl	%eax, %eax
	movl	%esi, 44(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L83:
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE68:
	.size	gzbuffer, .-gzbuffer
	.p2align 4
	.globl	gzrewind
	.type	gzrewind, @function
gzrewind:
.LFB69:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L109
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpl	$7247, 24(%rdi)
	jne	.L91
	movl	108(%rdi), %eax
	testl	%eax, %eax
	je	.L96
	cmpl	$-5, %eax
	jne	.L91
.L96:
	movq	72(%rbx), %rsi
	movl	28(%rbx), %edi
	xorl	%edx, %edx
	call	lseek@PLT
	cmpq	$-1, %rax
	je	.L91
	cmpl	$7247, 24(%rbx)
	movl	$0, (%rbx)
	jne	.L93
	movq	$0, 80(%rbx)
	movl	$0, 68(%rbx)
.L93:
	movq	112(%rbx), %rdi
	movl	$0, 104(%rbx)
	testq	%rdi, %rdi
	je	.L94
	cmpl	$-4, 108(%rbx)
	jne	.L110
.L95:
	movq	$0, 112(%rbx)
.L94:
	movl	$0, 108(%rbx)
	xorl	%eax, %eax
	movq	$0, 16(%rbx)
	movl	$0, 128(%rbx)
.L88:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L110:
	.cfi_restore_state
	call	free@PLT
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L91:
	movl	$-1, %eax
	jmp	.L88
.L109:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	orl	$-1, %eax
	ret
	.cfi_endproc
.LFE69:
	.size	gzrewind, .-gzrewind
	.p2align 4
	.globl	gzseek64
	.type	gzseek64, @function
gzseek64:
.LFB70:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L153
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	24(%rdi), %eax
	movq	%rdi, %rbx
	cmpl	$7247, %eax
	je	.L130
	cmpl	$31153, %eax
	jne	.L114
.L130:
	movl	108(%rbx), %ecx
	testl	%ecx, %ecx
	je	.L131
	cmpl	$-5, %ecx
	jne	.L114
.L131:
	cmpl	$1, %edx
	ja	.L114
	testl	%edx, %edx
	je	.L154
	movl	104(%rbx), %edx
	testl	%edx, %edx
	je	.L118
	addq	96(%rbx), %r12
.L118:
	movl	$0, 104(%rbx)
	cmpl	$7247, %eax
	je	.L155
.L119:
	testq	%r12, %r12
	js	.L114
	movq	16(%rbx), %rax
.L124:
	testq	%r12, %r12
	je	.L126
	movl	$1, 104(%rbx)
	movq	%r12, 96(%rbx)
.L126:
	addq	%r12, %rax
.L111:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L154:
	.cfi_restore_state
	movl	$0, 104(%rbx)
	subq	16(%rbx), %r12
	cmpl	$7247, %eax
	jne	.L119
.L155:
	cmpl	$1, 68(%rbx)
	movq	16(%rbx), %rax
	je	.L156
	testq	%r12, %r12
	jns	.L128
	addq	%rax, %r12
	js	.L114
	movq	%rbx, %rdi
	call	gzrewind
	cmpl	$-1, %eax
	je	.L114
	cmpl	$7247, 24(%rbx)
	movq	16(%rbx), %rax
	jne	.L124
.L128:
	movl	(%rbx), %edx
	xorl	%esi, %esi
	movq	%rdx, %rcx
	cmpq	%r12, %rdx
	jle	.L125
	movl	%edx, %esi
	movq	%r12, %rcx
	movl	%r12d, %edx
	subl	%r12d, %esi
.L125:
	addq	%rcx, %rax
	addq	%rdx, 8(%rbx)
	subq	%rcx, %r12
	movl	%esi, (%rbx)
	movq	%rax, 16(%rbx)
	jmp	.L124
	.p2align 4,,10
	.p2align 3
.L156:
	movq	%r12, %rcx
	addq	%rax, %rcx
	jns	.L157
	testq	%r12, %r12
	jns	.L128
	.p2align 4,,10
	.p2align 3
.L114:
	movq	$-1, %rax
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L157:
	movl	(%rbx), %eax
	movq	%r12, %rsi
	movl	28(%rbx), %edi
	movl	$1, %edx
	subq	%rax, %rsi
	call	lseek@PLT
	cmpq	$-1, %rax
	je	.L114
	movq	112(%rbx), %rdi
	movl	$0, (%rbx)
	movq	$0, 80(%rbx)
	movl	$0, 104(%rbx)
	testq	%rdi, %rdi
	je	.L122
	cmpl	$-4, 108(%rbx)
	je	.L123
	call	free@PLT
.L123:
	movq	$0, 112(%rbx)
.L122:
	movq	16(%rbx), %rax
	movl	$0, 108(%rbx)
	movl	$0, 128(%rbx)
	addq	%r12, %rax
	movq	%rax, 16(%rbx)
	jmp	.L111
.L153:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	orq	$-1, %rax
	ret
	.cfi_endproc
.LFE70:
	.size	gzseek64, .-gzseek64
	.p2align 4
	.globl	gzseek
	.type	gzseek, @function
gzseek:
.LFB71:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L200
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	24(%rdi), %eax
	movq	%rdi, %rbx
	cmpl	$7247, %eax
	je	.L177
	cmpl	$31153, %eax
	jne	.L161
.L177:
	movl	108(%rbx), %ecx
	testl	%ecx, %ecx
	je	.L178
	cmpl	$-5, %ecx
	jne	.L161
.L178:
	cmpl	$1, %edx
	ja	.L161
	testl	%edx, %edx
	je	.L201
	movl	104(%rbx), %edx
	testl	%edx, %edx
	je	.L165
	addq	96(%rbx), %r12
.L165:
	movl	$0, 104(%rbx)
	cmpl	$7247, %eax
	je	.L202
.L166:
	testq	%r12, %r12
	js	.L161
	movq	16(%rbx), %rax
.L171:
	testq	%r12, %r12
	je	.L173
	movl	$1, 104(%rbx)
	movq	%r12, 96(%rbx)
.L173:
	addq	%r12, %rax
.L158:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L201:
	.cfi_restore_state
	movl	$0, 104(%rbx)
	subq	16(%rbx), %r12
	cmpl	$7247, %eax
	jne	.L166
.L202:
	cmpl	$1, 68(%rbx)
	movq	16(%rbx), %rax
	je	.L203
	testq	%r12, %r12
	jns	.L175
	addq	%rax, %r12
	js	.L161
	movq	%rbx, %rdi
	call	gzrewind
	cmpl	$-1, %eax
	je	.L161
	cmpl	$7247, 24(%rbx)
	movq	16(%rbx), %rax
	jne	.L171
.L175:
	movl	(%rbx), %edx
	xorl	%esi, %esi
	movq	%rdx, %rcx
	cmpq	%r12, %rdx
	jle	.L172
	movl	%edx, %esi
	movq	%r12, %rcx
	movl	%r12d, %edx
	subl	%r12d, %esi
.L172:
	addq	%rcx, %rax
	addq	%rdx, 8(%rbx)
	subq	%rcx, %r12
	movl	%esi, (%rbx)
	movq	%rax, 16(%rbx)
	jmp	.L171
	.p2align 4,,10
	.p2align 3
.L203:
	movq	%r12, %rcx
	addq	%rax, %rcx
	jns	.L204
	testq	%r12, %r12
	jns	.L175
	.p2align 4,,10
	.p2align 3
.L161:
	movq	$-1, %rax
	jmp	.L158
	.p2align 4,,10
	.p2align 3
.L204:
	movl	(%rbx), %eax
	movq	%r12, %rsi
	movl	28(%rbx), %edi
	movl	$1, %edx
	subq	%rax, %rsi
	call	lseek@PLT
	cmpq	$-1, %rax
	je	.L161
	movq	112(%rbx), %rdi
	movl	$0, (%rbx)
	movq	$0, 80(%rbx)
	movl	$0, 104(%rbx)
	testq	%rdi, %rdi
	je	.L169
	cmpl	$-4, 108(%rbx)
	je	.L170
	call	free@PLT
.L170:
	movq	$0, 112(%rbx)
.L169:
	movq	16(%rbx), %rax
	movl	$0, 108(%rbx)
	movl	$0, 128(%rbx)
	addq	%r12, %rax
	movq	%rax, 16(%rbx)
	jmp	.L158
.L200:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	orq	$-1, %rax
	ret
	.cfi_endproc
.LFE71:
	.size	gzseek, .-gzseek
	.p2align 4
	.globl	gztell64
	.type	gztell64, @function
gztell64:
.LFB72:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L210
	movl	24(%rdi), %eax
	cmpl	$7247, %eax
	je	.L211
	cmpl	$31153, %eax
	jne	.L210
.L211:
	movl	104(%rdi), %edx
	movq	16(%rdi), %rax
	testl	%edx, %edx
	je	.L205
	addq	96(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L210:
	movq	$-1, %rax
.L205:
	ret
	.cfi_endproc
.LFE72:
	.size	gztell64, .-gztell64
	.p2align 4
	.globl	gztell
	.type	gztell, @function
gztell:
.LFB73:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L220
	movl	24(%rdi), %eax
	cmpl	$7247, %eax
	je	.L221
	cmpl	$31153, %eax
	jne	.L220
.L221:
	movl	104(%rdi), %edx
	movq	16(%rdi), %rax
	testl	%edx, %edx
	je	.L215
	addq	96(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L220:
	movq	$-1, %rax
.L215:
	ret
	.cfi_endproc
.LFE73:
	.size	gztell, .-gztell
	.p2align 4
	.globl	gzoffset64
	.type	gzoffset64, @function
gzoffset64:
.LFB74:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L240
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	24(%rdi), %eax
	cmpl	$7247, %eax
	je	.L230
	cmpl	$31153, %eax
	jne	.L228
.L230:
	movl	28(%rbx), %edi
	xorl	%esi, %esi
	movl	$1, %edx
	call	lseek@PLT
	cmpq	$-1, %rax
	je	.L228
	cmpl	$7247, 24(%rbx)
	jne	.L225
	movl	128(%rbx), %edx
	subq	%rdx, %rax
.L225:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L228:
	.cfi_restore_state
	movq	$-1, %rax
	jmp	.L225
.L240:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	orq	$-1, %rax
	ret
	.cfi_endproc
.LFE74:
	.size	gzoffset64, .-gzoffset64
	.p2align 4
	.globl	gzoffset
	.type	gzoffset, @function
gzoffset:
.LFB75:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L256
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	24(%rdi), %eax
	cmpl	$7247, %eax
	je	.L246
	cmpl	$31153, %eax
	jne	.L244
.L246:
	movl	28(%rbx), %edi
	xorl	%esi, %esi
	movl	$1, %edx
	call	lseek@PLT
	cmpq	$-1, %rax
	je	.L244
	cmpl	$7247, 24(%rbx)
	jne	.L241
	movl	128(%rbx), %edx
	subq	%rdx, %rax
.L241:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L244:
	.cfi_restore_state
	movq	$-1, %rax
	jmp	.L241
.L256:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	orq	$-1, %rax
	ret
	.cfi_endproc
.LFE75:
	.size	gzoffset, .-gzoffset
	.p2align 4
	.globl	gzeof
	.type	gzeof, @function
gzeof:
.LFB76:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L257
	cmpl	$7247, 24(%rdi)
	je	.L261
.L257:
	ret
	.p2align 4,,10
	.p2align 3
.L261:
	movl	84(%rdi), %eax
	ret
	.cfi_endproc
.LFE76:
	.size	gzeof, .-gzeof
	.section	.rodata.str1.1
.LC2:
	.string	""
.LC3:
	.string	"out of memory"
	.text
	.p2align 4
	.globl	gzerror
	.type	gzerror, @function
gzerror:
.LFB77:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L266
	movl	24(%rdi), %edx
	cmpl	$7247, %edx
	je	.L270
	xorl	%eax, %eax
	cmpl	$31153, %edx
	jne	.L262
.L270:
	movl	108(%rdi), %edx
	testq	%rsi, %rsi
	je	.L265
	movl	%edx, (%rsi)
	movl	108(%rdi), %edx
.L265:
	leaq	.LC3(%rip), %rax
	cmpl	$-4, %edx
	je	.L262
	movq	112(%rdi), %rax
	leaq	.LC2(%rip), %rdx
	testq	%rax, %rax
	cmove	%rdx, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L266:
	xorl	%eax, %eax
.L262:
	ret
	.cfi_endproc
.LFE77:
	.size	gzerror, .-gzerror
	.p2align 4
	.globl	gzclearerr
	.type	gzclearerr, @function
gzclearerr:
.LFB78:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L296
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	24(%rdi), %eax
	cmpl	$7247, %eax
	je	.L294
	cmpl	$31153, %eax
	je	.L299
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L299:
	.cfi_restore_state
	cmpl	$7247, %eax
	je	.L294
.L281:
	movq	112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L282
	cmpl	$-4, 108(%rbx)
	jne	.L300
.L283:
	movq	$0, 112(%rbx)
.L282:
	movl	$0, 108(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L300:
	.cfi_restore_state
	call	free@PLT
	jmp	.L283
	.p2align 4,,10
	.p2align 3
.L294:
	movq	$0, 80(%rbx)
	jmp	.L281
	.p2align 4,,10
	.p2align 3
.L296:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	ret
	.cfi_endproc
.LFE78:
	.size	gzclearerr, .-gzclearerr
	.section	.rodata.str1.1
.LC4:
	.string	"%s%s%s"
.LC5:
	.string	": "
	.text
	.p2align 4
	.globl	gz_error
	.hidden	gz_error
	.type	gz_error, @function
gz_error:
.LFB79:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L302
	cmpl	$-4, 108(%rbx)
	jne	.L317
.L303:
	movq	$0, 112(%rbx)
.L302:
	testl	%r12d, %r12d
	je	.L304
	cmpl	$-5, %r12d
	je	.L304
	movl	$0, (%rbx)
.L304:
	movl	%r12d, 108(%rbx)
	cmpl	$-4, %r12d
	je	.L301
	testq	%r13, %r13
	je	.L301
	movq	32(%rbx), %r12
	movq	%r12, %rdi
	call	strlen@PLT
	movq	%r13, %rdi
	movq	%rax, %r14
	call	strlen@PLT
	leaq	3(%r14,%rax), %rdi
	call	malloc@PLT
	movq	%rax, 112(%rbx)
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L318
	movq	%r12, %rdi
	call	strlen@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	strlen@PLT
	pushq	%r13
	movl	$1, %edx
	movq	%r12, %r9
	leaq	3(%rbx,%rax), %rsi
	leaq	.LC5(%rip), %rax
	movq	$-1, %rcx
	movq	%r14, %rdi
	pushq	%rax
	leaq	.LC4(%rip), %r8
	xorl	%eax, %eax
	call	__snprintf_chk@PLT
	popq	%rax
	popq	%rdx
.L301:
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L317:
	.cfi_restore_state
	call	free@PLT
	jmp	.L303
.L318:
	movl	$-4, 108(%rbx)
	jmp	.L301
	.cfi_endproc
.LFE79:
	.size	gz_error, .-gz_error
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
