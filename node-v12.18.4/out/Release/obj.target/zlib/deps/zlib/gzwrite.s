	.file	"gzwrite.c"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"out of memory"
.LC1:
	.string	"1.2.11"
	.text
	.p2align 4
	.type	gz_init, @function
gz_init:
.LFB63:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movl	44(%rdi), %ebx
	leal	(%rbx,%rbx), %edi
	call	malloc@PLT
	movq	%rax, 48(%r12)
	testq	%rax, %rax
	je	.L9
	movl	64(%r12), %esi
	testl	%esi, %esi
	je	.L13
	movl	%ebx, 40(%r12)
.L10:
	xorl	%eax, %eax
.L1:
	leaq	-24(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	.cfi_restore_state
	movl	%ebx, %edi
	movq	%rax, %r13
	call	malloc@PLT
	movq	%rax, 56(%r12)
	testq	%rax, %rax
	je	.L14
	pushq	$112
	pxor	%xmm0, %xmm0
	movl	88(%r12), %esi
	movl	$31, %ecx
	movl	92(%r12), %r9d
	movl	$8, %edx
	leaq	120(%r12), %rdi
	leaq	.LC1(%rip), %rax
	pushq	%rax
	movl	$8, %r8d
	movq	$0, 200(%r12)
	movups	%xmm0, 184(%r12)
	call	deflateInit2_@PLT
	popq	%rdx
	popq	%rcx
	testl	%eax, %eax
	jne	.L15
	movl	44(%r12), %edx
	movl	64(%r12), %eax
	movq	$0, 120(%r12)
	movl	%edx, 40(%r12)
	testl	%eax, %eax
	jne	.L10
	movl	%edx, 152(%r12)
	movq	56(%r12), %rdx
	movq	%rdx, 144(%r12)
	movq	%rdx, 8(%r12)
	leaq	-24(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L15:
	.cfi_restore_state
	movq	56(%r12), %rdi
	call	free@PLT
	movq	48(%r12), %rdi
.L11:
	call	free@PLT
.L9:
	leaq	.LC0(%rip), %rdx
	movl	$-4, %esi
	movq	%r12, %rdi
	call	gz_error
	movl	$-1, %eax
	jmp	.L1
.L14:
	movq	%r13, %rdi
	jmp	.L11
	.cfi_endproc
.LFE63:
	.size	gz_init, .-gz_init
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"internal error: deflate stream corrupt"
	.text
	.p2align 4
	.type	gz_comp, @function
gz_comp:
.LFB64:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movl	40(%rdi), %eax
	testl	%eax, %eax
	je	.L17
.L21:
	movl	64(%r15), %r13d
	testl	%r13d, %r13d
	jne	.L18
	movl	152(%r15), %ecx
	leaq	120(%r15), %r14
	xorl	%eax, %eax
	movl	$1073741824, %ebx
.L32:
	testl	%ecx, %ecx
	je	.L26
	testl	%r12d, %r12d
	je	.L35
	cmpl	$4, %r12d
	jne	.L28
	cmpl	$1, %eax
	je	.L28
.L35:
	movl	%ecx, %edx
.L27:
	movl	%r12d, %esi
	movq	%r14, %rdi
	movl	%edx, -52(%rbp)
	call	deflate@PLT
	movl	-52(%rbp), %edx
	cmpl	$-2, %eax
	je	.L45
	movl	152(%r15), %ecx
	cmpl	%edx, %ecx
	jne	.L32
	cmpl	$4, %r12d
	je	.L46
	.p2align 4,,10
	.p2align 3
.L25:
	xorl	%r13d, %r13d
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L18:
	movl	128(%r15), %edx
	testl	%edx, %edx
	je	.L25
	movq	120(%r15), %rsi
	movl	$1073741824, %ebx
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L23:
	movl	128(%r15), %edx
	subl	%eax, %edx
	cltq
	addq	120(%r15), %rax
	movl	%edx, 128(%r15)
	movq	%rax, %rsi
	movq	%rax, 120(%r15)
	testl	%edx, %edx
	je	.L25
.L24:
	cmpl	$1073741824, %edx
	movl	28(%r15), %edi
	cmova	%rbx, %rdx
	call	write@PLT
	testl	%eax, %eax
	jns	.L23
.L44:
	call	__errno_location@PLT
	movl	$-1, %r13d
	movl	(%rax), %edi
	call	strerror@PLT
	movl	$-1, %esi
	movq	%r15, %rdi
	movq	%rax, %rdx
	call	gz_error
.L16:
	addq	$24, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L28:
	.cfi_restore_state
	movq	144(%r15), %rdx
	movq	8(%r15), %rsi
	cmpq	%rsi, %rdx
	ja	.L30
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L29:
	movq	144(%r15), %rdx
	cltq
	addq	8(%r15), %rax
	movq	%rax, 8(%r15)
	movq	%rax, %rsi
	cmpq	%rdx, %rax
	jnb	.L47
.L30:
	subq	%rsi, %rdx
	movl	28(%r15), %edi
	cmpq	$1073741824, %rdx
	cmovg	%rbx, %rdx
	movl	%edx, %edx
	call	write@PLT
	testl	%eax, %eax
	jns	.L29
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L26:
	movq	144(%r15), %rdx
	movq	8(%r15), %rsi
	cmpq	%rsi, %rdx
	ja	.L30
	.p2align 4,,10
	.p2align 3
.L34:
	movq	56(%r15), %rax
	movl	40(%r15), %edx
	movq	%rax, 144(%r15)
	movl	%edx, 152(%r15)
	movq	%rax, 8(%r15)
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L47:
	movl	152(%r15), %edx
	testl	%edx, %edx
	jne	.L27
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L17:
	call	gz_init
	movl	%eax, %r13d
	cmpl	$-1, %eax
	jne	.L21
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L45:
	leaq	.LC2(%rip), %rdx
	movl	$-2, %esi
	movq	%r15, %rdi
	movl	$-1, %r13d
	call	gz_error
	jmp	.L16
.L46:
	movq	%r14, %rdi
	call	deflateReset@PLT
	jmp	.L16
	.cfi_endproc
.LFE64:
	.size	gz_comp, .-gz_comp
	.p2align 4
	.type	gz_comp.constprop.0, @function
gz_comp.constprop.0:
.LFB77:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movl	40(%rdi), %edx
	testl	%edx, %edx
	je	.L49
.L53:
	movl	64(%r13), %eax
	testl	%eax, %eax
	je	.L78
	movl	128(%r13), %edx
	testl	%edx, %edx
	je	.L57
	movq	120(%r13), %rsi
	movl	$1073741824, %ebx
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L55:
	movl	128(%r13), %edx
	subl	%eax, %edx
	cltq
	addq	120(%r13), %rax
	movl	%edx, 128(%r13)
	movq	%rax, %rsi
	movq	%rax, 120(%r13)
	testl	%edx, %edx
	je	.L57
.L56:
	cmpl	$1073741824, %edx
	movl	28(%r13), %edi
	cmova	%rbx, %rdx
	call	write@PLT
	testl	%eax, %eax
	jns	.L55
.L77:
	call	__errno_location@PLT
	movl	(%rax), %edi
	call	strerror@PLT
	movl	$-1, %esi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	gz_error
	movl	$-1, %eax
.L48:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L78:
	.cfi_restore_state
	movl	152(%r13), %eax
	leaq	120(%r13), %r12
	movl	$1073741824, %ebx
.L63:
	movl	%eax, %r14d
	testl	%eax, %eax
	je	.L79
.L58:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	deflate@PLT
	cmpl	$-2, %eax
	je	.L80
	movl	152(%r13), %eax
	cmpl	%r14d, %eax
	jne	.L63
.L57:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L79:
	.cfi_restore_state
	movq	144(%r13), %rdx
	movq	8(%r13), %rsi
	cmpq	%rdx, %rsi
	jb	.L59
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L60:
	movq	144(%r13), %rdx
	cltq
	addq	8(%r13), %rax
	movq	%rax, 8(%r13)
	movq	%rax, %rsi
	cmpq	%rax, %rdx
	jbe	.L81
.L59:
	subq	%rsi, %rdx
	movl	28(%r13), %edi
	cmpq	$1073741824, %rdx
	cmovg	%rbx, %rdx
	movl	%edx, %edx
	call	write@PLT
	testl	%eax, %eax
	jns	.L60
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L81:
	movl	152(%r13), %r14d
	testl	%r14d, %r14d
	jne	.L58
.L61:
	movq	56(%r13), %rax
	movl	40(%r13), %r14d
	movq	%rax, 144(%r13)
	movl	%r14d, 152(%r13)
	movq	%rax, 8(%r13)
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L49:
	call	gz_init
	cmpl	$-1, %eax
	jne	.L53
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L80:
	leaq	.LC2(%rip), %rdx
	movl	$-2, %esi
	movq	%r13, %rdi
	call	gz_error
	movl	$-1, %eax
	jmp	.L48
	.cfi_endproc
.LFE77:
	.size	gz_comp.constprop.0, .-gz_comp.constprop.0
	.p2align 4
	.type	gz_write, @function
gz_write:
.LFB66:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -64(%rbp)
	testq	%rdx, %rdx
	je	.L88
	movl	40(%rdi), %edx
	movq	%rdi, %r12
	movq	%rsi, %rbx
	testl	%edx, %edx
	je	.L85
.L89:
	movl	104(%r12), %eax
	movl	128(%r12), %edx
	testl	%eax, %eax
	jne	.L136
.L87:
	movl	40(%r12), %eax
	movq	-64(%rbp), %r14
	movq	%rax, %r15
	cmpq	%rax, %r14
	jnb	.L95
	.p2align 4,,10
	.p2align 3
.L100:
	movq	48(%r12), %rdi
	testl	%edx, %edx
	je	.L96
	movq	120(%r12), %rax
.L97:
	addq	%rdx, %rax
	subq	%rdi, %rax
	subl	%eax, %r15d
	movl	%r15d, %ecx
	movq	%rcx, %r13
	cmpq	%r14, %rcx
	jbe	.L98
	movl	%r14d, %ecx
	movl	%r14d, %r13d
	movq	%rcx, %r15
.L98:
	movl	%eax, %eax
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%rcx, -56(%rbp)
	addq	%rax, %rdi
	call	memcpy@PLT
	movq	-56(%rbp), %rcx
	addl	%r15d, 128(%r12)
	addq	%rcx, 16(%r12)
	subq	%r13, %r14
	jne	.L137
.L84:
	movq	-64(%rbp), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L96:
	.cfi_restore_state
	movq	%rdi, 120(%r12)
	movq	%rdi, %rax
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L137:
	movq	%r12, %rdi
	call	gz_comp.constprop.0
	cmpl	$-1, %eax
	je	.L88
	movl	128(%r12), %edx
	movl	40(%r12), %r15d
	addq	%r13, %rbx
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L95:
	testl	%edx, %edx
	je	.L103
	movq	%r12, %rdi
	call	gz_comp.constprop.0
	cmpl	$-1, %eax
	je	.L88
.L103:
	movq	%rbx, 120(%r12)
	movq	-64(%rbp), %rbx
	movl	$4294967294, %r15d
	movl	$4294967295, %r14d
	.p2align 4,,10
	.p2align 3
.L102:
	cmpq	%r15, %rbx
	ja	.L104
	addq	%rbx, 16(%r12)
	movq	%r12, %rdi
	movl	%ebx, 128(%r12)
	call	gz_comp.constprop.0
	cmpl	$-1, %eax
	jne	.L84
.L88:
	movq	$0, -64(%rbp)
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L85:
	call	gz_init
	cmpl	$-1, %eax
	jne	.L89
	jmp	.L88
	.p2align 4,,10
	.p2align 3
.L136:
	movl	$0, 104(%r12)
	movq	96(%r12), %r15
	testl	%edx, %edx
	jne	.L90
.L92:
	movl	$1, %eax
	testq	%r15, %r15
	jne	.L91
	.p2align 4,,10
	.p2align 3
.L135:
	movl	128(%r12), %edx
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L138:
	movl	%r13d, %edx
	xorl	%esi, %esi
	call	memset@PLT
	movq	48(%r12), %rdi
.L94:
	addq	%r14, 16(%r12)
	movq	%rdi, 120(%r12)
	movq	%r12, %rdi
	movl	%r13d, 128(%r12)
	call	gz_comp.constprop.0
	cmpl	$-1, %eax
	je	.L88
	xorl	%eax, %eax
	subq	%r14, %r15
	je	.L135
.L91:
	movl	40(%r12), %r14d
	movq	%r14, %r13
	cmpq	%r15, %r14
	jle	.L93
	movl	%r15d, %r13d
	movl	%r15d, %r14d
.L93:
	movq	48(%r12), %rdi
	testl	%eax, %eax
	je	.L94
	jmp	.L138
	.p2align 4,,10
	.p2align 3
.L104:
	addq	%r14, 16(%r12)
	movq	%r12, %rdi
	movl	$-1, 128(%r12)
	call	gz_comp.constprop.0
	cmpl	$-1, %eax
	je	.L88
	subq	%r14, %rbx
	jne	.L102
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L90:
	movq	%r12, %rdi
	call	gz_comp.constprop.0
	cmpl	$-1, %eax
	jne	.L92
	jmp	.L88
	.cfi_endproc
.LFE66:
	.size	gz_write, .-gz_write
	.section	.rodata.str1.8
	.align 8
.LC3:
	.string	"requested length does not fit in int"
	.text
	.p2align 4
	.globl	gzwrite
	.type	gzwrite, @function
gzwrite:
.LFB67:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L142
	cmpl	$31153, 24(%rdi)
	jne	.L146
	movl	108(%rdi), %eax
	testl	%eax, %eax
	jne	.L144
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	testl	%edx, %edx
	js	.L149
	movl	%edx, %edx
	call	gz_write
	leave
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L146:
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L149:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	leaq	.LC3(%rip), %rdx
	movl	$-3, %esi
	movl	%eax, -4(%rbp)
	call	gz_error
	movl	-4(%rbp), %eax
	leave
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L142:
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L144:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE67:
	.size	gzwrite, .-gzwrite
	.section	.rodata.str1.8
	.align 8
.LC4:
	.string	"request does not fit in a size_t"
	.text
	.p2align 4
	.globl	gzfwrite
	.type	gzfwrite, @function
gzfwrite:
.LFB68:
	.cfi_startproc
	endbr64
	xorl	%r8d, %r8d
	testq	%rcx, %rcx
	je	.L162
	cmpl	$31153, 24(%rcx)
	jne	.L162
	movl	108(%rcx), %eax
	testl	%eax, %eax
	jne	.L162
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rsi, %rbx
	subq	$8, %rsp
	testq	%rsi, %rsi
	je	.L150
	movq	%rdx, %r8
	movq	%rdx, %rsi
	xorl	%edx, %edx
	imulq	%rbx, %r8
	movq	%r8, %rax
	divq	%rbx
	cmpq	%rsi, %rax
	jne	.L165
	testq	%r8, %r8
	je	.L150
	movq	%r8, %rdx
	movq	%rdi, %rsi
	movq	%rcx, %rdi
	call	gz_write
	xorl	%edx, %edx
	divq	%rbx
	movq	%rax, %r8
.L150:
	addq	$8, %rsp
	movq	%r8, %rax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L162:
	.cfi_restore 3
	.cfi_restore 6
	movq	%r8, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L165:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	leaq	.LC4(%rip), %rdx
	movl	$-2, %esi
	movq	%rcx, %rdi
	call	gz_error
	xorl	%r8d, %r8d
	jmp	.L150
	.cfi_endproc
.LFE68:
	.size	gzfwrite, .-gzfwrite
	.p2align 4
	.globl	gzputc
	.type	gzputc, @function
gzputc:
.LFB69:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L169
	cmpl	$31153, 24(%rdi)
	movq	%rdi, %r12
	jne	.L169
	movl	108(%rdi), %ecx
	testl	%ecx, %ecx
	jne	.L169
	movl	104(%rdi), %edx
	movl	%esi, %ebx
	testl	%edx, %edx
	jne	.L170
.L175:
	movl	40(%r12), %ecx
	testl	%ecx, %ecx
	je	.L172
	movl	128(%r12), %edx
	movq	48(%r12), %rsi
	testl	%edx, %edx
	je	.L179
	movq	120(%r12), %rax
.L180:
	addq	%rdx, %rax
	subq	%rsi, %rax
	cmpl	%eax, %ecx
	jbe	.L172
	movl	%eax, %eax
	movb	%bl, (%rsi,%rax)
	movzbl	%bl, %eax
	addl	$1, 128(%r12)
	addq	$1, 16(%r12)
	jmp	.L166
	.p2align 4,,10
	.p2align 3
.L172:
	leaq	-57(%rbp), %rsi
	movl	$1, %edx
	movq	%r12, %rdi
	movb	%bl, -57(%rbp)
	call	gz_write
	cmpq	$1, %rax
	jne	.L169
	movzbl	%bl, %eax
.L166:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L195
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L179:
	.cfi_restore_state
	movq	%rsi, 120(%r12)
	movq	%rsi, %rax
	jmp	.L180
	.p2align 4,,10
	.p2align 3
.L170:
	movl	128(%rdi), %eax
	movq	96(%rdi), %r13
	movl	$0, 104(%rdi)
	testl	%eax, %eax
	jne	.L173
.L176:
	movl	$1, %eax
	testq	%r13, %r13
	jne	.L174
	jmp	.L175
	.p2align 4,,10
	.p2align 3
.L178:
	addq	%r14, 16(%r12)
	movq	%rdi, 120(%r12)
	movq	%r12, %rdi
	movl	%r15d, 128(%r12)
	call	gz_comp.constprop.0
	cmpl	$-1, %eax
	je	.L169
	subq	%r14, %r13
	je	.L175
	xorl	%eax, %eax
.L174:
	movl	40(%r12), %r14d
	movq	%r14, %r15
	cmpq	%r13, %r14
	jle	.L177
	movl	%r13d, %r15d
	movl	%r13d, %r14d
.L177:
	movq	48(%r12), %rdi
	testl	%eax, %eax
	je	.L178
	movl	%r15d, %edx
	xorl	%esi, %esi
	call	memset@PLT
	movq	48(%r12), %rdi
	jmp	.L178
	.p2align 4,,10
	.p2align 3
.L173:
	call	gz_comp.constprop.0
	cmpl	$-1, %eax
	jne	.L176
	.p2align 4,,10
	.p2align 3
.L169:
	movl	$-1, %eax
	jmp	.L166
.L195:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE69:
	.size	gzputc, .-gzputc
	.p2align 4
	.globl	gzputs
	.type	gzputs, @function
gzputs:
.LFB70:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L203
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	cmpl	$31153, 24(%rdi)
	jne	.L197
	movl	108(%rdi), %eax
	testl	%eax, %eax
	jne	.L197
	movq	%rsi, %rdi
	movq	%rsi, %r13
	call	strlen@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%rax, %rbx
	call	gz_write
	movl	%eax, %r8d
	testl	%eax, %eax
	jne	.L196
	testq	%rbx, %rbx
	jne	.L197
.L196:
	addq	$8, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L197:
	.cfi_restore_state
	movl	$-1, %r8d
	jmp	.L196
.L203:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	orl	$-1, %r8d
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE70:
	.size	gzputs, .-gzputs
	.p2align 4
	.globl	gzvprintf
	.type	gzvprintf, @function
gzvprintf:
.LFB71:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%rdi, %rdi
	je	.L224
	cmpl	$31153, 24(%rdi)
	movq	%rdi, %rbx
	jne	.L224
	movl	108(%rdi), %edi
	testl	%edi, %edi
	jne	.L224
	movl	40(%rbx), %ecx
	movq	%rsi, %r14
	movq	%rdx, %r15
	testl	%ecx, %ecx
	je	.L248
.L209:
	movl	104(%rbx), %edx
	movl	128(%rbx), %eax
	testl	%edx, %edx
	jne	.L249
.L210:
	testl	%eax, %eax
	je	.L219
	movq	120(%rbx), %rdx
.L220:
	leaq	(%rdx,%rax), %rdi
	movl	40(%rbx), %eax
	movq	%r15, %r9
	movq	%r14, %r8
	movq	$-1, %rcx
	movl	$1, %edx
	movq	%rdi, -56(%rbp)
	subl	$1, %eax
	movb	$0, (%rdi,%rax)
	movl	40(%rbx), %esi
	call	__vsnprintf_chk@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	je	.L207
	movl	40(%rbx), %r13d
	cmpl	%eax, %r13d
	jbe	.L227
	movq	-56(%rbp), %rdi
	leal	-1(%r13), %eax
	cmpb	$0, (%rdi,%rax)
	jne	.L227
	movl	128(%rbx), %r14d
	movslq	%r12d, %rax
	addq	%rax, 16(%rbx)
	addl	%r12d, %r14d
	movl	%r14d, 128(%rbx)
	cmpl	%r14d, %r13d
	jbe	.L250
.L207:
	addq	$24, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L219:
	.cfi_restore_state
	movq	48(%rbx), %rdx
	xorl	%eax, %eax
	movq	%rdx, 120(%rbx)
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L248:
	movq	%rbx, %rdi
	call	gz_init
	cmpl	$-1, %eax
	jne	.L209
.L247:
	movl	108(%rbx), %r12d
	jmp	.L207
	.p2align 4,,10
	.p2align 3
.L227:
	xorl	%r12d, %r12d
	jmp	.L207
	.p2align 4,,10
	.p2align 3
.L249:
	movl	$0, 104(%rbx)
	movq	96(%rbx), %r12
	testl	%eax, %eax
	jne	.L251
	testq	%r12, %r12
	je	.L219
.L213:
	movl	$1, %eax
	jmp	.L218
	.p2align 4,,10
	.p2align 3
.L217:
	addq	%r13, 16(%rbx)
	movq	%rdi, 120(%rbx)
	movq	%rbx, %rdi
	movl	%ecx, 128(%rbx)
	call	gz_comp.constprop.0
	cmpl	$-1, %eax
	je	.L247
	subq	%r13, %r12
	je	.L246
	xorl	%eax, %eax
.L218:
	movl	40(%rbx), %r13d
	movq	%r13, %rcx
	cmpq	%r12, %r13
	jle	.L216
	movl	%r12d, %ecx
	movl	%r12d, %r13d
.L216:
	movq	48(%rbx), %rdi
	testl	%eax, %eax
	je	.L217
	movl	%ecx, %edx
	xorl	%esi, %esi
	movl	%ecx, -56(%rbp)
	call	memset@PLT
	movq	48(%rbx), %rdi
	movl	-56(%rbp), %ecx
	jmp	.L217
	.p2align 4,,10
	.p2align 3
.L251:
	movq	%rbx, %rdi
	call	gz_comp.constprop.0
	cmpl	$-1, %eax
	je	.L247
	testq	%r12, %r12
	jne	.L213
	.p2align 4,,10
	.p2align 3
.L246:
	movl	128(%rbx), %eax
	jmp	.L210
	.p2align 4,,10
	.p2align 3
.L250:
	movl	%r13d, 128(%rbx)
	movq	%rbx, %rdi
	call	gz_comp.constprop.0
	cmpl	$-1, %eax
	je	.L247
	movq	48(%rbx), %rdi
	movl	40(%rbx), %esi
	subl	%r13d, %r14d
	movl	%r14d, %edx
	addq	%rdi, %rsi
	call	memcpy@PLT
	movq	48(%rbx), %rax
	movl	%r14d, 128(%rbx)
	movq	%rax, 120(%rbx)
	jmp	.L207
	.p2align 4,,10
	.p2align 3
.L224:
	movl	$-2, %r12d
	jmp	.L207
	.cfi_endproc
.LFE71:
	.size	gzvprintf, .-gzvprintf
	.p2align 4
	.globl	gzprintf
	.type	gzprintf, @function
gzprintf:
.LFB72:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$216, %rsp
	movq	%rdx, -208(%rbp)
	movq	%rcx, -200(%rbp)
	movq	%r8, -192(%rbp)
	movq	%r9, -184(%rbp)
	testb	%al, %al
	je	.L253
	movaps	%xmm0, -176(%rbp)
	movaps	%xmm1, -160(%rbp)
	movaps	%xmm2, -144(%rbp)
	movaps	%xmm3, -128(%rbp)
	movaps	%xmm4, -112(%rbp)
	movaps	%xmm5, -96(%rbp)
	movaps	%xmm6, -80(%rbp)
	movaps	%xmm7, -64(%rbp)
.L253:
	movq	%fs:40, %rax
	movq	%rax, -232(%rbp)
	xorl	%eax, %eax
	leaq	16(%rbp), %rax
	movl	$16, -256(%rbp)
	movq	%rax, -248(%rbp)
	leaq	-224(%rbp), %rax
	movl	$48, -252(%rbp)
	movq	%rax, -240(%rbp)
	testq	%rbx, %rbx
	je	.L271
	cmpl	$31153, 24(%rbx)
	jne	.L271
	movl	108(%rbx), %esi
	testl	%esi, %esi
	jne	.L271
	movl	40(%rbx), %ecx
	testl	%ecx, %ecx
	je	.L295
.L255:
	movl	104(%rbx), %edx
	movl	128(%rbx), %eax
	testl	%edx, %edx
	jne	.L296
.L256:
	testl	%eax, %eax
	je	.L265
	movq	120(%rbx), %rdx
.L266:
	leaq	(%rdx,%rax), %r15
	movl	40(%rbx), %eax
	leaq	-256(%rbp), %r9
	movq	%r14, %r8
	movq	$-1, %rcx
	movl	$1, %edx
	movq	%r15, %rdi
	subl	$1, %eax
	movb	$0, (%r15,%rax)
	movl	40(%rbx), %esi
	call	__vsnprintf_chk@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	je	.L252
	movl	40(%rbx), %r13d
	cmpl	%eax, %r13d
	jbe	.L274
	leal	-1(%r13), %eax
	cmpb	$0, (%r15,%rax)
	jne	.L274
	movl	128(%rbx), %r14d
	movslq	%r12d, %rax
	addq	%rax, 16(%rbx)
	addl	%r12d, %r14d
	movl	%r14d, 128(%rbx)
	cmpl	%r14d, %r13d
	jbe	.L297
	.p2align 4,,10
	.p2align 3
.L252:
	movq	-232(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L298
	addq	$216, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L265:
	.cfi_restore_state
	movq	48(%rbx), %rdx
	xorl	%eax, %eax
	movq	%rdx, 120(%rbx)
	jmp	.L266
	.p2align 4,,10
	.p2align 3
.L295:
	movq	%rbx, %rdi
	call	gz_init
	cmpl	$-1, %eax
	jne	.L255
.L294:
	movl	108(%rbx), %r12d
	jmp	.L252
	.p2align 4,,10
	.p2align 3
.L274:
	xorl	%r12d, %r12d
	jmp	.L252
	.p2align 4,,10
	.p2align 3
.L296:
	movl	$0, 104(%rbx)
	movq	96(%rbx), %r12
	testl	%eax, %eax
	jne	.L299
	testq	%r12, %r12
	je	.L265
.L259:
	movl	$1, %eax
	jmp	.L264
	.p2align 4,,10
	.p2align 3
.L263:
	addq	%r13, 16(%rbx)
	movq	%rdi, 120(%rbx)
	movq	%rbx, %rdi
	movl	%r15d, 128(%rbx)
	call	gz_comp.constprop.0
	cmpl	$-1, %eax
	je	.L294
	subq	%r13, %r12
	je	.L293
	xorl	%eax, %eax
.L264:
	movl	40(%rbx), %r13d
	movq	%r13, %r15
	cmpq	%r12, %r13
	jle	.L262
	movl	%r12d, %r15d
	movl	%r12d, %r13d
.L262:
	movq	48(%rbx), %rdi
	testl	%eax, %eax
	je	.L263
	movl	%r15d, %edx
	xorl	%esi, %esi
	call	memset@PLT
	movq	48(%rbx), %rdi
	jmp	.L263
	.p2align 4,,10
	.p2align 3
.L299:
	movq	%rbx, %rdi
	call	gz_comp.constprop.0
	cmpl	$-1, %eax
	je	.L294
	testq	%r12, %r12
	jne	.L259
	.p2align 4,,10
	.p2align 3
.L293:
	movl	128(%rbx), %eax
	jmp	.L256
	.p2align 4,,10
	.p2align 3
.L297:
	movl	%r13d, 128(%rbx)
	movq	%rbx, %rdi
	call	gz_comp.constprop.0
	cmpl	$-1, %eax
	je	.L294
	movq	48(%rbx), %rdi
	movl	40(%rbx), %esi
	subl	%r13d, %r14d
	movl	%r14d, %edx
	addq	%rdi, %rsi
	call	memcpy@PLT
	movq	48(%rbx), %rax
	movl	%r14d, 128(%rbx)
	movq	%rax, 120(%rbx)
	jmp	.L252
	.p2align 4,,10
	.p2align 3
.L271:
	movl	$-2, %r12d
	jmp	.L252
.L298:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE72:
	.size	gzprintf, .-gzprintf
	.p2align 4
	.globl	gzflush
	.type	gzflush, @function
gzflush:
.LFB73:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L309
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpl	$31153, 24(%rdi)
	jne	.L312
	movl	108(%rdi), %ecx
	testl	%ecx, %ecx
	jne	.L312
	movl	%esi, %r15d
	cmpl	$4, %esi
	ja	.L312
	movl	104(%rdi), %edx
	testl	%edx, %edx
	je	.L302
	movl	128(%rdi), %eax
	movq	96(%rdi), %r12
	movl	$0, 104(%rdi)
	testl	%eax, %eax
	jne	.L303
.L306:
	movl	$1, %eax
	testq	%r12, %r12
	jne	.L304
	jmp	.L302
	.p2align 4,,10
	.p2align 3
.L308:
	addq	%r13, 16(%rbx)
	movq	%rdi, 120(%rbx)
	movq	%rbx, %rdi
	movl	%r14d, 128(%rbx)
	call	gz_comp.constprop.0
	cmpl	$-1, %eax
	je	.L329
	subq	%r13, %r12
	je	.L302
	xorl	%eax, %eax
.L304:
	movl	40(%rbx), %r13d
	movq	%r13, %r14
	cmpq	%r12, %r13
	jle	.L307
	movl	%r12d, %r14d
	movl	%r12d, %r13d
.L307:
	movq	48(%rbx), %rdi
	testl	%eax, %eax
	je	.L308
	movl	%r14d, %edx
	xorl	%esi, %esi
	call	memset@PLT
	movq	48(%rbx), %rdi
	jmp	.L308
	.p2align 4,,10
	.p2align 3
.L302:
	movl	%r15d, %esi
	movq	%rbx, %rdi
	call	gz_comp
.L329:
	movl	108(%rbx), %eax
.L300:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L303:
	.cfi_restore_state
	call	gz_comp.constprop.0
	cmpl	$-1, %eax
	jne	.L306
	jmp	.L329
	.p2align 4,,10
	.p2align 3
.L312:
	movl	$-2, %eax
	jmp	.L300
.L309:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	movl	$-2, %eax
	ret
	.cfi_endproc
.LFE73:
	.size	gzflush, .-gzflush
	.p2align 4
	.globl	gzsetparams
	.type	gzsetparams, @function
gzsetparams:
.LFB74:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, -52(%rbp)
	testq	%rdi, %rdi
	je	.L344
	cmpl	$31153, 24(%rdi)
	movq	%rdi, %rbx
	jne	.L344
	movl	108(%rdi), %r15d
	testl	%r15d, %r15d
	jne	.L344
	movl	%esi, %r13d
	cmpl	%esi, 88(%rdi)
	je	.L359
	movl	104(%rbx), %esi
	testl	%esi, %esi
	jne	.L360
.L333:
	movl	40(%rbx), %edx
	testl	%edx, %edx
	je	.L340
	movl	128(%rbx), %eax
	testl	%eax, %eax
	je	.L341
	movl	$5, %esi
	movq	%rbx, %rdi
	call	gz_comp
	cmpl	$-1, %eax
	je	.L358
.L341:
	movl	-52(%rbp), %edx
	leaq	120(%rbx), %rdi
	movl	%r13d, %esi
	call	deflateParams@PLT
.L340:
	movl	-52(%rbp), %eax
	movl	%r13d, 88(%rbx)
	movl	%eax, 92(%rbx)
.L330:
	addq	$24, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L359:
	.cfi_restore_state
	cmpl	%edx, 92(%rdi)
	je	.L330
	movl	104(%rbx), %esi
	testl	%esi, %esi
	je	.L333
.L360:
	movl	128(%rbx), %ecx
	movq	96(%rbx), %r12
	movl	$0, 104(%rbx)
	testl	%ecx, %ecx
	jne	.L334
.L337:
	movl	$1, %eax
	testq	%r12, %r12
	jne	.L335
	jmp	.L333
	.p2align 4,,10
	.p2align 3
.L339:
	addq	%r14, 16(%rbx)
	movq	%rdi, 120(%rbx)
	movq	%rbx, %rdi
	movl	%r8d, 128(%rbx)
	call	gz_comp.constprop.0
	cmpl	$-1, %eax
	je	.L358
	subq	%r14, %r12
	je	.L333
	xorl	%eax, %eax
.L335:
	movl	40(%rbx), %r14d
	movq	%r14, %r8
	cmpq	%r12, %r14
	jle	.L338
	movl	%r12d, %r8d
	movl	%r12d, %r14d
.L338:
	movq	48(%rbx), %rdi
	testl	%eax, %eax
	je	.L339
	movl	%r8d, %edx
	xorl	%esi, %esi
	movl	%r8d, -56(%rbp)
	call	memset@PLT
	movq	48(%rbx), %rdi
	movl	-56(%rbp), %r8d
	jmp	.L339
	.p2align 4,,10
	.p2align 3
.L334:
	movq	%rbx, %rdi
	call	gz_comp.constprop.0
	cmpl	$-1, %eax
	jne	.L337
	.p2align 4,,10
	.p2align 3
.L358:
	movl	108(%rbx), %r15d
	jmp	.L330
	.p2align 4,,10
	.p2align 3
.L344:
	movl	$-2, %r15d
	jmp	.L330
	.cfi_endproc
.LFE74:
	.size	gzsetparams, .-gzsetparams
	.p2align 4
	.globl	gzclose_w
	.type	gzclose_w, @function
gzclose_w:
.LFB75:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testq	%rdi, %rdi
	je	.L377
	cmpl	$31153, 24(%rdi)
	movq	%rdi, %r12
	jne	.L377
	movl	104(%rdi), %esi
	testl	%esi, %esi
	jne	.L363
.L367:
	xorl	%r13d, %r13d
.L364:
	movl	$4, %esi
	movq	%r12, %rdi
	call	gz_comp
	cmpl	$-1, %eax
	jne	.L372
	movl	108(%r12), %r13d
.L372:
	movl	40(%r12), %edx
	testl	%edx, %edx
	je	.L373
	movl	64(%r12), %eax
	testl	%eax, %eax
	je	.L391
.L374:
	movq	48(%r12), %rdi
	call	free@PLT
.L373:
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	gz_error
	movq	32(%r12), %rdi
	call	free@PLT
	movl	28(%r12), %edi
	call	close@PLT
	movq	%r12, %rdi
	cmpl	$-1, %eax
	movl	$-1, %eax
	cmove	%eax, %r13d
	call	free@PLT
.L361:
	popq	%rbx
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L363:
	.cfi_restore_state
	movl	128(%rdi), %ecx
	movq	96(%rdi), %rbx
	movl	$0, 104(%rdi)
	testl	%ecx, %ecx
	jne	.L365
.L369:
	movl	$1, %eax
	testq	%rbx, %rbx
	jne	.L366
	jmp	.L367
	.p2align 4,,10
	.p2align 3
.L371:
	addq	%r13, 16(%r12)
	movq	%rdi, 120(%r12)
	movq	%r12, %rdi
	movl	%r14d, 128(%r12)
	call	gz_comp.constprop.0
	cmpl	$-1, %eax
	je	.L368
	xorl	%eax, %eax
	subq	%r13, %rbx
	je	.L367
.L366:
	movl	40(%r12), %r13d
	movq	%r13, %r14
	cmpq	%rbx, %r13
	jle	.L370
	movl	%ebx, %r14d
	movl	%ebx, %r13d
.L370:
	movq	48(%r12), %rdi
	testl	%eax, %eax
	je	.L371
	movl	%r14d, %edx
	xorl	%esi, %esi
	call	memset@PLT
	movq	48(%r12), %rdi
	jmp	.L371
	.p2align 4,,10
	.p2align 3
.L391:
	leaq	120(%r12), %rdi
	call	deflateEnd@PLT
	movq	56(%r12), %rdi
	call	free@PLT
	jmp	.L374
	.p2align 4,,10
	.p2align 3
.L365:
	call	gz_comp.constprop.0
	cmpl	$-1, %eax
	jne	.L369
	.p2align 4,,10
	.p2align 3
.L368:
	movl	108(%r12), %r13d
	jmp	.L364
	.p2align 4,,10
	.p2align 3
.L377:
	movl	$-2, %r13d
	jmp	.L361
	.cfi_endproc
.LFE75:
	.size	gzclose_w, .-gzclose_w
	.hidden	gz_error
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
