	.file	"zutil.c"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"1.2.11"
	.text
	.p2align 4
	.globl	zlibVersion
	.type	zlibVersion, @function
zlibVersion:
.LFB63:
	.cfi_startproc
	endbr64
	leaq	.LC0(%rip), %rax
	ret
	.cfi_endproc
.LFE63:
	.size	zlibVersion, .-zlibVersion
	.p2align 4
	.globl	zlibCompileFlags
	.type	zlibCompileFlags, @function
zlibCompileFlags:
.LFB64:
	.cfi_startproc
	endbr64
	movl	$169, %eax
	ret
	.cfi_endproc
.LFE64:
	.size	zlibCompileFlags, .-zlibCompileFlags
	.p2align 4
	.globl	zError
	.type	zError, @function
zError:
.LFB65:
	.cfi_startproc
	endbr64
	movl	$2, %eax
	leaq	z_errmsg(%rip), %rdx
	subl	%edi, %eax
	cltq
	movq	(%rdx,%rax,8), %rax
	ret
	.cfi_endproc
.LFE65:
	.size	zError, .-zError
	.p2align 4
	.globl	zcalloc
	.hidden	zcalloc
	.type	zcalloc, @function
zcalloc:
.LFB66:
	.cfi_startproc
	endbr64
	movl	%esi, %edi
	imull	%edx, %edi
	jmp	malloc@PLT
	.cfi_endproc
.LFE66:
	.size	zcalloc, .-zcalloc
	.p2align 4
	.globl	zcfree
	.hidden	zcfree
	.type	zcfree, @function
zcfree:
.LFB67:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdi
	jmp	free@PLT
	.cfi_endproc
.LFE67:
	.size	zcfree, .-zcfree
	.globl	z_errmsg
	.section	.rodata.str1.1
.LC1:
	.string	"need dictionary"
.LC2:
	.string	"stream end"
.LC3:
	.string	""
.LC4:
	.string	"file error"
.LC5:
	.string	"stream error"
.LC6:
	.string	"data error"
.LC7:
	.string	"insufficient memory"
.LC8:
	.string	"buffer error"
.LC9:
	.string	"incompatible version"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	z_errmsg, @object
	.size	z_errmsg, 80
z_errmsg:
	.quad	.LC1
	.quad	.LC2
	.quad	.LC3
	.quad	.LC4
	.quad	.LC5
	.quad	.LC6
	.quad	.LC7
	.quad	.LC8
	.quad	.LC9
	.quad	.LC3
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
