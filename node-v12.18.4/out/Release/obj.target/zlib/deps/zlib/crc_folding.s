	.file	"crc_folding.c"
	.text
	.p2align 4
	.globl	crc_fold_init
	.hidden	crc_fold_init
	.type	crc_fold_init, @function
crc_fold_init:
.LFB5310:
	.cfi_startproc
	endbr64
	movdqa	.LC0(%rip), %xmm0
	movq	(%rdi), %rax
	movups	%xmm0, 80(%rdi)
	pxor	%xmm0, %xmm0
	movups	%xmm0, 96(%rdi)
	movups	%xmm0, 112(%rdi)
	movups	%xmm0, 128(%rdi)
	movq	$0, 96(%rax)
	ret
	.cfi_endproc
.LFE5310:
	.size	crc_fold_init, .-crc_fold_init
	.p2align 4
	.globl	crc_fold_copy
	.hidden	crc_fold_copy
	.type	crc_fold_copy, @function
crc_fold_copy:
.LFB5316:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	movq	%rdx, %rsi
	subq	$120, %rsp
	movdqu	80(%rdi), %xmm2
	movdqu	96(%rdi), %xmm1
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movdqu	112(%rdi), %xmm4
	movdqu	128(%rdi), %xmm3
	cmpq	$15, %rcx
	jg	.L4
	testq	%rcx, %rcx
	je	.L3
.L27:
	movdqa	.LC2(%rip), %xmm0
.L6:
	pxor	%xmm5, %xmm5
	movq	%r13, %rdx
	leaq	-64(%rbp), %rdi
	movl	$16, %ecx
	movaps	%xmm0, -144(%rbp)
	salq	$4, %r13
	movaps	%xmm4, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm3, -96(%rbp)
	movaps	%xmm2, -80(%rbp)
	movaps	%xmm5, -64(%rbp)
	call	__memcpy_chk@PLT
	movq	-64(%rbp), %xmm5
	movdqa	-80(%rbp), %xmm2
	leaq	-16+pshufb_shf_table(%rip), %rax
	pinsrq	$1, -56(%rbp), %xmm5
	movdqa	-112(%rbp), %xmm1
	movdqa	%xmm2, %xmm6
	movdqa	-128(%rbp), %xmm4
	movdqa	-96(%rbp), %xmm3
	movups	%xmm5, (%rbx)
	movdqa	(%rax,%r13), %xmm7
	movdqa	-144(%rbp), %xmm0
	movdqa	%xmm7, %xmm8
	pshufb	%xmm7, %xmm6
	pxor	.LC1(%rip), %xmm7
	movdqa	%xmm6, %xmm9
	movdqa	%xmm1, %xmm6
	pshufb	%xmm8, %xmm5
	pshufb	%xmm8, %xmm6
	pshufb	%xmm7, %xmm2
	pshufb	%xmm7, %xmm1
	por	%xmm6, %xmm2
	movdqa	%xmm4, %xmm6
	pshufb	%xmm7, %xmm4
	pshufb	%xmm8, %xmm6
	movdqa	%xmm5, %xmm10
	por	%xmm6, %xmm1
	movdqa	%xmm3, %xmm6
	pshufb	%xmm7, %xmm3
	pshufb	%xmm8, %xmm6
	por	%xmm3, %xmm5
	por	%xmm4, %xmm6
	movdqa	%xmm9, %xmm4
	pclmulqdq	$1, %xmm0, %xmm9
	pclmulqdq	$16, %xmm0, %xmm4
	xorps	%xmm9, %xmm5
	xorps	%xmm4, %xmm5
.L12:
	movups	%xmm2, 80(%r12)
	movups	%xmm1, 96(%r12)
	movups	%xmm6, 112(%r12)
	movups	%xmm5, 128(%r12)
	movups	%xmm10, 144(%r12)
.L3:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L28
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	.cfi_restore_state
	movq	%rdx, %rax
	negq	%rax
	andl	$15, %eax
	jne	.L7
	movdqu	144(%rdi), %xmm10
.L8:
	movq	%r13, %r8
	subq	$64, %r8
	movq	%r8, %rcx
	js	.L16
	shrq	$6, %rcx
	movdqa	.LC2(%rip), %xmm0
	movq	%rsi, %rax
	leaq	1(%rcx), %rdi
	salq	$6, %rdi
	leaq	(%rbx,%rdi), %rdx
	.p2align 4,,10
	.p2align 3
.L10:
	movdqa	%xmm2, %xmm5
	pclmulqdq	$16, %xmm0, %xmm2
	movdqa	(%rax), %xmm9
	movdqa	16(%rax), %xmm8
	pclmulqdq	$1, %xmm0, %xmm5
	movdqa	32(%rax), %xmm7
	addq	$64, %rbx
	addq	$64, %rax
	xorps	%xmm5, %xmm2
	movdqa	%xmm1, %xmm5
	pclmulqdq	$16, %xmm0, %xmm1
	movdqa	-16(%rax), %xmm6
	pclmulqdq	$1, %xmm0, %xmm5
	movups	%xmm9, -64(%rbx)
	pxor	%xmm9, %xmm2
	xorps	%xmm5, %xmm1
	movdqa	%xmm4, %xmm5
	pclmulqdq	$16, %xmm0, %xmm4
	movups	%xmm7, -32(%rbx)
	pclmulqdq	$1, %xmm0, %xmm5
	movups	%xmm8, -48(%rbx)
	pxor	%xmm8, %xmm1
	xorps	%xmm5, %xmm4
	movdqa	%xmm3, %xmm5
	pclmulqdq	$16, %xmm0, %xmm3
	movups	%xmm6, -16(%rbx)
	pclmulqdq	$1, %xmm0, %xmm5
	pxor	%xmm7, %xmm4
	xorps	%xmm5, %xmm3
	pxor	%xmm6, %xmm3
	cmpq	%rdx, %rbx
	jne	.L10
	movq	%rcx, %rax
	addq	%rdi, %rsi
	negq	%rax
	salq	$6, %rax
	leaq	-128(%r13,%rax), %rcx
	leaq	(%r8,%rax), %r13
.L9:
	cmpq	$-16, %rcx
	jge	.L29
	cmpq	$-32, %rcx
	jge	.L30
	cmpq	$-48, %rcx
	jl	.L14
	movdqa	.LC2(%rip), %xmm0
	movdqa	%xmm2, %xmm6
	movdqa	(%rsi), %xmm5
	pclmulqdq	$1, %xmm0, %xmm6
	pclmulqdq	$16, %xmm0, %xmm2
	movups	%xmm5, (%rdx)
	xorps	%xmm6, %xmm2
	pxor	%xmm2, %xmm5
	subq	$16, %r13
	je	.L19
	movdqa	%xmm1, %xmm2
	leaq	16(%rdx), %rbx
	movdqa	%xmm4, %xmm1
	addq	$16, %rsi
	movdqa	%xmm3, %xmm4
	movdqa	%xmm5, %xmm3
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L30:
	movdqa	.LC2(%rip), %xmm0
	movdqa	%xmm1, %xmm7
	movdqa	(%rsi), %xmm6
	movdqa	16(%rsi), %xmm5
	pclmulqdq	$1, %xmm0, %xmm7
	pclmulqdq	$16, %xmm0, %xmm1
	movups	%xmm6, (%rdx)
	xorps	%xmm7, %xmm1
	movdqa	%xmm2, %xmm7
	pclmulqdq	$16, %xmm0, %xmm2
	movups	%xmm5, 16(%rdx)
	pclmulqdq	$1, %xmm0, %xmm7
	pxor	%xmm1, %xmm5
	xorps	%xmm7, %xmm2
	pxor	%xmm2, %xmm6
	subq	$32, %r13
	je	.L18
	movdqa	%xmm3, %xmm1
	movdqa	%xmm4, %xmm2
	leaq	32(%rdx), %rbx
	addq	$32, %rsi
	movdqa	%xmm5, %xmm3
	movdqa	%xmm6, %xmm4
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L7:
	movdqu	(%rdx), %xmm5
	addq	%rax, %rsi
	subq	%rax, %r13
	movdqa	%xmm2, %xmm7
	leaq	-16+pshufb_shf_table(%rip), %rdx
	movdqa	%xmm1, %xmm8
	movups	%xmm5, (%rbx)
	addq	%rax, %rbx
	salq	$4, %rax
	movdqa	(%rdx,%rax), %xmm0
	movdqa	%xmm0, %xmm6
	pshufb	%xmm0, %xmm7
	pxor	.LC1(%rip), %xmm0
	pshufb	%xmm6, %xmm8
	pshufb	%xmm6, %xmm5
	pshufb	%xmm0, %xmm2
	pshufb	%xmm0, %xmm1
	movdqa	%xmm5, %xmm10
	por	%xmm8, %xmm2
	movdqa	%xmm4, %xmm8
	pshufb	%xmm0, %xmm4
	pshufb	%xmm6, %xmm8
	por	%xmm8, %xmm1
	movdqa	%xmm3, %xmm8
	pshufb	%xmm0, %xmm3
	movdqa	.LC2(%rip), %xmm0
	pshufb	%xmm6, %xmm8
	por	%xmm5, %xmm3
	movdqa	%xmm7, %xmm6
	pclmulqdq	$1, %xmm0, %xmm7
	pclmulqdq	$16, %xmm0, %xmm6
	por	%xmm8, %xmm4
	xorps	%xmm7, %xmm3
	xorps	%xmm6, %xmm3
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L29:
	movdqa	.LC2(%rip), %xmm0
	movdqa	%xmm1, %xmm6
	movdqa	%xmm4, %xmm7
	movdqa	(%rsi), %xmm9
	movdqa	16(%rsi), %xmm8
	movdqa	32(%rsi), %xmm5
	pclmulqdq	$16, %xmm0, %xmm1
	pclmulqdq	$1, %xmm0, %xmm6
	pclmulqdq	$1, %xmm0, %xmm7
	movups	%xmm9, (%rdx)
	xorps	%xmm1, %xmm6
	movdqa	%xmm2, %xmm1
	pclmulqdq	$16, %xmm0, %xmm4
	movups	%xmm5, 32(%rdx)
	pclmulqdq	$16, %xmm0, %xmm2
	pclmulqdq	$1, %xmm0, %xmm1
	xorps	%xmm7, %xmm4
	movups	%xmm8, 16(%rdx)
	xorps	%xmm2, %xmm1
	pxor	%xmm8, %xmm6
	pxor	%xmm4, %xmm5
	pxor	%xmm9, %xmm1
	movdqa	%xmm3, %xmm2
	subq	$48, %r13
	je	.L12
	leaq	48(%rdx), %rbx
	movdqa	%xmm6, %xmm4
	movdqa	%xmm5, %xmm3
	addq	$48, %rsi
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L14:
	testq	%r13, %r13
	je	.L20
	movq	%rdx, %rbx
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L18:
	movdqa	%xmm3, %xmm1
	movdqa	%xmm4, %xmm2
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L16:
	movq	%rbx, %rdx
	jmp	.L9
.L19:
	movdqa	%xmm1, %xmm2
	movdqa	%xmm3, %xmm6
	movdqa	%xmm4, %xmm1
	jmp	.L12
.L20:
	movdqa	%xmm3, %xmm5
	movdqa	%xmm4, %xmm6
	jmp	.L12
.L28:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5316:
	.size	crc_fold_copy, .-crc_fold_copy
	.p2align 4
	.globl	crc_fold_512to32
	.hidden	crc_fold_512to32
	.type	crc_fold_512to32, @function
crc_fold_512to32:
.LFB5317:
	.cfi_startproc
	endbr64
	movdqu	80(%rdi), %xmm0
	movdqa	crc_k(%rip), %xmm3
	movdqu	96(%rdi), %xmm5
	movdqu	112(%rdi), %xmm6
	movdqa	%xmm0, %xmm2
	pclmulqdq	$1, %xmm3, %xmm0
	pclmulqdq	$16, %xmm3, %xmm2
	pxor	%xmm2, %xmm0
	pxor	%xmm5, %xmm0
	movdqa	%xmm0, %xmm1
	movdqa	%xmm0, %xmm2
	movdqu	128(%rdi), %xmm0
	pclmulqdq	$16, %xmm3, %xmm1
	pclmulqdq	$1, %xmm3, %xmm2
	pxor	%xmm1, %xmm2
	pxor	%xmm6, %xmm2
	movdqa	%xmm2, %xmm1
	pclmulqdq	$1, %xmm3, %xmm2
	pclmulqdq	$16, %xmm3, %xmm1
	pxor	%xmm1, %xmm2
	movdqa	crc_mask2(%rip), %xmm1
	pxor	%xmm0, %xmm2
	movdqa	%xmm2, %xmm0
	psrldq	$8, %xmm2
	pclmulqdq	$0, 16+crc_k(%rip), %xmm0
	pxor	%xmm0, %xmm2
	movdqa	%xmm2, %xmm4
	pslldq	$4, %xmm4
	movdqa	%xmm4, %xmm0
	pclmulqdq	$16, 16+crc_k(%rip), %xmm0
	pxor	%xmm2, %xmm0
	movdqa	crc_mask(%rip), %xmm2
	pand	%xmm0, %xmm1
	movdqa	%xmm1, %xmm0
	pclmulqdq	$0, 32+crc_k(%rip), %xmm0
	pxor	%xmm1, %xmm0
	pand	%xmm0, %xmm2
	movdqa	%xmm2, %xmm0
	pclmulqdq	$16, 32+crc_k(%rip), %xmm0
	pxor	%xmm1, %xmm0
	pxor	%xmm2, %xmm0
	pextrd	$2, %xmm0, %eax
	notl	%eax
	ret
	.cfi_endproc
.LFE5317:
	.size	crc_fold_512to32, .-crc_fold_512to32
	.section	.rodata
	.align 16
	.type	crc_mask2, @object
	.size	crc_mask2, 16
crc_mask2:
	.long	0
	.long	-1
	.long	-1
	.long	-1
	.align 16
	.type	crc_mask, @object
	.size	crc_mask, 16
crc_mask:
	.long	-1
	.long	-1
	.long	0
	.long	0
	.align 16
	.type	crc_k, @object
	.size	crc_k, 48
crc_k:
	.long	-861273954
	.long	0
	.long	1964611536
	.long	1
	.long	-861273954
	.long	0
	.long	1674404132
	.long	1
	.long	-150923712
	.long	1
	.long	-613349824
	.long	1
	.align 32
	.type	pshufb_shf_table, @object
	.size	pshufb_shf_table, 240
pshufb_shf_table:
	.long	-2071756159
	.long	-2004384123
	.long	-1937012087
	.long	9408141
	.long	-2054913150
	.long	-1987541114
	.long	-1920169078
	.long	16813966
	.long	-2038070141
	.long	-1970698105
	.long	-1903326069
	.long	33620111
	.long	-2021227132
	.long	-1953855096
	.long	-1886483060
	.long	50462976
	.long	-2004384123
	.long	-1937012087
	.long	9408141
	.long	67305985
	.long	-1987541114
	.long	-1920169078
	.long	16813966
	.long	84148994
	.long	-1970698105
	.long	-1903326069
	.long	33620111
	.long	100992003
	.long	-1953855096
	.long	-1886483060
	.long	50462976
	.long	117835012
	.long	-1937012087
	.long	9408141
	.long	67305985
	.long	134678021
	.long	-1920169078
	.long	16813966
	.long	84148994
	.long	151521030
	.long	-1903326069
	.long	33620111
	.long	100992003
	.long	168364039
	.long	-1886483060
	.long	50462976
	.long	117835012
	.long	185207048
	.long	9408141
	.long	67305985
	.long	134678021
	.long	202050057
	.long	16813966
	.long	84148994
	.long	151521030
	.long	218893066
	.long	33620111
	.long	100992003
	.long	168364039
	.long	235736075
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.quad	2645828743
	.quad	0
	.align 16
.LC1:
	.quad	-9187201950435737472
	.quad	-9187201950435737472
	.align 16
.LC2:
	.quad	7631803798
	.quad	5708721108
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
