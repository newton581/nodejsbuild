	.file	"x86.c"
	.text
	.p2align 4
	.type	_x86_check_features, @function
_x86_check_features:
.LFB40:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
#APP
# 45 "../deps/zlib/x86.c" 1
	cpuid
	
# 0 "" 2
#NO_APP
	movl	%ecx, %eax
	shrl	$26, %edx
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	andl	$512, %eax
	movl	%eax, x86_cpu_enable_ssse3(%rip)
	movl	%ecx, %eax
	shrl	%ecx
	shrl	$20, %eax
	andl	%ecx, %edx
	andl	$1, %eax
	andl	%eax, %edx
	movl	%edx, x86_cpu_enable_simd(%rip)
	ret
	.cfi_endproc
.LFE40:
	.size	_x86_check_features, .-_x86_check_features
	.p2align 4
	.globl	x86_check_features
	.type	x86_check_features, @function
x86_check_features:
.LFB39:
	.cfi_startproc
	endbr64
	leaq	_x86_check_features(%rip), %rsi
	leaq	cpu_check_inited_once(%rip), %rdi
	jmp	pthread_once@PLT
	.cfi_endproc
.LFE39:
	.size	x86_check_features, .-x86_check_features
	.globl	cpu_check_inited_once
	.bss
	.align 4
	.type	cpu_check_inited_once, @object
	.size	cpu_check_inited_once, 4
cpu_check_inited_once:
	.zero	4
	.hidden	x86_cpu_enable_simd
	.globl	x86_cpu_enable_simd
	.align 4
	.type	x86_cpu_enable_simd, @object
	.size	x86_cpu_enable_simd, 4
x86_cpu_enable_simd:
	.zero	4
	.hidden	x86_cpu_enable_ssse3
	.globl	x86_cpu_enable_ssse3
	.align 4
	.type	x86_cpu_enable_ssse3, @object
	.size	x86_cpu_enable_ssse3, 4
x86_cpu_enable_ssse3:
	.zero	4
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
