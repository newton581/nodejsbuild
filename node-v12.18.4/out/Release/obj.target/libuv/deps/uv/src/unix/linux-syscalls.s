	.file	"linux-syscalls.c"
	.text
.Ltext0:
	.p2align 4
	.globl	uv__sendmmsg
	.hidden	uv__sendmmsg
	.type	uv__sendmmsg, @function
uv__sendmmsg:
.LVL0:
.LFB22:
	.file 1 "../deps/uv/src/unix/linux-syscalls.c"
	.loc 1 134 38 view -0
	.cfi_startproc
	.loc 1 134 38 is_stmt 0 view .LVU1
	endbr64
	.loc 1 136 3 is_stmt 1 view .LVU2
	.loc 1 134 38 is_stmt 0 view .LVU3
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%ecx, %r8d
	.loc 1 136 10 view .LVU4
	xorl	%eax, %eax
	movl	%edx, %ecx
.LVL1:
	.loc 1 136 10 view .LVU5
	movq	%rsi, %rdx
.LVL2:
	.loc 1 136 10 view .LVU6
	movl	%edi, %esi
.LVL3:
	.loc 1 136 10 view .LVU7
	movl	$307, %edi
.LVL4:
	.loc 1 134 38 view .LVU8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	.loc 1 136 10 view .LVU9
	call	syscall@PLT
.LVL5:
	.loc 1 140 1 view .LVU10
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22:
	.size	uv__sendmmsg, .-uv__sendmmsg
	.p2align 4
	.globl	uv__recvmmsg
	.hidden	uv__recvmmsg
	.type	uv__recvmmsg, @function
uv__recvmmsg:
.LVL6:
.LFB23:
	.loc 1 147 44 is_stmt 1 view -0
	.cfi_startproc
	.loc 1 147 44 is_stmt 0 view .LVU12
	endbr64
	.loc 1 149 3 is_stmt 1 view .LVU13
	.loc 1 147 44 is_stmt 0 view .LVU14
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	.loc 1 149 10 view .LVU15
	movq	%r8, %r9
	xorl	%eax, %eax
	movl	%ecx, %r8d
.LVL7:
	.loc 1 149 10 view .LVU16
	movl	%edx, %ecx
.LVL8:
	.loc 1 149 10 view .LVU17
	movq	%rsi, %rdx
.LVL9:
	.loc 1 149 10 view .LVU18
	movl	%edi, %esi
.LVL10:
	.loc 1 149 10 view .LVU19
	movl	$299, %edi
.LVL11:
	.loc 1 147 44 view .LVU20
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	.loc 1 149 10 view .LVU21
	call	syscall@PLT
.LVL12:
	.loc 1 153 1 view .LVU22
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE23:
	.size	uv__recvmmsg, .-uv__recvmmsg
	.p2align 4
	.globl	uv__preadv
	.hidden	uv__preadv
	.type	uv__preadv, @function
uv__preadv:
.LVL13:
.LFB24:
	.loc 1 156 81 is_stmt 1 view -0
	.cfi_startproc
	.loc 1 156 81 is_stmt 0 view .LVU24
	endbr64
	.loc 1 158 3 is_stmt 1 view .LVU25
	.loc 1 156 81 is_stmt 0 view .LVU26
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	.loc 1 158 10 view .LVU27
	movq	%rcx, %r9
	.loc 1 156 81 view .LVU28
	movq	%rcx, %r8
	.loc 1 158 10 view .LVU29
	xorl	%eax, %eax
	movl	%edx, %ecx
.LVL14:
	.loc 1 158 10 view .LVU30
	sarq	$32, %r9
	movq	%rsi, %rdx
.LVL15:
	.loc 1 158 10 view .LVU31
	movl	%edi, %esi
.LVL16:
	.loc 1 158 10 view .LVU32
	movl	$295, %edi
.LVL17:
	.loc 1 156 81 view .LVU33
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	.loc 1 162 1 view .LVU34
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	.loc 1 158 10 view .LVU35
	jmp	syscall@PLT
.LVL18:
	.loc 1 158 10 view .LVU36
	.cfi_endproc
.LFE24:
	.size	uv__preadv, .-uv__preadv
	.p2align 4
	.globl	uv__pwritev
	.hidden	uv__pwritev
	.type	uv__pwritev, @function
uv__pwritev:
.LVL19:
.LFB25:
	.loc 1 165 82 is_stmt 1 view -0
	.cfi_startproc
	.loc 1 165 82 is_stmt 0 view .LVU38
	endbr64
	.loc 1 167 3 is_stmt 1 view .LVU39
	.loc 1 165 82 is_stmt 0 view .LVU40
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	.loc 1 167 10 view .LVU41
	movq	%rcx, %r9
	.loc 1 165 82 view .LVU42
	movq	%rcx, %r8
	.loc 1 167 10 view .LVU43
	xorl	%eax, %eax
	movl	%edx, %ecx
.LVL20:
	.loc 1 167 10 view .LVU44
	sarq	$32, %r9
	movq	%rsi, %rdx
.LVL21:
	.loc 1 167 10 view .LVU45
	movl	%edi, %esi
.LVL22:
	.loc 1 167 10 view .LVU46
	movl	$296, %edi
.LVL23:
	.loc 1 165 82 view .LVU47
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	.loc 1 171 1 view .LVU48
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	.loc 1 167 10 view .LVU49
	jmp	syscall@PLT
.LVL24:
	.loc 1 167 10 view .LVU50
	.cfi_endproc
.LFE25:
	.size	uv__pwritev, .-uv__pwritev
	.p2align 4
	.globl	uv__dup3
	.hidden	uv__dup3
	.type	uv__dup3, @function
uv__dup3:
.LVL25:
.LFB26:
	.loc 1 174 47 is_stmt 1 view -0
	.cfi_startproc
	.loc 1 174 47 is_stmt 0 view .LVU52
	endbr64
	.loc 1 176 3 is_stmt 1 view .LVU53
	.loc 1 174 47 is_stmt 0 view .LVU54
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %ecx
	.loc 1 176 10 view .LVU55
	xorl	%eax, %eax
	movl	%esi, %edx
.LVL26:
	.loc 1 176 10 view .LVU56
	movl	%edi, %esi
.LVL27:
	.loc 1 176 10 view .LVU57
	movl	$292, %edi
.LVL28:
	.loc 1 174 47 view .LVU58
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	.loc 1 176 10 view .LVU59
	call	syscall@PLT
.LVL29:
	.loc 1 180 1 view .LVU60
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE26:
	.size	uv__dup3, .-uv__dup3
	.p2align 4
	.globl	uv__statx
	.hidden	uv__statx
	.type	uv__statx, @function
uv__statx:
.LVL30:
.LFB27:
	.loc 1 187 43 is_stmt 1 view -0
	.cfi_startproc
	.loc 1 187 43 is_stmt 0 view .LVU62
	endbr64
	.loc 1 192 3 is_stmt 1 view .LVU63
	.loc 1 187 43 is_stmt 0 view .LVU64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	.loc 1 192 10 view .LVU65
	movq	%r8, %r9
	xorl	%eax, %eax
	movl	%ecx, %r8d
.LVL31:
	.loc 1 192 10 view .LVU66
	movl	%edx, %ecx
.LVL32:
	.loc 1 192 10 view .LVU67
	movq	%rsi, %rdx
.LVL33:
	.loc 1 192 10 view .LVU68
	movl	%edi, %esi
.LVL34:
	.loc 1 192 10 view .LVU69
	movl	$332, %edi
.LVL35:
	.loc 1 187 43 view .LVU70
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	.loc 1 192 10 view .LVU71
	call	syscall@PLT
.LVL36:
	.loc 1 196 1 view .LVU72
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE27:
	.size	uv__statx, .-uv__statx
	.p2align 4
	.globl	uv__getrandom
	.hidden	uv__getrandom
	.type	uv__getrandom, @function
uv__getrandom:
.LVL37:
.LFB28:
	.loc 1 199 65 is_stmt 1 view -0
	.cfi_startproc
	.loc 1 199 65 is_stmt 0 view .LVU74
	endbr64
	.loc 1 201 3 is_stmt 1 view .LVU75
	.loc 1 199 65 is_stmt 0 view .LVU76
	movl	%edx, %ecx
	.loc 1 201 10 view .LVU77
	xorl	%eax, %eax
	movq	%rsi, %rdx
.LVL38:
	.loc 1 201 10 view .LVU78
	movq	%rdi, %rsi
.LVL39:
	.loc 1 201 10 view .LVU79
	movl	$318, %edi
.LVL40:
	.loc 1 201 10 view .LVU80
	jmp	syscall@PLT
.LVL41:
	.loc 1 201 10 view .LVU81
	.cfi_endproc
.LFE28:
	.size	uv__getrandom, .-uv__getrandom
.Letext0:
	.file 2 "/usr/include/x86_64-linux-gnu/bits/types.h"
	.file 3 "/usr/include/x86_64-linux-gnu/bits/stdint-intn.h"
	.file 4 "/usr/include/x86_64-linux-gnu/bits/stdint-uintn.h"
	.file 5 "/usr/include/x86_64-linux-gnu/bits/types/struct_timespec.h"
	.file 6 "/usr/include/signal.h"
	.file 7 "/usr/lib/gcc/x86_64-linux-gnu/9/include/stddef.h"
	.file 8 "/usr/include/x86_64-linux-gnu/sys/types.h"
	.file 9 "/usr/include/x86_64-linux-gnu/bits/types/struct_iovec.h"
	.file 10 "/usr/include/x86_64-linux-gnu/bits/sockaddr.h"
	.file 11 "/usr/include/x86_64-linux-gnu/bits/socket.h"
	.file 12 "../deps/uv/src/unix/linux-syscalls.h"
	.file 13 "/usr/include/unistd.h"
	.file 14 "/usr/include/x86_64-linux-gnu/bits/getopt_core.h"
	.file 15 "/usr/include/errno.h"
	.section	.debug_info,"",@progbits
.Ldebug_info0:
	.long	0xa29
	.value	0x4
	.long	.Ldebug_abbrev0
	.byte	0x8
	.uleb128 0x1
	.long	.LASF100
	.byte	0x1
	.long	.LASF101
	.long	.LASF102
	.quad	.Ltext0
	.quad	.Letext0-.Ltext0
	.long	.Ldebug_line0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.long	.LASF0
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.long	.LASF1
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.long	.LASF2
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.long	.LASF3
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.long	.LASF4
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.long	.LASF5
	.uleb128 0x3
	.long	.LASF6
	.byte	0x2
	.byte	0x28
	.byte	0x1c
	.long	0x34
	.uleb128 0x3
	.long	.LASF7
	.byte	0x2
	.byte	0x29
	.byte	0x14
	.long	0x6f
	.uleb128 0x4
	.byte	0x4
	.byte	0x5
	.string	"int"
	.uleb128 0x3
	.long	.LASF8
	.byte	0x2
	.byte	0x2a
	.byte	0x16
	.long	0x3b
	.uleb128 0x3
	.long	.LASF9
	.byte	0x2
	.byte	0x2c
	.byte	0x19
	.long	0x8e
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.long	.LASF10
	.uleb128 0x3
	.long	.LASF11
	.byte	0x2
	.byte	0x2d
	.byte	0x1b
	.long	0x42
	.uleb128 0x3
	.long	.LASF12
	.byte	0x2
	.byte	0xa0
	.byte	0x12
	.long	0x8e
	.uleb128 0x5
	.byte	0x8
	.uleb128 0x3
	.long	.LASF13
	.byte	0x2
	.byte	0xc1
	.byte	0x12
	.long	0x8e
	.uleb128 0x3
	.long	.LASF14
	.byte	0x2
	.byte	0xc4
	.byte	0x12
	.long	0x8e
	.uleb128 0x6
	.byte	0x8
	.long	0xcd
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.long	.LASF15
	.uleb128 0x7
	.long	0xcd
	.uleb128 0x3
	.long	.LASF16
	.byte	0x3
	.byte	0x1a
	.byte	0x13
	.long	0x63
	.uleb128 0x3
	.long	.LASF17
	.byte	0x3
	.byte	0x1b
	.byte	0x13
	.long	0x82
	.uleb128 0x3
	.long	.LASF18
	.byte	0x4
	.byte	0x19
	.byte	0x14
	.long	0x57
	.uleb128 0x3
	.long	.LASF19
	.byte	0x4
	.byte	0x1a
	.byte	0x14
	.long	0x76
	.uleb128 0x3
	.long	.LASF20
	.byte	0x4
	.byte	0x1b
	.byte	0x14
	.long	0x95
	.uleb128 0x8
	.long	.LASF29
	.byte	0x10
	.byte	0x5
	.byte	0xa
	.byte	0x8
	.long	0x13d
	.uleb128 0x9
	.long	.LASF21
	.byte	0x5
	.byte	0xc
	.byte	0xc
	.long	0xa1
	.byte	0
	.uleb128 0x9
	.long	.LASF22
	.byte	0x5
	.byte	0x10
	.byte	0x15
	.long	0xbb
	.byte	0x8
	.byte	0
	.uleb128 0xa
	.long	0x158
	.long	0x14d
	.uleb128 0xb
	.long	0x42
	.byte	0x40
	.byte	0
	.uleb128 0x7
	.long	0x13d
	.uleb128 0x6
	.byte	0x8
	.long	0xd4
	.uleb128 0x7
	.long	0x152
	.uleb128 0xc
	.long	.LASF23
	.byte	0x6
	.value	0x11e
	.byte	0x1a
	.long	0x14d
	.uleb128 0xc
	.long	.LASF24
	.byte	0x6
	.value	0x11f
	.byte	0x1a
	.long	0x14d
	.uleb128 0x3
	.long	.LASF25
	.byte	0x7
	.byte	0xd1
	.byte	0x1b
	.long	0x42
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.long	.LASF26
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.long	.LASF27
	.uleb128 0x3
	.long	.LASF28
	.byte	0x8
	.byte	0x6c
	.byte	0x13
	.long	0xaf
	.uleb128 0x8
	.long	.LASF30
	.byte	0x10
	.byte	0x9
	.byte	0x1a
	.byte	0x8
	.long	0x1c5
	.uleb128 0x9
	.long	.LASF31
	.byte	0x9
	.byte	0x1c
	.byte	0xb
	.long	0xad
	.byte	0
	.uleb128 0x9
	.long	.LASF32
	.byte	0x9
	.byte	0x1d
	.byte	0xc
	.long	0x177
	.byte	0x8
	.byte	0
	.uleb128 0x7
	.long	0x19d
	.uleb128 0x3
	.long	.LASF33
	.byte	0xa
	.byte	0x1c
	.byte	0x1c
	.long	0x34
	.uleb128 0x8
	.long	.LASF34
	.byte	0x10
	.byte	0xb
	.byte	0xb2
	.byte	0x8
	.long	0x1fe
	.uleb128 0x9
	.long	.LASF35
	.byte	0xb
	.byte	0xb4
	.byte	0x11
	.long	0x1ca
	.byte	0
	.uleb128 0x9
	.long	.LASF36
	.byte	0xb
	.byte	0xb5
	.byte	0xa
	.long	0x203
	.byte	0x2
	.byte	0
	.uleb128 0x7
	.long	0x1d6
	.uleb128 0xa
	.long	0xcd
	.long	0x213
	.uleb128 0xb
	.long	0x42
	.byte	0xd
	.byte	0
	.uleb128 0x6
	.byte	0x8
	.long	0x1d6
	.uleb128 0xd
	.long	0x213
	.uleb128 0xe
	.long	.LASF37
	.uleb128 0x7
	.long	0x21e
	.uleb128 0x6
	.byte	0x8
	.long	0x21e
	.uleb128 0xd
	.long	0x228
	.uleb128 0xe
	.long	.LASF38
	.uleb128 0x7
	.long	0x233
	.uleb128 0x6
	.byte	0x8
	.long	0x233
	.uleb128 0xd
	.long	0x23d
	.uleb128 0xe
	.long	.LASF39
	.uleb128 0x7
	.long	0x248
	.uleb128 0x6
	.byte	0x8
	.long	0x248
	.uleb128 0xd
	.long	0x252
	.uleb128 0xe
	.long	.LASF40
	.uleb128 0x7
	.long	0x25d
	.uleb128 0x6
	.byte	0x8
	.long	0x25d
	.uleb128 0xd
	.long	0x267
	.uleb128 0xe
	.long	.LASF41
	.uleb128 0x7
	.long	0x272
	.uleb128 0x6
	.byte	0x8
	.long	0x272
	.uleb128 0xd
	.long	0x27c
	.uleb128 0xe
	.long	.LASF42
	.uleb128 0x7
	.long	0x287
	.uleb128 0x6
	.byte	0x8
	.long	0x287
	.uleb128 0xd
	.long	0x291
	.uleb128 0xe
	.long	.LASF43
	.uleb128 0x7
	.long	0x29c
	.uleb128 0x6
	.byte	0x8
	.long	0x29c
	.uleb128 0xd
	.long	0x2a6
	.uleb128 0xe
	.long	.LASF44
	.uleb128 0x7
	.long	0x2b1
	.uleb128 0x6
	.byte	0x8
	.long	0x2b1
	.uleb128 0xd
	.long	0x2bb
	.uleb128 0xe
	.long	.LASF45
	.uleb128 0x7
	.long	0x2c6
	.uleb128 0x6
	.byte	0x8
	.long	0x2c6
	.uleb128 0xd
	.long	0x2d0
	.uleb128 0xe
	.long	.LASF46
	.uleb128 0x7
	.long	0x2db
	.uleb128 0x6
	.byte	0x8
	.long	0x2db
	.uleb128 0xd
	.long	0x2e5
	.uleb128 0xe
	.long	.LASF47
	.uleb128 0x7
	.long	0x2f0
	.uleb128 0x6
	.byte	0x8
	.long	0x2f0
	.uleb128 0xd
	.long	0x2fa
	.uleb128 0xe
	.long	.LASF48
	.uleb128 0x7
	.long	0x305
	.uleb128 0x6
	.byte	0x8
	.long	0x305
	.uleb128 0xd
	.long	0x30f
	.uleb128 0x6
	.byte	0x8
	.long	0x1fe
	.uleb128 0xd
	.long	0x31a
	.uleb128 0x6
	.byte	0x8
	.long	0x223
	.uleb128 0xd
	.long	0x325
	.uleb128 0x6
	.byte	0x8
	.long	0x238
	.uleb128 0xd
	.long	0x330
	.uleb128 0x6
	.byte	0x8
	.long	0x24d
	.uleb128 0xd
	.long	0x33b
	.uleb128 0x6
	.byte	0x8
	.long	0x262
	.uleb128 0xd
	.long	0x346
	.uleb128 0x6
	.byte	0x8
	.long	0x277
	.uleb128 0xd
	.long	0x351
	.uleb128 0x6
	.byte	0x8
	.long	0x28c
	.uleb128 0xd
	.long	0x35c
	.uleb128 0x6
	.byte	0x8
	.long	0x2a1
	.uleb128 0xd
	.long	0x367
	.uleb128 0x6
	.byte	0x8
	.long	0x2b6
	.uleb128 0xd
	.long	0x372
	.uleb128 0x6
	.byte	0x8
	.long	0x2cb
	.uleb128 0xd
	.long	0x37d
	.uleb128 0x6
	.byte	0x8
	.long	0x2e0
	.uleb128 0xd
	.long	0x388
	.uleb128 0x6
	.byte	0x8
	.long	0x2f5
	.uleb128 0xd
	.long	0x393
	.uleb128 0x6
	.byte	0x8
	.long	0x30a
	.uleb128 0xd
	.long	0x39e
	.uleb128 0x8
	.long	.LASF49
	.byte	0x10
	.byte	0xc
	.byte	0x22
	.byte	0x8
	.long	0x3de
	.uleb128 0x9
	.long	.LASF21
	.byte	0xc
	.byte	0x23
	.byte	0xb
	.long	0xe5
	.byte	0
	.uleb128 0x9
	.long	.LASF22
	.byte	0xc
	.byte	0x24
	.byte	0xc
	.long	0xfd
	.byte	0x8
	.uleb128 0x9
	.long	.LASF50
	.byte	0xc
	.byte	0x25
	.byte	0xb
	.long	0xd9
	.byte	0xc
	.byte	0
	.uleb128 0xf
	.long	.LASF51
	.value	0x100
	.byte	0xc
	.byte	0x28
	.byte	0x8
	.long	0x4fe
	.uleb128 0x9
	.long	.LASF52
	.byte	0xc
	.byte	0x29
	.byte	0xc
	.long	0xfd
	.byte	0
	.uleb128 0x9
	.long	.LASF53
	.byte	0xc
	.byte	0x2a
	.byte	0xc
	.long	0xfd
	.byte	0x4
	.uleb128 0x9
	.long	.LASF54
	.byte	0xc
	.byte	0x2b
	.byte	0xc
	.long	0x109
	.byte	0x8
	.uleb128 0x9
	.long	.LASF55
	.byte	0xc
	.byte	0x2c
	.byte	0xc
	.long	0xfd
	.byte	0x10
	.uleb128 0x9
	.long	.LASF56
	.byte	0xc
	.byte	0x2d
	.byte	0xc
	.long	0xfd
	.byte	0x14
	.uleb128 0x9
	.long	.LASF57
	.byte	0xc
	.byte	0x2e
	.byte	0xc
	.long	0xfd
	.byte	0x18
	.uleb128 0x9
	.long	.LASF58
	.byte	0xc
	.byte	0x2f
	.byte	0xc
	.long	0xf1
	.byte	0x1c
	.uleb128 0x9
	.long	.LASF50
	.byte	0xc
	.byte	0x30
	.byte	0xc
	.long	0xf1
	.byte	0x1e
	.uleb128 0x9
	.long	.LASF59
	.byte	0xc
	.byte	0x31
	.byte	0xc
	.long	0x109
	.byte	0x20
	.uleb128 0x9
	.long	.LASF60
	.byte	0xc
	.byte	0x32
	.byte	0xc
	.long	0x109
	.byte	0x28
	.uleb128 0x9
	.long	.LASF61
	.byte	0xc
	.byte	0x33
	.byte	0xc
	.long	0x109
	.byte	0x30
	.uleb128 0x9
	.long	.LASF62
	.byte	0xc
	.byte	0x34
	.byte	0xc
	.long	0x109
	.byte	0x38
	.uleb128 0x9
	.long	.LASF63
	.byte	0xc
	.byte	0x35
	.byte	0x1e
	.long	0x3a9
	.byte	0x40
	.uleb128 0x9
	.long	.LASF64
	.byte	0xc
	.byte	0x36
	.byte	0x1e
	.long	0x3a9
	.byte	0x50
	.uleb128 0x9
	.long	.LASF65
	.byte	0xc
	.byte	0x37
	.byte	0x1e
	.long	0x3a9
	.byte	0x60
	.uleb128 0x9
	.long	.LASF66
	.byte	0xc
	.byte	0x38
	.byte	0x1e
	.long	0x3a9
	.byte	0x70
	.uleb128 0x9
	.long	.LASF67
	.byte	0xc
	.byte	0x39
	.byte	0xc
	.long	0xfd
	.byte	0x80
	.uleb128 0x9
	.long	.LASF68
	.byte	0xc
	.byte	0x3a
	.byte	0xc
	.long	0xfd
	.byte	0x84
	.uleb128 0x9
	.long	.LASF69
	.byte	0xc
	.byte	0x3b
	.byte	0xc
	.long	0xfd
	.byte	0x88
	.uleb128 0x9
	.long	.LASF70
	.byte	0xc
	.byte	0x3c
	.byte	0xc
	.long	0xfd
	.byte	0x8c
	.uleb128 0x9
	.long	.LASF71
	.byte	0xc
	.byte	0x3d
	.byte	0xc
	.long	0x4fe
	.byte	0x90
	.byte	0
	.uleb128 0xa
	.long	0x109
	.long	0x50e
	.uleb128 0xb
	.long	0x42
	.byte	0xd
	.byte	0
	.uleb128 0xc
	.long	.LASF72
	.byte	0xd
	.value	0x21f
	.byte	0xf
	.long	0x51b
	.uleb128 0x6
	.byte	0x8
	.long	0xc7
	.uleb128 0xc
	.long	.LASF73
	.byte	0xd
	.value	0x221
	.byte	0xf
	.long	0x51b
	.uleb128 0x10
	.long	.LASF74
	.byte	0xe
	.byte	0x24
	.byte	0xe
	.long	0xc7
	.uleb128 0x10
	.long	.LASF75
	.byte	0xe
	.byte	0x32
	.byte	0xc
	.long	0x6f
	.uleb128 0x10
	.long	.LASF76
	.byte	0xe
	.byte	0x37
	.byte	0xc
	.long	0x6f
	.uleb128 0x10
	.long	.LASF77
	.byte	0xe
	.byte	0x3b
	.byte	0xc
	.long	0x6f
	.uleb128 0x10
	.long	.LASF78
	.byte	0xf
	.byte	0x2d
	.byte	0xe
	.long	0xc7
	.uleb128 0x10
	.long	.LASF79
	.byte	0xf
	.byte	0x2e
	.byte	0xe
	.long	0xc7
	.uleb128 0x11
	.long	.LASF82
	.byte	0x1
	.byte	0xc7
	.byte	0x9
	.long	0x191
	.quad	.LFB28
	.quad	.LFE28-.LFB28
	.uleb128 0x1
	.byte	0x9c
	.long	0x5ff
	.uleb128 0x12
	.string	"buf"
	.byte	0x1
	.byte	0xc7
	.byte	0x1d
	.long	0xad
	.long	.LLST25
	.long	.LVUS25
	.uleb128 0x13
	.long	.LASF80
	.byte	0x1
	.byte	0xc7
	.byte	0x29
	.long	0x177
	.long	.LLST26
	.long	.LVUS26
	.uleb128 0x13
	.long	.LASF81
	.byte	0x1
	.byte	0xc7
	.byte	0x3a
	.long	0x3b
	.long	.LLST27
	.long	.LVUS27
	.uleb128 0x14
	.quad	.LVL41
	.long	0xa1f
	.uleb128 0x15
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x3
	.byte	0xa
	.value	0x13e
	.uleb128 0x15
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x3
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x15
	.uleb128 0x1
	.byte	0x51
	.uleb128 0x3
	.byte	0xf3
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x15
	.uleb128 0x1
	.byte	0x52
	.uleb128 0x3
	.byte	0xf3
	.uleb128 0x1
	.byte	0x51
	.byte	0
	.byte	0
	.uleb128 0x11
	.long	.LASF51
	.byte	0x1
	.byte	0xb7
	.byte	0x5
	.long	0x6f
	.quad	.LFB27
	.quad	.LFE27-.LFB27
	.uleb128 0x1
	.byte	0x9c
	.long	0x6be
	.uleb128 0x13
	.long	.LASF83
	.byte	0x1
	.byte	0xb7
	.byte	0x13
	.long	0x6f
	.long	.LLST20
	.long	.LVUS20
	.uleb128 0x13
	.long	.LASF84
	.byte	0x1
	.byte	0xb8
	.byte	0x1b
	.long	0x152
	.long	.LLST21
	.long	.LVUS21
	.uleb128 0x13
	.long	.LASF81
	.byte	0x1
	.byte	0xb9
	.byte	0x13
	.long	0x6f
	.long	.LLST22
	.long	.LVUS22
	.uleb128 0x13
	.long	.LASF85
	.byte	0x1
	.byte	0xba
	.byte	0x1c
	.long	0x3b
	.long	.LLST23
	.long	.LVUS23
	.uleb128 0x13
	.long	.LASF86
	.byte	0x1
	.byte	0xbb
	.byte	0x21
	.long	0x6be
	.long	.LLST24
	.long	.LVUS24
	.uleb128 0x16
	.quad	.LVL36
	.long	0xa1f
	.uleb128 0x15
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x3
	.byte	0xa
	.value	0x14c
	.uleb128 0x15
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x3
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x15
	.uleb128 0x1
	.byte	0x51
	.uleb128 0x3
	.byte	0xf3
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x15
	.uleb128 0x1
	.byte	0x52
	.uleb128 0x3
	.byte	0xf3
	.uleb128 0x1
	.byte	0x51
	.uleb128 0x15
	.uleb128 0x1
	.byte	0x58
	.uleb128 0x3
	.byte	0xf3
	.uleb128 0x1
	.byte	0x52
	.uleb128 0x15
	.uleb128 0x1
	.byte	0x59
	.uleb128 0x3
	.byte	0xf3
	.uleb128 0x1
	.byte	0x58
	.byte	0
	.byte	0
	.uleb128 0x6
	.byte	0x8
	.long	0x3de
	.uleb128 0x11
	.long	.LASF87
	.byte	0x1
	.byte	0xae
	.byte	0x5
	.long	0x6f
	.quad	.LFB26
	.quad	.LFE26-.LFB26
	.uleb128 0x1
	.byte	0x9c
	.long	0x74d
	.uleb128 0x13
	.long	.LASF88
	.byte	0x1
	.byte	0xae
	.byte	0x12
	.long	0x6f
	.long	.LLST17
	.long	.LVUS17
	.uleb128 0x13
	.long	.LASF89
	.byte	0x1
	.byte	0xae
	.byte	0x1d
	.long	0x6f
	.long	.LLST18
	.long	.LVUS18
	.uleb128 0x13
	.long	.LASF81
	.byte	0x1
	.byte	0xae
	.byte	0x28
	.long	0x6f
	.long	.LLST19
	.long	.LVUS19
	.uleb128 0x16
	.quad	.LVL29
	.long	0xa1f
	.uleb128 0x15
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x3
	.byte	0xa
	.value	0x124
	.uleb128 0x15
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x3
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x15
	.uleb128 0x1
	.byte	0x51
	.uleb128 0x3
	.byte	0xf3
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x15
	.uleb128 0x1
	.byte	0x52
	.uleb128 0x3
	.byte	0xf3
	.uleb128 0x1
	.byte	0x51
	.byte	0
	.byte	0
	.uleb128 0x11
	.long	.LASF90
	.byte	0x1
	.byte	0xa5
	.byte	0x9
	.long	0x191
	.quad	.LFB25
	.quad	.LFE25-.LFB25
	.uleb128 0x1
	.byte	0x9c
	.long	0x7fa
	.uleb128 0x12
	.string	"fd"
	.byte	0x1
	.byte	0xa5
	.byte	0x19
	.long	0x6f
	.long	.LLST13
	.long	.LVUS13
	.uleb128 0x12
	.string	"iov"
	.byte	0x1
	.byte	0xa5
	.byte	0x31
	.long	0x7fa
	.long	.LLST14
	.long	.LVUS14
	.uleb128 0x13
	.long	.LASF91
	.byte	0x1
	.byte	0xa5
	.byte	0x3a
	.long	0x6f
	.long	.LLST15
	.long	.LVUS15
	.uleb128 0x13
	.long	.LASF92
	.byte	0x1
	.byte	0xa5
	.byte	0x4a
	.long	0xe5
	.long	.LLST16
	.long	.LVUS16
	.uleb128 0x14
	.quad	.LVL24
	.long	0xa1f
	.uleb128 0x15
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x3
	.byte	0xa
	.value	0x128
	.uleb128 0x15
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x3
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x15
	.uleb128 0x1
	.byte	0x51
	.uleb128 0x3
	.byte	0xf3
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x15
	.uleb128 0x1
	.byte	0x52
	.uleb128 0x3
	.byte	0xf3
	.uleb128 0x1
	.byte	0x51
	.uleb128 0x15
	.uleb128 0x1
	.byte	0x58
	.uleb128 0x3
	.byte	0xf3
	.uleb128 0x1
	.byte	0x52
	.uleb128 0x15
	.uleb128 0x1
	.byte	0x59
	.uleb128 0x6
	.byte	0xf3
	.uleb128 0x1
	.byte	0x52
	.byte	0x8
	.byte	0x20
	.byte	0x26
	.byte	0
	.byte	0
	.uleb128 0x6
	.byte	0x8
	.long	0x1c5
	.uleb128 0x11
	.long	.LASF93
	.byte	0x1
	.byte	0x9c
	.byte	0x9
	.long	0x191
	.quad	.LFB24
	.quad	.LFE24-.LFB24
	.uleb128 0x1
	.byte	0x9c
	.long	0x8ad
	.uleb128 0x12
	.string	"fd"
	.byte	0x1
	.byte	0x9c
	.byte	0x18
	.long	0x6f
	.long	.LLST9
	.long	.LVUS9
	.uleb128 0x12
	.string	"iov"
	.byte	0x1
	.byte	0x9c
	.byte	0x30
	.long	0x7fa
	.long	.LLST10
	.long	.LVUS10
	.uleb128 0x13
	.long	.LASF91
	.byte	0x1
	.byte	0x9c
	.byte	0x39
	.long	0x6f
	.long	.LLST11
	.long	.LVUS11
	.uleb128 0x13
	.long	.LASF92
	.byte	0x1
	.byte	0x9c
	.byte	0x49
	.long	0xe5
	.long	.LLST12
	.long	.LVUS12
	.uleb128 0x14
	.quad	.LVL18
	.long	0xa1f
	.uleb128 0x15
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x3
	.byte	0xa
	.value	0x127
	.uleb128 0x15
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x3
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x15
	.uleb128 0x1
	.byte	0x51
	.uleb128 0x3
	.byte	0xf3
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x15
	.uleb128 0x1
	.byte	0x52
	.uleb128 0x3
	.byte	0xf3
	.uleb128 0x1
	.byte	0x51
	.uleb128 0x15
	.uleb128 0x1
	.byte	0x58
	.uleb128 0x3
	.byte	0xf3
	.uleb128 0x1
	.byte	0x52
	.uleb128 0x15
	.uleb128 0x1
	.byte	0x59
	.uleb128 0x6
	.byte	0xf3
	.uleb128 0x1
	.byte	0x52
	.byte	0x8
	.byte	0x20
	.byte	0x26
	.byte	0
	.byte	0
	.uleb128 0x11
	.long	.LASF94
	.byte	0x1
	.byte	0x8f
	.byte	0x5
	.long	0x6f
	.quad	.LFB23
	.quad	.LFE23-.LFB23
	.uleb128 0x1
	.byte	0x9c
	.long	0x96b
	.uleb128 0x12
	.string	"fd"
	.byte	0x1
	.byte	0x8f
	.byte	0x16
	.long	0x6f
	.long	.LLST4
	.long	.LVUS4
	.uleb128 0x13
	.long	.LASF95
	.byte	0x1
	.byte	0x90
	.byte	0x26
	.long	0x970
	.long	.LLST5
	.long	.LVUS5
	.uleb128 0x13
	.long	.LASF96
	.byte	0x1
	.byte	0x91
	.byte	0x1f
	.long	0x3b
	.long	.LLST6
	.long	.LVUS6
	.uleb128 0x13
	.long	.LASF81
	.byte	0x1
	.byte	0x92
	.byte	0x1f
	.long	0x3b
	.long	.LLST7
	.long	.LVUS7
	.uleb128 0x13
	.long	.LASF97
	.byte	0x1
	.byte	0x93
	.byte	0x23
	.long	0x976
	.long	.LLST8
	.long	.LVUS8
	.uleb128 0x16
	.quad	.LVL12
	.long	0xa1f
	.uleb128 0x15
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x3
	.byte	0xa
	.value	0x12b
	.uleb128 0x15
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x3
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x15
	.uleb128 0x1
	.byte	0x51
	.uleb128 0x3
	.byte	0xf3
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x15
	.uleb128 0x1
	.byte	0x52
	.uleb128 0x3
	.byte	0xf3
	.uleb128 0x1
	.byte	0x51
	.uleb128 0x15
	.uleb128 0x1
	.byte	0x58
	.uleb128 0x3
	.byte	0xf3
	.uleb128 0x1
	.byte	0x52
	.uleb128 0x15
	.uleb128 0x1
	.byte	0x59
	.uleb128 0x3
	.byte	0xf3
	.uleb128 0x1
	.byte	0x58
	.byte	0
	.byte	0
	.uleb128 0xe
	.long	.LASF98
	.uleb128 0x6
	.byte	0x8
	.long	0x96b
	.uleb128 0x6
	.byte	0x8
	.long	0x115
	.uleb128 0x11
	.long	.LASF99
	.byte	0x1
	.byte	0x83
	.byte	0x5
	.long	0x6f
	.quad	.LFB22
	.quad	.LFE22-.LFB22
	.uleb128 0x1
	.byte	0x9c
	.long	0xa1f
	.uleb128 0x12
	.string	"fd"
	.byte	0x1
	.byte	0x83
	.byte	0x16
	.long	0x6f
	.long	.LLST0
	.long	.LVUS0
	.uleb128 0x13
	.long	.LASF95
	.byte	0x1
	.byte	0x84
	.byte	0x26
	.long	0x970
	.long	.LLST1
	.long	.LVUS1
	.uleb128 0x13
	.long	.LASF96
	.byte	0x1
	.byte	0x85
	.byte	0x1f
	.long	0x3b
	.long	.LLST2
	.long	.LVUS2
	.uleb128 0x13
	.long	.LASF81
	.byte	0x1
	.byte	0x86
	.byte	0x1f
	.long	0x3b
	.long	.LLST3
	.long	.LVUS3
	.uleb128 0x16
	.quad	.LVL5
	.long	0xa1f
	.uleb128 0x15
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x3
	.byte	0xa
	.value	0x133
	.uleb128 0x15
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x3
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x15
	.uleb128 0x1
	.byte	0x51
	.uleb128 0x3
	.byte	0xf3
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x15
	.uleb128 0x1
	.byte	0x52
	.uleb128 0x3
	.byte	0xf3
	.uleb128 0x1
	.byte	0x51
	.uleb128 0x15
	.uleb128 0x1
	.byte	0x58
	.uleb128 0x3
	.byte	0xf3
	.uleb128 0x1
	.byte	0x52
	.byte	0
	.byte	0
	.uleb128 0x17
	.long	.LASF103
	.long	.LASF103
	.byte	0xd
	.value	0x420
	.byte	0x11
	.byte	0
	.section	.debug_abbrev,"",@progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1b
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x7
	.uleb128 0x10
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x13
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x37
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x13
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3c
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x13
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0xb
	.uleb128 0x5
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x7
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2117
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x17
	.uleb128 0x2137
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x17
	.uleb128 0x2137
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0x4109
	.byte	0x1
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x2115
	.uleb128 0x19
	.uleb128 0x31
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0x410a
	.byte	0
	.uleb128 0x2
	.uleb128 0x18
	.uleb128 0x2111
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x16
	.uleb128 0x4109
	.byte	0x1
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x31
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x17
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.uleb128 0x6e
	.uleb128 0xe
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",@progbits
.Ldebug_loc0:
.LVUS25:
	.uleb128 0
	.uleb128 .LVU80
	.uleb128 .LVU80
	.uleb128 .LVU81
	.uleb128 .LVU81
	.uleb128 0
.LLST25:
	.quad	.LVL37-.Ltext0
	.quad	.LVL40-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	.LVL40-.Ltext0
	.quad	.LVL41-1-.Ltext0
	.value	0x1
	.byte	0x54
	.quad	.LVL41-1-.Ltext0
	.quad	.LFE28-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS26:
	.uleb128 0
	.uleb128 .LVU79
	.uleb128 .LVU79
	.uleb128 .LVU81
	.uleb128 .LVU81
	.uleb128 0
.LLST26:
	.quad	.LVL37-.Ltext0
	.quad	.LVL39-.Ltext0
	.value	0x1
	.byte	0x54
	.quad	.LVL39-.Ltext0
	.quad	.LVL41-1-.Ltext0
	.value	0x1
	.byte	0x51
	.quad	.LVL41-1-.Ltext0
	.quad	.LFE28-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x54
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS27:
	.uleb128 0
	.uleb128 .LVU78
	.uleb128 .LVU78
	.uleb128 .LVU81
	.uleb128 .LVU81
	.uleb128 0
.LLST27:
	.quad	.LVL37-.Ltext0
	.quad	.LVL38-.Ltext0
	.value	0x1
	.byte	0x51
	.quad	.LVL38-.Ltext0
	.quad	.LVL41-1-.Ltext0
	.value	0x1
	.byte	0x52
	.quad	.LVL41-1-.Ltext0
	.quad	.LFE28-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x51
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS20:
	.uleb128 0
	.uleb128 .LVU70
	.uleb128 .LVU70
	.uleb128 .LVU72
	.uleb128 .LVU72
	.uleb128 0
.LLST20:
	.quad	.LVL30-.Ltext0
	.quad	.LVL35-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	.LVL35-.Ltext0
	.quad	.LVL36-1-.Ltext0
	.value	0x1
	.byte	0x54
	.quad	.LVL36-1-.Ltext0
	.quad	.LFE27-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS21:
	.uleb128 0
	.uleb128 .LVU69
	.uleb128 .LVU69
	.uleb128 .LVU72
	.uleb128 .LVU72
	.uleb128 0
.LLST21:
	.quad	.LVL30-.Ltext0
	.quad	.LVL34-.Ltext0
	.value	0x1
	.byte	0x54
	.quad	.LVL34-.Ltext0
	.quad	.LVL36-1-.Ltext0
	.value	0x1
	.byte	0x51
	.quad	.LVL36-1-.Ltext0
	.quad	.LFE27-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x54
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS22:
	.uleb128 0
	.uleb128 .LVU68
	.uleb128 .LVU68
	.uleb128 .LVU72
	.uleb128 .LVU72
	.uleb128 0
.LLST22:
	.quad	.LVL30-.Ltext0
	.quad	.LVL33-.Ltext0
	.value	0x1
	.byte	0x51
	.quad	.LVL33-.Ltext0
	.quad	.LVL36-1-.Ltext0
	.value	0x1
	.byte	0x52
	.quad	.LVL36-1-.Ltext0
	.quad	.LFE27-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x51
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS23:
	.uleb128 0
	.uleb128 .LVU67
	.uleb128 .LVU67
	.uleb128 .LVU72
	.uleb128 .LVU72
	.uleb128 0
.LLST23:
	.quad	.LVL30-.Ltext0
	.quad	.LVL32-.Ltext0
	.value	0x1
	.byte	0x52
	.quad	.LVL32-.Ltext0
	.quad	.LVL36-1-.Ltext0
	.value	0x1
	.byte	0x58
	.quad	.LVL36-1-.Ltext0
	.quad	.LFE27-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x52
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS24:
	.uleb128 0
	.uleb128 .LVU66
	.uleb128 .LVU66
	.uleb128 .LVU72
	.uleb128 .LVU72
	.uleb128 0
.LLST24:
	.quad	.LVL30-.Ltext0
	.quad	.LVL31-.Ltext0
	.value	0x1
	.byte	0x58
	.quad	.LVL31-.Ltext0
	.quad	.LVL36-1-.Ltext0
	.value	0x1
	.byte	0x59
	.quad	.LVL36-1-.Ltext0
	.quad	.LFE27-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x58
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS17:
	.uleb128 0
	.uleb128 .LVU58
	.uleb128 .LVU58
	.uleb128 .LVU60
	.uleb128 .LVU60
	.uleb128 0
.LLST17:
	.quad	.LVL25-.Ltext0
	.quad	.LVL28-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	.LVL28-.Ltext0
	.quad	.LVL29-1-.Ltext0
	.value	0x1
	.byte	0x54
	.quad	.LVL29-1-.Ltext0
	.quad	.LFE26-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS18:
	.uleb128 0
	.uleb128 .LVU57
	.uleb128 .LVU57
	.uleb128 .LVU60
	.uleb128 .LVU60
	.uleb128 0
.LLST18:
	.quad	.LVL25-.Ltext0
	.quad	.LVL27-.Ltext0
	.value	0x1
	.byte	0x54
	.quad	.LVL27-.Ltext0
	.quad	.LVL29-1-.Ltext0
	.value	0x1
	.byte	0x51
	.quad	.LVL29-1-.Ltext0
	.quad	.LFE26-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x54
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS19:
	.uleb128 0
	.uleb128 .LVU56
	.uleb128 .LVU56
	.uleb128 .LVU60
	.uleb128 .LVU60
	.uleb128 0
.LLST19:
	.quad	.LVL25-.Ltext0
	.quad	.LVL26-.Ltext0
	.value	0x1
	.byte	0x51
	.quad	.LVL26-.Ltext0
	.quad	.LVL29-1-.Ltext0
	.value	0x1
	.byte	0x52
	.quad	.LVL29-1-.Ltext0
	.quad	.LFE26-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x51
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS13:
	.uleb128 0
	.uleb128 .LVU47
	.uleb128 .LVU47
	.uleb128 .LVU50
	.uleb128 .LVU50
	.uleb128 0
.LLST13:
	.quad	.LVL19-.Ltext0
	.quad	.LVL23-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	.LVL23-.Ltext0
	.quad	.LVL24-1-.Ltext0
	.value	0x1
	.byte	0x54
	.quad	.LVL24-1-.Ltext0
	.quad	.LFE25-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS14:
	.uleb128 0
	.uleb128 .LVU46
	.uleb128 .LVU46
	.uleb128 .LVU50
	.uleb128 .LVU50
	.uleb128 0
.LLST14:
	.quad	.LVL19-.Ltext0
	.quad	.LVL22-.Ltext0
	.value	0x1
	.byte	0x54
	.quad	.LVL22-.Ltext0
	.quad	.LVL24-1-.Ltext0
	.value	0x1
	.byte	0x51
	.quad	.LVL24-1-.Ltext0
	.quad	.LFE25-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x54
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS15:
	.uleb128 0
	.uleb128 .LVU45
	.uleb128 .LVU45
	.uleb128 .LVU50
	.uleb128 .LVU50
	.uleb128 0
.LLST15:
	.quad	.LVL19-.Ltext0
	.quad	.LVL21-.Ltext0
	.value	0x1
	.byte	0x51
	.quad	.LVL21-.Ltext0
	.quad	.LVL24-1-.Ltext0
	.value	0x1
	.byte	0x52
	.quad	.LVL24-1-.Ltext0
	.quad	.LFE25-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x51
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS16:
	.uleb128 0
	.uleb128 .LVU44
	.uleb128 .LVU44
	.uleb128 .LVU50
	.uleb128 .LVU50
	.uleb128 0
.LLST16:
	.quad	.LVL19-.Ltext0
	.quad	.LVL20-.Ltext0
	.value	0x1
	.byte	0x52
	.quad	.LVL20-.Ltext0
	.quad	.LVL24-1-.Ltext0
	.value	0x1
	.byte	0x58
	.quad	.LVL24-1-.Ltext0
	.quad	.LFE25-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x52
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS9:
	.uleb128 0
	.uleb128 .LVU33
	.uleb128 .LVU33
	.uleb128 .LVU36
	.uleb128 .LVU36
	.uleb128 0
.LLST9:
	.quad	.LVL13-.Ltext0
	.quad	.LVL17-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	.LVL17-.Ltext0
	.quad	.LVL18-1-.Ltext0
	.value	0x1
	.byte	0x54
	.quad	.LVL18-1-.Ltext0
	.quad	.LFE24-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS10:
	.uleb128 0
	.uleb128 .LVU32
	.uleb128 .LVU32
	.uleb128 .LVU36
	.uleb128 .LVU36
	.uleb128 0
.LLST10:
	.quad	.LVL13-.Ltext0
	.quad	.LVL16-.Ltext0
	.value	0x1
	.byte	0x54
	.quad	.LVL16-.Ltext0
	.quad	.LVL18-1-.Ltext0
	.value	0x1
	.byte	0x51
	.quad	.LVL18-1-.Ltext0
	.quad	.LFE24-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x54
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS11:
	.uleb128 0
	.uleb128 .LVU31
	.uleb128 .LVU31
	.uleb128 .LVU36
	.uleb128 .LVU36
	.uleb128 0
.LLST11:
	.quad	.LVL13-.Ltext0
	.quad	.LVL15-.Ltext0
	.value	0x1
	.byte	0x51
	.quad	.LVL15-.Ltext0
	.quad	.LVL18-1-.Ltext0
	.value	0x1
	.byte	0x52
	.quad	.LVL18-1-.Ltext0
	.quad	.LFE24-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x51
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS12:
	.uleb128 0
	.uleb128 .LVU30
	.uleb128 .LVU30
	.uleb128 .LVU36
	.uleb128 .LVU36
	.uleb128 0
.LLST12:
	.quad	.LVL13-.Ltext0
	.quad	.LVL14-.Ltext0
	.value	0x1
	.byte	0x52
	.quad	.LVL14-.Ltext0
	.quad	.LVL18-1-.Ltext0
	.value	0x1
	.byte	0x58
	.quad	.LVL18-1-.Ltext0
	.quad	.LFE24-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x52
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS4:
	.uleb128 0
	.uleb128 .LVU20
	.uleb128 .LVU20
	.uleb128 .LVU22
	.uleb128 .LVU22
	.uleb128 0
.LLST4:
	.quad	.LVL6-.Ltext0
	.quad	.LVL11-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	.LVL11-.Ltext0
	.quad	.LVL12-1-.Ltext0
	.value	0x1
	.byte	0x54
	.quad	.LVL12-1-.Ltext0
	.quad	.LFE23-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS5:
	.uleb128 0
	.uleb128 .LVU19
	.uleb128 .LVU19
	.uleb128 .LVU22
	.uleb128 .LVU22
	.uleb128 0
.LLST5:
	.quad	.LVL6-.Ltext0
	.quad	.LVL10-.Ltext0
	.value	0x1
	.byte	0x54
	.quad	.LVL10-.Ltext0
	.quad	.LVL12-1-.Ltext0
	.value	0x1
	.byte	0x51
	.quad	.LVL12-1-.Ltext0
	.quad	.LFE23-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x54
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS6:
	.uleb128 0
	.uleb128 .LVU18
	.uleb128 .LVU18
	.uleb128 .LVU22
	.uleb128 .LVU22
	.uleb128 0
.LLST6:
	.quad	.LVL6-.Ltext0
	.quad	.LVL9-.Ltext0
	.value	0x1
	.byte	0x51
	.quad	.LVL9-.Ltext0
	.quad	.LVL12-1-.Ltext0
	.value	0x1
	.byte	0x52
	.quad	.LVL12-1-.Ltext0
	.quad	.LFE23-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x51
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS7:
	.uleb128 0
	.uleb128 .LVU17
	.uleb128 .LVU17
	.uleb128 .LVU22
	.uleb128 .LVU22
	.uleb128 0
.LLST7:
	.quad	.LVL6-.Ltext0
	.quad	.LVL8-.Ltext0
	.value	0x1
	.byte	0x52
	.quad	.LVL8-.Ltext0
	.quad	.LVL12-1-.Ltext0
	.value	0x1
	.byte	0x58
	.quad	.LVL12-1-.Ltext0
	.quad	.LFE23-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x52
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS8:
	.uleb128 0
	.uleb128 .LVU16
	.uleb128 .LVU16
	.uleb128 .LVU22
	.uleb128 .LVU22
	.uleb128 0
.LLST8:
	.quad	.LVL6-.Ltext0
	.quad	.LVL7-.Ltext0
	.value	0x1
	.byte	0x58
	.quad	.LVL7-.Ltext0
	.quad	.LVL12-1-.Ltext0
	.value	0x1
	.byte	0x59
	.quad	.LVL12-1-.Ltext0
	.quad	.LFE23-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x58
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS0:
	.uleb128 0
	.uleb128 .LVU8
	.uleb128 .LVU8
	.uleb128 .LVU10
	.uleb128 .LVU10
	.uleb128 0
.LLST0:
	.quad	.LVL0-.Ltext0
	.quad	.LVL4-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	.LVL4-.Ltext0
	.quad	.LVL5-1-.Ltext0
	.value	0x1
	.byte	0x54
	.quad	.LVL5-1-.Ltext0
	.quad	.LFE22-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS1:
	.uleb128 0
	.uleb128 .LVU7
	.uleb128 .LVU7
	.uleb128 .LVU10
	.uleb128 .LVU10
	.uleb128 0
.LLST1:
	.quad	.LVL0-.Ltext0
	.quad	.LVL3-.Ltext0
	.value	0x1
	.byte	0x54
	.quad	.LVL3-.Ltext0
	.quad	.LVL5-1-.Ltext0
	.value	0x1
	.byte	0x51
	.quad	.LVL5-1-.Ltext0
	.quad	.LFE22-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x54
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS2:
	.uleb128 0
	.uleb128 .LVU6
	.uleb128 .LVU6
	.uleb128 .LVU10
	.uleb128 .LVU10
	.uleb128 0
.LLST2:
	.quad	.LVL0-.Ltext0
	.quad	.LVL2-.Ltext0
	.value	0x1
	.byte	0x51
	.quad	.LVL2-.Ltext0
	.quad	.LVL5-1-.Ltext0
	.value	0x1
	.byte	0x52
	.quad	.LVL5-1-.Ltext0
	.quad	.LFE22-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x51
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS3:
	.uleb128 0
	.uleb128 .LVU5
	.uleb128 .LVU5
	.uleb128 .LVU10
	.uleb128 .LVU10
	.uleb128 0
.LLST3:
	.quad	.LVL0-.Ltext0
	.quad	.LVL1-.Ltext0
	.value	0x1
	.byte	0x52
	.quad	.LVL1-.Ltext0
	.quad	.LVL5-1-.Ltext0
	.value	0x1
	.byte	0x58
	.quad	.LVL5-1-.Ltext0
	.quad	.LFE22-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x52
	.byte	0x9f
	.quad	0
	.quad	0
	.section	.debug_aranges,"",@progbits
	.long	0x2c
	.value	0x2
	.long	.Ldebug_info0
	.byte	0x8
	.byte	0
	.value	0
	.value	0
	.quad	.Ltext0
	.quad	.Letext0-.Ltext0
	.quad	0
	.quad	0
	.section	.debug_line,"",@progbits
.Ldebug_line0:
	.section	.debug_str,"MS",@progbits,1
.LASF75:
	.string	"optind"
.LASF74:
	.string	"optarg"
.LASF80:
	.string	"buflen"
.LASF54:
	.string	"stx_attributes"
.LASF73:
	.string	"environ"
.LASF11:
	.string	"__uint64_t"
.LASF87:
	.string	"uv__dup3"
.LASF21:
	.string	"tv_sec"
.LASF20:
	.string	"uint64_t"
.LASF59:
	.string	"stx_ino"
.LASF45:
	.string	"sockaddr_iso"
.LASF25:
	.string	"size_t"
.LASF60:
	.string	"stx_size"
.LASF102:
	.string	"/workspace/nodejsbuild/node-v12.18.4/out"
.LASF43:
	.string	"sockaddr_inarp"
.LASF32:
	.string	"iov_len"
.LASF58:
	.string	"stx_mode"
.LASF26:
	.string	"long long int"
.LASF14:
	.string	"__syscall_slong_t"
.LASF47:
	.string	"sockaddr_un"
.LASF6:
	.string	"__uint16_t"
.LASF28:
	.string	"ssize_t"
.LASF72:
	.string	"__environ"
.LASF65:
	.string	"stx_ctime"
.LASF17:
	.string	"int64_t"
.LASF24:
	.string	"sys_siglist"
.LASF56:
	.string	"stx_uid"
.LASF55:
	.string	"stx_nlink"
.LASF33:
	.string	"sa_family_t"
.LASF5:
	.string	"short int"
.LASF53:
	.string	"stx_blksize"
.LASF46:
	.string	"sockaddr_ns"
.LASF44:
	.string	"sockaddr_ipx"
.LASF101:
	.string	"../deps/uv/src/unix/linux-syscalls.c"
.LASF13:
	.string	"__ssize_t"
.LASF10:
	.string	"long int"
.LASF9:
	.string	"__int64_t"
.LASF92:
	.string	"offset"
.LASF95:
	.string	"mmsg"
.LASF61:
	.string	"stx_blocks"
.LASF82:
	.string	"uv__getrandom"
.LASF68:
	.string	"stx_rdev_minor"
.LASF78:
	.string	"program_invocation_name"
.LASF0:
	.string	"unsigned char"
.LASF30:
	.string	"iovec"
.LASF63:
	.string	"stx_atime"
.LASF94:
	.string	"uv__recvmmsg"
.LASF22:
	.string	"tv_nsec"
.LASF4:
	.string	"signed char"
.LASF81:
	.string	"flags"
.LASF27:
	.string	"long long unsigned int"
.LASF42:
	.string	"sockaddr_in6"
.LASF19:
	.string	"uint32_t"
.LASF86:
	.string	"statxbuf"
.LASF31:
	.string	"iov_base"
.LASF2:
	.string	"unsigned int"
.LASF18:
	.string	"uint16_t"
.LASF76:
	.string	"opterr"
.LASF23:
	.string	"_sys_siglist"
.LASF1:
	.string	"short unsigned int"
.LASF39:
	.string	"sockaddr_dl"
.LASF15:
	.string	"char"
.LASF89:
	.string	"newfd"
.LASF16:
	.string	"int32_t"
.LASF7:
	.string	"__int32_t"
.LASF52:
	.string	"stx_mask"
.LASF96:
	.string	"vlen"
.LASF40:
	.string	"sockaddr_eon"
.LASF66:
	.string	"stx_mtime"
.LASF67:
	.string	"stx_rdev_major"
.LASF79:
	.string	"program_invocation_short_name"
.LASF3:
	.string	"long unsigned int"
.LASF83:
	.string	"dirfd"
.LASF50:
	.string	"unused0"
.LASF71:
	.string	"unused1"
.LASF12:
	.string	"__time_t"
.LASF34:
	.string	"sockaddr"
.LASF48:
	.string	"sockaddr_x25"
.LASF8:
	.string	"__uint32_t"
.LASF49:
	.string	"uv__statx_timestamp"
.LASF85:
	.string	"mask"
.LASF70:
	.string	"stx_dev_minor"
.LASF62:
	.string	"stx_attributes_mask"
.LASF57:
	.string	"stx_gid"
.LASF98:
	.string	"uv__mmsghdr"
.LASF99:
	.string	"uv__sendmmsg"
.LASF84:
	.string	"path"
.LASF64:
	.string	"stx_btime"
.LASF35:
	.string	"sa_family"
.LASF37:
	.string	"sockaddr_at"
.LASF51:
	.string	"uv__statx"
.LASF29:
	.string	"timespec"
.LASF36:
	.string	"sa_data"
.LASF77:
	.string	"optopt"
.LASF88:
	.string	"oldfd"
.LASF69:
	.string	"stx_dev_major"
.LASF103:
	.string	"syscall"
.LASF90:
	.string	"uv__pwritev"
.LASF100:
	.string	"GNU C89 9.3.0 -m64 -mtune=generic -march=x86-64 -g -O3 -std=gnu90 -fvisibility=hidden -fno-omit-frame-pointer -fasynchronous-unwind-tables -fstack-protector-strong -fstack-clash-protection -fcf-protection"
.LASF41:
	.string	"sockaddr_in"
.LASF91:
	.string	"iovcnt"
.LASF97:
	.string	"timeout"
.LASF93:
	.string	"uv__preadv"
.LASF38:
	.string	"sockaddr_ax25"
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
