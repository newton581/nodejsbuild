	.file	"fs-poll.c"
	.text
.Ltext0:
	.section	.text.unlikely,"ax",@progbits
.LCOLDB0:
	.text
.LHOTB0:
	.p2align 4
	.section	.text.unlikely
.Ltext_cold0:
	.text
	.type	poll_cb, @function
poll_cb:
.LVL0:
.LFB87:
	.file 1 "../deps/uv/src/fs-poll.c"
	.loc 1 185 35 view -0
	.cfi_startproc
	.loc 1 185 35 is_stmt 0 view .LVU1
	endbr64
	.loc 1 186 3 is_stmt 1 view .LVU2
	.loc 1 187 3 view .LVU3
	.loc 1 188 3 view .LVU4
	.loc 1 189 3 view .LVU5
	.loc 1 191 3 view .LVU6
.LVL1:
	.loc 1 192 3 view .LVU7
	.loc 1 185 35 is_stmt 0 view .LVU8
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	.loc 1 192 10 view .LVU9
	movq	-192(%rdi), %r12
.LVL2:
	.loc 1 194 3 is_stmt 1 view .LVU10
	.loc 1 194 8 is_stmt 0 view .LVU11
	movq	%r12, %rdi
.LVL3:
	.loc 1 194 8 view .LVU12
	call	uv_is_active@PLT
.LVL4:
	.loc 1 194 6 view .LVU13
	testl	%eax, %eax
	je	.L2
	.loc 1 194 43 discriminator 1 view .LVU14
	testb	$3, 88(%r12)
	jne	.L2
	.loc 1 197 3 is_stmt 1 view .LVU15
	.loc 1 197 10 is_stmt 0 view .LVU16
	movq	88(%rbx), %rsi
	movslq	-184(%rbx), %rax
	.loc 1 197 6 view .LVU17
	testq	%rsi, %rsi
	jne	.L21
	.loc 1 208 3 is_stmt 1 view .LVU18
.LVL5:
	.loc 1 210 3 view .LVU19
	.loc 1 210 6 is_stmt 0 view .LVU20
	testl	%eax, %eax
	je	.L4
	.loc 1 211 5 is_stmt 1 view .LVU21
	.loc 1 211 8 is_stmt 0 view .LVU22
	js	.L5
.LVL6:
.LBB8:
.LBI8:
	.loc 1 258 12 is_stmt 1 discriminator 1 view .LVU23
.LBB9:
	.loc 1 259 3 discriminator 1 view .LVU24
	.loc 1 272 7 is_stmt 0 discriminator 1 view .LVU25
	movq	248(%rbx), %rax
	cmpq	%rax, 576(%rbx)
	je	.L22
.LVL7:
.L5:
	.loc 1 272 7 discriminator 1 view .LVU26
.LBE9:
.LBE8:
	.loc 1 212 7 is_stmt 1 view .LVU27
	movq	-192(%rbx), %rdi
	.loc 1 208 11 is_stmt 0 view .LVU28
	leaq	112(%rbx), %rcx
	.loc 1 212 43 view .LVU29
	leaq	440(%rbx), %rdx
	.loc 1 212 7 view .LVU30
	xorl	%esi, %esi
	call	*-160(%rbx)
.LVL8:
.L4:
	.loc 1 214 3 is_stmt 1 view .LVU31
	.loc 1 214 16 is_stmt 0 view .LVU32
	movdqu	112(%rbx), %xmm0
	movdqu	128(%rbx), %xmm1
	.loc 1 215 21 view .LVU33
	movl	$1, -184(%rbx)
	.loc 1 214 16 view .LVU34
	movdqu	144(%rbx), %xmm2
	movdqu	160(%rbx), %xmm3
	movdqu	176(%rbx), %xmm4
	movdqu	192(%rbx), %xmm5
	movups	%xmm0, 440(%rbx)
	movdqu	208(%rbx), %xmm6
	movdqu	224(%rbx), %xmm7
	movups	%xmm1, 456(%rbx)
	movdqu	240(%rbx), %xmm0
	movdqu	256(%rbx), %xmm1
	movups	%xmm2, 472(%rbx)
	movups	%xmm3, 488(%rbx)
	movups	%xmm4, 504(%rbx)
	movups	%xmm5, 520(%rbx)
	movups	%xmm6, 536(%rbx)
	movups	%xmm7, 552(%rbx)
	movups	%xmm0, 568(%rbx)
	movups	%xmm1, 584(%rbx)
	.loc 1 215 3 is_stmt 1 view .LVU35
.LVL9:
.L2:
	.loc 1 218 3 view .LVU36
	movq	%rbx, %rdi
	leaq	-152(%rbx), %r13
	call	uv_fs_req_cleanup@PLT
.LVL10:
	.loc 1 220 3 view .LVU37
	.loc 1 220 8 is_stmt 0 view .LVU38
	movq	%r12, %rdi
	call	uv_is_active@PLT
.LVL11:
	.loc 1 220 6 view .LVU39
	testl	%eax, %eax
	je	.L6
	.loc 1 220 43 discriminator 1 view .LVU40
	testb	$3, 88(%r12)
	je	.L7
.L6:
	.loc 1 221 5 is_stmt 1 view .LVU41
	.loc 1 231 1 is_stmt 0 view .LVU42
	addq	$8, %rsp
	.loc 1 221 5 view .LVU43
	movq	%r13, %rdi
	leaq	timer_close_cb(%rip), %rsi
	.loc 1 231 1 view .LVU44
	popq	%rbx
.LVL12:
	.loc 1 231 1 view .LVU45
	popq	%r12
.LVL13:
	.loc 1 231 1 view .LVU46
	popq	%r13
.LVL14:
	.loc 1 231 1 view .LVU47
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	.loc 1 221 5 view .LVU48
	jmp	uv_close@PLT
.LVL15:
	.p2align 4,,10
	.p2align 3
.L7:
	.cfi_restore_state
	.loc 1 226 3 is_stmt 1 view .LVU49
	.loc 1 227 16 is_stmt 0 view .LVU50
	movq	-168(%rbx), %rdi
	.loc 1 226 12 view .LVU51
	movl	-180(%rbx), %r12d
.LVL16:
	.loc 1 227 3 is_stmt 1 view .LVU52
	.loc 1 227 16 is_stmt 0 view .LVU53
	call	uv_now@PLT
.LVL17:
	.loc 1 227 53 view .LVU54
	xorl	%edx, %edx
	.loc 1 227 34 view .LVU55
	subq	-176(%rbx), %rax
	.loc 1 229 7 view .LVU56
	xorl	%ecx, %ecx
	.loc 1 227 53 view .LVU57
	divq	%r12
.LVL18:
	.loc 1 229 3 is_stmt 1 view .LVU58
	.loc 1 229 7 is_stmt 0 view .LVU59
	leaq	timer_cb(%rip), %rsi
	movq	%r13, %rdi
	.loc 1 227 12 view .LVU60
	subq	%rdx, %r12
.LVL19:
	.loc 1 227 12 view .LVU61
	movq	%r12, %rdx
	.loc 1 229 7 view .LVU62
	call	uv_timer_start@PLT
.LVL20:
	.loc 1 229 6 view .LVU63
	testl	%eax, %eax
	jne	.L18
	.loc 1 231 1 view .LVU64
	addq	$8, %rsp
	popq	%rbx
.LVL21:
	.loc 1 231 1 view .LVU65
	popq	%r12
.LVL22:
	.loc 1 231 1 view .LVU66
	popq	%r13
.LVL23:
	.loc 1 231 1 view .LVU67
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.LVL24:
	.p2align 4,,10
	.p2align 3
.L21:
	.cfi_restore_state
	.loc 1 198 5 is_stmt 1 view .LVU68
	.loc 1 198 8 is_stmt 0 view .LVU69
	cmpq	%rax, %rsi
	je	.L2
	.loc 1 199 7 is_stmt 1 view .LVU70
	movq	-192(%rbx), %rdi
	.loc 1 201 20 is_stmt 0 view .LVU71
	leaq	440(%rbx), %rdx
	.loc 1 199 7 view .LVU72
	leaq	zero_statbuf(%rip), %rcx
	call	*-160(%rbx)
.LVL25:
	.loc 1 203 7 is_stmt 1 view .LVU73
	.loc 1 203 25 is_stmt 0 view .LVU74
	movq	88(%rbx), %rax
	movl	%eax, -184(%rbx)
	jmp	.L2
.LVL26:
	.p2align 4,,10
	.p2align 3
.L22:
.LBB12:
.LBB10:
	.loc 1 260 7 view .LVU75
	movq	232(%rbx), %rax
	cmpq	%rax, 560(%rbx)
	jne	.L5
	.loc 1 261 7 view .LVU76
	movq	264(%rbx), %rax
	cmpq	%rax, 592(%rbx)
	jne	.L5
	.loc 1 262 7 view .LVU77
	movq	240(%rbx), %rax
	cmpq	%rax, 568(%rbx)
	jne	.L5
	.loc 1 263 7 view .LVU78
	movq	224(%rbx), %rax
	cmpq	%rax, 552(%rbx)
	jne	.L5
	.loc 1 264 7 view .LVU79
	movq	256(%rbx), %rax
	cmpq	%rax, 584(%rbx)
	jne	.L5
	.loc 1 265 7 view .LVU80
	movq	168(%rbx), %rax
	cmpq	%rax, 496(%rbx)
	jne	.L5
	.loc 1 266 7 view .LVU81
	movq	120(%rbx), %rax
	cmpq	%rax, 448(%rbx)
	jne	.L5
	.loc 1 267 7 view .LVU82
	movq	136(%rbx), %rax
	cmpq	%rax, 464(%rbx)
	jne	.L5
	.loc 1 268 7 view .LVU83
	movq	144(%rbx), %rax
	cmpq	%rax, 472(%rbx)
	jne	.L5
	.loc 1 269 7 view .LVU84
	movq	160(%rbx), %rax
	cmpq	%rax, 488(%rbx)
	jne	.L5
	.loc 1 270 7 view .LVU85
	movq	112(%rbx), %rax
	cmpq	%rax, 440(%rbx)
	jne	.L5
	.loc 1 271 7 view .LVU86
	movq	192(%rbx), %rax
	cmpq	%rax, 520(%rbx)
	jne	.L5
	.loc 1 272 7 view .LVU87
	movq	200(%rbx), %rax
	cmpq	%rax, 528(%rbx)
	jne	.L5
	jmp	.L4
.LVL27:
	.loc 1 272 7 view .LVU88
.LBE10:
.LBE12:
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	poll_cb.cold, @function
poll_cb.cold:
.LFSB87:
.LBB13:
.LBB11:
.L18:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
.LBE11:
.LBE13:
	.loc 1 230 5 is_stmt 1 view -0
	call	abort@PLT
.LVL28:
	.cfi_endproc
.LFE87:
	.text
	.size	poll_cb, .-poll_cb
	.section	.text.unlikely
	.size	poll_cb.cold, .-poll_cb.cold
.LCOLDE0:
	.text
.LHOTE0:
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"../deps/uv/src/fs-poll.c"
.LC2:
	.string	"ctx->parent_handle != NULL"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"ctx->parent_handle->poll_ctx == ctx"
	.section	.text.unlikely
.LCOLDB4:
	.text
.LHOTB4:
	.p2align 4
	.type	timer_cb, @function
timer_cb:
.LVL29:
.LFB86:
	.loc 1 172 41 view -0
	.cfi_startproc
	.loc 1 172 41 is_stmt 0 view .LVU91
	endbr64
	.loc 1 173 3 is_stmt 1 view .LVU92
	.loc 1 175 3 view .LVU93
.LVL30:
	.loc 1 176 2 view .LVU94
	.loc 1 172 41 is_stmt 0 view .LVU95
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	.loc 1 176 5 view .LVU96
	movq	-40(%rdi), %rax
	.loc 1 176 45 view .LVU97
	testq	%rax, %rax
	je	.L29
	leaq	-40(%rdi), %rdx
.LVL31:
	.loc 1 177 2 is_stmt 1 view .LVU98
	.loc 1 177 34 is_stmt 0 view .LVU99
	cmpq	%rdx, 96(%rax)
	jne	.L30
	movq	%rdi, %rbx
	.loc 1 178 3 is_stmt 1 view .LVU100
	.loc 1 178 21 is_stmt 0 view .LVU101
	movq	-16(%rdi), %rdi
.LVL32:
	.loc 1 178 21 view .LVU102
	call	uv_now@PLT
.LVL33:
	.loc 1 180 7 view .LVU103
	movq	-16(%rbx), %rdi
	.loc 1 180 46 view .LVU104
	leaq	760(%rbx), %rdx
	.loc 1 180 7 view .LVU105
	leaq	152(%rbx), %rsi
	.loc 1 178 19 view .LVU106
	movq	%rax, -24(%rbx)
	.loc 1 180 3 is_stmt 1 view .LVU107
	.loc 1 180 7 is_stmt 0 view .LVU108
	leaq	poll_cb(%rip), %rcx
	call	uv_fs_stat@PLT
.LVL34:
	.loc 1 180 6 view .LVU109
	testl	%eax, %eax
	jne	.L27
	.loc 1 182 1 view .LVU110
	addq	$8, %rsp
	popq	%rbx
.LVL35:
	.loc 1 182 1 view .LVU111
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.LVL36:
.L29:
	.cfi_restore_state
	.loc 1 176 22 is_stmt 1 discriminator 1 view .LVU112
	leaq	__PRETTY_FUNCTION__.9103(%rip), %rcx
	movl	$176, %edx
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
.LVL37:
	.loc 1 176 22 is_stmt 0 discriminator 1 view .LVU113
	call	__assert_fail@PLT
.LVL38:
.L30:
.LBB16:
.LBI16:
	.loc 1 172 13 is_stmt 1 view .LVU114
.LBB17:
	.loc 1 177 11 view .LVU115
	leaq	__PRETTY_FUNCTION__.9103(%rip), %rcx
	movl	$177, %edx
.LVL39:
	.loc 1 177 11 is_stmt 0 view .LVU116
	leaq	.LC1(%rip), %rsi
	leaq	.LC3(%rip), %rdi
.LVL40:
	.loc 1 177 11 view .LVU117
	call	__assert_fail@PLT
.LVL41:
	.loc 1 177 11 view .LVU118
.LBE17:
.LBE16:
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	timer_cb.cold, @function
timer_cb.cold:
.LFSB86:
.LBB19:
.LBB18:
.L27:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
.LBE18:
.LBE19:
	.loc 1 181 5 is_stmt 1 view .LVU90
	call	abort@PLT
.LVL42:
	.cfi_endproc
.LFE86:
	.text
	.size	timer_cb, .-timer_cb
	.section	.text.unlikely
	.size	timer_cb.cold, .-timer_cb.cold
.LCOLDE4:
	.text
.LHOTE4:
	.section	.rodata.str1.1
.LC5:
	.string	"last->previous != NULL"
	.text
	.p2align 4
	.type	timer_close_cb, @function
timer_close_cb:
.LVL43:
.LFB88:
	.loc 1 234 48 view -0
	.cfi_startproc
	.loc 1 234 48 is_stmt 0 view .LVU121
	endbr64
	.loc 1 235 3 is_stmt 1 view .LVU122
	.loc 1 236 3 view .LVU123
	.loc 1 237 3 view .LVU124
	.loc 1 238 3 view .LVU125
	.loc 1 240 3 view .LVU126
	.loc 1 234 48 is_stmt 0 view .LVU127
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	.loc 1 240 7 view .LVU128
	leaq	-40(%rdi), %r12
.LVL44:
	.loc 1 241 3 is_stmt 1 view .LVU129
	.loc 1 234 48 is_stmt 0 view .LVU130
	subq	$8, %rsp
	.loc 1 241 10 view .LVU131
	movq	-40(%rdi), %r8
.LVL45:
	.loc 1 242 3 is_stmt 1 view .LVU132
	.loc 1 242 20 is_stmt 0 view .LVU133
	movq	96(%r8), %rdx
	.loc 1 242 6 view .LVU134
	cmpq	%r12, %rdx
	je	.L42
	.loc 1 247 5 is_stmt 1 view .LVU135
.LVL46:
	.loc 1 247 38 is_stmt 0 view .LVU136
	movq	792(%rdx), %rax
.LVL47:
	.loc 1 248 10 is_stmt 1 view .LVU137
	.loc 1 247 5 is_stmt 0 view .LVU138
	cmpq	%rax, %r12
	je	.L34
	.p2align 4,,10
	.p2align 3
.L37:
	.loc 1 247 5 view .LVU139
	movq	%rax, %rdx
.LVL48:
	.loc 1 250 6 is_stmt 1 view .LVU140
	.loc 1 250 49 is_stmt 0 view .LVU141
	testq	%rax, %rax
	je	.L43
.LVL49:
	.loc 1 249 10 is_stmt 1 view .LVU142
	.loc 1 249 24 is_stmt 0 view .LVU143
	movq	792(%rax), %rax
.LVL50:
	.loc 1 248 10 is_stmt 1 view .LVU144
	.loc 1 247 5 is_stmt 0 view .LVU145
	cmpq	%rax, %r12
	jne	.L37
.L34:
	.loc 1 252 5 is_stmt 1 view .LVU146
	.loc 1 252 25 is_stmt 0 view .LVU147
	movq	752(%rdi), %rax
.LVL51:
	.loc 1 252 20 view .LVU148
	movq	%rax, 792(%rdx)
.LVL52:
.L33:
	.loc 1 254 3 is_stmt 1 view .LVU149
	.loc 1 255 1 is_stmt 0 view .LVU150
	addq	$8, %rsp
	.loc 1 254 3 view .LVU151
	movq	%r12, %rdi
	.loc 1 255 1 view .LVU152
	popq	%r12
.LVL53:
	.loc 1 255 1 view .LVU153
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	.loc 1 254 3 view .LVU154
	jmp	uv__free@PLT
.LVL54:
	.p2align 4,,10
	.p2align 3
.L42:
	.cfi_restore_state
	.loc 1 243 5 is_stmt 1 view .LVU155
	.loc 1 243 27 is_stmt 0 view .LVU156
	movq	752(%rdi), %rax
	.loc 1 243 22 view .LVU157
	movq	%rax, 96(%r8)
	.loc 1 244 5 is_stmt 1 view .LVU158
	.loc 1 244 8 is_stmt 0 view .LVU159
	testq	%rax, %rax
	jne	.L33
	.loc 1 244 33 discriminator 1 view .LVU160
	testb	$3, 88(%r8)
	je	.L33
	.loc 1 245 7 is_stmt 1 view .LVU161
	movq	%r8, %rdi
.LVL55:
	.loc 1 245 7 is_stmt 0 view .LVU162
	call	uv__make_close_pending@PLT
.LVL56:
	.loc 1 245 7 view .LVU163
	jmp	.L33
.LVL57:
.L43:
.LBB22:
.LBI22:
	.loc 1 234 13 is_stmt 1 view .LVU164
.LBB23:
	.loc 1 250 26 view .LVU165
	leaq	__PRETTY_FUNCTION__.9119(%rip), %rcx
	movl	$250, %edx
	leaq	.LC1(%rip), %rsi
	leaq	.LC5(%rip), %rdi
.LVL58:
	.loc 1 250 26 is_stmt 0 view .LVU166
	call	__assert_fail@PLT
.LVL59:
	.loc 1 250 26 view .LVU167
.LBE23:
.LBE22:
	.cfi_endproc
.LFE88:
	.size	timer_close_cb, .-timer_close_cb
	.p2align 4
	.globl	uv_fs_poll_init
	.type	uv_fs_poll_init, @function
uv_fs_poll_init:
.LVL60:
.LFB81:
	.loc 1 59 60 is_stmt 1 view -0
	.cfi_startproc
	.loc 1 59 60 is_stmt 0 view .LVU169
	endbr64
	.loc 1 60 3 is_stmt 1 view .LVU170
	.loc 1 60 8 view .LVU171
	.loc 1 60 210 is_stmt 0 view .LVU172
	leaq	16(%rdi), %rax
	.loc 1 60 37 view .LVU173
	movq	%rdi, 8(%rsi)
	.loc 1 60 47 is_stmt 1 view .LVU174
	.loc 1 60 210 is_stmt 0 view .LVU175
	movq	%rax, 32(%rsi)
	.loc 1 60 340 view .LVU176
	movq	24(%rdi), %rdx
	.loc 1 60 440 view .LVU177
	leaq	32(%rsi), %rax
	.loc 1 60 76 view .LVU178
	movl	$4, 16(%rsi)
	.loc 1 60 92 is_stmt 1 view .LVU179
	.loc 1 60 122 is_stmt 0 view .LVU180
	movl	$8, 88(%rsi)
	.loc 1 60 139 is_stmt 1 view .LVU181
	.loc 1 60 144 view .LVU182
	.loc 1 60 234 view .LVU183
	.loc 1 60 297 is_stmt 0 view .LVU184
	movq	%rdx, 40(%rsi)
	.loc 1 60 347 is_stmt 1 view .LVU185
	.loc 1 60 437 is_stmt 0 view .LVU186
	movq	%rax, (%rdx)
	.loc 1 60 480 is_stmt 1 view .LVU187
	.loc 1 60 527 is_stmt 0 view .LVU188
	movq	%rax, 24(%rdi)
	.loc 1 60 578 is_stmt 1 view .LVU189
	.loc 1 60 583 view .LVU190
	.loc 1 63 1 is_stmt 0 view .LVU191
	xorl	%eax, %eax
	.loc 1 60 621 view .LVU192
	movq	$0, 80(%rsi)
	.loc 1 60 13 is_stmt 1 view .LVU193
	.loc 1 61 3 view .LVU194
	.loc 1 61 20 is_stmt 0 view .LVU195
	movq	$0, 96(%rsi)
	.loc 1 62 3 is_stmt 1 view .LVU196
	.loc 1 63 1 is_stmt 0 view .LVU197
	ret
	.cfi_endproc
.LFE81:
	.size	uv_fs_poll_init, .-uv_fs_poll_init
	.p2align 4
	.globl	uv_fs_poll_start
	.type	uv_fs_poll_start, @function
uv_fs_poll_start:
.LVL61:
.LFB82:
	.loc 1 69 45 is_stmt 1 view -0
	.cfi_startproc
	.loc 1 69 45 is_stmt 0 view .LVU199
	endbr64
	.loc 1 70 3 is_stmt 1 view .LVU200
	.loc 1 71 3 view .LVU201
	.loc 1 72 3 view .LVU202
	.loc 1 73 3 view .LVU203
	.loc 1 75 3 view .LVU204
	.loc 1 69 45 is_stmt 0 view .LVU205
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%ecx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	.loc 1 69 45 view .LVU206
	movq	%rsi, -56(%rbp)
	.loc 1 75 7 view .LVU207
	call	uv_is_active@PLT
.LVL62:
	.loc 1 75 6 view .LVU208
	testl	%eax, %eax
	je	.L46
.LVL63:
.L65:
	.loc 1 108 10 discriminator 3 view .LVU209
	xorl	%eax, %eax
.L45:
	.loc 1 113 1 view .LVU210
	addq	$24, %rsp
	popq	%rbx
.LVL64:
	.loc 1 113 1 view .LVU211
	popq	%r12
	popq	%r13
.LVL65:
	.loc 1 113 1 view .LVU212
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
.LVL66:
	.loc 1 113 1 view .LVU213
	ret
.LVL67:
	.p2align 4,,10
	.p2align 3
.L46:
	.cfi_restore_state
	.loc 1 78 3 is_stmt 1 view .LVU214
	.loc 1 79 9 is_stmt 0 view .LVU215
	movq	%r13, %rdi
	.loc 1 78 8 view .LVU216
	movq	8(%rbx), %r14
.LVL68:
	.loc 1 79 3 is_stmt 1 view .LVU217
	.loc 1 79 9 is_stmt 0 view .LVU218
	call	strlen@PLT
.LVL69:
	.loc 1 80 3 is_stmt 1 view .LVU219
	.loc 1 80 9 is_stmt 0 view .LVU220
	movl	$1, %edi
	leaq	808(%rax), %rsi
	movq	%rax, -64(%rbp)
	call	uv__calloc@PLT
.LVL70:
	.loc 1 80 9 view .LVU221
	movq	%rax, %r15
.LVL71:
	.loc 1 82 3 is_stmt 1 view .LVU222
	.loc 1 82 6 is_stmt 0 view .LVU223
	testq	%rax, %rax
	je	.L54
	.loc 1 85 3 is_stmt 1 view .LVU224
	.loc 1 86 16 is_stmt 0 view .LVU225
	movq	-56(%rbp), %rcx
	.loc 1 87 17 view .LVU226
	testl	%r12d, %r12d
	.loc 1 85 13 view .LVU227
	movq	%r14, 24(%rax)
	.loc 1 86 3 is_stmt 1 view .LVU228
	.loc 1 88 21 is_stmt 0 view .LVU229
	movq	%r14, %rdi
	.loc 1 86 16 view .LVU230
	movq	%rcx, 32(%rax)
	.loc 1 87 3 is_stmt 1 view .LVU231
	.loc 1 87 17 is_stmt 0 view .LVU232
	movl	$1, %ecx
	cmove	%ecx, %r12d
.LVL72:
	.loc 1 87 17 view .LVU233
	movl	%r12d, 12(%rax)
	.loc 1 88 3 is_stmt 1 view .LVU234
	.loc 1 90 13 is_stmt 0 view .LVU235
	leaq	800(%r15), %r12
	.loc 1 88 21 view .LVU236
	call	uv_now@PLT
.LVL73:
	.loc 1 90 3 view .LVU237
	movq	-64(%rbp), %rdx
	.loc 1 89 22 view .LVU238
	movq	%rbx, (%r15)
.LBB24:
.LBB25:
	.file 2 "/usr/include/x86_64-linux-gnu/bits/string_fortified.h"
	.loc 2 34 10 view .LVU239
	movq	%r13, %rsi
.LBE25:
.LBE24:
	.loc 1 88 19 view .LVU240
	movq	%rax, 16(%r15)
	.loc 1 89 3 is_stmt 1 view .LVU241
	.loc 1 90 3 view .LVU242
.LVL74:
.LBB28:
.LBI24:
	.loc 2 31 42 view .LVU243
.LBB26:
	.loc 2 34 3 view .LVU244
	.loc 2 34 10 is_stmt 0 view .LVU245
	movq	%r12, %rdi
.LBE26:
.LBE28:
	.loc 1 90 3 view .LVU246
	addq	$1, %rdx
.LVL75:
.LBB29:
.LBB27:
	.loc 2 34 10 view .LVU247
	call	memcpy@PLT
.LVL76:
	.loc 2 34 10 view .LVU248
.LBE27:
.LBE29:
	.loc 1 92 3 is_stmt 1 view .LVU249
	.loc 1 92 9 is_stmt 0 view .LVU250
	leaq	40(%r15), %rsi
	movq	%r14, %rdi
	call	uv_timer_init@PLT
.LVL77:
	.loc 1 93 3 is_stmt 1 view .LVU251
	.loc 1 93 6 is_stmt 0 view .LVU252
	testl	%eax, %eax
	js	.L48
	.loc 1 96 3 is_stmt 1 view .LVU253
	.loc 1 96 27 is_stmt 0 view .LVU254
	movl	128(%r15), %eax
.LVL78:
	.loc 1 97 3 is_stmt 1 view .LVU255
	.loc 1 97 8 view .LVU256
	.loc 1 97 11 is_stmt 0 view .LVU257
	testb	$8, %al
	jne	.L49
	.loc 1 96 27 view .LVU258
	orl	$16, %eax
	movl	%eax, 128(%r15)
.L50:
	.loc 1 97 303 is_stmt 1 discriminator 7 view .LVU259
	.loc 1 97 316 discriminator 7 view .LVU260
	.loc 1 99 3 discriminator 7 view .LVU261
	.loc 1 99 9 is_stmt 0 discriminator 7 view .LVU262
	leaq	192(%r15), %rsi
	leaq	poll_cb(%rip), %rcx
	movq	%r12, %rdx
	movq	%r14, %rdi
	call	uv_fs_stat@PLT
.LVL79:
	.loc 1 100 3 is_stmt 1 discriminator 7 view .LVU263
	.loc 1 100 6 is_stmt 0 discriminator 7 view .LVU264
	testl	%eax, %eax
	js	.L48
	.loc 1 103 3 is_stmt 1 view .LVU265
	.loc 1 103 13 is_stmt 0 view .LVU266
	movq	96(%rbx), %rax
.LVL80:
	.loc 1 103 6 view .LVU267
	testq	%rax, %rax
	je	.L52
	.loc 1 104 5 is_stmt 1 view .LVU268
	.loc 1 104 19 is_stmt 0 view .LVU269
	movq	%rax, 792(%r15)
.L52:
	.loc 1 105 3 is_stmt 1 view .LVU270
	.loc 1 106 21 is_stmt 0 view .LVU271
	movl	88(%rbx), %eax
	.loc 1 105 20 view .LVU272
	movq	%r15, 96(%rbx)
	.loc 1 106 3 is_stmt 1 view .LVU273
	.loc 1 106 8 view .LVU274
	.loc 1 106 11 is_stmt 0 view .LVU275
	testb	$4, %al
	jne	.L65
	.loc 1 106 62 is_stmt 1 discriminator 2 view .LVU276
	.loc 1 106 78 is_stmt 0 discriminator 2 view .LVU277
	movl	%eax, %edx
	orl	$4, %edx
	movl	%edx, 88(%rbx)
	.loc 1 106 99 is_stmt 1 discriminator 2 view .LVU278
	.loc 1 106 102 is_stmt 0 discriminator 2 view .LVU279
	testb	$8, %al
	je	.L65
	.loc 1 106 143 is_stmt 1 discriminator 3 view .LVU280
	.loc 1 106 148 discriminator 3 view .LVU281
	.loc 1 106 156 is_stmt 0 discriminator 3 view .LVU282
	movq	8(%rbx), %rax
	.loc 1 106 178 discriminator 3 view .LVU283
	addl	$1, 8(%rax)
	jmp	.L65
.LVL81:
	.p2align 4,,10
	.p2align 3
.L48:
	.loc 1 111 3 view .LVU284
	movq	%r15, %rdi
	movl	%eax, -56(%rbp)
.LVL82:
	.loc 1 111 3 is_stmt 1 view .LVU285
	call	uv__free@PLT
.LVL83:
	.loc 1 112 3 view .LVU286
	.loc 1 112 10 is_stmt 0 view .LVU287
	movl	-56(%rbp), %eax
	.loc 1 113 1 view .LVU288
	addq	$24, %rsp
	popq	%rbx
.LVL84:
	.loc 1 113 1 view .LVU289
	popq	%r12
	popq	%r13
.LVL85:
	.loc 1 113 1 view .LVU290
	popq	%r14
.LVL86:
	.loc 1 113 1 view .LVU291
	popq	%r15
.LVL87:
	.loc 1 113 1 view .LVU292
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.LVL88:
	.p2align 4,,10
	.p2align 3
.L49:
	.cfi_restore_state
	.loc 1 97 71 is_stmt 1 discriminator 2 view .LVU293
	.loc 1 97 99 is_stmt 0 discriminator 2 view .LVU294
	movl	%eax, %edx
	andl	$-9, %edx
	orl	$16, %edx
	movl	%edx, 128(%r15)
	.loc 1 97 118 is_stmt 1 discriminator 2 view .LVU295
	.loc 1 97 121 is_stmt 0 discriminator 2 view .LVU296
	testb	$1, %al
	jne	.L50
	.loc 1 97 185 is_stmt 1 discriminator 4 view .LVU297
	.loc 1 97 188 is_stmt 0 discriminator 4 view .LVU298
	testb	$4, %al
	je	.L50
	.loc 1 97 244 is_stmt 1 discriminator 5 view .LVU299
	.loc 1 97 249 discriminator 5 view .LVU300
	.loc 1 97 269 is_stmt 0 discriminator 5 view .LVU301
	movq	48(%r15), %rax
	.loc 1 97 291 discriminator 5 view .LVU302
	subl	$1, 8(%rax)
	jmp	.L50
.LVL89:
.L54:
	.loc 1 83 12 view .LVU303
	movl	$-12, %eax
.LVL90:
	.loc 1 83 12 view .LVU304
	jmp	.L45
	.cfi_endproc
.LFE82:
	.size	uv_fs_poll_start, .-uv_fs_poll_start
	.section	.rodata.str1.1
.LC6:
	.string	"ctx != NULL"
.LC7:
	.string	"ctx->parent_handle == handle"
	.text
	.p2align 4
	.globl	uv_fs_poll_stop
	.type	uv_fs_poll_stop, @function
uv_fs_poll_stop:
.LVL91:
.LFB83:
	.loc 1 116 43 is_stmt 1 view -0
	.cfi_startproc
	.loc 1 116 43 is_stmt 0 view .LVU306
	endbr64
	.loc 1 117 3 is_stmt 1 view .LVU307
	.loc 1 119 3 view .LVU308
	.loc 1 116 43 is_stmt 0 view .LVU309
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	.loc 1 116 43 view .LVU310
	movq	%rdi, %rbx
	.loc 1 119 8 view .LVU311
	call	uv_is_active@PLT
.LVL92:
	.loc 1 119 6 view .LVU312
	testl	%eax, %eax
	je	.L68
	.loc 1 122 3 is_stmt 1 view .LVU313
	.loc 1 122 7 is_stmt 0 view .LVU314
	movq	96(%rbx), %rdi
.LVL93:
	.loc 1 123 2 is_stmt 1 view .LVU315
	.loc 1 123 45 is_stmt 0 view .LVU316
	testq	%rdi, %rdi
	je	.L83
	.loc 1 124 2 is_stmt 1 view .LVU317
	.loc 1 124 34 is_stmt 0 view .LVU318
	cmpq	%rbx, (%rdi)
	jne	.L84
	.loc 1 129 3 is_stmt 1 view .LVU319
	.loc 1 129 34 is_stmt 0 view .LVU320
	leaq	40(%rdi), %r12
	.loc 1 129 7 view .LVU321
	movq	%r12, %rdi
.LVL94:
	.loc 1 129 7 view .LVU322
	call	uv_is_active@PLT
.LVL95:
	.loc 1 129 6 view .LVU323
	testl	%eax, %eax
	jne	.L85
.LVL96:
.LBB32:
.LBI32:
	.loc 1 116 5 is_stmt 1 view .LVU324
.LBB33:
	.loc 1 132 3 view .LVU325
	.loc 1 132 8 view .LVU326
	.loc 1 132 21 is_stmt 0 view .LVU327
	movl	88(%rbx), %eax
	.loc 1 132 11 view .LVU328
	testb	$4, %al
	jne	.L86
.LVL97:
.L68:
	.loc 1 132 11 view .LVU329
.LBE33:
.LBE32:
	.loc 1 135 1 view .LVU330
	popq	%rbx
.LVL98:
	.loc 1 135 1 view .LVU331
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.LVL99:
	.p2align 4,,10
	.p2align 3
.L86:
	.cfi_restore_state
.LBB36:
.LBB34:
	.loc 1 132 62 is_stmt 1 view .LVU332
	.loc 1 132 78 is_stmt 0 view .LVU333
	movl	%eax, %edx
	andl	$-5, %edx
	movl	%edx, 88(%rbx)
	.loc 1 132 100 is_stmt 1 view .LVU334
	.loc 1 132 103 is_stmt 0 view .LVU335
	testb	$8, %al
	je	.L68
	.loc 1 132 144 is_stmt 1 view .LVU336
	.loc 1 132 149 view .LVU337
	.loc 1 132 157 is_stmt 0 view .LVU338
	movq	8(%rbx), %rax
	.loc 1 132 179 view .LVU339
	subl	$1, 8(%rax)
.LVL100:
	.loc 1 132 179 view .LVU340
.LBE34:
.LBE36:
	.loc 1 135 1 view .LVU341
	xorl	%eax, %eax
	popq	%rbx
.LVL101:
	.loc 1 135 1 view .LVU342
	popq	%r12
.LVL102:
	.loc 1 135 1 view .LVU343
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.LVL103:
	.p2align 4,,10
	.p2align 3
.L85:
	.cfi_restore_state
	.loc 1 130 5 is_stmt 1 view .LVU344
	leaq	timer_close_cb(%rip), %rsi
	movq	%r12, %rdi
	call	uv_close@PLT
.LVL104:
.LBB37:
	.loc 1 116 5 view .LVU345
.LBB35:
	.loc 1 132 3 view .LVU346
	.loc 1 132 8 view .LVU347
	.loc 1 132 21 is_stmt 0 view .LVU348
	movl	88(%rbx), %eax
	.loc 1 132 11 view .LVU349
	testb	$4, %al
	je	.L68
	jmp	.L86
.LVL105:
.L83:
	.loc 1 132 11 view .LVU350
.LBE35:
.LBE37:
	.loc 1 123 22 is_stmt 1 discriminator 1 view .LVU351
	leaq	__PRETTY_FUNCTION__.9086(%rip), %rcx
	movl	$123, %edx
	leaq	.LC1(%rip), %rsi
	leaq	.LC6(%rip), %rdi
.LVL106:
	.loc 1 123 22 is_stmt 0 discriminator 1 view .LVU352
	call	__assert_fail@PLT
.LVL107:
.L84:
	.loc 1 124 11 is_stmt 1 discriminator 1 view .LVU353
	leaq	__PRETTY_FUNCTION__.9086(%rip), %rcx
	movl	$124, %edx
	leaq	.LC1(%rip), %rsi
	leaq	.LC7(%rip), %rdi
.LVL108:
	.loc 1 124 11 is_stmt 0 discriminator 1 view .LVU354
	call	__assert_fail@PLT
.LVL109:
	.loc 1 124 11 discriminator 1 view .LVU355
	.cfi_endproc
.LFE83:
	.size	uv_fs_poll_stop, .-uv_fs_poll_stop
	.p2align 4
	.globl	uv_fs_poll_getpath
	.type	uv_fs_poll_getpath, @function
uv_fs_poll_getpath:
.LVL110:
.LFB84:
	.loc 1 138 74 is_stmt 1 view -0
	.cfi_startproc
	.loc 1 138 74 is_stmt 0 view .LVU357
	endbr64
	.loc 1 139 3 is_stmt 1 view .LVU358
	.loc 1 140 3 view .LVU359
	.loc 1 142 3 view .LVU360
	.loc 1 138 74 is_stmt 0 view .LVU361
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	.loc 1 138 74 view .LVU362
	movq	%rdi, %rbx
	.loc 1 142 8 view .LVU363
	call	uv_is_active@PLT
.LVL111:
	.loc 1 142 6 view .LVU364
	testl	%eax, %eax
	je	.L93
	.loc 1 147 3 is_stmt 1 view .LVU365
	.loc 1 147 7 is_stmt 0 view .LVU366
	movq	96(%rbx), %rsi
.LVL112:
	.loc 1 148 2 is_stmt 1 view .LVU367
	.loc 1 148 45 is_stmt 0 view .LVU368
	testq	%rsi, %rsi
	je	.L94
	.loc 1 150 3 is_stmt 1 view .LVU369
	.loc 1 150 28 is_stmt 0 view .LVU370
	leaq	800(%rsi), %r14
	.loc 1 150 18 view .LVU371
	movq	%r14, %rdi
	call	strlen@PLT
.LVL113:
	.loc 1 150 18 view .LVU372
	movq	%rax, %rbx
.LVL114:
	.loc 1 151 3 is_stmt 1 view .LVU373
	.loc 1 151 6 is_stmt 0 view .LVU374
	cmpq	%rax, (%r12)
	jbe	.L95
	.loc 1 156 3 is_stmt 1 view .LVU375
.LVL115:
.LBB38:
.LBI38:
	.loc 2 31 42 view .LVU376
.LBB39:
	.loc 2 34 3 view .LVU377
	.loc 2 34 10 is_stmt 0 view .LVU378
	movq	%rax, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	memcpy@PLT
.LVL116:
	.loc 2 34 10 view .LVU379
.LBE39:
.LBE38:
	.loc 1 157 3 is_stmt 1 view .LVU380
	.loc 1 157 9 is_stmt 0 view .LVU381
	movq	%rbx, (%r12)
	.loc 1 158 3 is_stmt 1 view .LVU382
	.loc 1 160 10 is_stmt 0 view .LVU383
	xorl	%eax, %eax
	.loc 1 158 24 view .LVU384
	movb	$0, 0(%r13,%rbx)
	.loc 1 160 3 is_stmt 1 view .LVU385
.LVL117:
.L87:
	.loc 1 161 1 is_stmt 0 view .LVU386
	popq	%rbx
	popq	%r12
.LVL118:
	.loc 1 161 1 view .LVU387
	popq	%r13
.LVL119:
	.loc 1 161 1 view .LVU388
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.LVL120:
	.p2align 4,,10
	.p2align 3
.L95:
	.cfi_restore_state
	.loc 1 152 5 is_stmt 1 view .LVU389
	.loc 1 152 26 is_stmt 0 view .LVU390
	addq	$1, %rbx
	.loc 1 153 12 view .LVU391
	movl	$-105, %eax
.LVL121:
	.loc 1 152 26 view .LVU392
	movq	%rbx, (%r12)
	.loc 1 153 5 is_stmt 1 view .LVU393
	.loc 1 153 12 is_stmt 0 view .LVU394
	jmp	.L87
.LVL122:
	.p2align 4,,10
	.p2align 3
.L93:
	.loc 1 143 5 is_stmt 1 view .LVU395
	.loc 1 143 11 is_stmt 0 view .LVU396
	movq	$0, (%r12)
	.loc 1 144 5 is_stmt 1 view .LVU397
	.loc 1 144 12 is_stmt 0 view .LVU398
	movl	$-22, %eax
	jmp	.L87
.LVL123:
.L94:
	.loc 1 148 22 is_stmt 1 discriminator 1 view .LVU399
	leaq	__PRETTY_FUNCTION__.9095(%rip), %rcx
	movl	$148, %edx
	leaq	.LC1(%rip), %rsi
.LVL124:
	.loc 1 148 22 is_stmt 0 discriminator 1 view .LVU400
	leaq	.LC6(%rip), %rdi
	call	__assert_fail@PLT
.LVL125:
	.loc 1 148 22 discriminator 1 view .LVU401
	.cfi_endproc
.LFE84:
	.size	uv_fs_poll_getpath, .-uv_fs_poll_getpath
	.p2align 4
	.globl	uv__fs_poll_close
	.hidden	uv__fs_poll_close
	.type	uv__fs_poll_close, @function
uv__fs_poll_close:
.LVL126:
.LFB85:
	.loc 1 164 46 is_stmt 1 view -0
	.cfi_startproc
	.loc 1 164 46 is_stmt 0 view .LVU403
	endbr64
	.loc 1 165 3 is_stmt 1 view .LVU404
	.loc 1 164 46 is_stmt 0 view .LVU405
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.loc 1 164 46 view .LVU406
	movq	%rdi, %r12
.LVL127:
.LBB44:
.LBI44:
	.loc 1 116 5 is_stmt 1 view .LVU407
.LBB45:
	.loc 1 117 3 view .LVU408
	.loc 1 119 3 view .LVU409
	.loc 1 119 8 is_stmt 0 view .LVU410
	call	uv_is_active@PLT
.LVL128:
	.loc 1 119 6 view .LVU411
	testl	%eax, %eax
	je	.L98
	.loc 1 122 3 is_stmt 1 view .LVU412
	.loc 1 122 7 is_stmt 0 view .LVU413
	movq	96(%r12), %rdi
.LVL129:
	.loc 1 123 2 is_stmt 1 view .LVU414
	.loc 1 123 45 is_stmt 0 view .LVU415
	testq	%rdi, %rdi
	je	.L114
	.loc 1 124 2 is_stmt 1 view .LVU416
	.loc 1 124 34 is_stmt 0 view .LVU417
	cmpq	(%rdi), %r12
	jne	.L115
	.loc 1 129 3 is_stmt 1 view .LVU418
	.loc 1 129 34 is_stmt 0 view .LVU419
	leaq	40(%rdi), %r13
	.loc 1 129 7 view .LVU420
	movq	%r13, %rdi
.LVL130:
	.loc 1 129 7 view .LVU421
	call	uv_is_active@PLT
.LVL131:
	.loc 1 129 6 view .LVU422
	testl	%eax, %eax
	jne	.L116
.LVL132:
.LBB46:
.LBI46:
	.loc 1 116 5 is_stmt 1 view .LVU423
.LBB47:
	.loc 1 132 3 view .LVU424
	.loc 1 132 8 view .LVU425
	.loc 1 132 21 is_stmt 0 view .LVU426
	movl	88(%r12), %eax
	.loc 1 132 11 view .LVU427
	testb	$4, %al
	jne	.L117
.LVL133:
.L98:
	.loc 1 132 11 view .LVU428
.LBE47:
.LBE46:
.LBE45:
.LBE44:
	.loc 1 167 3 is_stmt 1 view .LVU429
	.loc 1 167 6 is_stmt 0 view .LVU430
	cmpq	$0, 96(%r12)
	je	.L118
.L96:
	.loc 1 169 1 view .LVU431
	popq	%r12
.LVL134:
	.loc 1 169 1 view .LVU432
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.LVL135:
	.p2align 4,,10
	.p2align 3
.L117:
	.cfi_restore_state
.LBB54:
.LBB52:
.LBB50:
.LBB48:
	.loc 1 132 62 is_stmt 1 view .LVU433
	.loc 1 132 78 is_stmt 0 view .LVU434
	movl	%eax, %edx
	andl	$-5, %edx
	movl	%edx, 88(%r12)
	.loc 1 132 100 is_stmt 1 view .LVU435
	.loc 1 132 103 is_stmt 0 view .LVU436
	testb	$8, %al
	je	.L98
	.loc 1 132 144 is_stmt 1 view .LVU437
	.loc 1 132 149 view .LVU438
	.loc 1 132 157 is_stmt 0 view .LVU439
	movq	8(%r12), %rax
	.loc 1 132 179 view .LVU440
	subl	$1, 8(%rax)
.LVL136:
	.loc 1 132 179 view .LVU441
.LBE48:
.LBE50:
.LBE52:
.LBE54:
	.loc 1 167 3 is_stmt 1 view .LVU442
	.loc 1 167 6 is_stmt 0 view .LVU443
	cmpq	$0, 96(%r12)
	jne	.L96
.L118:
	.loc 1 168 5 is_stmt 1 view .LVU444
	movq	%r12, %rdi
	.loc 1 169 1 is_stmt 0 view .LVU445
	popq	%r12
.LVL137:
	.loc 1 169 1 view .LVU446
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	.loc 1 168 5 view .LVU447
	jmp	uv__make_close_pending@PLT
.LVL138:
	.p2align 4,,10
	.p2align 3
.L116:
	.cfi_restore_state
.LBB55:
.LBB53:
	.loc 1 130 5 is_stmt 1 view .LVU448
	leaq	timer_close_cb(%rip), %rsi
	movq	%r13, %rdi
	call	uv_close@PLT
.LVL139:
.LBB51:
	.loc 1 116 5 view .LVU449
.LBB49:
	.loc 1 132 3 view .LVU450
	.loc 1 132 8 view .LVU451
	.loc 1 132 21 is_stmt 0 view .LVU452
	movl	88(%r12), %eax
	.loc 1 132 11 view .LVU453
	testb	$4, %al
	je	.L98
	jmp	.L117
.LVL140:
.L114:
	.loc 1 132 11 view .LVU454
.LBE49:
.LBE51:
	.loc 1 123 22 is_stmt 1 view .LVU455
	leaq	__PRETTY_FUNCTION__.9086(%rip), %rcx
	movl	$123, %edx
	leaq	.LC1(%rip), %rsi
	leaq	.LC6(%rip), %rdi
.LVL141:
	.loc 1 123 22 is_stmt 0 view .LVU456
	call	__assert_fail@PLT
.LVL142:
.L115:
	.loc 1 124 11 is_stmt 1 view .LVU457
	leaq	__PRETTY_FUNCTION__.9086(%rip), %rcx
	movl	$124, %edx
	leaq	.LC1(%rip), %rsi
	leaq	.LC7(%rip), %rdi
.LVL143:
	.loc 1 124 11 is_stmt 0 view .LVU458
	call	__assert_fail@PLT
.LVL144:
	.loc 1 124 11 view .LVU459
.LBE53:
.LBE55:
	.cfi_endproc
.LFE85:
	.size	uv__fs_poll_close, .-uv__fs_poll_close
	.section	.rodata
	.align 16
	.type	__PRETTY_FUNCTION__.9095, @object
	.size	__PRETTY_FUNCTION__.9095, 19
__PRETTY_FUNCTION__.9095:
	.string	"uv_fs_poll_getpath"
	.align 16
	.type	__PRETTY_FUNCTION__.9086, @object
	.size	__PRETTY_FUNCTION__.9086, 16
__PRETTY_FUNCTION__.9086:
	.string	"uv_fs_poll_stop"
	.align 8
	.type	__PRETTY_FUNCTION__.9119, @object
	.size	__PRETTY_FUNCTION__.9119, 15
__PRETTY_FUNCTION__.9119:
	.string	"timer_close_cb"
	.align 8
	.type	__PRETTY_FUNCTION__.9103, @object
	.size	__PRETTY_FUNCTION__.9103, 9
__PRETTY_FUNCTION__.9103:
	.string	"timer_cb"
	.local	zero_statbuf
	.comm	zero_statbuf,160,32
	.text
.Letext0:
	.section	.text.unlikely
.Letext_cold0:
	.file 3 "/usr/include/errno.h"
	.file 4 "/usr/lib/gcc/x86_64-linux-gnu/9/include/stddef.h"
	.file 5 "/usr/include/x86_64-linux-gnu/bits/types.h"
	.file 6 "/usr/include/x86_64-linux-gnu/bits/types/struct_FILE.h"
	.file 7 "/usr/include/x86_64-linux-gnu/bits/types/FILE.h"
	.file 8 "/usr/include/stdio.h"
	.file 9 "/usr/include/x86_64-linux-gnu/bits/sys_errlist.h"
	.file 10 "/usr/include/x86_64-linux-gnu/bits/stdint-uintn.h"
	.file 11 "/usr/include/x86_64-linux-gnu/sys/types.h"
	.file 12 "/usr/include/x86_64-linux-gnu/bits/thread-shared-types.h"
	.file 13 "/usr/include/x86_64-linux-gnu/bits/struct_mutex.h"
	.file 14 "/usr/include/x86_64-linux-gnu/bits/struct_rwlock.h"
	.file 15 "/usr/include/x86_64-linux-gnu/bits/pthreadtypes.h"
	.file 16 "/usr/include/x86_64-linux-gnu/bits/sockaddr.h"
	.file 17 "/usr/include/x86_64-linux-gnu/bits/socket.h"
	.file 18 "/usr/include/netinet/in.h"
	.file 19 "/usr/include/signal.h"
	.file 20 "/usr/include/time.h"
	.file 21 "../deps/uv/include/uv/threadpool.h"
	.file 22 "../deps/uv/include/uv.h"
	.file 23 "../deps/uv/include/uv/unix.h"
	.file 24 "../deps/uv/src/queue.h"
	.file 25 "../deps/uv/src/uv-common.h"
	.file 26 "/usr/include/stdlib.h"
	.file 27 "/usr/include/assert.h"
	.file 28 "../deps/uv/src/unix/internal.h"
	.file 29 "/usr/include/string.h"
	.file 30 "<built-in>"
	.section	.debug_info,"",@progbits
.Ldebug_info0:
	.long	0x2903
	.value	0x4
	.long	.Ldebug_abbrev0
	.byte	0x8
	.uleb128 0x1
	.long	.LASF509
	.byte	0x1
	.long	.LASF510
	.long	.LASF511
	.long	.Ldebug_ranges0+0x1d0
	.quad	0
	.long	.Ldebug_line0
	.uleb128 0x2
	.long	.LASF0
	.byte	0x3
	.byte	0x2d
	.byte	0xe
	.long	0x35
	.uleb128 0x3
	.byte	0x8
	.long	0x3b
	.uleb128 0x4
	.byte	0x1
	.byte	0x6
	.long	.LASF2
	.uleb128 0x5
	.long	0x3b
	.uleb128 0x2
	.long	.LASF1
	.byte	0x3
	.byte	0x2e
	.byte	0xe
	.long	0x35
	.uleb128 0x6
	.byte	0x4
	.byte	0x5
	.string	"int"
	.uleb128 0x4
	.byte	0x8
	.byte	0x5
	.long	.LASF3
	.uleb128 0x7
	.long	.LASF9
	.byte	0x4
	.byte	0xd1
	.byte	0x1b
	.long	0x6d
	.uleb128 0x4
	.byte	0x8
	.byte	0x7
	.long	.LASF4
	.uleb128 0x4
	.byte	0x4
	.byte	0x7
	.long	.LASF5
	.uleb128 0x8
	.byte	0x8
	.uleb128 0x9
	.long	0x7b
	.uleb128 0x4
	.byte	0x1
	.byte	0x8
	.long	.LASF6
	.uleb128 0x4
	.byte	0x2
	.byte	0x7
	.long	.LASF7
	.uleb128 0x4
	.byte	0x1
	.byte	0x6
	.long	.LASF8
	.uleb128 0x7
	.long	.LASF10
	.byte	0x5
	.byte	0x26
	.byte	0x17
	.long	0x82
	.uleb128 0x4
	.byte	0x2
	.byte	0x5
	.long	.LASF11
	.uleb128 0x7
	.long	.LASF12
	.byte	0x5
	.byte	0x28
	.byte	0x1c
	.long	0x89
	.uleb128 0x7
	.long	.LASF13
	.byte	0x5
	.byte	0x2a
	.byte	0x16
	.long	0x74
	.uleb128 0x7
	.long	.LASF14
	.byte	0x5
	.byte	0x2d
	.byte	0x1b
	.long	0x6d
	.uleb128 0x7
	.long	.LASF15
	.byte	0x5
	.byte	0x92
	.byte	0x16
	.long	0x74
	.uleb128 0x7
	.long	.LASF16
	.byte	0x5
	.byte	0x93
	.byte	0x16
	.long	0x74
	.uleb128 0x7
	.long	.LASF17
	.byte	0x5
	.byte	0x96
	.byte	0x16
	.long	0x74
	.uleb128 0x7
	.long	.LASF18
	.byte	0x5
	.byte	0x98
	.byte	0x12
	.long	0x5a
	.uleb128 0x7
	.long	.LASF19
	.byte	0x5
	.byte	0x99
	.byte	0x12
	.long	0x5a
	.uleb128 0xa
	.long	0x53
	.long	0x11a
	.uleb128 0xb
	.long	0x6d
	.byte	0x1
	.byte	0
	.uleb128 0x7
	.long	.LASF20
	.byte	0x5
	.byte	0xc1
	.byte	0x12
	.long	0x5a
	.uleb128 0xc
	.long	.LASF70
	.byte	0xd8
	.byte	0x6
	.byte	0x31
	.byte	0x8
	.long	0x2ad
	.uleb128 0xd
	.long	.LASF21
	.byte	0x6
	.byte	0x33
	.byte	0x7
	.long	0x53
	.byte	0
	.uleb128 0xd
	.long	.LASF22
	.byte	0x6
	.byte	0x36
	.byte	0x9
	.long	0x35
	.byte	0x8
	.uleb128 0xd
	.long	.LASF23
	.byte	0x6
	.byte	0x37
	.byte	0x9
	.long	0x35
	.byte	0x10
	.uleb128 0xd
	.long	.LASF24
	.byte	0x6
	.byte	0x38
	.byte	0x9
	.long	0x35
	.byte	0x18
	.uleb128 0xd
	.long	.LASF25
	.byte	0x6
	.byte	0x39
	.byte	0x9
	.long	0x35
	.byte	0x20
	.uleb128 0xd
	.long	.LASF26
	.byte	0x6
	.byte	0x3a
	.byte	0x9
	.long	0x35
	.byte	0x28
	.uleb128 0xd
	.long	.LASF27
	.byte	0x6
	.byte	0x3b
	.byte	0x9
	.long	0x35
	.byte	0x30
	.uleb128 0xd
	.long	.LASF28
	.byte	0x6
	.byte	0x3c
	.byte	0x9
	.long	0x35
	.byte	0x38
	.uleb128 0xd
	.long	.LASF29
	.byte	0x6
	.byte	0x3d
	.byte	0x9
	.long	0x35
	.byte	0x40
	.uleb128 0xd
	.long	.LASF30
	.byte	0x6
	.byte	0x40
	.byte	0x9
	.long	0x35
	.byte	0x48
	.uleb128 0xd
	.long	.LASF31
	.byte	0x6
	.byte	0x41
	.byte	0x9
	.long	0x35
	.byte	0x50
	.uleb128 0xd
	.long	.LASF32
	.byte	0x6
	.byte	0x42
	.byte	0x9
	.long	0x35
	.byte	0x58
	.uleb128 0xd
	.long	.LASF33
	.byte	0x6
	.byte	0x44
	.byte	0x16
	.long	0x2c6
	.byte	0x60
	.uleb128 0xd
	.long	.LASF34
	.byte	0x6
	.byte	0x46
	.byte	0x14
	.long	0x2cc
	.byte	0x68
	.uleb128 0xd
	.long	.LASF35
	.byte	0x6
	.byte	0x48
	.byte	0x7
	.long	0x53
	.byte	0x70
	.uleb128 0xd
	.long	.LASF36
	.byte	0x6
	.byte	0x49
	.byte	0x7
	.long	0x53
	.byte	0x74
	.uleb128 0xd
	.long	.LASF37
	.byte	0x6
	.byte	0x4a
	.byte	0xb
	.long	0xf2
	.byte	0x78
	.uleb128 0xd
	.long	.LASF38
	.byte	0x6
	.byte	0x4d
	.byte	0x12
	.long	0x89
	.byte	0x80
	.uleb128 0xd
	.long	.LASF39
	.byte	0x6
	.byte	0x4e
	.byte	0xf
	.long	0x90
	.byte	0x82
	.uleb128 0xd
	.long	.LASF40
	.byte	0x6
	.byte	0x4f
	.byte	0x8
	.long	0x2d2
	.byte	0x83
	.uleb128 0xd
	.long	.LASF41
	.byte	0x6
	.byte	0x51
	.byte	0xf
	.long	0x2e2
	.byte	0x88
	.uleb128 0xd
	.long	.LASF42
	.byte	0x6
	.byte	0x59
	.byte	0xd
	.long	0xfe
	.byte	0x90
	.uleb128 0xd
	.long	.LASF43
	.byte	0x6
	.byte	0x5b
	.byte	0x17
	.long	0x2ed
	.byte	0x98
	.uleb128 0xd
	.long	.LASF44
	.byte	0x6
	.byte	0x5c
	.byte	0x19
	.long	0x2f8
	.byte	0xa0
	.uleb128 0xd
	.long	.LASF45
	.byte	0x6
	.byte	0x5d
	.byte	0x14
	.long	0x2cc
	.byte	0xa8
	.uleb128 0xd
	.long	.LASF46
	.byte	0x6
	.byte	0x5e
	.byte	0x9
	.long	0x7b
	.byte	0xb0
	.uleb128 0xd
	.long	.LASF47
	.byte	0x6
	.byte	0x5f
	.byte	0xa
	.long	0x61
	.byte	0xb8
	.uleb128 0xd
	.long	.LASF48
	.byte	0x6
	.byte	0x60
	.byte	0x7
	.long	0x53
	.byte	0xc0
	.uleb128 0xd
	.long	.LASF49
	.byte	0x6
	.byte	0x62
	.byte	0x8
	.long	0x2fe
	.byte	0xc4
	.byte	0
	.uleb128 0x7
	.long	.LASF50
	.byte	0x7
	.byte	0x7
	.byte	0x19
	.long	0x126
	.uleb128 0xe
	.long	.LASF512
	.byte	0x6
	.byte	0x2b
	.byte	0xe
	.uleb128 0xf
	.long	.LASF51
	.uleb128 0x3
	.byte	0x8
	.long	0x2c1
	.uleb128 0x3
	.byte	0x8
	.long	0x126
	.uleb128 0xa
	.long	0x3b
	.long	0x2e2
	.uleb128 0xb
	.long	0x6d
	.byte	0
	.byte	0
	.uleb128 0x3
	.byte	0x8
	.long	0x2b9
	.uleb128 0xf
	.long	.LASF52
	.uleb128 0x3
	.byte	0x8
	.long	0x2e8
	.uleb128 0xf
	.long	.LASF53
	.uleb128 0x3
	.byte	0x8
	.long	0x2f3
	.uleb128 0xa
	.long	0x3b
	.long	0x30e
	.uleb128 0xb
	.long	0x6d
	.byte	0x13
	.byte	0
	.uleb128 0x3
	.byte	0x8
	.long	0x42
	.uleb128 0x5
	.long	0x30e
	.uleb128 0x7
	.long	.LASF54
	.byte	0x8
	.byte	0x41
	.byte	0x13
	.long	0xfe
	.uleb128 0x7
	.long	.LASF55
	.byte	0x8
	.byte	0x4d
	.byte	0x13
	.long	0x11a
	.uleb128 0x2
	.long	.LASF56
	.byte	0x8
	.byte	0x89
	.byte	0xe
	.long	0x33d
	.uleb128 0x3
	.byte	0x8
	.long	0x2ad
	.uleb128 0x2
	.long	.LASF57
	.byte	0x8
	.byte	0x8a
	.byte	0xe
	.long	0x33d
	.uleb128 0x2
	.long	.LASF58
	.byte	0x8
	.byte	0x8b
	.byte	0xe
	.long	0x33d
	.uleb128 0x2
	.long	.LASF59
	.byte	0x9
	.byte	0x1a
	.byte	0xc
	.long	0x53
	.uleb128 0xa
	.long	0x314
	.long	0x372
	.uleb128 0x10
	.byte	0
	.uleb128 0x5
	.long	0x367
	.uleb128 0x2
	.long	.LASF60
	.byte	0x9
	.byte	0x1b
	.byte	0x1a
	.long	0x372
	.uleb128 0x2
	.long	.LASF61
	.byte	0x9
	.byte	0x1e
	.byte	0xc
	.long	0x53
	.uleb128 0x2
	.long	.LASF62
	.byte	0x9
	.byte	0x1f
	.byte	0x1a
	.long	0x372
	.uleb128 0x7
	.long	.LASF63
	.byte	0xa
	.byte	0x18
	.byte	0x13
	.long	0x97
	.uleb128 0x7
	.long	.LASF64
	.byte	0xa
	.byte	0x19
	.byte	0x14
	.long	0xaa
	.uleb128 0x7
	.long	.LASF65
	.byte	0xa
	.byte	0x1a
	.byte	0x14
	.long	0xb6
	.uleb128 0x7
	.long	.LASF66
	.byte	0xa
	.byte	0x1b
	.byte	0x14
	.long	0xc2
	.uleb128 0x7
	.long	.LASF67
	.byte	0xb
	.byte	0x40
	.byte	0x11
	.long	0xda
	.uleb128 0x7
	.long	.LASF68
	.byte	0xb
	.byte	0x45
	.byte	0x12
	.long	0xe6
	.uleb128 0x7
	.long	.LASF69
	.byte	0xb
	.byte	0x4f
	.byte	0x11
	.long	0xce
	.uleb128 0xc
	.long	.LASF71
	.byte	0x10
	.byte	0xc
	.byte	0x31
	.byte	0x10
	.long	0x417
	.uleb128 0xd
	.long	.LASF72
	.byte	0xc
	.byte	0x33
	.byte	0x23
	.long	0x417
	.byte	0
	.uleb128 0xd
	.long	.LASF73
	.byte	0xc
	.byte	0x34
	.byte	0x23
	.long	0x417
	.byte	0x8
	.byte	0
	.uleb128 0x3
	.byte	0x8
	.long	0x3ef
	.uleb128 0x7
	.long	.LASF74
	.byte	0xc
	.byte	0x35
	.byte	0x3
	.long	0x3ef
	.uleb128 0xc
	.long	.LASF75
	.byte	0x28
	.byte	0xd
	.byte	0x16
	.byte	0x8
	.long	0x49f
	.uleb128 0xd
	.long	.LASF76
	.byte	0xd
	.byte	0x18
	.byte	0x7
	.long	0x53
	.byte	0
	.uleb128 0xd
	.long	.LASF77
	.byte	0xd
	.byte	0x19
	.byte	0x10
	.long	0x74
	.byte	0x4
	.uleb128 0xd
	.long	.LASF78
	.byte	0xd
	.byte	0x1a
	.byte	0x7
	.long	0x53
	.byte	0x8
	.uleb128 0xd
	.long	.LASF79
	.byte	0xd
	.byte	0x1c
	.byte	0x10
	.long	0x74
	.byte	0xc
	.uleb128 0xd
	.long	.LASF80
	.byte	0xd
	.byte	0x20
	.byte	0x7
	.long	0x53
	.byte	0x10
	.uleb128 0xd
	.long	.LASF81
	.byte	0xd
	.byte	0x22
	.byte	0x9
	.long	0xa3
	.byte	0x14
	.uleb128 0xd
	.long	.LASF82
	.byte	0xd
	.byte	0x23
	.byte	0x9
	.long	0xa3
	.byte	0x16
	.uleb128 0xd
	.long	.LASF83
	.byte	0xd
	.byte	0x24
	.byte	0x14
	.long	0x41d
	.byte	0x18
	.byte	0
	.uleb128 0xc
	.long	.LASF84
	.byte	0x38
	.byte	0xe
	.byte	0x17
	.byte	0x8
	.long	0x549
	.uleb128 0xd
	.long	.LASF85
	.byte	0xe
	.byte	0x19
	.byte	0x10
	.long	0x74
	.byte	0
	.uleb128 0xd
	.long	.LASF86
	.byte	0xe
	.byte	0x1a
	.byte	0x10
	.long	0x74
	.byte	0x4
	.uleb128 0xd
	.long	.LASF87
	.byte	0xe
	.byte	0x1b
	.byte	0x10
	.long	0x74
	.byte	0x8
	.uleb128 0xd
	.long	.LASF88
	.byte	0xe
	.byte	0x1c
	.byte	0x10
	.long	0x74
	.byte	0xc
	.uleb128 0xd
	.long	.LASF89
	.byte	0xe
	.byte	0x1d
	.byte	0x10
	.long	0x74
	.byte	0x10
	.uleb128 0xd
	.long	.LASF90
	.byte	0xe
	.byte	0x1e
	.byte	0x10
	.long	0x74
	.byte	0x14
	.uleb128 0xd
	.long	.LASF91
	.byte	0xe
	.byte	0x20
	.byte	0x7
	.long	0x53
	.byte	0x18
	.uleb128 0xd
	.long	.LASF92
	.byte	0xe
	.byte	0x21
	.byte	0x7
	.long	0x53
	.byte	0x1c
	.uleb128 0xd
	.long	.LASF93
	.byte	0xe
	.byte	0x22
	.byte	0xf
	.long	0x90
	.byte	0x20
	.uleb128 0xd
	.long	.LASF94
	.byte	0xe
	.byte	0x27
	.byte	0x11
	.long	0x549
	.byte	0x21
	.uleb128 0xd
	.long	.LASF95
	.byte	0xe
	.byte	0x2a
	.byte	0x15
	.long	0x6d
	.byte	0x28
	.uleb128 0xd
	.long	.LASF96
	.byte	0xe
	.byte	0x2d
	.byte	0x10
	.long	0x74
	.byte	0x30
	.byte	0
	.uleb128 0xa
	.long	0x82
	.long	0x559
	.uleb128 0xb
	.long	0x6d
	.byte	0x6
	.byte	0
	.uleb128 0x4
	.byte	0x8
	.byte	0x7
	.long	.LASF97
	.uleb128 0xa
	.long	0x3b
	.long	0x570
	.uleb128 0xb
	.long	0x6d
	.byte	0x37
	.byte	0
	.uleb128 0x11
	.byte	0x28
	.byte	0xf
	.byte	0x43
	.byte	0x9
	.long	0x59e
	.uleb128 0x12
	.long	.LASF98
	.byte	0xf
	.byte	0x45
	.byte	0x1c
	.long	0x429
	.uleb128 0x12
	.long	.LASF99
	.byte	0xf
	.byte	0x46
	.byte	0x8
	.long	0x59e
	.uleb128 0x12
	.long	.LASF100
	.byte	0xf
	.byte	0x47
	.byte	0xc
	.long	0x5a
	.byte	0
	.uleb128 0xa
	.long	0x3b
	.long	0x5ae
	.uleb128 0xb
	.long	0x6d
	.byte	0x27
	.byte	0
	.uleb128 0x7
	.long	.LASF101
	.byte	0xf
	.byte	0x48
	.byte	0x3
	.long	0x570
	.uleb128 0x4
	.byte	0x8
	.byte	0x5
	.long	.LASF102
	.uleb128 0x11
	.byte	0x38
	.byte	0xf
	.byte	0x56
	.byte	0x9
	.long	0x5ef
	.uleb128 0x12
	.long	.LASF98
	.byte	0xf
	.byte	0x58
	.byte	0x22
	.long	0x49f
	.uleb128 0x12
	.long	.LASF99
	.byte	0xf
	.byte	0x59
	.byte	0x8
	.long	0x560
	.uleb128 0x12
	.long	.LASF100
	.byte	0xf
	.byte	0x5a
	.byte	0xc
	.long	0x5a
	.byte	0
	.uleb128 0x7
	.long	.LASF103
	.byte	0xf
	.byte	0x5b
	.byte	0x3
	.long	0x5c1
	.uleb128 0x7
	.long	.LASF104
	.byte	0x10
	.byte	0x1c
	.byte	0x1c
	.long	0x89
	.uleb128 0xc
	.long	.LASF105
	.byte	0x10
	.byte	0x11
	.byte	0xb2
	.byte	0x8
	.long	0x62f
	.uleb128 0xd
	.long	.LASF106
	.byte	0x11
	.byte	0xb4
	.byte	0x11
	.long	0x5fb
	.byte	0
	.uleb128 0xd
	.long	.LASF107
	.byte	0x11
	.byte	0xb5
	.byte	0xa
	.long	0x634
	.byte	0x2
	.byte	0
	.uleb128 0x5
	.long	0x607
	.uleb128 0xa
	.long	0x3b
	.long	0x644
	.uleb128 0xb
	.long	0x6d
	.byte	0xd
	.byte	0
	.uleb128 0x3
	.byte	0x8
	.long	0x607
	.uleb128 0x9
	.long	0x644
	.uleb128 0xf
	.long	.LASF108
	.uleb128 0x5
	.long	0x64f
	.uleb128 0x3
	.byte	0x8
	.long	0x64f
	.uleb128 0x9
	.long	0x659
	.uleb128 0xf
	.long	.LASF109
	.uleb128 0x5
	.long	0x664
	.uleb128 0x3
	.byte	0x8
	.long	0x664
	.uleb128 0x9
	.long	0x66e
	.uleb128 0xf
	.long	.LASF110
	.uleb128 0x5
	.long	0x679
	.uleb128 0x3
	.byte	0x8
	.long	0x679
	.uleb128 0x9
	.long	0x683
	.uleb128 0xf
	.long	.LASF111
	.uleb128 0x5
	.long	0x68e
	.uleb128 0x3
	.byte	0x8
	.long	0x68e
	.uleb128 0x9
	.long	0x698
	.uleb128 0xc
	.long	.LASF112
	.byte	0x10
	.byte	0x12
	.byte	0xee
	.byte	0x8
	.long	0x6e5
	.uleb128 0xd
	.long	.LASF113
	.byte	0x12
	.byte	0xf0
	.byte	0x11
	.long	0x5fb
	.byte	0
	.uleb128 0xd
	.long	.LASF114
	.byte	0x12
	.byte	0xf1
	.byte	0xf
	.long	0x88c
	.byte	0x2
	.uleb128 0xd
	.long	.LASF115
	.byte	0x12
	.byte	0xf2
	.byte	0x14
	.long	0x871
	.byte	0x4
	.uleb128 0xd
	.long	.LASF116
	.byte	0x12
	.byte	0xf5
	.byte	0x13
	.long	0x92e
	.byte	0x8
	.byte	0
	.uleb128 0x5
	.long	0x6a3
	.uleb128 0x3
	.byte	0x8
	.long	0x6a3
	.uleb128 0x9
	.long	0x6ea
	.uleb128 0xc
	.long	.LASF117
	.byte	0x1c
	.byte	0x12
	.byte	0xfd
	.byte	0x8
	.long	0x748
	.uleb128 0xd
	.long	.LASF118
	.byte	0x12
	.byte	0xff
	.byte	0x11
	.long	0x5fb
	.byte	0
	.uleb128 0x13
	.long	.LASF119
	.byte	0x12
	.value	0x100
	.byte	0xf
	.long	0x88c
	.byte	0x2
	.uleb128 0x13
	.long	.LASF120
	.byte	0x12
	.value	0x101
	.byte	0xe
	.long	0x3b3
	.byte	0x4
	.uleb128 0x13
	.long	.LASF121
	.byte	0x12
	.value	0x102
	.byte	0x15
	.long	0x8f6
	.byte	0x8
	.uleb128 0x13
	.long	.LASF122
	.byte	0x12
	.value	0x103
	.byte	0xe
	.long	0x3b3
	.byte	0x18
	.byte	0
	.uleb128 0x5
	.long	0x6f5
	.uleb128 0x3
	.byte	0x8
	.long	0x6f5
	.uleb128 0x9
	.long	0x74d
	.uleb128 0xf
	.long	.LASF123
	.uleb128 0x5
	.long	0x758
	.uleb128 0x3
	.byte	0x8
	.long	0x758
	.uleb128 0x9
	.long	0x762
	.uleb128 0xf
	.long	.LASF124
	.uleb128 0x5
	.long	0x76d
	.uleb128 0x3
	.byte	0x8
	.long	0x76d
	.uleb128 0x9
	.long	0x777
	.uleb128 0xf
	.long	.LASF125
	.uleb128 0x5
	.long	0x782
	.uleb128 0x3
	.byte	0x8
	.long	0x782
	.uleb128 0x9
	.long	0x78c
	.uleb128 0xf
	.long	.LASF126
	.uleb128 0x5
	.long	0x797
	.uleb128 0x3
	.byte	0x8
	.long	0x797
	.uleb128 0x9
	.long	0x7a1
	.uleb128 0xf
	.long	.LASF127
	.uleb128 0x5
	.long	0x7ac
	.uleb128 0x3
	.byte	0x8
	.long	0x7ac
	.uleb128 0x9
	.long	0x7b6
	.uleb128 0xf
	.long	.LASF128
	.uleb128 0x5
	.long	0x7c1
	.uleb128 0x3
	.byte	0x8
	.long	0x7c1
	.uleb128 0x9
	.long	0x7cb
	.uleb128 0x3
	.byte	0x8
	.long	0x62f
	.uleb128 0x9
	.long	0x7d6
	.uleb128 0x3
	.byte	0x8
	.long	0x654
	.uleb128 0x9
	.long	0x7e1
	.uleb128 0x3
	.byte	0x8
	.long	0x669
	.uleb128 0x9
	.long	0x7ec
	.uleb128 0x3
	.byte	0x8
	.long	0x67e
	.uleb128 0x9
	.long	0x7f7
	.uleb128 0x3
	.byte	0x8
	.long	0x693
	.uleb128 0x9
	.long	0x802
	.uleb128 0x3
	.byte	0x8
	.long	0x6e5
	.uleb128 0x9
	.long	0x80d
	.uleb128 0x3
	.byte	0x8
	.long	0x748
	.uleb128 0x9
	.long	0x818
	.uleb128 0x3
	.byte	0x8
	.long	0x75d
	.uleb128 0x9
	.long	0x823
	.uleb128 0x3
	.byte	0x8
	.long	0x772
	.uleb128 0x9
	.long	0x82e
	.uleb128 0x3
	.byte	0x8
	.long	0x787
	.uleb128 0x9
	.long	0x839
	.uleb128 0x3
	.byte	0x8
	.long	0x79c
	.uleb128 0x9
	.long	0x844
	.uleb128 0x3
	.byte	0x8
	.long	0x7b1
	.uleb128 0x9
	.long	0x84f
	.uleb128 0x3
	.byte	0x8
	.long	0x7c6
	.uleb128 0x9
	.long	0x85a
	.uleb128 0x7
	.long	.LASF129
	.byte	0x12
	.byte	0x1e
	.byte	0x12
	.long	0x3b3
	.uleb128 0xc
	.long	.LASF130
	.byte	0x4
	.byte	0x12
	.byte	0x1f
	.byte	0x8
	.long	0x88c
	.uleb128 0xd
	.long	.LASF131
	.byte	0x12
	.byte	0x21
	.byte	0xf
	.long	0x865
	.byte	0
	.byte	0
	.uleb128 0x7
	.long	.LASF132
	.byte	0x12
	.byte	0x77
	.byte	0x12
	.long	0x3a7
	.uleb128 0x11
	.byte	0x10
	.byte	0x12
	.byte	0xd6
	.byte	0x5
	.long	0x8c6
	.uleb128 0x12
	.long	.LASF133
	.byte	0x12
	.byte	0xd8
	.byte	0xa
	.long	0x8c6
	.uleb128 0x12
	.long	.LASF134
	.byte	0x12
	.byte	0xd9
	.byte	0xb
	.long	0x8d6
	.uleb128 0x12
	.long	.LASF135
	.byte	0x12
	.byte	0xda
	.byte	0xb
	.long	0x8e6
	.byte	0
	.uleb128 0xa
	.long	0x39b
	.long	0x8d6
	.uleb128 0xb
	.long	0x6d
	.byte	0xf
	.byte	0
	.uleb128 0xa
	.long	0x3a7
	.long	0x8e6
	.uleb128 0xb
	.long	0x6d
	.byte	0x7
	.byte	0
	.uleb128 0xa
	.long	0x3b3
	.long	0x8f6
	.uleb128 0xb
	.long	0x6d
	.byte	0x3
	.byte	0
	.uleb128 0xc
	.long	.LASF136
	.byte	0x10
	.byte	0x12
	.byte	0xd4
	.byte	0x8
	.long	0x911
	.uleb128 0xd
	.long	.LASF137
	.byte	0x12
	.byte	0xdb
	.byte	0x9
	.long	0x898
	.byte	0
	.byte	0
	.uleb128 0x5
	.long	0x8f6
	.uleb128 0x2
	.long	.LASF138
	.byte	0x12
	.byte	0xe4
	.byte	0x1e
	.long	0x911
	.uleb128 0x2
	.long	.LASF139
	.byte	0x12
	.byte	0xe5
	.byte	0x1e
	.long	0x911
	.uleb128 0xa
	.long	0x82
	.long	0x93e
	.uleb128 0xb
	.long	0x6d
	.byte	0x7
	.byte	0
	.uleb128 0x14
	.uleb128 0x3
	.byte	0x8
	.long	0x93e
	.uleb128 0xa
	.long	0x314
	.long	0x955
	.uleb128 0xb
	.long	0x6d
	.byte	0x40
	.byte	0
	.uleb128 0x5
	.long	0x945
	.uleb128 0x15
	.long	.LASF140
	.byte	0x13
	.value	0x11e
	.byte	0x1a
	.long	0x955
	.uleb128 0x15
	.long	.LASF141
	.byte	0x13
	.value	0x11f
	.byte	0x1a
	.long	0x955
	.uleb128 0xa
	.long	0x35
	.long	0x984
	.uleb128 0xb
	.long	0x6d
	.byte	0x1
	.byte	0
	.uleb128 0x2
	.long	.LASF142
	.byte	0x14
	.byte	0x9f
	.byte	0xe
	.long	0x974
	.uleb128 0x2
	.long	.LASF143
	.byte	0x14
	.byte	0xa0
	.byte	0xc
	.long	0x53
	.uleb128 0x2
	.long	.LASF144
	.byte	0x14
	.byte	0xa1
	.byte	0x11
	.long	0x5a
	.uleb128 0x2
	.long	.LASF145
	.byte	0x14
	.byte	0xa6
	.byte	0xe
	.long	0x974
	.uleb128 0x2
	.long	.LASF146
	.byte	0x14
	.byte	0xae
	.byte	0xc
	.long	0x53
	.uleb128 0x2
	.long	.LASF147
	.byte	0x14
	.byte	0xaf
	.byte	0x11
	.long	0x5a
	.uleb128 0x15
	.long	.LASF148
	.byte	0x14
	.value	0x112
	.byte	0xc
	.long	0x53
	.uleb128 0xa
	.long	0x7b
	.long	0x9e9
	.uleb128 0xb
	.long	0x6d
	.byte	0x3
	.byte	0
	.uleb128 0xc
	.long	.LASF149
	.byte	0x28
	.byte	0x15
	.byte	0x1e
	.byte	0x8
	.long	0xa2a
	.uleb128 0xd
	.long	.LASF150
	.byte	0x15
	.byte	0x1f
	.byte	0xa
	.long	0xa3b
	.byte	0
	.uleb128 0xd
	.long	.LASF151
	.byte	0x15
	.byte	0x20
	.byte	0xa
	.long	0xa51
	.byte	0x8
	.uleb128 0xd
	.long	.LASF152
	.byte	0x15
	.byte	0x21
	.byte	0x15
	.long	0xc76
	.byte	0x10
	.uleb128 0x16
	.string	"wq"
	.byte	0x15
	.byte	0x22
	.byte	0x9
	.long	0xc7c
	.byte	0x18
	.byte	0
	.uleb128 0x17
	.long	0xa35
	.uleb128 0x18
	.long	0xa35
	.byte	0
	.uleb128 0x3
	.byte	0x8
	.long	0x9e9
	.uleb128 0x3
	.byte	0x8
	.long	0xa2a
	.uleb128 0x17
	.long	0xa51
	.uleb128 0x18
	.long	0xa35
	.uleb128 0x18
	.long	0x53
	.byte	0
	.uleb128 0x3
	.byte	0x8
	.long	0xa41
	.uleb128 0x19
	.long	.LASF153
	.value	0x350
	.byte	0x16
	.value	0x6ea
	.byte	0x8
	.long	0xc76
	.uleb128 0x13
	.long	.LASF154
	.byte	0x16
	.value	0x6ec
	.byte	0x9
	.long	0x7b
	.byte	0
	.uleb128 0x13
	.long	.LASF155
	.byte	0x16
	.value	0x6ee
	.byte	0x10
	.long	0x74
	.byte	0x8
	.uleb128 0x13
	.long	.LASF156
	.byte	0x16
	.value	0x6ef
	.byte	0x9
	.long	0xc7c
	.byte	0x10
	.uleb128 0x13
	.long	.LASF157
	.byte	0x16
	.value	0x6f3
	.byte	0x5
	.long	0x19ed
	.byte	0x20
	.uleb128 0x13
	.long	.LASF158
	.byte	0x16
	.value	0x6f5
	.byte	0x10
	.long	0x74
	.byte	0x30
	.uleb128 0x13
	.long	.LASF159
	.byte	0x16
	.value	0x6f6
	.byte	0x11
	.long	0x6d
	.byte	0x38
	.uleb128 0x13
	.long	.LASF160
	.byte	0x16
	.value	0x6f6
	.byte	0x1c
	.long	0x53
	.byte	0x40
	.uleb128 0x13
	.long	.LASF161
	.byte	0x16
	.value	0x6f6
	.byte	0x2e
	.long	0xc7c
	.byte	0x48
	.uleb128 0x13
	.long	.LASF162
	.byte	0x16
	.value	0x6f6
	.byte	0x46
	.long	0xc7c
	.byte	0x58
	.uleb128 0x13
	.long	.LASF163
	.byte	0x16
	.value	0x6f6
	.byte	0x63
	.long	0x1a3c
	.byte	0x68
	.uleb128 0x13
	.long	.LASF164
	.byte	0x16
	.value	0x6f6
	.byte	0x7a
	.long	0x74
	.byte	0x70
	.uleb128 0x13
	.long	.LASF165
	.byte	0x16
	.value	0x6f6
	.byte	0x92
	.long	0x74
	.byte	0x74
	.uleb128 0x1a
	.string	"wq"
	.byte	0x16
	.value	0x6f6
	.byte	0x9e
	.long	0xc7c
	.byte	0x78
	.uleb128 0x13
	.long	.LASF166
	.byte	0x16
	.value	0x6f6
	.byte	0xb0
	.long	0xd5f
	.byte	0x88
	.uleb128 0x13
	.long	.LASF167
	.byte	0x16
	.value	0x6f6
	.byte	0xc5
	.long	0x1206
	.byte	0xb0
	.uleb128 0x1b
	.long	.LASF168
	.byte	0x16
	.value	0x6f6
	.byte	0xdb
	.long	0xd6b
	.value	0x130
	.uleb128 0x1b
	.long	.LASF169
	.byte	0x16
	.value	0x6f6
	.byte	0xf6
	.long	0x1589
	.value	0x168
	.uleb128 0x1c
	.long	.LASF170
	.byte	0x16
	.value	0x6f6
	.value	0x10d
	.long	0xc7c
	.value	0x170
	.uleb128 0x1c
	.long	.LASF171
	.byte	0x16
	.value	0x6f6
	.value	0x127
	.long	0xc7c
	.value	0x180
	.uleb128 0x1c
	.long	.LASF172
	.byte	0x16
	.value	0x6f6
	.value	0x141
	.long	0xc7c
	.value	0x190
	.uleb128 0x1c
	.long	.LASF173
	.byte	0x16
	.value	0x6f6
	.value	0x159
	.long	0xc7c
	.value	0x1a0
	.uleb128 0x1c
	.long	.LASF174
	.byte	0x16
	.value	0x6f6
	.value	0x170
	.long	0xc7c
	.value	0x1b0
	.uleb128 0x1c
	.long	.LASF175
	.byte	0x16
	.value	0x6f6
	.value	0x189
	.long	0x93f
	.value	0x1c0
	.uleb128 0x1c
	.long	.LASF176
	.byte	0x16
	.value	0x6f6
	.value	0x1a7
	.long	0xd13
	.value	0x1c8
	.uleb128 0x1c
	.long	.LASF177
	.byte	0x16
	.value	0x6f6
	.value	0x1bd
	.long	0x53
	.value	0x200
	.uleb128 0x1c
	.long	.LASF178
	.byte	0x16
	.value	0x6f6
	.value	0x1f2
	.long	0x1a12
	.value	0x208
	.uleb128 0x1c
	.long	.LASF179
	.byte	0x16
	.value	0x6f6
	.value	0x207
	.long	0x3bf
	.value	0x218
	.uleb128 0x1c
	.long	.LASF180
	.byte	0x16
	.value	0x6f6
	.value	0x21f
	.long	0x3bf
	.value	0x220
	.uleb128 0x1c
	.long	.LASF181
	.byte	0x16
	.value	0x6f6
	.value	0x229
	.long	0x10a
	.value	0x228
	.uleb128 0x1c
	.long	.LASF182
	.byte	0x16
	.value	0x6f6
	.value	0x244
	.long	0xd13
	.value	0x230
	.uleb128 0x1c
	.long	.LASF183
	.byte	0x16
	.value	0x6f6
	.value	0x263
	.long	0x1350
	.value	0x268
	.uleb128 0x1c
	.long	.LASF184
	.byte	0x16
	.value	0x6f6
	.value	0x276
	.long	0x53
	.value	0x300
	.uleb128 0x1c
	.long	.LASF185
	.byte	0x16
	.value	0x6f6
	.value	0x28a
	.long	0xd13
	.value	0x308
	.uleb128 0x1c
	.long	.LASF186
	.byte	0x16
	.value	0x6f6
	.value	0x2a6
	.long	0x7b
	.value	0x340
	.uleb128 0x1c
	.long	.LASF187
	.byte	0x16
	.value	0x6f6
	.value	0x2bc
	.long	0x53
	.value	0x348
	.byte	0
	.uleb128 0x3
	.byte	0x8
	.long	0xa57
	.uleb128 0xa
	.long	0x7b
	.long	0xc8c
	.uleb128 0xb
	.long	0x6d
	.byte	0x1
	.byte	0
	.uleb128 0x7
	.long	.LASF188
	.byte	0x17
	.byte	0x59
	.byte	0x10
	.long	0xc98
	.uleb128 0x3
	.byte	0x8
	.long	0xc9e
	.uleb128 0x17
	.long	0xcb3
	.uleb128 0x18
	.long	0xc76
	.uleb128 0x18
	.long	0xcb3
	.uleb128 0x18
	.long	0x74
	.byte	0
	.uleb128 0x3
	.byte	0x8
	.long	0xcb9
	.uleb128 0xc
	.long	.LASF189
	.byte	0x38
	.byte	0x17
	.byte	0x5e
	.byte	0x8
	.long	0xd13
	.uleb128 0x16
	.string	"cb"
	.byte	0x17
	.byte	0x5f
	.byte	0xd
	.long	0xc8c
	.byte	0
	.uleb128 0xd
	.long	.LASF161
	.byte	0x17
	.byte	0x60
	.byte	0x9
	.long	0xc7c
	.byte	0x8
	.uleb128 0xd
	.long	.LASF162
	.byte	0x17
	.byte	0x61
	.byte	0x9
	.long	0xc7c
	.byte	0x18
	.uleb128 0xd
	.long	.LASF190
	.byte	0x17
	.byte	0x62
	.byte	0x10
	.long	0x74
	.byte	0x28
	.uleb128 0xd
	.long	.LASF191
	.byte	0x17
	.byte	0x63
	.byte	0x10
	.long	0x74
	.byte	0x2c
	.uleb128 0x16
	.string	"fd"
	.byte	0x17
	.byte	0x64
	.byte	0x7
	.long	0x53
	.byte	0x30
	.byte	0
	.uleb128 0x7
	.long	.LASF192
	.byte	0x17
	.byte	0x5c
	.byte	0x19
	.long	0xcb9
	.uleb128 0xc
	.long	.LASF193
	.byte	0x10
	.byte	0x17
	.byte	0x79
	.byte	0x10
	.long	0xd47
	.uleb128 0xd
	.long	.LASF194
	.byte	0x17
	.byte	0x7a
	.byte	0x9
	.long	0x35
	.byte	0
	.uleb128 0x16
	.string	"len"
	.byte	0x17
	.byte	0x7b
	.byte	0xa
	.long	0x61
	.byte	0x8
	.byte	0
	.uleb128 0x7
	.long	.LASF193
	.byte	0x17
	.byte	0x7c
	.byte	0x3
	.long	0xd1f
	.uleb128 0x7
	.long	.LASF195
	.byte	0x17
	.byte	0x7e
	.byte	0xd
	.long	0x53
	.uleb128 0x7
	.long	.LASF196
	.byte	0x17
	.byte	0x87
	.byte	0x19
	.long	0x5ae
	.uleb128 0x7
	.long	.LASF197
	.byte	0x17
	.byte	0x88
	.byte	0x1a
	.long	0x5ef
	.uleb128 0x7
	.long	.LASF198
	.byte	0x17
	.byte	0xa6
	.byte	0xf
	.long	0x3cb
	.uleb128 0x7
	.long	.LASF199
	.byte	0x17
	.byte	0xa7
	.byte	0xf
	.long	0x3e3
	.uleb128 0x1d
	.byte	0x5
	.byte	0x4
	.long	0x53
	.byte	0x16
	.byte	0xb6
	.byte	0xe
	.long	0xfb2
	.uleb128 0x1e
	.long	.LASF200
	.sleb128 -7
	.uleb128 0x1e
	.long	.LASF201
	.sleb128 -13
	.uleb128 0x1e
	.long	.LASF202
	.sleb128 -98
	.uleb128 0x1e
	.long	.LASF203
	.sleb128 -99
	.uleb128 0x1e
	.long	.LASF204
	.sleb128 -97
	.uleb128 0x1e
	.long	.LASF205
	.sleb128 -11
	.uleb128 0x1e
	.long	.LASF206
	.sleb128 -3000
	.uleb128 0x1e
	.long	.LASF207
	.sleb128 -3001
	.uleb128 0x1e
	.long	.LASF208
	.sleb128 -3002
	.uleb128 0x1e
	.long	.LASF209
	.sleb128 -3013
	.uleb128 0x1e
	.long	.LASF210
	.sleb128 -3003
	.uleb128 0x1e
	.long	.LASF211
	.sleb128 -3004
	.uleb128 0x1e
	.long	.LASF212
	.sleb128 -3005
	.uleb128 0x1e
	.long	.LASF213
	.sleb128 -3006
	.uleb128 0x1e
	.long	.LASF214
	.sleb128 -3007
	.uleb128 0x1e
	.long	.LASF215
	.sleb128 -3008
	.uleb128 0x1e
	.long	.LASF216
	.sleb128 -3009
	.uleb128 0x1e
	.long	.LASF217
	.sleb128 -3014
	.uleb128 0x1e
	.long	.LASF218
	.sleb128 -3010
	.uleb128 0x1e
	.long	.LASF219
	.sleb128 -3011
	.uleb128 0x1e
	.long	.LASF220
	.sleb128 -114
	.uleb128 0x1e
	.long	.LASF221
	.sleb128 -9
	.uleb128 0x1e
	.long	.LASF222
	.sleb128 -16
	.uleb128 0x1e
	.long	.LASF223
	.sleb128 -125
	.uleb128 0x1e
	.long	.LASF224
	.sleb128 -4080
	.uleb128 0x1e
	.long	.LASF225
	.sleb128 -103
	.uleb128 0x1e
	.long	.LASF226
	.sleb128 -111
	.uleb128 0x1e
	.long	.LASF227
	.sleb128 -104
	.uleb128 0x1e
	.long	.LASF228
	.sleb128 -89
	.uleb128 0x1e
	.long	.LASF229
	.sleb128 -17
	.uleb128 0x1e
	.long	.LASF230
	.sleb128 -14
	.uleb128 0x1e
	.long	.LASF231
	.sleb128 -27
	.uleb128 0x1e
	.long	.LASF232
	.sleb128 -113
	.uleb128 0x1e
	.long	.LASF233
	.sleb128 -4
	.uleb128 0x1e
	.long	.LASF234
	.sleb128 -22
	.uleb128 0x1e
	.long	.LASF235
	.sleb128 -5
	.uleb128 0x1e
	.long	.LASF236
	.sleb128 -106
	.uleb128 0x1e
	.long	.LASF237
	.sleb128 -21
	.uleb128 0x1e
	.long	.LASF238
	.sleb128 -40
	.uleb128 0x1e
	.long	.LASF239
	.sleb128 -24
	.uleb128 0x1e
	.long	.LASF240
	.sleb128 -90
	.uleb128 0x1e
	.long	.LASF241
	.sleb128 -36
	.uleb128 0x1e
	.long	.LASF242
	.sleb128 -100
	.uleb128 0x1e
	.long	.LASF243
	.sleb128 -101
	.uleb128 0x1e
	.long	.LASF244
	.sleb128 -23
	.uleb128 0x1e
	.long	.LASF245
	.sleb128 -105
	.uleb128 0x1e
	.long	.LASF246
	.sleb128 -19
	.uleb128 0x1e
	.long	.LASF247
	.sleb128 -2
	.uleb128 0x1e
	.long	.LASF248
	.sleb128 -12
	.uleb128 0x1e
	.long	.LASF249
	.sleb128 -64
	.uleb128 0x1e
	.long	.LASF250
	.sleb128 -92
	.uleb128 0x1e
	.long	.LASF251
	.sleb128 -28
	.uleb128 0x1e
	.long	.LASF252
	.sleb128 -38
	.uleb128 0x1e
	.long	.LASF253
	.sleb128 -107
	.uleb128 0x1e
	.long	.LASF254
	.sleb128 -20
	.uleb128 0x1e
	.long	.LASF255
	.sleb128 -39
	.uleb128 0x1e
	.long	.LASF256
	.sleb128 -88
	.uleb128 0x1e
	.long	.LASF257
	.sleb128 -95
	.uleb128 0x1e
	.long	.LASF258
	.sleb128 -1
	.uleb128 0x1e
	.long	.LASF259
	.sleb128 -32
	.uleb128 0x1e
	.long	.LASF260
	.sleb128 -71
	.uleb128 0x1e
	.long	.LASF261
	.sleb128 -93
	.uleb128 0x1e
	.long	.LASF262
	.sleb128 -91
	.uleb128 0x1e
	.long	.LASF263
	.sleb128 -34
	.uleb128 0x1e
	.long	.LASF264
	.sleb128 -30
	.uleb128 0x1e
	.long	.LASF265
	.sleb128 -108
	.uleb128 0x1e
	.long	.LASF266
	.sleb128 -29
	.uleb128 0x1e
	.long	.LASF267
	.sleb128 -3
	.uleb128 0x1e
	.long	.LASF268
	.sleb128 -110
	.uleb128 0x1e
	.long	.LASF269
	.sleb128 -26
	.uleb128 0x1e
	.long	.LASF270
	.sleb128 -18
	.uleb128 0x1e
	.long	.LASF271
	.sleb128 -4094
	.uleb128 0x1e
	.long	.LASF272
	.sleb128 -4095
	.uleb128 0x1e
	.long	.LASF273
	.sleb128 -6
	.uleb128 0x1e
	.long	.LASF274
	.sleb128 -31
	.uleb128 0x1e
	.long	.LASF275
	.sleb128 -112
	.uleb128 0x1e
	.long	.LASF276
	.sleb128 -121
	.uleb128 0x1e
	.long	.LASF277
	.sleb128 -25
	.uleb128 0x1e
	.long	.LASF278
	.sleb128 -4028
	.uleb128 0x1e
	.long	.LASF279
	.sleb128 -84
	.uleb128 0x1e
	.long	.LASF280
	.sleb128 -4096
	.byte	0
	.uleb128 0x1d
	.byte	0x7
	.byte	0x4
	.long	0x74
	.byte	0x16
	.byte	0xbd
	.byte	0xe
	.long	0x1033
	.uleb128 0x1f
	.long	.LASF281
	.byte	0
	.uleb128 0x1f
	.long	.LASF282
	.byte	0x1
	.uleb128 0x1f
	.long	.LASF283
	.byte	0x2
	.uleb128 0x1f
	.long	.LASF284
	.byte	0x3
	.uleb128 0x1f
	.long	.LASF285
	.byte	0x4
	.uleb128 0x1f
	.long	.LASF286
	.byte	0x5
	.uleb128 0x1f
	.long	.LASF287
	.byte	0x6
	.uleb128 0x1f
	.long	.LASF288
	.byte	0x7
	.uleb128 0x1f
	.long	.LASF289
	.byte	0x8
	.uleb128 0x1f
	.long	.LASF290
	.byte	0x9
	.uleb128 0x1f
	.long	.LASF291
	.byte	0xa
	.uleb128 0x1f
	.long	.LASF292
	.byte	0xb
	.uleb128 0x1f
	.long	.LASF293
	.byte	0xc
	.uleb128 0x1f
	.long	.LASF294
	.byte	0xd
	.uleb128 0x1f
	.long	.LASF295
	.byte	0xe
	.uleb128 0x1f
	.long	.LASF296
	.byte	0xf
	.uleb128 0x1f
	.long	.LASF297
	.byte	0x10
	.uleb128 0x1f
	.long	.LASF298
	.byte	0x11
	.uleb128 0x1f
	.long	.LASF299
	.byte	0x12
	.byte	0
	.uleb128 0x7
	.long	.LASF300
	.byte	0x16
	.byte	0xc4
	.byte	0x3
	.long	0xfb2
	.uleb128 0x1d
	.byte	0x7
	.byte	0x4
	.long	0x74
	.byte	0x16
	.byte	0xc6
	.byte	0xe
	.long	0x1096
	.uleb128 0x1f
	.long	.LASF301
	.byte	0
	.uleb128 0x1f
	.long	.LASF302
	.byte	0x1
	.uleb128 0x1f
	.long	.LASF303
	.byte	0x2
	.uleb128 0x1f
	.long	.LASF304
	.byte	0x3
	.uleb128 0x1f
	.long	.LASF305
	.byte	0x4
	.uleb128 0x1f
	.long	.LASF306
	.byte	0x5
	.uleb128 0x1f
	.long	.LASF307
	.byte	0x6
	.uleb128 0x1f
	.long	.LASF308
	.byte	0x7
	.uleb128 0x1f
	.long	.LASF309
	.byte	0x8
	.uleb128 0x1f
	.long	.LASF310
	.byte	0x9
	.uleb128 0x1f
	.long	.LASF311
	.byte	0xa
	.uleb128 0x1f
	.long	.LASF312
	.byte	0xb
	.byte	0
	.uleb128 0x7
	.long	.LASF313
	.byte	0x16
	.byte	0xcd
	.byte	0x3
	.long	0x103f
	.uleb128 0x7
	.long	.LASF314
	.byte	0x16
	.byte	0xd1
	.byte	0x1a
	.long	0xa57
	.uleb128 0x7
	.long	.LASF315
	.byte	0x16
	.byte	0xd2
	.byte	0x1c
	.long	0x10ba
	.uleb128 0x20
	.long	.LASF316
	.byte	0x60
	.byte	0x16
	.value	0x1bb
	.byte	0x8
	.long	0x1137
	.uleb128 0x13
	.long	.LASF154
	.byte	0x16
	.value	0x1bc
	.byte	0x9
	.long	0x7b
	.byte	0
	.uleb128 0x13
	.long	.LASF152
	.byte	0x16
	.value	0x1bc
	.byte	0x1a
	.long	0x17e6
	.byte	0x8
	.uleb128 0x13
	.long	.LASF317
	.byte	0x16
	.value	0x1bc
	.byte	0x2f
	.long	0x1033
	.byte	0x10
	.uleb128 0x13
	.long	.LASF318
	.byte	0x16
	.value	0x1bc
	.byte	0x41
	.long	0x1595
	.byte	0x18
	.uleb128 0x13
	.long	.LASF156
	.byte	0x16
	.value	0x1bc
	.byte	0x51
	.long	0xc7c
	.byte	0x20
	.uleb128 0x1a
	.string	"u"
	.byte	0x16
	.value	0x1bc
	.byte	0x87
	.long	0x17c2
	.byte	0x30
	.uleb128 0x13
	.long	.LASF319
	.byte	0x16
	.value	0x1bc
	.byte	0x97
	.long	0x1589
	.byte	0x50
	.uleb128 0x13
	.long	.LASF159
	.byte	0x16
	.value	0x1bc
	.byte	0xb2
	.long	0x74
	.byte	0x58
	.byte	0
	.uleb128 0x7
	.long	.LASF320
	.byte	0x16
	.byte	0xda
	.byte	0x1b
	.long	0x1143
	.uleb128 0x20
	.long	.LASF321
	.byte	0x98
	.byte	0x16
	.value	0x354
	.byte	0x8
	.long	0x1206
	.uleb128 0x13
	.long	.LASF154
	.byte	0x16
	.value	0x355
	.byte	0x9
	.long	0x7b
	.byte	0
	.uleb128 0x13
	.long	.LASF152
	.byte	0x16
	.value	0x355
	.byte	0x1a
	.long	0x17e6
	.byte	0x8
	.uleb128 0x13
	.long	.LASF317
	.byte	0x16
	.value	0x355
	.byte	0x2f
	.long	0x1033
	.byte	0x10
	.uleb128 0x13
	.long	.LASF318
	.byte	0x16
	.value	0x355
	.byte	0x41
	.long	0x1595
	.byte	0x18
	.uleb128 0x13
	.long	.LASF156
	.byte	0x16
	.value	0x355
	.byte	0x51
	.long	0xc7c
	.byte	0x20
	.uleb128 0x1a
	.string	"u"
	.byte	0x16
	.value	0x355
	.byte	0x87
	.long	0x1820
	.byte	0x30
	.uleb128 0x13
	.long	.LASF319
	.byte	0x16
	.value	0x355
	.byte	0x97
	.long	0x1589
	.byte	0x50
	.uleb128 0x13
	.long	.LASF159
	.byte	0x16
	.value	0x355
	.byte	0xb2
	.long	0x74
	.byte	0x58
	.uleb128 0x13
	.long	.LASF322
	.byte	0x16
	.value	0x356
	.byte	0xf
	.long	0x15b3
	.byte	0x60
	.uleb128 0x13
	.long	.LASF323
	.byte	0x16
	.value	0x356
	.byte	0x1f
	.long	0x1844
	.byte	0x68
	.uleb128 0x13
	.long	.LASF324
	.byte	0x16
	.value	0x356
	.byte	0x36
	.long	0x3bf
	.byte	0x80
	.uleb128 0x13
	.long	.LASF325
	.byte	0x16
	.value	0x356
	.byte	0x48
	.long	0x3bf
	.byte	0x88
	.uleb128 0x13
	.long	.LASF326
	.byte	0x16
	.value	0x356
	.byte	0x59
	.long	0x3bf
	.byte	0x90
	.byte	0
	.uleb128 0x7
	.long	.LASF327
	.byte	0x16
	.byte	0xde
	.byte	0x1b
	.long	0x1212
	.uleb128 0x20
	.long	.LASF328
	.byte	0x80
	.byte	0x16
	.value	0x344
	.byte	0x8
	.long	0x12b9
	.uleb128 0x13
	.long	.LASF154
	.byte	0x16
	.value	0x345
	.byte	0x9
	.long	0x7b
	.byte	0
	.uleb128 0x13
	.long	.LASF152
	.byte	0x16
	.value	0x345
	.byte	0x1a
	.long	0x17e6
	.byte	0x8
	.uleb128 0x13
	.long	.LASF317
	.byte	0x16
	.value	0x345
	.byte	0x2f
	.long	0x1033
	.byte	0x10
	.uleb128 0x13
	.long	.LASF318
	.byte	0x16
	.value	0x345
	.byte	0x41
	.long	0x1595
	.byte	0x18
	.uleb128 0x13
	.long	.LASF156
	.byte	0x16
	.value	0x345
	.byte	0x51
	.long	0xc7c
	.byte	0x20
	.uleb128 0x1a
	.string	"u"
	.byte	0x16
	.value	0x345
	.byte	0x87
	.long	0x17fc
	.byte	0x30
	.uleb128 0x13
	.long	.LASF319
	.byte	0x16
	.value	0x345
	.byte	0x97
	.long	0x1589
	.byte	0x50
	.uleb128 0x13
	.long	.LASF159
	.byte	0x16
	.value	0x345
	.byte	0xb2
	.long	0x74
	.byte	0x58
	.uleb128 0x13
	.long	.LASF329
	.byte	0x16
	.value	0x346
	.byte	0xf
	.long	0x15d7
	.byte	0x60
	.uleb128 0x13
	.long	.LASF330
	.byte	0x16
	.value	0x346
	.byte	0x1f
	.long	0xc7c
	.byte	0x68
	.uleb128 0x13
	.long	.LASF331
	.byte	0x16
	.value	0x346
	.byte	0x2d
	.long	0x53
	.byte	0x78
	.byte	0
	.uleb128 0x7
	.long	.LASF332
	.byte	0x16
	.byte	0xe1
	.byte	0x1d
	.long	0x12c5
	.uleb128 0x20
	.long	.LASF333
	.byte	0x68
	.byte	0x16
	.value	0x60b
	.byte	0x8
	.long	0x1350
	.uleb128 0x13
	.long	.LASF154
	.byte	0x16
	.value	0x60c
	.byte	0x9
	.long	0x7b
	.byte	0
	.uleb128 0x13
	.long	.LASF152
	.byte	0x16
	.value	0x60c
	.byte	0x1a
	.long	0x17e6
	.byte	0x8
	.uleb128 0x13
	.long	.LASF317
	.byte	0x16
	.value	0x60c
	.byte	0x2f
	.long	0x1033
	.byte	0x10
	.uleb128 0x13
	.long	.LASF318
	.byte	0x16
	.value	0x60c
	.byte	0x41
	.long	0x1595
	.byte	0x18
	.uleb128 0x13
	.long	.LASF156
	.byte	0x16
	.value	0x60c
	.byte	0x51
	.long	0xc7c
	.byte	0x20
	.uleb128 0x1a
	.string	"u"
	.byte	0x16
	.value	0x60c
	.byte	0x87
	.long	0x195c
	.byte	0x30
	.uleb128 0x13
	.long	.LASF319
	.byte	0x16
	.value	0x60c
	.byte	0x97
	.long	0x1589
	.byte	0x50
	.uleb128 0x13
	.long	.LASF159
	.byte	0x16
	.value	0x60c
	.byte	0xb2
	.long	0x74
	.byte	0x58
	.uleb128 0x13
	.long	.LASF334
	.byte	0x16
	.value	0x60e
	.byte	0x9
	.long	0x7b
	.byte	0x60
	.byte	0
	.uleb128 0x7
	.long	.LASF335
	.byte	0x16
	.byte	0xe2
	.byte	0x1c
	.long	0x135c
	.uleb128 0x20
	.long	.LASF336
	.byte	0x98
	.byte	0x16
	.value	0x61c
	.byte	0x8
	.long	0x141f
	.uleb128 0x13
	.long	.LASF154
	.byte	0x16
	.value	0x61d
	.byte	0x9
	.long	0x7b
	.byte	0
	.uleb128 0x13
	.long	.LASF152
	.byte	0x16
	.value	0x61d
	.byte	0x1a
	.long	0x17e6
	.byte	0x8
	.uleb128 0x13
	.long	.LASF317
	.byte	0x16
	.value	0x61d
	.byte	0x2f
	.long	0x1033
	.byte	0x10
	.uleb128 0x13
	.long	.LASF318
	.byte	0x16
	.value	0x61d
	.byte	0x41
	.long	0x1595
	.byte	0x18
	.uleb128 0x13
	.long	.LASF156
	.byte	0x16
	.value	0x61d
	.byte	0x51
	.long	0xc7c
	.byte	0x20
	.uleb128 0x1a
	.string	"u"
	.byte	0x16
	.value	0x61d
	.byte	0x87
	.long	0x1980
	.byte	0x30
	.uleb128 0x13
	.long	.LASF319
	.byte	0x16
	.value	0x61d
	.byte	0x97
	.long	0x1589
	.byte	0x50
	.uleb128 0x13
	.long	.LASF159
	.byte	0x16
	.value	0x61d
	.byte	0xb2
	.long	0x74
	.byte	0x58
	.uleb128 0x13
	.long	.LASF337
	.byte	0x16
	.value	0x61e
	.byte	0x10
	.long	0x1789
	.byte	0x60
	.uleb128 0x13
	.long	.LASF338
	.byte	0x16
	.value	0x61f
	.byte	0x7
	.long	0x53
	.byte	0x68
	.uleb128 0x13
	.long	.LASF339
	.byte	0x16
	.value	0x620
	.byte	0x7a
	.long	0x19a4
	.byte	0x70
	.uleb128 0x13
	.long	.LASF340
	.byte	0x16
	.value	0x620
	.byte	0x93
	.long	0x74
	.byte	0x90
	.uleb128 0x13
	.long	.LASF341
	.byte	0x16
	.value	0x620
	.byte	0xb0
	.long	0x74
	.byte	0x94
	.byte	0
	.uleb128 0x7
	.long	.LASF342
	.byte	0x16
	.byte	0xec
	.byte	0x18
	.long	0x142b
	.uleb128 0x19
	.long	.LASF343
	.value	0x1b8
	.byte	0x16
	.value	0x510
	.byte	0x8
	.long	0x1589
	.uleb128 0x13
	.long	.LASF154
	.byte	0x16
	.value	0x511
	.byte	0x9
	.long	0x7b
	.byte	0
	.uleb128 0x13
	.long	.LASF317
	.byte	0x16
	.value	0x511
	.byte	0x1b
	.long	0x1096
	.byte	0x8
	.uleb128 0x13
	.long	.LASF344
	.byte	0x16
	.value	0x511
	.byte	0x27
	.long	0x17b2
	.byte	0x10
	.uleb128 0x13
	.long	.LASF345
	.byte	0x16
	.value	0x512
	.byte	0xe
	.long	0x1948
	.byte	0x40
	.uleb128 0x13
	.long	.LASF152
	.byte	0x16
	.value	0x513
	.byte	0xe
	.long	0x17e6
	.byte	0x48
	.uleb128 0x1a
	.string	"cb"
	.byte	0x16
	.value	0x514
	.byte	0xc
	.long	0x15fb
	.byte	0x50
	.uleb128 0x13
	.long	.LASF346
	.byte	0x16
	.value	0x515
	.byte	0xb
	.long	0x325
	.byte	0x58
	.uleb128 0x1a
	.string	"ptr"
	.byte	0x16
	.value	0x516
	.byte	0x9
	.long	0x7b
	.byte	0x60
	.uleb128 0x13
	.long	.LASF347
	.byte	0x16
	.value	0x517
	.byte	0xf
	.long	0x30e
	.byte	0x68
	.uleb128 0x13
	.long	.LASF348
	.byte	0x16
	.value	0x518
	.byte	0xd
	.long	0x173e
	.byte	0x70
	.uleb128 0x1b
	.long	.LASF349
	.byte	0x16
	.value	0x519
	.byte	0xf
	.long	0x30e
	.value	0x110
	.uleb128 0x1b
	.long	.LASF350
	.byte	0x16
	.value	0x519
	.byte	0x21
	.long	0xd53
	.value	0x118
	.uleb128 0x1b
	.long	.LASF159
	.byte	0x16
	.value	0x519
	.byte	0x2b
	.long	0x53
	.value	0x11c
	.uleb128 0x1b
	.long	.LASF351
	.byte	0x16
	.value	0x519
	.byte	0x39
	.long	0x3d7
	.value	0x120
	.uleb128 0x1b
	.long	.LASF352
	.byte	0x16
	.value	0x519
	.byte	0x4c
	.long	0x74
	.value	0x124
	.uleb128 0x1b
	.long	.LASF353
	.byte	0x16
	.value	0x519
	.byte	0x5d
	.long	0x158f
	.value	0x128
	.uleb128 0x21
	.string	"off"
	.byte	0x16
	.value	0x519
	.byte	0x69
	.long	0x319
	.value	0x130
	.uleb128 0x21
	.string	"uid"
	.byte	0x16
	.value	0x519
	.byte	0x77
	.long	0xd83
	.value	0x138
	.uleb128 0x21
	.string	"gid"
	.byte	0x16
	.value	0x519
	.byte	0x85
	.long	0xd77
	.value	0x13c
	.uleb128 0x1b
	.long	.LASF354
	.byte	0x16
	.value	0x519
	.byte	0x91
	.long	0x1955
	.value	0x140
	.uleb128 0x1b
	.long	.LASF355
	.byte	0x16
	.value	0x519
	.byte	0x9f
	.long	0x1955
	.value	0x148
	.uleb128 0x1b
	.long	.LASF356
	.byte	0x16
	.value	0x519
	.byte	0xb6
	.long	0x9e9
	.value	0x150
	.uleb128 0x1b
	.long	.LASF357
	.byte	0x16
	.value	0x519
	.byte	0xc9
	.long	0x17ec
	.value	0x178
	.byte	0
	.uleb128 0x3
	.byte	0x8
	.long	0x10ae
	.uleb128 0x3
	.byte	0x8
	.long	0xd47
	.uleb128 0x22
	.long	.LASF358
	.byte	0x16
	.value	0x13e
	.byte	0x10
	.long	0x15a2
	.uleb128 0x3
	.byte	0x8
	.long	0x15a8
	.uleb128 0x17
	.long	0x15b3
	.uleb128 0x18
	.long	0x1589
	.byte	0
	.uleb128 0x22
	.long	.LASF359
	.byte	0x16
	.value	0x140
	.byte	0x10
	.long	0x15c0
	.uleb128 0x3
	.byte	0x8
	.long	0x15c6
	.uleb128 0x17
	.long	0x15d1
	.uleb128 0x18
	.long	0x15d1
	.byte	0
	.uleb128 0x3
	.byte	0x8
	.long	0x1137
	.uleb128 0x22
	.long	.LASF360
	.byte	0x16
	.value	0x141
	.byte	0x10
	.long	0x15e4
	.uleb128 0x3
	.byte	0x8
	.long	0x15ea
	.uleb128 0x17
	.long	0x15f5
	.uleb128 0x18
	.long	0x15f5
	.byte	0
	.uleb128 0x3
	.byte	0x8
	.long	0x1206
	.uleb128 0x22
	.long	.LASF361
	.byte	0x16
	.value	0x147
	.byte	0x10
	.long	0x1608
	.uleb128 0x3
	.byte	0x8
	.long	0x160e
	.uleb128 0x17
	.long	0x1619
	.uleb128 0x18
	.long	0x1619
	.byte	0
	.uleb128 0x3
	.byte	0x8
	.long	0x141f
	.uleb128 0x23
	.byte	0x10
	.byte	0x16
	.value	0x156
	.byte	0x9
	.long	0x1646
	.uleb128 0x13
	.long	.LASF362
	.byte	0x16
	.value	0x157
	.byte	0x8
	.long	0x5a
	.byte	0
	.uleb128 0x13
	.long	.LASF363
	.byte	0x16
	.value	0x158
	.byte	0x8
	.long	0x5a
	.byte	0x8
	.byte	0
	.uleb128 0x22
	.long	.LASF364
	.byte	0x16
	.value	0x159
	.byte	0x3
	.long	0x161f
	.uleb128 0x23
	.byte	0xa0
	.byte	0x16
	.value	0x15c
	.byte	0x9
	.long	0x173e
	.uleb128 0x13
	.long	.LASF365
	.byte	0x16
	.value	0x15d
	.byte	0xc
	.long	0x3bf
	.byte	0
	.uleb128 0x13
	.long	.LASF366
	.byte	0x16
	.value	0x15e
	.byte	0xc
	.long	0x3bf
	.byte	0x8
	.uleb128 0x13
	.long	.LASF367
	.byte	0x16
	.value	0x15f
	.byte	0xc
	.long	0x3bf
	.byte	0x10
	.uleb128 0x13
	.long	.LASF368
	.byte	0x16
	.value	0x160
	.byte	0xc
	.long	0x3bf
	.byte	0x18
	.uleb128 0x13
	.long	.LASF369
	.byte	0x16
	.value	0x161
	.byte	0xc
	.long	0x3bf
	.byte	0x20
	.uleb128 0x13
	.long	.LASF370
	.byte	0x16
	.value	0x162
	.byte	0xc
	.long	0x3bf
	.byte	0x28
	.uleb128 0x13
	.long	.LASF371
	.byte	0x16
	.value	0x163
	.byte	0xc
	.long	0x3bf
	.byte	0x30
	.uleb128 0x13
	.long	.LASF372
	.byte	0x16
	.value	0x164
	.byte	0xc
	.long	0x3bf
	.byte	0x38
	.uleb128 0x13
	.long	.LASF373
	.byte	0x16
	.value	0x165
	.byte	0xc
	.long	0x3bf
	.byte	0x40
	.uleb128 0x13
	.long	.LASF374
	.byte	0x16
	.value	0x166
	.byte	0xc
	.long	0x3bf
	.byte	0x48
	.uleb128 0x13
	.long	.LASF375
	.byte	0x16
	.value	0x167
	.byte	0xc
	.long	0x3bf
	.byte	0x50
	.uleb128 0x13
	.long	.LASF376
	.byte	0x16
	.value	0x168
	.byte	0xc
	.long	0x3bf
	.byte	0x58
	.uleb128 0x13
	.long	.LASF377
	.byte	0x16
	.value	0x169
	.byte	0x11
	.long	0x1646
	.byte	0x60
	.uleb128 0x13
	.long	.LASF378
	.byte	0x16
	.value	0x16a
	.byte	0x11
	.long	0x1646
	.byte	0x70
	.uleb128 0x13
	.long	.LASF379
	.byte	0x16
	.value	0x16b
	.byte	0x11
	.long	0x1646
	.byte	0x80
	.uleb128 0x13
	.long	.LASF380
	.byte	0x16
	.value	0x16c
	.byte	0x11
	.long	0x1646
	.byte	0x90
	.byte	0
	.uleb128 0x22
	.long	.LASF381
	.byte	0x16
	.value	0x16d
	.byte	0x3
	.long	0x1653
	.uleb128 0x5
	.long	0x173e
	.uleb128 0x22
	.long	.LASF382
	.byte	0x16
	.value	0x175
	.byte	0x10
	.long	0x175d
	.uleb128 0x3
	.byte	0x8
	.long	0x1763
	.uleb128 0x17
	.long	0x177d
	.uleb128 0x18
	.long	0x177d
	.uleb128 0x18
	.long	0x53
	.uleb128 0x18
	.long	0x1783
	.uleb128 0x18
	.long	0x1783
	.byte	0
	.uleb128 0x3
	.byte	0x8
	.long	0x12b9
	.uleb128 0x3
	.byte	0x8
	.long	0x174b
	.uleb128 0x22
	.long	.LASF383
	.byte	0x16
	.value	0x17a
	.byte	0x10
	.long	0x1796
	.uleb128 0x3
	.byte	0x8
	.long	0x179c
	.uleb128 0x17
	.long	0x17ac
	.uleb128 0x18
	.long	0x17ac
	.uleb128 0x18
	.long	0x53
	.byte	0
	.uleb128 0x3
	.byte	0x8
	.long	0x1350
	.uleb128 0xa
	.long	0x7b
	.long	0x17c2
	.uleb128 0xb
	.long	0x6d
	.byte	0x5
	.byte	0
	.uleb128 0x24
	.byte	0x20
	.byte	0x16
	.value	0x1bc
	.byte	0x62
	.long	0x17e6
	.uleb128 0x25
	.string	"fd"
	.byte	0x16
	.value	0x1bc
	.byte	0x6e
	.long	0x53
	.uleb128 0x26
	.long	.LASF344
	.byte	0x16
	.value	0x1bc
	.byte	0x78
	.long	0x9d9
	.byte	0
	.uleb128 0x3
	.byte	0x8
	.long	0x10a2
	.uleb128 0xa
	.long	0xd47
	.long	0x17fc
	.uleb128 0xb
	.long	0x6d
	.byte	0x3
	.byte	0
	.uleb128 0x24
	.byte	0x20
	.byte	0x16
	.value	0x345
	.byte	0x62
	.long	0x1820
	.uleb128 0x25
	.string	"fd"
	.byte	0x16
	.value	0x345
	.byte	0x6e
	.long	0x53
	.uleb128 0x26
	.long	.LASF344
	.byte	0x16
	.value	0x345
	.byte	0x78
	.long	0x9d9
	.byte	0
	.uleb128 0x24
	.byte	0x20
	.byte	0x16
	.value	0x355
	.byte	0x62
	.long	0x1844
	.uleb128 0x25
	.string	"fd"
	.byte	0x16
	.value	0x355
	.byte	0x6e
	.long	0x53
	.uleb128 0x26
	.long	.LASF344
	.byte	0x16
	.value	0x355
	.byte	0x78
	.long	0x9d9
	.byte	0
	.uleb128 0xa
	.long	0x7b
	.long	0x1854
	.uleb128 0xb
	.long	0x6d
	.byte	0x2
	.byte	0
	.uleb128 0x27
	.byte	0x5
	.byte	0x4
	.long	0x53
	.byte	0x16
	.value	0x4df
	.byte	0xe
	.long	0x1948
	.uleb128 0x1e
	.long	.LASF384
	.sleb128 -1
	.uleb128 0x1f
	.long	.LASF385
	.byte	0
	.uleb128 0x1f
	.long	.LASF386
	.byte	0x1
	.uleb128 0x1f
	.long	.LASF387
	.byte	0x2
	.uleb128 0x1f
	.long	.LASF388
	.byte	0x3
	.uleb128 0x1f
	.long	.LASF389
	.byte	0x4
	.uleb128 0x1f
	.long	.LASF390
	.byte	0x5
	.uleb128 0x1f
	.long	.LASF391
	.byte	0x6
	.uleb128 0x1f
	.long	.LASF392
	.byte	0x7
	.uleb128 0x1f
	.long	.LASF393
	.byte	0x8
	.uleb128 0x1f
	.long	.LASF394
	.byte	0x9
	.uleb128 0x1f
	.long	.LASF395
	.byte	0xa
	.uleb128 0x1f
	.long	.LASF396
	.byte	0xb
	.uleb128 0x1f
	.long	.LASF397
	.byte	0xc
	.uleb128 0x1f
	.long	.LASF398
	.byte	0xd
	.uleb128 0x1f
	.long	.LASF399
	.byte	0xe
	.uleb128 0x1f
	.long	.LASF400
	.byte	0xf
	.uleb128 0x1f
	.long	.LASF401
	.byte	0x10
	.uleb128 0x1f
	.long	.LASF402
	.byte	0x11
	.uleb128 0x1f
	.long	.LASF403
	.byte	0x12
	.uleb128 0x1f
	.long	.LASF404
	.byte	0x13
	.uleb128 0x1f
	.long	.LASF405
	.byte	0x14
	.uleb128 0x1f
	.long	.LASF406
	.byte	0x15
	.uleb128 0x1f
	.long	.LASF407
	.byte	0x16
	.uleb128 0x1f
	.long	.LASF408
	.byte	0x17
	.uleb128 0x1f
	.long	.LASF409
	.byte	0x18
	.uleb128 0x1f
	.long	.LASF410
	.byte	0x19
	.uleb128 0x1f
	.long	.LASF411
	.byte	0x1a
	.uleb128 0x1f
	.long	.LASF412
	.byte	0x1b
	.uleb128 0x1f
	.long	.LASF413
	.byte	0x1c
	.uleb128 0x1f
	.long	.LASF414
	.byte	0x1d
	.uleb128 0x1f
	.long	.LASF415
	.byte	0x1e
	.uleb128 0x1f
	.long	.LASF416
	.byte	0x1f
	.uleb128 0x1f
	.long	.LASF417
	.byte	0x20
	.uleb128 0x1f
	.long	.LASF418
	.byte	0x21
	.uleb128 0x1f
	.long	.LASF419
	.byte	0x22
	.uleb128 0x1f
	.long	.LASF420
	.byte	0x23
	.uleb128 0x1f
	.long	.LASF421
	.byte	0x24
	.byte	0
	.uleb128 0x22
	.long	.LASF422
	.byte	0x16
	.value	0x506
	.byte	0x3
	.long	0x1854
	.uleb128 0x4
	.byte	0x8
	.byte	0x4
	.long	.LASF423
	.uleb128 0x24
	.byte	0x20
	.byte	0x16
	.value	0x60c
	.byte	0x62
	.long	0x1980
	.uleb128 0x25
	.string	"fd"
	.byte	0x16
	.value	0x60c
	.byte	0x6e
	.long	0x53
	.uleb128 0x26
	.long	.LASF344
	.byte	0x16
	.value	0x60c
	.byte	0x78
	.long	0x9d9
	.byte	0
	.uleb128 0x24
	.byte	0x20
	.byte	0x16
	.value	0x61d
	.byte	0x62
	.long	0x19a4
	.uleb128 0x25
	.string	"fd"
	.byte	0x16
	.value	0x61d
	.byte	0x6e
	.long	0x53
	.uleb128 0x26
	.long	.LASF344
	.byte	0x16
	.value	0x61d
	.byte	0x78
	.long	0x9d9
	.byte	0
	.uleb128 0x23
	.byte	0x20
	.byte	0x16
	.value	0x620
	.byte	0x3
	.long	0x19e7
	.uleb128 0x13
	.long	.LASF424
	.byte	0x16
	.value	0x620
	.byte	0x20
	.long	0x19e7
	.byte	0
	.uleb128 0x13
	.long	.LASF425
	.byte	0x16
	.value	0x620
	.byte	0x3e
	.long	0x19e7
	.byte	0x8
	.uleb128 0x13
	.long	.LASF426
	.byte	0x16
	.value	0x620
	.byte	0x5d
	.long	0x19e7
	.byte	0x10
	.uleb128 0x13
	.long	.LASF427
	.byte	0x16
	.value	0x620
	.byte	0x6d
	.long	0x53
	.byte	0x18
	.byte	0
	.uleb128 0x3
	.byte	0x8
	.long	0x135c
	.uleb128 0x24
	.byte	0x10
	.byte	0x16
	.value	0x6f0
	.byte	0x3
	.long	0x1a12
	.uleb128 0x26
	.long	.LASF428
	.byte	0x16
	.value	0x6f1
	.byte	0xb
	.long	0xc7c
	.uleb128 0x26
	.long	.LASF429
	.byte	0x16
	.value	0x6f2
	.byte	0x12
	.long	0x74
	.byte	0
	.uleb128 0x28
	.byte	0x10
	.byte	0x16
	.value	0x6f6
	.value	0x1c8
	.long	0x1a3c
	.uleb128 0x29
	.string	"min"
	.byte	0x16
	.value	0x6f6
	.value	0x1d7
	.long	0x7b
	.byte	0
	.uleb128 0x2a
	.long	.LASF430
	.byte	0x16
	.value	0x6f6
	.value	0x1e9
	.long	0x74
	.byte	0x8
	.byte	0
	.uleb128 0x3
	.byte	0x8
	.long	0x1a42
	.uleb128 0x3
	.byte	0x8
	.long	0xd13
	.uleb128 0x7
	.long	.LASF431
	.byte	0x18
	.byte	0x15
	.byte	0xf
	.long	0xc7c
	.uleb128 0x1d
	.byte	0x7
	.byte	0x4
	.long	0x74
	.byte	0x19
	.byte	0x40
	.byte	0x6
	.long	0x1bac
	.uleb128 0x1f
	.long	.LASF432
	.byte	0x1
	.uleb128 0x1f
	.long	.LASF433
	.byte	0x2
	.uleb128 0x1f
	.long	.LASF434
	.byte	0x4
	.uleb128 0x1f
	.long	.LASF435
	.byte	0x8
	.uleb128 0x1f
	.long	.LASF436
	.byte	0x10
	.uleb128 0x1f
	.long	.LASF437
	.byte	0x20
	.uleb128 0x1f
	.long	.LASF438
	.byte	0x40
	.uleb128 0x1f
	.long	.LASF439
	.byte	0x80
	.uleb128 0x2b
	.long	.LASF440
	.value	0x100
	.uleb128 0x2b
	.long	.LASF441
	.value	0x200
	.uleb128 0x2b
	.long	.LASF442
	.value	0x400
	.uleb128 0x2b
	.long	.LASF443
	.value	0x800
	.uleb128 0x2b
	.long	.LASF444
	.value	0x1000
	.uleb128 0x2b
	.long	.LASF445
	.value	0x2000
	.uleb128 0x2b
	.long	.LASF446
	.value	0x4000
	.uleb128 0x2b
	.long	.LASF447
	.value	0x8000
	.uleb128 0x2c
	.long	.LASF448
	.long	0x10000
	.uleb128 0x2c
	.long	.LASF449
	.long	0x20000
	.uleb128 0x2c
	.long	.LASF450
	.long	0x40000
	.uleb128 0x2c
	.long	.LASF451
	.long	0x80000
	.uleb128 0x2c
	.long	.LASF452
	.long	0x100000
	.uleb128 0x2c
	.long	.LASF453
	.long	0x200000
	.uleb128 0x2c
	.long	.LASF454
	.long	0x400000
	.uleb128 0x2c
	.long	.LASF455
	.long	0x1000000
	.uleb128 0x2c
	.long	.LASF456
	.long	0x2000000
	.uleb128 0x2c
	.long	.LASF457
	.long	0x4000000
	.uleb128 0x2c
	.long	.LASF458
	.long	0x8000000
	.uleb128 0x2c
	.long	.LASF459
	.long	0x10000000
	.uleb128 0x2c
	.long	.LASF460
	.long	0x20000000
	.uleb128 0x2c
	.long	.LASF461
	.long	0x1000000
	.uleb128 0x2c
	.long	.LASF462
	.long	0x2000000
	.uleb128 0x2c
	.long	.LASF463
	.long	0x4000000
	.uleb128 0x2c
	.long	.LASF464
	.long	0x1000000
	.uleb128 0x2c
	.long	.LASF465
	.long	0x2000000
	.uleb128 0x2c
	.long	.LASF466
	.long	0x1000000
	.uleb128 0x2c
	.long	.LASF467
	.long	0x2000000
	.uleb128 0x2c
	.long	.LASF468
	.long	0x4000000
	.uleb128 0x2c
	.long	.LASF469
	.long	0x8000000
	.uleb128 0x2c
	.long	.LASF470
	.long	0x1000000
	.uleb128 0x2c
	.long	.LASF471
	.long	0x2000000
	.uleb128 0x2c
	.long	.LASF472
	.long	0x1000000
	.byte	0
	.uleb128 0x3
	.byte	0x8
	.long	0x1bb7
	.uleb128 0x9
	.long	0x1bac
	.uleb128 0x2d
	.uleb128 0x2e
	.long	.LASF334
	.value	0x328
	.byte	0x1
	.byte	0x25
	.byte	0x8
	.long	0x1c59
	.uleb128 0xd
	.long	.LASF473
	.byte	0x1
	.byte	0x26
	.byte	0x11
	.long	0x177d
	.byte	0
	.uleb128 0xd
	.long	.LASF474
	.byte	0x1
	.byte	0x27
	.byte	0x7
	.long	0x53
	.byte	0x8
	.uleb128 0xd
	.long	.LASF475
	.byte	0x1
	.byte	0x28
	.byte	0x10
	.long	0x74
	.byte	0xc
	.uleb128 0xd
	.long	.LASF476
	.byte	0x1
	.byte	0x29
	.byte	0xc
	.long	0x3bf
	.byte	0x10
	.uleb128 0xd
	.long	.LASF152
	.byte	0x1
	.byte	0x2a
	.byte	0xe
	.long	0x17e6
	.byte	0x18
	.uleb128 0xd
	.long	.LASF477
	.byte	0x1
	.byte	0x2b
	.byte	0x11
	.long	0x1750
	.byte	0x20
	.uleb128 0xd
	.long	.LASF478
	.byte	0x1
	.byte	0x2c
	.byte	0xe
	.long	0x1137
	.byte	0x28
	.uleb128 0xd
	.long	.LASF479
	.byte	0x1
	.byte	0x2d
	.byte	0xb
	.long	0x141f
	.byte	0xc0
	.uleb128 0x2f
	.long	.LASF348
	.byte	0x1
	.byte	0x2e
	.byte	0xd
	.long	0x173e
	.value	0x278
	.uleb128 0x2f
	.long	.LASF480
	.byte	0x1
	.byte	0x2f
	.byte	0x14
	.long	0x1c59
	.value	0x318
	.uleb128 0x2f
	.long	.LASF347
	.byte	0x1
	.byte	0x30
	.byte	0x8
	.long	0x2d2
	.value	0x320
	.byte	0
	.uleb128 0x3
	.byte	0x8
	.long	0x1bb8
	.uleb128 0x30
	.long	.LASF513
	.byte	0x1
	.byte	0x38
	.byte	0x12
	.long	0x173e
	.uleb128 0x9
	.byte	0x3
	.quad	zero_statbuf
	.uleb128 0x31
	.long	.LASF514
	.byte	0x1
	.value	0x102
	.byte	0xc
	.long	0x53
	.byte	0x1
	.long	0x1c9e
	.uleb128 0x32
	.string	"a"
	.byte	0x1
	.value	0x102
	.byte	0x28
	.long	0x1783
	.uleb128 0x32
	.string	"b"
	.byte	0x1
	.value	0x102
	.byte	0x3c
	.long	0x1783
	.byte	0
	.uleb128 0x33
	.long	.LASF484
	.byte	0x1
	.byte	0xea
	.byte	0xd
	.byte	0x1
	.long	0x1cfa
	.uleb128 0x34
	.long	.LASF481
	.byte	0x1
	.byte	0xea
	.byte	0x29
	.long	0x1589
	.uleb128 0x35
	.string	"ctx"
	.byte	0x1
	.byte	0xeb
	.byte	0x14
	.long	0x1c59
	.uleb128 0x35
	.string	"it"
	.byte	0x1
	.byte	0xec
	.byte	0x14
	.long	0x1c59
	.uleb128 0x36
	.long	.LASF482
	.byte	0x1
	.byte	0xed
	.byte	0x14
	.long	0x1c59
	.uleb128 0x36
	.long	.LASF483
	.byte	0x1
	.byte	0xee
	.byte	0x11
	.long	0x177d
	.uleb128 0x37
	.long	.LASF485
	.long	0x1d0a
	.uleb128 0x9
	.byte	0x3
	.quad	__PRETTY_FUNCTION__.9119
	.byte	0
	.uleb128 0xa
	.long	0x42
	.long	0x1d0a
	.uleb128 0xb
	.long	0x6d
	.byte	0xe
	.byte	0
	.uleb128 0x5
	.long	0x1cfa
	.uleb128 0x38
	.long	.LASF477
	.byte	0x1
	.byte	0xb9
	.byte	0xd
	.long	.Ldebug_ranges0+0
	.uleb128 0x1
	.byte	0x9c
	.long	0x1ec8
	.uleb128 0x39
	.string	"req"
	.byte	0x1
	.byte	0xb9
	.byte	0x1e
	.long	0x1619
	.long	.LLST0
	.long	.LVUS0
	.uleb128 0x3a
	.long	.LASF348
	.byte	0x1
	.byte	0xba
	.byte	0xe
	.long	0x1ec8
	.long	.LLST1
	.long	.LVUS1
	.uleb128 0x3b
	.string	"ctx"
	.byte	0x1
	.byte	0xbb
	.byte	0x14
	.long	0x1c59
	.long	.LLST2
	.long	.LVUS2
	.uleb128 0x3a
	.long	.LASF475
	.byte	0x1
	.byte	0xbc
	.byte	0xc
	.long	0x3bf
	.long	.LLST3
	.long	.LVUS3
	.uleb128 0x3a
	.long	.LASF483
	.byte	0x1
	.byte	0xbd
	.byte	0x11
	.long	0x177d
	.long	.LLST4
	.long	.LVUS4
	.uleb128 0x3c
	.string	"out"
	.byte	0x1
	.byte	0xd9
	.byte	0x1
	.quad	.L2
	.uleb128 0x3d
	.long	0x1c75
	.quad	.LBI8
	.byte	.LVU23
	.long	.Ldebug_ranges0+0x30
	.byte	0x1
	.byte	0xd3
	.byte	0x23
	.long	0x1dc9
	.uleb128 0x3e
	.long	0x1c92
	.long	.LLST5
	.long	.LVUS5
	.uleb128 0x3e
	.long	0x1c87
	.long	.LLST6
	.long	.LVUS6
	.byte	0
	.uleb128 0x3f
	.quad	.LVL4
	.long	0x2854
	.long	0x1de1
	.uleb128 0x40
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x2
	.byte	0x7c
	.sleb128 0
	.byte	0
	.uleb128 0x41
	.quad	.LVL8
	.long	0x1e02
	.uleb128 0x40
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x1
	.byte	0x30
	.uleb128 0x40
	.uleb128 0x1
	.byte	0x51
	.uleb128 0x3
	.byte	0x73
	.sleb128 440
	.uleb128 0x40
	.uleb128 0x1
	.byte	0x52
	.uleb128 0x3
	.byte	0x73
	.sleb128 112
	.byte	0
	.uleb128 0x3f
	.quad	.LVL10
	.long	0x2861
	.long	0x1e1a
	.uleb128 0x40
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x2
	.byte	0x73
	.sleb128 0
	.byte	0
	.uleb128 0x3f
	.quad	.LVL11
	.long	0x2854
	.long	0x1e32
	.uleb128 0x40
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x2
	.byte	0x7c
	.sleb128 0
	.byte	0
	.uleb128 0x42
	.quad	.LVL15
	.long	0x286e
	.long	0x1e5b
	.uleb128 0x40
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x6
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.byte	0x8
	.byte	0x98
	.byte	0x1c
	.uleb128 0x40
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x9
	.byte	0x3
	.quad	timer_close_cb
	.byte	0
	.uleb128 0x43
	.quad	.LVL17
	.long	0x287b
	.uleb128 0x3f
	.quad	.LVL20
	.long	0x2888
	.long	0x1e98
	.uleb128 0x40
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x40
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x9
	.byte	0x3
	.quad	timer_cb
	.uleb128 0x40
	.uleb128 0x1
	.byte	0x51
	.uleb128 0x2
	.byte	0x7c
	.sleb128 0
	.uleb128 0x40
	.uleb128 0x1
	.byte	0x52
	.uleb128 0x1
	.byte	0x30
	.byte	0
	.uleb128 0x41
	.quad	.LVL25
	.long	0x1eba
	.uleb128 0x40
	.uleb128 0x1
	.byte	0x51
	.uleb128 0x3
	.byte	0x73
	.sleb128 440
	.uleb128 0x40
	.uleb128 0x1
	.byte	0x52
	.uleb128 0x9
	.byte	0x3
	.quad	zero_statbuf
	.byte	0
	.uleb128 0x43
	.quad	.LVL28
	.long	0x2895
	.byte	0
	.uleb128 0x3
	.byte	0x8
	.long	0x173e
	.uleb128 0x33
	.long	.LASF322
	.byte	0x1
	.byte	0xac
	.byte	0xd
	.byte	0x1
	.long	0x1f07
	.uleb128 0x34
	.long	.LASF481
	.byte	0x1
	.byte	0xac
	.byte	0x22
	.long	0x15d1
	.uleb128 0x35
	.string	"ctx"
	.byte	0x1
	.byte	0xad
	.byte	0x14
	.long	0x1c59
	.uleb128 0x37
	.long	.LASF485
	.long	0x1f17
	.uleb128 0x9
	.byte	0x3
	.quad	__PRETTY_FUNCTION__.9103
	.byte	0
	.uleb128 0xa
	.long	0x42
	.long	0x1f17
	.uleb128 0xb
	.long	0x6d
	.byte	0x8
	.byte	0
	.uleb128 0x5
	.long	0x1f07
	.uleb128 0x44
	.long	.LASF515
	.byte	0x1
	.byte	0xa4
	.byte	0x6
	.quad	.LFB85
	.quad	.LFE85-.LFB85
	.uleb128 0x1
	.byte	0x9c
	.long	0x209f
	.uleb128 0x45
	.long	.LASF483
	.byte	0x1
	.byte	0xa4
	.byte	0x26
	.long	0x177d
	.long	.LLST38
	.long	.LVUS38
	.uleb128 0x3d
	.long	0x222c
	.quad	.LBI44
	.byte	.LVU407
	.long	.Ldebug_ranges0+0x150
	.byte	0x1
	.byte	0xa5
	.byte	0x3
	.long	0x2089
	.uleb128 0x3e
	.long	0x223d
	.long	.LLST39
	.long	.LVUS39
	.uleb128 0x46
	.long	.Ldebug_ranges0+0x150
	.uleb128 0x47
	.long	0x2249
	.long	.LLST40
	.long	.LVUS40
	.uleb128 0x3d
	.long	0x222c
	.quad	.LBI46
	.byte	.LVU423
	.long	.Ldebug_ranges0+0x190
	.byte	0x1
	.byte	0x74
	.byte	0x5
	.long	0x1fb8
	.uleb128 0x3e
	.long	0x223d
	.long	.LLST41
	.long	.LVUS41
	.uleb128 0x46
	.long	.Ldebug_ranges0+0x190
	.uleb128 0x48
	.long	0x2249
	.byte	0
	.byte	0
	.uleb128 0x3f
	.quad	.LVL128
	.long	0x2854
	.long	0x1fd0
	.uleb128 0x40
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x2
	.byte	0x7c
	.sleb128 0
	.byte	0
	.uleb128 0x3f
	.quad	.LVL131
	.long	0x2854
	.long	0x1fe8
	.uleb128 0x40
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x2
	.byte	0x7d
	.sleb128 0
	.byte	0
	.uleb128 0x3f
	.quad	.LVL139
	.long	0x286e
	.long	0x200d
	.uleb128 0x40
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x40
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x9
	.byte	0x3
	.quad	timer_close_cb
	.byte	0
	.uleb128 0x3f
	.quad	.LVL142
	.long	0x28a2
	.long	0x204c
	.uleb128 0x40
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x9
	.byte	0x3
	.quad	.LC6
	.uleb128 0x40
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x9
	.byte	0x3
	.quad	.LC1
	.uleb128 0x40
	.uleb128 0x1
	.byte	0x51
	.uleb128 0x2
	.byte	0x8
	.byte	0x7b
	.uleb128 0x40
	.uleb128 0x1
	.byte	0x52
	.uleb128 0x9
	.byte	0x3
	.quad	__PRETTY_FUNCTION__.9086
	.byte	0
	.uleb128 0x49
	.quad	.LVL144
	.long	0x28a2
	.uleb128 0x40
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x9
	.byte	0x3
	.quad	.LC7
	.uleb128 0x40
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x9
	.byte	0x3
	.quad	.LC1
	.uleb128 0x40
	.uleb128 0x1
	.byte	0x51
	.uleb128 0x2
	.byte	0x8
	.byte	0x7c
	.uleb128 0x40
	.uleb128 0x1
	.byte	0x52
	.uleb128 0x9
	.byte	0x3
	.quad	__PRETTY_FUNCTION__.9086
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x4a
	.quad	.LVL138
	.long	0x28ae
	.uleb128 0x40
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x3
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.byte	0
	.byte	0
	.uleb128 0x4b
	.long	.LASF489
	.byte	0x1
	.byte	0x8a
	.byte	0x5
	.long	0x53
	.quad	.LFB84
	.quad	.LFE84-.LFB84
	.uleb128 0x1
	.byte	0x9c
	.long	0x2211
	.uleb128 0x45
	.long	.LASF483
	.byte	0x1
	.byte	0x8a
	.byte	0x26
	.long	0x177d
	.long	.LLST30
	.long	.LVUS30
	.uleb128 0x45
	.long	.LASF486
	.byte	0x1
	.byte	0x8a
	.byte	0x34
	.long	0x35
	.long	.LLST31
	.long	.LVUS31
	.uleb128 0x45
	.long	.LASF487
	.byte	0x1
	.byte	0x8a
	.byte	0x44
	.long	0x2211
	.long	.LLST32
	.long	.LVUS32
	.uleb128 0x3b
	.string	"ctx"
	.byte	0x1
	.byte	0x8b
	.byte	0x14
	.long	0x1c59
	.long	.LLST33
	.long	.LVUS33
	.uleb128 0x3a
	.long	.LASF488
	.byte	0x1
	.byte	0x8c
	.byte	0xa
	.long	0x61
	.long	.LLST34
	.long	.LVUS34
	.uleb128 0x37
	.long	.LASF485
	.long	0x2227
	.uleb128 0x9
	.byte	0x3
	.quad	__PRETTY_FUNCTION__.9095
	.uleb128 0x4c
	.long	0x24c2
	.quad	.LBI38
	.byte	.LVU376
	.quad	.LBB38
	.quad	.LBE38-.LBB38
	.byte	0x1
	.byte	0x9c
	.byte	0x3
	.long	0x21a5
	.uleb128 0x3e
	.long	0x24eb
	.long	.LLST35
	.long	.LVUS35
	.uleb128 0x3e
	.long	0x24df
	.long	.LLST36
	.long	.LVUS36
	.uleb128 0x3e
	.long	0x24d3
	.long	.LLST37
	.long	.LVUS37
	.uleb128 0x49
	.quad	.LVL116
	.long	0x28ba
	.uleb128 0x40
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x40
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x2
	.byte	0x7e
	.sleb128 0
	.uleb128 0x40
	.uleb128 0x1
	.byte	0x51
	.uleb128 0x2
	.byte	0x73
	.sleb128 0
	.byte	0
	.byte	0
	.uleb128 0x3f
	.quad	.LVL111
	.long	0x2854
	.long	0x21bd
	.uleb128 0x40
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x2
	.byte	0x73
	.sleb128 0
	.byte	0
	.uleb128 0x3f
	.quad	.LVL113
	.long	0x28c5
	.long	0x21d5
	.uleb128 0x40
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x2
	.byte	0x7e
	.sleb128 0
	.byte	0
	.uleb128 0x49
	.quad	.LVL125
	.long	0x28a2
	.uleb128 0x40
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x9
	.byte	0x3
	.quad	.LC6
	.uleb128 0x40
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x9
	.byte	0x3
	.quad	.LC1
	.uleb128 0x40
	.uleb128 0x1
	.byte	0x51
	.uleb128 0x2
	.byte	0x8
	.byte	0x94
	.uleb128 0x40
	.uleb128 0x1
	.byte	0x52
	.uleb128 0x9
	.byte	0x3
	.quad	__PRETTY_FUNCTION__.9095
	.byte	0
	.byte	0
	.uleb128 0x3
	.byte	0x8
	.long	0x61
	.uleb128 0xa
	.long	0x42
	.long	0x2227
	.uleb128 0xb
	.long	0x6d
	.byte	0x12
	.byte	0
	.uleb128 0x5
	.long	0x2217
	.uleb128 0x4d
	.long	.LASF516
	.byte	0x1
	.byte	0x74
	.byte	0x5
	.long	0x53
	.byte	0x1
	.long	0x2269
	.uleb128 0x34
	.long	.LASF483
	.byte	0x1
	.byte	0x74
	.byte	0x23
	.long	0x177d
	.uleb128 0x35
	.string	"ctx"
	.byte	0x1
	.byte	0x75
	.byte	0x14
	.long	0x1c59
	.uleb128 0x37
	.long	.LASF485
	.long	0x2279
	.uleb128 0x9
	.byte	0x3
	.quad	__PRETTY_FUNCTION__.9086
	.byte	0
	.uleb128 0xa
	.long	0x42
	.long	0x2279
	.uleb128 0xb
	.long	0x6d
	.byte	0xf
	.byte	0
	.uleb128 0x5
	.long	0x2269
	.uleb128 0x4b
	.long	.LASF490
	.byte	0x1
	.byte	0x42
	.byte	0x5
	.long	0x53
	.quad	.LFB82
	.quad	.LFE82-.LFB82
	.uleb128 0x1
	.byte	0x9c
	.long	0x2483
	.uleb128 0x45
	.long	.LASF483
	.byte	0x1
	.byte	0x42
	.byte	0x24
	.long	0x177d
	.long	.LLST16
	.long	.LVUS16
	.uleb128 0x39
	.string	"cb"
	.byte	0x1
	.byte	0x43
	.byte	0x24
	.long	0x1750
	.long	.LLST17
	.long	.LVUS17
	.uleb128 0x45
	.long	.LASF347
	.byte	0x1
	.byte	0x44
	.byte	0x22
	.long	0x30e
	.long	.LLST18
	.long	.LVUS18
	.uleb128 0x45
	.long	.LASF475
	.byte	0x1
	.byte	0x45
	.byte	0x23
	.long	0x74
	.long	.LLST19
	.long	.LVUS19
	.uleb128 0x3b
	.string	"ctx"
	.byte	0x1
	.byte	0x46
	.byte	0x14
	.long	0x1c59
	.long	.LLST20
	.long	.LVUS20
	.uleb128 0x3a
	.long	.LASF152
	.byte	0x1
	.byte	0x47
	.byte	0xe
	.long	0x17e6
	.long	.LLST21
	.long	.LVUS21
	.uleb128 0x3b
	.string	"len"
	.byte	0x1
	.byte	0x48
	.byte	0xa
	.long	0x61
	.long	.LLST22
	.long	.LVUS22
	.uleb128 0x3b
	.string	"err"
	.byte	0x1
	.byte	0x49
	.byte	0x7
	.long	0x53
	.long	.LLST23
	.long	.LVUS23
	.uleb128 0x4e
	.long	.LASF491
	.byte	0x1
	.byte	0x6e
	.byte	0x1
	.quad	.L48
	.uleb128 0x3d
	.long	0x24c2
	.quad	.LBI24
	.byte	.LVU243
	.long	.Ldebug_ranges0+0xd0
	.byte	0x1
	.byte	0x5a
	.byte	0x3
	.long	0x23b4
	.uleb128 0x3e
	.long	0x24eb
	.long	.LLST24
	.long	.LVUS24
	.uleb128 0x3e
	.long	0x24df
	.long	.LLST25
	.long	.LVUS25
	.uleb128 0x3e
	.long	0x24d3
	.long	.LLST26
	.long	.LVUS26
	.uleb128 0x49
	.quad	.LVL76
	.long	0x28ba
	.uleb128 0x40
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x2
	.byte	0x7c
	.sleb128 0
	.uleb128 0x40
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x40
	.uleb128 0x1
	.byte	0x51
	.uleb128 0x6
	.byte	0x91
	.sleb128 -80
	.byte	0x6
	.byte	0x23
	.uleb128 0x1
	.byte	0
	.byte	0
	.uleb128 0x3f
	.quad	.LVL62
	.long	0x2854
	.long	0x23cc
	.uleb128 0x40
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x2
	.byte	0x73
	.sleb128 0
	.byte	0
	.uleb128 0x3f
	.quad	.LVL69
	.long	0x28c5
	.long	0x23e4
	.uleb128 0x40
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x2
	.byte	0x7d
	.sleb128 0
	.byte	0
	.uleb128 0x3f
	.quad	.LVL70
	.long	0x28d2
	.long	0x2406
	.uleb128 0x40
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x1
	.byte	0x31
	.uleb128 0x40
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x7
	.byte	0x91
	.sleb128 -80
	.byte	0x6
	.byte	0x23
	.uleb128 0x328
	.byte	0
	.uleb128 0x3f
	.quad	.LVL73
	.long	0x287b
	.long	0x241e
	.uleb128 0x40
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x2
	.byte	0x7e
	.sleb128 0
	.byte	0
	.uleb128 0x3f
	.quad	.LVL77
	.long	0x28df
	.long	0x243c
	.uleb128 0x40
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x2
	.byte	0x7e
	.sleb128 0
	.uleb128 0x40
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x2
	.byte	0x7f
	.sleb128 40
	.byte	0
	.uleb128 0x3f
	.quad	.LVL79
	.long	0x28ec
	.long	0x246e
	.uleb128 0x40
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x2
	.byte	0x7e
	.sleb128 0
	.uleb128 0x40
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x3
	.byte	0x7f
	.sleb128 192
	.uleb128 0x40
	.uleb128 0x1
	.byte	0x51
	.uleb128 0x2
	.byte	0x7c
	.sleb128 0
	.uleb128 0x40
	.uleb128 0x1
	.byte	0x52
	.uleb128 0x9
	.byte	0x3
	.quad	poll_cb
	.byte	0
	.uleb128 0x49
	.quad	.LVL83
	.long	0x28f9
	.uleb128 0x40
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x2
	.byte	0x7f
	.sleb128 0
	.byte	0
	.byte	0
	.uleb128 0x4b
	.long	.LASF492
	.byte	0x1
	.byte	0x3b
	.byte	0x5
	.long	0x53
	.quad	.LFB81
	.quad	.LFE81-.LFB81
	.uleb128 0x1
	.byte	0x9c
	.long	0x24c2
	.uleb128 0x4f
	.long	.LASF152
	.byte	0x1
	.byte	0x3b
	.byte	0x20
	.long	0x17e6
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x4f
	.long	.LASF483
	.byte	0x1
	.byte	0x3b
	.byte	0x34
	.long	0x177d
	.uleb128 0x1
	.byte	0x54
	.byte	0
	.uleb128 0x50
	.long	.LASF517
	.byte	0x2
	.byte	0x1f
	.byte	0x2a
	.long	0x7b
	.byte	0x3
	.long	0x24f8
	.uleb128 0x34
	.long	.LASF493
	.byte	0x2
	.byte	0x1f
	.byte	0x43
	.long	0x7d
	.uleb128 0x34
	.long	.LASF494
	.byte	0x2
	.byte	0x1f
	.byte	0x62
	.long	0x1bb2
	.uleb128 0x34
	.long	.LASF495
	.byte	0x2
	.byte	0x1f
	.byte	0x70
	.long	0x61
	.byte	0
	.uleb128 0x51
	.long	0x1ece
	.long	.Ldebug_ranges0+0x70
	.uleb128 0x1
	.byte	0x9c
	.long	0x2615
	.uleb128 0x3e
	.long	0x1edb
	.long	.LLST7
	.long	.LVUS7
	.uleb128 0x47
	.long	0x1ee7
	.long	.LLST8
	.long	.LVUS8
	.uleb128 0x3d
	.long	0x1ece
	.quad	.LBI16
	.byte	.LVU114
	.long	.Ldebug_ranges0+0xa0
	.byte	0x1
	.byte	0xac
	.byte	0xd
	.long	0x258e
	.uleb128 0x3e
	.long	0x1edb
	.long	.LLST9
	.long	.LVUS9
	.uleb128 0x46
	.long	.Ldebug_ranges0+0xa0
	.uleb128 0x48
	.long	0x1ee7
	.uleb128 0x49
	.quad	.LVL41
	.long	0x28a2
	.uleb128 0x40
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x9
	.byte	0x3
	.quad	.LC3
	.uleb128 0x40
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x9
	.byte	0x3
	.quad	.LC1
	.uleb128 0x40
	.uleb128 0x1
	.byte	0x51
	.uleb128 0x2
	.byte	0x8
	.byte	0xb1
	.uleb128 0x40
	.uleb128 0x1
	.byte	0x52
	.uleb128 0x9
	.byte	0x3
	.quad	__PRETTY_FUNCTION__.9103
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x43
	.quad	.LVL33
	.long	0x287b
	.uleb128 0x3f
	.quad	.LVL34
	.long	0x28ec
	.long	0x25c8
	.uleb128 0x40
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x3
	.byte	0x73
	.sleb128 152
	.uleb128 0x40
	.uleb128 0x1
	.byte	0x51
	.uleb128 0x3
	.byte	0x73
	.sleb128 760
	.uleb128 0x40
	.uleb128 0x1
	.byte	0x52
	.uleb128 0x9
	.byte	0x3
	.quad	poll_cb
	.byte	0
	.uleb128 0x3f
	.quad	.LVL38
	.long	0x28a2
	.long	0x2607
	.uleb128 0x40
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x9
	.byte	0x3
	.quad	.LC2
	.uleb128 0x40
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x9
	.byte	0x3
	.quad	.LC1
	.uleb128 0x40
	.uleb128 0x1
	.byte	0x51
	.uleb128 0x2
	.byte	0x8
	.byte	0xb0
	.uleb128 0x40
	.uleb128 0x1
	.byte	0x52
	.uleb128 0x9
	.byte	0x3
	.quad	__PRETTY_FUNCTION__.9103
	.byte	0
	.uleb128 0x43
	.quad	.LVL42
	.long	0x2895
	.byte	0
	.uleb128 0x52
	.long	0x1c9e
	.quad	.LFB88
	.quad	.LFE88-.LFB88
	.uleb128 0x1
	.byte	0x9c
	.long	0x271d
	.uleb128 0x3e
	.long	0x1cab
	.long	.LLST10
	.long	.LVUS10
	.uleb128 0x47
	.long	0x1cb7
	.long	.LLST11
	.long	.LVUS11
	.uleb128 0x47
	.long	0x1cc3
	.long	.LLST12
	.long	.LVUS12
	.uleb128 0x47
	.long	0x1cce
	.long	.LLST13
	.long	.LVUS13
	.uleb128 0x47
	.long	0x1cda
	.long	.LLST14
	.long	.LVUS14
	.uleb128 0x4c
	.long	0x1c9e
	.quad	.LBI22
	.byte	.LVU164
	.quad	.LBB22
	.quad	.LBE22-.LBB22
	.byte	0x1
	.byte	0xea
	.byte	0xd
	.long	0x26f3
	.uleb128 0x3e
	.long	0x1cab
	.long	.LLST15
	.long	.LVUS15
	.uleb128 0x48
	.long	0x1cb7
	.uleb128 0x48
	.long	0x1cc3
	.uleb128 0x48
	.long	0x1cce
	.uleb128 0x48
	.long	0x1cda
	.uleb128 0x49
	.quad	.LVL59
	.long	0x28a2
	.uleb128 0x40
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x9
	.byte	0x3
	.quad	.LC5
	.uleb128 0x40
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x9
	.byte	0x3
	.quad	.LC1
	.uleb128 0x40
	.uleb128 0x1
	.byte	0x51
	.uleb128 0x2
	.byte	0x8
	.byte	0xfa
	.uleb128 0x40
	.uleb128 0x1
	.byte	0x52
	.uleb128 0x9
	.byte	0x3
	.quad	__PRETTY_FUNCTION__.9119
	.byte	0
	.byte	0
	.uleb128 0x42
	.quad	.LVL54
	.long	0x28f9
	.long	0x270f
	.uleb128 0x40
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x6
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.byte	0x8
	.byte	0x28
	.byte	0x1c
	.byte	0
	.uleb128 0x43
	.quad	.LVL56
	.long	0x28ae
	.byte	0
	.uleb128 0x52
	.long	0x222c
	.quad	.LFB83
	.quad	.LFE83-.LFB83
	.uleb128 0x1
	.byte	0x9c
	.long	0x2854
	.uleb128 0x3e
	.long	0x223d
	.long	.LLST27
	.long	.LVUS27
	.uleb128 0x47
	.long	0x2249
	.long	.LLST28
	.long	.LVUS28
	.uleb128 0x3d
	.long	0x222c
	.quad	.LBI32
	.byte	.LVU324
	.long	.Ldebug_ranges0+0x110
	.byte	0x1
	.byte	0x74
	.byte	0x5
	.long	0x2784
	.uleb128 0x3e
	.long	0x223d
	.long	.LLST29
	.long	.LVUS29
	.uleb128 0x46
	.long	.Ldebug_ranges0+0x110
	.uleb128 0x48
	.long	0x2249
	.byte	0
	.byte	0
	.uleb128 0x3f
	.quad	.LVL92
	.long	0x2854
	.long	0x279c
	.uleb128 0x40
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x2
	.byte	0x73
	.sleb128 0
	.byte	0
	.uleb128 0x3f
	.quad	.LVL95
	.long	0x2854
	.long	0x27b4
	.uleb128 0x40
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x2
	.byte	0x7c
	.sleb128 0
	.byte	0
	.uleb128 0x3f
	.quad	.LVL104
	.long	0x286e
	.long	0x27d9
	.uleb128 0x40
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x2
	.byte	0x7c
	.sleb128 0
	.uleb128 0x40
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x9
	.byte	0x3
	.quad	timer_close_cb
	.byte	0
	.uleb128 0x3f
	.quad	.LVL107
	.long	0x28a2
	.long	0x2818
	.uleb128 0x40
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x9
	.byte	0x3
	.quad	.LC6
	.uleb128 0x40
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x9
	.byte	0x3
	.quad	.LC1
	.uleb128 0x40
	.uleb128 0x1
	.byte	0x51
	.uleb128 0x2
	.byte	0x8
	.byte	0x7b
	.uleb128 0x40
	.uleb128 0x1
	.byte	0x52
	.uleb128 0x9
	.byte	0x3
	.quad	__PRETTY_FUNCTION__.9086
	.byte	0
	.uleb128 0x49
	.quad	.LVL109
	.long	0x28a2
	.uleb128 0x40
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x9
	.byte	0x3
	.quad	.LC7
	.uleb128 0x40
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x9
	.byte	0x3
	.quad	.LC1
	.uleb128 0x40
	.uleb128 0x1
	.byte	0x51
	.uleb128 0x2
	.byte	0x8
	.byte	0x7c
	.uleb128 0x40
	.uleb128 0x1
	.byte	0x52
	.uleb128 0x9
	.byte	0x3
	.quad	__PRETTY_FUNCTION__.9086
	.byte	0
	.byte	0
	.uleb128 0x53
	.long	.LASF496
	.long	.LASF496
	.byte	0x16
	.value	0x1cc
	.byte	0x2c
	.uleb128 0x53
	.long	.LASF497
	.long	.LASF497
	.byte	0x16
	.value	0x523
	.byte	0x2d
	.uleb128 0x53
	.long	.LASF498
	.long	.LASF498
	.byte	0x16
	.value	0x1d4
	.byte	0x2d
	.uleb128 0x53
	.long	.LASF499
	.long	.LASF499
	.byte	0x16
	.value	0x12f
	.byte	0x31
	.uleb128 0x53
	.long	.LASF500
	.long	.LASF500
	.byte	0x16
	.value	0x35a
	.byte	0x2c
	.uleb128 0x53
	.long	.LASF501
	.long	.LASF501
	.byte	0x1a
	.value	0x24f
	.byte	0xd
	.uleb128 0x54
	.long	.LASF502
	.long	.LASF502
	.byte	0x1b
	.byte	0x45
	.byte	0xd
	.uleb128 0x54
	.long	.LASF503
	.long	.LASF503
	.byte	0x1c
	.byte	0xc3
	.byte	0x6
	.uleb128 0x55
	.long	.LASF517
	.long	.LASF518
	.byte	0x1e
	.byte	0
	.uleb128 0x53
	.long	.LASF504
	.long	.LASF504
	.byte	0x1d
	.value	0x181
	.byte	0xf
	.uleb128 0x53
	.long	.LASF505
	.long	.LASF505
	.byte	0x19
	.value	0x149
	.byte	0x7
	.uleb128 0x53
	.long	.LASF506
	.long	.LASF506
	.byte	0x16
	.value	0x359
	.byte	0x2c
	.uleb128 0x53
	.long	.LASF507
	.long	.LASF507
	.byte	0x16
	.value	0x57c
	.byte	0x2c
	.uleb128 0x53
	.long	.LASF508
	.long	.LASF508
	.byte	0x19
	.value	0x14d
	.byte	0x6
	.byte	0
	.section	.debug_abbrev,"",@progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1b
	.uleb128 0xe
	.uleb128 0x55
	.uleb128 0x17
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x10
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x37
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x13
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x13
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3c
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x21
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0x17
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0x15
	.byte	0
	.uleb128 0x27
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0x16
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x17
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x18
	.uleb128 0x5
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x19
	.uleb128 0x13
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0xb
	.uleb128 0x5
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1a
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x1b
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0x5
	.byte	0
	.byte	0
	.uleb128 0x1c
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0x5
	.byte	0
	.byte	0
	.uleb128 0x1d
	.uleb128 0x4
	.byte	0x1
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1e
	.uleb128 0x28
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1c
	.uleb128 0xd
	.byte	0
	.byte	0
	.uleb128 0x1f
	.uleb128 0x28
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1c
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x20
	.uleb128 0x13
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x21
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0x5
	.byte	0
	.byte	0
	.uleb128 0x22
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x23
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x24
	.uleb128 0x17
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x25
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x26
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x27
	.uleb128 0x4
	.byte	0x1
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x28
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0x5
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x29
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x2a
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x2b
	.uleb128 0x28
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1c
	.uleb128 0x5
	.byte	0
	.byte	0
	.uleb128 0x2c
	.uleb128 0x28
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1c
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2d
	.uleb128 0x26
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x2e
	.uleb128 0x13
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0xb
	.uleb128 0x5
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x2f
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0x5
	.byte	0
	.byte	0
	.uleb128 0x30
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x31
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x20
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x32
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x33
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x20
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x34
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x35
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x36
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x37
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x34
	.uleb128 0x19
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x38
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x55
	.uleb128 0x17
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2117
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x39
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x17
	.uleb128 0x2137
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x3a
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x17
	.uleb128 0x2137
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x3b
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x17
	.uleb128 0x2137
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x3c
	.uleb128 0xa
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.byte	0
	.byte	0
	.uleb128 0x3d
	.uleb128 0x1d
	.byte	0x1
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x2138
	.uleb128 0xb
	.uleb128 0x55
	.uleb128 0x17
	.uleb128 0x58
	.uleb128 0xb
	.uleb128 0x59
	.uleb128 0xb
	.uleb128 0x57
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x3e
	.uleb128 0x5
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x17
	.uleb128 0x2137
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x3f
	.uleb128 0x4109
	.byte	0x1
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x40
	.uleb128 0x410a
	.byte	0
	.uleb128 0x2
	.uleb128 0x18
	.uleb128 0x2111
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x41
	.uleb128 0x4109
	.byte	0x1
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x42
	.uleb128 0x4109
	.byte	0x1
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x2115
	.uleb128 0x19
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x43
	.uleb128 0x4109
	.byte	0
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x31
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x44
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x7
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2117
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x45
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x17
	.uleb128 0x2137
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x46
	.uleb128 0xb
	.byte	0x1
	.uleb128 0x55
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x47
	.uleb128 0x34
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x17
	.uleb128 0x2137
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x48
	.uleb128 0x34
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x49
	.uleb128 0x4109
	.byte	0x1
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x31
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4a
	.uleb128 0x4109
	.byte	0x1
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x2115
	.uleb128 0x19
	.uleb128 0x31
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4b
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x7
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2117
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4c
	.uleb128 0x1d
	.byte	0x1
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x2138
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x7
	.uleb128 0x58
	.uleb128 0xb
	.uleb128 0x59
	.uleb128 0xb
	.uleb128 0x57
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4d
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x20
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4e
	.uleb128 0xa
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.byte	0
	.byte	0
	.uleb128 0x4f
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x50
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x20
	.uleb128 0xb
	.uleb128 0x34
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x51
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x55
	.uleb128 0x17
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2117
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x52
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x7
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2117
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x53
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.uleb128 0x6e
	.uleb128 0xe
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x54
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.uleb128 0x6e
	.uleb128 0xe
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x55
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.uleb128 0x6e
	.uleb128 0xe
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",@progbits
.Ldebug_loc0:
.LVUS0:
	.uleb128 0
	.uleb128 .LVU12
	.uleb128 .LVU12
	.uleb128 .LVU45
	.uleb128 .LVU45
	.uleb128 .LVU47
	.uleb128 .LVU47
	.uleb128 .LVU49
	.uleb128 .LVU49
	.uleb128 .LVU49
	.uleb128 .LVU49
	.uleb128 .LVU65
	.uleb128 .LVU65
	.uleb128 .LVU67
	.uleb128 .LVU67
	.uleb128 .LVU68
	.uleb128 .LVU68
	.uleb128 0
	.uleb128 0
	.uleb128 0
.LLST0:
	.quad	.LVL0
	.quad	.LVL3
	.value	0x1
	.byte	0x55
	.quad	.LVL3
	.quad	.LVL12
	.value	0x1
	.byte	0x53
	.quad	.LVL12
	.quad	.LVL14
	.value	0x4
	.byte	0x7d
	.sleb128 152
	.byte	0x9f
	.quad	.LVL14
	.quad	.LVL15-1
	.value	0x4
	.byte	0x75
	.sleb128 152
	.byte	0x9f
	.quad	.LVL15-1
	.quad	.LVL15
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.byte	0x9f
	.quad	.LVL15
	.quad	.LVL21
	.value	0x1
	.byte	0x53
	.quad	.LVL21
	.quad	.LVL23
	.value	0x4
	.byte	0x7d
	.sleb128 152
	.byte	0x9f
	.quad	.LVL23
	.quad	.LVL24
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.byte	0x9f
	.quad	.LVL24
	.quad	.LHOTE0
	.value	0x1
	.byte	0x53
	.quad	.LFSB87
	.quad	.LCOLDE0
	.value	0x1
	.byte	0x53
	.quad	0
	.quad	0
.LVUS1:
	.uleb128 .LVU19
	.uleb128 .LVU36
	.uleb128 .LVU75
	.uleb128 .LVU88
.LLST1:
	.quad	.LVL5
	.quad	.LVL9
	.value	0x4
	.byte	0x73
	.sleb128 112
	.byte	0x9f
	.quad	.LVL26
	.quad	.LVL27
	.value	0x4
	.byte	0x73
	.sleb128 112
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS2:
	.uleb128 .LVU7
	.uleb128 .LVU12
	.uleb128 .LVU12
	.uleb128 .LVU45
	.uleb128 .LVU45
	.uleb128 .LVU47
	.uleb128 .LVU47
	.uleb128 .LVU49
	.uleb128 .LVU49
	.uleb128 .LVU49
	.uleb128 .LVU49
	.uleb128 .LVU65
	.uleb128 .LVU65
	.uleb128 .LVU67
	.uleb128 .LVU67
	.uleb128 .LVU68
	.uleb128 .LVU68
	.uleb128 0
	.uleb128 0
	.uleb128 0
.LLST2:
	.quad	.LVL1
	.quad	.LVL3
	.value	0x4
	.byte	0x75
	.sleb128 -192
	.byte	0x9f
	.quad	.LVL3
	.quad	.LVL12
	.value	0x4
	.byte	0x73
	.sleb128 -192
	.byte	0x9f
	.quad	.LVL12
	.quad	.LVL14
	.value	0x3
	.byte	0x7d
	.sleb128 -40
	.byte	0x9f
	.quad	.LVL14
	.quad	.LVL15-1
	.value	0x3
	.byte	0x75
	.sleb128 -40
	.byte	0x9f
	.quad	.LVL15-1
	.quad	.LVL15
	.value	0x7
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.byte	0x8
	.byte	0xc0
	.byte	0x1c
	.byte	0x9f
	.quad	.LVL15
	.quad	.LVL21
	.value	0x4
	.byte	0x73
	.sleb128 -192
	.byte	0x9f
	.quad	.LVL21
	.quad	.LVL23
	.value	0x3
	.byte	0x7d
	.sleb128 -40
	.byte	0x9f
	.quad	.LVL23
	.quad	.LVL24
	.value	0x7
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.byte	0x8
	.byte	0xc0
	.byte	0x1c
	.byte	0x9f
	.quad	.LVL24
	.quad	.LHOTE0
	.value	0x4
	.byte	0x73
	.sleb128 -192
	.byte	0x9f
	.quad	.LFSB87
	.quad	.LCOLDE0
	.value	0x4
	.byte	0x73
	.sleb128 -192
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS3:
	.uleb128 .LVU52
	.uleb128 .LVU58
	.uleb128 .LVU58
	.uleb128 .LVU61
	.uleb128 .LVU61
	.uleb128 .LVU66
	.uleb128 .LVU88
	.uleb128 0
	.uleb128 0
	.uleb128 0
.LLST3:
	.quad	.LVL16
	.quad	.LVL18
	.value	0x1
	.byte	0x5c
	.quad	.LVL18
	.quad	.LVL19
	.value	0x6
	.byte	0x7c
	.sleb128 0
	.byte	0x71
	.sleb128 0
	.byte	0x1c
	.byte	0x9f
	.quad	.LVL19
	.quad	.LVL22
	.value	0x1
	.byte	0x5c
	.quad	.LVL27
	.quad	.LHOTE0
	.value	0x1
	.byte	0x5c
	.quad	.LFSB87
	.quad	.LCOLDE0
	.value	0x1
	.byte	0x5c
	.quad	0
	.quad	0
.LVUS4:
	.uleb128 .LVU10
	.uleb128 .LVU46
	.uleb128 .LVU49
	.uleb128 .LVU52
	.uleb128 .LVU68
	.uleb128 .LVU88
.LLST4:
	.quad	.LVL2
	.quad	.LVL13
	.value	0x1
	.byte	0x5c
	.quad	.LVL15
	.quad	.LVL16
	.value	0x1
	.byte	0x5c
	.quad	.LVL24
	.quad	.LVL27
	.value	0x1
	.byte	0x5c
	.quad	0
	.quad	0
.LVUS5:
	.uleb128 .LVU23
	.uleb128 .LVU26
	.uleb128 .LVU75
	.uleb128 .LVU88
.LLST5:
	.quad	.LVL6
	.quad	.LVL7
	.value	0x4
	.byte	0x73
	.sleb128 112
	.byte	0x9f
	.quad	.LVL26
	.quad	.LVL27
	.value	0x4
	.byte	0x73
	.sleb128 112
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS6:
	.uleb128 .LVU23
	.uleb128 .LVU26
	.uleb128 .LVU75
	.uleb128 .LVU88
.LLST6:
	.quad	.LVL6
	.quad	.LVL7
	.value	0x4
	.byte	0x73
	.sleb128 440
	.byte	0x9f
	.quad	.LVL26
	.quad	.LVL27
	.value	0x4
	.byte	0x73
	.sleb128 440
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS38:
	.uleb128 0
	.uleb128 .LVU411
	.uleb128 .LVU411
	.uleb128 .LVU432
	.uleb128 .LVU432
	.uleb128 .LVU433
	.uleb128 .LVU433
	.uleb128 .LVU446
	.uleb128 .LVU446
	.uleb128 .LVU448
	.uleb128 .LVU448
	.uleb128 .LVU448
	.uleb128 .LVU448
	.uleb128 0
.LLST38:
	.quad	.LVL126
	.quad	.LVL128-1
	.value	0x1
	.byte	0x55
	.quad	.LVL128-1
	.quad	.LVL134
	.value	0x1
	.byte	0x5c
	.quad	.LVL134
	.quad	.LVL135
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.byte	0x9f
	.quad	.LVL135
	.quad	.LVL137
	.value	0x1
	.byte	0x5c
	.quad	.LVL137
	.quad	.LVL138-1
	.value	0x1
	.byte	0x55
	.quad	.LVL138-1
	.quad	.LVL138
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.byte	0x9f
	.quad	.LVL138
	.quad	.LFE85
	.value	0x1
	.byte	0x5c
	.quad	0
	.quad	0
.LVUS39:
	.uleb128 .LVU407
	.uleb128 .LVU411
	.uleb128 .LVU411
	.uleb128 .LVU428
	.uleb128 .LVU433
	.uleb128 .LVU441
	.uleb128 .LVU448
	.uleb128 0
.LLST39:
	.quad	.LVL127
	.quad	.LVL128-1
	.value	0x1
	.byte	0x55
	.quad	.LVL128-1
	.quad	.LVL133
	.value	0x1
	.byte	0x5c
	.quad	.LVL135
	.quad	.LVL136
	.value	0x1
	.byte	0x5c
	.quad	.LVL138
	.quad	.LFE85
	.value	0x1
	.byte	0x5c
	.quad	0
	.quad	0
.LVUS40:
	.uleb128 .LVU414
	.uleb128 .LVU421
	.uleb128 .LVU421
	.uleb128 .LVU422
	.uleb128 .LVU422
	.uleb128 .LVU428
	.uleb128 .LVU433
	.uleb128 .LVU441
	.uleb128 .LVU448
	.uleb128 .LVU454
	.uleb128 .LVU454
	.uleb128 .LVU456
	.uleb128 .LVU456
	.uleb128 .LVU457
	.uleb128 .LVU457
	.uleb128 .LVU458
	.uleb128 .LVU458
	.uleb128 .LVU459
.LLST40:
	.quad	.LVL129
	.quad	.LVL130
	.value	0x1
	.byte	0x55
	.quad	.LVL130
	.quad	.LVL131-1
	.value	0x3
	.byte	0x7c
	.sleb128 96
	.quad	.LVL131-1
	.quad	.LVL133
	.value	0x3
	.byte	0x7d
	.sleb128 -40
	.byte	0x9f
	.quad	.LVL135
	.quad	.LVL136
	.value	0x3
	.byte	0x7d
	.sleb128 -40
	.byte	0x9f
	.quad	.LVL138
	.quad	.LVL140
	.value	0x3
	.byte	0x7d
	.sleb128 -40
	.byte	0x9f
	.quad	.LVL140
	.quad	.LVL141
	.value	0x1
	.byte	0x55
	.quad	.LVL141
	.quad	.LVL142-1
	.value	0x3
	.byte	0x7c
	.sleb128 96
	.quad	.LVL142
	.quad	.LVL143
	.value	0x1
	.byte	0x55
	.quad	.LVL143
	.quad	.LVL144-1
	.value	0x3
	.byte	0x7c
	.sleb128 96
	.quad	0
	.quad	0
.LVUS41:
	.uleb128 .LVU423
	.uleb128 .LVU428
	.uleb128 .LVU433
	.uleb128 .LVU441
	.uleb128 .LVU449
	.uleb128 .LVU454
.LLST41:
	.quad	.LVL132
	.quad	.LVL133
	.value	0x1
	.byte	0x5c
	.quad	.LVL135
	.quad	.LVL136
	.value	0x1
	.byte	0x5c
	.quad	.LVL139
	.quad	.LVL140
	.value	0x1
	.byte	0x5c
	.quad	0
	.quad	0
.LVUS30:
	.uleb128 0
	.uleb128 .LVU364
	.uleb128 .LVU364
	.uleb128 .LVU373
	.uleb128 .LVU373
	.uleb128 .LVU395
	.uleb128 .LVU395
	.uleb128 0
.LLST30:
	.quad	.LVL110
	.quad	.LVL111-1
	.value	0x1
	.byte	0x55
	.quad	.LVL111-1
	.quad	.LVL114
	.value	0x1
	.byte	0x53
	.quad	.LVL114
	.quad	.LVL122
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.byte	0x9f
	.quad	.LVL122
	.quad	.LFE84
	.value	0x1
	.byte	0x53
	.quad	0
	.quad	0
.LVUS31:
	.uleb128 0
	.uleb128 .LVU364
	.uleb128 .LVU364
	.uleb128 .LVU388
	.uleb128 .LVU388
	.uleb128 .LVU389
	.uleb128 .LVU389
	.uleb128 0
.LLST31:
	.quad	.LVL110
	.quad	.LVL111-1
	.value	0x1
	.byte	0x54
	.quad	.LVL111-1
	.quad	.LVL119
	.value	0x1
	.byte	0x5d
	.quad	.LVL119
	.quad	.LVL120
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x54
	.byte	0x9f
	.quad	.LVL120
	.quad	.LFE84
	.value	0x1
	.byte	0x5d
	.quad	0
	.quad	0
.LVUS32:
	.uleb128 0
	.uleb128 .LVU364
	.uleb128 .LVU364
	.uleb128 .LVU387
	.uleb128 .LVU387
	.uleb128 .LVU389
	.uleb128 .LVU389
	.uleb128 0
.LLST32:
	.quad	.LVL110
	.quad	.LVL111-1
	.value	0x1
	.byte	0x51
	.quad	.LVL111-1
	.quad	.LVL118
	.value	0x1
	.byte	0x5c
	.quad	.LVL118
	.quad	.LVL120
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x51
	.byte	0x9f
	.quad	.LVL120
	.quad	.LFE84
	.value	0x1
	.byte	0x5c
	.quad	0
	.quad	0
.LVUS33:
	.uleb128 .LVU367
	.uleb128 .LVU372
	.uleb128 .LVU372
	.uleb128 .LVU386
	.uleb128 .LVU389
	.uleb128 .LVU395
	.uleb128 .LVU399
	.uleb128 .LVU400
	.uleb128 .LVU400
	.uleb128 .LVU401
.LLST33:
	.quad	.LVL112
	.quad	.LVL113-1
	.value	0x1
	.byte	0x54
	.quad	.LVL113-1
	.quad	.LVL117
	.value	0x4
	.byte	0x7e
	.sleb128 -800
	.byte	0x9f
	.quad	.LVL120
	.quad	.LVL122
	.value	0x4
	.byte	0x7e
	.sleb128 -800
	.byte	0x9f
	.quad	.LVL123
	.quad	.LVL124
	.value	0x1
	.byte	0x54
	.quad	.LVL124
	.quad	.LVL125-1
	.value	0x3
	.byte	0x73
	.sleb128 96
	.quad	0
	.quad	0
.LVUS34:
	.uleb128 .LVU373
	.uleb128 .LVU379
	.uleb128 .LVU379
	.uleb128 .LVU386
	.uleb128 .LVU389
	.uleb128 .LVU392
	.uleb128 .LVU392
	.uleb128 .LVU395
.LLST34:
	.quad	.LVL114
	.quad	.LVL116-1
	.value	0x1
	.byte	0x50
	.quad	.LVL116-1
	.quad	.LVL117
	.value	0x1
	.byte	0x53
	.quad	.LVL120
	.quad	.LVL121
	.value	0x1
	.byte	0x50
	.quad	.LVL121
	.quad	.LVL122
	.value	0x3
	.byte	0x73
	.sleb128 -1
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS35:
	.uleb128 .LVU376
	.uleb128 .LVU379
	.uleb128 .LVU379
	.uleb128 .LVU379
.LLST35:
	.quad	.LVL115
	.quad	.LVL116-1
	.value	0x1
	.byte	0x50
	.quad	.LVL116-1
	.quad	.LVL116
	.value	0x1
	.byte	0x53
	.quad	0
	.quad	0
.LVUS36:
	.uleb128 .LVU376
	.uleb128 .LVU379
.LLST36:
	.quad	.LVL115
	.quad	.LVL116
	.value	0x1
	.byte	0x5e
	.quad	0
	.quad	0
.LVUS37:
	.uleb128 .LVU376
	.uleb128 .LVU379
.LLST37:
	.quad	.LVL115
	.quad	.LVL116
	.value	0x1
	.byte	0x5d
	.quad	0
	.quad	0
.LVUS16:
	.uleb128 0
	.uleb128 .LVU208
	.uleb128 .LVU208
	.uleb128 .LVU211
	.uleb128 .LVU211
	.uleb128 .LVU214
	.uleb128 .LVU214
	.uleb128 .LVU289
	.uleb128 .LVU289
	.uleb128 .LVU293
	.uleb128 .LVU293
	.uleb128 0
.LLST16:
	.quad	.LVL61
	.quad	.LVL62-1
	.value	0x1
	.byte	0x55
	.quad	.LVL62-1
	.quad	.LVL64
	.value	0x1
	.byte	0x53
	.quad	.LVL64
	.quad	.LVL67
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.byte	0x9f
	.quad	.LVL67
	.quad	.LVL84
	.value	0x1
	.byte	0x53
	.quad	.LVL84
	.quad	.LVL88
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.byte	0x9f
	.quad	.LVL88
	.quad	.LFE82
	.value	0x1
	.byte	0x53
	.quad	0
	.quad	0
.LVUS17:
	.uleb128 0
	.uleb128 .LVU208
	.uleb128 .LVU208
	.uleb128 .LVU213
	.uleb128 .LVU213
	.uleb128 .LVU285
	.uleb128 .LVU285
	.uleb128 .LVU293
	.uleb128 .LVU293
	.uleb128 0
.LLST17:
	.quad	.LVL61
	.quad	.LVL62-1
	.value	0x1
	.byte	0x54
	.quad	.LVL62-1
	.quad	.LVL66
	.value	0x2
	.byte	0x76
	.sleb128 -56
	.quad	.LVL66
	.quad	.LVL82
	.value	0x3
	.byte	0x91
	.sleb128 -72
	.quad	.LVL82
	.quad	.LVL88
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x54
	.byte	0x9f
	.quad	.LVL88
	.quad	.LFE82
	.value	0x3
	.byte	0x91
	.sleb128 -72
	.quad	0
	.quad	0
.LVUS18:
	.uleb128 0
	.uleb128 .LVU208
	.uleb128 .LVU208
	.uleb128 .LVU212
	.uleb128 .LVU212
	.uleb128 .LVU214
	.uleb128 .LVU214
	.uleb128 .LVU290
	.uleb128 .LVU290
	.uleb128 .LVU293
	.uleb128 .LVU293
	.uleb128 0
.LLST18:
	.quad	.LVL61
	.quad	.LVL62-1
	.value	0x1
	.byte	0x51
	.quad	.LVL62-1
	.quad	.LVL65
	.value	0x1
	.byte	0x5d
	.quad	.LVL65
	.quad	.LVL67
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x51
	.byte	0x9f
	.quad	.LVL67
	.quad	.LVL85
	.value	0x1
	.byte	0x5d
	.quad	.LVL85
	.quad	.LVL88
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x51
	.byte	0x9f
	.quad	.LVL88
	.quad	.LFE82
	.value	0x1
	.byte	0x5d
	.quad	0
	.quad	0
.LVUS19:
	.uleb128 0
	.uleb128 .LVU208
	.uleb128 .LVU208
	.uleb128 .LVU209
	.uleb128 .LVU209
	.uleb128 .LVU214
	.uleb128 .LVU214
	.uleb128 .LVU233
	.uleb128 .LVU233
	.uleb128 .LVU303
	.uleb128 .LVU303
	.uleb128 0
.LLST19:
	.quad	.LVL61
	.quad	.LVL62-1
	.value	0x1
	.byte	0x52
	.quad	.LVL62-1
	.quad	.LVL63
	.value	0x1
	.byte	0x5c
	.quad	.LVL63
	.quad	.LVL67
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x52
	.byte	0x9f
	.quad	.LVL67
	.quad	.LVL72
	.value	0x1
	.byte	0x5c
	.quad	.LVL72
	.quad	.LVL89
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x52
	.byte	0x9f
	.quad	.LVL89
	.quad	.LFE82
	.value	0x1
	.byte	0x5c
	.quad	0
	.quad	0
.LVUS20:
	.uleb128 .LVU222
	.uleb128 .LVU237
	.uleb128 .LVU237
	.uleb128 .LVU292
	.uleb128 .LVU293
	.uleb128 .LVU303
	.uleb128 .LVU303
	.uleb128 .LVU304
	.uleb128 .LVU304
	.uleb128 0
.LLST20:
	.quad	.LVL71
	.quad	.LVL73-1
	.value	0x1
	.byte	0x50
	.quad	.LVL73-1
	.quad	.LVL87
	.value	0x1
	.byte	0x5f
	.quad	.LVL88
	.quad	.LVL89
	.value	0x1
	.byte	0x5f
	.quad	.LVL89
	.quad	.LVL90
	.value	0x1
	.byte	0x50
	.quad	.LVL90
	.quad	.LFE82
	.value	0x1
	.byte	0x5f
	.quad	0
	.quad	0
.LVUS21:
	.uleb128 .LVU217
	.uleb128 .LVU291
	.uleb128 .LVU293
	.uleb128 0
.LLST21:
	.quad	.LVL68
	.quad	.LVL86
	.value	0x1
	.byte	0x5e
	.quad	.LVL88
	.quad	.LFE82
	.value	0x1
	.byte	0x5e
	.quad	0
	.quad	0
.LVUS22:
	.uleb128 .LVU219
	.uleb128 .LVU221
	.uleb128 .LVU221
	.uleb128 0
.LLST22:
	.quad	.LVL69
	.quad	.LVL70-1
	.value	0x1
	.byte	0x50
	.quad	.LVL70-1
	.quad	.LFE82
	.value	0x3
	.byte	0x91
	.sleb128 -80
	.quad	0
	.quad	0
.LVUS23:
	.uleb128 .LVU251
	.uleb128 .LVU255
	.uleb128 .LVU263
	.uleb128 .LVU267
	.uleb128 .LVU284
	.uleb128 .LVU285
	.uleb128 .LVU285
	.uleb128 .LVU293
.LLST23:
	.quad	.LVL77
	.quad	.LVL78
	.value	0x1
	.byte	0x50
	.quad	.LVL79
	.quad	.LVL80
	.value	0x1
	.byte	0x50
	.quad	.LVL81
	.quad	.LVL82
	.value	0x1
	.byte	0x50
	.quad	.LVL82
	.quad	.LVL88
	.value	0x3
	.byte	0x91
	.sleb128 -72
	.quad	0
	.quad	0
.LVUS24:
	.uleb128 .LVU243
	.uleb128 .LVU247
	.uleb128 .LVU247
	.uleb128 .LVU248
	.uleb128 .LVU248
	.uleb128 .LVU248
.LLST24:
	.quad	.LVL74
	.quad	.LVL75
	.value	0x3
	.byte	0x71
	.sleb128 1
	.byte	0x9f
	.quad	.LVL75
	.quad	.LVL76-1
	.value	0x1
	.byte	0x51
	.quad	.LVL76-1
	.quad	.LVL76
	.value	0x7
	.byte	0x91
	.sleb128 -80
	.byte	0x6
	.byte	0x23
	.uleb128 0x1
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS25:
	.uleb128 .LVU243
	.uleb128 .LVU248
.LLST25:
	.quad	.LVL74
	.quad	.LVL76
	.value	0x1
	.byte	0x5d
	.quad	0
	.quad	0
.LVUS26:
	.uleb128 .LVU243
	.uleb128 .LVU248
.LLST26:
	.quad	.LVL74
	.quad	.LVL76
	.value	0x1
	.byte	0x5c
	.quad	0
	.quad	0
.LVUS7:
	.uleb128 0
	.uleb128 .LVU102
	.uleb128 .LVU102
	.uleb128 .LVU111
	.uleb128 .LVU111
	.uleb128 .LVU112
	.uleb128 .LVU112
	.uleb128 .LVU113
	.uleb128 .LVU113
	.uleb128 .LVU114
	.uleb128 .LVU114
	.uleb128 .LVU117
	.uleb128 .LVU117
	.uleb128 .LVU118
	.uleb128 .LVU118
	.uleb128 0
	.uleb128 0
	.uleb128 0
.LLST7:
	.quad	.LVL29
	.quad	.LVL32
	.value	0x1
	.byte	0x55
	.quad	.LVL32
	.quad	.LVL35
	.value	0x1
	.byte	0x53
	.quad	.LVL35
	.quad	.LVL36
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.byte	0x9f
	.quad	.LVL36
	.quad	.LVL37
	.value	0x1
	.byte	0x55
	.quad	.LVL37
	.quad	.LVL38
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.byte	0x9f
	.quad	.LVL38
	.quad	.LVL40
	.value	0x1
	.byte	0x55
	.quad	.LVL40
	.quad	.LVL41
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.byte	0x9f
	.quad	.LVL41
	.quad	.LHOTE4
	.value	0x1
	.byte	0x53
	.quad	.LFSB86
	.quad	.LCOLDE4
	.value	0x1
	.byte	0x53
	.quad	0
	.quad	0
.LVUS8:
	.uleb128 .LVU94
	.uleb128 .LVU98
	.uleb128 .LVU98
	.uleb128 .LVU103
	.uleb128 .LVU103
	.uleb128 .LVU111
	.uleb128 .LVU111
	.uleb128 .LVU112
	.uleb128 .LVU112
	.uleb128 .LVU113
	.uleb128 .LVU113
	.uleb128 .LVU114
	.uleb128 .LVU114
	.uleb128 .LVU116
	.uleb128 .LVU116
	.uleb128 .LVU117
	.uleb128 .LVU117
	.uleb128 .LVU118
	.uleb128 .LVU118
	.uleb128 0
	.uleb128 0
	.uleb128 0
.LLST8:
	.quad	.LVL30
	.quad	.LVL31
	.value	0x3
	.byte	0x75
	.sleb128 -40
	.byte	0x9f
	.quad	.LVL31
	.quad	.LVL33-1
	.value	0x1
	.byte	0x51
	.quad	.LVL33-1
	.quad	.LVL35
	.value	0x3
	.byte	0x73
	.sleb128 -40
	.byte	0x9f
	.quad	.LVL35
	.quad	.LVL36
	.value	0x7
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.byte	0x8
	.byte	0x28
	.byte	0x1c
	.byte	0x9f
	.quad	.LVL36
	.quad	.LVL37
	.value	0x3
	.byte	0x75
	.sleb128 -40
	.byte	0x9f
	.quad	.LVL37
	.quad	.LVL38
	.value	0x7
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.byte	0x8
	.byte	0x28
	.byte	0x1c
	.byte	0x9f
	.quad	.LVL38
	.quad	.LVL39
	.value	0x1
	.byte	0x51
	.quad	.LVL39
	.quad	.LVL40
	.value	0x3
	.byte	0x75
	.sleb128 -40
	.byte	0x9f
	.quad	.LVL40
	.quad	.LVL41
	.value	0x7
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.byte	0x8
	.byte	0x28
	.byte	0x1c
	.byte	0x9f
	.quad	.LVL41
	.quad	.LHOTE4
	.value	0x3
	.byte	0x73
	.sleb128 -40
	.byte	0x9f
	.quad	.LFSB86
	.quad	.LCOLDE4
	.value	0x3
	.byte	0x73
	.sleb128 -40
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS9:
	.uleb128 .LVU115
	.uleb128 .LVU117
	.uleb128 .LVU117
	.uleb128 .LVU118
.LLST9:
	.quad	.LVL38
	.quad	.LVL40
	.value	0x1
	.byte	0x55
	.quad	.LVL40
	.quad	.LVL41
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS10:
	.uleb128 0
	.uleb128 .LVU149
	.uleb128 .LVU149
	.uleb128 .LVU153
	.uleb128 .LVU153
	.uleb128 .LVU155
	.uleb128 .LVU155
	.uleb128 .LVU155
	.uleb128 .LVU155
	.uleb128 .LVU162
	.uleb128 .LVU162
	.uleb128 .LVU164
	.uleb128 .LVU164
	.uleb128 .LVU166
	.uleb128 .LVU166
	.uleb128 0
.LLST10:
	.quad	.LVL43
	.quad	.LVL52
	.value	0x1
	.byte	0x55
	.quad	.LVL52
	.quad	.LVL53
	.value	0x3
	.byte	0x7c
	.sleb128 40
	.byte	0x9f
	.quad	.LVL53
	.quad	.LVL54-1
	.value	0x3
	.byte	0x75
	.sleb128 40
	.byte	0x9f
	.quad	.LVL54-1
	.quad	.LVL54
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.byte	0x9f
	.quad	.LVL54
	.quad	.LVL55
	.value	0x1
	.byte	0x55
	.quad	.LVL55
	.quad	.LVL57
	.value	0x3
	.byte	0x7c
	.sleb128 40
	.byte	0x9f
	.quad	.LVL57
	.quad	.LVL58
	.value	0x1
	.byte	0x55
	.quad	.LVL58
	.quad	.LFE88
	.value	0x3
	.byte	0x7c
	.sleb128 40
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS11:
	.uleb128 .LVU129
	.uleb128 .LVU153
	.uleb128 .LVU153
	.uleb128 .LVU155
	.uleb128 .LVU155
	.uleb128 .LVU155
	.uleb128 .LVU155
	.uleb128 0
.LLST11:
	.quad	.LVL44
	.quad	.LVL53
	.value	0x1
	.byte	0x5c
	.quad	.LVL53
	.quad	.LVL54-1
	.value	0x1
	.byte	0x55
	.quad	.LVL54-1
	.quad	.LVL54
	.value	0x7
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.byte	0x8
	.byte	0x28
	.byte	0x1c
	.byte	0x9f
	.quad	.LVL54
	.quad	.LFE88
	.value	0x1
	.byte	0x5c
	.quad	0
	.quad	0
.LVUS12:
	.uleb128 .LVU137
	.uleb128 .LVU140
	.uleb128 .LVU140
	.uleb128 .LVU142
	.uleb128 .LVU142
	.uleb128 .LVU148
	.uleb128 .LVU164
	.uleb128 .LVU167
.LLST12:
	.quad	.LVL47
	.quad	.LVL48
	.value	0x1
	.byte	0x50
	.quad	.LVL48
	.quad	.LVL49
	.value	0x1
	.byte	0x51
	.quad	.LVL49
	.quad	.LVL51
	.value	0x1
	.byte	0x50
	.quad	.LVL57
	.quad	.LVL59-1
	.value	0x1
	.byte	0x50
	.quad	0
	.quad	0
.LVUS13:
	.uleb128 .LVU136
	.uleb128 .LVU140
	.uleb128 .LVU143
	.uleb128 .LVU144
	.uleb128 .LVU144
	.uleb128 .LVU149
.LLST13:
	.quad	.LVL46
	.quad	.LVL48
	.value	0x1
	.byte	0x51
	.quad	.LVL49
	.quad	.LVL50
	.value	0x1
	.byte	0x50
	.quad	.LVL50
	.quad	.LVL52
	.value	0x1
	.byte	0x51
	.quad	0
	.quad	0
.LVUS14:
	.uleb128 .LVU132
	.uleb128 .LVU149
	.uleb128 .LVU155
	.uleb128 .LVU163
	.uleb128 .LVU164
	.uleb128 .LVU167
.LLST14:
	.quad	.LVL45
	.quad	.LVL52
	.value	0x1
	.byte	0x58
	.quad	.LVL54
	.quad	.LVL56-1
	.value	0x1
	.byte	0x58
	.quad	.LVL57
	.quad	.LVL59-1
	.value	0x1
	.byte	0x58
	.quad	0
	.quad	0
.LVUS15:
	.uleb128 .LVU165
	.uleb128 .LVU166
	.uleb128 .LVU166
	.uleb128 0
.LLST15:
	.quad	.LVL57
	.quad	.LVL58
	.value	0x1
	.byte	0x55
	.quad	.LVL58
	.quad	.LFE88
	.value	0x3
	.byte	0x7c
	.sleb128 40
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS27:
	.uleb128 0
	.uleb128 .LVU312
	.uleb128 .LVU312
	.uleb128 .LVU331
	.uleb128 .LVU331
	.uleb128 .LVU332
	.uleb128 .LVU332
	.uleb128 .LVU342
	.uleb128 .LVU342
	.uleb128 .LVU344
	.uleb128 .LVU344
	.uleb128 0
.LLST27:
	.quad	.LVL91
	.quad	.LVL92-1
	.value	0x1
	.byte	0x55
	.quad	.LVL92-1
	.quad	.LVL98
	.value	0x1
	.byte	0x53
	.quad	.LVL98
	.quad	.LVL99
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.byte	0x9f
	.quad	.LVL99
	.quad	.LVL101
	.value	0x1
	.byte	0x53
	.quad	.LVL101
	.quad	.LVL103
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.byte	0x9f
	.quad	.LVL103
	.quad	.LFE83
	.value	0x1
	.byte	0x53
	.quad	0
	.quad	0
.LVUS28:
	.uleb128 .LVU315
	.uleb128 .LVU322
	.uleb128 .LVU322
	.uleb128 .LVU323
	.uleb128 .LVU323
	.uleb128 .LVU329
	.uleb128 .LVU332
	.uleb128 .LVU343
	.uleb128 .LVU344
	.uleb128 .LVU350
	.uleb128 .LVU350
	.uleb128 .LVU352
	.uleb128 .LVU352
	.uleb128 .LVU353
	.uleb128 .LVU353
	.uleb128 .LVU354
	.uleb128 .LVU354
	.uleb128 .LVU355
.LLST28:
	.quad	.LVL93
	.quad	.LVL94
	.value	0x1
	.byte	0x55
	.quad	.LVL94
	.quad	.LVL95-1
	.value	0x3
	.byte	0x73
	.sleb128 96
	.quad	.LVL95-1
	.quad	.LVL97
	.value	0x3
	.byte	0x7c
	.sleb128 -40
	.byte	0x9f
	.quad	.LVL99
	.quad	.LVL102
	.value	0x3
	.byte	0x7c
	.sleb128 -40
	.byte	0x9f
	.quad	.LVL103
	.quad	.LVL105
	.value	0x3
	.byte	0x7c
	.sleb128 -40
	.byte	0x9f
	.quad	.LVL105
	.quad	.LVL106
	.value	0x1
	.byte	0x55
	.quad	.LVL106
	.quad	.LVL107-1
	.value	0x3
	.byte	0x73
	.sleb128 96
	.quad	.LVL107
	.quad	.LVL108
	.value	0x1
	.byte	0x55
	.quad	.LVL108
	.quad	.LVL109-1
	.value	0x3
	.byte	0x73
	.sleb128 96
	.quad	0
	.quad	0
.LVUS29:
	.uleb128 .LVU324
	.uleb128 .LVU329
	.uleb128 .LVU332
	.uleb128 .LVU340
	.uleb128 .LVU345
	.uleb128 .LVU350
.LLST29:
	.quad	.LVL96
	.quad	.LVL97
	.value	0x1
	.byte	0x53
	.quad	.LVL99
	.quad	.LVL100
	.value	0x1
	.byte	0x53
	.quad	.LVL104
	.quad	.LVL105
	.value	0x1
	.byte	0x53
	.quad	0
	.quad	0
	.section	.debug_aranges,"",@progbits
	.long	0x3c
	.value	0x2
	.long	.Ldebug_info0
	.byte	0x8
	.byte	0
	.value	0
	.value	0
	.quad	.Ltext0
	.quad	.Letext0-.Ltext0
	.quad	.Ltext_cold0
	.quad	.Letext_cold0-.Ltext_cold0
	.quad	0
	.quad	0
	.section	.debug_ranges,"",@progbits
.Ldebug_ranges0:
	.quad	.LFB87
	.quad	.LHOTE0
	.quad	.LFSB87
	.quad	.LCOLDE0
	.quad	0
	.quad	0
	.quad	.LBB8
	.quad	.LBE8
	.quad	.LBB12
	.quad	.LBE12
	.quad	.LBB13
	.quad	.LBE13
	.quad	0
	.quad	0
	.quad	.LFB86
	.quad	.LHOTE4
	.quad	.LFSB86
	.quad	.LCOLDE4
	.quad	0
	.quad	0
	.quad	.LBB16
	.quad	.LBE16
	.quad	.LBB19
	.quad	.LBE19
	.quad	0
	.quad	0
	.quad	.LBB24
	.quad	.LBE24
	.quad	.LBB28
	.quad	.LBE28
	.quad	.LBB29
	.quad	.LBE29
	.quad	0
	.quad	0
	.quad	.LBB32
	.quad	.LBE32
	.quad	.LBB36
	.quad	.LBE36
	.quad	.LBB37
	.quad	.LBE37
	.quad	0
	.quad	0
	.quad	.LBB44
	.quad	.LBE44
	.quad	.LBB54
	.quad	.LBE54
	.quad	.LBB55
	.quad	.LBE55
	.quad	0
	.quad	0
	.quad	.LBB46
	.quad	.LBE46
	.quad	.LBB50
	.quad	.LBE50
	.quad	.LBB51
	.quad	.LBE51
	.quad	0
	.quad	0
	.quad	.Ltext0
	.quad	.Letext0
	.quad	.Ltext_cold0
	.quad	.Letext_cold0
	.quad	0
	.quad	0
	.section	.debug_line,"",@progbits
.Ldebug_line0:
	.section	.debug_str,"MS",@progbits,1
.LASF507:
	.string	"uv_fs_stat"
.LASF88:
	.string	"__writers_futex"
.LASF100:
	.string	"__align"
.LASF62:
	.string	"_sys_errlist"
.LASF49:
	.string	"_unused2"
.LASF35:
	.string	"_fileno"
.LASF75:
	.string	"__pthread_mutex_s"
.LASF483:
	.string	"handle"
.LASF125:
	.string	"sockaddr_iso"
.LASF67:
	.string	"gid_t"
.LASF182:
	.string	"signal_io_watcher"
.LASF10:
	.string	"__uint8_t"
.LASF337:
	.string	"signal_cb"
.LASF356:
	.string	"work_req"
.LASF400:
	.string	"UV_FS_FSYNC"
.LASF466:
	.string	"UV_HANDLE_TTY_READABLE"
.LASF40:
	.string	"_shortbuf"
.LASF188:
	.string	"uv__io_cb"
.LASF112:
	.string	"sockaddr_in"
.LASF104:
	.string	"sa_family_t"
.LASF295:
	.string	"UV_TTY"
.LASF151:
	.string	"done"
.LASF285:
	.string	"UV_FS_POLL"
.LASF172:
	.string	"check_handles"
.LASF492:
	.string	"uv_fs_poll_init"
.LASF267:
	.string	"UV_ESRCH"
.LASF503:
	.string	"uv__make_close_pending"
.LASF107:
	.string	"sa_data"
.LASF291:
	.string	"UV_PROCESS"
.LASF382:
	.string	"uv_fs_poll_cb"
.LASF64:
	.string	"uint16_t"
.LASF376:
	.string	"st_gen"
.LASF116:
	.string	"sin_zero"
.LASF314:
	.string	"uv_loop_t"
.LASF132:
	.string	"in_port_t"
.LASF21:
	.string	"_flags"
.LASF222:
	.string	"UV_EBUSY"
.LASF199:
	.string	"uv_uid_t"
.LASF18:
	.string	"__off_t"
.LASF216:
	.string	"UV_EAI_OVERFLOW"
.LASF447:
	.string	"UV_HANDLE_WRITABLE"
.LASF465:
	.string	"UV_HANDLE_PIPESERVER"
.LASF266:
	.string	"UV_ESPIPE"
.LASF372:
	.string	"st_size"
.LASF196:
	.string	"uv_mutex_t"
.LASF207:
	.string	"UV_EAI_AGAIN"
.LASF41:
	.string	"_lock"
.LASF419:
	.string	"UV_FS_STATFS"
.LASF219:
	.string	"UV_EAI_SOCKTYPE"
.LASF193:
	.string	"uv_buf_t"
.LASF402:
	.string	"UV_FS_UNLINK"
.LASF226:
	.string	"UV_ECONNREFUSED"
.LASF496:
	.string	"uv_is_active"
.LASF357:
	.string	"bufsml"
.LASF436:
	.string	"UV_HANDLE_INTERNAL"
.LASF364:
	.string	"uv_timespec_t"
.LASF373:
	.string	"st_blksize"
.LASF511:
	.string	"/workspace/nodejsbuild/node-v12.18.4/out"
.LASF106:
	.string	"sa_family"
.LASF228:
	.string	"UV_EDESTADDRREQ"
.LASF225:
	.string	"UV_ECONNABORTED"
.LASF240:
	.string	"UV_EMSGSIZE"
.LASF401:
	.string	"UV_FS_FDATASYNC"
.LASF517:
	.string	"memcpy"
.LASF126:
	.string	"sockaddr_ns"
.LASF369:
	.string	"st_gid"
.LASF233:
	.string	"UV_EINTR"
.LASF175:
	.string	"async_unused"
.LASF242:
	.string	"UV_ENETDOWN"
.LASF469:
	.string	"UV_HANDLE_TTY_SAVED_ATTRIBUTES"
.LASF84:
	.string	"__pthread_rwlock_arch_t"
.LASF501:
	.string	"abort"
.LASF27:
	.string	"_IO_write_end"
.LASF339:
	.string	"tree_entry"
.LASF78:
	.string	"__owner"
.LASF131:
	.string	"s_addr"
.LASF162:
	.string	"watcher_queue"
.LASF223:
	.string	"UV_ECANCELED"
.LASF252:
	.string	"UV_ENOSYS"
.LASF270:
	.string	"UV_EXDEV"
.LASF379:
	.string	"st_ctim"
.LASF435:
	.string	"UV_HANDLE_REF"
.LASF354:
	.string	"atime"
.LASF289:
	.string	"UV_POLL"
.LASF211:
	.string	"UV_EAI_FAIL"
.LASF399:
	.string	"UV_FS_FCHMOD"
.LASF255:
	.string	"UV_ENOTEMPTY"
.LASF142:
	.string	"__tzname"
.LASF515:
	.string	"uv__fs_poll_close"
.LASF76:
	.string	"__lock"
.LASF277:
	.string	"UV_ENOTTY"
.LASF462:
	.string	"UV_HANDLE_UDP_CONNECTED"
.LASF396:
	.string	"UV_FS_FUTIME"
.LASF74:
	.string	"__pthread_list_t"
.LASF343:
	.string	"uv_fs_s"
.LASF342:
	.string	"uv_fs_t"
.LASF331:
	.string	"pending"
.LASF113:
	.string	"sin_family"
.LASF417:
	.string	"UV_FS_READDIR"
.LASF508:
	.string	"uv__free"
.LASF453:
	.string	"UV_HANDLE_CANCELLATION_PENDING"
.LASF474:
	.string	"busy_polling"
.LASF303:
	.string	"UV_CONNECT"
.LASF250:
	.string	"UV_ENOPROTOOPT"
.LASF480:
	.string	"previous"
.LASF155:
	.string	"active_handles"
.LASF334:
	.string	"poll_ctx"
.LASF309:
	.string	"UV_GETADDRINFO"
.LASF317:
	.string	"type"
.LASF318:
	.string	"close_cb"
.LASF380:
	.string	"st_birthtim"
.LASF60:
	.string	"sys_errlist"
.LASF261:
	.string	"UV_EPROTONOSUPPORT"
.LASF15:
	.string	"__uid_t"
.LASF146:
	.string	"daylight"
.LASF422:
	.string	"uv_fs_type"
.LASF12:
	.string	"__uint16_t"
.LASF114:
	.string	"sin_port"
.LASF205:
	.string	"UV_EAGAIN"
.LASF467:
	.string	"UV_HANDLE_TTY_RAW"
.LASF438:
	.string	"UV_HANDLE_LISTENING"
.LASF449:
	.string	"UV_HANDLE_SYNC_BYPASS_IOCP"
.LASF351:
	.string	"mode"
.LASF392:
	.string	"UV_FS_LSTAT"
.LASF98:
	.string	"__data"
.LASF103:
	.string	"pthread_rwlock_t"
.LASF157:
	.string	"active_reqs"
.LASF302:
	.string	"UV_REQ"
.LASF221:
	.string	"UV_EBADF"
.LASF34:
	.string	"_chain"
.LASF256:
	.string	"UV_ENOTSOCK"
.LASF236:
	.string	"UV_EISCONN"
.LASF441:
	.string	"UV_HANDLE_SHUT"
.LASF274:
	.string	"UV_EMLINK"
.LASF248:
	.string	"UV_ENOMEM"
.LASF127:
	.string	"sockaddr_un"
.LASF284:
	.string	"UV_FS_EVENT"
.LASF375:
	.string	"st_flags"
.LASF6:
	.string	"unsigned char"
.LASF397:
	.string	"UV_FS_ACCESS"
.LASF91:
	.string	"__cur_writer"
.LASF189:
	.string	"uv__io_s"
.LASF192:
	.string	"uv__io_t"
.LASF101:
	.string	"pthread_mutex_t"
.LASF512:
	.string	"_IO_lock_t"
.LASF384:
	.string	"UV_FS_UNKNOWN"
.LASF479:
	.string	"fs_req"
.LASF490:
	.string	"uv_fs_poll_start"
.LASF358:
	.string	"uv_close_cb"
.LASF281:
	.string	"UV_UNKNOWN_HANDLE"
.LASF404:
	.string	"UV_FS_MKDIR"
.LASF498:
	.string	"uv_close"
.LASF476:
	.string	"start_time"
.LASF54:
	.string	"off_t"
.LASF335:
	.string	"uv_signal_t"
.LASF415:
	.string	"UV_FS_LCHOWN"
.LASF324:
	.string	"timeout"
.LASF80:
	.string	"__kind"
.LASF66:
	.string	"uint64_t"
.LASF17:
	.string	"__mode_t"
.LASF329:
	.string	"async_cb"
.LASF367:
	.string	"st_nlink"
.LASF322:
	.string	"timer_cb"
.LASF489:
	.string	"uv_fs_poll_getpath"
.LASF197:
	.string	"uv_rwlock_t"
.LASF315:
	.string	"uv_handle_t"
.LASF149:
	.string	"uv__work"
.LASF26:
	.string	"_IO_write_ptr"
.LASF431:
	.string	"QUEUE"
.LASF202:
	.string	"UV_EADDRINUSE"
.LASF239:
	.string	"UV_EMFILE"
.LASF464:
	.string	"UV_HANDLE_NON_OVERLAPPED_PIPE"
.LASF170:
	.string	"process_handles"
.LASF307:
	.string	"UV_FS"
.LASF456:
	.string	"UV_HANDLE_TCP_KEEPALIVE"
.LASF286:
	.string	"UV_HANDLE"
.LASF325:
	.string	"repeat"
.LASF488:
	.string	"required_len"
.LASF273:
	.string	"UV_ENXIO"
.LASF176:
	.string	"async_io_watcher"
.LASF414:
	.string	"UV_FS_COPYFILE"
.LASF99:
	.string	"__size"
.LASF487:
	.string	"size"
.LASF264:
	.string	"UV_EROFS"
.LASF231:
	.string	"UV_EFBIG"
.LASF50:
	.string	"FILE"
.LASF249:
	.string	"UV_ENONET"
.LASF497:
	.string	"uv_fs_req_cleanup"
.LASF204:
	.string	"UV_EAFNOSUPPORT"
.LASF360:
	.string	"uv_async_cb"
.LASF377:
	.string	"st_atim"
.LASF393:
	.string	"UV_FS_FSTAT"
.LASF491:
	.string	"error"
.LASF9:
	.string	"size_t"
.LASF359:
	.string	"uv_timer_cb"
.LASF148:
	.string	"getdate_err"
.LASF77:
	.string	"__count"
.LASF63:
	.string	"uint8_t"
.LASF336:
	.string	"uv_signal_s"
.LASF213:
	.string	"UV_EAI_MEMORY"
.LASF443:
	.string	"UV_HANDLE_READ_EOF"
.LASF428:
	.string	"unused"
.LASF500:
	.string	"uv_timer_start"
.LASF246:
	.string	"UV_ENODEV"
.LASF30:
	.string	"_IO_save_base"
.LASF241:
	.string	"UV_ENAMETOOLONG"
.LASF432:
	.string	"UV_HANDLE_CLOSING"
.LASF457:
	.string	"UV_HANDLE_TCP_SINGLE_ACCEPT"
.LASF385:
	.string	"UV_FS_CUSTOM"
.LASF300:
	.string	"uv_handle_type"
.LASF128:
	.string	"sockaddr_x25"
.LASF120:
	.string	"sin6_flowinfo"
.LASF350:
	.string	"file"
.LASF203:
	.string	"UV_EADDRNOTAVAIL"
.LASF95:
	.string	"__pad2"
.LASF430:
	.string	"nelts"
.LASF44:
	.string	"_wide_data"
.LASF262:
	.string	"UV_EPROTOTYPE"
.LASF371:
	.string	"st_ino"
.LASF366:
	.string	"st_mode"
.LASF340:
	.string	"caught_signals"
.LASF137:
	.string	"__in6_u"
.LASF235:
	.string	"UV_EIO"
.LASF201:
	.string	"UV_EACCES"
.LASF195:
	.string	"uv_file"
.LASF71:
	.string	"__pthread_internal_list"
.LASF338:
	.string	"signum"
.LASF72:
	.string	"__prev"
.LASF269:
	.string	"UV_ETXTBSY"
.LASF227:
	.string	"UV_ECONNRESET"
.LASF420:
	.string	"UV_FS_MKSTEMP"
.LASF232:
	.string	"UV_EHOSTUNREACH"
.LASF424:
	.string	"rbe_left"
.LASF477:
	.string	"poll_cb"
.LASF14:
	.string	"__uint64_t"
.LASF348:
	.string	"statbuf"
.LASF389:
	.string	"UV_FS_WRITE"
.LASF156:
	.string	"handle_queue"
.LASF167:
	.string	"wq_async"
.LASF344:
	.string	"reserved"
.LASF20:
	.string	"__ssize_t"
.LASF494:
	.string	"__src"
.LASF390:
	.string	"UV_FS_SENDFILE"
.LASF301:
	.string	"UV_UNKNOWN_REQ"
.LASF133:
	.string	"__u6_addr8"
.LASF171:
	.string	"prepare_handles"
.LASF484:
	.string	"timer_close_cb"
.LASF136:
	.string	"in6_addr"
.LASF144:
	.string	"__timezone"
.LASF121:
	.string	"sin6_addr"
.LASF381:
	.string	"uv_stat_t"
.LASF368:
	.string	"st_uid"
.LASF218:
	.string	"UV_EAI_SERVICE"
.LASF510:
	.string	"../deps/uv/src/fs-poll.c"
.LASF294:
	.string	"UV_TIMER"
.LASF278:
	.string	"UV_EFTYPE"
.LASF412:
	.string	"UV_FS_FCHOWN"
.LASF257:
	.string	"UV_ENOTSUP"
.LASF93:
	.string	"__rwelision"
.LASF478:
	.string	"timer_handle"
.LASF442:
	.string	"UV_HANDLE_READ_PARTIAL"
.LASF455:
	.string	"UV_HANDLE_TCP_NODELAY"
.LASF58:
	.string	"stderr"
.LASF346:
	.string	"result"
.LASF481:
	.string	"timer"
.LASF1:
	.string	"program_invocation_short_name"
.LASF460:
	.string	"UV_HANDLE_SHARED_TCP_SOCKET"
.LASF32:
	.string	"_IO_save_end"
.LASF183:
	.string	"child_watcher"
.LASF73:
	.string	"__next"
.LASF217:
	.string	"UV_EAI_PROTOCOL"
.LASF272:
	.string	"UV_EOF"
.LASF502:
	.string	"__assert_fail"
.LASF292:
	.string	"UV_STREAM"
.LASF57:
	.string	"stdout"
.LASF160:
	.string	"backend_fd"
.LASF215:
	.string	"UV_EAI_NONAME"
.LASF461:
	.string	"UV_HANDLE_UDP_PROCESSING"
.LASF485:
	.string	"__PRETTY_FUNCTION__"
.LASF452:
	.string	"UV_HANDLE_BLOCKING_WRITES"
.LASF82:
	.string	"__elision"
.LASF158:
	.string	"stop_flag"
.LASF353:
	.string	"bufs"
.LASF7:
	.string	"short unsigned int"
.LASF437:
	.string	"UV_HANDLE_ENDGAME_QUEUED"
.LASF8:
	.string	"signed char"
.LASF311:
	.string	"UV_RANDOM"
.LASF310:
	.string	"UV_GETNAMEINFO"
.LASF472:
	.string	"UV_HANDLE_POLL_SLOW"
.LASF386:
	.string	"UV_FS_OPEN"
.LASF446:
	.string	"UV_HANDLE_READABLE"
.LASF429:
	.string	"count"
.LASF413:
	.string	"UV_FS_REALPATH"
.LASF516:
	.string	"uv_fs_poll_stop"
.LASF185:
	.string	"inotify_read_watcher"
.LASF19:
	.string	"__off64_t"
.LASF495:
	.string	"__len"
.LASF111:
	.string	"sockaddr_eon"
.LASF24:
	.string	"_IO_read_base"
.LASF471:
	.string	"UV_SIGNAL_ONE_SHOT"
.LASF42:
	.string	"_offset"
.LASF427:
	.string	"rbe_color"
.LASF105:
	.string	"sockaddr"
.LASF168:
	.string	"cloexec_lock"
.LASF29:
	.string	"_IO_buf_end"
.LASF509:
	.string	"GNU C89 9.3.0 -m64 -mtune=generic -march=x86-64 -g -O3 -std=gnu90 -fvisibility=hidden -fno-omit-frame-pointer -fasynchronous-unwind-tables -fstack-protector-strong -fstack-clash-protection -fcf-protection"
.LASF408:
	.string	"UV_FS_LINK"
.LASF347:
	.string	"path"
.LASF178:
	.string	"timer_heap"
.LASF48:
	.string	"_mode"
.LASF323:
	.string	"heap_node"
.LASF25:
	.string	"_IO_write_base"
.LASF238:
	.string	"UV_ELOOP"
.LASF305:
	.string	"UV_SHUTDOWN"
.LASF352:
	.string	"nbufs"
.LASF258:
	.string	"UV_EPERM"
.LASF209:
	.string	"UV_EAI_BADHINTS"
.LASF180:
	.string	"time"
.LASF229:
	.string	"UV_EEXIST"
.LASF276:
	.string	"UV_EREMOTEIO"
.LASF3:
	.string	"long int"
.LASF326:
	.string	"start_id"
.LASF247:
	.string	"UV_ENOENT"
.LASF451:
	.string	"UV_HANDLE_EMULATE_IOCP"
.LASF51:
	.string	"_IO_marker"
.LASF304:
	.string	"UV_WRITE"
.LASF184:
	.string	"emfile_fd"
.LASF444:
	.string	"UV_HANDLE_READING"
.LASF244:
	.string	"UV_ENFILE"
.LASF450:
	.string	"UV_HANDLE_ZERO_READ"
.LASF454:
	.string	"UV_HANDLE_IPV6"
.LASF308:
	.string	"UV_WORK"
.LASF468:
	.string	"UV_HANDLE_TTY_SAVED_POSITION"
.LASF482:
	.string	"last"
.LASF130:
	.string	"in_addr"
.LASF65:
	.string	"uint32_t"
.LASF52:
	.string	"_IO_codecvt"
.LASF177:
	.string	"async_wfd"
.LASF475:
	.string	"interval"
.LASF313:
	.string	"uv_req_type"
.LASF398:
	.string	"UV_FS_CHMOD"
.LASF473:
	.string	"parent_handle"
.LASF459:
	.string	"UV_HANDLE_TCP_SOCKET_CLOSED"
.LASF212:
	.string	"UV_EAI_FAMILY"
.LASF395:
	.string	"UV_FS_UTIME"
.LASF403:
	.string	"UV_FS_RMDIR"
.LASF4:
	.string	"long unsigned int"
.LASF282:
	.string	"UV_ASYNC"
.LASF349:
	.string	"new_path"
.LASF174:
	.string	"async_handles"
.LASF224:
	.string	"UV_ECHARSET"
.LASF265:
	.string	"UV_ESHUTDOWN"
.LASF312:
	.string	"UV_REQ_TYPE_MAX"
.LASF2:
	.string	"char"
.LASF123:
	.string	"sockaddr_inarp"
.LASF122:
	.string	"sin6_scope_id"
.LASF56:
	.string	"stdin"
.LASF115:
	.string	"sin_addr"
.LASF81:
	.string	"__spins"
.LASF198:
	.string	"uv_gid_t"
.LASF28:
	.string	"_IO_buf_base"
.LASF79:
	.string	"__nusers"
.LASF200:
	.string	"UV_E2BIG"
.LASF493:
	.string	"__dest"
.LASF253:
	.string	"UV_ENOTCONN"
.LASF23:
	.string	"_IO_read_end"
.LASF70:
	.string	"_IO_FILE"
.LASF129:
	.string	"in_addr_t"
.LASF53:
	.string	"_IO_wide_data"
.LASF504:
	.string	"strlen"
.LASF145:
	.string	"tzname"
.LASF134:
	.string	"__u6_addr16"
.LASF150:
	.string	"work"
.LASF287:
	.string	"UV_IDLE"
.LASF169:
	.string	"closing_handles"
.LASF405:
	.string	"UV_FS_MKDTEMP"
.LASF86:
	.string	"__writers"
.LASF109:
	.string	"sockaddr_ax25"
.LASF486:
	.string	"buffer"
.LASF94:
	.string	"__pad1"
.LASF89:
	.string	"__pad3"
.LASF90:
	.string	"__pad4"
.LASF47:
	.string	"__pad5"
.LASF135:
	.string	"__u6_addr32"
.LASF288:
	.string	"UV_NAMED_PIPE"
.LASF190:
	.string	"pevents"
.LASF280:
	.string	"UV_ERRNO_MAX"
.LASF268:
	.string	"UV_ETIMEDOUT"
.LASF33:
	.string	"_markers"
.LASF260:
	.string	"UV_EPROTO"
.LASF298:
	.string	"UV_FILE"
.LASF345:
	.string	"fs_type"
.LASF245:
	.string	"UV_ENOBUFS"
.LASF439:
	.string	"UV_HANDLE_CONNECTION"
.LASF43:
	.string	"_codecvt"
.LASF293:
	.string	"UV_TCP"
.LASF423:
	.string	"double"
.LASF518:
	.string	"__builtin_memcpy"
.LASF416:
	.string	"UV_FS_OPENDIR"
.LASF186:
	.string	"inotify_watchers"
.LASF361:
	.string	"uv_fs_cb"
.LASF370:
	.string	"st_rdev"
.LASF425:
	.string	"rbe_right"
.LASF173:
	.string	"idle_handles"
.LASF365:
	.string	"st_dev"
.LASF55:
	.string	"ssize_t"
.LASF448:
	.string	"UV_HANDLE_READ_PENDING"
.LASF92:
	.string	"__shared"
.LASF13:
	.string	"__uint32_t"
.LASF154:
	.string	"data"
.LASF143:
	.string	"__daylight"
.LASF290:
	.string	"UV_PREPARE"
.LASF410:
	.string	"UV_FS_READLINK"
.LASF96:
	.string	"__flags"
.LASF237:
	.string	"UV_EISDIR"
.LASF140:
	.string	"_sys_siglist"
.LASF316:
	.string	"uv_handle_s"
.LASF181:
	.string	"signal_pipefd"
.LASF391:
	.string	"UV_FS_STAT"
.LASF164:
	.string	"nwatchers"
.LASF214:
	.string	"UV_EAI_NODATA"
.LASF279:
	.string	"UV_EILSEQ"
.LASF194:
	.string	"base"
.LASF407:
	.string	"UV_FS_SCANDIR"
.LASF513:
	.string	"zero_statbuf"
.LASF163:
	.string	"watchers"
.LASF514:
	.string	"statbuf_eq"
.LASF418:
	.string	"UV_FS_CLOSEDIR"
.LASF332:
	.string	"uv_fs_poll_t"
.LASF0:
	.string	"program_invocation_name"
.LASF505:
	.string	"uv__calloc"
.LASF153:
	.string	"uv_loop_s"
.LASF16:
	.string	"__gid_t"
.LASF118:
	.string	"sin6_family"
.LASF297:
	.string	"UV_SIGNAL"
.LASF330:
	.string	"queue"
.LASF46:
	.string	"_freeres_buf"
.LASF271:
	.string	"UV_UNKNOWN"
.LASF68:
	.string	"mode_t"
.LASF458:
	.string	"UV_HANDLE_TCP_ACCEPT_STATE_CHANGING"
.LASF362:
	.string	"tv_sec"
.LASF87:
	.string	"__wrphase_futex"
.LASF387:
	.string	"UV_FS_CLOSE"
.LASF97:
	.string	"long long unsigned int"
.LASF165:
	.string	"nfds"
.LASF38:
	.string	"_cur_column"
.LASF254:
	.string	"UV_ENOTDIR"
.LASF69:
	.string	"uid_t"
.LASF83:
	.string	"__list"
.LASF374:
	.string	"st_blocks"
.LASF210:
	.string	"UV_EAI_CANCELED"
.LASF434:
	.string	"UV_HANDLE_ACTIVE"
.LASF31:
	.string	"_IO_backup_base"
.LASF388:
	.string	"UV_FS_READ"
.LASF22:
	.string	"_IO_read_ptr"
.LASF263:
	.string	"UV_ERANGE"
.LASF283:
	.string	"UV_CHECK"
.LASF440:
	.string	"UV_HANDLE_SHUTTING"
.LASF230:
	.string	"UV_EFAULT"
.LASF187:
	.string	"inotify_fd"
.LASF45:
	.string	"_freeres_list"
.LASF296:
	.string	"UV_UDP"
.LASF61:
	.string	"_sys_nerr"
.LASF333:
	.string	"uv_fs_poll_s"
.LASF147:
	.string	"timezone"
.LASF85:
	.string	"__readers"
.LASF243:
	.string	"UV_ENETUNREACH"
.LASF37:
	.string	"_old_offset"
.LASF321:
	.string	"uv_timer_s"
.LASF320:
	.string	"uv_timer_t"
.LASF406:
	.string	"UV_FS_RENAME"
.LASF102:
	.string	"long long int"
.LASF139:
	.string	"in6addr_loopback"
.LASF36:
	.string	"_flags2"
.LASF319:
	.string	"next_closing"
.LASF179:
	.string	"timer_counter"
.LASF275:
	.string	"UV_EHOSTDOWN"
.LASF409:
	.string	"UV_FS_SYMLINK"
.LASF363:
	.string	"tv_nsec"
.LASF355:
	.string	"mtime"
.LASF108:
	.string	"sockaddr_at"
.LASF220:
	.string	"UV_EALREADY"
.LASF117:
	.string	"sockaddr_in6"
.LASF433:
	.string	"UV_HANDLE_CLOSED"
.LASF152:
	.string	"loop"
.LASF59:
	.string	"sys_nerr"
.LASF138:
	.string	"in6addr_any"
.LASF299:
	.string	"UV_HANDLE_TYPE_MAX"
.LASF161:
	.string	"pending_queue"
.LASF499:
	.string	"uv_now"
.LASF341:
	.string	"dispatched_signals"
.LASF208:
	.string	"UV_EAI_BADFLAGS"
.LASF259:
	.string	"UV_EPIPE"
.LASF421:
	.string	"UV_FS_LUTIME"
.LASF463:
	.string	"UV_HANDLE_UDP_RECVMMSG"
.LASF506:
	.string	"uv_timer_init"
.LASF110:
	.string	"sockaddr_dl"
.LASF191:
	.string	"events"
.LASF445:
	.string	"UV_HANDLE_BOUND"
.LASF5:
	.string	"unsigned int"
.LASF383:
	.string	"uv_signal_cb"
.LASF124:
	.string	"sockaddr_ipx"
.LASF394:
	.string	"UV_FS_FTRUNCATE"
.LASF378:
	.string	"st_mtim"
.LASF411:
	.string	"UV_FS_CHOWN"
.LASF11:
	.string	"short int"
.LASF470:
	.string	"UV_SIGNAL_ONE_SHOT_DISPATCHED"
.LASF306:
	.string	"UV_UDP_SEND"
.LASF166:
	.string	"wq_mutex"
.LASF39:
	.string	"_vtable_offset"
.LASF206:
	.string	"UV_EAI_ADDRFAMILY"
.LASF328:
	.string	"uv_async_s"
.LASF327:
	.string	"uv_async_t"
.LASF159:
	.string	"flags"
.LASF141:
	.string	"sys_siglist"
.LASF234:
	.string	"UV_EINVAL"
.LASF251:
	.string	"UV_ENOSPC"
.LASF119:
	.string	"sin6_port"
.LASF426:
	.string	"rbe_parent"
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
