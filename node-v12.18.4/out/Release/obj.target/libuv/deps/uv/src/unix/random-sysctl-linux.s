	.file	"random-sysctl-linux.c"
	.text
.Ltext0:
	.p2align 4
	.globl	uv__random_sysctl
	.hidden	uv__random_sysctl
	.type	uv__random_sysctl, @function
uv__random_sysctl:
.LVL0:
.LFB94:
	.file 1 "../deps/uv/src/unix/random-sysctl-linux.c"
	.loc 1 43 49 view -0
	.cfi_startproc
	.loc 1 43 49 is_stmt 0 view .LVU1
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.loc 1 52 6 view .LVU2
	leaq	(%rdi,%rsi), %r13
	.loc 1 43 49 view .LVU3
	pushq	%r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	.loc 1 43 49 view .LVU4
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	.loc 1 44 3 is_stmt 1 view .LVU5
	.loc 1 45 3 view .LVU6
	.loc 1 46 3 view .LVU7
	.loc 1 47 3 view .LVU8
	.loc 1 48 3 view .LVU9
	.loc 1 49 3 view .LVU10
	.loc 1 51 3 view .LVU11
.LVL1:
	.loc 1 52 3 view .LVU12
	.loc 1 54 3 view .LVU13
	.loc 1 54 9 view .LVU14
	cmpq	%r13, %rdi
	jnb	.L6
	leaq	-80(%rbp), %r14
	leaq	-168(%rbp), %rax
	movq	%rdi, %r12
	.loc 1 59 17 is_stmt 0 view .LVU15
	movq	%r14, %xmm2
	movq	%rax, %xmm3
	leaq	-160(%rbp), %r15
	punpcklqdq	%xmm3, %xmm2
	leaq	-152(%rbp), %rbx
	movaps	%xmm2, -192(%rbp)
.LVL2:
	.p2align 4,,10
	.p2align 3
.L2:
	.loc 1 55 5 is_stmt 1 view .LVU16
.LBB6:
.LBI6:
	.file 2 "/usr/include/x86_64-linux-gnu/bits/string_fortified.h"
	.loc 2 59 42 view .LVU17
.LBB7:
	.loc 2 71 3 view .LVU18
	.loc 2 71 10 is_stmt 0 view .LVU19
	movq	$0, 64(%rbx)
.LVL3:
	.loc 2 71 10 view .LVU20
.LBE7:
.LBE6:
	.loc 1 57 5 is_stmt 1 view .LVU21
.LBB11:
.LBB8:
	.loc 2 71 10 is_stmt 0 view .LVU22
	pxor	%xmm0, %xmm0
.LBE8:
.LBE11:
	.loc 1 71 9 view .LVU23
	movq	%r15, %rsi
	.loc 1 57 15 view .LVU24
	leaq	name.9955(%rip), %rax
	.loc 1 59 17 view .LVU25
	movdqa	-192(%rbp), %xmm1
.LBB12:
.LBB9:
	.loc 2 71 10 view .LVU26
	movups	%xmm0, (%rbx)
.LBE9:
.LBE12:
	.loc 1 71 9 view .LVU27
	movl	$156, %edi
	.loc 1 57 15 view .LVU28
	movq	%rax, -160(%rbp)
	.loc 1 58 5 is_stmt 1 view .LVU29
	.loc 1 71 9 is_stmt 0 view .LVU30
	xorl	%eax, %eax
.LBB13:
.LBB10:
	.loc 2 71 10 view .LVU31
	movups	%xmm0, 16(%rbx)
	movups	%xmm0, 32(%rbx)
	movups	%xmm0, 48(%rbx)
.LBE10:
.LBE13:
	.loc 1 58 15 view .LVU32
	movl	$3, -152(%rbp)
	.loc 1 59 5 is_stmt 1 view .LVU33
	.loc 1 60 5 view .LVU34
	.loc 1 61 7 is_stmt 0 view .LVU35
	movq	$16, -168(%rbp)
	.loc 1 59 17 view .LVU36
	movaps	%xmm1, -144(%rbp)
	.loc 1 61 5 is_stmt 1 view .LVU37
	.loc 1 71 5 view .LVU38
	.loc 1 71 9 is_stmt 0 view .LVU39
	call	syscall@PLT
.LVL4:
	.loc 1 71 8 view .LVU40
	cmpq	$-1, %rax
	je	.L13
	.loc 1 80 5 is_stmt 1 view .LVU41
	.loc 1 80 8 is_stmt 0 view .LVU42
	cmpq	$16, -168(%rbp)
	jne	.L8
	.loc 1 87 5 is_stmt 1 view .LVU43
	.loc 1 87 13 is_stmt 0 view .LVU44
	movzbl	-66(%rbp), %eax
	.loc 1 90 12 view .LVU45
	movq	%r13, %rdx
	subq	%r12, %rdx
	.loc 1 87 13 view .LVU46
	movb	%al, -74(%rbp)
	.loc 1 88 5 is_stmt 1 view .LVU47
	.loc 1 88 13 is_stmt 0 view .LVU48
	movzbl	-65(%rbp), %eax
	movb	%al, -72(%rbp)
	.loc 1 90 5 is_stmt 1 view .LVU49
	.loc 1 91 5 view .LVU50
	.loc 1 91 8 is_stmt 0 view .LVU51
	cmpq	$14, %rdx
	ja	.L5
	.loc 1 94 5 is_stmt 1 view .LVU52
.LVL5:
.LBB14:
.LBI14:
	.loc 2 31 42 view .LVU53
.LBB15:
	.loc 2 34 3 view .LVU54
	.loc 2 34 10 is_stmt 0 view .LVU55
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	memcpy@PLT
.LVL6:
	.loc 2 34 10 view .LVU56
.LBE15:
.LBE14:
	.loc 1 95 5 is_stmt 1 view .LVU57
	.loc 1 54 9 view .LVU58
.L6:
	.loc 1 98 10 is_stmt 0 view .LVU59
	xorl	%eax, %eax
.L1:
	.loc 1 99 1 view .LVU60
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L14
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
.LVL7:
	.loc 1 99 1 view .LVU61
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.LVL8:
	.p2align 4,,10
	.p2align 3
.L5:
	.cfi_restore_state
	.loc 1 92 7 is_stmt 1 view .LVU62
.LBB19:
.LBB16:
	.loc 2 34 10 is_stmt 0 view .LVU63
	movq	(%r14), %rdx
	leaq	14(%r12), %rax
.LBE16:
.LBE19:
	.loc 1 94 5 is_stmt 1 view .LVU64
.LVL9:
.LBB20:
	.loc 2 31 42 view .LVU65
.LBB17:
	.loc 2 34 3 view .LVU66
.LBE17:
.LBE20:
	.loc 1 92 9 is_stmt 0 view .LVU67
	movq	$14, -168(%rbp)
.LVL10:
.LBB21:
.LBB18:
	.loc 2 34 10 view .LVU68
	movq	%rdx, (%r12)
	movl	8(%r14), %edx
	movl	%edx, 8(%r12)
	movzwl	12(%r14), %edx
	movw	%dx, 12(%r12)
.LVL11:
	.loc 2 34 10 view .LVU69
.LBE18:
.LBE21:
	.loc 1 95 5 is_stmt 1 view .LVU70
	.loc 1 54 9 view .LVU71
	cmpq	%r13, %rax
	jnb	.L6
	.loc 1 54 9 is_stmt 0 view .LVU72
	movq	%rax, %r12
	jmp	.L2
.LVL12:
	.p2align 4,,10
	.p2align 3
.L13:
	.loc 1 72 7 is_stmt 1 view .LVU73
	.loc 1 72 15 is_stmt 0 view .LVU74
	call	__errno_location@PLT
.LVL13:
	.loc 1 72 15 view .LVU75
	movl	(%rax), %eax
	negl	%eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L8:
	.loc 1 81 14 view .LVU76
	movl	$-5, %eax
	jmp	.L1
.LVL14:
.L14:
	.loc 1 99 1 view .LVU77
	call	__stack_chk_fail@PLT
.LVL15:
	.cfi_endproc
.LFE94:
	.size	uv__random_sysctl, .-uv__random_sysctl
	.data
	.align 8
	.type	name.9955, @object
	.size	name.9955, 12
name.9955:
	.long	1
	.long	40
	.long	6
	.text
.Letext0:
	.file 3 "/usr/include/errno.h"
	.file 4 "/usr/lib/gcc/x86_64-linux-gnu/9/include/stddef.h"
	.file 5 "/usr/include/x86_64-linux-gnu/bits/types.h"
	.file 6 "/usr/include/x86_64-linux-gnu/bits/types/struct_FILE.h"
	.file 7 "/usr/include/x86_64-linux-gnu/bits/types/FILE.h"
	.file 8 "/usr/include/stdio.h"
	.file 9 "/usr/include/x86_64-linux-gnu/bits/sys_errlist.h"
	.file 10 "/usr/include/x86_64-linux-gnu/bits/stdint-uintn.h"
	.file 11 "/usr/include/x86_64-linux-gnu/bits/sockaddr.h"
	.file 12 "/usr/include/x86_64-linux-gnu/bits/socket.h"
	.file 13 "/usr/include/netinet/in.h"
	.file 14 "/usr/include/signal.h"
	.file 15 "/usr/include/time.h"
	.file 16 "/usr/include/unistd.h"
	.file 17 "/usr/include/x86_64-linux-gnu/bits/getopt_core.h"
	.file 18 "../deps/uv/include/uv.h"
	.file 19 "<built-in>"
	.section	.debug_info,"",@progbits
.Ldebug_info0:
	.long	0xc96
	.value	0x4
	.long	.Ldebug_abbrev0
	.byte	0x8
	.uleb128 0x1
	.long	.LASF214
	.byte	0x1
	.long	.LASF215
	.long	.LASF216
	.quad	.Ltext0
	.quad	.Letext0-.Ltext0
	.long	.Ldebug_line0
	.uleb128 0x2
	.long	.LASF0
	.byte	0x3
	.byte	0x2d
	.byte	0xe
	.long	0x39
	.uleb128 0x3
	.byte	0x8
	.long	0x3f
	.uleb128 0x4
	.byte	0x1
	.byte	0x6
	.long	.LASF2
	.uleb128 0x5
	.long	0x3f
	.uleb128 0x2
	.long	.LASF1
	.byte	0x3
	.byte	0x2e
	.byte	0xe
	.long	0x39
	.uleb128 0x6
	.byte	0x4
	.byte	0x5
	.string	"int"
	.uleb128 0x4
	.byte	0x8
	.byte	0x5
	.long	.LASF3
	.uleb128 0x7
	.long	.LASF9
	.byte	0x4
	.byte	0xd1
	.byte	0x1b
	.long	0x71
	.uleb128 0x4
	.byte	0x8
	.byte	0x7
	.long	.LASF4
	.uleb128 0x4
	.byte	0x4
	.byte	0x7
	.long	.LASF5
	.uleb128 0x8
	.byte	0x8
	.uleb128 0x9
	.long	0x7f
	.uleb128 0x4
	.byte	0x1
	.byte	0x8
	.long	.LASF6
	.uleb128 0x4
	.byte	0x2
	.byte	0x7
	.long	.LASF7
	.uleb128 0x4
	.byte	0x1
	.byte	0x6
	.long	.LASF8
	.uleb128 0x7
	.long	.LASF10
	.byte	0x5
	.byte	0x26
	.byte	0x17
	.long	0x86
	.uleb128 0x4
	.byte	0x2
	.byte	0x5
	.long	.LASF11
	.uleb128 0x7
	.long	.LASF12
	.byte	0x5
	.byte	0x28
	.byte	0x1c
	.long	0x8d
	.uleb128 0x7
	.long	.LASF13
	.byte	0x5
	.byte	0x2a
	.byte	0x16
	.long	0x78
	.uleb128 0x7
	.long	.LASF14
	.byte	0x5
	.byte	0x98
	.byte	0x12
	.long	0x5e
	.uleb128 0x7
	.long	.LASF15
	.byte	0x5
	.byte	0x99
	.byte	0x12
	.long	0x5e
	.uleb128 0xa
	.long	.LASF62
	.byte	0xd8
	.byte	0x6
	.byte	0x31
	.byte	0x8
	.long	0x265
	.uleb128 0xb
	.long	.LASF16
	.byte	0x6
	.byte	0x33
	.byte	0x7
	.long	0x57
	.byte	0
	.uleb128 0xb
	.long	.LASF17
	.byte	0x6
	.byte	0x36
	.byte	0x9
	.long	0x39
	.byte	0x8
	.uleb128 0xb
	.long	.LASF18
	.byte	0x6
	.byte	0x37
	.byte	0x9
	.long	0x39
	.byte	0x10
	.uleb128 0xb
	.long	.LASF19
	.byte	0x6
	.byte	0x38
	.byte	0x9
	.long	0x39
	.byte	0x18
	.uleb128 0xb
	.long	.LASF20
	.byte	0x6
	.byte	0x39
	.byte	0x9
	.long	0x39
	.byte	0x20
	.uleb128 0xb
	.long	.LASF21
	.byte	0x6
	.byte	0x3a
	.byte	0x9
	.long	0x39
	.byte	0x28
	.uleb128 0xb
	.long	.LASF22
	.byte	0x6
	.byte	0x3b
	.byte	0x9
	.long	0x39
	.byte	0x30
	.uleb128 0xb
	.long	.LASF23
	.byte	0x6
	.byte	0x3c
	.byte	0x9
	.long	0x39
	.byte	0x38
	.uleb128 0xb
	.long	.LASF24
	.byte	0x6
	.byte	0x3d
	.byte	0x9
	.long	0x39
	.byte	0x40
	.uleb128 0xb
	.long	.LASF25
	.byte	0x6
	.byte	0x40
	.byte	0x9
	.long	0x39
	.byte	0x48
	.uleb128 0xb
	.long	.LASF26
	.byte	0x6
	.byte	0x41
	.byte	0x9
	.long	0x39
	.byte	0x50
	.uleb128 0xb
	.long	.LASF27
	.byte	0x6
	.byte	0x42
	.byte	0x9
	.long	0x39
	.byte	0x58
	.uleb128 0xb
	.long	.LASF28
	.byte	0x6
	.byte	0x44
	.byte	0x16
	.long	0x27e
	.byte	0x60
	.uleb128 0xb
	.long	.LASF29
	.byte	0x6
	.byte	0x46
	.byte	0x14
	.long	0x284
	.byte	0x68
	.uleb128 0xb
	.long	.LASF30
	.byte	0x6
	.byte	0x48
	.byte	0x7
	.long	0x57
	.byte	0x70
	.uleb128 0xb
	.long	.LASF31
	.byte	0x6
	.byte	0x49
	.byte	0x7
	.long	0x57
	.byte	0x74
	.uleb128 0xb
	.long	.LASF32
	.byte	0x6
	.byte	0x4a
	.byte	0xb
	.long	0xc6
	.byte	0x78
	.uleb128 0xb
	.long	.LASF33
	.byte	0x6
	.byte	0x4d
	.byte	0x12
	.long	0x8d
	.byte	0x80
	.uleb128 0xb
	.long	.LASF34
	.byte	0x6
	.byte	0x4e
	.byte	0xf
	.long	0x94
	.byte	0x82
	.uleb128 0xb
	.long	.LASF35
	.byte	0x6
	.byte	0x4f
	.byte	0x8
	.long	0x28a
	.byte	0x83
	.uleb128 0xb
	.long	.LASF36
	.byte	0x6
	.byte	0x51
	.byte	0xf
	.long	0x29a
	.byte	0x88
	.uleb128 0xb
	.long	.LASF37
	.byte	0x6
	.byte	0x59
	.byte	0xd
	.long	0xd2
	.byte	0x90
	.uleb128 0xb
	.long	.LASF38
	.byte	0x6
	.byte	0x5b
	.byte	0x17
	.long	0x2a5
	.byte	0x98
	.uleb128 0xb
	.long	.LASF39
	.byte	0x6
	.byte	0x5c
	.byte	0x19
	.long	0x2b0
	.byte	0xa0
	.uleb128 0xb
	.long	.LASF40
	.byte	0x6
	.byte	0x5d
	.byte	0x14
	.long	0x284
	.byte	0xa8
	.uleb128 0xb
	.long	.LASF41
	.byte	0x6
	.byte	0x5e
	.byte	0x9
	.long	0x7f
	.byte	0xb0
	.uleb128 0xb
	.long	.LASF42
	.byte	0x6
	.byte	0x5f
	.byte	0xa
	.long	0x65
	.byte	0xb8
	.uleb128 0xb
	.long	.LASF43
	.byte	0x6
	.byte	0x60
	.byte	0x7
	.long	0x57
	.byte	0xc0
	.uleb128 0xb
	.long	.LASF44
	.byte	0x6
	.byte	0x62
	.byte	0x8
	.long	0x2b6
	.byte	0xc4
	.byte	0
	.uleb128 0x7
	.long	.LASF45
	.byte	0x7
	.byte	0x7
	.byte	0x19
	.long	0xde
	.uleb128 0xc
	.long	.LASF217
	.byte	0x6
	.byte	0x2b
	.byte	0xe
	.uleb128 0xd
	.long	.LASF46
	.uleb128 0x3
	.byte	0x8
	.long	0x279
	.uleb128 0x3
	.byte	0x8
	.long	0xde
	.uleb128 0xe
	.long	0x3f
	.long	0x29a
	.uleb128 0xf
	.long	0x71
	.byte	0
	.byte	0
	.uleb128 0x3
	.byte	0x8
	.long	0x271
	.uleb128 0xd
	.long	.LASF47
	.uleb128 0x3
	.byte	0x8
	.long	0x2a0
	.uleb128 0xd
	.long	.LASF48
	.uleb128 0x3
	.byte	0x8
	.long	0x2ab
	.uleb128 0xe
	.long	0x3f
	.long	0x2c6
	.uleb128 0xf
	.long	0x71
	.byte	0x13
	.byte	0
	.uleb128 0x3
	.byte	0x8
	.long	0x46
	.uleb128 0x5
	.long	0x2c6
	.uleb128 0x2
	.long	.LASF49
	.byte	0x8
	.byte	0x89
	.byte	0xe
	.long	0x2dd
	.uleb128 0x3
	.byte	0x8
	.long	0x265
	.uleb128 0x2
	.long	.LASF50
	.byte	0x8
	.byte	0x8a
	.byte	0xe
	.long	0x2dd
	.uleb128 0x2
	.long	.LASF51
	.byte	0x8
	.byte	0x8b
	.byte	0xe
	.long	0x2dd
	.uleb128 0x2
	.long	.LASF52
	.byte	0x9
	.byte	0x1a
	.byte	0xc
	.long	0x57
	.uleb128 0xe
	.long	0x2cc
	.long	0x312
	.uleb128 0x10
	.byte	0
	.uleb128 0x5
	.long	0x307
	.uleb128 0x2
	.long	.LASF53
	.byte	0x9
	.byte	0x1b
	.byte	0x1a
	.long	0x312
	.uleb128 0x2
	.long	.LASF54
	.byte	0x9
	.byte	0x1e
	.byte	0xc
	.long	0x57
	.uleb128 0x2
	.long	.LASF55
	.byte	0x9
	.byte	0x1f
	.byte	0x1a
	.long	0x312
	.uleb128 0x7
	.long	.LASF56
	.byte	0xa
	.byte	0x18
	.byte	0x13
	.long	0x9b
	.uleb128 0x7
	.long	.LASF57
	.byte	0xa
	.byte	0x19
	.byte	0x14
	.long	0xae
	.uleb128 0x7
	.long	.LASF58
	.byte	0xa
	.byte	0x1a
	.byte	0x14
	.long	0xba
	.uleb128 0x4
	.byte	0x8
	.byte	0x7
	.long	.LASF59
	.uleb128 0x4
	.byte	0x8
	.byte	0x5
	.long	.LASF60
	.uleb128 0x7
	.long	.LASF61
	.byte	0xb
	.byte	0x1c
	.byte	0x1c
	.long	0x8d
	.uleb128 0xa
	.long	.LASF63
	.byte	0x10
	.byte	0xc
	.byte	0xb2
	.byte	0x8
	.long	0x3a1
	.uleb128 0xb
	.long	.LASF64
	.byte	0xc
	.byte	0xb4
	.byte	0x11
	.long	0x36d
	.byte	0
	.uleb128 0xb
	.long	.LASF65
	.byte	0xc
	.byte	0xb5
	.byte	0xa
	.long	0x3a6
	.byte	0x2
	.byte	0
	.uleb128 0x5
	.long	0x379
	.uleb128 0xe
	.long	0x3f
	.long	0x3b6
	.uleb128 0xf
	.long	0x71
	.byte	0xd
	.byte	0
	.uleb128 0x3
	.byte	0x8
	.long	0x379
	.uleb128 0x9
	.long	0x3b6
	.uleb128 0xd
	.long	.LASF66
	.uleb128 0x5
	.long	0x3c1
	.uleb128 0x3
	.byte	0x8
	.long	0x3c1
	.uleb128 0x9
	.long	0x3cb
	.uleb128 0xd
	.long	.LASF67
	.uleb128 0x5
	.long	0x3d6
	.uleb128 0x3
	.byte	0x8
	.long	0x3d6
	.uleb128 0x9
	.long	0x3e0
	.uleb128 0xd
	.long	.LASF68
	.uleb128 0x5
	.long	0x3eb
	.uleb128 0x3
	.byte	0x8
	.long	0x3eb
	.uleb128 0x9
	.long	0x3f5
	.uleb128 0xd
	.long	.LASF69
	.uleb128 0x5
	.long	0x400
	.uleb128 0x3
	.byte	0x8
	.long	0x400
	.uleb128 0x9
	.long	0x40a
	.uleb128 0xa
	.long	.LASF70
	.byte	0x10
	.byte	0xd
	.byte	0xee
	.byte	0x8
	.long	0x457
	.uleb128 0xb
	.long	.LASF71
	.byte	0xd
	.byte	0xf0
	.byte	0x11
	.long	0x36d
	.byte	0
	.uleb128 0xb
	.long	.LASF72
	.byte	0xd
	.byte	0xf1
	.byte	0xf
	.long	0x5fe
	.byte	0x2
	.uleb128 0xb
	.long	.LASF73
	.byte	0xd
	.byte	0xf2
	.byte	0x14
	.long	0x5e3
	.byte	0x4
	.uleb128 0xb
	.long	.LASF74
	.byte	0xd
	.byte	0xf5
	.byte	0x13
	.long	0x6a0
	.byte	0x8
	.byte	0
	.uleb128 0x5
	.long	0x415
	.uleb128 0x3
	.byte	0x8
	.long	0x415
	.uleb128 0x9
	.long	0x45c
	.uleb128 0xa
	.long	.LASF75
	.byte	0x1c
	.byte	0xd
	.byte	0xfd
	.byte	0x8
	.long	0x4ba
	.uleb128 0xb
	.long	.LASF76
	.byte	0xd
	.byte	0xff
	.byte	0x11
	.long	0x36d
	.byte	0
	.uleb128 0x11
	.long	.LASF77
	.byte	0xd
	.value	0x100
	.byte	0xf
	.long	0x5fe
	.byte	0x2
	.uleb128 0x11
	.long	.LASF78
	.byte	0xd
	.value	0x101
	.byte	0xe
	.long	0x353
	.byte	0x4
	.uleb128 0x11
	.long	.LASF79
	.byte	0xd
	.value	0x102
	.byte	0x15
	.long	0x668
	.byte	0x8
	.uleb128 0x11
	.long	.LASF80
	.byte	0xd
	.value	0x103
	.byte	0xe
	.long	0x353
	.byte	0x18
	.byte	0
	.uleb128 0x5
	.long	0x467
	.uleb128 0x3
	.byte	0x8
	.long	0x467
	.uleb128 0x9
	.long	0x4bf
	.uleb128 0xd
	.long	.LASF81
	.uleb128 0x5
	.long	0x4ca
	.uleb128 0x3
	.byte	0x8
	.long	0x4ca
	.uleb128 0x9
	.long	0x4d4
	.uleb128 0xd
	.long	.LASF82
	.uleb128 0x5
	.long	0x4df
	.uleb128 0x3
	.byte	0x8
	.long	0x4df
	.uleb128 0x9
	.long	0x4e9
	.uleb128 0xd
	.long	.LASF83
	.uleb128 0x5
	.long	0x4f4
	.uleb128 0x3
	.byte	0x8
	.long	0x4f4
	.uleb128 0x9
	.long	0x4fe
	.uleb128 0xd
	.long	.LASF84
	.uleb128 0x5
	.long	0x509
	.uleb128 0x3
	.byte	0x8
	.long	0x509
	.uleb128 0x9
	.long	0x513
	.uleb128 0xd
	.long	.LASF85
	.uleb128 0x5
	.long	0x51e
	.uleb128 0x3
	.byte	0x8
	.long	0x51e
	.uleb128 0x9
	.long	0x528
	.uleb128 0xd
	.long	.LASF86
	.uleb128 0x5
	.long	0x533
	.uleb128 0x3
	.byte	0x8
	.long	0x533
	.uleb128 0x9
	.long	0x53d
	.uleb128 0x3
	.byte	0x8
	.long	0x3a1
	.uleb128 0x9
	.long	0x548
	.uleb128 0x3
	.byte	0x8
	.long	0x3c6
	.uleb128 0x9
	.long	0x553
	.uleb128 0x3
	.byte	0x8
	.long	0x3db
	.uleb128 0x9
	.long	0x55e
	.uleb128 0x3
	.byte	0x8
	.long	0x3f0
	.uleb128 0x9
	.long	0x569
	.uleb128 0x3
	.byte	0x8
	.long	0x405
	.uleb128 0x9
	.long	0x574
	.uleb128 0x3
	.byte	0x8
	.long	0x457
	.uleb128 0x9
	.long	0x57f
	.uleb128 0x3
	.byte	0x8
	.long	0x4ba
	.uleb128 0x9
	.long	0x58a
	.uleb128 0x3
	.byte	0x8
	.long	0x4cf
	.uleb128 0x9
	.long	0x595
	.uleb128 0x3
	.byte	0x8
	.long	0x4e4
	.uleb128 0x9
	.long	0x5a0
	.uleb128 0x3
	.byte	0x8
	.long	0x4f9
	.uleb128 0x9
	.long	0x5ab
	.uleb128 0x3
	.byte	0x8
	.long	0x50e
	.uleb128 0x9
	.long	0x5b6
	.uleb128 0x3
	.byte	0x8
	.long	0x523
	.uleb128 0x9
	.long	0x5c1
	.uleb128 0x3
	.byte	0x8
	.long	0x538
	.uleb128 0x9
	.long	0x5cc
	.uleb128 0x7
	.long	.LASF87
	.byte	0xd
	.byte	0x1e
	.byte	0x12
	.long	0x353
	.uleb128 0xa
	.long	.LASF88
	.byte	0x4
	.byte	0xd
	.byte	0x1f
	.byte	0x8
	.long	0x5fe
	.uleb128 0xb
	.long	.LASF89
	.byte	0xd
	.byte	0x21
	.byte	0xf
	.long	0x5d7
	.byte	0
	.byte	0
	.uleb128 0x7
	.long	.LASF90
	.byte	0xd
	.byte	0x77
	.byte	0x12
	.long	0x347
	.uleb128 0x12
	.byte	0x10
	.byte	0xd
	.byte	0xd6
	.byte	0x5
	.long	0x638
	.uleb128 0x13
	.long	.LASF91
	.byte	0xd
	.byte	0xd8
	.byte	0xa
	.long	0x638
	.uleb128 0x13
	.long	.LASF92
	.byte	0xd
	.byte	0xd9
	.byte	0xb
	.long	0x648
	.uleb128 0x13
	.long	.LASF93
	.byte	0xd
	.byte	0xda
	.byte	0xb
	.long	0x658
	.byte	0
	.uleb128 0xe
	.long	0x33b
	.long	0x648
	.uleb128 0xf
	.long	0x71
	.byte	0xf
	.byte	0
	.uleb128 0xe
	.long	0x347
	.long	0x658
	.uleb128 0xf
	.long	0x71
	.byte	0x7
	.byte	0
	.uleb128 0xe
	.long	0x353
	.long	0x668
	.uleb128 0xf
	.long	0x71
	.byte	0x3
	.byte	0
	.uleb128 0xa
	.long	.LASF94
	.byte	0x10
	.byte	0xd
	.byte	0xd4
	.byte	0x8
	.long	0x683
	.uleb128 0xb
	.long	.LASF95
	.byte	0xd
	.byte	0xdb
	.byte	0x9
	.long	0x60a
	.byte	0
	.byte	0
	.uleb128 0x5
	.long	0x668
	.uleb128 0x2
	.long	.LASF96
	.byte	0xd
	.byte	0xe4
	.byte	0x1e
	.long	0x683
	.uleb128 0x2
	.long	.LASF97
	.byte	0xd
	.byte	0xe5
	.byte	0x1e
	.long	0x683
	.uleb128 0xe
	.long	0x86
	.long	0x6b0
	.uleb128 0xf
	.long	0x71
	.byte	0x7
	.byte	0
	.uleb128 0x3
	.byte	0x8
	.long	0x39
	.uleb128 0xe
	.long	0x2cc
	.long	0x6c6
	.uleb128 0xf
	.long	0x71
	.byte	0x40
	.byte	0
	.uleb128 0x5
	.long	0x6b6
	.uleb128 0x14
	.long	.LASF98
	.byte	0xe
	.value	0x11e
	.byte	0x1a
	.long	0x6c6
	.uleb128 0x14
	.long	.LASF99
	.byte	0xe
	.value	0x11f
	.byte	0x1a
	.long	0x6c6
	.uleb128 0xe
	.long	0x39
	.long	0x6f5
	.uleb128 0xf
	.long	0x71
	.byte	0x1
	.byte	0
	.uleb128 0x2
	.long	.LASF100
	.byte	0xf
	.byte	0x9f
	.byte	0xe
	.long	0x6e5
	.uleb128 0x2
	.long	.LASF101
	.byte	0xf
	.byte	0xa0
	.byte	0xc
	.long	0x57
	.uleb128 0x2
	.long	.LASF102
	.byte	0xf
	.byte	0xa1
	.byte	0x11
	.long	0x5e
	.uleb128 0x2
	.long	.LASF103
	.byte	0xf
	.byte	0xa6
	.byte	0xe
	.long	0x6e5
	.uleb128 0x2
	.long	.LASF104
	.byte	0xf
	.byte	0xae
	.byte	0xc
	.long	0x57
	.uleb128 0x2
	.long	.LASF105
	.byte	0xf
	.byte	0xaf
	.byte	0x11
	.long	0x5e
	.uleb128 0x14
	.long	.LASF106
	.byte	0xf
	.value	0x112
	.byte	0xc
	.long	0x57
	.uleb128 0x15
	.byte	0x5
	.byte	0x4
	.long	0x57
	.byte	0x12
	.byte	0xb6
	.byte	0xe
	.long	0x96d
	.uleb128 0x16
	.long	.LASF107
	.sleb128 -7
	.uleb128 0x16
	.long	.LASF108
	.sleb128 -13
	.uleb128 0x16
	.long	.LASF109
	.sleb128 -98
	.uleb128 0x16
	.long	.LASF110
	.sleb128 -99
	.uleb128 0x16
	.long	.LASF111
	.sleb128 -97
	.uleb128 0x16
	.long	.LASF112
	.sleb128 -11
	.uleb128 0x16
	.long	.LASF113
	.sleb128 -3000
	.uleb128 0x16
	.long	.LASF114
	.sleb128 -3001
	.uleb128 0x16
	.long	.LASF115
	.sleb128 -3002
	.uleb128 0x16
	.long	.LASF116
	.sleb128 -3013
	.uleb128 0x16
	.long	.LASF117
	.sleb128 -3003
	.uleb128 0x16
	.long	.LASF118
	.sleb128 -3004
	.uleb128 0x16
	.long	.LASF119
	.sleb128 -3005
	.uleb128 0x16
	.long	.LASF120
	.sleb128 -3006
	.uleb128 0x16
	.long	.LASF121
	.sleb128 -3007
	.uleb128 0x16
	.long	.LASF122
	.sleb128 -3008
	.uleb128 0x16
	.long	.LASF123
	.sleb128 -3009
	.uleb128 0x16
	.long	.LASF124
	.sleb128 -3014
	.uleb128 0x16
	.long	.LASF125
	.sleb128 -3010
	.uleb128 0x16
	.long	.LASF126
	.sleb128 -3011
	.uleb128 0x16
	.long	.LASF127
	.sleb128 -114
	.uleb128 0x16
	.long	.LASF128
	.sleb128 -9
	.uleb128 0x16
	.long	.LASF129
	.sleb128 -16
	.uleb128 0x16
	.long	.LASF130
	.sleb128 -125
	.uleb128 0x16
	.long	.LASF131
	.sleb128 -4080
	.uleb128 0x16
	.long	.LASF132
	.sleb128 -103
	.uleb128 0x16
	.long	.LASF133
	.sleb128 -111
	.uleb128 0x16
	.long	.LASF134
	.sleb128 -104
	.uleb128 0x16
	.long	.LASF135
	.sleb128 -89
	.uleb128 0x16
	.long	.LASF136
	.sleb128 -17
	.uleb128 0x16
	.long	.LASF137
	.sleb128 -14
	.uleb128 0x16
	.long	.LASF138
	.sleb128 -27
	.uleb128 0x16
	.long	.LASF139
	.sleb128 -113
	.uleb128 0x16
	.long	.LASF140
	.sleb128 -4
	.uleb128 0x16
	.long	.LASF141
	.sleb128 -22
	.uleb128 0x16
	.long	.LASF142
	.sleb128 -5
	.uleb128 0x16
	.long	.LASF143
	.sleb128 -106
	.uleb128 0x16
	.long	.LASF144
	.sleb128 -21
	.uleb128 0x16
	.long	.LASF145
	.sleb128 -40
	.uleb128 0x16
	.long	.LASF146
	.sleb128 -24
	.uleb128 0x16
	.long	.LASF147
	.sleb128 -90
	.uleb128 0x16
	.long	.LASF148
	.sleb128 -36
	.uleb128 0x16
	.long	.LASF149
	.sleb128 -100
	.uleb128 0x16
	.long	.LASF150
	.sleb128 -101
	.uleb128 0x16
	.long	.LASF151
	.sleb128 -23
	.uleb128 0x16
	.long	.LASF152
	.sleb128 -105
	.uleb128 0x16
	.long	.LASF153
	.sleb128 -19
	.uleb128 0x16
	.long	.LASF154
	.sleb128 -2
	.uleb128 0x16
	.long	.LASF155
	.sleb128 -12
	.uleb128 0x16
	.long	.LASF156
	.sleb128 -64
	.uleb128 0x16
	.long	.LASF157
	.sleb128 -92
	.uleb128 0x16
	.long	.LASF158
	.sleb128 -28
	.uleb128 0x16
	.long	.LASF159
	.sleb128 -38
	.uleb128 0x16
	.long	.LASF160
	.sleb128 -107
	.uleb128 0x16
	.long	.LASF161
	.sleb128 -20
	.uleb128 0x16
	.long	.LASF162
	.sleb128 -39
	.uleb128 0x16
	.long	.LASF163
	.sleb128 -88
	.uleb128 0x16
	.long	.LASF164
	.sleb128 -95
	.uleb128 0x16
	.long	.LASF165
	.sleb128 -1
	.uleb128 0x16
	.long	.LASF166
	.sleb128 -32
	.uleb128 0x16
	.long	.LASF167
	.sleb128 -71
	.uleb128 0x16
	.long	.LASF168
	.sleb128 -93
	.uleb128 0x16
	.long	.LASF169
	.sleb128 -91
	.uleb128 0x16
	.long	.LASF170
	.sleb128 -34
	.uleb128 0x16
	.long	.LASF171
	.sleb128 -30
	.uleb128 0x16
	.long	.LASF172
	.sleb128 -108
	.uleb128 0x16
	.long	.LASF173
	.sleb128 -29
	.uleb128 0x16
	.long	.LASF174
	.sleb128 -3
	.uleb128 0x16
	.long	.LASF175
	.sleb128 -110
	.uleb128 0x16
	.long	.LASF176
	.sleb128 -26
	.uleb128 0x16
	.long	.LASF177
	.sleb128 -18
	.uleb128 0x16
	.long	.LASF178
	.sleb128 -4094
	.uleb128 0x16
	.long	.LASF179
	.sleb128 -4095
	.uleb128 0x16
	.long	.LASF180
	.sleb128 -6
	.uleb128 0x16
	.long	.LASF181
	.sleb128 -31
	.uleb128 0x16
	.long	.LASF182
	.sleb128 -112
	.uleb128 0x16
	.long	.LASF183
	.sleb128 -121
	.uleb128 0x16
	.long	.LASF184
	.sleb128 -25
	.uleb128 0x16
	.long	.LASF185
	.sleb128 -4028
	.uleb128 0x16
	.long	.LASF186
	.sleb128 -84
	.uleb128 0x16
	.long	.LASF187
	.sleb128 -4096
	.byte	0
	.uleb128 0x4
	.byte	0x8
	.byte	0x4
	.long	.LASF188
	.uleb128 0x3
	.byte	0x8
	.long	0x97f
	.uleb128 0x9
	.long	0x974
	.uleb128 0x17
	.uleb128 0x14
	.long	.LASF189
	.byte	0x10
	.value	0x21f
	.byte	0xf
	.long	0x6b0
	.uleb128 0x14
	.long	.LASF190
	.byte	0x10
	.value	0x221
	.byte	0xf
	.long	0x6b0
	.uleb128 0x2
	.long	.LASF191
	.byte	0x11
	.byte	0x24
	.byte	0xe
	.long	0x39
	.uleb128 0x2
	.long	.LASF192
	.byte	0x11
	.byte	0x32
	.byte	0xc
	.long	0x57
	.uleb128 0x2
	.long	.LASF193
	.byte	0x11
	.byte	0x37
	.byte	0xc
	.long	0x57
	.uleb128 0x2
	.long	.LASF194
	.byte	0x11
	.byte	0x3b
	.byte	0xc
	.long	0x57
	.uleb128 0xa
	.long	.LASF195
	.byte	0x50
	.byte	0x1
	.byte	0x20
	.byte	0x8
	.long	0xa33
	.uleb128 0xb
	.long	.LASF196
	.byte	0x1
	.byte	0x21
	.byte	0x8
	.long	0xa33
	.byte	0
	.uleb128 0xb
	.long	.LASF197
	.byte	0x1
	.byte	0x22
	.byte	0x7
	.long	0x57
	.byte	0x8
	.uleb128 0xb
	.long	.LASF198
	.byte	0x1
	.byte	0x23
	.byte	0x9
	.long	0x7f
	.byte	0x10
	.uleb128 0xb
	.long	.LASF199
	.byte	0x1
	.byte	0x24
	.byte	0xb
	.long	0xa39
	.byte	0x18
	.uleb128 0xb
	.long	.LASF200
	.byte	0x1
	.byte	0x25
	.byte	0x9
	.long	0x7f
	.byte	0x20
	.uleb128 0xb
	.long	.LASF201
	.byte	0x1
	.byte	0x26
	.byte	0xa
	.long	0x65
	.byte	0x28
	.uleb128 0xb
	.long	.LASF202
	.byte	0x1
	.byte	0x27
	.byte	0x11
	.long	0xa3f
	.byte	0x30
	.byte	0
	.uleb128 0x3
	.byte	0x8
	.long	0x57
	.uleb128 0x3
	.byte	0x8
	.long	0x65
	.uleb128 0xe
	.long	0x71
	.long	0xa4f
	.uleb128 0xf
	.long	0x71
	.byte	0x3
	.byte	0
	.uleb128 0x18
	.long	.LASF218
	.byte	0x1
	.byte	0x2b
	.byte	0x5
	.long	0x57
	.quad	.LFB94
	.quad	.LFE94-.LFB94
	.uleb128 0x1
	.byte	0x9c
	.long	0xbe0
	.uleb128 0x19
	.string	"buf"
	.byte	0x1
	.byte	0x2b
	.byte	0x1d
	.long	0x7f
	.long	.LLST0
	.long	.LVUS0
	.uleb128 0x1a
	.long	.LASF203
	.byte	0x1
	.byte	0x2b
	.byte	0x29
	.long	0x65
	.long	.LLST1
	.long	.LVUS1
	.uleb128 0x1b
	.long	.LASF196
	.byte	0x1
	.byte	0x2c
	.byte	0xe
	.long	0xbe0
	.uleb128 0x9
	.byte	0x3
	.quad	name.9955
	.uleb128 0x1b
	.long	.LASF204
	.byte	0x1
	.byte	0x2d
	.byte	0x1a
	.long	0x9ca
	.uleb128 0x3
	.byte	0x91
	.sleb128 -176
	.uleb128 0x1b
	.long	.LASF205
	.byte	0x1
	.byte	0x2e
	.byte	0x8
	.long	0xbf0
	.uleb128 0x3
	.byte	0x91
	.sleb128 -96
	.uleb128 0x1c
	.string	"p"
	.byte	0x1
	.byte	0x2f
	.byte	0x9
	.long	0x39
	.long	.LLST2
	.long	.LVUS2
	.uleb128 0x1c
	.string	"pe"
	.byte	0x1
	.byte	0x30
	.byte	0x9
	.long	0x39
	.long	.LLST3
	.long	.LVUS3
	.uleb128 0x1d
	.string	"n"
	.byte	0x1
	.byte	0x31
	.byte	0xa
	.long	0x65
	.uleb128 0x3
	.byte	0x91
	.sleb128 -184
	.uleb128 0x1e
	.long	0xc00
	.quad	.LBI6
	.byte	.LVU17
	.long	.Ldebug_ranges0+0
	.byte	0x1
	.byte	0x37
	.byte	0x5
	.long	0xb43
	.uleb128 0x1f
	.long	0xc29
	.long	.LLST4
	.long	.LVUS4
	.uleb128 0x1f
	.long	0xc1d
	.long	.LLST5
	.long	.LVUS5
	.uleb128 0x1f
	.long	0xc11
	.long	.LLST6
	.long	.LVUS6
	.byte	0
	.uleb128 0x1e
	.long	0xc36
	.quad	.LBI14
	.byte	.LVU53
	.long	.Ldebug_ranges0+0x50
	.byte	0x1
	.byte	0x5e
	.byte	0x5
	.long	0xba7
	.uleb128 0x1f
	.long	0xc5f
	.long	.LLST7
	.long	.LVUS7
	.uleb128 0x1f
	.long	0xc53
	.long	.LLST8
	.long	.LVUS8
	.uleb128 0x1f
	.long	0xc47
	.long	.LLST9
	.long	.LVUS9
	.uleb128 0x20
	.quad	.LVL6
	.long	0xc6c
	.uleb128 0x21
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x2
	.byte	0x7c
	.sleb128 0
	.uleb128 0x21
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x2
	.byte	0x7e
	.sleb128 0
	.uleb128 0x21
	.uleb128 0x1
	.byte	0x51
	.uleb128 0x5
	.byte	0x7d
	.sleb128 0
	.byte	0x7c
	.sleb128 0
	.byte	0x1c
	.byte	0
	.byte	0
	.uleb128 0x22
	.quad	.LVL4
	.long	0xc77
	.long	0xbc5
	.uleb128 0x21
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x2
	.byte	0x8
	.byte	0x9c
	.uleb128 0x21
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x2
	.byte	0x7f
	.sleb128 0
	.byte	0
	.uleb128 0x23
	.quad	.LVL13
	.long	0xc84
	.uleb128 0x23
	.quad	.LVL15
	.long	0xc90
	.byte	0
	.uleb128 0xe
	.long	0x57
	.long	0xbf0
	.uleb128 0xf
	.long	0x71
	.byte	0x2
	.byte	0
	.uleb128 0xe
	.long	0x3f
	.long	0xc00
	.uleb128 0xf
	.long	0x71
	.byte	0xf
	.byte	0
	.uleb128 0x24
	.long	.LASF209
	.byte	0x2
	.byte	0x3b
	.byte	0x2a
	.long	0x7f
	.byte	0x3
	.long	0xc36
	.uleb128 0x25
	.long	.LASF206
	.byte	0x2
	.byte	0x3b
	.byte	0x38
	.long	0x7f
	.uleb128 0x25
	.long	.LASF207
	.byte	0x2
	.byte	0x3b
	.byte	0x44
	.long	0x57
	.uleb128 0x25
	.long	.LASF208
	.byte	0x2
	.byte	0x3b
	.byte	0x51
	.long	0x65
	.byte	0
	.uleb128 0x24
	.long	.LASF210
	.byte	0x2
	.byte	0x1f
	.byte	0x2a
	.long	0x7f
	.byte	0x3
	.long	0xc6c
	.uleb128 0x25
	.long	.LASF206
	.byte	0x2
	.byte	0x1f
	.byte	0x43
	.long	0x81
	.uleb128 0x25
	.long	.LASF211
	.byte	0x2
	.byte	0x1f
	.byte	0x62
	.long	0x97a
	.uleb128 0x25
	.long	.LASF208
	.byte	0x2
	.byte	0x1f
	.byte	0x70
	.long	0x65
	.byte	0
	.uleb128 0x26
	.long	.LASF210
	.long	.LASF219
	.byte	0x13
	.byte	0
	.uleb128 0x27
	.long	.LASF212
	.long	.LASF212
	.byte	0x10
	.value	0x420
	.byte	0x11
	.uleb128 0x28
	.long	.LASF213
	.long	.LASF213
	.byte	0x3
	.byte	0x25
	.byte	0xd
	.uleb128 0x29
	.long	.LASF220
	.long	.LASF220
	.byte	0
	.section	.debug_abbrev,"",@progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1b
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x7
	.uleb128 0x10
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x37
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x13
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x13
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3c
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x21
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0x17
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0x4
	.byte	0x1
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x16
	.uleb128 0x28
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1c
	.uleb128 0xd
	.byte	0
	.byte	0
	.uleb128 0x17
	.uleb128 0x26
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x18
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x7
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2117
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x19
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x17
	.uleb128 0x2137
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x1a
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x17
	.uleb128 0x2137
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x1b
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x1c
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x17
	.uleb128 0x2137
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x1d
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x1e
	.uleb128 0x1d
	.byte	0x1
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x2138
	.uleb128 0xb
	.uleb128 0x55
	.uleb128 0x17
	.uleb128 0x58
	.uleb128 0xb
	.uleb128 0x59
	.uleb128 0xb
	.uleb128 0x57
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1f
	.uleb128 0x5
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x17
	.uleb128 0x2137
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x20
	.uleb128 0x4109
	.byte	0x1
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x31
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x21
	.uleb128 0x410a
	.byte	0
	.uleb128 0x2
	.uleb128 0x18
	.uleb128 0x2111
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x22
	.uleb128 0x4109
	.byte	0x1
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x23
	.uleb128 0x4109
	.byte	0
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x31
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x24
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x20
	.uleb128 0xb
	.uleb128 0x34
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x25
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x26
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.uleb128 0x6e
	.uleb128 0xe
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x27
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.uleb128 0x6e
	.uleb128 0xe
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x28
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.uleb128 0x6e
	.uleb128 0xe
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x29
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.uleb128 0x6e
	.uleb128 0xe
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",@progbits
.Ldebug_loc0:
.LVUS0:
	.uleb128 0
	.uleb128 .LVU16
	.uleb128 .LVU16
	.uleb128 0
.LLST0:
	.quad	.LVL0-.Ltext0
	.quad	.LVL2-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	.LVL2-.Ltext0
	.quad	.LFE94-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS1:
	.uleb128 0
	.uleb128 .LVU16
	.uleb128 .LVU16
	.uleb128 0
.LLST1:
	.quad	.LVL0-.Ltext0
	.quad	.LVL2-.Ltext0
	.value	0x1
	.byte	0x54
	.quad	.LVL2-.Ltext0
	.quad	.LFE94-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x54
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS2:
	.uleb128 .LVU12
	.uleb128 .LVU16
	.uleb128 .LVU16
	.uleb128 .LVU58
	.uleb128 .LVU62
	.uleb128 .LVU71
	.uleb128 .LVU73
	.uleb128 .LVU77
.LLST2:
	.quad	.LVL1-.Ltext0
	.quad	.LVL2-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	.LVL2-.Ltext0
	.quad	.LVL6-.Ltext0
	.value	0x1
	.byte	0x5c
	.quad	.LVL8-.Ltext0
	.quad	.LVL11-.Ltext0
	.value	0x1
	.byte	0x5c
	.quad	.LVL12-.Ltext0
	.quad	.LVL14-.Ltext0
	.value	0x1
	.byte	0x5c
	.quad	0
	.quad	0
.LVUS3:
	.uleb128 .LVU13
	.uleb128 .LVU61
	.uleb128 .LVU61
	.uleb128 .LVU62
	.uleb128 .LVU62
	.uleb128 0
.LLST3:
	.quad	.LVL1-.Ltext0
	.quad	.LVL7-.Ltext0
	.value	0x1
	.byte	0x5d
	.quad	.LVL7-.Ltext0
	.quad	.LVL8-.Ltext0
	.value	0x8
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.byte	0xf3
	.uleb128 0x1
	.byte	0x54
	.byte	0x22
	.byte	0x9f
	.quad	.LVL8-.Ltext0
	.quad	.LFE94-.Ltext0
	.value	0x1
	.byte	0x5d
	.quad	0
	.quad	0
.LVUS4:
	.uleb128 .LVU17
	.uleb128 .LVU20
.LLST4:
	.quad	.LVL2-.Ltext0
	.quad	.LVL3-.Ltext0
	.value	0x3
	.byte	0x8
	.byte	0x50
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS5:
	.uleb128 .LVU17
	.uleb128 .LVU20
.LLST5:
	.quad	.LVL2-.Ltext0
	.quad	.LVL3-.Ltext0
	.value	0x2
	.byte	0x30
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS6:
	.uleb128 .LVU17
	.uleb128 .LVU20
.LLST6:
	.quad	.LVL2-.Ltext0
	.quad	.LVL3-.Ltext0
	.value	0x1
	.byte	0x5f
	.quad	0
	.quad	0
.LVUS7:
	.uleb128 .LVU53
	.uleb128 .LVU56
	.uleb128 .LVU65
	.uleb128 .LVU68
.LLST7:
	.quad	.LVL5-.Ltext0
	.quad	.LVL6-1-.Ltext0
	.value	0x3
	.byte	0x76
	.sleb128 -168
	.quad	.LVL9-.Ltext0
	.quad	.LVL10-.Ltext0
	.value	0x3
	.byte	0x76
	.sleb128 -168
	.quad	0
	.quad	0
.LVUS8:
	.uleb128 .LVU53
	.uleb128 .LVU56
	.uleb128 .LVU65
	.uleb128 .LVU69
.LLST8:
	.quad	.LVL5-.Ltext0
	.quad	.LVL6-.Ltext0
	.value	0x1
	.byte	0x5e
	.quad	.LVL9-.Ltext0
	.quad	.LVL11-.Ltext0
	.value	0x1
	.byte	0x5e
	.quad	0
	.quad	0
.LVUS9:
	.uleb128 .LVU53
	.uleb128 .LVU56
	.uleb128 .LVU65
	.uleb128 .LVU69
.LLST9:
	.quad	.LVL5-.Ltext0
	.quad	.LVL6-.Ltext0
	.value	0x1
	.byte	0x5c
	.quad	.LVL9-.Ltext0
	.quad	.LVL11-.Ltext0
	.value	0x1
	.byte	0x5c
	.quad	0
	.quad	0
	.section	.debug_aranges,"",@progbits
	.long	0x2c
	.value	0x2
	.long	.Ldebug_info0
	.byte	0x8
	.byte	0
	.value	0
	.value	0
	.quad	.Ltext0
	.quad	.Letext0-.Ltext0
	.quad	0
	.quad	0
	.section	.debug_ranges,"",@progbits
.Ldebug_ranges0:
	.quad	.LBB6-.Ltext0
	.quad	.LBE6-.Ltext0
	.quad	.LBB11-.Ltext0
	.quad	.LBE11-.Ltext0
	.quad	.LBB12-.Ltext0
	.quad	.LBE12-.Ltext0
	.quad	.LBB13-.Ltext0
	.quad	.LBE13-.Ltext0
	.quad	0
	.quad	0
	.quad	.LBB14-.Ltext0
	.quad	.LBE14-.Ltext0
	.quad	.LBB19-.Ltext0
	.quad	.LBE19-.Ltext0
	.quad	.LBB20-.Ltext0
	.quad	.LBE20-.Ltext0
	.quad	.LBB21-.Ltext0
	.quad	.LBE21-.Ltext0
	.quad	0
	.quad	0
	.section	.debug_line,"",@progbits
.Ldebug_line0:
	.section	.debug_str,"MS",@progbits,1
.LASF115:
	.string	"UV_EAI_BADFLAGS"
.LASF14:
	.string	"__off_t"
.LASF17:
	.string	"_IO_read_ptr"
.LASF29:
	.string	"_chain"
.LASF141:
	.string	"UV_EINVAL"
.LASF79:
	.string	"sin6_addr"
.LASF117:
	.string	"UV_EAI_CANCELED"
.LASF95:
	.string	"__in6_u"
.LASF9:
	.string	"size_t"
.LASF111:
	.string	"UV_EAFNOSUPPORT"
.LASF35:
	.string	"_shortbuf"
.LASF10:
	.string	"__uint8_t"
.LASF148:
	.string	"UV_ENAMETOOLONG"
.LASF145:
	.string	"UV_ELOOP"
.LASF207:
	.string	"__ch"
.LASF23:
	.string	"_IO_buf_base"
.LASF132:
	.string	"UV_ECONNABORTED"
.LASF59:
	.string	"long long unsigned int"
.LASF168:
	.string	"UV_EPROTONOSUPPORT"
.LASF87:
	.string	"in_addr_t"
.LASF211:
	.string	"__src"
.LASF186:
	.string	"UV_EILSEQ"
.LASF200:
	.string	"newval"
.LASF58:
	.string	"uint32_t"
.LASF107:
	.string	"UV_E2BIG"
.LASF170:
	.string	"UV_ERANGE"
.LASF82:
	.string	"sockaddr_ipx"
.LASF38:
	.string	"_codecvt"
.LASF150:
	.string	"UV_ENETUNREACH"
.LASF102:
	.string	"__timezone"
.LASF60:
	.string	"long long int"
.LASF8:
	.string	"signed char"
.LASF81:
	.string	"sockaddr_inarp"
.LASF215:
	.string	"../deps/uv/src/unix/random-sysctl-linux.c"
.LASF174:
	.string	"UV_ESRCH"
.LASF214:
	.string	"GNU C89 9.3.0 -m64 -mtune=generic -march=x86-64 -g -O3 -std=gnu90 -fvisibility=hidden -fno-omit-frame-pointer -fasynchronous-unwind-tables -fstack-protector-strong -fstack-clash-protection -fcf-protection"
.LASF166:
	.string	"UV_EPIPE"
.LASF66:
	.string	"sockaddr_at"
.LASF30:
	.string	"_fileno"
.LASF179:
	.string	"UV_EOF"
.LASF18:
	.string	"_IO_read_end"
.LASF123:
	.string	"UV_EAI_OVERFLOW"
.LASF173:
	.string	"UV_ESPIPE"
.LASF92:
	.string	"__u6_addr16"
.LASF99:
	.string	"sys_siglist"
.LASF3:
	.string	"long int"
.LASF93:
	.string	"__u6_addr32"
.LASF16:
	.string	"_flags"
.LASF182:
	.string	"UV_EHOSTDOWN"
.LASF24:
	.string	"_IO_buf_end"
.LASF210:
	.string	"memcpy"
.LASF1:
	.string	"program_invocation_short_name"
.LASF77:
	.string	"sin6_port"
.LASF47:
	.string	"_IO_codecvt"
.LASF171:
	.string	"UV_EROFS"
.LASF128:
	.string	"UV_EBADF"
.LASF57:
	.string	"uint16_t"
.LASF55:
	.string	"_sys_errlist"
.LASF140:
	.string	"UV_EINTR"
.LASF0:
	.string	"program_invocation_name"
.LASF32:
	.string	"_old_offset"
.LASF37:
	.string	"_offset"
.LASF97:
	.string	"in6addr_loopback"
.LASF113:
	.string	"UV_EAI_ADDRFAMILY"
.LASF86:
	.string	"sockaddr_x25"
.LASF138:
	.string	"UV_EFBIG"
.LASF13:
	.string	"__uint32_t"
.LASF98:
	.string	"_sys_siglist"
.LASF105:
	.string	"timezone"
.LASF152:
	.string	"UV_ENOBUFS"
.LASF74:
	.string	"sin_zero"
.LASF120:
	.string	"UV_EAI_MEMORY"
.LASF178:
	.string	"UV_UNKNOWN"
.LASF208:
	.string	"__len"
.LASF49:
	.string	"stdin"
.LASF5:
	.string	"unsigned int"
.LASF89:
	.string	"s_addr"
.LASF41:
	.string	"_freeres_buf"
.LASF175:
	.string	"UV_ETIMEDOUT"
.LASF163:
	.string	"UV_ENOTSOCK"
.LASF197:
	.string	"nlen"
.LASF4:
	.string	"long unsigned int"
.LASF130:
	.string	"UV_ECANCELED"
.LASF21:
	.string	"_IO_write_ptr"
.LASF196:
	.string	"name"
.LASF158:
	.string	"UV_ENOSPC"
.LASF52:
	.string	"sys_nerr"
.LASF181:
	.string	"UV_EMLINK"
.LASF131:
	.string	"UV_ECHARSET"
.LASF7:
	.string	"short unsigned int"
.LASF73:
	.string	"sin_addr"
.LASF112:
	.string	"UV_EAGAIN"
.LASF216:
	.string	"/workspace/nodejsbuild/node-v12.18.4/out"
.LASF25:
	.string	"_IO_save_base"
.LASF190:
	.string	"environ"
.LASF180:
	.string	"UV_ENXIO"
.LASF177:
	.string	"UV_EXDEV"
.LASF219:
	.string	"__builtin_memcpy"
.LASF36:
	.string	"_lock"
.LASF153:
	.string	"UV_ENODEV"
.LASF201:
	.string	"newlen"
.LASF90:
	.string	"in_port_t"
.LASF169:
	.string	"UV_EPROTOTYPE"
.LASF50:
	.string	"stdout"
.LASF218:
	.string	"uv__random_sysctl"
.LASF176:
	.string	"UV_ETXTBSY"
.LASF85:
	.string	"sockaddr_un"
.LASF188:
	.string	"double"
.LASF154:
	.string	"UV_ENOENT"
.LASF71:
	.string	"sin_family"
.LASF121:
	.string	"UV_EAI_NODATA"
.LASF187:
	.string	"UV_ERRNO_MAX"
.LASF142:
	.string	"UV_EIO"
.LASF202:
	.string	"unused"
.LASF191:
	.string	"optarg"
.LASF149:
	.string	"UV_ENETDOWN"
.LASF106:
	.string	"getdate_err"
.LASF76:
	.string	"sin6_family"
.LASF192:
	.string	"optind"
.LASF22:
	.string	"_IO_write_end"
.LASF167:
	.string	"UV_EPROTO"
.LASF147:
	.string	"UV_EMSGSIZE"
.LASF206:
	.string	"__dest"
.LASF160:
	.string	"UV_ENOTCONN"
.LASF159:
	.string	"UV_ENOSYS"
.LASF84:
	.string	"sockaddr_ns"
.LASF88:
	.string	"in_addr"
.LASF62:
	.string	"_IO_FILE"
.LASF198:
	.string	"oldval"
.LASF144:
	.string	"UV_EISDIR"
.LASF189:
	.string	"__environ"
.LASF43:
	.string	"_mode"
.LASF46:
	.string	"_IO_marker"
.LASF72:
	.string	"sin_port"
.LASF64:
	.string	"sa_family"
.LASF53:
	.string	"sys_errlist"
.LASF28:
	.string	"_markers"
.LASF143:
	.string	"UV_EISCONN"
.LASF151:
	.string	"UV_ENFILE"
.LASF139:
	.string	"UV_EHOSTUNREACH"
.LASF129:
	.string	"UV_EBUSY"
.LASF126:
	.string	"UV_EAI_SOCKTYPE"
.LASF212:
	.string	"syscall"
.LASF80:
	.string	"sin6_scope_id"
.LASF109:
	.string	"UV_EADDRINUSE"
.LASF6:
	.string	"unsigned char"
.LASF83:
	.string	"sockaddr_iso"
.LASF127:
	.string	"UV_EALREADY"
.LASF96:
	.string	"in6addr_any"
.LASF11:
	.string	"short int"
.LASF48:
	.string	"_IO_wide_data"
.LASF31:
	.string	"_flags2"
.LASF118:
	.string	"UV_EAI_FAIL"
.LASF185:
	.string	"UV_EFTYPE"
.LASF54:
	.string	"_sys_nerr"
.LASF34:
	.string	"_vtable_offset"
.LASF110:
	.string	"UV_EADDRNOTAVAIL"
.LASF67:
	.string	"sockaddr_ax25"
.LASF45:
	.string	"FILE"
.LASF203:
	.string	"buflen"
.LASF220:
	.string	"__stack_chk_fail"
.LASF114:
	.string	"UV_EAI_AGAIN"
.LASF195:
	.string	"uv__sysctl_args"
.LASF94:
	.string	"in6_addr"
.LASF101:
	.string	"__daylight"
.LASF184:
	.string	"UV_ENOTTY"
.LASF194:
	.string	"optopt"
.LASF104:
	.string	"daylight"
.LASF137:
	.string	"UV_EFAULT"
.LASF135:
	.string	"UV_EDESTADDRREQ"
.LASF122:
	.string	"UV_EAI_NONAME"
.LASF205:
	.string	"uuid"
.LASF2:
	.string	"char"
.LASF134:
	.string	"UV_ECONNRESET"
.LASF78:
	.string	"sin6_flowinfo"
.LASF157:
	.string	"UV_ENOPROTOOPT"
.LASF12:
	.string	"__uint16_t"
.LASF161:
	.string	"UV_ENOTDIR"
.LASF91:
	.string	"__u6_addr8"
.LASF213:
	.string	"__errno_location"
.LASF193:
	.string	"opterr"
.LASF119:
	.string	"UV_EAI_FAMILY"
.LASF217:
	.string	"_IO_lock_t"
.LASF15:
	.string	"__off64_t"
.LASF33:
	.string	"_cur_column"
.LASF19:
	.string	"_IO_read_base"
.LASF124:
	.string	"UV_EAI_PROTOCOL"
.LASF27:
	.string	"_IO_save_end"
.LASF165:
	.string	"UV_EPERM"
.LASF172:
	.string	"UV_ESHUTDOWN"
.LASF156:
	.string	"UV_ENONET"
.LASF69:
	.string	"sockaddr_eon"
.LASF162:
	.string	"UV_ENOTEMPTY"
.LASF42:
	.string	"__pad5"
.LASF61:
	.string	"sa_family_t"
.LASF44:
	.string	"_unused2"
.LASF51:
	.string	"stderr"
.LASF209:
	.string	"memset"
.LASF75:
	.string	"sockaddr_in6"
.LASF63:
	.string	"sockaddr"
.LASF70:
	.string	"sockaddr_in"
.LASF56:
	.string	"uint8_t"
.LASF125:
	.string	"UV_EAI_SERVICE"
.LASF26:
	.string	"_IO_backup_base"
.LASF199:
	.string	"oldlenp"
.LASF164:
	.string	"UV_ENOTSUP"
.LASF136:
	.string	"UV_EEXIST"
.LASF68:
	.string	"sockaddr_dl"
.LASF183:
	.string	"UV_EREMOTEIO"
.LASF65:
	.string	"sa_data"
.LASF40:
	.string	"_freeres_list"
.LASF39:
	.string	"_wide_data"
.LASF116:
	.string	"UV_EAI_BADHINTS"
.LASF108:
	.string	"UV_EACCES"
.LASF204:
	.string	"args"
.LASF100:
	.string	"__tzname"
.LASF20:
	.string	"_IO_write_base"
.LASF103:
	.string	"tzname"
.LASF146:
	.string	"UV_EMFILE"
.LASF133:
	.string	"UV_ECONNREFUSED"
.LASF155:
	.string	"UV_ENOMEM"
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
