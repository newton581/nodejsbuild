	.file	"sysinfo-loadavg.c"
	.text
.Ltext0:
	.p2align 4
	.globl	uv_loadavg
	.type	uv_loadavg, @function
uv_loadavg:
.LVL0:
.LFB81:
	.file 1 "../deps/uv/src/unix/sysinfo-loadavg.c"
	.loc 1 28 32 view -0
	.cfi_startproc
	.loc 1 28 32 is_stmt 0 view .LVU1
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	.loc 1 31 7 view .LVU2
	leaq	-144(%rbp), %rdi
.LVL1:
	.loc 1 28 32 view .LVU3
	subq	$136, %rsp
	.loc 1 28 32 view .LVU4
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	.loc 1 29 3 is_stmt 1 view .LVU5
	.loc 1 31 3 view .LVU6
	.loc 1 31 7 is_stmt 0 view .LVU7
	call	sysinfo@PLT
.LVL2:
	.loc 1 31 6 view .LVU8
	testl	%eax, %eax
	js	.L1
	.loc 1 33 3 is_stmt 1 view .LVU9
	.loc 1 33 12 is_stmt 0 view .LVU10
	movq	-136(%rbp), %rax
	testq	%rax, %rax
	js	.L4
	pxor	%xmm1, %xmm1
	cvtsi2sdq	%rax, %xmm1
.L5:
	.loc 1 33 35 view .LVU11
	movsd	.LC0(%rip), %xmm2
	.loc 1 34 12 view .LVU12
	movq	-128(%rbp), %rax
	.loc 1 33 35 view .LVU13
	mulsd	%xmm2, %xmm1
	.loc 1 33 10 view .LVU14
	movsd	%xmm1, (%rbx)
	.loc 1 34 3 is_stmt 1 view .LVU15
	.loc 1 34 12 is_stmt 0 view .LVU16
	testq	%rax, %rax
	js	.L6
	pxor	%xmm1, %xmm1
	cvtsi2sdq	%rax, %xmm1
.L7:
	.loc 1 34 35 view .LVU17
	mulsd	%xmm2, %xmm1
	.loc 1 35 12 view .LVU18
	movq	-120(%rbp), %rax
	.loc 1 34 10 view .LVU19
	movsd	%xmm1, 8(%rbx)
	.loc 1 35 3 is_stmt 1 view .LVU20
	.loc 1 35 12 is_stmt 0 view .LVU21
	testq	%rax, %rax
	js	.L8
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
.L9:
	.loc 1 35 35 view .LVU22
	mulsd	%xmm2, %xmm0
	.loc 1 35 10 view .LVU23
	movsd	%xmm0, 16(%rbx)
.L1:
	.loc 1 36 1 view .LVU24
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L13
	addq	$136, %rsp
	popq	%rbx
.LVL3:
	.loc 1 36 1 view .LVU25
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.LVL4:
	.p2align 4,,10
	.p2align 3
.L4:
	.cfi_restore_state
	.loc 1 33 12 view .LVU26
	movq	%rax, %rdx
	andl	$1, %eax
	pxor	%xmm1, %xmm1
	shrq	%rdx
	orq	%rax, %rdx
	cvtsi2sdq	%rdx, %xmm1
	addsd	%xmm1, %xmm1
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L8:
	.loc 1 35 12 view .LVU27
	movq	%rax, %rdx
	andl	$1, %eax
	pxor	%xmm0, %xmm0
	shrq	%rdx
	orq	%rax, %rdx
	cvtsi2sdq	%rdx, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L6:
	.loc 1 34 12 view .LVU28
	movq	%rax, %rdx
	andl	$1, %eax
	pxor	%xmm1, %xmm1
	shrq	%rdx
	orq	%rax, %rdx
	cvtsi2sdq	%rdx, %xmm1
	addsd	%xmm1, %xmm1
	jmp	.L7
.L13:
	.loc 1 36 1 view .LVU29
	call	__stack_chk_fail@PLT
.LVL5:
	.cfi_endproc
.LFE81:
	.size	uv_loadavg, .-uv_loadavg
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC0:
	.long	0
	.long	1055916032
	.text
.Letext0:
	.file 2 "/usr/include/errno.h"
	.file 3 "/usr/lib/gcc/x86_64-linux-gnu/9/include/stddef.h"
	.file 4 "/usr/include/x86_64-linux-gnu/bits/types.h"
	.file 5 "/usr/include/x86_64-linux-gnu/bits/types/struct_FILE.h"
	.file 6 "/usr/include/x86_64-linux-gnu/bits/types/FILE.h"
	.file 7 "/usr/include/stdio.h"
	.file 8 "/usr/include/x86_64-linux-gnu/bits/sys_errlist.h"
	.file 9 "/usr/include/x86_64-linux-gnu/bits/stdint-uintn.h"
	.file 10 "/usr/include/asm-generic/int-ll64.h"
	.file 11 "/usr/include/asm-generic/posix_types.h"
	.file 12 "/usr/include/x86_64-linux-gnu/bits/sockaddr.h"
	.file 13 "/usr/include/x86_64-linux-gnu/bits/socket.h"
	.file 14 "/usr/include/netinet/in.h"
	.file 15 "/usr/include/signal.h"
	.file 16 "/usr/include/time.h"
	.file 17 "/usr/include/linux/sysinfo.h"
	.file 18 "/usr/include/x86_64-linux-gnu/sys/sysinfo.h"
	.section	.debug_info,"",@progbits
.Ldebug_info0:
	.long	0x8da
	.value	0x4
	.long	.Ldebug_abbrev0
	.byte	0x8
	.uleb128 0x1
	.long	.LASF125
	.byte	0x1
	.long	.LASF126
	.long	.LASF127
	.quad	.Ltext0
	.quad	.Letext0-.Ltext0
	.long	.Ldebug_line0
	.uleb128 0x2
	.long	.LASF0
	.byte	0x2
	.byte	0x2d
	.byte	0xe
	.long	0x39
	.uleb128 0x3
	.byte	0x8
	.long	0x3f
	.uleb128 0x4
	.byte	0x1
	.byte	0x6
	.long	.LASF2
	.uleb128 0x5
	.long	0x3f
	.uleb128 0x2
	.long	.LASF1
	.byte	0x2
	.byte	0x2e
	.byte	0xe
	.long	0x39
	.uleb128 0x6
	.byte	0x4
	.byte	0x5
	.string	"int"
	.uleb128 0x4
	.byte	0x8
	.byte	0x5
	.long	.LASF3
	.uleb128 0x7
	.long	.LASF9
	.byte	0x3
	.byte	0xd1
	.byte	0x1b
	.long	0x71
	.uleb128 0x4
	.byte	0x8
	.byte	0x7
	.long	.LASF4
	.uleb128 0x4
	.byte	0x4
	.byte	0x7
	.long	.LASF5
	.uleb128 0x8
	.byte	0x8
	.uleb128 0x4
	.byte	0x1
	.byte	0x8
	.long	.LASF6
	.uleb128 0x4
	.byte	0x2
	.byte	0x7
	.long	.LASF7
	.uleb128 0x4
	.byte	0x1
	.byte	0x6
	.long	.LASF8
	.uleb128 0x7
	.long	.LASF10
	.byte	0x4
	.byte	0x26
	.byte	0x17
	.long	0x81
	.uleb128 0x4
	.byte	0x2
	.byte	0x5
	.long	.LASF11
	.uleb128 0x7
	.long	.LASF12
	.byte	0x4
	.byte	0x28
	.byte	0x1c
	.long	0x88
	.uleb128 0x7
	.long	.LASF13
	.byte	0x4
	.byte	0x2a
	.byte	0x16
	.long	0x78
	.uleb128 0x7
	.long	.LASF14
	.byte	0x4
	.byte	0x98
	.byte	0x12
	.long	0x5e
	.uleb128 0x7
	.long	.LASF15
	.byte	0x4
	.byte	0x99
	.byte	0x12
	.long	0x5e
	.uleb128 0x9
	.long	.LASF66
	.byte	0xd8
	.byte	0x5
	.byte	0x31
	.byte	0x8
	.long	0x260
	.uleb128 0xa
	.long	.LASF16
	.byte	0x5
	.byte	0x33
	.byte	0x7
	.long	0x57
	.byte	0
	.uleb128 0xa
	.long	.LASF17
	.byte	0x5
	.byte	0x36
	.byte	0x9
	.long	0x39
	.byte	0x8
	.uleb128 0xa
	.long	.LASF18
	.byte	0x5
	.byte	0x37
	.byte	0x9
	.long	0x39
	.byte	0x10
	.uleb128 0xa
	.long	.LASF19
	.byte	0x5
	.byte	0x38
	.byte	0x9
	.long	0x39
	.byte	0x18
	.uleb128 0xa
	.long	.LASF20
	.byte	0x5
	.byte	0x39
	.byte	0x9
	.long	0x39
	.byte	0x20
	.uleb128 0xa
	.long	.LASF21
	.byte	0x5
	.byte	0x3a
	.byte	0x9
	.long	0x39
	.byte	0x28
	.uleb128 0xa
	.long	.LASF22
	.byte	0x5
	.byte	0x3b
	.byte	0x9
	.long	0x39
	.byte	0x30
	.uleb128 0xa
	.long	.LASF23
	.byte	0x5
	.byte	0x3c
	.byte	0x9
	.long	0x39
	.byte	0x38
	.uleb128 0xa
	.long	.LASF24
	.byte	0x5
	.byte	0x3d
	.byte	0x9
	.long	0x39
	.byte	0x40
	.uleb128 0xa
	.long	.LASF25
	.byte	0x5
	.byte	0x40
	.byte	0x9
	.long	0x39
	.byte	0x48
	.uleb128 0xa
	.long	.LASF26
	.byte	0x5
	.byte	0x41
	.byte	0x9
	.long	0x39
	.byte	0x50
	.uleb128 0xa
	.long	.LASF27
	.byte	0x5
	.byte	0x42
	.byte	0x9
	.long	0x39
	.byte	0x58
	.uleb128 0xa
	.long	.LASF28
	.byte	0x5
	.byte	0x44
	.byte	0x16
	.long	0x279
	.byte	0x60
	.uleb128 0xa
	.long	.LASF29
	.byte	0x5
	.byte	0x46
	.byte	0x14
	.long	0x27f
	.byte	0x68
	.uleb128 0xa
	.long	.LASF30
	.byte	0x5
	.byte	0x48
	.byte	0x7
	.long	0x57
	.byte	0x70
	.uleb128 0xa
	.long	.LASF31
	.byte	0x5
	.byte	0x49
	.byte	0x7
	.long	0x57
	.byte	0x74
	.uleb128 0xa
	.long	.LASF32
	.byte	0x5
	.byte	0x4a
	.byte	0xb
	.long	0xc1
	.byte	0x78
	.uleb128 0xa
	.long	.LASF33
	.byte	0x5
	.byte	0x4d
	.byte	0x12
	.long	0x88
	.byte	0x80
	.uleb128 0xa
	.long	.LASF34
	.byte	0x5
	.byte	0x4e
	.byte	0xf
	.long	0x8f
	.byte	0x82
	.uleb128 0xa
	.long	.LASF35
	.byte	0x5
	.byte	0x4f
	.byte	0x8
	.long	0x285
	.byte	0x83
	.uleb128 0xa
	.long	.LASF36
	.byte	0x5
	.byte	0x51
	.byte	0xf
	.long	0x295
	.byte	0x88
	.uleb128 0xa
	.long	.LASF37
	.byte	0x5
	.byte	0x59
	.byte	0xd
	.long	0xcd
	.byte	0x90
	.uleb128 0xa
	.long	.LASF38
	.byte	0x5
	.byte	0x5b
	.byte	0x17
	.long	0x2a0
	.byte	0x98
	.uleb128 0xa
	.long	.LASF39
	.byte	0x5
	.byte	0x5c
	.byte	0x19
	.long	0x2ab
	.byte	0xa0
	.uleb128 0xa
	.long	.LASF40
	.byte	0x5
	.byte	0x5d
	.byte	0x14
	.long	0x27f
	.byte	0xa8
	.uleb128 0xa
	.long	.LASF41
	.byte	0x5
	.byte	0x5e
	.byte	0x9
	.long	0x7f
	.byte	0xb0
	.uleb128 0xa
	.long	.LASF42
	.byte	0x5
	.byte	0x5f
	.byte	0xa
	.long	0x65
	.byte	0xb8
	.uleb128 0xa
	.long	.LASF43
	.byte	0x5
	.byte	0x60
	.byte	0x7
	.long	0x57
	.byte	0xc0
	.uleb128 0xa
	.long	.LASF44
	.byte	0x5
	.byte	0x62
	.byte	0x8
	.long	0x2b1
	.byte	0xc4
	.byte	0
	.uleb128 0x7
	.long	.LASF45
	.byte	0x6
	.byte	0x7
	.byte	0x19
	.long	0xd9
	.uleb128 0xb
	.long	.LASF128
	.byte	0x5
	.byte	0x2b
	.byte	0xe
	.uleb128 0xc
	.long	.LASF46
	.uleb128 0x3
	.byte	0x8
	.long	0x274
	.uleb128 0x3
	.byte	0x8
	.long	0xd9
	.uleb128 0xd
	.long	0x3f
	.long	0x295
	.uleb128 0xe
	.long	0x71
	.byte	0
	.byte	0
	.uleb128 0x3
	.byte	0x8
	.long	0x26c
	.uleb128 0xc
	.long	.LASF47
	.uleb128 0x3
	.byte	0x8
	.long	0x29b
	.uleb128 0xc
	.long	.LASF48
	.uleb128 0x3
	.byte	0x8
	.long	0x2a6
	.uleb128 0xd
	.long	0x3f
	.long	0x2c1
	.uleb128 0xe
	.long	0x71
	.byte	0x13
	.byte	0
	.uleb128 0x3
	.byte	0x8
	.long	0x46
	.uleb128 0x5
	.long	0x2c1
	.uleb128 0x2
	.long	.LASF49
	.byte	0x7
	.byte	0x89
	.byte	0xe
	.long	0x2d8
	.uleb128 0x3
	.byte	0x8
	.long	0x260
	.uleb128 0x2
	.long	.LASF50
	.byte	0x7
	.byte	0x8a
	.byte	0xe
	.long	0x2d8
	.uleb128 0x2
	.long	.LASF51
	.byte	0x7
	.byte	0x8b
	.byte	0xe
	.long	0x2d8
	.uleb128 0x2
	.long	.LASF52
	.byte	0x8
	.byte	0x1a
	.byte	0xc
	.long	0x57
	.uleb128 0xd
	.long	0x2c7
	.long	0x30d
	.uleb128 0xf
	.byte	0
	.uleb128 0x5
	.long	0x302
	.uleb128 0x2
	.long	.LASF53
	.byte	0x8
	.byte	0x1b
	.byte	0x1a
	.long	0x30d
	.uleb128 0x2
	.long	.LASF54
	.byte	0x8
	.byte	0x1e
	.byte	0xc
	.long	0x57
	.uleb128 0x2
	.long	.LASF55
	.byte	0x8
	.byte	0x1f
	.byte	0x1a
	.long	0x30d
	.uleb128 0x7
	.long	.LASF56
	.byte	0x9
	.byte	0x18
	.byte	0x13
	.long	0x96
	.uleb128 0x7
	.long	.LASF57
	.byte	0x9
	.byte	0x19
	.byte	0x14
	.long	0xa9
	.uleb128 0x7
	.long	.LASF58
	.byte	0x9
	.byte	0x1a
	.byte	0x14
	.long	0xb5
	.uleb128 0x4
	.byte	0x8
	.byte	0x7
	.long	.LASF59
	.uleb128 0x4
	.byte	0x8
	.byte	0x5
	.long	.LASF60
	.uleb128 0x7
	.long	.LASF61
	.byte	0xa
	.byte	0x18
	.byte	0x18
	.long	0x88
	.uleb128 0x7
	.long	.LASF62
	.byte	0xa
	.byte	0x1b
	.byte	0x16
	.long	0x78
	.uleb128 0x7
	.long	.LASF63
	.byte	0xb
	.byte	0xf
	.byte	0xe
	.long	0x5e
	.uleb128 0x7
	.long	.LASF64
	.byte	0xb
	.byte	0x10
	.byte	0x17
	.long	0x71
	.uleb128 0x7
	.long	.LASF65
	.byte	0xc
	.byte	0x1c
	.byte	0x1c
	.long	0x88
	.uleb128 0x9
	.long	.LASF67
	.byte	0x10
	.byte	0xd
	.byte	0xb2
	.byte	0x8
	.long	0x3cc
	.uleb128 0xa
	.long	.LASF68
	.byte	0xd
	.byte	0xb4
	.byte	0x11
	.long	0x398
	.byte	0
	.uleb128 0xa
	.long	.LASF69
	.byte	0xd
	.byte	0xb5
	.byte	0xa
	.long	0x3d1
	.byte	0x2
	.byte	0
	.uleb128 0x5
	.long	0x3a4
	.uleb128 0xd
	.long	0x3f
	.long	0x3e1
	.uleb128 0xe
	.long	0x71
	.byte	0xd
	.byte	0
	.uleb128 0x3
	.byte	0x8
	.long	0x3a4
	.uleb128 0x10
	.long	0x3e1
	.uleb128 0xc
	.long	.LASF70
	.uleb128 0x5
	.long	0x3ec
	.uleb128 0x3
	.byte	0x8
	.long	0x3ec
	.uleb128 0x10
	.long	0x3f6
	.uleb128 0xc
	.long	.LASF71
	.uleb128 0x5
	.long	0x401
	.uleb128 0x3
	.byte	0x8
	.long	0x401
	.uleb128 0x10
	.long	0x40b
	.uleb128 0xc
	.long	.LASF72
	.uleb128 0x5
	.long	0x416
	.uleb128 0x3
	.byte	0x8
	.long	0x416
	.uleb128 0x10
	.long	0x420
	.uleb128 0xc
	.long	.LASF73
	.uleb128 0x5
	.long	0x42b
	.uleb128 0x3
	.byte	0x8
	.long	0x42b
	.uleb128 0x10
	.long	0x435
	.uleb128 0x9
	.long	.LASF74
	.byte	0x10
	.byte	0xe
	.byte	0xee
	.byte	0x8
	.long	0x482
	.uleb128 0xa
	.long	.LASF75
	.byte	0xe
	.byte	0xf0
	.byte	0x11
	.long	0x398
	.byte	0
	.uleb128 0xa
	.long	.LASF76
	.byte	0xe
	.byte	0xf1
	.byte	0xf
	.long	0x629
	.byte	0x2
	.uleb128 0xa
	.long	.LASF77
	.byte	0xe
	.byte	0xf2
	.byte	0x14
	.long	0x60e
	.byte	0x4
	.uleb128 0xa
	.long	.LASF78
	.byte	0xe
	.byte	0xf5
	.byte	0x13
	.long	0x6cb
	.byte	0x8
	.byte	0
	.uleb128 0x5
	.long	0x440
	.uleb128 0x3
	.byte	0x8
	.long	0x440
	.uleb128 0x10
	.long	0x487
	.uleb128 0x9
	.long	.LASF79
	.byte	0x1c
	.byte	0xe
	.byte	0xfd
	.byte	0x8
	.long	0x4e5
	.uleb128 0xa
	.long	.LASF80
	.byte	0xe
	.byte	0xff
	.byte	0x11
	.long	0x398
	.byte	0
	.uleb128 0x11
	.long	.LASF81
	.byte	0xe
	.value	0x100
	.byte	0xf
	.long	0x629
	.byte	0x2
	.uleb128 0x11
	.long	.LASF82
	.byte	0xe
	.value	0x101
	.byte	0xe
	.long	0x34e
	.byte	0x4
	.uleb128 0x11
	.long	.LASF83
	.byte	0xe
	.value	0x102
	.byte	0x15
	.long	0x693
	.byte	0x8
	.uleb128 0x11
	.long	.LASF84
	.byte	0xe
	.value	0x103
	.byte	0xe
	.long	0x34e
	.byte	0x18
	.byte	0
	.uleb128 0x5
	.long	0x492
	.uleb128 0x3
	.byte	0x8
	.long	0x492
	.uleb128 0x10
	.long	0x4ea
	.uleb128 0xc
	.long	.LASF85
	.uleb128 0x5
	.long	0x4f5
	.uleb128 0x3
	.byte	0x8
	.long	0x4f5
	.uleb128 0x10
	.long	0x4ff
	.uleb128 0xc
	.long	.LASF86
	.uleb128 0x5
	.long	0x50a
	.uleb128 0x3
	.byte	0x8
	.long	0x50a
	.uleb128 0x10
	.long	0x514
	.uleb128 0xc
	.long	.LASF87
	.uleb128 0x5
	.long	0x51f
	.uleb128 0x3
	.byte	0x8
	.long	0x51f
	.uleb128 0x10
	.long	0x529
	.uleb128 0xc
	.long	.LASF88
	.uleb128 0x5
	.long	0x534
	.uleb128 0x3
	.byte	0x8
	.long	0x534
	.uleb128 0x10
	.long	0x53e
	.uleb128 0xc
	.long	.LASF89
	.uleb128 0x5
	.long	0x549
	.uleb128 0x3
	.byte	0x8
	.long	0x549
	.uleb128 0x10
	.long	0x553
	.uleb128 0xc
	.long	.LASF90
	.uleb128 0x5
	.long	0x55e
	.uleb128 0x3
	.byte	0x8
	.long	0x55e
	.uleb128 0x10
	.long	0x568
	.uleb128 0x3
	.byte	0x8
	.long	0x3cc
	.uleb128 0x10
	.long	0x573
	.uleb128 0x3
	.byte	0x8
	.long	0x3f1
	.uleb128 0x10
	.long	0x57e
	.uleb128 0x3
	.byte	0x8
	.long	0x406
	.uleb128 0x10
	.long	0x589
	.uleb128 0x3
	.byte	0x8
	.long	0x41b
	.uleb128 0x10
	.long	0x594
	.uleb128 0x3
	.byte	0x8
	.long	0x430
	.uleb128 0x10
	.long	0x59f
	.uleb128 0x3
	.byte	0x8
	.long	0x482
	.uleb128 0x10
	.long	0x5aa
	.uleb128 0x3
	.byte	0x8
	.long	0x4e5
	.uleb128 0x10
	.long	0x5b5
	.uleb128 0x3
	.byte	0x8
	.long	0x4fa
	.uleb128 0x10
	.long	0x5c0
	.uleb128 0x3
	.byte	0x8
	.long	0x50f
	.uleb128 0x10
	.long	0x5cb
	.uleb128 0x3
	.byte	0x8
	.long	0x524
	.uleb128 0x10
	.long	0x5d6
	.uleb128 0x3
	.byte	0x8
	.long	0x539
	.uleb128 0x10
	.long	0x5e1
	.uleb128 0x3
	.byte	0x8
	.long	0x54e
	.uleb128 0x10
	.long	0x5ec
	.uleb128 0x3
	.byte	0x8
	.long	0x563
	.uleb128 0x10
	.long	0x5f7
	.uleb128 0x7
	.long	.LASF91
	.byte	0xe
	.byte	0x1e
	.byte	0x12
	.long	0x34e
	.uleb128 0x9
	.long	.LASF92
	.byte	0x4
	.byte	0xe
	.byte	0x1f
	.byte	0x8
	.long	0x629
	.uleb128 0xa
	.long	.LASF93
	.byte	0xe
	.byte	0x21
	.byte	0xf
	.long	0x602
	.byte	0
	.byte	0
	.uleb128 0x7
	.long	.LASF94
	.byte	0xe
	.byte	0x77
	.byte	0x12
	.long	0x342
	.uleb128 0x12
	.byte	0x10
	.byte	0xe
	.byte	0xd6
	.byte	0x5
	.long	0x663
	.uleb128 0x13
	.long	.LASF95
	.byte	0xe
	.byte	0xd8
	.byte	0xa
	.long	0x663
	.uleb128 0x13
	.long	.LASF96
	.byte	0xe
	.byte	0xd9
	.byte	0xb
	.long	0x673
	.uleb128 0x13
	.long	.LASF97
	.byte	0xe
	.byte	0xda
	.byte	0xb
	.long	0x683
	.byte	0
	.uleb128 0xd
	.long	0x336
	.long	0x673
	.uleb128 0xe
	.long	0x71
	.byte	0xf
	.byte	0
	.uleb128 0xd
	.long	0x342
	.long	0x683
	.uleb128 0xe
	.long	0x71
	.byte	0x7
	.byte	0
	.uleb128 0xd
	.long	0x34e
	.long	0x693
	.uleb128 0xe
	.long	0x71
	.byte	0x3
	.byte	0
	.uleb128 0x9
	.long	.LASF98
	.byte	0x10
	.byte	0xe
	.byte	0xd4
	.byte	0x8
	.long	0x6ae
	.uleb128 0xa
	.long	.LASF99
	.byte	0xe
	.byte	0xdb
	.byte	0x9
	.long	0x635
	.byte	0
	.byte	0
	.uleb128 0x5
	.long	0x693
	.uleb128 0x2
	.long	.LASF100
	.byte	0xe
	.byte	0xe4
	.byte	0x1e
	.long	0x6ae
	.uleb128 0x2
	.long	.LASF101
	.byte	0xe
	.byte	0xe5
	.byte	0x1e
	.long	0x6ae
	.uleb128 0xd
	.long	0x81
	.long	0x6db
	.uleb128 0xe
	.long	0x71
	.byte	0x7
	.byte	0
	.uleb128 0xd
	.long	0x2c7
	.long	0x6eb
	.uleb128 0xe
	.long	0x71
	.byte	0x40
	.byte	0
	.uleb128 0x5
	.long	0x6db
	.uleb128 0x14
	.long	.LASF102
	.byte	0xf
	.value	0x11e
	.byte	0x1a
	.long	0x6eb
	.uleb128 0x14
	.long	.LASF103
	.byte	0xf
	.value	0x11f
	.byte	0x1a
	.long	0x6eb
	.uleb128 0xd
	.long	0x39
	.long	0x71a
	.uleb128 0xe
	.long	0x71
	.byte	0x1
	.byte	0
	.uleb128 0x2
	.long	.LASF104
	.byte	0x10
	.byte	0x9f
	.byte	0xe
	.long	0x70a
	.uleb128 0x2
	.long	.LASF105
	.byte	0x10
	.byte	0xa0
	.byte	0xc
	.long	0x57
	.uleb128 0x2
	.long	.LASF106
	.byte	0x10
	.byte	0xa1
	.byte	0x11
	.long	0x5e
	.uleb128 0x2
	.long	.LASF107
	.byte	0x10
	.byte	0xa6
	.byte	0xe
	.long	0x70a
	.uleb128 0x2
	.long	.LASF108
	.byte	0x10
	.byte	0xae
	.byte	0xc
	.long	0x57
	.uleb128 0x2
	.long	.LASF109
	.byte	0x10
	.byte	0xaf
	.byte	0x11
	.long	0x5e
	.uleb128 0x14
	.long	.LASF110
	.byte	0x10
	.value	0x112
	.byte	0xc
	.long	0x57
	.uleb128 0x4
	.byte	0x8
	.byte	0x4
	.long	.LASF111
	.uleb128 0x9
	.long	.LASF112
	.byte	0x70
	.byte	0x11
	.byte	0x8
	.byte	0x8
	.long	0x839
	.uleb128 0xa
	.long	.LASF113
	.byte	0x11
	.byte	0x9
	.byte	0x12
	.long	0x380
	.byte	0
	.uleb128 0xa
	.long	.LASF114
	.byte	0x11
	.byte	0xa
	.byte	0x13
	.long	0x839
	.byte	0x8
	.uleb128 0xa
	.long	.LASF115
	.byte	0x11
	.byte	0xb
	.byte	0x13
	.long	0x38c
	.byte	0x20
	.uleb128 0xa
	.long	.LASF116
	.byte	0x11
	.byte	0xc
	.byte	0x13
	.long	0x38c
	.byte	0x28
	.uleb128 0xa
	.long	.LASF117
	.byte	0x11
	.byte	0xd
	.byte	0x13
	.long	0x38c
	.byte	0x30
	.uleb128 0xa
	.long	.LASF118
	.byte	0x11
	.byte	0xe
	.byte	0x13
	.long	0x38c
	.byte	0x38
	.uleb128 0xa
	.long	.LASF119
	.byte	0x11
	.byte	0xf
	.byte	0x13
	.long	0x38c
	.byte	0x40
	.uleb128 0xa
	.long	.LASF120
	.byte	0x11
	.byte	0x10
	.byte	0x13
	.long	0x38c
	.byte	0x48
	.uleb128 0xa
	.long	.LASF121
	.byte	0x11
	.byte	0x11
	.byte	0x8
	.long	0x368
	.byte	0x50
	.uleb128 0x15
	.string	"pad"
	.byte	0x11
	.byte	0x12
	.byte	0x8
	.long	0x368
	.byte	0x52
	.uleb128 0xa
	.long	.LASF122
	.byte	0x11
	.byte	0x13
	.byte	0x13
	.long	0x38c
	.byte	0x58
	.uleb128 0xa
	.long	.LASF123
	.byte	0x11
	.byte	0x14
	.byte	0x13
	.long	0x38c
	.byte	0x60
	.uleb128 0xa
	.long	.LASF124
	.byte	0x11
	.byte	0x15
	.byte	0x8
	.long	0x374
	.byte	0x68
	.uleb128 0x15
	.string	"_f"
	.byte	0x11
	.byte	0x16
	.byte	0x7
	.long	0x849
	.byte	0x6c
	.byte	0
	.uleb128 0xd
	.long	0x38c
	.long	0x849
	.uleb128 0xe
	.long	0x71
	.byte	0x2
	.byte	0
	.uleb128 0xd
	.long	0x3f
	.long	0x859
	.uleb128 0x16
	.long	0x71
	.byte	0
	.byte	0
	.uleb128 0x17
	.long	.LASF129
	.byte	0x1
	.byte	0x1c
	.byte	0x6
	.quad	.LFB81
	.quad	.LFE81-.LFB81
	.uleb128 0x1
	.byte	0x9c
	.long	0x8c2
	.uleb128 0x18
	.string	"avg"
	.byte	0x1
	.byte	0x1c
	.byte	0x18
	.long	0x8c2
	.long	.LLST0
	.long	.LVUS0
	.uleb128 0x19
	.long	.LASF130
	.byte	0x1
	.byte	0x1d
	.byte	0x12
	.long	0x776
	.uleb128 0x3
	.byte	0x91
	.sleb128 -160
	.uleb128 0x1a
	.quad	.LVL2
	.long	0x8c8
	.long	0x8b4
	.uleb128 0x1b
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x3
	.byte	0x76
	.sleb128 -144
	.byte	0
	.uleb128 0x1c
	.quad	.LVL5
	.long	0x8d4
	.byte	0
	.uleb128 0x3
	.byte	0x8
	.long	0x76f
	.uleb128 0x1d
	.long	.LASF112
	.long	.LASF112
	.byte	0x12
	.byte	0x1d
	.byte	0xc
	.uleb128 0x1e
	.long	.LASF131
	.long	.LASF131
	.byte	0
	.section	.debug_abbrev,"",@progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1b
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x7
	.uleb128 0x10
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x13
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x13
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3c
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x21
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x37
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0x17
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x16
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x37
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x17
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x7
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2117
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x18
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x17
	.uleb128 0x2137
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x19
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x1a
	.uleb128 0x4109
	.byte	0x1
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1b
	.uleb128 0x410a
	.byte	0
	.uleb128 0x2
	.uleb128 0x18
	.uleb128 0x2111
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x1c
	.uleb128 0x4109
	.byte	0
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x31
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1d
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.uleb128 0x6e
	.uleb128 0xe
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x1e
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.uleb128 0x6e
	.uleb128 0xe
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",@progbits
.Ldebug_loc0:
.LVUS0:
	.uleb128 0
	.uleb128 .LVU3
	.uleb128 .LVU3
	.uleb128 .LVU25
	.uleb128 .LVU25
	.uleb128 .LVU26
	.uleb128 .LVU26
	.uleb128 0
.LLST0:
	.quad	.LVL0-.Ltext0
	.quad	.LVL1-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	.LVL1-.Ltext0
	.quad	.LVL3-.Ltext0
	.value	0x1
	.byte	0x53
	.quad	.LVL3-.Ltext0
	.quad	.LVL4-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.byte	0x9f
	.quad	.LVL4-.Ltext0
	.quad	.LFE81-.Ltext0
	.value	0x1
	.byte	0x53
	.quad	0
	.quad	0
	.section	.debug_aranges,"",@progbits
	.long	0x2c
	.value	0x2
	.long	.Ldebug_info0
	.byte	0x8
	.byte	0
	.value	0
	.value	0
	.quad	.Ltext0
	.quad	.Letext0-.Ltext0
	.quad	0
	.quad	0
	.section	.debug_line,"",@progbits
.Ldebug_line0:
	.section	.debug_str,"MS",@progbits,1
.LASF14:
	.string	"__off_t"
.LASF17:
	.string	"_IO_read_ptr"
.LASF29:
	.string	"_chain"
.LASF83:
	.string	"sin6_addr"
.LASF99:
	.string	"__in6_u"
.LASF9:
	.string	"size_t"
.LASF35:
	.string	"_shortbuf"
.LASF10:
	.string	"__uint8_t"
.LASF23:
	.string	"_IO_buf_base"
.LASF129:
	.string	"uv_loadavg"
.LASF59:
	.string	"long long unsigned int"
.LASF91:
	.string	"in_addr_t"
.LASF61:
	.string	"__u16"
.LASF123:
	.string	"freehigh"
.LASF38:
	.string	"_codecvt"
.LASF106:
	.string	"__timezone"
.LASF60:
	.string	"long long int"
.LASF8:
	.string	"signed char"
.LASF85:
	.string	"sockaddr_inarp"
.LASF125:
	.string	"GNU C89 9.3.0 -m64 -mtune=generic -march=x86-64 -g -O3 -std=gnu90 -fvisibility=hidden -fno-omit-frame-pointer -fasynchronous-unwind-tables -fstack-protector-strong -fstack-clash-protection -fcf-protection"
.LASF70:
	.string	"sockaddr_at"
.LASF30:
	.string	"_fileno"
.LASF126:
	.string	"../deps/uv/src/unix/sysinfo-loadavg.c"
.LASF118:
	.string	"bufferram"
.LASF128:
	.string	"_IO_lock_t"
.LASF96:
	.string	"__u6_addr16"
.LASF103:
	.string	"sys_siglist"
.LASF3:
	.string	"long int"
.LASF64:
	.string	"__kernel_ulong_t"
.LASF16:
	.string	"_flags"
.LASF24:
	.string	"_IO_buf_end"
.LASF49:
	.string	"stdin"
.LASF1:
	.string	"program_invocation_short_name"
.LASF47:
	.string	"_IO_codecvt"
.LASF81:
	.string	"sin6_port"
.LASF57:
	.string	"uint16_t"
.LASF55:
	.string	"_sys_errlist"
.LASF0:
	.string	"program_invocation_name"
.LASF32:
	.string	"_old_offset"
.LASF37:
	.string	"_offset"
.LASF101:
	.string	"in6addr_loopback"
.LASF90:
	.string	"sockaddr_x25"
.LASF86:
	.string	"sockaddr_ipx"
.LASF119:
	.string	"totalswap"
.LASF13:
	.string	"__uint32_t"
.LASF109:
	.string	"timezone"
.LASF78:
	.string	"sin_zero"
.LASF46:
	.string	"_IO_marker"
.LASF5:
	.string	"unsigned int"
.LASF93:
	.string	"s_addr"
.LASF41:
	.string	"_freeres_buf"
.LASF4:
	.string	"long unsigned int"
.LASF62:
	.string	"__u32"
.LASF21:
	.string	"_IO_write_ptr"
.LASF52:
	.string	"sys_nerr"
.LASF124:
	.string	"mem_unit"
.LASF7:
	.string	"short unsigned int"
.LASF77:
	.string	"sin_addr"
.LASF127:
	.string	"/workspace/nodejsbuild/node-v12.18.4/out"
.LASF25:
	.string	"_IO_save_base"
.LASF36:
	.string	"_lock"
.LASF97:
	.string	"__u6_addr32"
.LASF94:
	.string	"in_port_t"
.LASF50:
	.string	"stdout"
.LASF89:
	.string	"sockaddr_un"
.LASF121:
	.string	"procs"
.LASF75:
	.string	"sin_family"
.LASF18:
	.string	"_IO_read_end"
.LASF110:
	.string	"getdate_err"
.LASF80:
	.string	"sin6_family"
.LASF22:
	.string	"_IO_write_end"
.LASF88:
	.string	"sockaddr_ns"
.LASF100:
	.string	"in6addr_any"
.LASF66:
	.string	"_IO_FILE"
.LASF105:
	.string	"__daylight"
.LASF43:
	.string	"_mode"
.LASF76:
	.string	"sin_port"
.LASF68:
	.string	"sa_family"
.LASF53:
	.string	"sys_errlist"
.LASF120:
	.string	"freeswap"
.LASF28:
	.string	"_markers"
.LASF63:
	.string	"__kernel_long_t"
.LASF84:
	.string	"sin6_scope_id"
.LASF116:
	.string	"freeram"
.LASF6:
	.string	"unsigned char"
.LASF87:
	.string	"sockaddr_iso"
.LASF11:
	.string	"short int"
.LASF130:
	.string	"info"
.LASF48:
	.string	"_IO_wide_data"
.LASF31:
	.string	"_flags2"
.LASF54:
	.string	"_sys_nerr"
.LASF34:
	.string	"_vtable_offset"
.LASF107:
	.string	"tzname"
.LASF71:
	.string	"sockaddr_ax25"
.LASF45:
	.string	"FILE"
.LASF131:
	.string	"__stack_chk_fail"
.LASF98:
	.string	"in6_addr"
.LASF113:
	.string	"uptime"
.LASF108:
	.string	"daylight"
.LASF2:
	.string	"char"
.LASF82:
	.string	"sin6_flowinfo"
.LASF12:
	.string	"__uint16_t"
.LASF95:
	.string	"__u6_addr8"
.LASF122:
	.string	"totalhigh"
.LASF15:
	.string	"__off64_t"
.LASF33:
	.string	"_cur_column"
.LASF19:
	.string	"_IO_read_base"
.LASF27:
	.string	"_IO_save_end"
.LASF102:
	.string	"_sys_siglist"
.LASF112:
	.string	"sysinfo"
.LASF73:
	.string	"sockaddr_eon"
.LASF42:
	.string	"__pad5"
.LASF65:
	.string	"sa_family_t"
.LASF44:
	.string	"_unused2"
.LASF51:
	.string	"stderr"
.LASF79:
	.string	"sockaddr_in6"
.LASF67:
	.string	"sockaddr"
.LASF74:
	.string	"sockaddr_in"
.LASF117:
	.string	"sharedram"
.LASF56:
	.string	"uint8_t"
.LASF26:
	.string	"_IO_backup_base"
.LASF115:
	.string	"totalram"
.LASF72:
	.string	"sockaddr_dl"
.LASF69:
	.string	"sa_data"
.LASF40:
	.string	"_freeres_list"
.LASF39:
	.string	"_wide_data"
.LASF114:
	.string	"loads"
.LASF104:
	.string	"__tzname"
.LASF20:
	.string	"_IO_write_base"
.LASF111:
	.string	"double"
.LASF58:
	.string	"uint32_t"
.LASF92:
	.string	"in_addr"
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
