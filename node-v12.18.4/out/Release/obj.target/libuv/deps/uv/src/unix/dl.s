	.file	"dl.c"
	.text
.Ltext0:
	.p2align 4
	.globl	uv_dlopen
	.type	uv_dlopen, @function
uv_dlopen:
.LVL0:
.LFB81:
	.file 1 "../deps/uv/src/unix/dl.c"
	.loc 1 33 52 view -0
	.cfi_startproc
	.loc 1 33 52 is_stmt 0 view .LVU1
	endbr64
	.loc 1 34 3 is_stmt 1 view .LVU2
	.loc 1 33 52 is_stmt 0 view .LVU3
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	.loc 1 33 52 view .LVU4
	movq	%rsi, %rbx
	.loc 1 34 3 view .LVU5
	call	dlerror@PLT
.LVL1:
	.loc 1 35 3 is_stmt 1 view .LVU6
	.loc 1 35 15 is_stmt 0 view .LVU7
	movq	$0, 8(%rbx)
	.loc 1 36 3 is_stmt 1 view .LVU8
	.loc 1 36 17 is_stmt 0 view .LVU9
	movq	%r12, %rdi
	movl	$1, %esi
	.loc 1 37 26 view .LVU10
	xorl	%r12d, %r12d
.LVL2:
	.loc 1 36 17 view .LVU11
	call	dlopen@PLT
.LVL3:
	.loc 1 36 15 view .LVU12
	movq	%rax, (%rbx)
	.loc 1 37 3 is_stmt 1 view .LVU13
	.loc 1 37 26 is_stmt 0 view .LVU14
	testq	%rax, %rax
	je	.L7
.L1:
	.loc 1 38 1 view .LVU15
	movl	%r12d, %eax
	popq	%rbx
.LVL4:
	.loc 1 38 1 view .LVU16
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.LVL5:
	.p2align 4,,10
	.p2align 3
.L7:
	.cfi_restore_state
.LBB4:
.LBI4:
	.loc 1 65 12 is_stmt 1 discriminator 1 view .LVU17
.LBB5:
	.loc 1 66 3 discriminator 1 view .LVU18
	.loc 1 68 3 discriminator 1 view .LVU19
	movq	8(%rbx), %rdi
	call	uv__free@PLT
.LVL6:
	.loc 1 70 3 discriminator 1 view .LVU20
	.loc 1 70 12 is_stmt 0 discriminator 1 view .LVU21
	call	dlerror@PLT
.LVL7:
	.loc 1 72 3 is_stmt 1 discriminator 1 view .LVU22
	.loc 1 72 6 is_stmt 0 discriminator 1 view .LVU23
	testq	%rax, %rax
	jne	.L8
	.loc 1 77 5 is_stmt 1 view .LVU24
.LBE5:
.LBE4:
	.loc 1 38 1 is_stmt 0 view .LVU25
	movl	%r12d, %eax
.LVL8:
.LBB8:
.LBB6:
	.loc 1 77 17 view .LVU26
	movq	$0, 8(%rbx)
	.loc 1 78 5 is_stmt 1 view .LVU27
.LBE6:
.LBE8:
	.loc 1 38 1 is_stmt 0 view .LVU28
	popq	%rbx
.LVL9:
	.loc 1 38 1 view .LVU29
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.LVL10:
.L8:
	.cfi_restore_state
.LBB9:
.LBB7:
	.loc 1 73 5 is_stmt 1 view .LVU30
	.loc 1 73 19 is_stmt 0 view .LVU31
	movq	%rax, %rdi
	.loc 1 74 12 view .LVU32
	movl	$-1, %r12d
	.loc 1 73 19 view .LVU33
	call	uv__strdup@PLT
.LVL11:
	.loc 1 73 17 view .LVU34
	movq	%rax, 8(%rbx)
	.loc 1 74 5 is_stmt 1 view .LVU35
	.loc 1 74 12 is_stmt 0 view .LVU36
	jmp	.L1
.LBE7:
.LBE9:
	.cfi_endproc
.LFE81:
	.size	uv_dlopen, .-uv_dlopen
	.p2align 4
	.globl	uv_dlclose
	.type	uv_dlclose, @function
uv_dlclose:
.LVL12:
.LFB82:
	.loc 1 41 32 is_stmt 1 view -0
	.cfi_startproc
	.loc 1 41 32 is_stmt 0 view .LVU38
	endbr64
	.loc 1 42 3 is_stmt 1 view .LVU39
	.loc 1 41 32 is_stmt 0 view .LVU40
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	.loc 1 42 3 view .LVU41
	movq	8(%rdi), %rdi
.LVL13:
	.loc 1 42 3 view .LVU42
	call	uv__free@PLT
.LVL14:
	.loc 1 43 3 is_stmt 1 view .LVU43
	.loc 1 45 10 is_stmt 0 view .LVU44
	movq	(%rbx), %rdi
	.loc 1 43 15 view .LVU45
	movq	$0, 8(%rbx)
	.loc 1 45 3 is_stmt 1 view .LVU46
	.loc 1 45 6 is_stmt 0 view .LVU47
	testq	%rdi, %rdi
	je	.L9
	.loc 1 47 5 is_stmt 1 view .LVU48
	call	dlclose@PLT
.LVL15:
	.loc 1 48 5 view .LVU49
	.loc 1 48 17 is_stmt 0 view .LVU50
	movq	$0, (%rbx)
.L9:
	.loc 1 50 1 view .LVU51
	addq	$8, %rsp
	popq	%rbx
.LVL16:
	.loc 1 50 1 view .LVU52
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE82:
	.size	uv_dlclose, .-uv_dlclose
	.p2align 4
	.globl	uv_dlsym
	.type	uv_dlsym, @function
uv_dlsym:
.LVL17:
.LFB83:
	.loc 1 53 59 is_stmt 1 view -0
	.cfi_startproc
	.loc 1 53 59 is_stmt 0 view .LVU54
	endbr64
	.loc 1 54 3 is_stmt 1 view .LVU55
	.loc 1 53 59 is_stmt 0 view .LVU56
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	.loc 1 54 3 view .LVU57
	call	dlerror@PLT
.LVL18:
	.loc 1 55 3 is_stmt 1 view .LVU58
	.loc 1 55 10 is_stmt 0 view .LVU59
	movq	(%rbx), %rdi
	movq	%r13, %rsi
	call	dlsym@PLT
.LVL19:
	.loc 1 55 8 view .LVU60
	movq	%rax, (%r12)
	.loc 1 56 3 is_stmt 1 view .LVU61
.LBB12:
.LBI12:
	.loc 1 65 12 view .LVU62
.LVL20:
.LBB13:
	.loc 1 66 3 view .LVU63
	.loc 1 68 3 view .LVU64
	movq	8(%rbx), %rdi
	call	uv__free@PLT
.LVL21:
	.loc 1 70 3 view .LVU65
	.loc 1 70 12 is_stmt 0 view .LVU66
	call	dlerror@PLT
.LVL22:
	.loc 1 72 3 is_stmt 1 view .LVU67
	.loc 1 72 6 is_stmt 0 view .LVU68
	testq	%rax, %rax
	jne	.L19
	.loc 1 77 5 is_stmt 1 view .LVU69
	.loc 1 77 17 is_stmt 0 view .LVU70
	movq	$0, 8(%rbx)
	.loc 1 78 5 is_stmt 1 view .LVU71
	.loc 1 78 12 is_stmt 0 view .LVU72
	xorl	%eax, %eax
.LVL23:
.L15:
	.loc 1 78 12 view .LVU73
.LBE13:
.LBE12:
	.loc 1 57 1 view .LVU74
	addq	$8, %rsp
	popq	%rbx
.LVL24:
	.loc 1 57 1 view .LVU75
	popq	%r12
.LVL25:
	.loc 1 57 1 view .LVU76
	popq	%r13
.LVL26:
	.loc 1 57 1 view .LVU77
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.LVL27:
	.p2align 4,,10
	.p2align 3
.L19:
	.cfi_restore_state
.LBB15:
.LBB14:
	.loc 1 73 5 is_stmt 1 view .LVU78
	.loc 1 73 19 is_stmt 0 view .LVU79
	movq	%rax, %rdi
	call	uv__strdup@PLT
.LVL28:
	.loc 1 73 17 view .LVU80
	movq	%rax, 8(%rbx)
	.loc 1 74 5 is_stmt 1 view .LVU81
	.loc 1 74 12 is_stmt 0 view .LVU82
	movl	$-1, %eax
	jmp	.L15
.LBE14:
.LBE15:
	.cfi_endproc
.LFE83:
	.size	uv_dlsym, .-uv_dlsym
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"no error"
	.text
	.p2align 4
	.globl	uv_dlerror
	.type	uv_dlerror, @function
uv_dlerror:
.LVL29:
.LFB84:
	.loc 1 60 45 is_stmt 1 view -0
	.cfi_startproc
	.loc 1 60 45 is_stmt 0 view .LVU84
	endbr64
	.loc 1 61 3 is_stmt 1 view .LVU85
	.loc 1 61 13 is_stmt 0 view .LVU86
	movq	8(%rdi), %rax
	.loc 1 61 36 view .LVU87
	leaq	.LC0(%rip), %rdx
	testq	%rax, %rax
	cmove	%rdx, %rax
	.loc 1 62 1 view .LVU88
	ret
	.cfi_endproc
.LFE84:
	.size	uv_dlerror, .-uv_dlerror
.Letext0:
	.file 2 "/usr/include/errno.h"
	.file 3 "/usr/lib/gcc/x86_64-linux-gnu/9/include/stddef.h"
	.file 4 "/usr/include/x86_64-linux-gnu/bits/types.h"
	.file 5 "/usr/include/x86_64-linux-gnu/bits/types/struct_FILE.h"
	.file 6 "/usr/include/x86_64-linux-gnu/bits/types/FILE.h"
	.file 7 "/usr/include/stdio.h"
	.file 8 "/usr/include/x86_64-linux-gnu/bits/sys_errlist.h"
	.file 9 "/usr/include/x86_64-linux-gnu/bits/stdint-uintn.h"
	.file 10 "/usr/include/x86_64-linux-gnu/bits/sockaddr.h"
	.file 11 "/usr/include/x86_64-linux-gnu/bits/socket.h"
	.file 12 "/usr/include/netinet/in.h"
	.file 13 "/usr/include/signal.h"
	.file 14 "/usr/include/time.h"
	.file 15 "../deps/uv/include/uv/unix.h"
	.file 16 "../deps/uv/src/uv-common.h"
	.file 17 "/usr/include/dlfcn.h"
	.section	.debug_info,"",@progbits
.Ldebug_info0:
	.long	0xa30
	.value	0x4
	.long	.Ldebug_abbrev0
	.byte	0x8
	.uleb128 0x1
	.long	.LASF122
	.byte	0x1
	.long	.LASF123
	.long	.LASF124
	.quad	.Ltext0
	.quad	.Letext0-.Ltext0
	.long	.Ldebug_line0
	.uleb128 0x2
	.long	.LASF0
	.byte	0x2
	.byte	0x2d
	.byte	0xe
	.long	0x39
	.uleb128 0x3
	.byte	0x8
	.long	0x3f
	.uleb128 0x4
	.byte	0x1
	.byte	0x6
	.long	.LASF2
	.uleb128 0x5
	.long	0x3f
	.uleb128 0x2
	.long	.LASF1
	.byte	0x2
	.byte	0x2e
	.byte	0xe
	.long	0x39
	.uleb128 0x6
	.byte	0x4
	.byte	0x5
	.string	"int"
	.uleb128 0x4
	.byte	0x8
	.byte	0x5
	.long	.LASF3
	.uleb128 0x7
	.long	.LASF9
	.byte	0x3
	.byte	0xd1
	.byte	0x1b
	.long	0x71
	.uleb128 0x4
	.byte	0x8
	.byte	0x7
	.long	.LASF4
	.uleb128 0x4
	.byte	0x4
	.byte	0x7
	.long	.LASF5
	.uleb128 0x8
	.byte	0x8
	.uleb128 0x4
	.byte	0x1
	.byte	0x8
	.long	.LASF6
	.uleb128 0x4
	.byte	0x2
	.byte	0x7
	.long	.LASF7
	.uleb128 0x4
	.byte	0x1
	.byte	0x6
	.long	.LASF8
	.uleb128 0x7
	.long	.LASF10
	.byte	0x4
	.byte	0x26
	.byte	0x17
	.long	0x81
	.uleb128 0x4
	.byte	0x2
	.byte	0x5
	.long	.LASF11
	.uleb128 0x7
	.long	.LASF12
	.byte	0x4
	.byte	0x28
	.byte	0x1c
	.long	0x88
	.uleb128 0x7
	.long	.LASF13
	.byte	0x4
	.byte	0x2a
	.byte	0x16
	.long	0x78
	.uleb128 0x7
	.long	.LASF14
	.byte	0x4
	.byte	0x98
	.byte	0x12
	.long	0x5e
	.uleb128 0x7
	.long	.LASF15
	.byte	0x4
	.byte	0x99
	.byte	0x12
	.long	0x5e
	.uleb128 0x9
	.long	.LASF62
	.byte	0xd8
	.byte	0x5
	.byte	0x31
	.byte	0x8
	.long	0x260
	.uleb128 0xa
	.long	.LASF16
	.byte	0x5
	.byte	0x33
	.byte	0x7
	.long	0x57
	.byte	0
	.uleb128 0xa
	.long	.LASF17
	.byte	0x5
	.byte	0x36
	.byte	0x9
	.long	0x39
	.byte	0x8
	.uleb128 0xa
	.long	.LASF18
	.byte	0x5
	.byte	0x37
	.byte	0x9
	.long	0x39
	.byte	0x10
	.uleb128 0xa
	.long	.LASF19
	.byte	0x5
	.byte	0x38
	.byte	0x9
	.long	0x39
	.byte	0x18
	.uleb128 0xa
	.long	.LASF20
	.byte	0x5
	.byte	0x39
	.byte	0x9
	.long	0x39
	.byte	0x20
	.uleb128 0xa
	.long	.LASF21
	.byte	0x5
	.byte	0x3a
	.byte	0x9
	.long	0x39
	.byte	0x28
	.uleb128 0xa
	.long	.LASF22
	.byte	0x5
	.byte	0x3b
	.byte	0x9
	.long	0x39
	.byte	0x30
	.uleb128 0xa
	.long	.LASF23
	.byte	0x5
	.byte	0x3c
	.byte	0x9
	.long	0x39
	.byte	0x38
	.uleb128 0xa
	.long	.LASF24
	.byte	0x5
	.byte	0x3d
	.byte	0x9
	.long	0x39
	.byte	0x40
	.uleb128 0xa
	.long	.LASF25
	.byte	0x5
	.byte	0x40
	.byte	0x9
	.long	0x39
	.byte	0x48
	.uleb128 0xa
	.long	.LASF26
	.byte	0x5
	.byte	0x41
	.byte	0x9
	.long	0x39
	.byte	0x50
	.uleb128 0xa
	.long	.LASF27
	.byte	0x5
	.byte	0x42
	.byte	0x9
	.long	0x39
	.byte	0x58
	.uleb128 0xa
	.long	.LASF28
	.byte	0x5
	.byte	0x44
	.byte	0x16
	.long	0x279
	.byte	0x60
	.uleb128 0xa
	.long	.LASF29
	.byte	0x5
	.byte	0x46
	.byte	0x14
	.long	0x27f
	.byte	0x68
	.uleb128 0xa
	.long	.LASF30
	.byte	0x5
	.byte	0x48
	.byte	0x7
	.long	0x57
	.byte	0x70
	.uleb128 0xa
	.long	.LASF31
	.byte	0x5
	.byte	0x49
	.byte	0x7
	.long	0x57
	.byte	0x74
	.uleb128 0xa
	.long	.LASF32
	.byte	0x5
	.byte	0x4a
	.byte	0xb
	.long	0xc1
	.byte	0x78
	.uleb128 0xa
	.long	.LASF33
	.byte	0x5
	.byte	0x4d
	.byte	0x12
	.long	0x88
	.byte	0x80
	.uleb128 0xa
	.long	.LASF34
	.byte	0x5
	.byte	0x4e
	.byte	0xf
	.long	0x8f
	.byte	0x82
	.uleb128 0xa
	.long	.LASF35
	.byte	0x5
	.byte	0x4f
	.byte	0x8
	.long	0x285
	.byte	0x83
	.uleb128 0xa
	.long	.LASF36
	.byte	0x5
	.byte	0x51
	.byte	0xf
	.long	0x295
	.byte	0x88
	.uleb128 0xa
	.long	.LASF37
	.byte	0x5
	.byte	0x59
	.byte	0xd
	.long	0xcd
	.byte	0x90
	.uleb128 0xa
	.long	.LASF38
	.byte	0x5
	.byte	0x5b
	.byte	0x17
	.long	0x2a0
	.byte	0x98
	.uleb128 0xa
	.long	.LASF39
	.byte	0x5
	.byte	0x5c
	.byte	0x19
	.long	0x2ab
	.byte	0xa0
	.uleb128 0xa
	.long	.LASF40
	.byte	0x5
	.byte	0x5d
	.byte	0x14
	.long	0x27f
	.byte	0xa8
	.uleb128 0xa
	.long	.LASF41
	.byte	0x5
	.byte	0x5e
	.byte	0x9
	.long	0x7f
	.byte	0xb0
	.uleb128 0xa
	.long	.LASF42
	.byte	0x5
	.byte	0x5f
	.byte	0xa
	.long	0x65
	.byte	0xb8
	.uleb128 0xa
	.long	.LASF43
	.byte	0x5
	.byte	0x60
	.byte	0x7
	.long	0x57
	.byte	0xc0
	.uleb128 0xa
	.long	.LASF44
	.byte	0x5
	.byte	0x62
	.byte	0x8
	.long	0x2b1
	.byte	0xc4
	.byte	0
	.uleb128 0x7
	.long	.LASF45
	.byte	0x6
	.byte	0x7
	.byte	0x19
	.long	0xd9
	.uleb128 0xb
	.long	.LASF125
	.byte	0x5
	.byte	0x2b
	.byte	0xe
	.uleb128 0xc
	.long	.LASF46
	.uleb128 0x3
	.byte	0x8
	.long	0x274
	.uleb128 0x3
	.byte	0x8
	.long	0xd9
	.uleb128 0xd
	.long	0x3f
	.long	0x295
	.uleb128 0xe
	.long	0x71
	.byte	0
	.byte	0
	.uleb128 0x3
	.byte	0x8
	.long	0x26c
	.uleb128 0xc
	.long	.LASF47
	.uleb128 0x3
	.byte	0x8
	.long	0x29b
	.uleb128 0xc
	.long	.LASF48
	.uleb128 0x3
	.byte	0x8
	.long	0x2a6
	.uleb128 0xd
	.long	0x3f
	.long	0x2c1
	.uleb128 0xe
	.long	0x71
	.byte	0x13
	.byte	0
	.uleb128 0x3
	.byte	0x8
	.long	0x46
	.uleb128 0x5
	.long	0x2c1
	.uleb128 0x2
	.long	.LASF49
	.byte	0x7
	.byte	0x89
	.byte	0xe
	.long	0x2d8
	.uleb128 0x3
	.byte	0x8
	.long	0x260
	.uleb128 0x2
	.long	.LASF50
	.byte	0x7
	.byte	0x8a
	.byte	0xe
	.long	0x2d8
	.uleb128 0x2
	.long	.LASF51
	.byte	0x7
	.byte	0x8b
	.byte	0xe
	.long	0x2d8
	.uleb128 0x2
	.long	.LASF52
	.byte	0x8
	.byte	0x1a
	.byte	0xc
	.long	0x57
	.uleb128 0xd
	.long	0x2c7
	.long	0x30d
	.uleb128 0xf
	.byte	0
	.uleb128 0x5
	.long	0x302
	.uleb128 0x2
	.long	.LASF53
	.byte	0x8
	.byte	0x1b
	.byte	0x1a
	.long	0x30d
	.uleb128 0x2
	.long	.LASF54
	.byte	0x8
	.byte	0x1e
	.byte	0xc
	.long	0x57
	.uleb128 0x2
	.long	.LASF55
	.byte	0x8
	.byte	0x1f
	.byte	0x1a
	.long	0x30d
	.uleb128 0x7
	.long	.LASF56
	.byte	0x9
	.byte	0x18
	.byte	0x13
	.long	0x96
	.uleb128 0x7
	.long	.LASF57
	.byte	0x9
	.byte	0x19
	.byte	0x14
	.long	0xa9
	.uleb128 0x7
	.long	.LASF58
	.byte	0x9
	.byte	0x1a
	.byte	0x14
	.long	0xb5
	.uleb128 0x4
	.byte	0x8
	.byte	0x7
	.long	.LASF59
	.uleb128 0x4
	.byte	0x8
	.byte	0x5
	.long	.LASF60
	.uleb128 0x7
	.long	.LASF61
	.byte	0xa
	.byte	0x1c
	.byte	0x1c
	.long	0x88
	.uleb128 0x9
	.long	.LASF63
	.byte	0x10
	.byte	0xb
	.byte	0xb2
	.byte	0x8
	.long	0x39c
	.uleb128 0xa
	.long	.LASF64
	.byte	0xb
	.byte	0xb4
	.byte	0x11
	.long	0x368
	.byte	0
	.uleb128 0xa
	.long	.LASF65
	.byte	0xb
	.byte	0xb5
	.byte	0xa
	.long	0x3a1
	.byte	0x2
	.byte	0
	.uleb128 0x5
	.long	0x374
	.uleb128 0xd
	.long	0x3f
	.long	0x3b1
	.uleb128 0xe
	.long	0x71
	.byte	0xd
	.byte	0
	.uleb128 0x3
	.byte	0x8
	.long	0x374
	.uleb128 0x10
	.long	0x3b1
	.uleb128 0xc
	.long	.LASF66
	.uleb128 0x5
	.long	0x3bc
	.uleb128 0x3
	.byte	0x8
	.long	0x3bc
	.uleb128 0x10
	.long	0x3c6
	.uleb128 0xc
	.long	.LASF67
	.uleb128 0x5
	.long	0x3d1
	.uleb128 0x3
	.byte	0x8
	.long	0x3d1
	.uleb128 0x10
	.long	0x3db
	.uleb128 0xc
	.long	.LASF68
	.uleb128 0x5
	.long	0x3e6
	.uleb128 0x3
	.byte	0x8
	.long	0x3e6
	.uleb128 0x10
	.long	0x3f0
	.uleb128 0xc
	.long	.LASF69
	.uleb128 0x5
	.long	0x3fb
	.uleb128 0x3
	.byte	0x8
	.long	0x3fb
	.uleb128 0x10
	.long	0x405
	.uleb128 0x9
	.long	.LASF70
	.byte	0x10
	.byte	0xc
	.byte	0xee
	.byte	0x8
	.long	0x452
	.uleb128 0xa
	.long	.LASF71
	.byte	0xc
	.byte	0xf0
	.byte	0x11
	.long	0x368
	.byte	0
	.uleb128 0xa
	.long	.LASF72
	.byte	0xc
	.byte	0xf1
	.byte	0xf
	.long	0x5f9
	.byte	0x2
	.uleb128 0xa
	.long	.LASF73
	.byte	0xc
	.byte	0xf2
	.byte	0x14
	.long	0x5de
	.byte	0x4
	.uleb128 0xa
	.long	.LASF74
	.byte	0xc
	.byte	0xf5
	.byte	0x13
	.long	0x69b
	.byte	0x8
	.byte	0
	.uleb128 0x5
	.long	0x410
	.uleb128 0x3
	.byte	0x8
	.long	0x410
	.uleb128 0x10
	.long	0x457
	.uleb128 0x9
	.long	.LASF75
	.byte	0x1c
	.byte	0xc
	.byte	0xfd
	.byte	0x8
	.long	0x4b5
	.uleb128 0xa
	.long	.LASF76
	.byte	0xc
	.byte	0xff
	.byte	0x11
	.long	0x368
	.byte	0
	.uleb128 0x11
	.long	.LASF77
	.byte	0xc
	.value	0x100
	.byte	0xf
	.long	0x5f9
	.byte	0x2
	.uleb128 0x11
	.long	.LASF78
	.byte	0xc
	.value	0x101
	.byte	0xe
	.long	0x34e
	.byte	0x4
	.uleb128 0x11
	.long	.LASF79
	.byte	0xc
	.value	0x102
	.byte	0x15
	.long	0x663
	.byte	0x8
	.uleb128 0x11
	.long	.LASF80
	.byte	0xc
	.value	0x103
	.byte	0xe
	.long	0x34e
	.byte	0x18
	.byte	0
	.uleb128 0x5
	.long	0x462
	.uleb128 0x3
	.byte	0x8
	.long	0x462
	.uleb128 0x10
	.long	0x4ba
	.uleb128 0xc
	.long	.LASF81
	.uleb128 0x5
	.long	0x4c5
	.uleb128 0x3
	.byte	0x8
	.long	0x4c5
	.uleb128 0x10
	.long	0x4cf
	.uleb128 0xc
	.long	.LASF82
	.uleb128 0x5
	.long	0x4da
	.uleb128 0x3
	.byte	0x8
	.long	0x4da
	.uleb128 0x10
	.long	0x4e4
	.uleb128 0xc
	.long	.LASF83
	.uleb128 0x5
	.long	0x4ef
	.uleb128 0x3
	.byte	0x8
	.long	0x4ef
	.uleb128 0x10
	.long	0x4f9
	.uleb128 0xc
	.long	.LASF84
	.uleb128 0x5
	.long	0x504
	.uleb128 0x3
	.byte	0x8
	.long	0x504
	.uleb128 0x10
	.long	0x50e
	.uleb128 0xc
	.long	.LASF85
	.uleb128 0x5
	.long	0x519
	.uleb128 0x3
	.byte	0x8
	.long	0x519
	.uleb128 0x10
	.long	0x523
	.uleb128 0xc
	.long	.LASF86
	.uleb128 0x5
	.long	0x52e
	.uleb128 0x3
	.byte	0x8
	.long	0x52e
	.uleb128 0x10
	.long	0x538
	.uleb128 0x3
	.byte	0x8
	.long	0x39c
	.uleb128 0x10
	.long	0x543
	.uleb128 0x3
	.byte	0x8
	.long	0x3c1
	.uleb128 0x10
	.long	0x54e
	.uleb128 0x3
	.byte	0x8
	.long	0x3d6
	.uleb128 0x10
	.long	0x559
	.uleb128 0x3
	.byte	0x8
	.long	0x3eb
	.uleb128 0x10
	.long	0x564
	.uleb128 0x3
	.byte	0x8
	.long	0x400
	.uleb128 0x10
	.long	0x56f
	.uleb128 0x3
	.byte	0x8
	.long	0x452
	.uleb128 0x10
	.long	0x57a
	.uleb128 0x3
	.byte	0x8
	.long	0x4b5
	.uleb128 0x10
	.long	0x585
	.uleb128 0x3
	.byte	0x8
	.long	0x4ca
	.uleb128 0x10
	.long	0x590
	.uleb128 0x3
	.byte	0x8
	.long	0x4df
	.uleb128 0x10
	.long	0x59b
	.uleb128 0x3
	.byte	0x8
	.long	0x4f4
	.uleb128 0x10
	.long	0x5a6
	.uleb128 0x3
	.byte	0x8
	.long	0x509
	.uleb128 0x10
	.long	0x5b1
	.uleb128 0x3
	.byte	0x8
	.long	0x51e
	.uleb128 0x10
	.long	0x5bc
	.uleb128 0x3
	.byte	0x8
	.long	0x533
	.uleb128 0x10
	.long	0x5c7
	.uleb128 0x7
	.long	.LASF87
	.byte	0xc
	.byte	0x1e
	.byte	0x12
	.long	0x34e
	.uleb128 0x9
	.long	.LASF88
	.byte	0x4
	.byte	0xc
	.byte	0x1f
	.byte	0x8
	.long	0x5f9
	.uleb128 0xa
	.long	.LASF89
	.byte	0xc
	.byte	0x21
	.byte	0xf
	.long	0x5d2
	.byte	0
	.byte	0
	.uleb128 0x7
	.long	.LASF90
	.byte	0xc
	.byte	0x77
	.byte	0x12
	.long	0x342
	.uleb128 0x12
	.byte	0x10
	.byte	0xc
	.byte	0xd6
	.byte	0x5
	.long	0x633
	.uleb128 0x13
	.long	.LASF91
	.byte	0xc
	.byte	0xd8
	.byte	0xa
	.long	0x633
	.uleb128 0x13
	.long	.LASF92
	.byte	0xc
	.byte	0xd9
	.byte	0xb
	.long	0x643
	.uleb128 0x13
	.long	.LASF93
	.byte	0xc
	.byte	0xda
	.byte	0xb
	.long	0x653
	.byte	0
	.uleb128 0xd
	.long	0x336
	.long	0x643
	.uleb128 0xe
	.long	0x71
	.byte	0xf
	.byte	0
	.uleb128 0xd
	.long	0x342
	.long	0x653
	.uleb128 0xe
	.long	0x71
	.byte	0x7
	.byte	0
	.uleb128 0xd
	.long	0x34e
	.long	0x663
	.uleb128 0xe
	.long	0x71
	.byte	0x3
	.byte	0
	.uleb128 0x9
	.long	.LASF94
	.byte	0x10
	.byte	0xc
	.byte	0xd4
	.byte	0x8
	.long	0x67e
	.uleb128 0xa
	.long	.LASF95
	.byte	0xc
	.byte	0xdb
	.byte	0x9
	.long	0x605
	.byte	0
	.byte	0
	.uleb128 0x5
	.long	0x663
	.uleb128 0x2
	.long	.LASF96
	.byte	0xc
	.byte	0xe4
	.byte	0x1e
	.long	0x67e
	.uleb128 0x2
	.long	.LASF97
	.byte	0xc
	.byte	0xe5
	.byte	0x1e
	.long	0x67e
	.uleb128 0xd
	.long	0x81
	.long	0x6ab
	.uleb128 0xe
	.long	0x71
	.byte	0x7
	.byte	0
	.uleb128 0xd
	.long	0x2c7
	.long	0x6bb
	.uleb128 0xe
	.long	0x71
	.byte	0x40
	.byte	0
	.uleb128 0x5
	.long	0x6ab
	.uleb128 0x14
	.long	.LASF98
	.byte	0xd
	.value	0x11e
	.byte	0x1a
	.long	0x6bb
	.uleb128 0x14
	.long	.LASF99
	.byte	0xd
	.value	0x11f
	.byte	0x1a
	.long	0x6bb
	.uleb128 0xd
	.long	0x39
	.long	0x6ea
	.uleb128 0xe
	.long	0x71
	.byte	0x1
	.byte	0
	.uleb128 0x2
	.long	.LASF100
	.byte	0xe
	.byte	0x9f
	.byte	0xe
	.long	0x6da
	.uleb128 0x2
	.long	.LASF101
	.byte	0xe
	.byte	0xa0
	.byte	0xc
	.long	0x57
	.uleb128 0x2
	.long	.LASF102
	.byte	0xe
	.byte	0xa1
	.byte	0x11
	.long	0x5e
	.uleb128 0x2
	.long	.LASF103
	.byte	0xe
	.byte	0xa6
	.byte	0xe
	.long	0x6da
	.uleb128 0x2
	.long	.LASF104
	.byte	0xe
	.byte	0xae
	.byte	0xc
	.long	0x57
	.uleb128 0x2
	.long	.LASF105
	.byte	0xe
	.byte	0xaf
	.byte	0x11
	.long	0x5e
	.uleb128 0x14
	.long	.LASF106
	.byte	0xe
	.value	0x112
	.byte	0xc
	.long	0x57
	.uleb128 0x15
	.byte	0x10
	.byte	0xf
	.byte	0xd8
	.byte	0x9
	.long	0x763
	.uleb128 0xa
	.long	.LASF107
	.byte	0xf
	.byte	0xd9
	.byte	0x9
	.long	0x7f
	.byte	0
	.uleb128 0xa
	.long	.LASF108
	.byte	0xf
	.byte	0xda
	.byte	0x9
	.long	0x39
	.byte	0x8
	.byte	0
	.uleb128 0x7
	.long	.LASF109
	.byte	0xf
	.byte	0xdb
	.byte	0x3
	.long	0x73f
	.uleb128 0x5
	.long	0x763
	.uleb128 0x4
	.byte	0x8
	.byte	0x4
	.long	.LASF110
	.uleb128 0x16
	.long	.LASF126
	.byte	0x1
	.byte	0x41
	.byte	0xc
	.long	0x57
	.byte	0x1
	.long	0x7a5
	.uleb128 0x17
	.string	"lib"
	.byte	0x1
	.byte	0x41
	.byte	0x22
	.long	0x7a5
	.uleb128 0x18
	.long	.LASF108
	.byte	0x1
	.byte	0x42
	.byte	0xf
	.long	0x2c1
	.byte	0
	.uleb128 0x3
	.byte	0x8
	.long	0x763
	.uleb128 0x19
	.long	.LASF111
	.byte	0x1
	.byte	0x3c
	.byte	0xd
	.long	0x2c1
	.quad	.LFB84
	.quad	.LFE84-.LFB84
	.uleb128 0x1
	.byte	0x9c
	.long	0x7dc
	.uleb128 0x1a
	.string	"lib"
	.byte	0x1
	.byte	0x3c
	.byte	0x28
	.long	0x7dc
	.uleb128 0x1
	.byte	0x55
	.byte	0
	.uleb128 0x3
	.byte	0x8
	.long	0x76f
	.uleb128 0x19
	.long	.LASF112
	.byte	0x1
	.byte	0x35
	.byte	0x5
	.long	0x57
	.quad	.LFB83
	.quad	.LFE83-.LFB83
	.uleb128 0x1
	.byte	0x9c
	.long	0x8c3
	.uleb128 0x1b
	.string	"lib"
	.byte	0x1
	.byte	0x35
	.byte	0x18
	.long	0x7a5
	.long	.LLST5
	.long	.LVUS5
	.uleb128 0x1c
	.long	.LASF113
	.byte	0x1
	.byte	0x35
	.byte	0x29
	.long	0x2c1
	.long	.LLST6
	.long	.LVUS6
	.uleb128 0x1b
	.string	"ptr"
	.byte	0x1
	.byte	0x35
	.byte	0x36
	.long	0x8c3
	.long	.LLST7
	.long	.LVUS7
	.uleb128 0x1d
	.long	0x77b
	.quad	.LBI12
	.byte	.LVU62
	.long	.Ldebug_ranges0+0x40
	.byte	0x1
	.byte	0x38
	.byte	0xa
	.long	0x8a1
	.uleb128 0x1e
	.long	0x78c
	.long	.LLST8
	.long	.LVUS8
	.uleb128 0x1f
	.long	.Ldebug_ranges0+0x40
	.uleb128 0x20
	.long	0x798
	.long	.LLST9
	.long	.LVUS9
	.uleb128 0x21
	.quad	.LVL21
	.long	0x9e9
	.uleb128 0x21
	.quad	.LVL22
	.long	0x9f6
	.uleb128 0x21
	.quad	.LVL28
	.long	0xa02
	.byte	0
	.byte	0
	.uleb128 0x21
	.quad	.LVL18
	.long	0x9f6
	.uleb128 0x22
	.quad	.LVL19
	.long	0xa0f
	.uleb128 0x23
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x2
	.byte	0x7d
	.sleb128 0
	.byte	0
	.byte	0
	.uleb128 0x3
	.byte	0x8
	.long	0x7f
	.uleb128 0x24
	.long	.LASF127
	.byte	0x1
	.byte	0x29
	.byte	0x6
	.quad	.LFB82
	.quad	.LFE82-.LFB82
	.uleb128 0x1
	.byte	0x9c
	.long	0x916
	.uleb128 0x1b
	.string	"lib"
	.byte	0x1
	.byte	0x29
	.byte	0x1b
	.long	0x7a5
	.long	.LLST4
	.long	.LVUS4
	.uleb128 0x21
	.quad	.LVL14
	.long	0x9e9
	.uleb128 0x21
	.quad	.LVL15
	.long	0xa1b
	.byte	0
	.uleb128 0x19
	.long	.LASF114
	.byte	0x1
	.byte	0x21
	.byte	0x5
	.long	0x57
	.quad	.LFB81
	.quad	.LFE81-.LFB81
	.uleb128 0x1
	.byte	0x9c
	.long	0x9e9
	.uleb128 0x1c
	.long	.LASF115
	.byte	0x1
	.byte	0x21
	.byte	0x1b
	.long	0x2c1
	.long	.LLST0
	.long	.LVUS0
	.uleb128 0x1b
	.string	"lib"
	.byte	0x1
	.byte	0x21
	.byte	0x2f
	.long	0x7a5
	.long	.LLST1
	.long	.LVUS1
	.uleb128 0x1d
	.long	0x77b
	.quad	.LBI4
	.byte	.LVU17
	.long	.Ldebug_ranges0+0
	.byte	0x1
	.byte	0x25
	.byte	0x1c
	.long	0x9c1
	.uleb128 0x1e
	.long	0x78c
	.long	.LLST2
	.long	.LVUS2
	.uleb128 0x1f
	.long	.Ldebug_ranges0+0
	.uleb128 0x20
	.long	0x798
	.long	.LLST3
	.long	.LVUS3
	.uleb128 0x21
	.quad	.LVL6
	.long	0x9e9
	.uleb128 0x21
	.quad	.LVL7
	.long	0x9f6
	.uleb128 0x21
	.quad	.LVL11
	.long	0xa02
	.byte	0
	.byte	0
	.uleb128 0x21
	.quad	.LVL1
	.long	0x9f6
	.uleb128 0x22
	.quad	.LVL3
	.long	0xa27
	.uleb128 0x23
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x3
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x23
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x1
	.byte	0x31
	.byte	0
	.byte	0
	.uleb128 0x25
	.long	.LASF116
	.long	.LASF116
	.byte	0x10
	.value	0x14d
	.byte	0x6
	.uleb128 0x26
	.long	.LASF117
	.long	.LASF117
	.byte	0x11
	.byte	0x52
	.byte	0xe
	.uleb128 0x25
	.long	.LASF118
	.long	.LASF118
	.byte	0x10
	.value	0x14a
	.byte	0x7
	.uleb128 0x26
	.long	.LASF119
	.long	.LASF119
	.byte	0x11
	.byte	0x40
	.byte	0xe
	.uleb128 0x26
	.long	.LASF120
	.long	.LASF120
	.byte	0x11
	.byte	0x3c
	.byte	0xc
	.uleb128 0x26
	.long	.LASF121
	.long	.LASF121
	.byte	0x11
	.byte	0x38
	.byte	0xe
	.byte	0
	.section	.debug_abbrev,"",@progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1b
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x7
	.uleb128 0x10
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x13
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x13
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3c
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x21
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x37
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0x17
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x16
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x20
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x17
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x18
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x19
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x7
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2117
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1a
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x1b
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x17
	.uleb128 0x2137
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x1c
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x17
	.uleb128 0x2137
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x1d
	.uleb128 0x1d
	.byte	0x1
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x2138
	.uleb128 0xb
	.uleb128 0x55
	.uleb128 0x17
	.uleb128 0x58
	.uleb128 0xb
	.uleb128 0x59
	.uleb128 0xb
	.uleb128 0x57
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1e
	.uleb128 0x5
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x17
	.uleb128 0x2137
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x1f
	.uleb128 0xb
	.byte	0x1
	.uleb128 0x55
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x20
	.uleb128 0x34
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x17
	.uleb128 0x2137
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x21
	.uleb128 0x4109
	.byte	0
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x31
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x22
	.uleb128 0x4109
	.byte	0x1
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x31
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x23
	.uleb128 0x410a
	.byte	0
	.uleb128 0x2
	.uleb128 0x18
	.uleb128 0x2111
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x24
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x7
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2117
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x25
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.uleb128 0x6e
	.uleb128 0xe
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x26
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.uleb128 0x6e
	.uleb128 0xe
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",@progbits
.Ldebug_loc0:
.LVUS5:
	.uleb128 0
	.uleb128 .LVU58
	.uleb128 .LVU58
	.uleb128 .LVU75
	.uleb128 .LVU75
	.uleb128 .LVU78
	.uleb128 .LVU78
	.uleb128 0
.LLST5:
	.quad	.LVL17-.Ltext0
	.quad	.LVL18-1-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	.LVL18-1-.Ltext0
	.quad	.LVL24-.Ltext0
	.value	0x1
	.byte	0x53
	.quad	.LVL24-.Ltext0
	.quad	.LVL27-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.byte	0x9f
	.quad	.LVL27-.Ltext0
	.quad	.LFE83-.Ltext0
	.value	0x1
	.byte	0x53
	.quad	0
	.quad	0
.LVUS6:
	.uleb128 0
	.uleb128 .LVU58
	.uleb128 .LVU58
	.uleb128 .LVU77
	.uleb128 .LVU77
	.uleb128 .LVU78
	.uleb128 .LVU78
	.uleb128 0
.LLST6:
	.quad	.LVL17-.Ltext0
	.quad	.LVL18-1-.Ltext0
	.value	0x1
	.byte	0x54
	.quad	.LVL18-1-.Ltext0
	.quad	.LVL26-.Ltext0
	.value	0x1
	.byte	0x5d
	.quad	.LVL26-.Ltext0
	.quad	.LVL27-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x54
	.byte	0x9f
	.quad	.LVL27-.Ltext0
	.quad	.LFE83-.Ltext0
	.value	0x1
	.byte	0x5d
	.quad	0
	.quad	0
.LVUS7:
	.uleb128 0
	.uleb128 .LVU58
	.uleb128 .LVU58
	.uleb128 .LVU76
	.uleb128 .LVU76
	.uleb128 .LVU78
	.uleb128 .LVU78
	.uleb128 0
.LLST7:
	.quad	.LVL17-.Ltext0
	.quad	.LVL18-1-.Ltext0
	.value	0x1
	.byte	0x51
	.quad	.LVL18-1-.Ltext0
	.quad	.LVL25-.Ltext0
	.value	0x1
	.byte	0x5c
	.quad	.LVL25-.Ltext0
	.quad	.LVL27-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x51
	.byte	0x9f
	.quad	.LVL27-.Ltext0
	.quad	.LFE83-.Ltext0
	.value	0x1
	.byte	0x5c
	.quad	0
	.quad	0
.LVUS8:
	.uleb128 .LVU63
	.uleb128 .LVU75
	.uleb128 .LVU75
	.uleb128 .LVU78
	.uleb128 .LVU78
	.uleb128 0
.LLST8:
	.quad	.LVL20-.Ltext0
	.quad	.LVL24-.Ltext0
	.value	0x1
	.byte	0x53
	.quad	.LVL24-.Ltext0
	.quad	.LVL27-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.byte	0x9f
	.quad	.LVL27-.Ltext0
	.quad	.LFE83-.Ltext0
	.value	0x1
	.byte	0x53
	.quad	0
	.quad	0
.LVUS9:
	.uleb128 .LVU67
	.uleb128 .LVU73
	.uleb128 .LVU78
	.uleb128 .LVU80
.LLST9:
	.quad	.LVL22-.Ltext0
	.quad	.LVL23-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	.LVL27-.Ltext0
	.quad	.LVL28-1-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	0
	.quad	0
.LVUS4:
	.uleb128 0
	.uleb128 .LVU42
	.uleb128 .LVU42
	.uleb128 .LVU52
	.uleb128 .LVU52
	.uleb128 0
.LLST4:
	.quad	.LVL12-.Ltext0
	.quad	.LVL13-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	.LVL13-.Ltext0
	.quad	.LVL16-.Ltext0
	.value	0x1
	.byte	0x53
	.quad	.LVL16-.Ltext0
	.quad	.LFE82-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS0:
	.uleb128 0
	.uleb128 .LVU6
	.uleb128 .LVU6
	.uleb128 .LVU11
	.uleb128 .LVU11
	.uleb128 .LVU12
	.uleb128 .LVU12
	.uleb128 0
.LLST0:
	.quad	.LVL0-.Ltext0
	.quad	.LVL1-1-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	.LVL1-1-.Ltext0
	.quad	.LVL2-.Ltext0
	.value	0x1
	.byte	0x5c
	.quad	.LVL2-.Ltext0
	.quad	.LVL3-1-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	.LVL3-1-.Ltext0
	.quad	.LFE81-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS1:
	.uleb128 0
	.uleb128 .LVU6
	.uleb128 .LVU6
	.uleb128 .LVU16
	.uleb128 .LVU16
	.uleb128 .LVU17
	.uleb128 .LVU17
	.uleb128 .LVU29
	.uleb128 .LVU29
	.uleb128 .LVU30
	.uleb128 .LVU30
	.uleb128 0
.LLST1:
	.quad	.LVL0-.Ltext0
	.quad	.LVL1-1-.Ltext0
	.value	0x1
	.byte	0x54
	.quad	.LVL1-1-.Ltext0
	.quad	.LVL4-.Ltext0
	.value	0x1
	.byte	0x53
	.quad	.LVL4-.Ltext0
	.quad	.LVL5-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x54
	.byte	0x9f
	.quad	.LVL5-.Ltext0
	.quad	.LVL9-.Ltext0
	.value	0x1
	.byte	0x53
	.quad	.LVL9-.Ltext0
	.quad	.LVL10-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x54
	.byte	0x9f
	.quad	.LVL10-.Ltext0
	.quad	.LFE81-.Ltext0
	.value	0x1
	.byte	0x53
	.quad	0
	.quad	0
.LVUS2:
	.uleb128 .LVU18
	.uleb128 .LVU29
	.uleb128 .LVU29
	.uleb128 .LVU30
	.uleb128 .LVU30
	.uleb128 0
.LLST2:
	.quad	.LVL5-.Ltext0
	.quad	.LVL9-.Ltext0
	.value	0x1
	.byte	0x53
	.quad	.LVL9-.Ltext0
	.quad	.LVL10-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x54
	.byte	0x9f
	.quad	.LVL10-.Ltext0
	.quad	.LFE81-.Ltext0
	.value	0x1
	.byte	0x53
	.quad	0
	.quad	0
.LVUS3:
	.uleb128 .LVU22
	.uleb128 .LVU26
	.uleb128 .LVU30
	.uleb128 .LVU34
.LLST3:
	.quad	.LVL7-.Ltext0
	.quad	.LVL8-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	.LVL10-.Ltext0
	.quad	.LVL11-1-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	0
	.quad	0
	.section	.debug_aranges,"",@progbits
	.long	0x2c
	.value	0x2
	.long	.Ldebug_info0
	.byte	0x8
	.byte	0
	.value	0
	.value	0
	.quad	.Ltext0
	.quad	.Letext0-.Ltext0
	.quad	0
	.quad	0
	.section	.debug_ranges,"",@progbits
.Ldebug_ranges0:
	.quad	.LBB4-.Ltext0
	.quad	.LBE4-.Ltext0
	.quad	.LBB8-.Ltext0
	.quad	.LBE8-.Ltext0
	.quad	.LBB9-.Ltext0
	.quad	.LBE9-.Ltext0
	.quad	0
	.quad	0
	.quad	.LBB12-.Ltext0
	.quad	.LBE12-.Ltext0
	.quad	.LBB15-.Ltext0
	.quad	.LBE15-.Ltext0
	.quad	0
	.quad	0
	.section	.debug_line,"",@progbits
.Ldebug_line0:
	.section	.debug_str,"MS",@progbits,1
.LASF14:
	.string	"__off_t"
.LASF17:
	.string	"_IO_read_ptr"
.LASF29:
	.string	"_chain"
.LASF79:
	.string	"sin6_addr"
.LASF95:
	.string	"__in6_u"
.LASF9:
	.string	"size_t"
.LASF35:
	.string	"_shortbuf"
.LASF10:
	.string	"__uint8_t"
.LASF23:
	.string	"_IO_buf_base"
.LASF59:
	.string	"long long unsigned int"
.LASF87:
	.string	"in_addr_t"
.LASF38:
	.string	"_codecvt"
.LASF102:
	.string	"__timezone"
.LASF120:
	.string	"dlclose"
.LASF60:
	.string	"long long int"
.LASF8:
	.string	"signed char"
.LASF81:
	.string	"sockaddr_inarp"
.LASF122:
	.string	"GNU C89 9.3.0 -m64 -mtune=generic -march=x86-64 -g -O3 -std=gnu90 -fvisibility=hidden -fno-omit-frame-pointer -fasynchronous-unwind-tables -fstack-protector-strong -fstack-clash-protection -fcf-protection"
.LASF66:
	.string	"sockaddr_at"
.LASF30:
	.string	"_fileno"
.LASF18:
	.string	"_IO_read_end"
.LASF125:
	.string	"_IO_lock_t"
.LASF92:
	.string	"__u6_addr16"
.LASF99:
	.string	"sys_siglist"
.LASF3:
	.string	"long int"
.LASF16:
	.string	"_flags"
.LASF24:
	.string	"_IO_buf_end"
.LASF49:
	.string	"stdin"
.LASF1:
	.string	"program_invocation_short_name"
.LASF47:
	.string	"_IO_codecvt"
.LASF68:
	.string	"sockaddr_dl"
.LASF77:
	.string	"sin6_port"
.LASF57:
	.string	"uint16_t"
.LASF55:
	.string	"_sys_errlist"
.LASF0:
	.string	"program_invocation_name"
.LASF32:
	.string	"_old_offset"
.LASF37:
	.string	"_offset"
.LASF97:
	.string	"in6addr_loopback"
.LASF86:
	.string	"sockaddr_x25"
.LASF82:
	.string	"sockaddr_ipx"
.LASF13:
	.string	"__uint32_t"
.LASF105:
	.string	"timezone"
.LASF74:
	.string	"sin_zero"
.LASF111:
	.string	"uv_dlerror"
.LASF46:
	.string	"_IO_marker"
.LASF123:
	.string	"../deps/uv/src/unix/dl.c"
.LASF89:
	.string	"s_addr"
.LASF41:
	.string	"_freeres_buf"
.LASF4:
	.string	"long unsigned int"
.LASF21:
	.string	"_IO_write_ptr"
.LASF113:
	.string	"name"
.LASF52:
	.string	"sys_nerr"
.LASF7:
	.string	"short unsigned int"
.LASF73:
	.string	"sin_addr"
.LASF114:
	.string	"uv_dlopen"
.LASF124:
	.string	"/workspace/nodejsbuild/node-v12.18.4/out"
.LASF25:
	.string	"_IO_save_base"
.LASF36:
	.string	"_lock"
.LASF93:
	.string	"__u6_addr32"
.LASF90:
	.string	"in_port_t"
.LASF50:
	.string	"stdout"
.LASF85:
	.string	"sockaddr_un"
.LASF71:
	.string	"sin_family"
.LASF117:
	.string	"dlerror"
.LASF106:
	.string	"getdate_err"
.LASF115:
	.string	"filename"
.LASF22:
	.string	"_IO_write_end"
.LASF84:
	.string	"sockaddr_ns"
.LASF96:
	.string	"in6addr_any"
.LASF62:
	.string	"_IO_FILE"
.LASF121:
	.string	"dlopen"
.LASF101:
	.string	"__daylight"
.LASF43:
	.string	"_mode"
.LASF72:
	.string	"sin_port"
.LASF64:
	.string	"sa_family"
.LASF53:
	.string	"sys_errlist"
.LASF28:
	.string	"_markers"
.LASF80:
	.string	"sin6_scope_id"
.LASF6:
	.string	"unsigned char"
.LASF83:
	.string	"sockaddr_iso"
.LASF11:
	.string	"short int"
.LASF48:
	.string	"_IO_wide_data"
.LASF31:
	.string	"_flags2"
.LASF54:
	.string	"_sys_nerr"
.LASF34:
	.string	"_vtable_offset"
.LASF103:
	.string	"tzname"
.LASF67:
	.string	"sockaddr_ax25"
.LASF45:
	.string	"FILE"
.LASF112:
	.string	"uv_dlsym"
.LASF94:
	.string	"in6_addr"
.LASF104:
	.string	"daylight"
.LASF109:
	.string	"uv_lib_t"
.LASF127:
	.string	"uv_dlclose"
.LASF119:
	.string	"dlsym"
.LASF2:
	.string	"char"
.LASF116:
	.string	"uv__free"
.LASF5:
	.string	"unsigned int"
.LASF78:
	.string	"sin6_flowinfo"
.LASF12:
	.string	"__uint16_t"
.LASF91:
	.string	"__u6_addr8"
.LASF15:
	.string	"__off64_t"
.LASF108:
	.string	"errmsg"
.LASF33:
	.string	"_cur_column"
.LASF19:
	.string	"_IO_read_base"
.LASF27:
	.string	"_IO_save_end"
.LASF98:
	.string	"_sys_siglist"
.LASF69:
	.string	"sockaddr_eon"
.LASF42:
	.string	"__pad5"
.LASF76:
	.string	"sin6_family"
.LASF61:
	.string	"sa_family_t"
.LASF44:
	.string	"_unused2"
.LASF51:
	.string	"stderr"
.LASF75:
	.string	"sockaddr_in6"
.LASF63:
	.string	"sockaddr"
.LASF70:
	.string	"sockaddr_in"
.LASF56:
	.string	"uint8_t"
.LASF26:
	.string	"_IO_backup_base"
.LASF118:
	.string	"uv__strdup"
.LASF107:
	.string	"handle"
.LASF65:
	.string	"sa_data"
.LASF40:
	.string	"_freeres_list"
.LASF39:
	.string	"_wide_data"
.LASF126:
	.string	"uv__dlerror"
.LASF100:
	.string	"__tzname"
.LASF20:
	.string	"_IO_write_base"
.LASF110:
	.string	"double"
.LASF58:
	.string	"uint32_t"
.LASF88:
	.string	"in_addr"
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
