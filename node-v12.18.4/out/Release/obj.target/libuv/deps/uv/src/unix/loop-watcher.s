	.file	"loop-watcher.c"
	.text
.Ltext0:
	.p2align 4
	.globl	uv_prepare_init
	.type	uv_prepare_init, @function
uv_prepare_init:
.LVL0:
.LFB81:
	.file 1 "../deps/uv/src/unix/loop-watcher.c"
	.loc 1 66 60 view -0
	.cfi_startproc
	.loc 1 66 60 is_stmt 0 view .LVU1
	endbr64
	.loc 1 66 62 is_stmt 1 view .LVU2
	.loc 1 66 67 view .LVU3
	.loc 1 66 269 is_stmt 0 view .LVU4
	leaq	16(%rdi), %rax
	.loc 1 66 96 view .LVU5
	movq	%rdi, 8(%rsi)
	.loc 1 66 106 is_stmt 1 view .LVU6
	.loc 1 66 269 is_stmt 0 view .LVU7
	movq	%rax, 32(%rsi)
	.loc 1 66 399 view .LVU8
	movq	24(%rdi), %rdx
	.loc 1 66 499 view .LVU9
	leaq	32(%rsi), %rax
	.loc 1 66 135 view .LVU10
	movl	$9, 16(%rsi)
	.loc 1 66 151 is_stmt 1 view .LVU11
	.loc 1 66 181 is_stmt 0 view .LVU12
	movl	$8, 88(%rsi)
	.loc 1 66 198 is_stmt 1 view .LVU13
	.loc 1 66 203 view .LVU14
	.loc 1 66 293 view .LVU15
	.loc 1 66 356 is_stmt 0 view .LVU16
	movq	%rdx, 40(%rsi)
	.loc 1 66 406 is_stmt 1 view .LVU17
	.loc 1 66 496 is_stmt 0 view .LVU18
	movq	%rax, (%rdx)
	.loc 1 66 539 is_stmt 1 view .LVU19
	.loc 1 66 586 is_stmt 0 view .LVU20
	movq	%rax, 24(%rdi)
	.loc 1 66 637 is_stmt 1 view .LVU21
	.loc 1 66 642 view .LVU22
	.loc 1 66 13 is_stmt 0 view .LVU23
	xorl	%eax, %eax
	.loc 1 66 680 view .LVU24
	movq	$0, 80(%rsi)
	.loc 1 66 12 is_stmt 1 view .LVU25
	.loc 1 66 17 view .LVU26
	.loc 1 66 36 is_stmt 0 view .LVU27
	movq	$0, 96(%rsi)
	.loc 1 66 3 is_stmt 1 view .LVU28
	.loc 1 66 13 is_stmt 0 view .LVU29
	ret
	.cfi_endproc
.LFE81:
	.size	uv_prepare_init, .-uv_prepare_init
	.p2align 4
	.globl	uv_prepare_start
	.type	uv_prepare_start, @function
uv_prepare_start:
.LVL1:
.LFB82:
	.loc 1 66 76 is_stmt 1 view -0
	.cfi_startproc
	.loc 1 66 76 is_stmt 0 view .LVU31
	endbr64
	.loc 1 66 78 is_stmt 1 view .LVU32
	.loc 1 66 92 is_stmt 0 view .LVU33
	movl	88(%rdi), %eax
	.loc 1 66 81 view .LVU34
	testb	$4, %al
	jne	.L11
	.loc 1 66 137 is_stmt 1 discriminator 2 view .LVU35
	.loc 1 66 140 is_stmt 0 discriminator 2 view .LVU36
	testq	%rsi, %rsi
	je	.L7
	.loc 1 66 21 is_stmt 1 discriminator 4 view .LVU37
	.loc 1 66 26 discriminator 4 view .LVU38
	.loc 1 66 93 is_stmt 0 discriminator 4 view .LVU39
	movq	8(%rdi), %rdx
	.loc 1 66 118 discriminator 4 view .LVU40
	movq	384(%rdx), %rcx
	.loc 1 66 125 is_stmt 1 discriminator 4 view .LVU41
	.loc 1 66 168 is_stmt 0 discriminator 4 view .LVU42
	addq	$384, %rdx
	.loc 1 66 66 discriminator 4 view .LVU43
	movq	%rdx, %xmm1
	.loc 1 66 271 discriminator 4 view .LVU44
	leaq	104(%rdi), %rdx
	.loc 1 66 66 discriminator 4 view .LVU45
	movq	%rcx, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 104(%rdi)
	.loc 1 66 201 is_stmt 1 discriminator 4 view .LVU46
	.loc 1 66 268 is_stmt 0 discriminator 4 view .LVU47
	movq	%rdx, 8(%rcx)
	.loc 1 66 288 is_stmt 1 discriminator 4 view .LVU48
	.loc 1 66 313 is_stmt 0 discriminator 4 view .LVU49
	movq	8(%rdi), %rcx
	.loc 1 66 344 discriminator 4 view .LVU50
	movq	%rdx, 384(%rcx)
	.loc 1 66 372 is_stmt 1 discriminator 4 view .LVU51
	.loc 1 66 377 discriminator 4 view .LVU52
	.loc 1 66 477 is_stmt 0 discriminator 4 view .LVU53
	movl	%eax, %edx
	orl	$4, %edx
	.loc 1 66 396 discriminator 4 view .LVU54
	movq	%rsi, 96(%rdi)
	.loc 1 66 402 is_stmt 1 discriminator 4 view .LVU55
	.loc 1 66 407 discriminator 4 view .LVU56
	.loc 1 66 461 discriminator 4 view .LVU57
	.loc 1 66 477 is_stmt 0 discriminator 4 view .LVU58
	movl	%edx, 88(%rdi)
	.loc 1 66 498 is_stmt 1 discriminator 4 view .LVU59
	.loc 1 66 501 is_stmt 0 discriminator 4 view .LVU60
	testb	$8, %al
	je	.L11
	.loc 1 66 542 is_stmt 1 discriminator 7 view .LVU61
	.loc 1 66 547 discriminator 7 view .LVU62
	.loc 1 66 555 is_stmt 0 discriminator 7 view .LVU63
	movq	8(%rdi), %rax
	.loc 1 66 577 discriminator 7 view .LVU64
	addl	$1, 8(%rax)
.L11:
	.loc 1 66 614 discriminator 7 view .LVU65
	xorl	%eax, %eax
	ret
.L7:
	.loc 1 66 10 view .LVU66
	movl	$-22, %eax
	.loc 1 66 617 view .LVU67
	ret
	.cfi_endproc
.LFE82:
	.size	uv_prepare_start, .-uv_prepare_start
	.p2align 4
	.globl	uv_prepare_stop
	.type	uv_prepare_stop, @function
uv_prepare_stop:
.LVL2:
.LFB83:
	.loc 1 66 661 is_stmt 1 view -0
	.cfi_startproc
	.loc 1 66 661 is_stmt 0 view .LVU69
	endbr64
	.loc 1 66 663 is_stmt 1 view .LVU70
	.loc 1 66 678 is_stmt 0 view .LVU71
	movl	88(%rdi), %eax
	.loc 1 66 666 view .LVU72
	testb	$4, %al
	je	.L14
	.loc 1 66 723 is_stmt 1 discriminator 2 view .LVU73
	.loc 1 66 728 discriminator 2 view .LVU74
	.loc 1 66 748 is_stmt 0 discriminator 2 view .LVU75
	movq	112(%rdi), %rcx
	.loc 1 66 831 discriminator 2 view .LVU76
	movq	104(%rdi), %rdx
	.loc 1 66 795 discriminator 2 view .LVU77
	movq	%rdx, (%rcx)
	.loc 1 66 838 is_stmt 1 discriminator 2 view .LVU78
	.loc 1 66 941 is_stmt 0 discriminator 2 view .LVU79
	movq	112(%rdi), %rcx
	.loc 1 66 905 discriminator 2 view .LVU80
	movq	%rcx, 8(%rdx)
	.loc 1 66 956 is_stmt 1 discriminator 2 view .LVU81
	.loc 1 66 961 discriminator 2 view .LVU82
	.loc 1 66 966 discriminator 2 view .LVU83
	.loc 1 66 1020 discriminator 2 view .LVU84
	.loc 1 66 1036 is_stmt 0 discriminator 2 view .LVU85
	movl	%eax, %edx
	andl	$-5, %edx
	movl	%edx, 88(%rdi)
	.loc 1 66 1058 is_stmt 1 discriminator 2 view .LVU86
	.loc 1 66 1061 is_stmt 0 discriminator 2 view .LVU87
	testb	$8, %al
	je	.L14
.LVL3:
.LBB4:
.LBI4:
	.loc 1 66 623 is_stmt 1 view .LVU88
.LBB5:
	.loc 1 66 1102 view .LVU89
	.loc 1 66 1107 view .LVU90
	.loc 1 66 1115 is_stmt 0 view .LVU91
	movq	8(%rdi), %rax
	.loc 1 66 1137 view .LVU92
	subl	$1, 8(%rax)
.LVL4:
.L14:
	.loc 1 66 1137 view .LVU93
.LBE5:
.LBE4:
	.loc 1 66 1177 view .LVU94
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE83:
	.size	uv_prepare_stop, .-uv_prepare_stop
	.p2align 4
	.globl	uv__run_prepare
	.hidden	uv__run_prepare
	.type	uv__run_prepare, @function
uv__run_prepare:
.LVL5:
.LFB84:
	.loc 1 66 1217 is_stmt 1 view -0
	.cfi_startproc
	.loc 1 66 1217 is_stmt 0 view .LVU96
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.loc 1 66 1286 view .LVU97
	leaq	384(%rdi), %r12
	.loc 1 66 1217 view .LVU98
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -40
	.loc 1 66 1217 view .LVU99
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	.loc 1 66 1219 is_stmt 1 view .LVU100
	.loc 1 66 1236 view .LVU101
	.loc 1 66 1249 view .LVU102
	.loc 1 66 1259 view .LVU103
	.loc 1 66 1264 view .LVU104
	.loc 1 66 1313 is_stmt 0 view .LVU105
	movq	384(%rdi), %rax
	.loc 1 66 1267 view .LVU106
	cmpq	%rax, %r12
	je	.L19
.LBB6:
	.loc 1 66 1635 discriminator 2 view .LVU107
	movq	392(%rdi), %rdx
	.loc 1 66 1701 discriminator 2 view .LVU108
	leaq	-64(%rbp), %rbx
	movq	%rdi, %r13
	.loc 1 66 1492 is_stmt 1 discriminator 2 view .LVU109
.LVL6:
	.loc 1 66 1554 discriminator 2 view .LVU110
	.loc 1 66 1559 discriminator 2 view .LVU111
	.loc 1 66 1591 is_stmt 0 discriminator 2 view .LVU112
	movq	%rdx, -56(%rbp)
	.loc 1 66 1642 is_stmt 1 discriminator 2 view .LVU113
	.loc 1 66 1701 is_stmt 0 discriminator 2 view .LVU114
	movq	%rbx, (%rdx)
	.loc 1 66 1713 is_stmt 1 discriminator 2 view .LVU115
	.loc 1 66 1823 is_stmt 0 discriminator 2 view .LVU116
	movq	8(%rax), %rdx
	.loc 1 66 1745 discriminator 2 view .LVU117
	movq	%rax, -64(%rbp)
	.loc 1 66 1752 is_stmt 1 discriminator 2 view .LVU118
	.loc 1 66 1800 is_stmt 0 discriminator 2 view .LVU119
	movq	%rdx, 392(%rdi)
	.loc 1 66 1830 is_stmt 1 discriminator 2 view .LVU120
	.loc 1 66 1905 is_stmt 0 discriminator 2 view .LVU121
	movq	%r12, (%rdx)
	.loc 1 66 1933 is_stmt 1 discriminator 2 view .LVU122
	.loc 1 66 1960 is_stmt 0 discriminator 2 view .LVU123
	movq	%rbx, 8(%rax)
.LBE6:
	.loc 1 66 2006 is_stmt 1 discriminator 2 view .LVU124
	.loc 1 66 2037 is_stmt 0 discriminator 2 view .LVU125
	movq	-64(%rbp), %rax
.LVL7:
	.loc 1 66 2006 discriminator 2 view .LVU126
	cmpq	%rbx, %rax
	je	.L19
.LVL8:
	.p2align 4,,10
	.p2align 3
.L21:
	.loc 1 66 2089 is_stmt 1 discriminator 6 view .LVU127
	.loc 1 66 2128 discriminator 6 view .LVU128
	.loc 1 66 5 discriminator 6 view .LVU129
	.loc 1 66 10 discriminator 6 view .LVU130
	.loc 1 66 30 is_stmt 0 discriminator 6 view .LVU131
	movq	8(%rax), %rcx
	.loc 1 66 87 discriminator 6 view .LVU132
	movq	(%rax), %rdx
	.loc 1 66 2130 discriminator 6 view .LVU133
	leaq	-104(%rax), %rdi
.LVL9:
	.loc 1 66 64 discriminator 6 view .LVU134
	movq	%rdx, (%rcx)
	.loc 1 66 94 is_stmt 1 discriminator 6 view .LVU135
	.loc 1 66 171 is_stmt 0 discriminator 6 view .LVU136
	movq	8(%rax), %rcx
	.loc 1 66 148 discriminator 6 view .LVU137
	movq	%rcx, 8(%rdx)
	.loc 1 66 186 is_stmt 1 discriminator 6 view .LVU138
	.loc 1 66 191 discriminator 6 view .LVU139
	.loc 1 66 196 discriminator 6 view .LVU140
	.loc 1 66 223 is_stmt 0 discriminator 6 view .LVU141
	movq	%r12, (%rax)
	.loc 1 66 251 is_stmt 1 discriminator 6 view .LVU142
	.loc 1 66 322 is_stmt 0 discriminator 6 view .LVU143
	movq	392(%r13), %rdx
	.loc 1 66 278 discriminator 6 view .LVU144
	movq	%rdx, 8(%rax)
	.loc 1 66 329 is_stmt 1 discriminator 6 view .LVU145
	.loc 1 66 383 is_stmt 0 discriminator 6 view .LVU146
	movq	%rax, (%rdx)
	.loc 1 66 390 is_stmt 1 discriminator 6 view .LVU147
	.loc 1 66 438 is_stmt 0 discriminator 6 view .LVU148
	movq	%rax, 392(%r13)
	.loc 1 66 453 is_stmt 1 discriminator 6 view .LVU149
	.loc 1 66 458 discriminator 6 view .LVU150
	call	*-8(%rax)
.LVL10:
	.loc 1 66 2006 discriminator 6 view .LVU151
	.loc 1 66 2037 is_stmt 0 discriminator 6 view .LVU152
	movq	-64(%rbp), %rax
	.loc 1 66 2006 discriminator 6 view .LVU153
	cmpq	%rbx, %rax
	jne	.L21
.LVL11:
.L19:
	.loc 1 66 478 view .LVU154
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L25
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
.LVL12:
	.loc 1 66 478 view .LVU155
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L25:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
.LVL13:
	.cfi_endproc
.LFE84:
	.size	uv__run_prepare, .-uv__run_prepare
	.p2align 4
	.globl	uv__prepare_close
	.hidden	uv__prepare_close
	.type	uv__prepare_close, @function
uv__prepare_close:
.LVL14:
.LFB85:
	.loc 1 66 525 is_stmt 1 view -0
	.cfi_startproc
	.loc 1 66 525 is_stmt 0 view .LVU157
	endbr64
	.loc 1 66 527 is_stmt 1 view .LVU158
.LVL15:
.LBB11:
.LBI11:
	.loc 1 66 623 view .LVU159
.LBB12:
	.loc 1 66 663 view .LVU160
	.loc 1 66 678 is_stmt 0 view .LVU161
	movl	88(%rdi), %eax
	.loc 1 66 666 view .LVU162
	testb	$4, %al
	je	.L26
	.loc 1 66 723 is_stmt 1 view .LVU163
	.loc 1 66 728 view .LVU164
	.loc 1 66 748 is_stmt 0 view .LVU165
	movq	112(%rdi), %rcx
	.loc 1 66 831 view .LVU166
	movq	104(%rdi), %rdx
	.loc 1 66 795 view .LVU167
	movq	%rdx, (%rcx)
	.loc 1 66 838 is_stmt 1 view .LVU168
	.loc 1 66 941 is_stmt 0 view .LVU169
	movq	112(%rdi), %rcx
	.loc 1 66 905 view .LVU170
	movq	%rcx, 8(%rdx)
	.loc 1 66 956 is_stmt 1 view .LVU171
	.loc 1 66 961 view .LVU172
	.loc 1 66 966 view .LVU173
	.loc 1 66 1020 view .LVU174
	.loc 1 66 1036 is_stmt 0 view .LVU175
	movl	%eax, %edx
	andl	$-5, %edx
	movl	%edx, 88(%rdi)
	.loc 1 66 1058 is_stmt 1 view .LVU176
	.loc 1 66 1061 is_stmt 0 view .LVU177
	testb	$8, %al
	je	.L26
.LVL16:
.LBB13:
.LBI13:
	.loc 1 66 623 is_stmt 1 view .LVU178
.LBB14:
	.loc 1 66 1102 view .LVU179
	.loc 1 66 1107 view .LVU180
	.loc 1 66 1115 is_stmt 0 view .LVU181
	movq	8(%rdi), %rax
	.loc 1 66 1137 view .LVU182
	subl	$1, 8(%rax)
.LVL17:
.L26:
	.loc 1 66 1137 view .LVU183
.LBE14:
.LBE13:
.LBE12:
.LBE11:
	.loc 1 66 552 view .LVU184
	ret
	.cfi_endproc
.LFE85:
	.size	uv__prepare_close, .-uv__prepare_close
	.p2align 4
	.globl	uv_check_init
	.type	uv_check_init, @function
uv_check_init:
.LVL18:
.LFB86:
	.loc 1 67 56 is_stmt 1 view -0
	.cfi_startproc
	.loc 1 67 56 is_stmt 0 view .LVU186
	endbr64
	.loc 1 67 58 is_stmt 1 view .LVU187
	.loc 1 67 63 view .LVU188
	.loc 1 67 263 is_stmt 0 view .LVU189
	leaq	16(%rdi), %rax
	.loc 1 67 92 view .LVU190
	movq	%rdi, 8(%rsi)
	.loc 1 67 102 is_stmt 1 view .LVU191
	.loc 1 67 263 is_stmt 0 view .LVU192
	movq	%rax, 32(%rsi)
	.loc 1 67 393 view .LVU193
	movq	24(%rdi), %rdx
	.loc 1 67 493 view .LVU194
	leaq	32(%rsi), %rax
	.loc 1 67 131 view .LVU195
	movl	$2, 16(%rsi)
	.loc 1 67 145 is_stmt 1 view .LVU196
	.loc 1 67 175 is_stmt 0 view .LVU197
	movl	$8, 88(%rsi)
	.loc 1 67 192 is_stmt 1 view .LVU198
	.loc 1 67 197 view .LVU199
	.loc 1 67 287 view .LVU200
	.loc 1 67 350 is_stmt 0 view .LVU201
	movq	%rdx, 40(%rsi)
	.loc 1 67 400 is_stmt 1 view .LVU202
	.loc 1 67 490 is_stmt 0 view .LVU203
	movq	%rax, (%rdx)
	.loc 1 67 533 is_stmt 1 view .LVU204
	.loc 1 67 580 is_stmt 0 view .LVU205
	movq	%rax, 24(%rdi)
	.loc 1 67 631 is_stmt 1 view .LVU206
	.loc 1 67 636 view .LVU207
	.loc 1 67 13 is_stmt 0 view .LVU208
	xorl	%eax, %eax
	.loc 1 67 674 view .LVU209
	movq	$0, 80(%rsi)
	.loc 1 67 12 is_stmt 1 view .LVU210
	.loc 1 67 17 view .LVU211
	.loc 1 67 34 is_stmt 0 view .LVU212
	movq	$0, 96(%rsi)
	.loc 1 67 3 is_stmt 1 view .LVU213
	.loc 1 67 13 is_stmt 0 view .LVU214
	ret
	.cfi_endproc
.LFE86:
	.size	uv_check_init, .-uv_check_init
	.p2align 4
	.globl	uv_check_start
	.type	uv_check_start, @function
uv_check_start:
.LVL19:
.LFB87:
	.loc 1 67 70 is_stmt 1 view -0
	.cfi_startproc
	.loc 1 67 70 is_stmt 0 view .LVU216
	endbr64
	.loc 1 67 72 is_stmt 1 view .LVU217
	.loc 1 67 86 is_stmt 0 view .LVU218
	movl	88(%rdi), %eax
	.loc 1 67 75 view .LVU219
	testb	$4, %al
	jne	.L42
	.loc 1 67 131 is_stmt 1 discriminator 2 view .LVU220
	.loc 1 67 134 is_stmt 0 discriminator 2 view .LVU221
	testq	%rsi, %rsi
	je	.L38
	.loc 1 67 21 is_stmt 1 discriminator 4 view .LVU222
	.loc 1 67 26 discriminator 4 view .LVU223
	.loc 1 67 93 is_stmt 0 discriminator 4 view .LVU224
	movq	8(%rdi), %rdx
	.loc 1 67 116 discriminator 4 view .LVU225
	movq	400(%rdx), %rcx
	.loc 1 67 123 is_stmt 1 discriminator 4 view .LVU226
	.loc 1 67 166 is_stmt 0 discriminator 4 view .LVU227
	addq	$400, %rdx
	.loc 1 67 66 discriminator 4 view .LVU228
	movq	%rdx, %xmm1
	.loc 1 67 267 discriminator 4 view .LVU229
	leaq	104(%rdi), %rdx
	.loc 1 67 66 discriminator 4 view .LVU230
	movq	%rcx, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 104(%rdi)
	.loc 1 67 197 is_stmt 1 discriminator 4 view .LVU231
	.loc 1 67 264 is_stmt 0 discriminator 4 view .LVU232
	movq	%rdx, 8(%rcx)
	.loc 1 67 284 is_stmt 1 discriminator 4 view .LVU233
	.loc 1 67 309 is_stmt 0 discriminator 4 view .LVU234
	movq	8(%rdi), %rcx
	.loc 1 67 338 discriminator 4 view .LVU235
	movq	%rdx, 400(%rcx)
	.loc 1 67 366 is_stmt 1 discriminator 4 view .LVU236
	.loc 1 67 371 discriminator 4 view .LVU237
	.loc 1 67 469 is_stmt 0 discriminator 4 view .LVU238
	movl	%eax, %edx
	orl	$4, %edx
	.loc 1 67 388 discriminator 4 view .LVU239
	movq	%rsi, 96(%rdi)
	.loc 1 67 394 is_stmt 1 discriminator 4 view .LVU240
	.loc 1 67 399 discriminator 4 view .LVU241
	.loc 1 67 453 discriminator 4 view .LVU242
	.loc 1 67 469 is_stmt 0 discriminator 4 view .LVU243
	movl	%edx, 88(%rdi)
	.loc 1 67 490 is_stmt 1 discriminator 4 view .LVU244
	.loc 1 67 493 is_stmt 0 discriminator 4 view .LVU245
	testb	$8, %al
	je	.L42
	.loc 1 67 534 is_stmt 1 discriminator 7 view .LVU246
	.loc 1 67 539 discriminator 7 view .LVU247
	.loc 1 67 547 is_stmt 0 discriminator 7 view .LVU248
	movq	8(%rdi), %rax
	.loc 1 67 569 discriminator 7 view .LVU249
	addl	$1, 8(%rax)
.L42:
	.loc 1 67 606 discriminator 7 view .LVU250
	xorl	%eax, %eax
	ret
.L38:
	.loc 1 67 10 view .LVU251
	movl	$-22, %eax
	.loc 1 67 609 view .LVU252
	ret
	.cfi_endproc
.LFE87:
	.size	uv_check_start, .-uv_check_start
	.p2align 4
	.globl	uv_check_stop
	.type	uv_check_stop, @function
uv_check_stop:
.LVL20:
.LFB88:
	.loc 1 67 649 is_stmt 1 view -0
	.cfi_startproc
	.loc 1 67 649 is_stmt 0 view .LVU254
	endbr64
	.loc 1 67 651 is_stmt 1 view .LVU255
	.loc 1 67 666 is_stmt 0 view .LVU256
	movl	88(%rdi), %eax
	.loc 1 67 654 view .LVU257
	testb	$4, %al
	je	.L45
	.loc 1 67 711 is_stmt 1 discriminator 2 view .LVU258
	.loc 1 67 716 discriminator 2 view .LVU259
	.loc 1 67 736 is_stmt 0 discriminator 2 view .LVU260
	movq	112(%rdi), %rcx
	.loc 1 67 819 discriminator 2 view .LVU261
	movq	104(%rdi), %rdx
	.loc 1 67 783 discriminator 2 view .LVU262
	movq	%rdx, (%rcx)
	.loc 1 67 826 is_stmt 1 discriminator 2 view .LVU263
	.loc 1 67 929 is_stmt 0 discriminator 2 view .LVU264
	movq	112(%rdi), %rcx
	.loc 1 67 893 discriminator 2 view .LVU265
	movq	%rcx, 8(%rdx)
	.loc 1 67 944 is_stmt 1 discriminator 2 view .LVU266
	.loc 1 67 949 discriminator 2 view .LVU267
	.loc 1 67 954 discriminator 2 view .LVU268
	.loc 1 67 1008 discriminator 2 view .LVU269
	.loc 1 67 1024 is_stmt 0 discriminator 2 view .LVU270
	movl	%eax, %edx
	andl	$-5, %edx
	movl	%edx, 88(%rdi)
	.loc 1 67 1046 is_stmt 1 discriminator 2 view .LVU271
	.loc 1 67 1049 is_stmt 0 discriminator 2 view .LVU272
	testb	$8, %al
	je	.L45
.LVL21:
.LBB17:
.LBI17:
	.loc 1 67 615 is_stmt 1 view .LVU273
.LBB18:
	.loc 1 67 1090 view .LVU274
	.loc 1 67 1095 view .LVU275
	.loc 1 67 1103 is_stmt 0 view .LVU276
	movq	8(%rdi), %rax
	.loc 1 67 1125 view .LVU277
	subl	$1, 8(%rax)
.LVL22:
.L45:
	.loc 1 67 1125 view .LVU278
.LBE18:
.LBE17:
	.loc 1 67 1165 view .LVU279
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE88:
	.size	uv_check_stop, .-uv_check_stop
	.p2align 4
	.globl	uv__run_check
	.hidden	uv__run_check
	.type	uv__run_check, @function
uv__run_check:
.LVL23:
.LFB89:
	.loc 1 67 1203 is_stmt 1 view -0
	.cfi_startproc
	.loc 1 67 1203 is_stmt 0 view .LVU281
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.loc 1 67 1270 view .LVU282
	leaq	400(%rdi), %r12
	.loc 1 67 1203 view .LVU283
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -40
	.loc 1 67 1203 view .LVU284
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	.loc 1 67 1205 is_stmt 1 view .LVU285
	.loc 1 67 1220 view .LVU286
	.loc 1 67 1233 view .LVU287
	.loc 1 67 1243 view .LVU288
	.loc 1 67 1248 view .LVU289
	.loc 1 67 1295 is_stmt 0 view .LVU290
	movq	400(%rdi), %rax
	.loc 1 67 1251 view .LVU291
	cmpq	%rax, %r12
	je	.L50
.LBB19:
	.loc 1 67 1611 discriminator 2 view .LVU292
	movq	408(%rdi), %rdx
	.loc 1 67 1677 discriminator 2 view .LVU293
	leaq	-64(%rbp), %rbx
	movq	%rdi, %r13
	.loc 1 67 1472 is_stmt 1 discriminator 2 view .LVU294
.LVL24:
	.loc 1 67 1532 discriminator 2 view .LVU295
	.loc 1 67 1537 discriminator 2 view .LVU296
	.loc 1 67 1569 is_stmt 0 discriminator 2 view .LVU297
	movq	%rdx, -56(%rbp)
	.loc 1 67 1618 is_stmt 1 discriminator 2 view .LVU298
	.loc 1 67 1677 is_stmt 0 discriminator 2 view .LVU299
	movq	%rbx, (%rdx)
	.loc 1 67 1689 is_stmt 1 discriminator 2 view .LVU300
	.loc 1 67 1797 is_stmt 0 discriminator 2 view .LVU301
	movq	8(%rax), %rdx
	.loc 1 67 1721 discriminator 2 view .LVU302
	movq	%rax, -64(%rbp)
	.loc 1 67 1728 is_stmt 1 discriminator 2 view .LVU303
	.loc 1 67 1774 is_stmt 0 discriminator 2 view .LVU304
	movq	%rdx, 408(%rdi)
	.loc 1 67 1804 is_stmt 1 discriminator 2 view .LVU305
	.loc 1 67 1877 is_stmt 0 discriminator 2 view .LVU306
	movq	%r12, (%rdx)
	.loc 1 67 1903 is_stmt 1 discriminator 2 view .LVU307
	.loc 1 67 1930 is_stmt 0 discriminator 2 view .LVU308
	movq	%rbx, 8(%rax)
.LBE19:
	.loc 1 67 1976 is_stmt 1 discriminator 2 view .LVU309
	.loc 1 67 2007 is_stmt 0 discriminator 2 view .LVU310
	movq	-64(%rbp), %rax
.LVL25:
	.loc 1 67 1976 discriminator 2 view .LVU311
	cmpq	%rbx, %rax
	je	.L50
.LVL26:
	.p2align 4,,10
	.p2align 3
.L52:
	.loc 1 67 2059 is_stmt 1 discriminator 6 view .LVU312
	.loc 1 67 2098 discriminator 6 view .LVU313
	.loc 1 67 5 discriminator 6 view .LVU314
	.loc 1 67 10 discriminator 6 view .LVU315
	.loc 1 67 30 is_stmt 0 discriminator 6 view .LVU316
	movq	8(%rax), %rcx
	.loc 1 67 87 discriminator 6 view .LVU317
	movq	(%rax), %rdx
	.loc 1 67 2100 discriminator 6 view .LVU318
	leaq	-104(%rax), %rdi
.LVL27:
	.loc 1 67 64 discriminator 6 view .LVU319
	movq	%rdx, (%rcx)
	.loc 1 67 94 is_stmt 1 discriminator 6 view .LVU320
	.loc 1 67 171 is_stmt 0 discriminator 6 view .LVU321
	movq	8(%rax), %rcx
	.loc 1 67 148 discriminator 6 view .LVU322
	movq	%rcx, 8(%rdx)
	.loc 1 67 186 is_stmt 1 discriminator 6 view .LVU323
	.loc 1 67 191 discriminator 6 view .LVU324
	.loc 1 67 196 discriminator 6 view .LVU325
	.loc 1 67 223 is_stmt 0 discriminator 6 view .LVU326
	movq	%r12, (%rax)
	.loc 1 67 249 is_stmt 1 discriminator 6 view .LVU327
	.loc 1 67 318 is_stmt 0 discriminator 6 view .LVU328
	movq	408(%r13), %rdx
	.loc 1 67 276 discriminator 6 view .LVU329
	movq	%rdx, 8(%rax)
	.loc 1 67 325 is_stmt 1 discriminator 6 view .LVU330
	.loc 1 67 379 is_stmt 0 discriminator 6 view .LVU331
	movq	%rax, (%rdx)
	.loc 1 67 386 is_stmt 1 discriminator 6 view .LVU332
	.loc 1 67 432 is_stmt 0 discriminator 6 view .LVU333
	movq	%rax, 408(%r13)
	.loc 1 67 447 is_stmt 1 discriminator 6 view .LVU334
	.loc 1 67 452 discriminator 6 view .LVU335
	call	*-8(%rax)
.LVL28:
	.loc 1 67 1976 discriminator 6 view .LVU336
	.loc 1 67 2007 is_stmt 0 discriminator 6 view .LVU337
	movq	-64(%rbp), %rax
	.loc 1 67 1976 discriminator 6 view .LVU338
	cmpq	%rbx, %rax
	jne	.L52
.LVL29:
.L50:
	.loc 1 67 470 view .LVU339
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L56
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
.LVL30:
	.loc 1 67 470 view .LVU340
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L56:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
.LVL31:
	.cfi_endproc
.LFE89:
	.size	uv__run_check, .-uv__run_check
	.p2align 4
	.globl	uv__check_close
	.hidden	uv__check_close
	.type	uv__check_close, @function
uv__check_close:
.LVL32:
.LFB90:
	.loc 1 67 513 is_stmt 1 view -0
	.cfi_startproc
	.loc 1 67 513 is_stmt 0 view .LVU342
	endbr64
	.loc 1 67 515 is_stmt 1 view .LVU343
.LVL33:
.LBB24:
.LBI24:
	.loc 1 67 615 view .LVU344
.LBB25:
	.loc 1 67 651 view .LVU345
	.loc 1 67 666 is_stmt 0 view .LVU346
	movl	88(%rdi), %eax
	.loc 1 67 654 view .LVU347
	testb	$4, %al
	je	.L57
	.loc 1 67 711 is_stmt 1 view .LVU348
	.loc 1 67 716 view .LVU349
	.loc 1 67 736 is_stmt 0 view .LVU350
	movq	112(%rdi), %rcx
	.loc 1 67 819 view .LVU351
	movq	104(%rdi), %rdx
	.loc 1 67 783 view .LVU352
	movq	%rdx, (%rcx)
	.loc 1 67 826 is_stmt 1 view .LVU353
	.loc 1 67 929 is_stmt 0 view .LVU354
	movq	112(%rdi), %rcx
	.loc 1 67 893 view .LVU355
	movq	%rcx, 8(%rdx)
	.loc 1 67 944 is_stmt 1 view .LVU356
	.loc 1 67 949 view .LVU357
	.loc 1 67 954 view .LVU358
	.loc 1 67 1008 view .LVU359
	.loc 1 67 1024 is_stmt 0 view .LVU360
	movl	%eax, %edx
	andl	$-5, %edx
	movl	%edx, 88(%rdi)
	.loc 1 67 1046 is_stmt 1 view .LVU361
	.loc 1 67 1049 is_stmt 0 view .LVU362
	testb	$8, %al
	je	.L57
.LVL34:
.LBB26:
.LBI26:
	.loc 1 67 615 is_stmt 1 view .LVU363
.LBB27:
	.loc 1 67 1090 view .LVU364
	.loc 1 67 1095 view .LVU365
	.loc 1 67 1103 is_stmt 0 view .LVU366
	movq	8(%rdi), %rax
	.loc 1 67 1125 view .LVU367
	subl	$1, 8(%rax)
.LVL35:
.L57:
	.loc 1 67 1125 view .LVU368
.LBE27:
.LBE26:
.LBE25:
.LBE24:
	.loc 1 67 538 view .LVU369
	ret
	.cfi_endproc
.LFE90:
	.size	uv__check_close, .-uv__check_close
	.p2align 4
	.globl	uv_idle_init
	.type	uv_idle_init, @function
uv_idle_init:
.LVL36:
.LFB91:
	.loc 1 68 54 is_stmt 1 view -0
	.cfi_startproc
	.loc 1 68 54 is_stmt 0 view .LVU371
	endbr64
	.loc 1 68 56 is_stmt 1 view .LVU372
	.loc 1 68 61 view .LVU373
	.loc 1 68 260 is_stmt 0 view .LVU374
	leaq	16(%rdi), %rax
	.loc 1 68 90 view .LVU375
	movq	%rdi, 8(%rsi)
	.loc 1 68 100 is_stmt 1 view .LVU376
	.loc 1 68 260 is_stmt 0 view .LVU377
	movq	%rax, 32(%rsi)
	.loc 1 68 390 view .LVU378
	movq	24(%rdi), %rdx
	.loc 1 68 490 view .LVU379
	leaq	32(%rsi), %rax
	.loc 1 68 129 view .LVU380
	movl	$6, 16(%rsi)
	.loc 1 68 142 is_stmt 1 view .LVU381
	.loc 1 68 172 is_stmt 0 view .LVU382
	movl	$8, 88(%rsi)
	.loc 1 68 189 is_stmt 1 view .LVU383
	.loc 1 68 194 view .LVU384
	.loc 1 68 284 view .LVU385
	.loc 1 68 347 is_stmt 0 view .LVU386
	movq	%rdx, 40(%rsi)
	.loc 1 68 397 is_stmt 1 view .LVU387
	.loc 1 68 487 is_stmt 0 view .LVU388
	movq	%rax, (%rdx)
	.loc 1 68 530 is_stmt 1 view .LVU389
	.loc 1 68 577 is_stmt 0 view .LVU390
	movq	%rax, 24(%rdi)
	.loc 1 68 628 is_stmt 1 view .LVU391
	.loc 1 68 633 view .LVU392
	.loc 1 68 13 is_stmt 0 view .LVU393
	xorl	%eax, %eax
	.loc 1 68 671 view .LVU394
	movq	$0, 80(%rsi)
	.loc 1 68 12 is_stmt 1 view .LVU395
	.loc 1 68 17 view .LVU396
	.loc 1 68 33 is_stmt 0 view .LVU397
	movq	$0, 96(%rsi)
	.loc 1 68 3 is_stmt 1 view .LVU398
	.loc 1 68 13 is_stmt 0 view .LVU399
	ret
	.cfi_endproc
.LFE91:
	.size	uv_idle_init, .-uv_idle_init
	.p2align 4
	.globl	uv_idle_start
	.type	uv_idle_start, @function
uv_idle_start:
.LVL37:
.LFB92:
	.loc 1 68 67 is_stmt 1 view -0
	.cfi_startproc
	.loc 1 68 67 is_stmt 0 view .LVU401
	endbr64
	.loc 1 68 69 is_stmt 1 view .LVU402
	.loc 1 68 83 is_stmt 0 view .LVU403
	movl	88(%rdi), %eax
	.loc 1 68 72 view .LVU404
	testb	$4, %al
	jne	.L73
	.loc 1 68 128 is_stmt 1 discriminator 2 view .LVU405
	.loc 1 68 131 is_stmt 0 discriminator 2 view .LVU406
	testq	%rsi, %rsi
	je	.L69
	.loc 1 68 21 is_stmt 1 discriminator 4 view .LVU407
	.loc 1 68 26 discriminator 4 view .LVU408
	.loc 1 68 93 is_stmt 0 discriminator 4 view .LVU409
	movq	8(%rdi), %rdx
	.loc 1 68 115 discriminator 4 view .LVU410
	movq	416(%rdx), %rcx
	.loc 1 68 122 is_stmt 1 discriminator 4 view .LVU411
	.loc 1 68 165 is_stmt 0 discriminator 4 view .LVU412
	addq	$416, %rdx
	.loc 1 68 66 discriminator 4 view .LVU413
	movq	%rdx, %xmm1
	.loc 1 68 265 discriminator 4 view .LVU414
	leaq	104(%rdi), %rdx
	.loc 1 68 66 discriminator 4 view .LVU415
	movq	%rcx, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 104(%rdi)
	.loc 1 68 195 is_stmt 1 discriminator 4 view .LVU416
	.loc 1 68 262 is_stmt 0 discriminator 4 view .LVU417
	movq	%rdx, 8(%rcx)
	.loc 1 68 282 is_stmt 1 discriminator 4 view .LVU418
	.loc 1 68 307 is_stmt 0 discriminator 4 view .LVU419
	movq	8(%rdi), %rcx
	.loc 1 68 335 discriminator 4 view .LVU420
	movq	%rdx, 416(%rcx)
	.loc 1 68 363 is_stmt 1 discriminator 4 view .LVU421
	.loc 1 68 368 discriminator 4 view .LVU422
	.loc 1 68 465 is_stmt 0 discriminator 4 view .LVU423
	movl	%eax, %edx
	orl	$4, %edx
	.loc 1 68 384 discriminator 4 view .LVU424
	movq	%rsi, 96(%rdi)
	.loc 1 68 390 is_stmt 1 discriminator 4 view .LVU425
	.loc 1 68 395 discriminator 4 view .LVU426
	.loc 1 68 449 discriminator 4 view .LVU427
	.loc 1 68 465 is_stmt 0 discriminator 4 view .LVU428
	movl	%edx, 88(%rdi)
	.loc 1 68 486 is_stmt 1 discriminator 4 view .LVU429
	.loc 1 68 489 is_stmt 0 discriminator 4 view .LVU430
	testb	$8, %al
	je	.L73
	.loc 1 68 530 is_stmt 1 discriminator 7 view .LVU431
	.loc 1 68 535 discriminator 7 view .LVU432
	.loc 1 68 543 is_stmt 0 discriminator 7 view .LVU433
	movq	8(%rdi), %rax
	.loc 1 68 565 discriminator 7 view .LVU434
	addl	$1, 8(%rax)
.L73:
	.loc 1 68 602 discriminator 7 view .LVU435
	xorl	%eax, %eax
	ret
.L69:
	.loc 1 68 10 view .LVU436
	movl	$-22, %eax
	.loc 1 68 605 view .LVU437
	ret
	.cfi_endproc
.LFE92:
	.size	uv_idle_start, .-uv_idle_start
	.p2align 4
	.globl	uv_idle_stop
	.type	uv_idle_stop, @function
uv_idle_stop:
.LVL38:
.LFB93:
	.loc 1 68 643 is_stmt 1 view -0
	.cfi_startproc
	.loc 1 68 643 is_stmt 0 view .LVU439
	endbr64
	.loc 1 68 645 is_stmt 1 view .LVU440
	.loc 1 68 660 is_stmt 0 view .LVU441
	movl	88(%rdi), %eax
	.loc 1 68 648 view .LVU442
	testb	$4, %al
	je	.L76
	.loc 1 68 705 is_stmt 1 discriminator 2 view .LVU443
	.loc 1 68 710 discriminator 2 view .LVU444
	.loc 1 68 730 is_stmt 0 discriminator 2 view .LVU445
	movq	112(%rdi), %rcx
	.loc 1 68 813 discriminator 2 view .LVU446
	movq	104(%rdi), %rdx
	.loc 1 68 777 discriminator 2 view .LVU447
	movq	%rdx, (%rcx)
	.loc 1 68 820 is_stmt 1 discriminator 2 view .LVU448
	.loc 1 68 923 is_stmt 0 discriminator 2 view .LVU449
	movq	112(%rdi), %rcx
	.loc 1 68 887 discriminator 2 view .LVU450
	movq	%rcx, 8(%rdx)
	.loc 1 68 938 is_stmt 1 discriminator 2 view .LVU451
	.loc 1 68 943 discriminator 2 view .LVU452
	.loc 1 68 948 discriminator 2 view .LVU453
	.loc 1 68 1002 discriminator 2 view .LVU454
	.loc 1 68 1018 is_stmt 0 discriminator 2 view .LVU455
	movl	%eax, %edx
	andl	$-5, %edx
	movl	%edx, 88(%rdi)
	.loc 1 68 1040 is_stmt 1 discriminator 2 view .LVU456
	.loc 1 68 1043 is_stmt 0 discriminator 2 view .LVU457
	testb	$8, %al
	je	.L76
.LVL39:
.LBB30:
.LBI30:
	.loc 1 68 611 is_stmt 1 view .LVU458
.LBB31:
	.loc 1 68 1084 view .LVU459
	.loc 1 68 1089 view .LVU460
	.loc 1 68 1097 is_stmt 0 view .LVU461
	movq	8(%rdi), %rax
	.loc 1 68 1119 view .LVU462
	subl	$1, 8(%rax)
.LVL40:
.L76:
	.loc 1 68 1119 view .LVU463
.LBE31:
.LBE30:
	.loc 1 68 1159 view .LVU464
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE93:
	.size	uv_idle_stop, .-uv_idle_stop
	.p2align 4
	.globl	uv__run_idle
	.hidden	uv__run_idle
	.type	uv__run_idle, @function
uv__run_idle:
.LVL41:
.LFB94:
	.loc 1 68 1196 is_stmt 1 view -0
	.cfi_startproc
	.loc 1 68 1196 is_stmt 0 view .LVU466
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.loc 1 68 1262 view .LVU467
	leaq	416(%rdi), %r12
	.loc 1 68 1196 view .LVU468
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -40
	.loc 1 68 1196 view .LVU469
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	.loc 1 68 1198 is_stmt 1 view .LVU470
	.loc 1 68 1212 view .LVU471
	.loc 1 68 1225 view .LVU472
	.loc 1 68 1235 view .LVU473
	.loc 1 68 1240 view .LVU474
	.loc 1 68 1286 is_stmt 0 view .LVU475
	movq	416(%rdi), %rax
	.loc 1 68 1243 view .LVU476
	cmpq	%rax, %r12
	je	.L81
.LBB32:
	.loc 1 68 1599 discriminator 2 view .LVU477
	movq	424(%rdi), %rdx
	.loc 1 68 1665 discriminator 2 view .LVU478
	leaq	-64(%rbp), %rbx
	movq	%rdi, %r13
	.loc 1 68 1462 is_stmt 1 discriminator 2 view .LVU479
.LVL42:
	.loc 1 68 1521 discriminator 2 view .LVU480
	.loc 1 68 1526 discriminator 2 view .LVU481
	.loc 1 68 1558 is_stmt 0 discriminator 2 view .LVU482
	movq	%rdx, -56(%rbp)
	.loc 1 68 1606 is_stmt 1 discriminator 2 view .LVU483
	.loc 1 68 1665 is_stmt 0 discriminator 2 view .LVU484
	movq	%rbx, (%rdx)
	.loc 1 68 1677 is_stmt 1 discriminator 2 view .LVU485
	.loc 1 68 1784 is_stmt 0 discriminator 2 view .LVU486
	movq	8(%rax), %rdx
	.loc 1 68 1709 discriminator 2 view .LVU487
	movq	%rax, -64(%rbp)
	.loc 1 68 1716 is_stmt 1 discriminator 2 view .LVU488
	.loc 1 68 1761 is_stmt 0 discriminator 2 view .LVU489
	movq	%rdx, 424(%rdi)
	.loc 1 68 1791 is_stmt 1 discriminator 2 view .LVU490
	.loc 1 68 1863 is_stmt 0 discriminator 2 view .LVU491
	movq	%r12, (%rdx)
	.loc 1 68 1888 is_stmt 1 discriminator 2 view .LVU492
	.loc 1 68 1915 is_stmt 0 discriminator 2 view .LVU493
	movq	%rbx, 8(%rax)
.LBE32:
	.loc 1 68 1961 is_stmt 1 discriminator 2 view .LVU494
	.loc 1 68 1992 is_stmt 0 discriminator 2 view .LVU495
	movq	-64(%rbp), %rax
.LVL43:
	.loc 1 68 1961 discriminator 2 view .LVU496
	cmpq	%rbx, %rax
	je	.L81
.LVL44:
	.p2align 4,,10
	.p2align 3
.L83:
	.loc 1 68 2044 is_stmt 1 discriminator 6 view .LVU497
	.loc 1 68 2083 discriminator 6 view .LVU498
	.loc 1 68 5 discriminator 6 view .LVU499
	.loc 1 68 10 discriminator 6 view .LVU500
	.loc 1 68 30 is_stmt 0 discriminator 6 view .LVU501
	movq	8(%rax), %rcx
	.loc 1 68 87 discriminator 6 view .LVU502
	movq	(%rax), %rdx
	.loc 1 68 2085 discriminator 6 view .LVU503
	leaq	-104(%rax), %rdi
.LVL45:
	.loc 1 68 64 discriminator 6 view .LVU504
	movq	%rdx, (%rcx)
	.loc 1 68 94 is_stmt 1 discriminator 6 view .LVU505
	.loc 1 68 171 is_stmt 0 discriminator 6 view .LVU506
	movq	8(%rax), %rcx
	.loc 1 68 148 discriminator 6 view .LVU507
	movq	%rcx, 8(%rdx)
	.loc 1 68 186 is_stmt 1 discriminator 6 view .LVU508
	.loc 1 68 191 discriminator 6 view .LVU509
	.loc 1 68 196 discriminator 6 view .LVU510
	.loc 1 68 223 is_stmt 0 discriminator 6 view .LVU511
	movq	%r12, (%rax)
	.loc 1 68 248 is_stmt 1 discriminator 6 view .LVU512
	.loc 1 68 316 is_stmt 0 discriminator 6 view .LVU513
	movq	424(%r13), %rdx
	.loc 1 68 275 discriminator 6 view .LVU514
	movq	%rdx, 8(%rax)
	.loc 1 68 323 is_stmt 1 discriminator 6 view .LVU515
	.loc 1 68 377 is_stmt 0 discriminator 6 view .LVU516
	movq	%rax, (%rdx)
	.loc 1 68 384 is_stmt 1 discriminator 6 view .LVU517
	.loc 1 68 429 is_stmt 0 discriminator 6 view .LVU518
	movq	%rax, 424(%r13)
	.loc 1 68 444 is_stmt 1 discriminator 6 view .LVU519
	.loc 1 68 449 discriminator 6 view .LVU520
	call	*-8(%rax)
.LVL46:
	.loc 1 68 1961 discriminator 6 view .LVU521
	.loc 1 68 1992 is_stmt 0 discriminator 6 view .LVU522
	movq	-64(%rbp), %rax
	.loc 1 68 1961 discriminator 6 view .LVU523
	cmpq	%rbx, %rax
	jne	.L83
.LVL47:
.L81:
	.loc 1 68 466 view .LVU524
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L87
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
.LVL48:
	.loc 1 68 466 view .LVU525
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L87:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
.LVL49:
	.cfi_endproc
.LFE94:
	.size	uv__run_idle, .-uv__run_idle
	.p2align 4
	.globl	uv__idle_close
	.hidden	uv__idle_close
	.type	uv__idle_close, @function
uv__idle_close:
.LVL50:
.LFB95:
	.loc 1 68 507 is_stmt 1 view -0
	.cfi_startproc
	.loc 1 68 507 is_stmt 0 view .LVU527
	endbr64
	.loc 1 68 509 is_stmt 1 view .LVU528
.LVL51:
.LBB37:
.LBI37:
	.loc 1 68 611 view .LVU529
.LBB38:
	.loc 1 68 645 view .LVU530
	.loc 1 68 660 is_stmt 0 view .LVU531
	movl	88(%rdi), %eax
	.loc 1 68 648 view .LVU532
	testb	$4, %al
	je	.L88
	.loc 1 68 705 is_stmt 1 view .LVU533
	.loc 1 68 710 view .LVU534
	.loc 1 68 730 is_stmt 0 view .LVU535
	movq	112(%rdi), %rcx
	.loc 1 68 813 view .LVU536
	movq	104(%rdi), %rdx
	.loc 1 68 777 view .LVU537
	movq	%rdx, (%rcx)
	.loc 1 68 820 is_stmt 1 view .LVU538
	.loc 1 68 923 is_stmt 0 view .LVU539
	movq	112(%rdi), %rcx
	.loc 1 68 887 view .LVU540
	movq	%rcx, 8(%rdx)
	.loc 1 68 938 is_stmt 1 view .LVU541
	.loc 1 68 943 view .LVU542
	.loc 1 68 948 view .LVU543
	.loc 1 68 1002 view .LVU544
	.loc 1 68 1018 is_stmt 0 view .LVU545
	movl	%eax, %edx
	andl	$-5, %edx
	movl	%edx, 88(%rdi)
	.loc 1 68 1040 is_stmt 1 view .LVU546
	.loc 1 68 1043 is_stmt 0 view .LVU547
	testb	$8, %al
	je	.L88
.LVL52:
.LBB39:
.LBI39:
	.loc 1 68 611 is_stmt 1 view .LVU548
.LBB40:
	.loc 1 68 1084 view .LVU549
	.loc 1 68 1089 view .LVU550
	.loc 1 68 1097 is_stmt 0 view .LVU551
	movq	8(%rdi), %rax
	.loc 1 68 1119 view .LVU552
	subl	$1, 8(%rax)
.LVL53:
.L88:
	.loc 1 68 1119 view .LVU553
.LBE40:
.LBE39:
.LBE38:
.LBE37:
	.loc 1 68 531 view .LVU554
	ret
	.cfi_endproc
.LFE95:
	.size	uv__idle_close, .-uv__idle_close
.Letext0:
	.file 2 "/usr/include/errno.h"
	.file 3 "/usr/lib/gcc/x86_64-linux-gnu/9/include/stddef.h"
	.file 4 "/usr/include/x86_64-linux-gnu/bits/types.h"
	.file 5 "/usr/include/x86_64-linux-gnu/bits/types/struct_FILE.h"
	.file 6 "/usr/include/x86_64-linux-gnu/bits/types/FILE.h"
	.file 7 "/usr/include/stdio.h"
	.file 8 "/usr/include/x86_64-linux-gnu/bits/sys_errlist.h"
	.file 9 "/usr/include/x86_64-linux-gnu/bits/stdint-uintn.h"
	.file 10 "/usr/include/x86_64-linux-gnu/bits/thread-shared-types.h"
	.file 11 "/usr/include/x86_64-linux-gnu/bits/struct_mutex.h"
	.file 12 "/usr/include/x86_64-linux-gnu/bits/struct_rwlock.h"
	.file 13 "/usr/include/x86_64-linux-gnu/bits/pthreadtypes.h"
	.file 14 "/usr/include/x86_64-linux-gnu/bits/sockaddr.h"
	.file 15 "/usr/include/x86_64-linux-gnu/bits/socket.h"
	.file 16 "/usr/include/netinet/in.h"
	.file 17 "/usr/include/signal.h"
	.file 18 "/usr/include/time.h"
	.file 19 "../deps/uv/include/uv.h"
	.file 20 "../deps/uv/include/uv/unix.h"
	.file 21 "../deps/uv/src/queue.h"
	.file 22 "../deps/uv/src/uv-common.h"
	.section	.debug_info,"",@progbits
.Ldebug_info0:
	.long	0x1d4b
	.value	0x4
	.long	.Ldebug_abbrev0
	.byte	0x8
	.uleb128 0x1
	.long	.LASF384
	.byte	0x1
	.long	.LASF385
	.long	.LASF386
	.quad	.Ltext0
	.quad	.Letext0-.Ltext0
	.long	.Ldebug_line0
	.uleb128 0x2
	.long	.LASF0
	.byte	0x2
	.byte	0x2d
	.byte	0xe
	.long	0x39
	.uleb128 0x3
	.byte	0x8
	.long	0x3f
	.uleb128 0x4
	.byte	0x1
	.byte	0x6
	.long	.LASF2
	.uleb128 0x5
	.long	0x3f
	.uleb128 0x2
	.long	.LASF1
	.byte	0x2
	.byte	0x2e
	.byte	0xe
	.long	0x39
	.uleb128 0x6
	.byte	0x4
	.byte	0x5
	.string	"int"
	.uleb128 0x4
	.byte	0x8
	.byte	0x5
	.long	.LASF3
	.uleb128 0x7
	.long	.LASF9
	.byte	0x3
	.byte	0xd1
	.byte	0x1b
	.long	0x71
	.uleb128 0x4
	.byte	0x8
	.byte	0x7
	.long	.LASF4
	.uleb128 0x4
	.byte	0x4
	.byte	0x7
	.long	.LASF5
	.uleb128 0x8
	.byte	0x8
	.uleb128 0x4
	.byte	0x1
	.byte	0x8
	.long	.LASF6
	.uleb128 0x4
	.byte	0x2
	.byte	0x7
	.long	.LASF7
	.uleb128 0x4
	.byte	0x1
	.byte	0x6
	.long	.LASF8
	.uleb128 0x7
	.long	.LASF10
	.byte	0x4
	.byte	0x26
	.byte	0x17
	.long	0x81
	.uleb128 0x4
	.byte	0x2
	.byte	0x5
	.long	.LASF11
	.uleb128 0x7
	.long	.LASF12
	.byte	0x4
	.byte	0x28
	.byte	0x1c
	.long	0x88
	.uleb128 0x7
	.long	.LASF13
	.byte	0x4
	.byte	0x2a
	.byte	0x16
	.long	0x78
	.uleb128 0x7
	.long	.LASF14
	.byte	0x4
	.byte	0x2d
	.byte	0x1b
	.long	0x71
	.uleb128 0x7
	.long	.LASF15
	.byte	0x4
	.byte	0x98
	.byte	0x12
	.long	0x5e
	.uleb128 0x7
	.long	.LASF16
	.byte	0x4
	.byte	0x99
	.byte	0x12
	.long	0x5e
	.uleb128 0x9
	.long	0x57
	.long	0xf5
	.uleb128 0xa
	.long	0x71
	.byte	0x1
	.byte	0
	.uleb128 0xb
	.long	.LASF61
	.byte	0xd8
	.byte	0x5
	.byte	0x31
	.byte	0x8
	.long	0x27c
	.uleb128 0xc
	.long	.LASF17
	.byte	0x5
	.byte	0x33
	.byte	0x7
	.long	0x57
	.byte	0
	.uleb128 0xc
	.long	.LASF18
	.byte	0x5
	.byte	0x36
	.byte	0x9
	.long	0x39
	.byte	0x8
	.uleb128 0xc
	.long	.LASF19
	.byte	0x5
	.byte	0x37
	.byte	0x9
	.long	0x39
	.byte	0x10
	.uleb128 0xc
	.long	.LASF20
	.byte	0x5
	.byte	0x38
	.byte	0x9
	.long	0x39
	.byte	0x18
	.uleb128 0xc
	.long	.LASF21
	.byte	0x5
	.byte	0x39
	.byte	0x9
	.long	0x39
	.byte	0x20
	.uleb128 0xc
	.long	.LASF22
	.byte	0x5
	.byte	0x3a
	.byte	0x9
	.long	0x39
	.byte	0x28
	.uleb128 0xc
	.long	.LASF23
	.byte	0x5
	.byte	0x3b
	.byte	0x9
	.long	0x39
	.byte	0x30
	.uleb128 0xc
	.long	.LASF24
	.byte	0x5
	.byte	0x3c
	.byte	0x9
	.long	0x39
	.byte	0x38
	.uleb128 0xc
	.long	.LASF25
	.byte	0x5
	.byte	0x3d
	.byte	0x9
	.long	0x39
	.byte	0x40
	.uleb128 0xc
	.long	.LASF26
	.byte	0x5
	.byte	0x40
	.byte	0x9
	.long	0x39
	.byte	0x48
	.uleb128 0xc
	.long	.LASF27
	.byte	0x5
	.byte	0x41
	.byte	0x9
	.long	0x39
	.byte	0x50
	.uleb128 0xc
	.long	.LASF28
	.byte	0x5
	.byte	0x42
	.byte	0x9
	.long	0x39
	.byte	0x58
	.uleb128 0xc
	.long	.LASF29
	.byte	0x5
	.byte	0x44
	.byte	0x16
	.long	0x295
	.byte	0x60
	.uleb128 0xc
	.long	.LASF30
	.byte	0x5
	.byte	0x46
	.byte	0x14
	.long	0x29b
	.byte	0x68
	.uleb128 0xc
	.long	.LASF31
	.byte	0x5
	.byte	0x48
	.byte	0x7
	.long	0x57
	.byte	0x70
	.uleb128 0xc
	.long	.LASF32
	.byte	0x5
	.byte	0x49
	.byte	0x7
	.long	0x57
	.byte	0x74
	.uleb128 0xc
	.long	.LASF33
	.byte	0x5
	.byte	0x4a
	.byte	0xb
	.long	0xcd
	.byte	0x78
	.uleb128 0xc
	.long	.LASF34
	.byte	0x5
	.byte	0x4d
	.byte	0x12
	.long	0x88
	.byte	0x80
	.uleb128 0xc
	.long	.LASF35
	.byte	0x5
	.byte	0x4e
	.byte	0xf
	.long	0x8f
	.byte	0x82
	.uleb128 0xc
	.long	.LASF36
	.byte	0x5
	.byte	0x4f
	.byte	0x8
	.long	0x2a1
	.byte	0x83
	.uleb128 0xc
	.long	.LASF37
	.byte	0x5
	.byte	0x51
	.byte	0xf
	.long	0x2b1
	.byte	0x88
	.uleb128 0xc
	.long	.LASF38
	.byte	0x5
	.byte	0x59
	.byte	0xd
	.long	0xd9
	.byte	0x90
	.uleb128 0xc
	.long	.LASF39
	.byte	0x5
	.byte	0x5b
	.byte	0x17
	.long	0x2bc
	.byte	0x98
	.uleb128 0xc
	.long	.LASF40
	.byte	0x5
	.byte	0x5c
	.byte	0x19
	.long	0x2c7
	.byte	0xa0
	.uleb128 0xc
	.long	.LASF41
	.byte	0x5
	.byte	0x5d
	.byte	0x14
	.long	0x29b
	.byte	0xa8
	.uleb128 0xc
	.long	.LASF42
	.byte	0x5
	.byte	0x5e
	.byte	0x9
	.long	0x7f
	.byte	0xb0
	.uleb128 0xc
	.long	.LASF43
	.byte	0x5
	.byte	0x5f
	.byte	0xa
	.long	0x65
	.byte	0xb8
	.uleb128 0xc
	.long	.LASF44
	.byte	0x5
	.byte	0x60
	.byte	0x7
	.long	0x57
	.byte	0xc0
	.uleb128 0xc
	.long	.LASF45
	.byte	0x5
	.byte	0x62
	.byte	0x8
	.long	0x2cd
	.byte	0xc4
	.byte	0
	.uleb128 0x7
	.long	.LASF46
	.byte	0x6
	.byte	0x7
	.byte	0x19
	.long	0xf5
	.uleb128 0xd
	.long	.LASF387
	.byte	0x5
	.byte	0x2b
	.byte	0xe
	.uleb128 0xe
	.long	.LASF47
	.uleb128 0x3
	.byte	0x8
	.long	0x290
	.uleb128 0x3
	.byte	0x8
	.long	0xf5
	.uleb128 0x9
	.long	0x3f
	.long	0x2b1
	.uleb128 0xa
	.long	0x71
	.byte	0
	.byte	0
	.uleb128 0x3
	.byte	0x8
	.long	0x288
	.uleb128 0xe
	.long	.LASF48
	.uleb128 0x3
	.byte	0x8
	.long	0x2b7
	.uleb128 0xe
	.long	.LASF49
	.uleb128 0x3
	.byte	0x8
	.long	0x2c2
	.uleb128 0x9
	.long	0x3f
	.long	0x2dd
	.uleb128 0xa
	.long	0x71
	.byte	0x13
	.byte	0
	.uleb128 0x3
	.byte	0x8
	.long	0x46
	.uleb128 0x5
	.long	0x2dd
	.uleb128 0x2
	.long	.LASF50
	.byte	0x7
	.byte	0x89
	.byte	0xe
	.long	0x2f4
	.uleb128 0x3
	.byte	0x8
	.long	0x27c
	.uleb128 0x2
	.long	.LASF51
	.byte	0x7
	.byte	0x8a
	.byte	0xe
	.long	0x2f4
	.uleb128 0x2
	.long	.LASF52
	.byte	0x7
	.byte	0x8b
	.byte	0xe
	.long	0x2f4
	.uleb128 0x2
	.long	.LASF53
	.byte	0x8
	.byte	0x1a
	.byte	0xc
	.long	0x57
	.uleb128 0x9
	.long	0x2e3
	.long	0x329
	.uleb128 0xf
	.byte	0
	.uleb128 0x5
	.long	0x31e
	.uleb128 0x2
	.long	.LASF54
	.byte	0x8
	.byte	0x1b
	.byte	0x1a
	.long	0x329
	.uleb128 0x2
	.long	.LASF55
	.byte	0x8
	.byte	0x1e
	.byte	0xc
	.long	0x57
	.uleb128 0x2
	.long	.LASF56
	.byte	0x8
	.byte	0x1f
	.byte	0x1a
	.long	0x329
	.uleb128 0x7
	.long	.LASF57
	.byte	0x9
	.byte	0x18
	.byte	0x13
	.long	0x96
	.uleb128 0x7
	.long	.LASF58
	.byte	0x9
	.byte	0x19
	.byte	0x14
	.long	0xa9
	.uleb128 0x7
	.long	.LASF59
	.byte	0x9
	.byte	0x1a
	.byte	0x14
	.long	0xb5
	.uleb128 0x7
	.long	.LASF60
	.byte	0x9
	.byte	0x1b
	.byte	0x14
	.long	0xc1
	.uleb128 0xb
	.long	.LASF62
	.byte	0x10
	.byte	0xa
	.byte	0x31
	.byte	0x10
	.long	0x3aa
	.uleb128 0xc
	.long	.LASF63
	.byte	0xa
	.byte	0x33
	.byte	0x23
	.long	0x3aa
	.byte	0
	.uleb128 0xc
	.long	.LASF64
	.byte	0xa
	.byte	0x34
	.byte	0x23
	.long	0x3aa
	.byte	0x8
	.byte	0
	.uleb128 0x3
	.byte	0x8
	.long	0x382
	.uleb128 0x7
	.long	.LASF65
	.byte	0xa
	.byte	0x35
	.byte	0x3
	.long	0x382
	.uleb128 0xb
	.long	.LASF66
	.byte	0x28
	.byte	0xb
	.byte	0x16
	.byte	0x8
	.long	0x432
	.uleb128 0xc
	.long	.LASF67
	.byte	0xb
	.byte	0x18
	.byte	0x7
	.long	0x57
	.byte	0
	.uleb128 0xc
	.long	.LASF68
	.byte	0xb
	.byte	0x19
	.byte	0x10
	.long	0x78
	.byte	0x4
	.uleb128 0xc
	.long	.LASF69
	.byte	0xb
	.byte	0x1a
	.byte	0x7
	.long	0x57
	.byte	0x8
	.uleb128 0xc
	.long	.LASF70
	.byte	0xb
	.byte	0x1c
	.byte	0x10
	.long	0x78
	.byte	0xc
	.uleb128 0xc
	.long	.LASF71
	.byte	0xb
	.byte	0x20
	.byte	0x7
	.long	0x57
	.byte	0x10
	.uleb128 0xc
	.long	.LASF72
	.byte	0xb
	.byte	0x22
	.byte	0x9
	.long	0xa2
	.byte	0x14
	.uleb128 0xc
	.long	.LASF73
	.byte	0xb
	.byte	0x23
	.byte	0x9
	.long	0xa2
	.byte	0x16
	.uleb128 0xc
	.long	.LASF74
	.byte	0xb
	.byte	0x24
	.byte	0x14
	.long	0x3b0
	.byte	0x18
	.byte	0
	.uleb128 0xb
	.long	.LASF75
	.byte	0x38
	.byte	0xc
	.byte	0x17
	.byte	0x8
	.long	0x4dc
	.uleb128 0xc
	.long	.LASF76
	.byte	0xc
	.byte	0x19
	.byte	0x10
	.long	0x78
	.byte	0
	.uleb128 0xc
	.long	.LASF77
	.byte	0xc
	.byte	0x1a
	.byte	0x10
	.long	0x78
	.byte	0x4
	.uleb128 0xc
	.long	.LASF78
	.byte	0xc
	.byte	0x1b
	.byte	0x10
	.long	0x78
	.byte	0x8
	.uleb128 0xc
	.long	.LASF79
	.byte	0xc
	.byte	0x1c
	.byte	0x10
	.long	0x78
	.byte	0xc
	.uleb128 0xc
	.long	.LASF80
	.byte	0xc
	.byte	0x1d
	.byte	0x10
	.long	0x78
	.byte	0x10
	.uleb128 0xc
	.long	.LASF81
	.byte	0xc
	.byte	0x1e
	.byte	0x10
	.long	0x78
	.byte	0x14
	.uleb128 0xc
	.long	.LASF82
	.byte	0xc
	.byte	0x20
	.byte	0x7
	.long	0x57
	.byte	0x18
	.uleb128 0xc
	.long	.LASF83
	.byte	0xc
	.byte	0x21
	.byte	0x7
	.long	0x57
	.byte	0x1c
	.uleb128 0xc
	.long	.LASF84
	.byte	0xc
	.byte	0x22
	.byte	0xf
	.long	0x8f
	.byte	0x20
	.uleb128 0xc
	.long	.LASF85
	.byte	0xc
	.byte	0x27
	.byte	0x11
	.long	0x4dc
	.byte	0x21
	.uleb128 0xc
	.long	.LASF86
	.byte	0xc
	.byte	0x2a
	.byte	0x15
	.long	0x71
	.byte	0x28
	.uleb128 0xc
	.long	.LASF87
	.byte	0xc
	.byte	0x2d
	.byte	0x10
	.long	0x78
	.byte	0x30
	.byte	0
	.uleb128 0x9
	.long	0x81
	.long	0x4ec
	.uleb128 0xa
	.long	0x71
	.byte	0x6
	.byte	0
	.uleb128 0x4
	.byte	0x8
	.byte	0x7
	.long	.LASF88
	.uleb128 0x9
	.long	0x3f
	.long	0x503
	.uleb128 0xa
	.long	0x71
	.byte	0x37
	.byte	0
	.uleb128 0x10
	.byte	0x28
	.byte	0xd
	.byte	0x43
	.byte	0x9
	.long	0x531
	.uleb128 0x11
	.long	.LASF89
	.byte	0xd
	.byte	0x45
	.byte	0x1c
	.long	0x3bc
	.uleb128 0x11
	.long	.LASF90
	.byte	0xd
	.byte	0x46
	.byte	0x8
	.long	0x531
	.uleb128 0x11
	.long	.LASF91
	.byte	0xd
	.byte	0x47
	.byte	0xc
	.long	0x5e
	.byte	0
	.uleb128 0x9
	.long	0x3f
	.long	0x541
	.uleb128 0xa
	.long	0x71
	.byte	0x27
	.byte	0
	.uleb128 0x7
	.long	.LASF92
	.byte	0xd
	.byte	0x48
	.byte	0x3
	.long	0x503
	.uleb128 0x4
	.byte	0x8
	.byte	0x5
	.long	.LASF93
	.uleb128 0x10
	.byte	0x38
	.byte	0xd
	.byte	0x56
	.byte	0x9
	.long	0x582
	.uleb128 0x11
	.long	.LASF89
	.byte	0xd
	.byte	0x58
	.byte	0x22
	.long	0x432
	.uleb128 0x11
	.long	.LASF90
	.byte	0xd
	.byte	0x59
	.byte	0x8
	.long	0x4f3
	.uleb128 0x11
	.long	.LASF91
	.byte	0xd
	.byte	0x5a
	.byte	0xc
	.long	0x5e
	.byte	0
	.uleb128 0x7
	.long	.LASF94
	.byte	0xd
	.byte	0x5b
	.byte	0x3
	.long	0x554
	.uleb128 0x7
	.long	.LASF95
	.byte	0xe
	.byte	0x1c
	.byte	0x1c
	.long	0x88
	.uleb128 0xb
	.long	.LASF96
	.byte	0x10
	.byte	0xf
	.byte	0xb2
	.byte	0x8
	.long	0x5c2
	.uleb128 0xc
	.long	.LASF97
	.byte	0xf
	.byte	0xb4
	.byte	0x11
	.long	0x58e
	.byte	0
	.uleb128 0xc
	.long	.LASF98
	.byte	0xf
	.byte	0xb5
	.byte	0xa
	.long	0x5c7
	.byte	0x2
	.byte	0
	.uleb128 0x5
	.long	0x59a
	.uleb128 0x9
	.long	0x3f
	.long	0x5d7
	.uleb128 0xa
	.long	0x71
	.byte	0xd
	.byte	0
	.uleb128 0x3
	.byte	0x8
	.long	0x59a
	.uleb128 0x12
	.long	0x5d7
	.uleb128 0xe
	.long	.LASF99
	.uleb128 0x5
	.long	0x5e2
	.uleb128 0x3
	.byte	0x8
	.long	0x5e2
	.uleb128 0x12
	.long	0x5ec
	.uleb128 0xe
	.long	.LASF100
	.uleb128 0x5
	.long	0x5f7
	.uleb128 0x3
	.byte	0x8
	.long	0x5f7
	.uleb128 0x12
	.long	0x601
	.uleb128 0xe
	.long	.LASF101
	.uleb128 0x5
	.long	0x60c
	.uleb128 0x3
	.byte	0x8
	.long	0x60c
	.uleb128 0x12
	.long	0x616
	.uleb128 0xe
	.long	.LASF102
	.uleb128 0x5
	.long	0x621
	.uleb128 0x3
	.byte	0x8
	.long	0x621
	.uleb128 0x12
	.long	0x62b
	.uleb128 0xb
	.long	.LASF103
	.byte	0x10
	.byte	0x10
	.byte	0xee
	.byte	0x8
	.long	0x678
	.uleb128 0xc
	.long	.LASF104
	.byte	0x10
	.byte	0xf0
	.byte	0x11
	.long	0x58e
	.byte	0
	.uleb128 0xc
	.long	.LASF105
	.byte	0x10
	.byte	0xf1
	.byte	0xf
	.long	0x81f
	.byte	0x2
	.uleb128 0xc
	.long	.LASF106
	.byte	0x10
	.byte	0xf2
	.byte	0x14
	.long	0x804
	.byte	0x4
	.uleb128 0xc
	.long	.LASF107
	.byte	0x10
	.byte	0xf5
	.byte	0x13
	.long	0x8c1
	.byte	0x8
	.byte	0
	.uleb128 0x5
	.long	0x636
	.uleb128 0x3
	.byte	0x8
	.long	0x636
	.uleb128 0x12
	.long	0x67d
	.uleb128 0xb
	.long	.LASF108
	.byte	0x1c
	.byte	0x10
	.byte	0xfd
	.byte	0x8
	.long	0x6db
	.uleb128 0xc
	.long	.LASF109
	.byte	0x10
	.byte	0xff
	.byte	0x11
	.long	0x58e
	.byte	0
	.uleb128 0x13
	.long	.LASF110
	.byte	0x10
	.value	0x100
	.byte	0xf
	.long	0x81f
	.byte	0x2
	.uleb128 0x13
	.long	.LASF111
	.byte	0x10
	.value	0x101
	.byte	0xe
	.long	0x36a
	.byte	0x4
	.uleb128 0x13
	.long	.LASF112
	.byte	0x10
	.value	0x102
	.byte	0x15
	.long	0x889
	.byte	0x8
	.uleb128 0x13
	.long	.LASF113
	.byte	0x10
	.value	0x103
	.byte	0xe
	.long	0x36a
	.byte	0x18
	.byte	0
	.uleb128 0x5
	.long	0x688
	.uleb128 0x3
	.byte	0x8
	.long	0x688
	.uleb128 0x12
	.long	0x6e0
	.uleb128 0xe
	.long	.LASF114
	.uleb128 0x5
	.long	0x6eb
	.uleb128 0x3
	.byte	0x8
	.long	0x6eb
	.uleb128 0x12
	.long	0x6f5
	.uleb128 0xe
	.long	.LASF115
	.uleb128 0x5
	.long	0x700
	.uleb128 0x3
	.byte	0x8
	.long	0x700
	.uleb128 0x12
	.long	0x70a
	.uleb128 0xe
	.long	.LASF116
	.uleb128 0x5
	.long	0x715
	.uleb128 0x3
	.byte	0x8
	.long	0x715
	.uleb128 0x12
	.long	0x71f
	.uleb128 0xe
	.long	.LASF117
	.uleb128 0x5
	.long	0x72a
	.uleb128 0x3
	.byte	0x8
	.long	0x72a
	.uleb128 0x12
	.long	0x734
	.uleb128 0xe
	.long	.LASF118
	.uleb128 0x5
	.long	0x73f
	.uleb128 0x3
	.byte	0x8
	.long	0x73f
	.uleb128 0x12
	.long	0x749
	.uleb128 0xe
	.long	.LASF119
	.uleb128 0x5
	.long	0x754
	.uleb128 0x3
	.byte	0x8
	.long	0x754
	.uleb128 0x12
	.long	0x75e
	.uleb128 0x3
	.byte	0x8
	.long	0x5c2
	.uleb128 0x12
	.long	0x769
	.uleb128 0x3
	.byte	0x8
	.long	0x5e7
	.uleb128 0x12
	.long	0x774
	.uleb128 0x3
	.byte	0x8
	.long	0x5fc
	.uleb128 0x12
	.long	0x77f
	.uleb128 0x3
	.byte	0x8
	.long	0x611
	.uleb128 0x12
	.long	0x78a
	.uleb128 0x3
	.byte	0x8
	.long	0x626
	.uleb128 0x12
	.long	0x795
	.uleb128 0x3
	.byte	0x8
	.long	0x678
	.uleb128 0x12
	.long	0x7a0
	.uleb128 0x3
	.byte	0x8
	.long	0x6db
	.uleb128 0x12
	.long	0x7ab
	.uleb128 0x3
	.byte	0x8
	.long	0x6f0
	.uleb128 0x12
	.long	0x7b6
	.uleb128 0x3
	.byte	0x8
	.long	0x705
	.uleb128 0x12
	.long	0x7c1
	.uleb128 0x3
	.byte	0x8
	.long	0x71a
	.uleb128 0x12
	.long	0x7cc
	.uleb128 0x3
	.byte	0x8
	.long	0x72f
	.uleb128 0x12
	.long	0x7d7
	.uleb128 0x3
	.byte	0x8
	.long	0x744
	.uleb128 0x12
	.long	0x7e2
	.uleb128 0x3
	.byte	0x8
	.long	0x759
	.uleb128 0x12
	.long	0x7ed
	.uleb128 0x7
	.long	.LASF120
	.byte	0x10
	.byte	0x1e
	.byte	0x12
	.long	0x36a
	.uleb128 0xb
	.long	.LASF121
	.byte	0x4
	.byte	0x10
	.byte	0x1f
	.byte	0x8
	.long	0x81f
	.uleb128 0xc
	.long	.LASF122
	.byte	0x10
	.byte	0x21
	.byte	0xf
	.long	0x7f8
	.byte	0
	.byte	0
	.uleb128 0x7
	.long	.LASF123
	.byte	0x10
	.byte	0x77
	.byte	0x12
	.long	0x35e
	.uleb128 0x10
	.byte	0x10
	.byte	0x10
	.byte	0xd6
	.byte	0x5
	.long	0x859
	.uleb128 0x11
	.long	.LASF124
	.byte	0x10
	.byte	0xd8
	.byte	0xa
	.long	0x859
	.uleb128 0x11
	.long	.LASF125
	.byte	0x10
	.byte	0xd9
	.byte	0xb
	.long	0x869
	.uleb128 0x11
	.long	.LASF126
	.byte	0x10
	.byte	0xda
	.byte	0xb
	.long	0x879
	.byte	0
	.uleb128 0x9
	.long	0x352
	.long	0x869
	.uleb128 0xa
	.long	0x71
	.byte	0xf
	.byte	0
	.uleb128 0x9
	.long	0x35e
	.long	0x879
	.uleb128 0xa
	.long	0x71
	.byte	0x7
	.byte	0
	.uleb128 0x9
	.long	0x36a
	.long	0x889
	.uleb128 0xa
	.long	0x71
	.byte	0x3
	.byte	0
	.uleb128 0xb
	.long	.LASF127
	.byte	0x10
	.byte	0x10
	.byte	0xd4
	.byte	0x8
	.long	0x8a4
	.uleb128 0xc
	.long	.LASF128
	.byte	0x10
	.byte	0xdb
	.byte	0x9
	.long	0x82b
	.byte	0
	.byte	0
	.uleb128 0x5
	.long	0x889
	.uleb128 0x2
	.long	.LASF129
	.byte	0x10
	.byte	0xe4
	.byte	0x1e
	.long	0x8a4
	.uleb128 0x2
	.long	.LASF130
	.byte	0x10
	.byte	0xe5
	.byte	0x1e
	.long	0x8a4
	.uleb128 0x9
	.long	0x81
	.long	0x8d1
	.uleb128 0xa
	.long	0x71
	.byte	0x7
	.byte	0
	.uleb128 0x14
	.uleb128 0x3
	.byte	0x8
	.long	0x8d1
	.uleb128 0x9
	.long	0x2e3
	.long	0x8e8
	.uleb128 0xa
	.long	0x71
	.byte	0x40
	.byte	0
	.uleb128 0x5
	.long	0x8d8
	.uleb128 0x15
	.long	.LASF131
	.byte	0x11
	.value	0x11e
	.byte	0x1a
	.long	0x8e8
	.uleb128 0x15
	.long	.LASF132
	.byte	0x11
	.value	0x11f
	.byte	0x1a
	.long	0x8e8
	.uleb128 0x9
	.long	0x39
	.long	0x917
	.uleb128 0xa
	.long	0x71
	.byte	0x1
	.byte	0
	.uleb128 0x2
	.long	.LASF133
	.byte	0x12
	.byte	0x9f
	.byte	0xe
	.long	0x907
	.uleb128 0x2
	.long	.LASF134
	.byte	0x12
	.byte	0xa0
	.byte	0xc
	.long	0x57
	.uleb128 0x2
	.long	.LASF135
	.byte	0x12
	.byte	0xa1
	.byte	0x11
	.long	0x5e
	.uleb128 0x2
	.long	.LASF136
	.byte	0x12
	.byte	0xa6
	.byte	0xe
	.long	0x907
	.uleb128 0x2
	.long	.LASF137
	.byte	0x12
	.byte	0xae
	.byte	0xc
	.long	0x57
	.uleb128 0x2
	.long	.LASF138
	.byte	0x12
	.byte	0xaf
	.byte	0x11
	.long	0x5e
	.uleb128 0x15
	.long	.LASF139
	.byte	0x12
	.value	0x112
	.byte	0xc
	.long	0x57
	.uleb128 0x9
	.long	0x7f
	.long	0x97c
	.uleb128 0xa
	.long	0x71
	.byte	0x3
	.byte	0
	.uleb128 0x16
	.long	.LASF140
	.value	0x350
	.byte	0x13
	.value	0x6ea
	.byte	0x8
	.long	0xb9b
	.uleb128 0x13
	.long	.LASF141
	.byte	0x13
	.value	0x6ec
	.byte	0x9
	.long	0x7f
	.byte	0
	.uleb128 0x13
	.long	.LASF142
	.byte	0x13
	.value	0x6ee
	.byte	0x10
	.long	0x78
	.byte	0x8
	.uleb128 0x13
	.long	.LASF143
	.byte	0x13
	.value	0x6ef
	.byte	0x9
	.long	0xba1
	.byte	0x10
	.uleb128 0x13
	.long	.LASF144
	.byte	0x13
	.value	0x6f3
	.byte	0x5
	.long	0x151d
	.byte	0x20
	.uleb128 0x13
	.long	.LASF145
	.byte	0x13
	.value	0x6f5
	.byte	0x10
	.long	0x78
	.byte	0x30
	.uleb128 0x13
	.long	.LASF146
	.byte	0x13
	.value	0x6f6
	.byte	0x11
	.long	0x71
	.byte	0x38
	.uleb128 0x13
	.long	.LASF147
	.byte	0x13
	.value	0x6f6
	.byte	0x1c
	.long	0x57
	.byte	0x40
	.uleb128 0x13
	.long	.LASF148
	.byte	0x13
	.value	0x6f6
	.byte	0x2e
	.long	0xba1
	.byte	0x48
	.uleb128 0x13
	.long	.LASF149
	.byte	0x13
	.value	0x6f6
	.byte	0x46
	.long	0xba1
	.byte	0x58
	.uleb128 0x13
	.long	.LASF150
	.byte	0x13
	.value	0x6f6
	.byte	0x63
	.long	0x156c
	.byte	0x68
	.uleb128 0x13
	.long	.LASF151
	.byte	0x13
	.value	0x6f6
	.byte	0x7a
	.long	0x78
	.byte	0x70
	.uleb128 0x13
	.long	.LASF152
	.byte	0x13
	.value	0x6f6
	.byte	0x92
	.long	0x78
	.byte	0x74
	.uleb128 0x17
	.string	"wq"
	.byte	0x13
	.value	0x6f6
	.byte	0x9e
	.long	0xba1
	.byte	0x78
	.uleb128 0x13
	.long	.LASF153
	.byte	0x13
	.value	0x6f6
	.byte	0xb0
	.long	0xc44
	.byte	0x88
	.uleb128 0x13
	.long	.LASF154
	.byte	0x13
	.value	0x6f6
	.byte	0xc5
	.long	0x1190
	.byte	0xb0
	.uleb128 0x18
	.long	.LASF155
	.byte	0x13
	.value	0x6f6
	.byte	0xdb
	.long	0xc50
	.value	0x130
	.uleb128 0x18
	.long	.LASF156
	.byte	0x13
	.value	0x6f6
	.byte	0xf6
	.long	0x1312
	.value	0x168
	.uleb128 0x19
	.long	.LASF157
	.byte	0x13
	.value	0x6f6
	.value	0x10d
	.long	0xba1
	.value	0x170
	.uleb128 0x19
	.long	.LASF158
	.byte	0x13
	.value	0x6f6
	.value	0x127
	.long	0xba1
	.value	0x180
	.uleb128 0x19
	.long	.LASF159
	.byte	0x13
	.value	0x6f6
	.value	0x141
	.long	0xba1
	.value	0x190
	.uleb128 0x19
	.long	.LASF160
	.byte	0x13
	.value	0x6f6
	.value	0x159
	.long	0xba1
	.value	0x1a0
	.uleb128 0x19
	.long	.LASF161
	.byte	0x13
	.value	0x6f6
	.value	0x170
	.long	0xba1
	.value	0x1b0
	.uleb128 0x19
	.long	.LASF162
	.byte	0x13
	.value	0x6f6
	.value	0x189
	.long	0x8d2
	.value	0x1c0
	.uleb128 0x19
	.long	.LASF163
	.byte	0x13
	.value	0x6f6
	.value	0x1a7
	.long	0xc38
	.value	0x1c8
	.uleb128 0x19
	.long	.LASF164
	.byte	0x13
	.value	0x6f6
	.value	0x1bd
	.long	0x57
	.value	0x200
	.uleb128 0x19
	.long	.LASF165
	.byte	0x13
	.value	0x6f6
	.value	0x1f2
	.long	0x1542
	.value	0x208
	.uleb128 0x19
	.long	.LASF166
	.byte	0x13
	.value	0x6f6
	.value	0x207
	.long	0x376
	.value	0x218
	.uleb128 0x19
	.long	.LASF167
	.byte	0x13
	.value	0x6f6
	.value	0x21f
	.long	0x376
	.value	0x220
	.uleb128 0x19
	.long	.LASF168
	.byte	0x13
	.value	0x6f6
	.value	0x229
	.long	0xe5
	.value	0x228
	.uleb128 0x19
	.long	.LASF169
	.byte	0x13
	.value	0x6f6
	.value	0x244
	.long	0xc38
	.value	0x230
	.uleb128 0x19
	.long	.LASF170
	.byte	0x13
	.value	0x6f6
	.value	0x263
	.long	0x1243
	.value	0x268
	.uleb128 0x19
	.long	.LASF171
	.byte	0x13
	.value	0x6f6
	.value	0x276
	.long	0x57
	.value	0x300
	.uleb128 0x19
	.long	.LASF172
	.byte	0x13
	.value	0x6f6
	.value	0x28a
	.long	0xc38
	.value	0x308
	.uleb128 0x19
	.long	.LASF173
	.byte	0x13
	.value	0x6f6
	.value	0x2a6
	.long	0x7f
	.value	0x340
	.uleb128 0x19
	.long	.LASF174
	.byte	0x13
	.value	0x6f6
	.value	0x2bc
	.long	0x57
	.value	0x348
	.byte	0
	.uleb128 0x3
	.byte	0x8
	.long	0x97c
	.uleb128 0x9
	.long	0x7f
	.long	0xbb1
	.uleb128 0xa
	.long	0x71
	.byte	0x1
	.byte	0
	.uleb128 0x7
	.long	.LASF175
	.byte	0x14
	.byte	0x59
	.byte	0x10
	.long	0xbbd
	.uleb128 0x3
	.byte	0x8
	.long	0xbc3
	.uleb128 0x1a
	.long	0xbd8
	.uleb128 0x1b
	.long	0xb9b
	.uleb128 0x1b
	.long	0xbd8
	.uleb128 0x1b
	.long	0x78
	.byte	0
	.uleb128 0x3
	.byte	0x8
	.long	0xbde
	.uleb128 0xb
	.long	.LASF176
	.byte	0x38
	.byte	0x14
	.byte	0x5e
	.byte	0x8
	.long	0xc38
	.uleb128 0x1c
	.string	"cb"
	.byte	0x14
	.byte	0x5f
	.byte	0xd
	.long	0xbb1
	.byte	0
	.uleb128 0xc
	.long	.LASF148
	.byte	0x14
	.byte	0x60
	.byte	0x9
	.long	0xba1
	.byte	0x8
	.uleb128 0xc
	.long	.LASF149
	.byte	0x14
	.byte	0x61
	.byte	0x9
	.long	0xba1
	.byte	0x18
	.uleb128 0xc
	.long	.LASF177
	.byte	0x14
	.byte	0x62
	.byte	0x10
	.long	0x78
	.byte	0x28
	.uleb128 0xc
	.long	.LASF178
	.byte	0x14
	.byte	0x63
	.byte	0x10
	.long	0x78
	.byte	0x2c
	.uleb128 0x1c
	.string	"fd"
	.byte	0x14
	.byte	0x64
	.byte	0x7
	.long	0x57
	.byte	0x30
	.byte	0
	.uleb128 0x7
	.long	.LASF179
	.byte	0x14
	.byte	0x5c
	.byte	0x19
	.long	0xbde
	.uleb128 0x7
	.long	.LASF180
	.byte	0x14
	.byte	0x87
	.byte	0x19
	.long	0x541
	.uleb128 0x7
	.long	.LASF181
	.byte	0x14
	.byte	0x88
	.byte	0x1a
	.long	0x582
	.uleb128 0x1d
	.byte	0x5
	.byte	0x4
	.long	0x57
	.byte	0x13
	.byte	0xb6
	.byte	0xe
	.long	0xe7f
	.uleb128 0x1e
	.long	.LASF182
	.sleb128 -7
	.uleb128 0x1e
	.long	.LASF183
	.sleb128 -13
	.uleb128 0x1e
	.long	.LASF184
	.sleb128 -98
	.uleb128 0x1e
	.long	.LASF185
	.sleb128 -99
	.uleb128 0x1e
	.long	.LASF186
	.sleb128 -97
	.uleb128 0x1e
	.long	.LASF187
	.sleb128 -11
	.uleb128 0x1e
	.long	.LASF188
	.sleb128 -3000
	.uleb128 0x1e
	.long	.LASF189
	.sleb128 -3001
	.uleb128 0x1e
	.long	.LASF190
	.sleb128 -3002
	.uleb128 0x1e
	.long	.LASF191
	.sleb128 -3013
	.uleb128 0x1e
	.long	.LASF192
	.sleb128 -3003
	.uleb128 0x1e
	.long	.LASF193
	.sleb128 -3004
	.uleb128 0x1e
	.long	.LASF194
	.sleb128 -3005
	.uleb128 0x1e
	.long	.LASF195
	.sleb128 -3006
	.uleb128 0x1e
	.long	.LASF196
	.sleb128 -3007
	.uleb128 0x1e
	.long	.LASF197
	.sleb128 -3008
	.uleb128 0x1e
	.long	.LASF198
	.sleb128 -3009
	.uleb128 0x1e
	.long	.LASF199
	.sleb128 -3014
	.uleb128 0x1e
	.long	.LASF200
	.sleb128 -3010
	.uleb128 0x1e
	.long	.LASF201
	.sleb128 -3011
	.uleb128 0x1e
	.long	.LASF202
	.sleb128 -114
	.uleb128 0x1e
	.long	.LASF203
	.sleb128 -9
	.uleb128 0x1e
	.long	.LASF204
	.sleb128 -16
	.uleb128 0x1e
	.long	.LASF205
	.sleb128 -125
	.uleb128 0x1e
	.long	.LASF206
	.sleb128 -4080
	.uleb128 0x1e
	.long	.LASF207
	.sleb128 -103
	.uleb128 0x1e
	.long	.LASF208
	.sleb128 -111
	.uleb128 0x1e
	.long	.LASF209
	.sleb128 -104
	.uleb128 0x1e
	.long	.LASF210
	.sleb128 -89
	.uleb128 0x1e
	.long	.LASF211
	.sleb128 -17
	.uleb128 0x1e
	.long	.LASF212
	.sleb128 -14
	.uleb128 0x1e
	.long	.LASF213
	.sleb128 -27
	.uleb128 0x1e
	.long	.LASF214
	.sleb128 -113
	.uleb128 0x1e
	.long	.LASF215
	.sleb128 -4
	.uleb128 0x1e
	.long	.LASF216
	.sleb128 -22
	.uleb128 0x1e
	.long	.LASF217
	.sleb128 -5
	.uleb128 0x1e
	.long	.LASF218
	.sleb128 -106
	.uleb128 0x1e
	.long	.LASF219
	.sleb128 -21
	.uleb128 0x1e
	.long	.LASF220
	.sleb128 -40
	.uleb128 0x1e
	.long	.LASF221
	.sleb128 -24
	.uleb128 0x1e
	.long	.LASF222
	.sleb128 -90
	.uleb128 0x1e
	.long	.LASF223
	.sleb128 -36
	.uleb128 0x1e
	.long	.LASF224
	.sleb128 -100
	.uleb128 0x1e
	.long	.LASF225
	.sleb128 -101
	.uleb128 0x1e
	.long	.LASF226
	.sleb128 -23
	.uleb128 0x1e
	.long	.LASF227
	.sleb128 -105
	.uleb128 0x1e
	.long	.LASF228
	.sleb128 -19
	.uleb128 0x1e
	.long	.LASF229
	.sleb128 -2
	.uleb128 0x1e
	.long	.LASF230
	.sleb128 -12
	.uleb128 0x1e
	.long	.LASF231
	.sleb128 -64
	.uleb128 0x1e
	.long	.LASF232
	.sleb128 -92
	.uleb128 0x1e
	.long	.LASF233
	.sleb128 -28
	.uleb128 0x1e
	.long	.LASF234
	.sleb128 -38
	.uleb128 0x1e
	.long	.LASF235
	.sleb128 -107
	.uleb128 0x1e
	.long	.LASF236
	.sleb128 -20
	.uleb128 0x1e
	.long	.LASF237
	.sleb128 -39
	.uleb128 0x1e
	.long	.LASF238
	.sleb128 -88
	.uleb128 0x1e
	.long	.LASF239
	.sleb128 -95
	.uleb128 0x1e
	.long	.LASF240
	.sleb128 -1
	.uleb128 0x1e
	.long	.LASF241
	.sleb128 -32
	.uleb128 0x1e
	.long	.LASF242
	.sleb128 -71
	.uleb128 0x1e
	.long	.LASF243
	.sleb128 -93
	.uleb128 0x1e
	.long	.LASF244
	.sleb128 -91
	.uleb128 0x1e
	.long	.LASF245
	.sleb128 -34
	.uleb128 0x1e
	.long	.LASF246
	.sleb128 -30
	.uleb128 0x1e
	.long	.LASF247
	.sleb128 -108
	.uleb128 0x1e
	.long	.LASF248
	.sleb128 -29
	.uleb128 0x1e
	.long	.LASF249
	.sleb128 -3
	.uleb128 0x1e
	.long	.LASF250
	.sleb128 -110
	.uleb128 0x1e
	.long	.LASF251
	.sleb128 -26
	.uleb128 0x1e
	.long	.LASF252
	.sleb128 -18
	.uleb128 0x1e
	.long	.LASF253
	.sleb128 -4094
	.uleb128 0x1e
	.long	.LASF254
	.sleb128 -4095
	.uleb128 0x1e
	.long	.LASF255
	.sleb128 -6
	.uleb128 0x1e
	.long	.LASF256
	.sleb128 -31
	.uleb128 0x1e
	.long	.LASF257
	.sleb128 -112
	.uleb128 0x1e
	.long	.LASF258
	.sleb128 -121
	.uleb128 0x1e
	.long	.LASF259
	.sleb128 -25
	.uleb128 0x1e
	.long	.LASF260
	.sleb128 -4028
	.uleb128 0x1e
	.long	.LASF261
	.sleb128 -84
	.uleb128 0x1e
	.long	.LASF262
	.sleb128 -4096
	.byte	0
	.uleb128 0x1d
	.byte	0x7
	.byte	0x4
	.long	0x78
	.byte	0x13
	.byte	0xbd
	.byte	0xe
	.long	0xf00
	.uleb128 0x1f
	.long	.LASF263
	.byte	0
	.uleb128 0x1f
	.long	.LASF264
	.byte	0x1
	.uleb128 0x1f
	.long	.LASF265
	.byte	0x2
	.uleb128 0x1f
	.long	.LASF266
	.byte	0x3
	.uleb128 0x1f
	.long	.LASF267
	.byte	0x4
	.uleb128 0x1f
	.long	.LASF268
	.byte	0x5
	.uleb128 0x1f
	.long	.LASF269
	.byte	0x6
	.uleb128 0x1f
	.long	.LASF270
	.byte	0x7
	.uleb128 0x1f
	.long	.LASF271
	.byte	0x8
	.uleb128 0x1f
	.long	.LASF272
	.byte	0x9
	.uleb128 0x1f
	.long	.LASF273
	.byte	0xa
	.uleb128 0x1f
	.long	.LASF274
	.byte	0xb
	.uleb128 0x1f
	.long	.LASF275
	.byte	0xc
	.uleb128 0x1f
	.long	.LASF276
	.byte	0xd
	.uleb128 0x1f
	.long	.LASF277
	.byte	0xe
	.uleb128 0x1f
	.long	.LASF278
	.byte	0xf
	.uleb128 0x1f
	.long	.LASF279
	.byte	0x10
	.uleb128 0x1f
	.long	.LASF280
	.byte	0x11
	.uleb128 0x1f
	.long	.LASF281
	.byte	0x12
	.byte	0
	.uleb128 0x7
	.long	.LASF282
	.byte	0x13
	.byte	0xc4
	.byte	0x3
	.long	0xe7f
	.uleb128 0x7
	.long	.LASF283
	.byte	0x13
	.byte	0xd1
	.byte	0x1a
	.long	0x97c
	.uleb128 0x7
	.long	.LASF284
	.byte	0x13
	.byte	0xd2
	.byte	0x1c
	.long	0xf24
	.uleb128 0x20
	.long	.LASF285
	.byte	0x60
	.byte	0x13
	.value	0x1bb
	.byte	0x8
	.long	0xfa1
	.uleb128 0x13
	.long	.LASF141
	.byte	0x13
	.value	0x1bc
	.byte	0x9
	.long	0x7f
	.byte	0
	.uleb128 0x13
	.long	.LASF286
	.byte	0x13
	.value	0x1bc
	.byte	0x1a
	.long	0x1413
	.byte	0x8
	.uleb128 0x13
	.long	.LASF287
	.byte	0x13
	.value	0x1bc
	.byte	0x2f
	.long	0xf00
	.byte	0x10
	.uleb128 0x13
	.long	.LASF288
	.byte	0x13
	.value	0x1bc
	.byte	0x41
	.long	0x1318
	.byte	0x18
	.uleb128 0x13
	.long	.LASF143
	.byte	0x13
	.value	0x1bc
	.byte	0x51
	.long	0xba1
	.byte	0x20
	.uleb128 0x17
	.string	"u"
	.byte	0x13
	.value	0x1bc
	.byte	0x87
	.long	0x13ef
	.byte	0x30
	.uleb128 0x13
	.long	.LASF289
	.byte	0x13
	.value	0x1bc
	.byte	0x97
	.long	0x1312
	.byte	0x50
	.uleb128 0x13
	.long	.LASF146
	.byte	0x13
	.value	0x1bc
	.byte	0xb2
	.long	0x78
	.byte	0x58
	.byte	0
	.uleb128 0x7
	.long	.LASF290
	.byte	0x13
	.byte	0xdb
	.byte	0x1d
	.long	0xfad
	.uleb128 0x20
	.long	.LASF291
	.byte	0x78
	.byte	0x13
	.value	0x326
	.byte	0x8
	.long	0x1046
	.uleb128 0x13
	.long	.LASF141
	.byte	0x13
	.value	0x327
	.byte	0x9
	.long	0x7f
	.byte	0
	.uleb128 0x13
	.long	.LASF286
	.byte	0x13
	.value	0x327
	.byte	0x1a
	.long	0x1413
	.byte	0x8
	.uleb128 0x13
	.long	.LASF287
	.byte	0x13
	.value	0x327
	.byte	0x2f
	.long	0xf00
	.byte	0x10
	.uleb128 0x13
	.long	.LASF288
	.byte	0x13
	.value	0x327
	.byte	0x41
	.long	0x1318
	.byte	0x18
	.uleb128 0x13
	.long	.LASF143
	.byte	0x13
	.value	0x327
	.byte	0x51
	.long	0xba1
	.byte	0x20
	.uleb128 0x17
	.string	"u"
	.byte	0x13
	.value	0x327
	.byte	0x87
	.long	0x1419
	.byte	0x30
	.uleb128 0x13
	.long	.LASF289
	.byte	0x13
	.value	0x327
	.byte	0x97
	.long	0x1312
	.byte	0x50
	.uleb128 0x13
	.long	.LASF146
	.byte	0x13
	.value	0x327
	.byte	0xb2
	.long	0x78
	.byte	0x58
	.uleb128 0x13
	.long	.LASF292
	.byte	0x13
	.value	0x328
	.byte	0x11
	.long	0x135a
	.byte	0x60
	.uleb128 0x13
	.long	.LASF293
	.byte	0x13
	.value	0x328
	.byte	0x23
	.long	0xba1
	.byte	0x68
	.byte	0
	.uleb128 0x7
	.long	.LASF294
	.byte	0x13
	.byte	0xdc
	.byte	0x1b
	.long	0x1052
	.uleb128 0x20
	.long	.LASF295
	.byte	0x78
	.byte	0x13
	.value	0x330
	.byte	0x8
	.long	0x10eb
	.uleb128 0x13
	.long	.LASF141
	.byte	0x13
	.value	0x331
	.byte	0x9
	.long	0x7f
	.byte	0
	.uleb128 0x13
	.long	.LASF286
	.byte	0x13
	.value	0x331
	.byte	0x1a
	.long	0x1413
	.byte	0x8
	.uleb128 0x13
	.long	.LASF287
	.byte	0x13
	.value	0x331
	.byte	0x2f
	.long	0xf00
	.byte	0x10
	.uleb128 0x13
	.long	.LASF288
	.byte	0x13
	.value	0x331
	.byte	0x41
	.long	0x1318
	.byte	0x18
	.uleb128 0x13
	.long	.LASF143
	.byte	0x13
	.value	0x331
	.byte	0x51
	.long	0xba1
	.byte	0x20
	.uleb128 0x17
	.string	"u"
	.byte	0x13
	.value	0x331
	.byte	0x87
	.long	0x143d
	.byte	0x30
	.uleb128 0x13
	.long	.LASF289
	.byte	0x13
	.value	0x331
	.byte	0x97
	.long	0x1312
	.byte	0x50
	.uleb128 0x13
	.long	.LASF146
	.byte	0x13
	.value	0x331
	.byte	0xb2
	.long	0x78
	.byte	0x58
	.uleb128 0x13
	.long	.LASF296
	.byte	0x13
	.value	0x332
	.byte	0xf
	.long	0x137e
	.byte	0x60
	.uleb128 0x13
	.long	.LASF293
	.byte	0x13
	.value	0x332
	.byte	0x1f
	.long	0xba1
	.byte	0x68
	.byte	0
	.uleb128 0x7
	.long	.LASF297
	.byte	0x13
	.byte	0xdd
	.byte	0x1a
	.long	0x10f7
	.uleb128 0x20
	.long	.LASF298
	.byte	0x78
	.byte	0x13
	.value	0x33a
	.byte	0x8
	.long	0x1190
	.uleb128 0x13
	.long	.LASF141
	.byte	0x13
	.value	0x33b
	.byte	0x9
	.long	0x7f
	.byte	0
	.uleb128 0x13
	.long	.LASF286
	.byte	0x13
	.value	0x33b
	.byte	0x1a
	.long	0x1413
	.byte	0x8
	.uleb128 0x13
	.long	.LASF287
	.byte	0x13
	.value	0x33b
	.byte	0x2f
	.long	0xf00
	.byte	0x10
	.uleb128 0x13
	.long	.LASF288
	.byte	0x13
	.value	0x33b
	.byte	0x41
	.long	0x1318
	.byte	0x18
	.uleb128 0x13
	.long	.LASF143
	.byte	0x13
	.value	0x33b
	.byte	0x51
	.long	0xba1
	.byte	0x20
	.uleb128 0x17
	.string	"u"
	.byte	0x13
	.value	0x33b
	.byte	0x87
	.long	0x1461
	.byte	0x30
	.uleb128 0x13
	.long	.LASF289
	.byte	0x13
	.value	0x33b
	.byte	0x97
	.long	0x1312
	.byte	0x50
	.uleb128 0x13
	.long	.LASF146
	.byte	0x13
	.value	0x33b
	.byte	0xb2
	.long	0x78
	.byte	0x58
	.uleb128 0x13
	.long	.LASF299
	.byte	0x13
	.value	0x33c
	.byte	0xe
	.long	0x13a2
	.byte	0x60
	.uleb128 0x13
	.long	.LASF293
	.byte	0x13
	.value	0x33c
	.byte	0x1d
	.long	0xba1
	.byte	0x68
	.byte	0
	.uleb128 0x7
	.long	.LASF300
	.byte	0x13
	.byte	0xde
	.byte	0x1b
	.long	0x119c
	.uleb128 0x20
	.long	.LASF301
	.byte	0x80
	.byte	0x13
	.value	0x344
	.byte	0x8
	.long	0x1243
	.uleb128 0x13
	.long	.LASF141
	.byte	0x13
	.value	0x345
	.byte	0x9
	.long	0x7f
	.byte	0
	.uleb128 0x13
	.long	.LASF286
	.byte	0x13
	.value	0x345
	.byte	0x1a
	.long	0x1413
	.byte	0x8
	.uleb128 0x13
	.long	.LASF287
	.byte	0x13
	.value	0x345
	.byte	0x2f
	.long	0xf00
	.byte	0x10
	.uleb128 0x13
	.long	.LASF288
	.byte	0x13
	.value	0x345
	.byte	0x41
	.long	0x1318
	.byte	0x18
	.uleb128 0x13
	.long	.LASF143
	.byte	0x13
	.value	0x345
	.byte	0x51
	.long	0xba1
	.byte	0x20
	.uleb128 0x17
	.string	"u"
	.byte	0x13
	.value	0x345
	.byte	0x87
	.long	0x1485
	.byte	0x30
	.uleb128 0x13
	.long	.LASF289
	.byte	0x13
	.value	0x345
	.byte	0x97
	.long	0x1312
	.byte	0x50
	.uleb128 0x13
	.long	.LASF146
	.byte	0x13
	.value	0x345
	.byte	0xb2
	.long	0x78
	.byte	0x58
	.uleb128 0x13
	.long	.LASF302
	.byte	0x13
	.value	0x346
	.byte	0xf
	.long	0x1336
	.byte	0x60
	.uleb128 0x13
	.long	.LASF293
	.byte	0x13
	.value	0x346
	.byte	0x1f
	.long	0xba1
	.byte	0x68
	.uleb128 0x13
	.long	.LASF303
	.byte	0x13
	.value	0x346
	.byte	0x2d
	.long	0x57
	.byte	0x78
	.byte	0
	.uleb128 0x7
	.long	.LASF304
	.byte	0x13
	.byte	0xe2
	.byte	0x1c
	.long	0x124f
	.uleb128 0x20
	.long	.LASF305
	.byte	0x98
	.byte	0x13
	.value	0x61c
	.byte	0x8
	.long	0x1312
	.uleb128 0x13
	.long	.LASF141
	.byte	0x13
	.value	0x61d
	.byte	0x9
	.long	0x7f
	.byte	0
	.uleb128 0x13
	.long	.LASF286
	.byte	0x13
	.value	0x61d
	.byte	0x1a
	.long	0x1413
	.byte	0x8
	.uleb128 0x13
	.long	.LASF287
	.byte	0x13
	.value	0x61d
	.byte	0x2f
	.long	0xf00
	.byte	0x10
	.uleb128 0x13
	.long	.LASF288
	.byte	0x13
	.value	0x61d
	.byte	0x41
	.long	0x1318
	.byte	0x18
	.uleb128 0x13
	.long	.LASF143
	.byte	0x13
	.value	0x61d
	.byte	0x51
	.long	0xba1
	.byte	0x20
	.uleb128 0x17
	.string	"u"
	.byte	0x13
	.value	0x61d
	.byte	0x87
	.long	0x14b0
	.byte	0x30
	.uleb128 0x13
	.long	.LASF289
	.byte	0x13
	.value	0x61d
	.byte	0x97
	.long	0x1312
	.byte	0x50
	.uleb128 0x13
	.long	.LASF146
	.byte	0x13
	.value	0x61d
	.byte	0xb2
	.long	0x78
	.byte	0x58
	.uleb128 0x13
	.long	.LASF306
	.byte	0x13
	.value	0x61e
	.byte	0x10
	.long	0x13c6
	.byte	0x60
	.uleb128 0x13
	.long	.LASF307
	.byte	0x13
	.value	0x61f
	.byte	0x7
	.long	0x57
	.byte	0x68
	.uleb128 0x13
	.long	.LASF308
	.byte	0x13
	.value	0x620
	.byte	0x7a
	.long	0x14d4
	.byte	0x70
	.uleb128 0x13
	.long	.LASF309
	.byte	0x13
	.value	0x620
	.byte	0x93
	.long	0x78
	.byte	0x90
	.uleb128 0x13
	.long	.LASF310
	.byte	0x13
	.value	0x620
	.byte	0xb0
	.long	0x78
	.byte	0x94
	.byte	0
	.uleb128 0x3
	.byte	0x8
	.long	0xf18
	.uleb128 0x21
	.long	.LASF311
	.byte	0x13
	.value	0x13e
	.byte	0x10
	.long	0x1325
	.uleb128 0x3
	.byte	0x8
	.long	0x132b
	.uleb128 0x1a
	.long	0x1336
	.uleb128 0x1b
	.long	0x1312
	.byte	0
	.uleb128 0x21
	.long	.LASF312
	.byte	0x13
	.value	0x141
	.byte	0x10
	.long	0x1343
	.uleb128 0x3
	.byte	0x8
	.long	0x1349
	.uleb128 0x1a
	.long	0x1354
	.uleb128 0x1b
	.long	0x1354
	.byte	0
	.uleb128 0x3
	.byte	0x8
	.long	0x1190
	.uleb128 0x21
	.long	.LASF313
	.byte	0x13
	.value	0x142
	.byte	0x10
	.long	0x1367
	.uleb128 0x3
	.byte	0x8
	.long	0x136d
	.uleb128 0x1a
	.long	0x1378
	.uleb128 0x1b
	.long	0x1378
	.byte	0
	.uleb128 0x3
	.byte	0x8
	.long	0xfa1
	.uleb128 0x21
	.long	.LASF314
	.byte	0x13
	.value	0x143
	.byte	0x10
	.long	0x138b
	.uleb128 0x3
	.byte	0x8
	.long	0x1391
	.uleb128 0x1a
	.long	0x139c
	.uleb128 0x1b
	.long	0x139c
	.byte	0
	.uleb128 0x3
	.byte	0x8
	.long	0x1046
	.uleb128 0x21
	.long	.LASF315
	.byte	0x13
	.value	0x144
	.byte	0x10
	.long	0x13af
	.uleb128 0x3
	.byte	0x8
	.long	0x13b5
	.uleb128 0x1a
	.long	0x13c0
	.uleb128 0x1b
	.long	0x13c0
	.byte	0
	.uleb128 0x3
	.byte	0x8
	.long	0x10eb
	.uleb128 0x21
	.long	.LASF316
	.byte	0x13
	.value	0x17a
	.byte	0x10
	.long	0x13d3
	.uleb128 0x3
	.byte	0x8
	.long	0x13d9
	.uleb128 0x1a
	.long	0x13e9
	.uleb128 0x1b
	.long	0x13e9
	.uleb128 0x1b
	.long	0x57
	.byte	0
	.uleb128 0x3
	.byte	0x8
	.long	0x1243
	.uleb128 0x22
	.byte	0x20
	.byte	0x13
	.value	0x1bc
	.byte	0x62
	.long	0x1413
	.uleb128 0x23
	.string	"fd"
	.byte	0x13
	.value	0x1bc
	.byte	0x6e
	.long	0x57
	.uleb128 0x24
	.long	.LASF317
	.byte	0x13
	.value	0x1bc
	.byte	0x78
	.long	0x96c
	.byte	0
	.uleb128 0x3
	.byte	0x8
	.long	0xf0c
	.uleb128 0x22
	.byte	0x20
	.byte	0x13
	.value	0x327
	.byte	0x62
	.long	0x143d
	.uleb128 0x23
	.string	"fd"
	.byte	0x13
	.value	0x327
	.byte	0x6e
	.long	0x57
	.uleb128 0x24
	.long	.LASF317
	.byte	0x13
	.value	0x327
	.byte	0x78
	.long	0x96c
	.byte	0
	.uleb128 0x22
	.byte	0x20
	.byte	0x13
	.value	0x331
	.byte	0x62
	.long	0x1461
	.uleb128 0x23
	.string	"fd"
	.byte	0x13
	.value	0x331
	.byte	0x6e
	.long	0x57
	.uleb128 0x24
	.long	.LASF317
	.byte	0x13
	.value	0x331
	.byte	0x78
	.long	0x96c
	.byte	0
	.uleb128 0x22
	.byte	0x20
	.byte	0x13
	.value	0x33b
	.byte	0x62
	.long	0x1485
	.uleb128 0x23
	.string	"fd"
	.byte	0x13
	.value	0x33b
	.byte	0x6e
	.long	0x57
	.uleb128 0x24
	.long	.LASF317
	.byte	0x13
	.value	0x33b
	.byte	0x78
	.long	0x96c
	.byte	0
	.uleb128 0x22
	.byte	0x20
	.byte	0x13
	.value	0x345
	.byte	0x62
	.long	0x14a9
	.uleb128 0x23
	.string	"fd"
	.byte	0x13
	.value	0x345
	.byte	0x6e
	.long	0x57
	.uleb128 0x24
	.long	.LASF317
	.byte	0x13
	.value	0x345
	.byte	0x78
	.long	0x96c
	.byte	0
	.uleb128 0x4
	.byte	0x8
	.byte	0x4
	.long	.LASF318
	.uleb128 0x22
	.byte	0x20
	.byte	0x13
	.value	0x61d
	.byte	0x62
	.long	0x14d4
	.uleb128 0x23
	.string	"fd"
	.byte	0x13
	.value	0x61d
	.byte	0x6e
	.long	0x57
	.uleb128 0x24
	.long	.LASF317
	.byte	0x13
	.value	0x61d
	.byte	0x78
	.long	0x96c
	.byte	0
	.uleb128 0x25
	.byte	0x20
	.byte	0x13
	.value	0x620
	.byte	0x3
	.long	0x1517
	.uleb128 0x13
	.long	.LASF319
	.byte	0x13
	.value	0x620
	.byte	0x20
	.long	0x1517
	.byte	0
	.uleb128 0x13
	.long	.LASF320
	.byte	0x13
	.value	0x620
	.byte	0x3e
	.long	0x1517
	.byte	0x8
	.uleb128 0x13
	.long	.LASF321
	.byte	0x13
	.value	0x620
	.byte	0x5d
	.long	0x1517
	.byte	0x10
	.uleb128 0x13
	.long	.LASF322
	.byte	0x13
	.value	0x620
	.byte	0x6d
	.long	0x57
	.byte	0x18
	.byte	0
	.uleb128 0x3
	.byte	0x8
	.long	0x124f
	.uleb128 0x22
	.byte	0x10
	.byte	0x13
	.value	0x6f0
	.byte	0x3
	.long	0x1542
	.uleb128 0x24
	.long	.LASF323
	.byte	0x13
	.value	0x6f1
	.byte	0xb
	.long	0xba1
	.uleb128 0x24
	.long	.LASF324
	.byte	0x13
	.value	0x6f2
	.byte	0x12
	.long	0x78
	.byte	0
	.uleb128 0x26
	.byte	0x10
	.byte	0x13
	.value	0x6f6
	.value	0x1c8
	.long	0x156c
	.uleb128 0x27
	.string	"min"
	.byte	0x13
	.value	0x6f6
	.value	0x1d7
	.long	0x7f
	.byte	0
	.uleb128 0x28
	.long	.LASF325
	.byte	0x13
	.value	0x6f6
	.value	0x1e9
	.long	0x78
	.byte	0x8
	.byte	0
	.uleb128 0x3
	.byte	0x8
	.long	0x1572
	.uleb128 0x3
	.byte	0x8
	.long	0xc38
	.uleb128 0x7
	.long	.LASF326
	.byte	0x15
	.byte	0x15
	.byte	0xf
	.long	0xba1
	.uleb128 0x1d
	.byte	0x7
	.byte	0x4
	.long	0x78
	.byte	0x16
	.byte	0x40
	.byte	0x6
	.long	0x16dc
	.uleb128 0x1f
	.long	.LASF327
	.byte	0x1
	.uleb128 0x1f
	.long	.LASF328
	.byte	0x2
	.uleb128 0x1f
	.long	.LASF329
	.byte	0x4
	.uleb128 0x1f
	.long	.LASF330
	.byte	0x8
	.uleb128 0x1f
	.long	.LASF331
	.byte	0x10
	.uleb128 0x1f
	.long	.LASF332
	.byte	0x20
	.uleb128 0x1f
	.long	.LASF333
	.byte	0x40
	.uleb128 0x1f
	.long	.LASF334
	.byte	0x80
	.uleb128 0x29
	.long	.LASF335
	.value	0x100
	.uleb128 0x29
	.long	.LASF336
	.value	0x200
	.uleb128 0x29
	.long	.LASF337
	.value	0x400
	.uleb128 0x29
	.long	.LASF338
	.value	0x800
	.uleb128 0x29
	.long	.LASF339
	.value	0x1000
	.uleb128 0x29
	.long	.LASF340
	.value	0x2000
	.uleb128 0x29
	.long	.LASF341
	.value	0x4000
	.uleb128 0x29
	.long	.LASF342
	.value	0x8000
	.uleb128 0x2a
	.long	.LASF343
	.long	0x10000
	.uleb128 0x2a
	.long	.LASF344
	.long	0x20000
	.uleb128 0x2a
	.long	.LASF345
	.long	0x40000
	.uleb128 0x2a
	.long	.LASF346
	.long	0x80000
	.uleb128 0x2a
	.long	.LASF347
	.long	0x100000
	.uleb128 0x2a
	.long	.LASF348
	.long	0x200000
	.uleb128 0x2a
	.long	.LASF349
	.long	0x400000
	.uleb128 0x2a
	.long	.LASF350
	.long	0x1000000
	.uleb128 0x2a
	.long	.LASF351
	.long	0x2000000
	.uleb128 0x2a
	.long	.LASF352
	.long	0x4000000
	.uleb128 0x2a
	.long	.LASF353
	.long	0x8000000
	.uleb128 0x2a
	.long	.LASF354
	.long	0x10000000
	.uleb128 0x2a
	.long	.LASF355
	.long	0x20000000
	.uleb128 0x2a
	.long	.LASF356
	.long	0x1000000
	.uleb128 0x2a
	.long	.LASF357
	.long	0x2000000
	.uleb128 0x2a
	.long	.LASF358
	.long	0x4000000
	.uleb128 0x2a
	.long	.LASF359
	.long	0x1000000
	.uleb128 0x2a
	.long	.LASF360
	.long	0x2000000
	.uleb128 0x2a
	.long	.LASF361
	.long	0x1000000
	.uleb128 0x2a
	.long	.LASF362
	.long	0x2000000
	.uleb128 0x2a
	.long	.LASF363
	.long	0x4000000
	.uleb128 0x2a
	.long	.LASF364
	.long	0x8000000
	.uleb128 0x2a
	.long	.LASF365
	.long	0x1000000
	.uleb128 0x2a
	.long	.LASF366
	.long	0x2000000
	.uleb128 0x2a
	.long	.LASF367
	.long	0x1000000
	.byte	0
	.uleb128 0x2b
	.long	.LASF368
	.byte	0x1
	.byte	0x44
	.value	0x1d9
	.quad	.LFB95
	.quad	.LFE95-.LFB95
	.uleb128 0x1
	.byte	0x9c
	.long	0x176b
	.uleb128 0x2c
	.long	.LASF370
	.byte	0x1
	.byte	0x44
	.value	0x1f3
	.long	0x13c0
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x2d
	.long	0x1813
	.quad	.LBI37
	.byte	.LVU529
	.quad	.LBB37
	.quad	.LBE37-.LBB37
	.byte	0x1
	.byte	0x44
	.value	0x1fd
	.uleb128 0x2e
	.long	0x1825
	.long	.LLST19
	.long	.LVUS19
	.uleb128 0x2d
	.long	0x1813
	.quad	.LBI39
	.byte	.LVU548
	.quad	.LBB39
	.quad	.LBE39-.LBB39
	.byte	0x1
	.byte	0x44
	.value	0x263
	.uleb128 0x2e
	.long	0x1825
	.long	.LLST20
	.long	.LVUS20
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x2f
	.long	.LASF369
	.byte	0x1
	.byte	0x44
	.value	0x48e
	.quad	.LFB94
	.quad	.LFE94-.LFB94
	.uleb128 0x1
	.byte	0x9c
	.long	0x180d
	.uleb128 0x30
	.long	.LASF286
	.byte	0x1
	.byte	0x44
	.value	0x4a6
	.long	0x1413
	.long	.LLST15
	.long	.LVUS15
	.uleb128 0x31
	.string	"h"
	.byte	0x1
	.byte	0x44
	.value	0x4b9
	.long	0x13c0
	.long	.LLST16
	.long	.LVUS16
	.uleb128 0x32
	.long	.LASF293
	.byte	0x1
	.byte	0x44
	.value	0x4c2
	.long	0x1578
	.uleb128 0x3
	.byte	0x91
	.sleb128 -80
	.uleb128 0x31
	.string	"q"
	.byte	0x1
	.byte	0x44
	.value	0x4d0
	.long	0x180d
	.long	.LLST17
	.long	.LVUS17
	.uleb128 0x33
	.quad	.LBB32
	.quad	.LBE32-.LBB32
	.long	0x17ff
	.uleb128 0x31
	.string	"q"
	.byte	0x1
	.byte	0x44
	.value	0x5bd
	.long	0x180d
	.long	.LLST18
	.long	.LVUS18
	.byte	0
	.uleb128 0x34
	.quad	.LVL49
	.long	0x1d45
	.byte	0
	.uleb128 0x3
	.byte	0x8
	.long	0x1578
	.uleb128 0x35
	.long	.LASF375
	.byte	0x1
	.byte	0x44
	.value	0x263
	.long	0x57
	.byte	0x1
	.long	0x1833
	.uleb128 0x36
	.long	.LASF370
	.byte	0x1
	.byte	0x44
	.value	0x27b
	.long	0x13c0
	.byte	0
	.uleb128 0x37
	.long	.LASF371
	.byte	0x1
	.byte	0x44
	.byte	0x13
	.long	0x57
	.quad	.LFB92
	.quad	.LFE92-.LFB92
	.uleb128 0x1
	.byte	0x9c
	.long	0x1871
	.uleb128 0x38
	.long	.LASF370
	.byte	0x1
	.byte	0x44
	.byte	0x2c
	.long	0x13c0
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x39
	.string	"cb"
	.byte	0x1
	.byte	0x44
	.byte	0x3f
	.long	0x13a2
	.uleb128 0x1
	.byte	0x54
	.byte	0
	.uleb128 0x37
	.long	.LASF372
	.byte	0x1
	.byte	0x44
	.byte	0x5
	.long	0x57
	.quad	.LFB91
	.quad	.LFE91-.LFB91
	.uleb128 0x1
	.byte	0x9c
	.long	0x18b0
	.uleb128 0x38
	.long	.LASF286
	.byte	0x1
	.byte	0x44
	.byte	0x1d
	.long	0x1413
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x38
	.long	.LASF370
	.byte	0x1
	.byte	0x44
	.byte	0x2e
	.long	0x13c0
	.uleb128 0x1
	.byte	0x54
	.byte	0
	.uleb128 0x2b
	.long	.LASF373
	.byte	0x1
	.byte	0x43
	.value	0x1dd
	.quad	.LFB90
	.quad	.LFE90-.LFB90
	.uleb128 0x1
	.byte	0x9c
	.long	0x193f
	.uleb128 0x2c
	.long	.LASF370
	.byte	0x1
	.byte	0x43
	.value	0x1f9
	.long	0x139c
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x2d
	.long	0x19e1
	.quad	.LBI24
	.byte	.LVU344
	.quad	.LBB24
	.quad	.LBE24-.LBB24
	.byte	0x1
	.byte	0x43
	.value	0x203
	.uleb128 0x2e
	.long	0x19f3
	.long	.LLST12
	.long	.LVUS12
	.uleb128 0x2d
	.long	0x19e1
	.quad	.LBI26
	.byte	.LVU363
	.quad	.LBB26
	.quad	.LBE26-.LBB26
	.byte	0x1
	.byte	0x43
	.value	0x267
	.uleb128 0x2e
	.long	0x19f3
	.long	.LLST13
	.long	.LVUS13
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x2f
	.long	.LASF374
	.byte	0x1
	.byte	0x43
	.value	0x494
	.quad	.LFB89
	.quad	.LFE89-.LFB89
	.uleb128 0x1
	.byte	0x9c
	.long	0x19e1
	.uleb128 0x30
	.long	.LASF286
	.byte	0x1
	.byte	0x43
	.value	0x4ad
	.long	0x1413
	.long	.LLST8
	.long	.LVUS8
	.uleb128 0x31
	.string	"h"
	.byte	0x1
	.byte	0x43
	.value	0x4c1
	.long	0x139c
	.long	.LLST9
	.long	.LVUS9
	.uleb128 0x32
	.long	.LASF293
	.byte	0x1
	.byte	0x43
	.value	0x4ca
	.long	0x1578
	.uleb128 0x3
	.byte	0x91
	.sleb128 -80
	.uleb128 0x31
	.string	"q"
	.byte	0x1
	.byte	0x43
	.value	0x4d8
	.long	0x180d
	.long	.LLST10
	.long	.LVUS10
	.uleb128 0x33
	.quad	.LBB19
	.quad	.LBE19-.LBB19
	.long	0x19d3
	.uleb128 0x31
	.string	"q"
	.byte	0x1
	.byte	0x43
	.value	0x5c7
	.long	0x180d
	.long	.LLST11
	.long	.LVUS11
	.byte	0
	.uleb128 0x34
	.quad	.LVL31
	.long	0x1d45
	.byte	0
	.uleb128 0x35
	.long	.LASF376
	.byte	0x1
	.byte	0x43
	.value	0x267
	.long	0x57
	.byte	0x1
	.long	0x1a01
	.uleb128 0x36
	.long	.LASF370
	.byte	0x1
	.byte	0x43
	.value	0x281
	.long	0x139c
	.byte	0
	.uleb128 0x37
	.long	.LASF377
	.byte	0x1
	.byte	0x43
	.byte	0x13
	.long	0x57
	.quad	.LFB87
	.quad	.LFE87-.LFB87
	.uleb128 0x1
	.byte	0x9c
	.long	0x1a3f
	.uleb128 0x38
	.long	.LASF370
	.byte	0x1
	.byte	0x43
	.byte	0x2e
	.long	0x139c
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x39
	.string	"cb"
	.byte	0x1
	.byte	0x43
	.byte	0x42
	.long	0x137e
	.uleb128 0x1
	.byte	0x54
	.byte	0
	.uleb128 0x37
	.long	.LASF378
	.byte	0x1
	.byte	0x43
	.byte	0x5
	.long	0x57
	.quad	.LFB86
	.quad	.LFE86-.LFB86
	.uleb128 0x1
	.byte	0x9c
	.long	0x1a7e
	.uleb128 0x38
	.long	.LASF286
	.byte	0x1
	.byte	0x43
	.byte	0x1e
	.long	0x1413
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x38
	.long	.LASF370
	.byte	0x1
	.byte	0x43
	.byte	0x30
	.long	0x139c
	.uleb128 0x1
	.byte	0x54
	.byte	0
	.uleb128 0x2b
	.long	.LASF379
	.byte	0x1
	.byte	0x42
	.value	0x1e5
	.quad	.LFB85
	.quad	.LFE85-.LFB85
	.uleb128 0x1
	.byte	0x9c
	.long	0x1b0d
	.uleb128 0x2c
	.long	.LASF370
	.byte	0x1
	.byte	0x42
	.value	0x205
	.long	0x1378
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x2d
	.long	0x1baf
	.quad	.LBI11
	.byte	.LVU159
	.quad	.LBB11
	.quad	.LBE11-.LBB11
	.byte	0x1
	.byte	0x42
	.value	0x20f
	.uleb128 0x2e
	.long	0x1bc1
	.long	.LLST5
	.long	.LVUS5
	.uleb128 0x2d
	.long	0x1baf
	.quad	.LBI13
	.byte	.LVU178
	.quad	.LBB13
	.quad	.LBE13-.LBB13
	.byte	0x1
	.byte	0x42
	.value	0x26f
	.uleb128 0x2e
	.long	0x1bc1
	.long	.LLST6
	.long	.LVUS6
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x2f
	.long	.LASF380
	.byte	0x1
	.byte	0x42
	.value	0x4a0
	.quad	.LFB84
	.quad	.LFE84-.LFB84
	.uleb128 0x1
	.byte	0x9c
	.long	0x1baf
	.uleb128 0x30
	.long	.LASF286
	.byte	0x1
	.byte	0x42
	.value	0x4bb
	.long	0x1413
	.long	.LLST1
	.long	.LVUS1
	.uleb128 0x31
	.string	"h"
	.byte	0x1
	.byte	0x42
	.value	0x4d1
	.long	0x1378
	.long	.LLST2
	.long	.LVUS2
	.uleb128 0x32
	.long	.LASF293
	.byte	0x1
	.byte	0x42
	.value	0x4da
	.long	0x1578
	.uleb128 0x3
	.byte	0x91
	.sleb128 -80
	.uleb128 0x31
	.string	"q"
	.byte	0x1
	.byte	0x42
	.value	0x4e8
	.long	0x180d
	.long	.LLST3
	.long	.LVUS3
	.uleb128 0x33
	.quad	.LBB6
	.quad	.LBE6-.LBB6
	.long	0x1ba1
	.uleb128 0x31
	.string	"q"
	.byte	0x1
	.byte	0x42
	.value	0x5db
	.long	0x180d
	.long	.LLST4
	.long	.LVUS4
	.byte	0
	.uleb128 0x34
	.quad	.LVL13
	.long	0x1d45
	.byte	0
	.uleb128 0x35
	.long	.LASF381
	.byte	0x1
	.byte	0x42
	.value	0x26f
	.long	0x57
	.byte	0x1
	.long	0x1bcf
	.uleb128 0x36
	.long	.LASF370
	.byte	0x1
	.byte	0x42
	.value	0x28d
	.long	0x1378
	.byte	0
	.uleb128 0x37
	.long	.LASF382
	.byte	0x1
	.byte	0x42
	.byte	0x13
	.long	0x57
	.quad	.LFB82
	.quad	.LFE82-.LFB82
	.uleb128 0x1
	.byte	0x9c
	.long	0x1c0d
	.uleb128 0x38
	.long	.LASF370
	.byte	0x1
	.byte	0x42
	.byte	0x32
	.long	0x1378
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x39
	.string	"cb"
	.byte	0x1
	.byte	0x42
	.byte	0x48
	.long	0x135a
	.uleb128 0x1
	.byte	0x54
	.byte	0
	.uleb128 0x37
	.long	.LASF383
	.byte	0x1
	.byte	0x42
	.byte	0x5
	.long	0x57
	.quad	.LFB81
	.quad	.LFE81-.LFB81
	.uleb128 0x1
	.byte	0x9c
	.long	0x1c4c
	.uleb128 0x38
	.long	.LASF286
	.byte	0x1
	.byte	0x42
	.byte	0x20
	.long	0x1413
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x38
	.long	.LASF370
	.byte	0x1
	.byte	0x42
	.byte	0x34
	.long	0x1378
	.uleb128 0x1
	.byte	0x54
	.byte	0
	.uleb128 0x3a
	.long	0x1baf
	.quad	.LFB83
	.quad	.LFE83-.LFB83
	.uleb128 0x1
	.byte	0x9c
	.long	0x1c9f
	.uleb128 0x3b
	.long	0x1bc1
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x2d
	.long	0x1baf
	.quad	.LBI4
	.byte	.LVU88
	.quad	.LBB4
	.quad	.LBE4-.LBB4
	.byte	0x1
	.byte	0x42
	.value	0x26f
	.uleb128 0x2e
	.long	0x1bc1
	.long	.LLST0
	.long	.LVUS0
	.byte	0
	.byte	0
	.uleb128 0x3a
	.long	0x19e1
	.quad	.LFB88
	.quad	.LFE88-.LFB88
	.uleb128 0x1
	.byte	0x9c
	.long	0x1cf2
	.uleb128 0x3b
	.long	0x19f3
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x2d
	.long	0x19e1
	.quad	.LBI17
	.byte	.LVU273
	.quad	.LBB17
	.quad	.LBE17-.LBB17
	.byte	0x1
	.byte	0x43
	.value	0x267
	.uleb128 0x2e
	.long	0x19f3
	.long	.LLST7
	.long	.LVUS7
	.byte	0
	.byte	0
	.uleb128 0x3a
	.long	0x1813
	.quad	.LFB93
	.quad	.LFE93-.LFB93
	.uleb128 0x1
	.byte	0x9c
	.long	0x1d45
	.uleb128 0x3b
	.long	0x1825
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x2d
	.long	0x1813
	.quad	.LBI30
	.byte	.LVU458
	.quad	.LBB30
	.quad	.LBE30-.LBB30
	.byte	0x1
	.byte	0x44
	.value	0x263
	.uleb128 0x2e
	.long	0x1825
	.long	.LLST14
	.long	.LVUS14
	.byte	0
	.byte	0
	.uleb128 0x3c
	.long	.LASF388
	.long	.LASF388
	.byte	0
	.section	.debug_abbrev,"",@progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1b
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x7
	.uleb128 0x10
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x13
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x13
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3c
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x21
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x17
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0x37
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0x15
	.byte	0
	.uleb128 0x27
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0x16
	.uleb128 0x13
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0xb
	.uleb128 0x5
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x17
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x18
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0x5
	.byte	0
	.byte	0
	.uleb128 0x19
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0x5
	.byte	0
	.byte	0
	.uleb128 0x1a
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1b
	.uleb128 0x5
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1c
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x1d
	.uleb128 0x4
	.byte	0x1
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1e
	.uleb128 0x28
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1c
	.uleb128 0xd
	.byte	0
	.byte	0
	.uleb128 0x1f
	.uleb128 0x28
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1c
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x20
	.uleb128 0x13
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x21
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x22
	.uleb128 0x17
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x23
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x24
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x25
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x26
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0x5
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x27
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x28
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x29
	.uleb128 0x28
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1c
	.uleb128 0x5
	.byte	0
	.byte	0
	.uleb128 0x2a
	.uleb128 0x28
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1c
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2b
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x7
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2117
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x2c
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x2d
	.uleb128 0x1d
	.byte	0x1
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x2138
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x7
	.uleb128 0x58
	.uleb128 0xb
	.uleb128 0x59
	.uleb128 0xb
	.uleb128 0x57
	.uleb128 0x5
	.byte	0
	.byte	0
	.uleb128 0x2e
	.uleb128 0x5
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x17
	.uleb128 0x2137
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x2f
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x7
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2116
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x30
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x17
	.uleb128 0x2137
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x31
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x17
	.uleb128 0x2137
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x32
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x33
	.uleb128 0xb
	.byte	0x1
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x7
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x34
	.uleb128 0x4109
	.byte	0
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x31
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x35
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x20
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x36
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x37
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x7
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2117
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x38
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x39
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x3a
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x7
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2117
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x3b
	.uleb128 0x5
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x3c
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.uleb128 0x6e
	.uleb128 0xe
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",@progbits
.Ldebug_loc0:
.LVUS19:
	.uleb128 .LVU529
	.uleb128 .LVU553
.LLST19:
	.quad	.LVL51-.Ltext0
	.quad	.LVL53-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	0
	.quad	0
.LVUS20:
	.uleb128 .LVU548
	.uleb128 .LVU553
.LLST20:
	.quad	.LVL52-.Ltext0
	.quad	.LVL53-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	0
	.quad	0
.LVUS15:
	.uleb128 0
	.uleb128 .LVU497
	.uleb128 .LVU497
	.uleb128 .LVU524
	.uleb128 .LVU524
	.uleb128 .LVU525
	.uleb128 .LVU525
	.uleb128 0
.LLST15:
	.quad	.LVL41-.Ltext0
	.quad	.LVL44-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	.LVL44-.Ltext0
	.quad	.LVL47-.Ltext0
	.value	0x1
	.byte	0x5d
	.quad	.LVL47-.Ltext0
	.quad	.LVL48-.Ltext0
	.value	0x4
	.byte	0x7c
	.sleb128 -416
	.byte	0x9f
	.quad	.LVL48-.Ltext0
	.quad	.LFE94-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS16:
	.uleb128 .LVU499
	.uleb128 .LVU504
	.uleb128 .LVU504
	.uleb128 .LVU521
.LLST16:
	.quad	.LVL44-.Ltext0
	.quad	.LVL45-.Ltext0
	.value	0x4
	.byte	0x70
	.sleb128 -104
	.byte	0x9f
	.quad	.LVL45-.Ltext0
	.quad	.LVL46-1-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	0
	.quad	0
.LVUS17:
	.uleb128 .LVU498
	.uleb128 .LVU521
.LLST17:
	.quad	.LVL44-.Ltext0
	.quad	.LVL46-1-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	0
	.quad	0
.LVUS18:
	.uleb128 .LVU480
	.uleb128 .LVU496
.LLST18:
	.quad	.LVL42-.Ltext0
	.quad	.LVL43-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	0
	.quad	0
.LVUS12:
	.uleb128 .LVU344
	.uleb128 .LVU368
.LLST12:
	.quad	.LVL33-.Ltext0
	.quad	.LVL35-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	0
	.quad	0
.LVUS13:
	.uleb128 .LVU363
	.uleb128 .LVU368
.LLST13:
	.quad	.LVL34-.Ltext0
	.quad	.LVL35-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	0
	.quad	0
.LVUS8:
	.uleb128 0
	.uleb128 .LVU312
	.uleb128 .LVU312
	.uleb128 .LVU339
	.uleb128 .LVU339
	.uleb128 .LVU340
	.uleb128 .LVU340
	.uleb128 0
.LLST8:
	.quad	.LVL23-.Ltext0
	.quad	.LVL26-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	.LVL26-.Ltext0
	.quad	.LVL29-.Ltext0
	.value	0x1
	.byte	0x5d
	.quad	.LVL29-.Ltext0
	.quad	.LVL30-.Ltext0
	.value	0x4
	.byte	0x7c
	.sleb128 -400
	.byte	0x9f
	.quad	.LVL30-.Ltext0
	.quad	.LFE89-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS9:
	.uleb128 .LVU314
	.uleb128 .LVU319
	.uleb128 .LVU319
	.uleb128 .LVU336
.LLST9:
	.quad	.LVL26-.Ltext0
	.quad	.LVL27-.Ltext0
	.value	0x4
	.byte	0x70
	.sleb128 -104
	.byte	0x9f
	.quad	.LVL27-.Ltext0
	.quad	.LVL28-1-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	0
	.quad	0
.LVUS10:
	.uleb128 .LVU313
	.uleb128 .LVU336
.LLST10:
	.quad	.LVL26-.Ltext0
	.quad	.LVL28-1-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	0
	.quad	0
.LVUS11:
	.uleb128 .LVU295
	.uleb128 .LVU311
.LLST11:
	.quad	.LVL24-.Ltext0
	.quad	.LVL25-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	0
	.quad	0
.LVUS5:
	.uleb128 .LVU159
	.uleb128 .LVU183
.LLST5:
	.quad	.LVL15-.Ltext0
	.quad	.LVL17-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	0
	.quad	0
.LVUS6:
	.uleb128 .LVU178
	.uleb128 .LVU183
.LLST6:
	.quad	.LVL16-.Ltext0
	.quad	.LVL17-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	0
	.quad	0
.LVUS1:
	.uleb128 0
	.uleb128 .LVU127
	.uleb128 .LVU127
	.uleb128 .LVU154
	.uleb128 .LVU154
	.uleb128 .LVU155
	.uleb128 .LVU155
	.uleb128 0
.LLST1:
	.quad	.LVL5-.Ltext0
	.quad	.LVL8-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	.LVL8-.Ltext0
	.quad	.LVL11-.Ltext0
	.value	0x1
	.byte	0x5d
	.quad	.LVL11-.Ltext0
	.quad	.LVL12-.Ltext0
	.value	0x4
	.byte	0x7c
	.sleb128 -384
	.byte	0x9f
	.quad	.LVL12-.Ltext0
	.quad	.LFE84-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS2:
	.uleb128 .LVU129
	.uleb128 .LVU134
	.uleb128 .LVU134
	.uleb128 .LVU151
.LLST2:
	.quad	.LVL8-.Ltext0
	.quad	.LVL9-.Ltext0
	.value	0x4
	.byte	0x70
	.sleb128 -104
	.byte	0x9f
	.quad	.LVL9-.Ltext0
	.quad	.LVL10-1-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	0
	.quad	0
.LVUS3:
	.uleb128 .LVU128
	.uleb128 .LVU151
.LLST3:
	.quad	.LVL8-.Ltext0
	.quad	.LVL10-1-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	0
	.quad	0
.LVUS4:
	.uleb128 .LVU110
	.uleb128 .LVU126
.LLST4:
	.quad	.LVL6-.Ltext0
	.quad	.LVL7-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	0
	.quad	0
.LVUS0:
	.uleb128 .LVU88
	.uleb128 .LVU93
.LLST0:
	.quad	.LVL3-.Ltext0
	.quad	.LVL4-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	0
	.quad	0
.LVUS7:
	.uleb128 .LVU273
	.uleb128 .LVU278
.LLST7:
	.quad	.LVL21-.Ltext0
	.quad	.LVL22-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	0
	.quad	0
.LVUS14:
	.uleb128 .LVU458
	.uleb128 .LVU463
.LLST14:
	.quad	.LVL39-.Ltext0
	.quad	.LVL40-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	0
	.quad	0
	.section	.debug_aranges,"",@progbits
	.long	0x2c
	.value	0x2
	.long	.Ldebug_info0
	.byte	0x8
	.byte	0
	.value	0
	.value	0
	.quad	.Ltext0
	.quad	.Letext0-.Ltext0
	.quad	0
	.quad	0
	.section	.debug_line,"",@progbits
.Ldebug_line0:
	.section	.debug_str,"MS",@progbits,1
.LASF353:
	.string	"UV_HANDLE_TCP_ACCEPT_STATE_CHANGING"
.LASF221:
	.string	"UV_EMFILE"
.LASF164:
	.string	"async_wfd"
.LASF100:
	.string	"sockaddr_ax25"
.LASF206:
	.string	"UV_ECHARSET"
.LASF111:
	.string	"sin6_flowinfo"
.LASF343:
	.string	"UV_HANDLE_READ_PENDING"
.LASF36:
	.string	"_shortbuf"
.LASF387:
	.string	"_IO_lock_t"
.LASF119:
	.string	"sockaddr_x25"
.LASF1:
	.string	"program_invocation_short_name"
.LASF264:
	.string	"UV_ASYNC"
.LASF52:
	.string	"stderr"
.LASF172:
	.string	"inotify_read_watcher"
.LASF87:
	.string	"__flags"
.LASF25:
	.string	"_IO_buf_end"
.LASF176:
	.string	"uv__io_s"
.LASF179:
	.string	"uv__io_t"
.LASF98:
	.string	"sa_data"
.LASF336:
	.string	"UV_HANDLE_SHUT"
.LASF246:
	.string	"UV_EROFS"
.LASF146:
	.string	"flags"
.LASF208:
	.string	"UV_ECONNREFUSED"
.LASF204:
	.string	"UV_EBUSY"
.LASF330:
	.string	"UV_HANDLE_REF"
.LASF243:
	.string	"UV_EPROTONOSUPPORT"
.LASF286:
	.string	"loop"
.LASF113:
	.string	"sin6_scope_id"
.LASF82:
	.string	"__cur_writer"
.LASF23:
	.string	"_IO_write_end"
.LASF5:
	.string	"unsigned int"
.LASF117:
	.string	"sockaddr_ns"
.LASF361:
	.string	"UV_HANDLE_TTY_READABLE"
.LASF269:
	.string	"UV_IDLE"
.LASF154:
	.string	"wq_async"
.LASF139:
	.string	"getdate_err"
.LASF252:
	.string	"UV_EXDEV"
.LASF17:
	.string	"_flags"
.LASF142:
	.string	"active_handles"
.LASF149:
	.string	"watcher_queue"
.LASF296:
	.string	"check_cb"
.LASF46:
	.string	"FILE"
.LASF309:
	.string	"caught_signals"
.LASF147:
	.string	"backend_fd"
.LASF178:
	.string	"events"
.LASF155:
	.string	"cloexec_lock"
.LASF171:
	.string	"emfile_fd"
.LASF185:
	.string	"UV_EADDRNOTAVAIL"
.LASF285:
	.string	"uv_handle_s"
.LASF284:
	.string	"uv_handle_t"
.LASF29:
	.string	"_markers"
.LASF338:
	.string	"UV_HANDLE_READ_EOF"
.LASF55:
	.string	"_sys_nerr"
.LASF131:
	.string	"_sys_siglist"
.LASF200:
	.string	"UV_EAI_SERVICE"
.LASF253:
	.string	"UV_UNKNOWN"
.LASF203:
	.string	"UV_EBADF"
.LASF331:
	.string	"UV_HANDLE_INTERNAL"
.LASF260:
	.string	"UV_EFTYPE"
.LASF369:
	.string	"uv__run_idle"
.LASF302:
	.string	"async_cb"
.LASF222:
	.string	"UV_EMSGSIZE"
.LASF150:
	.string	"watchers"
.LASF144:
	.string	"active_reqs"
.LASF181:
	.string	"uv_rwlock_t"
.LASF77:
	.string	"__writers"
.LASF365:
	.string	"UV_SIGNAL_ONE_SHOT_DISPATCHED"
.LASF321:
	.string	"rbe_parent"
.LASF385:
	.string	"../deps/uv/src/unix/loop-watcher.c"
.LASF363:
	.string	"UV_HANDLE_TTY_SAVED_POSITION"
.LASF213:
	.string	"UV_EFBIG"
.LASF152:
	.string	"nfds"
.LASF241:
	.string	"UV_EPIPE"
.LASF292:
	.string	"prepare_cb"
.LASF128:
	.string	"__in6_u"
.LASF350:
	.string	"UV_HANDLE_TCP_NODELAY"
.LASF115:
	.string	"sockaddr_ipx"
.LASF357:
	.string	"UV_HANDLE_UDP_CONNECTED"
.LASF235:
	.string	"UV_ENOTCONN"
.LASF220:
	.string	"UV_ELOOP"
.LASF224:
	.string	"UV_ENETDOWN"
.LASF62:
	.string	"__pthread_internal_list"
.LASF83:
	.string	"__shared"
.LASF59:
	.string	"uint32_t"
.LASF63:
	.string	"__prev"
.LASF245:
	.string	"UV_ERANGE"
.LASF120:
	.string	"in_addr_t"
.LASF366:
	.string	"UV_SIGNAL_ONE_SHOT"
.LASF266:
	.string	"UV_FS_EVENT"
.LASF51:
	.string	"stdout"
.LASF28:
	.string	"_IO_save_end"
.LASF378:
	.string	"uv_check_init"
.LASF68:
	.string	"__count"
.LASF170:
	.string	"child_watcher"
.LASF88:
	.string	"long long unsigned int"
.LASF223:
	.string	"UV_ENAMETOOLONG"
.LASF34:
	.string	"_cur_column"
.LASF99:
	.string	"sockaddr_at"
.LASF324:
	.string	"count"
.LASF305:
	.string	"uv_signal_s"
.LASF304:
	.string	"uv_signal_t"
.LASF167:
	.string	"time"
.LASF295:
	.string	"uv_check_s"
.LASF294:
	.string	"uv_check_t"
.LASF372:
	.string	"uv_idle_init"
.LASF165:
	.string	"timer_heap"
.LASF124:
	.string	"__u6_addr8"
.LASF191:
	.string	"UV_EAI_BADHINTS"
.LASF193:
	.string	"UV_EAI_FAIL"
.LASF190:
	.string	"UV_EAI_BADFLAGS"
.LASF101:
	.string	"sockaddr_dl"
.LASF270:
	.string	"UV_NAMED_PIPE"
.LASF280:
	.string	"UV_FILE"
.LASF184:
	.string	"UV_EADDRINUSE"
.LASF316:
	.string	"uv_signal_cb"
.LASF104:
	.string	"sin_family"
.LASF12:
	.string	"__uint16_t"
.LASF54:
	.string	"sys_errlist"
.LASF69:
	.string	"__owner"
.LASF288:
	.string	"close_cb"
.LASF151:
	.string	"nwatchers"
.LASF267:
	.string	"UV_FS_POLL"
.LASF123:
	.string	"in_port_t"
.LASF73:
	.string	"__elision"
.LASF53:
	.string	"sys_nerr"
.LASF207:
	.string	"UV_ECONNABORTED"
.LASF197:
	.string	"UV_EAI_NONAME"
.LASF84:
	.string	"__rwelision"
.LASF332:
	.string	"UV_HANDLE_ENDGAME_QUEUED"
.LASF31:
	.string	"_fileno"
.LASF110:
	.string	"sin6_port"
.LASF107:
	.string	"sin_zero"
.LASF189:
	.string	"UV_EAI_AGAIN"
.LASF291:
	.string	"uv_prepare_s"
.LASF290:
	.string	"uv_prepare_t"
.LASF122:
	.string	"s_addr"
.LASF9:
	.string	"size_t"
.LASF95:
	.string	"sa_family_t"
.LASF7:
	.string	"short unsigned int"
.LASF244:
	.string	"UV_EPROTOTYPE"
.LASF262:
	.string	"UV_ERRNO_MAX"
.LASF212:
	.string	"UV_EFAULT"
.LASF247:
	.string	"UV_ESHUTDOWN"
.LASF20:
	.string	"_IO_read_base"
.LASF351:
	.string	"UV_HANDLE_TCP_KEEPALIVE"
.LASF373:
	.string	"uv__check_close"
.LASF114:
	.string	"sockaddr_inarp"
.LASF50:
	.string	"stdin"
.LASF349:
	.string	"UV_HANDLE_IPV6"
.LASF161:
	.string	"async_handles"
.LASF94:
	.string	"pthread_rwlock_t"
.LASF112:
	.string	"sin6_addr"
.LASF14:
	.string	"__uint64_t"
.LASF303:
	.string	"pending"
.LASF116:
	.string	"sockaddr_iso"
.LASF307:
	.string	"signum"
.LASF315:
	.string	"uv_idle_cb"
.LASF380:
	.string	"uv__run_prepare"
.LASF254:
	.string	"UV_EOF"
.LASF261:
	.string	"UV_EILSEQ"
.LASF215:
	.string	"UV_EINTR"
.LASF231:
	.string	"UV_ENONET"
.LASF64:
	.string	"__next"
.LASF79:
	.string	"__writers_futex"
.LASF314:
	.string	"uv_check_cb"
.LASF153:
	.string	"wq_mutex"
.LASF130:
	.string	"in6addr_loopback"
.LASF187:
	.string	"UV_EAGAIN"
.LASF237:
	.string	"UV_ENOTEMPTY"
.LASF386:
	.string	"/workspace/nodejsbuild/node-v12.18.4/out"
.LASF229:
	.string	"UV_ENOENT"
.LASF282:
	.string	"uv_handle_type"
.LASF2:
	.string	"char"
.LASF271:
	.string	"UV_POLL"
.LASF251:
	.string	"UV_ETXTBSY"
.LASF134:
	.string	"__daylight"
.LASF228:
	.string	"UV_ENODEV"
.LASF341:
	.string	"UV_HANDLE_READABLE"
.LASF136:
	.string	"tzname"
.LASF377:
	.string	"uv_check_start"
.LASF47:
	.string	"_IO_marker"
.LASF340:
	.string	"UV_HANDLE_BOUND"
.LASF312:
	.string	"uv_async_cb"
.LASF18:
	.string	"_IO_read_ptr"
.LASF141:
	.string	"data"
.LASF188:
	.string	"UV_EAI_ADDRFAMILY"
.LASF325:
	.string	"nelts"
.LASF356:
	.string	"UV_HANDLE_UDP_PROCESSING"
.LASF72:
	.string	"__spins"
.LASF322:
	.string	"rbe_color"
.LASF57:
	.string	"uint8_t"
.LASF258:
	.string	"UV_EREMOTEIO"
.LASF205:
	.string	"UV_ECANCELED"
.LASF299:
	.string	"idle_cb"
.LASF195:
	.string	"UV_EAI_MEMORY"
.LASF75:
	.string	"__pthread_rwlock_arch_t"
.LASF329:
	.string	"UV_HANDLE_ACTIVE"
.LASF132:
	.string	"sys_siglist"
.LASF233:
	.string	"UV_ENOSPC"
.LASF41:
	.string	"_freeres_list"
.LASF359:
	.string	"UV_HANDLE_NON_OVERLAPPED_PIPE"
.LASF381:
	.string	"uv_prepare_stop"
.LASF250:
	.string	"UV_ETIMEDOUT"
.LASF236:
	.string	"UV_ENOTDIR"
.LASF21:
	.string	"_IO_write_base"
.LASF74:
	.string	"__list"
.LASF93:
	.string	"long long int"
.LASF348:
	.string	"UV_HANDLE_CANCELLATION_PENDING"
.LASF96:
	.string	"sockaddr"
.LASF327:
	.string	"UV_HANDLE_CLOSING"
.LASF26:
	.string	"_IO_save_base"
.LASF232:
	.string	"UV_ENOPROTOOPT"
.LASF105:
	.string	"sin_port"
.LASF255:
	.string	"UV_ENXIO"
.LASF102:
	.string	"sockaddr_eon"
.LASF313:
	.string	"uv_prepare_cb"
.LASF126:
	.string	"__u6_addr32"
.LASF118:
	.string	"sockaddr_un"
.LASF177:
	.string	"pevents"
.LASF198:
	.string	"UV_EAI_OVERFLOW"
.LASF275:
	.string	"UV_TCP"
.LASF227:
	.string	"UV_ENOBUFS"
.LASF337:
	.string	"UV_HANDLE_READ_PARTIAL"
.LASF42:
	.string	"_freeres_buf"
.LASF129:
	.string	"in6addr_any"
.LASF27:
	.string	"_IO_backup_base"
.LASF383:
	.string	"uv_prepare_init"
.LASF218:
	.string	"UV_EISCONN"
.LASF106:
	.string	"sin_addr"
.LASF71:
	.string	"__kind"
.LASF311:
	.string	"uv_close_cb"
.LASF85:
	.string	"__pad1"
.LASF86:
	.string	"__pad2"
.LASF80:
	.string	"__pad3"
.LASF81:
	.string	"__pad4"
.LASF43:
	.string	"__pad5"
.LASF217:
	.string	"UV_EIO"
.LASF4:
	.string	"long unsigned int"
.LASF328:
	.string	"UV_HANDLE_CLOSED"
.LASF238:
	.string	"UV_ENOTSOCK"
.LASF274:
	.string	"UV_STREAM"
.LASF368:
	.string	"uv__idle_close"
.LASF143:
	.string	"handle_queue"
.LASF256:
	.string	"UV_EMLINK"
.LASF35:
	.string	"_vtable_offset"
.LASF249:
	.string	"UV_ESRCH"
.LASF0:
	.string	"program_invocation_name"
.LASF44:
	.string	"_mode"
.LASF202:
	.string	"UV_EALREADY"
.LASF159:
	.string	"check_handles"
.LASF65:
	.string	"__pthread_list_t"
.LASF281:
	.string	"UV_HANDLE_TYPE_MAX"
.LASF382:
	.string	"uv_prepare_start"
.LASF268:
	.string	"UV_HANDLE"
.LASF58:
	.string	"uint16_t"
.LASF183:
	.string	"UV_EACCES"
.LASF320:
	.string	"rbe_right"
.LASF156:
	.string	"closing_handles"
.LASF371:
	.string	"uv_idle_start"
.LASF196:
	.string	"UV_EAI_NODATA"
.LASF352:
	.string	"UV_HANDLE_TCP_SINGLE_ACCEPT"
.LASF138:
	.string	"timezone"
.LASF199:
	.string	"UV_EAI_PROTOCOL"
.LASF201:
	.string	"UV_EAI_SOCKTYPE"
.LASF211:
	.string	"UV_EEXIST"
.LASF344:
	.string	"UV_HANDLE_SYNC_BYPASS_IOCP"
.LASF226:
	.string	"UV_ENFILE"
.LASF125:
	.string	"__u6_addr16"
.LASF19:
	.string	"_IO_read_end"
.LASF109:
	.string	"sin6_family"
.LASF277:
	.string	"UV_TTY"
.LASF11:
	.string	"short int"
.LASF175:
	.string	"uv__io_cb"
.LASF234:
	.string	"UV_ENOSYS"
.LASF263:
	.string	"UV_UNKNOWN_HANDLE"
.LASF210:
	.string	"UV_EDESTADDRREQ"
.LASF3:
	.string	"long int"
.LASF319:
	.string	"rbe_left"
.LASF388:
	.string	"__stack_chk_fail"
.LASF367:
	.string	"UV_HANDLE_POLL_SLOW"
.LASF225:
	.string	"UV_ENETUNREACH"
.LASF166:
	.string	"timer_counter"
.LASF214:
	.string	"UV_EHOSTUNREACH"
.LASF49:
	.string	"_IO_wide_data"
.LASF240:
	.string	"UV_EPERM"
.LASF173:
	.string	"inotify_watchers"
.LASF60:
	.string	"uint64_t"
.LASF345:
	.string	"UV_HANDLE_ZERO_READ"
.LASF219:
	.string	"UV_EISDIR"
.LASF180:
	.string	"uv_mutex_t"
.LASF38:
	.string	"_offset"
.LASF289:
	.string	"next_closing"
.LASF342:
	.string	"UV_HANDLE_WRITABLE"
.LASF103:
	.string	"sockaddr_in"
.LASF10:
	.string	"__uint8_t"
.LASF376:
	.string	"uv_check_stop"
.LASF89:
	.string	"__data"
.LASF360:
	.string	"UV_HANDLE_PIPESERVER"
.LASF278:
	.string	"UV_UDP"
.LASF148:
	.string	"pending_queue"
.LASF364:
	.string	"UV_HANDLE_TTY_SAVED_ATTRIBUTES"
.LASF272:
	.string	"UV_PREPARE"
.LASF24:
	.string	"_IO_buf_base"
.LASF135:
	.string	"__timezone"
.LASF174:
	.string	"inotify_fd"
.LASF276:
	.string	"UV_TIMER"
.LASF70:
	.string	"__nusers"
.LASF40:
	.string	"_wide_data"
.LASF375:
	.string	"uv_idle_stop"
.LASF326:
	.string	"QUEUE"
.LASF37:
	.string	"_lock"
.LASF279:
	.string	"UV_SIGNAL"
.LASF127:
	.string	"in6_addr"
.LASF379:
	.string	"uv__prepare_close"
.LASF347:
	.string	"UV_HANDLE_BLOCKING_WRITES"
.LASF48:
	.string	"_IO_codecvt"
.LASF358:
	.string	"UV_HANDLE_UDP_RECVMMSG"
.LASF33:
	.string	"_old_offset"
.LASF242:
	.string	"UV_EPROTO"
.LASF61:
	.string	"_IO_FILE"
.LASF323:
	.string	"unused"
.LASF78:
	.string	"__wrphase_futex"
.LASF162:
	.string	"async_unused"
.LASF239:
	.string	"UV_ENOTSUP"
.LASF310:
	.string	"dispatched_signals"
.LASF293:
	.string	"queue"
.LASF92:
	.string	"pthread_mutex_t"
.LASF265:
	.string	"UV_CHECK"
.LASF67:
	.string	"__lock"
.LASF335:
	.string	"UV_HANDLE_SHUTTING"
.LASF121:
	.string	"in_addr"
.LASF194:
	.string	"UV_EAI_FAMILY"
.LASF287:
	.string	"type"
.LASF339:
	.string	"UV_HANDLE_READING"
.LASF6:
	.string	"unsigned char"
.LASF186:
	.string	"UV_EAFNOSUPPORT"
.LASF13:
	.string	"__uint32_t"
.LASF133:
	.string	"__tzname"
.LASF259:
	.string	"UV_ENOTTY"
.LASF22:
	.string	"_IO_write_ptr"
.LASF76:
	.string	"__readers"
.LASF273:
	.string	"UV_PROCESS"
.LASF308:
	.string	"tree_entry"
.LASF374:
	.string	"uv__run_check"
.LASF39:
	.string	"_codecvt"
.LASF137:
	.string	"daylight"
.LASF334:
	.string	"UV_HANDLE_CONNECTION"
.LASF354:
	.string	"UV_HANDLE_TCP_SOCKET_CLOSED"
.LASF298:
	.string	"uv_idle_s"
.LASF297:
	.string	"uv_idle_t"
.LASF15:
	.string	"__off_t"
.LASF301:
	.string	"uv_async_s"
.LASF300:
	.string	"uv_async_t"
.LASF8:
	.string	"signed char"
.LASF97:
	.string	"sa_family"
.LASF192:
	.string	"UV_EAI_CANCELED"
.LASF209:
	.string	"UV_ECONNRESET"
.LASF163:
	.string	"async_io_watcher"
.LASF370:
	.string	"handle"
.LASF157:
	.string	"process_handles"
.LASF56:
	.string	"_sys_errlist"
.LASF145:
	.string	"stop_flag"
.LASF182:
	.string	"UV_E2BIG"
.LASF168:
	.string	"signal_pipefd"
.LASF355:
	.string	"UV_HANDLE_SHARED_TCP_SOCKET"
.LASF317:
	.string	"reserved"
.LASF160:
	.string	"idle_handles"
.LASF169:
	.string	"signal_io_watcher"
.LASF248:
	.string	"UV_ESPIPE"
.LASF318:
	.string	"double"
.LASF362:
	.string	"UV_HANDLE_TTY_RAW"
.LASF333:
	.string	"UV_HANDLE_LISTENING"
.LASF91:
	.string	"__align"
.LASF306:
	.string	"signal_cb"
.LASF30:
	.string	"_chain"
.LASF140:
	.string	"uv_loop_s"
.LASF283:
	.string	"uv_loop_t"
.LASF158:
	.string	"prepare_handles"
.LASF32:
	.string	"_flags2"
.LASF346:
	.string	"UV_HANDLE_EMULATE_IOCP"
.LASF90:
	.string	"__size"
.LASF384:
	.string	"GNU C89 9.3.0 -m64 -mtune=generic -march=x86-64 -g -O3 -std=gnu90 -fvisibility=hidden -fno-omit-frame-pointer -fasynchronous-unwind-tables -fstack-protector-strong -fstack-clash-protection -fcf-protection"
.LASF108:
	.string	"sockaddr_in6"
.LASF230:
	.string	"UV_ENOMEM"
.LASF257:
	.string	"UV_EHOSTDOWN"
.LASF16:
	.string	"__off64_t"
.LASF45:
	.string	"_unused2"
.LASF66:
	.string	"__pthread_mutex_s"
.LASF216:
	.string	"UV_EINVAL"
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
