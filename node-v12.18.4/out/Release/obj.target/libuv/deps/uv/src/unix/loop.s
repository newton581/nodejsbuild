	.file	"loop.c"
	.text
.Ltext0:
	.p2align 4
	.globl	uv_loop_init
	.type	uv_loop_init, @function
uv_loop_init:
.LVL0:
.LFB100:
	.file 1 "../deps/uv/src/unix/loop.c"
	.loc 1 30 35 view -0
	.cfi_startproc
	.loc 1 30 35 is_stmt 0 view .LVU1
	endbr64
	.loc 1 31 3 is_stmt 1 view .LVU2
	.loc 1 32 3 view .LVU3
	.loc 1 35 3 view .LVU4
	.loc 1 30 35 is_stmt 0 view .LVU5
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
.LBB10:
.LBB11:
	.file 2 "/usr/include/x86_64-linux-gnu/bits/string_fortified.h"
	.loc 2 71 10 view .LVU6
	xorl	%eax, %eax
.LBE11:
.LBE10:
	.loc 1 30 35 view .LVU7
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	.loc 1 30 35 view .LVU8
	movq	%rdi, %rbx
	.loc 1 35 14 view .LVU9
	movq	(%rdi), %rdx
.LVL1:
	.loc 1 36 3 is_stmt 1 view .LVU10
.LBB13:
.LBI10:
	.loc 2 59 42 view .LVU11
.LBB12:
	.loc 2 71 3 view .LVU12
	.loc 2 71 10 is_stmt 0 view .LVU13
	addq	$8, %rdi
.LVL2:
	.loc 2 71 10 view .LVU14
	movq	$0, 832(%rdi)
	movq	%rbx, %rcx
	andq	$-8, %rdi
	subq	%rdi, %rcx
	addl	$848, %ecx
	shrl	$3, %ecx
	rep stosq
.LVL3:
	.loc 2 71 10 view .LVU15
.LBE12:
.LBE13:
	.loc 1 37 3 is_stmt 1 view .LVU16
	.loc 1 40 46 is_stmt 0 view .LVU17
	leaq	120(%rbx), %rax
	.loc 1 37 14 view .LVU18
	movq	%rdx, (%rbx)
	.loc 1 39 3 is_stmt 1 view .LVU19
.LBB14:
.LBI14:
	.file 3 "../deps/uv/src/heap-inl.h"
	.loc 3 62 37 view .LVU20
.LVL4:
.LBB15:
	.loc 3 63 3 view .LVU21
.LBE15:
.LBE14:
.LBB18:
.LBB19:
	.file 4 "../deps/uv/src/unix/internal.h"
	.loc 4 303 16 is_stmt 0 view .LVU22
	movl	$1, %edi
.LBE19:
.LBE18:
	.loc 1 40 46 view .LVU23
	movq	%rax, %xmm0
	.loc 1 42 57 view .LVU24
	leaq	432(%rbx), %rax
.LBB24:
.LBB16:
	.loc 3 63 13 view .LVU25
	movq	$0, 520(%rbx)
	.loc 3 64 3 is_stmt 1 view .LVU26
.LBE16:
.LBE24:
	.loc 1 40 43 is_stmt 0 view .LVU27
	punpcklqdq	%xmm0, %xmm0
	.loc 1 47 24 view .LVU28
	movl	$0, 8(%rbx)
	.loc 1 40 43 view .LVU29
	movups	%xmm0, 120(%rbx)
	.loc 1 42 57 view .LVU30
	movq	%rax, %xmm0
	.loc 1 41 56 view .LVU31
	leaq	416(%rbx), %rax
	movq	%rax, %xmm1
	.loc 1 43 57 view .LVU32
	leaq	400(%rbx), %rax
	.loc 1 44 56 view .LVU33
	punpcklqdq	%xmm0, %xmm0
.LBB25:
.LBB17:
	.loc 3 64 15 view .LVU34
	movl	$0, 528(%rbx)
.LBE17:
.LBE25:
	.loc 1 40 3 is_stmt 1 view .LVU35
	.loc 1 40 8 view .LVU36
	.loc 1 40 58 view .LVU37
	.loc 1 40 116 view .LVU38
	.loc 1 41 3 view .LVU39
	.loc 1 41 8 view .LVU40
	.loc 1 41 78 view .LVU41
	.loc 1 41 156 view .LVU42
	.loc 1 42 3 view .LVU43
	.loc 1 42 8 view .LVU44
	.loc 1 42 80 view .LVU45
	.loc 1 42 160 view .LVU46
	.loc 1 43 3 view .LVU47
	.loc 1 43 8 view .LVU48
	.loc 1 43 80 view .LVU49
	.loc 1 43 160 view .LVU50
	.loc 1 44 3 view .LVU51
	.loc 1 44 8 view .LVU52
	.loc 1 44 84 view .LVU53
	.loc 1 43 57 is_stmt 0 view .LVU54
	movq	%rax, %xmm2
	.loc 1 44 59 view .LVU55
	leaq	384(%rbx), %rax
	.loc 1 44 56 view .LVU56
	movups	%xmm0, 432(%rbx)
	.loc 1 44 168 is_stmt 1 view .LVU57
	.loc 1 45 3 view .LVU58
	.loc 1 45 8 view .LVU59
	.loc 1 45 78 view .LVU60
	.loc 1 44 56 is_stmt 0 view .LVU61
	punpcklqdq	%xmm1, %xmm1
	.loc 1 44 59 view .LVU62
	movq	%rax, %xmm3
	.loc 1 45 56 view .LVU63
	leaq	16(%rbx), %rax
	.loc 1 44 56 view .LVU64
	movups	%xmm1, 416(%rbx)
	punpcklqdq	%xmm2, %xmm2
	.loc 1 45 56 view .LVU65
	movq	%rax, %xmm0
	.loc 1 53 57 view .LVU66
	leaq	88(%rbx), %rax
	.loc 1 48 27 view .LVU67
	movl	$0, 32(%rbx)
	.loc 1 44 56 view .LVU68
	punpcklqdq	%xmm3, %xmm3
	.loc 1 45 53 view .LVU69
	punpcklqdq	%xmm0, %xmm0
	.loc 1 50 18 view .LVU70
	movq	$0, 104(%rbx)
	.loc 1 45 53 view .LVU71
	movups	%xmm0, 16(%rbx)
	.loc 1 45 156 is_stmt 1 view .LVU72
	.loc 1 47 3 view .LVU73
	.loc 1 48 3 view .LVU74
	.loc 1 49 3 view .LVU75
	.loc 1 50 3 view .LVU76
	.loc 1 51 3 view .LVU77
	.loc 1 53 57 is_stmt 0 view .LVU78
	movq	%rax, %xmm0
	.loc 1 52 57 view .LVU79
	leaq	72(%rbx), %rax
	movq	%rax, %xmm1
	.loc 1 51 19 view .LVU80
	movq	$0, 112(%rbx)
	.loc 1 52 3 is_stmt 1 view .LVU81
	.loc 1 52 8 view .LVU82
	.loc 1 52 80 view .LVU83
	.loc 1 52 160 view .LVU84
	.loc 1 53 3 view .LVU85
	.loc 1 53 8 view .LVU86
	.loc 1 53 80 view .LVU87
	.loc 1 52 54 is_stmt 0 view .LVU88
	punpcklqdq	%xmm0, %xmm0
	.loc 1 55 25 view .LVU89
	movq	$0, 360(%rbx)
	.loc 1 52 54 view .LVU90
	punpcklqdq	%xmm1, %xmm1
	.loc 1 44 56 view .LVU91
	movups	%xmm3, 384(%rbx)
	movups	%xmm2, 400(%rbx)
	.loc 1 52 54 view .LVU92
	movups	%xmm1, 72(%rbx)
	movups	%xmm0, 88(%rbx)
	.loc 1 53 160 is_stmt 1 view .LVU93
	.loc 1 55 3 view .LVU94
	.loc 1 56 3 view .LVU95
.LBB26:
.LBI18:
	.loc 4 300 37 view .LVU96
.LVL5:
.LBB20:
	.loc 4 303 3 view .LVU97
	.loc 4 303 16 is_stmt 0 view .LVU98
	call	uv__hrtime@PLT
.LVL6:
	.loc 4 303 16 view .LVU99
.LBE20:
.LBE26:
	.loc 1 61 20 view .LVU100
	movl	$-1, 64(%rbx)
	.loc 1 67 9 view .LVU101
	movq	%rbx, %rdi
.LBB27:
.LBB21:
	.loc 4 303 42 view .LVU102
	movabsq	$4835703278458516699, %rdx
	mulq	%rdx
.LBE21:
.LBE27:
	.loc 1 57 29 view .LVU103
	movl	$-1, 504(%rbx)
	.loc 1 58 19 view .LVU104
	movl	$-1, 512(%rbx)
	.loc 1 59 26 view .LVU105
	movq	$-1, 552(%rbx)
.LBB28:
.LBB22:
	.loc 4 303 42 view .LVU106
	shrq	$18, %rdx
.LBE22:
.LBE28:
	.loc 1 65 19 view .LVU107
	movl	$0, 48(%rbx)
.LBB29:
.LBB23:
	.loc 4 303 42 view .LVU108
	movq	%rdx, 544(%rbx)
.LBE23:
.LBE29:
	.loc 1 57 3 is_stmt 1 view .LVU109
	.loc 1 58 3 view .LVU110
	.loc 1 59 3 view .LVU111
	.loc 1 60 3 view .LVU112
	.loc 1 61 3 view .LVU113
	.loc 1 62 3 view .LVU114
	.loc 1 62 19 is_stmt 0 view .LVU115
	movl	$-1, 768(%rbx)
	.loc 1 64 3 is_stmt 1 view .LVU116
	.loc 1 64 23 is_stmt 0 view .LVU117
	movq	$0, 536(%rbx)
	.loc 1 65 3 is_stmt 1 view .LVU118
	.loc 1 67 3 view .LVU119
	.loc 1 67 9 is_stmt 0 view .LVU120
	call	uv__platform_loop_init@PLT
.LVL7:
	movl	%eax, %r12d
.LVL8:
	.loc 1 68 3 is_stmt 1 view .LVU121
	.loc 1 68 6 is_stmt 0 view .LVU122
	testl	%eax, %eax
	je	.L23
.LVL9:
.L1:
	.loc 1 112 1 view .LVU123
	popq	%rbx
.LVL10:
	.loc 1 112 1 view .LVU124
	movl	%r12d, %eax
	popq	%r12
.LVL11:
	.loc 1 112 1 view .LVU125
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.LVL12:
	.p2align 4,,10
	.p2align 3
.L23:
	.cfi_restore_state
	.loc 1 71 3 is_stmt 1 view .LVU126
	call	uv__signal_global_once_init@PLT
.LVL13:
	.loc 1 72 3 view .LVU127
	.loc 1 72 9 is_stmt 0 view .LVU128
	leaq	616(%rbx), %rsi
	movq	%rbx, %rdi
	call	uv_signal_init@PLT
.LVL14:
	movl	%eax, %r12d
.LVL15:
	.loc 1 73 3 is_stmt 1 view .LVU129
	.loc 1 73 6 is_stmt 0 view .LVU130
	testl	%eax, %eax
	jne	.L3
	.loc 1 76 3 is_stmt 1 view .LVU131
	.loc 1 76 8 view .LVU132
	.loc 1 76 35 is_stmt 0 view .LVU133
	movl	704(%rbx), %eax
.LVL16:
	.loc 1 76 11 view .LVU134
	testb	$8, %al
	je	.L4
	.loc 1 76 73 is_stmt 1 discriminator 2 view .LVU135
	.loc 1 76 103 is_stmt 0 discriminator 2 view .LVU136
	movl	%eax, %edx
	andl	$-9, %edx
	.loc 1 76 122 is_stmt 1 discriminator 2 view .LVU137
	.loc 1 76 125 is_stmt 0 discriminator 2 view .LVU138
	testb	$1, %al
	jne	.L9
	.loc 1 76 191 is_stmt 1 discriminator 4 view .LVU139
	.loc 1 76 226 is_stmt 0 discriminator 4 view .LVU140
	andl	$4, %eax
	movl	%edx, %eax
	.loc 1 76 194 discriminator 4 view .LVU141
	je	.L4
	.loc 1 76 252 is_stmt 1 discriminator 5 view .LVU142
	.loc 1 76 257 discriminator 5 view .LVU143
	.loc 1 76 279 is_stmt 0 discriminator 5 view .LVU144
	movq	624(%rbx), %rdx
	.loc 1 76 301 discriminator 5 view .LVU145
	subl	$1, 8(%rdx)
	.p2align 4,,10
	.p2align 3
.L4:
	.loc 1 76 313 is_stmt 1 discriminator 7 view .LVU146
	.loc 1 76 326 discriminator 7 view .LVU147
	.loc 1 77 3 discriminator 7 view .LVU148
	.loc 1 77 29 is_stmt 0 discriminator 7 view .LVU149
	orl	$16, %eax
	.loc 1 80 9 discriminator 7 view .LVU150
	leaq	304(%rbx), %r13
	.loc 1 77 29 discriminator 7 view .LVU151
	movl	%eax, 704(%rbx)
	.loc 1 78 3 is_stmt 1 discriminator 7 view .LVU152
	.loc 1 78 8 discriminator 7 view .LVU153
	.loc 1 78 84 discriminator 7 view .LVU154
	.loc 1 78 59 is_stmt 0 discriminator 7 view .LVU155
	leaq	368(%rbx), %rax
	.loc 1 80 9 discriminator 7 view .LVU156
	movq	%r13, %rdi
	.loc 1 78 59 discriminator 7 view .LVU157
	movq	%rax, %xmm0
	.loc 1 78 56 discriminator 7 view .LVU158
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 368(%rbx)
	.loc 1 78 168 is_stmt 1 discriminator 7 view .LVU159
	.loc 1 80 3 discriminator 7 view .LVU160
	.loc 1 80 9 is_stmt 0 discriminator 7 view .LVU161
	call	uv_rwlock_init@PLT
.LVL17:
	movl	%eax, %r12d
.LVL18:
	.loc 1 81 3 is_stmt 1 discriminator 7 view .LVU162
	.loc 1 81 6 is_stmt 0 discriminator 7 view .LVU163
	testl	%eax, %eax
	je	.L24
.LVL19:
.L5:
	.loc 1 104 3 is_stmt 1 view .LVU164
	movq	%rbx, %rdi
	call	uv__signal_loop_cleanup@PLT
.LVL20:
.L3:
	.loc 1 107 3 view .LVU165
	movq	%rbx, %rdi
	call	uv__platform_loop_delete@PLT
.LVL21:
	.loc 1 109 3 view .LVU166
	movq	104(%rbx), %rdi
	call	uv__free@PLT
.LVL22:
	.loc 1 110 3 view .LVU167
	.loc 1 112 1 is_stmt 0 view .LVU168
	movl	%r12d, %eax
	.loc 1 110 19 view .LVU169
	movl	$0, 112(%rbx)
	.loc 1 111 3 is_stmt 1 view .LVU170
	.loc 1 112 1 is_stmt 0 view .LVU171
	popq	%rbx
.LVL23:
	.loc 1 112 1 view .LVU172
	popq	%r12
.LVL24:
	.loc 1 112 1 view .LVU173
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.LVL25:
	.p2align 4,,10
	.p2align 3
.L24:
	.cfi_restore_state
	.loc 1 84 3 is_stmt 1 view .LVU174
	.loc 1 84 9 is_stmt 0 view .LVU175
	leaq	136(%rbx), %r14
	movq	%r14, %rdi
	call	uv_mutex_init@PLT
.LVL26:
	.loc 1 84 9 view .LVU176
	movl	%eax, %r12d
.LVL27:
	.loc 1 85 3 is_stmt 1 view .LVU177
	.loc 1 85 6 is_stmt 0 view .LVU178
	testl	%eax, %eax
	jne	.L6
	.loc 1 88 3 is_stmt 1 view .LVU179
	.loc 1 88 9 is_stmt 0 view .LVU180
	movq	uv__work_done@GOTPCREL(%rip), %rdx
	leaq	176(%rbx), %rsi
	movq	%rbx, %rdi
	call	uv_async_init@PLT
.LVL28:
	.loc 1 88 9 view .LVU181
	movl	%eax, %r12d
.LVL29:
	.loc 1 89 3 is_stmt 1 view .LVU182
	.loc 1 89 6 is_stmt 0 view .LVU183
	testl	%eax, %eax
	jne	.L25
	.loc 1 92 3 is_stmt 1 view .LVU184
	.loc 1 92 8 view .LVU185
	.loc 1 92 30 is_stmt 0 view .LVU186
	movl	264(%rbx), %eax
.LVL30:
	.loc 1 92 11 view .LVU187
	testb	$8, %al
	je	.L8
	.loc 1 92 68 is_stmt 1 discriminator 2 view .LVU188
	.loc 1 92 93 is_stmt 0 discriminator 2 view .LVU189
	movl	%eax, %edx
	andl	$-9, %edx
	.loc 1 92 112 is_stmt 1 discriminator 2 view .LVU190
	.loc 1 92 115 is_stmt 0 discriminator 2 view .LVU191
	testb	$1, %al
	jne	.L11
	.loc 1 92 176 is_stmt 1 discriminator 4 view .LVU192
	.loc 1 92 206 is_stmt 0 discriminator 4 view .LVU193
	andl	$4, %eax
	movl	%edx, %eax
	.loc 1 92 179 discriminator 4 view .LVU194
	je	.L8
	.loc 1 92 232 is_stmt 1 discriminator 5 view .LVU195
	.loc 1 92 237 discriminator 5 view .LVU196
	.loc 1 92 254 is_stmt 0 discriminator 5 view .LVU197
	movq	184(%rbx), %rdx
	.loc 1 92 276 discriminator 5 view .LVU198
	subl	$1, 8(%rdx)
	.p2align 4,,10
	.p2align 3
.L8:
	.loc 1 92 288 is_stmt 1 discriminator 7 view .LVU199
	.loc 1 92 301 discriminator 7 view .LVU200
	.loc 1 93 3 discriminator 7 view .LVU201
	.loc 1 93 24 is_stmt 0 discriminator 7 view .LVU202
	orl	$16, %eax
	movl	%eax, 264(%rbx)
	.loc 1 95 3 is_stmt 1 discriminator 7 view .LVU203
	.loc 1 95 10 is_stmt 0 discriminator 7 view .LVU204
	jmp	.L1
.LVL31:
	.p2align 4,,10
	.p2align 3
.L25:
	.loc 1 90 5 is_stmt 1 view .LVU205
.LDL1:
	.loc 1 98 3 view .LVU206
	movq	%r14, %rdi
	call	uv_mutex_destroy@PLT
.LVL32:
	.p2align 4,,10
	.p2align 3
.L6:
	.loc 1 101 3 view .LVU207
	movq	%r13, %rdi
	call	uv_rwlock_destroy@PLT
.LVL33:
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L9:
	.loc 1 101 3 is_stmt 0 view .LVU208
	movl	%edx, %eax
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L11:
	.loc 1 101 3 view .LVU209
	movl	%edx, %eax
	jmp	.L8
	.cfi_endproc
.LFE100:
	.size	uv_loop_init, .-uv_loop_init
	.p2align 4
	.globl	uv_loop_fork
	.type	uv_loop_fork, @function
uv_loop_fork:
.LVL34:
.LFB101:
	.loc 1 115 35 is_stmt 1 view -0
	.cfi_startproc
	.loc 1 115 35 is_stmt 0 view .LVU211
	endbr64
	.loc 1 116 3 is_stmt 1 view .LVU212
	.loc 1 117 3 view .LVU213
	.loc 1 118 3 view .LVU214
	.loc 1 120 3 view .LVU215
	.loc 1 115 35 is_stmt 0 view .LVU216
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	.loc 1 120 9 view .LVU217
	call	uv__io_fork@PLT
.LVL35:
	.loc 1 121 3 is_stmt 1 view .LVU218
	.loc 1 121 6 is_stmt 0 view .LVU219
	testl	%eax, %eax
	je	.L37
.L26:
	.loc 1 145 1 view .LVU220
	addq	$8, %rsp
	popq	%rbx
.LVL36:
	.loc 1 145 1 view .LVU221
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.LVL37:
	.p2align 4,,10
	.p2align 3
.L37:
	.cfi_restore_state
	.loc 1 124 3 is_stmt 1 view .LVU222
	.loc 1 124 9 is_stmt 0 view .LVU223
	movq	%rbx, %rdi
	call	uv__async_fork@PLT
.LVL38:
	.loc 1 125 3 is_stmt 1 view .LVU224
	.loc 1 125 6 is_stmt 0 view .LVU225
	testl	%eax, %eax
	jne	.L26
	.loc 1 128 3 is_stmt 1 view .LVU226
	.loc 1 128 9 is_stmt 0 view .LVU227
	movq	%rbx, %rdi
	call	uv__signal_loop_fork@PLT
.LVL39:
	.loc 1 129 3 is_stmt 1 view .LVU228
	.loc 1 129 6 is_stmt 0 view .LVU229
	testl	%eax, %eax
	jne	.L26
.LVL40:
	.loc 1 133 15 is_stmt 1 view .LVU230
	.loc 1 133 23 is_stmt 0 view .LVU231
	movl	112(%rbx), %edx
	.loc 1 133 3 view .LVU232
	testl	%edx, %edx
	je	.L26
	subl	$1, %edx
	.loc 1 140 58 view .LVU233
	leaq	88(%rbx), %r9
	leaq	8(,%rdx,8), %rsi
	.loc 1 133 3 view .LVU234
	xorl	%edx, %edx
	jmp	.L30
.LVL41:
	.p2align 4,,10
	.p2align 3
.L29:
	.loc 1 140 343 is_stmt 1 discriminator 1 view .LVU235
	.loc 1 133 36 discriminator 1 view .LVU236
	.loc 1 133 15 discriminator 1 view .LVU237
	addq	$8, %rdx
	.loc 1 133 3 is_stmt 0 discriminator 1 view .LVU238
	cmpq	%rdx, %rsi
	je	.L26
.LVL42:
.L30:
	.loc 1 134 5 is_stmt 1 view .LVU239
	.loc 1 134 7 is_stmt 0 view .LVU240
	movq	104(%rbx), %rcx
	movq	(%rcx,%rdx), %rcx
.LVL43:
	.loc 1 135 5 is_stmt 1 view .LVU241
	.loc 1 135 8 is_stmt 0 view .LVU242
	testq	%rcx, %rcx
	je	.L29
	.loc 1 138 5 is_stmt 1 view .LVU243
	.loc 1 138 8 is_stmt 0 view .LVU244
	movl	40(%rcx), %edi
	testl	%edi, %edi
	je	.L29
	.loc 1 138 46 discriminator 1 view .LVU245
	leaq	24(%rcx), %rdi
	.loc 1 138 25 discriminator 1 view .LVU246
	cmpq	24(%rcx), %rdi
	jne	.L29
	.loc 1 139 7 is_stmt 1 view .LVU247
	.loc 1 140 58 is_stmt 0 view .LVU248
	movq	%r9, 24(%rcx)
	.loc 1 140 166 view .LVU249
	movq	96(%rbx), %r8
	.loc 1 139 17 view .LVU250
	movl	$0, 44(%rcx)
	.loc 1 140 7 is_stmt 1 view .LVU251
	.loc 1 140 12 view .LVU252
	.loc 1 140 81 view .LVU253
	.loc 1 140 124 is_stmt 0 view .LVU254
	movq	%r8, 32(%rcx)
	.loc 1 140 173 is_stmt 1 view .LVU255
	.loc 1 140 243 is_stmt 0 view .LVU256
	movq	%rdi, (%r8)
	.loc 1 140 266 is_stmt 1 view .LVU257
	.loc 1 140 312 is_stmt 0 view .LVU258
	movq	%rdi, 96(%rbx)
	jmp	.L29
	.cfi_endproc
.LFE101:
	.size	uv_loop_fork, .-uv_loop_fork
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"../deps/uv/src/unix/loop.c"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"QUEUE_EMPTY(&loop->wq) && \"thread pool work queue not empty!\""
	.section	.rodata.str1.1
.LC2:
	.string	"!uv__has_active_reqs(loop)"
	.text
	.p2align 4
	.globl	uv__loop_close
	.hidden	uv__loop_close
	.type	uv__loop_close, @function
uv__loop_close:
.LVL44:
.LFB102:
	.loc 1 148 38 is_stmt 1 view -0
	.cfi_startproc
	.loc 1 148 38 is_stmt 0 view .LVU260
	endbr64
	.loc 1 149 3 is_stmt 1 view .LVU261
	.loc 1 148 38 is_stmt 0 view .LVU262
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	.loc 1 148 38 view .LVU263
	movq	%rdi, %rbx
	.loc 1 149 3 view .LVU264
	call	uv__signal_loop_cleanup@PLT
.LVL45:
	.loc 1 150 3 is_stmt 1 view .LVU265
	movq	%rbx, %rdi
	call	uv__platform_loop_delete@PLT
.LVL46:
	.loc 1 151 3 view .LVU266
	movq	%rbx, %rdi
	call	uv__async_stop@PLT
.LVL47:
	.loc 1 153 3 view .LVU267
	.loc 1 153 11 is_stmt 0 view .LVU268
	movl	768(%rbx), %edi
	.loc 1 153 6 view .LVU269
	cmpl	$-1, %edi
	je	.L39
	.loc 1 154 5 is_stmt 1 view .LVU270
	call	uv__close@PLT
.LVL48:
	.loc 1 155 5 view .LVU271
	.loc 1 155 21 is_stmt 0 view .LVU272
	movl	$-1, 768(%rbx)
.L39:
	.loc 1 158 3 is_stmt 1 view .LVU273
	.loc 1 158 11 is_stmt 0 view .LVU274
	movl	64(%rbx), %edi
	.loc 1 158 6 view .LVU275
	cmpl	$-1, %edi
	je	.L40
	.loc 1 159 5 is_stmt 1 view .LVU276
	call	uv__close@PLT
.LVL49:
	.loc 1 160 5 view .LVU277
	.loc 1 160 22 is_stmt 0 view .LVU278
	movl	$-1, 64(%rbx)
.L40:
	.loc 1 163 3 is_stmt 1 view .LVU279
	leaq	136(%rbx), %r12
	movq	%r12, %rdi
	call	uv_mutex_lock@PLT
.LVL50:
	.loc 1 164 2 view .LVU280
	.loc 1 164 20 is_stmt 0 view .LVU281
	leaq	120(%rbx), %rax
	.loc 1 164 34 view .LVU282
	cmpq	%rax, 120(%rbx)
	jne	.L50
	.loc 1 165 2 is_stmt 1 view .LVU283
	.loc 1 165 34 is_stmt 0 view .LVU284
	movl	32(%rbx), %eax
	testl	%eax, %eax
	jne	.L51
	.loc 1 166 3 is_stmt 1 view .LVU285
	movq	%r12, %rdi
	call	uv_mutex_unlock@PLT
.LVL51:
	.loc 1 167 3 view .LVU286
	movq	%r12, %rdi
	call	uv_mutex_destroy@PLT
.LVL52:
	.loc 1 173 3 view .LVU287
	leaq	304(%rbx), %rdi
	call	uv_rwlock_destroy@PLT
.LVL53:
	.loc 1 181 3 view .LVU288
	movq	104(%rbx), %rdi
	call	uv__free@PLT
.LVL54:
	.loc 1 182 3 view .LVU289
	.loc 1 182 18 is_stmt 0 view .LVU290
	movq	$0, 104(%rbx)
	.loc 1 183 3 is_stmt 1 view .LVU291
	.loc 1 183 19 is_stmt 0 view .LVU292
	movl	$0, 112(%rbx)
	.loc 1 184 1 view .LVU293
	popq	%rbx
.LVL55:
	.loc 1 184 1 view .LVU294
	popq	%r12
.LVL56:
	.loc 1 184 1 view .LVU295
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.LVL57:
.L50:
	.cfi_restore_state
	.loc 1 164 11 is_stmt 1 discriminator 1 view .LVU296
	leaq	__PRETTY_FUNCTION__.10050(%rip), %rcx
	movl	$164, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	__assert_fail@PLT
.LVL58:
.L51:
	.loc 1 165 11 discriminator 1 view .LVU297
	leaq	__PRETTY_FUNCTION__.10050(%rip), %rcx
	movl	$165, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	__assert_fail@PLT
.LVL59:
	.cfi_endproc
.LFE102:
	.size	uv__loop_close, .-uv__loop_close
	.p2align 4
	.globl	uv__loop_configure
	.hidden	uv__loop_configure
	.type	uv__loop_configure, @function
uv__loop_configure:
.LVL60:
.LFB103:
	.loc 1 187 76 view -0
	.cfi_startproc
	.loc 1 187 76 is_stmt 0 view .LVU299
	endbr64
	.loc 1 188 3 is_stmt 1 view .LVU300
	.loc 1 188 6 is_stmt 0 view .LVU301
	testl	%esi, %esi
	jne	.L56
	.loc 1 191 3 is_stmt 1 view .LVU302
	.loc 1 191 6 is_stmt 0 view .LVU303
	movl	(%rdx), %eax
	cmpl	$47, %eax
	ja	.L54
	movl	%eax, %ecx
	addl	$8, %eax
	addq	16(%rdx), %rcx
	movl	%eax, (%rdx)
.L55:
	.loc 1 191 6 view .LVU304
	cmpl	$27, (%rcx)
	jne	.L57
	.loc 1 194 3 is_stmt 1 view .LVU305
	.loc 1 194 15 is_stmt 0 view .LVU306
	orq	$1, 56(%rdi)
	.loc 1 195 3 is_stmt 1 view .LVU307
	.loc 1 195 10 is_stmt 0 view .LVU308
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L54:
	.loc 1 191 6 view .LVU309
	movq	8(%rdx), %rcx
	leaq	8(%rcx), %rax
	movq	%rax, 8(%rdx)
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L57:
	.loc 1 192 12 view .LVU310
	movl	$-22, %eax
	.loc 1 196 1 view .LVU311
	ret
	.p2align 4,,10
	.p2align 3
.L56:
	.loc 1 189 12 view .LVU312
	movl	$-38, %eax
	ret
	.cfi_endproc
.LFE103:
	.size	uv__loop_configure, .-uv__loop_configure
	.section	.rodata
	.align 8
	.type	__PRETTY_FUNCTION__.10050, @object
	.size	__PRETTY_FUNCTION__.10050, 15
__PRETTY_FUNCTION__.10050:
	.string	"uv__loop_close"
	.text
.Letext0:
	.file 5 "/usr/include/errno.h"
	.file 6 "<built-in>"
	.file 7 "/usr/lib/gcc/x86_64-linux-gnu/9/include/stddef.h"
	.file 8 "/usr/include/x86_64-linux-gnu/bits/types.h"
	.file 9 "/usr/include/x86_64-linux-gnu/bits/types/struct_FILE.h"
	.file 10 "/usr/include/x86_64-linux-gnu/bits/types/FILE.h"
	.file 11 "/usr/include/stdio.h"
	.file 12 "/usr/include/x86_64-linux-gnu/bits/sys_errlist.h"
	.file 13 "/usr/include/x86_64-linux-gnu/bits/stdint-uintn.h"
	.file 14 "/usr/include/x86_64-linux-gnu/bits/thread-shared-types.h"
	.file 15 "/usr/include/x86_64-linux-gnu/bits/struct_mutex.h"
	.file 16 "/usr/include/x86_64-linux-gnu/bits/struct_rwlock.h"
	.file 17 "/usr/include/x86_64-linux-gnu/bits/pthreadtypes.h"
	.file 18 "/usr/include/x86_64-linux-gnu/bits/sockaddr.h"
	.file 19 "/usr/include/x86_64-linux-gnu/bits/socket.h"
	.file 20 "/usr/include/netinet/in.h"
	.file 21 "/usr/include/signal.h"
	.file 22 "/usr/include/time.h"
	.file 23 "../deps/uv/include/uv.h"
	.file 24 "../deps/uv/include/uv/unix.h"
	.file 25 "../deps/uv/src/queue.h"
	.file 26 "../deps/uv/src/uv-common.h"
	.file 27 "/usr/include/unistd.h"
	.file 28 "/usr/include/x86_64-linux-gnu/bits/getopt_core.h"
	.file 29 "/usr/include/assert.h"
	.section	.debug_info,"",@progbits
.Ldebug_info0:
	.long	0x1bb2
	.value	0x4
	.long	.Ldebug_abbrev0
	.byte	0x8
	.uleb128 0x1
	.long	.LASF410
	.byte	0x1
	.long	.LASF411
	.long	.LASF412
	.quad	.Ltext0
	.quad	.Letext0-.Ltext0
	.long	.Ldebug_line0
	.uleb128 0x2
	.long	.LASF0
	.byte	0x5
	.byte	0x2d
	.byte	0xe
	.long	0x39
	.uleb128 0x3
	.byte	0x8
	.long	0x3f
	.uleb128 0x4
	.byte	0x1
	.byte	0x6
	.long	.LASF2
	.uleb128 0x5
	.long	0x3f
	.uleb128 0x2
	.long	.LASF1
	.byte	0x5
	.byte	0x2e
	.byte	0xe
	.long	0x39
	.uleb128 0x6
	.byte	0x4
	.byte	0x5
	.string	"int"
	.uleb128 0x4
	.byte	0x8
	.byte	0x5
	.long	.LASF3
	.uleb128 0x7
	.long	.LASF13
	.byte	0x7
	.byte	0xd1
	.byte	0x1b
	.long	0x71
	.uleb128 0x4
	.byte	0x8
	.byte	0x7
	.long	.LASF4
	.uleb128 0x8
	.long	.LASF413
	.byte	0x18
	.byte	0x6
	.byte	0
	.long	0xb5
	.uleb128 0x9
	.long	.LASF5
	.byte	0x6
	.byte	0
	.long	0xb5
	.byte	0
	.uleb128 0x9
	.long	.LASF6
	.byte	0x6
	.byte	0
	.long	0xb5
	.byte	0x4
	.uleb128 0x9
	.long	.LASF7
	.byte	0x6
	.byte	0
	.long	0xbc
	.byte	0x8
	.uleb128 0x9
	.long	.LASF8
	.byte	0x6
	.byte	0
	.long	0xbc
	.byte	0x10
	.byte	0
	.uleb128 0x4
	.byte	0x4
	.byte	0x7
	.long	.LASF9
	.uleb128 0xa
	.byte	0x8
	.uleb128 0x4
	.byte	0x1
	.byte	0x8
	.long	.LASF10
	.uleb128 0x4
	.byte	0x2
	.byte	0x7
	.long	.LASF11
	.uleb128 0x4
	.byte	0x1
	.byte	0x6
	.long	.LASF12
	.uleb128 0x7
	.long	.LASF14
	.byte	0x8
	.byte	0x26
	.byte	0x17
	.long	0xbe
	.uleb128 0x4
	.byte	0x2
	.byte	0x5
	.long	.LASF15
	.uleb128 0x7
	.long	.LASF16
	.byte	0x8
	.byte	0x28
	.byte	0x1c
	.long	0xc5
	.uleb128 0x7
	.long	.LASF17
	.byte	0x8
	.byte	0x2a
	.byte	0x16
	.long	0xb5
	.uleb128 0x7
	.long	.LASF18
	.byte	0x8
	.byte	0x2d
	.byte	0x1b
	.long	0x71
	.uleb128 0x7
	.long	.LASF19
	.byte	0x8
	.byte	0x98
	.byte	0x12
	.long	0x5e
	.uleb128 0x7
	.long	.LASF20
	.byte	0x8
	.byte	0x99
	.byte	0x12
	.long	0x5e
	.uleb128 0xb
	.long	0x57
	.long	0x132
	.uleb128 0xc
	.long	0x71
	.byte	0x1
	.byte	0
	.uleb128 0xd
	.long	.LASF65
	.byte	0xd8
	.byte	0x9
	.byte	0x31
	.byte	0x8
	.long	0x2b9
	.uleb128 0xe
	.long	.LASF21
	.byte	0x9
	.byte	0x33
	.byte	0x7
	.long	0x57
	.byte	0
	.uleb128 0xe
	.long	.LASF22
	.byte	0x9
	.byte	0x36
	.byte	0x9
	.long	0x39
	.byte	0x8
	.uleb128 0xe
	.long	.LASF23
	.byte	0x9
	.byte	0x37
	.byte	0x9
	.long	0x39
	.byte	0x10
	.uleb128 0xe
	.long	.LASF24
	.byte	0x9
	.byte	0x38
	.byte	0x9
	.long	0x39
	.byte	0x18
	.uleb128 0xe
	.long	.LASF25
	.byte	0x9
	.byte	0x39
	.byte	0x9
	.long	0x39
	.byte	0x20
	.uleb128 0xe
	.long	.LASF26
	.byte	0x9
	.byte	0x3a
	.byte	0x9
	.long	0x39
	.byte	0x28
	.uleb128 0xe
	.long	.LASF27
	.byte	0x9
	.byte	0x3b
	.byte	0x9
	.long	0x39
	.byte	0x30
	.uleb128 0xe
	.long	.LASF28
	.byte	0x9
	.byte	0x3c
	.byte	0x9
	.long	0x39
	.byte	0x38
	.uleb128 0xe
	.long	.LASF29
	.byte	0x9
	.byte	0x3d
	.byte	0x9
	.long	0x39
	.byte	0x40
	.uleb128 0xe
	.long	.LASF30
	.byte	0x9
	.byte	0x40
	.byte	0x9
	.long	0x39
	.byte	0x48
	.uleb128 0xe
	.long	.LASF31
	.byte	0x9
	.byte	0x41
	.byte	0x9
	.long	0x39
	.byte	0x50
	.uleb128 0xe
	.long	.LASF32
	.byte	0x9
	.byte	0x42
	.byte	0x9
	.long	0x39
	.byte	0x58
	.uleb128 0xe
	.long	.LASF33
	.byte	0x9
	.byte	0x44
	.byte	0x16
	.long	0x2d2
	.byte	0x60
	.uleb128 0xe
	.long	.LASF34
	.byte	0x9
	.byte	0x46
	.byte	0x14
	.long	0x2d8
	.byte	0x68
	.uleb128 0xe
	.long	.LASF35
	.byte	0x9
	.byte	0x48
	.byte	0x7
	.long	0x57
	.byte	0x70
	.uleb128 0xe
	.long	.LASF36
	.byte	0x9
	.byte	0x49
	.byte	0x7
	.long	0x57
	.byte	0x74
	.uleb128 0xe
	.long	.LASF37
	.byte	0x9
	.byte	0x4a
	.byte	0xb
	.long	0x10a
	.byte	0x78
	.uleb128 0xe
	.long	.LASF38
	.byte	0x9
	.byte	0x4d
	.byte	0x12
	.long	0xc5
	.byte	0x80
	.uleb128 0xe
	.long	.LASF39
	.byte	0x9
	.byte	0x4e
	.byte	0xf
	.long	0xcc
	.byte	0x82
	.uleb128 0xe
	.long	.LASF40
	.byte	0x9
	.byte	0x4f
	.byte	0x8
	.long	0x2de
	.byte	0x83
	.uleb128 0xe
	.long	.LASF41
	.byte	0x9
	.byte	0x51
	.byte	0xf
	.long	0x2ee
	.byte	0x88
	.uleb128 0xe
	.long	.LASF42
	.byte	0x9
	.byte	0x59
	.byte	0xd
	.long	0x116
	.byte	0x90
	.uleb128 0xe
	.long	.LASF43
	.byte	0x9
	.byte	0x5b
	.byte	0x17
	.long	0x2f9
	.byte	0x98
	.uleb128 0xe
	.long	.LASF44
	.byte	0x9
	.byte	0x5c
	.byte	0x19
	.long	0x304
	.byte	0xa0
	.uleb128 0xe
	.long	.LASF45
	.byte	0x9
	.byte	0x5d
	.byte	0x14
	.long	0x2d8
	.byte	0xa8
	.uleb128 0xe
	.long	.LASF46
	.byte	0x9
	.byte	0x5e
	.byte	0x9
	.long	0xbc
	.byte	0xb0
	.uleb128 0xe
	.long	.LASF47
	.byte	0x9
	.byte	0x5f
	.byte	0xa
	.long	0x65
	.byte	0xb8
	.uleb128 0xe
	.long	.LASF48
	.byte	0x9
	.byte	0x60
	.byte	0x7
	.long	0x57
	.byte	0xc0
	.uleb128 0xe
	.long	.LASF49
	.byte	0x9
	.byte	0x62
	.byte	0x8
	.long	0x30a
	.byte	0xc4
	.byte	0
	.uleb128 0x7
	.long	.LASF50
	.byte	0xa
	.byte	0x7
	.byte	0x19
	.long	0x132
	.uleb128 0xf
	.long	.LASF414
	.byte	0x9
	.byte	0x2b
	.byte	0xe
	.uleb128 0x10
	.long	.LASF51
	.uleb128 0x3
	.byte	0x8
	.long	0x2cd
	.uleb128 0x3
	.byte	0x8
	.long	0x132
	.uleb128 0xb
	.long	0x3f
	.long	0x2ee
	.uleb128 0xc
	.long	0x71
	.byte	0
	.byte	0
	.uleb128 0x3
	.byte	0x8
	.long	0x2c5
	.uleb128 0x10
	.long	.LASF52
	.uleb128 0x3
	.byte	0x8
	.long	0x2f4
	.uleb128 0x10
	.long	.LASF53
	.uleb128 0x3
	.byte	0x8
	.long	0x2ff
	.uleb128 0xb
	.long	0x3f
	.long	0x31a
	.uleb128 0xc
	.long	0x71
	.byte	0x13
	.byte	0
	.uleb128 0x3
	.byte	0x8
	.long	0x46
	.uleb128 0x5
	.long	0x31a
	.uleb128 0x2
	.long	.LASF54
	.byte	0xb
	.byte	0x89
	.byte	0xe
	.long	0x331
	.uleb128 0x3
	.byte	0x8
	.long	0x2b9
	.uleb128 0x2
	.long	.LASF55
	.byte	0xb
	.byte	0x8a
	.byte	0xe
	.long	0x331
	.uleb128 0x2
	.long	.LASF56
	.byte	0xb
	.byte	0x8b
	.byte	0xe
	.long	0x331
	.uleb128 0x2
	.long	.LASF57
	.byte	0xc
	.byte	0x1a
	.byte	0xc
	.long	0x57
	.uleb128 0xb
	.long	0x320
	.long	0x366
	.uleb128 0x11
	.byte	0
	.uleb128 0x5
	.long	0x35b
	.uleb128 0x2
	.long	.LASF58
	.byte	0xc
	.byte	0x1b
	.byte	0x1a
	.long	0x366
	.uleb128 0x2
	.long	.LASF59
	.byte	0xc
	.byte	0x1e
	.byte	0xc
	.long	0x57
	.uleb128 0x2
	.long	.LASF60
	.byte	0xc
	.byte	0x1f
	.byte	0x1a
	.long	0x366
	.uleb128 0x7
	.long	.LASF61
	.byte	0xd
	.byte	0x18
	.byte	0x13
	.long	0xd3
	.uleb128 0x7
	.long	.LASF62
	.byte	0xd
	.byte	0x19
	.byte	0x14
	.long	0xe6
	.uleb128 0x7
	.long	.LASF63
	.byte	0xd
	.byte	0x1a
	.byte	0x14
	.long	0xf2
	.uleb128 0x7
	.long	.LASF64
	.byte	0xd
	.byte	0x1b
	.byte	0x14
	.long	0xfe
	.uleb128 0xd
	.long	.LASF66
	.byte	0x10
	.byte	0xe
	.byte	0x31
	.byte	0x10
	.long	0x3e7
	.uleb128 0xe
	.long	.LASF67
	.byte	0xe
	.byte	0x33
	.byte	0x23
	.long	0x3e7
	.byte	0
	.uleb128 0xe
	.long	.LASF68
	.byte	0xe
	.byte	0x34
	.byte	0x23
	.long	0x3e7
	.byte	0x8
	.byte	0
	.uleb128 0x3
	.byte	0x8
	.long	0x3bf
	.uleb128 0x7
	.long	.LASF69
	.byte	0xe
	.byte	0x35
	.byte	0x3
	.long	0x3bf
	.uleb128 0xd
	.long	.LASF70
	.byte	0x28
	.byte	0xf
	.byte	0x16
	.byte	0x8
	.long	0x46f
	.uleb128 0xe
	.long	.LASF71
	.byte	0xf
	.byte	0x18
	.byte	0x7
	.long	0x57
	.byte	0
	.uleb128 0xe
	.long	.LASF72
	.byte	0xf
	.byte	0x19
	.byte	0x10
	.long	0xb5
	.byte	0x4
	.uleb128 0xe
	.long	.LASF73
	.byte	0xf
	.byte	0x1a
	.byte	0x7
	.long	0x57
	.byte	0x8
	.uleb128 0xe
	.long	.LASF74
	.byte	0xf
	.byte	0x1c
	.byte	0x10
	.long	0xb5
	.byte	0xc
	.uleb128 0xe
	.long	.LASF75
	.byte	0xf
	.byte	0x20
	.byte	0x7
	.long	0x57
	.byte	0x10
	.uleb128 0xe
	.long	.LASF76
	.byte	0xf
	.byte	0x22
	.byte	0x9
	.long	0xdf
	.byte	0x14
	.uleb128 0xe
	.long	.LASF77
	.byte	0xf
	.byte	0x23
	.byte	0x9
	.long	0xdf
	.byte	0x16
	.uleb128 0xe
	.long	.LASF78
	.byte	0xf
	.byte	0x24
	.byte	0x14
	.long	0x3ed
	.byte	0x18
	.byte	0
	.uleb128 0xd
	.long	.LASF79
	.byte	0x38
	.byte	0x10
	.byte	0x17
	.byte	0x8
	.long	0x519
	.uleb128 0xe
	.long	.LASF80
	.byte	0x10
	.byte	0x19
	.byte	0x10
	.long	0xb5
	.byte	0
	.uleb128 0xe
	.long	.LASF81
	.byte	0x10
	.byte	0x1a
	.byte	0x10
	.long	0xb5
	.byte	0x4
	.uleb128 0xe
	.long	.LASF82
	.byte	0x10
	.byte	0x1b
	.byte	0x10
	.long	0xb5
	.byte	0x8
	.uleb128 0xe
	.long	.LASF83
	.byte	0x10
	.byte	0x1c
	.byte	0x10
	.long	0xb5
	.byte	0xc
	.uleb128 0xe
	.long	.LASF84
	.byte	0x10
	.byte	0x1d
	.byte	0x10
	.long	0xb5
	.byte	0x10
	.uleb128 0xe
	.long	.LASF85
	.byte	0x10
	.byte	0x1e
	.byte	0x10
	.long	0xb5
	.byte	0x14
	.uleb128 0xe
	.long	.LASF86
	.byte	0x10
	.byte	0x20
	.byte	0x7
	.long	0x57
	.byte	0x18
	.uleb128 0xe
	.long	.LASF87
	.byte	0x10
	.byte	0x21
	.byte	0x7
	.long	0x57
	.byte	0x1c
	.uleb128 0xe
	.long	.LASF88
	.byte	0x10
	.byte	0x22
	.byte	0xf
	.long	0xcc
	.byte	0x20
	.uleb128 0xe
	.long	.LASF89
	.byte	0x10
	.byte	0x27
	.byte	0x11
	.long	0x519
	.byte	0x21
	.uleb128 0xe
	.long	.LASF90
	.byte	0x10
	.byte	0x2a
	.byte	0x15
	.long	0x71
	.byte	0x28
	.uleb128 0xe
	.long	.LASF91
	.byte	0x10
	.byte	0x2d
	.byte	0x10
	.long	0xb5
	.byte	0x30
	.byte	0
	.uleb128 0xb
	.long	0xbe
	.long	0x529
	.uleb128 0xc
	.long	0x71
	.byte	0x6
	.byte	0
	.uleb128 0x4
	.byte	0x8
	.byte	0x7
	.long	.LASF92
	.uleb128 0xb
	.long	0x3f
	.long	0x540
	.uleb128 0xc
	.long	0x71
	.byte	0x37
	.byte	0
	.uleb128 0x12
	.byte	0x28
	.byte	0x11
	.byte	0x43
	.byte	0x9
	.long	0x56e
	.uleb128 0x13
	.long	.LASF93
	.byte	0x11
	.byte	0x45
	.byte	0x1c
	.long	0x3f9
	.uleb128 0x13
	.long	.LASF94
	.byte	0x11
	.byte	0x46
	.byte	0x8
	.long	0x56e
	.uleb128 0x13
	.long	.LASF95
	.byte	0x11
	.byte	0x47
	.byte	0xc
	.long	0x5e
	.byte	0
	.uleb128 0xb
	.long	0x3f
	.long	0x57e
	.uleb128 0xc
	.long	0x71
	.byte	0x27
	.byte	0
	.uleb128 0x7
	.long	.LASF96
	.byte	0x11
	.byte	0x48
	.byte	0x3
	.long	0x540
	.uleb128 0x4
	.byte	0x8
	.byte	0x5
	.long	.LASF97
	.uleb128 0x12
	.byte	0x38
	.byte	0x11
	.byte	0x56
	.byte	0x9
	.long	0x5bf
	.uleb128 0x13
	.long	.LASF93
	.byte	0x11
	.byte	0x58
	.byte	0x22
	.long	0x46f
	.uleb128 0x13
	.long	.LASF94
	.byte	0x11
	.byte	0x59
	.byte	0x8
	.long	0x530
	.uleb128 0x13
	.long	.LASF95
	.byte	0x11
	.byte	0x5a
	.byte	0xc
	.long	0x5e
	.byte	0
	.uleb128 0x7
	.long	.LASF98
	.byte	0x11
	.byte	0x5b
	.byte	0x3
	.long	0x591
	.uleb128 0x7
	.long	.LASF99
	.byte	0x12
	.byte	0x1c
	.byte	0x1c
	.long	0xc5
	.uleb128 0xd
	.long	.LASF100
	.byte	0x10
	.byte	0x13
	.byte	0xb2
	.byte	0x8
	.long	0x5ff
	.uleb128 0xe
	.long	.LASF101
	.byte	0x13
	.byte	0xb4
	.byte	0x11
	.long	0x5cb
	.byte	0
	.uleb128 0xe
	.long	.LASF102
	.byte	0x13
	.byte	0xb5
	.byte	0xa
	.long	0x604
	.byte	0x2
	.byte	0
	.uleb128 0x5
	.long	0x5d7
	.uleb128 0xb
	.long	0x3f
	.long	0x614
	.uleb128 0xc
	.long	0x71
	.byte	0xd
	.byte	0
	.uleb128 0x3
	.byte	0x8
	.long	0x5d7
	.uleb128 0x14
	.long	0x614
	.uleb128 0x10
	.long	.LASF103
	.uleb128 0x5
	.long	0x61f
	.uleb128 0x3
	.byte	0x8
	.long	0x61f
	.uleb128 0x14
	.long	0x629
	.uleb128 0x10
	.long	.LASF104
	.uleb128 0x5
	.long	0x634
	.uleb128 0x3
	.byte	0x8
	.long	0x634
	.uleb128 0x14
	.long	0x63e
	.uleb128 0x10
	.long	.LASF105
	.uleb128 0x5
	.long	0x649
	.uleb128 0x3
	.byte	0x8
	.long	0x649
	.uleb128 0x14
	.long	0x653
	.uleb128 0x10
	.long	.LASF106
	.uleb128 0x5
	.long	0x65e
	.uleb128 0x3
	.byte	0x8
	.long	0x65e
	.uleb128 0x14
	.long	0x668
	.uleb128 0xd
	.long	.LASF107
	.byte	0x10
	.byte	0x14
	.byte	0xee
	.byte	0x8
	.long	0x6b5
	.uleb128 0xe
	.long	.LASF108
	.byte	0x14
	.byte	0xf0
	.byte	0x11
	.long	0x5cb
	.byte	0
	.uleb128 0xe
	.long	.LASF109
	.byte	0x14
	.byte	0xf1
	.byte	0xf
	.long	0x85c
	.byte	0x2
	.uleb128 0xe
	.long	.LASF110
	.byte	0x14
	.byte	0xf2
	.byte	0x14
	.long	0x841
	.byte	0x4
	.uleb128 0xe
	.long	.LASF111
	.byte	0x14
	.byte	0xf5
	.byte	0x13
	.long	0x8fe
	.byte	0x8
	.byte	0
	.uleb128 0x5
	.long	0x673
	.uleb128 0x3
	.byte	0x8
	.long	0x673
	.uleb128 0x14
	.long	0x6ba
	.uleb128 0xd
	.long	.LASF112
	.byte	0x1c
	.byte	0x14
	.byte	0xfd
	.byte	0x8
	.long	0x718
	.uleb128 0xe
	.long	.LASF113
	.byte	0x14
	.byte	0xff
	.byte	0x11
	.long	0x5cb
	.byte	0
	.uleb128 0x15
	.long	.LASF114
	.byte	0x14
	.value	0x100
	.byte	0xf
	.long	0x85c
	.byte	0x2
	.uleb128 0x15
	.long	.LASF115
	.byte	0x14
	.value	0x101
	.byte	0xe
	.long	0x3a7
	.byte	0x4
	.uleb128 0x15
	.long	.LASF116
	.byte	0x14
	.value	0x102
	.byte	0x15
	.long	0x8c6
	.byte	0x8
	.uleb128 0x15
	.long	.LASF117
	.byte	0x14
	.value	0x103
	.byte	0xe
	.long	0x3a7
	.byte	0x18
	.byte	0
	.uleb128 0x5
	.long	0x6c5
	.uleb128 0x3
	.byte	0x8
	.long	0x6c5
	.uleb128 0x14
	.long	0x71d
	.uleb128 0x10
	.long	.LASF118
	.uleb128 0x5
	.long	0x728
	.uleb128 0x3
	.byte	0x8
	.long	0x728
	.uleb128 0x14
	.long	0x732
	.uleb128 0x10
	.long	.LASF119
	.uleb128 0x5
	.long	0x73d
	.uleb128 0x3
	.byte	0x8
	.long	0x73d
	.uleb128 0x14
	.long	0x747
	.uleb128 0x10
	.long	.LASF120
	.uleb128 0x5
	.long	0x752
	.uleb128 0x3
	.byte	0x8
	.long	0x752
	.uleb128 0x14
	.long	0x75c
	.uleb128 0x10
	.long	.LASF121
	.uleb128 0x5
	.long	0x767
	.uleb128 0x3
	.byte	0x8
	.long	0x767
	.uleb128 0x14
	.long	0x771
	.uleb128 0x10
	.long	.LASF122
	.uleb128 0x5
	.long	0x77c
	.uleb128 0x3
	.byte	0x8
	.long	0x77c
	.uleb128 0x14
	.long	0x786
	.uleb128 0x10
	.long	.LASF123
	.uleb128 0x5
	.long	0x791
	.uleb128 0x3
	.byte	0x8
	.long	0x791
	.uleb128 0x14
	.long	0x79b
	.uleb128 0x3
	.byte	0x8
	.long	0x5ff
	.uleb128 0x14
	.long	0x7a6
	.uleb128 0x3
	.byte	0x8
	.long	0x624
	.uleb128 0x14
	.long	0x7b1
	.uleb128 0x3
	.byte	0x8
	.long	0x639
	.uleb128 0x14
	.long	0x7bc
	.uleb128 0x3
	.byte	0x8
	.long	0x64e
	.uleb128 0x14
	.long	0x7c7
	.uleb128 0x3
	.byte	0x8
	.long	0x663
	.uleb128 0x14
	.long	0x7d2
	.uleb128 0x3
	.byte	0x8
	.long	0x6b5
	.uleb128 0x14
	.long	0x7dd
	.uleb128 0x3
	.byte	0x8
	.long	0x718
	.uleb128 0x14
	.long	0x7e8
	.uleb128 0x3
	.byte	0x8
	.long	0x72d
	.uleb128 0x14
	.long	0x7f3
	.uleb128 0x3
	.byte	0x8
	.long	0x742
	.uleb128 0x14
	.long	0x7fe
	.uleb128 0x3
	.byte	0x8
	.long	0x757
	.uleb128 0x14
	.long	0x809
	.uleb128 0x3
	.byte	0x8
	.long	0x76c
	.uleb128 0x14
	.long	0x814
	.uleb128 0x3
	.byte	0x8
	.long	0x781
	.uleb128 0x14
	.long	0x81f
	.uleb128 0x3
	.byte	0x8
	.long	0x796
	.uleb128 0x14
	.long	0x82a
	.uleb128 0x7
	.long	.LASF124
	.byte	0x14
	.byte	0x1e
	.byte	0x12
	.long	0x3a7
	.uleb128 0xd
	.long	.LASF125
	.byte	0x4
	.byte	0x14
	.byte	0x1f
	.byte	0x8
	.long	0x85c
	.uleb128 0xe
	.long	.LASF126
	.byte	0x14
	.byte	0x21
	.byte	0xf
	.long	0x835
	.byte	0
	.byte	0
	.uleb128 0x7
	.long	.LASF127
	.byte	0x14
	.byte	0x77
	.byte	0x12
	.long	0x39b
	.uleb128 0x12
	.byte	0x10
	.byte	0x14
	.byte	0xd6
	.byte	0x5
	.long	0x896
	.uleb128 0x13
	.long	.LASF128
	.byte	0x14
	.byte	0xd8
	.byte	0xa
	.long	0x896
	.uleb128 0x13
	.long	.LASF129
	.byte	0x14
	.byte	0xd9
	.byte	0xb
	.long	0x8a6
	.uleb128 0x13
	.long	.LASF130
	.byte	0x14
	.byte	0xda
	.byte	0xb
	.long	0x8b6
	.byte	0
	.uleb128 0xb
	.long	0x38f
	.long	0x8a6
	.uleb128 0xc
	.long	0x71
	.byte	0xf
	.byte	0
	.uleb128 0xb
	.long	0x39b
	.long	0x8b6
	.uleb128 0xc
	.long	0x71
	.byte	0x7
	.byte	0
	.uleb128 0xb
	.long	0x3a7
	.long	0x8c6
	.uleb128 0xc
	.long	0x71
	.byte	0x3
	.byte	0
	.uleb128 0xd
	.long	.LASF131
	.byte	0x10
	.byte	0x14
	.byte	0xd4
	.byte	0x8
	.long	0x8e1
	.uleb128 0xe
	.long	.LASF132
	.byte	0x14
	.byte	0xdb
	.byte	0x9
	.long	0x868
	.byte	0
	.byte	0
	.uleb128 0x5
	.long	0x8c6
	.uleb128 0x2
	.long	.LASF133
	.byte	0x14
	.byte	0xe4
	.byte	0x1e
	.long	0x8e1
	.uleb128 0x2
	.long	.LASF134
	.byte	0x14
	.byte	0xe5
	.byte	0x1e
	.long	0x8e1
	.uleb128 0xb
	.long	0xbe
	.long	0x90e
	.uleb128 0xc
	.long	0x71
	.byte	0x7
	.byte	0
	.uleb128 0x3
	.byte	0x8
	.long	0x39
	.uleb128 0x16
	.uleb128 0x3
	.byte	0x8
	.long	0x914
	.uleb128 0xb
	.long	0x320
	.long	0x92b
	.uleb128 0xc
	.long	0x71
	.byte	0x40
	.byte	0
	.uleb128 0x5
	.long	0x91b
	.uleb128 0x17
	.long	.LASF135
	.byte	0x15
	.value	0x11e
	.byte	0x1a
	.long	0x92b
	.uleb128 0x17
	.long	.LASF136
	.byte	0x15
	.value	0x11f
	.byte	0x1a
	.long	0x92b
	.uleb128 0xb
	.long	0x39
	.long	0x95a
	.uleb128 0xc
	.long	0x71
	.byte	0x1
	.byte	0
	.uleb128 0x2
	.long	.LASF137
	.byte	0x16
	.byte	0x9f
	.byte	0xe
	.long	0x94a
	.uleb128 0x2
	.long	.LASF138
	.byte	0x16
	.byte	0xa0
	.byte	0xc
	.long	0x57
	.uleb128 0x2
	.long	.LASF139
	.byte	0x16
	.byte	0xa1
	.byte	0x11
	.long	0x5e
	.uleb128 0x2
	.long	.LASF140
	.byte	0x16
	.byte	0xa6
	.byte	0xe
	.long	0x94a
	.uleb128 0x2
	.long	.LASF141
	.byte	0x16
	.byte	0xae
	.byte	0xc
	.long	0x57
	.uleb128 0x2
	.long	.LASF142
	.byte	0x16
	.byte	0xaf
	.byte	0x11
	.long	0x5e
	.uleb128 0x17
	.long	.LASF143
	.byte	0x16
	.value	0x112
	.byte	0xc
	.long	0x57
	.uleb128 0xb
	.long	0xbc
	.long	0x9bf
	.uleb128 0xc
	.long	0x71
	.byte	0x3
	.byte	0
	.uleb128 0x18
	.long	.LASF144
	.value	0x350
	.byte	0x17
	.value	0x6ea
	.byte	0x8
	.long	0xbde
	.uleb128 0x15
	.long	.LASF145
	.byte	0x17
	.value	0x6ec
	.byte	0x9
	.long	0xbc
	.byte	0
	.uleb128 0x15
	.long	.LASF146
	.byte	0x17
	.value	0x6ee
	.byte	0x10
	.long	0xb5
	.byte	0x8
	.uleb128 0x15
	.long	.LASF147
	.byte	0x17
	.value	0x6ef
	.byte	0x9
	.long	0xbe4
	.byte	0x10
	.uleb128 0x15
	.long	.LASF148
	.byte	0x17
	.value	0x6f3
	.byte	0x5
	.long	0x12ba
	.byte	0x20
	.uleb128 0x15
	.long	.LASF149
	.byte	0x17
	.value	0x6f5
	.byte	0x10
	.long	0xb5
	.byte	0x30
	.uleb128 0x15
	.long	.LASF150
	.byte	0x17
	.value	0x6f6
	.byte	0x11
	.long	0x71
	.byte	0x38
	.uleb128 0x15
	.long	.LASF151
	.byte	0x17
	.value	0x6f6
	.byte	0x1c
	.long	0x57
	.byte	0x40
	.uleb128 0x15
	.long	.LASF152
	.byte	0x17
	.value	0x6f6
	.byte	0x2e
	.long	0xbe4
	.byte	0x48
	.uleb128 0x15
	.long	.LASF153
	.byte	0x17
	.value	0x6f6
	.byte	0x46
	.long	0xbe4
	.byte	0x58
	.uleb128 0x15
	.long	.LASF154
	.byte	0x17
	.value	0x6f6
	.byte	0x63
	.long	0x1309
	.byte	0x68
	.uleb128 0x15
	.long	.LASF155
	.byte	0x17
	.value	0x6f6
	.byte	0x7a
	.long	0xb5
	.byte	0x70
	.uleb128 0x15
	.long	.LASF156
	.byte	0x17
	.value	0x6f6
	.byte	0x92
	.long	0xb5
	.byte	0x74
	.uleb128 0x19
	.string	"wq"
	.byte	0x17
	.value	0x6f6
	.byte	0x9e
	.long	0xbe4
	.byte	0x78
	.uleb128 0x15
	.long	.LASF157
	.byte	0x17
	.value	0x6f6
	.byte	0xb0
	.long	0xc87
	.byte	0x88
	.uleb128 0x15
	.long	.LASF158
	.byte	0x17
	.value	0x6f6
	.byte	0xc5
	.long	0xfe4
	.byte	0xb0
	.uleb128 0x1a
	.long	.LASF159
	.byte	0x17
	.value	0x6f6
	.byte	0xdb
	.long	0xc93
	.value	0x130
	.uleb128 0x1a
	.long	.LASF160
	.byte	0x17
	.value	0x6f6
	.byte	0xf6
	.long	0x1187
	.value	0x168
	.uleb128 0x1b
	.long	.LASF161
	.byte	0x17
	.value	0x6f6
	.value	0x10d
	.long	0xbe4
	.value	0x170
	.uleb128 0x1b
	.long	.LASF162
	.byte	0x17
	.value	0x6f6
	.value	0x127
	.long	0xbe4
	.value	0x180
	.uleb128 0x1b
	.long	.LASF163
	.byte	0x17
	.value	0x6f6
	.value	0x141
	.long	0xbe4
	.value	0x190
	.uleb128 0x1b
	.long	.LASF164
	.byte	0x17
	.value	0x6f6
	.value	0x159
	.long	0xbe4
	.value	0x1a0
	.uleb128 0x1b
	.long	.LASF165
	.byte	0x17
	.value	0x6f6
	.value	0x170
	.long	0xbe4
	.value	0x1b0
	.uleb128 0x1b
	.long	.LASF166
	.byte	0x17
	.value	0x6f6
	.value	0x189
	.long	0x915
	.value	0x1c0
	.uleb128 0x1b
	.long	.LASF167
	.byte	0x17
	.value	0x6f6
	.value	0x1a7
	.long	0xc7b
	.value	0x1c8
	.uleb128 0x1b
	.long	.LASF168
	.byte	0x17
	.value	0x6f6
	.value	0x1bd
	.long	0x57
	.value	0x200
	.uleb128 0x1b
	.long	.LASF169
	.byte	0x17
	.value	0x6f6
	.value	0x1f2
	.long	0x12df
	.value	0x208
	.uleb128 0x1b
	.long	.LASF170
	.byte	0x17
	.value	0x6f6
	.value	0x207
	.long	0x3b3
	.value	0x218
	.uleb128 0x1b
	.long	.LASF171
	.byte	0x17
	.value	0x6f6
	.value	0x21f
	.long	0x3b3
	.value	0x220
	.uleb128 0x1b
	.long	.LASF172
	.byte	0x17
	.value	0x6f6
	.value	0x229
	.long	0x122
	.value	0x228
	.uleb128 0x1b
	.long	.LASF173
	.byte	0x17
	.value	0x6f6
	.value	0x244
	.long	0xc7b
	.value	0x230
	.uleb128 0x1b
	.long	.LASF174
	.byte	0x17
	.value	0x6f6
	.value	0x263
	.long	0x1097
	.value	0x268
	.uleb128 0x1b
	.long	.LASF175
	.byte	0x17
	.value	0x6f6
	.value	0x276
	.long	0x57
	.value	0x300
	.uleb128 0x1b
	.long	.LASF176
	.byte	0x17
	.value	0x6f6
	.value	0x28a
	.long	0xc7b
	.value	0x308
	.uleb128 0x1b
	.long	.LASF177
	.byte	0x17
	.value	0x6f6
	.value	0x2a6
	.long	0xbc
	.value	0x340
	.uleb128 0x1b
	.long	.LASF178
	.byte	0x17
	.value	0x6f6
	.value	0x2bc
	.long	0x57
	.value	0x348
	.byte	0
	.uleb128 0x3
	.byte	0x8
	.long	0x9bf
	.uleb128 0xb
	.long	0xbc
	.long	0xbf4
	.uleb128 0xc
	.long	0x71
	.byte	0x1
	.byte	0
	.uleb128 0x7
	.long	.LASF179
	.byte	0x18
	.byte	0x59
	.byte	0x10
	.long	0xc00
	.uleb128 0x3
	.byte	0x8
	.long	0xc06
	.uleb128 0x1c
	.long	0xc1b
	.uleb128 0x1d
	.long	0xbde
	.uleb128 0x1d
	.long	0xc1b
	.uleb128 0x1d
	.long	0xb5
	.byte	0
	.uleb128 0x3
	.byte	0x8
	.long	0xc21
	.uleb128 0xd
	.long	.LASF180
	.byte	0x38
	.byte	0x18
	.byte	0x5e
	.byte	0x8
	.long	0xc7b
	.uleb128 0x1e
	.string	"cb"
	.byte	0x18
	.byte	0x5f
	.byte	0xd
	.long	0xbf4
	.byte	0
	.uleb128 0xe
	.long	.LASF152
	.byte	0x18
	.byte	0x60
	.byte	0x9
	.long	0xbe4
	.byte	0x8
	.uleb128 0xe
	.long	.LASF153
	.byte	0x18
	.byte	0x61
	.byte	0x9
	.long	0xbe4
	.byte	0x18
	.uleb128 0xe
	.long	.LASF181
	.byte	0x18
	.byte	0x62
	.byte	0x10
	.long	0xb5
	.byte	0x28
	.uleb128 0xe
	.long	.LASF182
	.byte	0x18
	.byte	0x63
	.byte	0x10
	.long	0xb5
	.byte	0x2c
	.uleb128 0x1e
	.string	"fd"
	.byte	0x18
	.byte	0x64
	.byte	0x7
	.long	0x57
	.byte	0x30
	.byte	0
	.uleb128 0x7
	.long	.LASF183
	.byte	0x18
	.byte	0x5c
	.byte	0x19
	.long	0xc21
	.uleb128 0x7
	.long	.LASF184
	.byte	0x18
	.byte	0x87
	.byte	0x19
	.long	0x57e
	.uleb128 0x7
	.long	.LASF185
	.byte	0x18
	.byte	0x88
	.byte	0x1a
	.long	0x5bf
	.uleb128 0x1f
	.byte	0x5
	.byte	0x4
	.long	0x57
	.byte	0x17
	.byte	0xb6
	.byte	0xe
	.long	0xec2
	.uleb128 0x20
	.long	.LASF186
	.sleb128 -7
	.uleb128 0x20
	.long	.LASF187
	.sleb128 -13
	.uleb128 0x20
	.long	.LASF188
	.sleb128 -98
	.uleb128 0x20
	.long	.LASF189
	.sleb128 -99
	.uleb128 0x20
	.long	.LASF190
	.sleb128 -97
	.uleb128 0x20
	.long	.LASF191
	.sleb128 -11
	.uleb128 0x20
	.long	.LASF192
	.sleb128 -3000
	.uleb128 0x20
	.long	.LASF193
	.sleb128 -3001
	.uleb128 0x20
	.long	.LASF194
	.sleb128 -3002
	.uleb128 0x20
	.long	.LASF195
	.sleb128 -3013
	.uleb128 0x20
	.long	.LASF196
	.sleb128 -3003
	.uleb128 0x20
	.long	.LASF197
	.sleb128 -3004
	.uleb128 0x20
	.long	.LASF198
	.sleb128 -3005
	.uleb128 0x20
	.long	.LASF199
	.sleb128 -3006
	.uleb128 0x20
	.long	.LASF200
	.sleb128 -3007
	.uleb128 0x20
	.long	.LASF201
	.sleb128 -3008
	.uleb128 0x20
	.long	.LASF202
	.sleb128 -3009
	.uleb128 0x20
	.long	.LASF203
	.sleb128 -3014
	.uleb128 0x20
	.long	.LASF204
	.sleb128 -3010
	.uleb128 0x20
	.long	.LASF205
	.sleb128 -3011
	.uleb128 0x20
	.long	.LASF206
	.sleb128 -114
	.uleb128 0x20
	.long	.LASF207
	.sleb128 -9
	.uleb128 0x20
	.long	.LASF208
	.sleb128 -16
	.uleb128 0x20
	.long	.LASF209
	.sleb128 -125
	.uleb128 0x20
	.long	.LASF210
	.sleb128 -4080
	.uleb128 0x20
	.long	.LASF211
	.sleb128 -103
	.uleb128 0x20
	.long	.LASF212
	.sleb128 -111
	.uleb128 0x20
	.long	.LASF213
	.sleb128 -104
	.uleb128 0x20
	.long	.LASF214
	.sleb128 -89
	.uleb128 0x20
	.long	.LASF215
	.sleb128 -17
	.uleb128 0x20
	.long	.LASF216
	.sleb128 -14
	.uleb128 0x20
	.long	.LASF217
	.sleb128 -27
	.uleb128 0x20
	.long	.LASF218
	.sleb128 -113
	.uleb128 0x20
	.long	.LASF219
	.sleb128 -4
	.uleb128 0x20
	.long	.LASF220
	.sleb128 -22
	.uleb128 0x20
	.long	.LASF221
	.sleb128 -5
	.uleb128 0x20
	.long	.LASF222
	.sleb128 -106
	.uleb128 0x20
	.long	.LASF223
	.sleb128 -21
	.uleb128 0x20
	.long	.LASF224
	.sleb128 -40
	.uleb128 0x20
	.long	.LASF225
	.sleb128 -24
	.uleb128 0x20
	.long	.LASF226
	.sleb128 -90
	.uleb128 0x20
	.long	.LASF227
	.sleb128 -36
	.uleb128 0x20
	.long	.LASF228
	.sleb128 -100
	.uleb128 0x20
	.long	.LASF229
	.sleb128 -101
	.uleb128 0x20
	.long	.LASF230
	.sleb128 -23
	.uleb128 0x20
	.long	.LASF231
	.sleb128 -105
	.uleb128 0x20
	.long	.LASF232
	.sleb128 -19
	.uleb128 0x20
	.long	.LASF233
	.sleb128 -2
	.uleb128 0x20
	.long	.LASF234
	.sleb128 -12
	.uleb128 0x20
	.long	.LASF235
	.sleb128 -64
	.uleb128 0x20
	.long	.LASF236
	.sleb128 -92
	.uleb128 0x20
	.long	.LASF237
	.sleb128 -28
	.uleb128 0x20
	.long	.LASF238
	.sleb128 -38
	.uleb128 0x20
	.long	.LASF239
	.sleb128 -107
	.uleb128 0x20
	.long	.LASF240
	.sleb128 -20
	.uleb128 0x20
	.long	.LASF241
	.sleb128 -39
	.uleb128 0x20
	.long	.LASF242
	.sleb128 -88
	.uleb128 0x20
	.long	.LASF243
	.sleb128 -95
	.uleb128 0x20
	.long	.LASF244
	.sleb128 -1
	.uleb128 0x20
	.long	.LASF245
	.sleb128 -32
	.uleb128 0x20
	.long	.LASF246
	.sleb128 -71
	.uleb128 0x20
	.long	.LASF247
	.sleb128 -93
	.uleb128 0x20
	.long	.LASF248
	.sleb128 -91
	.uleb128 0x20
	.long	.LASF249
	.sleb128 -34
	.uleb128 0x20
	.long	.LASF250
	.sleb128 -30
	.uleb128 0x20
	.long	.LASF251
	.sleb128 -108
	.uleb128 0x20
	.long	.LASF252
	.sleb128 -29
	.uleb128 0x20
	.long	.LASF253
	.sleb128 -3
	.uleb128 0x20
	.long	.LASF254
	.sleb128 -110
	.uleb128 0x20
	.long	.LASF255
	.sleb128 -26
	.uleb128 0x20
	.long	.LASF256
	.sleb128 -18
	.uleb128 0x20
	.long	.LASF257
	.sleb128 -4094
	.uleb128 0x20
	.long	.LASF258
	.sleb128 -4095
	.uleb128 0x20
	.long	.LASF259
	.sleb128 -6
	.uleb128 0x20
	.long	.LASF260
	.sleb128 -31
	.uleb128 0x20
	.long	.LASF261
	.sleb128 -112
	.uleb128 0x20
	.long	.LASF262
	.sleb128 -121
	.uleb128 0x20
	.long	.LASF263
	.sleb128 -25
	.uleb128 0x20
	.long	.LASF264
	.sleb128 -4028
	.uleb128 0x20
	.long	.LASF265
	.sleb128 -84
	.uleb128 0x20
	.long	.LASF266
	.sleb128 -4096
	.byte	0
	.uleb128 0x1f
	.byte	0x7
	.byte	0x4
	.long	0xb5
	.byte	0x17
	.byte	0xbd
	.byte	0xe
	.long	0xf43
	.uleb128 0x21
	.long	.LASF267
	.byte	0
	.uleb128 0x21
	.long	.LASF268
	.byte	0x1
	.uleb128 0x21
	.long	.LASF269
	.byte	0x2
	.uleb128 0x21
	.long	.LASF270
	.byte	0x3
	.uleb128 0x21
	.long	.LASF271
	.byte	0x4
	.uleb128 0x21
	.long	.LASF272
	.byte	0x5
	.uleb128 0x21
	.long	.LASF273
	.byte	0x6
	.uleb128 0x21
	.long	.LASF274
	.byte	0x7
	.uleb128 0x21
	.long	.LASF275
	.byte	0x8
	.uleb128 0x21
	.long	.LASF276
	.byte	0x9
	.uleb128 0x21
	.long	.LASF277
	.byte	0xa
	.uleb128 0x21
	.long	.LASF278
	.byte	0xb
	.uleb128 0x21
	.long	.LASF279
	.byte	0xc
	.uleb128 0x21
	.long	.LASF280
	.byte	0xd
	.uleb128 0x21
	.long	.LASF281
	.byte	0xe
	.uleb128 0x21
	.long	.LASF282
	.byte	0xf
	.uleb128 0x21
	.long	.LASF283
	.byte	0x10
	.uleb128 0x21
	.long	.LASF284
	.byte	0x11
	.uleb128 0x21
	.long	.LASF285
	.byte	0x12
	.byte	0
	.uleb128 0x7
	.long	.LASF286
	.byte	0x17
	.byte	0xc4
	.byte	0x3
	.long	0xec2
	.uleb128 0x7
	.long	.LASF287
	.byte	0x17
	.byte	0xd1
	.byte	0x1a
	.long	0x9bf
	.uleb128 0x7
	.long	.LASF288
	.byte	0x17
	.byte	0xd2
	.byte	0x1c
	.long	0xf67
	.uleb128 0x22
	.long	.LASF289
	.byte	0x60
	.byte	0x17
	.value	0x1bb
	.byte	0x8
	.long	0xfe4
	.uleb128 0x15
	.long	.LASF145
	.byte	0x17
	.value	0x1bc
	.byte	0x9
	.long	0xbc
	.byte	0
	.uleb128 0x15
	.long	.LASF290
	.byte	0x17
	.value	0x1bc
	.byte	0x1a
	.long	0x121c
	.byte	0x8
	.uleb128 0x15
	.long	.LASF291
	.byte	0x17
	.value	0x1bc
	.byte	0x2f
	.long	0xf43
	.byte	0x10
	.uleb128 0x15
	.long	.LASF292
	.byte	0x17
	.value	0x1bc
	.byte	0x41
	.long	0x118d
	.byte	0x18
	.uleb128 0x15
	.long	.LASF147
	.byte	0x17
	.value	0x1bc
	.byte	0x51
	.long	0xbe4
	.byte	0x20
	.uleb128 0x19
	.string	"u"
	.byte	0x17
	.value	0x1bc
	.byte	0x87
	.long	0x11f8
	.byte	0x30
	.uleb128 0x15
	.long	.LASF293
	.byte	0x17
	.value	0x1bc
	.byte	0x97
	.long	0x1187
	.byte	0x50
	.uleb128 0x15
	.long	.LASF150
	.byte	0x17
	.value	0x1bc
	.byte	0xb2
	.long	0xb5
	.byte	0x58
	.byte	0
	.uleb128 0x7
	.long	.LASF294
	.byte	0x17
	.byte	0xde
	.byte	0x1b
	.long	0xff0
	.uleb128 0x22
	.long	.LASF295
	.byte	0x80
	.byte	0x17
	.value	0x344
	.byte	0x8
	.long	0x1097
	.uleb128 0x15
	.long	.LASF145
	.byte	0x17
	.value	0x345
	.byte	0x9
	.long	0xbc
	.byte	0
	.uleb128 0x15
	.long	.LASF290
	.byte	0x17
	.value	0x345
	.byte	0x1a
	.long	0x121c
	.byte	0x8
	.uleb128 0x15
	.long	.LASF291
	.byte	0x17
	.value	0x345
	.byte	0x2f
	.long	0xf43
	.byte	0x10
	.uleb128 0x15
	.long	.LASF292
	.byte	0x17
	.value	0x345
	.byte	0x41
	.long	0x118d
	.byte	0x18
	.uleb128 0x15
	.long	.LASF147
	.byte	0x17
	.value	0x345
	.byte	0x51
	.long	0xbe4
	.byte	0x20
	.uleb128 0x19
	.string	"u"
	.byte	0x17
	.value	0x345
	.byte	0x87
	.long	0x1222
	.byte	0x30
	.uleb128 0x15
	.long	.LASF293
	.byte	0x17
	.value	0x345
	.byte	0x97
	.long	0x1187
	.byte	0x50
	.uleb128 0x15
	.long	.LASF150
	.byte	0x17
	.value	0x345
	.byte	0xb2
	.long	0xb5
	.byte	0x58
	.uleb128 0x15
	.long	.LASF296
	.byte	0x17
	.value	0x346
	.byte	0xf
	.long	0x11ab
	.byte	0x60
	.uleb128 0x15
	.long	.LASF297
	.byte	0x17
	.value	0x346
	.byte	0x1f
	.long	0xbe4
	.byte	0x68
	.uleb128 0x15
	.long	.LASF298
	.byte	0x17
	.value	0x346
	.byte	0x2d
	.long	0x57
	.byte	0x78
	.byte	0
	.uleb128 0x7
	.long	.LASF299
	.byte	0x17
	.byte	0xe2
	.byte	0x1c
	.long	0x10a3
	.uleb128 0x22
	.long	.LASF300
	.byte	0x98
	.byte	0x17
	.value	0x61c
	.byte	0x8
	.long	0x1166
	.uleb128 0x15
	.long	.LASF145
	.byte	0x17
	.value	0x61d
	.byte	0x9
	.long	0xbc
	.byte	0
	.uleb128 0x15
	.long	.LASF290
	.byte	0x17
	.value	0x61d
	.byte	0x1a
	.long	0x121c
	.byte	0x8
	.uleb128 0x15
	.long	.LASF291
	.byte	0x17
	.value	0x61d
	.byte	0x2f
	.long	0xf43
	.byte	0x10
	.uleb128 0x15
	.long	.LASF292
	.byte	0x17
	.value	0x61d
	.byte	0x41
	.long	0x118d
	.byte	0x18
	.uleb128 0x15
	.long	.LASF147
	.byte	0x17
	.value	0x61d
	.byte	0x51
	.long	0xbe4
	.byte	0x20
	.uleb128 0x19
	.string	"u"
	.byte	0x17
	.value	0x61d
	.byte	0x87
	.long	0x124d
	.byte	0x30
	.uleb128 0x15
	.long	.LASF293
	.byte	0x17
	.value	0x61d
	.byte	0x97
	.long	0x1187
	.byte	0x50
	.uleb128 0x15
	.long	.LASF150
	.byte	0x17
	.value	0x61d
	.byte	0xb2
	.long	0xb5
	.byte	0x58
	.uleb128 0x15
	.long	.LASF301
	.byte	0x17
	.value	0x61e
	.byte	0x10
	.long	0x11cf
	.byte	0x60
	.uleb128 0x15
	.long	.LASF302
	.byte	0x17
	.value	0x61f
	.byte	0x7
	.long	0x57
	.byte	0x68
	.uleb128 0x15
	.long	.LASF303
	.byte	0x17
	.value	0x620
	.byte	0x7a
	.long	0x1271
	.byte	0x70
	.uleb128 0x15
	.long	.LASF304
	.byte	0x17
	.value	0x620
	.byte	0x93
	.long	0xb5
	.byte	0x90
	.uleb128 0x15
	.long	.LASF305
	.byte	0x17
	.value	0x620
	.byte	0xb0
	.long	0xb5
	.byte	0x94
	.byte	0
	.uleb128 0x1f
	.byte	0x7
	.byte	0x4
	.long	0xb5
	.byte	0x17
	.byte	0xf9
	.byte	0xe
	.long	0x117b
	.uleb128 0x21
	.long	.LASF306
	.byte	0
	.byte	0
	.uleb128 0x7
	.long	.LASF307
	.byte	0x17
	.byte	0xfb
	.byte	0x3
	.long	0x1166
	.uleb128 0x3
	.byte	0x8
	.long	0xf5b
	.uleb128 0x23
	.long	.LASF308
	.byte	0x17
	.value	0x13e
	.byte	0x10
	.long	0x119a
	.uleb128 0x3
	.byte	0x8
	.long	0x11a0
	.uleb128 0x1c
	.long	0x11ab
	.uleb128 0x1d
	.long	0x1187
	.byte	0
	.uleb128 0x23
	.long	.LASF309
	.byte	0x17
	.value	0x141
	.byte	0x10
	.long	0x11b8
	.uleb128 0x3
	.byte	0x8
	.long	0x11be
	.uleb128 0x1c
	.long	0x11c9
	.uleb128 0x1d
	.long	0x11c9
	.byte	0
	.uleb128 0x3
	.byte	0x8
	.long	0xfe4
	.uleb128 0x23
	.long	.LASF310
	.byte	0x17
	.value	0x17a
	.byte	0x10
	.long	0x11dc
	.uleb128 0x3
	.byte	0x8
	.long	0x11e2
	.uleb128 0x1c
	.long	0x11f2
	.uleb128 0x1d
	.long	0x11f2
	.uleb128 0x1d
	.long	0x57
	.byte	0
	.uleb128 0x3
	.byte	0x8
	.long	0x1097
	.uleb128 0x24
	.byte	0x20
	.byte	0x17
	.value	0x1bc
	.byte	0x62
	.long	0x121c
	.uleb128 0x25
	.string	"fd"
	.byte	0x17
	.value	0x1bc
	.byte	0x6e
	.long	0x57
	.uleb128 0x26
	.long	.LASF311
	.byte	0x17
	.value	0x1bc
	.byte	0x78
	.long	0x9af
	.byte	0
	.uleb128 0x3
	.byte	0x8
	.long	0xf4f
	.uleb128 0x24
	.byte	0x20
	.byte	0x17
	.value	0x345
	.byte	0x62
	.long	0x1246
	.uleb128 0x25
	.string	"fd"
	.byte	0x17
	.value	0x345
	.byte	0x6e
	.long	0x57
	.uleb128 0x26
	.long	.LASF311
	.byte	0x17
	.value	0x345
	.byte	0x78
	.long	0x9af
	.byte	0
	.uleb128 0x4
	.byte	0x8
	.byte	0x4
	.long	.LASF312
	.uleb128 0x24
	.byte	0x20
	.byte	0x17
	.value	0x61d
	.byte	0x62
	.long	0x1271
	.uleb128 0x25
	.string	"fd"
	.byte	0x17
	.value	0x61d
	.byte	0x6e
	.long	0x57
	.uleb128 0x26
	.long	.LASF311
	.byte	0x17
	.value	0x61d
	.byte	0x78
	.long	0x9af
	.byte	0
	.uleb128 0x27
	.byte	0x20
	.byte	0x17
	.value	0x620
	.byte	0x3
	.long	0x12b4
	.uleb128 0x15
	.long	.LASF313
	.byte	0x17
	.value	0x620
	.byte	0x20
	.long	0x12b4
	.byte	0
	.uleb128 0x15
	.long	.LASF314
	.byte	0x17
	.value	0x620
	.byte	0x3e
	.long	0x12b4
	.byte	0x8
	.uleb128 0x15
	.long	.LASF315
	.byte	0x17
	.value	0x620
	.byte	0x5d
	.long	0x12b4
	.byte	0x10
	.uleb128 0x15
	.long	.LASF316
	.byte	0x17
	.value	0x620
	.byte	0x6d
	.long	0x57
	.byte	0x18
	.byte	0
	.uleb128 0x3
	.byte	0x8
	.long	0x10a3
	.uleb128 0x24
	.byte	0x10
	.byte	0x17
	.value	0x6f0
	.byte	0x3
	.long	0x12df
	.uleb128 0x26
	.long	.LASF317
	.byte	0x17
	.value	0x6f1
	.byte	0xb
	.long	0xbe4
	.uleb128 0x26
	.long	.LASF318
	.byte	0x17
	.value	0x6f2
	.byte	0x12
	.long	0xb5
	.byte	0
	.uleb128 0x28
	.byte	0x10
	.byte	0x17
	.value	0x6f6
	.value	0x1c8
	.long	0x1309
	.uleb128 0x29
	.string	"min"
	.byte	0x17
	.value	0x6f6
	.value	0x1d7
	.long	0xbc
	.byte	0
	.uleb128 0x2a
	.long	.LASF319
	.byte	0x17
	.value	0x6f6
	.value	0x1e9
	.long	0xb5
	.byte	0x8
	.byte	0
	.uleb128 0x3
	.byte	0x8
	.long	0x130f
	.uleb128 0x3
	.byte	0x8
	.long	0xc7b
	.uleb128 0x7
	.long	.LASF320
	.byte	0x19
	.byte	0x15
	.byte	0xf
	.long	0xbe4
	.uleb128 0x1f
	.byte	0x7
	.byte	0x4
	.long	0xb5
	.byte	0x1a
	.byte	0x40
	.byte	0x6
	.long	0x1479
	.uleb128 0x21
	.long	.LASF321
	.byte	0x1
	.uleb128 0x21
	.long	.LASF322
	.byte	0x2
	.uleb128 0x21
	.long	.LASF323
	.byte	0x4
	.uleb128 0x21
	.long	.LASF324
	.byte	0x8
	.uleb128 0x21
	.long	.LASF325
	.byte	0x10
	.uleb128 0x21
	.long	.LASF326
	.byte	0x20
	.uleb128 0x21
	.long	.LASF327
	.byte	0x40
	.uleb128 0x21
	.long	.LASF328
	.byte	0x80
	.uleb128 0x2b
	.long	.LASF329
	.value	0x100
	.uleb128 0x2b
	.long	.LASF330
	.value	0x200
	.uleb128 0x2b
	.long	.LASF331
	.value	0x400
	.uleb128 0x2b
	.long	.LASF332
	.value	0x800
	.uleb128 0x2b
	.long	.LASF333
	.value	0x1000
	.uleb128 0x2b
	.long	.LASF334
	.value	0x2000
	.uleb128 0x2b
	.long	.LASF335
	.value	0x4000
	.uleb128 0x2b
	.long	.LASF336
	.value	0x8000
	.uleb128 0x2c
	.long	.LASF337
	.long	0x10000
	.uleb128 0x2c
	.long	.LASF338
	.long	0x20000
	.uleb128 0x2c
	.long	.LASF339
	.long	0x40000
	.uleb128 0x2c
	.long	.LASF340
	.long	0x80000
	.uleb128 0x2c
	.long	.LASF341
	.long	0x100000
	.uleb128 0x2c
	.long	.LASF342
	.long	0x200000
	.uleb128 0x2c
	.long	.LASF343
	.long	0x400000
	.uleb128 0x2c
	.long	.LASF344
	.long	0x1000000
	.uleb128 0x2c
	.long	.LASF345
	.long	0x2000000
	.uleb128 0x2c
	.long	.LASF346
	.long	0x4000000
	.uleb128 0x2c
	.long	.LASF347
	.long	0x8000000
	.uleb128 0x2c
	.long	.LASF348
	.long	0x10000000
	.uleb128 0x2c
	.long	.LASF349
	.long	0x20000000
	.uleb128 0x2c
	.long	.LASF350
	.long	0x1000000
	.uleb128 0x2c
	.long	.LASF351
	.long	0x2000000
	.uleb128 0x2c
	.long	.LASF352
	.long	0x4000000
	.uleb128 0x2c
	.long	.LASF353
	.long	0x1000000
	.uleb128 0x2c
	.long	.LASF354
	.long	0x2000000
	.uleb128 0x2c
	.long	.LASF355
	.long	0x1000000
	.uleb128 0x2c
	.long	.LASF356
	.long	0x2000000
	.uleb128 0x2c
	.long	.LASF357
	.long	0x4000000
	.uleb128 0x2c
	.long	.LASF358
	.long	0x8000000
	.uleb128 0x2c
	.long	.LASF359
	.long	0x1000000
	.uleb128 0x2c
	.long	.LASF360
	.long	0x2000000
	.uleb128 0x2c
	.long	.LASF361
	.long	0x1000000
	.byte	0
	.uleb128 0x1f
	.byte	0x7
	.byte	0x4
	.long	0xb5
	.byte	0x4
	.byte	0x88
	.byte	0x6
	.long	0x148e
	.uleb128 0x21
	.long	.LASF362
	.byte	0x1
	.byte	0
	.uleb128 0x1f
	.byte	0x7
	.byte	0x4
	.long	0xb5
	.byte	0x4
	.byte	0x92
	.byte	0xe
	.long	0x14a9
	.uleb128 0x21
	.long	.LASF363
	.byte	0
	.uleb128 0x21
	.long	.LASF364
	.byte	0x1
	.byte	0
	.uleb128 0xd
	.long	.LASF365
	.byte	0x18
	.byte	0x3
	.byte	0x1b
	.byte	0x8
	.long	0x14de
	.uleb128 0xe
	.long	.LASF366
	.byte	0x3
	.byte	0x1c
	.byte	0x15
	.long	0x14de
	.byte	0
	.uleb128 0xe
	.long	.LASF367
	.byte	0x3
	.byte	0x1d
	.byte	0x15
	.long	0x14de
	.byte	0x8
	.uleb128 0xe
	.long	.LASF368
	.byte	0x3
	.byte	0x1e
	.byte	0x15
	.long	0x14de
	.byte	0x10
	.byte	0
	.uleb128 0x3
	.byte	0x8
	.long	0x14a9
	.uleb128 0xd
	.long	.LASF369
	.byte	0x10
	.byte	0x3
	.byte	0x28
	.byte	0x8
	.long	0x150c
	.uleb128 0x1e
	.string	"min"
	.byte	0x3
	.byte	0x29
	.byte	0x15
	.long	0x14de
	.byte	0
	.uleb128 0xe
	.long	.LASF319
	.byte	0x3
	.byte	0x2a
	.byte	0x10
	.long	0xb5
	.byte	0x8
	.byte	0
	.uleb128 0x17
	.long	.LASF370
	.byte	0x1b
	.value	0x21f
	.byte	0xf
	.long	0x90e
	.uleb128 0x17
	.long	.LASF371
	.byte	0x1b
	.value	0x221
	.byte	0xf
	.long	0x90e
	.uleb128 0x2
	.long	.LASF372
	.byte	0x1c
	.byte	0x24
	.byte	0xe
	.long	0x39
	.uleb128 0x2
	.long	.LASF373
	.byte	0x1c
	.byte	0x32
	.byte	0xc
	.long	0x57
	.uleb128 0x2
	.long	.LASF374
	.byte	0x1c
	.byte	0x37
	.byte	0xc
	.long	0x57
	.uleb128 0x2
	.long	.LASF375
	.byte	0x1c
	.byte	0x3b
	.byte	0xc
	.long	0x57
	.uleb128 0x2d
	.long	.LASF377
	.byte	0x1
	.byte	0xbb
	.byte	0x5
	.long	0x57
	.quad	.LFB103
	.quad	.LFE103-.LFB103
	.uleb128 0x1
	.byte	0x9c
	.long	0x15a2
	.uleb128 0x2e
	.long	.LASF290
	.byte	0x1
	.byte	0xbb
	.byte	0x23
	.long	0x121c
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x2e
	.long	.LASF376
	.byte	0x1
	.byte	0xbb
	.byte	0x38
	.long	0x117b
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x2f
	.string	"ap"
	.byte	0x1
	.byte	0xbb
	.byte	0x48
	.long	0x15a2
	.uleb128 0x1
	.byte	0x51
	.byte	0
	.uleb128 0x3
	.byte	0x8
	.long	0x78
	.uleb128 0x30
	.long	.LASF415
	.byte	0x1
	.byte	0x94
	.byte	0x6
	.quad	.LFB102
	.quad	.LFE102-.LFB102
	.uleb128 0x1
	.byte	0x9c
	.long	0x1738
	.uleb128 0x31
	.long	.LASF290
	.byte	0x1
	.byte	0x94
	.byte	0x20
	.long	0x121c
	.long	.LLST13
	.long	.LVUS13
	.uleb128 0x32
	.long	.LASF416
	.long	0x1748
	.uleb128 0x9
	.byte	0x3
	.quad	__PRETTY_FUNCTION__.10050
	.uleb128 0x33
	.quad	.LVL45
	.long	0x1abc
	.long	0x1605
	.uleb128 0x34
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x2
	.byte	0x73
	.sleb128 0
	.byte	0
	.uleb128 0x33
	.quad	.LVL46
	.long	0x1ac8
	.long	0x161d
	.uleb128 0x34
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x2
	.byte	0x73
	.sleb128 0
	.byte	0
	.uleb128 0x33
	.quad	.LVL47
	.long	0x1ad4
	.long	0x1635
	.uleb128 0x34
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x2
	.byte	0x73
	.sleb128 0
	.byte	0
	.uleb128 0x35
	.quad	.LVL48
	.long	0x1ae0
	.uleb128 0x35
	.quad	.LVL49
	.long	0x1ae0
	.uleb128 0x33
	.quad	.LVL50
	.long	0x1aec
	.long	0x1667
	.uleb128 0x34
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x2
	.byte	0x7c
	.sleb128 0
	.byte	0
	.uleb128 0x33
	.quad	.LVL51
	.long	0x1af9
	.long	0x167f
	.uleb128 0x34
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x2
	.byte	0x7c
	.sleb128 0
	.byte	0
	.uleb128 0x33
	.quad	.LVL52
	.long	0x1b06
	.long	0x1697
	.uleb128 0x34
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x2
	.byte	0x7c
	.sleb128 0
	.byte	0
	.uleb128 0x33
	.quad	.LVL53
	.long	0x1b13
	.long	0x16b0
	.uleb128 0x34
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x3
	.byte	0x73
	.sleb128 304
	.byte	0
	.uleb128 0x35
	.quad	.LVL54
	.long	0x1b20
	.uleb128 0x33
	.quad	.LVL58
	.long	0x1b2d
	.long	0x16fc
	.uleb128 0x34
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x9
	.byte	0x3
	.quad	.LC1
	.uleb128 0x34
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x9
	.byte	0x3
	.quad	.LC0
	.uleb128 0x34
	.uleb128 0x1
	.byte	0x51
	.uleb128 0x2
	.byte	0x8
	.byte	0xa4
	.uleb128 0x34
	.uleb128 0x1
	.byte	0x52
	.uleb128 0x9
	.byte	0x3
	.quad	__PRETTY_FUNCTION__.10050
	.byte	0
	.uleb128 0x36
	.quad	.LVL59
	.long	0x1b2d
	.uleb128 0x34
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x9
	.byte	0x3
	.quad	.LC2
	.uleb128 0x34
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x9
	.byte	0x3
	.quad	.LC0
	.uleb128 0x34
	.uleb128 0x1
	.byte	0x51
	.uleb128 0x2
	.byte	0x8
	.byte	0xa5
	.uleb128 0x34
	.uleb128 0x1
	.byte	0x52
	.uleb128 0x9
	.byte	0x3
	.quad	__PRETTY_FUNCTION__.10050
	.byte	0
	.byte	0
	.uleb128 0xb
	.long	0x46
	.long	0x1748
	.uleb128 0xc
	.long	0x71
	.byte	0xe
	.byte	0
	.uleb128 0x5
	.long	0x1738
	.uleb128 0x2d
	.long	.LASF378
	.byte	0x1
	.byte	0x73
	.byte	0x5
	.long	0x57
	.quad	.LFB101
	.quad	.LFE101-.LFB101
	.uleb128 0x1
	.byte	0x9c
	.long	0x1800
	.uleb128 0x31
	.long	.LASF290
	.byte	0x1
	.byte	0x73
	.byte	0x1d
	.long	0x121c
	.long	.LLST9
	.long	.LVUS9
	.uleb128 0x37
	.string	"err"
	.byte	0x1
	.byte	0x74
	.byte	0x7
	.long	0x57
	.long	.LLST10
	.long	.LVUS10
	.uleb128 0x37
	.string	"i"
	.byte	0x1
	.byte	0x75
	.byte	0x10
	.long	0xb5
	.long	.LLST11
	.long	.LVUS11
	.uleb128 0x37
	.string	"w"
	.byte	0x1
	.byte	0x76
	.byte	0xd
	.long	0x130f
	.long	.LLST12
	.long	.LVUS12
	.uleb128 0x33
	.quad	.LVL35
	.long	0x1b39
	.long	0x17d3
	.uleb128 0x34
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x2
	.byte	0x73
	.sleb128 0
	.byte	0
	.uleb128 0x33
	.quad	.LVL38
	.long	0x1b45
	.long	0x17eb
	.uleb128 0x34
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x2
	.byte	0x73
	.sleb128 0
	.byte	0
	.uleb128 0x36
	.quad	.LVL39
	.long	0x1b51
	.uleb128 0x34
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x2
	.byte	0x73
	.sleb128 0
	.byte	0
	.byte	0
	.uleb128 0x2d
	.long	.LASF379
	.byte	0x1
	.byte	0x1e
	.byte	0x5
	.long	0x57
	.quad	.LFB100
	.quad	.LFE100-.LFB100
	.uleb128 0x1
	.byte	0x9c
	.long	0x1a4a
	.uleb128 0x31
	.long	.LASF290
	.byte	0x1
	.byte	0x1e
	.byte	0x1d
	.long	0x121c
	.long	.LLST0
	.long	.LVUS0
	.uleb128 0x38
	.long	.LASF380
	.byte	0x1
	.byte	0x1f
	.byte	0x9
	.long	0xbc
	.long	.LLST1
	.long	.LVUS1
	.uleb128 0x37
	.string	"err"
	.byte	0x1
	.byte	0x20
	.byte	0x7
	.long	0x57
	.long	.LLST2
	.long	.LVUS2
	.uleb128 0x39
	.long	.LASF381
	.byte	0x1
	.byte	0x6a
	.byte	0x1
	.quad	.L3
	.uleb128 0x39
	.long	.LASF382
	.byte	0x1
	.byte	0x67
	.byte	0x1
	.quad	.L5
	.uleb128 0x39
	.long	.LASF383
	.byte	0x1
	.byte	0x64
	.byte	0x1
	.quad	.L6
	.uleb128 0x39
	.long	.LASF384
	.byte	0x1
	.byte	0x61
	.byte	0x1
	.quad	.LDL1
	.uleb128 0x3a
	.long	0x1a86
	.quad	.LBI10
	.byte	.LVU11
	.long	.Ldebug_ranges0+0
	.byte	0x1
	.byte	0x24
	.byte	0x3
	.long	0x18df
	.uleb128 0x3b
	.long	0x1aaf
	.long	.LLST3
	.long	.LVUS3
	.uleb128 0x3b
	.long	0x1aa3
	.long	.LLST4
	.long	.LVUS4
	.uleb128 0x3b
	.long	0x1a97
	.long	.LLST5
	.long	.LVUS5
	.byte	0
	.uleb128 0x3a
	.long	0x1a4a
	.quad	.LBI14
	.byte	.LVU20
	.long	.Ldebug_ranges0+0x30
	.byte	0x1
	.byte	0x27
	.byte	0x3
	.long	0x1913
	.uleb128 0x3b
	.long	0x1a57
	.long	.LLST6
	.long	.LVUS6
	.uleb128 0x3b
	.long	0x1a57
	.long	.LLST7
	.long	.LVUS7
	.byte	0
	.uleb128 0x3a
	.long	0x1a6a
	.quad	.LBI18
	.byte	.LVU96
	.long	.Ldebug_ranges0+0x70
	.byte	0x1
	.byte	0x38
	.byte	0x3
	.long	0x194d
	.uleb128 0x3b
	.long	0x1a78
	.long	.LLST8
	.long	.LVUS8
	.uleb128 0x36
	.quad	.LVL6
	.long	0x1b5d
	.uleb128 0x34
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x1
	.byte	0x31
	.byte	0
	.byte	0
	.uleb128 0x33
	.quad	.LVL7
	.long	0x1b69
	.long	0x1965
	.uleb128 0x34
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x2
	.byte	0x73
	.sleb128 0
	.byte	0
	.uleb128 0x35
	.quad	.LVL13
	.long	0x1b75
	.uleb128 0x33
	.quad	.LVL14
	.long	0x1b81
	.long	0x1991
	.uleb128 0x34
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x2
	.byte	0x73
	.sleb128 0
	.uleb128 0x34
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x3
	.byte	0x73
	.sleb128 616
	.byte	0
	.uleb128 0x33
	.quad	.LVL17
	.long	0x1b8e
	.long	0x19a9
	.uleb128 0x34
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x2
	.byte	0x7d
	.sleb128 0
	.byte	0
	.uleb128 0x33
	.quad	.LVL20
	.long	0x1abc
	.long	0x19c1
	.uleb128 0x34
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x2
	.byte	0x73
	.sleb128 0
	.byte	0
	.uleb128 0x33
	.quad	.LVL21
	.long	0x1ac8
	.long	0x19d9
	.uleb128 0x34
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x2
	.byte	0x73
	.sleb128 0
	.byte	0
	.uleb128 0x35
	.quad	.LVL22
	.long	0x1b20
	.uleb128 0x33
	.quad	.LVL26
	.long	0x1b9b
	.long	0x19fe
	.uleb128 0x34
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x2
	.byte	0x7e
	.sleb128 0
	.byte	0
	.uleb128 0x33
	.quad	.LVL28
	.long	0x1ba8
	.long	0x1a1d
	.uleb128 0x34
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x2
	.byte	0x73
	.sleb128 0
	.uleb128 0x34
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x3
	.byte	0x73
	.sleb128 176
	.byte	0
	.uleb128 0x33
	.quad	.LVL32
	.long	0x1b06
	.long	0x1a35
	.uleb128 0x34
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x2
	.byte	0x7e
	.sleb128 0
	.byte	0
	.uleb128 0x36
	.quad	.LVL33
	.long	0x1b13
	.uleb128 0x34
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x2
	.byte	0x7d
	.sleb128 0
	.byte	0
	.byte	0
	.uleb128 0x3c
	.long	.LASF385
	.byte	0x3
	.byte	0x3e
	.byte	0x25
	.byte	0x1
	.long	0x1a64
	.uleb128 0x3d
	.long	.LASF369
	.byte	0x3
	.byte	0x3e
	.byte	0x3c
	.long	0x1a64
	.byte	0
	.uleb128 0x3
	.byte	0x8
	.long	0x14e4
	.uleb128 0x3e
	.long	.LASF386
	.byte	0x4
	.value	0x12c
	.byte	0x25
	.byte	0x1
	.long	0x1a86
	.uleb128 0x3f
	.long	.LASF290
	.byte	0x4
	.value	0x12c
	.byte	0x40
	.long	0x121c
	.byte	0
	.uleb128 0x40
	.long	.LASF417
	.byte	0x2
	.byte	0x3b
	.byte	0x2a
	.long	0xbc
	.byte	0x3
	.long	0x1abc
	.uleb128 0x3d
	.long	.LASF387
	.byte	0x2
	.byte	0x3b
	.byte	0x38
	.long	0xbc
	.uleb128 0x3d
	.long	.LASF388
	.byte	0x2
	.byte	0x3b
	.byte	0x44
	.long	0x57
	.uleb128 0x3d
	.long	.LASF389
	.byte	0x2
	.byte	0x3b
	.byte	0x51
	.long	0x65
	.byte	0
	.uleb128 0x41
	.long	.LASF390
	.long	.LASF390
	.byte	0x4
	.byte	0xf3
	.byte	0x6
	.uleb128 0x41
	.long	.LASF391
	.long	.LASF391
	.byte	0x4
	.byte	0xfa
	.byte	0x6
	.uleb128 0x41
	.long	.LASF392
	.long	.LASF392
	.byte	0x4
	.byte	0xd2
	.byte	0x6
	.uleb128 0x41
	.long	.LASF393
	.long	.LASF393
	.byte	0x4
	.byte	0xbe
	.byte	0x5
	.uleb128 0x42
	.long	.LASF394
	.long	.LASF394
	.byte	0x17
	.value	0x69b
	.byte	0x2d
	.uleb128 0x42
	.long	.LASF395
	.long	.LASF395
	.byte	0x17
	.value	0x69d
	.byte	0x2d
	.uleb128 0x42
	.long	.LASF396
	.long	.LASF396
	.byte	0x17
	.value	0x69a
	.byte	0x2d
	.uleb128 0x42
	.long	.LASF397
	.long	.LASF397
	.byte	0x17
	.value	0x6a0
	.byte	0x2d
	.uleb128 0x42
	.long	.LASF398
	.long	.LASF398
	.byte	0x1a
	.value	0x14d
	.byte	0x6
	.uleb128 0x41
	.long	.LASF399
	.long	.LASF399
	.byte	0x1d
	.byte	0x45
	.byte	0xd
	.uleb128 0x41
	.long	.LASF400
	.long	.LASF400
	.byte	0x4
	.byte	0xce
	.byte	0x5
	.uleb128 0x41
	.long	.LASF401
	.long	.LASF401
	.byte	0x4
	.byte	0xd3
	.byte	0x5
	.uleb128 0x41
	.long	.LASF402
	.long	.LASF402
	.byte	0x4
	.byte	0xf4
	.byte	0x5
	.uleb128 0x41
	.long	.LASF403
	.long	.LASF403
	.byte	0x4
	.byte	0xf7
	.byte	0xa
	.uleb128 0x41
	.long	.LASF404
	.long	.LASF404
	.byte	0x4
	.byte	0xf9
	.byte	0x5
	.uleb128 0x41
	.long	.LASF405
	.long	.LASF405
	.byte	0x4
	.byte	0xf2
	.byte	0x6
	.uleb128 0x42
	.long	.LASF406
	.long	.LASF406
	.byte	0x17
	.value	0x623
	.byte	0x2c
	.uleb128 0x42
	.long	.LASF407
	.long	.LASF407
	.byte	0x17
	.value	0x69f
	.byte	0x2c
	.uleb128 0x42
	.long	.LASF408
	.long	.LASF408
	.byte	0x17
	.value	0x698
	.byte	0x2c
	.uleb128 0x42
	.long	.LASF409
	.long	.LASF409
	.byte	0x17
	.value	0x349
	.byte	0x2c
	.byte	0
	.section	.debug_abbrev,"",@progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1b
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x7
	.uleb128 0x10
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x13
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x13
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x13
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3c
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0x21
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0x17
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0x37
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x16
	.uleb128 0x15
	.byte	0
	.uleb128 0x27
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0x17
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0x18
	.uleb128 0x13
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0xb
	.uleb128 0x5
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x19
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x1a
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0x5
	.byte	0
	.byte	0
	.uleb128 0x1b
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0x5
	.byte	0
	.byte	0
	.uleb128 0x1c
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1d
	.uleb128 0x5
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1e
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x1f
	.uleb128 0x4
	.byte	0x1
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x20
	.uleb128 0x28
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1c
	.uleb128 0xd
	.byte	0
	.byte	0
	.uleb128 0x21
	.uleb128 0x28
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1c
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x22
	.uleb128 0x13
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x23
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x24
	.uleb128 0x17
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x25
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x26
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x27
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x28
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0x5
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x29
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x2a
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x2b
	.uleb128 0x28
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1c
	.uleb128 0x5
	.byte	0
	.byte	0
	.uleb128 0x2c
	.uleb128 0x28
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1c
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2d
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x7
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2117
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x2e
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x2f
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x30
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x7
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2117
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x31
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x17
	.uleb128 0x2137
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x32
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x34
	.uleb128 0x19
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x33
	.uleb128 0x4109
	.byte	0x1
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x34
	.uleb128 0x410a
	.byte	0
	.uleb128 0x2
	.uleb128 0x18
	.uleb128 0x2111
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x35
	.uleb128 0x4109
	.byte	0
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x31
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x36
	.uleb128 0x4109
	.byte	0x1
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x31
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x37
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x17
	.uleb128 0x2137
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x38
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x17
	.uleb128 0x2137
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x39
	.uleb128 0xa
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.byte	0
	.byte	0
	.uleb128 0x3a
	.uleb128 0x1d
	.byte	0x1
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x2138
	.uleb128 0xb
	.uleb128 0x55
	.uleb128 0x17
	.uleb128 0x58
	.uleb128 0xb
	.uleb128 0x59
	.uleb128 0xb
	.uleb128 0x57
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x3b
	.uleb128 0x5
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x17
	.uleb128 0x2137
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x3c
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x20
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x3d
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x3e
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x20
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x3f
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x40
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x20
	.uleb128 0xb
	.uleb128 0x34
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x41
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.uleb128 0x6e
	.uleb128 0xe
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x42
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.uleb128 0x6e
	.uleb128 0xe
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",@progbits
.Ldebug_loc0:
.LVUS13:
	.uleb128 0
	.uleb128 .LVU265
	.uleb128 .LVU265
	.uleb128 .LVU294
	.uleb128 .LVU294
	.uleb128 .LVU295
	.uleb128 .LVU295
	.uleb128 .LVU296
	.uleb128 .LVU296
	.uleb128 0
.LLST13:
	.quad	.LVL44-.Ltext0
	.quad	.LVL45-1-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	.LVL45-1-.Ltext0
	.quad	.LVL55-.Ltext0
	.value	0x1
	.byte	0x53
	.quad	.LVL55-.Ltext0
	.quad	.LVL56-.Ltext0
	.value	0x4
	.byte	0x7c
	.sleb128 -136
	.byte	0x9f
	.quad	.LVL56-.Ltext0
	.quad	.LVL57-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.byte	0x9f
	.quad	.LVL57-.Ltext0
	.quad	.LFE102-.Ltext0
	.value	0x1
	.byte	0x53
	.quad	0
	.quad	0
.LVUS9:
	.uleb128 0
	.uleb128 .LVU218
	.uleb128 .LVU218
	.uleb128 .LVU221
	.uleb128 .LVU221
	.uleb128 .LVU222
	.uleb128 .LVU222
	.uleb128 0
.LLST9:
	.quad	.LVL34-.Ltext0
	.quad	.LVL35-1-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	.LVL35-1-.Ltext0
	.quad	.LVL36-.Ltext0
	.value	0x1
	.byte	0x53
	.quad	.LVL36-.Ltext0
	.quad	.LVL37-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.byte	0x9f
	.quad	.LVL37-.Ltext0
	.quad	.LFE101-.Ltext0
	.value	0x1
	.byte	0x53
	.quad	0
	.quad	0
.LVUS10:
	.uleb128 .LVU218
	.uleb128 .LVU224
	.uleb128 .LVU224
	.uleb128 .LVU228
	.uleb128 .LVU228
	.uleb128 0
.LLST10:
	.quad	.LVL35-.Ltext0
	.quad	.LVL38-1-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	.LVL38-.Ltext0
	.quad	.LVL39-1-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	.LVL39-.Ltext0
	.quad	.LFE101-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	0
	.quad	0
.LVUS11:
	.uleb128 .LVU230
	.uleb128 .LVU235
.LLST11:
	.quad	.LVL40-.Ltext0
	.quad	.LVL41-.Ltext0
	.value	0x2
	.byte	0x30
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS12:
	.uleb128 .LVU235
	.uleb128 .LVU239
	.uleb128 .LVU241
	.uleb128 0
.LLST12:
	.quad	.LVL41-.Ltext0
	.quad	.LVL42-.Ltext0
	.value	0x1
	.byte	0x52
	.quad	.LVL43-.Ltext0
	.quad	.LFE101-.Ltext0
	.value	0x1
	.byte	0x52
	.quad	0
	.quad	0
.LVUS0:
	.uleb128 0
	.uleb128 .LVU14
	.uleb128 .LVU14
	.uleb128 .LVU124
	.uleb128 .LVU124
	.uleb128 .LVU126
	.uleb128 .LVU126
	.uleb128 .LVU172
	.uleb128 .LVU172
	.uleb128 .LVU174
	.uleb128 .LVU174
	.uleb128 0
.LLST0:
	.quad	.LVL0-.Ltext0
	.quad	.LVL2-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	.LVL2-.Ltext0
	.quad	.LVL10-.Ltext0
	.value	0x1
	.byte	0x53
	.quad	.LVL10-.Ltext0
	.quad	.LVL12-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.byte	0x9f
	.quad	.LVL12-.Ltext0
	.quad	.LVL23-.Ltext0
	.value	0x1
	.byte	0x53
	.quad	.LVL23-.Ltext0
	.quad	.LVL25-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.byte	0x9f
	.quad	.LVL25-.Ltext0
	.quad	.LFE100-.Ltext0
	.value	0x1
	.byte	0x53
	.quad	0
	.quad	0
.LVUS1:
	.uleb128 .LVU10
	.uleb128 .LVU99
.LLST1:
	.quad	.LVL1-.Ltext0
	.quad	.LVL6-1-.Ltext0
	.value	0x1
	.byte	0x51
	.quad	0
	.quad	0
.LVUS2:
	.uleb128 .LVU121
	.uleb128 .LVU123
	.uleb128 .LVU123
	.uleb128 .LVU125
	.uleb128 .LVU125
	.uleb128 .LVU127
	.uleb128 .LVU127
	.uleb128 .LVU129
	.uleb128 .LVU129
	.uleb128 .LVU134
	.uleb128 .LVU134
	.uleb128 .LVU162
	.uleb128 .LVU162
	.uleb128 .LVU164
	.uleb128 .LVU164
	.uleb128 .LVU173
	.uleb128 .LVU173
	.uleb128 .LVU176
	.uleb128 .LVU176
	.uleb128 .LVU177
	.uleb128 .LVU177
	.uleb128 .LVU181
	.uleb128 .LVU181
	.uleb128 .LVU182
	.uleb128 .LVU182
	.uleb128 .LVU187
	.uleb128 .LVU187
	.uleb128 .LVU205
	.uleb128 .LVU205
	.uleb128 .LVU207
	.uleb128 .LVU207
	.uleb128 0
.LLST2:
	.quad	.LVL8-.Ltext0
	.quad	.LVL9-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	.LVL9-.Ltext0
	.quad	.LVL11-.Ltext0
	.value	0x1
	.byte	0x5c
	.quad	.LVL11-.Ltext0
	.quad	.LVL13-1-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	.LVL13-1-.Ltext0
	.quad	.LVL15-.Ltext0
	.value	0x1
	.byte	0x5c
	.quad	.LVL15-.Ltext0
	.quad	.LVL16-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	.LVL16-.Ltext0
	.quad	.LVL18-.Ltext0
	.value	0x1
	.byte	0x5c
	.quad	.LVL18-.Ltext0
	.quad	.LVL19-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	.LVL19-.Ltext0
	.quad	.LVL24-.Ltext0
	.value	0x1
	.byte	0x5c
	.quad	.LVL24-.Ltext0
	.quad	.LVL26-1-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	.LVL26-1-.Ltext0
	.quad	.LVL27-.Ltext0
	.value	0x1
	.byte	0x5c
	.quad	.LVL27-.Ltext0
	.quad	.LVL28-1-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	.LVL28-1-.Ltext0
	.quad	.LVL29-.Ltext0
	.value	0x1
	.byte	0x5c
	.quad	.LVL29-.Ltext0
	.quad	.LVL30-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	.LVL30-.Ltext0
	.quad	.LVL31-.Ltext0
	.value	0x1
	.byte	0x5c
	.quad	.LVL31-.Ltext0
	.quad	.LVL32-1-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	.LVL32-1-.Ltext0
	.quad	.LFE100-.Ltext0
	.value	0x1
	.byte	0x5c
	.quad	0
	.quad	0
.LVUS3:
	.uleb128 .LVU11
	.uleb128 .LVU15
.LLST3:
	.quad	.LVL1-.Ltext0
	.quad	.LVL3-.Ltext0
	.value	0x4
	.byte	0xa
	.value	0x350
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS4:
	.uleb128 .LVU11
	.uleb128 .LVU15
.LLST4:
	.quad	.LVL1-.Ltext0
	.quad	.LVL3-.Ltext0
	.value	0x2
	.byte	0x30
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS5:
	.uleb128 .LVU11
	.uleb128 .LVU14
	.uleb128 .LVU14
	.uleb128 .LVU15
.LLST5:
	.quad	.LVL1-.Ltext0
	.quad	.LVL2-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	.LVL2-.Ltext0
	.quad	.LVL3-.Ltext0
	.value	0x1
	.byte	0x53
	.quad	0
	.quad	0
.LVUS6:
	.uleb128 .LVU21
	.uleb128 .LVU124
	.uleb128 .LVU124
	.uleb128 .LVU126
	.uleb128 .LVU126
	.uleb128 .LVU172
	.uleb128 .LVU172
	.uleb128 .LVU174
	.uleb128 .LVU174
	.uleb128 0
.LLST6:
	.quad	.LVL4-.Ltext0
	.quad	.LVL10-.Ltext0
	.value	0x4
	.byte	0x73
	.sleb128 520
	.byte	0x9f
	.quad	.LVL10-.Ltext0
	.quad	.LVL12-.Ltext0
	.value	0x7
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.byte	0x23
	.uleb128 0x208
	.byte	0x9f
	.quad	.LVL12-.Ltext0
	.quad	.LVL23-.Ltext0
	.value	0x4
	.byte	0x73
	.sleb128 520
	.byte	0x9f
	.quad	.LVL23-.Ltext0
	.quad	.LVL25-.Ltext0
	.value	0x7
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.byte	0x23
	.uleb128 0x208
	.byte	0x9f
	.quad	.LVL25-.Ltext0
	.quad	.LFE100-.Ltext0
	.value	0x4
	.byte	0x73
	.sleb128 520
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS7:
	.uleb128 .LVU21
	.uleb128 .LVU124
	.uleb128 .LVU124
	.uleb128 .LVU126
	.uleb128 .LVU126
	.uleb128 .LVU172
	.uleb128 .LVU172
	.uleb128 .LVU174
	.uleb128 .LVU174
	.uleb128 0
.LLST7:
	.quad	.LVL4-.Ltext0
	.quad	.LVL10-.Ltext0
	.value	0x4
	.byte	0x73
	.sleb128 520
	.byte	0x9f
	.quad	.LVL10-.Ltext0
	.quad	.LVL12-.Ltext0
	.value	0x7
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.byte	0x23
	.uleb128 0x208
	.byte	0x9f
	.quad	.LVL12-.Ltext0
	.quad	.LVL23-.Ltext0
	.value	0x4
	.byte	0x73
	.sleb128 520
	.byte	0x9f
	.quad	.LVL23-.Ltext0
	.quad	.LVL25-.Ltext0
	.value	0x7
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.byte	0x23
	.uleb128 0x208
	.byte	0x9f
	.quad	.LVL25-.Ltext0
	.quad	.LFE100-.Ltext0
	.value	0x4
	.byte	0x73
	.sleb128 520
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS8:
	.uleb128 .LVU97
	.uleb128 .LVU124
	.uleb128 .LVU124
	.uleb128 .LVU126
	.uleb128 .LVU126
	.uleb128 .LVU172
	.uleb128 .LVU172
	.uleb128 .LVU174
	.uleb128 .LVU174
	.uleb128 0
.LLST8:
	.quad	.LVL5-.Ltext0
	.quad	.LVL10-.Ltext0
	.value	0x1
	.byte	0x53
	.quad	.LVL10-.Ltext0
	.quad	.LVL12-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.byte	0x9f
	.quad	.LVL12-.Ltext0
	.quad	.LVL23-.Ltext0
	.value	0x1
	.byte	0x53
	.quad	.LVL23-.Ltext0
	.quad	.LVL25-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.byte	0x9f
	.quad	.LVL25-.Ltext0
	.quad	.LFE100-.Ltext0
	.value	0x1
	.byte	0x53
	.quad	0
	.quad	0
	.section	.debug_aranges,"",@progbits
	.long	0x2c
	.value	0x2
	.long	.Ldebug_info0
	.byte	0x8
	.byte	0
	.value	0
	.value	0
	.quad	.Ltext0
	.quad	.Letext0-.Ltext0
	.quad	0
	.quad	0
	.section	.debug_ranges,"",@progbits
.Ldebug_ranges0:
	.quad	.LBB10-.Ltext0
	.quad	.LBE10-.Ltext0
	.quad	.LBB13-.Ltext0
	.quad	.LBE13-.Ltext0
	.quad	0
	.quad	0
	.quad	.LBB14-.Ltext0
	.quad	.LBE14-.Ltext0
	.quad	.LBB24-.Ltext0
	.quad	.LBE24-.Ltext0
	.quad	.LBB25-.Ltext0
	.quad	.LBE25-.Ltext0
	.quad	0
	.quad	0
	.quad	.LBB18-.Ltext0
	.quad	.LBE18-.Ltext0
	.quad	.LBB26-.Ltext0
	.quad	.LBE26-.Ltext0
	.quad	.LBB27-.Ltext0
	.quad	.LBE27-.Ltext0
	.quad	.LBB28-.Ltext0
	.quad	.LBE28-.Ltext0
	.quad	.LBB29-.Ltext0
	.quad	.LBE29-.Ltext0
	.quad	0
	.quad	0
	.section	.debug_line,"",@progbits
.Ldebug_line0:
	.section	.debug_str,"MS",@progbits,1
.LASF406:
	.string	"uv_signal_init"
.LASF347:
	.string	"UV_HANDLE_TCP_ACCEPT_STATE_CHANGING"
.LASF225:
	.string	"UV_EMFILE"
.LASF168:
	.string	"async_wfd"
.LASF104:
	.string	"sockaddr_ax25"
.LASF210:
	.string	"UV_ECHARSET"
.LASF115:
	.string	"sin6_flowinfo"
.LASF397:
	.string	"uv_rwlock_destroy"
.LASF337:
	.string	"UV_HANDLE_READ_PENDING"
.LASF40:
	.string	"_shortbuf"
.LASF414:
	.string	"_IO_lock_t"
.LASF123:
	.string	"sockaddr_x25"
.LASF5:
	.string	"gp_offset"
.LASF1:
	.string	"program_invocation_short_name"
.LASF268:
	.string	"UV_ASYNC"
.LASF56:
	.string	"stderr"
.LASF176:
	.string	"inotify_read_watcher"
.LASF91:
	.string	"__flags"
.LASF401:
	.string	"uv__async_fork"
.LASF29:
	.string	"_IO_buf_end"
.LASF180:
	.string	"uv__io_s"
.LASF183:
	.string	"uv__io_t"
.LASF102:
	.string	"sa_data"
.LASF330:
	.string	"UV_HANDLE_SHUT"
.LASF250:
	.string	"UV_EROFS"
.LASF150:
	.string	"flags"
.LASF212:
	.string	"UV_ECONNREFUSED"
.LASF208:
	.string	"UV_EBUSY"
.LASF324:
	.string	"UV_HANDLE_REF"
.LASF247:
	.string	"UV_EPROTONOSUPPORT"
.LASF290:
	.string	"loop"
.LASF117:
	.string	"sin6_scope_id"
.LASF86:
	.string	"__cur_writer"
.LASF27:
	.string	"_IO_write_end"
.LASF9:
	.string	"unsigned int"
.LASF121:
	.string	"sockaddr_ns"
.LASF355:
	.string	"UV_HANDLE_TTY_READABLE"
.LASF377:
	.string	"uv__loop_configure"
.LASF273:
	.string	"UV_IDLE"
.LASF158:
	.string	"wq_async"
.LASF143:
	.string	"getdate_err"
.LASF256:
	.string	"UV_EXDEV"
.LASF21:
	.string	"_flags"
.LASF146:
	.string	"active_handles"
.LASF153:
	.string	"watcher_queue"
.LASF304:
	.string	"caught_signals"
.LASF151:
	.string	"backend_fd"
.LASF369:
	.string	"heap"
.LASF159:
	.string	"cloexec_lock"
.LASF175:
	.string	"emfile_fd"
.LASF307:
	.string	"uv_loop_option"
.LASF189:
	.string	"UV_EADDRNOTAVAIL"
.LASF289:
	.string	"uv_handle_s"
.LASF288:
	.string	"uv_handle_t"
.LASF33:
	.string	"_markers"
.LASF332:
	.string	"UV_HANDLE_READ_EOF"
.LASF59:
	.string	"_sys_nerr"
.LASF135:
	.string	"_sys_siglist"
.LASF204:
	.string	"UV_EAI_SERVICE"
.LASF257:
	.string	"UV_UNKNOWN"
.LASF207:
	.string	"UV_EBADF"
.LASF325:
	.string	"UV_HANDLE_INTERNAL"
.LASF264:
	.string	"UV_EFTYPE"
.LASF296:
	.string	"async_cb"
.LASF226:
	.string	"UV_EMSGSIZE"
.LASF154:
	.string	"watchers"
.LASF148:
	.string	"active_reqs"
.LASF185:
	.string	"uv_rwlock_t"
.LASF383:
	.string	"fail_mutex_init"
.LASF81:
	.string	"__writers"
.LASF359:
	.string	"UV_SIGNAL_ONE_SHOT_DISPATCHED"
.LASF315:
	.string	"rbe_parent"
.LASF357:
	.string	"UV_HANDLE_TTY_SAVED_POSITION"
.LASF217:
	.string	"UV_EFBIG"
.LASF156:
	.string	"nfds"
.LASF245:
	.string	"UV_EPIPE"
.LASF132:
	.string	"__in6_u"
.LASF344:
	.string	"UV_HANDLE_TCP_NODELAY"
.LASF119:
	.string	"sockaddr_ipx"
.LASF351:
	.string	"UV_HANDLE_UDP_CONNECTED"
.LASF239:
	.string	"UV_ENOTCONN"
.LASF194:
	.string	"UV_EAI_BADFLAGS"
.LASF224:
	.string	"UV_ELOOP"
.LASF228:
	.string	"UV_ENETDOWN"
.LASF66:
	.string	"__pthread_internal_list"
.LASF87:
	.string	"__shared"
.LASF362:
	.string	"UV_LOOP_BLOCK_SIGPROF"
.LASF63:
	.string	"uint32_t"
.LASF67:
	.string	"__prev"
.LASF249:
	.string	"UV_ERANGE"
.LASF124:
	.string	"in_addr_t"
.LASF92:
	.string	"long long unsigned int"
.LASF360:
	.string	"UV_SIGNAL_ONE_SHOT"
.LASF270:
	.string	"UV_FS_EVENT"
.LASF55:
	.string	"stdout"
.LASF32:
	.string	"_IO_save_end"
.LASF72:
	.string	"__count"
.LASF174:
	.string	"child_watcher"
.LASF374:
	.string	"opterr"
.LASF227:
	.string	"UV_ENAMETOOLONG"
.LASF38:
	.string	"_cur_column"
.LASF389:
	.string	"__len"
.LASF318:
	.string	"count"
.LASF300:
	.string	"uv_signal_s"
.LASF299:
	.string	"uv_signal_t"
.LASF171:
	.string	"time"
.LASF50:
	.string	"FILE"
.LASF169:
	.string	"timer_heap"
.LASF7:
	.string	"overflow_arg_area"
.LASF128:
	.string	"__u6_addr8"
.LASF411:
	.string	"../deps/uv/src/unix/loop.c"
.LASF195:
	.string	"UV_EAI_BADHINTS"
.LASF197:
	.string	"UV_EAI_FAIL"
.LASF382:
	.string	"fail_rwlock_init"
.LASF105:
	.string	"sockaddr_dl"
.LASF274:
	.string	"UV_NAMED_PIPE"
.LASF284:
	.string	"UV_FILE"
.LASF188:
	.string	"UV_EADDRINUSE"
.LASF405:
	.string	"uv__signal_global_once_init"
.LASF182:
	.string	"events"
.LASF108:
	.string	"sin_family"
.LASF16:
	.string	"__uint16_t"
.LASF58:
	.string	"sys_errlist"
.LASF73:
	.string	"__owner"
.LASF292:
	.string	"close_cb"
.LASF155:
	.string	"nwatchers"
.LASF271:
	.string	"UV_FS_POLL"
.LASF127:
	.string	"in_port_t"
.LASF77:
	.string	"__elision"
.LASF57:
	.string	"sys_nerr"
.LASF211:
	.string	"UV_ECONNABORTED"
.LASF201:
	.string	"UV_EAI_NONAME"
.LASF88:
	.string	"__rwelision"
.LASF326:
	.string	"UV_HANDLE_ENDGAME_QUEUED"
.LASF35:
	.string	"_fileno"
.LASF114:
	.string	"sin6_port"
.LASF111:
	.string	"sin_zero"
.LASF193:
	.string	"UV_EAI_AGAIN"
.LASF126:
	.string	"s_addr"
.LASF13:
	.string	"size_t"
.LASF99:
	.string	"sa_family_t"
.LASF11:
	.string	"short unsigned int"
.LASF248:
	.string	"UV_EPROTOTYPE"
.LASF266:
	.string	"UV_ERRNO_MAX"
.LASF216:
	.string	"UV_EFAULT"
.LASF251:
	.string	"UV_ESHUTDOWN"
.LASF24:
	.string	"_IO_read_base"
.LASF345:
	.string	"UV_HANDLE_TCP_KEEPALIVE"
.LASF118:
	.string	"sockaddr_inarp"
.LASF310:
	.string	"uv_signal_cb"
.LASF54:
	.string	"stdin"
.LASF343:
	.string	"UV_HANDLE_IPV6"
.LASF165:
	.string	"async_handles"
.LASF98:
	.string	"pthread_rwlock_t"
.LASF116:
	.string	"sin6_addr"
.LASF18:
	.string	"__uint64_t"
.LASF298:
	.string	"pending"
.LASF120:
	.string	"sockaddr_iso"
.LASF403:
	.string	"uv__hrtime"
.LASF302:
	.string	"signum"
.LASF395:
	.string	"uv_mutex_unlock"
.LASF258:
	.string	"UV_EOF"
.LASF265:
	.string	"UV_EILSEQ"
.LASF400:
	.string	"uv__io_fork"
.LASF396:
	.string	"uv_mutex_destroy"
.LASF235:
	.string	"UV_ENONET"
.LASF68:
	.string	"__next"
.LASF83:
	.string	"__writers_futex"
.LASF157:
	.string	"wq_mutex"
.LASF134:
	.string	"in6addr_loopback"
.LASF191:
	.string	"UV_EAGAIN"
.LASF241:
	.string	"UV_ENOTEMPTY"
.LASF412:
	.string	"/workspace/nodejsbuild/node-v12.18.4/out"
.LASF233:
	.string	"UV_ENOENT"
.LASF409:
	.string	"uv_async_init"
.LASF286:
	.string	"uv_handle_type"
.LASF2:
	.string	"char"
.LASF379:
	.string	"uv_loop_init"
.LASF275:
	.string	"UV_POLL"
.LASF255:
	.string	"UV_ETXTBSY"
.LASF138:
	.string	"__daylight"
.LASF232:
	.string	"UV_ENODEV"
.LASF381:
	.string	"fail_signal_init"
.LASF335:
	.string	"UV_HANDLE_READABLE"
.LASF140:
	.string	"tzname"
.LASF51:
	.string	"_IO_marker"
.LASF371:
	.string	"environ"
.LASF334:
	.string	"UV_HANDLE_BOUND"
.LASF309:
	.string	"uv_async_cb"
.LASF22:
	.string	"_IO_read_ptr"
.LASF393:
	.string	"uv__close"
.LASF145:
	.string	"data"
.LASF192:
	.string	"UV_EAI_ADDRFAMILY"
.LASF319:
	.string	"nelts"
.LASF404:
	.string	"uv__platform_loop_init"
.LASF350:
	.string	"UV_HANDLE_UDP_PROCESSING"
.LASF76:
	.string	"__spins"
.LASF316:
	.string	"rbe_color"
.LASF61:
	.string	"uint8_t"
.LASF365:
	.string	"heap_node"
.LASF103:
	.string	"sockaddr_at"
.LASF262:
	.string	"UV_EREMOTEIO"
.LASF209:
	.string	"UV_ECANCELED"
.LASF199:
	.string	"UV_EAI_MEMORY"
.LASF394:
	.string	"uv_mutex_lock"
.LASF79:
	.string	"__pthread_rwlock_arch_t"
.LASF323:
	.string	"UV_HANDLE_ACTIVE"
.LASF136:
	.string	"sys_siglist"
.LASF237:
	.string	"UV_ENOSPC"
.LASF45:
	.string	"_freeres_list"
.LASF353:
	.string	"UV_HANDLE_NON_OVERLAPPED_PIPE"
.LASF367:
	.string	"right"
.LASF240:
	.string	"UV_ENOTDIR"
.LASF25:
	.string	"_IO_write_base"
.LASF78:
	.string	"__list"
.LASF97:
	.string	"long long int"
.LASF342:
	.string	"UV_HANDLE_CANCELLATION_PENDING"
.LASF100:
	.string	"sockaddr"
.LASF321:
	.string	"UV_HANDLE_CLOSING"
.LASF30:
	.string	"_IO_save_base"
.LASF236:
	.string	"UV_ENOPROTOOPT"
.LASF109:
	.string	"sin_port"
.LASF259:
	.string	"UV_ENXIO"
.LASF391:
	.string	"uv__platform_loop_delete"
.LASF106:
	.string	"sockaddr_eon"
.LASF385:
	.string	"heap_init"
.LASF130:
	.string	"__u6_addr32"
.LASF373:
	.string	"optind"
.LASF122:
	.string	"sockaddr_un"
.LASF181:
	.string	"pevents"
.LASF417:
	.string	"memset"
.LASF202:
	.string	"UV_EAI_OVERFLOW"
.LASF279:
	.string	"UV_TCP"
.LASF231:
	.string	"UV_ENOBUFS"
.LASF331:
	.string	"UV_HANDLE_READ_PARTIAL"
.LASF386:
	.string	"uv__update_time"
.LASF46:
	.string	"_freeres_buf"
.LASF133:
	.string	"in6addr_any"
.LASF31:
	.string	"_IO_backup_base"
.LASF222:
	.string	"UV_EISCONN"
.LASF110:
	.string	"sin_addr"
.LASF75:
	.string	"__kind"
.LASF308:
	.string	"uv_close_cb"
.LASF89:
	.string	"__pad1"
.LASF90:
	.string	"__pad2"
.LASF84:
	.string	"__pad3"
.LASF85:
	.string	"__pad4"
.LASF47:
	.string	"__pad5"
.LASF221:
	.string	"UV_EIO"
.LASF4:
	.string	"long unsigned int"
.LASF322:
	.string	"UV_HANDLE_CLOSED"
.LASF242:
	.string	"UV_ENOTSOCK"
.LASF278:
	.string	"UV_STREAM"
.LASF147:
	.string	"handle_queue"
.LASF260:
	.string	"UV_EMLINK"
.LASF39:
	.string	"_vtable_offset"
.LASF253:
	.string	"UV_ESRCH"
.LASF0:
	.string	"program_invocation_name"
.LASF48:
	.string	"_mode"
.LASF372:
	.string	"optarg"
.LASF206:
	.string	"UV_EALREADY"
.LASF163:
	.string	"check_handles"
.LASF69:
	.string	"__pthread_list_t"
.LASF285:
	.string	"UV_HANDLE_TYPE_MAX"
.LASF272:
	.string	"UV_HANDLE"
.LASF62:
	.string	"uint16_t"
.LASF187:
	.string	"UV_EACCES"
.LASF314:
	.string	"rbe_right"
.LASF160:
	.string	"closing_handles"
.LASF200:
	.string	"UV_EAI_NODATA"
.LASF346:
	.string	"UV_HANDLE_TCP_SINGLE_ACCEPT"
.LASF142:
	.string	"timezone"
.LASF203:
	.string	"UV_EAI_PROTOCOL"
.LASF205:
	.string	"UV_EAI_SOCKTYPE"
.LASF215:
	.string	"UV_EEXIST"
.LASF338:
	.string	"UV_HANDLE_SYNC_BYPASS_IOCP"
.LASF230:
	.string	"UV_ENFILE"
.LASF388:
	.string	"__ch"
.LASF129:
	.string	"__u6_addr16"
.LASF415:
	.string	"uv__loop_close"
.LASF23:
	.string	"_IO_read_end"
.LASF368:
	.string	"parent"
.LASF113:
	.string	"sin6_family"
.LASF375:
	.string	"optopt"
.LASF281:
	.string	"UV_TTY"
.LASF15:
	.string	"short int"
.LASF179:
	.string	"uv__io_cb"
.LASF238:
	.string	"UV_ENOSYS"
.LASF267:
	.string	"UV_UNKNOWN_HANDLE"
.LASF214:
	.string	"UV_EDESTADDRREQ"
.LASF3:
	.string	"long int"
.LASF313:
	.string	"rbe_left"
.LASF43:
	.string	"_codecvt"
.LASF361:
	.string	"UV_HANDLE_POLL_SLOW"
.LASF229:
	.string	"UV_ENETUNREACH"
.LASF170:
	.string	"timer_counter"
.LASF218:
	.string	"UV_EHOSTUNREACH"
.LASF53:
	.string	"_IO_wide_data"
.LASF244:
	.string	"UV_EPERM"
.LASF376:
	.string	"option"
.LASF177:
	.string	"inotify_watchers"
.LASF64:
	.string	"uint64_t"
.LASF339:
	.string	"UV_HANDLE_ZERO_READ"
.LASF370:
	.string	"__environ"
.LASF413:
	.string	"__va_list_tag"
.LASF407:
	.string	"uv_rwlock_init"
.LASF223:
	.string	"UV_EISDIR"
.LASF184:
	.string	"uv_mutex_t"
.LASF42:
	.string	"_offset"
.LASF390:
	.string	"uv__signal_loop_cleanup"
.LASF293:
	.string	"next_closing"
.LASF336:
	.string	"UV_HANDLE_WRITABLE"
.LASF366:
	.string	"left"
.LASF107:
	.string	"sockaddr_in"
.LASF6:
	.string	"fp_offset"
.LASF14:
	.string	"__uint8_t"
.LASF93:
	.string	"__data"
.LASF354:
	.string	"UV_HANDLE_PIPESERVER"
.LASF282:
	.string	"UV_UDP"
.LASF152:
	.string	"pending_queue"
.LASF358:
	.string	"UV_HANDLE_TTY_SAVED_ATTRIBUTES"
.LASF276:
	.string	"UV_PREPARE"
.LASF28:
	.string	"_IO_buf_base"
.LASF139:
	.string	"__timezone"
.LASF178:
	.string	"inotify_fd"
.LASF280:
	.string	"UV_TIMER"
.LASF74:
	.string	"__nusers"
.LASF44:
	.string	"_wide_data"
.LASF320:
	.string	"QUEUE"
.LASF41:
	.string	"_lock"
.LASF283:
	.string	"UV_SIGNAL"
.LASF131:
	.string	"in6_addr"
.LASF341:
	.string	"UV_HANDLE_BLOCKING_WRITES"
.LASF52:
	.string	"_IO_codecvt"
.LASF408:
	.string	"uv_mutex_init"
.LASF352:
	.string	"UV_HANDLE_UDP_RECVMMSG"
.LASF37:
	.string	"_old_offset"
.LASF246:
	.string	"UV_EPROTO"
.LASF65:
	.string	"_IO_FILE"
.LASF317:
	.string	"unused"
.LASF82:
	.string	"__wrphase_futex"
.LASF166:
	.string	"async_unused"
.LASF8:
	.string	"reg_save_area"
.LASF243:
	.string	"UV_ENOTSUP"
.LASF305:
	.string	"dispatched_signals"
.LASF363:
	.string	"UV_CLOCK_PRECISE"
.LASF380:
	.string	"saved_data"
.LASF399:
	.string	"__assert_fail"
.LASF96:
	.string	"pthread_mutex_t"
.LASF269:
	.string	"UV_CHECK"
.LASF71:
	.string	"__lock"
.LASF329:
	.string	"UV_HANDLE_SHUTTING"
.LASF297:
	.string	"queue"
.LASF125:
	.string	"in_addr"
.LASF387:
	.string	"__dest"
.LASF378:
	.string	"uv_loop_fork"
.LASF198:
	.string	"UV_EAI_FAMILY"
.LASF291:
	.string	"type"
.LASF333:
	.string	"UV_HANDLE_READING"
.LASF10:
	.string	"unsigned char"
.LASF190:
	.string	"UV_EAFNOSUPPORT"
.LASF17:
	.string	"__uint32_t"
.LASF137:
	.string	"__tzname"
.LASF402:
	.string	"uv__signal_loop_fork"
.LASF263:
	.string	"UV_ENOTTY"
.LASF26:
	.string	"_IO_write_ptr"
.LASF254:
	.string	"UV_ETIMEDOUT"
.LASF80:
	.string	"__readers"
.LASF277:
	.string	"UV_PROCESS"
.LASF303:
	.string	"tree_entry"
.LASF219:
	.string	"UV_EINTR"
.LASF398:
	.string	"uv__free"
.LASF141:
	.string	"daylight"
.LASF328:
	.string	"UV_HANDLE_CONNECTION"
.LASF392:
	.string	"uv__async_stop"
.LASF348:
	.string	"UV_HANDLE_TCP_SOCKET_CLOSED"
.LASF19:
	.string	"__off_t"
.LASF295:
	.string	"uv_async_s"
.LASF294:
	.string	"uv_async_t"
.LASF12:
	.string	"signed char"
.LASF101:
	.string	"sa_family"
.LASF384:
	.string	"fail_async_init"
.LASF196:
	.string	"UV_EAI_CANCELED"
.LASF213:
	.string	"UV_ECONNRESET"
.LASF167:
	.string	"async_io_watcher"
.LASF161:
	.string	"process_handles"
.LASF60:
	.string	"_sys_errlist"
.LASF149:
	.string	"stop_flag"
.LASF186:
	.string	"UV_E2BIG"
.LASF416:
	.string	"__PRETTY_FUNCTION__"
.LASF172:
	.string	"signal_pipefd"
.LASF349:
	.string	"UV_HANDLE_SHARED_TCP_SOCKET"
.LASF311:
	.string	"reserved"
.LASF164:
	.string	"idle_handles"
.LASF173:
	.string	"signal_io_watcher"
.LASF252:
	.string	"UV_ESPIPE"
.LASF312:
	.string	"double"
.LASF356:
	.string	"UV_HANDLE_TTY_RAW"
.LASF327:
	.string	"UV_HANDLE_LISTENING"
.LASF95:
	.string	"__align"
.LASF301:
	.string	"signal_cb"
.LASF34:
	.string	"_chain"
.LASF364:
	.string	"UV_CLOCK_FAST"
.LASF144:
	.string	"uv_loop_s"
.LASF287:
	.string	"uv_loop_t"
.LASF162:
	.string	"prepare_handles"
.LASF36:
	.string	"_flags2"
.LASF340:
	.string	"UV_HANDLE_EMULATE_IOCP"
.LASF94:
	.string	"__size"
.LASF410:
	.string	"GNU C89 9.3.0 -m64 -mtune=generic -march=x86-64 -g -O3 -std=gnu90 -fvisibility=hidden -fno-omit-frame-pointer -fasynchronous-unwind-tables -fstack-protector-strong -fstack-clash-protection -fcf-protection"
.LASF112:
	.string	"sockaddr_in6"
.LASF234:
	.string	"UV_ENOMEM"
.LASF261:
	.string	"UV_EHOSTDOWN"
.LASF20:
	.string	"__off64_t"
.LASF49:
	.string	"_unused2"
.LASF70:
	.string	"__pthread_mutex_s"
.LASF220:
	.string	"UV_EINVAL"
.LASF306:
	.string	"UV_LOOP_BLOCK_SIGNAL"
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
