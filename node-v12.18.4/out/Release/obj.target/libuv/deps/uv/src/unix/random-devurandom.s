	.file	"random-devurandom.c"
	.text
.Ltext0:
	.p2align 4
	.globl	uv__random_readpath
	.hidden	uv__random_readpath
	.type	uv__random_readpath, @function
uv__random_readpath:
.LVL0:
.LFB94:
	.file 1 "../deps/uv/src/unix/random-devurandom.c"
	.loc 1 32 69 view -0
	.cfi_startproc
	.loc 1 32 69 is_stmt 0 view .LVU1
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$184, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	.loc 1 32 69 view .LVU2
	movq	%rsi, -216(%rbp)
	.loc 1 38 8 view .LVU3
	xorl	%esi, %esi
.LVL1:
	.loc 1 32 69 view .LVU4
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	.loc 1 33 3 is_stmt 1 view .LVU5
	.loc 1 34 3 view .LVU6
	.loc 1 35 3 view .LVU7
	.loc 1 36 3 view .LVU8
	.loc 1 38 3 view .LVU9
	.loc 1 38 8 is_stmt 0 view .LVU10
	call	uv__open_cloexec@PLT
.LVL2:
	.loc 1 38 8 view .LVU11
	movl	%eax, %r12d
.LVL3:
	.loc 1 40 3 is_stmt 1 view .LVU12
	.loc 1 40 6 is_stmt 0 view .LVU13
	testl	%eax, %eax
	js	.L1
	.loc 1 43 3 is_stmt 1 view .LVU14
.LVL4:
.LBB6:
.LBI6:
	.file 2 "/usr/include/x86_64-linux-gnu/sys/stat.h"
	.loc 2 467 42 view .LVU15
.LBB7:
	.loc 2 469 3 view .LVU16
	.loc 2 469 10 is_stmt 0 view .LVU17
	leaq	-208(%rbp), %rdx
.LVL5:
	.loc 2 469 10 view .LVU18
	movl	%eax, %esi
	movl	$1, %edi
	call	__fxstat64@PLT
.LVL6:
	.loc 2 469 10 view .LVU19
.LBE7:
.LBE6:
	.loc 1 43 6 view .LVU20
	testl	%eax, %eax
	jne	.L21
	.loc 1 48 3 is_stmt 1 view .LVU21
	.loc 1 48 10 is_stmt 0 view .LVU22
	movl	-184(%rbp), %eax
	andl	$61440, %eax
	.loc 1 48 6 view .LVU23
	cmpl	$8192, %eax
	jne	.L10
.LVL7:
	.loc 1 53 17 is_stmt 1 view .LVU24
	.loc 1 53 12 is_stmt 0 view .LVU25
	xorl	%r14d, %r14d
	.loc 1 53 3 view .LVU26
	testq	%r15, %r15
	je	.L9
.LVL8:
	.p2align 4,,10
	.p2align 3
.L5:
	.loc 1 53 3 view .LVU27
	movq	-216(%rbp), %rax
	movq	%r15, %r13
	subq	%r14, %r13
	leaq	(%rax,%r14), %rbx
	jmp	.L7
.LVL9:
	.p2align 4,,10
	.p2align 3
.L23:
	.loc 1 56 24 discriminator 1 view .LVU28
	call	__errno_location@PLT
.LVL10:
	.loc 1 56 20 discriminator 1 view .LVU29
	cmpl	$4, (%rax)
	jne	.L22
.L7:
	.loc 1 54 5 is_stmt 1 discriminator 2 view .LVU30
	.loc 1 55 7 discriminator 2 view .LVU31
.LVL11:
.LBB8:
.LBI8:
	.file 3 "/usr/include/x86_64-linux-gnu/bits/unistd.h"
	.loc 3 34 1 discriminator 2 view .LVU32
.LBB9:
	.loc 3 36 3 discriminator 2 view .LVU33
	.loc 3 44 3 discriminator 2 view .LVU34
	.loc 3 44 10 is_stmt 0 discriminator 2 view .LVU35
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movl	%r12d, %edi
	call	read@PLT
.LVL12:
	.loc 3 44 10 discriminator 2 view .LVU36
.LBE9:
.LBE8:
	.loc 1 56 11 is_stmt 1 discriminator 2 view .LVU37
	.loc 1 56 36 is_stmt 0 discriminator 2 view .LVU38
	cmpq	$-1, %rax
	je	.L23
	.loc 1 58 5 is_stmt 1 view .LVU39
	.loc 1 63 5 view .LVU40
	.loc 1 63 8 is_stmt 0 view .LVU41
	testq	%rax, %rax
	je	.L10
	.loc 1 53 32 is_stmt 1 discriminator 2 view .LVU42
	.loc 1 53 36 is_stmt 0 discriminator 2 view .LVU43
	addq	%rax, %r14
.LVL13:
	.loc 1 53 17 is_stmt 1 discriminator 2 view .LVU44
	.loc 1 53 3 is_stmt 0 discriminator 2 view .LVU45
	cmpq	%r14, %r15
	jne	.L5
.LVL14:
.L9:
	.loc 1 69 3 is_stmt 1 view .LVU46
	movl	%r12d, %edi
	.loc 1 70 10 is_stmt 0 view .LVU47
	xorl	%r12d, %r12d
.LVL15:
	.loc 1 69 3 view .LVU48
	call	uv__close@PLT
.LVL16:
	.loc 1 70 3 is_stmt 1 view .LVU49
	.p2align 4,,10
	.p2align 3
.L1:
	.loc 1 71 1 is_stmt 0 view .LVU50
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L24
	addq	$184, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
.LVL17:
	.loc 1 71 1 view .LVU51
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.LVL18:
.L21:
	.cfi_restore_state
	.loc 1 44 5 is_stmt 1 view .LVU52
	movl	%r12d, %edi
	call	uv__close@PLT
.LVL19:
	.loc 1 45 5 view .LVU53
	.loc 1 45 13 is_stmt 0 view .LVU54
	call	__errno_location@PLT
.LVL20:
	.loc 1 45 13 view .LVU55
	movl	(%rax), %r12d
.LVL21:
	.loc 1 45 13 view .LVU56
	negl	%r12d
	jmp	.L1
.LVL22:
	.p2align 4,,10
	.p2align 3
.L22:
	.loc 1 59 7 view .LVU57
	movl	%r12d, %edi
	movq	%rax, -216(%rbp)
.LVL23:
	.loc 1 58 5 is_stmt 1 view .LVU58
	.loc 1 59 7 view .LVU59
	call	uv__close@PLT
.LVL24:
	.loc 1 60 7 view .LVU60
	.loc 1 60 15 is_stmt 0 view .LVU61
	movq	-216(%rbp), %rdx
	movl	(%rdx), %r12d
.LVL25:
	.loc 1 60 15 view .LVU62
	negl	%r12d
	jmp	.L1
.LVL26:
.L10:
	.loc 1 64 7 is_stmt 1 view .LVU63
	movl	%r12d, %edi
	.loc 1 65 14 is_stmt 0 view .LVU64
	movl	$-5, %r12d
.LVL27:
	.loc 1 64 7 view .LVU65
	call	uv__close@PLT
.LVL28:
	.loc 1 65 7 is_stmt 1 view .LVU66
	.loc 1 65 14 is_stmt 0 view .LVU67
	jmp	.L1
.LVL29:
.L24:
	.loc 1 71 1 view .LVU68
	call	__stack_chk_fail@PLT
.LVL30:
	.cfi_endproc
.LFE94:
	.size	uv__random_readpath, .-uv__random_readpath
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"/dev/random"
	.text
	.p2align 4
	.type	uv__random_devurandom_init, @function
uv__random_devurandom_init:
.LFB95:
	.loc 1 74 46 is_stmt 1 view -0
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	.loc 1 82 12 is_stmt 0 view .LVU70
	movl	$1, %edx
	leaq	.LC0(%rip), %rdi
	.loc 1 74 46 view .LVU71
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	.loc 1 74 46 view .LVU72
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	.loc 1 75 3 is_stmt 1 view .LVU73
	.loc 1 82 3 view .LVU74
	.loc 1 82 12 is_stmt 0 view .LVU75
	leaq	-9(%rbp), %rsi
	call	uv__random_readpath
.LVL31:
	.loc 1 82 10 view .LVU76
	movl	%eax, status(%rip)
	.loc 1 83 1 view .LVU77
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L28
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L28:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
.LVL32:
	.cfi_endproc
.LFE95:
	.size	uv__random_devurandom_init, .-uv__random_devurandom_init
	.section	.rodata.str1.1
.LC1:
	.string	"/dev/urandom"
	.text
	.p2align 4
	.globl	uv__random_devurandom
	.hidden	uv__random_devurandom
	.type	uv__random_devurandom, @function
uv__random_devurandom:
.LVL33:
.LFB96:
	.loc 1 86 53 is_stmt 1 view -0
	.cfi_startproc
	.loc 1 86 53 is_stmt 0 view .LVU79
	endbr64
	.loc 1 87 3 is_stmt 1 view .LVU80
	.loc 1 86 53 is_stmt 0 view .LVU81
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	.loc 1 87 3 view .LVU82
	leaq	uv__random_devurandom_init(%rip), %rsi
.LVL34:
	.loc 1 86 53 view .LVU83
	pushq	%r12
	.cfi_offset 12, -32
	.loc 1 86 53 view .LVU84
	movq	%rdi, %r12
	.loc 1 87 3 view .LVU85
	leaq	once(%rip), %rdi
.LVL35:
	.loc 1 87 3 view .LVU86
	call	uv_once@PLT
.LVL36:
	.loc 1 89 3 is_stmt 1 view .LVU87
	.loc 1 89 14 is_stmt 0 view .LVU88
	movl	status(%rip), %eax
	.loc 1 89 6 view .LVU89
	testl	%eax, %eax
	je	.L32
	.loc 1 93 1 view .LVU90
	popq	%r12
.LVL37:
	.loc 1 93 1 view .LVU91
	popq	%r13
.LVL38:
	.loc 1 93 1 view .LVU92
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.LVL39:
	.p2align 4,,10
	.p2align 3
.L32:
	.cfi_restore_state
	.loc 1 92 3 is_stmt 1 view .LVU93
	.loc 1 92 10 is_stmt 0 view .LVU94
	movq	%r13, %rdx
	movq	%r12, %rsi
	leaq	.LC1(%rip), %rdi
	.loc 1 93 1 view .LVU95
	popq	%r12
.LVL40:
	.loc 1 93 1 view .LVU96
	popq	%r13
.LVL41:
	.loc 1 93 1 view .LVU97
	popq	%rbp
	.cfi_def_cfa 7, 8
	.loc 1 92 10 view .LVU98
	jmp	uv__random_readpath
.LVL42:
	.loc 1 92 10 view .LVU99
	.cfi_endproc
.LFE96:
	.size	uv__random_devurandom, .-uv__random_devurandom
	.local	status
	.comm	status,4,4
	.local	once
	.comm	once,4,4
.Letext0:
	.file 4 "/usr/include/errno.h"
	.file 5 "/usr/lib/gcc/x86_64-linux-gnu/9/include/stddef.h"
	.file 6 "/usr/include/x86_64-linux-gnu/bits/types.h"
	.file 7 "/usr/include/x86_64-linux-gnu/bits/types/struct_FILE.h"
	.file 8 "/usr/include/x86_64-linux-gnu/bits/types/FILE.h"
	.file 9 "/usr/include/stdio.h"
	.file 10 "/usr/include/x86_64-linux-gnu/bits/sys_errlist.h"
	.file 11 "/usr/include/x86_64-linux-gnu/bits/stdint-uintn.h"
	.file 12 "/usr/include/x86_64-linux-gnu/bits/types/struct_timespec.h"
	.file 13 "/usr/include/x86_64-linux-gnu/bits/pthreadtypes.h"
	.file 14 "/usr/include/x86_64-linux-gnu/bits/stat.h"
	.file 15 "/usr/include/x86_64-linux-gnu/bits/sockaddr.h"
	.file 16 "/usr/include/x86_64-linux-gnu/bits/socket.h"
	.file 17 "/usr/include/netinet/in.h"
	.file 18 "/usr/include/signal.h"
	.file 19 "/usr/include/time.h"
	.file 20 "../deps/uv/include/uv/unix.h"
	.file 21 "/usr/include/unistd.h"
	.file 22 "/usr/include/x86_64-linux-gnu/bits/getopt_core.h"
	.file 23 "../deps/uv/include/uv.h"
	.file 24 "../deps/uv/src/unix/internal.h"
	.section	.debug_info,"",@progbits
.Ldebug_info0:
	.long	0xf58
	.value	0x4
	.long	.Ldebug_abbrev0
	.byte	0x8
	.uleb128 0x1
	.long	.LASF247
	.byte	0x1
	.long	.LASF248
	.long	.LASF249
	.quad	.Ltext0
	.quad	.Letext0-.Ltext0
	.long	.Ldebug_line0
	.uleb128 0x2
	.long	.LASF0
	.byte	0x4
	.byte	0x2d
	.byte	0xe
	.long	0x39
	.uleb128 0x3
	.byte	0x8
	.long	0x3f
	.uleb128 0x4
	.byte	0x1
	.byte	0x6
	.long	.LASF2
	.uleb128 0x5
	.long	0x3f
	.uleb128 0x2
	.long	.LASF1
	.byte	0x4
	.byte	0x2e
	.byte	0xe
	.long	0x39
	.uleb128 0x6
	.byte	0x4
	.byte	0x5
	.string	"int"
	.uleb128 0x4
	.byte	0x8
	.byte	0x5
	.long	.LASF3
	.uleb128 0x7
	.long	.LASF9
	.byte	0x5
	.byte	0xd1
	.byte	0x1b
	.long	0x71
	.uleb128 0x4
	.byte	0x8
	.byte	0x7
	.long	.LASF4
	.uleb128 0x4
	.byte	0x4
	.byte	0x7
	.long	.LASF5
	.uleb128 0x8
	.byte	0x8
	.uleb128 0x4
	.byte	0x1
	.byte	0x8
	.long	.LASF6
	.uleb128 0x4
	.byte	0x2
	.byte	0x7
	.long	.LASF7
	.uleb128 0x4
	.byte	0x1
	.byte	0x6
	.long	.LASF8
	.uleb128 0x7
	.long	.LASF10
	.byte	0x6
	.byte	0x26
	.byte	0x17
	.long	0x81
	.uleb128 0x4
	.byte	0x2
	.byte	0x5
	.long	.LASF11
	.uleb128 0x7
	.long	.LASF12
	.byte	0x6
	.byte	0x28
	.byte	0x1c
	.long	0x88
	.uleb128 0x7
	.long	.LASF13
	.byte	0x6
	.byte	0x2a
	.byte	0x16
	.long	0x78
	.uleb128 0x7
	.long	.LASF14
	.byte	0x6
	.byte	0x91
	.byte	0x1b
	.long	0x71
	.uleb128 0x7
	.long	.LASF15
	.byte	0x6
	.byte	0x92
	.byte	0x16
	.long	0x78
	.uleb128 0x7
	.long	.LASF16
	.byte	0x6
	.byte	0x93
	.byte	0x16
	.long	0x78
	.uleb128 0x7
	.long	.LASF17
	.byte	0x6
	.byte	0x94
	.byte	0x1b
	.long	0x71
	.uleb128 0x7
	.long	.LASF18
	.byte	0x6
	.byte	0x96
	.byte	0x16
	.long	0x78
	.uleb128 0x7
	.long	.LASF19
	.byte	0x6
	.byte	0x97
	.byte	0x1b
	.long	0x71
	.uleb128 0x7
	.long	.LASF20
	.byte	0x6
	.byte	0x98
	.byte	0x12
	.long	0x5e
	.uleb128 0x7
	.long	.LASF21
	.byte	0x6
	.byte	0x99
	.byte	0x12
	.long	0x5e
	.uleb128 0x7
	.long	.LASF22
	.byte	0x6
	.byte	0xa0
	.byte	0x12
	.long	0x5e
	.uleb128 0x7
	.long	.LASF23
	.byte	0x6
	.byte	0xae
	.byte	0x12
	.long	0x5e
	.uleb128 0x7
	.long	.LASF24
	.byte	0x6
	.byte	0xb3
	.byte	0x12
	.long	0x5e
	.uleb128 0x7
	.long	.LASF25
	.byte	0x6
	.byte	0xc1
	.byte	0x12
	.long	0x5e
	.uleb128 0x7
	.long	.LASF26
	.byte	0x6
	.byte	0xc4
	.byte	0x12
	.long	0x5e
	.uleb128 0x9
	.long	.LASF71
	.byte	0xd8
	.byte	0x7
	.byte	0x31
	.byte	0x8
	.long	0x2e4
	.uleb128 0xa
	.long	.LASF27
	.byte	0x7
	.byte	0x33
	.byte	0x7
	.long	0x57
	.byte	0
	.uleb128 0xa
	.long	.LASF28
	.byte	0x7
	.byte	0x36
	.byte	0x9
	.long	0x39
	.byte	0x8
	.uleb128 0xa
	.long	.LASF29
	.byte	0x7
	.byte	0x37
	.byte	0x9
	.long	0x39
	.byte	0x10
	.uleb128 0xa
	.long	.LASF30
	.byte	0x7
	.byte	0x38
	.byte	0x9
	.long	0x39
	.byte	0x18
	.uleb128 0xa
	.long	.LASF31
	.byte	0x7
	.byte	0x39
	.byte	0x9
	.long	0x39
	.byte	0x20
	.uleb128 0xa
	.long	.LASF32
	.byte	0x7
	.byte	0x3a
	.byte	0x9
	.long	0x39
	.byte	0x28
	.uleb128 0xa
	.long	.LASF33
	.byte	0x7
	.byte	0x3b
	.byte	0x9
	.long	0x39
	.byte	0x30
	.uleb128 0xa
	.long	.LASF34
	.byte	0x7
	.byte	0x3c
	.byte	0x9
	.long	0x39
	.byte	0x38
	.uleb128 0xa
	.long	.LASF35
	.byte	0x7
	.byte	0x3d
	.byte	0x9
	.long	0x39
	.byte	0x40
	.uleb128 0xa
	.long	.LASF36
	.byte	0x7
	.byte	0x40
	.byte	0x9
	.long	0x39
	.byte	0x48
	.uleb128 0xa
	.long	.LASF37
	.byte	0x7
	.byte	0x41
	.byte	0x9
	.long	0x39
	.byte	0x50
	.uleb128 0xa
	.long	.LASF38
	.byte	0x7
	.byte	0x42
	.byte	0x9
	.long	0x39
	.byte	0x58
	.uleb128 0xa
	.long	.LASF39
	.byte	0x7
	.byte	0x44
	.byte	0x16
	.long	0x2fd
	.byte	0x60
	.uleb128 0xa
	.long	.LASF40
	.byte	0x7
	.byte	0x46
	.byte	0x14
	.long	0x303
	.byte	0x68
	.uleb128 0xa
	.long	.LASF41
	.byte	0x7
	.byte	0x48
	.byte	0x7
	.long	0x57
	.byte	0x70
	.uleb128 0xa
	.long	.LASF42
	.byte	0x7
	.byte	0x49
	.byte	0x7
	.long	0x57
	.byte	0x74
	.uleb128 0xa
	.long	.LASF43
	.byte	0x7
	.byte	0x4a
	.byte	0xb
	.long	0x109
	.byte	0x78
	.uleb128 0xa
	.long	.LASF44
	.byte	0x7
	.byte	0x4d
	.byte	0x12
	.long	0x88
	.byte	0x80
	.uleb128 0xa
	.long	.LASF45
	.byte	0x7
	.byte	0x4e
	.byte	0xf
	.long	0x8f
	.byte	0x82
	.uleb128 0xa
	.long	.LASF46
	.byte	0x7
	.byte	0x4f
	.byte	0x8
	.long	0x309
	.byte	0x83
	.uleb128 0xa
	.long	.LASF47
	.byte	0x7
	.byte	0x51
	.byte	0xf
	.long	0x319
	.byte	0x88
	.uleb128 0xa
	.long	.LASF48
	.byte	0x7
	.byte	0x59
	.byte	0xd
	.long	0x115
	.byte	0x90
	.uleb128 0xa
	.long	.LASF49
	.byte	0x7
	.byte	0x5b
	.byte	0x17
	.long	0x324
	.byte	0x98
	.uleb128 0xa
	.long	.LASF50
	.byte	0x7
	.byte	0x5c
	.byte	0x19
	.long	0x32f
	.byte	0xa0
	.uleb128 0xa
	.long	.LASF51
	.byte	0x7
	.byte	0x5d
	.byte	0x14
	.long	0x303
	.byte	0xa8
	.uleb128 0xa
	.long	.LASF52
	.byte	0x7
	.byte	0x5e
	.byte	0x9
	.long	0x7f
	.byte	0xb0
	.uleb128 0xa
	.long	.LASF53
	.byte	0x7
	.byte	0x5f
	.byte	0xa
	.long	0x65
	.byte	0xb8
	.uleb128 0xa
	.long	.LASF54
	.byte	0x7
	.byte	0x60
	.byte	0x7
	.long	0x57
	.byte	0xc0
	.uleb128 0xa
	.long	.LASF55
	.byte	0x7
	.byte	0x62
	.byte	0x8
	.long	0x335
	.byte	0xc4
	.byte	0
	.uleb128 0x7
	.long	.LASF56
	.byte	0x8
	.byte	0x7
	.byte	0x19
	.long	0x15d
	.uleb128 0xb
	.long	.LASF250
	.byte	0x7
	.byte	0x2b
	.byte	0xe
	.uleb128 0xc
	.long	.LASF57
	.uleb128 0x3
	.byte	0x8
	.long	0x2f8
	.uleb128 0x3
	.byte	0x8
	.long	0x15d
	.uleb128 0xd
	.long	0x3f
	.long	0x319
	.uleb128 0xe
	.long	0x71
	.byte	0
	.byte	0
	.uleb128 0x3
	.byte	0x8
	.long	0x2f0
	.uleb128 0xc
	.long	.LASF58
	.uleb128 0x3
	.byte	0x8
	.long	0x31f
	.uleb128 0xc
	.long	.LASF59
	.uleb128 0x3
	.byte	0x8
	.long	0x32a
	.uleb128 0xd
	.long	0x3f
	.long	0x345
	.uleb128 0xe
	.long	0x71
	.byte	0x13
	.byte	0
	.uleb128 0x3
	.byte	0x8
	.long	0x46
	.uleb128 0x5
	.long	0x345
	.uleb128 0x7
	.long	.LASF60
	.byte	0x9
	.byte	0x4d
	.byte	0x13
	.long	0x145
	.uleb128 0x2
	.long	.LASF61
	.byte	0x9
	.byte	0x89
	.byte	0xe
	.long	0x368
	.uleb128 0x3
	.byte	0x8
	.long	0x2e4
	.uleb128 0x2
	.long	.LASF62
	.byte	0x9
	.byte	0x8a
	.byte	0xe
	.long	0x368
	.uleb128 0x2
	.long	.LASF63
	.byte	0x9
	.byte	0x8b
	.byte	0xe
	.long	0x368
	.uleb128 0x2
	.long	.LASF64
	.byte	0xa
	.byte	0x1a
	.byte	0xc
	.long	0x57
	.uleb128 0xd
	.long	0x34b
	.long	0x39d
	.uleb128 0xf
	.byte	0
	.uleb128 0x5
	.long	0x392
	.uleb128 0x2
	.long	.LASF65
	.byte	0xa
	.byte	0x1b
	.byte	0x1a
	.long	0x39d
	.uleb128 0x2
	.long	.LASF66
	.byte	0xa
	.byte	0x1e
	.byte	0xc
	.long	0x57
	.uleb128 0x2
	.long	.LASF67
	.byte	0xa
	.byte	0x1f
	.byte	0x1a
	.long	0x39d
	.uleb128 0x7
	.long	.LASF68
	.byte	0xb
	.byte	0x18
	.byte	0x13
	.long	0x96
	.uleb128 0x7
	.long	.LASF69
	.byte	0xb
	.byte	0x19
	.byte	0x14
	.long	0xa9
	.uleb128 0x7
	.long	.LASF70
	.byte	0xb
	.byte	0x1a
	.byte	0x14
	.long	0xb5
	.uleb128 0x9
	.long	.LASF72
	.byte	0x10
	.byte	0xc
	.byte	0xa
	.byte	0x8
	.long	0x412
	.uleb128 0xa
	.long	.LASF73
	.byte	0xc
	.byte	0xc
	.byte	0xc
	.long	0x121
	.byte	0
	.uleb128 0xa
	.long	.LASF74
	.byte	0xc
	.byte	0x10
	.byte	0x15
	.long	0x151
	.byte	0x8
	.byte	0
	.uleb128 0x4
	.byte	0x8
	.byte	0x7
	.long	.LASF75
	.uleb128 0x7
	.long	.LASF76
	.byte	0xd
	.byte	0x35
	.byte	0xd
	.long	0x57
	.uleb128 0x4
	.byte	0x8
	.byte	0x5
	.long	.LASF77
	.uleb128 0x9
	.long	.LASF78
	.byte	0x90
	.byte	0xe
	.byte	0x2e
	.byte	0x8
	.long	0x4fd
	.uleb128 0xa
	.long	.LASF79
	.byte	0xe
	.byte	0x30
	.byte	0xd
	.long	0xc1
	.byte	0
	.uleb128 0xa
	.long	.LASF80
	.byte	0xe
	.byte	0x35
	.byte	0xd
	.long	0xe5
	.byte	0x8
	.uleb128 0xa
	.long	.LASF81
	.byte	0xe
	.byte	0x3d
	.byte	0xf
	.long	0xfd
	.byte	0x10
	.uleb128 0xa
	.long	.LASF82
	.byte	0xe
	.byte	0x3e
	.byte	0xe
	.long	0xf1
	.byte	0x18
	.uleb128 0xa
	.long	.LASF83
	.byte	0xe
	.byte	0x40
	.byte	0xd
	.long	0xcd
	.byte	0x1c
	.uleb128 0xa
	.long	.LASF84
	.byte	0xe
	.byte	0x41
	.byte	0xd
	.long	0xd9
	.byte	0x20
	.uleb128 0xa
	.long	.LASF85
	.byte	0xe
	.byte	0x43
	.byte	0x9
	.long	0x57
	.byte	0x24
	.uleb128 0xa
	.long	.LASF86
	.byte	0xe
	.byte	0x45
	.byte	0xd
	.long	0xc1
	.byte	0x28
	.uleb128 0xa
	.long	.LASF87
	.byte	0xe
	.byte	0x4a
	.byte	0xd
	.long	0x109
	.byte	0x30
	.uleb128 0xa
	.long	.LASF88
	.byte	0xe
	.byte	0x4e
	.byte	0x11
	.long	0x12d
	.byte	0x38
	.uleb128 0xa
	.long	.LASF89
	.byte	0xe
	.byte	0x50
	.byte	0x10
	.long	0x139
	.byte	0x40
	.uleb128 0xa
	.long	.LASF90
	.byte	0xe
	.byte	0x5b
	.byte	0x15
	.long	0x3ea
	.byte	0x48
	.uleb128 0xa
	.long	.LASF91
	.byte	0xe
	.byte	0x5c
	.byte	0x15
	.long	0x3ea
	.byte	0x58
	.uleb128 0xa
	.long	.LASF92
	.byte	0xe
	.byte	0x5d
	.byte	0x15
	.long	0x3ea
	.byte	0x68
	.uleb128 0xa
	.long	.LASF93
	.byte	0xe
	.byte	0x6a
	.byte	0x17
	.long	0x4fd
	.byte	0x78
	.byte	0
	.uleb128 0xd
	.long	0x151
	.long	0x50d
	.uleb128 0xe
	.long	0x71
	.byte	0x2
	.byte	0
	.uleb128 0x7
	.long	.LASF94
	.byte	0xf
	.byte	0x1c
	.byte	0x1c
	.long	0x88
	.uleb128 0x9
	.long	.LASF95
	.byte	0x10
	.byte	0x10
	.byte	0xb2
	.byte	0x8
	.long	0x541
	.uleb128 0xa
	.long	.LASF96
	.byte	0x10
	.byte	0xb4
	.byte	0x11
	.long	0x50d
	.byte	0
	.uleb128 0xa
	.long	.LASF97
	.byte	0x10
	.byte	0xb5
	.byte	0xa
	.long	0x546
	.byte	0x2
	.byte	0
	.uleb128 0x5
	.long	0x519
	.uleb128 0xd
	.long	0x3f
	.long	0x556
	.uleb128 0xe
	.long	0x71
	.byte	0xd
	.byte	0
	.uleb128 0x3
	.byte	0x8
	.long	0x519
	.uleb128 0x10
	.long	0x556
	.uleb128 0xc
	.long	.LASF98
	.uleb128 0x5
	.long	0x561
	.uleb128 0x3
	.byte	0x8
	.long	0x561
	.uleb128 0x10
	.long	0x56b
	.uleb128 0xc
	.long	.LASF99
	.uleb128 0x5
	.long	0x576
	.uleb128 0x3
	.byte	0x8
	.long	0x576
	.uleb128 0x10
	.long	0x580
	.uleb128 0xc
	.long	.LASF100
	.uleb128 0x5
	.long	0x58b
	.uleb128 0x3
	.byte	0x8
	.long	0x58b
	.uleb128 0x10
	.long	0x595
	.uleb128 0xc
	.long	.LASF101
	.uleb128 0x5
	.long	0x5a0
	.uleb128 0x3
	.byte	0x8
	.long	0x5a0
	.uleb128 0x10
	.long	0x5aa
	.uleb128 0x9
	.long	.LASF102
	.byte	0x10
	.byte	0x11
	.byte	0xee
	.byte	0x8
	.long	0x5f7
	.uleb128 0xa
	.long	.LASF103
	.byte	0x11
	.byte	0xf0
	.byte	0x11
	.long	0x50d
	.byte	0
	.uleb128 0xa
	.long	.LASF104
	.byte	0x11
	.byte	0xf1
	.byte	0xf
	.long	0x79e
	.byte	0x2
	.uleb128 0xa
	.long	.LASF105
	.byte	0x11
	.byte	0xf2
	.byte	0x14
	.long	0x783
	.byte	0x4
	.uleb128 0xa
	.long	.LASF106
	.byte	0x11
	.byte	0xf5
	.byte	0x13
	.long	0x840
	.byte	0x8
	.byte	0
	.uleb128 0x5
	.long	0x5b5
	.uleb128 0x3
	.byte	0x8
	.long	0x5b5
	.uleb128 0x10
	.long	0x5fc
	.uleb128 0x9
	.long	.LASF107
	.byte	0x1c
	.byte	0x11
	.byte	0xfd
	.byte	0x8
	.long	0x65a
	.uleb128 0xa
	.long	.LASF108
	.byte	0x11
	.byte	0xff
	.byte	0x11
	.long	0x50d
	.byte	0
	.uleb128 0x11
	.long	.LASF109
	.byte	0x11
	.value	0x100
	.byte	0xf
	.long	0x79e
	.byte	0x2
	.uleb128 0x11
	.long	.LASF110
	.byte	0x11
	.value	0x101
	.byte	0xe
	.long	0x3de
	.byte	0x4
	.uleb128 0x11
	.long	.LASF111
	.byte	0x11
	.value	0x102
	.byte	0x15
	.long	0x808
	.byte	0x8
	.uleb128 0x11
	.long	.LASF112
	.byte	0x11
	.value	0x103
	.byte	0xe
	.long	0x3de
	.byte	0x18
	.byte	0
	.uleb128 0x5
	.long	0x607
	.uleb128 0x3
	.byte	0x8
	.long	0x607
	.uleb128 0x10
	.long	0x65f
	.uleb128 0xc
	.long	.LASF113
	.uleb128 0x5
	.long	0x66a
	.uleb128 0x3
	.byte	0x8
	.long	0x66a
	.uleb128 0x10
	.long	0x674
	.uleb128 0xc
	.long	.LASF114
	.uleb128 0x5
	.long	0x67f
	.uleb128 0x3
	.byte	0x8
	.long	0x67f
	.uleb128 0x10
	.long	0x689
	.uleb128 0xc
	.long	.LASF115
	.uleb128 0x5
	.long	0x694
	.uleb128 0x3
	.byte	0x8
	.long	0x694
	.uleb128 0x10
	.long	0x69e
	.uleb128 0xc
	.long	.LASF116
	.uleb128 0x5
	.long	0x6a9
	.uleb128 0x3
	.byte	0x8
	.long	0x6a9
	.uleb128 0x10
	.long	0x6b3
	.uleb128 0xc
	.long	.LASF117
	.uleb128 0x5
	.long	0x6be
	.uleb128 0x3
	.byte	0x8
	.long	0x6be
	.uleb128 0x10
	.long	0x6c8
	.uleb128 0xc
	.long	.LASF118
	.uleb128 0x5
	.long	0x6d3
	.uleb128 0x3
	.byte	0x8
	.long	0x6d3
	.uleb128 0x10
	.long	0x6dd
	.uleb128 0x3
	.byte	0x8
	.long	0x541
	.uleb128 0x10
	.long	0x6e8
	.uleb128 0x3
	.byte	0x8
	.long	0x566
	.uleb128 0x10
	.long	0x6f3
	.uleb128 0x3
	.byte	0x8
	.long	0x57b
	.uleb128 0x10
	.long	0x6fe
	.uleb128 0x3
	.byte	0x8
	.long	0x590
	.uleb128 0x10
	.long	0x709
	.uleb128 0x3
	.byte	0x8
	.long	0x5a5
	.uleb128 0x10
	.long	0x714
	.uleb128 0x3
	.byte	0x8
	.long	0x5f7
	.uleb128 0x10
	.long	0x71f
	.uleb128 0x3
	.byte	0x8
	.long	0x65a
	.uleb128 0x10
	.long	0x72a
	.uleb128 0x3
	.byte	0x8
	.long	0x66f
	.uleb128 0x10
	.long	0x735
	.uleb128 0x3
	.byte	0x8
	.long	0x684
	.uleb128 0x10
	.long	0x740
	.uleb128 0x3
	.byte	0x8
	.long	0x699
	.uleb128 0x10
	.long	0x74b
	.uleb128 0x3
	.byte	0x8
	.long	0x6ae
	.uleb128 0x10
	.long	0x756
	.uleb128 0x3
	.byte	0x8
	.long	0x6c3
	.uleb128 0x10
	.long	0x761
	.uleb128 0x3
	.byte	0x8
	.long	0x6d8
	.uleb128 0x10
	.long	0x76c
	.uleb128 0x7
	.long	.LASF119
	.byte	0x11
	.byte	0x1e
	.byte	0x12
	.long	0x3de
	.uleb128 0x9
	.long	.LASF120
	.byte	0x4
	.byte	0x11
	.byte	0x1f
	.byte	0x8
	.long	0x79e
	.uleb128 0xa
	.long	.LASF121
	.byte	0x11
	.byte	0x21
	.byte	0xf
	.long	0x777
	.byte	0
	.byte	0
	.uleb128 0x7
	.long	.LASF122
	.byte	0x11
	.byte	0x77
	.byte	0x12
	.long	0x3d2
	.uleb128 0x12
	.byte	0x10
	.byte	0x11
	.byte	0xd6
	.byte	0x5
	.long	0x7d8
	.uleb128 0x13
	.long	.LASF123
	.byte	0x11
	.byte	0xd8
	.byte	0xa
	.long	0x7d8
	.uleb128 0x13
	.long	.LASF124
	.byte	0x11
	.byte	0xd9
	.byte	0xb
	.long	0x7e8
	.uleb128 0x13
	.long	.LASF125
	.byte	0x11
	.byte	0xda
	.byte	0xb
	.long	0x7f8
	.byte	0
	.uleb128 0xd
	.long	0x3c6
	.long	0x7e8
	.uleb128 0xe
	.long	0x71
	.byte	0xf
	.byte	0
	.uleb128 0xd
	.long	0x3d2
	.long	0x7f8
	.uleb128 0xe
	.long	0x71
	.byte	0x7
	.byte	0
	.uleb128 0xd
	.long	0x3de
	.long	0x808
	.uleb128 0xe
	.long	0x71
	.byte	0x3
	.byte	0
	.uleb128 0x9
	.long	.LASF126
	.byte	0x10
	.byte	0x11
	.byte	0xd4
	.byte	0x8
	.long	0x823
	.uleb128 0xa
	.long	.LASF127
	.byte	0x11
	.byte	0xdb
	.byte	0x9
	.long	0x7aa
	.byte	0
	.byte	0
	.uleb128 0x5
	.long	0x808
	.uleb128 0x2
	.long	.LASF128
	.byte	0x11
	.byte	0xe4
	.byte	0x1e
	.long	0x823
	.uleb128 0x2
	.long	.LASF129
	.byte	0x11
	.byte	0xe5
	.byte	0x1e
	.long	0x823
	.uleb128 0xd
	.long	0x81
	.long	0x850
	.uleb128 0xe
	.long	0x71
	.byte	0x7
	.byte	0
	.uleb128 0x3
	.byte	0x8
	.long	0x39
	.uleb128 0xd
	.long	0x34b
	.long	0x866
	.uleb128 0xe
	.long	0x71
	.byte	0x40
	.byte	0
	.uleb128 0x5
	.long	0x856
	.uleb128 0x14
	.long	.LASF130
	.byte	0x12
	.value	0x11e
	.byte	0x1a
	.long	0x866
	.uleb128 0x14
	.long	.LASF131
	.byte	0x12
	.value	0x11f
	.byte	0x1a
	.long	0x866
	.uleb128 0xd
	.long	0x39
	.long	0x895
	.uleb128 0xe
	.long	0x71
	.byte	0x1
	.byte	0
	.uleb128 0x2
	.long	.LASF132
	.byte	0x13
	.byte	0x9f
	.byte	0xe
	.long	0x885
	.uleb128 0x2
	.long	.LASF133
	.byte	0x13
	.byte	0xa0
	.byte	0xc
	.long	0x57
	.uleb128 0x2
	.long	.LASF134
	.byte	0x13
	.byte	0xa1
	.byte	0x11
	.long	0x5e
	.uleb128 0x2
	.long	.LASF135
	.byte	0x13
	.byte	0xa6
	.byte	0xe
	.long	0x885
	.uleb128 0x2
	.long	.LASF136
	.byte	0x13
	.byte	0xae
	.byte	0xc
	.long	0x57
	.uleb128 0x2
	.long	.LASF137
	.byte	0x13
	.byte	0xaf
	.byte	0x11
	.long	0x5e
	.uleb128 0x14
	.long	.LASF138
	.byte	0x13
	.value	0x112
	.byte	0xc
	.long	0x57
	.uleb128 0x7
	.long	.LASF139
	.byte	0x14
	.byte	0x85
	.byte	0x18
	.long	0x419
	.uleb128 0x15
	.byte	0x5
	.byte	0x4
	.long	0x57
	.byte	0x17
	.byte	0xb6
	.byte	0xe
	.long	0xb19
	.uleb128 0x16
	.long	.LASF140
	.sleb128 -7
	.uleb128 0x16
	.long	.LASF141
	.sleb128 -13
	.uleb128 0x16
	.long	.LASF142
	.sleb128 -98
	.uleb128 0x16
	.long	.LASF143
	.sleb128 -99
	.uleb128 0x16
	.long	.LASF144
	.sleb128 -97
	.uleb128 0x16
	.long	.LASF145
	.sleb128 -11
	.uleb128 0x16
	.long	.LASF146
	.sleb128 -3000
	.uleb128 0x16
	.long	.LASF147
	.sleb128 -3001
	.uleb128 0x16
	.long	.LASF148
	.sleb128 -3002
	.uleb128 0x16
	.long	.LASF149
	.sleb128 -3013
	.uleb128 0x16
	.long	.LASF150
	.sleb128 -3003
	.uleb128 0x16
	.long	.LASF151
	.sleb128 -3004
	.uleb128 0x16
	.long	.LASF152
	.sleb128 -3005
	.uleb128 0x16
	.long	.LASF153
	.sleb128 -3006
	.uleb128 0x16
	.long	.LASF154
	.sleb128 -3007
	.uleb128 0x16
	.long	.LASF155
	.sleb128 -3008
	.uleb128 0x16
	.long	.LASF156
	.sleb128 -3009
	.uleb128 0x16
	.long	.LASF157
	.sleb128 -3014
	.uleb128 0x16
	.long	.LASF158
	.sleb128 -3010
	.uleb128 0x16
	.long	.LASF159
	.sleb128 -3011
	.uleb128 0x16
	.long	.LASF160
	.sleb128 -114
	.uleb128 0x16
	.long	.LASF161
	.sleb128 -9
	.uleb128 0x16
	.long	.LASF162
	.sleb128 -16
	.uleb128 0x16
	.long	.LASF163
	.sleb128 -125
	.uleb128 0x16
	.long	.LASF164
	.sleb128 -4080
	.uleb128 0x16
	.long	.LASF165
	.sleb128 -103
	.uleb128 0x16
	.long	.LASF166
	.sleb128 -111
	.uleb128 0x16
	.long	.LASF167
	.sleb128 -104
	.uleb128 0x16
	.long	.LASF168
	.sleb128 -89
	.uleb128 0x16
	.long	.LASF169
	.sleb128 -17
	.uleb128 0x16
	.long	.LASF170
	.sleb128 -14
	.uleb128 0x16
	.long	.LASF171
	.sleb128 -27
	.uleb128 0x16
	.long	.LASF172
	.sleb128 -113
	.uleb128 0x16
	.long	.LASF173
	.sleb128 -4
	.uleb128 0x16
	.long	.LASF174
	.sleb128 -22
	.uleb128 0x16
	.long	.LASF175
	.sleb128 -5
	.uleb128 0x16
	.long	.LASF176
	.sleb128 -106
	.uleb128 0x16
	.long	.LASF177
	.sleb128 -21
	.uleb128 0x16
	.long	.LASF178
	.sleb128 -40
	.uleb128 0x16
	.long	.LASF179
	.sleb128 -24
	.uleb128 0x16
	.long	.LASF180
	.sleb128 -90
	.uleb128 0x16
	.long	.LASF181
	.sleb128 -36
	.uleb128 0x16
	.long	.LASF182
	.sleb128 -100
	.uleb128 0x16
	.long	.LASF183
	.sleb128 -101
	.uleb128 0x16
	.long	.LASF184
	.sleb128 -23
	.uleb128 0x16
	.long	.LASF185
	.sleb128 -105
	.uleb128 0x16
	.long	.LASF186
	.sleb128 -19
	.uleb128 0x16
	.long	.LASF187
	.sleb128 -2
	.uleb128 0x16
	.long	.LASF188
	.sleb128 -12
	.uleb128 0x16
	.long	.LASF189
	.sleb128 -64
	.uleb128 0x16
	.long	.LASF190
	.sleb128 -92
	.uleb128 0x16
	.long	.LASF191
	.sleb128 -28
	.uleb128 0x16
	.long	.LASF192
	.sleb128 -38
	.uleb128 0x16
	.long	.LASF193
	.sleb128 -107
	.uleb128 0x16
	.long	.LASF194
	.sleb128 -20
	.uleb128 0x16
	.long	.LASF195
	.sleb128 -39
	.uleb128 0x16
	.long	.LASF196
	.sleb128 -88
	.uleb128 0x16
	.long	.LASF197
	.sleb128 -95
	.uleb128 0x16
	.long	.LASF198
	.sleb128 -1
	.uleb128 0x16
	.long	.LASF199
	.sleb128 -32
	.uleb128 0x16
	.long	.LASF200
	.sleb128 -71
	.uleb128 0x16
	.long	.LASF201
	.sleb128 -93
	.uleb128 0x16
	.long	.LASF202
	.sleb128 -91
	.uleb128 0x16
	.long	.LASF203
	.sleb128 -34
	.uleb128 0x16
	.long	.LASF204
	.sleb128 -30
	.uleb128 0x16
	.long	.LASF205
	.sleb128 -108
	.uleb128 0x16
	.long	.LASF206
	.sleb128 -29
	.uleb128 0x16
	.long	.LASF207
	.sleb128 -3
	.uleb128 0x16
	.long	.LASF208
	.sleb128 -110
	.uleb128 0x16
	.long	.LASF209
	.sleb128 -26
	.uleb128 0x16
	.long	.LASF210
	.sleb128 -18
	.uleb128 0x16
	.long	.LASF211
	.sleb128 -4094
	.uleb128 0x16
	.long	.LASF212
	.sleb128 -4095
	.uleb128 0x16
	.long	.LASF213
	.sleb128 -6
	.uleb128 0x16
	.long	.LASF214
	.sleb128 -31
	.uleb128 0x16
	.long	.LASF215
	.sleb128 -112
	.uleb128 0x16
	.long	.LASF216
	.sleb128 -121
	.uleb128 0x16
	.long	.LASF217
	.sleb128 -25
	.uleb128 0x16
	.long	.LASF218
	.sleb128 -4028
	.uleb128 0x16
	.long	.LASF219
	.sleb128 -84
	.uleb128 0x16
	.long	.LASF220
	.sleb128 -4096
	.byte	0
	.uleb128 0x4
	.byte	0x8
	.byte	0x4
	.long	.LASF221
	.uleb128 0x14
	.long	.LASF222
	.byte	0x15
	.value	0x21f
	.byte	0xf
	.long	0x850
	.uleb128 0x14
	.long	.LASF223
	.byte	0x15
	.value	0x221
	.byte	0xf
	.long	0x850
	.uleb128 0x2
	.long	.LASF224
	.byte	0x16
	.byte	0x24
	.byte	0xe
	.long	0x39
	.uleb128 0x2
	.long	.LASF225
	.byte	0x16
	.byte	0x32
	.byte	0xc
	.long	0x57
	.uleb128 0x2
	.long	.LASF226
	.byte	0x16
	.byte	0x37
	.byte	0xc
	.long	0x57
	.uleb128 0x2
	.long	.LASF227
	.byte	0x16
	.byte	0x3b
	.byte	0xc
	.long	0x57
	.uleb128 0x17
	.long	.LASF228
	.byte	0x1
	.byte	0x1c
	.byte	0x12
	.long	0x8ea
	.uleb128 0x9
	.byte	0x3
	.quad	once
	.uleb128 0x17
	.long	.LASF229
	.byte	0x1
	.byte	0x1d
	.byte	0xc
	.long	0x57
	.uleb128 0x9
	.byte	0x3
	.quad	status
	.uleb128 0x18
	.long	.LASF231
	.byte	0x1
	.byte	0x56
	.byte	0x5
	.long	0x57
	.quad	.LFB96
	.quad	.LFE96-.LFB96
	.uleb128 0x1
	.byte	0x9c
	.long	0xc36
	.uleb128 0x19
	.string	"buf"
	.byte	0x1
	.byte	0x56
	.byte	0x21
	.long	0x7f
	.long	.LLST11
	.long	.LVUS11
	.uleb128 0x1a
	.long	.LASF230
	.byte	0x1
	.byte	0x56
	.byte	0x2d
	.long	0x65
	.long	.LLST12
	.long	.LVUS12
	.uleb128 0x1b
	.quad	.LVL36
	.long	0xf08
	.long	0xc0c
	.uleb128 0x1c
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x9
	.byte	0x3
	.quad	once
	.uleb128 0x1c
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x9
	.byte	0x3
	.quad	uv__random_devurandom_init
	.byte	0
	.uleb128 0x1d
	.quad	.LVL42
	.long	0xc99
	.uleb128 0x1c
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x9
	.byte	0x3
	.quad	.LC1
	.uleb128 0x1c
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x3
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x1c
	.uleb128 0x1
	.byte	0x51
	.uleb128 0x3
	.byte	0xf3
	.uleb128 0x1
	.byte	0x54
	.byte	0
	.byte	0
	.uleb128 0x1e
	.long	.LASF251
	.byte	0x1
	.byte	0x4a
	.byte	0xd
	.quad	.LFB95
	.quad	.LFE95-.LFB95
	.uleb128 0x1
	.byte	0x9c
	.long	0xc99
	.uleb128 0x1f
	.string	"c"
	.byte	0x1
	.byte	0x4b
	.byte	0x8
	.long	0x3f
	.uleb128 0x2
	.byte	0x91
	.sleb128 -25
	.uleb128 0x1b
	.quad	.LVL31
	.long	0xc99
	.long	0xc8b
	.uleb128 0x1c
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x9
	.byte	0x3
	.quad	.LC0
	.uleb128 0x1c
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x2
	.byte	0x76
	.sleb128 -9
	.uleb128 0x1c
	.uleb128 0x1
	.byte	0x51
	.uleb128 0x1
	.byte	0x31
	.byte	0
	.uleb128 0x20
	.quad	.LVL32
	.long	0xf15
	.byte	0
	.uleb128 0x18
	.long	.LASF232
	.byte	0x1
	.byte	0x20
	.byte	0x5
	.long	0x57
	.quad	.LFB94
	.quad	.LFE94-.LFB94
	.uleb128 0x1
	.byte	0x9c
	.long	0xe9b
	.uleb128 0x1a
	.long	.LASF233
	.byte	0x1
	.byte	0x20
	.byte	0x25
	.long	0x345
	.long	.LLST0
	.long	.LVUS0
	.uleb128 0x19
	.string	"buf"
	.byte	0x1
	.byte	0x20
	.byte	0x31
	.long	0x7f
	.long	.LLST1
	.long	.LVUS1
	.uleb128 0x1a
	.long	.LASF230
	.byte	0x1
	.byte	0x20
	.byte	0x3d
	.long	0x65
	.long	.LLST2
	.long	.LVUS2
	.uleb128 0x1f
	.string	"s"
	.byte	0x1
	.byte	0x21
	.byte	0xf
	.long	0x42c
	.uleb128 0x3
	.byte	0x91
	.sleb128 -224
	.uleb128 0x21
	.string	"pos"
	.byte	0x1
	.byte	0x22
	.byte	0xa
	.long	0x65
	.long	.LLST3
	.long	.LVUS3
	.uleb128 0x21
	.string	"n"
	.byte	0x1
	.byte	0x23
	.byte	0xb
	.long	0x350
	.long	.LLST4
	.long	.LVUS4
	.uleb128 0x21
	.string	"fd"
	.byte	0x1
	.byte	0x24
	.byte	0x7
	.long	0x57
	.long	.LLST5
	.long	.LVUS5
	.uleb128 0x22
	.long	0xed1
	.quad	.LBI6
	.byte	.LVU15
	.quad	.LBB6
	.quad	.LBE6-.LBB6
	.byte	0x1
	.byte	0x2b
	.byte	0x7
	.long	0xd9e
	.uleb128 0x23
	.long	0xef4
	.long	.LLST6
	.long	.LVUS6
	.uleb128 0x23
	.long	0xee7
	.long	.LLST7
	.long	.LVUS7
	.uleb128 0x24
	.quad	.LVL6
	.long	0xf1e
	.uleb128 0x1c
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x1
	.byte	0x31
	.uleb128 0x1c
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x2
	.byte	0x7c
	.sleb128 0
	.uleb128 0x1c
	.uleb128 0x1
	.byte	0x51
	.uleb128 0x3
	.byte	0x76
	.sleb128 -208
	.byte	0
	.byte	0
	.uleb128 0x22
	.long	0xe9b
	.quad	.LBI8
	.byte	.LVU32
	.quad	.LBB8
	.quad	.LBE8-.LBB8
	.byte	0x1
	.byte	0x37
	.byte	0xb
	.long	0xe0b
	.uleb128 0x23
	.long	0xec4
	.long	.LLST8
	.long	.LVUS8
	.uleb128 0x23
	.long	0xeb8
	.long	.LLST9
	.long	.LVUS9
	.uleb128 0x23
	.long	0xeac
	.long	.LLST10
	.long	.LVUS10
	.uleb128 0x24
	.quad	.LVL12
	.long	0xf2b
	.uleb128 0x1c
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x2
	.byte	0x7c
	.sleb128 0
	.uleb128 0x1c
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x2
	.byte	0x73
	.sleb128 0
	.uleb128 0x1c
	.uleb128 0x1
	.byte	0x51
	.uleb128 0x2
	.byte	0x7d
	.sleb128 0
	.byte	0
	.byte	0
	.uleb128 0x1b
	.quad	.LVL2
	.long	0xf37
	.long	0xe29
	.uleb128 0x1c
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x3
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x1c
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x1
	.byte	0x30
	.byte	0
	.uleb128 0x20
	.quad	.LVL10
	.long	0xf43
	.uleb128 0x20
	.quad	.LVL16
	.long	0xf4f
	.uleb128 0x1b
	.quad	.LVL19
	.long	0xf4f
	.long	0xe5b
	.uleb128 0x1c
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x2
	.byte	0x7c
	.sleb128 0
	.byte	0
	.uleb128 0x20
	.quad	.LVL20
	.long	0xf43
	.uleb128 0x1b
	.quad	.LVL24
	.long	0xf4f
	.long	0xe80
	.uleb128 0x1c
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x2
	.byte	0x7c
	.sleb128 0
	.byte	0
	.uleb128 0x20
	.quad	.LVL28
	.long	0xf4f
	.uleb128 0x20
	.quad	.LVL30
	.long	0xf15
	.byte	0
	.uleb128 0x25
	.long	.LASF237
	.byte	0x3
	.byte	0x22
	.byte	0x1
	.long	0x350
	.byte	0x3
	.long	0xed1
	.uleb128 0x26
	.long	.LASF234
	.byte	0x3
	.byte	0x22
	.byte	0xb
	.long	0x57
	.uleb128 0x26
	.long	.LASF235
	.byte	0x3
	.byte	0x22
	.byte	0x17
	.long	0x7f
	.uleb128 0x26
	.long	.LASF236
	.byte	0x3
	.byte	0x22
	.byte	0x25
	.long	0x65
	.byte	0
	.uleb128 0x27
	.long	.LASF238
	.byte	0x2
	.value	0x1d3
	.byte	0x2a
	.long	.LASF252
	.long	0x57
	.byte	0x3
	.long	0xf02
	.uleb128 0x28
	.long	.LASF234
	.byte	0x2
	.value	0x1d3
	.byte	0x35
	.long	0x57
	.uleb128 0x28
	.long	.LASF239
	.byte	0x2
	.value	0x1d3
	.byte	0x48
	.long	0xf02
	.byte	0
	.uleb128 0x3
	.byte	0x8
	.long	0x42c
	.uleb128 0x29
	.long	.LASF240
	.long	.LASF240
	.byte	0x17
	.value	0x6bc
	.byte	0x2d
	.uleb128 0x2a
	.long	.LASF253
	.long	.LASF253
	.uleb128 0x29
	.long	.LASF241
	.long	.LASF242
	.byte	0x2
	.value	0x196
	.byte	0xc
	.uleb128 0x2b
	.long	.LASF237
	.long	.LASF243
	.byte	0x3
	.byte	0x19
	.byte	0x10
	.uleb128 0x2b
	.long	.LASF244
	.long	.LASF244
	.byte	0x18
	.byte	0xe6
	.byte	0x5
	.uleb128 0x2b
	.long	.LASF245
	.long	.LASF245
	.byte	0x4
	.byte	0x25
	.byte	0xd
	.uleb128 0x2b
	.long	.LASF246
	.long	.LASF246
	.byte	0x18
	.byte	0xbe
	.byte	0x5
	.byte	0
	.section	.debug_abbrev,"",@progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1b
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x7
	.uleb128 0x10
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x13
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x13
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3c
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x21
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x37
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0x17
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0x4
	.byte	0x1
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x16
	.uleb128 0x28
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1c
	.uleb128 0xd
	.byte	0
	.byte	0
	.uleb128 0x17
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x18
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x7
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2117
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x19
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x17
	.uleb128 0x2137
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x1a
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x17
	.uleb128 0x2137
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x1b
	.uleb128 0x4109
	.byte	0x1
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1c
	.uleb128 0x410a
	.byte	0
	.uleb128 0x2
	.uleb128 0x18
	.uleb128 0x2111
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x1d
	.uleb128 0x4109
	.byte	0x1
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x2115
	.uleb128 0x19
	.uleb128 0x31
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1e
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x7
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2117
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1f
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x20
	.uleb128 0x4109
	.byte	0
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x31
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x21
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x17
	.uleb128 0x2137
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x22
	.uleb128 0x1d
	.byte	0x1
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x2138
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x7
	.uleb128 0x58
	.uleb128 0xb
	.uleb128 0x59
	.uleb128 0xb
	.uleb128 0x57
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x23
	.uleb128 0x5
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x17
	.uleb128 0x2137
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x24
	.uleb128 0x4109
	.byte	0x1
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x31
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x25
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x20
	.uleb128 0xb
	.uleb128 0x34
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x26
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x27
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x6e
	.uleb128 0xe
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x20
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x28
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x29
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.uleb128 0x6e
	.uleb128 0xe
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x2a
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.uleb128 0x6e
	.uleb128 0xe
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x2b
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.uleb128 0x6e
	.uleb128 0xe
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",@progbits
.Ldebug_loc0:
.LVUS11:
	.uleb128 0
	.uleb128 .LVU86
	.uleb128 .LVU86
	.uleb128 .LVU91
	.uleb128 .LVU91
	.uleb128 .LVU93
	.uleb128 .LVU93
	.uleb128 .LVU96
	.uleb128 .LVU96
	.uleb128 .LVU99
	.uleb128 .LVU99
	.uleb128 0
.LLST11:
	.quad	.LVL33-.Ltext0
	.quad	.LVL35-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	.LVL35-.Ltext0
	.quad	.LVL37-.Ltext0
	.value	0x1
	.byte	0x5c
	.quad	.LVL37-.Ltext0
	.quad	.LVL39-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.byte	0x9f
	.quad	.LVL39-.Ltext0
	.quad	.LVL40-.Ltext0
	.value	0x1
	.byte	0x5c
	.quad	.LVL40-.Ltext0
	.quad	.LVL42-1-.Ltext0
	.value	0x1
	.byte	0x54
	.quad	.LVL42-1-.Ltext0
	.quad	.LFE96-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS12:
	.uleb128 0
	.uleb128 .LVU83
	.uleb128 .LVU83
	.uleb128 .LVU92
	.uleb128 .LVU92
	.uleb128 .LVU93
	.uleb128 .LVU93
	.uleb128 .LVU97
	.uleb128 .LVU97
	.uleb128 .LVU99
	.uleb128 .LVU99
	.uleb128 0
.LLST12:
	.quad	.LVL33-.Ltext0
	.quad	.LVL34-.Ltext0
	.value	0x1
	.byte	0x54
	.quad	.LVL34-.Ltext0
	.quad	.LVL38-.Ltext0
	.value	0x1
	.byte	0x5d
	.quad	.LVL38-.Ltext0
	.quad	.LVL39-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x54
	.byte	0x9f
	.quad	.LVL39-.Ltext0
	.quad	.LVL41-.Ltext0
	.value	0x1
	.byte	0x5d
	.quad	.LVL41-.Ltext0
	.quad	.LVL42-1-.Ltext0
	.value	0x1
	.byte	0x51
	.quad	.LVL42-1-.Ltext0
	.quad	.LFE96-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x54
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS0:
	.uleb128 0
	.uleb128 .LVU11
	.uleb128 .LVU11
	.uleb128 0
.LLST0:
	.quad	.LVL0-.Ltext0
	.quad	.LVL2-1-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	.LVL2-1-.Ltext0
	.quad	.LFE94-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS1:
	.uleb128 0
	.uleb128 .LVU4
	.uleb128 .LVU4
	.uleb128 .LVU50
	.uleb128 .LVU50
	.uleb128 .LVU52
	.uleb128 .LVU52
	.uleb128 .LVU58
	.uleb128 .LVU58
	.uleb128 .LVU63
	.uleb128 .LVU63
	.uleb128 .LVU68
	.uleb128 .LVU68
	.uleb128 0
.LLST1:
	.quad	.LVL0-.Ltext0
	.quad	.LVL1-.Ltext0
	.value	0x1
	.byte	0x54
	.quad	.LVL1-.Ltext0
	.quad	.LVL16-.Ltext0
	.value	0x3
	.byte	0x76
	.sleb128 -216
	.quad	.LVL16-.Ltext0
	.quad	.LVL18-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x54
	.byte	0x9f
	.quad	.LVL18-.Ltext0
	.quad	.LVL23-.Ltext0
	.value	0x3
	.byte	0x76
	.sleb128 -216
	.quad	.LVL23-.Ltext0
	.quad	.LVL26-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x54
	.byte	0x9f
	.quad	.LVL26-.Ltext0
	.quad	.LVL29-.Ltext0
	.value	0x3
	.byte	0x76
	.sleb128 -216
	.quad	.LVL29-.Ltext0
	.quad	.LFE94-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x54
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS2:
	.uleb128 0
	.uleb128 .LVU11
	.uleb128 .LVU11
	.uleb128 .LVU51
	.uleb128 .LVU51
	.uleb128 .LVU52
	.uleb128 .LVU52
	.uleb128 0
.LLST2:
	.quad	.LVL0-.Ltext0
	.quad	.LVL2-1-.Ltext0
	.value	0x1
	.byte	0x51
	.quad	.LVL2-1-.Ltext0
	.quad	.LVL17-.Ltext0
	.value	0x1
	.byte	0x5f
	.quad	.LVL17-.Ltext0
	.quad	.LVL18-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x51
	.byte	0x9f
	.quad	.LVL18-.Ltext0
	.quad	.LFE94-.Ltext0
	.value	0x1
	.byte	0x5f
	.quad	0
	.quad	0
.LVUS3:
	.uleb128 .LVU24
	.uleb128 .LVU27
	.uleb128 .LVU27
	.uleb128 .LVU46
	.uleb128 .LVU57
	.uleb128 .LVU63
.LLST3:
	.quad	.LVL7-.Ltext0
	.quad	.LVL8-.Ltext0
	.value	0x2
	.byte	0x30
	.byte	0x9f
	.quad	.LVL8-.Ltext0
	.quad	.LVL14-.Ltext0
	.value	0x1
	.byte	0x5e
	.quad	.LVL22-.Ltext0
	.quad	.LVL26-.Ltext0
	.value	0x1
	.byte	0x5e
	.quad	0
	.quad	0
.LVUS4:
	.uleb128 .LVU28
	.uleb128 .LVU29
	.uleb128 .LVU36
	.uleb128 .LVU46
.LLST4:
	.quad	.LVL9-.Ltext0
	.quad	.LVL10-1-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	.LVL12-.Ltext0
	.quad	.LVL14-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	0
	.quad	0
.LVUS5:
	.uleb128 .LVU12
	.uleb128 .LVU19
	.uleb128 .LVU19
	.uleb128 .LVU48
	.uleb128 .LVU48
	.uleb128 .LVU49
	.uleb128 .LVU52
	.uleb128 .LVU56
	.uleb128 .LVU57
	.uleb128 .LVU62
	.uleb128 .LVU63
	.uleb128 .LVU65
	.uleb128 .LVU65
	.uleb128 .LVU66
.LLST5:
	.quad	.LVL3-.Ltext0
	.quad	.LVL6-1-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	.LVL6-1-.Ltext0
	.quad	.LVL15-.Ltext0
	.value	0x1
	.byte	0x5c
	.quad	.LVL15-.Ltext0
	.quad	.LVL16-1-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	.LVL18-.Ltext0
	.quad	.LVL21-.Ltext0
	.value	0x1
	.byte	0x5c
	.quad	.LVL22-.Ltext0
	.quad	.LVL25-.Ltext0
	.value	0x1
	.byte	0x5c
	.quad	.LVL26-.Ltext0
	.quad	.LVL27-.Ltext0
	.value	0x1
	.byte	0x5c
	.quad	.LVL27-.Ltext0
	.quad	.LVL28-1-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	0
	.quad	0
.LVUS6:
	.uleb128 .LVU15
	.uleb128 .LVU18
	.uleb128 .LVU18
	.uleb128 .LVU19
	.uleb128 .LVU19
	.uleb128 .LVU19
.LLST6:
	.quad	.LVL4-.Ltext0
	.quad	.LVL5-.Ltext0
	.value	0x4
	.byte	0x76
	.sleb128 -208
	.byte	0x9f
	.quad	.LVL5-.Ltext0
	.quad	.LVL6-1-.Ltext0
	.value	0x1
	.byte	0x51
	.quad	.LVL6-1-.Ltext0
	.quad	.LVL6-.Ltext0
	.value	0x4
	.byte	0x76
	.sleb128 -208
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS7:
	.uleb128 .LVU15
	.uleb128 .LVU19
	.uleb128 .LVU19
	.uleb128 .LVU19
.LLST7:
	.quad	.LVL4-.Ltext0
	.quad	.LVL6-1-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	.LVL6-1-.Ltext0
	.quad	.LVL6-.Ltext0
	.value	0x1
	.byte	0x5c
	.quad	0
	.quad	0
.LVUS8:
	.uleb128 .LVU32
	.uleb128 .LVU36
.LLST8:
	.quad	.LVL11-.Ltext0
	.quad	.LVL12-.Ltext0
	.value	0x6
	.byte	0x7f
	.sleb128 0
	.byte	0x7e
	.sleb128 0
	.byte	0x1c
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS9:
	.uleb128 .LVU32
	.uleb128 .LVU36
.LLST9:
	.quad	.LVL11-.Ltext0
	.quad	.LVL12-.Ltext0
	.value	0x8
	.byte	0x76
	.sleb128 -216
	.byte	0x6
	.byte	0x7e
	.sleb128 0
	.byte	0x22
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS10:
	.uleb128 .LVU32
	.uleb128 .LVU36
.LLST10:
	.quad	.LVL11-.Ltext0
	.quad	.LVL12-.Ltext0
	.value	0x1
	.byte	0x5c
	.quad	0
	.quad	0
	.section	.debug_aranges,"",@progbits
	.long	0x2c
	.value	0x2
	.long	.Ldebug_info0
	.byte	0x8
	.byte	0
	.value	0
	.value	0
	.quad	.Ltext0
	.quad	.Letext0-.Ltext0
	.quad	0
	.quad	0
	.section	.debug_line,"",@progbits
.Ldebug_line0:
	.section	.debug_str,"MS",@progbits,1
.LASF99:
	.string	"sockaddr_ax25"
.LASF164:
	.string	"UV_ECHARSET"
.LASF110:
	.string	"sin6_flowinfo"
.LASF46:
	.string	"_shortbuf"
.LASF250:
	.string	"_IO_lock_t"
.LASF203:
	.string	"UV_ERANGE"
.LASF1:
	.string	"program_invocation_short_name"
.LASF63:
	.string	"stderr"
.LASF35:
	.string	"_IO_buf_end"
.LASF97:
	.string	"sa_data"
.LASF227:
	.string	"optopt"
.LASF204:
	.string	"UV_EROFS"
.LASF166:
	.string	"UV_ECONNREFUSED"
.LASF162:
	.string	"UV_EBUSY"
.LASF95:
	.string	"sockaddr"
.LASF201:
	.string	"UV_EPROTONOSUPPORT"
.LASF112:
	.string	"sin6_scope_id"
.LASF33:
	.string	"_IO_write_end"
.LASF5:
	.string	"unsigned int"
.LASF116:
	.string	"sockaddr_ns"
.LASF51:
	.string	"_freeres_list"
.LASF138:
	.string	"getdate_err"
.LASF88:
	.string	"st_blksize"
.LASF210:
	.string	"UV_EXDEV"
.LASF27:
	.string	"_flags"
.LASF44:
	.string	"_cur_column"
.LASF143:
	.string	"UV_EADDRNOTAVAIL"
.LASF39:
	.string	"_markers"
.LASF19:
	.string	"__nlink_t"
.LASF66:
	.string	"_sys_nerr"
.LASF130:
	.string	"_sys_siglist"
.LASF158:
	.string	"UV_EAI_SERVICE"
.LASF161:
	.string	"UV_EBADF"
.LASF218:
	.string	"UV_EFTYPE"
.LASF180:
	.string	"UV_EMSGSIZE"
.LASF171:
	.string	"UV_EFBIG"
.LASF199:
	.string	"UV_EPIPE"
.LASF127:
	.string	"__in6_u"
.LASF245:
	.string	"__errno_location"
.LASF114:
	.string	"sockaddr_ipx"
.LASF134:
	.string	"__timezone"
.LASF17:
	.string	"__ino_t"
.LASF178:
	.string	"UV_ELOOP"
.LASF182:
	.string	"UV_ENETDOWN"
.LASF70:
	.string	"uint32_t"
.LASF211:
	.string	"UV_UNKNOWN"
.LASF119:
	.string	"in_addr_t"
.LASF62:
	.string	"stdout"
.LASF243:
	.string	"__read_alias"
.LASF38:
	.string	"_IO_save_end"
.LASF226:
	.string	"opterr"
.LASF181:
	.string	"UV_ENAMETOOLONG"
.LASF98:
	.string	"sockaddr_at"
.LASF185:
	.string	"UV_ENOBUFS"
.LASF75:
	.string	"long long unsigned int"
.LASF238:
	.string	"fstat"
.LASF123:
	.string	"__u6_addr8"
.LASF89:
	.string	"st_blocks"
.LASF149:
	.string	"UV_EAI_BADHINTS"
.LASF151:
	.string	"UV_EAI_FAIL"
.LASF148:
	.string	"UV_EAI_BADFLAGS"
.LASF93:
	.string	"__glibc_reserved"
.LASF142:
	.string	"UV_EADDRINUSE"
.LASF103:
	.string	"sin_family"
.LASF12:
	.string	"__uint16_t"
.LASF65:
	.string	"sys_errlist"
.LASF37:
	.string	"_IO_backup_base"
.LASF48:
	.string	"_offset"
.LASF122:
	.string	"in_port_t"
.LASF64:
	.string	"sys_nerr"
.LASF165:
	.string	"UV_ECONNABORTED"
.LASF155:
	.string	"UV_EAI_NONAME"
.LASF109:
	.string	"sin6_port"
.LASF41:
	.string	"_fileno"
.LASF78:
	.string	"stat"
.LASF235:
	.string	"__buf"
.LASF106:
	.string	"sin_zero"
.LASF147:
	.string	"UV_EAI_AGAIN"
.LASF121:
	.string	"s_addr"
.LASF74:
	.string	"tv_nsec"
.LASF9:
	.string	"size_t"
.LASF94:
	.string	"sa_family_t"
.LASF18:
	.string	"__mode_t"
.LASF202:
	.string	"UV_EPROTOTYPE"
.LASF220:
	.string	"UV_ERRNO_MAX"
.LASF170:
	.string	"UV_EFAULT"
.LASF205:
	.string	"UV_ESHUTDOWN"
.LASF30:
	.string	"_IO_read_base"
.LASF84:
	.string	"st_gid"
.LASF113:
	.string	"sockaddr_inarp"
.LASF240:
	.string	"uv_once"
.LASF61:
	.string	"stdin"
.LASF244:
	.string	"uv__open_cloexec"
.LASF82:
	.string	"st_mode"
.LASF111:
	.string	"sin6_addr"
.LASF115:
	.string	"sockaddr_iso"
.LASF81:
	.string	"st_nlink"
.LASF233:
	.string	"path"
.LASF212:
	.string	"UV_EOF"
.LASF219:
	.string	"UV_EILSEQ"
.LASF173:
	.string	"UV_EINTR"
.LASF189:
	.string	"UV_ENONET"
.LASF129:
	.string	"in6addr_loopback"
.LASF145:
	.string	"UV_EAGAIN"
.LASF195:
	.string	"UV_ENOTEMPTY"
.LASF249:
	.string	"/workspace/nodejsbuild/node-v12.18.4/out"
.LASF187:
	.string	"UV_ENOENT"
.LASF72:
	.string	"timespec"
.LASF2:
	.string	"char"
.LASF100:
	.string	"sockaddr_dl"
.LASF209:
	.string	"UV_ETXTBSY"
.LASF133:
	.string	"__daylight"
.LASF186:
	.string	"UV_ENODEV"
.LASF135:
	.string	"tzname"
.LASF57:
	.string	"_IO_marker"
.LASF223:
	.string	"environ"
.LASF28:
	.string	"_IO_read_ptr"
.LASF230:
	.string	"buflen"
.LASF246:
	.string	"uv__close"
.LASF146:
	.string	"UV_EAI_ADDRFAMILY"
.LASF60:
	.string	"ssize_t"
.LASF193:
	.string	"UV_ENOTCONN"
.LASF68:
	.string	"uint8_t"
.LASF229:
	.string	"status"
.LASF216:
	.string	"UV_EREMOTEIO"
.LASF163:
	.string	"UV_ECANCELED"
.LASF153:
	.string	"UV_EAI_MEMORY"
.LASF131:
	.string	"sys_siglist"
.LASF191:
	.string	"UV_ENOSPC"
.LASF80:
	.string	"st_ino"
.LASF208:
	.string	"UV_ETIMEDOUT"
.LASF194:
	.string	"UV_ENOTDIR"
.LASF31:
	.string	"_IO_write_base"
.LASF77:
	.string	"long long int"
.LASF36:
	.string	"_IO_save_base"
.LASF190:
	.string	"UV_ENOPROTOOPT"
.LASF104:
	.string	"sin_port"
.LASF213:
	.string	"UV_ENXIO"
.LASF101:
	.string	"sockaddr_eon"
.LASF125:
	.string	"__u6_addr32"
.LASF225:
	.string	"optind"
.LASF117:
	.string	"sockaddr_un"
.LASF26:
	.string	"__syscall_slong_t"
.LASF156:
	.string	"UV_EAI_OVERFLOW"
.LASF252:
	.string	"fstat64"
.LASF52:
	.string	"_freeres_buf"
.LASF128:
	.string	"in6addr_any"
.LASF176:
	.string	"UV_EISCONN"
.LASF105:
	.string	"sin_addr"
.LASF85:
	.string	"__pad0"
.LASF53:
	.string	"__pad5"
.LASF175:
	.string	"UV_EIO"
.LASF4:
	.string	"long unsigned int"
.LASF196:
	.string	"UV_ENOTSOCK"
.LASF214:
	.string	"UV_EMLINK"
.LASF45:
	.string	"_vtable_offset"
.LASF248:
	.string	"../deps/uv/src/unix/random-devurandom.c"
.LASF179:
	.string	"UV_EMFILE"
.LASF207:
	.string	"UV_ESRCH"
.LASF0:
	.string	"program_invocation_name"
.LASF54:
	.string	"_mode"
.LASF224:
	.string	"optarg"
.LASF160:
	.string	"UV_EALREADY"
.LASF7:
	.string	"short unsigned int"
.LASF140:
	.string	"UV_E2BIG"
.LASF251:
	.string	"uv__random_devurandom_init"
.LASF16:
	.string	"__gid_t"
.LASF69:
	.string	"uint16_t"
.LASF141:
	.string	"UV_EACCES"
.LASF154:
	.string	"UV_EAI_NODATA"
.LASF13:
	.string	"__uint32_t"
.LASF137:
	.string	"timezone"
.LASF157:
	.string	"UV_EAI_PROTOCOL"
.LASF159:
	.string	"UV_EAI_SOCKTYPE"
.LASF169:
	.string	"UV_EEXIST"
.LASF184:
	.string	"UV_ENFILE"
.LASF124:
	.string	"__u6_addr16"
.LASF29:
	.string	"_IO_read_end"
.LASF242:
	.string	"__fxstat"
.LASF108:
	.string	"sin6_family"
.LASF231:
	.string	"uv__random_devurandom"
.LASF11:
	.string	"short int"
.LASF91:
	.string	"st_mtim"
.LASF192:
	.string	"UV_ENOSYS"
.LASF168:
	.string	"UV_EDESTADDRREQ"
.LASF3:
	.string	"long int"
.LASF253:
	.string	"__stack_chk_fail"
.LASF183:
	.string	"UV_ENETUNREACH"
.LASF14:
	.string	"__dev_t"
.LASF172:
	.string	"UV_EHOSTUNREACH"
.LASF59:
	.string	"_IO_wide_data"
.LASF198:
	.string	"UV_EPERM"
.LASF241:
	.string	"__fxstat64"
.LASF222:
	.string	"__environ"
.LASF177:
	.string	"UV_EISDIR"
.LASF23:
	.string	"__blksize_t"
.LASF25:
	.string	"__ssize_t"
.LASF102:
	.string	"sockaddr_in"
.LASF15:
	.string	"__uid_t"
.LASF10:
	.string	"__uint8_t"
.LASF90:
	.string	"st_atim"
.LASF217:
	.string	"UV_ENOTTY"
.LASF237:
	.string	"read"
.LASF34:
	.string	"_IO_buf_base"
.LASF232:
	.string	"uv__random_readpath"
.LASF50:
	.string	"_wide_data"
.LASF47:
	.string	"_lock"
.LASF73:
	.string	"tv_sec"
.LASF126:
	.string	"in6_addr"
.LASF58:
	.string	"_IO_codecvt"
.LASF43:
	.string	"_old_offset"
.LASF200:
	.string	"UV_EPROTO"
.LASF71:
	.string	"_IO_FILE"
.LASF197:
	.string	"UV_ENOTSUP"
.LASF120:
	.string	"in_addr"
.LASF76:
	.string	"pthread_once_t"
.LASF152:
	.string	"UV_EAI_FAMILY"
.LASF6:
	.string	"unsigned char"
.LASF228:
	.string	"once"
.LASF144:
	.string	"UV_EAFNOSUPPORT"
.LASF139:
	.string	"uv_once_t"
.LASF132:
	.string	"__tzname"
.LASF32:
	.string	"_IO_write_ptr"
.LASF234:
	.string	"__fd"
.LASF22:
	.string	"__time_t"
.LASF87:
	.string	"st_size"
.LASF49:
	.string	"_codecvt"
.LASF136:
	.string	"daylight"
.LASF83:
	.string	"st_uid"
.LASF20:
	.string	"__off_t"
.LASF92:
	.string	"st_ctim"
.LASF79:
	.string	"st_dev"
.LASF8:
	.string	"signed char"
.LASF96:
	.string	"sa_family"
.LASF167:
	.string	"UV_ECONNRESET"
.LASF150:
	.string	"UV_EAI_CANCELED"
.LASF67:
	.string	"_sys_errlist"
.LASF206:
	.string	"UV_ESPIPE"
.LASF221:
	.string	"double"
.LASF24:
	.string	"__blkcnt_t"
.LASF239:
	.string	"__statbuf"
.LASF40:
	.string	"_chain"
.LASF236:
	.string	"__nbytes"
.LASF86:
	.string	"st_rdev"
.LASF56:
	.string	"FILE"
.LASF42:
	.string	"_flags2"
.LASF247:
	.string	"GNU C89 9.3.0 -m64 -mtune=generic -march=x86-64 -g -O3 -std=gnu90 -fvisibility=hidden -fno-omit-frame-pointer -fasynchronous-unwind-tables -fstack-protector-strong -fstack-clash-protection -fcf-protection"
.LASF107:
	.string	"sockaddr_in6"
.LASF188:
	.string	"UV_ENOMEM"
.LASF215:
	.string	"UV_EHOSTDOWN"
.LASF21:
	.string	"__off64_t"
.LASF55:
	.string	"_unused2"
.LASF118:
	.string	"sockaddr_x25"
.LASF174:
	.string	"UV_EINVAL"
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
