	.file	"version.c"
	.text
.Ltext0:
	.p2align 4
	.globl	uv_version
	.type	uv_version, @function
uv_version:
.LFB54:
	.file 1 "../deps/uv/src/version.c"
	.loc 1 38 31 view -0
	.cfi_startproc
	endbr64
	.loc 1 39 3 view .LVU1
	.loc 1 40 1 is_stmt 0 view .LVU2
	movl	$75264, %eax
	ret
	.cfi_endproc
.LFE54:
	.size	uv_version, .-uv_version
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"1.38.0"
	.text
	.p2align 4
	.globl	uv_version_string
	.type	uv_version_string, @function
uv_version_string:
.LFB55:
	.loc 1 43 37 is_stmt 1 view -0
	.cfi_startproc
	endbr64
	.loc 1 44 3 view .LVU4
	.loc 1 45 1 is_stmt 0 view .LVU5
	leaq	.LC0(%rip), %rax
	ret
	.cfi_endproc
.LFE55:
	.size	uv_version_string, .-uv_version_string
.Letext0:
	.file 2 "/usr/include/errno.h"
	.file 3 "/usr/lib/gcc/x86_64-linux-gnu/9/include/stddef.h"
	.file 4 "/usr/include/x86_64-linux-gnu/bits/types.h"
	.file 5 "/usr/include/x86_64-linux-gnu/bits/types/struct_FILE.h"
	.file 6 "/usr/include/x86_64-linux-gnu/bits/types/FILE.h"
	.file 7 "/usr/include/stdio.h"
	.file 8 "/usr/include/x86_64-linux-gnu/bits/sys_errlist.h"
	.file 9 "/usr/include/x86_64-linux-gnu/bits/stdint-uintn.h"
	.file 10 "/usr/include/x86_64-linux-gnu/bits/sockaddr.h"
	.file 11 "/usr/include/x86_64-linux-gnu/bits/socket.h"
	.file 12 "/usr/include/netinet/in.h"
	.file 13 "/usr/include/signal.h"
	.file 14 "/usr/include/time.h"
	.section	.debug_info,"",@progbits
.Ldebug_info0:
	.long	0x77f
	.value	0x4
	.long	.Ldebug_abbrev0
	.byte	0x8
	.uleb128 0x1
	.long	.LASF110
	.byte	0x1
	.long	.LASF111
	.long	.LASF112
	.quad	.Ltext0
	.quad	.Letext0-.Ltext0
	.long	.Ldebug_line0
	.uleb128 0x2
	.long	.LASF0
	.byte	0x2
	.byte	0x2d
	.byte	0xe
	.long	0x39
	.uleb128 0x3
	.byte	0x8
	.long	0x3f
	.uleb128 0x4
	.byte	0x1
	.byte	0x6
	.long	.LASF2
	.uleb128 0x5
	.long	0x3f
	.uleb128 0x2
	.long	.LASF1
	.byte	0x2
	.byte	0x2e
	.byte	0xe
	.long	0x39
	.uleb128 0x6
	.byte	0x4
	.byte	0x5
	.string	"int"
	.uleb128 0x4
	.byte	0x8
	.byte	0x5
	.long	.LASF3
	.uleb128 0x7
	.long	.LASF9
	.byte	0x3
	.byte	0xd1
	.byte	0x1b
	.long	0x71
	.uleb128 0x4
	.byte	0x8
	.byte	0x7
	.long	.LASF4
	.uleb128 0x4
	.byte	0x4
	.byte	0x7
	.long	.LASF5
	.uleb128 0x8
	.byte	0x8
	.uleb128 0x4
	.byte	0x1
	.byte	0x8
	.long	.LASF6
	.uleb128 0x4
	.byte	0x2
	.byte	0x7
	.long	.LASF7
	.uleb128 0x4
	.byte	0x1
	.byte	0x6
	.long	.LASF8
	.uleb128 0x7
	.long	.LASF10
	.byte	0x4
	.byte	0x26
	.byte	0x17
	.long	0x81
	.uleb128 0x4
	.byte	0x2
	.byte	0x5
	.long	.LASF11
	.uleb128 0x7
	.long	.LASF12
	.byte	0x4
	.byte	0x28
	.byte	0x1c
	.long	0x88
	.uleb128 0x7
	.long	.LASF13
	.byte	0x4
	.byte	0x2a
	.byte	0x16
	.long	0x78
	.uleb128 0x7
	.long	.LASF14
	.byte	0x4
	.byte	0x98
	.byte	0x12
	.long	0x5e
	.uleb128 0x7
	.long	.LASF15
	.byte	0x4
	.byte	0x99
	.byte	0x12
	.long	0x5e
	.uleb128 0x9
	.long	.LASF62
	.byte	0xd8
	.byte	0x5
	.byte	0x31
	.byte	0x8
	.long	0x260
	.uleb128 0xa
	.long	.LASF16
	.byte	0x5
	.byte	0x33
	.byte	0x7
	.long	0x57
	.byte	0
	.uleb128 0xa
	.long	.LASF17
	.byte	0x5
	.byte	0x36
	.byte	0x9
	.long	0x39
	.byte	0x8
	.uleb128 0xa
	.long	.LASF18
	.byte	0x5
	.byte	0x37
	.byte	0x9
	.long	0x39
	.byte	0x10
	.uleb128 0xa
	.long	.LASF19
	.byte	0x5
	.byte	0x38
	.byte	0x9
	.long	0x39
	.byte	0x18
	.uleb128 0xa
	.long	.LASF20
	.byte	0x5
	.byte	0x39
	.byte	0x9
	.long	0x39
	.byte	0x20
	.uleb128 0xa
	.long	.LASF21
	.byte	0x5
	.byte	0x3a
	.byte	0x9
	.long	0x39
	.byte	0x28
	.uleb128 0xa
	.long	.LASF22
	.byte	0x5
	.byte	0x3b
	.byte	0x9
	.long	0x39
	.byte	0x30
	.uleb128 0xa
	.long	.LASF23
	.byte	0x5
	.byte	0x3c
	.byte	0x9
	.long	0x39
	.byte	0x38
	.uleb128 0xa
	.long	.LASF24
	.byte	0x5
	.byte	0x3d
	.byte	0x9
	.long	0x39
	.byte	0x40
	.uleb128 0xa
	.long	.LASF25
	.byte	0x5
	.byte	0x40
	.byte	0x9
	.long	0x39
	.byte	0x48
	.uleb128 0xa
	.long	.LASF26
	.byte	0x5
	.byte	0x41
	.byte	0x9
	.long	0x39
	.byte	0x50
	.uleb128 0xa
	.long	.LASF27
	.byte	0x5
	.byte	0x42
	.byte	0x9
	.long	0x39
	.byte	0x58
	.uleb128 0xa
	.long	.LASF28
	.byte	0x5
	.byte	0x44
	.byte	0x16
	.long	0x279
	.byte	0x60
	.uleb128 0xa
	.long	.LASF29
	.byte	0x5
	.byte	0x46
	.byte	0x14
	.long	0x27f
	.byte	0x68
	.uleb128 0xa
	.long	.LASF30
	.byte	0x5
	.byte	0x48
	.byte	0x7
	.long	0x57
	.byte	0x70
	.uleb128 0xa
	.long	.LASF31
	.byte	0x5
	.byte	0x49
	.byte	0x7
	.long	0x57
	.byte	0x74
	.uleb128 0xa
	.long	.LASF32
	.byte	0x5
	.byte	0x4a
	.byte	0xb
	.long	0xc1
	.byte	0x78
	.uleb128 0xa
	.long	.LASF33
	.byte	0x5
	.byte	0x4d
	.byte	0x12
	.long	0x88
	.byte	0x80
	.uleb128 0xa
	.long	.LASF34
	.byte	0x5
	.byte	0x4e
	.byte	0xf
	.long	0x8f
	.byte	0x82
	.uleb128 0xa
	.long	.LASF35
	.byte	0x5
	.byte	0x4f
	.byte	0x8
	.long	0x285
	.byte	0x83
	.uleb128 0xa
	.long	.LASF36
	.byte	0x5
	.byte	0x51
	.byte	0xf
	.long	0x295
	.byte	0x88
	.uleb128 0xa
	.long	.LASF37
	.byte	0x5
	.byte	0x59
	.byte	0xd
	.long	0xcd
	.byte	0x90
	.uleb128 0xa
	.long	.LASF38
	.byte	0x5
	.byte	0x5b
	.byte	0x17
	.long	0x2a0
	.byte	0x98
	.uleb128 0xa
	.long	.LASF39
	.byte	0x5
	.byte	0x5c
	.byte	0x19
	.long	0x2ab
	.byte	0xa0
	.uleb128 0xa
	.long	.LASF40
	.byte	0x5
	.byte	0x5d
	.byte	0x14
	.long	0x27f
	.byte	0xa8
	.uleb128 0xa
	.long	.LASF41
	.byte	0x5
	.byte	0x5e
	.byte	0x9
	.long	0x7f
	.byte	0xb0
	.uleb128 0xa
	.long	.LASF42
	.byte	0x5
	.byte	0x5f
	.byte	0xa
	.long	0x65
	.byte	0xb8
	.uleb128 0xa
	.long	.LASF43
	.byte	0x5
	.byte	0x60
	.byte	0x7
	.long	0x57
	.byte	0xc0
	.uleb128 0xa
	.long	.LASF44
	.byte	0x5
	.byte	0x62
	.byte	0x8
	.long	0x2b1
	.byte	0xc4
	.byte	0
	.uleb128 0x7
	.long	.LASF45
	.byte	0x6
	.byte	0x7
	.byte	0x19
	.long	0xd9
	.uleb128 0xb
	.long	.LASF113
	.byte	0x5
	.byte	0x2b
	.byte	0xe
	.uleb128 0xc
	.long	.LASF46
	.uleb128 0x3
	.byte	0x8
	.long	0x274
	.uleb128 0x3
	.byte	0x8
	.long	0xd9
	.uleb128 0xd
	.long	0x3f
	.long	0x295
	.uleb128 0xe
	.long	0x71
	.byte	0
	.byte	0
	.uleb128 0x3
	.byte	0x8
	.long	0x26c
	.uleb128 0xc
	.long	.LASF47
	.uleb128 0x3
	.byte	0x8
	.long	0x29b
	.uleb128 0xc
	.long	.LASF48
	.uleb128 0x3
	.byte	0x8
	.long	0x2a6
	.uleb128 0xd
	.long	0x3f
	.long	0x2c1
	.uleb128 0xe
	.long	0x71
	.byte	0x13
	.byte	0
	.uleb128 0x3
	.byte	0x8
	.long	0x46
	.uleb128 0x5
	.long	0x2c1
	.uleb128 0x2
	.long	.LASF49
	.byte	0x7
	.byte	0x89
	.byte	0xe
	.long	0x2d8
	.uleb128 0x3
	.byte	0x8
	.long	0x260
	.uleb128 0x2
	.long	.LASF50
	.byte	0x7
	.byte	0x8a
	.byte	0xe
	.long	0x2d8
	.uleb128 0x2
	.long	.LASF51
	.byte	0x7
	.byte	0x8b
	.byte	0xe
	.long	0x2d8
	.uleb128 0x2
	.long	.LASF52
	.byte	0x8
	.byte	0x1a
	.byte	0xc
	.long	0x57
	.uleb128 0xd
	.long	0x2c7
	.long	0x30d
	.uleb128 0xf
	.byte	0
	.uleb128 0x5
	.long	0x302
	.uleb128 0x2
	.long	.LASF53
	.byte	0x8
	.byte	0x1b
	.byte	0x1a
	.long	0x30d
	.uleb128 0x2
	.long	.LASF54
	.byte	0x8
	.byte	0x1e
	.byte	0xc
	.long	0x57
	.uleb128 0x2
	.long	.LASF55
	.byte	0x8
	.byte	0x1f
	.byte	0x1a
	.long	0x30d
	.uleb128 0x7
	.long	.LASF56
	.byte	0x9
	.byte	0x18
	.byte	0x13
	.long	0x96
	.uleb128 0x7
	.long	.LASF57
	.byte	0x9
	.byte	0x19
	.byte	0x14
	.long	0xa9
	.uleb128 0x7
	.long	.LASF58
	.byte	0x9
	.byte	0x1a
	.byte	0x14
	.long	0xb5
	.uleb128 0x4
	.byte	0x8
	.byte	0x7
	.long	.LASF59
	.uleb128 0x4
	.byte	0x8
	.byte	0x5
	.long	.LASF60
	.uleb128 0x7
	.long	.LASF61
	.byte	0xa
	.byte	0x1c
	.byte	0x1c
	.long	0x88
	.uleb128 0x9
	.long	.LASF63
	.byte	0x10
	.byte	0xb
	.byte	0xb2
	.byte	0x8
	.long	0x39c
	.uleb128 0xa
	.long	.LASF64
	.byte	0xb
	.byte	0xb4
	.byte	0x11
	.long	0x368
	.byte	0
	.uleb128 0xa
	.long	.LASF65
	.byte	0xb
	.byte	0xb5
	.byte	0xa
	.long	0x3a1
	.byte	0x2
	.byte	0
	.uleb128 0x5
	.long	0x374
	.uleb128 0xd
	.long	0x3f
	.long	0x3b1
	.uleb128 0xe
	.long	0x71
	.byte	0xd
	.byte	0
	.uleb128 0x3
	.byte	0x8
	.long	0x374
	.uleb128 0x10
	.long	0x3b1
	.uleb128 0xc
	.long	.LASF66
	.uleb128 0x5
	.long	0x3bc
	.uleb128 0x3
	.byte	0x8
	.long	0x3bc
	.uleb128 0x10
	.long	0x3c6
	.uleb128 0xc
	.long	.LASF67
	.uleb128 0x5
	.long	0x3d1
	.uleb128 0x3
	.byte	0x8
	.long	0x3d1
	.uleb128 0x10
	.long	0x3db
	.uleb128 0xc
	.long	.LASF68
	.uleb128 0x5
	.long	0x3e6
	.uleb128 0x3
	.byte	0x8
	.long	0x3e6
	.uleb128 0x10
	.long	0x3f0
	.uleb128 0xc
	.long	.LASF69
	.uleb128 0x5
	.long	0x3fb
	.uleb128 0x3
	.byte	0x8
	.long	0x3fb
	.uleb128 0x10
	.long	0x405
	.uleb128 0x9
	.long	.LASF70
	.byte	0x10
	.byte	0xc
	.byte	0xee
	.byte	0x8
	.long	0x452
	.uleb128 0xa
	.long	.LASF71
	.byte	0xc
	.byte	0xf0
	.byte	0x11
	.long	0x368
	.byte	0
	.uleb128 0xa
	.long	.LASF72
	.byte	0xc
	.byte	0xf1
	.byte	0xf
	.long	0x5f9
	.byte	0x2
	.uleb128 0xa
	.long	.LASF73
	.byte	0xc
	.byte	0xf2
	.byte	0x14
	.long	0x5de
	.byte	0x4
	.uleb128 0xa
	.long	.LASF74
	.byte	0xc
	.byte	0xf5
	.byte	0x13
	.long	0x69b
	.byte	0x8
	.byte	0
	.uleb128 0x5
	.long	0x410
	.uleb128 0x3
	.byte	0x8
	.long	0x410
	.uleb128 0x10
	.long	0x457
	.uleb128 0x9
	.long	.LASF75
	.byte	0x1c
	.byte	0xc
	.byte	0xfd
	.byte	0x8
	.long	0x4b5
	.uleb128 0xa
	.long	.LASF76
	.byte	0xc
	.byte	0xff
	.byte	0x11
	.long	0x368
	.byte	0
	.uleb128 0x11
	.long	.LASF77
	.byte	0xc
	.value	0x100
	.byte	0xf
	.long	0x5f9
	.byte	0x2
	.uleb128 0x11
	.long	.LASF78
	.byte	0xc
	.value	0x101
	.byte	0xe
	.long	0x34e
	.byte	0x4
	.uleb128 0x11
	.long	.LASF79
	.byte	0xc
	.value	0x102
	.byte	0x15
	.long	0x663
	.byte	0x8
	.uleb128 0x11
	.long	.LASF80
	.byte	0xc
	.value	0x103
	.byte	0xe
	.long	0x34e
	.byte	0x18
	.byte	0
	.uleb128 0x5
	.long	0x462
	.uleb128 0x3
	.byte	0x8
	.long	0x462
	.uleb128 0x10
	.long	0x4ba
	.uleb128 0xc
	.long	.LASF81
	.uleb128 0x5
	.long	0x4c5
	.uleb128 0x3
	.byte	0x8
	.long	0x4c5
	.uleb128 0x10
	.long	0x4cf
	.uleb128 0xc
	.long	.LASF82
	.uleb128 0x5
	.long	0x4da
	.uleb128 0x3
	.byte	0x8
	.long	0x4da
	.uleb128 0x10
	.long	0x4e4
	.uleb128 0xc
	.long	.LASF83
	.uleb128 0x5
	.long	0x4ef
	.uleb128 0x3
	.byte	0x8
	.long	0x4ef
	.uleb128 0x10
	.long	0x4f9
	.uleb128 0xc
	.long	.LASF84
	.uleb128 0x5
	.long	0x504
	.uleb128 0x3
	.byte	0x8
	.long	0x504
	.uleb128 0x10
	.long	0x50e
	.uleb128 0xc
	.long	.LASF85
	.uleb128 0x5
	.long	0x519
	.uleb128 0x3
	.byte	0x8
	.long	0x519
	.uleb128 0x10
	.long	0x523
	.uleb128 0xc
	.long	.LASF86
	.uleb128 0x5
	.long	0x52e
	.uleb128 0x3
	.byte	0x8
	.long	0x52e
	.uleb128 0x10
	.long	0x538
	.uleb128 0x3
	.byte	0x8
	.long	0x39c
	.uleb128 0x10
	.long	0x543
	.uleb128 0x3
	.byte	0x8
	.long	0x3c1
	.uleb128 0x10
	.long	0x54e
	.uleb128 0x3
	.byte	0x8
	.long	0x3d6
	.uleb128 0x10
	.long	0x559
	.uleb128 0x3
	.byte	0x8
	.long	0x3eb
	.uleb128 0x10
	.long	0x564
	.uleb128 0x3
	.byte	0x8
	.long	0x400
	.uleb128 0x10
	.long	0x56f
	.uleb128 0x3
	.byte	0x8
	.long	0x452
	.uleb128 0x10
	.long	0x57a
	.uleb128 0x3
	.byte	0x8
	.long	0x4b5
	.uleb128 0x10
	.long	0x585
	.uleb128 0x3
	.byte	0x8
	.long	0x4ca
	.uleb128 0x10
	.long	0x590
	.uleb128 0x3
	.byte	0x8
	.long	0x4df
	.uleb128 0x10
	.long	0x59b
	.uleb128 0x3
	.byte	0x8
	.long	0x4f4
	.uleb128 0x10
	.long	0x5a6
	.uleb128 0x3
	.byte	0x8
	.long	0x509
	.uleb128 0x10
	.long	0x5b1
	.uleb128 0x3
	.byte	0x8
	.long	0x51e
	.uleb128 0x10
	.long	0x5bc
	.uleb128 0x3
	.byte	0x8
	.long	0x533
	.uleb128 0x10
	.long	0x5c7
	.uleb128 0x7
	.long	.LASF87
	.byte	0xc
	.byte	0x1e
	.byte	0x12
	.long	0x34e
	.uleb128 0x9
	.long	.LASF88
	.byte	0x4
	.byte	0xc
	.byte	0x1f
	.byte	0x8
	.long	0x5f9
	.uleb128 0xa
	.long	.LASF89
	.byte	0xc
	.byte	0x21
	.byte	0xf
	.long	0x5d2
	.byte	0
	.byte	0
	.uleb128 0x7
	.long	.LASF90
	.byte	0xc
	.byte	0x77
	.byte	0x12
	.long	0x342
	.uleb128 0x12
	.byte	0x10
	.byte	0xc
	.byte	0xd6
	.byte	0x5
	.long	0x633
	.uleb128 0x13
	.long	.LASF91
	.byte	0xc
	.byte	0xd8
	.byte	0xa
	.long	0x633
	.uleb128 0x13
	.long	.LASF92
	.byte	0xc
	.byte	0xd9
	.byte	0xb
	.long	0x643
	.uleb128 0x13
	.long	.LASF93
	.byte	0xc
	.byte	0xda
	.byte	0xb
	.long	0x653
	.byte	0
	.uleb128 0xd
	.long	0x336
	.long	0x643
	.uleb128 0xe
	.long	0x71
	.byte	0xf
	.byte	0
	.uleb128 0xd
	.long	0x342
	.long	0x653
	.uleb128 0xe
	.long	0x71
	.byte	0x7
	.byte	0
	.uleb128 0xd
	.long	0x34e
	.long	0x663
	.uleb128 0xe
	.long	0x71
	.byte	0x3
	.byte	0
	.uleb128 0x9
	.long	.LASF94
	.byte	0x10
	.byte	0xc
	.byte	0xd4
	.byte	0x8
	.long	0x67e
	.uleb128 0xa
	.long	.LASF95
	.byte	0xc
	.byte	0xdb
	.byte	0x9
	.long	0x605
	.byte	0
	.byte	0
	.uleb128 0x5
	.long	0x663
	.uleb128 0x2
	.long	.LASF96
	.byte	0xc
	.byte	0xe4
	.byte	0x1e
	.long	0x67e
	.uleb128 0x2
	.long	.LASF97
	.byte	0xc
	.byte	0xe5
	.byte	0x1e
	.long	0x67e
	.uleb128 0xd
	.long	0x81
	.long	0x6ab
	.uleb128 0xe
	.long	0x71
	.byte	0x7
	.byte	0
	.uleb128 0xd
	.long	0x2c7
	.long	0x6bb
	.uleb128 0xe
	.long	0x71
	.byte	0x40
	.byte	0
	.uleb128 0x5
	.long	0x6ab
	.uleb128 0x14
	.long	.LASF98
	.byte	0xd
	.value	0x11e
	.byte	0x1a
	.long	0x6bb
	.uleb128 0x14
	.long	.LASF99
	.byte	0xd
	.value	0x11f
	.byte	0x1a
	.long	0x6bb
	.uleb128 0xd
	.long	0x39
	.long	0x6ea
	.uleb128 0xe
	.long	0x71
	.byte	0x1
	.byte	0
	.uleb128 0x2
	.long	.LASF100
	.byte	0xe
	.byte	0x9f
	.byte	0xe
	.long	0x6da
	.uleb128 0x2
	.long	.LASF101
	.byte	0xe
	.byte	0xa0
	.byte	0xc
	.long	0x57
	.uleb128 0x2
	.long	.LASF102
	.byte	0xe
	.byte	0xa1
	.byte	0x11
	.long	0x5e
	.uleb128 0x2
	.long	.LASF103
	.byte	0xe
	.byte	0xa6
	.byte	0xe
	.long	0x6da
	.uleb128 0x2
	.long	.LASF104
	.byte	0xe
	.byte	0xae
	.byte	0xc
	.long	0x57
	.uleb128 0x2
	.long	.LASF105
	.byte	0xe
	.byte	0xaf
	.byte	0x11
	.long	0x5e
	.uleb128 0x14
	.long	.LASF106
	.byte	0xe
	.value	0x112
	.byte	0xc
	.long	0x57
	.uleb128 0x4
	.byte	0x8
	.byte	0x4
	.long	.LASF107
	.uleb128 0x15
	.long	.LASF108
	.byte	0x1
	.byte	0x2b
	.byte	0xd
	.long	0x2c1
	.quad	.LFB55
	.quad	.LFE55-.LFB55
	.uleb128 0x1
	.byte	0x9c
	.uleb128 0x15
	.long	.LASF109
	.byte	0x1
	.byte	0x26
	.byte	0xe
	.long	0x78
	.quad	.LFB54
	.quad	.LFE54-.LFB54
	.uleb128 0x1
	.byte	0x9c
	.byte	0
	.section	.debug_abbrev,"",@progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1b
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x7
	.uleb128 0x10
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x13
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x13
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3c
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x21
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x37
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0x17
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x7
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2117
	.uleb128 0x19
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_aranges,"",@progbits
	.long	0x2c
	.value	0x2
	.long	.Ldebug_info0
	.byte	0x8
	.byte	0
	.value	0
	.value	0
	.quad	.Ltext0
	.quad	.Letext0-.Ltext0
	.quad	0
	.quad	0
	.section	.debug_line,"",@progbits
.Ldebug_line0:
	.section	.debug_str,"MS",@progbits,1
.LASF24:
	.string	"_IO_buf_end"
.LASF32:
	.string	"_old_offset"
.LASF19:
	.string	"_IO_read_base"
.LASF2:
	.string	"char"
.LASF52:
	.string	"sys_nerr"
.LASF27:
	.string	"_IO_save_end"
.LASF83:
	.string	"sockaddr_iso"
.LASF9:
	.string	"size_t"
.LASF94:
	.string	"in6_addr"
.LASF81:
	.string	"sockaddr_inarp"
.LASF37:
	.string	"_offset"
.LASF100:
	.string	"__tzname"
.LASF99:
	.string	"sys_siglist"
.LASF21:
	.string	"_IO_write_ptr"
.LASF16:
	.string	"_flags"
.LASF13:
	.string	"__uint32_t"
.LASF78:
	.string	"sin6_flowinfo"
.LASF85:
	.string	"sockaddr_un"
.LASF12:
	.string	"__uint16_t"
.LASF107:
	.string	"double"
.LASF54:
	.string	"_sys_nerr"
.LASF11:
	.string	"short int"
.LASF28:
	.string	"_markers"
.LASF18:
	.string	"_IO_read_end"
.LASF41:
	.string	"_freeres_buf"
.LASF104:
	.string	"daylight"
.LASF56:
	.string	"uint8_t"
.LASF38:
	.string	"_codecvt"
.LASF80:
	.string	"sin6_scope_id"
.LASF105:
	.string	"timezone"
.LASF71:
	.string	"sin_family"
.LASF77:
	.string	"sin6_port"
.LASF55:
	.string	"_sys_errlist"
.LASF98:
	.string	"_sys_siglist"
.LASF51:
	.string	"stderr"
.LASF84:
	.string	"sockaddr_ns"
.LASF60:
	.string	"long long int"
.LASF92:
	.string	"__u6_addr16"
.LASF102:
	.string	"__timezone"
.LASF36:
	.string	"_lock"
.LASF110:
	.string	"GNU C89 9.3.0 -m64 -mtune=generic -march=x86-64 -g -O3 -std=gnu90 -fvisibility=hidden -fno-omit-frame-pointer -fasynchronous-unwind-tables -fstack-protector-strong -fstack-clash-protection -fcf-protection"
.LASF3:
	.string	"long int"
.LASF106:
	.string	"getdate_err"
.LASF33:
	.string	"_cur_column"
.LASF10:
	.string	"__uint8_t"
.LASF42:
	.string	"__pad5"
.LASF0:
	.string	"program_invocation_name"
.LASF20:
	.string	"_IO_write_base"
.LASF73:
	.string	"sin_addr"
.LASF97:
	.string	"in6addr_loopback"
.LASF62:
	.string	"_IO_FILE"
.LASF61:
	.string	"sa_family_t"
.LASF6:
	.string	"unsigned char"
.LASF23:
	.string	"_IO_buf_base"
.LASF67:
	.string	"sockaddr_ax25"
.LASF8:
	.string	"signed char"
.LASF74:
	.string	"sin_zero"
.LASF59:
	.string	"long long unsigned int"
.LASF75:
	.string	"sockaddr_in6"
.LASF58:
	.string	"uint32_t"
.LASF5:
	.string	"unsigned int"
.LASF46:
	.string	"_IO_marker"
.LASF35:
	.string	"_shortbuf"
.LASF89:
	.string	"s_addr"
.LASF29:
	.string	"_chain"
.LASF44:
	.string	"_unused2"
.LASF76:
	.string	"sin6_family"
.LASF17:
	.string	"_IO_read_ptr"
.LASF79:
	.string	"sin6_addr"
.LASF65:
	.string	"sa_data"
.LASF7:
	.string	"short unsigned int"
.LASF68:
	.string	"sockaddr_dl"
.LASF109:
	.string	"uv_version"
.LASF57:
	.string	"uint16_t"
.LASF39:
	.string	"_wide_data"
.LASF40:
	.string	"_freeres_list"
.LASF103:
	.string	"tzname"
.LASF87:
	.string	"in_addr_t"
.LASF112:
	.string	"/workspace/nodejsbuild/node-v12.18.4/out"
.LASF69:
	.string	"sockaddr_eon"
.LASF101:
	.string	"__daylight"
.LASF66:
	.string	"sockaddr_at"
.LASF47:
	.string	"_IO_codecvt"
.LASF1:
	.string	"program_invocation_short_name"
.LASF108:
	.string	"uv_version_string"
.LASF4:
	.string	"long unsigned int"
.LASF88:
	.string	"in_addr"
.LASF22:
	.string	"_IO_write_end"
.LASF15:
	.string	"__off64_t"
.LASF86:
	.string	"sockaddr_x25"
.LASF30:
	.string	"_fileno"
.LASF72:
	.string	"sin_port"
.LASF14:
	.string	"__off_t"
.LASF111:
	.string	"../deps/uv/src/version.c"
.LASF26:
	.string	"_IO_backup_base"
.LASF49:
	.string	"stdin"
.LASF90:
	.string	"in_port_t"
.LASF31:
	.string	"_flags2"
.LASF64:
	.string	"sa_family"
.LASF43:
	.string	"_mode"
.LASF95:
	.string	"__in6_u"
.LASF93:
	.string	"__u6_addr32"
.LASF34:
	.string	"_vtable_offset"
.LASF48:
	.string	"_IO_wide_data"
.LASF96:
	.string	"in6addr_any"
.LASF25:
	.string	"_IO_save_base"
.LASF53:
	.string	"sys_errlist"
.LASF45:
	.string	"FILE"
.LASF91:
	.string	"__u6_addr8"
.LASF82:
	.string	"sockaddr_ipx"
.LASF70:
	.string	"sockaddr_in"
.LASF50:
	.string	"stdout"
.LASF63:
	.string	"sockaddr"
.LASF113:
	.string	"_IO_lock_t"
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
