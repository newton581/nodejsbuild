	.file	"strscpy.c"
	.text
.Ltext0:
	.p2align 4
	.globl	uv__strscpy
	.hidden	uv__strscpy
	.type	uv__strscpy, @function
uv__strscpy:
.LVL0:
.LFB54:
	.file 1 "../deps/uv/src/strscpy.c"
	.loc 1 4 55 view -0
	.cfi_startproc
	.loc 1 4 55 is_stmt 0 view .LVU1
	endbr64
	.loc 1 5 3 is_stmt 1 view .LVU2
	.loc 1 7 3 view .LVU3
.LVL1:
	.loc 1 7 15 view .LVU4
	.loc 1 7 10 is_stmt 0 view .LVU5
	xorl	%eax, %eax
	.loc 1 7 3 view .LVU6
	testq	%rdx, %rdx
	jne	.L4
	jmp	.L5
.LVL2:
	.p2align 4,,10
	.p2align 3
.L3:
	.loc 1 7 22 is_stmt 1 discriminator 2 view .LVU7
	.loc 1 7 23 is_stmt 0 discriminator 2 view .LVU8
	addq	$1, %rax
.LVL3:
	.loc 1 7 15 is_stmt 1 discriminator 2 view .LVU9
	.loc 1 7 3 is_stmt 0 discriminator 2 view .LVU10
	cmpq	%rax, %rdx
	je	.L9
.LVL4:
.L4:
	.loc 1 8 5 is_stmt 1 view .LVU11
	.loc 1 8 26 is_stmt 0 view .LVU12
	movzbl	(%rsi,%rax), %ecx
	leaq	(%rdi,%rax), %r8
	.loc 1 8 23 view .LVU13
	movb	%cl, (%rdi,%rax)
	.loc 1 8 8 view .LVU14
	testb	%cl, %cl
	jne	.L3
	.loc 1 9 7 is_stmt 1 view .LVU15
	.loc 1 9 49 is_stmt 0 view .LVU16
	testq	%rax, %rax
	movq	$-7, %rdx
.LVL5:
	.loc 1 9 49 view .LVU17
	cmovs	%rdx, %rax
	ret
.LVL6:
	.p2align 4,,10
	.p2align 3
.L9:
	.loc 1 11 3 is_stmt 1 view .LVU18
	.loc 1 14 3 view .LVU19
	.loc 1 14 10 is_stmt 0 view .LVU20
	movb	$0, (%r8)
	.loc 1 16 3 is_stmt 1 view .LVU21
	.loc 1 16 10 is_stmt 0 view .LVU22
	movq	$-7, %rax
.LVL7:
	.loc 1 16 10 view .LVU23
	ret
.LVL8:
.L5:
	.loc 1 17 1 view .LVU24
	ret
	.cfi_endproc
.LFE54:
	.size	uv__strscpy, .-uv__strscpy
.Letext0:
	.file 2 "/usr/include/errno.h"
	.file 3 "/usr/lib/gcc/x86_64-linux-gnu/9/include/stddef.h"
	.file 4 "/usr/include/x86_64-linux-gnu/bits/types.h"
	.file 5 "/usr/include/x86_64-linux-gnu/bits/types/struct_FILE.h"
	.file 6 "/usr/include/x86_64-linux-gnu/bits/types/FILE.h"
	.file 7 "/usr/include/stdio.h"
	.file 8 "/usr/include/x86_64-linux-gnu/bits/sys_errlist.h"
	.file 9 "/usr/include/x86_64-linux-gnu/bits/stdint-uintn.h"
	.file 10 "/usr/include/x86_64-linux-gnu/bits/sockaddr.h"
	.file 11 "/usr/include/x86_64-linux-gnu/bits/socket.h"
	.file 12 "/usr/include/netinet/in.h"
	.file 13 "/usr/include/signal.h"
	.file 14 "/usr/include/time.h"
	.file 15 "../deps/uv/include/uv.h"
	.section	.debug_info,"",@progbits
.Ldebug_info0:
	.long	0x9d9
	.value	0x4
	.long	.Ldebug_abbrev0
	.byte	0x8
	.uleb128 0x1
	.long	.LASF191
	.byte	0x1
	.long	.LASF192
	.long	.LASF193
	.quad	.Ltext0
	.quad	.Letext0-.Ltext0
	.long	.Ldebug_line0
	.uleb128 0x2
	.long	.LASF0
	.byte	0x2
	.byte	0x2d
	.byte	0xe
	.long	0x39
	.uleb128 0x3
	.byte	0x8
	.long	0x3f
	.uleb128 0x4
	.byte	0x1
	.byte	0x6
	.long	.LASF2
	.uleb128 0x5
	.long	0x3f
	.uleb128 0x2
	.long	.LASF1
	.byte	0x2
	.byte	0x2e
	.byte	0xe
	.long	0x39
	.uleb128 0x6
	.byte	0x4
	.byte	0x5
	.string	"int"
	.uleb128 0x4
	.byte	0x8
	.byte	0x5
	.long	.LASF3
	.uleb128 0x7
	.long	.LASF9
	.byte	0x3
	.byte	0xd1
	.byte	0x1b
	.long	0x71
	.uleb128 0x4
	.byte	0x8
	.byte	0x7
	.long	.LASF4
	.uleb128 0x4
	.byte	0x4
	.byte	0x7
	.long	.LASF5
	.uleb128 0x8
	.byte	0x8
	.uleb128 0x4
	.byte	0x1
	.byte	0x8
	.long	.LASF6
	.uleb128 0x4
	.byte	0x2
	.byte	0x7
	.long	.LASF7
	.uleb128 0x4
	.byte	0x1
	.byte	0x6
	.long	.LASF8
	.uleb128 0x7
	.long	.LASF10
	.byte	0x4
	.byte	0x26
	.byte	0x17
	.long	0x81
	.uleb128 0x4
	.byte	0x2
	.byte	0x5
	.long	.LASF11
	.uleb128 0x7
	.long	.LASF12
	.byte	0x4
	.byte	0x28
	.byte	0x1c
	.long	0x88
	.uleb128 0x7
	.long	.LASF13
	.byte	0x4
	.byte	0x2a
	.byte	0x16
	.long	0x78
	.uleb128 0x7
	.long	.LASF14
	.byte	0x4
	.byte	0x98
	.byte	0x12
	.long	0x5e
	.uleb128 0x7
	.long	.LASF15
	.byte	0x4
	.byte	0x99
	.byte	0x12
	.long	0x5e
	.uleb128 0x7
	.long	.LASF16
	.byte	0x4
	.byte	0xc1
	.byte	0x12
	.long	0x5e
	.uleb128 0x9
	.long	.LASF64
	.byte	0xd8
	.byte	0x5
	.byte	0x31
	.byte	0x8
	.long	0x26c
	.uleb128 0xa
	.long	.LASF17
	.byte	0x5
	.byte	0x33
	.byte	0x7
	.long	0x57
	.byte	0
	.uleb128 0xa
	.long	.LASF18
	.byte	0x5
	.byte	0x36
	.byte	0x9
	.long	0x39
	.byte	0x8
	.uleb128 0xa
	.long	.LASF19
	.byte	0x5
	.byte	0x37
	.byte	0x9
	.long	0x39
	.byte	0x10
	.uleb128 0xa
	.long	.LASF20
	.byte	0x5
	.byte	0x38
	.byte	0x9
	.long	0x39
	.byte	0x18
	.uleb128 0xa
	.long	.LASF21
	.byte	0x5
	.byte	0x39
	.byte	0x9
	.long	0x39
	.byte	0x20
	.uleb128 0xa
	.long	.LASF22
	.byte	0x5
	.byte	0x3a
	.byte	0x9
	.long	0x39
	.byte	0x28
	.uleb128 0xa
	.long	.LASF23
	.byte	0x5
	.byte	0x3b
	.byte	0x9
	.long	0x39
	.byte	0x30
	.uleb128 0xa
	.long	.LASF24
	.byte	0x5
	.byte	0x3c
	.byte	0x9
	.long	0x39
	.byte	0x38
	.uleb128 0xa
	.long	.LASF25
	.byte	0x5
	.byte	0x3d
	.byte	0x9
	.long	0x39
	.byte	0x40
	.uleb128 0xa
	.long	.LASF26
	.byte	0x5
	.byte	0x40
	.byte	0x9
	.long	0x39
	.byte	0x48
	.uleb128 0xa
	.long	.LASF27
	.byte	0x5
	.byte	0x41
	.byte	0x9
	.long	0x39
	.byte	0x50
	.uleb128 0xa
	.long	.LASF28
	.byte	0x5
	.byte	0x42
	.byte	0x9
	.long	0x39
	.byte	0x58
	.uleb128 0xa
	.long	.LASF29
	.byte	0x5
	.byte	0x44
	.byte	0x16
	.long	0x285
	.byte	0x60
	.uleb128 0xa
	.long	.LASF30
	.byte	0x5
	.byte	0x46
	.byte	0x14
	.long	0x28b
	.byte	0x68
	.uleb128 0xa
	.long	.LASF31
	.byte	0x5
	.byte	0x48
	.byte	0x7
	.long	0x57
	.byte	0x70
	.uleb128 0xa
	.long	.LASF32
	.byte	0x5
	.byte	0x49
	.byte	0x7
	.long	0x57
	.byte	0x74
	.uleb128 0xa
	.long	.LASF33
	.byte	0x5
	.byte	0x4a
	.byte	0xb
	.long	0xc1
	.byte	0x78
	.uleb128 0xa
	.long	.LASF34
	.byte	0x5
	.byte	0x4d
	.byte	0x12
	.long	0x88
	.byte	0x80
	.uleb128 0xa
	.long	.LASF35
	.byte	0x5
	.byte	0x4e
	.byte	0xf
	.long	0x8f
	.byte	0x82
	.uleb128 0xa
	.long	.LASF36
	.byte	0x5
	.byte	0x4f
	.byte	0x8
	.long	0x291
	.byte	0x83
	.uleb128 0xa
	.long	.LASF37
	.byte	0x5
	.byte	0x51
	.byte	0xf
	.long	0x2a1
	.byte	0x88
	.uleb128 0xa
	.long	.LASF38
	.byte	0x5
	.byte	0x59
	.byte	0xd
	.long	0xcd
	.byte	0x90
	.uleb128 0xa
	.long	.LASF39
	.byte	0x5
	.byte	0x5b
	.byte	0x17
	.long	0x2ac
	.byte	0x98
	.uleb128 0xa
	.long	.LASF40
	.byte	0x5
	.byte	0x5c
	.byte	0x19
	.long	0x2b7
	.byte	0xa0
	.uleb128 0xa
	.long	.LASF41
	.byte	0x5
	.byte	0x5d
	.byte	0x14
	.long	0x28b
	.byte	0xa8
	.uleb128 0xa
	.long	.LASF42
	.byte	0x5
	.byte	0x5e
	.byte	0x9
	.long	0x7f
	.byte	0xb0
	.uleb128 0xa
	.long	.LASF43
	.byte	0x5
	.byte	0x5f
	.byte	0xa
	.long	0x65
	.byte	0xb8
	.uleb128 0xa
	.long	.LASF44
	.byte	0x5
	.byte	0x60
	.byte	0x7
	.long	0x57
	.byte	0xc0
	.uleb128 0xa
	.long	.LASF45
	.byte	0x5
	.byte	0x62
	.byte	0x8
	.long	0x2bd
	.byte	0xc4
	.byte	0
	.uleb128 0x7
	.long	.LASF46
	.byte	0x6
	.byte	0x7
	.byte	0x19
	.long	0xe5
	.uleb128 0xb
	.long	.LASF194
	.byte	0x5
	.byte	0x2b
	.byte	0xe
	.uleb128 0xc
	.long	.LASF47
	.uleb128 0x3
	.byte	0x8
	.long	0x280
	.uleb128 0x3
	.byte	0x8
	.long	0xe5
	.uleb128 0xd
	.long	0x3f
	.long	0x2a1
	.uleb128 0xe
	.long	0x71
	.byte	0
	.byte	0
	.uleb128 0x3
	.byte	0x8
	.long	0x278
	.uleb128 0xc
	.long	.LASF48
	.uleb128 0x3
	.byte	0x8
	.long	0x2a7
	.uleb128 0xc
	.long	.LASF49
	.uleb128 0x3
	.byte	0x8
	.long	0x2b2
	.uleb128 0xd
	.long	0x3f
	.long	0x2cd
	.uleb128 0xe
	.long	0x71
	.byte	0x13
	.byte	0
	.uleb128 0x3
	.byte	0x8
	.long	0x46
	.uleb128 0x5
	.long	0x2cd
	.uleb128 0x7
	.long	.LASF50
	.byte	0x7
	.byte	0x4d
	.byte	0x13
	.long	0xd9
	.uleb128 0x2
	.long	.LASF51
	.byte	0x7
	.byte	0x89
	.byte	0xe
	.long	0x2f0
	.uleb128 0x3
	.byte	0x8
	.long	0x26c
	.uleb128 0x2
	.long	.LASF52
	.byte	0x7
	.byte	0x8a
	.byte	0xe
	.long	0x2f0
	.uleb128 0x2
	.long	.LASF53
	.byte	0x7
	.byte	0x8b
	.byte	0xe
	.long	0x2f0
	.uleb128 0x2
	.long	.LASF54
	.byte	0x8
	.byte	0x1a
	.byte	0xc
	.long	0x57
	.uleb128 0xd
	.long	0x2d3
	.long	0x325
	.uleb128 0xf
	.byte	0
	.uleb128 0x5
	.long	0x31a
	.uleb128 0x2
	.long	.LASF55
	.byte	0x8
	.byte	0x1b
	.byte	0x1a
	.long	0x325
	.uleb128 0x2
	.long	.LASF56
	.byte	0x8
	.byte	0x1e
	.byte	0xc
	.long	0x57
	.uleb128 0x2
	.long	.LASF57
	.byte	0x8
	.byte	0x1f
	.byte	0x1a
	.long	0x325
	.uleb128 0x7
	.long	.LASF58
	.byte	0x9
	.byte	0x18
	.byte	0x13
	.long	0x96
	.uleb128 0x7
	.long	.LASF59
	.byte	0x9
	.byte	0x19
	.byte	0x14
	.long	0xa9
	.uleb128 0x7
	.long	.LASF60
	.byte	0x9
	.byte	0x1a
	.byte	0x14
	.long	0xb5
	.uleb128 0x4
	.byte	0x8
	.byte	0x7
	.long	.LASF61
	.uleb128 0x4
	.byte	0x8
	.byte	0x5
	.long	.LASF62
	.uleb128 0x7
	.long	.LASF63
	.byte	0xa
	.byte	0x1c
	.byte	0x1c
	.long	0x88
	.uleb128 0x9
	.long	.LASF65
	.byte	0x10
	.byte	0xb
	.byte	0xb2
	.byte	0x8
	.long	0x3b4
	.uleb128 0xa
	.long	.LASF66
	.byte	0xb
	.byte	0xb4
	.byte	0x11
	.long	0x380
	.byte	0
	.uleb128 0xa
	.long	.LASF67
	.byte	0xb
	.byte	0xb5
	.byte	0xa
	.long	0x3b9
	.byte	0x2
	.byte	0
	.uleb128 0x5
	.long	0x38c
	.uleb128 0xd
	.long	0x3f
	.long	0x3c9
	.uleb128 0xe
	.long	0x71
	.byte	0xd
	.byte	0
	.uleb128 0x3
	.byte	0x8
	.long	0x38c
	.uleb128 0x10
	.long	0x3c9
	.uleb128 0xc
	.long	.LASF68
	.uleb128 0x5
	.long	0x3d4
	.uleb128 0x3
	.byte	0x8
	.long	0x3d4
	.uleb128 0x10
	.long	0x3de
	.uleb128 0xc
	.long	.LASF69
	.uleb128 0x5
	.long	0x3e9
	.uleb128 0x3
	.byte	0x8
	.long	0x3e9
	.uleb128 0x10
	.long	0x3f3
	.uleb128 0xc
	.long	.LASF70
	.uleb128 0x5
	.long	0x3fe
	.uleb128 0x3
	.byte	0x8
	.long	0x3fe
	.uleb128 0x10
	.long	0x408
	.uleb128 0xc
	.long	.LASF71
	.uleb128 0x5
	.long	0x413
	.uleb128 0x3
	.byte	0x8
	.long	0x413
	.uleb128 0x10
	.long	0x41d
	.uleb128 0x9
	.long	.LASF72
	.byte	0x10
	.byte	0xc
	.byte	0xee
	.byte	0x8
	.long	0x46a
	.uleb128 0xa
	.long	.LASF73
	.byte	0xc
	.byte	0xf0
	.byte	0x11
	.long	0x380
	.byte	0
	.uleb128 0xa
	.long	.LASF74
	.byte	0xc
	.byte	0xf1
	.byte	0xf
	.long	0x611
	.byte	0x2
	.uleb128 0xa
	.long	.LASF75
	.byte	0xc
	.byte	0xf2
	.byte	0x14
	.long	0x5f6
	.byte	0x4
	.uleb128 0xa
	.long	.LASF76
	.byte	0xc
	.byte	0xf5
	.byte	0x13
	.long	0x6b3
	.byte	0x8
	.byte	0
	.uleb128 0x5
	.long	0x428
	.uleb128 0x3
	.byte	0x8
	.long	0x428
	.uleb128 0x10
	.long	0x46f
	.uleb128 0x9
	.long	.LASF77
	.byte	0x1c
	.byte	0xc
	.byte	0xfd
	.byte	0x8
	.long	0x4cd
	.uleb128 0xa
	.long	.LASF78
	.byte	0xc
	.byte	0xff
	.byte	0x11
	.long	0x380
	.byte	0
	.uleb128 0x11
	.long	.LASF79
	.byte	0xc
	.value	0x100
	.byte	0xf
	.long	0x611
	.byte	0x2
	.uleb128 0x11
	.long	.LASF80
	.byte	0xc
	.value	0x101
	.byte	0xe
	.long	0x366
	.byte	0x4
	.uleb128 0x11
	.long	.LASF81
	.byte	0xc
	.value	0x102
	.byte	0x15
	.long	0x67b
	.byte	0x8
	.uleb128 0x11
	.long	.LASF82
	.byte	0xc
	.value	0x103
	.byte	0xe
	.long	0x366
	.byte	0x18
	.byte	0
	.uleb128 0x5
	.long	0x47a
	.uleb128 0x3
	.byte	0x8
	.long	0x47a
	.uleb128 0x10
	.long	0x4d2
	.uleb128 0xc
	.long	.LASF83
	.uleb128 0x5
	.long	0x4dd
	.uleb128 0x3
	.byte	0x8
	.long	0x4dd
	.uleb128 0x10
	.long	0x4e7
	.uleb128 0xc
	.long	.LASF84
	.uleb128 0x5
	.long	0x4f2
	.uleb128 0x3
	.byte	0x8
	.long	0x4f2
	.uleb128 0x10
	.long	0x4fc
	.uleb128 0xc
	.long	.LASF85
	.uleb128 0x5
	.long	0x507
	.uleb128 0x3
	.byte	0x8
	.long	0x507
	.uleb128 0x10
	.long	0x511
	.uleb128 0xc
	.long	.LASF86
	.uleb128 0x5
	.long	0x51c
	.uleb128 0x3
	.byte	0x8
	.long	0x51c
	.uleb128 0x10
	.long	0x526
	.uleb128 0xc
	.long	.LASF87
	.uleb128 0x5
	.long	0x531
	.uleb128 0x3
	.byte	0x8
	.long	0x531
	.uleb128 0x10
	.long	0x53b
	.uleb128 0xc
	.long	.LASF88
	.uleb128 0x5
	.long	0x546
	.uleb128 0x3
	.byte	0x8
	.long	0x546
	.uleb128 0x10
	.long	0x550
	.uleb128 0x3
	.byte	0x8
	.long	0x3b4
	.uleb128 0x10
	.long	0x55b
	.uleb128 0x3
	.byte	0x8
	.long	0x3d9
	.uleb128 0x10
	.long	0x566
	.uleb128 0x3
	.byte	0x8
	.long	0x3ee
	.uleb128 0x10
	.long	0x571
	.uleb128 0x3
	.byte	0x8
	.long	0x403
	.uleb128 0x10
	.long	0x57c
	.uleb128 0x3
	.byte	0x8
	.long	0x418
	.uleb128 0x10
	.long	0x587
	.uleb128 0x3
	.byte	0x8
	.long	0x46a
	.uleb128 0x10
	.long	0x592
	.uleb128 0x3
	.byte	0x8
	.long	0x4cd
	.uleb128 0x10
	.long	0x59d
	.uleb128 0x3
	.byte	0x8
	.long	0x4e2
	.uleb128 0x10
	.long	0x5a8
	.uleb128 0x3
	.byte	0x8
	.long	0x4f7
	.uleb128 0x10
	.long	0x5b3
	.uleb128 0x3
	.byte	0x8
	.long	0x50c
	.uleb128 0x10
	.long	0x5be
	.uleb128 0x3
	.byte	0x8
	.long	0x521
	.uleb128 0x10
	.long	0x5c9
	.uleb128 0x3
	.byte	0x8
	.long	0x536
	.uleb128 0x10
	.long	0x5d4
	.uleb128 0x3
	.byte	0x8
	.long	0x54b
	.uleb128 0x10
	.long	0x5df
	.uleb128 0x7
	.long	.LASF89
	.byte	0xc
	.byte	0x1e
	.byte	0x12
	.long	0x366
	.uleb128 0x9
	.long	.LASF90
	.byte	0x4
	.byte	0xc
	.byte	0x1f
	.byte	0x8
	.long	0x611
	.uleb128 0xa
	.long	.LASF91
	.byte	0xc
	.byte	0x21
	.byte	0xf
	.long	0x5ea
	.byte	0
	.byte	0
	.uleb128 0x7
	.long	.LASF92
	.byte	0xc
	.byte	0x77
	.byte	0x12
	.long	0x35a
	.uleb128 0x12
	.byte	0x10
	.byte	0xc
	.byte	0xd6
	.byte	0x5
	.long	0x64b
	.uleb128 0x13
	.long	.LASF93
	.byte	0xc
	.byte	0xd8
	.byte	0xa
	.long	0x64b
	.uleb128 0x13
	.long	.LASF94
	.byte	0xc
	.byte	0xd9
	.byte	0xb
	.long	0x65b
	.uleb128 0x13
	.long	.LASF95
	.byte	0xc
	.byte	0xda
	.byte	0xb
	.long	0x66b
	.byte	0
	.uleb128 0xd
	.long	0x34e
	.long	0x65b
	.uleb128 0xe
	.long	0x71
	.byte	0xf
	.byte	0
	.uleb128 0xd
	.long	0x35a
	.long	0x66b
	.uleb128 0xe
	.long	0x71
	.byte	0x7
	.byte	0
	.uleb128 0xd
	.long	0x366
	.long	0x67b
	.uleb128 0xe
	.long	0x71
	.byte	0x3
	.byte	0
	.uleb128 0x9
	.long	.LASF96
	.byte	0x10
	.byte	0xc
	.byte	0xd4
	.byte	0x8
	.long	0x696
	.uleb128 0xa
	.long	.LASF97
	.byte	0xc
	.byte	0xdb
	.byte	0x9
	.long	0x61d
	.byte	0
	.byte	0
	.uleb128 0x5
	.long	0x67b
	.uleb128 0x2
	.long	.LASF98
	.byte	0xc
	.byte	0xe4
	.byte	0x1e
	.long	0x696
	.uleb128 0x2
	.long	.LASF99
	.byte	0xc
	.byte	0xe5
	.byte	0x1e
	.long	0x696
	.uleb128 0xd
	.long	0x81
	.long	0x6c3
	.uleb128 0xe
	.long	0x71
	.byte	0x7
	.byte	0
	.uleb128 0xd
	.long	0x2d3
	.long	0x6d3
	.uleb128 0xe
	.long	0x71
	.byte	0x40
	.byte	0
	.uleb128 0x5
	.long	0x6c3
	.uleb128 0x14
	.long	.LASF100
	.byte	0xd
	.value	0x11e
	.byte	0x1a
	.long	0x6d3
	.uleb128 0x14
	.long	.LASF101
	.byte	0xd
	.value	0x11f
	.byte	0x1a
	.long	0x6d3
	.uleb128 0xd
	.long	0x39
	.long	0x702
	.uleb128 0xe
	.long	0x71
	.byte	0x1
	.byte	0
	.uleb128 0x2
	.long	.LASF102
	.byte	0xe
	.byte	0x9f
	.byte	0xe
	.long	0x6f2
	.uleb128 0x2
	.long	.LASF103
	.byte	0xe
	.byte	0xa0
	.byte	0xc
	.long	0x57
	.uleb128 0x2
	.long	.LASF104
	.byte	0xe
	.byte	0xa1
	.byte	0x11
	.long	0x5e
	.uleb128 0x2
	.long	.LASF105
	.byte	0xe
	.byte	0xa6
	.byte	0xe
	.long	0x6f2
	.uleb128 0x2
	.long	.LASF106
	.byte	0xe
	.byte	0xae
	.byte	0xc
	.long	0x57
	.uleb128 0x2
	.long	.LASF107
	.byte	0xe
	.byte	0xaf
	.byte	0x11
	.long	0x5e
	.uleb128 0x14
	.long	.LASF108
	.byte	0xe
	.value	0x112
	.byte	0xc
	.long	0x57
	.uleb128 0x15
	.byte	0x5
	.byte	0x4
	.long	0x57
	.byte	0xf
	.byte	0xb6
	.byte	0xe
	.long	0x97a
	.uleb128 0x16
	.long	.LASF109
	.sleb128 -7
	.uleb128 0x16
	.long	.LASF110
	.sleb128 -13
	.uleb128 0x16
	.long	.LASF111
	.sleb128 -98
	.uleb128 0x16
	.long	.LASF112
	.sleb128 -99
	.uleb128 0x16
	.long	.LASF113
	.sleb128 -97
	.uleb128 0x16
	.long	.LASF114
	.sleb128 -11
	.uleb128 0x16
	.long	.LASF115
	.sleb128 -3000
	.uleb128 0x16
	.long	.LASF116
	.sleb128 -3001
	.uleb128 0x16
	.long	.LASF117
	.sleb128 -3002
	.uleb128 0x16
	.long	.LASF118
	.sleb128 -3013
	.uleb128 0x16
	.long	.LASF119
	.sleb128 -3003
	.uleb128 0x16
	.long	.LASF120
	.sleb128 -3004
	.uleb128 0x16
	.long	.LASF121
	.sleb128 -3005
	.uleb128 0x16
	.long	.LASF122
	.sleb128 -3006
	.uleb128 0x16
	.long	.LASF123
	.sleb128 -3007
	.uleb128 0x16
	.long	.LASF124
	.sleb128 -3008
	.uleb128 0x16
	.long	.LASF125
	.sleb128 -3009
	.uleb128 0x16
	.long	.LASF126
	.sleb128 -3014
	.uleb128 0x16
	.long	.LASF127
	.sleb128 -3010
	.uleb128 0x16
	.long	.LASF128
	.sleb128 -3011
	.uleb128 0x16
	.long	.LASF129
	.sleb128 -114
	.uleb128 0x16
	.long	.LASF130
	.sleb128 -9
	.uleb128 0x16
	.long	.LASF131
	.sleb128 -16
	.uleb128 0x16
	.long	.LASF132
	.sleb128 -125
	.uleb128 0x16
	.long	.LASF133
	.sleb128 -4080
	.uleb128 0x16
	.long	.LASF134
	.sleb128 -103
	.uleb128 0x16
	.long	.LASF135
	.sleb128 -111
	.uleb128 0x16
	.long	.LASF136
	.sleb128 -104
	.uleb128 0x16
	.long	.LASF137
	.sleb128 -89
	.uleb128 0x16
	.long	.LASF138
	.sleb128 -17
	.uleb128 0x16
	.long	.LASF139
	.sleb128 -14
	.uleb128 0x16
	.long	.LASF140
	.sleb128 -27
	.uleb128 0x16
	.long	.LASF141
	.sleb128 -113
	.uleb128 0x16
	.long	.LASF142
	.sleb128 -4
	.uleb128 0x16
	.long	.LASF143
	.sleb128 -22
	.uleb128 0x16
	.long	.LASF144
	.sleb128 -5
	.uleb128 0x16
	.long	.LASF145
	.sleb128 -106
	.uleb128 0x16
	.long	.LASF146
	.sleb128 -21
	.uleb128 0x16
	.long	.LASF147
	.sleb128 -40
	.uleb128 0x16
	.long	.LASF148
	.sleb128 -24
	.uleb128 0x16
	.long	.LASF149
	.sleb128 -90
	.uleb128 0x16
	.long	.LASF150
	.sleb128 -36
	.uleb128 0x16
	.long	.LASF151
	.sleb128 -100
	.uleb128 0x16
	.long	.LASF152
	.sleb128 -101
	.uleb128 0x16
	.long	.LASF153
	.sleb128 -23
	.uleb128 0x16
	.long	.LASF154
	.sleb128 -105
	.uleb128 0x16
	.long	.LASF155
	.sleb128 -19
	.uleb128 0x16
	.long	.LASF156
	.sleb128 -2
	.uleb128 0x16
	.long	.LASF157
	.sleb128 -12
	.uleb128 0x16
	.long	.LASF158
	.sleb128 -64
	.uleb128 0x16
	.long	.LASF159
	.sleb128 -92
	.uleb128 0x16
	.long	.LASF160
	.sleb128 -28
	.uleb128 0x16
	.long	.LASF161
	.sleb128 -38
	.uleb128 0x16
	.long	.LASF162
	.sleb128 -107
	.uleb128 0x16
	.long	.LASF163
	.sleb128 -20
	.uleb128 0x16
	.long	.LASF164
	.sleb128 -39
	.uleb128 0x16
	.long	.LASF165
	.sleb128 -88
	.uleb128 0x16
	.long	.LASF166
	.sleb128 -95
	.uleb128 0x16
	.long	.LASF167
	.sleb128 -1
	.uleb128 0x16
	.long	.LASF168
	.sleb128 -32
	.uleb128 0x16
	.long	.LASF169
	.sleb128 -71
	.uleb128 0x16
	.long	.LASF170
	.sleb128 -93
	.uleb128 0x16
	.long	.LASF171
	.sleb128 -91
	.uleb128 0x16
	.long	.LASF172
	.sleb128 -34
	.uleb128 0x16
	.long	.LASF173
	.sleb128 -30
	.uleb128 0x16
	.long	.LASF174
	.sleb128 -108
	.uleb128 0x16
	.long	.LASF175
	.sleb128 -29
	.uleb128 0x16
	.long	.LASF176
	.sleb128 -3
	.uleb128 0x16
	.long	.LASF177
	.sleb128 -110
	.uleb128 0x16
	.long	.LASF178
	.sleb128 -26
	.uleb128 0x16
	.long	.LASF179
	.sleb128 -18
	.uleb128 0x16
	.long	.LASF180
	.sleb128 -4094
	.uleb128 0x16
	.long	.LASF181
	.sleb128 -4095
	.uleb128 0x16
	.long	.LASF182
	.sleb128 -6
	.uleb128 0x16
	.long	.LASF183
	.sleb128 -31
	.uleb128 0x16
	.long	.LASF184
	.sleb128 -112
	.uleb128 0x16
	.long	.LASF185
	.sleb128 -121
	.uleb128 0x16
	.long	.LASF186
	.sleb128 -25
	.uleb128 0x16
	.long	.LASF187
	.sleb128 -4028
	.uleb128 0x16
	.long	.LASF188
	.sleb128 -84
	.uleb128 0x16
	.long	.LASF189
	.sleb128 -4096
	.byte	0
	.uleb128 0x4
	.byte	0x8
	.byte	0x4
	.long	.LASF190
	.uleb128 0x17
	.long	.LASF195
	.byte	0x1
	.byte	0x4
	.byte	0x9
	.long	0x2d8
	.quad	.LFB54
	.quad	.LFE54-.LFB54
	.uleb128 0x1
	.byte	0x9c
	.uleb128 0x18
	.string	"d"
	.byte	0x1
	.byte	0x4
	.byte	0x1b
	.long	0x39
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x18
	.string	"s"
	.byte	0x1
	.byte	0x4
	.byte	0x2a
	.long	0x2cd
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x19
	.string	"n"
	.byte	0x1
	.byte	0x4
	.byte	0x34
	.long	0x65
	.long	.LLST0
	.long	.LVUS0
	.uleb128 0x1a
	.string	"i"
	.byte	0x1
	.byte	0x5
	.byte	0xa
	.long	0x65
	.long	.LLST1
	.long	.LVUS1
	.byte	0
	.byte	0
	.section	.debug_abbrev,"",@progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1b
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x7
	.uleb128 0x10
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x13
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x13
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3c
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x21
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x37
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0x17
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0x4
	.byte	0x1
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x16
	.uleb128 0x28
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1c
	.uleb128 0xd
	.byte	0
	.byte	0
	.uleb128 0x17
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x7
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2117
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0x18
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x19
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x17
	.uleb128 0x2137
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x1a
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x17
	.uleb128 0x2137
	.uleb128 0x17
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",@progbits
.Ldebug_loc0:
.LVUS0:
	.uleb128 0
	.uleb128 .LVU17
	.uleb128 .LVU17
	.uleb128 .LVU18
	.uleb128 .LVU18
	.uleb128 0
.LLST0:
	.quad	.LVL0-.Ltext0
	.quad	.LVL5-.Ltext0
	.value	0x1
	.byte	0x51
	.quad	.LVL5-.Ltext0
	.quad	.LVL6-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x51
	.byte	0x9f
	.quad	.LVL6-.Ltext0
	.quad	.LFE54-.Ltext0
	.value	0x1
	.byte	0x51
	.quad	0
	.quad	0
.LVUS1:
	.uleb128 .LVU4
	.uleb128 .LVU7
	.uleb128 .LVU8
	.uleb128 .LVU9
	.uleb128 .LVU9
	.uleb128 .LVU11
	.uleb128 .LVU18
	.uleb128 .LVU23
	.uleb128 .LVU24
	.uleb128 0
.LLST1:
	.quad	.LVL1-.Ltext0
	.quad	.LVL2-.Ltext0
	.value	0x2
	.byte	0x30
	.byte	0x9f
	.quad	.LVL2-.Ltext0
	.quad	.LVL3-.Ltext0
	.value	0x3
	.byte	0x70
	.sleb128 1
	.byte	0x9f
	.quad	.LVL3-.Ltext0
	.quad	.LVL4-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	.LVL6-.Ltext0
	.quad	.LVL7-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	.LVL8-.Ltext0
	.quad	.LFE54-.Ltext0
	.value	0x2
	.byte	0x30
	.byte	0x9f
	.quad	0
	.quad	0
	.section	.debug_aranges,"",@progbits
	.long	0x2c
	.value	0x2
	.long	.Ldebug_info0
	.byte	0x8
	.byte	0
	.value	0
	.value	0
	.quad	.Ltext0
	.quad	.Letext0-.Ltext0
	.quad	0
	.quad	0
	.section	.debug_line,"",@progbits
.Ldebug_line0:
	.section	.debug_str,"MS",@progbits,1
.LASF117:
	.string	"UV_EAI_BADFLAGS"
.LASF14:
	.string	"__off_t"
.LASF18:
	.string	"_IO_read_ptr"
.LASF30:
	.string	"_chain"
.LASF143:
	.string	"UV_EINVAL"
.LASF81:
	.string	"sin6_addr"
.LASF97:
	.string	"__in6_u"
.LASF9:
	.string	"size_t"
.LASF113:
	.string	"UV_EAFNOSUPPORT"
.LASF36:
	.string	"_shortbuf"
.LASF10:
	.string	"__uint8_t"
.LASF50:
	.string	"ssize_t"
.LASF150:
	.string	"UV_ENAMETOOLONG"
.LASF147:
	.string	"UV_ELOOP"
.LASF24:
	.string	"_IO_buf_base"
.LASF134:
	.string	"UV_ECONNABORTED"
.LASF61:
	.string	"long long unsigned int"
.LASF170:
	.string	"UV_EPROTONOSUPPORT"
.LASF89:
	.string	"in_addr_t"
.LASF188:
	.string	"UV_EILSEQ"
.LASF60:
	.string	"uint32_t"
.LASF109:
	.string	"UV_E2BIG"
.LASF172:
	.string	"UV_ERANGE"
.LASF84:
	.string	"sockaddr_ipx"
.LASF39:
	.string	"_codecvt"
.LASF152:
	.string	"UV_ENETUNREACH"
.LASF104:
	.string	"__timezone"
.LASF62:
	.string	"long long int"
.LASF8:
	.string	"signed char"
.LASF83:
	.string	"sockaddr_inarp"
.LASF176:
	.string	"UV_ESRCH"
.LASF191:
	.string	"GNU C89 9.3.0 -m64 -mtune=generic -march=x86-64 -g -O3 -std=gnu90 -fvisibility=hidden -fno-omit-frame-pointer -fasynchronous-unwind-tables -fstack-protector-strong -fstack-clash-protection -fcf-protection"
.LASF168:
	.string	"UV_EPIPE"
.LASF68:
	.string	"sockaddr_at"
.LASF31:
	.string	"_fileno"
.LASF181:
	.string	"UV_EOF"
.LASF19:
	.string	"_IO_read_end"
.LASF125:
	.string	"UV_EAI_OVERFLOW"
.LASF175:
	.string	"UV_ESPIPE"
.LASF94:
	.string	"__u6_addr16"
.LASF101:
	.string	"sys_siglist"
.LASF3:
	.string	"long int"
.LASF17:
	.string	"_flags"
.LASF184:
	.string	"UV_EHOSTDOWN"
.LASF16:
	.string	"__ssize_t"
.LASF25:
	.string	"_IO_buf_end"
.LASF51:
	.string	"stdin"
.LASF1:
	.string	"program_invocation_short_name"
.LASF48:
	.string	"_IO_codecvt"
.LASF173:
	.string	"UV_EROFS"
.LASF130:
	.string	"UV_EBADF"
.LASF59:
	.string	"uint16_t"
.LASF57:
	.string	"_sys_errlist"
.LASF142:
	.string	"UV_EINTR"
.LASF0:
	.string	"program_invocation_name"
.LASF33:
	.string	"_old_offset"
.LASF38:
	.string	"_offset"
.LASF99:
	.string	"in6addr_loopback"
.LASF115:
	.string	"UV_EAI_ADDRFAMILY"
.LASF192:
	.string	"../deps/uv/src/strscpy.c"
.LASF88:
	.string	"sockaddr_x25"
.LASF140:
	.string	"UV_EFBIG"
.LASF13:
	.string	"__uint32_t"
.LASF100:
	.string	"_sys_siglist"
.LASF107:
	.string	"timezone"
.LASF154:
	.string	"UV_ENOBUFS"
.LASF76:
	.string	"sin_zero"
.LASF122:
	.string	"UV_EAI_MEMORY"
.LASF180:
	.string	"UV_UNKNOWN"
.LASF47:
	.string	"_IO_marker"
.LASF5:
	.string	"unsigned int"
.LASF91:
	.string	"s_addr"
.LASF42:
	.string	"_freeres_buf"
.LASF177:
	.string	"UV_ETIMEDOUT"
.LASF165:
	.string	"UV_ENOTSOCK"
.LASF4:
	.string	"long unsigned int"
.LASF132:
	.string	"UV_ECANCELED"
.LASF22:
	.string	"_IO_write_ptr"
.LASF160:
	.string	"UV_ENOSPC"
.LASF54:
	.string	"sys_nerr"
.LASF183:
	.string	"UV_EMLINK"
.LASF133:
	.string	"UV_ECHARSET"
.LASF7:
	.string	"short unsigned int"
.LASF75:
	.string	"sin_addr"
.LASF49:
	.string	"_IO_wide_data"
.LASF114:
	.string	"UV_EAGAIN"
.LASF193:
	.string	"/workspace/nodejsbuild/node-v12.18.4/out"
.LASF26:
	.string	"_IO_save_base"
.LASF195:
	.string	"uv__strscpy"
.LASF119:
	.string	"UV_EAI_CANCELED"
.LASF182:
	.string	"UV_ENXIO"
.LASF179:
	.string	"UV_EXDEV"
.LASF37:
	.string	"_lock"
.LASF155:
	.string	"UV_ENODEV"
.LASF95:
	.string	"__u6_addr32"
.LASF92:
	.string	"in_port_t"
.LASF171:
	.string	"UV_EPROTOTYPE"
.LASF52:
	.string	"stdout"
.LASF178:
	.string	"UV_ETXTBSY"
.LASF87:
	.string	"sockaddr_un"
.LASF190:
	.string	"double"
.LASF156:
	.string	"UV_ENOENT"
.LASF73:
	.string	"sin_family"
.LASF123:
	.string	"UV_EAI_NODATA"
.LASF189:
	.string	"UV_ERRNO_MAX"
.LASF144:
	.string	"UV_EIO"
.LASF151:
	.string	"UV_ENETDOWN"
.LASF108:
	.string	"getdate_err"
.LASF78:
	.string	"sin6_family"
.LASF23:
	.string	"_IO_write_end"
.LASF169:
	.string	"UV_EPROTO"
.LASF149:
	.string	"UV_EMSGSIZE"
.LASF162:
	.string	"UV_ENOTCONN"
.LASF161:
	.string	"UV_ENOSYS"
.LASF86:
	.string	"sockaddr_ns"
.LASF90:
	.string	"in_addr"
.LASF64:
	.string	"_IO_FILE"
.LASF146:
	.string	"UV_EISDIR"
.LASF103:
	.string	"__daylight"
.LASF44:
	.string	"_mode"
.LASF74:
	.string	"sin_port"
.LASF66:
	.string	"sa_family"
.LASF55:
	.string	"sys_errlist"
.LASF29:
	.string	"_markers"
.LASF145:
	.string	"UV_EISCONN"
.LASF153:
	.string	"UV_ENFILE"
.LASF141:
	.string	"UV_EHOSTUNREACH"
.LASF131:
	.string	"UV_EBUSY"
.LASF128:
	.string	"UV_EAI_SOCKTYPE"
.LASF82:
	.string	"sin6_scope_id"
.LASF111:
	.string	"UV_EADDRINUSE"
.LASF6:
	.string	"unsigned char"
.LASF85:
	.string	"sockaddr_iso"
.LASF129:
	.string	"UV_EALREADY"
.LASF98:
	.string	"in6addr_any"
.LASF11:
	.string	"short int"
.LASF32:
	.string	"_flags2"
.LASF120:
	.string	"UV_EAI_FAIL"
.LASF187:
	.string	"UV_EFTYPE"
.LASF56:
	.string	"_sys_nerr"
.LASF35:
	.string	"_vtable_offset"
.LASF112:
	.string	"UV_EADDRNOTAVAIL"
.LASF69:
	.string	"sockaddr_ax25"
.LASF46:
	.string	"FILE"
.LASF116:
	.string	"UV_EAI_AGAIN"
.LASF96:
	.string	"in6_addr"
.LASF79:
	.string	"sin6_port"
.LASF186:
	.string	"UV_ENOTTY"
.LASF106:
	.string	"daylight"
.LASF139:
	.string	"UV_EFAULT"
.LASF137:
	.string	"UV_EDESTADDRREQ"
.LASF124:
	.string	"UV_EAI_NONAME"
.LASF2:
	.string	"char"
.LASF136:
	.string	"UV_ECONNRESET"
.LASF80:
	.string	"sin6_flowinfo"
.LASF159:
	.string	"UV_ENOPROTOOPT"
.LASF12:
	.string	"__uint16_t"
.LASF163:
	.string	"UV_ENOTDIR"
.LASF93:
	.string	"__u6_addr8"
.LASF121:
	.string	"UV_EAI_FAMILY"
.LASF194:
	.string	"_IO_lock_t"
.LASF15:
	.string	"__off64_t"
.LASF34:
	.string	"_cur_column"
.LASF20:
	.string	"_IO_read_base"
.LASF126:
	.string	"UV_EAI_PROTOCOL"
.LASF28:
	.string	"_IO_save_end"
.LASF167:
	.string	"UV_EPERM"
.LASF174:
	.string	"UV_ESHUTDOWN"
.LASF158:
	.string	"UV_ENONET"
.LASF71:
	.string	"sockaddr_eon"
.LASF164:
	.string	"UV_ENOTEMPTY"
.LASF43:
	.string	"__pad5"
.LASF63:
	.string	"sa_family_t"
.LASF45:
	.string	"_unused2"
.LASF53:
	.string	"stderr"
.LASF77:
	.string	"sockaddr_in6"
.LASF65:
	.string	"sockaddr"
.LASF72:
	.string	"sockaddr_in"
.LASF58:
	.string	"uint8_t"
.LASF127:
	.string	"UV_EAI_SERVICE"
.LASF27:
	.string	"_IO_backup_base"
.LASF166:
	.string	"UV_ENOTSUP"
.LASF138:
	.string	"UV_EEXIST"
.LASF70:
	.string	"sockaddr_dl"
.LASF185:
	.string	"UV_EREMOTEIO"
.LASF67:
	.string	"sa_data"
.LASF41:
	.string	"_freeres_list"
.LASF40:
	.string	"_wide_data"
.LASF118:
	.string	"UV_EAI_BADHINTS"
.LASF110:
	.string	"UV_EACCES"
.LASF102:
	.string	"__tzname"
.LASF21:
	.string	"_IO_write_base"
.LASF105:
	.string	"tzname"
.LASF148:
	.string	"UV_EMFILE"
.LASF135:
	.string	"UV_ECONNREFUSED"
.LASF157:
	.string	"UV_ENOMEM"
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
