	.file	"linux-inotify.c"
	.text
.Ltext0:
	.p2align 4
	.type	maybe_free_watcher_list.part.0, @function
maybe_free_watcher_list.part.0:
.LVL0:
.LFB118:
	.file 1 "../deps/uv/src/unix/linux-inotify.c"
	.loc 1 157 13 view -0
	.cfi_startproc
	.loc 1 161 5 view .LVU1
.LBB73:
.LBI73:
	.loc 1 58 493 view .LVU2
.LBB74:
	.loc 1 58 571 view .LVU3
	.loc 1 58 620 view .LVU4
	.loc 1 58 631 view .LVU5
.LBE74:
.LBE73:
	.loc 1 157 13 is_stmt 0 view .LVU6
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
.LBB97:
.LBB95:
	.loc 1 58 647 view .LVU7
	movq	(%rdi), %rdx
	movq	8(%rdi), %rcx
	.loc 1 58 634 view .LVU8
	testq	%rdx, %rdx
	je	.L133
	.loc 1 58 40 is_stmt 1 view .LVU9
	.loc 1 58 43 is_stmt 0 view .LVU10
	testq	%rcx, %rcx
	je	.L5
.LVL1:
	.p2align 4,,10
	.p2align 3
.L6:
.LBB75:
	.loc 1 58 104 is_stmt 1 view .LVU11
	movq	%rcx, %rdx
	.loc 1 58 111 is_stmt 0 view .LVU12
	movq	(%rcx), %rcx
.LVL2:
	.loc 1 58 104 view .LVU13
	testq	%rcx, %rcx
	jne	.L6
	.loc 1 58 15 is_stmt 1 view .LVU14
	.loc 1 58 54 is_stmt 0 view .LVU15
	movq	16(%rdx), %rax
	.loc 1 58 21 view .LVU16
	movq	8(%rdx), %rcx
.LVL3:
	.loc 1 58 47 is_stmt 1 view .LVU17
	.loc 1 58 81 view .LVU18
	.loc 1 58 87 is_stmt 0 view .LVU19
	movl	24(%rdx), %edi
.LVL4:
	.loc 1 58 113 is_stmt 1 view .LVU20
	.loc 1 58 54 is_stmt 0 view .LVU21
	movq	%rax, %r8
	.loc 1 58 116 view .LVU22
	testq	%rcx, %rcx
	je	.L7
	.loc 1 58 124 is_stmt 1 view .LVU23
	.loc 1 58 150 is_stmt 0 view .LVU24
	movq	%rax, 16(%rcx)
	movq	16(%rdx), %r8
.L7:
	.loc 1 58 160 is_stmt 1 view .LVU25
	.loc 1 58 163 is_stmt 0 view .LVU26
	testq	%rax, %rax
	je	.L8
	.loc 1 58 174 is_stmt 1 view .LVU27
	.loc 1 58 177 is_stmt 0 view .LVU28
	cmpq	(%rax), %rdx
	je	.L134
	.loc 1 58 250 is_stmt 1 view .LVU29
	.loc 1 58 276 is_stmt 0 view .LVU30
	movq	%rcx, 8(%rax)
.LVL5:
.L10:
	.loc 1 58 335 is_stmt 1 view .LVU31
	.loc 1 58 398 is_stmt 0 view .LVU32
	movdqu	(%r12), %xmm0
	.loc 1 58 338 view .LVU33
	cmpq	%r8, %r12
	cmove	%rdx, %rax
.LVL6:
	.loc 1 58 385 is_stmt 1 view .LVU34
	.loc 1 58 398 is_stmt 0 view .LVU35
	movups	%xmm0, (%rdx)
	movdqu	16(%r12), %xmm1
	movups	%xmm1, 16(%rdx)
	.loc 1 58 414 is_stmt 1 view .LVU36
	.loc 1 58 430 is_stmt 0 view .LVU37
	movq	16(%r12), %r8
	.loc 1 58 417 view .LVU38
	testq	%r8, %r8
	je	.L12
	.loc 1 58 445 is_stmt 1 view .LVU39
	.loc 1 58 448 is_stmt 0 view .LVU40
	cmpq	(%r8), %r12
	je	.L135
	.loc 1 58 553 is_stmt 1 view .LVU41
	.loc 1 58 596 is_stmt 0 view .LVU42
	movq	%rdx, 8(%r8)
.L14:
	.loc 1 58 651 is_stmt 1 view .LVU43
	.loc 1 58 693 is_stmt 0 view .LVU44
	movq	(%r12), %r8
	movq	%rdx, 16(%r8)
	.loc 1 58 700 is_stmt 1 view .LVU45
	.loc 1 58 716 is_stmt 0 view .LVU46
	movq	8(%r12), %r8
	.loc 1 58 703 view .LVU47
	testq	%r8, %r8
	je	.L15
	.loc 1 58 728 is_stmt 1 view .LVU48
	.loc 1 58 771 is_stmt 0 view .LVU49
	movq	%rdx, 16(%r8)
.L15:
	.loc 1 58 778 is_stmt 1 view .LVU50
	.loc 1 58 781 is_stmt 0 view .LVU51
	testq	%rax, %rax
	je	.L16
	movq	%rax, %rdx
.LVL7:
	.p2align 4,,10
	.p2align 3
.L17:
	.loc 1 58 807 is_stmt 1 view .LVU52
	.loc 1 58 812 view .LVU53
	.loc 1 58 816 view .LVU54
	.loc 1 58 824 view .LVU55
	.loc 1 58 837 view .LVU56
	.loc 1 58 844 is_stmt 0 view .LVU57
	movq	16(%rdx), %rdx
.LVL8:
	.loc 1 58 1 view .LVU58
	testq	%rdx, %rdx
	jne	.L17
	jmp	.L16
.LVL9:
	.p2align 4,,10
	.p2align 3
.L5:
	.loc 1 58 1 view .LVU59
.LBE75:
	.loc 1 58 20 is_stmt 1 view .LVU60
	.loc 1 58 27 is_stmt 0 view .LVU61
	movq	16(%rdi), %rax
.LVL10:
	.loc 1 58 54 is_stmt 1 view .LVU62
	.loc 1 58 60 is_stmt 0 view .LVU63
	movl	24(%rdi), %edi
.LVL11:
	.loc 1 58 86 is_stmt 1 view .LVU64
	.loc 1 58 60 is_stmt 0 view .LVU65
	movq	%rdx, %rcx
.LVL12:
.L3:
	.loc 1 58 97 is_stmt 1 view .LVU66
	.loc 1 58 123 is_stmt 0 view .LVU67
	movq	%rax, 16(%rcx)
	.loc 1 58 133 is_stmt 1 view .LVU68
	.loc 1 58 136 is_stmt 0 view .LVU69
	testq	%rax, %rax
	je	.L18
.LVL13:
.L142:
	.loc 1 58 147 is_stmt 1 view .LVU70
	.loc 1 58 150 is_stmt 0 view .LVU71
	cmpq	(%rax), %r12
	je	.L136
	.loc 1 58 223 is_stmt 1 view .LVU72
	.loc 1 58 249 is_stmt 0 view .LVU73
	movq	%rcx, 8(%rax)
.LVL14:
.L16:
	.loc 1 58 315 is_stmt 1 view .LVU74
	.loc 1 58 318 is_stmt 0 view .LVU75
	testl	%edi, %edi
	je	.L20
.LVL15:
.L21:
	.loc 1 58 382 is_stmt 1 view .LVU76
	.loc 1 58 382 is_stmt 0 view .LVU77
.LBE95:
.LBE97:
	.loc 1 162 5 is_stmt 1 view .LVU78
	movl	64(%r12), %r8d
	movl	840(%rsi), %edi
	movl	%r8d, %esi
.LVL16:
	.loc 1 162 5 is_stmt 0 view .LVU79
	call	inotify_rm_watch@PLT
.LVL17:
	.loc 1 163 5 is_stmt 1 view .LVU80
	.loc 1 165 1 is_stmt 0 view .LVU81
	addq	$8, %rsp
	.loc 1 163 5 view .LVU82
	movq	%r12, %rdi
	.loc 1 165 1 view .LVU83
	popq	%r12
.LVL18:
	.loc 1 165 1 view .LVU84
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	.loc 1 163 5 view .LVU85
	jmp	uv__free@PLT
.LVL19:
	.p2align 4,,10
	.p2align 3
.L22:
	.cfi_restore_state
.LBB98:
.LBB96:
.LBB76:
.LBB77:
	.loc 1 58 405 is_stmt 1 view .LVU86
	.loc 1 58 437 view .LVU87
	.loc 1 58 440 is_stmt 0 view .LVU88
	cmpl	$1, 24(%rdx)
	je	.L137
.LVL20:
.L43:
	.loc 1 58 398 is_stmt 1 view .LVU89
	.loc 1 58 415 is_stmt 0 view .LVU90
	movq	(%rdx), %rdi
	.loc 1 58 401 view .LVU91
	testq	%rdi, %rdi
	je	.L48
	.loc 1 58 1 view .LVU92
	movl	24(%rdi), %ecx
	testl	%ecx, %ecx
	jne	.L49
.L48:
	.loc 1 58 67 view .LVU93
	movq	8(%rdx), %rcx
	.loc 1 58 51 view .LVU94
	testq	%rcx, %rcx
	je	.L50
	.loc 1 58 1 view .LVU95
	movl	24(%rcx), %r11d
	testl	%r11d, %r11d
	jne	.L51
.L50:
	.loc 1 58 55 is_stmt 1 view .LVU96
	.loc 1 58 78 is_stmt 0 view .LVU97
	movl	$1, 24(%rdx)
	.loc 1 58 83 is_stmt 1 view .LVU98
.LVL21:
	.loc 1 58 97 view .LVU99
	.loc 1 58 104 is_stmt 0 view .LVU100
	movq	16(%rax), %rdx
.LVL22:
	.loc 1 58 368 view .LVU101
	movq	%rax, %rcx
	movq	%rdx, %rax
.LVL23:
.L20:
	.loc 1 58 595 is_stmt 1 view .LVU102
	testq	%rcx, %rcx
	je	.L62
	.loc 1 58 1 is_stmt 0 view .LVU103
	movl	24(%rcx), %r10d
	testl	%r10d, %r10d
	jne	.L72
.L62:
	.loc 1 58 49 view .LVU104
	movq	832(%rsi), %rdx
	.loc 1 58 33 view .LVU105
	cmpq	%rdx, %rcx
	je	.L42
	.loc 1 58 63 is_stmt 1 view .LVU106
	.loc 1 58 82 is_stmt 0 view .LVU107
	movq	(%rax), %rdx
	.loc 1 58 66 view .LVU108
	cmpq	%rcx, %rdx
	jne	.L22
	.loc 1 58 102 is_stmt 1 view .LVU109
	.loc 1 58 106 is_stmt 0 view .LVU110
	movq	8(%rax), %rdx
.LVL24:
	.loc 1 58 135 is_stmt 1 view .LVU111
	.loc 1 58 138 is_stmt 0 view .LVU112
	cmpl	$1, 24(%rdx)
	movq	(%rdx), %rcx
.LVL25:
	.loc 1 58 138 view .LVU113
	je	.L138
.LVL26:
.L23:
	.loc 1 58 398 is_stmt 1 view .LVU114
	.loc 1 58 401 is_stmt 0 view .LVU115
	testq	%rcx, %rcx
	je	.L28
	.loc 1 58 1 view .LVU116
	movl	24(%rcx), %r9d
	testl	%r9d, %r9d
	jne	.L29
.L28:
	.loc 1 58 67 view .LVU117
	movq	8(%rdx), %rdi
	.loc 1 58 51 view .LVU118
	testq	%rdi, %rdi
	je	.L50
	.loc 1 58 1 view .LVU119
	movl	24(%rdi), %r8d
	testl	%r8d, %r8d
	je	.L50
.L31:
	.loc 1 58 393 is_stmt 1 view .LVU120
	.loc 1 58 433 is_stmt 0 view .LVU121
	movl	24(%rax), %r8d
	.loc 1 58 416 view .LVU122
	movl	%r8d, 24(%rdx)
	.loc 1 58 445 is_stmt 1 view .LVU123
	.loc 1 58 471 is_stmt 0 view .LVU124
	movl	$0, 24(%rax)
	.loc 1 58 476 is_stmt 1 view .LVU125
.L69:
	.loc 1 58 504 view .LVU126
	.loc 1 58 546 is_stmt 0 view .LVU127
	movl	$0, 24(%rdi)
.L37:
	.loc 1 58 551 is_stmt 1 view .LVU128
	.loc 1 58 556 view .LVU129
.LVL27:
	.loc 1 58 591 view .LVU130
	.loc 1 58 622 is_stmt 0 view .LVU131
	movq	%rcx, 8(%rax)
.LVL28:
	.loc 1 58 594 view .LVU132
	testq	%rcx, %rcx
	je	.L38
	.loc 1 58 5 is_stmt 1 view .LVU133
	.loc 1 58 47 is_stmt 0 view .LVU134
	movq	%rax, 16(%rcx)
.L38:
	.loc 1 58 61 is_stmt 1 view .LVU135
	.loc 1 58 65 view .LVU136
	.loc 1 58 73 view .LVU137
	.loc 1 58 78 view .LVU138
	.loc 1 58 124 is_stmt 0 view .LVU139
	movq	16(%rax), %rcx
	.loc 1 58 107 view .LVU140
	movq	%rcx, 16(%rdx)
	.loc 1 58 81 view .LVU141
	testq	%rcx, %rcx
	je	.L39
	.loc 1 58 5 is_stmt 1 view .LVU142
	.loc 1 58 8 is_stmt 0 view .LVU143
	cmpq	(%rcx), %rax
	je	.L139
	.loc 1 58 126 is_stmt 1 view .LVU144
	.loc 1 58 172 is_stmt 0 view .LVU145
	movq	%rdx, 8(%rcx)
.L41:
	.loc 1 58 214 is_stmt 1 view .LVU146
	.loc 1 58 236 is_stmt 0 view .LVU147
	movq	%rax, (%rdx)
	.loc 1 58 248 is_stmt 1 view .LVU148
	.loc 1 58 275 is_stmt 0 view .LVU149
	movq	%rdx, 16(%rax)
	.loc 1 58 284 is_stmt 1 view .LVU150
	.loc 1 58 288 view .LVU151
	.loc 1 58 296 view .LVU152
	.loc 1 58 301 view .LVU153
	.loc 1 58 332 view .LVU154
	.loc 1 58 336 view .LVU155
	.loc 1 58 344 view .LVU156
	.loc 1 58 357 view .LVU157
	.loc 1 58 363 view .LVU158
	.loc 1 58 367 is_stmt 0 view .LVU159
	movq	832(%rsi), %rdx
.LVL29:
	.loc 1 58 387 is_stmt 1 view .LVU160
.L42:
	.loc 1 58 401 view .LVU161
	.loc 1 58 404 is_stmt 0 view .LVU162
	testq	%rdx, %rdx
	je	.L21
.L63:
	.loc 1 58 410 is_stmt 1 view .LVU163
	.loc 1 58 433 is_stmt 0 view .LVU164
	movl	$0, 24(%rdx)
	jmp	.L21
.LVL30:
	.p2align 4,,10
	.p2align 3
.L137:
	.loc 1 58 472 is_stmt 1 view .LVU165
	.loc 1 58 477 view .LVU166
	.loc 1 58 633 is_stmt 0 view .LVU167
	movq	8(%rdx), %rcx
.LVL31:
	.loc 1 58 500 view .LVU168
	movl	$0, 24(%rdx)
	.loc 1 58 505 is_stmt 1 view .LVU169
	.loc 1 58 531 is_stmt 0 view .LVU170
	movl	$1, 24(%rax)
	.loc 1 58 544 is_stmt 1 view .LVU171
	.loc 1 58 550 view .LVU172
	.loc 1 58 555 view .LVU173
	.loc 1 58 589 view .LVU174
	.loc 1 58 619 is_stmt 0 view .LVU175
	movq	%rcx, (%rax)
	.loc 1 58 592 view .LVU176
	testq	%rcx, %rcx
	je	.L44
	.loc 1 58 5 is_stmt 1 view .LVU177
	.loc 1 58 48 is_stmt 0 view .LVU178
	movq	%rax, 16(%rcx)
.L44:
	.loc 1 58 62 is_stmt 1 view .LVU179
	.loc 1 58 66 view .LVU180
	.loc 1 58 74 view .LVU181
	.loc 1 58 79 view .LVU182
	.loc 1 58 125 is_stmt 0 view .LVU183
	movq	16(%rax), %rdi
	.loc 1 58 108 view .LVU184
	movq	%rdi, 16(%rdx)
	.loc 1 58 82 view .LVU185
	testq	%rdi, %rdi
	je	.L45
	.loc 1 58 5 is_stmt 1 view .LVU186
	.loc 1 58 56 is_stmt 0 view .LVU187
	movq	16(%rax), %r8
	.loc 1 58 8 view .LVU188
	cmpq	(%r8), %rax
	je	.L140
	.loc 1 58 126 is_stmt 1 view .LVU189
	.loc 1 58 172 is_stmt 0 view .LVU190
	movq	%rdx, 8(%rdi)
.L47:
	.loc 1 58 214 is_stmt 1 view .LVU191
	.loc 1 58 237 is_stmt 0 view .LVU192
	movq	%rax, 8(%rdx)
	.loc 1 58 249 is_stmt 1 view .LVU193
	.loc 1 58 276 is_stmt 0 view .LVU194
	movq	%rdx, 16(%rax)
	.loc 1 58 285 is_stmt 1 view .LVU195
	.loc 1 58 289 view .LVU196
	.loc 1 58 297 view .LVU197
	.loc 1 58 302 view .LVU198
	.loc 1 58 333 view .LVU199
	.loc 1 58 337 view .LVU200
	.loc 1 58 345 view .LVU201
	.loc 1 58 358 view .LVU202
	.loc 1 58 364 view .LVU203
.LVL32:
	.loc 1 58 368 is_stmt 0 view .LVU204
	movq	%rcx, %rdx
.LVL33:
	.loc 1 58 368 view .LVU205
	jmp	.L43
.LVL34:
	.p2align 4,,10
	.p2align 3
.L138:
	.loc 1 58 170 is_stmt 1 view .LVU206
	.loc 1 58 175 view .LVU207
	.loc 1 58 198 is_stmt 0 view .LVU208
	movl	$0, 24(%rdx)
	.loc 1 58 203 is_stmt 1 view .LVU209
	.loc 1 58 229 is_stmt 0 view .LVU210
	movl	$1, 24(%rax)
	.loc 1 58 242 is_stmt 1 view .LVU211
	.loc 1 58 248 view .LVU212
	.loc 1 58 253 view .LVU213
	.loc 1 58 288 view .LVU214
	.loc 1 58 319 is_stmt 0 view .LVU215
	movq	%rcx, 8(%rax)
	.loc 1 58 291 view .LVU216
	testq	%rcx, %rcx
	je	.L24
	.loc 1 58 5 is_stmt 1 view .LVU217
	.loc 1 58 47 is_stmt 0 view .LVU218
	movq	%rax, 16(%rcx)
.L24:
	.loc 1 58 61 is_stmt 1 view .LVU219
	.loc 1 58 65 view .LVU220
	.loc 1 58 73 view .LVU221
	.loc 1 58 78 view .LVU222
	.loc 1 58 124 is_stmt 0 view .LVU223
	movq	16(%rax), %rdi
	.loc 1 58 107 view .LVU224
	movq	%rdi, 16(%rdx)
	.loc 1 58 81 view .LVU225
	testq	%rdi, %rdi
	je	.L25
	.loc 1 58 5 is_stmt 1 view .LVU226
	.loc 1 58 8 is_stmt 0 view .LVU227
	cmpq	(%rdi), %rax
	je	.L141
	.loc 1 58 126 is_stmt 1 view .LVU228
	.loc 1 58 172 is_stmt 0 view .LVU229
	movq	%rdx, 8(%rdi)
	movq	8(%rax), %rdi
.L27:
	.loc 1 58 214 is_stmt 1 view .LVU230
	.loc 1 58 236 is_stmt 0 view .LVU231
	movq	%rax, (%rdx)
	.loc 1 58 248 is_stmt 1 view .LVU232
	movq	(%rdi), %rcx
	.loc 1 58 275 is_stmt 0 view .LVU233
	movq	%rdx, 16(%rax)
	.loc 1 58 284 is_stmt 1 view .LVU234
	.loc 1 58 288 view .LVU235
	.loc 1 58 296 view .LVU236
	.loc 1 58 301 view .LVU237
	.loc 1 58 332 view .LVU238
	.loc 1 58 336 view .LVU239
	.loc 1 58 344 view .LVU240
	.loc 1 58 357 view .LVU241
	.loc 1 58 363 view .LVU242
.LVL35:
	.loc 1 58 275 is_stmt 0 view .LVU243
	movq	%rdi, %rdx
.LVL36:
	.loc 1 58 275 view .LVU244
	jmp	.L23
.LVL37:
	.p2align 4,,10
	.p2align 3
.L45:
	.loc 1 58 188 is_stmt 1 view .LVU245
	.loc 1 58 205 is_stmt 0 view .LVU246
	movq	%rdx, 832(%rsi)
	movq	(%rax), %rcx
	jmp	.L47
.LVL38:
	.p2align 4,,10
	.p2align 3
.L133:
	.loc 1 58 205 view .LVU247
.LBE77:
.LBE76:
	.loc 1 58 3 is_stmt 1 view .LVU248
	.loc 1 58 20 view .LVU249
	.loc 1 58 27 is_stmt 0 view .LVU250
	movq	16(%rdi), %rax
.LVL39:
	.loc 1 58 54 is_stmt 1 view .LVU251
	.loc 1 58 60 is_stmt 0 view .LVU252
	movl	24(%rdi), %edi
.LVL40:
	.loc 1 58 86 is_stmt 1 view .LVU253
	.loc 1 58 89 is_stmt 0 view .LVU254
	testq	%rcx, %rcx
	jne	.L3
	.loc 1 58 133 is_stmt 1 view .LVU255
	.loc 1 58 136 is_stmt 0 view .LVU256
	testq	%rax, %rax
	jne	.L142
.LVL41:
.L18:
	.loc 1 58 282 is_stmt 1 view .LVU257
	.loc 1 58 299 is_stmt 0 view .LVU258
	movq	%rcx, 832(%rsi)
	jmp	.L16
.LVL42:
	.p2align 4,,10
	.p2align 3
.L140:
.LBB88:
.LBB84:
	.loc 1 58 67 is_stmt 1 view .LVU259
	.loc 1 58 112 is_stmt 0 view .LVU260
	movq	%rdx, (%rdi)
	movq	(%rax), %rcx
	jmp	.L47
.LVL43:
	.p2align 4,,10
	.p2align 3
.L134:
	.loc 1 58 112 view .LVU261
.LBE84:
.LBE88:
.LBB89:
	.loc 1 58 211 is_stmt 1 view .LVU262
	.loc 1 58 236 is_stmt 0 view .LVU263
	movq	%rcx, (%rax)
.LVL44:
	.loc 1 58 236 view .LVU264
	jmp	.L10
.LVL45:
	.p2align 4,,10
	.p2align 3
.L25:
	.loc 1 58 236 view .LVU265
.LBE89:
.LBB90:
.LBB85:
	.loc 1 58 188 is_stmt 1 view .LVU266
	.loc 1 58 205 is_stmt 0 view .LVU267
	movq	%rdx, 832(%rsi)
	movq	8(%rax), %rdi
	jmp	.L27
.LVL46:
	.p2align 4,,10
	.p2align 3
.L136:
	.loc 1 58 205 view .LVU268
.LBE85:
.LBE90:
	.loc 1 58 184 is_stmt 1 view .LVU269
	.loc 1 58 209 is_stmt 0 view .LVU270
	movq	%rcx, (%rax)
	jmp	.L16
.LVL47:
	.p2align 4,,10
	.p2align 3
.L12:
.LBB91:
	.loc 1 58 627 is_stmt 1 view .LVU271
	.loc 1 58 644 is_stmt 0 view .LVU272
	movq	%rdx, 832(%rsi)
	jmp	.L14
.LVL48:
	.p2align 4,,10
	.p2align 3
.L8:
	.loc 1 58 309 is_stmt 1 view .LVU273
	.loc 1 58 326 is_stmt 0 view .LVU274
	movq	%rcx, 832(%rsi)
	jmp	.L10
.LVL49:
	.p2align 4,,10
	.p2align 3
.L141:
	.loc 1 58 326 view .LVU275
.LBE91:
.LBB92:
.LBB86:
	.loc 1 58 67 is_stmt 1 view .LVU276
	.loc 1 58 112 is_stmt 0 view .LVU277
	movq	%rdx, (%rdi)
	movq	%rcx, %rdi
	jmp	.L27
.LVL50:
	.p2align 4,,10
	.p2align 3
.L135:
	.loc 1 58 112 view .LVU278
.LBE86:
.LBE92:
.LBB93:
	.loc 1 58 499 is_stmt 1 view .LVU279
	.loc 1 58 541 is_stmt 0 view .LVU280
	movq	%rdx, (%r8)
	jmp	.L14
.LVL51:
	.p2align 4,,10
	.p2align 3
.L72:
	.loc 1 58 541 view .LVU281
.LBE93:
.LBB94:
.LBB87:
	movq	%rcx, %rdx
	jmp	.L63
.LVL52:
	.p2align 4,,10
	.p2align 3
.L51:
	.loc 1 58 140 is_stmt 1 view .LVU282
	.loc 1 58 143 is_stmt 0 view .LVU283
	testq	%rdi, %rdi
	je	.L68
	.loc 1 58 1 view .LVU284
	movl	24(%rdi), %r8d
	testl	%r8d, %r8d
	je	.L68
	.p2align 4,,10
	.p2align 3
.L49:
	.loc 1 58 397 is_stmt 1 view .LVU285
	.loc 1 58 437 is_stmt 0 view .LVU286
	movl	24(%rax), %ecx
	.loc 1 58 420 view .LVU287
	movl	%ecx, 24(%rdx)
	.loc 1 58 449 is_stmt 1 view .LVU288
	.loc 1 58 475 is_stmt 0 view .LVU289
	movl	$0, 24(%rax)
	.loc 1 58 480 is_stmt 1 view .LVU290
.L70:
	.loc 1 58 507 view .LVU291
	.loc 1 58 548 is_stmt 0 view .LVU292
	movl	$0, 24(%rdi)
.L57:
	.loc 1 58 553 is_stmt 1 view .LVU293
	.loc 1 58 558 view .LVU294
.LVL53:
	.loc 1 58 592 view .LVU295
	.loc 1 58 636 is_stmt 0 view .LVU296
	movq	8(%rdx), %rcx
	.loc 1 58 622 view .LVU297
	movq	%rcx, (%rax)
.LVL54:
	.loc 1 58 595 view .LVU298
	testq	%rcx, %rcx
	je	.L58
	.loc 1 58 5 is_stmt 1 view .LVU299
	.loc 1 58 48 is_stmt 0 view .LVU300
	movq	%rax, 16(%rcx)
.L58:
	.loc 1 58 62 is_stmt 1 view .LVU301
	.loc 1 58 66 view .LVU302
	.loc 1 58 74 view .LVU303
	.loc 1 58 79 view .LVU304
	.loc 1 58 125 is_stmt 0 view .LVU305
	movq	16(%rax), %rcx
	.loc 1 58 108 view .LVU306
	movq	%rcx, 16(%rdx)
	.loc 1 58 82 view .LVU307
	testq	%rcx, %rcx
	je	.L59
	.loc 1 58 5 is_stmt 1 view .LVU308
	.loc 1 58 56 is_stmt 0 view .LVU309
	movq	16(%rax), %rdi
	.loc 1 58 8 view .LVU310
	cmpq	(%rdi), %rax
	je	.L143
	.loc 1 58 126 is_stmt 1 view .LVU311
	.loc 1 58 172 is_stmt 0 view .LVU312
	movq	%rdx, 8(%rcx)
.L61:
	.loc 1 58 214 is_stmt 1 view .LVU313
	.loc 1 58 237 is_stmt 0 view .LVU314
	movq	%rax, 8(%rdx)
	.loc 1 58 249 is_stmt 1 view .LVU315
	.loc 1 58 276 is_stmt 0 view .LVU316
	movq	%rdx, 16(%rax)
	.loc 1 58 285 is_stmt 1 view .LVU317
	.loc 1 58 289 view .LVU318
	.loc 1 58 297 view .LVU319
	.loc 1 58 302 view .LVU320
	.loc 1 58 333 view .LVU321
	.loc 1 58 337 view .LVU322
	.loc 1 58 345 view .LVU323
	.loc 1 58 358 view .LVU324
	.loc 1 58 364 view .LVU325
	.loc 1 58 368 is_stmt 0 view .LVU326
	movq	832(%rsi), %rdx
.LVL55:
	.loc 1 58 388 is_stmt 1 view .LVU327
	.loc 1 58 401 view .LVU328
	.loc 1 58 404 is_stmt 0 view .LVU329
	testq	%rdx, %rdx
	jne	.L63
	.loc 1 58 404 view .LVU330
	jmp	.L21
.LVL56:
	.p2align 4,,10
	.p2align 3
.L68:
.LBB78:
	.loc 1 58 3 is_stmt 1 view .LVU331
	.loc 1 58 147 is_stmt 0 view .LVU332
	movq	(%rcx), %rdi
	.loc 1 58 29 view .LVU333
	movl	$0, 24(%rcx)
	.loc 1 58 34 is_stmt 1 view .LVU334
	.loc 1 58 57 is_stmt 0 view .LVU335
	movl	$1, 24(%rdx)
	.loc 1 58 62 is_stmt 1 view .LVU336
	.loc 1 58 67 view .LVU337
	.loc 1 58 102 view .LVU338
	.loc 1 58 130 is_stmt 0 view .LVU339
	movq	%rdi, 8(%rdx)
	.loc 1 58 105 view .LVU340
	testq	%rdi, %rdi
	je	.L53
	.loc 1 58 5 is_stmt 1 view .LVU341
	.loc 1 58 50 is_stmt 0 view .LVU342
	movq	%rdx, 16(%rdi)
.L53:
	.loc 1 58 61 is_stmt 1 view .LVU343
	.loc 1 58 65 view .LVU344
	.loc 1 58 73 view .LVU345
	.loc 1 58 78 view .LVU346
	.loc 1 58 124 is_stmt 0 view .LVU347
	movq	16(%rdx), %rdi
	.loc 1 58 110 view .LVU348
	movq	%rdi, 16(%rcx)
	.loc 1 58 81 view .LVU349
	testq	%rdi, %rdi
	je	.L54
	.loc 1 58 5 is_stmt 1 view .LVU350
	.loc 1 58 8 is_stmt 0 view .LVU351
	cmpq	(%rdi), %rdx
	je	.L144
	.loc 1 58 120 is_stmt 1 view .LVU352
	.loc 1 58 163 is_stmt 0 view .LVU353
	movq	%rcx, 8(%rdi)
.L56:
	.loc 1 58 211 is_stmt 1 view .LVU354
	.loc 1 58 236 is_stmt 0 view .LVU355
	movq	%rdx, (%rcx)
	.loc 1 58 245 is_stmt 1 view .LVU356
	.loc 1 58 269 is_stmt 0 view .LVU357
	movq	%rcx, 16(%rdx)
	.loc 1 58 281 is_stmt 1 view .LVU358
	.loc 1 58 285 view .LVU359
	.loc 1 58 293 view .LVU360
	.loc 1 58 298 view .LVU361
	.loc 1 58 332 view .LVU362
	.loc 1 58 336 view .LVU363
	.loc 1 58 344 view .LVU364
	.loc 1 58 357 view .LVU365
	.loc 1 58 363 view .LVU366
	.loc 1 58 367 is_stmt 0 view .LVU367
	movq	(%rax), %rdx
.LVL57:
	.loc 1 58 367 view .LVU368
.LBE78:
	.loc 1 58 437 view .LVU369
	movl	24(%rax), %ecx
.LVL58:
	.loc 1 58 437 view .LVU370
	movq	(%rdx), %rdi
	.loc 1 58 397 is_stmt 1 view .LVU371
	.loc 1 58 420 is_stmt 0 view .LVU372
	movl	%ecx, 24(%rdx)
	.loc 1 58 449 is_stmt 1 view .LVU373
	.loc 1 58 475 is_stmt 0 view .LVU374
	movl	$0, 24(%rax)
	.loc 1 58 480 is_stmt 1 view .LVU375
	.loc 1 58 483 is_stmt 0 view .LVU376
	testq	%rdi, %rdi
	je	.L57
	.loc 1 58 483 view .LVU377
	jmp	.L70
.LVL59:
	.p2align 4,,10
	.p2align 3
.L54:
.LBB79:
	.loc 1 58 182 is_stmt 1 view .LVU378
	.loc 1 58 199 is_stmt 0 view .LVU379
	movq	%rcx, 832(%rsi)
	jmp	.L56
.LVL60:
	.p2align 4,,10
	.p2align 3
.L59:
	.loc 1 58 199 view .LVU380
.LBE79:
	.loc 1 58 188 is_stmt 1 view .LVU381
	.loc 1 58 205 is_stmt 0 view .LVU382
	movq	%rdx, 832(%rsi)
	jmp	.L61
.LVL61:
	.p2align 4,,10
	.p2align 3
.L29:
	.loc 1 58 140 is_stmt 1 view .LVU383
	.loc 1 58 156 is_stmt 0 view .LVU384
	movq	8(%rdx), %rdi
	.loc 1 58 143 view .LVU385
	testq	%rdi, %rdi
	je	.L66
	.loc 1 58 1 view .LVU386
	movl	24(%rdi), %r9d
	testl	%r9d, %r9d
	jne	.L31
.L66:
.LVL62:
.LBB80:
	.loc 1 58 3 is_stmt 1 view .LVU387
	.loc 1 58 142 is_stmt 0 view .LVU388
	movq	8(%rcx), %rdi
	.loc 1 58 28 view .LVU389
	movl	$0, 24(%rcx)
	.loc 1 58 33 is_stmt 1 view .LVU390
	.loc 1 58 56 is_stmt 0 view .LVU391
	movl	$1, 24(%rdx)
	.loc 1 58 61 is_stmt 1 view .LVU392
	.loc 1 58 66 view .LVU393
	.loc 1 58 99 view .LVU394
	.loc 1 58 126 is_stmt 0 view .LVU395
	movq	%rdi, (%rdx)
.LVL63:
	.loc 1 58 102 view .LVU396
	testq	%rdi, %rdi
	je	.L33
	.loc 1 58 5 is_stmt 1 view .LVU397
	.loc 1 58 50 is_stmt 0 view .LVU398
	movq	%rdx, 16(%rdi)
.L33:
	.loc 1 58 61 is_stmt 1 view .LVU399
	.loc 1 58 65 view .LVU400
	.loc 1 58 73 view .LVU401
	.loc 1 58 78 view .LVU402
	.loc 1 58 123 is_stmt 0 view .LVU403
	movq	16(%rdx), %rdi
	.loc 1 58 109 view .LVU404
	movq	%rdi, 16(%rcx)
	.loc 1 58 81 view .LVU405
	testq	%rdi, %rdi
	je	.L34
	.loc 1 58 5 is_stmt 1 view .LVU406
	.loc 1 58 8 is_stmt 0 view .LVU407
	cmpq	(%rdi), %rdx
	je	.L145
	.loc 1 58 119 is_stmt 1 view .LVU408
	.loc 1 58 162 is_stmt 0 view .LVU409
	movq	%rcx, 8(%rdi)
.L36:
	.loc 1 58 208 is_stmt 1 view .LVU410
	.loc 1 58 233 is_stmt 0 view .LVU411
	movq	%rdx, 8(%rcx)
	.loc 1 58 242 is_stmt 1 view .LVU412
.LBE80:
	.loc 1 58 433 is_stmt 0 view .LVU413
	movl	24(%rax), %r8d
.LBB81:
	.loc 1 58 266 view .LVU414
	movq	%rcx, 16(%rdx)
	.loc 1 58 277 is_stmt 1 view .LVU415
	.loc 1 58 281 view .LVU416
	.loc 1 58 289 view .LVU417
	.loc 1 58 294 view .LVU418
	.loc 1 58 327 view .LVU419
	.loc 1 58 331 view .LVU420
	.loc 1 58 339 view .LVU421
	.loc 1 58 352 view .LVU422
	.loc 1 58 358 view .LVU423
	.loc 1 58 362 is_stmt 0 view .LVU424
	movq	8(%rax), %rdx
.LVL64:
	.loc 1 58 362 view .LVU425
	movq	8(%rdx), %rdi
	movq	(%rdx), %rcx
	.loc 1 58 362 view .LVU426
.LBE81:
	.loc 1 58 393 is_stmt 1 view .LVU427
	.loc 1 58 416 is_stmt 0 view .LVU428
	movl	%r8d, 24(%rdx)
	.loc 1 58 445 is_stmt 1 view .LVU429
	.loc 1 58 471 is_stmt 0 view .LVU430
	movl	$0, 24(%rax)
	.loc 1 58 476 is_stmt 1 view .LVU431
	.loc 1 58 479 is_stmt 0 view .LVU432
	testq	%rdi, %rdi
	je	.L37
	jmp	.L69
.LVL65:
	.p2align 4,,10
	.p2align 3
.L143:
	.loc 1 58 67 is_stmt 1 view .LVU433
	.loc 1 58 112 is_stmt 0 view .LVU434
	movq	%rdx, (%rcx)
	jmp	.L61
.LVL66:
	.p2align 4,,10
	.p2align 3
.L144:
.LBB82:
	.loc 1 58 61 is_stmt 1 view .LVU435
	.loc 1 58 103 is_stmt 0 view .LVU436
	movq	%rcx, (%rdi)
	jmp	.L56
.LVL67:
	.p2align 4,,10
	.p2align 3
.L39:
	.loc 1 58 103 view .LVU437
.LBE82:
	.loc 1 58 188 is_stmt 1 view .LVU438
	.loc 1 58 205 is_stmt 0 view .LVU439
	movq	%rdx, 832(%rsi)
	jmp	.L41
.L139:
	.loc 1 58 67 is_stmt 1 view .LVU440
	.loc 1 58 112 is_stmt 0 view .LVU441
	movq	%rdx, (%rcx)
	jmp	.L41
.LVL68:
.L34:
.LBB83:
	.loc 1 58 180 is_stmt 1 view .LVU442
	.loc 1 58 197 is_stmt 0 view .LVU443
	movq	%rcx, 832(%rsi)
	jmp	.L36
.L145:
	.loc 1 58 61 is_stmt 1 view .LVU444
	.loc 1 58 103 is_stmt 0 view .LVU445
	movq	%rcx, (%rdi)
	jmp	.L36
.LBE83:
.LBE87:
.LBE94:
.LBE96:
.LBE98:
	.cfi_endproc
.LFE118:
	.size	maybe_free_watcher_list.part.0, .-maybe_free_watcher_list.part.0
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/uv/src/unix/linux-inotify.c"
	.align 8
.LC1:
	.string	"errno == EAGAIN || errno == EWOULDBLOCK"
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC2:
	.string	"size > 0"
	.text
	.p2align 4
	.type	uv__inotify_read, @function
uv__inotify_read:
.LVL69:
.LFB108:
	.loc 1 169 51 is_stmt 1 view -0
	.cfi_startproc
	.loc 1 169 51 is_stmt 0 view .LVU447
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	.loc 1 169 51 view .LVU448
	movq	%rdi, -4192(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-4160(%rbp), %rax
	movq	%rax, -4208(%rbp)
.LBB99:
	.loc 1 225 409 view .LVU449
	leaq	-4176(%rbp), %rax
	movq	%rax, -4184(%rbp)
.LVL70:
	.p2align 4,,10
	.p2align 3
.L147:
	.loc 1 225 409 view .LVU450
.LBE99:
	.loc 1 170 3 is_stmt 1 discriminator 2 view .LVU451
	.loc 1 171 3 discriminator 2 view .LVU452
	.loc 1 172 3 discriminator 2 view .LVU453
	.loc 1 173 3 discriminator 2 view .LVU454
	.loc 1 174 3 discriminator 2 view .LVU455
	.loc 1 175 3 discriminator 2 view .LVU456
	.loc 1 176 3 discriminator 2 view .LVU457
	.loc 1 177 3 discriminator 2 view .LVU458
	.loc 1 179 3 discriminator 2 view .LVU459
	.loc 1 181 3 discriminator 2 view .LVU460
	.loc 1 182 5 discriminator 2 view .LVU461
	.loc 1 183 7 discriminator 2 view .LVU462
.LBB100:
.LBI100:
	.file 2 "/usr/include/x86_64-linux-gnu/bits/unistd.h"
	.loc 2 34 1 discriminator 2 view .LVU463
.LBB101:
	.loc 2 36 3 discriminator 2 view .LVU464
	.loc 2 38 7 discriminator 2 view .LVU465
	.loc 2 41 7 discriminator 2 view .LVU466
	.loc 2 44 3 discriminator 2 view .LVU467
	.loc 2 44 10 is_stmt 0 discriminator 2 view .LVU468
	movq	-4192(%rbp), %rax
	movq	-4208(%rbp), %rsi
	movl	$4096, %edx
	movl	840(%rax), %edi
	call	read@PLT
.LVL71:
	.loc 2 44 10 discriminator 2 view .LVU469
.LBE101:
.LBE100:
	.loc 1 184 11 is_stmt 1 discriminator 2 view .LVU470
	.loc 1 184 39 is_stmt 0 discriminator 2 view .LVU471
	cmpq	$-1, %rax
	je	.L187
	.loc 1 186 5 is_stmt 1 view .LVU472
	.loc 1 191 4 view .LVU473
	.loc 1 191 36 is_stmt 0 view .LVU474
	testq	%rax, %rax
	jle	.L151
.LVL72:
	.loc 1 194 19 is_stmt 1 view .LVU475
	.loc 1 194 27 is_stmt 0 view .LVU476
	movq	-4208(%rbp), %rbx
	addq	%rbx, %rax
.LVL73:
	.loc 1 194 12 view .LVU477
	movq	%rbx, %r12
	.loc 1 194 27 view .LVU478
	movq	%rax, -4200(%rbp)
	.loc 1 194 5 view .LVU479
	cmpq	%rbx, %rax
	jbe	.L147
.LVL74:
	.p2align 4,,10
	.p2align 3
.L152:
	.loc 1 195 7 is_stmt 1 view .LVU480
	.loc 1 197 7 view .LVU481
	.loc 1 198 7 view .LVU482
	.loc 1 198 12 is_stmt 0 view .LVU483
	movl	4(%r12), %edx
.LBB102:
.LBB103:
	.loc 1 154 10 view .LVU484
	movq	-4192(%rbp), %rbx
.LBE103:
.LBE102:
	.loc 1 198 19 view .LVU485
	movl	%edx, %ecx
.LBB113:
.LBB110:
	.loc 1 154 10 view .LVU486
	movq	832(%rbx), %r15
.LBE110:
.LBE113:
	.loc 1 198 19 view .LVU487
	andl	$6, %ecx
	.loc 1 198 10 view .LVU488
	cmpl	$1, %ecx
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$3, %eax
	cmpl	$1, %ecx
	sbbl	%r14d, %r14d
	notl	%r14d
	andl	$2, %r14d
.LVL75:
	.loc 1 200 7 is_stmt 1 view .LVU489
	.loc 1 200 10 is_stmt 0 view .LVU490
	andl	$-7, %edx
	cmovne	%eax, %r14d
	.loc 1 203 7 is_stmt 1 view .LVU491
	.loc 1 203 11 is_stmt 0 view .LVU492
	movl	(%r12), %eax
.LVL76:
.LBB114:
.LBI102:
	.loc 1 151 29 is_stmt 1 view .LVU493
.LBB111:
	.loc 1 152 3 view .LVU494
	.loc 1 153 3 view .LVU495
	.loc 1 154 3 view .LVU496
.L186:
.LBB104:
.LBI104:
	.loc 1 58 59 view .LVU497
.LBB105:
	.loc 1 58 135 view .LVU498
	.loc 1 58 180 view .LVU499
	.loc 1 58 190 view .LVU500
	.loc 1 58 196 view .LVU501
	testq	%r15, %r15
	je	.L156
	.loc 1 58 204 view .LVU502
.LBB106:
.LBI106:
	.loc 1 50 12 view .LVU503
.LVL77:
.LBB107:
	.loc 1 52 3 view .LVU504
	.loc 1 52 6 is_stmt 0 view .LVU505
	cmpl	64(%r15), %eax
	jl	.L188
	.loc 1 53 3 is_stmt 1 view .LVU506
	.loc 1 53 6 is_stmt 0 view .LVU507
	jg	.L189
.LVL78:
	.loc 1 53 6 view .LVU508
.LBE107:
.LBE106:
.LBE105:
.LBE104:
.LBE111:
.LBE114:
	.loc 1 204 7 is_stmt 1 view .LVU509
	.loc 1 211 7 view .LVU510
	.loc 1 211 45 is_stmt 0 view .LVU511
	movl	12(%r12), %eax
	leaq	16(%r12), %r13
	testl	%eax, %eax
	jne	.L161
	.loc 1 211 63 discriminator 2 view .LVU512
	movq	56(%r15), %r13
.LVL79:
.LBB115:
.LBI115:
	.file 3 "../deps/uv/src/unix/internal.h"
	.loc 3 306 38 is_stmt 1 discriminator 2 view .LVU513
.LBB116:
	.loc 3 307 3 discriminator 2 view .LVU514
	.loc 3 309 3 discriminator 2 view .LVU515
	.loc 3 309 7 is_stmt 0 discriminator 2 view .LVU516
	movl	$47, %esi
	movq	%r13, %rdi
	call	strrchr@PLT
.LVL80:
	.loc 3 310 3 is_stmt 1 discriminator 2 view .LVU517
	.loc 3 313 12 is_stmt 0 discriminator 2 view .LVU518
	leaq	1(%rax), %rdx
	testq	%rax, %rax
	cmovne	%rdx, %r13
.LVL81:
.L161:
	.loc 3 313 12 discriminator 2 view .LVU519
.LBE116:
.LBE115:
	.loc 1 224 7 is_stmt 1 discriminator 4 view .LVU520
	.loc 1 225 51 is_stmt 0 discriminator 4 view .LVU521
	movq	32(%r15), %rax
	.loc 1 225 34 discriminator 4 view .LVU522
	leaq	32(%r15), %rbx
.LVL82:
	.loc 1 224 20 discriminator 4 view .LVU523
	movl	$1, 48(%r15)
	.loc 1 225 7 is_stmt 1 discriminator 4 view .LVU524
	.loc 1 225 12 discriminator 4 view .LVU525
	.loc 1 225 15 is_stmt 0 discriminator 4 view .LVU526
	cmpq	%rax, %rbx
	je	.L190
.LBB117:
	.loc 1 225 220 is_stmt 1 discriminator 2 view .LVU527
.LVL83:
	.loc 1 225 272 discriminator 2 view .LVU528
	.loc 1 225 277 discriminator 2 view .LVU529
	.loc 1 225 343 is_stmt 0 discriminator 2 view .LVU530
	movq	40(%r15), %rdx
	.loc 1 225 409 discriminator 2 view .LVU531
	movq	-4184(%rbp), %rsi
	.loc 1 225 309 discriminator 2 view .LVU532
	movq	%rdx, -4168(%rbp)
	.loc 1 225 350 is_stmt 1 discriminator 2 view .LVU533
	.loc 1 225 409 is_stmt 0 discriminator 2 view .LVU534
	movq	%rsi, (%rdx)
	.loc 1 225 421 is_stmt 1 discriminator 2 view .LVU535
	.loc 1 225 521 is_stmt 0 discriminator 2 view .LVU536
	movq	8(%rax), %rdx
	.loc 1 225 453 discriminator 2 view .LVU537
	movq	%rax, -4176(%rbp)
	.loc 1 225 460 is_stmt 1 discriminator 2 view .LVU538
	.loc 1 225 498 is_stmt 0 discriminator 2 view .LVU539
	movq	%rdx, 40(%r15)
	.loc 1 225 528 is_stmt 1 discriminator 2 view .LVU540
	.loc 1 225 593 is_stmt 0 discriminator 2 view .LVU541
	movq	%rbx, (%rdx)
	.loc 1 225 611 is_stmt 1 discriminator 2 view .LVU542
	.loc 1 225 638 is_stmt 0 discriminator 2 view .LVU543
	movq	%rsi, 8(%rax)
.LBE117:
	.loc 1 226 13 is_stmt 1 discriminator 2 view .LVU544
	.loc 1 226 44 is_stmt 0 discriminator 2 view .LVU545
	movq	-4176(%rbp), %rax
.LVL84:
	.loc 1 226 13 discriminator 2 view .LVU546
	cmpq	%rsi, %rax
	je	.L164
	.p2align 4,,10
	.p2align 3
.L165:
	.loc 1 227 9 is_stmt 1 view .LVU547
.LVL85:
	.loc 1 228 9 view .LVU548
	.loc 1 230 9 view .LVU549
	.loc 1 230 14 view .LVU550
	.loc 1 230 34 is_stmt 0 view .LVU551
	movq	8(%rax), %rcx
	.loc 1 230 91 view .LVU552
	movq	(%rax), %rdx
	.loc 1 228 11 view .LVU553
	leaq	-112(%rax), %rdi
.LVL86:
	.loc 1 233 9 view .LVU554
	movq	%r13, %rsi
	.loc 1 230 68 view .LVU555
	movq	%rdx, (%rcx)
	.loc 1 230 98 is_stmt 1 view .LVU556
	.loc 1 230 175 is_stmt 0 view .LVU557
	movq	8(%rax), %rcx
	.loc 1 230 152 view .LVU558
	movq	%rcx, 8(%rdx)
	.loc 1 230 190 is_stmt 1 view .LVU559
	.loc 1 231 9 view .LVU560
	.loc 1 231 14 view .LVU561
	.loc 1 233 9 is_stmt 0 view .LVU562
	xorl	%ecx, %ecx
	.loc 1 231 41 view .LVU563
	movq	%rbx, (%rax)
	.loc 1 231 59 is_stmt 1 view .LVU564
	.loc 1 231 120 is_stmt 0 view .LVU565
	movq	40(%r15), %rdx
	.loc 1 231 86 view .LVU566
	movq	%rdx, 8(%rax)
	.loc 1 231 127 is_stmt 1 view .LVU567
	.loc 1 231 181 is_stmt 0 view .LVU568
	movq	%rax, (%rdx)
	.loc 1 231 188 is_stmt 1 view .LVU569
	.loc 1 233 9 is_stmt 0 view .LVU570
	movl	%r14d, %edx
	.loc 1 231 226 view .LVU571
	movq	%rax, 40(%r15)
	.loc 1 231 241 is_stmt 1 view .LVU572
	.loc 1 233 9 view .LVU573
	call	*-8(%rax)
.LVL87:
	.loc 1 226 13 view .LVU574
	.loc 1 226 44 is_stmt 0 view .LVU575
	movq	-4176(%rbp), %rax
	.loc 1 226 13 view .LVU576
	cmpq	-4184(%rbp), %rax
	jne	.L165
.L164:
	.loc 1 226 13 view .LVU577
	movq	32(%r15), %rax
	.loc 1 236 7 is_stmt 1 view .LVU578
	.loc 1 236 20 is_stmt 0 view .LVU579
	movl	$0, 48(%r15)
	.loc 1 237 7 is_stmt 1 view .LVU580
.LVL88:
.LBB118:
.LBI118:
	.loc 1 157 13 view .LVU581
.LBB119:
	.loc 1 159 3 view .LVU582
	.loc 1 159 23 is_stmt 0 view .LVU583
	cmpq	%rax, %rbx
	je	.L163
.LVL89:
.L156:
	.loc 1 159 23 view .LVU584
.LBE119:
.LBE118:
	.loc 1 194 35 is_stmt 1 discriminator 2 view .LVU585
	.loc 1 194 54 is_stmt 0 discriminator 2 view .LVU586
	movl	12(%r12), %eax
	.loc 1 194 37 discriminator 2 view .LVU587
	leaq	16(%r12,%rax), %r12
.LVL90:
	.loc 1 194 19 is_stmt 1 discriminator 2 view .LVU588
	.loc 1 194 5 is_stmt 0 discriminator 2 view .LVU589
	cmpq	-4200(%rbp), %r12
	jb	.L152
	jmp	.L147
.LVL91:
	.p2align 4,,10
	.p2align 3
.L188:
.LBB121:
.LBB112:
.LBB109:
.LBB108:
	.loc 1 58 239 is_stmt 1 view .LVU590
	.loc 1 58 253 view .LVU591
	.loc 1 58 257 is_stmt 0 view .LVU592
	movq	(%r15), %r15
.LVL92:
	.loc 1 58 257 view .LVU593
	jmp	.L186
.LVL93:
	.p2align 4,,10
	.p2align 3
.L189:
	.loc 1 58 287 is_stmt 1 view .LVU594
	.loc 1 58 301 view .LVU595
	.loc 1 58 305 is_stmt 0 view .LVU596
	movq	8(%r15), %r15
.LVL94:
	.loc 1 58 196 is_stmt 1 view .LVU597
	jmp	.L186
.LVL95:
	.p2align 4,,10
	.p2align 3
.L190:
	.loc 1 58 196 is_stmt 0 view .LVU598
.LBE108:
.LBE109:
.LBE112:
.LBE121:
	.loc 1 225 107 is_stmt 1 discriminator 1 view .LVU599
	.loc 1 225 112 discriminator 1 view .LVU600
	.loc 1 225 156 discriminator 1 view .LVU601
	.loc 1 225 144 is_stmt 0 discriminator 1 view .LVU602
	movq	-4184(%rbp), %xmm0
	.loc 1 236 20 discriminator 1 view .LVU603
	movl	$0, 48(%r15)
	.loc 1 225 144 discriminator 1 view .LVU604
	punpcklqdq	%xmm0, %xmm0
	movaps	%xmm0, -4176(%rbp)
	.loc 1 225 208 is_stmt 1 discriminator 1 view .LVU605
	.loc 1 226 13 discriminator 1 view .LVU606
	.loc 1 236 7 discriminator 1 view .LVU607
	.loc 1 237 7 discriminator 1 view .LVU608
.LVL96:
.LBB122:
	.loc 1 157 13 discriminator 1 view .LVU609
.LBB120:
	.loc 1 159 3 discriminator 1 view .LVU610
.L163:
	.loc 1 159 3 is_stmt 0 discriminator 1 view .LVU611
	movq	-4192(%rbp), %rsi
	movq	%r15, %rdi
	call	maybe_free_watcher_list.part.0
.LVL97:
	jmp	.L156
.LVL98:
.L187:
	.loc 1 159 3 discriminator 1 view .LVU612
.LBE120:
.LBE122:
	.loc 1 184 27 discriminator 1 view .LVU613
	call	__errno_location@PLT
.LVL99:
	.loc 1 184 26 discriminator 1 view .LVU614
	movl	(%rax), %eax
	.loc 1 184 23 discriminator 1 view .LVU615
	cmpl	$4, %eax
	je	.L147
	.loc 1 186 5 is_stmt 1 view .LVU616
	.loc 1 187 6 view .LVU617
	.loc 1 187 40 is_stmt 0 view .LVU618
	cmpl	$11, %eax
	jne	.L191
	.loc 1 240 1 view .LVU619
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L192
	addq	$4168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
.LVL100:
	.loc 1 240 1 view .LVU620
	ret
.LVL101:
.L151:
	.cfi_restore_state
	.loc 1 191 13 is_stmt 1 discriminator 1 view .LVU621
	leaq	__PRETTY_FUNCTION__.10132(%rip), %rcx
	movl	$191, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	__assert_fail@PLT
.LVL102:
.L192:
	.loc 1 240 1 is_stmt 0 view .LVU622
	call	__stack_chk_fail@PLT
.LVL103:
.L191:
	.loc 1 187 17 is_stmt 1 discriminator 1 view .LVU623
	leaq	__PRETTY_FUNCTION__.10132(%rip), %rcx
	movl	$187, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	__assert_fail@PLT
.LVL104:
	.cfi_endproc
.LFE108:
	.size	uv__inotify_read, .-uv__inotify_read
	.p2align 4
	.type	uv_fs_event_start.part.0, @function
uv_fs_event_start.part.0:
.LVL105:
.LFB119:
	.loc 1 249 5 view -0
	.cfi_startproc
	.loc 1 266 3 view .LVU625
	.loc 1 275 3 view .LVU626
	.loc 1 249 5 is_stmt 0 view .LVU627
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	.loc 1 275 8 view .LVU628
	movl	$4038, %edx
.LVL106:
	.loc 1 249 5 view .LVU629
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	.loc 1 275 8 view .LVU630
	movq	%r15, %rsi
.LVL107:
	.loc 1 249 5 view .LVU631
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	.loc 1 275 8 view .LVU632
	movq	8(%rdi), %rax
	movl	840(%rax), %edi
.LVL108:
	.loc 1 275 8 view .LVU633
	call	inotify_add_watch@PLT
.LVL109:
	.loc 1 276 3 is_stmt 1 view .LVU634
	.loc 1 276 6 is_stmt 0 view .LVU635
	cmpl	$-1, %eax
	je	.L269
	.loc 1 279 7 view .LVU636
	movq	8(%r12), %rdi
	movl	%eax, %r14d
	.loc 1 279 3 is_stmt 1 view .LVU637
.LVL110:
.LBB137:
.LBI137:
	.loc 1 151 29 view .LVU638
.LBB138:
	.loc 1 152 3 view .LVU639
	.loc 1 153 3 view .LVU640
	.loc 1 154 3 view .LVU641
	.loc 1 154 10 is_stmt 0 view .LVU642
	movq	832(%rdi), %rbx
.L266:
.LVL111:
.LBB139:
.LBI139:
	.loc 1 58 59 is_stmt 1 view .LVU643
.LBB140:
	.loc 1 58 135 view .LVU644
	.loc 1 58 180 view .LVU645
	.loc 1 58 190 view .LVU646
	.loc 1 58 196 view .LVU647
	testq	%rbx, %rbx
	je	.L196
	.loc 1 58 204 view .LVU648
.LBB141:
.LBI141:
	.loc 1 50 12 view .LVU649
.LVL112:
.LBB142:
	.loc 1 52 3 view .LVU650
	.loc 1 52 6 is_stmt 0 view .LVU651
	cmpl	64(%rbx), %r14d
	jl	.L270
	.loc 1 53 3 is_stmt 1 view .LVU652
	.loc 1 53 6 is_stmt 0 view .LVU653
	jg	.L199
	leaq	32(%rbx), %r8
.LVL113:
.L200:
	.loc 1 53 6 view .LVU654
.LBE142:
.LBE141:
.LBE140:
.LBE139:
.LBE138:
.LBE137:
	.loc 1 295 3 is_stmt 1 view .LVU655
	.loc 1 295 8 view .LVU656
	.loc 1 295 21 is_stmt 0 view .LVU657
	movl	88(%r12), %eax
	.loc 1 295 11 view .LVU658
	testb	$4, %al
	je	.L271
.L235:
	.loc 1 295 190 is_stmt 1 view .LVU659
	.loc 1 295 203 view .LVU660
	.loc 1 296 3 view .LVU661
	.loc 1 296 8 view .LVU662
	.loc 1 296 51 is_stmt 0 view .LVU663
	movq	%r8, 112(%r12)
	.loc 1 296 69 is_stmt 1 view .LVU664
	.loc 1 296 146 is_stmt 0 view .LVU665
	movq	40(%rbx), %rdx
	.loc 1 296 226 view .LVU666
	leaq	112(%r12), %rax
	.loc 1 296 112 view .LVU667
	movq	%rdx, 120(%r12)
	.loc 1 296 153 is_stmt 1 view .LVU668
	.loc 1 296 223 is_stmt 0 view .LVU669
	movq	%rax, (%rdx)
	.loc 1 296 246 is_stmt 1 view .LVU670
	.loc 1 296 284 is_stmt 0 view .LVU671
	movq	%rax, 40(%rbx)
	.loc 1 296 315 is_stmt 1 view .LVU672
	.loc 1 297 3 view .LVU673
	.loc 1 297 16 is_stmt 0 view .LVU674
	movq	56(%rbx), %rax
	.loc 1 298 14 view .LVU675
	movq	%r13, 104(%r12)
	.loc 1 299 14 view .LVU676
	movl	%r14d, 128(%r12)
	.loc 1 297 16 view .LVU677
	movq	%rax, 96(%r12)
	.loc 1 298 3 is_stmt 1 view .LVU678
	.loc 1 299 3 view .LVU679
	.loc 1 301 3 view .LVU680
	.loc 1 301 10 is_stmt 0 view .LVU681
	xorl	%eax, %eax
.L193:
	.loc 1 302 1 view .LVU682
	addq	$24, %rsp
	popq	%rbx
.LVL114:
	.loc 1 302 1 view .LVU683
	popq	%r12
.LVL115:
	.loc 1 302 1 view .LVU684
	popq	%r13
.LVL116:
	.loc 1 302 1 view .LVU685
	popq	%r14
.LVL117:
	.loc 1 302 1 view .LVU686
	popq	%r15
.LVL118:
	.loc 1 302 1 view .LVU687
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.LVL119:
	.p2align 4,,10
	.p2align 3
.L270:
	.cfi_restore_state
.LBB149:
.LBB147:
.LBB145:
.LBB143:
	.loc 1 58 239 is_stmt 1 view .LVU688
	.loc 1 58 253 view .LVU689
	.loc 1 58 257 is_stmt 0 view .LVU690
	movq	(%rbx), %rbx
.LVL120:
	.loc 1 58 257 view .LVU691
	jmp	.L266
.LVL121:
	.p2align 4,,10
	.p2align 3
.L271:
	.loc 1 58 257 view .LVU692
.LBE143:
.LBE145:
.LBE147:
.LBE149:
	.loc 1 295 62 is_stmt 1 view .LVU693
	.loc 1 295 78 is_stmt 0 view .LVU694
	movl	%eax, %edx
	orl	$4, %edx
	movl	%edx, 88(%r12)
	.loc 1 295 99 is_stmt 1 view .LVU695
	.loc 1 295 102 is_stmt 0 view .LVU696
	testb	$8, %al
	je	.L235
	.loc 1 295 143 is_stmt 1 view .LVU697
	.loc 1 295 148 view .LVU698
	.loc 1 295 178 is_stmt 0 view .LVU699
	addl	$1, 8(%rdi)
	jmp	.L235
.LVL122:
	.p2align 4,,10
	.p2align 3
.L199:
.LBB150:
.LBB148:
.LBB146:
.LBB144:
	.loc 1 58 287 is_stmt 1 view .LVU700
	.loc 1 58 301 view .LVU701
	.loc 1 58 305 is_stmt 0 view .LVU702
	movq	8(%rbx), %rbx
.LVL123:
	.loc 1 58 196 is_stmt 1 view .LVU703
	jmp	.L266
.LVL124:
	.p2align 4,,10
	.p2align 3
.L196:
	.loc 1 58 196 is_stmt 0 view .LVU704
.LBE144:
.LBE146:
.LBE148:
.LBE150:
	.loc 1 280 3 is_stmt 1 view .LVU705
	.loc 1 283 3 view .LVU706
	.loc 1 283 9 is_stmt 0 view .LVU707
	movq	%r15, %rdi
.LVL125:
	.loc 1 283 9 view .LVU708
	call	strlen@PLT
.LVL126:
	.loc 1 283 7 view .LVU709
	leaq	1(%rax), %rdx
	.loc 1 284 7 view .LVU710
	leaq	73(%rax), %rdi
	.loc 1 283 7 view .LVU711
	movq	%rdx, -56(%rbp)
.LVL127:
	.loc 1 284 3 is_stmt 1 view .LVU712
	.loc 1 284 7 is_stmt 0 view .LVU713
	call	uv__malloc@PLT
.LVL128:
	.loc 1 285 6 view .LVU714
	movq	-56(%rbp), %rdx
	testq	%rax, %rax
	.loc 1 284 7 view .LVU715
	movq	%rax, %rbx
.LVL129:
	.loc 1 285 3 is_stmt 1 view .LVU716
	.loc 1 285 6 is_stmt 0 view .LVU717
	je	.L272
	.loc 1 288 3 is_stmt 1 view .LVU718
	.loc 1 288 9 is_stmt 0 view .LVU719
	movl	%r14d, 64(%rbx)
	.loc 1 289 3 is_stmt 1 view .LVU720
.LVL130:
.LBB151:
.LBI151:
	.file 4 "/usr/include/x86_64-linux-gnu/bits/string_fortified.h"
	.loc 4 31 42 view .LVU721
.LBB152:
	.loc 4 34 3 view .LVU722
.LBE152:
.LBE151:
	.loc 1 289 22 is_stmt 0 view .LVU723
	leaq	72(%rbx), %rdi
.LVL131:
.LBB154:
.LBB153:
	.loc 4 34 10 view .LVU724
	movq	%r15, %rsi
	call	memcpy@PLT
.LVL132:
	.loc 4 34 10 view .LVU725
.LBE153:
.LBE154:
	.loc 1 290 49 view .LVU726
	leaq	32(%rbx), %r8
	.loc 1 291 16 view .LVU727
	movl	$0, 48(%rbx)
	.loc 1 290 46 view .LVU728
	movq	%r8, %xmm0
	.loc 1 289 11 view .LVU729
	movq	%rax, 56(%rbx)
	.loc 1 290 3 is_stmt 1 view .LVU730
	.loc 1 290 8 view .LVU731
	.loc 1 290 64 view .LVU732
	.loc 1 290 46 is_stmt 0 view .LVU733
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 32(%rbx)
	.loc 1 290 128 is_stmt 1 view .LVU734
	.loc 1 291 3 view .LVU735
	.loc 1 292 3 view .LVU736
	.loc 1 292 57 is_stmt 0 view .LVU737
	movq	8(%r12), %rdi
.LVL133:
.LBB155:
.LBI155:
	.loc 1 58 451 is_stmt 1 view .LVU738
.LBB156:
	.loc 1 58 529 view .LVU739
	.loc 1 58 555 view .LVU740
	.loc 1 58 3 view .LVU741
	.loc 1 58 17 view .LVU742
	.loc 1 58 21 is_stmt 0 view .LVU743
	movq	832(%rdi), %rax
.LVL134:
	.loc 1 58 41 is_stmt 1 view .LVU744
	.loc 1 58 47 view .LVU745
	testq	%rax, %rax
	jne	.L205
	jmp	.L273
.LVL135:
	.p2align 4,,10
	.p2align 3
.L203:
.LBB157:
.LBB158:
	.loc 1 53 3 view .LVU746
	.loc 1 53 6 is_stmt 0 view .LVU747
	jle	.L200
.LVL136:
	.loc 1 53 6 view .LVU748
.LBE158:
.LBE157:
	.loc 1 58 157 is_stmt 1 view .LVU749
	.loc 1 58 171 view .LVU750
	.loc 1 58 175 is_stmt 0 view .LVU751
	movq	8(%rax), %rdx
.LVL137:
	.loc 1 58 175 view .LVU752
	movl	$1, %ecx
.LVL138:
	.loc 1 58 47 is_stmt 1 view .LVU753
	testq	%rdx, %rdx
	je	.L274
.L240:
	.loc 1 58 47 is_stmt 0 view .LVU754
	movq	%rdx, %rax
.LVL139:
.L205:
	.loc 1 58 55 is_stmt 1 view .LVU755
	.loc 1 58 69 view .LVU756
.LBB161:
.LBI157:
	.loc 1 50 12 view .LVU757
.LBB159:
	.loc 1 52 3 view .LVU758
	.loc 1 52 6 is_stmt 0 view .LVU759
	cmpl	64(%rax), %r14d
	jge	.L203
.LVL140:
	.loc 1 52 6 view .LVU760
.LBE159:
.LBE161:
	.loc 1 58 109 is_stmt 1 view .LVU761
	.loc 1 58 123 view .LVU762
	.loc 1 58 127 is_stmt 0 view .LVU763
	movq	(%rax), %rdx
.LVL141:
.LBB162:
.LBB160:
	.loc 1 52 29 view .LVU764
	movl	$-1, %ecx
.LVL142:
	.loc 1 52 29 view .LVU765
.LBE160:
.LBE162:
	.loc 1 58 47 is_stmt 1 view .LVU766
	testq	%rdx, %rdx
	jne	.L240
.L274:
	.loc 1 58 222 view .LVU767
	.loc 1 58 227 view .LVU768
	.loc 1 58 283 is_stmt 0 view .LVU769
	pxor	%xmm0, %xmm0
	.loc 1 58 251 view .LVU770
	movq	%rax, 16(%rbx)
	.loc 1 58 261 is_stmt 1 view .LVU771
	.loc 1 58 26 is_stmt 0 view .LVU772
	movl	$1, 24(%rbx)
	.loc 1 58 283 view .LVU773
	movups	%xmm0, (%rbx)
	.loc 1 58 3 is_stmt 1 view .LVU774
	.loc 1 58 39 view .LVU775
	.loc 1 58 45 view .LVU776
	.loc 1 58 5 view .LVU777
	.loc 1 58 8 is_stmt 0 view .LVU778
	cmpl	$-1, %ecx
	je	.L275
	.loc 1 58 56 is_stmt 1 view .LVU779
	.loc 1 58 82 is_stmt 0 view .LVU780
	movq	%rbx, 8(%rax)
.L209:
.LVL143:
.LBB163:
.LBB164:
	.loc 1 58 338 is_stmt 1 view .LVU781
	.loc 1 58 342 view .LVU782
	.loc 1 58 350 view .LVU783
	.loc 1 58 363 view .LVU784
	.loc 1 58 172 view .LVU785
	.loc 1 58 1 is_stmt 0 view .LVU786
	movq	%rbx, %rcx
.LVL144:
	.loc 1 58 1 view .LVU787
	jmp	.L232
.LVL145:
	.p2align 4,,10
	.p2align 3
.L210:
	.loc 1 58 379 is_stmt 1 view .LVU788
	.loc 1 58 412 view .LVU789
	.loc 1 58 415 is_stmt 0 view .LVU790
	testq	%rsi, %rsi
	je	.L222
	.loc 1 58 420 view .LVU791
	cmpl	$1, 24(%rsi)
	je	.L267
.L222:
	.loc 1 58 591 is_stmt 1 view .LVU792
	.loc 1 58 594 is_stmt 0 view .LVU793
	cmpq	%rcx, (%rax)
	je	.L276
.LVL146:
.L223:
	.loc 1 58 405 is_stmt 1 view .LVU794
	.loc 1 58 410 view .LVU795
	.loc 1 58 436 is_stmt 0 view .LVU796
	movl	$0, 24(%rax)
	.loc 1 58 441 is_stmt 1 view .LVU797
	.loc 1 58 498 is_stmt 0 view .LVU798
	movq	8(%rdx), %rax
.LVL147:
	.loc 1 58 468 view .LVU799
	movl	$1, 24(%rdx)
	.loc 1 58 481 is_stmt 1 view .LVU800
	.loc 1 58 487 view .LVU801
	.loc 1 58 492 view .LVU802
.LVL148:
	.loc 1 58 528 view .LVU803
	.loc 1 58 574 is_stmt 0 view .LVU804
	movq	(%rax), %rsi
	.loc 1 58 560 view .LVU805
	movq	%rsi, 8(%rdx)
	.loc 1 58 531 view .LVU806
	testq	%rsi, %rsi
	je	.L228
	.loc 1 58 5 is_stmt 1 view .LVU807
	.loc 1 58 47 is_stmt 0 view .LVU808
	movq	%rdx, 16(%rsi)
.L228:
	.loc 1 58 62 is_stmt 1 view .LVU809
	.loc 1 58 66 view .LVU810
	.loc 1 58 74 view .LVU811
	.loc 1 58 79 view .LVU812
	.loc 1 58 126 is_stmt 0 view .LVU813
	movq	16(%rdx), %rsi
	.loc 1 58 108 view .LVU814
	movq	%rsi, 16(%rax)
	.loc 1 58 82 view .LVU815
	testq	%rsi, %rsi
	je	.L229
	.loc 1 58 5 is_stmt 1 view .LVU816
	.loc 1 58 8 is_stmt 0 view .LVU817
	cmpq	(%rsi), %rdx
	je	.L277
	.loc 1 58 129 is_stmt 1 view .LVU818
	.loc 1 58 176 is_stmt 0 view .LVU819
	movq	%rax, 8(%rsi)
.L231:
	.loc 1 58 218 is_stmt 1 view .LVU820
	.loc 1 58 240 is_stmt 0 view .LVU821
	movq	%rdx, (%rax)
	.loc 1 58 253 is_stmt 1 view .LVU822
	.loc 1 58 281 is_stmt 0 view .LVU823
	movq	%rax, 16(%rdx)
	.loc 1 58 290 is_stmt 1 view .LVU824
	.loc 1 58 294 view .LVU825
	.loc 1 58 302 view .LVU826
	.loc 1 58 307 view .LVU827
.LVL149:
.L212:
	.loc 1 58 338 view .LVU828
	.loc 1 58 342 view .LVU829
	.loc 1 58 350 view .LVU830
	.loc 1 58 363 view .LVU831
	.loc 1 58 172 view .LVU832
	.loc 1 58 181 is_stmt 0 view .LVU833
	movq	16(%rcx), %rax
.LVL150:
	.loc 1 58 172 view .LVU834
	testq	%rax, %rax
	je	.L268
.LVL151:
.L232:
	.loc 1 58 1 view .LVU835
	cmpl	$1, 24(%rax)
	jne	.L268
	.loc 1 58 38 is_stmt 1 view .LVU836
	.loc 1 58 46 is_stmt 0 view .LVU837
	movq	16(%rax), %rdx
.LVL152:
	.loc 1 58 76 is_stmt 1 view .LVU838
	.loc 1 58 106 is_stmt 0 view .LVU839
	movq	(%rdx), %rsi
	.loc 1 58 79 view .LVU840
	cmpq	%rax, %rsi
	jne	.L210
	.loc 1 58 119 is_stmt 1 view .LVU841
	.loc 1 58 123 is_stmt 0 view .LVU842
	movq	8(%rdx), %rsi
.LVL153:
	.loc 1 58 153 is_stmt 1 view .LVU843
	.loc 1 58 156 is_stmt 0 view .LVU844
	testq	%rsi, %rsi
	je	.L211
	.loc 1 58 161 view .LVU845
	cmpl	$1, 24(%rsi)
	je	.L267
.L211:
	.loc 1 58 332 is_stmt 1 view .LVU846
	movq	%rax, %rsi
.LVL154:
	.loc 1 58 335 is_stmt 0 view .LVU847
	cmpq	%rcx, 8(%rax)
	je	.L278
.LVL155:
.L213:
	.loc 1 58 404 is_stmt 1 view .LVU848
	.loc 1 58 409 view .LVU849
	.loc 1 58 435 is_stmt 0 view .LVU850
	movl	$0, 24(%rax)
	.loc 1 58 440 is_stmt 1 view .LVU851
	.loc 1 58 571 is_stmt 0 view .LVU852
	movq	8(%rsi), %rax
.LVL156:
	.loc 1 58 467 view .LVU853
	movl	$1, 24(%rdx)
	.loc 1 58 480 is_stmt 1 view .LVU854
	.loc 1 58 486 view .LVU855
	.loc 1 58 491 view .LVU856
.LVL157:
	.loc 1 58 526 view .LVU857
	.loc 1 58 557 is_stmt 0 view .LVU858
	movq	%rax, (%rdx)
.LVL158:
	.loc 1 58 529 view .LVU859
	testq	%rax, %rax
	je	.L218
	.loc 1 58 5 is_stmt 1 view .LVU860
	.loc 1 58 48 is_stmt 0 view .LVU861
	movq	%rdx, 16(%rax)
.L218:
	.loc 1 58 63 is_stmt 1 view .LVU862
	.loc 1 58 67 view .LVU863
	.loc 1 58 75 view .LVU864
	.loc 1 58 80 view .LVU865
	.loc 1 58 127 is_stmt 0 view .LVU866
	movq	16(%rdx), %rax
	.loc 1 58 109 view .LVU867
	movq	%rax, 16(%rsi)
	.loc 1 58 83 view .LVU868
	testq	%rax, %rax
	je	.L219
	.loc 1 58 5 is_stmt 1 view .LVU869
	.loc 1 58 8 is_stmt 0 view .LVU870
	cmpq	(%rax), %rdx
	je	.L279
	.loc 1 58 129 is_stmt 1 view .LVU871
	.loc 1 58 176 is_stmt 0 view .LVU872
	movq	%rsi, 8(%rax)
.L221:
	.loc 1 58 218 is_stmt 1 view .LVU873
	.loc 1 58 241 is_stmt 0 view .LVU874
	movq	%rdx, 8(%rsi)
	.loc 1 58 254 is_stmt 1 view .LVU875
	.loc 1 58 282 is_stmt 0 view .LVU876
	movq	%rsi, 16(%rdx)
	.loc 1 58 291 is_stmt 1 view .LVU877
	.loc 1 58 295 view .LVU878
	.loc 1 58 303 view .LVU879
	.loc 1 58 308 view .LVU880
	.loc 1 58 339 view .LVU881
	.loc 1 58 343 view .LVU882
	.loc 1 58 351 view .LVU883
	.loc 1 58 364 view .LVU884
	jmp	.L212
.LVL159:
	.p2align 4,,10
	.p2align 3
.L269:
	.loc 1 58 364 is_stmt 0 view .LVU885
.LBE164:
.LBE163:
.LBE156:
.LBE155:
	.loc 1 277 5 is_stmt 1 view .LVU886
	.loc 1 277 13 is_stmt 0 view .LVU887
	call	__errno_location@PLT
.LVL160:
	.loc 1 277 13 view .LVU888
	movl	(%rax), %eax
	.loc 1 302 1 view .LVU889
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
.LVL161:
	.loc 1 277 13 view .LVU890
	negl	%eax
	.loc 1 302 1 view .LVU891
	popq	%r13
.LVL162:
	.loc 1 302 1 view .LVU892
	popq	%r14
	popq	%r15
.LVL163:
	.loc 1 302 1 view .LVU893
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.LVL164:
	.p2align 4,,10
	.p2align 3
.L276:
	.cfi_restore_state
.LBB172:
.LBB171:
.LBB168:
.LBB165:
	.loc 1 58 630 is_stmt 1 view .LVU894
	.loc 1 58 635 view .LVU895
	.loc 1 58 669 view .LVU896
	.loc 1 58 713 is_stmt 0 view .LVU897
	movq	8(%rcx), %rsi
	.loc 1 58 699 view .LVU898
	movq	%rsi, (%rax)
	.loc 1 58 672 view .LVU899
	testq	%rsi, %rsi
	je	.L224
	.loc 1 58 5 is_stmt 1 view .LVU900
	.loc 1 58 48 is_stmt 0 view .LVU901
	movq	%rax, 16(%rsi)
	.loc 1 58 62 is_stmt 1 view .LVU902
	.loc 1 58 66 view .LVU903
	.loc 1 58 74 view .LVU904
	.loc 1 58 79 view .LVU905
	.loc 1 58 125 is_stmt 0 view .LVU906
	movq	16(%rax), %rsi
	.loc 1 58 108 view .LVU907
	movq	%rsi, 16(%rcx)
	.loc 1 58 82 view .LVU908
	testq	%rsi, %rsi
	je	.L225
.L239:
	.loc 1 58 5 is_stmt 1 view .LVU909
	.loc 1 58 8 is_stmt 0 view .LVU910
	cmpq	%rax, (%rsi)
	je	.L280
	.loc 1 58 126 is_stmt 1 view .LVU911
	.loc 1 58 172 is_stmt 0 view .LVU912
	movq	%rcx, 8(%rsi)
.L227:
	.loc 1 58 214 is_stmt 1 view .LVU913
	.loc 1 58 276 is_stmt 0 view .LVU914
	movq	%rcx, %rsi
	.loc 1 58 237 view .LVU915
	movq	%rax, 8(%rcx)
	.loc 1 58 249 is_stmt 1 view .LVU916
	.loc 1 58 276 is_stmt 0 view .LVU917
	movq	%rcx, 16(%rax)
	.loc 1 58 285 is_stmt 1 view .LVU918
	.loc 1 58 289 view .LVU919
	.loc 1 58 297 view .LVU920
	.loc 1 58 302 view .LVU921
	.loc 1 58 333 view .LVU922
	.loc 1 58 337 view .LVU923
	.loc 1 58 345 view .LVU924
	.loc 1 58 358 view .LVU925
	.loc 1 58 364 view .LVU926
.LVL165:
	.loc 1 58 378 view .LVU927
	.loc 1 58 392 view .LVU928
	.loc 1 58 276 is_stmt 0 view .LVU929
	movq	%rax, %rcx
	movq	%rsi, %rax
	jmp	.L223
.LVL166:
	.p2align 4,,10
	.p2align 3
.L229:
	.loc 1 58 192 is_stmt 1 view .LVU930
	.loc 1 58 209 is_stmt 0 view .LVU931
	movq	%rax, 832(%rdi)
	jmp	.L231
.LVL167:
	.p2align 4,,10
	.p2align 3
.L267:
	.loc 1 58 454 is_stmt 1 view .LVU932
	.loc 1 58 477 is_stmt 0 view .LVU933
	movl	$0, 24(%rsi)
	.loc 1 58 482 is_stmt 1 view .LVU934
	.loc 1 58 487 view .LVU935
	.loc 1 58 1 is_stmt 0 view .LVU936
	movq	%rdx, %rcx
.LVL168:
	.loc 1 58 513 view .LVU937
	movl	$0, 24(%rax)
	.loc 1 58 518 is_stmt 1 view .LVU938
	.loc 1 58 545 is_stmt 0 view .LVU939
	movl	$1, 24(%rdx)
	.loc 1 58 558 is_stmt 1 view .LVU940
	.loc 1 58 564 view .LVU941
.LVL169:
	.loc 1 58 579 view .LVU942
	.loc 1 58 1 is_stmt 0 view .LVU943
	jmp	.L212
.LVL170:
	.p2align 4,,10
	.p2align 3
.L277:
	.loc 1 58 69 is_stmt 1 view .LVU944
	.loc 1 58 115 is_stmt 0 view .LVU945
	movq	%rax, (%rsi)
	jmp	.L231
.LVL171:
	.p2align 4,,10
	.p2align 3
.L268:
	.loc 1 58 115 view .LVU946
	movq	832(%rdi), %rax
.LVL172:
.L207:
	.loc 1 58 373 is_stmt 1 view .LVU947
	.loc 1 58 407 is_stmt 0 view .LVU948
	movl	$0, 24(%rax)
	.loc 1 58 412 view .LVU949
	jmp	.L200
.LVL173:
	.p2align 4,,10
	.p2align 3
.L278:
	.loc 1 58 372 is_stmt 1 view .LVU950
	.loc 1 58 377 view .LVU951
	.loc 1 58 412 view .LVU952
	.loc 1 58 457 is_stmt 0 view .LVU953
	movq	(%rcx), %rsi
	.loc 1 58 443 view .LVU954
	movq	%rsi, 8(%rax)
	.loc 1 58 415 view .LVU955
	testq	%rsi, %rsi
	je	.L214
	.loc 1 58 5 is_stmt 1 view .LVU956
	.loc 1 58 47 is_stmt 0 view .LVU957
	movq	%rax, 16(%rsi)
	.loc 1 58 61 is_stmt 1 view .LVU958
	.loc 1 58 65 view .LVU959
	.loc 1 58 73 view .LVU960
	.loc 1 58 78 view .LVU961
	.loc 1 58 124 is_stmt 0 view .LVU962
	movq	16(%rax), %rsi
	.loc 1 58 107 view .LVU963
	movq	%rsi, 16(%rcx)
	.loc 1 58 81 view .LVU964
	testq	%rsi, %rsi
	je	.L215
	.loc 1 58 5 is_stmt 1 view .LVU965
	.loc 1 58 8 is_stmt 0 view .LVU966
	cmpq	(%rsi), %rax
	je	.L238
	.loc 1 58 126 is_stmt 1 view .LVU967
	.loc 1 58 172 is_stmt 0 view .LVU968
	movq	%rcx, 8(%rsi)
.L217:
	.loc 1 58 214 is_stmt 1 view .LVU969
	.loc 1 58 275 is_stmt 0 view .LVU970
	movq	%rcx, %r9
	.loc 1 58 236 view .LVU971
	movq	%rax, (%rcx)
	.loc 1 58 248 is_stmt 1 view .LVU972
	movq	(%rdx), %rsi
	.loc 1 58 275 is_stmt 0 view .LVU973
	movq	%rcx, 16(%rax)
	.loc 1 58 284 is_stmt 1 view .LVU974
	.loc 1 58 288 view .LVU975
	.loc 1 58 296 view .LVU976
	.loc 1 58 301 view .LVU977
	.loc 1 58 332 view .LVU978
	.loc 1 58 336 view .LVU979
	.loc 1 58 344 view .LVU980
	.loc 1 58 357 view .LVU981
	.loc 1 58 363 view .LVU982
.LVL174:
	.loc 1 58 377 view .LVU983
	.loc 1 58 391 view .LVU984
	.loc 1 58 275 is_stmt 0 view .LVU985
	movq	%rax, %rcx
	movq	%r9, %rax
	jmp	.L213
.LVL175:
	.p2align 4,,10
	.p2align 3
.L219:
	.loc 1 58 192 is_stmt 1 view .LVU986
	.loc 1 58 209 is_stmt 0 view .LVU987
	movq	%rsi, 832(%rdi)
	jmp	.L221
	.p2align 4,,10
	.p2align 3
.L279:
	.loc 1 58 69 is_stmt 1 view .LVU988
	.loc 1 58 115 is_stmt 0 view .LVU989
	movq	%rsi, (%rax)
	jmp	.L221
.LVL176:
	.p2align 4,,10
	.p2align 3
.L225:
	.loc 1 58 188 is_stmt 1 view .LVU990
	.loc 1 58 205 is_stmt 0 view .LVU991
	movq	%rcx, 832(%rdi)
	jmp	.L227
	.p2align 4,,10
	.p2align 3
.L224:
	.loc 1 58 62 is_stmt 1 view .LVU992
	.loc 1 58 66 view .LVU993
	.loc 1 58 74 view .LVU994
	.loc 1 58 79 view .LVU995
	.loc 1 58 108 is_stmt 0 view .LVU996
	movq	%rdx, 16(%rcx)
	movq	%rdx, %rsi
	jmp	.L239
	.p2align 4,,10
	.p2align 3
.L215:
	.loc 1 58 188 is_stmt 1 view .LVU997
	.loc 1 58 205 is_stmt 0 view .LVU998
	movq	%rcx, 832(%rdi)
	jmp	.L217
	.p2align 4,,10
	.p2align 3
.L280:
	.loc 1 58 67 is_stmt 1 view .LVU999
	.loc 1 58 112 is_stmt 0 view .LVU1000
	movq	%rcx, (%rsi)
	jmp	.L227
.LVL177:
.L273:
	.loc 1 58 112 view .LVU1001
.LBE165:
.LBE168:
	.loc 1 58 222 is_stmt 1 view .LVU1002
	.loc 1 58 227 view .LVU1003
	.loc 1 58 283 is_stmt 0 view .LVU1004
	pxor	%xmm0, %xmm0
	.loc 1 58 251 view .LVU1005
	movq	$0, 16(%rbx)
	.loc 1 58 261 is_stmt 1 view .LVU1006
	.loc 1 58 130 is_stmt 0 view .LVU1007
	movq	%rbx, %rax
.LVL178:
	.loc 1 58 26 view .LVU1008
	movl	$1, 24(%rbx)
	.loc 1 58 283 view .LVU1009
	movups	%xmm0, (%rbx)
.LVL179:
	.loc 1 58 3 is_stmt 1 view .LVU1010
	.loc 1 58 39 view .LVU1011
	.loc 1 58 45 view .LVU1012
	.loc 1 58 113 view .LVU1013
	.loc 1 58 130 is_stmt 0 view .LVU1014
	movq	%rbx, 832(%rdi)
.LVL180:
.LBB169:
.LBB166:
	.loc 1 58 338 is_stmt 1 view .LVU1015
	.loc 1 58 342 view .LVU1016
	.loc 1 58 350 view .LVU1017
	.loc 1 58 363 view .LVU1018
	.loc 1 58 172 view .LVU1019
	.loc 1 58 172 is_stmt 0 view .LVU1020
	jmp	.L207
.LVL181:
	.p2align 4,,10
	.p2align 3
.L275:
	.loc 1 58 172 view .LVU1021
.LBE166:
.LBE169:
	.loc 1 58 19 is_stmt 1 view .LVU1022
	.loc 1 58 44 is_stmt 0 view .LVU1023
	movq	%rbx, (%rax)
	jmp	.L209
.LVL182:
	.p2align 4,,10
	.p2align 3
.L214:
.LBB170:
.LBB167:
	.loc 1 58 61 is_stmt 1 view .LVU1024
	.loc 1 58 65 view .LVU1025
	.loc 1 58 73 view .LVU1026
	.loc 1 58 78 view .LVU1027
	.loc 1 58 107 is_stmt 0 view .LVU1028
	movq	%rdx, 16(%rcx)
	.loc 1 58 5 is_stmt 1 view .LVU1029
	.loc 1 58 107 is_stmt 0 view .LVU1030
	movq	%rdx, %rsi
.L238:
	.loc 1 58 67 is_stmt 1 view .LVU1031
	.loc 1 58 112 is_stmt 0 view .LVU1032
	movq	%rcx, (%rsi)
	jmp	.L217
.LVL183:
.L272:
	.loc 1 58 112 view .LVU1033
.LBE167:
.LBE170:
.LBE171:
.LBE172:
	.loc 1 286 12 view .LVU1034
	movl	$-12, %eax
.LVL184:
	.loc 1 286 12 view .LVU1035
	jmp	.L193
	.cfi_endproc
.LFE119:
	.size	uv_fs_event_start.part.0, .-uv_fs_event_start.part.0
	.section	.rodata.str1.1
.LC3:
	.string	"tmp_path != NULL"
.LC4:
	.string	"w != NULL"
	.text
	.p2align 4
	.globl	uv__inotify_fork
	.hidden	uv__inotify_fork
	.type	uv__inotify_fork, @function
uv__inotify_fork:
.LVL185:
.LFB105:
	.loc 1 86 59 is_stmt 1 view -0
	.cfi_startproc
	.loc 1 86 59 is_stmt 0 view .LVU1037
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	.loc 1 86 59 view .LVU1038
	movq	%rdi, -168(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	.loc 1 88 3 is_stmt 1 view .LVU1039
	.loc 1 89 3 view .LVU1040
	.loc 1 90 3 view .LVU1041
	.loc 1 91 3 view .LVU1042
	.loc 1 92 3 view .LVU1043
	.loc 1 93 3 view .LVU1044
	.loc 1 94 3 view .LVU1045
	.loc 1 95 3 view .LVU1046
	.loc 1 97 3 view .LVU1047
	.loc 1 97 6 is_stmt 0 view .LVU1048
	testq	%rsi, %rsi
	je	.L307
	leaq	-96(%rbp), %r13
	.loc 1 101 5 is_stmt 1 view .LVU1049
	.loc 1 101 28 is_stmt 0 view .LVU1050
	movq	%rsi, 832(%rdi)
	.loc 1 103 5 is_stmt 1 view .LVU1051
	.loc 1 103 10 view .LVU1052
	.loc 1 103 94 view .LVU1053
	.loc 1 103 62 is_stmt 0 view .LVU1054
	movq	%r13, %xmm0
	punpcklqdq	%xmm0, %xmm0
	movaps	%xmm0, -96(%rbp)
	.loc 1 103 186 is_stmt 1 view .LVU1055
	.loc 1 109 5 view .LVU1056
.LVL186:
.LBB195:
.LBI195:
	.loc 1 58 1299 view .LVU1057
.LBB196:
	.loc 1 58 1360 view .LVU1058
	.loc 1 58 1405 view .LVU1059
	.loc 1 58 3 view .LVU1060
	.loc 1 58 9 view .LVU1061
	.p2align 4,,10
	.p2align 3
.L284:
	.loc 1 58 17 view .LVU1062
	.loc 1 58 31 view .LVU1063
	.loc 1 58 44 view .LVU1064
	movq	%rsi, %r15
	.loc 1 58 48 is_stmt 0 view .LVU1065
	movq	(%rsi), %rsi
.LVL187:
	.loc 1 58 9 is_stmt 1 view .LVU1066
	testq	%rsi, %rsi
	jne	.L284
	leaq	-144(%rbp), %rbx
.LVL188:
	.p2align 4,,10
	.p2align 3
.L285:
	.loc 1 58 9 is_stmt 0 view .LVU1067
.LBE196:
.LBE195:
.LBB197:
.LBI197:
	.loc 1 58 259 is_stmt 1 view .LVU1068
.LBB198:
	.loc 1 58 308 view .LVU1069
	.loc 1 58 324 is_stmt 0 view .LVU1070
	movq	8(%r15), %rax
	.loc 1 58 311 view .LVU1071
	testq	%rax, %rax
	je	.L301
.LVL189:
	.p2align 4,,10
	.p2align 3
.L302:
	.loc 1 58 374 is_stmt 1 view .LVU1072
	movq	%rax, -152(%rbp)
	.loc 1 58 387 is_stmt 0 view .LVU1073
	movq	(%rax), %rax
.LVL190:
	.loc 1 58 374 view .LVU1074
	testq	%rax, %rax
	jne	.L302
.LVL191:
.L303:
	.loc 1 58 710 is_stmt 1 view .LVU1075
	.loc 1 58 710 is_stmt 0 view .LVU1076
.LBE198:
.LBE197:
	.loc 1 111 7 is_stmt 1 view .LVU1077
	.loc 1 112 62 is_stmt 0 view .LVU1078
	movq	32(%r15), %rax
	.loc 1 112 34 view .LVU1079
	leaq	32(%r15), %r12
	.loc 1 111 31 view .LVU1080
	movl	$1, 48(%r15)
	.loc 1 112 7 is_stmt 1 view .LVU1081
	.loc 1 112 12 view .LVU1082
	.loc 1 112 15 is_stmt 0 view .LVU1083
	cmpq	%rax, %r12
	je	.L333
.LBB201:
	.loc 1 112 242 is_stmt 1 discriminator 2 view .LVU1084
.LVL192:
	.loc 1 112 305 discriminator 2 view .LVU1085
	.loc 1 112 310 discriminator 2 view .LVU1086
	.loc 1 112 387 is_stmt 0 discriminator 2 view .LVU1087
	movq	40(%r15), %rdx
	.loc 1 112 342 discriminator 2 view .LVU1088
	movq	%rdx, -136(%rbp)
	.loc 1 112 394 is_stmt 1 discriminator 2 view .LVU1089
	.loc 1 112 453 is_stmt 0 discriminator 2 view .LVU1090
	movq	%rbx, (%rdx)
	.loc 1 112 465 is_stmt 1 discriminator 2 view .LVU1091
	.loc 1 112 576 is_stmt 0 discriminator 2 view .LVU1092
	movq	8(%rax), %rdx
	.loc 1 112 497 discriminator 2 view .LVU1093
	movq	%rax, -144(%rbp)
	.loc 1 112 504 is_stmt 1 discriminator 2 view .LVU1094
	.loc 1 112 553 is_stmt 0 discriminator 2 view .LVU1095
	movq	%rdx, 40(%r15)
	.loc 1 112 583 is_stmt 1 discriminator 2 view .LVU1096
	.loc 1 112 659 is_stmt 0 discriminator 2 view .LVU1097
	movq	%r12, (%rdx)
	.loc 1 112 688 is_stmt 1 discriminator 2 view .LVU1098
.LBE201:
	.loc 1 113 44 is_stmt 0 discriminator 2 view .LVU1099
	movq	-144(%rbp), %r14
.LBB202:
	.loc 1 112 715 discriminator 2 view .LVU1100
	movq	%rbx, 8(%rax)
.LBE202:
	.loc 1 113 13 is_stmt 1 discriminator 2 view .LVU1101
	cmpq	%rbx, %r14
	jne	.L297
	jmp	.L287
.LVL193:
	.p2align 4,,10
	.p2align 3
.L292:
.LBB203:
.LBB204:
.LBB205:
.LBB206:
.LBB207:
.LBB208:
.LBB209:
.LBB210:
	.loc 1 53 3 view .LVU1102
	.loc 1 53 6 is_stmt 0 view .LVU1103
	jg	.L334
.LVL194:
	.loc 1 53 6 view .LVU1104
.LBE210:
.LBE209:
.LBE208:
.LBE207:
.LBE206:
.LBE205:
	.loc 1 312 2 is_stmt 1 view .LVU1105
	.loc 1 314 3 view .LVU1106
	.loc 1 316 78 is_stmt 0 view .LVU1107
	movl	%ecx, %r9d
	.loc 1 314 14 view .LVU1108
	movl	$-1, 16(%r14)
	.loc 1 315 3 is_stmt 1 view .LVU1109
	.loc 1 316 78 is_stmt 0 view .LVU1110
	andl	$-5, %r9d
	.loc 1 316 103 view .LVU1111
	andl	$8, %ecx
	.loc 1 315 16 view .LVU1112
	movq	$0, -16(%r14)
	.loc 1 316 3 is_stmt 1 view .LVU1113
	.loc 1 316 8 view .LVU1114
	.loc 1 316 62 view .LVU1115
	.loc 1 316 78 is_stmt 0 view .LVU1116
	movl	%r9d, -24(%r14)
	.loc 1 316 100 is_stmt 1 view .LVU1117
	.loc 1 316 103 is_stmt 0 view .LVU1118
	je	.L317
	.loc 1 316 144 is_stmt 1 view .LVU1119
	.loc 1 316 149 view .LVU1120
	.loc 1 316 179 is_stmt 0 view .LVU1121
	subl	$1, 8(%rsi)
.L317:
	.loc 1 316 191 is_stmt 1 view .LVU1122
	.loc 1 316 204 view .LVU1123
	.loc 1 317 3 view .LVU1124
	.loc 1 317 8 view .LVU1125
	.loc 1 317 28 is_stmt 0 view .LVU1126
	movq	8(%r14), %rcx
	.loc 1 317 117 view .LVU1127
	movq	(%r14), %rsi
.LVL195:
.LBB220:
.LBB221:
	.loc 1 159 6 view .LVU1128
	movl	48(%rdi), %edx
.LBE221:
.LBE220:
	.loc 1 317 78 view .LVU1129
	movq	%rsi, (%rcx)
.LVL196:
	.loc 1 317 124 is_stmt 1 view .LVU1130
	.loc 1 317 144 is_stmt 0 view .LVU1131
	movq	(%r14), %rcx
	.loc 1 317 233 view .LVU1132
	movq	8(%r14), %rsi
	.loc 1 317 194 view .LVU1133
	movq	%rsi, 8(%rcx)
	.loc 1 317 248 is_stmt 1 view .LVU1134
	.loc 1 319 3 view .LVU1135
.LVL197:
.LBB224:
.LBI220:
	.loc 1 157 13 view .LVU1136
.LBB222:
	.loc 1 159 3 view .LVU1137
	.loc 1 159 6 is_stmt 0 view .LVU1138
	testl	%edx, %edx
	jne	.L290
	.loc 1 159 44 view .LVU1139
	leaq	32(%rdi), %rcx
	.loc 1 159 23 view .LVU1140
	cmpq	%rcx, 32(%rdi)
	je	.L335
.LVL198:
.L290:
	.loc 1 159 23 view .LVU1141
.LBE222:
.LBE224:
.LBE204:
.LBE203:
	.loc 1 126 9 is_stmt 1 view .LVU1142
	.loc 1 126 14 view .LVU1143
	.loc 1 126 57 is_stmt 0 view .LVU1144
	movq	%r13, (%r14)
	.loc 1 126 89 is_stmt 1 view .LVU1145
	.loc 1 126 180 is_stmt 0 view .LVU1146
	movq	-88(%rbp), %rcx
	.loc 1 126 132 view .LVU1147
	movq	%rcx, 8(%r14)
	.loc 1 126 187 is_stmt 1 view .LVU1148
	.loc 1 126 257 is_stmt 0 view .LVU1149
	movq	%r14, (%rcx)
	.loc 1 126 280 is_stmt 1 view .LVU1150
	.loc 1 126 332 is_stmt 0 view .LVU1151
	movq	%r14, -88(%rbp)
	.loc 1 126 363 is_stmt 1 view .LVU1152
	.loc 1 127 9 view .LVU1153
	.loc 1 127 22 is_stmt 0 view .LVU1154
	movq	%rax, -16(%r14)
	.loc 1 113 13 is_stmt 1 view .LVU1155
	.loc 1 113 44 is_stmt 0 view .LVU1156
	movq	-144(%rbp), %r14
.LVL199:
	.loc 1 113 13 view .LVU1157
	cmpq	%rbx, %r14
	je	.L287
.LVL200:
.L297:
	.loc 1 114 9 is_stmt 1 view .LVU1158
	.loc 1 115 9 view .LVU1159
	.loc 1 120 9 view .LVU1160
	.loc 1 120 20 is_stmt 0 view .LVU1161
	movq	-16(%r14), %rdi
	call	uv__strdup@PLT
.LVL201:
	.loc 1 121 8 is_stmt 1 view .LVU1162
	.loc 1 121 51 is_stmt 0 view .LVU1163
	testq	%rax, %rax
	je	.L336
	.loc 1 122 9 is_stmt 1 view .LVU1164
	.loc 1 122 14 view .LVU1165
	.loc 1 122 34 is_stmt 0 view .LVU1166
	movq	8(%r14), %rsi
	.loc 1 122 91 view .LVU1167
	movq	(%r14), %rcx
	.loc 1 122 68 view .LVU1168
	movq	%rcx, (%rsi)
	.loc 1 122 98 is_stmt 1 view .LVU1169
	.loc 1 122 175 is_stmt 0 view .LVU1170
	movq	8(%r14), %rsi
	.loc 1 122 152 view .LVU1171
	movq	%rsi, 8(%rcx)
	.loc 1 122 190 is_stmt 1 view .LVU1172
	.loc 1 123 9 view .LVU1173
	.loc 1 123 14 view .LVU1174
	.loc 1 123 41 is_stmt 0 view .LVU1175
	movq	%r12, (%r14)
	.loc 1 123 70 is_stmt 1 view .LVU1176
	.loc 1 123 142 is_stmt 0 view .LVU1177
	movq	40(%r15), %rcx
	.loc 1 123 97 view .LVU1178
	movq	%rcx, 8(%r14)
	.loc 1 123 149 is_stmt 1 view .LVU1179
	.loc 1 123 203 is_stmt 0 view .LVU1180
	movq	%r14, (%rcx)
	.loc 1 123 210 is_stmt 1 view .LVU1181
.LBB230:
.LBB228:
	.loc 1 308 18 is_stmt 0 view .LVU1182
	movl	-24(%r14), %ecx
.LBE228:
.LBE230:
	.loc 1 123 259 view .LVU1183
	movq	%r14, 40(%r15)
	.loc 1 123 274 is_stmt 1 view .LVU1184
	.loc 1 124 9 view .LVU1185
.LVL202:
.LBB231:
.LBI203:
	.loc 1 305 5 view .LVU1186
.LBB229:
	.loc 1 306 3 view .LVU1187
	.loc 1 308 3 view .LVU1188
	.loc 1 308 6 is_stmt 0 view .LVU1189
	testb	$4, %cl
	je	.L290
	.loc 1 311 3 is_stmt 1 view .LVU1190
	.loc 1 311 7 is_stmt 0 view .LVU1191
	movq	-104(%r14), %rsi
.LVL203:
	.loc 1 311 7 view .LVU1192
	movl	16(%r14), %r9d
.LVL204:
.LBB225:
.LBI205:
	.loc 1 151 29 is_stmt 1 view .LVU1193
.LBB218:
	.loc 1 152 3 view .LVU1194
	.loc 1 153 3 view .LVU1195
	.loc 1 154 3 view .LVU1196
	.loc 1 154 10 is_stmt 0 view .LVU1197
	movq	832(%rsi), %rdi
.L332:
.LVL205:
.LBB216:
.LBI207:
	.loc 1 58 59 is_stmt 1 view .LVU1198
.LBB213:
	.loc 1 58 135 view .LVU1199
	.loc 1 58 180 view .LVU1200
	.loc 1 58 190 view .LVU1201
	.loc 1 58 196 view .LVU1202
	testq	%rdi, %rdi
	je	.L291
.L337:
	.loc 1 58 204 view .LVU1203
.LBB212:
.LBI209:
	.loc 1 50 12 view .LVU1204
.LVL206:
.LBB211:
	.loc 1 52 3 view .LVU1205
	.loc 1 52 6 is_stmt 0 view .LVU1206
	cmpl	64(%rdi), %r9d
	jge	.L292
.LVL207:
	.loc 1 52 6 view .LVU1207
.LBE211:
.LBE212:
	.loc 1 58 239 is_stmt 1 view .LVU1208
	.loc 1 58 253 view .LVU1209
	.loc 1 58 257 is_stmt 0 view .LVU1210
	movq	(%rdi), %rdi
.LVL208:
	.loc 1 58 257 view .LVU1211
.LBE213:
	.loc 1 58 59 is_stmt 1 view .LVU1212
.LBB214:
	.loc 1 58 135 view .LVU1213
	.loc 1 58 180 view .LVU1214
	.loc 1 58 190 view .LVU1215
	.loc 1 58 196 view .LVU1216
	testq	%rdi, %rdi
	jne	.L337
.LVL209:
.L291:
	.loc 1 58 196 is_stmt 0 view .LVU1217
.LBE214:
.LBE216:
.LBE218:
.LBE225:
	.loc 1 312 2 is_stmt 1 view .LVU1218
	.loc 1 312 22 view .LVU1219
	leaq	__PRETTY_FUNCTION__.10164(%rip), %rcx
	movl	$312, %edx
	leaq	.LC0(%rip), %rsi
.LVL210:
	.loc 1 312 22 is_stmt 0 view .LVU1220
	leaq	.LC4(%rip), %rdi
	call	__assert_fail@PLT
.LVL211:
	.p2align 4,,10
	.p2align 3
.L334:
.LBB226:
.LBB219:
.LBB217:
.LBB215:
	.loc 1 58 287 is_stmt 1 view .LVU1221
	.loc 1 58 301 view .LVU1222
	.loc 1 58 305 is_stmt 0 view .LVU1223
	movq	8(%rdi), %rdi
.LVL212:
	.loc 1 58 196 is_stmt 1 view .LVU1224
	jmp	.L332
.LVL213:
	.p2align 4,,10
	.p2align 3
.L335:
	.loc 1 58 196 is_stmt 0 view .LVU1225
.LBE215:
.LBE217:
.LBE219:
.LBE226:
.LBB227:
.LBB223:
	movq	-104(%r14), %rsi
	movq	%rax, -160(%rbp)
	call	maybe_free_watcher_list.part.0
.LVL214:
	.loc 1 58 196 view .LVU1226
	movq	-160(%rbp), %rax
	jmp	.L290
.LVL215:
	.p2align 4,,10
	.p2align 3
.L287:
	.loc 1 58 196 view .LVU1227
	movq	32(%r15), %rax
.LBE223:
.LBE227:
.LBE229:
.LBE231:
	.loc 1 129 7 is_stmt 1 view .LVU1228
	.loc 1 129 31 is_stmt 0 view .LVU1229
	movl	$0, 48(%r15)
	.loc 1 130 7 is_stmt 1 view .LVU1230
.LVL216:
.LBB232:
.LBI232:
	.loc 1 157 13 view .LVU1231
.LBB233:
	.loc 1 159 3 view .LVU1232
	.loc 1 159 23 is_stmt 0 view .LVU1233
	cmpq	%rax, %r12
	je	.L286
.L299:
.LVL217:
	.loc 1 159 23 view .LVU1234
.LBE233:
.LBE232:
	.loc 1 109 96 is_stmt 1 discriminator 1 view .LVU1235
	.loc 1 109 5 is_stmt 0 discriminator 1 view .LVU1236
	movq	-152(%rbp), %r15
	testq	%r15, %r15
	jne	.L285
	.loc 1 133 5 is_stmt 1 view .LVU1237
	.loc 1 133 10 view .LVU1238
	.loc 1 133 63 is_stmt 0 view .LVU1239
	movq	-96(%rbp), %rax
	.loc 1 133 13 view .LVU1240
	cmpq	%r13, %rax
	je	.L307
.LBB235:
	.loc 1 133 246 is_stmt 1 discriminator 2 view .LVU1241
.LVL218:
	.loc 1 133 312 discriminator 2 view .LVU1242
	.loc 1 133 317 discriminator 2 view .LVU1243
	.loc 1 133 397 is_stmt 0 discriminator 2 view .LVU1244
	movq	-88(%rbp), %rdx
	.loc 1 133 349 discriminator 2 view .LVU1245
	movq	%rdx, -136(%rbp)
	.loc 1 133 404 is_stmt 1 discriminator 2 view .LVU1246
	.loc 1 133 463 is_stmt 0 discriminator 2 view .LVU1247
	movq	%rbx, (%rdx)
	.loc 1 133 475 is_stmt 1 discriminator 2 view .LVU1248
	.loc 1 133 589 is_stmt 0 discriminator 2 view .LVU1249
	movq	8(%rax), %rdx
	.loc 1 133 507 discriminator 2 view .LVU1250
	movq	%rax, -144(%rbp)
	.loc 1 133 514 is_stmt 1 discriminator 2 view .LVU1251
	.loc 1 133 566 is_stmt 0 discriminator 2 view .LVU1252
	movq	%rdx, -88(%rbp)
	.loc 1 133 596 is_stmt 1 discriminator 2 view .LVU1253
	.loc 1 133 675 is_stmt 0 discriminator 2 view .LVU1254
	movq	%r13, (%rdx)
	.loc 1 133 707 is_stmt 1 discriminator 2 view .LVU1255
.LBE235:
	.loc 1 134 11 discriminator 2 view .LVU1256
.LBB236:
	.loc 1 133 734 is_stmt 0 discriminator 2 view .LVU1257
	movq	%rbx, 8(%rax)
.LBE236:
	.loc 1 134 42 discriminator 2 view .LVU1258
	movq	-144(%rbp), %rax
.LVL219:
	.loc 1 134 11 discriminator 2 view .LVU1259
	cmpq	%rbx, %rax
	je	.L307
.LVL220:
	.p2align 4,,10
	.p2align 3
.L315:
	.loc 1 135 9 is_stmt 1 view .LVU1260
	.loc 1 136 9 view .LVU1261
	.loc 1 136 14 view .LVU1262
	.loc 1 136 34 is_stmt 0 view .LVU1263
	movq	8(%rax), %rcx
	.loc 1 136 91 view .LVU1264
	movq	(%rax), %rdx
	.loc 1 137 16 view .LVU1265
	leaq	-112(%rax), %r15
	.loc 1 136 68 view .LVU1266
	movq	%rdx, (%rcx)
	.loc 1 136 98 is_stmt 1 view .LVU1267
	.loc 1 136 175 is_stmt 0 view .LVU1268
	movq	8(%rax), %rcx
	.loc 1 136 152 view .LVU1269
	movq	%rcx, 8(%rdx)
	.loc 1 136 190 is_stmt 1 view .LVU1270
	.loc 1 137 9 view .LVU1271
.LVL221:
	.loc 1 138 9 view .LVU1272
	.loc 1 138 18 is_stmt 0 view .LVU1273
	movq	-16(%rax), %r14
.LVL222:
	.loc 1 139 9 is_stmt 1 view .LVU1274
	.loc 1 140 15 is_stmt 0 view .LVU1275
	movq	-8(%rax), %r13
	.loc 1 139 22 view .LVU1276
	movq	$0, -16(%rax)
	.loc 1 140 9 is_stmt 1 view .LVU1277
.LVL223:
.LBB237:
.LBI237:
	.loc 1 249 5 view .LVU1278
.LBB238:
	.loc 1 253 3 view .LVU1279
	.loc 1 254 3 view .LVU1280
	.loc 1 255 3 view .LVU1281
	.loc 1 256 3 view .LVU1282
	.loc 1 257 3 view .LVU1283
	.loc 1 259 3 view .LVU1284
	.loc 1 259 6 is_stmt 0 view .LVU1285
	testb	$4, -24(%rax)
	jne	.L338
	.loc 1 262 3 is_stmt 1 view .LVU1286
	.loc 1 262 9 is_stmt 0 view .LVU1287
	movq	-104(%rax), %r12
.LVL224:
.LBB239:
.LBI239:
	.loc 1 68 12 is_stmt 1 view .LVU1288
.LBB240:
	.loc 1 69 3 view .LVU1289
	.loc 1 71 3 view .LVU1290
	.loc 1 71 6 is_stmt 0 view .LVU1291
	cmpl	$-1, 840(%r12)
	je	.L339
.LVL225:
.L311:
	.loc 1 71 6 view .LVU1292
.LBE240:
.LBE239:
	movq	%r15, %rdi
	movq	%r14, %rdx
	movq	%r13, %rsi
	call	uv_fs_event_start.part.0
.LVL226:
.LBE238:
.LBE237:
	.loc 1 141 9 view .LVU1293
	movq	%r14, %rdi
.LBB251:
.LBB247:
	movl	%eax, %r12d
.LVL227:
	.loc 1 141 9 view .LVU1294
.LBE247:
.LBE251:
	.loc 1 141 9 is_stmt 1 view .LVU1295
	call	uv__free@PLT
.LVL228:
	.loc 1 142 9 view .LVU1296
	.loc 1 142 12 is_stmt 0 view .LVU1297
	testl	%r12d, %r12d
	jne	.L281
	.loc 1 134 11 is_stmt 1 view .LVU1298
	.loc 1 134 42 is_stmt 0 view .LVU1299
	movq	-144(%rbp), %rax
	.loc 1 134 11 view .LVU1300
	cmpq	%rbx, %rax
	jne	.L315
.LVL229:
.L307:
	.loc 1 147 10 view .LVU1301
	xorl	%r12d, %r12d
.L281:
	.loc 1 148 1 view .LVU1302
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L340
	addq	$136, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
.LVL230:
	.loc 1 148 1 view .LVU1303
	ret
.LVL231:
	.p2align 4,,10
	.p2align 3
.L333:
	.cfi_restore_state
	.loc 1 112 129 is_stmt 1 discriminator 1 view .LVU1304
	.loc 1 112 134 discriminator 1 view .LVU1305
	.loc 1 112 178 discriminator 1 view .LVU1306
	.loc 1 112 166 is_stmt 0 discriminator 1 view .LVU1307
	movq	%rbx, %xmm0
	.loc 1 129 31 discriminator 1 view .LVU1308
	movl	$0, 48(%r15)
	.loc 1 112 166 discriminator 1 view .LVU1309
	punpcklqdq	%xmm0, %xmm0
	movaps	%xmm0, -144(%rbp)
	.loc 1 112 230 is_stmt 1 discriminator 1 view .LVU1310
	.loc 1 113 13 discriminator 1 view .LVU1311
	.loc 1 129 7 discriminator 1 view .LVU1312
	.loc 1 130 7 discriminator 1 view .LVU1313
.LVL232:
.LBB252:
	.loc 1 157 13 discriminator 1 view .LVU1314
.LBB234:
	.loc 1 159 3 discriminator 1 view .LVU1315
.L286:
	.loc 1 159 3 is_stmt 0 discriminator 1 view .LVU1316
	movq	-168(%rbp), %rsi
	movq	%r15, %rdi
	call	maybe_free_watcher_list.part.0
.LVL233:
	jmp	.L299
.LVL234:
	.p2align 4,,10
	.p2align 3
.L301:
	.loc 1 159 3 discriminator 1 view .LVU1317
.LBE234:
.LBE252:
.LBB253:
.LBB199:
	.loc 1 58 436 is_stmt 1 view .LVU1318
	.loc 1 58 452 is_stmt 0 view .LVU1319
	movq	16(%r15), %rax
	movq	%rax, -152(%rbp)
	.loc 1 58 439 view .LVU1320
	testq	%rax, %rax
	je	.L303
	movq	%r15, %rdx
	.loc 1 58 464 view .LVU1321
	cmpq	(%rax), %r15
	jne	.L304
.LVL235:
	.loc 1 58 464 view .LVU1322
	jmp	.L303
.LVL236:
	.p2align 4,,10
	.p2align 3
.L342:
	.loc 1 58 464 view .LVU1323
	movq	%rax, %rdx
.LVL237:
	.loc 1 58 464 view .LVU1324
	movq	16(%rax), %rax
.LVL238:
	.loc 1 58 563 is_stmt 1 view .LVU1325
	testq	%rax, %rax
	je	.L341
	movq	%rax, -152(%rbp)
.L304:
	.loc 1 58 588 is_stmt 0 view .LVU1326
	cmpq	%rdx, 8(%rax)
	je	.L342
	.loc 1 58 588 view .LVU1327
	jmp	.L303
.LVL239:
.L339:
	.loc 1 58 588 view .LVU1328
.LBE199:
.LBE253:
.LBB254:
.LBB248:
.LBB244:
.LBB241:
	.loc 1 74 3 is_stmt 1 view .LVU1329
	.loc 1 74 8 is_stmt 0 view .LVU1330
	movl	$526336, %edi
	call	inotify_init1@PLT
.LVL240:
	.loc 1 74 8 view .LVU1331
	movl	%eax, %edx
.LVL241:
	.loc 1 75 3 is_stmt 1 view .LVU1332
	.loc 1 75 6 is_stmt 0 view .LVU1333
	testl	%eax, %eax
	js	.L343
	.loc 1 78 3 is_stmt 1 view .LVU1334
	.loc 1 78 20 is_stmt 0 view .LVU1335
	movl	%eax, 840(%r12)
	.loc 1 79 3 is_stmt 1 view .LVU1336
	leaq	776(%r12), %r9
	leaq	uv__inotify_read(%rip), %rsi
	movq	%r9, %rdi
	movq	%r9, -152(%rbp)
	call	uv__io_init@PLT
.LVL242:
	.loc 1 80 3 view .LVU1337
	movq	-152(%rbp), %r9
	movl	$1, %edx
	movq	%r12, %rdi
	movq	%r9, %rsi
	call	uv__io_start@PLT
.LVL243:
	.loc 1 82 3 view .LVU1338
	.loc 1 82 3 is_stmt 0 view .LVU1339
.LBE241:
.LBE244:
	.loc 1 263 3 is_stmt 1 view .LVU1340
	jmp	.L311
.LVL244:
.L343:
.LBB245:
.LBB242:
	.loc 1 76 5 view .LVU1341
	.loc 1 76 13 is_stmt 0 view .LVU1342
	call	__errno_location@PLT
.LVL245:
	.loc 1 76 12 view .LVU1343
	movl	(%rax), %r12d
.LVL246:
	.loc 1 76 12 view .LVU1344
.LBE242:
.LBE245:
	.loc 1 263 3 is_stmt 1 view .LVU1345
	.loc 1 263 6 is_stmt 0 view .LVU1346
	testl	%r12d, %r12d
	je	.L311
.LBE248:
.LBE254:
	.loc 1 141 9 view .LVU1347
	movq	%r14, %rdi
.LBB255:
.LBB249:
.LBB246:
.LBB243:
	.loc 1 76 13 view .LVU1348
	negl	%r12d
.LVL247:
	.loc 1 76 13 view .LVU1349
.LBE243:
.LBE246:
.LBE249:
.LBE255:
	.loc 1 141 9 is_stmt 1 view .LVU1350
	call	uv__free@PLT
.LVL248:
	.loc 1 142 9 view .LVU1351
	jmp	.L281
.LVL249:
.L341:
.LBB256:
.LBB200:
	.loc 1 142 9 is_stmt 0 view .LVU1352
	movq	$0, -152(%rbp)
	jmp	.L303
.LVL250:
.L338:
	.loc 1 142 9 view .LVU1353
.LBE200:
.LBE256:
	.loc 1 141 9 is_stmt 1 view .LVU1354
	movq	%r14, %rdi
.LBB257:
.LBB250:
	.loc 1 260 12 is_stmt 0 view .LVU1355
	movl	$-22, %r12d
.LBE250:
.LBE257:
	.loc 1 141 9 view .LVU1356
	call	uv__free@PLT
.LVL251:
	.loc 1 142 9 is_stmt 1 view .LVU1357
	jmp	.L281
.LVL252:
.L336:
	.loc 1 121 28 discriminator 1 view .LVU1358
	leaq	__PRETTY_FUNCTION__.10096(%rip), %rcx
	movl	$121, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	call	__assert_fail@PLT
.LVL253:
.L340:
	.loc 1 148 1 is_stmt 0 view .LVU1359
	call	__stack_chk_fail@PLT
.LVL254:
	.cfi_endproc
.LFE105:
	.size	uv__inotify_fork, .-uv__inotify_fork
	.p2align 4
	.globl	uv_fs_event_init
	.type	uv_fs_event_init, @function
uv_fs_event_init:
.LVL255:
.LFB109:
	.loc 1 243 62 is_stmt 1 view -0
	.cfi_startproc
	.loc 1 243 62 is_stmt 0 view .LVU1361
	endbr64
	.loc 1 244 3 is_stmt 1 view .LVU1362
	.loc 1 244 8 view .LVU1363
	.loc 1 244 211 is_stmt 0 view .LVU1364
	leaq	16(%rdi), %rax
	.loc 1 244 37 view .LVU1365
	movq	%rdi, 8(%rsi)
	.loc 1 244 47 is_stmt 1 view .LVU1366
	.loc 1 244 211 is_stmt 0 view .LVU1367
	movq	%rax, 32(%rsi)
	.loc 1 244 341 view .LVU1368
	movq	24(%rdi), %rdx
	.loc 1 244 441 view .LVU1369
	leaq	32(%rsi), %rax
	.loc 1 244 76 view .LVU1370
	movl	$3, 16(%rsi)
	.loc 1 244 93 is_stmt 1 view .LVU1371
	.loc 1 244 123 is_stmt 0 view .LVU1372
	movl	$8, 88(%rsi)
	.loc 1 244 140 is_stmt 1 view .LVU1373
	.loc 1 244 145 view .LVU1374
	.loc 1 244 235 view .LVU1375
	.loc 1 244 298 is_stmt 0 view .LVU1376
	movq	%rdx, 40(%rsi)
	.loc 1 244 348 is_stmt 1 view .LVU1377
	.loc 1 244 438 is_stmt 0 view .LVU1378
	movq	%rax, (%rdx)
	.loc 1 244 481 is_stmt 1 view .LVU1379
	.loc 1 244 528 is_stmt 0 view .LVU1380
	movq	%rax, 24(%rdi)
	.loc 1 244 579 is_stmt 1 view .LVU1381
	.loc 1 244 584 view .LVU1382
	.loc 1 246 1 is_stmt 0 view .LVU1383
	xorl	%eax, %eax
	.loc 1 244 622 view .LVU1384
	movq	$0, 80(%rsi)
	.loc 1 244 13 is_stmt 1 view .LVU1385
	.loc 1 245 3 view .LVU1386
	.loc 1 246 1 is_stmt 0 view .LVU1387
	ret
	.cfi_endproc
.LFE109:
	.size	uv_fs_event_init, .-uv_fs_event_init
	.p2align 4
	.globl	uv_fs_event_start
	.type	uv_fs_event_start, @function
uv_fs_event_start:
.LVL256:
.LFB110:
	.loc 1 252 43 is_stmt 1 view -0
	.cfi_startproc
	.loc 1 252 43 is_stmt 0 view .LVU1389
	endbr64
	.loc 1 253 3 is_stmt 1 view .LVU1390
	.loc 1 254 3 view .LVU1391
	.loc 1 255 3 view .LVU1392
	.loc 1 256 3 view .LVU1393
	.loc 1 257 3 view .LVU1394
	.loc 1 259 3 view .LVU1395
	.loc 1 259 6 is_stmt 0 view .LVU1396
	testb	$4, 88(%rdi)
	jne	.L351
	.loc 1 252 43 view .LVU1397
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	.loc 1 262 3 is_stmt 1 view .LVU1398
	.loc 1 252 43 is_stmt 0 view .LVU1399
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	.loc 1 262 9 view .LVU1400
	movq	8(%rdi), %r14
.LVL257:
.LBB260:
.LBI260:
	.loc 1 68 12 is_stmt 1 view .LVU1401
.LBB261:
	.loc 1 69 3 view .LVU1402
	.loc 1 71 3 view .LVU1403
	.loc 1 71 6 is_stmt 0 view .LVU1404
	cmpl	$-1, 840(%r14)
	je	.L356
.LVL258:
.L348:
	.loc 1 71 6 view .LVU1405
.LBE261:
.LBE260:
	.loc 1 302 1 view .LVU1406
	addq	$8, %rsp
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
.LVL259:
	.loc 1 302 1 view .LVU1407
	popq	%r13
	.cfi_restore 13
.LVL260:
	.loc 1 302 1 view .LVU1408
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
.LVL261:
	.loc 1 302 1 view .LVU1409
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	uv_fs_event_start.part.0
.LVL262:
	.p2align 4,,10
	.p2align 3
.L356:
	.cfi_restore_state
.LBB264:
.LBB262:
	.loc 1 74 3 is_stmt 1 view .LVU1410
	.loc 1 74 8 is_stmt 0 view .LVU1411
	movl	$526336, %edi
.LVL263:
	.loc 1 74 8 view .LVU1412
	call	inotify_init1@PLT
.LVL264:
	.loc 1 74 8 view .LVU1413
	movl	%eax, %edx
.LVL265:
	.loc 1 75 3 is_stmt 1 view .LVU1414
	.loc 1 75 6 is_stmt 0 view .LVU1415
	testl	%eax, %eax
	js	.L357
	.loc 1 78 3 is_stmt 1 view .LVU1416
	.loc 1 78 20 is_stmt 0 view .LVU1417
	movl	%eax, 840(%r14)
	.loc 1 79 3 is_stmt 1 view .LVU1418
	leaq	776(%r14), %rbx
	leaq	uv__inotify_read(%rip), %rsi
	movq	%rbx, %rdi
	call	uv__io_init@PLT
.LVL266:
	.loc 1 80 3 view .LVU1419
	movl	$1, %edx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	uv__io_start@PLT
.LVL267:
	.loc 1 82 3 view .LVU1420
	.loc 1 82 3 is_stmt 0 view .LVU1421
.LBE262:
.LBE264:
	.loc 1 263 3 is_stmt 1 view .LVU1422
	jmp	.L348
.LVL268:
	.p2align 4,,10
	.p2align 3
.L357:
.LBB265:
.LBB263:
	.loc 1 76 5 view .LVU1423
	.loc 1 76 13 is_stmt 0 view .LVU1424
	call	__errno_location@PLT
.LVL269:
	.loc 1 76 12 view .LVU1425
	movl	(%rax), %edx
	.loc 1 76 13 view .LVU1426
	movl	%edx, %eax
	negl	%eax
.LVL270:
	.loc 1 76 13 view .LVU1427
.LBE263:
.LBE265:
	.loc 1 263 3 is_stmt 1 view .LVU1428
	.loc 1 263 6 is_stmt 0 view .LVU1429
	testl	%edx, %edx
	je	.L348
	.loc 1 302 1 view .LVU1430
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
.LVL271:
	.loc 1 302 1 view .LVU1431
	popq	%r13
.LVL272:
	.loc 1 302 1 view .LVU1432
	popq	%r14
	popq	%r15
.LVL273:
	.loc 1 302 1 view .LVU1433
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
.LVL274:
	.p2align 4,,10
	.p2align 3
.L351:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	.loc 1 260 12 view .LVU1434
	movl	$-22, %eax
	.loc 1 302 1 view .LVU1435
	ret
	.cfi_endproc
.LFE110:
	.size	uv_fs_event_start, .-uv_fs_event_start
	.p2align 4
	.globl	uv_fs_event_stop
	.type	uv_fs_event_stop, @function
uv_fs_event_stop:
.LVL275:
.LFB111:
	.loc 1 305 45 is_stmt 1 view -0
	.cfi_startproc
	.loc 1 305 45 is_stmt 0 view .LVU1437
	endbr64
	.loc 1 306 3 is_stmt 1 view .LVU1438
	.loc 1 308 3 view .LVU1439
	.loc 1 308 18 is_stmt 0 view .LVU1440
	movl	88(%rdi), %eax
	.loc 1 308 6 view .LVU1441
	testb	$4, %al
	je	.L375
	.loc 1 311 3 is_stmt 1 view .LVU1442
	.loc 1 305 45 is_stmt 0 view .LVU1443
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	.loc 1 311 7 view .LVU1444
	movq	8(%rdi), %rcx
.LVL276:
	.loc 1 311 7 view .LVU1445
	movl	128(%rdi), %edx
.LVL277:
.LBB274:
.LBI274:
	.loc 1 151 29 is_stmt 1 view .LVU1446
.LBB275:
	.loc 1 152 3 view .LVU1447
	.loc 1 153 3 view .LVU1448
	.loc 1 154 3 view .LVU1449
	.loc 1 154 10 is_stmt 0 view .LVU1450
	movq	832(%rcx), %r8
.LBE275:
.LBE274:
	.loc 1 305 45 view .LVU1451
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
.L378:
.LVL278:
.LBB287:
.LBB284:
.LBB276:
.LBI276:
	.loc 1 58 59 is_stmt 1 view .LVU1452
.LBB277:
	.loc 1 58 135 view .LVU1453
	.loc 1 58 180 view .LVU1454
	.loc 1 58 190 view .LVU1455
	.loc 1 58 196 view .LVU1456
	testq	%r8, %r8
	je	.L361
	.loc 1 58 204 view .LVU1457
.LBB278:
.LBI278:
	.loc 1 50 12 view .LVU1458
.LVL279:
.LBB279:
	.loc 1 52 3 view .LVU1459
	.loc 1 52 6 is_stmt 0 view .LVU1460
	cmpl	64(%r8), %edx
	jl	.L379
	.loc 1 53 3 is_stmt 1 view .LVU1461
	.loc 1 53 6 is_stmt 0 view .LVU1462
	jg	.L380
.LVL280:
	.loc 1 53 6 view .LVU1463
.LBE279:
.LBE278:
.LBE277:
.LBE276:
.LBE284:
.LBE287:
	.loc 1 312 2 is_stmt 1 view .LVU1464
	.loc 1 314 3 view .LVU1465
	.loc 1 316 78 is_stmt 0 view .LVU1466
	movl	%eax, %edx
	.loc 1 315 16 view .LVU1467
	movq	$0, 96(%rdi)
	.loc 1 314 14 view .LVU1468
	movl	$-1, 128(%rdi)
	.loc 1 315 3 is_stmt 1 view .LVU1469
	.loc 1 316 3 view .LVU1470
	.loc 1 316 8 view .LVU1471
	.loc 1 316 62 view .LVU1472
	.loc 1 316 78 is_stmt 0 view .LVU1473
	andl	$-5, %edx
	movl	%edx, 88(%rdi)
	.loc 1 316 100 is_stmt 1 view .LVU1474
	.loc 1 316 103 is_stmt 0 view .LVU1475
	testb	$8, %al
	je	.L368
	.loc 1 316 144 is_stmt 1 discriminator 3 view .LVU1476
	.loc 1 316 149 discriminator 3 view .LVU1477
	.loc 1 316 179 is_stmt 0 discriminator 3 view .LVU1478
	subl	$1, 8(%rcx)
.L368:
	.loc 1 316 191 is_stmt 1 discriminator 5 view .LVU1479
	.loc 1 316 204 discriminator 5 view .LVU1480
	.loc 1 317 3 discriminator 5 view .LVU1481
	.loc 1 317 8 discriminator 5 view .LVU1482
	.loc 1 317 28 is_stmt 0 discriminator 5 view .LVU1483
	movq	120(%rdi), %rax
	.loc 1 317 117 discriminator 5 view .LVU1484
	movq	112(%rdi), %rdx
	.loc 1 317 78 discriminator 5 view .LVU1485
	movq	%rdx, (%rax)
	.loc 1 317 124 is_stmt 1 discriminator 5 view .LVU1486
	.loc 1 317 144 is_stmt 0 discriminator 5 view .LVU1487
	movq	112(%rdi), %rax
	.loc 1 317 233 discriminator 5 view .LVU1488
	movq	120(%rdi), %rdx
	.loc 1 317 194 discriminator 5 view .LVU1489
	movq	%rdx, 8(%rax)
	.loc 1 317 248 is_stmt 1 discriminator 5 view .LVU1490
	.loc 1 319 3 discriminator 5 view .LVU1491
.LVL281:
.LBB288:
.LBI288:
	.loc 1 157 13 discriminator 5 view .LVU1492
.LBB289:
	.loc 1 159 3 discriminator 5 view .LVU1493
	.loc 1 159 6 is_stmt 0 discriminator 5 view .LVU1494
	movl	48(%r8), %eax
	testl	%eax, %eax
	jne	.L370
	.loc 1 159 44 view .LVU1495
	leaq	32(%r8), %rax
	.loc 1 159 23 view .LVU1496
	cmpq	%rax, 32(%r8)
	je	.L381
.LVL282:
.L370:
	.loc 1 159 23 view .LVU1497
.LBE289:
.LBE288:
	.loc 1 322 1 view .LVU1498
	xorl	%eax, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.LVL283:
	.p2align 4,,10
	.p2align 3
.L379:
	.cfi_restore_state
.LBB291:
.LBB285:
.LBB282:
.LBB280:
	.loc 1 58 239 is_stmt 1 view .LVU1499
	.loc 1 58 253 view .LVU1500
	.loc 1 58 257 is_stmt 0 view .LVU1501
	movq	(%r8), %r8
.LVL284:
	.loc 1 58 257 view .LVU1502
	jmp	.L378
.LVL285:
	.p2align 4,,10
	.p2align 3
.L375:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	.loc 1 58 257 view .LVU1503
.LBE280:
.LBE282:
.LBE285:
.LBE291:
	.loc 1 322 1 view .LVU1504
	xorl	%eax, %eax
	ret
.LVL286:
	.p2align 4,,10
	.p2align 3
.L380:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
.LBB292:
.LBB286:
.LBB283:
.LBB281:
	.loc 1 58 287 is_stmt 1 view .LVU1505
	.loc 1 58 301 view .LVU1506
	.loc 1 58 305 is_stmt 0 view .LVU1507
	movq	8(%r8), %r8
.LVL287:
	.loc 1 58 196 is_stmt 1 view .LVU1508
	jmp	.L378
.LVL288:
	.p2align 4,,10
	.p2align 3
.L381:
	.loc 1 58 196 is_stmt 0 view .LVU1509
.LBE281:
.LBE283:
.LBE286:
.LBE292:
.LBB293:
.LBB290:
	movq	8(%rdi), %rsi
	movq	%r8, %rdi
.LVL289:
	.loc 1 58 196 view .LVU1510
	call	maybe_free_watcher_list.part.0
.LVL290:
	.loc 1 58 196 view .LVU1511
	jmp	.L370
.LVL291:
.L361:
	.loc 1 58 196 view .LVU1512
.LBE290:
.LBE293:
	.loc 1 312 2 is_stmt 1 view .LVU1513
	.loc 1 312 22 view .LVU1514
	leaq	__PRETTY_FUNCTION__.10164(%rip), %rcx
.LVL292:
	.loc 1 312 22 is_stmt 0 view .LVU1515
	movl	$312, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC4(%rip), %rdi
.LVL293:
	.loc 1 312 22 view .LVU1516
	call	__assert_fail@PLT
.LVL294:
	.loc 1 312 22 view .LVU1517
	.cfi_endproc
.LFE111:
	.size	uv_fs_event_stop, .-uv_fs_event_stop
	.p2align 4
	.globl	uv__fs_event_close
	.hidden	uv__fs_event_close
	.type	uv__fs_event_close, @function
uv__fs_event_close:
.LVL295:
.LFB112:
	.loc 1 325 48 is_stmt 1 view -0
	.cfi_startproc
	.loc 1 325 48 is_stmt 0 view .LVU1519
	endbr64
	.loc 1 326 3 is_stmt 1 view .LVU1520
.LVL296:
.LBB304:
.LBI304:
	.loc 1 305 5 view .LVU1521
.LBB305:
	.loc 1 306 3 view .LVU1522
	.loc 1 308 3 view .LVU1523
	.loc 1 308 18 is_stmt 0 view .LVU1524
	movl	88(%rdi), %eax
	.loc 1 308 6 view .LVU1525
	testb	$4, %al
	je	.L382
	.loc 1 311 3 is_stmt 1 view .LVU1526
	.loc 1 311 7 is_stmt 0 view .LVU1527
	movq	8(%rdi), %rdx
.LVL297:
	.loc 1 311 7 view .LVU1528
	movl	128(%rdi), %ecx
.LVL298:
.LBB306:
.LBI306:
	.loc 1 151 29 is_stmt 1 view .LVU1529
.LBB307:
	.loc 1 152 3 view .LVU1530
	.loc 1 153 3 view .LVU1531
	.loc 1 154 3 view .LVU1532
	.loc 1 154 10 is_stmt 0 view .LVU1533
	movq	832(%rdx), %r8
.L399:
.LVL299:
.LBB308:
.LBI308:
	.loc 1 58 59 is_stmt 1 view .LVU1534
.LBB309:
	.loc 1 58 135 view .LVU1535
	.loc 1 58 180 view .LVU1536
	.loc 1 58 190 view .LVU1537
	.loc 1 58 196 view .LVU1538
	testq	%r8, %r8
	je	.L385
	.loc 1 58 204 view .LVU1539
.LBB310:
.LBI310:
	.loc 1 50 12 view .LVU1540
.LVL300:
.LBB311:
	.loc 1 52 3 view .LVU1541
	.loc 1 52 6 is_stmt 0 view .LVU1542
	cmpl	64(%r8), %ecx
	jl	.L400
	.loc 1 53 3 is_stmt 1 view .LVU1543
	.loc 1 53 6 is_stmt 0 view .LVU1544
	jg	.L401
.LVL301:
	.loc 1 53 6 view .LVU1545
.LBE311:
.LBE310:
.LBE309:
.LBE308:
.LBE307:
.LBE306:
	.loc 1 312 2 is_stmt 1 view .LVU1546
	.loc 1 314 3 view .LVU1547
	.loc 1 316 78 is_stmt 0 view .LVU1548
	movl	%eax, %ecx
	.loc 1 315 16 view .LVU1549
	movq	$0, 96(%rdi)
	.loc 1 314 14 view .LVU1550
	movl	$-1, 128(%rdi)
	.loc 1 315 3 is_stmt 1 view .LVU1551
	.loc 1 316 3 view .LVU1552
	.loc 1 316 8 view .LVU1553
	.loc 1 316 62 view .LVU1554
	.loc 1 316 78 is_stmt 0 view .LVU1555
	andl	$-5, %ecx
	movl	%ecx, 88(%rdi)
	.loc 1 316 100 is_stmt 1 view .LVU1556
	.loc 1 316 103 is_stmt 0 view .LVU1557
	testb	$8, %al
	je	.L392
	.loc 1 316 144 is_stmt 1 view .LVU1558
	.loc 1 316 149 view .LVU1559
	.loc 1 316 179 is_stmt 0 view .LVU1560
	subl	$1, 8(%rdx)
.L392:
	.loc 1 316 191 is_stmt 1 view .LVU1561
	.loc 1 316 204 view .LVU1562
	.loc 1 317 3 view .LVU1563
	.loc 1 317 8 view .LVU1564
	.loc 1 317 28 is_stmt 0 view .LVU1565
	movq	120(%rdi), %rax
	.loc 1 317 117 view .LVU1566
	movq	112(%rdi), %rdx
.LVL302:
	.loc 1 317 78 view .LVU1567
	movq	%rdx, (%rax)
.LVL303:
	.loc 1 317 124 is_stmt 1 view .LVU1568
	.loc 1 317 144 is_stmt 0 view .LVU1569
	movq	112(%rdi), %rax
	.loc 1 317 233 view .LVU1570
	movq	120(%rdi), %rdx
	.loc 1 317 194 view .LVU1571
	movq	%rdx, 8(%rax)
	.loc 1 317 248 is_stmt 1 view .LVU1572
	.loc 1 319 3 view .LVU1573
.LVL304:
.LBB315:
.LBI315:
	.loc 1 157 13 view .LVU1574
.LBB316:
	.loc 1 159 3 view .LVU1575
	.loc 1 159 6 is_stmt 0 view .LVU1576
	movl	48(%r8), %eax
	testl	%eax, %eax
	jne	.L382
	.loc 1 159 44 view .LVU1577
	leaq	32(%r8), %rax
	.loc 1 159 23 view .LVU1578
	cmpq	%rax, 32(%r8)
	je	.L402
.LVL305:
.L382:
	.loc 1 159 23 view .LVU1579
	ret
.LVL306:
	.p2align 4,,10
	.p2align 3
.L400:
	.loc 1 159 23 view .LVU1580
.LBE316:
.LBE315:
.LBB318:
.LBB314:
.LBB313:
.LBB312:
	.loc 1 58 239 is_stmt 1 view .LVU1581
	.loc 1 58 253 view .LVU1582
	.loc 1 58 257 is_stmt 0 view .LVU1583
	movq	(%r8), %r8
.LVL307:
	.loc 1 58 257 view .LVU1584
	jmp	.L399
.LVL308:
	.p2align 4,,10
	.p2align 3
.L401:
	.loc 1 58 287 is_stmt 1 view .LVU1585
	.loc 1 58 301 view .LVU1586
	.loc 1 58 305 is_stmt 0 view .LVU1587
	movq	8(%r8), %r8
.LVL309:
	.loc 1 58 196 is_stmt 1 view .LVU1588
	jmp	.L399
.LVL310:
	.p2align 4,,10
	.p2align 3
.L402:
	.loc 1 58 196 is_stmt 0 view .LVU1589
.LBE312:
.LBE313:
.LBE314:
.LBE318:
.LBB319:
.LBB317:
	movq	8(%rdi), %rsi
	movq	%r8, %rdi
.LVL311:
	.loc 1 58 196 view .LVU1590
	jmp	maybe_free_watcher_list.part.0
.LVL312:
.L385:
	.loc 1 58 196 view .LVU1591
.LBE317:
.LBE319:
	.loc 1 312 2 is_stmt 1 view .LVU1592
	.loc 1 312 22 view .LVU1593
.LBE305:
.LBE304:
	.loc 1 325 48 is_stmt 0 view .LVU1594
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
.LBB322:
.LBB320:
	.loc 1 312 22 view .LVU1595
	leaq	__PRETTY_FUNCTION__.10164(%rip), %rcx
	movl	$312, %edx
.LVL313:
	.loc 1 312 22 view .LVU1596
	leaq	.LC0(%rip), %rsi
	leaq	.LC4(%rip), %rdi
.LVL314:
	.loc 1 312 22 view .LVU1597
.LBE320:
.LBE322:
	.loc 1 325 48 view .LVU1598
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
.LBB323:
.LBB321:
	.loc 1 312 22 view .LVU1599
	call	__assert_fail@PLT
.LVL315:
.LBE321:
.LBE323:
	.cfi_endproc
.LFE112:
	.size	uv__fs_event_close, .-uv__fs_event_close
	.section	.rodata
	.align 16
	.type	__PRETTY_FUNCTION__.10164, @object
	.size	__PRETTY_FUNCTION__.10164, 17
__PRETTY_FUNCTION__.10164:
	.string	"uv_fs_event_stop"
	.align 16
	.type	__PRETTY_FUNCTION__.10132, @object
	.size	__PRETTY_FUNCTION__.10132, 17
__PRETTY_FUNCTION__.10132:
	.string	"uv__inotify_read"
	.align 16
	.type	__PRETTY_FUNCTION__.10096, @object
	.size	__PRETTY_FUNCTION__.10096, 17
__PRETTY_FUNCTION__.10096:
	.string	"uv__inotify_fork"
	.text
.Letext0:
	.file 5 "/usr/include/errno.h"
	.file 6 "/usr/lib/gcc/x86_64-linux-gnu/9/include/stddef.h"
	.file 7 "/usr/include/x86_64-linux-gnu/bits/types.h"
	.file 8 "/usr/include/x86_64-linux-gnu/bits/types/struct_FILE.h"
	.file 9 "/usr/include/x86_64-linux-gnu/bits/types/FILE.h"
	.file 10 "/usr/include/stdio.h"
	.file 11 "/usr/include/x86_64-linux-gnu/bits/sys_errlist.h"
	.file 12 "/usr/include/x86_64-linux-gnu/bits/stdint-uintn.h"
	.file 13 "/usr/include/x86_64-linux-gnu/bits/thread-shared-types.h"
	.file 14 "/usr/include/x86_64-linux-gnu/bits/struct_mutex.h"
	.file 15 "/usr/include/x86_64-linux-gnu/bits/struct_rwlock.h"
	.file 16 "/usr/include/x86_64-linux-gnu/bits/pthreadtypes.h"
	.file 17 "/usr/include/x86_64-linux-gnu/bits/sockaddr.h"
	.file 18 "/usr/include/x86_64-linux-gnu/bits/socket.h"
	.file 19 "/usr/include/netinet/in.h"
	.file 20 "/usr/include/signal.h"
	.file 21 "/usr/include/time.h"
	.file 22 "../deps/uv/include/uv.h"
	.file 23 "../deps/uv/include/uv/unix.h"
	.file 24 "../deps/uv/src/queue.h"
	.file 25 "../deps/uv/src/uv-common.h"
	.file 26 "/usr/include/x86_64-linux-gnu/bits/inotify.h"
	.file 27 "/usr/include/x86_64-linux-gnu/sys/inotify.h"
	.file 28 "/usr/include/unistd.h"
	.file 29 "/usr/include/x86_64-linux-gnu/bits/getopt_core.h"
	.file 30 "/usr/include/assert.h"
	.file 31 "/usr/include/string.h"
	.file 32 "<built-in>"
	.section	.debug_info,"",@progbits
.Ldebug_info0:
	.long	0x2e6a
	.value	0x4
	.long	.Ldebug_abbrev0
	.byte	0x8
	.uleb128 0x1
	.long	.LASF436
	.byte	0x1
	.long	.LASF437
	.long	.LASF438
	.quad	.Ltext0
	.quad	.Letext0-.Ltext0
	.long	.Ldebug_line0
	.uleb128 0x2
	.long	.LASF0
	.byte	0x5
	.byte	0x2d
	.byte	0xe
	.long	0x39
	.uleb128 0x3
	.byte	0x8
	.long	0x3f
	.uleb128 0x4
	.byte	0x1
	.byte	0x6
	.long	.LASF2
	.uleb128 0x5
	.long	0x3f
	.uleb128 0x2
	.long	.LASF1
	.byte	0x5
	.byte	0x2e
	.byte	0xe
	.long	0x39
	.uleb128 0x6
	.byte	0x4
	.byte	0x5
	.string	"int"
	.uleb128 0x4
	.byte	0x8
	.byte	0x5
	.long	.LASF3
	.uleb128 0x7
	.long	.LASF9
	.byte	0x6
	.byte	0xd1
	.byte	0x1b
	.long	0x71
	.uleb128 0x4
	.byte	0x8
	.byte	0x7
	.long	.LASF4
	.uleb128 0x4
	.byte	0x4
	.byte	0x7
	.long	.LASF5
	.uleb128 0x8
	.byte	0x8
	.uleb128 0x9
	.long	0x7f
	.uleb128 0x4
	.byte	0x1
	.byte	0x8
	.long	.LASF6
	.uleb128 0x4
	.byte	0x2
	.byte	0x7
	.long	.LASF7
	.uleb128 0x4
	.byte	0x1
	.byte	0x6
	.long	.LASF8
	.uleb128 0x7
	.long	.LASF10
	.byte	0x7
	.byte	0x26
	.byte	0x17
	.long	0x86
	.uleb128 0x4
	.byte	0x2
	.byte	0x5
	.long	.LASF11
	.uleb128 0x7
	.long	.LASF12
	.byte	0x7
	.byte	0x28
	.byte	0x1c
	.long	0x8d
	.uleb128 0x7
	.long	.LASF13
	.byte	0x7
	.byte	0x2a
	.byte	0x16
	.long	0x78
	.uleb128 0x7
	.long	.LASF14
	.byte	0x7
	.byte	0x2d
	.byte	0x1b
	.long	0x71
	.uleb128 0x7
	.long	.LASF15
	.byte	0x7
	.byte	0x98
	.byte	0x12
	.long	0x5e
	.uleb128 0x7
	.long	.LASF16
	.byte	0x7
	.byte	0x99
	.byte	0x12
	.long	0x5e
	.uleb128 0xa
	.long	0x57
	.long	0xfa
	.uleb128 0xb
	.long	0x71
	.byte	0x1
	.byte	0
	.uleb128 0x7
	.long	.LASF17
	.byte	0x7
	.byte	0xc1
	.byte	0x12
	.long	0x5e
	.uleb128 0xc
	.long	.LASF63
	.byte	0xd8
	.byte	0x8
	.byte	0x31
	.byte	0x8
	.long	0x28d
	.uleb128 0xd
	.long	.LASF18
	.byte	0x8
	.byte	0x33
	.byte	0x7
	.long	0x57
	.byte	0
	.uleb128 0xd
	.long	.LASF19
	.byte	0x8
	.byte	0x36
	.byte	0x9
	.long	0x39
	.byte	0x8
	.uleb128 0xd
	.long	.LASF20
	.byte	0x8
	.byte	0x37
	.byte	0x9
	.long	0x39
	.byte	0x10
	.uleb128 0xd
	.long	.LASF21
	.byte	0x8
	.byte	0x38
	.byte	0x9
	.long	0x39
	.byte	0x18
	.uleb128 0xd
	.long	.LASF22
	.byte	0x8
	.byte	0x39
	.byte	0x9
	.long	0x39
	.byte	0x20
	.uleb128 0xd
	.long	.LASF23
	.byte	0x8
	.byte	0x3a
	.byte	0x9
	.long	0x39
	.byte	0x28
	.uleb128 0xd
	.long	.LASF24
	.byte	0x8
	.byte	0x3b
	.byte	0x9
	.long	0x39
	.byte	0x30
	.uleb128 0xd
	.long	.LASF25
	.byte	0x8
	.byte	0x3c
	.byte	0x9
	.long	0x39
	.byte	0x38
	.uleb128 0xd
	.long	.LASF26
	.byte	0x8
	.byte	0x3d
	.byte	0x9
	.long	0x39
	.byte	0x40
	.uleb128 0xd
	.long	.LASF27
	.byte	0x8
	.byte	0x40
	.byte	0x9
	.long	0x39
	.byte	0x48
	.uleb128 0xd
	.long	.LASF28
	.byte	0x8
	.byte	0x41
	.byte	0x9
	.long	0x39
	.byte	0x50
	.uleb128 0xd
	.long	.LASF29
	.byte	0x8
	.byte	0x42
	.byte	0x9
	.long	0x39
	.byte	0x58
	.uleb128 0xd
	.long	.LASF30
	.byte	0x8
	.byte	0x44
	.byte	0x16
	.long	0x2a6
	.byte	0x60
	.uleb128 0xd
	.long	.LASF31
	.byte	0x8
	.byte	0x46
	.byte	0x14
	.long	0x2ac
	.byte	0x68
	.uleb128 0xd
	.long	.LASF32
	.byte	0x8
	.byte	0x48
	.byte	0x7
	.long	0x57
	.byte	0x70
	.uleb128 0xd
	.long	.LASF33
	.byte	0x8
	.byte	0x49
	.byte	0x7
	.long	0x57
	.byte	0x74
	.uleb128 0xd
	.long	.LASF34
	.byte	0x8
	.byte	0x4a
	.byte	0xb
	.long	0xd2
	.byte	0x78
	.uleb128 0xd
	.long	.LASF35
	.byte	0x8
	.byte	0x4d
	.byte	0x12
	.long	0x8d
	.byte	0x80
	.uleb128 0xd
	.long	.LASF36
	.byte	0x8
	.byte	0x4e
	.byte	0xf
	.long	0x94
	.byte	0x82
	.uleb128 0xd
	.long	.LASF37
	.byte	0x8
	.byte	0x4f
	.byte	0x8
	.long	0x2b2
	.byte	0x83
	.uleb128 0xd
	.long	.LASF38
	.byte	0x8
	.byte	0x51
	.byte	0xf
	.long	0x2c2
	.byte	0x88
	.uleb128 0xd
	.long	.LASF39
	.byte	0x8
	.byte	0x59
	.byte	0xd
	.long	0xde
	.byte	0x90
	.uleb128 0xd
	.long	.LASF40
	.byte	0x8
	.byte	0x5b
	.byte	0x17
	.long	0x2cd
	.byte	0x98
	.uleb128 0xd
	.long	.LASF41
	.byte	0x8
	.byte	0x5c
	.byte	0x19
	.long	0x2d8
	.byte	0xa0
	.uleb128 0xd
	.long	.LASF42
	.byte	0x8
	.byte	0x5d
	.byte	0x14
	.long	0x2ac
	.byte	0xa8
	.uleb128 0xd
	.long	.LASF43
	.byte	0x8
	.byte	0x5e
	.byte	0x9
	.long	0x7f
	.byte	0xb0
	.uleb128 0xd
	.long	.LASF44
	.byte	0x8
	.byte	0x5f
	.byte	0xa
	.long	0x65
	.byte	0xb8
	.uleb128 0xd
	.long	.LASF45
	.byte	0x8
	.byte	0x60
	.byte	0x7
	.long	0x57
	.byte	0xc0
	.uleb128 0xd
	.long	.LASF46
	.byte	0x8
	.byte	0x62
	.byte	0x8
	.long	0x2de
	.byte	0xc4
	.byte	0
	.uleb128 0x7
	.long	.LASF47
	.byte	0x9
	.byte	0x7
	.byte	0x19
	.long	0x106
	.uleb128 0xe
	.long	.LASF439
	.byte	0x8
	.byte	0x2b
	.byte	0xe
	.uleb128 0xf
	.long	.LASF48
	.uleb128 0x3
	.byte	0x8
	.long	0x2a1
	.uleb128 0x3
	.byte	0x8
	.long	0x106
	.uleb128 0xa
	.long	0x3f
	.long	0x2c2
	.uleb128 0xb
	.long	0x71
	.byte	0
	.byte	0
	.uleb128 0x3
	.byte	0x8
	.long	0x299
	.uleb128 0xf
	.long	.LASF49
	.uleb128 0x3
	.byte	0x8
	.long	0x2c8
	.uleb128 0xf
	.long	.LASF50
	.uleb128 0x3
	.byte	0x8
	.long	0x2d3
	.uleb128 0xa
	.long	0x3f
	.long	0x2ee
	.uleb128 0xb
	.long	0x71
	.byte	0x13
	.byte	0
	.uleb128 0x3
	.byte	0x8
	.long	0x46
	.uleb128 0x5
	.long	0x2ee
	.uleb128 0x7
	.long	.LASF51
	.byte	0xa
	.byte	0x4d
	.byte	0x13
	.long	0xfa
	.uleb128 0x2
	.long	.LASF52
	.byte	0xa
	.byte	0x89
	.byte	0xe
	.long	0x311
	.uleb128 0x3
	.byte	0x8
	.long	0x28d
	.uleb128 0x2
	.long	.LASF53
	.byte	0xa
	.byte	0x8a
	.byte	0xe
	.long	0x311
	.uleb128 0x2
	.long	.LASF54
	.byte	0xa
	.byte	0x8b
	.byte	0xe
	.long	0x311
	.uleb128 0x2
	.long	.LASF55
	.byte	0xb
	.byte	0x1a
	.byte	0xc
	.long	0x57
	.uleb128 0xa
	.long	0x2f4
	.long	0x346
	.uleb128 0x10
	.byte	0
	.uleb128 0x5
	.long	0x33b
	.uleb128 0x2
	.long	.LASF56
	.byte	0xb
	.byte	0x1b
	.byte	0x1a
	.long	0x346
	.uleb128 0x2
	.long	.LASF57
	.byte	0xb
	.byte	0x1e
	.byte	0xc
	.long	0x57
	.uleb128 0x2
	.long	.LASF58
	.byte	0xb
	.byte	0x1f
	.byte	0x1a
	.long	0x346
	.uleb128 0x7
	.long	.LASF59
	.byte	0xc
	.byte	0x18
	.byte	0x13
	.long	0x9b
	.uleb128 0x7
	.long	.LASF60
	.byte	0xc
	.byte	0x19
	.byte	0x14
	.long	0xae
	.uleb128 0x7
	.long	.LASF61
	.byte	0xc
	.byte	0x1a
	.byte	0x14
	.long	0xba
	.uleb128 0x7
	.long	.LASF62
	.byte	0xc
	.byte	0x1b
	.byte	0x14
	.long	0xc6
	.uleb128 0xc
	.long	.LASF64
	.byte	0x10
	.byte	0xd
	.byte	0x31
	.byte	0x10
	.long	0x3c7
	.uleb128 0xd
	.long	.LASF65
	.byte	0xd
	.byte	0x33
	.byte	0x23
	.long	0x3c7
	.byte	0
	.uleb128 0xd
	.long	.LASF66
	.byte	0xd
	.byte	0x34
	.byte	0x23
	.long	0x3c7
	.byte	0x8
	.byte	0
	.uleb128 0x3
	.byte	0x8
	.long	0x39f
	.uleb128 0x7
	.long	.LASF67
	.byte	0xd
	.byte	0x35
	.byte	0x3
	.long	0x39f
	.uleb128 0xc
	.long	.LASF68
	.byte	0x28
	.byte	0xe
	.byte	0x16
	.byte	0x8
	.long	0x44f
	.uleb128 0xd
	.long	.LASF69
	.byte	0xe
	.byte	0x18
	.byte	0x7
	.long	0x57
	.byte	0
	.uleb128 0xd
	.long	.LASF70
	.byte	0xe
	.byte	0x19
	.byte	0x10
	.long	0x78
	.byte	0x4
	.uleb128 0xd
	.long	.LASF71
	.byte	0xe
	.byte	0x1a
	.byte	0x7
	.long	0x57
	.byte	0x8
	.uleb128 0xd
	.long	.LASF72
	.byte	0xe
	.byte	0x1c
	.byte	0x10
	.long	0x78
	.byte	0xc
	.uleb128 0xd
	.long	.LASF73
	.byte	0xe
	.byte	0x20
	.byte	0x7
	.long	0x57
	.byte	0x10
	.uleb128 0xd
	.long	.LASF74
	.byte	0xe
	.byte	0x22
	.byte	0x9
	.long	0xa7
	.byte	0x14
	.uleb128 0xd
	.long	.LASF75
	.byte	0xe
	.byte	0x23
	.byte	0x9
	.long	0xa7
	.byte	0x16
	.uleb128 0xd
	.long	.LASF76
	.byte	0xe
	.byte	0x24
	.byte	0x14
	.long	0x3cd
	.byte	0x18
	.byte	0
	.uleb128 0xc
	.long	.LASF77
	.byte	0x38
	.byte	0xf
	.byte	0x17
	.byte	0x8
	.long	0x4f9
	.uleb128 0xd
	.long	.LASF78
	.byte	0xf
	.byte	0x19
	.byte	0x10
	.long	0x78
	.byte	0
	.uleb128 0xd
	.long	.LASF79
	.byte	0xf
	.byte	0x1a
	.byte	0x10
	.long	0x78
	.byte	0x4
	.uleb128 0xd
	.long	.LASF80
	.byte	0xf
	.byte	0x1b
	.byte	0x10
	.long	0x78
	.byte	0x8
	.uleb128 0xd
	.long	.LASF81
	.byte	0xf
	.byte	0x1c
	.byte	0x10
	.long	0x78
	.byte	0xc
	.uleb128 0xd
	.long	.LASF82
	.byte	0xf
	.byte	0x1d
	.byte	0x10
	.long	0x78
	.byte	0x10
	.uleb128 0xd
	.long	.LASF83
	.byte	0xf
	.byte	0x1e
	.byte	0x10
	.long	0x78
	.byte	0x14
	.uleb128 0xd
	.long	.LASF84
	.byte	0xf
	.byte	0x20
	.byte	0x7
	.long	0x57
	.byte	0x18
	.uleb128 0xd
	.long	.LASF85
	.byte	0xf
	.byte	0x21
	.byte	0x7
	.long	0x57
	.byte	0x1c
	.uleb128 0xd
	.long	.LASF86
	.byte	0xf
	.byte	0x22
	.byte	0xf
	.long	0x94
	.byte	0x20
	.uleb128 0xd
	.long	.LASF87
	.byte	0xf
	.byte	0x27
	.byte	0x11
	.long	0x4f9
	.byte	0x21
	.uleb128 0xd
	.long	.LASF88
	.byte	0xf
	.byte	0x2a
	.byte	0x15
	.long	0x71
	.byte	0x28
	.uleb128 0xd
	.long	.LASF89
	.byte	0xf
	.byte	0x2d
	.byte	0x10
	.long	0x78
	.byte	0x30
	.byte	0
	.uleb128 0xa
	.long	0x86
	.long	0x509
	.uleb128 0xb
	.long	0x71
	.byte	0x6
	.byte	0
	.uleb128 0x4
	.byte	0x8
	.byte	0x7
	.long	.LASF90
	.uleb128 0xa
	.long	0x3f
	.long	0x520
	.uleb128 0xb
	.long	0x71
	.byte	0x37
	.byte	0
	.uleb128 0x11
	.byte	0x28
	.byte	0x10
	.byte	0x43
	.byte	0x9
	.long	0x54e
	.uleb128 0x12
	.long	.LASF91
	.byte	0x10
	.byte	0x45
	.byte	0x1c
	.long	0x3d9
	.uleb128 0x12
	.long	.LASF92
	.byte	0x10
	.byte	0x46
	.byte	0x8
	.long	0x54e
	.uleb128 0x12
	.long	.LASF93
	.byte	0x10
	.byte	0x47
	.byte	0xc
	.long	0x5e
	.byte	0
	.uleb128 0xa
	.long	0x3f
	.long	0x55e
	.uleb128 0xb
	.long	0x71
	.byte	0x27
	.byte	0
	.uleb128 0x7
	.long	.LASF94
	.byte	0x10
	.byte	0x48
	.byte	0x3
	.long	0x520
	.uleb128 0x4
	.byte	0x8
	.byte	0x5
	.long	.LASF95
	.uleb128 0x11
	.byte	0x38
	.byte	0x10
	.byte	0x56
	.byte	0x9
	.long	0x59f
	.uleb128 0x12
	.long	.LASF91
	.byte	0x10
	.byte	0x58
	.byte	0x22
	.long	0x44f
	.uleb128 0x12
	.long	.LASF92
	.byte	0x10
	.byte	0x59
	.byte	0x8
	.long	0x510
	.uleb128 0x12
	.long	.LASF93
	.byte	0x10
	.byte	0x5a
	.byte	0xc
	.long	0x5e
	.byte	0
	.uleb128 0x7
	.long	.LASF96
	.byte	0x10
	.byte	0x5b
	.byte	0x3
	.long	0x571
	.uleb128 0x7
	.long	.LASF97
	.byte	0x11
	.byte	0x1c
	.byte	0x1c
	.long	0x8d
	.uleb128 0xc
	.long	.LASF98
	.byte	0x10
	.byte	0x12
	.byte	0xb2
	.byte	0x8
	.long	0x5df
	.uleb128 0xd
	.long	.LASF99
	.byte	0x12
	.byte	0xb4
	.byte	0x11
	.long	0x5ab
	.byte	0
	.uleb128 0xd
	.long	.LASF100
	.byte	0x12
	.byte	0xb5
	.byte	0xa
	.long	0x5e4
	.byte	0x2
	.byte	0
	.uleb128 0x5
	.long	0x5b7
	.uleb128 0xa
	.long	0x3f
	.long	0x5f4
	.uleb128 0xb
	.long	0x71
	.byte	0xd
	.byte	0
	.uleb128 0x3
	.byte	0x8
	.long	0x5b7
	.uleb128 0x9
	.long	0x5f4
	.uleb128 0xf
	.long	.LASF101
	.uleb128 0x5
	.long	0x5ff
	.uleb128 0x3
	.byte	0x8
	.long	0x5ff
	.uleb128 0x9
	.long	0x609
	.uleb128 0xf
	.long	.LASF102
	.uleb128 0x5
	.long	0x614
	.uleb128 0x3
	.byte	0x8
	.long	0x614
	.uleb128 0x9
	.long	0x61e
	.uleb128 0xf
	.long	.LASF103
	.uleb128 0x5
	.long	0x629
	.uleb128 0x3
	.byte	0x8
	.long	0x629
	.uleb128 0x9
	.long	0x633
	.uleb128 0xf
	.long	.LASF104
	.uleb128 0x5
	.long	0x63e
	.uleb128 0x3
	.byte	0x8
	.long	0x63e
	.uleb128 0x9
	.long	0x648
	.uleb128 0xc
	.long	.LASF105
	.byte	0x10
	.byte	0x13
	.byte	0xee
	.byte	0x8
	.long	0x695
	.uleb128 0xd
	.long	.LASF106
	.byte	0x13
	.byte	0xf0
	.byte	0x11
	.long	0x5ab
	.byte	0
	.uleb128 0xd
	.long	.LASF107
	.byte	0x13
	.byte	0xf1
	.byte	0xf
	.long	0x83c
	.byte	0x2
	.uleb128 0xd
	.long	.LASF108
	.byte	0x13
	.byte	0xf2
	.byte	0x14
	.long	0x821
	.byte	0x4
	.uleb128 0xd
	.long	.LASF109
	.byte	0x13
	.byte	0xf5
	.byte	0x13
	.long	0x8de
	.byte	0x8
	.byte	0
	.uleb128 0x5
	.long	0x653
	.uleb128 0x3
	.byte	0x8
	.long	0x653
	.uleb128 0x9
	.long	0x69a
	.uleb128 0xc
	.long	.LASF110
	.byte	0x1c
	.byte	0x13
	.byte	0xfd
	.byte	0x8
	.long	0x6f8
	.uleb128 0xd
	.long	.LASF111
	.byte	0x13
	.byte	0xff
	.byte	0x11
	.long	0x5ab
	.byte	0
	.uleb128 0x13
	.long	.LASF112
	.byte	0x13
	.value	0x100
	.byte	0xf
	.long	0x83c
	.byte	0x2
	.uleb128 0x13
	.long	.LASF113
	.byte	0x13
	.value	0x101
	.byte	0xe
	.long	0x387
	.byte	0x4
	.uleb128 0x13
	.long	.LASF114
	.byte	0x13
	.value	0x102
	.byte	0x15
	.long	0x8a6
	.byte	0x8
	.uleb128 0x13
	.long	.LASF115
	.byte	0x13
	.value	0x103
	.byte	0xe
	.long	0x387
	.byte	0x18
	.byte	0
	.uleb128 0x5
	.long	0x6a5
	.uleb128 0x3
	.byte	0x8
	.long	0x6a5
	.uleb128 0x9
	.long	0x6fd
	.uleb128 0xf
	.long	.LASF116
	.uleb128 0x5
	.long	0x708
	.uleb128 0x3
	.byte	0x8
	.long	0x708
	.uleb128 0x9
	.long	0x712
	.uleb128 0xf
	.long	.LASF117
	.uleb128 0x5
	.long	0x71d
	.uleb128 0x3
	.byte	0x8
	.long	0x71d
	.uleb128 0x9
	.long	0x727
	.uleb128 0xf
	.long	.LASF118
	.uleb128 0x5
	.long	0x732
	.uleb128 0x3
	.byte	0x8
	.long	0x732
	.uleb128 0x9
	.long	0x73c
	.uleb128 0xf
	.long	.LASF119
	.uleb128 0x5
	.long	0x747
	.uleb128 0x3
	.byte	0x8
	.long	0x747
	.uleb128 0x9
	.long	0x751
	.uleb128 0xf
	.long	.LASF120
	.uleb128 0x5
	.long	0x75c
	.uleb128 0x3
	.byte	0x8
	.long	0x75c
	.uleb128 0x9
	.long	0x766
	.uleb128 0xf
	.long	.LASF121
	.uleb128 0x5
	.long	0x771
	.uleb128 0x3
	.byte	0x8
	.long	0x771
	.uleb128 0x9
	.long	0x77b
	.uleb128 0x3
	.byte	0x8
	.long	0x5df
	.uleb128 0x9
	.long	0x786
	.uleb128 0x3
	.byte	0x8
	.long	0x604
	.uleb128 0x9
	.long	0x791
	.uleb128 0x3
	.byte	0x8
	.long	0x619
	.uleb128 0x9
	.long	0x79c
	.uleb128 0x3
	.byte	0x8
	.long	0x62e
	.uleb128 0x9
	.long	0x7a7
	.uleb128 0x3
	.byte	0x8
	.long	0x643
	.uleb128 0x9
	.long	0x7b2
	.uleb128 0x3
	.byte	0x8
	.long	0x695
	.uleb128 0x9
	.long	0x7bd
	.uleb128 0x3
	.byte	0x8
	.long	0x6f8
	.uleb128 0x9
	.long	0x7c8
	.uleb128 0x3
	.byte	0x8
	.long	0x70d
	.uleb128 0x9
	.long	0x7d3
	.uleb128 0x3
	.byte	0x8
	.long	0x722
	.uleb128 0x9
	.long	0x7de
	.uleb128 0x3
	.byte	0x8
	.long	0x737
	.uleb128 0x9
	.long	0x7e9
	.uleb128 0x3
	.byte	0x8
	.long	0x74c
	.uleb128 0x9
	.long	0x7f4
	.uleb128 0x3
	.byte	0x8
	.long	0x761
	.uleb128 0x9
	.long	0x7ff
	.uleb128 0x3
	.byte	0x8
	.long	0x776
	.uleb128 0x9
	.long	0x80a
	.uleb128 0x7
	.long	.LASF122
	.byte	0x13
	.byte	0x1e
	.byte	0x12
	.long	0x387
	.uleb128 0xc
	.long	.LASF123
	.byte	0x4
	.byte	0x13
	.byte	0x1f
	.byte	0x8
	.long	0x83c
	.uleb128 0xd
	.long	.LASF124
	.byte	0x13
	.byte	0x21
	.byte	0xf
	.long	0x815
	.byte	0
	.byte	0
	.uleb128 0x7
	.long	.LASF125
	.byte	0x13
	.byte	0x77
	.byte	0x12
	.long	0x37b
	.uleb128 0x11
	.byte	0x10
	.byte	0x13
	.byte	0xd6
	.byte	0x5
	.long	0x876
	.uleb128 0x12
	.long	.LASF126
	.byte	0x13
	.byte	0xd8
	.byte	0xa
	.long	0x876
	.uleb128 0x12
	.long	.LASF127
	.byte	0x13
	.byte	0xd9
	.byte	0xb
	.long	0x886
	.uleb128 0x12
	.long	.LASF128
	.byte	0x13
	.byte	0xda
	.byte	0xb
	.long	0x896
	.byte	0
	.uleb128 0xa
	.long	0x36f
	.long	0x886
	.uleb128 0xb
	.long	0x71
	.byte	0xf
	.byte	0
	.uleb128 0xa
	.long	0x37b
	.long	0x896
	.uleb128 0xb
	.long	0x71
	.byte	0x7
	.byte	0
	.uleb128 0xa
	.long	0x387
	.long	0x8a6
	.uleb128 0xb
	.long	0x71
	.byte	0x3
	.byte	0
	.uleb128 0xc
	.long	.LASF129
	.byte	0x10
	.byte	0x13
	.byte	0xd4
	.byte	0x8
	.long	0x8c1
	.uleb128 0xd
	.long	.LASF130
	.byte	0x13
	.byte	0xdb
	.byte	0x9
	.long	0x848
	.byte	0
	.byte	0
	.uleb128 0x5
	.long	0x8a6
	.uleb128 0x2
	.long	.LASF131
	.byte	0x13
	.byte	0xe4
	.byte	0x1e
	.long	0x8c1
	.uleb128 0x2
	.long	.LASF132
	.byte	0x13
	.byte	0xe5
	.byte	0x1e
	.long	0x8c1
	.uleb128 0xa
	.long	0x86
	.long	0x8ee
	.uleb128 0xb
	.long	0x71
	.byte	0x7
	.byte	0
	.uleb128 0x3
	.byte	0x8
	.long	0x39
	.uleb128 0x14
	.uleb128 0x3
	.byte	0x8
	.long	0x8f4
	.uleb128 0xa
	.long	0x2f4
	.long	0x90b
	.uleb128 0xb
	.long	0x71
	.byte	0x40
	.byte	0
	.uleb128 0x5
	.long	0x8fb
	.uleb128 0x15
	.long	.LASF133
	.byte	0x14
	.value	0x11e
	.byte	0x1a
	.long	0x90b
	.uleb128 0x15
	.long	.LASF134
	.byte	0x14
	.value	0x11f
	.byte	0x1a
	.long	0x90b
	.uleb128 0xa
	.long	0x39
	.long	0x93a
	.uleb128 0xb
	.long	0x71
	.byte	0x1
	.byte	0
	.uleb128 0x2
	.long	.LASF135
	.byte	0x15
	.byte	0x9f
	.byte	0xe
	.long	0x92a
	.uleb128 0x2
	.long	.LASF136
	.byte	0x15
	.byte	0xa0
	.byte	0xc
	.long	0x57
	.uleb128 0x2
	.long	.LASF137
	.byte	0x15
	.byte	0xa1
	.byte	0x11
	.long	0x5e
	.uleb128 0x2
	.long	.LASF138
	.byte	0x15
	.byte	0xa6
	.byte	0xe
	.long	0x92a
	.uleb128 0x2
	.long	.LASF139
	.byte	0x15
	.byte	0xae
	.byte	0xc
	.long	0x57
	.uleb128 0x2
	.long	.LASF140
	.byte	0x15
	.byte	0xaf
	.byte	0x11
	.long	0x5e
	.uleb128 0x15
	.long	.LASF141
	.byte	0x15
	.value	0x112
	.byte	0xc
	.long	0x57
	.uleb128 0xa
	.long	0x7f
	.long	0x99f
	.uleb128 0xb
	.long	0x71
	.byte	0x3
	.byte	0
	.uleb128 0x16
	.long	.LASF142
	.value	0x350
	.byte	0x16
	.value	0x6ea
	.byte	0x8
	.long	0xbbe
	.uleb128 0x13
	.long	.LASF143
	.byte	0x16
	.value	0x6ec
	.byte	0x9
	.long	0x7f
	.byte	0
	.uleb128 0x13
	.long	.LASF144
	.byte	0x16
	.value	0x6ee
	.byte	0x10
	.long	0x78
	.byte	0x8
	.uleb128 0x13
	.long	.LASF145
	.byte	0x16
	.value	0x6ef
	.byte	0x9
	.long	0xbc4
	.byte	0x10
	.uleb128 0x13
	.long	.LASF146
	.byte	0x16
	.value	0x6f3
	.byte	0x5
	.long	0x13af
	.byte	0x20
	.uleb128 0x13
	.long	.LASF147
	.byte	0x16
	.value	0x6f5
	.byte	0x10
	.long	0x78
	.byte	0x30
	.uleb128 0x13
	.long	.LASF148
	.byte	0x16
	.value	0x6f6
	.byte	0x11
	.long	0x71
	.byte	0x38
	.uleb128 0x13
	.long	.LASF149
	.byte	0x16
	.value	0x6f6
	.byte	0x1c
	.long	0x57
	.byte	0x40
	.uleb128 0x13
	.long	.LASF150
	.byte	0x16
	.value	0x6f6
	.byte	0x2e
	.long	0xbc4
	.byte	0x48
	.uleb128 0x13
	.long	.LASF151
	.byte	0x16
	.value	0x6f6
	.byte	0x46
	.long	0xbc4
	.byte	0x58
	.uleb128 0x13
	.long	.LASF152
	.byte	0x16
	.value	0x6f6
	.byte	0x63
	.long	0x13fe
	.byte	0x68
	.uleb128 0x13
	.long	.LASF153
	.byte	0x16
	.value	0x6f6
	.byte	0x7a
	.long	0x78
	.byte	0x70
	.uleb128 0x13
	.long	.LASF154
	.byte	0x16
	.value	0x6f6
	.byte	0x92
	.long	0x78
	.byte	0x74
	.uleb128 0x17
	.string	"wq"
	.byte	0x16
	.value	0x6f6
	.byte	0x9e
	.long	0xbc4
	.byte	0x78
	.uleb128 0x13
	.long	.LASF155
	.byte	0x16
	.value	0x6f6
	.byte	0xb0
	.long	0xc67
	.byte	0x88
	.uleb128 0x13
	.long	.LASF156
	.byte	0x16
	.value	0x6f6
	.byte	0xc5
	.long	0xfc4
	.byte	0xb0
	.uleb128 0x18
	.long	.LASF157
	.byte	0x16
	.value	0x6f6
	.byte	0xdb
	.long	0xc73
	.value	0x130
	.uleb128 0x18
	.long	.LASF158
	.byte	0x16
	.value	0x6f6
	.byte	0xf6
	.long	0x1205
	.value	0x168
	.uleb128 0x19
	.long	.LASF159
	.byte	0x16
	.value	0x6f6
	.value	0x10d
	.long	0xbc4
	.value	0x170
	.uleb128 0x19
	.long	.LASF160
	.byte	0x16
	.value	0x6f6
	.value	0x127
	.long	0xbc4
	.value	0x180
	.uleb128 0x19
	.long	.LASF161
	.byte	0x16
	.value	0x6f6
	.value	0x141
	.long	0xbc4
	.value	0x190
	.uleb128 0x19
	.long	.LASF162
	.byte	0x16
	.value	0x6f6
	.value	0x159
	.long	0xbc4
	.value	0x1a0
	.uleb128 0x19
	.long	.LASF163
	.byte	0x16
	.value	0x6f6
	.value	0x170
	.long	0xbc4
	.value	0x1b0
	.uleb128 0x19
	.long	.LASF164
	.byte	0x16
	.value	0x6f6
	.value	0x189
	.long	0x8f5
	.value	0x1c0
	.uleb128 0x19
	.long	.LASF165
	.byte	0x16
	.value	0x6f6
	.value	0x1a7
	.long	0xc5b
	.value	0x1c8
	.uleb128 0x19
	.long	.LASF166
	.byte	0x16
	.value	0x6f6
	.value	0x1bd
	.long	0x57
	.value	0x200
	.uleb128 0x19
	.long	.LASF167
	.byte	0x16
	.value	0x6f6
	.value	0x1f2
	.long	0x13d4
	.value	0x208
	.uleb128 0x19
	.long	.LASF168
	.byte	0x16
	.value	0x6f6
	.value	0x207
	.long	0x393
	.value	0x218
	.uleb128 0x19
	.long	.LASF169
	.byte	0x16
	.value	0x6f6
	.value	0x21f
	.long	0x393
	.value	0x220
	.uleb128 0x19
	.long	.LASF170
	.byte	0x16
	.value	0x6f6
	.value	0x229
	.long	0xea
	.value	0x228
	.uleb128 0x19
	.long	.LASF171
	.byte	0x16
	.value	0x6f6
	.value	0x244
	.long	0xc5b
	.value	0x230
	.uleb128 0x19
	.long	.LASF172
	.byte	0x16
	.value	0x6f6
	.value	0x263
	.long	0x1136
	.value	0x268
	.uleb128 0x19
	.long	.LASF173
	.byte	0x16
	.value	0x6f6
	.value	0x276
	.long	0x57
	.value	0x300
	.uleb128 0x19
	.long	.LASF174
	.byte	0x16
	.value	0x6f6
	.value	0x28a
	.long	0xc5b
	.value	0x308
	.uleb128 0x19
	.long	.LASF175
	.byte	0x16
	.value	0x6f6
	.value	0x2a6
	.long	0x7f
	.value	0x340
	.uleb128 0x19
	.long	.LASF176
	.byte	0x16
	.value	0x6f6
	.value	0x2bc
	.long	0x57
	.value	0x348
	.byte	0
	.uleb128 0x3
	.byte	0x8
	.long	0x99f
	.uleb128 0xa
	.long	0x7f
	.long	0xbd4
	.uleb128 0xb
	.long	0x71
	.byte	0x1
	.byte	0
	.uleb128 0x7
	.long	.LASF177
	.byte	0x17
	.byte	0x59
	.byte	0x10
	.long	0xbe0
	.uleb128 0x3
	.byte	0x8
	.long	0xbe6
	.uleb128 0x1a
	.long	0xbfb
	.uleb128 0x1b
	.long	0xbbe
	.uleb128 0x1b
	.long	0xbfb
	.uleb128 0x1b
	.long	0x78
	.byte	0
	.uleb128 0x3
	.byte	0x8
	.long	0xc01
	.uleb128 0xc
	.long	.LASF178
	.byte	0x38
	.byte	0x17
	.byte	0x5e
	.byte	0x8
	.long	0xc5b
	.uleb128 0x1c
	.string	"cb"
	.byte	0x17
	.byte	0x5f
	.byte	0xd
	.long	0xbd4
	.byte	0
	.uleb128 0xd
	.long	.LASF150
	.byte	0x17
	.byte	0x60
	.byte	0x9
	.long	0xbc4
	.byte	0x8
	.uleb128 0xd
	.long	.LASF151
	.byte	0x17
	.byte	0x61
	.byte	0x9
	.long	0xbc4
	.byte	0x18
	.uleb128 0xd
	.long	.LASF179
	.byte	0x17
	.byte	0x62
	.byte	0x10
	.long	0x78
	.byte	0x28
	.uleb128 0xd
	.long	.LASF180
	.byte	0x17
	.byte	0x63
	.byte	0x10
	.long	0x78
	.byte	0x2c
	.uleb128 0x1c
	.string	"fd"
	.byte	0x17
	.byte	0x64
	.byte	0x7
	.long	0x57
	.byte	0x30
	.byte	0
	.uleb128 0x7
	.long	.LASF181
	.byte	0x17
	.byte	0x5c
	.byte	0x19
	.long	0xc01
	.uleb128 0x7
	.long	.LASF182
	.byte	0x17
	.byte	0x87
	.byte	0x19
	.long	0x55e
	.uleb128 0x7
	.long	.LASF183
	.byte	0x17
	.byte	0x88
	.byte	0x1a
	.long	0x59f
	.uleb128 0x1d
	.byte	0x5
	.byte	0x4
	.long	0x57
	.byte	0x16
	.byte	0xb6
	.byte	0xe
	.long	0xea2
	.uleb128 0x1e
	.long	.LASF184
	.sleb128 -7
	.uleb128 0x1e
	.long	.LASF185
	.sleb128 -13
	.uleb128 0x1e
	.long	.LASF186
	.sleb128 -98
	.uleb128 0x1e
	.long	.LASF187
	.sleb128 -99
	.uleb128 0x1e
	.long	.LASF188
	.sleb128 -97
	.uleb128 0x1e
	.long	.LASF189
	.sleb128 -11
	.uleb128 0x1e
	.long	.LASF190
	.sleb128 -3000
	.uleb128 0x1e
	.long	.LASF191
	.sleb128 -3001
	.uleb128 0x1e
	.long	.LASF192
	.sleb128 -3002
	.uleb128 0x1e
	.long	.LASF193
	.sleb128 -3013
	.uleb128 0x1e
	.long	.LASF194
	.sleb128 -3003
	.uleb128 0x1e
	.long	.LASF195
	.sleb128 -3004
	.uleb128 0x1e
	.long	.LASF196
	.sleb128 -3005
	.uleb128 0x1e
	.long	.LASF197
	.sleb128 -3006
	.uleb128 0x1e
	.long	.LASF198
	.sleb128 -3007
	.uleb128 0x1e
	.long	.LASF199
	.sleb128 -3008
	.uleb128 0x1e
	.long	.LASF200
	.sleb128 -3009
	.uleb128 0x1e
	.long	.LASF201
	.sleb128 -3014
	.uleb128 0x1e
	.long	.LASF202
	.sleb128 -3010
	.uleb128 0x1e
	.long	.LASF203
	.sleb128 -3011
	.uleb128 0x1e
	.long	.LASF204
	.sleb128 -114
	.uleb128 0x1e
	.long	.LASF205
	.sleb128 -9
	.uleb128 0x1e
	.long	.LASF206
	.sleb128 -16
	.uleb128 0x1e
	.long	.LASF207
	.sleb128 -125
	.uleb128 0x1e
	.long	.LASF208
	.sleb128 -4080
	.uleb128 0x1e
	.long	.LASF209
	.sleb128 -103
	.uleb128 0x1e
	.long	.LASF210
	.sleb128 -111
	.uleb128 0x1e
	.long	.LASF211
	.sleb128 -104
	.uleb128 0x1e
	.long	.LASF212
	.sleb128 -89
	.uleb128 0x1e
	.long	.LASF213
	.sleb128 -17
	.uleb128 0x1e
	.long	.LASF214
	.sleb128 -14
	.uleb128 0x1e
	.long	.LASF215
	.sleb128 -27
	.uleb128 0x1e
	.long	.LASF216
	.sleb128 -113
	.uleb128 0x1e
	.long	.LASF217
	.sleb128 -4
	.uleb128 0x1e
	.long	.LASF218
	.sleb128 -22
	.uleb128 0x1e
	.long	.LASF219
	.sleb128 -5
	.uleb128 0x1e
	.long	.LASF220
	.sleb128 -106
	.uleb128 0x1e
	.long	.LASF221
	.sleb128 -21
	.uleb128 0x1e
	.long	.LASF222
	.sleb128 -40
	.uleb128 0x1e
	.long	.LASF223
	.sleb128 -24
	.uleb128 0x1e
	.long	.LASF224
	.sleb128 -90
	.uleb128 0x1e
	.long	.LASF225
	.sleb128 -36
	.uleb128 0x1e
	.long	.LASF226
	.sleb128 -100
	.uleb128 0x1e
	.long	.LASF227
	.sleb128 -101
	.uleb128 0x1e
	.long	.LASF228
	.sleb128 -23
	.uleb128 0x1e
	.long	.LASF229
	.sleb128 -105
	.uleb128 0x1e
	.long	.LASF230
	.sleb128 -19
	.uleb128 0x1e
	.long	.LASF231
	.sleb128 -2
	.uleb128 0x1e
	.long	.LASF232
	.sleb128 -12
	.uleb128 0x1e
	.long	.LASF233
	.sleb128 -64
	.uleb128 0x1e
	.long	.LASF234
	.sleb128 -92
	.uleb128 0x1e
	.long	.LASF235
	.sleb128 -28
	.uleb128 0x1e
	.long	.LASF236
	.sleb128 -38
	.uleb128 0x1e
	.long	.LASF237
	.sleb128 -107
	.uleb128 0x1e
	.long	.LASF238
	.sleb128 -20
	.uleb128 0x1e
	.long	.LASF239
	.sleb128 -39
	.uleb128 0x1e
	.long	.LASF240
	.sleb128 -88
	.uleb128 0x1e
	.long	.LASF241
	.sleb128 -95
	.uleb128 0x1e
	.long	.LASF242
	.sleb128 -1
	.uleb128 0x1e
	.long	.LASF243
	.sleb128 -32
	.uleb128 0x1e
	.long	.LASF244
	.sleb128 -71
	.uleb128 0x1e
	.long	.LASF245
	.sleb128 -93
	.uleb128 0x1e
	.long	.LASF246
	.sleb128 -91
	.uleb128 0x1e
	.long	.LASF247
	.sleb128 -34
	.uleb128 0x1e
	.long	.LASF248
	.sleb128 -30
	.uleb128 0x1e
	.long	.LASF249
	.sleb128 -108
	.uleb128 0x1e
	.long	.LASF250
	.sleb128 -29
	.uleb128 0x1e
	.long	.LASF251
	.sleb128 -3
	.uleb128 0x1e
	.long	.LASF252
	.sleb128 -110
	.uleb128 0x1e
	.long	.LASF253
	.sleb128 -26
	.uleb128 0x1e
	.long	.LASF254
	.sleb128 -18
	.uleb128 0x1e
	.long	.LASF255
	.sleb128 -4094
	.uleb128 0x1e
	.long	.LASF256
	.sleb128 -4095
	.uleb128 0x1e
	.long	.LASF257
	.sleb128 -6
	.uleb128 0x1e
	.long	.LASF258
	.sleb128 -31
	.uleb128 0x1e
	.long	.LASF259
	.sleb128 -112
	.uleb128 0x1e
	.long	.LASF260
	.sleb128 -121
	.uleb128 0x1e
	.long	.LASF261
	.sleb128 -25
	.uleb128 0x1e
	.long	.LASF262
	.sleb128 -4028
	.uleb128 0x1e
	.long	.LASF263
	.sleb128 -84
	.uleb128 0x1e
	.long	.LASF264
	.sleb128 -4096
	.byte	0
	.uleb128 0x1d
	.byte	0x7
	.byte	0x4
	.long	0x78
	.byte	0x16
	.byte	0xbd
	.byte	0xe
	.long	0xf23
	.uleb128 0x1f
	.long	.LASF265
	.byte	0
	.uleb128 0x1f
	.long	.LASF266
	.byte	0x1
	.uleb128 0x1f
	.long	.LASF267
	.byte	0x2
	.uleb128 0x1f
	.long	.LASF268
	.byte	0x3
	.uleb128 0x1f
	.long	.LASF269
	.byte	0x4
	.uleb128 0x1f
	.long	.LASF270
	.byte	0x5
	.uleb128 0x1f
	.long	.LASF271
	.byte	0x6
	.uleb128 0x1f
	.long	.LASF272
	.byte	0x7
	.uleb128 0x1f
	.long	.LASF273
	.byte	0x8
	.uleb128 0x1f
	.long	.LASF274
	.byte	0x9
	.uleb128 0x1f
	.long	.LASF275
	.byte	0xa
	.uleb128 0x1f
	.long	.LASF276
	.byte	0xb
	.uleb128 0x1f
	.long	.LASF277
	.byte	0xc
	.uleb128 0x1f
	.long	.LASF278
	.byte	0xd
	.uleb128 0x1f
	.long	.LASF279
	.byte	0xe
	.uleb128 0x1f
	.long	.LASF280
	.byte	0xf
	.uleb128 0x1f
	.long	.LASF281
	.byte	0x10
	.uleb128 0x1f
	.long	.LASF282
	.byte	0x11
	.uleb128 0x1f
	.long	.LASF283
	.byte	0x12
	.byte	0
	.uleb128 0x7
	.long	.LASF284
	.byte	0x16
	.byte	0xc4
	.byte	0x3
	.long	0xea2
	.uleb128 0x7
	.long	.LASF285
	.byte	0x16
	.byte	0xd1
	.byte	0x1a
	.long	0x99f
	.uleb128 0x7
	.long	.LASF286
	.byte	0x16
	.byte	0xd2
	.byte	0x1c
	.long	0xf47
	.uleb128 0x20
	.long	.LASF287
	.byte	0x60
	.byte	0x16
	.value	0x1bb
	.byte	0x8
	.long	0xfc4
	.uleb128 0x13
	.long	.LASF143
	.byte	0x16
	.value	0x1bc
	.byte	0x9
	.long	0x7f
	.byte	0
	.uleb128 0x13
	.long	.LASF288
	.byte	0x16
	.value	0x1bc
	.byte	0x1a
	.long	0x12cd
	.byte	0x8
	.uleb128 0x13
	.long	.LASF289
	.byte	0x16
	.value	0x1bc
	.byte	0x2f
	.long	0xf23
	.byte	0x10
	.uleb128 0x13
	.long	.LASF290
	.byte	0x16
	.value	0x1bc
	.byte	0x41
	.long	0x120b
	.byte	0x18
	.uleb128 0x13
	.long	.LASF145
	.byte	0x16
	.value	0x1bc
	.byte	0x51
	.long	0xbc4
	.byte	0x20
	.uleb128 0x17
	.string	"u"
	.byte	0x16
	.value	0x1bc
	.byte	0x87
	.long	0x12a9
	.byte	0x30
	.uleb128 0x13
	.long	.LASF291
	.byte	0x16
	.value	0x1bc
	.byte	0x97
	.long	0x1205
	.byte	0x50
	.uleb128 0x13
	.long	.LASF148
	.byte	0x16
	.value	0x1bc
	.byte	0xb2
	.long	0x78
	.byte	0x58
	.byte	0
	.uleb128 0x7
	.long	.LASF292
	.byte	0x16
	.byte	0xde
	.byte	0x1b
	.long	0xfd0
	.uleb128 0x20
	.long	.LASF293
	.byte	0x80
	.byte	0x16
	.value	0x344
	.byte	0x8
	.long	0x1077
	.uleb128 0x13
	.long	.LASF143
	.byte	0x16
	.value	0x345
	.byte	0x9
	.long	0x7f
	.byte	0
	.uleb128 0x13
	.long	.LASF288
	.byte	0x16
	.value	0x345
	.byte	0x1a
	.long	0x12cd
	.byte	0x8
	.uleb128 0x13
	.long	.LASF289
	.byte	0x16
	.value	0x345
	.byte	0x2f
	.long	0xf23
	.byte	0x10
	.uleb128 0x13
	.long	.LASF290
	.byte	0x16
	.value	0x345
	.byte	0x41
	.long	0x120b
	.byte	0x18
	.uleb128 0x13
	.long	.LASF145
	.byte	0x16
	.value	0x345
	.byte	0x51
	.long	0xbc4
	.byte	0x20
	.uleb128 0x17
	.string	"u"
	.byte	0x16
	.value	0x345
	.byte	0x87
	.long	0x12d3
	.byte	0x30
	.uleb128 0x13
	.long	.LASF291
	.byte	0x16
	.value	0x345
	.byte	0x97
	.long	0x1205
	.byte	0x50
	.uleb128 0x13
	.long	.LASF148
	.byte	0x16
	.value	0x345
	.byte	0xb2
	.long	0x78
	.byte	0x58
	.uleb128 0x13
	.long	.LASF294
	.byte	0x16
	.value	0x346
	.byte	0xf
	.long	0x1229
	.byte	0x60
	.uleb128 0x13
	.long	.LASF295
	.byte	0x16
	.value	0x346
	.byte	0x1f
	.long	0xbc4
	.byte	0x68
	.uleb128 0x13
	.long	.LASF296
	.byte	0x16
	.value	0x346
	.byte	0x2d
	.long	0x57
	.byte	0x78
	.byte	0
	.uleb128 0x7
	.long	.LASF297
	.byte	0x16
	.byte	0xe0
	.byte	0x1e
	.long	0x1083
	.uleb128 0x20
	.long	.LASF298
	.byte	0x88
	.byte	0x16
	.value	0x600
	.byte	0x8
	.long	0x1136
	.uleb128 0x13
	.long	.LASF143
	.byte	0x16
	.value	0x601
	.byte	0x9
	.long	0x7f
	.byte	0
	.uleb128 0x13
	.long	.LASF288
	.byte	0x16
	.value	0x601
	.byte	0x1a
	.long	0x12cd
	.byte	0x8
	.uleb128 0x13
	.long	.LASF289
	.byte	0x16
	.value	0x601
	.byte	0x2f
	.long	0xf23
	.byte	0x10
	.uleb128 0x13
	.long	.LASF290
	.byte	0x16
	.value	0x601
	.byte	0x41
	.long	0x120b
	.byte	0x18
	.uleb128 0x13
	.long	.LASF145
	.byte	0x16
	.value	0x601
	.byte	0x51
	.long	0xbc4
	.byte	0x20
	.uleb128 0x17
	.string	"u"
	.byte	0x16
	.value	0x601
	.byte	0x87
	.long	0x131e
	.byte	0x30
	.uleb128 0x13
	.long	.LASF291
	.byte	0x16
	.value	0x601
	.byte	0x97
	.long	0x1205
	.byte	0x50
	.uleb128 0x13
	.long	.LASF148
	.byte	0x16
	.value	0x601
	.byte	0xb2
	.long	0x78
	.byte	0x58
	.uleb128 0x13
	.long	.LASF299
	.byte	0x16
	.value	0x603
	.byte	0x9
	.long	0x39
	.byte	0x60
	.uleb128 0x17
	.string	"cb"
	.byte	0x16
	.value	0x604
	.byte	0x12
	.long	0x124d
	.byte	0x68
	.uleb128 0x13
	.long	.LASF152
	.byte	0x16
	.value	0x604
	.byte	0x1c
	.long	0xbc4
	.byte	0x70
	.uleb128 0x17
	.string	"wd"
	.byte	0x16
	.value	0x604
	.byte	0x2d
	.long	0x57
	.byte	0x80
	.byte	0
	.uleb128 0x7
	.long	.LASF300
	.byte	0x16
	.byte	0xe2
	.byte	0x1c
	.long	0x1142
	.uleb128 0x20
	.long	.LASF301
	.byte	0x98
	.byte	0x16
	.value	0x61c
	.byte	0x8
	.long	0x1205
	.uleb128 0x13
	.long	.LASF143
	.byte	0x16
	.value	0x61d
	.byte	0x9
	.long	0x7f
	.byte	0
	.uleb128 0x13
	.long	.LASF288
	.byte	0x16
	.value	0x61d
	.byte	0x1a
	.long	0x12cd
	.byte	0x8
	.uleb128 0x13
	.long	.LASF289
	.byte	0x16
	.value	0x61d
	.byte	0x2f
	.long	0xf23
	.byte	0x10
	.uleb128 0x13
	.long	.LASF290
	.byte	0x16
	.value	0x61d
	.byte	0x41
	.long	0x120b
	.byte	0x18
	.uleb128 0x13
	.long	.LASF145
	.byte	0x16
	.value	0x61d
	.byte	0x51
	.long	0xbc4
	.byte	0x20
	.uleb128 0x17
	.string	"u"
	.byte	0x16
	.value	0x61d
	.byte	0x87
	.long	0x1342
	.byte	0x30
	.uleb128 0x13
	.long	.LASF291
	.byte	0x16
	.value	0x61d
	.byte	0x97
	.long	0x1205
	.byte	0x50
	.uleb128 0x13
	.long	.LASF148
	.byte	0x16
	.value	0x61d
	.byte	0xb2
	.long	0x78
	.byte	0x58
	.uleb128 0x13
	.long	.LASF302
	.byte	0x16
	.value	0x61e
	.byte	0x10
	.long	0x1280
	.byte	0x60
	.uleb128 0x13
	.long	.LASF303
	.byte	0x16
	.value	0x61f
	.byte	0x7
	.long	0x57
	.byte	0x68
	.uleb128 0x13
	.long	.LASF304
	.byte	0x16
	.value	0x620
	.byte	0x7a
	.long	0x1366
	.byte	0x70
	.uleb128 0x13
	.long	.LASF305
	.byte	0x16
	.value	0x620
	.byte	0x93
	.long	0x78
	.byte	0x90
	.uleb128 0x13
	.long	.LASF306
	.byte	0x16
	.value	0x620
	.byte	0xb0
	.long	0x78
	.byte	0x94
	.byte	0
	.uleb128 0x3
	.byte	0x8
	.long	0xf3b
	.uleb128 0x21
	.long	.LASF307
	.byte	0x16
	.value	0x13e
	.byte	0x10
	.long	0x1218
	.uleb128 0x3
	.byte	0x8
	.long	0x121e
	.uleb128 0x1a
	.long	0x1229
	.uleb128 0x1b
	.long	0x1205
	.byte	0
	.uleb128 0x21
	.long	.LASF308
	.byte	0x16
	.value	0x141
	.byte	0x10
	.long	0x1236
	.uleb128 0x3
	.byte	0x8
	.long	0x123c
	.uleb128 0x1a
	.long	0x1247
	.uleb128 0x1b
	.long	0x1247
	.byte	0
	.uleb128 0x3
	.byte	0x8
	.long	0xfc4
	.uleb128 0x21
	.long	.LASF309
	.byte	0x16
	.value	0x170
	.byte	0x10
	.long	0x125a
	.uleb128 0x3
	.byte	0x8
	.long	0x1260
	.uleb128 0x1a
	.long	0x127a
	.uleb128 0x1b
	.long	0x127a
	.uleb128 0x1b
	.long	0x2ee
	.uleb128 0x1b
	.long	0x57
	.uleb128 0x1b
	.long	0x57
	.byte	0
	.uleb128 0x3
	.byte	0x8
	.long	0x1077
	.uleb128 0x21
	.long	.LASF310
	.byte	0x16
	.value	0x17a
	.byte	0x10
	.long	0x128d
	.uleb128 0x3
	.byte	0x8
	.long	0x1293
	.uleb128 0x1a
	.long	0x12a3
	.uleb128 0x1b
	.long	0x12a3
	.uleb128 0x1b
	.long	0x57
	.byte	0
	.uleb128 0x3
	.byte	0x8
	.long	0x1136
	.uleb128 0x22
	.byte	0x20
	.byte	0x16
	.value	0x1bc
	.byte	0x62
	.long	0x12cd
	.uleb128 0x23
	.string	"fd"
	.byte	0x16
	.value	0x1bc
	.byte	0x6e
	.long	0x57
	.uleb128 0x24
	.long	.LASF311
	.byte	0x16
	.value	0x1bc
	.byte	0x78
	.long	0x98f
	.byte	0
	.uleb128 0x3
	.byte	0x8
	.long	0xf2f
	.uleb128 0x22
	.byte	0x20
	.byte	0x16
	.value	0x345
	.byte	0x62
	.long	0x12f7
	.uleb128 0x23
	.string	"fd"
	.byte	0x16
	.value	0x345
	.byte	0x6e
	.long	0x57
	.uleb128 0x24
	.long	.LASF311
	.byte	0x16
	.value	0x345
	.byte	0x78
	.long	0x98f
	.byte	0
	.uleb128 0x4
	.byte	0x8
	.byte	0x4
	.long	.LASF312
	.uleb128 0x25
	.long	.LASF440
	.byte	0x7
	.byte	0x4
	.long	0x78
	.byte	0x16
	.value	0x5fa
	.byte	0x6
	.long	0x131e
	.uleb128 0x1f
	.long	.LASF313
	.byte	0x1
	.uleb128 0x1f
	.long	.LASF314
	.byte	0x2
	.byte	0
	.uleb128 0x22
	.byte	0x20
	.byte	0x16
	.value	0x601
	.byte	0x62
	.long	0x1342
	.uleb128 0x23
	.string	"fd"
	.byte	0x16
	.value	0x601
	.byte	0x6e
	.long	0x57
	.uleb128 0x24
	.long	.LASF311
	.byte	0x16
	.value	0x601
	.byte	0x78
	.long	0x98f
	.byte	0
	.uleb128 0x22
	.byte	0x20
	.byte	0x16
	.value	0x61d
	.byte	0x62
	.long	0x1366
	.uleb128 0x23
	.string	"fd"
	.byte	0x16
	.value	0x61d
	.byte	0x6e
	.long	0x57
	.uleb128 0x24
	.long	.LASF311
	.byte	0x16
	.value	0x61d
	.byte	0x78
	.long	0x98f
	.byte	0
	.uleb128 0x26
	.byte	0x20
	.byte	0x16
	.value	0x620
	.byte	0x3
	.long	0x13a9
	.uleb128 0x13
	.long	.LASF315
	.byte	0x16
	.value	0x620
	.byte	0x20
	.long	0x13a9
	.byte	0
	.uleb128 0x13
	.long	.LASF316
	.byte	0x16
	.value	0x620
	.byte	0x3e
	.long	0x13a9
	.byte	0x8
	.uleb128 0x13
	.long	.LASF317
	.byte	0x16
	.value	0x620
	.byte	0x5d
	.long	0x13a9
	.byte	0x10
	.uleb128 0x13
	.long	.LASF318
	.byte	0x16
	.value	0x620
	.byte	0x6d
	.long	0x57
	.byte	0x18
	.byte	0
	.uleb128 0x3
	.byte	0x8
	.long	0x1142
	.uleb128 0x22
	.byte	0x10
	.byte	0x16
	.value	0x6f0
	.byte	0x3
	.long	0x13d4
	.uleb128 0x24
	.long	.LASF319
	.byte	0x16
	.value	0x6f1
	.byte	0xb
	.long	0xbc4
	.uleb128 0x24
	.long	.LASF320
	.byte	0x16
	.value	0x6f2
	.byte	0x12
	.long	0x78
	.byte	0
	.uleb128 0x27
	.byte	0x10
	.byte	0x16
	.value	0x6f6
	.value	0x1c8
	.long	0x13fe
	.uleb128 0x28
	.string	"min"
	.byte	0x16
	.value	0x6f6
	.value	0x1d7
	.long	0x7f
	.byte	0
	.uleb128 0x29
	.long	.LASF321
	.byte	0x16
	.value	0x6f6
	.value	0x1e9
	.long	0x78
	.byte	0x8
	.byte	0
	.uleb128 0x3
	.byte	0x8
	.long	0x1404
	.uleb128 0x3
	.byte	0x8
	.long	0xc5b
	.uleb128 0x7
	.long	.LASF322
	.byte	0x18
	.byte	0x15
	.byte	0xf
	.long	0xbc4
	.uleb128 0x1d
	.byte	0x7
	.byte	0x4
	.long	0x78
	.byte	0x19
	.byte	0x40
	.byte	0x6
	.long	0x156e
	.uleb128 0x1f
	.long	.LASF323
	.byte	0x1
	.uleb128 0x1f
	.long	.LASF324
	.byte	0x2
	.uleb128 0x1f
	.long	.LASF325
	.byte	0x4
	.uleb128 0x1f
	.long	.LASF326
	.byte	0x8
	.uleb128 0x1f
	.long	.LASF327
	.byte	0x10
	.uleb128 0x1f
	.long	.LASF328
	.byte	0x20
	.uleb128 0x1f
	.long	.LASF329
	.byte	0x40
	.uleb128 0x1f
	.long	.LASF330
	.byte	0x80
	.uleb128 0x2a
	.long	.LASF331
	.value	0x100
	.uleb128 0x2a
	.long	.LASF332
	.value	0x200
	.uleb128 0x2a
	.long	.LASF333
	.value	0x400
	.uleb128 0x2a
	.long	.LASF334
	.value	0x800
	.uleb128 0x2a
	.long	.LASF335
	.value	0x1000
	.uleb128 0x2a
	.long	.LASF336
	.value	0x2000
	.uleb128 0x2a
	.long	.LASF337
	.value	0x4000
	.uleb128 0x2a
	.long	.LASF338
	.value	0x8000
	.uleb128 0x2b
	.long	.LASF339
	.long	0x10000
	.uleb128 0x2b
	.long	.LASF340
	.long	0x20000
	.uleb128 0x2b
	.long	.LASF341
	.long	0x40000
	.uleb128 0x2b
	.long	.LASF342
	.long	0x80000
	.uleb128 0x2b
	.long	.LASF343
	.long	0x100000
	.uleb128 0x2b
	.long	.LASF344
	.long	0x200000
	.uleb128 0x2b
	.long	.LASF345
	.long	0x400000
	.uleb128 0x2b
	.long	.LASF346
	.long	0x1000000
	.uleb128 0x2b
	.long	.LASF347
	.long	0x2000000
	.uleb128 0x2b
	.long	.LASF348
	.long	0x4000000
	.uleb128 0x2b
	.long	.LASF349
	.long	0x8000000
	.uleb128 0x2b
	.long	.LASF350
	.long	0x10000000
	.uleb128 0x2b
	.long	.LASF351
	.long	0x20000000
	.uleb128 0x2b
	.long	.LASF352
	.long	0x1000000
	.uleb128 0x2b
	.long	.LASF353
	.long	0x2000000
	.uleb128 0x2b
	.long	.LASF354
	.long	0x4000000
	.uleb128 0x2b
	.long	.LASF355
	.long	0x1000000
	.uleb128 0x2b
	.long	.LASF356
	.long	0x2000000
	.uleb128 0x2b
	.long	.LASF357
	.long	0x1000000
	.uleb128 0x2b
	.long	.LASF358
	.long	0x2000000
	.uleb128 0x2b
	.long	.LASF359
	.long	0x4000000
	.uleb128 0x2b
	.long	.LASF360
	.long	0x8000000
	.uleb128 0x2b
	.long	.LASF361
	.long	0x1000000
	.uleb128 0x2b
	.long	.LASF362
	.long	0x2000000
	.uleb128 0x2b
	.long	.LASF363
	.long	0x1000000
	.byte	0
	.uleb128 0x3
	.byte	0x8
	.long	0x1579
	.uleb128 0x9
	.long	0x156e
	.uleb128 0x2c
	.uleb128 0x1d
	.byte	0x7
	.byte	0x4
	.long	0x78
	.byte	0x1a
	.byte	0x18
	.byte	0x3
	.long	0x1599
	.uleb128 0x2b
	.long	.LASF364
	.long	0x80000
	.uleb128 0x2a
	.long	.LASF365
	.value	0x800
	.byte	0
	.uleb128 0xc
	.long	.LASF366
	.byte	0x10
	.byte	0x1b
	.byte	0x1c
	.byte	0x8
	.long	0x15e7
	.uleb128 0x1c
	.string	"wd"
	.byte	0x1b
	.byte	0x1e
	.byte	0x7
	.long	0x57
	.byte	0
	.uleb128 0xd
	.long	.LASF367
	.byte	0x1b
	.byte	0x1f
	.byte	0xc
	.long	0x387
	.byte	0x4
	.uleb128 0xd
	.long	.LASF368
	.byte	0x1b
	.byte	0x20
	.byte	0xc
	.long	0x387
	.byte	0x8
	.uleb128 0x1c
	.string	"len"
	.byte	0x1b
	.byte	0x21
	.byte	0xc
	.long	0x387
	.byte	0xc
	.uleb128 0xd
	.long	.LASF369
	.byte	0x1b
	.byte	0x22
	.byte	0x8
	.long	0x15ec
	.byte	0x10
	.byte	0
	.uleb128 0x5
	.long	0x1599
	.uleb128 0xa
	.long	0x3f
	.long	0x15fb
	.uleb128 0x2d
	.long	0x71
	.byte	0
	.uleb128 0x15
	.long	.LASF370
	.byte	0x1c
	.value	0x21f
	.byte	0xf
	.long	0x8ee
	.uleb128 0x15
	.long	.LASF371
	.byte	0x1c
	.value	0x221
	.byte	0xf
	.long	0x8ee
	.uleb128 0x2
	.long	.LASF372
	.byte	0x1d
	.byte	0x24
	.byte	0xe
	.long	0x39
	.uleb128 0x2
	.long	.LASF373
	.byte	0x1d
	.byte	0x32
	.byte	0xc
	.long	0x57
	.uleb128 0x2
	.long	.LASF374
	.byte	0x1d
	.byte	0x37
	.byte	0xc
	.long	0x57
	.uleb128 0x2
	.long	.LASF375
	.byte	0x1d
	.byte	0x3b
	.byte	0xc
	.long	0x57
	.uleb128 0x2e
	.byte	0x20
	.byte	0x1
	.byte	0x25
	.byte	0x3
	.long	0x1683
	.uleb128 0xd
	.long	.LASF315
	.byte	0x1
	.byte	0x25
	.byte	0x21
	.long	0x16d6
	.byte	0
	.uleb128 0xd
	.long	.LASF316
	.byte	0x1
	.byte	0x25
	.byte	0x40
	.long	0x16d6
	.byte	0x8
	.uleb128 0xd
	.long	.LASF317
	.byte	0x1
	.byte	0x25
	.byte	0x60
	.long	0x16d6
	.byte	0x10
	.uleb128 0xd
	.long	.LASF318
	.byte	0x1
	.byte	0x25
	.byte	0x70
	.long	0x57
	.byte	0x18
	.byte	0
	.uleb128 0xc
	.long	.LASF376
	.byte	0x48
	.byte	0x1
	.byte	0x24
	.byte	0x8
	.long	0x16d1
	.uleb128 0xd
	.long	.LASF377
	.byte	0x1
	.byte	0x25
	.byte	0x7d
	.long	0x1645
	.byte	0
	.uleb128 0xd
	.long	.LASF152
	.byte	0x1
	.byte	0x26
	.byte	0x9
	.long	0x140a
	.byte	0x20
	.uleb128 0xd
	.long	.LASF378
	.byte	0x1
	.byte	0x27
	.byte	0x7
	.long	0x57
	.byte	0x30
	.uleb128 0xd
	.long	.LASF299
	.byte	0x1
	.byte	0x28
	.byte	0x9
	.long	0x39
	.byte	0x38
	.uleb128 0x1c
	.string	"wd"
	.byte	0x1
	.byte	0x29
	.byte	0x7
	.long	0x57
	.byte	0x40
	.byte	0
	.uleb128 0x5
	.long	0x1683
	.uleb128 0x3
	.byte	0x8
	.long	0x1683
	.uleb128 0xc
	.long	.LASF379
	.byte	0x8
	.byte	0x1
	.byte	0x2c
	.byte	0x8
	.long	0x16f7
	.uleb128 0xd
	.long	.LASF380
	.byte	0x1
	.byte	0x2d
	.byte	0x18
	.long	0x16d6
	.byte	0
	.byte	0
	.uleb128 0x2f
	.long	.LASF441
	.byte	0x1
	.value	0x145
	.byte	0x6
	.quad	.LFB112
	.quad	.LFE112-.LFB112
	.uleb128 0x1
	.byte	0x9c
	.long	0x18b2
	.uleb128 0x30
	.long	.LASF383
	.byte	0x1
	.value	0x145
	.byte	0x28
	.long	0x127a
	.long	.LLST124
	.long	.LVUS124
	.uleb128 0x31
	.long	0x18b2
	.quad	.LBI304
	.value	.LVU1521
	.long	.Ldebug_ranges0+0x780
	.byte	0x1
	.value	0x146
	.byte	0x3
	.uleb128 0x32
	.long	0x18c4
	.long	.LLST125
	.long	.LVUS125
	.uleb128 0x33
	.long	.Ldebug_ranges0+0x780
	.uleb128 0x34
	.long	0x18d1
	.long	.LLST126
	.long	.LVUS126
	.uleb128 0x35
	.long	0x1dce
	.quad	.LBI306
	.value	.LVU1529
	.long	.Ldebug_ranges0+0x7c0
	.byte	0x1
	.value	0x137
	.byte	0x7
	.long	0x1830
	.uleb128 0x32
	.long	0x1deb
	.long	.LLST127
	.long	.LVUS127
	.uleb128 0x32
	.long	0x1ddf
	.long	.LLST128
	.long	.LVUS128
	.uleb128 0x33
	.long	.Ldebug_ranges0+0x7c0
	.uleb128 0x36
	.long	0x1df6
	.uleb128 0x37
	.long	0x2407
	.quad	.LBI308
	.value	.LVU1534
	.long	.Ldebug_ranges0+0x7f0
	.byte	0x1
	.byte	0x9a
	.byte	0xa
	.uleb128 0x32
	.long	0x2418
	.long	.LLST129
	.long	.LVUS129
	.uleb128 0x32
	.long	0x2424
	.long	.LLST130
	.long	.LVUS130
	.uleb128 0x33
	.long	.Ldebug_ranges0+0x7f0
	.uleb128 0x34
	.long	0x2430
	.long	.LLST131
	.long	.LVUS131
	.uleb128 0x34
	.long	0x243c
	.long	.LLST132
	.long	.LVUS132
	.uleb128 0x38
	.long	0x25c1
	.quad	.LBI310
	.value	.LVU1540
	.quad	.LBB310
	.quad	.LBE310-.LBB310
	.byte	0x1
	.byte	0x3a
	.byte	0xd3
	.uleb128 0x32
	.long	0x25d2
	.long	.LLST133
	.long	.LVUS133
	.uleb128 0x32
	.long	0x25dc
	.long	.LLST134
	.long	.LVUS134
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x35
	.long	0x1daa
	.quad	.LBI315
	.value	.LVU1574
	.long	.Ldebug_ranges0+0x820
	.byte	0x1
	.value	0x13f
	.byte	0x3
	.long	0x1873
	.uleb128 0x32
	.long	0x1dc1
	.long	.LLST135
	.long	.LVUS135
	.uleb128 0x32
	.long	0x1db7
	.long	.LLST136
	.long	.LVUS136
	.uleb128 0x39
	.quad	.LVL312
	.long	0x2684
	.byte	0
	.uleb128 0x3a
	.quad	.LVL315
	.long	0x2db9
	.uleb128 0x3b
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x9
	.byte	0x3
	.quad	.LC4
	.uleb128 0x3b
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x9
	.byte	0x3
	.quad	.LC0
	.uleb128 0x3b
	.uleb128 0x1
	.byte	0x51
	.uleb128 0x3
	.byte	0xa
	.value	0x138
	.uleb128 0x3b
	.uleb128 0x1
	.byte	0x52
	.uleb128 0x9
	.byte	0x3
	.quad	__PRETTY_FUNCTION__.10164
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x3c
	.long	.LASF381
	.byte	0x1
	.value	0x131
	.byte	0x5
	.long	0x57
	.byte	0x1
	.long	0x18f0
	.uleb128 0x3d
	.long	.LASF383
	.byte	0x1
	.value	0x131
	.byte	0x25
	.long	0x127a
	.uleb128 0x3e
	.string	"w"
	.byte	0x1
	.value	0x132
	.byte	0x18
	.long	0x16d6
	.uleb128 0x3f
	.long	.LASF386
	.long	0x1900
	.uleb128 0x9
	.byte	0x3
	.quad	__PRETTY_FUNCTION__.10164
	.byte	0
	.uleb128 0xa
	.long	0x46
	.long	0x1900
	.uleb128 0xb
	.long	0x71
	.byte	0x10
	.byte	0
	.uleb128 0x5
	.long	0x18f0
	.uleb128 0x40
	.long	.LASF382
	.byte	0x1
	.byte	0xf9
	.byte	0x5
	.long	0x57
	.byte	0x1
	.long	0x198a
	.uleb128 0x41
	.long	.LASF383
	.byte	0x1
	.byte	0xf9
	.byte	0x26
	.long	0x127a
	.uleb128 0x42
	.string	"cb"
	.byte	0x1
	.byte	0xfa
	.byte	0x26
	.long	0x124d
	.uleb128 0x41
	.long	.LASF299
	.byte	0x1
	.byte	0xfb
	.byte	0x23
	.long	0x2ee
	.uleb128 0x41
	.long	.LASF148
	.byte	0x1
	.byte	0xfc
	.byte	0x24
	.long	0x78
	.uleb128 0x43
	.string	"w"
	.byte	0x1
	.byte	0xfd
	.byte	0x18
	.long	0x16d6
	.uleb128 0x43
	.string	"len"
	.byte	0x1
	.byte	0xfe
	.byte	0xa
	.long	0x65
	.uleb128 0x44
	.long	.LASF180
	.byte	0x1
	.byte	0xff
	.byte	0x7
	.long	0x57
	.uleb128 0x3e
	.string	"err"
	.byte	0x1
	.value	0x100
	.byte	0x7
	.long	0x57
	.uleb128 0x3e
	.string	"wd"
	.byte	0x1
	.value	0x101
	.byte	0x7
	.long	0x57
	.uleb128 0x45
	.long	.LASF405
	.byte	0x1
	.value	0x126
	.byte	0x1
	.byte	0
	.uleb128 0x46
	.long	.LASF387
	.byte	0x1
	.byte	0xf3
	.byte	0x5
	.long	0x57
	.quad	.LFB109
	.quad	.LFE109-.LFB109
	.uleb128 0x1
	.byte	0x9c
	.long	0x19c9
	.uleb128 0x47
	.long	.LASF288
	.byte	0x1
	.byte	0xf3
	.byte	0x21
	.long	0x12cd
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x47
	.long	.LASF383
	.byte	0x1
	.byte	0xf3
	.byte	0x36
	.long	0x127a
	.uleb128 0x1
	.byte	0x54
	.byte	0
	.uleb128 0x48
	.long	.LASF442
	.byte	0x1
	.byte	0xa7
	.byte	0xd
	.quad	.LFB108
	.quad	.LFE108-.LFB108
	.uleb128 0x1
	.byte	0x9c
	.long	0x1d8d
	.uleb128 0x49
	.long	.LASF288
	.byte	0x1
	.byte	0xa7
	.byte	0x29
	.long	0x12cd
	.long	.LLST14
	.long	.LVUS14
	.uleb128 0x49
	.long	.LASF384
	.byte	0x1
	.byte	0xa8
	.byte	0x28
	.long	0x1404
	.long	.LLST15
	.long	.LVUS15
	.uleb128 0x49
	.long	.LASF180
	.byte	0x1
	.byte	0xa9
	.byte	0x2b
	.long	0x78
	.long	.LLST16
	.long	.LVUS16
	.uleb128 0x4a
	.string	"e"
	.byte	0x1
	.byte	0xaa
	.byte	0x1f
	.long	0x1d8d
	.long	.LLST17
	.long	.LVUS17
	.uleb128 0x4a
	.string	"w"
	.byte	0x1
	.byte	0xab
	.byte	0x18
	.long	0x16d6
	.long	.LLST18
	.long	.LVUS18
	.uleb128 0x4a
	.string	"h"
	.byte	0x1
	.byte	0xac
	.byte	0x12
	.long	0x127a
	.long	.LLST19
	.long	.LVUS19
	.uleb128 0x4b
	.long	.LASF295
	.byte	0x1
	.byte	0xad
	.byte	0x9
	.long	0x140a
	.uleb128 0x3
	.byte	0x91
	.sleb128 -4192
	.uleb128 0x4a
	.string	"q"
	.byte	0x1
	.byte	0xae
	.byte	0xa
	.long	0x1d93
	.long	.LLST20
	.long	.LVUS20
	.uleb128 0x4c
	.long	.LASF299
	.byte	0x1
	.byte	0xaf
	.byte	0xf
	.long	0x2ee
	.long	.LLST21
	.long	.LVUS21
	.uleb128 0x4c
	.long	.LASF385
	.byte	0x1
	.byte	0xb0
	.byte	0xb
	.long	0x2f9
	.long	.LLST22
	.long	.LVUS22
	.uleb128 0x4a
	.string	"p"
	.byte	0x1
	.byte	0xb1
	.byte	0xf
	.long	0x2ee
	.long	.LLST23
	.long	.LVUS23
	.uleb128 0x4d
	.string	"buf"
	.byte	0x1
	.byte	0xb3
	.byte	0x8
	.long	0x1d99
	.uleb128 0x3
	.byte	0x91
	.sleb128 -4176
	.uleb128 0x3f
	.long	.LASF386
	.long	0x1900
	.uleb128 0x9
	.byte	0x3
	.quad	__PRETTY_FUNCTION__.10132
	.uleb128 0x4e
	.long	.Ldebug_ranges0+0x170
	.long	0x1af4
	.uleb128 0x4a
	.string	"q"
	.byte	0x1
	.byte	0xe1
	.byte	0xe3
	.long	0x1d93
	.long	.LLST24
	.long	.LVUS24
	.byte	0
	.uleb128 0x4f
	.long	0x25ed
	.quad	.LBI100
	.value	.LVU463
	.quad	.LBB100
	.quad	.LBE100-.LBB100
	.byte	0x1
	.byte	0xb7
	.byte	0xe
	.long	0x1b5f
	.uleb128 0x32
	.long	0x2616
	.long	.LLST25
	.long	.LVUS25
	.uleb128 0x32
	.long	0x260a
	.long	.LLST26
	.long	.LVUS26
	.uleb128 0x32
	.long	0x25fe
	.long	.LLST27
	.long	.LVUS27
	.uleb128 0x3a
	.quad	.LVL71
	.long	0x2dc5
	.uleb128 0x3b
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x4
	.byte	0x76
	.sleb128 -4208
	.byte	0x6
	.uleb128 0x3b
	.uleb128 0x1
	.byte	0x51
	.uleb128 0x3
	.byte	0xa
	.value	0x1000
	.byte	0
	.byte	0
	.uleb128 0x50
	.long	0x1dce
	.quad	.LBI102
	.value	.LVU493
	.long	.Ldebug_ranges0+0x1a0
	.byte	0x1
	.byte	0xcb
	.byte	0xb
	.long	0x1c2d
	.uleb128 0x32
	.long	0x1deb
	.long	.LLST28
	.long	.LVUS28
	.uleb128 0x32
	.long	0x1ddf
	.long	.LLST29
	.long	.LVUS29
	.uleb128 0x33
	.long	.Ldebug_ranges0+0x1a0
	.uleb128 0x36
	.long	0x1df6
	.uleb128 0x37
	.long	0x2407
	.quad	.LBI104
	.value	.LVU497
	.long	.Ldebug_ranges0+0x1f0
	.byte	0x1
	.byte	0x9a
	.byte	0xa
	.uleb128 0x32
	.long	0x2418
	.long	.LLST30
	.long	.LVUS30
	.uleb128 0x32
	.long	0x2424
	.long	.LLST31
	.long	.LVUS31
	.uleb128 0x33
	.long	.Ldebug_ranges0+0x1f0
	.uleb128 0x34
	.long	0x2430
	.long	.LLST32
	.long	.LVUS32
	.uleb128 0x34
	.long	0x243c
	.long	.LLST33
	.long	.LVUS33
	.uleb128 0x38
	.long	0x25c1
	.quad	.LBI106
	.value	.LVU503
	.quad	.LBB106
	.quad	.LBE106-.LBB106
	.byte	0x1
	.byte	0x3a
	.byte	0xd3
	.uleb128 0x32
	.long	0x25d2
	.long	.LLST34
	.long	.LVUS34
	.uleb128 0x32
	.long	0x25dc
	.long	.LLST35
	.long	.LVUS35
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x4f
	.long	0x2623
	.quad	.LBI115
	.value	.LVU513
	.quad	.LBB115
	.quad	.LBE115-.LBB115
	.byte	0x1
	.byte	0xd3
	.byte	0x2f
	.long	0x1c88
	.uleb128 0x32
	.long	0x2635
	.long	.LLST36
	.long	.LVUS36
	.uleb128 0x34
	.long	0x2642
	.long	.LLST37
	.long	.LVUS37
	.uleb128 0x3a
	.quad	.LVL80
	.long	0x2dd1
	.uleb128 0x3b
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x3b
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x2
	.byte	0x8
	.byte	0x2f
	.byte	0
	.byte	0
	.uleb128 0x50
	.long	0x1daa
	.quad	.LBI118
	.value	.LVU581
	.long	.Ldebug_ranges0+0x220
	.byte	0x1
	.byte	0xed
	.byte	0x7
	.long	0x1cd9
	.uleb128 0x32
	.long	0x1dc1
	.long	.LLST38
	.long	.LVUS38
	.uleb128 0x32
	.long	0x1db7
	.long	.LLST39
	.long	.LVUS39
	.uleb128 0x3a
	.quad	.LVL97
	.long	0x2684
	.uleb128 0x3b
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x2
	.byte	0x7f
	.sleb128 0
	.uleb128 0x3b
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x4
	.byte	0x76
	.sleb128 -4192
	.byte	0x6
	.byte	0
	.byte	0
	.uleb128 0x51
	.quad	.LVL87
	.long	0x1cf8
	.uleb128 0x3b
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x3b
	.uleb128 0x1
	.byte	0x51
	.uleb128 0x2
	.byte	0x7e
	.sleb128 0
	.uleb128 0x3b
	.uleb128 0x1
	.byte	0x52
	.uleb128 0x1
	.byte	0x30
	.byte	0
	.uleb128 0x52
	.quad	.LVL99
	.long	0x2ddd
	.uleb128 0x53
	.quad	.LVL102
	.long	0x2db9
	.long	0x1d44
	.uleb128 0x3b
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x9
	.byte	0x3
	.quad	.LC2
	.uleb128 0x3b
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x9
	.byte	0x3
	.quad	.LC0
	.uleb128 0x3b
	.uleb128 0x1
	.byte	0x51
	.uleb128 0x2
	.byte	0x8
	.byte	0xbf
	.uleb128 0x3b
	.uleb128 0x1
	.byte	0x52
	.uleb128 0x9
	.byte	0x3
	.quad	__PRETTY_FUNCTION__.10132
	.byte	0
	.uleb128 0x52
	.quad	.LVL103
	.long	0x2de9
	.uleb128 0x3a
	.quad	.LVL104
	.long	0x2db9
	.uleb128 0x3b
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x9
	.byte	0x3
	.quad	.LC1
	.uleb128 0x3b
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x9
	.byte	0x3
	.quad	.LC0
	.uleb128 0x3b
	.uleb128 0x1
	.byte	0x51
	.uleb128 0x2
	.byte	0x8
	.byte	0xbb
	.uleb128 0x3b
	.uleb128 0x1
	.byte	0x52
	.uleb128 0x9
	.byte	0x3
	.quad	__PRETTY_FUNCTION__.10132
	.byte	0
	.byte	0
	.uleb128 0x3
	.byte	0x8
	.long	0x15e7
	.uleb128 0x3
	.byte	0x8
	.long	0x140a
	.uleb128 0xa
	.long	0x3f
	.long	0x1daa
	.uleb128 0x54
	.long	0x71
	.value	0xfff
	.byte	0
	.uleb128 0x55
	.long	.LASF407
	.byte	0x1
	.byte	0x9d
	.byte	0xd
	.byte	0x1
	.long	0x1dce
	.uleb128 0x42
	.string	"w"
	.byte	0x1
	.byte	0x9d
	.byte	0x3a
	.long	0x16d6
	.uleb128 0x41
	.long	.LASF288
	.byte	0x1
	.byte	0x9d
	.byte	0x48
	.long	0x12cd
	.byte	0
	.uleb128 0x56
	.long	.LASF393
	.byte	0x1
	.byte	0x97
	.byte	0x1d
	.long	0x16d6
	.byte	0x1
	.long	0x1e01
	.uleb128 0x41
	.long	.LASF288
	.byte	0x1
	.byte	0x97
	.byte	0x35
	.long	0x12cd
	.uleb128 0x42
	.string	"wd"
	.byte	0x1
	.byte	0x97
	.byte	0x3f
	.long	0x57
	.uleb128 0x43
	.string	"w"
	.byte	0x1
	.byte	0x98
	.byte	0x17
	.long	0x1683
	.byte	0
	.uleb128 0x46
	.long	.LASF388
	.byte	0x1
	.byte	0x56
	.byte	0x5
	.long	0x57
	.quad	.LFB105
	.quad	.LFE105-.LFB105
	.uleb128 0x1
	.byte	0x9c
	.long	0x2371
	.uleb128 0x49
	.long	.LASF288
	.byte	0x1
	.byte	0x56
	.byte	0x21
	.long	0x12cd
	.long	.LLST69
	.long	.LVUS69
	.uleb128 0x49
	.long	.LASF389
	.byte	0x1
	.byte	0x56
	.byte	0x2d
	.long	0x7f
	.long	.LLST70
	.long	.LVUS70
	.uleb128 0x4a
	.string	"err"
	.byte	0x1
	.byte	0x58
	.byte	0x7
	.long	0x57
	.long	.LLST71
	.long	.LVUS71
	.uleb128 0x4c
	.long	.LASF390
	.byte	0x1
	.byte	0x59
	.byte	0x18
	.long	0x16d6
	.long	.LLST72
	.long	.LVUS72
	.uleb128 0x4c
	.long	.LASF376
	.byte	0x1
	.byte	0x5a
	.byte	0x18
	.long	0x16d6
	.long	.LLST73
	.long	.LVUS73
	.uleb128 0x4b
	.long	.LASF391
	.byte	0x1
	.byte	0x5b
	.byte	0x17
	.long	0x1683
	.uleb128 0x3
	.byte	0x91
	.sleb128 -144
	.uleb128 0x4b
	.long	.LASF295
	.byte	0x1
	.byte	0x5c
	.byte	0x9
	.long	0x140a
	.uleb128 0x3
	.byte	0x91
	.sleb128 -160
	.uleb128 0x4a
	.string	"q"
	.byte	0x1
	.byte	0x5d
	.byte	0xa
	.long	0x1d93
	.long	.LLST74
	.long	.LVUS74
	.uleb128 0x4c
	.long	.LASF383
	.byte	0x1
	.byte	0x5e
	.byte	0x12
	.long	0x127a
	.long	.LLST75
	.long	.LVUS75
	.uleb128 0x4c
	.long	.LASF392
	.byte	0x1
	.byte	0x5f
	.byte	0x9
	.long	0x39
	.long	.LLST76
	.long	.LVUS76
	.uleb128 0x3f
	.long	.LASF386
	.long	0x1900
	.uleb128 0x9
	.byte	0x3
	.quad	__PRETTY_FUNCTION__.10096
	.uleb128 0x4e
	.long	.Ldebug_ranges0+0x400
	.long	0x1f10
	.uleb128 0x4a
	.string	"q"
	.byte	0x1
	.byte	0x70
	.byte	0xf9
	.long	0x1d93
	.long	.LLST82
	.long	.LVUS82
	.byte	0
	.uleb128 0x4e
	.long	.Ldebug_ranges0+0x5e0
	.long	0x1f2c
	.uleb128 0x4a
	.string	"q"
	.byte	0x1
	.byte	0x85
	.byte	0xfd
	.long	0x1d93
	.long	.LLST97
	.long	.LVUS97
	.byte	0
	.uleb128 0x4f
	.long	0x239a
	.quad	.LBI195
	.value	.LVU1057
	.quad	.LBB195
	.quad	.LBE195-.LBB195
	.byte	0x1
	.byte	0x6d
	.byte	0x1b
	.long	0x1f87
	.uleb128 0x32
	.long	0x23ac
	.long	.LLST77
	.long	.LVUS77
	.uleb128 0x32
	.long	0x23b9
	.long	.LLST78
	.long	.LVUS78
	.uleb128 0x34
	.long	0x23c6
	.long	.LLST79
	.long	.LVUS79
	.uleb128 0x34
	.long	0x23d3
	.long	.LLST80
	.long	.LVUS80
	.byte	0
	.uleb128 0x50
	.long	0x23e7
	.quad	.LBI197
	.value	.LVU1068
	.long	.Ldebug_ranges0+0x3c0
	.byte	0x1
	.byte	0x6d
	.byte	0x24
	.long	0x1faf
	.uleb128 0x32
	.long	0x23f9
	.long	.LLST81
	.long	.LVUS81
	.byte	0
	.uleb128 0x50
	.long	0x18b2
	.quad	.LBI203
	.value	.LVU1186
	.long	.Ldebug_ranges0+0x430
	.byte	0x1
	.byte	0x7c
	.byte	0x9
	.long	0x212c
	.uleb128 0x32
	.long	0x18c4
	.long	.LLST83
	.long	.LVUS83
	.uleb128 0x33
	.long	.Ldebug_ranges0+0x430
	.uleb128 0x34
	.long	0x18d1
	.long	.LLST84
	.long	.LVUS84
	.uleb128 0x35
	.long	0x1dce
	.quad	.LBI205
	.value	.LVU1193
	.long	.Ldebug_ranges0+0x470
	.byte	0x1
	.value	0x137
	.byte	0x7
	.long	0x20ab
	.uleb128 0x32
	.long	0x1deb
	.long	.LLST85
	.long	.LVUS85
	.uleb128 0x32
	.long	0x1ddf
	.long	.LLST86
	.long	.LVUS86
	.uleb128 0x33
	.long	.Ldebug_ranges0+0x470
	.uleb128 0x36
	.long	0x1df6
	.uleb128 0x37
	.long	0x2407
	.quad	.LBI207
	.value	.LVU1198
	.long	.Ldebug_ranges0+0x4b0
	.byte	0x1
	.byte	0x9a
	.byte	0xa
	.uleb128 0x32
	.long	0x2418
	.long	.LLST87
	.long	.LVUS87
	.uleb128 0x32
	.long	0x2424
	.long	.LLST88
	.long	.LVUS88
	.uleb128 0x33
	.long	.Ldebug_ranges0+0x4f0
	.uleb128 0x34
	.long	0x2430
	.long	.LLST89
	.long	.LVUS89
	.uleb128 0x34
	.long	0x243c
	.long	.LLST90
	.long	.LVUS90
	.uleb128 0x37
	.long	0x25c1
	.quad	.LBI209
	.value	.LVU1204
	.long	.Ldebug_ranges0+0x540
	.byte	0x1
	.byte	0x3a
	.byte	0xd3
	.uleb128 0x32
	.long	0x25d2
	.long	.LLST91
	.long	.LVUS91
	.uleb128 0x32
	.long	0x25dc
	.long	.LLST92
	.long	.LVUS92
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x35
	.long	0x1daa
	.quad	.LBI220
	.value	.LVU1136
	.long	.Ldebug_ranges0+0x570
	.byte	0x1
	.value	0x13f
	.byte	0x3
	.long	0x20ee
	.uleb128 0x32
	.long	0x1dc1
	.long	.LLST93
	.long	.LVUS93
	.uleb128 0x32
	.long	0x1db7
	.long	.LLST94
	.long	.LVUS94
	.uleb128 0x52
	.quad	.LVL214
	.long	0x2684
	.byte	0
	.uleb128 0x3a
	.quad	.LVL211
	.long	0x2db9
	.uleb128 0x3b
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x9
	.byte	0x3
	.quad	.LC4
	.uleb128 0x3b
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x9
	.byte	0x3
	.quad	.LC0
	.uleb128 0x3b
	.uleb128 0x1
	.byte	0x51
	.uleb128 0x3
	.byte	0xa
	.value	0x138
	.uleb128 0x3b
	.uleb128 0x1
	.byte	0x52
	.uleb128 0x9
	.byte	0x3
	.quad	__PRETTY_FUNCTION__.10164
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x50
	.long	0x1daa
	.quad	.LBI232
	.value	.LVU1231
	.long	.Ldebug_ranges0+0x5b0
	.byte	0x1
	.byte	0x82
	.byte	0x7
	.long	0x217d
	.uleb128 0x32
	.long	0x1dc1
	.long	.LLST95
	.long	.LVUS95
	.uleb128 0x32
	.long	0x1db7
	.long	.LLST96
	.long	.LVUS96
	.uleb128 0x3a
	.quad	.LVL233
	.long	0x2684
	.uleb128 0x3b
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x2
	.byte	0x7f
	.sleb128 0
	.uleb128 0x3b
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x4
	.byte	0x91
	.sleb128 -184
	.byte	0x6
	.byte	0
	.byte	0
	.uleb128 0x50
	.long	0x1905
	.quad	.LBI237
	.value	.LVU1278
	.long	.Ldebug_ranges0+0x610
	.byte	0x1
	.byte	0x8c
	.byte	0xf
	.long	0x22cf
	.uleb128 0x32
	.long	0x1939
	.long	.LLST98
	.long	.LVUS98
	.uleb128 0x32
	.long	0x192d
	.long	.LLST99
	.long	.LVUS99
	.uleb128 0x32
	.long	0x1922
	.long	.LLST100
	.long	.LVUS100
	.uleb128 0x32
	.long	0x1916
	.long	.LLST101
	.long	.LVUS101
	.uleb128 0x33
	.long	.Ldebug_ranges0+0x610
	.uleb128 0x36
	.long	0x1945
	.uleb128 0x36
	.long	0x194f
	.uleb128 0x36
	.long	0x195b
	.uleb128 0x34
	.long	0x1967
	.long	.LLST102
	.long	.LVUS102
	.uleb128 0x36
	.long	0x1974
	.uleb128 0x57
	.long	0x1980
	.uleb128 0x35
	.long	0x2371
	.quad	.LBI239
	.value	.LVU1288
	.long	.Ldebug_ranges0+0x670
	.byte	0x1
	.value	0x106
	.byte	0x9
	.long	0x22a6
	.uleb128 0x32
	.long	0x2382
	.long	.LLST103
	.long	.LVUS103
	.uleb128 0x33
	.long	.Ldebug_ranges0+0x670
	.uleb128 0x34
	.long	0x238e
	.long	.LLST104
	.long	.LVUS104
	.uleb128 0x53
	.quad	.LVL240
	.long	0x2df2
	.long	0x224b
	.uleb128 0x3b
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x5
	.byte	0xc
	.long	0x80800
	.byte	0
	.uleb128 0x53
	.quad	.LVL242
	.long	0x2dfe
	.long	0x2272
	.uleb128 0x3b
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x4
	.byte	0x91
	.sleb128 -168
	.byte	0x6
	.uleb128 0x3b
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x9
	.byte	0x3
	.quad	uv__inotify_read
	.byte	0
	.uleb128 0x53
	.quad	.LVL243
	.long	0x2e0a
	.long	0x2297
	.uleb128 0x3b
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x2
	.byte	0x7c
	.sleb128 0
	.uleb128 0x3b
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x4
	.byte	0x91
	.sleb128 -168
	.byte	0x6
	.uleb128 0x3b
	.uleb128 0x1
	.byte	0x51
	.uleb128 0x1
	.byte	0x31
	.byte	0
	.uleb128 0x52
	.quad	.LVL245
	.long	0x2ddd
	.byte	0
	.byte	0
	.uleb128 0x3a
	.quad	.LVL226
	.long	0x27e5
	.uleb128 0x3b
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x2
	.byte	0x7f
	.sleb128 0
	.uleb128 0x3b
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x3b
	.uleb128 0x1
	.byte	0x51
	.uleb128 0x2
	.byte	0x7e
	.sleb128 0
	.uleb128 0x58
	.long	0x1939
	.uleb128 0x1
	.byte	0x30
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x52
	.quad	.LVL201
	.long	0x2e16
	.uleb128 0x53
	.quad	.LVL228
	.long	0x2e23
	.long	0x22f4
	.uleb128 0x3b
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x2
	.byte	0x7e
	.sleb128 0
	.byte	0
	.uleb128 0x53
	.quad	.LVL248
	.long	0x2e23
	.long	0x230c
	.uleb128 0x3b
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x2
	.byte	0x7e
	.sleb128 0
	.byte	0
	.uleb128 0x53
	.quad	.LVL251
	.long	0x2e23
	.long	0x2324
	.uleb128 0x3b
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x2
	.byte	0x7e
	.sleb128 0
	.byte	0
	.uleb128 0x53
	.quad	.LVL253
	.long	0x2db9
	.long	0x2363
	.uleb128 0x3b
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x9
	.byte	0x3
	.quad	.LC3
	.uleb128 0x3b
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x9
	.byte	0x3
	.quad	.LC0
	.uleb128 0x3b
	.uleb128 0x1
	.byte	0x51
	.uleb128 0x2
	.byte	0x8
	.byte	0x79
	.uleb128 0x3b
	.uleb128 0x1
	.byte	0x52
	.uleb128 0x9
	.byte	0x3
	.quad	__PRETTY_FUNCTION__.10096
	.byte	0
	.uleb128 0x52
	.quad	.LVL254
	.long	0x2de9
	.byte	0
	.uleb128 0x56
	.long	.LASF394
	.byte	0x1
	.byte	0x44
	.byte	0xc
	.long	0x57
	.byte	0x1
	.long	0x239a
	.uleb128 0x41
	.long	.LASF288
	.byte	0x1
	.byte	0x44
	.byte	0x24
	.long	0x12cd
	.uleb128 0x43
	.string	"fd"
	.byte	0x1
	.byte	0x45
	.byte	0x7
	.long	0x57
	.byte	0
	.uleb128 0x59
	.long	.LASF395
	.byte	0x1
	.byte	0x3a
	.value	0x513
	.long	0x16d6
	.byte	0x1
	.long	0x23e1
	.uleb128 0x5a
	.long	.LASF396
	.byte	0x1
	.byte	0x3a
	.value	0x53f
	.long	0x23e1
	.uleb128 0x5b
	.string	"val"
	.byte	0x1
	.byte	0x3a
	.value	0x549
	.long	0x57
	.uleb128 0x5c
	.string	"tmp"
	.byte	0x1
	.byte	0x3a
	.value	0x565
	.long	0x16d6
	.uleb128 0x5d
	.long	.LASF397
	.byte	0x1
	.byte	0x3a
	.value	0x592
	.long	0x16d6
	.byte	0
	.uleb128 0x3
	.byte	0x8
	.long	0x16dc
	.uleb128 0x59
	.long	.LASF398
	.byte	0x1
	.byte	0x3a
	.value	0x103
	.long	0x16d6
	.byte	0x1
	.long	0x2407
	.uleb128 0x5b
	.string	"elm"
	.byte	0x1
	.byte	0x3a
	.value	0x12d
	.long	0x16d6
	.byte	0
	.uleb128 0x56
	.long	.LASF399
	.byte	0x1
	.byte	0x3a
	.byte	0x3b
	.long	0x16d6
	.byte	0x1
	.long	0x2449
	.uleb128 0x41
	.long	.LASF396
	.byte	0x1
	.byte	0x3a
	.byte	0x65
	.long	0x23e1
	.uleb128 0x42
	.string	"elm"
	.byte	0x1
	.byte	0x3a
	.byte	0x80
	.long	0x16d6
	.uleb128 0x43
	.string	"tmp"
	.byte	0x1
	.byte	0x3a
	.byte	0x9c
	.long	0x16d6
	.uleb128 0x44
	.long	.LASF400
	.byte	0x1
	.byte	0x3a
	.byte	0xb8
	.long	0x57
	.byte	0
	.uleb128 0x59
	.long	.LASF401
	.byte	0x1
	.byte	0x3a
	.value	0x1c3
	.long	0x16d6
	.byte	0x1
	.long	0x249c
	.uleb128 0x5a
	.long	.LASF396
	.byte	0x1
	.byte	0x3a
	.value	0x1ef
	.long	0x23e1
	.uleb128 0x5b
	.string	"elm"
	.byte	0x1
	.byte	0x3a
	.value	0x20a
	.long	0x16d6
	.uleb128 0x5c
	.string	"tmp"
	.byte	0x1
	.byte	0x3a
	.value	0x226
	.long	0x16d6
	.uleb128 0x5d
	.long	.LASF397
	.byte	0x1
	.byte	0x3a
	.value	0x240
	.long	0x16d6
	.uleb128 0x44
	.long	.LASF400
	.byte	0x1
	.byte	0x3a
	.byte	0x7
	.long	0x57
	.byte	0
	.uleb128 0x59
	.long	.LASF402
	.byte	0x1
	.byte	0x3a
	.value	0x1ed
	.long	0x16d6
	.byte	0x1
	.long	0x2514
	.uleb128 0x5a
	.long	.LASF396
	.byte	0x1
	.byte	0x3a
	.value	0x219
	.long	0x23e1
	.uleb128 0x5b
	.string	"elm"
	.byte	0x1
	.byte	0x3a
	.value	0x234
	.long	0x16d6
	.uleb128 0x5d
	.long	.LASF403
	.byte	0x1
	.byte	0x3a
	.value	0x250
	.long	0x16d6
	.uleb128 0x5d
	.long	.LASF397
	.byte	0x1
	.byte	0x3a
	.value	0x258
	.long	0x16d6
	.uleb128 0x5c
	.string	"old"
	.byte	0x1
	.byte	0x3a
	.value	0x261
	.long	0x16d6
	.uleb128 0x5d
	.long	.LASF404
	.byte	0x1
	.byte	0x3a
	.value	0x270
	.long	0x57
	.uleb128 0x5e
	.long	.LASF404
	.byte	0x1
	.byte	0x3a
	.value	0x134
	.uleb128 0x5f
	.uleb128 0x44
	.long	.LASF406
	.byte	0x1
	.byte	0x3a
	.byte	0x3e
	.long	0x16d6
	.byte	0
	.byte	0
	.uleb128 0x60
	.long	.LASF408
	.byte	0x1
	.byte	0x3a
	.value	0x1c2
	.byte	0x1
	.long	0x2577
	.uleb128 0x5a
	.long	.LASF396
	.byte	0x1
	.byte	0x3a
	.value	0x1f4
	.long	0x23e1
	.uleb128 0x5a
	.long	.LASF397
	.byte	0x1
	.byte	0x3a
	.value	0x20f
	.long	0x16d6
	.uleb128 0x5b
	.string	"elm"
	.byte	0x1
	.byte	0x3a
	.value	0x22c
	.long	0x16d6
	.uleb128 0x5c
	.string	"tmp"
	.byte	0x1
	.byte	0x3a
	.value	0x248
	.long	0x16d6
	.uleb128 0x61
	.long	0x2568
	.uleb128 0x44
	.long	.LASF409
	.byte	0x1
	.byte	0x3a
	.byte	0x4b
	.long	0x16d6
	.byte	0
	.uleb128 0x5f
	.uleb128 0x44
	.long	.LASF410
	.byte	0x1
	.byte	0x3a
	.byte	0x4a
	.long	0x16d6
	.byte	0
	.byte	0
	.uleb128 0x55
	.long	.LASF411
	.byte	0x1
	.byte	0x3a
	.byte	0x25
	.byte	0x1
	.long	0x25c1
	.uleb128 0x41
	.long	.LASF396
	.byte	0x1
	.byte	0x3a
	.byte	0x57
	.long	0x23e1
	.uleb128 0x42
	.string	"elm"
	.byte	0x1
	.byte	0x3a
	.byte	0x72
	.long	0x16d6
	.uleb128 0x44
	.long	.LASF397
	.byte	0x1
	.byte	0x3a
	.byte	0x8e
	.long	0x16d6
	.uleb128 0x44
	.long	.LASF412
	.byte	0x1
	.byte	0x3a
	.byte	0x97
	.long	0x16d6
	.uleb128 0x43
	.string	"tmp"
	.byte	0x1
	.byte	0x3a
	.byte	0xa1
	.long	0x16d6
	.byte	0
	.uleb128 0x56
	.long	.LASF413
	.byte	0x1
	.byte	0x32
	.byte	0xc
	.long	0x57
	.byte	0x1
	.long	0x25e7
	.uleb128 0x42
	.string	"a"
	.byte	0x1
	.byte	0x32
	.byte	0x38
	.long	0x25e7
	.uleb128 0x42
	.string	"b"
	.byte	0x1
	.byte	0x33
	.byte	0x38
	.long	0x25e7
	.byte	0
	.uleb128 0x3
	.byte	0x8
	.long	0x16d1
	.uleb128 0x62
	.long	.LASF418
	.byte	0x2
	.byte	0x22
	.byte	0x1
	.long	0x2f9
	.byte	0x3
	.long	0x2623
	.uleb128 0x41
	.long	.LASF414
	.byte	0x2
	.byte	0x22
	.byte	0xb
	.long	0x57
	.uleb128 0x41
	.long	.LASF415
	.byte	0x2
	.byte	0x22
	.byte	0x17
	.long	0x7f
	.uleb128 0x41
	.long	.LASF416
	.byte	0x2
	.byte	0x22
	.byte	0x25
	.long	0x65
	.byte	0
	.uleb128 0x63
	.long	.LASF417
	.byte	0x3
	.value	0x132
	.byte	0x26
	.long	0x39
	.byte	0x1
	.long	0x264e
	.uleb128 0x3d
	.long	.LASF299
	.byte	0x3
	.value	0x132
	.byte	0x41
	.long	0x2ee
	.uleb128 0x3e
	.string	"s"
	.byte	0x3
	.value	0x133
	.byte	0x9
	.long	0x39
	.byte	0
	.uleb128 0x62
	.long	.LASF419
	.byte	0x4
	.byte	0x1f
	.byte	0x2a
	.long	0x7f
	.byte	0x3
	.long	0x2684
	.uleb128 0x41
	.long	.LASF420
	.byte	0x4
	.byte	0x1f
	.byte	0x43
	.long	0x81
	.uleb128 0x41
	.long	.LASF421
	.byte	0x4
	.byte	0x1f
	.byte	0x62
	.long	0x1574
	.uleb128 0x41
	.long	.LASF422
	.byte	0x4
	.byte	0x1f
	.byte	0x70
	.long	0x65
	.byte	0
	.uleb128 0x64
	.long	0x1daa
	.quad	.LFB118
	.quad	.LFE118-.LFB118
	.uleb128 0x1
	.byte	0x9c
	.long	0x27e5
	.uleb128 0x32
	.long	0x1db7
	.long	.LLST0
	.long	.LVUS0
	.uleb128 0x32
	.long	0x1dc1
	.long	.LLST1
	.long	.LVUS1
	.uleb128 0x50
	.long	0x249c
	.quad	.LBI73
	.value	.LVU2
	.long	.Ldebug_ranges0+0
	.byte	0x1
	.byte	0xa1
	.byte	0x5
	.long	0x27c2
	.uleb128 0x32
	.long	0x24bb
	.long	.LLST2
	.long	.LVUS2
	.uleb128 0x32
	.long	0x24ae
	.long	.LLST3
	.long	.LVUS3
	.uleb128 0x33
	.long	.Ldebug_ranges0+0
	.uleb128 0x34
	.long	0x24c8
	.long	.LLST4
	.long	.LVUS4
	.uleb128 0x34
	.long	0x24d5
	.long	.LLST5
	.long	.LVUS5
	.uleb128 0x34
	.long	0x24e2
	.long	.LLST6
	.long	.LVUS6
	.uleb128 0x34
	.long	0x24ef
	.long	.LLST7
	.long	.LVUS7
	.uleb128 0x65
	.long	0x24fc
	.quad	.L16
	.uleb128 0x66
	.long	0x2505
	.long	.Ldebug_ranges0+0x40
	.long	0x274e
	.uleb128 0x34
	.long	0x2506
	.long	.LLST8
	.long	.LVUS8
	.byte	0
	.uleb128 0x67
	.long	0x2514
	.long	.Ldebug_ranges0+0x90
	.byte	0x1
	.byte	0x3a
	.value	0x14b
	.uleb128 0x68
	.long	0x2522
	.uleb128 0x32
	.long	0x253c
	.long	.LLST9
	.long	.LVUS9
	.uleb128 0x32
	.long	0x252f
	.long	.LLST10
	.long	.LVUS10
	.uleb128 0x33
	.long	.Ldebug_ranges0+0x90
	.uleb128 0x34
	.long	0x2549
	.long	.LLST11
	.long	.LVUS11
	.uleb128 0x66
	.long	0x2568
	.long	.Ldebug_ranges0+0xf0
	.long	0x27a7
	.uleb128 0x34
	.long	0x2569
	.long	.LLST12
	.long	.LVUS12
	.byte	0
	.uleb128 0x69
	.long	0x2556
	.long	.Ldebug_ranges0+0x130
	.uleb128 0x34
	.long	0x255b
	.long	.LLST13
	.long	.LVUS13
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x52
	.quad	.LVL17
	.long	0x2e30
	.uleb128 0x6a
	.quad	.LVL19
	.long	0x2e23
	.uleb128 0x3b
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x3
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.byte	0
	.byte	0
	.uleb128 0x64
	.long	0x1905
	.quad	.LFB119
	.quad	.LFE119-.LFB119
	.uleb128 0x1
	.byte	0x9c
	.long	0x2aec
	.uleb128 0x32
	.long	0x1916
	.long	.LLST40
	.long	.LVUS40
	.uleb128 0x32
	.long	0x1922
	.long	.LLST41
	.long	.LVUS41
	.uleb128 0x32
	.long	0x192d
	.long	.LLST42
	.long	.LVUS42
	.uleb128 0x34
	.long	0x1945
	.long	.LLST43
	.long	.LVUS43
	.uleb128 0x34
	.long	0x194f
	.long	.LLST44
	.long	.LVUS44
	.uleb128 0x6b
	.long	0x195b
	.value	0xfc6
	.uleb128 0x36
	.long	0x1967
	.uleb128 0x34
	.long	0x1974
	.long	.LLST45
	.long	.LVUS45
	.uleb128 0x65
	.long	0x1980
	.quad	.L200
	.uleb128 0x32
	.long	0x1939
	.long	.LLST46
	.long	.LVUS46
	.uleb128 0x35
	.long	0x1dce
	.quad	.LBI137
	.value	.LVU638
	.long	.Ldebug_ranges0+0x250
	.byte	0x1
	.value	0x117
	.byte	0x7
	.long	0x2943
	.uleb128 0x32
	.long	0x1deb
	.long	.LLST47
	.long	.LVUS47
	.uleb128 0x32
	.long	0x1ddf
	.long	.LLST48
	.long	.LVUS48
	.uleb128 0x33
	.long	.Ldebug_ranges0+0x250
	.uleb128 0x36
	.long	0x1df6
	.uleb128 0x37
	.long	0x2407
	.quad	.LBI139
	.value	.LVU643
	.long	.Ldebug_ranges0+0x290
	.byte	0x1
	.byte	0x9a
	.byte	0xa
	.uleb128 0x32
	.long	0x2418
	.long	.LLST49
	.long	.LVUS49
	.uleb128 0x32
	.long	0x2424
	.long	.LLST50
	.long	.LVUS50
	.uleb128 0x33
	.long	.Ldebug_ranges0+0x290
	.uleb128 0x34
	.long	0x2430
	.long	.LLST51
	.long	.LVUS51
	.uleb128 0x34
	.long	0x243c
	.long	.LLST52
	.long	.LVUS52
	.uleb128 0x38
	.long	0x25c1
	.quad	.LBI141
	.value	.LVU649
	.quad	.LBB141
	.quad	.LBE141-.LBB141
	.byte	0x1
	.byte	0x3a
	.byte	0xd3
	.uleb128 0x32
	.long	0x25d2
	.long	.LLST53
	.long	.LVUS53
	.uleb128 0x32
	.long	0x25dc
	.long	.LLST54
	.long	.LVUS54
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x35
	.long	0x264e
	.quad	.LBI151
	.value	.LVU721
	.long	.Ldebug_ranges0+0x2d0
	.byte	0x1
	.value	0x121
	.byte	0xd
	.long	0x29a8
	.uleb128 0x32
	.long	0x2677
	.long	.LLST55
	.long	.LVUS55
	.uleb128 0x32
	.long	0x266b
	.long	.LLST56
	.long	.LVUS56
	.uleb128 0x32
	.long	0x265f
	.long	.LLST57
	.long	.LVUS57
	.uleb128 0x3a
	.quad	.LVL132
	.long	0x2e3c
	.uleb128 0x3b
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x3
	.byte	0x73
	.sleb128 72
	.uleb128 0x3b
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x2
	.byte	0x7f
	.sleb128 0
	.uleb128 0x3b
	.uleb128 0x1
	.byte	0x51
	.uleb128 0x3
	.byte	0x76
	.sleb128 -56
	.byte	0x6
	.byte	0
	.byte	0
	.uleb128 0x35
	.long	0x2449
	.quad	.LBI155
	.value	.LVU738
	.long	.Ldebug_ranges0+0x300
	.byte	0x1
	.value	0x124
	.byte	0x3
	.long	0x2a8c
	.uleb128 0x32
	.long	0x2468
	.long	.LLST58
	.long	.LVUS58
	.uleb128 0x32
	.long	0x245b
	.long	.LLST59
	.long	.LVUS59
	.uleb128 0x33
	.long	.Ldebug_ranges0+0x300
	.uleb128 0x34
	.long	0x2475
	.long	.LLST60
	.long	.LVUS60
	.uleb128 0x34
	.long	0x2482
	.long	.LLST61
	.long	.LVUS61
	.uleb128 0x34
	.long	0x248f
	.long	.LLST62
	.long	.LVUS62
	.uleb128 0x50
	.long	0x25c1
	.quad	.LBI157
	.value	.LVU757
	.long	.Ldebug_ranges0+0x330
	.byte	0x1
	.byte	0x3a
	.byte	0x4c
	.long	0x2a3e
	.uleb128 0x32
	.long	0x25d2
	.long	.LLST63
	.long	.LVUS63
	.uleb128 0x32
	.long	0x25dc
	.long	.LLST64
	.long	.LVUS64
	.byte	0
	.uleb128 0x6c
	.long	0x2577
	.long	.Ldebug_ranges0+0x370
	.byte	0x1
	.byte	0x3a
	.byte	0x89
	.uleb128 0x68
	.long	0x2584
	.uleb128 0x32
	.long	0x2590
	.long	.LLST65
	.long	.LVUS65
	.uleb128 0x33
	.long	.Ldebug_ranges0+0x370
	.uleb128 0x34
	.long	0x259c
	.long	.LLST66
	.long	.LVUS66
	.uleb128 0x34
	.long	0x25a8
	.long	.LLST67
	.long	.LVUS67
	.uleb128 0x34
	.long	0x25b4
	.long	.LLST68
	.long	.LVUS68
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x53
	.quad	.LVL109
	.long	0x2e47
	.long	0x2aab
	.uleb128 0x3b
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x2
	.byte	0x7f
	.sleb128 0
	.uleb128 0x3b
	.uleb128 0x1
	.byte	0x51
	.uleb128 0x3
	.byte	0xa
	.value	0xfc6
	.byte	0
	.uleb128 0x53
	.quad	.LVL126
	.long	0x2e53
	.long	0x2ac3
	.uleb128 0x3b
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x2
	.byte	0x7f
	.sleb128 0
	.byte	0
	.uleb128 0x53
	.quad	.LVL128
	.long	0x2e60
	.long	0x2ade
	.uleb128 0x3b
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x5
	.byte	0x76
	.sleb128 -56
	.byte	0x6
	.byte	0x23
	.uleb128 0x48
	.byte	0
	.uleb128 0x52
	.quad	.LVL160
	.long	0x2ddd
	.byte	0
	.uleb128 0x64
	.long	0x1905
	.quad	.LFB110
	.quad	.LFE110-.LFB110
	.uleb128 0x1
	.byte	0x9c
	.long	0x2c35
	.uleb128 0x32
	.long	0x1916
	.long	.LLST105
	.long	.LVUS105
	.uleb128 0x32
	.long	0x1922
	.long	.LLST106
	.long	.LVUS106
	.uleb128 0x32
	.long	0x192d
	.long	.LLST107
	.long	.LVUS107
	.uleb128 0x32
	.long	0x1939
	.long	.LLST108
	.long	.LVUS108
	.uleb128 0x36
	.long	0x1945
	.uleb128 0x36
	.long	0x194f
	.uleb128 0x36
	.long	0x195b
	.uleb128 0x34
	.long	0x1967
	.long	.LLST109
	.long	.LVUS109
	.uleb128 0x36
	.long	0x1974
	.uleb128 0x35
	.long	0x2371
	.quad	.LBI260
	.value	.LVU1401
	.long	.Ldebug_ranges0+0x6c0
	.byte	0x1
	.value	0x106
	.byte	0x9
	.long	0x2c08
	.uleb128 0x32
	.long	0x2382
	.long	.LLST110
	.long	.LVUS110
	.uleb128 0x33
	.long	.Ldebug_ranges0+0x6c0
	.uleb128 0x34
	.long	0x238e
	.long	.LLST111
	.long	.LVUS111
	.uleb128 0x53
	.quad	.LVL264
	.long	0x2df2
	.long	0x2bb1
	.uleb128 0x3b
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x5
	.byte	0xc
	.long	0x80800
	.byte	0
	.uleb128 0x53
	.quad	.LVL266
	.long	0x2dfe
	.long	0x2bd6
	.uleb128 0x3b
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x2
	.byte	0x73
	.sleb128 0
	.uleb128 0x3b
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x9
	.byte	0x3
	.quad	uv__inotify_read
	.byte	0
	.uleb128 0x53
	.quad	.LVL267
	.long	0x2e0a
	.long	0x2bf9
	.uleb128 0x3b
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x2
	.byte	0x7e
	.sleb128 0
	.uleb128 0x3b
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x2
	.byte	0x73
	.sleb128 0
	.uleb128 0x3b
	.uleb128 0x1
	.byte	0x51
	.uleb128 0x1
	.byte	0x31
	.byte	0
	.uleb128 0x52
	.quad	.LVL269
	.long	0x2ddd
	.byte	0
	.byte	0
	.uleb128 0x6a
	.quad	.LVL262
	.long	0x27e5
	.uleb128 0x3b
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x3
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x3b
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x3
	.byte	0xf3
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x3b
	.uleb128 0x1
	.byte	0x51
	.uleb128 0x3
	.byte	0xf3
	.uleb128 0x1
	.byte	0x51
	.uleb128 0x58
	.long	0x1939
	.uleb128 0x3
	.byte	0xf3
	.uleb128 0x1
	.byte	0x52
	.byte	0
	.byte	0
	.uleb128 0x64
	.long	0x18b2
	.quad	.LFB111
	.quad	.LFE111-.LFB111
	.uleb128 0x1
	.byte	0x9c
	.long	0x2db9
	.uleb128 0x32
	.long	0x18c4
	.long	.LLST112
	.long	.LVUS112
	.uleb128 0x34
	.long	0x18d1
	.long	.LLST113
	.long	.LVUS113
	.uleb128 0x35
	.long	0x1dce
	.quad	.LBI274
	.value	.LVU1446
	.long	.Ldebug_ranges0+0x700
	.byte	0x1
	.value	0x137
	.byte	0x7
	.long	0x2d39
	.uleb128 0x32
	.long	0x1deb
	.long	.LLST114
	.long	.LVUS114
	.uleb128 0x32
	.long	0x1ddf
	.long	.LLST115
	.long	.LVUS115
	.uleb128 0x33
	.long	.Ldebug_ranges0+0x700
	.uleb128 0x36
	.long	0x1df6
	.uleb128 0x37
	.long	0x2407
	.quad	.LBI276
	.value	.LVU1452
	.long	.Ldebug_ranges0+0x710
	.byte	0x1
	.byte	0x9a
	.byte	0xa
	.uleb128 0x32
	.long	0x2418
	.long	.LLST116
	.long	.LVUS116
	.uleb128 0x32
	.long	0x2424
	.long	.LLST117
	.long	.LVUS117
	.uleb128 0x33
	.long	.Ldebug_ranges0+0x710
	.uleb128 0x34
	.long	0x2430
	.long	.LLST118
	.long	.LVUS118
	.uleb128 0x34
	.long	0x243c
	.long	.LLST119
	.long	.LVUS119
	.uleb128 0x38
	.long	0x25c1
	.quad	.LBI278
	.value	.LVU1458
	.quad	.LBB278
	.quad	.LBE278-.LBB278
	.byte	0x1
	.byte	0x3a
	.byte	0xd3
	.uleb128 0x32
	.long	0x25d2
	.long	.LLST120
	.long	.LVUS120
	.uleb128 0x32
	.long	0x25dc
	.long	.LLST121
	.long	.LVUS121
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x35
	.long	0x1daa
	.quad	.LBI288
	.value	.LVU1492
	.long	.Ldebug_ranges0+0x750
	.byte	0x1
	.value	0x13f
	.byte	0x3
	.long	0x2d7c
	.uleb128 0x32
	.long	0x1dc1
	.long	.LLST122
	.long	.LVUS122
	.uleb128 0x32
	.long	0x1db7
	.long	.LLST123
	.long	.LVUS123
	.uleb128 0x52
	.quad	.LVL290
	.long	0x2684
	.byte	0
	.uleb128 0x3a
	.quad	.LVL294
	.long	0x2db9
	.uleb128 0x3b
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x9
	.byte	0x3
	.quad	.LC4
	.uleb128 0x3b
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x9
	.byte	0x3
	.quad	.LC0
	.uleb128 0x3b
	.uleb128 0x1
	.byte	0x51
	.uleb128 0x3
	.byte	0xa
	.value	0x138
	.uleb128 0x3b
	.uleb128 0x1
	.byte	0x52
	.uleb128 0x9
	.byte	0x3
	.quad	__PRETTY_FUNCTION__.10164
	.byte	0
	.byte	0
	.uleb128 0x6d
	.long	.LASF423
	.long	.LASF423
	.byte	0x1e
	.byte	0x45
	.byte	0xd
	.uleb128 0x6d
	.long	.LASF418
	.long	.LASF424
	.byte	0x2
	.byte	0x19
	.byte	0x10
	.uleb128 0x6d
	.long	.LASF425
	.long	.LASF425
	.byte	0x1f
	.byte	0xfd
	.byte	0xe
	.uleb128 0x6d
	.long	.LASF426
	.long	.LASF426
	.byte	0x5
	.byte	0x25
	.byte	0xd
	.uleb128 0x6e
	.long	.LASF443
	.long	.LASF443
	.uleb128 0x6d
	.long	.LASF427
	.long	.LASF427
	.byte	0x1b
	.byte	0x58
	.byte	0xc
	.uleb128 0x6d
	.long	.LASF428
	.long	.LASF428
	.byte	0x3
	.byte	0xc6
	.byte	0x6
	.uleb128 0x6d
	.long	.LASF429
	.long	.LASF429
	.byte	0x3
	.byte	0xc7
	.byte	0x6
	.uleb128 0x6f
	.long	.LASF430
	.long	.LASF430
	.byte	0x19
	.value	0x14a
	.byte	0x7
	.uleb128 0x6f
	.long	.LASF431
	.long	.LASF431
	.byte	0x19
	.value	0x14d
	.byte	0x6
	.uleb128 0x6d
	.long	.LASF432
	.long	.LASF432
	.byte	0x1b
	.byte	0x60
	.byte	0xc
	.uleb128 0x70
	.long	.LASF419
	.long	.LASF444
	.byte	0x20
	.byte	0
	.uleb128 0x6d
	.long	.LASF433
	.long	.LASF433
	.byte	0x1b
	.byte	0x5c
	.byte	0xc
	.uleb128 0x6f
	.long	.LASF434
	.long	.LASF434
	.byte	0x1f
	.value	0x181
	.byte	0xf
	.uleb128 0x6f
	.long	.LASF435
	.long	.LASF435
	.byte	0x19
	.value	0x14c
	.byte	0x7
	.byte	0
	.section	.debug_abbrev,"",@progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1b
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x7
	.uleb128 0x10
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x37
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x13
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x13
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3c
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x21
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0x17
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0x15
	.byte	0
	.uleb128 0x27
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0x16
	.uleb128 0x13
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0xb
	.uleb128 0x5
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x17
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x18
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0x5
	.byte	0
	.byte	0
	.uleb128 0x19
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0x5
	.byte	0
	.byte	0
	.uleb128 0x1a
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1b
	.uleb128 0x5
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1c
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x1d
	.uleb128 0x4
	.byte	0x1
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1e
	.uleb128 0x28
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1c
	.uleb128 0xd
	.byte	0
	.byte	0
	.uleb128 0x1f
	.uleb128 0x28
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1c
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x20
	.uleb128 0x13
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x21
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x22
	.uleb128 0x17
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x23
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x24
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x25
	.uleb128 0x4
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x26
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x27
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0x5
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x28
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x29
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x2a
	.uleb128 0x28
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1c
	.uleb128 0x5
	.byte	0
	.byte	0
	.uleb128 0x2b
	.uleb128 0x28
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1c
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2c
	.uleb128 0x26
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x2d
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x2e
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x2f
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x7
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2117
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x30
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x17
	.uleb128 0x2137
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x31
	.uleb128 0x1d
	.byte	0x1
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x2138
	.uleb128 0x5
	.uleb128 0x55
	.uleb128 0x17
	.uleb128 0x58
	.uleb128 0xb
	.uleb128 0x59
	.uleb128 0x5
	.uleb128 0x57
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x32
	.uleb128 0x5
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x17
	.uleb128 0x2137
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x33
	.uleb128 0xb
	.byte	0x1
	.uleb128 0x55
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x34
	.uleb128 0x34
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x17
	.uleb128 0x2137
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x35
	.uleb128 0x1d
	.byte	0x1
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x2138
	.uleb128 0x5
	.uleb128 0x55
	.uleb128 0x17
	.uleb128 0x58
	.uleb128 0xb
	.uleb128 0x59
	.uleb128 0x5
	.uleb128 0x57
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x36
	.uleb128 0x34
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x37
	.uleb128 0x1d
	.byte	0x1
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x2138
	.uleb128 0x5
	.uleb128 0x55
	.uleb128 0x17
	.uleb128 0x58
	.uleb128 0xb
	.uleb128 0x59
	.uleb128 0xb
	.uleb128 0x57
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x38
	.uleb128 0x1d
	.byte	0x1
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x2138
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x7
	.uleb128 0x58
	.uleb128 0xb
	.uleb128 0x59
	.uleb128 0xb
	.uleb128 0x57
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x39
	.uleb128 0x4109
	.byte	0
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x2115
	.uleb128 0x19
	.uleb128 0x31
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x3a
	.uleb128 0x4109
	.byte	0x1
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x31
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x3b
	.uleb128 0x410a
	.byte	0
	.uleb128 0x2
	.uleb128 0x18
	.uleb128 0x2111
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x3c
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x20
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x3d
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x3e
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x3f
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x34
	.uleb128 0x19
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x40
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x20
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x41
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x42
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x43
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x44
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x45
	.uleb128 0xa
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x46
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x7
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2117
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x47
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x48
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x7
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2117
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x49
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x17
	.uleb128 0x2137
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x4a
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x17
	.uleb128 0x2137
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x4b
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x4c
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x17
	.uleb128 0x2137
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x4d
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x4e
	.uleb128 0xb
	.byte	0x1
	.uleb128 0x55
	.uleb128 0x17
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4f
	.uleb128 0x1d
	.byte	0x1
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x2138
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x7
	.uleb128 0x58
	.uleb128 0xb
	.uleb128 0x59
	.uleb128 0xb
	.uleb128 0x57
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x50
	.uleb128 0x1d
	.byte	0x1
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x2138
	.uleb128 0x5
	.uleb128 0x55
	.uleb128 0x17
	.uleb128 0x58
	.uleb128 0xb
	.uleb128 0x59
	.uleb128 0xb
	.uleb128 0x57
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x51
	.uleb128 0x4109
	.byte	0x1
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x52
	.uleb128 0x4109
	.byte	0
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x31
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x53
	.uleb128 0x4109
	.byte	0x1
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x54
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0x5
	.byte	0
	.byte	0
	.uleb128 0x55
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x20
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x56
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x20
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x57
	.uleb128 0xa
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x58
	.uleb128 0x410a
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x2111
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x59
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x20
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x5a
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x5b
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x5c
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x5d
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x5e
	.uleb128 0xa
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0x5
	.byte	0
	.byte	0
	.uleb128 0x5f
	.uleb128 0xb
	.byte	0x1
	.byte	0
	.byte	0
	.uleb128 0x60
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x20
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x61
	.uleb128 0xb
	.byte	0x1
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x62
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x20
	.uleb128 0xb
	.uleb128 0x34
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x63
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x20
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x64
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x7
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2117
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x65
	.uleb128 0xa
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.byte	0
	.byte	0
	.uleb128 0x66
	.uleb128 0xb
	.byte	0x1
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x55
	.uleb128 0x17
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x67
	.uleb128 0x1d
	.byte	0x1
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x55
	.uleb128 0x17
	.uleb128 0x58
	.uleb128 0xb
	.uleb128 0x59
	.uleb128 0xb
	.uleb128 0x57
	.uleb128 0x5
	.byte	0
	.byte	0
	.uleb128 0x68
	.uleb128 0x5
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x69
	.uleb128 0xb
	.byte	0x1
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x55
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x6a
	.uleb128 0x4109
	.byte	0x1
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x2115
	.uleb128 0x19
	.uleb128 0x31
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6b
	.uleb128 0x34
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x1c
	.uleb128 0x5
	.byte	0
	.byte	0
	.uleb128 0x6c
	.uleb128 0x1d
	.byte	0x1
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x55
	.uleb128 0x17
	.uleb128 0x58
	.uleb128 0xb
	.uleb128 0x59
	.uleb128 0xb
	.uleb128 0x57
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x6d
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.uleb128 0x6e
	.uleb128 0xe
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x6e
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.uleb128 0x6e
	.uleb128 0xe
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x6f
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.uleb128 0x6e
	.uleb128 0xe
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x70
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.uleb128 0x6e
	.uleb128 0xe
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",@progbits
.Ldebug_loc0:
.LVUS124:
	.uleb128 0
	.uleb128 .LVU1590
	.uleb128 .LVU1590
	.uleb128 .LVU1591
	.uleb128 .LVU1591
	.uleb128 .LVU1597
	.uleb128 .LVU1597
	.uleb128 0
.LLST124:
	.quad	.LVL295-.Ltext0
	.quad	.LVL311-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	.LVL311-.Ltext0
	.quad	.LVL312-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.byte	0x9f
	.quad	.LVL312-.Ltext0
	.quad	.LVL314-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	.LVL314-.Ltext0
	.quad	.LFE112-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS125:
	.uleb128 .LVU1521
	.uleb128 .LVU1590
	.uleb128 .LVU1590
	.uleb128 .LVU1591
	.uleb128 .LVU1591
	.uleb128 .LVU1597
	.uleb128 .LVU1597
	.uleb128 0
.LLST125:
	.quad	.LVL296-.Ltext0
	.quad	.LVL311-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	.LVL311-.Ltext0
	.quad	.LVL312-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.byte	0x9f
	.quad	.LVL312-.Ltext0
	.quad	.LVL314-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	.LVL314-.Ltext0
	.quad	.LFE112-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS126:
	.uleb128 .LVU1545
	.uleb128 .LVU1579
	.uleb128 .LVU1589
	.uleb128 .LVU1591
	.uleb128 .LVU1591
	.uleb128 0
.LLST126:
	.quad	.LVL301-.Ltext0
	.quad	.LVL305-.Ltext0
	.value	0x1
	.byte	0x58
	.quad	.LVL310-.Ltext0
	.quad	.LVL312-1-.Ltext0
	.value	0x1
	.byte	0x58
	.quad	.LVL312-.Ltext0
	.quad	.LFE112-.Ltext0
	.value	0x2
	.byte	0x30
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS127:
	.uleb128 .LVU1529
	.uleb128 .LVU1545
	.uleb128 .LVU1580
	.uleb128 .LVU1589
.LLST127:
	.quad	.LVL298-.Ltext0
	.quad	.LVL301-.Ltext0
	.value	0x1
	.byte	0x52
	.quad	.LVL306-.Ltext0
	.quad	.LVL310-.Ltext0
	.value	0x1
	.byte	0x52
	.quad	0
	.quad	0
.LVUS128:
	.uleb128 .LVU1528
	.uleb128 .LVU1545
	.uleb128 .LVU1580
	.uleb128 .LVU1589
.LLST128:
	.quad	.LVL297-.Ltext0
	.quad	.LVL301-.Ltext0
	.value	0x1
	.byte	0x51
	.quad	.LVL306-.Ltext0
	.quad	.LVL310-.Ltext0
	.value	0x1
	.byte	0x51
	.quad	0
	.quad	0
.LVUS129:
	.uleb128 .LVU1535
	.uleb128 .LVU1567
	.uleb128 .LVU1567
	.uleb128 .LVU1568
	.uleb128 .LVU1580
	.uleb128 .LVU1589
	.uleb128 .LVU1591
	.uleb128 .LVU1596
.LLST129:
	.quad	.LVL299-.Ltext0
	.quad	.LVL302-.Ltext0
	.value	0x4
	.byte	0x71
	.sleb128 832
	.byte	0x9f
	.quad	.LVL302-.Ltext0
	.quad	.LVL303-.Ltext0
	.value	0x7
	.byte	0x75
	.sleb128 8
	.byte	0x6
	.byte	0x23
	.uleb128 0x340
	.byte	0x9f
	.quad	.LVL306-.Ltext0
	.quad	.LVL310-.Ltext0
	.value	0x4
	.byte	0x71
	.sleb128 832
	.byte	0x9f
	.quad	.LVL312-.Ltext0
	.quad	.LVL313-.Ltext0
	.value	0x4
	.byte	0x71
	.sleb128 832
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS130:
	.uleb128 .LVU1534
	.uleb128 .LVU1545
	.uleb128 .LVU1580
	.uleb128 .LVU1589
.LLST130:
	.quad	.LVL299-.Ltext0
	.quad	.LVL301-.Ltext0
	.value	0x6
	.byte	0xf2
	.long	.Ldebug_info0+6043
	.sleb128 0
	.quad	.LVL306-.Ltext0
	.quad	.LVL310-.Ltext0
	.value	0x6
	.byte	0xf2
	.long	.Ldebug_info0+6043
	.sleb128 0
	.quad	0
	.quad	0
.LVUS131:
	.uleb128 .LVU1536
	.uleb128 .LVU1545
	.uleb128 .LVU1580
	.uleb128 .LVU1589
.LLST131:
	.quad	.LVL299-.Ltext0
	.quad	.LVL301-.Ltext0
	.value	0x1
	.byte	0x58
	.quad	.LVL306-.Ltext0
	.quad	.LVL310-.Ltext0
	.value	0x1
	.byte	0x58
	.quad	0
	.quad	0
.LVUS132:
	.uleb128 .LVU1580
	.uleb128 .LVU1585
.LLST132:
	.quad	.LVL306-.Ltext0
	.quad	.LVL308-.Ltext0
	.value	0x3
	.byte	0x9
	.byte	0xff
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS133:
	.uleb128 .LVU1541
	.uleb128 .LVU1579
	.uleb128 .LVU1580
	.uleb128 .LVU1591
.LLST133:
	.quad	.LVL300-.Ltext0
	.quad	.LVL305-.Ltext0
	.value	0x6
	.byte	0xf2
	.long	.Ldebug_info0+6043
	.sleb128 0
	.quad	.LVL306-.Ltext0
	.quad	.LVL312-.Ltext0
	.value	0x6
	.byte	0xf2
	.long	.Ldebug_info0+6043
	.sleb128 0
	.quad	0
	.quad	0
.LVUS134:
	.uleb128 .LVU1541
	.uleb128 .LVU1579
	.uleb128 .LVU1580
	.uleb128 .LVU1584
	.uleb128 .LVU1585
	.uleb128 .LVU1588
	.uleb128 .LVU1589
	.uleb128 .LVU1591
.LLST134:
	.quad	.LVL300-.Ltext0
	.quad	.LVL305-.Ltext0
	.value	0x1
	.byte	0x58
	.quad	.LVL306-.Ltext0
	.quad	.LVL307-.Ltext0
	.value	0x1
	.byte	0x58
	.quad	.LVL308-.Ltext0
	.quad	.LVL309-.Ltext0
	.value	0x1
	.byte	0x58
	.quad	.LVL310-.Ltext0
	.quad	.LVL312-1-.Ltext0
	.value	0x1
	.byte	0x58
	.quad	0
	.quad	0
.LVUS135:
	.uleb128 .LVU1574
	.uleb128 .LVU1579
	.uleb128 .LVU1589
	.uleb128 .LVU1590
	.uleb128 .LVU1590
	.uleb128 .LVU1591
.LLST135:
	.quad	.LVL304-.Ltext0
	.quad	.LVL305-.Ltext0
	.value	0x2
	.byte	0x75
	.sleb128 8
	.quad	.LVL310-.Ltext0
	.quad	.LVL311-.Ltext0
	.value	0x2
	.byte	0x75
	.sleb128 8
	.quad	.LVL311-.Ltext0
	.quad	.LVL312-1-.Ltext0
	.value	0x5
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.byte	0x23
	.uleb128 0x8
	.quad	0
	.quad	0
.LVUS136:
	.uleb128 .LVU1574
	.uleb128 .LVU1579
	.uleb128 .LVU1589
	.uleb128 .LVU1591
.LLST136:
	.quad	.LVL304-.Ltext0
	.quad	.LVL305-.Ltext0
	.value	0x1
	.byte	0x58
	.quad	.LVL310-.Ltext0
	.quad	.LVL312-1-.Ltext0
	.value	0x1
	.byte	0x58
	.quad	0
	.quad	0
.LVUS14:
	.uleb128 0
	.uleb128 .LVU450
	.uleb128 .LVU450
	.uleb128 .LVU620
	.uleb128 .LVU620
	.uleb128 0
.LLST14:
	.quad	.LVL69-.Ltext0
	.quad	.LVL70-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	.LVL70-.Ltext0
	.quad	.LVL100-.Ltext0
	.value	0x3
	.byte	0x76
	.sleb128 -4192
	.quad	.LVL100-.Ltext0
	.quad	.LFE108-.Ltext0
	.value	0x3
	.byte	0x91
	.sleb128 -4208
	.quad	0
	.quad	0
.LVUS15:
	.uleb128 0
	.uleb128 .LVU450
	.uleb128 .LVU450
	.uleb128 0
.LLST15:
	.quad	.LVL69-.Ltext0
	.quad	.LVL70-.Ltext0
	.value	0x1
	.byte	0x54
	.quad	.LVL70-.Ltext0
	.quad	.LFE108-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x54
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS16:
	.uleb128 0
	.uleb128 .LVU450
	.uleb128 .LVU482
	.uleb128 .LVU489
.LLST16:
	.quad	.LVL69-.Ltext0
	.quad	.LVL70-.Ltext0
	.value	0x1
	.byte	0x51
	.quad	.LVL74-.Ltext0
	.quad	.LVL75-.Ltext0
	.value	0x2
	.byte	0x30
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS17:
	.uleb128 .LVU481
	.uleb128 .LVU588
	.uleb128 .LVU590
	.uleb128 .LVU612
.LLST17:
	.quad	.LVL74-.Ltext0
	.quad	.LVL90-.Ltext0
	.value	0x1
	.byte	0x5c
	.quad	.LVL91-.Ltext0
	.quad	.LVL98-.Ltext0
	.value	0x1
	.byte	0x5c
	.quad	0
	.quad	0
.LVUS18:
	.uleb128 .LVU508
	.uleb128 .LVU584
	.uleb128 .LVU598
	.uleb128 .LVU612
.LLST18:
	.quad	.LVL78-.Ltext0
	.quad	.LVL89-.Ltext0
	.value	0x1
	.byte	0x5f
	.quad	.LVL95-.Ltext0
	.quad	.LVL98-.Ltext0
	.value	0x1
	.byte	0x5f
	.quad	0
	.quad	0
.LVUS19:
	.uleb128 .LVU549
	.uleb128 .LVU554
	.uleb128 .LVU554
	.uleb128 .LVU574
.LLST19:
	.quad	.LVL85-.Ltext0
	.quad	.LVL86-.Ltext0
	.value	0x4
	.byte	0x70
	.sleb128 -112
	.byte	0x9f
	.quad	.LVL86-.Ltext0
	.quad	.LVL87-1-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	0
	.quad	0
.LVUS20:
	.uleb128 .LVU548
	.uleb128 .LVU574
.LLST20:
	.quad	.LVL85-.Ltext0
	.quad	.LVL87-1-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	0
	.quad	0
.LVUS21:
	.uleb128 .LVU519
	.uleb128 .LVU584
	.uleb128 .LVU598
	.uleb128 .LVU612
.LLST21:
	.quad	.LVL81-.Ltext0
	.quad	.LVL89-.Ltext0
	.value	0x1
	.byte	0x5d
	.quad	.LVL95-.Ltext0
	.quad	.LVL98-.Ltext0
	.value	0x1
	.byte	0x5d
	.quad	0
	.quad	0
.LVUS22:
	.uleb128 .LVU469
	.uleb128 .LVU477
	.uleb128 .LVU612
	.uleb128 .LVU614
	.uleb128 .LVU621
	.uleb128 .LVU622
.LLST22:
	.quad	.LVL71-.Ltext0
	.quad	.LVL73-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	.LVL98-.Ltext0
	.quad	.LVL99-1-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	.LVL101-.Ltext0
	.quad	.LVL102-1-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	0
	.quad	0
.LVUS23:
	.uleb128 .LVU475
	.uleb128 .LVU480
	.uleb128 .LVU480
	.uleb128 .LVU612
.LLST23:
	.quad	.LVL72-.Ltext0
	.quad	.LVL74-.Ltext0
	.value	0x3
	.byte	0x76
	.sleb128 -4208
	.quad	.LVL74-.Ltext0
	.quad	.LVL98-.Ltext0
	.value	0x1
	.byte	0x5c
	.quad	0
	.quad	0
.LVUS24:
	.uleb128 .LVU528
	.uleb128 .LVU546
.LLST24:
	.quad	.LVL83-.Ltext0
	.quad	.LVL84-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	0
	.quad	0
.LVUS25:
	.uleb128 .LVU463
	.uleb128 .LVU469
.LLST25:
	.quad	.LVL70-.Ltext0
	.quad	.LVL71-.Ltext0
	.value	0x4
	.byte	0xa
	.value	0x1000
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS26:
	.uleb128 .LVU463
	.uleb128 .LVU469
.LLST26:
	.quad	.LVL70-.Ltext0
	.quad	.LVL71-.Ltext0
	.value	0x3
	.byte	0x76
	.sleb128 -4208
	.quad	0
	.quad	0
.LVUS27:
	.uleb128 .LVU463
	.uleb128 .LVU469
.LLST27:
	.quad	.LVL70-.Ltext0
	.quad	.LVL71-1-.Ltext0
	.value	0x7
	.byte	0x76
	.sleb128 -4192
	.byte	0x6
	.byte	0x23
	.uleb128 0x348
	.quad	0
	.quad	0
.LVUS28:
	.uleb128 .LVU493
	.uleb128 .LVU508
	.uleb128 .LVU590
	.uleb128 .LVU598
.LLST28:
	.quad	.LVL76-.Ltext0
	.quad	.LVL78-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	.LVL91-.Ltext0
	.quad	.LVL95-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	0
	.quad	0
.LVUS29:
	.uleb128 .LVU493
	.uleb128 .LVU508
	.uleb128 .LVU590
	.uleb128 .LVU598
.LLST29:
	.quad	.LVL76-.Ltext0
	.quad	.LVL78-.Ltext0
	.value	0x1
	.byte	0x53
	.quad	.LVL91-.Ltext0
	.quad	.LVL95-.Ltext0
	.value	0x1
	.byte	0x53
	.quad	0
	.quad	0
.LVUS30:
	.uleb128 .LVU498
	.uleb128 .LVU523
	.uleb128 .LVU523
	.uleb128 .LVU590
	.uleb128 .LVU590
	.uleb128 .LVU598
	.uleb128 .LVU598
	.uleb128 .LVU612
.LLST30:
	.quad	.LVL76-.Ltext0
	.quad	.LVL82-.Ltext0
	.value	0x4
	.byte	0x73
	.sleb128 832
	.byte	0x9f
	.quad	.LVL82-.Ltext0
	.quad	.LVL91-.Ltext0
	.value	0x8
	.byte	0x76
	.sleb128 -4192
	.byte	0x6
	.byte	0x23
	.uleb128 0x340
	.byte	0x9f
	.quad	.LVL91-.Ltext0
	.quad	.LVL95-.Ltext0
	.value	0x4
	.byte	0x73
	.sleb128 832
	.byte	0x9f
	.quad	.LVL95-.Ltext0
	.quad	.LVL98-.Ltext0
	.value	0x8
	.byte	0x76
	.sleb128 -4192
	.byte	0x6
	.byte	0x23
	.uleb128 0x340
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS31:
	.uleb128 .LVU497
	.uleb128 .LVU508
	.uleb128 .LVU590
	.uleb128 .LVU598
.LLST31:
	.quad	.LVL76-.Ltext0
	.quad	.LVL78-.Ltext0
	.value	0x6
	.byte	0xf2
	.long	.Ldebug_info0+7064
	.sleb128 0
	.quad	.LVL91-.Ltext0
	.quad	.LVL95-.Ltext0
	.value	0x6
	.byte	0xf2
	.long	.Ldebug_info0+7064
	.sleb128 0
	.quad	0
	.quad	0
.LVUS32:
	.uleb128 .LVU499
	.uleb128 .LVU508
	.uleb128 .LVU590
	.uleb128 .LVU598
.LLST32:
	.quad	.LVL76-.Ltext0
	.quad	.LVL78-.Ltext0
	.value	0x1
	.byte	0x5f
	.quad	.LVL91-.Ltext0
	.quad	.LVL95-.Ltext0
	.value	0x1
	.byte	0x5f
	.quad	0
	.quad	0
.LVUS33:
	.uleb128 .LVU590
	.uleb128 .LVU594
.LLST33:
	.quad	.LVL91-.Ltext0
	.quad	.LVL93-.Ltext0
	.value	0x3
	.byte	0x9
	.byte	0xff
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS34:
	.uleb128 .LVU504
	.uleb128 .LVU584
	.uleb128 .LVU590
	.uleb128 .LVU612
.LLST34:
	.quad	.LVL77-.Ltext0
	.quad	.LVL89-.Ltext0
	.value	0x6
	.byte	0xf2
	.long	.Ldebug_info0+7064
	.sleb128 0
	.quad	.LVL91-.Ltext0
	.quad	.LVL98-.Ltext0
	.value	0x6
	.byte	0xf2
	.long	.Ldebug_info0+7064
	.sleb128 0
	.quad	0
	.quad	0
.LVUS35:
	.uleb128 .LVU504
	.uleb128 .LVU584
	.uleb128 .LVU590
	.uleb128 .LVU593
	.uleb128 .LVU594
	.uleb128 .LVU597
	.uleb128 .LVU598
	.uleb128 .LVU612
.LLST35:
	.quad	.LVL77-.Ltext0
	.quad	.LVL89-.Ltext0
	.value	0x1
	.byte	0x5f
	.quad	.LVL91-.Ltext0
	.quad	.LVL92-.Ltext0
	.value	0x1
	.byte	0x5f
	.quad	.LVL93-.Ltext0
	.quad	.LVL94-.Ltext0
	.value	0x1
	.byte	0x5f
	.quad	.LVL95-.Ltext0
	.quad	.LVL98-.Ltext0
	.value	0x1
	.byte	0x5f
	.quad	0
	.quad	0
.LVUS36:
	.uleb128 .LVU513
	.uleb128 .LVU519
.LLST36:
	.quad	.LVL79-.Ltext0
	.quad	.LVL81-.Ltext0
	.value	0x1
	.byte	0x5d
	.quad	0
	.quad	0
.LVUS37:
	.uleb128 .LVU517
	.uleb128 .LVU519
.LLST37:
	.quad	.LVL80-.Ltext0
	.quad	.LVL81-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	0
	.quad	0
.LVUS38:
	.uleb128 .LVU581
	.uleb128 .LVU584
	.uleb128 .LVU609
	.uleb128 .LVU612
.LLST38:
	.quad	.LVL88-.Ltext0
	.quad	.LVL89-.Ltext0
	.value	0x3
	.byte	0x76
	.sleb128 -4192
	.quad	.LVL96-.Ltext0
	.quad	.LVL98-.Ltext0
	.value	0x3
	.byte	0x76
	.sleb128 -4192
	.quad	0
	.quad	0
.LVUS39:
	.uleb128 .LVU581
	.uleb128 .LVU584
	.uleb128 .LVU609
	.uleb128 .LVU612
.LLST39:
	.quad	.LVL88-.Ltext0
	.quad	.LVL89-.Ltext0
	.value	0x1
	.byte	0x5f
	.quad	.LVL96-.Ltext0
	.quad	.LVL98-.Ltext0
	.value	0x1
	.byte	0x5f
	.quad	0
	.quad	0
.LVUS69:
	.uleb128 0
	.uleb128 .LVU1067
	.uleb128 .LVU1067
	.uleb128 .LVU1303
	.uleb128 .LVU1303
	.uleb128 0
.LLST69:
	.quad	.LVL185-.Ltext0
	.quad	.LVL188-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	.LVL188-.Ltext0
	.quad	.LVL230-.Ltext0
	.value	0x3
	.byte	0x76
	.sleb128 -168
	.quad	.LVL230-.Ltext0
	.quad	.LFE105-.Ltext0
	.value	0x3
	.byte	0x91
	.sleb128 -184
	.quad	0
	.quad	0
.LVUS70:
	.uleb128 0
	.uleb128 .LVU1062
	.uleb128 .LVU1062
	.uleb128 .LVU1067
	.uleb128 .LVU1067
	.uleb128 0
.LLST70:
	.quad	.LVL185-.Ltext0
	.quad	.LVL186-.Ltext0
	.value	0x1
	.byte	0x54
	.quad	.LVL186-.Ltext0
	.quad	.LVL188-.Ltext0
	.value	0x3
	.byte	0x75
	.sleb128 832
	.quad	.LVL188-.Ltext0
	.quad	.LFE105-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x54
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS71:
	.uleb128 .LVU1294
	.uleb128 .LVU1296
	.uleb128 .LVU1296
	.uleb128 .LVU1301
	.uleb128 .LVU1349
	.uleb128 .LVU1352
	.uleb128 .LVU1353
	.uleb128 .LVU1358
.LLST71:
	.quad	.LVL227-.Ltext0
	.quad	.LVL228-1-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	.LVL228-1-.Ltext0
	.quad	.LVL229-.Ltext0
	.value	0x1
	.byte	0x5c
	.quad	.LVL247-.Ltext0
	.quad	.LVL249-.Ltext0
	.value	0x1
	.byte	0x5c
	.quad	.LVL250-.Ltext0
	.quad	.LVL252-.Ltext0
	.value	0x3
	.byte	0x9
	.byte	0xea
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS72:
	.uleb128 .LVU1076
	.uleb128 .LVU1260
	.uleb128 .LVU1304
	.uleb128 .LVU1317
	.uleb128 .LVU1358
	.uleb128 .LVU1359
.LLST72:
	.quad	.LVL191-.Ltext0
	.quad	.LVL220-.Ltext0
	.value	0x3
	.byte	0x76
	.sleb128 -152
	.quad	.LVL231-.Ltext0
	.quad	.LVL234-.Ltext0
	.value	0x3
	.byte	0x91
	.sleb128 -168
	.quad	.LVL252-.Ltext0
	.quad	.LVL253-.Ltext0
	.value	0x3
	.byte	0x91
	.sleb128 -168
	.quad	0
	.quad	0
.LVUS73:
	.uleb128 .LVU1067
	.uleb128 .LVU1234
	.uleb128 .LVU1234
	.uleb128 .LVU1260
	.uleb128 .LVU1304
	.uleb128 .LVU1322
	.uleb128 .LVU1322
	.uleb128 .LVU1323
	.uleb128 .LVU1323
	.uleb128 .LVU1328
	.uleb128 .LVU1352
	.uleb128 .LVU1353
	.uleb128 .LVU1358
	.uleb128 .LVU1359
.LLST73:
	.quad	.LVL188-.Ltext0
	.quad	.LVL217-.Ltext0
	.value	0x1
	.byte	0x5f
	.quad	.LVL217-.Ltext0
	.quad	.LVL220-.Ltext0
	.value	0x3
	.byte	0x76
	.sleb128 -152
	.quad	.LVL231-.Ltext0
	.quad	.LVL235-.Ltext0
	.value	0x1
	.byte	0x5f
	.quad	.LVL235-.Ltext0
	.quad	.LVL236-.Ltext0
	.value	0x1
	.byte	0x51
	.quad	.LVL236-.Ltext0
	.quad	.LVL239-.Ltext0
	.value	0x1
	.byte	0x5f
	.quad	.LVL249-.Ltext0
	.quad	.LVL250-.Ltext0
	.value	0x1
	.byte	0x5f
	.quad	.LVL252-.Ltext0
	.quad	.LVL253-.Ltext0
	.value	0x1
	.byte	0x5f
	.quad	0
	.quad	0
.LVUS74:
	.uleb128 .LVU1102
	.uleb128 .LVU1157
	.uleb128 .LVU1157
	.uleb128 .LVU1158
	.uleb128 .LVU1159
	.uleb128 .LVU1227
	.uleb128 .LVU1261
	.uleb128 .LVU1292
	.uleb128 .LVU1292
	.uleb128 .LVU1301
	.uleb128 .LVU1328
	.uleb128 .LVU1331
	.uleb128 .LVU1331
	.uleb128 .LVU1352
	.uleb128 .LVU1353
	.uleb128 .LVU1357
	.uleb128 .LVU1357
	.uleb128 .LVU1358
	.uleb128 .LVU1358
	.uleb128 .LVU1359
.LLST74:
	.quad	.LVL193-.Ltext0
	.quad	.LVL199-.Ltext0
	.value	0x1
	.byte	0x5e
	.quad	.LVL199-.Ltext0
	.quad	.LVL200-.Ltext0
	.value	0x3
	.byte	0x76
	.sleb128 -88
	.quad	.LVL200-.Ltext0
	.quad	.LVL215-.Ltext0
	.value	0x1
	.byte	0x5e
	.quad	.LVL220-.Ltext0
	.quad	.LVL225-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	.LVL225-.Ltext0
	.quad	.LVL229-.Ltext0
	.value	0x4
	.byte	0x7f
	.sleb128 112
	.byte	0x9f
	.quad	.LVL239-.Ltext0
	.quad	.LVL240-1-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	.LVL240-1-.Ltext0
	.quad	.LVL249-.Ltext0
	.value	0x4
	.byte	0x7f
	.sleb128 112
	.byte	0x9f
	.quad	.LVL250-.Ltext0
	.quad	.LVL251-1-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	.LVL251-1-.Ltext0
	.quad	.LVL252-.Ltext0
	.value	0x4
	.byte	0x7f
	.sleb128 112
	.byte	0x9f
	.quad	.LVL252-.Ltext0
	.quad	.LVL253-.Ltext0
	.value	0x1
	.byte	0x5e
	.quad	0
	.quad	0
.LVUS75:
	.uleb128 .LVU1102
	.uleb128 .LVU1157
	.uleb128 .LVU1157
	.uleb128 .LVU1158
	.uleb128 .LVU1160
	.uleb128 .LVU1227
	.uleb128 .LVU1272
	.uleb128 .LVU1301
	.uleb128 .LVU1328
	.uleb128 .LVU1352
	.uleb128 .LVU1353
	.uleb128 .LVU1358
	.uleb128 .LVU1358
	.uleb128 .LVU1359
.LLST75:
	.quad	.LVL193-.Ltext0
	.quad	.LVL199-.Ltext0
	.value	0x4
	.byte	0x7e
	.sleb128 -112
	.byte	0x9f
	.quad	.LVL199-.Ltext0
	.quad	.LVL200-.Ltext0
	.value	0x8
	.byte	0x76
	.sleb128 -88
	.byte	0x6
	.byte	0x8
	.byte	0x70
	.byte	0x1c
	.byte	0x9f
	.quad	.LVL200-.Ltext0
	.quad	.LVL215-.Ltext0
	.value	0x4
	.byte	0x7e
	.sleb128 -112
	.byte	0x9f
	.quad	.LVL221-.Ltext0
	.quad	.LVL229-.Ltext0
	.value	0x1
	.byte	0x5f
	.quad	.LVL239-.Ltext0
	.quad	.LVL249-.Ltext0
	.value	0x1
	.byte	0x5f
	.quad	.LVL250-.Ltext0
	.quad	.LVL252-.Ltext0
	.value	0x1
	.byte	0x5f
	.quad	.LVL252-.Ltext0
	.quad	.LVL253-.Ltext0
	.value	0x4
	.byte	0x7e
	.sleb128 -112
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS76:
	.uleb128 .LVU1102
	.uleb128 .LVU1141
	.uleb128 .LVU1162
	.uleb128 .LVU1221
	.uleb128 .LVU1221
	.uleb128 .LVU1226
	.uleb128 .LVU1226
	.uleb128 .LVU1227
	.uleb128 .LVU1274
	.uleb128 .LVU1301
	.uleb128 .LVU1328
	.uleb128 .LVU1352
	.uleb128 .LVU1353
	.uleb128 .LVU1358
	.uleb128 .LVU1358
	.uleb128 .LVU1359
.LLST76:
	.quad	.LVL193-.Ltext0
	.quad	.LVL198-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	.LVL201-.Ltext0
	.quad	.LVL211-1-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	.LVL211-.Ltext0
	.quad	.LVL214-1-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	.LVL214-1-.Ltext0
	.quad	.LVL215-.Ltext0
	.value	0x3
	.byte	0x76
	.sleb128 -160
	.quad	.LVL222-.Ltext0
	.quad	.LVL229-.Ltext0
	.value	0x1
	.byte	0x5e
	.quad	.LVL239-.Ltext0
	.quad	.LVL249-.Ltext0
	.value	0x1
	.byte	0x5e
	.quad	.LVL250-.Ltext0
	.quad	.LVL252-.Ltext0
	.value	0x1
	.byte	0x5e
	.quad	.LVL252-.Ltext0
	.quad	.LVL253-1-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	0
	.quad	0
.LVUS82:
	.uleb128 .LVU1085
	.uleb128 .LVU1102
.LLST82:
	.quad	.LVL192-.Ltext0
	.quad	.LVL193-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	0
	.quad	0
.LVUS97:
	.uleb128 .LVU1242
	.uleb128 .LVU1259
.LLST97:
	.quad	.LVL218-.Ltext0
	.quad	.LVL219-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	0
	.quad	0
.LVUS77:
	.uleb128 .LVU1058
	.uleb128 .LVU1301
	.uleb128 .LVU1304
	.uleb128 .LVU1359
.LLST77:
	.quad	.LVL186-.Ltext0
	.quad	.LVL229-.Ltext0
	.value	0x6
	.byte	0xf2
	.long	.Ldebug_info0+7735
	.sleb128 0
	.quad	.LVL231-.Ltext0
	.quad	.LVL253-.Ltext0
	.value	0x6
	.byte	0xf2
	.long	.Ldebug_info0+7735
	.sleb128 0
	.quad	0
	.quad	0
.LVUS78:
	.uleb128 .LVU1057
	.uleb128 .LVU1067
.LLST78:
	.quad	.LVL186-.Ltext0
	.quad	.LVL188-.Ltext0
	.value	0x3
	.byte	0x9
	.byte	0xff
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS79:
	.uleb128 .LVU1059
	.uleb128 .LVU1067
.LLST79:
	.quad	.LVL186-.Ltext0
	.quad	.LVL188-.Ltext0
	.value	0x1
	.byte	0x54
	.quad	0
	.quad	0
.LVUS80:
	.uleb128 .LVU1060
	.uleb128 .LVU1063
	.uleb128 .LVU1063
	.uleb128 .LVU1066
	.uleb128 .LVU1066
	.uleb128 .LVU1067
.LLST80:
	.quad	.LVL186-.Ltext0
	.quad	.LVL186-.Ltext0
	.value	0x2
	.byte	0x30
	.byte	0x9f
	.quad	.LVL186-.Ltext0
	.quad	.LVL187-.Ltext0
	.value	0x1
	.byte	0x54
	.quad	.LVL187-.Ltext0
	.quad	.LVL188-.Ltext0
	.value	0x1
	.byte	0x5f
	.quad	0
	.quad	0
.LVUS81:
	.uleb128 .LVU1067
	.uleb128 .LVU1072
	.uleb128 .LVU1072
	.uleb128 .LVU1074
	.uleb128 .LVU1074
	.uleb128 .LVU1076
	.uleb128 .LVU1317
	.uleb128 .LVU1322
	.uleb128 .LVU1322
	.uleb128 .LVU1324
	.uleb128 .LVU1325
	.uleb128 .LVU1328
	.uleb128 .LVU1352
	.uleb128 .LVU1353
.LLST81:
	.quad	.LVL188-.Ltext0
	.quad	.LVL189-.Ltext0
	.value	0x1
	.byte	0x5f
	.quad	.LVL189-.Ltext0
	.quad	.LVL190-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	.LVL190-.Ltext0
	.quad	.LVL191-.Ltext0
	.value	0x3
	.byte	0x76
	.sleb128 -152
	.quad	.LVL234-.Ltext0
	.quad	.LVL235-.Ltext0
	.value	0x1
	.byte	0x5f
	.quad	.LVL235-.Ltext0
	.quad	.LVL237-.Ltext0
	.value	0x1
	.byte	0x51
	.quad	.LVL238-.Ltext0
	.quad	.LVL239-.Ltext0
	.value	0x1
	.byte	0x51
	.quad	.LVL249-.Ltext0
	.quad	.LVL250-.Ltext0
	.value	0x1
	.byte	0x51
	.quad	0
	.quad	0
.LVUS83:
	.uleb128 .LVU1102
	.uleb128 .LVU1141
	.uleb128 .LVU1186
	.uleb128 .LVU1227
.LLST83:
	.quad	.LVL193-.Ltext0
	.quad	.LVL198-.Ltext0
	.value	0x4
	.byte	0x7e
	.sleb128 -112
	.byte	0x9f
	.quad	.LVL202-.Ltext0
	.quad	.LVL215-.Ltext0
	.value	0x4
	.byte	0x7e
	.sleb128 -112
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS84:
	.uleb128 .LVU1104
	.uleb128 .LVU1141
	.uleb128 .LVU1217
	.uleb128 .LVU1221
	.uleb128 .LVU1225
	.uleb128 .LVU1226
.LLST84:
	.quad	.LVL194-.Ltext0
	.quad	.LVL198-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	.LVL209-.Ltext0
	.quad	.LVL211-.Ltext0
	.value	0x2
	.byte	0x30
	.byte	0x9f
	.quad	.LVL213-.Ltext0
	.quad	.LVL214-1-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	0
	.quad	0
.LVUS85:
	.uleb128 .LVU1102
	.uleb128 .LVU1104
	.uleb128 .LVU1193
	.uleb128 .LVU1217
	.uleb128 .LVU1221
	.uleb128 .LVU1225
.LLST85:
	.quad	.LVL193-.Ltext0
	.quad	.LVL194-.Ltext0
	.value	0x1
	.byte	0x59
	.quad	.LVL204-.Ltext0
	.quad	.LVL209-.Ltext0
	.value	0x1
	.byte	0x59
	.quad	.LVL211-.Ltext0
	.quad	.LVL213-.Ltext0
	.value	0x1
	.byte	0x59
	.quad	0
	.quad	0
.LVUS86:
	.uleb128 .LVU1102
	.uleb128 .LVU1104
	.uleb128 .LVU1192
	.uleb128 .LVU1217
	.uleb128 .LVU1221
	.uleb128 .LVU1225
.LLST86:
	.quad	.LVL193-.Ltext0
	.quad	.LVL194-.Ltext0
	.value	0x1
	.byte	0x54
	.quad	.LVL203-.Ltext0
	.quad	.LVL209-.Ltext0
	.value	0x1
	.byte	0x54
	.quad	.LVL211-.Ltext0
	.quad	.LVL213-.Ltext0
	.value	0x1
	.byte	0x54
	.quad	0
	.quad	0
.LVUS87:
	.uleb128 .LVU1102
	.uleb128 .LVU1128
	.uleb128 .LVU1128
	.uleb128 .LVU1130
	.uleb128 .LVU1199
	.uleb128 .LVU1220
	.uleb128 .LVU1220
	.uleb128 .LVU1221
	.uleb128 .LVU1221
	.uleb128 .LVU1225
.LLST87:
	.quad	.LVL193-.Ltext0
	.quad	.LVL195-.Ltext0
	.value	0x4
	.byte	0x74
	.sleb128 832
	.byte	0x9f
	.quad	.LVL195-.Ltext0
	.quad	.LVL196-.Ltext0
	.value	0x8
	.byte	0x7e
	.sleb128 -104
	.byte	0x6
	.byte	0x23
	.uleb128 0x340
	.byte	0x9f
	.quad	.LVL205-.Ltext0
	.quad	.LVL210-.Ltext0
	.value	0x4
	.byte	0x74
	.sleb128 832
	.byte	0x9f
	.quad	.LVL210-.Ltext0
	.quad	.LVL211-1-.Ltext0
	.value	0x8
	.byte	0x7e
	.sleb128 -104
	.byte	0x6
	.byte	0x23
	.uleb128 0x340
	.byte	0x9f
	.quad	.LVL211-.Ltext0
	.quad	.LVL213-.Ltext0
	.value	0x4
	.byte	0x74
	.sleb128 832
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS88:
	.uleb128 .LVU1102
	.uleb128 .LVU1104
	.uleb128 .LVU1198
	.uleb128 .LVU1217
	.uleb128 .LVU1221
	.uleb128 .LVU1225
.LLST88:
	.quad	.LVL193-.Ltext0
	.quad	.LVL194-.Ltext0
	.value	0x6
	.byte	0xf2
	.long	.Ldebug_info0+8226
	.sleb128 0
	.quad	.LVL205-.Ltext0
	.quad	.LVL209-.Ltext0
	.value	0x6
	.byte	0xf2
	.long	.Ldebug_info0+8226
	.sleb128 0
	.quad	.LVL211-.Ltext0
	.quad	.LVL213-.Ltext0
	.value	0x6
	.byte	0xf2
	.long	.Ldebug_info0+8226
	.sleb128 0
	.quad	0
	.quad	0
.LVUS89:
	.uleb128 .LVU1102
	.uleb128 .LVU1104
	.uleb128 .LVU1200
	.uleb128 .LVU1217
	.uleb128 .LVU1221
	.uleb128 .LVU1225
.LLST89:
	.quad	.LVL193-.Ltext0
	.quad	.LVL194-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	.LVL205-.Ltext0
	.quad	.LVL209-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	.LVL211-.Ltext0
	.quad	.LVL213-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	0
	.quad	0
.LVUS90:
	.uleb128 .LVU1207
	.uleb128 .LVU1217
.LLST90:
	.quad	.LVL207-.Ltext0
	.quad	.LVL209-.Ltext0
	.value	0x3
	.byte	0x9
	.byte	0xff
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS91:
	.uleb128 .LVU1102
	.uleb128 .LVU1141
	.uleb128 .LVU1205
	.uleb128 .LVU1217
	.uleb128 .LVU1221
	.uleb128 .LVU1227
.LLST91:
	.quad	.LVL193-.Ltext0
	.quad	.LVL198-.Ltext0
	.value	0x6
	.byte	0xf2
	.long	.Ldebug_info0+8226
	.sleb128 0
	.quad	.LVL206-.Ltext0
	.quad	.LVL209-.Ltext0
	.value	0x6
	.byte	0xf2
	.long	.Ldebug_info0+8226
	.sleb128 0
	.quad	.LVL211-.Ltext0
	.quad	.LVL215-.Ltext0
	.value	0x6
	.byte	0xf2
	.long	.Ldebug_info0+8226
	.sleb128 0
	.quad	0
	.quad	0
.LVUS92:
	.uleb128 .LVU1102
	.uleb128 .LVU1141
	.uleb128 .LVU1205
	.uleb128 .LVU1211
	.uleb128 .LVU1221
	.uleb128 .LVU1224
	.uleb128 .LVU1225
	.uleb128 .LVU1226
.LLST92:
	.quad	.LVL193-.Ltext0
	.quad	.LVL198-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	.LVL206-.Ltext0
	.quad	.LVL208-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	.LVL211-.Ltext0
	.quad	.LVL212-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	.LVL213-.Ltext0
	.quad	.LVL214-1-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	0
	.quad	0
.LVUS93:
	.uleb128 .LVU1136
	.uleb128 .LVU1141
	.uleb128 .LVU1225
	.uleb128 .LVU1226
.LLST93:
	.quad	.LVL197-.Ltext0
	.quad	.LVL198-.Ltext0
	.value	0x3
	.byte	0x7e
	.sleb128 -104
	.quad	.LVL213-.Ltext0
	.quad	.LVL214-1-.Ltext0
	.value	0x3
	.byte	0x7e
	.sleb128 -104
	.quad	0
	.quad	0
.LVUS94:
	.uleb128 .LVU1136
	.uleb128 .LVU1141
	.uleb128 .LVU1225
	.uleb128 .LVU1226
.LLST94:
	.quad	.LVL197-.Ltext0
	.quad	.LVL198-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	.LVL213-.Ltext0
	.quad	.LVL214-1-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	0
	.quad	0
.LVUS95:
	.uleb128 .LVU1231
	.uleb128 .LVU1234
	.uleb128 .LVU1314
	.uleb128 .LVU1317
.LLST95:
	.quad	.LVL216-.Ltext0
	.quad	.LVL217-.Ltext0
	.value	0x3
	.byte	0x76
	.sleb128 -168
	.quad	.LVL232-.Ltext0
	.quad	.LVL234-.Ltext0
	.value	0x3
	.byte	0x91
	.sleb128 -184
	.quad	0
	.quad	0
.LVUS96:
	.uleb128 .LVU1231
	.uleb128 .LVU1234
	.uleb128 .LVU1314
	.uleb128 .LVU1317
.LLST96:
	.quad	.LVL216-.Ltext0
	.quad	.LVL217-.Ltext0
	.value	0x1
	.byte	0x5f
	.quad	.LVL232-.Ltext0
	.quad	.LVL234-.Ltext0
	.value	0x1
	.byte	0x5f
	.quad	0
	.quad	0
.LVUS98:
	.uleb128 .LVU1279
	.uleb128 .LVU1294
	.uleb128 .LVU1328
	.uleb128 .LVU1349
.LLST98:
	.quad	.LVL223-.Ltext0
	.quad	.LVL227-.Ltext0
	.value	0x2
	.byte	0x30
	.byte	0x9f
	.quad	.LVL239-.Ltext0
	.quad	.LVL247-.Ltext0
	.value	0x2
	.byte	0x30
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS99:
	.uleb128 .LVU1278
	.uleb128 .LVU1294
	.uleb128 .LVU1328
	.uleb128 .LVU1349
.LLST99:
	.quad	.LVL223-.Ltext0
	.quad	.LVL227-.Ltext0
	.value	0x1
	.byte	0x5e
	.quad	.LVL239-.Ltext0
	.quad	.LVL247-.Ltext0
	.value	0x1
	.byte	0x5e
	.quad	0
	.quad	0
.LVUS100:
	.uleb128 .LVU1278
	.uleb128 .LVU1294
	.uleb128 .LVU1328
	.uleb128 .LVU1349
.LLST100:
	.quad	.LVL223-.Ltext0
	.quad	.LVL227-.Ltext0
	.value	0x1
	.byte	0x5d
	.quad	.LVL239-.Ltext0
	.quad	.LVL247-.Ltext0
	.value	0x1
	.byte	0x5d
	.quad	0
	.quad	0
.LVUS101:
	.uleb128 .LVU1278
	.uleb128 .LVU1294
	.uleb128 .LVU1328
	.uleb128 .LVU1349
.LLST101:
	.quad	.LVL223-.Ltext0
	.quad	.LVL227-.Ltext0
	.value	0x1
	.byte	0x5f
	.quad	.LVL239-.Ltext0
	.quad	.LVL247-.Ltext0
	.value	0x1
	.byte	0x5f
	.quad	0
	.quad	0
.LVUS102:
	.uleb128 .LVU1339
	.uleb128 .LVU1341
	.uleb128 .LVU1344
	.uleb128 .LVU1349
.LLST102:
	.quad	.LVL243-.Ltext0
	.quad	.LVL244-.Ltext0
	.value	0x2
	.byte	0x30
	.byte	0x9f
	.quad	.LVL246-.Ltext0
	.quad	.LVL247-.Ltext0
	.value	0x4
	.byte	0x7c
	.sleb128 0
	.byte	0x1f
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS103:
	.uleb128 .LVU1288
	.uleb128 .LVU1292
	.uleb128 .LVU1328
	.uleb128 .LVU1339
	.uleb128 .LVU1341
	.uleb128 .LVU1344
.LLST103:
	.quad	.LVL224-.Ltext0
	.quad	.LVL225-.Ltext0
	.value	0x1
	.byte	0x5c
	.quad	.LVL239-.Ltext0
	.quad	.LVL243-.Ltext0
	.value	0x1
	.byte	0x5c
	.quad	.LVL244-.Ltext0
	.quad	.LVL246-.Ltext0
	.value	0x1
	.byte	0x5c
	.quad	0
	.quad	0
.LVUS104:
	.uleb128 .LVU1332
	.uleb128 .LVU1337
	.uleb128 .LVU1341
	.uleb128 .LVU1343
.LLST104:
	.quad	.LVL241-.Ltext0
	.quad	.LVL242-1-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	.LVL244-.Ltext0
	.quad	.LVL245-1-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	0
	.quad	0
.LVUS0:
	.uleb128 0
	.uleb128 .LVU20
	.uleb128 .LVU20
	.uleb128 .LVU59
	.uleb128 .LVU59
	.uleb128 .LVU64
	.uleb128 .LVU64
	.uleb128 .LVU84
	.uleb128 .LVU84
	.uleb128 .LVU86
	.uleb128 .LVU86
	.uleb128 .LVU86
	.uleb128 .LVU86
	.uleb128 .LVU247
	.uleb128 .LVU247
	.uleb128 .LVU253
	.uleb128 .LVU253
	.uleb128 0
.LLST0:
	.quad	.LVL0-.Ltext0
	.quad	.LVL4-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	.LVL4-.Ltext0
	.quad	.LVL9-.Ltext0
	.value	0x1
	.byte	0x5c
	.quad	.LVL9-.Ltext0
	.quad	.LVL11-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	.LVL11-.Ltext0
	.quad	.LVL18-.Ltext0
	.value	0x1
	.byte	0x5c
	.quad	.LVL18-.Ltext0
	.quad	.LVL19-1-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	.LVL19-1-.Ltext0
	.quad	.LVL19-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.byte	0x9f
	.quad	.LVL19-.Ltext0
	.quad	.LVL38-.Ltext0
	.value	0x1
	.byte	0x5c
	.quad	.LVL38-.Ltext0
	.quad	.LVL40-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	.LVL40-.Ltext0
	.quad	.LFE118-.Ltext0
	.value	0x1
	.byte	0x5c
	.quad	0
	.quad	0
.LVUS1:
	.uleb128 0
	.uleb128 .LVU79
	.uleb128 .LVU79
	.uleb128 .LVU86
	.uleb128 .LVU86
	.uleb128 0
.LLST1:
	.quad	.LVL0-.Ltext0
	.quad	.LVL16-.Ltext0
	.value	0x1
	.byte	0x54
	.quad	.LVL16-.Ltext0
	.quad	.LVL19-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x54
	.byte	0x9f
	.quad	.LVL19-.Ltext0
	.quad	.LFE118-.Ltext0
	.value	0x1
	.byte	0x54
	.quad	0
	.quad	0
.LVUS2:
	.uleb128 .LVU2
	.uleb128 .LVU11
	.uleb128 .LVU11
	.uleb128 .LVU13
	.uleb128 .LVU13
	.uleb128 .LVU52
	.uleb128 .LVU59
	.uleb128 .LVU64
	.uleb128 .LVU64
	.uleb128 .LVU74
	.uleb128 .LVU247
	.uleb128 .LVU253
	.uleb128 .LVU253
	.uleb128 .LVU259
	.uleb128 .LVU261
	.uleb128 .LVU265
	.uleb128 .LVU268
	.uleb128 .LVU271
	.uleb128 .LVU271
	.uleb128 .LVU275
	.uleb128 .LVU278
	.uleb128 .LVU281
.LLST2:
	.quad	.LVL0-.Ltext0
	.quad	.LVL1-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	.LVL1-.Ltext0
	.quad	.LVL2-.Ltext0
	.value	0x1
	.byte	0x52
	.quad	.LVL2-.Ltext0
	.quad	.LVL7-.Ltext0
	.value	0x1
	.byte	0x51
	.quad	.LVL9-.Ltext0
	.quad	.LVL11-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	.LVL11-.Ltext0
	.quad	.LVL14-.Ltext0
	.value	0x1
	.byte	0x5c
	.quad	.LVL38-.Ltext0
	.quad	.LVL40-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	.LVL40-.Ltext0
	.quad	.LVL42-.Ltext0
	.value	0x1
	.byte	0x5c
	.quad	.LVL43-.Ltext0
	.quad	.LVL45-.Ltext0
	.value	0x1
	.byte	0x51
	.quad	.LVL46-.Ltext0
	.quad	.LVL47-.Ltext0
	.value	0x1
	.byte	0x5c
	.quad	.LVL47-.Ltext0
	.quad	.LVL49-.Ltext0
	.value	0x1
	.byte	0x51
	.quad	.LVL50-.Ltext0
	.quad	.LVL51-.Ltext0
	.value	0x1
	.byte	0x51
	.quad	0
	.quad	0
.LVUS3:
	.uleb128 .LVU2
	.uleb128 .LVU77
	.uleb128 .LVU86
	.uleb128 0
.LLST3:
	.quad	.LVL0-.Ltext0
	.quad	.LVL15-.Ltext0
	.value	0x4
	.byte	0x74
	.sleb128 832
	.byte	0x9f
	.quad	.LVL19-.Ltext0
	.quad	.LFE118-.Ltext0
	.value	0x4
	.byte	0x74
	.sleb128 832
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS4:
	.uleb128 .LVU17
	.uleb128 .LVU59
	.uleb128 .LVU59
	.uleb128 .LVU66
	.uleb128 .LVU66
	.uleb128 .LVU70
	.uleb128 .LVU74
	.uleb128 .LVU76
	.uleb128 .LVU249
	.uleb128 .LVU253
	.uleb128 .LVU253
	.uleb128 .LVU257
	.uleb128 .LVU261
	.uleb128 .LVU265
	.uleb128 .LVU271
	.uleb128 .LVU275
	.uleb128 .LVU278
	.uleb128 .LVU281
.LLST4:
	.quad	.LVL3-.Ltext0
	.quad	.LVL9-.Ltext0
	.value	0x1
	.byte	0x52
	.quad	.LVL9-.Ltext0
	.quad	.LVL12-.Ltext0
	.value	0x1
	.byte	0x51
	.quad	.LVL12-.Ltext0
	.quad	.LVL13-.Ltext0
	.value	0x1
	.byte	0x52
	.quad	.LVL14-.Ltext0
	.quad	.LVL15-.Ltext0
	.value	0x1
	.byte	0x52
	.quad	.LVL38-.Ltext0
	.quad	.LVL40-.Ltext0
	.value	0x2
	.byte	0x75
	.sleb128 8
	.quad	.LVL40-.Ltext0
	.quad	.LVL41-.Ltext0
	.value	0x2
	.byte	0x7c
	.sleb128 8
	.quad	.LVL43-.Ltext0
	.quad	.LVL45-.Ltext0
	.value	0x1
	.byte	0x52
	.quad	.LVL47-.Ltext0
	.quad	.LVL49-.Ltext0
	.value	0x1
	.byte	0x52
	.quad	.LVL50-.Ltext0
	.quad	.LVL51-.Ltext0
	.value	0x1
	.byte	0x52
	.quad	0
	.quad	0
.LVUS5:
	.uleb128 .LVU18
	.uleb128 .LVU59
	.uleb128 .LVU62
	.uleb128 .LVU76
	.uleb128 .LVU251
	.uleb128 .LVU259
	.uleb128 .LVU261
	.uleb128 .LVU265
	.uleb128 .LVU268
	.uleb128 .LVU275
	.uleb128 .LVU278
	.uleb128 .LVU281
.LLST5:
	.quad	.LVL3-.Ltext0
	.quad	.LVL9-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	.LVL10-.Ltext0
	.quad	.LVL15-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	.LVL39-.Ltext0
	.quad	.LVL42-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	.LVL43-.Ltext0
	.quad	.LVL45-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	.LVL46-.Ltext0
	.quad	.LVL49-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	.LVL50-.Ltext0
	.quad	.LVL51-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	0
	.quad	0
.LVUS6:
	.uleb128 .LVU4
	.uleb128 .LVU20
	.uleb128 .LVU20
	.uleb128 .LVU59
	.uleb128 .LVU59
	.uleb128 .LVU64
	.uleb128 .LVU64
	.uleb128 .LVU84
	.uleb128 .LVU84
	.uleb128 .LVU86
	.uleb128 .LVU86
	.uleb128 .LVU86
	.uleb128 .LVU86
	.uleb128 .LVU247
	.uleb128 .LVU247
	.uleb128 .LVU253
	.uleb128 .LVU253
	.uleb128 0
.LLST6:
	.quad	.LVL0-.Ltext0
	.quad	.LVL4-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	.LVL4-.Ltext0
	.quad	.LVL9-.Ltext0
	.value	0x1
	.byte	0x5c
	.quad	.LVL9-.Ltext0
	.quad	.LVL11-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	.LVL11-.Ltext0
	.quad	.LVL18-.Ltext0
	.value	0x1
	.byte	0x5c
	.quad	.LVL18-.Ltext0
	.quad	.LVL19-1-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	.LVL19-1-.Ltext0
	.quad	.LVL19-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.byte	0x9f
	.quad	.LVL19-.Ltext0
	.quad	.LVL38-.Ltext0
	.value	0x1
	.byte	0x5c
	.quad	.LVL38-.Ltext0
	.quad	.LVL40-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	.LVL40-.Ltext0
	.quad	.LFE118-.Ltext0
	.value	0x1
	.byte	0x5c
	.quad	0
	.quad	0
.LVUS7:
	.uleb128 .LVU20
	.uleb128 .LVU59
	.uleb128 .LVU64
	.uleb128 .LVU76
	.uleb128 .LVU253
	.uleb128 .LVU259
	.uleb128 .LVU261
	.uleb128 .LVU265
	.uleb128 .LVU268
	.uleb128 .LVU275
	.uleb128 .LVU278
	.uleb128 .LVU281
.LLST7:
	.quad	.LVL4-.Ltext0
	.quad	.LVL9-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	.LVL11-.Ltext0
	.quad	.LVL15-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	.LVL40-.Ltext0
	.quad	.LVL42-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	.LVL43-.Ltext0
	.quad	.LVL45-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	.LVL46-.Ltext0
	.quad	.LVL49-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	.LVL50-.Ltext0
	.quad	.LVL51-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	0
	.quad	0
.LVUS8:
	.uleb128 .LVU13
	.uleb128 .LVU17
	.uleb128 .LVU17
	.uleb128 .LVU31
	.uleb128 .LVU52
	.uleb128 .LVU59
	.uleb128 .LVU261
	.uleb128 .LVU264
	.uleb128 .LVU273
	.uleb128 .LVU275
.LLST8:
	.quad	.LVL2-.Ltext0
	.quad	.LVL3-.Ltext0
	.value	0x1
	.byte	0x52
	.quad	.LVL3-.Ltext0
	.quad	.LVL5-.Ltext0
	.value	0x2
	.byte	0x71
	.sleb128 0
	.quad	.LVL7-.Ltext0
	.quad	.LVL9-.Ltext0
	.value	0x1
	.byte	0x51
	.quad	.LVL43-.Ltext0
	.quad	.LVL44-.Ltext0
	.value	0x2
	.byte	0x71
	.sleb128 0
	.quad	.LVL48-.Ltext0
	.quad	.LVL49-.Ltext0
	.value	0x2
	.byte	0x71
	.sleb128 0
	.quad	0
	.quad	0
.LVUS9:
	.uleb128 .LVU86
	.uleb128 .LVU89
	.uleb128 .LVU99
	.uleb128 .LVU102
	.uleb128 .LVU102
	.uleb128 .LVU113
	.uleb128 .LVU160
	.uleb128 .LVU165
	.uleb128 .LVU165
	.uleb128 .LVU168
	.uleb128 .LVU281
	.uleb128 .LVU282
	.uleb128 .LVU327
	.uleb128 .LVU331
.LLST9:
	.quad	.LVL19-.Ltext0
	.quad	.LVL20-.Ltext0
	.value	0x1
	.byte	0x52
	.quad	.LVL21-.Ltext0
	.quad	.LVL23-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	.LVL23-.Ltext0
	.quad	.LVL25-.Ltext0
	.value	0x1
	.byte	0x52
	.quad	.LVL29-.Ltext0
	.quad	.LVL30-.Ltext0
	.value	0x1
	.byte	0x51
	.quad	.LVL30-.Ltext0
	.quad	.LVL31-.Ltext0
	.value	0x1
	.byte	0x52
	.quad	.LVL51-.Ltext0
	.quad	.LVL52-.Ltext0
	.value	0x1
	.byte	0x52
	.quad	.LVL55-.Ltext0
	.quad	.LVL56-.Ltext0
	.value	0x1
	.byte	0x51
	.quad	0
	.quad	0
.LVUS10:
	.uleb128 .LVU86
	.uleb128 .LVU101
	.uleb128 .LVU101
	.uleb128 .LVU102
	.uleb128 .LVU102
	.uleb128 .LVU247
	.uleb128 .LVU259
	.uleb128 .LVU261
	.uleb128 .LVU265
	.uleb128 .LVU268
	.uleb128 .LVU275
	.uleb128 .LVU278
	.uleb128 .LVU281
	.uleb128 0
.LLST10:
	.quad	.LVL19-.Ltext0
	.quad	.LVL22-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	.LVL22-.Ltext0
	.quad	.LVL23-.Ltext0
	.value	0x1
	.byte	0x51
	.quad	.LVL23-.Ltext0
	.quad	.LVL38-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	.LVL42-.Ltext0
	.quad	.LVL43-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	.LVL45-.Ltext0
	.quad	.LVL46-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	.LVL49-.Ltext0
	.quad	.LVL50-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	.LVL51-.Ltext0
	.quad	.LFE118-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	0
	.quad	0
.LVUS11:
	.uleb128 .LVU87
	.uleb128 .LVU101
	.uleb128 .LVU111
	.uleb128 .LVU130
	.uleb128 .LVU130
	.uleb128 .LVU132
	.uleb128 .LVU165
	.uleb128 .LVU204
	.uleb128 .LVU204
	.uleb128 .LVU205
	.uleb128 .LVU205
	.uleb128 .LVU206
	.uleb128 .LVU206
	.uleb128 .LVU243
	.uleb128 .LVU243
	.uleb128 .LVU244
	.uleb128 .LVU244
	.uleb128 .LVU245
	.uleb128 .LVU245
	.uleb128 .LVU247
	.uleb128 .LVU259
	.uleb128 .LVU261
	.uleb128 .LVU265
	.uleb128 .LVU268
	.uleb128 .LVU275
	.uleb128 .LVU278
	.uleb128 .LVU282
	.uleb128 .LVU295
	.uleb128 .LVU295
	.uleb128 .LVU298
	.uleb128 .LVU331
	.uleb128 .LVU380
	.uleb128 .LVU383
	.uleb128 .LVU433
	.uleb128 .LVU435
	.uleb128 .LVU437
	.uleb128 .LVU442
	.uleb128 0
.LLST11:
	.quad	.LVL19-.Ltext0
	.quad	.LVL22-.Ltext0
	.value	0x1
	.byte	0x51
	.quad	.LVL24-.Ltext0
	.quad	.LVL27-.Ltext0
	.value	0x1
	.byte	0x51
	.quad	.LVL27-.Ltext0
	.quad	.LVL28-.Ltext0
	.value	0x2
	.byte	0x70
	.sleb128 8
	.quad	.LVL30-.Ltext0
	.quad	.LVL32-.Ltext0
	.value	0x1
	.byte	0x51
	.quad	.LVL32-.Ltext0
	.quad	.LVL33-.Ltext0
	.value	0x3
	.byte	0x71
	.sleb128 8
	.byte	0x6
	.quad	.LVL33-.Ltext0
	.quad	.LVL34-.Ltext0
	.value	0x2
	.byte	0x70
	.sleb128 0
	.quad	.LVL34-.Ltext0
	.quad	.LVL35-.Ltext0
	.value	0x1
	.byte	0x51
	.quad	.LVL35-.Ltext0
	.quad	.LVL36-.Ltext0
	.value	0x5
	.byte	0x71
	.sleb128 0
	.byte	0x6
	.byte	0x23
	.uleb128 0x8
	.quad	.LVL36-.Ltext0
	.quad	.LVL37-.Ltext0
	.value	0x2
	.byte	0x70
	.sleb128 8
	.quad	.LVL37-.Ltext0
	.quad	.LVL38-.Ltext0
	.value	0x1
	.byte	0x51
	.quad	.LVL42-.Ltext0
	.quad	.LVL43-.Ltext0
	.value	0x1
	.byte	0x51
	.quad	.LVL45-.Ltext0
	.quad	.LVL46-.Ltext0
	.value	0x1
	.byte	0x51
	.quad	.LVL49-.Ltext0
	.quad	.LVL50-.Ltext0
	.value	0x1
	.byte	0x51
	.quad	.LVL52-.Ltext0
	.quad	.LVL53-.Ltext0
	.value	0x1
	.byte	0x51
	.quad	.LVL53-.Ltext0
	.quad	.LVL54-.Ltext0
	.value	0x2
	.byte	0x70
	.sleb128 0
	.quad	.LVL56-.Ltext0
	.quad	.LVL60-.Ltext0
	.value	0x1
	.byte	0x51
	.quad	.LVL61-.Ltext0
	.quad	.LVL65-.Ltext0
	.value	0x1
	.byte	0x51
	.quad	.LVL66-.Ltext0
	.quad	.LVL67-.Ltext0
	.value	0x1
	.byte	0x51
	.quad	.LVL68-.Ltext0
	.quad	.LFE118-.Ltext0
	.value	0x1
	.byte	0x51
	.quad	0
	.quad	0
.LVUS12:
	.uleb128 .LVU331
	.uleb128 .LVU370
	.uleb128 .LVU378
	.uleb128 .LVU380
	.uleb128 .LVU435
	.uleb128 .LVU437
.LLST12:
	.quad	.LVL56-.Ltext0
	.quad	.LVL58-.Ltext0
	.value	0x1
	.byte	0x52
	.quad	.LVL59-.Ltext0
	.quad	.LVL60-.Ltext0
	.value	0x1
	.byte	0x52
	.quad	.LVL66-.Ltext0
	.quad	.LVL67-.Ltext0
	.value	0x1
	.byte	0x52
	.quad	0
	.quad	0
.LVUS13:
	.uleb128 .LVU387
	.uleb128 .LVU396
.LLST13:
	.quad	.LVL62-.Ltext0
	.quad	.LVL63-.Ltext0
	.value	0x2
	.byte	0x71
	.sleb128 0
	.quad	0
	.quad	0
.LVUS40:
	.uleb128 0
	.uleb128 .LVU633
	.uleb128 .LVU633
	.uleb128 .LVU684
	.uleb128 .LVU684
	.uleb128 .LVU688
	.uleb128 .LVU688
	.uleb128 .LVU890
	.uleb128 .LVU890
	.uleb128 .LVU894
	.uleb128 .LVU894
	.uleb128 0
.LLST40:
	.quad	.LVL105-.Ltext0
	.quad	.LVL108-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	.LVL108-.Ltext0
	.quad	.LVL115-.Ltext0
	.value	0x1
	.byte	0x5c
	.quad	.LVL115-.Ltext0
	.quad	.LVL119-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.byte	0x9f
	.quad	.LVL119-.Ltext0
	.quad	.LVL161-.Ltext0
	.value	0x1
	.byte	0x5c
	.quad	.LVL161-.Ltext0
	.quad	.LVL164-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.byte	0x9f
	.quad	.LVL164-.Ltext0
	.quad	.LFE119-.Ltext0
	.value	0x1
	.byte	0x5c
	.quad	0
	.quad	0
.LVUS41:
	.uleb128 0
	.uleb128 .LVU631
	.uleb128 .LVU631
	.uleb128 .LVU685
	.uleb128 .LVU685
	.uleb128 .LVU688
	.uleb128 .LVU688
	.uleb128 .LVU892
	.uleb128 .LVU892
	.uleb128 .LVU894
	.uleb128 .LVU894
	.uleb128 0
.LLST41:
	.quad	.LVL105-.Ltext0
	.quad	.LVL107-.Ltext0
	.value	0x1
	.byte	0x54
	.quad	.LVL107-.Ltext0
	.quad	.LVL116-.Ltext0
	.value	0x1
	.byte	0x5d
	.quad	.LVL116-.Ltext0
	.quad	.LVL119-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x54
	.byte	0x9f
	.quad	.LVL119-.Ltext0
	.quad	.LVL162-.Ltext0
	.value	0x1
	.byte	0x5d
	.quad	.LVL162-.Ltext0
	.quad	.LVL164-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x54
	.byte	0x9f
	.quad	.LVL164-.Ltext0
	.quad	.LFE119-.Ltext0
	.value	0x1
	.byte	0x5d
	.quad	0
	.quad	0
.LVUS42:
	.uleb128 0
	.uleb128 .LVU629
	.uleb128 .LVU629
	.uleb128 .LVU687
	.uleb128 .LVU687
	.uleb128 .LVU688
	.uleb128 .LVU688
	.uleb128 .LVU893
	.uleb128 .LVU893
	.uleb128 .LVU894
	.uleb128 .LVU894
	.uleb128 0
.LLST42:
	.quad	.LVL105-.Ltext0
	.quad	.LVL106-.Ltext0
	.value	0x1
	.byte	0x51
	.quad	.LVL106-.Ltext0
	.quad	.LVL118-.Ltext0
	.value	0x1
	.byte	0x5f
	.quad	.LVL118-.Ltext0
	.quad	.LVL119-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x51
	.byte	0x9f
	.quad	.LVL119-.Ltext0
	.quad	.LVL163-.Ltext0
	.value	0x1
	.byte	0x5f
	.quad	.LVL163-.Ltext0
	.quad	.LVL164-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x51
	.byte	0x9f
	.quad	.LVL164-.Ltext0
	.quad	.LFE119-.Ltext0
	.value	0x1
	.byte	0x5f
	.quad	0
	.quad	0
.LVUS43:
	.uleb128 .LVU654
	.uleb128 .LVU683
	.uleb128 .LVU692
	.uleb128 .LVU700
	.uleb128 .LVU704
	.uleb128 .LVU716
	.uleb128 .LVU716
	.uleb128 .LVU725
	.uleb128 .LVU725
	.uleb128 .LVU885
	.uleb128 .LVU894
	.uleb128 .LVU1033
	.uleb128 .LVU1033
	.uleb128 .LVU1035
	.uleb128 .LVU1035
	.uleb128 0
.LLST43:
	.quad	.LVL113-.Ltext0
	.quad	.LVL114-.Ltext0
	.value	0x1
	.byte	0x53
	.quad	.LVL121-.Ltext0
	.quad	.LVL122-.Ltext0
	.value	0x1
	.byte	0x53
	.quad	.LVL124-.Ltext0
	.quad	.LVL129-.Ltext0
	.value	0x2
	.byte	0x30
	.byte	0x9f
	.quad	.LVL129-.Ltext0
	.quad	.LVL132-1-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	.LVL132-1-.Ltext0
	.quad	.LVL159-.Ltext0
	.value	0x1
	.byte	0x53
	.quad	.LVL164-.Ltext0
	.quad	.LVL183-.Ltext0
	.value	0x1
	.byte	0x53
	.quad	.LVL183-.Ltext0
	.quad	.LVL184-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	.LVL184-.Ltext0
	.quad	.LFE119-.Ltext0
	.value	0x1
	.byte	0x53
	.quad	0
	.quad	0
.LVUS44:
	.uleb128 .LVU712
	.uleb128 .LVU714
	.uleb128 .LVU714
	.uleb128 .LVU885
	.uleb128 .LVU894
	.uleb128 0
.LLST44:
	.quad	.LVL127-.Ltext0
	.quad	.LVL128-1-.Ltext0
	.value	0x1
	.byte	0x51
	.quad	.LVL128-1-.Ltext0
	.quad	.LVL159-.Ltext0
	.value	0x2
	.byte	0x76
	.sleb128 -56
	.quad	.LVL164-.Ltext0
	.quad	.LFE119-.Ltext0
	.value	0x2
	.byte	0x76
	.sleb128 -56
	.quad	0
	.quad	0
.LVUS45:
	.uleb128 .LVU634
	.uleb128 .LVU654
	.uleb128 .LVU654
	.uleb128 .LVU686
	.uleb128 .LVU688
	.uleb128 .LVU692
	.uleb128 .LVU692
	.uleb128 .LVU700
	.uleb128 .LVU700
	.uleb128 .LVU709
	.uleb128 .LVU709
	.uleb128 .LVU885
	.uleb128 .LVU885
	.uleb128 .LVU888
	.uleb128 .LVU894
	.uleb128 0
.LLST45:
	.quad	.LVL109-.Ltext0
	.quad	.LVL113-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	.LVL113-.Ltext0
	.quad	.LVL117-.Ltext0
	.value	0x1
	.byte	0x5e
	.quad	.LVL119-.Ltext0
	.quad	.LVL121-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	.LVL121-.Ltext0
	.quad	.LVL122-.Ltext0
	.value	0x1
	.byte	0x5e
	.quad	.LVL122-.Ltext0
	.quad	.LVL126-1-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	.LVL126-1-.Ltext0
	.quad	.LVL159-.Ltext0
	.value	0x1
	.byte	0x5e
	.quad	.LVL159-.Ltext0
	.quad	.LVL160-1-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	.LVL164-.Ltext0
	.quad	.LFE119-.Ltext0
	.value	0x1
	.byte	0x5e
	.quad	0
	.quad	0
.LVUS46:
	.uleb128 .LVU625
	.uleb128 0
.LLST46:
	.quad	.LVL105-.Ltext0
	.quad	.LFE119-.Ltext0
	.value	0x6
	.byte	0xfa
	.long	0x1939
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS47:
	.uleb128 .LVU638
	.uleb128 .LVU654
	.uleb128 .LVU688
	.uleb128 .LVU692
	.uleb128 .LVU700
	.uleb128 .LVU704
.LLST47:
	.quad	.LVL110-.Ltext0
	.quad	.LVL113-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	.LVL119-.Ltext0
	.quad	.LVL121-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	.LVL122-.Ltext0
	.quad	.LVL124-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	0
	.quad	0
.LVUS48:
	.uleb128 .LVU638
	.uleb128 .LVU654
	.uleb128 .LVU688
	.uleb128 .LVU692
	.uleb128 .LVU700
	.uleb128 .LVU704
.LLST48:
	.quad	.LVL110-.Ltext0
	.quad	.LVL113-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	.LVL119-.Ltext0
	.quad	.LVL121-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	.LVL122-.Ltext0
	.quad	.LVL124-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	0
	.quad	0
.LVUS49:
	.uleb128 .LVU644
	.uleb128 .LVU654
	.uleb128 .LVU688
	.uleb128 .LVU692
	.uleb128 .LVU700
	.uleb128 .LVU708
	.uleb128 .LVU708
	.uleb128 .LVU709
.LLST49:
	.quad	.LVL111-.Ltext0
	.quad	.LVL113-.Ltext0
	.value	0x4
	.byte	0x75
	.sleb128 832
	.byte	0x9f
	.quad	.LVL119-.Ltext0
	.quad	.LVL121-.Ltext0
	.value	0x4
	.byte	0x75
	.sleb128 832
	.byte	0x9f
	.quad	.LVL122-.Ltext0
	.quad	.LVL125-.Ltext0
	.value	0x4
	.byte	0x75
	.sleb128 832
	.byte	0x9f
	.quad	.LVL125-.Ltext0
	.quad	.LVL126-1-.Ltext0
	.value	0x7
	.byte	0x7c
	.sleb128 8
	.byte	0x6
	.byte	0x23
	.uleb128 0x340
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS50:
	.uleb128 .LVU643
	.uleb128 .LVU654
	.uleb128 .LVU688
	.uleb128 .LVU692
	.uleb128 .LVU700
	.uleb128 .LVU704
.LLST50:
	.quad	.LVL111-.Ltext0
	.quad	.LVL113-.Ltext0
	.value	0x6
	.byte	0xf2
	.long	.Ldebug_info0+10414
	.sleb128 0
	.quad	.LVL119-.Ltext0
	.quad	.LVL121-.Ltext0
	.value	0x6
	.byte	0xf2
	.long	.Ldebug_info0+10414
	.sleb128 0
	.quad	.LVL122-.Ltext0
	.quad	.LVL124-.Ltext0
	.value	0x6
	.byte	0xf2
	.long	.Ldebug_info0+10414
	.sleb128 0
	.quad	0
	.quad	0
.LVUS51:
	.uleb128 .LVU645
	.uleb128 .LVU654
	.uleb128 .LVU688
	.uleb128 .LVU692
	.uleb128 .LVU700
	.uleb128 .LVU704
.LLST51:
	.quad	.LVL111-.Ltext0
	.quad	.LVL113-.Ltext0
	.value	0x1
	.byte	0x53
	.quad	.LVL119-.Ltext0
	.quad	.LVL121-.Ltext0
	.value	0x1
	.byte	0x53
	.quad	.LVL122-.Ltext0
	.quad	.LVL124-.Ltext0
	.value	0x1
	.byte	0x53
	.quad	0
	.quad	0
.LVUS52:
	.uleb128 .LVU688
	.uleb128 .LVU692
.LLST52:
	.quad	.LVL119-.Ltext0
	.quad	.LVL121-.Ltext0
	.value	0x3
	.byte	0x9
	.byte	0xff
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS53:
	.uleb128 .LVU650
	.uleb128 .LVU654
	.uleb128 .LVU688
	.uleb128 .LVU692
	.uleb128 .LVU700
	.uleb128 .LVU704
.LLST53:
	.quad	.LVL112-.Ltext0
	.quad	.LVL113-.Ltext0
	.value	0x6
	.byte	0xf2
	.long	.Ldebug_info0+10414
	.sleb128 0
	.quad	.LVL119-.Ltext0
	.quad	.LVL121-.Ltext0
	.value	0x6
	.byte	0xf2
	.long	.Ldebug_info0+10414
	.sleb128 0
	.quad	.LVL122-.Ltext0
	.quad	.LVL124-.Ltext0
	.value	0x6
	.byte	0xf2
	.long	.Ldebug_info0+10414
	.sleb128 0
	.quad	0
	.quad	0
.LVUS54:
	.uleb128 .LVU650
	.uleb128 .LVU654
	.uleb128 .LVU688
	.uleb128 .LVU691
	.uleb128 .LVU700
	.uleb128 .LVU703
.LLST54:
	.quad	.LVL112-.Ltext0
	.quad	.LVL113-.Ltext0
	.value	0x1
	.byte	0x53
	.quad	.LVL119-.Ltext0
	.quad	.LVL120-.Ltext0
	.value	0x1
	.byte	0x53
	.quad	.LVL122-.Ltext0
	.quad	.LVL123-.Ltext0
	.value	0x1
	.byte	0x53
	.quad	0
	.quad	0
.LVUS55:
	.uleb128 .LVU721
	.uleb128 .LVU725
	.uleb128 .LVU725
	.uleb128 .LVU725
.LLST55:
	.quad	.LVL130-.Ltext0
	.quad	.LVL132-1-.Ltext0
	.value	0x1
	.byte	0x51
	.quad	.LVL132-1-.Ltext0
	.quad	.LVL132-.Ltext0
	.value	0x2
	.byte	0x76
	.sleb128 -56
	.quad	0
	.quad	0
.LVUS56:
	.uleb128 .LVU721
	.uleb128 .LVU725
.LLST56:
	.quad	.LVL130-.Ltext0
	.quad	.LVL132-.Ltext0
	.value	0x1
	.byte	0x5f
	.quad	0
	.quad	0
.LVUS57:
	.uleb128 .LVU721
	.uleb128 .LVU724
	.uleb128 .LVU724
	.uleb128 .LVU725
	.uleb128 .LVU725
	.uleb128 .LVU725
.LLST57:
	.quad	.LVL130-.Ltext0
	.quad	.LVL131-.Ltext0
	.value	0x4
	.byte	0x70
	.sleb128 72
	.byte	0x9f
	.quad	.LVL131-.Ltext0
	.quad	.LVL132-1-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	.LVL132-1-.Ltext0
	.quad	.LVL132-.Ltext0
	.value	0x4
	.byte	0x73
	.sleb128 72
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS58:
	.uleb128 .LVU738
	.uleb128 .LVU885
	.uleb128 .LVU894
	.uleb128 .LVU1033
.LLST58:
	.quad	.LVL133-.Ltext0
	.quad	.LVL159-.Ltext0
	.value	0x1
	.byte	0x53
	.quad	.LVL164-.Ltext0
	.quad	.LVL183-.Ltext0
	.value	0x1
	.byte	0x53
	.quad	0
	.quad	0
.LVUS59:
	.uleb128 .LVU738
	.uleb128 .LVU885
	.uleb128 .LVU894
	.uleb128 .LVU1033
.LLST59:
	.quad	.LVL133-.Ltext0
	.quad	.LVL159-.Ltext0
	.value	0x4
	.byte	0x75
	.sleb128 832
	.byte	0x9f
	.quad	.LVL164-.Ltext0
	.quad	.LVL183-.Ltext0
	.value	0x4
	.byte	0x75
	.sleb128 832
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS60:
	.uleb128 .LVU744
	.uleb128 .LVU752
	.uleb128 .LVU752
	.uleb128 .LVU755
	.uleb128 .LVU755
	.uleb128 .LVU764
	.uleb128 .LVU764
	.uleb128 .LVU788
	.uleb128 .LVU1001
	.uleb128 .LVU1008
	.uleb128 .LVU1008
	.uleb128 .LVU1010
	.uleb128 .LVU1021
	.uleb128 .LVU1024
.LLST60:
	.quad	.LVL134-.Ltext0
	.quad	.LVL137-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	.LVL137-.Ltext0
	.quad	.LVL139-.Ltext0
	.value	0x1
	.byte	0x51
	.quad	.LVL139-.Ltext0
	.quad	.LVL141-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	.LVL141-.Ltext0
	.quad	.LVL145-.Ltext0
	.value	0x1
	.byte	0x51
	.quad	.LVL177-.Ltext0
	.quad	.LVL178-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	.LVL178-.Ltext0
	.quad	.LVL179-.Ltext0
	.value	0x3
	.byte	0x75
	.sleb128 832
	.quad	.LVL181-.Ltext0
	.quad	.LVL182-.Ltext0
	.value	0x1
	.byte	0x51
	.quad	0
	.quad	0
.LVUS61:
	.uleb128 .LVU741
	.uleb128 .LVU746
	.uleb128 .LVU746
	.uleb128 .LVU755
	.uleb128 .LVU756
	.uleb128 .LVU788
	.uleb128 .LVU1001
	.uleb128 .LVU1021
	.uleb128 .LVU1021
	.uleb128 .LVU1024
.LLST61:
	.quad	.LVL133-.Ltext0
	.quad	.LVL135-.Ltext0
	.value	0x2
	.byte	0x30
	.byte	0x9f
	.quad	.LVL135-.Ltext0
	.quad	.LVL139-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	.LVL139-.Ltext0
	.quad	.LVL145-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	.LVL177-.Ltext0
	.quad	.LVL181-.Ltext0
	.value	0x2
	.byte	0x30
	.byte	0x9f
	.quad	.LVL181-.Ltext0
	.quad	.LVL182-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	0
	.quad	0
.LVUS62:
	.uleb128 .LVU742
	.uleb128 .LVU748
	.uleb128 .LVU753
	.uleb128 .LVU755
	.uleb128 .LVU755
	.uleb128 .LVU760
	.uleb128 .LVU760
	.uleb128 .LVU765
	.uleb128 .LVU765
	.uleb128 .LVU787
	.uleb128 .LVU1001
	.uleb128 .LVU1021
	.uleb128 .LVU1021
	.uleb128 .LVU1024
.LLST62:
	.quad	.LVL133-.Ltext0
	.quad	.LVL136-.Ltext0
	.value	0x2
	.byte	0x30
	.byte	0x9f
	.quad	.LVL138-.Ltext0
	.quad	.LVL139-.Ltext0
	.value	0x1
	.byte	0x52
	.quad	.LVL139-.Ltext0
	.quad	.LVL140-.Ltext0
	.value	0x2
	.byte	0x30
	.byte	0x9f
	.quad	.LVL140-.Ltext0
	.quad	.LVL142-.Ltext0
	.value	0x3
	.byte	0x9
	.byte	0xff
	.byte	0x9f
	.quad	.LVL142-.Ltext0
	.quad	.LVL144-.Ltext0
	.value	0x1
	.byte	0x52
	.quad	.LVL177-.Ltext0
	.quad	.LVL181-.Ltext0
	.value	0x2
	.byte	0x30
	.byte	0x9f
	.quad	.LVL181-.Ltext0
	.quad	.LVL182-.Ltext0
	.value	0x1
	.byte	0x52
	.quad	0
	.quad	0
.LVUS63:
	.uleb128 .LVU746
	.uleb128 .LVU755
	.uleb128 .LVU758
	.uleb128 .LVU885
	.uleb128 .LVU894
	.uleb128 .LVU947
	.uleb128 .LVU950
	.uleb128 .LVU1001
	.uleb128 .LVU1021
	.uleb128 .LVU1033
.LLST63:
	.quad	.LVL135-.Ltext0
	.quad	.LVL139-.Ltext0
	.value	0x1
	.byte	0x53
	.quad	.LVL139-.Ltext0
	.quad	.LVL159-.Ltext0
	.value	0x1
	.byte	0x53
	.quad	.LVL164-.Ltext0
	.quad	.LVL172-.Ltext0
	.value	0x1
	.byte	0x53
	.quad	.LVL173-.Ltext0
	.quad	.LVL177-.Ltext0
	.value	0x1
	.byte	0x53
	.quad	.LVL181-.Ltext0
	.quad	.LVL183-.Ltext0
	.value	0x1
	.byte	0x53
	.quad	0
	.quad	0
.LVUS64:
	.uleb128 .LVU746
	.uleb128 .LVU755
	.uleb128 .LVU758
	.uleb128 .LVU788
	.uleb128 .LVU1021
	.uleb128 .LVU1024
.LLST64:
	.quad	.LVL135-.Ltext0
	.quad	.LVL139-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	.LVL139-.Ltext0
	.quad	.LVL145-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	.LVL181-.Ltext0
	.quad	.LVL182-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	0
	.quad	0
.LVUS65:
	.uleb128 .LVU781
	.uleb128 .LVU788
	.uleb128 .LVU788
	.uleb128 .LVU885
	.uleb128 .LVU894
	.uleb128 .LVU929
	.uleb128 .LVU929
	.uleb128 .LVU930
	.uleb128 .LVU930
	.uleb128 .LVU937
	.uleb128 .LVU942
	.uleb128 .LVU947
	.uleb128 .LVU950
	.uleb128 .LVU985
	.uleb128 .LVU985
	.uleb128 .LVU986
	.uleb128 .LVU986
	.uleb128 .LVU1001
	.uleb128 .LVU1015
	.uleb128 .LVU1021
	.uleb128 .LVU1024
	.uleb128 .LVU1033
.LLST65:
	.quad	.LVL143-.Ltext0
	.quad	.LVL145-.Ltext0
	.value	0x1
	.byte	0x53
	.quad	.LVL145-.Ltext0
	.quad	.LVL159-.Ltext0
	.value	0x1
	.byte	0x52
	.quad	.LVL164-.Ltext0
	.quad	.LVL165-.Ltext0
	.value	0x1
	.byte	0x52
	.quad	.LVL165-.Ltext0
	.quad	.LVL166-.Ltext0
	.value	0x2
	.byte	0x74
	.sleb128 8
	.quad	.LVL166-.Ltext0
	.quad	.LVL168-.Ltext0
	.value	0x1
	.byte	0x52
	.quad	.LVL169-.Ltext0
	.quad	.LVL172-.Ltext0
	.value	0x1
	.byte	0x52
	.quad	.LVL173-.Ltext0
	.quad	.LVL174-.Ltext0
	.value	0x1
	.byte	0x52
	.quad	.LVL174-.Ltext0
	.quad	.LVL175-.Ltext0
	.value	0x2
	.byte	0x79
	.sleb128 0
	.quad	.LVL175-.Ltext0
	.quad	.LVL177-.Ltext0
	.value	0x1
	.byte	0x52
	.quad	.LVL180-.Ltext0
	.quad	.LVL181-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	.LVL182-.Ltext0
	.quad	.LVL183-.Ltext0
	.value	0x1
	.byte	0x52
	.quad	0
	.quad	0
.LVUS66:
	.uleb128 .LVU786
	.uleb128 .LVU799
	.uleb128 .LVU834
	.uleb128 .LVU853
	.uleb128 .LVU894
	.uleb128 .LVU928
	.uleb128 .LVU928
	.uleb128 .LVU930
	.uleb128 .LVU932
	.uleb128 .LVU944
	.uleb128 .LVU946
	.uleb128 .LVU947
	.uleb128 .LVU950
	.uleb128 .LVU984
	.uleb128 .LVU984
	.uleb128 .LVU986
	.uleb128 .LVU990
	.uleb128 .LVU1001
	.uleb128 .LVU1020
	.uleb128 .LVU1021
	.uleb128 .LVU1024
	.uleb128 .LVU1033
.LLST66:
	.quad	.LVL143-.Ltext0
	.quad	.LVL147-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	.LVL150-.Ltext0
	.quad	.LVL156-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	.LVL164-.Ltext0
	.quad	.LVL165-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	.LVL165-.Ltext0
	.quad	.LVL166-.Ltext0
	.value	0x1
	.byte	0x54
	.quad	.LVL167-.Ltext0
	.quad	.LVL170-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	.LVL171-.Ltext0
	.quad	.LVL172-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	.LVL173-.Ltext0
	.quad	.LVL174-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	.LVL174-.Ltext0
	.quad	.LVL175-.Ltext0
	.value	0x1
	.byte	0x59
	.quad	.LVL176-.Ltext0
	.quad	.LVL177-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	.LVL180-.Ltext0
	.quad	.LVL181-.Ltext0
	.value	0x2
	.byte	0x30
	.byte	0x9f
	.quad	.LVL182-.Ltext0
	.quad	.LVL183-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	0
	.quad	0
.LVUS67:
	.uleb128 .LVU788
	.uleb128 .LVU835
	.uleb128 .LVU838
	.uleb128 .LVU885
	.uleb128 .LVU894
	.uleb128 .LVU946
	.uleb128 .LVU950
	.uleb128 .LVU1001
	.uleb128 .LVU1024
	.uleb128 .LVU1033
.LLST67:
	.quad	.LVL145-.Ltext0
	.quad	.LVL151-.Ltext0
	.value	0x1
	.byte	0x51
	.quad	.LVL152-.Ltext0
	.quad	.LVL159-.Ltext0
	.value	0x1
	.byte	0x51
	.quad	.LVL164-.Ltext0
	.quad	.LVL171-.Ltext0
	.value	0x1
	.byte	0x51
	.quad	.LVL173-.Ltext0
	.quad	.LVL177-.Ltext0
	.value	0x1
	.byte	0x51
	.quad	.LVL182-.Ltext0
	.quad	.LVL183-.Ltext0
	.value	0x1
	.byte	0x51
	.quad	0
	.quad	0
.LVUS68:
	.uleb128 .LVU789
	.uleb128 .LVU794
	.uleb128 .LVU803
	.uleb128 .LVU828
	.uleb128 .LVU843
	.uleb128 .LVU847
	.uleb128 .LVU847
	.uleb128 .LVU848
	.uleb128 .LVU857
	.uleb128 .LVU859
	.uleb128 .LVU894
	.uleb128 .LVU896
	.uleb128 .LVU896
	.uleb128 .LVU927
	.uleb128 .LVU927
	.uleb128 .LVU930
	.uleb128 .LVU930
	.uleb128 .LVU932
	.uleb128 .LVU932
	.uleb128 .LVU944
	.uleb128 .LVU944
	.uleb128 .LVU946
	.uleb128 .LVU950
	.uleb128 .LVU952
	.uleb128 .LVU952
	.uleb128 .LVU983
	.uleb128 .LVU983
	.uleb128 .LVU986
	.uleb128 .LVU990
	.uleb128 .LVU1001
	.uleb128 .LVU1024
	.uleb128 .LVU1033
.LLST68:
	.quad	.LVL145-.Ltext0
	.quad	.LVL146-.Ltext0
	.value	0x1
	.byte	0x54
	.quad	.LVL148-.Ltext0
	.quad	.LVL149-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	.LVL153-.Ltext0
	.quad	.LVL154-.Ltext0
	.value	0x1
	.byte	0x54
	.quad	.LVL154-.Ltext0
	.quad	.LVL155-.Ltext0
	.value	0x2
	.byte	0x71
	.sleb128 8
	.quad	.LVL157-.Ltext0
	.quad	.LVL158-.Ltext0
	.value	0x2
	.byte	0x71
	.sleb128 0
	.quad	.LVL164-.Ltext0
	.quad	.LVL164-.Ltext0
	.value	0x1
	.byte	0x54
	.quad	.LVL164-.Ltext0
	.quad	.LVL165-.Ltext0
	.value	0x1
	.byte	0x52
	.quad	.LVL165-.Ltext0
	.quad	.LVL166-.Ltext0
	.value	0x2
	.byte	0x74
	.sleb128 8
	.quad	.LVL166-.Ltext0
	.quad	.LVL167-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	.LVL167-.Ltext0
	.quad	.LVL170-.Ltext0
	.value	0x1
	.byte	0x54
	.quad	.LVL170-.Ltext0
	.quad	.LVL171-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	.LVL173-.Ltext0
	.quad	.LVL173-.Ltext0
	.value	0x2
	.byte	0x71
	.sleb128 8
	.quad	.LVL173-.Ltext0
	.quad	.LVL174-.Ltext0
	.value	0x1
	.byte	0x52
	.quad	.LVL174-.Ltext0
	.quad	.LVL175-.Ltext0
	.value	0x2
	.byte	0x79
	.sleb128 0
	.quad	.LVL176-.Ltext0
	.quad	.LVL177-.Ltext0
	.value	0x1
	.byte	0x52
	.quad	.LVL182-.Ltext0
	.quad	.LVL183-.Ltext0
	.value	0x1
	.byte	0x52
	.quad	0
	.quad	0
.LVUS105:
	.uleb128 0
	.uleb128 .LVU1405
	.uleb128 .LVU1405
	.uleb128 .LVU1407
	.uleb128 .LVU1407
	.uleb128 .LVU1410
	.uleb128 .LVU1410
	.uleb128 .LVU1410
	.uleb128 .LVU1410
	.uleb128 .LVU1412
	.uleb128 .LVU1412
	.uleb128 .LVU1431
	.uleb128 .LVU1431
	.uleb128 .LVU1434
	.uleb128 .LVU1434
	.uleb128 0
.LLST105:
	.quad	.LVL256-.Ltext0
	.quad	.LVL258-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	.LVL258-.Ltext0
	.quad	.LVL259-.Ltext0
	.value	0x1
	.byte	0x5c
	.quad	.LVL259-.Ltext0
	.quad	.LVL262-1-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	.LVL262-1-.Ltext0
	.quad	.LVL262-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.byte	0x9f
	.quad	.LVL262-.Ltext0
	.quad	.LVL263-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	.LVL263-.Ltext0
	.quad	.LVL271-.Ltext0
	.value	0x1
	.byte	0x5c
	.quad	.LVL271-.Ltext0
	.quad	.LVL274-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.byte	0x9f
	.quad	.LVL274-.Ltext0
	.quad	.LFE110-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	0
	.quad	0
.LVUS106:
	.uleb128 0
	.uleb128 .LVU1405
	.uleb128 .LVU1405
	.uleb128 .LVU1409
	.uleb128 .LVU1409
	.uleb128 .LVU1410
	.uleb128 .LVU1410
	.uleb128 .LVU1410
	.uleb128 .LVU1410
	.uleb128 .LVU1413
	.uleb128 .LVU1413
	.uleb128 .LVU1433
	.uleb128 .LVU1433
	.uleb128 .LVU1434
	.uleb128 .LVU1434
	.uleb128 0
.LLST106:
	.quad	.LVL256-.Ltext0
	.quad	.LVL258-.Ltext0
	.value	0x1
	.byte	0x54
	.quad	.LVL258-.Ltext0
	.quad	.LVL261-.Ltext0
	.value	0x1
	.byte	0x5f
	.quad	.LVL261-.Ltext0
	.quad	.LVL262-1-.Ltext0
	.value	0x1
	.byte	0x54
	.quad	.LVL262-1-.Ltext0
	.quad	.LVL262-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x54
	.byte	0x9f
	.quad	.LVL262-.Ltext0
	.quad	.LVL264-1-.Ltext0
	.value	0x1
	.byte	0x54
	.quad	.LVL264-1-.Ltext0
	.quad	.LVL273-.Ltext0
	.value	0x1
	.byte	0x5f
	.quad	.LVL273-.Ltext0
	.quad	.LVL274-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x54
	.byte	0x9f
	.quad	.LVL274-.Ltext0
	.quad	.LFE110-.Ltext0
	.value	0x1
	.byte	0x54
	.quad	0
	.quad	0
.LVUS107:
	.uleb128 0
	.uleb128 .LVU1405
	.uleb128 .LVU1405
	.uleb128 .LVU1408
	.uleb128 .LVU1408
	.uleb128 .LVU1410
	.uleb128 .LVU1410
	.uleb128 .LVU1410
	.uleb128 .LVU1410
	.uleb128 .LVU1413
	.uleb128 .LVU1413
	.uleb128 .LVU1432
	.uleb128 .LVU1432
	.uleb128 .LVU1434
	.uleb128 .LVU1434
	.uleb128 0
.LLST107:
	.quad	.LVL256-.Ltext0
	.quad	.LVL258-.Ltext0
	.value	0x1
	.byte	0x51
	.quad	.LVL258-.Ltext0
	.quad	.LVL260-.Ltext0
	.value	0x1
	.byte	0x5d
	.quad	.LVL260-.Ltext0
	.quad	.LVL262-1-.Ltext0
	.value	0x1
	.byte	0x51
	.quad	.LVL262-1-.Ltext0
	.quad	.LVL262-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x51
	.byte	0x9f
	.quad	.LVL262-.Ltext0
	.quad	.LVL264-1-.Ltext0
	.value	0x1
	.byte	0x51
	.quad	.LVL264-1-.Ltext0
	.quad	.LVL272-.Ltext0
	.value	0x1
	.byte	0x5d
	.quad	.LVL272-.Ltext0
	.quad	.LVL274-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x51
	.byte	0x9f
	.quad	.LVL274-.Ltext0
	.quad	.LFE110-.Ltext0
	.value	0x1
	.byte	0x51
	.quad	0
	.quad	0
.LVUS108:
	.uleb128 0
	.uleb128 .LVU1405
	.uleb128 .LVU1405
	.uleb128 .LVU1410
	.uleb128 .LVU1410
	.uleb128 .LVU1413
	.uleb128 .LVU1413
	.uleb128 .LVU1434
	.uleb128 .LVU1434
	.uleb128 0
.LLST108:
	.quad	.LVL256-.Ltext0
	.quad	.LVL258-.Ltext0
	.value	0x1
	.byte	0x52
	.quad	.LVL258-.Ltext0
	.quad	.LVL262-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x52
	.byte	0x9f
	.quad	.LVL262-.Ltext0
	.quad	.LVL264-1-.Ltext0
	.value	0x1
	.byte	0x52
	.quad	.LVL264-1-.Ltext0
	.quad	.LVL274-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x52
	.byte	0x9f
	.quad	.LVL274-.Ltext0
	.quad	.LFE110-.Ltext0
	.value	0x1
	.byte	0x52
	.quad	0
	.quad	0
.LVUS109:
	.uleb128 .LVU1421
	.uleb128 .LVU1423
	.uleb128 .LVU1427
	.uleb128 .LVU1434
.LLST109:
	.quad	.LVL267-.Ltext0
	.quad	.LVL268-.Ltext0
	.value	0x2
	.byte	0x30
	.byte	0x9f
	.quad	.LVL270-.Ltext0
	.quad	.LVL274-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	0
	.quad	0
.LVUS110:
	.uleb128 .LVU1401
	.uleb128 .LVU1405
	.uleb128 .LVU1410
	.uleb128 .LVU1421
	.uleb128 .LVU1423
	.uleb128 .LVU1427
.LLST110:
	.quad	.LVL257-.Ltext0
	.quad	.LVL258-.Ltext0
	.value	0x1
	.byte	0x5e
	.quad	.LVL262-.Ltext0
	.quad	.LVL267-.Ltext0
	.value	0x1
	.byte	0x5e
	.quad	.LVL268-.Ltext0
	.quad	.LVL270-.Ltext0
	.value	0x1
	.byte	0x5e
	.quad	0
	.quad	0
.LVUS111:
	.uleb128 .LVU1414
	.uleb128 .LVU1419
	.uleb128 .LVU1423
	.uleb128 .LVU1425
.LLST111:
	.quad	.LVL265-.Ltext0
	.quad	.LVL266-1-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	.LVL268-.Ltext0
	.quad	.LVL269-1-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	0
	.quad	0
.LVUS112:
	.uleb128 0
	.uleb128 .LVU1497
	.uleb128 .LVU1497
	.uleb128 .LVU1499
	.uleb128 .LVU1499
	.uleb128 .LVU1510
	.uleb128 .LVU1510
	.uleb128 .LVU1512
	.uleb128 .LVU1512
	.uleb128 .LVU1516
	.uleb128 .LVU1516
	.uleb128 0
.LLST112:
	.quad	.LVL275-.Ltext0
	.quad	.LVL282-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	.LVL282-.Ltext0
	.quad	.LVL283-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.byte	0x9f
	.quad	.LVL283-.Ltext0
	.quad	.LVL289-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	.LVL289-.Ltext0
	.quad	.LVL291-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.byte	0x9f
	.quad	.LVL291-.Ltext0
	.quad	.LVL293-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	.LVL293-.Ltext0
	.quad	.LFE111-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS113:
	.uleb128 .LVU1463
	.uleb128 .LVU1497
	.uleb128 .LVU1509
	.uleb128 .LVU1511
	.uleb128 .LVU1512
	.uleb128 0
.LLST113:
	.quad	.LVL280-.Ltext0
	.quad	.LVL282-.Ltext0
	.value	0x1
	.byte	0x58
	.quad	.LVL288-.Ltext0
	.quad	.LVL290-1-.Ltext0
	.value	0x1
	.byte	0x58
	.quad	.LVL291-.Ltext0
	.quad	.LFE111-.Ltext0
	.value	0x2
	.byte	0x30
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS114:
	.uleb128 .LVU1446
	.uleb128 .LVU1463
	.uleb128 .LVU1499
	.uleb128 .LVU1503
	.uleb128 .LVU1505
	.uleb128 .LVU1509
.LLST114:
	.quad	.LVL277-.Ltext0
	.quad	.LVL280-.Ltext0
	.value	0x1
	.byte	0x51
	.quad	.LVL283-.Ltext0
	.quad	.LVL285-.Ltext0
	.value	0x1
	.byte	0x51
	.quad	.LVL286-.Ltext0
	.quad	.LVL288-.Ltext0
	.value	0x1
	.byte	0x51
	.quad	0
	.quad	0
.LVUS115:
	.uleb128 .LVU1445
	.uleb128 .LVU1463
	.uleb128 .LVU1499
	.uleb128 .LVU1503
	.uleb128 .LVU1505
	.uleb128 .LVU1509
.LLST115:
	.quad	.LVL276-.Ltext0
	.quad	.LVL280-.Ltext0
	.value	0x1
	.byte	0x52
	.quad	.LVL283-.Ltext0
	.quad	.LVL285-.Ltext0
	.value	0x1
	.byte	0x52
	.quad	.LVL286-.Ltext0
	.quad	.LVL288-.Ltext0
	.value	0x1
	.byte	0x52
	.quad	0
	.quad	0
.LVUS116:
	.uleb128 .LVU1453
	.uleb128 .LVU1497
	.uleb128 .LVU1499
	.uleb128 .LVU1503
	.uleb128 .LVU1505
	.uleb128 .LVU1511
	.uleb128 .LVU1512
	.uleb128 .LVU1515
	.uleb128 .LVU1515
	.uleb128 .LVU1516
	.uleb128 .LVU1516
	.uleb128 .LVU1517
.LLST116:
	.quad	.LVL278-.Ltext0
	.quad	.LVL282-.Ltext0
	.value	0x4
	.byte	0x72
	.sleb128 832
	.byte	0x9f
	.quad	.LVL283-.Ltext0
	.quad	.LVL285-.Ltext0
	.value	0x4
	.byte	0x72
	.sleb128 832
	.byte	0x9f
	.quad	.LVL286-.Ltext0
	.quad	.LVL290-1-.Ltext0
	.value	0x4
	.byte	0x72
	.sleb128 832
	.byte	0x9f
	.quad	.LVL291-.Ltext0
	.quad	.LVL292-.Ltext0
	.value	0x4
	.byte	0x72
	.sleb128 832
	.byte	0x9f
	.quad	.LVL292-.Ltext0
	.quad	.LVL293-.Ltext0
	.value	0x7
	.byte	0x75
	.sleb128 8
	.byte	0x6
	.byte	0x23
	.uleb128 0x340
	.byte	0x9f
	.quad	.LVL293-.Ltext0
	.quad	.LVL294-1-.Ltext0
	.value	0xa
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.byte	0x23
	.uleb128 0x8
	.byte	0x6
	.byte	0x23
	.uleb128 0x340
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS117:
	.uleb128 .LVU1452
	.uleb128 .LVU1463
	.uleb128 .LVU1499
	.uleb128 .LVU1503
	.uleb128 .LVU1505
	.uleb128 .LVU1509
.LLST117:
	.quad	.LVL278-.Ltext0
	.quad	.LVL280-.Ltext0
	.value	0x6
	.byte	0xf2
	.long	.Ldebug_info0+11428
	.sleb128 0
	.quad	.LVL283-.Ltext0
	.quad	.LVL285-.Ltext0
	.value	0x6
	.byte	0xf2
	.long	.Ldebug_info0+11428
	.sleb128 0
	.quad	.LVL286-.Ltext0
	.quad	.LVL288-.Ltext0
	.value	0x6
	.byte	0xf2
	.long	.Ldebug_info0+11428
	.sleb128 0
	.quad	0
	.quad	0
.LVUS118:
	.uleb128 .LVU1454
	.uleb128 .LVU1463
	.uleb128 .LVU1499
	.uleb128 .LVU1503
	.uleb128 .LVU1505
	.uleb128 .LVU1509
.LLST118:
	.quad	.LVL278-.Ltext0
	.quad	.LVL280-.Ltext0
	.value	0x1
	.byte	0x58
	.quad	.LVL283-.Ltext0
	.quad	.LVL285-.Ltext0
	.value	0x1
	.byte	0x58
	.quad	.LVL286-.Ltext0
	.quad	.LVL288-.Ltext0
	.value	0x1
	.byte	0x58
	.quad	0
	.quad	0
.LVUS119:
	.uleb128 .LVU1499
	.uleb128 .LVU1503
.LLST119:
	.quad	.LVL283-.Ltext0
	.quad	.LVL285-.Ltext0
	.value	0x3
	.byte	0x9
	.byte	0xff
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS120:
	.uleb128 .LVU1459
	.uleb128 .LVU1503
	.uleb128 .LVU1505
	.uleb128 .LVU1512
.LLST120:
	.quad	.LVL279-.Ltext0
	.quad	.LVL285-.Ltext0
	.value	0x6
	.byte	0xf2
	.long	.Ldebug_info0+11428
	.sleb128 0
	.quad	.LVL286-.Ltext0
	.quad	.LVL291-.Ltext0
	.value	0x6
	.byte	0xf2
	.long	.Ldebug_info0+11428
	.sleb128 0
	.quad	0
	.quad	0
.LVUS121:
	.uleb128 .LVU1459
	.uleb128 .LVU1497
	.uleb128 .LVU1499
	.uleb128 .LVU1502
	.uleb128 .LVU1505
	.uleb128 .LVU1508
	.uleb128 .LVU1509
	.uleb128 .LVU1511
.LLST121:
	.quad	.LVL279-.Ltext0
	.quad	.LVL282-.Ltext0
	.value	0x1
	.byte	0x58
	.quad	.LVL283-.Ltext0
	.quad	.LVL284-.Ltext0
	.value	0x1
	.byte	0x58
	.quad	.LVL286-.Ltext0
	.quad	.LVL287-.Ltext0
	.value	0x1
	.byte	0x58
	.quad	.LVL288-.Ltext0
	.quad	.LVL290-1-.Ltext0
	.value	0x1
	.byte	0x58
	.quad	0
	.quad	0
.LVUS122:
	.uleb128 .LVU1492
	.uleb128 .LVU1497
	.uleb128 .LVU1509
	.uleb128 .LVU1510
	.uleb128 .LVU1510
	.uleb128 .LVU1511
.LLST122:
	.quad	.LVL281-.Ltext0
	.quad	.LVL282-.Ltext0
	.value	0x2
	.byte	0x75
	.sleb128 8
	.quad	.LVL288-.Ltext0
	.quad	.LVL289-.Ltext0
	.value	0x2
	.byte	0x75
	.sleb128 8
	.quad	.LVL289-.Ltext0
	.quad	.LVL290-1-.Ltext0
	.value	0x5
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.byte	0x23
	.uleb128 0x8
	.quad	0
	.quad	0
.LVUS123:
	.uleb128 .LVU1492
	.uleb128 .LVU1497
	.uleb128 .LVU1509
	.uleb128 .LVU1511
.LLST123:
	.quad	.LVL281-.Ltext0
	.quad	.LVL282-.Ltext0
	.value	0x1
	.byte	0x58
	.quad	.LVL288-.Ltext0
	.quad	.LVL290-1-.Ltext0
	.value	0x1
	.byte	0x58
	.quad	0
	.quad	0
	.section	.debug_aranges,"",@progbits
	.long	0x2c
	.value	0x2
	.long	.Ldebug_info0
	.byte	0x8
	.byte	0
	.value	0
	.value	0
	.quad	.Ltext0
	.quad	.Letext0-.Ltext0
	.quad	0
	.quad	0
	.section	.debug_ranges,"",@progbits
.Ldebug_ranges0:
	.quad	.LBB73-.Ltext0
	.quad	.LBE73-.Ltext0
	.quad	.LBB97-.Ltext0
	.quad	.LBE97-.Ltext0
	.quad	.LBB98-.Ltext0
	.quad	.LBE98-.Ltext0
	.quad	0
	.quad	0
	.quad	.LBB75-.Ltext0
	.quad	.LBE75-.Ltext0
	.quad	.LBB89-.Ltext0
	.quad	.LBE89-.Ltext0
	.quad	.LBB91-.Ltext0
	.quad	.LBE91-.Ltext0
	.quad	.LBB93-.Ltext0
	.quad	.LBE93-.Ltext0
	.quad	0
	.quad	0
	.quad	.LBB76-.Ltext0
	.quad	.LBE76-.Ltext0
	.quad	.LBB88-.Ltext0
	.quad	.LBE88-.Ltext0
	.quad	.LBB90-.Ltext0
	.quad	.LBE90-.Ltext0
	.quad	.LBB92-.Ltext0
	.quad	.LBE92-.Ltext0
	.quad	.LBB94-.Ltext0
	.quad	.LBE94-.Ltext0
	.quad	0
	.quad	0
	.quad	.LBB78-.Ltext0
	.quad	.LBE78-.Ltext0
	.quad	.LBB79-.Ltext0
	.quad	.LBE79-.Ltext0
	.quad	.LBB82-.Ltext0
	.quad	.LBE82-.Ltext0
	.quad	0
	.quad	0
	.quad	.LBB80-.Ltext0
	.quad	.LBE80-.Ltext0
	.quad	.LBB81-.Ltext0
	.quad	.LBE81-.Ltext0
	.quad	.LBB83-.Ltext0
	.quad	.LBE83-.Ltext0
	.quad	0
	.quad	0
	.quad	.LBB99-.Ltext0
	.quad	.LBE99-.Ltext0
	.quad	.LBB117-.Ltext0
	.quad	.LBE117-.Ltext0
	.quad	0
	.quad	0
	.quad	.LBB102-.Ltext0
	.quad	.LBE102-.Ltext0
	.quad	.LBB113-.Ltext0
	.quad	.LBE113-.Ltext0
	.quad	.LBB114-.Ltext0
	.quad	.LBE114-.Ltext0
	.quad	.LBB121-.Ltext0
	.quad	.LBE121-.Ltext0
	.quad	0
	.quad	0
	.quad	.LBB104-.Ltext0
	.quad	.LBE104-.Ltext0
	.quad	.LBB109-.Ltext0
	.quad	.LBE109-.Ltext0
	.quad	0
	.quad	0
	.quad	.LBB118-.Ltext0
	.quad	.LBE118-.Ltext0
	.quad	.LBB122-.Ltext0
	.quad	.LBE122-.Ltext0
	.quad	0
	.quad	0
	.quad	.LBB137-.Ltext0
	.quad	.LBE137-.Ltext0
	.quad	.LBB149-.Ltext0
	.quad	.LBE149-.Ltext0
	.quad	.LBB150-.Ltext0
	.quad	.LBE150-.Ltext0
	.quad	0
	.quad	0
	.quad	.LBB139-.Ltext0
	.quad	.LBE139-.Ltext0
	.quad	.LBB145-.Ltext0
	.quad	.LBE145-.Ltext0
	.quad	.LBB146-.Ltext0
	.quad	.LBE146-.Ltext0
	.quad	0
	.quad	0
	.quad	.LBB151-.Ltext0
	.quad	.LBE151-.Ltext0
	.quad	.LBB154-.Ltext0
	.quad	.LBE154-.Ltext0
	.quad	0
	.quad	0
	.quad	.LBB155-.Ltext0
	.quad	.LBE155-.Ltext0
	.quad	.LBB172-.Ltext0
	.quad	.LBE172-.Ltext0
	.quad	0
	.quad	0
	.quad	.LBB157-.Ltext0
	.quad	.LBE157-.Ltext0
	.quad	.LBB161-.Ltext0
	.quad	.LBE161-.Ltext0
	.quad	.LBB162-.Ltext0
	.quad	.LBE162-.Ltext0
	.quad	0
	.quad	0
	.quad	.LBB163-.Ltext0
	.quad	.LBE163-.Ltext0
	.quad	.LBB168-.Ltext0
	.quad	.LBE168-.Ltext0
	.quad	.LBB169-.Ltext0
	.quad	.LBE169-.Ltext0
	.quad	.LBB170-.Ltext0
	.quad	.LBE170-.Ltext0
	.quad	0
	.quad	0
	.quad	.LBB197-.Ltext0
	.quad	.LBE197-.Ltext0
	.quad	.LBB253-.Ltext0
	.quad	.LBE253-.Ltext0
	.quad	.LBB256-.Ltext0
	.quad	.LBE256-.Ltext0
	.quad	0
	.quad	0
	.quad	.LBB201-.Ltext0
	.quad	.LBE201-.Ltext0
	.quad	.LBB202-.Ltext0
	.quad	.LBE202-.Ltext0
	.quad	0
	.quad	0
	.quad	.LBB203-.Ltext0
	.quad	.LBE203-.Ltext0
	.quad	.LBB230-.Ltext0
	.quad	.LBE230-.Ltext0
	.quad	.LBB231-.Ltext0
	.quad	.LBE231-.Ltext0
	.quad	0
	.quad	0
	.quad	.LBB205-.Ltext0
	.quad	.LBE205-.Ltext0
	.quad	.LBB225-.Ltext0
	.quad	.LBE225-.Ltext0
	.quad	.LBB226-.Ltext0
	.quad	.LBE226-.Ltext0
	.quad	0
	.quad	0
	.quad	.LBB207-.Ltext0
	.quad	.LBE207-.Ltext0
	.quad	.LBB216-.Ltext0
	.quad	.LBE216-.Ltext0
	.quad	.LBB217-.Ltext0
	.quad	.LBE217-.Ltext0
	.quad	0
	.quad	0
	.quad	.LBB208-.Ltext0
	.quad	.LBE208-.Ltext0
	.quad	.LBB213-.Ltext0
	.quad	.LBE213-.Ltext0
	.quad	.LBB214-.Ltext0
	.quad	.LBE214-.Ltext0
	.quad	.LBB215-.Ltext0
	.quad	.LBE215-.Ltext0
	.quad	0
	.quad	0
	.quad	.LBB209-.Ltext0
	.quad	.LBE209-.Ltext0
	.quad	.LBB212-.Ltext0
	.quad	.LBE212-.Ltext0
	.quad	0
	.quad	0
	.quad	.LBB220-.Ltext0
	.quad	.LBE220-.Ltext0
	.quad	.LBB224-.Ltext0
	.quad	.LBE224-.Ltext0
	.quad	.LBB227-.Ltext0
	.quad	.LBE227-.Ltext0
	.quad	0
	.quad	0
	.quad	.LBB232-.Ltext0
	.quad	.LBE232-.Ltext0
	.quad	.LBB252-.Ltext0
	.quad	.LBE252-.Ltext0
	.quad	0
	.quad	0
	.quad	.LBB235-.Ltext0
	.quad	.LBE235-.Ltext0
	.quad	.LBB236-.Ltext0
	.quad	.LBE236-.Ltext0
	.quad	0
	.quad	0
	.quad	.LBB237-.Ltext0
	.quad	.LBE237-.Ltext0
	.quad	.LBB251-.Ltext0
	.quad	.LBE251-.Ltext0
	.quad	.LBB254-.Ltext0
	.quad	.LBE254-.Ltext0
	.quad	.LBB255-.Ltext0
	.quad	.LBE255-.Ltext0
	.quad	.LBB257-.Ltext0
	.quad	.LBE257-.Ltext0
	.quad	0
	.quad	0
	.quad	.LBB239-.Ltext0
	.quad	.LBE239-.Ltext0
	.quad	.LBB244-.Ltext0
	.quad	.LBE244-.Ltext0
	.quad	.LBB245-.Ltext0
	.quad	.LBE245-.Ltext0
	.quad	.LBB246-.Ltext0
	.quad	.LBE246-.Ltext0
	.quad	0
	.quad	0
	.quad	.LBB260-.Ltext0
	.quad	.LBE260-.Ltext0
	.quad	.LBB264-.Ltext0
	.quad	.LBE264-.Ltext0
	.quad	.LBB265-.Ltext0
	.quad	.LBE265-.Ltext0
	.quad	0
	.quad	0
	.quad	.LBB274-.Ltext0
	.quad	.LBE274-.Ltext0
	.quad	.LBB287-.Ltext0
	.quad	.LBE287-.Ltext0
	.quad	.LBB291-.Ltext0
	.quad	.LBE291-.Ltext0
	.quad	.LBB292-.Ltext0
	.quad	.LBE292-.Ltext0
	.quad	0
	.quad	0
	.quad	.LBB288-.Ltext0
	.quad	.LBE288-.Ltext0
	.quad	.LBB293-.Ltext0
	.quad	.LBE293-.Ltext0
	.quad	0
	.quad	0
	.quad	.LBB304-.Ltext0
	.quad	.LBE304-.Ltext0
	.quad	.LBB322-.Ltext0
	.quad	.LBE322-.Ltext0
	.quad	.LBB323-.Ltext0
	.quad	.LBE323-.Ltext0
	.quad	0
	.quad	0
	.quad	.LBB306-.Ltext0
	.quad	.LBE306-.Ltext0
	.quad	.LBB318-.Ltext0
	.quad	.LBE318-.Ltext0
	.quad	0
	.quad	0
	.quad	.LBB308-.Ltext0
	.quad	.LBE308-.Ltext0
	.quad	.LBB313-.Ltext0
	.quad	.LBE313-.Ltext0
	.quad	0
	.quad	0
	.quad	.LBB315-.Ltext0
	.quad	.LBE315-.Ltext0
	.quad	.LBB319-.Ltext0
	.quad	.LBE319-.Ltext0
	.quad	0
	.quad	0
	.section	.debug_line,"",@progbits
.Ldebug_line0:
	.section	.debug_str,"MS",@progbits,1
.LASF349:
	.string	"UV_HANDLE_TCP_ACCEPT_STATE_CHANGING"
.LASF166:
	.string	"async_wfd"
.LASF102:
	.string	"sockaddr_ax25"
.LASF208:
	.string	"UV_ECHARSET"
.LASF113:
	.string	"sin6_flowinfo"
.LASF412:
	.string	"gparent"
.LASF429:
	.string	"uv__io_start"
.LASF339:
	.string	"UV_HANDLE_READ_PENDING"
.LASF37:
	.string	"_shortbuf"
.LASF369:
	.string	"name"
.LASF425:
	.string	"strrchr"
.LASF439:
	.string	"_IO_lock_t"
.LASF121:
	.string	"sockaddr_x25"
.LASF1:
	.string	"program_invocation_short_name"
.LASF266:
	.string	"UV_ASYNC"
.LASF54:
	.string	"stderr"
.LASF174:
	.string	"inotify_read_watcher"
.LASF89:
	.string	"__flags"
.LASF26:
	.string	"_IO_buf_end"
.LASF399:
	.string	"watcher_root_RB_FIND"
.LASF181:
	.string	"uv__io_t"
.LASF100:
	.string	"sa_data"
.LASF332:
	.string	"UV_HANDLE_SHUT"
.LASF248:
	.string	"UV_EROFS"
.LASF317:
	.string	"rbe_parent"
.LASF148:
	.string	"flags"
.LASF210:
	.string	"UV_ECONNREFUSED"
.LASF413:
	.string	"compare_watchers"
.LASF206:
	.string	"UV_EBUSY"
.LASF326:
	.string	"UV_HANDLE_REF"
.LASF245:
	.string	"UV_EPROTONOSUPPORT"
.LASF288:
	.string	"loop"
.LASF115:
	.string	"sin6_scope_id"
.LASF84:
	.string	"__cur_writer"
.LASF24:
	.string	"_IO_write_end"
.LASF5:
	.string	"unsigned int"
.LASF119:
	.string	"sockaddr_ns"
.LASF357:
	.string	"UV_HANDLE_TTY_READABLE"
.LASF271:
	.string	"UV_IDLE"
.LASF156:
	.string	"wq_async"
.LASF141:
	.string	"getdate_err"
.LASF254:
	.string	"UV_EXDEV"
.LASF18:
	.string	"_flags"
.LASF144:
	.string	"active_handles"
.LASF151:
	.string	"watcher_queue"
.LASF47:
	.string	"FILE"
.LASF305:
	.string	"caught_signals"
.LASF149:
	.string	"backend_fd"
.LASF180:
	.string	"events"
.LASF157:
	.string	"cloexec_lock"
.LASF173:
	.string	"emfile_fd"
.LASF187:
	.string	"UV_EADDRNOTAVAIL"
.LASF287:
	.string	"uv_handle_s"
.LASF286:
	.string	"uv_handle_t"
.LASF30:
	.string	"_markers"
.LASF334:
	.string	"UV_HANDLE_READ_EOF"
.LASF57:
	.string	"_sys_nerr"
.LASF133:
	.string	"_sys_siglist"
.LASF202:
	.string	"UV_EAI_SERVICE"
.LASF255:
	.string	"UV_UNKNOWN"
.LASF205:
	.string	"UV_EBADF"
.LASF327:
	.string	"UV_HANDLE_INTERNAL"
.LASF262:
	.string	"UV_EFTYPE"
.LASF428:
	.string	"uv__io_init"
.LASF294:
	.string	"async_cb"
.LASF224:
	.string	"UV_EMSGSIZE"
.LASF152:
	.string	"watchers"
.LASF146:
	.string	"active_reqs"
.LASF183:
	.string	"uv_rwlock_t"
.LASF79:
	.string	"__writers"
.LASF361:
	.string	"UV_SIGNAL_ONE_SHOT_DISPATCHED"
.LASF444:
	.string	"__builtin_memcpy"
.LASF298:
	.string	"uv_fs_event_s"
.LASF297:
	.string	"uv_fs_event_t"
.LASF442:
	.string	"uv__inotify_read"
.LASF51:
	.string	"ssize_t"
.LASF215:
	.string	"UV_EFBIG"
.LASF154:
	.string	"nfds"
.LASF243:
	.string	"UV_EPIPE"
.LASF130:
	.string	"__in6_u"
.LASF426:
	.string	"__errno_location"
.LASF346:
	.string	"UV_HANDLE_TCP_NODELAY"
.LASF117:
	.string	"sockaddr_ipx"
.LASF353:
	.string	"UV_HANDLE_UDP_CONNECTED"
.LASF313:
	.string	"UV_RENAME"
.LASF222:
	.string	"UV_ELOOP"
.LASF90:
	.string	"long long unsigned int"
.LASF226:
	.string	"UV_ENETDOWN"
.LASF64:
	.string	"__pthread_internal_list"
.LASF85:
	.string	"__shared"
.LASF61:
	.string	"uint32_t"
.LASF65:
	.string	"__prev"
.LASF247:
	.string	"UV_ERANGE"
.LASF122:
	.string	"in_addr_t"
.LASF309:
	.string	"uv_fs_event_cb"
.LASF362:
	.string	"UV_SIGNAL_ONE_SHOT"
.LASF268:
	.string	"UV_FS_EVENT"
.LASF53:
	.string	"stdout"
.LASF424:
	.string	"__read_alias"
.LASF29:
	.string	"_IO_save_end"
.LASF70:
	.string	"__count"
.LASF172:
	.string	"child_watcher"
.LASF374:
	.string	"opterr"
.LASF368:
	.string	"cookie"
.LASF225:
	.string	"UV_ENAMETOOLONG"
.LASF365:
	.string	"IN_NONBLOCK"
.LASF422:
	.string	"__len"
.LASF320:
	.string	"count"
.LASF301:
	.string	"uv_signal_s"
.LASF300:
	.string	"uv_signal_t"
.LASF169:
	.string	"time"
.LASF410:
	.string	"oright"
.LASF167:
	.string	"timer_heap"
.LASF126:
	.string	"__u6_addr8"
.LASF193:
	.string	"UV_EAI_BADHINTS"
.LASF195:
	.string	"UV_EAI_FAIL"
.LASF184:
	.string	"UV_E2BIG"
.LASF192:
	.string	"UV_EAI_BADFLAGS"
.LASF103:
	.string	"sockaddr_dl"
.LASF272:
	.string	"UV_NAMED_PIPE"
.LASF282:
	.string	"UV_FILE"
.LASF13:
	.string	"__uint32_t"
.LASF186:
	.string	"UV_EADDRINUSE"
.LASF310:
	.string	"uv_signal_cb"
.LASF106:
	.string	"sin_family"
.LASF12:
	.string	"__uint16_t"
.LASF56:
	.string	"sys_errlist"
.LASF366:
	.string	"inotify_event"
.LASF71:
	.string	"__owner"
.LASF290:
	.string	"close_cb"
.LASF153:
	.string	"nwatchers"
.LASF269:
	.string	"UV_FS_POLL"
.LASF403:
	.string	"child"
.LASF125:
	.string	"in_port_t"
.LASF75:
	.string	"__elision"
.LASF55:
	.string	"sys_nerr"
.LASF384:
	.string	"dummy"
.LASF209:
	.string	"UV_ECONNABORTED"
.LASF199:
	.string	"UV_EAI_NONAME"
.LASF86:
	.string	"__rwelision"
.LASF328:
	.string	"UV_HANDLE_ENDGAME_QUEUED"
.LASF32:
	.string	"_fileno"
.LASF87:
	.string	"__pad1"
.LASF415:
	.string	"__buf"
.LASF112:
	.string	"sin6_port"
.LASF404:
	.string	"color"
.LASF109:
	.string	"sin_zero"
.LASF191:
	.string	"UV_EAI_AGAIN"
.LASF124:
	.string	"s_addr"
.LASF388:
	.string	"uv__inotify_fork"
.LASF9:
	.string	"size_t"
.LASF97:
	.string	"sa_family_t"
.LASF7:
	.string	"short unsigned int"
.LASF246:
	.string	"UV_EPROTOTYPE"
.LASF264:
	.string	"UV_ERRNO_MAX"
.LASF214:
	.string	"UV_EFAULT"
.LASF249:
	.string	"UV_ESHUTDOWN"
.LASF21:
	.string	"_IO_read_base"
.LASF35:
	.string	"_cur_column"
.LASF347:
	.string	"UV_HANDLE_TCP_KEEPALIVE"
.LASF116:
	.string	"sockaddr_inarp"
.LASF52:
	.string	"stdin"
.LASF345:
	.string	"UV_HANDLE_IPV6"
.LASF163:
	.string	"async_handles"
.LASF96:
	.string	"pthread_rwlock_t"
.LASF114:
	.string	"sin6_addr"
.LASF14:
	.string	"__uint64_t"
.LASF296:
	.string	"pending"
.LASF118:
	.string	"sockaddr_iso"
.LASF303:
	.string	"signum"
.LASF299:
	.string	"path"
.LASF381:
	.string	"uv_fs_event_stop"
.LASF256:
	.string	"UV_EOF"
.LASF263:
	.string	"UV_EILSEQ"
.LASF411:
	.string	"watcher_root_RB_INSERT_COLOR"
.LASF217:
	.string	"UV_EINTR"
.LASF233:
	.string	"UV_ENONET"
.LASF244:
	.string	"UV_EPROTO"
.LASF66:
	.string	"__next"
.LASF81:
	.string	"__writers_futex"
.LASF155:
	.string	"wq_mutex"
.LASF132:
	.string	"in6addr_loopback"
.LASF189:
	.string	"UV_EAGAIN"
.LASF239:
	.string	"UV_ENOTEMPTY"
.LASF438:
	.string	"/workspace/nodejsbuild/node-v12.18.4/out"
.LASF231:
	.string	"UV_ENOENT"
.LASF284:
	.string	"uv_handle_type"
.LASF2:
	.string	"char"
.LASF427:
	.string	"inotify_init1"
.LASF273:
	.string	"UV_POLL"
.LASF253:
	.string	"UV_ETXTBSY"
.LASF136:
	.string	"__daylight"
.LASF230:
	.string	"UV_ENODEV"
.LASF337:
	.string	"UV_HANDLE_READABLE"
.LASF138:
	.string	"tzname"
.LASF48:
	.string	"_IO_marker"
.LASF371:
	.string	"environ"
.LASF336:
	.string	"UV_HANDLE_BOUND"
.LASF308:
	.string	"uv_async_cb"
.LASF19:
	.string	"_IO_read_ptr"
.LASF409:
	.string	"oleft"
.LASF143:
	.string	"data"
.LASF190:
	.string	"UV_EAI_ADDRFAMILY"
.LASF321:
	.string	"nelts"
.LASF314:
	.string	"UV_CHANGE"
.LASF352:
	.string	"UV_HANDLE_UDP_PROCESSING"
.LASF386:
	.string	"__PRETTY_FUNCTION__"
.LASF318:
	.string	"rbe_color"
.LASF433:
	.string	"inotify_add_watch"
.LASF59:
	.string	"uint8_t"
.LASF101:
	.string	"sockaddr_at"
.LASF382:
	.string	"uv_fs_event_start"
.LASF207:
	.string	"UV_ECANCELED"
.LASF260:
	.string	"UV_EREMOTEIO"
.LASF197:
	.string	"UV_EAI_MEMORY"
.LASF77:
	.string	"__pthread_rwlock_arch_t"
.LASF325:
	.string	"UV_HANDLE_ACTIVE"
.LASF134:
	.string	"sys_siglist"
.LASF235:
	.string	"UV_ENOSPC"
.LASF42:
	.string	"_freeres_list"
.LASF401:
	.string	"watcher_root_RB_INSERT"
.LASF355:
	.string	"UV_HANDLE_NON_OVERLAPPED_PIPE"
.LASF252:
	.string	"UV_ETIMEDOUT"
.LASF238:
	.string	"UV_ENOTDIR"
.LASF22:
	.string	"_IO_write_base"
.LASF76:
	.string	"__list"
.LASF95:
	.string	"long long int"
.LASF437:
	.string	"../deps/uv/src/unix/linux-inotify.c"
.LASF131:
	.string	"in6addr_any"
.LASF344:
	.string	"UV_HANDLE_CANCELLATION_PENDING"
.LASF98:
	.string	"sockaddr"
.LASF323:
	.string	"UV_HANDLE_CLOSING"
.LASF27:
	.string	"_IO_save_base"
.LASF234:
	.string	"UV_ENOPROTOOPT"
.LASF107:
	.string	"sin_port"
.LASF257:
	.string	"UV_ENXIO"
.LASF394:
	.string	"init_inotify"
.LASF104:
	.string	"sockaddr_eon"
.LASF432:
	.string	"inotify_rm_watch"
.LASF380:
	.string	"rbh_root"
.LASF128:
	.string	"__u6_addr32"
.LASF373:
	.string	"optind"
.LASF390:
	.string	"tmp_watcher_list_iter"
.LASF120:
	.string	"sockaddr_un"
.LASF179:
	.string	"pevents"
.LASF237:
	.string	"UV_ENOTCONN"
.LASF200:
	.string	"UV_EAI_OVERFLOW"
.LASF277:
	.string	"UV_TCP"
.LASF229:
	.string	"UV_ENOBUFS"
.LASF333:
	.string	"UV_HANDLE_READ_PARTIAL"
.LASF385:
	.string	"size"
.LASF43:
	.string	"_freeres_buf"
.LASF20:
	.string	"_IO_read_end"
.LASF28:
	.string	"_IO_backup_base"
.LASF220:
	.string	"UV_EISCONN"
.LASF108:
	.string	"sin_addr"
.LASF73:
	.string	"__kind"
.LASF307:
	.string	"uv_close_cb"
.LASF367:
	.string	"mask"
.LASF88:
	.string	"__pad2"
.LASF82:
	.string	"__pad3"
.LASF83:
	.string	"__pad4"
.LASF44:
	.string	"__pad5"
.LASF219:
	.string	"UV_EIO"
.LASF4:
	.string	"long unsigned int"
.LASF324:
	.string	"UV_HANDLE_CLOSED"
.LASF240:
	.string	"UV_ENOTSOCK"
.LASF396:
	.string	"head"
.LASF276:
	.string	"UV_STREAM"
.LASF145:
	.string	"handle_queue"
.LASF258:
	.string	"UV_EMLINK"
.LASF36:
	.string	"_vtable_offset"
.LASF223:
	.string	"UV_EMFILE"
.LASF434:
	.string	"strlen"
.LASF251:
	.string	"UV_ESRCH"
.LASF0:
	.string	"program_invocation_name"
.LASF45:
	.string	"_mode"
.LASF372:
	.string	"optarg"
.LASF204:
	.string	"UV_EALREADY"
.LASF161:
	.string	"check_handles"
.LASF67:
	.string	"__pthread_list_t"
.LASF283:
	.string	"UV_HANDLE_TYPE_MAX"
.LASF270:
	.string	"UV_HANDLE"
.LASF60:
	.string	"uint16_t"
.LASF185:
	.string	"UV_EACCES"
.LASF316:
	.string	"rbe_right"
.LASF158:
	.string	"closing_handles"
.LASF364:
	.string	"IN_CLOEXEC"
.LASF198:
	.string	"UV_EAI_NODATA"
.LASF348:
	.string	"UV_HANDLE_TCP_SINGLE_ACCEPT"
.LASF140:
	.string	"timezone"
.LASF201:
	.string	"UV_EAI_PROTOCOL"
.LASF203:
	.string	"UV_EAI_SOCKTYPE"
.LASF213:
	.string	"UV_EEXIST"
.LASF340:
	.string	"UV_HANDLE_SYNC_BYPASS_IOCP"
.LASF228:
	.string	"UV_ENFILE"
.LASF127:
	.string	"__u6_addr16"
.LASF436:
	.string	"GNU C89 9.3.0 -m64 -mtune=generic -march=x86-64 -g -O3 -std=gnu90 -fvisibility=hidden -fno-omit-frame-pointer -fasynchronous-unwind-tables -fstack-protector-strong -fstack-clash-protection -fcf-protection"
.LASF430:
	.string	"uv__strdup"
.LASF397:
	.string	"parent"
.LASF111:
	.string	"sin6_family"
.LASF375:
	.string	"optopt"
.LASF279:
	.string	"UV_TTY"
.LASF11:
	.string	"short int"
.LASF177:
	.string	"uv__io_cb"
.LASF72:
	.string	"__nusers"
.LASF236:
	.string	"UV_ENOSYS"
.LASF265:
	.string	"UV_UNKNOWN_HANDLE"
.LASF212:
	.string	"UV_EDESTADDRREQ"
.LASF3:
	.string	"long int"
.LASF315:
	.string	"rbe_left"
.LASF376:
	.string	"watcher_list"
.LASF443:
	.string	"__stack_chk_fail"
.LASF363:
	.string	"UV_HANDLE_POLL_SLOW"
.LASF227:
	.string	"UV_ENETUNREACH"
.LASF168:
	.string	"timer_counter"
.LASF216:
	.string	"UV_EHOSTUNREACH"
.LASF50:
	.string	"_IO_wide_data"
.LASF242:
	.string	"UV_EPERM"
.LASF175:
	.string	"inotify_watchers"
.LASF62:
	.string	"uint64_t"
.LASF417:
	.string	"uv__basename_r"
.LASF370:
	.string	"__environ"
.LASF221:
	.string	"UV_EISDIR"
.LASF182:
	.string	"uv_mutex_t"
.LASF39:
	.string	"_offset"
.LASF378:
	.string	"iterating"
.LASF291:
	.string	"next_closing"
.LASF395:
	.string	"watcher_root_RB_MINMAX"
.LASF338:
	.string	"UV_HANDLE_WRITABLE"
.LASF17:
	.string	"__ssize_t"
.LASF105:
	.string	"sockaddr_in"
.LASF398:
	.string	"watcher_root_RB_NEXT"
.LASF10:
	.string	"__uint8_t"
.LASF391:
	.string	"tmp_watcher_list"
.LASF91:
	.string	"__data"
.LASF356:
	.string	"UV_HANDLE_PIPESERVER"
.LASF418:
	.string	"read"
.LASF280:
	.string	"UV_UDP"
.LASF341:
	.string	"UV_HANDLE_ZERO_READ"
.LASF150:
	.string	"pending_queue"
.LASF360:
	.string	"UV_HANDLE_TTY_SAVED_ATTRIBUTES"
.LASF274:
	.string	"UV_PREPARE"
.LASF25:
	.string	"_IO_buf_base"
.LASF137:
	.string	"__timezone"
.LASF440:
	.string	"uv_fs_event"
.LASF176:
	.string	"inotify_fd"
.LASF278:
	.string	"UV_TIMER"
.LASF393:
	.string	"find_watcher"
.LASF41:
	.string	"_wide_data"
.LASF322:
	.string	"QUEUE"
.LASF38:
	.string	"_lock"
.LASF281:
	.string	"UV_SIGNAL"
.LASF129:
	.string	"in6_addr"
.LASF343:
	.string	"UV_HANDLE_BLOCKING_WRITES"
.LASF49:
	.string	"_IO_codecvt"
.LASF40:
	.string	"_codecvt"
.LASF354:
	.string	"UV_HANDLE_UDP_RECVMMSG"
.LASF34:
	.string	"_old_offset"
.LASF178:
	.string	"uv__io_s"
.LASF63:
	.string	"_IO_FILE"
.LASF319:
	.string	"unused"
.LASF80:
	.string	"__wrphase_futex"
.LASF402:
	.string	"watcher_root_RB_REMOVE"
.LASF164:
	.string	"async_unused"
.LASF241:
	.string	"UV_ENOTSUP"
.LASF306:
	.string	"dispatched_signals"
.LASF295:
	.string	"queue"
.LASF423:
	.string	"__assert_fail"
.LASF94:
	.string	"pthread_mutex_t"
.LASF267:
	.string	"UV_CHECK"
.LASF69:
	.string	"__lock"
.LASF331:
	.string	"UV_HANDLE_SHUTTING"
.LASF441:
	.string	"uv__fs_event_close"
.LASF123:
	.string	"in_addr"
.LASF377:
	.string	"entry"
.LASF420:
	.string	"__dest"
.LASF196:
	.string	"UV_EAI_FAMILY"
.LASF289:
	.string	"type"
.LASF335:
	.string	"UV_HANDLE_READING"
.LASF6:
	.string	"unsigned char"
.LASF342:
	.string	"UV_HANDLE_EMULATE_IOCP"
.LASF188:
	.string	"UV_EAFNOSUPPORT"
.LASF387:
	.string	"uv_fs_event_init"
.LASF379:
	.string	"watcher_root"
.LASF135:
	.string	"__tzname"
.LASF421:
	.string	"__src"
.LASF261:
	.string	"UV_ENOTTY"
.LASF23:
	.string	"_IO_write_ptr"
.LASF414:
	.string	"__fd"
.LASF78:
	.string	"__readers"
.LASF275:
	.string	"UV_PROCESS"
.LASF304:
	.string	"tree_entry"
.LASF406:
	.string	"left"
.LASF392:
	.string	"tmp_path"
.LASF431:
	.string	"uv__free"
.LASF139:
	.string	"daylight"
.LASF330:
	.string	"UV_HANDLE_CONNECTION"
.LASF359:
	.string	"UV_HANDLE_TTY_SAVED_POSITION"
.LASF350:
	.string	"UV_HANDLE_TCP_SOCKET_CLOSED"
.LASF74:
	.string	"__spins"
.LASF15:
	.string	"__off_t"
.LASF293:
	.string	"uv_async_s"
.LASF292:
	.string	"uv_async_t"
.LASF8:
	.string	"signed char"
.LASF99:
	.string	"sa_family"
.LASF389:
	.string	"old_watchers"
.LASF194:
	.string	"UV_EAI_CANCELED"
.LASF211:
	.string	"UV_ECONNRESET"
.LASF165:
	.string	"async_io_watcher"
.LASF383:
	.string	"handle"
.LASF419:
	.string	"memcpy"
.LASF159:
	.string	"process_handles"
.LASF58:
	.string	"_sys_errlist"
.LASF147:
	.string	"stop_flag"
.LASF405:
	.string	"no_insert"
.LASF170:
	.string	"signal_pipefd"
.LASF351:
	.string	"UV_HANDLE_SHARED_TCP_SOCKET"
.LASF311:
	.string	"reserved"
.LASF162:
	.string	"idle_handles"
.LASF171:
	.string	"signal_io_watcher"
.LASF250:
	.string	"UV_ESPIPE"
.LASF312:
	.string	"double"
.LASF358:
	.string	"UV_HANDLE_TTY_RAW"
.LASF329:
	.string	"UV_HANDLE_LISTENING"
.LASF93:
	.string	"__align"
.LASF302:
	.string	"signal_cb"
.LASF31:
	.string	"_chain"
.LASF416:
	.string	"__nbytes"
.LASF142:
	.string	"uv_loop_s"
.LASF285:
	.string	"uv_loop_t"
.LASF160:
	.string	"prepare_handles"
.LASF407:
	.string	"maybe_free_watcher_list"
.LASF33:
	.string	"_flags2"
.LASF400:
	.string	"comp"
.LASF92:
	.string	"__size"
.LASF435:
	.string	"uv__malloc"
.LASF110:
	.string	"sockaddr_in6"
.LASF232:
	.string	"UV_ENOMEM"
.LASF408:
	.string	"watcher_root_RB_REMOVE_COLOR"
.LASF259:
	.string	"UV_EHOSTDOWN"
.LASF16:
	.string	"__off64_t"
.LASF46:
	.string	"_unused2"
.LASF68:
	.string	"__pthread_mutex_s"
.LASF218:
	.string	"UV_EINVAL"
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
