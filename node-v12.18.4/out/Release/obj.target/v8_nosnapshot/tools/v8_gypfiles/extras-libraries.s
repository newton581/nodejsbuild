	.file	"extras-libraries.cc"
	.text
	.section	.text._ZN2v88internal17NativesCollectionILNS0_10NativeTypeE0EE16GetBuiltinsCountEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17NativesCollectionILNS0_10NativeTypeE0EE16GetBuiltinsCountEv
	.type	_ZN2v88internal17NativesCollectionILNS0_10NativeTypeE0EE16GetBuiltinsCountEv, @function
_ZN2v88internal17NativesCollectionILNS0_10NativeTypeE0EE16GetBuiltinsCountEv:
.LFB5320:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE5320:
	.size	_ZN2v88internal17NativesCollectionILNS0_10NativeTypeE0EE16GetBuiltinsCountEv, .-_ZN2v88internal17NativesCollectionILNS0_10NativeTypeE0EE16GetBuiltinsCountEv
	.section	.rodata._ZN2v88internal17NativesCollectionILNS0_10NativeTypeE0EE8GetIndexEPKc.str1.1,"aMS",@progbits,1
.LC0:
	.string	"dummy"
	.section	.text._ZN2v88internal17NativesCollectionILNS0_10NativeTypeE0EE8GetIndexEPKc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17NativesCollectionILNS0_10NativeTypeE0EE8GetIndexEPKc
	.type	_ZN2v88internal17NativesCollectionILNS0_10NativeTypeE0EE8GetIndexEPKc, @function
_ZN2v88internal17NativesCollectionILNS0_10NativeTypeE0EE8GetIndexEPKc:
.LFB5321:
	.cfi_startproc
	endbr64
	movq	%rdi, %rsi
	movl	$6, %ecx
	leaq	.LC0(%rip), %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	setne	%al
	movzbl	%al, %eax
	negl	%eax
	ret
	.cfi_endproc
.LFE5321:
	.size	_ZN2v88internal17NativesCollectionILNS0_10NativeTypeE0EE8GetIndexEPKc, .-_ZN2v88internal17NativesCollectionILNS0_10NativeTypeE0EE8GetIndexEPKc
	.section	.rodata._ZN2v88internal17NativesCollectionILNS0_10NativeTypeE0EE15GetScriptSourceEi.str1.1,"aMS",@progbits,1
.LC1:
	.string	""
	.section	.text._ZN2v88internal17NativesCollectionILNS0_10NativeTypeE0EE15GetScriptSourceEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17NativesCollectionILNS0_10NativeTypeE0EE15GetScriptSourceEi
	.type	_ZN2v88internal17NativesCollectionILNS0_10NativeTypeE0EE15GetScriptSourceEi, @function
_ZN2v88internal17NativesCollectionILNS0_10NativeTypeE0EE15GetScriptSourceEi:
.LFB5322:
	.cfi_startproc
	endbr64
	cmpl	$1, %edi
	leaq	.LC1(%rip), %rcx
	leaq	_ZN2v88internalL7sourcesE(%rip), %rax
	sbbq	%rdx, %rdx
	andl	$15, %edx
	testl	%edi, %edi
	cmovne	%rcx, %rax
	ret
	.cfi_endproc
.LFE5322:
	.size	_ZN2v88internal17NativesCollectionILNS0_10NativeTypeE0EE15GetScriptSourceEi, .-_ZN2v88internal17NativesCollectionILNS0_10NativeTypeE0EE15GetScriptSourceEi
	.section	.rodata._ZN2v88internal17NativesCollectionILNS0_10NativeTypeE0EE13GetScriptNameEi.str1.1,"aMS",@progbits,1
.LC2:
	.string	"native dummy.js"
	.section	.text._ZN2v88internal17NativesCollectionILNS0_10NativeTypeE0EE13GetScriptNameEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17NativesCollectionILNS0_10NativeTypeE0EE13GetScriptNameEi
	.type	_ZN2v88internal17NativesCollectionILNS0_10NativeTypeE0EE13GetScriptNameEi, @function
_ZN2v88internal17NativesCollectionILNS0_10NativeTypeE0EE13GetScriptNameEi:
.LFB5323:
	.cfi_startproc
	endbr64
	cmpl	$1, %edi
	leaq	.LC1(%rip), %rcx
	leaq	.LC2(%rip), %rax
	sbbq	%rdx, %rdx
	andl	$15, %edx
	testl	%edi, %edi
	cmovne	%rcx, %rax
	ret
	.cfi_endproc
.LFE5323:
	.size	_ZN2v88internal17NativesCollectionILNS0_10NativeTypeE0EE13GetScriptNameEi, .-_ZN2v88internal17NativesCollectionILNS0_10NativeTypeE0EE13GetScriptNameEi
	.section	.text._ZN2v88internal17NativesCollectionILNS0_10NativeTypeE0EE16GetScriptsSourceEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17NativesCollectionILNS0_10NativeTypeE0EE16GetScriptsSourceEv
	.type	_ZN2v88internal17NativesCollectionILNS0_10NativeTypeE0EE16GetScriptsSourceEv, @function
_ZN2v88internal17NativesCollectionILNS0_10NativeTypeE0EE16GetScriptsSourceEv:
.LFB5324:
	.cfi_startproc
	endbr64
	leaq	_ZN2v88internalL7sourcesE(%rip), %rax
	movl	$15, %edx
	ret
	.cfi_endproc
.LFE5324:
	.size	_ZN2v88internal17NativesCollectionILNS0_10NativeTypeE0EE16GetScriptsSourceEv, .-_ZN2v88internal17NativesCollectionILNS0_10NativeTypeE0EE16GetScriptsSourceEv
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal17NativesCollectionILNS0_10NativeTypeE0EE16GetBuiltinsCountEv,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal17NativesCollectionILNS0_10NativeTypeE0EE16GetBuiltinsCountEv, @function
_GLOBAL__sub_I__ZN2v88internal17NativesCollectionILNS0_10NativeTypeE0EE16GetBuiltinsCountEv:
.LFB6130:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE6130:
	.size	_GLOBAL__sub_I__ZN2v88internal17NativesCollectionILNS0_10NativeTypeE0EE16GetBuiltinsCountEv, .-_GLOBAL__sub_I__ZN2v88internal17NativesCollectionILNS0_10NativeTypeE0EE16GetBuiltinsCountEv
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal17NativesCollectionILNS0_10NativeTypeE0EE16GetBuiltinsCountEv
	.section	.rodata._ZN2v88internalL7sourcesE,"a"
	.align 8
	.type	_ZN2v88internalL7sourcesE, @object
	.size	_ZN2v88internalL7sourcesE, 15
_ZN2v88internalL7sourcesE:
	.ascii	"(function() {})"
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
