	.file	"nseq.c"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"%s: Use -help for summary.\n"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"%s: Error reading certs file %s\n"
	.align 8
.LC2:
	.string	"%s: Error reading sequence file %s\n"
	.text
	.p2align 4
	.globl	nseq_main
	.type	nseq_main, @function
nseq_main:
.LFB1435:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	nseq_options(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	xorl	%r14d, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	.L5(%rip), %rbx
	subq	$24, %rsp
	call	opt_init@PLT
	movq	%rax, %r13
.L2:
	call	opt_next@PLT
	testl	%eax, %eax
	je	.L30
.L11:
	addl	$1, %eax
	cmpl	$5, %eax
	ja	.L2
	movslq	(%rbx,%rax,4), %rax
	addq	%rbx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L5:
	.long	.L9-.L5
	.long	.L2-.L5
	.long	.L8-.L5
	.long	.L7-.L5
	.long	.L6-.L5
	.long	.L4-.L5
	.text
.L7:
	movl	$1, %r12d
	call	opt_next@PLT
	testl	%eax, %eax
	jne	.L11
.L30:
	call	opt_num_rest@PLT
	testl	%eax, %eax
	jne	.L9
	movl	$32773, %edx
	movl	$114, %esi
	movq	%r14, %rdi
	call	bio_open_default@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L18
	movl	$32773, %edx
	movl	$119, %esi
	movq	%r15, %rdi
	movq	%rax, -56(%rbp)
	call	bio_open_default@PLT
	movq	-56(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.L19
	testl	%r12d, %r12d
	je	.L12
	call	NETSCAPE_CERT_SEQUENCE_new@PLT
	movq	-56(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %r15
	je	.L10
	call	OPENSSL_sk_new_null@PLT
	movq	-56(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, 8(%r15)
	jne	.L13
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L14:
	movq	8(%r15), %rdi
	movq	%r8, -56(%rbp)
	call	OPENSSL_sk_push@PLT
	movq	-56(%rbp), %r8
.L13:
	xorl	%esi, %esi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r8, %rdi
	movq	%r8, -56(%rbp)
	call	PEM_read_bio_X509@PLT
	movq	-56(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %rsi
	jne	.L14
	movq	8(%r15), %rdi
	call	OPENSSL_sk_num@PLT
	movq	-56(%rbp), %r8
	testl	%eax, %eax
	jne	.L15
	movq	bio_err(%rip), %rdi
	movq	%r14, %rcx
	movq	%r13, %rdx
	leaq	.LC1(%rip), %rsi
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	movq	-56(%rbp), %r8
	jmp	.L10
.L9:
	movq	bio_err(%rip), %rdi
	movq	%r13, %rdx
	xorl	%eax, %eax
	xorl	%r15d, %r15d
	leaq	.LC0(%rip), %rsi
	movl	$1, %r12d
	xorl	%ebx, %ebx
	call	BIO_printf@PLT
	xorl	%r8d, %r8d
.L10:
	movq	%r8, %rdi
	call	BIO_free@PLT
	movq	%rbx, %rdi
	call	BIO_free_all@PLT
	movq	%r15, %rdi
	call	NETSCAPE_CERT_SEQUENCE_free@PLT
	addq	$24, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L4:
	.cfi_restore_state
	call	opt_arg@PLT
	movq	%rax, %r15
	jmp	.L2
.L6:
	call	opt_arg@PLT
	movq	%rax, %r14
	jmp	.L2
.L8:
	leaq	nseq_options(%rip), %rdi
	xorl	%r12d, %r12d
	xorl	%r15d, %r15d
	xorl	%ebx, %ebx
	call	opt_help@PLT
	xorl	%r8d, %r8d
	jmp	.L10
.L18:
	xorl	%r15d, %r15d
	xorl	%ebx, %ebx
	movl	$1, %r12d
	jmp	.L10
.L12:
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	movq	%r8, %rdi
	movq	%r8, -56(%rbp)
	call	PEM_read_bio_NETSCAPE_CERT_SEQUENCE@PLT
	xorl	%edx, %edx
	movq	-56(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %r15
	jne	.L16
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L17:
	movq	8(%r15), %rdi
	movl	%edx, %esi
	movq	%r8, -64(%rbp)
	movl	%edx, -56(%rbp)
	call	OPENSSL_sk_value@PLT
	movq	%rbx, %rdi
	movq	%rax, %r13
	movq	%rax, %rsi
	call	dump_cert_text@PLT
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	PEM_write_bio_X509@PLT
	movl	-56(%rbp), %edx
	movq	-64(%rbp), %r8
	addl	$1, %edx
.L16:
	movq	8(%r15), %rdi
	movl	%edx, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	OPENSSL_sk_num@PLT
	movl	-64(%rbp), %edx
	movq	-56(%rbp), %r8
	cmpl	%eax, %edx
	jl	.L17
	jmp	.L10
.L19:
	xorl	%r15d, %r15d
	movl	$1, %r12d
	jmp	.L10
.L15:
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	%r8, -56(%rbp)
	xorl	%r12d, %r12d
	call	PEM_write_bio_NETSCAPE_CERT_SEQUENCE@PLT
	movq	-56(%rbp), %r8
	jmp	.L10
.L31:
	movq	bio_err(%rip), %rdi
	movq	%r14, %rcx
	movq	%r13, %rdx
	xorl	%eax, %eax
	leaq	.LC2(%rip), %rsi
	movl	$1, %r12d
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	movq	-56(%rbp), %r8
	jmp	.L10
	.cfi_endproc
.LFE1435:
	.size	nseq_main, .-nseq_main
	.globl	nseq_options
	.section	.rodata.str1.1
.LC3:
	.string	"help"
.LC4:
	.string	"Display this summary"
.LC5:
	.string	"toseq"
.LC6:
	.string	"Output NS Sequence file"
.LC7:
	.string	"in"
.LC8:
	.string	"Input file"
.LC9:
	.string	"out"
.LC10:
	.string	"Output file"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	nseq_options, @object
	.size	nseq_options, 120
nseq_options:
	.quad	.LC3
	.long	1
	.long	45
	.quad	.LC4
	.quad	.LC5
	.long	2
	.long	45
	.quad	.LC6
	.quad	.LC7
	.long	3
	.long	60
	.quad	.LC8
	.quad	.LC9
	.long	4
	.long	62
	.quad	.LC10
	.quad	0
	.zero	16
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
