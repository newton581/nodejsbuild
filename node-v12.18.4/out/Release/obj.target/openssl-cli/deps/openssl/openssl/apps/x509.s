	.file	"x509.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"error with certificate to be certified - should be self signed\n"
	.align 8
.LC1:
	.string	"error with certificate - error %d at depth %d\n%s\n"
	.text
	.p2align 4
	.type	callb, @function
callb:
.LFB1438:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	$1, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movl	%edi, %ebx
	movq	%rsi, %rdi
	call	X509_STORE_CTX_get_error@PLT
	cmpl	$18, %eax
	je	.L1
	testl	%ebx, %ebx
	je	.L3
	movq	bio_err(%rip), %rdi
	leaq	.LC0(%rip), %rsi
	xorl	%eax, %eax
	xorl	%r14d, %r14d
	call	BIO_printf@PLT
.L1:
	popq	%rbx
	movl	%r14d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	.cfi_restore_state
	movq	%r13, %rdi
	movl	%eax, %r12d
	call	X509_STORE_CTX_get_current_cert@PLT
	movq	%rax, %rdi
	call	X509_get_subject_name@PLT
	movq	bio_err(%rip), %rdi
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	movq	%rax, %rdx
	call	print_name@PLT
	movslq	%r12d, %rdi
	call	X509_verify_cert_error_string@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	X509_STORE_CTX_get_error_depth@PLT
	movq	bio_err(%rip), %rdi
	movq	%rbx, %r8
	movl	%r12d, %edx
	movl	%eax, %ecx
	leaq	.LC1(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	popq	%rbx
	movl	%r14d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1438:
	.size	callb, .-callb
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC2:
	.string	"default"
.LC3:
	.string	"%s: Use -help for summary.\n"
.LC4:
	.string	"Serial number supplied twice\n"
	.section	.rodata.str1.8
	.align 8
.LC5:
	.string	"%s: Invalid trust object value %s\n"
	.align 8
.LC6:
	.string	"%s: Invalid reject object value %s\n"
	.section	.rodata.str1.1
.LC7:
	.string	"%s: Unknown parameter %s\n"
.LC8:
	.string	"Error getting password\n"
.LC9:
	.string	"Forced key"
	.section	.rodata.str1.8
	.align 8
.LC10:
	.string	"need to specify a CAkey if using the CA command\n"
	.section	.rodata.str1.1
.LC11:
	.string	"extensions"
	.section	.rodata.str1.8
	.align 8
.LC12:
	.string	"Error Loading extension section %s\n"
	.align 8
.LC13:
	.string	"We need a private key to sign with\n"
	.section	.rodata.str1.1
.LC14:
	.string	"error unpacking public key\n"
.LC15:
	.string	"Signature verification error\n"
	.section	.rodata.str1.8
	.align 8
.LC16:
	.string	"Signature did not match the certificate request\n"
	.section	.rodata.str1.1
.LC17:
	.string	"Signature ok\n"
.LC18:
	.string	"subject="
.LC19:
	.string	"Certificate"
.LC20:
	.string	"CA Certificate"
.LC21:
	.string	"SET x509v3 extension 3"
.LC22:
	.string	"SET.ex3"
.LC23:
	.string	"2.99999.3"
.LC24:
	.string	"issuer="
.LC25:
	.string	"serial="
.LC26:
	.string	"\n"
.LC27:
	.string	"%s\n"
.LC28:
	.string	"<No Alias>\n"
.LC29:
	.string	"%08lx\n"
.LC30:
	.string	"Certificate purposes:\n"
.LC31:
	.string	""
.LC32:
	.string	"%s%s : "
.LC33:
	.string	"Yes (WARNING code=%d)\n"
.LC34:
	.string	"No\n"
.LC35:
	.string	"Yes\n"
.LC36:
	.string	" CA"
.LC37:
	.string	"Modulus=unavailable\n"
.LC38:
	.string	"Modulus="
.LC39:
	.string	"Wrong Algorithm type"
.LC40:
	.string	"Error getting public key\n"
.LC41:
	.string	"/*\n * Subject: "
.LC42:
	.string	" * Issuer:  "
.LC43:
	.string	" */\n"
.LC44:
	.string	"x509 name buffer"
.LC45:
	.string	"the_subject_name"
.LC46:
	.string	"the_public_key"
.LC47:
	.string	"the_certificate"
	.section	.rodata.str1.8
	.align 8
.LC48:
	.string	"../deps/openssl/openssl/apps/x509.c"
	.section	.rodata.str1.1
.LC49:
	.string	"notBefore="
.LC50:
	.string	"notAfter="
.LC51:
	.string	"out of memory\n"
.LC52:
	.string	"%s Fingerprint="
.LC53:
	.string	"%02X%c"
.LC54:
	.string	"Getting Private key\n"
.LC55:
	.string	"Private key"
.LC56:
	.string	"Getting CA Private Key\n"
.LC57:
	.string	"CA Private Key"
	.section	.rodata.str1.8
	.align 8
.LC58:
	.string	"Error obtaining CA X509 public key\n"
	.align 8
.LC59:
	.string	"Error initialising X509 store\n"
	.section	.rodata.str1.1
.LC60:
	.string	"serial# buffer"
.LC61:
	.string	"add_word failure\n"
	.section	.rodata.str1.8
	.align 8
.LC62:
	.string	"CA certificate and CA private key do not match\n"
	.section	.rodata.str1.1
.LC63:
	.string	"Getting request Private Key\n"
	.section	.rodata.str1.8
	.align 8
.LC64:
	.string	"no request key file specified\n"
	.section	.rodata.str1.1
.LC65:
	.string	"request key"
	.section	.rodata.str1.8
	.align 8
.LC66:
	.string	"Generating certificate request\n"
	.section	.rodata.str1.1
.LC67:
	.string	"No extensions in certificate\n"
.LC68:
	.string	"Invalid extension names: %s\n"
.LC69:
	.string	"UNDEF"
	.section	.rodata.str1.8
	.align 8
.LC70:
	.string	"No extensions matched with %s\n"
	.section	.rodata.str1.1
.LC71:
	.string	"Certificate will expire\n"
.LC72:
	.string	"Certificate will not expire\n"
	.section	.rodata.str1.8
	.align 8
.LC73:
	.string	"bad output format specified for outfile\n"
	.section	.rodata.str1.1
.LC74:
	.string	"unable to write certificate\n"
	.text
	.p2align 4
	.globl	x509_main
	.type	x509_main, @function
x509_main:
.LFB1435:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%edi, %r12d
	pushq	%rbx
	subq	$584, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -216(%rbp)
	movq	$0, -208(%rbp)
	movl	$32773, -236(%rbp)
	movl	$32773, -232(%rbp)
	movl	$32773, -228(%rbp)
	movl	$32773, -224(%rbp)
	movl	$32773, -220(%rbp)
	movq	$0, -200(%rbp)
	call	X509_STORE_new@PLT
	movq	%rax, -344(%rbp)
	testq	%rax, %rax
	je	.L244
	movq	%rax, %rdi
	leaq	callb(%rip), %rsi
	xorl	%r15d, %r15d
	xorl	%r14d, %r14d
	call	X509_STORE_set_verify_cb@PLT
	movq	%r13, %rsi
	movl	%r12d, %edi
	leaq	x509_options(%rip), %rdx
	call	opt_init@PLT
	xorl	%r13d, %r13d
	leaq	.L15(%rip), %rbx
	movl	$0, -444(%rbp)
	movl	$0, -428(%rbp)
	movq	%rax, %r12
	movq	$0, -328(%rbp)
	movl	$0, -360(%rbp)
	movq	$0, -584(%rbp)
	movl	$0, -364(%rbp)
	movl	$0, -508(%rbp)
	movl	$0, -368(%rbp)
	movl	$0, -372(%rbp)
	movl	$0, -376(%rbp)
	movl	$0, -380(%rbp)
	movl	$0, -448(%rbp)
	movl	$0, -504(%rbp)
	movl	$0, -576(%rbp)
	movl	$0, -416(%rbp)
	movl	$0, -568(%rbp)
	movl	$0, -560(%rbp)
	movl	$0, -352(%rbp)
	movl	$0, -412(%rbp)
	movl	$0, -388(%rbp)
	movl	$0, -552(%rbp)
	movl	$0, -392(%rbp)
	movl	$0, -476(%rbp)
	movl	$0, -436(%rbp)
	movl	$0, -548(%rbp)
	movl	$0, -440(%rbp)
	movl	$0, -432(%rbp)
	movl	$0, -384(%rbp)
	movl	$0, -500(%rbp)
	movl	$0, -512(%rbp)
	movl	$0, -496(%rbp)
	movl	$0, -472(%rbp)
	movl	$0, -492(%rbp)
	movl	$0, -468(%rbp)
	movl	$30, -356(%rbp)
	movl	$0, -480(%rbp)
	movq	$0, -424(%rbp)
	movq	$0, -464(%rbp)
	movq	$0, -544(%rbp)
	movq	$0, -304(%rbp)
	movq	$0, -296(%rbp)
	movq	$0, -248(%rbp)
	movq	$0, -400(%rbp)
	movq	$0, -456(%rbp)
	movq	$0, -536(%rbp)
	movq	$0, -528(%rbp)
	movq	$0, -520(%rbp)
	movq	$0, -320(%rbp)
	movq	$0, -256(%rbp)
	movq	$0, -488(%rbp)
	movq	$0, -408(%rbp)
	movq	$0, -288(%rbp)
	movq	$0, -280(%rbp)
	movq	$0, -272(%rbp)
	movq	$0, -264(%rbp)
.L10:
	call	opt_next@PLT
	testl	%eax, %eax
	je	.L432
.L87:
	cmpl	$62, %eax
	jg	.L11
	cmpl	$-1, %eax
	jl	.L10
	addl	$1, %eax
	cmpl	$63, %eax
	ja	.L10
	movslq	(%rbx,%rax,4), %rax
	addq	%rbx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L15:
	.long	.L76-.L15
	.long	.L10-.L15
	.long	.L75-.L15
	.long	.L74-.L15
	.long	.L73-.L15
	.long	.L72-.L15
	.long	.L71-.L15
	.long	.L70-.L15
	.long	.L69-.L15
	.long	.L68-.L15
	.long	.L67-.L15
	.long	.L66-.L15
	.long	.L65-.L15
	.long	.L64-.L15
	.long	.L63-.L15
	.long	.L62-.L15
	.long	.L61-.L15
	.long	.L60-.L15
	.long	.L59-.L15
	.long	.L58-.L15
	.long	.L57-.L15
	.long	.L56-.L15
	.long	.L55-.L15
	.long	.L54-.L15
	.long	.L53-.L15
	.long	.L52-.L15
	.long	.L51-.L15
	.long	.L50-.L15
	.long	.L49-.L15
	.long	.L48-.L15
	.long	.L47-.L15
	.long	.L46-.L15
	.long	.L45-.L15
	.long	.L44-.L15
	.long	.L43-.L15
	.long	.L42-.L15
	.long	.L41-.L15
	.long	.L40-.L15
	.long	.L39-.L15
	.long	.L38-.L15
	.long	.L37-.L15
	.long	.L36-.L15
	.long	.L35-.L15
	.long	.L34-.L15
	.long	.L33-.L15
	.long	.L32-.L15
	.long	.L31-.L15
	.long	.L30-.L15
	.long	.L29-.L15
	.long	.L28-.L15
	.long	.L27-.L15
	.long	.L26-.L15
	.long	.L25-.L15
	.long	.L24-.L15
	.long	.L23-.L15
	.long	.L22-.L15
	.long	.L21-.L15
	.long	.L20-.L15
	.long	.L19-.L15
	.long	.L18-.L15
	.long	.L17-.L15
	.long	.L16-.L15
	.long	.L245-.L15
	.long	.L14-.L15
	.text
.L245:
	movl	$1, %r15d
	call	opt_next@PLT
	testl	%eax, %eax
	jne	.L87
.L432:
	call	opt_num_rest@PLT
	movl	%eax, %ebx
	call	opt_rest@PLT
	testl	%ebx, %ebx
	jne	.L433
	movq	-296(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	leaq	-208(%rbp), %rdx
	call	app_passwd@PLT
	testl	%eax, %eax
	je	.L434
	movq	-344(%rbp), %rdi
	call	X509_STORE_set_default_paths@PLT
	testl	%eax, %eax
	je	.L435
	movq	-256(%rbp), %rax
	testq	%rax, %rax
	je	.L91
	movq	-328(%rbp), %r8
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rdi
	movl	-220(%rbp), %esi
	leaq	.LC9(%rip), %r9
	call	load_pubkey@PLT
	movq	%rax, -256(%rbp)
	testq	%rax, %rax
	jne	.L91
	movq	$0, -320(%rbp)
	xorl	%r12d, %r12d
	xorl	%ebx, %ebx
	xorl	%r9d, %r9d
	movq	$0, -336(%rbp)
	movl	$1, %r14d
	movq	$0, -312(%rbp)
	movq	$0, -304(%rbp)
	movq	$0, -296(%rbp)
	movq	$0, -248(%rbp)
	jmp	.L9
.L71:
	movl	$1, %r14d
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L11:
	cmpl	$1502, %eax
	jg	.L77
	cmpl	$1500, %eax
	jle	.L10
	movl	%eax, %edi
	call	opt_rand@PLT
	testl	%eax, %eax
	jne	.L10
	movl	$1, %r14d
	jmp	.L424
	.p2align 4,,10
	.p2align 3
.L77:
	cmpl	$1504, %eax
	jne	.L10
	addl	$1, %r13d
	movl	%r13d, -508(%rbp)
	call	opt_arg@PLT
	movq	%rax, -456(%rbp)
	jmp	.L10
.L441:
	call	opt_arg@PLT
	movq	bio_err(%rip), %rdi
	movq	%r12, %rdx
	leaq	.LC6(%rip), %rsi
	movq	%rax, %rcx
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L76:
	movq	bio_err(%rip), %rdi
	movq	%r12, %rdx
	leaq	.LC3(%rip), %rsi
	xorl	%eax, %eax
	movl	$1, %r14d
	call	BIO_printf@PLT
.L424:
	movq	$0, -320(%rbp)
	xorl	%r12d, %r12d
	movq	$0, -336(%rbp)
	movq	$0, -312(%rbp)
	movq	$0, -256(%rbp)
.L427:
	movq	$0, -304(%rbp)
	xorl	%ebx, %ebx
	xorl	%r9d, %r9d
	movq	$0, -296(%rbp)
	movq	$0, -248(%rbp)
.L9:
	movq	-248(%rbp), %rdi
	movq	%r9, -352(%rbp)
	call	NCONF_free@PLT
	movq	%rbx, %rdi
	call	BIO_free_all@PLT
	movq	-344(%rbp), %rdi
	call	X509_STORE_free@PLT
	movq	-336(%rbp), %rdi
	call	X509_REQ_free@PLT
	movq	%r12, %rdi
	call	X509_free@PLT
	movq	-312(%rbp), %rdi
	call	X509_free@PLT
	movq	-296(%rbp), %rdi
	call	EVP_PKEY_free@PLT
	movq	-304(%rbp), %rdi
	call	EVP_PKEY_free@PLT
	movq	-256(%rbp), %rdi
	call	EVP_PKEY_free@PLT
	movq	-288(%rbp), %rdi
	call	OPENSSL_sk_free@PLT
	movq	-320(%rbp), %rdi
	call	X509_REQ_free@PLT
	movq	-264(%rbp), %rdi
	call	ASN1_INTEGER_free@PLT
	movq	ASN1_OBJECT_free@GOTPCREL(%rip), %r12
	movq	-272(%rbp), %rdi
	movq	%r12, %rsi
	call	OPENSSL_sk_pop_free@PLT
	movq	-280(%rbp), %rdi
	movq	%r12, %rsi
	call	OPENSSL_sk_pop_free@PLT
	movq	-352(%rbp), %r9
	movq	%r9, %rdi
	call	ASN1_OBJECT_free@PLT
	movq	-328(%rbp), %rdi
	call	release_engine@PLT
	movq	-208(%rbp), %rdi
	movl	$907, %edx
	leaq	.LC48(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L436
	addq	$584, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L244:
	.cfi_restore_state
	movq	$0, -328(%rbp)
	xorl	%r12d, %r12d
	xorl	%ebx, %ebx
	xorl	%r9d, %r9d
	movq	$0, -320(%rbp)
	movl	$1, %r14d
	movq	$0, -336(%rbp)
	movq	$0, -312(%rbp)
	movq	$0, -288(%rbp)
	movq	$0, -280(%rbp)
	movq	$0, -272(%rbp)
	movq	$0, -256(%rbp)
	movq	$0, -304(%rbp)
	movq	$0, -296(%rbp)
	movq	$0, -248(%rbp)
	movq	$0, -264(%rbp)
	jmp	.L9
.L14:
	cmpl	$30, -356(%rbp)
	jne	.L76
	movl	$1, -360(%rbp)
	jmp	.L10
.L75:
	leaq	x509_options(%rip), %rdi
	xorl	%r14d, %r14d
	call	opt_help@PLT
	jmp	.L424
.L74:
	call	opt_arg@PLT
	leaq	-228(%rbp), %rdx
	movl	$1982, %esi
	movq	%rax, %rdi
	call	opt_format@PLT
	testl	%eax, %eax
	jne	.L10
	jmp	.L76
.L73:
	call	opt_arg@PLT
	leaq	-224(%rbp), %rdx
	movl	$1982, %esi
	movq	%rax, %rdi
	call	opt_format@PLT
	testl	%eax, %eax
	jne	.L10
	jmp	.L76
.L72:
	call	opt_arg@PLT
	leaq	-220(%rbp), %rdx
	movl	$18, %esi
	movq	%rax, %rdi
	call	opt_format@PLT
	testl	%eax, %eax
	jne	.L10
	jmp	.L76
.L70:
	call	opt_arg@PLT
	leaq	-236(%rbp), %rdx
	movl	$2, %esi
	movq	%rax, %rdi
	call	opt_format@PLT
	testl	%eax, %eax
	jne	.L10
	jmp	.L76
.L69:
	call	opt_arg@PLT
	leaq	-232(%rbp), %rdx
	movl	$18, %esi
	movq	%rax, %rdi
	call	opt_format@PLT
	testl	%eax, %eax
	jne	.L10
	jmp	.L76
.L68:
	cmpq	$0, -288(%rbp)
	je	.L437
.L80:
	call	opt_arg@PLT
	movq	-288(%rbp), %rdi
	movq	%rax, %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	jne	.L10
	jmp	.L76
.L67:
	movl	-360(%rbp), %eax
	testl	%eax, %eax
	jne	.L76
	call	opt_arg@PLT
	movl	$10, %edx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	strtol@PLT
	movl	%eax, -356(%rbp)
	jmp	.L10
.L66:
	call	opt_arg@PLT
	movq	%rax, -296(%rbp)
	jmp	.L10
.L65:
	call	opt_arg@PLT
	movq	%rax, -248(%rbp)
	jmp	.L10
.L64:
	call	opt_arg@PLT
	movq	%rax, -400(%rbp)
	jmp	.L10
.L63:
	call	opt_arg@PLT
	movq	%rax, -304(%rbp)
	jmp	.L10
.L62:
	call	opt_arg@PLT
	movq	%rax, -544(%rbp)
	jmp	.L10
.L61:
	call	opt_arg@PLT
	movq	%rax, -464(%rbp)
	leal	1(%r13), %eax
	movl	%eax, -476(%rbp)
	movl	%eax, %r13d
	jmp	.L10
.L60:
	call	opt_arg@PLT
	movq	%rax, -424(%rbp)
	leal	1(%r13), %eax
	movl	%eax, -392(%rbp)
	movl	%eax, %r13d
	jmp	.L10
.L59:
	call	opt_arg@PLT
	movq	%rax, -408(%rbp)
	jmp	.L10
.L58:
	call	opt_arg@PLT
	movq	%rax, -488(%rbp)
	jmp	.L10
.L57:
	cmpq	$0, -264(%rbp)
	jne	.L438
	call	opt_arg@PLT
	xorl	%edi, %edi
	movq	%rax, %rsi
	call	s2i_ASN1_INTEGER@PLT
	movq	%rax, -264(%rbp)
	testq	%rax, %rax
	jne	.L10
	jmp	.L76
.L56:
	call	opt_arg@PLT
	movq	%rax, -256(%rbp)
	jmp	.L10
.L55:
	call	opt_arg@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	OBJ_txt2obj@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L439
	cmpq	$0, -272(%rbp)
	je	.L440
.L83:
	movq	-272(%rbp), %rdi
	call	OPENSSL_sk_push@PLT
	movl	$1, -352(%rbp)
	jmp	.L10
.L54:
	call	opt_arg@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	OBJ_txt2obj@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L441
	cmpq	$0, -280(%rbp)
	je	.L442
.L85:
	movq	-280(%rbp), %rdi
	call	OPENSSL_sk_push@PLT
	movl	$1, -352(%rbp)
	jmp	.L10
.L53:
	call	opt_arg@PLT
	movl	$1, -352(%rbp)
	movq	%rax, -320(%rbp)
	jmp	.L10
.L52:
	call	opt_arg@PLT
	leaq	-200(%rbp), %rdi
	movq	%rax, %rsi
	call	set_cert_ex@PLT
	testl	%eax, %eax
	jne	.L10
	jmp	.L76
.L51:
	call	opt_arg@PLT
	movq	%rax, %rdi
	call	set_nameopt@PLT
	testl	%eax, %eax
	jne	.L10
	jmp	.L76
.L50:
	leal	1(%r13), %eax
	movl	%eax, -496(%rbp)
	movl	%eax, %r13d
	jmp	.L10
.L49:
	leal	1(%r13), %eax
	movl	%eax, -388(%rbp)
	movl	%eax, %r13d
	jmp	.L10
.L48:
	leal	1(%r13), %eax
	movl	%eax, -412(%rbp)
	movl	%eax, %r13d
	jmp	.L10
.L47:
	addl	$1, %r13d
	movl	%r13d, -380(%rbp)
	jmp	.L10
.L46:
	leal	1(%r13), %eax
	movl	%eax, -384(%rbp)
	movl	%eax, %r13d
	jmp	.L10
.L45:
	leal	1(%r13), %eax
	movl	%eax, -468(%rbp)
	movl	%eax, %r13d
	jmp	.L10
.L44:
	leal	1(%r13), %eax
	movl	%eax, -492(%rbp)
	movl	%eax, %r13d
	jmp	.L10
.L43:
	leal	1(%r13), %eax
	movl	%eax, -480(%rbp)
	movl	%eax, %r13d
	jmp	.L10
.L42:
	leal	1(%r13), %eax
	movl	%eax, -448(%rbp)
	movl	%eax, %r13d
	jmp	.L10
.L41:
	leal	1(%r13), %eax
	movl	%eax, -432(%rbp)
	movl	%eax, %r13d
	jmp	.L10
.L40:
	leal	1(%r13), %eax
	movl	%eax, -440(%rbp)
	movl	%eax, %r13d
	jmp	.L10
.L39:
	leal	1(%r13), %eax
	movl	%eax, -376(%rbp)
	movl	%eax, %r13d
	jmp	.L10
.L38:
	leal	1(%r13), %eax
	movl	%eax, -372(%rbp)
	movl	%eax, %r13d
	jmp	.L10
.L37:
	leal	1(%r13), %eax
	movl	%eax, -512(%rbp)
	movl	%eax, %r13d
	jmp	.L10
.L36:
	leal	1(%r13), %eax
	addl	$2, %r13d
	movl	%eax, -368(%rbp)
	movl	%r13d, -364(%rbp)
	jmp	.L10
.L35:
	leal	1(%r13), %eax
	movl	%eax, -472(%rbp)
	movl	%eax, %r13d
	jmp	.L10
.L34:
	leal	1(%r13), %eax
	movl	%eax, -368(%rbp)
	movl	%eax, %r13d
	jmp	.L10
.L33:
	leal	1(%r13), %eax
	movl	%eax, -364(%rbp)
	movl	%eax, %r13d
	jmp	.L10
.L32:
	movq	$0, -192(%rbp)
	call	opt_arg@PLT
	leaq	-192(%rbp), %rsi
	movq	%rax, %rdi
	call	opt_imax@PLT
	testl	%eax, %eax
	je	.L76
	movq	-192(%rbp), %rax
	movl	$1, -500(%rbp)
	movq	%rax, -584(%rbp)
	jmp	.L10
.L31:
	call	opt_arg@PLT
	movq	%rax, -520(%rbp)
	jmp	.L10
.L30:
	call	opt_arg@PLT
	movq	%rax, -528(%rbp)
	jmp	.L10
.L29:
	call	opt_arg@PLT
	movq	%rax, -536(%rbp)
	jmp	.L10
.L28:
	leal	1(%r13), %eax
	movl	%eax, -436(%rbp)
	movl	%eax, %r13d
	jmp	.L10
.L27:
	movl	$1, -352(%rbp)
	jmp	.L10
.L26:
	leal	1(%r13), %eax
	movl	%eax, -560(%rbp)
	movl	%eax, %r13d
	jmp	.L10
.L25:
	leal	1(%r13), %eax
	movl	%eax, -568(%rbp)
	movl	%eax, %r13d
	jmp	.L10
.L24:
	leal	1(%r13), %eax
	movl	%eax, -416(%rbp)
	movl	%eax, %r13d
	jmp	.L10
.L23:
	leal	1(%r13), %eax
	movl	%eax, -552(%rbp)
	movl	%eax, %r13d
	jmp	.L10
.L22:
	movl	$1, -504(%rbp)
	jmp	.L10
.L21:
	leal	1(%r13), %eax
	movl	%eax, -548(%rbp)
	movl	%eax, %r13d
	jmp	.L10
.L20:
	leal	1(%r13), %eax
	movl	%eax, -428(%rbp)
	movl	%eax, %r13d
	jmp	.L10
.L19:
	leal	1(%r13), %eax
	movl	%eax, -444(%rbp)
	movl	%eax, %r13d
	jmp	.L10
.L18:
	movl	$1, -576(%rbp)
	jmp	.L10
.L17:
	call	opt_unknown@PLT
	leaq	-216(%rbp), %rsi
	movq	%rax, %rdi
	call	opt_md@PLT
	testl	%eax, %eax
	jne	.L10
	jmp	.L76
.L16:
	call	opt_arg@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	setup_engine@PLT
	movq	%rax, -328(%rbp)
	jmp	.L10
.L433:
	movq	(%rax), %rcx
	movq	bio_err(%rip), %rdi
	movq	%r12, %rdx
	xorl	%eax, %eax
	leaq	.LC7(%rip), %rsi
	call	BIO_printf@PLT
	jmp	.L76
.L91:
	cmpq	$0, -408(%rbp)
	jne	.L92
	movl	-392(%rbp), %eax
	testl	%eax, %eax
	je	.L92
	cmpl	$32773, -236(%rbp)
	je	.L250
	movq	bio_err(%rip), %rdi
	leaq	.LC10(%rip), %rsi
	xorl	%eax, %eax
	xorl	%r12d, %r12d
	movl	$1, %r14d
	call	BIO_printf@PLT
	movq	$0, -320(%rbp)
	movq	$0, -336(%rbp)
	movq	$0, -312(%rbp)
	jmp	.L427
.L250:
	movq	-424(%rbp), %rax
	movq	%rax, -408(%rbp)
.L92:
	movq	-248(%rbp), %rax
	testq	%rax, %rax
	je	.L93
	movq	%rax, %rdi
	call	app_load_config@PLT
	movq	%rax, -248(%rbp)
	testq	%rax, %rax
	je	.L94
	cmpq	$0, -400(%rbp)
	je	.L443
.L95:
	leaq	-192(%rbp), %r12
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	movl	$1, %r9d
	movq	%r12, %rdi
	call	X509V3_set_ctx@PLT
	movq	-248(%rbp), %rbx
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	X509V3_set_nconf@PLT
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	-400(%rbp), %rdx
	call	X509V3_EXT_add_nconf@PLT
	testl	%eax, %eax
	jne	.L93
	movq	bio_err(%rip), %rdi
	movq	-400(%rbp), %rdx
	leaq	.LC12(%rip), %rsi
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
.L94:
	movl	$1, %r14d
.L428:
	movq	$0, -320(%rbp)
	movq	$0, -336(%rbp)
.L429:
	movq	$0, -312(%rbp)
	xorl	%r12d, %r12d
	xorl	%ebx, %ebx
	xorl	%r9d, %r9d
	movq	$0, -304(%rbp)
	movq	$0, -296(%rbp)
	jmp	.L9
.L93:
	testl	%r14d, %r14d
	je	.L97
	movl	-476(%rbp), %eax
	orl	-392(%rbp), %eax
	je	.L444
	movl	-228(%rbp), %edx
	movq	-304(%rbp), %rdi
	movl	$114, %esi
	call	bio_open_default@PLT
	movq	%rax, %r9
	testq	%rax, %rax
	je	.L251
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%rax, -296(%rbp)
	call	PEM_read_bio_X509_REQ@PLT
	movq	-296(%rbp), %r9
	movq	%rax, %rbx
	movq	%rax, -336(%rbp)
	movq	%r9, %rdi
	call	BIO_free@PLT
	testq	%rbx, %rbx
	je	.L445
	movq	-336(%rbp), %rdi
	call	X509_REQ_get0_pubkey@PLT
	testq	%rax, %rax
	je	.L446
	movq	-336(%rbp), %rdi
	movq	%rax, %rsi
	call	X509_REQ_verify@PLT
	testl	%eax, %eax
	js	.L447
	movq	bio_err(%rip), %rdi
	je	.L448
	leaq	.LC17(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	call	get_nameopt@PLT
	movq	-336(%rbp), %rdi
	movq	%rax, %r12
	call	X509_REQ_get_subject_name@PLT
	movq	bio_err(%rip), %rdi
	movq	%r12, %rcx
	leaq	.LC18(%rip), %rsi
	movq	%rax, %rdx
	call	print_name@PLT
	call	X509_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L259
	cmpq	$0, -264(%rbp)
	je	.L449
	movq	-264(%rbp), %rsi
	movq	%rax, %rdi
	call	X509_set_serialNumber@PLT
	testl	%eax, %eax
	je	.L259
.L104:
	movq	-336(%rbp), %rbx
	movq	%rbx, %rdi
	call	X509_REQ_get_subject_name@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	X509_set_issuer_name@PLT
	testl	%eax, %eax
	je	.L259
	movq	%rbx, %rdi
	call	X509_REQ_get_subject_name@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	X509_set_subject_name@PLT
	testl	%eax, %eax
	je	.L259
	movl	-356(%rbp), %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	set_cert_times@PLT
	testl	%eax, %eax
	je	.L259
	movq	-256(%rbp), %rax
	testq	%rax, %rax
	je	.L105
.L418:
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	X509_set_pubkey@PLT
.L106:
	movl	-392(%rbp), %eax
	testl	%eax, %eax
	je	.L261
	movl	-236(%rbp), %esi
	movq	-424(%rbp), %rdi
	leaq	.LC20(%rip), %rdx
	call	load_cert@PLT
	movq	%rax, -312(%rbp)
	testq	%rax, %rax
	je	.L450
.L107:
	movl	-224(%rbp), %edx
	movq	-544(%rbp), %rdi
	movl	$119, %esi
	call	bio_open_default@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L263
	movl	-384(%rbp), %eax
	orl	-448(%rbp), %eax
	jne	.L282
	movl	-436(%rbp), %eax
	testl	%eax, %eax
	jne	.L108
.L282:
	leaq	.LC21(%rip), %rdx
	leaq	.LC22(%rip), %rsi
	leaq	.LC23(%rip), %rdi
	call	OBJ_create@PLT
.L108:
	movq	-320(%rbp), %rax
	testq	%rax, %rax
	je	.L110
	movl	$-1, %edx
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	X509_alias_set1@PLT
.L110:
	movl	-560(%rbp), %r11d
	testl	%r11d, %r11d
	jne	.L451
.L111:
	movl	-568(%rbp), %r10d
	testl	%r10d, %r10d
	jne	.L452
.L112:
	xorl	%edx, %edx
	cmpq	$0, -272(%rbp)
	je	.L117
	movq	%rbx, -296(%rbp)
	movq	-272(%rbp), %rbx
	movl	%r13d, -304(%rbp)
	movl	%edx, %r13d
	jmp	.L113
.L116:
	movl	%r13d, %esi
	movq	%rbx, %rdi
	addl	$1, %r13d
	call	OPENSSL_sk_value@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	X509_add1_trust_object@PLT
.L113:
	movq	%rbx, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r13d
	jl	.L116
	movq	-296(%rbp), %rbx
	movl	-304(%rbp), %r13d
.L117:
	xorl	%edx, %edx
	cmpq	$0, -280(%rbp)
	je	.L115
	movq	%rbx, -296(%rbp)
	movq	-280(%rbp), %rbx
	movl	%r13d, -304(%rbp)
	movl	%edx, %r13d
	jmp	.L114
.L120:
	movl	%r13d, %esi
	movq	%rbx, %rdi
	addl	$1, %r13d
	call	OPENSSL_sk_value@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	X509_add1_reject_object@PLT
.L114:
	movq	%rbx, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r13d
	jl	.L120
	movq	-296(%rbp), %rbx
	movl	-304(%rbp), %r13d
.L115:
	movl	-576(%rbp), %r9d
	testl	%r9d, %r9d
	jne	.L453
.L119:
	testl	%r13d, %r13d
	je	.L266
	movl	$1, %eax
	movl	%r15d, -620(%rbp)
	movq	%r12, %r15
	movq	$0, -320(%rbp)
	movl	%eax, %r12d
	movq	$0, -304(%rbp)
	movq	$0, -296(%rbp)
	movl	%r13d, -544(%rbp)
	movl	%r14d, -624(%rbp)
	jmp	.L235
	.p2align 4,,10
	.p2align 3
.L122:
	cmpl	%r12d, -376(%rbp)
	je	.L454
	cmpl	%r12d, -380(%rbp)
	je	.L455
	cmpl	%r12d, -384(%rbp)
	je	.L456
	cmpl	%r12d, -388(%rbp)
	je	.L401
	cmpl	%r12d, -412(%rbp)
	je	.L457
	cmpl	%r12d, -416(%rbp)
	je	.L458
	cmpl	%r12d, -432(%rbp)
	je	.L459
	cmpl	%r12d, -428(%rbp)
	je	.L460
	cmpl	%r12d, -440(%rbp)
	je	.L461
	cmpl	%r12d, -444(%rbp)
	je	.L462
	cmpl	%r12d, -472(%rbp)
	je	.L463
	cmpl	%r12d, -468(%rbp)
	je	.L464
	cmpl	%r12d, -492(%rbp)
	je	.L465
	cmpl	%r12d, -496(%rbp)
	je	.L466
	cmpl	%r12d, -448(%rbp)
	je	.L467
	cmpl	%r12d, -368(%rbp)
	je	.L468
	cmpl	%r12d, -364(%rbp)
	je	.L469
	cmpl	%r12d, -512(%rbp)
	je	.L470
	cmpl	%r12d, -476(%rbp)
	jne	.L166
	movl	-480(%rbp), %r8d
	testl	%r8d, %r8d
	jne	.L166
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
	leaq	.LC54(%rip), %rsi
	call	BIO_printf@PLT
	cmpq	$0, -296(%rbp)
	je	.L471
.L167:
	movq	%r15, %rdi
	movq	-216(%rbp), %r13
	call	X509_get_subject_name@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	X509_set_issuer_name@PLT
	testl	%eax, %eax
	je	.L413
	movl	-360(%rbp), %edi
	testl	%edi, %edi
	jne	.L173
	movl	-356(%rbp), %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	set_cert_times@PLT
	testl	%eax, %eax
	je	.L413
.L173:
	movq	-296(%rbp), %rsi
	movq	%r15, %rdi
	call	X509_set_pubkey@PLT
	testl	%eax, %eax
	je	.L413
	movl	-504(%rbp), %esi
	testl	%esi, %esi
	jne	.L174
.L178:
	cmpq	$0, -248(%rbp)
	je	.L176
	leaq	-192(%rbp), %r14
	movl	$2, %esi
	movq	%r15, %rdi
	call	X509_set_version@PLT
	xorl	%ecx, %ecx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r15, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	X509V3_set_ctx@PLT
	movq	-248(%rbp), %rsi
	movq	%r14, %rdi
	call	X509V3_set_nconf@PLT
	movq	-400(%rbp), %rdx
	movq	%r15, %rcx
	movq	%r14, %rsi
	movq	-248(%rbp), %rdi
	call	X509V3_EXT_add_nconf@PLT
	testl	%eax, %eax
	je	.L413
.L176:
	movq	-296(%rbp), %rsi
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	X509_sign@PLT
	testl	%eax, %eax
	je	.L413
	.p2align 4,,10
	.p2align 3
.L123:
	addl	$1, %r12d
	cmpl	%r12d, -544(%rbp)
	jl	.L472
.L235:
	cmpl	%r12d, -372(%rbp)
	jne	.L122
	call	get_nameopt@PLT
	movq	%r15, %rdi
	movq	%rax, %r13
	call	X509_get_issuer_name@PLT
	movq	%r13, %rcx
	leaq	.LC24(%rip), %rsi
	movq	%rbx, %rdi
	movq	%rax, %rdx
	call	print_name@PLT
	jmp	.L123
.L434:
	movq	bio_err(%rip), %rdi
	leaq	.LC8(%rip), %rsi
	movl	$1, %r14d
	call	BIO_printf@PLT
	jmp	.L424
.L440:
	movq	%rax, -312(%rbp)
	call	OPENSSL_sk_new_null@PLT
	movq	-312(%rbp), %rsi
	testq	%rax, %rax
	movq	%rax, -272(%rbp)
	jne	.L83
.L248:
	movq	$0, -320(%rbp)
	movq	%rsi, %r9
	xorl	%r12d, %r12d
	xorl	%ebx, %ebx
	movq	$0, -336(%rbp)
	movl	$1, %r14d
	movq	$0, -312(%rbp)
	movq	$0, -256(%rbp)
	movq	$0, -304(%rbp)
	movq	$0, -296(%rbp)
	movq	$0, -248(%rbp)
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L442:
	movq	%rax, -312(%rbp)
	call	OPENSSL_sk_new_null@PLT
	movq	-312(%rbp), %rsi
	testq	%rax, %rax
	movq	%rax, -280(%rbp)
	jne	.L85
	jmp	.L248
	.p2align 4,,10
	.p2align 3
.L97:
	movl	-228(%rbp), %esi
	movq	-304(%rbp), %rdi
	leaq	.LC19(%rip), %rdx
	call	load_cert@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L260
	movq	$0, -336(%rbp)
	jmp	.L106
.L177:
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	X509_delete_ext@PLT
.L174:
	movq	%r15, %rdi
	call	X509_get_ext_count@PLT
	testl	%eax, %eax
	jg	.L177
	jmp	.L178
	.p2align 4,,10
	.p2align 3
.L437:
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, -288(%rbp)
	testq	%rax, %rax
	jne	.L80
	jmp	.L76
.L435:
	movq	bio_err(%rip), %rdi
	movl	$1, %r14d
	call	ERR_print_errors@PLT
	jmp	.L424
.L454:
	call	get_nameopt@PLT
	movq	%r15, %rdi
	movq	%rax, %r13
	call	X509_get_subject_name@PLT
	movq	%r13, %rcx
	leaq	.LC18(%rip), %rsi
	movq	%rbx, %rdi
	movq	%rax, %rdx
	call	print_name@PLT
	jmp	.L123
.L455:
	leaq	.LC25(%rip), %rsi
	movq	%rbx, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	%r15, %rdi
	call	X509_get_serialNumber@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	i2a_ASN1_INTEGER@PLT
	leaq	.LC26(%rip), %rsi
	movq	%rbx, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L123
.L456:
	movq	%r15, %rdi
	call	X509_get_serialNumber@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	ASN1_INTEGER_to_BN@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L275
	movl	$1, %esi
	movq	%rax, %rdi
	call	BN_add_word@PLT
	testl	%eax, %eax
	je	.L268
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	BN_to_ASN1_INTEGER@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L272
	movq	%r13, %rdi
	call	BN_free@PLT
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	i2a_ASN1_INTEGER@PLT
	movq	%r14, %rdi
	call	ASN1_INTEGER_free@PLT
	leaq	.LC26(%rip), %rsi
	movq	%rbx, %rdi
	call	BIO_puts@PLT
	jmp	.L123
.L457:
	cmpl	%r12d, -388(%rbp)
	je	.L401
	movq	%r15, %rdi
	call	X509_get1_ocsp@PLT
	movq	%rax, %r14
.L130:
	xorl	%r13d, %r13d
	jmp	.L131
	.p2align 4,,10
	.p2align 3
.L132:
	movl	%r13d, %esi
	movq	%r14, %rdi
	addl	$1, %r13d
	call	OPENSSL_sk_value@PLT
	leaq	.LC27(%rip), %rsi
	movq	%rbx, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L131:
	movq	%r14, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r13d
	jl	.L132
	movq	%r14, %rdi
	call	X509_email_free@PLT
	jmp	.L123
.L444:
	movq	bio_err(%rip), %rdi
	leaq	.LC13(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L428
.L166:
	cmpl	%r12d, -392(%rbp)
	je	.L473
	cmpl	%r12d, -480(%rbp)
	je	.L474
	cmpl	%r12d, -548(%rbp)
	je	.L475
	cmpl	%r12d, -508(%rbp)
	jne	.L123
	movq	%r15, %rdi
	call	X509_get0_extensions@PLT
	movq	%rax, %rdi
	movq	%rax, -616(%rbp)
	call	OPENSSL_sk_num@PLT
	movl	%eax, -592(%rbp)
	testl	%eax, %eax
	jle	.L476
	movq	-456(%rbp), %rdi
	movl	$1153, %edx
	leaq	.LC48(%rip), %rsi
	call	CRYPTO_strdup@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L277
	xorl	%eax, %eax
	orq	$-1, %rcx
	movq	%r14, %rdi
	repnz scasb
	movq	%rcx, %rax
	notq	%rax
	subq	$1, %rax
	cltq
	testq	%rax, %rax
	js	.L218
	xorl	%r13d, %r13d
	movq	%r14, %rcx
	movq	%r14, %rdx
.L222:
	movzbl	(%rdx), %esi
	addq	$1, %rdx
	cmpb	$44, %sil
	je	.L284
	testb	%sil, %sil
	jne	.L219
.L284:
	leaq	-1(%rdx), %rsi
	cmpq	%rsi, %rcx
	setne	%cl
	movzbl	%cl, %ecx
	addl	%ecx, %r13d
	movq	%rdx, %rcx
.L219:
	movq	%rdx, %rsi
	subq	%r14, %rsi
	cmpq	%rsi, %rax
	jge	.L222
	testl	%r13d, %r13d
	je	.L218
	movslq	%r13d, %rdi
	movl	$1159, %edx
	leaq	.LC48(%rip), %rsi
	salq	$3, %rdi
	call	CRYPTO_malloc@PLT
	movq	%rax, -560(%rbp)
	testq	%rax, %rax
	je	.L278
	xorl	%eax, %eax
	orq	$-1, %rcx
	movq	%r14, %rdi
	repnz scasb
	movq	%rcx, %rax
	notq	%rax
	subq	$1, %rax
	cltq
	testq	%rax, %rax
	js	.L229
	xorl	%esi, %esi
	movq	%r14, %rcx
	movq	%r14, %rdx
.L224:
	movzbl	(%rdx), %edi
	addq	$1, %rdx
	cmpb	$44, %dil
	je	.L285
	testb	%dil, %dil
	jne	.L226
.L285:
	leaq	-1(%rdx), %rdi
	cmpq	%rdi, %rcx
	je	.L228
	movq	-560(%rbp), %r11
	movslq	%esi, %rdi
	addl	$1, %esi
	movq	%rcx, (%r11,%rdi,8)
	movb	$0, -1(%rdx)
.L228:
	movq	%rdx, %rcx
.L226:
	movq	%rdx, %rdi
	subq	%r14, %rdi
	cmpq	%rdi, %rax
	jge	.L224
.L229:
	movq	-560(%rbp), %rcx
	leal	-1(%r13), %eax
	xorl	%r13d, %r13d
	movq	%r14, -600(%rbp)
	movq	%rbx, -568(%rbp)
	movq	%r13, %r14
	leaq	8(%rcx,%rax,8), %rax
	movq	%r15, -576(%rbp)
	movq	%rax, -608(%rbp)
	xorl	%eax, %eax
	movl	%r12d, -588(%rbp)
	movl	%eax, %r13d
	jmp	.L225
.L230:
	addl	$1, %r13d
	cmpl	%r13d, -592(%rbp)
	je	.L477
.L225:
	movq	-616(%rbp), %rdi
	movl	%r13d, %esi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	X509_EXTENSION_get_object@PLT
	movq	%rax, %rdi
	call	OBJ_obj2nid@PLT
	movl	%eax, %edi
	call	OBJ_nid2sn@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L230
	movq	%rax, %rsi
	movl	$6, %ecx
	leaq	.LC69(%rip), %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L230
	movq	-560(%rbp), %r15
	jmp	.L233
.L232:
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L416
.L231:
	addq	$8, %r15
	cmpq	-608(%rbp), %r15
	je	.L230
.L233:
	movq	(%r15), %rsi
	movq	%rbx, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L231
	testq	%r14, %r14
	jne	.L232
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	jne	.L232
.L416:
	movq	%r14, %r13
	movq	-568(%rbp), %rbx
	movq	-576(%rbp), %r15
	movl	-588(%rbp), %r12d
	movq	-600(%rbp), %r14
	jmp	.L217
	.p2align 4,,10
	.p2align 3
.L458:
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	X509_alias_get0@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L134
	leaq	.LC27(%rip), %rsi
	movq	%rbx, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L123
.L459:
	movq	%r15, %rdi
	call	X509_subject_name_hash@PLT
.L422:
	movq	%rax, %rdx
	leaq	.LC29(%rip), %rsi
	movq	%rbx, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L123
.L251:
	movq	$0, -320(%rbp)
	xorl	%r12d, %r12d
	xorl	%ebx, %ebx
	movq	$0, -336(%rbp)
	movq	$0, -312(%rbp)
	movq	$0, -304(%rbp)
	movq	$0, -296(%rbp)
	jmp	.L9
.L461:
	movq	%r15, %rdi
	call	X509_issuer_name_hash@PLT
	jmp	.L422
.L261:
	movq	$0, -312(%rbp)
	jmp	.L107
.L401:
	movq	%r15, %rdi
	call	X509_get1_email@PLT
	movq	%rax, %r14
	jmp	.L130
.L460:
	movq	%r15, %rdi
	call	X509_subject_name_hash_old@PLT
	jmp	.L422
.L462:
	movq	%r15, %rdi
	call	X509_issuer_name_hash_old@PLT
	jmp	.L422
.L443:
	leaq	.LC11(%rip), %rdx
	leaq	.LC2(%rip), %rsi
	movq	%rax, %rdi
	call	NCONF_get_string@PLT
	movq	%rax, -400(%rbp)
	testq	%rax, %rax
	jne	.L95
	call	ERR_clear_error@PLT
	leaq	.LC2(%rip), %rax
	movq	%rax, -400(%rbp)
	jmp	.L95
.L413:
.L169:
	endbr64
	movq	bio_err(%rip), %rdi
	movq	%r15, %r12
	movl	$1, %r14d
	call	ERR_print_errors@PLT
	xorl	%r9d, %r9d
	jmp	.L9
.L259:
	movq	$0, -320(%rbp)
	xorl	%ebx, %ebx
	xorl	%r9d, %r9d
	movq	$0, -312(%rbp)
	movq	$0, -304(%rbp)
	movq	$0, -296(%rbp)
	jmp	.L9
.L266:
	movq	$0, -320(%rbp)
	movq	$0, -304(%rbp)
	movq	$0, -296(%rbp)
.L121:
	movl	-500(%rbp), %edx
	testl	%edx, %edx
	jne	.L478
	movq	-536(%rbp), %r8
	movq	-528(%rbp), %rcx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	-520(%rbp), %rdx
	call	print_cert_checks@PLT
	movl	-436(%rbp), %r14d
	orl	%r15d, %r14d
	jne	.L280
	movl	-224(%rbp), %eax
	cmpl	$4, %eax
	je	.L479
	cmpl	$32773, %eax
	jne	.L241
	movl	-352(%rbp), %eax
	movq	%r12, %rsi
	movq	%rbx, %rdi
	testl	%eax, %eax
	je	.L242
	call	PEM_write_bio_X509_AUX@PLT
.L240:
	xorl	%r9d, %r9d
	testl	%eax, %eax
	jne	.L9
	movq	%r9, -352(%rbp)
	movq	bio_err(%rip), %rdi
	leaq	.LC74(%rip), %rsi
.L431:
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	movl	$1, %r14d
	call	ERR_print_errors@PLT
	movq	-352(%rbp), %r9
	jmp	.L9
.L464:
	movq	%r15, %rdi
	call	X509_get0_pubkey@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L480
	leaq	.LC38(%rip), %rsi
	movq	%rbx, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	%r13, %rdi
	call	EVP_PKEY_id@PLT
	movq	%r13, %rdi
	cmpl	$6, %eax
	je	.L481
	call	EVP_PKEY_id@PLT
	cmpl	$116, %eax
	je	.L482
	leaq	.LC39(%rip), %rsi
	movq	%rbx, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L151:
	leaq	.LC26(%rip), %rsi
	movq	%rbx, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L123
.L465:
	movq	%r15, %rdi
	call	X509_get0_pubkey@PLT
	testq	%rax, %rax
	je	.L483
	movq	%rax, %rsi
	movq	%rbx, %rdi
	call	PEM_write_bio_PUBKEY@PLT
	jmp	.L123
.L472:
	movq	%r15, %r12
	movl	-620(%rbp), %r15d
	jmp	.L121
.L467:
	movq	-200(%rbp), %r13
	call	get_nameopt@PLT
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	%rax, %rdx
	movq	%r13, %rcx
	call	X509_print_ex@PLT
	jmp	.L123
.L466:
	call	get_nameopt@PLT
	movq	%r15, %rdi
	leaq	-192(%rbp), %r14
	movq	%rax, %r13
	call	X509_get_subject_name@PLT
	movq	%r13, %rcx
	leaq	.LC41(%rip), %rsi
	movq	%rbx, %rdi
	movq	%rax, %rdx
	call	print_name@PLT
	call	get_nameopt@PLT
	movq	%r15, %rdi
	movq	%rax, %r13
	call	X509_get_issuer_name@PLT
	movq	%r13, %rcx
	movq	%rbx, %rdi
	leaq	.LC42(%rip), %rsi
	movq	%rax, %rdx
	call	print_name@PLT
	movq	%rbx, %rdi
	leaq	.LC43(%rip), %rsi
	call	BIO_puts@PLT
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	i2d_X509@PLT
	leaq	.LC44(%rip), %rsi
	movl	%eax, %edi
	call	app_malloc@PLT
	movq	%r15, %rdi
	movq	%rax, %r13
	movq	%rax, -192(%rbp)
	call	X509_get_subject_name@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	i2d_X509_NAME@PLT
	movq	%r13, %rcx
	leaq	.LC45(%rip), %rsi
	movq	%rbx, %rdi
	movl	%eax, %edx
	call	print_array@PLT
	movq	%r15, %rdi
	movq	%r13, -192(%rbp)
	call	X509_get_X509_PUBKEY@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	i2d_X509_PUBKEY@PLT
	movq	%r13, %rcx
	movq	%rbx, %rdi
	leaq	.LC46(%rip), %rsi
	movl	%eax, %edx
	call	print_array@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%r13, -192(%rbp)
	call	i2d_X509@PLT
	movq	%rbx, %rdi
	movq	%r13, %rcx
	leaq	.LC47(%rip), %rsi
	movl	%eax, %edx
	call	print_array@PLT
	movl	$760, %edx
	leaq	.LC48(%rip), %rsi
	movq	%r13, %rdi
	call	CRYPTO_free@PLT
	jmp	.L123
.L439:
	call	opt_arg@PLT
	movq	bio_err(%rip), %rdi
	movq	%r12, %rdx
	leaq	.LC5(%rip), %rsi
	movq	%rax, %rcx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L76
.L438:
	movq	bio_err(%rip), %rdi
	leaq	.LC4(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L76
.L453:
	leaq	-192(%rbp), %rdi
	movq	%r12, %rdx
	xorl	%esi, %esi
	call	X509_get0_signature@PLT
	movq	-192(%rbp), %rdi
	call	corrupt_signature@PLT
	jmp	.L119
.L260:
	movq	$0, -320(%rbp)
	movq	$0, -336(%rbp)
	movq	$0, -312(%rbp)
.L430:
	movq	$0, -304(%rbp)
	xorl	%ebx, %ebx
	xorl	%r9d, %r9d
	movl	$1, %r14d
	movq	$0, -296(%rbp)
	jmp	.L9
.L448:
	leaq	.LC16(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	$0, -320(%rbp)
	jmp	.L429
.L134:
	leaq	.LC28(%rip), %rsi
	movq	%rbx, %rdi
	call	BIO_puts@PLT
	jmp	.L123
.L452:
	movq	%r12, %rdi
	call	X509_reject_clear@PLT
	jmp	.L112
.L451:
	movq	%r12, %rdi
	call	X509_trust_clear@PLT
	jmp	.L111
.L478:
	xorl	%edi, %edi
	call	time@PLT
	movq	%r12, %rdi
	addq	-584(%rbp), %rax
	movq	%rax, -192(%rbp)
	call	X509_get0_notAfter@PLT
	leaq	-192(%rbp), %rsi
	movq	%rax, %rdi
	call	X509_cmp_time@PLT
	testl	%eax, %eax
	js	.L484
	leaq	.LC72(%rip), %rsi
	movq	%rbx, %rdi
	xorl	%eax, %eax
	xorl	%r14d, %r14d
	call	BIO_printf@PLT
.L426:
	xorl	%r9d, %r9d
	jmp	.L9
.L280:
	xorl	%r14d, %r14d
	jmp	.L426
.L463:
	leaq	.LC30(%rip), %rsi
	movq	%rbx, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	xorl	%eax, %eax
	movl	%r12d, -560(%rbp)
	movl	%eax, %r12d
	jmp	.L140
.L487:
	movl	%r8d, %edx
	leaq	.LC33(%rip), %rsi
	movq	%rbx, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L143:
	movl	%r14d, %esi
	movl	$1, %edx
	movq	%r15, %rdi
	call	X509_check_purpose@PLT
	leaq	.LC36(%rip), %rcx
	movq	%r13, %rdx
	movq	%rbx, %rdi
	movl	%eax, %r14d
	leaq	.LC32(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	cmpl	$1, %r14d
	je	.L485
	testl	%r14d, %r14d
	jne	.L146
	leaq	.LC34(%rip), %rsi
	movq	%rbx, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L145:
	addl	$1, %r12d
.L140:
	call	X509_PURPOSE_get_count@PLT
	cmpl	%r12d, %eax
	jle	.L486
	movl	%r12d, %edi
	call	X509_PURPOSE_get0@PLT
	movq	%rax, %rdi
	movq	%rax, %r13
	call	X509_PURPOSE_get_id@PLT
	movq	%r13, %rdi
	movl	%eax, %r14d
	call	X509_PURPOSE_get0_name@PLT
	xorl	%edx, %edx
	movl	%r14d, %esi
	movq	%r15, %rdi
	movq	%rax, %r13
	call	X509_check_purpose@PLT
	leaq	.LC31(%rip), %rcx
	movq	%r13, %rdx
	movq	%rbx, %rdi
	movl	%eax, -568(%rbp)
	leaq	.LC32(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movl	-568(%rbp), %r8d
	cmpl	$1, %r8d
	je	.L141
	testl	%r8d, %r8d
	jne	.L487
	leaq	.LC34(%rip), %rsi
	movq	%rbx, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L143
.L146:
	movl	%r14d, %edx
	leaq	.LC33(%rip), %rsi
	movq	%rbx, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L145
.L485:
	leaq	.LC35(%rip), %rsi
	movq	%rbx, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L145
.L141:
	leaq	.LC35(%rip), %rsi
	movq	%rbx, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L143
.L486:
	movl	-560(%rbp), %r12d
	jmp	.L123
.L263:
	movq	$0, -320(%rbp)
	xorl	%r9d, %r9d
	movl	$1, %r14d
	movq	$0, -304(%rbp)
	movq	$0, -296(%rbp)
	jmp	.L9
.L469:
	movq	%rbx, %rdi
	leaq	.LC50(%rip), %rsi
	call	BIO_puts@PLT
	movq	%r15, %rdi
	call	X509_get0_notAfter@PLT
.L423:
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	ASN1_TIME_print@PLT
	leaq	.LC26(%rip), %rsi
	movq	%rbx, %rdi
	call	BIO_puts@PLT
	jmp	.L123
.L468:
	movq	%rbx, %rdi
	leaq	.LC49(%rip), %rsi
	call	BIO_puts@PLT
	movq	%r15, %rdi
	call	X509_get0_notBefore@PLT
	jmp	.L423
.L446:
	movq	bio_err(%rip), %rdi
	movq	%rax, -352(%rbp)
	xorl	%eax, %eax
	xorl	%r12d, %r12d
	leaq	.LC14(%rip), %rsi
	xorl	%ebx, %ebx
	call	BIO_printf@PLT
	movq	-352(%rbp), %r9
	movq	$0, -320(%rbp)
	movq	$0, -312(%rbp)
	movq	$0, -304(%rbp)
	movq	$0, -296(%rbp)
	jmp	.L9
.L445:
	movq	bio_err(%rip), %rdi
	xorl	%r12d, %r12d
	call	ERR_print_errors@PLT
	xorl	%r9d, %r9d
	movq	$0, -320(%rbp)
	movq	$0, -312(%rbp)
	movq	$0, -304(%rbp)
	movq	$0, -296(%rbp)
	jmp	.L9
.L450:
	movq	$0, -320(%rbp)
	jmp	.L430
.L470:
	movq	-216(%rbp), %r14
	testq	%r14, %r14
	je	.L488
.L160:
	leaq	-128(%rbp), %r13
	leaq	-192(%rbp), %rcx
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%r13, %rdx
	call	X509_digest@PLT
	testl	%eax, %eax
	je	.L489
	movq	%r14, %rdi
	xorl	%r14d, %r14d
	call	EVP_MD_type@PLT
	movl	%eax, %edi
	call	OBJ_nid2sn@PLT
	leaq	.LC52(%rip), %rsi
	movq	%rbx, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movl	-192(%rbp), %eax
	testl	%eax, %eax
	jle	.L123
.L162:
	leal	1(%r14), %edx
	movl	$58, %ecx
	cmpl	%edx, %eax
	movzbl	0(%r13,%r14), %edx
	jne	.L419
	movl	$10, %ecx
.L419:
	leaq	.LC53(%rip), %rsi
	movq	%rbx, %rdi
	xorl	%eax, %eax
	addq	$1, %r14
	call	BIO_printf@PLT
	movl	-192(%rbp), %eax
	cmpl	%r14d, %eax
	jg	.L162
	jmp	.L123
.L241:
	movq	bio_err(%rip), %rdi
	leaq	.LC73(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L425:
	movl	$1, %r14d
	xorl	%r9d, %r9d
	jmp	.L9
.L479:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	i2d_X509_bio@PLT
	jmp	.L240
.L481:
	call	EVP_PKEY_get0_RSA@PLT
	leaq	-192(%rbp), %rsi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rdi
	call	RSA_get0_key@PLT
	movq	-192(%rbp), %rsi
	movq	%rbx, %rdi
	call	BN_print@PLT
	jmp	.L151
.L471:
	movq	-328(%rbp), %r8
	movq	-208(%rbp), %rcx
	xorl	%edx, %edx
	leaq	.LC55(%rip), %r9
	movl	-220(%rbp), %esi
	movq	-464(%rbp), %rdi
	call	load_key@PLT
	movq	%rax, -296(%rbp)
	testq	%rax, %rax
	jne	.L167
.L272:
	movq	%r15, %r12
	xorl	%r9d, %r9d
	movl	$1, %r14d
	jmp	.L9
.L484:
	leaq	.LC71(%rip), %rsi
	movq	%rbx, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movl	-500(%rbp), %r14d
	jmp	.L426
.L242:
	call	PEM_write_bio_X509@PLT
	jmp	.L240
.L474:
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
	leaq	.LC63(%rip), %rsi
	call	BIO_printf@PLT
	cmpq	$0, -464(%rbp)
	je	.L490
	movq	-328(%rbp), %r8
	movq	-208(%rbp), %rcx
	xorl	%edx, %edx
	leaq	.LC65(%rip), %r9
	movl	-220(%rbp), %esi
	movq	-464(%rbp), %rdi
	call	load_key@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L275
	movq	bio_err(%rip), %rdi
	leaq	.LC66(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	-216(%rbp), %rdx
	movq	%r15, %rdi
	movq	%r13, %rsi
	call	X509_to_X509_REQ@PLT
	movq	%r13, %rdi
	movq	%rax, %r14
	movq	%rax, -320(%rbp)
	call	EVP_PKEY_free@PLT
	testq	%r14, %r14
	je	.L491
	movl	-436(%rbp), %ecx
	testl	%ecx, %ecx
	je	.L492
	movl	$1, -436(%rbp)
	jmp	.L123
.L473:
	movq	bio_err(%rip), %rdi
	leaq	.LC56(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	-408(%rbp), %rax
	testq	%rax, %rax
	je	.L181
	movq	-328(%rbp), %r8
	movq	-208(%rbp), %rcx
	xorl	%edx, %edx
	movq	%rax, %rdi
	movl	-232(%rbp), %esi
	leaq	.LC57(%rip), %r9
	call	load_key@PLT
	movq	%rax, -304(%rbp)
	testq	%rax, %rax
	je	.L272
.L181:
	movq	-216(%rbp), %rax
	movq	-312(%rbp), %rdi
	movq	%rax, -568(%rbp)
	call	X509_get0_pubkey@PLT
	testq	%rax, %rax
	je	.L493
	movq	-304(%rbp), %rsi
	movq	%rax, %rdi
	call	EVP_PKEY_copy_parameters@PLT
	call	X509_STORE_CTX_new@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L185
	movq	-344(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%r15, %rdx
	movq	%rax, %rdi
	call	X509_STORE_CTX_init@PLT
	testl	%eax, %eax
	je	.L185
	movq	-264(%rbp), %rax
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L494
.L186:
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	X509_STORE_CTX_set_cert@PLT
	movl	$16384, %esi
	movq	%r14, %rdi
	call	X509_STORE_CTX_set_flags@PLT
	cmpl	$0, -624(%rbp)
	jne	.L197
	movq	%r14, %rdi
	call	X509_verify_cert@PLT
	testl	%eax, %eax
	jle	.L421
.L197:
	movq	-304(%rbp), %rsi
	movq	-312(%rbp), %rdi
	call	X509_check_private_key@PLT
	testl	%eax, %eax
	je	.L495
	movq	-312(%rbp), %rdi
	call	X509_get_subject_name@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	X509_set_issuer_name@PLT
	testl	%eax, %eax
	je	.L198
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	X509_set_serialNumber@PLT
	testl	%eax, %eax
	je	.L198
	cmpl	$0, -360(%rbp)
	je	.L200
.L204:
	cmpl	$0, -504(%rbp)
	jne	.L201
.L202:
	cmpq	$0, -248(%rbp)
	je	.L206
	movl	$2, %esi
	movq	%r15, %rdi
	call	X509_set_version@PLT
	xorl	%ecx, %ecx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	leaq	-192(%rbp), %r10
	movq	-312(%rbp), %rsi
	movq	%r15, %rdx
	movq	%r10, %rdi
	movq	%r10, -560(%rbp)
	call	X509V3_set_ctx@PLT
	movq	-560(%rbp), %r10
	movq	-248(%rbp), %rsi
	movq	%r10, %rdi
	call	X509V3_set_nconf@PLT
	movq	-560(%rbp), %r10
	movq	-400(%rbp), %rdx
	movq	%r15, %rcx
	movq	-248(%rbp), %rdi
	movq	%r10, %rsi
	call	X509V3_EXT_add_nconf@PLT
	testl	%eax, %eax
	je	.L198
.L206:
	movq	-288(%rbp), %rcx
	movq	-568(%rbp), %rdx
	movq	%r15, %rdi
	movq	-304(%rbp), %rsi
	call	do_X509_sign@PLT
	testl	%eax, %eax
	je	.L198
	movq	%r14, %rdi
	call	X509_STORE_CTX_free@PLT
	cmpq	$0, -264(%rbp)
	jne	.L123
	movq	%r13, %rdi
	call	ASN1_INTEGER_free@PLT
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L475:
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	X509_ocspid_print@PLT
	jmp	.L123
.L482:
	movq	$0, -192(%rbp)
	movq	%r13, %rdi
	call	EVP_PKEY_get0_DSA@PLT
	leaq	-192(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rax, %rdi
	call	DSA_get0_key@PLT
	movq	-192(%rbp), %rsi
	movq	%rbx, %rdi
	call	BN_print@PLT
	jmp	.L151
.L447:
	movq	bio_err(%rip), %rdi
	leaq	.LC15(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	movq	$0, -320(%rbp)
	jmp	.L429
.L449:
	call	ASN1_INTEGER_new@PLT
	movq	%rax, %r9
	testq	%rax, %rax
	je	.L253
	xorl	%edi, %edi
	movq	%rax, %rsi
	movq	%rax, -296(%rbp)
	call	rand_serial@PLT
	movq	-296(%rbp), %r9
	testl	%eax, %eax
	je	.L255
	movq	%r9, %rsi
	movq	%r12, %rdi
	call	X509_set_serialNumber@PLT
	movq	-296(%rbp), %r9
	testl	%eax, %eax
	je	.L255
	movq	%r9, %rdi
	call	ASN1_INTEGER_free@PLT
	jmp	.L104
.L255:
	movq	%r9, -264(%rbp)
	xorl	%ebx, %ebx
	xorl	%r9d, %r9d
	movq	$0, -320(%rbp)
	movq	$0, -312(%rbp)
	movq	$0, -304(%rbp)
	movq	$0, -296(%rbp)
	jmp	.L9
.L253:
	movq	$0, -320(%rbp)
	xorl	%ebx, %ebx
	movq	$0, -312(%rbp)
	movq	$0, -304(%rbp)
	movq	$0, -296(%rbp)
	movq	$0, -264(%rbp)
	jmp	.L9
.L495:
	movq	bio_err(%rip), %rdi
	leaq	.LC62(%rip), %rsi
	movq	%r15, %r12
	movq	%r13, %r15
	call	BIO_printf@PLT
	movq	%r14, %rdi
	call	X509_STORE_CTX_free@PLT
.L183:
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	cmpq	$0, -264(%rbp)
	jne	.L425
.L211:
	movq	%r15, %rdi
	movl	$1, %r14d
	call	ASN1_INTEGER_free@PLT
	xorl	%r9d, %r9d
	movq	$0, -264(%rbp)
	jmp	.L9
.L494:
	cmpq	$0, -488(%rbp)
	movq	$0, -192(%rbp)
	je	.L496
	movq	$0, -560(%rbp)
	movq	-488(%rbp), %r8
.L187:
	movl	-552(%rbp), %esi
	xorl	%edx, %edx
	movq	%r8, %rdi
	movq	%r8, -576(%rbp)
	call	load_serial@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L190
	movl	$1, %esi
	movq	%rax, %rdi
	call	BN_add_word@PLT
	movq	-576(%rbp), %r8
	testl	%eax, %eax
	je	.L497
	leaq	-192(%rbp), %rcx
	movq	%r13, %rdx
	xorl	%esi, %esi
	movq	%r8, %rdi
	call	save_serial@PLT
.L190:
	movq	-560(%rbp), %rdi
	movl	$941, %edx
	leaq	.LC48(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%r13, %rdi
	call	BN_free@PLT
	movq	-192(%rbp), %r13
	testq	%r13, %r13
	jne	.L186
	movq	%r14, %rdi
	movq	%r15, %r12
	movq	%r13, %r15
	call	X509_STORE_CTX_free@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	jmp	.L211
.L496:
	movq	-424(%rbp), %rdi
	movl	$46, %esi
	call	strrchr@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L188
	subq	-424(%rbp), %r13
.L189:
	leal	5(%r13), %edi
	leaq	.LC60(%rip), %rsi
	call	app_malloc@PLT
	movq	-424(%rbp), %rsi
	movq	%r13, %rdx
	movq	%rax, %rdi
	call	memcpy@PLT
	addq	%rax, %r13
	movq	%rax, -560(%rbp)
	movq	%rax, %r8
	movl	$1819439918, 0(%r13)
	movb	$0, 4(%r13)
	jmp	.L187
.L497:
	movq	bio_err(%rip), %rdi
	leaq	.LC61(%rip), %rsi
	call	BIO_printf@PLT
	jmp	.L190
.L188:
	movq	-424(%rbp), %rdi
	xorl	%eax, %eax
	orq	$-1, %rcx
	repnz scasb
	movq	%rcx, %rax
	notq	%rax
	leaq	-1(%rax), %r13
	jmp	.L189
.L185:
	movq	bio_err(%rip), %rdi
	leaq	.LC59(%rip), %rsi
	xorl	%eax, %eax
	movq	%r15, %r12
	xorl	%r15d, %r15d
	call	BIO_printf@PLT
	movq	%r14, %rdi
	call	X509_STORE_CTX_free@PLT
	jmp	.L183
.L493:
	movq	bio_err(%rip), %rdi
	movq	%r15, %r12
	leaq	.LC58(%rip), %rsi
	movq	%rax, %r15
	xorl	%eax, %eax
	call	BIO_printf@PLT
	xorl	%edi, %edi
	call	X509_STORE_CTX_free@PLT
	jmp	.L183
.L491:
	movq	bio_err(%rip), %rdi
	movq	%r15, %r12
	movl	$1, %r14d
	call	ERR_print_errors@PLT
	xorl	%r9d, %r9d
	jmp	.L9
.L490:
	movq	bio_err(%rip), %rdi
	leaq	.LC64(%rip), %rsi
	xorl	%eax, %eax
	movq	%r15, %r12
	movl	$1, %r14d
	call	BIO_printf@PLT
	xorl	%r9d, %r9d
	jmp	.L9
.L492:
	call	get_nameopt@PLT
	movq	-320(%rbp), %r14
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	movq	%rax, %rdx
	movq	%r14, %rsi
	call	X509_REQ_print_ex@PLT
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	PEM_write_bio_X509_REQ@PLT
	movl	$1, -436(%rbp)
	jmp	.L123
.L200:
	movl	-356(%rbp), %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	set_cert_times@PLT
	testl	%eax, %eax
	jne	.L204
.L421:
	movq	%r14, %rdi
	movq	%r15, %r12
	movq	%r13, %r15
	call	X509_STORE_CTX_free@PLT
	jmp	.L183
.L207:
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	X509_delete_ext@PLT
.L201:
	movq	%r15, %rdi
	call	X509_get_ext_count@PLT
	testl	%eax, %eax
	jg	.L207
	jmp	.L202
.L436:
	call	__stack_chk_fail@PLT
.L198:
	movq	%r14, %rdi
	movq	%r15, %r12
	movq	%r13, %r15
	call	X509_STORE_CTX_free@PLT
	jmp	.L183
.L218:
	movq	-456(%rbp), %rdx
	movq	%rbx, %rdi
	xorl	%eax, %eax
	xorl	%r13d, %r13d
	leaq	.LC68(%rip), %rsi
	call	BIO_printf@PLT
	movq	$0, -560(%rbp)
.L217:
	movq	%r13, %rdi
	call	OPENSSL_sk_free@PLT
	movq	-560(%rbp), %rdi
	movl	$1193, %edx
	leaq	.LC48(%rip), %rsi
	call	CRYPTO_free@PLT
	movl	$1194, %edx
	leaq	.LC48(%rip), %rsi
	movq	%r14, %rdi
	call	CRYPTO_free@PLT
	jmp	.L123
.L277:
	movq	$0, -560(%rbp)
	xorl	%r13d, %r13d
	jmp	.L217
.L476:
	leaq	.LC67(%rip), %rsi
	movq	%rbx, %rdi
	xorl	%eax, %eax
	xorl	%r14d, %r14d
	call	BIO_printf@PLT
	xorl	%r13d, %r13d
	movq	$0, -560(%rbp)
	jmp	.L217
.L268:
	movq	%r15, %r12
	movl	$1, %r14d
	xorl	%r9d, %r9d
	jmp	.L9
.L275:
	movq	%r13, %r9
	movq	%r15, %r12
	movl	$1, %r14d
	jmp	.L9
.L105:
	movq	-336(%rbp), %rdi
	call	X509_REQ_get0_pubkey@PLT
	jmp	.L418
.L278:
	xorl	%r13d, %r13d
	jmp	.L217
.L477:
	movq	%r14, %r13
	movq	-568(%rbp), %rbx
	movq	-576(%rbp), %r15
	movq	%r13, %rdi
	movl	-588(%rbp), %r12d
	movq	-600(%rbp), %r14
	call	OPENSSL_sk_num@PLT
	testl	%eax, %eax
	je	.L498
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	X509V3_extensions_print@PLT
	jmp	.L217
.L488:
	call	EVP_sha1@PLT
	movq	%rax, %r14
	jmp	.L160
.L489:
	movq	bio_err(%rip), %rdi
	leaq	.LC51(%rip), %rsi
	movq	%r15, %r12
	movl	$1, %r14d
	call	BIO_printf@PLT
	xorl	%r9d, %r9d
	jmp	.L9
.L480:
	movq	%rax, -352(%rbp)
	movq	bio_err(%rip), %rdi
	movq	%r15, %r12
	xorl	%eax, %eax
	leaq	.LC37(%rip), %rsi
	jmp	.L431
.L483:
	movq	%rax, -352(%rbp)
	movq	bio_err(%rip), %rdi
	movq	%r15, %r12
	xorl	%eax, %eax
	leaq	.LC40(%rip), %rsi
	jmp	.L431
.L498:
	movq	-456(%rbp), %rdx
	leaq	.LC70(%rip), %rsi
	movq	%rbx, %rdi
	call	BIO_printf@PLT
	jmp	.L217
	.cfi_endproc
.LFE1435:
	.size	x509_main, .-x509_main
	.globl	x509_options
	.section	.rodata.str1.1
.LC75:
	.string	"help"
.LC76:
	.string	"Display this summary"
.LC77:
	.string	"inform"
	.section	.rodata.str1.8
	.align 8
.LC78:
	.string	"Input format - default PEM (one of DER or PEM)"
	.section	.rodata.str1.1
.LC79:
	.string	"in"
.LC80:
	.string	"Input file - default stdin"
.LC81:
	.string	"outform"
	.section	.rodata.str1.8
	.align 8
.LC82:
	.string	"Output format - default PEM (one of DER or PEM)"
	.section	.rodata.str1.1
.LC83:
	.string	"out"
.LC84:
	.string	"Output file - default stdout"
.LC85:
	.string	"keyform"
	.section	.rodata.str1.8
	.align 8
.LC86:
	.string	"Private key format - default PEM"
	.section	.rodata.str1.1
.LC87:
	.string	"passin"
	.section	.rodata.str1.8
	.align 8
.LC88:
	.string	"Private key password/pass-phrase source"
	.section	.rodata.str1.1
.LC89:
	.string	"serial"
.LC90:
	.string	"Print serial number value"
.LC91:
	.string	"subject_hash"
.LC92:
	.string	"Print subject hash value"
.LC93:
	.string	"issuer_hash"
.LC94:
	.string	"Print issuer hash value"
.LC95:
	.string	"hash"
.LC96:
	.string	"Synonym for -subject_hash"
.LC97:
	.string	"subject"
.LC98:
	.string	"Print subject DN"
.LC99:
	.string	"issuer"
.LC100:
	.string	"Print issuer DN"
.LC101:
	.string	"email"
.LC102:
	.string	"Print email address(es)"
.LC103:
	.string	"startdate"
.LC104:
	.string	"Set notBefore field"
.LC105:
	.string	"enddate"
.LC106:
	.string	"Set notAfter field"
.LC107:
	.string	"purpose"
	.section	.rodata.str1.8
	.align 8
.LC108:
	.string	"Print out certificate purposes"
	.section	.rodata.str1.1
.LC109:
	.string	"dates"
.LC110:
	.string	"Both Before and After dates"
.LC111:
	.string	"modulus"
.LC112:
	.string	"Print the RSA key modulus"
.LC113:
	.string	"pubkey"
.LC114:
	.string	"Output the public key"
.LC115:
	.string	"fingerprint"
	.section	.rodata.str1.8
	.align 8
.LC116:
	.string	"Print the certificate fingerprint"
	.section	.rodata.str1.1
.LC117:
	.string	"alias"
.LC118:
	.string	"Output certificate alias"
.LC119:
	.string	"noout"
.LC120:
	.string	"No output, just status"
.LC121:
	.string	"nocert"
.LC122:
	.string	"No certificate output"
.LC123:
	.string	"ocspid"
	.section	.rodata.str1.8
	.align 8
.LC124:
	.string	"Print OCSP hash values for the subject name and public key"
	.section	.rodata.str1.1
.LC125:
	.string	"ocsp_uri"
.LC126:
	.string	"Print OCSP Responder URL(s)"
.LC127:
	.string	"trustout"
.LC128:
	.string	"Output a trusted certificate"
.LC129:
	.string	"clrtrust"
.LC130:
	.string	"Clear all trusted purposes"
.LC131:
	.string	"clrext"
	.section	.rodata.str1.8
	.align 8
.LC132:
	.string	"Clear all certificate extensions"
	.section	.rodata.str1.1
.LC133:
	.string	"addtrust"
	.section	.rodata.str1.8
	.align 8
.LC134:
	.string	"Trust certificate for a given purpose"
	.section	.rodata.str1.1
.LC135:
	.string	"addreject"
	.section	.rodata.str1.8
	.align 8
.LC136:
	.string	"Reject certificate for a given purpose"
	.section	.rodata.str1.1
.LC137:
	.string	"setalias"
.LC138:
	.string	"Set certificate alias"
.LC139:
	.string	"days"
	.section	.rodata.str1.8
	.align 8
.LC140:
	.string	"How long till expiry of a signed certificate - def 30 days"
	.section	.rodata.str1.1
.LC141:
	.string	"checkend"
	.section	.rodata.str1.8
	.align 8
.LC142:
	.string	"Check whether the cert expires in the next arg seconds"
	.section	.rodata.str1.1
.LC143:
	.string	"Exit 1 if so, 0 if not"
.LC144:
	.string	"signkey"
.LC145:
	.string	"Self sign cert with arg"
.LC146:
	.string	"x509toreq"
	.section	.rodata.str1.8
	.align 8
.LC147:
	.string	"Output a certification request object"
	.section	.rodata.str1.1
.LC148:
	.string	"req"
	.section	.rodata.str1.8
	.align 8
.LC149:
	.string	"Input is a certificate request, sign and output"
	.section	.rodata.str1.1
.LC150:
	.string	"CA"
	.section	.rodata.str1.8
	.align 8
.LC151:
	.string	"Set the CA certificate, must be PEM format"
	.section	.rodata.str1.1
.LC152:
	.string	"CAkey"
	.section	.rodata.str1.8
	.align 8
.LC153:
	.string	"The CA key, must be PEM format; if not in CAfile"
	.section	.rodata.str1.1
.LC154:
	.string	"CAcreateserial"
	.section	.rodata.str1.8
	.align 8
.LC155:
	.string	"Create serial number file if it does not exist"
	.section	.rodata.str1.1
.LC156:
	.string	"CAserial"
.LC157:
	.string	"Serial file"
.LC158:
	.string	"set_serial"
.LC159:
	.string	"Serial number to use"
.LC160:
	.string	"text"
	.section	.rodata.str1.8
	.align 8
.LC161:
	.string	"Print the certificate in text form"
	.section	.rodata.str1.1
.LC162:
	.string	"ext"
	.section	.rodata.str1.8
	.align 8
.LC163:
	.string	"Print various X509V3 extensions"
	.section	.rodata.str1.1
.LC164:
	.string	"C"
.LC165:
	.string	"Print out C code forms"
.LC166:
	.string	"extfile"
	.section	.rodata.str1.8
	.align 8
.LC167:
	.string	"File with X509V3 extensions to add"
	.section	.rodata.str1.1
.LC168:
	.string	"rand"
	.section	.rodata.str1.8
	.align 8
.LC169:
	.string	"Load the file(s) into the random number generator"
	.section	.rodata.str1.1
.LC170:
	.string	"writerand"
	.section	.rodata.str1.8
	.align 8
.LC171:
	.string	"Write random data to the specified file"
	.align 8
.LC172:
	.string	"Section from config file to use"
	.section	.rodata.str1.1
.LC173:
	.string	"nameopt"
	.section	.rodata.str1.8
	.align 8
.LC174:
	.string	"Various certificate name options"
	.section	.rodata.str1.1
.LC175:
	.string	"certopt"
	.section	.rodata.str1.8
	.align 8
.LC176:
	.string	"Various certificate text options"
	.section	.rodata.str1.1
.LC177:
	.string	"checkhost"
	.section	.rodata.str1.8
	.align 8
.LC178:
	.string	"Check certificate matches host"
	.section	.rodata.str1.1
.LC179:
	.string	"checkemail"
	.section	.rodata.str1.8
	.align 8
.LC180:
	.string	"Check certificate matches email"
	.section	.rodata.str1.1
.LC181:
	.string	"checkip"
	.section	.rodata.str1.8
	.align 8
.LC182:
	.string	"Check certificate matches ipaddr"
	.section	.rodata.str1.1
.LC183:
	.string	"CAform"
.LC184:
	.string	"CA format - default PEM"
.LC185:
	.string	"CAkeyform"
.LC186:
	.string	"CA key format - default PEM"
.LC187:
	.string	"sigopt"
	.section	.rodata.str1.8
	.align 8
.LC188:
	.string	"Signature parameter in n:v form"
	.section	.rodata.str1.1
.LC189:
	.string	"force_pubkey"
	.section	.rodata.str1.8
	.align 8
.LC190:
	.string	"Force the Key to put inside certificate"
	.section	.rodata.str1.1
.LC191:
	.string	"next_serial"
	.section	.rodata.str1.8
	.align 8
.LC192:
	.string	"Increment current certificate serial number"
	.section	.rodata.str1.1
.LC193:
	.string	"clrreject"
	.section	.rodata.str1.8
	.align 8
.LC194:
	.string	"Clears all the prohibited or rejected uses of the certificate"
	.section	.rodata.str1.1
.LC195:
	.string	"badsig"
	.section	.rodata.str1.8
	.align 8
.LC196:
	.string	"Corrupt last byte of certificate signature (for test)"
	.section	.rodata.str1.1
.LC197:
	.string	"Any supported digest"
.LC198:
	.string	"subject_hash_old"
	.section	.rodata.str1.8
	.align 8
.LC199:
	.string	"Print old-style (MD5) issuer hash value"
	.section	.rodata.str1.1
.LC200:
	.string	"issuer_hash_old"
	.section	.rodata.str1.8
	.align 8
.LC201:
	.string	"Print old-style (MD5) subject hash value"
	.section	.rodata.str1.1
.LC202:
	.string	"engine"
	.section	.rodata.str1.8
	.align 8
.LC203:
	.string	"Use engine, possibly a hardware device"
	.section	.rodata.str1.1
.LC204:
	.string	"preserve_dates"
	.section	.rodata.str1.8
	.align 8
.LC205:
	.string	"preserve existing dates when signing"
	.section	.data.rel.ro,"aw"
	.align 32
	.type	x509_options, @object
	.size	x509_options, 1632
x509_options:
	.quad	.LC75
	.long	1
	.long	45
	.quad	.LC76
	.quad	.LC77
	.long	2
	.long	102
	.quad	.LC78
	.quad	.LC79
	.long	13
	.long	60
	.quad	.LC80
	.quad	.LC81
	.long	3
	.long	102
	.quad	.LC82
	.quad	.LC83
	.long	14
	.long	62
	.quad	.LC84
	.quad	.LC85
	.long	4
	.long	69
	.quad	.LC86
	.quad	.LC87
	.long	10
	.long	115
	.quad	.LC88
	.quad	.LC89
	.long	29
	.long	45
	.quad	.LC90
	.quad	.LC91
	.long	35
	.long	45
	.quad	.LC92
	.quad	.LC93
	.long	36
	.long	45
	.quad	.LC94
	.quad	.LC95
	.long	35
	.long	45
	.quad	.LC96
	.quad	.LC97
	.long	37
	.long	45
	.quad	.LC98
	.quad	.LC99
	.long	38
	.long	45
	.quad	.LC100
	.quad	.LC101
	.long	27
	.long	45
	.quad	.LC102
	.quad	.LC103
	.long	42
	.long	45
	.quad	.LC104
	.quad	.LC105
	.long	43
	.long	45
	.quad	.LC106
	.quad	.LC107
	.long	41
	.long	45
	.quad	.LC108
	.quad	.LC109
	.long	40
	.long	45
	.quad	.LC110
	.quad	.LC111
	.long	31
	.long	45
	.quad	.LC112
	.quad	.LC113
	.long	32
	.long	45
	.quad	.LC114
	.quad	.LC115
	.long	39
	.long	45
	.quad	.LC116
	.quad	.LC117
	.long	52
	.long	45
	.quad	.LC118
	.quad	.LC119
	.long	48
	.long	45
	.quad	.LC120
	.quad	.LC121
	.long	61
	.long	45
	.quad	.LC122
	.quad	.LC123
	.long	55
	.long	45
	.quad	.LC124
	.quad	.LC125
	.long	28
	.long	45
	.quad	.LC126
	.quad	.LC127
	.long	49
	.long	45
	.quad	.LC128
	.quad	.LC129
	.long	50
	.long	45
	.quad	.LC130
	.quad	.LC131
	.long	54
	.long	45
	.quad	.LC132
	.quad	.LC133
	.long	21
	.long	115
	.quad	.LC134
	.quad	.LC135
	.long	22
	.long	115
	.quad	.LC136
	.quad	.LC137
	.long	23
	.long	115
	.quad	.LC138
	.quad	.LC139
	.long	9
	.long	110
	.quad	.LC140
	.quad	.LC141
	.long	44
	.long	77
	.quad	.LC142
	.quad	OPT_MORE_STR
	.long	1
	.long	1
	.quad	.LC143
	.quad	.LC144
	.long	15
	.long	115
	.quad	.LC145
	.quad	.LC146
	.long	33
	.long	45
	.quad	.LC147
	.quad	.LC148
	.long	5
	.long	45
	.quad	.LC149
	.quad	.LC150
	.long	16
	.long	60
	.quad	.LC151
	.quad	.LC152
	.long	17
	.long	115
	.quad	.LC153
	.quad	.LC154
	.long	53
	.long	45
	.quad	.LC155
	.quad	.LC156
	.long	18
	.long	115
	.quad	.LC157
	.quad	.LC158
	.long	19
	.long	115
	.quad	.LC159
	.quad	.LC160
	.long	34
	.long	45
	.quad	.LC161
	.quad	.LC162
	.long	1504
	.long	115
	.quad	.LC163
	.quad	.LC164
	.long	26
	.long	45
	.quad	.LC165
	.quad	.LC166
	.long	11
	.long	60
	.quad	.LC167
	.quad	.LC168
	.long	1501
	.long	115
	.quad	.LC169
	.quad	.LC170
	.long	1502
	.long	62
	.quad	.LC171
	.quad	.LC11
	.long	12
	.long	115
	.quad	.LC172
	.quad	.LC173
	.long	25
	.long	115
	.quad	.LC174
	.quad	.LC175
	.long	24
	.long	115
	.quad	.LC176
	.quad	.LC177
	.long	45
	.long	115
	.quad	.LC178
	.quad	.LC179
	.long	46
	.long	115
	.quad	.LC180
	.quad	.LC181
	.long	47
	.long	115
	.quad	.LC182
	.quad	.LC183
	.long	6
	.long	70
	.quad	.LC184
	.quad	.LC185
	.long	7
	.long	69
	.quad	.LC186
	.quad	.LC187
	.long	8
	.long	115
	.quad	.LC188
	.quad	.LC189
	.long	20
	.long	60
	.quad	.LC190
	.quad	.LC191
	.long	30
	.long	45
	.quad	.LC192
	.quad	.LC193
	.long	51
	.long	45
	.quad	.LC194
	.quad	.LC195
	.long	58
	.long	45
	.quad	.LC196
	.quad	.LC31
	.long	59
	.long	45
	.quad	.LC197
	.quad	.LC198
	.long	56
	.long	45
	.quad	.LC199
	.quad	.LC200
	.long	57
	.long	45
	.quad	.LC201
	.quad	.LC202
	.long	60
	.long	115
	.quad	.LC203
	.quad	.LC204
	.long	62
	.long	45
	.quad	.LC205
	.quad	0
	.zero	16
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
