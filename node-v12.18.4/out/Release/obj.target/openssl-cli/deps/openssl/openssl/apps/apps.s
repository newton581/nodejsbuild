	.file	"apps.c"
	.text
	.p2align 4
	.type	index_name_qual, @function
index_name_qual:
.LFB1615:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	cmpb	$86, (%rax)
	sete	%al
	movzbl	%al, %eax
	ret
	.cfi_endproc
.LFE1615:
	.size	index_name_qual, .-index_name_qual
	.p2align 4
	.type	ui_close, @function
ui_close:
.LFB1578:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	ui_fallback_method(%rip), %rdi
	call	UI_method_get_closer@PLT
	testq	%rax, %rax
	je	.L4
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L4:
	.cfi_restore_state
	addq	$8, %rsp
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1578:
	.size	ui_close, .-ui_close
	.p2align 4
	.type	ui_write, @function
ui_write:
.LFB1577:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	movq	%rsi, %rdi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	call	UI_get_input_flags@PLT
	testb	$2, %al
	je	.L10
	movq	%r13, %rdi
	call	UI_get0_user_data@PLT
	testq	%rax, %rax
	je	.L10
	movq	%r12, %rdi
	call	UI_get_string_type@PLT
	subl	$1, %eax
	cmpl	$1, %eax
	jbe	.L19
.L10:
	movq	ui_fallback_method(%rip), %rdi
	call	UI_method_get_writer@PLT
	testq	%rax, %rax
	je	.L11
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L19:
	.cfi_restore_state
	movq	%r13, %rdi
	call	UI_get0_user_data@PLT
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L10
	cmpb	$0, (%rax)
	je	.L10
.L11:
	popq	%r12
	movl	$1, %eax
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1577:
	.size	ui_write, .-ui_write
	.p2align 4
	.type	ui_read, @function
ui_read:
.LFB1576:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	movq	%rsi, %rdi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	call	UI_get_input_flags@PLT
	testb	$2, %al
	je	.L21
	movq	%r13, %rdi
	call	UI_get0_user_data@PLT
	testq	%rax, %rax
	je	.L21
	movq	%r12, %rdi
	call	UI_get_string_type@PLT
	subl	$1, %eax
	cmpl	$1, %eax
	jbe	.L35
.L21:
	movq	ui_fallback_method(%rip), %rdi
	call	UI_method_get_reader@PLT
	testq	%rax, %rax
	je	.L20
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L35:
	.cfi_restore_state
	movq	%r13, %rdi
	call	UI_get0_user_data@PLT
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L21
	cmpb	$0, (%rdx)
	je	.L21
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	UI_set_result@PLT
.L20:
	popq	%r12
	movl	$1, %eax
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1576:
	.size	ui_read, .-ui_read
	.p2align 4
	.type	ui_open, @function
ui_open:
.LFB1575:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	ui_fallback_method(%rip), %rdi
	call	UI_method_get_opener@PLT
	testq	%rax, %rax
	je	.L37
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L37:
	.cfi_restore_state
	addq	$8, %rsp
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1575:
	.size	ui_open, .-ui_open
	.p2align 4
	.type	index_serial_LHASH_COMP, @function
index_serial_LHASH_COMP:
.LFB1619:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rdi
	cmpb	$48, (%rdi)
	jne	.L40
	.p2align 4,,10
	.p2align 3
.L41:
	addq	$1, %rdi
	cmpb	$48, (%rdi)
	je	.L41
.L40:
	movq	24(%rsi), %rsi
	cmpb	$48, (%rsi)
	jne	.L42
	.p2align 4,,10
	.p2align 3
.L43:
	addq	$1, %rsi
	cmpb	$48, (%rsi)
	je	.L43
.L42:
	jmp	strcmp@PLT
	.cfi_endproc
.LFE1619:
	.size	index_serial_LHASH_COMP, .-index_serial_LHASH_COMP
	.p2align 4
	.type	index_name_LHASH_COMP, @function
index_name_LHASH_COMP:
.LFB1621:
	.cfi_startproc
	endbr64
	movq	40(%rsi), %rsi
	movq	40(%rdi), %rdi
	jmp	strcmp@PLT
	.cfi_endproc
.LFE1621:
	.size	index_name_LHASH_COMP, .-index_name_LHASH_COMP
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"certificate"
.LC1:
	.string	"CRL"
.LC2:
	.string	"https not supported\n"
.LC3:
	.string	"GET"
.LC4:
	.string	"Host"
.LC5:
	.string	"Error loading %s from %s\n"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC6:
	.string	"../deps/openssl/openssl/apps/apps.c"
	.text
	.p2align 4
	.type	load_cert_crl_http, @function
load_cert_crl_http:
.LFB1591:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	leaq	-64(%rbp), %rcx
	leaq	-84(%rbp), %r8
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	leaq	-80(%rbp), %rsi
	subq	$72, %rsp
	movq	%rdx, -104(%rbp)
	leaq	-72(%rbp), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movq	$0, -72(%rbp)
	movq	$0, -64(%rbp)
	call	OCSP_parse_url@PLT
	testl	%eax, %eax
	je	.L48
	movl	-84(%rbp), %eax
	testl	%eax, %eax
	jne	.L89
	movq	-80(%rbp), %rdi
	call	BIO_new_connect@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L48
	movq	-72(%rbp), %rcx
	movl	$1, %edx
	movq	%rax, %rdi
	xorl	%r14d, %r14d
	movl	$100, %esi
	call	BIO_ctrl@PLT
	testq	%rax, %rax
	je	.L50
	movl	$1024, %esi
	movq	%r13, %rdi
	call	OCSP_REQ_CTX_new@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L50
	movq	-64(%rbp), %rdx
	leaq	.LC3(%rip), %rsi
	movq	%rax, %rdi
	call	OCSP_REQ_CTX_http@PLT
	testl	%eax, %eax
	je	.L50
	movq	-80(%rbp), %rdx
	leaq	.LC4(%rip), %rsi
	movq	%r14, %rdi
	call	OCSP_REQ_CTX_add1_header@PLT
	testl	%eax, %eax
	je	.L50
	testq	%rbx, %rbx
	je	.L51
	.p2align 4,,10
	.p2align 3
.L52:
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	X509_http_nbio@PLT
	movl	%eax, %r15d
	cmpl	$-1, %eax
	je	.L52
	movq	-80(%rbp), %rdi
	movl	$625, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-64(%rbp), %rdi
	movl	$626, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-72(%rbp), %rdi
	movl	$627, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%r13, %rdi
	call	BIO_free_all@PLT
	movq	%r14, %rdi
	call	OCSP_REQ_CTX_free@PLT
	cmpl	$1, %r15d
	jne	.L90
.L58:
	movl	$1, %r15d
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L89:
	movq	bio_err(%rip), %rdi
	leaq	.LC2(%rip), %rsi
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	call	BIO_puts@PLT
.L50:
	movq	-80(%rbp), %rdi
	movl	$625, %edx
	leaq	.LC6(%rip), %rsi
	xorl	%r15d, %r15d
	call	CRYPTO_free@PLT
	movq	-64(%rbp), %rdi
	movl	$626, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-72(%rbp), %rdi
	movl	$627, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%r13, %rdi
	call	BIO_free_all@PLT
	movq	%r14, %rdi
	call	OCSP_REQ_CTX_free@PLT
	testq	%rbx, %rbx
	leaq	.LC0(%rip), %rdx
	leaq	.LC1(%rip), %rax
	cmove	%rax, %rdx
.L55:
	movq	bio_err(%rip), %rdi
	movq	%r12, %rcx
	leaq	.LC5(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
.L47:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L91
	addq	$72, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L48:
	.cfi_restore_state
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L51:
	movq	-104(%rbp), %rsi
	movq	%r14, %rdi
	call	X509_CRL_http_nbio@PLT
	movl	%eax, %r15d
	cmpl	$-1, %eax
	je	.L51
	movq	-80(%rbp), %rdi
	movl	$625, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-64(%rbp), %rdi
	movl	$626, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-72(%rbp), %rdi
	movl	$627, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%r13, %rdi
	call	BIO_free_all@PLT
	movq	%r14, %rdi
	call	OCSP_REQ_CTX_free@PLT
	cmpl	$1, %r15d
	je	.L58
	leaq	.LC1(%rip), %rdx
	jmp	.L55
.L91:
	call	__stack_chk_fail@PLT
.L90:
	leaq	.LC0(%rip), %rdx
	jmp	.L55
	.cfi_endproc
.LFE1591:
	.size	load_cert_crl_http, .-load_cert_crl_http
	.p2align 4
	.type	index_name_LHASH_HASH, @function
index_name_LHASH_HASH:
.LFB1620:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rdi
	jmp	OPENSSL_LH_strhash@PLT
	.cfi_endproc
.LFE1620:
	.size	index_name_LHASH_HASH, .-index_name_LHASH_HASH
	.p2align 4
	.type	index_serial_LHASH_HASH, @function
index_serial_LHASH_HASH:
.LFB1618:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rdi
	cmpb	$48, (%rdi)
	jne	.L94
	.p2align 4,,10
	.p2align 3
.L95:
	addq	$1, %rdi
	cmpb	$48, (%rdi)
	je	.L95
.L94:
	jmp	OPENSSL_LH_strhash@PLT
	.cfi_endproc
.LFE1618:
	.size	index_serial_LHASH_HASH, .-index_serial_LHASH_HASH
	.section	.rodata.str1.1
.LC7:
	.string	"pass:"
.LC8:
	.string	"env:"
	.section	.rodata.str1.8
	.align 8
.LC9:
	.string	"Can't read environment variable %s\n"
	.section	.rodata.str1.1
.LC10:
	.string	"file:"
.LC11:
	.string	"r"
.LC12:
	.string	"Can't open file %s\n"
	.section	.rodata.str1.8
	.align 8
.LC13:
	.string	"Can't access file descriptor %s\n"
	.section	.rodata.str1.1
.LC14:
	.string	"stdin"
.LC15:
	.string	"Can't open BIO for stdin\n"
	.section	.rodata.str1.8
	.align 8
.LC16:
	.string	"Invalid password argument \"%s\"\n"
	.align 8
.LC17:
	.string	"Error reading password from BIO\n"
	.text
	.p2align 4
	.type	app_get_pass, @function
app_get_pass:
.LFB1584:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$5, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	leaq	.LC7(%rip), %rdi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movl	%esi, %ebx
	movq	%r13, %rsi
	subq	$1048, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L127
	movl	$4, %ecx
	leaq	.LC8(%rip), %rdi
	movq	%r13, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L128
	testl	%ebx, %ebx
	jne	.L129
.L102:
	movl	$5, %ecx
	leaq	.LC10(%rip), %rdi
	movq	%r13, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L130
	cmpb	$102, 0(%r13)
	jne	.L106
	cmpb	$100, 1(%r13)
	jne	.L106
	cmpb	$58, 2(%r13)
	jne	.L106
	addq	$3, %r13
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%r13, %rdi
	call	strtol@PLT
	testl	%eax, %eax
	jns	.L107
.L108:
	movq	bio_err(%rip), %rdi
	movq	%r13, %rdx
	xorl	%eax, %eax
	xorl	%r12d, %r12d
	leaq	.LC13(%rip), %rsi
	call	BIO_printf@PLT
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L129:
	movq	pwdbio.27474(%rip), %r12
	testq	%r12, %r12
	je	.L102
.L103:
	leaq	-1072(%rbp), %r13
	movq	%r12, %rdi
	movl	$1024, %edx
	movq	%r13, %rsi
	call	BIO_gets@PLT
	movl	%eax, %r12d
	cmpl	$1, %ebx
	je	.L110
	movq	pwdbio.27474(%rip), %rdi
	call	BIO_free_all@PLT
	movq	$0, pwdbio.27474(%rip)
.L110:
	testl	%r12d, %r12d
	jle	.L131
	movl	$10, %esi
	movq	%r13, %rdi
	call	strchr@PLT
	testq	%rax, %rax
	je	.L112
	movb	$0, (%rax)
.L112:
	movl	$449, %edx
	leaq	.LC6(%rip), %rsi
	movq	%r13, %rdi
	call	CRYPTO_strdup@PLT
	movq	%rax, %r12
.L97:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L132
	addq	$1048, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L128:
	.cfi_restore_state
	addq	$4, %r13
	movq	%r13, %rdi
	call	getenv@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L133
	movl	$393, %edx
	leaq	.LC6(%rip), %rsi
	movq	%rax, %rdi
	call	CRYPTO_strdup@PLT
	movq	%rax, %r12
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L127:
	leaq	5(%r13), %rdi
	movl	$386, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_strdup@PLT
	movq	%rax, %r12
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L106:
	movl	$6, %ecx
	leaq	.LC14(%rip), %rdi
	movq	%r13, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L109
	movq	stdin(%rip), %rdi
	movl	$16, %esi
	call	BIO_new_fp@PLT
	movq	%rax, pwdbio.27474(%rip)
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L103
	movq	bio_err(%rip), %rdi
	leaq	.LC15(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L107:
	xorl	%esi, %esi
	movl	%eax, %edi
	call	BIO_new_fd@PLT
	movq	%rax, pwdbio.27474(%rip)
	testq	%rax, %rax
	je	.L108
	call	BIO_f_buffer@PLT
	movq	%rax, %rdi
	call	BIO_new@PLT
	movq	pwdbio.27474(%rip), %rsi
	movq	%rax, %rdi
	call	BIO_push@PLT
	movq	%rax, pwdbio.27474(%rip)
	movq	%rax, %r12
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L130:
	addq	$5, %r13
	leaq	.LC11(%rip), %rsi
	movq	%r13, %rdi
	call	BIO_new_file@PLT
	movq	%rax, pwdbio.27474(%rip)
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L103
	movq	bio_err(%rip), %rdi
	movq	%r13, %rdx
	leaq	.LC12(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L133:
	movq	bio_err(%rip), %rdi
	movq	%r13, %rdx
	leaq	.LC9(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L131:
	movq	bio_err(%rip), %rdi
	leaq	.LC17(%rip), %rsi
	xorl	%eax, %eax
	xorl	%r12d, %r12d
	call	BIO_printf@PLT
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L109:
	movq	bio_err(%rip), %rdi
	movq	%r13, %rdx
	xorl	%eax, %eax
	xorl	%r12d, %r12d
	leaq	.LC16(%rip), %rsi
	call	BIO_printf@PLT
	jmp	.L97
.L132:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1584:
	.size	app_get_pass, .-app_get_pass
	.p2align 4
	.type	set_multi_opts.part.0, @function
set_multi_opts.part.0:
.LFB1666:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	xorl	%ebx, %ebx
	subq	$24, %rsp
	movq	%rdi, -64(%rbp)
	movq	%rsi, %rdi
	call	X509V3_parse_list@PLT
	movl	$1, -56(%rbp)
	movq	%rax, %r12
	movq	%r12, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jge	.L147
	.p2align 4,,10
	.p2align 3
.L142:
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	OPENSSL_sk_value@PLT
	movq	8(%rax), %r14
	movzbl	(%r14), %eax
	cmpb	$45, %al
	je	.L148
	cmpb	$43, %al
	movb	$1, -49(%rbp)
	sete	%al
	movzbl	%al, %eax
	addq	%rax, %r14
.L137:
	movq	0(%r13), %rsi
	testq	%rsi, %rsi
	je	.L144
	movq	%r13, %r15
	jmp	.L141
	.p2align 4,,10
	.p2align 3
.L139:
	movq	24(%r15), %rsi
	addq	$24, %r15
	testq	%rsi, %rsi
	je	.L144
.L141:
	movq	%r14, %rdi
	call	strcasecmp@PLT
	testl	%eax, %eax
	jne	.L139
	movq	16(%r15), %rax
	movq	-64(%rbp), %rcx
	notq	%rax
	andq	(%rcx), %rax
	cmpb	$0, -49(%rbp)
	movq	%rax, (%rcx)
	je	.L140
	orq	8(%r15), %rax
	movq	%rax, (%rcx)
.L138:
	movq	%r12, %rdi
	addl	$1, %ebx
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jl	.L142
.L147:
	movq	X509V3_conf_free@GOTPCREL(%rip), %rsi
	movq	%r12, %rdi
	call	OPENSSL_sk_pop_free@PLT
	movl	-56(%rbp), %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L144:
	.cfi_restore_state
	movl	$0, -56(%rbp)
	jmp	.L138
	.p2align 4,,10
	.p2align 3
.L148:
	movb	$0, -49(%rbp)
	addq	$1, %r14
	jmp	.L137
	.p2align 4,,10
	.p2align 3
.L140:
	movq	8(%r15), %rdx
	notq	%rdx
	andq	%rdx, %rax
	movq	-64(%rbp), %rdx
	movq	%rax, (%rdx)
	jmp	.L138
	.cfi_endproc
.LFE1666:
	.size	set_multi_opts.part.0, .-set_multi_opts.part.0
	.section	.rodata.str1.8
	.align 8
.LC18:
	.string	"%s: Could not allocate %d bytes for %s\n"
	.text
	.p2align 4
	.type	app_malloc.part.0, @function
app_malloc.part.0:
.LFB1670:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%edi, %r12d
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	call	opt_getprog@PLT
	movq	bio_err(%rip), %rdi
	movq	%rbx, %r8
	movl	%r12d, %ecx
	movq	%rax, %rdx
	leaq	.LC18(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	movl	$1, %edi
	call	exit@PLT
	.cfi_endproc
.LFE1670:
	.size	app_malloc.part.0, .-app_malloc.part.0
	.section	.rodata.str1.1
.LC19:
	.string	""
	.section	.rodata.str1.8
	.align 8
.LC20:
	.string	"Error loading PKCS12 file for %s\n"
	.align 8
.LC21:
	.string	"Passphrase callback error for %s\n"
	.align 8
.LC22:
	.string	"Mac verify error (wrong password?) in PKCS12 file for %s\n"
	.text
	.p2align 4
	.type	load_pkcs12.constprop.0, @function
load_pkcs12.constprop.0:
.LFB1673:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%r9, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$1064, %rsp
	movq	%rsi, -1096(%rbp)
	xorl	%esi, %esi
	movq	%rcx, -1104(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	d2i_PKCS12_bio@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L162
	xorl	%edx, %edx
	leaq	.LC19(%rip), %rsi
	movq	%rax, %rdi
	call	PKCS12_verify_mac@PLT
	testl	%eax, %eax
	je	.L163
.L154:
	leaq	.LC19(%rip), %rsi
.L158:
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movq	%r12, %rdx
	movq	%r14, %rdi
	call	PKCS12_parse@PLT
	movl	%eax, %r15d
.L153:
	movq	%r14, %rdi
	call	PKCS12_free@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L164
	addq	$1064, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L163:
	.cfi_restore_state
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	PKCS12_verify_mac@PLT
	movl	%eax, %r15d
	testl	%eax, %eax
	jne	.L154
	testq	%rbx, %rbx
	leaq	-1088(%rbp), %r10
	leaq	password_callback(%rip), %rax
	movq	-1104(%rbp), %rcx
	cmove	%rax, %rbx
	movq	%r10, -1104(%rbp)
	xorl	%edx, %edx
	movq	%r10, %rdi
	movl	$1024, %esi
	call	*%rbx
	movq	-1104(%rbp), %r10
	testl	%eax, %eax
	movl	%eax, %edx
	js	.L165
	cmpl	$1023, %eax
	jg	.L157
	cltq
	movb	$0, -1088(%rbp,%rax)
.L157:
	movq	%r10, %rsi
	movq	%r14, %rdi
	movq	%r10, -1104(%rbp)
	call	PKCS12_verify_mac@PLT
	movq	-1104(%rbp), %r10
	movl	%eax, %r15d
	movq	%r10, %rsi
	testl	%eax, %eax
	jne	.L158
	movq	-1096(%rbp), %rdx
	movq	bio_err(%rip), %rdi
	leaq	.LC22(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L153
	.p2align 4,,10
	.p2align 3
.L162:
	movq	-1096(%rbp), %rdx
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
	xorl	%r15d, %r15d
	leaq	.LC20(%rip), %rsi
	call	BIO_printf@PLT
	jmp	.L153
	.p2align 4,,10
	.p2align 3
.L165:
	movq	-1096(%rbp), %rdx
	movq	bio_err(%rip), %rdi
	leaq	.LC21(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L153
.L164:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1673:
	.size	load_pkcs12.constprop.0, .-load_pkcs12.constprop.0
	.section	.rodata.str1.1
.LC23:
	.string	"http://"
	.text
	.p2align 4
	.type	load_crl_crldp, @function
load_crl_crldp:
.LFB1640:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	movq	%r14, %rdi
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r12d
	jge	.L179
	.p2align 4,,10
	.p2align 3
.L175:
	movl	%r12d, %esi
	movq	%r14, %rdi
	call	OPENSSL_sk_value@PLT
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L169
	movl	(%rax), %ebx
	testl	%ebx, %ebx
	jne	.L169
	movq	8(%rax), %r13
	jmp	.L170
	.p2align 4,,10
	.p2align 3
.L172:
	addl	$1, %ebx
.L170:
	movq	%r13, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jge	.L169
	movl	%ebx, %esi
	movq	%r13, %rdi
	leaq	-64(%rbp), %r15
	call	OPENSSL_sk_value@PLT
	movq	%r15, %rsi
	movq	%rax, %rdi
	call	GENERAL_NAME_get0_value@PLT
	cmpl	$6, -64(%rbp)
	movq	%rax, %rdi
	jne	.L172
	movq	%rax, -72(%rbp)
	call	ASN1_STRING_length@PLT
	movq	-72(%rbp), %rdi
	cmpl	$6, %eax
	jle	.L172
	call	ASN1_STRING_get0_data@PLT
	movl	$7, %ecx
	leaq	.LC23(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	jne	.L172
	movq	%r15, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	$0, -64(%rbp)
	call	load_cert_crl_http
	movq	-64(%rbp), %rax
	jmp	.L166
	.p2align 4,,10
	.p2align 3
.L169:
	movq	%r14, %rdi
	addl	$1, %r12d
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r12d
	jl	.L175
.L179:
	xorl	%eax, %eax
.L166:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L180
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L180:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1640:
	.size	load_crl_crldp, .-load_crl_crldp
	.p2align 4
	.type	crls_http_cb, @function
crls_http_cb:
.LFB1641:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L181
	movq	%r13, %rdi
	call	X509_STORE_CTX_get_current_cert@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$103, %esi
	movq	%rax, %rdi
	movq	%rax, %r14
	call	X509_get_ext_d2i@PLT
	movq	%rax, %rdi
	movq	%rax, %r13
	call	load_crl_crldp
	movq	DIST_POINT_free@GOTPCREL(%rip), %rbx
	movq	%r13, %rdi
	movq	%rax, %r15
	movq	%rbx, %rsi
	call	OPENSSL_sk_pop_free@PLT
	testq	%r15, %r15
	je	.L191
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	OPENSSL_sk_push@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$857, %esi
	movq	%r14, %rdi
	call	X509_get_ext_d2i@PLT
	movq	%rax, %rdi
	movq	%rax, %r13
	call	load_crl_crldp
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rax, %r14
	call	OPENSSL_sk_pop_free@PLT
	testq	%r14, %r14
	je	.L181
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	OPENSSL_sk_push@PLT
.L181:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L191:
	.cfi_restore_state
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	OPENSSL_sk_free@PLT
	jmp	.L181
	.cfi_endproc
.LFE1641:
	.size	crls_http_cb, .-crls_http_cb
	.section	.rodata.str1.1
.LC24:
	.string	"pass phrase"
.LC25:
	.string	"Out of memory\n"
.LC26:
	.string	"password buffer"
.LC27:
	.string	"User interface error\n"
.LC28:
	.string	"aborted!\n"
	.text
	.p2align 4
	.globl	password_callback
	.type	password_callback, @function
password_callback:
.LFB1582:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$24, %rsp
	movq	%rdi, -56(%rbp)
	movq	ui_method(%rip), %rdi
	call	UI_new_method@PLT
	testq	%rax, %rax
	je	.L205
	movq	%rax, %r13
	xorl	%edx, %edx
	testq	%rbx, %rbx
	je	.L194
	movq	8(%rbx), %rdx
.L194:
	leaq	.LC24(%rip), %rsi
	movq	%r13, %rdi
	call	UI_construct_prompt@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L216
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$1, %edx
	movl	$1, %esi
	movq	%r13, %rdi
	call	UI_ctrl@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	UI_add_user_data@PLT
	movq	-56(%rbp), %rcx
	movq	%r14, %rsi
	movq	%r13, %rdi
	leal	-1(%r12), %r9d
	movl	$4, %r8d
	movl	$2, %edx
	movl	%r9d, -60(%rbp)
	call	UI_add_input_string@PLT
	movl	%eax, %ebx
	testl	%eax, %eax
	js	.L207
	testl	%r15d, %r15d
	jne	.L217
.L207:
	xorl	%r15d, %r15d
.L196:
	testl	%ebx, %ebx
	jns	.L200
	jmp	.L198
	.p2align 4,,10
	.p2align 3
.L218:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$2, %esi
	movq	%r13, %rdi
	call	UI_ctrl@PLT
	testl	%eax, %eax
	je	.L198
.L200:
	movq	%r13, %rdi
	call	UI_process@PLT
	movl	%eax, %ebx
	testl	%eax, %eax
	js	.L218
	movl	%r12d, %esi
	movl	$332, %ecx
	leaq	.LC6(%rip), %rdx
	movq	%r15, %rdi
	call	CRYPTO_clear_free@PLT
	movq	-56(%rbp), %rdi
	call	strlen@PLT
	movl	%eax, %r12d
.L202:
	movq	%r13, %rdi
	call	UI_free@PLT
	movl	$348, %edx
	leaq	.LC6(%rip), %rsi
	movq	%r14, %rdi
	call	CRYPTO_free@PLT
.L192:
	leaq	-40(%rbp), %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L205:
	.cfi_restore_state
	xorl	%r12d, %r12d
	jmp	.L192
	.p2align 4,,10
	.p2align 3
.L198:
	movl	%r12d, %r12d
	movl	$332, %ecx
	leaq	.LC6(%rip), %rdx
	movq	%r15, %rdi
	movq	%r12, %rsi
	call	CRYPTO_clear_free@PLT
	cmpl	$-1, %ebx
	je	.L219
	cmpl	$-2, %ebx
	jne	.L215
	movq	bio_err(%rip), %rdi
	leaq	.LC28(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	-56(%rbp), %rdi
	movq	%r12, %rsi
	call	OPENSSL_cleanse@PLT
.L215:
	xorl	%r12d, %r12d
	jmp	.L202
	.p2align 4,,10
	.p2align 3
.L217:
	movslq	%r12d, %rdi
	movl	$957, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_malloc@PLT
	movl	-60(%rbp), %r9d
	testq	%rax, %rax
	movq	%rax, %r15
	je	.L220
	subq	$8, %rsp
	pushq	-56(%rbp)
	movl	$2, %edx
	movq	%rax, %rcx
	movl	$4, %r8d
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	UI_add_verify_string@PLT
	movl	%eax, %ebx
	popq	%rax
	popq	%rdx
	jmp	.L196
	.p2align 4,,10
	.p2align 3
.L219:
	movq	bio_err(%rip), %rdi
	leaq	.LC27(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	movq	-56(%rbp), %rdi
	movq	%r12, %rsi
	xorl	%r12d, %r12d
	call	OPENSSL_cleanse@PLT
	jmp	.L202
	.p2align 4,,10
	.p2align 3
.L216:
	movq	bio_err(%rip), %rdi
	leaq	.LC25(%rip), %rsi
	xorl	%eax, %eax
	xorl	%r12d, %r12d
	call	BIO_printf@PLT
	movq	%r13, %rdi
	call	UI_free@PLT
	jmp	.L192
.L220:
	leaq	.LC26(%rip), %rsi
	movl	%r12d, %edi
	call	app_malloc.part.0
	.cfi_endproc
.LFE1582:
	.size	password_callback, .-password_callback
	.section	.rodata.str1.1
.LC29:
	.string	"argv space"
	.text
	.p2align 4
	.globl	chopup_args
	.type	chopup_args, @function
chopup_args:
.LFB1568:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movl	(%rdi), %eax
	movl	$0, 4(%rdi)
	testl	%eax, %eax
	je	.L222
	movq	8(%rdi), %rdi
	xorl	%r14d, %r14d
.L223:
	movzbl	(%r12), %ebx
	testb	%bl, %bl
	je	.L225
	movq	%rdi, -56(%rbp)
	call	__ctype_b_loc@PLT
	movq	-56(%rbp), %rdi
	movq	%rax, %r13
	.p2align 4,,10
	.p2align 3
.L226:
	movq	0(%r13), %rdx
	movzbl	%bl, %eax
	testb	$32, 1(%rdx,%rax,2)
	je	.L262
	movzbl	1(%r12), %ebx
	addq	$1, %r12
.L235:
	testb	%bl, %bl
	jne	.L226
.L225:
	movq	$0, (%rdi,%r14,8)
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L262:
	.cfi_restore_state
	movl	(%r15), %eax
	cmpl	%r14d, %eax
	jle	.L263
.L228:
	leal	1(%r14), %esi
	leaq	(%rdi,%r14,8), %rdx
	cmpb	$39, %bl
	je	.L242
	cmpb	$34, %bl
	je	.L242
	movl	%esi, 4(%r15)
	movq	%r12, (%rdx)
	movzbl	(%r12), %ebx
	testb	%bl, %bl
	je	.L241
	movq	0(%r13), %rdx
	jmp	.L238
	.p2align 4,,10
	.p2align 3
.L236:
	testb	%bl, %bl
	je	.L241
.L238:
	movzbl	%bl, %eax
	movq	%r12, %rcx
	movzbl	1(%r12), %ebx
	addq	$1, %r12
	testb	$32, 1(%rdx,%rax,2)
	je	.L236
	movb	$0, (%rcx)
	movq	8(%r15), %rdi
	movslq	4(%r15), %r14
	jmp	.L235
	.p2align 4,,10
	.p2align 3
.L242:
	leaq	1(%r12), %rax
	movl	%esi, 4(%r15)
	movq	%rax, (%rdx)
	movzbl	1(%r12), %edx
	cmpb	%bl, %dl
	jne	.L261
	jmp	.L232
	.p2align 4,,10
	.p2align 3
.L264:
	movzbl	1(%rax), %edx
	addq	$1, %rax
	cmpb	%bl, %dl
	je	.L232
.L261:
	testb	%dl, %dl
	jne	.L264
.L232:
	movb	$0, (%rax)
	movzbl	1(%rax), %ebx
	leaq	1(%rax), %r12
	movq	8(%r15), %rdi
	movslq	4(%r15), %r14
	jmp	.L235
	.p2align 4,,10
	.p2align 3
.L263:
	addl	$20, %eax
	movl	$96, %ecx
	leaq	.LC6(%rip), %rdx
	movl	%eax, (%r15)
	cltq
	leaq	0(,%rax,8), %rsi
	call	CRYPTO_realloc@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L239
	movq	%rax, 8(%r15)
	movslq	4(%r15), %r14
	movzbl	(%r12), %ebx
	jmp	.L228
	.p2align 4,,10
	.p2align 3
.L241:
	movslq	%esi, %r14
	jmp	.L225
.L222:
	movl	$20, (%rdi)
	movl	$957, %edx
	movl	$160, %edi
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_malloc@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L265
	movq	%rax, 8(%r15)
	movslq	4(%r15), %r14
	jmp	.L223
.L239:
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L265:
	.cfi_restore_state
	leaq	.LC29(%rip), %rsi
	movl	$160, %edi
	call	app_malloc.part.0
	.cfi_endproc
.LFE1568:
	.size	chopup_args, .-chopup_args
	.p2align 4
	.globl	app_init
	.type	app_init, @function
app_init:
.LFB1569:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE1569:
	.size	app_init, .-app_init
	.p2align 4
	.globl	ctx_set_verify_locations
	.type	ctx_set_verify_locations, @function
ctx_set_verify_locations:
.LFB1570:
	.cfi_startproc
	endbr64
	movq	%rsi, %rax
	orq	%rdx, %rax
	jne	.L268
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movl	%r8d, %ebx
	subq	$24, %rsp
	testl	%ecx, %ecx
	je	.L269
	movl	$1, %eax
	testl	%ebx, %ebx
	je	.L278
.L267:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L278:
	.cfi_restore_state
	call	SSL_CTX_set_default_verify_dir@PLT
	testl	%eax, %eax
	setg	%al
	addq	$24, %rsp
	movzbl	%al, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L269:
	.cfi_restore_state
	movq	%rdi, -24(%rbp)
	call	SSL_CTX_set_default_verify_file@PLT
	movq	-24(%rbp), %rdi
	movl	%eax, %r8d
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jle	.L267
	movl	$1, %eax
	testl	%ebx, %ebx
	jne	.L267
	jmp	.L278
	.p2align 4,,10
	.p2align 3
.L268:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	jmp	SSL_CTX_load_verify_locations@PLT
	.cfi_endproc
.LFE1570:
	.size	ctx_set_verify_locations, .-ctx_set_verify_locations
	.p2align 4
	.globl	ctx_set_ctlog_list_file
	.type	ctx_set_ctlog_list_file, @function
ctx_set_ctlog_list_file:
.LFB1571:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L281
	jmp	SSL_CTX_set_ctlog_list_file@PLT
	.p2align 4,,10
	.p2align 3
.L281:
	jmp	SSL_CTX_set_default_ctlog_list_file@PLT
	.cfi_endproc
.LFE1571:
	.size	ctx_set_ctlog_list_file, .-ctx_set_ctlog_list_file
	.p2align 4
	.globl	set_nameopt
	.type	set_nameopt, @function
set_nameopt:
.LFB1572:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L296
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	leaq	ex_tbl.27627(%rip), %rdx
	leaq	nmflag(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	set_multi_opts.part.0
	testl	%eax, %eax
	je	.L285
	movq	nmflag(%rip), %rax
	testq	%rax, %rax
	je	.L286
	testl	$983040, %eax
	jne	.L286
	orq	$131072, %rax
	movq	%rax, nmflag(%rip)
.L286:
	movl	$1, %eax
	movb	$1, nmflag_set(%rip)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L285:
	.cfi_restore_state
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L296:
	.cfi_restore 6
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1572:
	.size	set_nameopt, .-set_nameopt
	.p2align 4
	.globl	get_nameopt
	.type	get_nameopt, @function
get_nameopt:
.LFB1573:
	.cfi_startproc
	endbr64
	cmpb	$0, nmflag_set(%rip)
	movl	$8520479, %eax
	je	.L297
	movq	nmflag(%rip), %rax
.L297:
	ret
	.cfi_endproc
.LFE1573:
	.size	get_nameopt, .-get_nameopt
	.section	.rodata.str1.8
	.align 8
.LC30:
	.string	"OpenSSL application user interface"
	.text
	.p2align 4
	.globl	setup_ui_method
	.type	setup_ui_method, @function
setup_ui_method:
.LFB1579:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	UI_null@PLT
	movq	%rax, ui_fallback_method(%rip)
	call	UI_OpenSSL@PLT
	leaq	.LC30(%rip), %rdi
	movq	%rax, ui_fallback_method(%rip)
	call	UI_create_method@PLT
	leaq	ui_open(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, ui_method(%rip)
	call	UI_method_set_opener@PLT
	movq	ui_method(%rip), %rdi
	leaq	ui_read(%rip), %rsi
	call	UI_method_set_reader@PLT
	movq	ui_method(%rip), %rdi
	leaq	ui_write(%rip), %rsi
	call	UI_method_set_writer@PLT
	movq	ui_method(%rip), %rdi
	leaq	ui_close(%rip), %rsi
	call	UI_method_set_closer@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1579:
	.size	setup_ui_method, .-setup_ui_method
	.p2align 4
	.globl	destroy_ui_method
	.type	destroy_ui_method, @function
destroy_ui_method:
.LFB1580:
	.cfi_startproc
	endbr64
	movq	ui_method(%rip), %rdi
	testq	%rdi, %rdi
	je	.L308
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	UI_destroy_method@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	movq	$0, ui_method(%rip)
	ret
	.p2align 4,,10
	.p2align 3
.L308:
	.cfi_restore 6
	ret
	.cfi_endproc
.LFE1580:
	.size	destroy_ui_method, .-destroy_ui_method
	.p2align 4
	.globl	get_ui_method
	.type	get_ui_method, @function
get_ui_method:
.LFB1581:
	.cfi_startproc
	endbr64
	movq	ui_method(%rip), %rax
	ret
	.cfi_endproc
.LFE1581:
	.size	get_ui_method, .-get_ui_method
	.p2align 4
	.globl	app_passwd
	.type	app_passwd, @function
app_passwd:
.LFB1583:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$8, %rsp
	testq	%rsi, %rsi
	je	.L313
	testq	%rdi, %rdi
	je	.L316
	call	strcmp@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	testl	%eax, %eax
	movl	%eax, %r15d
	sete	%sil
	call	app_get_pass
	movq	%rax, (%rbx)
	testq	%rax, %rax
	je	.L317
	testl	%r15d, %r15d
	jne	.L322
	movl	$2, %esi
.L321:
	movq	%r12, %rdi
	call	app_get_pass
	testq	%rax, %rax
	movq	%rax, (%r14)
	setne	%al
	movzbl	%al, %eax
.L312:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L313:
	.cfi_restore_state
	testq	%rdi, %rdi
	jne	.L333
.L316:
	testq	%rbx, %rbx
	je	.L334
	movq	$0, (%rbx)
	testq	%r12, %r12
	je	.L320
.L322:
	xorl	%esi, %esi
	jmp	.L321
	.p2align 4,,10
	.p2align 3
.L333:
	xorl	%esi, %esi
	call	app_get_pass
	movq	%rax, (%rbx)
	testq	%rax, %rax
	je	.L317
	testq	%r12, %r12
	jne	.L322
.L320:
	movl	$1, %eax
	testq	%r14, %r14
	je	.L312
	movq	$0, (%r14)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L317:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L334:
	.cfi_restore_state
	testq	%r12, %r12
	je	.L320
	xorl	%esi, %esi
	jmp	.L321
	.cfi_endproc
.LFE1583:
	.size	app_passwd, .-app_passwd
	.section	.rodata.str1.1
.LC31:
	.string	"%s: Can't load "
.LC32:
	.string	"%s: Error on line %ld of "
.LC33:
	.string	"config file \"%s\"\n"
.LC34:
	.string	"config input"
	.text
	.p2align 4
	.globl	app_load_config_bio
	.type	app_load_config_bio, @function
app_load_config_bio:
.LFB1585:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	xorl	%edi, %edi
	pushq	%r12
	subq	$24, %rsp
	.cfi_offset 12, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$-1, -48(%rbp)
	call	NCONF_new@PLT
	leaq	-48(%rbp), %rdx
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	NCONF_load_bio@PLT
	testl	%eax, %eax
	jle	.L343
.L335:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L344
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L343:
	.cfi_restore_state
	movq	-48(%rbp), %r13
	testq	%r13, %r13
	jle	.L345
	call	opt_getprog@PLT
	movq	bio_err(%rip), %rdi
	movq	%r13, %rcx
	leaq	.LC32(%rip), %rsi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	testq	%r14, %r14
	je	.L339
.L346:
	movq	%r14, %rdx
	leaq	.LC33(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L340:
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	NCONF_free@PLT
	jmp	.L335
	.p2align 4,,10
	.p2align 3
.L345:
	call	opt_getprog@PLT
	movq	bio_err(%rip), %rdi
	leaq	.LC31(%rip), %rsi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	testq	%r14, %r14
	jne	.L346
.L339:
	leaq	.LC34(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L340
.L344:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1585:
	.size	app_load_config_bio, .-app_load_config_bio
	.section	.rodata.str1.1
.LC35:
	.string	"Can't open %s, %s\n"
.LC36:
	.string	"reading"
.LC37:
	.string	"Can't open %s for %s, %s\n"
	.text
	.p2align 4
	.globl	app_load_config
	.type	app_load_config, @function
app_load_config:
.LFB1586:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L348
	cmpb	$45, (%rdi)
	je	.L366
.L350:
	leaq	.LC11(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_new_file@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	jne	.L351
	call	__errno_location@PLT
	movl	(%rax), %edi
	call	strerror@PLT
	movq	bio_err(%rip), %rdi
	movq	%r12, %rdx
	leaq	.LC36(%rip), %rcx
	movq	%rax, %r8
	leaq	.LC37(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L352
	.p2align 4,,10
	.p2align 3
.L366:
	cmpb	$0, 1(%rdi)
	jne	.L350
.L348:
	movq	stdin(%rip), %rdi
	movl	$16, %esi
	call	BIO_new_fp@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L367
.L351:
	xorl	%edi, %edi
	movq	$-1, -48(%rbp)
	call	NCONF_new@PLT
	leaq	-48(%rbp), %rdx
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%rax, %r14
	call	NCONF_load_bio@PLT
	testl	%eax, %eax
	jle	.L368
.L358:
	movq	%r13, %rdi
	call	BIO_free@PLT
.L347:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L369
	addq	$16, %rsp
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L368:
	.cfi_restore_state
	movq	-48(%rbp), %r15
	testq	%r15, %r15
	jle	.L370
	call	opt_getprog@PLT
	movq	bio_err(%rip), %rdi
	movq	%r15, %rcx
	leaq	.LC32(%rip), %rsi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L355:
	movq	bio_err(%rip), %rdi
	testq	%r12, %r12
	je	.L356
	movq	%r12, %rdx
	leaq	.LC33(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L357:
	movq	%r14, %rdi
	xorl	%r14d, %r14d
	call	NCONF_free@PLT
	jmp	.L358
	.p2align 4,,10
	.p2align 3
.L370:
	call	opt_getprog@PLT
	movq	bio_err(%rip), %rdi
	leaq	.LC31(%rip), %rsi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L355
	.p2align 4,,10
	.p2align 3
.L367:
	call	__errno_location@PLT
	movl	(%rax), %edi
	call	strerror@PLT
	movq	bio_err(%rip), %rdi
	leaq	.LC14(%rip), %rdx
	leaq	.LC35(%rip), %rsi
	movq	%rax, %rcx
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L352:
	movq	bio_err(%rip), %rdi
	xorl	%r14d, %r14d
	call	ERR_print_errors@PLT
	jmp	.L347
	.p2align 4,,10
	.p2align 3
.L356:
	leaq	.LC34(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L357
.L369:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1586:
	.size	app_load_config, .-app_load_config
	.p2align 4
	.globl	app_load_config_quiet
	.type	app_load_config_quiet, @function
app_load_config_quiet:
.LFB1587:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L372
	cmpb	$45, (%rdi)
	je	.L388
.L374:
	leaq	.LC11(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_new_file@PLT
	movq	%rax, %r13
	call	ERR_clear_error@PLT
	testq	%r13, %r13
	jne	.L389
.L383:
	xorl	%r14d, %r14d
	jmp	.L371
	.p2align 4,,10
	.p2align 3
.L388:
	cmpb	$0, 1(%rdi)
	jne	.L374
.L372:
	movq	stdin(%rip), %rdi
	movl	$16, %esi
	call	BIO_new_fp@PLT
	movq	%rax, %r13
	call	ERR_clear_error@PLT
	testq	%r13, %r13
	je	.L383
.L389:
	xorl	%edi, %edi
	movq	$-1, -48(%rbp)
	call	NCONF_new@PLT
	leaq	-48(%rbp), %rdx
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%rax, %r14
	call	NCONF_load_bio@PLT
	testl	%eax, %eax
	jle	.L390
.L377:
	movq	%r13, %rdi
	call	BIO_free@PLT
.L371:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L391
	addq	$16, %rsp
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L390:
	.cfi_restore_state
	movq	-48(%rbp), %r15
	testq	%r15, %r15
	jle	.L392
	call	opt_getprog@PLT
	movq	bio_err(%rip), %rdi
	movq	%r15, %rcx
	leaq	.LC32(%rip), %rsi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L379:
	movq	bio_err(%rip), %rdi
	testq	%r12, %r12
	je	.L380
	movq	%r12, %rdx
	leaq	.LC33(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L381:
	movq	%r14, %rdi
	xorl	%r14d, %r14d
	call	NCONF_free@PLT
	jmp	.L377
	.p2align 4,,10
	.p2align 3
.L392:
	call	opt_getprog@PLT
	movq	bio_err(%rip), %rdi
	leaq	.LC31(%rip), %rsi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L379
	.p2align 4,,10
	.p2align 3
.L380:
	leaq	.LC34(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L381
.L391:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1587:
	.size	app_load_config_quiet, .-app_load_config_quiet
	.section	.rodata.str1.1
.LC38:
	.string	"oid_section"
	.section	.rodata.str1.8
	.align 8
.LC39:
	.string	"problem loading oid section %s\n"
	.align 8
.LC40:
	.string	"problem creating object %s=%s\n"
	.text
	.p2align 4
	.globl	add_oid_section
	.type	add_oid_section, @function
add_oid_section:
.LFB1589:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	leaq	.LC38(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	call	NCONF_get_string@PLT
	testq	%rax, %rax
	je	.L402
	movq	%r13, %rdi
	movq	%rax, %rsi
	movq	%rax, %r12
	xorl	%r14d, %r14d
	call	NCONF_get_section@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	jne	.L396
	jmp	.L403
	.p2align 4,,10
	.p2align 3
.L398:
	movl	%r14d, %esi
	movq	%r13, %rdi
	call	OPENSSL_sk_value@PLT
	movq	8(%rax), %rsi
	movq	16(%rax), %rdi
	movq	%rax, %rbx
	movq	%rsi, %rdx
	call	OBJ_create@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	je	.L404
	addl	$1, %r14d
.L396:
	movq	%r13, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r14d
	jl	.L398
	movl	$1, %r12d
.L393:
	popq	%rbx
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L404:
	.cfi_restore_state
	movq	16(%rbx), %rcx
	movq	8(%rbx), %rdx
	leaq	.LC40(%rip), %rsi
	xorl	%eax, %eax
	movq	bio_err(%rip), %rdi
	call	BIO_printf@PLT
	popq	%rbx
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L402:
	.cfi_restore_state
	call	ERR_clear_error@PLT
	movl	$1, %r12d
	popq	%rbx
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L403:
	.cfi_restore_state
	movq	bio_err(%rip), %rdi
	movq	%r12, %rdx
	leaq	.LC39(%rip), %rsi
	xorl	%eax, %eax
	xorl	%r12d, %r12d
	call	BIO_printf@PLT
	jmp	.L393
	.cfi_endproc
.LFE1589:
	.size	add_oid_section, .-add_oid_section
	.p2align 4
	.globl	app_malloc
	.type	app_malloc, @function
app_malloc:
.LFB1597:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movl	$957, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	leaq	.LC6(%rip), %rsi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	CRYPTO_malloc@PLT
	testq	%rax, %rax
	je	.L408
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L408:
	.cfi_restore_state
	movq	%r13, %rsi
	movl	%r12d, %edi
	call	app_malloc.part.0
	.cfi_endproc
.LFE1597:
	.size	app_malloc, .-app_malloc
	.p2align 4
	.globl	set_cert_ex
	.type	set_cert_ex, @function
set_cert_ex:
.LFB1600:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L410
	leaq	cert_tbl.27622(%rip), %rdx
	jmp	set_multi_opts.part.0
	.p2align 4,,10
	.p2align 3
.L410:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1600:
	.size	set_cert_ex, .-set_cert_ex
	.p2align 4
	.globl	set_name_ex
	.type	set_name_ex, @function
set_name_ex:
.LFB1601:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L424
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	ex_tbl.27627(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	set_multi_opts.part.0
	testl	%eax, %eax
	je	.L414
	movq	(%rbx), %rdx
	movl	$1, %eax
	testq	%rdx, %rdx
	je	.L411
	testl	$983040, %edx
	jne	.L411
	orq	$131072, %rdx
	movq	%rdx, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L414:
	.cfi_restore_state
	xorl	%eax, %eax
.L411:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L424:
	.cfi_restore 3
	.cfi_restore 6
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1601:
	.size	set_name_ex, .-set_name_ex
	.section	.rodata.str1.1
.LC41:
	.string	"none"
.LC42:
	.string	"copy"
.LC43:
	.string	"copyall"
	.text
	.p2align 4
	.globl	set_ext_copy
	.type	set_ext_copy, @function
set_ext_copy:
.LFB1602:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	leaq	.LC41(%rip), %rsi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%r12, %rdi
	call	strcasecmp@PLT
	testl	%eax, %eax
	jne	.L426
	movl	$0, (%rbx)
	movl	$1, %eax
.L425:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L426:
	.cfi_restore_state
	leaq	.LC42(%rip), %rsi
	movq	%r12, %rdi
	call	strcasecmp@PLT
	testl	%eax, %eax
	jne	.L428
	movl	$1, (%rbx)
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L428:
	.cfi_restore_state
	leaq	.LC43(%rip), %rsi
	movq	%r12, %rdi
	call	strcasecmp@PLT
	movl	%eax, %r8d
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jne	.L425
	movl	$2, (%rbx)
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1602:
	.size	set_ext_copy, .-set_ext_copy
	.p2align 4
	.globl	copy_extensions
	.type	copy_extensions, @function
copy_extensions:
.LFB1603:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%rsi, %rsi
	movl	%edx, -56(%rbp)
	sete	%dl
	testl	%eax, %eax
	sete	%al
	orb	%al, %dl
	jne	.L439
	movq	%rdi, %rbx
	testq	%rdi, %rdi
	jne	.L449
.L439:
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L449:
	.cfi_restore_state
	movq	%rsi, %rdi
	call	X509_REQ_get_extensions@PLT
	movl	$0, -52(%rbp)
	movq	%rax, %r15
	movq	%r15, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, -52(%rbp)
	jge	.L450
	.p2align 4,,10
	.p2align 3
.L438:
	movl	-52(%rbp), %esi
	movq	%r15, %rdi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %rdi
	movq	%rax, -64(%rbp)
	call	X509_EXTENSION_get_object@PLT
	movl	$-1, %edx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	movq	%rax, %r12
	call	X509_get_ext_by_OBJ@PLT
	movl	%eax, %r13d
	cmpl	$-1, %eax
	je	.L434
	cmpl	$1, -56(%rbp)
	je	.L437
	.p2align 4,,10
	.p2align 3
.L435:
	movl	%r13d, %esi
	movq	%rbx, %rdi
	call	X509_get_ext@PLT
	movl	%r13d, %esi
	movq	%rbx, %rdi
	movq	%rax, %r14
	call	X509_delete_ext@PLT
	movq	%r14, %rdi
	call	X509_EXTENSION_free@PLT
	movl	$-1, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	X509_get_ext_by_OBJ@PLT
	movl	%eax, %r13d
	cmpl	$-1, %eax
	jne	.L435
.L434:
	movq	-64(%rbp), %rsi
	movl	$-1, %edx
	movq	%rbx, %rdi
	call	X509_add_ext@PLT
	testl	%eax, %eax
	je	.L436
.L437:
	movq	%r15, %rdi
	addl	$1, -52(%rbp)
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, -52(%rbp)
	jl	.L438
.L450:
	movl	$1, %eax
.L436:
	movq	X509_EXTENSION_free@GOTPCREL(%rip), %rsi
	movq	%r15, %rdi
	movl	%eax, -52(%rbp)
	call	OPENSSL_sk_pop_free@PLT
	movl	-52(%rbp), %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1603:
	.size	copy_extensions, .-copy_extensions
	.section	.rodata.str1.1
.LC44:
	.string	"\n"
	.text
	.p2align 4
	.globl	print_name
	.type	print_name, @function
print_name:
.LFB1606:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$8, %rsp
	testq	%rsi, %rsi
	je	.L452
	call	BIO_puts@PLT
.L452:
	movq	%r13, %rax
	andl	$983040, %eax
	cmpq	$262144, %rax
	je	.L453
	xorl	%edx, %edx
	testq	%r13, %r13
	je	.L460
.L454:
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%r13, %rcx
	call	X509_NAME_print_ex@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	leaq	.LC44(%rip), %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	BIO_puts@PLT
	.p2align 4,,10
	.p2align 3
.L460:
	.cfi_restore_state
	movq	%r14, %rdi
	xorl	%esi, %esi
	call	X509_NAME_oneline@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %r13
	call	BIO_puts@PLT
	movq	%r12, %rdi
	leaq	.LC44(%rip), %rsi
	call	BIO_puts@PLT
	addq	$8, %rsp
	movq	%r13, %rdi
	movl	$1184, %edx
	popq	%r12
	leaq	.LC6(%rip), %rsi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_free@PLT
	.p2align 4,,10
	.p2align 3
.L453:
	.cfi_restore_state
	leaq	.LC44(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_puts@PLT
	movl	$4, %edx
	jmp	.L454
	.cfi_endproc
.LFE1606:
	.size	print_name, .-print_name
	.section	.rodata.str1.1
.LC45:
	.string	"subject="
.LC46:
	.string	"issuer="
	.text
	.p2align 4
	.globl	dump_cert_text
	.type	dump_cert_text, @function
dump_cert_text:
.LFB1574:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$8, %rsp
	cmpb	$0, nmflag_set(%rip)
	movq	nmflag(%rip), %r14
	jne	.L462
	movl	$8520479, %r14d
.L462:
	movq	%r13, %rdi
	call	X509_get_subject_name@PLT
	movq	%r14, %rcx
	leaq	.LC45(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	movl	$8520479, %r14d
	call	print_name
	leaq	.LC44(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_puts@PLT
	cmpb	$0, nmflag_set(%rip)
	je	.L463
	movq	nmflag(%rip), %r14
.L463:
	movq	%r13, %rdi
	call	X509_get_issuer_name@PLT
	movq	%r14, %rcx
	movq	%r12, %rdi
	leaq	.LC46(%rip), %rsi
	movq	%rax, %rdx
	call	print_name
	movq	%r12, %rdi
	leaq	.LC44(%rip), %rsi
	call	BIO_puts@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1574:
	.size	dump_cert_text, .-dump_cert_text
	.section	.rodata.str1.1
.LC47:
	.string	"\n        "
.LC48:
	.string	" "
	.section	.rodata.str1.8
	.align 8
.LC49:
	.string	"    static unsigned char %s_%d[] = {"
	.section	.rodata.str1.1
.LC50:
	.string	"\n        0x00"
.LC51:
	.string	"0x%02X,"
.LC52:
	.string	"0x%02X"
.LC53:
	.string	"\n    };\n"
	.text
	.p2align 4
	.globl	print_bignum_var
	.type	print_bignum_var, @function
print_bignum_var:
.LFB1607:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	leaq	.LC49(%rip), %rsi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$24, %rsp
	call	BIO_printf@PLT
	movq	%r12, %rdi
	call	BN_is_zero@PLT
	testl	%eax, %eax
	je	.L469
	leaq	.LC50(%rip), %rsi
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L470:
	addq	$24, %rsp
	movq	%r14, %rdi
	leaq	.LC53(%rip), %rsi
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	BIO_printf@PLT
	.p2align 4,,10
	.p2align 3
.L469:
	.cfi_restore_state
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movl	%eax, %r15d
	call	BN_bn2bin@PLT
	leaq	.LC47(%rip), %rsi
	movq	%rsi, -56(%rbp)
	movl	%eax, %r12d
	leal	-1(%rax), %r13d
	testl	%eax, %eax
	jg	.L472
	jmp	.L470
	.p2align 4,,10
	.p2align 3
.L481:
	leaq	.LC51(%rip), %rsi
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L475:
	addl	$1, %r15d
	cmpl	%r15d, %r12d
	je	.L470
	imull	$-858993459, %r15d, %eax
	leaq	.LC48(%rip), %rsi
	rorl	%eax
	cmpl	$429496730, %eax
	cmovb	-56(%rbp), %rsi
	addq	$1, %rbx
.L472:
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movzbl	(%rbx), %edx
	cmpl	%r15d, %r13d
	jg	.L481
	leaq	.LC52(%rip), %rsi
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L475
	.cfi_endproc
.LFE1607:
	.size	print_bignum_var, .-print_bignum_var
	.section	.rodata.str1.1
.LC54:
	.string	"unsigned char %s[%d] = {"
.LC55:
	.string	"\n};\n"
.LC56:
	.string	"\n    "
.LC57:
	.string	"0x%02X, "
	.text
	.p2align 4
	.globl	print_array
	.type	print_array, @function
print_array:
.LFB1608:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%edx, %r12d
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	%rcx, -56(%rbp)
	movl	%edx, %ecx
	movq	%rsi, %rdx
	leaq	.LC54(%rip), %rsi
	call	BIO_printf@PLT
	testl	%r12d, %r12d
	jle	.L489
	leal	-1(%r12), %eax
	movl	%eax, -60(%rbp)
	testl	%eax, %eax
	jle	.L494
	movq	-56(%rbp), %r14
	xorl	%ebx, %ebx
	leaq	.LC57(%rip), %r15
	.p2align 4,,10
	.p2align 3
.L488:
	imull	$-858993459, %ebx, %eax
	rorl	%eax
	cmpl	$429496729, %eax
	jbe	.L497
	movzbl	(%r14), %edx
	movq	%r15, %rsi
	movq	%r13, %rdi
	xorl	%eax, %eax
	addl	$1, %ebx
	addq	$1, %r14
	call	BIO_printf@PLT
	cmpl	-60(%rbp), %ebx
	jl	.L488
	cmpl	%r12d, %ebx
	jge	.L489
.L484:
	movslq	%ebx, %rbx
	leaq	.LC52(%rip), %r14
	jmp	.L493
	.p2align 4,,10
	.p2align 3
.L490:
	movq	-56(%rbp), %rax
	movq	%r14, %rsi
	movq	%r13, %rdi
	movzbl	(%rax,%rbx), %edx
	xorl	%eax, %eax
	addq	$1, %rbx
	call	BIO_printf@PLT
	cmpl	%ebx, %r12d
	jle	.L489
.L493:
	imull	$-858993459, %ebx, %eax
	rorl	%eax
	cmpl	$429496729, %eax
	ja	.L490
	leaq	.LC56(%rip), %rsi
	movq	%r13, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L490
	.p2align 4,,10
	.p2align 3
.L497:
	leaq	.LC56(%rip), %rsi
	movq	%r13, %rdi
	xorl	%eax, %eax
	addl	$1, %ebx
	call	BIO_printf@PLT
	movzbl	(%r14), %edx
	movq	%r15, %rsi
	movq	%r13, %rdi
	xorl	%eax, %eax
	addq	$1, %r14
	call	BIO_printf@PLT
	cmpl	%ebx, -60(%rbp)
	jg	.L488
	cmpl	%r12d, %ebx
	jl	.L484
.L489:
	addq	$24, %rsp
	movq	%r13, %rdi
	leaq	.LC55(%rip), %rsi
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	BIO_printf@PLT
	.p2align 4,,10
	.p2align 3
.L494:
	.cfi_restore_state
	xorl	%ebx, %ebx
	jmp	.L484
	.cfi_endproc
.LFE1608:
	.size	print_array, .-print_array
	.section	.rodata.str1.1
.LC58:
	.string	"Error loading file %s\n"
.LC59:
	.string	"Error loading directory %s\n"
	.text
	.p2align 4
	.globl	setup_verify
	.type	setup_verify, @function
setup_verify:
.LFB1609:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%ecx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$8, %rsp
	call	X509_STORE_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L504
	testq	%r14, %r14
	jne	.L500
	testl	%ebx, %ebx
	je	.L500
.L501:
	testq	%r13, %r13
	jne	.L505
	testl	%r15d, %r15d
	je	.L505
.L506:
	call	ERR_clear_error@PLT
.L498:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L505:
	.cfi_restore_state
	call	X509_LOOKUP_hash_dir@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	X509_STORE_add_lookup@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L504
	xorl	%r8d, %r8d
	testq	%r13, %r13
	je	.L507
	movl	$1, %ecx
	movq	%r13, %rdx
	movl	$2, %esi
	call	X509_LOOKUP_ctrl@PLT
	testl	%eax, %eax
	jne	.L506
	movq	bio_err(%rip), %rdi
	movq	%r13, %rdx
	leaq	.LC59(%rip), %rsi
	call	BIO_printf@PLT
	.p2align 4,,10
	.p2align 3
.L504:
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	X509_STORE_free@PLT
	jmp	.L498
	.p2align 4,,10
	.p2align 3
.L500:
	call	X509_LOOKUP_file@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	X509_STORE_add_lookup@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L504
	xorl	%r8d, %r8d
	testq	%r14, %r14
	je	.L502
	movl	$1, %ecx
	movq	%r14, %rdx
	movl	$1, %esi
	call	X509_LOOKUP_ctrl@PLT
	testl	%eax, %eax
	jne	.L501
	movq	bio_err(%rip), %rdi
	movq	%r14, %rdx
	leaq	.LC58(%rip), %rsi
	call	BIO_printf@PLT
	jmp	.L504
	.p2align 4,,10
	.p2align 3
.L502:
	movl	$3, %ecx
	xorl	%edx, %edx
	movl	$1, %esi
	call	X509_LOOKUP_ctrl@PLT
	jmp	.L501
	.p2align 4,,10
	.p2align 3
.L507:
	movl	$3, %ecx
	xorl	%edx, %edx
	movl	$2, %esi
	call	X509_LOOKUP_ctrl@PLT
	jmp	.L506
	.cfi_endproc
.LFE1609:
	.size	setup_verify, .-setup_verify
	.section	.rodata.str1.1
.LC60:
	.string	"auto"
.LC61:
	.string	"enabling auto ENGINE support\n"
.LC62:
	.string	"dynamic"
.LC63:
	.string	"SO_PATH"
.LC64:
	.string	"LOAD"
.LC65:
	.string	"invalid engine \"%s\"\n"
.LC66:
	.string	"SET_USER_INTERFACE"
.LC67:
	.string	"can't use that engine\n"
.LC68:
	.string	"engine \"%s\" set.\n"
	.text
	.p2align 4
	.globl	setup_engine
	.type	setup_engine, @function
setup_engine:
.LFB1611:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testq	%rdi, %rdi
	je	.L529
	movq	%rdi, %r13
	movl	%esi, %ebx
	movl	$5, %ecx
	leaq	.LC60(%rip), %rdi
	movq	%r13, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L540
	movq	%r13, %rdi
	call	ENGINE_by_id@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L541
.L523:
	testl	%ebx, %ebx
	jne	.L542
.L527:
	movq	ui_method(%rip), %rcx
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movq	%r12, %rdi
	leaq	.LC66(%rip), %rsi
	movl	$1, %r9d
	call	ENGINE_ctrl_cmd@PLT
	movl	$65535, %esi
	movq	%r12, %rdi
	call	ENGINE_set_default@PLT
	testl	%eax, %eax
	je	.L543
	movq	%r12, %rdi
	call	ENGINE_get_id@PLT
	movq	bio_err(%rip), %rdi
	leaq	.LC68(%rip), %rsi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L520:
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L542:
	.cfi_restore_state
	movq	bio_err(%rip), %rcx
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movq	%r12, %rdi
	movl	$1, %esi
	call	ENGINE_ctrl@PLT
	jmp	.L527
	.p2align 4,,10
	.p2align 3
.L529:
	xorl	%r12d, %r12d
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L540:
	.cfi_restore_state
	movq	bio_err(%rip), %rdi
	leaq	.LC61(%rip), %rsi
	xorl	%r12d, %r12d
	call	BIO_printf@PLT
	call	ENGINE_register_all_complete@PLT
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L541:
	.cfi_restore_state
	leaq	.LC62(%rip), %rdi
	call	ENGINE_by_id@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L524
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	leaq	.LC63(%rip), %rsi
	movq	%rax, %rdi
	call	ENGINE_ctrl_cmd_string@PLT
	testl	%eax, %eax
	je	.L526
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	leaq	.LC64(%rip), %rsi
	movq	%r14, %rdi
	call	ENGINE_ctrl_cmd_string@PLT
	testl	%eax, %eax
	je	.L526
	movq	%r14, %r12
	jmp	.L523
	.p2align 4,,10
	.p2align 3
.L543:
	movq	bio_err(%rip), %rdi
	leaq	.LC67(%rip), %rsi
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	ENGINE_free@PLT
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L526:
	.cfi_restore_state
	movq	%r14, %rdi
	call	ENGINE_free@PLT
.L524:
	movq	bio_err(%rip), %rdi
	movq	%r13, %rdx
	leaq	.LC65(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	jmp	.L520
	.cfi_endproc
.LFE1611:
	.size	setup_engine, .-setup_engine
	.p2align 4
	.globl	release_engine
	.type	release_engine, @function
release_engine:
.LFB1612:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L544
	jmp	ENGINE_free@PLT
	.p2align 4,,10
	.p2align 3
.L544:
	ret
	.cfi_endproc
.LFE1612:
	.size	release_engine, .-release_engine
	.p2align 4
	.globl	index_name_cmp
	.type	index_name_cmp, @function
index_name_cmp:
.LFB1617:
	.cfi_startproc
	endbr64
	movq	40(%rsi), %rsi
	movq	40(%rdi), %rdi
	jmp	strcmp@PLT
	.cfi_endproc
.LFE1617:
	.size	index_name_cmp, .-index_name_cmp
	.section	.rodata.str1.8
	.align 8
.LC69:
	.string	"unable to load number from %s\n"
	.align 8
.LC70:
	.string	"error converting number from bin to BIGNUM\n"
	.text
	.p2align 4
	.globl	load_serial
	.type	load_serial, @function
load_serial:
.LFB1622:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%esi, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$1048, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	ASN1_INTEGER_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L556
	leaq	.LC11(%rip), %rsi
	movq	%r14, %rdi
	call	BIO_new_file@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L571
	leaq	-1088(%rbp), %rdx
	movl	$1024, %ecx
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	a2i_ASN1_INTEGER@PLT
	testl	%eax, %eax
	je	.L572
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	ASN1_INTEGER_to_BN@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L573
.L569:
	testq	%rbx, %rbx
	je	.L548
	movq	%r12, (%rbx)
	xorl	%r12d, %r12d
.L548:
	movq	%r13, %rdi
	call	BIO_free@PLT
	movq	%r12, %rdi
	call	ASN1_INTEGER_free@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L574
	addq	$1048, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L571:
	.cfi_restore_state
	testl	%r15d, %r15d
	je	.L575
	call	ERR_clear_error@PLT
	call	BN_new@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L553
	xorl	%ecx, %ecx
	movl	$-1, %edx
	movl	$159, %esi
	movq	%rax, %rdi
	call	BN_rand@PLT
	testl	%eax, %eax
	je	.L553
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	BN_to_ASN1_INTEGER@PLT
	testq	%rax, %rax
	jne	.L552
	.p2align 4,,10
	.p2align 3
.L553:
	movq	bio_err(%rip), %rdi
	leaq	.LC25(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L552:
	testq	%r14, %r14
	je	.L548
	jmp	.L569
	.p2align 4,,10
	.p2align 3
.L556:
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	jmp	.L548
	.p2align 4,,10
	.p2align 3
.L572:
	movq	bio_err(%rip), %rdi
	movq	%r14, %rdx
	leaq	.LC69(%rip), %rsi
	xorl	%r14d, %r14d
	call	BIO_printf@PLT
	jmp	.L548
	.p2align 4,,10
	.p2align 3
.L575:
	movq	%r14, %rdi
	xorl	%r14d, %r14d
	call	perror@PLT
	jmp	.L548
	.p2align 4,,10
	.p2align 3
.L573:
	movq	bio_err(%rip), %rdi
	leaq	.LC70(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L548
.L574:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1622:
	.size	load_serial, .-load_serial
	.section	.rodata.str1.1
.LC71:
	.string	"file name too long\n"
.LC72:
	.string	"%s.%s"
.LC73:
	.string	"w"
	.section	.rodata.str1.8
	.align 8
.LC74:
	.string	"error converting serial to ASN.1 format\n"
	.text
	.p2align 4
	.globl	save_serial
	.type	save_serial, @function
save_serial:
.LFB1623:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$280, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strlen@PLT
	movq	%rax, %r15
	testq	%r12, %r12
	je	.L590
	movq	%r12, %rdi
	call	strlen@PLT
	leal	1(%r15,%rax), %eax
	cmpl	$255, %eax
	jg	.L578
	leaq	-320(%rbp), %r15
	movq	%r12, %r8
	movq	%r14, %rcx
	xorl	%eax, %eax
	leaq	.LC72(%rip), %rdx
	movl	$256, %esi
	movq	%r15, %rdi
	call	BIO_snprintf@PLT
.L582:
	leaq	.LC73(%rip), %rsi
	movq	%r15, %rdi
	call	BIO_new_file@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L591
	movq	%r13, %rdi
	xorl	%esi, %esi
	call	BN_to_ASN1_INTEGER@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L592
	movq	%rax, %rsi
	movq	%r12, %rdi
	movl	$1, %r14d
	call	i2a_ASN1_INTEGER@PLT
	leaq	.LC44(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_puts@PLT
	testq	%rbx, %rbx
	je	.L581
	movq	%r13, (%rbx)
	xorl	%r13d, %r13d
.L581:
	movq	%r12, %rdi
	call	BIO_free_all@PLT
	movq	%r13, %rdi
	call	ASN1_INTEGER_free@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L593
	addq	$280, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L590:
	.cfi_restore_state
	cmpl	$255, %eax
	jg	.L578
	leaq	-320(%rbp), %r15
	movl	$256, %edx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	OPENSSL_strlcpy@PLT
	jmp	.L582
	.p2align 4,,10
	.p2align 3
.L578:
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
	xorl	%r13d, %r13d
	xorl	%r14d, %r14d
	leaq	.LC71(%rip), %rsi
	xorl	%r12d, %r12d
	call	BIO_printf@PLT
	jmp	.L581
	.p2align 4,,10
	.p2align 3
.L591:
	movq	bio_err(%rip), %rdi
	xorl	%r13d, %r13d
	xorl	%r14d, %r14d
	call	ERR_print_errors@PLT
	jmp	.L581
	.p2align 4,,10
	.p2align 3
.L592:
	movq	bio_err(%rip), %rdi
	leaq	.LC74(%rip), %rsi
	xorl	%eax, %eax
	xorl	%r14d, %r14d
	call	BIO_printf@PLT
	jmp	.L581
.L593:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1623:
	.size	save_serial, .-save_serial
	.section	.rodata.str1.1
.LC75:
	.string	"unable to rename %s to %s\n"
.LC76:
	.string	"reason"
	.text
	.p2align 4
	.globl	rotate_serial
	.type	rotate_serial, @function
rotate_serial:
.LFB1624:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$536, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strlen@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	strlen@PLT
	movq	%r14, %rdi
	movq	%rax, %r15
	call	strlen@PLT
	addl	%ebx, %eax
	addl	%r15d, %ebx
	cmpl	%eax, %ebx
	cmovl	%eax, %ebx
	cmpl	$254, %ebx
	jg	.L607
	leaq	-576(%rbp), %r15
	movq	%r14, %r8
	movq	%r12, %rcx
	xorl	%eax, %eax
	leaq	.LC72(%rip), %rdx
	movl	$256, %esi
	movq	%r15, %rdi
	call	BIO_snprintf@PLT
	movl	$256, %esi
	xorl	%eax, %eax
	movq	%r13, %r8
	leaq	-320(%rbp), %r14
	movq	%r12, %rcx
	leaq	.LC72(%rip), %rdx
	movq	%r14, %rdi
	call	BIO_snprintf@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	rename@PLT
	testl	%eax, %eax
	jns	.L597
	call	__errno_location@PLT
	movl	(%rax), %eax
	cmpl	$20, %eax
	je	.L597
	cmpl	$2, %eax
	jne	.L608
.L597:
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	rename@PLT
	movl	%eax, %r8d
	movl	$1, %eax
	testl	%r8d, %r8d
	js	.L609
.L594:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L610
	addq	$536, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L607:
	.cfi_restore_state
	movq	bio_err(%rip), %rdi
	leaq	.LC71(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	xorl	%eax, %eax
	jmp	.L594
	.p2align 4,,10
	.p2align 3
.L609:
	movq	bio_err(%rip), %rdi
	movq	%r12, %rcx
	movq	%r15, %rdx
	xorl	%eax, %eax
	leaq	.LC75(%rip), %rsi
	call	BIO_printf@PLT
	leaq	.LC76(%rip), %rdi
	call	perror@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	rename@PLT
	xorl	%eax, %eax
	jmp	.L594
	.p2align 4,,10
	.p2align 3
.L608:
	movq	bio_err(%rip), %rdi
	movq	%r14, %rcx
	movq	%r12, %rdx
	xorl	%eax, %eax
	leaq	.LC75(%rip), %rsi
	call	BIO_printf@PLT
	leaq	.LC76(%rip), %rdi
	call	perror@PLT
	xorl	%eax, %eax
	jmp	.L594
.L610:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1624:
	.size	rotate_serial, .-rotate_serial
	.p2align 4
	.globl	rand_serial
	.type	rand_serial, @function
rand_serial:
.LFB1625:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	subq	$16, %rsp
	.cfi_offset 12, -32
	testq	%rdi, %rdi
	je	.L630
	xorl	%ecx, %ecx
	movl	$-1, %edx
	movl	$159, %esi
	movq	%rdi, %r12
	call	BN_rand@PLT
	testl	%eax, %eax
	je	.L617
	testq	%r13, %r13
	je	.L620
	movq	%r12, %rdi
	movq	%r13, %rsi
	xorl	%r12d, %r12d
	call	BN_to_ASN1_INTEGER@PLT
	testq	%rax, %rax
	setne	%r12b
	jmp	.L611
	.p2align 4,,10
	.p2align 3
.L630:
	call	BN_new@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L613
.L617:
	xorl	%r12d, %r12d
.L611:
	addq	$16, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L620:
	.cfi_restore_state
	movl	$1, %r12d
	jmp	.L611
.L613:
	xorl	%ecx, %ecx
	orl	$-1, %edx
	movl	$159, %esi
	movq	%rax, -24(%rbp)
	call	BN_rand@PLT
	movq	-24(%rbp), %rdi
	testl	%eax, %eax
	movl	%eax, %r12d
	je	.L615
	movl	$1, %r12d
	testq	%r13, %r13
	je	.L615
	movq	%r13, %rsi
	movq	%rdi, -24(%rbp)
	xorl	%r12d, %r12d
	call	BN_to_ASN1_INTEGER@PLT
	movq	-24(%rbp), %rdi
	testq	%rax, %rax
	setne	%r12b
.L615:
	call	BN_free@PLT
	addq	$16, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1625:
	.size	rand_serial, .-rand_serial
	.section	.rodata.str1.8
	.align 8
.LC77:
	.string	"error creating serial number index:(%ld,%ld,%ld)\n"
	.align 8
.LC78:
	.string	"error creating name index:(%ld,%ld,%ld)\n"
	.text
	.p2align 4
	.globl	index_index
	.type	index_index, @function
index_index:
.LFB1627:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	leaq	index_serial_LHASH_COMP(%rip), %r8
	movl	$3, %esi
	leaq	index_serial_LHASH_HASH(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	8(%rdi), %rdi
	call	TXT_DB_create_index@PLT
	testl	%eax, %eax
	je	.L637
	movl	(%rbx), %eax
	testl	%eax, %eax
	jne	.L634
.L635:
	movl	$1, %r12d
.L631:
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L634:
	.cfi_restore_state
	movq	8(%rbx), %rdi
	leaq	index_name_LHASH_COMP(%rip), %r8
	leaq	index_name_LHASH_HASH(%rip), %rcx
	movl	$5, %esi
	leaq	index_name_qual(%rip), %rdx
	call	TXT_DB_create_index@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	jne	.L635
	movq	8(%rbx), %rax
	movq	bio_err(%rip), %rdi
	leaq	.LC78(%rip), %rsi
	movq	40(%rax), %rcx
	movq	32(%rax), %rdx
	movq	48(%rax), %r8
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L631
	.p2align 4,,10
	.p2align 3
.L637:
	movl	%eax, %r12d
	movq	8(%rbx), %rax
	movq	bio_err(%rip), %rdi
	leaq	.LC77(%rip), %rsi
	movq	40(%rax), %rcx
	movq	32(%rax), %rdx
	movq	48(%rax), %r8
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1627:
	.size	index_index, .-index_index
	.section	.rodata.str1.1
.LC79:
	.string	"yes"
.LC80:
	.string	"no"
.LC81:
	.string	"%s.attr"
.LC82:
	.string	"%s.attr.%s"
.LC83:
	.string	"unable to open '%s'\n"
.LC84:
	.string	"unique_subject = %s\n"
	.text
	.p2align 4
	.globl	save_index
	.type	save_index, @function
save_index:
.LFB1628:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$808, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strlen@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	strlen@PLT
	addl	%eax, %ebx
	cmpl	$249, %ebx
	jg	.L648
	leaq	-320(%rbp), %rax
	movq	%r12, %rcx
	leaq	.LC81(%rip), %rdx
	movl	$256, %esi
	movq	%rax, %rdi
	movq	%rax, -848(%rbp)
	xorl	%eax, %eax
	leaq	-576(%rbp), %r15
	call	BIO_snprintf@PLT
	movq	%r13, %r8
	movq	%r12, %rcx
	movl	$256, %esi
	leaq	-832(%rbp), %rbx
	leaq	.LC82(%rip), %rdx
	movq	%r15, %rdi
	xorl	%eax, %eax
	call	BIO_snprintf@PLT
	movq	%r13, %r8
	movq	%r12, %rcx
	movl	$256, %esi
	leaq	.LC72(%rip), %rdx
	movq	%rbx, %rdi
	xorl	%eax, %eax
	call	BIO_snprintf@PLT
	leaq	.LC73(%rip), %rsi
	movq	%rbx, %rdi
	call	BIO_new_file@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L649
	movq	8(%r14), %rsi
	movq	%rax, %rdi
	xorl	%r12d, %r12d
	call	TXT_DB_write@PLT
	movq	%r13, %rdi
	movq	%rax, -840(%rbp)
	call	BIO_free@PLT
	movq	-840(%rbp), %rdx
	testl	%edx, %edx
	jle	.L638
	leaq	.LC73(%rip), %rsi
	movq	%r15, %rdi
	call	BIO_new_file@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L650
	movl	(%r14), %eax
	leaq	.LC79(%rip), %rdx
	leaq	.LC84(%rip), %rsi
	movq	%r13, %rdi
	movl	$1, %r12d
	testl	%eax, %eax
	leaq	.LC80(%rip), %rax
	cmove	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	%r13, %rdi
	call	BIO_free@PLT
.L638:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L651
	addq	$808, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L648:
	.cfi_restore_state
	movq	bio_err(%rip), %rdi
	leaq	.LC71(%rip), %rsi
	xorl	%eax, %eax
	xorl	%r12d, %r12d
	call	BIO_printf@PLT
	jmp	.L638
	.p2align 4,,10
	.p2align 3
.L649:
	movq	%r12, %rdi
	call	perror@PLT
	movq	bio_err(%rip), %rdi
	movq	%r12, %rdx
	xorl	%eax, %eax
	leaq	.LC83(%rip), %rsi
	xorl	%r12d, %r12d
	call	BIO_printf@PLT
	jmp	.L638
	.p2align 4,,10
	.p2align 3
.L650:
	movq	-848(%rbp), %rbx
	movq	%rbx, %rdi
	call	perror@PLT
	movq	bio_err(%rip), %rdi
	movq	%rbx, %rdx
	xorl	%eax, %eax
	leaq	.LC83(%rip), %rsi
	call	BIO_printf@PLT
	jmp	.L638
.L651:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1628:
	.size	save_index, .-save_index
	.p2align 4
	.globl	rotate_index
	.type	rotate_index, @function
rotate_index:
.LFB1629:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$1336, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strlen@PLT
	movq	%r13, %rdi
	movq	%rax, %r14
	call	strlen@PLT
	movq	%rbx, %rdi
	movq	%rax, %r15
	call	strlen@PLT
	addl	%r14d, %eax
	addl	%r15d, %r14d
	cmpl	%eax, %r14d
	cmovl	%eax, %r14d
	cmpl	$249, %r14d
	jg	.L674
	leaq	-1344(%rbp), %rax
	movq	%r12, %rcx
	leaq	-320(%rbp), %r15
	movl	$256, %esi
	movq	%rax, -1352(%rbp)
	leaq	.LC81(%rip), %rdx
	xorl	%eax, %eax
	movq	%r15, %rdi
	call	BIO_snprintf@PLT
	movq	%r13, %r8
	movl	$256, %esi
	xorl	%eax, %eax
	leaq	-576(%rbp), %rcx
	leaq	.LC82(%rip), %rdx
	movq	%rcx, %rdi
	movq	%rcx, -1360(%rbp)
	movq	%r12, %rcx
	leaq	-1088(%rbp), %r14
	call	BIO_snprintf@PLT
	movq	%rbx, %r8
	movl	$256, %esi
	xorl	%eax, %eax
	leaq	-832(%rbp), %rcx
	leaq	.LC82(%rip), %rdx
	movq	%rcx, %rdi
	movq	%rcx, -1368(%rbp)
	movq	%r12, %rcx
	call	BIO_snprintf@PLT
	movq	%r13, %r8
	movq	%r12, %rcx
	movl	$256, %esi
	leaq	.LC72(%rip), %rdx
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	BIO_snprintf@PLT
	movl	$256, %esi
	xorl	%eax, %eax
	movq	%rbx, %r8
	movq	-1352(%rbp), %rdi
	movq	%r12, %rcx
	leaq	.LC72(%rip), %rdx
	call	BIO_snprintf@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	rename@PLT
	testl	%eax, %eax
	jns	.L655
	call	__errno_location@PLT
	movl	(%rax), %eax
	cmpl	$2, %eax
	je	.L655
	cmpl	$20, %eax
	jne	.L675
.L655:
	movq	-1352(%rbp), %rdi
	movq	%r12, %rsi
	call	rename@PLT
	testl	%eax, %eax
	js	.L676
	movq	-1360(%rbp), %rsi
	movq	%r15, %rdi
	call	rename@PLT
	testl	%eax, %eax
	jns	.L657
	call	__errno_location@PLT
	movl	(%rax), %eax
	cmpl	$2, %eax
	je	.L657
	cmpl	$20, %eax
	je	.L657
	movq	bio_err(%rip), %rdi
	movq	-1360(%rbp), %rcx
	movq	%r15, %rdx
	xorl	%eax, %eax
	leaq	.LC75(%rip), %rsi
	call	BIO_printf@PLT
	leaq	.LC76(%rip), %rdi
	call	perror@PLT
.L673:
	movq	-1352(%rbp), %rsi
	movq	%r12, %rdi
	call	rename@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	rename@PLT
	xorl	%eax, %eax
	jmp	.L652
	.p2align 4,,10
	.p2align 3
.L657:
	movq	-1368(%rbp), %rdi
	movq	%r15, %rsi
	call	rename@PLT
	movl	%eax, %r8d
	movl	$1, %eax
	testl	%r8d, %r8d
	js	.L677
.L652:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L678
	addq	$1336, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L674:
	.cfi_restore_state
	movq	bio_err(%rip), %rdi
	leaq	.LC71(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	xorl	%eax, %eax
	jmp	.L652
	.p2align 4,,10
	.p2align 3
.L676:
	movq	-1352(%rbp), %rdx
	movq	bio_err(%rip), %rdi
	movq	%r12, %rcx
	xorl	%eax, %eax
	leaq	.LC75(%rip), %rsi
	call	BIO_printf@PLT
	leaq	.LC76(%rip), %rdi
	call	perror@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	rename@PLT
	xorl	%eax, %eax
	jmp	.L652
	.p2align 4,,10
	.p2align 3
.L675:
	movq	bio_err(%rip), %rdi
	movq	%r14, %rcx
	movq	%r12, %rdx
	xorl	%eax, %eax
	leaq	.LC75(%rip), %rsi
	call	BIO_printf@PLT
	leaq	.LC76(%rip), %rdi
	call	perror@PLT
	xorl	%eax, %eax
	jmp	.L652
	.p2align 4,,10
	.p2align 3
.L677:
	movq	-1368(%rbp), %rdx
	movq	bio_err(%rip), %rdi
	movq	%r15, %rcx
	xorl	%eax, %eax
	leaq	.LC75(%rip), %rsi
	call	BIO_printf@PLT
	leaq	.LC76(%rip), %rdi
	call	perror@PLT
	movq	-1360(%rbp), %rdi
	movq	%r15, %rsi
	call	rename@PLT
	jmp	.L673
.L678:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1629:
	.size	rotate_index, .-rotate_index
	.p2align 4
	.globl	free_index
	.type	free_index, @function
free_index:
.LFB1630:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L679
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	8(%rdi), %rdi
	call	TXT_DB_free@PLT
	movq	16(%r12), %rdi
	movl	$1742, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$1743, %edx
	popq	%r12
	.cfi_restore 12
	leaq	.LC6(%rip), %rsi
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_free@PLT
	.p2align 4,,10
	.p2align 3
.L679:
	ret
	.cfi_endproc
.LFE1630:
	.size	free_index, .-free_index
	.p2align 4
	.globl	parse_yesno
	.type	parse_yesno, @function
parse_yesno:
.LFB1631:
	.cfi_startproc
	endbr64
	movl	%esi, %eax
	testq	%rdi, %rdi
	je	.L685
	movzbl	(%rdi), %edx
	subl	$48, %edx
	cmpb	$73, %dl
	ja	.L685
	leaq	.L687(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rdx
	addq	%rcx, %rdx
	notrack jmp	*%rdx
	.section	.rodata
	.align 4
	.align 4
.L687:
	.long	.L688-.L687
	.long	.L686-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L688-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L688-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L686-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L686-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L688-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L688-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L686-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L686-.L687
	.text
	.p2align 4,,10
	.p2align 3
.L686:
	movl	$1, %eax
.L685:
	ret
	.p2align 4,,10
	.p2align 3
.L688:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1631:
	.size	parse_yesno, .-parse_yesno
	.section	.rodata.str1.8
	.align 8
.LC85:
	.string	"name is expected to be in the format /type0=value0/type1=value1/type2=... where characters may be escaped by \\. This name is not in that format: '%s'\n"
	.align 8
.LC86:
	.string	"%s: Hit end of string before finding the equals.\n"
	.align 8
.LC87:
	.string	"%s: escape character at end of string\n"
	.align 8
.LC88:
	.string	"%s: Skipping unknown attribute \"%s\"\n"
	.align 8
.LC89:
	.string	"%s: No value provided for Subject Attribute %s, skipped\n"
	.text
	.p2align 4
	.globl	parse_name
	.type	parse_name, @function
parse_name:
.LFB1632:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	cmpb	$47, (%rdi)
	movq	%rsi, -80(%rbp)
	movl	%edx, -64(%rbp)
	jne	.L755
	call	X509_NAME_new@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L692
	leaq	1(%r15), %r14
	movl	$1790, %edx
	leaq	.LC6(%rip), %rsi
	movq	%r14, %rdi
	call	CRYPTO_strdup@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L701
	movzbl	1(%r15), %eax
	xorl	%ebx, %ebx
	testb	%al, %al
	je	.L697
.L696:
	movq	%r12, %r15
	cmpb	$61, %al
	je	.L700
	.p2align 4,,10
	.p2align 3
.L698:
	addq	$1, %r14
	movb	%al, (%r15)
	addq	$1, %r15
	movzbl	(%r14), %eax
	testb	%al, %al
	je	.L752
	cmpb	$61, %al
	jne	.L698
	testb	%al, %al
	je	.L752
.L700:
	movb	$0, (%r15)
	movzbl	1(%r14), %eax
	leaq	1(%r15), %r11
	leaq	1(%r14), %rdx
	cmpb	$47, %al
	je	.L714
	testb	%al, %al
	je	.L714
	movl	-64(%rbp), %esi
	testl	%esi, %esi
	movq	%r11, %rsi
	setne	%r9b
	jmp	.L707
	.p2align 4,,10
	.p2align 3
.L704:
	movzbl	(%rdx), %eax
	movq	%rcx, %rdi
	movq	%rdx, %rcx
	addq	$1, %rsi
	movq	%rdi, %rdx
	movb	%al, -1(%rsi)
	movzbl	1(%rcx), %eax
	testb	%al, %al
	je	.L754
.L756:
	cmpb	$47, %al
	je	.L754
.L707:
	cmpb	$43, %al
	jne	.L719
	testb	%r9b, %r9b
	jne	.L715
.L719:
	leaq	1(%rdx), %rcx
	cmpb	$92, %al
	jne	.L704
	movzbl	1(%rdx), %eax
	testb	%al, %al
	je	.L705
	movb	%al, (%rsi)
	movzbl	1(%rcx), %eax
	addq	$1, %rsi
	addq	$2, %rdx
	testb	%al, %al
	jne	.L756
	.p2align 4,,10
	.p2align 3
.L754:
	movl	$0, -60(%rbp)
.L702:
	movb	$0, (%rsi)
	cmpb	$1, (%rdx)
	movq	%r12, %rdi
	sbbq	$-1, %rdx
	movq	%r11, -56(%rbp)
	movq	%rdx, %r14
	call	OBJ_txt2nid@PLT
	movq	-56(%rbp), %r11
	testl	%eax, %eax
	je	.L757
	cmpb	$0, 1(%r15)
	je	.L758
	movq	%r11, %rdi
	movq	%r11, -56(%rbp)
	negl	%ebx
	movl	%eax, -68(%rbp)
	call	strlen@PLT
	subq	$8, %rsp
	movq	-56(%rbp), %r11
	movl	-80(%rbp), %edx
	pushq	%rbx
	movl	-68(%rbp), %esi
	movl	$-1, %r9d
	movl	%eax, %r8d
	movq	%r11, %rcx
	movq	%r13, %rdi
	call	X509_NAME_add_entry_by_NID@PLT
	popq	%rdx
	popq	%rcx
	testl	%eax, %eax
	je	.L701
.L711:
	movzbl	(%r14), %eax
	testb	%al, %al
	je	.L697
	movl	-60(%rbp), %ebx
	jmp	.L696
	.p2align 4,,10
	.p2align 3
.L752:
	call	opt_getprog@PLT
	movq	bio_err(%rip), %rdi
	leaq	.LC86(%rip), %rsi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	.p2align 4,,10
	.p2align 3
.L701:
	movq	%r13, %rdi
	xorl	%r13d, %r13d
	call	X509_NAME_free@PLT
	movl	$1858, %edx
	leaq	.LC6(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
.L692:
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L715:
	.cfi_restore_state
	movl	$1, -60(%rbp)
	jmp	.L702
	.p2align 4,,10
	.p2align 3
.L705:
	call	opt_getprog@PLT
	movq	bio_err(%rip), %rdi
	leaq	.LC87(%rip), %rsi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L701
	.p2align 4,,10
	.p2align 3
.L757:
	call	opt_getprog@PLT
	movq	bio_err(%rip), %rdi
	movq	%r12, %rcx
	leaq	.LC88(%rip), %rsi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L711
.L755:
	movq	%rdi, %rdx
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
	xorl	%r13d, %r13d
	leaq	.LC85(%rip), %rsi
	call	BIO_printf@PLT
	jmp	.L692
.L758:
	call	opt_getprog@PLT
	movq	bio_err(%rip), %rdi
	movq	%r12, %rcx
	leaq	.LC89(%rip), %rsi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L711
.L714:
	movq	%r11, %rsi
	jmp	.L754
.L697:
	movl	$1853, %edx
	leaq	.LC6(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
	jmp	.L692
	.cfi_endproc
.LFE1632:
	.size	parse_name, .-parse_name
	.p2align 4
	.globl	bio_to_mem
	.type	bio_to_mem, @function
bio_to_mem:
.LFB1633:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$1064, %rsp
	movq	%rdi, -1096(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	BIO_s_mem@PLT
	movq	%rax, %rdi
	call	BIO_new@PLT
	testq	%rax, %rax
	je	.L768
	movq	%rax, %r13
	leaq	-1088(%rbp), %r12
	jmp	.L766
	.p2align 4,,10
	.p2align 3
.L761:
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	BIO_read@PLT
	movl	%eax, %r15d
	testl	%eax, %eax
	js	.L779
	je	.L764
	movl	%eax, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	BIO_write@PLT
	cmpl	%r15d, %eax
	jne	.L779
	subl	%eax, %ebx
	je	.L764
.L766:
	cmpl	$-1, %ebx
	je	.L770
	movl	%ebx, %edx
	cmpl	$1023, %ebx
	jle	.L761
.L770:
	movl	$1024, %edx
	jmp	.L761
	.p2align 4,,10
	.p2align 3
.L779:
	movq	%r13, %rdi
	movl	$-1, %r12d
	call	BIO_free@PLT
.L759:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L780
	addq	$1064, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L764:
	.cfi_restore_state
	movq	-1096(%rbp), %rcx
	xorl	%edx, %edx
	movq	%r13, %rdi
	movl	$3, %esi
	call	BIO_ctrl@PLT
	movq	%r13, %rdi
	movl	$512, %esi
	movl	%eax, %r12d
	call	BIO_set_flags@PLT
	movq	%r13, %rdi
	call	BIO_free@PLT
	jmp	.L759
.L768:
	movl	$-1, %r12d
	jmp	.L759
.L780:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1633:
	.size	bio_to_mem, .-bio_to_mem
	.p2align 4
	.globl	pkey_ctrl_string
	.type	pkey_ctrl_string, @function
pkey_ctrl_string:
.LFB1634:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1907, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	movq	%rsi, %rdi
	pushq	%r12
	.cfi_offset 12, -32
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_strdup@PLT
	testq	%rax, %rax
	je	.L784
	movl	$58, %esi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	strchr@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L783
	movb	$0, (%rax)
	addq	$1, %rdx
.L783:
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	EVP_PKEY_CTX_ctrl_str@PLT
	movl	$1916, %edx
	leaq	.LC6(%rip), %rsi
	movq	%r12, %rdi
	movl	%eax, %r13d
	call	CRYPTO_free@PLT
.L781:
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L784:
	.cfi_restore_state
	movl	$-1, %r13d
	jmp	.L781
	.cfi_endproc
.LFE1634:
	.size	pkey_ctrl_string, .-pkey_ctrl_string
	.section	.rodata.str1.1
.LC90:
	.string	"True"
.LC91:
	.string	"False"
.LC92:
	.string	"Require explicit Policy: %s\n"
.LC93:
	.string	"Authority"
.LC94:
	.string	"%s Policies:"
.LC95:
	.string	" <empty>\n"
.LC96:
	.string	"User"
	.text
	.p2align 4
	.globl	policies_print
	.type	policies_print, @function
policies_print:
.LFB1636:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	call	X509_STORE_CTX_get0_policy_tree@PLT
	movq	%r13, %rdi
	movq	%rax, %r12
	call	X509_STORE_CTX_get_explicit_policy@PLT
	leaq	.LC90(%rip), %rdx
	movq	bio_err(%rip), %rdi
	leaq	.LC92(%rip), %rsi
	testl	%eax, %eax
	leaq	.LC91(%rip), %rax
	cmove	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	%r12, %rdi
	call	X509_policy_tree_get0_policies@PLT
	movq	bio_err(%rip), %rdi
	leaq	.LC93(%rip), %rdx
	leaq	.LC94(%rip), %rsi
	movq	%rax, %rbx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	testq	%rbx, %rbx
	je	.L791
	movq	bio_err(%rip), %rdi
	leaq	.LC44(%rip), %rsi
	xorl	%r13d, %r13d
	call	BIO_puts@PLT
	jmp	.L792
	.p2align 4,,10
	.p2align 3
.L793:
	movl	%r13d, %esi
	movq	%rbx, %rdi
	addl	$1, %r13d
	call	OPENSSL_sk_value@PLT
	movq	bio_err(%rip), %rdi
	movl	$2, %edx
	movq	%rax, %rsi
	call	X509_POLICY_NODE_print@PLT
.L792:
	movq	%rbx, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r13d
	jl	.L793
.L794:
	movq	%r12, %rdi
	call	X509_policy_tree_get0_user_policies@PLT
	movq	bio_err(%rip), %rdi
	leaq	.LC96(%rip), %rdx
	leaq	.LC94(%rip), %rsi
	movq	%rax, %rbx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	testq	%rbx, %rbx
	je	.L795
	movq	bio_err(%rip), %rdi
	leaq	.LC44(%rip), %rsi
	xorl	%r12d, %r12d
	call	BIO_puts@PLT
	jmp	.L796
	.p2align 4,,10
	.p2align 3
.L797:
	movl	%r12d, %esi
	movq	%rbx, %rdi
	addl	$1, %r12d
	call	OPENSSL_sk_value@PLT
	movq	bio_err(%rip), %rdi
	movl	$2, %edx
	movq	%rax, %rsi
	call	X509_POLICY_NODE_print@PLT
.L796:
	movq	%rbx, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r12d
	jl	.L797
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L791:
	.cfi_restore_state
	movq	bio_err(%rip), %rdi
	leaq	.LC95(%rip), %rsi
	call	BIO_puts@PLT
	jmp	.L794
	.p2align 4,,10
	.p2align 3
.L795:
	movq	bio_err(%rip), %rdi
	addq	$8, %rsp
	leaq	.LC95(%rip), %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	BIO_puts@PLT
	.cfi_endproc
.LFE1636:
	.size	policies_print, .-policies_print
	.section	.rodata.str1.1
.LC97:
	.string	"NPN buffer"
	.text
	.p2align 4
	.globl	next_protos_parse
	.type	next_protos_parse, @function
next_protos_parse:
.LFB1637:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	call	strlen@PLT
	movq	%rax, %rbx
	subq	$1, %rax
	cmpq	$65533, %rax
	ja	.L811
	leal	1(%rbx), %edi
	movl	$957, %edx
	leaq	.LC6(%rip), %rsi
	movq	%rdi, %r14
	call	CRYPTO_malloc@PLT
	testq	%rax, %rax
	je	.L815
	xorl	%edx, %edx
	xorl	%edi, %edi
	xorl	%ecx, %ecx
	jmp	.L804
	.p2align 4,,10
	.p2align 3
.L806:
	addq	$1, %rdx
	movq	%rdx, %r8
	subq	%rdi, %r8
	movb	%sil, (%rax,%r8)
.L808:
	cmpq	%rbx, %rdx
	ja	.L816
.L804:
	cmpq	%rdx, %rbx
	je	.L805
	movzbl	0(%r13,%rdx), %esi
	cmpb	$44, %sil
	jne	.L806
.L805:
	cmpq	%rcx, %rdx
	je	.L817
	movq	%rdx, %rsi
	subq	%rcx, %rsi
	cmpq	$255, %rsi
	ja	.L818
	movq	%rcx, %rsi
	movl	%edx, %r9d
	addq	$1, %rdx
	subl	%ecx, %r9d
	subq	%rdi, %rsi
	movq	%rdx, %rcx
	movb	%r9b, (%rax,%rsi)
	cmpq	%rbx, %rdx
	jbe	.L804
.L816:
	cmpq	%rbx, %rdi
	jnb	.L819
	addq	$1, %rbx
	subq	%rdi, %rbx
	movq	%rbx, (%r12)
.L802:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L817:
	.cfi_restore_state
	leaq	1(%rcx), %rdx
	addq	$1, %rdi
	movq	%rdx, %rcx
	jmp	.L808
	.p2align 4,,10
	.p2align 3
.L811:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L818:
	.cfi_restore_state
	movq	%rax, %rdi
	movl	$1989, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L819:
	.cfi_restore_state
	movq	%rax, %rdi
	movl	$2000, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	xorl	%eax, %eax
	jmp	.L802
.L815:
	leaq	.LC97(%rip), %rsi
	movl	%r14d, %edi
	call	app_malloc.part.0
	.cfi_endproc
.LFE1637:
	.size	next_protos_parse, .-next_protos_parse
	.section	.rodata.str1.1
.LC98:
	.string	" NOT"
	.section	.rodata.str1.8
	.align 8
.LC99:
	.string	"Hostname %s does%s match certificate\n"
	.align 8
.LC100:
	.string	"Email %s does%s match certificate\n"
	.align 8
.LC101:
	.string	"IP %s does%s match certificate\n"
	.text
	.p2align 4
	.globl	print_cert_checks
	.type	print_cert_checks, @function
print_cert_checks:
.LFB1638:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L842
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	testq	%rdx, %rdx
	je	.L823
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	X509_check_host@PLT
	leaq	.LC19(%rip), %rcx
	movq	%r15, %rdx
	movq	%rbx, %rdi
	cmpl	$1, %eax
	leaq	.LC98(%rip), %rax
	leaq	.LC99(%rip), %rsi
	cmovne	%rax, %rcx
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L823:
	testq	%r14, %r14
	je	.L825
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	X509_check_email@PLT
	leaq	.LC19(%rip), %rcx
	movq	%r14, %rdx
	movq	%rbx, %rdi
	testl	%eax, %eax
	leaq	.LC98(%rip), %rax
	leaq	.LC100(%rip), %rsi
	cmove	%rax, %rcx
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L825:
	testq	%r13, %r13
	je	.L820
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	X509_check_ip_asc@PLT
	leaq	.LC19(%rip), %rcx
	movq	%r13, %rdx
	movq	%rbx, %rdi
	testl	%eax, %eax
	leaq	.LC98(%rip), %rax
	leaq	.LC101(%rip), %rsi
	cmove	%rax, %rcx
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	BIO_printf@PLT
	.p2align 4,,10
	.p2align 3
.L820:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L842:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.cfi_endproc
.LFE1638:
	.size	print_cert_checks, .-print_cert_checks
	.p2align 4
	.globl	store_setup_crl_download
	.type	store_setup_crl_download, @function
store_setup_crl_download:
.LFB1642:
	.cfi_startproc
	endbr64
	leaq	crls_http_cb(%rip), %rsi
	jmp	X509_STORE_set_lookup_crls@PLT
	.cfi_endproc
.LFE1642:
	.size	store_setup_crl_download, .-store_setup_crl_download
	.p2align 4
	.globl	app_tminterval
	.type	app_tminterval, @function
app_tminterval:
.LFB1643:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%edi, %r12d
	pushq	%rbx
	leaq	-64(%rbp), %rdi
	.cfi_offset 3, -32
	movl	%esi, %ebx
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	times@PLT
	testl	%ebx, %ebx
	cmovne	-64(%rbp), %rax
	movq	%rax, %rbx
	testl	%r12d, %r12d
	jne	.L848
	movq	%rax, tmstart.27984(%rip)
	pxor	%xmm0, %xmm0
.L846:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L854
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L848:
	.cfi_restore_state
	movl	$2, %edi
	call	sysconf@PLT
	subq	tmstart.27984(%rip), %rbx
	pxor	%xmm0, %xmm0
	pxor	%xmm1, %xmm1
	cvtsi2sdq	%rbx, %xmm0
	cvtsi2sdq	%rax, %xmm1
	divsd	%xmm1, %xmm0
	jmp	.L846
.L854:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1643:
	.size	app_tminterval, .-app_tminterval
	.p2align 4
	.globl	app_access
	.type	app_access, @function
app_access:
.LFB1644:
	.cfi_startproc
	endbr64
	jmp	access@PLT
	.cfi_endproc
.LFE1644:
	.size	app_access, .-app_access
	.p2align 4
	.globl	app_isdir
	.type	app_isdir, @function
app_isdir:
.LFB1645:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movl	$1, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$160, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-160(%rbp), %rdx
	call	__xstat@PLT
	movl	%eax, %r8d
	movl	$-1, %eax
	testl	%r8d, %r8d
	jne	.L856
	movl	-136(%rbp), %eax
	andl	$61440, %eax
	cmpl	$16384, %eax
	sete	%al
	movzbl	%al, %eax
.L856:
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L862
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L862:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1645:
	.size	app_isdir, .-app_isdir
	.p2align 4
	.globl	fileno_stdin
	.type	fileno_stdin, @function
fileno_stdin:
.LFB1646:
	.cfi_startproc
	endbr64
	movq	stdin(%rip), %rdi
	jmp	fileno@PLT
	.cfi_endproc
.LFE1646:
	.size	fileno_stdin, .-fileno_stdin
	.p2align 4
	.globl	fileno_stdout
	.type	fileno_stdout, @function
fileno_stdout:
.LFB1647:
	.cfi_startproc
	endbr64
	movq	stdout(%rip), %rdi
	jmp	fileno@PLT
	.cfi_endproc
.LFE1647:
	.size	fileno_stdout, .-fileno_stdout
	.p2align 4
	.globl	raw_read_stdin
	.type	raw_read_stdin, @function
raw_read_stdin:
.LFB1648:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	stdin(%rip), %rdi
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%esi, %ebx
	call	fileno@PLT
	movslq	%ebx, %rdx
	movq	%r12, %rsi
	movl	%eax, %edi
	call	read@PLT
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1648:
	.size	raw_read_stdin, .-raw_read_stdin
	.p2align 4
	.globl	raw_write_stdout
	.type	raw_write_stdout, @function
raw_write_stdout:
.LFB1649:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	stdout(%rip), %rdi
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%esi, %ebx
	call	fileno@PLT
	movslq	%ebx, %rdx
	movq	%r12, %rsi
	movl	%eax, %edi
	call	write@PLT
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1649:
	.size	raw_write_stdout, .-raw_write_stdout
	.p2align 4
	.globl	dup_bio_in
	.type	dup_bio_in, @function
dup_bio_in:
.LFB1651:
	.cfi_startproc
	endbr64
	movl	%edi, %esi
	movl	$16, %eax
	movq	stdin(%rip), %rdi
	andl	$32768, %esi
	cmovne	%eax, %esi
	jmp	BIO_new_fp@PLT
	.cfi_endproc
.LFE1651:
	.size	dup_bio_in, .-dup_bio_in
	.section	.rodata.str1.1
.LC103:
	.string	"HARNESS_OSSL_PREFIX"
	.text
	.p2align 4
	.globl	dup_bio_out
	.type	dup_bio_out, @function
dup_bio_out:
.LFB1652:
	.cfi_startproc
	endbr64
	andl	$32768, %edi
	movq	stdout(%rip), %r8
	je	.L881
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%r8, %rdi
	movl	$16, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	call	BIO_new_fp@PLT
	leaq	.LC103(%rip), %rdi
	movq	%rax, %r12
	call	getenv@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L872
	movq	prefix_method(%rip), %rdi
	testq	%rdi, %rdi
	je	.L882
.L875:
	call	BIO_new@PLT
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	BIO_push@PLT
	movq	%r13, %rcx
	xorl	%edx, %edx
	movl	$32768, %esi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	BIO_ctrl@PLT
.L872:
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L881:
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	xorl	%esi, %esi
	movq	%r8, %rdi
	jmp	BIO_new_fp@PLT
	.p2align 4,,10
	.p2align 3
.L882:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	call	apps_bf_prefix@PLT
	movq	%rax, prefix_method(%rip)
	movq	%rax, %rdi
	jmp	.L875
	.cfi_endproc
.LFE1652:
	.size	dup_bio_out, .-dup_bio_out
	.section	.rodata.str1.1
.LC104:
	.string	"stdout"
.LC105:
	.string	"ab"
.LC106:
	.string	"rb"
.LC107:
	.string	"a"
.LC108:
	.string	"wb"
.LC109:
	.string	"(doing something)"
.LC110:
	.string	"writing"
.LC111:
	.string	"appending"
	.section	.rodata.str1.8
	.align 8
.LC112:
	.string	"assertion failed: mode == 'a' || mode == 'r' || mode == 'w'"
	.text
	.p2align 4
	.type	bio_open_default_, @function
bio_open_default_:
.LFB1659:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%ecx, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movl	%edx, %edi
	pushq	%rbx
	.cfi_offset 3, -40
	movl	%esi, %ebx
	subq	$24, %rsp
	testq	%r12, %r12
	je	.L884
	cmpb	$45, (%r12)
	je	.L915
.L886:
	leal	-97(%rbx), %eax
	cmpb	$22, %al
	ja	.L894
	movl	$4325377, %edx
	btq	%rax, %rdx
	jnc	.L894
	cmpb	$114, %bl
	je	.L896
	cmpb	$119, %bl
	je	.L897
	xorl	%esi, %esi
	cmpb	$97, %bl
	je	.L916
.L898:
	movq	%r12, %rdi
	call	BIO_new_file@PLT
	testl	%r13d, %r13d
	jne	.L914
	testq	%rax, %rax
	jne	.L883
	call	__errno_location@PLT
	movl	(%rax), %edi
	call	strerror@PLT
	leaq	.LC36(%rip), %rcx
	movq	%rax, %r8
	cmpb	$114, %bl
	je	.L900
	leaq	.LC110(%rip), %rcx
	cmpb	$119, %bl
	je	.L900
	cmpb	$97, %bl
	leaq	.LC109(%rip), %rcx
	leaq	.LC111(%rip), %rax
	cmove	%rax, %rcx
.L900:
	movq	bio_err(%rip), %rdi
	movq	%r12, %rdx
	leaq	.LC37(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L893
	.p2align 4,,10
	.p2align 3
.L915:
	cmpb	$0, 1(%r12)
	jne	.L886
.L884:
	cmpb	$114, %bl
	jne	.L887
	movl	%edi, %esi
	movl	$16, %eax
	movq	stdin(%rip), %rdi
	andl	$32768, %esi
	cmovne	%eax, %esi
	call	BIO_new_fp@PLT
	testl	%r13d, %r13d
	jne	.L914
.L890:
	testq	%rax, %rax
	je	.L917
.L883:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L887:
	.cfi_restore_state
	call	dup_bio_out
	testl	%r13d, %r13d
	je	.L890
.L914:
	movq	%rax, -40(%rbp)
	call	ERR_clear_error@PLT
	movq	-40(%rbp), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L917:
	.cfi_restore_state
	call	__errno_location@PLT
	movl	(%rax), %edi
	call	strerror@PLT
	cmpb	$114, %bl
	leaq	.LC14(%rip), %rdx
	movq	bio_err(%rip), %rdi
	movq	%rax, %rcx
	leaq	.LC104(%rip), %rax
	leaq	.LC35(%rip), %rsi
	cmovne	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L893:
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L916:
	.cfi_restore_state
	andl	$32768, %edi
	leaq	.LC107(%rip), %rsi
	leaq	.LC105(%rip), %rax
	cmove	%rax, %rsi
	jmp	.L898
	.p2align 4,,10
	.p2align 3
.L897:
	andl	$32768, %edi
	leaq	.LC73(%rip), %rsi
	leaq	.LC108(%rip), %rax
	cmove	%rax, %rsi
	jmp	.L898
	.p2align 4,,10
	.p2align 3
.L896:
	andl	$32768, %edi
	leaq	.LC11(%rip), %rsi
	leaq	.LC106(%rip), %rax
	cmove	%rax, %rsi
	jmp	.L898
.L894:
	movl	$2555, %edx
	leaq	.LC6(%rip), %rsi
	leaq	.LC112(%rip), %rdi
	call	OPENSSL_die@PLT
	.cfi_endproc
.LFE1659:
	.size	bio_open_default_, .-bio_open_default_
	.section	.rodata.str1.1
.LC113:
	.string	"no keyfile specified\n"
.LC114:
	.string	"no engine specified\n"
.LC115:
	.string	"cannot load %s from engine\n"
	.section	.rodata.str1.8
	.align 8
.LC116:
	.string	"bad input format specified for key file\n"
	.section	.rodata.str1.1
.LC117:
	.string	"unable to load %s\n"
	.text
	.p2align 4
	.globl	load_key
	.type	load_key, @function
load_key:
.LFB1594:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %xmm0
	movq	%rdi, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%r9, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%esi, %ebx
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -72(%rbp)
	movaps	%xmm0, -64(%rbp)
	testq	%rdi, %rdi
	je	.L948
	cmpl	$8, %esi
	je	.L949
	movq	%r12, %rdi
	xorl	%ecx, %ecx
	movl	%ebx, %edx
	movl	$114, %esi
	call	bio_open_default_
	movq	%rax, %r12
	testq	%r12, %r12
	je	.L928
.L954:
	cmpl	$4, %ebx
	je	.L950
	cmpl	$32773, %ebx
	je	.L951
	cmpl	$6, %ebx
	je	.L952
	cmpl	$11, %ebx
	je	.L953
	cmpl	$12, %ebx
	jne	.L933
	leaq	-64(%rbp), %rdx
	leaq	password_callback(%rip), %rsi
	movq	%r12, %rdi
	call	b2i_PVK_bio@PLT
	movq	%rax, -72(%rbp)
	jmp	.L922
	.p2align 4,,10
	.p2align 3
.L948:
	testl	%edx, %edx
	je	.L939
	cmpl	$8, %esi
	je	.L939
	movq	stdin(%rip), %rdi
	xorl	%esi, %esi
	call	setbuf@PLT
	movl	%ebx, %esi
	movl	$16, %eax
	movq	stdin(%rip), %rdi
	andl	$32768, %esi
	cmovne	%eax, %esi
	call	BIO_new_fp@PLT
	movq	%rax, %r12
	testq	%r12, %r12
	jne	.L954
.L928:
	xorl	%r12d, %r12d
.L922:
	movq	%r12, %rdi
	call	BIO_free@PLT
	movq	-72(%rbp), %rax
	testq	%rax, %rax
	je	.L955
.L918:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L956
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L949:
	.cfi_restore_state
	movq	%r8, %r14
	testq	%r8, %r8
	je	.L957
	movq	%r8, %rdi
	call	ENGINE_init@PLT
	testl	%eax, %eax
	jne	.L958
.L924:
	movq	-72(%rbp), %r12
	testq	%r12, %r12
	jne	.L928
	movq	bio_err(%rip), %rdi
	movq	%r13, %rdx
	leaq	.LC115(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	jmp	.L922
	.p2align 4,,10
	.p2align 3
.L933:
	movq	bio_err(%rip), %rdi
	leaq	.LC116(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L922
	.p2align 4,,10
	.p2align 3
.L955:
	movq	bio_err(%rip), %rdi
	movq	%r13, %rdx
	leaq	.LC117(%rip), %rsi
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	movq	-72(%rbp), %rax
	jmp	.L918
	.p2align 4,,10
	.p2align 3
.L950:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	d2i_PrivateKey_bio@PLT
	movq	%rax, -72(%rbp)
	jmp	.L922
	.p2align 4,,10
	.p2align 3
.L951:
	leaq	-64(%rbp), %rcx
	leaq	password_callback(%rip), %rdx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	PEM_read_bio_PrivateKey@PLT
	movq	%rax, -72(%rbp)
	jmp	.L922
	.p2align 4,,10
	.p2align 3
.L952:
	leaq	-64(%rbp), %rcx
	xorl	%r9d, %r9d
	leaq	-72(%rbp), %r8
	movq	%r13, %rsi
	leaq	password_callback(%rip), %rdx
	movq	%r12, %rdi
	call	load_pkcs12.constprop.0
	jmp	.L922
	.p2align 4,,10
	.p2align 3
.L953:
	movq	%r12, %rdi
	call	b2i_PrivateKey_bio@PLT
	movq	%rax, -72(%rbp)
	jmp	.L922
	.p2align 4,,10
	.p2align 3
.L939:
	movq	bio_err(%rip), %rdi
	leaq	.LC113(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L922
	.p2align 4,,10
	.p2align 3
.L957:
	movq	bio_err(%rip), %rdi
	leaq	.LC114(%rip), %rsi
	xorl	%eax, %eax
	xorl	%r12d, %r12d
	call	BIO_printf@PLT
	jmp	.L922
	.p2align 4,,10
	.p2align 3
.L958:
	movq	ui_method(%rip), %rdx
	movq	%r14, %rdi
	leaq	-64(%rbp), %rcx
	movq	%r12, %rsi
	call	ENGINE_load_private_key@PLT
	movq	%r14, %rdi
	movq	%rax, -72(%rbp)
	call	ENGINE_finish@PLT
	jmp	.L924
.L956:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1594:
	.size	load_key, .-load_key
	.p2align 4
	.globl	load_pubkey
	.type	load_pubkey, @function
load_pubkey:
.LFB1595:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %xmm0
	movq	%rdi, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%r9, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	testq	%rdi, %rdi
	je	.L999
	cmpl	$8, %esi
	je	.L1000
	xorl	%ecx, %ecx
	movl	%ebx, %edx
	movl	$114, %esi
	call	bio_open_default_
	movq	%rax, %r12
.L967:
	testq	%r12, %r12
	je	.L995
	cmpl	$4, %ebx
	je	.L1001
	cmpl	$10, %ebx
	je	.L1002
	cmpl	$32777, %ebx
	je	.L1003
	cmpl	$32773, %ebx
	je	.L1004
	movq	%r12, %rdi
	cmpl	$11, %ebx
	je	.L978
.L994:
	call	BIO_free@PLT
.L963:
	movq	bio_err(%rip), %rdi
	movq	%r13, %rdx
	xorl	%eax, %eax
	xorl	%r14d, %r14d
	leaq	.LC117(%rip), %rsi
	call	BIO_printf@PLT
.L959:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1005
	addq	$40, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1000:
	.cfi_restore_state
	testq	%r8, %r8
	je	.L1006
	movq	ui_method(%rip), %rdx
	movq	%rdi, %rsi
	leaq	-80(%rbp), %rcx
	movq	%r8, %rdi
	call	ENGINE_load_public_key@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L1007
	xorl	%edi, %edi
	call	BIO_free@PLT
	jmp	.L959
	.p2align 4,,10
	.p2align 3
.L999:
	testl	%edx, %edx
	je	.L984
	cmpl	$8, %esi
	je	.L984
	movq	stdin(%rip), %rdi
	xorl	%esi, %esi
	call	setbuf@PLT
	movl	%ebx, %esi
	movl	$16, %eax
	movq	stdin(%rip), %rdi
	andl	$32768, %esi
	cmovne	%eax, %esi
	call	BIO_new_fp@PLT
	movq	%rax, %r12
	jmp	.L967
	.p2align 4,,10
	.p2align 3
.L978:
	call	b2i_PublicKey_bio@PLT
	movq	%rax, %r14
.L970:
	movq	%r12, %rdi
	call	BIO_free@PLT
	testq	%r14, %r14
	jne	.L959
	jmp	.L963
	.p2align 4,,10
	.p2align 3
.L1006:
	leaq	.LC114(%rip), %rsi
.L993:
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L995:
	xorl	%edi, %edi
	jmp	.L994
	.p2align 4,,10
	.p2align 3
.L1001:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	d2i_PUBKEY_bio@PLT
	movq	%rax, %r14
	jmp	.L970
	.p2align 4,,10
	.p2align 3
.L1002:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	d2i_RSAPublicKey_bio@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L996
.L975:
	call	EVP_PKEY_new@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L976
	movq	%r15, %rsi
	movq	%rax, %rdi
	call	EVP_PKEY_set1_RSA@PLT
.L976:
	movq	%r15, %rdi
	call	RSA_free@PLT
	jmp	.L970
	.p2align 4,,10
	.p2align 3
.L1003:
	leaq	-80(%rbp), %rcx
	leaq	password_callback(%rip), %rdx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	PEM_read_bio_RSAPublicKey@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	jne	.L975
.L996:
	movq	%r12, %rdi
	call	BIO_free@PLT
	jmp	.L963
	.p2align 4,,10
	.p2align 3
.L1004:
	leaq	-80(%rbp), %rcx
	leaq	password_callback(%rip), %rdx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	PEM_read_bio_PUBKEY@PLT
	movq	%rax, %r14
	jmp	.L970
	.p2align 4,,10
	.p2align 3
.L984:
	leaq	.LC113(%rip), %rsi
	jmp	.L993
	.p2align 4,,10
	.p2align 3
.L1007:
	movq	bio_err(%rip), %rdi
	movq	%r13, %rdx
	leaq	.LC115(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	xorl	%edi, %edi
	call	BIO_free@PLT
	jmp	.L963
.L1005:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1595:
	.size	load_pubkey, .-load_pubkey
	.section	.rodata.str1.1
.LC118:
	.string	"certificates"
.LC119:
	.string	"CRLs"
	.section	.rodata.str1.8
	.align 8
.LC120:
	.string	"bad input format specified for %s\n"
	.text
	.p2align 4
	.type	load_certs_crls, @function
load_certs_crls:
.LFB1596:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -88(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movhps	-88(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	cmpl	$32773, %esi
	je	.L1009
	movq	bio_err(%rip), %rdi
	movq	%rcx, %rdx
	leaq	.LC120(%rip), %rsi
	call	BIO_printf@PLT
	xorl	%eax, %eax
.L1008:
	movq	-56(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L1062
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1009:
	.cfi_restore_state
	xorl	%ecx, %ecx
	movl	$32773, %edx
	movl	$114, %esi
	movq	%r8, %r14
	movq	%r9, %r15
	call	bio_open_default_
	movq	%rax, %r12
	xorl	%eax, %eax
	testq	%r12, %r12
	je	.L1008
	xorl	%esi, %esi
	leaq	-80(%rbp), %rcx
	leaq	password_callback(%rip), %rdx
	movq	%r12, %rdi
	call	PEM_X509_INFO_read_bio@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	BIO_free@PLT
	testq	%r14, %r14
	je	.L1011
	cmpq	$0, (%r14)
	je	.L1063
.L1011:
	testq	%r15, %r15
	je	.L1015
	cmpq	$0, (%r15)
	je	.L1064
.L1015:
	xorl	%r12d, %r12d
	jmp	.L1014
	.p2align 4,,10
	.p2align 3
.L1021:
	movl	%r12d, %esi
	movq	%r13, %rdi
	call	OPENSSL_sk_value@PLT
	movq	(%rax), %rsi
	movq	%rax, %rbx
	testq	%rsi, %rsi
	je	.L1017
	testq	%r14, %r14
	je	.L1017
	movq	(%r14), %rdi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L1018
	movq	$0, (%rbx)
.L1017:
	movq	8(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L1019
	testq	%r15, %r15
	je	.L1019
	movq	(%r15), %rdi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L1020
	movq	$0, 8(%rbx)
.L1019:
	addl	$1, %r12d
.L1014:
	movq	%r13, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r12d
	jl	.L1021
	testq	%r14, %r14
	je	.L1022
	movq	(%r14), %rdi
	call	OPENSSL_sk_num@PLT
	testl	%eax, %eax
	jle	.L1023
	testq	%r15, %r15
	je	.L1056
	movq	(%r15), %rdi
	call	OPENSSL_sk_num@PLT
.L1056:
	movq	X509_INFO_free@GOTPCREL(%rip), %rsi
	movq	%r13, %rdi
	call	OPENSSL_sk_pop_free@PLT
	movl	$1, %eax
	jmp	.L1008
	.p2align 4,,10
	.p2align 3
.L1022:
	testq	%r15, %r15
	je	.L1065
	movq	(%r15), %rdi
	call	OPENSSL_sk_num@PLT
	testl	%eax, %eax
	jg	.L1056
	movq	X509_INFO_free@GOTPCREL(%rip), %rsi
	movq	%r13, %rdi
	call	OPENSSL_sk_pop_free@PLT
.L1036:
	movq	X509_CRL_free@GOTPCREL(%rip), %rsi
	movq	(%r15), %rdi
	call	OPENSSL_sk_pop_free@PLT
	movq	$0, (%r15)
	leaq	.LC119(%rip), %rdx
	jmp	.L1026
.L1063:
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, (%r14)
	testq	%rax, %rax
	jne	.L1011
	.p2align 4,,10
	.p2align 3
.L1018:
	movq	X509_INFO_free@GOTPCREL(%rip), %rsi
	movq	%r13, %rdi
	call	OPENSSL_sk_pop_free@PLT
.L1012:
	movq	X509_free@GOTPCREL(%rip), %rsi
	movq	(%r14), %rdi
	call	OPENSSL_sk_pop_free@PLT
	movq	$0, (%r14)
	testq	%r15, %r15
	je	.L1060
.L1033:
	movq	X509_CRL_free@GOTPCREL(%rip), %rsi
	movq	(%r15), %rdi
	call	OPENSSL_sk_pop_free@PLT
	movq	$0, (%r15)
.L1060:
	leaq	.LC118(%rip), %rdx
.L1026:
	movq	bio_err(%rip), %rdi
	leaq	.LC117(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	xorl	%eax, %eax
	jmp	.L1008
	.p2align 4,,10
	.p2align 3
.L1020:
	movq	X509_INFO_free@GOTPCREL(%rip), %rsi
	movq	%r13, %rdi
	call	OPENSSL_sk_pop_free@PLT
	testq	%r14, %r14
	je	.L1036
	jmp	.L1012
.L1065:
	movq	X509_INFO_free@GOTPCREL(%rip), %rsi
	movq	%r13, %rdi
	call	OPENSSL_sk_pop_free@PLT
	leaq	.LC119(%rip), %rdx
	jmp	.L1026
.L1064:
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, (%r15)
	testq	%rax, %rax
	jne	.L1015
	movq	X509_INFO_free@GOTPCREL(%rip), %rsi
	movq	%r13, %rdi
	call	OPENSSL_sk_pop_free@PLT
	testq	%r14, %r14
	je	.L1036
.L1061:
	movq	X509_free@GOTPCREL(%rip), %rsi
	movq	(%r14), %rdi
	call	OPENSSL_sk_pop_free@PLT
	movq	$0, (%r14)
	jmp	.L1033
.L1023:
	testq	%r15, %r15
	je	.L1066
	movq	(%r15), %rdi
	call	OPENSSL_sk_num@PLT
	testl	%eax, %eax
	jg	.L1056
	movq	X509_INFO_free@GOTPCREL(%rip), %rsi
	movq	%r13, %rdi
	call	OPENSSL_sk_pop_free@PLT
	jmp	.L1061
	.p2align 4,,10
	.p2align 3
.L1066:
	movq	X509_INFO_free@GOTPCREL(%rip), %rsi
	movq	%r13, %rdi
	call	OPENSSL_sk_pop_free@PLT
	movq	X509_free@GOTPCREL(%rip), %rsi
	movq	(%r14), %rdi
	call	OPENSSL_sk_pop_free@PLT
	movq	$0, (%r14)
	leaq	.LC118(%rip), %rdx
	jmp	.L1026
.L1062:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1596:
	.size	load_certs_crls, .-load_certs_crls
	.p2align 4
	.globl	load_certs
	.type	load_certs, @function
load_certs:
.LFB1598:
	.cfi_startproc
	endbr64
	movq	%rsi, %r10
	xorl	%r9d, %r9d
	movl	%edx, %esi
	movq	%rcx, %rdx
	movq	%r8, %rcx
	movq	%r10, %r8
	jmp	load_certs_crls
	.cfi_endproc
.LFE1598:
	.size	load_certs, .-load_certs
	.p2align 4
	.globl	load_crls
	.type	load_crls, @function
load_crls:
.LFB1599:
	.cfi_startproc
	endbr64
	movq	%rsi, %r9
	movl	%edx, %esi
	movq	%rcx, %rdx
	movq	%r8, %rcx
	xorl	%r8d, %r8d
	jmp	load_certs_crls
	.cfi_endproc
.LFE1599:
	.size	load_crls, .-load_crls
	.section	.rodata.str1.1
.LC121:
	.string	"')"
.LC122:
	.string	"fstat('"
.LC123:
	.string	"new DB"
.LC124:
	.string	"unique_subject"
	.text
	.p2align 4
	.globl	load_index
	.type	load_index, @function
load_index:
.LFB1626:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	leaq	.LC11(%rip), %rsi
	subq	$456, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	BIO_new_file@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1089
	xorl	%edx, %edx
	movl	$107, %esi
	leaq	-472(%rbp), %rcx
	movq	%rax, %rdi
	call	BIO_ctrl@PLT
	movq	-472(%rbp), %rdi
	call	fileno@PLT
	leaq	-464(%rbp), %rdx
	movl	$1, %edi
	movl	%eax, %esi
	call	__fxstat@PLT
	cmpl	$-1, %eax
	je	.L1090
	movl	$6, %esi
	movq	%r12, %rdi
	call	TXT_DB_read@PLT
	movq	%rax, -488(%rbp)
	testq	%rax, %rax
	je	.L1082
	leaq	-320(%rbp), %r14
	movq	%r13, %rcx
	movl	$256, %esi
	xorl	%eax, %eax
	leaq	.LC81(%rip), %rdx
	movq	%r14, %rdi
	call	BIO_snprintf@PLT
	movl	$1, %ecx
	movl	$32769, %edx
	movq	%r14, %rdi
	movl	$114, %esi
	call	bio_open_default_
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L1083
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	app_load_config_bio
	movq	%r15, %rdi
	movq	%rax, %r14
	call	BIO_free@PLT
.L1073:
	movl	$957, %edx
	leaq	.LC6(%rip), %rsi
	movl	$168, %edi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L1091
	movq	-488(%rbp), %rax
	movq	%rax, 8(%r15)
	testq	%rbx, %rbx
	je	.L1075
	movl	(%rbx), %eax
	movl	%eax, (%r15)
.L1076:
	testq	%r14, %r14
	je	.L1078
	xorl	%esi, %esi
	leaq	.LC124(%rip), %rdx
	movq	%r14, %rdi
	call	NCONF_get_string@PLT
	testq	%rax, %rax
	je	.L1078
	movzbl	(%rax), %ecx
	subl	$48, %ecx
	cmpb	$62, %cl
	ja	.L1084
	movabsq	$4629700418014806017, %rdx
	movl	$1, %eax
	salq	%cl, %rax
	testq	%rdx, %rax
	sete	%al
	movzbl	%al, %eax
.L1080:
	movl	%eax, (%r15)
.L1078:
	movl	$1589, %edx
	leaq	.LC6(%rip), %rsi
	movq	%r13, %rdi
	call	CRYPTO_strdup@PLT
	movdqa	-464(%rbp), %xmm0
	movdqa	-448(%rbp), %xmm1
	movdqa	-432(%rbp), %xmm2
	movdqa	-416(%rbp), %xmm3
	movq	%rax, 16(%r15)
	movdqa	-400(%rbp), %xmm4
	movdqa	-384(%rbp), %xmm5
	movups	%xmm0, 24(%r15)
	movdqa	-368(%rbp), %xmm6
	movdqa	-352(%rbp), %xmm7
	movups	%xmm1, 40(%r15)
	movdqa	-336(%rbp), %xmm0
	movups	%xmm2, 56(%r15)
	movups	%xmm3, 72(%r15)
	movups	%xmm4, 88(%r15)
	movups	%xmm5, 104(%r15)
	movups	%xmm6, 120(%r15)
	movups	%xmm7, 136(%r15)
	movups	%xmm0, 152(%r15)
.L1071:
	movq	%r14, %rdi
	call	NCONF_free@PLT
	xorl	%edi, %edi
	call	TXT_DB_free@PLT
	movq	%r12, %rdi
	call	BIO_free_all@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1092
	addq	$456, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1075:
	.cfi_restore_state
	movl	$1, (%r15)
	jmp	.L1076
	.p2align 4,,10
	.p2align 3
.L1090:
	call	__errno_location@PLT
	movl	$24, %esi
	movl	$2, %edi
	leaq	.LC6(%rip), %rcx
	movl	(%rax), %edx
	movl	$1556, %r8d
	call	ERR_put_error@PLT
	movq	%r13, %rdx
	movl	$3, %edi
	xorl	%eax, %eax
	leaq	.LC121(%rip), %rcx
	leaq	.LC122(%rip), %rsi
	call	ERR_add_error_data@PLT
.L1089:
	movq	bio_err(%rip), %rdi
	xorl	%r14d, %r14d
	xorl	%r15d, %r15d
	call	ERR_print_errors@PLT
	jmp	.L1071
	.p2align 4,,10
	.p2align 3
.L1082:
	xorl	%r14d, %r14d
	xorl	%r15d, %r15d
	jmp	.L1071
	.p2align 4,,10
	.p2align 3
.L1083:
	xorl	%r14d, %r14d
	jmp	.L1073
	.p2align 4,,10
	.p2align 3
.L1084:
	movl	$1, %eax
	jmp	.L1080
.L1092:
	call	__stack_chk_fail@PLT
.L1091:
	leaq	.LC123(%rip), %rsi
	movl	$168, %edi
	call	app_malloc.part.0
	.cfi_endproc
.LFE1626:
	.size	load_index, .-load_index
	.section	.rodata.str1.8
	.align 8
.LC125:
	.string	"Error configuring OpenSSL modules\n"
	.text
	.p2align 4
	.globl	app_load_modules
	.type	app_load_modules, @function
app_load_modules:
.LFB1588:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	xorl	%r13d, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	testq	%rdi, %rdi
	je	.L1106
.L1094:
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	CONF_modules_load@PLT
	testl	%eax, %eax
	jle	.L1107
	movq	%r13, %rdi
	call	NCONF_free@PLT
.L1105:
	popq	%r12
	movl	$1, %eax
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1107:
	.cfi_restore_state
	movq	bio_err(%rip), %rdi
	leaq	.LC125(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	movq	%r13, %rdi
	call	NCONF_free@PLT
	popq	%r12
	xorl	%eax, %eax
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1106:
	.cfi_restore_state
	movl	$1, %ecx
	movl	$32769, %edx
	movl	$114, %esi
	movq	default_config_file(%rip), %r12
	movq	%r12, %rdi
	call	bio_open_default_
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1105
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	app_load_config_bio
	movq	%r13, %rdi
	movq	%rax, %r12
	call	BIO_free@PLT
	testq	%r12, %r12
	je	.L1105
	movq	%r12, %r13
	jmp	.L1094
	.cfi_endproc
.LFE1588:
	.size	app_load_modules, .-app_load_modules
	.section	.rodata.str1.8
	.align 8
.LC126:
	.string	"bad input format specified for input crl\n"
	.section	.rodata.str1.1
.LC127:
	.string	"unable to load CRL\n"
	.text
	.p2align 4
	.globl	load_crl
	.type	load_crl, @function
load_crl:
.LFB1593:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -48(%rbp)
	cmpl	$13, %esi
	je	.L1130
	movl	%esi, %eax
	movl	%esi, %ebx
	andl	$32768, %eax
	testq	%rdi, %rdi
	je	.L1111
	cmpb	$45, (%rdi)
	je	.L1131
.L1113:
	testl	%eax, %eax
	je	.L1132
	leaq	.LC11(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_new_file@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	jne	.L1122
	.p2align 4,,10
	.p2align 3
.L1124:
	call	__errno_location@PLT
	movl	(%rax), %edi
	call	strerror@PLT
	movq	bio_err(%rip), %rdi
	movq	%r12, %rdx
	leaq	.LC36(%rip), %rcx
	movq	%rax, %r8
	leaq	.LC37(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L1116
	.p2align 4,,10
	.p2align 3
.L1131:
	cmpb	$0, 1(%rdi)
	jne	.L1113
.L1111:
	movq	stdin(%rip), %rdi
	testl	%eax, %eax
	jne	.L1114
	xorl	%esi, %esi
	call	BIO_new_fp@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1123
.L1115:
	cmpl	$4, %ebx
	je	.L1133
.L1122:
	cmpl	$32773, %ebx
	jne	.L1120
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	PEM_read_bio_X509_CRL@PLT
	movq	%rax, -48(%rbp)
.L1119:
	testq	%rax, %rax
	je	.L1134
.L1118:
	movq	%r13, %rdi
	call	BIO_free@PLT
	movq	-48(%rbp), %rax
.L1108:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1135
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1114:
	.cfi_restore_state
	movl	$16, %esi
	call	BIO_new_fp@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	jne	.L1122
.L1123:
	call	__errno_location@PLT
	movl	(%rax), %edi
	call	strerror@PLT
	movq	bio_err(%rip), %rdi
	leaq	.LC14(%rip), %rdx
	leaq	.LC35(%rip), %rsi
	movq	%rax, %rcx
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L1116:
	movq	bio_err(%rip), %rdi
	xorl	%r13d, %r13d
	call	ERR_print_errors@PLT
	jmp	.L1118
	.p2align 4,,10
	.p2align 3
.L1120:
	movq	bio_err(%rip), %rdi
	leaq	.LC126(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L1118
	.p2align 4,,10
	.p2align 3
.L1130:
	leaq	-48(%rbp), %rdx
	xorl	%esi, %esi
	call	load_cert_crl_http
	movq	-48(%rbp), %rax
	jmp	.L1108
	.p2align 4,,10
	.p2align 3
.L1133:
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	d2i_X509_CRL_bio@PLT
	movq	%rax, -48(%rbp)
	jmp	.L1119
	.p2align 4,,10
	.p2align 3
.L1132:
	leaq	.LC106(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_new_file@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	jne	.L1115
	jmp	.L1124
	.p2align 4,,10
	.p2align 3
.L1134:
	movq	bio_err(%rip), %rdi
	leaq	.LC127(%rip), %rsi
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	jmp	.L1118
.L1135:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1593:
	.size	load_crl, .-load_crl
	.section	.rodata.str1.1
.LC128:
	.string	"unable to load certificate\n"
	.text
	.p2align 4
	.globl	load_cert
	.type	load_cert, @function
load_cert:
.LFB1592:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -48(%rbp)
	cmpl	$13, %esi
	je	.L1166
	movl	%esi, %r12d
	movl	%esi, %ebx
	movq	%rdx, %r13
	andl	$32768, %r12d
	testq	%rdi, %rdi
	je	.L1167
	cmpb	$45, (%rdi)
	je	.L1168
.L1144:
	testl	%r12d, %r12d
	je	.L1169
	leaq	.LC11(%rip), %rsi
	movq	%r14, %rdi
	call	BIO_new_file@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L1152
	.p2align 4,,10
	.p2align 3
.L1154:
	call	__errno_location@PLT
	movl	(%rax), %edi
	call	strerror@PLT
	movq	bio_err(%rip), %rdi
	movq	%r14, %rdx
	leaq	.LC36(%rip), %rcx
	movq	%rax, %r8
	leaq	.LC37(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L1146
	.p2align 4,,10
	.p2align 3
.L1168:
	cmpb	$0, 1(%rdi)
	jne	.L1144
	movq	stdin(%rip), %rdi
	testl	%r12d, %r12d
	jne	.L1145
	xorl	%esi, %esi
	call	BIO_new_fp@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1153
.L1141:
	cmpl	$4, %ebx
	je	.L1170
.L1148:
	cmpl	$32773, %ebx
	je	.L1155
	cmpl	$6, %ebx
	jne	.L1150
	leaq	-48(%rbp), %r9
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	load_pkcs12.constprop.0
	movq	-48(%rbp), %rax
.L1142:
	testq	%rax, %rax
	je	.L1171
.L1151:
	movq	%r12, %rdi
	call	BIO_free@PLT
	movq	-48(%rbp), %rax
.L1136:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1172
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1167:
	.cfi_restore_state
	movq	stdin(%rip), %rdi
	xorl	%esi, %esi
	call	setbuf@PLT
	testl	%r12d, %r12d
	je	.L1173
	movq	stdin(%rip), %rdi
	movl	$16, %esi
	call	BIO_new_fp@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L1152
	jmp	.L1164
	.p2align 4,,10
	.p2align 3
.L1145:
	movl	$16, %esi
	call	BIO_new_fp@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1153
.L1152:
	cmpl	$32773, %ebx
	je	.L1155
.L1150:
	movq	bio_err(%rip), %rdi
	movq	%r13, %rdx
	leaq	.LC120(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L1164:
	movq	-48(%rbp), %rax
	testq	%rax, %rax
	jne	.L1151
.L1171:
	movq	bio_err(%rip), %rdi
	leaq	.LC128(%rip), %rsi
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	jmp	.L1151
	.p2align 4,,10
	.p2align 3
.L1173:
	movq	stdin(%rip), %rdi
	xorl	%esi, %esi
	call	BIO_new_fp@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1164
	cmpl	$4, %ebx
	jne	.L1148
.L1170:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	d2i_X509_bio@PLT
	movq	%rax, -48(%rbp)
	jmp	.L1142
	.p2align 4,,10
	.p2align 3
.L1155:
	xorl	%ecx, %ecx
	leaq	password_callback(%rip), %rdx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	PEM_read_bio_X509_AUX@PLT
	movq	%rax, -48(%rbp)
	jmp	.L1142
	.p2align 4,,10
	.p2align 3
.L1166:
	leaq	-48(%rbp), %rsi
	xorl	%edx, %edx
	call	load_cert_crl_http
	movq	-48(%rbp), %rax
	jmp	.L1136
	.p2align 4,,10
	.p2align 3
.L1153:
	call	__errno_location@PLT
	movl	(%rax), %edi
	call	strerror@PLT
	movq	bio_err(%rip), %rdi
	leaq	.LC14(%rip), %rdx
	leaq	.LC35(%rip), %rsi
	movq	%rax, %rcx
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L1146:
	movq	bio_err(%rip), %rdi
	xorl	%r12d, %r12d
	call	ERR_print_errors@PLT
	movq	-48(%rbp), %rax
	jmp	.L1142
	.p2align 4,,10
	.p2align 3
.L1169:
	leaq	.LC106(%rip), %rsi
	movq	%r14, %rdi
	call	BIO_new_file@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L1141
	jmp	.L1154
.L1172:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1592:
	.size	load_cert, .-load_cert
	.p2align 4
	.globl	dup_bio_err
	.type	dup_bio_err, @function
dup_bio_err:
.LFB1653:
	.cfi_startproc
	endbr64
	movl	%edi, %esi
	movl	$16, %eax
	movq	stderr(%rip), %rdi
	andl	$32768, %esi
	cmovne	%eax, %esi
	jmp	BIO_new_fp@PLT
	.cfi_endproc
.LFE1653:
	.size	dup_bio_err, .-dup_bio_err
	.p2align 4
	.globl	destroy_prefix_method
	.type	destroy_prefix_method, @function
destroy_prefix_method:
.LFB1654:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	prefix_method(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	BIO_meth_free@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	movq	$0, prefix_method(%rip)
	ret
	.cfi_endproc
.LFE1654:
	.size	destroy_prefix_method, .-destroy_prefix_method
	.p2align 4
	.globl	unbuffer
	.type	unbuffer, @function
unbuffer:
.LFB1655:
	.cfi_startproc
	endbr64
	xorl	%esi, %esi
	jmp	setbuf@PLT
	.cfi_endproc
.LFE1655:
	.size	unbuffer, .-unbuffer
	.section	.rodata.str1.8
	.align 8
.LC129:
	.string	"%s: Can't open \"%s\" for writing, %s\n"
	.text
	.p2align 4
	.globl	bio_open_owner
	.type	bio_open_owner, @function
bio_open_owner:
.LFB1658:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	testl	%edx, %edx
	je	.L1181
	testq	%rdi, %rdi
	je	.L1181
	cmpb	$45, (%rdi)
	je	.L1199
.L1183:
	movl	$384, %edx
	movl	$577, %esi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	open@PLT
	movl	%eax, %r14d
	testl	%eax, %eax
	js	.L1184
	andl	$32768, %r13d
	jne	.L1185
	leaq	.LC108(%rip), %rsi
	movl	%eax, %edi
	call	fdopen@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L1189
	movl	$1, %esi
.L1190:
	movq	%r15, %rdi
	call	BIO_new_fp@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	jne	.L1180
	call	__errno_location@PLT
	movl	(%rax), %edi
	call	strerror@PLT
	movq	%rax, %r14
	call	opt_getprog@PLT
	movq	bio_err(%rip), %rdi
	movq	%r14, %r8
	movq	%r12, %rcx
	movq	%rax, %rdx
	leaq	.LC129(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	movq	%r15, %rdi
	call	fclose@PLT
.L1180:
	popq	%r12
	movq	%r13, %rax
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1199:
	.cfi_restore_state
	cmpb	$0, 1(%rdi)
	jne	.L1183
.L1181:
	movl	%r13d, %edx
	movq	%r12, %rdi
	xorl	%ecx, %ecx
	popq	%r12
	movl	$119, %esi
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	bio_open_default_
	.p2align 4,,10
	.p2align 3
.L1185:
	.cfi_restore_state
	leaq	.LC73(%rip), %rsi
	movl	%eax, %edi
	call	fdopen@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L1189
	movl	$17, %esi
	jmp	.L1190
	.p2align 4,,10
	.p2align 3
.L1184:
	call	__errno_location@PLT
	movl	(%rax), %edi
	call	strerror@PLT
	movq	%rax, %r13
	call	opt_getprog@PLT
	movq	bio_err(%rip), %rdi
	movq	%r13, %r8
	movq	%r12, %rcx
	movq	%rax, %rdx
	leaq	.LC129(%rip), %rsi
	xorl	%eax, %eax
	xorl	%r13d, %r13d
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	popq	%r12
	movq	%r13, %rax
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1189:
	.cfi_restore_state
	call	__errno_location@PLT
	movl	(%rax), %edi
	call	strerror@PLT
	movq	%rax, %r13
	call	opt_getprog@PLT
	movq	bio_err(%rip), %rdi
	movq	%r13, %r8
	movq	%r12, %rcx
	movq	%rax, %rdx
	leaq	.LC129(%rip), %rsi
	xorl	%eax, %eax
	xorl	%r13d, %r13d
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	movl	%r14d, %edi
	call	close@PLT
	jmp	.L1180
	.cfi_endproc
.LFE1658:
	.size	bio_open_owner, .-bio_open_owner
	.p2align 4
	.globl	bio_open_default
	.type	bio_open_default, @function
bio_open_default:
.LFB1660:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movl	%edx, %edi
	subq	$8, %rsp
	testq	%r12, %r12
	je	.L1201
	cmpb	$45, (%r12)
	je	.L1236
.L1203:
	leal	-97(%rsi), %eax
	cmpb	$22, %al
	ja	.L1210
	movl	$4325377, %edx
	btq	%rax, %rdx
	jnc	.L1210
	cmpb	$114, %sil
	je	.L1212
	cmpb	$119, %sil
	je	.L1213
	cmpb	$97, %sil
	je	.L1237
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	BIO_new_file@PLT
	testq	%rax, %rax
	jne	.L1200
	call	__errno_location@PLT
	movl	(%rax), %edi
	call	strerror@PLT
	leaq	.LC109(%rip), %rcx
	movq	%rax, %r8
.L1219:
	movq	bio_err(%rip), %rdi
	movq	%r12, %rdx
	leaq	.LC37(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L1209
	.p2align 4,,10
	.p2align 3
.L1236:
	cmpb	$0, 1(%r12)
	jne	.L1203
.L1201:
	cmpb	$114, %sil
	jne	.L1204
	andl	$32768, %edi
	movl	$16, %eax
	movl	%edi, %esi
	movq	stdin(%rip), %rdi
	cmovne	%eax, %esi
	call	BIO_new_fp@PLT
	testq	%rax, %rax
	je	.L1238
.L1200:
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1204:
	.cfi_restore_state
	call	dup_bio_out
	testq	%rax, %rax
	jne	.L1200
	call	__errno_location@PLT
	movl	(%rax), %edi
	call	strerror@PLT
	leaq	.LC104(%rip), %rdx
	movq	%rax, %rcx
.L1218:
	movq	bio_err(%rip), %rdi
	leaq	.LC35(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L1209:
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1237:
	.cfi_restore_state
	andl	$32768, %edi
	leaq	.LC107(%rip), %rsi
	movq	%r12, %rdi
	leaq	.LC105(%rip), %rax
	cmove	%rax, %rsi
	call	BIO_new_file@PLT
	testq	%rax, %rax
	jne	.L1200
	call	__errno_location@PLT
	movl	(%rax), %edi
	call	strerror@PLT
	leaq	.LC111(%rip), %rcx
	movq	%rax, %r8
	jmp	.L1219
	.p2align 4,,10
	.p2align 3
.L1213:
	andl	$32768, %edi
	leaq	.LC73(%rip), %rsi
	movq	%r12, %rdi
	leaq	.LC108(%rip), %rax
	cmove	%rax, %rsi
	call	BIO_new_file@PLT
	testq	%rax, %rax
	jne	.L1200
	call	__errno_location@PLT
	movl	(%rax), %edi
	call	strerror@PLT
	leaq	.LC110(%rip), %rcx
	movq	%rax, %r8
	jmp	.L1219
	.p2align 4,,10
	.p2align 3
.L1212:
	andl	$32768, %edi
	leaq	.LC11(%rip), %rsi
	movq	%r12, %rdi
	leaq	.LC106(%rip), %rax
	cmove	%rax, %rsi
	call	BIO_new_file@PLT
	testq	%rax, %rax
	jne	.L1200
	call	__errno_location@PLT
	movl	(%rax), %edi
	call	strerror@PLT
	leaq	.LC36(%rip), %rcx
	movq	%rax, %r8
	jmp	.L1219
.L1210:
	movl	$2555, %edx
	leaq	.LC6(%rip), %rsi
	leaq	.LC112(%rip), %rdi
	call	OPENSSL_die@PLT
	.p2align 4,,10
	.p2align 3
.L1238:
	call	__errno_location@PLT
	movl	(%rax), %edi
	call	strerror@PLT
	leaq	.LC14(%rip), %rdx
	movq	%rax, %rcx
	jmp	.L1218
	.cfi_endproc
.LFE1660:
	.size	bio_open_default, .-bio_open_default
	.p2align 4
	.globl	bio_open_default_quiet
	.type	bio_open_default_quiet, @function
bio_open_default_quiet:
.LFB1661:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r8
	movl	%edx, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	testq	%r8, %r8
	je	.L1240
	cmpb	$45, (%r8)
	je	.L1262
.L1242:
	leal	-97(%rsi), %eax
	cmpb	$22, %al
	ja	.L1247
	movl	$4325377, %edx
	btq	%rax, %rdx
	jnc	.L1247
	cmpb	$114, %sil
	je	.L1249
	cmpb	$119, %sil
	je	.L1250
	xorl	%r9d, %r9d
	cmpb	$97, %sil
	je	.L1263
.L1251:
	movq	%r9, %rsi
	movq	%r8, %rdi
	call	BIO_new_file@PLT
	movq	%rax, %r12
	call	ERR_clear_error@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1262:
	.cfi_restore_state
	cmpb	$0, 1(%r8)
	jne	.L1242
.L1240:
	cmpb	$114, %sil
	jne	.L1243
	andl	$32768, %edi
	movl	$16, %eax
	movl	%edi, %esi
	movq	stdin(%rip), %rdi
	cmovne	%eax, %esi
	call	BIO_new_fp@PLT
	movq	%rax, %r12
	call	ERR_clear_error@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1243:
	.cfi_restore_state
	call	dup_bio_out
	movq	%rax, %r12
	call	ERR_clear_error@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1263:
	.cfi_restore_state
	andl	$32768, %edi
	leaq	.LC107(%rip), %r9
	leaq	.LC105(%rip), %rsi
	cmove	%rsi, %r9
	jmp	.L1251
	.p2align 4,,10
	.p2align 3
.L1250:
	andl	$32768, %edi
	leaq	.LC73(%rip), %r9
	leaq	.LC108(%rip), %rsi
	cmove	%rsi, %r9
	jmp	.L1251
	.p2align 4,,10
	.p2align 3
.L1249:
	andl	$32768, %edi
	leaq	.LC11(%rip), %r9
	leaq	.LC106(%rip), %rsi
	cmove	%rsi, %r9
	jmp	.L1251
.L1247:
	movl	$2555, %edx
	leaq	.LC6(%rip), %rsi
	leaq	.LC112(%rip), %rdi
	call	OPENSSL_die@PLT
	.cfi_endproc
.LFE1661:
	.size	bio_open_default_quiet, .-bio_open_default_quiet
	.section	.rodata.str1.1
.LC130:
	.string	"allocate async fds"
	.text
	.p2align 4
	.globl	wait_for_async
	.type	wait_for_async, @function
wait_for_async:
.LFB1662:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-200(%rbp), %r13
	pushq	%r12
	movq	%r13, %rdx
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	SSL_get_all_async_fds@PLT
	testl	%eax, %eax
	je	.L1264
	movq	-200(%rbp), %rax
	testq	%rax, %rax
	jne	.L1282
.L1264:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1283
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1282:
	.cfi_restore_state
	leal	0(,%rax,4), %r15d
	movl	$957, %edx
	leaq	.LC6(%rip), %rsi
	movslq	%r15d, %rdi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L1284
	movq	%r13, %rdx
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	SSL_get_all_async_fds@PLT
	testl	%eax, %eax
	je	.L1285
	leaq	-192(%rbp), %r12
	xorl	%eax, %eax
	movl	$16, %ecx
	movq	%r12, %rdi
#APP
# 2707 "../deps/openssl/openssl/apps/apps.c" 1
	cld; rep; stosq
# 0 "" 2
#NO_APP
	cmpq	$0, -200(%rbp)
	je	.L1273
	xorl	%r15d, %r15d
	xorl	%r13d, %r13d
	movl	$1, %ebx
	.p2align 4,,10
	.p2align 3
.L1270:
	movslq	(%r14,%r15,4), %rdi
	cmpl	%r13d, %edi
	leal	1(%rdi), %eax
	cmovge	%eax, %r13d
	call	__fdelt_chk@PLT
	movl	(%r14,%r15,4), %ecx
	addq	$1, %r15
	movq	%rax, %r8
	movl	%ecx, %eax
	sarl	$31, %eax
	shrl	$26, %eax
	addl	%eax, %ecx
	andl	$63, %ecx
	subl	%eax, %ecx
	movq	%rbx, %rax
	salq	%cl, %rax
	orq	%rax, -192(%rbp,%r8,8)
	cmpq	%r15, -200(%rbp)
	ja	.L1270
.L1268:
	xorl	%edx, %edx
	movq	%r12, %rsi
	movl	%r13d, %edi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	call	select@PLT
	movl	$2714, %edx
	leaq	.LC6(%rip), %rsi
	movq	%r14, %rdi
	call	CRYPTO_free@PLT
	jmp	.L1264
	.p2align 4,,10
	.p2align 3
.L1285:
	movl	$2703, %edx
	leaq	.LC6(%rip), %rsi
	movq	%r14, %rdi
	call	CRYPTO_free@PLT
	jmp	.L1264
	.p2align 4,,10
	.p2align 3
.L1273:
	xorl	%r13d, %r13d
	jmp	.L1268
.L1283:
	call	__stack_chk_fail@PLT
.L1284:
	leaq	.LC130(%rip), %rsi
	movl	%r15d, %edi
	call	app_malloc.part.0
	.cfi_endproc
.LFE1662:
	.size	wait_for_async, .-wait_for_async
	.p2align 4
	.globl	corrupt_signature
	.type	corrupt_signature, @function
corrupt_signature:
.LFB1663:
	.cfi_startproc
	endbr64
	movslq	(%rdi), %rax
	movq	8(%rdi), %rdx
	xorb	$1, -1(%rdx,%rax)
	ret
	.cfi_endproc
.LFE1663:
	.size	corrupt_signature, .-corrupt_signature
	.section	.rodata.str1.1
.LC131:
	.string	"today"
	.text
	.p2align 4
	.globl	set_cert_times
	.type	set_cert_times, @function
set_cert_times:
.LFB1664:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%ecx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	testq	%rsi, %rsi
	je	.L1288
	movl	$6, %ecx
	leaq	.LC131(%rip), %rdi
	movq	%rsi, %r12
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L1289
.L1288:
	movq	%r13, %rdi
	call	X509_getm_notBefore@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	X509_gmtime_adj@PLT
	testq	%rax, %rax
	je	.L1290
.L1294:
	testq	%r14, %r14
	je	.L1306
	movq	%r13, %rdi
	call	X509_getm_notAfter@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	ASN1_TIME_set_string_X509@PLT
	popq	%r12
	popq	%r13
	testl	%eax, %eax
	popq	%r14
	popq	%r15
	setne	%al
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movzbl	%al, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1289:
	.cfi_restore_state
	movq	%r13, %rdi
	call	X509_getm_notBefore@PLT
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	ASN1_TIME_set_string_X509@PLT
	testl	%eax, %eax
	jne	.L1294
.L1290:
	popq	%r12
	xorl	%eax, %eax
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1306:
	.cfi_restore_state
	movq	%r13, %rdi
	call	X509_getm_notAfter@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	%r15d, %esi
	movq	%rax, %rdi
	call	X509_time_adj_ex@PLT
	popq	%r12
	popq	%r13
	testq	%rax, %rax
	popq	%r14
	popq	%r15
	setne	%al
	popq	%rbp
	.cfi_def_cfa 7, 8
	movzbl	%al, %eax
	ret
	.cfi_endproc
.LFE1664:
	.size	set_cert_times, .-set_cert_times
	.p2align 4
	.globl	make_uppercase
	.type	make_uppercase, @function
make_uppercase:
.LFB1665:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movzbl	(%rdi), %ebx
	testb	%bl, %bl
	je	.L1307
	movq	%rdi, %r12
	call	__ctype_toupper_loc@PLT
	.p2align 4,,10
	.p2align 3
.L1309:
	movq	(%rax), %rdx
	addq	$1, %r12
	movl	(%rdx,%rbx,4), %edx
	movb	%dl, -1(%r12)
	movzbl	(%r12), %ebx
	testb	%bl, %bl
	jne	.L1309
.L1307:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1665:
	.size	make_uppercase, .-make_uppercase
	.local	tmstart.27984
	.comm	tmstart.27984,8,8
	.section	.rodata.str1.1
.LC132:
	.string	"esc_2253"
.LC133:
	.string	"esc_2254"
.LC134:
	.string	"esc_ctrl"
.LC135:
	.string	"esc_msb"
.LC136:
	.string	"use_quote"
.LC137:
	.string	"utf8"
.LC138:
	.string	"ignore_type"
.LC139:
	.string	"show_type"
.LC140:
	.string	"dump_all"
.LC141:
	.string	"dump_nostr"
.LC142:
	.string	"dump_der"
.LC143:
	.string	"compat"
.LC144:
	.string	"sep_comma_plus"
.LC145:
	.string	"sep_comma_plus_space"
.LC146:
	.string	"sep_semi_plus_space"
.LC147:
	.string	"sep_multiline"
.LC148:
	.string	"dn_rev"
.LC149:
	.string	"nofname"
.LC150:
	.string	"sname"
.LC151:
	.string	"lname"
.LC152:
	.string	"align"
.LC153:
	.string	"oid"
.LC154:
	.string	"space_eq"
.LC155:
	.string	"dump_unknown"
.LC156:
	.string	"RFC2253"
.LC157:
	.string	"oneline"
.LC158:
	.string	"multiline"
.LC159:
	.string	"ca_default"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	ex_tbl.27627, @object
	.size	ex_tbl.27627, 696
ex_tbl.27627:
	.quad	.LC132
	.quad	1
	.quad	0
	.quad	.LC133
	.quad	1024
	.quad	0
	.quad	.LC134
	.quad	2
	.quad	0
	.quad	.LC135
	.quad	4
	.quad	0
	.quad	.LC136
	.quad	8
	.quad	0
	.quad	.LC137
	.quad	16
	.quad	0
	.quad	.LC138
	.quad	32
	.quad	0
	.quad	.LC139
	.quad	64
	.quad	0
	.quad	.LC140
	.quad	128
	.quad	0
	.quad	.LC141
	.quad	256
	.quad	0
	.quad	.LC142
	.quad	512
	.quad	0
	.quad	.LC143
	.quad	0
	.quad	4294967295
	.quad	.LC144
	.quad	65536
	.quad	983040
	.quad	.LC145
	.quad	131072
	.quad	983040
	.quad	.LC146
	.quad	196608
	.quad	983040
	.quad	.LC147
	.quad	262144
	.quad	983040
	.quad	.LC148
	.quad	1048576
	.quad	0
	.quad	.LC149
	.quad	6291456
	.quad	6291456
	.quad	.LC150
	.quad	0
	.quad	6291456
	.quad	.LC151
	.quad	2097152
	.quad	6291456
	.quad	.LC152
	.quad	33554432
	.quad	0
	.quad	.LC153
	.quad	4194304
	.quad	6291456
	.quad	.LC154
	.quad	8388608
	.quad	0
	.quad	.LC155
	.quad	16777216
	.quad	0
	.quad	.LC156
	.quad	17892119
	.quad	4294967295
	.quad	.LC157
	.quad	8520479
	.quad	4294967295
	.quad	.LC158
	.quad	44302342
	.quad	4294967295
	.quad	.LC159
	.quad	44302342
	.quad	4294967295
	.quad	0
	.quad	0
	.quad	0
	.section	.rodata.str1.1
.LC160:
	.string	"compatible"
.LC161:
	.string	"no_header"
.LC162:
	.string	"no_version"
.LC163:
	.string	"no_serial"
.LC164:
	.string	"no_signame"
.LC165:
	.string	"no_validity"
.LC166:
	.string	"no_subject"
.LC167:
	.string	"no_issuer"
.LC168:
	.string	"no_pubkey"
.LC169:
	.string	"no_extensions"
.LC170:
	.string	"no_sigdump"
.LC171:
	.string	"no_aux"
.LC172:
	.string	"no_attributes"
.LC173:
	.string	"ext_default"
.LC174:
	.string	"ext_error"
.LC175:
	.string	"ext_parse"
.LC176:
	.string	"ext_dump"
	.section	.data.rel.ro.local
	.align 32
	.type	cert_tbl.27622, @object
	.size	cert_tbl.27622, 456
cert_tbl.27622:
	.quad	.LC160
	.quad	0
	.quad	4294967295
	.quad	.LC159
	.quad	147
	.quad	4294967295
	.quad	.LC161
	.quad	1
	.quad	0
	.quad	.LC162
	.quad	2
	.quad	0
	.quad	.LC163
	.quad	4
	.quad	0
	.quad	.LC164
	.quad	8
	.quad	0
	.quad	.LC165
	.quad	32
	.quad	0
	.quad	.LC166
	.quad	64
	.quad	0
	.quad	.LC167
	.quad	16
	.quad	0
	.quad	.LC168
	.quad	128
	.quad	0
	.quad	.LC169
	.quad	256
	.quad	0
	.quad	.LC170
	.quad	512
	.quad	0
	.quad	.LC171
	.quad	1024
	.quad	0
	.quad	.LC172
	.quad	2048
	.quad	0
	.quad	.LC173
	.quad	0
	.quad	983040
	.quad	.LC174
	.quad	65536
	.quad	983040
	.quad	.LC175
	.quad	131072
	.quad	983040
	.quad	.LC176
	.quad	196608
	.quad	983040
	.quad	0
	.quad	0
	.quad	0
	.local	pwdbio.27474
	.comm	pwdbio.27474,8,8
	.local	prefix_method
	.comm	prefix_method,8,8
	.local	nmflag_set
	.comm	nmflag_set,1,1
	.local	nmflag
	.comm	nmflag,8,8
	.local	ui_fallback_method
	.comm	ui_fallback_method,8,8
	.local	ui_method
	.comm	ui_method,8,8
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
