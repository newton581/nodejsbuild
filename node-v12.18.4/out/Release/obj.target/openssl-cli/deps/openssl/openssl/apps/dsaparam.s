	.file	"dsaparam.c"
	.text
	.p2align 4
	.type	dsa_cb, @function
dsa_cb:
.LFB1436:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	$63, %eax
	cmpl	$3, %edi
	ja	.L2
	movslq	%edi, %rdi
	leaq	symbols.24577(%rip), %rax
	movzbl	(%rax,%rdi), %eax
.L2:
	movq	%r12, %rdi
	movb	%al, -25(%rbp)
	call	BN_GENCB_get_arg@PLT
	leaq	-25(%rbp), %rsi
	movl	$1, %edx
	movq	%rax, %rdi
	call	BIO_write@PLT
	movq	%r12, %rdi
	call	BN_GENCB_get_arg@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	movq	%rax, %rdi
	call	BIO_ctrl@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L8
	addq	$24, %rsp
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L8:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1436:
	.size	dsa_cb, .-dsa_cb
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"%s: Use -help for summary.\n"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"Warning: It is not recommended to use more than %d bit for DSA keys.\n         Your key size is %d! Larger key size may behave not as expected.\n"
	.align 8
.LC2:
	.string	"Error allocating BN_GENCB object\n"
	.section	.rodata.str1.1
.LC3:
	.string	"Error allocating DSA object\n"
	.section	.rodata.str1.8
	.align 8
.LC4:
	.string	"Generating DSA parameters, %d bit long prime\n"
	.section	.rodata.str1.1
.LC5:
	.string	"This could take some time\n"
	.section	.rodata.str1.8
	.align 8
.LC6:
	.string	"Error, DSA key generation failed\n"
	.align 8
.LC7:
	.string	"unable to load DSA parameters\n"
	.section	.rodata.str1.1
.LC8:
	.string	"BN space"
	.section	.rodata.str1.8
	.align 8
.LC9:
	.string	"static DSA *get_dsa%d(void)\n{\n"
	.section	.rodata.str1.1
.LC10:
	.string	"dsap"
.LC11:
	.string	"dsaq"
.LC12:
	.string	"dsag"
	.section	.rodata.str1.8
	.align 8
.LC13:
	.string	"    DSA *dsa = DSA_new();\n    BIGNUM *p, *q, *g;\n\n"
	.align 8
.LC14:
	.string	"    if (dsa == NULL)\n        return NULL;\n"
	.align 8
.LC15:
	.string	"    if (!DSA_set0_pqg(dsa, p = BN_bin2bn(dsap_%d, sizeof(dsap_%d), NULL),\n"
	.align 8
.LC16:
	.string	"                           q = BN_bin2bn(dsaq_%d, sizeof(dsaq_%d), NULL),\n"
	.align 8
.LC17:
	.string	"                           g = BN_bin2bn(dsag_%d, sizeof(dsag_%d), NULL))) {\n"
	.align 8
.LC18:
	.string	"        DSA_free(dsa);\n        BN_free(p);\n        BN_free(q);\n        BN_free(g);\n        return NULL;\n    }\n    return dsa;\n}\n"
	.align 8
.LC19:
	.string	"../deps/openssl/openssl/apps/dsaparam.c"
	.align 8
.LC20:
	.string	"unable to write DSA parameters\n"
	.text
	.p2align 4
	.globl	dsaparam_main
	.type	dsaparam_main, @function
dsaparam_main:
.LFB1435:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	dsaparam_options(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	xorl	%r13d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	.L15(%rip), %rbx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$0, -92(%rbp)
	movl	$32773, -88(%rbp)
	movl	$32773, -84(%rbp)
	call	opt_init@PLT
	movq	$0, -120(%rbp)
	movl	$0, -124(%rbp)
	movq	%rax, %r14
	movl	$0, -104(%rbp)
	movq	$0, -112(%rbp)
.L10:
	call	opt_next@PLT
	testl	%eax, %eax
	je	.L83
.L28:
	cmpl	$10, %eax
	jg	.L11
	cmpl	$-1, %eax
	jl	.L10
	addl	$1, %eax
	cmpl	$11, %eax
	ja	.L10
	movslq	(%rbx,%rax,4), %rax
	addq	%rbx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L15:
	.long	.L24-.L15
	.long	.L10-.L15
	.long	.L23-.L15
	.long	.L22-.L15
	.long	.L21-.L15
	.long	.L20-.L15
	.long	.L19-.L15
	.long	.L18-.L15
	.long	.L52-.L15
	.long	.L17-.L15
	.long	.L16-.L15
	.long	.L14-.L15
	.text
.L52:
	movl	$1, %r12d
	call	opt_next@PLT
	testl	%eax, %eax
	jne	.L28
.L83:
	call	opt_num_rest@PLT
	movl	$-1, %ebx
	movl	%eax, %r14d
	call	opt_rest@PLT
	cmpl	$1, %r14d
	je	.L84
.L29:
	movl	-88(%rbp), %edx
	movl	$114, %esi
	movq	%r15, %rdi
	call	bio_open_default@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L30
	movl	-104(%rbp), %edx
	movl	-84(%rbp), %esi
	movq	-120(%rbp), %rdi
	call	bio_open_owner@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L54
	testl	%ebx, %ebx
	jle	.L31
	cmpl	$10000, %ebx
	jg	.L85
.L32:
	call	BN_GENCB_new@PLT
	testq	%rax, %rax
	je	.L86
	movq	bio_err(%rip), %rdx
	leaq	dsa_cb(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, -120(%rbp)
	call	BN_GENCB_set@PLT
	call	DSA_new@PLT
	movq	-120(%rbp), %r10
	leaq	.LC3(%rip), %rsi
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.L82
	movl	-92(%rbp), %edx
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
	movq	%r10, -120(%rbp)
	leaq	.LC4(%rip), %rsi
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	leaq	.LC5(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	-120(%rbp), %r10
	subq	$8, %rsp
	movl	-92(%rbp), %esi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	pushq	%r10
	movq	%rbx, %rdi
	call	DSA_generate_parameters_ex@PLT
	popq	%r9
	popq	%r10
	testl	%eax, %eax
	movq	-120(%rbp), %r10
	je	.L87
.L35:
	testl	%r13d, %r13d
	jne	.L88
.L38:
	testl	%r12d, %r12d
	jne	.L89
.L39:
	cmpl	$4, -84(%rbp)
	je	.L90
	movl	-124(%rbp), %eax
	testl	%eax, %eax
	je	.L91
.L50:
	movl	-104(%rbp), %edi
	testl	%edi, %edi
	je	.L26
.L44:
	movq	%rbx, %rdi
	movq	%r10, -120(%rbp)
	call	DSAparams_dup@PLT
	movl	$1, -104(%rbp)
	movq	-120(%rbp), %r10
	testq	%rax, %rax
	movq	%rax, %r12
	je	.L26
	movq	%rax, %rdi
	movq	%r10, -120(%rbp)
	call	DSA_generate_key@PLT
	movq	-120(%rbp), %r10
	testl	%eax, %eax
	je	.L92
	cmpl	$4, -84(%rbp)
	movq	%r10, -104(%rbp)
	je	.L93
	subq	$8, %rsp
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	xorl	%r9d, %r9d
	pushq	$0
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movq	%r15, %rdi
	call	PEM_write_bio_DSAPrivateKey@PLT
	movq	-104(%rbp), %r10
	popq	%rcx
	popq	%rsi
.L47:
	movq	%r12, %rdi
	movq	%r10, -120(%rbp)
	call	DSA_free@PLT
	movl	$0, -104(%rbp)
	movq	-120(%rbp), %r10
	jmp	.L26
.L18:
	movl	$1, %r13d
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L11:
	leal	-1501(%rax), %edx
	cmpl	$1, %edx
	ja	.L10
	movl	%eax, %edi
	call	opt_rand@PLT
	testl	%eax, %eax
	jne	.L10
	.p2align 4,,10
	.p2align 3
.L30:
	movl	$1, -104(%rbp)
	xorl	%r10d, %r10d
	xorl	%r15d, %r15d
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
.L26:
	movq	%r10, %rdi
	call	BN_GENCB_free@PLT
	movq	%r14, %rdi
	call	BIO_free@PLT
	movq	%r15, %rdi
	call	BIO_free_all@PLT
	movq	%rbx, %rdi
	call	DSA_free@PLT
	movq	-112(%rbp), %rdi
	call	release_engine@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L94
	movl	-104(%rbp), %eax
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L22:
	.cfi_restore_state
	call	opt_arg@PLT
	leaq	-88(%rbp), %rdx
	movl	$2, %esi
	movq	%rax, %rdi
	call	opt_format@PLT
	testl	%eax, %eax
	jne	.L10
.L24:
	movq	bio_err(%rip), %rdi
	movq	%r14, %rdx
	leaq	.LC0(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L30
.L14:
	call	opt_arg@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	setup_engine@PLT
	movq	%rax, -112(%rbp)
	jmp	.L10
.L16:
	movl	$1, -104(%rbp)
	jmp	.L10
.L17:
	movl	$1, -124(%rbp)
	jmp	.L10
.L19:
	call	opt_arg@PLT
	movq	%rax, -120(%rbp)
	jmp	.L10
.L20:
	call	opt_arg@PLT
	movq	%rax, %r15
	jmp	.L10
.L21:
	call	opt_arg@PLT
	leaq	-84(%rbp), %rdx
	movl	$2, %esi
	movq	%rax, %rdi
	call	opt_format@PLT
	testl	%eax, %eax
	jne	.L10
	jmp	.L24
.L23:
	leaq	dsaparam_options(%rip), %rdi
	xorl	%r15d, %r15d
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	call	opt_help@PLT
	movl	$0, -104(%rbp)
	xorl	%r10d, %r10d
	jmp	.L26
.L31:
	xorl	%ecx, %ecx
	cmpl	$4, -88(%rbp)
	je	.L95
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	PEM_read_bio_DSAparams@PLT
	movq	%rax, %rbx
.L37:
	xorl	%r10d, %r10d
	testq	%rbx, %rbx
	jne	.L35
	movq	bio_err(%rip), %rdi
	leaq	.LC7(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	movl	$1, -104(%rbp)
	movq	%rbx, %r10
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L84:
	movq	(%rax), %rdi
	leaq	-92(%rbp), %rsi
	call	opt_int@PLT
	testl	%eax, %eax
	je	.L30
	movl	-92(%rbp), %ebx
	testl	%ebx, %ebx
	jns	.L29
	jmp	.L30
.L54:
	movl	$1, -104(%rbp)
	xorl	%r10d, %r10d
	xorl	%ebx, %ebx
	jmp	.L26
.L90:
	movl	-104(%rbp), %r8d
	testl	%r8d, %r8d
	jne	.L44
	movl	-124(%rbp), %edx
	testl	%edx, %edx
	jne	.L26
	movq	i2d_DSAparams@GOTPCREL(%rip), %rdi
	movq	%rbx, %rdx
	movq	%r15, %rsi
	movq	%r10, -120(%rbp)
	call	ASN1_i2d_bio@PLT
	movq	-120(%rbp), %r10
	testl	%eax, %eax
	jne	.L26
.L43:
	movq	bio_err(%rip), %rdi
	leaq	.LC20(%rip), %rsi
	xorl	%eax, %eax
	movq	%r10, -120(%rbp)
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	movl	$1, -104(%rbp)
	movq	-120(%rbp), %r10
	jmp	.L26
.L89:
	leaq	-64(%rbp), %rcx
	leaq	-72(%rbp), %rdx
	movq	%rbx, %rdi
	movq	%r10, -120(%rbp)
	leaq	-80(%rbp), %rsi
	movq	$0, -80(%rbp)
	movq	$0, -72(%rbp)
	movq	$0, -64(%rbp)
	call	DSA_get0_pqg@PLT
	movq	-80(%rbp), %rdi
	call	BN_num_bits@PLT
	movq	-80(%rbp), %rdi
	movl	%eax, %r13d
	call	BN_num_bits@PLT
	leal	14(%r13), %edi
	addl	$7, %r13d
	leaq	.LC8(%rip), %rsi
	cmovns	%r13d, %edi
	movl	%eax, %r12d
	sarl	$3, %edi
	addl	$20, %edi
	call	app_malloc@PLT
	movq	bio_out(%rip), %rdi
	movl	%r12d, %edx
	leaq	.LC9(%rip), %rsi
	movq	%rax, %r13
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	-80(%rbp), %rsi
	movq	%r13, %r8
	movl	%r12d, %ecx
	movq	bio_out(%rip), %rdi
	leaq	.LC10(%rip), %rdx
	call	print_bignum_var@PLT
	movq	-72(%rbp), %rsi
	movq	%r13, %r8
	movl	%r12d, %ecx
	movq	bio_out(%rip), %rdi
	leaq	.LC11(%rip), %rdx
	call	print_bignum_var@PLT
	movq	-64(%rbp), %rsi
	movq	%r13, %r8
	movl	%r12d, %ecx
	movq	bio_out(%rip), %rdi
	leaq	.LC12(%rip), %rdx
	call	print_bignum_var@PLT
	movq	bio_out(%rip), %rdi
	leaq	.LC13(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_out(%rip), %rdi
	leaq	.LC14(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movl	%r12d, %ecx
	movl	%r12d, %edx
	xorl	%eax, %eax
	movq	bio_out(%rip), %rdi
	leaq	.LC15(%rip), %rsi
	call	BIO_printf@PLT
	movl	%r12d, %ecx
	movl	%r12d, %edx
	xorl	%eax, %eax
	movq	bio_out(%rip), %rdi
	leaq	.LC16(%rip), %rsi
	call	BIO_printf@PLT
	movl	%r12d, %ecx
	movl	%r12d, %edx
	xorl	%eax, %eax
	movq	bio_out(%rip), %rdi
	leaq	.LC17(%rip), %rsi
	call	BIO_printf@PLT
	movq	bio_out(%rip), %rdi
	leaq	.LC18(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movl	$200, %edx
	leaq	.LC19(%rip), %rsi
	movq	%r13, %rdi
	call	CRYPTO_free@PLT
	movq	-120(%rbp), %r10
	jmp	.L39
.L88:
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movq	%r10, -120(%rbp)
	call	DSAparams_print@PLT
	movq	-120(%rbp), %r10
	jmp	.L38
.L85:
	movq	bio_err(%rip), %rdi
	movl	%ebx, %ecx
	movl	$10000, %edx
	xorl	%eax, %eax
	leaq	.LC1(%rip), %rsi
	call	BIO_printf@PLT
	jmp	.L32
.L91:
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movq	%r10, -120(%rbp)
	call	PEM_write_bio_DSAparams@PLT
	movq	-120(%rbp), %r10
	testl	%eax, %eax
	jne	.L50
	jmp	.L43
.L87:
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	leaq	.LC6(%rip), %rsi
.L82:
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movl	$1, -104(%rbp)
	movq	-120(%rbp), %r10
	jmp	.L26
.L95:
	movq	d2i_DSAparams@GOTPCREL(%rip), %rsi
	movq	DSA_new@GOTPCREL(%rip), %rdi
	movq	%r14, %rdx
	call	ASN1_d2i_bio@PLT
	movq	%rax, %rbx
	jmp	.L37
.L86:
	movq	bio_err(%rip), %rdi
	movq	%rax, -120(%rbp)
	leaq	.LC2(%rip), %rsi
	xorl	%eax, %eax
	xorl	%ebx, %ebx
	call	BIO_printf@PLT
	movl	$1, -104(%rbp)
	movq	-120(%rbp), %r10
	jmp	.L26
.L93:
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	i2d_DSAPrivateKey_bio@PLT
	movq	-104(%rbp), %r10
	jmp	.L47
.L92:
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	movq	%r12, %rdi
	call	DSA_free@PLT
	movq	-120(%rbp), %r10
	jmp	.L26
.L94:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1435:
	.size	dsaparam_main, .-dsaparam_main
	.section	.rodata
	.type	symbols.24577, @object
	.size	symbols.24577, 5
symbols.24577:
	.string	".+*\n"
	.globl	dsaparam_options
	.section	.rodata.str1.1
.LC21:
	.string	"help"
.LC22:
	.string	"Display this summary"
.LC23:
	.string	"inform"
.LC24:
	.string	"Input format - DER or PEM"
.LC25:
	.string	"in"
.LC26:
	.string	"Input file"
.LC27:
	.string	"outform"
.LC28:
	.string	"Output format - DER or PEM"
.LC29:
	.string	"out"
.LC30:
	.string	"Output file"
.LC31:
	.string	"text"
.LC32:
	.string	"Print as text"
.LC33:
	.string	"C"
.LC34:
	.string	"Output C code"
.LC35:
	.string	"noout"
.LC36:
	.string	"No output"
.LC37:
	.string	"genkey"
.LC38:
	.string	"Generate a DSA key"
.LC39:
	.string	"rand"
	.section	.rodata.str1.8
	.align 8
.LC40:
	.string	"Load the file(s) into the random number generator"
	.section	.rodata.str1.1
.LC41:
	.string	"writerand"
	.section	.rodata.str1.8
	.align 8
.LC42:
	.string	"Write random data to the specified file"
	.section	.rodata.str1.1
.LC43:
	.string	"engine"
	.section	.rodata.str1.8
	.align 8
.LC44:
	.string	"Use engine e, possibly a hardware device"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	dsaparam_options, @object
	.size	dsaparam_options, 312
dsaparam_options:
	.quad	.LC21
	.long	1
	.long	45
	.quad	.LC22
	.quad	.LC23
	.long	2
	.long	70
	.quad	.LC24
	.quad	.LC25
	.long	4
	.long	60
	.quad	.LC26
	.quad	.LC27
	.long	3
	.long	70
	.quad	.LC28
	.quad	.LC29
	.long	5
	.long	62
	.quad	.LC30
	.quad	.LC31
	.long	6
	.long	45
	.quad	.LC32
	.quad	.LC33
	.long	7
	.long	45
	.quad	.LC34
	.quad	.LC35
	.long	8
	.long	45
	.quad	.LC36
	.quad	.LC37
	.long	9
	.long	45
	.quad	.LC38
	.quad	.LC39
	.long	1501
	.long	115
	.quad	.LC40
	.quad	.LC41
	.long	1502
	.long	62
	.quad	.LC42
	.quad	.LC43
	.long	10
	.long	115
	.quad	.LC44
	.quad	0
	.zero	16
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
