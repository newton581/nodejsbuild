	.file	"prime.c"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"%s: Use -help for summary.\n"
.LC1:
	.string	"Extra arguments given.\n"
.LC2:
	.string	"%s: No prime specified\n"
.LC3:
	.string	"Specify the number of bits.\n"
.LC4:
	.string	"Out of memory.\n"
.LC5:
	.string	"Failed to generate prime.\n"
.LC6:
	.string	"%s\n"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC7:
	.string	"../deps/openssl/openssl/apps/prime.c"
	.section	.rodata.str1.1
.LC8:
	.string	"is not"
.LC9:
	.string	" (%s) %s prime\n"
.LC10:
	.string	"is"
.LC11:
	.string	"Failed to process value (%s)\n"
	.text
	.p2align 4
	.globl	prime_main
	.type	prime_main, @function
prime_main:
.LFB1435:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	prime_options(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	$20, %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	xorl	%r13d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	.L5(%rip), %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -64(%rbp)
	call	opt_init@PLT
	movl	$0, -72(%rbp)
	movl	$0, -68(%rbp)
	movq	%rax, %r14
.L2:
	call	opt_next@PLT
	testl	%eax, %eax
	je	.L51
.L13:
	addl	$1, %eax
	cmpl	$7, %eax
	ja	.L2
	movslq	(%rbx,%rax,4), %rax
	addq	%rbx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L5:
	.long	.L11-.L5
	.long	.L2-.L5
	.long	.L10-.L5
	.long	.L9-.L5
	.long	.L8-.L5
	.long	.L7-.L5
	.long	.L6-.L5
	.long	.L4-.L5
	.text
.L9:
	movl	$1, %r12d
	call	opt_next@PLT
	testl	%eax, %eax
	jne	.L13
.L51:
	call	opt_num_rest@PLT
	movl	%eax, -76(%rbp)
	call	opt_rest@PLT
	testl	%r13d, %r13d
	movl	-76(%rbp), %edx
	movq	%rax, %rbx
	je	.L14
	testl	%edx, %edx
	jne	.L52
	movl	-68(%rbp), %eax
	testl	%eax, %eax
	je	.L53
	call	BN_new@PLT
	movq	%rax, -64(%rbp)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L49
	movl	-72(%rbp), %edx
	movl	-68(%rbp), %esi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	call	BN_generate_prime_ex@PLT
	testl	%eax, %eax
	je	.L54
	movq	-64(%rbp), %rdi
	testl	%r12d, %r12d
	je	.L21
	call	BN_bn2hex@PLT
	movq	%rax, %r12
.L22:
	testq	%r12, %r12
	je	.L49
	movq	bio_out(%rip), %rdi
	movq	%r12, %rdx
	xorl	%eax, %eax
	xorl	%r13d, %r13d
	leaq	.LC6(%rip), %rsi
	call	BIO_printf@PLT
	movl	$106, %edx
	leaq	.LC7(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
	jmp	.L12
.L52:
	movq	bio_err(%rip), %rdi
	leaq	.LC1(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L11:
	movq	bio_err(%rip), %rdi
	movq	%r14, %rdx
	leaq	.LC0(%rip), %rsi
	xorl	%eax, %eax
	movl	$1, %r13d
	call	BIO_printf@PLT
.L12:
	movq	-64(%rbp), %rdi
	call	BN_free@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L55
	addq	$40, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L4:
	.cfi_restore_state
	call	opt_arg@PLT
	movl	$10, %edx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	strtol@PLT
	movl	%eax, %r15d
	jmp	.L2
.L6:
	movl	$1, -72(%rbp)
	jmp	.L2
.L7:
	call	opt_arg@PLT
	movl	$10, %edx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	strtol@PLT
	movl	%eax, -68(%rbp)
	jmp	.L2
.L8:
	movl	$1, %r13d
	jmp	.L2
.L10:
	leaq	prime_options(%rip), %rdi
	xorl	%r13d, %r13d
	call	opt_help@PLT
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L14:
	testl	%edx, %edx
	je	.L16
	movq	(%rax), %rsi
	testq	%rsi, %rsi
	je	.L12
	testl	%r12d, %r12d
	leaq	.LC8(%rip), %r14
	leaq	-64(%rbp), %r12
	jne	.L24
	leaq	.LC10(%rip), %r14
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L56:
	movq	(%rbx), %rdx
	movq	bio_out(%rip), %rdi
	leaq	.LC8(%rip), %rcx
	leaq	.LC9(%rip), %rsi
.L47:
	call	BIO_printf@PLT
	movq	8(%rbx), %rsi
	addq	$8, %rbx
	testq	%rsi, %rsi
	je	.L12
.L29:
	movq	%r12, %rdi
	call	BN_dec2bn@PLT
	testl	%eax, %eax
	je	.L25
	movq	-64(%rbp), %rsi
	movq	bio_out(%rip), %rdi
	call	BN_print@PLT
	movq	-64(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	%r15d, %esi
	call	BN_is_prime_ex@PLT
	testl	%eax, %eax
	je	.L56
	movq	(%rbx), %rdx
	movq	bio_out(%rip), %rdi
	movq	%r14, %rcx
	xorl	%eax, %eax
	leaq	.LC9(%rip), %rsi
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L30:
	movq	-64(%rbp), %rsi
	movq	bio_out(%rip), %rdi
	call	BN_print@PLT
	movq	-64(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	%r15d, %esi
	call	BN_is_prime_ex@PLT
	leaq	.LC10(%rip), %rcx
	testl	%eax, %eax
	jne	.L48
	movq	%r14, %rcx
.L48:
	movq	(%rbx), %rdx
	movq	bio_out(%rip), %rdi
	xorl	%eax, %eax
	addq	$8, %rbx
	leaq	.LC9(%rip), %rsi
	call	BIO_printf@PLT
	movq	(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L12
.L24:
	movq	%r12, %rdi
	call	BN_hex2bn@PLT
	testl	%eax, %eax
	jne	.L30
.L25:
	movq	(%rbx), %rdx
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
	movl	$1, %r13d
	leaq	.LC11(%rip), %rsi
	call	BIO_printf@PLT
	jmp	.L12
.L53:
	movq	bio_err(%rip), %rdi
	leaq	.LC3(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L12
.L16:
	movq	bio_err(%rip), %rdi
	movq	%r14, %rdx
	leaq	.LC2(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L11
.L21:
	call	BN_bn2dec@PLT
	movq	%rax, %r12
	jmp	.L22
.L49:
	movq	bio_err(%rip), %rdi
	leaq	.LC4(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L12
.L54:
	movq	bio_err(%rip), %rdi
	leaq	.LC5(%rip), %rsi
	call	BIO_printf@PLT
	jmp	.L12
.L55:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1435:
	.size	prime_main, .-prime_main
	.globl	prime_options
	.section	.rodata.str1.8
	.align 8
.LC12:
	.string	"Usage: %s [options] [number...]\n"
	.align 8
.LC13:
	.string	"  number Number to check for primality\n"
	.section	.rodata.str1.1
.LC14:
	.string	"help"
.LC15:
	.string	"Display this summary"
.LC16:
	.string	"hex"
.LC17:
	.string	"Hex output"
.LC18:
	.string	"generate"
.LC19:
	.string	"Generate a prime"
.LC20:
	.string	"bits"
.LC21:
	.string	"Size of number in bits"
.LC22:
	.string	"safe"
	.section	.rodata.str1.8
	.align 8
.LC23:
	.string	"When used with -generate, generate a safe prime"
	.section	.rodata.str1.1
.LC24:
	.string	"checks"
.LC25:
	.string	"Number of checks"
	.section	.data.rel.ro,"aw"
	.align 32
	.type	prime_options, @object
	.size	prime_options, 216
prime_options:
	.quad	OPT_HELP_STR
	.long	1
	.long	45
	.quad	.LC12
	.quad	OPT_HELP_STR
	.long	1
	.long	45
	.quad	.LC13
	.quad	.LC14
	.long	1
	.long	45
	.quad	.LC15
	.quad	.LC16
	.long	2
	.long	45
	.quad	.LC17
	.quad	.LC18
	.long	3
	.long	45
	.quad	.LC19
	.quad	.LC20
	.long	4
	.long	112
	.quad	.LC21
	.quad	.LC22
	.long	5
	.long	45
	.quad	.LC23
	.quad	.LC24
	.long	6
	.long	112
	.quad	.LC25
	.quad	0
	.zero	16
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
