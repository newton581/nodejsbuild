	.file	"s_server.c"
	.text
	.p2align 4
	.type	next_proto_cb, @function
next_proto_cb:
.LFB1638:
	.cfi_startproc
	endbr64
	movq	(%rcx), %rax
	movq	%rax, (%rsi)
	movq	8(%rcx), %rax
	movl	%eax, (%rdx)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1638:
	.size	next_proto_cb, .-next_proto_cb
	.p2align 4
	.type	not_resumable_sess_cb, @function
not_resumable_sess_cb:
.LFB1640:
	.cfi_startproc
	endbr64
	movl	%esi, %eax
	ret
	.cfi_endproc
.LFE1640:
	.size	not_resumable_sess_cb, .-not_resumable_sess_cb
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"%4ld items in the session cache\n"
	.align 8
.LC1:
	.string	"%4ld client connects (SSL_connect())\n"
	.align 8
.LC2:
	.string	"%4ld client renegotiates (SSL_connect())\n"
	.align 8
.LC3:
	.string	"%4ld client connects that finished\n"
	.align 8
.LC4:
	.string	"%4ld server accepts (SSL_accept())\n"
	.align 8
.LC5:
	.string	"%4ld server renegotiates (SSL_accept())\n"
	.align 8
.LC6:
	.string	"%4ld server accepts that finished\n"
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC7:
	.string	"%4ld session cache hits\n"
.LC8:
	.string	"%4ld session cache misses\n"
.LC9:
	.string	"%4ld session cache timeouts\n"
.LC10:
	.string	"%4ld callback cache hits\n"
	.section	.rodata.str1.8
	.align 8
.LC11:
	.string	"%4ld cache full overflows (%ld allowed)\n"
	.text
	.p2align 4
	.type	print_stats, @function
print_stats:
.LFB1642:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	movl	$20, %esi
	movq	%r12, %rdi
	subq	$8, %rsp
	call	SSL_CTX_ctrl@PLT
	movq	%r13, %rdi
	leaq	.LC0(%rip), %rsi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	movl	$21, %esi
	call	SSL_CTX_ctrl@PLT
	movq	%r13, %rdi
	leaq	.LC1(%rip), %rsi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	movl	$23, %esi
	call	SSL_CTX_ctrl@PLT
	movq	%r13, %rdi
	leaq	.LC2(%rip), %rsi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	movl	$22, %esi
	call	SSL_CTX_ctrl@PLT
	movq	%r13, %rdi
	leaq	.LC3(%rip), %rsi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	movl	$24, %esi
	call	SSL_CTX_ctrl@PLT
	movq	%r13, %rdi
	leaq	.LC4(%rip), %rsi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	movl	$26, %esi
	call	SSL_CTX_ctrl@PLT
	movq	%r13, %rdi
	leaq	.LC5(%rip), %rsi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	xorl	%edx, %edx
	movl	$25, %esi
	call	SSL_CTX_ctrl@PLT
	movq	%r13, %rdi
	leaq	.LC6(%rip), %rsi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	xorl	%edx, %edx
	movl	$27, %esi
	call	SSL_CTX_ctrl@PLT
	movq	%r13, %rdi
	leaq	.LC7(%rip), %rsi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	xorl	%edx, %edx
	movl	$29, %esi
	call	SSL_CTX_ctrl@PLT
	movq	%r13, %rdi
	leaq	.LC8(%rip), %rsi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	xorl	%edx, %edx
	movl	$30, %esi
	call	SSL_CTX_ctrl@PLT
	movq	%r13, %rdi
	leaq	.LC9(%rip), %rsi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	xorl	%edx, %edx
	movl	$28, %esi
	call	SSL_CTX_ctrl@PLT
	movq	%r13, %rdi
	leaq	.LC10(%rip), %rsi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	%r12, %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$43, %esi
	call	SSL_CTX_ctrl@PLT
	movq	%r12, %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$31, %esi
	movq	%rax, %r14
	call	SSL_CTX_ctrl@PLT
	addq	$8, %rsp
	movq	%r14, %rcx
	movq	%r13, %rdi
	popq	%r12
	movq	%rax, %rdx
	popq	%r13
	xorl	%eax, %eax
	popq	%r14
	leaq	.LC11(%rip), %rsi
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	BIO_printf@PLT
	.cfi_endproc
.LFE1642:
	.size	print_stats, .-print_stats
	.section	.rodata.str1.1
.LC12:
	.string	"(NONE)"
.LC13:
	.string	""
.LC14:
	.string	" NOT"
.LC15:
	.string	"Client certificate\n"
.LC16:
	.string	"Shared ciphers:%s\n"
.LC17:
	.string	"CIPHER is %s\n"
.LC18:
	.string	"NEXTPROTO is "
.LC19:
	.string	"\n"
	.section	.rodata.str1.8
	.align 8
.LC20:
	.string	"SRTP Extension negotiated, profile=%s\n"
	.section	.rodata.str1.1
.LC21:
	.string	"Reused session-id\n"
	.section	.rodata.str1.8
	.align 8
.LC22:
	.string	"Secure Renegotiation IS%s supported\n"
	.section	.rodata.str1.1
.LC23:
	.string	"Renegotiation is DISABLED\n"
.LC24:
	.string	"Keying material exporter:\n"
.LC25:
	.string	"    Label: '%s'\n"
.LC26:
	.string	"    Length: %i bytes\n"
.LC27:
	.string	"export key"
.LC28:
	.string	"    Error\n"
.LC29:
	.string	"    Keying material: "
.LC30:
	.string	"%02X"
	.section	.rodata.str1.8
	.align 8
.LC31:
	.string	"../deps/openssl/openssl/apps/s_server.c"
	.text
	.p2align 4
	.type	print_connection_info, @function
print_connection_info:
.LFB1647:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$32, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movl	s_brief(%rip), %esi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%rdi, %r12
	testl	%esi, %esi
	jne	.L42
.L7:
	movq	%r12, %rdi
	call	SSL_get_session@PLT
	movq	bio_s_out(%rip), %rdi
	movq	%rax, %rsi
	call	PEM_write_bio_SSL_SESSION@PLT
	movq	%r12, %rdi
	call	SSL_get_peer_certificate@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L8
	movq	bio_s_out(%rip), %rdi
	leaq	.LC15(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_s_out(%rip), %rdi
	movq	%r13, %rsi
	call	PEM_write_bio_X509@PLT
	movq	bio_s_out(%rip), %rdi
	movq	%r13, %rsi
	call	dump_cert_text@PLT
	movq	%r13, %rdi
	call	X509_free@PLT
.L8:
	leaq	-8240(%rbp), %r13
	movl	$8192, %edx
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	SSL_get_shared_ciphers@PLT
	testq	%rax, %rax
	je	.L9
	movq	bio_s_out(%rip), %rdi
	movq	%r13, %rdx
	leaq	.LC16(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L9:
	movq	%r12, %rdi
	call	SSL_get_current_cipher@PLT
	movq	%rax, %rdi
	call	SSL_CIPHER_get_name@PLT
	movq	bio_s_out(%rip), %rdi
	movq	%r12, %rsi
	movq	%rax, %r13
	call	ssl_print_sigalgs@PLT
	movq	bio_s_out(%rip), %rdi
	movq	%r12, %rsi
	call	ssl_print_point_formats@PLT
	movq	bio_s_out(%rip), %rdi
	xorl	%edx, %edx
	movq	%r12, %rsi
	call	ssl_print_groups@PLT
	movq	bio_s_out(%rip), %rdi
	movq	%r12, %rsi
	call	print_ca_names@PLT
	testq	%r13, %r13
	leaq	.LC12(%rip), %rax
	movq	bio_s_out(%rip), %rdi
	cmove	%rax, %r13
	leaq	.LC17(%rip), %rsi
	xorl	%eax, %eax
	movq	%r13, %rdx
	call	BIO_printf@PLT
	leaq	-8252(%rbp), %rdx
	leaq	-8248(%rbp), %rsi
	movq	%r12, %rdi
	call	SSL_get0_next_proto_negotiated@PLT
	cmpq	$0, -8248(%rbp)
	je	.L11
	movq	bio_s_out(%rip), %rdi
	leaq	.LC18(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	-8248(%rbp), %rsi
	movq	bio_s_out(%rip), %rdi
	movl	-8252(%rbp), %edx
	call	BIO_write@PLT
	movq	bio_s_out(%rip), %rdi
	leaq	.LC19(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L11:
	movq	%r12, %rdi
	call	SSL_get_selected_srtp_profile@PLT
	testq	%rax, %rax
	je	.L12
	movq	(%rax), %rdx
	movq	bio_s_out(%rip), %rdi
	leaq	.LC20(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L12:
	movq	%r12, %rdi
	call	SSL_session_reused@PLT
	testl	%eax, %eax
	jne	.L43
.L13:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$76, %esi
	movq	%r12, %rdi
	call	SSL_ctrl@PLT
	leaq	.LC13(%rip), %rdx
	movq	bio_s_out(%rip), %rdi
	leaq	.LC22(%rip), %rsi
	testq	%rax, %rax
	leaq	.LC14(%rip), %rax
	cmove	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	%r12, %rdi
	call	SSL_get_options@PLT
	testl	$1073741824, %eax
	jne	.L44
.L15:
	cmpq	$0, keymatexportlabel(%rip)
	movq	bio_s_out(%rip), %rdi
	je	.L16
	leaq	.LC24(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	keymatexportlabel(%rip), %rdx
	movq	bio_s_out(%rip), %rdi
	xorl	%eax, %eax
	leaq	.LC25(%rip), %rsi
	call	BIO_printf@PLT
	movl	keymatexportlen(%rip), %edx
	movq	bio_s_out(%rip), %rdi
	xorl	%eax, %eax
	leaq	.LC26(%rip), %rsi
	call	BIO_printf@PLT
	movl	keymatexportlen(%rip), %edi
	leaq	.LC27(%rip), %rsi
	call	app_malloc@PLT
	movq	keymatexportlabel(%rip), %r14
	movq	%rax, %r13
	movq	%r14, %rdi
	call	strlen@PLT
	pushq	$0
	movq	%r14, %rcx
	xorl	%r9d, %r9d
	pushq	$0
	movslq	keymatexportlen(%rip), %rdx
	movq	%rax, %r8
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	SSL_export_keying_material@PLT
	popq	%rdx
	popq	%rcx
	testl	%eax, %eax
	jne	.L17
	movq	bio_s_out(%rip), %rdi
	leaq	.LC28(%rip), %rsi
	call	BIO_printf@PLT
.L18:
	movq	%r13, %rdi
	movl	$2924, %edx
	leaq	.LC31(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	bio_s_out(%rip), %rdi
.L16:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	call	BIO_ctrl@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L45
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	.cfi_restore_state
	movq	bio_s_out(%rip), %rdi
	leaq	.LC29(%rip), %rsi
	xorl	%eax, %eax
	xorl	%r12d, %r12d
	leaq	.LC30(%rip), %rbx
	call	BIO_printf@PLT
	movl	keymatexportlen(%rip), %eax
	testl	%eax, %eax
	jle	.L20
	.p2align 4,,10
	.p2align 3
.L19:
	movzbl	0(%r13,%r12), %edx
	movq	bio_s_out(%rip), %rdi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	addq	$1, %r12
	call	BIO_printf@PLT
	cmpl	%r12d, keymatexportlen(%rip)
	jg	.L19
.L20:
	movq	bio_s_out(%rip), %rdi
	leaq	.LC19(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L44:
	movq	bio_s_out(%rip), %rdi
	leaq	.LC23(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L43:
	movq	bio_s_out(%rip), %rdi
	leaq	.LC21(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L42:
	call	print_ssl_summary@PLT
	jmp	.L7
.L45:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1647:
	.size	print_connection_info, .-print_connection_info
	.section	.rodata.str1.1
.LC32:
	.string	"server buffer"
.LC33:
	.string	"Turned on non blocking io\n"
	.section	.rodata.str1.8
	.align 8
.LC34:
	.string	"Error setting session id context\n"
	.align 8
.LC35:
	.string	"Error clearing SSL connection\n"
	.align 8
.LC37:
	.string	"MTU too small. Must be at least %ld\n"
	.section	.rodata.str1.1
.LC38:
	.string	"Failed to set MTU\n"
.LC39:
	.string	"Unable to create BIO\n"
.LC40:
	.string	"Error reading early data\n"
.LC41:
	.string	"Early data received:\n"
.LC42:
	.string	"No early data received\n"
.LC43:
	.string	"Early data was rejected\n"
.LC44:
	.string	"\nEnd of early data\n"
.LC45:
	.string	"TIMEOUT occurred\n"
.LC48:
	.string	"DONE\n"
.LC49:
	.string	"shutdown accept socket\n"
.LC50:
	.string	"SSL_do_handshake -> %d\n"
.LC51:
	.string	"Failed to initiate request"
.LC52:
	.string	"Lets print some clear text\n"
.LC53:
	.string	"LOOKUP renego during write\n"
.LC54:
	.string	"LOOKUP done %s\n"
.LC55:
	.string	"LOOKUP not successful\n"
.LC56:
	.string	"Write BLOCK (Async)\n"
.LC57:
	.string	"Write BLOCK\n"
.LC58:
	.string	"ERROR\n"
.LC59:
	.string	"ERROR - memory\n"
.LC60:
	.string	"ERROR - unable to connect\n"
.LC61:
	.string	"LOOKUP during accept %s\n"
.LC62:
	.string	"DELAY\n"
.LC63:
	.string	"verify error:%s\n"
.LC64:
	.string	"LOOKUP renego during read\n"
.LC65:
	.string	"Read BLOCK (Async)\n"
.LC66:
	.string	"Read BLOCK\n"
.LC67:
	.string	"CONNECTION CLOSED\n"
.LC68:
	.string	"shutting down SSL\n"
	.text
	.p2align 4
	.type	sv_body, @function
sv_body:
.LFB1643:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%esi, %ebx
	leaq	.LC32(%rip), %rsi
	subq	$232, %rsp
	movl	%edi, -228(%rbp)
	movl	$16384, %edi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	app_malloc@PLT
	movl	s_nbio(%rip), %ecx
	movq	%rax, %r15
	testl	%ecx, %ecx
	jne	.L302
.L48:
	movq	ctx(%rip), %rdi
	call	SSL_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L195
	movl	s_tlsextdebug(%rip), %eax
	testl	%eax, %eax
	jne	.L303
.L52:
	testq	%r13, %r13
	je	.L53
	movq	%r13, %rdi
	call	strlen@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	SSL_set_session_id_context@PLT
	testl	%eax, %eax
	je	.L304
.L53:
	movq	%r12, %rdi
	call	SSL_clear@PLT
	testl	%eax, %eax
	je	.L305
	movl	-228(%rbp), %edi
	xorl	%esi, %esi
	cmpl	$2, %ebx
	je	.L306
	call	BIO_new_socket@PLT
	movq	%rax, %r13
.L61:
	leaq	.LC39(%rip), %rsi
	testq	%r13, %r13
	je	.L300
	movl	s_nbio_test(%rip), %eax
	testl	%eax, %eax
	jne	.L307
.L63:
	movq	%r13, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	SSL_set_bio@PLT
	movq	%r12, %rdi
	call	SSL_set_accept_state@PLT
	movl	s_debug(%rip), %eax
	testl	%eax, %eax
	jne	.L308
.L64:
	movl	s_msg(%rip), %eax
	testl	%eax, %eax
	jne	.L309
	movl	s_tlsextdebug(%rip), %eax
	testl	%eax, %eax
	jne	.L310
.L69:
	movl	early_data(%rip), %r14d
	movl	$1, %r13d
	leaq	-216(%rbp), %rbx
	testl	%r14d, %r14d
	je	.L71
.L70:
	movq	%rbx, %rcx
	movl	$16384, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	SSL_read_early_data@PLT
	movl	%eax, %r14d
	testl	%eax, %eax
	jne	.L72
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	SSL_get_error@PLT
	cmpl	$3, %eax
	jg	.L73
	cmpl	$1, %eax
	jg	.L70
.L75:
	leaq	.LC40(%rip), %rsi
.L300:
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L301:
	movq	bio_err(%rip), %rdi
	movl	$1, %r13d
	call	ERR_print_errors@PLT
.L54:
	movq	bio_s_out(%rip), %rdi
	leaq	.LC68(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	%r12, %rdi
	movl	$3, %esi
	call	SSL_set_shutdown@PLT
	movq	%r12, %rdi
	call	SSL_free@PLT
.L51:
	movq	bio_s_out(%rip), %rdi
	leaq	.LC67(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movl	$2714, %ecx
	movl	$16384, %esi
	movq	%r15, %rdi
	leaq	.LC31(%rip), %rdx
	call	CRYPTO_clear_free@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L311
	addq	$232, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L73:
	.cfi_restore_state
	cmpl	$9, %eax
	je	.L70
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L309:
	cmpl	$2, %eax
	je	.L312
	movq	msg_cb@GOTPCREL(%rip), %rsi
	movq	%r12, %rdi
	call	SSL_set_msg_callback@PLT
.L67:
	movq	bio_s_msg(%rip), %rcx
	movl	$16, %esi
	movq	%r12, %rdi
	testq	%rcx, %rcx
	cmove	bio_s_out(%rip), %rcx
	xorl	%edx, %edx
	call	SSL_ctrl@PLT
	movl	s_tlsextdebug(%rip), %eax
	testl	%eax, %eax
	je	.L69
.L310:
	movq	tlsext_cb@GOTPCREL(%rip), %rdx
	movq	%r12, %rdi
	movl	$56, %esi
	call	SSL_callback_ctrl@PLT
	xorl	%edx, %edx
	movl	$57, %esi
	movq	%r12, %rdi
	movq	bio_s_out(%rip), %rcx
	call	SSL_ctrl@PLT
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L306:
	call	BIO_new_dgram@PLT
	movq	%rax, %r13
	movl	enable_timeouts(%rip), %eax
	testl	%eax, %eax
	jne	.L313
.L57:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	cmpq	$0, socket_mtu(%rip)
	je	.L58
	movl	$121, %esi
	movq	%r12, %rdi
	call	SSL_ctrl@PLT
	cmpq	socket_mtu(%rip), %rax
	jg	.L314
	movl	$4096, %esi
	movq	%r12, %rdi
	call	SSL_set_options@PLT
	xorl	%ecx, %ecx
	movl	$120, %esi
	movq	%r12, %rdi
	movq	socket_mtu(%rip), %rdx
	call	SSL_ctrl@PLT
	testq	%rax, %rax
	jne	.L60
	movq	bio_err(%rip), %rdi
	leaq	.LC38(%rip), %rsi
	call	BIO_printf@PLT
	movq	%r13, %rdi
	orl	$-1, %r13d
	call	BIO_free@PLT
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L72:
	movq	-216(%rbp), %rsi
	testq	%rsi, %rsi
	je	.L76
	testl	%r13d, %r13d
	jne	.L315
.L77:
	movq	%r15, %rdi
	xorl	%r13d, %r13d
	call	raw_write_stdout@PLT
	movq	bio_s_out(%rip), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	call	BIO_ctrl@PLT
.L76:
	cmpl	$2, %r14d
	jne	.L70
	testl	%r13d, %r13d
	je	.L78
	movq	%r12, %rdi
	call	SSL_get_early_data_status@PLT
	testl	%eax, %eax
	jne	.L79
	movq	bio_s_out(%rip), %rdi
	leaq	.LC42(%rip), %rsi
	call	BIO_printf@PLT
.L80:
	movq	%r12, %rdi
	call	SSL_is_init_finished@PLT
	testl	%eax, %eax
	je	.L71
	movq	%r12, %rdi
	call	print_connection_info
	.p2align 4,,10
	.p2align 3
.L71:
	call	fileno_stdin@PLT
	movl	-228(%rbp), %ebx
	leal	1(%rbx), %ecx
	movl	%ecx, -232(%rbp)
	cmpl	%ebx, %eax
	jg	.L316
.L83:
	movl	-228(%rbp), %ecx
	movq	%r15, -240(%rbp)
	movl	%ecx, %eax
	sarl	$31, %eax
	shrl	$26, %eax
	addl	%eax, %ecx
	andl	$63, %ecx
	subl	%eax, %ecx
	movl	$1, %eax
	salq	%cl, %rax
	movq	%rax, -256(%rbp)
.L84:
	movq	%r12, %rdi
	call	SSL_has_pending@PLT
	movl	%eax, %r13d
	testl	%eax, %eax
	jne	.L92
	movl	async(%rip), %r10d
	testl	%r10d, %r10d
	je	.L93
	movq	%r12, %rdi
	call	SSL_waiting_for_async@PLT
	testl	%eax, %eax
	jne	.L92
.L93:
	leaq	-192(%rbp), %r9
	xorl	%eax, %eax
	movl	$16, %ecx
	movq	%r9, -248(%rbp)
	movq	%r9, %rdi
#APP
# 2420 "../deps/openssl/openssl/apps/s_server.c" 1
	cld; rep; stosq
# 0 "" 2
#NO_APP
	call	fileno_stdin@PLT
	movl	%eax, %ebx
	call	fileno_stdin@PLT
	movslq	%eax, %rdi
	call	__fdelt_chk@PLT
	movslq	-228(%rbp), %r14
	movq	%rax, %r8
	movl	%ebx, %eax
	sarl	$31, %eax
	movq	%r14, %rdi
	shrl	$26, %eax
	leal	(%rbx,%rax), %ecx
	andl	$63, %ecx
	subl	%eax, %ecx
	movl	$1, %eax
	salq	%cl, %rax
	orq	%rax, -192(%rbp,%r8,8)
	call	__fdelt_chk@PLT
	movq	%r12, %rdi
	movq	-256(%rbp), %rcx
	orq	%rcx, -192(%rbp,%rax,8)
	call	SSL_is_dtls@PLT
	movq	-248(%rbp), %r9
	testl	%eax, %eax
	jne	.L317
.L91:
	xorl	%r8d, %r8d
.L94:
	movl	-232(%rbp), %edi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r9, %rsi
	call	select@PLT
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	SSL_is_dtls@PLT
	testl	%eax, %eax
	jne	.L318
.L96:
	testl	%ebx, %ebx
	jle	.L84
	call	fileno_stdin@PLT
	movslq	%eax, %rdi
	call	__fdelt_chk@PLT
	movq	-192(%rbp,%rax,8), %rbx
	call	fileno_stdin@PLT
	movq	%r14, %rdi
	cltd
	shrl	$26, %edx
	leal	(%rax,%rdx), %ecx
	movl	$1, %eax
	andl	$63, %ecx
	subl	%edx, %ecx
	salq	%cl, %rax
	andq	%rax, %rbx
	call	__fdelt_chk@PLT
	movq	-256(%rbp), %rcx
	testq	%rcx, -192(%rbp,%rax,8)
	jne	.L99
	movl	$0, -260(%rbp)
	testq	%rbx, %rbx
	je	.L84
.L100:
	movl	s_crlf(%rip), %r9d
	testl	%r9d, %r9d
	jne	.L319
	movq	-240(%rbp), %rdi
	movl	$16384, %esi
	call	raw_read_stdin@PLT
	movl	%eax, %ebx
	movl	s_quiet(%rip), %eax
	orl	s_brief(%rip), %eax
	jne	.L128
	testl	%ebx, %ebx
	jle	.L282
.L126:
	movq	-240(%rbp), %rax
	movzbl	(%rax), %eax
	cmpb	$81, %al
	je	.L282
	cmpb	$113, %al
	je	.L320
	cmpb	$114, %al
	je	.L321
	cmpb	$82, %al
	jne	.L133
	movq	-240(%rbp), %rax
	movzbl	1(%rax), %eax
	cmpb	$10, %al
	je	.L202
	cmpb	$13, %al
	je	.L202
	.p2align 4,,10
	.p2align 3
.L128:
	movq	-240(%rbp), %r13
	leaq	.LC53(%rip), %r14
	movq	$0, -248(%rbp)
	.p2align 4,,10
	.p2align 3
.L143:
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%ebx, %edx
	call	SSL_write@PLT
	movq	%r12, %rdi
	movl	%eax, %esi
	movl	%eax, %r15d
	call	SSL_get_error@PLT
	cmpl	$4, %eax
	jne	.L322
	movq	bio_s_out(%rip), %rdi
	movq	%r14, %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	16+srp_callback_parm(%rip), %rdi
	call	SRP_user_pwd_free@PLT
	movq	srp_callback_parm(%rip), %rsi
	movq	8+srp_callback_parm(%rip), %rdi
	call	SRP_VBASE_get1_by_user@PLT
	movq	%rax, 16+srp_callback_parm(%rip)
	testq	%rax, %rax
	je	.L142
	movq	40(%rax), %rdx
	movq	bio_s_out(%rip), %rdi
	leaq	.LC54(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L143
	.p2align 4,,10
	.p2align 3
.L151:
	testl	%ebx, %ebx
	jg	.L143
.L152:
	movl	-260(%rbp), %r8d
	testl	%r8d, %r8d
	je	.L84
	.p2align 4,,10
	.p2align 3
.L92:
	movl	async(%rip), %r11d
	testl	%r11d, %r11d
	je	.L87
	movq	%r12, %rdi
	call	SSL_waiting_for_async@PLT
	testl	%eax, %eax
	jne	.L156
	.p2align 4,,10
	.p2align 3
.L87:
	movq	%r12, %rdi
	call	SSL_is_init_finished@PLT
	movl	%eax, %r14d
	testl	%eax, %eax
	jne	.L156
	movl	dtlslisten(%rip), %edi
	testl	%edi, %edi
	jne	.L157
	movl	stateless(%rip), %esi
	leaq	.LC61(%rip), %r13
	testl	%esi, %esi
	je	.L159
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	call	SSL_stateless@PLT
	movl	%eax, %ebx
.L161:
	testl	%ebx, %ebx
	jle	.L162
	movl	$-1, -216(%rbp)
	movl	dtlslisten(%rip), %ecx
	testl	%ecx, %ecx
	jne	.L323
	movl	$0, stateless(%rip)
.L166:
	movq	%r12, %rdi
	call	SSL_accept@PLT
	movl	%eax, %ebx
	testl	%eax, %eax
	jg	.L168
.L169:
	movl	dtlslisten(%rip), %edx
	testl	%edx, %edx
	jne	.L176
	movl	stateless(%rip), %eax
	testl	%ebx, %ebx
	jne	.L177
	testl	%eax, %eax
	jne	.L178
.L277:
	andl	$1, %r14d
	je	.L290
.L178:
	movq	bio_s_out(%rip), %rdi
	leaq	.LC62(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L302:
	movl	-228(%rbp), %edi
	movl	$1, %esi
	call	BIO_socket_nbio@PLT
	testl	%eax, %eax
	je	.L324
	movl	s_quiet(%rip), %edx
	testl	%edx, %edx
	jne	.L48
	movq	bio_err(%rip), %rdi
	leaq	.LC33(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L303:
	movq	tlsext_cb@GOTPCREL(%rip), %rdx
	movq	%r12, %rdi
	movl	$56, %esi
	call	SSL_callback_ctrl@PLT
	xorl	%edx, %edx
	movl	$57, %esi
	movq	%r12, %rdi
	movq	bio_s_out(%rip), %rcx
	call	SSL_ctrl@PLT
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L308:
	movq	%r12, %rdi
	call	SSL_get_rbio@PLT
	movq	bio_dump_callback@GOTPCREL(%rip), %rsi
	movq	%rax, %rdi
	call	BIO_set_callback@PLT
	movq	bio_s_out(%rip), %r13
	movq	%r12, %rdi
	call	SSL_get_rbio@PLT
	movq	%rax, %rdi
	movq	%r13, %rsi
	call	BIO_set_callback_arg@PLT
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L307:
	call	BIO_f_nbio_test@PLT
	movq	%rax, %rdi
	call	BIO_new@PLT
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	BIO_push@PLT
	movq	%rax, %r13
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L58:
	movl	$39, %esi
	movq	%r13, %rdi
	call	BIO_ctrl@PLT
.L60:
	movl	$8192, %esi
	movq	%r12, %rdi
	call	SSL_set_options@PLT
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L156:
	movq	-240(%rbp), %r14
	leaq	.LC64(%rip), %rbx
	leaq	.L186(%rip), %r13
	.p2align 4,,10
	.p2align 3
.L183:
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$16384, %edx
	call	SSL_read@PLT
	movq	%r12, %rdi
	movl	%eax, %esi
	movl	%eax, %r15d
	call	SSL_get_error@PLT
	cmpl	$4, %eax
	jne	.L325
	movq	bio_s_out(%rip), %rdi
	movq	%rbx, %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	16+srp_callback_parm(%rip), %rdi
	call	SRP_user_pwd_free@PLT
	movq	srp_callback_parm(%rip), %rsi
	movq	8+srp_callback_parm(%rip), %rdi
	call	SRP_VBASE_get1_by_user@PLT
	movq	%rax, 16+srp_callback_parm(%rip)
	testq	%rax, %rax
	je	.L182
	movq	40(%rax), %rdx
	movq	bio_s_out(%rip), %rdi
	leaq	.LC54(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L183
	.p2align 4,,10
	.p2align 3
.L142:
	movq	bio_s_out(%rip), %rdi
	leaq	.LC55(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L143
	.p2align 4,,10
	.p2align 3
.L322:
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	SSL_get_error@PLT
	cmpl	$10, %eax
	ja	.L145
	leaq	.L147(%rip), %rcx
	movl	%eax, %eax
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L147:
	.long	.L145-.L147
	.long	.L146-.L147
	.long	.L150-.L147
	.long	.L150-.L147
	.long	.L150-.L147
	.long	.L146-.L147
	.long	.L149-.L147
	.long	.L145-.L147
	.long	.L145-.L147
	.long	.L148-.L147
	.long	.L146-.L147
	.text
	.p2align 4,,10
	.p2align 3
.L150:
	movq	bio_s_out(%rip), %rdi
	leaq	.LC57(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_s_out(%rip), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	call	BIO_ctrl@PLT
.L145:
	testl	%r15d, %r15d
	jle	.L151
	subl	%r15d, %ebx
	movslq	%r15d, %rax
	addq	%rax, -248(%rbp)
	movq	-248(%rbp), %rax
	testl	%ebx, %ebx
	jle	.L152
	movq	-240(%rbp), %rcx
	leaq	(%rcx,%rax), %r13
	jmp	.L143
	.p2align 4,,10
	.p2align 3
.L146:
	movq	-240(%rbp), %r15
.L298:
	movq	bio_s_out(%rip), %rdi
	leaq	.LC58(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_s_out(%rip), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	call	BIO_ctrl@PLT
	jmp	.L301
	.p2align 4,,10
	.p2align 3
.L148:
	movq	bio_s_out(%rip), %rdi
	leaq	.LC56(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_s_out(%rip), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	call	BIO_ctrl@PLT
	movq	%r12, %rdi
	call	wait_for_async@PLT
	jmp	.L145
	.p2align 4,,10
	.p2align 3
.L149:
	movq	-240(%rbp), %r15
.L297:
	movq	bio_s_out(%rip), %rdi
	leaq	.LC48(%rip), %rsi
	xorl	%eax, %eax
	movl	$1, %r13d
	call	BIO_printf@PLT
	movq	bio_s_out(%rip), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	call	BIO_ctrl@PLT
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L182:
	movq	bio_s_out(%rip), %rdi
	leaq	.LC55(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L183
	.p2align 4,,10
	.p2align 3
.L312:
	movq	SSL_trace@GOTPCREL(%rip), %rsi
	movq	%r12, %rdi
	call	SSL_set_msg_callback@PLT
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L325:
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	SSL_get_error@PLT
	cmpl	$10, %eax
	ja	.L84
	movl	%eax, %eax
	movslq	0(%r13,%rax,4), %rax
	addq	%r13, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L186:
	.long	.L190-.L186
	.long	.L185-.L186
	.long	.L189-.L186
	.long	.L189-.L186
	.long	.L84-.L186
	.long	.L185-.L186
	.long	.L188-.L186
	.long	.L84-.L186
	.long	.L84-.L186
	.long	.L187-.L186
	.long	.L185-.L186
	.text
	.p2align 4,,10
	.p2align 3
.L133:
	movl	%eax, %edx
	andl	$-33, %edx
	cmpb	$75, %dl
	jne	.L135
	movq	-240(%rbp), %rcx
	movzbl	1(%rcx), %edx
	cmpb	$10, %dl
	je	.L203
	cmpb	$13, %dl
	je	.L203
.L135:
	cmpb	$99, %al
	jne	.L137
	movq	-240(%rbp), %rax
	movzbl	1(%rax), %eax
	cmpb	$10, %al
	je	.L204
	cmpb	$13, %al
	jne	.L128
.L204:
	xorl	%edx, %edx
	movq	%r12, %rdi
	movl	$1, %esi
	call	SSL_set_verify@PLT
	movq	%r12, %rdi
	call	SSL_verify_client_post_handshake@PLT
	testl	%eax, %eax
	jne	.L293
	leaq	.LC51(%rip), %rdi
	call	puts@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L313:
	movdqa	.LC36(%rip), %xmm0
	leaq	-208(%rbp), %r14
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	%r14, %rcx
	movl	$33, %esi
	movaps	%xmm0, -208(%rbp)
	call	BIO_ctrl@PLT
	movq	%r14, %rcx
	xorl	%edx, %edx
	movl	$35, %esi
	movdqa	.LC36(%rip), %xmm0
	movq	%r13, %rdi
	movaps	%xmm0, -208(%rbp)
	call	BIO_ctrl@PLT
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L318:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$74, %esi
	movq	%r12, %rdi
	call	SSL_ctrl@PLT
	testq	%rax, %rax
	jle	.L96
	movq	bio_err(%rip), %rdi
	leaq	.LC45(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L317:
	leaq	-208(%rbp), %rbx
	xorl	%edx, %edx
	movl	$73, %esi
	movq	%r12, %rdi
	movq	%rbx, %rcx
	movq	%r9, -248(%rbp)
	call	SSL_ctrl@PLT
	movq	-248(%rbp), %r9
	testq	%rax, %rax
	je	.L91
	movq	%rbx, %r8
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L316:
	call	fileno_stdin@PLT
	addl	$1, %eax
	movl	%eax, -232(%rbp)
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L324:
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L319:
	movq	-240(%rbp), %r14
	movl	$8192, %esi
	movq	%r14, %rdi
	call	raw_read_stdin@PLT
	movl	%eax, %ebx
	testl	%eax, %eax
	jle	.L102
	leal	-1(%rax), %eax
	cmpl	$14, %eax
	jbe	.L198
	movl	%ebx, %ecx
	pxor	%xmm1, %xmm1
	pxor	%xmm6, %xmm6
	movq	%r14, %rdx
	shrl	$4, %ecx
	pxor	%xmm5, %xmm5
	salq	$4, %rcx
	addq	%r14, %rcx
	.p2align 4,,10
	.p2align 3
.L104:
	movdqu	(%rdx), %xmm0
	movdqa	%xmm6, %xmm2
	movdqa	%xmm5, %xmm4
	addq	$16, %rdx
	pcmpeqb	.LC46(%rip), %xmm0
	pand	.LC47(%rip), %xmm0
	pcmpgtb	%xmm0, %xmm2
	movdqa	%xmm0, %xmm3
	punpcklbw	%xmm2, %xmm3
	punpckhbw	%xmm2, %xmm0
	pcmpgtw	%xmm3, %xmm4
	movdqa	%xmm3, %xmm2
	punpcklwd	%xmm4, %xmm2
	punpckhwd	%xmm4, %xmm3
	paddd	%xmm2, %xmm1
	movdqa	%xmm5, %xmm2
	pcmpgtw	%xmm0, %xmm2
	paddd	%xmm3, %xmm1
	movdqa	%xmm0, %xmm3
	punpcklwd	%xmm2, %xmm3
	punpckhwd	%xmm2, %xmm0
	paddd	%xmm3, %xmm1
	paddd	%xmm0, %xmm1
	cmpq	%rdx, %rcx
	jne	.L104
	movdqa	%xmm1, %xmm0
	movl	%ebx, %edx
	psrldq	$8, %xmm0
	andl	$-16, %edx
	paddd	%xmm1, %xmm0
	movdqa	%xmm0, %xmm1
	psrldq	$4, %xmm1
	paddd	%xmm1, %xmm0
	movd	%xmm0, %r13d
	testb	$15, %bl
	je	.L105
.L103:
	movq	-240(%rbp), %rsi
	movslq	%edx, %rcx
	cmpb	$10, (%rsi,%rcx)
	sete	%cl
	movzbl	%cl, %ecx
	addl	%ecx, %r13d
	leal	1(%rdx), %ecx
	cmpl	%ebx, %ecx
	jge	.L105
	movslq	%ecx, %rcx
	cmpb	$10, (%rsi,%rcx)
	sete	%cl
	movzbl	%cl, %ecx
	addl	%ecx, %r13d
	leal	2(%rdx), %ecx
	cmpl	%ecx, %ebx
	jle	.L105
	movslq	%ecx, %rcx
	cmpb	$10, (%rsi,%rcx)
	sete	%cl
	movzbl	%cl, %ecx
	addl	%ecx, %r13d
	leal	3(%rdx), %ecx
	cmpl	%ebx, %ecx
	jge	.L105
	movslq	%ecx, %rcx
	cmpb	$10, (%rsi,%rcx)
	sete	%cl
	movzbl	%cl, %ecx
	addl	%ecx, %r13d
	leal	4(%rdx), %ecx
	cmpl	%ebx, %ecx
	jge	.L105
	movslq	%ecx, %rcx
	cmpb	$10, (%rsi,%rcx)
	sete	%cl
	movzbl	%cl, %ecx
	addl	%ecx, %r13d
	leal	5(%rdx), %ecx
	cmpl	%ebx, %ecx
	jge	.L105
	movslq	%ecx, %rcx
	cmpb	$10, (%rsi,%rcx)
	sete	%cl
	movzbl	%cl, %ecx
	addl	%ecx, %r13d
	leal	6(%rdx), %ecx
	cmpl	%ebx, %ecx
	jge	.L105
	movslq	%ecx, %rcx
	cmpb	$10, (%rsi,%rcx)
	sete	%cl
	movzbl	%cl, %ecx
	addl	%ecx, %r13d
	leal	7(%rdx), %ecx
	cmpl	%ecx, %ebx
	jle	.L105
	movslq	%ecx, %rcx
	cmpb	$10, (%rsi,%rcx)
	sete	%cl
	movzbl	%cl, %ecx
	addl	%ecx, %r13d
	leal	8(%rdx), %ecx
	cmpl	%ecx, %ebx
	jle	.L105
	movslq	%ecx, %rcx
	cmpb	$10, (%rsi,%rcx)
	sete	%cl
	movzbl	%cl, %ecx
	addl	%ecx, %r13d
	leal	9(%rdx), %ecx
	cmpl	%ecx, %ebx
	jle	.L105
	movslq	%ecx, %rcx
	cmpb	$10, (%rsi,%rcx)
	sete	%cl
	movzbl	%cl, %ecx
	addl	%ecx, %r13d
	leal	10(%rdx), %ecx
	cmpl	%ecx, %ebx
	jle	.L105
	movslq	%ecx, %rcx
	cmpb	$10, (%rsi,%rcx)
	sete	%cl
	movzbl	%cl, %ecx
	addl	%ecx, %r13d
	leal	11(%rdx), %ecx
	cmpl	%ebx, %ecx
	jge	.L105
	movslq	%ecx, %rcx
	cmpb	$10, (%rsi,%rcx)
	sete	%cl
	movzbl	%cl, %ecx
	addl	%ecx, %r13d
	leal	12(%rdx), %ecx
	cmpl	%ecx, %ebx
	jle	.L105
	movslq	%ecx, %rcx
	cmpb	$10, (%rsi,%rcx)
	sete	%cl
	movzbl	%cl, %ecx
	addl	%ecx, %r13d
	leal	13(%rdx), %ecx
	cmpl	%ecx, %ebx
	jle	.L105
	movslq	%ecx, %rcx
	cmpb	$10, (%rsi,%rcx)
	sete	%cl
	addl	$14, %edx
	movzbl	%cl, %ecx
	addl	%ecx, %r13d
	cmpl	%edx, %ebx
	jle	.L105
	movslq	%edx, %rdx
	cmpb	$10, (%rsi,%rdx)
	sete	%dl
	movzbl	%dl, %edx
	addl	%edx, %r13d
	.p2align 4,,10
	.p2align 3
.L105:
	movq	-240(%rbp), %rsi
	cltq
	.p2align 4,,10
	.p2align 3
.L125:
	movzbl	(%rsi,%rax), %ecx
	leal	0(%r13,%rax), %edx
	movslq	%edx, %rdx
	movb	%cl, (%rsi,%rdx)
	cmpb	$10, %cl
	jne	.L124
	subl	$1, %r13d
	addl	$1, %ebx
	leal	0(%r13,%rax), %edx
	movslq	%edx, %rdx
	movb	$13, (%rsi,%rdx)
.L124:
	subq	$1, %rax
	testl	%eax, %eax
	jns	.L125
	movl	s_quiet(%rip), %eax
	orl	s_brief(%rip), %eax
	je	.L126
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L99:
	testq	%rbx, %rbx
	je	.L92
	movl	$1, -260(%rbp)
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L321:
	movq	-240(%rbp), %rax
	movzbl	1(%rax), %eax
	cmpb	$10, %al
	je	.L292
	cmpb	$13, %al
	jne	.L128
.L292:
	movq	%r12, %rdi
	call	SSL_renegotiate@PLT
.L293:
	movq	%r12, %rdi
	call	SSL_do_handshake@PLT
	leaq	.LC50(%rip), %rsi
	movl	$1, %edi
	movl	%eax, %edx
	xorl	%eax, %eax
	call	__printf_chk@PLT
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L159:
	movq	%r12, %rdi
	call	SSL_accept@PLT
	movl	%eax, %ebx
	testl	%eax, %eax
	jle	.L326
	.p2align 4,,10
	.p2align 3
.L168:
	movq	%r12, %rdi
	call	print_connection_info
	jmp	.L84
.L157:
	call	BIO_ADDR_new@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L327
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	DTLSv1_listen@PLT
	movl	%eax, %ebx
	jmp	.L161
.L185:
	movq	%r14, %r15
	jmp	.L298
.L189:
	movq	bio_s_out(%rip), %rdi
	leaq	.LC66(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_s_out(%rip), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	call	BIO_ctrl@PLT
	jmp	.L84
.L187:
	movq	bio_s_out(%rip), %rdi
	leaq	.LC65(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_s_out(%rip), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	call	BIO_ctrl@PLT
	movq	%r12, %rdi
	call	wait_for_async@PLT
	jmp	.L84
.L190:
	movl	%r15d, %esi
	movq	%r14, %rdi
	call	raw_write_stdout@PLT
	movq	bio_s_out(%rip), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	call	BIO_ctrl@PLT
	movq	%r12, %rdi
	call	SSL_has_pending@PLT
	testl	%eax, %eax
	jne	.L183
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L188:
	movq	%r14, %r15
	jmp	.L297
.L202:
	xorl	%edx, %edx
	movl	$5, %esi
	movq	%r12, %rdi
	call	SSL_set_verify@PLT
	jmp	.L292
.L326:
	movl	%eax, %esi
	movq	%r12, %rdi
	call	SSL_get_error@PLT
	movl	%ebx, %esi
	movq	%r12, %rdi
	movl	%eax, %ecx
	leal	-5(%rax), %eax
	cmpl	$1, %eax
	seta	%al
	xorl	%edx, %edx
	cmpl	$1, %ecx
	setne	%dl
	andl	%eax, %edx
	movl	%edx, %r14d
	call	SSL_get_error@PLT
	cmpl	$4, %eax
	jne	.L328
	.p2align 4,,10
	.p2align 3
.L174:
	movq	srp_callback_parm(%rip), %rdx
	movq	bio_s_out(%rip), %rdi
	movq	%r13, %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	16+srp_callback_parm(%rip), %rdi
	call	SRP_user_pwd_free@PLT
	movq	srp_callback_parm(%rip), %rsi
	movq	8+srp_callback_parm(%rip), %rdi
	call	SRP_VBASE_get1_by_user@PLT
	movq	%rax, 16+srp_callback_parm(%rip)
	testq	%rax, %rax
	je	.L172
	movq	40(%rax), %rdx
	movq	bio_s_out(%rip), %rdi
	leaq	.LC54(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L295:
	movq	%r12, %rdi
	call	SSL_accept@PLT
	movl	%eax, %ebx
	testl	%eax, %eax
	jg	.L168
	movl	%eax, %esi
	movq	%r12, %rdi
	call	SSL_get_error@PLT
	movl	%ebx, %esi
	movq	%r12, %rdi
	leal	-5(%rax), %edx
	cmpl	$1, %edx
	seta	%dl
	cmpl	$1, %eax
	setne	%al
	movzbl	%al, %eax
	andl	%edx, %eax
	movl	%eax, %r14d
	call	SSL_get_error@PLT
	cmpl	$4, %eax
	je	.L174
.L328:
	testl	%ebx, %ebx
	je	.L169
	movq	%r12, %rdi
	call	SSL_waiting_for_async@PLT
	testl	%eax, %eax
	jne	.L159
	movl	dtlslisten(%rip), %eax
	testl	%eax, %eax
	jne	.L290
	movl	stateless(%rip), %eax
.L177:
	testl	%eax, %eax
	je	.L277
.L290:
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
	leaq	.LC58(%rip), %rsi
	movq	-240(%rbp), %r15
	call	BIO_printf@PLT
	movq	%r12, %rdi
	call	SSL_get_verify_result@PLT
	testq	%rax, %rax
	je	.L301
	movq	%rax, %rdi
	call	X509_verify_cert_error_string@PLT
	movq	bio_err(%rip), %rdi
	leaq	.LC63(%rip), %rsi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L301
	.p2align 4,,10
	.p2align 3
.L172:
	movq	bio_s_out(%rip), %rdi
	leaq	.LC55(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L295
.L137:
	cmpb	$80, %al
	jne	.L140
	movq	%r12, %rdi
	call	SSL_get_wbio@PLT
	movl	$27, %edx
	leaq	.LC52(%rip), %rsi
	movq	%rax, %rdi
	call	BIO_write@PLT
	movq	-240(%rbp), %rax
	movzbl	(%rax), %eax
.L140:
	cmpb	$83, %al
	jne	.L128
	movq	%r12, %rdi
	call	SSL_get_SSL_CTX@PLT
	movq	bio_s_out(%rip), %rdi
	movq	%rax, %rsi
	call	print_stats
	jmp	.L128
.L78:
	movq	bio_s_out(%rip), %rdi
	leaq	.LC44(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L80
.L176:
	testl	%ebx, %ebx
	je	.L178
	jmp	.L290
	.p2align 4,,10
	.p2align 3
.L162:
	movq	%r13, %rdi
	call	BIO_ADDR_free@PLT
	jmp	.L169
.L323:
	movq	%r12, %rdi
	call	SSL_get_wbio@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L165
	xorl	%edx, %edx
	movl	$105, %esi
	movq	%rax, %rdi
	leaq	-216(%rbp), %rcx
	call	BIO_ctrl@PLT
	movl	-216(%rbp), %edi
	xorl	%edx, %edx
	movq	%r13, %rsi
	call	BIO_connect@PLT
	testl	%eax, %eax
	je	.L165
	movq	%r13, %rcx
	xorl	%edx, %edx
	movl	$32, %esi
	movq	%rbx, %rdi
	call	BIO_ctrl@PLT
	movq	%r13, %rdi
	call	BIO_ADDR_free@PLT
	movl	$0, dtlslisten(%rip)
	jmp	.L166
.L203:
	xorl	%esi, %esi
	cmpb	$75, %al
	movq	%r12, %rdi
	sete	%sil
	call	SSL_key_update@PLT
	jmp	.L293
.L315:
	movq	bio_s_out(%rip), %rdi
	leaq	.LC41(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	-216(%rbp), %rsi
	jmp	.L77
.L165:
	movq	bio_err(%rip), %rdi
	leaq	.LC60(%rip), %rsi
	xorl	%eax, %eax
	movq	-240(%rbp), %r15
	call	BIO_printf@PLT
	movq	%r13, %rdi
	movl	$1, %r13d
	call	BIO_ADDR_free@PLT
	jmp	.L54
.L102:
	movl	s_quiet(%rip), %eax
	orl	s_brief(%rip), %eax
	jne	.L128
.L282:
	movq	bio_s_out(%rip), %rdi
	leaq	.LC48(%rip), %rsi
	xorl	%eax, %eax
	movq	-240(%rbp), %r15
	movl	$-11, %r13d
	call	BIO_printf@PLT
	movq	bio_s_out(%rip), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	call	BIO_ctrl@PLT
	movl	-228(%rbp), %edi
	call	BIO_closesocket@PLT
	movq	bio_err(%rip), %rdi
	leaq	.LC49(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movl	accept_socket(%rip), %edi
	testl	%edi, %edi
	js	.L54
.L299:
	call	BIO_closesocket@PLT
	jmp	.L54
.L79:
	movq	bio_s_out(%rip), %rdi
	leaq	.LC43(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L80
.L198:
	xorl	%edx, %edx
	jmp	.L103
.L327:
	movq	bio_err(%rip), %rdi
	leaq	.LC59(%rip), %rsi
	xorl	%eax, %eax
	movq	-240(%rbp), %r15
	movl	$1, %r13d
	call	BIO_printf@PLT
	jmp	.L54
.L305:
	movq	bio_err(%rip), %rdi
	leaq	.LC35(%rip), %rsi
	movl	$-1, %r13d
	call	BIO_printf@PLT
	jmp	.L54
.L195:
	movl	$-1, %r13d
	jmp	.L51
.L304:
	movq	bio_err(%rip), %rdi
	leaq	.LC34(%rip), %rsi
	movl	$-1, %r13d
	call	BIO_printf@PLT
	jmp	.L54
.L320:
	movq	bio_s_out(%rip), %rdi
	leaq	.LC48(%rip), %rsi
	xorl	%eax, %eax
	movq	-240(%rbp), %r15
	movl	$1, %r13d
	call	BIO_printf@PLT
	movq	bio_s_out(%rip), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	call	BIO_ctrl@PLT
	movq	%r12, %rdi
	call	SSL_version@PLT
	cmpl	$65279, %eax
	je	.L54
	movl	-228(%rbp), %edi
	jmp	.L299
.L314:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$121, %esi
	movq	%r12, %rdi
	call	SSL_ctrl@PLT
	movq	bio_err(%rip), %rdi
	leaq	.LC37(%rip), %rsi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	%r13, %rdi
	movl	$-1, %r13d
	call	BIO_free@PLT
	jmp	.L54
.L311:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1643:
	.size	sv_body, .-sv_body
	.section	.rodata.str1.1
.LC69:
	.string	" "
.LC70:
	.string	"---\nReused, "
.LC71:
	.string	"---\nNew, "
.LC72:
	.string	"server www buffer"
.LC73:
	.string	"read R BLOCK\n"
.LC74:
	.string	"GET "
.LC75:
	.string	"GET /stats "
.LC76:
	.string	"GET /renegcert"
.LC77:
	.string	"SSL_renegotiate -> %d\n"
.LC78:
	.string	"SSL_do_handshake() Retval %d\n"
	.section	.rodata.str1.8
	.align 8
.LC79:
	.string	"Error waiting for client response\n"
	.align 8
.LC80:
	.string	"HTTP/1.0 200 ok\r\nContent-type: text/html\r\n\r\n"
	.align 8
.LC81:
	.string	"<HTML><BODY BGCOLOR=\"#ffffff\">\n"
	.section	.rodata.str1.1
.LC82:
	.string	"<pre>\n"
.LC83:
	.string	"&lt;"
.LC84:
	.string	"&gt;"
.LC85:
	.string	"&amp;"
	.section	.rodata.str1.8
	.align 8
.LC86:
	.string	"Ciphers supported in s_server binary\n"
	.section	.rodata.str1.1
.LC87:
	.string	"%-11s:%-25s "
	.section	.rodata.str1.8
	.align 8
.LC88:
	.string	"---\nCiphers common between both SSL end points:\n"
	.section	.rodata.str1.1
.LC89:
	.string	"                          "
.LC90:
	.string	"%s, Cipher is %s\n"
.LC91:
	.string	"---\n"
	.section	.rodata.str1.8
	.align 8
.LC92:
	.string	"no client certificate available\n"
	.section	.rodata.str1.1
.LC93:
	.string	"</pre></BODY></HTML>\r\n\r\n"
.LC94:
	.string	"GET /"
	.section	.rodata.str1.8
	.align 8
.LC95:
	.string	"HTTP/1.0 200 ok\r\nContent-type: text/plain\r\n\r\n"
	.section	.rodata.str1.1
.LC96:
	.string	"'%s' contains '..' or ':'\r\n"
.LC97:
	.string	"'%s' is an invalid path\r\n"
.LC98:
	.string	"'%s' is a directory\r\n"
.LC99:
	.string	"r"
.LC100:
	.string	"Error opening '%s'\r\n"
.LC101:
	.string	"FILE:%s\n"
.LC102:
	.string	".html"
.LC103:
	.string	".php"
.LC104:
	.string	".htm"
.LC105:
	.string	"rwrite W BLOCK\n"
.LC106:
	.string	"GET /reneg"
	.section	.rodata.str1.8
	.align 8
.LC107:
	.string	"'%s' is an invalid file name\r\n"
	.text
	.p2align 4
	.type	www_body, @function
www_body:
.LFB1649:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC72(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$184, %rsp
	movl	%edi, -200(%rbp)
	movl	$16384, %edi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	app_malloc@PLT
	movq	%rax, %r13
	call	BIO_f_buffer@PLT
	movq	%rax, %rdi
	call	BIO_new@PLT
	movq	%rax, %r12
	call	BIO_f_ssl@PLT
	movq	%rax, %rdi
	call	BIO_new@PLT
	testq	%r12, %r12
	je	.L332
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L332
	movl	s_nbio(%rip), %r10d
	testl	%r10d, %r10d
	jne	.L506
.L334:
	movl	$1, %ecx
	movl	$16384, %edx
	movl	$117, %esi
	movq	%r12, %rdi
	call	BIO_int_ctrl@PLT
	testq	%rax, %rax
	jne	.L507
.L332:
	movl	$3333, %edx
	leaq	.LC31(%rip), %rsi
	movq	%r13, %rdi
	call	CRYPTO_free@PLT
	movq	%r12, %rdi
	call	BIO_free_all@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L508
	addq	$184, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L507:
	.cfi_restore_state
	movq	ctx(%rip), %rdi
	call	SSL_new@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L332
	movl	s_tlsextdebug(%rip), %r8d
	testl	%r8d, %r8d
	jne	.L509
.L338:
	testq	%rbx, %rbx
	je	.L339
	movq	%rbx, %rdi
	call	strlen@PLT
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	SSL_set_session_id_context@PLT
	testl	%eax, %eax
	je	.L510
.L339:
	movl	-200(%rbp), %edi
	xorl	%esi, %esi
	call	BIO_new_socket@PLT
	movl	s_nbio_test(%rip), %edi
	movq	%rax, %rsi
	testl	%edi, %edi
	jne	.L511
.L340:
	movq	%rsi, %rdx
	movq	%r14, %rdi
	call	SSL_set_bio@PLT
	movq	%r14, %rdi
	call	SSL_set_accept_state@PLT
	movq	%r14, %rcx
	movl	$1, %edx
	movq	%r15, %rdi
	movl	$109, %esi
	call	BIO_ctrl@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	BIO_push@PLT
	movl	s_debug(%rip), %esi
	testl	%esi, %esi
	jne	.L512
.L341:
	movl	s_msg(%rip), %eax
	testl	%eax, %eax
	jne	.L513
.L342:
	leaq	.LC94(%rip), %rbx
	leaq	.LC74(%rip), %r15
	.p2align 4,,10
	.p2align 3
.L346:
	movl	$16383, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	BIO_gets@PLT
	testl	%eax, %eax
	js	.L514
	je	.L417
	movl	www(%rip), %eax
	cmpl	$1, %eax
	je	.L515
	cmpl	$2, %eax
	jne	.L355
	movl	$11, %ecx
	leaq	.LC75(%rip), %rsi
	movq	%r13, %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L356
.L357:
	movl	$5, %ecx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L346
	movzbl	5(%r13), %edx
	leaq	5(%r13), %r15
	testb	%dl, %dl
	je	.L389
	movq	%r15, %rcx
	movl	$1, %eax
	jmp	.L399
	.p2align 4,,10
	.p2align 3
.L516:
	cmpb	$46, %dl
	je	.L393
.L504:
	cmpb	$47, %dl
	movq	%rdi, %rcx
	sete	%al
	cmpb	$92, %dl
	sete	%dl
	orl	%edx, %eax
	movl	%esi, %edx
.L394:
	movzbl	%al, %eax
.L398:
	testb	%dl, %dl
	je	.L389
.L399:
	cmpb	$32, %dl
	je	.L390
	cmpb	$58, %dl
	je	.L391
	movzbl	1(%rcx), %esi
	leaq	1(%rcx), %rdi
	cmpl	$1, %eax
	je	.L516
	testl	%eax, %eax
	je	.L504
	movq	%rdi, %rcx
	movl	%esi, %edx
	movl	$-1, %eax
	jmp	.L398
	.p2align 4,,10
	.p2align 3
.L506:
	movl	-200(%rbp), %edi
	movl	$1, %esi
	call	BIO_socket_nbio@PLT
	testl	%eax, %eax
	je	.L517
	movl	s_quiet(%rip), %r9d
	testl	%r9d, %r9d
	jne	.L334
	movq	bio_err(%rip), %rdi
	leaq	.LC33(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L334
	.p2align 4,,10
	.p2align 3
.L514:
	movl	$8, %esi
	movq	%r12, %rdi
	call	BIO_test_flags@PLT
	testl	%eax, %eax
	jne	.L348
	movq	%r14, %rdi
	call	SSL_waiting_for_async@PLT
	testl	%eax, %eax
	je	.L518
.L348:
	movq	bio_s_out(%rip), %rdi
	leaq	.LC73(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movl	$4, %esi
	movq	%r12, %rdi
	call	BIO_test_flags@PLT
	testl	%eax, %eax
	je	.L349
	movq	%r12, %rdi
	call	BIO_get_retry_reason@PLT
	cmpl	$1, %eax
	je	.L519
.L349:
	movl	$1, %edi
	call	sleep@PLT
	jmp	.L346
	.p2align 4,,10
	.p2align 3
.L515:
	movl	$4, %ecx
	movq	%r15, %rsi
	movq	%r13, %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L346
	movl	$10, %ecx
	leaq	.LC106(%rip), %rsi
	movq	%r13, %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L356
	movl	$14, %ecx
	leaq	.LC76(%rip), %rsi
	movq	%r13, %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L520
.L358:
	movq	%r14, %rdi
	call	SSL_renegotiate@PLT
	movq	bio_s_out(%rip), %rdi
	leaq	.LC77(%rip), %rsi
	movl	%eax, %edx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	%r14, %rdi
	call	SSL_do_handshake@PLT
	movl	%eax, %esi
	testl	%eax, %eax
	jle	.L521
	leaq	-192(%rbp), %rsi
	xorl	%eax, %eax
	movl	$16, %ecx
	movq	%rsi, -208(%rbp)
	movq	%rsi, %rdi
#APP
# 3084 "../deps/openssl/openssl/apps/s_server.c" 1
	cld; rep; stosq
# 0 "" 2
#NO_APP
	movslq	-200(%rbp), %r15
	movl	$1, %ebx
	movq	%r15, %rdi
	call	__fdelt_chk@PLT
	movq	-208(%rbp), %rsi
	xorl	%edx, %edx
	leal	1(%r15), %edi
	movq	%rax, %r8
	movl	%r15d, %eax
	sarl	$31, %eax
	shrl	$26, %eax
	leal	(%r15,%rax), %ecx
	andl	$63, %ecx
	subl	%eax, %ecx
	salq	%cl, %rbx
	xorl	%ecx, %ecx
	orq	%rbx, -192(%rbp,%r8,8)
	xorl	%r8d, %r8d
	call	select@PLT
	testl	%eax, %eax
	jle	.L361
	movq	%r15, %rdi
	call	__fdelt_chk@PLT
	testq	%rbx, -192(%rbp,%rax,8)
	je	.L361
	movl	$16383, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	BIO_gets@PLT
.L356:
	leaq	.LC80(%rip), %rsi
	movq	%r12, %rdi
	xorl	%ebx, %ebx
	call	BIO_puts@PLT
	leaq	.LC81(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_puts@PLT
	leaq	.LC82(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_puts@PLT
	leaq	.LC19(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_puts@PLT
	movl	local_argc(%rip), %edx
	testl	%edx, %edx
	jle	.L366
	.p2align 4,,10
	.p2align 3
.L362:
	movq	local_argv(%rip), %rax
	movq	(%rax,%rbx,8), %r15
	movzbl	(%r15), %eax
	testb	%al, %al
	jne	.L365
	jmp	.L372
	.p2align 4,,10
	.p2align 3
.L523:
	cmpb	$38, %al
	je	.L522
	movl	$1, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	BIO_write@PLT
.L371:
	movzbl	1(%r15), %eax
	addq	$1, %r15
	testb	%al, %al
	je	.L372
.L365:
	cmpb	$60, %al
	je	.L367
	cmpb	$62, %al
	jne	.L523
	leaq	.LC84(%rip), %rsi
	movq	%r12, %rdi
	addq	$1, %r15
	call	BIO_puts@PLT
	movzbl	(%r15), %eax
	testb	%al, %al
	jne	.L365
.L372:
	movl	$1, %edx
	leaq	.LC69(%rip), %rsi
	movq	%r12, %rdi
	addq	$1, %rbx
	call	BIO_write@PLT
	cmpl	%ebx, local_argc(%rip)
	jg	.L362
.L366:
	leaq	.LC19(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_puts@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$76, %esi
	movq	%r14, %rdi
	call	SSL_ctrl@PLT
	leaq	.LC13(%rip), %rdx
	leaq	.LC22(%rip), %rsi
	movq	%r12, %rdi
	testq	%rax, %rax
	leaq	.LC14(%rip), %rax
	cmove	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	leaq	.LC86(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	%r14, %rdi
	call	SSL_get_ciphers@PLT
	movq	%rax, %rdi
	movq	%rax, %rbx
	call	OPENSSL_sk_num@PLT
	movl	%eax, -212(%rbp)
	testl	%eax, %eax
	jle	.L378
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %r15
	movq	%rax, %rdi
	call	SSL_CIPHER_get_name@PLT
	movq	%r15, %rdi
	movl	$1, %r15d
	movq	%rax, -200(%rbp)
	call	SSL_CIPHER_get_version@PLT
	movq	-200(%rbp), %rcx
	leaq	.LC87(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	cmpl	$1, -212(%rbp)
	je	.L378
	.p2align 4,,10
	.p2align 3
.L379:
	movl	%r15d, %esi
	movq	%rbx, %rdi
	addl	$1, %r15d
	call	OPENSSL_sk_value@PLT
	movq	%rax, %rdi
	movq	%rax, -208(%rbp)
	call	SSL_CIPHER_get_name@PLT
	movq	-208(%rbp), %rdi
	movq	%rax, -200(%rbp)
	call	SSL_CIPHER_get_version@PLT
	movq	-200(%rbp), %rcx
	leaq	.LC87(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	cmpl	-212(%rbp), %r15d
	je	.L378
	testb	$1, %r15b
	jne	.L376
	leaq	.LC19(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_puts@PLT
	jmp	.L379
	.p2align 4,,10
	.p2align 3
.L355:
	cmpl	$3, %eax
	jne	.L346
	jmp	.L357
	.p2align 4,,10
	.p2align 3
.L519:
	movq	bio_s_out(%rip), %rdi
	leaq	.LC64(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	16+srp_callback_parm(%rip), %rdi
	call	SRP_user_pwd_free@PLT
	movq	srp_callback_parm(%rip), %rsi
	movq	8+srp_callback_parm(%rip), %rdi
	call	SRP_VBASE_get1_by_user@PLT
	movq	%rax, 16+srp_callback_parm(%rip)
	testq	%rax, %rax
	je	.L350
	movq	40(%rax), %rdx
	movq	bio_s_out(%rip), %rdi
	leaq	.LC54(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L346
	.p2align 4,,10
	.p2align 3
.L513:
	cmpl	$2, %eax
	je	.L524
	movq	msg_cb@GOTPCREL(%rip), %rsi
	movq	%r14, %rdi
	call	SSL_set_msg_callback@PLT
.L344:
	movq	bio_s_msg(%rip), %rcx
	movl	$16, %esi
	movq	%r14, %rdi
	testq	%rcx, %rcx
	cmove	bio_s_out(%rip), %rcx
	xorl	%edx, %edx
	call	SSL_ctrl@PLT
	jmp	.L342
	.p2align 4,,10
	.p2align 3
.L509:
	movq	tlsext_cb@GOTPCREL(%rip), %rdx
	movl	$56, %esi
	movq	%rax, %rdi
	call	SSL_callback_ctrl@PLT
	xorl	%edx, %edx
	movl	$57, %esi
	movq	%r14, %rdi
	movq	bio_s_out(%rip), %rcx
	call	SSL_ctrl@PLT
	jmp	.L338
	.p2align 4,,10
	.p2align 3
.L512:
	movq	%r14, %rdi
	call	SSL_get_rbio@PLT
	movq	bio_dump_callback@GOTPCREL(%rip), %rsi
	movq	%rax, %rdi
	call	BIO_set_callback@PLT
	movq	bio_s_out(%rip), %r15
	movq	%r14, %rdi
	call	SSL_get_rbio@PLT
	movq	%rax, %rdi
	movq	%r15, %rsi
	call	BIO_set_callback_arg@PLT
	jmp	.L341
	.p2align 4,,10
	.p2align 3
.L511:
	movq	%rax, -208(%rbp)
	call	BIO_f_nbio_test@PLT
	movq	%rax, %rdi
	call	BIO_new@PLT
	movq	-208(%rbp), %rsi
	movq	%rax, %rdi
	call	BIO_push@PLT
	movq	%rax, %rsi
	jmp	.L340
	.p2align 4,,10
	.p2align 3
.L517:
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	jmp	.L334
	.p2align 4,,10
	.p2align 3
.L350:
	movq	bio_s_out(%rip), %rdi
	leaq	.LC55(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L346
.L384:
	leaq	.LC19(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_puts@PLT
.L375:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	ssl_print_sigalgs@PLT
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	ssl_print_groups@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	print_ca_names@PLT
	movq	%r14, %rdi
	call	SSL_session_reused@PLT
	leaq	.LC70(%rip), %rsi
	movq	%r12, %rdi
	testl	%eax, %eax
	leaq	.LC71(%rip), %rax
	cmove	%rax, %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	%r14, %rdi
	call	SSL_get_current_cipher@PLT
	movq	%rax, %r15
	movq	%rax, %rdi
	call	SSL_CIPHER_get_name@PLT
	movq	%r15, %rdi
	movq	%rax, %rbx
	call	SSL_CIPHER_get_version@PLT
	movq	%rbx, %rcx
	leaq	.LC90(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	%r14, %rdi
	call	SSL_get_session@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	SSL_SESSION_print@PLT
	leaq	.LC91(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	%r14, %rdi
	call	SSL_get_SSL_CTX@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	print_stats
	leaq	.LC91(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	%r14, %rdi
	call	SSL_get_peer_certificate@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L386
	leaq	.LC15(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	X509_print@PLT
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	PEM_write_bio_X509@PLT
	movq	%r15, %rdi
	call	X509_free@PLT
.L387:
	leaq	.LC93(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_puts@PLT
	jmp	.L418
	.p2align 4,,10
	.p2align 3
.L525:
	movl	$8, %esi
	movq	%r12, %rdi
	call	BIO_test_flags@PLT
	testl	%eax, %eax
	je	.L417
.L418:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	movq	%r12, %rdi
	call	BIO_ctrl@PLT
	testl	%eax, %eax
	jle	.L525
.L417:
	movl	$3, %esi
	movq	%r14, %rdi
	call	SSL_set_shutdown@PLT
	jmp	.L332
	.p2align 4,,10
	.p2align 3
.L367:
	leaq	.LC83(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_puts@PLT
	jmp	.L371
	.p2align 4,,10
	.p2align 3
.L522:
	leaq	.LC85(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_puts@PLT
	jmp	.L371
	.p2align 4,,10
	.p2align 3
.L376:
	cmpl	-212(%rbp), %r15d
	jne	.L379
	.p2align 4,,10
	.p2align 3
.L378:
	leaq	.LC19(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_puts@PLT
	movl	$16384, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	SSL_get_shared_ciphers@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L375
	leaq	.LC88(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	xorl	%r15d, %r15d
	call	BIO_printf@PLT
	movzbl	(%rbx), %eax
	movl	$0, -200(%rbp)
	testb	%al, %al
	jne	.L380
	jmp	.L384
	.p2align 4,,10
	.p2align 3
.L381:
	movl	$1, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	addl	$1, %r15d
	call	BIO_write@PLT
.L383:
	movzbl	1(%rbx), %eax
	addq	$1, %rbx
	testb	%al, %al
	je	.L384
.L380:
	cmpb	$58, %al
	jne	.L381
	movl	$26, %edx
	leaq	.LC89(%rip), %rsi
	movq	%r12, %rdi
	subl	%r15d, %edx
	call	BIO_write@PLT
	addl	$1, -200(%rbp)
	movl	$1, %edx
	movq	%r12, %rdi
	movl	-200(%rbp), %eax
	leaq	.LC19(%rip), %rsi
	imull	$-1431655765, %eax, %eax
	cmpl	$1431655765, %eax
	leaq	.LC69(%rip), %rax
	cmova	%rax, %rsi
	xorl	%r15d, %r15d
	call	BIO_write@PLT
	jmp	.L383
.L510:
	movq	%r14, %rdi
	call	SSL_free@PLT
	jmp	.L332
.L386:
	leaq	.LC92(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_puts@PLT
	jmp	.L387
.L524:
	movq	SSL_trace@GOTPCREL(%rip), %rsi
	movq	%r14, %rdi
	call	SSL_set_msg_callback@PLT
	jmp	.L344
.L393:
	testb	%sil, %sil
	je	.L389
	cmpb	$32, %sil
	je	.L395
	cmpb	$58, %sil
	je	.L428
	cmpb	$46, %sil
	je	.L396
	cmpb	$92, %sil
	sete	%al
	cmpb	$47, %sil
	sete	%dl
	addq	$2, %rcx
	orl	%edx, %eax
	movzbl	(%rcx), %edx
	jmp	.L394
.L396:
	movzbl	2(%rcx), %edx
	leaq	2(%rcx), %rax
	testb	%dl, %dl
	je	.L389
	cmpb	$32, %dl
	je	.L430
	cmpb	$58, %dl
	je	.L430
	cmpb	$47, %dl
	sete	%al
	cmpb	$92, %dl
	sete	%dl
	orb	%dl, %al
	jne	.L397
	movzbl	3(%rcx), %edx
	addq	$3, %rcx
	jmp	.L394
.L397:
	movzbl	3(%rcx), %eax
	leaq	3(%rcx), %rdx
	testb	%al, %al
	je	.L389
	cmpb	$58, %al
	je	.L431
	cmpb	$32, %al
	je	.L431
	movzbl	4(%rcx), %edx
	movl	$-1, %eax
	addq	$4, %rcx
	jmp	.L398
.L518:
	movl	s_quiet(%rip), %ecx
	testl	%ecx, %ecx
	jne	.L332
.L505:
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	jmp	.L332
.L390:
	movb	$0, (%rcx)
	cmpl	$-1, %eax
	je	.L420
.L421:
	movzbl	5(%r13), %eax
	cmpb	$47, %al
	je	.L434
	cmpb	$92, %al
	je	.L434
	movq	%r15, %rdi
	call	app_isdir@PLT
	testl	%eax, %eax
	jle	.L402
	movq	%r12, %rdi
	leaq	.LC95(%rip), %rsi
	call	BIO_puts@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC98(%rip), %rsi
	call	BIO_printf@PLT
	jmp	.L418
.L389:
	movq	%r12, %rdi
	leaq	.LC95(%rip), %rsi
	call	BIO_puts@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC107(%rip), %rsi
	call	BIO_printf@PLT
	jmp	.L418
.L434:
	movq	%r12, %rdi
	leaq	.LC95(%rip), %rsi
	call	BIO_puts@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC97(%rip), %rsi
	call	BIO_printf@PLT
	jmp	.L418
.L428:
	movq	%rdi, %rcx
.L391:
	movb	$0, (%rcx)
.L420:
	movq	%r12, %rdi
	leaq	.LC95(%rip), %rsi
	call	BIO_puts@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC96(%rip), %rsi
	call	BIO_printf@PLT
	jmp	.L418
.L361:
	movq	bio_s_out(%rip), %rdi
	leaq	.LC79(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	jmp	.L332
.L520:
	xorl	%edx, %edx
	movl	$5, %esi
	movq	%r14, %rdi
	call	SSL_set_verify@PLT
	jmp	.L358
.L402:
	leaq	.LC99(%rip), %rsi
	movq	%r15, %rdi
	call	BIO_new_file@PLT
	movq	%rax, -200(%rbp)
	testq	%rax, %rax
	je	.L526
	movl	s_quiet(%rip), %eax
	testl	%eax, %eax
	je	.L527
.L404:
	cmpl	$2, www(%rip)
	je	.L528
.L416:
	movq	-200(%rbp), %rdi
	movl	$16384, %edx
	movq	%r13, %rsi
	call	BIO_read@PLT
	movl	%eax, %ebx
	testl	%eax, %eax
	jle	.L410
	xorl	%r15d, %r15d
	jmp	.L415
	.p2align 4,,10
	.p2align 3
.L411:
	addl	%eax, %r15d
.L413:
	cmpl	%ebx, %r15d
	jge	.L416
.L415:
	movl	%ebx, %edx
	movslq	%r15d, %rsi
	movq	%r12, %rdi
	subl	%r15d, %edx
	addq	%r13, %rsi
	call	BIO_write@PLT
	testl	%eax, %eax
	jg	.L411
	movl	$8, %esi
	movq	%r12, %rdi
	call	BIO_test_flags@PLT
	testl	%eax, %eax
	jne	.L414
	movq	%r14, %rdi
	call	SSL_waiting_for_async@PLT
	testl	%eax, %eax
	je	.L410
.L414:
	movq	bio_s_out(%rip), %rdi
	leaq	.LC105(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L413
.L410:
	movq	-200(%rbp), %rdi
	call	BIO_free@PLT
	jmp	.L418
.L431:
	movq	%rdx, %rcx
	jmp	.L391
.L521:
	movq	%r14, %rdi
	call	SSL_get_error@PLT
	movq	bio_s_out(%rip), %rdi
	leaq	.LC78(%rip), %rsi
	movl	%eax, %edx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L505
.L430:
	movq	%rax, %rcx
	jmp	.L391
.L395:
	movb	$0, 1(%rcx)
	jmp	.L421
.L528:
	movq	%r15, %rdi
	call	strlen@PLT
	cmpl	$5, %eax
	jle	.L406
	movslq	%eax, %rbx
	leaq	.LC102(%rip), %rsi
	leaq	0(%r13,%rbx), %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L407
.L408:
	leaq	-4(%r15,%rbx), %r15
	leaq	.LC103(%rip), %rsi
	movq	%r15, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L407
	leaq	.LC104(%rip), %rsi
	movq	%r15, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L407
.L409:
	leaq	.LC95(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_puts@PLT
	jmp	.L416
.L527:
	movq	bio_err(%rip), %rdi
	movq	%r15, %rdx
	leaq	.LC101(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L404
.L508:
	call	__stack_chk_fail@PLT
.L526:
	movq	%r12, %rdi
	leaq	.LC95(%rip), %rsi
	call	BIO_puts@PLT
	movq	%r12, %rdi
	movq	%r15, %rdx
	xorl	%eax, %eax
	leaq	.LC100(%rip), %rsi
	call	BIO_printf@PLT
	movq	%r12, %rdi
	call	ERR_print_errors@PLT
	jmp	.L418
.L407:
	leaq	.LC80(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_puts@PLT
	jmp	.L416
.L406:
	jne	.L409
	movl	$5, %ebx
	jmp	.L408
	.cfi_endproc
.LFE1649:
	.size	www_body, .-www_body
	.section	.rodata.str1.1
.LC108:
	.string	"server rev buffer"
.LC109:
	.string	"CONNECTION FAILURE\n"
.LC110:
	.string	"LOOKUP renego during accept\n"
.LC111:
	.string	"CONNECTION ESTABLISHED\n"
.LC112:
	.string	"CLOSE"
	.text
	.p2align 4
	.type	rev_body, @function
rev_body:
.LFB1650:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC108(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edi, -52(%rbp)
	movl	$16384, %edi
	call	app_malloc@PLT
	movq	%rax, %r13
	call	BIO_f_buffer@PLT
	movq	%rax, %rdi
	call	BIO_new@PLT
	movq	%rax, %r12
	call	BIO_f_ssl@PLT
	movq	%rax, %rdi
	call	BIO_new@PLT
	testq	%r12, %r12
	je	.L532
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L532
	movl	$1, %ecx
	movl	$16384, %edx
	movl	$117, %esi
	movq	%r12, %rdi
	call	BIO_int_ctrl@PLT
	testq	%rax, %rax
	je	.L532
	movq	ctx(%rip), %rdi
	call	SSL_new@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L532
	movl	s_tlsextdebug(%rip), %edi
	testl	%edi, %edi
	jne	.L588
.L534:
	testq	%r15, %r15
	je	.L535
	movq	%r15, %rdi
	call	strlen@PLT
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	%rax, %rdx
	call	SSL_set_session_id_context@PLT
	testl	%eax, %eax
	je	.L589
.L535:
	movl	-52(%rbp), %edi
	xorl	%esi, %esi
	call	BIO_new_socket@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	movq	%rax, %rdx
	call	SSL_set_bio@PLT
	movq	%rbx, %rdi
	call	SSL_set_accept_state@PLT
	movq	%rbx, %rcx
	movl	$1, %edx
	movq	%r14, %rdi
	movl	$109, %esi
	call	BIO_ctrl@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	BIO_push@PLT
	movl	s_debug(%rip), %esi
	testl	%esi, %esi
	jne	.L590
.L536:
	movl	s_msg(%rip), %eax
	testl	%eax, %eax
	je	.L537
	cmpl	$2, %eax
	je	.L591
	movq	msg_cb@GOTPCREL(%rip), %rsi
	movq	%rbx, %rdi
	call	SSL_set_msg_callback@PLT
.L539:
	movq	bio_s_msg(%rip), %rcx
	movl	$16, %esi
	movq	%rbx, %rdi
	testq	%rcx, %rcx
	cmove	bio_s_out(%rip), %rcx
	xorl	%edx, %edx
	call	SSL_ctrl@PLT
.L537:
	leaq	.LC110(%rip), %r14
	jmp	.L545
	.p2align 4,,10
	.p2align 3
.L594:
	movl	$8, %esi
	movq	%r12, %rdi
	call	BIO_test_flags@PLT
	testl	%eax, %eax
	je	.L592
	movl	$4, %esi
	movq	%r12, %rdi
	call	BIO_test_flags@PLT
	testl	%eax, %eax
	jne	.L593
.L545:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$101, %esi
	movq	%r12, %rdi
	call	BIO_ctrl@PLT
	testl	%eax, %eax
	jle	.L594
	movq	bio_err(%rip), %rdi
	leaq	.LC111(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	%rbx, %rdi
	call	print_ssl_summary@PLT
.L549:
	movl	$16383, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	BIO_gets@PLT
	testl	%eax, %eax
	jns	.L550
	movl	$8, %esi
	movq	%r12, %rdi
	call	BIO_test_flags@PLT
	testl	%eax, %eax
	je	.L595
	movq	bio_s_out(%rip), %rdi
	leaq	.LC73(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movl	$4, %esi
	movq	%r12, %rdi
	call	BIO_test_flags@PLT
	testl	%eax, %eax
	je	.L552
	movq	%r12, %rdi
	call	BIO_get_retry_reason@PLT
	cmpl	$1, %eax
	je	.L596
.L552:
	movl	$1, %edi
	call	sleep@PLT
	jmp	.L549
	.p2align 4,,10
	.p2align 3
.L592:
	movq	bio_err(%rip), %rdi
	leaq	.LC109(%rip), %rsi
	call	BIO_puts@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
.L544:
	movl	$3, %esi
	movq	%rbx, %rdi
	call	SSL_set_shutdown@PLT
.L532:
	movq	%r13, %rdi
	movl	$3488, %edx
	leaq	.LC31(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%r12, %rdi
	call	BIO_free_all@PLT
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L550:
	.cfi_restore_state
	je	.L561
	movslq	%eax, %rdx
	leaq	-1(%r13,%rdx), %rdx
	jmp	.L556
	.p2align 4,,10
	.p2align 3
.L558:
	subq	$1, %rdx
	subl	$1, %eax
	je	.L563
.L556:
	movzbl	(%rdx), %ecx
	cmpb	$10, %cl
	je	.L558
	cmpb	$13, %cl
	je	.L558
	movl	s_ign_eof(%rip), %edx
	testl	%edx, %edx
	jne	.L587
	cmpl	$5, %eax
	jne	.L587
	movl	$5, %ecx
	movq	%r13, %rsi
	leaq	.LC112(%rip), %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L561
	leaq	5(%r13), %r14
	movl	$6, %r15d
	movl	$5, %edx
	jmp	.L557
	.p2align 4,,10
	.p2align 3
.L593:
	movq	%r12, %rdi
	call	BIO_get_retry_reason@PLT
	cmpl	$1, %eax
	jne	.L545
	movq	bio_s_out(%rip), %rdi
	movq	%r14, %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	16+srp_callback_parm(%rip), %rdi
	call	SRP_user_pwd_free@PLT
	movq	srp_callback_parm(%rip), %rsi
	movq	8+srp_callback_parm(%rip), %rdi
	call	SRP_VBASE_get1_by_user@PLT
	movq	%rax, 16+srp_callback_parm(%rip)
	testq	%rax, %rax
	je	.L547
	movq	40(%rax), %rdx
	movq	bio_s_out(%rip), %rdi
	leaq	.LC54(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L545
	.p2align 4,,10
	.p2align 3
.L563:
	movq	%r13, %r14
	movl	$1, %r15d
	xorl	%edx, %edx
.L557:
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	BUF_reverse@PLT
	movb	$10, (%r14)
	movl	%r15d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	BIO_write@PLT
	jmp	.L562
	.p2align 4,,10
	.p2align 3
.L597:
	movl	$8, %esi
	movq	%r12, %rdi
	call	BIO_test_flags@PLT
	testl	%eax, %eax
	je	.L544
.L562:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	movq	%r12, %rdi
	call	BIO_ctrl@PLT
	testl	%eax, %eax
	jle	.L597
	jmp	.L549
	.p2align 4,,10
	.p2align 3
.L588:
	movq	tlsext_cb@GOTPCREL(%rip), %rdx
	movl	$56, %esi
	movq	%rax, %rdi
	call	SSL_callback_ctrl@PLT
	xorl	%edx, %edx
	movl	$57, %esi
	movq	%rbx, %rdi
	movq	bio_s_out(%rip), %rcx
	call	SSL_ctrl@PLT
	jmp	.L534
	.p2align 4,,10
	.p2align 3
.L590:
	movq	%rbx, %rdi
	call	SSL_get_rbio@PLT
	movq	bio_dump_callback@GOTPCREL(%rip), %rsi
	movq	%rax, %rdi
	call	BIO_set_callback@PLT
	movq	bio_s_out(%rip), %r14
	movq	%rbx, %rdi
	call	SSL_get_rbio@PLT
	movq	%rax, %rdi
	movq	%r14, %rsi
	call	BIO_set_callback_arg@PLT
	jmp	.L536
	.p2align 4,,10
	.p2align 3
.L587:
	movslq	%eax, %rdx
	leal	1(%rax), %r15d
	leaq	0(%r13,%rdx), %r14
	jmp	.L557
	.p2align 4,,10
	.p2align 3
.L591:
	movq	SSL_trace@GOTPCREL(%rip), %rsi
	movq	%rbx, %rdi
	call	SSL_set_msg_callback@PLT
	jmp	.L539
	.p2align 4,,10
	.p2align 3
.L589:
	movq	%rbx, %rdi
	call	SSL_free@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	jmp	.L532
.L596:
	movq	bio_s_out(%rip), %rdi
	leaq	.LC64(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	16+srp_callback_parm(%rip), %rdi
	call	SRP_user_pwd_free@PLT
	movq	srp_callback_parm(%rip), %rsi
	movq	8+srp_callback_parm(%rip), %rdi
	call	SRP_VBASE_get1_by_user@PLT
	movq	%rax, 16+srp_callback_parm(%rip)
	testq	%rax, %rax
	je	.L553
	movq	40(%rax), %rdx
	movq	bio_s_out(%rip), %rdi
	leaq	.LC54(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L549
.L553:
	movq	bio_s_out(%rip), %rdi
	leaq	.LC55(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L549
.L561:
	movq	bio_err(%rip), %rdi
	leaq	.LC67(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L544
.L595:
	movl	s_quiet(%rip), %ecx
	testl	%ecx, %ecx
	jne	.L532
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	jmp	.L532
.L547:
	movq	bio_s_out(%rip), %rdi
	leaq	.LC55(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L545
	.cfi_endproc
.LFE1650:
	.size	rev_body, .-rev_body
	.section	.rodata.str1.1
.LC113:
	.string	"cert_status: callback called\n"
	.section	.rodata.str1.8
	.align 8
.LC114:
	.string	"cert_status: Cannot open OCSP response file\n"
	.align 8
.LC115:
	.string	"cert_status: Error reading OCSP response\n"
	.align 8
.LC116:
	.string	"cert_status: can't parse AIA URL\n"
	.section	.rodata.str1.1
.LC117:
	.string	"cert_status: AIA URL: %s\n"
	.section	.rodata.str1.8
	.align 8
.LC118:
	.string	"cert_status: no AIA and no default responder URL\n"
	.align 8
.LC119:
	.string	"cert_status: Can't retrieve issuer certificate.\n"
	.align 8
.LC120:
	.string	"cert_status: error querying responder\n"
	.align 8
.LC121:
	.string	"cert_status: ocsp response sent:\n"
	.text
	.p2align 4
	.type	cert_status_cb, @function
cert_status_cb:
.LFB1637:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$104, %rsp
	movl	44(%rsi), %esi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	testl	%esi, %esi
	jne	.L644
	movq	8(%rbx), %r12
	testq	%r12, %r12
	je	.L600
.L650:
	movl	$4, %edx
	movl	$114, %esi
	movq	%r12, %rdi
	call	bio_open_default@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L645
	movq	OCSP_RESPONSE_new@GOTPCREL(%rip), %rdi
	movq	d2i_OCSP_RESPONSE@GOTPCREL(%rip), %rsi
	xorl	%ecx, %ecx
	movq	%rax, %rdx
	call	ASN1_d2i_bio@PLT
	movq	%r14, %rdi
	movq	%rax, %r12
	call	BIO_free@PLT
	testq	%r12, %r12
	je	.L646
	leaq	-96(%rbp), %rsi
	movq	%r12, %rdi
	call	i2d_OCSP_RESPONSE@PLT
	testl	%eax, %eax
	jle	.L647
.L620:
	movq	-96(%rbp), %rcx
	movslq	%eax, %rdx
	movl	$71, %esi
	movq	%r13, %rdi
	call	SSL_ctrl@PLT
	movl	44(%rbx), %r15d
	testl	%r15d, %r15d
	jne	.L648
.L619:
	movq	%r12, %rdi
	call	OCSP_RESPONSE_free@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L649
	leaq	-40(%rbp), %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L644:
	.cfi_restore_state
	movq	bio_err(%rip), %rdi
	leaq	.LC113(%rip), %rsi
	call	BIO_puts@PLT
	movq	8(%rbx), %r12
	testq	%r12, %r12
	jne	.L650
.L600:
	movq	%r13, %rdi
	movq	$0, -88(%rbp)
	movq	$0, -80(%rbp)
	movq	$0, -72(%rbp)
	call	SSL_get_certificate@PLT
	movq	%rax, %rdi
	movq	%rax, %r15
	call	X509_get1_ocsp@PLT
	movq	%rax, -128(%rbp)
	testq	%rax, %rax
	je	.L604
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	OPENSSL_sk_value@PLT
	leaq	-72(%rbp), %rcx
	leaq	-80(%rbp), %rdx
	movq	%rax, %rdi
	leaq	-88(%rbp), %rsi
	leaq	-100(%rbp), %r8
	call	OCSP_parse_url@PLT
	testl	%eax, %eax
	je	.L651
	movl	44(%rbx), %ecx
	testl	%ecx, %ecx
	jne	.L652
.L607:
	call	X509_STORE_CTX_new@PLT
	movq	%rax, -120(%rbp)
	testq	%rax, %rax
	je	.L653
.L621:
	movq	%r13, %rdi
	call	SSL_get_SSL_CTX@PLT
	movq	%rax, %rdi
	call	SSL_CTX_get_cert_store@PLT
	movq	-120(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	call	X509_STORE_CTX_init@PLT
	testl	%eax, %eax
	je	.L613
	movq	%r15, %rdi
	call	X509_get_issuer_name@PLT
	movq	-120(%rbp), %rdi
	movl	$1, %esi
	movq	%rax, %rdx
	call	X509_STORE_CTX_get_obj_by_subject@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L654
	movq	%rax, %rdi
	call	X509_OBJECT_get0_X509@PLT
	movq	%r15, %rsi
	xorl	%edi, %edi
	movq	%rax, %rdx
	call	OCSP_cert_to_id@PLT
	movq	%r14, %rdi
	movq	%rax, -136(%rbp)
	call	X509_OBJECT_free@PLT
	movq	-136(%rbp), %r8
	testq	%r8, %r8
	je	.L613
	call	OCSP_REQUEST_new@PLT
	movq	-136(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %r14
	je	.L643
	movq	%r8, %rsi
	movq	%rax, %rdi
	call	OCSP_request_add0_id@PLT
	movq	-136(%rbp), %r8
	testq	%rax, %rax
	je	.L643
	leaq	-64(%rbp), %rcx
	xorl	%edx, %edx
	movl	$66, %esi
	movq	%r13, %rdi
	call	SSL_ctrl@PLT
	xorl	%r15d, %r15d
	jmp	.L614
	.p2align 4,,10
	.p2align 3
.L615:
	movq	-64(%rbp), %rdi
	movl	%r15d, %esi
	call	OPENSSL_sk_value@PLT
	movl	$-1, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	OCSP_REQUEST_add_ext@PLT
	testl	%eax, %eax
	je	.L628
	addl	$1, %r15d
.L614:
	movq	-64(%rbp), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r15d
	jl	.L615
	movl	(%rbx), %eax
	subq	$8, %rsp
	movl	-100(%rbp), %r8d
	xorl	%r9d, %r9d
	movq	-72(%rbp), %rdx
	movq	-80(%rbp), %rcx
	movq	%r14, %rdi
	pushq	%rax
	movq	-88(%rbp), %rsi
	call	process_responder@PLT
	movq	%rax, %r8
	popq	%rax
	popq	%rdx
	testq	%r8, %r8
	je	.L655
	cmpq	$0, -128(%rbp)
	jne	.L656
	xorl	%edi, %edi
	movq	%r8, -128(%rbp)
	call	OCSP_CERTID_free@PLT
	movq	%r14, %rdi
	call	OCSP_REQUEST_free@PLT
	movq	-120(%rbp), %rdi
	call	X509_STORE_CTX_free@PLT
	movq	-128(%rbp), %r8
	movq	%r8, %r12
	jmp	.L618
	.p2align 4,,10
	.p2align 3
.L604:
	movq	16(%rbx), %rax
	testq	%rax, %rax
	je	.L657
	movq	%rax, -88(%rbp)
	movq	24(%rbx), %rax
	movq	%rax, -72(%rbp)
	movq	32(%rbx), %rax
	movq	%rax, -80(%rbp)
	movl	40(%rbx), %eax
	movl	%eax, -100(%rbp)
	call	X509_STORE_CTX_new@PLT
	movq	%rax, -120(%rbp)
	testq	%rax, %rax
	jne	.L621
	xorl	%edi, %edi
	movl	$2, %r15d
	call	OCSP_CERTID_free@PLT
	xorl	%edi, %edi
	call	OCSP_REQUEST_free@PLT
	xorl	%edi, %edi
	call	X509_STORE_CTX_free@PLT
	jmp	.L602
	.p2align 4,,10
	.p2align 3
.L647:
	movl	$2, %r15d
.L602:
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	jmp	.L619
	.p2align 4,,10
	.p2align 3
.L648:
	movq	bio_err(%rip), %rdi
	leaq	.LC121(%rip), %rsi
	xorl	%r15d, %r15d
	call	BIO_puts@PLT
	movq	bio_err(%rip), %rdi
	movl	$2, %edx
	movq	%r12, %rsi
	call	OCSP_RESPONSE_print@PLT
	jmp	.L619
	.p2align 4,,10
	.p2align 3
.L645:
	movq	bio_err(%rip), %rdi
	leaq	.LC114(%rip), %rsi
	xorl	%r12d, %r12d
	movl	$2, %r15d
	call	BIO_puts@PLT
	jmp	.L602
	.p2align 4,,10
	.p2align 3
.L613:
	xorl	%r8d, %r8d
	xorl	%r14d, %r14d
	movl	$2, %r15d
.L611:
	cmpq	$0, -128(%rbp)
	je	.L658
.L606:
	movq	-88(%rbp), %rdi
	movl	$595, %edx
	leaq	.LC31(%rip), %rsi
	movq	%r8, -136(%rbp)
	call	CRYPTO_free@PLT
	movq	-72(%rbp), %rdi
	movl	$596, %edx
	leaq	.LC31(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-80(%rbp), %rdi
	movl	$597, %edx
	leaq	.LC31(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-128(%rbp), %rdi
	call	X509_email_free@PLT
	movq	-136(%rbp), %r8
	movq	%r8, %rdi
	call	OCSP_CERTID_free@PLT
	movq	%r14, %rdi
	call	OCSP_REQUEST_free@PLT
	movq	-120(%rbp), %rdi
	call	X509_STORE_CTX_free@PLT
	testl	%r15d, %r15d
	jne	.L602
.L618:
	leaq	-96(%rbp), %rsi
	movq	%r12, %rdi
	call	i2d_OCSP_RESPONSE@PLT
	testl	%eax, %eax
	jg	.L620
	xorl	%r15d, %r15d
	jmp	.L619
	.p2align 4,,10
	.p2align 3
.L658:
	movq	%r8, %rdi
	xorl	%r12d, %r12d
	call	OCSP_CERTID_free@PLT
	movq	%r14, %rdi
	call	OCSP_REQUEST_free@PLT
	movq	-120(%rbp), %rdi
	call	X509_STORE_CTX_free@PLT
	jmp	.L602
	.p2align 4,,10
	.p2align 3
.L628:
	xorl	%r8d, %r8d
.L643:
	movl	$2, %r15d
	jmp	.L611
	.p2align 4,,10
	.p2align 3
.L646:
	movq	bio_err(%rip), %rdi
	leaq	.LC115(%rip), %rsi
	movl	$2, %r15d
	call	BIO_puts@PLT
	jmp	.L602
	.p2align 4,,10
	.p2align 3
.L652:
	movq	-128(%rbp), %rdi
	xorl	%esi, %esi
	call	OPENSSL_sk_value@PLT
	movq	bio_err(%rip), %rdi
	leaq	.LC117(%rip), %rsi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L607
	.p2align 4,,10
	.p2align 3
.L653:
	xorl	%r12d, %r12d
	xorl	%r14d, %r14d
	xorl	%r8d, %r8d
	movl	$2, %r15d
	jmp	.L606
	.p2align 4,,10
	.p2align 3
.L651:
	movq	bio_err(%rip), %rdi
	leaq	.LC116(%rip), %rsi
	xorl	%r14d, %r14d
	movl	$2, %r15d
	call	BIO_puts@PLT
	movq	$0, -120(%rbp)
	xorl	%r8d, %r8d
	jmp	.L606
	.p2align 4,,10
	.p2align 3
.L654:
	movq	bio_err(%rip), %rdi
	leaq	.LC119(%rip), %rsi
	movl	$3, %r15d
	call	BIO_puts@PLT
	xorl	%r8d, %r8d
	jmp	.L611
.L657:
	movq	bio_err(%rip), %rdi
	leaq	.LC118(%rip), %rsi
	movl	$3, %r15d
	call	BIO_puts@PLT
	xorl	%edi, %edi
	call	OCSP_CERTID_free@PLT
	xorl	%edi, %edi
	call	OCSP_REQUEST_free@PLT
	xorl	%edi, %edi
	call	X509_STORE_CTX_free@PLT
	jmp	.L602
.L655:
	movq	bio_err(%rip), %rdi
	leaq	.LC120(%rip), %rsi
	movq	%r8, -136(%rbp)
	movl	$3, %r15d
	call	BIO_puts@PLT
	movq	-136(%rbp), %r8
	jmp	.L611
.L649:
	call	__stack_chk_fail@PLT
.L656:
	movq	%r8, %r12
	xorl	%r15d, %r15d
	xorl	%r8d, %r8d
	jmp	.L606
	.cfi_endproc
.LFE1637:
	.size	cert_status_cb, .-cert_status_cb
	.section	.rodata.str1.1
.LC122:
	.string	"SRP username = \"%s\"\n"
.LC123:
	.string	"User %s doesn't exist\n"
	.section	.rodata.str1.8
	.align 8
.LC124:
	.string	"SRP parameters set: username = \"%s\" info=\"%s\" \n"
	.text
	.p2align 4
	.type	ssl_srp_server_param_cb, @function
ssl_srp_server_param_cb:
.LFB1634:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdx, %rbx
	movq	(%rdx), %rdx
	movq	16(%rbx), %rax
	testq	%rdx, %rdx
	je	.L666
	testq	%rax, %rax
	je	.L667
.L661:
	movq	8(%rax), %rcx
	movq	24(%rax), %rdx
	movq	32(%rax), %rsi
	movq	40(%rax), %r9
	movq	16(%rax), %r8
	call	SSL_set_srp_server_param@PLT
	testl	%eax, %eax
	js	.L668
	movq	16(%rbx), %rax
	movq	(%rbx), %rdx
	xorl	%r12d, %r12d
	leaq	.LC124(%rip), %rsi
	movq	bio_err(%rip), %rdi
	movq	40(%rax), %rcx
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L663:
	movq	16(%rbx), %rdi
	call	SRP_user_pwd_free@PLT
	movq	$0, 16(%rbx)
	movq	$0, (%rbx)
.L659:
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L668:
	.cfi_restore_state
	movl	$80, (%r12)
	movl	$2, %r12d
	jmp	.L663
	.p2align 4,,10
	.p2align 3
.L666:
	testq	%rax, %rax
	jne	.L661
	call	SSL_get_srp_username@PLT
	movq	bio_err(%rip), %rdi
	leaq	.LC122(%rip), %rsi
	orl	$-1, %r12d
	movq	%rax, (%rbx)
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L659
	.p2align 4,,10
	.p2align 3
.L667:
	movq	bio_err(%rip), %rdi
	leaq	.LC123(%rip), %rsi
	movl	$2, %r12d
	call	BIO_printf@PLT
	jmp	.L663
	.cfi_endproc
.LFE1634:
	.size	ssl_srp_server_param_cb, .-ssl_srp_server_param_cb
	.section	.rodata.str1.1
.LC125:
	.string	"\\x%02x"
.LC126:
	.string	"%c"
.LC127:
	.string	"Hostname in TLS extension: \""
.LC128:
	.string	"\"\n"
.LC129:
	.string	"Switching server context.\n"
	.text
	.p2align 4
	.type	ssl_servername_cb, @function
ssl_servername_cb:
.LFB1635:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	%rdi, -64(%rbp)
	call	SSL_get_servername@PLT
	testq	%rax, %rax
	je	.L670
	movq	8(%r12), %rdi
	movq	%rax, %r13
	testq	%rdi, %rdi
	je	.L671
	leaq	.LC127(%rip), %rsi
	xorl	%eax, %eax
	leaq	1(%r13), %rbx
	call	BIO_printf@PLT
	movzbl	0(%r13), %edx
	testb	%dl, %dl
	je	.L672
	leaq	.LC125(%rip), %r14
	.p2align 4,,10
	.p2align 3
.L674:
	testb	$-128, %dl
	movb	%dl, -49(%rbp)
	movzbl	%dl, %r15d
	movq	%r14, %rsi
	jne	.L673
	call	__ctype_b_loc@PLT
	movzbl	-49(%rbp), %edx
	leaq	.LC126(%rip), %rsi
	movq	(%rax), %rax
	testb	$64, 1(%rax,%rdx,2)
	cmove	%r14, %rsi
.L673:
	movq	8(%r12), %rdi
	movl	%r15d, %edx
	xorl	%eax, %eax
	addq	$1, %rbx
	call	BIO_printf@PLT
	movzbl	-1(%rbx), %edx
	testb	%dl, %dl
	jne	.L674
.L672:
	movq	8(%r12), %rdi
	leaq	.LC128(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L671:
	movq	(%r12), %rsi
	testq	%rsi, %rsi
	je	.L678
	movq	%r13, %rdi
	call	strcasecmp@PLT
	movl	%eax, %r13d
	testl	%eax, %eax
	jne	.L694
	cmpq	$0, ctx2(%rip)
	je	.L675
	movq	8(%r12), %rdi
	leaq	.LC129(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	ctx2(%rip), %rsi
	movq	-64(%rbp), %rdi
	call	SSL_set_SSL_CTX@PLT
.L669:
	addq	$24, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L694:
	.cfi_restore_state
	movl	16(%r12), %r13d
	jmp	.L669
	.p2align 4,,10
	.p2align 3
.L670:
	cmpq	$0, (%r12)
	je	.L678
.L675:
	xorl	%r13d, %r13d
	jmp	.L669
	.p2align 4,,10
	.p2align 3
.L678:
	movl	$3, %r13d
	jmp	.L669
	.cfi_endproc
.LFE1635:
	.size	ssl_servername_cb, .-ssl_servername_cb
	.section	.rodata.str1.1
.LC130:
	.string	"psk_server_cb\n"
	.section	.rodata.str1.8
	.align 8
.LC131:
	.string	"Error: client did not send PSK identity\n"
	.section	.rodata.str1.1
.LC132:
	.string	"identity_len=%d identity=%s\n"
	.section	.rodata.str1.8
	.align 8
.LC133:
	.string	"PSK warning: client identity not what we expected (got '%s' expected '%s')\n"
	.section	.rodata.str1.1
.LC134:
	.string	"PSK client identity found\n"
	.section	.rodata.str1.8
	.align 8
.LC135:
	.string	"Could not convert PSK key '%s' to buffer\n"
	.align 8
.LC136:
	.string	"psk buffer of callback is too small (%d) for key (%ld)\n"
	.section	.rodata.str1.1
.LC137:
	.string	"fetched PSK len=%ld\n"
.LC138:
	.string	"Error in PSK server callback\n"
	.text
	.p2align 4
	.type	psk_server_cb, @function
psk_server_cb:
.LFB1632:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%ecx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	subq	$16, %rsp
	movl	s_debug(%rip), %edi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -48(%rbp)
	testl	%edi, %edi
	jne	.L716
	testq	%rsi, %rsi
	je	.L710
.L700:
	movq	psk_identity(%rip), %r15
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L708
.L702:
	movq	psk_key(%rip), %rdi
	leaq	-48(%rbp), %rsi
	call	OPENSSL_hexstr2buf@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L717
	movq	-48(%rbp), %rdx
	movslq	%r13d, %rax
	cmpq	%rdx, %rax
	jl	.L718
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	memcpy@PLT
	movl	$169, %edx
	leaq	.LC31(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
	movl	s_debug(%rip), %eax
	testl	%eax, %eax
	jne	.L719
.L707:
	movl	-48(%rbp), %eax
.L695:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L720
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L716:
	.cfi_restore_state
	movq	bio_s_out(%rip), %rdi
	leaq	.LC130(%rip), %rsi
	call	BIO_printf@PLT
	testq	%r12, %r12
	je	.L710
	movl	s_debug(%rip), %ecx
	testl	%ecx, %ecx
	je	.L700
	movq	%r12, %rdi
	call	strlen@PLT
	movq	bio_s_out(%rip), %rdi
	leaq	.LC132(%rip), %rsi
	movq	%r12, %rcx
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	psk_identity(%rip), %r15
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L701
.L708:
	movq	bio_s_out(%rip), %rdi
	movq	%r15, %rcx
	movq	%r12, %rdx
	xorl	%eax, %eax
	leaq	.LC133(%rip), %rsi
	call	BIO_printf@PLT
	jmp	.L702
	.p2align 4,,10
	.p2align 3
.L718:
	movq	bio_err(%rip), %rdi
	movq	%rdx, %rcx
	leaq	.LC136(%rip), %rsi
	movl	%r13d, %edx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movl	$164, %edx
	leaq	.LC31(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
	xorl	%eax, %eax
	jmp	.L695
	.p2align 4,,10
	.p2align 3
.L701:
	movl	s_debug(%rip), %edx
	testl	%edx, %edx
	je	.L702
	movq	bio_s_out(%rip), %rdi
	leaq	.LC134(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L702
	.p2align 4,,10
	.p2align 3
.L719:
	movq	-48(%rbp), %rdx
	movq	bio_s_out(%rip), %rdi
	leaq	.LC137(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L707
	.p2align 4,,10
	.p2align 3
.L710:
	movq	bio_err(%rip), %rdi
	leaq	.LC131(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movl	s_debug(%rip), %esi
	testl	%esi, %esi
	jne	.L721
.L699:
	movq	bio_err(%rip), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	call	BIO_ctrl@PLT
	movq	bio_s_out(%rip), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	call	BIO_ctrl@PLT
	xorl	%eax, %eax
	jmp	.L695
	.p2align 4,,10
	.p2align 3
.L717:
	movq	psk_key(%rip), %rdx
	movq	bio_err(%rip), %rdi
	leaq	.LC135(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	xorl	%eax, %eax
	jmp	.L695
	.p2align 4,,10
	.p2align 3
.L721:
	movq	bio_err(%rip), %rdi
	leaq	.LC138(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L699
.L720:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1632:
	.size	psk_server_cb, .-psk_server_cb
	.section	.rodata.str1.8
	.align 8
.LC139:
	.string	"ALPN protocols advertised by the client: "
	.section	.rodata.str1.1
.LC140:
	.string	", "
.LC141:
	.string	"ALPN protocols selected: "
	.text
	.p2align 4
	.type	alpn_cb, @function
alpn_cb:
.LFB1639:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%r9, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%r8d, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$24, %rsp
	movl	s_quiet(%rip), %eax
	movq	%rsi, -56(%rbp)
	movq	%rdx, -64(%rbp)
	testl	%eax, %eax
	je	.L737
.L723:
	movl	8(%r13), %ecx
	movq	0(%r13), %rdx
	movl	%r12d, %r9d
	movq	%rbx, %r8
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %rdi
	movl	$3, %r12d
	call	SSL_select_next_proto@PLT
	cmpl	$1, %eax
	jne	.L722
	movl	s_quiet(%rip), %r12d
	testl	%r12d, %r12d
	je	.L738
	xorl	%r12d, %r12d
.L722:
	addq	$24, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L738:
	.cfi_restore_state
	movq	bio_s_out(%rip), %rdi
	leaq	.LC141(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	-64(%rbp), %rax
	movq	bio_s_out(%rip), %rdi
	movzbl	(%rax), %edx
	movq	-56(%rbp), %rax
	movq	(%rax), %rsi
	call	BIO_write@PLT
	movq	bio_s_out(%rip), %rdi
	movl	$1, %edx
	leaq	.LC19(%rip), %rsi
	call	BIO_write@PLT
	jmp	.L722
	.p2align 4,,10
	.p2align 3
.L737:
	movq	bio_s_out(%rip), %rdi
	leaq	.LC139(%rip), %rsi
	xorl	%eax, %eax
	xorl	%r14d, %r14d
	call	BIO_printf@PLT
	movq	bio_s_out(%rip), %rdi
	testl	%r12d, %r12d
	je	.L728
	.p2align 4,,10
	.p2align 3
.L725:
	movl	%r14d, %r8d
	leal	1(%r14), %esi
	leaq	(%rbx,%r8), %r15
	addq	%rbx, %rsi
	movzbl	(%r15), %edx
	call	BIO_write@PLT
	movzbl	(%r15), %eax
	leal	1(%r14,%rax), %r14d
	cmpl	%r14d, %r12d
	jbe	.L728
	movq	bio_s_out(%rip), %rdi
	testl	%r14d, %r14d
	je	.L725
	movl	$2, %edx
	leaq	.LC140(%rip), %rsi
	call	BIO_write@PLT
	movq	bio_s_out(%rip), %rdi
	jmp	.L725
	.p2align 4,,10
	.p2align 3
.L728:
	movq	bio_s_out(%rip), %rdi
	movl	$1, %edx
	leaq	.LC19(%rip), %rsi
	call	BIO_write@PLT
	jmp	.L723
	.cfi_endproc
.LFE1639:
	.size	alpn_cb, .-alpn_cb
	.p2align 4
	.type	generate_session_id, @function
generate_session_id:
.LFB1651:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	$10, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$8, %rsp
	.p2align 4,,10
	.p2align 3
.L742:
	movl	(%rbx), %esi
	movq	%r12, %rdi
	call	RAND_bytes@PLT
	testl	%eax, %eax
	jle	.L740
	movq	session_id_prefix(%rip), %r15
	movq	%r15, %rdi
	call	strlen@PLT
	movl	(%rbx), %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	cmpq	%rax, %rdx
	cmova	%rax, %rdx
	call	memcpy@PLT
	movl	(%rbx), %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	SSL_has_matching_session_id@PLT
	testl	%eax, %eax
	je	.L743
	subl	$1, %r13d
	jne	.L742
.L740:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L743:
	.cfi_restore_state
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1651:
	.size	generate_session_id, .-generate_session_id
	.p2align 4
	.type	init_session_cache_ctx, @function
init_session_cache_ctx:
.LFB1655:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%ecx, %ecx
	movl	$770, %edx
	movl	$44, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	SSL_CTX_ctrl@PLT
	movq	%r12, %rdi
	leaq	add_session(%rip), %rsi
	call	SSL_CTX_sess_set_new_cb@PLT
	movq	%r12, %rdi
	leaq	get_session(%rip), %rsi
	call	SSL_CTX_sess_set_get_cb@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	leaq	del_session(%rip), %rsi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	SSL_CTX_sess_set_remove_cb@PLT
	.cfi_endproc
.LFE1655:
	.size	init_session_cache_ctx, .-init_session_cache_ctx
	.p2align 4
	.type	del_session, @function
del_session:
.LFB1654:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	leaq	-60(%rbp), %rsi
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	SSL_SESSION_get_id@PLT
	movq	first(%rip), %r12
	testq	%r12, %r12
	je	.L748
	movq	%rax, %r13
	movl	-60(%rbp), %eax
	xorl	%r15d, %r15d
	movq	%rax, -72(%rbp)
	movq	%rax, %rbx
	jmp	.L754
	.p2align 4,,10
	.p2align 3
.L750:
	movq	32(%r12), %rax
	movq	%r12, %r15
	testq	%rax, %rax
	je	.L748
	movq	%rax, %r12
.L754:
	cmpl	%ebx, 8(%r12)
	jne	.L750
	movq	(%r12), %r14
	movq	-72(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L750
	movq	32(%r12), %rax
	testq	%r15, %r15
	je	.L751
	movq	%rax, 32(%r15)
.L752:
	movl	$3603, %edx
	leaq	.LC31(%rip), %rsi
	movq	%r14, %rdi
	call	CRYPTO_free@PLT
	movq	16(%r12), %rdi
	movl	$3604, %edx
	leaq	.LC31(%rip), %rsi
	call	CRYPTO_free@PLT
	movl	$3605, %edx
	leaq	.LC31(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
.L748:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L761
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L751:
	.cfi_restore_state
	movq	%rax, first(%rip)
	jmp	.L752
.L761:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1654:
	.size	del_session, .-del_session
	.section	.rodata.str1.1
.LC142:
	.string	"Lookup session: cache hit\n"
.LC143:
	.string	"Lookup session: cache miss\n"
	.text
	.p2align 4
	.type	get_session, @function
get_session:
.LFB1653:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	first(%rip), %rbx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	$0, (%rcx)
	testq	%rbx, %rbx
	je	.L763
	movq	%rsi, %r13
	movl	%edx, %r12d
	movslq	%edx, %r14
	jmp	.L766
	.p2align 4,,10
	.p2align 3
.L764:
	movq	32(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L763
.L766:
	cmpl	%r12d, 8(%rbx)
	jne	.L764
	movq	(%rbx), %rdi
	movq	%r14, %rdx
	movq	%r13, %rsi
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L764
	movq	16(%rbx), %rax
	movq	bio_err(%rip), %rdi
	leaq	.LC142(%rip), %rsi
	movq	%rax, -48(%rbp)
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movslq	24(%rbx), %rdx
	leaq	-48(%rbp), %rsi
	xorl	%edi, %edi
	call	d2i_SSL_SESSION@PLT
	jmp	.L762
	.p2align 4,,10
	.p2align 3
.L763:
	movq	bio_err(%rip), %rdi
	leaq	.LC143(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	xorl	%eax, %eax
.L762:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L773
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L773:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1653:
	.size	get_session, .-get_session
	.section	.rodata.str1.1
.LC144:
	.string	"get session"
.LC145:
	.string	"Error encoding session\n"
.LC146:
	.string	"get session buffer"
	.section	.rodata.str1.8
	.align 8
.LC147:
	.string	"Out of memory adding to external cache\n"
	.align 8
.LC148:
	.string	"Unexpected session encoding length\n"
	.align 8
.LC149:
	.string	"New session added to external cache\n"
	.text
	.p2align 4
	.type	add_session, @function
add_session:
.LFB1652:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$40, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	leaq	.LC144(%rip), %rsi
	pushq	%r12
	subq	$24, %rsp
	.cfi_offset 12, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	app_malloc@PLT
	movq	%r13, %rdi
	leaq	8(%rax), %rsi
	movq	%rax, %r12
	call	SSL_SESSION_get_id@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	i2d_SSL_SESSION@PLT
	movl	%eax, 24(%r12)
	testl	%eax, %eax
	js	.L781
	xorl	%esi, %esi
	movl	8(%r12), %r14d
	movq	%r13, %rdi
	call	SSL_SESSION_get_id@PLT
	movl	$3549, %ecx
	leaq	.LC31(%rip), %rdx
	movq	%rax, %rdi
	movq	%r14, %rsi
	call	CRYPTO_memdup@PLT
	movl	24(%r12), %edi
	leaq	.LC146(%rip), %rsi
	movq	%rax, (%r12)
	call	app_malloc@PLT
	cmpq	$0, (%r12)
	movq	%rax, 16(%r12)
	je	.L782
	leaq	-48(%rbp), %rsi
	movq	%r13, %rdi
	movq	%rax, -48(%rbp)
	call	i2d_SSL_SESSION@PLT
	cmpl	24(%r12), %eax
	jne	.L783
	movq	first(%rip), %rax
	movq	bio_err(%rip), %rdi
	leaq	.LC149(%rip), %rsi
	movq	%r12, first(%rip)
	movq	%rax, 32(%r12)
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L776:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L784
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L783:
	.cfi_restore_state
	movq	bio_err(%rip), %rdi
	leaq	.LC148(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	(%r12), %rdi
	movl	$3563, %edx
	leaq	.LC31(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	16(%r12), %rdi
	movl	$3564, %edx
	leaq	.LC31(%rip), %rsi
	call	CRYPTO_free@PLT
	movl	$3565, %edx
	leaq	.LC31(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
	jmp	.L776
	.p2align 4,,10
	.p2align 3
.L781:
	movq	bio_err(%rip), %rdi
	leaq	.LC145(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movl	$3545, %edx
	leaq	.LC31(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
	jmp	.L776
	.p2align 4,,10
	.p2align 3
.L782:
	movq	bio_err(%rip), %rdi
	leaq	.LC147(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	(%r12), %rdi
	movl	$3553, %edx
	leaq	.LC31(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	16(%r12), %rdi
	movl	$3554, %edx
	leaq	.LC31(%rip), %rsi
	call	CRYPTO_free@PLT
	movl	$3555, %edx
	leaq	.LC31(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
	jmp	.L776
.L784:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1652:
	.size	add_session, .-add_session
	.p2align 4
	.type	load_dh_param, @function
load_dh_param:
.LFB1648:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC99(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	call	BIO_new_file@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L787
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	PEM_read_bio_DHparams@PLT
	movq	%rax, %r13
.L786:
	movq	%r12, %rdi
	call	BIO_free@PLT
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L787:
	.cfi_restore_state
	xorl	%r13d, %r13d
	jmp	.L786
	.cfi_endproc
.LFE1648:
	.size	load_dh_param, .-load_dh_param
	.section	.rodata.str1.8
	.align 8
.LC150:
	.string	"Error finding suitable ciphersuite\n"
	.text
	.p2align 4
	.type	psk_find_session_cb, @function
psk_find_session_cb:
.LFB1633:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$40, %rsp
	movq	psk_identity(%rip), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r15, %rdi
	call	strlen@PLT
	cmpq	%rbx, %rax
	jne	.L790
	movq	%r14, %rsi
	movq	%rax, %rdx
	movq	%r15, %rdi
	call	memcmp@PLT
	movl	%eax, %r14d
	testl	%eax, %eax
	jne	.L790
	movq	psksess(%rip), %rdi
	testq	%rdi, %rdi
	je	.L793
	call	SSL_SESSION_up_ref@PLT
	movq	psksess(%rip), %rax
	movl	$1, %r14d
	movq	%rax, (%r12)
	jmp	.L789
	.p2align 4,,10
	.p2align 3
.L790:
	movq	$0, (%r12)
	movl	$1, %r14d
.L789:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L810
	addq	$40, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L793:
	.cfi_restore_state
	movq	psk_key(%rip), %rdi
	leaq	-64(%rbp), %rsi
	call	OPENSSL_hexstr2buf@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L811
	leaq	tls13_aes128gcmsha256_id(%rip), %rsi
	movq	%r13, %rdi
	call	SSL_CIPHER_find@PLT
	movq	%rax, -72(%rbp)
	testq	%rax, %rax
	je	.L812
	call	SSL_SESSION_new@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L798
	movq	-64(%rbp), %rdx
	movq	%r15, %rsi
	movq	%rax, %rdi
	call	SSL_SESSION_set1_master_key@PLT
	testl	%eax, %eax
	jne	.L813
.L798:
	movl	$223, %edx
	leaq	.LC31(%rip), %rsi
	movq	%r15, %rdi
	call	CRYPTO_free@PLT
	jmp	.L789
	.p2align 4,,10
	.p2align 3
.L813:
	movq	-72(%rbp), %rsi
	movq	%rbx, %rdi
	call	SSL_SESSION_set_cipher@PLT
	testl	%eax, %eax
	je	.L798
	movq	%r13, %rdi
	call	SSL_version@PLT
	movq	%rbx, %rdi
	movl	%eax, %esi
	call	SSL_SESSION_set_protocol_version@PLT
	testl	%eax, %eax
	je	.L798
	movl	$226, %edx
	leaq	.LC31(%rip), %rsi
	movq	%r15, %rdi
	movl	$1, %r14d
	call	CRYPTO_free@PLT
	movq	%rbx, (%r12)
	jmp	.L789
	.p2align 4,,10
	.p2align 3
.L811:
	movq	psk_key(%rip), %rdx
	movq	bio_err(%rip), %rdi
	leaq	.LC135(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L789
	.p2align 4,,10
	.p2align 3
.L812:
	movq	bio_err(%rip), %rdi
	leaq	.LC150(%rip), %rsi
	call	BIO_printf@PLT
	movl	$214, %edx
	leaq	.LC31(%rip), %rsi
	movq	%r15, %rdi
	call	CRYPTO_free@PLT
	jmp	.L789
.L810:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1633:
	.size	psk_find_session_cb, .-psk_find_session_cb
	.section	.rodata.str1.1
.LC151:
	.string	"server2.pem"
.LC152:
	.string	"server.pem"
.LC153:
	.string	"4433"
	.section	.rodata.str1.8
	.align 8
.LC154:
	.string	"Cannot supply multiple protocol flags\n"
	.align 8
.LC155:
	.string	"Cannot supply both a protocol flag and '-no_<prot>'\n"
	.section	.rodata.str1.1
.LC156:
	.string	"%s: Use -help for summary.\n"
	.section	.rodata.str1.8
	.align 8
.LC157:
	.string	"%s: -port argument malformed or ambiguous\n"
	.align 8
.LC158:
	.string	"%s: -accept argument malformed or ambiguous\n"
	.section	.rodata.str1.1
.LC159:
	.string	"verify depth is %d\n"
	.section	.rodata.str1.8
	.align 8
.LC160:
	.string	"verify depth is %d, must return a certificate\n"
	.align 8
.LC161:
	.string	"%s: Memory allocation failure\n"
	.section	.rodata.str1.1
.LC162:
	.string	"Error parsing URL\n"
.LC163:
	.string	"w"
.LC164:
	.string	"Not a hex number '%s'\n"
	.section	.rodata.str1.8
	.align 8
.LC165:
	.string	"Invalid value for max_early_data\n"
	.align 8
.LC166:
	.string	"Invalid value for recv_max_early_data\n"
	.align 8
.LC167:
	.string	"Cannot supply -nextprotoneg with TLSv1.3\n"
	.align 8
.LC168:
	.string	"Can't use -HTTP, -www or -WWW with DTLS\n"
	.align 8
.LC169:
	.string	"Can only use -listen with DTLS\n"
	.align 8
.LC170:
	.string	"Can only use --stateless with TLS\n"
	.align 8
.LC171:
	.string	"Can't use unix sockets and datagrams together\n"
	.align 8
.LC172:
	.string	"Can't use -early_data in combination with -www, -WWW, -HTTP, or -rev\n"
	.section	.rodata.str1.1
.LC173:
	.string	"Error getting password\n"
	.section	.rodata.str1.8
	.align 8
.LC174:
	.string	"server certificate private key file"
	.section	.rodata.str1.1
.LC175:
	.string	"server certificate file"
.LC176:
	.string	"server certificate chain"
	.section	.rodata.str1.8
	.align 8
.LC177:
	.string	"second server certificate private key file"
	.align 8
.LC178:
	.string	"second server certificate file"
	.section	.rodata.str1.1
.LC179:
	.string	"Error loading CRL\n"
.LC180:
	.string	"Error adding CRL\n"
	.section	.rodata.str1.8
	.align 8
.LC181:
	.string	"second certificate private key file"
	.align 8
.LC182:
	.string	"second server certificate chain"
	.align 8
.LC183:
	.string	"Error using configuration \"%s\"\n"
	.align 8
.LC184:
	.string	"warning: id_prefix is too long, only one new session will be possible\n"
	.section	.rodata.str1.1
.LC185:
	.string	"error setting 'id_prefix'\n"
.LC186:
	.string	"id_prefix '%s' set.\n"
	.section	.rodata.str1.8
	.align 8
.LC187:
	.string	"%s: Max send fragment size %u is out of permitted range\n"
	.align 8
.LC188:
	.string	"%s: Split send fragment size %u is out of permitted range\n"
	.align 8
.LC189:
	.string	"%s: Max pipelines %u is out of permitted range\n"
	.section	.rodata.str1.1
.LC190:
	.string	"Error setting SRTP profile\n"
.LC191:
	.string	"Error setting verify params\n"
	.section	.rodata.str1.8
	.align 8
.LC192:
	.string	"Error loading store locations\n"
	.align 8
.LC193:
	.string	"Setting secondary ctx parameters\n"
	.section	.rodata.str1.1
.LC194:
	.string	"Setting temp DH parameters\n"
	.section	.rodata.str1.8
	.align 8
.LC195:
	.string	"Using default temp DH parameters\n"
	.align 8
.LC196:
	.string	"Error setting temp DH parameters\n"
	.align 8
.LC197:
	.string	"PSK key given, setting server callback\n"
	.align 8
.LC198:
	.string	"error setting PSK identity hint to context\n"
	.align 8
.LC199:
	.string	"Can't open PSK session file %s\n"
	.align 8
.LC200:
	.string	"Can't read PSK session file %s\n"
	.align 8
.LC201:
	.string	"error setting session id context\n"
	.align 8
.LC202:
	.string	"Cannot initialize SRP verifier file \"%s\":ret=%d\n"
	.text
	.p2align 4
	.globl	s_server_main
	.type	s_server_main, @function
s_server_main:
.LFB1641:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%edi, %r12d
	pushq	%rbx
	subq	$552, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	TLS_server_method@PLT
	movl	$1006, %edx
	leaq	.LC31(%rip), %rsi
	leaq	.LC153(%rip), %rdi
	movq	%rax, -280(%rbp)
	movq	$0, -168(%rbp)
	movq	$0, -160(%rbp)
	movq	$0, -152(%rbp)
	movq	$0, -144(%rbp)
	movq	$0, -136(%rbp)
	movl	$32773, -192(%rbp)
	movl	$32773, -188(%rbp)
	movl	$32773, -184(%rbp)
	movl	$32773, -180(%rbp)
	movl	$32773, -176(%rbp)
	movq	$0, -128(%rbp)
	call	CRYPTO_strdup@PLT
	pxor	%xmm0, %xmm0
	movl	$1, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	movq	%rax, -120(%rbp)
	movq	$0, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	$0, -96(%rbp)
	movq	$0, -88(%rbp)
	movl	$1, -172(%rbp)
	movl	%r12d, local_argc(%rip)
	movq	%r13, local_argv(%rip)
	movq	$0, ctx2(%rip)
	movq	$0, ctx(%rip)
	movl	$0, s_nbio_test(%rip)
	movl	$0, s_nbio(%rip)
	movl	$0, www(%rip)
	movq	$0, bio_s_out(%rip)
	movl	$0, s_debug(%rip)
	movl	$0, s_msg(%rip)
	movl	$0, s_quiet(%rip)
	movl	$0, s_brief(%rip)
	movl	$0, async(%rip)
	call	SSL_CONF_CTX_new@PLT
	movq	%rax, -264(%rbp)
	movq	%rax, %rbx
	call	X509_VERIFY_PARAM_new@PLT
	movq	%rax, -240(%rbp)
	testq	%rbx, %rbx
	je	.L1077
	testq	%rax, %rax
	je	.L1077
	movq	%rbx, %rdi
	movl	$9, %esi
	xorl	%r15d, %r15d
	xorl	%r14d, %r14d
	call	SSL_CONF_CTX_set_flags@PLT
	movq	%r13, %rsi
	movl	%r12d, %edi
	leaq	s_server_options(%rip), %rdx
	call	opt_init@PLT
	xorl	%r12d, %r12d
	movq	$0, -392(%rbp)
	xorl	%r13d, %r13d
	movq	%rax, -336(%rbp)
	leaq	.LC151(%rip), %rax
	leaq	.L828(%rip), %rbx
	movq	%rax, -344(%rbp)
	leaq	.LC152(%rip), %rax
	movl	$-1, -476(%rbp)
	movl	$-1, -296(%rbp)
	movq	$0, -576(%rbp)
	movq	$0, -488(%rbp)
	movl	$0, -352(%rbp)
	movl	$0, -372(%rbp)
	movl	$0, -376(%rbp)
	movl	$0, -480(%rbp)
	movl	$0, -292(%rbp)
	movq	$0, -496(%rbp)
	movq	$0, -504(%rbp)
	movq	$0, -200(%rbp)
	movq	$0, -320(%rbp)
	movq	$0, -408(%rbp)
	movq	$0, -224(%rbp)
	movq	%rax, -328(%rbp)
	movl	$0, -312(%rbp)
	movl	$0, -284(%rbp)
	movl	$0, -268(%rbp)
	movq	$0, -472(%rbp)
	movq	$0, -400(%rbp)
	movq	$0, -568(%rbp)
	movq	$0, -464(%rbp)
	movq	$0, -368(%rbp)
	movq	$0, -304(%rbp)
	movl	$0, -556(%rbp)
	movq	$0, -456(%rbp)
	movq	$0, -552(%rbp)
	movl	$0, -448(%rbp)
	movl	$1, -288(%rbp)
	movl	$0, -272(%rbp)
	movl	$0, -308(%rbp)
	movl	$-1, -544(%rbp)
	movl	$0, -348(%rbp)
	movl	$0, -444(%rbp)
	movl	$0, -432(%rbp)
	movl	$0, -384(%rbp)
	movl	$0, -540(%rbp)
	movq	$0, -440(%rbp)
	movl	$0, -428(%rbp)
	movl	$0, -424(%rbp)
	movl	$0, -380(%rbp)
	movl	$0, -420(%rbp)
	movq	$0, -208(%rbp)
	movq	$0, -536(%rbp)
	movq	$0, -528(%rbp)
	movq	$0, -256(%rbp)
	movq	$0, -248(%rbp)
	movq	$0, -520(%rbp)
	movq	$0, -512(%rbp)
	movq	$0, -360(%rbp)
	movq	$0, -416(%rbp)
	movq	$0, -216(%rbp)
	movq	$0, -232(%rbp)
.L816:
	call	opt_next@PLT
	testl	%eax, %eax
	je	.L1267
.L949:
	movl	%eax, %edx
	andl	$-5, %edx
	cmpl	$83, %edx
	je	.L817
	leal	-84(%rax), %edx
	andl	$-5, %edx
	cmpl	$2, %edx
	jbe	.L817
	movl	%r12d, %ecx
	leal	-3001(%rax), %edx
	andl	$1, %ecx
	cmpl	$4, %edx
	ja	.L821
	testl	%r12d, %r12d
	je	.L1268
	.p2align 4,,10
	.p2align 3
.L820:
	leaq	.LC155(%rip), %rsi
.L1264:
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
.L1263:
	call	BIO_printf@PLT
.L1261:
	movq	$0, -224(%rbp)
	xorl	%ebx, %ebx
	movl	$1, %r12d
.L1257:
	movq	$0, -200(%rbp)
	xorl	%r15d, %r15d
	movq	$0, -248(%rbp)
	movq	$0, -208(%rbp)
	movq	$0, -256(%rbp)
	jmp	.L815
	.p2align 4,,10
	.p2align 3
.L1077:
	movq	$0, -224(%rbp)
	xorl	%ebx, %ebx
	xorl	%r15d, %r15d
	movl	$1, %r12d
	movq	$0, -200(%rbp)
	movq	$0, -248(%rbp)
	movq	$0, -208(%rbp)
	movq	$0, -216(%rbp)
	movq	$0, -256(%rbp)
	movq	$0, -232(%rbp)
.L815:
	movq	ctx(%rip), %rdi
	call	SSL_CTX_free@PLT
	movq	psksess(%rip), %rdi
	call	SSL_SESSION_free@PLT
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	set_keylog_file@PLT
	movq	-248(%rbp), %rdi
	call	X509_free@PLT
	movq	X509_CRL_free@GOTPCREL(%rip), %rsi
	movq	-208(%rbp), %rdi
	call	OPENSSL_sk_pop_free@PLT
	movq	-200(%rbp), %rdi
	call	X509_free@PLT
	movq	-256(%rbp), %rdi
	call	EVP_PKEY_free@PLT
	movq	%r15, %rdi
	call	EVP_PKEY_free@PLT
	movq	X509_free@GOTPCREL(%rip), %r13
	movq	-160(%rbp), %rdi
	movq	%r13, %rsi
	call	OPENSSL_sk_pop_free@PLT
	movq	-152(%rbp), %rdi
	movq	%r13, %rsi
	leaq	.LC31(%rip), %r13
	call	OPENSSL_sk_pop_free@PLT
	movq	-136(%rbp), %rdi
	movl	$2170, %edx
	leaq	.LC31(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-144(%rbp), %rdi
	movl	$2171, %edx
	leaq	.LC31(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-128(%rbp), %rdi
	movl	$2172, %edx
	leaq	.LC31(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-120(%rbp), %rdi
	movl	$2173, %edx
	leaq	.LC31(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-240(%rbp), %rdi
	call	X509_VERIFY_PARAM_free@PLT
	movq	first(%rip), %r14
	testq	%r14, %r14
	je	.L1074
	.p2align 4,,10
	.p2align 3
.L1073:
	movq	(%r14), %rdi
	movl	$3626, %edx
	movq	%r13, %rsi
	call	CRYPTO_free@PLT
	movq	16(%r14), %rdi
	movl	$3627, %edx
	movq	%r13, %rsi
	call	CRYPTO_free@PLT
	movq	%r14, %rdi
	movq	32(%r14), %r14
	movq	%r13, %rsi
	movl	$3630, %edx
	call	CRYPTO_free@PLT
	testq	%r14, %r14
	jne	.L1073
.L1074:
	movq	16+tlscstatp(%rip), %rdi
	movl	$2176, %edx
	leaq	.LC31(%rip), %rsi
	movq	$0, first(%rip)
	call	CRYPTO_free@PLT
	movq	32+tlscstatp(%rip), %rdi
	movl	$2177, %edx
	leaq	.LC31(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	24+tlscstatp(%rip), %rdi
	movl	$2178, %edx
	leaq	.LC31(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	ctx2(%rip), %rdi
	call	SSL_CTX_free@PLT
	movq	%rbx, %rdi
	call	X509_free@PLT
	movq	-224(%rbp), %rdi
	call	EVP_PKEY_free@PLT
	movq	-112(%rbp), %rdi
	movl	$2183, %edx
	leaq	.LC31(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-96(%rbp), %rdi
	movl	$2185, %edx
	leaq	.LC31(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-168(%rbp), %rdi
	call	ssl_excert_free@PLT
	movq	-216(%rbp), %rdi
	call	OPENSSL_sk_free@PLT
	movq	-264(%rbp), %rdi
	call	SSL_CONF_CTX_free@PLT
	movq	-232(%rbp), %rdi
	call	release_engine@PLT
	movq	bio_s_out(%rip), %rdi
	call	BIO_free@PLT
	movq	bio_s_msg(%rip), %rdi
	movq	$0, bio_s_out(%rip)
	call	BIO_free@PLT
	movq	$0, bio_s_msg(%rip)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1269
	leaq	-40(%rbp), %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L817:
	.cfi_restore_state
	cmpl	$1, %r12d
	je	.L819
	leal	-3001(%rax), %edx
	cmpl	$4, %edx
	jbe	.L820
	movl	$1, %ecx
	movl	$1, %r12d
.L821:
	testl	%r15d, %r15d
	je	.L823
	testb	%cl, %cl
	jne	.L820
.L823:
	cmpl	$112, %eax
	jg	.L824
	cmpl	$-1, %eax
	jl	.L816
	addl	$1, %eax
	cmpl	$113, %eax
	ja	.L816
	movslq	(%rbx,%rax,4), %rax
	addq	%rbx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L828:
	.long	.L936-.L828
	.long	.L816-.L828
	.long	.L935-.L828
	.long	.L934-.L828
	.long	.L933-.L828
	.long	.L932-.L828
	.long	.L931-.L828
	.long	.L930-.L828
	.long	.L929-.L828
	.long	.L928-.L828
	.long	.L927-.L828
	.long	.L926-.L828
	.long	.L925-.L828
	.long	.L924-.L828
	.long	.L923-.L828
	.long	.L922-.L828
	.long	.L921-.L828
	.long	.L1078-.L828
	.long	.L920-.L828
	.long	.L919-.L828
	.long	.L918-.L828
	.long	.L917-.L828
	.long	.L916-.L828
	.long	.L915-.L828
	.long	.L914-.L828
	.long	.L913-.L828
	.long	.L912-.L828
	.long	.L911-.L828
	.long	.L910-.L828
	.long	.L909-.L828
	.long	.L908-.L828
	.long	.L907-.L828
	.long	.L906-.L828
	.long	.L905-.L828
	.long	.L904-.L828
	.long	.L903-.L828
	.long	.L902-.L828
	.long	.L901-.L828
	.long	.L900-.L828
	.long	.L899-.L828
	.long	.L898-.L828
	.long	.L897-.L828
	.long	.L896-.L828
	.long	.L895-.L828
	.long	.L894-.L828
	.long	.L893-.L828
	.long	.L892-.L828
	.long	.L891-.L828
	.long	.L890-.L828
	.long	.L889-.L828
	.long	.L888-.L828
	.long	.L887-.L828
	.long	.L886-.L828
	.long	.L885-.L828
	.long	.L884-.L828
	.long	.L883-.L828
	.long	.L882-.L828
	.long	.L881-.L828
	.long	.L880-.L828
	.long	.L879-.L828
	.long	.L878-.L828
	.long	.L877-.L828
	.long	.L876-.L828
	.long	.L875-.L828
	.long	.L874-.L828
	.long	.L873-.L828
	.long	.L872-.L828
	.long	.L871-.L828
	.long	.L870-.L828
	.long	.L869-.L828
	.long	.L868-.L828
	.long	.L867-.L828
	.long	.L866-.L828
	.long	.L865-.L828
	.long	.L864-.L828
	.long	.L863-.L828
	.long	.L862-.L828
	.long	.L861-.L828
	.long	.L860-.L828
	.long	.L859-.L828
	.long	.L858-.L828
	.long	.L857-.L828
	.long	.L856-.L828
	.long	.L855-.L828
	.long	.L854-.L828
	.long	.L853-.L828
	.long	.L852-.L828
	.long	.L851-.L828
	.long	.L850-.L828
	.long	.L849-.L828
	.long	.L848-.L828
	.long	.L847-.L828
	.long	.L816-.L828
	.long	.L846-.L828
	.long	.L845-.L828
	.long	.L844-.L828
	.long	.L843-.L828
	.long	.L842-.L828
	.long	.L841-.L828
	.long	.L840-.L828
	.long	.L839-.L828
	.long	.L838-.L828
	.long	.L837-.L828
	.long	.L836-.L828
	.long	.L835-.L828
	.long	.L834-.L828
	.long	.L833-.L828
	.long	.L832-.L828
	.long	.L831-.L828
	.long	.L830-.L828
	.long	.L829-.L828
	.long	.L827-.L828
	.long	.L827-.L828
	.long	.L827-.L828
	.text
.L1078:
	movl	$1, %r14d
	call	opt_next@PLT
	testl	%eax, %eax
	jne	.L949
.L1267:
	call	opt_num_rest@PLT
	call	opt_rest@PLT
	cmpl	$772, -268(%rbp)
	jne	.L950
	cmpq	$0, -304(%rbp)
	jne	.L1270
.L950:
	movl	www(%rip), %eax
	testl	%eax, %eax
	jne	.L1271
	movl	dtlslisten(%rip), %r10d
	testl	%r10d, %r10d
	je	.L953
	cmpl	$2, -288(%rbp)
	jne	.L1075
.L953:
	movl	stateless(%rip), %r9d
	testl	%r9d, %r9d
	je	.L954
	cmpl	$1, -288(%rbp)
	leaq	.LC170(%rip), %rsi
	jne	.L1264
.L1253:
	cmpl	$1, -272(%rbp)
	sete	-557(%rbp)
.L956:
	movl	early_data(%rip), %r8d
	testl	%r8d, %r8d
	je	.L957
	testl	%eax, %eax
	jg	.L1104
	movl	-348(%rbp), %edi
	testl	%edi, %edi
	jne	.L1104
.L957:
	movq	-248(%rbp), %rsi
	movq	-256(%rbp), %rdi
	leaq	-144(%rbp), %rcx
	leaq	-136(%rbp), %rdx
	call	app_passwd@PLT
	movq	bio_err(%rip), %rdi
	leaq	.LC173(%rip), %rsi
	testl	%eax, %eax
	je	.L1263
	movq	-224(%rbp), %rax
	leaq	-168(%rbp), %rdi
	testq	%rax, %rax
	cmove	-328(%rbp), %rax
	movq	%rax, -224(%rbp)
	movq	-320(%rbp), %rax
	testq	%rax, %rax
	cmove	-344(%rbp), %rax
	movq	%rax, -320(%rbp)
	call	load_excert@PLT
	testl	%eax, %eax
	je	.L1261
	movl	-384(%rbp), %esi
	testl	%esi, %esi
	je	.L1272
	movq	$0, -224(%rbp)
	xorl	%ebx, %ebx
	movq	$0, -248(%rbp)
	movq	$0, -256(%rbp)
.L962:
	cmpq	$0, -304(%rbp)
	je	.L972
	movq	-304(%rbp), %rsi
	leaq	-104(%rbp), %rdi
	call	next_protos_parse@PLT
	movq	%rax, -112(%rbp)
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L1092
.L972:
	cmpq	$0, -368(%rbp)
	movq	$0, -96(%rbp)
	je	.L971
	movq	-368(%rbp), %rsi
	leaq	-88(%rbp), %rdi
	call	next_protos_parse@PLT
	movq	%rax, -96(%rbp)
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L1092
.L971:
	cmpq	$0, -208(%rbp)
	je	.L974
	movl	-176(%rbp), %esi
	movq	-208(%rbp), %rdi
	call	load_crl@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L1273
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, -208(%rbp)
	testq	%rax, %rax
	je	.L977
	movq	%rax, %rdi
	movq	%r15, %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L977
.L974:
	movq	-200(%rbp), %rcx
	testq	%rcx, %rcx
	je	.L1093
	movq	-504(%rbp), %rax
	movq	-232(%rbp), %r8
	leaq	.LC181(%rip), %r9
	movl	-180(%rbp), %esi
	testq	%rax, %rax
	cmove	%rcx, %rax
	movq	-144(%rbp), %rcx
	xorl	%edx, %edx
	movq	%rax, %rdi
	call	load_key@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L1274
	movl	-184(%rbp), %esi
	movq	-200(%rbp), %rdi
	leaq	.LC178(%rip), %rdx
	call	load_cert@PLT
	movq	%rax, -200(%rbp)
	testq	%rax, %rax
	je	.L1260
	movq	-496(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L978
	leaq	-152(%rbp), %rsi
	leaq	.LC182(%rip), %r8
	xorl	%ecx, %ecx
	movl	$32773, %edx
	call	load_certs@PLT
	movl	$1, %r12d
	testl	%eax, %eax
	je	.L815
.L978:
	cmpq	$0, bio_s_out(%rip)
	je	.L1275
.L983:
	movl	-384(%rbp), %ecx
	movq	-344(%rbp), %rsi
	xorl	%eax, %eax
	movq	-280(%rbp), %rdi
	testl	%ecx, %ecx
	cmovne	%rax, %rsi
	cmove	-328(%rbp), %rax
	movq	%rsi, -344(%rbp)
	movq	%rax, -328(%rbp)
	call	SSL_CTX_new@PLT
	movq	%rax, ctx(%rip)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1260
	xorl	%ecx, %ecx
	movl	$4, %edx
	movl	$78, %esi
	call	SSL_CTX_ctrl@PLT
	movl	-308(%rbp), %eax
	testl	%eax, %eax
	jne	.L1276
.L988:
	movq	ctx(%rip), %rdx
	movq	-216(%rbp), %rsi
	movl	$1, %r12d
	movq	-264(%rbp), %rdi
	call	config_ctx@PLT
	testl	%eax, %eax
	je	.L815
	movq	-456(%rbp), %rax
	testq	%rax, %rax
	je	.L989
	movq	ctx(%rip), %rdi
	movq	%rax, %rsi
	call	SSL_CTX_config@PLT
	testl	%eax, %eax
	je	.L1277
.L989:
	movl	-268(%rbp), %eax
	testl	%eax, %eax
	jne	.L990
.L993:
	movl	-284(%rbp), %eax
	testl	%eax, %eax
	jne	.L1278
.L992:
	movq	session_id_prefix(%rip), %rdi
	testq	%rdi, %rdi
	je	.L995
	call	strlen@PLT
	cmpq	$31, %rax
	ja	.L1279
.L996:
	movq	ctx(%rip), %rdi
	leaq	generate_session_id(%rip), %rsi
	call	SSL_CTX_set_generate_session_id@PLT
	testl	%eax, %eax
	je	.L1266
	movq	session_id_prefix(%rip), %rdx
	movq	bio_err(%rip), %rdi
	leaq	.LC186(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L995:
	movq	ctx(%rip), %rdi
	movl	$1, %esi
	call	SSL_CTX_set_quiet_shutdown@PLT
	movq	-168(%rbp), %rsi
	testq	%rsi, %rsi
	je	.L998
	movq	ctx(%rip), %rdi
	call	ssl_ctx_set_excert@PLT
.L998:
	movl	-448(%rbp), %eax
	movq	ctx(%rip), %rdi
	testl	%eax, %eax
	jne	.L1280
.L999:
	movl	-424(%rbp), %eax
	testl	%eax, %eax
	jne	.L1281
	movl	-428(%rbp), %r12d
	testl	%r12d, %r12d
	je	.L1002
	call	init_session_cache_ctx
.L1001:
	movl	async(%rip), %r11d
	movq	ctx(%rip), %rdi
	testl	%r11d, %r11d
	jne	.L1282
.L1003:
	movl	-376(%rbp), %r10d
	testl	%r10d, %r10d
	jne	.L1283
.L1004:
	movl	-372(%rbp), %r9d
	testl	%r9d, %r9d
	jne	.L1284
.L1006:
	movl	-352(%rbp), %r8d
	testl	%r8d, %r8d
	jne	.L1285
.L1008:
	movl	-556(%rbp), %eax
	testl	%eax, %eax
	jle	.L1009
	movslq	%eax, %rsi
	call	SSL_CTX_set_default_read_buffer_len@PLT
	movq	ctx(%rip), %rdi
.L1009:
	movq	-472(%rbp), %rax
	testq	%rax, %rax
	je	.L1010
	movq	%rax, %rsi
	call	SSL_CTX_set_tlsext_use_srtp@PLT
	movq	ctx(%rip), %rdi
	testl	%eax, %eax
	jne	.L1286
.L1010:
	movl	-432(%rbp), %r8d
	movl	-444(%rbp), %ecx
	movq	-416(%rbp), %rdx
	movq	-360(%rbp), %rsi
	call	ctx_set_verify_locations@PLT
	testl	%eax, %eax
	je	.L1260
	cmpl	$0, -420(%rbp)
	je	.L1012
	movq	-240(%rbp), %rsi
	movq	ctx(%rip), %rdi
	call	SSL_CTX_set1_param@PLT
	testl	%eax, %eax
	je	.L1265
.L1012:
	movq	-208(%rbp), %r12
	movq	ctx(%rip), %rdi
	xorl	%edx, %edx
	movq	%r12, %rsi
	call	ssl_ctx_add_crls@PLT
	movq	-528(%rbp), %rsi
	movq	%r12, %r9
	movq	ctx(%rip), %rdi
	movq	-520(%rbp), %r8
	movq	-536(%rbp), %rdx
	pushq	%rcx
	pushq	%r14
	movq	-512(%rbp), %rcx
	call	ssl_load_stores@PLT
	popq	%rsi
	leaq	.LC192(%rip), %rsi
	popq	%rdi
	testl	%eax, %eax
	je	.L1259
	testq	%rbx, %rbx
	je	.L1014
	movq	-280(%rbp), %rdi
	call	SSL_CTX_new@PLT
	movq	%rax, ctx2(%rip)
	testq	%rax, %rax
	je	.L1260
.L1018:
	movq	bio_s_out(%rip), %rdi
	xorl	%eax, %eax
	leaq	.LC193(%rip), %rsi
	call	BIO_printf@PLT
	cmpl	$0, -308(%rbp)
	jne	.L1287
.L1017:
	movq	session_id_prefix(%rip), %rdi
	testq	%rdi, %rdi
	je	.L1021
	xorl	%eax, %eax
	orq	$-1, %rcx
	repnz scasb
	movq	%rcx, %rax
	notq	%rax
	subq	$1, %rax
	cmpq	$31, %rax
	ja	.L1288
.L1022:
	movq	ctx2(%rip), %rdi
	leaq	generate_session_id(%rip), %rsi
	call	SSL_CTX_set_generate_session_id@PLT
	testl	%eax, %eax
	je	.L1266
	movq	session_id_prefix(%rip), %rdx
	movq	bio_err(%rip), %rdi
	leaq	.LC186(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L1021:
	movq	ctx2(%rip), %rdi
	movl	$1, %esi
	call	SSL_CTX_set_quiet_shutdown@PLT
	movq	-168(%rbp), %rsi
	testq	%rsi, %rsi
	je	.L1024
	movq	ctx2(%rip), %rdi
	call	ssl_ctx_set_excert@PLT
.L1024:
	cmpl	$0, -448(%rbp)
	jne	.L1289
.L1025:
	cmpl	$0, -424(%rbp)
	movq	ctx2(%rip), %rdi
	jne	.L1290
	cmpl	$0, -428(%rbp)
	je	.L1028
	call	init_session_cache_ctx
.L1027:
	cmpl	$0, async(%rip)
	jne	.L1291
.L1029:
	movl	-432(%rbp), %r8d
	movl	-444(%rbp), %ecx
	movq	-416(%rbp), %rdx
	movq	-360(%rbp), %rsi
	movq	ctx2(%rip), %rdi
	call	ctx_set_verify_locations@PLT
	testl	%eax, %eax
	je	.L1260
	cmpl	$0, -420(%rbp)
	je	.L1031
	movq	-240(%rbp), %rsi
	movq	ctx2(%rip), %rdi
	call	SSL_CTX_set1_param@PLT
	testl	%eax, %eax
	je	.L1265
.L1031:
	movq	-208(%rbp), %rsi
	movq	ctx2(%rip), %rdi
	xorl	%edx, %edx
	call	ssl_ctx_add_crls@PLT
	movq	ctx2(%rip), %rdx
	movq	-216(%rbp), %rsi
	movq	-264(%rbp), %rdi
	call	config_ctx@PLT
	testl	%eax, %eax
	je	.L1101
.L1032:
	cmpq	$0, -112(%rbp)
	je	.L1020
	movq	ctx(%rip), %rdi
	leaq	-112(%rbp), %rdx
	leaq	next_proto_cb(%rip), %rsi
	call	SSL_CTX_set_next_protos_advertised_cb@PLT
.L1020:
	cmpq	$0, -96(%rbp)
	je	.L1033
	movq	ctx(%rip), %rdi
	leaq	-96(%rbp), %rdx
	leaq	alpn_cb(%rip), %rsi
	call	SSL_CTX_set_alpn_select_cb@PLT
.L1033:
	cmpl	$0, -540(%rbp)
	jne	.L1034
	movq	-440(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1035
.L1254:
	call	load_dh_param
	movq	bio_s_out(%rip), %rdi
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1037
	leaq	.LC194(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_s_out(%rip), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	call	BIO_ctrl@PLT
	xorl	%edx, %edx
	movq	%r12, %rcx
	movl	$3, %esi
	movq	ctx(%rip), %rdi
	call	SSL_CTX_ctrl@PLT
	testq	%rax, %rax
	je	.L1262
.L1039:
	cmpq	$0, ctx2(%rip)
	je	.L1040
	cmpq	$0, -440(%rbp)
	je	.L1292
.L1041:
	testq	%r12, %r12
	je	.L1293
.L1042:
	movq	ctx2(%rip), %rdi
	xorl	%edx, %edx
	movq	%r12, %rcx
	movl	$3, %esi
	call	SSL_CTX_ctrl@PLT
	testq	%rax, %rax
	je	.L1262
.L1040:
	movq	%r12, %rdi
	call	DH_free@PLT
.L1034:
	movl	-380(%rbp), %r8d
	movq	-160(%rbp), %rcx
	movq	-256(%rbp), %rdx
	movq	-248(%rbp), %rsi
	movq	ctx(%rip), %rdi
	call	set_cert_key_stuff@PLT
	testl	%eax, %eax
	je	.L1101
	movq	-488(%rbp), %rax
	testq	%rax, %rax
	je	.L1043
	movq	ctx(%rip), %rdi
	movq	%rax, %rsi
	call	SSL_CTX_use_serverinfo_file@PLT
	testl	%eax, %eax
	je	.L1260
.L1043:
	movq	ctx2(%rip), %rdi
	testq	%rdi, %rdi
	je	.L1047
	movl	-380(%rbp), %r8d
	movq	-224(%rbp), %rdx
	xorl	%ecx, %ecx
	movq	%rbx, %rsi
	call	set_cert_key_stuff@PLT
	testl	%eax, %eax
	je	.L1101
.L1047:
	cmpq	$0, -200(%rbp)
	je	.L1046
	movl	-380(%rbp), %r8d
	movq	-152(%rbp), %rcx
	movq	%r15, %rdx
	movq	-200(%rbp), %rsi
	movq	ctx(%rip), %rdi
	call	set_cert_key_stuff@PLT
	testl	%eax, %eax
	je	.L1101
.L1046:
	cmpl	$0, -480(%rbp)
	jne	.L1294
.L1050:
	cmpq	$0, psk_key(%rip)
	je	.L1051
	cmpl	$0, s_debug(%rip)
	jne	.L1295
.L1052:
	movq	ctx(%rip), %rdi
	leaq	psk_server_cb(%rip), %rsi
	call	SSL_CTX_set_psk_server_callback@PLT
.L1051:
	movq	-464(%rbp), %rsi
	movq	ctx(%rip), %rdi
	call	SSL_CTX_use_psk_identity_hint@PLT
	leaq	.LC198(%rip), %rsi
	testl	%eax, %eax
	je	.L1259
	movq	-392(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1054
	leaq	.LC99(%rip), %rsi
	call	BIO_new_file@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1296
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	PEM_read_bio_SSL_SESSION@PLT
	movq	%r12, %rdi
	movq	%rax, psksess(%rip)
	call	BIO_free@PLT
	cmpq	$0, psksess(%rip)
	je	.L1297
.L1056:
	movq	ctx(%rip), %rdi
	leaq	psk_find_session_cb(%rip), %rsi
	call	SSL_CTX_set_psk_find_session_callback@PLT
.L1057:
	movq	verify_callback@GOTPCREL(%rip), %r14
	movl	-312(%rbp), %esi
	leaq	-172(%rbp), %r12
	movq	ctx(%rip), %rdi
	movq	%r14, %rdx
	call	SSL_CTX_set_verify@PLT
	movq	ctx(%rip), %rdi
	movl	$4, %edx
	movq	%r12, %rsi
	call	SSL_CTX_set_session_id_context@PLT
	testl	%eax, %eax
	je	.L1256
	movq	generate_cookie_callback@GOTPCREL(%rip), %rsi
	movq	ctx(%rip), %rdi
	call	SSL_CTX_set_cookie_generate_cb@PLT
	movq	verify_cookie_callback@GOTPCREL(%rip), %rsi
	movq	ctx(%rip), %rdi
	call	SSL_CTX_set_cookie_verify_cb@PLT
	movq	generate_stateless_cookie_callback@GOTPCREL(%rip), %rsi
	movq	ctx(%rip), %rdi
	call	SSL_CTX_set_stateless_cookie_generate_cb@PLT
	movq	ctx(%rip), %rdi
	movq	verify_stateless_cookie_callback@GOTPCREL(%rip), %rsi
	call	SSL_CTX_set_stateless_cookie_verify_cb@PLT
	movq	ctx2(%rip), %rdi
	testq	%rdi, %rdi
	je	.L1059
	movl	-312(%rbp), %esi
	movq	%r14, %rdx
	call	SSL_CTX_set_verify@PLT
	movq	ctx2(%rip), %rdi
	movl	$4, %edx
	movq	%r12, %rsi
	call	SSL_CTX_set_session_id_context@PLT
	testl	%eax, %eax
	je	.L1256
	movq	bio_s_out(%rip), %rax
	movq	ctx2(%rip), %rdi
	leaq	ssl_servername_cb(%rip), %rdx
	movl	$53, %esi
	leaq	-80(%rbp), %r12
	movq	%rax, -72(%rbp)
	call	SSL_CTX_callback_ctrl@PLT
	movq	%r12, %rcx
	xorl	%edx, %edx
	movl	$54, %esi
	movq	ctx2(%rip), %rdi
	call	SSL_CTX_ctrl@PLT
	movq	ctx(%rip), %rdi
	movl	$53, %esi
	leaq	ssl_servername_cb(%rip), %rdx
	call	SSL_CTX_callback_ctrl@PLT
	movq	%r12, %rcx
	xorl	%edx, %edx
	movl	$54, %esi
	movq	ctx(%rip), %rdi
	call	SSL_CTX_ctrl@PLT
.L1059:
	movq	-400(%rbp), %r12
	testq	%r12, %r12
	je	.L1061
	movq	-568(%rbp), %rdi
	call	SRP_VBASE_new@PLT
	movq	%r12, %rsi
	movq	$0, 16+srp_callback_parm(%rip)
	movq	%rax, %rdi
	movq	%rax, 8+srp_callback_parm(%rip)
	movq	$0, srp_callback_parm(%rip)
	call	SRP_VBASE_init@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	jne	.L1298
	movq	ctx(%rip), %rdi
	movq	%r14, %rdx
	xorl	%esi, %esi
	call	SSL_CTX_set_verify@PLT
	movq	ctx(%rip), %rdi
	leaq	srp_callback_parm(%rip), %rsi
	call	SSL_CTX_set_srp_cb_arg@PLT
	movq	ctx(%rip), %rdi
	leaq	ssl_srp_server_param_cb(%rip), %rsi
	call	SSL_CTX_set_srp_username_callback@PLT
.L1063:
	cmpl	$0, -292(%rbp)
	jne	.L1299
.L1067:
	movq	-576(%rbp), %rsi
	movq	ctx(%rip), %rdi
	call	set_keylog_file@PLT
	testl	%eax, %eax
	jne	.L815
	movl	-296(%rbp), %eax
	cmpl	$-1, %eax
	je	.L1069
	movq	ctx(%rip), %rdi
	movl	%eax, %esi
	call	SSL_CTX_set_max_early_data@PLT
.L1069:
	movl	-476(%rbp), %eax
	cmpl	$-1, %eax
	je	.L1070
	movq	ctx(%rip), %rdi
	movl	%eax, %esi
	call	SSL_CTX_set_recv_max_early_data@PLT
.L1070:
	cmpl	$0, -348(%rbp)
	leaq	rev_body(%rip), %r12
	jne	.L1071
	cmpl	$0, www(%rip)
	leaq	www_body(%rip), %r12
	leaq	sv_body(%rip), %rax
	cmove	%rax, %r12
.L1071:
	testl	%r13d, %r13d
	je	.L1072
	cmpb	$0, -557(%rbp)
	je	.L1072
	movq	-128(%rbp), %rdi
	call	unlink@PLT
.L1072:
	movl	-544(%rbp), %eax
	pushq	bio_s_out(%rip)
	xorl	%r9d, %r9d
	leaq	accept_socket(%rip), %rdi
	movl	-288(%rbp), %r8d
	movl	-272(%rbp), %ecx
	movq	-120(%rbp), %rdx
	pushq	%rax
	pushq	-552(%rbp)
	movq	-128(%rbp), %rsi
	pushq	%r12
	xorl	%r12d, %r12d
	call	do_server@PLT
	movq	ctx(%rip), %rsi
	movq	bio_s_out(%rip), %rdi
	addq	$32, %rsp
	call	print_stats
	jmp	.L815
.L928:
	movl	$1, %r13d
	jmp	.L816
	.p2align 4,,10
	.p2align 3
.L824:
	cmpl	$1502, %eax
	jg	.L937
	cmpl	$1500, %eax
	jg	.L938
	leal	-1001(%rax), %edx
	cmpl	$5, %edx
	ja	.L816
	leaq	-168(%rbp), %rsi
	movl	%eax, %edi
	call	args_excert@PLT
	testl	%eax, %eax
	jne	.L816
	jmp	.L1261
	.p2align 4,,10
	.p2align 3
.L937:
	cmpl	$2030, %eax
	jg	.L940
	cmpl	$2000, %eax
	jle	.L816
	movq	-240(%rbp), %rsi
	movl	%eax, %edi
	call	opt_verify@PLT
	testl	%eax, %eax
	je	.L1261
	addl	$1, -420(%rbp)
	jmp	.L816
	.p2align 4,,10
	.p2align 3
.L940:
	cmpl	$29, %edx
	ja	.L816
.L827:
	cmpq	$0, -216(%rbp)
	je	.L1300
.L945:
	call	opt_flag@PLT
	movq	-216(%rbp), %rdi
	movq	%rax, %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L946
	call	opt_arg@PLT
	movq	-216(%rbp), %rdi
	movq	%rax, %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	jne	.L816
.L946:
	movq	-336(%rbp), %rdx
	movq	bio_err(%rip), %rdi
	leaq	.LC161(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L1261
.L1270:
	movq	bio_err(%rip), %rdi
	leaq	.LC167(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L936:
	movq	-336(%rbp), %rdx
	movq	bio_err(%rip), %rdi
	leaq	.LC156(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L1261
.L829:
	movl	-296(%rbp), %ecx
	movl	$16384, %eax
	movl	$1, early_data(%rip)
	cmpl	$-1, %ecx
	cmovne	%ecx, %eax
	movl	%eax, -296(%rbp)
	jmp	.L816
.L830:
	call	opt_arg@PLT
	movl	$10, %edx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	strtol@PLT
	movl	%eax, -476(%rbp)
	testl	%eax, %eax
	jns	.L816
	leaq	.LC166(%rip), %rsi
	jmp	.L1264
.L831:
	call	opt_arg@PLT
	movl	$10, %edx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	strtol@PLT
	movl	%eax, -296(%rbp)
	testl	%eax, %eax
	jns	.L816
	leaq	.LC165(%rip), %rsi
	jmp	.L1264
.L832:
	call	opt_arg@PLT
	movq	%rax, -576(%rbp)
	jmp	.L816
.L833:
	call	opt_arg@PLT
	movl	$10, %edx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	strtol@PLT
	movl	%eax, keymatexportlen(%rip)
	jmp	.L816
.L834:
	call	opt_arg@PLT
	movq	%rax, keymatexportlabel(%rip)
	jmp	.L816
.L835:
	call	opt_arg@PLT
	movq	%rax, -472(%rbp)
	jmp	.L816
.L836:
	call	opt_arg@PLT
	movq	%rax, -368(%rbp)
	jmp	.L816
.L837:
	call	opt_arg@PLT
	movq	%rax, -304(%rbp)
	jmp	.L816
.L838:
	call	opt_arg@PLT
	movq	%rax, -320(%rbp)
	jmp	.L816
.L839:
	call	opt_arg@PLT
	movq	%rax, -344(%rbp)
	jmp	.L816
.L840:
	movl	$2, -64(%rbp)
	jmp	.L816
.L841:
	call	opt_arg@PLT
	movq	%rax, -80(%rbp)
	jmp	.L816
.L842:
	call	opt_arg@PLT
	movq	%rax, session_id_prefix(%rip)
	jmp	.L816
.L843:
	movl	$1, stateless(%rip)
	jmp	.L816
.L844:
	movl	$1, dtlslisten(%rip)
	jmp	.L816
.L845:
	call	opt_arg@PLT
	movl	$10, %edx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	strtol@PLT
	movq	%rax, socket_mtu(%rip)
	jmp	.L816
.L846:
	movl	$1, enable_timeouts(%rip)
	jmp	.L816
.L847:
	call	DTLS_server_method@PLT
	movl	$65277, -284(%rbp)
	movq	%rax, -280(%rbp)
	movl	$65277, -268(%rbp)
	movl	$2, -288(%rbp)
	jmp	.L816
.L848:
	call	DTLS_server_method@PLT
	movl	$65279, -284(%rbp)
	movq	%rax, -280(%rbp)
	movl	$65279, -268(%rbp)
	movl	$2, -288(%rbp)
	jmp	.L816
.L849:
	call	DTLS_server_method@PLT
	movl	$2, -288(%rbp)
	movq	%rax, -280(%rbp)
	jmp	.L816
.L850:
	movl	$769, -284(%rbp)
	movl	$769, -268(%rbp)
	jmp	.L816
.L851:
	movl	$770, -284(%rbp)
	movl	$770, -268(%rbp)
	jmp	.L816
.L852:
	movl	$771, -284(%rbp)
	movl	$771, -268(%rbp)
	jmp	.L816
.L853:
	movl	$772, -284(%rbp)
	movl	$772, -268(%rbp)
	jmp	.L816
.L854:
	movl	$768, -284(%rbp)
	movl	$768, -268(%rbp)
	jmp	.L816
.L855:
	call	opt_arg@PLT
	movl	$10, %edx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	strtol@PLT
	movl	%eax, -556(%rbp)
	jmp	.L816
.L856:
	call	opt_arg@PLT
	movl	$10, %edx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	strtol@PLT
	movl	%eax, -352(%rbp)
	jmp	.L816
.L857:
	call	opt_arg@PLT
	movl	$10, %edx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	strtol@PLT
	movl	%eax, -372(%rbp)
	jmp	.L816
.L858:
	call	opt_arg@PLT
	movl	$10, %edx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	strtol@PLT
	movl	%eax, -376(%rbp)
	jmp	.L816
.L859:
	call	opt_arg@PLT
	movq	%rax, -456(%rbp)
	jmp	.L816
.L860:
	movl	$1, async(%rip)
	jmp	.L816
.L861:
	movl	$3, www(%rip)
	jmp	.L816
.L862:
	movl	$2, www(%rip)
	jmp	.L816
.L863:
	movl	$1, www(%rip)
	jmp	.L816
.L864:
	movl	$1, -348(%rbp)
	jmp	.L816
.L865:
	call	opt_arg@PLT
	movl	-268(%rbp), %ecx
	movq	%rax, -568(%rbp)
	movl	$769, %eax
	cmpl	$769, %ecx
	cmovge	%ecx, %eax
	movl	%eax, -268(%rbp)
	jmp	.L816
.L866:
	call	opt_arg@PLT
	movl	-268(%rbp), %ecx
	movq	%rax, -400(%rbp)
	movl	$769, %eax
	cmpl	$769, %ecx
	cmovge	%ecx, %eax
	movl	%eax, -268(%rbp)
	jmp	.L816
.L867:
	call	opt_arg@PLT
	movq	%rax, -392(%rbp)
	jmp	.L816
.L868:
	call	opt_arg@PLT
	movq	%rax, psk_key(%rip)
	movzbl	(%rax), %edx
	movq	%rax, -584(%rbp)
	testb	%dl, %dl
	movb	%dl, -557(%rbp)
	je	.L816
	call	__ctype_b_loc@PLT
	movq	-584(%rbp), %r8
	movzbl	-557(%rbp), %edx
	movq	(%rax), %rcx
	movq	%r8, %rax
.L948:
	testb	$16, 1(%rcx,%rdx,2)
	je	.L947
	movzbl	1(%rax), %edx
	addq	$1, %rax
	testb	%dl, %dl
	jne	.L948
	jmp	.L816
.L869:
	call	opt_arg@PLT
	movq	%rax, -464(%rbp)
	jmp	.L816
.L870:
	call	opt_arg@PLT
	movq	%rax, psk_identity(%rip)
	jmp	.L816
.L871:
	movl	$1, -480(%rbp)
	jmp	.L816
.L872:
	movl	$1, -540(%rbp)
	jmp	.L816
.L873:
	movl	$1, 4+verify_args(%rip)
	movl	$1, s_brief(%rip)
	movl	$1, s_quiet(%rip)
	jmp	.L816
.L874:
	movl	$1, s_quiet(%rip)
	jmp	.L816
.L875:
	movl	$1, s_crlf(%rip)
	jmp	.L816
.L876:
	movl	$1, -448(%rbp)
	jmp	.L816
.L877:
	movl	$2, -308(%rbp)
	jmp	.L816
.L878:
	movl	$1, -308(%rbp)
	jmp	.L816
.L879:
	movl	$2, s_msg(%rip)
	jmp	.L816
.L880:
	call	opt_arg@PLT
	leaq	.LC163(%rip), %rsi
	movq	%rax, %rdi
	call	BIO_new_file@PLT
	movq	%rax, bio_s_msg(%rip)
	jmp	.L816
.L881:
	movl	$1, s_msg(%rip)
	jmp	.L816
.L882:
	call	opt_arg@PLT
	movl	$1, -292(%rbp)
	movq	%rax, 8+tlscstatp(%rip)
	jmp	.L816
.L883:
	call	opt_arg@PLT
	leaq	40+tlscstatp(%rip), %r8
	leaq	-16(%r8), %rcx
	movq	%rax, %rdi
	leaq	8(%rcx), %rdx
	leaq	-16(%rdx), %rsi
	call	OCSP_parse_url@PLT
	testl	%eax, %eax
	je	.L1301
	movl	$1, -292(%rbp)
	jmp	.L816
.L884:
	call	opt_arg@PLT
	movl	$10, %edx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	strtol@PLT
	movl	$1, -292(%rbp)
	movl	%eax, tlscstatp(%rip)
	jmp	.L816
.L885:
	movl	$1, 44+tlscstatp(%rip)
	movl	$1, -292(%rbp)
	jmp	.L816
.L886:
	movl	$1, -292(%rbp)
	jmp	.L816
.L887:
	movl	$1, s_tlsextdebug(%rip)
	jmp	.L816
.L888:
	movl	$1, s_debug(%rip)
	jmp	.L816
.L889:
	movl	$0, s_ign_eof(%rip)
	jmp	.L816
.L890:
	movl	$1, s_ign_eof(%rip)
	jmp	.L816
.L891:
	movl	$1, s_nbio_test(%rip)
	movl	$1, s_nbio(%rip)
	jmp	.L816
.L892:
	movl	$1, s_nbio(%rip)
	jmp	.L816
.L893:
	call	opt_arg@PLT
	movq	%rax, -536(%rbp)
	jmp	.L816
.L894:
	call	opt_arg@PLT
	movq	%rax, -520(%rbp)
	jmp	.L816
.L895:
	movl	$1, -444(%rbp)
	jmp	.L816
.L896:
	call	opt_arg@PLT
	movq	%rax, -360(%rbp)
	jmp	.L816
.L897:
	movl	$1, -380(%rbp)
	jmp	.L816
.L898:
	movl	$1, 4+verify_args(%rip)
	jmp	.L816
.L899:
	movl	$1, 12+verify_args(%rip)
	jmp	.L816
.L900:
	call	opt_arg@PLT
	leaq	-176(%rbp), %rdx
	movl	$2, %esi
	movq	%rax, %rdi
	call	opt_format@PLT
	testl	%eax, %eax
	jne	.L816
	jmp	.L936
.L901:
	movl	$1, -428(%rbp)
	jmp	.L816
.L902:
	movl	$1, -424(%rbp)
	jmp	.L816
.L903:
	call	opt_arg@PLT
	movq	%rax, -528(%rbp)
	jmp	.L816
.L904:
	call	opt_arg@PLT
	movq	%rax, -512(%rbp)
	jmp	.L816
.L905:
	movl	$1, -432(%rbp)
	jmp	.L816
.L906:
	call	opt_arg@PLT
	movq	%rax, -416(%rbp)
	jmp	.L816
.L907:
	movl	$1, -384(%rbp)
	jmp	.L816
.L908:
	call	opt_arg@PLT
	movq	%rax, -496(%rbp)
	jmp	.L816
.L909:
	call	opt_arg@PLT
	movq	%rax, -504(%rbp)
	jmp	.L816
.L910:
	call	opt_arg@PLT
	movq	%rax, -248(%rbp)
	jmp	.L816
.L911:
	call	opt_arg@PLT
	leaq	-180(%rbp), %rdx
	movl	$2, %esi
	movq	%rax, %rdi
	call	opt_format@PLT
	testl	%eax, %eax
	jne	.L816
	jmp	.L936
.L912:
	call	opt_arg@PLT
	movq	%rax, -200(%rbp)
	jmp	.L816
.L913:
	call	opt_arg@PLT
	leaq	-184(%rbp), %rdx
	movl	$2, %esi
	movq	%rax, %rdi
	call	opt_format@PLT
	testl	%eax, %eax
	jne	.L816
	jmp	.L936
.L914:
	call	opt_arg@PLT
	movq	%rax, -440(%rbp)
	jmp	.L816
.L915:
	call	opt_arg@PLT
	movq	%rax, -408(%rbp)
	jmp	.L816
.L916:
	call	opt_arg@PLT
	movq	%rax, -256(%rbp)
	jmp	.L816
.L917:
	call	opt_arg@PLT
	leaq	-188(%rbp), %rdx
	movl	$1982, %esi
	movq	%rax, %rdi
	call	opt_format@PLT
	testl	%eax, %eax
	jne	.L816
	jmp	.L936
.L918:
	call	opt_arg@PLT
	movq	%rax, -224(%rbp)
	jmp	.L816
.L919:
	call	opt_arg@PLT
	leaq	-192(%rbp), %rdx
	movl	$2, %esi
	movq	%rax, %rdi
	call	opt_format@PLT
	testl	%eax, %eax
	jne	.L816
	jmp	.L936
.L920:
	call	opt_arg@PLT
	movq	%rax, -488(%rbp)
	jmp	.L816
.L921:
	call	opt_arg@PLT
	movq	%rax, -208(%rbp)
	jmp	.L816
.L922:
	call	opt_arg@PLT
	movq	%rax, -328(%rbp)
	jmp	.L816
.L923:
	call	opt_arg@PLT
	movq	%rax, -552(%rbp)
	jmp	.L816
.L924:
	call	opt_arg@PLT
	movl	$10, %edx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	strtol@PLT
	movl	s_quiet(%rip), %r11d
	movl	$7, -312(%rbp)
	movl	%eax, verify_args(%rip)
	testl	%r11d, %r11d
	jne	.L816
	movq	bio_err(%rip), %rdi
	movl	%eax, %edx
	leaq	.LC160(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L816
.L925:
	call	opt_arg@PLT
	movq	%rax, %rdi
	call	set_nameopt@PLT
	testl	%eax, %eax
	jne	.L816
	jmp	.L1261
.L926:
	call	opt_arg@PLT
	movl	$10, %edx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	strtol@PLT
	movl	s_quiet(%rip), %edx
	movl	$5, -312(%rbp)
	movl	%eax, verify_args(%rip)
	testl	%edx, %edx
	jne	.L816
	movq	bio_err(%rip), %rdi
	movl	%eax, %edx
	leaq	.LC159(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L816
.L927:
	call	opt_arg@PLT
	movl	$10, %edx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	strtol@PLT
	movl	%eax, -544(%rbp)
	jmp	.L816
.L929:
	movq	-128(%rbp), %rdi
	movl	$1155, %edx
	leaq	.LC31(%rip), %rsi
	call	CRYPTO_free@PLT
	call	opt_arg@PLT
	movl	$1155, %edx
	leaq	.LC31(%rip), %rsi
	movq	%rax, %rdi
	call	CRYPTO_strdup@PLT
	movq	-120(%rbp), %rdi
	movl	$1156, %edx
	leaq	.LC31(%rip), %rsi
	movq	%rax, -128(%rbp)
	call	CRYPTO_free@PLT
	movq	$0, -120(%rbp)
	movl	$1, -272(%rbp)
	jmp	.L816
.L930:
	movl	-272(%rbp), %ecx
	movl	$0, %eax
	movq	-120(%rbp), %rdi
	movl	$1128, %edx
	leaq	.LC31(%rip), %rsi
	cmpl	$1, %ecx
	cmovne	%ecx, %eax
	movl	%eax, -272(%rbp)
	call	CRYPTO_free@PLT
	movq	-128(%rbp), %rdi
	movl	$1129, %edx
	leaq	.LC31(%rip), %rsi
	movq	$0, -120(%rbp)
	call	CRYPTO_free@PLT
	movq	$0, -128(%rbp)
	call	opt_arg@PLT
	xorl	%esi, %esi
	leaq	-120(%rbp), %rdx
	movl	$1, %ecx
	movq	%rax, %rdi
	call	BIO_parse_hostserv@PLT
	testl	%eax, %eax
	jg	.L816
	movq	-120(%rbp), %rdx
	movq	bio_err(%rip), %rdi
	leaq	.LC157(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L1261
.L931:
	movl	-272(%rbp), %ecx
	movl	$0, %eax
	movq	-120(%rbp), %rdi
	movl	$1143, %edx
	leaq	.LC31(%rip), %rsi
	cmpl	$1, %ecx
	cmovne	%ecx, %eax
	movl	%eax, -272(%rbp)
	call	CRYPTO_free@PLT
	movq	-128(%rbp), %rdi
	movl	$1144, %edx
	leaq	.LC31(%rip), %rsi
	movq	$0, -120(%rbp)
	call	CRYPTO_free@PLT
	movq	$0, -128(%rbp)
	call	opt_arg@PLT
	leaq	-120(%rbp), %rdx
	leaq	-128(%rbp), %rsi
	movl	$1, %ecx
	movq	%rax, %rdi
	call	BIO_parse_hostserv@PLT
	testl	%eax, %eax
	jg	.L816
	movq	-120(%rbp), %rdx
	movq	bio_err(%rip), %rdi
	leaq	.LC158(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L1261
.L932:
	cmpl	$1, -272(%rbp)
	je	.L1302
	movl	$10, -272(%rbp)
	jmp	.L816
.L933:
	cmpl	$1, -272(%rbp)
	je	.L1303
	movl	$2, -272(%rbp)
	jmp	.L816
.L934:
	call	opt_arg@PLT
	movl	$1, %esi
	movq	%rax, %rdi
	call	setup_engine@PLT
	movq	%rax, -232(%rbp)
	jmp	.L816
.L935:
	leaq	s_server_options(%rip), %rdi
	xorl	%ebx, %ebx
	xorl	%r12d, %r12d
	call	opt_help@PLT
	movq	$0, -224(%rbp)
	jmp	.L1257
	.p2align 4,,10
	.p2align 3
.L819:
	movq	bio_err(%rip), %rdi
	leaq	.LC154(%rip), %rsi
	xorl	%eax, %eax
	xorl	%ebx, %ebx
	call	BIO_printf@PLT
	movq	$0, -224(%rbp)
	jmp	.L1257
.L1268:
	addl	$1, %r15d
	jmp	.L827
	.p2align 4,,10
	.p2align 3
.L938:
	movl	%eax, %edi
	call	opt_rand@PLT
	testl	%eax, %eax
	jne	.L816
	jmp	.L1261
	.p2align 4,,10
	.p2align 3
.L1271:
	cmpl	$2, -288(%rbp)
	leaq	.LC168(%rip), %rsi
	je	.L1264
	movl	dtlslisten(%rip), %edx
	testl	%edx, %edx
	je	.L1253
.L1075:
	leaq	.LC169(%rip), %rsi
	jmp	.L1264
.L1302:
	movq	-128(%rbp), %rdi
	movl	$1111, %edx
	leaq	.LC31(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-120(%rbp), %rdi
	movl	$1112, %edx
	leaq	.LC31(%rip), %rsi
	movq	$0, -128(%rbp)
	call	CRYPTO_free@PLT
	movq	$0, -120(%rbp)
	movl	$10, -272(%rbp)
	jmp	.L816
.L1303:
	movq	-128(%rbp), %rdi
	movl	$1100, %edx
	leaq	.LC31(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-120(%rbp), %rdi
	movl	$1101, %edx
	leaq	.LC31(%rip), %rsi
	movq	$0, -128(%rbp)
	call	CRYPTO_free@PLT
	movq	$0, -120(%rbp)
	movl	$2, -272(%rbp)
	jmp	.L816
.L1300:
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, -216(%rbp)
	testq	%rax, %rax
	jne	.L945
	jmp	.L946
.L1301:
	movq	bio_err(%rip), %rdi
	leaq	.LC162(%rip), %rsi
	jmp	.L1263
	.p2align 4,,10
	.p2align 3
.L947:
	movq	bio_err(%rip), %rdi
	movq	%r8, %rdx
	leaq	.LC164(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L1261
.L954:
	cmpl	$1, -272(%rbp)
	sete	%dl
	cmpl	$1, -288(%rbp)
	movb	%dl, -557(%rbp)
	je	.L956
	leaq	.LC171(%rip), %rsi
	testb	%dl, %dl
	jne	.L1264
	jmp	.L956
.L1104:
	leaq	.LC172(%rip), %rsi
	jmp	.L1264
.L1272:
	movq	-232(%rbp), %r8
	movq	-136(%rbp), %rcx
	xorl	%edx, %edx
	leaq	.LC174(%rip), %r9
	movl	-188(%rbp), %esi
	movq	-224(%rbp), %rdi
	call	load_key@PLT
	movq	%rax, -256(%rbp)
	testq	%rax, %rax
	je	.L1304
	movl	-192(%rbp), %esi
	movq	-328(%rbp), %rdi
	leaq	.LC175(%rip), %rdx
	call	load_cert@PLT
	movq	%rax, -248(%rbp)
	testq	%rax, %rax
	je	.L1305
	cmpq	$0, -408(%rbp)
	je	.L967
	movq	-408(%rbp), %rdi
	xorl	%ecx, %ecx
	leaq	-160(%rbp), %rsi
	leaq	.LC176(%rip), %r8
	movl	$32773, %edx
	call	load_certs@PLT
	testl	%eax, %eax
	je	.L1306
.L967:
	movq	-80(%rbp), %rax
	xorl	%ebx, %ebx
	movq	%rax, -224(%rbp)
	testq	%rax, %rax
	je	.L962
	movq	-232(%rbp), %r8
	movq	-136(%rbp), %rcx
	xorl	%edx, %edx
	leaq	.LC177(%rip), %r9
	movl	-188(%rbp), %esi
	movq	-320(%rbp), %rdi
	call	load_key@PLT
	movq	%rax, -224(%rbp)
	testq	%rax, %rax
	je	.L1307
	movl	-192(%rbp), %esi
	movq	-344(%rbp), %rdi
	leaq	.LC178(%rip), %rdx
	call	load_cert@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	jne	.L962
	movq	bio_err(%rip), %rdi
	xorl	%r15d, %r15d
	movl	$1, %r12d
	call	ERR_print_errors@PLT
	movq	$0, -200(%rbp)
	movq	$0, -208(%rbp)
	jmp	.L815
.L1256:
	leaq	.LC201(%rip), %rsi
.L1259:
	movq	bio_err(%rip), %rdi
	call	BIO_printf@PLT
.L1260:
	movq	bio_err(%rip), %rdi
	movl	$1, %r12d
	call	ERR_print_errors@PLT
	jmp	.L815
.L1092:
	movq	$0, -200(%rbp)
	movl	$1, %r12d
	movq	$0, -208(%rbp)
	jmp	.L815
.L977:
	movq	bio_err(%rip), %rdi
	leaq	.LC180(%rip), %rsi
	movl	$1, %r12d
	call	BIO_puts@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	movq	%r15, %rdi
	xorl	%r15d, %r15d
	call	X509_CRL_free@PLT
	movq	$0, -200(%rbp)
	jmp	.L815
.L1093:
	xorl	%r15d, %r15d
	jmp	.L978
.L1276:
	movl	-308(%rbp), %esi
	movq	ctx(%rip), %rdi
	call	ssl_ctx_security_debug@PLT
	jmp	.L988
.L1275:
	movl	s_quiet(%rip), %ecx
	testl	%ecx, %ecx
	je	.L984
	movl	s_debug(%rip), %edx
	testl	%edx, %edx
	je	.L1308
.L984:
	movl	$32769, %edi
	call	dup_bio_out@PLT
	movq	%rax, bio_s_out(%rip)
	jmp	.L983
.L1304:
	movq	bio_err(%rip), %rdi
	xorl	%ebx, %ebx
	xorl	%r15d, %r15d
	movl	$1, %r12d
	call	ERR_print_errors@PLT
	movq	$0, -224(%rbp)
	movq	$0, -200(%rbp)
	movq	$0, -248(%rbp)
	movq	$0, -208(%rbp)
	jmp	.L815
.L1273:
	movq	bio_err(%rip), %rdi
	leaq	.LC179(%rip), %rsi
	movl	$1, %r12d
	call	BIO_puts@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	movq	$0, -200(%rbp)
	movq	$0, -208(%rbp)
	jmp	.L815
.L1274:
	movq	bio_err(%rip), %rdi
	movl	$1, %r12d
	call	ERR_print_errors@PLT
	movq	$0, -200(%rbp)
	jmp	.L815
.L990:
	movslq	-268(%rbp), %rdx
	movq	ctx(%rip), %rdi
	xorl	%ecx, %ecx
	movl	$123, %esi
	call	SSL_CTX_ctrl@PLT
	testq	%rax, %rax
	jne	.L993
.L1101:
	movl	$1, %r12d
	jmp	.L815
.L1014:
	cmpq	$0, ctx2(%rip)
	je	.L1032
	jmp	.L1018
.L1278:
	movslq	-284(%rbp), %rdx
	movq	ctx(%rip), %rdi
	xorl	%ecx, %ecx
	movl	$124, %esi
	call	SSL_CTX_ctrl@PLT
	testq	%rax, %rax
	jne	.L992
	jmp	.L1101
.L1281:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$44, %esi
	call	SSL_CTX_ctrl@PLT
	jmp	.L1001
.L1280:
	movq	apps_ssl_info_callback@GOTPCREL(%rip), %rsi
	call	SSL_CTX_set_info_callback@PLT
	movq	ctx(%rip), %rdi
	jmp	.L999
.L1284:
	movl	-372(%rbp), %edx
	xorl	%ecx, %ecx
	movl	$125, %esi
	call	SSL_CTX_ctrl@PLT
	testq	%rax, %rax
	je	.L1007
	movq	ctx(%rip), %rdi
	jmp	.L1006
.L1283:
	movl	-376(%rbp), %edx
	xorl	%ecx, %ecx
	movl	$52, %esi
	call	SSL_CTX_ctrl@PLT
	testq	%rax, %rax
	je	.L1005
	movq	ctx(%rip), %rdi
	jmp	.L1004
.L1308:
	call	BIO_s_null@PLT
	movq	%rax, %rdi
	call	BIO_new@PLT
	cmpl	$0, s_msg(%rip)
	movq	%rax, bio_s_out(%rip)
	je	.L983
	cmpq	$0, bio_s_msg(%rip)
	jne	.L983
	movl	$32769, %edi
	call	dup_bio_out@PLT
	movq	%rax, bio_s_msg(%rip)
	jmp	.L983
.L1277:
	movq	-456(%rbp), %rdx
	movq	bio_err(%rip), %rdi
	leaq	.LC183(%rip), %rsi
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	jmp	.L815
.L1307:
	movq	bio_err(%rip), %rdi
	xorl	%ebx, %ebx
	xorl	%r15d, %r15d
	movl	$1, %r12d
	call	ERR_print_errors@PLT
	movq	$0, -200(%rbp)
	movq	$0, -208(%rbp)
	jmp	.L815
.L1285:
	movl	-352(%rbp), %edx
	xorl	%ecx, %ecx
	movl	$126, %esi
	call	SSL_CTX_ctrl@PLT
	movq	ctx(%rip), %rdi
	testq	%rax, %rax
	jne	.L1008
	movl	-352(%rbp), %ecx
	movq	-336(%rbp), %rdx
	leaq	.LC189(%rip), %rsi
	movl	$1, %r12d
	movq	bio_err(%rip), %rdi
	call	BIO_printf@PLT
	jmp	.L815
.L1282:
	xorl	%ecx, %ecx
	movl	$256, %edx
	movl	$33, %esi
	call	SSL_CTX_ctrl@PLT
	movq	ctx(%rip), %rdi
	jmp	.L1003
.L1002:
	xorl	%ecx, %ecx
	movl	$128, %edx
	movl	$42, %esi
	call	SSL_CTX_ctrl@PLT
	jmp	.L1001
.L1305:
	movq	bio_err(%rip), %rdi
	xorl	%ebx, %ebx
	xorl	%r15d, %r15d
	movl	$1, %r12d
	call	ERR_print_errors@PLT
	movq	$0, -224(%rbp)
	movq	$0, -200(%rbp)
	movq	$0, -208(%rbp)
	jmp	.L815
.L1269:
	call	__stack_chk_fail@PLT
.L1266:
	leaq	.LC185(%rip), %rsi
	jmp	.L1259
.L1279:
	movq	bio_err(%rip), %rdi
	leaq	.LC184(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L996
.L1306:
	movq	$0, -224(%rbp)
	xorl	%ebx, %ebx
	movl	$1, %r12d
	xorl	%r15d, %r15d
	movq	$0, -200(%rbp)
	movq	$0, -208(%rbp)
	jmp	.L815
.L1265:
	leaq	.LC191(%rip), %rsi
	jmp	.L1259
.L1288:
	movq	bio_err(%rip), %rdi
	leaq	.LC184(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L1022
.L1291:
	movq	ctx2(%rip), %rdi
	xorl	%ecx, %ecx
	movl	$256, %edx
	movl	$33, %esi
	call	SSL_CTX_ctrl@PLT
	jmp	.L1029
.L1007:
	movl	-372(%rbp), %ecx
	movq	-336(%rbp), %rdx
	leaq	.LC188(%rip), %rsi
	xorl	%eax, %eax
	movq	bio_err(%rip), %rdi
	movl	$1, %r12d
	call	BIO_printf@PLT
	jmp	.L815
.L1262:
	movq	bio_err(%rip), %rdi
	leaq	.LC196(%rip), %rsi
	call	BIO_puts@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	movq	%r12, %rdi
	movl	$1, %r12d
	call	DH_free@PLT
	jmp	.L815
.L1037:
	leaq	.LC195(%rip), %rsi
	xorl	%eax, %eax
	xorl	%r12d, %r12d
	call	BIO_printf@PLT
	movq	bio_s_out(%rip), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	call	BIO_ctrl@PLT
	movq	ctx(%rip), %rdi
	xorl	%ecx, %ecx
	movl	$1, %edx
	movl	$118, %esi
	call	SSL_CTX_ctrl@PLT
	jmp	.L1039
.L1293:
	movq	ctx2(%rip), %rdi
	xorl	%ecx, %ecx
	movl	$1, %edx
	movl	$118, %esi
	call	SSL_CTX_ctrl@PLT
	jmp	.L1040
.L1292:
	movq	-344(%rbp), %rdi
	call	load_dh_param
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L1041
	movq	bio_s_out(%rip), %rdi
	leaq	.LC194(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_s_out(%rip), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	call	BIO_ctrl@PLT
	movq	%r12, %rdi
	movq	%r14, %r12
	call	DH_free@PLT
	jmp	.L1042
.L1286:
	movq	bio_err(%rip), %rdi
	leaq	.LC190(%rip), %rsi
	xorl	%eax, %eax
	movl	$1, %r12d
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	jmp	.L815
.L1005:
	movl	-376(%rbp), %ecx
	movq	-336(%rbp), %rdx
	leaq	.LC187(%rip), %rsi
	xorl	%eax, %eax
	movq	bio_err(%rip), %rdi
	movl	$1, %r12d
	call	BIO_printf@PLT
	jmp	.L815
.L1299:
	movq	ctx(%rip), %rdi
	leaq	cert_status_cb(%rip), %rdx
	movl	$63, %esi
	call	SSL_CTX_callback_ctrl@PLT
	movq	ctx(%rip), %rdi
	xorl	%edx, %edx
	leaq	tlscstatp(%rip), %rcx
	movl	$64, %esi
	call	SSL_CTX_ctrl@PLT
	movq	ctx2(%rip), %rdi
	testq	%rdi, %rdi
	je	.L1067
	leaq	cert_status_cb(%rip), %rdx
	movl	$63, %esi
	call	SSL_CTX_callback_ctrl@PLT
	movq	ctx2(%rip), %rdi
	xorl	%edx, %edx
	leaq	tlscstatp(%rip), %rcx
	movl	$64, %esi
	call	SSL_CTX_ctrl@PLT
	jmp	.L1067
.L1298:
	movq	-400(%rbp), %rdx
	movq	bio_err(%rip), %rdi
	movl	%eax, %ecx
	leaq	.LC202(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L815
.L1028:
	xorl	%ecx, %ecx
	movl	$128, %edx
	movl	$42, %esi
	call	SSL_CTX_ctrl@PLT
	jmp	.L1027
.L1290:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$44, %esi
	call	SSL_CTX_ctrl@PLT
	jmp	.L1027
.L1289:
	movq	apps_ssl_info_callback@GOTPCREL(%rip), %rsi
	movq	ctx2(%rip), %rdi
	call	SSL_CTX_set_info_callback@PLT
	jmp	.L1025
.L1287:
	movl	-308(%rbp), %esi
	movq	ctx2(%rip), %rdi
	call	ssl_ctx_security_debug@PLT
	jmp	.L1017
.L1061:
	cmpq	$0, -360(%rbp)
	je	.L1255
	movq	-360(%rbp), %r14
	movq	%r14, %rdi
	call	SSL_load_client_CA_file@PLT
	movq	ctx(%rip), %rdi
	movq	%rax, %rsi
	call	SSL_CTX_set_client_CA_list@PLT
	cmpq	$0, ctx2(%rip)
	je	.L1255
	movq	%r14, %rdi
	call	SSL_load_client_CA_file@PLT
	movq	ctx2(%rip), %rdi
	movq	%rax, %rsi
	call	SSL_CTX_set_client_CA_list@PLT
.L1255:
	movl	$1, %r12d
	jmp	.L1063
.L1297:
	movq	-392(%rbp), %rdx
	leaq	.LC200(%rip), %rsi
.L1258:
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
	movl	$1, %r12d
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	jmp	.L815
.L1296:
	movq	-392(%rbp), %rdx
	leaq	.LC199(%rip), %rsi
	jmp	.L1258
.L1054:
	cmpq	$0, psk_key(%rip)
	jne	.L1056
	cmpq	$0, psksess(%rip)
	je	.L1057
	jmp	.L1056
.L1294:
	movq	ctx(%rip), %rdi
	leaq	not_resumable_sess_cb(%rip), %rsi
	call	SSL_CTX_set_not_resumable_session_callback@PLT
	movq	ctx2(%rip), %rdi
	testq	%rdi, %rdi
	je	.L1050
	leaq	not_resumable_sess_cb(%rip), %rsi
	call	SSL_CTX_set_not_resumable_session_callback@PLT
	jmp	.L1050
.L1035:
	cmpq	$0, -328(%rbp)
	movq	bio_s_out(%rip), %rdi
	je	.L1037
	movq	-328(%rbp), %rdi
	jmp	.L1254
.L1295:
	movq	bio_s_out(%rip), %rdi
	leaq	.LC197(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L1052
	.cfi_endproc
.LFE1641:
	.size	s_server_main, .-s_server_main
	.local	first
	.comm	first,8,8
	.globl	s_server_options
	.section	.rodata.str1.1
.LC203:
	.string	"help"
.LC204:
	.string	"Display this summary"
.LC205:
	.string	"port"
	.section	.rodata.str1.8
	.align 8
.LC206:
	.string	"TCP/IP port to listen on for connections (default is 4433)"
	.section	.rodata.str1.1
.LC207:
	.string	"accept"
	.section	.rodata.str1.8
	.align 8
.LC208:
	.string	"TCP/IP optional host and port to listen on for connections (default is *:4433)"
	.section	.rodata.str1.1
.LC209:
	.string	"unix"
	.section	.rodata.str1.8
	.align 8
.LC210:
	.string	"Unix domain socket to accept on"
	.section	.rodata.str1.1
.LC211:
	.string	"4"
.LC212:
	.string	"Use IPv4 only"
.LC213:
	.string	"6"
.LC214:
	.string	"Use IPv6 only"
.LC215:
	.string	"unlink"
	.section	.rodata.str1.8
	.align 8
.LC216:
	.string	"For -unix, unlink existing socket first"
	.section	.rodata.str1.1
.LC217:
	.string	"context"
.LC218:
	.string	"Set session ID context"
.LC219:
	.string	"verify"
	.section	.rodata.str1.8
	.align 8
.LC220:
	.string	"Turn on peer certificate verification"
	.section	.rodata.str1.1
.LC221:
	.string	"Verify"
	.section	.rodata.str1.8
	.align 8
.LC222:
	.string	"Turn on peer certificate verification, must have a cert"
	.section	.rodata.str1.1
.LC223:
	.string	"cert"
	.section	.rodata.str1.8
	.align 8
.LC224:
	.string	"Certificate file to use; default is server.pem"
	.section	.rodata.str1.1
.LC225:
	.string	"nameopt"
	.section	.rodata.str1.8
	.align 8
.LC226:
	.string	"Various certificate name options"
	.section	.rodata.str1.1
.LC227:
	.string	"naccept"
	.section	.rodata.str1.8
	.align 8
.LC228:
	.string	"Terminate after #num connections"
	.section	.rodata.str1.1
.LC229:
	.string	"serverinfo"
	.section	.rodata.str1.8
	.align 8
.LC230:
	.string	"PEM serverinfo file for certificate"
	.section	.rodata.str1.1
.LC231:
	.string	"certform"
	.section	.rodata.str1.8
	.align 8
.LC232:
	.string	"Certificate format (PEM or DER) PEM default"
	.section	.rodata.str1.1
.LC233:
	.string	"key"
	.section	.rodata.str1.8
	.align 8
.LC234:
	.string	"Private Key if not in -cert; default is server.pem"
	.section	.rodata.str1.1
.LC235:
	.string	"keyform"
	.section	.rodata.str1.8
	.align 8
.LC236:
	.string	"Key format (PEM, DER or ENGINE) PEM default"
	.section	.rodata.str1.1
.LC237:
	.string	"pass"
	.section	.rodata.str1.8
	.align 8
.LC238:
	.string	"Private key file pass phrase source"
	.section	.rodata.str1.1
.LC239:
	.string	"dcert"
	.section	.rodata.str1.8
	.align 8
.LC240:
	.string	"Second certificate file to use (usually for DSA)"
	.section	.rodata.str1.1
.LC241:
	.string	"dhparam"
.LC242:
	.string	"DH parameters file to use"
.LC243:
	.string	"dcertform"
	.section	.rodata.str1.8
	.align 8
.LC244:
	.string	"Second certificate format (PEM or DER) PEM default"
	.section	.rodata.str1.1
.LC245:
	.string	"dkey"
	.section	.rodata.str1.8
	.align 8
.LC246:
	.string	"Second private key file to use (usually for DSA)"
	.section	.rodata.str1.1
.LC247:
	.string	"dkeyform"
	.section	.rodata.str1.8
	.align 8
.LC248:
	.string	"Second key format (PEM, DER or ENGINE) PEM default"
	.section	.rodata.str1.1
.LC249:
	.string	"dpass"
	.section	.rodata.str1.8
	.align 8
.LC250:
	.string	"Second private key file pass phrase source"
	.section	.rodata.str1.1
.LC251:
	.string	"nbio_test"
	.section	.rodata.str1.8
	.align 8
.LC252:
	.string	"Test with the non-blocking test bio"
	.section	.rodata.str1.1
.LC253:
	.string	"crlf"
	.section	.rodata.str1.8
	.align 8
.LC254:
	.string	"Convert LF from terminal into CRLF"
	.section	.rodata.str1.1
.LC255:
	.string	"debug"
.LC256:
	.string	"Print more output"
.LC257:
	.string	"msg"
.LC258:
	.string	"Show protocol messages"
.LC259:
	.string	"msgfile"
	.section	.rodata.str1.8
	.align 8
.LC260:
	.string	"File to send output of -msg or -trace, instead of stdout"
	.section	.rodata.str1.1
.LC261:
	.string	"state"
.LC262:
	.string	"Print the SSL states"
.LC263:
	.string	"CAfile"
.LC264:
	.string	"PEM format file of CA's"
.LC265:
	.string	"CApath"
.LC266:
	.string	"PEM format directory of CA's"
.LC267:
	.string	"no-CAfile"
	.section	.rodata.str1.8
	.align 8
.LC268:
	.string	"Do not load the default certificates file"
	.section	.rodata.str1.1
.LC269:
	.string	"no-CApath"
	.section	.rodata.str1.8
	.align 8
.LC270:
	.string	"Do not load certificates from the default certificates directory"
	.section	.rodata.str1.1
.LC271:
	.string	"nocert"
	.section	.rodata.str1.8
	.align 8
.LC272:
	.string	"Don't use any certificates (Anon-DH)"
	.section	.rodata.str1.1
.LC273:
	.string	"quiet"
.LC274:
	.string	"No server output"
.LC275:
	.string	"no_resume_ephemeral"
	.section	.rodata.str1.8
	.align 8
.LC276:
	.string	"Disable caching and tickets if ephemeral (EC)DH is used"
	.section	.rodata.str1.1
.LC277:
	.string	"www"
	.section	.rodata.str1.8
	.align 8
.LC278:
	.string	"Respond to a 'GET /' with a status page"
	.section	.rodata.str1.1
.LC279:
	.string	"WWW"
	.section	.rodata.str1.8
	.align 8
.LC280:
	.string	"Respond to a 'GET with the file ./path"
	.section	.rodata.str1.1
.LC281:
	.string	"servername"
	.section	.rodata.str1.8
	.align 8
.LC282:
	.string	"Servername for HostName TLS extension"
	.section	.rodata.str1.1
.LC283:
	.string	"servername_fatal"
	.section	.rodata.str1.8
	.align 8
.LC284:
	.string	"mismatch send fatal alert (default warning alert)"
	.section	.rodata.str1.1
.LC285:
	.string	"cert2"
	.section	.rodata.str1.8
	.align 8
.LC286:
	.string	"Certificate file to use for servername; default isserver2.pem"
	.section	.rodata.str1.1
.LC287:
	.string	"key2"
	.section	.rodata.str1.8
	.align 8
.LC288:
	.string	"-Private Key file to use for servername if not in -cert2"
	.section	.rodata.str1.1
.LC289:
	.string	"tlsextdebug"
	.section	.rodata.str1.8
	.align 8
.LC290:
	.string	"Hex dump of all TLS extensions received"
	.section	.rodata.str1.1
.LC291:
	.string	"HTTP"
	.section	.rodata.str1.8
	.align 8
.LC292:
	.string	"Like -WWW but ./path includes HTTP headers"
	.section	.rodata.str1.1
.LC293:
	.string	"id_prefix"
	.section	.rodata.str1.8
	.align 8
.LC294:
	.string	"Generate SSL/TLS session IDs prefixed by arg"
	.section	.rodata.str1.1
.LC295:
	.string	"rand"
	.section	.rodata.str1.8
	.align 8
.LC296:
	.string	"Load the file(s) into the random number generator"
	.section	.rodata.str1.1
.LC297:
	.string	"writerand"
	.section	.rodata.str1.8
	.align 8
.LC298:
	.string	"Write random data to the specified file"
	.section	.rodata.str1.1
.LC299:
	.string	"keymatexport"
	.section	.rodata.str1.8
	.align 8
.LC300:
	.string	"Export keying material using label"
	.section	.rodata.str1.1
.LC301:
	.string	"keymatexportlen"
	.section	.rodata.str1.8
	.align 8
.LC302:
	.string	"Export len bytes of keying material (default 20)"
	.section	.rodata.str1.1
.LC303:
	.string	"CRL"
.LC304:
	.string	"CRL file to use"
.LC305:
	.string	"crl_download"
	.section	.rodata.str1.8
	.align 8
.LC306:
	.string	"Download CRL from distribution points"
	.section	.rodata.str1.1
.LC307:
	.string	"cert_chain"
	.section	.rodata.str1.8
	.align 8
.LC308:
	.string	"certificate chain file in PEM format"
	.section	.rodata.str1.1
.LC309:
	.string	"dcert_chain"
	.section	.rodata.str1.8
	.align 8
.LC310:
	.string	"second certificate chain file in PEM format"
	.section	.rodata.str1.1
.LC311:
	.string	"chainCApath"
	.section	.rodata.str1.8
	.align 8
.LC312:
	.string	"use dir as certificate store path to build CA certificate chain"
	.section	.rodata.str1.1
.LC313:
	.string	"verifyCApath"
	.section	.rodata.str1.8
	.align 8
.LC314:
	.string	"use dir as certificate store path to verify CA certificate"
	.section	.rodata.str1.1
.LC315:
	.string	"no_cache"
.LC316:
	.string	"Disable session cache"
.LC317:
	.string	"ext_cache"
	.section	.rodata.str1.8
	.align 8
.LC318:
	.string	"Disable internal cache, setup and use external cache"
	.section	.rodata.str1.1
.LC319:
	.string	"CRLform"
	.section	.rodata.str1.8
	.align 8
.LC320:
	.string	"CRL format (PEM or DER) PEM is default"
	.section	.rodata.str1.1
.LC321:
	.string	"verify_return_error"
	.section	.rodata.str1.8
	.align 8
.LC322:
	.string	"Close connection on verification error"
	.section	.rodata.str1.1
.LC323:
	.string	"verify_quiet"
	.section	.rodata.str1.8
	.align 8
.LC324:
	.string	"No verify output except verify errors"
	.section	.rodata.str1.1
.LC325:
	.string	"build_chain"
.LC326:
	.string	"Build certificate chain"
.LC327:
	.string	"chainCAfile"
	.section	.rodata.str1.8
	.align 8
.LC328:
	.string	"CA file for certificate chain (PEM format)"
	.section	.rodata.str1.1
.LC329:
	.string	"verifyCAfile"
	.section	.rodata.str1.8
	.align 8
.LC330:
	.string	"CA file for certificate verification (PEM format)"
	.section	.rodata.str1.1
.LC331:
	.string	"ign_eof"
	.section	.rodata.str1.8
	.align 8
.LC332:
	.string	"ignore input eof (default when -quiet)"
	.section	.rodata.str1.1
.LC333:
	.string	"no_ign_eof"
.LC334:
	.string	"Do not ignore input eof"
.LC335:
	.string	"status"
	.section	.rodata.str1.8
	.align 8
.LC336:
	.string	"Request certificate status from server"
	.section	.rodata.str1.1
.LC337:
	.string	"status_verbose"
	.section	.rodata.str1.8
	.align 8
.LC338:
	.string	"Print more output in certificate status callback"
	.section	.rodata.str1.1
.LC339:
	.string	"status_timeout"
	.section	.rodata.str1.8
	.align 8
.LC340:
	.string	"Status request responder timeout"
	.section	.rodata.str1.1
.LC341:
	.string	"status_url"
.LC342:
	.string	"Status request fallback URL"
.LC343:
	.string	"status_file"
	.section	.rodata.str1.8
	.align 8
.LC344:
	.string	"File containing DER encoded OCSP Response"
	.section	.rodata.str1.1
.LC345:
	.string	"trace"
.LC346:
	.string	"trace protocol messages"
.LC347:
	.string	"security_debug"
	.section	.rodata.str1.8
	.align 8
.LC348:
	.string	"Print output from SSL/TLS security framework"
	.section	.rodata.str1.1
.LC349:
	.string	"security_debug_verbose"
	.section	.rodata.str1.8
	.align 8
.LC350:
	.string	"Print more output from SSL/TLS security framework"
	.section	.rodata.str1.1
.LC351:
	.string	"brief"
	.section	.rodata.str1.8
	.align 8
.LC352:
	.string	"Restrict output to brief summary of connection parameters"
	.section	.rodata.str1.1
.LC353:
	.string	"rev"
	.section	.rodata.str1.8
	.align 8
.LC354:
	.string	"act as a simple test server which just sends back with the received text reversed"
	.section	.rodata.str1.1
.LC355:
	.string	"async"
.LC356:
	.string	"Operate in asynchronous mode"
.LC357:
	.string	"ssl_config"
	.section	.rodata.str1.8
	.align 8
.LC358:
	.string	"Configure SSL_CTX using the configuration 'val'"
	.section	.rodata.str1.1
.LC359:
	.string	"max_send_frag"
.LC360:
	.string	"Maximum Size of send frames "
.LC361:
	.string	"split_send_frag"
	.section	.rodata.str1.8
	.align 8
.LC362:
	.string	"Size used to split data for encrypt pipelines"
	.section	.rodata.str1.1
.LC363:
	.string	"max_pipelines"
	.section	.rodata.str1.8
	.align 8
.LC364:
	.string	"Maximum number of encrypt/decrypt pipelines to be used"
	.section	.rodata.str1.1
.LC365:
	.string	"read_buf"
	.section	.rodata.str1.8
	.align 8
.LC366:
	.string	"Default read buffer size to be used for connections"
	.section	.rodata.str1.1
.LC367:
	.string	"no_ssl3"
.LC368:
	.string	"Just disable SSLv3"
.LC369:
	.string	"no_tls1"
.LC370:
	.string	"Just disable TLSv1"
.LC371:
	.string	"no_tls1_1"
.LC372:
	.string	"Just disable TLSv1.1"
.LC373:
	.string	"no_tls1_2"
.LC374:
	.string	"Just disable TLSv1.2"
.LC375:
	.string	"no_tls1_3"
.LC376:
	.string	"Just disable TLSv1.3"
.LC377:
	.string	"bugs"
.LC378:
	.string	"Turn on SSL bug compatibility"
.LC379:
	.string	"no_comp"
	.section	.rodata.str1.8
	.align 8
.LC380:
	.string	"Disable SSL/TLS compression (default)"
	.section	.rodata.str1.1
.LC381:
	.string	"comp"
.LC382:
	.string	"Use SSL/TLS-level compression"
.LC383:
	.string	"no_ticket"
	.section	.rodata.str1.8
	.align 8
.LC384:
	.string	"Disable use of TLS session tickets"
	.section	.rodata.str1.1
.LC385:
	.string	"serverpref"
	.section	.rodata.str1.8
	.align 8
.LC386:
	.string	"Use server's cipher preferences"
	.section	.rodata.str1.1
.LC387:
	.string	"legacy_renegotiation"
	.section	.rodata.str1.8
	.align 8
.LC388:
	.string	"Enable use of legacy renegotiation (dangerous)"
	.section	.rodata.str1.1
.LC389:
	.string	"no_renegotiation"
.LC390:
	.string	"Disable all renegotiation."
.LC391:
	.string	"legacy_server_connect"
	.section	.rodata.str1.8
	.align 8
.LC392:
	.string	"Allow initial connection to servers that don't support RI"
	.section	.rodata.str1.1
.LC393:
	.string	"no_resumption_on_reneg"
	.section	.rodata.str1.8
	.align 8
.LC394:
	.string	"Disallow session resumption on renegotiation"
	.section	.rodata.str1.1
.LC395:
	.string	"no_legacy_server_connect"
	.section	.rodata.str1.8
	.align 8
.LC396:
	.string	"Disallow initial connection to servers that don't support RI"
	.section	.rodata.str1.1
.LC397:
	.string	"allow_no_dhe_kex"
	.section	.rodata.str1.8
	.align 8
.LC398:
	.string	"In TLSv1.3 allow non-(ec)dhe based key exchange on resumption"
	.section	.rodata.str1.1
.LC399:
	.string	"prioritize_chacha"
	.section	.rodata.str1.8
	.align 8
.LC400:
	.string	"Prioritize ChaCha ciphers when preferred by clients"
	.section	.rodata.str1.1
.LC401:
	.string	"strict"
	.section	.rodata.str1.8
	.align 8
.LC402:
	.string	"Enforce strict certificate checks as per TLS standard"
	.section	.rodata.str1.1
.LC403:
	.string	"sigalgs"
	.section	.rodata.str1.8
	.align 8
.LC404:
	.string	"Signature algorithms to support (colon-separated list)"
	.section	.rodata.str1.1
.LC405:
	.string	"client_sigalgs"
	.section	.rodata.str1.8
	.align 8
.LC406:
	.string	"Signature algorithms to support for client certificate authentication (colon-separated list)"
	.section	.rodata.str1.1
.LC407:
	.string	"groups"
	.section	.rodata.str1.8
	.align 8
.LC408:
	.string	"Groups to advertise (colon-separated list)"
	.section	.rodata.str1.1
.LC409:
	.string	"curves"
.LC410:
	.string	"named_curve"
	.section	.rodata.str1.8
	.align 8
.LC411:
	.string	"Elliptic curve used for ECDHE (server-side only)"
	.section	.rodata.str1.1
.LC412:
	.string	"cipher"
	.section	.rodata.str1.8
	.align 8
.LC413:
	.string	"Specify TLSv1.2 and below cipher list to be used"
	.section	.rodata.str1.1
.LC414:
	.string	"ciphersuites"
	.section	.rodata.str1.8
	.align 8
.LC415:
	.string	"Specify TLSv1.3 ciphersuites to be used"
	.section	.rodata.str1.1
.LC416:
	.string	"min_protocol"
	.section	.rodata.str1.8
	.align 8
.LC417:
	.string	"Specify the minimum protocol version to be used"
	.section	.rodata.str1.1
.LC418:
	.string	"max_protocol"
	.section	.rodata.str1.8
	.align 8
.LC419:
	.string	"Specify the maximum protocol version to be used"
	.section	.rodata.str1.1
.LC420:
	.string	"record_padding"
	.section	.rodata.str1.8
	.align 8
.LC421:
	.string	"Block size to pad TLS 1.3 records to."
	.section	.rodata.str1.1
.LC422:
	.string	"debug_broken_protocol"
	.section	.rodata.str1.8
	.align 8
.LC423:
	.string	"Perform all sorts of protocol violations for testing purposes"
	.section	.rodata.str1.1
.LC424:
	.string	"no_middlebox"
	.section	.rodata.str1.8
	.align 8
.LC425:
	.string	"Disable TLSv1.3 middlebox compat mode"
	.section	.rodata.str1.1
.LC426:
	.string	"policy"
	.section	.rodata.str1.8
	.align 8
.LC427:
	.string	"adds policy to the acceptable policy set"
	.section	.rodata.str1.1
.LC428:
	.string	"purpose"
.LC429:
	.string	"certificate chain purpose"
.LC430:
	.string	"verify_name"
.LC431:
	.string	"verification policy name"
.LC432:
	.string	"verify_depth"
.LC433:
	.string	"chain depth limit"
.LC434:
	.string	"auth_level"
	.section	.rodata.str1.8
	.align 8
.LC435:
	.string	"chain authentication security level"
	.section	.rodata.str1.1
.LC436:
	.string	"attime"
.LC437:
	.string	"verification epoch time"
.LC438:
	.string	"verify_hostname"
.LC439:
	.string	"expected peer hostname"
.LC440:
	.string	"verify_email"
.LC441:
	.string	"expected peer email"
.LC442:
	.string	"verify_ip"
.LC443:
	.string	"expected peer IP address"
.LC444:
	.string	"ignore_critical"
	.section	.rodata.str1.8
	.align 8
.LC445:
	.string	"permit unhandled critical extensions"
	.section	.rodata.str1.1
.LC446:
	.string	"issuer_checks"
.LC447:
	.string	"(deprecated)"
.LC448:
	.string	"crl_check"
	.section	.rodata.str1.8
	.align 8
.LC449:
	.string	"check leaf certificate revocation"
	.section	.rodata.str1.1
.LC450:
	.string	"crl_check_all"
.LC451:
	.string	"check full chain revocation"
.LC452:
	.string	"policy_check"
.LC453:
	.string	"perform rfc5280 policy checks"
.LC454:
	.string	"explicit_policy"
	.section	.rodata.str1.8
	.align 8
.LC455:
	.string	"set policy variable require-explicit-policy"
	.section	.rodata.str1.1
.LC456:
	.string	"inhibit_any"
	.section	.rodata.str1.8
	.align 8
.LC457:
	.string	"set policy variable inhibit-any-policy"
	.section	.rodata.str1.1
.LC458:
	.string	"inhibit_map"
	.section	.rodata.str1.8
	.align 8
.LC459:
	.string	"set policy variable inhibit-policy-mapping"
	.section	.rodata.str1.1
.LC460:
	.string	"x509_strict"
	.section	.rodata.str1.8
	.align 8
.LC461:
	.string	"disable certificate compatibility work-arounds"
	.section	.rodata.str1.1
.LC462:
	.string	"extended_crl"
.LC463:
	.string	"enable extended CRL features"
.LC464:
	.string	"use_deltas"
.LC465:
	.string	"use delta CRLs"
.LC466:
	.string	"policy_print"
	.section	.rodata.str1.8
	.align 8
.LC467:
	.string	"print policy processing diagnostics"
	.section	.rodata.str1.1
.LC468:
	.string	"check_ss_sig"
.LC469:
	.string	"check root CA self-signatures"
.LC470:
	.string	"trusted_first"
	.section	.rodata.str1.8
	.align 8
.LC471:
	.string	"search trust store first (default)"
	.section	.rodata.str1.1
.LC472:
	.string	"suiteB_128_only"
.LC473:
	.string	"Suite B 128-bit-only mode"
.LC474:
	.string	"suiteB_128"
	.section	.rodata.str1.8
	.align 8
.LC475:
	.string	"Suite B 128-bit mode allowing 192-bit algorithms"
	.section	.rodata.str1.1
.LC476:
	.string	"suiteB_192"
.LC477:
	.string	"Suite B 192-bit-only mode"
.LC478:
	.string	"partial_chain"
	.section	.rodata.str1.8
	.align 8
.LC479:
	.string	"accept chains anchored by intermediate trust-store CAs"
	.section	.rodata.str1.1
.LC480:
	.string	"no_alt_chains"
.LC481:
	.string	"no_check_time"
	.section	.rodata.str1.8
	.align 8
.LC482:
	.string	"ignore certificate validity time"
	.section	.rodata.str1.1
.LC483:
	.string	"allow_proxy_certs"
	.section	.rodata.str1.8
	.align 8
.LC484:
	.string	"allow the use of proxy certificates"
	.section	.rodata.str1.1
.LC485:
	.string	"xkey"
.LC486:
	.string	"key for Extended certificates"
.LC487:
	.string	"xcert"
	.section	.rodata.str1.8
	.align 8
.LC488:
	.string	"cert for Extended certificates"
	.section	.rodata.str1.1
.LC489:
	.string	"xchain"
	.section	.rodata.str1.8
	.align 8
.LC490:
	.string	"chain for Extended certificates"
	.section	.rodata.str1.1
.LC491:
	.string	"xchain_build"
	.section	.rodata.str1.8
	.align 8
.LC492:
	.string	"build certificate chain for the extended certificates"
	.section	.rodata.str1.1
.LC493:
	.string	"xcertform"
	.section	.rodata.str1.8
	.align 8
.LC494:
	.string	"format of Extended certificate (PEM or DER) PEM default "
	.section	.rodata.str1.1
.LC495:
	.string	"xkeyform"
	.section	.rodata.str1.8
	.align 8
.LC496:
	.string	"format of Extended certificate's key (PEM or DER) PEM default"
	.section	.rodata.str1.1
.LC497:
	.string	"nbio"
.LC498:
	.string	"Use non-blocking IO"
.LC499:
	.string	"psk_identity"
.LC500:
	.string	"PSK identity to expect"
.LC501:
	.string	"psk_hint"
.LC502:
	.string	"PSK identity hint to use"
.LC503:
	.string	"psk"
.LC504:
	.string	"PSK in hex (without 0x)"
.LC505:
	.string	"psk_session"
	.section	.rodata.str1.8
	.align 8
.LC506:
	.string	"File to read PSK SSL session from"
	.section	.rodata.str1.1
.LC507:
	.string	"srpvfile"
.LC508:
	.string	"The verifier file for SRP"
.LC509:
	.string	"srpuserseed"
	.section	.rodata.str1.8
	.align 8
.LC510:
	.string	"A seed string for a default user salt"
	.section	.rodata.str1.1
.LC511:
	.string	"tls1"
.LC512:
	.string	"Just talk TLSv1"
.LC513:
	.string	"tls1_1"
.LC514:
	.string	"Just talk TLSv1.1"
.LC515:
	.string	"tls1_2"
.LC516:
	.string	"just talk TLSv1.2"
.LC517:
	.string	"tls1_3"
.LC518:
	.string	"just talk TLSv1.3"
.LC519:
	.string	"dtls"
.LC520:
	.string	"Use any DTLS version"
.LC521:
	.string	"timeout"
.LC522:
	.string	"Enable timeouts"
.LC523:
	.string	"mtu"
.LC524:
	.string	"Set link layer MTU"
.LC525:
	.string	"listen"
	.section	.rodata.str1.8
	.align 8
.LC526:
	.string	"Listen for a DTLS ClientHello with a cookie and then connect"
	.section	.rodata.str1.1
.LC527:
	.string	"stateless"
.LC528:
	.string	"Require TLSv1.3 cookies"
.LC529:
	.string	"dtls1"
.LC530:
	.string	"Just talk DTLSv1"
.LC531:
	.string	"dtls1_2"
.LC532:
	.string	"Just talk DTLSv1.2"
.LC533:
	.string	"no_dhe"
.LC534:
	.string	"Disable ephemeral DH"
.LC535:
	.string	"nextprotoneg"
	.section	.rodata.str1.8
	.align 8
.LC536:
	.string	"Set the advertised protocols for the NPN extension (comma-separated list)"
	.section	.rodata.str1.1
.LC537:
	.string	"use_srtp"
	.section	.rodata.str1.8
	.align 8
.LC538:
	.string	"Offer SRTP key management with a colon-separated profile list"
	.section	.rodata.str1.1
.LC539:
	.string	"alpn"
	.section	.rodata.str1.8
	.align 8
.LC540:
	.string	"Set the advertised protocols for the ALPN extension (comma-separated list)"
	.section	.rodata.str1.1
.LC541:
	.string	"engine"
	.section	.rodata.str1.8
	.align 8
.LC542:
	.string	"Use engine, possibly a hardware device"
	.section	.rodata.str1.1
.LC543:
	.string	"keylogfile"
.LC544:
	.string	"Write TLS secrets to file"
.LC545:
	.string	"max_early_data"
	.section	.rodata.str1.8
	.align 8
.LC546:
	.string	"The maximum number of bytes of early data as advertised in tickets"
	.section	.rodata.str1.1
.LC547:
	.string	"recv_max_early_data"
	.section	.rodata.str1.8
	.align 8
.LC548:
	.string	"The maximum number of bytes of early data (hard limit)"
	.section	.rodata.str1.1
.LC549:
	.string	"early_data"
.LC550:
	.string	"Attempt to read early data"
.LC551:
	.string	"num_tickets"
	.section	.rodata.str1.8
	.align 8
.LC552:
	.string	"The number of TLSv1.3 session tickets that a server will automatically  issue"
	.section	.rodata.str1.1
.LC553:
	.string	"anti_replay"
	.section	.rodata.str1.8
	.align 8
.LC554:
	.string	"Switch on anti-replay protection (default)"
	.section	.rodata.str1.1
.LC555:
	.string	"no_anti_replay"
	.section	.rodata.str1.8
	.align 8
.LC556:
	.string	"Switch off anti-replay protection"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	s_server_options, @object
	.size	s_server_options, 4296
s_server_options:
	.quad	.LC203
	.long	1
	.long	45
	.quad	.LC204
	.quad	.LC205
	.long	6
	.long	112
	.quad	.LC206
	.quad	.LC207
	.long	5
	.long	115
	.quad	.LC208
	.quad	.LC209
	.long	7
	.long	115
	.quad	.LC210
	.quad	.LC211
	.long	3
	.long	45
	.quad	.LC212
	.quad	.LC213
	.long	4
	.long	45
	.quad	.LC214
	.quad	.LC215
	.long	8
	.long	45
	.quad	.LC216
	.quad	.LC217
	.long	13
	.long	115
	.quad	.LC218
	.quad	.LC219
	.long	10
	.long	110
	.quad	.LC220
	.quad	.LC221
	.long	12
	.long	110
	.quad	.LC222
	.quad	.LC223
	.long	14
	.long	60
	.quad	.LC224
	.quad	.LC225
	.long	11
	.long	115
	.quad	.LC226
	.quad	.LC227
	.long	9
	.long	112
	.quad	.LC228
	.quad	.LC229
	.long	17
	.long	115
	.quad	.LC230
	.quad	.LC231
	.long	18
	.long	70
	.quad	.LC232
	.quad	.LC233
	.long	19
	.long	115
	.quad	.LC234
	.quad	.LC235
	.long	20
	.long	102
	.quad	.LC236
	.quad	.LC237
	.long	21
	.long	115
	.quad	.LC238
	.quad	.LC239
	.long	25
	.long	60
	.quad	.LC240
	.quad	.LC241
	.long	23
	.long	60
	.quad	.LC242
	.quad	.LC243
	.long	24
	.long	70
	.quad	.LC244
	.quad	.LC245
	.long	28
	.long	60
	.quad	.LC246
	.quad	.LC247
	.long	26
	.long	70
	.quad	.LC248
	.quad	.LC249
	.long	27
	.long	115
	.quad	.LC250
	.quad	.LC251
	.long	46
	.long	45
	.quad	.LC252
	.quad	.LC253
	.long	62
	.long	45
	.quad	.LC254
	.quad	.LC255
	.long	49
	.long	45
	.quad	.LC256
	.quad	.LC257
	.long	56
	.long	45
	.quad	.LC258
	.quad	.LC259
	.long	57
	.long	62
	.quad	.LC260
	.quad	.LC261
	.long	61
	.long	45
	.quad	.LC262
	.quad	.LC263
	.long	41
	.long	60
	.quad	.LC264
	.quad	.LC265
	.long	31
	.long	47
	.quad	.LC266
	.quad	.LC267
	.long	42
	.long	45
	.quad	.LC268
	.quad	.LC269
	.long	32
	.long	45
	.quad	.LC270
	.quad	.LC271
	.long	30
	.long	45
	.quad	.LC272
	.quad	.LC273
	.long	63
	.long	45
	.quad	.LC274
	.quad	.LC275
	.long	66
	.long	45
	.quad	.LC276
	.quad	.LC277
	.long	74
	.long	45
	.quad	.LC278
	.quad	.LC279
	.long	75
	.long	45
	.quad	.LC280
	.quad	.LC281
	.long	97
	.long	115
	.quad	.LC282
	.quad	.LC283
	.long	98
	.long	45
	.quad	.LC284
	.quad	.LC285
	.long	99
	.long	60
	.quad	.LC286
	.quad	.LC287
	.long	100
	.long	60
	.quad	.LC288
	.quad	.LC289
	.long	50
	.long	45
	.quad	.LC290
	.quad	.LC291
	.long	76
	.long	45
	.quad	.LC292
	.quad	.LC293
	.long	96
	.long	115
	.quad	.LC294
	.quad	.LC295
	.long	1501
	.long	115
	.quad	.LC296
	.quad	.LC297
	.long	1502
	.long	62
	.quad	.LC298
	.quad	.LC299
	.long	104
	.long	115
	.quad	.LC300
	.quad	.LC301
	.long	105
	.long	112
	.quad	.LC302
	.quad	.LC303
	.long	15
	.long	60
	.quad	.LC304
	.quad	.LC305
	.long	16
	.long	45
	.quad	.LC306
	.quad	.LC307
	.long	22
	.long	60
	.quad	.LC308
	.quad	.LC309
	.long	29
	.long	60
	.quad	.LC310
	.quad	.LC311
	.long	33
	.long	47
	.quad	.LC312
	.quad	.LC313
	.long	34
	.long	47
	.quad	.LC314
	.quad	.LC315
	.long	35
	.long	45
	.quad	.LC316
	.quad	.LC317
	.long	36
	.long	45
	.quad	.LC318
	.quad	.LC319
	.long	37
	.long	70
	.quad	.LC320
	.quad	.LC321
	.long	38
	.long	45
	.quad	.LC322
	.quad	.LC323
	.long	39
	.long	45
	.quad	.LC324
	.quad	.LC325
	.long	40
	.long	45
	.quad	.LC326
	.quad	.LC327
	.long	43
	.long	60
	.quad	.LC328
	.quad	.LC329
	.long	44
	.long	60
	.quad	.LC330
	.quad	.LC331
	.long	47
	.long	45
	.quad	.LC332
	.quad	.LC333
	.long	48
	.long	45
	.quad	.LC334
	.quad	.LC335
	.long	51
	.long	45
	.quad	.LC336
	.quad	.LC337
	.long	52
	.long	45
	.quad	.LC338
	.quad	.LC339
	.long	53
	.long	110
	.quad	.LC340
	.quad	.LC341
	.long	54
	.long	115
	.quad	.LC342
	.quad	.LC343
	.long	55
	.long	60
	.quad	.LC344
	.quad	.LC345
	.long	58
	.long	45
	.quad	.LC346
	.quad	.LC347
	.long	59
	.long	45
	.quad	.LC348
	.quad	.LC349
	.long	60
	.long	45
	.quad	.LC350
	.quad	.LC351
	.long	64
	.long	45
	.quad	.LC352
	.quad	.LC353
	.long	73
	.long	45
	.quad	.LC354
	.quad	.LC355
	.long	77
	.long	45
	.quad	.LC356
	.quad	.LC357
	.long	78
	.long	115
	.quad	.LC358
	.quad	.LC359
	.long	79
	.long	112
	.quad	.LC360
	.quad	.LC361
	.long	80
	.long	112
	.quad	.LC362
	.quad	.LC363
	.long	81
	.long	112
	.quad	.LC364
	.quad	.LC365
	.long	82
	.long	112
	.quad	.LC366
	.quad	.LC367
	.long	3001
	.long	45
	.quad	.LC368
	.quad	.LC369
	.long	3002
	.long	45
	.quad	.LC370
	.quad	.LC371
	.long	3003
	.long	45
	.quad	.LC372
	.quad	.LC373
	.long	3004
	.long	45
	.quad	.LC374
	.quad	.LC375
	.long	3005
	.long	45
	.quad	.LC376
	.quad	.LC377
	.long	3006
	.long	45
	.quad	.LC378
	.quad	.LC379
	.long	3007
	.long	45
	.quad	.LC380
	.quad	.LC381
	.long	3026
	.long	45
	.quad	.LC382
	.quad	.LC383
	.long	3008
	.long	45
	.quad	.LC384
	.quad	.LC385
	.long	3009
	.long	45
	.quad	.LC386
	.quad	.LC387
	.long	3010
	.long	45
	.quad	.LC388
	.quad	.LC389
	.long	3029
	.long	45
	.quad	.LC390
	.quad	.LC391
	.long	3011
	.long	45
	.quad	.LC392
	.quad	.LC393
	.long	3012
	.long	45
	.quad	.LC394
	.quad	.LC395
	.long	3013
	.long	45
	.quad	.LC396
	.quad	.LC397
	.long	3014
	.long	45
	.quad	.LC398
	.quad	.LC399
	.long	3015
	.long	45
	.quad	.LC400
	.quad	.LC401
	.long	3016
	.long	45
	.quad	.LC402
	.quad	.LC403
	.long	3017
	.long	115
	.quad	.LC404
	.quad	.LC405
	.long	3018
	.long	115
	.quad	.LC406
	.quad	.LC407
	.long	3019
	.long	115
	.quad	.LC408
	.quad	.LC409
	.long	3020
	.long	115
	.quad	.LC408
	.quad	.LC410
	.long	3021
	.long	115
	.quad	.LC411
	.quad	.LC412
	.long	3022
	.long	115
	.quad	.LC413
	.quad	.LC414
	.long	3023
	.long	115
	.quad	.LC415
	.quad	.LC416
	.long	3027
	.long	115
	.quad	.LC417
	.quad	.LC418
	.long	3028
	.long	115
	.quad	.LC419
	.quad	.LC420
	.long	3024
	.long	115
	.quad	.LC421
	.quad	.LC422
	.long	3025
	.long	45
	.quad	.LC423
	.quad	.LC424
	.long	3030
	.long	45
	.quad	.LC425
	.quad	.LC426
	.long	2001
	.long	115
	.quad	.LC427
	.quad	.LC428
	.long	2002
	.long	115
	.quad	.LC429
	.quad	.LC430
	.long	2003
	.long	115
	.quad	.LC431
	.quad	.LC432
	.long	2004
	.long	110
	.quad	.LC433
	.quad	.LC434
	.long	2029
	.long	110
	.quad	.LC435
	.quad	.LC436
	.long	2005
	.long	77
	.quad	.LC437
	.quad	.LC438
	.long	2006
	.long	115
	.quad	.LC439
	.quad	.LC440
	.long	2007
	.long	115
	.quad	.LC441
	.quad	.LC442
	.long	2008
	.long	115
	.quad	.LC443
	.quad	.LC444
	.long	2009
	.long	45
	.quad	.LC445
	.quad	.LC446
	.long	2010
	.long	45
	.quad	.LC447
	.quad	.LC448
	.long	2011
	.long	45
	.quad	.LC449
	.quad	.LC450
	.long	2012
	.long	45
	.quad	.LC451
	.quad	.LC452
	.long	2013
	.long	45
	.quad	.LC453
	.quad	.LC454
	.long	2014
	.long	45
	.quad	.LC455
	.quad	.LC456
	.long	2015
	.long	45
	.quad	.LC457
	.quad	.LC458
	.long	2016
	.long	45
	.quad	.LC459
	.quad	.LC460
	.long	2017
	.long	45
	.quad	.LC461
	.quad	.LC462
	.long	2018
	.long	45
	.quad	.LC463
	.quad	.LC464
	.long	2019
	.long	45
	.quad	.LC465
	.quad	.LC466
	.long	2020
	.long	45
	.quad	.LC467
	.quad	.LC468
	.long	2021
	.long	45
	.quad	.LC469
	.quad	.LC470
	.long	2022
	.long	45
	.quad	.LC471
	.quad	.LC472
	.long	2023
	.long	45
	.quad	.LC473
	.quad	.LC474
	.long	2024
	.long	45
	.quad	.LC475
	.quad	.LC476
	.long	2025
	.long	45
	.quad	.LC477
	.quad	.LC478
	.long	2026
	.long	45
	.quad	.LC479
	.quad	.LC480
	.long	2027
	.long	45
	.quad	.LC447
	.quad	.LC481
	.long	2028
	.long	45
	.quad	.LC482
	.quad	.LC483
	.long	2030
	.long	45
	.quad	.LC484
	.quad	.LC485
	.long	1001
	.long	60
	.quad	.LC486
	.quad	.LC487
	.long	1002
	.long	60
	.quad	.LC488
	.quad	.LC489
	.long	1003
	.long	60
	.quad	.LC490
	.quad	.LC491
	.long	1004
	.long	45
	.quad	.LC492
	.quad	.LC493
	.long	1005
	.long	70
	.quad	.LC494
	.quad	.LC495
	.long	1006
	.long	70
	.quad	.LC496
	.quad	.LC497
	.long	45
	.long	45
	.quad	.LC498
	.quad	.LC499
	.long	67
	.long	115
	.quad	.LC500
	.quad	.LC501
	.long	68
	.long	115
	.quad	.LC502
	.quad	.LC503
	.long	69
	.long	115
	.quad	.LC504
	.quad	.LC505
	.long	70
	.long	60
	.quad	.LC506
	.quad	.LC507
	.long	71
	.long	60
	.quad	.LC508
	.quad	.LC509
	.long	72
	.long	115
	.quad	.LC510
	.quad	.LC511
	.long	87
	.long	45
	.quad	.LC512
	.quad	.LC513
	.long	86
	.long	45
	.quad	.LC514
	.quad	.LC515
	.long	85
	.long	45
	.quad	.LC516
	.quad	.LC517
	.long	84
	.long	45
	.quad	.LC518
	.quad	.LC519
	.long	88
	.long	45
	.quad	.LC520
	.quad	.LC521
	.long	92
	.long	45
	.quad	.LC522
	.quad	.LC523
	.long	93
	.long	112
	.quad	.LC524
	.quad	.LC525
	.long	94
	.long	45
	.quad	.LC526
	.quad	.LC527
	.long	95
	.long	45
	.quad	.LC528
	.quad	.LC529
	.long	89
	.long	45
	.quad	.LC530
	.quad	.LC531
	.long	90
	.long	45
	.quad	.LC532
	.quad	.LC533
	.long	65
	.long	45
	.quad	.LC534
	.quad	.LC535
	.long	101
	.long	115
	.quad	.LC536
	.quad	.LC537
	.long	103
	.long	115
	.quad	.LC538
	.quad	.LC539
	.long	102
	.long	115
	.quad	.LC540
	.quad	.LC541
	.long	2
	.long	115
	.quad	.LC542
	.quad	.LC543
	.long	106
	.long	62
	.quad	.LC544
	.quad	.LC545
	.long	107
	.long	110
	.quad	.LC546
	.quad	.LC547
	.long	108
	.long	110
	.quad	.LC548
	.quad	.LC549
	.long	109
	.long	45
	.quad	.LC550
	.quad	.LC551
	.long	110
	.long	110
	.quad	.LC552
	.quad	.LC553
	.long	111
	.long	45
	.quad	.LC554
	.quad	.LC555
	.long	112
	.long	45
	.quad	.LC556
	.quad	0
	.long	0
	.long	0
	.quad	0
	.data
	.align 32
	.type	tlscstatp, @object
	.size	tlscstatp, 48
tlscstatp:
	.long	-1
	.zero	44
	.local	local_argv
	.comm	local_argv,8,8
	.local	local_argc
	.comm	local_argc,4,4
	.local	srp_callback_parm
	.comm	srp_callback_parm,24,16
	.globl	psk_key
	.bss
	.align 8
	.type	psk_key, @object
	.size	psk_key, 8
psk_key:
	.zero	8
	.section	.rodata.str1.1
.LC557:
	.string	"Client_identity"
	.section	.data.rel.local,"aw"
	.align 8
	.type	psk_identity, @object
	.size	psk_identity, 8
psk_identity:
	.quad	.LC557
	.local	psksess
	.comm	psksess,8,8
	.local	early_data
	.comm	early_data,4,4
	.local	stateless
	.comm	stateless,4,4
	.local	dtlslisten
	.comm	dtlslisten,4,4
	.local	socket_mtu
	.comm	socket_mtu,8,8
	.local	enable_timeouts
	.comm	enable_timeouts,4,4
	.local	session_id_prefix
	.comm	session_id_prefix,8,8
	.local	async
	.comm	async,4,4
	.data
	.align 4
	.type	keymatexportlen, @object
	.size	keymatexportlen, 4
keymatexportlen:
	.long	20
	.local	keymatexportlabel
	.comm	keymatexportlabel,8,8
	.local	s_brief
	.comm	s_brief,4,4
	.local	s_ign_eof
	.comm	s_ign_eof,4,4
	.local	s_quiet
	.comm	s_quiet,4,4
	.local	s_msg
	.comm	s_msg,4,4
	.local	s_tlsextdebug
	.comm	s_tlsextdebug,4,4
	.local	s_debug
	.comm	s_debug,4,4
	.local	bio_s_msg
	.comm	bio_s_msg,8,8
	.local	bio_s_out
	.comm	bio_s_out,8,8
	.local	www
	.comm	www,4,4
	.local	ctx2
	.comm	ctx2,8,8
	.local	ctx
	.comm	ctx,8,8
	.local	s_crlf
	.comm	s_crlf,4,4
	.local	s_nbio_test
	.comm	s_nbio_test,4,4
	.local	s_nbio
	.comm	s_nbio,4,4
	.align 4
	.type	accept_socket, @object
	.size	accept_socket, 4
accept_socket:
	.long	-1
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC36:
	.quad	0
	.quad	250000
	.align 16
.LC46:
	.byte	10
	.byte	10
	.byte	10
	.byte	10
	.byte	10
	.byte	10
	.byte	10
	.byte	10
	.byte	10
	.byte	10
	.byte	10
	.byte	10
	.byte	10
	.byte	10
	.byte	10
	.byte	10
	.align 16
.LC47:
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
