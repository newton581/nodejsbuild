	.file	"s_client.c"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"OCSP response: "
.LC1:
	.string	"no response sent\n"
.LC2:
	.string	"response parse error\n"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"\n======================================\n"
	.align 8
.LC4:
	.string	"======================================\n"
	.text
	.p2align 4
	.type	ocsp_resp_cb, @function
ocsp_resp_cb:
.LFB1651:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rsi, %r12
	movl	$70, %esi
	pushq	%rbx
	movq	%r13, %rcx
	subq	$24, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	SSL_ctrl@PLT
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	BIO_puts@PLT
	cmpq	$0, -48(%rbp)
	je	.L8
	movq	%r13, %rsi
	movslq	%ebx, %rdx
	xorl	%edi, %edi
	call	d2i_OCSP_RESPONSE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L9
	leaq	.LC3(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_puts@PLT
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	OCSP_RESPONSE_print@PLT
	leaq	.LC4(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_puts@PLT
	movq	%r13, %rdi
	call	OCSP_RESPONSE_free@PLT
	movl	$1, %eax
.L1:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L10
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	.cfi_restore_state
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_puts@PLT
	movl	$1, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L9:
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_puts@PLT
	movq	-48(%rbp), %rsi
	movl	$4, %ecx
	movl	%ebx, %edx
	movq	%r12, %rdi
	call	BIO_dump_indent@PLT
	xorl	%eax, %eax
	jmp	.L1
.L10:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1651:
	.size	ocsp_resp_cb, .-ocsp_resp_cb
	.section	.rodata.str1.1
.LC5:
	.string	"SRP parameters:\n"
.LC6:
	.string	"\tN="
.LC7:
	.string	"\n\tg="
.LC8:
	.string	"\n"
	.section	.rodata.str1.8
	.align 8
.LC9:
	.string	"SRP param N and g are not known params, going to check deeper.\n"
	.section	.rodata.str1.1
.LC10:
	.string	"SRP param N and g rejected.\n"
	.text
	.p2align 4
	.type	ssl_srp_verify_param_cb, @function
ssl_srp_verify_param_cb:
.LFB1639:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	call	SSL_get_srp_N@PLT
	testq	%rax, %rax
	je	.L37
	movq	%r13, %rdi
	movq	%rax, %r12
	call	SSL_get_srp_g@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L37
	cmpq	$0, 16(%rbx)
	jne	.L15
	cmpl	$1, 24(%rbx)
	je	.L15
.L16:
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	$1, %r14d
	call	SRP_check_known_gN_param@PLT
	testq	%rax, %rax
	jne	.L11
	cmpl	$1, 24(%rbx)
	jne	.L18
	movl	20(%rbx), %edx
	testl	%edx, %edx
	jne	.L38
.L19:
	movq	%r13, %rdi
	call	BN_num_bits@PLT
	cmpl	$128, %eax
	jle	.L39
.L18:
	movq	bio_err(%rip), %rdi
	leaq	.LC10(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L37:
	xorl	%r14d, %r14d
.L11:
	addq	$24, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	.cfi_restore_state
	movq	bio_err(%rip), %rdi
	leaq	.LC5(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	leaq	.LC6(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	movq	%r12, %rsi
	call	BN_print@PLT
	movq	bio_err(%rip), %rdi
	leaq	.LC7(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	movq	%r13, %rsi
	call	BN_print@PLT
	movq	bio_err(%rip), %rdi
	leaq	.LC8(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L39:
	call	BN_CTX_new@PLT
	movq	%rax, %r15
	call	BN_new@PLT
	movq	%rax, %rbx
	call	BN_new@PLT
	testq	%r15, %r15
	je	.L21
	movq	%r12, %rdi
	movq	%rax, -56(%rbp)
	call	BN_is_odd@PLT
	movq	-56(%rbp), %r9
	testl	%eax, %eax
	jne	.L40
.L22:
	movq	%r9, %rdi
.L36:
	call	BN_free@PLT
	movq	%rbx, %rdi
	call	BN_free@PLT
	movq	%r15, %rdi
	call	BN_CTX_free@PLT
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L38:
	movq	bio_err(%rip), %rdi
	leaq	.LC9(%rip), %rsi
	call	BIO_printf@PLT
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L21:
	movq	%rax, %rdi
	call	BN_free@PLT
	movq	%rbx, %rdi
	call	BN_free@PLT
	xorl	%edi, %edi
	call	BN_CTX_free@PLT
	jmp	.L18
.L40:
	xorl	%ecx, %ecx
	movq	%r15, %rdx
	movl	$64, %esi
	movq	%r12, %rdi
	call	BN_is_prime_ex@PLT
	movq	-56(%rbp), %r9
	cmpl	$1, %eax
	jne	.L22
	testq	%rbx, %rbx
	je	.L24
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	BN_rshift1@PLT
	movq	-56(%rbp), %r9
	testl	%eax, %eax
	je	.L22
	xorl	%ecx, %ecx
	movq	%r15, %rdx
	movl	$64, %esi
	movq	%rbx, %rdi
	call	BN_is_prime_ex@PLT
	movq	-56(%rbp), %r9
	cmpl	$1, %eax
	movl	%eax, %r14d
	jne	.L22
	testq	%r9, %r9
	je	.L27
	movq	%r9, %rdi
	movq	%r15, %r8
	movq	%r12, %rcx
	movq	%rbx, %rdx
	movq	%r13, %rsi
	call	BN_mod_exp@PLT
	movq	-56(%rbp), %r9
	testl	%eax, %eax
	je	.L22
	movq	%r9, %rdi
	movl	$1, %esi
	call	BN_add_word@PLT
	movq	-56(%rbp), %r9
	testl	%eax, %eax
	je	.L22
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	BN_cmp@PLT
	movq	-56(%rbp), %r9
	testl	%eax, %eax
	movq	%r9, %rdi
	jne	.L36
	call	BN_free@PLT
	movq	%rbx, %rdi
	call	BN_free@PLT
	movq	%r15, %rdi
	call	BN_CTX_free@PLT
	jmp	.L11
.L24:
	movq	%r9, %rdi
	call	BN_free@PLT
	xorl	%edi, %edi
	call	BN_free@PLT
	movq	%r15, %rdi
	call	BN_CTX_free@PLT
	jmp	.L18
.L27:
	xorl	%edi, %edi
	jmp	.L36
	.cfi_endproc
.LFE1639:
	.size	ssl_srp_verify_param_cb, .-ssl_srp_verify_param_cb
	.section	.rodata.str1.1
.LC11:
	.string	"SRP password buffer"
.LC12:
	.string	"SRP user"
.LC13:
	.string	"Can't read Password\n"
	.section	.rodata.str1.8
	.align 8
.LC14:
	.string	"../deps/openssl/openssl/apps/s_client.c"
	.text
	.p2align 4
	.type	ssl_give_srp_client_pwd_cb, @function
ssl_give_srp_client_pwd_cb:
.LFB1640:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1025, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	leaq	.LC11(%rip), %rsi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	app_malloc@PLT
	xorl	%edx, %edx
	leaq	-48(%rbp), %rcx
	movl	$1024, %esi
	movq	%rax, %r12
	movq	(%rbx), %rax
	movq	%r12, %rdi
	movq	%rax, -48(%rbp)
	leaq	.LC12(%rip), %rax
	movq	%rax, -40(%rbp)
	call	password_callback@PLT
	testl	%eax, %eax
	js	.L46
	cltq
	movb	$0, (%r12,%rax)
.L41:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L47
	addq	$32, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L46:
	.cfi_restore_state
	movq	bio_err(%rip), %rdi
	leaq	.LC13(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	movl	$362, %edx
	leaq	.LC14(%rip), %rsi
	call	CRYPTO_free@PLT
	jmp	.L41
.L47:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1640:
	.size	ssl_give_srp_client_pwd_cb, .-ssl_give_srp_client_pwd_cb
	.section	.rodata.str1.1
.LC15:
	.string	"SERVERINFO FOR EXTENSION %d"
.LC16:
	.string	""
	.text
	.p2align 4
	.type	serverinfo_cli_parse_cb, @function
serverinfo_cli_parse_cb:
.LFB1642:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	leaq	-65536(%rsp), %r11
.LPSRL0:
	subq	$4096, %rsp
	orq	$0, (%rsp)
	cmpq	%r11, %rsp
	jne	.LPSRL0
	addq	$-128, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	%esi, %r12d
	movzwl	%cx, %ebx
	rolw	$8, %cx
	movl	%r12d, %eax
	movq	%rdx, %rsi
	movw	%cx, -65582(%rbp)
	leaq	-65580(%rbp), %rdi
	rolw	$8, %ax
	movl	$65536, %ecx
	movq	%rbx, %rdx
	leaq	-65696(%rbp), %r13
	movw	%ax, -65584(%rbp)
	leaq	-65584(%rbp), %r14
	call	__memcpy_chk@PLT
	movl	%r12d, %ecx
	movl	$100, %esi
	movq	%r13, %rdi
	leaq	.LC15(%rip), %rdx
	xorl	%eax, %eax
	call	BIO_snprintf@PLT
	leaq	4(%rbx), %r8
	movq	%r14, %rcx
	movq	%r13, %rsi
	movq	bio_c_out(%rip), %rdi
	leaq	.LC16(%rip), %rdx
	call	PEM_write_bio@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L51
	addq	$65664, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L51:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1642:
	.size	serverinfo_cli_parse_cb, .-serverinfo_cli_parse_cb
	.section	.rodata.str1.8
	.align 8
.LC17:
	.string	"Protocols advertised by server: "
	.section	.rodata.str1.1
.LC18:
	.string	", "
	.text
	.p2align 4
	.type	next_proto_cb, @function
next_proto_cb:
.LFB1641:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%r8d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$24, %rsp
	movl	c_quiet(%rip), %eax
	movq	%rsi, -56(%rbp)
	movq	%rdx, -64(%rbp)
	testl	%eax, %eax
	je	.L64
.L53:
	movl	8(%rbx), %r9d
	movq	(%rbx), %r8
	movl	%r13d, %ecx
	movq	%r12, %rdx
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %rdi
	call	SSL_select_next_proto@PLT
	movl	%eax, 16(%rbx)
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L64:
	.cfi_restore_state
	movq	bio_c_out(%rip), %rdi
	leaq	.LC17(%rip), %rsi
	xorl	%eax, %eax
	xorl	%r14d, %r14d
	call	BIO_printf@PLT
	movq	bio_c_out(%rip), %rdi
	testl	%r13d, %r13d
	je	.L58
	.p2align 4,,10
	.p2align 3
.L55:
	movl	%r14d, %r8d
	leal	1(%r14), %esi
	leaq	(%r12,%r8), %r15
	addq	%r12, %rsi
	movzbl	(%r15), %edx
	call	BIO_write@PLT
	movzbl	(%r15), %eax
	leal	1(%r14,%rax), %r14d
	cmpl	%r14d, %r13d
	jbe	.L58
	movq	bio_c_out(%rip), %rdi
	testl	%r14d, %r14d
	je	.L55
	movl	$2, %edx
	leaq	.LC18(%rip), %rsi
	call	BIO_write@PLT
	movq	bio_c_out(%rip), %rdi
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L58:
	movq	bio_c_out(%rip), %rdi
	movl	$1, %edx
	leaq	.LC8(%rip), %rsi
	call	BIO_write@PLT
	jmp	.L53
	.cfi_endproc
.LFE1641:
	.size	next_proto_cb, .-next_proto_cb
	.section	.rodata.str1.1
.LC19:
	.string	"psk_client_cb\n"
	.section	.rodata.str1.8
	.align 8
.LC20:
	.string	"NULL received PSK identity hint, continuing anyway\n"
	.align 8
.LC21:
	.string	"Received PSK identity hint '%s'\n"
	.section	.rodata.str1.1
.LC22:
	.string	"%s"
.LC23:
	.string	"created identity '%s' len=%d\n"
	.section	.rodata.str1.8
	.align 8
.LC24:
	.string	"Could not convert PSK key '%s' to buffer\n"
	.align 8
.LC25:
	.string	"psk buffer of callback is too small (%d) for key (%ld)\n"
	.section	.rodata.str1.1
.LC26:
	.string	"created PSK len=%ld\n"
.LC27:
	.string	"Error in PSK client callback\n"
	.text
	.p2align 4
	.type	psk_client_cb, @function
psk_client_cb:
.LFB1635:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%r9d, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%ecx, %ebx
	subq	$24, %rsp
	movl	c_debug(%rip), %r8d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jne	.L83
.L69:
	movq	psk_identity(%rip), %rcx
	xorl	%eax, %eax
	movl	%ebx, %esi
	movq	%r13, %rdi
	leaq	.LC22(%rip), %rdx
	call	BIO_snprintf@PLT
	testl	%eax, %eax
	js	.L80
	cmpl	%ebx, %eax
	ja	.L80
	movl	c_debug(%rip), %edx
	testl	%edx, %edx
	jne	.L84
.L74:
	movq	psk_key(%rip), %rdi
	leaq	-64(%rbp), %rsi
	call	OPENSSL_hexstr2buf@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L85
	movq	-64(%rbp), %rcx
	testl	%r12d, %r12d
	js	.L76
	movl	%r12d, %eax
	cmpq	%rcx, %rax
	jge	.L77
.L76:
	movq	bio_err(%rip), %rdi
	movl	%r12d, %edx
	leaq	.LC25(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movl	$160, %edx
	leaq	.LC14(%rip), %rsi
	movq	%r13, %rdi
	call	CRYPTO_free@PLT
	xorl	%eax, %eax
.L65:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L86
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L77:
	.cfi_restore_state
	movq	%rcx, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	memcpy@PLT
	movl	$165, %edx
	leaq	.LC14(%rip), %rsi
	movq	%r13, %rdi
	call	CRYPTO_free@PLT
	movl	c_debug(%rip), %eax
	testl	%eax, %eax
	jne	.L87
.L78:
	movl	-64(%rbp), %eax
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L84:
	movq	bio_c_out(%rip), %rdi
	movl	%eax, %ecx
	xorl	%eax, %eax
	movq	%r13, %rdx
	leaq	.LC23(%rip), %rsi
	call	BIO_printf@PLT
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L80:
	movl	c_debug(%rip), %ecx
	testl	%ecx, %ecx
	jne	.L72
	xorl	%eax, %eax
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L83:
	movq	bio_c_out(%rip), %rdi
	movq	%rsi, %r15
	leaq	.LC19(%rip), %rsi
	call	BIO_printf@PLT
	testq	%r15, %r15
	je	.L88
	movl	c_debug(%rip), %esi
	testl	%esi, %esi
	je	.L69
	movq	bio_c_out(%rip), %rdi
	movq	%r15, %rdx
	leaq	.LC21(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L72:
	movq	bio_err(%rip), %rdi
	leaq	.LC27(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	xorl	%eax, %eax
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L87:
	movq	-64(%rbp), %rdx
	movq	bio_c_out(%rip), %rdi
	leaq	.LC26(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L88:
	movl	c_debug(%rip), %edi
	testl	%edi, %edi
	je	.L69
	movq	bio_c_out(%rip), %rdi
	leaq	.LC20(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L85:
	movq	psk_key(%rip), %rdx
	movq	bio_err(%rip), %rdi
	leaq	.LC24(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	xorl	%eax, %eax
	jmp	.L65
.L86:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1635:
	.size	psk_client_cb, .-psk_client_cb
	.section	.rodata.str1.8
	.align 8
.LC28:
	.string	"Error finding suitable ciphersuite\n"
	.text
	.p2align 4
	.type	psk_use_session_cb, @function
psk_use_session_cb:
.LFB1636:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$40, %rsp
	movq	psksess(%rip), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L90
	call	SSL_SESSION_up_ref@PLT
	movq	psksess(%rip), %r15
.L91:
	movq	%r15, %rdi
	call	SSL_SESSION_get0_cipher@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L96
	testq	%rbx, %rbx
	je	.L99
	call	SSL_CIPHER_get_handshake_digest@PLT
	cmpq	%rax, %rbx
	je	.L99
	movq	$0, 0(%r13)
	movq	%r15, %rdi
	movq	$0, (%r12)
	movq	$0, (%r14)
	call	SSL_SESSION_free@PLT
	movl	$1, %eax
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L99:
	movq	psk_identity(%rip), %rdi
	movq	%r15, (%r14)
	movq	%rdi, 0(%r13)
	call	strlen@PLT
	movq	%rax, (%r12)
	movl	$1, %eax
.L89:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L118
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L90:
	.cfi_restore_state
	movq	psk_key(%rip), %rdi
	leaq	-64(%rbp), %rsi
	call	OPENSSL_hexstr2buf@PLT
	movq	%rax, -72(%rbp)
	testq	%rax, %rax
	je	.L119
	leaq	tls13_aes128gcmsha256_id(%rip), %rsi
	movq	%r15, %rdi
	call	SSL_CIPHER_find@PLT
	movq	%rax, -80(%rbp)
	testq	%rax, %rax
	je	.L120
	call	SSL_SESSION_new@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L97
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %rsi
	movq	%rax, %rdi
	call	SSL_SESSION_set1_master_key@PLT
	testl	%eax, %eax
	je	.L97
	movq	-80(%rbp), %rsi
	movq	%r15, %rdi
	call	SSL_SESSION_set_cipher@PLT
	testl	%eax, %eax
	jne	.L121
.L97:
	movq	-72(%rbp), %rdi
	movl	$214, %edx
	leaq	.LC14(%rip), %rsi
	call	CRYPTO_free@PLT
.L96:
	movq	%r15, %rdi
	call	SSL_SESSION_free@PLT
	xorl	%eax, %eax
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L119:
	movq	psk_key(%rip), %rdx
	movq	bio_err(%rip), %rdi
	leaq	.LC24(%rip), %rsi
	call	BIO_printf@PLT
	xorl	%eax, %eax
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L121:
	movl	$772, %esi
	movq	%r15, %rdi
	call	SSL_SESSION_set_protocol_version@PLT
	testl	%eax, %eax
	je	.L97
	movq	-72(%rbp), %rdi
	movl	$217, %edx
	leaq	.LC14(%rip), %rsi
	call	CRYPTO_free@PLT
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L120:
	movq	bio_err(%rip), %rdi
	leaq	.LC28(%rip), %rsi
	call	BIO_printf@PLT
	movq	-72(%rbp), %rdi
	movl	$205, %edx
	leaq	.LC14(%rip), %rsi
	call	CRYPTO_free@PLT
	xorl	%eax, %eax
	jmp	.L89
.L118:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1636:
	.size	psk_use_session_cb, .-psk_use_session_cb
	.p2align 4
	.type	checked_uint8, @function
checked_uint8:
.LFB1644:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -72(%rbp)
	movq	(%rdi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	__errno_location@PLT
	movl	$10, %edx
	leaq	-64(%rbp), %rsi
	movq	%r14, %rdi
	movl	(%rax), %r13d
	movl	$0, (%rax)
	movq	%rax, %rbx
	movl	%r13d, saved_errno(%rip)
	call	strtol@PLT
	movl	(%rbx), %edx
	movl	%r13d, (%rbx)
	movq	%rax, %r15
	movabsq	$9223372036854775807, %rax
	addq	%r15, %rax
	cmpq	$-3, %rax
	jbe	.L123
	cmpl	$34, %edx
	je	.L125
.L123:
	movq	-64(%rbp), %rbx
	cmpq	%r14, %rbx
	je	.L125
	call	__ctype_b_loc@PLT
	movzbl	(%rbx), %ecx
	movq	(%rax), %rdx
	testb	$32, 1(%rdx,%rcx,2)
	je	.L125
	movq	-72(%rbp), %rcx
	movb	%r15b, (%rcx)
	testq	$-256, %r15
	jne	.L125
	movq	(%rax), %rax
	movzbl	(%rbx), %edx
	testb	$32, 1(%rax,%rdx,2)
	jne	.L127
.L126:
	movq	%rbx, (%r12)
	movl	$1, %eax
	jmp	.L122
	.p2align 4,,10
	.p2align 3
.L125:
	movq	$-1, %rax
.L122:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L143
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L127:
	.cfi_restore_state
	movzbl	1(%rbx), %edx
	addq	$1, %rbx
	testb	$32, 1(%rax,%rdx,2)
	je	.L126
	jmp	.L127
.L143:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1644:
	.size	checked_uint8, .-checked_uint8
	.section	.rodata.str1.1
.LC29:
	.string	"hexdecode"
	.text
	.p2align 4
	.type	hexdecode, @function
hexdecode:
.LFB1643:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %r15
	movq	%rdi, -72(%rbp)
	movq	%rsi, -80(%rbp)
	movq	%r15, %rdi
	call	strlen@PLT
	leaq	.LC29(%rip), %rsi
	shrq	%rax
	movq	%rax, %rdi
	call	app_malloc@PLT
	movq	%rax, -56(%rbp)
	testq	%rax, %rax
	je	.L152
	movzbl	(%r15), %edi
	testb	%dil, %dil
	movb	%dil, -57(%rbp)
	je	.L153
	movq	%rax, %r13
	xorl	%ebx, %ebx
	xorl	%r12d, %r12d
	call	__ctype_b_loc@PLT
	movzbl	-57(%rbp), %edi
	movq	%rax, %r14
	jmp	.L150
	.p2align 4,,10
	.p2align 3
.L149:
	sall	$4, %r12d
	movl	$1, %ebx
.L147:
	movzbl	1(%r15), %edi
	addq	$1, %r15
	testb	%dil, %dil
	je	.L156
.L150:
	movq	(%r14), %rsi
	movzbl	%dil, %eax
	testb	$32, 1(%rsi,%rax,2)
	jne	.L147
	call	OPENSSL_hexchar2int@PLT
	testl	%eax, %eax
	js	.L157
	orl	%eax, %r12d
	cmpl	$1, %ebx
	jne	.L149
	addq	$1, %r15
	movb	%r12b, 0(%r13)
	xorl	%ebx, %ebx
	addq	$1, %r13
	movzbl	(%r15), %edi
	xorl	%r12d, %r12d
	testb	%dil, %dil
	jne	.L150
	.p2align 4,,10
	.p2align 3
.L156:
	testl	%ebx, %ebx
	jne	.L151
	movq	%r13, %rax
	subq	-56(%rbp), %rax
.L146:
	movq	-72(%rbp), %rcx
	movq	-80(%rbp), %rdx
	movq	%r15, (%rcx)
	movq	-56(%rbp), %rcx
	movq	%rcx, (%rdx)
.L144:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L157:
	.cfi_restore_state
	movq	-56(%rbp), %rdi
	movl	$451, %edx
	leaq	.LC14(%rip), %rsi
	call	CRYPTO_free@PLT
	addq	$40, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L151:
	.cfi_restore_state
	movq	-56(%rbp), %rdi
	movl	$463, %edx
	leaq	.LC14(%rip), %rsi
	call	CRYPTO_free@PLT
	addq	$40, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L153:
	.cfi_restore_state
	xorl	%eax, %eax
	jmp	.L146
.L152:
	movq	$-1, %rax
	jmp	.L144
	.cfi_endproc
.LFE1643:
	.size	hexdecode, .-hexdecode
	.p2align 4
	.type	do_ssl_shutdown, @function
do_ssl_shutdown:
.LFB1634:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
.L162:
	movq	%rbx, %rdi
	call	SSL_shutdown@PLT
	testl	%eax, %eax
	js	.L165
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L165:
	.cfi_restore_state
	movl	%eax, %esi
	movq	%rbx, %rdi
	call	SSL_get_error@PLT
	cmpl	$3, %eax
	jg	.L160
	cmpl	$1, %eax
	jg	.L162
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L160:
	.cfi_restore_state
	subl	$9, %eax
	cmpl	$1, %eax
	jbe	.L162
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1634:
	.size	do_ssl_shutdown, .-do_ssl_shutdown
	.section	.rodata.str1.1
.LC30:
	.string	" NOT"
.LC31:
	.string	"---\nCertificate chain\n"
.LC32:
	.string	"%2d s:"
.LC33:
	.string	"   i:"
.LC34:
	.string	"---\n"
.LC35:
	.string	"Server certificate\n"
	.section	.rodata.str1.8
	.align 8
.LC36:
	.string	"no peer certificate available\n"
	.align 8
.LC37:
	.string	"---\nSSL handshake has read %ju bytes and written %ju bytes\n"
	.section	.rodata.str1.1
.LC38:
	.string	"---\nSCTs present (%i)\n"
.LC39:
	.string	"SCT validation status: %s\n"
.LC40:
	.string	"\n---\n"
.LC41:
	.string	"---\nNew, "
.LC42:
	.string	"%s, Cipher is %s\n"
.LC43:
	.string	"Server public key is %d bit\n"
	.section	.rodata.str1.8
	.align 8
.LC44:
	.string	"Secure Renegotiation IS%s supported\n"
	.section	.rodata.str1.1
.LC45:
	.string	"Next protocol: (%d) "
.LC46:
	.string	"ALPN protocol: "
.LC47:
	.string	"No ALPN negotiated\n"
	.section	.rodata.str1.8
	.align 8
.LC48:
	.string	"SRTP Extension negotiated, profile=%s\n"
	.section	.rodata.str1.1
.LC49:
	.string	"Early data was not sent\n"
.LC50:
	.string	"Early data was rejected\n"
.LC51:
	.string	"Early data was accepted\n"
.LC52:
	.string	"Verify return code: %ld (%s)\n"
.LC53:
	.string	"Keying material exporter:\n"
.LC54:
	.string	"    Label: '%s'\n"
.LC55:
	.string	"    Length: %i bytes\n"
.LC56:
	.string	"export key"
.LC57:
	.string	"    Error\n"
.LC58:
	.string	"    Keying material: "
.LC59:
	.string	"%02X"
.LC60:
	.string	"---\nReused, "
	.text
	.p2align 4
	.type	print_stuff, @function
print_stuff:
.LFB1650:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	SSL_version@PLT
	movq	%r13, %rdi
	movl	%eax, -84(%rbp)
	call	SSL_get_SSL_CTX@PLT
	movq	%rax, -96(%rbp)
	testl	%ebx, %ebx
	jne	.L238
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	print_verify_detail@PLT
	movq	%r13, %rdi
	call	SSL_session_reused@PLT
	leaq	.LC41(%rip), %rsi
	movq	%r12, %rdi
	testl	%eax, %eax
	jne	.L176
.L237:
	call	BIO_printf@PLT
	movq	%r13, %rdi
	call	SSL_get_current_cipher@PLT
	movq	%rax, %r14
	movq	%rax, %rdi
	call	SSL_CIPHER_get_name@PLT
	movq	%r14, %rdi
	xorl	%r14d, %r14d
	movq	%rax, %r15
	call	SSL_CIPHER_get_version@PLT
	movq	%r15, %rcx
	leaq	.LC42(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L200:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$76, %esi
	movq	%r13, %rdi
	call	SSL_ctrl@PLT
	movq	%r12, %rdi
	leaq	-68(%rbp), %rbx
	leaq	-64(%rbp), %r15
	testq	%rax, %rax
	leaq	.LC16(%rip), %rdx
	leaq	.LC30(%rip), %rax
	cmove	%rax, %rdx
	leaq	.LC44(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	cmpl	$-1, 16+next_proto(%rip)
	je	.L186
	movq	%rbx, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	SSL_get0_next_proto_negotiated@PLT
	movl	16+next_proto(%rip), %edx
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC45(%rip), %rsi
	call	BIO_printf@PLT
	movl	-68(%rbp), %edx
	movq	-64(%rbp), %rsi
	movq	%r12, %rdi
	call	BIO_write@PLT
	movl	$1, %edx
	leaq	.LC8(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_write@PLT
.L186:
	movq	%r15, %rsi
	movq	%rbx, %rdx
	movq	%r13, %rdi
	call	SSL_get0_alpn_selected@PLT
	movl	-68(%rbp), %esi
	testl	%esi, %esi
	je	.L187
	leaq	.LC46(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movl	-68(%rbp), %edx
	movq	-64(%rbp), %rsi
	movq	%r12, %rdi
	call	BIO_write@PLT
	movl	$1, %edx
	leaq	.LC8(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_write@PLT
.L188:
	movq	%r13, %rdi
	call	SSL_get_selected_srtp_profile@PLT
	testq	%rax, %rax
	je	.L189
	movq	(%rax), %rdx
	leaq	.LC48(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L189:
	cmpl	$772, -84(%rbp)
	movq	%r13, %rdi
	je	.L239
	call	SSL_get_session@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	SSL_SESSION_print@PLT
.L194:
	movq	%r13, %rdi
	call	SSL_get_session@PLT
	testq	%rax, %rax
	je	.L195
	cmpq	$0, keymatexportlabel(%rip)
	je	.L195
	leaq	.LC53(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	keymatexportlabel(%rip), %rdx
	leaq	.LC54(%rip), %rsi
	xorl	%eax, %eax
	movq	%r12, %rdi
	call	BIO_printf@PLT
	movl	keymatexportlen(%rip), %edx
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC55(%rip), %rsi
	call	BIO_printf@PLT
	movl	keymatexportlen(%rip), %edi
	leaq	.LC56(%rip), %rsi
	call	app_malloc@PLT
	movq	keymatexportlabel(%rip), %rbx
	movq	%rax, %r15
	movq	%rbx, %rdi
	call	strlen@PLT
	pushq	$0
	movq	%rbx, %rcx
	xorl	%r9d, %r9d
	pushq	$0
	movslq	keymatexportlen(%rip), %rdx
	movq	%rax, %r8
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	SSL_export_keying_material@PLT
	popq	%rdx
	popq	%rcx
	testl	%eax, %eax
	jne	.L196
	leaq	.LC57(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_printf@PLT
.L197:
	movl	$3361, %edx
	leaq	.LC14(%rip), %rsi
	movq	%r15, %rdi
	call	CRYPTO_free@PLT
.L195:
	leaq	.LC34(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	%r14, %rdi
	call	X509_free@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	movq	%r12, %rdi
	call	BIO_ctrl@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L240
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L176:
	.cfi_restore_state
	leaq	.LC60(%rip), %rsi
.L236:
	movq	%r12, %rdi
	xorl	%eax, %eax
	jmp	.L237
	.p2align 4,,10
	.p2align 3
.L187:
	leaq	.LC47(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L188
	.p2align 4,,10
	.p2align 3
.L239:
	call	SSL_get_early_data_status@PLT
	cmpl	$1, %eax
	je	.L191
	cmpl	$2, %eax
	je	.L192
	testl	%eax, %eax
	je	.L241
.L193:
	movq	%r13, %rdi
	call	SSL_get_verify_result@PLT
	movq	%rax, %rdi
	movq	%rax, %r15
	call	X509_verify_cert_error_string@PLT
	movq	%r15, %rdx
	leaq	.LC52(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L194
	.p2align 4,,10
	.p2align 3
.L238:
	movq	%r13, %rdi
	call	SSL_get_peer_cert_chain@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L168
	leaq	.LC31(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	xorl	%ebx, %ebx
	call	BIO_printf@PLT
	leaq	.LC32(%rip), %r15
	movq	%r13, -104(%rbp)
	jmp	.L169
	.p2align 4,,10
	.p2align 3
.L170:
	addl	$1, %ebx
.L169:
	movq	%r14, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jge	.L242
	movl	%ebx, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	call	get_nameopt@PLT
	movl	%ebx, %esi
	movq	%r14, %rdi
	movq	%rax, %r13
	call	OPENSSL_sk_value@PLT
	movq	%rax, %rdi
	call	X509_get_subject_name@PLT
	movq	%r13, %rcx
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	X509_NAME_print_ex@PLT
	leaq	.LC8(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_puts@PLT
	leaq	.LC33(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	call	get_nameopt@PLT
	movl	%ebx, %esi
	movq	%r14, %rdi
	movq	%rax, %r13
	call	OPENSSL_sk_value@PLT
	movq	%rax, %rdi
	call	X509_get_issuer_name@PLT
	movq	%r13, %rcx
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	X509_NAME_print_ex@PLT
	leaq	.LC8(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_puts@PLT
	movl	c_showcerts(%rip), %r9d
	testl	%r9d, %r9d
	je	.L170
	movl	%ebx, %esi
	movq	%r14, %rdi
	call	OPENSSL_sk_value@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	PEM_write_bio_X509@PLT
	jmp	.L170
	.p2align 4,,10
	.p2align 3
.L196:
	leaq	.LC58(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	xorl	%ebx, %ebx
	call	BIO_printf@PLT
	movl	keymatexportlen(%rip), %eax
	leaq	.LC59(%rip), %r13
	testl	%eax, %eax
	jle	.L199
	.p2align 4,,10
	.p2align 3
.L198:
	movzbl	(%r15,%rbx), %edx
	xorl	%eax, %eax
	movq	%r13, %rsi
	movq	%r12, %rdi
	addq	$1, %rbx
	call	BIO_printf@PLT
	cmpl	%ebx, keymatexportlen(%rip)
	jg	.L198
.L199:
	leaq	.LC8(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L197
	.p2align 4,,10
	.p2align 3
.L168:
	leaq	.LC34(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	%r13, %rdi
	call	SSL_get_peer_certificate@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L172
	leaq	.LC35(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L205:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	PEM_write_bio_X509@PLT
.L173:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	dump_cert_text@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	print_ca_names@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	ssl_print_sigalgs@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	ssl_print_tmp_key@PLT
	movq	%r13, %rdi
	call	SSL_session_reused@PLT
	movl	%eax, %ebx
	testl	%eax, %eax
	je	.L243
.L179:
	movq	%r13, %rdi
	call	SSL_get_wbio@PLT
	movq	%rax, %rdi
	call	BIO_number_written@PLT
	movq	%r13, %rdi
	movq	%rax, %r15
	call	SSL_get_rbio@PLT
	movq	%rax, %rdi
	call	BIO_number_read@PLT
	movq	%r15, %rcx
	leaq	.LC37(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	print_verify_detail@PLT
	movq	%r13, %rdi
	call	SSL_session_reused@PLT
	leaq	.LC60(%rip), %rsi
	testl	%eax, %eax
	jne	.L235
	leaq	.LC41(%rip), %rsi
.L235:
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	%r13, %rdi
	call	SSL_get_current_cipher@PLT
	movq	%rax, %r15
	movq	%rax, %rdi
	call	SSL_CIPHER_get_name@PLT
	movq	%r15, %rdi
	movq	%rax, %rbx
	call	SSL_CIPHER_get_version@PLT
	movq	%rbx, %rcx
	leaq	.LC42(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	%r14, %rdi
	call	X509_get0_pubkey@PLT
	movq	%rax, %rdi
	call	EVP_PKEY_bits@PLT
	leaq	.LC43(%rip), %rsi
	movq	%r12, %rdi
	movl	%eax, %edx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L200
	.p2align 4,,10
	.p2align 3
.L242:
	movq	-104(%rbp), %r13
	leaq	.LC34(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	%r13, %rdi
	call	SSL_get_peer_certificate@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L172
	leaq	.LC35(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movl	c_showcerts(%rip), %r8d
	testl	%r8d, %r8d
	jne	.L173
	jmp	.L205
	.p2align 4,,10
	.p2align 3
.L172:
	leaq	.LC36(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	print_ca_names@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	ssl_print_sigalgs@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	ssl_print_tmp_key@PLT
	movq	%r13, %rdi
	call	SSL_get_wbio@PLT
	movq	%rax, %rdi
	call	BIO_number_written@PLT
	movq	%r13, %rdi
	movq	%rax, %r14
	call	SSL_get_rbio@PLT
	movq	%rax, %rdi
	call	BIO_number_read@PLT
	movq	%r14, %rcx
	leaq	.LC37(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	print_verify_detail@PLT
	movq	%r13, %rdi
	call	SSL_session_reused@PLT
	testl	%eax, %eax
	jne	.L176
	leaq	.LC41(%rip), %rsi
	jmp	.L236
	.p2align 4,,10
	.p2align 3
.L241:
	leaq	.LC49(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_printf@PLT
	jmp	.L193
	.p2align 4,,10
	.p2align 3
.L192:
	leaq	.LC51(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L193
	.p2align 4,,10
	.p2align 3
.L191:
	leaq	.LC50(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L193
	.p2align 4,,10
	.p2align 3
.L243:
	movq	%r13, %rdi
	call	SSL_ct_is_enabled@PLT
	testl	%eax, %eax
	je	.L179
	movq	%r13, %rdi
	call	SSL_get0_peer_scts@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L244
	movq	%rax, %rdi
	call	OPENSSL_sk_num@PLT
	movq	%r12, %rdi
	leaq	.LC38(%rip), %rsi
	movl	%eax, -104(%rbp)
	movl	%eax, %edx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movl	-104(%rbp), %edi
	testl	%edi, %edi
	jle	.L179
	movq	-96(%rbp), %rdi
	call	SSL_CTX_get0_ctlog_store@PLT
	leaq	.LC34(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -112(%rbp)
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movl	-104(%rbp), %ecx
	subl	$1, %ecx
	movl	%ecx, -96(%rbp)
	testl	%ecx, %ecx
	jle	.L180
	movq	%r14, -120(%rbp)
	movq	-112(%rbp), %r14
	movq	%r13, -128(%rbp)
	movl	%ebx, %r13d
	.p2align 4,,10
	.p2align 3
.L181:
	movl	%r13d, %esi
	movq	%r15, %rdi
	addl	$1, %r13d
	call	OPENSSL_sk_value@PLT
	movq	%rax, %rdi
	movq	%rax, %rbx
	call	SCT_validation_status_string@PLT
	leaq	.LC39(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	%r14, %rcx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	SCT_print@PLT
	leaq	.LC40(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	cmpl	-96(%rbp), %r13d
	jl	.L181
	movl	%r13d, %ebx
	movq	-120(%rbp), %r14
	movq	-128(%rbp), %r13
	cmpl	-104(%rbp), %ebx
	jge	.L184
.L180:
	movq	%r14, -96(%rbp)
	movq	-112(%rbp), %r14
	movq	%r13, -112(%rbp)
	movl	%ebx, %r13d
	.p2align 4,,10
	.p2align 3
.L183:
	movl	%r13d, %esi
	movq	%r15, %rdi
	addl	$1, %r13d
	call	OPENSSL_sk_value@PLT
	movq	%rax, %rdi
	movq	%rax, %rbx
	call	SCT_validation_status_string@PLT
	leaq	.LC39(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	%r14, %rcx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	SCT_print@PLT
	cmpl	-104(%rbp), %r13d
	jl	.L183
	movq	-96(%rbp), %r14
	movq	-112(%rbp), %r13
.L184:
	leaq	.LC8(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L179
.L244:
	xorl	%edx, %edx
	leaq	.LC38(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L179
.L240:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1650:
	.size	print_stuff, .-print_stuff
	.section	.rodata.str1.1
.LC61:
	.string	"w"
	.section	.rodata.str1.8
	.align 8
.LC62:
	.string	"Error writing session file %s\n"
	.align 8
.LC63:
	.string	"---\nPost-Handshake New Session Ticket arrived:\n"
	.text
	.p2align 4
	.type	new_session_cb, @function
new_session_cb:
.LFB1648:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	sess_out(%rip), %rdi
	testq	%rdi, %rdi
	je	.L246
	leaq	.LC61(%rip), %rsi
	call	BIO_new_file@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L253
	movq	%rax, %rdi
	movq	%r13, %rsi
	call	PEM_write_bio_SSL_SESSION@PLT
	movq	%r14, %rdi
	call	BIO_free@PLT
.L246:
	movq	%r12, %rdi
	call	SSL_version@PLT
	cmpl	$772, %eax
	je	.L254
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L254:
	.cfi_restore_state
	movq	bio_c_out(%rip), %rdi
	leaq	.LC63(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_c_out(%rip), %rdi
	movq	%r13, %rsi
	call	SSL_SESSION_print@PLT
	movq	bio_c_out(%rip), %rdi
	leaq	.LC34(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L253:
	.cfi_restore_state
	movq	sess_out(%rip), %rdx
	movq	bio_err(%rip), %rdi
	leaq	.LC62(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L246
	.cfi_endproc
.LFE1648:
	.size	new_session_cb, .-new_session_cb
	.section	.rodata.str1.1
.LC64:
	.string	"Can't use SSL_get_servername\n"
	.text
	.p2align 4
	.type	ssl_servername_cb, @function
ssl_servername_cb:
.LFB1637:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdx, %rbx
	subq	$8, %rsp
	call	SSL_get_servername@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	SSL_get_servername_type@PLT
	cmpl	$-1, %eax
	je	.L256
	movq	%r12, %rdi
	call	SSL_session_reused@PLT
	testq	%r13, %r13
	setne	%dl
	testl	%eax, %eax
	sete	%al
	movzbl	%al, %eax
	andl	%edx, %eax
	movl	%eax, 8(%rbx)
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L256:
	.cfi_restore_state
	movq	bio_err(%rip), %rdi
	leaq	.LC64(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1637:
	.size	ssl_servername_cb, .-ssl_servername_cb
	.section	.rodata.str1.1
.LC65:
	.string	"mail.example.com"
.LC66:
	.string	"client"
.LC67:
	.string	"server"
.LC68:
	.string	"localhost"
.LC69:
	.string	"4433"
.LC71:
	.string	"%s: out of memory\n"
.LC72:
	.string	"cbuf"
.LC73:
	.string	"sbuf"
.LC74:
	.string	"mbuf"
	.section	.rodata.str1.8
	.align 8
.LC75:
	.string	"%s: Intermixed protocol flags (unix and internet domains)\n"
	.align 8
.LC76:
	.string	"%s: Intermixed protocol flags (internet and unix domains)\n"
	.align 8
.LC77:
	.string	"Cannot supply multiple protocol flags\n"
	.align 8
.LC78:
	.string	"Cannot supply both a protocol flag and '-no_<prot>'\n"
	.section	.rodata.str1.1
.LC79:
	.string	"%s: Use -help for summary.\n"
.LC80:
	.string	"verify depth is %d\n"
	.section	.rodata.str1.8
	.align 8
.LC81:
	.string	"%s: Memory allocation failure\n"
	.align 8
.LC82:
	.string	"Error getting client auth engine\n"
	.section	.rodata.str1.1
.LC83:
	.string	"Not a hex number '%s'\n"
	.section	.rodata.str1.8
	.align 8
.LC84:
	.string	"SRP minimal length for N is %d\n"
	.align 8
.LC85:
	.string	"%s: Max Fragment Len %u is out of permitted values"
	.section	.rodata.str1.1
.LC86:
	.string	"%s: Can't use both -4 and -6\n"
	.section	.rodata.str1.8
	.align 8
.LC87:
	.string	"%s: Can't use -servername and -noservername together\n"
	.align 8
.LC88:
	.string	"%s: Can't use -dane_tlsa_domain and -noservername together\n"
	.align 8
.LC89:
	.string	"%s: must not provide both -connect option and target parameter\n"
	.align 8
.LC90:
	.string	"Cannot supply -nextprotoneg with TLSv1.3\n"
	.align 8
.LC91:
	.string	"%s: -proxy requires use of -connect or target parameter\n"
	.align 8
.LC92:
	.string	"%s: -proxy argument malformed or ambiguous\n"
	.align 8
.LC93:
	.string	"%s: -connect argument or target parameter malformed or ambiguous\n"
	.align 8
.LC94:
	.string	"%s: -bind argument parameter malformed or ambiguous\n"
	.align 8
.LC95:
	.string	"Can't use unix sockets and datagrams together\n"
	.align 8
.LC96:
	.string	"Error parsing -nextprotoneg argument\n"
	.section	.rodata.str1.1
.LC97:
	.string	"Error getting password\n"
	.section	.rodata.str1.8
	.align 8
.LC98:
	.string	"client certificate private key file"
	.section	.rodata.str1.1
.LC99:
	.string	"client certificate file"
.LC100:
	.string	"client certificate chain"
.LC101:
	.string	"Error loading CRL\n"
.LC102:
	.string	"Error adding CRL\n"
	.section	.rodata.str1.8
	.align 8
.LC103:
	.string	"Error using configuration \"%s\"\n"
	.section	.rodata.str1.1
.LC104:
	.string	"Error setting verify params\n"
	.section	.rodata.str1.8
	.align 8
.LC105:
	.string	"%s: Max send fragment size %u is out of permitted range\n"
	.align 8
.LC106:
	.string	"%s: Split send fragment size %u is out of permitted range\n"
	.align 8
.LC107:
	.string	"%s: Max pipelines %u is out of permitted range\n"
	.align 8
.LC108:
	.string	"%s: Max Fragment Length code %u is out of permitted values\n"
	.align 8
.LC109:
	.string	"Error loading store locations\n"
	.section	.rodata.str1.1
.LC110:
	.string	"Error loading CA names\n"
	.section	.rodata.str1.8
	.align 8
.LC111:
	.string	"Error setting client auth engine\n"
	.align 8
.LC112:
	.string	"PSK key given, setting client callback\n"
	.section	.rodata.str1.1
.LC113:
	.string	"r"
	.section	.rodata.str1.8
	.align 8
.LC114:
	.string	"Can't open PSK session file %s\n"
	.align 8
.LC115:
	.string	"Can't read PSK session file %s\n"
	.section	.rodata.str1.1
.LC116:
	.string	"Error setting SRTP profile\n"
.LC117:
	.string	"Error parsing -alpn argument\n"
.LC118:
	.string	"Error setting ALPN\n"
	.section	.rodata.str1.8
	.align 8
.LC119:
	.string	"Warning: Unable to add custom extension %u, skipping\n"
	.section	.rodata.str1.1
.LC120:
	.string	"Unable to set SRP username\n"
	.section	.rodata.str1.8
	.align 8
.LC121:
	.string	"%s: Error enabling DANE TLSA authentication.\n"
	.section	.rodata.str1.1
.LC122:
	.string	"Can't open session file %s\n"
.LC123:
	.string	"Can't set session\n"
	.section	.rodata.str1.8
	.align 8
.LC124:
	.string	"Unable to set TLS servername extension.\n"
	.align 8
.LC125:
	.string	"%s: DANE TLSA authentication requires at least one -dane_tlsa_rrdata option.\n"
	.align 8
.LC126:
	.string	"%s: warning: bad TLSA %s field in: %s\n"
	.align 8
.LC127:
	.string	"%s: warning: unusable TLSA rrdata: %s\n"
	.align 8
.LC128:
	.string	"%s: warning: error loading TLSA rrdata: %s\n"
	.align 8
.LC129:
	.string	"%s: Failed to import any TLSA records.\n"
	.align 8
.LC130:
	.string	"%s: DANE TLSA authentication requires the -dane_tlsa_domain option.\n"
	.section	.rodata.str1.1
.LC131:
	.string	"connect:errno=%d\n"
.LC132:
	.string	"CONNECTED(%08X)\n"
.LC133:
	.string	"Turned on non blocking io\n"
.LC134:
	.string	"memory allocation failure\n"
.LC135:
	.string	"getsockname:errno=%d\n"
	.section	.rodata.str1.8
	.align 8
.LC137:
	.string	"MTU too small. Must be at least %ld\n"
	.section	.rodata.str1.1
.LC138:
	.string	"Failed to set MTU\n"
.LC139:
	.string	"LHLO %s\r\n"
.LC140:
	.string	"EHLO %s\r\n"
.LC141:
	.string	"STARTTLS"
	.section	.rodata.str1.8
	.align 8
.LC142:
	.string	"Didn't find STARTTLS in server response, trying anyway...\n"
	.section	.rodata.str1.1
.LC143:
	.string	"STARTTLS\r\n"
.LC144:
	.string	"STLS\r\n"
.LC145:
	.string	"BIO_read failed\n"
.LC146:
	.string	". CAPABILITY\r\n"
.LC147:
	.string	". STARTTLS\r\n"
.LC148:
	.string	"AUTH TLS\r\n"
	.section	.rodata.str1.8
	.align 8
.LC149:
	.string	"<stream:stream xmlns:stream='http://etherx.jabber.org/streams' xmlns='jabber:%s' to='%s' version='1.0'>"
	.align 8
.LC150:
	.string	"<starttls xmlns='urn:ietf:params:xml:ns:xmpp-tls'"
	.align 8
.LC151:
	.string	"<starttls xmlns=\"urn:ietf:params:xml:ns:xmpp-tls\""
	.align 8
.LC152:
	.string	"<starttls xmlns='urn:ietf:params:xml:ns:xmpp-tls'/>"
	.section	.rodata.str1.1
.LC153:
	.string	"<proceed"
.LC154:
	.string	"CONNECT %s HTTP/1.0\r\n\r\n"
	.section	.rodata.str1.8
	.align 8
.LC155:
	.string	"%s: HTTP CONNECT failed, insufficient response from proxy (got %d octets)\n"
	.align 8
.LC156:
	.string	"%s: HTTP CONNECT failed, incorrect response from proxy\n"
	.section	.rodata.str1.1
.LC157:
	.string	"%s: HTTP CONNECT failed: %s "
	.section	.rodata.str1.8
	.align 8
.LC159:
	.string	"Timeout waiting for response (%d seconds).\n"
	.section	.rodata.str1.1
.LC160:
	.string	"%*s %d"
.LC161:
	.string	"STARTTLS not supported: %s"
.LC162:
	.string	"STARTTLS negotiation failed: "
	.section	.rodata.str1.8
	.align 8
.LC163:
	.string	"Server does not support STARTTLS.\n"
	.section	.rodata.str1.1
.LC164:
	.string	"MySQL packet too short.\n"
	.section	.rodata.str1.8
	.align 8
.LC165:
	.string	"MySQL packet length does not match.\n"
	.align 8
.LC166:
	.string	"Only MySQL protocol version 10 is supported.\n"
	.align 8
.LC167:
	.string	"Cannot confirm server version. "
	.align 8
.LC168:
	.string	"MySQL server handshake packet is broken.\n"
	.section	.rodata.str1.1
.LC169:
	.string	"MySQL packet is broken.\n"
	.section	.rodata.str1.8
	.align 8
.LC170:
	.string	"MySQL server does not support SSL.\n"
	.section	.rodata.str1.1
.LC171:
	.string	"CAPABILITIES\r\n"
.LC172:
	.string	"382"
.LC173:
	.string	"STARTTLS failed: %s"
.LC174:
	.string	"\"STARTTLS\""
.LC175:
	.string	"OK"
.LC176:
	.string	"NCONF_load_bio failed\n"
.LC177:
	.string	"Error on line %ld\n"
.LC178:
	.string	"asn1"
.LC179:
	.string	"default"
.LC180:
	.string	"NCONF_get_string failed\n"
.LC181:
	.string	"ASN1_generate_nconf failed\n"
.LC182:
	.string	"Unexpected LDAP response\n"
.LC183:
	.string	"No MessageID\n"
.LC184:
	.string	"Not ExtendedResponse\n"
.LC185:
	.string	"Not LDAPResult\n"
	.section	.rodata.str1.8
	.align 8
.LC186:
	.string	"ldap_ExtendedResponse_parse failed\n"
	.align 8
.LC187:
	.string	"STARTTLS failed, LDAP Result Code: %i\n"
	.section	.rodata.str1.1
.LC188:
	.string	"Cannot open early data file\n"
.LC189:
	.string	"Error writing early data\n"
.LC190:
	.string	"CONNECTION ESTABLISHED\n"
	.section	.rodata.str1.8
	.align 8
.LC191:
	.string	"drop connection and then reconnect\n"
	.section	.rodata.str1.1
.LC192:
	.string	"bad select %d\n"
.LC193:
	.string	"write W BLOCK\n"
.LC194:
	.string	"write A BLOCK\n"
.LC195:
	.string	"write R BLOCK\n"
.LC196:
	.string	"write X BLOCK\n"
.LC197:
	.string	"shutdown\n"
.LC198:
	.string	"write:errno=%d\n"
.LC199:
	.string	"DONE\n"
.LC200:
	.string	"read A BLOCK\n"
.LC201:
	.string	"read W BLOCK\n"
.LC202:
	.string	"read R BLOCK\n"
.LC203:
	.string	"read X BLOCK\n"
.LC204:
	.string	"CONNECTION CLOSED BY SERVER\n"
.LC205:
	.string	"read:errno=%d\n"
.LC206:
	.string	"closed\n"
.LC207:
	.string	"RENEGOTIATING\n"
.LC208:
	.string	"KEYUPDATE\n"
.LC210:
	.string	"TIMEOUT occurred\n"
	.text
	.p2align 4
	.globl	s_client_main
	.type	s_client_main, @function
s_client_main:
.LFB1649:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%edi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$1208, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -656(%rbp)
	movq	$0, -648(%rbp)
	call	TLS_client_method@PLT
	movl	$904, %edx
	leaq	.LC14(%rip), %rsi
	leaq	.LC69(%rip), %rdi
	movq	%rax, -848(%rbp)
	movq	$0, -640(%rbp)
	call	CRYPTO_strdup@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edx
	leaq	-512(%rbp), %rdi
	movaps	%xmm0, -544(%rbp)
	movq	%rdx, %rcx
	movdqa	.LC70(%rip), %xmm0
	movq	%rax, -632(%rbp)
	xorl	%eax, %eax
	movq	$0, -624(%rbp)
	movq	$0, -616(%rbp)
	movq	$0, -608(%rbp)
	movl	$32773, -684(%rbp)
	movl	$32773, -680(%rbp)
	movl	$-1, -676(%rbp)
	movl	$0, -672(%rbp)
	movl	$32773, -668(%rbp)
	movq	$0, -560(%rbp)
	movl	$0, -552(%rbp)
	movq	%rdi, -856(%rbp)
	movaps	%xmm0, -528(%rbp)
#APP
# 986 "../deps/openssl/openssl/apps/s_client.c" 1
	cld; rep; stosq
# 0 "" 2
#NO_APP
	leaq	-384(%rbp), %rdi
	movq	%rdx, %rcx
	movq	%rdi, -864(%rbp)
#APP
# 987 "../deps/openssl/openssl/apps/s_client.c" 1
	cld; rep; stosq
# 0 "" 2
#NO_APP
	movq	(%r12), %rdi
	call	opt_progname@PLT
	movl	$0, c_quiet(%rip)
	movq	%rax, prog(%rip)
	movl	$0, c_debug(%rip)
	movl	$0, c_showcerts(%rip)
	call	X509_VERIFY_PARAM_new@PLT
	movq	%rax, -832(%rbp)
	movq	%rax, %rbx
	call	SSL_CONF_CTX_new@PLT
	movq	%rax, -752(%rbp)
	testq	%rbx, %rbx
	je	.L777
	testq	%rax, %rax
	je	.L777
	leaq	.LC72(%rip), %rsi
	movl	$8192, %edi
	xorl	%r15d, %r15d
	xorl	%r14d, %r14d
	call	app_malloc@PLT
	leaq	.LC73(%rip), %rsi
	movl	$8192, %edi
	leaq	.L290(%rip), %rbx
	movq	%rax, -824(%rbp)
	call	app_malloc@PLT
	leaq	.LC74(%rip), %rsi
	movl	$8192, %edi
	movq	%rax, -792(%rbp)
	call	app_malloc@PLT
	movq	-752(%rbp), %rdi
	movl	$5, %esi
	movq	%rax, -800(%rbp)
	call	SSL_CONF_CTX_set_flags@PLT
	movq	%r12, %rsi
	movl	%r13d, %edi
	leaq	s_client_options(%rip), %rdx
	call	opt_init@PLT
	movb	$0, -928(%rbp)
	movl	$2, %r13d
	leaq	.L278(%rip), %r12
	movq	%rax, prog(%rip)
	leaq	-648(%rbp), %rax
	movq	$0, -776(%rbp)
	movq	$0, -768(%rbp)
	movl	$0, -1200(%rbp)
	movq	$0, -968(%rbp)
	movl	$0, -884(%rbp)
	movq	$0, -1064(%rbp)
	movq	$0, -1208(%rbp)
	movq	$0, -808(%rbp)
	movl	$0, -1196(%rbp)
	movl	$0, -1192(%rbp)
	movl	$0, -1052(%rbp)
	movl	$0, -880(%rbp)
	movl	$0, -912(%rbp)
	movl	$0, -1000(%rbp)
	movl	$0, -712(%rbp)
	movl	$0, -996(%rbp)
	movl	$0, -992(%rbp)
	movl	$0, -988(%rbp)
	movl	$0, -1188(%rbp)
	movl	$0, -720(%rbp)
	movl	$0, -872(%rbp)
	movl	$0, -840(%rbp)
	movl	$0, -908(%rbp)
	movq	$0, -1048(%rbp)
	movq	$0, -1184(%rbp)
	movl	$0, -1168(%rbp)
	movq	$0, -1080(%rbp)
	movq	$0, -952(%rbp)
	movl	$0, -940(%rbp)
	movq	$0, -1040(%rbp)
	movq	$0, -1176(%rbp)
	movl	$0, -896(%rbp)
	movq	$0, -904(%rbp)
	movq	$0, -816(%rbp)
	movq	$0, -984(%rbp)
	movq	$0, -1032(%rbp)
	movl	$0, -1164(%rbp)
	movl	$0, -708(%rbp)
	movl	$0, -1136(%rbp)
	movl	$0, -696(%rbp)
	movl	$1, -888(%rbp)
	movl	$0, -876(%rbp)
	movl	$1, -704(%rbp)
	movl	$0, -1116(%rbp)
	movl	$0, -1120(%rbp)
	movl	$0, -1124(%rbp)
	movl	$0, -936(%rbp)
	movl	$0, -944(%rbp)
	movl	$0, -924(%rbp)
	movl	$0, -1092(%rbp)
	movl	$0, -1096(%rbp)
	movl	$0, -1132(%rbp)
	movl	$0, -1128(%rbp)
	movq	$0, -960(%rbp)
	movq	$0, -728(%rbp)
	movq	$0, -976(%rbp)
	movq	$0, -1024(%rbp)
	movq	$0, -1160(%rbp)
	movq	$0, -1104(%rbp)
	movq	$0, -1112(%rbp)
	movq	$0, -1088(%rbp)
	movq	$0, -1144(%rbp)
	movq	$0, -1008(%rbp)
	movq	$0, -1016(%rbp)
	movq	$0, -744(%rbp)
	movq	$0, -736(%rbp)
	movq	$0, -1152(%rbp)
	movq	$0, -1072(%rbp)
	movl	$0, -1056(%rbp)
	movq	$0, -784(%rbp)
	movq	$0, -920(%rbp)
	movq	$0, -760(%rbp)
	movq	%rax, -1216(%rbp)
.L263:
	call	opt_next@PLT
	testl	%eax, %eax
	je	.L1104
.L397:
	cmpl	$1, %r13d
	je	.L1105
	testl	%r13d, %r13d
	jne	.L265
	cmpl	$8, %eax
	je	.L1106
.L265:
	cmpl	$50, %eax
	je	.L266
	cmpl	$55, %eax
	je	.L266
	leal	-52(%rax), %edx
	andl	$-5, %edx
	cmpl	$2, %edx
	jbe	.L266
	movl	%r15d, %ecx
	leal	-3001(%rax), %edx
	andl	$1, %ecx
	cmpl	$4, %edx
	ja	.L270
	testl	%r15d, %r15d
	jne	.L269
	addl	$1, -720(%rbp)
.L287:
	cmpq	$0, -760(%rbp)
	je	.L1107
.L386:
	call	opt_flag@PLT
	movq	-760(%rbp), %rdi
	movq	%rax, %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L391
	call	opt_arg@PLT
	movq	-760(%rbp), %rdi
	movq	%rax, %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	jne	.L263
	jmp	.L391
	.p2align 4,,10
	.p2align 3
.L266:
	cmpl	$1, %r15d
	je	.L268
	leal	-3001(%rax), %edx
	cmpl	$4, %edx
	ja	.L1108
.L269:
	movq	bio_err(%rip), %rdi
	leaq	.LC78(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L1090:
	movq	$0, -728(%rbp)
	movl	$1, %r15d
	xorl	%r13d, %r13d
	movq	$0, -744(%rbp)
	movq	$0, -736(%rbp)
.L697:
	movq	psksess(%rip), %rdi
	call	SSL_SESSION_free@PLT
	movq	next_proto(%rip), %rdi
	movl	$3120, %edx
	leaq	.LC14(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%r13, %rdi
	call	SSL_CTX_free@PLT
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	set_keylog_file@PLT
	movq	-744(%rbp), %rdi
	call	X509_free@PLT
	movq	X509_CRL_free@GOTPCREL(%rip), %rsi
	movq	-728(%rbp), %rdi
	call	OPENSSL_sk_pop_free@PLT
	movq	-736(%rbp), %rdi
	call	EVP_PKEY_free@PLT
	movq	X509_free@GOTPCREL(%rip), %rsi
	movq	-656(%rbp), %rdi
	call	OPENSSL_sk_pop_free@PLT
	movq	-608(%rbp), %rdi
	movl	$3128, %edx
	leaq	.LC14(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-544(%rbp), %rdi
	movl	$3130, %edx
	leaq	.LC14(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-768(%rbp), %rdi
	movl	$3132, %edx
	leaq	.LC14(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-776(%rbp), %rdi
	movl	$3133, %edx
	leaq	.LC14(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-640(%rbp), %rdi
	movl	$3134, %edx
	leaq	.LC14(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-632(%rbp), %rdi
	movl	$3135, %edx
	leaq	.LC14(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-832(%rbp), %rdi
	call	X509_VERIFY_PARAM_free@PLT
	movq	-648(%rbp), %rdi
	call	ssl_excert_free@PLT
	movq	-760(%rbp), %rdi
	call	OPENSSL_sk_free@PLT
	movq	-784(%rbp), %rdi
	call	OPENSSL_sk_free@PLT
	movq	-752(%rbp), %rdi
	call	SSL_CONF_CTX_free@PLT
	movl	$3141, %ecx
	movl	$8192, %esi
	movq	-824(%rbp), %rdi
	leaq	.LC14(%rip), %rdx
	call	CRYPTO_clear_free@PLT
	movl	$3142, %ecx
	movl	$8192, %esi
	movq	-792(%rbp), %rdi
	leaq	.LC14(%rip), %rdx
	call	CRYPTO_clear_free@PLT
	movl	$3143, %ecx
	movl	$8192, %esi
	movq	-800(%rbp), %rdi
	leaq	.LC14(%rip), %rdx
	call	CRYPTO_clear_free@PLT
	movq	-816(%rbp), %rdi
	call	release_engine@PLT
	movq	bio_c_out(%rip), %rdi
	call	BIO_free@PLT
	movq	-808(%rbp), %rdi
	movq	$0, bio_c_out(%rip)
	call	BIO_free@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1109
	leaq	-40(%rbp), %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1108:
	.cfi_restore_state
	movl	$1, %ecx
	movl	$1, %r15d
.L270:
	movl	-720(%rbp), %esi
	testl	%esi, %esi
	je	.L272
	testb	%cl, %cl
	jne	.L269
.L272:
	cmpl	$94, %eax
	jg	.L273
	cmpl	$-1, %eax
	jl	.L263
	leal	1(%rax), %edx
	cmpl	$95, %edx
	ja	.L263
	movslq	(%rbx,%rdx,4), %rdx
	addq	%rbx, %rdx
	notrack jmp	*%rdx
	.section	.rodata
	.align 4
	.align 4
.L290:
	.long	.L380-.L290
	.long	.L263-.L290
	.long	.L379-.L290
	.long	.L378-.L290
	.long	.L377-.L290
	.long	.L376-.L290
	.long	.L375-.L290
	.long	.L374-.L290
	.long	.L373-.L290
	.long	.L372-.L290
	.long	.L298-.L290
	.long	.L371-.L290
	.long	.L370-.L290
	.long	.L369-.L290
	.long	.L368-.L290
	.long	.L367-.L290
	.long	.L366-.L290
	.long	.L365-.L290
	.long	.L364-.L290
	.long	.L363-.L290
	.long	.L362-.L290
	.long	.L361-.L290
	.long	.L360-.L290
	.long	.L724-.L290
	.long	.L359-.L290
	.long	.L358-.L290
	.long	.L357-.L290
	.long	.L356-.L290
	.long	.L355-.L290
	.long	.L354-.L290
	.long	.L353-.L290
	.long	.L352-.L290
	.long	.L351-.L290
	.long	.L263-.L290
	.long	.L350-.L290
	.long	.L349-.L290
	.long	.L348-.L290
	.long	.L347-.L290
	.long	.L346-.L290
	.long	.L345-.L290
	.long	.L344-.L290
	.long	.L343-.L290
	.long	.L342-.L290
	.long	.L341-.L290
	.long	.L340-.L290
	.long	.L339-.L290
	.long	.L338-.L290
	.long	.L337-.L290
	.long	.L336-.L290
	.long	.L335-.L290
	.long	.L334-.L290
	.long	.L333-.L290
	.long	.L332-.L290
	.long	.L331-.L290
	.long	.L330-.L290
	.long	.L329-.L290
	.long	.L328-.L290
	.long	.L327-.L290
	.long	.L326-.L290
	.long	.L325-.L290
	.long	.L263-.L290
	.long	.L324-.L290
	.long	.L323-.L290
	.long	.L322-.L290
	.long	.L321-.L290
	.long	.L320-.L290
	.long	.L319-.L290
	.long	.L318-.L290
	.long	.L317-.L290
	.long	.L316-.L290
	.long	.L315-.L290
	.long	.L314-.L290
	.long	.L313-.L290
	.long	.L312-.L290
	.long	.L311-.L290
	.long	.L310-.L290
	.long	.L309-.L290
	.long	.L308-.L290
	.long	.L307-.L290
	.long	.L306-.L290
	.long	.L305-.L290
	.long	.L304-.L290
	.long	.L303-.L290
	.long	.L302-.L290
	.long	.L301-.L290
	.long	.L300-.L290
	.long	.L299-.L290
	.long	.L298-.L290
	.long	.L297-.L290
	.long	.L296-.L290
	.long	.L295-.L290
	.long	.L294-.L290
	.long	.L293-.L290
	.long	.L292-.L290
	.long	.L291-.L290
	.long	.L289-.L290
	.text
.L724:
	movl	$1, %r14d
	call	opt_next@PLT
	testl	%eax, %eax
	jne	.L397
.L1104:
	cmpl	$1, -712(%rbp)
	jg	.L1110
	movl	-896(%rbp), %r12d
	testl	%r12d, %r12d
	je	.L399
	cmpq	$0, -904(%rbp)
	jne	.L1111
	cmpq	$0, -920(%rbp)
	jne	.L1112
.L399:
	call	opt_num_rest@PLT
	cmpl	$1, %eax
	je	.L1113
	testl	%eax, %eax
	jne	.L380
.L403:
	cmpl	$772, -840(%rbp)
	jne	.L404
	cmpq	$0, -952(%rbp)
	jne	.L1114
.L404:
	cmpq	$0, -736(%rbp)
	movq	-640(%rbp), %r13
	movq	-632(%rbp), %r12
	je	.L405
	cmpq	$0, -768(%rbp)
	je	.L1115
	movq	-736(%rbp), %rdi
	leaq	-632(%rbp), %rdx
	leaq	-640(%rbp), %rsi
	xorl	%ecx, %ecx
	call	BIO_parse_hostserv@PLT
	movl	%eax, %ebx
	cmpq	%r13, -640(%rbp)
	je	.L407
	movl	$1563, %edx
	leaq	.LC14(%rip), %rsi
	movq	%r13, %rdi
	call	CRYPTO_free@PLT
.L407:
	cmpq	%r12, -632(%rbp)
	je	.L408
	movl	$1565, %edx
	leaq	.LC14(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
.L408:
	movq	prog(%rip), %rdx
	leaq	.LC92(%rip), %rsi
	testl	%ebx, %ebx
	je	.L1089
.L409:
	movq	-776(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L413
	leaq	-616(%rbp), %rdx
	leaq	-624(%rbp), %rsi
	xorl	%ecx, %ecx
	call	BIO_parse_hostserv@PLT
	movq	prog(%rip), %rdx
	movq	bio_err(%rip), %rdi
	leaq	.LC94(%rip), %rsi
	testl	%eax, %eax
	je	.L1094
.L413:
	cmpl	$1, -876(%rbp)
	jne	.L414
	cmpl	$1, -888(%rbp)
	jne	.L1116
.L414:
	movq	-952(%rbp), %rsi
	movl	$-1, 16+next_proto(%rip)
	testq	%rsi, %rsi
	je	.L415
	leaq	8+next_proto(%rip), %rdi
	call	next_protos_parse@PLT
	movq	%rax, -736(%rbp)
	movq	%rax, next_proto(%rip)
	testq	%rax, %rax
	je	.L1117
.L416:
	movq	-1112(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	leaq	-608(%rbp), %rdx
	call	app_passwd@PLT
	testl	%eax, %eax
	je	.L1118
	cmpq	$0, -1016(%rbp)
	je	.L1119
	movq	-816(%rbp), %r8
	movq	-608(%rbp), %rcx
	xorl	%edx, %edx
	leaq	.LC98(%rip), %r9
	movl	-680(%rbp), %esi
	movq	-1016(%rbp), %rdi
	call	load_key@PLT
	movq	%rax, -736(%rbp)
	testq	%rax, %rax
	je	.L718
	cmpq	$0, -744(%rbp)
	je	.L420
.L719:
	movl	-684(%rbp), %esi
	movq	-744(%rbp), %rdi
	leaq	.LC99(%rip), %rdx
	call	load_cert@PLT
	movq	%rax, -744(%rbp)
	testq	%rax, %rax
	je	.L1096
.L420:
	cmpq	$0, -1008(%rbp)
	je	.L425
	movq	-1008(%rbp), %rdi
	xorl	%ecx, %ecx
	leaq	-656(%rbp), %rsi
	leaq	.LC100(%rip), %r8
	movl	$32773, %edx
	call	load_certs@PLT
	testl	%eax, %eax
	je	.L1120
.L425:
	cmpq	$0, -728(%rbp)
	je	.L424
	movl	-668(%rbp), %esi
	movq	-728(%rbp), %rdi
	call	load_crl@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L1121
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, -728(%rbp)
	testq	%rax, %rax
	je	.L428
	movq	%rax, %rdi
	movq	%rbx, %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L428
.L424:
	leaq	-648(%rbp), %rdi
	call	load_excert@PLT
	testl	%eax, %eax
	je	.L741
	cmpq	$0, bio_c_out(%rip)
	je	.L1122
.L429:
	leaq	-544(%rbp), %rax
	movq	-1080(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	movq	%rax, %rdx
	movq	%rax, -720(%rbp)
	call	app_passwd@PLT
	testl	%eax, %eax
	je	.L1123
	movq	-848(%rbp), %rdi
	call	SSL_CTX_new@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1101
	xorl	%ecx, %ecx
	movl	$4, %edx
	movl	$78, %esi
	movq	%rax, %rdi
	call	SSL_CTX_ctrl@PLT
	movl	-924(%rbp), %ebx
	testl	%ebx, %ebx
	jne	.L1124
.L433:
	movq	-760(%rbp), %rsi
	movq	-752(%rbp), %rdi
	movq	%r13, %rdx
	movl	$1, %r15d
	call	config_ctx@PLT
	testl	%eax, %eax
	je	.L697
	movq	-1040(%rbp), %rax
	testq	%rax, %rax
	je	.L434
	movq	%rax, %rsi
	movq	%r13, %rdi
	call	SSL_CTX_config@PLT
	testl	%eax, %eax
	je	.L1125
.L434:
	cmpl	$0, -840(%rbp)
	jne	.L435
.L438:
	cmpl	$0, -872(%rbp)
	jne	.L1126
.L437:
	cmpl	$0, -1124(%rbp)
	jne	.L1127
.L440:
	cmpl	$0, -1188(%rbp)
	jne	.L1128
.L441:
	cmpl	$0, -988(%rbp)
	jne	.L1129
.L442:
	cmpl	$0, -992(%rbp)
	jne	.L1130
.L443:
	cmpl	$0, -996(%rbp)
	jne	.L1131
.L444:
	movl	-1136(%rbp), %eax
	testl	%eax, %eax
	jle	.L445
	movslq	%eax, %rsi
	movq	%r13, %rdi
	call	SSL_CTX_set_default_read_buffer_len@PLT
.L445:
	movzbl	-928(%rbp), %eax
	testb	%al, %al
	je	.L446
	movzbl	%al, %r12d
	movq	%r13, %rdi
	movl	%r12d, %esi
	call	SSL_CTX_set_tlsext_max_fragment_length@PLT
	testl	%eax, %eax
	je	.L1132
.L446:
	movl	-696(%rbp), %eax
	pushq	%r9
	movq	%r13, %rdi
	movq	-728(%rbp), %r9
	movq	-1088(%rbp), %r8
	movq	-1144(%rbp), %rcx
	movq	-1160(%rbp), %rdx
	pushq	%rax
	movq	-1104(%rbp), %rsi
	call	ssl_load_stores@PLT
	popq	%r10
	popq	%r11
	testl	%eax, %eax
	je	.L1133
	cmpq	$0, -1024(%rbp)
	je	.L448
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L450
	movq	-1024(%rbp), %rsi
	movq	%rax, %rdi
	call	SSL_add_file_cert_subjects_to_stack@PLT
	testl	%eax, %eax
	je	.L450
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	SSL_CTX_set0_CA_list@PLT
.L448:
	movq	-984(%rbp), %rax
	testq	%rax, %rax
	je	.L451
	movq	%rax, %rsi
	movq	%r13, %rdi
	call	SSL_CTX_set_client_cert_engine@PLT
	testl	%eax, %eax
	je	.L1134
	movq	-984(%rbp), %rdi
	call	ENGINE_free@PLT
.L451:
	cmpq	$0, psk_key(%rip)
	je	.L453
	cmpl	$0, c_debug(%rip)
	jne	.L1135
.L454:
	leaq	psk_client_cb(%rip), %rsi
	movq	%r13, %rdi
	call	SSL_CTX_set_psk_client_callback@PLT
	cmpq	$0, -968(%rbp)
	je	.L455
.L717:
	movq	-968(%rbp), %rdi
	leaq	.LC113(%rip), %rsi
	call	BIO_new_file@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L1136
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	PEM_read_bio_SSL_SESSION@PLT
	movq	%rbx, %rdi
	movq	%rax, psksess(%rip)
	call	BIO_free@PLT
	cmpq	$0, psksess(%rip)
	je	.L1137
.L457:
	leaq	psk_use_session_cb(%rip), %rsi
	movq	%r13, %rdi
	call	SSL_CTX_set_psk_use_session_callback@PLT
.L460:
	movq	-1184(%rbp), %rax
	testq	%rax, %rax
	je	.L461
	movq	%rax, %rsi
	movq	%r13, %rdi
	call	SSL_CTX_set_tlsext_use_srtp@PLT
	testl	%eax, %eax
	jne	.L1138
.L461:
	movq	-648(%rbp), %rsi
	testq	%rsi, %rsi
	je	.L462
	movq	%r13, %rdi
	call	ssl_ctx_set_excert@PLT
.L462:
	cmpq	$0, next_proto(%rip)
	je	.L463
	leaq	next_proto(%rip), %rdx
	leaq	next_proto_cb(%rip), %rsi
	movq	%r13, %rdi
	call	SSL_CTX_set_next_proto_select_cb@PLT
.L463:
	movq	-1176(%rbp), %rsi
	testq	%rsi, %rsi
	je	.L464
	leaq	-576(%rbp), %r12
	movq	%r12, %rdi
	call	next_protos_parse@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1139
	movl	-576(%rbp), %edx
	movq	%rax, %rsi
	movq	%r13, %rdi
	call	SSL_CTX_set_alpn_protos@PLT
	testl	%eax, %eax
	jne	.L1140
	movl	$1863, %edx
	leaq	.LC14(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
.L464:
	xorl	%r15d, %r15d
	cmpl	$0, -940(%rbp)
	leaq	-256(%rbp), %r12
	leaq	serverinfo_cli_parse_cb(%rip), %rbx
	jne	.L469
	jmp	.L472
.L468:
	addq	$1, %r15
	cmpl	%r15d, -940(%rbp)
	jle	.L472
.L469:
	movzwl	(%r12,%r15,2), %r10d
	pushq	%rsi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	pushq	$0
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	%rbx, %r9
	movl	%r10d, %esi
	movl	%r10d, -840(%rbp)
	call	SSL_CTX_add_client_custom_ext@PLT
	popq	%rdi
	movl	-840(%rbp), %r10d
	testl	%eax, %eax
	popq	%r8
	jne	.L468
	movq	bio_err(%rip), %rdi
	movl	%r10d, %edx
	leaq	.LC119(%rip), %rsi
	call	BIO_printf@PLT
	jmp	.L468
.L367:
	movl	$1, -696(%rbp)
	jmp	.L263
	.p2align 4,,10
	.p2align 3
.L273:
	cmpl	$3041, %eax
	jg	.L263
	cmpl	$3000, %eax
	jle	.L1141
	cmpl	$40, %edx
	ja	.L263
	movslq	(%r12,%rdx,4), %rax
	addq	%r12, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L278:
	.long	.L287-.L278
	.long	.L287-.L278
	.long	.L287-.L278
	.long	.L287-.L278
	.long	.L287-.L278
	.long	.L287-.L278
	.long	.L287-.L278
	.long	.L287-.L278
	.long	.L287-.L278
	.long	.L287-.L278
	.long	.L287-.L278
	.long	.L287-.L278
	.long	.L287-.L278
	.long	.L287-.L278
	.long	.L287-.L278
	.long	.L287-.L278
	.long	.L287-.L278
	.long	.L287-.L278
	.long	.L287-.L278
	.long	.L287-.L278
	.long	.L287-.L278
	.long	.L287-.L278
	.long	.L287-.L278
	.long	.L287-.L278
	.long	.L287-.L278
	.long	.L287-.L278
	.long	.L287-.L278
	.long	.L287-.L278
	.long	.L287-.L278
	.long	.L287-.L278
	.long	.L263-.L278
	.long	.L723-.L278
	.long	.L286-.L278
	.long	.L285-.L278
	.long	.L284-.L278
	.long	.L283-.L278
	.long	.L282-.L278
	.long	.L281-.L278
	.long	.L280-.L278
	.long	.L279-.L278
	.long	.L277-.L278
	.text
.L286:
	movl	$0, -704(%rbp)
	jmp	.L263
.L723:
	movl	$1, -708(%rbp)
	jmp	.L263
	.p2align 4,,10
	.p2align 3
.L1141:
	cmpl	$1502, %eax
	jg	.L382
	cmpl	$1500, %eax
	jg	.L383
	leal	-1001(%rax), %edx
	cmpl	$5, %edx
	ja	.L263
	movq	-1216(%rbp), %rsi
	movl	%eax, %edi
	call	args_excert@PLT
	testl	%eax, %eax
	jne	.L263
	jmp	.L1090
	.p2align 4,,10
	.p2align 3
.L382:
	leal	-2001(%rax), %edx
	cmpl	$29, %edx
	ja	.L263
	movq	-832(%rbp), %rsi
	movl	%eax, %edi
	call	opt_verify@PLT
	testl	%eax, %eax
	je	.L1090
	addl	$1, -1124(%rbp)
	jmp	.L263
	.p2align 4,,10
	.p2align 3
.L1105:
	leal	-2(%rax), %edx
	cmpl	$4, %edx
	ja	.L265
	movq	prog(%rip), %rdx
	leaq	.LC75(%rip), %rsi
	jmp	.L1089
	.p2align 4,,10
	.p2align 3
.L777:
	movq	prog(%rip), %rdx
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
	xorl	%r13d, %r13d
	leaq	.LC71(%rip), %rsi
	movl	$1, %r15d
	call	BIO_printf@PLT
	movq	$0, -776(%rbp)
	movq	$0, -768(%rbp)
	movq	$0, -808(%rbp)
	movq	$0, -816(%rbp)
	movq	$0, -800(%rbp)
	movq	$0, -792(%rbp)
	movq	$0, -824(%rbp)
	movq	$0, -728(%rbp)
	movq	$0, -784(%rbp)
	movq	$0, -760(%rbp)
	movq	$0, -744(%rbp)
	movq	$0, -736(%rbp)
	jmp	.L697
.L356:
	call	opt_arg@PLT
	movq	%rax, %rdi
	call	ENGINE_by_id@PLT
	movq	%rax, -984(%rbp)
	testq	%rax, %rax
	jne	.L263
	movq	bio_err(%rip), %rdi
	leaq	.LC82(%rip), %rsi
	call	BIO_printf@PLT
.L380:
	movq	prog(%rip), %rdx
	leaq	.LC79(%rip), %rsi
.L1089:
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
.L1094:
	call	BIO_printf@PLT
	jmp	.L1090
	.p2align 4,,10
	.p2align 3
.L1106:
	movq	prog(%rip), %rdx
	leaq	.LC76(%rip), %rsi
	jmp	.L1089
	.p2align 4,,10
	.p2align 3
.L268:
	movq	bio_err(%rip), %rdi
	leaq	.LC77(%rip), %rsi
	xorl	%eax, %eax
	xorl	%r13d, %r13d
	call	BIO_printf@PLT
	movq	$0, -728(%rbp)
	movq	$0, -744(%rbp)
	movq	$0, -736(%rbp)
	jmp	.L697
.L379:
	leaq	s_client_options(%rip), %rdi
	xorl	%r15d, %r15d
	xorl	%r13d, %r13d
	call	opt_help@PLT
	movq	$0, -728(%rbp)
	movq	$0, -744(%rbp)
	movq	$0, -736(%rbp)
	jmp	.L697
.L277:
	movl	$1, -1200(%rbp)
	jmp	.L263
.L279:
	movl	$1, -1056(%rbp)
	jmp	.L263
.L280:
	cmpq	$0, -784(%rbp)
	je	.L1142
.L390:
	call	opt_arg@PLT
	movq	-784(%rbp), %rdi
	movq	%rax, %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	jne	.L263
.L391:
	movq	prog(%rip), %rdx
	leaq	.LC81(%rip), %rsi
	jmp	.L1089
.L281:
	call	opt_arg@PLT
	movq	%rax, -1048(%rbp)
	jmp	.L263
.L282:
	movl	$0, -908(%rbp)
	jmp	.L263
.L283:
	movl	$1, -908(%rbp)
	jmp	.L263
.L284:
	call	opt_arg@PLT
	movq	%rax, -920(%rbp)
	jmp	.L263
.L285:
	call	opt_arg@PLT
	movl	$8, -672(%rbp)
	movq	%rax, -736(%rbp)
	jmp	.L263
.L289:
	call	opt_arg@PLT
	movq	%rax, -1024(%rbp)
	jmp	.L263
.L299:
	call	opt_arg@PLT
	movl	$10, %edx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	strtol@PLT
	movl	%eax, keymatexportlen(%rip)
	jmp	.L263
.L300:
	call	opt_arg@PLT
	movq	%rax, keymatexportlabel(%rip)
	jmp	.L263
.L301:
	call	opt_arg@PLT
	movq	%rax, -1184(%rbp)
	jmp	.L263
.L302:
	movl	$1, -1188(%rbp)
	jmp	.L263
.L303:
	movl	$1, -896(%rbp)
	jmp	.L263
.L304:
	call	opt_arg@PLT
	movq	%rax, -904(%rbp)
	jmp	.L263
.L305:
	call	opt_arg@PLT
	leaq	-672(%rbp), %rdx
	leaq	services(%rip), %rsi
	movq	%rax, %rdi
	call	opt_pair@PLT
	testl	%eax, %eax
	jne	.L263
	jmp	.L1090
.L306:
	call	opt_arg@PLT
	movq	%rax, %rdi
	movq	%rax, -1224(%rbp)
	call	strlen@PLT
	movl	%eax, %r8d
	testl	%eax, %eax
	js	.L263
	movq	-1224(%rbp), %r9
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	movl	%r14d, -1232(%rbp)
	movl	%r13d, -1236(%rbp)
	movq	%rcx, %r14
	movl	-940(%rbp), %r13d
	movl	%r15d, -1240(%rbp)
	movq	%r9, %r15
	jmp	.L394
	.p2align 4,,10
	.p2align 3
.L393:
	addq	$1, %r14
	cmpl	%r8d, %edx
	jg	.L1143
.L394:
	cmpl	%r14d, %r8d
	je	.L392
	cmpb	$44, (%r15,%r14)
	leal	1(%r14), %edx
	jne	.L393
.L392:
	cltq
	movl	$10, %edx
	xorl	%esi, %esi
	movl	%r8d, -940(%rbp)
	leaq	(%r15,%rax), %rdi
	call	strtol@PLT
	movq	%rax, %r11
	movslq	%r13d, %rax
	addl	$1, %r13d
	movw	%r11w, -256(%rbp,%rax,2)
	cmpl	$100, %r13d
	je	.L1064
	leal	1(%r14), %eax
	movl	-940(%rbp), %r8d
	movl	%eax, %edx
	jmp	.L393
.L307:
	call	opt_arg@PLT
	movq	%rax, -1176(%rbp)
	jmp	.L263
.L308:
	call	opt_arg@PLT
	movq	%rax, -952(%rbp)
	jmp	.L263
.L311:
	movl	$1, -1132(%rbp)
	jmp	.L263
.L312:
	call	opt_arg@PLT
	movq	%rax, -1152(%rbp)
	jmp	.L263
.L313:
	movl	$1, -1096(%rbp)
	jmp	.L263
.L314:
	movl	$5, -944(%rbp)
	jmp	.L263
.L309:
	call	opt_arg@PLT
	movq	%rax, -1160(%rbp)
	jmp	.L263
.L310:
	call	opt_arg@PLT
	movq	%rax, -1088(%rbp)
	jmp	.L263
.L294:
	call	opt_arg@PLT
	movl	$10, %edx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	strtol@PLT
	movl	%eax, -996(%rbp)
	jmp	.L263
.L295:
	call	opt_arg@PLT
	movl	$10, %edx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	strtol@PLT
	movl	%eax, -992(%rbp)
	jmp	.L263
.L296:
	call	opt_arg@PLT
	movl	$10, %edx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	strtol@PLT
	movl	%eax, -988(%rbp)
	jmp	.L263
.L297:
	call	opt_arg@PLT
	movl	$10, %edx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	strtol@PLT
	movl	%eax, %ecx
	cmpl	$2048, %eax
	je	.L736
	jg	.L395
	cmpl	$512, %eax
	je	.L737
	cmpl	$1024, %eax
	jne	.L396
	movb	$2, -928(%rbp)
	jmp	.L263
.L292:
	call	opt_arg@PLT
	movq	%rax, -1208(%rbp)
	jmp	.L263
.L293:
	call	opt_arg@PLT
	movl	$10, %edx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	strtol@PLT
	movl	%eax, -1136(%rbp)
	jmp	.L263
.L291:
	call	opt_arg@PLT
	movq	%rax, -1064(%rbp)
	jmp	.L263
.L315:
	call	opt_arg@PLT
	movq	%rax, -1016(%rbp)
	jmp	.L263
.L316:
	call	opt_arg@PLT
	movq	%rax, -1104(%rbp)
	jmp	.L263
.L317:
	call	opt_arg@PLT
	movq	%rax, -1144(%rbp)
	jmp	.L263
.L318:
	movl	$1, -1128(%rbp)
	jmp	.L263
.L319:
	call	opt_arg@PLT
	movq	%rax, -1072(%rbp)
	jmp	.L263
.L320:
	call	opt_arg@PLT
	movq	%rax, -1008(%rbp)
	jmp	.L263
.L321:
	call	opt_arg@PLT
	movq	%rax, -1112(%rbp)
	jmp	.L263
.L322:
	call	opt_arg@PLT
	leaq	-680(%rbp), %rdx
	movl	$18, %esi
	movq	%rax, %rdi
	call	opt_format@PLT
	testl	%eax, %eax
	jne	.L263
	jmp	.L380
.L323:
	call	opt_arg@PLT
	movl	$10, %edx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	strtol@PLT
	movq	%rax, -1032(%rbp)
	jmp	.L263
.L324:
	movl	$1, -1164(%rbp)
	jmp	.L263
.L325:
	call	DTLS_client_method@PLT
	movl	$1, -884(%rbp)
	movq	%rax, -848(%rbp)
	movl	$65277, -872(%rbp)
	movl	$65277, -840(%rbp)
	movl	$2, -888(%rbp)
	jmp	.L263
.L326:
	call	DTLS_client_method@PLT
	movl	$1, -884(%rbp)
	movq	%rax, -848(%rbp)
	movl	$65279, -872(%rbp)
	movl	$65279, -840(%rbp)
	movl	$2, -888(%rbp)
	jmp	.L263
.L327:
	call	DTLS_client_method@PLT
	movl	$1, -884(%rbp)
	movq	%rax, -848(%rbp)
	movl	$2, -888(%rbp)
	jmp	.L263
.L328:
	movl	$769, -872(%rbp)
	movl	$769, -840(%rbp)
	jmp	.L263
.L329:
	movl	$770, -872(%rbp)
	movl	$770, -840(%rbp)
	jmp	.L263
.L330:
	movl	$771, -872(%rbp)
	movl	$771, -840(%rbp)
	jmp	.L263
.L331:
	movl	$772, -872(%rbp)
	movl	$772, -840(%rbp)
	jmp	.L263
.L332:
	call	opt_arg@PLT
	movq	%rax, -1040(%rbp)
	jmp	.L263
.L333:
	movl	$768, -872(%rbp)
	movl	$768, -840(%rbp)
	jmp	.L263
.L334:
	movl	-840(%rbp), %ecx
	movl	$769, %eax
	movl	$1, -520(%rbp)
	cmpl	$769, %ecx
	cmovge	%ecx, %eax
	movl	%eax, -840(%rbp)
	jmp	.L263
.L335:
	movl	-840(%rbp), %ecx
	movl	$769, %eax
	movl	$1, -1168(%rbp)
	cmpl	$769, %ecx
	cmovge	%ecx, %eax
	movl	%eax, -840(%rbp)
	jmp	.L263
.L336:
	call	opt_arg@PLT
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%rax, %rdi
	call	strtol@PLT
	movq	bio_err(%rip), %rdi
	leaq	.LC84(%rip), %rsi
	movl	%eax, -516(%rbp)
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movl	-840(%rbp), %ecx
	movl	$769, %eax
	cmpl	$769, %ecx
	cmovge	%ecx, %eax
	movl	%eax, -840(%rbp)
	jmp	.L263
.L337:
	call	opt_arg@PLT
	movl	-840(%rbp), %ecx
	movq	%rax, -1080(%rbp)
	movl	$769, %eax
	cmpl	$769, %ecx
	cmovge	%ecx, %eax
	movl	%eax, -840(%rbp)
	jmp	.L263
.L338:
	call	opt_arg@PLT
	movl	-840(%rbp), %ecx
	movq	%rax, -536(%rbp)
	movl	$769, %eax
	cmpl	$769, %ecx
	cmovge	%ecx, %eax
	movl	%eax, -840(%rbp)
	jmp	.L263
.L339:
	call	opt_arg@PLT
	movq	%rax, -968(%rbp)
	jmp	.L263
.L340:
	call	opt_arg@PLT
	movq	%rax, psk_key(%rip)
	movzbl	(%rax), %edx
	movq	%rax, -1232(%rbp)
	testb	%dl, %dl
	movb	%dl, -1224(%rbp)
	je	.L263
	call	__ctype_b_loc@PLT
	movq	-1232(%rbp), %r8
	movzbl	-1224(%rbp), %edx
	movq	(%rax), %rcx
	movq	%r8, %rax
.L389:
	testb	$16, 1(%rcx,%rdx,2)
	je	.L388
	movzbl	1(%rax), %edx
	addq	$1, %rax
	testb	%dl, %dl
	jne	.L389
	jmp	.L263
.L341:
	call	opt_arg@PLT
	movq	%rax, psk_identity(%rip)
	jmp	.L263
.L342:
	movl	$1, -1116(%rbp)
	jmp	.L263
.L343:
	movl	$1, -1120(%rbp)
	jmp	.L263
.L344:
	movl	$1, c_showcerts(%rip)
	jmp	.L263
.L345:
	movl	$2, -924(%rbp)
	jmp	.L263
.L346:
	movl	$1, -924(%rbp)
	jmp	.L263
.L347:
	movl	$2, -912(%rbp)
	jmp	.L263
.L348:
	call	opt_arg@PLT
	movl	$1, %esi
	movq	%rax, %rdi
	call	setup_engine@PLT
	movq	%rax, -816(%rbp)
	jmp	.L263
.L349:
	call	opt_arg@PLT
	leaq	.LC61(%rip), %rsi
	movq	%rax, %rdi
	call	BIO_new_file@PLT
	movq	%rax, -808(%rbp)
	jmp	.L263
.L350:
	movl	$1, -912(%rbp)
	jmp	.L263
.L351:
	movl	$1, -1196(%rbp)
	jmp	.L263
.L352:
	movl	$1, -1192(%rbp)
	jmp	.L263
.L353:
	movl	$1, c_debug(%rip)
	jmp	.L263
.L354:
	movl	$0, -880(%rbp)
	jmp	.L263
.L355:
	movl	$1, -880(%rbp)
	jmp	.L263
.L357:
	movl	$1, -1000(%rbp)
	jmp	.L263
.L358:
	movl	$1, c_quiet(%rip)
	movl	$1, -880(%rbp)
	jmp	.L263
.L359:
	movl	$1, -1092(%rbp)
	jmp	.L263
.L360:
	movl	$1, c_quiet(%rip)
	movl	$1, 4+verify_args(%rip)
	movl	$1, -1052(%rbp)
	jmp	.L263
.L361:
	movl	$1, 4+verify_args(%rip)
	jmp	.L263
.L362:
	movl	$1, 12+verify_args(%rip)
	movl	$1, -936(%rbp)
	jmp	.L263
.L363:
	call	opt_arg@PLT
	leaq	-668(%rbp), %rdx
	movl	$2, %esi
	movq	%rax, %rdi
	call	opt_format@PLT
	testl	%eax, %eax
	jne	.L263
	jmp	.L380
.L364:
	call	opt_arg@PLT
	leaq	-684(%rbp), %rdx
	movl	$2, %esi
	movq	%rax, %rdi
	call	opt_format@PLT
	testl	%eax, %eax
	jne	.L263
	jmp	.L380
.L365:
	call	opt_arg@PLT
	movq	%rax, -976(%rbp)
	jmp	.L263
.L366:
	call	opt_arg@PLT
	movq	%rax, sess_out(%rip)
	jmp	.L263
.L368:
	call	opt_arg@PLT
	movq	%rax, -728(%rbp)
	jmp	.L263
.L369:
	call	opt_arg@PLT
	movq	%rax, -744(%rbp)
	jmp	.L263
.L370:
	call	opt_arg@PLT
	movq	%rax, %rdi
	call	set_nameopt@PLT
	testl	%eax, %eax
	jne	.L263
	jmp	.L1090
.L371:
	call	opt_arg@PLT
	movl	$10, %edx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	strtol@PLT
	movl	c_quiet(%rip), %edx
	movl	$1, -936(%rbp)
	movl	%eax, verify_args(%rip)
	testl	%edx, %edx
	jne	.L263
	movq	bio_err(%rip), %rdi
	movl	%eax, %edx
	leaq	.LC80(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L263
.L298:
	call	opt_arg@PLT
	movq	%rax, -960(%rbp)
	jmp	.L263
.L372:
	call	opt_arg@PLT
	movq	-640(%rbp), %rdi
	movl	$844, %edx
	leaq	.LC14(%rip), %rsi
	movq	%rax, %r13
	call	CRYPTO_free@PLT
	movq	$0, -640(%rbp)
	testq	%r13, %r13
	je	.L729
	movq	%r13, %rdi
	movl	$847, %edx
	leaq	.LC14(%rip), %rsi
	movl	$1, %r13d
	call	CRYPTO_strdup@PLT
	movl	$1, -876(%rbp)
	movq	%rax, -640(%rbp)
	jmp	.L263
.L373:
	call	opt_arg@PLT
	movq	-776(%rbp), %rdi
	movl	$844, %edx
	leaq	.LC14(%rip), %rsi
	movq	%rax, -1224(%rbp)
	call	CRYPTO_free@PLT
	movq	-1224(%rbp), %rax
	movq	$0, -776(%rbp)
	testq	%rax, %rax
	je	.L263
	movl	$847, %edx
	leaq	.LC14(%rip), %rsi
	movq	%rax, %rdi
	call	CRYPTO_strdup@PLT
	movq	%rax, -776(%rbp)
	jmp	.L263
.L375:
	call	opt_arg@PLT
	movq	-632(%rbp), %rdi
	movl	$844, %edx
	xorl	%r13d, %r13d
	leaq	.LC14(%rip), %rsi
	movq	%rax, -1224(%rbp)
	call	CRYPTO_free@PLT
	movq	-1224(%rbp), %rax
	movq	$0, -632(%rbp)
	testq	%rax, %rax
	je	.L263
	movl	$847, %edx
	leaq	.LC14(%rip), %rsi
	movq	%rax, %rdi
	call	CRYPTO_strdup@PLT
	movq	%rax, -632(%rbp)
	jmp	.L263
.L376:
	call	opt_arg@PLT
	movq	-640(%rbp), %rdi
	movl	$844, %edx
	xorl	%r13d, %r13d
	leaq	.LC14(%rip), %rsi
	movq	%rax, -1224(%rbp)
	call	CRYPTO_free@PLT
	movq	-1224(%rbp), %rax
	movq	$0, -640(%rbp)
	testq	%rax, %rax
	je	.L263
	movl	$847, %edx
	leaq	.LC14(%rip), %rsi
	movq	%rax, %rdi
	call	CRYPTO_strdup@PLT
	movq	%rax, -640(%rbp)
	jmp	.L263
.L377:
	addl	$1, -712(%rbp)
	xorl	%r13d, %r13d
	movl	$10, -876(%rbp)
	jmp	.L263
.L378:
	addl	$1, -712(%rbp)
	xorl	%r13d, %r13d
	movl	%eax, -876(%rbp)
	jmp	.L263
.L374:
	call	opt_arg@PLT
	movq	-768(%rbp), %rdi
	movl	$844, %edx
	leaq	.LC14(%rip), %rsi
	movq	%rax, %r13
	call	CRYPTO_free@PLT
	testq	%r13, %r13
	je	.L727
	movq	%r13, %rdi
	movl	$847, %edx
	leaq	.LC14(%rip), %rsi
	xorl	%r13d, %r13d
	call	CRYPTO_strdup@PLT
	movq	%rax, -768(%rbp)
	jmp	.L263
.L1111:
	movq	prog(%rip), %rdx
	movq	bio_err(%rip), %rdi
	leaq	.LC87(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L380
	.p2align 4,,10
	.p2align 3
.L383:
	movl	%eax, %edi
	call	opt_rand@PLT
	testl	%eax, %eax
	jne	.L263
	jmp	.L1090
	.p2align 4,,10
	.p2align 3
.L395:
	cmpl	$4096, %eax
	jne	.L396
	movb	$4, -928(%rbp)
	jmp	.L263
.L388:
	movq	%r8, %rdx
	leaq	.LC83(%rip), %rsi
	jmp	.L1089
.L1143:
	movl	%r13d, -940(%rbp)
	movl	-1232(%rbp), %r14d
	movl	-1236(%rbp), %r13d
	movl	-1240(%rbp), %r15d
	jmp	.L263
.L1110:
	movq	prog(%rip), %rdx
	movq	bio_err(%rip), %rdi
	leaq	.LC86(%rip), %rsi
	call	BIO_printf@PLT
	jmp	.L380
.L727:
	movq	$0, -768(%rbp)
	xorl	%r13d, %r13d
	jmp	.L263
.L729:
	movl	$1, -876(%rbp)
	movl	$1, %r13d
	jmp	.L263
.L737:
	movb	$1, -928(%rbp)
	jmp	.L263
.L1113:
	cmpq	$0, -768(%rbp)
	je	.L402
	movq	prog(%rip), %rdx
	movq	bio_err(%rip), %rdi
	leaq	.LC89(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L380
.L1064:
	movl	%r13d, -940(%rbp)
	movl	-1232(%rbp), %r14d
	movl	-1236(%rbp), %r13d
	movl	-1240(%rbp), %r15d
	jmp	.L263
.L1112:
	movq	prog(%rip), %rdx
	movq	bio_err(%rip), %rdi
	leaq	.LC88(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L380
.L736:
	movb	$3, -928(%rbp)
	jmp	.L263
.L396:
	movq	prog(%rip), %rdx
	movq	bio_err(%rip), %rdi
	leaq	.LC85(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L380
.L1107:
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, -760(%rbp)
	testq	%rax, %rax
	jne	.L386
	jmp	.L391
.L1142:
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, -784(%rbp)
	testq	%rax, %rax
	jne	.L390
	jmp	.L391
.L402:
	call	opt_rest@PLT
	xorl	%edi, %edi
	movl	$844, %edx
	leaq	.LC14(%rip), %rsi
	movq	(%rax), %rax
	movq	%rax, %rbx
	movq	%rax, -768(%rbp)
	call	CRYPTO_free@PLT
	testq	%rbx, %rbx
	je	.L403
	movl	$847, %edx
	leaq	.LC14(%rip), %rsi
	movq	%rbx, %rdi
	call	CRYPTO_strdup@PLT
	movq	%rax, -768(%rbp)
	jmp	.L403
.L405:
	movq	-768(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L409
	leaq	-632(%rbp), %rdx
	leaq	-640(%rbp), %rsi
	xorl	%ecx, %ecx
	call	BIO_parse_hostserv@PLT
	movl	%eax, %ebx
	cmpq	%r13, -640(%rbp)
	je	.L1077
	movl	$1578, %edx
	leaq	.LC14(%rip), %rsi
	movq	%r13, %rdi
	call	CRYPTO_free@PLT
.L1077:
	movq	-632(%rbp), %rax
	cmpq	%rax, %r12
	je	.L412
	movl	$1580, %edx
	leaq	.LC14(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
.L412:
	testl	%ebx, %ebx
	jne	.L409
	movq	prog(%rip), %rdx
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
	xorl	%r13d, %r13d
	leaq	.LC93(%rip), %rsi
	movl	$1, %r15d
	call	BIO_printf@PLT
	movq	$0, -728(%rbp)
	movq	$0, -744(%rbp)
	jmp	.L697
.L1114:
	movq	bio_err(%rip), %rdi
	leaq	.LC90(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L380
.L415:
	movq	$0, next_proto(%rip)
	jmp	.L416
.L1119:
	cmpq	$0, -744(%rbp)
	jne	.L419
	movq	$0, -736(%rbp)
	jmp	.L420
.L1115:
	movq	prog(%rip), %rdx
	movq	bio_err(%rip), %rdi
	leaq	.LC91(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L380
.L1116:
	movq	bio_err(%rip), %rdi
	leaq	.LC95(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L1090
.L1118:
	movq	bio_err(%rip), %rdi
	leaq	.LC97(%rip), %rsi
	call	BIO_printf@PLT
	jmp	.L1090
.L428:
	movq	bio_err(%rip), %rdi
	leaq	.LC102(%rip), %rsi
	movl	$1, %r15d
	xorl	%r13d, %r13d
	call	BIO_puts@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	movq	%rbx, %rdi
	call	X509_CRL_free@PLT
	jmp	.L697
.L1120:
	movq	$0, -728(%rbp)
	movl	$1, %r15d
	xorl	%r13d, %r13d
	jmp	.L697
.L1123:
	movq	bio_err(%rip), %rdi
	leaq	.LC97(%rip), %rsi
	movl	$1, %r15d
	xorl	%r13d, %r13d
	call	BIO_printf@PLT
	jmp	.L697
.L741:
	movl	$1, %r15d
	xorl	%r13d, %r13d
	jmp	.L697
.L1121:
	movq	bio_err(%rip), %rdi
	leaq	.LC101(%rip), %rsi
	call	BIO_puts@PLT
.L1096:
	movq	bio_err(%rip), %rdi
	xorl	%r13d, %r13d
	call	ERR_print_errors@PLT
	movq	$0, -728(%rbp)
.L1099:
	movl	$1, %r15d
	jmp	.L697
.L419:
	movq	-816(%rbp), %r8
	movq	-608(%rbp), %rcx
	xorl	%edx, %edx
	leaq	.LC98(%rip), %r9
	movl	-680(%rbp), %esi
	movq	-744(%rbp), %rdi
	call	load_key@PLT
	movq	%rax, -736(%rbp)
	testq	%rax, %rax
	jne	.L719
.L718:
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	jmp	.L1090
.L1109:
	call	__stack_chk_fail@PLT
.L1127:
	movq	-832(%rbp), %rsi
	movq	%r13, %rdi
	call	SSL_CTX_set1_param@PLT
	leaq	.LC104(%rip), %rsi
	testl	%eax, %eax
	jne	.L440
.L1098:
	movq	bio_err(%rip), %rdi
	call	BIO_printf@PLT
.L1101:
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	jmp	.L1099
.L1122:
	cmpl	$0, c_quiet(%rip)
	je	.L430
	cmpl	$0, c_debug(%rip)
	je	.L1144
.L430:
	movl	$32769, %edi
	call	dup_bio_out@PLT
	movq	%rax, bio_c_out(%rip)
	jmp	.L429
.L1117:
	movq	bio_err(%rip), %rdi
	leaq	.LC96(%rip), %rsi
	xorl	%r13d, %r13d
	movl	$1, %r15d
	call	BIO_printf@PLT
	movq	$0, -728(%rbp)
	movq	$0, -744(%rbp)
	jmp	.L697
.L1124:
	movl	-924(%rbp), %esi
	movq	%r13, %rdi
	call	ssl_ctx_security_debug@PLT
	jmp	.L433
.L435:
	movslq	-840(%rbp), %rdx
	xorl	%ecx, %ecx
	movl	$123, %esi
	movq	%r13, %rdi
	call	SSL_CTX_ctrl@PLT
	testq	%rax, %rax
	jne	.L438
	jmp	.L1099
.L1126:
	movslq	-872(%rbp), %rdx
	xorl	%ecx, %ecx
	movl	$124, %esi
	movq	%r13, %rdi
	call	SSL_CTX_ctrl@PLT
	testq	%rax, %rax
	jne	.L437
	jmp	.L1099
.L1131:
	movl	-996(%rbp), %edx
	xorl	%ecx, %ecx
	movl	$126, %esi
	movq	%r13, %rdi
	call	SSL_CTX_ctrl@PLT
	testq	%rax, %rax
	jne	.L444
	movl	-996(%rbp), %ecx
	movq	prog(%rip), %rdx
	leaq	.LC107(%rip), %rsi
	movl	$1, %r15d
	movq	bio_err(%rip), %rdi
	call	BIO_printf@PLT
	jmp	.L697
.L1130:
	movl	-992(%rbp), %edx
	xorl	%ecx, %ecx
	movl	$125, %esi
	movq	%r13, %rdi
	call	SSL_CTX_ctrl@PLT
	testq	%rax, %rax
	jne	.L443
	movl	-992(%rbp), %ecx
	movq	prog(%rip), %rdx
	leaq	.LC106(%rip), %rsi
	movl	$1, %r15d
	movq	bio_err(%rip), %rdi
	call	BIO_printf@PLT
	jmp	.L697
.L1129:
	movl	-988(%rbp), %edx
	xorl	%ecx, %ecx
	movl	$52, %esi
	movq	%r13, %rdi
	call	SSL_CTX_ctrl@PLT
	testq	%rax, %rax
	jne	.L442
	movl	-988(%rbp), %ecx
	movq	prog(%rip), %rdx
	leaq	.LC105(%rip), %rsi
	movl	$1, %r15d
	movq	bio_err(%rip), %rdi
	call	BIO_printf@PLT
	jmp	.L697
.L1128:
	xorl	%ecx, %ecx
	movl	$256, %edx
	movl	$33, %esi
	movq	%r13, %rdi
	call	SSL_CTX_ctrl@PLT
	jmp	.L441
.L1144:
	call	BIO_s_null@PLT
	movq	%rax, %rdi
	call	BIO_new@PLT
	cmpl	$0, -912(%rbp)
	movq	%rax, bio_c_out(%rip)
	je	.L429
	cmpq	$0, -808(%rbp)
	jne	.L429
	movl	$32769, %edi
	call	dup_bio_out@PLT
	movq	%rax, -808(%rbp)
	jmp	.L429
.L1125:
	movq	-1040(%rbp), %rdx
	movq	bio_err(%rip), %rdi
	leaq	.LC103(%rip), %rsi
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	jmp	.L697
.L472:
	cmpl	$0, -1116(%rbp)
	jne	.L1145
.L471:
	cmpl	$0, -908(%rbp)
	je	.L473
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	SSL_CTX_enable_ct@PLT
	testl	%eax, %eax
	je	.L708
	movq	-1048(%rbp), %rsi
	movq	%r13, %rdi
	call	ctx_set_ctlog_list_file@PLT
	testl	%eax, %eax
	jne	.L709
.L708:
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	movl	-908(%rbp), %r15d
	jmp	.L697
.L473:
	movq	-1048(%rbp), %rsi
	movq	%r13, %rdi
	call	ctx_set_ctlog_list_file@PLT
	testl	%eax, %eax
	jne	.L709
	call	ERR_clear_error@PLT
.L709:
	movq	verify_callback@GOTPCREL(%rip), %rdx
	movl	-936(%rbp), %esi
	movq	%r13, %rdi
	call	SSL_CTX_set_verify@PLT
	movl	-1128(%rbp), %r8d
	movq	%r13, %rdi
	movl	-1132(%rbp), %ecx
	movq	-1072(%rbp), %rdx
	movq	-1152(%rbp), %rsi
	call	ctx_set_verify_locations@PLT
	testl	%eax, %eax
	je	.L1101
	movl	-696(%rbp), %edx
	movq	-728(%rbp), %rsi
	movq	%r13, %rdi
	call	ssl_ctx_add_crls@PLT
	movl	-1096(%rbp), %r8d
	movq	%r13, %rdi
	movq	-656(%rbp), %rcx
	movq	-736(%rbp), %rdx
	movq	-744(%rbp), %rsi
	call	set_cert_key_stuff@PLT
	testl	%eax, %eax
	je	.L1099
	cmpl	$0, -896(%rbp)
	je	.L1146
.L476:
	movq	-536(%rbp), %rsi
	testq	%rsi, %rsi
	je	.L478
	cmpl	$0, -1168(%rbp)
	jne	.L479
	movq	%r13, %rdi
	call	SSL_CTX_set_srp_username@PLT
	testl	%eax, %eax
	je	.L1147
.L479:
	movl	-912(%rbp), %ebx
	movl	c_debug(%rip), %eax
	movq	%r13, %rdi
	movq	-720(%rbp), %rsi
	movl	%ebx, -528(%rbp)
	movl	%eax, -524(%rbp)
	call	SSL_CTX_set_srp_cb_arg@PLT
	leaq	ssl_give_srp_client_pwd_cb(%rip), %rsi
	movq	%r13, %rdi
	call	SSL_CTX_set_srp_client_pwd_callback@PLT
	movl	-516(%rbp), %esi
	movq	%r13, %rdi
	call	SSL_CTX_set_srp_strength@PLT
	orl	c_debug(%rip), %ebx
	jne	.L480
	cmpl	$0, -520(%rbp)
	jne	.L478
.L480:
	leaq	ssl_srp_verify_param_cb(%rip), %rsi
	movq	%r13, %rdi
	call	SSL_CTX_set_srp_verify_param_callback@PLT
.L478:
	cmpq	$0, -920(%rbp)
	je	.L482
	movq	%r13, %rdi
	call	SSL_CTX_dane_enable@PLT
	testl	%eax, %eax
	jle	.L1148
.L482:
	xorl	%ecx, %ecx
	movl	$513, %edx
	movl	$44, %esi
	movq	%r13, %rdi
	call	SSL_CTX_ctrl@PLT
	leaq	new_session_cb(%rip), %rsi
	movq	%r13, %rdi
	call	SSL_CTX_sess_set_new_cb@PLT
	movq	-1208(%rbp), %rsi
	movq	%r13, %rdi
	call	set_keylog_file@PLT
	movl	%eax, %r15d
	testl	%eax, %eax
	jne	.L1099
	movq	%r13, %rdi
	call	SSL_new@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L1099
	cmpl	$0, -1200(%rbp)
	jne	.L1149
.L483:
	movq	-976(%rbp), %rax
	testq	%rax, %rax
	je	.L484
	movq	%rax, %rdi
	leaq	.LC113(%rip), %rsi
	call	BIO_new_file@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1102
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, -696(%rbp)
	call	PEM_read_bio_SSL_SESSION@PLT
	movq	-696(%rbp), %rdi
	movq	%rax, %r12
	call	BIO_free@PLT
	testq	%r12, %r12
	je	.L1102
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	SSL_set_session@PLT
	testl	%eax, %eax
	je	.L1150
	movq	%r12, %rdi
	call	SSL_SESSION_free@PLT
.L484:
	cmpl	$0, -708(%rbp)
	jne	.L1151
.L488:
	cmpl	$0, -896(%rbp)
	jne	.L489
	cmpq	$0, -904(%rbp)
	jne	.L493
	cmpq	$0, -920(%rbp)
	je	.L490
.L502:
	movq	-920(%rbp), %rsi
	movq	%rbx, %rdi
	call	SSL_dane_enable@PLT
	testl	%eax, %eax
	jle	.L1152
	cmpq	$0, -784(%rbp)
	je	.L1153
	movq	-784(%rbp), %rdi
	call	OPENSSL_sk_num@PLT
	xorl	%ecx, %ecx
	movq	%r13, -848(%rbp)
	movl	%eax, -720(%rbp)
	leaq	-576(%rbp), %rax
	movl	%ecx, %r13d
	movl	$0, -696(%rbp)
	movq	%rax, -840(%rbp)
.L505:
	cmpl	%r13d, -720(%rbp)
	jle	.L1154
	movq	-784(%rbp), %rdi
	movl	%r13d, %esi
	movq	%rbx, %r12
	call	OPENSSL_sk_value@PLT
	leaq	tlsa_fields.28727(%rip), %rdx
	movl	%r15d, -708(%rbp)
	xorl	%r9d, %r9d
	movq	%rax, -576(%rbp)
	movq	%rax, %r15
	movq	%rdx, %rbx
	jmp	.L506
.L509:
	movq	-840(%rbp), %rdi
	call	*16(%rbx)
	movq	%rax, %r9
	testq	%rax, %rax
	jle	.L1155
	addq	$24, %rbx
.L506:
	movq	(%rbx), %rsi
	testq	%rsi, %rsi
	jne	.L509
	movzbl	mtype.28725(%rip), %ecx
	movzbl	selector.28724(%rip), %edx
	movq	%r12, %rbx
	movq	%r15, %r12
	movzbl	usage.28723(%rip), %esi
	movq	data.28726(%rip), %r8
	movq	%rbx, %rdi
	movl	-708(%rbp), %r15d
	call	SSL_dane_tlsa_add@PLT
	movq	data.28726(%rip), %rdi
	movl	$534, %edx
	leaq	.LC14(%rip), %rsi
	movl	%eax, -708(%rbp)
	call	CRYPTO_free@PLT
	movl	-708(%rbp), %eax
	testl	%eax, %eax
	je	.L1156
	js	.L1157
	addl	$1, -696(%rbp)
	jmp	.L508
.L1149:
	movl	$1, %esi
	movq	%rax, %rdi
	call	SSL_set_post_handshake_auth@PLT
	jmp	.L483
.L1155:
	movq	%rbx, %rdx
	movq	bio_err(%rip), %rdi
	movq	%r12, %rbx
	movq	%r15, %r12
	movq	8(%rdx), %rcx
	movq	prog(%rip), %rdx
	movq	%r12, %r8
	xorl	%eax, %eax
	leaq	.LC126(%rip), %rsi
	movl	-708(%rbp), %r15d
	call	BIO_printf@PLT
.L508:
	addl	$1, %r13d
	jmp	.L505
.L1154:
	cmpl	$0, -696(%rbp)
	movq	-848(%rbp), %r13
	je	.L1158
	cmpl	$0, -1056(%rbp)
	jne	.L1159
.L515:
	leaq	-676(%rbp), %rax
	movl	$0, -940(%rbp)
	movl	$0, -840(%rbp)
	movl	$1, -720(%rbp)
	movq	%rax, -968(%rbp)
	movl	%r14d, -708(%rbp)
	movq	%r13, -696(%rbp)
	movl	%r15d, -924(%rbp)
	movq	-800(%rbp), %r15
.L653:
	movl	-888(%rbp), %eax
	movq	-624(%rbp), %rcx
	pushq	$0
	movq	-632(%rbp), %rdx
	movl	-876(%rbp), %r9d
	movq	-616(%rbp), %r8
	movq	-640(%rbp), %rsi
	pushq	%rax
	movq	-968(%rbp), %rdi
	call	init_client@PLT
	popq	%rdx
	popq	%rcx
	testl	%eax, %eax
	je	.L1160
	movl	-676(%rbp), %edx
	movq	bio_c_out(%rip), %rdi
	xorl	%eax, %eax
	leaq	.LC132(%rip), %rsi
	call	BIO_printf@PLT
	cmpl	$0, -1000(%rbp)
	jne	.L1161
.L517:
	xorl	%esi, %esi
	cmpl	$0, -884(%rbp)
	movl	-676(%rbp), %edi
	je	.L519
	call	BIO_new_dgram@PLT
	movq	%rax, %r13
	call	BIO_ADDR_new@PLT
	movq	%rax, -584(%rbp)
	testq	%rax, %rax
	je	.L1162
	leaq	-584(%rbp), %r12
	movl	-676(%rbp), %edi
	xorl	%esi, %esi
	movq	%r12, %rdx
	call	BIO_sock_info@PLT
	testl	%eax, %eax
	je	.L1163
	movq	-584(%rbp), %rcx
	xorl	%edx, %edx
	movl	$32, %esi
	movq	%r13, %rdi
	call	BIO_ctrl@PLT
	movq	-584(%rbp), %rdi
	call	BIO_ADDR_free@PLT
	cmpl	$0, -1164(%rbp)
	movq	$0, -584(%rbp)
	jne	.L1164
.L523:
	movq	-1032(%rbp), %r14
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	testq	%r14, %r14
	je	.L524
	movl	$121, %esi
	movq	%rbx, %rdi
	call	SSL_ctrl@PLT
	cmpq	%r14, %rax
	jg	.L1165
	movl	$4096, %esi
	movq	%rbx, %rdi
	call	SSL_set_options@PLT
	xorl	%ecx, %ecx
	movl	$120, %esi
	movq	%rbx, %rdi
	movq	-1032(%rbp), %rdx
	call	SSL_ctrl@PLT
	testq	%rax, %rax
	je	.L1166
.L528:
	cmpl	$0, -1120(%rbp)
	jne	.L1167
.L530:
	cmpl	$0, c_debug(%rip)
	jne	.L1168
.L531:
	movl	-912(%rbp), %eax
	testl	%eax, %eax
	je	.L532
	cmpl	$2, %eax
	je	.L1169
	movq	msg_cb@GOTPCREL(%rip), %rsi
	movq	%rbx, %rdi
	call	SSL_set_msg_callback@PLT
.L534:
	movq	-808(%rbp), %rax
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1170
.L535:
	xorl	%edx, %edx
	movl	$16, %esi
	movq	%rbx, %rdi
	call	SSL_ctrl@PLT
.L532:
	cmpl	$0, -1192(%rbp)
	jne	.L1171
.L536:
	cmpl	$0, -1196(%rbp)
	jne	.L1172
.L537:
	movq	%r13, %rdx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	SSL_set_bio@PLT
	movq	%rbx, %rdi
	call	SSL_set_connect_state@PLT
	call	fileno_stdin@PLT
	movq	%rbx, %rdi
	movl	%eax, %r12d
	call	SSL_get_fd@PLT
	cmpl	%eax, %r12d
	jle	.L538
	call	fileno_stdin@PLT
	addl	$1, %eax
	movl	%eax, -920(%rbp)
.L539:
	movl	-672(%rbp), %edx
	leal	-1(%rdx), %eax
	cmpl	$14, %eax
	ja	.L540
	leaq	.L542(%rip), %rcx
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L542:
	.long	.L545-.L542
	.long	.L554-.L542
	.long	.L553-.L542
	.long	.L552-.L542
	.long	.L551-.L542
	.long	.L550-.L542
	.long	.L550-.L542
	.long	.L549-.L542
	.long	.L548-.L542
	.long	.L547-.L542
	.long	.L546-.L542
	.long	.L545-.L542
	.long	.L544-.L542
	.long	.L543-.L542
	.long	.L541-.L542
	.text
.L1168:
	movq	bio_dump_callback@GOTPCREL(%rip), %rsi
	movq	%r13, %rdi
	call	BIO_set_callback@PLT
	movq	bio_c_out(%rip), %rsi
	movq	%r13, %rdi
	call	BIO_set_callback_arg@PLT
	jmp	.L531
.L543:
	call	BIO_f_buffer@PLT
	xorl	%r14d, %r14d
	movq	%rax, %rdi
	call	BIO_new@PLT
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	BIO_push@PLT
.L706:
	movl	$8192, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	BIO_gets@PLT
	subl	$1, %eax
	jle	.L601
	cmpb	$34, (%r15)
	je	.L1173
.L601:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	movq	%r12, %rdi
	call	BIO_ctrl@PLT
	movq	%r12, %rdi
	call	BIO_pop@PLT
	movq	%r12, %rdi
	call	BIO_free@PLT
	testl	%r14d, %r14d
	je	.L1174
.L603:
	leaq	.LC143(%rip), %rsi
	movq	%r13, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movl	$8192, %edx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	BIO_read@PLT
	movl	%eax, -840(%rbp)
	testl	%eax, %eax
	js	.L1086
	movslq	-840(%rbp), %rax
	movq	%rax, %rcx
	movb	$0, (%r15,%rax)
	subl	$1, %ecx
	jle	.L1175
	movq	-792(%rbp), %r14
	movl	$2, %edx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	strncpy@PLT
	movq	%r14, %rdi
	call	make_uppercase@PLT
	movl	$2, %edx
	leaq	.LC175(%rip), %rsi
	movq	%r14, %rdi
	call	strncmp@PLT
	testl	%eax, %eax
	jne	.L1176
.L540:
	cmpq	$0, -1064(%rbp)
	je	.L639
	movq	%rbx, %rdi
	call	SSL_get_session@PLT
	testq	%rax, %rax
	je	.L630
	movq	%rbx, %rdi
	call	SSL_get_session@PLT
	movq	%rax, %rdi
	call	SSL_SESSION_get_max_early_data@PLT
	testl	%eax, %eax
	jne	.L633
.L630:
	movq	psksess(%rip), %rdi
	testq	%rdi, %rdi
	je	.L639
	call	SSL_SESSION_get_max_early_data@PLT
	testl	%eax, %eax
	jne	.L633
.L639:
	movl	-720(%rbp), %eax
	movl	$1, %ecx
	movl	$0, -712(%rbp)
	xorl	%r14d, %r14d
	movl	$0, -928(%rbp)
	movl	$1, %r12d
	movl	$0, -908(%rbp)
	movl	$0, -904(%rbp)
	movl	%eax, -848(%rbp)
	movl	$1, %eax
	movq	%r13, -896(%rbp)
	movl	%eax, %r13d
	movq	%r15, -872(%rbp)
	movl	%ecx, %r15d
.L640:
	movq	-856(%rbp), %rdi
	movl	$16, %ecx
	xorl	%eax, %eax
#APP
# 2741 "../deps/openssl/openssl/apps/s_client.c" 1
	cld; rep; stosq
# 0 "" 2
#NO_APP
	movl	$16, %ecx
	movq	-864(%rbp), %rdi
#APP
# 2742 "../deps/openssl/openssl/apps/s_client.c" 1
	cld; rep; stosq
# 0 "" 2
#NO_APP
	movq	%rbx, %rdi
	call	SSL_is_dtls@PLT
	testl	%eax, %eax
	jne	.L642
.L644:
	movq	$0, -936(%rbp)
.L643:
	movq	%rbx, %rdi
	call	SSL_is_init_finished@PLT
	testl	%eax, %eax
	je	.L645
.L648:
	cmpl	$0, -848(%rbp)
	je	.L647
	cmpl	$0, -1052(%rbp)
	jne	.L1177
.L650:
	movl	-720(%rbp), %edx
	movq	bio_c_out(%rip), %rdi
	movq	%rbx, %rsi
	call	print_stuff
	cmpl	$0, -672(%rbp)
	jne	.L1178
	cmpl	$0, -944(%rbp)
	jne	.L654
	movl	$0, -720(%rbp)
.L647:
	testl	%r15d, %r15d
	je	.L716
	movq	%rbx, %rdi
	call	SSL_has_pending@PLT
	movl	$0, -848(%rbp)
	testl	%eax, %eax
	je	.L716
.L720:
	movq	%rbx, %rdi
	call	SSL_is_dtls@PLT
	testl	%eax, %eax
	jne	.L1179
.L699:
	movq	-792(%rbp), %rsi
	movq	%rbx, %rdi
	movl	$1024, %edx
	call	SSL_read@PLT
	movq	%rbx, %rdi
	movl	%eax, %esi
	movl	%eax, -936(%rbp)
	call	SSL_get_error@PLT
	cmpl	$10, %eax
	ja	.L640
	leaq	.L672(%rip), %rsi
	movl	%eax, %ecx
	movl	-936(%rbp), %edx
	movslq	(%rsi,%rcx,4), %rcx
	addq	%rsi, %rcx
	notrack jmp	*%rcx
	.section	.rodata
	.align 4
	.align 4
.L672:
	.long	.L679-.L672
	.long	.L671-.L672
	.long	.L678-.L672
	.long	.L677-.L672
	.long	.L676-.L672
	.long	.L675-.L672
	.long	.L674-.L672
	.long	.L640-.L672
	.long	.L640-.L672
	.long	.L673-.L672
	.long	.L671-.L672
	.text
.L645:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$12, %esi
	movq	%rbx, %rdi
	call	SSL_ctrl@PLT
	testq	%rax, %rax
	jne	.L648
	movq	%rbx, %rdi
	call	SSL_get_key_update_type@PLT
	addl	$1, %eax
	jne	.L648
	testl	%r15d, %r15d
	jne	.L714
	movl	$1, -848(%rbp)
.L657:
	testl	%r12d, %r12d
	jne	.L1180
.L658:
	movq	-936(%rbp), %r8
	movq	-864(%rbp), %rdx
	xorl	%ecx, %ecx
	movq	-856(%rbp), %rsi
	movl	-920(%rbp), %edi
	call	select@PLT
	testl	%eax, %eax
	js	.L1181
	movq	%rbx, %rdi
	call	SSL_is_dtls@PLT
	testl	%eax, %eax
	je	.L704
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$74, %esi
	movq	%rbx, %rdi
	call	SSL_ctrl@PLT
	testq	%rax, %rax
	jle	.L704
	movq	bio_err(%rip), %rdi
	leaq	.LC210(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L704:
	movq	%rbx, %rdi
	call	SSL_get_fd@PLT
	movslq	%eax, %rdi
	call	__fdelt_chk@PLT
	movq	%rbx, %rdi
	movq	-384(%rbp,%rax,8), %rcx
	movq	%rcx, -936(%rbp)
	call	SSL_get_fd@PLT
	movl	$64, %ecx
	cltd
	idivl	%ecx
	movq	-936(%rbp), %rcx
	btq	%rdx, %rcx
	jc	.L1182
	call	fileno_stdout@PLT
	movslq	%eax, %rdi
	call	__fdelt_chk@PLT
	movq	-384(%rbp,%rax,8), %rax
	movq	%rax, -936(%rbp)
	call	fileno_stdout@PLT
	movl	$64, %esi
	cltd
	idivl	%esi
	movl	$1, %eax
	movl	%edx, %ecx
	salq	%cl, %rax
	testq	%rax, -936(%rbp)
	jne	.L1183
	movq	%rbx, %rdi
	call	SSL_get_fd@PLT
	movslq	%eax, %rdi
	call	__fdelt_chk@PLT
	movq	%rbx, %rdi
	movq	-512(%rbp,%rax,8), %rax
	movq	%rax, -936(%rbp)
	call	SSL_get_fd@PLT
	movl	$64, %esi
	cltd
	idivl	%esi
	movl	$1, %eax
	movl	%edx, %ecx
	salq	%cl, %rax
	testq	%rax, -936(%rbp)
	jne	.L699
	call	fileno_stdin@PLT
	movslq	%eax, %rdi
	call	__fdelt_chk@PLT
	movq	-512(%rbp,%rax,8), %rax
	movq	%rax, -936(%rbp)
	call	fileno_stdin@PLT
	movl	$64, %esi
	cltd
	idivl	%esi
	movl	$1, %eax
	movl	%edx, %ecx
	salq	%cl, %rax
	testq	%rax, -936(%rbp)
	je	.L640
	cmpl	$0, -1092(%rbp)
	jne	.L1184
	movq	-824(%rbp), %rdi
	movl	$8192, %esi
	call	raw_read_stdin@PLT
	movl	%eax, %r14d
.L688:
	testl	%r14d, %r14d
	je	.L689
	cmpl	$0, -880(%rbp)
	jne	.L773
	testl	%r14d, %r14d
	jle	.L690
	movq	-824(%rbp), %rax
	movzbl	-704(%rbp), %edx
	movzbl	(%rax), %eax
	andl	$1, %edx
	cmpb	$81, %al
	jne	.L691
	testb	%dl, %dl
	jne	.L690
.L691:
	cmpb	$82, %al
	jne	.L692
	testb	%dl, %dl
	jne	.L1185
.L692:
	andl	$-33, %eax
	cmpb	$75, %al
	jne	.L774
	testb	%dl, %dl
	jne	.L1186
.L774:
	movl	$0, -904(%rbp)
	xorl	%r13d, %r13d
	movl	$1, %r12d
	jmp	.L640
.L1185:
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
	xorl	%r13d, %r13d
	xorl	%r14d, %r14d
	leaq	.LC207(%rip), %rsi
	movl	$1, %r12d
	call	BIO_printf@PLT
	movq	%rbx, %rdi
	call	SSL_renegotiate@PLT
	jmp	.L640
.L1186:
	movq	bio_err(%rip), %rdi
	leaq	.LC208(%rip), %rsi
	xorl	%eax, %eax
	movl	$1, %r12d
	call	BIO_printf@PLT
	movq	-824(%rbp), %rax
	xorl	%esi, %esi
	movq	%rbx, %rdi
	cmpb	$75, (%rax)
	sete	%sil
	xorl	%r13d, %r13d
	xorl	%r14d, %r14d
	call	SSL_key_update@PLT
	jmp	.L640
.L690:
	movq	-896(%rbp), %rax
	movq	bio_err(%rip), %rdi
	leaq	.LC199(%rip), %rsi
	movl	-708(%rbp), %r14d
	movq	-696(%rbp), %r13
	movq	%rax, -704(%rbp)
	movq	-872(%rbp), %rax
	movl	-848(%rbp), %r12d
	movl	-924(%rbp), %r15d
	movq	%rax, -800(%rbp)
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L529:
	testl	%r12d, %r12d
	jne	.L1187
.L693:
	movq	%rbx, %rdi
	leaq	-576(%rbp), %r12
	call	do_ssl_shutdown
	movq	%rbx, %rdi
	call	SSL_get_fd@PLT
	movl	$1, %esi
	movl	%eax, %edi
	call	shutdown@PLT
	movdqa	.LC209(%rip), %xmm0
	movaps	%xmm0, -576(%rbp)
	jmp	.L695
.L694:
	movq	-792(%rbp), %rsi
	movq	-704(%rbp), %rdi
	movl	$8192, %edx
	call	BIO_read@PLT
	testl	%eax, %eax
	jle	.L696
.L695:
	movq	-856(%rbp), %rdi
	movl	$16, %ecx
	xorl	%eax, %eax
#APP
# 3106 "../deps/openssl/openssl/apps/s_client.c" 1
	cld; rep; stosq
# 0 "" 2
#NO_APP
	movslq	-676(%rbp), %rdi
	call	__fdelt_chk@PLT
	movl	-676(%rbp), %esi
	movl	$64, %edi
	movq	%rax, %r8
	movl	%esi, %eax
	cltd
	idivl	%edi
	movl	$1, %eax
	leal	1(%rsi), %edi
	movq	-856(%rbp), %rsi
	movl	%edx, %ecx
	xorl	%edx, %edx
	salq	%cl, %rax
	xorl	%ecx, %ecx
	orq	%rax, -512(%rbp,%r8,8)
	movq	%r12, %r8
	call	select@PLT
	testl	%eax, %eax
	jg	.L694
.L696:
	movq	%rbx, %rdi
	call	SSL_get_fd@PLT
	movl	%eax, %edi
	call	BIO_closesocket@PLT
.L262:
	testq	%rbx, %rbx
	je	.L697
	testl	%r14d, %r14d
	jne	.L1188
.L698:
	movq	%rbx, %rdi
	call	SSL_free@PLT
	jmp	.L697
.L773:
	movl	$0, -904(%rbp)
	movl	-880(%rbp), %r12d
	xorl	%r13d, %r13d
	jmp	.L640
	.p2align 4,,10
	.p2align 3
.L1188:
	movq	bio_c_out(%rip), %rdi
	movl	$1, %edx
	movq	%rbx, %rsi
	call	print_stuff
	jmp	.L698
.L1187:
	movl	-720(%rbp), %edx
	movq	bio_c_out(%rip), %rdi
	movq	%rbx, %rsi
	call	print_stuff
	jmp	.L693
.L689:
	movl	-880(%rbp), %r12d
	testl	%r12d, %r12d
	je	.L690
	movl	%r12d, -940(%rbp)
	xorl	%r13d, %r13d
	movl	$0, -904(%rbp)
	jmp	.L640
.L1184:
	movq	-824(%rbp), %rdi
	movl	$4096, %esi
	call	raw_read_stdin@PLT
	xorl	%edx, %edx
	movl	%eax, %r14d
	xorl	%eax, %eax
	jmp	.L682
.L684:
	movq	-824(%rbp), %rcx
	cmpb	$10, (%rcx,%rax)
	jne	.L683
	addl	$1, %edx
.L683:
	addq	$1, %rax
.L682:
	cmpl	%eax, %r14d
	jg	.L684
	leal	-1(%r14), %eax
	movq	-824(%rbp), %rdi
	cltq
	jmp	.L685
.L687:
	movzbl	(%rdi,%rax), %r8d
	leal	(%rdx,%rax), %esi
	movslq	%esi, %rsi
	movb	%r8b, (%rdi,%rsi)
	cmpb	$10, %r8b
	jne	.L686
	subl	$1, %edx
	addl	$1, %r14d
	addl	%edx, %ecx
	movslq	%ecx, %rcx
	movb	$13, (%rdi,%rcx)
.L686:
	subq	$1, %rax
.L685:
	movl	%eax, %ecx
	testl	%eax, %eax
	jns	.L687
	jmp	.L688
.L1183:
	movl	-908(%rbp), %esi
	movslq	-928(%rbp), %rdi
	addq	-792(%rbp), %rdi
	call	raw_write_stdout@PLT
	testl	%eax, %eax
	jle	.L1189
	subl	%eax, -908(%rbp)
	movl	-908(%rbp), %ecx
	addl	%eax, -928(%rbp)
	movl	$1, %eax
	testl	%ecx, %ecx
	movl	$0, %ecx
	cmovg	-712(%rbp), %ecx
	cmovle	%eax, %r15d
	movl	%ecx, -712(%rbp)
	jmp	.L640
.L1182:
	movslq	-904(%rbp), %rsi
	movq	%rbx, %rdi
	addq	-824(%rbp), %rsi
	movl	%r14d, %edx
	call	SSL_write@PLT
	movq	%rbx, %rdi
	movl	%eax, %esi
	movl	%eax, -936(%rbp)
	call	SSL_get_error@PLT
	cmpl	$10, %eax
	ja	.L640
	leaq	.L662(%rip), %rsi
	movl	%eax, %ecx
	movl	-936(%rbp), %edx
	movslq	(%rsi,%rcx,4), %rcx
	addq	%rsi, %rcx
	notrack jmp	*%rcx
	.section	.rodata
	.align 4
	.align 4
.L662:
	.long	.L669-.L662
	.long	.L671-.L662
	.long	.L668-.L662
	.long	.L667-.L662
	.long	.L666-.L662
	.long	.L665-.L662
	.long	.L664-.L662
	.long	.L640-.L662
	.long	.L640-.L662
	.long	.L663-.L662
	.long	.L671-.L662
	.text
.L1189:
	movq	-896(%rbp), %rax
	movq	bio_c_out(%rip), %rdi
	leaq	.LC199(%rip), %rsi
	movl	-708(%rbp), %r14d
	movq	-696(%rbp), %r13
	movq	%rax, -704(%rbp)
	movq	-872(%rbp), %rax
	movl	-848(%rbp), %r12d
	movl	-924(%rbp), %r15d
	movq	%rax, -800(%rbp)
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L529
.L663:
	movq	bio_c_out(%rip), %rdi
	leaq	.LC194(%rip), %rsi
	xorl	%eax, %eax
	xorl	%r13d, %r13d
	movl	$1, %r12d
	call	BIO_printf@PLT
	movq	%rbx, %rdi
	call	wait_for_async@PLT
	jmp	.L640
.L664:
	testl	%r14d, %r14d
	jne	.L1190
.L769:
	xorl	%r12d, %r12d
	movl	$1, %r13d
	jmp	.L640
.L665:
	orl	%edx, %r14d
	je	.L769
	movq	-896(%rbp), %rax
	movl	-708(%rbp), %r14d
	movl	$1, %r15d
	movq	-696(%rbp), %r13
	movl	-848(%rbp), %r12d
	movq	%rax, -704(%rbp)
	movq	-872(%rbp), %rax
	movq	%rax, -800(%rbp)
	call	__errno_location@PLT
	movq	bio_err(%rip), %rdi
	leaq	.LC198(%rip), %rsi
	movl	(%rax), %edx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L529
.L1190:
	movq	-896(%rbp), %rax
	movq	bio_c_out(%rip), %rdi
	leaq	.LC197(%rip), %rsi
	movl	-708(%rbp), %r14d
	movq	-696(%rbp), %r13
	movq	%rax, -704(%rbp)
	movq	-872(%rbp), %rax
	movl	-848(%rbp), %r12d
	movl	-924(%rbp), %r15d
	movq	%rax, -800(%rbp)
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L529
.L666:
	movq	bio_c_out(%rip), %rdi
	leaq	.LC196(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L640
.L667:
	leaq	.LC193(%rip), %rsi
.L1080:
	movq	bio_c_out(%rip), %rdi
	xorl	%eax, %eax
	xorl	%r13d, %r13d
	movl	$1, %r12d
	call	BIO_printf@PLT
	jmp	.L640
.L668:
	movq	bio_c_out(%rip), %rdi
	leaq	.LC195(%rip), %rsi
	xorl	%eax, %eax
	xorl	%r12d, %r12d
	movl	$1, %r15d
	call	BIO_printf@PLT
	movl	$0, -712(%rbp)
	jmp	.L640
.L669:
	addl	%edx, -904(%rbp)
	subl	%edx, %r14d
	testl	%edx, %edx
	jle	.L770
	movl	$1, %r12d
	testl	%r14d, %r14d
	movl	%r12d, %r13d
	cmovle	%eax, %r12d
	cmovg	%eax, %r13d
	jmp	.L640
.L1181:
	movq	-896(%rbp), %rax
	movl	-708(%rbp), %r14d
	movl	$1, %r15d
	movq	-696(%rbp), %r13
	movl	-848(%rbp), %r12d
	movq	%rax, -704(%rbp)
	movq	-872(%rbp), %rax
	movq	%rax, -800(%rbp)
	call	__errno_location@PLT
	movq	bio_err(%rip), %rdi
	leaq	.LC192(%rip), %rsi
	movl	(%rax), %edx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L529
.L1180:
	movq	%rbx, %rdi
	call	SSL_get_fd@PLT
	movq	%rbx, %rdi
	movl	%eax, -952(%rbp)
	call	SSL_get_fd@PLT
	movslq	%eax, %rdi
	call	__fdelt_chk@PLT
	movl	$64, %esi
	movq	%rax, %r8
	movl	-952(%rbp), %eax
	cltd
	idivl	%esi
	movl	$1, %eax
	movl	%edx, %ecx
	salq	%cl, %rax
	orq	%rax, -384(%rbp,%r8,8)
	jmp	.L658
.L770:
	movq	-872(%rbp), %rax
	movl	-708(%rbp), %r14d
	movq	-696(%rbp), %r13
	movq	%rax, -800(%rbp)
.L1087:
	movl	$1, %r15d
	jmp	.L262
.L714:
	movq	%rbx, %rdi
	call	SSL_has_pending@PLT
	movl	%r15d, -848(%rbp)
	testl	%eax, %eax
	jne	.L720
.L721:
	movq	%rbx, %rdi
	call	SSL_get_fd@PLT
	movq	%rbx, %rdi
	movl	%eax, -952(%rbp)
	call	SSL_get_fd@PLT
	movslq	%eax, %rdi
	call	__fdelt_chk@PLT
	movl	$64, %esi
	movq	%rax, %r8
	movl	-952(%rbp), %eax
	cltd
	idivl	%esi
	movl	$1, %eax
	movl	%edx, %ecx
	salq	%cl, %rax
	orq	%rax, -512(%rbp,%r8,8)
	jmp	.L657
.L642:
	leaq	-576(%rbp), %rcx
	xorl	%edx, %edx
	movl	$73, %esi
	movq	%rbx, %rdi
	movq	%rcx, -936(%rbp)
	call	SSL_ctrl@PLT
	testq	%rax, %rax
	je	.L644
	jmp	.L643
.L673:
	movq	bio_c_out(%rip), %rdi
	leaq	.LC200(%rip), %rsi
	xorl	%eax, %eax
	movl	%r13d, %r15d
	call	BIO_printf@PLT
	movq	%rbx, %rdi
	call	wait_for_async@PLT
	orl	%r12d, %r15d
	movl	%r15d, -712(%rbp)
	jne	.L1081
.L772:
	xorl	%r13d, %r13d
	movl	$1, %r15d
	movl	$1, %r12d
	jmp	.L640
.L674:
	movq	-896(%rbp), %rax
	movq	bio_c_out(%rip), %rdi
	leaq	.LC206(%rip), %rsi
	movl	-708(%rbp), %r14d
	movq	-696(%rbp), %r13
	movq	%rax, -704(%rbp)
	movq	-872(%rbp), %rax
	movl	-848(%rbp), %r12d
	movl	-924(%rbp), %r15d
	movq	%rax, -800(%rbp)
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L529
.L678:
	movq	bio_c_out(%rip), %rdi
	xorl	%eax, %eax
	leaq	.LC202(%rip), %rsi
	movl	%r13d, %r15d
	call	BIO_printf@PLT
	orl	%r12d, %r15d
	movl	%r15d, -712(%rbp)
	je	.L772
.L1081:
	movl	$0, -712(%rbp)
	jmp	.L640
.L675:
	movq	-896(%rbp), %rax
	movl	-708(%rbp), %r14d
	movq	-696(%rbp), %r13
	movl	-848(%rbp), %r12d
	movq	%rax, -704(%rbp)
	movq	-872(%rbp), %rax
	movq	%rax, -800(%rbp)
	call	__errno_location@PLT
	cmpl	$0, -1052(%rbp)
	movq	bio_err(%rip), %rdi
	movl	(%rax), %r15d
	je	.L680
	leaq	.LC204(%rip), %rsi
	call	BIO_puts@PLT
	jmp	.L529
.L676:
	movq	bio_c_out(%rip), %rdi
	leaq	.LC203(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L640
.L680:
	movl	%r15d, %edx
	leaq	.LC205(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L529
.L677:
	leaq	.LC201(%rip), %rsi
	jmp	.L1080
.L671:
	movq	-896(%rbp), %rax
	movq	bio_err(%rip), %rdi
	movl	$1, %r15d
	movl	-708(%rbp), %r14d
	movq	-696(%rbp), %r13
	movq	%rax, -704(%rbp)
	movq	-872(%rbp), %rax
	movl	-848(%rbp), %r12d
	movq	%rax, -800(%rbp)
	call	ERR_print_errors@PLT
	jmp	.L529
.L679:
	testl	%edx, %edx
	jle	.L770
	movl	%eax, -928(%rbp)
	movl	%eax, %r15d
	movl	%edx, -908(%rbp)
	movl	$1, -712(%rbp)
	jmp	.L640
.L1179:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$74, %esi
	movq	%rbx, %rdi
	call	SSL_ctrl@PLT
	testq	%rax, %rax
	jle	.L699
	movq	bio_err(%rip), %rdi
	leaq	.LC210(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L699
.L716:
	movl	-940(%rbp), %eax
	xorl	$1, %eax
	testl	%r13d, %eax
	jne	.L1191
.L655:
	cmpl	$0, -712(%rbp)
	jne	.L1192
.L656:
	movl	$0, -848(%rbp)
	testl	%r15d, %r15d
	je	.L657
	jmp	.L721
.L633:
	movq	-1064(%rbp), %rdi
	leaq	.LC113(%rip), %rsi
	call	BIO_new_file@PLT
	movq	%rax, -848(%rbp)
	testq	%rax, %rax
	je	.L631
	leaq	-584(%rbp), %r12
	leaq	-592(%rbp), %r14
.L632:
	movq	-824(%rbp), %rsi
	movq	-848(%rbp), %rdi
	movq	%r14, %rcx
	movl	$8192, %edx
	call	BIO_read_ex@PLT
	movl	%eax, -872(%rbp)
.L966:
	movq	-592(%rbp), %rdx
	movq	-824(%rbp), %rsi
	movq	%r12, %rcx
	movq	%rbx, %rdi
	call	SSL_write_early_data@PLT
	testl	%eax, %eax
	jne	.L1193
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	SSL_get_error@PLT
	cmpl	$3, %eax
	jg	.L636
	subl	$1, %eax
	jg	.L966
.L637:
	movq	bio_err(%rip), %rdi
	leaq	.LC189(%rip), %rsi
	xorl	%eax, %eax
	movq	%r13, -704(%rbp)
	movl	-708(%rbp), %r14d
	movq	-696(%rbp), %r13
	call	BIO_printf@PLT
	movq	-848(%rbp), %rdi
	call	BIO_free@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
.L1084:
	movl	-720(%rbp), %r12d
	movl	$1, %r15d
	jmp	.L529
.L654:
	movq	bio_c_out(%rip), %rdi
	leaq	.LC191(%rip), %rsi
	xorl	%eax, %eax
	subl	$1, -944(%rbp)
	movq	-872(%rbp), %r15
	call	BIO_printf@PLT
	movq	%rbx, %rdi
	call	do_ssl_shutdown
	movq	%rbx, %rdi
	call	SSL_set_connect_state@PLT
	movq	%rbx, %rdi
	call	SSL_get_fd@PLT
	movl	%eax, %edi
	call	BIO_closesocket@PLT
	movl	$0, -720(%rbp)
	jmp	.L653
.L636:
	cmpl	$9, %eax
	je	.L966
	jmp	.L637
.L1193:
	cmpl	$0, -872(%rbp)
	jne	.L632
	movq	-848(%rbp), %rdi
	call	BIO_free@PLT
	jmp	.L639
.L631:
	movq	bio_err(%rip), %rdi
	leaq	.LC188(%rip), %rsi
	xorl	%eax, %eax
	movq	%r13, -704(%rbp)
	movl	-708(%rbp), %r14d
	movq	-696(%rbp), %r13
	call	BIO_printf@PLT
	jmp	.L1084
.L1178:
	movl	-840(%rbp), %edx
	movq	-872(%rbp), %rsi
	movq	bio_err(%rip), %rdi
	call	BIO_write@PLT
	cmpl	$0, -944(%rbp)
	jne	.L654
	movl	$0, -672(%rbp)
	movl	$0, -720(%rbp)
	jmp	.L647
.L1177:
	movq	bio_err(%rip), %rdi
	leaq	.LC190(%rip), %rsi
	call	BIO_puts@PLT
	movq	%rbx, %rdi
	call	print_ssl_summary@PLT
	jmp	.L650
.L1192:
	call	fileno_stdout@PLT
	movl	%eax, -848(%rbp)
	call	fileno_stdout@PLT
	movslq	%eax, %rdi
	call	__fdelt_chk@PLT
	movl	$64, %esi
	movq	%rax, %r8
	movl	-848(%rbp), %eax
	cltd
	idivl	%esi
	movl	$1, %eax
	movl	%edx, %ecx
	salq	%cl, %rax
	orq	%rax, -384(%rbp,%r8,8)
	jmp	.L656
.L1191:
	call	fileno_stdin@PLT
	movl	%eax, -848(%rbp)
	call	fileno_stdin@PLT
	movslq	%eax, %rdi
	call	__fdelt_chk@PLT
	movl	$64, %esi
	movq	%rax, %r8
	movl	-848(%rbp), %eax
	cltd
	idivl	%esi
	movl	$1, %eax
	movl	%edx, %ecx
	salq	%cl, %rax
	orq	%rax, -512(%rbp,%r8,8)
	jmp	.L655
.L1176:
	movq	%r13, -704(%rbp)
	movl	-708(%rbp), %r14d
	leaq	.LC161(%rip), %rsi
	movq	-696(%rbp), %r13
	movq	-800(%rbp), %rdx
.L1085:
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
	movl	$1, %r15d
	call	BIO_printf@PLT
	movl	-720(%rbp), %r12d
	jmp	.L529
.L1175:
	movq	%r13, -704(%rbp)
	movl	-708(%rbp), %r14d
	leaq	.LC173(%rip), %rsi
	movq	-696(%rbp), %r13
	movq	-800(%rbp), %rdx
	jmp	.L1085
.L554:
	movl	$8192, %edx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	BIO_read@PLT
	leaq	.LC144(%rip), %rsi
	movq	%r13, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	-792(%rbp), %rsi
	movl	$8192, %edx
	movq	%r13, %rdi
	call	BIO_read@PLT
	movl	%eax, -840(%rbp)
	testl	%eax, %eax
	jns	.L540
.L1086:
	movq	bio_err(%rip), %rdi
	leaq	.LC145(%rip), %rsi
	xorl	%eax, %eax
	movl	-708(%rbp), %r14d
	movq	-696(%rbp), %r13
	movl	$1, %r15d
	call	BIO_printf@PLT
	jmp	.L262
.L1174:
	movq	bio_err(%rip), %rdi
	leaq	.LC142(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L603
.L1173:
	movq	%r15, %rdi
	call	make_uppercase@PLT
	movl	$10, %edx
	leaq	.LC174(%rip), %rsi
	movq	%r15, %rdi
	call	strncmp@PLT
	testl	%eax, %eax
	movl	$1, %eax
	cmove	%eax, %r14d
	cmpb	$34, (%r15)
	je	.L706
	jmp	.L601
.L538:
	movq	%rbx, %rdi
	call	SSL_get_fd@PLT
	addl	$1, %eax
	movl	%eax, -920(%rbp)
	jmp	.L539
.L1172:
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	movl	$1, %edx
	movl	$65, %esi
	call	SSL_ctrl@PLT
	movq	-696(%rbp), %r14
	movl	$63, %esi
	leaq	ocsp_resp_cb(%rip), %rdx
	movq	%r14, %rdi
	call	SSL_CTX_callback_ctrl@PLT
	xorl	%edx, %edx
	movl	$64, %esi
	movq	%r14, %rdi
	movq	bio_c_out(%rip), %rcx
	call	SSL_CTX_ctrl@PLT
	jmp	.L537
.L1171:
	movq	tlsext_cb@GOTPCREL(%rip), %rdx
	movq	%rbx, %rdi
	movl	$56, %esi
	call	SSL_callback_ctrl@PLT
	xorl	%edx, %edx
	movl	$57, %esi
	movq	%rbx, %rdi
	movq	bio_c_out(%rip), %rcx
	call	SSL_ctrl@PLT
	jmp	.L536
.L553:
	call	BIO_f_buffer@PLT
	xorl	%r12d, %r12d
	movq	%rax, %rdi
	call	BIO_new@PLT
	movq	%r13, %rsi
	movq	%rax, %r14
	movq	%rax, %rdi
	call	BIO_push@PLT
	movl	$8192, %edx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	BIO_gets@PLT
	leaq	.LC146(%rip), %rsi
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	movq	%r14, %rdi
	call	BIO_ctrl@PLT
	jmp	.L566
.L1194:
	cmpb	$46, (%r15)
	je	.L565
.L566:
	movl	$8192, %edx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	BIO_gets@PLT
	leaq	.LC141(%rip), %rsi
	movq	%r15, %rdi
	movl	%eax, -840(%rbp)
	call	strstr@PLT
	movl	$1, %ecx
	testq	%rax, %rax
	cmovne	%ecx, %r12d
	cmpl	$3, -840(%rbp)
	jg	.L1194
.L565:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	movq	%r14, %rdi
	call	BIO_ctrl@PLT
	movq	%r14, %rdi
	call	BIO_pop@PLT
	movq	%r14, %rdi
	call	BIO_free@PLT
	testl	%r12d, %r12d
	je	.L1195
.L567:
	leaq	.LC147(%rip), %rsi
.L1079:
	movq	%r13, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	-792(%rbp), %rsi
	movl	$8192, %edx
	movq	%r13, %rdi
	call	BIO_read@PLT
	jmp	.L540
.L545:
	call	BIO_f_buffer@PLT
	movq	%rax, %rdi
	call	BIO_new@PLT
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%rax, %r14
	call	BIO_push@PLT
	jmp	.L556
.L1196:
	cmpb	$45, 3(%r15)
	jne	.L555
.L556:
	movl	$8192, %edx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	BIO_gets@PLT
	cmpl	$3, %eax
	jg	.L1196
.L555:
	cmpq	$0, -960(%rbp)
	je	.L1197
.L557:
	cmpl	$12, -672(%rbp)
	movq	-960(%rbp), %rdx
	je	.L1198
	leaq	.LC140(%rip), %rsi
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L559:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	movq	%r14, %rdi
	call	BIO_ctrl@PLT
	xorl	%r12d, %r12d
	jmp	.L562
.L1199:
	cmpb	$45, 3(%r15)
	jne	.L561
.L562:
	movl	$8192, %edx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	BIO_gets@PLT
	leaq	.LC141(%rip), %rsi
	movq	%r15, %rdi
	movl	%eax, -840(%rbp)
	call	strstr@PLT
	movl	$1, %ecx
	testq	%rax, %rax
	cmovne	%ecx, %r12d
	cmpl	$3, -840(%rbp)
	jg	.L1199
.L561:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	movq	%r14, %rdi
	call	BIO_ctrl@PLT
	movq	%r14, %rdi
	call	BIO_pop@PLT
	movq	%r14, %rdi
	call	BIO_free@PLT
	testl	%r12d, %r12d
	je	.L1200
.L563:
	leaq	.LC143(%rip), %rsi
	jmp	.L1079
.L541:
	movq	$-1, -600(%rbp)
	call	BIO_s_mem@PLT
	movq	%rax, %rdi
	call	BIO_new@PLT
	xorl	%edi, %edi
	movq	%rax, %r14
	call	NCONF_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1201
	leaq	ldap_tls_genconf.29399(%rip), %rsi
	movq	%r14, %rdi
	call	BIO_puts@PLT
	leaq	-600(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	NCONF_load_bio@PLT
	testl	%eax, %eax
	jle	.L1202
	movq	%r14, %rdi
	call	BIO_free@PLT
	movq	%r12, %rdi
	leaq	.LC178(%rip), %rdx
	leaq	.LC179(%rip), %rsi
	call	NCONF_get_string@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1203
	movq	%r12, %rsi
	call	ASN1_generate_nconf@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L1204
	movq	%r12, %rdi
	call	NCONF_free@PLT
	movq	8(%r14), %rax
	movq	%r13, %rdi
	movq	8(%rax), %rsi
	movl	(%rax), %edx
	call	BIO_write@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	movq	%r13, %rdi
	call	BIO_ctrl@PLT
	movq	%r14, %rdi
	call	ASN1_TYPE_free@PLT
	movl	$8192, %edx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	BIO_read@PLT
	movslq	%eax, %r8
	testl	%r8d, %r8d
	js	.L1205
	leaq	-664(%rbp), %r9
	leaq	(%r15,%r8), %rax
	movq	%r15, -592(%rbp)
	leaq	-584(%rbp), %r12
	leaq	-592(%rbp), %r14
	movq	%r9, %rdx
	movq	%rax, -848(%rbp)
	leaq	-660(%rbp), %rcx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%r9, -840(%rbp)
	movq	%rcx, -872(%rbp)
	call	ASN1_get_object@PLT
	cmpl	$32, %eax
	jne	.L613
	cmpl	$16, -664(%rbp)
	jne	.L613
	movq	-584(%rbp), %r8
	movq	-848(%rbp), %rax
	subq	-592(%rbp), %rax
	movq	-840(%rbp), %r9
	cmpq	%r8, %rax
	movq	-872(%rbp), %rcx
	jl	.L613
	movq	%r9, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%rcx, -872(%rbp)
	movq	%r9, -840(%rbp)
	call	ASN1_get_object@PLT
	testl	%eax, %eax
	jne	.L616
	cmpl	$2, -664(%rbp)
	jne	.L616
	movq	-592(%rbp), %rax
	movq	-848(%rbp), %rsi
	movq	-584(%rbp), %rdx
	movq	-840(%rbp), %r9
	subq	%rax, %rsi
	movq	-872(%rbp), %rcx
	cmpq	%rdx, %rsi
	jl	.L616
	movq	-848(%rbp), %r8
	addq	%rdx, %rax
	movq	%r12, %rsi
	movq	%r9, %rdx
	movq	%r14, %rdi
	movq	%rax, -592(%rbp)
	subq	%rax, %r8
	movq	%rcx, -872(%rbp)
	movq	%r9, -840(%rbp)
	call	ASN1_get_object@PLT
	cmpl	$32, %eax
	jne	.L618
	cmpl	$64, -660(%rbp)
	jne	.L618
	cmpl	$24, -664(%rbp)
	movq	-840(%rbp), %r9
	movq	-872(%rbp), %rcx
	jne	.L618
	movq	-848(%rbp), %r8
	movq	%r9, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	subq	-592(%rbp), %r8
	call	ASN1_get_object@PLT
	movl	%eax, -840(%rbp)
	testl	%eax, %eax
	jne	.L620
	cmpl	$10, -664(%rbp)
	jne	.L620
	movq	-584(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L620
	movq	-592(%rbp), %rcx
	movq	-848(%rbp), %r8
	subq	%rcx, %r8
	cmpq	%r8, %rdx
	jg	.L620
	xorl	%eax, %eax
	jmp	.L621
.L622:
	movl	-840(%rbp), %esi
	movzbl	(%rcx,%rax), %edi
	addq	$1, %rax
	sall	$8, %esi
	orl	%esi, %edi
	movl	%edi, -840(%rbp)
.L621:
	cmpq	%rdx, %rax
	jl	.L622
.L623:
	endbr64
	cmpl	$0, -840(%rbp)
	js	.L1206
	je	.L540
	movl	-840(%rbp), %edx
	movq	bio_err(%rip), %rdi
	leaq	.LC187(%rip), %rsi
	xorl	%eax, %eax
	movq	%r13, -704(%rbp)
	movl	-708(%rbp), %r14d
	movq	-696(%rbp), %r13
	call	BIO_printf@PLT
	jmp	.L1084
.L1195:
	movq	bio_err(%rip), %rdi
	leaq	.LC142(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L567
.L620:
	movq	bio_err(%rip), %rdi
	leaq	.LC185(%rip), %rsi
	xorl	%eax, %eax
	movq	%r13, -704(%rbp)
	movl	-708(%rbp), %r14d
	movq	-696(%rbp), %r13
	call	BIO_printf@PLT
.L615:
	movq	bio_err(%rip), %rdi
	leaq	.LC186(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L1084
.L1198:
	leaq	.LC139(%rip), %rsi
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L559
.L1197:
	leaq	.LC65(%rip), %rax
	movq	%rax, -960(%rbp)
	jmp	.L557
.L1206:
	movq	%r13, -704(%rbp)
	movl	-708(%rbp), %r14d
	movq	-696(%rbp), %r13
	jmp	.L615
.L1200:
	movq	bio_err(%rip), %rdi
	leaq	.LC142(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L563
.L618:
	movq	bio_err(%rip), %rdi
	leaq	.LC184(%rip), %rsi
	xorl	%eax, %eax
	movq	%r13, -704(%rbp)
	movl	-708(%rbp), %r14d
	movq	-696(%rbp), %r13
	call	BIO_printf@PLT
	jmp	.L615
.L616:
	movq	bio_err(%rip), %rdi
	leaq	.LC183(%rip), %rsi
	xorl	%eax, %eax
	movq	%r13, -704(%rbp)
	movl	-708(%rbp), %r14d
	movq	-696(%rbp), %r13
	call	BIO_printf@PLT
	jmp	.L615
.L613:
	movq	bio_err(%rip), %rdi
	leaq	.LC182(%rip), %rsi
	xorl	%eax, %eax
	movq	%r13, -704(%rbp)
	movl	-708(%rbp), %r14d
	movq	-696(%rbp), %r13
	call	BIO_printf@PLT
	jmp	.L615
.L1205:
	movq	bio_err(%rip), %rdi
	leaq	.LC145(%rip), %rsi
	xorl	%eax, %eax
	movl	-708(%rbp), %r14d
	movq	-696(%rbp), %r13
	call	BIO_printf@PLT
	jmp	.L1087
.L1204:
	movq	%r12, %rdi
	movl	-708(%rbp), %r14d
	movq	-696(%rbp), %r13
	call	NCONF_free@PLT
	movq	bio_err(%rip), %rdi
	leaq	.LC181(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L1087
.L1203:
	movq	%r12, %rdi
	movl	-708(%rbp), %r14d
	movq	-696(%rbp), %r13
	call	NCONF_free@PLT
	movq	bio_err(%rip), %rdi
	leaq	.LC180(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L1087
.L1202:
	movq	%r14, %r8
	movq	-696(%rbp), %r13
	movl	-708(%rbp), %r14d
	movq	%r8, %rdi
	call	BIO_free@PLT
	movq	%r12, %rdi
	call	NCONF_free@PLT
	movq	-600(%rbp), %rdx
	testq	%rdx, %rdx
	jle	.L1207
	movq	bio_err(%rip), %rdi
	leaq	.LC177(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L1087
.L1201:
	movq	%r14, %r8
	movq	-696(%rbp), %r13
	movl	-708(%rbp), %r14d
	movq	%r8, %rdi
	call	BIO_free@PLT
	jmp	.L1087
.L1207:
	movq	bio_err(%rip), %rdi
	leaq	.LC176(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L1087
.L544:
	call	BIO_f_buffer@PLT
	xorl	%r12d, %r12d
	movq	%rax, %rdi
	call	BIO_new@PLT
	movq	%r13, %rsi
	movq	%rax, %r14
	movq	%rax, %rdi
	call	BIO_push@PLT
	movl	$8192, %edx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	BIO_gets@PLT
	leaq	.LC171(%rip), %rsi
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	movq	%r14, %rdi
	call	BIO_ctrl@PLT
	jmp	.L598
.L1208:
	cmpb	$46, (%r15)
	je	.L597
.L598:
	movl	$8192, %edx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	BIO_gets@PLT
	leaq	.LC141(%rip), %rsi
	movq	%r15, %rdi
	movl	%eax, -840(%rbp)
	call	strstr@PLT
	movl	$1, %ecx
	testq	%rax, %rax
	cmovne	%ecx, %r12d
	cmpl	$1, -840(%rbp)
	jg	.L1208
.L597:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	movq	%r14, %rdi
	call	BIO_ctrl@PLT
	movq	%r14, %rdi
	call	BIO_pop@PLT
	movq	%r14, %rdi
	call	BIO_free@PLT
	testl	%r12d, %r12d
	je	.L1209
.L599:
	leaq	.LC143(%rip), %rsi
	movq	%r13, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movl	$8192, %edx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	BIO_read@PLT
	movl	%eax, -840(%rbp)
	testl	%eax, %eax
	js	.L1086
	movslq	-840(%rbp), %rax
	leaq	.LC172(%rip), %rsi
	movq	%r15, %rdi
	movb	$0, (%r15,%rax)
	call	strstr@PLT
	testq	%rax, %rax
	jne	.L540
	movq	-800(%rbp), %rdx
	movq	bio_err(%rip), %rdi
	leaq	.LC173(%rip), %rsi
	movq	%r13, -704(%rbp)
	movl	-708(%rbp), %r14d
	movq	-696(%rbp), %r13
	movl	$1, %r15d
	call	BIO_printf@PLT
	movl	-720(%rbp), %r12d
	jmp	.L529
.L546:
	movl	$8, %edx
	leaq	ssl_request.29386(%rip), %rsi
	movq	%r13, %rdi
	call	BIO_write@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	movq	%r13, %rdi
	call	BIO_ctrl@PLT
	movq	-792(%rbp), %r14
	movl	$8192, %edx
	movq	%r13, %rdi
	movq	%r14, %rsi
	call	BIO_read@PLT
	subl	$1, %eax
	jne	.L1075
	cmpb	$83, (%r14)
	je	.L540
.L1075:
	movq	%r13, -704(%rbp)
	movl	-708(%rbp), %r14d
	movq	-696(%rbp), %r13
	jmp	.L1084
.L547:
	movq	-792(%rbp), %rsi
	movl	$8192, %edx
	movq	%r13, %rdi
	call	BIO_read@PLT
	movl	%eax, %edx
	testl	%eax, %eax
	js	.L1103
	cmpl	$20, %eax
	jle	.L1210
	movq	-792(%rbp), %rsi
	movzbl	1(%rsi), %eax
	movzbl	(%rsi), %ecx
	sall	$8, %eax
	leal	4(%rcx,%rax), %eax
	movzbl	2(%rsi), %ecx
	sall	$16, %ecx
	addl	%ecx, %eax
	cmpl	%edx, %eax
	jne	.L1211
	movq	-792(%rbp), %rax
	cmpb	$10, 4(%rax)
	jne	.L1212
	movl	$5, %ecx
.L591:
	movq	-792(%rbp), %rsi
	movl	%ecx, %eax
	cmpb	$0, (%rsi,%rcx)
	je	.L1213
	addq	$1, %rcx
	cmpl	%ecx, %edx
	jg	.L591
	movq	%r13, -704(%rbp)
	movl	-708(%rbp), %r14d
	leaq	.LC167(%rip), %rsi
	movq	-696(%rbp), %r13
.L1083:
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
	movl	$1, %r15d
	call	BIO_printf@PLT
	movl	-720(%rbp), %r12d
	jmp	.L529
.L548:
	call	BIO_f_buffer@PLT
	movq	%rax, %rdi
	call	BIO_new@PLT
	movq	%r13, %rsi
	movq	%rax, %r14
	movq	%rax, %rdi
	call	BIO_push@PLT
	leaq	.LC143(%rip), %rsi
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	movq	%r14, %rdi
	call	BIO_ctrl@PLT
	movq	%rbx, %rdi
	call	SSL_get_fd@PLT
	addl	$1, %eax
	movl	%eax, -920(%rbp)
	jmp	.L586
.L1216:
	cmpl	$421, %r12d
	je	.L780
.L583:
	cmpl	$691, %r12d
	je	.L1214
	cmpl	$670, %r12d
	je	.L582
.L586:
	movq	-856(%rbp), %rdi
	xorl	%eax, %eax
	movl	$16, %ecx
	movl	$0, -584(%rbp)
#APP
# 2400 "../deps/openssl/openssl/apps/s_client.c" 1
	cld; rep; stosq
# 0 "" 2
#NO_APP
	movq	%rbx, %rdi
	call	SSL_get_fd@PLT
	movq	%rbx, %rdi
	movl	%eax, %r12d
	call	SSL_get_fd@PLT
	movslq	%eax, %rdi
	call	__fdelt_chk@PLT
	movl	$64, %esi
	movdqa	.LC158(%rip), %xmm3
	movq	%r14, %rdi
	movq	%rax, %r8
	movl	%r12d, %eax
	cltd
	movaps	%xmm3, -576(%rbp)
	idivl	%esi
	movl	$1, %eax
	movl	$116, %esi
	movl	%edx, %ecx
	xorl	%edx, %edx
	salq	%cl, %rax
	xorl	%ecx, %ecx
	orq	%rax, -512(%rbp,%r8,8)
	call	BIO_ctrl@PLT
	testq	%rax, %rax
	jne	.L581
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$10, %esi
	movq	%r14, %rdi
	call	BIO_ctrl@PLT
	testl	%eax, %eax
	je	.L1215
.L581:
	movl	$8192, %edx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	BIO_gets@PLT
	movl	%eax, -840(%rbp)
	testl	%eax, %eax
	jle	.L582
	xorl	%eax, %eax
	leaq	-584(%rbp), %rdx
	leaq	.LC160(%rip), %rsi
	movq	%r15, %rdi
	call	__isoc99_sscanf@PLT
	subl	$1, %eax
	jne	.L582
	movl	-584(%rbp), %r12d
	cmpl	$451, %r12d
	jne	.L1216
.L780:
	leaq	.LC141(%rip), %rsi
	movq	%r15, %rdi
	call	strstr@PLT
	testq	%rax, %rax
	je	.L583
	movq	bio_err(%rip), %rdi
	movq	%r15, %rdx
	leaq	.LC161(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L582
.L1209:
	movq	bio_err(%rip), %rdi
	leaq	.LC142(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L599
.L1215:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$10, %esi
	movq	%r13, %rdi
	call	BIO_ctrl@PLT
	testl	%eax, %eax
	jne	.L581
	movq	-856(%rbp), %rsi
	movl	-920(%rbp), %edi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	leaq	-576(%rbp), %r8
	call	select@PLT
	testl	%eax, %eax
	jg	.L581
	movq	bio_err(%rip), %rdi
	movl	$8, %edx
	leaq	.LC159(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L582:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	movq	%r14, %rdi
	call	BIO_ctrl@PLT
	movq	%r14, %rdi
	call	BIO_pop@PLT
	movq	%r14, %rdi
	call	BIO_free@PLT
	cmpl	$670, -584(%rbp)
	je	.L540
	movq	%r13, -704(%rbp)
	movl	-708(%rbp), %r14d
	leaq	.LC163(%rip), %rsi
	movq	-696(%rbp), %r13
	jmp	.L1083
.L1213:
	leal	15(%rcx), %ecx
	cmpl	%edx, %ecx
	jge	.L1217
	movq	-792(%rbp), %rcx
	leal	14(%rax), %edx
	addl	$13, %eax
	cltq
	cmpb	$0, (%rcx,%rax)
	jne	.L1218
	movq	-792(%rbp), %rcx
	movslq	%edx, %rax
	testb	$8, 1(%rcx,%rax)
	je	.L1219
	movq	%r13, %rdi
	movl	$36, %edx
	leaq	ssl_req.29378(%rip), %rsi
	call	BIO_write@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	movq	%r13, %rdi
	call	BIO_ctrl@PLT
	jmp	.L540
.L1214:
	movq	bio_err(%rip), %rdi
	leaq	.LC162(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	jmp	.L582
.L1219:
	movq	%r13, -704(%rbp)
	movl	-708(%rbp), %r14d
	leaq	.LC170(%rip), %rsi
	movq	-696(%rbp), %r13
	jmp	.L1083
.L1218:
	movq	%r13, -704(%rbp)
	movl	-708(%rbp), %r14d
	leaq	.LC169(%rip), %rsi
	movq	-696(%rbp), %r13
	jmp	.L1083
.L1217:
	movq	%r13, -704(%rbp)
	movl	-708(%rbp), %r14d
	leaq	.LC168(%rip), %rsi
	movq	-696(%rbp), %r13
	jmp	.L1083
.L1212:
	movq	%r13, -704(%rbp)
	movl	-708(%rbp), %r14d
	leaq	.LC166(%rip), %rsi
	movq	-696(%rbp), %r13
	jmp	.L1083
.L1211:
	movq	%r13, -704(%rbp)
	movl	-708(%rbp), %r14d
	leaq	.LC165(%rip), %rsi
	movq	-696(%rbp), %r13
	jmp	.L1083
.L1210:
	movq	%r13, -704(%rbp)
	movl	-708(%rbp), %r14d
	leaq	.LC164(%rip), %rsi
	movq	-696(%rbp), %r13
	jmp	.L1083
.L1103:
	movq	%r13, -704(%rbp)
	movl	-708(%rbp), %r14d
	leaq	.LC145(%rip), %rsi
	movq	-696(%rbp), %r13
	jmp	.L1083
.L549:
	call	BIO_f_buffer@PLT
	movq	%rax, %rdi
	call	BIO_new@PLT
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	BIO_push@PLT
	movq	-768(%rbp), %rdx
	leaq	.LC154(%rip), %rsi
	xorl	%eax, %eax
	movq	%r12, %rdi
	call	BIO_printf@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	movq	%r12, %rdi
	call	BIO_ctrl@PLT
	movl	$8192, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	BIO_gets@PLT
	movl	%eax, %ecx
	cmpl	$11, %eax
	jle	.L1220
	cmpb	$32, 8(%r15)
	je	.L578
	movq	prog(%rip), %rdx
	movq	bio_err(%rip), %rdi
	leaq	.LC156(%rip), %rsi
	xorl	%eax, %eax
	movq	%r13, -704(%rbp)
	movl	-708(%rbp), %r14d
	movq	-696(%rbp), %r13
	call	BIO_printf@PLT
.L1082:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	movq	%r12, %rdi
	call	BIO_ctrl@PLT
	movq	%r12, %rdi
	movl	$1, %r15d
	call	BIO_pop@PLT
	movq	%r12, %rdi
	call	BIO_free@PLT
	movl	-720(%rbp), %r12d
	jmp	.L529
.L550:
	movq	-960(%rbp), %rax
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1221
.L570:
	cmpl	$6, %edx
	leaq	.LC67(%rip), %rax
	leaq	.LC66(%rip), %rdx
	movq	%r13, %rdi
	cmovne	%rax, %rdx
	leaq	.LC149(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movl	$8192, %edx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	BIO_read@PLT
	testl	%eax, %eax
	js	.L1086
	cltq
	leaq	.LC150(%rip), %r12
	movb	$0, (%r15,%rax)
	jmp	.L573
.L1222:
	leaq	.LC151(%rip), %rsi
	movq	%r15, %rdi
	call	strstr@PLT
	testq	%rax, %rax
	jne	.L574
	movl	$8192, %edx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	BIO_read@PLT
	testl	%eax, %eax
	jle	.L1075
	cltq
	movb	$0, (%r15,%rax)
.L573:
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	strstr@PLT
	testq	%rax, %rax
	je	.L1222
.L574:
	leaq	.LC152(%rip), %rsi
	movq	%r13, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	-792(%rbp), %rsi
	movl	$8192, %edx
	movq	%r13, %rdi
	call	BIO_read@PLT
	testl	%eax, %eax
	js	.L1103
	movq	-792(%rbp), %rcx
	cltq
	leaq	.LC153(%rip), %rsi
	movb	$0, (%rcx,%rax)
	movq	%rcx, %rdi
	call	strstr@PLT
	testq	%rax, %rax
	je	.L1075
	movb	$0, (%r15)
	jmp	.L540
.L578:
	cmpb	$50, 9(%r15)
	movl	$2, %r14d
	je	.L580
	movq	prog(%rip), %rdx
	movq	bio_err(%rip), %rdi
	leaq	9(%r15), %rcx
	xorl	%eax, %eax
	leaq	.LC157(%rip), %rsi
	movl	$1, %r14d
	call	BIO_printf@PLT
.L580:
	movl	$8192, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	BIO_gets@PLT
	movl	%eax, -840(%rbp)
	cmpl	$2, %eax
	jg	.L580
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	movq	%r12, %rdi
	call	BIO_ctrl@PLT
	movq	%r12, %rdi
	call	BIO_pop@PLT
	movq	%r12, %rdi
	call	BIO_free@PLT
	cmpl	$2, %r14d
	je	.L540
	jmp	.L1075
.L1220:
	movq	prog(%rip), %rdx
	movq	bio_err(%rip), %rdi
	leaq	.LC155(%rip), %rsi
	xorl	%eax, %eax
	movq	%r13, -704(%rbp)
	movl	-708(%rbp), %r14d
	movq	-696(%rbp), %r13
	call	BIO_printf@PLT
	jmp	.L1082
.L551:
	movl	$8192, %edx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	BIO_read@PLT
	cmpl	$3, %eax
	jne	.L1075
	movl	$3, %edx
	leaq	tls_do.29355(%rip), %rsi
	movq	%r15, %rdi
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L1075
	movl	$3, %edx
	leaq	tls_will.29356(%rip), %rsi
	movq	%r13, %rdi
	call	BIO_write@PLT
	movl	$6, %edx
	leaq	tls_follows.29357(%rip), %rsi
	movq	%r13, %rdi
	call	BIO_write@PLT
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movl	$11, %esi
	movq	%r13, %rdi
	call	BIO_ctrl@PLT
	movl	$8192, %edx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	BIO_read@PLT
	cmpl	$6, %eax
	jne	.L1075
	movl	$6, %edx
	leaq	tls_follows.29357(%rip), %rsi
	movq	%r15, %rdi
	call	memcmp@PLT
	testl	%eax, %eax
	je	.L540
	jmp	.L1075
.L552:
	call	BIO_f_buffer@PLT
	movq	%rax, %rdi
	call	BIO_new@PLT
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	BIO_push@PLT
.L965:
	movl	$8192, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	BIO_gets@PLT
	movl	%eax, -840(%rbp)
	cmpl	$3, %eax
	jle	.L568
	call	__ctype_b_loc@PLT
	movsbq	(%r15), %rdx
	movq	(%rax), %rax
	testb	$8, 1(%rax,%rdx,2)
	je	.L965
	movsbq	1(%r15), %rdx
	testb	$8, 1(%rax,%rdx,2)
	je	.L965
	movsbq	2(%r15), %rdx
	testb	$8, 1(%rax,%rdx,2)
	je	.L965
	cmpb	$32, 3(%r15)
	jne	.L965
.L568:
	movl	$11, %esi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	BIO_ctrl@PLT
	movq	%r12, %rdi
	call	BIO_pop@PLT
	movq	%r12, %rdi
	call	BIO_free@PLT
	leaq	.LC148(%rip), %rsi
	jmp	.L1079
.L1221:
	movq	-640(%rbp), %rcx
	jmp	.L570
.L1167:
	call	BIO_f_nbio_test@PLT
	movq	%rax, %rdi
	call	BIO_new@PLT
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	BIO_push@PLT
	movq	%rax, %r13
	jmp	.L530
.L1166:
	movq	bio_err(%rip), %rdi
	movq	%r13, %r15
	leaq	.LC138(%rip), %rsi
	movq	%r13, -704(%rbp)
	movl	-708(%rbp), %r14d
	movq	-696(%rbp), %r13
	call	BIO_printf@PLT
	movq	%r15, %rdi
	call	BIO_free@PLT
.L526:
	movl	-720(%rbp), %r12d
	movl	-884(%rbp), %r15d
	jmp	.L529
.L1165:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movl	$121, %esi
	movq	%r13, -704(%rbp)
	movq	%r13, %r15
	movl	-708(%rbp), %r14d
	movq	-696(%rbp), %r13
	call	SSL_ctrl@PLT
	movq	bio_err(%rip), %rdi
	leaq	.LC137(%rip), %rsi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	%r15, %rdi
	call	BIO_free@PLT
	jmp	.L526
.L524:
	movl	$39, %esi
	movq	%r13, %rdi
	call	BIO_ctrl@PLT
	jmp	.L528
.L1164:
	movdqa	.LC136(%rip), %xmm1
	leaq	-576(%rbp), %r12
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	%r12, %rcx
	movl	$33, %esi
	movaps	%xmm1, -576(%rbp)
	call	BIO_ctrl@PLT
	movq	%r12, %rcx
	xorl	%edx, %edx
	movl	$35, %esi
	movdqa	.LC136(%rip), %xmm2
	movq	%r13, %rdi
	movaps	%xmm2, -576(%rbp)
	call	BIO_ctrl@PLT
	jmp	.L523
.L1163:
	call	__errno_location@PLT
	movq	bio_err(%rip), %rdi
	leaq	.LC135(%rip), %rsi
	movl	-708(%rbp), %r14d
	movl	(%rax), %edx
	xorl	%eax, %eax
	movq	-696(%rbp), %r13
	call	BIO_printf@PLT
	movq	-584(%rbp), %rdi
	call	BIO_ADDR_free@PLT
	movl	-676(%rbp), %edi
	call	BIO_closesocket@PLT
.L521:
	movl	-884(%rbp), %r15d
	jmp	.L262
.L1162:
	movq	bio_err(%rip), %rdi
	leaq	.LC134(%rip), %rsi
	movl	-708(%rbp), %r14d
	movq	-696(%rbp), %r13
	call	BIO_printf@PLT
	movl	-676(%rbp), %edi
	call	BIO_closesocket@PLT
	jmp	.L521
.L519:
	call	BIO_new_socket@PLT
	movq	%rax, %r13
	jmp	.L528
.L1161:
	movl	-676(%rbp), %edi
	movl	$1, %esi
	call	BIO_socket_nbio@PLT
	testl	%eax, %eax
	je	.L1223
	movq	bio_c_out(%rip), %rdi
	leaq	.LC133(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L517
.L1160:
	call	__errno_location@PLT
	movq	bio_err(%rip), %rdi
	leaq	.LC131(%rip), %rsi
	movl	-708(%rbp), %r14d
	movl	(%rax), %edx
	xorl	%eax, %eax
	movq	-696(%rbp), %r13
	movl	$1, %r15d
	call	BIO_printf@PLT
	movl	-676(%rbp), %edi
	call	BIO_closesocket@PLT
	jmp	.L262
.L1223:
	movq	bio_err(%rip), %rdi
	movl	-708(%rbp), %r14d
	movq	-696(%rbp), %r13
	call	ERR_print_errors@PLT
	movl	-1000(%rbp), %r15d
	jmp	.L262
.L1159:
	movl	$1, %esi
	movq	%rbx, %rdi
	call	SSL_dane_set_flags@PLT
	jmp	.L515
.L1158:
	movq	prog(%rip), %rdx
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
	movl	$1, %r15d
	leaq	.LC129(%rip), %rsi
	call	BIO_printf@PLT
	jmp	.L262
.L1157:
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	movq	prog(%rip), %rdx
	movq	%r12, %rcx
	xorl	%eax, %eax
	movq	bio_err(%rip), %rdi
	leaq	.LC128(%rip), %rsi
	call	BIO_printf@PLT
	jmp	.L508
.L1156:
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	movq	prog(%rip), %rdx
	movq	%r12, %rcx
	xorl	%eax, %eax
	movq	bio_err(%rip), %rdi
	leaq	.LC127(%rip), %rsi
	call	BIO_printf@PLT
	jmp	.L508
.L1170:
	movq	bio_c_out(%rip), %rcx
	jmp	.L535
.L1169:
	movq	SSL_trace@GOTPCREL(%rip), %rsi
	movq	%rbx, %rdi
	call	SSL_set_msg_callback@PLT
	jmp	.L534
.L1153:
	movq	prog(%rip), %rdx
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
	movl	$1, %r15d
	leaq	.LC125(%rip), %rsi
	call	BIO_printf@PLT
	jmp	.L262
.L1152:
	movq	prog(%rip), %rdx
	leaq	.LC121(%rip), %rsi
.L1088:
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
	movl	$1, %r15d
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	jmp	.L262
.L490:
	cmpq	$0, -904(%rbp)
	jne	.L493
	movq	-640(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L749
	movq	%rdi, -904(%rbp)
	orq	$-1, %rdx
	xorl	%eax, %eax
	movq	%rdi, %r11
	movq	%rdx, %rcx
	movl	$1, %esi
	repnz scasb
	xorl	%edi, %edi
	xorl	%eax, %eax
	movq	%rcx, %rdx
	xorl	%ecx, %ecx
	notq	%rdx
	leaq	-1(%rdx), %r9
	subq	$2, %rdx
.L494:
	cmpq	%rcx, %r9
	jbe	.L779
	cmpq	$62, %rax
	ja	.L779
	movzbl	(%r11,%rcx), %r8d
	movl	%r8d, %r10d
	andl	$-33, %r10d
	subl	$65, %r10d
	cmpb	$25, %r10b
	jbe	.L778
	cmpb	$95, %r8b
	je	.L778
	leal	-48(%r8), %r10d
	cmpb	$9, %r10b
	jbe	.L1078
	testq	%rcx, %rcx
	jne	.L1224
.L489:
	cmpq	$0, -920(%rbp)
	jne	.L502
	cmpq	$0, -784(%rbp)
	je	.L515
	movq	prog(%rip), %rdx
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
	movl	$1, %r15d
	leaq	.LC130(%rip), %rsi
	call	BIO_printf@PLT
	jmp	.L262
.L749:
	leaq	.LC68(%rip), %rax
	movq	%rax, -904(%rbp)
.L493:
	movq	-904(%rbp), %rcx
	xorl	%edx, %edx
	movl	$55, %esi
	movq	%rbx, %rdi
	call	SSL_ctrl@PLT
	testq	%rax, %rax
	jne	.L489
	movq	bio_err(%rip), %rdi
	leaq	.LC124(%rip), %rsi
	xorl	%eax, %eax
	movl	$1, %r15d
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	jmp	.L262
.L1150:
	movq	bio_err(%rip), %rdi
	leaq	.LC123(%rip), %rsi
	movl	$1, %r15d
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	jmp	.L262
.L1102:
	movq	-976(%rbp), %rdx
	leaq	.LC122(%rip), %rsi
	jmp	.L1088
.L1151:
	xorl	%ecx, %ecx
	movl	$128, %edx
	movl	$33, %esi
	movq	%rbx, %rdi
	call	SSL_ctrl@PLT
	jmp	.L488
.L1224:
	cmpq	%rdx, %rcx
	jnb	.L489
	cmpb	$45, %r8b
	je	.L1078
	cmpb	$46, %r8b
	jne	.L489
	movzbl	1(%r11,%rcx), %eax
	cmpb	$46, %al
	je	.L489
	cmpb	$45, -1(%r11,%rcx)
	je	.L489
	cmpb	$45, %al
	je	.L489
	xorl	%eax, %eax
	movl	$1, %edi
.L497:
	addq	$1, %rcx
	jmp	.L494
.L1078:
	addq	$1, %rax
	jmp	.L497
.L778:
	addq	$1, %rax
	xorl	%esi, %esi
	jmp	.L497
.L779:
	cmpq	$63, %rax
	movl	%esi, %eax
	setne	%dl
	xorl	$1, %eax
	andl	%edx, %eax
	testl	%edi, %eax
	jne	.L493
	jmp	.L489
.L455:
	cmpq	$0, psk_key(%rip)
	jne	.L457
.L458:
	cmpq	$0, psksess(%rip)
	je	.L460
	jmp	.L457
.L1135:
	movq	bio_c_out(%rip), %rdi
	leaq	.LC112(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L454
.L453:
	cmpq	$0, -968(%rbp)
	jne	.L717
	jmp	.L458
.L1138:
	leaq	.LC116(%rip), %rsi
.L1095:
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L1101
.L1132:
	movq	prog(%rip), %rdx
	movl	%r12d, %ecx
	movq	bio_err(%rip), %rdi
	leaq	.LC108(%rip), %rsi
	movl	$1, %r15d
	call	BIO_printf@PLT
	jmp	.L697
.L450:
	movq	X509_NAME_free@GOTPCREL(%rip), %rsi
	movq	%rbx, %rdi
	call	OPENSSL_sk_pop_free@PLT
	leaq	.LC110(%rip), %rsi
	jmp	.L1095
.L1133:
	leaq	.LC109(%rip), %rsi
	jmp	.L1098
.L1134:
	movq	bio_err(%rip), %rdi
	leaq	.LC111(%rip), %rsi
	movl	$1, %r15d
	call	BIO_puts@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	movq	-984(%rbp), %rdi
	call	ENGINE_free@PLT
	jmp	.L697
.L1137:
	movq	-968(%rbp), %rdx
	leaq	.LC115(%rip), %rsi
.L1093:
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L1101
.L1136:
	movq	-968(%rbp), %rdx
	leaq	.LC114(%rip), %rsi
	jmp	.L1093
.L1140:
	movq	bio_err(%rip), %rdi
	leaq	.LC118(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L1099
.L1139:
	movq	bio_err(%rip), %rdi
	leaq	.LC117(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L1099
.L1145:
	movq	apps_ssl_info_callback@GOTPCREL(%rip), %rsi
	movq	%r13, %rdi
	call	SSL_CTX_set_info_callback@PLT
	jmp	.L471
.L1147:
	movq	bio_err(%rip), %rdi
	leaq	.LC120(%rip), %rsi
	movl	$1, %r15d
	call	BIO_printf@PLT
	jmp	.L697
.L1146:
	movq	bio_err(%rip), %rax
	movq	%r13, %rdi
	movl	$53, %esi
	leaq	ssl_servername_cb(%rip), %rdx
	movq	%rax, -560(%rbp)
	call	SSL_CTX_callback_ctrl@PLT
	xorl	%edx, %edx
	movl	$54, %esi
	movq	%r13, %rdi
	leaq	-560(%rbp), %rcx
	call	SSL_CTX_ctrl@PLT
	jmp	.L476
.L1148:
	movq	prog(%rip), %rdx
	leaq	.LC121(%rip), %rsi
	jmp	.L1093
	.cfi_endproc
.LFE1649:
	.size	s_client_main, .-s_client_main
	.local	usage.28723
	.comm	usage.28723,1,1
	.local	selector.28724
	.comm	selector.28724,1,1
	.local	mtype.28725
	.comm	mtype.28725,1,1
	.local	data.28726
	.comm	data.28726,8,8
	.section	.rodata.str1.1
.LC211:
	.string	"usage"
.LC212:
	.string	"selector"
.LC213:
	.string	"mtype"
.LC214:
	.string	"data"
	.section	.data.rel.local,"aw"
	.align 32
	.type	tlsa_fields.28727, @object
	.size	tlsa_fields.28727, 120
tlsa_fields.28727:
	.quad	usage.28723
	.quad	.LC211
	.quad	checked_uint8
	.quad	selector.28724
	.quad	.LC212
	.quad	checked_uint8
	.quad	mtype.28725
	.quad	.LC213
	.quad	checked_uint8
	.quad	data.28726
	.quad	.LC214
	.quad	hexdecode
	.quad	0
	.zero	16
	.data
	.align 32
	.type	ldap_tls_genconf.29399, @object
	.size	ldap_tls_genconf.29399, 138
ldap_tls_genconf.29399:
	.string	"asn1=SEQUENCE:LDAPMessage\n[LDAPMessage]\nmessageID=INTEGER:1\nextendedReq=EXPLICIT:23A,IMPLICIT:0C,FORMAT:ASCII,OCT:1.3.6.1.4.1.1466.20037\n"
	.section	.rodata
	.align 8
	.type	ssl_request.29386, @object
	.size	ssl_request.29386, 8
ssl_request.29386:
	.string	""
	.string	""
	.string	""
	.ascii	"\b\004\322\026/"
	.align 32
	.type	ssl_req.29378, @object
	.size	ssl_req.29378, 36
ssl_req.29378:
	.string	" "
	.string	""
	.string	"\001\205\256\177"
	.string	""
	.string	""
	.string	""
	.string	"\001!"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.type	tls_follows.29357, @object
	.size	tls_follows.29357, 6
tls_follows.29357:
	.ascii	"\377\372.\001\377\360"
	.type	tls_will.29356, @object
	.size	tls_will.29356, 3
tls_will.29356:
	.ascii	"\377\373."
	.type	tls_do.29355, @object
	.size	tls_do.29355, 3
tls_do.29355:
	.ascii	"\377\375."
	.section	.rodata.str1.1
.LC215:
	.string	"smtp"
.LC216:
	.string	"pop3"
.LC217:
	.string	"imap"
.LC218:
	.string	"ftp"
.LC219:
	.string	"xmpp"
.LC220:
	.string	"xmpp-server"
.LC221:
	.string	"telnet"
.LC222:
	.string	"irc"
.LC223:
	.string	"mysql"
.LC224:
	.string	"postgres"
.LC225:
	.string	"lmtp"
.LC226:
	.string	"nntp"
.LC227:
	.string	"sieve"
.LC228:
	.string	"ldap"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	services, @object
	.size	services, 240
services:
	.quad	.LC215
	.long	1
	.zero	4
	.quad	.LC216
	.long	2
	.zero	4
	.quad	.LC217
	.long	3
	.zero	4
	.quad	.LC218
	.long	4
	.zero	4
	.quad	.LC219
	.long	6
	.zero	4
	.quad	.LC220
	.long	7
	.zero	4
	.quad	.LC221
	.long	5
	.zero	4
	.quad	.LC222
	.long	9
	.zero	4
	.quad	.LC223
	.long	10
	.zero	4
	.quad	.LC224
	.long	11
	.zero	4
	.quad	.LC225
	.long	12
	.zero	4
	.quad	.LC226
	.long	13
	.zero	4
	.quad	.LC227
	.long	14
	.zero	4
	.quad	.LC228
	.long	15
	.zero	4
	.quad	0
	.long	0
	.zero	4
	.globl	s_client_options
	.section	.rodata.str1.1
.LC229:
	.string	"help"
.LC230:
	.string	"Display this summary"
.LC231:
	.string	"host"
.LC232:
	.string	"Use -connect instead"
.LC233:
	.string	"port"
.LC234:
	.string	"connect"
	.section	.rodata.str1.8
	.align 8
.LC235:
	.string	"TCP/IP where to connect (default is :4433)"
	.section	.rodata.str1.1
.LC236:
	.string	"bind"
	.section	.rodata.str1.8
	.align 8
.LC237:
	.string	"bind local address for connection"
	.section	.rodata.str1.1
.LC238:
	.string	"proxy"
	.section	.rodata.str1.8
	.align 8
.LC239:
	.string	"Connect to via specified proxy to the real server"
	.section	.rodata.str1.1
.LC240:
	.string	"unix"
	.section	.rodata.str1.8
	.align 8
.LC241:
	.string	"Connect over the specified Unix-domain socket"
	.section	.rodata.str1.1
.LC242:
	.string	"4"
.LC243:
	.string	"Use IPv4 only"
.LC244:
	.string	"6"
.LC245:
	.string	"Use IPv6 only"
.LC246:
	.string	"verify"
	.section	.rodata.str1.8
	.align 8
.LC247:
	.string	"Turn on peer certificate verification"
	.section	.rodata.str1.1
.LC248:
	.string	"cert"
	.section	.rodata.str1.8
	.align 8
.LC249:
	.string	"Certificate file to use, PEM format assumed"
	.section	.rodata.str1.1
.LC250:
	.string	"certform"
	.section	.rodata.str1.8
	.align 8
.LC251:
	.string	"Certificate format (PEM or DER) PEM default"
	.section	.rodata.str1.1
.LC252:
	.string	"nameopt"
	.section	.rodata.str1.8
	.align 8
.LC253:
	.string	"Various certificate name options"
	.section	.rodata.str1.1
.LC254:
	.string	"key"
	.section	.rodata.str1.8
	.align 8
.LC255:
	.string	"Private key file to use, if not in -cert file"
	.section	.rodata.str1.1
.LC256:
	.string	"keyform"
	.section	.rodata.str1.8
	.align 8
.LC257:
	.string	"Key format (PEM, DER or engine) PEM default"
	.section	.rodata.str1.1
.LC258:
	.string	"pass"
	.section	.rodata.str1.8
	.align 8
.LC259:
	.string	"Private key file pass phrase source"
	.section	.rodata.str1.1
.LC260:
	.string	"CApath"
.LC261:
	.string	"PEM format directory of CA's"
.LC262:
	.string	"CAfile"
.LC263:
	.string	"PEM format file of CA's"
.LC264:
	.string	"no-CAfile"
	.section	.rodata.str1.8
	.align 8
.LC265:
	.string	"Do not load the default certificates file"
	.section	.rodata.str1.1
.LC266:
	.string	"no-CApath"
	.section	.rodata.str1.8
	.align 8
.LC267:
	.string	"Do not load certificates from the default certificates directory"
	.section	.rodata.str1.1
.LC268:
	.string	"requestCAfile"
	.section	.rodata.str1.8
	.align 8
.LC269:
	.string	"PEM format file of CA names to send to the server"
	.section	.rodata.str1.1
.LC270:
	.string	"dane_tlsa_domain"
.LC271:
	.string	"DANE TLSA base domain"
.LC272:
	.string	"dane_tlsa_rrdata"
	.section	.rodata.str1.8
	.align 8
.LC273:
	.string	"DANE TLSA rrdata presentation form"
	.section	.rodata.str1.1
.LC274:
	.string	"dane_ee_no_namechecks"
	.section	.rodata.str1.8
	.align 8
.LC275:
	.string	"Disable name checks when matching DANE-EE(3) TLSA records"
	.section	.rodata.str1.1
.LC276:
	.string	"reconnect"
	.section	.rodata.str1.8
	.align 8
.LC277:
	.string	"Drop and re-make the connection with the same Session-ID"
	.section	.rodata.str1.1
.LC278:
	.string	"showcerts"
	.section	.rodata.str1.8
	.align 8
.LC279:
	.string	"Show all certificates sent by the server"
	.section	.rodata.str1.1
.LC280:
	.string	"debug"
.LC281:
	.string	"Extra output"
.LC282:
	.string	"msg"
.LC283:
	.string	"Show protocol messages"
.LC284:
	.string	"msgfile"
	.section	.rodata.str1.8
	.align 8
.LC285:
	.string	"File to send output of -msg or -trace, instead of stdout"
	.section	.rodata.str1.1
.LC286:
	.string	"nbio_test"
.LC287:
	.string	"More ssl protocol testing"
.LC288:
	.string	"state"
.LC289:
	.string	"Print the ssl states"
.LC290:
	.string	"crlf"
	.section	.rodata.str1.8
	.align 8
.LC291:
	.string	"Convert LF from terminal into CRLF"
	.section	.rodata.str1.1
.LC292:
	.string	"quiet"
.LC293:
	.string	"No s_client output"
.LC294:
	.string	"ign_eof"
	.section	.rodata.str1.8
	.align 8
.LC295:
	.string	"Ignore input eof (default when -quiet)"
	.section	.rodata.str1.1
.LC296:
	.string	"no_ign_eof"
.LC297:
	.string	"Don't ignore input eof"
.LC298:
	.string	"starttls"
	.section	.rodata.str1.8
	.align 8
.LC299:
	.string	"Use the appropriate STARTTLS command before starting TLS"
	.section	.rodata.str1.1
.LC300:
	.string	"xmpphost"
	.section	.rodata.str1.8
	.align 8
.LC301:
	.string	"Alias of -name option for \"-starttls xmpp[-server]\""
	.section	.rodata.str1.1
.LC302:
	.string	"rand"
	.section	.rodata.str1.8
	.align 8
.LC303:
	.string	"Load the file(s) into the random number generator"
	.section	.rodata.str1.1
.LC304:
	.string	"writerand"
	.section	.rodata.str1.8
	.align 8
.LC305:
	.string	"Write random data to the specified file"
	.section	.rodata.str1.1
.LC306:
	.string	"sess_out"
.LC307:
	.string	"File to write SSL session to"
.LC308:
	.string	"sess_in"
.LC309:
	.string	"File to read SSL session from"
.LC310:
	.string	"use_srtp"
	.section	.rodata.str1.8
	.align 8
.LC311:
	.string	"Offer SRTP key management with a colon-separated profile list"
	.section	.rodata.str1.1
.LC312:
	.string	"keymatexport"
	.section	.rodata.str1.8
	.align 8
.LC313:
	.string	"Export keying material using label"
	.section	.rodata.str1.1
.LC314:
	.string	"keymatexportlen"
	.section	.rodata.str1.8
	.align 8
.LC315:
	.string	"Export len bytes of keying material (default 20)"
	.section	.rodata.str1.1
.LC316:
	.string	"maxfraglen"
	.section	.rodata.str1.8
	.align 8
.LC317:
	.string	"Enable Maximum Fragment Length Negotiation (len values: 512, 1024, 2048 and 4096)"
	.section	.rodata.str1.1
.LC318:
	.string	"fallback_scsv"
.LC319:
	.string	"Send the fallback SCSV"
.LC320:
	.string	"name"
	.section	.rodata.str1.8
	.align 8
.LC321:
	.string	"Hostname to use for \"-starttls lmtp\", \"-starttls smtp\" or \"-starttls xmpp[-server]\""
	.section	.rodata.str1.1
.LC322:
	.string	"CRL"
.LC323:
	.string	"CRL file to use"
.LC324:
	.string	"crl_download"
	.section	.rodata.str1.8
	.align 8
.LC325:
	.string	"Download CRL from distribution points"
	.section	.rodata.str1.1
.LC326:
	.string	"CRLform"
	.section	.rodata.str1.8
	.align 8
.LC327:
	.string	"CRL format (PEM or DER) PEM is default"
	.section	.rodata.str1.1
.LC328:
	.string	"verify_return_error"
	.section	.rodata.str1.8
	.align 8
.LC329:
	.string	"Close connection on verification error"
	.section	.rodata.str1.1
.LC330:
	.string	"verify_quiet"
	.section	.rodata.str1.8
	.align 8
.LC331:
	.string	"Restrict verify output to errors"
	.section	.rodata.str1.1
.LC332:
	.string	"brief"
	.section	.rodata.str1.8
	.align 8
.LC333:
	.string	"Restrict output to brief summary of connection parameters"
	.section	.rodata.str1.1
.LC334:
	.string	"prexit"
	.section	.rodata.str1.8
	.align 8
.LC335:
	.string	"Print session information when the program exits"
	.section	.rodata.str1.1
.LC336:
	.string	"security_debug"
	.section	.rodata.str1.8
	.align 8
.LC337:
	.string	"Enable security debug messages"
	.section	.rodata.str1.1
.LC338:
	.string	"security_debug_verbose"
	.section	.rodata.str1.8
	.align 8
.LC339:
	.string	"Output more security debug output"
	.section	.rodata.str1.1
.LC340:
	.string	"cert_chain"
	.section	.rodata.str1.8
	.align 8
.LC341:
	.string	"Certificate chain file (in PEM format)"
	.section	.rodata.str1.1
.LC342:
	.string	"chainCApath"
	.section	.rodata.str1.8
	.align 8
.LC343:
	.string	"Use dir as certificate store path to build CA certificate chain"
	.section	.rodata.str1.1
.LC344:
	.string	"verifyCApath"
	.section	.rodata.str1.8
	.align 8
.LC345:
	.string	"Use dir as certificate store path to verify CA certificate"
	.section	.rodata.str1.1
.LC346:
	.string	"build_chain"
.LC347:
	.string	"Build certificate chain"
.LC348:
	.string	"chainCAfile"
	.section	.rodata.str1.8
	.align 8
.LC349:
	.string	"CA file for certificate chain (PEM format)"
	.section	.rodata.str1.1
.LC350:
	.string	"verifyCAfile"
	.section	.rodata.str1.8
	.align 8
.LC351:
	.string	"CA file for certificate verification (PEM format)"
	.section	.rodata.str1.1
.LC352:
	.string	"nocommands"
	.section	.rodata.str1.8
	.align 8
.LC353:
	.string	"Do not use interactive command letters"
	.section	.rodata.str1.1
.LC354:
	.string	"servername"
	.section	.rodata.str1.8
	.align 8
.LC355:
	.string	"Set TLS extension servername (SNI) in ClientHello (default)"
	.section	.rodata.str1.1
.LC356:
	.string	"noservername"
	.section	.rodata.str1.8
	.align 8
.LC357:
	.string	"Do not send the server name (SNI) extension in the ClientHello"
	.section	.rodata.str1.1
.LC358:
	.string	"tlsextdebug"
	.section	.rodata.str1.8
	.align 8
.LC359:
	.string	"Hex dump of all TLS extensions received"
	.section	.rodata.str1.1
.LC360:
	.string	"status"
	.section	.rodata.str1.8
	.align 8
.LC361:
	.string	"Request certificate status from server"
	.section	.rodata.str1.1
.LC362:
	.string	"serverinfo"
	.section	.rodata.str1.8
	.align 8
.LC363:
	.string	"types  Send empty ClientHello extensions (comma-separated numbers)"
	.section	.rodata.str1.1
.LC364:
	.string	"alpn"
	.section	.rodata.str1.8
	.align 8
.LC365:
	.string	"Enable ALPN extension, considering named protocols supported (comma-separated list)"
	.section	.rodata.str1.1
.LC366:
	.string	"async"
	.section	.rodata.str1.8
	.align 8
.LC367:
	.string	"Support asynchronous operation"
	.section	.rodata.str1.1
.LC368:
	.string	"ssl_config"
	.section	.rodata.str1.8
	.align 8
.LC369:
	.string	"Use specified configuration file"
	.section	.rodata.str1.1
.LC370:
	.string	"max_send_frag"
.LC371:
	.string	"Maximum Size of send frames "
.LC372:
	.string	"split_send_frag"
	.section	.rodata.str1.8
	.align 8
.LC373:
	.string	"Size used to split data for encrypt pipelines"
	.section	.rodata.str1.1
.LC374:
	.string	"max_pipelines"
	.section	.rodata.str1.8
	.align 8
.LC375:
	.string	"Maximum number of encrypt/decrypt pipelines to be used"
	.section	.rodata.str1.1
.LC376:
	.string	"read_buf"
	.section	.rodata.str1.8
	.align 8
.LC377:
	.string	"Default read buffer size to be used for connections"
	.section	.rodata.str1.1
.LC378:
	.string	"no_ssl3"
.LC379:
	.string	"Just disable SSLv3"
.LC380:
	.string	"no_tls1"
.LC381:
	.string	"Just disable TLSv1"
.LC382:
	.string	"no_tls1_1"
.LC383:
	.string	"Just disable TLSv1.1"
.LC384:
	.string	"no_tls1_2"
.LC385:
	.string	"Just disable TLSv1.2"
.LC386:
	.string	"no_tls1_3"
.LC387:
	.string	"Just disable TLSv1.3"
.LC388:
	.string	"bugs"
.LC389:
	.string	"Turn on SSL bug compatibility"
.LC390:
	.string	"no_comp"
	.section	.rodata.str1.8
	.align 8
.LC391:
	.string	"Disable SSL/TLS compression (default)"
	.section	.rodata.str1.1
.LC392:
	.string	"comp"
.LC393:
	.string	"Use SSL/TLS-level compression"
.LC394:
	.string	"no_ticket"
	.section	.rodata.str1.8
	.align 8
.LC395:
	.string	"Disable use of TLS session tickets"
	.section	.rodata.str1.1
.LC396:
	.string	"serverpref"
	.section	.rodata.str1.8
	.align 8
.LC397:
	.string	"Use server's cipher preferences"
	.section	.rodata.str1.1
.LC398:
	.string	"legacy_renegotiation"
	.section	.rodata.str1.8
	.align 8
.LC399:
	.string	"Enable use of legacy renegotiation (dangerous)"
	.section	.rodata.str1.1
.LC400:
	.string	"no_renegotiation"
.LC401:
	.string	"Disable all renegotiation."
.LC402:
	.string	"legacy_server_connect"
	.section	.rodata.str1.8
	.align 8
.LC403:
	.string	"Allow initial connection to servers that don't support RI"
	.section	.rodata.str1.1
.LC404:
	.string	"no_resumption_on_reneg"
	.section	.rodata.str1.8
	.align 8
.LC405:
	.string	"Disallow session resumption on renegotiation"
	.section	.rodata.str1.1
.LC406:
	.string	"no_legacy_server_connect"
	.section	.rodata.str1.8
	.align 8
.LC407:
	.string	"Disallow initial connection to servers that don't support RI"
	.section	.rodata.str1.1
.LC408:
	.string	"allow_no_dhe_kex"
	.section	.rodata.str1.8
	.align 8
.LC409:
	.string	"In TLSv1.3 allow non-(ec)dhe based key exchange on resumption"
	.section	.rodata.str1.1
.LC410:
	.string	"prioritize_chacha"
	.section	.rodata.str1.8
	.align 8
.LC411:
	.string	"Prioritize ChaCha ciphers when preferred by clients"
	.section	.rodata.str1.1
.LC412:
	.string	"strict"
	.section	.rodata.str1.8
	.align 8
.LC413:
	.string	"Enforce strict certificate checks as per TLS standard"
	.section	.rodata.str1.1
.LC414:
	.string	"sigalgs"
	.section	.rodata.str1.8
	.align 8
.LC415:
	.string	"Signature algorithms to support (colon-separated list)"
	.section	.rodata.str1.1
.LC416:
	.string	"client_sigalgs"
	.section	.rodata.str1.8
	.align 8
.LC417:
	.string	"Signature algorithms to support for client certificate authentication (colon-separated list)"
	.section	.rodata.str1.1
.LC418:
	.string	"groups"
	.section	.rodata.str1.8
	.align 8
.LC419:
	.string	"Groups to advertise (colon-separated list)"
	.section	.rodata.str1.1
.LC420:
	.string	"curves"
.LC421:
	.string	"named_curve"
	.section	.rodata.str1.8
	.align 8
.LC422:
	.string	"Elliptic curve used for ECDHE (server-side only)"
	.section	.rodata.str1.1
.LC423:
	.string	"cipher"
	.section	.rodata.str1.8
	.align 8
.LC424:
	.string	"Specify TLSv1.2 and below cipher list to be used"
	.section	.rodata.str1.1
.LC425:
	.string	"ciphersuites"
	.section	.rodata.str1.8
	.align 8
.LC426:
	.string	"Specify TLSv1.3 ciphersuites to be used"
	.section	.rodata.str1.1
.LC427:
	.string	"min_protocol"
	.section	.rodata.str1.8
	.align 8
.LC428:
	.string	"Specify the minimum protocol version to be used"
	.section	.rodata.str1.1
.LC429:
	.string	"max_protocol"
	.section	.rodata.str1.8
	.align 8
.LC430:
	.string	"Specify the maximum protocol version to be used"
	.section	.rodata.str1.1
.LC431:
	.string	"record_padding"
	.section	.rodata.str1.8
	.align 8
.LC432:
	.string	"Block size to pad TLS 1.3 records to."
	.section	.rodata.str1.1
.LC433:
	.string	"debug_broken_protocol"
	.section	.rodata.str1.8
	.align 8
.LC434:
	.string	"Perform all sorts of protocol violations for testing purposes"
	.section	.rodata.str1.1
.LC435:
	.string	"no_middlebox"
	.section	.rodata.str1.8
	.align 8
.LC436:
	.string	"Disable TLSv1.3 middlebox compat mode"
	.section	.rodata.str1.1
.LC437:
	.string	"policy"
	.section	.rodata.str1.8
	.align 8
.LC438:
	.string	"adds policy to the acceptable policy set"
	.section	.rodata.str1.1
.LC439:
	.string	"purpose"
.LC440:
	.string	"certificate chain purpose"
.LC441:
	.string	"verify_name"
.LC442:
	.string	"verification policy name"
.LC443:
	.string	"verify_depth"
.LC444:
	.string	"chain depth limit"
.LC445:
	.string	"auth_level"
	.section	.rodata.str1.8
	.align 8
.LC446:
	.string	"chain authentication security level"
	.section	.rodata.str1.1
.LC447:
	.string	"attime"
.LC448:
	.string	"verification epoch time"
.LC449:
	.string	"verify_hostname"
.LC450:
	.string	"expected peer hostname"
.LC451:
	.string	"verify_email"
.LC452:
	.string	"expected peer email"
.LC453:
	.string	"verify_ip"
.LC454:
	.string	"expected peer IP address"
.LC455:
	.string	"ignore_critical"
	.section	.rodata.str1.8
	.align 8
.LC456:
	.string	"permit unhandled critical extensions"
	.section	.rodata.str1.1
.LC457:
	.string	"issuer_checks"
.LC458:
	.string	"(deprecated)"
.LC459:
	.string	"crl_check"
	.section	.rodata.str1.8
	.align 8
.LC460:
	.string	"check leaf certificate revocation"
	.section	.rodata.str1.1
.LC461:
	.string	"crl_check_all"
.LC462:
	.string	"check full chain revocation"
.LC463:
	.string	"policy_check"
.LC464:
	.string	"perform rfc5280 policy checks"
.LC465:
	.string	"explicit_policy"
	.section	.rodata.str1.8
	.align 8
.LC466:
	.string	"set policy variable require-explicit-policy"
	.section	.rodata.str1.1
.LC467:
	.string	"inhibit_any"
	.section	.rodata.str1.8
	.align 8
.LC468:
	.string	"set policy variable inhibit-any-policy"
	.section	.rodata.str1.1
.LC469:
	.string	"inhibit_map"
	.section	.rodata.str1.8
	.align 8
.LC470:
	.string	"set policy variable inhibit-policy-mapping"
	.section	.rodata.str1.1
.LC471:
	.string	"x509_strict"
	.section	.rodata.str1.8
	.align 8
.LC472:
	.string	"disable certificate compatibility work-arounds"
	.section	.rodata.str1.1
.LC473:
	.string	"extended_crl"
.LC474:
	.string	"enable extended CRL features"
.LC475:
	.string	"use_deltas"
.LC476:
	.string	"use delta CRLs"
.LC477:
	.string	"policy_print"
	.section	.rodata.str1.8
	.align 8
.LC478:
	.string	"print policy processing diagnostics"
	.section	.rodata.str1.1
.LC479:
	.string	"check_ss_sig"
.LC480:
	.string	"check root CA self-signatures"
.LC481:
	.string	"trusted_first"
	.section	.rodata.str1.8
	.align 8
.LC482:
	.string	"search trust store first (default)"
	.section	.rodata.str1.1
.LC483:
	.string	"suiteB_128_only"
.LC484:
	.string	"Suite B 128-bit-only mode"
.LC485:
	.string	"suiteB_128"
	.section	.rodata.str1.8
	.align 8
.LC486:
	.string	"Suite B 128-bit mode allowing 192-bit algorithms"
	.section	.rodata.str1.1
.LC487:
	.string	"suiteB_192"
.LC488:
	.string	"Suite B 192-bit-only mode"
.LC489:
	.string	"partial_chain"
	.section	.rodata.str1.8
	.align 8
.LC490:
	.string	"accept chains anchored by intermediate trust-store CAs"
	.section	.rodata.str1.1
.LC491:
	.string	"no_alt_chains"
.LC492:
	.string	"no_check_time"
	.section	.rodata.str1.8
	.align 8
.LC493:
	.string	"ignore certificate validity time"
	.section	.rodata.str1.1
.LC494:
	.string	"allow_proxy_certs"
	.section	.rodata.str1.8
	.align 8
.LC495:
	.string	"allow the use of proxy certificates"
	.section	.rodata.str1.1
.LC496:
	.string	"xkey"
.LC497:
	.string	"key for Extended certificates"
.LC498:
	.string	"xcert"
	.section	.rodata.str1.8
	.align 8
.LC499:
	.string	"cert for Extended certificates"
	.section	.rodata.str1.1
.LC500:
	.string	"xchain"
	.section	.rodata.str1.8
	.align 8
.LC501:
	.string	"chain for Extended certificates"
	.section	.rodata.str1.1
.LC502:
	.string	"xchain_build"
	.section	.rodata.str1.8
	.align 8
.LC503:
	.string	"build certificate chain for the extended certificates"
	.section	.rodata.str1.1
.LC504:
	.string	"xcertform"
	.section	.rodata.str1.8
	.align 8
.LC505:
	.string	"format of Extended certificate (PEM or DER) PEM default "
	.section	.rodata.str1.1
.LC506:
	.string	"xkeyform"
	.section	.rodata.str1.8
	.align 8
.LC507:
	.string	"format of Extended certificate's key (PEM or DER) PEM default"
	.section	.rodata.str1.1
.LC508:
	.string	"tls1"
.LC509:
	.string	"Just use TLSv1"
.LC510:
	.string	"tls1_1"
.LC511:
	.string	"Just use TLSv1.1"
.LC512:
	.string	"tls1_2"
.LC513:
	.string	"Just use TLSv1.2"
.LC514:
	.string	"tls1_3"
.LC515:
	.string	"Just use TLSv1.3"
.LC516:
	.string	"dtls"
.LC517:
	.string	"Use any version of DTLS"
.LC518:
	.string	"timeout"
	.section	.rodata.str1.8
	.align 8
.LC519:
	.string	"Enable send/receive timeout on DTLS connections"
	.section	.rodata.str1.1
.LC520:
	.string	"mtu"
.LC521:
	.string	"Set the link layer MTU"
.LC522:
	.string	"dtls1"
.LC523:
	.string	"Just use DTLSv1"
.LC524:
	.string	"dtls1_2"
.LC525:
	.string	"Just use DTLSv1.2"
.LC526:
	.string	"trace"
	.section	.rodata.str1.8
	.align 8
.LC527:
	.string	"Show trace output of protocol messages"
	.section	.rodata.str1.1
.LC528:
	.string	"nbio"
.LC529:
	.string	"Use non-blocking IO"
.LC530:
	.string	"psk_identity"
.LC531:
	.string	"PSK identity"
.LC532:
	.string	"psk"
.LC533:
	.string	"PSK in hex (without 0x)"
.LC534:
	.string	"psk_session"
	.section	.rodata.str1.8
	.align 8
.LC535:
	.string	"File to read PSK SSL session from"
	.section	.rodata.str1.1
.LC536:
	.string	"srpuser"
.LC537:
	.string	"SRP authentication for 'user'"
.LC538:
	.string	"srppass"
.LC539:
	.string	"Password for 'user'"
.LC540:
	.string	"srp_lateuser"
	.section	.rodata.str1.8
	.align 8
.LC541:
	.string	"SRP username into second ClientHello message"
	.section	.rodata.str1.1
.LC542:
	.string	"srp_moregroups"
	.section	.rodata.str1.8
	.align 8
.LC543:
	.string	"Tolerate other than the known g N values."
	.section	.rodata.str1.1
.LC544:
	.string	"srp_strength"
.LC545:
	.string	"Minimal length in bits for N"
.LC546:
	.string	"nextprotoneg"
	.section	.rodata.str1.8
	.align 8
.LC547:
	.string	"Enable NPN extension, considering named protocols supported (comma-separated list)"
	.section	.rodata.str1.1
.LC548:
	.string	"engine"
	.section	.rodata.str1.8
	.align 8
.LC549:
	.string	"Use engine, possibly a hardware device"
	.section	.rodata.str1.1
.LC550:
	.string	"ssl_client_engine"
	.section	.rodata.str1.8
	.align 8
.LC551:
	.string	"Specify engine to be used for client certificate operations"
	.section	.rodata.str1.1
.LC552:
	.string	"ct"
	.section	.rodata.str1.8
	.align 8
.LC553:
	.string	"Request and parse SCTs (also enables OCSP stapling)"
	.section	.rodata.str1.1
.LC554:
	.string	"noct"
	.section	.rodata.str1.8
	.align 8
.LC555:
	.string	"Do not request or parse SCTs (default)"
	.section	.rodata.str1.1
.LC556:
	.string	"ctlogfile"
.LC557:
	.string	"CT log list CONF file"
.LC558:
	.string	"keylogfile"
.LC559:
	.string	"Write TLS secrets to file"
.LC560:
	.string	"early_data"
.LC561:
	.string	"File to send as early data"
.LC562:
	.string	"enable_pha"
	.section	.rodata.str1.8
	.align 8
.LC563:
	.string	"Enable post-handshake-authentication"
	.section	.data.rel.ro.local
	.align 32
	.type	s_client_options, @object
	.size	s_client_options, 4080
s_client_options:
	.quad	.LC229
	.long	1
	.long	45
	.quad	.LC230
	.quad	.LC231
	.long	4
	.long	115
	.quad	.LC232
	.quad	.LC233
	.long	5
	.long	112
	.quad	.LC232
	.quad	.LC234
	.long	6
	.long	115
	.quad	.LC235
	.quad	.LC236
	.long	7
	.long	115
	.quad	.LC237
	.quad	.LC238
	.long	3034
	.long	115
	.quad	.LC239
	.quad	.LC240
	.long	8
	.long	115
	.quad	.LC241
	.quad	.LC242
	.long	2
	.long	45
	.quad	.LC243
	.quad	.LC244
	.long	3
	.long	45
	.quad	.LC245
	.quad	.LC246
	.long	10
	.long	112
	.quad	.LC247
	.quad	.LC248
	.long	12
	.long	60
	.quad	.LC249
	.quad	.LC250
	.long	17
	.long	70
	.quad	.LC251
	.quad	.LC252
	.long	11
	.long	115
	.quad	.LC253
	.quad	.LC254
	.long	69
	.long	115
	.quad	.LC255
	.quad	.LC256
	.long	62
	.long	69
	.quad	.LC257
	.quad	.LC258
	.long	63
	.long	115
	.quad	.LC259
	.quad	.LC260
	.long	65
	.long	47
	.quad	.LC261
	.quad	.LC262
	.long	72
	.long	60
	.quad	.LC263
	.quad	.LC264
	.long	73
	.long	45
	.quad	.LC265
	.quad	.LC266
	.long	66
	.long	45
	.quad	.LC267
	.quad	.LC268
	.long	94
	.long	60
	.quad	.LC269
	.quad	.LC270
	.long	3035
	.long	115
	.quad	.LC271
	.quad	.LC272
	.long	3039
	.long	115
	.quad	.LC273
	.quad	.LC274
	.long	3040
	.long	45
	.quad	.LC275
	.quad	.LC276
	.long	70
	.long	45
	.quad	.LC277
	.quad	.LC278
	.long	39
	.long	45
	.quad	.LC279
	.quad	.LC280
	.long	29
	.long	45
	.quad	.LC281
	.quad	.LC282
	.long	33
	.long	45
	.quad	.LC283
	.quad	.LC284
	.long	34
	.long	62
	.quad	.LC285
	.quad	.LC286
	.long	40
	.long	45
	.quad	.LC287
	.quad	.LC288
	.long	41
	.long	45
	.quad	.LC289
	.quad	.LC290
	.long	23
	.long	45
	.quad	.LC291
	.quad	.LC292
	.long	24
	.long	45
	.quad	.LC293
	.quad	.LC294
	.long	27
	.long	45
	.quad	.LC295
	.quad	.LC296
	.long	28
	.long	45
	.quad	.LC297
	.quad	.LC298
	.long	79
	.long	115
	.quad	.LC299
	.quad	.LC300
	.long	9
	.long	115
	.quad	.LC301
	.quad	.LC302
	.long	1501
	.long	115
	.quad	.LC303
	.quad	.LC304
	.long	1502
	.long	62
	.quad	.LC305
	.quad	.LC306
	.long	15
	.long	62
	.quad	.LC307
	.quad	.LC308
	.long	16
	.long	60
	.quad	.LC309
	.quad	.LC310
	.long	83
	.long	115
	.quad	.LC311
	.quad	.LC312
	.long	84
	.long	115
	.quad	.LC313
	.quad	.LC314
	.long	85
	.long	112
	.quad	.LC315
	.quad	.LC316
	.long	87
	.long	112
	.quad	.LC317
	.quad	.LC318
	.long	3032
	.long	45
	.quad	.LC319
	.quad	.LC320
	.long	86
	.long	115
	.quad	.LC321
	.quad	.LC322
	.long	13
	.long	60
	.quad	.LC323
	.quad	.LC324
	.long	14
	.long	45
	.quad	.LC325
	.quad	.LC326
	.long	18
	.long	70
	.quad	.LC327
	.quad	.LC328
	.long	19
	.long	45
	.quad	.LC329
	.quad	.LC330
	.long	20
	.long	45
	.quad	.LC331
	.quad	.LC332
	.long	21
	.long	45
	.quad	.LC333
	.quad	.LC334
	.long	22
	.long	45
	.quad	.LC335
	.quad	.LC336
	.long	37
	.long	45
	.quad	.LC337
	.quad	.LC338
	.long	38
	.long	45
	.quad	.LC339
	.quad	.LC340
	.long	64
	.long	60
	.quad	.LC341
	.quad	.LC342
	.long	67
	.long	47
	.quad	.LC343
	.quad	.LC344
	.long	68
	.long	47
	.quad	.LC345
	.quad	.LC346
	.long	71
	.long	45
	.quad	.LC347
	.quad	.LC348
	.long	74
	.long	60
	.quad	.LC349
	.quad	.LC350
	.long	75
	.long	60
	.quad	.LC351
	.quad	.LC352
	.long	3033
	.long	45
	.quad	.LC353
	.quad	.LC354
	.long	80
	.long	115
	.quad	.LC355
	.quad	.LC356
	.long	81
	.long	45
	.quad	.LC357
	.quad	.LC358
	.long	30
	.long	45
	.quad	.LC359
	.quad	.LC360
	.long	31
	.long	45
	.quad	.LC361
	.quad	.LC362
	.long	78
	.long	115
	.quad	.LC363
	.quad	.LC364
	.long	77
	.long	115
	.quad	.LC365
	.quad	.LC366
	.long	82
	.long	45
	.quad	.LC367
	.quad	.LC368
	.long	51
	.long	115
	.quad	.LC369
	.quad	.LC370
	.long	88
	.long	112
	.quad	.LC371
	.quad	.LC372
	.long	89
	.long	112
	.quad	.LC373
	.quad	.LC374
	.long	90
	.long	112
	.quad	.LC375
	.quad	.LC376
	.long	91
	.long	112
	.quad	.LC377
	.quad	.LC378
	.long	3001
	.long	45
	.quad	.LC379
	.quad	.LC380
	.long	3002
	.long	45
	.quad	.LC381
	.quad	.LC382
	.long	3003
	.long	45
	.quad	.LC383
	.quad	.LC384
	.long	3004
	.long	45
	.quad	.LC385
	.quad	.LC386
	.long	3005
	.long	45
	.quad	.LC387
	.quad	.LC388
	.long	3006
	.long	45
	.quad	.LC389
	.quad	.LC390
	.long	3007
	.long	45
	.quad	.LC391
	.quad	.LC392
	.long	3026
	.long	45
	.quad	.LC393
	.quad	.LC394
	.long	3008
	.long	45
	.quad	.LC395
	.quad	.LC396
	.long	3009
	.long	45
	.quad	.LC397
	.quad	.LC398
	.long	3010
	.long	45
	.quad	.LC399
	.quad	.LC400
	.long	3029
	.long	45
	.quad	.LC401
	.quad	.LC402
	.long	3011
	.long	45
	.quad	.LC403
	.quad	.LC404
	.long	3012
	.long	45
	.quad	.LC405
	.quad	.LC406
	.long	3013
	.long	45
	.quad	.LC407
	.quad	.LC408
	.long	3014
	.long	45
	.quad	.LC409
	.quad	.LC410
	.long	3015
	.long	45
	.quad	.LC411
	.quad	.LC412
	.long	3016
	.long	45
	.quad	.LC413
	.quad	.LC414
	.long	3017
	.long	115
	.quad	.LC415
	.quad	.LC416
	.long	3018
	.long	115
	.quad	.LC417
	.quad	.LC418
	.long	3019
	.long	115
	.quad	.LC419
	.quad	.LC420
	.long	3020
	.long	115
	.quad	.LC419
	.quad	.LC421
	.long	3021
	.long	115
	.quad	.LC422
	.quad	.LC423
	.long	3022
	.long	115
	.quad	.LC424
	.quad	.LC425
	.long	3023
	.long	115
	.quad	.LC426
	.quad	.LC427
	.long	3027
	.long	115
	.quad	.LC428
	.quad	.LC429
	.long	3028
	.long	115
	.quad	.LC430
	.quad	.LC431
	.long	3024
	.long	115
	.quad	.LC432
	.quad	.LC433
	.long	3025
	.long	45
	.quad	.LC434
	.quad	.LC435
	.long	3030
	.long	45
	.quad	.LC436
	.quad	.LC437
	.long	2001
	.long	115
	.quad	.LC438
	.quad	.LC439
	.long	2002
	.long	115
	.quad	.LC440
	.quad	.LC441
	.long	2003
	.long	115
	.quad	.LC442
	.quad	.LC443
	.long	2004
	.long	110
	.quad	.LC444
	.quad	.LC445
	.long	2029
	.long	110
	.quad	.LC446
	.quad	.LC447
	.long	2005
	.long	77
	.quad	.LC448
	.quad	.LC449
	.long	2006
	.long	115
	.quad	.LC450
	.quad	.LC451
	.long	2007
	.long	115
	.quad	.LC452
	.quad	.LC453
	.long	2008
	.long	115
	.quad	.LC454
	.quad	.LC455
	.long	2009
	.long	45
	.quad	.LC456
	.quad	.LC457
	.long	2010
	.long	45
	.quad	.LC458
	.quad	.LC459
	.long	2011
	.long	45
	.quad	.LC460
	.quad	.LC461
	.long	2012
	.long	45
	.quad	.LC462
	.quad	.LC463
	.long	2013
	.long	45
	.quad	.LC464
	.quad	.LC465
	.long	2014
	.long	45
	.quad	.LC466
	.quad	.LC467
	.long	2015
	.long	45
	.quad	.LC468
	.quad	.LC469
	.long	2016
	.long	45
	.quad	.LC470
	.quad	.LC471
	.long	2017
	.long	45
	.quad	.LC472
	.quad	.LC473
	.long	2018
	.long	45
	.quad	.LC474
	.quad	.LC475
	.long	2019
	.long	45
	.quad	.LC476
	.quad	.LC477
	.long	2020
	.long	45
	.quad	.LC478
	.quad	.LC479
	.long	2021
	.long	45
	.quad	.LC480
	.quad	.LC481
	.long	2022
	.long	45
	.quad	.LC482
	.quad	.LC483
	.long	2023
	.long	45
	.quad	.LC484
	.quad	.LC485
	.long	2024
	.long	45
	.quad	.LC486
	.quad	.LC487
	.long	2025
	.long	45
	.quad	.LC488
	.quad	.LC489
	.long	2026
	.long	45
	.quad	.LC490
	.quad	.LC491
	.long	2027
	.long	45
	.quad	.LC458
	.quad	.LC492
	.long	2028
	.long	45
	.quad	.LC493
	.quad	.LC494
	.long	2030
	.long	45
	.quad	.LC495
	.quad	.LC496
	.long	1001
	.long	60
	.quad	.LC497
	.quad	.LC498
	.long	1002
	.long	60
	.quad	.LC499
	.quad	.LC500
	.long	1003
	.long	60
	.quad	.LC501
	.quad	.LC502
	.long	1004
	.long	45
	.quad	.LC503
	.quad	.LC504
	.long	1005
	.long	70
	.quad	.LC505
	.quad	.LC506
	.long	1006
	.long	70
	.quad	.LC507
	.quad	.LC508
	.long	55
	.long	45
	.quad	.LC509
	.quad	.LC510
	.long	54
	.long	45
	.quad	.LC511
	.quad	.LC512
	.long	53
	.long	45
	.quad	.LC513
	.quad	.LC514
	.long	52
	.long	45
	.quad	.LC515
	.quad	.LC516
	.long	56
	.long	45
	.quad	.LC517
	.quad	.LC518
	.long	60
	.long	45
	.quad	.LC519
	.quad	.LC520
	.long	61
	.long	112
	.quad	.LC521
	.quad	.LC522
	.long	57
	.long	45
	.quad	.LC523
	.quad	.LC524
	.long	58
	.long	45
	.quad	.LC525
	.quad	.LC526
	.long	36
	.long	45
	.quad	.LC527
	.quad	.LC528
	.long	25
	.long	45
	.quad	.LC529
	.quad	.LC530
	.long	42
	.long	115
	.quad	.LC531
	.quad	.LC532
	.long	43
	.long	115
	.quad	.LC533
	.quad	.LC534
	.long	44
	.long	60
	.quad	.LC535
	.quad	.LC536
	.long	45
	.long	115
	.quad	.LC537
	.quad	.LC538
	.long	46
	.long	115
	.quad	.LC539
	.quad	.LC540
	.long	48
	.long	45
	.quad	.LC541
	.quad	.LC542
	.long	49
	.long	45
	.quad	.LC543
	.quad	.LC544
	.long	47
	.long	112
	.quad	.LC545
	.quad	.LC546
	.long	76
	.long	115
	.quad	.LC547
	.quad	.LC548
	.long	35
	.long	115
	.quad	.LC549
	.quad	.LC550
	.long	26
	.long	115
	.quad	.LC551
	.quad	.LC552
	.long	3036
	.long	45
	.quad	.LC553
	.quad	.LC554
	.long	3037
	.long	45
	.quad	.LC555
	.quad	.LC556
	.long	3038
	.long	60
	.quad	.LC557
	.quad	.LC558
	.long	92
	.long	62
	.quad	.LC559
	.quad	.LC560
	.long	93
	.long	60
	.quad	.LC561
	.quad	.LC562
	.long	3041
	.long	45
	.quad	.LC563
	.quad	0
	.long	0
	.long	0
	.quad	0
	.local	next_proto
	.comm	next_proto,24,16
	.globl	tls13_aes256gcmsha384_id
	.section	.rodata
	.type	tls13_aes256gcmsha384_id, @object
	.size	tls13_aes256gcmsha384_id, 2
tls13_aes256gcmsha384_id:
	.ascii	"\023\002"
	.globl	tls13_aes128gcmsha256_id
	.type	tls13_aes128gcmsha256_id, @object
	.size	tls13_aes128gcmsha256_id, 2
tls13_aes128gcmsha256_id:
	.ascii	"\023\001"
	.section	.rodata.str1.1
.LC564:
	.string	"Client_identity"
	.section	.data.rel.local
	.align 8
	.type	psk_identity, @object
	.size	psk_identity, 8
psk_identity:
	.quad	.LC564
	.local	saved_errno
	.comm	saved_errno,4,4
	.local	psksess
	.comm	psksess,8,8
	.local	sess_out
	.comm	sess_out,8,8
	.local	c_quiet
	.comm	c_quiet,4,4
	.local	bio_c_out
	.comm	bio_c_out,8,8
	.data
	.align 4
	.type	keymatexportlen, @object
	.size	keymatexportlen, 4
keymatexportlen:
	.long	20
	.local	keymatexportlabel
	.comm	keymatexportlabel,8,8
	.local	c_showcerts
	.comm	c_showcerts,4,4
	.local	c_debug
	.comm	c_debug,4,4
	.local	prog
	.comm	prog,8,8
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC70:
	.long	0
	.long	0
	.long	0
	.long	1024
	.align 16
.LC136:
	.quad	0
	.quad	250000
	.align 16
.LC158:
	.quad	8
	.quad	0
	.align 16
.LC209:
	.quad	0
	.quad	500000
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
