	.file	"pkcs7.c"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"%s: Use -help for summary.\n"
.LC1:
	.string	"unable to load PKCS7 object\n"
.LC2:
	.string	"\n"
.LC3:
	.string	"unable to write pkcs7 object\n"
	.text
	.p2align 4
	.globl	pkcs7_main
	.type	pkcs7_main, @function
pkcs7_main:
.LFB1435:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	pkcs7_options(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	xorl	%r14d, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	.L5(%rip), %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$32773, -64(%rbp)
	movl	$32773, -60(%rbp)
	call	opt_init@PLT
	movl	$0, -96(%rbp)
	movl	$0, -80(%rbp)
	movq	%rax, %r13
	movq	$0, -88(%rbp)
	movq	$0, -72(%rbp)
.L2:
	call	opt_next@PLT
	testl	%eax, %eax
	je	.L69
.L18:
	addl	$1, %eax
	cmpl	$11, %eax
	ja	.L2
	movslq	(%rbx,%rax,4), %rax
	addq	%rbx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L5:
	.long	.L15-.L5
	.long	.L2-.L5
	.long	.L14-.L5
	.long	.L13-.L5
	.long	.L12-.L5
	.long	.L11-.L5
	.long	.L10-.L5
	.long	.L9-.L5
	.long	.L8-.L5
	.long	.L7-.L5
	.long	.L6-.L5
	.long	.L4-.L5
	.text
.L9:
	movl	$1, %r12d
	call	opt_next@PLT
	testl	%eax, %eax
	jne	.L18
.L69:
	call	opt_num_rest@PLT
	testl	%eax, %eax
	jne	.L15
	movl	-64(%rbp), %edx
	movq	-72(%rbp), %rdi
	movl	$114, %esi
	call	bio_open_default@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L44
	cmpl	$4, -64(%rbp)
	je	.L70
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	PEM_read_bio_PKCS7@PLT
	movq	%rax, %r9
.L20:
	movq	%r9, -72(%rbp)
	testq	%r9, %r9
	je	.L71
	movl	-60(%rbp), %edx
	movq	-88(%rbp), %rdi
	movl	$119, %esi
	call	bio_open_default@PLT
	movq	-72(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %r13
	je	.L45
	movl	-96(%rbp), %edx
	testl	%edx, %edx
	jne	.L72
.L22:
	testl	%r14d, %r14d
	jne	.L73
	testl	%r12d, %r12d
	jne	.L16
	cmpl	$4, -60(%rbp)
	movq	%r9, -72(%rbp)
	movq	%r9, %rsi
	movq	%r13, %rdi
	je	.L74
	call	PEM_write_bio_PKCS7@PLT
	movq	-72(%rbp), %r9
.L41:
	xorl	%r14d, %r14d
	testl	%eax, %eax
	jne	.L16
	movq	bio_err(%rip), %rdi
	movq	%r9, -72(%rbp)
	leaq	.LC3(%rip), %rsi
	movl	$1, %r14d
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	movq	-72(%rbp), %r9
	jmp	.L16
.L13:
	call	opt_arg@PLT
	leaq	-64(%rbp), %rdx
	movl	$2, %esi
	movq	%rax, %rdi
	call	opt_format@PLT
	testl	%eax, %eax
	jne	.L2
.L15:
	movq	bio_err(%rip), %rdi
	movq	%r13, %rdx
	xorl	%eax, %eax
	xorl	%r13d, %r13d
	leaq	.LC0(%rip), %rsi
	movl	$1, %r14d
	xorl	%ebx, %ebx
	call	BIO_printf@PLT
	xorl	%r9d, %r9d
.L16:
	movq	%r9, %rdi
	call	PKCS7_free@PLT
	movq	%r15, %rdi
	call	release_engine@PLT
	movq	%rbx, %rdi
	call	BIO_free@PLT
	movq	%r13, %rdi
	call	BIO_free_all@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L75
	addq	$56, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L4:
	.cfi_restore_state
	call	opt_arg@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	setup_engine@PLT
	movq	%rax, %r15
	jmp	.L2
.L6:
	movl	$1, %r14d
	jmp	.L2
.L7:
	movl	$1, -96(%rbp)
	jmp	.L2
.L8:
	movl	$1, -80(%rbp)
	jmp	.L2
.L10:
	call	opt_arg@PLT
	movq	%rax, -88(%rbp)
	jmp	.L2
.L11:
	call	opt_arg@PLT
	movq	%rax, -72(%rbp)
	jmp	.L2
.L12:
	call	opt_arg@PLT
	leaq	-60(%rbp), %rdx
	movl	$2, %esi
	movq	%rax, %rdi
	call	opt_format@PLT
	testl	%eax, %eax
	jne	.L2
	jmp	.L15
.L14:
	leaq	pkcs7_options(%rip), %rdi
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	xorl	%ebx, %ebx
	call	opt_help@PLT
	xorl	%r9d, %r9d
	jmp	.L16
.L44:
	xorl	%r13d, %r13d
	xorl	%r9d, %r9d
	movl	$1, %r14d
	jmp	.L16
.L70:
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	d2i_PKCS7_bio@PLT
	movq	%rax, %r9
	jmp	.L20
.L72:
	movq	%r9, %rsi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rdi
	call	PKCS7_print_ctx@PLT
	movq	-72(%rbp), %r9
	jmp	.L22
.L71:
	movq	bio_err(%rip), %rdi
	leaq	.LC1(%rip), %rsi
	xorl	%eax, %eax
	xorl	%r13d, %r13d
	movl	$1, %r14d
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	movq	-72(%rbp), %r9
	jmp	.L16
.L45:
	movl	$1, %r14d
	jmp	.L16
.L73:
	movq	24(%r9), %rdi
	movq	%r9, -72(%rbp)
	call	OBJ_obj2nid@PLT
	movq	-72(%rbp), %r9
	cmpl	$22, %eax
	je	.L25
	cmpl	$24, %eax
	jne	.L51
.L25:
	movq	32(%r9), %rax
	xorl	%r14d, %r14d
	testq	%rax, %rax
	je	.L16
	movq	16(%rax), %r14
	movq	24(%rax), %rax
	movq	%rax, -72(%rbp)
	testq	%r14, %r14
	je	.L27
	testl	%r12d, %r12d
	je	.L76
	movl	-80(%rbp), %eax
	testl	%eax, %eax
	jne	.L49
	leaq	.LC2(%rip), %r12
	jmp	.L34
.L77:
	movl	%ecx, %esi
	movq	%r14, %rdi
	call	OPENSSL_sk_value@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	dump_cert_text@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	BIO_puts@PLT
	addl	$1, -80(%rbp)
	movq	-88(%rbp), %r9
.L34:
	movq	%r14, %rdi
	movq	%r9, -88(%rbp)
	call	OPENSSL_sk_num@PLT
	movl	-80(%rbp), %ecx
	movq	-88(%rbp), %r9
	cmpl	%eax, %ecx
	jl	.L77
.L33:
	cmpq	$0, -72(%rbp)
	je	.L51
.L42:
	xorl	%r14d, %r14d
	leaq	.LC2(%rip), %r12
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L78:
	movq	-72(%rbp), %rdi
	movl	%r14d, %esi
	movq	%r9, -88(%rbp)
	addl	$1, %r14d
	call	OPENSSL_sk_value@PLT
	movq	%rax, -80(%rbp)
	call	get_nameopt@PLT
	movq	-80(%rbp), %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	X509_CRL_print_ex@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	BIO_puts@PLT
	movq	-88(%rbp), %r9
.L36:
	movq	-72(%rbp), %rdi
	movq	%r9, -80(%rbp)
	call	OPENSSL_sk_num@PLT
	movq	-80(%rbp), %r9
	cmpl	%eax, %r14d
	jl	.L78
.L51:
	xorl	%r14d, %r14d
	jmp	.L16
.L74:
	call	i2d_PKCS7_bio@PLT
	movq	-72(%rbp), %r9
	jmp	.L41
.L27:
	cmpq	$0, -72(%rbp)
	je	.L51
	testl	%r12d, %r12d
	jne	.L42
.L35:
	xorl	%r12d, %r12d
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L39:
	movq	-72(%rbp), %rdi
	movl	%r12d, %esi
	movq	%r9, -80(%rbp)
	addl	$1, %r12d
	call	OPENSSL_sk_value@PLT
	movq	%rax, %r14
	call	get_nameopt@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	X509_CRL_print_ex@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	PEM_write_bio_X509_CRL@PLT
	leaq	.LC2(%rip), %rsi
	movq	%r13, %rdi
	call	BIO_puts@PLT
	movq	-80(%rbp), %r9
.L38:
	movq	-72(%rbp), %rdi
	movq	%r9, -80(%rbp)
	call	OPENSSL_sk_num@PLT
	movq	-80(%rbp), %r9
	cmpl	%eax, %r12d
	jl	.L39
	jmp	.L51
.L75:
	call	__stack_chk_fail@PLT
.L49:
	xorl	%r12d, %r12d
	jmp	.L32
.L79:
	movl	%r12d, %esi
	movq	%r14, %rdi
	addl	$1, %r12d
	call	OPENSSL_sk_value@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	X509_print@PLT
	leaq	.LC2(%rip), %rsi
	movq	%r13, %rdi
	call	BIO_puts@PLT
	movq	-80(%rbp), %r9
.L32:
	movq	%r14, %rdi
	movq	%r9, -80(%rbp)
	call	OPENSSL_sk_num@PLT
	movq	-80(%rbp), %r9
	cmpl	%r12d, %eax
	jg	.L79
	jmp	.L33
.L76:
	cmpl	$0, -80(%rbp)
	jne	.L48
	leaq	.LC2(%rip), %r12
	jmp	.L31
.L80:
	movl	%ecx, %esi
	movq	%r14, %rdi
	movq	%r9, -96(%rbp)
	call	OPENSSL_sk_value@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	movq	%rax, -88(%rbp)
	call	dump_cert_text@PLT
	movq	-88(%rbp), %rsi
	movq	%r13, %rdi
	call	PEM_write_bio_X509@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	BIO_puts@PLT
	addl	$1, -80(%rbp)
	movq	-96(%rbp), %r9
.L31:
	movq	%r14, %rdi
	movq	%r9, -88(%rbp)
	call	OPENSSL_sk_num@PLT
	movl	-80(%rbp), %ecx
	movq	-88(%rbp), %r9
	cmpl	%eax, %ecx
	jl	.L80
.L30:
	cmpq	$0, -72(%rbp)
	jne	.L35
	jmp	.L51
.L48:
	xorl	%r12d, %r12d
	jmp	.L29
.L37:
	movl	%r12d, %esi
	movq	%r14, %rdi
	movq	%r9, -88(%rbp)
	addl	$1, %r12d
	call	OPENSSL_sk_value@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	movq	%rax, -80(%rbp)
	call	X509_print@PLT
	movq	-80(%rbp), %rsi
	movq	%r13, %rdi
	call	PEM_write_bio_X509@PLT
	leaq	.LC2(%rip), %rsi
	movq	%r13, %rdi
	call	BIO_puts@PLT
	movq	-88(%rbp), %r9
.L29:
	movq	%r14, %rdi
	movq	%r9, -80(%rbp)
	call	OPENSSL_sk_num@PLT
	movq	-80(%rbp), %r9
	cmpl	%eax, %r12d
	jl	.L37
	jmp	.L30
	.cfi_endproc
.LFE1435:
	.size	pkcs7_main, .-pkcs7_main
	.globl	pkcs7_options
	.section	.rodata.str1.1
.LC4:
	.string	"help"
.LC5:
	.string	"Display this summary"
.LC6:
	.string	"inform"
.LC7:
	.string	"Input format - DER or PEM"
.LC8:
	.string	"in"
.LC9:
	.string	"Input file"
.LC10:
	.string	"outform"
.LC11:
	.string	"Output format - DER or PEM"
.LC12:
	.string	"out"
.LC13:
	.string	"Output file"
.LC14:
	.string	"noout"
.LC15:
	.string	"Don't output encoded data"
.LC16:
	.string	"text"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC17:
	.string	"Print full details of certificates"
	.section	.rodata.str1.1
.LC18:
	.string	"print"
	.section	.rodata.str1.8
	.align 8
.LC19:
	.string	"Print out all fields of the PKCS7 structure"
	.section	.rodata.str1.1
.LC20:
	.string	"print_certs"
	.section	.rodata.str1.8
	.align 8
.LC21:
	.string	"Print_certs  print any certs or crl in the input"
	.section	.rodata.str1.1
.LC22:
	.string	"engine"
	.section	.rodata.str1.8
	.align 8
.LC23:
	.string	"Use engine, possibly a hardware device"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	pkcs7_options, @object
	.size	pkcs7_options, 264
pkcs7_options:
	.quad	.LC4
	.long	1
	.long	45
	.quad	.LC5
	.quad	.LC6
	.long	2
	.long	70
	.quad	.LC7
	.quad	.LC8
	.long	4
	.long	60
	.quad	.LC9
	.quad	.LC10
	.long	3
	.long	70
	.quad	.LC11
	.quad	.LC12
	.long	5
	.long	62
	.quad	.LC13
	.quad	.LC14
	.long	6
	.long	45
	.quad	.LC15
	.quad	.LC16
	.long	7
	.long	45
	.quad	.LC17
	.quad	.LC18
	.long	8
	.long	45
	.quad	.LC19
	.quad	.LC20
	.long	9
	.long	45
	.quad	.LC21
	.quad	.LC22
	.long	10
	.long	115
	.quad	.LC23
	.quad	0
	.zero	16
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
