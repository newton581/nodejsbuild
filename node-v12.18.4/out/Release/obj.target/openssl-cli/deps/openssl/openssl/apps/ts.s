	.file	"ts.c"
	.text
	.p2align 4
	.type	verify_cb, @function
verify_cb:
.LFB1523:
	.cfi_startproc
	endbr64
	movl	%edi, %eax
	ret
	.cfi_endproc
.LFE1523:
	.size	verify_cb, .-verify_cb
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"digest buffer"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"../deps/openssl/openssl/apps/ts.c"
	.align 8
.LC2:
	.string	"bad digest, %d bytes must be specified\n"
	.text
	.p2align 4
	.type	create_digest, @function
create_digest:
.LFB1512:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%rdx, %rdi
	movq	%rsi, %r12
	movq	%rdx, %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rcx, %r15
	call	EVP_MD_size@PLT
	testl	%eax, %eax
	js	.L14
	movl	%eax, %r13d
	testq	%rbx, %rbx
	je	.L5
	call	EVP_MD_CTX_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L14
	movl	%r13d, %edi
	leaq	.LC0(%rip), %rsi
	leaq	-4160(%rbp), %r13
	call	app_malloc@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, (%r15)
	call	EVP_DigestInit@PLT
	testl	%eax, %eax
	jne	.L8
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L9:
	movslq	%eax, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	je	.L7
.L8:
	movl	$4096, %edx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	BIO_read@PLT
	testl	%eax, %eax
	jg	.L9
	movq	(%r15), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	EVP_DigestFinal@PLT
	testl	%eax, %eax
	je	.L7
	movq	%r14, %rdi
	call	EVP_MD_size@PLT
	movl	%eax, %r13d
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L14:
	xorl	%r13d, %r13d
.L3:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L28
	addq	$4136, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	.cfi_restore_state
	xorl	%r13d, %r13d
.L10:
	movq	%r12, %rdi
	call	EVP_MD_CTX_free@PLT
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L5:
	movq	%r12, %rdi
	leaq	-4168(%rbp), %rsi
	call	OPENSSL_hexstr2buf@PLT
	movq	%rax, (%r15)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L11
	movslq	%r13d, %rax
	cmpq	-4168(%rbp), %rax
	jne	.L11
	xorl	%r12d, %r12d
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L11:
	movl	$510, %edx
	leaq	.LC1(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, (%r15)
	movl	%r13d, %edx
	xorl	%eax, %eax
	movq	bio_err(%rip), %rdi
	leaq	.LC2(%rip), %rsi
	xorl	%r13d, %r13d
	call	BIO_printf@PLT
	jmp	.L3
.L28:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1512:
	.size	create_digest, .-create_digest
	.section	.rodata.str1.1
.LC3:
	.string	"r"
	.section	.rodata.str1.8
	.align 8
.LC4:
	.string	"Warning: could not open file %s for reading, using serial number: 1\n"
	.align 8
.LC5:
	.string	"unable to load number from %s\n"
	.text
	.p2align 4
	.type	next_serial, @function
next_serial:
.LFB1518:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	subq	$1048, %rsp
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	ASN1_INTEGER_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L36
	leaq	.LC3(%rip), %rsi
	movq	%r14, %rdi
	call	BIO_new_file@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L47
	leaq	-1072(%rbp), %rdx
	movl	$1024, %ecx
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	a2i_ASN1_INTEGER@PLT
	testl	%eax, %eax
	je	.L48
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	ASN1_INTEGER_to_BN@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L30
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	ASN1_INTEGER_free@PLT
	movl	$1, %esi
	movq	%r14, %rdi
	call	BN_add_word@PLT
	testl	%eax, %eax
	je	.L30
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	BN_to_ASN1_INTEGER@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L30
.L32:
	movq	%r13, %rdi
	call	BIO_free_all@PLT
	movq	%r14, %rdi
	call	BN_free@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L49
	addq	$1048, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L47:
	.cfi_restore_state
	call	ERR_clear_error@PLT
	movq	bio_err(%rip), %rdi
	movq	%r14, %rdx
	xorl	%eax, %eax
	leaq	.LC4(%rip), %rsi
	xorl	%r14d, %r14d
	call	BIO_printf@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	call	ASN1_INTEGER_set@PLT
	testl	%eax, %eax
	jne	.L32
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L36:
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
.L30:
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	ASN1_INTEGER_free@PLT
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L48:
	movq	bio_err(%rip), %rdi
	movq	%r14, %rdx
	leaq	.LC5(%rip), %rsi
	xorl	%r14d, %r14d
	call	BIO_printf@PLT
	jmp	.L30
.L49:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1518:
	.size	next_serial, .-next_serial
	.section	.rodata.str1.8
	.align 8
.LC6:
	.string	"Error during serial number generation."
	.section	.rodata.str1.1
.LC7:
	.string	"w"
	.section	.rodata.str1.8
	.align 8
.LC8:
	.string	"could not save serial number to %s\n"
	.section	.rodata.str1.1
.LC9:
	.string	"\n"
	.text
	.p2align 4
	.type	serial_cb, @function
serial_cb:
.LFB1517:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	movq	%rsi, %rdi
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -40
	call	next_serial
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L57
	leaq	.LC7(%rip), %rsi
	movq	%r13, %rdi
	call	BIO_new_file@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L55
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	i2a_ASN1_INTEGER@PLT
	testl	%eax, %eax
	jle	.L55
	leaq	.LC9(%rip), %rsi
	movq	%r14, %rdi
	call	BIO_puts@PLT
	testl	%eax, %eax
	jle	.L55
.L54:
	movq	%r14, %rdi
	call	BIO_free_all@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L55:
	.cfi_restore_state
	movq	bio_err(%rip), %rdi
	movq	%r13, %rdx
	leaq	.LC8(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L57:
	movq	%r14, %rdi
	leaq	.LC6(%rip), %rdx
	movl	$2, %esi
	call	TS_RESP_CTX_set_status_info@PLT
	movq	%r14, %rdi
	movl	$17, %esi
	call	TS_RESP_CTX_add_failure_info@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1517:
	.size	serial_cb, .-serial_cb
	.section	.rodata.str1.1
.LC10:
	.string	"%s: Use -help for summary.\n"
.LC11:
	.string	"%s\n"
.LC12:
	.string	"Error getting password.\n"
.LC13:
	.string	"Using configuration from %s\n"
.LC14:
	.string	"oid_file"
.LC15:
	.string	"sha1"
.LC16:
	.string	"cannot convert %s to OID\n"
.LC17:
	.string	"nonce buffer"
.LC18:
	.string	"could not create nonce\n"
.LC19:
	.string	"could not create query\n"
.LC20:
	.string	"rb"
.LC21:
	.string	"Verification: "
.LC22:
	.string	"invalid digest string\n"
.LC23:
	.string	"memory allocation failure\n"
.LC24:
	.string	"Error loading directory %s\n"
.LC25:
	.string	"Error loading file %s\n"
.LC26:
	.string	"OK"
.LC27:
	.string	"FAILED"
.LC28:
	.string	"Response is not generated.\n"
.LC29:
	.string	"Response has been generated.\n"
	.text
	.p2align 4
	.globl	ts_main
	.type	ts_main, @function
ts_main:
.LFB1507:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%edi, %r12d
	pushq	%rbx
	subq	$232, %rsp
	.cfi_offset 3, -56
	movq	default_config_file(%rip), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -104(%rbp)
	movq	$0, -96(%rbp)
	call	X509_VERIFY_PARAM_new@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L371
	movq	%r15, %rsi
	movl	%r12d, %edi
	xorl	%r15d, %r15d
	xorl	%r12d, %r12d
	leaq	ts_options(%rip), %rdx
	leaq	.L65(%rip), %rbx
	call	opt_init@PLT
	movl	$-1, -120(%rbp)
	movq	%rax, -128(%rbp)
	movl	$0, -212(%rbp)
	movl	$0, -180(%rbp)
	movl	$0, -152(%rbp)
	movl	$0, -184(%rbp)
	movl	$0, -216(%rbp)
	movq	$0, -208(%rbp)
	movq	$0, -264(%rbp)
	movq	$0, -256(%rbp)
	movq	$0, -248(%rbp)
	movq	$0, -168(%rbp)
	movq	$0, -176(%rbp)
	movq	$0, -144(%rbp)
	movq	$0, -200(%rbp)
	movq	$0, -160(%rbp)
	movq	$0, -136(%rbp)
	movq	$0, -240(%rbp)
	movq	$0, -232(%rbp)
	movq	$0, -224(%rbp)
	movq	$0, -192(%rbp)
.L61:
	call	opt_next@PLT
	testl	%eax, %eax
	je	.L372
.L97:
	cmpl	$26, %eax
	jg	.L62
	cmpl	$-1, %eax
	jl	.L61
	leal	1(%rax), %edx
	cmpl	$27, %edx
	ja	.L61
	movslq	(%rbx,%rdx,4), %rdx
	addq	%rbx, %rdx
	notrack jmp	*%rdx
	.section	.rodata
	.align 4
	.align 4
.L65:
	.long	.L174-.L65
	.long	.L61-.L65
	.long	.L88-.L65
	.long	.L87-.L65
	.long	.L86-.L65
	.long	.L85-.L65
	.long	.L69-.L65
	.long	.L84-.L65
	.long	.L83-.L65
	.long	.L82-.L65
	.long	.L81-.L65
	.long	.L80-.L65
	.long	.L79-.L65
	.long	.L78-.L65
	.long	.L77-.L65
	.long	.L76-.L65
	.long	.L75-.L65
	.long	.L69-.L65
	.long	.L74-.L65
	.long	.L73-.L65
	.long	.L72-.L65
	.long	.L71-.L65
	.long	.L70-.L65
	.long	.L69-.L65
	.long	.L68-.L65
	.long	.L67-.L65
	.long	.L66-.L65
	.long	.L64-.L65
	.text
.L81:
	movl	$1, %r12d
	call	opt_next@PLT
	testl	%eax, %eax
	jne	.L97
.L372:
	cmpl	$-1, -120(%rbp)
	je	.L174
	call	opt_num_rest@PLT
	movl	%eax, %ebx
	testl	%eax, %eax
	jne	.L174
	cmpl	$16, -120(%rbp)
	jne	.L98
	testq	%r15, %r15
	je	.L98
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	leaq	-104(%rbp), %rdx
	movq	%r15, %rdi
	call	app_passwd@PLT
	testl	%eax, %eax
	jne	.L98
	movq	bio_err(%rip), %rdi
	leaq	.LC12(%rip), %rsi
	call	BIO_printf@PLT
	.p2align 4,,10
	.p2align 3
.L371:
	movl	$1, %ebx
	xorl	%r15d, %r15d
.L60:
	movq	%r13, %rdi
	call	X509_VERIFY_PARAM_free@PLT
	movq	%r15, %rdi
	call	NCONF_free@PLT
	movq	-104(%rbp), %rdi
	movl	$321, %edx
	leaq	.LC1(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L373
	addq	$232, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L64:
	.cfi_restore_state
	call	opt_unknown@PLT
	leaq	-96(%rbp), %rsi
	movq	%rax, %rdi
	call	opt_md@PLT
	testl	%eax, %eax
	jne	.L61
.L174:
	xorl	%r15d, %r15d
.L89:
	movq	-128(%rbp), %rdx
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
	movl	$1, %ebx
	leaq	.LC10(%rip), %rsi
	call	BIO_printf@PLT
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L62:
	cmpl	$1502, %eax
	jg	.L90
	cmpl	$1500, %eax
	jle	.L61
	movl	%eax, %edi
	call	opt_rand@PLT
	testl	%eax, %eax
	jne	.L61
	jmp	.L371
	.p2align 4,,10
	.p2align 3
.L90:
	leal	-2001(%rax), %edx
	cmpl	$29, %edx
	ja	.L61
	movq	%r13, %rsi
	movl	%eax, %edi
	call	opt_verify@PLT
	testl	%eax, %eax
	je	.L371
	addl	$1, -152(%rbp)
	jmp	.L61
.L66:
	call	opt_arg@PLT
	movq	%rax, -224(%rbp)
	jmp	.L61
.L67:
	call	opt_arg@PLT
	movq	%rax, -192(%rbp)
	jmp	.L61
.L68:
	call	opt_arg@PLT
	movq	%rax, -208(%rbp)
	jmp	.L61
.L70:
	call	opt_arg@PLT
	movq	%rax, -264(%rbp)
	jmp	.L61
.L71:
	call	opt_arg@PLT
	movq	%rax, -256(%rbp)
	jmp	.L61
.L72:
	call	opt_arg@PLT
	movq	%rax, -248(%rbp)
	jmp	.L61
.L73:
	call	opt_arg@PLT
	movq	%rax, %r15
	jmp	.L61
.L74:
	call	opt_arg@PLT
	movq	%rax, -168(%rbp)
	jmp	.L61
.L75:
	movl	$1, -184(%rbp)
	jmp	.L61
.L76:
	movl	$1, -212(%rbp)
	jmp	.L61
.L77:
	call	opt_arg@PLT
	movq	%rax, -176(%rbp)
	jmp	.L61
.L78:
	movl	$1, -180(%rbp)
	jmp	.L61
.L79:
	call	opt_arg@PLT
	movq	%rax, -144(%rbp)
	jmp	.L61
.L80:
	movl	$1, -216(%rbp)
	jmp	.L61
.L82:
	call	opt_arg@PLT
	movq	%rax, -200(%rbp)
	jmp	.L61
.L83:
	call	opt_arg@PLT
	movq	%rax, -160(%rbp)
	jmp	.L61
.L84:
	call	opt_arg@PLT
	movq	%rax, -136(%rbp)
	jmp	.L61
.L69:
	cmpl	$-1, -120(%rbp)
	jne	.L174
	movl	%eax, -120(%rbp)
	jmp	.L61
.L85:
	call	opt_arg@PLT
	movq	%rax, -240(%rbp)
	jmp	.L61
.L86:
	call	opt_arg@PLT
	movq	%rax, %r14
	jmp	.L61
.L87:
	call	opt_arg@PLT
	movq	%rax, -232(%rbp)
	jmp	.L61
.L88:
	leaq	ts_options(%rip), %rdi
	leaq	opt_helplist(%rip), %rbx
	call	opt_help@PLT
	movq	opt_helplist(%rip), %rdx
	leaq	.LC11(%rip), %r12
	testq	%rdx, %rdx
	je	.L94
	.p2align 4,,10
	.p2align 3
.L93:
	movq	bio_err(%rip), %rdi
	movq	%r12, %rsi
	xorl	%eax, %eax
	addq	$8, %rbx
	call	BIO_printf@PLT
	movq	(%rbx), %rdx
	testq	%rdx, %rdx
	jne	.L93
.L94:
	xorl	%ebx, %ebx
	xorl	%r15d, %r15d
	jmp	.L60
.L98:
	movq	%r14, %rdi
	call	app_load_config@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L371
	movq	bio_err(%rip), %rdi
	movq	%r14, %rdx
	leaq	.LC13(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	%r15, %rdi
	leaq	.LC14(%rip), %rdx
	xorl	%esi, %esi
	call	NCONF_get_string@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L99
	leaq	.LC3(%rip), %rsi
	call	BIO_new_file@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L374
	movq	%rax, -272(%rbp)
	call	OBJ_create_objects@PLT
	movq	-272(%rbp), %rdi
	call	BIO_free_all@PLT
.L101:
	movq	%r15, %rdi
	call	add_oid_section@PLT
	testl	%eax, %eax
	je	.L375
.L102:
	cmpq	%r14, default_config_file(%rip)
	je	.L172
	movq	%r15, %rdi
	call	app_load_modules@PLT
	testl	%eax, %eax
	je	.L177
.L172:
	cmpl	$5, -120(%rbp)
	je	.L376
	cmpl	$16, -120(%rbp)
	je	.L377
	cmpq	$0, -144(%rbp)
	je	.L89
	cmpl	$22, -120(%rbp)
	jne	.L89
	cmpq	$0, -168(%rbp)
	je	.L207
	cmpq	$0, -136(%rbp)
	jne	.L360
.L140:
	cmpq	$0, -160(%rbp)
	jne	.L89
.L143:
	movl	-152(%rbp), %ecx
	movl	$0, %eax
	movq	-144(%rbp), %rdi
	leaq	.LC20(%rip), %rsi
	testl	%ecx, %ecx
	cmovne	%r13, %rax
	movq	%rax, -176(%rbp)
	call	BIO_new_file@PLT
	movq	%rax, -152(%rbp)
	testq	%rax, %rax
	je	.L148
	movl	-180(%rbp), %edx
	movq	%rax, %rdi
	xorl	%esi, %esi
	testl	%edx, %edx
	je	.L147
	call	d2i_PKCS7_bio@PLT
	movq	%rax, -128(%rbp)
	testq	%rax, %rax
	je	.L148
	movq	$0, -144(%rbp)
.L149:
	movq	-136(%rbp), %rax
	orq	-160(%rbp), %rax
	jne	.L378
	movq	-168(%rbp), %rax
	testq	%rax, %rax
	je	.L156
	leaq	.LC20(%rip), %rsi
	movq	%rax, %rdi
	call	BIO_new_file@PLT
	movq	%rax, -136(%rbp)
	testq	%rax, %rax
	je	.L157
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	d2i_TS_REQ_bio@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L201
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	TS_REQ_to_TS_VERIFY_CTX@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L152
	xorl	%eax, %eax
.L154:
	orl	$1, %eax
	movq	%r12, %rdi
	movl	%eax, %esi
	call	TS_VERIFY_CTX_add_flags@PLT
	call	X509_STORE_new@PLT
	leaq	verify_cb(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, -120(%rbp)
	call	X509_STORE_set_verify_cb@PLT
	cmpq	$0, -208(%rbp)
	je	.L158
	call	X509_LOOKUP_hash_dir@PLT
	movq	-120(%rbp), %rdi
	movq	%rax, %rsi
	call	X509_STORE_add_lookup@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L370
	movq	-208(%rbp), %rdx
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movl	$2, %esi
	call	X509_LOOKUP_ctrl@PLT
	testl	%eax, %eax
	je	.L379
.L158:
	cmpq	$0, -192(%rbp)
	je	.L161
	call	X509_LOOKUP_file@PLT
	movq	-120(%rbp), %rdi
	movq	%rax, %rsi
	call	X509_STORE_add_lookup@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L370
	movq	-192(%rbp), %rdx
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movl	$1, %esi
	call	X509_LOOKUP_ctrl@PLT
	testl	%eax, %eax
	je	.L380
.L161:
	movq	-176(%rbp), %rax
	testq	%rax, %rax
	je	.L163
	movq	-120(%rbp), %rdi
	movq	%rax, %rsi
	call	X509_STORE_set1_param@PLT
.L163:
	movq	-120(%rbp), %rsi
	movq	%r12, %rdi
	call	TS_VERIFY_CTX_set_store@PLT
	testq	%rax, %rax
	je	.L152
	movq	-224(%rbp), %rax
	testq	%rax, %rax
	je	.L164
	movq	%rax, %rdi
	call	TS_CONF_load_certs@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	TS_VERIFY_CTS_set_certs@PLT
	testq	%rax, %rax
	je	.L152
.L164:
	movq	-136(%rbp), %rdi
	call	BIO_free_all@PLT
	movq	%r14, %rdi
	call	TS_REQ_free@PLT
	movl	-180(%rbp), %eax
	testl	%eax, %eax
	je	.L381
	movq	-128(%rbp), %rsi
	movq	%r12, %rdi
	call	TS_RESP_verify_token@PLT
	movl	%eax, %r14d
.L165:
	xorl	%eax, %eax
	leaq	.LC21(%rip), %rsi
	movl	$1, %edi
	call	__printf_chk@PLT
	testl	%r14d, %r14d
	je	.L146
	leaq	.LC26(%rip), %rdi
	call	puts@PLT
.L166:
	movq	-152(%rbp), %rdi
	call	BIO_free_all@PLT
	movq	-128(%rbp), %rdi
	call	PKCS7_free@PLT
	movq	-144(%rbp), %rdi
	call	TS_RESP_free@PLT
	movq	%r12, %rdi
	call	TS_VERIFY_CTX_free@PLT
	jmp	.L60
.L376:
	movl	-152(%rbp), %ebx
	testl	%ebx, %ebx
	jne	.L89
	cmpq	$0, -136(%rbp)
	je	.L205
	cmpq	$0, -160(%rbp)
	jne	.L89
.L205:
	movq	-144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L105
	movl	$4, %edx
	movl	$114, %esi
	call	bio_open_default@PLT
	movq	%rax, -144(%rbp)
	testq	%rax, %rax
	je	.L110
	movq	%rax, %rdi
	xorl	%esi, %esi
	call	d2i_TS_REQ_bio@PLT
	movq	$0, -128(%rbp)
	movq	%rax, %r14
.L108:
	testq	%r14, %r14
	je	.L191
	movl	-184(%rbp), %r11d
	testl	%r11d, %r11d
	je	.L122
	movq	-176(%rbp), %rdi
	movl	$32769, %edx
	movl	$119, %esi
	call	bio_open_default@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L107
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	TS_REQ_print_bio@PLT
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
	movl	%eax, -152(%rbp)
.L107:
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	movq	-144(%rbp), %rdi
	call	BIO_free_all@PLT
	movq	-128(%rbp), %rdi
	call	BIO_free_all@PLT
	movq	%r12, %rdi
	call	BIO_free_all@PLT
	movq	%r14, %rdi
	call	TS_REQ_free@PLT
	movl	-152(%rbp), %ebx
	xorl	$1, %ebx
	jmp	.L60
.L375:
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	jmp	.L102
.L377:
	movl	-152(%rbp), %r10d
	testl	%r10d, %r10d
	jne	.L89
	movq	-144(%rbp), %rax
	testq	%rax, %rax
	je	.L355
	cmpq	$0, -168(%rbp)
	jne	.L89
	testq	%rax, %rax
	je	.L355
	movq	%rax, %rdi
	leaq	.LC20(%rip), %rsi
	call	BIO_new_file@PLT
	movq	%rax, -144(%rbp)
	testq	%rax, %rax
	je	.L382
	movl	-180(%rbp), %r8d
	movq	-144(%rbp), %rdi
	xorl	%esi, %esi
	testl	%r8d, %r8d
	je	.L127
	call	d2i_PKCS7_bio@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L193
	movq	%rax, %rdi
	call	PKCS7_to_TS_TST_INFO@PLT
	movq	%rax, -120(%rbp)
	testq	%rax, %rax
	je	.L194
	call	TS_RESP_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L195
	call	TS_STATUS_INFO_new@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L128
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	TS_STATUS_INFO_set_status@PLT
	testl	%eax, %eax
	je	.L128
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	TS_RESP_set_status_info@PLT
	testl	%eax, %eax
	jne	.L383
.L128:
	movq	%rbx, %rdi
	call	PKCS7_free@PLT
	movq	-120(%rbp), %rdi
	call	TS_TST_INFO_free@PLT
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	TS_RESP_free@PLT
	movq	%r14, %rdi
	xorl	%r14d, %r14d
	call	TS_STATUS_INFO_free@PLT
.L130:
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	movq	-144(%rbp), %rdi
	call	BIO_free_all@PLT
	xorl	%edi, %edi
	call	BIO_free_all@PLT
	xorl	%edi, %edi
	call	BIO_free_all@PLT
	xorl	%edi, %edi
	call	BIO_free_all@PLT
	movq	%r14, %rdi
	call	BIO_free_all@PLT
	movq	%r12, %rdi
	call	TS_RESP_free@PLT
	movl	-152(%rbp), %ebx
	xorl	$1, %ebx
	jmp	.L60
.L99:
	call	ERR_clear_error@PLT
	jmp	.L101
.L177:
	movl	$1, %ebx
	jmp	.L60
.L374:
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	jmp	.L101
.L105:
	movq	-96(%rbp), %rax
	cmpq	$0, -160(%rbp)
	movq	$0, -128(%rbp)
	movq	%rax, -168(%rbp)
	je	.L384
.L109:
	cmpq	$0, -168(%rbp)
	movq	$0, -88(%rbp)
	je	.L385
.L111:
	call	TS_REQ_new@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L113
	movl	$1, %esi
	movq	%rax, %rdi
	call	TS_REQ_set_version@PLT
	testl	%eax, %eax
	je	.L179
	call	TS_MSG_IMPRINT_new@PLT
	movq	%rax, -136(%rbp)
	testq	%rax, %rax
	je	.L180
	call	X509_ALGOR_new@PLT
	movq	%rax, -120(%rbp)
	testq	%rax, %rax
	je	.L187
	movq	-168(%rbp), %rdi
	call	EVP_MD_type@PLT
	movl	%eax, %edi
	call	OBJ_nid2obj@PLT
	movq	%rax, %rbx
	movq	-120(%rbp), %rax
	movq	%rbx, (%rax)
	testq	%rbx, %rbx
	je	.L202
	call	ASN1_TYPE_new@PLT
	movq	%rax, %rbx
	movq	-120(%rbp), %rax
	movq	%rbx, 8(%rax)
	testq	%rbx, %rbx
	je	.L202
	movl	$5, (%rbx)
	movq	-136(%rbp), %rbx
	movq	%rax, %rsi
	movq	%rbx, %rdi
	call	TS_MSG_IMPRINT_set_algo@PLT
	testl	%eax, %eax
	je	.L187
	movq	-168(%rbp), %rdx
	movq	-160(%rbp), %rsi
	leaq	-88(%rbp), %rcx
	movq	-128(%rbp), %rdi
	call	create_digest
	movl	%eax, %edx
	testl	%eax, %eax
	je	.L187
	movq	-88(%rbp), %rsi
	movq	%rbx, %rdi
	call	TS_MSG_IMPRINT_set_msg@PLT
	testl	%eax, %eax
	je	.L187
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	TS_REQ_set_msg_imprint@PLT
	testl	%eax, %eax
	je	.L187
	movq	-200(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L188
	xorl	%esi, %esi
	call	OBJ_txt2obj@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L386
	movq	%rax, %rsi
	movq	%r14, %rdi
	call	TS_REQ_set_policy_id@PLT
	testl	%eax, %eax
	je	.L202
.L114:
	testl	%r12d, %r12d
	jne	.L189
	leaq	-80(%rbp), %rax
	movl	$8, %esi
	movq	%rax, %rdi
	movq	%rax, -168(%rbp)
	call	RAND_bytes@PLT
	testl	%eax, %eax
	jle	.L117
	xorl	%eax, %eax
.L118:
	movq	-168(%rbp), %rcx
	movl	%eax, -160(%rbp)
	cmpb	$0, (%rcx,%rax)
	je	.L387
.L119:
	call	ASN1_INTEGER_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L117
	movq	8(%rax), %rdi
	movl	$540, %edx
	leaq	.LC1(%rip), %rsi
	call	CRYPTO_free@PLT
	movl	-160(%rbp), %ecx
	movl	$8, %eax
	movl	$9, %edi
	leaq	.LC17(%rip), %rsi
	subl	%ecx, %eax
	subl	%ecx, %edi
	movl	%eax, (%r12)
	call	app_malloc@PLT
	movq	-168(%rbp), %rsi
	movslq	(%r12), %rdx
	movq	%rax, %rdi
	movq	%rax, 8(%r12)
	movslq	-160(%rbp), %rax
	addq	%rax, %rsi
	call	memcpy@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	TS_REQ_set_nonce@PLT
	testl	%eax, %eax
	je	.L112
.L116:
	movl	-216(%rbp), %esi
	movq	%r14, %rdi
	call	TS_REQ_set_cert_req@PLT
	testl	%eax, %eax
	je	.L112
.L121:
	movq	-136(%rbp), %rdi
	call	TS_MSG_IMPRINT_free@PLT
	movq	-120(%rbp), %rdi
	call	X509_ALGOR_free@PLT
	movq	-88(%rbp), %rdi
	movl	$472, %edx
	leaq	.LC1(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%rbx, %rdi
	call	ASN1_OBJECT_free@PLT
	movq	%r12, %rdi
	call	ASN1_INTEGER_free@PLT
	jmp	.L108
.L207:
	cmpq	$0, -136(%rbp)
	je	.L142
.L360:
	cmpq	$0, -168(%rbp)
	je	.L140
.L142:
	movq	-136(%rbp), %rax
	orq	-168(%rbp), %rax
	jne	.L89
	cmpq	$0, -160(%rbp)
	jne	.L143
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L147:
	call	d2i_TS_RESP_bio@PLT
	movq	%rax, -144(%rbp)
	testq	%rax, %rax
	je	.L148
	movq	$0, -128(%rbp)
	jmp	.L149
.L122:
	movq	-176(%rbp), %rdi
	movl	$4, %edx
	movl	$119, %esi
	call	bio_open_default@PLT
	movl	$0, -152(%rbp)
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L107
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	i2d_TS_REQ_bio@PLT
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
	movl	%eax, -152(%rbp)
	jmp	.L107
.L355:
	movl	-180(%rbp), %r9d
	testl	%r9d, %r9d
	jne	.L89
	movq	-96(%rbp), %rax
	movq	-168(%rbp), %rdi
	leaq	.LC20(%rip), %rsi
	movq	%rax, -120(%rbp)
	movq	-104(%rbp), %rax
	movq	%rax, -128(%rbp)
	call	BIO_new_file@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L203
	movq	-240(%rbp), %rsi
	movq	%r15, %rdi
	call	TS_CONF_get_tsa_section@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L203
	call	TS_RESP_CTX_new@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L203
	movq	%rax, %rcx
	leaq	serial_cb(%rip), %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	TS_CONF_set_serial@PLT
	testl	%eax, %eax
	je	.L132
	movq	-232(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	TS_CONF_set_crypto_device@PLT
	testl	%eax, %eax
	je	.L132
	movq	-256(%rbp), %rdx
	movq	%r14, %rcx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	TS_CONF_set_signer_cert@PLT
	testl	%eax, %eax
	je	.L132
	movq	-264(%rbp), %rdx
	movq	%r14, %rcx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	TS_CONF_set_certs@PLT
	testl	%eax, %eax
	je	.L132
	movq	-128(%rbp), %rcx
	movq	%r14, %r8
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	-248(%rbp), %rdx
	call	TS_CONF_set_signer_key@PLT
	testl	%eax, %eax
	je	.L132
	movq	-120(%rbp), %rax
	testq	%rax, %rax
	je	.L133
	movq	%rax, %rsi
	movq	%r14, %rdi
	call	TS_RESP_CTX_set_signer_digest@PLT
	testl	%eax, %eax
	je	.L132
.L135:
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	TS_CONF_set_ess_cert_id_digest@PLT
	testl	%eax, %eax
	je	.L132
	movq	-200(%rbp), %rdx
	movq	%r14, %rcx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	TS_CONF_set_def_policy@PLT
	testl	%eax, %eax
	je	.L132
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	TS_CONF_set_policies@PLT
	testl	%eax, %eax
	je	.L132
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	TS_CONF_set_digests@PLT
	testl	%eax, %eax
	je	.L132
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	TS_CONF_set_accuracy@PLT
	testl	%eax, %eax
	je	.L132
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	TS_CONF_set_clock_precision_digits@PLT
	testl	%eax, %eax
	je	.L132
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	TS_CONF_set_ordering@PLT
	testl	%eax, %eax
	je	.L132
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	TS_CONF_set_tsa_name@PLT
	testl	%eax, %eax
	je	.L132
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	TS_CONF_set_ess_cert_id_chain@PLT
	testl	%eax, %eax
	je	.L132
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	TS_RESP_create_response@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L132
	movq	%r14, %rdi
	call	TS_RESP_CTX_free@PLT
	movq	%rbx, %rdi
	call	BIO_free_all@PLT
	movq	bio_err(%rip), %rdi
	leaq	.LC29(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L129:
	movl	-184(%rbp), %esi
	testl	%esi, %esi
	je	.L137
	movq	-176(%rbp), %rdi
	movl	$32769, %edx
	movl	$119, %esi
	call	bio_open_default@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L130
	cmpl	$0, -212(%rbp)
	je	.L138
	movq	%r12, %rdi
	call	TS_RESP_get_tst_info@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	TS_TST_INFO_print_bio@PLT
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
	movl	%eax, -152(%rbp)
	jmp	.L130
.L201:
	xorl	%r12d, %r12d
.L152:
	movq	%r12, %rdi
	call	TS_VERIFY_CTX_free@PLT
	movq	-136(%rbp), %rdi
	call	BIO_free_all@PLT
	movq	%r14, %rdi
	call	TS_REQ_free@PLT
.L156:
	leaq	.LC21(%rip), %rsi
	movl	$1, %edi
	xorl	%eax, %eax
	xorl	%r12d, %r12d
	call	__printf_chk@PLT
.L146:
	leaq	.LC27(%rip), %rdi
	movl	$1, %ebx
	call	puts@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	jmp	.L166
.L137:
	movq	-176(%rbp), %rdi
	movl	$4, %edx
	movl	$119, %esi
	call	bio_open_default@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L369
	cmpl	$0, -212(%rbp)
	je	.L139
	movq	%r12, %rdi
	call	TS_RESP_get_token@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	i2d_PKCS7_bio@PLT
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
	movl	%eax, -152(%rbp)
	jmp	.L130
.L127:
	call	d2i_TS_RESP_bio@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L129
	movl	$0, -152(%rbp)
	xorl	%r14d, %r14d
	jmp	.L130
.L382:
	xorl	%r14d, %r14d
	xorl	%r12d, %r12d
	jmp	.L130
.L139:
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	i2d_TS_RESP_bio@PLT
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
	movl	%eax, -152(%rbp)
	jmp	.L130
.L203:
	xorl	%r14d, %r14d
.L132:
	xorl	%edi, %edi
	xorl	%r12d, %r12d
	call	TS_RESP_free@PLT
	movq	%r14, %rdi
	xorl	%r14d, %r14d
	call	TS_RESP_CTX_free@PLT
	movq	%rbx, %rdi
	call	BIO_free_all@PLT
	movq	bio_err(%rip), %rdi
	leaq	.LC28(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L369:
	movl	$0, -152(%rbp)
	jmp	.L130
.L381:
	movq	-144(%rbp), %rsi
	movq	%r12, %rdi
	call	TS_RESP_verify_response@PLT
	movl	%eax, %r14d
	jmp	.L165
.L138:
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	TS_RESP_print_bio@PLT
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
	movl	%eax, -152(%rbp)
	jmp	.L130
.L191:
	xorl	%r12d, %r12d
	jmp	.L107
.L383:
	movq	-120(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	TS_RESP_set_tst_info@PLT
	xorl	%edi, %edi
	call	PKCS7_free@PLT
	xorl	%edi, %edi
	call	TS_TST_INFO_free@PLT
	movq	%r14, %rdi
	call	TS_STATUS_INFO_free@PLT
	jmp	.L129
.L384:
	movq	-136(%rbp), %rdi
	movl	$4, %edx
	movl	$114, %esi
	call	bio_open_default@PLT
	movq	%rax, -128(%rbp)
	testq	%rax, %rax
	jne	.L109
.L110:
	movq	$0, -128(%rbp)
	xorl	%r12d, %r12d
	xorl	%r14d, %r14d
	movq	$0, -144(%rbp)
	jmp	.L107
.L157:
	movq	$0, -136(%rbp)
	xorl	%r14d, %r14d
	xorl	%r12d, %r12d
	jmp	.L152
.L378:
	call	TS_VERIFY_CTX_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L157
	movq	-136(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L153
	leaq	.LC20(%rip), %rsi
	call	BIO_new_file@PLT
	movq	%rax, -136(%rbp)
	testq	%rax, %rax
	je	.L199
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	TS_VERIFY_CTX_set_data@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L388
	movq	$0, -136(%rbp)
	movl	$82, %eax
	xorl	%r14d, %r14d
	jmp	.L154
.L373:
	call	__stack_chk_fail@PLT
.L148:
	leaq	.LC21(%rip), %rsi
	movl	$1, %edi
	xorl	%eax, %eax
	xorl	%r12d, %r12d
	call	__printf_chk@PLT
	movq	$0, -128(%rbp)
	movq	$0, -144(%rbp)
	jmp	.L146
.L179:
	movq	$0, -120(%rbp)
	xorl	%r12d, %r12d
	xorl	%ebx, %ebx
	movq	$0, -136(%rbp)
.L112:
	movq	%r14, %rdi
	xorl	%r14d, %r14d
	call	TS_REQ_free@PLT
	movq	bio_err(%rip), %rdi
	leaq	.LC19(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	jmp	.L121
.L385:
	leaq	.LC15(%rip), %rdi
	call	EVP_get_digestbyname@PLT
	movq	%rax, -168(%rbp)
	testq	%rax, %rax
	jne	.L111
.L113:
	movq	$0, -120(%rbp)
	xorl	%r12d, %r12d
	xorl	%ebx, %ebx
	xorl	%r14d, %r14d
	movq	$0, -136(%rbp)
	jmp	.L112
.L188:
	xorl	%ebx, %ebx
	jmp	.L114
.L202:
	xorl	%r12d, %r12d
	jmp	.L112
.L187:
	xorl	%r12d, %r12d
	xorl	%ebx, %ebx
	jmp	.L112
.L180:
	movq	$0, -120(%rbp)
	xorl	%r12d, %r12d
	xorl	%ebx, %ebx
	jmp	.L112
.L379:
	movq	-208(%rbp), %rdx
	movq	bio_err(%rip), %rdi
	leaq	.LC24(%rip), %rsi
	call	BIO_printf@PLT
.L160:
	movq	-120(%rbp), %rdi
	call	X509_STORE_free@PLT
	movq	$0, -120(%rbp)
	jmp	.L163
.L370:
	movq	bio_err(%rip), %rdi
	leaq	.LC23(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L160
.L194:
	xorl	%r14d, %r14d
	xorl	%r12d, %r12d
	jmp	.L128
.L193:
	movq	$0, -120(%rbp)
	xorl	%r14d, %r14d
	xorl	%r12d, %r12d
	jmp	.L128
.L380:
	movq	-192(%rbp), %rdx
	movq	bio_err(%rip), %rdi
	leaq	.LC25(%rip), %rsi
	call	BIO_printf@PLT
	jmp	.L160
.L133:
	xorl	%edx, %edx
	movq	%r14, %rcx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	TS_CONF_set_signer_digest@PLT
	testl	%eax, %eax
	jne	.L135
	jmp	.L132
.L387:
	addq	$1, %rax
	cmpq	$8, %rax
	jne	.L118
	movl	$8, -160(%rbp)
	jmp	.L119
.L117:
	movq	bio_err(%rip), %rdi
	leaq	.LC18(%rip), %rsi
	xorl	%eax, %eax
	xorl	%r12d, %r12d
	call	BIO_printf@PLT
	xorl	%edi, %edi
	call	ASN1_INTEGER_free@PLT
	jmp	.L112
.L189:
	xorl	%r12d, %r12d
	jmp	.L116
.L386:
	movq	-200(%rbp), %rdx
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
	xorl	%r12d, %r12d
	leaq	.LC16(%rip), %rsi
	call	BIO_printf@PLT
	jmp	.L112
.L195:
	xorl	%r14d, %r14d
	jmp	.L128
.L388:
	movq	-136(%rbp), %rdi
	call	BIO_free_all@PLT
	movq	$0, -136(%rbp)
	jmp	.L152
.L199:
	xorl	%r14d, %r14d
	jmp	.L152
.L153:
	movq	-160(%rbp), %rdi
	leaq	-88(%rbp), %rsi
	call	OPENSSL_hexstr2buf@PLT
	movq	-88(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	TS_VERIFY_CTX_set_imprint@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L389
	xorl	%r14d, %r14d
	movl	$74, %eax
	jmp	.L154
.L389:
	movq	bio_err(%rip), %rdi
	leaq	.LC22(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L152
	.cfi_endproc
.LFE1507:
	.size	ts_main, .-ts_main
	.section	.rodata.str1.1
.LC30:
	.string	"Typical uses:"
	.section	.rodata.str1.8
	.align 8
.LC31:
	.string	"ts -query [-rand file...] [-config file] [-data file]"
	.align 8
.LC32:
	.string	"          [-digest hexstring] [-tspolicy oid] [-no_nonce] [-cert]"
	.align 8
.LC33:
	.string	"          [-in file] [-out file] [-text]"
	.section	.rodata.str1.1
.LC34:
	.string	"  or"
	.section	.rodata.str1.8
	.align 8
.LC35:
	.string	"ts -reply [-config file] [-section tsa_section]"
	.align 8
.LC36:
	.string	"          [-queryfile file] [-passin password]"
	.align 8
.LC37:
	.string	"          [-signer tsa_cert.pem] [-inkey private_key.pem]"
	.align 8
.LC38:
	.string	"          [-chain certs_file.pem] [-tspolicy oid]"
	.align 8
.LC39:
	.string	"          [-in file] [-token_in] [-out file] [-token_out]"
	.align 8
.LC40:
	.string	"          [-text] [-engine id]"
	.align 8
.LC41:
	.string	"ts -verify -CApath dir -CAfile file.pem -untrusted file.pem"
	.align 8
.LC42:
	.string	"           [-data file] [-digest hexstring]"
	.align 8
.LC43:
	.string	"           [-queryfile file] -in file [-token_in]"
	.align 8
.LC44:
	.string	"           [[options specific to 'ts -verify']]"
	.section	.data.rel.local,"aw"
	.align 32
	.type	opt_helplist, @object
	.size	opt_helplist, 136
opt_helplist:
	.quad	.LC30
	.quad	.LC31
	.quad	.LC32
	.quad	.LC33
	.quad	.LC34
	.quad	.LC35
	.quad	.LC36
	.quad	.LC37
	.quad	.LC38
	.quad	.LC39
	.quad	.LC40
	.quad	.LC34
	.quad	.LC41
	.quad	.LC42
	.quad	.LC43
	.quad	.LC44
	.quad	0
	.globl	ts_options
	.section	.rodata.str1.1
.LC45:
	.string	"help"
.LC46:
	.string	"Display this summary"
.LC47:
	.string	"config"
.LC48:
	.string	"Configuration file"
.LC49:
	.string	"section"
	.section	.rodata.str1.8
	.align 8
.LC50:
	.string	"Section to use within config file"
	.section	.rodata.str1.1
.LC51:
	.string	"query"
.LC52:
	.string	"Generate a TS query"
.LC53:
	.string	"data"
.LC54:
	.string	"File to hash"
.LC55:
	.string	"digest"
.LC56:
	.string	"Digest (as a hex string)"
.LC57:
	.string	"rand"
	.section	.rodata.str1.8
	.align 8
.LC58:
	.string	"Load the file(s) into the random number generator"
	.section	.rodata.str1.1
.LC59:
	.string	"writerand"
	.section	.rodata.str1.8
	.align 8
.LC60:
	.string	"Write random data to the specified file"
	.section	.rodata.str1.1
.LC61:
	.string	"tspolicy"
.LC62:
	.string	"Policy OID to use"
.LC63:
	.string	"no_nonce"
.LC64:
	.string	"Do not include a nonce"
.LC65:
	.string	"cert"
.LC66:
	.string	"Put cert request into query"
.LC67:
	.string	"in"
.LC68:
	.string	"Input file"
.LC69:
	.string	"token_in"
.LC70:
	.string	"Input is a PKCS#7 file"
.LC71:
	.string	"out"
.LC72:
	.string	"Output file"
.LC73:
	.string	"token_out"
.LC74:
	.string	"Output is a PKCS#7 file"
.LC75:
	.string	"text"
.LC76:
	.string	"Output text (not DER)"
.LC77:
	.string	"reply"
.LC78:
	.string	"Generate a TS reply"
.LC79:
	.string	"queryfile"
.LC80:
	.string	"File containing a TS query"
.LC81:
	.string	"passin"
.LC82:
	.string	"Input file pass phrase source"
.LC83:
	.string	"inkey"
	.section	.rodata.str1.8
	.align 8
.LC84:
	.string	"File with private key for reply"
	.section	.rodata.str1.1
.LC85:
	.string	"signer"
.LC86:
	.string	"Signer certificate file"
.LC87:
	.string	"chain"
.LC88:
	.string	"File with signer CA chain"
.LC89:
	.string	"verify"
.LC90:
	.string	"Verify a TS response"
.LC91:
	.string	"CApath"
.LC92:
	.string	"Path to trusted CA files"
.LC93:
	.string	"CAfile"
.LC94:
	.string	"File with trusted CA certs"
.LC95:
	.string	"untrusted"
.LC96:
	.string	"File with untrusted certs"
.LC97:
	.string	""
.LC98:
	.string	"Any supported digest"
.LC99:
	.string	"engine"
	.section	.rodata.str1.8
	.align 8
.LC100:
	.string	"Use engine, possibly a hardware device"
	.align 8
.LC101:
	.string	"\nOptions specific to 'ts -verify': \n"
	.section	.rodata.str1.1
.LC102:
	.string	"policy"
	.section	.rodata.str1.8
	.align 8
.LC103:
	.string	"adds policy to the acceptable policy set"
	.section	.rodata.str1.1
.LC104:
	.string	"purpose"
.LC105:
	.string	"certificate chain purpose"
.LC106:
	.string	"verify_name"
.LC107:
	.string	"verification policy name"
.LC108:
	.string	"verify_depth"
.LC109:
	.string	"chain depth limit"
.LC110:
	.string	"auth_level"
	.section	.rodata.str1.8
	.align 8
.LC111:
	.string	"chain authentication security level"
	.section	.rodata.str1.1
.LC112:
	.string	"attime"
.LC113:
	.string	"verification epoch time"
.LC114:
	.string	"verify_hostname"
.LC115:
	.string	"expected peer hostname"
.LC116:
	.string	"verify_email"
.LC117:
	.string	"expected peer email"
.LC118:
	.string	"verify_ip"
.LC119:
	.string	"expected peer IP address"
.LC120:
	.string	"ignore_critical"
	.section	.rodata.str1.8
	.align 8
.LC121:
	.string	"permit unhandled critical extensions"
	.section	.rodata.str1.1
.LC122:
	.string	"issuer_checks"
.LC123:
	.string	"(deprecated)"
.LC124:
	.string	"crl_check"
	.section	.rodata.str1.8
	.align 8
.LC125:
	.string	"check leaf certificate revocation"
	.section	.rodata.str1.1
.LC126:
	.string	"crl_check_all"
.LC127:
	.string	"check full chain revocation"
.LC128:
	.string	"policy_check"
.LC129:
	.string	"perform rfc5280 policy checks"
.LC130:
	.string	"explicit_policy"
	.section	.rodata.str1.8
	.align 8
.LC131:
	.string	"set policy variable require-explicit-policy"
	.section	.rodata.str1.1
.LC132:
	.string	"inhibit_any"
	.section	.rodata.str1.8
	.align 8
.LC133:
	.string	"set policy variable inhibit-any-policy"
	.section	.rodata.str1.1
.LC134:
	.string	"inhibit_map"
	.section	.rodata.str1.8
	.align 8
.LC135:
	.string	"set policy variable inhibit-policy-mapping"
	.section	.rodata.str1.1
.LC136:
	.string	"x509_strict"
	.section	.rodata.str1.8
	.align 8
.LC137:
	.string	"disable certificate compatibility work-arounds"
	.section	.rodata.str1.1
.LC138:
	.string	"extended_crl"
.LC139:
	.string	"enable extended CRL features"
.LC140:
	.string	"use_deltas"
.LC141:
	.string	"use delta CRLs"
.LC142:
	.string	"policy_print"
	.section	.rodata.str1.8
	.align 8
.LC143:
	.string	"print policy processing diagnostics"
	.section	.rodata.str1.1
.LC144:
	.string	"check_ss_sig"
.LC145:
	.string	"check root CA self-signatures"
.LC146:
	.string	"trusted_first"
	.section	.rodata.str1.8
	.align 8
.LC147:
	.string	"search trust store first (default)"
	.section	.rodata.str1.1
.LC148:
	.string	"suiteB_128_only"
.LC149:
	.string	"Suite B 128-bit-only mode"
.LC150:
	.string	"suiteB_128"
	.section	.rodata.str1.8
	.align 8
.LC151:
	.string	"Suite B 128-bit mode allowing 192-bit algorithms"
	.section	.rodata.str1.1
.LC152:
	.string	"suiteB_192"
.LC153:
	.string	"Suite B 192-bit-only mode"
.LC154:
	.string	"partial_chain"
	.section	.rodata.str1.8
	.align 8
.LC155:
	.string	"accept chains anchored by intermediate trust-store CAs"
	.section	.rodata.str1.1
.LC156:
	.string	"no_alt_chains"
.LC157:
	.string	"no_check_time"
	.section	.rodata.str1.8
	.align 8
.LC158:
	.string	"ignore certificate validity time"
	.section	.rodata.str1.1
.LC159:
	.string	"allow_proxy_certs"
	.section	.rodata.str1.8
	.align 8
.LC160:
	.string	"allow the use of proxy certificates"
	.section	.data.rel.ro,"aw"
	.align 32
	.type	ts_options, @object
	.size	ts_options, 1464
ts_options:
	.quad	.LC45
	.long	1
	.long	45
	.quad	.LC46
	.quad	.LC47
	.long	3
	.long	60
	.quad	.LC48
	.quad	.LC49
	.long	4
	.long	115
	.quad	.LC50
	.quad	.LC51
	.long	5
	.long	45
	.quad	.LC52
	.quad	.LC53
	.long	6
	.long	60
	.quad	.LC54
	.quad	.LC55
	.long	7
	.long	115
	.quad	.LC56
	.quad	.LC57
	.long	1501
	.long	115
	.quad	.LC58
	.quad	.LC59
	.long	1502
	.long	62
	.quad	.LC60
	.quad	.LC61
	.long	8
	.long	115
	.quad	.LC62
	.quad	.LC63
	.long	9
	.long	45
	.quad	.LC64
	.quad	.LC65
	.long	10
	.long	45
	.quad	.LC66
	.quad	.LC67
	.long	11
	.long	60
	.quad	.LC68
	.quad	.LC69
	.long	12
	.long	45
	.quad	.LC70
	.quad	.LC71
	.long	13
	.long	62
	.quad	.LC72
	.quad	.LC73
	.long	14
	.long	45
	.quad	.LC74
	.quad	.LC75
	.long	15
	.long	45
	.quad	.LC76
	.quad	.LC77
	.long	16
	.long	45
	.quad	.LC78
	.quad	.LC79
	.long	17
	.long	60
	.quad	.LC80
	.quad	.LC81
	.long	18
	.long	115
	.quad	.LC82
	.quad	.LC83
	.long	19
	.long	115
	.quad	.LC84
	.quad	.LC85
	.long	20
	.long	115
	.quad	.LC86
	.quad	.LC87
	.long	21
	.long	60
	.quad	.LC88
	.quad	.LC89
	.long	22
	.long	45
	.quad	.LC90
	.quad	.LC91
	.long	23
	.long	47
	.quad	.LC92
	.quad	.LC93
	.long	24
	.long	60
	.quad	.LC94
	.quad	.LC95
	.long	25
	.long	60
	.quad	.LC96
	.quad	.LC97
	.long	26
	.long	45
	.quad	.LC98
	.quad	.LC99
	.long	2
	.long	115
	.quad	.LC100
	.quad	OPT_HELP_STR
	.long	1
	.long	45
	.quad	.LC101
	.quad	.LC102
	.long	2001
	.long	115
	.quad	.LC103
	.quad	.LC104
	.long	2002
	.long	115
	.quad	.LC105
	.quad	.LC106
	.long	2003
	.long	115
	.quad	.LC107
	.quad	.LC108
	.long	2004
	.long	110
	.quad	.LC109
	.quad	.LC110
	.long	2029
	.long	110
	.quad	.LC111
	.quad	.LC112
	.long	2005
	.long	77
	.quad	.LC113
	.quad	.LC114
	.long	2006
	.long	115
	.quad	.LC115
	.quad	.LC116
	.long	2007
	.long	115
	.quad	.LC117
	.quad	.LC118
	.long	2008
	.long	115
	.quad	.LC119
	.quad	.LC120
	.long	2009
	.long	45
	.quad	.LC121
	.quad	.LC122
	.long	2010
	.long	45
	.quad	.LC123
	.quad	.LC124
	.long	2011
	.long	45
	.quad	.LC125
	.quad	.LC126
	.long	2012
	.long	45
	.quad	.LC127
	.quad	.LC128
	.long	2013
	.long	45
	.quad	.LC129
	.quad	.LC130
	.long	2014
	.long	45
	.quad	.LC131
	.quad	.LC132
	.long	2015
	.long	45
	.quad	.LC133
	.quad	.LC134
	.long	2016
	.long	45
	.quad	.LC135
	.quad	.LC136
	.long	2017
	.long	45
	.quad	.LC137
	.quad	.LC138
	.long	2018
	.long	45
	.quad	.LC139
	.quad	.LC140
	.long	2019
	.long	45
	.quad	.LC141
	.quad	.LC142
	.long	2020
	.long	45
	.quad	.LC143
	.quad	.LC144
	.long	2021
	.long	45
	.quad	.LC145
	.quad	.LC146
	.long	2022
	.long	45
	.quad	.LC147
	.quad	.LC148
	.long	2023
	.long	45
	.quad	.LC149
	.quad	.LC150
	.long	2024
	.long	45
	.quad	.LC151
	.quad	.LC152
	.long	2025
	.long	45
	.quad	.LC153
	.quad	.LC154
	.long	2026
	.long	45
	.quad	.LC155
	.quad	.LC156
	.long	2027
	.long	45
	.quad	.LC123
	.quad	.LC157
	.long	2028
	.long	45
	.quad	.LC158
	.quad	.LC159
	.long	2030
	.long	45
	.quad	.LC160
	.quad	OPT_HELP_STR
	.long	1
	.long	45
	.quad	.LC9
	.quad	0
	.zero	16
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
