	.file	"rehash.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/apps/rehash.c"
	.text
	.p2align 4
	.type	str_free, @function
str_free:
.LFB1442:
	.cfi_startproc
	endbr64
	movl	$308, %edx
	leaq	.LC0(%rip), %rsi
	jmp	CRYPTO_free@PLT
	.cfi_endproc
.LFE1442:
	.size	str_free, .-str_free
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"certificate"
.LC2:
	.string	"CRL"
	.section	.rodata.str1.8
	.align 8
.LC3:
	.string	"%s: warning: skipping duplicate %s in %s\n"
	.align 8
.LC4:
	.string	"%s: error: hash table overflow for %s\n"
	.section	.rodata.str1.1
.LC5:
	.string	"collision bucket"
.LC6:
	.string	"hash bucket"
	.text
	.p2align 4
	.type	add_entry, @function
add_entry:
.LFB1439:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leal	(%rdi,%rsi), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	hash_table(%rip), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	movq	%rax, %rcx
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	movl	$4278255361, %edx
	imulq	%rdx, %rax
	pushq	%r12
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	shrq	$40, %rax
	movl	%eax, %edx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movl	%edi, -52(%rbp)
	sall	$8, %edx
	movl	%r8d, -56(%rbp)
	addl	%edx, %eax
	movl	%ecx, %edx
	movl	%r9d, -68(%rbp)
	subl	%eax, %edx
	movq	(%r15,%rdx,8), %rbx
	testq	%rbx, %rbx
	jne	.L7
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L5:
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L4
.L7:
	movzwl	28(%rbx), %eax
	cmpl	-52(%rbp), %eax
	jne	.L5
	cmpl	%r12d, 24(%rbx)
	jne	.L5
.L6:
	movq	8(%rbx), %r15
	testq	%r15, %r15
	je	.L8
	movslq	evpmdsize(%rip), %rax
	xorl	%r12d, %r12d
	movq	%rax, -64(%rbp)
	testq	%r14, %r14
	je	.L11
	.p2align 4,,10
	.p2align 3
.L9:
	movq	-64(%rbp), %rdx
	leaq	19(%r15), %rsi
	movq	%r14, %rdi
	call	memcmp@PLT
	testl	%eax, %eax
	je	.L40
	movq	8(%r15), %rsi
	movq	%r13, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	cmove	%r15, %r12
	movq	(%r15), %r15
	testq	%r15, %r15
	jne	.L9
	testq	%r12, %r12
	jne	.L10
	.p2align 4,,10
	.p2align 3
.L8:
	cmpw	$255, 30(%rbx)
	ja	.L41
	leaq	.LC5(%rip), %rsi
	movl	$88, %edi
	call	app_malloc@PLT
	movdqa	nilhentry.24651(%rip), %xmm2
	movq	%r13, %rdi
	movdqa	16+nilhentry.24651(%rip), %xmm3
	movdqa	32+nilhentry.24651(%rip), %xmm4
	movdqa	48+nilhentry.24651(%rip), %xmm5
	movq	%rax, %r12
	movl	$-1, %edx
	movdqa	64+nilhentry.24651(%rip), %xmm6
	movups	%xmm2, (%rax)
	leaq	.LC0(%rip), %rsi
	movups	%xmm3, 16(%rax)
	movups	%xmm4, 32(%rax)
	movups	%xmm5, 48(%rax)
	movups	%xmm6, 64(%rax)
	movq	80+nilhentry.24651(%rip), %rax
	movw	%dx, 16(%r12)
	movl	$171, %edx
	movq	%rax, 80(%r12)
	call	CRYPTO_strdup@PLT
	movq	%rax, 8(%r12)
	movq	16(%rbx), %rax
	testq	%rax, %rax
	je	.L17
	movq	%r12, (%rax)
.L17:
	cmpq	$0, 8(%rbx)
	je	.L42
.L18:
	movq	%r12, 16(%rbx)
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L43:
	movq	(%r15), %r15
	testq	%r15, %r15
	je	.L8
.L11:
	movq	8(%r15), %rsi
	movq	%r13, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L43
	movq	%r15, %r12
.L10:
	movl	-68(%rbp), %eax
	cmpw	%ax, 16(%r12)
	jbe	.L19
	movw	%ax, 16(%r12)
.L19:
	movl	-56(%rbp), %eax
	xorl	%r9d, %r9d
	testl	%eax, %eax
	je	.L3
	cmpb	$0, 18(%r12)
	je	.L44
.L3:
	addq	$40, %rsp
	movl	%r9d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L40:
	.cfi_restore_state
	movl	-52(%rbp), %ecx
	leaq	.LC1(%rip), %r12
	movl	%eax, -64(%rbp)
	testl	%ecx, %ecx
	leaq	.LC2(%rip), %rcx
	cmovne	%rcx, %r12
	call	opt_getprog@PLT
	movq	bio_err(%rip), %rdi
	movq	%r13, %r8
	leaq	.LC3(%rip), %rsi
	movq	%rax, %rdx
	movq	%r12, %rcx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movl	-64(%rbp), %r9d
	jmp	.L3
.L41:
	call	opt_getprog@PLT
	movq	bio_err(%rip), %rdi
	movq	%r13, %rcx
	leaq	.LC4(%rip), %rsi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movl	$1, %r9d
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L4:
	leaq	.LC6(%rip), %rsi
	movl	$32, %edi
	movq	%rdx, -64(%rbp)
	call	app_malloc@PLT
	movq	-64(%rbp), %rdx
	movdqa	nilbucket.24650(%rip), %xmm0
	movdqa	16+nilbucket.24650(%rip), %xmm1
	movq	%rax, %rbx
	movups	%xmm0, (%rax)
	movups	%xmm1, 16(%rax)
	movq	(%r15,%rdx,8), %rax
	movl	%r12d, 24(%rbx)
	movq	%rax, (%rbx)
	movzwl	-52(%rbp), %eax
	movq	%rbx, (%r15,%rdx,8)
	movw	%ax, 28(%rbx)
	jmp	.L6
.L44:
	movb	$1, 18(%r12)
	movslq	evpmdsize(%rip), %rdx
	leaq	19(%r12), %rdi
	movq	%r14, %rsi
	addw	$1, 30(%rbx)
	movl	%r9d, -52(%rbp)
	call	memcpy@PLT
	movl	-52(%rbp), %r9d
	jmp	.L3
.L42:
	movq	%r12, 8(%rbx)
	jmp	.L18
	.cfi_endproc
.LFE1439:
	.size	add_entry, .-add_entry
	.section	.rodata.str1.1
.LC7:
	.string	"r"
	.text
	.p2align 4
	.type	handle_symlink, @function
handle_symlink:
.LFB1440:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	xorl	%ebx, %ebx
	movq	%rsi, -4184(%rbp)
	movq	%rdi, %r12
	movq	%r12, %r15
	leaq	8(%r12), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	__ctype_b_loc@PLT
	movq	%rax, %r14
	.p2align 4,,10
	.p2align 3
.L48:
	movzbl	(%r15), %edx
	movq	(%r14), %rax
	movq	%rdx, %rdi
	testb	$16, 1(%rax,%rdx,2)
	je	.L49
	call	OPENSSL_hexchar2int@PLT
	sall	$4, %ebx
	addq	$1, %r15
	addl	%eax, %ebx
	cmpq	%r15, %r13
	jne	.L48
	cmpb	$46, 8(%r12)
	jne	.L49
	leaq	9(%r12), %rsi
	movl	$1, %edx
	leaq	.LC7(%rip), %rdi
	xorl	%r13d, %r13d
	call	strncasecmp@PLT
	testl	%eax, %eax
	leaq	suffixes(%rip), %rax
	sete	%dl
	sete	%r13b
	movzbl	%dl, %edx
	movq	(%rax,%rdx,8), %rdi
	call	strlen@PLT
	leaq	-4168(%rbp), %rsi
	movl	$10, %edx
	leal	9(%rax), %edi
	movslq	%edi, %rdi
	addq	%r12, %rdi
	call	strtoul@PLT
	movq	%rax, %r12
	movq	-4168(%rbp), %rax
	cmpb	$0, (%rax)
	jne	.L49
	leaq	-4160(%rbp), %r15
	movq	-4184(%rbp), %rdi
	movl	$4096, %edx
	movq	%r15, %rsi
	call	readlink@PLT
	cmpq	$4095, %rax
	ja	.L49
	movzwl	%r12w, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r15, %rdx
	movl	%ebx, %esi
	movl	%r13d, %edi
	movb	$0, -4160(%rbp,%rax)
	call	add_entry
.L45:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L53
	addq	$4152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L49:
	.cfi_restore_state
	movl	$-1, %eax
	jmp	.L45
.L53:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1440:
	.size	handle_symlink, .-handle_symlink
	.section	.rodata.str1.1
.LC8:
	.string	""
.LC9:
	.string	"/"
.LC10:
	.string	"Skipping %s, can't write\n"
.LC11:
	.string	"filename buffer"
.LC12:
	.string	"Doing %s\n"
.LC13:
	.string	"Skipping %s, out of memory\n"
.LC14:
	.string	"out of memory\n"
.LC15:
	.string	"%s%s%s"
.LC16:
	.string	"pem"
.LC17:
	.string	"crt"
.LC18:
	.string	"cer"
.LC19:
	.string	"crl"
	.section	.rodata.str1.8
	.align 8
.LC20:
	.string	"%s: error: skipping %s, cannot open file\n"
	.align 8
.LC21:
	.string	"%s: warning: skipping %s,it does not contain exactly one certificate or CRL\n"
	.section	.rodata.str1.1
.LC22:
	.string	"%08x.%s%d"
.LC23:
	.string	"link %s -> %s\n"
.LC24:
	.string	"%s%s%n%08x.%s%d"
.LC25:
	.string	"%s: Can't unlink %s, %s\n"
.LC26:
	.string	"%s: Can't symlink %s, %s\n"
.LC27:
	.string	"unlink %s\n"
	.text
	.p2align 4
	.type	do_dir, @function
do_dir:
.LFB1444:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%esi, %ebx
	movl	$2, %esi
	subq	$360, %rsp
	movq	%rdi, -344(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -280(%rbp)
	call	app_access@PLT
	testl	%eax, %eax
	js	.L165
	movq	-344(%rbp), %r14
	movq	%r14, %rdi
	call	strlen@PLT
	leaq	.LC8(%rip), %rsi
	movq	%rsi, -360(%rbp)
	testl	%eax, %eax
	jne	.L166
.L57:
	addl	$257, %eax
	leaq	.LC11(%rip), %rsi
	movl	%eax, %edi
	movl	%eax, -308(%rbp)
	call	app_malloc@PLT
	movl	verbose(%rip), %r8d
	movq	%rax, %r14
	testl	%r8d, %r8d
	jne	.L167
.L58:
	call	OPENSSL_sk_new_null@PLT
	leaq	-280(%rbp), %r12
	leaq	.LC0(%rip), %r13
	movl	%ebx, %r15d
	movq	%rax, -392(%rbp)
	testq	%rax, %rax
	jne	.L59
	jmp	.L168
	.p2align 4,,10
	.p2align 3
.L62:
	movl	$360, %edx
	movq	%r13, %rsi
	call	CRYPTO_strdup@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L61
	movq	-392(%rbp), %rdi
	movq	%rax, %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L61
.L59:
	movq	-344(%rbp), %rsi
	movq	%r12, %rdi
	call	OPENSSL_DIR_read@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L62
	movq	%r12, %rdi
	movl	%r15d, %ebx
	call	OPENSSL_DIR_end@PLT
	movq	-392(%rbp), %r15
	movq	%r15, %rdi
	call	OPENSSL_sk_sort@PLT
	movq	%r15, %rdi
	call	OPENSSL_sk_num@PLT
	movl	$0, -284(%rbp)
	movl	%eax, -304(%rbp)
	testl	%eax, %eax
	jle	.L100
	leal	-1(%rbx), %eax
	andl	$-3, %ebx
	xorl	%esi, %esi
	movl	%eax, -328(%rbp)
	movslq	-308(%rbp), %rax
	movl	%ebx, -320(%rbp)
	xorl	%ebx, %ebx
	movq	%rax, -336(%rbp)
	leaq	-272(%rbp), %rax
	movq	%rax, -352(%rbp)
	jmp	.L77
.L68:
	movl	$46, %esi
	movq	%r13, %rdi
	call	strrchr@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L66
	addq	$1, %r12
	leaq	.LC16(%rip), %rdi
	movq	%r12, %rsi
	call	strcasecmp@PLT
	testl	%eax, %eax
	je	.L69
	movq	%r12, %rsi
	leaq	.LC17(%rip), %rdi
	call	strcasecmp@PLT
	testl	%eax, %eax
	je	.L69
	movq	%r12, %rsi
	leaq	.LC18(%rip), %rdi
	call	strcasecmp@PLT
	testl	%eax, %eax
	je	.L69
	movq	%r12, %rsi
	leaq	.LC19(%rip), %rdi
	xorl	%r12d, %r12d
	call	strcasecmp@PLT
	testl	%eax, %eax
	je	.L69
.L66:
	movq	X509_INFO_free@GOTPCREL(%rip), %rsi
	movq	%r12, %rdi
	call	OPENSSL_sk_pop_free@PLT
.L64:
	movl	-284(%rbp), %eax
	leal	1(%rax), %esi
	movl	%esi, -284(%rbp)
	cmpl	-304(%rbp), %esi
	jge	.L169
.L77:
	movq	-392(%rbp), %rdi
	call	OPENSSL_sk_value@PLT
	movq	-360(%rbp), %r8
	movq	%r14, %rdi
	movq	-344(%rbp), %rcx
	movq	-336(%rbp), %rsi
	movq	%rax, %r13
	movq	%rax, %r9
	xorl	%eax, %eax
	leaq	.LC15(%rip), %rdx
	call	BIO_snprintf@PLT
	cmpl	-308(%rbp), %eax
	jge	.L64
	movq	-352(%rbp), %rdx
	movq	%r14, %rsi
	movl	$1, %edi
	call	__lxstat@PLT
	testl	%eax, %eax
	js	.L64
	movl	-248(%rbp), %eax
	andl	$61440, %eax
	cmpl	$40960, %eax
	jne	.L68
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	handle_symlink
	testl	%eax, %eax
	jne	.L68
	jmp	.L64
.L61:
	movl	$362, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rbx, %rdi
	call	CRYPTO_free@PLT
	movq	bio_err(%rip), %rdi
	leaq	.LC14(%rip), %rsi
	call	BIO_puts@PLT
	movl	$1, -368(%rbp)
.L60:
	movq	-392(%rbp), %rdi
	leaq	str_free(%rip), %rsi
	call	OPENSSL_sk_pop_free@PLT
	movl	$452, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r14, %rdi
	call	CRYPTO_free@PLT
.L54:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L170
	movl	-368(%rbp), %eax
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L166:
	.cfi_restore_state
	leaq	.LC9(%rip), %rsi
	cmpb	$0, (%r14)
	movq	%rsi, -360(%rbp)
	je	.L57
	cmpb	$47, -1(%r14,%rax)
	leaq	.LC8(%rip), %rdx
	cmovne	%rsi, %rdx
	movq	%rdx, -360(%rbp)
	jmp	.L57
.L69:
	leaq	.LC7(%rip), %rsi
	movq	%r14, %rdi
	call	BIO_new_file@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L171
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	PEM_X509_INFO_read_bio@PLT
	movq	%r15, %rdi
	movq	%rax, %r12
	call	BIO_free@PLT
	testq	%r12, %r12
	je	.L66
	movq	%r12, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	$1, %eax
	jne	.L172
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	OPENSSL_sk_value@PLT
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L72
	movq	%rax, -296(%rbp)
	leaq	-128(%rbp), %r15
	call	X509_get_subject_name@PLT
	movq	-296(%rbp), %r8
	xorl	%ecx, %ecx
	movq	%r15, %rdx
	movq	evpmd(%rip), %rsi
	movq	%rax, -368(%rbp)
	movq	(%r8), %rdi
	call	X509_digest@PLT
	movq	-368(%rbp), %r10
	movl	$0, -296(%rbp)
	testl	%eax, %eax
	je	.L163
.L73:
	testq	%r10, %r10
	je	.L66
	xorl	%r11d, %r11d
	cmpl	$1, -328(%rbp)
	jbe	.L173
.L75:
	movl	-320(%rbp), %edi
	testl	%edi, %edi
	je	.L76
.L162:
	addl	%r11d, %ebx
	jmp	.L66
.L169:
	movl	%ebx, -368(%rbp)
.L63:
	leaq	hash_table(%rip), %rax
	movq	%r14, -296(%rbp)
	movq	%rax, -384(%rbp)
.L95:
	movq	-384(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -320(%rbp)
	testq	%rax, %rax
	je	.L78
	leaq	-128(%rbp), %rax
	movq	%rax, -304(%rbp)
	leaq	-284(%rbp), %rax
	movq	%rax, -352(%rbp)
	.p2align 4,,10
	.p2align 3
.L94:
	movq	-320(%rbp), %rax
	movq	-304(%rbp), %rdi
	movl	$32, %ecx
	xorl	%esi, %esi
	movq	%rax, %r15
	movq	(%rax), %rax
	movzwl	30(%r15), %edx
	movq	%rax, -320(%rbp)
	movl	%edx, %ebx
	addl	$7, %edx
	sarl	$3, %edx
	movslq	%edx, %rdx
	call	__memset_chk@PLT
	movq	8(%r15), %r13
	testq	%r13, %r13
	je	.L79
	movq	%r13, %rax
	.p2align 4,,10
	.p2align 3
.L83:
	movzwl	16(%rax), %ecx
	cmpw	%cx, %bx
	jbe	.L80
	movq	%rcx, %rdx
	movq	(%rax), %rax
	andl	$7, %ecx
	movl	$1, %esi
	movq	-304(%rbp), %rdi
	salq	$48, %rdx
	sall	%cl, %esi
	shrq	$51, %rdx
	orb	%sil, (%rdi,%rdx)
	testq	%rax, %rax
	jne	.L83
.L82:
	xorl	%r12d, %r12d
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L84:
	cmpb	$0, 18(%r13)
	jne	.L174
	movl	remove_links(%rip), %edx
	testl	%edx, %edx
	jne	.L175
.L85:
	movq	8(%r13), %rdi
	movl	$442, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movl	$443, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r13, %rdi
	call	CRYPTO_free@PLT
	testq	%r14, %r14
	je	.L79
	movzwl	30(%r15), %ebx
	movq	%r14, %r13
.L93:
	movzwl	16(%r13), %r9d
	movq	0(%r13), %r14
	cmpw	%bx, %r9w
	jnb	.L84
	movzwl	28(%r15), %eax
	leaq	suffixes(%rip), %rdi
	movl	24(%r15), %ecx
	leaq	.LC22(%rip), %rdx
	movslq	-308(%rbp), %rsi
	movq	(%rdi,%rax,8), %r8
	movq	-296(%rbp), %rdi
	xorl	%eax, %eax
	call	BIO_snprintf@PLT
	movl	verbose(%rip), %esi
	testl	%esi, %esi
	je	.L85
	movq	8(%r13), %rdx
	movq	-296(%rbp), %rcx
	leaq	.LC23(%rip), %rsi
	xorl	%eax, %eax
	movq	bio_out(%rip), %rdi
	call	BIO_printf@PLT
	jmp	.L85
	.p2align 4,,10
	.p2align 3
.L80:
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.L83
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L174:
	movl	%r12d, %ebx
	movq	-304(%rbp), %rdx
	shrl	$3, %ebx
	jmp	.L164
	.p2align 4,,10
	.p2align 3
.L176:
	addl	$1, %r12d
	movl	%r12d, %ebx
	shrl	$3, %ebx
.L164:
	addq	%rdx, %rbx
	movl	%r12d, %ecx
	movl	$1, %r11d
	andl	$7, %ecx
	movzbl	(%rbx), %eax
	sall	%cl, %r11d
	testl	%r11d, %eax
	jne	.L176
	movb	%al, -328(%rbp)
	movzwl	28(%r15), %eax
	subq	$8, %rsp
	leaq	suffixes(%rip), %rdi
	pushq	%r12
	movq	-344(%rbp), %rcx
	leaq	.LC24(%rip), %rdx
	pushq	(%rdi,%rax,8)
	movl	24(%r15), %eax
	movslq	-308(%rbp), %rsi
	movl	%r11d, -336(%rbp)
	movq	-352(%rbp), %r9
	movq	-360(%rbp), %r8
	pushq	%rax
	xorl	%eax, %eax
	movq	-296(%rbp), %rdi
	call	BIO_snprintf@PLT
	movl	verbose(%rip), %ecx
	addq	$32, %rsp
	movzbl	-328(%rbp), %r10d
	movl	-336(%rbp), %r11d
	testl	%ecx, %ecx
	jne	.L177
.L89:
	movq	-296(%rbp), %rdi
	movl	%r11d, -336(%rbp)
	movb	%r10b, -328(%rbp)
	call	unlink@PLT
	movzbl	-328(%rbp), %r10d
	movl	-336(%rbp), %r11d
	testl	%eax, %eax
	js	.L178
.L90:
	movq	8(%r13), %rdi
	movq	-296(%rbp), %rsi
	movl	%r11d, -336(%rbp)
	movb	%r10b, -328(%rbp)
	call	symlink@PLT
	movzbl	-328(%rbp), %r10d
	movl	-336(%rbp), %r11d
	testl	%eax, %eax
	js	.L179
.L91:
	orl	%r11d, %r10d
	movb	%r10b, (%rbx)
	jmp	.L85
	.p2align 4,,10
	.p2align 3
.L175:
	movzwl	28(%r15), %eax
	subq	$8, %rsp
	leaq	suffixes(%rip), %rdi
	movslq	-308(%rbp), %rsi
	pushq	%r9
	movq	-360(%rbp), %r8
	leaq	.LC24(%rip), %rdx
	pushq	(%rdi,%rax,8)
	movl	24(%r15), %eax
	movq	-352(%rbp), %r9
	movq	-344(%rbp), %rcx
	movq	-296(%rbp), %rdi
	pushq	%rax
	xorl	%eax, %eax
	call	BIO_snprintf@PLT
	movl	verbose(%rip), %eax
	addq	$32, %rsp
	testl	%eax, %eax
	jne	.L180
.L92:
	movq	-296(%rbp), %rdi
	call	unlink@PLT
	testl	%eax, %eax
	jns	.L85
	call	__errno_location@PLT
	movl	(%rax), %edi
	cmpl	$2, %edi
	je	.L85
	call	strerror@PLT
	movq	%rax, %rbx
	call	opt_getprog@PLT
	movq	-296(%rbp), %rcx
	movq	%rbx, %r8
	movq	bio_err(%rip), %rdi
	movq	%rax, %rdx
	leaq	.LC25(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	addl	$1, -368(%rbp)
	jmp	.L85
	.p2align 4,,10
	.p2align 3
.L79:
	movl	$445, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r15, %rdi
	call	CRYPTO_free@PLT
	cmpq	$0, -320(%rbp)
	jne	.L94
.L78:
	movq	-384(%rbp), %rax
	leaq	2056+hash_table(%rip), %rsi
	movq	$0, (%rax)
	addq	$8, %rax
	movq	%rax, -384(%rbp)
	cmpq	%rax, %rsi
	jne	.L95
	movq	-296(%rbp), %r14
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L178:
	call	__errno_location@PLT
	movzbl	-328(%rbp), %r10d
	movl	-336(%rbp), %r11d
	movl	(%rax), %edi
	cmpl	$2, %edi
	je	.L90
	movl	%r11d, -312(%rbp)
	movb	%r10b, -336(%rbp)
	call	strerror@PLT
	movq	%rax, -328(%rbp)
	call	opt_getprog@PLT
	movq	-328(%rbp), %r8
	movq	-296(%rbp), %rcx
	leaq	.LC25(%rip), %rsi
	movq	bio_err(%rip), %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	addl	$1, -368(%rbp)
	movl	-312(%rbp), %r11d
	movzbl	-336(%rbp), %r10d
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L177:
	movq	8(%r13), %rdx
	movq	bio_out(%rip), %rdi
	leaq	.LC23(%rip), %rsi
	xorl	%eax, %eax
	movslq	-284(%rbp), %rcx
	addq	-296(%rbp), %rcx
	call	BIO_printf@PLT
	movl	-336(%rbp), %r11d
	movzbl	-328(%rbp), %r10d
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L180:
	movq	bio_out(%rip), %rdi
	movslq	-284(%rbp), %rdx
	leaq	.LC27(%rip), %rsi
	xorl	%eax, %eax
	addq	-296(%rbp), %rdx
	call	BIO_printf@PLT
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L179:
	movl	%r11d, -372(%rbp)
	movb	%r10b, -312(%rbp)
	call	__errno_location@PLT
	movl	(%rax), %edi
	call	strerror@PLT
	movq	8(%r13), %rcx
	movq	%rax, -336(%rbp)
	movq	%rcx, -328(%rbp)
	call	opt_getprog@PLT
	movq	-336(%rbp), %r8
	movq	-328(%rbp), %rcx
	leaq	.LC26(%rip), %rsi
	movq	bio_err(%rip), %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	addl	$1, -368(%rbp)
	movl	-372(%rbp), %r11d
	movzbl	-312(%rbp), %r10d
	jmp	.L91
.L167:
	movq	-344(%rbp), %rdx
	movq	bio_out(%rip), %rdi
	leaq	.LC12(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L58
.L165:
	movq	-344(%rbp), %rdx
	movq	bio_err(%rip), %rdi
	leaq	.LC10(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movl	$1, -368(%rbp)
	jmp	.L54
.L172:
	call	opt_getprog@PLT
	movq	bio_err(%rip), %rdi
	movq	%r13, %rcx
	leaq	.LC21(%rip), %rsi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L66
.L171:
	call	opt_getprog@PLT
	movq	%r13, %rcx
	addl	$1, %ebx
	xorl	%r12d, %r12d
	movq	bio_err(%rip), %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	leaq	.LC20(%rip), %rsi
	call	BIO_printf@PLT
	jmp	.L66
.L168:
	movq	-344(%rbp), %rdx
	movq	bio_err(%rip), %rdi
	leaq	.LC13(%rip), %rsi
	call	BIO_printf@PLT
	movl	$1, -368(%rbp)
	jmp	.L60
.L72:
	movq	8(%rax), %rdi
	testq	%rdi, %rdi
	je	.L161
	movq	%rax, -296(%rbp)
	leaq	-128(%rbp), %r15
	call	X509_CRL_get_issuer@PLT
	movq	-296(%rbp), %r8
	xorl	%ecx, %ecx
	movq	%r15, %rdx
	movq	evpmd(%rip), %rsi
	movq	%rax, -368(%rbp)
	movq	8(%r8), %rdi
	call	X509_CRL_digest@PLT
	movq	-368(%rbp), %r10
	movl	$1, -296(%rbp)
	testl	%eax, %eax
	jne	.L73
.L163:
	movq	bio_err(%rip), %rdi
	leaq	.LC14(%rip), %rsi
	call	BIO_printf@PLT
.L161:
	addl	$1, %ebx
	jmp	.L66
.L76:
	movq	%r10, %rdi
	movl	%r11d, -368(%rbp)
	call	X509_NAME_hash_old@PLT
	movl	-296(%rbp), %edi
	movq	%r15, %rcx
	movq	%r13, %rdx
	movq	%rax, %rsi
	movl	$65535, %r9d
	movl	$1, %r8d
	call	add_entry
	movl	-368(%rbp), %r11d
	addl	%eax, %r11d
	jmp	.L162
.L100:
	movl	$0, -368(%rbp)
	jmp	.L63
.L173:
	movq	%r10, %rdi
	movq	%r10, -368(%rbp)
	call	X509_NAME_hash@PLT
	movl	-296(%rbp), %edi
	movq	%r15, %rcx
	movq	%r13, %rdx
	movq	%rax, %rsi
	movl	$65535, %r9d
	movl	$1, %r8d
	call	add_entry
	movq	-368(%rbp), %r10
	movl	%eax, %r11d
	jmp	.L75
.L170:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1444:
	.size	do_dir, .-do_dir
	.section	.rodata.str1.1
.LC28:
	.string	"%s: Use -help for summary.\n"
	.text
	.p2align 4
	.globl	rehash_main
	.type	rehash_main, @function
rehash_main:
.LFB1445:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	rehash_options(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	$1, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	leaq	.L185(%rip), %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	opt_init@PLT
	movq	%rax, %r14
.L182:
	call	opt_next@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	je	.L205
.L192:
	addl	$1, %r12d
	cmpl	$6, %r12d
	ja	.L182
	movslq	(%rbx,%r12,4), %rax
	addq	%rbx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L185:
	.long	.L190-.L185
	.long	.L182-.L185
	.long	.L189-.L185
	.long	.L188-.L185
	.long	.L187-.L185
	.long	.L186-.L185
	.long	.L184-.L185
	.text
.L188:
	call	opt_next@PLT
	movl	$2, %r13d
	movl	%eax, %r12d
	testl	%eax, %eax
	jne	.L192
.L205:
	call	opt_num_rest@PLT
	call	opt_rest@PLT
	movq	%rax, %rbx
	call	EVP_sha1@PLT
	movq	%rax, %rdi
	movq	%rax, evpmd(%rip)
	call	EVP_MD_size@PLT
	movq	(%rbx), %rdi
	movl	%eax, evpmdsize(%rip)
	testq	%rdi, %rdi
	je	.L193
	.p2align 4,,10
	.p2align 3
.L194:
	addq	$8, %rbx
	movl	%r13d, %esi
	call	do_dir
	movq	(%rbx), %rdi
	addl	%eax, %r12d
	testq	%rdi, %rdi
	jne	.L194
	jmp	.L181
.L184:
	movl	$1, verbose(%rip)
	jmp	.L182
.L186:
	movl	$0, remove_links(%rip)
	jmp	.L182
.L187:
	xorl	%r13d, %r13d
	jmp	.L182
.L189:
	leaq	rehash_options(%rip), %rdi
	xorl	%r12d, %r12d
	call	opt_help@PLT
.L181:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L206
	addq	$16, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L190:
	.cfi_restore_state
	movq	bio_err(%rip), %rdi
	movq	%r14, %rdx
	xorl	%eax, %eax
	xorl	%r12d, %r12d
	leaq	.LC28(%rip), %rsi
	call	BIO_printf@PLT
	jmp	.L181
.L193:
	call	X509_get_default_cert_dir_env@PLT
	movq	%rax, %rdi
	call	getenv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L195
	movl	$58, %eax
	movl	$517, %edx
	leaq	.LC0(%rip), %rsi
	movw	%ax, -42(%rbp)
	leaq	-42(%rbp), %rbx
	call	CRYPTO_strdup@PLT
	movq	%rbx, %rsi
	movq	%rax, %rdi
	movq	%rax, %r14
	call	strtok@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L196
	.p2align 4,,10
	.p2align 3
.L197:
	movl	%r13d, %esi
	call	do_dir
	xorl	%edi, %edi
	movq	%rbx, %rsi
	addl	%eax, %r12d
	call	strtok@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L197
.L196:
	movl	$520, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r14, %rdi
	call	CRYPTO_free@PLT
	jmp	.L181
.L195:
.L191:
	endbr64
	call	X509_get_default_cert_dir@PLT
	movl	%r13d, %esi
	movq	%rax, %rdi
	call	do_dir
	movl	%eax, %r12d
	jmp	.L181
.L206:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1445:
	.size	rehash_main, .-rehash_main
	.section	.rodata
	.align 32
	.type	nilhentry.24651, @object
	.size	nilhentry.24651, 88
nilhentry.24651:
	.zero	88
	.align 32
	.type	nilbucket.24650, @object
	.size	nilbucket.24650, 32
nilbucket.24650:
	.zero	32
	.globl	rehash_options
	.section	.rodata.str1.8
	.align 8
.LC29:
	.string	"Usage: %s [options] [cert-directory...]\n"
	.section	.rodata.str1.1
.LC30:
	.string	"Valid options are:\n"
.LC31:
	.string	"help"
.LC32:
	.string	"Display this summary"
.LC33:
	.string	"h"
.LC34:
	.string	"compat"
	.section	.rodata.str1.8
	.align 8
.LC35:
	.string	"Create both new- and old-style hash links"
	.section	.rodata.str1.1
.LC36:
	.string	"old"
	.section	.rodata.str1.8
	.align 8
.LC37:
	.string	"Use old-style hash to generate links"
	.section	.rodata.str1.1
.LC38:
	.string	"n"
.LC39:
	.string	"Do not remove existing links"
.LC40:
	.string	"v"
.LC41:
	.string	"Verbose output"
	.section	.data.rel.ro,"aw"
	.align 32
	.type	rehash_options, @object
	.size	rehash_options, 216
rehash_options:
	.quad	OPT_HELP_STR
	.long	1
	.long	45
	.quad	.LC29
	.quad	OPT_HELP_STR
	.long	1
	.long	45
	.quad	.LC30
	.quad	.LC31
	.long	1
	.long	45
	.quad	.LC32
	.quad	.LC33
	.long	1
	.long	45
	.quad	.LC32
	.quad	.LC34
	.long	2
	.long	45
	.quad	.LC35
	.quad	.LC36
	.long	3
	.long	45
	.quad	.LC37
	.quad	.LC38
	.long	4
	.long	45
	.quad	.LC39
	.quad	.LC40
	.long	5
	.long	45
	.quad	.LC41
	.quad	0
	.zero	16
	.section	.data.rel.ro.local,"aw"
	.align 16
	.type	suffixes, @object
	.size	suffixes, 16
suffixes:
	.quad	.LC8
	.quad	.LC7
	.local	hash_table
	.comm	hash_table,2056,32
	.local	verbose
	.comm	verbose,4,4
	.data
	.align 4
	.type	remove_links, @object
	.size	remove_links, 4
remove_links:
	.long	1
	.local	evpmd
	.comm	evpmd,8,8
	.local	evpmdsize
	.comm	evpmdsize,4,4
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
