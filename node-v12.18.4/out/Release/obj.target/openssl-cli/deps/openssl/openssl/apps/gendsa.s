	.file	"gendsa.c"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"%s: Use -help for summary.\n"
.LC1:
	.string	"Error getting password\n"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"unable to load DSA parameter file\n"
	.align 8
.LC3:
	.string	"Warning: It is not recommended to use more than %d bit for DSA keys.\n         Your key size is %d! Larger key size may behave not as expected.\n"
	.section	.rodata.str1.1
.LC4:
	.string	"Generating DSA key, %d bits\n"
	.section	.rodata.str1.8
	.align 8
.LC5:
	.string	"../deps/openssl/openssl/apps/gendsa.c"
	.text
	.p2align 4
	.globl	gendsa_main
	.type	gendsa_main, @function
gendsa_main:
.LFB1435:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	gendsa_options(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	xorl	%r14d, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	xorl	%r13d, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	.L6(%rip), %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movq	$0, -72(%rbp)
	movq	$0, -64(%rbp)
	call	opt_init@PLT
	movq	%rax, %r12
.L28:
	call	opt_next@PLT
	movl	%eax, %edi
	testl	%eax, %eax
	je	.L36
	cmpl	$5, %edi
	jg	.L3
	cmpl	$-1, %edi
	jl	.L28
	addl	$1, %edi
	cmpl	$6, %edi
	ja	.L28
	movslq	(%rbx,%rdi,4), %rax
	addq	%rbx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L6:
	.long	.L11-.L6
	.long	.L28-.L6
	.long	.L10-.L6
	.long	.L9-.L6
	.long	.L8-.L6
	.long	.L7-.L6
	.long	.L5-.L6
	.text
	.p2align 4,,10
	.p2align 3
.L36:
	call	opt_num_rest@PLT
	movl	%eax, %ebx
	call	opt_rest@PLT
	cmpl	$1, %ebx
	je	.L37
.L11:
	movq	bio_err(%rip), %rdi
	movq	%r12, %rdx
	xorl	%eax, %eax
	xorl	%r14d, %r14d
	leaq	.LC0(%rip), %rsi
	xorl	%r15d, %r15d
	xorl	%r12d, %r12d
	call	BIO_printf@PLT
.L13:
	movq	bio_err(%rip), %rdi
	movl	$1, %ebx
	call	ERR_print_errors@PLT
.L14:
	movq	%r15, %rdi
	call	BIO_free@PLT
	movq	%r12, %rdi
	call	BIO_free_all@PLT
	movq	%r14, %rdi
	call	DSA_free@PLT
	movq	%r13, %rdi
	call	release_engine@PLT
	movq	-72(%rbp), %rdi
	movl	$139, %edx
	leaq	.LC5(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L38
	leaq	-40(%rbp), %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	.cfi_restore_state
	leal	-1501(%rdi), %eax
	cmpl	$1, %eax
	ja	.L28
	call	opt_rand@PLT
	testl	%eax, %eax
	jne	.L28
.L34:
	xorl	%r14d, %r14d
	xorl	%r15d, %r15d
	xorl	%r12d, %r12d
	jmp	.L13
.L5:
	call	opt_unknown@PLT
	leaq	-80(%rbp), %rsi
	movq	%rax, %rdi
	call	opt_cipher@PLT
	testl	%eax, %eax
	jne	.L28
	jmp	.L34
.L7:
	call	opt_arg@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	setup_engine@PLT
	movq	%rax, %r13
	jmp	.L28
.L8:
	call	opt_arg@PLT
	movq	%rax, %r14
	jmp	.L28
.L9:
	call	opt_arg@PLT
	movq	%rax, %r15
	jmp	.L28
.L10:
	leaq	gendsa_options(%rip), %rdi
	xorl	%ebx, %ebx
	xorl	%r14d, %r14d
	xorl	%r15d, %r15d
	call	opt_help@PLT
	xorl	%r12d, %r12d
	jmp	.L14
.L37:
	xorl	%edx, %edx
	xorl	%edi, %edi
	leaq	-72(%rbp), %rcx
	movq	%r14, %rsi
	movq	(%rax), %r12
	call	app_passwd@PLT
	testl	%eax, %eax
	je	.L39
	movq	%r12, %rdi
	movl	$32773, %edx
	movl	$114, %esi
	call	bio_open_default@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L22
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	PEM_read_bio_DSAparams@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L40
	movq	%r12, %rdi
	call	BIO_free@PLT
	movq	%r15, %rdi
	movl	$1, %edx
	xorl	%r15d, %r15d
	movl	$32773, %esi
	call	bio_open_owner@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L14
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	leaq	-64(%rbp), %rsi
	movq	%r14, %rdi
	call	DSA_get0_pqg@PLT
	movq	-64(%rbp), %rdi
	call	BN_num_bits@PLT
	cmpl	$10000, %eax
	jg	.L41
.L20:
	movq	-64(%rbp), %rdi
	xorl	%r15d, %r15d
	call	BN_num_bits@PLT
	movq	bio_err(%rip), %rdi
	leaq	.LC4(%rip), %rsi
	movl	%eax, %edx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	%r14, %rdi
	call	DSA_generate_key@PLT
	testl	%eax, %eax
	je	.L13
	subq	$8, %rsp
	pushq	-72(%rbp)
	movq	-80(%rbp), %rdx
	xorl	%ecx, %ecx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movq	%r12, %rdi
	xorl	%ebx, %ebx
	call	PEM_write_bio_DSAPrivateKey@PLT
	popq	%rdx
	popq	%rcx
	testl	%eax, %eax
	jne	.L14
	jmp	.L13
.L39:
	movq	bio_err(%rip), %rdi
	leaq	.LC1(%rip), %rsi
	call	BIO_printf@PLT
	jmp	.L34
.L22:
	xorl	%r14d, %r14d
	xorl	%r15d, %r15d
	jmp	.L14
.L41:
	movq	-64(%rbp), %rdi
	call	BN_num_bits@PLT
	movq	bio_err(%rip), %rdi
	movl	$10000, %edx
	leaq	.LC3(%rip), %rsi
	movl	%eax, %ecx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L20
.L40:
	movq	bio_err(%rip), %rdi
	leaq	.LC2(%rip), %rsi
	xorl	%eax, %eax
	movq	%r12, %r15
	xorl	%r12d, %r12d
	call	BIO_printf@PLT
	jmp	.L13
.L38:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1435:
	.size	gendsa_main, .-gendsa_main
	.globl	gendsa_options
	.section	.rodata.str1.8
	.align 8
.LC6:
	.string	"Usage: %s [args] dsaparam-file\n"
	.section	.rodata.str1.1
.LC7:
	.string	"Valid options are:\n"
.LC8:
	.string	"help"
.LC9:
	.string	"Display this summary"
.LC10:
	.string	"out"
	.section	.rodata.str1.8
	.align 8
.LC11:
	.string	"Output the key to the specified file"
	.section	.rodata.str1.1
.LC12:
	.string	"passout"
	.section	.rodata.str1.8
	.align 8
.LC13:
	.string	"Output file pass phrase source"
	.section	.rodata.str1.1
.LC14:
	.string	"rand"
	.section	.rodata.str1.8
	.align 8
.LC15:
	.string	"Load the file(s) into the random number generator"
	.section	.rodata.str1.1
.LC16:
	.string	"writerand"
	.section	.rodata.str1.8
	.align 8
.LC17:
	.string	"Write random data to the specified file"
	.section	.rodata.str1.1
.LC18:
	.string	""
	.section	.rodata.str1.8
	.align 8
.LC19:
	.string	"Encrypt the output with any supported cipher"
	.section	.rodata.str1.1
.LC20:
	.string	"engine"
	.section	.rodata.str1.8
	.align 8
.LC21:
	.string	"Use engine, possibly a hardware device"
	.section	.data.rel.ro,"aw"
	.align 32
	.type	gendsa_options, @object
	.size	gendsa_options, 240
gendsa_options:
	.quad	OPT_HELP_STR
	.long	1
	.long	45
	.quad	.LC6
	.quad	OPT_HELP_STR
	.long	1
	.long	45
	.quad	.LC7
	.quad	.LC8
	.long	1
	.long	45
	.quad	.LC9
	.quad	.LC10
	.long	2
	.long	62
	.quad	.LC11
	.quad	.LC12
	.long	3
	.long	115
	.quad	.LC13
	.quad	.LC14
	.long	1501
	.long	115
	.quad	.LC15
	.quad	.LC16
	.long	1502
	.long	62
	.quad	.LC17
	.quad	.LC18
	.long	5
	.long	45
	.quad	.LC19
	.quad	.LC20
	.long	4
	.long	115
	.quad	.LC21
	.quad	0
	.zero	16
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
