	.file	"rand.c"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"%s: Use -help for summary.\n"
.LC1:
	.string	"Extra arguments given.\n"
.LC2:
	.string	"%02x"
.LC3:
	.string	"\n"
	.text
	.p2align 4
	.globl	rand_main
	.type	rand_main, @function
rand_main:
.LFB1437:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	rand_options(%rip), %rdx
	movl	$2, %ebx
	xorl	%r12d, %r12d
	leaq	.L6(%rip), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$-1, -4164(%rbp)
	call	opt_init@PLT
	movq	$0, -4184(%rbp)
	movq	%rax, %r14
.L2:
	call	opt_next@PLT
	movl	%eax, %r15d
	testl	%eax, %eax
	je	.L44
.L15:
	cmpl	$5, %r15d
	jg	.L3
	cmpl	$-1, %r15d
	jl	.L2
	leal	1(%r15), %r8d
	cmpl	$6, %r8d
	ja	.L2
	movslq	0(%r13,%r8,4), %rax
	addq	%r13, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L6:
	.long	.L11-.L6
	.long	.L2-.L6
	.long	.L10-.L6
	.long	.L9-.L6
	.long	.L8-.L6
	.long	.L7-.L6
	.long	.L29-.L6
	.text
.L29:
	call	opt_next@PLT
	movl	$32769, %ebx
	movl	%eax, %r15d
	testl	%eax, %eax
	jne	.L15
.L44:
	call	opt_num_rest@PLT
	movl	%eax, %r13d
	call	opt_rest@PLT
	cmpl	$1, %r13d
	je	.L45
	testl	%r13d, %r13d
	jg	.L46
.L18:
	movq	-4184(%rbp), %rdi
	movl	%ebx, %edx
	movl	$119, %esi
	call	bio_open_default@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L17
	cmpl	$32771, %ebx
	je	.L47
	movl	-4164(%rbp), %eax
	testl	%eax, %eax
	jle	.L26
.L27:
	leaq	-4160(%rbp), %rcx
	movq	%rcx, -4200(%rbp)
.L25:
	cmpl	$4096, %eax
	movl	$4096, %r14d
	movq	-4200(%rbp), %rdi
	cmovle	%eax, %r14d
	movl	%r14d, %esi
	call	RAND_bytes@PLT
	testl	%eax, %eax
	jle	.L13
	cmpl	$32769, %ebx
	je	.L22
	movq	-4200(%rbp), %rsi
	movl	%r14d, %edx
	movq	%r13, %rdi
	call	BIO_write@PLT
	cmpl	%r14d, %eax
	jne	.L13
.L23:
	movl	-4164(%rbp), %eax
	subl	%r14d, %eax
	movl	%eax, -4164(%rbp)
	testl	%eax, %eax
	jg	.L25
.L26:
	cmpl	$32769, %ebx
	jne	.L20
	leaq	.LC3(%rip), %rsi
	movq	%r13, %rdi
	call	BIO_puts@PLT
	jmp	.L20
.L7:
	movl	$32771, %ebx
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L3:
	leal	-1501(%r15), %eax
	cmpl	$1, %eax
	ja	.L2
	movl	%r15d, %edi
	call	opt_rand@PLT
	testl	%eax, %eax
	jne	.L2
.L17:
	xorl	%r13d, %r13d
	jmp	.L13
.L46:
	movq	bio_err(%rip), %rdi
	leaq	.LC1(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L11:
	movq	bio_err(%rip), %rdi
	movq	%r14, %rdx
	xorl	%eax, %eax
	xorl	%r13d, %r13d
	leaq	.LC0(%rip), %rsi
	call	BIO_printf@PLT
.L13:
	movq	bio_err(%rip), %rdi
	movl	$1, %r15d
	call	ERR_print_errors@PLT
.L14:
	movq	%r12, %rdi
	call	release_engine@PLT
	movq	%r13, %rdi
	call	BIO_free_all@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L48
	addq	$4168, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L8:
	.cfi_restore_state
	call	opt_arg@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	setup_engine@PLT
	movq	%rax, %r12
	jmp	.L2
.L9:
	call	opt_arg@PLT
	movq	%rax, -4184(%rbp)
	jmp	.L2
.L10:
	leaq	rand_options(%rip), %rdi
	xorl	%r15d, %r15d
	xorl	%r13d, %r13d
	call	opt_help@PLT
	jmp	.L14
.L47:
	call	BIO_f_base64@PLT
	movq	%rax, %rdi
	call	BIO_new@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L13
	movq	%r13, %rsi
	call	BIO_push@PLT
	movq	%rax, %r13
	movl	-4164(%rbp), %eax
	testl	%eax, %eax
	jg	.L27
.L20:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	movq	%r13, %rdi
	call	BIO_ctrl@PLT
	testl	%eax, %eax
	jg	.L14
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L22:
	leal	-1(%r14), %eax
	movq	-4200(%rbp), %rcx
	leaq	-4159(%rbp,%rax), %rax
	movq	%rax, -4192(%rbp)
	.p2align 4,,10
	.p2align 3
.L24:
	movzbl	(%rcx), %edx
	xorl	%eax, %eax
	leaq	.LC2(%rip), %rsi
	movq	%r13, %rdi
	movq	%rcx, -4184(%rbp)
	call	BIO_printf@PLT
	cmpl	$2, %eax
	jne	.L13
	movq	-4184(%rbp), %rcx
	addq	$1, %rcx
	cmpq	-4192(%rbp), %rcx
	jne	.L24
	jmp	.L23
.L45:
	movq	(%rax), %rdi
	leaq	-4164(%rbp), %rsi
	call	opt_int@PLT
	testl	%eax, %eax
	je	.L17
	movl	-4164(%rbp), %eax
	testl	%eax, %eax
	jg	.L18
	jmp	.L17
.L48:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1437:
	.size	rand_main, .-rand_main
	.globl	rand_options
	.section	.rodata.str1.1
.LC4:
	.string	"Usage: %s [flags] num\n"
.LC5:
	.string	"Valid options are:\n"
.LC6:
	.string	"help"
.LC7:
	.string	"Display this summary"
.LC8:
	.string	"out"
.LC9:
	.string	"Output file"
.LC10:
	.string	"rand"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC11:
	.string	"Load the file(s) into the random number generator"
	.section	.rodata.str1.1
.LC12:
	.string	"writerand"
	.section	.rodata.str1.8
	.align 8
.LC13:
	.string	"Write random data to the specified file"
	.section	.rodata.str1.1
.LC14:
	.string	"base64"
.LC15:
	.string	"Base64 encode output"
.LC16:
	.string	"hex"
.LC17:
	.string	"Hex encode output"
.LC18:
	.string	"engine"
	.section	.rodata.str1.8
	.align 8
.LC19:
	.string	"Use engine, possibly a hardware device"
	.section	.data.rel.ro,"aw"
	.align 32
	.type	rand_options, @object
	.size	rand_options, 240
rand_options:
	.quad	OPT_HELP_STR
	.long	1
	.long	45
	.quad	.LC4
	.quad	OPT_HELP_STR
	.long	1
	.long	45
	.quad	.LC5
	.quad	.LC6
	.long	1
	.long	45
	.quad	.LC7
	.quad	.LC8
	.long	2
	.long	62
	.quad	.LC9
	.quad	.LC10
	.long	1501
	.long	115
	.quad	.LC11
	.quad	.LC12
	.long	1502
	.long	62
	.quad	.LC13
	.quad	.LC14
	.long	4
	.long	45
	.quad	.LC15
	.quad	.LC16
	.long	5
	.long	45
	.quad	.LC17
	.quad	.LC18
	.long	3
	.long	115
	.quad	.LC19
	.quad	0
	.zero	16
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
