	.file	"spkac.c"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"default"
.LC1:
	.string	"SPKAC"
.LC2:
	.string	"%s: Use -help for summary.\n"
.LC3:
	.string	"Error getting password\n"
.LC4:
	.string	"private key"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC5:
	.string	"../deps/openssl/openssl/apps/spkac.c"
	.section	.rodata.str1.1
.LC6:
	.string	"SPKAC=%s\n"
.LC7:
	.string	"Can't find SPKAC called \"%s\"\n"
.LC8:
	.string	"Error loading SPKAC\n"
.LC9:
	.string	"Signature OK\n"
.LC10:
	.string	"Signature Failure\n"
	.text
	.p2align 4
	.globl	spkac_main
	.type	spkac_main, @function
spkac_main:
.LFB1435:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	spkac_options(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	xorl	%r14d, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	.L5(%rip), %rbx
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -64(%rbp)
	movl	$32773, -68(%rbp)
	call	opt_init@PLT
	movl	$0, -120(%rbp)
	movq	%rax, %r13
	leaq	.LC0(%rip), %rax
	movl	$0, -116(%rbp)
	movq	%rax, -136(%rbp)
	leaq	.LC1(%rip), %rax
	movq	%rax, -112(%rbp)
	movq	$0, -88(%rbp)
	movq	$0, -104(%rbp)
	movq	$0, -128(%rbp)
	movq	$0, -96(%rbp)
.L2:
	call	opt_next@PLT
	testl	%eax, %eax
	je	.L52
.L20:
	addl	$1, %eax
	cmpl	$14, %eax
	ja	.L2
	movslq	(%rbx,%rax,4), %rax
	addq	%rbx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L5:
	.long	.L18-.L5
	.long	.L2-.L5
	.long	.L17-.L5
	.long	.L16-.L5
	.long	.L15-.L5
	.long	.L14-.L5
	.long	.L13-.L5
	.long	.L12-.L5
	.long	.L11-.L5
	.long	.L10-.L5
	.long	.L9-.L5
	.long	.L8-.L5
	.long	.L7-.L5
	.long	.L6-.L5
	.long	.L4-.L5
	.text
.L16:
	movl	$1, %r12d
	call	opt_next@PLT
	testl	%eax, %eax
	jne	.L20
.L52:
	call	opt_num_rest@PLT
	movl	%eax, %ebx
	testl	%eax, %eax
	jne	.L18
	movq	-88(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	leaq	-64(%rbp), %rdx
	call	app_passwd@PLT
	testl	%eax, %eax
	je	.L53
	testq	%r15, %r15
	je	.L23
	cmpb	$45, (%r15)
	movq	-64(%rbp), %rcx
	movl	-68(%rbp), %esi
	je	.L54
.L25:
	leaq	.LC4(%rip), %r9
	movq	%r14, %r8
	movl	$1, %edx
	movq	%r15, %rdi
	call	load_key@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L29
	call	NETSCAPE_SPKI_new@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L36
	movq	-96(%rbp), %r15
	testq	%r15, %r15
	je	.L27
	movq	%r15, %rdi
	call	strlen@PLT
	movq	%r15, %rsi
	movq	%rax, %rdx
	movq	0(%r13), %rax
	movq	8(%rax), %rdi
	call	ASN1_STRING_set@PLT
.L27:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	NETSCAPE_SPKI_set_pubkey@PLT
	call	EVP_md5@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	NETSCAPE_SPKI_sign@PLT
	movq	%r13, %rdi
	call	NETSCAPE_SPKI_b64_encode@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L37
	movq	-104(%rbp), %rdi
	movl	$32769, %edx
	movl	$119, %esi
	call	bio_open_default@PLT
	testq	%rax, %rax
	je	.L55
	movq	%r15, %rdx
	movq	%rax, %rdi
	movq	%rax, -88(%rbp)
	xorl	%eax, %eax
	leaq	.LC6(%rip), %rsi
	call	BIO_printf@PLT
	movq	%r15, %rdi
	xorl	%r15d, %r15d
	movl	$148, %edx
	leaq	.LC5(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-88(%rbp), %r8
	jmp	.L19
.L4:
	call	opt_arg@PLT
	leaq	-68(%rbp), %rdx
	movl	$1982, %esi
	movq	%rax, %rdi
	call	opt_format@PLT
	testl	%eax, %eax
	jne	.L2
.L18:
	movq	bio_err(%rip), %rdi
	movq	%r13, %rdx
	leaq	.LC2(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L29:
	movl	$1, %ebx
	xorl	%r13d, %r13d
	xorl	%r12d, %r12d
	xorl	%r15d, %r15d
	xorl	%r8d, %r8d
.L19:
	movq	%r15, %rdi
	movq	%r8, -88(%rbp)
	call	NCONF_free@PLT
	movq	%r13, %rdi
	call	NETSCAPE_SPKI_free@PLT
	movq	-88(%rbp), %r8
	movq	%r8, %rdi
	call	BIO_free_all@PLT
	movq	%r12, %rdi
	call	EVP_PKEY_free@PLT
	movq	%r14, %rdi
	call	release_engine@PLT
	movq	-64(%rbp), %rdi
	movl	$200, %edx
	leaq	.LC5(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L56
	addq	$104, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L6:
	.cfi_restore_state
	call	opt_arg@PLT
	movq	%rax, -136(%rbp)
	jmp	.L2
.L7:
	call	opt_arg@PLT
	movq	%rax, -112(%rbp)
	jmp	.L2
.L8:
	call	opt_arg@PLT
	movq	%rax, -88(%rbp)
	jmp	.L2
.L9:
	call	opt_arg@PLT
	movq	%rax, -96(%rbp)
	jmp	.L2
.L10:
	call	opt_arg@PLT
	movq	%rax, %r15
	jmp	.L2
.L11:
	call	opt_arg@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	setup_engine@PLT
	movq	%rax, %r14
	jmp	.L2
.L12:
	call	opt_arg@PLT
	movq	%rax, -104(%rbp)
	jmp	.L2
.L13:
	call	opt_arg@PLT
	movq	%rax, -128(%rbp)
	jmp	.L2
.L14:
	movl	$1, -116(%rbp)
	jmp	.L2
.L15:
	movl	$1, -120(%rbp)
	jmp	.L2
.L17:
	leaq	spkac_options(%rip), %rdi
	xorl	%ebx, %ebx
	xorl	%r13d, %r13d
	xorl	%r12d, %r12d
	call	opt_help@PLT
	xorl	%r15d, %r15d
	xorl	%r8d, %r8d
	jmp	.L19
.L54:
	cmpb	$0, 1(%r15)
	movl	$0, %eax
	cmove	%rax, %r15
	jmp	.L25
.L53:
	movq	bio_err(%rip), %rdi
	leaq	.LC3(%rip), %rsi
	call	BIO_printf@PLT
	jmp	.L29
.L23:
	movq	-128(%rbp), %rdi
	call	app_load_config@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L29
	movq	-112(%rbp), %rdx
	movq	-136(%rbp), %rsi
	movq	%rax, %rdi
	call	NCONF_get_string@PLT
	testq	%rax, %rax
	je	.L57
	movl	$-1, %esi
	movq	%rax, %rdi
	call	NETSCAPE_SPKI_b64_decode@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L58
	movq	-104(%rbp), %rdi
	movl	$32769, %edx
	movl	$119, %esi
	call	bio_open_default@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L38
	testl	%r12d, %r12d
	je	.L59
.L32:
	movq	%r13, %rdi
	movq	%r8, -88(%rbp)
	call	NETSCAPE_SPKI_get_pubkey@PLT
	movl	-116(%rbp), %edx
	movq	-88(%rbp), %r8
	movq	%rax, %r12
	testl	%edx, %edx
	jne	.L60
.L33:
	movl	-120(%rbp), %eax
	testl	%eax, %eax
	jne	.L61
	xorl	%ebx, %ebx
	jmp	.L19
.L37:
	xorl	%r8d, %r8d
	movl	$1, %ebx
	jmp	.L19
.L36:
	xorl	%r15d, %r15d
	xorl	%r8d, %r8d
	movl	$1, %ebx
	jmp	.L19
.L55:
	movq	%r15, %rdi
	movl	$144, %edx
	movq	%rax, -88(%rbp)
	xorl	%r15d, %r15d
	leaq	.LC5(%rip), %rsi
	movl	$1, %ebx
	call	CRYPTO_free@PLT
	movq	-88(%rbp), %r8
	jmp	.L19
.L61:
	movq	%r8, %rdi
	movq	%r12, %rsi
	movq	%r8, -88(%rbp)
	call	PEM_write_bio_PUBKEY@PLT
	movq	-88(%rbp), %r8
	jmp	.L19
.L60:
	movq	%rax, %rsi
	movq	%r13, %rdi
	call	NETSCAPE_SPKI_verify@PLT
	movq	-88(%rbp), %r8
	testl	%eax, %eax
	jle	.L34
	movq	bio_err(%rip), %rdi
	leaq	.LC9(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	-88(%rbp), %r8
	jmp	.L33
.L59:
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%rax, -88(%rbp)
	call	NETSCAPE_SPKI_print@PLT
	movq	-88(%rbp), %r8
	jmp	.L32
.L58:
	movq	bio_err(%rip), %rdi
	leaq	.LC8(%rip), %rsi
	xorl	%eax, %eax
	xorl	%r12d, %r12d
	movl	$1, %ebx
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	xorl	%r8d, %r8d
	jmp	.L19
.L57:
	movq	-112(%rbp), %rdx
	movq	%rax, -88(%rbp)
	xorl	%eax, %eax
	xorl	%r13d, %r13d
	movq	bio_err(%rip), %rdi
	leaq	.LC7(%rip), %rsi
	xorl	%r12d, %r12d
	movl	$1, %ebx
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	movq	-88(%rbp), %r8
	jmp	.L19
.L38:
	xorl	%r12d, %r12d
	movl	$1, %ebx
	jmp	.L19
.L56:
	call	__stack_chk_fail@PLT
.L34:
	movq	bio_err(%rip), %rdi
	leaq	.LC10(%rip), %rsi
	xorl	%eax, %eax
	movq	%r8, -88(%rbp)
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	movl	-116(%rbp), %ebx
	movq	-88(%rbp), %r8
	jmp	.L19
	.cfi_endproc
.LFE1435:
	.size	spkac_main, .-spkac_main
	.globl	spkac_options
	.section	.rodata.str1.1
.LC11:
	.string	"help"
.LC12:
	.string	"Display this summary"
.LC13:
	.string	"in"
.LC14:
	.string	"Input file"
.LC15:
	.string	"out"
.LC16:
	.string	"Output file"
.LC17:
	.string	"key"
	.section	.rodata.str1.8
	.align 8
.LC18:
	.string	"Create SPKAC using private key"
	.section	.rodata.str1.1
.LC19:
	.string	"keyform"
	.section	.rodata.str1.8
	.align 8
.LC20:
	.string	"Private key file format - default PEM (PEM, DER, or ENGINE)"
	.section	.rodata.str1.1
.LC21:
	.string	"passin"
.LC22:
	.string	"Input file pass phrase source"
.LC23:
	.string	"challenge"
.LC24:
	.string	"Challenge string"
.LC25:
	.string	"spkac"
.LC26:
	.string	"Alternative SPKAC name"
.LC27:
	.string	"noout"
.LC28:
	.string	"Don't print SPKAC"
.LC29:
	.string	"pubkey"
.LC30:
	.string	"Output public key"
.LC31:
	.string	"verify"
.LC32:
	.string	"Verify SPKAC signature"
.LC33:
	.string	"spksect"
	.section	.rodata.str1.8
	.align 8
.LC34:
	.string	"Specify the name of an SPKAC-dedicated section of configuration"
	.section	.rodata.str1.1
.LC35:
	.string	"engine"
	.section	.rodata.str1.8
	.align 8
.LC36:
	.string	"Use engine, possibly a hardware device"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	spkac_options, @object
	.size	spkac_options, 336
spkac_options:
	.quad	.LC11
	.long	1
	.long	45
	.quad	.LC12
	.quad	.LC13
	.long	5
	.long	60
	.quad	.LC14
	.quad	.LC15
	.long	6
	.long	62
	.quad	.LC16
	.quad	.LC17
	.long	8
	.long	60
	.quad	.LC18
	.quad	.LC19
	.long	13
	.long	102
	.quad	.LC20
	.quad	.LC21
	.long	10
	.long	115
	.quad	.LC22
	.quad	.LC23
	.long	9
	.long	115
	.quad	.LC24
	.quad	.LC25
	.long	11
	.long	115
	.quad	.LC26
	.quad	.LC27
	.long	2
	.long	45
	.quad	.LC28
	.quad	.LC29
	.long	3
	.long	45
	.quad	.LC30
	.quad	.LC31
	.long	4
	.long	45
	.quad	.LC32
	.quad	.LC33
	.long	12
	.long	115
	.quad	.LC34
	.quad	.LC35
	.long	7
	.long	115
	.quad	.LC36
	.quad	0
	.zero	16
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
