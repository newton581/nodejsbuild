	.file	"openssl.c"
	.text
	.p2align 4
	.type	function_cmp, @function
function_cmp:
.LFB1607:
	.cfi_startproc
	endbr64
	movq	8(%rsi), %rsi
	movq	8(%rdi), %rdi
	movl	$8, %edx
	jmp	strncmp@PLT
	.cfi_endproc
.LFE1607:
	.size	function_cmp, .-function_cmp
	.p2align 4
	.type	function_hash, @function
function_hash:
.LFB1608:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	jmp	OPENSSL_LH_strhash@PLT
	.cfi_endproc
.LFE1608:
	.size	function_hash, .-function_hash
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"%s\n"
.LC1:
	.string	"quit"
.LC2:
	.string	"exit"
.LC3:
	.string	"bye"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"Invalid command '%s'; type \"help\" for a list.\n"
	.text
	.p2align 4
	.type	do_cmd, @function
do_cmd:
.LFB1604:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	xorl	%r13d, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%esi, %esi
	jle	.L4
	movq	(%rdx), %rax
	movq	%rdx, %r12
	testq	%rax, %rax
	je	.L4
	leaq	-96(%rbp), %rbx
	movl	%esi, %r14d
	movq	%rax, -88(%rbp)
	movq	%rdi, %r15
	movq	%rbx, %rsi
	call	OPENSSL_LH_retrieve@PLT
	testq	%rax, %rax
	je	.L26
	movq	16(%rax), %rax
.L8:
	movq	%r12, %rsi
	movl	%r14d, %edi
	call	*%rax
	movl	%eax, %r13d
.L4:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L27
	addq	$56, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	.cfi_restore_state
	movq	(%r12), %rdi
	call	EVP_get_digestbyname@PLT
	testq	%rax, %rax
	je	.L7
	movq	dgst_main@GOTPCREL(%rip), %rax
	movl	$2, -96(%rbp)
	movq	%rax, -80(%rbp)
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L7:
	movq	(%r12), %rdi
	call	EVP_get_cipherbyname@PLT
	testq	%rax, %rax
	je	.L28
	movq	enc_main@GOTPCREL(%rip), %rax
	movl	$3, -96(%rbp)
	movq	%rax, -80(%rbp)
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L28:
	movq	(%r12), %rdx
	cmpb	$110, (%rdx)
	jne	.L22
	cmpb	$111, 1(%rdx)
	jne	.L22
	movzbl	2(%rdx), %r13d
	subl	$45, %r13d
	jne	.L22
	addq	$3, %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movq	%rdx, -88(%rbp)
	call	OPENSSL_LH_retrieve@PLT
	movq	(%r12), %rdx
	testq	%rax, %rax
	je	.L29
	movq	bio_out(%rip), %rdi
	addq	$3, %rdx
	leaq	.LC0(%rip), %rsi
	xorl	%eax, %eax
	movl	$1, %r13d
	call	BIO_printf@PLT
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L22:
	movl	$5, %ecx
	leaq	.LC1(%rip), %rdi
	movq	%rdx, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L21
	cmpb	$113, (%rdx)
	jne	.L23
	cmpb	$0, 1(%rdx)
	je	.L21
.L23:
	movl	$5, %ecx
	leaq	.LC2(%rip), %rdi
	movq	%rdx, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L21
	movl	$4, %ecx
	leaq	.LC3(%rip), %rdi
	movq	%rdx, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L21
	movq	bio_err(%rip), %rdi
	leaq	.LC4(%rip), %rsi
	xorl	%eax, %eax
	movl	$1, %r13d
	call	BIO_printf@PLT
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L29:
	movq	bio_out(%rip), %rdi
	leaq	.LC0(%rip), %rsi
	call	BIO_printf@PLT
	jmp	.L4
.L21:
	movl	$-1, %r13d
	jmp	.L4
.L27:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1604:
	.size	do_cmd, .-do_cmd
	.p2align 4
	.type	SortFnByName, @function
SortFnByName:
.LFB1609:
	.cfi_startproc
	endbr64
	movl	(%rdi), %eax
	movl	(%rsi), %edx
	cmpl	%edx, %eax
	je	.L31
	subl	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L31:
	movq	8(%rsi), %rsi
	movq	8(%rdi), %rdi
	jmp	strcmp@PLT
	.cfi_endproc
.LFE1609:
	.size	SortFnByName, .-SortFnByName
	.section	.rodata.str1.1
.LC5:
	.string	"<undefined>"
.LC6:
	.string	"%s => %s\n"
	.text
	.p2align 4
	.type	list_md_fn, @function
list_md_fn:
.LFB1598:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rcx, %r12
	subq	$8, %rsp
	testq	%rdi, %rdi
	je	.L35
	call	EVP_MD_type@PLT
	movl	%eax, %edi
	call	OBJ_nid2sn@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	leaq	.LC0(%rip), %rsi
	movq	%rax, %rdx
	popq	%r12
	xorl	%eax, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	BIO_printf@PLT
	.p2align 4,,10
	.p2align 3
.L35:
	.cfi_restore_state
	leaq	.LC5(%rip), %rax
	testq	%rsi, %rsi
	movq	%r12, %rdi
	cmove	%rax, %rsi
	testq	%rdx, %rdx
	cmove	%rax, %rdx
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	movq	%rdx, %rcx
	movq	%rsi, %rdx
	leaq	.LC6(%rip), %rsi
	jmp	BIO_printf@PLT
	.cfi_endproc
.LFE1598:
	.size	list_md_fn, .-list_md_fn
	.p2align 4
	.type	list_cipher_fn, @function
list_cipher_fn:
.LFB1597:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rcx, %r12
	subq	$8, %rsp
	testq	%rdi, %rdi
	je	.L40
	call	EVP_CIPHER_nid@PLT
	movl	%eax, %edi
	call	OBJ_nid2sn@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	leaq	.LC0(%rip), %rsi
	movq	%rax, %rdx
	popq	%r12
	xorl	%eax, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	BIO_printf@PLT
	.p2align 4,,10
	.p2align 3
.L40:
	.cfi_restore_state
	leaq	.LC5(%rip), %rax
	testq	%rsi, %rsi
	movq	%r12, %rdi
	cmove	%rax, %rsi
	testq	%rdx, %rdx
	cmove	%rax, %rdx
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	movq	%rdx, %rcx
	movq	%rsi, %rdx
	leaq	.LC6(%rip), %rsi
	jmp	BIO_printf@PLT
	.cfi_endproc
.LFE1597:
	.size	list_cipher_fn, .-list_cipher_fn
	.section	.rodata.str1.1
.LC7:
	.string	"\n\n"
.LC8:
	.string	"%-*s"
.LC9:
	.string	"\n"
	.text
	.p2align 4
	.type	list_type, @function
list_type:
.LFB1603:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%edi, %r12d
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	8+functions(%rip), %rcx
	testl	%esi, %esi
	je	.L73
	leaq	functions(%rip), %rbx
	leaq	.LC0(%rip), %r13
	testq	%rcx, %rcx
	je	.L44
.L57:
	cmpl	%r12d, (%rbx)
	jne	.L54
	movq	bio_out(%rip), %rdi
	movq	%rcx, %rdx
	movq	%r13, %rsi
	xorl	%eax, %eax
	addq	$32, %rbx
	call	BIO_printf@PLT
	movq	8(%rbx), %rcx
	testq	%rcx, %rcx
	jne	.L57
.L44:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L54:
	.cfi_restore_state
	movq	40(%rbx), %rcx
	addq	$32, %rbx
	testq	%rcx, %rcx
	jne	.L57
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L73:
	.cfi_restore_state
	testq	%rcx, %rcx
	je	.L46
	movl	%esi, %r14d
	xorl	%r13d, %r13d
	leaq	functions(%rip), %rbx
	movq	%rcx, %rdi
	.p2align 4,,10
	.p2align 3
.L48:
	movl	(%rbx), %eax
	subl	$1, %eax
	cmpl	$2, %eax
	ja	.L47
	movq	%rcx, -56(%rbp)
	call	strlen@PLT
	movq	-56(%rbp), %rcx
	cmpl	%eax, %r13d
	cmovl	%eax, %r13d
.L47:
	movq	40(%rbx), %rdi
	addq	$32, %rbx
	testq	%rdi, %rdi
	jne	.L48
	movl	$79, %eax
	addl	$2, %r13d
	leaq	functions(%rip), %rbx
	cltd
	leaq	.LC8(%rip), %r15
	idivl	%r13d
	movl	%eax, -56(%rbp)
	.p2align 4,,10
	.p2align 3
.L49:
	cmpl	(%rbx), %r12d
	jne	.L50
	movl	%r14d, %eax
	movq	bio_out(%rip), %rdi
	cltd
	idivl	-56(%rbp)
	testl	%edx, %edx
	jne	.L53
	testl	%r14d, %r14d
	jne	.L51
.L53:
	movl	%r13d, %edx
	movq	%r15, %rsi
	xorl	%eax, %eax
	addl	$1, %r14d
	call	BIO_printf@PLT
.L50:
	movq	40(%rbx), %rcx
	addq	$32, %rbx
	testq	%rcx, %rcx
	jne	.L49
.L46:
	movq	bio_out(%rip), %rdi
	addq	$24, %rsp
	leaq	.LC7(%rip), %rsi
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	BIO_printf@PLT
	.p2align 4,,10
	.p2align 3
.L51:
	.cfi_restore_state
	leaq	.LC9(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	8(%rbx), %rcx
	movq	bio_out(%rip), %rdi
	jmp	.L53
	.cfi_endproc
.LFE1603:
	.size	list_type, .-list_type
	.section	.rodata.str1.1
.LC10:
	.string	"External"
.LC11:
	.string	"Builtin"
.LC12:
	.string	"(none)"
.LC13:
	.string	"%s: Use -help for summary.\n"
.LC14:
	.string	"Name: %s\n"
.LC15:
	.string	"\tAlias for: %s\n"
.LC16:
	.string	"\tType: %s Algorithm\n"
.LC17:
	.string	"\tOID: %s\n"
.LC18:
	.string	"\tPEM string: %s\n"
.LC19:
	.string	"Disabled algorithms:\n"
.LC20:
	.string	"COMP\n"
.LC21:
	.string	"GOST\n"
.LC22:
	.string	"HEARTBEATS\n"
.LC23:
	.string	"MD2\n"
.LC24:
	.string	"RC5\n"
.LC25:
	.string	"SCTP\n"
.LC26:
	.string	"SSL3\n"
.LC27:
	.string	"ZLIB\n"
.LC28:
	.string	"%s %s\n"
.LC29:
	.string	"%s *\n"
.LC30:
	.string	"%s %c\n"
.LC31:
	.string	"Extra arguments given.\n"
	.text
	.p2align 4
	.globl	list_main
	.type	list_main, @function
list_main:
.LFB1601:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	list_options(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	xorl	%ebx, %ebx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	opt_init@PLT
	movq	%rax, -112(%rbp)
	call	opt_next@PLT
	testl	%eax, %eax
	je	.L148
	.p2align 4,,10
	.p2align 3
.L112:
	addl	$1, %eax
	cmpl	$13, %eax
	ja	.L76
	leaq	.L78(%rip), %rcx
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L78:
	.long	.L90-.L78
	.long	.L76-.L78
	.long	.L89-.L78
	.long	.L88-.L78
	.long	.L87-.L78
	.long	.L86-.L78
	.long	.L85-.L78
	.long	.L84-.L78
	.long	.L83-.L78
	.long	.L82-.L78
	.long	.L115-.L78
	.long	.L80-.L78
	.long	.L79-.L78
	.long	.L77-.L78
	.text
.L115:
	xorl	%ebx, %ebx
	leaq	-64(%rbp), %r12
	leaq	.LC10(%rip), %r14
	leaq	.LC11(%rip), %r13
	jmp	.L81
	.p2align 4,,10
	.p2align 3
.L149:
	movl	-84(%rbp), %edi
	call	OBJ_nid2ln@PLT
	movq	bio_out(%rip), %rdi
	leaq	.LC14(%rip), %rsi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movl	-80(%rbp), %edi
	call	OBJ_nid2ln@PLT
	movq	bio_out(%rip), %rdi
	leaq	.LC15(%rip), %rsi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L94:
	addl	$1, %ebx
.L81:
	call	EVP_PKEY_asn1_get_count@PLT
	cmpl	%eax, %ebx
	jge	.L76
	movl	%ebx, %edi
	call	EVP_PKEY_asn1_get0@PLT
	leaq	-72(%rbp), %rcx
	leaq	-76(%rbp), %rdx
	movq	%r12, %r8
	movq	%rax, %r9
	leaq	-80(%rbp), %rsi
	leaq	-84(%rbp), %rdi
	call	EVP_PKEY_asn1_get0_info@PLT
	testb	$1, -76(%rbp)
	jne	.L149
	movq	-72(%rbp), %rdx
	movq	bio_out(%rip), %rdi
	leaq	.LC14(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	testb	$2, -76(%rbp)
	movq	%r13, %rdx
	movq	bio_out(%rip), %rdi
	cmovne	%r14, %rdx
	leaq	.LC16(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movl	-84(%rbp), %edi
	call	OBJ_nid2ln@PLT
	movq	bio_out(%rip), %rdi
	leaq	.LC17(%rip), %rsi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	-64(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L150
.L96:
	movq	bio_out(%rip), %rdi
	leaq	.LC18(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L94
.L88:
	movl	$1, %r15d
.L76:
	movl	$1, %ebx
	call	opt_next@PLT
	testl	%eax, %eax
	jne	.L112
.L148:
	call	opt_num_rest@PLT
	testl	%eax, %eax
	jne	.L151
	testl	%ebx, %ebx
	jne	.L74
.L90:
	movq	-112(%rbp), %rdx
	movq	bio_err(%rip), %rdi
	leaq	.LC13(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movl	$1, %eax
.L74:
	movq	-56(%rbp), %rsi
	xorq	%fs:40, %rsi
	jne	.L152
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L89:
	.cfi_restore_state
	leaq	list_options(%rip), %rdi
	call	opt_help@PLT
	jmp	.L76
.L87:
	movl	%r15d, %esi
	movl	$1, %edi
	call	list_type
	jmp	.L76
.L83:
	movl	%r15d, %esi
	movl	$3, %edi
	call	list_type
	jmp	.L76
.L84:
	movq	bio_out(%rip), %rsi
	leaq	list_md_fn(%rip), %rdi
	call	EVP_MD_do_all_sorted@PLT
	jmp	.L76
.L85:
	call	opt_arg@PLT
	movq	8+functions(%rip), %rdi
	movq	%rax, %r12
	testq	%rdi, %rdi
	je	.L107
	leaq	functions(%rip), %rbx
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L153:
	movq	40(%rbx), %rdi
	addq	$32, %rbx
	testq	%rdi, %rdi
	je	.L107
.L109:
	movq	%r12, %rsi
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L153
	movq	24(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L145
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L111:
	leaq	OPT_MORE_STR(%rip), %rax
	cmpq	%rax, %rdx
	je	.L110
	leaq	OPT_HELP_STR(%rip), %rax
	cmpq	%rax, %rdx
	je	.L110
	cmpb	$0, (%rdx)
	je	.L110
	movl	12(%rbx), %ecx
	movq	bio_out(%rip), %rdi
	leaq	.LC30(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L110:
	addq	$24, %rbx
.L145:
	movq	(%rbx), %rdx
	testq	%rdx, %rdx
	jne	.L111
	jmp	.L76
.L86:
	movl	%r15d, %esi
	movl	$2, %edi
	call	list_type
	jmp	.L76
.L82:
	movq	bio_out(%rip), %rsi
	leaq	list_cipher_fn(%rip), %rdi
	call	EVP_CIPHER_do_all_sorted@PLT
	jmp	.L76
.L77:
	movq	8+functions(%rip), %rdx
	leaq	functions(%rip), %r12
	leaq	.LC28(%rip), %r13
	testq	%rdx, %rdx
	je	.L76
	movq	24(%r12), %rbx
	testq	%rbx, %rbx
	jne	.L154
	.p2align 4,,10
	.p2align 3
.L101:
	movq	dgst_main@GOTPCREL(%rip), %rax
	cmpq	%rax, 16(%r12)
	je	.L103
	movq	bio_out(%rip), %rdi
	leaq	.LC29(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L103:
	movq	40(%r12), %rdx
	addq	$32, %r12
	testq	%rdx, %rdx
	je	.L76
	movq	24(%r12), %rbx
	testq	%rbx, %rbx
	je	.L101
.L154:
	movq	(%rbx), %rcx
	testq	%rcx, %rcx
	je	.L103
.L102:
	cmpq	$0, 16(%rbx)
	je	.L155
.L104:
	movq	24(%rbx), %rcx
	addq	$24, %rbx
	testq	%rcx, %rcx
	je	.L103
	cmpq	$0, 16(%rbx)
	jne	.L104
.L155:
	movq	8(%r12), %rdx
	movq	%r13, %rsi
	xorl	%eax, %eax
	addq	$24, %rbx
	movq	bio_out(%rip), %rdi
	call	BIO_printf@PLT
	movq	(%rbx), %rcx
	testq	%rcx, %rcx
	jne	.L102
	jmp	.L103
.L79:
	movq	bio_out(%rip), %rdi
	leaq	.LC19(%rip), %rsi
	call	BIO_puts@PLT
	movq	bio_out(%rip), %rdi
	leaq	.LC20(%rip), %rsi
	call	BIO_puts@PLT
	movq	bio_out(%rip), %rdi
	leaq	.LC21(%rip), %rsi
	call	BIO_puts@PLT
	movq	bio_out(%rip), %rdi
	leaq	.LC22(%rip), %rsi
	call	BIO_puts@PLT
	movq	bio_out(%rip), %rdi
	leaq	.LC23(%rip), %rsi
	call	BIO_puts@PLT
	movq	bio_out(%rip), %rdi
	leaq	.LC24(%rip), %rsi
	call	BIO_puts@PLT
	movq	bio_out(%rip), %rdi
	leaq	.LC25(%rip), %rsi
	call	BIO_puts@PLT
	movq	bio_out(%rip), %rdi
	leaq	.LC26(%rip), %rsi
	call	BIO_puts@PLT
	movq	bio_out(%rip), %rdi
	leaq	.LC27(%rip), %rsi
	call	BIO_puts@PLT
	jmp	.L76
.L80:
	call	EVP_PKEY_meth_get_count@PLT
	movq	%rax, -104(%rbp)
	testq	%rax, %rax
	je	.L76
	xorl	%ebx, %ebx
	leaq	-64(%rbp), %r14
	leaq	-72(%rbp), %r13
	leaq	.LC0(%rip), %r12
	.p2align 4,,10
	.p2align 3
.L100:
	movq	%rbx, %rdi
	call	EVP_PKEY_meth_get0@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	EVP_PKEY_meth_get0_info@PLT
	movl	-72(%rbp), %edi
	call	OBJ_nid2ln@PLT
	movq	bio_out(%rip), %rdi
	movq	%r12, %rsi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	leaq	.LC10(%rip), %rdx
	testb	$2, -64(%rbp)
	jne	.L143
	leaq	.LC11(%rip), %rdx
.L143:
	movq	bio_out(%rip), %rdi
	leaq	.LC16(%rip), %rsi
	xorl	%eax, %eax
	addq	$1, %rbx
	call	BIO_printf@PLT
	cmpq	%rbx, -104(%rbp)
	jne	.L100
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L150:
	leaq	.LC12(%rip), %rax
	movq	%rax, -64(%rbp)
	movq	%rax, %rdx
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L151:
	movq	bio_err(%rip), %rdi
	leaq	.LC31(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L107:
	movq	bio_err(%rip), %rdi
	movq	%r12, %rdx
	leaq	.LC4(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L76
.L152:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1601:
	.size	list_main, .-list_main
	.section	.rodata.str1.1
.LC32:
	.string	"--help"
.LC33:
	.string	"Usage: %s\n"
.LC34:
	.string	"Standard commands"
	.section	.rodata.str1.8
	.align 8
.LC35:
	.string	"\nMessage Digest commands (see the `dgst' command for more details)\n"
	.align 8
.LC36:
	.string	"\nCipher commands (see the `enc' command for more details)\n"
	.text
	.p2align 4
	.globl	help_main
	.type	help_main, @function
help_main:
.LFB1602:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	help_options(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	opt_init@PLT
	movq	%rax, %r12
.L157:
	call	opt_next@PLT
	testl	%eax, %eax
	je	.L197
	cmpl	$-1, %eax
	je	.L158
	cmpl	$1, %eax
	jne	.L157
	leaq	help_options(%rip), %rdi
	xorl	%r13d, %r13d
	call	opt_help@PLT
.L156:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L198
	addq	$56, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L158:
	.cfi_restore_state
	movq	bio_err(%rip), %rdi
	movq	%r12, %rdx
	leaq	.LC13(%rip), %rsi
	xorl	%eax, %eax
	movl	$1, %r13d
	call	BIO_printf@PLT
	jmp	.L156
	.p2align 4,,10
	.p2align 3
.L197:
	call	opt_num_rest@PLT
	cmpl	$1, %eax
	je	.L199
	call	opt_num_rest@PLT
	movl	%eax, %r13d
	testl	%eax, %eax
	jne	.L169
	movq	8+functions(%rip), %rdi
	xorl	%ebx, %ebx
	leaq	functions(%rip), %r12
	testq	%rdi, %rdi
	je	.L200
	.p2align 4,,10
	.p2align 3
.L170:
	movl	(%r12), %eax
	subl	$1, %eax
	cmpl	$2, %eax
	ja	.L172
	call	strlen@PLT
	cmpl	%eax, %ebx
	cmovl	%eax, %ebx
.L172:
	movq	40(%r12), %rdi
	addq	$32, %r12
	testq	%rdi, %rdi
	jne	.L170
	movl	$79, %eax
	addl	$2, %ebx
	cltd
	idivl	%ebx
	movl	%eax, -84(%rbp)
.L171:
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
	xorl	%r12d, %r12d
	xorl	%r14d, %r14d
	leaq	.LC34(%rip), %rsi
	leaq	functions(%rip), %r15
	call	BIO_printf@PLT
	movq	8+functions(%rip), %rcx
	testq	%rcx, %rcx
	jne	.L173
	jmp	.L180
	.p2align 4,,10
	.p2align 3
.L201:
	movl	(%r15), %r12d
	movq	bio_err(%rip), %rdi
	cmpl	%r14d, %r12d
	je	.L195
	leaq	.LC9(%rip), %rsi
	xorl	%eax, %eax
	movl	%r9d, -88(%rbp)
	movl	%r12d, %r14d
	call	BIO_printf@PLT
	movl	-88(%rbp), %r9d
.L177:
	movq	bio_err(%rip), %rdi
	leaq	.LC35(%rip), %rsi
	cmpl	$2, %r14d
	je	.L196
	cmpl	$3, %r14d
	je	.L179
	movq	8(%r15), %rcx
	movl	%r9d, %r12d
.L176:
	movl	%ebx, %edx
	leaq	.LC8(%rip), %rsi
	xorl	%eax, %eax
	addq	$32, %r15
	call	BIO_printf@PLT
	movq	8(%r15), %rcx
	testq	%rcx, %rcx
	je	.L180
.L173:
	movl	%r12d, %eax
	leal	1(%r12), %r9d
	cltd
	idivl	-84(%rbp)
	testl	%edx, %edx
	jne	.L201
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
	leaq	.LC9(%rip), %rsi
	movl	%r9d, -88(%rbp)
	call	BIO_printf@PLT
	movl	(%r15), %eax
	movl	-88(%rbp), %r9d
	cmpl	%eax, %r14d
	je	.L202
	movl	%eax, %r14d
	jmp	.L177
.L199:
	call	opt_rest@PLT
	movq	(%rax), %rax
	movq	$0, -64(%rbp)
	movq	%rax, -80(%rbp)
	leaq	.LC32(%rip), %rax
	movq	%rax, -72(%rbp)
	movl	prog_inited.27474(%rip), %eax
	testl	%eax, %eax
	jne	.L167
	movl	$1, prog_inited.27474(%rip)
	xorl	%esi, %esi
	cmpq	$0, 8+functions(%rip)
	je	.L165
	leaq	functions(%rip), %rax
	.p2align 4,,10
	.p2align 3
.L166:
	addq	$32, %rax
	addq	$1, %rsi
	cmpq	$0, 8(%rax)
	jne	.L166
.L165:
	leaq	SortFnByName(%rip), %rcx
	movl	$32, %edx
	leaq	functions(%rip), %rdi
	call	qsort@PLT
	leaq	function_hash(%rip), %rdi
	leaq	function_cmp(%rip), %rsi
	call	OPENSSL_LH_new@PLT
	movq	%rax, ret.27473(%rip)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L164
	cmpq	$0, 8+functions(%rip)
	je	.L167
	leaq	functions(%rip), %rbx
	jmp	.L168
	.p2align 4,,10
	.p2align 3
.L203:
	movq	ret.27473(%rip), %rdi
.L168:
	movq	%rbx, %rsi
	addq	$32, %rbx
	call	OPENSSL_LH_insert@PLT
	cmpq	$0, 8(%rbx)
	jne	.L203
.L167:
	movq	ret.27473(%rip), %rdi
.L164:
	leaq	-80(%rbp), %rdx
	movl	$2, %esi
	call	do_cmd
	movl	%eax, %r13d
	jmp	.L156
	.p2align 4,,10
	.p2align 3
.L202:
	movq	8(%r15), %rcx
	movq	bio_err(%rip), %rdi
.L195:
	movl	%r9d, %r12d
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L179:
	leaq	.LC36(%rip), %rsi
.L196:
	xorl	%eax, %eax
	movl	$1, %r12d
	call	BIO_printf@PLT
	movq	8(%r15), %rcx
	movq	bio_err(%rip), %rdi
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L180:
	movq	bio_err(%rip), %rdi
	leaq	.LC7(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L156
.L169:
	movq	bio_err(%rip), %rdi
	movq	%r12, %rdx
	leaq	.LC33(%rip), %rsi
	xorl	%eax, %eax
	movl	$1, %r13d
	call	BIO_printf@PLT
	jmp	.L156
.L200:
	movl	$39, -84(%rbp)
	movl	$2, %ebx
	jmp	.L171
.L198:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1602:
	.size	help_main, .-help_main
	.section	.rodata.str1.1
.LC37:
	.string	"OpenSSL> "
.LC38:
	.string	"OPENSSL_CONF"
	.section	.rodata.str1.8
	.align 8
.LC39:
	.string	"../deps/openssl/openssl/apps/openssl.c"
	.section	.rodata.str1.1
.LC40:
	.string	"config filename buffer"
.LC41:
	.string	"OPENSSL_DEBUG_MEMORY"
.LC42:
	.string	"OPENSSL_FIPS"
.LC43:
	.string	"FIPS mode not supported.\n"
	.section	.rodata.str1.8
	.align 8
.LC44:
	.string	"FATAL: Startup failure (dev note: apps_startup() failed)\n"
	.align 8
.LC45:
	.string	"FATAL: Startup failure (dev note: prog_init() failed)\n"
	.section	.rodata.str1.1
.LC46:
	.string	"> "
.LC47:
	.string	"Can't parse (no memory?)\n"
.LC48:
	.string	"error in %s\n"
	.section	.text.startup,"ax",@progbits
	.p2align 4
	.globl	main
	.type	main, @function
main:
.LFB1596:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%edi, %r12d
	leaq	.LC38(%rip), %rdi
	pushq	%rbx
	subq	$1096, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -1128(%rbp)
	movl	$0, -1136(%rbp)
	call	getenv@PLT
	testq	%rax, %rax
	je	.L205
	movq	%rax, %rdi
	movl	$105, %edx
	leaq	.LC39(%rip), %rsi
	call	CRYPTO_strdup@PLT
	movq	%rax, %rbx
.L206:
	movl	$32769, %edi
	movq	%rbx, default_config_file(%rip)
	call	dup_bio_in@PLT
	movl	$32769, %edi
	movq	%rax, bio_in(%rip)
	call	dup_bio_out@PLT
	movl	$32769, %edi
	movq	%rax, bio_out(%rip)
	call	dup_bio_err@PLT
	leaq	.LC41(%rip), %rdi
	movq	%rax, bio_err(%rip)
	call	getenv@PLT
	testq	%rax, %rax
	je	.L207
	cmpb	$111, (%rax)
	je	.L247
.L207:
	movl	$1, %edi
	call	CRYPTO_mem_ctrl@PLT
	leaq	.LC42(%rip), %rdi
	call	getenv@PLT
	testq	%rax, %rax
	je	.L209
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
	leaq	.LC43(%rip), %rsi
	call	BIO_printf@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L248
	addq	$1096, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L247:
	.cfi_restore_state
	cmpb	$110, 1(%rax)
	jne	.L207
	cmpb	$0, 2(%rax)
	jne	.L207
	movl	$1, %edi
	call	CRYPTO_set_mem_debug@PLT
	jmp	.L207
.L205:
	call	X509_get_default_cert_area@PLT
	orq	$-1, %rcx
	leaq	.LC40(%rip), %rsi
	movq	%rax, %r13
	xorl	%eax, %eax
	movq	%r13, %rdi
	repnz scasb
	notq	%rcx
	leaq	12(%rcx), %rdi
	call	app_malloc@PLT
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%rax, %rbx
	call	stpcpy@PLT
	movabsq	$3345175562840010863, %rdx
	movb	$47, (%rax)
	movq	%rdx, 1(%rax)
	movl	$6712931, 9(%rax)
	jmp	.L206
.L209:
	movl	$1, %esi
	movl	$13, %edi
	call	signal@PLT
	xorl	%esi, %esi
	movl	$30272, %edi
	call	OPENSSL_init_ssl@PLT
	testl	%eax, %eax
	jne	.L210
	movq	bio_err(%rip), %rdi
	leaq	.LC44(%rip), %rsi
.L246:
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	xorl	%r13d, %r13d
	movl	$1, %r12d
	call	ERR_print_errors@PLT
.L211:
	movl	$261, %edx
	leaq	.LC39(%rip), %rsi
	xorl	%edi, %edi
	call	CRYPTO_free@PLT
	movq	default_config_file(%rip), %rdi
	movl	$262, %edx
	leaq	.LC39(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%r13, %rdi
	call	OPENSSL_LH_free@PLT
	movq	-1128(%rbp), %rdi
	movl	$264, %edx
	leaq	.LC39(%rip), %rsi
	call	CRYPTO_free@PLT
	call	app_RAND_write@PLT
	movq	bio_in(%rip), %rdi
	call	BIO_free@PLT
	movq	bio_out(%rip), %rdi
	call	BIO_free_all@PLT
	call	destroy_ui_method@PLT
	call	destroy_prefix_method@PLT
	movq	bio_err(%rip), %rdi
	call	BIO_free@PLT
	movl	%r12d, %edi
	call	exit@PLT
.L248:
	call	__stack_chk_fail@PLT
.L210:
	call	setup_ui_method@PLT
	cmpl	$0, prog_inited.27474(%rip)
	movq	ret.27473(%rip), %r13
	jne	.L213
	movl	$1, prog_inited.27474(%rip)
	xorl	%esi, %esi
	leaq	functions(%rip), %rax
	jmp	.L214
.L215:
	addq	$32, %rax
	addq	$1, %rsi
.L214:
	cmpq	$0, 8(%rax)
	jne	.L215
	leaq	SortFnByName(%rip), %rcx
	movl	$32, %edx
	leaq	functions(%rip), %rdi
	call	qsort@PLT
	leaq	function_cmp(%rip), %rsi
	leaq	function_hash(%rip), %rdi
	call	OPENSSL_LH_new@PLT
	leaq	functions(%rip), %rbx
	movq	%rax, ret.27473(%rip)
	testq	%rax, %rax
	jne	.L216
	jmp	.L218
.L217:
	movq	%rbx, %rsi
	movq	%r13, %rdi
	addq	$32, %rbx
	call	OPENSSL_LH_insert@PLT
.L216:
	cmpq	$0, 8(%rbx)
	movq	ret.27473(%rip), %r13
	jne	.L217
.L213:
	testq	%r13, %r13
	je	.L218
	movq	(%r14), %rdi
	call	opt_progname@PLT
	leaq	-1120(%rbp), %rsi
	movq	%r13, %rdi
	movq	%rax, -1112(%rbp)
	movq	%rax, %rbx
	call	OPENSSL_LH_retrieve@PLT
	testq	%rax, %rax
	je	.L219
	movl	%r12d, %edi
	movq	%rbx, (%r14)
	movq	%r14, %rsi
	call	*16(%rax)
	movl	%eax, %r12d
	jmp	.L211
.L219:
	leaq	-1088(%rbp), %rbx
	cmpl	$1, %r12d
	jne	.L249
.L220:
	movl	$1024, %r15d
	movq	%rbx, %r14
	leaq	.LC37(%rip), %rdi
.L222:
	movb	$0, (%r14)
	movq	stdout(%rip), %rsi
	call	fputs@PLT
	movq	stdout(%rip), %rdi
	call	fflush@PLT
	movq	stdin(%rip), %rdx
	movl	%r15d, %esi
	movq	%r14, %rdi
	call	fgets@PLT
	testq	%rax, %rax
	je	.L229
	cmpb	$0, (%r14)
	je	.L229
	xorl	%eax, %eax
	orq	$-1, %rcx
	movq	%r14, %rdi
	repnz scasb
	movq	%rcx, %rax
	notq	%rax
	subq	$1, %rax
	cmpl	$1, %eax
	jle	.L221
	movslq	%eax, %rdx
	cmpb	$92, -2(%r14,%rdx)
	jne	.L221
	subl	$2, %eax
	leaq	.LC46(%rip), %rdi
	movslq	%eax, %rdx
	subl	%eax, %r15d
	addq	%rdx, %r14
	testl	%r15d, %r15d
	jg	.L222
.L221:
	leaq	-1136(%rbp), %rdi
	movq	%rbx, %rsi
	call	chopup_args@PLT
	testl	%eax, %eax
	je	.L250
	movq	-1128(%rbp), %rdx
	movl	-1132(%rbp), %esi
	movq	%r13, %rdi
	call	do_cmd
	cmpl	$-1, %eax
	je	.L229
	testl	%eax, %eax
	jne	.L251
.L224:
	movq	bio_out(%rip), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	call	BIO_ctrl@PLT
	movq	bio_err(%rip), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	call	BIO_ctrl@PLT
	jmp	.L220
.L229:
	xorl	%r12d, %r12d
	jmp	.L211
.L249:
	leal	-1(%r12), %esi
	leaq	8(%r14), %rdx
	movq	%r13, %rdi
	call	do_cmd
	movl	%eax, %r12d
	testl	%eax, %eax
	movl	$0, %eax
	cmovs	%eax, %r12d
	jmp	.L211
.L218:
	movq	bio_err(%rip), %rdi
	leaq	.LC45(%rip), %rsi
	xorl	%eax, %eax
	jmp	.L246
.L251:
	movq	-1128(%rbp), %rax
	movq	bio_err(%rip), %rdi
	leaq	.LC48(%rip), %rsi
	movq	(%rax), %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L224
.L250:
	movq	bio_err(%rip), %rdi
	leaq	.LC47(%rip), %rsi
	call	BIO_printf@PLT
	jmp	.L211
	.cfi_endproc
.LFE1596:
	.size	main, .-main
	.local	ret.27473
	.comm	ret.27473,8,8
	.local	prog_inited.27474
	.comm	prog_inited.27474,4,4
	.globl	help_options
	.section	.rodata.str1.1
.LC49:
	.string	"Usage: help [options]\n"
.LC50:
	.string	"       help [command]\n"
.LC51:
	.string	"help"
.LC52:
	.string	"Display this summary"
	.section	.data.rel.ro,"aw"
	.align 32
	.type	help_options, @object
	.size	help_options, 96
help_options:
	.quad	OPT_HELP_STR
	.long	1
	.long	45
	.quad	.LC49
	.quad	OPT_HELP_STR
	.long	1
	.long	45
	.quad	.LC50
	.quad	.LC51
	.long	1
	.long	45
	.quad	.LC52
	.quad	0
	.zero	16
	.globl	list_options
	.section	.rodata.str1.1
.LC53:
	.string	"1"
.LC54:
	.string	"List in one column"
.LC55:
	.string	"commands"
.LC56:
	.string	"List of standard commands"
.LC57:
	.string	"digest-commands"
	.section	.rodata.str1.8
	.align 8
.LC58:
	.string	"List of message digest commands"
	.section	.rodata.str1.1
.LC59:
	.string	"digest-algorithms"
	.section	.rodata.str1.8
	.align 8
.LC60:
	.string	"List of message digest algorithms"
	.section	.rodata.str1.1
.LC61:
	.string	"cipher-commands"
.LC62:
	.string	"List of cipher commands"
.LC63:
	.string	"cipher-algorithms"
.LC64:
	.string	"List of cipher algorithms"
.LC65:
	.string	"public-key-algorithms"
.LC66:
	.string	"List of public key algorithms"
.LC67:
	.string	"public-key-methods"
.LC68:
	.string	"List of public key methods"
.LC69:
	.string	"disabled"
.LC70:
	.string	"List of disabled features"
.LC71:
	.string	"missing-help"
	.section	.rodata.str1.8
	.align 8
.LC72:
	.string	"List missing detailed help strings"
	.section	.rodata.str1.1
.LC73:
	.string	"options"
	.section	.rodata.str1.8
	.align 8
.LC74:
	.string	"List options for specified command"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	list_options, @object
	.size	list_options, 312
list_options:
	.quad	.LC51
	.long	1
	.long	45
	.quad	.LC52
	.quad	.LC53
	.long	2
	.long	45
	.quad	.LC54
	.quad	.LC55
	.long	3
	.long	45
	.quad	.LC56
	.quad	.LC57
	.long	4
	.long	45
	.quad	.LC58
	.quad	.LC59
	.long	6
	.long	45
	.quad	.LC60
	.quad	.LC61
	.long	7
	.long	45
	.quad	.LC62
	.quad	.LC63
	.long	8
	.long	45
	.quad	.LC64
	.quad	.LC65
	.long	9
	.long	45
	.quad	.LC66
	.quad	.LC67
	.long	10
	.long	45
	.quad	.LC68
	.quad	.LC69
	.long	11
	.long	45
	.quad	.LC70
	.quad	.LC71
	.long	12
	.long	45
	.quad	.LC72
	.quad	.LC73
	.long	5
	.long	115
	.quad	.LC74
	.quad	0
	.zero	16
	.globl	bio_err
	.bss
	.align 8
	.type	bio_err, @object
	.size	bio_err, 8
bio_err:
	.zero	8
	.globl	bio_out
	.align 8
	.type	bio_out, @object
	.size	bio_out, 8
bio_out:
	.zero	8
	.globl	bio_in
	.align 8
	.type	bio_in, @object
	.size	bio_in, 8
bio_in:
	.zero	8
	.globl	default_config_file
	.align 8
	.type	default_config_file, @object
	.size	default_config_file, 8
default_config_file:
	.zero	8
	.section	.rodata.str1.1
.LC75:
	.string	"asn1parse"
.LC76:
	.string	"ca"
.LC77:
	.string	"ciphers"
.LC78:
	.string	"cms"
.LC79:
	.string	"crl"
.LC80:
	.string	"crl2pkcs7"
.LC81:
	.string	"dgst"
.LC82:
	.string	"dhparam"
.LC83:
	.string	"dsa"
.LC84:
	.string	"dsaparam"
.LC85:
	.string	"ec"
.LC86:
	.string	"ecparam"
.LC87:
	.string	"enc"
.LC88:
	.string	"engine"
.LC89:
	.string	"errstr"
.LC90:
	.string	"gendsa"
.LC91:
	.string	"genpkey"
.LC92:
	.string	"genrsa"
.LC93:
	.string	"list"
.LC94:
	.string	"nseq"
.LC95:
	.string	"ocsp"
.LC96:
	.string	"passwd"
.LC97:
	.string	"pkcs12"
.LC98:
	.string	"pkcs7"
.LC99:
	.string	"pkcs8"
.LC100:
	.string	"pkey"
.LC101:
	.string	"pkeyparam"
.LC102:
	.string	"pkeyutl"
.LC103:
	.string	"prime"
.LC104:
	.string	"rand"
.LC105:
	.string	"rehash"
.LC106:
	.string	"req"
.LC107:
	.string	"rsa"
.LC108:
	.string	"rsautl"
.LC109:
	.string	"s_client"
.LC110:
	.string	"s_server"
.LC111:
	.string	"s_time"
.LC112:
	.string	"sess_id"
.LC113:
	.string	"smime"
.LC114:
	.string	"speed"
.LC115:
	.string	"spkac"
.LC116:
	.string	"srp"
.LC117:
	.string	"storeutl"
.LC118:
	.string	"ts"
.LC119:
	.string	"verify"
.LC120:
	.string	"version"
.LC121:
	.string	"x509"
.LC122:
	.string	"md4"
.LC123:
	.string	"md5"
.LC124:
	.string	"sha1"
.LC125:
	.string	"sha224"
.LC126:
	.string	"sha256"
.LC127:
	.string	"sha384"
.LC128:
	.string	"sha512"
.LC129:
	.string	"sha512-224"
.LC130:
	.string	"sha512-256"
.LC131:
	.string	"sha3-224"
.LC132:
	.string	"sha3-256"
.LC133:
	.string	"sha3-384"
.LC134:
	.string	"sha3-512"
.LC135:
	.string	"shake128"
.LC136:
	.string	"shake256"
.LC137:
	.string	"mdc2"
.LC138:
	.string	"rmd160"
.LC139:
	.string	"blake2b512"
.LC140:
	.string	"blake2s256"
.LC141:
	.string	"sm3"
.LC142:
	.string	"aes-128-cbc"
.LC143:
	.string	"aes-128-ecb"
.LC144:
	.string	"aes-192-cbc"
.LC145:
	.string	"aes-192-ecb"
.LC146:
	.string	"aes-256-cbc"
.LC147:
	.string	"aes-256-ecb"
.LC148:
	.string	"aria-128-cbc"
.LC149:
	.string	"aria-128-cfb"
.LC150:
	.string	"aria-128-ctr"
.LC151:
	.string	"aria-128-ecb"
.LC152:
	.string	"aria-128-ofb"
.LC153:
	.string	"aria-128-cfb1"
.LC154:
	.string	"aria-128-cfb8"
.LC155:
	.string	"aria-192-cbc"
.LC156:
	.string	"aria-192-cfb"
.LC157:
	.string	"aria-192-ctr"
.LC158:
	.string	"aria-192-ecb"
.LC159:
	.string	"aria-192-ofb"
.LC160:
	.string	"aria-192-cfb1"
.LC161:
	.string	"aria-192-cfb8"
.LC162:
	.string	"aria-256-cbc"
.LC163:
	.string	"aria-256-cfb"
.LC164:
	.string	"aria-256-ctr"
.LC165:
	.string	"aria-256-ecb"
.LC166:
	.string	"aria-256-ofb"
.LC167:
	.string	"aria-256-cfb1"
.LC168:
	.string	"aria-256-cfb8"
.LC169:
	.string	"camellia-128-cbc"
.LC170:
	.string	"camellia-128-ecb"
.LC171:
	.string	"camellia-192-cbc"
.LC172:
	.string	"camellia-192-ecb"
.LC173:
	.string	"camellia-256-cbc"
.LC174:
	.string	"camellia-256-ecb"
.LC175:
	.string	"base64"
.LC176:
	.string	"des"
.LC177:
	.string	"des3"
.LC178:
	.string	"desx"
.LC179:
	.string	"idea"
.LC180:
	.string	"seed"
.LC181:
	.string	"rc4"
.LC182:
	.string	"rc4-40"
.LC183:
	.string	"rc2"
.LC184:
	.string	"bf"
.LC185:
	.string	"cast"
.LC186:
	.string	"des-ecb"
.LC187:
	.string	"des-ede"
.LC188:
	.string	"des-ede3"
.LC189:
	.string	"des-cbc"
.LC190:
	.string	"des-ede-cbc"
.LC191:
	.string	"des-ede3-cbc"
.LC192:
	.string	"des-cfb"
.LC193:
	.string	"des-ede-cfb"
.LC194:
	.string	"des-ede3-cfb"
.LC195:
	.string	"des-ofb"
.LC196:
	.string	"des-ede-ofb"
.LC197:
	.string	"des-ede3-ofb"
.LC198:
	.string	"idea-cbc"
.LC199:
	.string	"idea-ecb"
.LC200:
	.string	"idea-cfb"
.LC201:
	.string	"idea-ofb"
.LC202:
	.string	"seed-cbc"
.LC203:
	.string	"seed-ecb"
.LC204:
	.string	"seed-cfb"
.LC205:
	.string	"seed-ofb"
.LC206:
	.string	"rc2-cbc"
.LC207:
	.string	"rc2-ecb"
.LC208:
	.string	"rc2-cfb"
.LC209:
	.string	"rc2-ofb"
.LC210:
	.string	"rc2-64-cbc"
.LC211:
	.string	"rc2-40-cbc"
.LC212:
	.string	"bf-cbc"
.LC213:
	.string	"bf-ecb"
.LC214:
	.string	"bf-cfb"
.LC215:
	.string	"bf-ofb"
.LC216:
	.string	"cast5-cbc"
.LC217:
	.string	"cast5-ecb"
.LC218:
	.string	"cast5-cfb"
.LC219:
	.string	"cast5-ofb"
.LC220:
	.string	"cast-cbc"
.LC221:
	.string	"sm4-cbc"
.LC222:
	.string	"sm4-ecb"
.LC223:
	.string	"sm4-cfb"
.LC224:
	.string	"sm4-ofb"
.LC225:
	.string	"sm4-ctr"
	.section	.data.rel,"aw"
	.align 32
	.type	functions, @object
	.size	functions, 4896
functions:
	.long	1
	.zero	4
	.quad	.LC75
	.quad	asn1parse_main
	.quad	asn1parse_options
	.long	1
	.zero	4
	.quad	.LC76
	.quad	ca_main
	.quad	ca_options
	.long	1
	.zero	4
	.quad	.LC77
	.quad	ciphers_main
	.quad	ciphers_options
	.long	1
	.zero	4
	.quad	.LC78
	.quad	cms_main
	.quad	cms_options
	.long	1
	.zero	4
	.quad	.LC79
	.quad	crl_main
	.quad	crl_options
	.long	1
	.zero	4
	.quad	.LC80
	.quad	crl2pkcs7_main
	.quad	crl2pkcs7_options
	.long	1
	.zero	4
	.quad	.LC81
	.quad	dgst_main
	.quad	dgst_options
	.long	1
	.zero	4
	.quad	.LC82
	.quad	dhparam_main
	.quad	dhparam_options
	.long	1
	.zero	4
	.quad	.LC83
	.quad	dsa_main
	.quad	dsa_options
	.long	1
	.zero	4
	.quad	.LC84
	.quad	dsaparam_main
	.quad	dsaparam_options
	.long	1
	.zero	4
	.quad	.LC85
	.quad	ec_main
	.quad	ec_options
	.long	1
	.zero	4
	.quad	.LC86
	.quad	ecparam_main
	.quad	ecparam_options
	.long	1
	.zero	4
	.quad	.LC87
	.quad	enc_main
	.quad	enc_options
	.long	1
	.zero	4
	.quad	.LC88
	.quad	engine_main
	.quad	engine_options
	.long	1
	.zero	4
	.quad	.LC89
	.quad	errstr_main
	.quad	errstr_options
	.long	1
	.zero	4
	.quad	.LC90
	.quad	gendsa_main
	.quad	gendsa_options
	.long	1
	.zero	4
	.quad	.LC91
	.quad	genpkey_main
	.quad	genpkey_options
	.long	1
	.zero	4
	.quad	.LC92
	.quad	genrsa_main
	.quad	genrsa_options
	.long	1
	.zero	4
	.quad	.LC51
	.quad	help_main
	.quad	help_options
	.long	1
	.zero	4
	.quad	.LC93
	.quad	list_main
	.quad	list_options
	.long	1
	.zero	4
	.quad	.LC94
	.quad	nseq_main
	.quad	nseq_options
	.long	1
	.zero	4
	.quad	.LC95
	.quad	ocsp_main
	.quad	ocsp_options
	.long	1
	.zero	4
	.quad	.LC96
	.quad	passwd_main
	.quad	passwd_options
	.long	1
	.zero	4
	.quad	.LC97
	.quad	pkcs12_main
	.quad	pkcs12_options
	.long	1
	.zero	4
	.quad	.LC98
	.quad	pkcs7_main
	.quad	pkcs7_options
	.long	1
	.zero	4
	.quad	.LC99
	.quad	pkcs8_main
	.quad	pkcs8_options
	.long	1
	.zero	4
	.quad	.LC100
	.quad	pkey_main
	.quad	pkey_options
	.long	1
	.zero	4
	.quad	.LC101
	.quad	pkeyparam_main
	.quad	pkeyparam_options
	.long	1
	.zero	4
	.quad	.LC102
	.quad	pkeyutl_main
	.quad	pkeyutl_options
	.long	1
	.zero	4
	.quad	.LC103
	.quad	prime_main
	.quad	prime_options
	.long	1
	.zero	4
	.quad	.LC104
	.quad	rand_main
	.quad	rand_options
	.long	1
	.zero	4
	.quad	.LC105
	.quad	rehash_main
	.quad	rehash_options
	.long	1
	.zero	4
	.quad	.LC106
	.quad	req_main
	.quad	req_options
	.long	1
	.zero	4
	.quad	.LC107
	.quad	rsa_main
	.quad	rsa_options
	.long	1
	.zero	4
	.quad	.LC108
	.quad	rsautl_main
	.quad	rsautl_options
	.long	1
	.zero	4
	.quad	.LC109
	.quad	s_client_main
	.quad	s_client_options
	.long	1
	.zero	4
	.quad	.LC110
	.quad	s_server_main
	.quad	s_server_options
	.long	1
	.zero	4
	.quad	.LC111
	.quad	s_time_main
	.quad	s_time_options
	.long	1
	.zero	4
	.quad	.LC112
	.quad	sess_id_main
	.quad	sess_id_options
	.long	1
	.zero	4
	.quad	.LC113
	.quad	smime_main
	.quad	smime_options
	.long	1
	.zero	4
	.quad	.LC114
	.quad	speed_main
	.quad	speed_options
	.long	1
	.zero	4
	.quad	.LC115
	.quad	spkac_main
	.quad	spkac_options
	.long	1
	.zero	4
	.quad	.LC116
	.quad	srp_main
	.quad	srp_options
	.long	1
	.zero	4
	.quad	.LC117
	.quad	storeutl_main
	.quad	storeutl_options
	.long	1
	.zero	4
	.quad	.LC118
	.quad	ts_main
	.quad	ts_options
	.long	1
	.zero	4
	.quad	.LC119
	.quad	verify_main
	.quad	verify_options
	.long	1
	.zero	4
	.quad	.LC120
	.quad	version_main
	.quad	version_options
	.long	1
	.zero	4
	.quad	.LC121
	.quad	x509_main
	.quad	x509_options
	.long	2
	.zero	4
	.quad	.LC122
	.quad	dgst_main
	.zero	8
	.long	2
	.zero	4
	.quad	.LC123
	.quad	dgst_main
	.zero	8
	.long	2
	.zero	4
	.quad	.LC124
	.quad	dgst_main
	.zero	8
	.long	2
	.zero	4
	.quad	.LC125
	.quad	dgst_main
	.zero	8
	.long	2
	.zero	4
	.quad	.LC126
	.quad	dgst_main
	.zero	8
	.long	2
	.zero	4
	.quad	.LC127
	.quad	dgst_main
	.zero	8
	.long	2
	.zero	4
	.quad	.LC128
	.quad	dgst_main
	.zero	8
	.long	2
	.zero	4
	.quad	.LC129
	.quad	dgst_main
	.zero	8
	.long	2
	.zero	4
	.quad	.LC130
	.quad	dgst_main
	.zero	8
	.long	2
	.zero	4
	.quad	.LC131
	.quad	dgst_main
	.zero	8
	.long	2
	.zero	4
	.quad	.LC132
	.quad	dgst_main
	.zero	8
	.long	2
	.zero	4
	.quad	.LC133
	.quad	dgst_main
	.zero	8
	.long	2
	.zero	4
	.quad	.LC134
	.quad	dgst_main
	.zero	8
	.long	2
	.zero	4
	.quad	.LC135
	.quad	dgst_main
	.zero	8
	.long	2
	.zero	4
	.quad	.LC136
	.quad	dgst_main
	.zero	8
	.long	2
	.zero	4
	.quad	.LC137
	.quad	dgst_main
	.zero	8
	.long	2
	.zero	4
	.quad	.LC138
	.quad	dgst_main
	.zero	8
	.long	2
	.zero	4
	.quad	.LC139
	.quad	dgst_main
	.zero	8
	.long	2
	.zero	4
	.quad	.LC140
	.quad	dgst_main
	.zero	8
	.long	2
	.zero	4
	.quad	.LC141
	.quad	dgst_main
	.zero	8
	.long	3
	.zero	4
	.quad	.LC142
	.quad	enc_main
	.quad	enc_options
	.long	3
	.zero	4
	.quad	.LC143
	.quad	enc_main
	.quad	enc_options
	.long	3
	.zero	4
	.quad	.LC144
	.quad	enc_main
	.quad	enc_options
	.long	3
	.zero	4
	.quad	.LC145
	.quad	enc_main
	.quad	enc_options
	.long	3
	.zero	4
	.quad	.LC146
	.quad	enc_main
	.quad	enc_options
	.long	3
	.zero	4
	.quad	.LC147
	.quad	enc_main
	.quad	enc_options
	.long	3
	.zero	4
	.quad	.LC148
	.quad	enc_main
	.quad	enc_options
	.long	3
	.zero	4
	.quad	.LC149
	.quad	enc_main
	.quad	enc_options
	.long	3
	.zero	4
	.quad	.LC150
	.quad	enc_main
	.quad	enc_options
	.long	3
	.zero	4
	.quad	.LC151
	.quad	enc_main
	.quad	enc_options
	.long	3
	.zero	4
	.quad	.LC152
	.quad	enc_main
	.quad	enc_options
	.long	3
	.zero	4
	.quad	.LC153
	.quad	enc_main
	.quad	enc_options
	.long	3
	.zero	4
	.quad	.LC154
	.quad	enc_main
	.quad	enc_options
	.long	3
	.zero	4
	.quad	.LC155
	.quad	enc_main
	.quad	enc_options
	.long	3
	.zero	4
	.quad	.LC156
	.quad	enc_main
	.quad	enc_options
	.long	3
	.zero	4
	.quad	.LC157
	.quad	enc_main
	.quad	enc_options
	.long	3
	.zero	4
	.quad	.LC158
	.quad	enc_main
	.quad	enc_options
	.long	3
	.zero	4
	.quad	.LC159
	.quad	enc_main
	.quad	enc_options
	.long	3
	.zero	4
	.quad	.LC160
	.quad	enc_main
	.quad	enc_options
	.long	3
	.zero	4
	.quad	.LC161
	.quad	enc_main
	.quad	enc_options
	.long	3
	.zero	4
	.quad	.LC162
	.quad	enc_main
	.quad	enc_options
	.long	3
	.zero	4
	.quad	.LC163
	.quad	enc_main
	.quad	enc_options
	.long	3
	.zero	4
	.quad	.LC164
	.quad	enc_main
	.quad	enc_options
	.long	3
	.zero	4
	.quad	.LC165
	.quad	enc_main
	.quad	enc_options
	.long	3
	.zero	4
	.quad	.LC166
	.quad	enc_main
	.quad	enc_options
	.long	3
	.zero	4
	.quad	.LC167
	.quad	enc_main
	.quad	enc_options
	.long	3
	.zero	4
	.quad	.LC168
	.quad	enc_main
	.quad	enc_options
	.long	3
	.zero	4
	.quad	.LC169
	.quad	enc_main
	.quad	enc_options
	.long	3
	.zero	4
	.quad	.LC170
	.quad	enc_main
	.quad	enc_options
	.long	3
	.zero	4
	.quad	.LC171
	.quad	enc_main
	.quad	enc_options
	.long	3
	.zero	4
	.quad	.LC172
	.quad	enc_main
	.quad	enc_options
	.long	3
	.zero	4
	.quad	.LC173
	.quad	enc_main
	.quad	enc_options
	.long	3
	.zero	4
	.quad	.LC174
	.quad	enc_main
	.quad	enc_options
	.long	3
	.zero	4
	.quad	.LC175
	.quad	enc_main
	.quad	enc_options
	.long	3
	.zero	4
	.quad	.LC176
	.quad	enc_main
	.quad	enc_options
	.long	3
	.zero	4
	.quad	.LC177
	.quad	enc_main
	.quad	enc_options
	.long	3
	.zero	4
	.quad	.LC178
	.quad	enc_main
	.quad	enc_options
	.long	3
	.zero	4
	.quad	.LC179
	.quad	enc_main
	.quad	enc_options
	.long	3
	.zero	4
	.quad	.LC180
	.quad	enc_main
	.quad	enc_options
	.long	3
	.zero	4
	.quad	.LC181
	.quad	enc_main
	.quad	enc_options
	.long	3
	.zero	4
	.quad	.LC182
	.quad	enc_main
	.quad	enc_options
	.long	3
	.zero	4
	.quad	.LC183
	.quad	enc_main
	.quad	enc_options
	.long	3
	.zero	4
	.quad	.LC184
	.quad	enc_main
	.quad	enc_options
	.long	3
	.zero	4
	.quad	.LC185
	.quad	enc_main
	.quad	enc_options
	.long	3
	.zero	4
	.quad	.LC186
	.quad	enc_main
	.quad	enc_options
	.long	3
	.zero	4
	.quad	.LC187
	.quad	enc_main
	.quad	enc_options
	.long	3
	.zero	4
	.quad	.LC188
	.quad	enc_main
	.quad	enc_options
	.long	3
	.zero	4
	.quad	.LC189
	.quad	enc_main
	.quad	enc_options
	.long	3
	.zero	4
	.quad	.LC190
	.quad	enc_main
	.quad	enc_options
	.long	3
	.zero	4
	.quad	.LC191
	.quad	enc_main
	.quad	enc_options
	.long	3
	.zero	4
	.quad	.LC192
	.quad	enc_main
	.quad	enc_options
	.long	3
	.zero	4
	.quad	.LC193
	.quad	enc_main
	.quad	enc_options
	.long	3
	.zero	4
	.quad	.LC194
	.quad	enc_main
	.quad	enc_options
	.long	3
	.zero	4
	.quad	.LC195
	.quad	enc_main
	.quad	enc_options
	.long	3
	.zero	4
	.quad	.LC196
	.quad	enc_main
	.quad	enc_options
	.long	3
	.zero	4
	.quad	.LC197
	.quad	enc_main
	.quad	enc_options
	.long	3
	.zero	4
	.quad	.LC198
	.quad	enc_main
	.quad	enc_options
	.long	3
	.zero	4
	.quad	.LC199
	.quad	enc_main
	.quad	enc_options
	.long	3
	.zero	4
	.quad	.LC200
	.quad	enc_main
	.quad	enc_options
	.long	3
	.zero	4
	.quad	.LC201
	.quad	enc_main
	.quad	enc_options
	.long	3
	.zero	4
	.quad	.LC202
	.quad	enc_main
	.quad	enc_options
	.long	3
	.zero	4
	.quad	.LC203
	.quad	enc_main
	.quad	enc_options
	.long	3
	.zero	4
	.quad	.LC204
	.quad	enc_main
	.quad	enc_options
	.long	3
	.zero	4
	.quad	.LC205
	.quad	enc_main
	.quad	enc_options
	.long	3
	.zero	4
	.quad	.LC206
	.quad	enc_main
	.quad	enc_options
	.long	3
	.zero	4
	.quad	.LC207
	.quad	enc_main
	.quad	enc_options
	.long	3
	.zero	4
	.quad	.LC208
	.quad	enc_main
	.quad	enc_options
	.long	3
	.zero	4
	.quad	.LC209
	.quad	enc_main
	.quad	enc_options
	.long	3
	.zero	4
	.quad	.LC210
	.quad	enc_main
	.quad	enc_options
	.long	3
	.zero	4
	.quad	.LC211
	.quad	enc_main
	.quad	enc_options
	.long	3
	.zero	4
	.quad	.LC212
	.quad	enc_main
	.quad	enc_options
	.long	3
	.zero	4
	.quad	.LC213
	.quad	enc_main
	.quad	enc_options
	.long	3
	.zero	4
	.quad	.LC214
	.quad	enc_main
	.quad	enc_options
	.long	3
	.zero	4
	.quad	.LC215
	.quad	enc_main
	.quad	enc_options
	.long	3
	.zero	4
	.quad	.LC216
	.quad	enc_main
	.quad	enc_options
	.long	3
	.zero	4
	.quad	.LC217
	.quad	enc_main
	.quad	enc_options
	.long	3
	.zero	4
	.quad	.LC218
	.quad	enc_main
	.quad	enc_options
	.long	3
	.zero	4
	.quad	.LC219
	.quad	enc_main
	.quad	enc_options
	.long	3
	.zero	4
	.quad	.LC220
	.quad	enc_main
	.quad	enc_options
	.long	3
	.zero	4
	.quad	.LC221
	.quad	enc_main
	.quad	enc_options
	.long	3
	.zero	4
	.quad	.LC222
	.quad	enc_main
	.quad	enc_options
	.long	3
	.zero	4
	.quad	.LC223
	.quad	enc_main
	.quad	enc_options
	.long	3
	.zero	4
	.quad	.LC224
	.quad	enc_main
	.quad	enc_options
	.long	3
	.zero	4
	.quad	.LC225
	.quad	enc_main
	.quad	enc_options
	.long	0
	.zero	4
	.quad	0
	.quad	0
	.zero	8
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
