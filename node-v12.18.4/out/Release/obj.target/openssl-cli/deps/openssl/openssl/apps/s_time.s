	.file	"s_time.c"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"ERROR\n"
.LC1:
	.string	"verify error:%s\n"
	.text
	.p2align 4
	.type	doConnection, @function
doConnection:
.LFB1560:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	BIO_s_connect@PLT
	movq	%rax, %rdi
	call	BIO_new@PLT
	testq	%rax, %rax
	je	.L13
	xorl	%edx, %edx
	movq	%rax, %r12
	movq	%r14, %rcx
	movl	$100, %esi
	movq	%rax, %rdi
	call	BIO_ctrl@PLT
	xorl	%ecx, %ecx
	movl	$16, %edx
	movq	%r12, %rdi
	movl	$155, %esi
	call	BIO_ctrl@PLT
	testq	%rbx, %rbx
	je	.L15
	movq	%rbx, %rdi
	movq	%rbx, %r13
	call	SSL_set_connect_state@PLT
.L5:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movq	%r12, %rsi
	call	SSL_set_bio@PLT
	movq	%r13, %rdi
	call	SSL_connect@PLT
	testl	%eax, %eax
	jle	.L16
	movq	%r13, %rdi
	movq	$1, -48(%rbp)
	call	SSL_get_fd@PLT
	movl	%eax, %edi
	testl	%eax, %eax
	jns	.L17
.L1:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L18
	addq	$16, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	.cfi_restore_state
	leaq	-48(%rbp), %rcx
	movl	$8, %r8d
	movl	$13, %edx
	movl	$1, %esi
	call	setsockopt@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L16:
	movq	bio_err(%rip), %rdi
	leaq	.LC0(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movslq	8+verify_args(%rip), %rdi
	testl	%edi, %edi
	je	.L7
	call	X509_verify_cert_error_string@PLT
	movq	bio_err(%rip), %rdi
	leaq	.LC1(%rip), %rsi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L8:
	testq	%rbx, %rbx
	jne	.L13
	movq	%r13, %rdi
	call	SSL_free@PLT
	.p2align 4,,10
	.p2align 3
.L13:
	xorl	%r13d, %r13d
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L15:
	movq	%r13, %rdi
	call	SSL_new@PLT
	movq	%rax, %r13
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L7:
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	jmp	.L8
.L18:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1560:
	.size	doConnection, .-doConnection
	.section	.rodata.str1.1
.LC2:
	.string	"localhost:4433"
.LC3:
	.string	"%s: Use -help for summary.\n"
.LC4:
	.string	"%s: verify depth is %d\n"
.LC5:
	.string	"%s: -www option is too long\n"
.LC6:
	.string	"SSL_CIPHER"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC7:
	.string	"Collecting connection statistics for %d seconds\n"
	.align 8
.LC9:
	.string	"\n\n%d connections in %.2fs; %.2f connections/user sec, bytes read %ld\n"
	.align 8
.LC10:
	.string	"%d connections in %ld real seconds, %ld bytes read per connection\n"
	.align 8
.LC11:
	.string	"\n\nNow timing with session id reuse."
	.section	.rodata.str1.1
.LC12:
	.string	"Unable to get connection\n"
.LC13:
	.string	"starting"
	.text
	.p2align 4
	.globl	s_time_main
	.type	s_time_main, @function
s_time_main:
.LFB1559:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$120, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	$3, %r14d
	xorl	%r15d, %r15d
	leaq	.L23(%rip), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	%edi, %r12d
	movq	%rsi, %r13
	movl	$30, -8260(%rbp)
	call	TLS_client_method@PLT
	movq	%r13, %rsi
	movl	%r12d, %edi
	leaq	s_time_options(%rip), %rdx
	movq	%rax, -8352(%rbp)
	xorl	%r13d, %r13d
	call	opt_init@PLT
	movl	$0, -8288(%rbp)
	movl	$0, -8292(%rbp)
	movq	%rax, %r12
	leaq	.LC2(%rip), %rax
	movl	$0, -8340(%rbp)
	movl	$0, -8296(%rbp)
	movq	$0, -8320(%rbp)
	movq	$0, -8312(%rbp)
	movq	%rax, -8280(%rbp)
	movq	$0, -8304(%rbp)
	movq	$0, -8336(%rbp)
	movq	$0, -8328(%rbp)
.L20:
	call	opt_next@PLT
	testl	%eax, %eax
	je	.L124
.L45:
	addl	$1, %eax
	cmpl	$19, %eax
	ja	.L20
	movslq	(%rbx,%rax,4), %rax
	addq	%rbx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L23:
	.long	.L41-.L23
	.long	.L20-.L23
	.long	.L40-.L23
	.long	.L39-.L23
	.long	.L38-.L23
	.long	.L37-.L23
	.long	.L36-.L23
	.long	.L35-.L23
	.long	.L34-.L23
	.long	.L33-.L23
	.long	.L32-.L23
	.long	.L31-.L23
	.long	.L30-.L23
	.long	.L29-.L23
	.long	.L28-.L23
	.long	.L27-.L23
	.long	.L26-.L23
	.long	.L25-.L23
	.long	.L24-.L23
	.long	.L22-.L23
	.text
.L28:
	movl	$2, %r14d
	call	opt_next@PLT
	testl	%eax, %eax
	jne	.L45
.L124:
	call	opt_num_rest@PLT
	testl	%eax, %eax
	jne	.L41
	testq	%r13, %r13
	je	.L125
.L46:
	movq	-8352(%rbp), %rdi
	call	SSL_CTX_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L123
	xorl	%ecx, %ecx
	movl	$4, %edx
	movl	$33, %esi
	movq	%rax, %rdi
	call	SSL_CTX_ctrl@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	call	SSL_CTX_set_quiet_shutdown@PLT
	xorl	%ecx, %ecx
	movl	$124, %esi
	movq	%r12, %rdi
	movslq	-8288(%rbp), %rdx
	call	SSL_CTX_ctrl@PLT
	testq	%rax, %rax
	je	.L79
	movl	-8292(%rbp), %eax
	testl	%eax, %eax
	jne	.L126
.L48:
	testq	%r13, %r13
	je	.L52
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	SSL_CTX_set_cipher_list@PLT
	testl	%eax, %eax
	je	.L79
.L52:
	testq	%r15, %r15
	je	.L51
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	SSL_CTX_set_ciphersuites@PLT
	testl	%eax, %eax
	je	.L79
.L51:
	movq	-8320(%rbp), %rdx
	movq	-8312(%rbp), %rsi
	movq	%r12, %rdi
	call	set_cert_stuff@PLT
	testl	%eax, %eax
	je	.L79
	movl	-8296(%rbp), %r8d
	movl	-8340(%rbp), %ecx
	movq	%r12, %rdi
	movq	-8328(%rbp), %rdx
	movq	-8336(%rbp), %rsi
	call	ctx_set_verify_locations@PLT
	testl	%eax, %eax
	je	.L127
	movl	%r14d, %r13d
	andl	$1, %r13d
	je	.L55
	movl	-8260(%rbp), %edx
	leaq	.LC7(%rip), %rsi
	movl	$1, %edi
	xorl	%eax, %eax
	leaq	-8256(%rbp), %rbx
	call	__printf_chk@PLT
	xorl	%edi, %edi
	call	time@PLT
	movslq	-8260(%rbp), %rdx
	movl	$1, %esi
	xorl	%edi, %edi
	addq	%rdx, %rax
	movq	%rax, -8288(%rbp)
	call	app_tminterval@PLT
	xorl	%eax, %eax
	movl	%r14d, -8320(%rbp)
	movq	-8304(%rbp), %r14
	movl	%r13d, -8312(%rbp)
	movq	%rax, %r13
	movl	$0, -8292(%rbp)
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L57:
	movl	$3, %esi
	movq	%r15, %rdi
	call	SSL_set_shutdown@PLT
	movq	%r15, %rdi
	call	SSL_get_fd@PLT
	movl	%eax, %edi
	call	BIO_closesocket@PLT
	movq	%r15, %rdi
	addl	$1, -8292(%rbp)
	call	SSL_session_reused@PLT
	movl	$114, %edi
	testl	%eax, %eax
	je	.L128
.L60:
	movq	stdout(%rip), %rsi
	call	fputc@PLT
	movq	stdout(%rip), %rdi
	call	fflush@PLT
	movq	%r15, %rdi
	call	SSL_free@PLT
.L61:
	xorl	%edi, %edi
	call	time@PLT
	cmpq	-8288(%rbp), %rax
	jg	.L56
	movq	-8280(%rbp), %rsi
	movq	%r12, %rdx
	xorl	%edi, %edi
	call	doConnection
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L119
	testq	%r14, %r14
	je	.L57
	leaq	fmt_http_get_cmd(%rip), %rdx
	movq	%r14, %rcx
	movq	%rbx, %rdi
	xorl	%eax, %eax
	movl	$8192, %esi
	call	BIO_snprintf@PLT
	movl	%eax, %edx
	testl	%eax, %eax
	jle	.L119
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	SSL_write@PLT
	testl	%eax, %eax
	jg	.L58
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L59:
	cltq
	addq	%rax, %r13
.L58:
	movl	$8192, %edx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	SSL_read@PLT
	testl	%eax, %eax
	jg	.L59
	jmp	.L57
.L25:
	call	opt_arg@PLT
	leaq	-8260(%rbp), %rsi
	movq	%rax, %rdi
	call	opt_int@PLT
	testl	%eax, %eax
	jne	.L20
.L41:
	movq	%r12, %rdx
	leaq	.LC3(%rip), %rsi
.L122:
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L123:
	movl	$1, %r13d
	xorl	%r12d, %r12d
	xorl	%r15d, %r15d
.L42:
	movq	%r15, %rdi
	call	SSL_free@PLT
	movq	%r12, %rdi
	call	SSL_CTX_free@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L129
	addq	$8312, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L22:
	.cfi_restore_state
	call	opt_arg@PLT
	movq	%rax, %rdi
	movq	%rax, -8304(%rbp)
	call	strlen@PLT
	addq	$18, %rax
	cmpq	$8192, %rax
	jbe	.L20
	movq	%r12, %rdx
	leaq	.LC5(%rip), %rsi
	jmp	.L122
.L24:
	movl	$768, -8288(%rbp)
	jmp	.L20
.L26:
	call	opt_arg@PLT
	leaq	verify_args(%rip), %rsi
	movq	%rax, %rdi
	call	opt_int@PLT
	testl	%eax, %eax
	je	.L41
	movl	verify_args(%rip), %ecx
	movq	bio_err(%rip), %rdi
	movq	%r12, %rdx
	xorl	%eax, %eax
	leaq	.LC4(%rip), %rsi
	call	BIO_printf@PLT
	jmp	.L20
.L27:
	movl	$1, -8292(%rbp)
	jmp	.L20
.L29:
	movl	$1, %r14d
	jmp	.L20
.L30:
	movl	$1, -8340(%rbp)
	jmp	.L20
.L31:
	movl	$1, -8296(%rbp)
	jmp	.L20
.L32:
	call	opt_arg@PLT
	movq	%rax, -8336(%rbp)
	jmp	.L20
.L33:
	call	opt_arg@PLT
	movq	%rax, -8328(%rbp)
	jmp	.L20
.L34:
	call	opt_arg@PLT
	movq	%rax, -8320(%rbp)
	jmp	.L20
.L35:
	call	opt_arg@PLT
	movq	%rax, %rdi
	call	set_nameopt@PLT
	testl	%eax, %eax
	jne	.L20
	jmp	.L123
.L36:
	call	opt_arg@PLT
	movq	%rax, -8312(%rbp)
	jmp	.L20
.L37:
	call	opt_arg@PLT
	movq	%rax, %r15
	jmp	.L20
.L38:
	call	opt_arg@PLT
	movq	%rax, %r13
	jmp	.L20
.L39:
	call	opt_arg@PLT
	movq	%rax, -8280(%rbp)
	jmp	.L20
.L40:
	leaq	s_time_options(%rip), %rdi
	xorl	%r13d, %r13d
	xorl	%r12d, %r12d
	xorl	%r15d, %r15d
	call	opt_help@PLT
	jmp	.L42
.L79:
	movl	$1, %r13d
	xorl	%r15d, %r15d
	jmp	.L42
.L128:
	movq	%r15, %rdi
	call	SSL_version@PLT
	movl	$116, %edi
	cmpl	$769, %eax
	je	.L60
	xorl	%edi, %edi
	cmpl	$768, %eax
	sete	%dil
	leal	42(%rdi,%rdi,8), %edi
	jmp	.L60
.L125:
	leaq	.LC6(%rip), %rdi
	call	getenv@PLT
	movq	%rax, %r13
	jmp	.L46
.L126:
	movl	$2147485780, %esi
	movq	%r12, %rdi
	call	SSL_CTX_set_options@PLT
	jmp	.L48
.L56:
	movl	$1, %esi
	movl	$1, %edi
	movl	-8320(%rbp), %r14d
	call	app_tminterval@PLT
	addsd	.LC8(%rip), %xmm0
	xorl	%edi, %edi
	movsd	%xmm0, -8312(%rbp)
	call	time@PLT
	movl	-8292(%rbp), %ebx
	pxor	%xmm1, %xmm1
	movq	%r13, %rcx
	movsd	-8312(%rbp), %xmm0
	movl	$1, %edi
	movl	$2, %eax
	leaq	.LC9(%rip), %rsi
	cvtsi2sdl	%ebx, %xmm1
	movl	%ebx, %edx
	divsd	%xmm0, %xmm1
	call	__printf_chk@PLT
	movq	%r13, %rax
	movslq	%ebx, %rcx
	xorl	%edi, %edi
	cqto
	idivq	%rcx
	movq	%rax, %r13
	call	time@PLT
	movq	%r13, %r8
	movl	%ebx, %edx
	movl	$1, %edi
	subq	-8288(%rbp), %rax
	leaq	.LC10(%rip), %rsi
	movq	%rax, %rcx
	movslq	-8260(%rbp), %rax
	addq	%rax, %rcx
	xorl	%eax, %eax
	call	__printf_chk@PLT
.L55:
	andl	$2, %r14d
	je	.L79
	leaq	.LC11(%rip), %rdi
	call	puts@PLT
	movq	-8280(%rbp), %rsi
	movq	%r12, %rdx
	xorl	%edi, %edi
	call	doConnection
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L130
	movq	-8304(%rbp), %rcx
	testq	%rcx, %rcx
	je	.L63
	leaq	-8256(%rbp), %rbx
	leaq	fmt_http_get_cmd(%rip), %rdx
	movl	$8192, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movl	$1, %r13d
	call	BIO_snprintf@PLT
	movl	%eax, %edx
	testl	%eax, %eax
	jle	.L42
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	SSL_write@PLT
	testl	%eax, %eax
	jle	.L42
.L64:
	movl	$8192, %edx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	SSL_read@PLT
	testl	%eax, %eax
	jg	.L64
.L63:
	movl	$3, %esi
	movq	%r15, %rdi
	xorl	%r13d, %r13d
	call	SSL_set_shutdown@PLT
	movq	%r15, %rdi
	leaq	-8256(%rbp), %rbx
	call	SSL_get_fd@PLT
	movl	%eax, %edi
	call	BIO_closesocket@PLT
	xorl	%edi, %edi
	call	time@PLT
	movslq	-8260(%rbp), %rdx
	leaq	.LC13(%rip), %rdi
	leaq	(%rdx,%rax), %r14
	call	puts@PLT
	movl	$1, %esi
	xorl	%edi, %edi
	call	app_tminterval@PLT
	movl	$0, -8288(%rbp)
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L66:
	movl	$3, %esi
	movq	%r15, %rdi
	call	SSL_set_shutdown@PLT
	movq	%r15, %rdi
	call	SSL_get_fd@PLT
	movl	%eax, %edi
	call	BIO_closesocket@PLT
	movq	%r15, %rdi
	addl	$1, -8288(%rbp)
	call	SSL_session_reused@PLT
	movl	$114, %edi
	testl	%eax, %eax
	je	.L131
.L69:
	movq	stdout(%rip), %rsi
	call	fputc@PLT
	movq	stdout(%rip), %rdi
	call	fflush@PLT
.L70:
	xorl	%edi, %edi
	call	time@PLT
	cmpq	%r14, %rax
	jg	.L65
	movq	-8280(%rbp), %rsi
	movq	%r12, %rdx
	movq	%r15, %rdi
	call	doConnection
	testq	%rax, %rax
	je	.L84
	movq	-8304(%rbp), %rcx
	testq	%rcx, %rcx
	je	.L66
	leaq	fmt_http_get_cmd(%rip), %rdx
	movl	$8192, %esi
	movq	%rbx, %rdi
	xorl	%eax, %eax
	call	BIO_snprintf@PLT
	movl	%eax, %edx
	testl	%eax, %eax
	jle	.L84
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	SSL_write@PLT
	testl	%eax, %eax
	jg	.L67
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L68:
	cltq
	addq	%rax, %r13
.L67:
	movl	$8192, %edx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	SSL_read@PLT
	testl	%eax, %eax
	jg	.L68
	jmp	.L66
.L131:
	movq	%r15, %rdi
	call	SSL_version@PLT
	movl	$116, %edi
	cmpl	$769, %eax
	je	.L69
	xorl	%edi, %edi
	cmpl	$768, %eax
	sete	%dil
	leal	42(%rdi,%rdi,8), %edi
	jmp	.L69
.L119:
	movl	-8312(%rbp), %r13d
	jmp	.L42
.L127:
	movq	bio_err(%rip), %rdi
	movl	$1, %r13d
	xorl	%r15d, %r15d
	call	ERR_print_errors@PLT
	jmp	.L42
.L84:
	movl	$1, %r13d
	jmp	.L42
.L65:
	movl	$1, %esi
	movl	$1, %edi
	call	app_tminterval@PLT
	movl	-8288(%rbp), %ebx
	pxor	%xmm1, %xmm1
	movq	%r13, %rcx
	addsd	.LC8(%rip), %xmm0
	movl	$1, %edi
	movl	$2, %eax
	cvtsi2sdl	%ebx, %xmm1
	leaq	.LC9(%rip), %rsi
	movl	%ebx, %edx
	divsd	%xmm0, %xmm1
	call	__printf_chk@PLT
	movq	%r13, %rax
	movslq	%ebx, %rcx
	xorl	%edi, %edi
	cqto
	idivq	%rcx
	movq	%rax, %r13
	call	time@PLT
	movq	%r13, %r8
	movl	%ebx, %edx
	movl	$1, %edi
	subq	%r14, %rax
	leaq	.LC10(%rip), %rsi
	xorl	%r13d, %r13d
	movq	%rax, %rcx
	movslq	-8260(%rbp), %rax
	addq	%rax, %rcx
	xorl	%eax, %eax
	call	__printf_chk@PLT
	jmp	.L42
.L130:
	movq	bio_err(%rip), %rdi
	leaq	.LC12(%rip), %rsi
	xorl	%eax, %eax
	movl	$1, %r13d
	call	BIO_printf@PLT
	jmp	.L42
.L129:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1559:
	.size	s_time_main, .-s_time_main
	.globl	s_time_options
	.section	.rodata.str1.1
.LC14:
	.string	"help"
.LC15:
	.string	"Display this summary"
.LC16:
	.string	"connect"
	.section	.rodata.str1.8
	.align 8
.LC17:
	.string	"Where to connect as post:port (default is localhost:4433)"
	.section	.rodata.str1.1
.LC18:
	.string	"cipher"
	.section	.rodata.str1.8
	.align 8
.LC19:
	.string	"TLSv1.2 and below cipher list to be used"
	.section	.rodata.str1.1
.LC20:
	.string	"ciphersuites"
	.section	.rodata.str1.8
	.align 8
.LC21:
	.string	"Specify TLSv1.3 ciphersuites to be used"
	.section	.rodata.str1.1
.LC22:
	.string	"cert"
	.section	.rodata.str1.8
	.align 8
.LC23:
	.string	"Cert file to use, PEM format assumed"
	.section	.rodata.str1.1
.LC24:
	.string	"nameopt"
	.section	.rodata.str1.8
	.align 8
.LC25:
	.string	"Various certificate name options"
	.section	.rodata.str1.1
.LC26:
	.string	"key"
	.section	.rodata.str1.8
	.align 8
.LC27:
	.string	"File with key, PEM; default is -cert file"
	.section	.rodata.str1.1
.LC28:
	.string	"CApath"
.LC29:
	.string	"PEM format directory of CA's"
.LC30:
	.string	"cafile"
.LC31:
	.string	"PEM format file of CA's"
.LC32:
	.string	"CAfile"
.LC33:
	.string	"no-CAfile"
	.section	.rodata.str1.8
	.align 8
.LC34:
	.string	"Do not load the default certificates file"
	.section	.rodata.str1.1
.LC35:
	.string	"no-CApath"
	.section	.rodata.str1.8
	.align 8
.LC36:
	.string	"Do not load certificates from the default certificates directory"
	.section	.rodata.str1.1
.LC37:
	.string	"new"
.LC38:
	.string	"Just time new connections"
.LC39:
	.string	"reuse"
.LC40:
	.string	"Just time connection reuse"
.LC41:
	.string	"bugs"
.LC42:
	.string	"Turn on SSL bug compatibility"
.LC43:
	.string	"verify"
	.section	.rodata.str1.8
	.align 8
.LC44:
	.string	"Turn on peer certificate verification, set depth"
	.section	.rodata.str1.1
.LC45:
	.string	"time"
	.section	.rodata.str1.8
	.align 8
.LC46:
	.string	"Seconds to collect data, default 30"
	.section	.rodata.str1.1
.LC47:
	.string	"www"
	.section	.rodata.str1.8
	.align 8
.LC48:
	.string	"Fetch specified page from the site"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	s_time_options, @object
	.size	s_time_options, 456
s_time_options:
	.quad	.LC14
	.long	1
	.long	45
	.quad	.LC15
	.quad	.LC16
	.long	2
	.long	115
	.quad	.LC17
	.quad	.LC18
	.long	3
	.long	115
	.quad	.LC19
	.quad	.LC20
	.long	4
	.long	115
	.quad	.LC21
	.quad	.LC22
	.long	5
	.long	60
	.quad	.LC23
	.quad	.LC24
	.long	6
	.long	115
	.quad	.LC25
	.quad	.LC26
	.long	7
	.long	60
	.quad	.LC27
	.quad	.LC28
	.long	8
	.long	47
	.quad	.LC29
	.quad	.LC30
	.long	9
	.long	60
	.quad	.LC31
	.quad	.LC32
	.long	9
	.long	60
	.quad	.LC31
	.quad	.LC33
	.long	11
	.long	45
	.quad	.LC34
	.quad	.LC35
	.long	10
	.long	45
	.quad	.LC36
	.quad	.LC37
	.long	12
	.long	45
	.quad	.LC38
	.quad	.LC39
	.long	13
	.long	45
	.quad	.LC40
	.quad	.LC41
	.long	14
	.long	45
	.quad	.LC42
	.quad	.LC43
	.long	15
	.long	112
	.quad	.LC44
	.quad	.LC45
	.long	16
	.long	112
	.quad	.LC46
	.quad	.LC47
	.long	18
	.long	115
	.quad	.LC48
	.quad	0
	.zero	16
	.section	.rodata
	.align 16
	.type	fmt_http_get_cmd, @object
	.size	fmt_http_get_cmd, 20
fmt_http_get_cmd:
	.string	"GET %s HTTP/1.0\r\n\r\n"
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC8:
	.long	0
	.long	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
