	.file	"pkcs8.c"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"%s: Use -help for summary.\n"
.LC1:
	.string	"%s: Unknown PBE algorithm %s\n"
.LC2:
	.string	"%s: Unknown PRF algorithm %s\n"
.LC3:
	.string	"Error getting passwords\n"
.LC4:
	.string	"key"
.LC5:
	.string	"Error converting key\n"
.LC6:
	.string	"Bad format specified for key\n"
.LC7:
	.string	"Error setting PBE algorithm\n"
.LC8:
	.string	"Enter Encryption Password:"
.LC9:
	.string	"Error encrypting key\n"
.LC10:
	.string	"Error reading key\n"
.LC11:
	.string	"Enter Password:"
.LC12:
	.string	"Can't read Password\n"
.LC13:
	.string	"Error decrypting key\n"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC14:
	.string	"../deps/openssl/openssl/apps/pkcs8.c"
	.text
	.p2align 4
	.globl	pkcs8_main
	.type	pkcs8_main, @function
pkcs8_main:
.LFB1459:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	pkcs8_options(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	.L7(%rip), %r14
	pushq	%r13
	.cfi_offset 13, -40
	xorl	%r13d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	xorl	%ebx, %ebx
	subq	$1160, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -1136(%rbp)
	movq	$0, -1128(%rbp)
	movq	$0, -1120(%rbp)
	movl	$2048, -1148(%rbp)
	movl	$32773, -1144(%rbp)
	movl	$32773, -1140(%rbp)
	movq	$0, -1112(%rbp)
	movq	$0, -1104(%rbp)
	movq	$0, -1096(%rbp)
	call	opt_init@PLT
	movl	$0, -1196(%rbp)
	movl	$-1, -1160(%rbp)
	movq	%rax, %r15
	movq	$0, -1168(%rbp)
	movq	$0, -1176(%rbp)
	movq	$0, -1192(%rbp)
	movq	$0, -1184(%rbp)
.L2:
	call	opt_next@PLT
	testl	%eax, %eax
	je	.L111
.L31:
	cmpl	$20, %eax
	jg	.L3
	cmpl	$-1, %eax
	jl	.L2
	addl	$1, %eax
	cmpl	$21, %eax
	ja	.L2
	movslq	(%r14,%rax,4), %rax
	addq	%r14, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L7:
	.long	.L26-.L7
	.long	.L2-.L7
	.long	.L25-.L7
	.long	.L24-.L7
	.long	.L23-.L7
	.long	.L22-.L7
	.long	.L21-.L7
	.long	.L20-.L7
	.long	.L19-.L7
	.long	.L18-.L7
	.long	.L63-.L7
	.long	.L17-.L7
	.long	.L16-.L7
	.long	.L15-.L7
	.long	.L14-.L7
	.long	.L13-.L7
	.long	.L12-.L7
	.long	.L11-.L7
	.long	.L10-.L7
	.long	.L9-.L7
	.long	.L8-.L7
	.long	.L6-.L7
	.text
.L63:
	movl	$1, %ebx
	call	opt_next@PLT
	testl	%eax, %eax
	jne	.L31
.L111:
	call	opt_num_rest@PLT
	testl	%eax, %eax
	jne	.L26
	movq	-1168(%rbp), %rsi
	movq	-1176(%rbp), %rdi
	leaq	-1120(%rbp), %rcx
	leaq	-1128(%rbp), %rdx
	call	app_passwd@PLT
	testl	%eax, %eax
	je	.L112
	cmpl	$-1, -1160(%rbp)
	je	.L113
.L33:
	movl	-1144(%rbp), %edx
	movq	-1184(%rbp), %rdi
	movl	$114, %esi
	call	bio_open_default@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L105
	movl	-1140(%rbp), %esi
	movq	-1192(%rbp), %rdi
	movl	$1, %edx
	call	bio_open_owner@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L64
	movl	-1144(%rbp), %esi
	testl	%r12d, %r12d
	jne	.L114
	testl	%ebx, %ebx
	je	.L48
	cmpl	$32773, %esi
	je	.L115
	cmpl	$4, %esi
	jne	.L51
	xorl	%esi, %esi
	movq	%r14, %rdi
	xorl	%ebx, %ebx
	call	d2i_PKCS8_PRIV_KEY_INFO_bio@PLT
	movq	%rax, %r10
.L50:
	testq	%r10, %r10
	je	.L116
	movq	%r10, %rdi
	movq	%r10, -1160(%rbp)
	call	EVP_PKCS82PKEY@PLT
	movq	-1160(%rbp), %r10
	testq	%rax, %rax
	movq	%rax, %r11
	je	.L117
	movl	-1140(%rbp), %eax
	cmpl	$32773, %eax
	je	.L118
	movq	%r10, -1168(%rbp)
	cmpl	$4, %eax
	jne	.L61
	movq	%r11, %rsi
	movq	%r15, %rdi
	movq	%r11, -1160(%rbp)
	call	i2d_PrivateKey_bio@PLT
	movq	-1160(%rbp), %r11
	movq	-1168(%rbp), %r10
	jmp	.L28
.L19:
	movl	$1, %r12d
	jmp	.L2
.L12:
	call	opt_arg@PLT
	movq	%rax, %rdi
	call	OBJ_txt2nid@PLT
	movl	%eax, -1160(%rbp)
	testl	%eax, %eax
	jne	.L2
	call	opt_arg@PLT
	movq	bio_err(%rip), %rdi
	movq	%r15, %rdx
	leaq	.LC1(%rip), %rsi
	movq	%rax, %rcx
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L26:
	movq	bio_err(%rip), %rdi
	movq	%r15, %rdx
	leaq	.LC0(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L105:
	movl	$1, %r12d
.L107:
	xorl	%ebx, %ebx
	xorl	%r10d, %r10d
	xorl	%r11d, %r11d
	xorl	%r15d, %r15d
	xorl	%r14d, %r14d
.L28:
	movq	%rbx, %rdi
	movq	%r11, -1160(%rbp)
	movq	%r10, -1168(%rbp)
	call	X509_SIG_free@PLT
	movq	-1168(%rbp), %r10
	movq	%r10, %rdi
	call	PKCS8_PRIV_KEY_INFO_free@PLT
	movq	-1160(%rbp), %r11
	movq	%r11, %rdi
	call	EVP_PKEY_free@PLT
	movq	%r13, %rdi
	call	release_engine@PLT
	movq	%r15, %rdi
	call	BIO_free_all@PLT
	movq	%r14, %rdi
	call	BIO_free@PLT
	movq	-1128(%rbp), %rdi
	movl	$355, %edx
	leaq	.LC14(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-1120(%rbp), %rdi
	movl	$356, %edx
	leaq	.LC14(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L119
	leaq	-40(%rbp), %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	.cfi_restore_state
	leal	-1501(%rax), %edx
	cmpl	$1, %edx
	ja	.L2
	movl	%eax, %edi
	call	opt_rand@PLT
	testl	%eax, %eax
	jne	.L2
	jmp	.L105
.L6:
	movl	$1, -1196(%rbp)
	jmp	.L2
.L8:
	call	opt_arg@PLT
	movq	%rax, -1168(%rbp)
	jmp	.L2
.L9:
	call	opt_arg@PLT
	movq	%rax, -1176(%rbp)
	jmp	.L2
.L10:
	call	opt_arg@PLT
	leaq	-1148(%rbp), %rsi
	movq	%rax, %rdi
	call	opt_int@PLT
	testl	%eax, %eax
	jne	.L2
	jmp	.L26
.L11:
	call	opt_arg@PLT
	movq	%rax, %rdi
	call	OBJ_txt2nid@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	%eax, %esi
	movl	$1, %edi
	movl	%eax, -1160(%rbp)
	call	EVP_PBE_find@PLT
	testl	%eax, %eax
	je	.L120
.L109:
	cmpq	$0, -1136(%rbp)
	jne	.L2
	call	EVP_aes_256_cbc@PLT
	movq	%rax, -1136(%rbp)
	jmp	.L2
.L13:
	call	opt_arg@PLT
	leaq	-1136(%rbp), %rsi
	movq	%rax, %rdi
	call	opt_cipher@PLT
	testl	%eax, %eax
	jne	.L2
	jmp	.L26
.L14:
	call	opt_arg@PLT
	leaq	-1096(%rbp), %rsi
	movq	%rax, %rdi
	call	opt_long@PLT
	testl	%eax, %eax
	je	.L26
	cmpq	$0, -1096(%rbp)
	jg	.L2
	jmp	.L26
.L15:
	call	opt_arg@PLT
	leaq	-1104(%rbp), %rsi
	movq	%rax, %rdi
	call	opt_long@PLT
	testl	%eax, %eax
	je	.L26
	cmpq	$0, -1104(%rbp)
	jg	.L2
	jmp	.L26
.L16:
	call	opt_arg@PLT
	leaq	-1112(%rbp), %rsi
	movq	%rax, %rdi
	call	opt_long@PLT
	testl	%eax, %eax
	je	.L26
	cmpq	$0, -1112(%rbp)
	jg	.L2
	jmp	.L26
.L17:
	movq	$16384, -1112(%rbp)
	movq	$8, -1104(%rbp)
	movq	$1, -1096(%rbp)
	jmp	.L109
.L18:
	movl	$1, -1148(%rbp)
	jmp	.L2
.L20:
	call	opt_arg@PLT
	movq	%rax, -1192(%rbp)
	jmp	.L2
.L21:
	call	opt_arg@PLT
	movq	%rax, -1184(%rbp)
	jmp	.L2
.L22:
	call	opt_arg@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	setup_engine@PLT
	movq	%rax, %r13
	jmp	.L2
.L23:
	call	opt_arg@PLT
	leaq	-1140(%rbp), %rdx
	movl	$2, %esi
	movq	%rax, %rdi
	call	opt_format@PLT
	testl	%eax, %eax
	jne	.L2
	jmp	.L26
.L24:
	call	opt_arg@PLT
	leaq	-1144(%rbp), %rdx
	movl	$2, %esi
	movq	%rax, %rdi
	call	opt_format@PLT
	testl	%eax, %eax
	jne	.L2
	jmp	.L26
.L25:
	leaq	pkcs8_options(%rip), %rdi
	xorl	%r12d, %r12d
	call	opt_help@PLT
	jmp	.L107
.L113:
	cmpq	$0, -1136(%rbp)
	jne	.L33
	call	EVP_aes_256_cbc@PLT
	movq	%rax, -1136(%rbp)
	jmp	.L33
.L112:
	movq	bio_err(%rip), %rdi
	leaq	.LC3(%rip), %rsi
	call	BIO_printf@PLT
	jmp	.L105
.L48:
	cmpl	$32773, %esi
	je	.L121
	cmpl	$4, %esi
	jne	.L54
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	d2i_PKCS8_bio@PLT
	movq	%rax, %rbx
.L53:
	testq	%rbx, %rbx
	je	.L122
	movq	-1128(%rbp), %r11
	testq	%r11, %r11
	je	.L123
.L56:
	movq	%r11, %rdi
	movq	%r11, -1160(%rbp)
	call	strlen@PLT
	movq	-1160(%rbp), %r11
	movq	%rbx, %rdi
	movq	%rax, %rdx
	movq	%r11, %rsi
	call	PKCS8_decrypt@PLT
	movq	%rax, %r10
	jmp	.L50
.L114:
	movq	-1128(%rbp), %rcx
	movq	-1184(%rbp), %rdi
	movq	%r13, %r8
	movl	$1, %edx
	leaq	.LC4(%rip), %r9
	call	load_key@PLT
	movq	%rax, %r11
	testq	%rax, %rax
	je	.L65
	movq	%rax, %rdi
	movq	%rax, -1168(%rbp)
	call	EVP_PKEY2PKCS8@PLT
	movq	-1168(%rbp), %r11
	testq	%rax, %rax
	movq	%rax, %r10
	je	.L124
	testl	%ebx, %ebx
	je	.L37
	movl	-1140(%rbp), %eax
	cmpl	$32773, %eax
	je	.L125
	cmpl	$4, %eax
	je	.L126
	movq	bio_err(%rip), %rdi
	leaq	.LC6(%rip), %rsi
	xorl	%eax, %eax
	movq	%r10, -1168(%rbp)
	movq	%r11, -1160(%rbp)
	movl	%ebx, %r12d
	xorl	%ebx, %ebx
	call	BIO_printf@PLT
	movq	-1160(%rbp), %r11
	movq	-1168(%rbp), %r10
	jmp	.L28
.L64:
	xorl	%ebx, %ebx
	xorl	%r10d, %r10d
	xorl	%r11d, %r11d
	movl	$1, %r12d
	jmp	.L28
.L120:
	call	opt_arg@PLT
	movq	bio_err(%rip), %rdi
	movq	%r15, %rdx
	leaq	.LC2(%rip), %rsi
	movq	%rax, %rcx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L26
.L51:
	movq	bio_err(%rip), %rdi
	leaq	.LC6(%rip), %rsi
	xorl	%eax, %eax
	movl	%ebx, %r12d
	xorl	%ebx, %ebx
	call	BIO_printf@PLT
	xorl	%r10d, %r10d
	xorl	%r11d, %r11d
	jmp	.L28
.L54:
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
	movl	$1, %r12d
	xorl	%ebx, %ebx
	leaq	.LC6(%rip), %rsi
	call	BIO_printf@PLT
	xorl	%r10d, %r10d
	xorl	%r11d, %r11d
	jmp	.L28
.L37:
	movq	-1136(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L40
	movq	-1112(%rbp), %r8
	testq	%r8, %r8
	je	.L41
	movq	-1104(%rbp), %r9
	testq	%r9, %r9
	je	.L41
	movq	-1096(%rbp), %rax
	testq	%rax, %rax
	jne	.L127
.L41:
	movl	-1160(%rbp), %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	-1148(%rbp), %esi
	movq	%r10, -1176(%rbp)
	movq	%r11, -1168(%rbp)
	call	PKCS5_pbe2_set_iv@PLT
	movq	-1168(%rbp), %r11
	movq	-1176(%rbp), %r10
	movq	%rax, %r8
.L42:
	testq	%r8, %r8
	je	.L128
	movq	-1120(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L129
.L44:
	movq	%rbx, %rdi
	movq	%r11, -1176(%rbp)
	movq	%r8, -1168(%rbp)
	movq	%r10, -1160(%rbp)
	call	strlen@PLT
	movq	-1168(%rbp), %r8
	movq	-1160(%rbp), %r10
	movq	%rbx, %rdi
	movq	%rax, %rsi
	movq	%r8, %rcx
	movq	%r10, %rdx
	call	PKCS8_set0_pbe@PLT
	movq	-1160(%rbp), %r10
	movq	-1168(%rbp), %r8
	testq	%rax, %rax
	movq	-1176(%rbp), %r11
	movq	%rax, %rbx
	je	.L130
	movl	-1140(%rbp), %eax
	movq	%r10, -1168(%rbp)
	movq	%r11, -1160(%rbp)
	cmpl	$32773, %eax
	je	.L131
	cmpl	$4, %eax
	je	.L132
	movq	bio_err(%rip), %rdi
	leaq	.LC6(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	-1160(%rbp), %r11
	movq	-1168(%rbp), %r10
	jmp	.L28
.L65:
	xorl	%ebx, %ebx
	xorl	%r10d, %r10d
	jmp	.L28
.L61:
	movq	bio_err(%rip), %rdi
	leaq	.LC6(%rip), %rsi
	xorl	%eax, %eax
	movq	%r11, -1160(%rbp)
	movl	$1, %r12d
	call	BIO_printf@PLT
	movq	-1160(%rbp), %r11
	movq	-1168(%rbp), %r10
	jmp	.L28
.L118:
	movl	-1196(%rbp), %edi
	movq	%r10, -1168(%rbp)
	movq	-1120(%rbp), %rax
	testl	%edi, %edi
	je	.L60
	subq	$8, %rsp
	movq	%r11, %rsi
	xorl	%ecx, %ecx
	xorl	%r9d, %r9d
	pushq	%rax
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movq	%r15, %rdi
	movq	%r11, -1160(%rbp)
	call	PEM_write_bio_PrivateKey_traditional@PLT
	popq	%rcx
	popq	%rsi
	movq	-1160(%rbp), %r11
	movq	-1168(%rbp), %r10
	jmp	.L28
.L115:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	PEM_read_bio_PKCS8_PRIV_KEY_INFO@PLT
	xorl	%ebx, %ebx
	movq	%rax, %r10
	jmp	.L50
.L116:
	movq	bio_err(%rip), %rdi
	leaq	.LC13(%rip), %rsi
	xorl	%eax, %eax
	movq	%r10, -1160(%rbp)
	movl	$1, %r12d
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	movq	-1160(%rbp), %r10
	xorl	%r11d, %r11d
	jmp	.L28
.L121:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	PEM_read_bio_PKCS8@PLT
	movq	%rax, %rbx
	jmp	.L53
.L123:
	leaq	-1088(%rbp), %rdi
	xorl	%ecx, %ecx
	leaq	.LC11(%rip), %rdx
	movl	$1024, %esi
	movq	%r11, -1168(%rbp)
	movq	%rdi, -1160(%rbp)
	call	EVP_read_pw_string@PLT
	movq	-1160(%rbp), %rdi
	movq	-1168(%rbp), %r11
	testl	%eax, %eax
	jne	.L133
	movq	%rdi, %r11
	jmp	.L56
.L60:
	subq	$8, %rsp
	xorl	%edx, %edx
	movq	%r11, %rsi
	xorl	%r9d, %r9d
	pushq	%rax
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r15, %rdi
	movq	%r11, -1160(%rbp)
	xorl	%r12d, %r12d
	call	PEM_write_bio_PrivateKey@PLT
	popq	%rax
	popq	%rdx
	movq	-1160(%rbp), %r11
	movq	-1168(%rbp), %r10
	jmp	.L28
.L117:
	movq	bio_err(%rip), %rdi
	movq	%rax, -1160(%rbp)
	xorl	%eax, %eax
	leaq	.LC5(%rip), %rsi
	movq	%r10, -1168(%rbp)
	movl	$1, %r12d
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	movq	-1160(%rbp), %r11
	movq	-1168(%rbp), %r10
	jmp	.L28
.L122:
	movq	bio_err(%rip), %rdi
	leaq	.LC10(%rip), %rsi
	xorl	%eax, %eax
	movl	$1, %r12d
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	xorl	%r10d, %r10d
	xorl	%r11d, %r11d
	jmp	.L28
.L126:
	movq	%r10, %rsi
	movq	%r15, %rdi
	movq	%r11, -1168(%rbp)
	xorl	%r12d, %r12d
	movq	%r10, -1160(%rbp)
	xorl	%ebx, %ebx
	call	i2d_PKCS8_PRIV_KEY_INFO_bio@PLT
	movq	-1160(%rbp), %r10
	movq	-1168(%rbp), %r11
	jmp	.L28
.L40:
	movl	-1148(%rbp), %esi
	movl	-1160(%rbp), %edi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r11, -1168(%rbp)
	movq	%rax, -1176(%rbp)
	call	PKCS5_pbe_set@PLT
	movq	-1176(%rbp), %r10
	movq	-1168(%rbp), %r11
	movq	%rax, %r8
	jmp	.L42
.L125:
	movq	%r10, %rsi
	movq	%r15, %rdi
	movq	%r11, -1168(%rbp)
	xorl	%r12d, %r12d
	movq	%r10, -1160(%rbp)
	xorl	%ebx, %ebx
	call	PEM_write_bio_PKCS8_PRIV_KEY_INFO@PLT
	movq	-1160(%rbp), %r10
	movq	-1168(%rbp), %r11
	jmp	.L28
.L124:
	movq	%rax, -1168(%rbp)
	leaq	.LC5(%rip), %rsi
	movq	%r11, -1160(%rbp)
.L106:
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
	xorl	%ebx, %ebx
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	movq	-1160(%rbp), %r11
	movq	-1168(%rbp), %r10
	jmp	.L28
.L132:
	movq	%rbx, %rsi
	movq	%r15, %rdi
	xorl	%r12d, %r12d
	call	i2d_PKCS8_bio@PLT
	movq	-1160(%rbp), %r11
	movq	-1168(%rbp), %r10
	jmp	.L28
.L129:
	leaq	-1088(%rbp), %rdi
	movl	$1, %ecx
	leaq	.LC8(%rip), %rdx
	movl	$1024, %esi
	movq	%r8, -1184(%rbp)
	movq	%r10, -1176(%rbp)
	movq	%r11, -1168(%rbp)
	movq	%rdi, -1160(%rbp)
	call	EVP_read_pw_string@PLT
	movq	-1160(%rbp), %rdi
	movq	-1168(%rbp), %r11
	testl	%eax, %eax
	movq	-1176(%rbp), %r10
	movq	-1184(%rbp), %r8
	jne	.L134
	movq	%rdi, %rbx
	jmp	.L44
.L119:
	call	__stack_chk_fail@PLT
.L133:
	movq	bio_err(%rip), %rdi
	leaq	.LC12(%rip), %rsi
	xorl	%eax, %eax
	movq	%r11, -1160(%rbp)
	movl	$1, %r12d
	call	BIO_printf@PLT
	movq	-1160(%rbp), %r11
	xorl	%r10d, %r10d
	jmp	.L28
.L134:
	movq	%r8, %rdi
	movq	%r10, -1168(%rbp)
	movq	%r11, -1160(%rbp)
	call	X509_ALGOR_free@PLT
	movq	-1160(%rbp), %r11
	movq	-1168(%rbp), %r10
	jmp	.L28
.L127:
	movq	%r10, -1168(%rbp)
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	pushq	%r10
	pushq	%rax
	movq	%r11, -1160(%rbp)
	call	PKCS5_pbe2_set_scrypt@PLT
	popq	%r11
	movq	-1168(%rbp), %r10
	movq	-1160(%rbp), %r11
	movq	%rax, %r8
	popq	%rbx
	jmp	.L42
.L128:
	movq	%r10, -1168(%rbp)
	leaq	.LC7(%rip), %rsi
	movq	%r11, -1160(%rbp)
	jmp	.L106
.L131:
	movq	%rbx, %rsi
	movq	%r15, %rdi
	xorl	%r12d, %r12d
	call	PEM_write_bio_PKCS8@PLT
	movq	-1160(%rbp), %r11
	movq	-1168(%rbp), %r10
	jmp	.L28
.L130:
	movq	%r8, %rdi
	movq	%r10, -1168(%rbp)
	movq	%r11, -1160(%rbp)
	call	X509_ALGOR_free@PLT
	movq	bio_err(%rip), %rdi
	leaq	.LC9(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	movq	-1160(%rbp), %r11
	movq	-1168(%rbp), %r10
	jmp	.L28
	.cfi_endproc
.LFE1459:
	.size	pkcs8_main, .-pkcs8_main
	.globl	pkcs8_options
	.section	.rodata.str1.1
.LC15:
	.string	"help"
.LC16:
	.string	"Display this summary"
.LC17:
	.string	"inform"
.LC18:
	.string	"Input format (DER or PEM)"
.LC19:
	.string	"outform"
.LC20:
	.string	"Output format (DER or PEM)"
.LC21:
	.string	"in"
.LC22:
	.string	"Input file"
.LC23:
	.string	"out"
.LC24:
	.string	"Output file"
.LC25:
	.string	"topk8"
.LC26:
	.string	"Output PKCS8 file"
.LC27:
	.string	"noiter"
.LC28:
	.string	"Use 1 as iteration count"
.LC29:
	.string	"nocrypt"
	.section	.rodata.str1.8
	.align 8
.LC30:
	.string	"Use or expect unencrypted private key"
	.section	.rodata.str1.1
.LC31:
	.string	"rand"
	.section	.rodata.str1.8
	.align 8
.LC32:
	.string	"Load the file(s) into the random number generator"
	.section	.rodata.str1.1
.LC33:
	.string	"writerand"
	.section	.rodata.str1.8
	.align 8
.LC34:
	.string	"Write random data to the specified file"
	.section	.rodata.str1.1
.LC35:
	.string	"v2"
.LC36:
	.string	"Use PKCS#5 v2.0 and cipher"
.LC37:
	.string	"v1"
.LC38:
	.string	"Use PKCS#5 v1.5 and cipher"
.LC39:
	.string	"v2prf"
	.section	.rodata.str1.8
	.align 8
.LC40:
	.string	"Set the PRF algorithm to use with PKCS#5 v2.0"
	.section	.rodata.str1.1
.LC41:
	.string	"iter"
.LC42:
	.string	"Specify the iteration count"
.LC43:
	.string	"passin"
.LC44:
	.string	"Input file pass phrase source"
.LC45:
	.string	"passout"
	.section	.rodata.str1.8
	.align 8
.LC46:
	.string	"Output file pass phrase source"
	.section	.rodata.str1.1
.LC47:
	.string	"traditional"
	.section	.rodata.str1.8
	.align 8
.LC48:
	.string	"use traditional format private key"
	.section	.rodata.str1.1
.LC49:
	.string	"engine"
	.section	.rodata.str1.8
	.align 8
.LC50:
	.string	"Use engine, possibly a hardware device"
	.section	.rodata.str1.1
.LC51:
	.string	"scrypt"
.LC52:
	.string	"Use scrypt algorithm"
.LC53:
	.string	"scrypt_N"
.LC54:
	.string	"Set scrypt N parameter"
.LC55:
	.string	"scrypt_r"
.LC56:
	.string	"Set scrypt r parameter"
.LC57:
	.string	"scrypt_p"
.LC58:
	.string	"Set scrypt p parameter"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	pkcs8_options, @object
	.size	pkcs8_options, 552
pkcs8_options:
	.quad	.LC15
	.long	1
	.long	45
	.quad	.LC16
	.quad	.LC17
	.long	2
	.long	70
	.quad	.LC18
	.quad	.LC19
	.long	3
	.long	70
	.quad	.LC20
	.quad	.LC21
	.long	5
	.long	60
	.quad	.LC22
	.quad	.LC23
	.long	6
	.long	62
	.quad	.LC24
	.quad	.LC25
	.long	7
	.long	45
	.quad	.LC26
	.quad	.LC27
	.long	8
	.long	45
	.quad	.LC28
	.quad	.LC29
	.long	9
	.long	45
	.quad	.LC30
	.quad	.LC31
	.long	1501
	.long	115
	.quad	.LC32
	.quad	.LC33
	.long	1502
	.long	62
	.quad	.LC34
	.quad	.LC35
	.long	14
	.long	115
	.quad	.LC36
	.quad	.LC37
	.long	15
	.long	115
	.quad	.LC38
	.quad	.LC39
	.long	16
	.long	115
	.quad	.LC40
	.quad	.LC41
	.long	17
	.long	112
	.quad	.LC42
	.quad	.LC43
	.long	18
	.long	115
	.quad	.LC44
	.quad	.LC45
	.long	19
	.long	115
	.quad	.LC46
	.quad	.LC47
	.long	20
	.long	45
	.quad	.LC48
	.quad	.LC49
	.long	4
	.long	115
	.quad	.LC50
	.quad	.LC51
	.long	10
	.long	45
	.quad	.LC52
	.quad	.LC53
	.long	11
	.long	115
	.quad	.LC54
	.quad	.LC55
	.long	12
	.long	115
	.quad	.LC56
	.quad	.LC57
	.long	13
	.long	115
	.quad	.LC58
	.quad	0
	.zero	16
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
