	.file	"asn1pars.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"%s: Memory allocation failure\n"
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"%s: Use -help for summary.\n"
.LC2:
	.string	"Unknown item name %s\n"
.LC3:
	.string	"Supported types:\n"
.LC4:
	.string	"    %s\n"
.LC5:
	.string	"Error reading PEM file\n"
.LC6:
	.string	"asn1"
.LC7:
	.string	"default"
.LC8:
	.string	"Can't find 'asn1' in '%s'\n"
.LC9:
	.string	"'%s' is out of range\n"
.LC10:
	.string	"Error parsing structure\n"
.LC11:
	.string	"Can't parse %s type\n"
.LC12:
	.string	"Error: offset out of range\n"
.LC13:
	.string	"Error writing output\n"
.LC14:
	.string	"Error parsing item %s\n"
	.section	.rodata.str1.8
	.align 8
.LC15:
	.string	"../deps/openssl/openssl/apps/asn1pars.c"
	.text
	.p2align 4
	.globl	asn1parse_main
	.type	asn1parse_main, @function
asn1parse_main:
.LFB1459:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	asn1parse_options(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -104(%rbp)
	movq	$0, -96(%rbp)
	movq	$0, -88(%rbp)
	movl	$32773, -108(%rbp)
	call	opt_init@PLT
	movq	%rax, %r14
	call	OPENSSL_sk_new_null@PLT
	movq	%r14, %rdx
	leaq	.LC0(%rip), %rsi
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L104
	movq	$0, -176(%rbp)
	xorl	%ebx, %ebx
	xorl	%r12d, %r12d
	leaq	.L6(%rip), %r13
	movl	$0, -180(%rbp)
	movl	$0, -144(%rbp)
	movl	$0, -160(%rbp)
	movl	$0, -140(%rbp)
	movq	$0, -120(%rbp)
	movq	$0, -128(%rbp)
	movq	$0, -152(%rbp)
	movq	$0, -168(%rbp)
	movq	$0, -136(%rbp)
.L2:
	call	opt_next@PLT
	testl	%eax, %eax
	je	.L111
.L26:
	addl	$1, %eax
	cmpl	$17, %eax
	ja	.L2
	movslq	0(%r13,%rax,4), %rax
	addq	%r13, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L6:
	.long	.L22-.L6
	.long	.L2-.L6
	.long	.L21-.L6
	.long	.L20-.L6
	.long	.L19-.L6
	.long	.L18-.L6
	.long	.L17-.L6
	.long	.L60-.L6
	.long	.L15-.L6
	.long	.L14-.L6
	.long	.L13-.L6
	.long	.L12-.L6
	.long	.L11-.L6
	.long	.L10-.L6
	.long	.L9-.L6
	.long	.L8-.L6
	.long	.L7-.L6
	.long	.L5-.L6
	.text
.L60:
	movl	$1, %ebx
	call	opt_next@PLT
	testl	%eax, %eax
	jne	.L26
.L111:
	call	opt_num_rest@PLT
	movl	%eax, -184(%rbp)
	testl	%eax, %eax
	jne	.L22
	movq	-128(%rbp), %rax
	testq	%rax, %rax
	je	.L28
	movl	$32769, %edx
	movl	$114, %esi
	movq	%rax, %rdi
	call	bio_open_default@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L107
	movq	%rax, %rdi
	call	OBJ_create_objects@PLT
	movq	%r13, %rdi
	call	BIO_free@PLT
.L28:
	movl	-108(%rbp), %edx
	movq	-152(%rbp), %rdi
	movl	$114, %esi
	call	bio_open_default@PLT
	movq	%rax, -128(%rbp)
	testq	%rax, %rax
	je	.L107
	movq	-120(%rbp), %rax
	testq	%rax, %rax
	je	.L29
	movl	$4, %edx
	movl	$119, %esi
	movq	%rax, %rdi
	call	bio_open_default@PLT
	movq	%rax, -120(%rbp)
	testq	%rax, %rax
	jne	.L29
	movq	$0, -136(%rbp)
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	jmp	.L3
.L17:
	movl	$1, %r12d
	jmp	.L2
.L20:
	call	opt_arg@PLT
	leaq	-108(%rbp), %rdx
	movl	$2, %esi
	movq	%rax, %rdi
	call	opt_format@PLT
	testl	%eax, %eax
	jne	.L2
.L22:
	movq	%r14, %rdx
	leaq	.LC1(%rip), %rsi
.L104:
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L107:
	movq	$0, -120(%rbp)
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	movq	$0, -136(%rbp)
	movq	$0, -128(%rbp)
.L3:
	movq	-120(%rbp), %rdi
	movl	$1, %r12d
	call	BIO_free@PLT
	movq	-128(%rbp), %rdi
	call	BIO_free@PLT
	movq	-136(%rbp), %rdi
	call	BIO_free@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
.L57:
	movq	%r14, %rdi
	call	BUF_MEM_free@PLT
	movq	-96(%rbp), %rdi
	movl	$304, %edx
	leaq	.LC15(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-88(%rbp), %rdi
	movl	$305, %edx
	leaq	.LC15(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%r13, %rdi
	call	ASN1_TYPE_free@PLT
	movq	%r15, %rdi
	call	OPENSSL_sk_free@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L112
	addq	$152, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L5:
	.cfi_restore_state
	call	opt_arg@PLT
	movq	%rax, %rdi
	call	ASN1_ITEM_lookup@PLT
	movq	%rax, -176(%rbp)
	testq	%rax, %rax
	jne	.L2
	call	opt_arg@PLT
	movq	bio_err(%rip), %rdi
	xorl	%ebx, %ebx
	leaq	.LC2(%rip), %rsi
	movq	%rax, %rdx
	xorl	%eax, %eax
	leaq	.LC4(%rip), %r12
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	leaq	.LC3(%rip), %rsi
	call	BIO_puts@PLT
	jmp	.L25
.L113:
	movq	48(%rax), %rdx
	movq	bio_err(%rip), %rdi
	movq	%r12, %rsi
	xorl	%eax, %eax
	addq	$1, %rbx
	call	BIO_printf@PLT
.L25:
	movq	%rbx, %rdi
	call	ASN1_ITEM_get@PLT
	testq	%rax, %rax
	jne	.L113
	jmp	.L107
.L7:
	movl	$32773, -108(%rbp)
	movl	$1, -160(%rbp)
	jmp	.L2
.L8:
	call	opt_arg@PLT
	movq	%rax, -168(%rbp)
	jmp	.L2
.L9:
	call	opt_arg@PLT
	movq	%rax, -136(%rbp)
	jmp	.L2
.L10:
	call	opt_arg@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	OPENSSL_sk_push@PLT
	jmp	.L2
.L11:
	call	opt_arg@PLT
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	strtol@PLT
	movl	%eax, -140(%rbp)
	jmp	.L2
.L12:
	movl	$-1, -140(%rbp)
	jmp	.L2
.L13:
	call	opt_arg@PLT
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	strtol@PLT
	movl	%eax, -180(%rbp)
	jmp	.L2
.L14:
	call	opt_arg@PLT
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	strtol@PLT
	movl	%eax, -144(%rbp)
	jmp	.L2
.L15:
	call	opt_arg@PLT
	movq	%rax, -128(%rbp)
	jmp	.L2
.L18:
	call	opt_arg@PLT
	movq	%rax, -120(%rbp)
	jmp	.L2
.L19:
	call	opt_arg@PLT
	movq	%rax, -152(%rbp)
	jmp	.L2
.L21:
	leaq	asn1parse_options(%rip), %rdi
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	call	opt_help@PLT
	movq	$0, -120(%rbp)
	movq	$0, -136(%rbp)
	movq	$0, -128(%rbp)
.L23:
	movq	-120(%rbp), %rdi
	xorl	%r12d, %r12d
	call	BIO_free@PLT
	movq	-128(%rbp), %rdi
	call	BIO_free@PLT
	movq	-136(%rbp), %rdi
	call	BIO_free@PLT
	jmp	.L57
.L29:
	call	BUF_MEM_new@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L106
	movl	-160(%rbp), %eax
	testl	%eax, %eax
	je	.L30
	movq	-128(%rbp), %rdi
	leaq	-104(%rbp), %rcx
	leaq	-88(%rbp), %rdx
	leaq	-96(%rbp), %rsi
	leaq	-72(%rbp), %r8
	call	PEM_read_bio@PLT
	cmpl	$1, %eax
	jne	.L114
	movq	$0, -136(%rbp)
	movq	-104(%rbp), %rax
	movq	%rax, 8(%r14)
	movq	-72(%rbp), %rax
	movq	%rax, 16(%r14)
	movq	%rax, (%r14)
.L32:
	movq	%r15, %rdi
	call	OPENSSL_sk_num@PLT
	testl	%eax, %eax
	je	.L70
	movq	-104(%rbp), %rax
	leaq	-80(%rbp), %rcx
	movl	%ebx, -188(%rbp)
	xorl	%r13d, %r13d
	movl	%r12d, -192(%rbp)
	movl	-184(%rbp), %ebx
	movq	%rax, -152(%rbp)
	movq	-72(%rbp), %rax
	movq	%rcx, -160(%rbp)
	movq	%r14, -168(%rbp)
	movq	%rax, %r12
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L43:
	movq	-152(%rbp), %r14
	movq	-160(%rbp), %rsi
	movq	%r12, %rdx
	xorl	%edi, %edi
	subq	%rax, %rdx
	addq	%rax, %r14
	movq	%r14, -80(%rbp)
	call	d2i_ASN1_TYPE@PLT
	movq	%r13, %rdi
	movq	%rax, %r14
	call	ASN1_TYPE_free@PLT
	testq	%r14, %r14
	je	.L115
	movq	%r14, %rdi
	call	ASN1_TYPE_get@PLT
	cmpl	$6, %eax
	je	.L46
	cmpl	$1, %eax
	je	.L46
	cmpl	$5, %eax
	je	.L46
	movq	8(%r14), %rax
	movq	%r14, %r13
	movq	8(%rax), %rcx
	movslq	(%rax), %r12
	movq	%rcx, -152(%rbp)
.L44:
	addl	$1, %ebx
.L41:
	movq	%r15, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jge	.L116
	movl	%ebx, %esi
	movq	%r15, %rdi
	call	OPENSSL_sk_value@PLT
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	strtol@PLT
	testl	%eax, %eax
	jle	.L42
	cltq
	cmpq	%r12, %rax
	jl	.L43
.L42:
	movl	%ebx, %esi
	movq	%r15, %rdi
	call	OPENSSL_sk_value@PLT
	movq	bio_err(%rip), %rdi
	leaq	.LC9(%rip), %rsi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L44
.L122:
	leaq	.LC6(%rip), %rdx
	leaq	.LC7(%rip), %rsi
	movq	%rax, %rdi
	call	NCONF_get_string@PLT
	movq	%rax, -136(%rbp)
	testq	%rax, %rax
	jne	.L34
	movq	-168(%rbp), %rdx
	movq	bio_err(%rip), %rdi
	leaq	.LC8(%rip), %rsi
	call	BIO_printf@PLT
.L35:
	movq	%r13, %rdi
	call	NCONF_free@PLT
	movq	-136(%rbp), %rdi
	call	ASN1_TYPE_free@PLT
.L36:
	movq	bio_err(%rip), %rdi
	movq	$-1, -72(%rbp)
	call	ERR_print_errors@PLT
.L106:
	movq	$0, -136(%rbp)
	xorl	%r13d, %r13d
	jmp	.L3
.L30:
	movl	$65536, %esi
	movq	%r14, %rdi
	call	BUF_MEM_grow@PLT
	testq	%rax, %rax
	je	.L106
	movq	-136(%rbp), %r13
	orq	-168(%rbp), %r13
	jne	.L117
	cmpl	$32773, -108(%rbp)
	movq	$0, -136(%rbp)
	je	.L118
.L38:
	movl	%ebx, -152(%rbp)
	xorl	%eax, %eax
	movq	-128(%rbp), %rbx
	movq	$0, -72(%rbp)
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L119:
	cltq
	addq	-72(%rbp), %rax
	movq	%rax, -72(%rbp)
.L39:
	leaq	8192(%rax), %rsi
	movq	%r14, %rdi
	call	BUF_MEM_grow@PLT
	testq	%rax, %rax
	je	.L3
	movq	-72(%rbp), %rsi
	movl	$8192, %edx
	addq	8(%r14), %rsi
	movq	%rbx, %rdi
	call	BIO_read@PLT
	testl	%eax, %eax
	jg	.L119
	movl	-152(%rbp), %ebx
.L37:
	movq	8(%r14), %rax
	movq	%rax, -104(%rbp)
	jmp	.L32
.L70:
	xorl	%r13d, %r13d
.L40:
	movl	-144(%rbp), %eax
	testl	%eax, %eax
	js	.L49
	movslq	%eax, %rcx
	movq	-72(%rbp), %rax
	cmpq	%rax, %rcx
	jge	.L49
	movl	-180(%rbp), %esi
	subq	%rcx, %rax
	movq	%rax, -72(%rbp)
	movl	%eax, %edx
	testl	%esi, %esi
	je	.L51
	cmpl	%esi, %eax
	cmovbe	%eax, %esi
	movl	%esi, %edx
.L51:
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L52
	movq	%rcx, %rsi
	addq	-104(%rbp), %rsi
	movq	%rcx, -160(%rbp)
	movl	%edx, -152(%rbp)
	call	BIO_write@PLT
	movl	-152(%rbp), %edx
	movq	-160(%rbp), %rcx
	cmpl	%eax, %edx
	jne	.L120
.L52:
	testl	%ebx, %ebx
	jne	.L23
	addq	-104(%rbp), %rcx
	movq	%rcx, -64(%rbp)
	movq	%rcx, %rsi
	movq	-176(%rbp), %rcx
	testq	%rcx, %rcx
	je	.L53
	leaq	-64(%rbp), %rsi
	xorl	%edi, %edi
	call	ASN1_item_d2i@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L121
	movq	-176(%rbp), %rbx
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	bio_out(%rip), %rdi
	movq	%rbx, %rcx
	call	ASN1_item_print@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	ASN1_item_free@PLT
	jmp	.L23
.L49:
	movq	bio_err(%rip), %rdi
	leaq	.LC12(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L3
.L114:
	movq	bio_err(%rip), %rdi
	leaq	.LC5(%rip), %rsi
	xorl	%eax, %eax
	xorl	%r13d, %r13d
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	movq	$0, -136(%rbp)
	jmp	.L3
.L116:
	movq	-152(%rbp), %rax
	movq	%r12, %rcx
	movl	-188(%rbp), %ebx
	movq	%rcx, -72(%rbp)
	movl	-192(%rbp), %r12d
	movq	%rax, -104(%rbp)
	movq	-168(%rbp), %r14
	jmp	.L40
.L117:
	movq	-168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L64
	call	app_load_config@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L65
	cmpq	$0, -136(%rbp)
	je	.L122
.L34:
	movq	-136(%rbp), %rdi
	movq	%r13, %rsi
	call	ASN1_generate_nconf@PLT
	movq	%r13, %rdi
	movq	%rax, -136(%rbp)
	call	NCONF_free@PLT
	movq	-136(%rbp), %rax
	testq	%rax, %rax
	je	.L36
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	i2d_ASN1_TYPE@PLT
	movslq	%eax, %r13
	testl	%r13d, %r13d
	jle	.L67
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	BUF_MEM_grow@PLT
	testq	%rax, %rax
	je	.L67
	movq	8(%r14), %rax
	movq	-136(%rbp), %rdi
	leaq	-64(%rbp), %rsi
	movq	%rax, -64(%rbp)
	call	i2d_ASN1_TYPE@PLT
	movq	-136(%rbp), %rdi
	call	ASN1_TYPE_free@PLT
	movq	%r13, -72(%rbp)
	movq	$0, -136(%rbp)
	jmp	.L37
.L46:
	movl	%eax, %edi
	movq	%r14, %r13
	movq	-168(%rbp), %r14
	call	ASN1_tag2str@PLT
	leaq	.LC11(%rip), %rsi
	movq	%rax, %rdx
.L105:
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L109:
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	jmp	.L3
.L118:
	call	BIO_f_base64@PLT
	movq	%rax, %rdi
	call	BIO_new@PLT
	movq	%rax, -136(%rbp)
	testq	%rax, %rax
	je	.L69
	movq	-128(%rbp), %rsi
	movq	%rax, %rdi
	call	BIO_push@PLT
	movq	-128(%rbp), %rax
	movq	-136(%rbp), %rcx
	movq	%rax, -136(%rbp)
	movq	%rcx, -128(%rbp)
	jmp	.L38
.L120:
	leaq	.LC13(%rip), %rsi
.L108:
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	jmp	.L3
.L115:
	movq	%r14, %r13
	leaq	.LC10(%rip), %rsi
	movq	-168(%rbp), %r14
	jmp	.L108
.L53:
	movl	-140(%rbp), %r8d
	movq	bio_out(%rip), %rdi
	movl	%r12d, %ecx
	call	ASN1_parse_dump@PLT
	testl	%eax, %eax
	jne	.L23
	jmp	.L109
.L64:
	xorl	%r13d, %r13d
	jmp	.L34
.L121:
	movq	-176(%rbp), %rax
	leaq	.LC14(%rip), %rsi
	movq	48(%rax), %rdx
	jmp	.L105
.L112:
	call	__stack_chk_fail@PLT
.L69:
	movq	-136(%rbp), %r13
	jmp	.L3
.L65:
	movq	$0, -136(%rbp)
	jmp	.L35
.L67:
	xorl	%r13d, %r13d
	jmp	.L35
	.cfi_endproc
.LFE1459:
	.size	asn1parse_main, .-asn1parse_main
	.globl	asn1parse_options
	.section	.rodata.str1.1
.LC16:
	.string	"help"
.LC17:
	.string	"Display this summary"
.LC18:
	.string	"inform"
.LC19:
	.string	"input format - one of DER PEM"
.LC20:
	.string	"in"
.LC21:
	.string	"input file"
.LC22:
	.string	"out"
	.section	.rodata.str1.8
	.align 8
.LC23:
	.string	"output file (output format is always DER)"
	.section	.rodata.str1.1
.LC24:
	.string	"i"
.LC25:
	.string	"indents the output"
.LC26:
	.string	"noout"
.LC27:
	.string	"do not produce any output"
.LC28:
	.string	"offset"
.LC29:
	.string	"offset into file"
.LC30:
	.string	"length"
.LC31:
	.string	"length of section in file"
.LC32:
	.string	"oid"
.LC33:
	.string	"file of extra oid definitions"
.LC34:
	.string	"dump"
.LC35:
	.string	"unknown data in hex form"
.LC36:
	.string	"dlimit"
	.section	.rodata.str1.8
	.align 8
.LC37:
	.string	"dump the first arg bytes of unknown data in hex form"
	.section	.rodata.str1.1
.LC38:
	.string	"strparse"
	.section	.rodata.str1.8
	.align 8
.LC39:
	.string	"offset; a series of these can be used to 'dig'"
	.align 8
.LC40:
	.string	"into multiple ASN1 blob wrappings"
	.section	.rodata.str1.1
.LC41:
	.string	"genstr"
	.section	.rodata.str1.8
	.align 8
.LC42:
	.string	"string to generate ASN1 structure from"
	.section	.rodata.str1.1
.LC43:
	.string	"genconf"
	.section	.rodata.str1.8
	.align 8
.LC44:
	.string	"file to generate ASN1 structure from"
	.section	.rodata.str1.1
.LC45:
	.string	"(-inform  will be ignored)"
.LC46:
	.string	"strictpem"
	.section	.rodata.str1.8
	.align 8
.LC47:
	.string	"do not attempt base64 decode outside PEM markers"
	.section	.rodata.str1.1
.LC48:
	.string	"item"
.LC49:
	.string	"item to parse and print"
	.section	.data.rel.ro,"aw"
	.align 32
	.type	asn1parse_options, @object
	.size	asn1parse_options, 456
asn1parse_options:
	.quad	.LC16
	.long	1
	.long	45
	.quad	.LC17
	.quad	.LC18
	.long	2
	.long	70
	.quad	.LC19
	.quad	.LC20
	.long	3
	.long	60
	.quad	.LC21
	.quad	.LC22
	.long	4
	.long	62
	.quad	.LC23
	.quad	.LC24
	.long	5
	.long	0
	.quad	.LC25
	.quad	.LC26
	.long	6
	.long	0
	.quad	.LC27
	.quad	.LC28
	.long	8
	.long	112
	.quad	.LC29
	.quad	.LC30
	.long	9
	.long	112
	.quad	.LC31
	.quad	.LC32
	.long	7
	.long	60
	.quad	.LC33
	.quad	.LC34
	.long	10
	.long	0
	.quad	.LC35
	.quad	.LC36
	.long	11
	.long	112
	.quad	.LC37
	.quad	.LC38
	.long	12
	.long	112
	.quad	.LC39
	.quad	OPT_MORE_STR
	.long	0
	.long	0
	.quad	.LC40
	.quad	.LC41
	.long	13
	.long	115
	.quad	.LC42
	.quad	.LC43
	.long	14
	.long	115
	.quad	.LC44
	.quad	OPT_MORE_STR
	.long	0
	.long	0
	.quad	.LC45
	.quad	.LC46
	.long	15
	.long	0
	.quad	.LC47
	.quad	.LC48
	.long	16
	.long	115
	.quad	.LC49
	.quad	0
	.zero	16
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
