	.file	"enc.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"hex string is too long, ignoring excess\n"
	.align 8
.LC1:
	.string	"hex string is too short, padding with zero bytes to length\n"
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC2:
	.string	"non-hex digit\n"
	.text
	.p2align 4
	.type	set_hex, @function
set_hex:
.LFB1439:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leal	(%rdx,%rdx), %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	strlen@PLT
	cmpl	%eax, %r15d
	jl	.L14
	movl	%eax, %r14d
	jg	.L15
.L3:
	xorl	%esi, %esi
	movslq	%r13d, %rdx
	movq	%r12, %rdi
	call	memset@PLT
	testl	%r14d, %r14d
	jle	.L10
	call	__ctype_b_loc@PLT
	leal	-1(%r14), %r13d
	xorl	%r14d, %r14d
	movq	%rax, %r15
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L17:
	orb	%al, (%rdx)
	leaq	1(%r14), %rax
	cmpq	%r14, %r13
	je	.L10
.L11:
	movq	%rax, %r14
.L9:
	movzbl	(%rbx,%r14), %edx
	movq	(%r15), %rax
	movq	%rdx, %rdi
	testb	$16, 1(%rax,%rdx,2)
	je	.L16
	call	OPENSSL_hexchar2int@PLT
	movl	%r14d, %edx
	sarl	%edx
	movslq	%edx, %rdx
	addq	%r12, %rdx
	testb	$1, %r14b
	jne	.L17
	sall	$4, %eax
	movb	%al, (%rdx)
	leaq	1(%r14), %rax
	cmpq	%r14, %r13
	jne	.L11
.L10:
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	.cfi_restore_state
	movq	bio_err(%rip), %rdi
	leaq	.LC1(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L16:
	movq	bio_err(%rip), %rdi
	leaq	.LC2(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	.cfi_restore_state
	movq	bio_err(%rip), %rdi
	leaq	.LC0(%rip), %rsi
	xorl	%eax, %eax
	movl	%r15d, %r14d
	call	BIO_printf@PLT
	jmp	.L3
	.cfi_endproc
.LFE1439:
	.size	set_hex, .-set_hex
	.section	.rodata.str1.1
.LC3:
	.string	"-%-25s"
.LC4:
	.string	"\n"
.LC5:
	.string	" "
	.text
	.p2align 4
	.type	show_ciphers, @function
show_ciphers:
.LFB1438:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	__ctype_b_loc@PLT
	movq	8(%rbx), %rdi
	movq	(%rax), %rax
	movzbl	(%rdi), %edx
	testb	$2, 1(%rax,%rdx,2)
	jne	.L30
.L18:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	.cfi_restore_state
	call	EVP_get_cipherbyname@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L18
	movq	%rax, %rdi
	call	EVP_CIPHER_flags@PLT
	testl	$2097152, %eax
	jne	.L18
	movq	%r13, %rdi
	call	EVP_CIPHER_flags@PLT
	andl	$983047, %eax
	cmpq	$65537, %rax
	je	.L18
	movq	8(%rbx), %rdx
	movq	(%r12), %rdi
	leaq	.LC3(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movl	8(%r12), %eax
	addl	$1, %eax
	movl	%eax, 8(%r12)
	cmpl	$3, %eax
	je	.L31
	movq	(%r12), %rdi
	addq	$8, %rsp
	leaq	.LC5(%rip), %rsi
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	BIO_printf@PLT
	.p2align 4,,10
	.p2align 3
.L31:
	.cfi_restore_state
	movq	(%r12), %rdi
	leaq	.LC4(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movl	$0, 8(%r12)
	jmp	.L18
	.cfi_endproc
.LFE1438:
	.size	show_ciphers, .-show_ciphers
	.section	.rodata.str1.1
.LC6:
	.string	"base64"
.LC7:
	.string	"enc"
.LC8:
	.string	"%s is not a known cipher\n"
.LC9:
	.string	"%s: Use -help for summary.\n"
.LC10:
	.string	"Supported ciphers:\n"
.LC11:
	.string	"%s Can't read key from %s\n"
.LC12:
	.string	"Extra arguments given.\n"
	.section	.rodata.str1.8
	.align 8
.LC13:
	.string	"%s: AEAD ciphers not supported\n"
	.section	.rodata.str1.1
.LC14:
	.string	"%s XTS ciphers not supported\n"
.LC15:
	.string	"bufsize=%d\n"
.LC16:
	.string	"strbuf"
.LC17:
	.string	"evp buffer"
.LC18:
	.string	"Error getting password\n"
.LC19:
	.string	"encryption"
.LC20:
	.string	"decryption"
.LC21:
	.string	"enter %s %s password:"
.LC22:
	.string	"bad password read\n"
.LC23:
	.string	"invalid hex salt value\n"
.LC24:
	.string	"error writing output file\n"
.LC25:
	.string	"error reading input file\n"
.LC26:
	.string	"bad magic number\n"
.LC27:
	.string	"PKCS5_PBKDF2_HMAC failed\n"
	.section	.rodata.str1.8
	.align 8
.LC28:
	.string	"*** WARNING : deprecated key derivation used.\nUsing -iter or -pbkdf2 would be better.\n"
	.section	.rodata.str1.1
.LC29:
	.string	"EVP_BytesToKey failed\n"
	.section	.rodata.str1.8
	.align 8
.LC30:
	.string	"warning: iv not used by this cipher\n"
	.section	.rodata.str1.1
.LC31:
	.string	"invalid hex iv value\n"
.LC32:
	.string	"iv undefined\n"
.LC33:
	.string	"invalid hex key value\n"
.LC34:
	.string	"Error setting cipher %s\n"
.LC35:
	.string	"salt="
.LC36:
	.string	"%02X"
.LC37:
	.string	"key="
.LC38:
	.string	"iv ="
.LC39:
	.string	"bad decrypt\n"
.LC40:
	.string	"bytes read   : %8ju\n"
.LC41:
	.string	"bytes written: %8ju\n"
	.section	.rodata.str1.8
	.align 8
.LC42:
	.string	"../deps/openssl/openssl/apps/enc.c"
	.section	.rodata.str1.1
.LC43:
	.string	"%s: zero length password\n"
	.text
	.p2align 4
	.globl	enc_main
	.type	enc_main, @function
enc_main:
.LFB1437:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edi, %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$504, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -408(%rbp)
	movq	$0, -392(%rbp)
	movq	$0, -384(%rbp)
	movl	$0, -412(%rbp)
	call	opt_progname@PLT
	movl	$7, %ecx
	leaq	.LC6(%rip), %rdi
	movq	%rax, %rsi
	movq	%rax, %r14
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L152
	movq	%r14, %rdi
	call	EVP_get_cipherbyname@PLT
	movl	$0, -456(%rbp)
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L263
.L33:
	movq	%r13, %rsi
	movl	%r15d, %edi
	xorl	%r15d, %r15d
	movl	$1, %ebx
	leaq	enc_options(%rip), %rdx
	leaq	.L40(%rip), %r13
	call	opt_init@PLT
	movl	$0, -488(%rbp)
	movl	$0, -504(%rbp)
	movq	%rax, %r14
	movl	$0, -476(%rbp)
	movl	$0, -472(%rbp)
	movl	$0, -528(%rbp)
	movl	$0, -500(%rbp)
	movl	$8192, -480(%rbp)
	movq	$0, -440(%rbp)
	movq	$0, -464(%rbp)
	movq	$0, -512(%rbp)
	movq	$0, -432(%rbp)
	movq	$0, -536(%rbp)
	movq	$0, -520(%rbp)
	movq	$0, -496(%rbp)
	movq	$0, -424(%rbp)
.L35:
	call	opt_next@PLT
	testl	%eax, %eax
	je	.L264
.L77:
	cmpl	$29, %eax
	jg	.L36
	cmpl	$-1, %eax
	jl	.L35
	addl	$1, %eax
	cmpl	$30, %eax
	ja	.L35
	movslq	0(%r13,%rax,4), %rax
	addq	%r13, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L40:
	.long	.L67-.L40
	.long	.L35-.L40
	.long	.L66-.L40
	.long	.L65-.L40
	.long	.L64-.L40
	.long	.L63-.L40
	.long	.L62-.L40
	.long	.L61-.L40
	.long	.L60-.L40
	.long	.L154-.L40
	.long	.L59-.L40
	.long	.L58-.L40
	.long	.L57-.L40
	.long	.L56-.L40
	.long	.L55-.L40
	.long	.L54-.L40
	.long	.L53-.L40
	.long	.L52-.L40
	.long	.L51-.L40
	.long	.L35-.L40
	.long	.L50-.L40
	.long	.L49-.L40
	.long	.L48-.L40
	.long	.L47-.L40
	.long	.L46-.L40
	.long	.L45-.L40
	.long	.L44-.L40
	.long	.L43-.L40
	.long	.L42-.L40
	.long	.L41-.L40
	.long	.L39-.L40
	.text
.L154:
	xorl	%ebx, %ebx
	call	opt_next@PLT
	testl	%eax, %eax
	jne	.L77
.L264:
	call	opt_num_rest@PLT
	testl	%eax, %eax
	jne	.L265
	testq	%r12, %r12
	je	.L79
	movq	%r12, %rdi
	call	EVP_CIPHER_flags@PLT
	testl	$2097152, %eax
	jne	.L266
	movq	%r12, %rdi
	call	EVP_CIPHER_flags@PLT
	andl	$983047, %eax
	cmpq	$65537, %rax
	je	.L267
.L79:
	cmpq	$0, -392(%rbp)
	je	.L268
.L81:
	movl	-412(%rbp), %edx
	testl	%edx, %edx
	jne	.L82
	movl	$1, -412(%rbp)
.L82:
	cmpl	$79, -480(%rbp)
	jg	.L170
	testb	$1, -456(%rbp)
	je	.L170
	movl	$80, -480(%rbp)
	movl	$192, %r13d
.L83:
	testl	%r15d, %r15d
	jne	.L269
.L85:
	movl	-456(%rbp), %eax
	testl	%eax, %eax
	je	.L160
	cmpl	$1, %ebx
	sbbl	%eax, %eax
	andb	$127, %ah
	addl	$32771, %eax
	cmpl	$1, %ebx
	sbbl	%r14d, %r14d
	movl	%eax, -540(%rbp)
	andl	$32769, %r14d
	addl	$2, %r14d
.L86:
	leaq	.LC16(%rip), %rsi
	movl	$512, %edi
	call	app_malloc@PLT
	movl	%r13d, %edi
	leaq	.LC17(%rip), %rsi
	movq	%rax, -448(%rbp)
	call	app_malloc@PLT
	cmpq	$0, -432(%rbp)
	movq	%rax, %r13
	je	.L270
	movq	-432(%rbp), %rdi
	movl	%r14d, %edx
	movl	$114, %esi
	call	bio_open_default@PLT
	movq	%rax, -432(%rbp)
.L88:
	cmpq	$0, -432(%rbp)
	je	.L162
	movq	-440(%rbp), %rdi
	cmpq	$0, -464(%rbp)
	sete	%al
	testq	%rdi, %rdi
	je	.L89
	testb	%al, %al
	jne	.L271
.L89:
	testq	%r12, %r12
	je	.L91
	testb	%al, %al
	je	.L91
	cmpq	$0, -496(%rbp)
	movq	$0, -464(%rbp)
	je	.L272
.L91:
	movl	-540(%rbp), %edx
	movq	-512(%rbp), %rdi
	movl	$119, %esi
	call	bio_open_default@PLT
	movq	%rax, -440(%rbp)
	testq	%rax, %rax
	je	.L164
	movl	-500(%rbp), %eax
	testl	%eax, %eax
	jne	.L273
.L96:
	movl	-456(%rbp), %eax
	testl	%eax, %eax
	jne	.L274
	movq	-440(%rbp), %rax
	xorl	%r14d, %r14d
	movq	%rax, -456(%rbp)
	movq	-432(%rbp), %rax
	movq	%rax, -512(%rbp)
.L97:
	testq	%r12, %r12
	je	.L252
	movq	-464(%rbp), %rax
	testq	%rax, %rax
	je	.L102
	movq	%rax, %rdi
	call	strlen@PLT
	movl	-472(%rbp), %r11d
	movq	%rax, -528(%rbp)
	testl	%r11d, %r11d
	jne	.L103
	testl	%ebx, %ebx
	je	.L104
	movq	-536(%rbp), %rdi
	leaq	-344(%rbp), %rax
	movq	%rax, -536(%rbp)
	testq	%rdi, %rdi
	je	.L105
	movl	$8, %edx
	movq	%rax, %rsi
	call	set_hex
	testl	%eax, %eax
	je	.L106
.L109:
	cmpl	$2, -476(%rbp)
	je	.L108
	movq	-456(%rbp), %rdi
	movl	$8, %edx
	leaq	magic.24651(%rip), %rsi
	call	BIO_write@PLT
	cmpl	$8, %eax
	jne	.L111
	movq	-536(%rbp), %rsi
	movq	-456(%rbp), %rdi
	movl	$8, %edx
	call	BIO_write@PLT
	cmpl	$8, %eax
	je	.L108
.L111:
	leaq	.LC24(%rip), %rsi
.L258:
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L259:
	movl	%ebx, %r15d
	xorl	%r12d, %r12d
	jmp	.L34
.L64:
	movl	$1, %ebx
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L36:
	leal	-1501(%rax), %edx
	cmpl	$1, %edx
	ja	.L35
	movl	%eax, %edi
	call	opt_rand@PLT
	testl	%eax, %eax
	jne	.L35
	jmp	.L262
	.p2align 4,,10
	.p2align 3
.L152:
	movl	$1, -456(%rbp)
	xorl	%r12d, %r12d
	jmp	.L33
.L265:
	movq	bio_err(%rip), %rdi
	leaq	.LC12(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L67:
	movq	%r14, %rdx
	leaq	.LC9(%rip), %rsi
.L261:
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L262:
	xorl	%r13d, %r13d
	movl	$1, %r15d
.L255:
	movq	$0, -448(%rbp)
	xorl	%r12d, %r12d
	xorl	%r14d, %r14d
	movq	$0, -440(%rbp)
	movq	$0, -432(%rbp)
.L34:
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	movq	-448(%rbp), %rdi
	movl	$611, %edx
	leaq	.LC42(%rip), %rsi
	call	CRYPTO_free@PLT
	movl	$612, %edx
	leaq	.LC42(%rip), %rsi
	movq	%r13, %rdi
	call	CRYPTO_free@PLT
	movq	-432(%rbp), %rdi
	call	BIO_free@PLT
	movq	-440(%rbp), %rdi
	call	BIO_free_all@PLT
	movq	%r12, %rdi
	call	BIO_free@PLT
	movq	%r14, %rdi
	call	BIO_free@PLT
	movq	-424(%rbp), %rdi
	call	release_engine@PLT
	movq	-384(%rbp), %rdi
	movl	$621, %edx
	leaq	.LC42(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L275
	leaq	-40(%rbp), %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L39:
	.cfi_restore_state
	call	opt_unknown@PLT
	leaq	-400(%rbp), %rsi
	movq	%rax, %rdi
	call	opt_cipher@PLT
	testl	%eax, %eax
	je	.L67
	movq	-400(%rbp), %r12
	jmp	.L35
.L41:
	movl	$1, -488(%rbp)
	movl	-412(%rbp), %ecx
	testl	%ecx, %ecx
	jne	.L35
	movl	$10000, -412(%rbp)
	jmp	.L35
.L42:
	call	opt_arg@PLT
	leaq	-412(%rbp), %rsi
	movq	%rax, %rdi
	call	opt_int@PLT
	testl	%eax, %eax
	je	.L67
	movl	$1, -488(%rbp)
	jmp	.L35
.L43:
	call	opt_arg@PLT
	leaq	-392(%rbp), %rsi
	movq	%rax, %rdi
	call	opt_md@PLT
	testl	%eax, %eax
	jne	.L35
	jmp	.L67
.L44:
	call	opt_arg@PLT
	movq	%rax, -520(%rbp)
	jmp	.L35
.L45:
	call	opt_arg@PLT
	movq	%rax, -536(%rbp)
	jmp	.L35
.L46:
	xorl	%r12d, %r12d
	jmp	.L35
.L47:
	call	opt_arg@PLT
	movq	%rax, -496(%rbp)
	jmp	.L35
.L48:
	call	opt_arg@PLT
	movl	$32769, %edx
	movl	$114, %esi
	movq	%rax, %rdi
	call	bio_open_default@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L67
	movl	$128, %edx
	leaq	buf.24650(%rip), %rsi
	movq	%rax, -464(%rbp)
	call	BIO_gets@PLT
	movq	-464(%rbp), %rdi
	movl	%eax, -448(%rbp)
	call	BIO_free@PLT
	movl	-448(%rbp), %edx
	testl	%edx, %edx
	jle	.L73
	cmpl	$1, %edx
	je	.L74
	movslq	%edx, %rcx
	leaq	buf.24650(%rip), %rax
	subl	$2, %edx
	leaq	-1+buf.24650(%rip), %rsi
	addq	%rcx, %rax
	addq	%rsi, %rcx
	subq	%rdx, %rcx
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L76:
	movb	$0, -1(%rax)
	subq	$1, %rax
	cmpq	%rcx, %rax
	je	.L74
.L75:
	movzbl	-1(%rax), %edx
	cmpb	$13, %dl
	je	.L76
	cmpb	$10, %dl
	je	.L76
	leaq	buf.24650(%rip), %rax
	movq	%rax, -464(%rbp)
	jmp	.L35
.L49:
	call	opt_arg@PLT
	movq	%rax, -464(%rbp)
	jmp	.L35
.L50:
	call	opt_arg@PLT
	movq	%rax, %rdi
	movq	%rax, -448(%rbp)
	call	strlen@PLT
	xorl	%ecx, %ecx
	subl	$1, %eax
	testl	%eax, %eax
	jle	.L69
	movq	-448(%rbp), %rdx
	cltq
	addq	%rax, %rdx
	cmpb	$107, (%rdx)
	jne	.L69
	movb	$0, (%rdx)
	movl	$1, %ecx
.L69:
	movl	%ecx, -448(%rbp)
	call	opt_arg@PLT
	leaq	-376(%rbp), %rsi
	movq	%rax, %rdi
	call	opt_long@PLT
	testl	%eax, %eax
	je	.L67
	movq	-376(%rbp), %rax
	testq	%rax, %rax
	js	.L67
	movl	-448(%rbp), %ecx
	testl	%ecx, %ecx
	je	.L72
	movabsq	$9007199254740990, %rcx
	cmpq	%rcx, %rax
	jg	.L67
	salq	$10, %rax
	movq	%rax, -376(%rbp)
.L72:
	movl	%eax, -480(%rbp)
	jmp	.L35
.L51:
	movl	$1, -456(%rbp)
	jmp	.L35
.L52:
	movl	$1, -528(%rbp)
	jmp	.L35
.L53:
	movl	$2, -476(%rbp)
	jmp	.L35
.L54:
	movl	$1, -500(%rbp)
	jmp	.L35
.L55:
	movl	$1, -472(%rbp)
	jmp	.L35
.L56:
	movl	$0, -472(%rbp)
	jmp	.L35
.L57:
	movl	$1, -504(%rbp)
	jmp	.L35
.L58:
	movl	$1, %r15d
	jmp	.L35
.L59:
	movl	$1, -476(%rbp)
	jmp	.L35
.L60:
	call	opt_arg@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	setup_engine@PLT
	movq	%rax, -424(%rbp)
	jmp	.L35
.L61:
	call	opt_arg@PLT
	movq	%rax, -440(%rbp)
	jmp	.L35
.L62:
	call	opt_arg@PLT
	movq	%rax, -512(%rbp)
	jmp	.L35
.L63:
	call	opt_arg@PLT
	movq	%rax, -432(%rbp)
	jmp	.L35
.L65:
	movq	bio_out(%rip), %rdi
	leaq	.LC10(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_out(%rip), %rax
	leaq	show_ciphers(%rip), %rsi
	movl	$2, %edi
	leaq	-368(%rbp), %rdx
	movl	$0, -360(%rbp)
	movq	%rax, -368(%rbp)
	call	OBJ_NAME_do_all_sorted@PLT
	movq	bio_out(%rip), %rdi
	leaq	.LC4(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L256:
	xorl	%r13d, %r13d
	xorl	%r15d, %r15d
	jmp	.L255
.L66:
	leaq	enc_options(%rip), %rdi
	call	opt_help@PLT
	jmp	.L256
	.p2align 4,,10
	.p2align 3
.L263:
	movl	$4, %ecx
	leaq	.LC7(%rip), %rdi
	movq	%r14, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	movsbl	%al, %eax
	movl	%eax, -456(%rbp)
	testl	%eax, %eax
	je	.L33
	movq	bio_err(%rip), %rdi
	movq	%r14, %rdx
	xorl	%eax, %eax
	xorl	%r13d, %r13d
	leaq	.LC8(%rip), %rsi
	xorl	%r14d, %r14d
	movl	$1, %r15d
	call	BIO_printf@PLT
	movq	$0, -448(%rbp)
	movq	$0, -440(%rbp)
	movq	$0, -432(%rbp)
	movq	$0, -424(%rbp)
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L74:
	movq	bio_err(%rip), %rdi
	movq	%r14, %rdx
	leaq	.LC43(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L67
.L160:
	movl	$2, -540(%rbp)
	movl	$2, %r14d
	jmp	.L86
.L170:
	movl	-480(%rbp), %ecx
	leal	2(%rcx), %eax
	movslq	%eax, %rdx
	sarl	$31, %eax
	imulq	$1431655766, %rdx, %rdx
	shrq	$32, %rdx
	subl	%eax, %edx
	movslq	%ecx, %rax
	sarl	$31, %ecx
	imulq	$715827883, %rax, %rax
	sarq	$35, %rax
	subl	%ecx, %eax
	leal	41(%rax,%rdx,2), %r13d
	addl	%r13d, %r13d
	jmp	.L83
.L271:
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	leaq	-384(%rbp), %rdx
	call	app_passwd@PLT
	testl	%eax, %eax
	je	.L276
	movq	-384(%rbp), %rax
	testq	%rax, %rax
	movq	%rax, -464(%rbp)
	sete	%al
	jmp	.L89
.L274:
	call	BIO_f_base64@PLT
	movq	%rax, %rdi
	call	BIO_new@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L166
	movl	-500(%rbp), %eax
	testl	%eax, %eax
	jne	.L277
.L98:
	movl	-528(%rbp), %eax
	testl	%eax, %eax
	jne	.L278
.L99:
	testl	%ebx, %ebx
	je	.L100
	movq	-440(%rbp), %rsi
	movq	%r14, %rdi
	call	BIO_push@PLT
	movq	%rax, -456(%rbp)
	movq	-432(%rbp), %rax
	movq	%rax, -512(%rbp)
	jmp	.L97
.L273:
	movq	BIO_debug_callback@GOTPCREL(%rip), %r14
	movq	-432(%rbp), %rdi
	movq	%r14, %rsi
	call	BIO_set_callback@PLT
	movq	%r14, %rsi
	movq	-440(%rbp), %r14
	movq	%r14, %rdi
	call	BIO_set_callback@PLT
	movq	-432(%rbp), %rdi
	movq	bio_err(%rip), %rsi
	call	BIO_set_callback_arg@PLT
	movq	bio_err(%rip), %rsi
	movq	%r14, %rdi
	call	BIO_set_callback_arg@PLT
	jmp	.L96
.L272:
	leaq	.LC19(%rip), %r14
	testl	%ebx, %ebx
	leaq	.LC20(%rip), %rax
	movq	%r13, -440(%rbp)
	cmovne	%r14, %rax
	movl	%r15d, -464(%rbp)
	movq	-448(%rbp), %r13
	leaq	-256(%rbp), %r14
	movq	%rax, %r15
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L92:
	js	.L279
.L95:
	movq	%r12, %rdi
	call	EVP_CIPHER_nid@PLT
	movl	%eax, %edi
	call	OBJ_nid2ln@PLT
	movl	$200, %esi
	movq	%r14, %rdi
	movq	%r15, %r8
	movq	%rax, %rcx
	leaq	.LC21(%rip), %rdx
	xorl	%eax, %eax
	call	BIO_snprintf@PLT
	movb	$0, 0(%r13)
	movl	%ebx, %ecx
	movq	%r14, %rdx
	movl	$512, %esi
	movq	%r13, %rdi
	call	EVP_read_pw_string@PLT
	testl	%eax, %eax
	jne	.L92
	movq	-448(%rbp), %rax
	movq	-440(%rbp), %r13
	movl	-464(%rbp), %r15d
	cmpb	$0, (%rax)
	je	.L162
	movq	%rax, -464(%rbp)
	jmp	.L91
.L269:
	movl	-480(%rbp), %edx
	movq	bio_err(%rip), %rdi
	leaq	.LC15(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L85
.L270:
	movl	%r14d, %edi
	call	dup_bio_in@PLT
	movq	%rax, -432(%rbp)
	jmp	.L88
.L279:
	movq	bio_err(%rip), %rdi
	leaq	.LC22(%rip), %rsi
	xorl	%eax, %eax
	movq	-440(%rbp), %r13
	call	BIO_printf@PLT
.L162:
	xorl	%r12d, %r12d
	xorl	%r14d, %r14d
	movl	$1, %r15d
	movq	$0, -440(%rbp)
	jmp	.L34
.L268:
	call	EVP_sha256@PLT
	movq	%rax, -392(%rbp)
	jmp	.L81
.L252:
	movq	%r14, -464(%rbp)
	movq	-512(%rbp), %rbx
	movl	%r15d, -476(%rbp)
	movl	-480(%rbp), %r14d
	movq	%r12, -472(%rbp)
	movq	-456(%rbp), %r15
	jmp	.L140
	.p2align 4,,10
	.p2align 3
.L137:
	movl	%eax, %edx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	BIO_write@PLT
	cmpl	%r12d, %eax
	jne	.L280
.L140:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$10, %esi
	movq	%rbx, %rdi
	call	BIO_ctrl@PLT
	testl	%eax, %eax
	jne	.L141
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$2, %esi
	movq	%rbx, %rdi
	call	BIO_ctrl@PLT
	testl	%eax, %eax
	jne	.L254
.L141:
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	BIO_read@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	jg	.L137
.L254:
	movq	-456(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	movq	-464(%rbp), %r14
	movq	-472(%rbp), %r12
	movl	-476(%rbp), %r15d
	call	BIO_ctrl@PLT
	testl	%eax, %eax
	je	.L281
	testl	%r15d, %r15d
	je	.L34
	movq	-432(%rbp), %rdi
	xorl	%r15d, %r15d
	call	BIO_number_read@PLT
	movq	bio_err(%rip), %rdi
	leaq	.LC40(%rip), %rsi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	-440(%rbp), %rdi
	call	BIO_number_written@PLT
	movq	bio_err(%rip), %rdi
	leaq	.LC41(%rip), %rsi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L34
.L266:
	movq	%r14, %rdx
	leaq	.LC13(%rip), %rsi
	jmp	.L261
.L103:
	cmpl	$1, -488(%rbp)
	je	.L144
	xorl	%edx, %edx
.L145:
	movq	bio_err(%rip), %rdi
	leaq	.LC28(%rip), %rsi
	xorl	%eax, %eax
	movq	%rdx, -488(%rbp)
	call	BIO_printf@PLT
	leaq	-336(%rbp), %rax
	movq	%r12, %rdi
	movl	-528(%rbp), %r8d
	pushq	%rax
	leaq	-320(%rbp), %rax
	movq	-464(%rbp), %rcx
	movl	$1, %r9d
	movq	-488(%rbp), %rdx
	movq	-392(%rbp), %rsi
	pushq	%rax
	call	EVP_BytesToKey@PLT
	popq	%rdi
	popq	%r8
	testl	%eax, %eax
	je	.L282
.L115:
	movq	-448(%rbp), %rcx
	cmpq	%rcx, -464(%rbp)
	je	.L283
	movq	-528(%rbp), %rsi
	movq	-464(%rbp), %rdi
	call	OPENSSL_cleanse@PLT
.L117:
	cmpq	$0, -520(%rbp)
	je	.L119
.L149:
	movq	%r12, %rdi
	call	EVP_CIPHER_iv_length@PLT
	movl	%eax, %edx
	testl	%eax, %eax
	jne	.L118
	movq	bio_err(%rip), %rdi
	leaq	.LC30(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L119:
	cmpq	$0, -496(%rbp)
	je	.L121
	movq	%r12, %rdi
	call	EVP_CIPHER_key_length@PLT
	movq	-496(%rbp), %rdi
	leaq	-320(%rbp), %rsi
	movl	%eax, %edx
	call	set_hex
	testl	%eax, %eax
	je	.L284
	movq	-496(%rbp), %rdi
	call	strlen@PLT
	movq	-496(%rbp), %rdi
	movq	%rax, %rsi
	call	OPENSSL_cleanse@PLT
.L121:
	call	BIO_f_cipher@PLT
	movq	%rax, %rdi
	call	BIO_new@PLT
	movq	%rax, -464(%rbp)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L168
	xorl	%edx, %edx
	leaq	-408(%rbp), %rcx
	movl	$129, %esi
	call	BIO_ctrl@PLT
	movq	-408(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	movl	%ebx, %r9d
	movq	%r12, %rsi
	call	EVP_CipherInit_ex@PLT
	testl	%eax, %eax
	je	.L257
	movl	-504(%rbp), %esi
	movq	-408(%rbp), %rdi
	testl	%esi, %esi
	jne	.L285
.L124:
	leaq	-320(%rbp), %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movl	%ebx, %r9d
	leaq	-336(%rbp), %r8
	movq	%rax, %rcx
	movq	%rax, -496(%rbp)
	movq	%r8, -488(%rbp)
	call	EVP_CipherInit_ex@PLT
	testl	%eax, %eax
	je	.L257
	movl	-500(%rbp), %ecx
	testl	%ecx, %ecx
	jne	.L286
.L126:
	movl	-476(%rbp), %edx
	testl	%edx, %edx
	je	.L136
	movl	-472(%rbp), %eax
	testl	%eax, %eax
	je	.L287
.L128:
	movq	%r12, %rdi
	call	EVP_CIPHER_key_length@PLT
	testl	%eax, %eax
	jg	.L288
.L130:
	movq	%r12, %rdi
	call	EVP_CIPHER_iv_length@PLT
	testl	%eax, %eax
	jg	.L289
.L133:
	cmpl	$2, -476(%rbp)
	je	.L290
.L136:
	movq	-464(%rbp), %r12
	movq	-456(%rbp), %rsi
	movq	%r12, %rdi
	call	BIO_push@PLT
	movq	%r14, -464(%rbp)
	movq	-512(%rbp), %rbx
	movl	%r15d, -476(%rbp)
	movl	-480(%rbp), %r14d
	movq	%rax, %r15
	movq	%rax, -456(%rbp)
	movq	%r12, -472(%rbp)
	jmp	.L140
.L164:
	xorl	%r12d, %r12d
	xorl	%r14d, %r14d
	movl	$1, %r15d
	jmp	.L34
.L102:
	cmpq	$0, -520(%rbp)
	jne	.L149
	movq	%r12, %rdi
	call	EVP_CIPHER_iv_length@PLT
	testl	%eax, %eax
	je	.L119
	movq	bio_err(%rip), %rdi
	leaq	.LC32(%rip), %rsi
	xorl	%eax, %eax
	xorl	%r12d, %r12d
	movl	$1, %r15d
	call	BIO_printf@PLT
	jmp	.L34
.L280:
	movq	bio_err(%rip), %rdi
	leaq	.LC24(%rip), %rsi
	xorl	%eax, %eax
	movq	-464(%rbp), %r14
	movq	-472(%rbp), %r12
	movl	$1, %r15d
	call	BIO_printf@PLT
	jmp	.L34
.L100:
	movq	-432(%rbp), %rsi
	movq	%r14, %rdi
	call	BIO_push@PLT
	movq	%rax, -512(%rbp)
	movq	-440(%rbp), %rax
	movq	%rax, -456(%rbp)
	jmp	.L97
.L118:
	movq	-520(%rbp), %rdi
	leaq	-336(%rbp), %rsi
	call	set_hex
	testl	%eax, %eax
	jne	.L119
	movq	bio_err(%rip), %rdi
	leaq	.LC31(%rip), %rsi
	movl	$1, %r15d
	xorl	%r12d, %r12d
	call	BIO_printf@PLT
	jmp	.L34
.L267:
	movq	%r14, %rdx
	leaq	.LC14(%rip), %rsi
	jmp	.L261
.L108:
	cmpl	$1, -488(%rbp)
	movq	-536(%rbp), %rdx
	jne	.L145
	movq	%r12, %rdi
	call	EVP_CIPHER_key_length@PLT
	movq	%r12, %rdi
	movl	%eax, -488(%rbp)
	call	EVP_CIPHER_iv_length@PLT
	movq	-536(%rbp), %rdx
	movl	$8, %ecx
	movl	%eax, -540(%rbp)
.L143:
	leaq	-256(%rbp), %r10
	movl	-488(%rbp), %eax
	addl	-540(%rbp), %eax
	movq	-392(%rbp), %r9
	movl	-412(%rbp), %r8d
	pushq	%r10
	movl	-528(%rbp), %esi
	movq	-464(%rbp), %rdi
	pushq	%rax
	movq	%r10, -536(%rbp)
	call	PKCS5_PBKDF2_HMAC@PLT
	popq	%r9
	popq	%r10
	testl	%eax, %eax
	movq	-536(%rbp), %r10
	je	.L291
	movslq	-488(%rbp), %rdx
	leaq	-320(%rbp), %rdi
	movq	%r10, %rsi
	movl	$64, %ecx
	movq	%r10, -488(%rbp)
	movq	%rdx, -536(%rbp)
	call	__memcpy_chk@PLT
	movq	-536(%rbp), %rdx
	movslq	-540(%rbp), %r8
	leaq	-336(%rbp), %rdi
	movq	-488(%rbp), %r10
	movl	$16, %ecx
	leaq	(%r10,%rdx), %rsi
	movq	%r8, %rdx
	call	__memcpy_chk@PLT
	jmp	.L115
.L276:
	movq	bio_err(%rip), %rdi
	leaq	.LC18(%rip), %rsi
	xorl	%r12d, %r12d
	xorl	%r14d, %r14d
	movl	$1, %r15d
	call	BIO_printf@PLT
	movq	$0, -440(%rbp)
	jmp	.L34
.L168:
	xorl	%r12d, %r12d
	movl	$1, %r15d
	jmp	.L34
.L284:
	movq	bio_err(%rip), %rdi
	leaq	.LC33(%rip), %rsi
	movl	$1, %r15d
	xorl	%r12d, %r12d
	call	BIO_printf@PLT
	jmp	.L34
.L73:
	call	opt_arg@PLT
	movq	bio_err(%rip), %rdi
	movq	%r14, %rdx
	leaq	.LC11(%rip), %rsi
	movq	%rax, %rcx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L67
.L104:
	movq	-512(%rbp), %rdi
	leaq	-352(%rbp), %rsi
	movl	$8, %edx
	call	BIO_read@PLT
	cmpl	$8, %eax
	jne	.L113
	leaq	-344(%rbp), %rax
	movq	-512(%rbp), %rdi
	movl	$8, %edx
	movq	%rax, %rsi
	movq	%rax, -536(%rbp)
	call	BIO_read@PLT
	cmpl	$8, %eax
	jne	.L113
	movabsq	$6872321943298400595, %rax
	cmpq	%rax, -352(%rbp)
	je	.L108
	leaq	.LC26(%rip), %rsi
.L260:
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
	movl	$1, %r15d
	xorl	%r12d, %r12d
	call	BIO_printf@PLT
	jmp	.L34
.L277:
	movq	BIO_debug_callback@GOTPCREL(%rip), %rsi
	movq	%r14, %rdi
	call	BIO_set_callback@PLT
	movq	bio_err(%rip), %rsi
	movq	%r14, %rdi
	call	BIO_set_callback_arg@PLT
	jmp	.L98
.L278:
	movl	$256, %esi
	movq	%r14, %rdi
	call	BIO_set_flags@PLT
	jmp	.L99
.L257:
	movq	%r12, %rdi
	movl	$1, %r15d
	call	EVP_CIPHER_nid@PLT
	movl	%eax, %edi
	call	OBJ_nid2sn@PLT
	movq	bio_err(%rip), %rdi
	leaq	.LC34(%rip), %rsi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	movq	-464(%rbp), %r12
	jmp	.L34
.L283:
	movl	$512, %esi
	movq	%rcx, %rdi
	call	OPENSSL_cleanse@PLT
	jmp	.L117
.L285:
	xorl	%esi, %esi
	call	EVP_CIPHER_CTX_set_padding@PLT
	movq	-408(%rbp), %rdi
	jmp	.L124
.L166:
	movl	-456(%rbp), %r15d
	xorl	%r12d, %r12d
	jmp	.L34
.L113:
	leaq	.LC25(%rip), %rsi
	jmp	.L260
.L281:
	movq	bio_err(%rip), %rdi
	leaq	.LC39(%rip), %rsi
	xorl	%eax, %eax
	movl	$1, %r15d
	call	BIO_printf@PLT
	jmp	.L34
.L282:
	movq	bio_err(%rip), %rdi
	leaq	.LC29(%rip), %rsi
	movl	$1, %r15d
	xorl	%r12d, %r12d
	call	BIO_printf@PLT
	jmp	.L34
.L291:
	movq	bio_err(%rip), %rdi
	leaq	.LC27(%rip), %rsi
	movl	$1, %r15d
	xorl	%r12d, %r12d
	call	BIO_printf@PLT
	jmp	.L34
.L286:
	movq	-464(%rbp), %rbx
	movq	BIO_debug_callback@GOTPCREL(%rip), %rsi
	movq	%rbx, %rdi
	call	BIO_set_callback@PLT
	movq	bio_err(%rip), %rsi
	movq	%rbx, %rdi
	call	BIO_set_callback_arg@PLT
	jmp	.L126
.L105:
	movl	$8, %esi
	movq	%rax, %rdi
	call	RAND_bytes@PLT
	testl	%eax, %eax
	jg	.L109
	jmp	.L259
.L289:
	leaq	.LC38(%rip), %rsi
	movl	$1, %edi
	xorl	%eax, %eax
	xorl	%ebx, %ebx
	call	__printf_chk@PLT
	jmp	.L134
.L135:
	movq	-488(%rbp), %rax
	leaq	.LC36(%rip), %rsi
	movl	$1, %edi
	movzbl	(%rax,%rbx), %edx
	xorl	%eax, %eax
	addq	$1, %rbx
	call	__printf_chk@PLT
.L134:
	movq	%r12, %rdi
	call	EVP_CIPHER_iv_length@PLT
	cmpl	%ebx, %eax
	jg	.L135
	movl	$10, %edi
	call	putchar@PLT
	jmp	.L133
.L288:
	leaq	.LC37(%rip), %rsi
	movl	$1, %edi
	xorl	%eax, %eax
	xorl	%ebx, %ebx
	call	__printf_chk@PLT
	jmp	.L131
.L132:
	movq	-496(%rbp), %rax
	leaq	.LC36(%rip), %rsi
	movl	$1, %edi
	movzbl	(%rax,%rbx), %edx
	xorl	%eax, %eax
	addq	$1, %rbx
	call	__printf_chk@PLT
.L131:
	movq	%r12, %rdi
	call	EVP_CIPHER_key_length@PLT
	cmpl	%ebx, %eax
	jg	.L132
	movl	$10, %edi
	call	putchar@PLT
	jmp	.L130
.L287:
	leaq	.LC35(%rip), %rsi
	movl	$1, %edi
	xorl	%eax, %eax
	call	__printf_chk@PLT
	leaq	-344(%rbp), %rbx
	leaq	.LC36(%rip), %rcx
.L129:
	movzbl	(%rbx), %edx
	movq	%rcx, %rsi
	xorl	%eax, %eax
	movl	$1, %edi
	addq	$1, %rbx
	call	__printf_chk@PLT
	cmpq	%rbx, -488(%rbp)
	leaq	.LC36(%rip), %rcx
	jne	.L129
	movl	$10, %edi
	call	putchar@PLT
	jmp	.L128
.L106:
	leaq	.LC23(%rip), %rsi
	jmp	.L258
.L144:
	movq	%r12, %rdi
	call	EVP_CIPHER_key_length@PLT
	movq	%r12, %rdi
	movl	%eax, -488(%rbp)
	call	EVP_CIPHER_iv_length@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	%eax, -540(%rbp)
	jmp	.L143
.L290:
	movq	-464(%rbp), %r12
	xorl	%r15d, %r15d
	jmp	.L34
.L275:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1437:
	.size	enc_main, .-enc_main
	.section	.rodata
	.align 8
	.type	magic.24651, @object
	.size	magic.24651, 9
magic.24651:
	.string	"Salted__"
	.local	buf.24650
	.comm	buf.24650,128,32
	.globl	enc_options
	.section	.rodata.str1.1
.LC44:
	.string	"help"
.LC45:
	.string	"Display this summary"
.LC46:
	.string	"list"
.LC47:
	.string	"List ciphers"
.LC48:
	.string	"ciphers"
.LC49:
	.string	"Alias for -list"
.LC50:
	.string	"in"
.LC51:
	.string	"Input file"
.LC52:
	.string	"out"
.LC53:
	.string	"Output file"
.LC54:
	.string	"pass"
.LC55:
	.string	"Passphrase source"
.LC56:
	.string	"e"
.LC57:
	.string	"Encrypt"
.LC58:
	.string	"d"
.LC59:
	.string	"Decrypt"
.LC60:
	.string	"p"
.LC61:
	.string	"Print the iv/key"
.LC62:
	.string	"P"
.LC63:
	.string	"Print the iv/key and exit"
.LC64:
	.string	"v"
.LC65:
	.string	"Verbose output"
.LC66:
	.string	"nopad"
	.section	.rodata.str1.8
	.align 8
.LC67:
	.string	"Disable standard block padding"
	.section	.rodata.str1.1
.LC68:
	.string	"salt"
.LC69:
	.string	"Use salt in the KDF (default)"
.LC70:
	.string	"nosalt"
.LC71:
	.string	"Do not use salt in the KDF"
.LC72:
	.string	"debug"
.LC73:
	.string	"Print debug info"
.LC74:
	.string	"a"
	.section	.rodata.str1.8
	.align 8
.LC75:
	.string	"Base64 encode/decode, depending on encryption flag"
	.section	.rodata.str1.1
.LC76:
	.string	"Same as option -a"
.LC77:
	.string	"A"
	.section	.rodata.str1.8
	.align 8
.LC78:
	.string	"Used with -[base64|a] to specify base64 buffer as a single line"
	.section	.rodata.str1.1
.LC79:
	.string	"bufsize"
.LC80:
	.string	"Buffer size"
.LC81:
	.string	"k"
.LC82:
	.string	"Passphrase"
.LC83:
	.string	"kfile"
.LC84:
	.string	"Read passphrase from file"
.LC85:
	.string	"K"
.LC86:
	.string	"Raw key, in hex"
.LC87:
	.string	"S"
.LC88:
	.string	"Salt, in hex"
.LC89:
	.string	"iv"
.LC90:
	.string	"IV in hex"
.LC91:
	.string	"md"
	.section	.rodata.str1.8
	.align 8
.LC92:
	.string	"Use specified digest to create a key from the passphrase"
	.section	.rodata.str1.1
.LC93:
	.string	"iter"
	.section	.rodata.str1.8
	.align 8
.LC94:
	.string	"Specify the iteration count and force use of PBKDF2"
	.section	.rodata.str1.1
.LC95:
	.string	"pbkdf2"
	.section	.rodata.str1.8
	.align 8
.LC96:
	.string	"Use password-based key derivation function 2"
	.section	.rodata.str1.1
.LC97:
	.string	"none"
.LC98:
	.string	"Don't encrypt"
.LC99:
	.string	""
.LC100:
	.string	"Any supported cipher"
.LC101:
	.string	"rand"
	.section	.rodata.str1.8
	.align 8
.LC102:
	.string	"Load the file(s) into the random number generator"
	.section	.rodata.str1.1
.LC103:
	.string	"writerand"
	.section	.rodata.str1.8
	.align 8
.LC104:
	.string	"Write random data to the specified file"
	.section	.rodata.str1.1
.LC105:
	.string	"engine"
	.section	.rodata.str1.8
	.align 8
.LC106:
	.string	"Use engine, possibly a hardware device"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	enc_options, @object
	.size	enc_options, 792
enc_options:
	.quad	.LC44
	.long	1
	.long	45
	.quad	.LC45
	.quad	.LC46
	.long	2
	.long	45
	.quad	.LC47
	.quad	.LC48
	.long	2
	.long	45
	.quad	.LC49
	.quad	.LC50
	.long	4
	.long	60
	.quad	.LC51
	.quad	.LC52
	.long	5
	.long	62
	.quad	.LC53
	.quad	.LC54
	.long	6
	.long	115
	.quad	.LC55
	.quad	.LC56
	.long	3
	.long	45
	.quad	.LC57
	.quad	.LC58
	.long	8
	.long	45
	.quad	.LC59
	.quad	.LC60
	.long	9
	.long	45
	.quad	.LC61
	.quad	.LC62
	.long	15
	.long	45
	.quad	.LC63
	.quad	.LC64
	.long	10
	.long	45
	.quad	.LC65
	.quad	.LC66
	.long	11
	.long	45
	.quad	.LC67
	.quad	.LC68
	.long	12
	.long	45
	.quad	.LC69
	.quad	.LC70
	.long	13
	.long	45
	.quad	.LC71
	.quad	.LC72
	.long	14
	.long	45
	.quad	.LC73
	.quad	.LC74
	.long	17
	.long	45
	.quad	.LC75
	.quad	.LC6
	.long	17
	.long	45
	.quad	.LC76
	.quad	.LC77
	.long	16
	.long	45
	.quad	.LC78
	.quad	.LC79
	.long	19
	.long	115
	.quad	.LC80
	.quad	.LC81
	.long	20
	.long	115
	.quad	.LC82
	.quad	.LC83
	.long	21
	.long	60
	.quad	.LC84
	.quad	.LC85
	.long	22
	.long	115
	.quad	.LC86
	.quad	.LC87
	.long	24
	.long	115
	.quad	.LC88
	.quad	.LC89
	.long	25
	.long	115
	.quad	.LC90
	.quad	.LC91
	.long	26
	.long	115
	.quad	.LC92
	.quad	.LC93
	.long	27
	.long	112
	.quad	.LC94
	.quad	.LC95
	.long	28
	.long	45
	.quad	.LC96
	.quad	.LC97
	.long	23
	.long	45
	.quad	.LC98
	.quad	.LC99
	.long	29
	.long	45
	.quad	.LC100
	.quad	.LC101
	.long	1501
	.long	115
	.quad	.LC102
	.quad	.LC103
	.long	1502
	.long	62
	.quad	.LC104
	.quad	.LC105
	.long	7
	.long	115
	.quad	.LC106
	.quad	0
	.zero	16
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
