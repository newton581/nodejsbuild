	.file	"passwd.c"
	.text
	.section	.rodata
.LC0:
	.string	""
	.string	""
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"../deps/openssl/openssl/apps/passwd.c"
	.text
	.p2align 4
	.type	md5crypt, @function
md5crypt:
.LFB1436:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	leaq	-94(%rbp), %r15
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	out_buf.24812(%rip), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$104, %rsp
	movq	%rdx, -104(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strlen@PLT
	movq	%r14, %rdi
	movb	$0, out_buf.24812(%rip)
	movq	%rax, -128(%rbp)
	call	strlen@PLT
	movq	%r14, %rsi
	movl	$5, %edx
	movq	%r15, %rdi
	movq	%rax, %r13
	leaq	-89(%rbp), %r14
	call	OPENSSL_strlcpy@PLT
	movq	-104(%rbp), %r8
	movl	$9, %edx
	movq	%r14, %rdi
	movq	%r8, %rsi
	call	OPENSSL_strlcpy@PLT
	movq	%r14, %rdi
	call	strlen@PLT
	movl	$41, %edx
	movq	%rax, -104(%rbp)
	testq	%r13, %r13
	je	.L2
	leaq	ascii_dollar(%rip), %rsi
	movq	%r12, %rdi
	call	OPENSSL_strlcat@PLT
	cmpq	$4, %r13
	ja	.L37
	movl	$41, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	OPENSSL_strlcat@PLT
	movl	$41, %edx
	leaq	ascii_dollar(%rip), %rsi
	movq	%r12, %rdi
	call	OPENSSL_strlcat@PLT
	movl	$41, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	OPENSSL_strlcat@PLT
	movq	%r12, %rdx
.L4:
	movl	(%rdx), %ecx
	addq	$4, %rdx
	leal	-16843009(%rcx), %eax
	notl	%ecx
	andl	%ecx, %eax
	andl	$-2139062144, %eax
	je	.L4
	movl	%eax, %ecx
	shrl	$16, %ecx
	testl	$32896, %eax
	cmove	%ecx, %eax
	leaq	2(%rdx), %rcx
	cmove	%rcx, %rdx
	movl	%eax, %esi
	addb	%al, %sil
	sbbq	$3, %rdx
	subq	%r12, %rdx
	cmpq	$14, %rdx
	ja	.L37
	leaq	2(%r12,%r13), %rax
	movq	%rax, -112(%rbp)
	call	EVP_MD_CTX_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L37
.L99:
	call	EVP_md5@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	EVP_DigestInit_ex@PLT
	testl	%eax, %eax
	je	.L33
	movq	-128(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	je	.L33
	testq	%r13, %r13
	je	.L9
	movl	$1, %edx
	leaq	ascii_dollar(%rip), %rsi
	movq	%r12, %rdi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	je	.L33
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	je	.L33
	movl	$1, %edx
	leaq	ascii_dollar(%rip), %rsi
	movq	%r12, %rdi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	je	.L33
.L9:
	movq	-104(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	je	.L33
	call	EVP_MD_CTX_new@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L6
	call	EVP_md5@PLT
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	EVP_DigestInit_ex@PLT
	testl	%eax, %eax
	je	.L6
	movq	-128(%rbp), %r15
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%r15, %rdx
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	je	.L6
	movq	-104(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	je	.L6
	movq	%r15, %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%r15, -128(%rbp)
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	je	.L6
	leaq	-80(%rbp), %r15
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	%r15, %rsi
	call	EVP_DigestFinal_ex@PLT
	testl	%eax, %eax
	je	.L6
	movq	-128(%rbp), %rax
	movl	%eax, %ecx
	cmpl	$16, %eax
	jbe	.L10
	.p2align 4,,10
	.p2align 3
.L11:
	movl	$16, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%ecx, -132(%rbp)
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	je	.L6
	movl	-132(%rbp), %ecx
	subl	$16, %ecx
	cmpl	$16, %ecx
	ja	.L11
.L10:
	movl	%ecx, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	je	.L6
	movq	-128(%rbp), %rax
	movl	%eax, %ecx
	testl	%eax, %eax
	je	.L12
	.p2align 4,,10
	.p2align 3
.L14:
	testb	$1, %cl
	leaq	.LC0(%rip), %rsi
	movl	$1, %edx
	movq	%r12, %rdi
	cmove	%rbx, %rsi
	movl	%ecx, -132(%rbp)
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	je	.L6
	movl	-132(%rbp), %ecx
	sarl	%ecx
	jne	.L14
.L12:
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	EVP_DigestFinal_ex@PLT
	testl	%eax, %eax
	je	.L35
	movl	$0, -132(%rbp)
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L98:
	movl	$16, %edx
	movq	%r15, %rsi
.L16:
	movq	%r13, %rdi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	je	.L6
	imull	$-1431655765, -132(%rbp), %eax
	cmpl	$1431655765, %eax
	ja	.L17
.L20:
	imull	$-1227133513, -132(%rbp), %eax
	cmpl	$613566756, %eax
	ja	.L96
.L19:
	movl	-136(%rbp), %eax
	testl	%eax, %eax
	je	.L21
	movl	$16, %edx
	movq	%r15, %rsi
.L22:
	movq	%r13, %rdi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	je	.L6
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	EVP_DigestFinal_ex@PLT
	testl	%eax, %eax
	je	.L6
	addl	$1, -132(%rbp)
	movl	-132(%rbp), %eax
	cmpl	$1000, %eax
	je	.L97
.L23:
	call	EVP_md5@PLT
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	EVP_DigestInit_ex@PLT
	testl	%eax, %eax
	je	.L6
	movl	-132(%rbp), %eax
	andl	$1, %eax
	movl	%eax, -136(%rbp)
	je	.L98
	movq	-128(%rbp), %rdx
	movq	%rbx, %rsi
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L2:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	OPENSSL_strlcat@PLT
	movq	%r12, %rdx
.L24:
	movl	(%rdx), %ecx
	addq	$4, %rdx
	leal	-16843009(%rcx), %eax
	notl	%ecx
	andl	%ecx, %eax
	andl	$-2139062144, %eax
	je	.L24
	movl	%eax, %ecx
	shrl	$16, %ecx
	testl	$32896, %eax
	cmove	%ecx, %eax
	leaq	2(%rdx), %rcx
	cmove	%rcx, %rdx
	movl	%eax, %esi
	addb	%al, %sil
	sbbq	$3, %rdx
	subq	%r12, %rdx
	cmpq	$14, %rdx
	ja	.L37
	leaq	out_buf.24812(%rip), %rax
	movq	%rax, -112(%rbp)
	call	EVP_MD_CTX_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L99
	.p2align 4,,10
	.p2align 3
.L37:
	xorl	%r13d, %r13d
	xorl	%r12d, %r12d
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L33:
	xorl	%r13d, %r13d
.L6:
	movl	$482, %edx
	leaq	.LC1(%rip), %rsi
	xorl	%edi, %edi
	call	CRYPTO_free@PLT
	movq	%r13, %rdi
	call	EVP_MD_CTX_free@PLT
	movq	%r12, %rdi
	call	EVP_MD_CTX_free@PLT
	xorl	%eax, %eax
.L1:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L100
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	.cfi_restore_state
	movq	-128(%rbp), %rdx
	movq	%rbx, %rsi
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L17:
	movq	-104(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	jne	.L20
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L96:
	movq	-128(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	jne	.L19
	jmp	.L6
.L35:
	xorl	%eax, %eax
	jmp	.L1
.L97:
	movq	%r13, %rdi
	call	EVP_MD_CTX_free@PLT
	movq	%r12, %rdi
	call	EVP_MD_CTX_free@PLT
	movzbl	-72(%rbp), %edx
	movzbl	-78(%rbp), %eax
	movzbl	-69(%rbp), %edi
	movzbl	-75(%rbp), %esi
	salq	$8, %rdx
	movzbl	-80(%rbp), %ecx
	orq	%rax, %rdx
	movzbl	-67(%rbp), %eax
	salq	$8, %rdx
	orq	%rax, %rdx
	movzbl	-73(%rbp), %eax
	salq	$8, %rdx
	orq	%rax, %rdx
	movzbl	-79(%rbp), %eax
	salq	$8, %rdx
	orq	%rax, %rdx
	movzbl	-68(%rbp), %eax
	salq	$8, %rdx
	orq	%rax, %rdx
	movzbl	-74(%rbp), %eax
	salq	$8, %rdx
	orq	%rax, %rdx
	movzbl	%dil, %eax
	salq	$8, %rax
	salq	$8, %rdx
	orq	%rsi, %rax
	movzbl	-70(%rbp), %esi
	orq	%rcx, %rdx
	leaq	cov_2char(%rip), %rcx
	salq	$8, %rax
	movq	%rdx, -128(%rbp)
	movq	-128(%rbp), %rbx
	orq	%rsi, %rax
	movzbl	-76(%rbp), %esi
	movq	-112(%rbp), %rdx
	salq	$8, %rax
	movq	%rbx, %r8
	addq	-104(%rbp), %rdx
	orq	%rsi, %rax
	movzbl	-65(%rbp), %esi
	shrq	$16, %r8
	movb	$36, (%rdx)
	salq	$8, %rax
	orq	%rsi, %rax
	movzbl	-71(%rbp), %esi
	salq	$8, %rax
	orq	%rsi, %rax
	movzbl	-77(%rbp), %esi
	salq	$8, %rax
	orq	%rsi, %rax
	movzbl	-66(%rbp), %esi
	salq	$8, %rax
	orq	%rsi, %rax
	movq	%r8, %rsi
	andl	$63, %esi
	shrb	$6, %r8b
	movq	%rax, -120(%rbp)
	movzbl	(%rcx,%rsi), %esi
	movzbl	%r8b, %r8d
	movb	%sil, 1(%rdx)
	movzbl	%bh, %esi
	movq	%rsi, %r9
	movl	%ebx, %esi
	shrl	$6, %esi
	shrb	$4, %r9b
	andl	$60, %esi
	orl	%r8d, %esi
	movzbl	%r9b, %r8d
	movq	%rbx, %r9
	movslq	%esi, %rsi
	shrq	$40, %r9
	movzbl	(%rcx,%rsi), %esi
	movb	%sil, 2(%rdx)
	movl	%ebx, %esi
	sall	$4, %esi
	andl	$48, %esi
	orl	%r8d, %esi
	movq	%rbx, %r8
	movslq	%esi, %rsi
	shrq	$32, %r8
	movzbl	(%rcx,%rsi), %esi
	movb	%sil, 3(%rdx)
	movl	%ebx, %esi
	shrb	$2, %sil
	andl	$63, %esi
	movzbl	(%rcx,%rsi), %esi
	movb	%sil, 4(%rdx)
	movq	%r9, %rsi
	shrb	$6, %r9b
	andl	$63, %esi
	movzbl	%r9b, %r9d
	movzbl	(%rcx,%rsi), %esi
	movb	%sil, 5(%rdx)
	leal	0(,%r8,4), %esi
	shrb	$4, %r8b
	andl	$60, %esi
	movzbl	%r8b, %r8d
	orl	%r9d, %esi
	movq	%rax, %r9
	movslq	%esi, %rsi
	movzbl	(%rcx,%rsi), %esi
	movb	%sil, 6(%rdx)
	movl	%ebx, %esi
	shrl	$20, %esi
	andl	$48, %esi
	orl	%r8d, %esi
	movl	%eax, %r8d
	movslq	%esi, %rsi
	shrb	$6, %r8b
	movzbl	(%rcx,%rsi), %esi
	movzbl	%r8b, %r8d
	movb	%sil, 7(%rdx)
	movl	%ebx, %esi
	shrl	$26, %esi
	movzbl	(%rcx,%rsi), %esi
	movb	%sil, 8(%rdx)
	movq	%rax, %rsi
	andl	$63, %esi
	movzbl	(%rcx,%rsi), %esi
	movb	%sil, 9(%rdx)
	movq	%rbx, %rsi
	shrq	$56, %rsi
	sall	$2, %esi
	andl	$60, %esi
	orl	%r8d, %esi
	movq	%rbx, %r8
	shrq	$60, %rbx
	movslq	%esi, %rsi
	shrq	$48, %r8
	movzbl	(%rcx,%rsi), %esi
	movb	%sil, 10(%rdx)
	movl	%r8d, %esi
	sall	$4, %esi
	andl	$48, %esi
	orl	%esi, %ebx
	shrq	$24, %r9
	movslq	%ebx, %rsi
	movzbl	%ah, %ebx
	movzbl	(%rcx,%rsi), %esi
	shrb	$2, %bl
	movb	%sil, 11(%rdx)
	movl	%r8d, %esi
	movq	%rax, %r8
	shrb	$2, %sil
	shrq	$16, %r8
	andl	$63, %esi
	movzbl	(%rcx,%rsi), %esi
	movb	%sil, 12(%rdx)
	movq	%r9, %rsi
	shrb	$6, %r9b
	andl	$63, %esi
	movzbl	%r9b, %r9d
	movzbl	(%rcx,%rsi), %esi
	movb	%sil, 13(%rdx)
	leal	0(,%r8,4), %esi
	shrb	$4, %r8b
	andl	$60, %esi
	movzbl	%r8b, %r8d
	orl	%r9d, %esi
	movq	%rax, %r9
	movslq	%esi, %rsi
	shrq	$48, %r9
	movzbl	(%rcx,%rsi), %esi
	movb	%sil, 14(%rdx)
	movl	%eax, %esi
	shrl	$4, %esi
	andl	$48, %esi
	orl	%r8d, %esi
	movq	%rax, %r8
	shrq	$32, %rax
	movslq	%esi, %rsi
	shrq	$40, %r8
	movzbl	(%rcx,%rsi), %esi
	movb	%sil, 15(%rdx)
	movl	%ebx, %esi
	andl	$63, %esi
	movzbl	(%rcx,%rsi), %esi
	movb	%sil, 16(%rdx)
	movq	%r9, %rsi
	shrb	$6, %r9b
	andl	$63, %esi
	movzbl	%r9b, %r9d
	movzbl	(%rcx,%rsi), %esi
	movb	%sil, 17(%rdx)
	leal	0(,%r8,4), %esi
	shrb	$4, %r8b
	andl	$60, %esi
	movzbl	%r8b, %r8d
	orl	%r9d, %esi
	movslq	%esi, %rsi
	movzbl	(%rcx,%rsi), %esi
	movb	%sil, 18(%rdx)
	movl	%eax, %esi
	shrb	$2, %al
	andl	$63, %eax
	sall	$4, %esi
	movzbl	(%rcx,%rax), %eax
	andl	$48, %esi
	orl	%r8d, %esi
	movb	%al, 20(%rdx)
	movq	%rdi, %rax
	movslq	%esi, %rsi
	andl	$63, %eax
	movzbl	(%rcx,%rsi), %esi
	movzbl	(%rcx,%rax), %eax
	movb	%sil, 19(%rdx)
	movb	%al, 21(%rdx)
	movl	%edi, %eax
	shrb	$6, %al
	andl	$3, %eax
	movzbl	(%rcx,%rax), %eax
	movb	$0, 23(%rdx)
	movb	%al, 22(%rdx)
	leaq	out_buf.24812(%rip), %rax
	jmp	.L1
.L100:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1436:
	.size	md5crypt, .-md5crypt
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC2:
	.string	"apr1"
.LC3:
	.string	"1"
.LC4:
	.string	"5"
.LC5:
	.string	"6"
.LC6:
	.string	"salt buffer"
	.section	.rodata.str1.8
	.align 8
.LC7:
	.string	"Warning: truncating password to %u characters\n"
	.section	.rodata.str1.1
.LC8:
	.string	""
.LC9:
	.string	"rounds=%u"
.LC10:
	.string	"%s\t%s\n"
.LC11:
	.string	"%s\n"
	.text
	.p2align 4
	.type	do_passwd, @function
do_passwd:
.LFB1438:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%r9d, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	subq	$312, %rsp
	.cfi_offset 3, -56
	movl	40(%rbp), %ebx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%edi, %edi
	jne	.L102
	cmpl	$1, %ebx
	je	.L194
	leal	-2(%rbx), %eax
	cmpl	$1, %eax
	jbe	.L225
	cmpl	$6, %ebx
	jne	.L195
.L225:
	movl	$8, %r8d
	movl	$8, %ecx
.L104:
	leal	-4(%rbx), %eax
	movl	$16, %esi
	cmpl	$1, %eax
	movl	$16, %eax
	cmovbe	%esi, %r8d
	cmovbe	%rax, %rcx
.L103:
	cmpq	$0, (%rdx)
	je	.L106
	movq	(%r15), %rdi
.L107:
	movl	%r8d, %esi
	movq	%rcx, -264(%rbp)
	call	RAND_bytes@PLT
	movl	%eax, %r8d
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jle	.L101
	movq	-264(%rbp), %rcx
	testq	%rcx, %rcx
	je	.L109
	movq	(%r15), %rsi
	leaq	cov_2char(%rip), %rax
	movzbl	(%rsi), %edx
	andl	$63, %edx
	movzbl	(%rax,%rdx), %edx
	movb	%dl, (%rsi)
	movq	(%r15), %rsi
	movzbl	1(%rsi), %edx
	andl	$63, %edx
	movzbl	(%rax,%rdx), %edx
	movb	%dl, 1(%rsi)
	cmpq	$2, %rcx
	je	.L109
	movq	(%r15), %rsi
	movzbl	2(%rsi), %edx
	andl	$63, %edx
	movzbl	(%rax,%rdx), %edx
	movb	%dl, 2(%rsi)
	movq	(%r15), %rsi
	movzbl	3(%rsi), %edx
	andl	$63, %edx
	movzbl	(%rax,%rdx), %edx
	movb	%dl, 3(%rsi)
	movq	(%r15), %rsi
	movzbl	4(%rsi), %edx
	andl	$63, %edx
	movzbl	(%rax,%rdx), %edx
	movb	%dl, 4(%rsi)
	movq	(%r15), %rsi
	movzbl	5(%rsi), %edx
	andl	$63, %edx
	movzbl	(%rax,%rdx), %edx
	movb	%dl, 5(%rsi)
	movq	(%r15), %rsi
	movzbl	6(%rsi), %edx
	andl	$63, %edx
	movzbl	(%rax,%rdx), %edx
	movb	%dl, 6(%rsi)
	movq	(%r15), %rsi
	movzbl	7(%rsi), %edx
	andl	$63, %edx
	movzbl	(%rax,%rdx), %edx
	movb	%dl, 7(%rsi)
	cmpq	$8, %rcx
	je	.L109
	movq	(%r15), %rsi
	movzbl	8(%rsi), %edx
	andl	$63, %edx
	movzbl	(%rax,%rdx), %edx
	movb	%dl, 8(%rsi)
	movq	(%r15), %rsi
	movzbl	9(%rsi), %edx
	andl	$63, %edx
	movzbl	(%rax,%rdx), %edx
	movb	%dl, 9(%rsi)
	cmpq	$10, %rcx
	je	.L109
	movq	(%r15), %rsi
	movzbl	10(%rsi), %edx
	andl	$63, %edx
	movzbl	(%rax,%rdx), %edx
	movb	%dl, 10(%rsi)
	movq	(%r15), %rsi
	movzbl	11(%rsi), %edx
	andl	$63, %edx
	movzbl	(%rax,%rdx), %edx
	movb	%dl, 11(%rsi)
	movq	(%r15), %rsi
	movzbl	12(%rsi), %edx
	andl	$63, %edx
	movzbl	(%rax,%rdx), %edx
	movb	%dl, 12(%rsi)
	movq	(%r15), %rsi
	movzbl	13(%rsi), %edx
	andl	$63, %edx
	movzbl	(%rax,%rdx), %edx
	movb	%dl, 13(%rsi)
	movq	(%r15), %rsi
	movzbl	14(%rsi), %edx
	andl	$63, %edx
	movzbl	(%rax,%rdx), %edx
	movb	%dl, 14(%rsi)
	cmpq	$16, %rcx
	jne	.L197
	movq	(%r15), %rsi
	movzbl	15(%rsi), %edx
	andl	$63, %edx
	movzbl	(%rax,%rdx), %eax
	movb	%al, 15(%rsi)
.L109:
	movq	(%r15), %rax
	movb	$0, (%rax,%rcx)
.L102:
	movq	%r12, %rdi
	call	strlen@PLT
	cmpq	32(%rbp), %rax
	jbe	.L110
	testl	%r14d, %r14d
	je	.L342
.L111:
	movq	32(%rbp), %rax
	movb	$0, (%r12,%rax)
.L110:
	cmpl	$1, %ebx
	je	.L343
	leal	-2(%rbx), %eax
	cmpl	$1, %eax
	ja	.L114
	cmpl	$2, %ebx
	leaq	.LC3(%rip), %rax
	movq	(%r15), %rdx
	movq	%r12, %rdi
	leaq	.LC2(%rip), %rsi
	cmove	%rax, %rsi
	call	md5crypt
	movq	%rax, %rdx
.L113:
	movl	16(%rbp), %esi
	movl	24(%rbp), %edi
	testl	%esi, %esi
	setne	%al
	testl	%edi, %edi
	jne	.L328
.L345:
	testb	%al, %al
	je	.L189
	movq	%rdx, %rcx
	movq	%r12, %rdx
.L341:
	leaq	.LC10(%rip), %rsi
	movq	%r13, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movl	$1, %eax
.L101:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L344
	addq	$312, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L114:
	.cfi_restore_state
	cmpl	$6, %ebx
	jne	.L117
	movq	(%r15), %rdx
	leaq	.LC8(%rip), %rsi
	movq	%r12, %rdi
	call	md5crypt
	movl	16(%rbp), %esi
	movl	24(%rbp), %edi
	movq	%rax, %rdx
	testl	%esi, %esi
	setne	%al
	testl	%edi, %edi
	je	.L345
	.p2align 4,,10
	.p2align 3
.L328:
	testb	%al, %al
	je	.L190
	movq	%r12, %rcx
	jmp	.L341
	.p2align 4,,10
	.p2align 3
.L117:
	leal	-4(%rbx), %eax
	cmpl	$1, %eax
	ja	.L199
	movq	%r12, %rdi
	movq	(%r15), %r14
	call	strlen@PLT
	movq	%rax, -272(%rbp)
	cmpl	$4, %ebx
	je	.L118
	call	EVP_sha512@PLT
	movq	$64, -288(%rbp)
	movq	%rax, -280(%rbp)
	leaq	.LC5(%rip), %rax
	movq	%rax, -264(%rbp)
.L119:
	movl	$7, %edx
	leaq	rounds_prefix.24850(%rip), %rsi
	movq	%r14, %rdi
	call	strncmp@PLT
	testl	%eax, %eax
	je	.L346
	leaq	-242(%rbp), %r8
	movq	-264(%rbp), %rsi
	movl	$2, %edx
	leaq	-240(%rbp), %rbx
	movq	%r8, %rdi
	movq	%r8, -296(%rbp)
	movq	%rbx, %r15
	call	OPENSSL_strlcpy@PLT
	movl	$17, %edx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	OPENSSL_strlcpy@PLT
	movq	-296(%rbp), %r8
.L191:
	movl	(%r15), %edx
	addq	$4, %r15
	leal	-16843009(%rdx), %eax
	notl	%edx
	andl	%edx, %eax
	andl	$-2139062144, %eax
	je	.L191
	movl	%eax, %edx
	leaq	ascii_dollar(%rip), %rsi
	movq	%r8, -296(%rbp)
	movl	$3, %r14d
	shrl	$16, %edx
	testl	$32896, %eax
	movb	$0, out_buf.24851(%rip)
	cmove	%edx, %eax
	leaq	2(%r15), %rdx
	cmove	%rdx, %r15
	movl	$124, %edx
	movl	%eax, %edi
	addb	%al, %dil
	leaq	out_buf.24851(%rip), %rdi
	sbbq	$3, %r15
	call	OPENSSL_strlcat@PLT
	movq	-296(%rbp), %r8
	movl	$124, %edx
	subq	%rbx, %r15
	leaq	out_buf.24851(%rip), %rdi
	movq	%r8, %rsi
	call	OPENSSL_strlcat@PLT
	movl	$124, %edx
	leaq	ascii_dollar(%rip), %rsi
	leaq	out_buf.24851(%rip), %rdi
	call	OPENSSL_strlcat@PLT
	movl	$5000, -296(%rbp)
.L125:
	movl	$124, %edx
	movq	%rbx, %rsi
	leaq	out_buf.24851(%rip), %rdi
	call	OPENSSL_strlcat@PLT
	leaq	out_buf.24851(%rip), %rsi
	movq	%rsi, %rdx
.L127:
	movl	(%rdx), %ecx
	addq	$4, %rdx
	leal	-16843009(%rcx), %eax
	notl	%ecx
	andl	%ecx, %eax
	andl	$-2139062144, %eax
	je	.L127
	movl	%eax, %ecx
	shrl	$16, %ecx
	testl	$32896, %eax
	cmove	%ecx, %eax
	leaq	2(%rdx), %rcx
	cmove	%rcx, %rdx
	movl	%eax, %edi
	addb	%al, %dil
	sbbq	$3, %rdx
	addq	%r15, %r14
	subq	%rsi, %rdx
	cmpq	%r14, %rdx
	ja	.L129
	call	EVP_MD_CTX_new@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L129
	movq	-280(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rax, %rdi
	call	EVP_DigestInit_ex@PLT
	testl	%eax, %eax
	je	.L204
	movq	-272(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	je	.L204
	movq	%r15, %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	je	.L204
	call	EVP_MD_CTX_new@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L224
	movq	-280(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rax, %rdi
	movq	%rax, -304(%rbp)
	call	EVP_DigestInit_ex@PLT
	movq	-304(%rbp), %r8
	testl	%eax, %eax
	je	.L224
	movq	-272(%rbp), %rdx
	movq	%r8, %rdi
	movq	%r12, %rsi
	call	EVP_DigestUpdate@PLT
	movq	-304(%rbp), %r8
	testl	%eax, %eax
	je	.L224
	movq	%r8, %rdi
	movq	%r15, %rdx
	movq	%rbx, %rsi
	call	EVP_DigestUpdate@PLT
	movq	-304(%rbp), %r8
	testl	%eax, %eax
	je	.L224
	movq	-272(%rbp), %rdx
	movq	%r8, %rdi
	movq	%r12, %rsi
	call	EVP_DigestUpdate@PLT
	movq	-304(%rbp), %r8
	testl	%eax, %eax
	je	.L224
	leaq	-208(%rbp), %rax
	xorl	%edx, %edx
	movq	%r8, %rdi
	movq	%r8, -312(%rbp)
	movq	%rax, %rsi
	movq	%rax, -304(%rbp)
	call	EVP_DigestFinal_ex@PLT
	movq	-312(%rbp), %r8
	testl	%eax, %eax
	je	.L224
	movq	-288(%rbp), %rax
	movq	-272(%rbp), %r10
	cmpq	%r10, %rax
	jnb	.L131
	movq	%r15, -328(%rbp)
	movq	-304(%rbp), %r15
	movq	%r12, -320(%rbp)
	movq	%rax, %r12
	movq	%rbx, -336(%rbp)
	movq	%r10, %rbx
	jmp	.L132
	.p2align 4,,10
	.p2align 3
.L348:
	subq	%r12, %rbx
	cmpq	%rbx, %r12
	jnb	.L347
.L132:
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	jne	.L348
	movq	-312(%rbp), %r8
	movq	-320(%rbp), %r12
	xorl	%ebx, %ebx
	xorl	%r10d, %r10d
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L189:
	movl	24(%rbp), %ecx
	testl	%ecx, %ecx
	jne	.L328
.L190:
	leaq	.LC11(%rip), %rsi
	movq	%r13, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movl	$1, %eax
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L194:
	movl	$2, %r8d
	movl	$2, %ecx
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L346:
	movl	$10, %edx
	leaq	-256(%rbp), %rsi
	leaq	7(%r14), %rdi
	call	strtoul@PLT
	movq	-256(%rbp), %rcx
	xorl	%edx, %edx
	cmpb	$36, (%rcx)
	jne	.L113
	movl	$999999999, -296(%rbp)
	leaq	1(%rcx), %r14
	cmpq	$999999999, %rax
	jbe	.L349
.L122:
	leaq	-242(%rbp), %r8
	movq	-264(%rbp), %rsi
	movl	$2, %edx
	leaq	-240(%rbp), %rbx
	movq	%r8, %rdi
	movq	%r8, -304(%rbp)
	movq	%rbx, %r15
	call	OPENSSL_strlcpy@PLT
	movl	$17, %edx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	OPENSSL_strlcpy@PLT
	movq	-304(%rbp), %r8
.L123:
	movl	(%r15), %edx
	addq	$4, %r15
	leal	-16843009(%rdx), %eax
	notl	%edx
	andl	%edx, %eax
	andl	$-2139062144, %eax
	je	.L123
	movl	%eax, %edx
	leaq	ascii_dollar(%rip), %rsi
	movq	%r8, -304(%rbp)
	leaq	-144(%rbp), %r14
	shrl	$16, %edx
	testl	$32896, %eax
	movb	$0, out_buf.24851(%rip)
	cmove	%edx, %eax
	leaq	2(%r15), %rdx
	cmove	%rdx, %r15
	movl	$124, %edx
	movl	%eax, %edi
	addb	%al, %dil
	leaq	out_buf.24851(%rip), %rdi
	sbbq	$3, %r15
	call	OPENSSL_strlcat@PLT
	movq	-304(%rbp), %r8
	movl	$124, %edx
	subq	%rbx, %r15
	leaq	out_buf.24851(%rip), %rdi
	movq	%r8, %rsi
	call	OPENSSL_strlcat@PLT
	movl	$124, %edx
	leaq	ascii_dollar(%rip), %rsi
	leaq	out_buf.24851(%rip), %rdi
	call	OPENSSL_strlcat@PLT
	movq	%r14, %rdi
	movl	$80, %edx
	xorl	%eax, %eax
	movl	-296(%rbp), %r8d
	leaq	.LC9(%rip), %rcx
	movl	$1, %esi
	call	__sprintf_chk@PLT
	movq	%r14, %rsi
	movl	$124, %edx
	leaq	out_buf.24851(%rip), %rdi
	call	OPENSSL_strlcat@PLT
	movl	$124, %edx
	leaq	ascii_dollar(%rip), %rsi
	leaq	out_buf.24851(%rip), %rdi
	call	OPENSSL_strlcat@PLT
	movl	$20, %r14d
	jmp	.L125
	.p2align 4,,10
	.p2align 3
.L195:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L204:
	xorl	%ebx, %ebx
	xorl	%r10d, %r10d
	xorl	%r8d, %r8d
.L130:
	movq	%r8, %rdi
	movq	%r10, -264(%rbp)
	call	EVP_MD_CTX_free@PLT
	movq	%r14, %rdi
	call	EVP_MD_CTX_free@PLT
	movq	-264(%rbp), %r10
	movl	$768, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r10, %rdi
	call	CRYPTO_free@PLT
	movl	$769, %edx
	leaq	.LC1(%rip), %rsi
	movq	%rbx, %rdi
	call	CRYPTO_free@PLT
	movl	$770, %edx
	leaq	.LC1(%rip), %rsi
	xorl	%edi, %edi
	call	CRYPTO_free@PLT
	xorl	%edx, %edx
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L343:
	movq	(%r15), %rsi
	movq	%r12, %rdi
	call	DES_crypt@PLT
	movq	%rax, %rdx
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L342:
	movl	32(%rbp), %edx
	movq	bio_err(%rip), %rdi
	leaq	.LC7(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L118:
	call	EVP_sha256@PLT
	movq	$32, -288(%rbp)
	movq	%rax, -280(%rbp)
	leaq	.LC4(%rip), %rax
	movq	%rax, -264(%rbp)
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L106:
	leal	1(%rcx), %edi
	leaq	.LC6(%rip), %rsi
	movq	%rdx, -280(%rbp)
	movl	%r8d, -272(%rbp)
	movq	%rcx, -264(%rbp)
	call	app_malloc@PLT
	movq	-280(%rbp), %rdx
	movl	-272(%rbp), %r8d
	movq	-264(%rbp), %rcx
	movq	%rax, %rdi
	movq	%rax, (%rdx)
	movq	%rax, (%r15)
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L129:
	xorl	%ebx, %ebx
	xorl	%r10d, %r10d
	xorl	%r8d, %r8d
	xorl	%r14d, %r14d
	jmp	.L130
.L349:
	cmpq	$1000, %rax
	movl	$1000, %edx
	cmovnb	%eax, %edx
	movl	%edx, -296(%rbp)
	jmp	.L122
.L224:
	xorl	%ebx, %ebx
	xorl	%r10d, %r10d
	jmp	.L130
.L197:
	movl	$15, %ecx
	jmp	.L109
.L347:
	movq	%rbx, %r10
	movq	-304(%rbp), %rsi
	movq	%r14, %rdi
	movq	-328(%rbp), %r15
	movq	%r10, %rdx
	movq	-320(%rbp), %r12
	movq	-336(%rbp), %rbx
	call	EVP_DigestUpdate@PLT
	movq	-312(%rbp), %r8
	testl	%eax, %eax
	je	.L224
.L134:
	movq	-272(%rbp), %rcx
	movq	%rbx, %rax
	movq	%r13, -320(%rbp)
	movq	%r14, %rbx
	movq	%r8, -312(%rbp)
	movq	%rax, %r14
	movq	%rcx, %r13
	jmp	.L138
.L351:
	movq	-288(%rbp), %rdx
	movq	-304(%rbp), %rsi
.L137:
	movq	%rbx, %rdi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	je	.L214
	shrq	%r13
	je	.L350
.L138:
	testb	$1, %r13b
	jne	.L351
	movq	-272(%rbp), %rdx
	movq	%r12, %rsi
	jmp	.L137
.L350:
	movq	%r14, %rax
	movq	-312(%rbp), %r8
	movq	%rbx, %r14
	movq	-320(%rbp), %r13
	movq	%rax, %rbx
.L139:
	movq	-304(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r14, %rdi
	movq	%r8, -312(%rbp)
	call	EVP_DigestFinal_ex@PLT
	movq	-312(%rbp), %r8
	testl	%eax, %eax
	je	.L199
	movq	-280(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r8, %rdi
	movq	%r8, -312(%rbp)
	call	EVP_DigestInit_ex@PLT
	movq	-312(%rbp), %r8
	testl	%eax, %eax
	je	.L224
	movq	-272(%rbp), %rax
	testq	%rax, %rax
	je	.L142
	movq	%r13, -312(%rbp)
	movq	%r12, %r13
	movq	%r8, %r12
	movq	%rbx, -320(%rbp)
	movq	%rax, %rbx
	jmp	.L140
.L353:
	subq	$1, %rbx
	je	.L352
.L140:
	movq	-272(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	jne	.L353
	movq	%r12, %r8
	xorl	%ebx, %ebx
	movq	%r13, %r12
	xorl	%r10d, %r10d
	movq	-312(%rbp), %r13
	jmp	.L130
.L214:
	movq	%rbx, %r14
	movq	-312(%rbp), %r8
	xorl	%ebx, %ebx
	xorl	%r10d, %r10d
	movq	-320(%rbp), %r13
	jmp	.L130
.L344:
	call	__stack_chk_fail@PLT
.L352:
	movq	%r12, %r8
	movq	-320(%rbp), %rbx
	movq	%r13, %r12
	movq	-312(%rbp), %r13
.L142:
	leaq	-144(%rbp), %rax
	xorl	%edx, %edx
	movq	%r8, %rdi
	movq	%r8, -320(%rbp)
	movq	%rax, %rsi
	movq	%rax, -312(%rbp)
	call	EVP_DigestFinal_ex@PLT
	movq	-320(%rbp), %r8
	testl	%eax, %eax
	je	.L199
	movq	-272(%rbp), %rdi
	movl	$649, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r8, -320(%rbp)
	call	CRYPTO_zalloc@PLT
	movq	-320(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %r10
	je	.L340
	movq	-288(%rbp), %rcx
	movq	-272(%rbp), %rdx
	cmpq	%rdx, %rcx
	jnb	.L219
	movq	-312(%rbp), %rdi
	movl	%ecx, %r9d
	leaq	(%rdi,%rcx), %r11
.L144:
	movq	-312(%rbp), %rsi
	leaq	8(%rax), %rdi
	andq	$-8, %rdi
	movq	(%rsi), %rcx
	movq	%rcx, (%rax)
	movq	-8(%r11), %rcx
	movq	%rcx, -8(%rax,%r9)
	movq	%rax, %rcx
	subq	%rdi, %rcx
	subq	%rcx, %rsi
	addl	-288(%rbp), %ecx
	shrl	$3, %ecx
	rep movsq
	movq	-288(%rbp), %rcx
	subq	%rcx, %rdx
	addq	%rcx, %rax
	cmpq	%rdx, %rcx
	jb	.L144
.L143:
	movq	-312(%rbp), %rsi
	movl	%edx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rdi
	rep movsb
	movq	-280(%rbp), %rsi
	movq	%r8, %rdi
	movq	%r10, -328(%rbp)
	movq	%r8, -320(%rbp)
	call	EVP_DigestInit_ex@PLT
	movq	-320(%rbp), %r8
	movq	-328(%rbp), %r10
	testl	%eax, %eax
	je	.L340
	movzbl	-208(%rbp), %eax
	movq	%r12, -320(%rbp)
	movq	%rbx, %r12
	movq	%r8, %rbx
	movq	%r13, -328(%rbp)
	movq	%r10, %r13
	addq	$16, %rax
	movq	%rax, -336(%rbp)
.L145:
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	je	.L221
	subq	$1, -336(%rbp)
	jne	.L145
	movq	-312(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movq	%r13, -344(%rbp)
	movq	-320(%rbp), %r12
	movq	-328(%rbp), %r13
	movq	%rbx, -320(%rbp)
	call	EVP_DigestFinal_ex@PLT
	testl	%eax, %eax
	je	.L199
	movl	$666, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r15, %rdi
	call	CRYPTO_zalloc@PLT
	movq	-320(%rbp), %r8
	movq	-344(%rbp), %r10
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.L130
	movq	%rax, %rdi
	xorl	%eax, %eax
	movq	-312(%rbp), %rsi
	movq	%r12, -320(%rbp)
	movq	%r14, -312(%rbp)
	movl	%r15d, %ecx
	movq	%r10, %r14
	movq	%r8, %r12
	rep movsb
	movq	%r13, -328(%rbp)
	movq	%rax, %r13
	jmp	.L153
.L356:
	movq	-288(%rbp), %rdx
	movq	-304(%rbp), %rsi
.L147:
	movq	%r12, %rdi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	je	.L337
	movl	$3, %ecx
	movq	%r13, %rax
	xorl	%edx, %edx
	divq	%rcx
	testq	%rdx, %rdx
	jne	.L148
.L151:
	movl	$7, %ecx
	movq	%r13, %rax
	xorl	%edx, %edx
	divq	%rcx
	testq	%rdx, %rdx
	jne	.L354
.L150:
	cmpq	$0, -344(%rbp)
	je	.L223
	movq	-288(%rbp), %rdx
	movq	-304(%rbp), %rsi
.L152:
	movq	%r12, %rdi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	je	.L337
	movq	-304(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	EVP_DigestFinal_ex@PLT
	testl	%eax, %eax
	je	.L337
	movl	-296(%rbp), %eax
	addq	$1, %r13
	cmpq	%rax, %r13
	jnb	.L355
.L153:
	movq	-280(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	EVP_DigestInit_ex@PLT
	testl	%eax, %eax
	je	.L337
	movq	%r13, %rax
	andl	$1, %eax
	movq	%rax, -344(%rbp)
	je	.L356
	movq	-272(%rbp), %rdx
	movq	%r14, %rsi
	jmp	.L147
.L131:
	movq	-272(%rbp), %rdx
	movq	-304(%rbp), %rsi
	movq	%r14, %rdi
	movq	%r8, -312(%rbp)
	call	EVP_DigestUpdate@PLT
	movq	-312(%rbp), %r8
	testl	%eax, %eax
	je	.L224
	cmpq	$0, -272(%rbp)
	je	.L139
	jmp	.L134
.L199:
	xorl	%edx, %edx
	jmp	.L113
.L219:
	movq	-272(%rbp), %rdx
	jmp	.L143
.L221:
	movq	%r13, %r10
	movq	-320(%rbp), %r12
	movq	-328(%rbp), %r13
	movq	%rbx, %r8
.L340:
	xorl	%ebx, %ebx
	jmp	.L130
.L223:
	movq	-272(%rbp), %rdx
	movq	%r14, %rsi
	jmp	.L152
.L355:
	movq	%r12, %r8
	movq	%r14, -272(%rbp)
	movq	-312(%rbp), %r14
	movq	%r8, %rdi
	movq	-320(%rbp), %r12
	movq	-328(%rbp), %r13
	call	EVP_MD_CTX_free@PLT
	movq	%r14, %rdi
	call	EVP_MD_CTX_free@PLT
	movq	-272(%rbp), %r10
	movl	$698, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r10, %rdi
	call	CRYPTO_free@PLT
	movq	%rbx, %rdi
	movl	$699, %edx
	leaq	.LC1(%rip), %rsi
	call	CRYPTO_free@PLT
	xorl	%eax, %eax
	orq	$-1, %rcx
	leaq	out_buf.24851(%rip), %rdi
	repnz scasb
	leaq	out_buf.24851(%rip), %rbx
	movq	%rcx, %rax
	notq	%rax
	leaq	-1(%rbx,%rax), %rdx
	movq	-264(%rbp), %rax
	movb	$36, (%rdx)
	movzbl	(%rax), %eax
	cmpb	$53, %al
	je	.L154
	cmpb	$54, %al
	jne	.L129
	movzbl	-208(%rbp), %ecx
	movzbl	-187(%rbp), %eax
	xorl	%esi, %esi
	sall	$8, %eax
	sall	$16, %ecx
	orl	%eax, %ecx
	movzbl	-166(%rbp), %eax
	orl	%eax, %ecx
	leaq	cov_2char(%rip), %rax
.L168:
	movl	%ecx, %edi
	shrl	$6, %ecx
	andl	$63, %edi
	movzbl	(%rax,%rdi), %edi
	movb	%dil, 1(%rdx,%rsi)
	addq	$1, %rsi
	cmpq	$4, %rsi
	jne	.L168
	movzbl	-186(%rbp), %ecx
	movzbl	-165(%rbp), %esi
	sall	$16, %ecx
	sall	$8, %esi
	orl	%esi, %ecx
	movzbl	-207(%rbp), %esi
	orl	%esi, %ecx
	xorl	%esi, %esi
.L169:
	movl	%ecx, %edi
	shrl	$6, %ecx
	andl	$63, %edi
	movzbl	(%rax,%rdi), %edi
	movb	%dil, 5(%rdx,%rsi)
	addq	$1, %rsi
	cmpq	$4, %rsi
	jne	.L169
	movzbl	-164(%rbp), %ecx
	movzbl	-206(%rbp), %esi
	sall	$16, %ecx
	sall	$8, %esi
	orl	%esi, %ecx
	movzbl	-185(%rbp), %esi
	orl	%esi, %ecx
	xorl	%esi, %esi
.L170:
	movl	%ecx, %edi
	shrl	$6, %ecx
	andl	$63, %edi
	movzbl	(%rax,%rdi), %edi
	movb	%dil, 9(%rdx,%rsi)
	addq	$1, %rsi
	cmpq	$4, %rsi
	jne	.L170
	movzbl	-205(%rbp), %ecx
	movzbl	-184(%rbp), %esi
	sall	$16, %ecx
	sall	$8, %esi
	orl	%esi, %ecx
	movzbl	-163(%rbp), %esi
	orl	%esi, %ecx
	xorl	%esi, %esi
.L171:
	movl	%ecx, %edi
	shrl	$6, %ecx
	andl	$63, %edi
	movzbl	(%rax,%rdi), %edi
	movb	%dil, 13(%rdx,%rsi)
	addq	$1, %rsi
	cmpq	$4, %rsi
	jne	.L171
	movzbl	-183(%rbp), %ecx
	movzbl	-162(%rbp), %esi
	sall	$16, %ecx
	sall	$8, %esi
	orl	%esi, %ecx
	movzbl	-204(%rbp), %esi
	orl	%esi, %ecx
	xorl	%esi, %esi
.L172:
	movl	%ecx, %edi
	shrl	$6, %ecx
	andl	$63, %edi
	movzbl	(%rax,%rdi), %edi
	movb	%dil, 17(%rdx,%rsi)
	addq	$1, %rsi
	cmpq	$4, %rsi
	jne	.L172
	movzbl	-161(%rbp), %ecx
	movzbl	-203(%rbp), %esi
	sall	$16, %ecx
	sall	$8, %esi
	orl	%esi, %ecx
	movzbl	-182(%rbp), %esi
	orl	%esi, %ecx
	xorl	%esi, %esi
.L173:
	movl	%ecx, %edi
	shrl	$6, %ecx
	andl	$63, %edi
	movzbl	(%rax,%rdi), %edi
	movb	%dil, 21(%rdx,%rsi)
	addq	$1, %rsi
	cmpq	$4, %rsi
	jne	.L173
	movzbl	-202(%rbp), %ecx
	movzbl	-181(%rbp), %esi
	sall	$16, %ecx
	sall	$8, %esi
	orl	%esi, %ecx
	movzbl	-160(%rbp), %esi
	orl	%esi, %ecx
	xorl	%esi, %esi
.L174:
	movl	%ecx, %edi
	shrl	$6, %ecx
	andl	$63, %edi
	movzbl	(%rax,%rdi), %edi
	movb	%dil, 25(%rdx,%rsi)
	addq	$1, %rsi
	cmpq	$4, %rsi
	jne	.L174
	movzbl	-180(%rbp), %ecx
	movzbl	-159(%rbp), %esi
	sall	$16, %ecx
	sall	$8, %esi
	orl	%esi, %ecx
	movzbl	-201(%rbp), %esi
	orl	%esi, %ecx
	xorl	%esi, %esi
.L175:
	movl	%ecx, %edi
	shrl	$6, %ecx
	andl	$63, %edi
	movzbl	(%rax,%rdi), %edi
	movb	%dil, 29(%rdx,%rsi)
	addq	$1, %rsi
	cmpq	$4, %rsi
	jne	.L175
	movzbl	-158(%rbp), %ecx
	movzbl	-200(%rbp), %esi
	sall	$16, %ecx
	sall	$8, %esi
	orl	%esi, %ecx
	movzbl	-179(%rbp), %esi
	orl	%esi, %ecx
	xorl	%esi, %esi
.L176:
	movl	%ecx, %edi
	shrl	$6, %ecx
	andl	$63, %edi
	movzbl	(%rax,%rdi), %edi
	movb	%dil, 33(%rdx,%rsi)
	addq	$1, %rsi
	cmpq	$4, %rsi
	jne	.L176
	movzbl	-199(%rbp), %ecx
	movzbl	-178(%rbp), %esi
	sall	$16, %ecx
	sall	$8, %esi
	orl	%esi, %ecx
	movzbl	-157(%rbp), %esi
	orl	%esi, %ecx
	xorl	%esi, %esi
.L177:
	movl	%ecx, %edi
	shrl	$6, %ecx
	andl	$63, %edi
	movzbl	(%rax,%rdi), %edi
	movb	%dil, 37(%rdx,%rsi)
	addq	$1, %rsi
	cmpq	$4, %rsi
	jne	.L177
	movzbl	-177(%rbp), %ecx
	movzbl	-156(%rbp), %esi
	sall	$16, %ecx
	sall	$8, %esi
	orl	%esi, %ecx
	movzbl	-198(%rbp), %esi
	orl	%esi, %ecx
	xorl	%esi, %esi
.L178:
	movl	%ecx, %edi
	shrl	$6, %ecx
	andl	$63, %edi
	movzbl	(%rax,%rdi), %edi
	movb	%dil, 41(%rdx,%rsi)
	addq	$1, %rsi
	cmpq	$4, %rsi
	jne	.L178
	movzbl	-155(%rbp), %ecx
	movzbl	-197(%rbp), %esi
	sall	$16, %ecx
	sall	$8, %esi
	orl	%esi, %ecx
	movzbl	-176(%rbp), %esi
	orl	%esi, %ecx
	xorl	%esi, %esi
.L179:
	movl	%ecx, %edi
	shrl	$6, %ecx
	andl	$63, %edi
	movzbl	(%rax,%rdi), %edi
	movb	%dil, 45(%rdx,%rsi)
	addq	$1, %rsi
	cmpq	$4, %rsi
	jne	.L179
	movzbl	-196(%rbp), %ecx
	movzbl	-175(%rbp), %esi
	sall	$16, %ecx
	sall	$8, %esi
	orl	%esi, %ecx
	movzbl	-154(%rbp), %esi
	orl	%esi, %ecx
	xorl	%esi, %esi
.L180:
	movl	%ecx, %edi
	shrl	$6, %ecx
	andl	$63, %edi
	movzbl	(%rax,%rdi), %edi
	movb	%dil, 49(%rdx,%rsi)
	addq	$1, %rsi
	cmpq	$4, %rsi
	jne	.L180
	movzbl	-174(%rbp), %ecx
	movzbl	-153(%rbp), %esi
	sall	$16, %ecx
	sall	$8, %esi
	orl	%esi, %ecx
	movzbl	-195(%rbp), %esi
	orl	%esi, %ecx
	xorl	%esi, %esi
.L181:
	movl	%ecx, %edi
	shrl	$6, %ecx
	andl	$63, %edi
	movzbl	(%rax,%rdi), %edi
	movb	%dil, 53(%rdx,%rsi)
	addq	$1, %rsi
	cmpq	$4, %rsi
	jne	.L181
	movzbl	-152(%rbp), %ecx
	movzbl	-194(%rbp), %esi
	sall	$16, %ecx
	sall	$8, %esi
	orl	%esi, %ecx
	movzbl	-173(%rbp), %esi
	orl	%esi, %ecx
	xorl	%esi, %esi
.L182:
	movl	%ecx, %edi
	shrl	$6, %ecx
	andl	$63, %edi
	movzbl	(%rax,%rdi), %edi
	movb	%dil, 57(%rdx,%rsi)
	addq	$1, %rsi
	cmpq	$4, %rsi
	jne	.L182
	movzbl	-193(%rbp), %ecx
	movzbl	-172(%rbp), %esi
	sall	$16, %ecx
	sall	$8, %esi
	orl	%esi, %ecx
	movzbl	-151(%rbp), %esi
	orl	%esi, %ecx
	xorl	%esi, %esi
.L183:
	movl	%ecx, %edi
	shrl	$6, %ecx
	andl	$63, %edi
	movzbl	(%rax,%rdi), %edi
	movb	%dil, 61(%rdx,%rsi)
	addq	$1, %rsi
	cmpq	$4, %rsi
	jne	.L183
	movzbl	-171(%rbp), %ecx
	movzbl	-150(%rbp), %esi
	sall	$16, %ecx
	sall	$8, %esi
	orl	%esi, %ecx
	movzbl	-192(%rbp), %esi
	orl	%esi, %ecx
	xorl	%esi, %esi
.L184:
	movl	%ecx, %edi
	shrl	$6, %ecx
	andl	$63, %edi
	movzbl	(%rax,%rdi), %edi
	movb	%dil, 65(%rdx,%rsi)
	addq	$1, %rsi
	cmpq	$4, %rsi
	jne	.L184
	movzbl	-149(%rbp), %ecx
	movzbl	-191(%rbp), %esi
	sall	$16, %ecx
	sall	$8, %esi
	orl	%esi, %ecx
	movzbl	-170(%rbp), %esi
	orl	%esi, %ecx
	xorl	%esi, %esi
.L185:
	movl	%ecx, %edi
	shrl	$6, %ecx
	andl	$63, %edi
	movzbl	(%rax,%rdi), %edi
	movb	%dil, 69(%rdx,%rsi)
	addq	$1, %rsi
	cmpq	$4, %rsi
	jne	.L185
	movzbl	-190(%rbp), %ecx
	movzbl	-169(%rbp), %esi
	sall	$16, %ecx
	sall	$8, %esi
	orl	%esi, %ecx
	movzbl	-148(%rbp), %esi
	orl	%esi, %ecx
	xorl	%esi, %esi
.L186:
	movl	%ecx, %edi
	shrl	$6, %ecx
	andl	$63, %edi
	movzbl	(%rax,%rdi), %edi
	movb	%dil, 73(%rdx,%rsi)
	addq	$1, %rsi
	cmpq	$4, %rsi
	jne	.L186
	movzbl	-168(%rbp), %ecx
	movzbl	-147(%rbp), %esi
	sall	$16, %ecx
	sall	$8, %esi
	orl	%esi, %ecx
	movzbl	-189(%rbp), %esi
	orl	%esi, %ecx
	xorl	%esi, %esi
.L187:
	movl	%ecx, %edi
	shrl	$6, %ecx
	andl	$63, %edi
	movzbl	(%rax,%rdi), %edi
	movb	%dil, 77(%rdx,%rsi)
	addq	$1, %rsi
	cmpq	$4, %rsi
	jne	.L187
	movzbl	-146(%rbp), %ecx
	movzbl	-188(%rbp), %esi
	sall	$16, %ecx
	sall	$8, %esi
	orl	%esi, %ecx
	movzbl	-167(%rbp), %esi
	orl	%esi, %ecx
	movq	-336(%rbp), %rsi
.L188:
	movl	%ecx, %edi
	shrl	$6, %ecx
	andl	$63, %edi
	movzbl	(%rax,%rdi), %edi
	movb	%dil, 81(%rdx,%rsi)
	addq	$1, %rsi
	cmpq	$4, %rsi
	jne	.L188
	movzbl	-145(%rbp), %ecx
	movq	%rcx, %rsi
	shrq	$6, %rcx
	andl	$63, %esi
	andl	$3, %ecx
	movzbl	(%rax,%rsi), %esi
	movzbl	(%rax,%rcx), %eax
	movb	%sil, 85(%rdx)
	leaq	87(%rdx), %rsi
	movb	%al, 86(%rdx)
	jmp	.L167
.L148:
	movq	%r15, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	jne	.L151
.L337:
	movq	%r12, %r8
	movq	%r14, %r10
	movq	-320(%rbp), %r12
	movq	-312(%rbp), %r14
	movq	-328(%rbp), %r13
	jmp	.L130
.L354:
	movq	-272(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	jne	.L150
	jmp	.L337
.L154:
	movzbl	-208(%rbp), %ecx
	movzbl	-198(%rbp), %eax
	xorl	%esi, %esi
	sall	$8, %eax
	sall	$16, %ecx
	orl	%eax, %ecx
	movzbl	-188(%rbp), %eax
	orl	%eax, %ecx
	leaq	cov_2char(%rip), %rax
.L156:
	movl	%ecx, %edi
	shrl	$6, %ecx
	andl	$63, %edi
	movzbl	(%rax,%rdi), %edi
	movb	%dil, 1(%rdx,%rsi)
	addq	$1, %rsi
	cmpq	$4, %rsi
	jne	.L156
	movzbl	-187(%rbp), %ecx
	movzbl	-207(%rbp), %esi
	sall	$16, %ecx
	sall	$8, %esi
	orl	%esi, %ecx
	movzbl	-197(%rbp), %esi
	orl	%esi, %ecx
	xorl	%esi, %esi
.L157:
	movl	%ecx, %edi
	shrl	$6, %ecx
	andl	$63, %edi
	movzbl	(%rax,%rdi), %edi
	movb	%dil, 5(%rdx,%rsi)
	addq	$1, %rsi
	cmpq	$4, %rsi
	jne	.L157
	movzbl	-196(%rbp), %ecx
	movzbl	-186(%rbp), %esi
	sall	$16, %ecx
	sall	$8, %esi
	orl	%esi, %ecx
	movzbl	-206(%rbp), %esi
	orl	%esi, %ecx
	xorl	%esi, %esi
.L158:
	movl	%ecx, %edi
	shrl	$6, %ecx
	andl	$63, %edi
	movzbl	(%rax,%rdi), %edi
	movb	%dil, 9(%rdx,%rsi)
	addq	$1, %rsi
	cmpq	$4, %rsi
	jne	.L158
	movzbl	-205(%rbp), %ecx
	movzbl	-195(%rbp), %esi
	sall	$16, %ecx
	sall	$8, %esi
	orl	%esi, %ecx
	movzbl	-185(%rbp), %esi
	orl	%esi, %ecx
	xorl	%esi, %esi
.L159:
	movl	%ecx, %edi
	shrl	$6, %ecx
	andl	$63, %edi
	movzbl	(%rax,%rdi), %edi
	movb	%dil, 13(%rdx,%rsi)
	addq	$1, %rsi
	cmpq	$4, %rsi
	jne	.L159
	movzbl	-184(%rbp), %ecx
	movzbl	-204(%rbp), %esi
	sall	$16, %ecx
	sall	$8, %esi
	orl	%esi, %ecx
	movzbl	-194(%rbp), %esi
	orl	%esi, %ecx
	xorl	%esi, %esi
.L160:
	movl	%ecx, %edi
	shrl	$6, %ecx
	andl	$63, %edi
	movzbl	(%rax,%rdi), %edi
	movb	%dil, 17(%rdx,%rsi)
	addq	$1, %rsi
	cmpq	$4, %rsi
	jne	.L160
	movzbl	-193(%rbp), %ecx
	movzbl	-183(%rbp), %esi
	sall	$16, %ecx
	sall	$8, %esi
	orl	%esi, %ecx
	movzbl	-203(%rbp), %esi
	orl	%esi, %ecx
	xorl	%esi, %esi
.L161:
	movl	%ecx, %edi
	shrl	$6, %ecx
	andl	$63, %edi
	movzbl	(%rax,%rdi), %edi
	movb	%dil, 21(%rdx,%rsi)
	addq	$1, %rsi
	cmpq	$4, %rsi
	jne	.L161
	movzbl	-202(%rbp), %ecx
	movzbl	-192(%rbp), %esi
	sall	$16, %ecx
	sall	$8, %esi
	orl	%esi, %ecx
	movzbl	-182(%rbp), %esi
	orl	%esi, %ecx
	xorl	%esi, %esi
.L162:
	movl	%ecx, %edi
	shrl	$6, %ecx
	andl	$63, %edi
	movzbl	(%rax,%rdi), %edi
	movb	%dil, 25(%rdx,%rsi)
	addq	$1, %rsi
	cmpq	$4, %rsi
	jne	.L162
	movzbl	-181(%rbp), %ecx
	movzbl	-201(%rbp), %esi
	sall	$16, %ecx
	sall	$8, %esi
	orl	%esi, %ecx
	movzbl	-191(%rbp), %esi
	orl	%esi, %ecx
	xorl	%esi, %esi
.L163:
	movl	%ecx, %edi
	shrl	$6, %ecx
	andl	$63, %edi
	movzbl	(%rax,%rdi), %edi
	movb	%dil, 29(%rdx,%rsi)
	addq	$1, %rsi
	cmpq	$4, %rsi
	jne	.L163
	movzbl	-190(%rbp), %ecx
	movzbl	-180(%rbp), %esi
	sall	$16, %ecx
	sall	$8, %esi
	orl	%esi, %ecx
	movzbl	-200(%rbp), %esi
	orl	%esi, %ecx
	xorl	%esi, %esi
.L164:
	movl	%ecx, %edi
	shrl	$6, %ecx
	andl	$63, %edi
	movzbl	(%rax,%rdi), %edi
	movb	%dil, 33(%rdx,%rsi)
	addq	$1, %rsi
	cmpq	$4, %rsi
	jne	.L164
	movzbl	-199(%rbp), %ecx
	movzbl	-189(%rbp), %esi
	sall	$16, %ecx
	sall	$8, %esi
	orl	%esi, %ecx
	movzbl	-179(%rbp), %esi
	orl	%esi, %ecx
	xorl	%esi, %esi
.L165:
	movl	%ecx, %edi
	shrl	$6, %ecx
	andl	$63, %edi
	movzbl	(%rax,%rdi), %edi
	movb	%dil, 37(%rdx,%rsi)
	addq	$1, %rsi
	cmpq	$4, %rsi
	jne	.L165
	movzwl	-178(%rbp), %ecx
	movq	-336(%rbp), %rsi
.L166:
	movl	%ecx, %edi
	shrl	$6, %ecx
	andl	$63, %edi
	movzbl	(%rax,%rdi), %edi
	movb	%dil, 41(%rdx,%rsi)
	addq	$1, %rsi
	cmpq	$3, %rsi
	jne	.L166
	leaq	44(%rdx), %rsi
.L167:
	movb	$0, (%rsi)
	leaq	out_buf.24851(%rip), %rdx
	jmp	.L113
	.cfi_endproc
.LFE1438:
	.size	do_passwd, .-do_passwd
	.section	.rodata.str1.1
.LC12:
	.string	"%s: Use -help for summary.\n"
	.section	.rodata.str1.8
	.align 8
.LC13:
	.string	"%s: Can't combine -in and -stdin\n"
	.section	.rodata.str1.1
.LC14:
	.string	"password buffer"
.LC15:
	.string	"Password: "
	.text
	.p2align 4
	.globl	passwd_main
	.type	passwd_main, @function
passwd_main:
.LFB1435:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	passwd_options(%rip), %rdx
	xorl	%r13d, %r13d
	xorl	%r14d, %r14d
	xorl	%r12d, %r12d
	leaq	.L363(%rip), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -8272(%rbp)
	movq	$0, -8264(%rbp)
	call	opt_init@PLT
	movl	$0, -8284(%rbp)
	movq	%rax, -8312(%rbp)
	movl	$0, -8288(%rbp)
	movl	$0, -8292(%rbp)
	movl	$0, -8280(%rbp)
	movl	$0, -8296(%rbp)
	movq	$0, -8304(%rbp)
.L358:
	call	opt_next@PLT
	testl	%eax, %eax
	je	.L428
.L381:
	cmpl	$14, %eax
	jg	.L359
	cmpl	$-1, %eax
	jl	.L358
	addl	$1, %eax
	cmpl	$15, %eax
	ja	.L358
	movslq	(%rbx,%rax,4), %rax
	addq	%rbx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L363:
	.long	.L376-.L363
	.long	.L358-.L363
	.long	.L375-.L363
	.long	.L374-.L363
	.long	.L373-.L363
	.long	.L398-.L363
	.long	.L372-.L363
	.long	.L371-.L363
	.long	.L370-.L363
	.long	.L369-.L363
	.long	.L368-.L363
	.long	.L367-.L363
	.long	.L366-.L363
	.long	.L365-.L363
	.long	.L364-.L363
	.long	.L362-.L363
	.text
.L398:
	movl	$1, %r14d
	call	opt_next@PLT
	testl	%eax, %eax
	jne	.L381
.L428:
	movl	%eax, %r15d
	call	opt_num_rest@PLT
	call	opt_rest@PLT
	cmpq	$0, (%rax)
	movq	%rax, %r11
	je	.L399
	movl	-8280(%rbp), %eax
	testl	%eax, %eax
	jne	.L376
.L382:
	cmpq	$0, -8304(%rbp)
	movzbl	-8296(%rbp), %eax
	setne	%dl
	andl	$1, %eax
	testb	%dl, %dl
	je	.L408
	testb	%al, %al
	jne	.L429
.L407:
	movq	-8304(%rbp), %rdi
	movl	$32769, %edx
	movl	$114, %esi
	movq	%r11, -8312(%rbp)
	call	bio_open_default@PLT
	movq	-8312(%rbp), %r11
	testq	%rax, %rax
	movq	%rax, -8280(%rbp)
	jne	.L384
	.p2align 4,,10
	.p2align 3
.L426:
	movq	$0, -8280(%rbp)
	movl	$1, %r15d
	xorl	%r13d, %r13d
.L378:
	movq	-8264(%rbp), %rdi
	movl	$296, %edx
	leaq	.LC1(%rip), %rsi
	call	CRYPTO_free@PLT
	movl	$297, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r13, %rdi
	call	CRYPTO_free@PLT
	movq	-8280(%rbp), %rdi
	call	BIO_free@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L430
	leaq	-40(%rbp), %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L373:
	.cfi_restore_state
	movl	$1, %r12d
	jmp	.L358
	.p2align 4,,10
	.p2align 3
.L359:
	leal	-1501(%rax), %edx
	cmpl	$1, %edx
	ja	.L358
	movl	%eax, %edi
	call	opt_rand@PLT
	testl	%eax, %eax
	jne	.L358
	jmp	.L426
.L376:
	movq	-8312(%rbp), %rdx
	leaq	.LC12(%rip), %rsi
.L425:
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L426
.L362:
	movl	-8280(%rbp), %edx
	testl	%edx, %edx
	jne	.L376
	movl	$1, -8280(%rbp)
	movl	$1, -8296(%rbp)
	jmp	.L358
.L364:
	call	opt_arg@PLT
	movl	$1, -8292(%rbp)
	movq	%rax, -8272(%rbp)
	jmp	.L358
.L365:
	testl	%r13d, %r13d
	jne	.L376
	movl	$6, %r13d
	jmp	.L358
.L366:
	testl	%r13d, %r13d
	jne	.L376
	movl	$1, %r13d
	jmp	.L358
.L367:
	testl	%r13d, %r13d
	jne	.L376
	movl	$5, %r13d
	jmp	.L358
.L368:
	testl	%r13d, %r13d
	jne	.L376
	movl	$4, %r13d
	jmp	.L358
.L369:
	testl	%r13d, %r13d
	jne	.L376
	movl	$2, %r13d
	jmp	.L358
.L370:
	testl	%r13d, %r13d
	jne	.L376
	movl	$3, %r13d
	jmp	.L358
.L371:
	movl	$1, -8284(%rbp)
	jmp	.L358
.L372:
	movl	$1, -8288(%rbp)
	jmp	.L358
.L374:
	movl	-8280(%rbp), %ecx
	testl	%ecx, %ecx
	jne	.L376
	call	opt_arg@PLT
	movl	$1, -8280(%rbp)
	movq	%rax, -8304(%rbp)
	jmp	.L358
.L375:
	leaq	passwd_options(%rip), %rdi
	xorl	%r15d, %r15d
	xorl	%r13d, %r13d
	call	opt_help@PLT
	movq	$0, -8280(%rbp)
	jmp	.L378
.L408:
	movq	$0, -8280(%rbp)
	testb	%al, %al
	jne	.L407
.L384:
	cmpl	$2, %r13d
	sbbl	%eax, %eax
	andb	$8, %al
	addl	$257, %eax
	cmpl	$2, %r13d
	sbbl	%ebx, %ebx
	movl	%eax, -8304(%rbp)
	andb	$8, %bl
	addl	$258, %ebx
	cmpl	$2, %r13d
	sbbq	%rax, %rax
	andb	$8, %al
	addq	$256, %rax
	testl	%r13d, %r13d
	movq	%rax, -8312(%rbp)
	movl	$1, %eax
	cmovne	%r13d, %eax
	movl	%eax, -8296(%rbp)
	testq	%r11, %r11
	je	.L431
	xorl	%r13d, %r13d
	cmpq	$0, -8280(%rbp)
	je	.L390
.L389:
	movl	%r15d, -8316(%rbp)
	movq	-8280(%rbp), %r15
	leaq	-8264(%rbp), %r12
	.p2align 4,,10
	.p2align 3
.L392:
	movl	-8304(%rbp), %edx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	BIO_gets@PLT
	testl	%eax, %eax
	jle	.L423
	movl	$10, %esi
	movq	%r13, %rdi
	leaq	-8256(%rbp), %rbx
	call	strchr@PLT
	testq	%rax, %rax
	je	.L393
	movb	$0, (%rax)
.L421:
	movl	-8296(%rbp), %eax
	movl	%r14d, %r9d
	movq	%r13, %rcx
	movq	%r12, %rdx
	movq	bio_out(%rip), %r8
	movl	-8292(%rbp), %edi
	leaq	-8272(%rbp), %rsi
	pushq	%rax
	movl	-8284(%rbp), %eax
	pushq	-8312(%rbp)
	pushq	%rax
	movl	-8288(%rbp), %eax
	pushq	%rax
	call	do_passwd
	addq	$32, %rsp
	testl	%eax, %eax
	jne	.L392
.L406:
	movl	$1, %r15d
	jmp	.L378
	.p2align 4,,10
	.p2align 3
.L432:
	movl	$10, %esi
	movq	%rbx, %rdi
	call	strchr@PLT
	testq	%rax, %rax
	jne	.L421
.L393:
	movl	$8192, %edx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	BIO_gets@PLT
	testl	%eax, %eax
	jg	.L432
	movl	-8296(%rbp), %eax
	movl	-8292(%rbp), %edi
	leaq	-8264(%rbp), %rdx
	leaq	-8272(%rbp), %rsi
	movq	bio_out(%rip), %r8
	movl	%r14d, %r9d
	movq	%r13, %rcx
	xorl	%r15d, %r15d
	pushq	%rax
	movl	-8284(%rbp), %eax
	pushq	-8312(%rbp)
	pushq	%rax
	movl	-8288(%rbp), %eax
	pushq	%rax
	call	do_passwd
	addq	$32, %rsp
	testl	%eax, %eax
	sete	%r15b
	jmp	.L378
.L429:
	movq	-8312(%rbp), %rdx
	leaq	.LC13(%rip), %rsi
	jmp	.L425
.L431:
	leaq	.LC14(%rip), %rsi
	movl	%ebx, %edi
	call	app_malloc@PLT
	cmpq	$0, -8280(%rbp)
	movq	%rax, %r13
	jne	.L389
	movl	-8292(%rbp), %ecx
	leaq	.LC15(%rip), %rdx
	movl	%ebx, %esi
	movq	%rax, %rdi
	orl	%r12d, %ecx
	xorl	$1, %ecx
	call	EVP_read_pw_string@PLT
	testl	%eax, %eax
	jne	.L406
	movq	%r13, passwds_static.24796(%rip)
	leaq	passwds_static.24796(%rip), %r11
.L390:
	leaq	-8264(%rbp), %rbx
	movq	%r13, -8304(%rbp)
	leaq	-8272(%rbp), %r12
	movl	-8292(%rbp), %r13d
	movq	%rbx, %rax
	movl	%r15d, -8292(%rbp)
	movl	%r14d, %ebx
	movq	%r11, %r15
	movq	%rax, %r14
	jmp	.L391
	.p2align 4,,10
	.p2align 3
.L434:
	movq	(%r15), %rax
	testq	%rax, %rax
	je	.L433
.L391:
	movl	-8296(%rbp), %eax
	movq	(%r15), %rcx
	movl	%ebx, %r9d
	movq	%r14, %rdx
	movq	bio_out(%rip), %r8
	movq	%r12, %rsi
	movl	%r13d, %edi
	addq	$8, %r15
	pushq	%rax
	movl	-8284(%rbp), %eax
	pushq	-8312(%rbp)
	pushq	%rax
	movl	-8288(%rbp), %eax
	pushq	%rax
	call	do_passwd
	addq	$32, %rsp
	testl	%eax, %eax
	jne	.L434
	movq	-8304(%rbp), %r13
	movl	$1, %r15d
	movq	$0, -8280(%rbp)
	jmp	.L378
	.p2align 4,,10
	.p2align 3
.L423:
	movl	-8316(%rbp), %r15d
	jmp	.L378
.L399:
	xorl	%r11d, %r11d
	jmp	.L382
.L433:
	movq	%rax, -8280(%rbp)
	movq	-8304(%rbp), %r13
	movl	-8292(%rbp), %r15d
	jmp	.L378
.L430:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1435:
	.size	passwd_main, .-passwd_main
	.local	out_buf.24851
	.comm	out_buf.24851,124,32
	.section	.rodata
	.align 8
	.type	rounds_prefix.24850, @object
	.size	rounds_prefix.24850, 8
rounds_prefix.24850:
	.string	"rounds="
	.local	out_buf.24812
	.comm	out_buf.24812,41,32
	.local	passwds_static.24796
	.comm	passwds_static.24796,16,16
	.globl	passwd_options
	.section	.rodata.str1.1
.LC16:
	.string	"help"
.LC17:
	.string	"Display this summary"
.LC18:
	.string	"in"
.LC19:
	.string	"Read passwords from file"
.LC20:
	.string	"noverify"
	.section	.rodata.str1.8
	.align 8
.LC21:
	.string	"Never verify when reading password from terminal"
	.section	.rodata.str1.1
.LC22:
	.string	"quiet"
.LC23:
	.string	"No warnings"
.LC24:
	.string	"table"
.LC25:
	.string	"Format output as table"
.LC26:
	.string	"reverse"
.LC27:
	.string	"Switch table columns"
.LC28:
	.string	"salt"
.LC29:
	.string	"Use provided salt"
.LC30:
	.string	"stdin"
.LC31:
	.string	"Read passwords from stdin"
	.section	.rodata.str1.8
	.align 8
.LC32:
	.string	"SHA512-based password algorithm"
	.align 8
.LC33:
	.string	"SHA256-based password algorithm"
	.align 8
.LC34:
	.string	"MD5-based password algorithm, Apache variant"
	.section	.rodata.str1.1
.LC35:
	.string	"MD5-based password algorithm"
.LC36:
	.string	"aixmd5"
	.section	.rodata.str1.8
	.align 8
.LC37:
	.string	"AIX MD5-based password algorithm"
	.section	.rodata.str1.1
.LC38:
	.string	"crypt"
	.section	.rodata.str1.8
	.align 8
.LC39:
	.string	"Standard Unix password algorithm (default)"
	.section	.rodata.str1.1
.LC40:
	.string	"rand"
	.section	.rodata.str1.8
	.align 8
.LC41:
	.string	"Load the file(s) into the random number generator"
	.section	.rodata.str1.1
.LC42:
	.string	"writerand"
	.section	.rodata.str1.8
	.align 8
.LC43:
	.string	"Write random data to the specified file"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	passwd_options, @object
	.size	passwd_options, 408
passwd_options:
	.quad	.LC16
	.long	1
	.long	45
	.quad	.LC17
	.quad	.LC18
	.long	2
	.long	60
	.quad	.LC19
	.quad	.LC20
	.long	3
	.long	45
	.quad	.LC21
	.quad	.LC22
	.long	4
	.long	45
	.quad	.LC23
	.quad	.LC24
	.long	5
	.long	45
	.quad	.LC25
	.quad	.LC26
	.long	6
	.long	45
	.quad	.LC27
	.quad	.LC28
	.long	13
	.long	115
	.quad	.LC29
	.quad	.LC30
	.long	14
	.long	45
	.quad	.LC31
	.quad	.LC5
	.long	10
	.long	45
	.quad	.LC32
	.quad	.LC4
	.long	9
	.long	45
	.quad	.LC33
	.quad	.LC2
	.long	7
	.long	45
	.quad	.LC34
	.quad	.LC3
	.long	8
	.long	45
	.quad	.LC35
	.quad	.LC36
	.long	12
	.long	45
	.quad	.LC37
	.quad	.LC38
	.long	11
	.long	45
	.quad	.LC39
	.quad	.LC40
	.long	1501
	.long	115
	.quad	.LC41
	.quad	.LC42
	.long	1502
	.long	62
	.quad	.LC43
	.quad	0
	.zero	16
	.section	.rodata
	.type	ascii_dollar, @object
	.size	ascii_dollar, 2
ascii_dollar:
	.string	"$"
	.align 32
	.type	cov_2char, @object
	.size	cov_2char, 64
cov_2char:
	.ascii	"./0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuv"
	.ascii	"wxyz"
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
