	.file	"pkeyutl.c"
	.text
	.p2align 4
	.type	do_keyop, @function
do_keyop:
.LFB1438:
	.cfi_startproc
	movq	%rdx, %r10
	movq	%rcx, %rdx
	movq	%r8, %rcx
	movq	%r9, %r8
	cmpl	$256, %esi
	je	.L2
	jle	.L10
	cmpl	$512, %esi
	je	.L7
	cmpl	$1024, %esi
	jne	.L1
	movq	%r10, %rsi
	jmp	EVP_PKEY_derive@PLT
	.p2align 4,,10
	.p2align 3
.L10:
	cmpl	$8, %esi
	je	.L4
	cmpl	$32, %esi
	jne	.L1
	movq	%r10, %rsi
	jmp	EVP_PKEY_verify_recover@PLT
	.p2align 4,,10
	.p2align 3
.L2:
	movq	%r10, %rsi
	jmp	EVP_PKEY_encrypt@PLT
	.p2align 4,,10
	.p2align 3
.L7:
	movq	%r10, %rsi
	jmp	EVP_PKEY_decrypt@PLT
.L1:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	movq	%r10, %rsi
	jmp	EVP_PKEY_sign@PLT
	.cfi_endproc
.LFE1438:
	.size	do_keyop, .-do_keyop
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"%s: Use -help for summary.\n"
.LC1:
	.string	"out of memory\n"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"%s: no KDF length given (-kdflen parameter).\n"
	.align 8
.LC3:
	.string	"%s: no private key given (-inkey parameter).\n"
	.align 8
.LC4:
	.string	"%s: no peer key given (-peerkey parameter).\n"
	.align 8
.LC5:
	.string	"A private key is needed for this operation\n"
	.align 8
.LC6:
	.string	"../deps/openssl/openssl/apps/pkeyutl.c"
	.section	.rodata.str1.1
.LC7:
	.string	"Error getting password\n"
.LC8:
	.string	"Private Key"
.LC9:
	.string	"Public Key"
.LC10:
	.string	"Certificate"
	.section	.rodata.str1.8
	.align 8
.LC11:
	.string	"The given KDF \"%s\" is unknown.\n"
	.align 8
.LC12:
	.string	"%s: Error initializing context\n"
	.section	.rodata.str1.1
.LC13:
	.string	"Peer Key"
.LC14:
	.string	"Error reading peer key %s\n"
	.section	.rodata.str1.8
	.align 8
.LC15:
	.string	"%s: Error setting up peer key\n"
	.align 8
.LC16:
	.string	"%s: Can't set parameter \"%s\":\n"
	.align 8
.LC17:
	.string	"%s: Signature file specified for non verify\n"
	.align 8
.LC18:
	.string	"%s: No signature file specified for verify\n"
	.section	.rodata.str1.1
.LC19:
	.string	"rb"
.LC20:
	.string	"Can't open signature file %s\n"
.LC21:
	.string	"Error reading signature data\n"
.LC22:
	.string	"Error reading input Data\n"
	.section	.rodata.str1.8
	.align 8
.LC23:
	.string	"Error: The input data looks too long to be a hash\n"
	.align 8
.LC24:
	.string	"Signature Verified Successfully\n"
	.align 8
.LC25:
	.string	"Signature Verification Failure\n"
	.section	.rodata.str1.1
.LC26:
	.string	"buffer output"
.LC27:
	.string	"Public Key operation error\n"
.LC28:
	.string	"Key derivation failed\n"
	.text
	.p2align 4
	.globl	pkeyutl_main
	.type	pkeyutl_main, @function
pkeyutl_main:
.LFB1435:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	pkeyutl_options(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movl	$8, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	xorl	%r13d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movl	$1, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	.L17(%rip), %rbx
	subq	$152, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movq	$0, -72(%rbp)
	movl	$32773, -88(%rbp)
	movl	$32773, -84(%rbp)
	call	opt_init@PLT
	movq	$0, -112(%rbp)
	movq	%rax, -120(%rbp)
	movl	$0, -140(%rbp)
	movq	$0, -128(%rbp)
	movq	$0, -152(%rbp)
	movb	$0, -143(%rbp)
	movb	$0, -142(%rbp)
	movb	$0, -141(%rbp)
	movq	$0, -160(%rbp)
	movq	$0, -136(%rbp)
	movq	$0, -168(%rbp)
	movq	$0, -176(%rbp)
	movq	$0, -104(%rbp)
.L12:
	call	opt_next@PLT
	testl	%eax, %eax
	je	.L191
.L47:
	cmpl	$25, %eax
	jg	.L13
	cmpl	$-1, %eax
	jl	.L12
	addl	$1, %eax
	cmpl	$26, %eax
	ja	.L12
	movslq	(%rbx,%rax,4), %rax
	addq	%rbx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L17:
	.long	.L41-.L17
	.long	.L12-.L17
	.long	.L40-.L17
	.long	.L39-.L17
	.long	.L38-.L17
	.long	.L37-.L17
	.long	.L36-.L17
	.long	.L117-.L17
	.long	.L35-.L17
	.long	.L34-.L17
	.long	.L33-.L17
	.long	.L32-.L17
	.long	.L31-.L17
	.long	.L30-.L17
	.long	.L29-.L17
	.long	.L28-.L17
	.long	.L27-.L17
	.long	.L182-.L17
	.long	.L25-.L17
	.long	.L24-.L17
	.long	.L23-.L17
	.long	.L22-.L17
	.long	.L21-.L17
	.long	.L20-.L17
	.long	.L19-.L17
	.long	.L18-.L17
	.long	.L16-.L17
	.text
.L117:
	movl	$2, %r12d
	call	opt_next@PLT
	testl	%eax, %eax
	jne	.L47
.L191:
	call	opt_num_rest@PLT
	movl	%eax, %ebx
	testl	%eax, %eax
	jne	.L41
	cmpq	$0, -112(%rbp)
	je	.L48
	movl	-140(%rbp), %edx
	testl	%edx, %edx
	je	.L192
	cmpl	$8, %r14d
	movq	$0, -64(%rbp)
	movl	-88(%rbp), %r10d
	sete	-144(%rbp)
	movzbl	-144(%rbp), %eax
	cmpl	$512, %r14d
	je	.L53
	testb	%al, %al
	jne	.L53
.L115:
	movb	$0, -144(%rbp)
	cmpl	$1024, %r14d
	je	.L52
.L53:
	leaq	-64(%rbp), %rax
	movq	-160(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	movq	%rax, %rdx
	movl	%r10d, -180(%rbp)
	movq	%rax, -192(%rbp)
	call	app_passwd@PLT
	movl	-180(%rbp), %r10d
	testl	%eax, %eax
	je	.L193
	cmpl	$2, %r12d
	je	.L56
	cmpl	$3, %r12d
	je	.L57
	cmpl	$1, %r12d
	je	.L58
.L190:
	testl	%r13d, %r13d
	jne	.L194
	cmpq	$0, -112(%rbp)
	je	.L64
	xorl	%r12d, %r12d
.L60:
	movq	-112(%rbp), %r13
	movq	%r13, %rdi
	call	OBJ_sn2nid@PLT
	movl	%eax, %edi
	testl	%eax, %eax
	jne	.L67
	movq	%r13, %rdi
	call	OBJ_ln2nid@PLT
	movl	%eax, %edi
	testl	%eax, %eax
	je	.L195
.L67:
	movq	%r12, %rsi
	call	EVP_PKEY_CTX_new_id@PLT
	movl	$-1, -152(%rbp)
	movq	%rax, %r12
.L68:
	testq	%r12, %r12
	je	.L64
	cmpl	$256, %r14d
	je	.L70
	jg	.L71
	cmpl	$16, %r14d
	je	.L72
	cmpl	$32, %r14d
	jne	.L196
	movq	%r12, %rdi
	call	EVP_PKEY_verify_recover_init@PLT
.L78:
	testl	%eax, %eax
	jle	.L75
	movq	-64(%rbp), %rdi
	movl	$467, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	cmpq	$0, -128(%rbp)
	je	.L83
	movl	-84(%rbp), %esi
	movl	$0, %r8d
	movq	-128(%rbp), %rdi
	leaq	.LC13(%rip), %r9
	cmpl	$8, %esi
	cmove	-104(%rbp), %r8
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	call	load_pubkey@PLT
	testq	%rax, %rax
	je	.L197
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -112(%rbp)
	call	EVP_PKEY_derive_set_peer@PLT
	movq	-112(%rbp), %r8
	movl	%eax, %r13d
	movq	%r8, %rdi
	call	EVP_PKEY_free@PLT
	testl	%r13d, %r13d
	jle	.L198
.L83:
	testq	%r15, %r15
	je	.L88
	movq	%r15, %rdi
	call	OPENSSL_sk_num@PLT
	movl	%eax, %r13d
	testl	%eax, %eax
	jle	.L88
	xorl	%edx, %edx
	movl	%ebx, -128(%rbp)
	movl	%r14d, -112(%rbp)
	movl	%edx, %ebx
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L89:
	addl	$1, %ebx
	cmpl	%r13d, %ebx
	je	.L199
.L90:
	movl	%ebx, %esi
	movq	%r15, %rdi
	call	OPENSSL_sk_value@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %r14
	call	pkey_ctrl_string@PLT
	testl	%eax, %eax
	jg	.L89
	movq	-120(%rbp), %rdx
	movq	bio_err(%rip), %rdi
	movq	%r14, %rcx
	xorl	%eax, %eax
	leaq	.LC16(%rip), %rsi
	call	BIO_printf@PLT
.L184:
	movq	bio_err(%rip), %rdi
	movl	$1, %ebx
	xorl	%r13d, %r13d
	call	ERR_print_errors@PLT
	xorl	%r10d, %r10d
	xorl	%r11d, %r11d
	jmp	.L43
.L38:
	movl	$1, %r13d
	jmp	.L12
.L18:
	call	opt_arg@PLT
	xorl	%r12d, %r12d
	movq	%rax, -112(%rbp)
.L182:
	movl	$1024, %r14d
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L13:
	leal	-1501(%rax), %edx
	cmpl	$1, %edx
	ja	.L12
	movl	%eax, %edi
	call	opt_rand@PLT
	testl	%eax, %eax
	jne	.L12
	.p2align 4,,10
	.p2align 3
.L118:
	movl	$1, %ebx
	xorl	%r13d, %r13d
	xorl	%r12d, %r12d
	xorl	%r10d, %r10d
	xorl	%r11d, %r11d
.L43:
	movq	%r12, %rdi
	movq	%r10, -120(%rbp)
	movq	%r11, -112(%rbp)
	call	EVP_PKEY_CTX_free@PLT
	movq	-104(%rbp), %rdi
	call	release_engine@PLT
	movq	-112(%rbp), %r11
	movq	%r11, %rdi
	call	BIO_free@PLT
	movq	-120(%rbp), %r10
	movq	%r10, %rdi
	call	BIO_free_all@PLT
	movq	-80(%rbp), %rdi
	movl	$357, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movl	$358, %edx
	leaq	.LC6(%rip), %rsi
	movq	%r13, %rdi
	call	CRYPTO_free@PLT
	movq	-72(%rbp), %rdi
	movl	$359, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%r15, %rdi
	call	OPENSSL_sk_free@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L200
	addq	$152, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L202:
	.cfi_restore_state
	movq	-120(%rbp), %rdx
	movq	bio_err(%rip), %rdi
	leaq	.LC3(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L41:
	movq	-120(%rbp), %rdx
	movq	bio_err(%rip), %rdi
	leaq	.LC0(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L118
.L16:
	call	opt_arg@PLT
	movl	$10, %edx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	strtol@PLT
	movl	%eax, -140(%rbp)
	jmp	.L12
.L19:
	testq	%r15, %r15
	je	.L201
.L45:
	call	opt_arg@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	jne	.L12
.L46:
	movq	bio_err(%rip), %rdi
	leaq	.LC1(%rip), %rsi
	call	BIO_puts@PLT
	jmp	.L118
.L20:
	call	opt_arg@PLT
	leaq	-88(%rbp), %rdx
	movl	$18, %esi
	movq	%rax, %rdi
	call	opt_format@PLT
	testl	%eax, %eax
	jne	.L12
	jmp	.L41
.L21:
	call	opt_arg@PLT
	leaq	-84(%rbp), %rdx
	movl	$18, %esi
	movq	%rax, %rdi
	call	opt_format@PLT
	testl	%eax, %eax
	jne	.L12
	jmp	.L41
.L22:
	call	opt_arg@PLT
	movq	%rax, -160(%rbp)
	jmp	.L12
.L23:
	call	opt_arg@PLT
	movq	%rax, -128(%rbp)
	jmp	.L12
.L24:
	call	opt_arg@PLT
	movq	%rax, -152(%rbp)
	jmp	.L12
.L25:
	call	opt_arg@PLT
	movq	%rax, -136(%rbp)
	jmp	.L12
.L27:
	movl	$512, %r14d
	jmp	.L12
.L28:
	movl	$256, %r14d
	jmp	.L12
.L29:
	movb	$1, -143(%rbp)
	jmp	.L12
.L30:
	movl	$32, %r14d
	jmp	.L12
.L31:
	movl	$16, %r14d
	jmp	.L12
.L32:
	movl	$8, %r14d
	jmp	.L12
.L33:
	movb	$1, -141(%rbp)
	jmp	.L12
.L34:
	movb	$1, -142(%rbp)
	jmp	.L12
.L35:
	movl	$3, %r12d
	jmp	.L12
.L36:
	call	opt_arg@PLT
	movq	%rax, -168(%rbp)
	jmp	.L12
.L37:
	call	opt_arg@PLT
	movq	%rax, -176(%rbp)
	jmp	.L12
.L39:
	call	opt_arg@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	setup_engine@PLT
	movq	%rax, -104(%rbp)
	jmp	.L12
.L40:
	leaq	pkeyutl_options(%rip), %rdi
	xorl	%ebx, %ebx
	xorl	%r13d, %r13d
	xorl	%r12d, %r12d
	call	opt_help@PLT
	xorl	%r10d, %r10d
	xorl	%r11d, %r11d
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L201:
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	jne	.L45
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L48:
	cmpq	$0, -152(%rbp)
	je	.L202
	cmpq	$0, -128(%rbp)
	je	.L51
	cmpl	$1024, %r14d
	jne	.L203
.L51:
	cmpl	$8, %r14d
	movq	$0, -64(%rbp)
	movl	-88(%rbp), %r10d
	sete	-144(%rbp)
	movzbl	-144(%rbp), %eax
	cmpl	$512, %r14d
	je	.L52
	testb	%al, %al
	je	.L115
.L52:
	cmpl	$1, %r12d
	je	.L53
	cmpq	$0, -112(%rbp)
	jne	.L53
	movq	bio_err(%rip), %rdi
	leaq	.LC5(%rip), %rsi
	xorl	%eax, %eax
.L183:
	call	BIO_printf@PLT
	movq	-64(%rbp), %rdi
	movl	$467, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
.L54:
	movq	-120(%rbp), %rdx
	movq	bio_err(%rip), %rdi
	leaq	.LC12(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	jmp	.L118
.L194:
	cmpq	$0, -112(%rbp)
	movq	-104(%rbp), %r12
	jne	.L60
.L64:
	movq	-64(%rbp), %rdi
	movl	$467, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	jmp	.L54
.L196:
	cmpl	$8, %r14d
	jne	.L75
	movq	%r12, %rdi
	call	EVP_PKEY_sign_init@PLT
	jmp	.L78
.L71:
	cmpl	$512, %r14d
	je	.L76
	cmpl	$1024, %r14d
	jne	.L75
	movq	%r12, %rdi
	call	EVP_PKEY_derive_init@PLT
	jmp	.L78
.L199:
	movl	-112(%rbp), %r14d
	movl	-128(%rbp), %ebx
.L88:
	cmpq	$0, -136(%rbp)
	je	.L87
	cmpl	$16, %r14d
	jne	.L204
.L87:
	cmpl	$16, %r14d
	sete	-112(%rbp)
	cmpq	$0, -136(%rbp)
	movzbl	-112(%rbp), %eax
	jne	.L91
	testb	%al, %al
	jne	.L205
.L91:
	cmpl	$1024, %r14d
	je	.L124
	movq	-176(%rbp), %rdi
	movl	$2, %edx
	movl	$114, %esi
	call	bio_open_default@PLT
	movq	%rax, %r11
	testq	%rax, %rax
	je	.L206
.L92:
	movq	-168(%rbp), %rdi
	movl	$2, %edx
	movl	$119, %esi
	movq	%r11, -120(%rbp)
	call	bio_open_default@PLT
	movq	-120(%rbp), %r11
	testq	%rax, %rax
	movq	%rax, %r10
	je	.L126
	movq	-136(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L127
	leaq	.LC19(%rip), %rsi
	movq	%rax, -128(%rbp)
	call	BIO_new_file@PLT
	movq	-120(%rbp), %r11
	movq	-128(%rbp), %r10
	testq	%rax, %rax
	movq	%rax, %r13
	je	.L207
	movl	-152(%rbp), %eax
	leaq	-72(%rbp), %rdi
	movq	%r13, %rdx
	movq	%r10, -136(%rbp)
	movq	%r11, -128(%rbp)
	leal	(%rax,%rax,4), %esi
	addl	%esi, %esi
	call	bio_to_mem@PLT
	movq	%r13, %rdi
	movl	%eax, -120(%rbp)
	call	BIO_free@PLT
	movl	-120(%rbp), %eax
	movq	-128(%rbp), %r11
	movq	-136(%rbp), %r10
	testl	%eax, %eax
	js	.L208
.L93:
	testq	%r11, %r11
	je	.L128
	movl	-152(%rbp), %eax
	movq	%r11, %rdx
	leaq	-80(%rbp), %rdi
	movq	%r10, -136(%rbp)
	movq	%r11, -128(%rbp)
	leal	(%rax,%rax,4), %esi
	addl	%esi, %esi
	call	bio_to_mem@PLT
	movq	-128(%rbp), %r11
	movq	-136(%rbp), %r10
	movslq	%eax, %r9
	testl	%r9d, %r9d
	js	.L209
	cmpb	$0, -143(%rbp)
	je	.L100
	movslq	%r9d, %r13
	movq	%r13, %r8
	shrq	%r8
	je	.L100
	subq	$1, %r13
	xorl	%eax, %eax
.L101:
	movq	-80(%rbp), %rsi
	movq	%r13, %rdx
	subq	%rax, %rdx
	leaq	(%rsi,%rax), %rcx
	movzbl	(%rsi,%rdx), %esi
	addq	$1, %rax
	movzbl	(%rcx), %edi
	movb	%sil, (%rcx)
	movq	-80(%rbp), %rcx
	movb	%dil, (%rcx,%rdx)
	cmpq	%rax, %r8
	jne	.L101
.L100:
	cmpl	$64, %r9d
	jle	.L95
	cmpb	$0, -112(%rbp)
	jne	.L132
	cmpb	$0, -144(%rbp)
	jne	.L132
.L104:
	movslq	-140(%rbp), %rax
	testl	%eax, %eax
	je	.L106
	movq	%rax, -64(%rbp)
.L107:
	movl	-140(%rbp), %edi
	leaq	.LC26(%rip), %rsi
	movq	%r10, -120(%rbp)
	movq	%r11, -112(%rbp)
	movq	%r9, -128(%rbp)
	call	app_malloc@PLT
	movq	-128(%rbp), %r9
	movq	-80(%rbp), %r8
	movl	%r14d, %esi
	movq	-192(%rbp), %rcx
	movq	%rax, %rdx
	movq	%r12, %rdi
	movq	%rax, %r13
	call	do_keyop
	movq	-64(%rbp), %rdx
	movq	-112(%rbp), %r11
	testl	%eax, %eax
	movq	-120(%rbp), %r10
	jg	.L109
.L108:
	movq	%r10, -120(%rbp)
	movq	bio_err(%rip), %rdi
	movq	%r11, -112(%rbp)
	cmpl	$1024, %r14d
	je	.L110
	leaq	.LC27(%rip), %rsi
	call	BIO_puts@PLT
	movq	-112(%rbp), %r11
	movq	-120(%rbp), %r10
.L111:
	movq	bio_err(%rip), %rdi
	movq	%r10, -120(%rbp)
	movl	$1, %ebx
	movq	%r11, -112(%rbp)
	call	ERR_print_errors@PLT
	movq	-112(%rbp), %r11
	movq	-120(%rbp), %r10
	jmp	.L43
.L75:
	movq	%r12, %rdi
	call	EVP_PKEY_CTX_free@PLT
	movq	-64(%rbp), %rdi
	movl	$467, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	jmp	.L54
.L58:
	movq	-104(%rbp), %r8
	movq	-64(%rbp), %rcx
	xorl	%edx, %edx
	movl	%r10d, %esi
	movq	-152(%rbp), %rdi
	leaq	.LC8(%rip), %r9
	call	load_key@PLT
	movq	%rax, %r8
.L62:
	testl	%r13d, %r13d
	movl	$0, %r12d
	cmovne	-104(%rbp), %r12
	cmpq	$0, -112(%rbp)
	jne	.L60
	testq	%r8, %r8
	je	.L64
	movq	%r8, %rdi
	movq	%r8, -112(%rbp)
	call	EVP_PKEY_size@PLT
	movq	-112(%rbp), %r8
	movq	%r12, %rsi
	movl	%eax, -152(%rbp)
	movq	%r8, %rdi
	call	EVP_PKEY_CTX_new@PLT
	movq	-112(%rbp), %r8
	movq	%rax, %r12
	movq	%r8, %rdi
	call	EVP_PKEY_free@PLT
	jmp	.L68
.L57:
	movq	-152(%rbp), %rdi
	leaq	.LC10(%rip), %rdx
	movl	%r10d, %esi
	call	load_cert@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L190
	movq	%rax, %rdi
	call	X509_get_pubkey@PLT
	movq	%r12, %rdi
	movq	%rax, -152(%rbp)
	call	X509_free@PLT
	movq	-152(%rbp), %r8
	jmp	.L62
.L56:
	movq	-104(%rbp), %r8
	movq	-152(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	leaq	.LC9(%rip), %r9
	movl	%r10d, %esi
	call	load_pubkey@PLT
	movq	%rax, %r8
	jmp	.L62
.L192:
	movq	-120(%rbp), %rdx
	movq	bio_err(%rip), %rdi
	leaq	.LC2(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L41
.L128:
	xorl	%r9d, %r9d
.L95:
	cmpl	$16, %r14d
	jne	.L104
	movslq	-120(%rbp), %rdx
	movq	-80(%rbp), %rcx
	movq	%r9, %r8
	movq	%r12, %rdi
	movq	-72(%rbp), %rsi
	movq	%r10, -128(%rbp)
	movq	%r11, -112(%rbp)
	call	EVP_PKEY_verify@PLT
	movq	-112(%rbp), %r11
	movq	-128(%rbp), %r10
	cmpl	$1, %eax
	movq	%r11, -120(%rbp)
	je	.L210
	movq	%r10, %rdi
	movq	%r10, -112(%rbp)
	movl	$1, %ebx
	xorl	%r13d, %r13d
	leaq	.LC25(%rip), %rsi
	call	BIO_puts@PLT
	movq	-112(%rbp), %r10
	movq	-120(%rbp), %r11
	jmp	.L43
.L127:
	movl	$-1, -120(%rbp)
	jmp	.L93
.L124:
	xorl	%r11d, %r11d
	jmp	.L92
.L109:
	cmpb	$0, -142(%rbp)
	jne	.L211
	cmpb	$0, -141(%rbp)
	movq	%r11, -120(%rbp)
	movq	%r13, %rsi
	movq	%r10, %rdi
	movq	%r10, -112(%rbp)
	je	.L113
	call	BIO_dump@PLT
	movq	-112(%rbp), %r10
	movq	-120(%rbp), %r11
	jmp	.L43
.L204:
	movq	-120(%rbp), %rdx
	leaq	.LC17(%rip), %rsi
.L185:
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
	movl	$1, %ebx
	xorl	%r13d, %r13d
	call	BIO_printf@PLT
	xorl	%r10d, %r10d
	xorl	%r11d, %r11d
	jmp	.L43
.L205:
	movq	-120(%rbp), %rdx
	leaq	.LC18(%rip), %rsi
	jmp	.L185
.L76:
	movq	%r12, %rdi
	call	EVP_PKEY_decrypt_init@PLT
	jmp	.L78
.L198:
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	testl	%r13d, %r13d
	jne	.L83
.L82:
	movq	-120(%rbp), %rdx
	movq	bio_err(%rip), %rdi
	leaq	.LC15(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L184
.L70:
	movq	%r12, %rdi
	call	EVP_PKEY_encrypt_init@PLT
	jmp	.L78
.L72:
	movq	%r12, %rdi
	call	EVP_PKEY_verify_init@PLT
	jmp	.L78
.L197:
	movq	-128(%rbp), %rdx
	movq	bio_err(%rip), %rdi
	leaq	.LC14(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	jmp	.L82
.L193:
	movq	bio_err(%rip), %rdi
	leaq	.LC7(%rip), %rsi
	jmp	.L183
.L203:
	movq	-120(%rbp), %rdx
	movq	bio_err(%rip), %rdi
	leaq	.LC4(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L41
.L106:
	movq	-80(%rbp), %r8
	movq	-192(%rbp), %rcx
	xorl	%edx, %edx
	movl	%r14d, %esi
	movq	%r12, %rdi
	movq	%r10, -128(%rbp)
	movq	%r11, -120(%rbp)
	movq	%r9, -112(%rbp)
	call	do_keyop
	movq	-120(%rbp), %r11
	movq	-128(%rbp), %r10
	testl	%eax, %eax
	jle	.L129
	movq	-64(%rbp), %rdx
	xorl	%r13d, %r13d
	movq	-112(%rbp), %r9
	testq	%rdx, %rdx
	je	.L109
	movl	%edx, -140(%rbp)
	jmp	.L107
.L126:
	xorl	%r13d, %r13d
	movl	$1, %ebx
	jmp	.L43
.L206:
	xorl	%r13d, %r13d
	xorl	%r10d, %r10d
	movl	$1, %ebx
	jmp	.L43
.L113:
	call	BIO_write@PLT
	movq	-112(%rbp), %r10
	movq	-120(%rbp), %r11
	jmp	.L43
.L211:
	movq	%r10, %rdi
	movl	$-1, %r8d
	movq	%r13, %rsi
	movq	%r11, -120(%rbp)
	movl	$1, %ecx
	movq	%r10, -112(%rbp)
	call	ASN1_parse_dump@PLT
	movq	-112(%rbp), %r10
	movq	-120(%rbp), %r11
	testl	%eax, %eax
	jne	.L43
	movq	bio_err(%rip), %rdi
	movq	%r10, -120(%rbp)
	xorl	%ebx, %ebx
	movq	%r11, -112(%rbp)
	call	ERR_print_errors@PLT
	movq	-112(%rbp), %r11
	movq	-120(%rbp), %r10
	jmp	.L43
.L207:
	movq	-136(%rbp), %rdx
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
	movq	%r10, -120(%rbp)
	leaq	.LC20(%rip), %rsi
	movq	%r11, -112(%rbp)
	movl	$1, %ebx
	call	BIO_printf@PLT
	movq	-112(%rbp), %r11
	movq	-120(%rbp), %r10
	jmp	.L43
.L208:
	movq	%r10, -120(%rbp)
	leaq	.LC21(%rip), %rsi
	movq	%r11, -112(%rbp)
.L186:
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
	movl	$1, %ebx
	xorl	%r13d, %r13d
	call	BIO_printf@PLT
	movq	-112(%rbp), %r11
	movq	-120(%rbp), %r10
	jmp	.L43
.L195:
	movq	-112(%rbp), %rdx
	movq	bio_err(%rip), %rdi
	leaq	.LC11(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	-64(%rbp), %rdi
	movl	$467, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	jmp	.L54
.L209:
	movq	%r10, -120(%rbp)
	leaq	.LC22(%rip), %rsi
	movq	%r11, -112(%rbp)
	jmp	.L186
.L129:
	xorl	%r13d, %r13d
	jmp	.L108
.L110:
	leaq	.LC28(%rip), %rsi
	call	BIO_puts@PLT
	movq	-120(%rbp), %r10
	movq	-112(%rbp), %r11
	jmp	.L111
.L132:
	movq	%r10, -120(%rbp)
	leaq	.LC23(%rip), %rsi
	movq	%r11, -112(%rbp)
	jmp	.L186
.L210:
	movq	%r10, %rdi
	leaq	.LC24(%rip), %rsi
	movq	%r10, -112(%rbp)
	xorl	%r13d, %r13d
	call	BIO_puts@PLT
	movq	-112(%rbp), %r10
	movq	-120(%rbp), %r11
	jmp	.L43
.L200:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1435:
	.size	pkeyutl_main, .-pkeyutl_main
	.globl	pkeyutl_options
	.section	.rodata.str1.1
.LC29:
	.string	"help"
.LC30:
	.string	"Display this summary"
.LC31:
	.string	"in"
.LC32:
	.string	"Input file - default stdin"
.LC33:
	.string	"out"
.LC34:
	.string	"Output file - default stdout"
.LC35:
	.string	"pubin"
.LC36:
	.string	"Input is a public key"
.LC37:
	.string	"certin"
	.section	.rodata.str1.8
	.align 8
.LC38:
	.string	"Input is a cert with a public key"
	.section	.rodata.str1.1
.LC39:
	.string	"asn1parse"
.LC40:
	.string	"asn1parse the output data"
.LC41:
	.string	"hexdump"
.LC42:
	.string	"Hex dump output"
.LC43:
	.string	"sign"
	.section	.rodata.str1.8
	.align 8
.LC44:
	.string	"Sign input data with private key"
	.section	.rodata.str1.1
.LC45:
	.string	"verify"
.LC46:
	.string	"Verify with public key"
.LC47:
	.string	"verifyrecover"
	.section	.rodata.str1.8
	.align 8
.LC48:
	.string	"Verify with public key, recover original data"
	.section	.rodata.str1.1
.LC49:
	.string	"rev"
	.section	.rodata.str1.8
	.align 8
.LC50:
	.string	"Reverse the order of the input buffer"
	.section	.rodata.str1.1
.LC51:
	.string	"encrypt"
	.section	.rodata.str1.8
	.align 8
.LC52:
	.string	"Encrypt input data with public key"
	.section	.rodata.str1.1
.LC53:
	.string	"decrypt"
	.section	.rodata.str1.8
	.align 8
.LC54:
	.string	"Decrypt input data with private key"
	.section	.rodata.str1.1
.LC55:
	.string	"derive"
.LC56:
	.string	"Derive shared secret"
.LC57:
	.string	"kdf"
.LC58:
	.string	"Use KDF algorithm"
.LC59:
	.string	"kdflen"
.LC60:
	.string	"KDF algorithm output length"
.LC61:
	.string	"sigfile"
	.section	.rodata.str1.8
	.align 8
.LC62:
	.string	"Signature file (verify operation only)"
	.section	.rodata.str1.1
.LC63:
	.string	"inkey"
.LC64:
	.string	"Input private key file"
.LC65:
	.string	"peerkey"
	.section	.rodata.str1.8
	.align 8
.LC66:
	.string	"Peer key file used in key derivation"
	.section	.rodata.str1.1
.LC67:
	.string	"passin"
.LC68:
	.string	"Input file pass phrase source"
.LC69:
	.string	"peerform"
.LC70:
	.string	"Peer key format - default PEM"
.LC71:
	.string	"keyform"
	.section	.rodata.str1.8
	.align 8
.LC72:
	.string	"Private key format - default PEM"
	.section	.rodata.str1.1
.LC73:
	.string	"pkeyopt"
	.section	.rodata.str1.8
	.align 8
.LC74:
	.string	"Public key options as opt:value"
	.section	.rodata.str1.1
.LC75:
	.string	"rand"
	.section	.rodata.str1.8
	.align 8
.LC76:
	.string	"Load the file(s) into the random number generator"
	.section	.rodata.str1.1
.LC77:
	.string	"writerand"
	.section	.rodata.str1.8
	.align 8
.LC78:
	.string	"Write random data to the specified file"
	.section	.rodata.str1.1
.LC79:
	.string	"engine"
	.section	.rodata.str1.8
	.align 8
.LC80:
	.string	"Use engine, possibly a hardware device"
	.section	.rodata.str1.1
.LC81:
	.string	"engine_impl"
	.section	.rodata.str1.8
	.align 8
.LC82:
	.string	"Also use engine given by -engine for crypto operations"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	pkeyutl_options, @object
	.size	pkeyutl_options, 672
pkeyutl_options:
	.quad	.LC29
	.long	1
	.long	45
	.quad	.LC30
	.quad	.LC31
	.long	4
	.long	60
	.quad	.LC32
	.quad	.LC33
	.long	5
	.long	62
	.quad	.LC34
	.quad	.LC35
	.long	6
	.long	45
	.quad	.LC36
	.quad	.LC37
	.long	7
	.long	45
	.quad	.LC38
	.quad	.LC39
	.long	8
	.long	45
	.quad	.LC40
	.quad	.LC41
	.long	9
	.long	45
	.quad	.LC42
	.quad	.LC43
	.long	10
	.long	45
	.quad	.LC44
	.quad	.LC45
	.long	11
	.long	45
	.quad	.LC46
	.quad	.LC47
	.long	12
	.long	45
	.quad	.LC48
	.quad	.LC49
	.long	13
	.long	45
	.quad	.LC50
	.quad	.LC51
	.long	14
	.long	45
	.quad	.LC52
	.quad	.LC53
	.long	15
	.long	45
	.quad	.LC54
	.quad	.LC55
	.long	16
	.long	45
	.quad	.LC56
	.quad	.LC57
	.long	24
	.long	115
	.quad	.LC58
	.quad	.LC59
	.long	25
	.long	112
	.quad	.LC60
	.quad	.LC61
	.long	17
	.long	60
	.quad	.LC62
	.quad	.LC63
	.long	18
	.long	115
	.quad	.LC64
	.quad	.LC65
	.long	19
	.long	115
	.quad	.LC66
	.quad	.LC67
	.long	20
	.long	115
	.quad	.LC68
	.quad	.LC69
	.long	21
	.long	69
	.quad	.LC70
	.quad	.LC71
	.long	22
	.long	69
	.quad	.LC72
	.quad	.LC73
	.long	23
	.long	115
	.quad	.LC74
	.quad	.LC75
	.long	1501
	.long	115
	.quad	.LC76
	.quad	.LC77
	.long	1502
	.long	62
	.quad	.LC78
	.quad	.LC79
	.long	2
	.long	115
	.quad	.LC80
	.quad	.LC81
	.long	3
	.long	45
	.quad	.LC82
	.quad	0
	.zero	16
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
