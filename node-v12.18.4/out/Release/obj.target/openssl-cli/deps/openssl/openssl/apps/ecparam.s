	.file	"ecparam.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"CURVE DESCRIPTION NOT AVAILABLE"
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"%s: Use -help for summary.\n"
.LC2:
	.string	"list curves"
	.section	.rodata.str1.8
	.align 8
.LC3:
	.string	"../deps/openssl/openssl/apps/ecparam.c"
	.section	.rodata.str1.1
.LC4:
	.string	""
.LC5:
	.string	"  %-10s: "
.LC6:
	.string	"%s\n"
.LC7:
	.string	"secp192r1"
	.section	.rodata.str1.8
	.align 8
.LC8:
	.string	"using curve name prime192v1 instead of secp192r1\n"
	.section	.rodata.str1.1
.LC9:
	.string	"secp256r1"
	.section	.rodata.str1.8
	.align 8
.LC10:
	.string	"using curve name prime256v1 instead of secp256r1\n"
	.section	.rodata.str1.1
.LC11:
	.string	"unknown curve name (%s)\n"
.LC12:
	.string	"unable to create curve (%s)\n"
	.section	.rodata.str1.8
	.align 8
.LC13:
	.string	"unable to load elliptic curve parameters\n"
	.align 8
.LC14:
	.string	"checking elliptic curve parameters: "
	.section	.rodata.str1.1
.LC15:
	.string	"failed\n"
.LC16:
	.string	"ok\n"
.LC17:
	.string	"Can't allocate BN"
	.section	.rodata.str1.8
	.align 8
.LC18:
	.string	"Can only handle X9.62 prime fields\n"
	.section	.rodata.str1.1
.LC19:
	.string	"BN buffer"
	.section	.rodata.str1.8
	.align 8
.LC20:
	.string	"EC_GROUP *get_ec_group_%d(void)\n{\n"
	.section	.rodata.str1.1
.LC21:
	.string	"ec_p"
.LC22:
	.string	"ec_a"
.LC23:
	.string	"ec_b"
.LC24:
	.string	"ec_gen"
.LC25:
	.string	"ec_order"
.LC26:
	.string	"ec_cofactor"
	.section	.rodata.str1.8
	.align 8
.LC27:
	.string	"    int ok = 0;\n    EC_GROUP *group = NULL;\n    EC_POINT *point = NULL;\n    BIGNUM *tmp_1 = NULL;\n    BIGNUM *tmp_2 = NULL;\n    BIGNUM *tmp_3 = NULL;\n\n"
	.align 8
.LC28:
	.string	"    if ((tmp_1 = BN_bin2bn(ec_p_%d, sizeof(ec_p_%d), NULL)) == NULL)\n        goto err;\n"
	.align 8
.LC29:
	.string	"    if ((tmp_2 = BN_bin2bn(ec_a_%d, sizeof(ec_a_%d), NULL)) == NULL)\n        goto err;\n"
	.align 8
.LC30:
	.string	"    if ((tmp_3 = BN_bin2bn(ec_b_%d, sizeof(ec_b_%d), NULL)) == NULL)\n        goto err;\n"
	.align 8
.LC31:
	.string	"    if ((group = EC_GROUP_new_curve_GFp(tmp_1, tmp_2, tmp_3, NULL)) == NULL)\n        goto err;\n\n"
	.section	.rodata.str1.1
.LC32:
	.string	"    /* build generator */\n"
	.section	.rodata.str1.8
	.align 8
.LC33:
	.string	"    if ((tmp_1 = BN_bin2bn(ec_gen_%d, sizeof(ec_gen_%d), tmp_1)) == NULL)\n        goto err;\n"
	.align 8
.LC34:
	.string	"    point = EC_POINT_bn2point(group, tmp_1, NULL, NULL);\n"
	.align 8
.LC35:
	.string	"    if (point == NULL)\n        goto err;\n"
	.align 8
.LC36:
	.string	"    if ((tmp_2 = BN_bin2bn(ec_order_%d, sizeof(ec_order_%d), tmp_2)) == NULL)\n        goto err;\n"
	.align 8
.LC37:
	.string	"    if ((tmp_3 = BN_bin2bn(ec_cofactor_%d, sizeof(ec_cofactor_%d), tmp_3)) == NULL)\n        goto err;\n"
	.align 8
.LC38:
	.string	"    if (!EC_GROUP_set_generator(group, point, tmp_2, tmp_3))\n        goto err;\nok = 1;\n"
	.align 8
.LC39:
	.string	"err:\n    BN_free(tmp_1);\n    BN_free(tmp_2);\n    BN_free(tmp_3);\n    EC_POINT_free(point);\n    if (!ok) {\n        EC_GROUP_free(group);\n        return NULL;\n    }\n    return (group);\n}\n"
	.align 8
.LC40:
	.string	"unable to write elliptic curve parameters\n"
	.align 8
.LC41:
	.string	"unable to set group when generating key\n"
	.section	.rodata.str1.1
.LC42:
	.string	"unable to generate key\n"
	.text
	.p2align 4
	.globl	ecparam_main
	.type	ecparam_main, @function
ecparam_main:
.LFB1435:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	ecparam_options(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	xorl	%r13d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	.L7(%rip), %rbx
	subq	$136, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$1, -72(%rbp)
	movl	$32773, -68(%rbp)
	movl	$32773, -64(%rbp)
	movl	$0, -60(%rbp)
	call	opt_init@PLT
	movl	$0, -128(%rbp)
	movl	$0, -120(%rbp)
	movq	%rax, %r14
	movl	$0, -144(%rbp)
	movl	$0, -104(%rbp)
	movl	$0, -152(%rbp)
	movl	$0, -160(%rbp)
	movq	$0, -112(%rbp)
	movq	$0, -96(%rbp)
	movl	$4, -136(%rbp)
	movq	$0, -88(%rbp)
.L2:
	call	opt_next@PLT
	testl	%eax, %eax
	je	.L134
.L26:
	cmpl	$16, %eax
	jg	.L3
	cmpl	$-1, %eax
	jl	.L2
	addl	$1, %eax
	cmpl	$17, %eax
	ja	.L2
	movslq	(%rbx,%rax,4), %rax
	addq	%rbx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L7:
	.long	.L22-.L7
	.long	.L2-.L7
	.long	.L21-.L7
	.long	.L20-.L7
	.long	.L19-.L7
	.long	.L18-.L7
	.long	.L17-.L7
	.long	.L16-.L7
	.long	.L72-.L7
	.long	.L15-.L7
	.long	.L14-.L7
	.long	.L13-.L7
	.long	.L12-.L7
	.long	.L11-.L7
	.long	.L10-.L7
	.long	.L9-.L7
	.long	.L8-.L7
	.long	.L6-.L7
	.text
.L72:
	movl	$1, %r12d
	call	opt_next@PLT
	testl	%eax, %eax
	jne	.L26
.L134:
	call	opt_num_rest@PLT
	testl	%eax, %eax
	jne	.L22
	movl	-68(%rbp), %edx
	movq	%r15, %rdi
	movl	$114, %esi
	call	bio_open_default@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L27
	movl	-128(%rbp), %edx
	movl	-64(%rbp), %esi
	movq	-112(%rbp), %rdi
	call	bio_open_owner@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L73
	movl	-104(%rbp), %eax
	testl	%eax, %eax
	jne	.L135
	movq	-96(%rbp), %rsi
	testq	%rsi, %rsi
	je	.L36
	movl	$10, %ecx
	leaq	.LC7(%rip), %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L136
	movq	-96(%rbp), %rsi
	movl	$10, %ecx
	leaq	.LC9(%rip), %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L39
	movq	bio_err(%rip), %rdi
	leaq	.LC10(%rip), %rsi
	call	BIO_printf@PLT
	movl	$415, %edi
.L38:
	call	EC_GROUP_new_by_curve_name@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L137
	movl	-72(%rbp), %esi
	movq	%rax, %rdi
	call	EC_GROUP_set_asn1_flag@PLT
	movl	-136(%rbp), %esi
	movq	%rbx, %rdi
	call	EC_GROUP_set_point_conversion_form@PLT
.L41:
	movl	-60(%rbp), %eax
	testl	%eax, %eax
	jne	.L138
.L44:
	movl	-160(%rbp), %eax
	testl	%eax, %eax
	jne	.L139
.L45:
	movl	-144(%rbp), %eax
	testl	%eax, %eax
	jne	.L140
.L46:
	testl	%r13d, %r13d
	jne	.L47
.L50:
	movl	-120(%rbp), %r13d
	testl	%r13d, %r13d
	jne	.L141
.L49:
	testl	%r12d, %r12d
	jne	.L142
	movq	$0, -120(%rbp)
	xorl	%r13d, %r13d
	xorl	%r10d, %r10d
	xorl	%r11d, %r11d
	movq	$0, -112(%rbp)
	movq	$0, -96(%rbp)
	movq	$0, -104(%rbp)
.L52:
	cmpl	$4, -64(%rbp)
	je	.L143
	movl	-152(%rbp), %eax
	testl	%eax, %eax
	je	.L144
.L70:
	movl	-128(%rbp), %r9d
	testl	%r9d, %r9d
	jne	.L62
.L130:
	xorl	%r12d, %r12d
	jmp	.L24
.L16:
	movl	$1, %r13d
	jmp	.L2
.L19:
	call	opt_arg@PLT
	leaq	-64(%rbp), %rdx
	movl	$2, %esi
	movq	%rax, %rdi
	call	opt_format@PLT
	testl	%eax, %eax
	jne	.L2
.L22:
	movq	bio_err(%rip), %rdi
	movq	%r14, %rdx
	leaq	.LC1(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L27:
	movl	$1, %r12d
	xorl	%r13d, %r13d
	xorl	%ebx, %ebx
	xorl	%r14d, %r14d
	xorl	%r15d, %r15d
.L129:
	movq	$0, -120(%rbp)
	xorl	%r10d, %r10d
	xorl	%r11d, %r11d
	movq	$0, -112(%rbp)
	movq	$0, -96(%rbp)
	movq	$0, -104(%rbp)
.L24:
	movq	%r10, %rdi
	movq	%r11, -128(%rbp)
	call	BN_free@PLT
	movq	-112(%rbp), %rdi
	call	BN_free@PLT
	movq	-120(%rbp), %rdi
	call	BN_free@PLT
	movq	-104(%rbp), %rdi
	call	BN_free@PLT
	movq	-96(%rbp), %rdi
	call	BN_free@PLT
	movq	-128(%rbp), %r11
	movq	%r11, %rdi
	call	BN_free@PLT
	movl	$438, %edx
	leaq	.LC3(%rip), %rsi
	movq	%r13, %rdi
	call	CRYPTO_free@PLT
	movq	%rbx, %rdi
	call	EC_GROUP_free@PLT
	movq	-88(%rbp), %rdi
	call	release_engine@PLT
	movq	%r15, %rdi
	call	BIO_free@PLT
	movq	%r14, %rdi
	call	BIO_free_all@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L145
	leaq	-40(%rbp), %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	.cfi_restore_state
	leal	-1501(%rax), %edx
	cmpl	$1, %edx
	ja	.L2
	movl	%eax, %edi
	call	opt_rand@PLT
	testl	%eax, %eax
	jne	.L2
	jmp	.L27
.L6:
	call	opt_arg@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	setup_engine@PLT
	movq	%rax, -88(%rbp)
	jmp	.L2
.L8:
	movl	$1, -128(%rbp)
	jmp	.L2
.L9:
	call	opt_arg@PLT
	leaq	-72(%rbp), %rdx
	leaq	encodings(%rip), %rsi
	movq	%rax, %rdi
	call	opt_pair@PLT
	testl	%eax, %eax
	je	.L22
	movl	$1, -160(%rbp)
	jmp	.L2
.L10:
	call	opt_arg@PLT
	leaq	-60(%rbp), %rdx
	leaq	forms(%rip), %rsi
	movq	%rax, %rdi
	call	opt_pair@PLT
	testl	%eax, %eax
	je	.L22
	movl	-60(%rbp), %eax
	movl	$1, -60(%rbp)
	movl	%eax, -136(%rbp)
	jmp	.L2
.L11:
	call	opt_arg@PLT
	movq	%rax, -96(%rbp)
	jmp	.L2
.L12:
	movl	$1, -152(%rbp)
	jmp	.L2
.L13:
	movl	$1, -144(%rbp)
	jmp	.L2
.L14:
	movl	$1, -104(%rbp)
	jmp	.L2
.L15:
	movl	$1, -120(%rbp)
	jmp	.L2
.L17:
	call	opt_arg@PLT
	movq	%rax, -112(%rbp)
	jmp	.L2
.L18:
	call	opt_arg@PLT
	movq	%rax, %r15
	jmp	.L2
.L20:
	call	opt_arg@PLT
	leaq	-68(%rbp), %rdx
	movl	$2, %esi
	movq	%rax, %rdi
	call	opt_format@PLT
	testl	%eax, %eax
	jne	.L2
	jmp	.L22
.L21:
	leaq	ecparam_options(%rip), %rdi
	xorl	%r12d, %r12d
	xorl	%r13d, %r13d
	xorl	%ebx, %ebx
	call	opt_help@PLT
	xorl	%r14d, %r14d
	xorl	%r15d, %r15d
	jmp	.L129
.L73:
	xorl	%r13d, %r13d
	xorl	%ebx, %ebx
.L131:
	movq	$0, -120(%rbp)
	xorl	%r10d, %r10d
	xorl	%r11d, %r11d
	movl	$1, %r12d
	movq	$0, -112(%rbp)
	movq	$0, -96(%rbp)
	movq	$0, -104(%rbp)
	jmp	.L24
.L135:
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	EC_get_builtin_curves@PLT
	leaq	.LC2(%rip), %rsi
	movl	%eax, %edi
	movq	%rax, -112(%rbp)
	sall	$4, %edi
	call	app_malloc@PLT
	movq	-112(%rbp), %rdx
	movq	%rax, %rdi
	movq	%rax, -96(%rbp)
	movq	%rax, %rbx
	movq	%rdx, %rsi
	call	EC_get_builtin_curves@PLT
	testq	%rax, %rax
	je	.L29
	movq	-112(%rbp), %rdx
	movq	%rbx, %rax
	movq	%rbx, %r12
	movq	%rdx, %rbx
	salq	$4, %rbx
	addq	%rax, %rbx
	testq	%rdx, %rdx
	je	.L31
.L35:
	movl	(%r12), %edi
	movq	8(%r12), %r13
	call	OBJ_nid2sn@PLT
	testq	%r13, %r13
	movq	%rax, %rdx
	leaq	.LC0(%rip), %rax
	cmove	%rax, %r13
	testq	%rdx, %rdx
	je	.L146
	leaq	.LC5(%rip), %rsi
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	addq	$16, %r12
	movq	%r13, %rdx
	movq	%r14, %rdi
	leaq	.LC6(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	cmpq	%r12, %rbx
	jne	.L35
.L31:
	movq	-96(%rbp), %rdi
	movl	$199, %edx
	xorl	%r12d, %r12d
	xorl	%r13d, %r13d
	leaq	.LC3(%rip), %rsi
	xorl	%ebx, %ebx
	call	CRYPTO_free@PLT
	jmp	.L129
.L146:
	leaq	.LC4(%rip), %rdx
	movq	%r14, %rdi
	xorl	%eax, %eax
	addq	$16, %r12
	leaq	.LC5(%rip), %rsi
	call	BIO_printf@PLT
	movq	%r13, %rdx
	movq	%r14, %rdi
	xorl	%eax, %eax
	leaq	.LC6(%rip), %rsi
	call	BIO_printf@PLT
	cmpq	%rbx, %r12
	jne	.L35
	jmp	.L31
.L36:
	xorl	%ecx, %ecx
	cmpl	$4, -68(%rbp)
	je	.L147
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	PEM_read_bio_ECPKParameters@PLT
	movq	%rax, %rbx
.L43:
	testq	%rbx, %rbx
	jne	.L41
	movq	bio_err(%rip), %rdi
	leaq	.LC13(%rip), %rsi
	xorl	%eax, %eax
	xorl	%r13d, %r13d
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	jmp	.L131
.L39:
	movq	-96(%rbp), %rdi
	call	OBJ_sn2nid@PLT
	movl	%eax, %edi
	testl	%eax, %eax
	jne	.L38
	movq	-96(%rbp), %rbx
	movq	%rbx, %rdi
	call	EC_curve_nist2nid@PLT
	movl	%eax, %edi
	testl	%eax, %eax
	jne	.L38
	movq	bio_err(%rip), %rdi
	movq	%rbx, %rdx
	xorl	%eax, %eax
	xorl	%r13d, %r13d
	leaq	.LC11(%rip), %rsi
	movl	$1, %r12d
	xorl	%ebx, %ebx
	call	BIO_printf@PLT
	jmp	.L129
.L136:
	movq	bio_err(%rip), %rdi
	leaq	.LC8(%rip), %rsi
	call	BIO_printf@PLT
	movl	$409, %edi
	jmp	.L38
.L141:
	movq	bio_err(%rip), %rdi
	leaq	.LC14(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	EC_GROUP_check@PLT
	testl	%eax, %eax
	je	.L148
	movq	bio_err(%rip), %rdi
	leaq	.LC16(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L49
.L47:
	xorl	%edx, %edx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	ECPKParameters_print@PLT
	testl	%eax, %eax
	jne	.L50
	movl	%r13d, %r12d
.L132:
	xorl	%r13d, %r13d
	jmp	.L129
.L140:
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	EC_GROUP_set_seed@PLT
	jmp	.L46
.L139:
	movl	-72(%rbp), %esi
	movq	%rbx, %rdi
	call	EC_GROUP_set_asn1_flag@PLT
	jmp	.L45
.L138:
	movl	-136(%rbp), %esi
	movq	%rbx, %rdi
	call	EC_GROUP_set_point_conversion_form@PLT
	jmp	.L44
.L142:
	movq	%rbx, %rdi
	call	EC_GROUP_method_of@PLT
	movq	%rax, %r13
	call	BN_new@PLT
	movq	%rax, %r10
	testq	%rax, %rax
	je	.L55
	movq	%rax, -96(%rbp)
	call	BN_new@PLT
	movq	-96(%rbp), %r10
	testq	%rax, %rax
	movq	%rax, -112(%rbp)
	je	.L55
	call	BN_new@PLT
	movq	-96(%rbp), %r10
	testq	%rax, %rax
	movq	%rax, -120(%rbp)
	je	.L76
	call	BN_new@PLT
	movq	-96(%rbp), %r10
	testq	%rax, %rax
	movq	%rax, -104(%rbp)
	je	.L77
	movq	%r10, -144(%rbp)
	call	BN_new@PLT
	movq	-144(%rbp), %r10
	testq	%rax, %rax
	movq	%rax, -96(%rbp)
	je	.L54
	call	BN_new@PLT
	movq	-144(%rbp), %r10
	testq	%rax, %rax
	je	.L54
	movq	%r13, %rdi
	movq	%r10, -160(%rbp)
	movq	%rax, -144(%rbp)
	call	EC_METHOD_get_field_type@PLT
	movq	-144(%rbp), %r11
	movq	-160(%rbp), %r10
	cmpl	$406, %eax
	je	.L57
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
	movq	%r11, -128(%rbp)
	xorl	%r13d, %r13d
	leaq	.LC18(%rip), %rsi
	movq	%r10, -136(%rbp)
	call	BIO_printf@PLT
	movq	-128(%rbp), %r11
	movq	-136(%rbp), %r10
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L29:
	movq	-96(%rbp), %rdi
	movl	$181, %edx
	xorl	%r13d, %r13d
	xorl	%ebx, %ebx
	leaq	.LC3(%rip), %rsi
	call	CRYPTO_free@PLT
	movl	-104(%rbp), %r12d
	jmp	.L129
.L55:
	movq	$0, -120(%rbp)
	movq	$0, -112(%rbp)
	movq	$0, -96(%rbp)
	movq	$0, -104(%rbp)
.L54:
	leaq	.LC17(%rip), %rdi
	movq	%r10, -128(%rbp)
	xorl	%r13d, %r13d
	call	perror@PLT
	movq	-128(%rbp), %r10
	xorl	%r11d, %r11d
	jmp	.L24
.L143:
	movl	-128(%rbp), %r12d
	testl	%r12d, %r12d
	jne	.L62
	movl	-152(%rbp), %edx
	testl	%edx, %edx
	jne	.L130
	movq	i2d_ECPKParameters@GOTPCREL(%rip), %rdi
	movq	%rbx, %rdx
	movq	%r14, %rsi
	xorl	%r12d, %r12d
	movq	%r10, -136(%rbp)
	movq	%r11, -128(%rbp)
	call	ASN1_i2d_bio@PLT
	movq	-128(%rbp), %r11
	movq	-136(%rbp), %r10
	testl	%eax, %eax
	jne	.L24
.L61:
	movq	bio_err(%rip), %rdi
	leaq	.LC40(%rip), %rsi
	xorl	%eax, %eax
	movq	%r10, -136(%rbp)
	movq	%r11, -128(%rbp)
	movl	$1, %r12d
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	movq	-128(%rbp), %r11
	movq	-136(%rbp), %r10
	jmp	.L24
.L147:
	movq	d2i_ECPKParameters@GOTPCREL(%rip), %rsi
	movq	%r15, %rdx
	xorl	%edi, %edi
	call	ASN1_d2i_bio@PLT
	movq	%rax, %rbx
	jmp	.L43
.L62:
	movq	%r10, -152(%rbp)
	movl	$1, %r12d
	movq	%r11, -144(%rbp)
	call	EC_KEY_new@PLT
	movq	-144(%rbp), %r11
	movq	-152(%rbp), %r10
	testq	%rax, %rax
	movq	%rax, -128(%rbp)
	je	.L24
	movq	%rax, %rdi
	movq	%rbx, %rsi
	movq	%r10, -152(%rbp)
	movq	%r11, -144(%rbp)
	call	EC_KEY_set_group@PLT
	movq	-144(%rbp), %r11
	movq	-152(%rbp), %r10
	testl	%eax, %eax
	je	.L149
	movl	-60(%rbp), %r8d
	testl	%r8d, %r8d
	jne	.L150
.L64:
	movq	-128(%rbp), %rdi
	movq	%r10, -144(%rbp)
	movq	%r11, -136(%rbp)
	call	EC_KEY_generate_key@PLT
	movq	-136(%rbp), %r11
	movq	-144(%rbp), %r10
	testl	%eax, %eax
	je	.L151
	cmpl	$4, -64(%rbp)
	movq	%r10, -144(%rbp)
	movq	%r11, -136(%rbp)
	je	.L152
	pushq	%rcx
	movq	-128(%rbp), %rsi
	movq	%r14, %rdi
	xorl	%r9d, %r9d
	pushq	$0
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	call	PEM_write_bio_ECPrivateKey@PLT
	movq	-144(%rbp), %r10
	movq	-136(%rbp), %r11
	popq	%rsi
	popq	%rdi
.L67:
	movq	-128(%rbp), %rdi
	movq	%r10, -144(%rbp)
	xorl	%r12d, %r12d
	movq	%r11, -136(%rbp)
	call	EC_KEY_free@PLT
	movq	-136(%rbp), %r11
	movq	-144(%rbp), %r10
	jmp	.L24
.L144:
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movq	%r10, -152(%rbp)
	movq	%r11, -144(%rbp)
	call	PEM_write_bio_ECPKParameters@PLT
	movq	-144(%rbp), %r11
	movq	-152(%rbp), %r10
	testl	%eax, %eax
	jne	.L70
	jmp	.L61
.L137:
	movq	-96(%rbp), %rdx
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
	xorl	%r13d, %r13d
	leaq	.LC12(%rip), %rsi
	call	BIO_printf@PLT
	jmp	.L131
.L148:
	movq	bio_err(%rip), %rdi
	leaq	.LC15(%rip), %rsi
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	movl	-120(%rbp), %r12d
	jmp	.L132
.L77:
	movq	$0, -96(%rbp)
	jmp	.L54
.L76:
	movq	$0, -96(%rbp)
	movq	$0, -104(%rbp)
	jmp	.L54
.L145:
	call	__stack_chk_fail@PLT
.L152:
	movq	-128(%rbp), %rsi
	movq	%r14, %rdi
	call	i2d_ECPrivateKey_bio@PLT
	movq	-136(%rbp), %r11
	movq	-144(%rbp), %r10
	jmp	.L67
.L150:
	movl	-136(%rbp), %esi
	movq	-128(%rbp), %rdi
	movq	%r10, -152(%rbp)
	movq	%r11, -144(%rbp)
	call	EC_KEY_set_conv_form@PLT
	movq	-152(%rbp), %r10
	movq	-144(%rbp), %r11
	jmp	.L64
.L149:
	movq	bio_err(%rip), %rdi
	leaq	.LC41(%rip), %rsi
	movq	%r10, -144(%rbp)
	movq	%r11, -136(%rbp)
	call	BIO_printf@PLT
	movq	-128(%rbp), %rdi
	call	EC_KEY_free@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	movq	-136(%rbp), %r11
	movq	-144(%rbp), %r10
	jmp	.L24
.L57:
	movq	-120(%rbp), %rcx
	movq	-112(%rbp), %rdx
	xorl	%r8d, %r8d
	movq	%r10, %rsi
	movq	%rbx, %rdi
	movq	%r11, -160(%rbp)
	xorl	%r13d, %r13d
	movq	%r10, -144(%rbp)
	call	EC_GROUP_get_curve@PLT
	movq	-144(%rbp), %r10
	movq	-160(%rbp), %r11
	testl	%eax, %eax
	je	.L24
	movq	%rbx, %rdi
	movq	%r10, -160(%rbp)
	movq	%r11, -144(%rbp)
	call	EC_GROUP_get0_generator@PLT
	movq	-144(%rbp), %r11
	movq	-160(%rbp), %r10
	testq	%rax, %rax
	movq	%rax, %r13
	je	.L24
	movq	%rbx, %rdi
	call	EC_GROUP_get_point_conversion_form@PLT
	movq	-104(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r13, %rsi
	movl	%eax, %edx
	movq	%rbx, %rdi
	call	EC_POINT_point2bn@PLT
	movq	-144(%rbp), %r11
	movq	-160(%rbp), %r10
	testq	%rax, %rax
	movq	%rax, %r13
	je	.L24
	movq	-96(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	xorl	%r13d, %r13d
	call	EC_GROUP_get_order@PLT
	movq	-144(%rbp), %r11
	movq	-160(%rbp), %r10
	testl	%eax, %eax
	je	.L24
	xorl	%edx, %edx
	movq	%r11, %rsi
	movq	%rbx, %rdi
	call	EC_GROUP_get_cofactor@PLT
	movq	-144(%rbp), %r11
	movq	-160(%rbp), %r10
	testl	%eax, %eax
	je	.L24
	movq	-96(%rbp), %rdi
	movq	%r11, -176(%rbp)
	movq	%r10, -144(%rbp)
	call	BN_num_bits@PLT
	movq	-144(%rbp), %r10
	movl	%eax, %r12d
	movq	%r10, %rdi
	movq	%r10, -168(%rbp)
	call	BN_num_bits@PLT
	movq	-112(%rbp), %rdi
	movl	%eax, %r13d
	call	BN_num_bits@PLT
	movq	-120(%rbp), %rdi
	movl	%eax, %r8d
	leal	7(%r13), %eax
	movl	$8, %r13d
	cltd
	idivl	%r13d
	movslq	%eax, %rcx
	leal	7(%r8), %eax
	cltd
	idivl	%r13d
	cltq
	cmpq	%rax, %rcx
	cmovb	%rax, %rcx
	movq	%rcx, -144(%rbp)
	call	BN_num_bits@PLT
	movq	-144(%rbp), %rcx
	movq	-104(%rbp), %rdi
	addl	$7, %eax
	cltd
	idivl	%r13d
	cltq
	cmpq	%rcx, %rax
	cmovnb	%rax, %rcx
	movq	%rcx, -144(%rbp)
	call	BN_num_bits@PLT
	movq	-144(%rbp), %rcx
	movq	-96(%rbp), %rdi
	addl	$7, %eax
	cltd
	idivl	%r13d
	cltq
	cmpq	%rcx, %rax
	cmovnb	%rax, %rcx
	movq	%rcx, -144(%rbp)
	call	BN_num_bits@PLT
	movq	-144(%rbp), %rcx
	movq	-176(%rbp), %r11
	addl	$7, %eax
	cltd
	movq	%r11, %rdi
	movq	%r11, -144(%rbp)
	idivl	%r13d
	cltq
	cmpq	%rcx, %rax
	cmovnb	%rax, %rcx
	movq	%rcx, -160(%rbp)
	call	BN_num_bits@PLT
	movq	-160(%rbp), %rcx
	leaq	.LC19(%rip), %rsi
	addl	$7, %eax
	cltd
	idivl	%r13d
	cltq
	cmpq	%rcx, %rax
	cmovb	%rcx, %rax
	movl	%eax, %edi
	call	app_malloc@PLT
	movl	%r12d, %edx
	leaq	.LC20(%rip), %rsi
	movq	%r14, %rdi
	movq	%rax, %r13
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	%r13, %r8
	movl	%r12d, %ecx
	movq	%r14, %rdi
	movq	-168(%rbp), %r10
	leaq	.LC21(%rip), %rdx
	movq	%r10, %rsi
	movq	%r10, -160(%rbp)
	call	print_bignum_var@PLT
	movq	-112(%rbp), %rsi
	movq	%r13, %r8
	movl	%r12d, %ecx
	leaq	.LC22(%rip), %rdx
	movq	%r14, %rdi
	call	print_bignum_var@PLT
	movq	-120(%rbp), %rsi
	movq	%r13, %r8
	movl	%r12d, %ecx
	leaq	.LC23(%rip), %rdx
	movq	%r14, %rdi
	call	print_bignum_var@PLT
	movq	-104(%rbp), %rsi
	movq	%r13, %r8
	movl	%r12d, %ecx
	leaq	.LC24(%rip), %rdx
	movq	%r14, %rdi
	call	print_bignum_var@PLT
	movq	-96(%rbp), %rsi
	movq	%r13, %r8
	movl	%r12d, %ecx
	leaq	.LC25(%rip), %rdx
	movq	%r14, %rdi
	call	print_bignum_var@PLT
	movq	%r13, %r8
	movl	%r12d, %ecx
	movq	%r14, %rdi
	movq	-144(%rbp), %r11
	leaq	.LC26(%rip), %rdx
	movq	%r11, %rsi
	call	print_bignum_var@PLT
	leaq	.LC27(%rip), %rsi
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movl	%r12d, %ecx
	movl	%r12d, %edx
	movq	%r14, %rdi
	leaq	.LC28(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movl	%r12d, %ecx
	movl	%r12d, %edx
	movq	%r14, %rdi
	leaq	.LC29(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movl	%r12d, %ecx
	movl	%r12d, %edx
	movq	%r14, %rdi
	leaq	.LC30(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	leaq	.LC31(%rip), %rsi
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	leaq	.LC32(%rip), %rsi
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movl	%r12d, %ecx
	movl	%r12d, %edx
	movq	%r14, %rdi
	leaq	.LC33(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	leaq	.LC34(%rip), %rsi
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	leaq	.LC35(%rip), %rsi
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movl	%r12d, %ecx
	movl	%r12d, %edx
	movq	%r14, %rdi
	leaq	.LC36(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movl	%r12d, %ecx
	movl	%r12d, %edx
	movq	%r14, %rdi
	leaq	.LC37(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	leaq	.LC38(%rip), %rsi
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	leaq	.LC39(%rip), %rsi
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	-144(%rbp), %r11
	movq	-160(%rbp), %r10
	jmp	.L52
.L151:
	movq	bio_err(%rip), %rdi
	leaq	.LC42(%rip), %rsi
	movl	$1, %r12d
	call	BIO_printf@PLT
	movq	-128(%rbp), %rdi
	call	EC_KEY_free@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	movq	-136(%rbp), %r11
	movq	-144(%rbp), %r10
	jmp	.L24
	.cfi_endproc
.LFE1435:
	.size	ecparam_main, .-ecparam_main
	.section	.rodata.str1.1
.LC43:
	.string	"named_curve"
.LC44:
	.string	"explicit"
	.section	.data.rel.local,"aw"
	.align 32
	.type	encodings, @object
	.size	encodings, 48
encodings:
	.quad	.LC43
	.long	1
	.zero	4
	.quad	.LC44
	.long	0
	.zero	4
	.quad	0
	.zero	8
	.section	.rodata.str1.1
.LC45:
	.string	"compressed"
.LC46:
	.string	"uncompressed"
.LC47:
	.string	"hybrid"
	.section	.data.rel.local
	.align 32
	.type	forms, @object
	.size	forms, 64
forms:
	.quad	.LC45
	.long	2
	.zero	4
	.quad	.LC46
	.long	4
	.zero	4
	.quad	.LC47
	.long	6
	.zero	4
	.quad	0
	.zero	8
	.globl	ecparam_options
	.section	.rodata.str1.1
.LC48:
	.string	"help"
.LC49:
	.string	"Display this summary"
.LC50:
	.string	"inform"
	.section	.rodata.str1.8
	.align 8
.LC51:
	.string	"Input format - default PEM (DER or PEM)"
	.section	.rodata.str1.1
.LC52:
	.string	"outform"
.LC53:
	.string	"Output format - default PEM"
.LC54:
	.string	"in"
.LC55:
	.string	"Input file  - default stdin"
.LC56:
	.string	"out"
.LC57:
	.string	"Output file - default stdout"
.LC58:
	.string	"text"
	.section	.rodata.str1.8
	.align 8
.LC59:
	.string	"Print the ec parameters in text form"
	.section	.rodata.str1.1
.LC60:
	.string	"C"
	.section	.rodata.str1.8
	.align 8
.LC61:
	.string	"Print a 'C' function creating the parameters"
	.section	.rodata.str1.1
.LC62:
	.string	"check"
.LC63:
	.string	"Validate the ec parameters"
.LC64:
	.string	"list_curves"
	.section	.rodata.str1.8
	.align 8
.LC65:
	.string	"Prints a list of all curve 'short names'"
	.section	.rodata.str1.1
.LC66:
	.string	"no_seed"
	.section	.rodata.str1.8
	.align 8
.LC67:
	.string	"If 'explicit' parameters are chosen do not use the seed"
	.section	.rodata.str1.1
.LC68:
	.string	"noout"
.LC69:
	.string	"Do not print the ec parameter"
.LC70:
	.string	"name"
	.section	.rodata.str1.8
	.align 8
.LC71:
	.string	"Use the ec parameters with specified 'short name'"
	.section	.rodata.str1.1
.LC72:
	.string	"conv_form"
	.section	.rodata.str1.8
	.align 8
.LC73:
	.string	"Specifies the point conversion form "
	.section	.rodata.str1.1
.LC74:
	.string	"param_enc"
	.section	.rodata.str1.8
	.align 8
.LC75:
	.string	"Specifies the way the ec parameters are encoded"
	.section	.rodata.str1.1
.LC76:
	.string	"genkey"
.LC77:
	.string	"Generate ec key"
.LC78:
	.string	"rand"
	.section	.rodata.str1.8
	.align 8
.LC79:
	.string	"Load the file(s) into the random number generator"
	.section	.rodata.str1.1
.LC80:
	.string	"writerand"
	.section	.rodata.str1.8
	.align 8
.LC81:
	.string	"Write random data to the specified file"
	.section	.rodata.str1.1
.LC82:
	.string	"engine"
	.section	.rodata.str1.8
	.align 8
.LC83:
	.string	"Use engine, possibly a hardware device"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	ecparam_options, @object
	.size	ecparam_options, 456
ecparam_options:
	.quad	.LC48
	.long	1
	.long	45
	.quad	.LC49
	.quad	.LC50
	.long	2
	.long	70
	.quad	.LC51
	.quad	.LC52
	.long	3
	.long	70
	.quad	.LC53
	.quad	.LC54
	.long	4
	.long	60
	.quad	.LC55
	.quad	.LC56
	.long	5
	.long	62
	.quad	.LC57
	.quad	.LC58
	.long	6
	.long	45
	.quad	.LC59
	.quad	.LC60
	.long	7
	.long	45
	.quad	.LC61
	.quad	.LC62
	.long	8
	.long	45
	.quad	.LC63
	.quad	.LC64
	.long	9
	.long	45
	.quad	.LC65
	.quad	.LC66
	.long	10
	.long	45
	.quad	.LC67
	.quad	.LC68
	.long	11
	.long	45
	.quad	.LC69
	.quad	.LC70
	.long	12
	.long	115
	.quad	.LC71
	.quad	.LC72
	.long	13
	.long	115
	.quad	.LC73
	.quad	.LC74
	.long	14
	.long	115
	.quad	.LC75
	.quad	.LC76
	.long	15
	.long	45
	.quad	.LC77
	.quad	.LC78
	.long	1501
	.long	115
	.quad	.LC79
	.quad	.LC80
	.long	1502
	.long	62
	.quad	.LC81
	.quad	.LC82
	.long	16
	.long	115
	.quad	.LC83
	.quad	0
	.zero	16
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
