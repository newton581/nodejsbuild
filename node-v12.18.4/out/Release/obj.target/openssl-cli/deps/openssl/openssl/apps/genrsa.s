	.file	"genrsa.c"
	.text
	.p2align 4
	.type	genrsa_cb, @function
genrsa_cb:
.LFB1436:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movb	$42, -25(%rbp)
	testl	%edi, %edi
	jne	.L2
	movb	$46, -25(%rbp)
.L6:
	movq	%r12, %rdi
	call	BN_GENCB_get_arg@PLT
	leaq	-25(%rbp), %rsi
	movl	$1, %edx
	movq	%rax, %rdi
	call	BIO_write@PLT
	movq	%r12, %rdi
	call	BN_GENCB_get_arg@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	movq	%rax, %rdi
	call	BIO_ctrl@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L10
	addq	$24, %rsp
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	.cfi_restore_state
	cmpl	$1, %edi
	jne	.L11
	movb	$43, -25(%rbp)
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L11:
	cmpl	$2, %edi
	je	.L6
	cmpl	$3, %edi
	jne	.L6
	movb	$10, -25(%rbp)
	jmp	.L6
.L10:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1436:
	.size	genrsa_cb, .-genrsa_cb
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"%s: Use -help for summary.\n"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"Warning: It is not recommended to use more than %d bit for RSA keys.\n         Your key size is %d! Larger key size may behave not as expected.\n"
	.section	.rodata.str1.1
.LC2:
	.string	"Extra arguments given.\n"
.LC3:
	.string	"Error getting password\n"
	.section	.rodata.str1.8
	.align 8
.LC4:
	.string	"Generating RSA private key, %d bit long modulus (%d primes)\n"
	.section	.rodata.str1.1
.LC5:
	.string	"e is %s (0x%s)\n"
	.section	.rodata.str1.8
	.align 8
.LC6:
	.string	"../deps/openssl/openssl/apps/genrsa.c"
	.text
	.p2align 4
	.globl	genrsa_main
	.type	genrsa_main, @function
genrsa_main:
.LFB1435:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%edi, %r12d
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	BN_GENCB_new@PLT
	movq	%rax, %r13
	call	BN_new@PLT
	movq	$0, -96(%rbp)
	movl	$2048, -112(%rbp)
	movq	%rax, %r14
	movl	$2, -108(%rbp)
	movq	$0, -88(%rbp)
	testq	%rax, %rax
	je	.L39
	testq	%r13, %r13
	je	.L39
	movq	bio_err(%rip), %rdx
	leaq	genrsa_cb(%rip), %rsi
	movq	%r13, %rdi
	leaq	.L19(%rip), %rbx
	call	BN_GENCB_set@PLT
	movq	%r15, %rsi
	movl	%r12d, %edi
	leaq	genrsa_options(%rip), %rdx
	call	opt_init@PLT
	movq	$0, -120(%rbp)
	movl	$65537, %r12d
	xorl	%r15d, %r15d
	movq	%rax, -136(%rbp)
	movq	$0, -128(%rbp)
.L14:
	call	opt_next@PLT
	testl	%eax, %eax
	je	.L67
	cmpl	$8, %eax
	jg	.L15
	cmpl	$-1, %eax
	jl	.L14
	addl	$1, %eax
	cmpl	$9, %eax
	ja	.L14
	movslq	(%rbx,%rax,4), %rax
	addq	%rbx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L19:
	.long	.L26-.L19
	.long	.L14-.L19
	.long	.L25-.L19
	.long	.L24-.L19
	.long	.L40-.L19
	.long	.L23-.L19
	.long	.L22-.L19
	.long	.L21-.L19
	.long	.L20-.L19
	.long	.L18-.L19
	.text
	.p2align 4,,10
	.p2align 3
.L39:
	xorl	%r10d, %r10d
	xorl	%ebx, %ebx
	xorl	%r15d, %r15d
.L13:
	movq	%r14, %rdi
	movq	%r10, -120(%rbp)
	call	BN_free@PLT
	movq	%r13, %rdi
	call	BN_GENCB_free@PLT
	movq	-120(%rbp), %r10
	movq	%r10, %rdi
	call	RSA_free@PLT
	movq	%rbx, %rdi
	call	BIO_free_all@PLT
	movq	%r15, %rdi
	call	release_engine@PLT
	movq	-88(%rbp), %rdi
	movl	$175, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	movl	$1, %eax
.L12:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L68
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L40:
	.cfi_restore_state
	movl	$65537, %r12d
	jmp	.L14
.L24:
	movl	$3, %r12d
	jmp	.L14
.L70:
	movq	bio_err(%rip), %rdi
	leaq	.LC2(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L26:
	movq	-136(%rbp), %rdx
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
	xorl	%ebx, %ebx
	leaq	.LC0(%rip), %rsi
	call	BIO_printf@PLT
	xorl	%r10d, %r10d
	jmp	.L13
.L25:
	leaq	genrsa_options(%rip), %rdi
	xorl	%ebx, %ebx
	call	opt_help@PLT
	xorl	%r10d, %r10d
.L28:
	movq	%r14, %rdi
	movq	%r10, -120(%rbp)
	call	BN_free@PLT
	movq	%r13, %rdi
	call	BN_GENCB_free@PLT
	movq	-120(%rbp), %r10
	movq	%r10, %rdi
	call	RSA_free@PLT
	movq	%rbx, %rdi
	call	BIO_free_all@PLT
	movq	%r15, %rdi
	call	release_engine@PLT
	movq	-88(%rbp), %rdi
	movl	$175, %edx
	leaq	.LC6(%rip), %rsi
	call	CRYPTO_free@PLT
	xorl	%eax, %eax
	jmp	.L12
.L18:
	call	opt_arg@PLT
	leaq	-108(%rbp), %rsi
	movq	%rax, %rdi
	call	opt_int@PLT
	testl	%eax, %eax
	jne	.L14
.L45:
	xorl	%r10d, %r10d
	xorl	%ebx, %ebx
	jmp	.L13
.L20:
	call	opt_unknown@PLT
	leaq	-96(%rbp), %rsi
	movq	%rax, %rdi
	call	opt_cipher@PLT
	testl	%eax, %eax
	jne	.L14
	jmp	.L45
.L21:
	call	opt_arg@PLT
	movq	%rax, -120(%rbp)
	jmp	.L14
.L22:
	call	opt_arg@PLT
	movq	%rax, -128(%rbp)
	jmp	.L14
.L23:
	call	opt_arg@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	setup_engine@PLT
	movq	%rax, %r15
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L15:
	leal	-1501(%rax), %edx
	cmpl	$1, %edx
	ja	.L14
	movl	%eax, %edi
	call	opt_rand@PLT
	testl	%eax, %eax
	jne	.L14
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L67:
	call	opt_num_rest@PLT
	movl	%eax, %ebx
	call	opt_rest@PLT
	cmpl	$1, %ebx
	je	.L69
	testl	%ebx, %ebx
	jg	.L70
.L32:
	movq	-120(%rbp), %rsi
	xorl	%edx, %edx
	xorl	%edi, %edi
	leaq	-88(%rbp), %rcx
	call	app_passwd@PLT
	testl	%eax, %eax
	je	.L71
	movq	-128(%rbp), %rdi
	movl	$1, %edx
	movl	$32773, %esi
	call	bio_open_owner@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L46
	movl	-108(%rbp), %ecx
	movl	-112(%rbp), %edx
	xorl	%eax, %eax
	leaq	.LC4(%rip), %rsi
	movq	bio_err(%rip), %rdi
	call	BIO_printf@PLT
	testq	%r15, %r15
	je	.L34
	movq	%r15, %rdi
	call	RSA_new_method@PLT
	movq	%rax, %r10
.L35:
	testq	%r10, %r10
	je	.L13
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%r10, -120(%rbp)
	call	BN_set_word@PLT
	movq	-120(%rbp), %r10
	testl	%eax, %eax
	je	.L13
	movl	-108(%rbp), %edx
	movl	-112(%rbp), %esi
	movq	%r10, %rdi
	movq	%r13, %r8
	movq	%r14, %rcx
	call	RSA_generate_multi_prime_key@PLT
	movq	-120(%rbp), %r10
	testl	%eax, %eax
	je	.L13
	movq	%r10, %rdi
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	movq	%r10, -136(%rbp)
	leaq	-104(%rbp), %rdx
	call	RSA_get0_key@PLT
	movq	-104(%rbp), %rdi
	call	BN_bn2hex@PLT
	movq	-104(%rbp), %rdi
	movq	%rax, -120(%rbp)
	call	BN_bn2dec@PLT
	movq	-120(%rbp), %r8
	movq	-136(%rbp), %r10
	movq	%rax, %r12
	testq	%r8, %r8
	je	.L36
	testq	%rax, %rax
	je	.L36
	movq	bio_err(%rip), %rdi
	movq	%r8, %rcx
	movq	%rax, %rdx
	xorl	%eax, %eax
	leaq	.LC5(%rip), %rsi
	call	BIO_printf@PLT
	movq	-136(%rbp), %r10
	movq	-120(%rbp), %r8
.L36:
	movq	%r8, %rdi
	movl	$158, %edx
	leaq	.LC6(%rip), %rsi
	movq	%r10, -120(%rbp)
	call	CRYPTO_free@PLT
	movl	$159, %edx
	leaq	.LC6(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
	movq	-88(%rbp), %rax
	subq	$8, %rsp
	xorl	%ecx, %ecx
	movq	-120(%rbp), %r10
	movq	-96(%rbp), %rdx
	xorl	%r8d, %r8d
	movq	%rbx, %rdi
	movq	%rax, -80(%rbp)
	movq	-128(%rbp), %rax
	movq	password_callback@GOTPCREL(%rip), %r9
	movq	%r10, %rsi
	movq	%rax, -72(%rbp)
	leaq	-80(%rbp), %rax
	pushq	%rax
	call	PEM_write_bio_RSAPrivateKey@PLT
	popq	%rdx
	movq	-120(%rbp), %r10
	testl	%eax, %eax
	popq	%rcx
	jne	.L28
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L71:
	movq	bio_err(%rip), %rdi
	leaq	.LC3(%rip), %rsi
	xorl	%ebx, %ebx
	call	BIO_printf@PLT
	xorl	%r10d, %r10d
	jmp	.L13
.L69:
	movq	(%rax), %rdi
	leaq	-112(%rbp), %rsi
	call	opt_int@PLT
	testl	%eax, %eax
	je	.L45
	movl	-112(%rbp), %ecx
	testl	%ecx, %ecx
	jle	.L45
	cmpl	$16384, %ecx
	jle	.L32
	movq	bio_err(%rip), %rdi
	movl	$16384, %edx
	leaq	.LC1(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L32
.L46:
	xorl	%r10d, %r10d
	jmp	.L13
.L34:
	call	RSA_new@PLT
	movq	%rax, %r10
	jmp	.L35
.L68:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1435:
	.size	genrsa_main, .-genrsa_main
	.globl	genrsa_options
	.section	.rodata.str1.1
.LC7:
	.string	"help"
.LC8:
	.string	"Display this summary"
.LC9:
	.string	"3"
.LC10:
	.string	"Use 3 for the E value"
.LC11:
	.string	"F4"
	.section	.rodata.str1.8
	.align 8
.LC12:
	.string	"Use F4 (0x10001) for the E value"
	.section	.rodata.str1.1
.LC13:
	.string	"f4"
.LC14:
	.string	"out"
	.section	.rodata.str1.8
	.align 8
.LC15:
	.string	"Output the key to specified file"
	.section	.rodata.str1.1
.LC16:
	.string	"rand"
	.section	.rodata.str1.8
	.align 8
.LC17:
	.string	"Load the file(s) into the random number generator"
	.section	.rodata.str1.1
.LC18:
	.string	"writerand"
	.section	.rodata.str1.8
	.align 8
.LC19:
	.string	"Write random data to the specified file"
	.section	.rodata.str1.1
.LC20:
	.string	"passout"
	.section	.rodata.str1.8
	.align 8
.LC21:
	.string	"Output file pass phrase source"
	.section	.rodata.str1.1
.LC22:
	.string	""
	.section	.rodata.str1.8
	.align 8
.LC23:
	.string	"Encrypt the output with any supported cipher"
	.section	.rodata.str1.1
.LC24:
	.string	"engine"
	.section	.rodata.str1.8
	.align 8
.LC25:
	.string	"Use engine, possibly a hardware device"
	.section	.rodata.str1.1
.LC26:
	.string	"primes"
.LC27:
	.string	"Specify number of primes"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	genrsa_options, @object
	.size	genrsa_options, 288
genrsa_options:
	.quad	.LC7
	.long	1
	.long	45
	.quad	.LC8
	.quad	.LC9
	.long	2
	.long	45
	.quad	.LC10
	.quad	.LC11
	.long	3
	.long	45
	.quad	.LC12
	.quad	.LC13
	.long	3
	.long	45
	.quad	.LC12
	.quad	.LC14
	.long	5
	.long	62
	.quad	.LC15
	.quad	.LC16
	.long	1501
	.long	115
	.quad	.LC17
	.quad	.LC18
	.long	1502
	.long	62
	.quad	.LC19
	.quad	.LC20
	.long	6
	.long	115
	.quad	.LC21
	.quad	.LC22
	.long	7
	.long	45
	.quad	.LC23
	.quad	.LC24
	.long	4
	.long	115
	.quad	.LC25
	.quad	.LC26
	.long	8
	.long	112
	.quad	.LC27
	.quad	0
	.zero	16
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
