	.file	"pkeyparam.c"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"%s: Use -help for summary.\n"
.LC1:
	.string	"Error reading parameters\n"
.LC2:
	.string	"Parameters are valid\n"
.LC3:
	.string	"Parameters are invalid\n"
.LC4:
	.string	"Detailed error: %s\n"
	.text
	.p2align 4
	.globl	pkeyparam_main
	.type	pkeyparam_main, @function
pkeyparam_main:
.LFB1435:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	pkeyparam_options(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	xorl	%r14d, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	.L5(%rip), %rbx
	subq	$40, %rsp
	call	opt_init@PLT
	movq	$0, -56(%rbp)
	movl	$0, -60(%rbp)
	movq	%rax, %r13
	movl	$0, -64(%rbp)
.L2:
	call	opt_next@PLT
	testl	%eax, %eax
	je	.L33
.L14:
	addl	$1, %eax
	cmpl	$8, %eax
	ja	.L2
	movslq	(%rbx,%rax,4), %rax
	addq	%rbx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L5:
	.long	.L12-.L5
	.long	.L2-.L5
	.long	.L11-.L5
	.long	.L10-.L5
	.long	.L9-.L5
	.long	.L8-.L5
	.long	.L7-.L5
	.long	.L6-.L5
	.long	.L4-.L5
	.text
.L8:
	movl	$1, %r12d
	call	opt_next@PLT
	testl	%eax, %eax
	jne	.L14
.L33:
	call	opt_num_rest@PLT
	testl	%eax, %eax
	jne	.L12
	movq	%r15, %rdi
	movl	$32773, %edx
	movl	$114, %esi
	call	bio_open_default@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L23
	movq	-56(%rbp), %rdi
	movl	$32773, %edx
	movl	$119, %esi
	call	bio_open_default@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L24
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	PEM_read_bio_Parameters@PLT
	movq	%rax, %r9
	testq	%rax, %rax
	je	.L34
	movl	-60(%rbp), %edx
	testl	%edx, %edx
	jne	.L35
.L16:
	movl	-64(%rbp), %eax
	testl	%eax, %eax
	je	.L36
.L22:
	testl	%r12d, %r12d
	je	.L13
	movq	%r9, %rsi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	%r9, -56(%rbp)
	xorl	%r12d, %r12d
	call	EVP_PKEY_print_params@PLT
	movq	-56(%rbp), %r9
	jmp	.L13
.L12:
	movq	bio_err(%rip), %rdi
	movq	%r13, %rdx
	xorl	%eax, %eax
	xorl	%r13d, %r13d
	leaq	.LC0(%rip), %rsi
	movl	$1, %r12d
	xorl	%r15d, %r15d
	call	BIO_printf@PLT
	xorl	%r9d, %r9d
.L13:
	movq	%r9, %rdi
	call	EVP_PKEY_free@PLT
	movq	%r14, %rdi
	call	release_engine@PLT
	movq	%r13, %rdi
	call	BIO_free_all@PLT
	movq	%r15, %rdi
	call	BIO_free@PLT
	addq	$40, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L4:
	.cfi_restore_state
	movl	$1, -60(%rbp)
	jmp	.L2
.L6:
	call	opt_arg@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	setup_engine@PLT
	movq	%rax, %r14
	jmp	.L2
.L7:
	movl	$1, -64(%rbp)
	jmp	.L2
.L9:
	call	opt_arg@PLT
	movq	%rax, -56(%rbp)
	jmp	.L2
.L10:
	call	opt_arg@PLT
	movq	%rax, %r15
	jmp	.L2
.L11:
	leaq	pkeyparam_options(%rip), %rdi
	xorl	%r12d, %r12d
	xorl	%r13d, %r13d
	xorl	%r15d, %r15d
	call	opt_help@PLT
	xorl	%r9d, %r9d
	jmp	.L13
.L23:
	xorl	%r9d, %r9d
	xorl	%r13d, %r13d
	movl	$1, %r12d
	jmp	.L13
.L24:
	xorl	%r9d, %r9d
	movl	$1, %r12d
	jmp	.L13
.L35:
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -56(%rbp)
	call	EVP_PKEY_CTX_new@PLT
	movq	-56(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, -72(%rbp)
	je	.L37
	movq	%rax, %rdi
	movq	%r9, -56(%rbp)
	call	EVP_PKEY_param_check@PLT
	movq	-56(%rbp), %r9
	cmpl	$1, %eax
	je	.L38
	leaq	.LC3(%rip), %rsi
	movq	%r13, %rdi
	xorl	%eax, %eax
	movq	%r9, -56(%rbp)
	call	BIO_printf@PLT
	leaq	.LC4(%rip), %rbx
	movq	-56(%rbp), %r9
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L21:
	movq	%r9, -56(%rbp)
	call	ERR_reason_error_string@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	call	ERR_get_error@PLT
	movq	-56(%rbp), %r9
.L20:
	movq	%r9, -56(%rbp)
	call	ERR_peek_error@PLT
	movq	-56(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %rdi
	jne	.L21
.L19:
	movq	-72(%rbp), %rdi
	movq	%r9, -56(%rbp)
	call	EVP_PKEY_CTX_free@PLT
	movq	-56(%rbp), %r9
	jmp	.L16
.L36:
	movq	%r9, %rsi
	movq	%r13, %rdi
	movq	%r9, -56(%rbp)
	call	PEM_write_bio_Parameters@PLT
	movq	-56(%rbp), %r9
	jmp	.L22
.L34:
	movq	bio_err(%rip), %rdi
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	.LC1(%rip), %rsi
	movl	$1, %r12d
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	movq	-56(%rbp), %r9
	jmp	.L13
.L38:
	leaq	.LC2(%rip), %rsi
	movq	%r13, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	-56(%rbp), %r9
	jmp	.L19
.L37:
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	movl	-60(%rbp), %r12d
	movq	-56(%rbp), %r9
	jmp	.L13
	.cfi_endproc
.LFE1435:
	.size	pkeyparam_main, .-pkeyparam_main
	.globl	pkeyparam_options
	.section	.rodata.str1.1
.LC5:
	.string	"help"
.LC6:
	.string	"Display this summary"
.LC7:
	.string	"in"
.LC8:
	.string	"Input file"
.LC9:
	.string	"out"
.LC10:
	.string	"Output file"
.LC11:
	.string	"text"
.LC12:
	.string	"Print parameters as text"
.LC13:
	.string	"noout"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC14:
	.string	"Don't output encoded parameters"
	.section	.rodata.str1.1
.LC15:
	.string	"engine"
	.section	.rodata.str1.8
	.align 8
.LC16:
	.string	"Use engine, possibly a hardware device"
	.section	.rodata.str1.1
.LC17:
	.string	"check"
.LC18:
	.string	"Check key param consistency"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	pkeyparam_options, @object
	.size	pkeyparam_options, 192
pkeyparam_options:
	.quad	.LC5
	.long	1
	.long	45
	.quad	.LC6
	.quad	.LC7
	.long	2
	.long	60
	.quad	.LC8
	.quad	.LC9
	.long	3
	.long	62
	.quad	.LC10
	.quad	.LC11
	.long	4
	.long	45
	.quad	.LC12
	.quad	.LC13
	.long	5
	.long	45
	.quad	.LC14
	.quad	.LC15
	.long	6
	.long	115
	.quad	.LC16
	.quad	.LC17
	.long	7
	.long	45
	.quad	.LC18
	.quad	0
	.zero	16
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
