	.file	"s_cb.c"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"???"
.LC1:
	.string	"yes"
.LC2:
	.string	"no"
.LC3:
	.string	"Security callback: "
.LC4:
	.string	"Version=%s"
.LC5:
	.string	"%s="
.LC6:
	.string	"%d"
.LC7:
	.string	""
.LC8:
	.string	"%s, bits=%d"
.LC9:
	.string	"%s"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC10:
	.string	"s_cb.c:security_callback_debug op=0x%x"
	.section	.rodata.str1.1
.LC11:
	.string	" digest=%s, algorithm=%s"
.LC12:
	.string	" scheme=unknown(0x%04x)"
.LC13:
	.string	", security bits=%d"
.LC14:
	.string	": %s\n"
.LC15:
	.string	" scheme=%s"
	.text
	.p2align 4
	.type	security_callback_debug, @function
security_callback_debug:
.LFB1576:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%r8d, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r9, %r13
	pushq	%r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%ecx, -68(%rbp)
	movq	16(%rbp), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	pushq	%rbx
	call	*16(%rbx)
	popq	%rdx
	popq	%rcx
	movl	%eax, %r12d
	cmpl	$1, %eax
	jne	.L2
	cmpl	$1, 8(%rbx)
	jle	.L1
.L2:
	movq	(%rbx), %rdi
	leaq	.LC3(%rip), %rsi
	call	BIO_puts@PLT
	movq	callback_types(%rip), %rdx
	testq	%rdx, %rdx
	je	.L4
	leaq	callback_types(%rip), %rax
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L75:
	movq	16(%rax), %rdx
	addq	$16, %rax
	testq	%rdx, %rdx
	je	.L4
.L5:
	cmpl	8(%rax), %r14d
	jne	.L75
.L4:
	cmpl	$327694, %r14d
	jg	.L6
	cmpl	$327690, %r14d
	jg	.L39
	cmpl	$10, %r14d
	je	.L8
	cmpl	$15, %r14d
	je	.L8
	cmpl	$9, %r14d
	jne	.L76
	movq	ssl_versions(%rip), %rdx
	testq	%rdx, %rdx
	je	.L40
	leaq	ssl_versions(%rip), %rax
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L77:
	movq	16(%rax), %rdx
	addq	$16, %rax
	testq	%rdx, %rdx
	je	.L40
.L14:
	cmpl	8(%rax), %r15d
	jne	.L77
.L13:
	movq	(%rbx), %rdi
	leaq	.LC4(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	.p2align 4,,10
	.p2align 3
.L8:
	testl	%r12d, %r12d
	leaq	.LC2(%rip), %rax
	movq	(%rbx), %rdi
	leaq	.LC1(%rip), %rdx
	cmove	%rax, %rdx
	leaq	.LC14(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L1:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L78
	leaq	-40(%rbp), %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	.cfi_restore_state
	movl	%r14d, %eax
	xorl	%ecx, %ecx
	andb	$-17, %ah
	cmpl	$393234, %eax
	sete	%cl
	testq	%rdx, %rdx
	je	.L79
.L11:
	movq	(%rbx), %rdi
	leaq	.LC5(%rip), %rsi
	xorl	%eax, %eax
	movl	%ecx, -72(%rbp)
	movq	%rdx, -80(%rbp)
	call	BIO_printf@PLT
	movq	-80(%rbp), %rdx
	movl	-72(%rbp), %ecx
.L7:
	movl	%r14d, %eax
	xorw	%ax, %ax
	cmpl	$196608, %eax
	je	.L30
	ja	.L31
	cmpl	$65536, %eax
	je	.L32
	cmpl	$131072, %eax
	jne	.L15
	movl	%r15d, %edi
	call	EC_curve_nid2nist@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L80
.L16:
	movq	(%rbx), %rdi
	call	BIO_puts@PLT
.L15:
	movl	-68(%rbp), %edx
	movq	(%rbx), %rdi
	leaq	.LC13(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L31:
	cmpl	$327680, %eax
	je	.L34
.L37:
	cmpl	$393216, %eax
	jne	.L15
	movq	%r13, %rdi
	testl	%ecx, %ecx
	je	.L18
	call	X509_get_signature_nid@PLT
	movl	%eax, %edi
	call	OBJ_nid2sn@PLT
	movq	(%rbx), %rdi
	movq	%rax, %rsi
	call	BIO_puts@PLT
	jmp	.L15
.L18:
	call	X509_get0_pubkey@PLT
	movq	%rax, %r13
	leaq	.LC7(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, -64(%rbp)
	call	EVP_PKEY_get0_asn1@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r9
	leaq	-64(%rbp), %r8
	xorl	%edi, %edi
	call	EVP_PKEY_asn1_get0_info@PLT
	movq	%r13, %rdi
	call	EVP_PKEY_bits@PLT
	movq	-64(%rbp), %rdx
	movq	(%rbx), %rdi
	leaq	.LC8(%rip), %rsi
	movl	%eax, %ecx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L40:
	leaq	.LC0(%rip), %rdx
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L39:
	xorl	%ecx, %ecx
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L30:
	movq	%r13, %rdi
	call	DH_bits@PLT
	movq	(%rbx), %rdi
	leaq	.LC6(%rip), %rsi
	movl	%eax, %edx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L34:
	movzbl	0(%r13), %r15d
	movzbl	1(%r13), %eax
	movq	(%rbx), %rdi
	sall	$8, %r15d
	addl	%eax, %r15d
	testq	%rdx, %rdx
	je	.L19
	leaq	.LC9(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L20:
	movq	signature_tls13_scheme_list(%rip), %rdx
	testq	%rdx, %rdx
	je	.L21
	leaq	signature_tls13_scheme_list(%rip), %rax
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L81:
	movq	16(%rax), %rdx
	addq	$16, %rax
	testq	%rdx, %rdx
	je	.L21
.L23:
	cmpl	%r15d, 8(%rax)
	jne	.L81
	movq	(%rbx), %rdi
	leaq	.LC15(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L32:
	movq	%r13, %rdi
	call	SSL_CIPHER_get_name@PLT
	movq	(%rbx), %rdi
	movq	%rax, %rsi
	call	BIO_puts@PLT
	jmp	.L15
.L21:
	movq	signature_tls12_alg_list(%rip), %rcx
	movzbl	1(%r13), %edx
	leaq	signature_tls12_alg_list(%rip), %rax
	movzbl	0(%r13), %esi
	testq	%rcx, %rcx
	jne	.L25
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L82:
	movq	16(%rax), %rcx
	addq	$16, %rax
	testq	%rcx, %rcx
	je	.L24
.L25:
	cmpl	8(%rax), %edx
	jne	.L82
.L24:
	movq	signature_tls12_hash_list(%rip), %rdx
	testq	%rdx, %rdx
	je	.L26
	leaq	signature_tls12_hash_list(%rip), %rax
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L83:
	movq	16(%rax), %rdx
	addq	$16, %rax
	testq	%rdx, %rdx
	je	.L26
.L28:
	cmpl	8(%rax), %esi
	jne	.L83
	movq	(%rbx), %rdi
	testq	%rcx, %rcx
	je	.L26
	leaq	.LC11(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L15
.L80:
	movl	%r15d, %edi
	call	OBJ_nid2sn@PLT
	movq	%rax, %rsi
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L79:
	movl	%r14d, %eax
	xorw	%ax, %ax
	cmpl	$327680, %eax
	jne	.L37
	movzbl	0(%r13), %r15d
	movzbl	1(%r13), %eax
	movq	(%rbx), %rdi
	sall	$8, %r15d
	addl	%eax, %r15d
.L19:
	movl	%r14d, %edx
	leaq	.LC10(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L20
.L26:
	movq	(%rbx), %rdi
	movl	%r15d, %edx
	leaq	.LC12(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L15
.L78:
	call	__stack_chk_fail@PLT
.L76:
	xorl	%ecx, %ecx
	testq	%rdx, %rdx
	jne	.L11
	jmp	.L7
	.cfi_endproc
.LFE1576:
	.size	security_callback_debug, .-security_callback_debug
	.section	.rodata.str1.8
	.align 8
.LC16:
	.string	"Keylog callback is invoked without valid file!\n"
	.section	.rodata.str1.1
.LC17:
	.string	"%s\n"
	.text
	.p2align 4
	.type	keylog_callback, @function
keylog_callback:
.LFB1578:
	.cfi_startproc
	endbr64
	movq	bio_keylog(%rip), %rdi
	testq	%rdi, %rdi
	je	.L88
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdx
	xorl	%eax, %eax
	leaq	.LC17(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	BIO_printf@PLT
	movq	bio_keylog(%rip), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	BIO_ctrl@PLT
	.p2align 4,,10
	.p2align 3
.L88:
	movq	bio_err(%rip), %rdi
	leaq	.LC16(%rip), %rsi
	xorl	%eax, %eax
	jmp	BIO_printf@PLT
	.cfi_endproc
.LFE1578:
	.size	keylog_callback, .-keylog_callback
	.section	.rodata.str1.1
.LC18:
	.string	"OK\n"
.LC19:
	.string	"NOT OK\n"
	.section	.rodata.str1.8
	.align 8
.LC20:
	.string	"Checking cert chain %d:\nSubject: "
	.section	.rodata.str1.1
.LC21:
	.string	"\n"
.LC22:
	.string	"OK"
.LC23:
	.string	"\t%s: %s\n"
.LC24:
	.string	"NOT OK"
.LC25:
	.string	"\tSuite B: "
.LC26:
	.string	"not tested\n"
	.text
	.p2align 4
	.type	set_cert_cb, @function
set_cert_cb:
.LFB1562:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	call	SSL_certs_clear@PLT
	testq	%r12, %r12
	je	.L90
	.p2align 4,,10
	.p2align 3
.L91:
	movq	%r12, %rbx
	movq	72(%r12), %r12
	testq	%r12, %r12
	jne	.L91
	xorl	%r14d, %r14d
	.p2align 4,,10
	.p2align 3
.L106:
	movq	56(%rbx), %rcx
	movq	48(%rbx), %rdx
	movq	%r13, %rdi
	addl	$1, %r14d
	movq	40(%rbx), %rsi
	call	SSL_check_chain@PLT
	movq	bio_err(%rip), %rdi
	movl	%r14d, %edx
	leaq	.LC20(%rip), %rsi
	movl	%eax, %r12d
	xorl	%eax, %eax
	call	BIO_printf@PLT
	call	get_nameopt@PLT
	movq	40(%rbx), %rdi
	movq	%rax, %r15
	call	X509_get_subject_name@PLT
	movq	bio_err(%rip), %rdi
	xorl	%edx, %edx
	movq	%r15, %rcx
	movq	%rax, %rsi
	call	X509_NAME_print_ex@PLT
	movq	bio_err(%rip), %rdi
	leaq	.LC21(%rip), %rsi
	call	BIO_puts@PLT
	leaq	chain_flags(%rip), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L92
	movq	%rax, %r15
	.p2align 4,,10
	.p2align 3
.L95:
	leaq	.LC22(%rip), %rcx
	testl	%r12d, 8(%r15)
	jne	.L129
	leaq	.LC24(%rip), %rcx
.L129:
	movq	bio_err(%rip), %rdi
	leaq	.LC23(%rip), %rsi
	xorl	%eax, %eax
	addq	$16, %r15
	call	BIO_printf@PLT
	movq	(%r15), %rdx
	testq	%rdx, %rdx
	jne	.L95
.L92:
	movq	bio_err(%rip), %rdi
	leaq	.LC25(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$99, %esi
	movq	%r13, %rdi
	call	SSL_ctrl@PLT
	testl	$196608, %eax
	je	.L96
	testl	$2048, %r12d
	leaq	.LC19(%rip), %rsi
	leaq	.LC18(%rip), %rax
	movq	bio_err(%rip), %rdi
	cmovne	%rax, %rsi
	call	BIO_puts@PLT
.L98:
	andl	$1, %r12d
	jne	.L130
.L100:
	movq	80(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L106
.L90:
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L130:
	.cfi_restore_state
	movq	40(%rbx), %rsi
	movq	%r13, %rdi
	call	SSL_use_certificate@PLT
	testl	%eax, %eax
	je	.L103
	movq	48(%rbx), %rsi
	movq	%r13, %rdi
	call	SSL_use_PrivateKey@PLT
	testl	%eax, %eax
	je	.L103
	movl	64(%rbx), %eax
	testl	%eax, %eax
	jne	.L131
	movq	56(%rbx), %rcx
	testq	%rcx, %rcx
	je	.L100
	movl	$1, %edx
	movl	$88, %esi
	movq	%r13, %rdi
	call	SSL_ctrl@PLT
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L96:
	movq	bio_err(%rip), %rdi
	leaq	.LC26(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L98
	.p2align 4,,10
	.p2align 3
.L131:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$105, %esi
	movq	%r13, %rdi
	call	SSL_ctrl@PLT
	testq	%rax, %rax
	jne	.L100
.L103:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1562:
	.size	set_cert_cb, .-set_cert_cb
	.section	.rodata.str1.8
	.align 8
.LC27:
	.string	"%s: %zu-byte buffer too large to hexencode\n"
	.text
	.p2align 4
	.type	hexencode.part.0, @function
hexencode.part.0:
.LFB1583:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	opt_getprog@PLT
	movq	bio_err(%rip), %rdi
	movq	%r12, %rcx
	leaq	.LC27(%rip), %rsi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movl	$1, %edi
	call	exit@PLT
	.cfi_endproc
.LFE1583:
	.size	hexencode.part.0, .-hexencode.part.0
	.section	.rodata.str1.1
.LC28:
	.string	"gost2012_256"
.LC29:
	.string	"ECDSA"
.LC30:
	.string	"Ed448"
.LC31:
	.string	"DSA"
.LC32:
	.string	"RSA"
.LC33:
	.string	"gost2001"
.LC34:
	.string	"RSA-PSS"
.LC35:
	.string	"Ed25519"
.LC36:
	.string	"gost2012_512"
.LC37:
	.string	"Shared "
.LC38:
	.string	"Requested "
.LC39:
	.string	"Signature Algorithms: "
.LC40:
	.string	":"
.LC41:
	.string	"0x%02X"
.LC42:
	.string	"+%s"
.LC43:
	.string	"+0x%02X"
	.text
	.p2align 4
	.type	do_print_sigalgs, @function
do_print_sigalgs:
.LFB1548:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movl	%edx, -88(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	SSL_is_server@PLT
	movl	%eax, %ebx
	testl	%r14d, %r14d
	je	.L135
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	pushq	$0
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	SSL_get_shared_sigalgs@PLT
	popq	%r11
	popq	%r14
	movl	%eax, -84(%rbp)
	testl	%eax, %eax
	jne	.L176
.L137:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L177
	leaq	-40(%rbp), %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L176:
	.cfi_restore_state
	leaq	.LC37(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_puts@PLT
	testl	%ebx, %ebx
	je	.L178
.L140:
	leaq	.LC39(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_puts@PLT
	movl	-84(%rbp), %r8d
	testl	%r8d, %r8d
	jle	.L141
	leaq	-60(%rbp), %rax
	xorl	%ebx, %ebx
	leaq	-65(%rbp), %r15
	movq	%rax, -104(%rbp)
	leaq	-66(%rbp), %rax
	leaq	-64(%rbp), %r14
	movq	%rax, -96(%rbp)
	jmp	.L148
	.p2align 4,,10
	.p2align 3
.L180:
	cmpl	$408, %eax
	je	.L156
	jle	.L179
.L152:
	movzbl	-65(%rbp), %edx
	movq	%r12, %rdi
	leaq	.LC41(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movl	-64(%rbp), %edi
	testl	%edi, %edi
	je	.L147
.L145:
	call	OBJ_nid2sn@PLT
	leaq	.LC42(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L146:
	addl	$1, %ebx
	cmpl	-84(%rbp), %ebx
	je	.L141
.L148:
	movl	-88(%rbp), %edi
	testl	%edi, %edi
	je	.L142
	subq	$8, %rsp
	pushq	-96(%rbp)
	movq	-104(%rbp), %rdx
	movq	%r14, %rcx
	movl	%ebx, %esi
	movq	%r15, %r9
	xorl	%r8d, %r8d
	movq	%r13, %rdi
	call	SSL_get_shared_sigalgs@PLT
	popq	%rcx
	popq	%rsi
.L143:
	testl	%ebx, %ebx
	je	.L144
	leaq	.LC40(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_puts@PLT
.L144:
	movl	-60(%rbp), %eax
	cmpl	$979, %eax
	je	.L154
	jg	.L150
	cmpl	$811, %eax
	je	.L155
	jle	.L180
	leaq	.LC34(%rip), %rdx
	cmpl	$912, %eax
	jne	.L152
	.p2align 4,,10
	.p2align 3
.L149:
	movq	%r12, %rdi
	leaq	.LC9(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movl	-64(%rbp), %edi
	testl	%edi, %edi
	jne	.L145
	addl	$1, %ebx
	cmpl	-84(%rbp), %ebx
	jne	.L148
.L141:
	leaq	.LC21(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_puts@PLT
	jmp	.L137
	.p2align 4,,10
	.p2align 3
.L150:
	cmpl	$1087, %eax
	je	.L160
	cmpl	$1088, %eax
	jne	.L181
	leaq	.LC30(%rip), %rdx
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L179:
	cmpl	$6, %eax
	je	.L157
	leaq	.LC31(%rip), %rdx
	cmpl	$116, %eax
	je	.L149
	jmp	.L152
.L181:
	cmpl	$980, %eax
	jne	.L152
	leaq	.LC36(%rip), %rdx
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L142:
	subq	$8, %rsp
	pushq	-96(%rbp)
	movq	-104(%rbp), %rdx
	movq	%r15, %r9
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	%ebx, %esi
	movq	%r13, %rdi
	call	SSL_get_sigalgs@PLT
	popq	%rax
	popq	%rdx
	jmp	.L143
	.p2align 4,,10
	.p2align 3
.L156:
	leaq	.LC29(%rip), %rdx
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L160:
	leaq	.LC35(%rip), %rdx
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L155:
	leaq	.LC33(%rip), %rdx
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L147:
	movzbl	-66(%rbp), %edx
	leaq	.LC43(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L154:
	leaq	.LC28(%rip), %rdx
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L135:
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	pushq	$0
	xorl	%edx, %edx
	movl	$-1, %esi
	movq	%r13, %rdi
	call	SSL_get_sigalgs@PLT
	popq	%r9
	popq	%r10
	movl	%eax, -84(%rbp)
	testl	%eax, %eax
	je	.L137
	testl	%ebx, %ebx
	jne	.L140
.L178:
	leaq	.LC38(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_puts@PLT
	jmp	.L140
.L177:
	call	__stack_chk_fail@PLT
.L157:
	leaq	.LC32(%rip), %rdx
	jmp	.L149
	.cfi_endproc
.LFE1548:
	.size	do_print_sigalgs, .-do_print_sigalgs
	.section	.rodata.str1.1
.LC44:
	.string	"depth=%d "
.LC45:
	.string	"<no cert>\n"
.LC46:
	.string	"verify error:num=%d:%s\n"
.LC47:
	.string	"issuer= "
.LC48:
	.string	"notBefore="
.LC49:
	.string	"notAfter="
.LC50:
	.string	"verify return:%d\n"
	.text
	.p2align 4
	.globl	verify_callback
	.type	verify_callback, @function
verify_callback:
.LFB1543:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edi, %ebx
	movq	%rsi, %rdi
	subq	$24, %rsp
	call	X509_STORE_CTX_get_current_cert@PLT
	movq	%r14, %rdi
	movq	%rax, %r15
	call	X509_STORE_CTX_get_error@PLT
	movq	%r14, %rdi
	movl	%eax, %r13d
	call	X509_STORE_CTX_get_error_depth@PLT
	movl	4+verify_args(%rip), %edi
	movl	%eax, %r12d
	testl	%edi, %edi
	je	.L211
	testl	%ebx, %ebx
	je	.L211
.L183:
	cmpl	$43, %r13d
	ja	.L198
	leaq	.L199(%rip), %rcx
	movl	%r13d, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L199:
	.long	.L198-.L199
	.long	.L198-.L199
	.long	.L207-.L199
	.long	.L198-.L199
	.long	.L198-.L199
	.long	.L198-.L199
	.long	.L198-.L199
	.long	.L198-.L199
	.long	.L198-.L199
	.long	.L193-.L199
	.long	.L192-.L199
	.long	.L198-.L199
	.long	.L198-.L199
	.long	.L193-.L199
	.long	.L192-.L199
	.long	.L198-.L199
	.long	.L198-.L199
	.long	.L198-.L199
	.long	.L198-.L199
	.long	.L198-.L199
	.long	.L198-.L199
	.long	.L198-.L199
	.long	.L198-.L199
	.long	.L198-.L199
	.long	.L198-.L199
	.long	.L198-.L199
	.long	.L198-.L199
	.long	.L198-.L199
	.long	.L198-.L199
	.long	.L198-.L199
	.long	.L198-.L199
	.long	.L198-.L199
	.long	.L198-.L199
	.long	.L198-.L199
	.long	.L198-.L199
	.long	.L198-.L199
	.long	.L198-.L199
	.long	.L198-.L199
	.long	.L198-.L199
	.long	.L198-.L199
	.long	.L198-.L199
	.long	.L198-.L199
	.long	.L198-.L199
	.long	.L208-.L199
	.text
	.p2align 4,,10
	.p2align 3
.L211:
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
	movl	%r12d, %edx
	leaq	.LC44(%rip), %rsi
	call	BIO_printf@PLT
	testq	%r15, %r15
	je	.L185
	call	get_nameopt@PLT
	movq	%r15, %rdi
	movq	%rax, -56(%rbp)
	call	X509_get_subject_name@PLT
	movq	bio_err(%rip), %rdi
	movq	-56(%rbp), %rcx
	xorl	%edx, %edx
	movq	%rax, %rsi
	call	X509_NAME_print_ex@PLT
	movq	bio_err(%rip), %rdi
	leaq	.LC21(%rip), %rsi
	call	BIO_puts@PLT
.L186:
	testl	%ebx, %ebx
	jne	.L183
	movslq	%r13d, %rdi
	call	X509_verify_cert_error_string@PLT
	movq	bio_err(%rip), %rdi
	movl	%r13d, %edx
	leaq	.LC46(%rip), %rsi
	movq	%rax, %rcx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movl	verify_args(%rip), %eax
	testl	%eax, %eax
	js	.L212
	cmpl	%r12d, %eax
	jl	.L187
.L212:
	movl	12+verify_args(%rip), %esi
	xorl	%r12d, %r12d
	movl	%r13d, 8+verify_args(%rip)
	testl	%esi, %esi
	sete	%r12b
	cmpl	$43, %r13d
	ja	.L189
	leaq	.L191(%rip), %rdx
	movl	%r13d, %r13d
	movslq	(%rdx,%r13,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L191:
	.long	.L189-.L191
	.long	.L189-.L191
	.long	.L194-.L191
	.long	.L189-.L191
	.long	.L189-.L191
	.long	.L189-.L191
	.long	.L189-.L191
	.long	.L189-.L191
	.long	.L189-.L191
	.long	.L203-.L191
	.long	.L204-.L191
	.long	.L189-.L191
	.long	.L189-.L191
	.long	.L203-.L191
	.long	.L204-.L191
	.long	.L189-.L191
	.long	.L189-.L191
	.long	.L189-.L191
	.long	.L189-.L191
	.long	.L189-.L191
	.long	.L189-.L191
	.long	.L189-.L191
	.long	.L189-.L191
	.long	.L189-.L191
	.long	.L189-.L191
	.long	.L189-.L191
	.long	.L189-.L191
	.long	.L189-.L191
	.long	.L189-.L191
	.long	.L189-.L191
	.long	.L189-.L191
	.long	.L189-.L191
	.long	.L189-.L191
	.long	.L189-.L191
	.long	.L189-.L191
	.long	.L189-.L191
	.long	.L189-.L191
	.long	.L189-.L191
	.long	.L189-.L191
	.long	.L189-.L191
	.long	.L189-.L191
	.long	.L189-.L191
	.long	.L189-.L191
	.long	.L190-.L191
	.text
	.p2align 4,,10
	.p2align 3
.L198:
	movl	%ebx, %r12d
	cmpl	$2, %ebx
	jne	.L189
	testl	%r13d, %r13d
	jne	.L189
	movl	4+verify_args(%rip), %edx
	testl	%edx, %edx
	je	.L230
.L182:
	addq	$24, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L208:
	.cfi_restore_state
	movl	%ebx, %r12d
.L190:
	movl	4+verify_args(%rip), %ecx
	testl	%ecx, %ecx
	jne	.L189
.L201:
	movq	%r14, %rdi
	call	policies_print@PLT
.L189:
	testl	%r12d, %r12d
	je	.L182
	jmp	.L202
	.p2align 4,,10
	.p2align 3
.L185:
	movq	bio_err(%rip), %rdi
	leaq	.LC45(%rip), %rsi
	call	BIO_puts@PLT
	jmp	.L186
	.p2align 4,,10
	.p2align 3
.L187:
	movl	$22, 8+verify_args(%rip)
	cmpl	$43, %r13d
	ja	.L205
	leaq	.L197(%rip), %rdx
	movl	%r13d, %r13d
	movslq	(%rdx,%r13,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L197:
	.long	.L205-.L197
	.long	.L205-.L197
	.long	.L206-.L197
	.long	.L205-.L197
	.long	.L205-.L197
	.long	.L205-.L197
	.long	.L205-.L197
	.long	.L205-.L197
	.long	.L205-.L197
	.long	.L193-.L197
	.long	.L192-.L197
	.long	.L205-.L197
	.long	.L205-.L197
	.long	.L193-.L197
	.long	.L192-.L197
	.long	.L205-.L197
	.long	.L205-.L197
	.long	.L205-.L197
	.long	.L205-.L197
	.long	.L205-.L197
	.long	.L205-.L197
	.long	.L205-.L197
	.long	.L205-.L197
	.long	.L205-.L197
	.long	.L205-.L197
	.long	.L205-.L197
	.long	.L205-.L197
	.long	.L205-.L197
	.long	.L205-.L197
	.long	.L205-.L197
	.long	.L205-.L197
	.long	.L205-.L197
	.long	.L205-.L197
	.long	.L205-.L197
	.long	.L205-.L197
	.long	.L205-.L197
	.long	.L205-.L197
	.long	.L205-.L197
	.long	.L205-.L197
	.long	.L205-.L197
	.long	.L205-.L197
	.long	.L205-.L197
	.long	.L205-.L197
	.long	.L196-.L197
	.text
.L196:
	movl	4+verify_args(%rip), %r12d
	testl	%r12d, %r12d
	je	.L201
	.p2align 4,,10
	.p2align 3
.L205:
	xorl	%r12d, %r12d
	jmp	.L182
	.p2align 4,,10
	.p2align 3
.L204:
	movl	%r12d, %ebx
.L192:
	movq	bio_err(%rip), %rdi
	leaq	.LC49(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	%r15, %rdi
	call	X509_get0_notAfter@PLT
.L229:
	movq	bio_err(%rip), %rdi
	movq	%rax, %rsi
	movl	%ebx, %r12d
	call	ASN1_TIME_print@PLT
	movq	bio_err(%rip), %rdi
	leaq	.LC21(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	testl	%r12d, %r12d
	je	.L182
	jmp	.L202
	.p2align 4,,10
	.p2align 3
.L203:
	movl	%r12d, %ebx
.L193:
	movq	bio_err(%rip), %rdi
	leaq	.LC48(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	%r15, %rdi
	call	X509_get0_notBefore@PLT
	jmp	.L229
	.p2align 4,,10
	.p2align 3
.L207:
	movl	%ebx, %r12d
.L194:
	movq	bio_err(%rip), %rdi
	leaq	.LC47(%rip), %rsi
	call	BIO_puts@PLT
	call	get_nameopt@PLT
	movq	%r15, %rdi
	movq	%rax, %r13
	call	X509_get_issuer_name@PLT
	movq	bio_err(%rip), %rdi
	xorl	%edx, %edx
	movq	%r13, %rcx
	movq	%rax, %rsi
	call	X509_NAME_print_ex@PLT
	movq	bio_err(%rip), %rdi
	leaq	.LC21(%rip), %rsi
	call	BIO_puts@PLT
	testl	%r12d, %r12d
	je	.L182
	jmp	.L202
	.p2align 4,,10
	.p2align 3
.L230:
	movq	%r14, %rdi
	call	policies_print@PLT
.L202:
	movl	4+verify_args(%rip), %eax
	testl	%eax, %eax
	jne	.L182
	movq	bio_err(%rip), %rdi
	movl	%r12d, %edx
	leaq	.LC50(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L182
.L206:
	xorl	%r12d, %r12d
	jmp	.L194
	.cfi_endproc
.LFE1543:
	.size	verify_callback, .-verify_callback
	.section	.rodata.str1.8
	.align 8
.LC51:
	.string	"unable to get certificate from '%s'\n"
	.align 8
.LC52:
	.string	"unable to get private key from '%s'\n"
	.align 8
.LC53:
	.string	"Private key does not match the certificate public key\n"
	.text
	.p2align 4
	.globl	set_cert_stuff
	.type	set_cert_stuff, @function
set_cert_stuff:
.LFB1544:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	testq	%rsi, %rsi
	je	.L237
	movq	%rdx, %r13
	movl	$1, %edx
	movq	%rsi, %r12
	movq	%rdi, %r14
	call	SSL_CTX_use_certificate_file@PLT
	movq	%r12, %rdx
	leaq	.LC51(%rip), %rsi
	testl	%eax, %eax
	jle	.L239
	testq	%r13, %r13
	movl	$1, %edx
	movq	%r14, %rdi
	cmove	%r12, %r13
	movq	%r13, %rsi
	call	SSL_CTX_use_PrivateKey_file@PLT
	testl	%eax, %eax
	jle	.L240
	movq	%r14, %rdi
	call	SSL_CTX_check_private_key@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	je	.L241
.L237:
	movl	$1, %r12d
.L231:
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L240:
	.cfi_restore_state
	movq	%r13, %rdx
	leaq	.LC52(%rip), %rsi
.L239:
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
	xorl	%r12d, %r12d
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L241:
	.cfi_restore_state
	movq	bio_err(%rip), %rdi
	leaq	.LC53(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L231
	.cfi_endproc
.LFE1544:
	.size	set_cert_stuff, .-set_cert_stuff
	.section	.rodata.str1.1
.LC54:
	.string	"error setting certificate\n"
.LC55:
	.string	"error setting private key\n"
	.section	.rodata.str1.8
	.align 8
.LC56:
	.string	"error setting certificate chain\n"
	.align 8
.LC57:
	.string	"error building certificate chain\n"
	.text
	.p2align 4
	.globl	set_cert_key_stuff
	.type	set_cert_key_stuff, @function
set_cert_key_stuff:
.LFB1545:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	xorl	%r14d, %r14d
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%rcx, %rcx
	setne	%r14b
	salq	$2, %r14
	testq	%rsi, %rsi
	je	.L250
	movq	%rdi, %r12
	movq	%rdx, %r15
	movq	%rcx, %r13
	movl	%r8d, %ebx
	call	SSL_CTX_use_certificate@PLT
	leaq	.LC54(%rip), %rsi
	testl	%eax, %eax
	jle	.L259
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	SSL_CTX_use_PrivateKey@PLT
	testl	%eax, %eax
	jle	.L261
	movq	%r12, %rdi
	call	SSL_CTX_check_private_key@PLT
	movl	%eax, %r15d
	testl	%eax, %eax
	je	.L262
	testq	%r13, %r13
	je	.L249
	movl	$88, %esi
	movq	%r13, %rcx
	movl	$1, %edx
	movq	%r12, %rdi
	call	SSL_CTX_ctrl@PLT
	leaq	.LC56(%rip), %rsi
	testq	%rax, %rax
	je	.L260
.L249:
	testl	%ebx, %ebx
	jne	.L263
.L250:
	movl	$1, %r15d
.L242:
	addq	$8, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L261:
	.cfi_restore_state
	leaq	.LC55(%rip), %rsi
.L259:
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
	xorl	%r15d, %r15d
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	jmp	.L242
	.p2align 4,,10
	.p2align 3
.L263:
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movl	$105, %esi
	movq	%r12, %rdi
	call	SSL_CTX_ctrl@PLT
	testq	%rax, %rax
	jne	.L250
	leaq	.LC57(%rip), %rsi
.L260:
	movq	bio_err(%rip), %rdi
	xorl	%r15d, %r15d
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	jmp	.L242
	.p2align 4,,10
	.p2align 3
.L262:
	movq	bio_err(%rip), %rdi
	leaq	.LC53(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L242
	.cfi_endproc
.LFE1545:
	.size	set_cert_key_stuff, .-set_cert_key_stuff
	.section	.rodata.str1.1
.LC58:
	.string	"Client Certificate Types: "
.LC59:
	.string	", "
.LC60:
	.string	"UNKNOWN (%d),"
.LC61:
	.string	"Peer signing digest: %s\n"
.LC62:
	.string	"Peer signature type: %s\n"
	.text
	.p2align 4
	.globl	ssl_print_sigalgs
	.type	ssl_print_sigalgs, @function
ssl_print_sigalgs:
.LFB1549:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-64(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%rsi, %rdi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	SSL_is_server@PLT
	testl	%eax, %eax
	je	.L319
.L265:
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	do_print_sigalgs
	movl	$1, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	do_print_sigalgs
	xorl	%edx, %edx
	movq	%r14, %rcx
	movl	$108, %esi
	movq	%r12, %rdi
	call	SSL_ctrl@PLT
	testq	%rax, %rax
	jne	.L320
.L273:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	SSL_get_peer_signature_type_nid@PLT
	testl	%eax, %eax
	jne	.L321
.L274:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L322
	addq	$40, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L320:
	.cfi_restore_state
	movl	-64(%rbp), %edi
	testl	%edi, %edi
	je	.L273
	call	OBJ_nid2sn@PLT
	leaq	.LC61(%rip), %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	SSL_get_peer_signature_type_nid@PLT
	testl	%eax, %eax
	je	.L274
	.p2align 4,,10
	.p2align 3
.L321:
	movl	-64(%rbp), %eax
	leaq	.LC34(%rip), %rdx
	cmpl	$912, %eax
	je	.L275
	jle	.L323
	leaq	.LC35(%rip), %rdx
	cmpl	$1087, %eax
	je	.L275
	jle	.L324
	cmpl	$1088, %eax
	leaq	.LC30(%rip), %rdx
	movl	$0, %eax
	cmovne	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L275:
	leaq	.LC62(%rip), %rsi
	movq	%r13, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L274
	.p2align 4,,10
	.p2align 3
.L323:
	leaq	.LC29(%rip), %rdx
	cmpl	$408, %eax
	je	.L275
	jle	.L325
	cmpl	$811, %eax
	leaq	.LC33(%rip), %rdx
	movl	$0, %eax
	cmovne	%rax, %rdx
	jmp	.L275
	.p2align 4,,10
	.p2align 3
.L325:
	leaq	.LC32(%rip), %rdx
	cmpl	$6, %eax
	je	.L275
	cmpl	$116, %eax
	leaq	.LC31(%rip), %rdx
	movl	$0, %eax
	cmovne	%rax, %rdx
	jmp	.L275
	.p2align 4,,10
	.p2align 3
.L324:
	leaq	.LC28(%rip), %rdx
	cmpl	$979, %eax
	je	.L275
	cmpl	$980, %eax
	leaq	.LC36(%rip), %rdx
	movl	$0, %eax
	cmovne	%rax, %rdx
	jmp	.L275
	.p2align 4,,10
	.p2align 3
.L319:
	movq	%r14, %rcx
	xorl	%edx, %edx
	movl	$103, %esi
	movq	%r12, %rdi
	call	SSL_ctrl@PLT
	movq	%rax, %rbx
	testl	%eax, %eax
	je	.L265
	leaq	.LC58(%rip), %rsi
	movq	%r13, %rdi
	call	BIO_puts@PLT
	testl	%ebx, %ebx
	jle	.L267
	leal	-1(%rbx), %eax
	leaq	.LC59(%rip), %r15
	xorl	%ebx, %ebx
	movq	%rax, -72(%rbp)
	.p2align 4,,10
	.p2align 3
.L272:
	movq	-64(%rbp), %rax
	movzbl	(%rax,%rbx), %edx
	leaq	cert_type_list(%rip), %rax
	movq	(%rax), %r8
	testq	%r8, %r8
	jne	.L270
	jmp	.L268
	.p2align 4,,10
	.p2align 3
.L326:
	movq	16(%rax), %r8
	addq	$16, %rax
	testq	%r8, %r8
	je	.L268
.L270:
	cmpl	8(%rax), %edx
	jne	.L326
	testq	%rbx, %rbx
	je	.L271
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%r8, -80(%rbp)
	call	BIO_puts@PLT
	movq	-80(%rbp), %r8
.L271:
	movq	%r8, %rsi
	movq	%r13, %rdi
	call	BIO_puts@PLT
	leaq	1(%rbx), %rax
	cmpq	-72(%rbp), %rbx
	je	.L267
.L282:
	movq	%rax, %rbx
	jmp	.L272
	.p2align 4,,10
	.p2align 3
.L268:
	testq	%rbx, %rbx
	je	.L279
	movq	%r15, %rsi
	movq	%r13, %rdi
	movl	%edx, -80(%rbp)
	call	BIO_puts@PLT
	movl	-80(%rbp), %edx
.L279:
	leaq	.LC60(%rip), %rsi
	movq	%r13, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	leaq	1(%rbx), %rax
	cmpq	-72(%rbp), %rbx
	jne	.L282
.L267:
	leaq	.LC21(%rip), %rsi
	movq	%r13, %rdi
	call	BIO_puts@PLT
	jmp	.L265
.L322:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1549:
	.size	ssl_print_sigalgs, .-ssl_print_sigalgs
	.section	.rodata.str1.8
	.align 8
.LC63:
	.string	"Supported Elliptic Curve Point Formats: "
	.section	.rodata.str1.1
.LC64:
	.string	"uncompressed"
.LC65:
	.string	"ansiX962_compressed_prime"
.LC66:
	.string	"ansiX962_compressed_char2"
.LC67:
	.string	"unknown(%d)"
	.text
	.p2align 4
	.globl	ssl_print_point_formats
	.type	ssl_print_point_formats, @function
ssl_print_point_formats:
.LFB1550:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	leaq	-48(%rbp), %rcx
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%rsi, %rdi
	movl	$111, %esi
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	SSL_ctrl@PLT
	testl	%eax, %eax
	jle	.L328
	leaq	.LC63(%rip), %rsi
	movq	%r12, %rdi
	movl	%eax, %r13d
	xorl	%ebx, %ebx
	call	BIO_puts@PLT
	leaq	.LC65(%rip), %r14
	jmp	.L329
	.p2align 4,,10
	.p2align 3
.L339:
	testb	%dl, %dl
	je	.L337
	leaq	.LC67(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L333:
	addl	$1, %ebx
	addq	$1, -48(%rbp)
	cmpl	%ebx, %r13d
	je	.L338
.L334:
	leaq	.LC40(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_puts@PLT
.L329:
	movq	-48(%rbp), %rax
	movsbl	(%rax), %edx
	cmpb	$1, %dl
	je	.L330
	cmpb	$2, %dl
	jne	.L339
	leaq	.LC66(%rip), %rsi
	movq	%r12, %rdi
	addl	$1, %ebx
	call	BIO_puts@PLT
	addq	$1, -48(%rbp)
	cmpl	%ebx, %r13d
	jne	.L334
.L338:
	leaq	.LC21(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_puts@PLT
.L328:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L340
	addq	$16, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L330:
	.cfi_restore_state
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	BIO_puts@PLT
	jmp	.L333
	.p2align 4,,10
	.p2align 3
.L337:
	leaq	.LC64(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_puts@PLT
	jmp	.L333
.L340:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1550:
	.size	ssl_print_point_formats, .-ssl_print_point_formats
	.section	.rodata.str1.1
.LC68:
	.string	"groups to print"
.LC69:
	.string	"Supported Elliptic Groups: "
.LC70:
	.string	"0x%04X"
	.section	.rodata.str1.8
	.align 8
.LC71:
	.string	"../deps/openssl/openssl/apps/s_cb.c"
	.section	.rodata.str1.1
.LC72:
	.string	"\nShared Elliptic groups: "
.LC73:
	.string	"NONE"
	.text
	.p2align 4
	.globl	ssl_print_groups
	.type	ssl_print_groups, @function
ssl_print_groups:
.LFB1551:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%ecx, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movl	$90, %esi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movl	%edx, -60(%rbp)
	xorl	%edx, %edx
	call	SSL_ctrl@PLT
	testl	%eax, %eax
	jle	.L342
	leal	0(,%rax,4), %edi
	leaq	.LC68(%rip), %rsi
	movq	%rax, %r14
	call	app_malloc@PLT
	xorl	%edx, %edx
	movl	$90, %esi
	movq	%r13, %rdi
	movq	%rax, %rcx
	movq	%rax, %rbx
	movq	%rax, -72(%rbp)
	leaq	.LC9(%rip), %r15
	call	SSL_ctrl@PLT
	leaq	.LC69(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_puts@PLT
	leal	-1(%r14), %eax
	leaq	4(%rbx,%rax,4), %r14
	jmp	.L343
	.p2align 4,,10
	.p2align 3
.L358:
	movzwl	%di, %edx
	leaq	.LC70(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	addq	$4, %rbx
	cmpq	%rbx, %r14
	je	.L357
.L347:
	leaq	.LC40(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_puts@PLT
.L343:
	movl	(%rbx), %edi
	testl	$16777216, %edi
	jne	.L358
	movl	%edi, -56(%rbp)
	call	EC_curve_nid2nist@PLT
	movl	-56(%rbp), %edi
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L359
.L346:
	movq	%r15, %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	addq	$4, %rbx
	call	BIO_printf@PLT
	cmpq	%rbx, %r14
	jne	.L347
.L357:
	movq	-72(%rbp), %rdi
	movl	$371, %edx
	leaq	.LC71(%rip), %rsi
	call	CRYPTO_free@PLT
	movl	-60(%rbp), %eax
	testl	%eax, %eax
	jne	.L354
	leaq	.LC72(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_puts@PLT
	xorl	%ecx, %ecx
	movl	$93, %esi
	movq	%r13, %rdi
	movq	$-1, %rdx
	call	SSL_ctrl@PLT
	testl	%eax, %eax
	jle	.L349
	subl	$1, %eax
	xorl	%r15d, %r15d
	leaq	.LC9(%rip), %r14
	movq	%rax, -56(%rbp)
	jmp	.L350
	.p2align 4,,10
	.p2align 3
.L351:
	movq	%r14, %rsi
	movq	%r12, %rdi
	leaq	1(%r15), %rbx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	cmpq	%r15, -56(%rbp)
	je	.L354
	leaq	.LC40(%rip), %rsi
	movq	%r12, %rdi
	movq	%rbx, %r15
	call	BIO_puts@PLT
.L350:
	movq	%r15, %rdx
	xorl	%ecx, %ecx
	movl	$93, %esi
	movq	%r13, %rdi
	call	SSL_ctrl@PLT
	movl	%eax, %edi
	movq	%rax, %rbx
	call	EC_curve_nid2nist@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	jne	.L351
	movl	%ebx, %edi
	call	OBJ_nid2sn@PLT
	movq	%rax, %rdx
	jmp	.L351
.L349:
	jne	.L354
	leaq	.LC73(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_puts@PLT
	.p2align 4,,10
	.p2align 3
.L354:
	leaq	.LC21(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_puts@PLT
.L342:
	addq	$40, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L359:
	.cfi_restore_state
	call	OBJ_nid2sn@PLT
	movq	%rax, %rdx
	jmp	.L346
	.cfi_endproc
.LFE1551:
	.size	ssl_print_groups, .-ssl_print_groups
	.section	.rodata.str1.1
.LC74:
	.string	"Server Temp Key: "
.LC75:
	.string	"RSA, %d bits\n"
.LC76:
	.string	"DH, %d bits\n"
.LC77:
	.string	"ECDH, %s, %d bits\n"
.LC78:
	.string	"%s, %d bits\n"
	.text
	.p2align 4
	.globl	ssl_print_tmp_key
	.type	ssl_print_tmp_key, @function
ssl_print_tmp_key:
.LFB1552:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	leaq	-48(%rbp), %rcx
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%rsi, %rdi
	movl	$109, %esi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	SSL_ctrl@PLT
	testq	%rax, %rax
	jne	.L372
.L361:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L373
	addq	$24, %rsp
	movl	$1, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L372:
	.cfi_restore_state
	leaq	.LC74(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_puts@PLT
	movq	-48(%rbp), %rdi
	call	EVP_PKEY_id@PLT
	movq	-48(%rbp), %rdi
	cmpl	$28, %eax
	je	.L362
	cmpl	$408, %eax
	je	.L363
	cmpl	$6, %eax
	je	.L374
	call	EVP_PKEY_bits@PLT
	movq	-48(%rbp), %rdi
	movl	%eax, %r13d
	call	EVP_PKEY_id@PLT
	movl	%eax, %edi
	call	OBJ_nid2sn@PLT
	movl	%r13d, %ecx
	leaq	.LC78(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L365:
	movq	-48(%rbp), %rdi
	call	EVP_PKEY_free@PLT
	jmp	.L361
	.p2align 4,,10
	.p2align 3
.L362:
	call	EVP_PKEY_bits@PLT
	leaq	.LC76(%rip), %rsi
	movq	%r12, %rdi
	movl	%eax, %edx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L365
	.p2align 4,,10
	.p2align 3
.L374:
	call	EVP_PKEY_bits@PLT
	leaq	.LC75(%rip), %rsi
	movq	%r12, %rdi
	movl	%eax, %edx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L365
	.p2align 4,,10
	.p2align 3
.L363:
	call	EVP_PKEY_get1_EC_KEY@PLT
	movq	%rax, %rdi
	movq	%rax, %r13
	call	EC_KEY_get0_group@PLT
	movq	%rax, %rdi
	call	EC_GROUP_get_curve_name@PLT
	movq	%r13, %rdi
	movl	%eax, %r14d
	call	EC_KEY_free@PLT
	movl	%r14d, %edi
	call	EC_curve_nid2nist@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L375
.L366:
	movq	-48(%rbp), %rdi
	call	EVP_PKEY_bits@PLT
	movq	%r13, %rdx
	leaq	.LC77(%rip), %rsi
	movq	%r12, %rdi
	movl	%eax, %ecx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L365
	.p2align 4,,10
	.p2align 3
.L375:
	movl	%r14d, %edi
	call	OBJ_nid2sn@PLT
	movq	%rax, %r13
	jmp	.L366
.L373:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1552:
	.size	ssl_print_tmp_key, .-ssl_print_tmp_key
	.section	.rodata.str1.8
	.align 8
.LC79:
	.string	"read from %p [%p] (%lu bytes => %ld (0x%lX))\n"
	.align 8
.LC80:
	.string	"write to %p [%p] (%lu bytes => %ld (0x%lX))\n"
	.text
	.p2align 4
	.globl	bio_dump_callback
	.type	bio_dump_callback, @function
bio_dump_callback:
.LFB1553:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r9, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$24, %rsp
	movl	%ecx, -52(%rbp)
	call	BIO_get_callback_arg@PLT
	testq	%rax, %rax
	je	.L377
	movq	%rax, %r15
	cmpl	$130, %ebx
	je	.L384
	cmpl	$131, %ebx
	je	.L385
.L377:
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L385:
	.cfi_restore_state
	subq	$8, %rsp
	movslq	-52(%rbp), %r8
	movq	%r12, %r9
	movq	%r13, %rcx
	pushq	%r12
	movq	%r14, %rdx
	leaq	.LC80(%rip), %rsi
.L383:
	movq	%r15, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movl	%r12d, %edx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	BIO_dump@PLT
	popq	%rax
	popq	%rdx
	jmp	.L377
	.p2align 4,,10
	.p2align 3
.L384:
	subq	$8, %rsp
	movslq	-52(%rbp), %r8
	movq	%r12, %r9
	movq	%r13, %rcx
	pushq	%r12
	movq	%r14, %rdx
	leaq	.LC79(%rip), %rsi
	jmp	.L383
	.cfi_endproc
.LFE1553:
	.size	bio_dump_callback, .-bio_dump_callback
	.section	.rodata.str1.1
.LC81:
	.string	"SSL_connect"
.LC82:
	.string	"SSL_accept"
.LC83:
	.string	"undefined"
.LC84:
	.string	"read"
.LC85:
	.string	"write"
.LC86:
	.string	"%s:%s\n"
.LC87:
	.string	"SSL3 alert %s:%s:%s\n"
.LC88:
	.string	"%s:failed in %s\n"
.LC89:
	.string	"%s:error in %s\n"
	.text
	.p2align 4
	.globl	apps_ssl_info_callback
	.type	apps_ssl_info_callback, @function
apps_ssl_info_callback:
.LFB1554:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	.LC81(%rip), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movl	%edx, %r12d
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	testl	$4096, %esi
	jne	.L387
	testl	$8192, %esi
	leaq	.LC82(%rip), %r13
	leaq	.LC83(%rip), %rdx
	cmove	%rdx, %r13
.L387:
	testb	$1, %sil
	jne	.L402
	testl	$16384, %esi
	je	.L389
	andl	$4, %esi
	leaq	.LC85(%rip), %rdx
	movl	%r12d, %edi
	leaq	.LC84(%rip), %r13
	cmove	%rdx, %r13
	call	SSL_alert_desc_string_long@PLT
	movl	%r12d, %edi
	movq	%rax, %rbx
	call	SSL_alert_type_string_long@PLT
	movq	bio_err(%rip), %rdi
	addq	$8, %rsp
	movq	%rbx, %r8
	movq	%rax, %rcx
	popq	%rbx
	movq	%r13, %rdx
	popq	%r12
	leaq	.LC87(%rip), %rsi
	popq	%r13
	xorl	%eax, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	BIO_printf@PLT
	.p2align 4,,10
	.p2align 3
.L389:
	.cfi_restore_state
	andl	$2, %esi
	je	.L386
	testl	%r12d, %r12d
	je	.L403
	js	.L404
.L386:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L402:
	.cfi_restore_state
	call	SSL_state_string_long@PLT
	movq	%r13, %rdx
	leaq	.LC86(%rip), %rsi
	movq	%rax, %rcx
.L401:
	movq	bio_err(%rip), %rdi
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	BIO_printf@PLT
	.p2align 4,,10
	.p2align 3
.L403:
	.cfi_restore_state
	call	SSL_state_string_long@PLT
	movq	%r13, %rdx
	leaq	.LC88(%rip), %rsi
	movq	%rax, %rcx
	jmp	.L401
	.p2align 4,,10
	.p2align 3
.L404:
	call	SSL_state_string_long@PLT
	movq	%r13, %rdx
	leaq	.LC89(%rip), %rsi
	movq	%rax, %rcx
	jmp	.L401
	.cfi_endproc
.LFE1554:
	.size	apps_ssl_info_callback, .-apps_ssl_info_callback
	.section	.rodata.str1.1
.LC90:
	.string	">>>"
.LC91:
	.string	"<<<"
.LC92:
	.string	", warning"
.LC93:
	.string	", ???"
.LC94:
	.string	", fatal"
.LC95:
	.string	", ApplicationData"
.LC96:
	.string	", ChangeCipherSpec"
.LC97:
	.string	", Alert"
.LC98:
	.string	" ???"
.LC99:
	.string	", Handshake"
.LC100:
	.string	"%s %s%s [length %04lx]%s%s\n"
.LC101:
	.string	"\n   "
.LC102:
	.string	" %02x"
.LC103:
	.string	"   "
	.text
	.p2align 4
	.globl	msg_cb
	.type	msg_cb, @function
msg_cb:
.LFB1555:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC90(%rip), %r10
	leaq	.LC91(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$8, %rsp
	movq	ssl_versions(%rip), %rcx
	testl	%edi, %edi
	movq	16(%rbp), %r14
	cmove	%rax, %r10
	testq	%rcx, %rcx
	je	.L427
	leaq	ssl_versions(%rip), %rax
	jmp	.L408
	.p2align 4,,10
	.p2align 3
.L450:
	movq	16(%rax), %rcx
	addq	$16, %rax
	testq	%rcx, %rcx
	je	.L427
.L408:
	cmpl	8(%rax), %esi
	jne	.L450
.L407:
	cmpl	$65279, %esi
	sete	%dil
	cmpl	$256, %esi
	sete	%al
	orb	%al, %dil
	jne	.L438
	subl	$768, %esi
	cmpl	$4, %esi
	ja	.L430
.L438:
	cmpl	$22, %edx
	je	.L411
	jg	.L412
	cmpl	$20, %edx
	je	.L429
	cmpl	$21, %edx
	jne	.L430
	leaq	.LC93(%rip), %rax
	leaq	.LC97(%rip), %r8
	leaq	.LC7(%rip), %rdx
	cmpq	$2, %rbx
	jne	.L409
	movzbl	(%r12), %esi
	leaq	.LC92(%rip), %rdx
	cmpb	$1, %sil
	je	.L414
	cmpb	$2, %sil
	leaq	.LC94(%rip), %rdx
	cmovne	%rax, %rdx
.L414:
	movq	alert_types(%rip), %rsi
	movzbl	1(%r12), %edi
	testq	%rsi, %rsi
	je	.L434
	leaq	alert_types(%rip), %rax
	jmp	.L416
	.p2align 4,,10
	.p2align 3
.L451:
	movq	16(%rax), %rsi
	addq	$16, %rax
	testq	%rsi, %rsi
	je	.L434
.L416:
	cmpl	8(%rax), %edi
	jne	.L451
	leaq	.LC97(%rip), %r8
	jmp	.L415
	.p2align 4,,10
	.p2align 3
.L412:
	cmpl	$23, %edx
	je	.L452
.L430:
	leaq	.LC7(%rip), %rax
	movq	%rax, %rdx
	movq	%rax, %r8
.L409:
	pushq	%rdx
	movq	%r14, %rdi
	movq	%rbx, %r9
	movq	%r10, %rdx
	pushq	%rax
	leaq	.LC100(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	popq	%rdi
	popq	%r8
	testq	%rbx, %rbx
	jne	.L453
.L420:
	leaq	-40(%rbp), %rsp
	movq	%r14, %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	popq	%rbx
	movl	$11, %esi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	BIO_ctrl@PLT
	.p2align 4,,10
	.p2align 3
.L427:
	.cfi_restore_state
	leaq	.LC0(%rip), %rcx
	jmp	.L407
	.p2align 4,,10
	.p2align 3
.L452:
	leaq	.LC7(%rip), %rdx
	leaq	.LC95(%rip), %r8
	movq	%rdx, %rax
	jmp	.L409
	.p2align 4,,10
	.p2align 3
.L429:
	leaq	.LC7(%rip), %rdx
	leaq	.LC96(%rip), %r8
	movq	%rdx, %rax
	jmp	.L409
	.p2align 4,,10
	.p2align 3
.L453:
	leaq	.LC103(%rip), %rsi
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L425:
	xorl	%r13d, %r13d
	leaq	.LC102(%rip), %r15
	.p2align 4,,10
	.p2align 3
.L424:
	testb	$15, %r13b
	jne	.L421
	testq	%r13, %r13
	je	.L421
	leaq	.LC101(%rip), %rsi
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L421:
	movzbl	(%r12,%r13), %edx
	movq	%r15, %rsi
	movq	%r14, %rdi
	xorl	%eax, %eax
	addq	$1, %r13
	call	BIO_printf@PLT
	cmpq	%r13, %rbx
	ja	.L424
	leaq	.LC21(%rip), %rsi
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L420
	.p2align 4,,10
	.p2align 3
.L411:
	testq	%rbx, %rbx
	je	.L417
	movq	handshakes(%rip), %rdx
	movzbl	(%r12), %esi
	testq	%rdx, %rdx
	je	.L436
	leaq	handshakes(%rip), %rax
	jmp	.L418
	.p2align 4,,10
	.p2align 3
.L454:
	movq	16(%rax), %rdx
	addq	$16, %rax
	testq	%rdx, %rdx
	je	.L436
.L418:
	cmpl	8(%rax), %esi
	jne	.L454
	leaq	.LC7(%rip), %rsi
	leaq	.LC99(%rip), %r8
.L415:
	pushq	%rsi
	movq	%rbx, %r9
	leaq	.LC100(%rip), %rsi
	movq	%r14, %rdi
	pushq	%rdx
	xorl	%eax, %eax
	movq	%r10, %rdx
	call	BIO_printf@PLT
	leaq	.LC103(%rip), %rsi
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	popq	%rax
	popq	%rdx
	jmp	.L425
.L417:
	leaq	.LC7(%rip), %rax
	xorl	%r9d, %r9d
	movq	%r10, %rdx
	movq	%r14, %rdi
	pushq	%rax
	leaq	.LC0(%rip), %rax
	leaq	.LC100(%rip), %rsi
	pushq	%rax
	leaq	.LC99(%rip), %r8
	xorl	%eax, %eax
	call	BIO_printf@PLT
	popq	%rcx
	popq	%rsi
	jmp	.L420
.L436:
	leaq	.LC7(%rip), %rsi
	leaq	.LC0(%rip), %rdx
	leaq	.LC99(%rip), %r8
	jmp	.L415
.L434:
	leaq	.LC98(%rip), %rsi
	leaq	.LC97(%rip), %r8
	jmp	.L415
	.cfi_endproc
.LFE1555:
	.size	msg_cb, .-msg_cb
	.section	.rodata.str1.1
.LC104:
	.string	"unknown"
.LC105:
	.string	"server"
.LC106:
	.string	"client"
	.section	.rodata.str1.8
	.align 8
.LC107:
	.string	"TLS %s extension \"%s\" (id=%d), len=%d\n"
	.text
	.p2align 4
	.globl	tlsext_cb
	.type	tlsext_cb, @function
tlsext_cb:
.LFB1556:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movl	%r8d, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%r9, %r12
	subq	$8, %rsp
	movq	tlsext_types(%rip), %rcx
	testq	%rcx, %rcx
	je	.L459
	leaq	tlsext_types(%rip), %rax
	jmp	.L457
	.p2align 4,,10
	.p2align 3
.L463:
	movq	16(%rax), %rcx
	addq	$16, %rax
	testq	%rcx, %rcx
	je	.L459
.L457:
	cmpl	8(%rax), %edx
	jne	.L463
.L456:
	testl	%esi, %esi
	leaq	.LC106(%rip), %rax
	movl	%r13d, %r9d
	movl	%edx, %r8d
	leaq	.LC105(%rip), %r10
	movq	%r12, %rdi
	leaq	.LC107(%rip), %rsi
	cmove	%rax, %r10
	xorl	%eax, %eax
	movq	%r10, %rdx
	call	BIO_printf@PLT
	movl	%r13d, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	BIO_dump@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	xorl	%ecx, %ecx
	popq	%r12
	xorl	%edx, %edx
	popq	%r13
	movl	$11, %esi
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	BIO_ctrl@PLT
	.p2align 4,,10
	.p2align 3
.L459:
	.cfi_restore_state
	leaq	.LC104(%rip), %rcx
	jmp	.L456
	.cfi_endproc
.LFE1556:
	.size	tlsext_cb, .-tlsext_cb
	.section	.rodata.str1.8
	.align 8
.LC108:
	.string	"error setting random cookie secret\n"
	.section	.rodata.str1.1
.LC109:
	.string	"memory full\n"
.LC110:
	.string	"Failed getting peer address\n"
.LC111:
	.string	"assertion failed: length != 0"
.LC112:
	.string	"cookie generate buffer"
	.text
	.p2align 4
	.globl	generate_cookie_callback
	.type	generate_cookie_callback, @function
generate_cookie_callback:
.LFB1557:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$40, %rsp
	movq	%rdx, -72(%rbp)
	movl	cookie_initialized(%rip), %r12d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -64(%rbp)
	testl	%r12d, %r12d
	jne	.L465
	movl	$16, %esi
	leaq	cookie_secret(%rip), %rdi
	call	RAND_bytes@PLT
	testl	%eax, %eax
	jle	.L476
	movl	$1, cookie_initialized(%rip)
.L465:
	movq	%r15, %rdi
	xorl	%r14d, %r14d
	call	SSL_is_dtls@PLT
	movq	ourpeer(%rip), %r13
	testl	%eax, %eax
	jne	.L477
.L470:
	leaq	-64(%rbp), %rdx
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	BIO_ADDR_rawaddress@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	je	.L478
	cmpq	$0, -64(%rbp)
	je	.L479
	movq	%r13, %rdi
	call	BIO_ADDR_rawport@PLT
	leaq	.LC112(%rip), %rsi
	movl	%eax, %r15d
	movq	-64(%rbp), %rax
	leaq	2(%rax), %rdi
	movq	%rdi, -64(%rbp)
	call	app_malloc@PLT
	xorl	%edx, %edx
	movq	%r13, %rdi
	movw	%r15w, (%rax)
	leaq	2(%rax), %rsi
	movq	%rax, %r12
	call	BIO_ADDR_rawaddress@PLT
	movq	-64(%rbp), %r15
	call	EVP_sha1@PLT
	subq	$8, %rsp
	pushq	-72(%rbp)
	movq	%r12, %rcx
	movq	%rbx, %r9
	movq	%r15, %r8
	movq	%rax, %rdi
	movl	$16, %edx
	leaq	cookie_secret(%rip), %rsi
	call	HMAC@PLT
	movl	$794, %edx
	movq	%r12, %rdi
	leaq	.LC71(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%r14, %rdi
	movl	$1, %r12d
	call	BIO_ADDR_free@PLT
	popq	%rax
	popq	%rdx
.L464:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L480
	leaq	-40(%rbp), %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L477:
	.cfi_restore_state
	call	BIO_ADDR_new@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L481
	movq	%r15, %rdi
	movq	%r14, %r13
	call	SSL_get_rbio@PLT
	movq	%r14, %rcx
	xorl	%edx, %edx
	movl	$46, %esi
	movq	%rax, %rdi
	call	BIO_ctrl@PLT
	jmp	.L470
	.p2align 4,,10
	.p2align 3
.L476:
	movq	bio_err(%rip), %rdi
	leaq	.LC108(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L464
	.p2align 4,,10
	.p2align 3
.L478:
	movq	bio_err(%rip), %rdi
	leaq	.LC110(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L464
	.p2align 4,,10
	.p2align 3
.L481:
	movq	bio_err(%rip), %rdi
	leaq	.LC109(%rip), %rsi
	xorl	%eax, %eax
	xorl	%r12d, %r12d
	call	BIO_printf@PLT
	jmp	.L464
.L480:
	call	__stack_chk_fail@PLT
.L479:
	movl	$782, %edx
	leaq	.LC71(%rip), %rsi
	leaq	.LC111(%rip), %rdi
	call	OPENSSL_die@PLT
	.cfi_endproc
.LFE1557:
	.size	generate_cookie_callback, .-generate_cookie_callback
	.p2align 4
	.globl	verify_cookie_callback
	.type	verify_cookie_callback, @function
verify_cookie_callback:
.LFB1558:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	cookie_initialized(%rip), %eax
	testl	%eax, %eax
	jne	.L494
.L482:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L495
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L494:
	.cfi_restore_state
	leaq	-112(%rbp), %r13
	movq	%rsi, %r12
	movl	%edx, %ebx
	movq	%r13, %rsi
	leaq	-116(%rbp), %rdx
	call	generate_cookie_callback
	testl	%eax, %eax
	je	.L482
	xorl	%eax, %eax
	cmpl	-116(%rbp), %ebx
	jne	.L482
	movl	%ebx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	memcmp@PLT
	testl	%eax, %eax
	sete	%al
	movzbl	%al, %eax
	jmp	.L482
.L495:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1558:
	.size	verify_cookie_callback, .-verify_cookie_callback
	.p2align 4
	.globl	generate_stateless_cookie_callback
	.type	generate_stateless_cookie_callback, @function
generate_stateless_cookie_callback:
.LFB1559:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdx, %rbx
	leaq	-28(%rbp), %rdx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	generate_cookie_callback
	movl	-28(%rbp), %ecx
	movq	%rcx, (%rbx)
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L499
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L499:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1559:
	.size	generate_stateless_cookie_callback, .-generate_stateless_cookie_callback
	.p2align 4
	.globl	verify_stateless_cookie_callback
	.type	verify_stateless_cookie_callback, @function
verify_stateless_cookie_callback:
.LFB1560:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	cookie_initialized(%rip), %eax
	testl	%eax, %eax
	jne	.L512
.L500:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L513
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L512:
	.cfi_restore_state
	leaq	-112(%rbp), %r13
	movq	%rsi, %r12
	movq	%rdx, %rbx
	movq	%r13, %rsi
	leaq	-116(%rbp), %rdx
	call	generate_cookie_callback
	testl	%eax, %eax
	je	.L500
	movl	-116(%rbp), %edx
	xorl	%eax, %eax
	cmpl	%ebx, %edx
	jne	.L500
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	memcmp@PLT
	testl	%eax, %eax
	sete	%al
	movzbl	%al, %eax
	jmp	.L500
.L513:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1560:
	.size	verify_stateless_cookie_callback, .-verify_stateless_cookie_callback
	.p2align 4
	.globl	ssl_ctx_set_excert
	.type	ssl_ctx_set_excert, @function
ssl_ctx_set_excert:
.LFB1563:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdx
	leaq	set_cert_cb(%rip), %rsi
	jmp	SSL_CTX_set_cert_cb@PLT
	.cfi_endproc
.LFE1563:
	.size	ssl_ctx_set_excert, .-ssl_ctx_set_excert
	.p2align 4
	.globl	ssl_excert_free
	.type	ssl_excert_free, @function
ssl_excert_free:
.LFB1565:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L523
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	.LC71(%rip), %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	X509_free@GOTPCREL(%rip), %r13
	.p2align 4,,10
	.p2align 3
.L517:
	movq	40(%rbx), %rdi
	call	X509_free@PLT
	movq	48(%rbx), %rdi
	call	EVP_PKEY_free@PLT
	movq	56(%rbx), %rdi
	movq	%r13, %rsi
	call	OPENSSL_sk_pop_free@PLT
	movq	%rbx, %rdi
	movq	72(%rbx), %rbx
	movq	%r12, %rsi
	movl	$983, %edx
	call	CRYPTO_free@PLT
	testq	%rbx, %rbx
	jne	.L517
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L523:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.cfi_endproc
.LFE1565:
	.size	ssl_excert_free, .-ssl_excert_free
	.section	.rodata.str1.1
.LC113:
	.string	"Missing filename\n"
.LC114:
	.string	"Server Certificate"
.LC115:
	.string	"Server Key"
.LC116:
	.string	"Server Chain"
	.text
	.p2align 4
	.globl	load_excert
	.type	load_excert, @function
load_excert:
.LFB1566:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	(%rdi), %rbx
	testq	%rbx, %rbx
	je	.L527
	movq	%rdi, %r13
	movq	8(%rbx), %rdi
	leaq	.LC114(%rip), %r12
	testq	%rdi, %rdi
	jne	.L528
	jmp	.L555
	.p2align 4,,10
	.p2align 3
.L556:
	movl	16(%rbx), %esi
	leaq	.LC115(%rip), %r9
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	call	load_key@PLT
	movq	%rax, 48(%rbx)
	testq	%rax, %rax
	je	.L534
.L557:
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L537
	xorl	%ecx, %ecx
	leaq	56(%rbx), %rsi
	movl	$32773, %edx
	leaq	.LC116(%rip), %r8
	call	load_certs@PLT
	testl	%eax, %eax
	je	.L534
.L537:
	movq	72(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L527
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L538
.L528:
	movl	(%rbx), %esi
	movq	%r12, %rdx
	call	load_cert@PLT
	movq	%rax, 40(%rbx)
	testq	%rax, %rax
	je	.L534
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.L556
	movq	8(%rbx), %rdi
	movl	(%rbx), %esi
	leaq	.LC115(%rip), %r9
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	call	load_key@PLT
	movq	%rax, 48(%rbx)
	testq	%rax, %rax
	jne	.L557
	.p2align 4,,10
	.p2align 3
.L534:
	xorl	%eax, %eax
.L526:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L527:
	.cfi_restore_state
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L555:
	.cfi_restore_state
	cmpq	$0, 72(%rbx)
	je	.L558
.L538:
	movq	bio_err(%rip), %rdi
	leaq	.LC113(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L558:
	.cfi_restore_state
	movq	X509_free@GOTPCREL(%rip), %r14
	leaq	.LC71(%rip), %r12
	.p2align 4,,10
	.p2align 3
.L529:
	movq	40(%rbx), %rdi
	call	X509_free@PLT
	movq	48(%rbx), %rdi
	call	EVP_PKEY_free@PLT
	movq	56(%rbx), %rdi
	movq	%r14, %rsi
	call	OPENSSL_sk_pop_free@PLT
	movq	%rbx, %rdi
	movq	72(%rbx), %rbx
	movq	%r12, %rsi
	movl	$983, %edx
	call	CRYPTO_free@PLT
	testq	%rbx, %rbx
	jne	.L529
	movq	$0, 0(%r13)
	movl	$1, %eax
	jmp	.L526
	.cfi_endproc
.LFE1566:
	.size	load_excert, .-load_excert
	.section	.rodata.str1.1
.LC117:
	.string	"prepend cert"
.LC118:
	.string	"%s: Key already specified\n"
.LC119:
	.string	"%s: Chain already specified\n"
	.text
	.p2align 4
	.globl	args_excert
	.type	args_excert, @function
args_excert:
.LFB1567:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	%edi, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	(%rsi), %r12
	movq	%rsi, %rbx
	testq	%r12, %r12
	je	.L581
.L560:
	leal	-1000(%r13), %edi
	cmpl	$7, %edi
	ja	.L579
	leaq	.L563(%rip), %rdx
	movslq	(%rdx,%rdi,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L563:
	.long	.L575-.L563
	.long	.L569-.L563
	.long	.L568-.L563
	.long	.L567-.L563
	.long	.L566-.L563
	.long	.L565-.L563
	.long	.L564-.L563
	.long	.L575-.L563
	.text
	.p2align 4,,10
	.p2align 3
.L575:
	xorl	%eax, %eax
.L559:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L564:
	.cfi_restore_state
	call	opt_arg@PLT
	leaq	16(%r12), %rdx
	movq	%rax, %rdi
.L580:
	movl	$2, %esi
	call	opt_format@PLT
	popq	%rbx
	popq	%r12
	testl	%eax, %eax
	popq	%r13
	popq	%r14
	setne	%al
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movzbl	%al, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L569:
	.cfi_restore_state
	cmpq	$0, 24(%r12)
	je	.L571
	call	opt_getprog@PLT
	movq	bio_err(%rip), %rdi
	leaq	.LC118(%rip), %rsi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L572:
	movq	bio_err(%rip), %rdi
	leaq	.LC71(%rip), %r13
	call	ERR_print_errors@PLT
	movq	X509_free@GOTPCREL(%rip), %r14
	.p2align 4,,10
	.p2align 3
.L574:
	movq	40(%r12), %rdi
	call	X509_free@PLT
	movq	48(%r12), %rdi
	call	EVP_PKEY_free@PLT
	movq	56(%r12), %rdi
	movq	%r14, %rsi
	call	OPENSSL_sk_pop_free@PLT
	movq	%r12, %rdi
	movq	72(%r12), %r12
	movq	%r13, %rsi
	movl	$983, %edx
	call	CRYPTO_free@PLT
	testq	%r12, %r12
	jne	.L574
	movq	$0, (%rbx)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L568:
	.cfi_restore_state
	cmpq	$0, 8(%r12)
	movq	%r12, %r13
	je	.L570
	movl	$88, %edi
	leaq	.LC117(%rip), %rsi
	call	app_malloc@PLT
	leaq	8(%rax), %rdi
	movq	%rax, %rcx
	movq	%rax, %r13
	movq	$0, (%rax)
	andq	$-8, %rdi
	movq	$0, 80(%rax)
	xorl	%eax, %eax
	subq	%rdi, %rcx
	addl	$88, %ecx
	shrl	$3, %ecx
	rep stosq
	movq	%r12, 72(%r13)
	movl	(%r12), %eax
	movl	%eax, 0(%r13)
	movl	16(%r12), %eax
	movl	%eax, 16(%r13)
	movq	%r13, 80(%r12)
.L570:
	movq	%r13, (%rbx)
	call	opt_arg@PLT
	movq	%rax, 8(%r13)
.L579:
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L567:
	.cfi_restore_state
	cmpq	$0, 32(%r12)
	je	.L573
	call	opt_getprog@PLT
	movq	bio_err(%rip), %rdi
	leaq	.LC119(%rip), %rsi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L572
	.p2align 4,,10
	.p2align 3
.L566:
	movl	$1, 64(%r12)
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L565:
	.cfi_restore_state
	call	opt_arg@PLT
	movq	%r12, %rdx
	movq	%rax, %rdi
	jmp	.L580
	.p2align 4,,10
	.p2align 3
.L581:
	movl	$88, %edi
	leaq	.LC117(%rip), %rsi
	call	app_malloc@PLT
	leaq	8(%rax), %rdi
	movq	%rax, %rcx
	movq	%rax, %r12
	movq	$0, (%rax)
	andq	$-8, %rdi
	movq	$0, 80(%rax)
	xorl	%eax, %eax
	subq	%rdi, %rcx
	addl	$88, %ecx
	shrl	$3, %ecx
	rep stosq
	movl	$32773, (%r12)
	movq	$0, 72(%r12)
	movl	$32773, 16(%r12)
	movq	%r12, (%rbx)
	jmp	.L560
	.p2align 4,,10
	.p2align 3
.L573:
	call	opt_arg@PLT
	movq	%rax, 32(%r12)
	movl	$1, %eax
	jmp	.L559
	.p2align 4,,10
	.p2align 3
.L571:
	call	opt_arg@PLT
	movq	%rax, 24(%r12)
	movl	$1, %eax
	jmp	.L559
	.cfi_endproc
.LFE1567:
	.size	args_excert, .-args_excert
	.section	.rodata.str1.1
.LC120:
	.string	"matched TA certificate"
.LC121:
	.string	"signed the certificate"
.LC122:
	.string	"matched EE certificate"
.LC123:
	.string	"..."
.LC124:
	.string	"Verification: OK\n"
.LC125:
	.string	"Verified peername: %s\n"
.LC126:
	.string	"Verification error: %s\n"
.LC127:
	.string	"TLSA hex data buffer"
.LC128:
	.string	"0123456789abcdef"
	.section	.rodata.str1.8
	.align 8
.LC129:
	.string	"DANE TLSA %d %d %d %s%s %s at depth %d\n"
	.text
	.p2align 4
	.globl	print_verify_detail
	.type	print_verify_detail, @function
print_verify_detail:
.LFB1570:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	SSL_get_verify_result@PLT
	testq	%rax, %rax
	jne	.L583
	movq	%r12, %rdi
	call	SSL_get0_peername@PLT
	leaq	.LC124(%rip), %rsi
	movq	%r13, %rdi
	movq	%rax, %r14
	xorl	%eax, %eax
	call	BIO_printf@PLT
	testq	%r14, %r14
	je	.L584
	movq	%r14, %rdx
	leaq	.LC125(%rip), %rsi
	movq	%r13, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L584:
	xorl	%esi, %esi
	leaq	-80(%rbp), %rdx
	movq	%r12, %rdi
	call	SSL_get0_dane_authority@PLT
	testl	%eax, %eax
	jns	.L635
.L582:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L636
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L583:
	.cfi_restore_state
	movq	%rax, %rdi
	call	X509_verify_cert_error_string@PLT
	leaq	.LC126(%rip), %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L584
	.p2align 4,,10
	.p2align 3
.L635:
	leaq	-81(%rbp), %rcx
	leaq	-82(%rbp), %rdx
	movq	%r12, %rdi
	movq	$0, -72(%rbp)
	leaq	-83(%rbp), %rsi
	leaq	-64(%rbp), %r9
	movq	$0, -64(%rbp)
	leaq	-72(%rbp), %r8
	call	SSL_get0_dane_tlsa@PLT
	movq	-64(%rbp), %rbx
	movl	%eax, %r15d
	cmpq	$12, %rbx
	jbe	.L586
	movq	-72(%rbp), %rax
	leaq	.LC127(%rip), %rsi
	movl	$25, %edi
	leaq	-12(%rax,%rbx), %rbx
	call	app_malloc@PLT
	movzbl	(%rbx), %edx
	movq	%rax, %r12
	leaq	.LC128(%rip), %rax
	shrb	$4, %dl
	movzbl	%dl, %edx
	movzbl	(%rax,%rdx), %edx
	movb	%dl, (%r12)
	movzbl	(%rbx), %edx
	andl	$15, %edx
	movzbl	(%rax,%rdx), %edx
	movb	%dl, 1(%r12)
	movzbl	1(%rbx), %edx
	shrb	$4, %dl
	movzbl	%dl, %edx
	movzbl	(%rax,%rdx), %edx
	movb	%dl, 2(%r12)
	movzbl	1(%rbx), %edx
	andl	$15, %edx
	movzbl	(%rax,%rdx), %edx
	movb	%dl, 3(%r12)
	movzbl	2(%rbx), %edx
	shrb	$4, %dl
	movzbl	%dl, %edx
	movzbl	(%rax,%rdx), %edx
	movb	%dl, 4(%r12)
	movzbl	2(%rbx), %edx
	andl	$15, %edx
	movzbl	(%rax,%rdx), %edx
	movb	%dl, 5(%r12)
	movzbl	3(%rbx), %edx
	shrb	$4, %dl
	movzbl	%dl, %edx
	movzbl	(%rax,%rdx), %edx
	movb	%dl, 6(%r12)
	movzbl	3(%rbx), %edx
	andl	$15, %edx
	movzbl	(%rax,%rdx), %edx
	movb	%dl, 7(%r12)
	movzbl	4(%rbx), %edx
	shrb	$4, %dl
	movzbl	%dl, %edx
	movzbl	(%rax,%rdx), %edx
	movb	%dl, 8(%r12)
	movzbl	4(%rbx), %edx
	andl	$15, %edx
	movzbl	(%rax,%rdx), %edx
	movb	%dl, 9(%r12)
	movzbl	5(%rbx), %edx
	shrb	$4, %dl
	movzbl	%dl, %edx
	movzbl	(%rax,%rdx), %edx
	movb	%dl, 10(%r12)
	movzbl	5(%rbx), %edx
	andl	$15, %edx
	movzbl	(%rax,%rdx), %edx
	movb	%dl, 11(%r12)
	movzbl	6(%rbx), %edx
	shrb	$4, %dl
	movzbl	%dl, %edx
	movzbl	(%rax,%rdx), %edx
	movb	%dl, 12(%r12)
	movzbl	6(%rbx), %edx
	andl	$15, %edx
	movzbl	(%rax,%rdx), %edx
	movb	%dl, 13(%r12)
	movzbl	7(%rbx), %edx
	shrb	$4, %dl
	movzbl	%dl, %edx
	movzbl	(%rax,%rdx), %edx
	movb	%dl, 14(%r12)
	movzbl	7(%rbx), %edx
	andl	$15, %edx
	movzbl	(%rax,%rdx), %edx
	movb	%dl, 15(%r12)
	movzbl	8(%rbx), %edx
	shrb	$4, %dl
	movzbl	%dl, %edx
	movzbl	(%rax,%rdx), %edx
	movb	%dl, 16(%r12)
	movzbl	8(%rbx), %edx
	andl	$15, %edx
	movzbl	(%rax,%rdx), %edx
	movb	%dl, 17(%r12)
	movzbl	9(%rbx), %edx
	shrb	$4, %dl
	movzbl	%dl, %edx
	movzbl	(%rax,%rdx), %edx
	movb	%dl, 18(%r12)
	movzbl	9(%rbx), %edx
	andl	$15, %edx
	movzbl	(%rax,%rdx), %edx
	movb	%dl, 19(%r12)
	movzbl	10(%rbx), %edx
	shrb	$4, %dl
	movzbl	%dl, %edx
	movzbl	(%rax,%rdx), %edx
	movb	%dl, 20(%r12)
	movzbl	10(%rbx), %edx
	andl	$15, %edx
	movzbl	(%rax,%rdx), %edx
	movb	%dl, 21(%r12)
	movzbl	11(%rbx), %edx
	shrb	$4, %dl
	movzbl	%dl, %edx
	movzbl	(%rax,%rdx), %edx
	movb	%dl, 22(%r12)
	movzbl	11(%rbx), %edx
	movb	$0, 24(%r12)
	andl	$15, %edx
	cmpq	$0, -80(%rbp)
	movzbl	(%rax,%rdx), %eax
	movb	%al, 23(%r12)
	leaq	.LC121(%rip), %rax
	je	.L637
.L591:
	cmpq	$12, -64(%rbp)
	leaq	.LC7(%rip), %rdx
	movzbl	-82(%rbp), %ecx
	movq	%r13, %rdi
	leaq	.LC123(%rip), %r9
	movzbl	-81(%rbp), %r8d
	leaq	.LC129(%rip), %rsi
	cmovbe	%rdx, %r9
	subq	$8, %rsp
	movzbl	-83(%rbp), %edx
	pushq	%r15
	pushq	%rax
	xorl	%eax, %eax
	pushq	%r12
	call	BIO_printf@PLT
	addq	$32, %rsp
	movl	$1190, %edx
	movq	%r12, %rdi
	leaq	.LC71(%rip), %rsi
	call	CRYPTO_free@PLT
	jmp	.L582
	.p2align 4,,10
	.p2align 3
.L586:
	leaq	(%rbx,%rbx), %rcx
	movq	-72(%rbp), %r14
	leaq	1(%rcx), %rdi
	cmpq	%rdi, %rbx
	ja	.L638
	leaq	.LC127(%rip), %rsi
	movq	%rcx, -104(%rbp)
	call	app_malloc@PLT
	movq	%rax, %r12
	testq	%rbx, %rbx
	je	.L594
	movzbl	(%r14), %edx
	leaq	.LC128(%rip), %rax
	movq	-104(%rbp), %rcx
	shrb	$4, %dl
	movzbl	%dl, %edx
	movzbl	(%rax,%rdx), %edx
	movb	%dl, (%r12)
	movzbl	(%r14), %edx
	andl	$15, %edx
	cmpq	$1, %rbx
	movzbl	(%rax,%rdx), %edx
	movb	%dl, 1(%r12)
	je	.L590
	movzbl	1(%r14), %edx
	shrb	$4, %dl
	movzbl	%dl, %edx
	movzbl	(%rax,%rdx), %edx
	movb	%dl, 2(%r12)
	movzbl	1(%r14), %edx
	andl	$15, %edx
	movzbl	(%rax,%rdx), %edx
	movb	%dl, 3(%r12)
	cmpq	$2, %rbx
	je	.L590
	movzbl	2(%r14), %edx
	shrb	$4, %dl
	movzbl	%dl, %edx
	movzbl	(%rax,%rdx), %edx
	movb	%dl, 4(%r12)
	movzbl	2(%r14), %edx
	andl	$15, %edx
	movzbl	(%rax,%rdx), %edx
	movb	%dl, 5(%r12)
	cmpq	$3, %rbx
	je	.L590
	movzbl	3(%r14), %edx
	shrb	$4, %dl
	movzbl	%dl, %edx
	movzbl	(%rax,%rdx), %edx
	movb	%dl, 6(%r12)
	movzbl	3(%r14), %edx
	andl	$15, %edx
	movzbl	(%rax,%rdx), %edx
	movb	%dl, 7(%r12)
	cmpq	$4, %rbx
	je	.L590
	movzbl	4(%r14), %edx
	shrb	$4, %dl
	movzbl	%dl, %edx
	movzbl	(%rax,%rdx), %edx
	movb	%dl, 8(%r12)
	movzbl	4(%r14), %edx
	andl	$15, %edx
	movzbl	(%rax,%rdx), %edx
	movb	%dl, 9(%r12)
	cmpq	$5, %rbx
	je	.L590
	movzbl	5(%r14), %edx
	shrb	$4, %dl
	movzbl	%dl, %edx
	movzbl	(%rax,%rdx), %edx
	movb	%dl, 10(%r12)
	movzbl	5(%r14), %edx
	andl	$15, %edx
	movzbl	(%rax,%rdx), %edx
	movb	%dl, 11(%r12)
	cmpq	$6, %rbx
	je	.L590
	movzbl	6(%r14), %edx
	shrb	$4, %dl
	movzbl	%dl, %edx
	movzbl	(%rax,%rdx), %edx
	movb	%dl, 12(%r12)
	movzbl	6(%r14), %edx
	andl	$15, %edx
	movzbl	(%rax,%rdx), %edx
	movb	%dl, 13(%r12)
	cmpq	$7, %rbx
	je	.L590
	movzbl	7(%r14), %edx
	shrb	$4, %dl
	movzbl	%dl, %edx
	movzbl	(%rax,%rdx), %edx
	movb	%dl, 14(%r12)
	movzbl	7(%r14), %edx
	andl	$15, %edx
	movzbl	(%rax,%rdx), %edx
	movb	%dl, 15(%r12)
	cmpq	$8, %rbx
	je	.L590
	movzbl	8(%r14), %edx
	shrb	$4, %dl
	movzbl	%dl, %edx
	movzbl	(%rax,%rdx), %edx
	movb	%dl, 16(%r12)
	movzbl	8(%r14), %edx
	andl	$15, %edx
	movzbl	(%rax,%rdx), %edx
	movb	%dl, 17(%r12)
	cmpq	$9, %rbx
	je	.L590
	movzbl	9(%r14), %edx
	shrb	$4, %dl
	movzbl	%dl, %edx
	movzbl	(%rax,%rdx), %edx
	movb	%dl, 18(%r12)
	movzbl	9(%r14), %edx
	andl	$15, %edx
	movzbl	(%rax,%rdx), %edx
	movb	%dl, 19(%r12)
	cmpq	$10, %rbx
	je	.L590
	movzbl	10(%r14), %edx
	shrb	$4, %dl
	movzbl	%dl, %edx
	movzbl	(%rax,%rdx), %edx
	movb	%dl, 20(%r12)
	movzbl	10(%r14), %edx
	andl	$15, %edx
	movzbl	(%rax,%rdx), %edx
	movb	%dl, 21(%r12)
	cmpq	$11, %rbx
	je	.L590
	movzbl	11(%r14), %edx
	shrb	$4, %dl
	movzbl	%dl, %edx
	movzbl	(%rax,%rdx), %edx
	movb	%dl, 22(%r12)
	movzbl	11(%r14), %edx
	andl	$15, %edx
	movzbl	(%rax,%rdx), %eax
	movb	%al, 23(%r12)
.L590:
	addq	%r12, %rcx
.L589:
	movb	$0, (%rcx)
	cmpq	$0, -80(%rbp)
	leaq	.LC121(%rip), %rax
	jne	.L591
.L637:
	testl	%r15d, %r15d
	leaq	.LC120(%rip), %rax
	leaq	.LC122(%rip), %rdx
	cmove	%rdx, %rax
	jmp	.L591
	.p2align 4,,10
	.p2align 3
.L594:
	movq	%rax, %rcx
	jmp	.L589
.L636:
	call	__stack_chk_fail@PLT
.L638:
	movq	%rbx, %rdi
	call	hexencode.part.0
	.cfi_endproc
.LFE1570:
	.size	print_verify_detail, .-print_verify_detail
	.section	.rodata.str1.1
.LC130:
	.string	"Protocol version: %s\n"
.LC131:
	.string	"assertion failed: num == 2"
.LC132:
	.string	"Client cipher list: "
.LC133:
	.string	"SCSV"
.LC134:
	.string	"0x"
.LC135:
	.string	"%02X"
.LC136:
	.string	"Ciphersuite: %s\n"
.LC137:
	.string	"Peer certificate: "
.LC138:
	.string	"Hash used: %s\n"
.LC139:
	.string	"Signature type: %s\n"
.LC140:
	.string	"No peer certificate\n"
	.text
	.p2align 4
	.globl	print_ssl_summary
	.type	print_ssl_summary, @function
print_ssl_summary:
.LFB1571:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	SSL_get_version@PLT
	movq	bio_err(%rip), %rdi
	leaq	.LC130(%rip), %rsi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	%r12, %rdi
	call	SSL_is_server@PLT
	testl	%eax, %eax
	jne	.L686
.L641:
	movq	%r12, %rdi
	call	SSL_get_current_cipher@PLT
	movq	%rax, %rdi
	call	SSL_CIPHER_get_name@PLT
	movq	bio_err(%rip), %rdi
	leaq	.LC136(%rip), %rsi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	xorl	%edx, %edx
	movq	%r12, %rsi
	call	do_print_sigalgs
	movq	%r12, %rdi
	call	SSL_get_peer_certificate@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L650
	movq	bio_err(%rip), %rdi
	leaq	.LC137(%rip), %rsi
	call	BIO_puts@PLT
	call	get_nameopt@PLT
	movq	%r13, %rdi
	movq	%rax, %r14
	call	X509_get_subject_name@PLT
	movq	bio_err(%rip), %rdi
	movq	%r14, %rcx
	xorl	%edx, %edx
	movq	%rax, %rsi
	leaq	-64(%rbp), %r14
	call	X509_NAME_print_ex@PLT
	movq	bio_err(%rip), %rdi
	leaq	.LC21(%rip), %rsi
	call	BIO_puts@PLT
	xorl	%edx, %edx
	movq	%r14, %rcx
	movl	$108, %esi
	movq	%r12, %rdi
	call	SSL_ctrl@PLT
	testq	%rax, %rax
	jne	.L687
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	SSL_get_peer_signature_type_nid@PLT
	testl	%eax, %eax
	jne	.L688
.L652:
	movq	bio_err(%rip), %rsi
	movq	%r12, %rdi
	call	print_verify_detail
.L657:
	movq	%r13, %rdi
	call	X509_free@PLT
	movq	bio_err(%rip), %rdi
	movq	%r12, %rsi
	call	ssl_print_point_formats
	movq	%r12, %rdi
	call	SSL_is_server@PLT
	testl	%eax, %eax
	jne	.L689
	movq	bio_err(%rip), %rdi
	movq	%r12, %rsi
	call	ssl_print_tmp_key
.L639:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L690
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L689:
	.cfi_restore_state
	movq	bio_err(%rip), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	call	ssl_print_groups
	jmp	.L639
	.p2align 4,,10
	.p2align 3
.L688:
	movl	-64(%rbp), %eax
	leaq	.LC34(%rip), %rdx
	cmpl	$912, %eax
	je	.L653
	jle	.L691
	leaq	.LC35(%rip), %rdx
	cmpl	$1087, %eax
	je	.L653
	jle	.L692
	cmpl	$1088, %eax
	leaq	.LC30(%rip), %rdx
	movl	$0, %eax
	cmovne	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L653:
	movq	bio_err(%rip), %rdi
	leaq	.LC139(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L652
	.p2align 4,,10
	.p2align 3
.L687:
	movl	-64(%rbp), %edi
	call	OBJ_nid2sn@PLT
	movq	bio_err(%rip), %rdi
	leaq	.LC138(%rip), %rsi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	SSL_get_peer_signature_type_nid@PLT
	testl	%eax, %eax
	je	.L652
	jmp	.L688
	.p2align 4,,10
	.p2align 3
.L691:
	leaq	.LC29(%rip), %rdx
	cmpl	$408, %eax
	je	.L653
	jle	.L693
	cmpl	$811, %eax
	leaq	.LC33(%rip), %rdx
	movl	$0, %eax
	cmovne	%rax, %rdx
	jmp	.L653
	.p2align 4,,10
	.p2align 3
.L693:
	leaq	.LC32(%rip), %rdx
	cmpl	$6, %eax
	je	.L653
	cmpl	$116, %eax
	leaq	.LC31(%rip), %rdx
	movl	$0, %eax
	cmovne	%rax, %rdx
	jmp	.L653
	.p2align 4,,10
	.p2align 3
.L692:
	leaq	.LC28(%rip), %rdx
	cmpl	$979, %eax
	je	.L653
	cmpl	$980, %eax
	leaq	.LC36(%rip), %rdx
	movl	$0, %eax
	cmovne	%rax, %rdx
	jmp	.L653
	.p2align 4,,10
	.p2align 3
.L686:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$110, %esi
	movq	%r12, %rdi
	call	SSL_ctrl@PLT
	cmpq	$2, %rax
	jne	.L694
	xorl	%edx, %edx
	movl	$110, %esi
	leaq	-64(%rbp), %rcx
	movq	%r12, %rdi
	call	SSL_ctrl@PLT
	movq	bio_err(%rip), %rdi
	leaq	.LC132(%rip), %rsi
	movq	%rax, %r14
	call	BIO_puts@PLT
	testq	%r14, %r14
	je	.L649
	movq	-64(%rbp), %rsi
	movq	%r12, %rdi
	xorl	%ebx, %ebx
	leaq	.LC134(%rip), %r15
	call	SSL_CIPHER_find@PLT
	movq	%rax, %r13
.L660:
	testq	%r13, %r13
	je	.L645
.L695:
	movq	%r13, %rdi
	call	SSL_CIPHER_get_name@PLT
	movq	bio_err(%rip), %rdi
	movq	%rax, %rsi
	call	BIO_puts@PLT
.L646:
	movq	-64(%rbp), %rax
	addq	$2, %rbx
	leaq	2(%rax), %rsi
	movq	%rsi, -64(%rbp)
	cmpq	%rbx, %r14
	jbe	.L649
	movq	%r12, %rdi
	call	SSL_CIPHER_find@PLT
	movq	%rax, %r13
	testq	%rbx, %rbx
	je	.L660
	movq	bio_err(%rip), %rdi
	leaq	.LC40(%rip), %rsi
	call	BIO_puts@PLT
	testq	%r13, %r13
	jne	.L695
.L645:
	movq	-64(%rbp), %rax
	movq	bio_err(%rip), %rdi
	cmpw	$-256, (%rax)
	jne	.L647
	leaq	.LC133(%rip), %rsi
	call	BIO_puts@PLT
	jmp	.L646
	.p2align 4,,10
	.p2align 3
.L647:
	movq	%r15, %rsi
	call	BIO_puts@PLT
	movq	-64(%rbp), %rax
	movq	bio_err(%rip), %rdi
	leaq	.LC135(%rip), %rsi
	movzbl	(%rax), %edx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	-64(%rbp), %rax
	movq	bio_err(%rip), %rdi
	leaq	.LC135(%rip), %rsi
	movzbl	1(%rax), %edx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L646
	.p2align 4,,10
	.p2align 3
.L649:
	movq	bio_err(%rip), %rdi
	leaq	.LC21(%rip), %rsi
	call	BIO_puts@PLT
	jmp	.L641
	.p2align 4,,10
	.p2align 3
.L650:
	movq	bio_err(%rip), %rdi
	leaq	.LC140(%rip), %rsi
	call	BIO_puts@PLT
	jmp	.L657
.L690:
	call	__stack_chk_fail@PLT
.L694:
	movl	$1099, %edx
	leaq	.LC71(%rip), %rsi
	leaq	.LC131(%rip), %rdi
	call	OPENSSL_die@PLT
	.cfi_endproc
.LFE1571:
	.size	print_ssl_summary, .-print_ssl_summary
	.section	.rodata.str1.1
.LC141:
	.string	"Error with command: \"%s %s\"\n"
.LC142:
	.string	"Error with command: \"%s\"\n"
.LC143:
	.string	"Error finishing context\n"
	.text
	.p2align 4
	.globl	config_ctx
	.type	config_ctx, @function
config_ctx:
.LFB1572:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	movq	%rdx, %rsi
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	xorl	%ebx, %ebx
	subq	$8, %rsp
	call	SSL_CONF_CTX_set_ssl_ctx@PLT
	jmp	.L697
	.p2align 4,,10
	.p2align 3
.L702:
	movl	%ebx, %esi
	movq	%r14, %rdi
	call	OPENSSL_sk_value@PLT
	leal	1(%rbx), %esi
	movq	%r14, %rdi
	movq	%rax, %r13
	call	OPENSSL_sk_value@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%rax, %rdx
	movq	%rax, %r12
	call	SSL_CONF_cmd@PLT
	testl	%eax, %eax
	jle	.L705
	addl	$2, %ebx
.L697:
	movq	%r14, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jl	.L702
	movq	%r15, %rdi
	call	SSL_CONF_CTX_finish@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	je	.L706
	movl	$1, %r12d
.L696:
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L705:
	.cfi_restore_state
	movq	bio_err(%rip), %rdi
	testq	%r12, %r12
	je	.L699
	movq	%r12, %rcx
	movq	%r13, %rdx
	leaq	.LC141(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L700:
	movq	bio_err(%rip), %rdi
	xorl	%r12d, %r12d
	call	ERR_print_errors@PLT
	jmp	.L696
	.p2align 4,,10
	.p2align 3
.L706:
	movq	bio_err(%rip), %rdi
	leaq	.LC143(%rip), %rsi
	call	BIO_puts@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	jmp	.L696
	.p2align 4,,10
	.p2align 3
.L699:
	movq	%r13, %rdx
	leaq	.LC142(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L700
	.cfi_endproc
.LFE1572:
	.size	config_ctx, .-config_ctx
	.p2align 4
	.globl	ssl_ctx_add_crls
	.type	ssl_ctx_add_crls, @function
ssl_ctx_add_crls:
.LFB1574:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%edx, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	xorl	%ebx, %ebx
	call	SSL_CTX_get_cert_store@PLT
	movq	%rax, %r13
	jmp	.L708
	.p2align 4,,10
	.p2align 3
.L709:
	movl	%ebx, %esi
	movq	%r12, %rdi
	addl	$1, %ebx
	call	OPENSSL_sk_value@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	X509_STORE_add_crl@PLT
.L708:
	movq	%r12, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jl	.L709
	testl	%r14d, %r14d
	jne	.L715
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L715:
	.cfi_restore_state
	movq	%r13, %rdi
	call	store_setup_crl_download@PLT
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1574:
	.size	ssl_ctx_add_crls, .-ssl_ctx_add_crls
	.p2align 4
	.globl	ssl_load_stores
	.type	ssl_load_stores, @function
ssl_load_stores:
.LFB1575:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -64(%rbp)
	movq	%r8, -56(%rbp)
	orq	%rdx, %r14
	jne	.L738
.L717:
	movq	-56(%rbp), %r12
	movl	$1, %r15d
	orq	%r13, %r12
	jne	.L739
.L718:
	movq	%r14, %rdi
	call	X509_STORE_free@PLT
	movq	%r12, %rdi
	call	X509_STORE_free@PLT
	addq	$24, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L738:
	.cfi_restore_state
	movq	%rsi, %r15
	movq	%rdx, %r12
	movq	%r9, %rbx
	call	X509_STORE_new@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L721
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%rax, %rdi
	xorl	%r12d, %r12d
	call	X509_STORE_load_locations@PLT
	movl	%eax, %r15d
	testl	%eax, %eax
	je	.L718
	jmp	.L719
	.p2align 4,,10
	.p2align 3
.L720:
	movl	%r12d, %esi
	movq	%rbx, %rdi
	addl	$1, %r12d
	call	OPENSSL_sk_value@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	X509_STORE_add_crl@PLT
.L719:
	movq	%rbx, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r12d
	jl	.L720
	movq	-64(%rbp), %rdi
	movq	%r14, %rcx
	movl	$1, %edx
	movl	$106, %esi
	call	SSL_CTX_ctrl@PLT
	movl	16(%rbp), %eax
	testl	%eax, %eax
	je	.L717
	movq	%r14, %rdi
	call	store_setup_crl_download@PLT
	jmp	.L717
	.p2align 4,,10
	.p2align 3
.L739:
	call	X509_STORE_new@PLT
	xorl	%r15d, %r15d
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L718
	movq	-56(%rbp), %rsi
	movq	%r13, %rdx
	movq	%rax, %rdi
	call	X509_STORE_load_locations@PLT
	movl	%eax, %r15d
	testl	%eax, %eax
	je	.L718
	movq	-64(%rbp), %rdi
	movq	%r12, %rcx
	movl	$1, %edx
	movl	$107, %esi
	movl	$1, %r15d
	call	SSL_CTX_ctrl@PLT
	jmp	.L718
	.p2align 4,,10
	.p2align 3
.L721:
	xorl	%r12d, %r12d
	xorl	%r15d, %r15d
	jmp	.L718
	.cfi_endproc
.LFE1575:
	.size	ssl_load_stores, .-ssl_load_stores
	.p2align 4
	.globl	ssl_ctx_security_debug
	.type	ssl_ctx_security_debug, @function
ssl_ctx_security_debug:
.LFB1577:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	bio_err(%rip), %rax
	movl	%esi, 8+sdb.27365(%rip)
	movq	%rax, sdb.27365(%rip)
	call	SSL_CTX_get_security_callback@PLT
	movq	%r12, %rdi
	leaq	security_callback_debug(%rip), %rsi
	movq	%rax, 16+sdb.27365(%rip)
	call	SSL_CTX_set_security_callback@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	leaq	sdb.27365(%rip), %rsi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	SSL_CTX_set0_security_ex_data@PLT
	.cfi_endproc
.LFE1577:
	.size	ssl_ctx_security_debug, .-ssl_ctx_security_debug
	.section	.rodata.str1.1
.LC144:
	.string	"a"
.LC145:
	.string	"Error writing keylog file %s\n"
	.section	.rodata.str1.8
	.align 8
.LC146:
	.string	"# SSL/TLS secrets log file, generated by OpenSSL\n"
	.text
	.p2align 4
	.globl	set_keylog_file
	.type	set_keylog_file, @function
set_keylog_file:
.LFB1579:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	bio_keylog(%rip), %rdi
	call	BIO_free_all@PLT
	movq	$0, bio_keylog(%rip)
	testq	%r12, %r12
	je	.L746
	testq	%r13, %r13
	je	.L746
	movq	%r13, %rdi
	leaq	.LC144(%rip), %rsi
	call	BIO_new_file@PLT
	movq	%rax, bio_keylog(%rip)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L748
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$133, %esi
	call	BIO_ctrl@PLT
	testl	%eax, %eax
	je	.L749
.L745:
	movq	%r12, %rdi
	leaq	keylog_callback(%rip), %rsi
	call	SSL_CTX_set_keylog_callback@PLT
	popq	%r12
	xorl	%eax, %eax
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L746:
	.cfi_restore_state
	xorl	%eax, %eax
.L742:
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L749:
	.cfi_restore_state
	movq	bio_keylog(%rip), %rdi
	leaq	.LC146(%rip), %rsi
	call	BIO_puts@PLT
	movq	bio_keylog(%rip), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	call	BIO_ctrl@PLT
	jmp	.L745
	.p2align 4,,10
	.p2align 3
.L748:
	movq	bio_err(%rip), %rdi
	movq	%r13, %rdx
	leaq	.LC145(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movl	$1, %eax
	jmp	.L742
	.cfi_endproc
.LFE1579:
	.size	set_keylog_file, .-set_keylog_file
	.section	.rodata.str1.8
	.align 8
.LC147:
	.string	"---\nNo %s certificate CA names sent\n"
	.align 8
.LC148:
	.string	"---\nAcceptable %s certificate CA names\n"
	.text
	.p2align 4
	.globl	print_ca_names
	.type	print_ca_names, @function
print_ca_names:
.LFB1580:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%rsi, %rdi
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	call	SSL_is_server@PLT
	leaq	.LC105(%rip), %rdx
	movq	%r14, %rdi
	testl	%eax, %eax
	leaq	.LC106(%rip), %rax
	cmovne	%rdx, %rax
	movq	%rax, %r15
	call	SSL_get0_peer_CA_list@PLT
	testq	%rax, %rax
	je	.L755
	movq	%rax, %rdi
	movq	%rax, %r12
	call	OPENSSL_sk_num@PLT
	testl	%eax, %eax
	jne	.L766
.L755:
	movq	%r14, %rdi
	call	SSL_is_server@PLT
	testl	%eax, %eax
	je	.L767
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L766:
	.cfi_restore_state
	movq	%r15, %rdx
	leaq	.LC148(%rip), %rsi
	movq	%r13, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	xorl	%ebx, %ebx
	leaq	.LC21(%rip), %r14
	jmp	.L758
	.p2align 4,,10
	.p2align 3
.L759:
	call	get_nameopt@PLT
	movl	%ebx, %esi
	movq	%r12, %rdi
	addl	$1, %ebx
	movq	%rax, %r15
	call	OPENSSL_sk_value@PLT
	movq	%r15, %rcx
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	X509_NAME_print_ex@PLT
	movl	$1, %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	BIO_write@PLT
.L758:
	movq	%r12, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jl	.L759
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L767:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r15, %rdx
	movq	%r13, %rdi
	xorl	%eax, %eax
	popq	%rbx
	leaq	.LC147(%rip), %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	BIO_printf@PLT
	.cfi_endproc
.LFE1580:
	.size	print_ca_names, .-print_ca_names
	.local	sdb.27365
	.comm	sdb.27365,24,16
	.section	.rodata.str1.1
.LC149:
	.string	"Supported Ciphersuite"
.LC150:
	.string	"Shared Ciphersuite"
.LC151:
	.string	"Check Ciphersuite"
.LC152:
	.string	"Temp DH key bits"
.LC153:
	.string	"Supported Curve"
.LC154:
	.string	"Shared Curve"
.LC155:
	.string	"Check Curve"
.LC156:
	.string	"Supported Signature Algorithm"
.LC157:
	.string	"Shared Signature Algorithm"
.LC158:
	.string	"Check Signature Algorithm"
.LC159:
	.string	"Signature Algorithm mask"
.LC160:
	.string	"Certificate chain EE key"
.LC161:
	.string	"Certificate chain CA key"
.LC162:
	.string	"Peer Chain EE key"
.LC163:
	.string	"Peer Chain CA key"
.LC164:
	.string	"Certificate chain CA digest"
.LC165:
	.string	"Peer chain CA digest"
.LC166:
	.string	"SSL compression"
.LC167:
	.string	"Session ticket"
	.section	.data.rel.local,"aw"
	.align 32
	.type	callback_types, @object
	.size	callback_types, 320
callback_types:
	.quad	.LC149
	.long	65537
	.zero	4
	.quad	.LC150
	.long	65538
	.zero	4
	.quad	.LC151
	.long	65539
	.zero	4
	.quad	.LC152
	.long	262151
	.zero	4
	.quad	.LC153
	.long	131076
	.zero	4
	.quad	.LC154
	.long	131077
	.zero	4
	.quad	.LC155
	.long	131078
	.zero	4
	.quad	.LC156
	.long	327691
	.zero	4
	.quad	.LC157
	.long	327692
	.zero	4
	.quad	.LC158
	.long	327693
	.zero	4
	.quad	.LC159
	.long	327694
	.zero	4
	.quad	.LC160
	.long	393232
	.zero	4
	.quad	.LC161
	.long	393233
	.zero	4
	.quad	.LC162
	.long	397328
	.zero	4
	.quad	.LC163
	.long	397329
	.zero	4
	.quad	.LC164
	.long	393234
	.zero	4
	.quad	.LC165
	.long	397330
	.zero	4
	.quad	.LC166
	.long	15
	.zero	4
	.quad	.LC167
	.long	10
	.zero	4
	.quad	0
	.zero	8
	.section	.rodata.str1.1
.LC168:
	.string	"Overall Validity"
.LC169:
	.string	"Sign with EE key"
.LC170:
	.string	"EE signature"
.LC171:
	.string	"CA signature"
.LC172:
	.string	"EE key parameters"
.LC173:
	.string	"CA key parameters"
.LC174:
	.string	"Explicitly sign with EE key"
.LC175:
	.string	"Issuer Name"
.LC176:
	.string	"Certificate Type"
	.section	.data.rel.local
	.align 32
	.type	chain_flags, @object
	.size	chain_flags, 160
chain_flags:
	.quad	.LC168
	.long	1
	.zero	4
	.quad	.LC169
	.long	2
	.zero	4
	.quad	.LC170
	.long	16
	.zero	4
	.quad	.LC171
	.long	32
	.zero	4
	.quad	.LC172
	.long	64
	.zero	4
	.quad	.LC173
	.long	128
	.zero	4
	.quad	.LC174
	.long	256
	.zero	4
	.quad	.LC175
	.long	512
	.zero	4
	.quad	.LC176
	.long	1024
	.zero	4
	.quad	0
	.zero	8
	.section	.rodata.str1.1
.LC177:
	.string	"none"
.LC178:
	.string	"MD5"
.LC179:
	.string	"SHA1"
.LC180:
	.string	"SHA224"
.LC181:
	.string	"SHA256"
.LC182:
	.string	"SHA384"
.LC183:
	.string	"SHA512"
	.section	.data.rel.local
	.align 32
	.type	signature_tls12_hash_list, @object
	.size	signature_tls12_hash_list, 128
signature_tls12_hash_list:
	.quad	.LC177
	.long	0
	.zero	4
	.quad	.LC178
	.long	1
	.zero	4
	.quad	.LC179
	.long	2
	.zero	4
	.quad	.LC180
	.long	3
	.zero	4
	.quad	.LC181
	.long	4
	.zero	4
	.quad	.LC182
	.long	5
	.zero	4
	.quad	.LC183
	.long	6
	.zero	4
	.quad	0
	.zero	8
	.section	.rodata.str1.1
.LC184:
	.string	"anonymous"
	.section	.data.rel.local
	.align 32
	.type	signature_tls12_alg_list, @object
	.size	signature_tls12_alg_list, 80
signature_tls12_alg_list:
	.quad	.LC184
	.long	0
	.zero	4
	.quad	.LC32
	.long	1
	.zero	4
	.quad	.LC31
	.long	2
	.zero	4
	.quad	.LC29
	.long	3
	.zero	4
	.quad	0
	.zero	8
	.section	.rodata.str1.1
.LC185:
	.string	"rsa_pkcs1_sha1"
.LC186:
	.string	"ecdsa_sha1"
.LC187:
	.string	"rsa_pkcs1_sha256"
.LC188:
	.string	"ecdsa_secp256r1_sha256"
.LC189:
	.string	"rsa_pkcs1_sha384"
.LC190:
	.string	"ecdsa_secp384r1_sha384"
.LC191:
	.string	"rsa_pkcs1_sha512"
.LC192:
	.string	"ecdsa_secp521r1_sha512"
.LC193:
	.string	"rsa_pss_rsae_sha256"
.LC194:
	.string	"rsa_pss_rsae_sha384"
.LC195:
	.string	"rsa_pss_rsae_sha512"
.LC196:
	.string	"ed25519"
.LC197:
	.string	"ed448"
.LC198:
	.string	"rsa_pss_pss_sha256"
.LC199:
	.string	"rsa_pss_pss_sha384"
.LC200:
	.string	"rsa_pss_pss_sha512"
.LC201:
	.string	"gostr34102001"
.LC202:
	.string	"gostr34102012_256"
.LC203:
	.string	"gostr34102012_512"
	.section	.data.rel.local
	.align 32
	.type	signature_tls13_scheme_list, @object
	.size	signature_tls13_scheme_list, 320
signature_tls13_scheme_list:
	.quad	.LC185
	.long	513
	.zero	4
	.quad	.LC186
	.long	515
	.zero	4
	.quad	.LC187
	.long	1025
	.zero	4
	.quad	.LC188
	.long	1027
	.zero	4
	.quad	.LC189
	.long	1281
	.zero	4
	.quad	.LC190
	.long	1283
	.zero	4
	.quad	.LC191
	.long	1537
	.zero	4
	.quad	.LC192
	.long	1539
	.zero	4
	.quad	.LC193
	.long	2052
	.zero	4
	.quad	.LC194
	.long	2053
	.zero	4
	.quad	.LC195
	.long	2054
	.zero	4
	.quad	.LC196
	.long	2055
	.zero	4
	.quad	.LC197
	.long	2056
	.zero	4
	.quad	.LC198
	.long	2057
	.zero	4
	.quad	.LC199
	.long	2058
	.zero	4
	.quad	.LC200
	.long	2059
	.zero	4
	.quad	.LC201
	.long	60909
	.zero	4
	.quad	.LC202
	.long	61166
	.zero	4
	.quad	.LC203
	.long	61423
	.zero	4
	.quad	0
	.zero	8
	.section	.rodata.str1.1
.LC204:
	.string	"server name"
.LC205:
	.string	"max fragment length"
.LC206:
	.string	"client certificate URL"
.LC207:
	.string	"trusted CA keys"
.LC208:
	.string	"truncated HMAC"
.LC209:
	.string	"status request"
.LC210:
	.string	"user mapping"
.LC211:
	.string	"client authz"
.LC212:
	.string	"server authz"
.LC213:
	.string	"cert type"
.LC214:
	.string	"supported_groups"
.LC215:
	.string	"EC point formats"
.LC216:
	.string	"SRP"
.LC217:
	.string	"signature algorithms"
.LC218:
	.string	"use SRTP"
.LC219:
	.string	"heartbeat"
.LC220:
	.string	"session ticket"
.LC221:
	.string	"renegotiation info"
.LC222:
	.string	"signed certificate timestamps"
.LC223:
	.string	"TLS padding"
.LC224:
	.string	"next protocol"
.LC225:
	.string	"encrypt-then-mac"
	.section	.rodata.str1.8
	.align 8
.LC226:
	.string	"application layer protocol negotiation"
	.section	.rodata.str1.1
.LC227:
	.string	"extended master secret"
.LC228:
	.string	"key share"
.LC229:
	.string	"supported versions"
.LC230:
	.string	"psk"
.LC231:
	.string	"psk kex modes"
.LC232:
	.string	"certificate authorities"
.LC233:
	.string	"post handshake auth"
	.section	.data.rel.local
	.align 32
	.type	tlsext_types, @object
	.size	tlsext_types, 496
tlsext_types:
	.quad	.LC204
	.long	0
	.zero	4
	.quad	.LC205
	.long	1
	.zero	4
	.quad	.LC206
	.long	2
	.zero	4
	.quad	.LC207
	.long	3
	.zero	4
	.quad	.LC208
	.long	4
	.zero	4
	.quad	.LC209
	.long	5
	.zero	4
	.quad	.LC210
	.long	6
	.zero	4
	.quad	.LC211
	.long	7
	.zero	4
	.quad	.LC212
	.long	8
	.zero	4
	.quad	.LC213
	.long	9
	.zero	4
	.quad	.LC214
	.long	10
	.zero	4
	.quad	.LC215
	.long	11
	.zero	4
	.quad	.LC216
	.long	12
	.zero	4
	.quad	.LC217
	.long	13
	.zero	4
	.quad	.LC218
	.long	14
	.zero	4
	.quad	.LC219
	.long	15
	.zero	4
	.quad	.LC220
	.long	35
	.zero	4
	.quad	.LC221
	.long	65281
	.zero	4
	.quad	.LC222
	.long	18
	.zero	4
	.quad	.LC223
	.long	21
	.zero	4
	.quad	.LC224
	.long	13172
	.zero	4
	.quad	.LC225
	.long	22
	.zero	4
	.quad	.LC226
	.long	16
	.zero	4
	.quad	.LC227
	.long	23
	.zero	4
	.quad	.LC228
	.long	51
	.zero	4
	.quad	.LC229
	.long	43
	.zero	4
	.quad	.LC230
	.long	41
	.zero	4
	.quad	.LC231
	.long	45
	.zero	4
	.quad	.LC232
	.long	47
	.zero	4
	.quad	.LC233
	.long	49
	.zero	4
	.quad	0
	.zero	8
	.section	.rodata.str1.1
.LC234:
	.string	", HelloRequest"
.LC235:
	.string	", ClientHello"
.LC236:
	.string	", ServerHello"
.LC237:
	.string	", HelloVerifyRequest"
.LC238:
	.string	", NewSessionTicket"
.LC239:
	.string	", EndOfEarlyData"
.LC240:
	.string	", EncryptedExtensions"
.LC241:
	.string	", Certificate"
.LC242:
	.string	", ServerKeyExchange"
.LC243:
	.string	", CertificateRequest"
.LC244:
	.string	", ServerHelloDone"
.LC245:
	.string	", CertificateVerify"
.LC246:
	.string	", ClientKeyExchange"
.LC247:
	.string	", Finished"
.LC248:
	.string	", CertificateUrl"
.LC249:
	.string	", CertificateStatus"
.LC250:
	.string	", SupplementalData"
.LC251:
	.string	", KeyUpdate"
.LC252:
	.string	", NextProto"
.LC253:
	.string	", MessageHash"
	.section	.data.rel.local
	.align 32
	.type	handshakes, @object
	.size	handshakes, 336
handshakes:
	.quad	.LC234
	.long	0
	.zero	4
	.quad	.LC235
	.long	1
	.zero	4
	.quad	.LC236
	.long	2
	.zero	4
	.quad	.LC237
	.long	3
	.zero	4
	.quad	.LC238
	.long	4
	.zero	4
	.quad	.LC239
	.long	5
	.zero	4
	.quad	.LC240
	.long	8
	.zero	4
	.quad	.LC241
	.long	11
	.zero	4
	.quad	.LC242
	.long	12
	.zero	4
	.quad	.LC243
	.long	13
	.zero	4
	.quad	.LC244
	.long	14
	.zero	4
	.quad	.LC245
	.long	15
	.zero	4
	.quad	.LC246
	.long	16
	.zero	4
	.quad	.LC247
	.long	20
	.zero	4
	.quad	.LC248
	.long	21
	.zero	4
	.quad	.LC249
	.long	22
	.zero	4
	.quad	.LC250
	.long	23
	.zero	4
	.quad	.LC251
	.long	24
	.zero	4
	.quad	.LC252
	.long	67
	.zero	4
	.quad	.LC253
	.long	254
	.zero	4
	.quad	0
	.zero	8
	.section	.rodata.str1.1
.LC254:
	.string	" close_notify"
.LC255:
	.string	" end_of_early_data"
.LC256:
	.string	" unexpected_message"
.LC257:
	.string	" bad_record_mac"
.LC258:
	.string	" decryption_failed"
.LC259:
	.string	" record_overflow"
.LC260:
	.string	" decompression_failure"
.LC261:
	.string	" handshake_failure"
.LC262:
	.string	" bad_certificate"
.LC263:
	.string	" unsupported_certificate"
.LC264:
	.string	" certificate_revoked"
.LC265:
	.string	" certificate_expired"
.LC266:
	.string	" certificate_unknown"
.LC267:
	.string	" illegal_parameter"
.LC268:
	.string	" unknown_ca"
.LC269:
	.string	" access_denied"
.LC270:
	.string	" decode_error"
.LC271:
	.string	" decrypt_error"
.LC272:
	.string	" export_restriction"
.LC273:
	.string	" protocol_version"
.LC274:
	.string	" insufficient_security"
.LC275:
	.string	" internal_error"
.LC276:
	.string	" inappropriate_fallback"
.LC277:
	.string	" user_canceled"
.LC278:
	.string	" no_renegotiation"
.LC279:
	.string	" missing_extension"
.LC280:
	.string	" unsupported_extension"
.LC281:
	.string	" certificate_unobtainable"
.LC282:
	.string	" unrecognized_name"
	.section	.rodata.str1.8
	.align 8
.LC283:
	.string	" bad_certificate_status_response"
	.section	.rodata.str1.1
.LC284:
	.string	" bad_certificate_hash_value"
.LC285:
	.string	" unknown_psk_identity"
.LC286:
	.string	" certificate_required"
	.section	.data.rel.local
	.align 32
	.type	alert_types, @object
	.size	alert_types, 544
alert_types:
	.quad	.LC254
	.long	0
	.zero	4
	.quad	.LC255
	.long	1
	.zero	4
	.quad	.LC256
	.long	10
	.zero	4
	.quad	.LC257
	.long	20
	.zero	4
	.quad	.LC258
	.long	21
	.zero	4
	.quad	.LC259
	.long	22
	.zero	4
	.quad	.LC260
	.long	30
	.zero	4
	.quad	.LC261
	.long	40
	.zero	4
	.quad	.LC262
	.long	42
	.zero	4
	.quad	.LC263
	.long	43
	.zero	4
	.quad	.LC264
	.long	44
	.zero	4
	.quad	.LC265
	.long	45
	.zero	4
	.quad	.LC266
	.long	46
	.zero	4
	.quad	.LC267
	.long	47
	.zero	4
	.quad	.LC268
	.long	48
	.zero	4
	.quad	.LC269
	.long	49
	.zero	4
	.quad	.LC270
	.long	50
	.zero	4
	.quad	.LC271
	.long	51
	.zero	4
	.quad	.LC272
	.long	60
	.zero	4
	.quad	.LC273
	.long	70
	.zero	4
	.quad	.LC274
	.long	71
	.zero	4
	.quad	.LC275
	.long	80
	.zero	4
	.quad	.LC276
	.long	86
	.zero	4
	.quad	.LC277
	.long	90
	.zero	4
	.quad	.LC278
	.long	100
	.zero	4
	.quad	.LC279
	.long	109
	.zero	4
	.quad	.LC280
	.long	110
	.zero	4
	.quad	.LC281
	.long	111
	.zero	4
	.quad	.LC282
	.long	112
	.zero	4
	.quad	.LC283
	.long	113
	.zero	4
	.quad	.LC284
	.long	114
	.zero	4
	.quad	.LC285
	.long	115
	.zero	4
	.quad	.LC286
	.long	116
	.zero	4
	.quad	0
	.zero	8
	.section	.rodata.str1.1
.LC287:
	.string	"SSL 3.0"
.LC288:
	.string	"TLS 1.0"
.LC289:
	.string	"TLS 1.1"
.LC290:
	.string	"TLS 1.2"
.LC291:
	.string	"TLS 1.3"
.LC292:
	.string	"DTLS 1.0"
.LC293:
	.string	"DTLS 1.0 (bad)"
	.section	.data.rel.local
	.align 32
	.type	ssl_versions, @object
	.size	ssl_versions, 128
ssl_versions:
	.quad	.LC287
	.long	768
	.zero	4
	.quad	.LC288
	.long	769
	.zero	4
	.quad	.LC289
	.long	770
	.zero	4
	.quad	.LC290
	.long	771
	.zero	4
	.quad	.LC291
	.long	772
	.zero	4
	.quad	.LC292
	.long	65279
	.zero	4
	.quad	.LC293
	.long	256
	.zero	4
	.quad	0
	.zero	8
	.section	.rodata.str1.1
.LC294:
	.string	"RSA sign"
.LC295:
	.string	"DSA sign"
.LC296:
	.string	"RSA fixed DH"
.LC297:
	.string	"DSS fixed DH"
.LC298:
	.string	"ECDSA sign"
.LC299:
	.string	"RSA fixed ECDH"
.LC300:
	.string	"ECDSA fixed ECDH"
.LC301:
	.string	"GOST01 Sign"
.LC302:
	.string	"GOST12 Sign"
	.section	.data.rel.local
	.align 32
	.type	cert_type_list, @object
	.size	cert_type_list, 160
cert_type_list:
	.quad	.LC294
	.long	1
	.zero	4
	.quad	.LC295
	.long	2
	.zero	4
	.quad	.LC296
	.long	3
	.zero	4
	.quad	.LC297
	.long	4
	.zero	4
	.quad	.LC298
	.long	64
	.zero	4
	.quad	.LC299
	.long	65
	.zero	4
	.quad	.LC300
	.long	66
	.zero	4
	.quad	.LC301
	.long	22
	.zero	4
	.quad	.LC302
	.long	238
	.zero	4
	.quad	0
	.zero	8
	.local	bio_keylog
	.comm	bio_keylog,8,8
	.local	cookie_initialized
	.comm	cookie_initialized,4,4
	.local	cookie_secret
	.comm	cookie_secret,16,16
	.globl	verify_args
	.data
	.align 16
	.type	verify_args, @object
	.size	verify_args, 16
verify_args:
	.long	-1
	.long	0
	.long	0
	.long	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
