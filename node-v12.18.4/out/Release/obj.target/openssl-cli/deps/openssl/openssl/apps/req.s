	.file	"req.c"
	.text
	.p2align 4
	.type	ext_name_cmp, @function
ext_name_cmp:
.LFB1438:
	.cfi_startproc
	endbr64
	jmp	strcmp@PLT
	.cfi_endproc
.LFE1438:
	.size	ext_name_cmp, .-ext_name_cmp
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/apps/req.c"
	.text
	.p2align 4
	.type	exts_cleanup, @function
exts_cleanup:
.LFB1439:
	.cfi_startproc
	endbr64
	movl	$168, %edx
	leaq	.LC0(%rip), %rsi
	jmp	CRYPTO_free@PLT
	.cfi_endproc
.LFE1439:
	.size	exts_cleanup, .-exts_cleanup
	.p2align 4
	.type	genpkey_cb, @function
genpkey_cb:
.LFB1453:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	subq	$16, %rsp
	.cfi_offset 12, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movb	$42, -25(%rbp)
	call	EVP_PKEY_CTX_get_app_data@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	movq	%rax, %r12
	call	EVP_PKEY_CTX_get_keygen_info@PLT
	testl	%eax, %eax
	jne	.L5
	movb	$46, -25(%rbp)
.L6:
	leaq	-25(%rbp), %rsi
	movl	$1, %edx
	movq	%r12, %rdi
	call	BIO_write@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	movq	%r12, %rdi
	call	BIO_ctrl@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L14
	addq	$16, %rsp
	movl	$1, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	.cfi_restore_state
	cmpl	$1, %eax
	jne	.L15
	movb	$43, -25(%rbp)
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L15:
	cmpl	$2, %eax
	jne	.L16
	movb	$42, -25(%rbp)
	jmp	.L6
.L16:
	cmpl	$3, %eax
	jne	.L6
	movb	$10, -25(%rbp)
	jmp	.L6
.L14:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1453:
	.size	genpkey_cb, .-genpkey_cb
	.p2align 4
	.type	ext_name_hash, @function
ext_name_hash:
.LFB1437:
	.cfi_startproc
	endbr64
	jmp	OPENSSL_LH_strhash@PLT
	.cfi_endproc
.LFE1437:
	.size	ext_name_hash, .-ext_name_hash
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"param:"
.LC2:
	.string	"Unknown algorithm %.*s\n"
.LC3:
	.string	"r"
.LC4:
	.string	"Can't open parameter file %s\n"
	.section	.rodata.str1.8
	.align 8
.LC5:
	.string	"Error reading parameter file %s\n"
	.align 8
.LC6:
	.string	"Key Type does not match parameters\n"
	.align 8
.LC7:
	.string	"Internal error: can't find key algorithm\n"
	.align 8
.LC8:
	.string	"Error allocating keygen context\n"
	.align 8
.LC9:
	.string	"Error initializing keygen context\n"
	.section	.rodata.str1.1
.LC10:
	.string	"Error setting RSA keysize\n"
	.text
	.p2align 4
	.type	set_keygen_ctx, @function
set_keygen_ctx:
.LFB1452:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$72, %rsp
	movq	%rdx, -88(%rbp)
	movq	%rcx, -96(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L56
	movzbl	(%rdi), %eax
	subl	$48, %eax
	cmpb	$9, %al
	jbe	.L57
	movl	$6, %ecx
	leaq	.LC1(%rip), %rdi
	movq	%r12, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L22
	leaq	6(%r12), %rax
	movq	%rax, -104(%rbp)
.L23:
	movq	-104(%rbp), %rdi
	leaq	.LC3(%rip), %rsi
	call	BIO_new_file@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L58
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	PEM_read_bio_Parameters@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L59
	movq	%r13, %rdi
	call	BIO_free@PLT
.L35:
	movl	(%rbx), %r15d
	movq	%r12, %rdi
	cmpl	$-1, %r15d
	je	.L60
	call	EVP_PKEY_base_id@PLT
	cmpl	%eax, %r15d
	jne	.L61
	movl	(%rbx), %esi
	movq	$-1, %r15
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L57:
	movl	$6, (%rsi)
	movl	$10, %edx
	xorl	%esi, %esi
.L54:
	call	strtol@PLT
	movl	(%rbx), %esi
	xorl	%r12d, %r12d
	movq	%rax, %r15
	movq	-88(%rbp), %rax
	movq	%r15, (%rax)
.L20:
	leaq	-72(%rbp), %rdi
	call	EVP_PKEY_asn1_find@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L62
	movq	%r13, %r9
	leaq	-64(%rbp), %r8
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	EVP_PKEY_asn1_get0_info@PLT
	movq	-64(%rbp), %rdi
	movl	$1555, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_strdup@PLT
	movq	-96(%rbp), %rcx
	movq	-72(%rbp), %rdi
	movq	%rax, (%rcx)
	call	ENGINE_finish@PLT
	movq	%r14, %rsi
	testq	%r12, %r12
	je	.L38
	movq	%r12, %rdi
	call	EVP_PKEY_CTX_new@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	EVP_PKEY_bits@PLT
	movq	-88(%rbp), %rdx
	movq	%r12, %rdi
	cltq
	movq	%rax, (%rdx)
	call	EVP_PKEY_free@PLT
.L39:
	testq	%r13, %r13
	je	.L63
	movq	%r13, %rdi
	call	EVP_PKEY_keygen_init@PLT
	leaq	.LC9(%rip), %rsi
	testl	%eax, %eax
	jle	.L55
	cmpl	$6, (%rbx)
	jne	.L18
	cmpq	$-1, %r15
	jne	.L64
.L18:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L65
	addq	$72, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L56:
	.cfi_restore_state
	movl	$6, (%rsi)
	movq	(%rdx), %r15
	movl	$6, %esi
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L38:
	movl	(%rbx), %edi
	call	EVP_PKEY_CTX_new_id@PLT
	movq	%rax, %r13
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L22:
	movl	$58, %esi
	movq	%r12, %rdi
	call	strchr@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L24
	movl	%eax, %edx
	subl	%r12d, %edx
.L25:
	leaq	-64(%rbp), %rdi
	movq	%r12, %rsi
	movl	%edx, -104(%rbp)
	call	EVP_PKEY_asn1_find_str@PLT
	movl	-104(%rbp), %edx
	testq	%rax, %rax
	movq	%rax, %r13
	je	.L66
	movq	%rbx, %rsi
	movq	%rax, %r9
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%edi, %edi
	call	EVP_PKEY_asn1_get0_info@PLT
	movq	-64(%rbp), %rdi
	call	ENGINE_finish@PLT
	movl	(%rbx), %esi
	cmpl	$6, %esi
	je	.L67
	leaq	1(%r15), %rax
	movq	%rax, -104(%rbp)
	testq	%r15, %r15
	jne	.L23
	xorl	%r12d, %r12d
	movq	$-1, %r15
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L60:
	call	EVP_PKEY_id@PLT
	movq	$-1, %r15
	movl	%eax, (%rbx)
	movl	%eax, %esi
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L24:
	movq	%r12, %rdi
	call	strlen@PLT
	movl	%eax, %edx
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L64:
	xorl	%r8d, %r8d
	movl	%r15d, %ecx
	movl	$4099, %edx
	movl	$4, %esi
	movq	%r13, %rdi
	call	RSA_pkey_ctx_ctrl@PLT
	testl	%eax, %eax
	jg	.L18
	leaq	.LC10(%rip), %rsi
	.p2align 4,,10
	.p2align 3
.L55:
	movq	bio_err(%rip), %rdi
	call	BIO_puts@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	movq	%r13, %rdi
	xorl	%r13d, %r13d
	call	EVP_PKEY_CTX_free@PLT
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L67:
	testq	%r15, %r15
	je	.L29
	leaq	1(%r15), %rdi
	movl	$10, %edx
	xorl	%esi, %esi
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L63:
	movq	bio_err(%rip), %rdi
	leaq	.LC8(%rip), %rsi
	call	BIO_puts@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L59:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %esi
	movq	%r13, %rdi
	call	BIO_ctrl@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	PEM_read_bio_X509@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L68
	movq	%rax, %rdi
	call	X509_get_pubkey@PLT
	movq	%r15, %rdi
	movq	%rax, %r12
	call	X509_free@PLT
	movq	%r13, %rdi
	call	BIO_free@PLT
	testq	%r12, %r12
	jne	.L35
	.p2align 4,,10
	.p2align 3
.L34:
	movq	-104(%rbp), %rdx
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
	xorl	%r13d, %r13d
	leaq	.LC5(%rip), %rsi
	call	BIO_printf@PLT
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L29:
	movq	-88(%rbp), %rax
	xorl	%r12d, %r12d
	movq	(%rax), %r15
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L68:
	movq	%r13, %rdi
	call	BIO_free@PLT
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L62:
	movq	bio_err(%rip), %rdi
	leaq	.LC7(%rip), %rsi
	call	BIO_puts@PLT
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L61:
	movq	bio_err(%rip), %rdi
	leaq	.LC6(%rip), %rsi
	xorl	%eax, %eax
	xorl	%r13d, %r13d
	call	BIO_printf@PLT
	movq	%r12, %rdi
	call	EVP_PKEY_free@PLT
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L58:
	movq	-104(%rbp), %rdx
	movq	bio_err(%rip), %rdi
	leaq	.LC4(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L66:
	movq	bio_err(%rip), %rdi
	movq	%r12, %rcx
	leaq	.LC2(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L18
.L65:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1452:
	.size	set_keygen_ctx, .-set_keygen_ctx
	.section	.rodata.str1.1
.LC11:
	.string	"parameter error \"%s\"\n"
	.text
	.p2align 4
	.type	do_sign_init, @function
do_sign_init:
.LFB1454:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -48(%rbp)
	testq	%rdi, %rdi
	je	.L82
	movq	%rsi, %rbx
	movq	%rdi, %r12
	leaq	-52(%rbp), %rsi
	movq	%rdx, %r14
	movq	%rbx, %rdi
	movq	%rcx, %r13
	call	EVP_PKEY_get_default_digest_nid@PLT
	cmpl	$2, %eax
	je	.L83
.L72:
	movq	%rbx, %r8
	leaq	-48(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movq	%r12, %rdi
	xorl	%ebx, %ebx
	call	EVP_DigestSignInit@PLT
	testl	%eax, %eax
	jne	.L74
.L82:
	xorl	%eax, %eax
.L69:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L84
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L76:
	.cfi_restore_state
	movl	%ebx, %esi
	movq	%r13, %rdi
	call	OPENSSL_sk_value@PLT
	movq	-48(%rbp), %rdi
	movq	%rax, %rsi
	movq	%rax, %r12
	call	pkey_ctrl_string@PLT
	testl	%eax, %eax
	jle	.L85
	addl	$1, %ebx
.L74:
	movq	%r13, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jl	.L76
	movl	$1, %eax
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L83:
	movl	-52(%rbp), %eax
	testl	%eax, %eax
	movl	$0, %eax
	cmove	%rax, %r14
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L85:
	movq	bio_err(%rip), %rdi
	movq	%r12, %rdx
	leaq	.LC11(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	jmp	.L82
.L84:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1454:
	.size	do_sign_init, .-do_sign_init
	.section	.rodata.str1.1
.LC12:
	.string	"%s [%s]:"
.LC13:
	.string	"%s '%s' too long\n"
.LC14:
	.string	"%s\n"
.LC15:
	.string	"weird input :-(\n"
	.section	.rodata.str1.8
	.align 8
.LC16:
	.string	"string is too short, it needs to be at least %d bytes long\n"
	.align 8
.LC17:
	.string	"string is too long, it needs to be no more than %d bytes long\n"
	.text
	.p2align 4
	.type	build_data.constprop.0, @function
build_data.constprop.0:
.LFB1460:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%ecx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movl	%r8d, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$24, %rsp
	movl	batch(%rip), %r9d
	movq	%rdi, -64(%rbp)
	movq	bio_err(%rip), %rdi
	testl	%r9d, %r9d
	je	.L105
.L87:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	call	BIO_ctrl@PLT
	testq	%r12, %r12
	je	.L88
	movq	%r12, %rdi
	call	strlen@PLT
	movq	%rax, %rdx
	leaq	2(%rax), %rax
	cmpq	$1024, %rax
	ja	.L118
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%rdx, -56(%rbp)
	call	memcpy@PLT
	movq	-56(%rbp), %rdx
	movl	$10, %r8d
	xorl	%eax, %eax
	leaq	.LC14(%rip), %rsi
	movw	%r8w, (%rbx,%rdx)
	movq	bio_err(%rip), %rdi
	movq	%r12, %rdx
	call	BIO_printf@PLT
.L91:
	movzbl	(%rbx), %eax
	testb	%al, %al
	je	.L117
	cmpb	$10, %al
	je	.L93
	cmpb	$46, %al
	je	.L119
.L99:
	movq	%rbx, %rax
.L100:
	movl	(%rax), %ecx
	addq	$4, %rax
	leal	-16843009(%rcx), %edx
	notl	%ecx
	andl	%ecx, %edx
	andl	$-2139062144, %edx
	je	.L100
	movl	%edx, %ecx
	shrl	$16, %ecx
	testl	$32896, %edx
	cmove	%ecx, %edx
	leaq	2(%rax), %rcx
	cmove	%rcx, %rax
	movl	%edx, %esi
	addb	%dl, %sil
	sbbq	$3, %rax
	subq	%rbx, %rax
	movslq	%eax, %rdx
	cmpb	$10, -1(%rbx,%rdx)
	jne	.L120
	subl	$1, %eax
	movslq	%eax, %rdx
	movb	$0, (%rbx,%rdx)
	movl	$0, %edx
	cmovns	%eax, %edx
	cmpl	%edx, %r15d
	jg	.L121
	testl	%r14d, %r14d
	js	.L106
	cmpl	%r14d, %eax
	jg	.L122
.L106:
	movl	$2, %eax
.L86:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L121:
	.cfi_restore_state
	movq	bio_err(%rip), %rdi
	movl	%r15d, %edx
	leaq	.LC16(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L104:
	movl	batch(%rip), %eax
	testl	%eax, %eax
	jne	.L117
	testq	%r12, %r12
	jne	.L117
	movq	bio_err(%rip), %rdi
.L105:
	movq	-64(%rbp), %rdx
	movq	%r13, %rcx
	leaq	.LC12(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L88:
	movl	batch(%rip), %edi
	movb	$0, (%rbx)
	testl	%edi, %edi
	jne	.L92
	movq	stdin(%rip), %rdx
	movl	$1024, %esi
	movq	%rbx, %rdi
	call	fgets@PLT
	testq	%rax, %rax
	jne	.L91
	.p2align 4,,10
	.p2align 3
.L117:
	xorl	%eax, %eax
.L124:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L92:
	.cfi_restore_state
	movl	$10, %esi
	movw	%si, (%rbx)
.L93:
	testq	%r13, %r13
	je	.L97
	cmpb	$0, 0(%r13)
	je	.L97
	movq	%r13, %rdi
	call	strlen@PLT
	movq	%rax, %rdx
	leaq	2(%rax), %rax
	cmpq	$1024, %rax
	ja	.L123
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movq	%rdx, -56(%rbp)
	call	memcpy@PLT
	movq	-56(%rbp), %rdx
	movl	$10, %ecx
	movw	%cx, (%rbx,%rdx)
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L119:
	cmpb	$10, 1(%rbx)
	jne	.L99
.L97:
	movl	$1, %eax
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L118:
	movq	16(%rbp), %rdx
	movq	%r12, %rcx
.L116:
	movq	bio_err(%rip), %rdi
	leaq	.LC13(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	xorl	%eax, %eax
	jmp	.L124
	.p2align 4,,10
	.p2align 3
.L122:
	movq	bio_err(%rip), %rdi
	movl	%r14d, %edx
	leaq	.LC17(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L123:
	movq	24(%rbp), %rdx
	movq	%r13, %rcx
	jmp	.L116
	.p2align 4,,10
	.p2align 3
.L120:
	movq	bio_err(%rip), %rdi
	leaq	.LC15(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	xorl	%eax, %eax
	jmp	.L86
	.cfi_endproc
.LFE1460:
	.size	build_data.constprop.0, .-build_data.constprop.0
	.p2align 4
	.globl	do_X509_sign
	.type	do_X509_sign, @function
do_X509_sign:
.LFB1455:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$8, %rsp
	call	EVP_MD_CTX_new@PLT
	movq	%rbx, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	do_sign_init
	movl	%eax, %ebx
	testl	%eax, %eax
	jle	.L126
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	X509_sign_ctx@PLT
	movl	%eax, %ebx
.L126:
	movq	%r12, %rdi
	call	EVP_MD_CTX_free@PLT
	xorl	%eax, %eax
	testl	%ebx, %ebx
	setg	%al
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1455:
	.size	do_X509_sign, .-do_X509_sign
	.section	.rodata.str1.1
.LC18:
	.string	""
.LC19:
	.string	"%s: Use -help for summary.\n"
.LC20:
	.string	"Can't find keygen engine %s\n"
.LC21:
	.string	"Serial number supplied twice\n"
	.section	.rodata.str1.8
	.align 8
.LC22:
	.string	"Ignoring -days; not generating a certificate\n"
	.section	.rodata.str1.1
.LC23:
	.string	"Error getting passwords\n"
.LC24:
	.string	"Using configuration from %s\n"
	.section	.rodata.str1.8
	.align 8
.LC25:
	.string	"Using additional configuration from command line\n"
	.section	.rodata.str1.1
.LC26:
	.string	"oid_file"
.LC27:
	.string	"default_md"
.LC28:
	.string	"req"
.LC29:
	.string	"x509_extensions"
	.section	.rodata.str1.8
	.align 8
.LC30:
	.string	"Error Loading extension section %s\n"
	.section	.rodata.str1.1
.LC31:
	.string	"default"
	.section	.rodata.str1.8
	.align 8
.LC32:
	.string	"Error Loading command line extensions\n"
	.section	.rodata.str1.1
.LC33:
	.string	"input_password"
.LC34:
	.string	"output_password"
.LC35:
	.string	"string_mask"
	.section	.rodata.str1.8
	.align 8
.LC36:
	.string	"Invalid global string mask setting %s\n"
	.section	.rodata.str1.1
.LC37:
	.string	"utf8"
.LC38:
	.string	"yes"
.LC39:
	.string	"req_extensions"
	.section	.rodata.str1.8
	.align 8
.LC40:
	.string	"Error Loading request extension section %s\n"
	.section	.rodata.str1.1
.LC41:
	.string	"Private Key"
.LC42:
	.string	"default_bits"
	.section	.rodata.str1.8
	.align 8
.LC43:
	.string	"private key length is too short,\n"
	.align 8
.LC44:
	.string	"it needs to be at least %d bits, not %ld\n"
	.align 8
.LC45:
	.string	"Warning: It is not recommended to use more than %d bit for RSA keys.\n         Your key size is %ld! Larger key size may behave not as expected.\n"
	.align 8
.LC46:
	.string	"Warning: It is not recommended to use more than %d bit for DSA keys.\n         Your key size is %ld! Larger key size may behave not as expected.\n"
	.section	.rodata.str1.1
.LC47:
	.string	"Generating an EC private key\n"
.LC48:
	.string	"Generating a %s private key\n"
.LC49:
	.string	"Error Generating Key\n"
.LC50:
	.string	"default_keyfile"
	.section	.rodata.str1.8
	.align 8
.LC51:
	.string	"writing new private key to stdout\n"
	.section	.rodata.str1.1
.LC52:
	.string	"encrypt_rsa_key"
.LC53:
	.string	"encrypt_key"
.LC54:
	.string	"-----\n"
.LC55:
	.string	"unable to load X509 request\n"
	.section	.rodata.str1.8
	.align 8
.LC56:
	.string	"you need to specify a private key\n"
	.section	.rodata.str1.1
.LC57:
	.string	"prompt"
.LC58:
	.string	"distinguished_name"
	.section	.rodata.str1.8
	.align 8
.LC59:
	.string	"unable to find '%s' in config\n"
	.section	.rodata.str1.1
.LC60:
	.string	"unable to get '%s' section\n"
.LC61:
	.string	"attributes"
	.section	.rodata.str1.8
	.align 8
.LC62:
	.string	"error, no objects specified in config file\n"
	.align 8
.LC63:
	.string	"You are about to be asked to enter information that will be incorporated\n"
	.align 8
.LC64:
	.string	"into your certificate request.\n"
	.align 8
.LC65:
	.string	"What you are about to enter is what is called a Distinguished Name or a DN.\n"
	.align 8
.LC66:
	.string	"There are quite a few fields but you can leave some blank\n"
	.align 8
.LC67:
	.string	"For some fields there will be a default value,\n"
	.align 8
.LC68:
	.string	"If you enter '.', the field will be left blank.\n"
	.section	.rodata.str1.1
.LC69:
	.string	"_min"
.LC70:
	.string	"_max"
.LC71:
	.string	"_default"
.LC72:
	.string	"_value"
.LC73:
	.string	"Name"
.LC74:
	.string	"DN default"
.LC75:
	.string	"DN value"
	.section	.rodata.str1.8
	.align 8
.LC76:
	.string	"\nPlease enter the following 'extra' attributes\n"
	.align 8
.LC77:
	.string	"to be sent with your certificate request\n"
	.section	.rodata.str1.1
.LC78:
	.string	"Attribute default"
.LC79:
	.string	"Attribute value"
.LC80:
	.string	"Error adding attribute\n"
	.section	.rodata.str1.8
	.align 8
.LC81:
	.string	"No template, please set one up.\n"
	.align 8
.LC82:
	.string	"problems making Certificate Request\n"
	.align 8
.LC83:
	.string	"Error adding poison extension\n"
	.align 8
.LC84:
	.string	"Cannot modify certificate subject\n"
	.section	.rodata.str1.1
.LC85:
	.string	"Modifying Request's Subject\n"
.LC86:
	.string	"old subject="
.LC87:
	.string	"ERROR: cannot modify subject\n"
.LC88:
	.string	"new subject="
.LC89:
	.string	"verify failure\n"
.LC90:
	.string	"verify OK\n"
.LC91:
	.string	"Error getting public key\n"
.LC92:
	.string	"Error printing certificate\n"
	.section	.rodata.str1.8
	.align 8
.LC93:
	.string	"Error printing certificate request\n"
	.section	.rodata.str1.1
.LC94:
	.string	"subject="
.LC95:
	.string	"Modulus=unavailable\n"
.LC96:
	.string	"Modulus="
.LC97:
	.string	"Wrong Algorithm type"
.LC98:
	.string	"unable to write X509 request\n"
	.section	.rodata.str1.8
	.align 8
.LC99:
	.string	"unable to write X509 certificate\n"
	.section	.rodata.str1.1
.LC100:
	.string	"no"
	.section	.rodata.str1.8
	.align 8
.LC101:
	.string	"writing new private key to '%s'\n"
	.text
	.p2align 4
	.globl	req_main
	.type	req_main, @function
req_main:
.LFB1441:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	xorl	%r14d, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%edi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$1608, %rsp
	movq	%rsi, -1600(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	default_config_file(%rip), %rax
	movq	$0, -1328(%rbp)
	movq	$0, -1320(%rbp)
	movq	$0, -1312(%rbp)
	movq	$0, -1304(%rbp)
	movq	$0, -1296(%rbp)
	movq	%rax, -1416(%rbp)
	movl	$-1, -1344(%rbp)
	movl	$32773, -1340(%rbp)
	movl	$32773, -1336(%rbp)
	movl	$32773, -1332(%rbp)
	movq	$-1, -1288(%rbp)
	movq	$0, -1280(%rbp)
	call	EVP_des_ede3_cbc@PLT
	movq	%rbx, %rsi
	movl	%r12d, %edi
	leaq	req_options(%rip), %rdx
	movq	%rax, -1592(%rbp)
	xorl	%r12d, %r12d
	leaq	.L135(%rip), %rbx
	call	opt_init@PLT
	movq	$4097, -1560(%rbp)
	movq	%rax, -1424(%rbp)
	movl	$0, -1580(%rbp)
	movl	$0, -1516(%rbp)
	movl	$0, -1568(%rbp)
	movl	$0, -1576(%rbp)
	movl	$0, -1520(%rbp)
	movl	$0, -1496(%rbp)
	movl	$0, -1564(%rbp)
	movl	$0, -1536(%rbp)
	movl	$0, -1492(%rbp)
	movl	$0, -1440(%rbp)
	movl	$0, -1448(%rbp)
	movl	$0, -1432(%rbp)
	movq	$0, -1352(%rbp)
	movq	$0, -1504(%rbp)
	movq	$0, -1512(%rbp)
	movq	$0, -1488(%rbp)
	movq	$0, -1464(%rbp)
	movq	$0, -1472(%rbp)
	movq	$0, -1544(%rbp)
	movq	$0, -1552(%rbp)
	movq	$0, -1456(%rbp)
	movq	$0, -1480(%rbp)
	movq	$0, -1384(%rbp)
	movq	$0, -1528(%rbp)
	movq	$0, -1392(%rbp)
	movq	$0, -1368(%rbp)
	movq	$0, -1360(%rbp)
	movq	$0, -1400(%rbp)
	movq	$0, -1408(%rbp)
	movq	$0, -1376(%rbp)
.L130:
	call	opt_next@PLT
	testl	%eax, %eax
	je	.L698
.L191:
	cmpl	$40, %eax
	jg	.L131
	cmpl	$-1, %eax
	jl	.L130
	addl	$1, %eax
	cmpl	$41, %eax
	ja	.L130
	movslq	(%rbx,%rax,4), %rax
	addq	%rbx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L135:
	.long	.L174-.L135
	.long	.L130-.L135
	.long	.L173-.L135
	.long	.L172-.L135
	.long	.L171-.L135
	.long	.L170-.L135
	.long	.L169-.L135
	.long	.L168-.L135
	.long	.L167-.L135
	.long	.L398-.L135
	.long	.L166-.L135
	.long	.L165-.L135
	.long	.L164-.L135
	.long	.L163-.L135
	.long	.L162-.L135
	.long	.L161-.L135
	.long	.L160-.L135
	.long	.L159-.L135
	.long	.L158-.L135
	.long	.L157-.L135
	.long	.L156-.L135
	.long	.L155-.L135
	.long	.L154-.L135
	.long	.L153-.L135
	.long	.L152-.L135
	.long	.L151-.L135
	.long	.L150-.L135
	.long	.L149-.L135
	.long	.L148-.L135
	.long	.L147-.L135
	.long	.L146-.L135
	.long	.L145-.L135
	.long	.L144-.L135
	.long	.L143-.L135
	.long	.L142-.L135
	.long	.L141-.L135
	.long	.L140-.L135
	.long	.L139-.L135
	.long	.L138-.L135
	.long	.L137-.L135
	.long	.L136-.L135
	.long	.L134-.L135
	.text
.L398:
	movl	$1, %r12d
	call	opt_next@PLT
	testl	%eax, %eax
	jne	.L191
.L698:
	call	opt_num_rest@PLT
	movl	%eax, %r15d
	testl	%eax, %eax
	jne	.L174
	movl	-1432(%rbp), %eax
	movl	-1448(%rbp), %r8d
	movl	%eax, %ecx
	xorl	$1, %eax
	xorl	$1, %ecx
	movb	%al, -1600(%rbp)
	movl	%ecx, -1604(%rbp)
	testl	%r8d, %r8d
	je	.L192
	testb	%al, %al
	je	.L192
	movq	bio_err(%rip), %rdi
	leaq	.LC22(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L192:
	movl	-1432(%rbp), %ecx
	movl	%ecx, %eax
	andl	$1, %eax
	cmpq	$0, -1456(%rbp)
	sete	%bl
	movb	%al, -1581(%rbp)
	andb	%cl, %bl
	jne	.L403
	movl	%r12d, %ebx
	andl	$1, %ebx
.L193:
	movq	-1464(%rbp), %rsi
	movq	-1472(%rbp), %rdi
	leaq	-1296(%rbp), %rcx
	leaq	-1304(%rbp), %rdx
	movq	-1328(%rbp), %r13
	call	app_passwd@PLT
	testl	%eax, %eax
	je	.L699
	movl	-1440(%rbp), %edi
	testl	%edi, %edi
	jne	.L700
.L195:
	movq	-1416(%rbp), %rdi
	call	app_load_config@PLT
	movq	%rax, req_conf(%rip)
	movq	%rax, %r10
	testq	%rax, %rax
	je	.L405
	cmpq	$0, -1384(%rbp)
	je	.L196
	movl	-1440(%rbp), %esi
	testl	%esi, %esi
	jne	.L701
.L197:
	movq	-1384(%rbp), %rdi
	xorl	%esi, %esi
	call	app_load_config_bio@PLT
	movq	%rax, addext_conf(%rip)
	movq	%rax, %r10
	testq	%rax, %rax
	je	.L405
	movq	-1416(%rbp), %rax
	cmpq	%rax, default_config_file(%rip)
	movq	req_conf(%rip), %r10
	je	.L198
.L395:
	movq	%r10, %rdi
	call	app_load_modules@PLT
	testl	%eax, %eax
	je	.L407
	movq	req_conf(%rip), %r10
.L198:
	testq	%r10, %r10
	jne	.L396
.L199:
	movq	%r10, %rdi
	call	add_oid_section@PLT
	testl	%eax, %eax
	je	.L407
	cmpq	$0, -1320(%rbp)
	je	.L702
.L201:
	cmpq	$0, -1480(%rbp)
	je	.L703
.L203:
	leaq	-1264(%rbp), %r10
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	movq	%r10, %rdi
	movl	$1, %r9d
	movq	%r10, -1416(%rbp)
	call	X509V3_set_ctx@PLT
	movq	-1416(%rbp), %r10
	movq	req_conf(%rip), %rsi
	movq	%r10, %rdi
	call	X509V3_set_nconf@PLT
	movq	-1416(%rbp), %r10
	movq	-1480(%rbp), %rdx
	xorl	%ecx, %ecx
	movq	req_conf(%rip), %rdi
	movq	%r10, %rsi
	call	X509V3_EXT_add_nconf@PLT
	testl	%eax, %eax
	je	.L704
.L204:
	cmpq	$0, addext_conf(%rip)
	je	.L205
	leaq	-1264(%rbp), %r10
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	movq	%r10, %rdi
	movl	$1, %r9d
	movq	%r10, -1416(%rbp)
	call	X509V3_set_ctx@PLT
	movq	-1416(%rbp), %r10
	movq	addext_conf(%rip), %rsi
	movq	%r10, %rdi
	call	X509V3_set_nconf@PLT
	movq	-1416(%rbp), %r10
	xorl	%ecx, %ecx
	movq	addext_conf(%rip), %rdi
	leaq	.LC31(%rip), %rdx
	movq	%r10, %rsi
	call	X509V3_EXT_add_nconf@PLT
	testl	%eax, %eax
	je	.L705
.L205:
	cmpq	$0, -1304(%rbp)
	movq	req_conf(%rip), %rdi
	movq	$0, -1416(%rbp)
	je	.L706
.L207:
	cmpq	$0, -1296(%rbp)
	movq	$0, -1424(%rbp)
	je	.L707
.L209:
	leaq	.LC35(%rip), %rdx
	leaq	.LC28(%rip), %rsi
	call	NCONF_get_string@PLT
	testq	%rax, %rax
	je	.L708
	movq	%rax, %rdi
	movq	%rax, -1464(%rbp)
	call	ASN1_STRING_set_default_mask_asc@PLT
	movq	-1464(%rbp), %rdx
	testl	%eax, %eax
	je	.L709
.L212:
	cmpq	$4096, -1560(%rbp)
	je	.L216
	movq	req_conf(%rip), %rdi
	leaq	.LC28(%rip), %rsi
	leaq	.LC37(%rip), %rdx
	call	NCONF_get_string@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L710
	movl	$4, %ecx
	leaq	.LC38(%rip), %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L216
	movl	$4097, -1464(%rbp)
	movq	$4097, -1472(%rbp)
.L214:
	cmpq	$0, -1488(%rbp)
	je	.L711
.L217:
	leaq	-1264(%rbp), %r10
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	movq	%r10, %rdi
	movl	$1, %r9d
	movq	%r10, -1560(%rbp)
	call	X509V3_set_ctx@PLT
	movq	-1560(%rbp), %r10
	movq	req_conf(%rip), %rsi
	movq	%r10, %rdi
	call	X509V3_set_nconf@PLT
	movq	-1560(%rbp), %r10
	movq	-1488(%rbp), %rdx
	xorl	%ecx, %ecx
	movq	req_conf(%rip), %rdi
	movq	%r10, %rsi
	call	X509V3_EXT_add_nconf@PLT
	testl	%eax, %eax
	je	.L712
.L218:
	movq	-1544(%rbp), %rax
	testq	%rax, %rax
	je	.L219
	movq	-1408(%rbp), %r8
	movq	-1304(%rbp), %rcx
	xorl	%edx, %edx
	movq	%rax, %rdi
	movl	-1332(%rbp), %esi
	leaq	.LC41(%rip), %r9
	call	load_key@PLT
	movq	%rax, -1328(%rbp)
	movq	%rax, %r10
	testq	%rax, %rax
	je	.L416
	movq	req_conf(%rip), %rdi
	leaq	.LC28(%rip), %rsi
	call	app_RAND_load_conf@PLT
.L219:
	movq	-1328(%rbp), %r10
	testq	%r10, %r10
	jne	.L220
	testb	%bl, %bl
	je	.L220
	movq	req_conf(%rip), %rdi
	leaq	.LC28(%rip), %rsi
	leaq	-1288(%rbp), %r12
	call	app_RAND_load_conf@PLT
	movq	req_conf(%rip), %rdi
	movq	%r12, %rcx
	leaq	.LC42(%rip), %rdx
	leaq	.LC28(%rip), %rsi
	call	NCONF_get_number_e@PLT
	testl	%eax, %eax
	jne	.L221
	cmpq	$0, -1352(%rbp)
	movq	$2048, -1288(%rbp)
	je	.L222
.L224:
	movq	-1400(%rbp), %r8
	movq	-1352(%rbp), %rdi
	movq	%r12, %rdx
	leaq	-1312(%rbp), %rcx
	leaq	-1344(%rbp), %rsi
	call	set_keygen_ctx
	movq	%rax, -1352(%rbp)
	testq	%rax, %rax
	je	.L412
.L223:
	movl	-1344(%rbp), %eax
	movq	-1288(%rbp), %rcx
	cmpl	$6, %eax
	sete	%dl
	cmpq	$511, %rcx
	jg	.L225
	cmpl	$116, %eax
	je	.L438
	testb	%dl, %dl
	jne	.L438
.L226:
	cmpq	$0, -1352(%rbp)
	je	.L394
.L229:
	xorl	%ebx, %ebx
	cmpq	$0, -1360(%rbp)
	je	.L235
	movq	%r13, -1456(%rbp)
	movq	-1352(%rbp), %r12
	movl	%ebx, %r13d
	movl	%r14d, -1544(%rbp)
	movq	-1360(%rbp), %rbx
	jmp	.L230
.L234:
	movl	%r13d, %esi
	movq	%rbx, %rdi
	call	OPENSSL_sk_value@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %r14
	call	pkey_ctrl_string@PLT
	testl	%eax, %eax
	jle	.L713
	addl	$1, %r13d
.L230:
	movq	%rbx, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r13d
	jl	.L234
	movq	-1456(%rbp), %r13
	movl	-1544(%rbp), %r14d
.L235:
	cmpl	$408, -1344(%rbp)
	movq	bio_err(%rip), %rdi
	je	.L714
	movq	-1312(%rbp), %rdx
	leaq	.LC48(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L236:
	movq	-1352(%rbp), %rbx
	leaq	genpkey_cb(%rip), %rsi
	movq	%rbx, %rdi
	call	EVP_PKEY_CTX_set_cb@PLT
	movq	bio_err(%rip), %rsi
	movq	%rbx, %rdi
	call	EVP_PKEY_CTX_set_app_data@PLT
	leaq	-1328(%rbp), %rsi
	movq	%rbx, %rdi
	call	EVP_PKEY_keygen@PLT
	testl	%eax, %eax
	jle	.L715
	movq	-1352(%rbp), %rdi
	call	EVP_PKEY_CTX_free@PLT
	cmpq	$0, -1504(%rbp)
	je	.L716
.L238:
	movq	-1504(%rbp), %rdx
	movq	bio_err(%rip), %rdi
	leaq	.LC101(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L381:
	xorl	%edx, %edx
	movl	-1336(%rbp), %esi
	testq	%r13, %r13
	movq	-1504(%rbp), %rdi
	sete	%dl
	call	bio_open_owner@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L414
	movq	req_conf(%rip), %rdi
	leaq	.LC52(%rip), %rdx
	leaq	.LC28(%rip), %rsi
	call	NCONF_get_string@PLT
	testq	%rax, %rax
	je	.L717
.L239:
	cmpb	$110, (%rax)
	jne	.L380
	cmpb	$111, 1(%rax)
	jne	.L380
	cmpb	$0, 2(%rax)
	movl	$0, %eax
	cmovne	-1592(%rbp), %rax
	movq	%rax, -1592(%rbp)
.L380:
	movl	-1576(%rbp), %ecx
	movl	$0, %eax
	testl	%ecx, %ecx
	cmove	-1592(%rbp), %rax
	xorl	%ebx, %ebx
	movq	%rax, -1592(%rbp)
.L241:
	subq	$8, %rsp
	pushq	-1296(%rbp)
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	-1592(%rbp), %rdx
	xorl	%ecx, %ecx
	movq	%r13, %rdi
	movq	-1328(%rbp), %rsi
	call	PEM_write_bio_PrivateKey@PLT
	popq	%r12
	popq	%rdx
	testl	%eax, %eax
	jne	.L242
	call	ERR_peek_error@PLT
	andl	$4095, %eax
	cmpl	$109, %eax
	jne	.L415
	cmpl	$3, %ebx
	je	.L415
	call	ERR_clear_error@PLT
	addl	$1, %ebx
	jmp	.L241
.L167:
	movl	$1, %r14d
	jmp	.L130
.L139:
	call	opt_arg@PLT
	cmpq	$0, -1392(%rbp)
	movq	%rax, %r15
	jne	.L182
	jmp	.L718
	.p2align 4,,10
	.p2align 3
.L719:
	call	__ctype_b_loc@PLT
	movq	(%rax), %rax
	testb	$32, 1(%rax,%r13,2)
	je	.L183
	addq	$1, %r15
	cmpb	$0, (%r15)
	je	.L174
.L182:
	movsbq	(%r15), %r13
	testb	%r13b, %r13b
	jne	.L719
.L183:
	movl	$61, %esi
	movq	%r15, %rdi
	call	strchr@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L174
	movq	%r15, %rdi
	movl	$189, %edx
	leaq	.LC0(%rip), %rsi
	subq	%r15, %r13
	call	CRYPTO_strdup@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L401
	addq	%rax, %r13
	cmpq	%r13, %rax
	jnb	.L185
	call	__ctype_b_loc@PLT
	movq	(%rax), %rdx
	jmp	.L186
	.p2align 4,,10
	.p2align 3
.L720:
	subq	$1, %r13
	cmpq	%r13, %r15
	je	.L683
.L186:
	movsbq	-1(%r13), %rax
	testb	$32, 1(%rdx,%rax,2)
	jne	.L720
.L185:
	cmpq	%r15, %r13
	je	.L683
	movb	$0, 0(%r13)
	movq	-1392(%rbp), %rdi
	movq	%r15, %rsi
	call	OPENSSL_LH_insert@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L189
	movl	$205, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rax, %rdi
	call	CRYPTO_free@PLT
.L174:
	movq	-1424(%rbp), %rdx
	movq	bio_err(%rip), %rdi
	leaq	.LC19(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L407:
	movq	$0, -1424(%rbp)
	movl	$1, %r15d
	movq	$0, -1416(%rbp)
.L695:
	xorl	%ebx, %ebx
	xorl	%r12d, %r12d
	xorl	%r13d, %r13d
	xorl	%r10d, %r10d
	movq	$0, -1352(%rbp)
.L176:
	movq	bio_err(%rip), %rdi
	movq	%r10, -1432(%rbp)
	call	ERR_print_errors@PLT
	movq	-1432(%rbp), %r10
.L177:
	movq	req_conf(%rip), %rdi
	movq	%r10, -1432(%rbp)
	call	NCONF_free@PLT
	movq	addext_conf(%rip), %rdi
	call	NCONF_free@PLT
	movq	-1384(%rbp), %rdi
	call	BIO_free@PLT
	movq	-1432(%rbp), %r10
	movq	%r10, %rdi
	call	BIO_free@PLT
	movq	%r13, %rdi
	call	BIO_free_all@PLT
	movq	-1328(%rbp), %rdi
	call	EVP_PKEY_free@PLT
	movq	-1352(%rbp), %rdi
	call	EVP_PKEY_CTX_free@PLT
	movq	-1360(%rbp), %rdi
	call	OPENSSL_sk_free@PLT
	movq	-1368(%rbp), %rdi
	call	OPENSSL_sk_free@PLT
	movq	-1392(%rbp), %r14
	leaq	exts_cleanup(%rip), %rsi
	movq	%r14, %rdi
	call	OPENSSL_LH_doall@PLT
	movq	%r14, %rdi
	call	OPENSSL_LH_free@PLT
	movq	-1400(%rbp), %rdi
	call	ENGINE_free@PLT
	movq	-1312(%rbp), %rdi
	movl	$977, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%rbx, %rdi
	call	X509_REQ_free@PLT
	movq	%r12, %rdi
	call	X509_free@PLT
	movq	-1376(%rbp), %rdi
	call	ASN1_INTEGER_free@PLT
	movq	-1408(%rbp), %rdi
	call	release_engine@PLT
	movq	-1304(%rbp), %rdi
	cmpq	-1416(%rbp), %rdi
	je	.L375
	movl	$983, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
.L375:
	movq	-1296(%rbp), %rdi
	cmpq	-1424(%rbp), %rdi
	je	.L129
	movl	$985, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
.L129:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L721
	leaq	-40(%rbp), %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L131:
	.cfi_restore_state
	leal	-1501(%rax), %edx
	cmpl	$1, %edx
	ja	.L130
	movl	%eax, %edi
	call	opt_rand@PLT
	testl	%eax, %eax
	jne	.L130
	jmp	.L407
.L134:
	call	opt_unknown@PLT
	leaq	-1320(%rbp), %rsi
	movq	%rax, %rdi
	call	opt_md@PLT
	testl	%eax, %eax
	je	.L174
	movq	-1320(%rbp), %rax
	movq	%rax, -1528(%rbp)
	jmp	.L130
.L136:
	movl	$1, -1580(%rbp)
	movl	$1, %r12d
	jmp	.L130
.L137:
	call	opt_arg@PLT
	movq	%rax, -1488(%rbp)
	jmp	.L130
.L138:
	call	opt_arg@PLT
	movq	%rax, -1480(%rbp)
	jmp	.L130
.L140:
	cmpq	$0, -1376(%rbp)
	jne	.L722
	call	opt_arg@PLT
	xorl	%edi, %edi
	movq	%rax, %rsi
	call	s2i_ASN1_INTEGER@PLT
	movq	%rax, -1376(%rbp)
	testq	%rax, %rax
	jne	.L130
	jmp	.L174
.L141:
	call	opt_arg@PLT
	movl	$10, %edx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	strtol@PLT
	movl	%eax, -1448(%rbp)
	jmp	.L130
.L142:
	movl	$1, -1536(%rbp)
	jmp	.L130
.L143:
	movl	$1, -1432(%rbp)
	jmp	.L130
.L144:
	movl	$1, -1520(%rbp)
	jmp	.L130
.L145:
	movl	$1, -1516(%rbp)
	jmp	.L130
.L146:
	call	opt_arg@PLT
	movq	%rax, -1512(%rbp)
	jmp	.L130
.L147:
	call	opt_arg@PLT
	leaq	-1280(%rbp), %rdi
	movq	%rax, %rsi
	call	set_cert_ex@PLT
	testl	%eax, %eax
	jne	.L130
	jmp	.L174
.L148:
	call	opt_arg@PLT
	movq	%rax, %rdi
	call	set_nameopt@PLT
	testl	%eax, %eax
	jne	.L130
	jmp	.L174
.L149:
	movq	$4096, -1560(%rbp)
	jmp	.L130
.L150:
	movl	$1, -1440(%rbp)
	jmp	.L130
.L151:
	movl	$1, -1496(%rbp)
	jmp	.L130
.L152:
	movl	$1, -1576(%rbp)
	jmp	.L130
.L153:
	movl	$1, -1564(%rbp)
	jmp	.L130
.L154:
	movl	$1, -1492(%rbp)
	jmp	.L130
.L155:
	movl	$1, -1568(%rbp)
	jmp	.L130
.L156:
	movl	$1, batch(%rip)
	jmp	.L130
.L157:
	cmpq	$0, -1368(%rbp)
	je	.L723
.L179:
	call	opt_arg@PLT
	movq	-1368(%rbp), %rdi
	movq	%rax, %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	jne	.L130
	jmp	.L174
.L158:
	cmpq	$0, -1360(%rbp)
	je	.L724
.L178:
	call	opt_arg@PLT
	movq	-1360(%rbp), %rdi
	movq	%rax, %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	jne	.L130
	jmp	.L174
.L159:
	call	opt_arg@PLT
	movl	$1, %r12d
	movq	%rax, -1352(%rbp)
	jmp	.L130
.L160:
	call	opt_arg@PLT
	movq	%rax, -1464(%rbp)
	jmp	.L130
.L161:
	call	opt_arg@PLT
	movq	%rax, -1472(%rbp)
	jmp	.L130
.L162:
	call	opt_arg@PLT
	movq	%rax, -1504(%rbp)
	jmp	.L130
.L163:
	call	opt_arg@PLT
	movq	%rax, -1552(%rbp)
	jmp	.L130
.L164:
	call	opt_arg@PLT
	movq	%rax, -1456(%rbp)
	jmp	.L130
.L165:
	call	opt_arg@PLT
	leaq	-1332(%rbp), %rdx
	movl	$1982, %esi
	movq	%rax, %rdi
	call	opt_format@PLT
	testl	%eax, %eax
	jne	.L130
	jmp	.L174
.L166:
	call	opt_arg@PLT
	movq	%rax, -1416(%rbp)
	jmp	.L130
.L168:
	call	opt_arg@PLT
	movq	%rax, -1544(%rbp)
	jmp	.L130
.L169:
	call	opt_arg@PLT
	movq	%rax, %rdi
	call	ENGINE_by_id@PLT
	movq	%rax, -1400(%rbp)
	testq	%rax, %rax
	jne	.L130
	movq	-1600(%rbp), %rax
	movq	bio_err(%rip), %rdi
	leaq	.LC20(%rip), %rsi
	movq	(%rax), %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L174
.L170:
	call	opt_arg@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	setup_engine@PLT
	movq	%rax, -1408(%rbp)
	jmp	.L130
.L171:
	call	opt_arg@PLT
	leaq	-1336(%rbp), %rdx
	movl	$2, %esi
	movq	%rax, %rdi
	call	opt_format@PLT
	testl	%eax, %eax
	jne	.L130
	jmp	.L174
.L172:
	call	opt_arg@PLT
	leaq	-1340(%rbp), %rdx
	movl	$2, %esi
	movq	%rax, %rdi
	call	opt_format@PLT
	testl	%eax, %eax
	jne	.L130
	jmp	.L174
.L173:
	leaq	req_options(%rip), %rdi
	xorl	%r15d, %r15d
	xorl	%ebx, %ebx
	xorl	%r12d, %r12d
	call	opt_help@PLT
	xorl	%r13d, %r13d
	xorl	%r10d, %r10d
	movq	$0, -1424(%rbp)
	movq	$0, -1416(%rbp)
	movq	$0, -1352(%rbp)
	jmp	.L177
.L718:
	leaq	ext_name_cmp(%rip), %rsi
	leaq	ext_name_hash(%rip), %rdi
	call	OPENSSL_LH_new@PLT
	movq	%rax, %r13
	call	BIO_s_mem@PLT
	movq	%rax, %rdi
	call	BIO_new@PLT
	movq	%rax, -1384(%rbp)
	testq	%r13, %r13
	je	.L400
	testq	%rax, %rax
	je	.L400
	movq	%r13, -1392(%rbp)
	jmp	.L182
.L403:
	movl	$1, %r12d
	jmp	.L193
.L683:
	movl	$197, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r15, %rdi
	call	CRYPTO_free@PLT
	jmp	.L174
.L724:
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, -1360(%rbp)
	testq	%rax, %rax
	jne	.L178
	jmp	.L174
.L723:
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, -1368(%rbp)
	testq	%rax, %rax
	jne	.L179
	jmp	.L174
.L196:
	movq	-1416(%rbp), %rax
	cmpq	default_config_file(%rip), %rax
	jne	.L395
.L396:
	xorl	%esi, %esi
	leaq	.LC26(%rip), %rdx
	movq	%r10, %rdi
	call	NCONF_get_string@PLT
	testq	%rax, %rax
	je	.L725
	movq	%rax, %rdi
	leaq	.LC3(%rip), %rsi
	call	BIO_new_file@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L684
	movq	%rdi, -1416(%rbp)
	call	OBJ_create_objects@PLT
	movq	-1416(%rbp), %rdi
	call	BIO_free@PLT
.L684:
	movq	req_conf(%rip), %r10
	jmp	.L199
.L702:
	movq	req_conf(%rip), %rdi
	leaq	.LC27(%rip), %rdx
	leaq	.LC28(%rip), %rsi
	call	NCONF_get_string@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L726
	leaq	-1320(%rbp), %rsi
	call	opt_md@PLT
	testl	%eax, %eax
	je	.L174
	movq	-1320(%rbp), %rax
	movq	%rax, -1528(%rbp)
	jmp	.L201
.L703:
	movq	req_conf(%rip), %rdi
	leaq	.LC29(%rip), %rdx
	leaq	.LC28(%rip), %rsi
	call	NCONF_get_string@PLT
	movq	%rax, -1480(%rbp)
	testq	%rax, %rax
	jne	.L203
	call	ERR_clear_error@PLT
	jmp	.L204
	.p2align 4,,10
	.p2align 3
.L405:
	movq	$0, -1424(%rbp)
	movq	$0, -1416(%rbp)
.L416:
	xorl	%ebx, %ebx
	xorl	%r12d, %r12d
	xorl	%r13d, %r13d
	movl	$1, %r15d
	movq	$0, -1352(%rbp)
	jmp	.L176
.L700:
	movq	-1416(%rbp), %rdx
	movq	bio_err(%rip), %rdi
	leaq	.LC24(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L195
.L189:
	movq	-1392(%rbp), %rdi
	call	OPENSSL_LH_error@PLT
	testl	%eax, %eax
	jne	.L727
	call	opt_arg@PLT
	movq	-1384(%rbp), %rdi
	leaq	.LC14(%rip), %rsi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	testl	%eax, %eax
	jns	.L130
	movq	$0, -1424(%rbp)
	movq	$0, -1416(%rbp)
.L414:
	xorl	%ebx, %ebx
	xorl	%r12d, %r12d
	xorl	%r10d, %r10d
	movl	$1, %r15d
	movq	$0, -1352(%rbp)
	jmp	.L176
.L699:
	movq	bio_err(%rip), %rdi
	leaq	.LC23(%rip), %rsi
	call	BIO_printf@PLT
	jmp	.L407
.L701:
	movq	bio_err(%rip), %rdi
	leaq	.LC25(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L197
.L216:
	movl	$4096, -1464(%rbp)
	movq	$4096, -1472(%rbp)
	jmp	.L214
.L401:
	movq	$0, -1424(%rbp)
	movq	%rax, %r10
	movq	$0, -1416(%rbp)
	jmp	.L416
.L220:
	testl	%r12d, %r12d
	je	.L728
.L243:
	testq	%r10, %r10
	je	.L729
	call	X509_REQ_new@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L418
	movq	-1328(%rbp), %rax
	movq	req_conf(%rip), %rdi
	leaq	.LC57(%rip), %rdx
	leaq	.LC28(%rip), %rsi
	movq	%rax, -1456(%rbp)
	call	NCONF_get_string@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L730
	leaq	.LC100(%rip), %rsi
	call	strcmp@PLT
	testl	%eax, %eax
	sete	%r12b
.L378:
	movq	req_conf(%rip), %rdi
	leaq	.LC58(%rip), %rdx
	leaq	.LC28(%rip), %rsi
	call	NCONF_get_string@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L731
	movq	req_conf(%rip), %rdi
	movq	%rax, %rsi
	call	NCONF_get_section@PLT
	movq	%rax, -1544(%rbp)
	testq	%rax, %rax
	je	.L732
	movq	req_conf(%rip), %rdi
	leaq	.LC61(%rip), %rdx
	leaq	.LC28(%rip), %rsi
	call	NCONF_get_string@PLT
	movq	%rax, -1576(%rbp)
	testq	%rax, %rax
	je	.L733
	movq	-1576(%rbp), %rsi
	movq	req_conf(%rip), %rdi
	call	NCONF_get_section@PLT
	movq	%rax, -1560(%rbp)
	testq	%rax, %rax
	je	.L734
.L256:
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	X509_REQ_set_version@PLT
	testl	%eax, %eax
	je	.L253
	movq	-1512(%rbp), %rax
	testq	%rax, %rax
	je	.L258
	movl	-1536(%rbp), %edx
	movq	-1472(%rbp), %rsi
	movq	%rax, %rdi
	call	parse_name@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L253
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	X509_REQ_set_subject_name@PLT
	movq	%r12, %rdi
	testl	%eax, %eax
	je	.L735
	call	X509_NAME_free@PLT
	movq	-1456(%rbp), %rsi
	movq	%rbx, %rdi
	call	X509_REQ_set_pubkey@PLT
	testl	%eax, %eax
	je	.L253
	movl	-1432(%rbp), %r8d
	testl	%r8d, %r8d
	jne	.L736
.L316:
	leaq	-1264(%rbp), %r13
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	X509V3_set_ctx@PLT
	movq	req_conf(%rip), %rsi
	movq	%r13, %rdi
	call	X509V3_set_nconf@PLT
	movq	-1488(%rbp), %rax
	testq	%rax, %rax
	je	.L333
	movq	req_conf(%rip), %rdi
	movq	%rbx, %rcx
	movq	%rax, %rdx
	movq	%r13, %rsi
	call	X509V3_EXT_REQ_add_nconf@PLT
	testl	%eax, %eax
	je	.L737
.L333:
	movq	addext_conf(%rip), %rdi
	testq	%rdi, %rdi
	je	.L335
	movq	%rbx, %rcx
	leaq	.LC31(%rip), %rdx
	movq	%r13, %rsi
	call	X509V3_EXT_REQ_add_nconf@PLT
	testl	%eax, %eax
	je	.L738
.L335:
	movq	-1328(%rbp), %r13
	call	EVP_MD_CTX_new@PLT
	movq	-1368(%rbp), %rcx
	movq	-1528(%rbp), %rdx
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	do_sign_init
	testl	%eax, %eax
	jg	.L739
	movq	%r12, %rdi
	call	EVP_MD_CTX_free@PLT
.L377:
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
.L334:
	movl	$1, %r15d
	xorl	%r12d, %r12d
	xorl	%r13d, %r13d
	xorl	%r10d, %r10d
	movq	$0, -1352(%rbp)
	jmp	.L176
.L727:
	movl	$208, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r15, %rdi
	call	CRYPTO_free@PLT
	movq	$0, -1424(%rbp)
	movq	$0, -1416(%rbp)
	jmp	.L414
.L725:
	call	ERR_clear_error@PLT
	movq	req_conf(%rip), %r10
	jmp	.L199
.L711:
	movq	req_conf(%rip), %rdi
	leaq	.LC39(%rip), %rdx
	leaq	.LC28(%rip), %rsi
	call	NCONF_get_string@PLT
	movq	%rax, -1488(%rbp)
	testq	%rax, %rax
	jne	.L217
	call	ERR_clear_error@PLT
	jmp	.L218
.L722:
	movq	bio_err(%rip), %rdi
	leaq	.LC21(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L174
.L728:
	movl	-1340(%rbp), %edx
	movq	-1456(%rbp), %rdi
	movl	$114, %esi
	call	bio_open_default@PLT
	movq	%rax, %r10
	testq	%rax, %rax
	je	.L416
	cmpl	$4, -1340(%rbp)
	je	.L740
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%rax, -1352(%rbp)
	call	PEM_read_bio_X509_REQ@PLT
	movq	-1352(%rbp), %r10
	movq	%rax, %rbx
.L245:
	testq	%rbx, %rbx
	je	.L741
	movl	-1432(%rbp), %r11d
	testl	%r11d, %r11d
	je	.L249
	cmpq	$0, -1328(%rbp)
	je	.L247
.L250:
	movq	%r10, -1352(%rbp)
	call	X509_new@PLT
	movq	-1352(%rbp), %r10
	testq	%rax, %rax
	movq	%rax, %r12
	je	.L326
	cmpq	$0, -1480(%rbp)
	je	.L742
.L318:
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%r10, -1352(%rbp)
	call	X509_set_version@PLT
	movq	-1352(%rbp), %r10
	testl	%eax, %eax
	je	.L326
.L321:
	cmpq	$0, -1376(%rbp)
	je	.L743
	movq	-1376(%rbp), %rsi
	movq	%r12, %rdi
	movq	%r10, -1352(%rbp)
	call	X509_set_serialNumber@PLT
	movq	-1352(%rbp), %r10
	testl	%eax, %eax
	je	.L326
.L323:
	movq	%rbx, %rdi
	movq	%r10, -1352(%rbp)
	call	X509_REQ_get_subject_name@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	X509_set_issuer_name@PLT
	movq	-1352(%rbp), %r10
	testl	%eax, %eax
	je	.L326
	cmpl	$0, -1448(%rbp)
	jne	.L324
	movl	$30, -1448(%rbp)
.L324:
	movl	-1448(%rbp), %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%r10, -1352(%rbp)
	call	set_cert_times@PLT
	movq	-1352(%rbp), %r10
	testl	%eax, %eax
	je	.L326
	movq	%rbx, %rdi
	call	X509_REQ_get_subject_name@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	X509_set_subject_name@PLT
	movq	-1352(%rbp), %r10
	testl	%eax, %eax
	je	.L326
	movq	%rbx, %rdi
	call	X509_REQ_get0_pubkey@PLT
	movq	-1352(%rbp), %r10
	testq	%rax, %rax
	movq	%rax, %rsi
	je	.L326
	movq	%r12, %rdi
	call	X509_set_pubkey@PLT
	movq	-1352(%rbp), %r10
	testl	%eax, %eax
	je	.L326
	leaq	-1264(%rbp), %r13
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r12, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	X509V3_set_ctx@PLT
	movq	req_conf(%rip), %rsi
	movq	%r13, %rdi
	call	X509V3_set_nconf@PLT
	movq	-1480(%rbp), %rdx
	movq	-1352(%rbp), %r10
	testq	%rdx, %rdx
	je	.L325
	movq	req_conf(%rip), %rdi
	movq	%r12, %rcx
	movq	%r13, %rsi
	call	X509V3_EXT_add_nconf@PLT
	movq	-1352(%rbp), %r10
	testl	%eax, %eax
	je	.L744
.L325:
	movq	addext_conf(%rip), %rdi
	testq	%rdi, %rdi
	je	.L327
	movq	%r12, %rcx
	leaq	.LC31(%rip), %rdx
	movq	%r13, %rsi
	movq	%r10, -1352(%rbp)
	call	X509V3_EXT_add_nconf@PLT
	movq	-1352(%rbp), %r10
	testl	%eax, %eax
	je	.L745
.L327:
	cmpl	$0, -1580(%rbp)
	je	.L328
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movl	$1, %ecx
	movl	$952, %esi
	movq	%r12, %rdi
	movq	%r10, -1352(%rbp)
	call	X509_add1_ext_i2d@PLT
	movq	-1352(%rbp), %r10
	subl	$1, %eax
	jne	.L746
.L328:
	movq	-1368(%rbp), %rcx
	movq	-1528(%rbp), %rdx
	movq	%r12, %rdi
	movq	%r10, -1352(%rbp)
	movq	-1328(%rbp), %rsi
	call	do_X509_sign
	movq	-1352(%rbp), %r10
	testl	%eax, %eax
	je	.L747
	cmpq	$0, -1512(%rbp)
	setne	%al
	cmpb	$0, -1581(%rbp)
	je	.L332
	testb	%al, %al
	je	.L332
	movq	%r10, -1432(%rbp)
	leaq	.LC84(%rip), %rsi
.L694:
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
	movl	$1, %r15d
	xorl	%r13d, %r13d
	call	BIO_printf@PLT
	movq	-1432(%rbp), %r10
	movq	$0, -1352(%rbp)
	jmp	.L176
.L708:
	call	ERR_clear_error@PLT
	jmp	.L212
.L707:
	leaq	.LC34(%rip), %rdx
	leaq	.LC28(%rip), %rsi
	call	NCONF_get_string@PLT
	movq	%rax, -1424(%rbp)
	movq	%rax, -1296(%rbp)
	testq	%rax, %rax
	je	.L210
.L686:
	movq	req_conf(%rip), %rdi
	jmp	.L209
.L706:
	leaq	.LC33(%rip), %rdx
	leaq	.LC28(%rip), %rsi
	call	NCONF_get_string@PLT
	movq	%rax, -1416(%rbp)
	movq	%rax, -1304(%rbp)
	testq	%rax, %rax
	je	.L208
.L685:
	movq	req_conf(%rip), %rdi
	jmp	.L207
.L705:
	movq	bio_err(%rip), %rdi
	leaq	.LC32(%rip), %rsi
	call	BIO_printf@PLT
	jmp	.L407
.L704:
	movq	-1480(%rbp), %rdx
	movq	bio_err(%rip), %rdi
	leaq	.LC30(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L407
.L712:
	movq	-1488(%rbp), %rdx
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
	movl	$1, %r15d
	leaq	.LC40(%rip), %rsi
	call	BIO_printf@PLT
	jmp	.L695
.L744:
	movq	-1480(%rbp), %rdx
	movq	bio_err(%rip), %rdi
	leaq	.LC30(%rip), %rsi
	call	BIO_printf@PLT
	movq	-1352(%rbp), %r10
.L326:
	movq	$0, -1352(%rbp)
	movl	$1, %r15d
	xorl	%r13d, %r13d
	jmp	.L176
.L221:
	cmpq	$0, -1352(%rbp)
	jne	.L224
	jmp	.L223
.L332:
	andb	-1600(%rbp), %al
.L392:
	testb	%al, %al
	je	.L340
	movl	-1440(%rbp), %edi
	testl	%edi, %edi
	jne	.L748
.L341:
	movl	-1536(%rbp), %edx
	movq	-1472(%rbp), %rsi
	movq	%r10, -1352(%rbp)
	movq	-1512(%rbp), %rdi
	call	parse_name@PLT
	movq	-1352(%rbp), %r10
	testq	%rax, %rax
	movq	%rax, %r13
	je	.L342
	movq	%rax, %rsi
	movq	%rbx, %rdi
	call	X509_REQ_set_subject_name@PLT
	movq	-1352(%rbp), %r10
	testl	%eax, %eax
	je	.L749
	movq	%r13, %rdi
	movq	%r10, -1352(%rbp)
	call	X509_NAME_free@PLT
	movl	-1440(%rbp), %esi
	movq	-1352(%rbp), %r10
	testl	%esi, %esi
	jne	.L750
.L340:
	movl	-1564(%rbp), %eax
	andl	-1604(%rbp), %eax
	movl	%eax, -1352(%rbp)
	je	.L345
	movq	-1328(%rbp), %r13
	testq	%r13, %r13
	je	.L751
.L346:
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movq	%r10, -1440(%rbp)
	call	X509_REQ_verify@PLT
	movq	-1440(%rbp), %r10
	testl	%eax, %eax
	js	.L426
	movq	%r10, -1352(%rbp)
	movq	bio_err(%rip), %rdi
	jne	.L347
	leaq	.LC89(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	movq	-1352(%rbp), %r10
.L345:
	movl	-1520(%rbp), %eax
	xorl	$1, %eax
	testl	%eax, -1496(%rbp)
	je	.L348
	movl	-1516(%rbp), %eax
	orl	%r14d, %eax
	orl	-1492(%rbp), %eax
	je	.L427
.L348:
	movq	-1504(%rbp), %rdi
	movl	-1336(%rbp), %r13d
	testq	%rdi, %rdi
	je	.L428
	movq	-1552(%rbp), %rsi
	testq	%rsi, %rsi
	je	.L428
	movq	%r10, -1352(%rbp)
	call	strcmp@PLT
	movq	-1352(%rbp), %r10
	cmpl	$1, %eax
	sbbl	%esi, %esi
	andl	$-22, %esi
	addl	$119, %esi
.L349:
	movq	-1552(%rbp), %rdi
	movl	%r13d, %edx
	movq	%r10, -1352(%rbp)
	call	bio_open_default@PLT
	movq	-1352(%rbp), %r10
	testq	%rax, %rax
	movq	%rax, %r13
	je	.L430
	testl	%r14d, %r14d
	jne	.L752
.L350:
	movl	-1520(%rbp), %ecx
	testl	%ecx, %ecx
	je	.L352
	movl	-1432(%rbp), %edx
	movq	%r10, -1352(%rbp)
	movq	-1280(%rbp), %r14
	testl	%edx, %edx
	je	.L353
	call	get_nameopt@PLT
	movq	%r14, %rcx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	X509_print_ex@PLT
	movq	-1352(%rbp), %r10
	testl	%eax, %eax
	movl	%eax, -1440(%rbp)
	je	.L753
	cmpl	$0, -1516(%rbp)
	jne	.L389
	cmpl	$0, -1492(%rbp)
	je	.L362
.L361:
	movq	%r12, %rdi
	movq	%r10, -1448(%rbp)
	call	X509_get0_pubkey@PLT
	movq	-1448(%rbp), %r10
	movq	%rax, -1352(%rbp)
.L366:
	cmpq	$0, -1352(%rbp)
	movq	stdout(%rip), %rcx
	je	.L754
	movl	$8, %edx
	movl	$1, %esi
	leaq	.LC96(%rip), %rdi
	movq	%r10, -1448(%rbp)
	call	fwrite@PLT
	movq	-1352(%rbp), %rdi
	call	EVP_PKEY_base_id@PLT
	movq	-1448(%rbp), %r10
	cmpl	$6, %eax
	je	.L755
	movq	stdout(%rip), %rcx
	movl	$20, %edx
	movl	$1, %esi
	leaq	.LC97(%rip), %rdi
	movq	%r10, -1352(%rbp)
	call	fwrite@PLT
	movq	-1352(%rbp), %r10
.L369:
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	movq	%r10, -1352(%rbp)
	call	fputc@PLT
	movq	-1352(%rbp), %r10
.L365:
	movl	-1432(%rbp), %eax
	orl	-1496(%rbp), %eax
	jne	.L362
.L388:
	cmpl	$4, -1336(%rbp)
	je	.L756
	movl	-1568(%rbp), %eax
	movq	%r10, -1352(%rbp)
	movq	%rbx, %rsi
	movq	%r13, %rdi
	testl	%eax, %eax
	je	.L372
	call	PEM_write_bio_X509_REQ_NEW@PLT
	movq	-1352(%rbp), %r10
.L371:
	movq	$0, -1352(%rbp)
	testl	%eax, %eax
	jne	.L177
	movq	bio_err(%rip), %rdi
	leaq	.LC98(%rip), %rsi
	movq	%r10, -1432(%rbp)
	call	BIO_printf@PLT
	movl	-1440(%rbp), %r15d
	movq	-1432(%rbp), %r10
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L709:
	movq	bio_err(%rip), %rdi
	leaq	.LC36(%rip), %rsi
	xorl	%eax, %eax
	movl	$1, %r15d
	call	BIO_printf@PLT
	jmp	.L695
.L713:
	movq	bio_err(%rip), %rdi
	movq	%r14, %rdx
	leaq	.LC11(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
.L696:
	movl	$1, %r15d
	xorl	%ebx, %ebx
	xorl	%r12d, %r12d
	xorl	%r13d, %r13d
	xorl	%r10d, %r10d
	jmp	.L176
.L415:
	movl	$1, %r15d
	xorl	%ebx, %ebx
	xorl	%r12d, %r12d
	xorl	%r10d, %r10d
	movq	$0, -1352(%rbp)
	jmp	.L176
.L710:
	call	ERR_clear_error@PLT
	movl	$4097, -1464(%rbp)
	movq	$4097, -1472(%rbp)
	jmp	.L214
.L222:
	cmpl	$116, -1344(%rbp)
	jne	.L394
.L393:
	movq	-1288(%rbp), %rcx
	cmpq	$10000, %rcx
	jle	.L226
	movq	bio_err(%rip), %rdi
	movl	$10000, %edx
	leaq	.LC46(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L226
.L726:
	call	ERR_clear_error@PLT
	jmp	.L201
.L249:
	cmpq	$0, -1512(%rbp)
	setne	%al
	xorl	%r12d, %r12d
	andb	-1600(%rbp), %al
	jmp	.L392
.L739:
	movq	%rbx, %rdi
	movq	%r12, %rsi
	call	X509_REQ_sign_ctx@PLT
	movq	%r12, %rdi
	movl	%eax, %r13d
	call	EVP_MD_CTX_free@PLT
	testl	%r13d, %r13d
	jle	.L377
	xorl	%r12d, %r12d
	xorl	%r10d, %r10d
	jmp	.L340
.L352:
	movl	-1516(%rbp), %eax
	testl	%eax, %eax
	je	.L359
	movl	%eax, -1440(%rbp)
	movl	-1432(%rbp), %eax
	testl	%eax, %eax
	je	.L360
	movl	%eax, -1440(%rbp)
.L389:
	movq	%r10, -1352(%rbp)
	call	get_nameopt@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	call	X509_get_subject_name@PLT
	movq	%r14, %rcx
	leaq	.LC94(%rip), %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	print_name@PLT
	cmpl	$0, -1492(%rbp)
	movq	-1352(%rbp), %r10
	jne	.L361
.L362:
	movl	-1496(%rbp), %eax
	xorl	$1, %eax
	testl	%eax, -1432(%rbp)
	je	.L435
	movq	$0, -1352(%rbp)
	testq	%r12, %r12
	je	.L177
	cmpl	$4, -1336(%rbp)
	movq	%r10, -1352(%rbp)
	movq	%r12, %rsi
	movq	%r13, %rdi
	je	.L757
	call	PEM_write_bio_X509@PLT
	movq	-1352(%rbp), %r10
.L374:
	movq	$0, -1352(%rbp)
	testl	%eax, %eax
	jne	.L177
	movq	bio_err(%rip), %rdi
	leaq	.LC99(%rip), %rsi
	movq	%r10, -1432(%rbp)
	call	BIO_printf@PLT
	movl	-1440(%rbp), %r15d
	movq	-1432(%rbp), %r10
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L428:
	movl	$119, %esi
	jmp	.L349
.L430:
	movq	$0, -1352(%rbp)
	movl	$1, %r15d
	jmp	.L176
.L353:
	call	get_nameopt@PLT
	movq	%r14, %rcx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	X509_REQ_print_ex@PLT
	movq	-1352(%rbp), %r10
	testl	%eax, %eax
	movl	%eax, -1440(%rbp)
	je	.L758
	cmpl	$0, -1516(%rbp)
	jne	.L360
	cmpl	$0, -1492(%rbp)
	je	.L365
.L363:
	movq	%rbx, %rdi
	movq	%r10, -1448(%rbp)
	call	X509_REQ_get0_pubkey@PLT
	movq	-1448(%rbp), %r10
	movq	%rax, -1352(%rbp)
	jmp	.L366
.L752:
	movq	%rbx, %rdi
	movq	%r10, -1440(%rbp)
	call	X509_REQ_get0_pubkey@PLT
	movq	-1440(%rbp), %r10
	testq	%rax, %rax
	movq	%rax, -1352(%rbp)
	je	.L759
	movq	%rax, %rsi
	movq	%r13, %rdi
	movq	%r10, -1440(%rbp)
	call	PEM_write_bio_PUBKEY@PLT
	movq	-1440(%rbp), %r10
	jmp	.L350
.L360:
	movq	%r10, -1352(%rbp)
	call	get_nameopt@PLT
	movq	%rbx, %rdi
	movq	%rax, %r14
	call	X509_REQ_get_subject_name@PLT
	movq	%r14, %rcx
	leaq	.LC94(%rip), %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	print_name@PLT
	cmpl	$0, -1492(%rbp)
	movq	-1352(%rbp), %r10
	jne	.L363
	cmpl	$0, -1496(%rbp)
	je	.L388
.L435:
	movq	$0, -1352(%rbp)
	xorl	%r15d, %r15d
	jmp	.L177
	.p2align 4,,10
	.p2align 3
.L400:
	movq	%r13, %rdx
	xorl	%ebx, %ebx
	xorl	%r12d, %r12d
	xorl	%r13d, %r13d
	movq	%rdx, -1392(%rbp)
	xorl	%r10d, %r10d
	movl	$1, %r15d
	movq	$0, -1424(%rbp)
	movq	$0, -1416(%rbp)
	movq	$0, -1352(%rbp)
	jmp	.L176
.L427:
	movq	$0, -1352(%rbp)
	xorl	%r15d, %r15d
	xorl	%r13d, %r13d
	jmp	.L177
.L210:
	call	ERR_clear_error@PLT
	jmp	.L686
.L729:
	xorl	%ebx, %ebx
.L247:
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
	xorl	%r12d, %r12d
	xorl	%r13d, %r13d
	leaq	.LC56(%rip), %rsi
	movq	%r10, -1432(%rbp)
	movl	$1, %r15d
	call	BIO_printf@PLT
	movq	-1432(%rbp), %r10
	movq	$0, -1352(%rbp)
	jmp	.L176
.L740:
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%rax, -1352(%rbp)
	call	d2i_X509_REQ_bio@PLT
	movq	-1352(%rbp), %r10
	movq	%rax, %rbx
	jmp	.L245
.L359:
	movl	-1492(%rbp), %eax
	testl	%eax, %eax
	je	.L432
	movl	%eax, -1440(%rbp)
	movl	-1432(%rbp), %eax
	testl	%eax, %eax
	je	.L363
	movl	%eax, -1440(%rbp)
	jmp	.L361
.L208:
	call	ERR_clear_error@PLT
	jmp	.L685
.L742:
	cmpq	$0, addext_conf(%rip)
	jne	.L318
	jmp	.L321
.L394:
	movq	-1400(%rbp), %r8
	leaq	-1312(%rbp), %rcx
	movq	%r12, %rdx
	xorl	%edi, %edi
	leaq	-1344(%rbp), %rsi
	call	set_keygen_ctx
	movq	%rax, -1352(%rbp)
	testq	%rax, %rax
	jne	.L229
.L412:
	xorl	%ebx, %ebx
	xorl	%r12d, %r12d
	xorl	%r13d, %r13d
	xorl	%r10d, %r10d
	movl	$1, %r15d
	jmp	.L176
.L750:
	movq	%r10, -1352(%rbp)
	call	get_nameopt@PLT
	movq	%rbx, %rdi
	movq	%rax, %r13
	call	X509_REQ_get_subject_name@PLT
	movq	bio_err(%rip), %rdi
	movq	%r13, %rcx
	leaq	.LC88(%rip), %rsi
	movq	%rax, %rdx
	call	print_name@PLT
	movq	-1352(%rbp), %r10
	jmp	.L340
.L749:
	movq	%r13, %rdi
	call	X509_NAME_free@PLT
	movq	-1352(%rbp), %r10
.L342:
	movq	%r10, -1432(%rbp)
	leaq	.LC87(%rip), %rsi
	jmp	.L694
.L748:
	movq	bio_err(%rip), %rdi
	leaq	.LC85(%rip), %rsi
	xorl	%eax, %eax
	movq	%r10, -1352(%rbp)
	call	BIO_printf@PLT
	call	get_nameopt@PLT
	movq	%rbx, %rdi
	movq	%rax, %r13
	call	X509_REQ_get_subject_name@PLT
	movq	bio_err(%rip), %rdi
	movq	%r13, %rcx
	leaq	.LC86(%rip), %rsi
	movq	%rax, %rdx
	call	print_name@PLT
	movq	-1352(%rbp), %r10
	jmp	.L341
.L347:
	leaq	.LC90(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	-1352(%rbp), %r10
	jmp	.L345
.L426:
	movl	-1352(%rbp), %r15d
	xorl	%r13d, %r13d
	movq	$0, -1352(%rbp)
	jmp	.L176
.L751:
	movq	%rbx, %rdi
	movq	%r10, -1440(%rbp)
	call	X509_REQ_get0_pubkey@PLT
	movq	-1440(%rbp), %r10
	testq	%rax, %rax
	movq	%rax, %r13
	jne	.L346
	movl	-1352(%rbp), %r15d
	movq	$0, -1352(%rbp)
	jmp	.L176
.L736:
	movq	$0, -1512(%rbp)
	xorl	%r10d, %r10d
	jmp	.L250
.L715:
	movq	bio_err(%rip), %rdi
	leaq	.LC49(%rip), %rsi
	call	BIO_puts@PLT
	jmp	.L696
.L714:
	leaq	.LC47(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L236
.L438:
	movq	bio_err(%rip), %rdi
	leaq	.LC43(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	-1288(%rbp), %rcx
	xorl	%eax, %eax
	movq	bio_err(%rip), %rdi
	movl	$512, %edx
	leaq	.LC44(%rip), %rsi
	call	BIO_printf@PLT
	jmp	.L696
.L225:
	cmpq	$16384, %rcx
	jle	.L228
	testb	%dl, %dl
	je	.L228
	movq	bio_err(%rip), %rdi
	movl	$16384, %edx
	leaq	.LC45(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movl	-1344(%rbp), %eax
.L228:
	cmpl	$116, %eax
	je	.L393
	jmp	.L226
.L716:
	movq	req_conf(%rip), %rdi
	leaq	.LC50(%rip), %rdx
	leaq	.LC28(%rip), %rsi
	call	NCONF_get_string@PLT
	movq	%rax, -1504(%rbp)
	testq	%rax, %rax
	jne	.L238
	call	ERR_clear_error@PLT
	movq	bio_err(%rip), %rdi
	leaq	.LC51(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L381
.L759:
	movq	bio_err(%rip), %rdi
	leaq	.LC91(%rip), %rsi
	movq	%r10, -1432(%rbp)
	movl	%r14d, %r15d
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	movq	-1432(%rbp), %r10
	jmp	.L176
.L432:
	movl	$1, -1440(%rbp)
	jmp	.L365
.L747:
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	movq	-1352(%rbp), %r10
	jmp	.L326
.L738:
	movq	bio_err(%rip), %rdi
	leaq	.LC32(%rip), %rsi
	call	BIO_printf@PLT
	jmp	.L334
.L735:
	call	X509_NAME_free@PLT
.L253:
	movq	bio_err(%rip), %rdi
	leaq	.LC82(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L334
.L258:
	movq	%rbx, %rdi
	testb	%r12b, %r12b
	je	.L262
	call	X509_REQ_get_subject_name@PLT
	xorl	%r13d, %r13d
	movq	%rax, %r12
	movq	%rbx, %rax
	movl	%r13d, %ebx
	movq	%rax, %r13
.L263:
	movq	-1544(%rbp), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jge	.L760
	movq	-1544(%rbp), %rdi
	movl	%ebx, %esi
	call	OPENSSL_sk_value@PLT
	movq	8(%rax), %rsi
	movq	%rax, %rdx
	movzbl	(%rsi), %eax
	testb	%al, %al
	je	.L419
	movq	%rsi, %rcx
	jmp	.L266
.L761:
	testb	%al, %al
	je	.L267
.L266:
	movl	%eax, %edi
	andl	$-3, %edi
	cmpb	$44, %dil
	sete	%r8b
	cmpb	$58, %al
	movzbl	1(%rcx), %eax
	sete	%dil
	addq	$1, %rcx
	orb	%dil, %r8b
	je	.L761
	testb	%al, %al
	cmovne	%rcx, %rsi
.L267:
	xorl	%eax, %eax
	cmpb	$43, (%rsi)
	jne	.L264
	addq	$1, %rsi
	orl	$-1, %eax
.L264:
	movq	16(%rdx), %rcx
	pushq	%rdi
	orl	$-1, %r9d
	orl	$-1, %r8d
	pushq	%rax
	movl	-1464(%rbp), %edx
	movq	%r12, %rdi
	call	X509_NAME_add_entry_by_txt@PLT
	popq	%r8
	popq	%r9
	testl	%eax, %eax
	je	.L679
	addl	$1, %ebx
	jmp	.L263
.L679:
	movq	%r13, %rbx
	jmp	.L253
.L419:
	xorl	%eax, %eax
	jmp	.L264
.L760:
	movq	%r12, %rdi
	movq	%r13, %rbx
	call	X509_NAME_entry_count@PLT
	testl	%eax, %eax
	je	.L691
	cmpl	$0, -1432(%rbp)
	jne	.L271
	movq	-1560(%rbp), %r13
	xorl	%r12d, %r12d
	jmp	.L270
.L272:
	movl	%r12d, %esi
	movq	%r13, %rdi
	call	OPENSSL_sk_value@PLT
	movl	-1464(%rbp), %edx
	orl	$-1, %r8d
	movq	%rbx, %rdi
	movq	16(%rax), %rcx
	movq	8(%rax), %rsi
	call	X509_REQ_add1_attr_by_txt@PLT
	testl	%eax, %eax
	je	.L253
	addl	$1, %r12d
.L270:
	movq	%r13, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r12d
	jl	.L272
	jmp	.L690
.L262:
	call	X509_REQ_get_subject_name@PLT
	cmpl	$0, batch(%rip)
	movq	%rax, -1616(%rbp)
	je	.L762
.L274:
	movq	-1544(%rbp), %rdi
	call	OPENSSL_sk_num@PLT
	movl	$-1, -1592(%rbp)
	testl	%eax, %eax
	je	.L668
	movl	%r14d, -1628(%rbp)
	movq	%rbx, -1352(%rbp)
	movl	%r15d, -1632(%rbp)
	movq	%r13, %r15
.L275:
	movq	-1544(%rbp), %r14
	addl	$1, -1592(%rbp)
	movl	-1592(%rbp), %ebx
	movq	%r14, %rdi
	call	OPENSSL_sk_num@PLT
	movl	%ebx, %esi
	cmpl	%eax, %ebx
	jge	.L277
	movq	%r14, %rdi
	call	OPENSSL_sk_value@PLT
	movq	8(%rax), %r12
	movq	%rax, %rbx
	movq	%r12, %rdi
	call	strlen@PLT
	movq	%rax, %r13
	cmpq	$3, %rax
	jbe	.L278
	leaq	-4(%r12,%rax), %rax
	movl	$5, %ecx
	leaq	.LC69(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L275
	movq	%rax, %rsi
	movl	$5, %ecx
	leaq	.LC70(%rip), %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L275
	cmpq	$7, %r13
	jbe	.L280
	leaq	-8(%r12,%r13), %rdi
	leaq	.LC71(%rip), %rsi
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L275
.L281:
	leaq	-6(%r12,%r13), %rdi
	leaq	.LC72(%rip), %rsi
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L275
.L278:
	movzbl	(%r12), %eax
	movq	%r12, %rdx
	xorl	%r14d, %r14d
	testb	%al, %al
	jne	.L284
	jmp	.L285
	.p2align 4,,10
	.p2align 3
.L282:
	testb	%al, %al
	je	.L283
.L284:
	movl	%eax, %ecx
	andl	$-3, %ecx
	cmpb	$44, %cl
	sete	%sil
	cmpb	$58, %al
	movzbl	1(%rdx), %eax
	sete	%cl
	addq	$1, %rdx
	orb	%cl, %sil
	je	.L282
	testb	%al, %al
	cmovne	%rdx, %r12
.L283:
	xorl	%r14d, %r14d
	cmpb	$43, (%r12)
	jne	.L285
	addq	$1, %r12
	orl	$-1, %r14d
.L285:
	movq	%r12, %rdi
	call	OBJ_txt2nid@PLT
	movl	%eax, -1608(%rbp)
	testl	%eax, %eax
	je	.L275
	movq	8(%rbx), %rsi
	xorl	%eax, %eax
	orq	$-1, %rcx
	movq	%rsi, %rdi
	repnz scasb
	movq	%rcx, %rax
	notq	%rax
	leaq	-1(%rax), %r13
	addq	$8, %rax
	cmpq	$100, %rax
	ja	.L697
	leaq	-1200(%rbp), %r12
	movq	%r13, %rdx
	movl	$100, %ecx
	movq	%r12, %rdi
	addq	%r12, %r13
	call	__memcpy_chk@PLT
	movb	$0, 8(%r13)
	movq	%r12, %rdx
	movq	%r15, %rsi
	movabsq	$8389209267074589791, %rax
	movq	req_conf(%rip), %rdi
	movq	%rax, 0(%r13)
	call	NCONF_get_string@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L763
.L287:
	movq	8(%rbx), %rsi
	xorl	%eax, %eax
	orq	$-1, %rcx
	movq	%rsi, %rdi
	repnz scasb
	movq	%rcx, %rax
	notq	%rax
	leaq	-1(%rax), %rdx
	addq	$6, %rax
	cmpq	$100, %rax
	ja	.L697
	movl	$100, %ecx
	movq	%r12, %rdi
	movq	%rdx, -1624(%rbp)
	call	__memcpy_chk@PLT
	movq	-1624(%rbp), %rdx
	movl	$7, %ecx
	leaq	.LC72(%rip), %rsi
	addq	%r12, %rdx
	movq	%rdx, %rdi
	movq	%r12, %rdx
	rep movsb
	movq	req_conf(%rip), %rdi
	movq	%r15, %rsi
	call	NCONF_get_string@PLT
	movq	%rax, -1624(%rbp)
	testq	%rax, %rax
	je	.L764
.L289:
	movq	8(%rbx), %rsi
	xorl	%eax, %eax
	orq	$-1, %rcx
	movq	%rsi, %rdi
	repnz scasb
	movq	%rcx, %rax
	notq	%rax
	leaq	-1(%rax), %rdx
	addq	$4, %rax
	cmpq	$100, %rax
	ja	.L697
	movl	$100, %ecx
	movq	%r12, %rdi
	movq	%rdx, -1640(%rbp)
	call	__memcpy_chk@PLT
	movq	-1640(%rbp), %rdx
	movq	req_conf(%rip), %rdi
	movq	%r15, %rsi
	leaq	-1272(%rbp), %rcx
	addq	%r12, %rdx
	movl	$1852403039, (%rdx)
	movb	$0, 4(%rdx)
	movq	%r12, %rdx
	call	NCONF_get_number_e@PLT
	testl	%eax, %eax
	je	.L765
.L291:
	movq	8(%rbx), %rsi
	xorl	%eax, %eax
	orq	$-1, %rcx
	movq	%rsi, %rdi
	repnz scasb
	movq	%rcx, %rax
	notq	%rax
	leaq	-1(%rax), %rdx
	addq	$4, %rax
	cmpq	$100, %rax
	ja	.L697
	movl	$100, %ecx
	movq	%r12, %rdi
	movq	%rdx, -1640(%rbp)
	call	__memcpy_chk@PLT
	movq	-1640(%rbp), %rdx
	movq	req_conf(%rip), %rdi
	movq	%r15, %rsi
	leaq	-1264(%rbp), %rcx
	addq	%r12, %rdx
	movl	$2019650911, (%rdx)
	movb	$0, 4(%rdx)
	movq	%r12, %rdx
	call	NCONF_get_number_e@PLT
	testl	%eax, %eax
	je	.L766
.L293:
	leaq	.LC74(%rip), %rax
	movq	16(%rbx), %rdi
	movq	%r13, %rsi
	movq	-1624(%rbp), %rdx
	pushq	%rax
	leaq	.LC75(%rip), %rax
	movl	-1264(%rbp), %r8d
	leaq	-1088(%rbp), %r12
	pushq	%rax
	movl	-1272(%rbp), %ecx
	movq	%r12, %r9
	call	build_data.constprop.0
	popq	%r13
	popq	%rdx
	cmpl	$1, %eax
	jbe	.L294
	pushq	%r11
	movl	-1464(%rbp), %edx
	movq	%r12, %rcx
	orl	$-1, %r9d
	pushq	%r14
	movl	-1608(%rbp), %esi
	orl	$-1, %r8d
	movq	-1616(%rbp), %rdi
	call	X509_NAME_add_entry_by_NID@PLT
	popq	%rbx
	popq	%r12
	testl	%eax, %eax
	jne	.L275
.L692:
	movq	-1352(%rbp), %rbx
	jmp	.L253
.L668:
	movq	bio_err(%rip), %rdi
	leaq	.LC81(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L253
.L277:
	movq	-1616(%rbp), %rdi
	movl	-1628(%rbp), %r14d
	movq	-1352(%rbp), %rbx
	movl	-1632(%rbp), %r15d
	call	X509_NAME_entry_count@PLT
	testl	%eax, %eax
	je	.L691
	cmpl	$0, -1432(%rbp)
	jne	.L271
	movq	-1560(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L690
	call	OPENSSL_sk_num@PLT
	testl	%eax, %eax
	jle	.L301
	cmpl	$0, batch(%rip)
	je	.L767
.L301:
	movl	$-1, -1352(%rbp)
	movl	%r15d, -1512(%rbp)
	movl	%r14d, -1480(%rbp)
	movq	-1576(%rbp), %r14
.L302:
	movq	-1560(%rbp), %r13
	addl	$1, -1352(%rbp)
	movl	-1352(%rbp), %r15d
	movq	%r13, %rdi
	call	OPENSSL_sk_num@PLT
	movl	%r15d, %esi
	cmpl	%eax, %r15d
	jge	.L303
	movq	%r13, %rdi
	call	OPENSSL_sk_value@PLT
	movq	8(%rax), %r15
	movq	%rax, -1440(%rbp)
	movq	%r15, %rdi
	call	OBJ_txt2nid@PLT
	movl	%eax, -1448(%rbp)
	testl	%eax, %eax
	je	.L302
	xorl	%eax, %eax
	orq	$-1, %rcx
	movq	%r15, %rdi
	repnz scasb
	movq	%rcx, %rax
	notq	%rax
	leaq	-1(%rax), %r13
	addq	$8, %rax
	cmpq	$100, %rax
	ja	.L693
	leaq	-1200(%rbp), %r12
	movq	%r13, %rdx
	movl	$100, %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	addq	%r12, %r13
	call	__memcpy_chk@PLT
	movb	$0, 8(%r13)
	movq	%r12, %rdx
	movq	%r14, %rsi
	movabsq	$8389209267074589791, %rax
	movq	req_conf(%rip), %rdi
	movq	%rax, 0(%r13)
	call	NCONF_get_string@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L768
.L306:
	xorl	%eax, %eax
	orq	$-1, %rcx
	movq	%r15, %rdi
	repnz scasb
	movq	%rcx, %rax
	notq	%rax
	leaq	-1(%rax), %rdx
	addq	$6, %rax
	cmpq	$100, %rax
	ja	.L693
	movl	$100, %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rdx, -1472(%rbp)
	call	__memcpy_chk@PLT
	movq	-1472(%rbp), %rdx
	movl	$7, %ecx
	leaq	.LC72(%rip), %rsi
	addq	%r12, %rdx
	movq	%rdx, %rdi
	movq	%r12, %rdx
	rep movsb
	movq	req_conf(%rip), %rdi
	movq	%r14, %rsi
	call	NCONF_get_string@PLT
	movq	%rax, -1472(%rbp)
	testq	%rax, %rax
	je	.L769
.L308:
	xorl	%eax, %eax
	orq	$-1, %rcx
	movq	%r15, %rdi
	repnz scasb
	movq	%rcx, %rax
	notq	%rax
	leaq	-1(%rax), %rdx
	addq	$4, %rax
	cmpq	$100, %rax
	ja	.L693
	movl	$100, %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rdx, -1536(%rbp)
	call	__memcpy_chk@PLT
	movq	-1536(%rbp), %rdx
	movq	req_conf(%rip), %rdi
	movq	%r14, %rsi
	leaq	-1272(%rbp), %rcx
	addq	%r12, %rdx
	movl	$1852403039, (%rdx)
	movb	$0, 4(%rdx)
	movq	%r12, %rdx
	call	NCONF_get_number_e@PLT
	testl	%eax, %eax
	je	.L770
.L310:
	xorl	%eax, %eax
	orq	$-1, %rcx
	movq	%r15, %rdi
	repnz scasb
	movq	%rcx, %rax
	notq	%rax
	leaq	-1(%rax), %rdx
	addq	$4, %rax
	cmpq	$100, %rax
	ja	.L693
	movl	$100, %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rdx, -1536(%rbp)
	call	__memcpy_chk@PLT
	movq	-1536(%rbp), %rdx
	movq	req_conf(%rip), %rdi
	movq	%r14, %rsi
	leaq	-1264(%rbp), %rcx
	addq	%r12, %rdx
	movl	$2019650911, (%rdx)
	movb	$0, 4(%rdx)
	movq	%r12, %rdx
	call	NCONF_get_number_e@PLT
	testl	%eax, %eax
	je	.L771
.L312:
	movq	-1440(%rbp), %rax
	movl	-1272(%rbp), %ecx
	leaq	-1088(%rbp), %r12
	movq	%r13, %rsi
	movl	-1264(%rbp), %r8d
	movq	-1472(%rbp), %rdx
	movq	%r12, %r9
	movq	16(%rax), %rdi
	leaq	.LC78(%rip), %rax
	pushq	%rax
	leaq	.LC79(%rip), %rax
	pushq	%rax
	call	build_data.constprop.0
	popq	%r9
	popq	%r10
	cmpl	$1, %eax
	jbe	.L313
	movl	-1464(%rbp), %edx
	orl	$-1, %r8d
	movq	%r12, %rcx
	movq	%rbx, %rdi
	movl	-1448(%rbp), %esi
	call	X509_REQ_add1_attr_by_NID@PLT
	testl	%eax, %eax
	jne	.L302
	movq	bio_err(%rip), %rdi
	leaq	.LC80(%rip), %rsi
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	jmp	.L253
.L280:
	cmpq	$5, %r13
	ja	.L281
	jmp	.L278
.L767:
	movq	bio_err(%rip), %rdi
	leaq	.LC76(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	leaq	.LC77(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L301
.L313:
	testl	%eax, %eax
	jne	.L302
	jmp	.L253
.L771:
	call	ERR_clear_error@PLT
	movq	$-1, -1264(%rbp)
	jmp	.L312
.L770:
	call	ERR_clear_error@PLT
	movq	$-1, -1272(%rbp)
	jmp	.L310
.L769:
	call	ERR_clear_error@PLT
	jmp	.L308
.L768:
	call	ERR_clear_error@PLT
	leaq	.LC18(%rip), %r13
	jmp	.L306
.L693:
	movq	%r15, %rcx
.L688:
	movq	bio_err(%rip), %rdi
	leaq	.LC73(%rip), %rdx
	leaq	.LC13(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L253
.L303:
	movl	-1480(%rbp), %r14d
	movl	-1512(%rbp), %r15d
.L690:
	movq	-1456(%rbp), %rsi
	movq	%rbx, %rdi
	call	X509_REQ_set_pubkey@PLT
	testl	%eax, %eax
	jne	.L316
	jmp	.L253
.L734:
	movq	-1576(%rbp), %rdx
	movq	bio_err(%rip), %rdi
	leaq	.LC60(%rip), %rsi
	call	BIO_printf@PLT
	jmp	.L253
.L271:
	movq	-1456(%rbp), %rsi
	movq	%rbx, %rdi
	call	X509_REQ_set_pubkey@PLT
	testl	%eax, %eax
	je	.L253
	xorl	%r10d, %r10d
	jmp	.L250
.L691:
	movq	bio_err(%rip), %rdi
	leaq	.LC62(%rip), %rsi
	call	BIO_printf@PLT
	jmp	.L253
.L762:
	movq	bio_err(%rip), %rdi
	leaq	.LC63(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	leaq	.LC64(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	leaq	.LC65(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	leaq	.LC66(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	leaq	.LC67(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	leaq	.LC68(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	leaq	.LC54(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L274
.L294:
	testl	%eax, %eax
	jne	.L275
	jmp	.L692
.L766:
	call	ERR_clear_error@PLT
	movq	$-1, -1264(%rbp)
	jmp	.L293
.L765:
	call	ERR_clear_error@PLT
	movq	$-1, -1272(%rbp)
	jmp	.L291
.L764:
	call	ERR_clear_error@PLT
	jmp	.L289
.L763:
	call	ERR_clear_error@PLT
	leaq	.LC18(%rip), %r13
	jmp	.L287
.L697:
	movq	-1352(%rbp), %rbx
	movq	%rsi, %rcx
	jmp	.L688
.L733:
	call	ERR_clear_error@PLT
	movq	$0, -1560(%rbp)
	jmp	.L256
.L732:
	movq	bio_err(%rip), %rdi
	movq	%r13, %rdx
	leaq	.LC60(%rip), %rsi
	call	BIO_printf@PLT
	jmp	.L253
.L731:
	movq	bio_err(%rip), %rdi
	leaq	.LC58(%rip), %rdx
	leaq	.LC59(%rip), %rsi
	call	BIO_printf@PLT
	jmp	.L253
.L730:
	call	ERR_clear_error@PLT
	xorl	%r12d, %r12d
	jmp	.L378
.L721:
	call	__stack_chk_fail@PLT
.L737:
	movq	-1488(%rbp), %rdx
	movq	bio_err(%rip), %rdi
	leaq	.LC30(%rip), %rsi
	call	BIO_printf@PLT
	jmp	.L334
.L418:
	xorl	%r12d, %r12d
	xorl	%r13d, %r13d
	xorl	%r10d, %r10d
	movl	$1, %r15d
	movq	$0, -1352(%rbp)
	jmp	.L176
.L741:
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
	xorl	%r12d, %r12d
	xorl	%r13d, %r13d
	leaq	.LC55(%rip), %rsi
	movq	%r10, -1432(%rbp)
	movl	$1, %r15d
	call	BIO_printf@PLT
	movq	-1432(%rbp), %r10
	movq	$0, -1352(%rbp)
	jmp	.L176
.L372:
	call	PEM_write_bio_X509_REQ@PLT
	movq	-1352(%rbp), %r10
	jmp	.L371
.L756:
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%r10, -1352(%rbp)
	call	i2d_X509_REQ_bio@PLT
	movq	-1352(%rbp), %r10
	jmp	.L371
.L755:
	movq	-1352(%rbp), %rdi
	call	EVP_PKEY_get0_RSA@PLT
	leaq	-1264(%rbp), %rsi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rdi
	call	RSA_get0_key@PLT
	movq	-1264(%rbp), %rsi
	movq	%r13, %rdi
	call	BN_print@PLT
	movq	-1448(%rbp), %r10
	jmp	.L369
.L754:
	movl	$20, %edx
	movl	$1, %esi
	leaq	.LC95(%rip), %rdi
	movq	%r10, -1432(%rbp)
	call	fwrite@PLT
	movl	-1440(%rbp), %r15d
	movq	-1432(%rbp), %r10
	jmp	.L176
.L757:
	call	i2d_X509_bio@PLT
	movq	-1352(%rbp), %r10
	jmp	.L374
.L743:
	movq	%r12, %rdi
	movq	%r10, -1352(%rbp)
	call	X509_get_serialNumber@PLT
	xorl	%edi, %edi
	movq	%rax, %rsi
	call	rand_serial@PLT
	movq	-1352(%rbp), %r10
	testl	%eax, %eax
	jne	.L323
	jmp	.L326
.L745:
	movq	bio_err(%rip), %rdi
	leaq	.LC32(%rip), %rsi
	call	BIO_printf@PLT
	movq	-1352(%rbp), %r10
	jmp	.L326
.L746:
	movq	bio_err(%rip), %rdi
	leaq	.LC83(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	-1352(%rbp), %r10
	jmp	.L326
.L753:
	movq	bio_err(%rip), %rdi
	leaq	.LC92(%rip), %rsi
	xorl	%eax, %eax
	movq	%r10, -1352(%rbp)
	call	BIO_printf@PLT
	movq	-1352(%rbp), %r10
.L358:
	movq	bio_err(%rip), %rdi
	movq	%r10, -1432(%rbp)
	call	ERR_print_errors@PLT
	movq	-1432(%rbp), %r10
	movq	$0, -1352(%rbp)
	jmp	.L177
.L242:
	movq	%r13, %rdi
	call	BIO_free@PLT
	movq	bio_err(%rip), %rdi
	leaq	.LC54(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	-1328(%rbp), %r10
	jmp	.L243
.L717:
	call	ERR_clear_error@PLT
	movq	req_conf(%rip), %rdi
	leaq	.LC53(%rip), %rdx
	leaq	.LC28(%rip), %rsi
	call	NCONF_get_string@PLT
	testq	%rax, %rax
	jne	.L239
	call	ERR_clear_error@PLT
	jmp	.L380
.L758:
	movq	bio_err(%rip), %rdi
	leaq	.LC93(%rip), %rsi
	xorl	%eax, %eax
	movq	%r10, -1352(%rbp)
	call	BIO_printf@PLT
	movq	-1352(%rbp), %r10
	jmp	.L358
	.cfi_endproc
.LFE1441:
	.size	req_main, .-req_main
	.p2align 4
	.globl	do_X509_REQ_sign
	.type	do_X509_REQ_sign, @function
do_X509_REQ_sign:
.LFB1456:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$8, %rsp
	call	EVP_MD_CTX_new@PLT
	movq	%rbx, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	do_sign_init
	movl	%eax, %ebx
	testl	%eax, %eax
	jle	.L773
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	X509_REQ_sign_ctx@PLT
	movl	%eax, %ebx
.L773:
	movq	%r12, %rdi
	call	EVP_MD_CTX_free@PLT
	xorl	%eax, %eax
	testl	%ebx, %ebx
	setg	%al
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1456:
	.size	do_X509_REQ_sign, .-do_X509_REQ_sign
	.p2align 4
	.globl	do_X509_CRL_sign
	.type	do_X509_CRL_sign, @function
do_X509_CRL_sign:
.LFB1457:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$8, %rsp
	call	EVP_MD_CTX_new@PLT
	movq	%rbx, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	do_sign_init
	movl	%eax, %ebx
	testl	%eax, %eax
	jle	.L777
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	X509_CRL_sign_ctx@PLT
	movl	%eax, %ebx
.L777:
	movq	%r12, %rdi
	call	EVP_MD_CTX_free@PLT
	xorl	%eax, %eax
	testl	%ebx, %ebx
	setg	%al
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1457:
	.size	do_X509_CRL_sign, .-do_X509_CRL_sign
	.globl	req_options
	.section	.rodata.str1.1
.LC102:
	.string	"help"
.LC103:
	.string	"Display this summary"
.LC104:
	.string	"inform"
.LC105:
	.string	"Input format - DER or PEM"
.LC106:
	.string	"outform"
.LC107:
	.string	"Output format - DER or PEM"
.LC108:
	.string	"in"
.LC109:
	.string	"Input file"
.LC110:
	.string	"out"
.LC111:
	.string	"Output file"
.LC112:
	.string	"key"
.LC113:
	.string	"Private key to use"
.LC114:
	.string	"keyform"
.LC115:
	.string	"Key file format"
.LC116:
	.string	"pubkey"
.LC117:
	.string	"Output public key"
.LC118:
	.string	"new"
.LC119:
	.string	"New request"
.LC120:
	.string	"config"
.LC121:
	.string	"Request template file"
.LC122:
	.string	"keyout"
.LC123:
	.string	"File to send the key to"
.LC124:
	.string	"passin"
.LC125:
	.string	"Private key password source"
.LC126:
	.string	"passout"
	.section	.rodata.str1.8
	.align 8
.LC127:
	.string	"Output file pass phrase source"
	.section	.rodata.str1.1
.LC128:
	.string	"rand"
	.section	.rodata.str1.8
	.align 8
.LC129:
	.string	"Load the file(s) into the random number generator"
	.section	.rodata.str1.1
.LC130:
	.string	"writerand"
	.section	.rodata.str1.8
	.align 8
.LC131:
	.string	"Write random data to the specified file"
	.section	.rodata.str1.1
.LC132:
	.string	"newkey"
.LC133:
	.string	"Specify as type:bits"
.LC134:
	.string	"pkeyopt"
	.section	.rodata.str1.8
	.align 8
.LC135:
	.string	"Public key options as opt:value"
	.section	.rodata.str1.1
.LC136:
	.string	"sigopt"
	.section	.rodata.str1.8
	.align 8
.LC137:
	.string	"Signature parameter in n:v form"
	.section	.rodata.str1.1
.LC138:
	.string	"batch"
	.section	.rodata.str1.8
	.align 8
.LC139:
	.string	"Do not ask anything during request generation"
	.section	.rodata.str1.1
.LC140:
	.string	"newhdr"
	.section	.rodata.str1.8
	.align 8
.LC141:
	.string	"Output \"NEW\" in the header lines"
	.section	.rodata.str1.1
.LC142:
	.string	"modulus"
.LC143:
	.string	"RSA modulus"
.LC144:
	.string	"verify"
.LC145:
	.string	"Verify signature on REQ"
.LC146:
	.string	"nodes"
.LC147:
	.string	"Don't encrypt the output key"
.LC148:
	.string	"noout"
.LC149:
	.string	"Do not output REQ"
.LC150:
	.string	"verbose"
.LC151:
	.string	"Verbose output"
	.section	.rodata.str1.8
	.align 8
.LC152:
	.string	"Input characters are UTF8 (default ASCII)"
	.section	.rodata.str1.1
.LC153:
	.string	"nameopt"
	.section	.rodata.str1.8
	.align 8
.LC154:
	.string	"Various certificate name options"
	.section	.rodata.str1.1
.LC155:
	.string	"reqopt"
.LC156:
	.string	"Various request text options"
.LC157:
	.string	"text"
.LC158:
	.string	"Text form of request"
.LC159:
	.string	"x509"
	.section	.rodata.str1.8
	.align 8
.LC160:
	.string	"Output a x509 structure instead of a cert request"
	.section	.rodata.str1.1
.LC161:
	.string	"(Required by some CA's)"
.LC162:
	.string	"subj"
.LC163:
	.string	"Set or modify request subject"
.LC164:
	.string	"subject"
.LC165:
	.string	"Output the request's subject"
.LC166:
	.string	"multivalue-rdn"
	.section	.rodata.str1.8
	.align 8
.LC167:
	.string	"Enable support for multivalued RDNs"
	.section	.rodata.str1.1
.LC168:
	.string	"days"
	.section	.rodata.str1.8
	.align 8
.LC169:
	.string	"Number of days cert is valid for"
	.section	.rodata.str1.1
.LC170:
	.string	"set_serial"
.LC171:
	.string	"Serial number to use"
.LC172:
	.string	"addext"
	.section	.rodata.str1.8
	.align 8
.LC173:
	.string	"Additional cert extension key=value pair (may be given more than once)"
	.section	.rodata.str1.1
.LC174:
	.string	"extensions"
	.section	.rodata.str1.8
	.align 8
.LC175:
	.string	"Cert extension section (override value in config file)"
	.section	.rodata.str1.1
.LC176:
	.string	"reqexts"
	.section	.rodata.str1.8
	.align 8
.LC177:
	.string	"Request extension section (override value in config file)"
	.section	.rodata.str1.1
.LC178:
	.string	"precert"
	.section	.rodata.str1.8
	.align 8
.LC179:
	.string	"Add a poison extension (implies -new)"
	.section	.rodata.str1.1
.LC180:
	.string	"Any supported digest"
.LC181:
	.string	"engine"
	.section	.rodata.str1.8
	.align 8
.LC182:
	.string	"Use engine, possibly a hardware device"
	.section	.rodata.str1.1
.LC183:
	.string	"keygen_engine"
	.section	.rodata.str1.8
	.align 8
.LC184:
	.string	"Specify engine to be used for key generation operations"
	.section	.data.rel.ro,"aw"
	.align 32
	.type	req_options, @object
	.size	req_options, 1056
req_options:
	.quad	.LC102
	.long	1
	.long	45
	.quad	.LC103
	.quad	.LC104
	.long	2
	.long	70
	.quad	.LC105
	.quad	.LC106
	.long	3
	.long	70
	.quad	.LC107
	.quad	.LC108
	.long	11
	.long	60
	.quad	.LC109
	.quad	.LC110
	.long	12
	.long	62
	.quad	.LC111
	.quad	.LC112
	.long	6
	.long	115
	.quad	.LC113
	.quad	.LC114
	.long	10
	.long	102
	.quad	.LC115
	.quad	.LC116
	.long	7
	.long	45
	.quad	.LC117
	.quad	.LC118
	.long	8
	.long	45
	.quad	.LC119
	.quad	.LC120
	.long	9
	.long	60
	.quad	.LC121
	.quad	.LC122
	.long	13
	.long	62
	.quad	.LC123
	.quad	.LC124
	.long	14
	.long	115
	.quad	.LC125
	.quad	.LC126
	.long	15
	.long	115
	.quad	.LC127
	.quad	.LC128
	.long	1501
	.long	115
	.quad	.LC129
	.quad	.LC130
	.long	1502
	.long	62
	.quad	.LC131
	.quad	.LC132
	.long	16
	.long	115
	.quad	.LC133
	.quad	.LC134
	.long	17
	.long	115
	.quad	.LC135
	.quad	.LC136
	.long	18
	.long	115
	.quad	.LC137
	.quad	.LC138
	.long	19
	.long	45
	.quad	.LC139
	.quad	.LC140
	.long	20
	.long	45
	.quad	.LC141
	.quad	.LC142
	.long	21
	.long	45
	.quad	.LC143
	.quad	.LC144
	.long	22
	.long	45
	.quad	.LC145
	.quad	.LC146
	.long	23
	.long	45
	.quad	.LC147
	.quad	.LC148
	.long	24
	.long	45
	.quad	.LC149
	.quad	.LC150
	.long	25
	.long	45
	.quad	.LC151
	.quad	.LC37
	.long	26
	.long	45
	.quad	.LC152
	.quad	.LC153
	.long	27
	.long	115
	.quad	.LC154
	.quad	.LC155
	.long	28
	.long	115
	.quad	.LC156
	.quad	.LC157
	.long	31
	.long	45
	.quad	.LC158
	.quad	.LC159
	.long	32
	.long	45
	.quad	.LC160
	.quad	OPT_MORE_STR
	.long	1
	.long	1
	.quad	.LC161
	.quad	.LC162
	.long	29
	.long	115
	.quad	.LC163
	.quad	.LC164
	.long	30
	.long	45
	.quad	.LC165
	.quad	.LC166
	.long	33
	.long	45
	.quad	.LC167
	.quad	.LC168
	.long	34
	.long	112
	.quad	.LC169
	.quad	.LC170
	.long	35
	.long	115
	.quad	.LC171
	.quad	.LC172
	.long	36
	.long	115
	.quad	.LC173
	.quad	.LC174
	.long	37
	.long	115
	.quad	.LC175
	.quad	.LC176
	.long	38
	.long	115
	.quad	.LC177
	.quad	.LC178
	.long	39
	.long	45
	.quad	.LC179
	.quad	.LC18
	.long	40
	.long	45
	.quad	.LC180
	.quad	.LC181
	.long	4
	.long	115
	.quad	.LC182
	.quad	.LC183
	.long	5
	.long	115
	.quad	.LC184
	.quad	0
	.zero	16
	.local	batch
	.comm	batch,4,4
	.local	addext_conf
	.comm	addext_conf,8,8
	.local	req_conf
	.comm	req_conf,8,8
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
