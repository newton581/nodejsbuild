	.file	"rsautl.c"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"%s: Use -help for summary.\n"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"A private key is needed for this operation\n"
	.section	.rodata.str1.1
.LC2:
	.string	"Error getting password\n"
.LC3:
	.string	"Private Key"
.LC4:
	.string	"Public Key"
.LC5:
	.string	"Certificate"
.LC6:
	.string	"Error getting RSA key\n"
.LC7:
	.string	"hold rsa key"
.LC8:
	.string	"output rsa key"
.LC9:
	.string	"Error reading input Data\n"
.LC10:
	.string	"RSA operation error\n"
	.section	.rodata.str1.8
	.align 8
.LC11:
	.string	"../deps/openssl/openssl/apps/rsautl.c"
	.text
	.p2align 4
	.globl	rsautl_main
	.type	rsautl_main, @function
rsautl_main:
.LFB1435:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	rsautl_options(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movl	$1, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	leaq	.L7(%rip), %r13
	pushq	%r12
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	xorl	%ebx, %ebx
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -64(%rbp)
	movl	$32773, -68(%rbp)
	call	opt_init@PLT
	movb	$1, -88(%rbp)
	movq	%rax, -120(%rbp)
	movl	$0, -148(%rbp)
	movb	$2, -96(%rbp)
	movq	$0, -112(%rbp)
	movq	$0, -128(%rbp)
	movq	$0, -144(%rbp)
	movq	$0, -136(%rbp)
	movq	$0, -104(%rbp)
.L2:
	call	opt_next@PLT
	testl	%eax, %eax
	je	.L76
.L30:
	cmpl	$21, %eax
	jg	.L3
	cmpl	$-1, %eax
	jl	.L2
	addl	$1, %eax
	cmpl	$22, %eax
	ja	.L2
	movslq	0(%r13,%rax,4), %rax
	addq	%r13, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L7:
	.long	.L27-.L7
	.long	.L2-.L7
	.long	.L26-.L7
	.long	.L25-.L7
	.long	.L24-.L7
	.long	.L23-.L7
	.long	.L22-.L7
	.long	.L54-.L7
	.long	.L21-.L7
	.long	.L20-.L7
	.long	.L19-.L7
	.long	.L18-.L7
	.long	.L17-.L7
	.long	.L16-.L7
	.long	.L15-.L7
	.long	.L14-.L7
	.long	.L13-.L7
	.long	.L12-.L7
	.long	.L11-.L7
	.long	.L10-.L7
	.long	.L9-.L7
	.long	.L8-.L7
	.long	.L6-.L7
	.text
.L54:
	movl	$1, %ebx
	call	opt_next@PLT
	testl	%eax, %eax
	jne	.L30
.L76:
	call	opt_num_rest@PLT
	movl	%eax, %r13d
	testl	%eax, %eax
	jne	.L27
	cmpb	$1, %r14b
	je	.L32
	testl	%r15d, %r15d
	je	.L32
	movq	bio_err(%rip), %rdi
	leaq	.LC1(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	.p2align 4,,10
	.p2align 3
.L55:
	movl	$1, %r13d
.L74:
	xorl	%r9d, %r9d
	xorl	%r15d, %r15d
	xorl	%r14d, %r14d
	xorl	%r10d, %r10d
	xorl	%r11d, %r11d
.L29:
	movq	%r14, %rdi
	movq	%r9, -112(%rbp)
	movq	%r10, -96(%rbp)
	movq	%r11, -88(%rbp)
	call	RSA_free@PLT
	movq	-104(%rbp), %rdi
	call	release_engine@PLT
	movq	-88(%rbp), %r11
	movq	%r11, %rdi
	call	BIO_free@PLT
	movq	-96(%rbp), %r10
	movq	%r10, %rdi
	call	BIO_free_all@PLT
	movl	$273, %edx
	leaq	.LC11(%rip), %rsi
	movq	%r15, %rdi
	call	CRYPTO_free@PLT
	movq	-112(%rbp), %r9
	movl	$274, %edx
	leaq	.LC11(%rip), %rsi
	movq	%r9, %rdi
	call	CRYPTO_free@PLT
	movq	-64(%rbp), %rdi
	movl	$275, %edx
	leaq	.LC11(%rip), %rsi
	call	CRYPTO_free@PLT
.L1:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L77
	addq	$120, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L22:
	.cfi_restore_state
	movl	$1, %r12d
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L3:
	leal	-1501(%rax), %edx
	cmpl	$1, %edx
	ja	.L2
	movl	%eax, %edi
	call	opt_rand@PLT
	testl	%eax, %eax
	jne	.L2
	jmp	.L55
.L6:
	call	opt_arg@PLT
	leaq	-68(%rbp), %rdx
	movl	$18, %esi
	movq	%rax, %rdi
	call	opt_format@PLT
	testl	%eax, %eax
	jne	.L2
.L27:
	movq	-120(%rbp), %rdx
	movq	bio_err(%rip), %rdi
	leaq	.LC0(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L55
.L8:
	call	opt_arg@PLT
	movq	%rax, -112(%rbp)
	jmp	.L2
.L9:
	call	opt_arg@PLT
	movq	%rax, -128(%rbp)
	jmp	.L2
.L10:
	movl	$3, %r14d
	jmp	.L2
.L11:
	movl	$2, %r14d
	jmp	.L2
.L12:
	movb	$4, -96(%rbp)
	movl	$1, %r15d
	jmp	.L2
.L13:
	movb	$3, -96(%rbp)
	jmp	.L2
.L14:
	movl	$1, -148(%rbp)
	jmp	.L2
.L15:
	movb	$2, -96(%rbp)
	jmp	.L2
.L16:
	movb	$1, -96(%rbp)
	movl	$1, %r15d
	jmp	.L2
.L17:
	movb	$5, -88(%rbp)
	jmp	.L2
.L18:
	movb	$1, -88(%rbp)
	jmp	.L2
.L19:
	movb	$2, -88(%rbp)
	jmp	.L2
.L20:
	movb	$4, -88(%rbp)
	jmp	.L2
.L21:
	movb	$3, -88(%rbp)
	jmp	.L2
.L23:
	call	opt_arg@PLT
	movq	%rax, -144(%rbp)
	jmp	.L2
.L24:
	call	opt_arg@PLT
	movq	%rax, -136(%rbp)
	jmp	.L2
.L25:
	call	opt_arg@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	setup_engine@PLT
	movq	%rax, -104(%rbp)
	jmp	.L2
.L26:
	leaq	rsautl_options(%rip), %rdi
	xorl	%r13d, %r13d
	call	opt_help@PLT
	jmp	.L74
.L32:
	movq	-112(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	leaq	-64(%rbp), %rdx
	call	app_passwd@PLT
	testl	%eax, %eax
	je	.L78
	movl	-68(%rbp), %esi
	cmpb	$2, %r14b
	je	.L34
	cmpb	$3, %r14b
	jne	.L79
	movq	-128(%rbp), %rdi
	leaq	.LC5(%rip), %rdx
	call	load_cert@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L39
	movq	%rax, %rdi
	call	X509_get_pubkey@PLT
	movq	%r14, %rdi
	movq	%rax, %r15
	call	X509_free@PLT
.L36:
	testq	%r15, %r15
	je	.L39
	movq	%r15, %rdi
	call	EVP_PKEY_get1_RSA@PLT
	movq	%r15, %rdi
	movq	%rax, %r14
	call	EVP_PKEY_free@PLT
	testq	%r14, %r14
	je	.L80
	movq	-136(%rbp), %rdi
	movl	$2, %edx
	movl	$114, %esi
	call	bio_open_default@PLT
	movq	%rax, %r11
	testq	%rax, %rax
	je	.L56
	movq	-144(%rbp), %rdi
	movl	$2, %edx
	movl	$119, %esi
	movq	%rax, -112(%rbp)
	call	bio_open_default@PLT
	movq	-112(%rbp), %r11
	testq	%rax, %rax
	movq	%rax, %r10
	je	.L57
	movq	%r14, %rdi
	movq	%r11, -128(%rbp)
	movq	%rax, -136(%rbp)
	call	RSA_size@PLT
	leaq	.LC7(%rip), %rsi
	leal	(%rax,%rax), %edx
	movl	%eax, -120(%rbp)
	movl	%edx, %edi
	movl	%edx, -112(%rbp)
	call	app_malloc@PLT
	movl	-120(%rbp), %r8d
	leaq	.LC8(%rip), %rsi
	movq	%rax, %r15
	movl	%r8d, %edi
	call	app_malloc@PLT
	movq	-128(%rbp), %r11
	movl	-112(%rbp), %edx
	movq	%r15, %rsi
	movq	%rax, -120(%rbp)
	movq	%r11, %rdi
	movq	%r11, -112(%rbp)
	call	BIO_read@PLT
	movq	-112(%rbp), %r11
	movq	-120(%rbp), %r9
	testl	%eax, %eax
	movq	-136(%rbp), %r10
	movl	%eax, %edi
	js	.L81
	movl	-148(%rbp), %eax
	testl	%eax, %eax
	je	.L47
	movl	%edi, %ecx
	sarl	%ecx
	je	.L47
	leal	-1(%rdi), %eax
	movslq	%edi, %rsi
	subl	$1, %ecx
	movq	%r15, %rdx
	cltq
	leaq	-2(%r15,%rsi), %r8
	addq	%r15, %rax
	subq	%rcx, %r8
.L48:
	movzbl	(%rdx), %ecx
	movzbl	(%rax), %esi
	subq	$1, %rax
	addq	$1, %rdx
	movb	%sil, -1(%rdx)
	movb	%cl, 1(%rax)
	cmpq	%r8, %rax
	jne	.L48
.L47:
	movzbl	-96(%rbp), %eax
	movzbl	-88(%rbp), %r8d
	cmpb	$3, %al
	je	.L43
	cmpb	$4, %al
	je	.L44
	cmpb	$2, %al
	je	.L82
	movq	%r9, %rdx
	movq	%r14, %rcx
	movq	%r15, %rsi
	movq	%r10, -112(%rbp)
	movq	%r11, -96(%rbp)
	movq	%r9, -88(%rbp)
	call	RSA_private_encrypt@PLT
	movq	-88(%rbp), %r9
	movq	-96(%rbp), %r11
	movq	-112(%rbp), %r10
	movslq	%eax, %rdx
.L49:
	testl	%edx, %edx
	js	.L83
	movq	%r11, -112(%rbp)
	testl	%r12d, %r12d
	jne	.L84
	movq	%r9, -96(%rbp)
	movq	%r9, %rsi
	movq	%r10, %rdi
	movq	%r10, -88(%rbp)
	testl	%ebx, %ebx
	je	.L52
	call	BIO_dump@PLT
	xorl	%r13d, %r13d
	movq	-88(%rbp), %r10
	movq	-96(%rbp), %r9
	movq	-112(%rbp), %r11
	jmp	.L29
.L79:
	movq	-104(%rbp), %r8
	movq	-64(%rbp), %rcx
	leaq	.LC3(%rip), %r9
	xorl	%edx, %edx
	movq	-128(%rbp), %rdi
	call	load_key@PLT
	movq	%rax, %r15
	jmp	.L36
.L34:
	movq	-104(%rbp), %r8
	movq	-128(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	leaq	.LC4(%rip), %r9
	call	load_pubkey@PLT
	movq	%rax, %r15
	jmp	.L36
.L39:
	movl	$1, %r13d
	jmp	.L1
.L78:
	movq	bio_err(%rip), %rdi
	leaq	.LC2(%rip), %rsi
	call	BIO_printf@PLT
	jmp	.L55
.L56:
	xorl	%r9d, %r9d
	xorl	%r15d, %r15d
	xorl	%r10d, %r10d
	movl	$1, %r13d
	jmp	.L29
.L80:
	movq	bio_err(%rip), %rdi
	leaq	.LC6(%rip), %rsi
	xorl	%eax, %eax
	xorl	%r15d, %r15d
	movl	$1, %r13d
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	xorl	%r9d, %r9d
	xorl	%r10d, %r10d
	xorl	%r11d, %r11d
	jmp	.L29
.L57:
	xorl	%r9d, %r9d
	xorl	%r15d, %r15d
	movl	$1, %r13d
	jmp	.L29
.L52:
	call	BIO_write@PLT
	xorl	%r13d, %r13d
	movq	-88(%rbp), %r10
	movq	-96(%rbp), %r9
	movq	-112(%rbp), %r11
	jmp	.L29
.L84:
	movq	%r9, %rsi
	movq	%r10, %rdi
	movl	$-1, %r8d
	movq	%r9, -96(%rbp)
	movl	$1, %ecx
	movq	%r10, -88(%rbp)
	call	ASN1_parse_dump@PLT
	movq	-88(%rbp), %r10
	movq	-96(%rbp), %r9
	testl	%eax, %eax
	movq	-112(%rbp), %r11
	jne	.L29
	movq	bio_err(%rip), %rdi
	movq	%r9, -112(%rbp)
	xorl	%r13d, %r13d
	movq	%r10, -96(%rbp)
	movq	%r11, -88(%rbp)
	call	ERR_print_errors@PLT
	movq	-88(%rbp), %r11
	movq	-96(%rbp), %r10
	movq	-112(%rbp), %r9
	jmp	.L29
.L81:
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
	movq	%r9, -112(%rbp)
	movl	$1, %r13d
	leaq	.LC9(%rip), %rsi
	movq	%r10, -96(%rbp)
	movq	%r11, -88(%rbp)
	call	BIO_printf@PLT
	movq	-88(%rbp), %r11
	movq	-96(%rbp), %r10
	movq	-112(%rbp), %r9
	jmp	.L29
.L82:
	movq	%r9, %rdx
	movq	%r14, %rcx
	movq	%r15, %rsi
	movq	%r10, -112(%rbp)
	movq	%r11, -96(%rbp)
	movq	%r9, -88(%rbp)
	call	RSA_public_decrypt@PLT
	movq	-88(%rbp), %r9
	movq	-96(%rbp), %r11
	movq	-112(%rbp), %r10
	movslq	%eax, %rdx
	jmp	.L49
.L44:
	movq	%r9, %rdx
	movq	%r14, %rcx
	movq	%r15, %rsi
	movq	%r10, -112(%rbp)
	movq	%r11, -96(%rbp)
	movq	%r9, -88(%rbp)
	call	RSA_private_decrypt@PLT
	movq	-112(%rbp), %r10
	movq	-96(%rbp), %r11
	movq	-88(%rbp), %r9
	movslq	%eax, %rdx
	jmp	.L49
.L43:
	movq	%r9, %rdx
	movq	%r14, %rcx
	movq	%r15, %rsi
	movq	%r10, -112(%rbp)
	movq	%r11, -96(%rbp)
	movq	%r9, -88(%rbp)
	call	RSA_public_encrypt@PLT
	movq	-88(%rbp), %r9
	movq	-96(%rbp), %r11
	movq	-112(%rbp), %r10
	movslq	%eax, %rdx
	jmp	.L49
.L83:
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
	movq	%r9, -112(%rbp)
	movl	$1, %r13d
	leaq	.LC10(%rip), %rsi
	movq	%r10, -96(%rbp)
	movq	%r11, -88(%rbp)
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	movq	-88(%rbp), %r11
	movq	-96(%rbp), %r10
	movq	-112(%rbp), %r9
	jmp	.L29
.L77:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1435:
	.size	rsautl_main, .-rsautl_main
	.globl	rsautl_options
	.section	.rodata.str1.1
.LC12:
	.string	"help"
.LC13:
	.string	"Display this summary"
.LC14:
	.string	"in"
.LC15:
	.string	"Input file"
.LC16:
	.string	"out"
.LC17:
	.string	"Output file"
.LC18:
	.string	"inkey"
.LC19:
	.string	"Input key"
.LC20:
	.string	"keyform"
	.section	.rodata.str1.8
	.align 8
.LC21:
	.string	"Private key format - default PEM"
	.section	.rodata.str1.1
.LC22:
	.string	"pubin"
.LC23:
	.string	"Input is an RSA public"
.LC24:
	.string	"certin"
	.section	.rodata.str1.8
	.align 8
.LC25:
	.string	"Input is a cert carrying an RSA public key"
	.section	.rodata.str1.1
.LC26:
	.string	"ssl"
.LC27:
	.string	"Use SSL v2 padding"
.LC28:
	.string	"raw"
.LC29:
	.string	"Use no padding"
.LC30:
	.string	"pkcs"
	.section	.rodata.str1.8
	.align 8
.LC31:
	.string	"Use PKCS#1 v1.5 padding (default)"
	.section	.rodata.str1.1
.LC32:
	.string	"oaep"
.LC33:
	.string	"Use PKCS#1 OAEP"
.LC34:
	.string	"sign"
.LC35:
	.string	"Sign with private key"
.LC36:
	.string	"verify"
.LC37:
	.string	"Verify with public key"
.LC38:
	.string	"asn1parse"
	.section	.rodata.str1.8
	.align 8
.LC39:
	.string	"Run output through asn1parse; useful with -verify"
	.section	.rodata.str1.1
.LC40:
	.string	"hexdump"
.LC41:
	.string	"Hex dump output"
.LC42:
	.string	"x931"
.LC43:
	.string	"Use ANSI X9.31 padding"
.LC44:
	.string	"rev"
	.section	.rodata.str1.8
	.align 8
.LC45:
	.string	"Reverse the order of the input buffer"
	.section	.rodata.str1.1
.LC46:
	.string	"encrypt"
.LC47:
	.string	"Encrypt with public key"
.LC48:
	.string	"decrypt"
.LC49:
	.string	"Decrypt with private key"
.LC50:
	.string	"passin"
.LC51:
	.string	"Input file pass phrase source"
.LC52:
	.string	"rand"
	.section	.rodata.str1.8
	.align 8
.LC53:
	.string	"Load the file(s) into the random number generator"
	.section	.rodata.str1.1
.LC54:
	.string	"writerand"
	.section	.rodata.str1.8
	.align 8
.LC55:
	.string	"Write random data to the specified file"
	.section	.rodata.str1.1
.LC56:
	.string	"engine"
	.section	.rodata.str1.8
	.align 8
.LC57:
	.string	"Use engine, possibly a hardware device"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	rsautl_options, @object
	.size	rsautl_options, 576
rsautl_options:
	.quad	.LC12
	.long	1
	.long	45
	.quad	.LC13
	.quad	.LC14
	.long	3
	.long	60
	.quad	.LC15
	.quad	.LC16
	.long	4
	.long	62
	.quad	.LC17
	.quad	.LC18
	.long	19
	.long	115
	.quad	.LC19
	.quad	.LC20
	.long	21
	.long	69
	.quad	.LC21
	.quad	.LC22
	.long	17
	.long	45
	.quad	.LC23
	.quad	.LC24
	.long	18
	.long	45
	.quad	.LC25
	.quad	.LC26
	.long	9
	.long	45
	.quad	.LC27
	.quad	.LC28
	.long	7
	.long	45
	.quad	.LC29
	.quad	.LC30
	.long	10
	.long	45
	.quad	.LC31
	.quad	.LC32
	.long	8
	.long	45
	.quad	.LC33
	.quad	.LC34
	.long	12
	.long	45
	.quad	.LC35
	.quad	.LC36
	.long	13
	.long	45
	.quad	.LC37
	.quad	.LC38
	.long	5
	.long	45
	.quad	.LC39
	.quad	.LC40
	.long	6
	.long	45
	.quad	.LC41
	.quad	.LC42
	.long	11
	.long	45
	.quad	.LC43
	.quad	.LC44
	.long	14
	.long	45
	.quad	.LC45
	.quad	.LC46
	.long	15
	.long	45
	.quad	.LC47
	.quad	.LC48
	.long	16
	.long	45
	.quad	.LC49
	.quad	.LC50
	.long	20
	.long	115
	.quad	.LC51
	.quad	.LC52
	.long	1501
	.long	115
	.quad	.LC53
	.quad	.LC54
	.long	1502
	.long	62
	.quad	.LC55
	.quad	.LC56
	.long	2
	.long	115
	.quad	.LC57
	.quad	0
	.zero	16
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
