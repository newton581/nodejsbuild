	.file	"opt.c"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"a hexadecimal"
.LC1:
	.string	"0x"
.LC2:
	.string	"0X"
.LC3:
	.string	"0"
.LC4:
	.string	"an octal"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC5:
	.string	"%s: Can't parse \"%s\" as %s number\n"
	.align 8
.LC6:
	.string	"%s: Can't parse \"%s\" as a number\n"
	.text
	.p2align 4
	.type	opt_number_error, @function
opt_number_error:
.LFB1433:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC3(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-112(%rbp), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	xorl	%ebx, %ebx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	.LC1(%rip), %rax
	movq	%rax, %xmm0
	leaq	.LC2(%rip), %rax
	movhps	.LC7(%rip), %xmm0
	movaps	%xmm0, -112(%rbp)
	movq	%rax, %xmm0
	leaq	.LC4(%rip), %rax
	movhps	.LC7(%rip), %xmm0
	movq	%rax, %xmm1
	movaps	%xmm0, -96(%rbp)
	movq	%rcx, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
.L4:
	movq	%rbx, %r13
	salq	$4, %r13
	movq	(%r14,%r13), %r15
	movq	%r15, %rdi
	call	strlen@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	strncmp@PLT
	testl	%eax, %eax
	je	.L11
	addq	$1, %rbx
	cmpq	$3, %rbx
	jne	.L4
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L9
	movq	bio_err(%rip), %rdi
	addq	$72, %rsp
	movq	%r12, %rcx
	xorl	%eax, %eax
	popq	%rbx
	leaq	prog(%rip), %rdx
	leaq	.LC6(%rip), %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	BIO_printf@PLT
	.p2align 4,,10
	.p2align 3
.L11:
	.cfi_restore_state
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L9
	movq	-104(%rbp,%r13), %r8
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
	addq	$72, %rsp
	popq	%rbx
	movq	%r12, %rcx
	popq	%r12
	leaq	prog(%rip), %rdx
	popq	%r13
	leaq	.LC5(%rip), %rsi
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	BIO_printf@PLT
.L9:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1433:
	.size	opt_number_error, .-opt_number_error
	.p2align 4
	.globl	opt_progname
	.type	opt_progname, @function
opt_progname:
.LFB1424:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	strlen@PLT
	leaq	(%rbx,%rax), %rsi
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L15:
	cmpb	$47, (%rsi)
	je	.L16
.L13:
	movq	%rsi, %rax
	subq	$1, %rsi
	cmpq	%rsi, %rbx
	jb	.L15
.L14:
	movl	$39, %edx
	leaq	prog(%rip), %rdi
	call	strncpy@PLT
	movb	$0, 39+prog(%rip)
	addq	$8, %rsp
	leaq	prog(%rip), %rax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	.cfi_restore_state
	movq	%rax, %rsi
	jmp	.L14
	.cfi_endproc
.LFE1424:
	.size	opt_progname, .-opt_progname
	.p2align 4
	.globl	opt_getprog
	.type	opt_getprog, @function
opt_getprog:
.LFB1425:
	.cfi_startproc
	endbr64
	leaq	prog(%rip), %rax
	ret
	.cfi_endproc
.LFE1425:
	.size	opt_getprog, .-opt_getprog
	.p2align 4
	.globl	opt_init
	.type	opt_init, @function
opt_init:
.LFB1426:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, argv(%rip)
	movl	$1, opt_index(%rip)
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rsi), %r12
	movq	%rdx, %rbx
	movq	%rdx, opts(%rip)
	movq	%r12, %rdi
	call	strlen@PLT
	leaq	(%r12,%rax), %rsi
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L22:
	cmpb	$47, (%rsi)
	je	.L26
.L20:
	movq	%rsi, %rax
	subq	$1, %rsi
	cmpq	%rsi, %r12
	jb	.L22
.L21:
	movl	$39, %edx
	leaq	prog(%rip), %rdi
	call	strncpy@PLT
	movb	$0, 39+prog(%rip)
	movq	(%rbx), %rax
	movq	$0, unknown(%rip)
	testq	%rax, %rax
	je	.L23
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	leaq	OPT_HELP_STR(%rip), %rsi
	movl	$1, %r8d
	leaq	OPT_MORE_STR(%rip), %rdi
	.p2align 4,,10
	.p2align 3
.L25:
	cmpq	%rsi, %rax
	je	.L24
	cmpq	%rdi, %rax
	je	.L24
	cmpb	$0, (%rax)
	cmove	%rbx, %rcx
	cmove	%r8d, %edx
.L24:
	movq	24(%rbx), %rax
	addq	$24, %rbx
	testq	%rax, %rax
	jne	.L25
	testb	%dl, %dl
	je	.L23
	movq	%rcx, unknown(%rip)
.L23:
	popq	%rbx
	leaq	prog(%rip), %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	.cfi_restore_state
	movq	%rax, %rsi
	jmp	.L21
	.cfi_endproc
.LFE1426:
	.size	opt_init, .-opt_init
	.section	.rodata.str1.8
	.align 8
.LC8:
	.string	"%s: Bad format \"%s\"; must be pem or der\n"
	.align 8
.LC9:
	.string	"%s: Bad format \"%s\"; must be one of:\n"
	.section	.rodata.str1.1
.LC10:
	.string	"   %s\n"
	.text
	.p2align 4
	.globl	opt_format_error
	.type	opt_format_error, @function
opt_format_error:
.LFB1427:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	bio_err(%rip), %rdi
	cmpq	$2, %rsi
	je	.L49
	leaq	prog(%rip), %rdx
	movq	%rsi, %r12
	xorl	%eax, %eax
	leaq	.LC9(%rip), %rsi
	leaq	formats(%rip), %rbx
	call	BIO_printf@PLT
	movq	formats(%rip), %rdx
	leaq	.LC10(%rip), %r13
	testq	%rdx, %rdx
	je	.L38
	.p2align 4,,10
	.p2align 3
.L39:
	movslq	8(%rbx), %rax
	testq	%r12, %rax
	jne	.L50
	movq	16(%rbx), %rdx
	addq	$16, %rbx
	testq	%rdx, %rdx
	jne	.L39
.L38:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L50:
	.cfi_restore_state
	movq	bio_err(%rip), %rdi
	movq	%r13, %rsi
	xorl	%eax, %eax
	addq	$16, %rbx
	call	BIO_printf@PLT
	movq	(%rbx), %rdx
	testq	%rdx, %rdx
	jne	.L39
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L49:
	.cfi_restore_state
	leaq	prog(%rip), %rdx
	leaq	.LC8(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1427:
	.size	opt_format_error, .-opt_format_error
	.section	.rodata.str1.1
.LC11:
	.string	"NSS"
.LC12:
	.string	"nss"
.LC13:
	.string	"PEM"
.LC14:
	.string	"pem"
.LC15:
	.string	"PVK"
.LC16:
	.string	"pvk"
.LC17:
	.string	"P12"
.LC18:
	.string	"p12"
.LC19:
	.string	"PKCS12"
.LC20:
	.string	"pkcs12"
	.text
	.p2align 4
	.globl	opt_format
	.type	opt_format, @function
opt_format:
.LFB1428:
	.cfi_startproc
	endbr64
	movzbl	(%rdi), %eax
	subl	$49, %eax
	cmpb	$67, %al
	ja	.L211
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.L54(%rip), %rcx
	movzbl	%al, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -40
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L54:
	.long	.L62-.L54
	.long	.L123-.L54
	.long	.L123-.L54
	.long	.L123-.L54
	.long	.L123-.L54
	.long	.L123-.L54
	.long	.L123-.L54
	.long	.L123-.L54
	.long	.L123-.L54
	.long	.L123-.L54
	.long	.L123-.L54
	.long	.L123-.L54
	.long	.L123-.L54
	.long	.L123-.L54
	.long	.L123-.L54
	.long	.L123-.L54
	.long	.L123-.L54
	.long	.L123-.L54
	.long	.L123-.L54
	.long	.L61-.L54
	.long	.L60-.L54
	.long	.L123-.L54
	.long	.L123-.L54
	.long	.L59-.L54
	.long	.L123-.L54
	.long	.L123-.L54
	.long	.L123-.L54
	.long	.L123-.L54
	.long	.L58-.L54
	.long	.L57-.L54
	.long	.L123-.L54
	.long	.L56-.L54
	.long	.L123-.L54
	.long	.L123-.L54
	.long	.L55-.L54
	.long	.L53-.L54
	.long	.L123-.L54
	.long	.L123-.L54
	.long	.L123-.L54
	.long	.L123-.L54
	.long	.L123-.L54
	.long	.L123-.L54
	.long	.L123-.L54
	.long	.L123-.L54
	.long	.L123-.L54
	.long	.L123-.L54
	.long	.L123-.L54
	.long	.L123-.L54
	.long	.L123-.L54
	.long	.L123-.L54
	.long	.L123-.L54
	.long	.L61-.L54
	.long	.L60-.L54
	.long	.L123-.L54
	.long	.L123-.L54
	.long	.L59-.L54
	.long	.L123-.L54
	.long	.L123-.L54
	.long	.L123-.L54
	.long	.L123-.L54
	.long	.L58-.L54
	.long	.L57-.L54
	.long	.L123-.L54
	.long	.L56-.L54
	.long	.L123-.L54
	.long	.L123-.L54
	.long	.L55-.L54
	.long	.L53-.L54
	.text
	.p2align 4,,10
	.p2align 3
.L217:
	movq	bio_err(%rip), %rdi
	movq	%r13, %rcx
	leaq	prog(%rip), %rdx
	cmpq	$2, %rsi
	je	.L215
	leaq	.LC9(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	formats(%rip), %rdx
	testq	%rdx, %rdx
	je	.L123
	leaq	formats(%rip), %r13
	leaq	.LC10(%rip), %rbx
	.p2align 4,,10
	.p2align 3
.L73:
	movslq	8(%r13), %rax
	testq	%r12, %rax
	jne	.L216
	movq	16(%r13), %rdx
	addq	$16, %r13
	testq	%rdx, %rdx
	jne	.L73
	.p2align 4,,10
	.p2align 3
.L123:
	xorl	%eax, %eax
.L51:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L211:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L53:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	testl	$256, %esi
	je	.L217
	movl	$32769, (%rdx)
	movl	$1, %eax
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L61:
	testb	$2, %sil
	je	.L218
	movl	$4, (%rdx)
	movl	$1, %eax
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L60:
	testb	$16, %sil
	je	.L219
	movl	$8, (%rdx)
	movl	$1, %eax
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L59:
	testl	$512, %esi
	je	.L220
	movl	$13, (%rdx)
	movl	$1, %eax
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L58:
	testb	$32, %sil
	je	.L221
	movl	$11, (%rdx)
	movl	$1, %eax
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L57:
	testb	$-128, %sil
	je	.L222
	movl	$4, %ecx
	leaq	.LC11(%rip), %rdi
	movq	%r13, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L79
	movl	$4, %ecx
	leaq	.LC12(%rip), %rdi
	movq	%r13, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L79
	movq	bio_err(%rip), %rdi
	leaq	prog(%rip), %rdx
	movq	%r13, %rcx
	xorl	%eax, %eax
	leaq	.LC9(%rip), %rsi
	call	BIO_printf@PLT
	movq	formats(%rip), %rdx
	testq	%rdx, %rdx
	je	.L123
	leaq	formats(%rip), %r13
	leaq	.LC10(%rip), %rbx
	.p2align 4,,10
	.p2align 3
.L82:
	movslq	8(%r13), %rax
	testq	%r12, %rax
	jne	.L223
	movq	16(%r13), %rdx
	addq	$16, %r13
	testq	%rdx, %rdx
	jne	.L82
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L79:
	movl	$14, (%rdx)
	movl	$1, %eax
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L56:
	cmpb	$0, 1(%rdi)
	je	.L108
	movl	$4, %ecx
	leaq	.LC13(%rip), %rdi
	movq	%r13, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L108
	movl	$4, %ecx
	leaq	.LC14(%rip), %rdi
	movq	%r13, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L108
	movl	$4, %ecx
	leaq	.LC15(%rip), %rdi
	movq	%r13, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L115
	movl	$4, %ecx
	leaq	.LC16(%rip), %rdi
	movq	%r13, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L115
	movl	$4, %ecx
	leaq	.LC17(%rip), %rdi
	movq	%r13, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L122
	movl	$4, %ecx
	leaq	.LC18(%rip), %rdi
	movq	%r13, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L122
	leaq	.LC19(%rip), %rsi
	movq	%r13, %rdi
	movq	%rdx, -40(%rbp)
	call	strcmp@PLT
	movq	-40(%rbp), %rdx
	testl	%eax, %eax
	je	.L122
	leaq	.LC20(%rip), %rsi
	movq	%r13, %rdi
	call	strcmp@PLT
	movq	-40(%rbp), %rdx
	testl	%eax, %eax
	jne	.L123
.L122:
	testb	$4, %r12b
	jne	.L103
	addq	$24, %rsp
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	opt_format_error
	.p2align 4,,10
	.p2align 3
.L55:
	.cfi_restore_state
	testb	$8, %sil
	je	.L224
	movl	$32775, (%rdx)
	movl	$1, %eax
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L108:
	testb	$2, %r12b
	je	.L225
	movl	$32773, (%rdx)
	movl	$1, %eax
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L62:
	testb	$4, %sil
	je	.L226
.L103:
	movl	$6, (%rdx)
	movl	$1, %eax
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L222:
	movq	bio_err(%rip), %rdi
	movq	%r13, %rcx
	leaq	prog(%rip), %rdx
	cmpq	$2, %rsi
	je	.L215
	leaq	.LC9(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	formats(%rip), %rdx
	testq	%rdx, %rdx
	je	.L123
	leaq	formats(%rip), %r13
	leaq	.LC10(%rip), %rbx
	.p2align 4,,10
	.p2align 3
.L78:
	movslq	8(%r13), %rax
	testq	%r12, %rax
	jne	.L227
	movq	16(%r13), %rdx
	addq	$16, %r13
	testq	%rdx, %rdx
	jne	.L78
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L215:
	leaq	.LC8(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	xorl	%eax, %eax
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L227:
	movq	bio_err(%rip), %rdi
	movq	%rbx, %rsi
	xorl	%eax, %eax
	addq	$16, %r13
	call	BIO_printf@PLT
	movq	0(%r13), %rdx
	testq	%rdx, %rdx
	jne	.L78
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L221:
	movq	bio_err(%rip), %rdi
	movq	%r13, %rcx
	leaq	prog(%rip), %rdx
	cmpq	$2, %rsi
	je	.L215
	leaq	.LC9(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	formats(%rip), %rdx
	testq	%rdx, %rdx
	je	.L123
	leaq	formats(%rip), %r13
	leaq	.LC10(%rip), %rbx
	.p2align 4,,10
	.p2align 3
.L92:
	movslq	8(%r13), %rax
	testq	%r12, %rax
	jne	.L228
	movq	16(%r13), %rdx
	addq	$16, %r13
	testq	%rdx, %rdx
	jne	.L92
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L228:
	movq	bio_err(%rip), %rdi
	movq	%rbx, %rsi
	xorl	%eax, %eax
	addq	$16, %r13
	call	BIO_printf@PLT
	movq	0(%r13), %rdx
	testq	%rdx, %rdx
	jne	.L92
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L220:
	movq	bio_err(%rip), %rdi
	movq	%r13, %rcx
	leaq	prog(%rip), %rdx
	cmpq	$2, %rsi
	je	.L215
	leaq	.LC9(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	formats(%rip), %rdx
	testq	%rdx, %rdx
	je	.L123
	leaq	formats(%rip), %r13
	leaq	.LC10(%rip), %rbx
	.p2align 4,,10
	.p2align 3
.L102:
	movslq	8(%r13), %rax
	testq	%r12, %rax
	jne	.L229
	movq	16(%r13), %rdx
	addq	$16, %r13
	testq	%rdx, %rdx
	jne	.L102
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L229:
	movq	bio_err(%rip), %rdi
	movq	%rbx, %rsi
	xorl	%eax, %eax
	addq	$16, %r13
	call	BIO_printf@PLT
	movq	0(%r13), %rdx
	testq	%rdx, %rdx
	jne	.L102
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L219:
	movq	bio_err(%rip), %rdi
	movq	%r13, %rcx
	leaq	prog(%rip), %rdx
	cmpq	$2, %rsi
	je	.L215
	leaq	.LC9(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	formats(%rip), %rdx
	testq	%rdx, %rdx
	je	.L123
	leaq	formats(%rip), %r13
	leaq	.LC10(%rip), %rbx
	.p2align 4,,10
	.p2align 3
.L97:
	movslq	8(%r13), %rax
	testq	%r12, %rax
	jne	.L230
	movq	16(%r13), %rdx
	addq	$16, %r13
	testq	%rdx, %rdx
	jne	.L97
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L230:
	movq	bio_err(%rip), %rdi
	movq	%rbx, %rsi
	xorl	%eax, %eax
	addq	$16, %r13
	call	BIO_printf@PLT
	movq	0(%r13), %rdx
	testq	%rdx, %rdx
	jne	.L97
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L218:
	movq	bio_err(%rip), %rdi
	movq	%r13, %rcx
	leaq	prog(%rip), %rdx
	cmpq	$2, %rsi
	je	.L215
	leaq	.LC9(%rip), %rsi
	xorl	%eax, %eax
	leaq	formats(%rip), %r13
	call	BIO_printf@PLT
	movq	formats(%rip), %rdx
	leaq	.LC10(%rip), %rbx
	testq	%rdx, %rdx
	je	.L123
	.p2align 4,,10
	.p2align 3
.L65:
	movslq	8(%r13), %rax
	testq	%r12, %rax
	jne	.L231
	movq	16(%r13), %rdx
	addq	$16, %r13
	testq	%rdx, %rdx
	jne	.L65
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L231:
	movq	bio_err(%rip), %rdi
	movq	%rbx, %rsi
	xorl	%eax, %eax
	addq	$16, %r13
	call	BIO_printf@PLT
	movq	0(%r13), %rdx
	testq	%rdx, %rdx
	jne	.L65
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L216:
	movq	bio_err(%rip), %rdi
	movq	%rbx, %rsi
	xorl	%eax, %eax
	addq	$16, %r13
	call	BIO_printf@PLT
	movq	0(%r13), %rdx
	testq	%rdx, %rdx
	jne	.L73
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L226:
	movq	bio_err(%rip), %rdi
	movq	%r13, %rcx
	leaq	prog(%rip), %rdx
	cmpq	$2, %rsi
	je	.L215
	leaq	.LC9(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	formats(%rip), %rdx
	testq	%rdx, %rdx
	je	.L123
	leaq	formats(%rip), %r13
	leaq	.LC10(%rip), %rbx
	.p2align 4,,10
	.p2align 3
.L107:
	movslq	8(%r13), %rax
	testq	%r12, %rax
	jne	.L232
	movq	16(%r13), %rdx
	addq	$16, %r13
	testq	%rdx, %rdx
	jne	.L107
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L232:
	movq	bio_err(%rip), %rdi
	movq	%rbx, %rsi
	xorl	%eax, %eax
	addq	$16, %r13
	call	BIO_printf@PLT
	movq	0(%r13), %rdx
	testq	%rdx, %rdx
	jne	.L107
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L224:
	movq	bio_err(%rip), %rdi
	movq	%r13, %rcx
	leaq	prog(%rip), %rdx
	cmpq	$2, %rsi
	je	.L215
	leaq	.LC9(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	formats(%rip), %rdx
	testq	%rdx, %rdx
	je	.L123
	leaq	formats(%rip), %r13
	leaq	.LC10(%rip), %rbx
	.p2align 4,,10
	.p2align 3
.L87:
	movslq	8(%r13), %rax
	testq	%r12, %rax
	jne	.L233
	movq	16(%r13), %rdx
	addq	$16, %r13
	testq	%rdx, %rdx
	jne	.L87
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L233:
	movq	bio_err(%rip), %rdi
	movq	%rbx, %rsi
	xorl	%eax, %eax
	addq	$16, %r13
	call	BIO_printf@PLT
	movq	0(%r13), %rdx
	testq	%rdx, %rdx
	jne	.L87
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L225:
	movq	bio_err(%rip), %rdi
	movq	%r13, %rcx
	leaq	prog(%rip), %rdx
	cmpq	$2, %r12
	je	.L215
	leaq	.LC9(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	formats(%rip), %rdx
	testq	%rdx, %rdx
	je	.L123
	leaq	formats(%rip), %r13
	leaq	.LC10(%rip), %rbx
	.p2align 4,,10
	.p2align 3
.L114:
	movslq	8(%r13), %rax
	testq	%r12, %rax
	jne	.L234
	movq	16(%r13), %rdx
	addq	$16, %r13
	testq	%rdx, %rdx
	jne	.L114
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L234:
	movq	bio_err(%rip), %rdi
	movq	%rbx, %rsi
	xorl	%eax, %eax
	addq	$16, %r13
	call	BIO_printf@PLT
	movq	0(%r13), %rdx
	testq	%rdx, %rdx
	jne	.L114
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L223:
	movq	bio_err(%rip), %rdi
	movq	%rbx, %rsi
	xorl	%eax, %eax
	addq	$16, %r13
	call	BIO_printf@PLT
	movq	0(%r13), %rdx
	testq	%rdx, %rdx
	jne	.L82
	jmp	.L123
.L115:
	testl	$1024, %r12d
	je	.L235
	movl	$12, (%rdx)
	movl	$1, %eax
	jmp	.L51
.L235:
	movq	bio_err(%rip), %rdi
	movq	%r13, %rcx
	leaq	prog(%rip), %rdx
	cmpq	$2, %r12
	je	.L215
	leaq	.LC9(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	formats(%rip), %rdx
	testq	%rdx, %rdx
	je	.L123
	leaq	formats(%rip), %rbx
	leaq	.LC10(%rip), %r13
	.p2align 4,,10
	.p2align 3
.L121:
	movslq	8(%rbx), %rax
	testq	%r12, %rax
	jne	.L236
	movq	16(%rbx), %rdx
	addq	$16, %rbx
	testq	%rdx, %rdx
	jne	.L121
	jmp	.L123
.L236:
	movq	bio_err(%rip), %rdi
	movq	%r13, %rsi
	xorl	%eax, %eax
	addq	$16, %rbx
	call	BIO_printf@PLT
	movq	(%rbx), %rdx
	testq	%rdx, %rdx
	jne	.L121
	jmp	.L123
	.cfi_endproc
.LFE1428:
	.size	opt_format, .-opt_format
	.section	.rodata.str1.1
.LC21:
	.string	"%s: Unrecognized flag %s\n"
	.text
	.p2align 4
	.globl	opt_cipher
	.type	opt_cipher, @function
opt_cipher:
.LFB1429:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	call	EVP_get_cipherbyname@PLT
	movl	$1, %r8d
	movq	%rax, (%rbx)
	testq	%rax, %rax
	je	.L242
	popq	%rbx
	movl	%r8d, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L242:
	.cfi_restore_state
	movq	bio_err(%rip), %rdi
	movq	%r12, %rcx
	leaq	prog(%rip), %rdx
	leaq	.LC21(%rip), %rsi
	call	BIO_printf@PLT
	xorl	%r8d, %r8d
	popq	%rbx
	popq	%r12
	movl	%r8d, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1429:
	.size	opt_cipher, .-opt_cipher
	.p2align 4
	.globl	opt_md
	.type	opt_md, @function
opt_md:
.LFB1430:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	call	EVP_get_digestbyname@PLT
	movl	$1, %r8d
	movq	%rax, (%rbx)
	testq	%rax, %rax
	je	.L248
	popq	%rbx
	movl	%r8d, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L248:
	.cfi_restore_state
	movq	bio_err(%rip), %rdi
	movq	%r12, %rcx
	leaq	prog(%rip), %rdx
	leaq	.LC21(%rip), %rsi
	call	BIO_printf@PLT
	xorl	%r8d, %r8d
	popq	%rbx
	popq	%r12
	movl	%r8d, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1430:
	.size	opt_md, .-opt_md
	.section	.rodata.str1.1
.LC22:
	.string	"%s: Value must be one of:\n"
.LC23:
	.string	"\t%s\n"
	.text
	.p2align 4
	.globl	opt_pair
	.type	opt_pair, @function
opt_pair:
.LFB1431:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	(%rsi), %rdi
	testq	%rdi, %rdi
	je	.L250
	movq	%rdx, %r14
	movq	%rsi, %rbx
	jmp	.L253
	.p2align 4,,10
	.p2align 3
.L251:
	movq	16(%rbx), %rdi
	addq	$16, %rbx
	testq	%rdi, %rdi
	je	.L250
.L253:
	movq	%r13, %rsi
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L251
	movl	8(%rbx), %eax
	movl	%eax, (%r14)
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L250:
	.cfi_restore_state
	movq	bio_err(%rip), %rdi
	leaq	prog(%rip), %rdx
	leaq	.LC22(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	(%r12), %rdx
	testq	%rdx, %rdx
	je	.L254
	leaq	.LC23(%rip), %rbx
	.p2align 4,,10
	.p2align 3
.L255:
	movq	bio_err(%rip), %rdi
	movq	%rbx, %rsi
	xorl	%eax, %eax
	addq	$16, %r12
	call	BIO_printf@PLT
	movq	(%r12), %rdx
	testq	%rdx, %rdx
	jne	.L255
.L254:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1431:
	.size	opt_pair, .-opt_pair
	.section	.rodata.str1.8
	.align 8
.LC24:
	.string	"%s: Value \"%s\" outside integer range\n"
	.text
	.p2align 4
	.globl	opt_int
	.type	opt_int, @function
opt_int:
.LFB1432:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	__errno_location@PLT
	xorl	%edx, %edx
	leaq	-48(%rbp), %rsi
	movq	%r12, %rdi
	movl	(%rax), %r14d
	movl	$0, (%rax)
	movq	%rax, %rbx
	call	strtol@PLT
	movq	-48(%rbp), %rdx
	cmpb	$0, (%rdx)
	jne	.L266
	cmpq	%rdx, %r12
	je	.L266
	movabsq	$9223372036854775807, %rdx
	addq	%rax, %rdx
	cmpq	$-3, %rdx
	jbe	.L267
	cmpl	$34, (%rbx)
	je	.L266
.L268:
	movslq	%eax, %rdx
	movl	%r14d, (%rbx)
	movl	$1, %r8d
	movl	%eax, 0(%r13)
	cmpq	%rax, %rdx
	je	.L265
	movq	bio_err(%rip), %rdi
	movq	%r12, %rcx
	leaq	prog(%rip), %rdx
	xorl	%eax, %eax
	leaq	.LC24(%rip), %rsi
	call	BIO_printf@PLT
	xorl	%r8d, %r8d
	jmp	.L265
	.p2align 4,,10
	.p2align 3
.L267:
	testq	%rax, %rax
	jne	.L268
	movl	(%rbx), %eax
	testl	%eax, %eax
	je	.L269
	.p2align 4,,10
	.p2align 3
.L266:
	movq	%r12, %rdi
	call	opt_number_error
	movl	%r14d, (%rbx)
	xorl	%r8d, %r8d
.L265:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L274
	addq	$16, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L269:
	.cfi_restore_state
	movl	%r14d, (%rbx)
	movl	$1, %r8d
	movl	$0, 0(%r13)
	jmp	.L265
.L274:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1432:
	.size	opt_int, .-opt_int
	.p2align 4
	.globl	opt_long
	.type	opt_long, @function
opt_long:
.LFB1434:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	__errno_location@PLT
	xorl	%edx, %edx
	leaq	-48(%rbp), %rsi
	movq	%r12, %rdi
	movl	(%rax), %r14d
	movl	$0, (%rax)
	movq	%rax, %rbx
	call	strtol@PLT
	movq	-48(%rbp), %rdx
	cmpb	$0, (%rdx)
	jne	.L276
	cmpq	%r12, %rdx
	je	.L276
	movabsq	$9223372036854775807, %rdx
	addq	%rax, %rdx
	cmpq	$-3, %rdx
	jbe	.L277
	cmpl	$34, (%rbx)
	je	.L276
.L278:
	movq	%rax, 0(%r13)
	movl	$1, %eax
	movl	%r14d, (%rbx)
	jmp	.L275
	.p2align 4,,10
	.p2align 3
.L277:
	testq	%rax, %rax
	jne	.L278
	movl	(%rbx), %edx
	testl	%edx, %edx
	je	.L278
	.p2align 4,,10
	.p2align 3
.L276:
	movq	%r12, %rdi
	call	opt_number_error
	movl	%r14d, (%rbx)
	xorl	%eax, %eax
.L275:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L282
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L282:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1434:
	.size	opt_long, .-opt_long
	.p2align 4
	.globl	opt_imax
	.type	opt_imax, @function
opt_imax:
.LFB1435:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	__errno_location@PLT
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	leaq	-48(%rbp), %rsi
	movl	(%rax), %r14d
	movl	$0, (%rax)
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	__strtol_internal@PLT
	movq	-48(%rbp), %rdx
	cmpb	$0, (%rdx)
	jne	.L284
	cmpq	%r12, %rdx
	je	.L284
	movabsq	$9223372036854775807, %rdx
	addq	%rax, %rdx
	cmpq	$-3, %rdx
	jbe	.L285
	cmpl	$34, (%rbx)
	je	.L284
.L286:
	movq	%rax, 0(%r13)
	movl	$1, %eax
	movl	%r14d, (%rbx)
	jmp	.L283
	.p2align 4,,10
	.p2align 3
.L285:
	testq	%rax, %rax
	jne	.L286
	movl	(%rbx), %edx
	testl	%edx, %edx
	je	.L286
	.p2align 4,,10
	.p2align 3
.L284:
	movq	%r12, %rdi
	call	opt_number_error
	movl	%r14d, (%rbx)
	xorl	%eax, %eax
.L283:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L290
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L290:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1435:
	.size	opt_imax, .-opt_imax
	.p2align 4
	.globl	opt_umax
	.type	opt_umax, @function
opt_umax:
.LFB1436:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	__errno_location@PLT
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	leaq	-48(%rbp), %rsi
	movl	(%rax), %r14d
	movl	$0, (%rax)
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	__strtoul_internal@PLT
	movq	-48(%rbp), %rdx
	cmpb	$0, (%rdx)
	jne	.L292
	cmpq	%r12, %rdx
	je	.L292
	cmpq	$-1, %rax
	je	.L298
	testq	%rax, %rax
	jne	.L294
	movl	(%rbx), %edx
	testl	%edx, %edx
	jne	.L292
.L294:
	movq	%rax, 0(%r13)
	movl	$1, %eax
	movl	%r14d, (%rbx)
.L291:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L299
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L298:
	.cfi_restore_state
	cmpl	$34, (%rbx)
	jne	.L294
	.p2align 4,,10
	.p2align 3
.L292:
	movq	%r12, %rdi
	call	opt_number_error
	movl	%r14d, (%rbx)
	xorl	%eax, %eax
	jmp	.L291
.L299:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1436:
	.size	opt_umax, .-opt_umax
	.p2align 4
	.globl	opt_ulong
	.type	opt_ulong, @function
opt_ulong:
.LFB1437:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	__errno_location@PLT
	xorl	%edx, %edx
	leaq	-48(%rbp), %rsi
	movq	%r12, %rdi
	movl	(%rax), %r14d
	movl	$0, (%rax)
	movq	%rax, %rbx
	call	strtoul@PLT
	movq	-48(%rbp), %rdx
	cmpb	$0, (%rdx)
	jne	.L301
	cmpq	%r12, %rdx
	je	.L301
	cmpq	$-1, %rax
	je	.L307
	testq	%rax, %rax
	jne	.L303
	movl	(%rbx), %edx
	testl	%edx, %edx
	jne	.L301
.L303:
	movq	%rax, 0(%r13)
	movl	$1, %eax
	movl	%r14d, (%rbx)
.L300:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L308
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L307:
	.cfi_restore_state
	cmpl	$34, (%rbx)
	jne	.L303
	.p2align 4,,10
	.p2align 3
.L301:
	movq	%r12, %rdi
	call	opt_number_error
	movl	%r14d, (%rbx)
	xorl	%eax, %eax
	jmp	.L300
.L308:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1437:
	.size	opt_ulong, .-opt_ulong
	.section	.rodata.str1.1
.LC25:
	.string	"%s: Invalid Policy %s\n"
.LC26:
	.string	"%s: Invalid purpose %s\n"
	.section	.rodata.str1.8
	.align 8
.LC27:
	.string	"%s: Internal error setting purpose %s\n"
	.section	.rodata.str1.1
.LC28:
	.string	"%s: Invalid verify name %s\n"
	.text
	.p2align 4
	.globl	opt_verify
	.type	opt_verify, @function
opt_verify:
.LFB1438:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	subl	$2000, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpl	$31, %edi
	ja	.L353
	leaq	.L312(%rip), %rdx
	movq	%rsi, %r12
	movslq	(%rdx,%rdi,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L312:
	.long	.L351-.L312
	.long	.L341-.L312
	.long	.L340-.L312
	.long	.L339-.L312
	.long	.L338-.L312
	.long	.L337-.L312
	.long	.L336-.L312
	.long	.L335-.L312
	.long	.L334-.L312
	.long	.L333-.L312
	.long	.L353-.L312
	.long	.L332-.L312
	.long	.L331-.L312
	.long	.L330-.L312
	.long	.L329-.L312
	.long	.L328-.L312
	.long	.L327-.L312
	.long	.L326-.L312
	.long	.L325-.L312
	.long	.L324-.L312
	.long	.L323-.L312
	.long	.L322-.L312
	.long	.L321-.L312
	.long	.L320-.L312
	.long	.L319-.L312
	.long	.L318-.L312
	.long	.L317-.L312
	.long	.L316-.L312
	.long	.L315-.L312
	.long	.L314-.L312
	.long	.L313-.L312
	.long	.L351-.L312
	.text
	.p2align 4,,10
	.p2align 3
.L351:
	xorl	%r12d, %r12d
	.p2align 4,,10
	.p2align 3
.L309:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L354
	addq	$16, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L315:
	.cfi_restore_state
	movl	$2097152, %esi
	movq	%r12, %rdi
	call	X509_VERIFY_PARAM_set_flags@PLT
.L353:
	movl	$1, %r12d
	jmp	.L309
	.p2align 4,,10
	.p2align 3
.L341:
	movq	arg(%rip), %rdi
	xorl	%esi, %esi
	call	OBJ_txt2obj@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L355
	movq	%r12, %rdi
	movl	$1, %r12d
	call	X509_VERIFY_PARAM_add0_policy@PLT
	jmp	.L309
	.p2align 4,,10
	.p2align 3
.L313:
	movq	%r12, %rdi
	movl	$64, %esi
	movl	$1, %r12d
	call	X509_VERIFY_PARAM_set_flags@PLT
	jmp	.L309
	.p2align 4,,10
	.p2align 3
.L316:
	movq	%r12, %rdi
	movl	$1048576, %esi
	movl	$1, %r12d
	call	X509_VERIFY_PARAM_set_flags@PLT
	jmp	.L309
	.p2align 4,,10
	.p2align 3
.L314:
	movq	arg(%rip), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	call	strtol@PLT
	testl	%eax, %eax
	js	.L353
	movq	%r12, %rdi
	movl	%eax, %esi
	movl	$1, %r12d
	call	X509_VERIFY_PARAM_set_auth_level@PLT
	jmp	.L309
	.p2align 4,,10
	.p2align 3
.L317:
	movq	%r12, %rdi
	movl	$524288, %esi
	movl	$1, %r12d
	call	X509_VERIFY_PARAM_set_flags@PLT
	jmp	.L309
	.p2align 4,,10
	.p2align 3
.L318:
	movq	%r12, %rdi
	movl	$131072, %esi
	movl	$1, %r12d
	call	X509_VERIFY_PARAM_set_flags@PLT
	jmp	.L309
	.p2align 4,,10
	.p2align 3
.L319:
	movq	%r12, %rdi
	movl	$196608, %esi
	movl	$1, %r12d
	call	X509_VERIFY_PARAM_set_flags@PLT
	jmp	.L309
	.p2align 4,,10
	.p2align 3
.L320:
	movq	%r12, %rdi
	movl	$65536, %esi
	movl	$1, %r12d
	call	X509_VERIFY_PARAM_set_flags@PLT
	jmp	.L309
	.p2align 4,,10
	.p2align 3
.L321:
	movq	%r12, %rdi
	movl	$32768, %esi
	movl	$1, %r12d
	call	X509_VERIFY_PARAM_set_flags@PLT
	jmp	.L309
	.p2align 4,,10
	.p2align 3
.L322:
	movq	%r12, %rdi
	movl	$16384, %esi
	movl	$1, %r12d
	call	X509_VERIFY_PARAM_set_flags@PLT
	jmp	.L309
	.p2align 4,,10
	.p2align 3
.L323:
	movq	%r12, %rdi
	movl	$2048, %esi
	movl	$1, %r12d
	call	X509_VERIFY_PARAM_set_flags@PLT
	jmp	.L309
	.p2align 4,,10
	.p2align 3
.L324:
	movq	%r12, %rdi
	movl	$8192, %esi
	movl	$1, %r12d
	call	X509_VERIFY_PARAM_set_flags@PLT
	jmp	.L309
	.p2align 4,,10
	.p2align 3
.L325:
	movq	%r12, %rdi
	movl	$4096, %esi
	movl	$1, %r12d
	call	X509_VERIFY_PARAM_set_flags@PLT
	jmp	.L309
	.p2align 4,,10
	.p2align 3
.L326:
	movq	%r12, %rdi
	movl	$32, %esi
	movl	$1, %r12d
	call	X509_VERIFY_PARAM_set_flags@PLT
	jmp	.L309
	.p2align 4,,10
	.p2align 3
.L327:
	movq	%r12, %rdi
	movl	$1024, %esi
	movl	$1, %r12d
	call	X509_VERIFY_PARAM_set_flags@PLT
	jmp	.L309
	.p2align 4,,10
	.p2align 3
.L328:
	movq	%r12, %rdi
	movl	$512, %esi
	movl	$1, %r12d
	call	X509_VERIFY_PARAM_set_flags@PLT
	jmp	.L309
	.p2align 4,,10
	.p2align 3
.L329:
	movq	%r12, %rdi
	movl	$256, %esi
	movl	$1, %r12d
	call	X509_VERIFY_PARAM_set_flags@PLT
	jmp	.L309
	.p2align 4,,10
	.p2align 3
.L330:
	movq	%r12, %rdi
	movl	$128, %esi
	movl	$1, %r12d
	call	X509_VERIFY_PARAM_set_flags@PLT
	jmp	.L309
	.p2align 4,,10
	.p2align 3
.L331:
	movq	%r12, %rdi
	movl	$12, %esi
	movl	$1, %r12d
	call	X509_VERIFY_PARAM_set_flags@PLT
	jmp	.L309
	.p2align 4,,10
	.p2align 3
.L332:
	movq	%r12, %rdi
	movl	$4, %esi
	movl	$1, %r12d
	call	X509_VERIFY_PARAM_set_flags@PLT
	jmp	.L309
	.p2align 4,,10
	.p2align 3
.L333:
	movq	%r12, %rdi
	movl	$16, %esi
	movl	$1, %r12d
	call	X509_VERIFY_PARAM_set_flags@PLT
	jmp	.L309
	.p2align 4,,10
	.p2align 3
.L334:
	movq	arg(%rip), %rsi
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	X509_VERIFY_PARAM_set1_ip_asc@PLT
	testl	%eax, %eax
	setne	%r12b
	jmp	.L309
	.p2align 4,,10
	.p2align 3
.L335:
	movq	arg(%rip), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	X509_VERIFY_PARAM_set1_email@PLT
	testl	%eax, %eax
	setne	%r12b
	jmp	.L309
	.p2align 4,,10
	.p2align 3
.L336:
	movq	arg(%rip), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	X509_VERIFY_PARAM_set1_host@PLT
	testl	%eax, %eax
	setne	%r12b
	jmp	.L309
	.p2align 4,,10
	.p2align 3
.L337:
	call	__errno_location@PLT
	movq	arg(%rip), %r14
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	(%rax), %r13d
	movl	$0, (%rax)
	leaq	-48(%rbp), %rsi
	movq	%rax, %rbx
	movq	%r14, %rdi
	call	__strtol_internal@PLT
	movq	%rax, %rsi
	movq	-48(%rbp), %rax
	cmpb	$0, (%rax)
	jne	.L347
	cmpq	%rax, %r14
	je	.L347
	movabsq	$9223372036854775807, %rax
	addq	%rsi, %rax
	cmpq	$-3, %rax
	jbe	.L348
	cmpl	$34, (%rbx)
	je	.L347
.L349:
	movl	%r13d, (%rbx)
	movq	%r12, %rdi
	movl	$1, %r12d
	call	X509_VERIFY_PARAM_set_time@PLT
	jmp	.L309
	.p2align 4,,10
	.p2align 3
.L338:
	movq	arg(%rip), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	call	strtol@PLT
	testl	%eax, %eax
	js	.L353
	movq	%r12, %rdi
	movl	%eax, %esi
	movl	$1, %r12d
	call	X509_VERIFY_PARAM_set_depth@PLT
	jmp	.L309
	.p2align 4,,10
	.p2align 3
.L339:
	movq	arg(%rip), %rdi
	call	X509_VERIFY_PARAM_lookup@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L356
	movq	%r12, %rdi
	movl	$1, %r12d
	call	X509_VERIFY_PARAM_set1@PLT
	jmp	.L309
	.p2align 4,,10
	.p2align 3
.L340:
	movq	arg(%rip), %rdi
	call	X509_PURPOSE_get_by_sname@PLT
	movl	%eax, %edi
	testl	%eax, %eax
	js	.L357
	call	X509_PURPOSE_get0@PLT
	movq	%rax, %rdi
	call	X509_PURPOSE_get_id@PLT
	movq	%r12, %rdi
	movl	%eax, %esi
	call	X509_VERIFY_PARAM_set_purpose@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	jne	.L353
	movq	arg(%rip), %rcx
	movq	bio_err(%rip), %rdi
	leaq	prog(%rip), %rdx
	xorl	%eax, %eax
	leaq	.LC27(%rip), %rsi
	call	BIO_printf@PLT
	jmp	.L309
	.p2align 4,,10
	.p2align 3
.L348:
	testq	%rsi, %rsi
	jne	.L349
	movl	(%rbx), %eax
	testl	%eax, %eax
	je	.L349
	.p2align 4,,10
	.p2align 3
.L347:
	movq	%r14, %rdi
	xorl	%r12d, %r12d
	call	opt_number_error
	movl	%r13d, (%rbx)
	jmp	.L309
.L357:
	movq	arg(%rip), %rcx
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
	xorl	%r12d, %r12d
	leaq	prog(%rip), %rdx
	leaq	.LC26(%rip), %rsi
	call	BIO_printf@PLT
	jmp	.L309
.L356:
	movq	arg(%rip), %rcx
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
	xorl	%r12d, %r12d
	leaq	prog(%rip), %rdx
	leaq	.LC28(%rip), %rsi
	call	BIO_printf@PLT
	jmp	.L309
.L355:
	movq	arg(%rip), %rcx
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
	xorl	%r12d, %r12d
	leaq	prog(%rip), %rdx
	leaq	.LC25(%rip), %rsi
	call	BIO_printf@PLT
	jmp	.L309
.L354:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1438:
	.size	opt_verify, .-opt_verify
	.section	.rodata.str1.8
	.align 8
.LC29:
	.string	"%s: Option -%s does not take a value\n"
	.section	.rodata.str1.1
.LC30:
	.string	"%s: Option -%s needs a value\n"
.LC31:
	.string	"%s: Not a directory: %s\n"
	.section	.rodata.str1.8
	.align 8
.LC32:
	.string	"%s: Non-positive number \"%s\" for -%s\n"
	.align 8
.LC33:
	.string	"%s: Invalid number \"%s\" for -%s\n"
	.align 8
.LC34:
	.string	"%s: Invalid format \"%s\" for -%s\n"
	.align 8
.LC35:
	.string	"%s: Option unknown option -%s\n"
	.text
	.p2align 4
	.globl	opt_next
	.type	opt_next, @function
opt_next:
.LFB1439:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	argv(%rip), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movslq	opt_index(%rip), %rax
	movq	$0, arg(%rip)
	leaq	0(,%rax,8), %rcx
	movq	%rax, %rbx
	movq	(%r12,%rax,8), %rax
	movq	%rcx, -72(%rbp)
	testq	%rax, %rax
	je	.L361
	cmpb	$45, (%rax)
	je	.L437
.L361:
	xorl	%eax, %eax
.L358:
	movq	-56(%rbp), %rdi
	xorq	%fs:40, %rdi
	jne	.L438
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L437:
	.cfi_restore_state
	leal	1(%rbx), %edx
	cmpb	$45, 1(%rax)
	movl	%edx, opt_index(%rip)
	jne	.L406
	cmpb	$0, 2(%rax)
	je	.L361
	.p2align 4,,10
	.p2align 3
.L406:
	cmpb	$45, 1(%rax)
	leaq	1(%rax), %r13
	je	.L439
.L364:
	movl	$61, %esi
	movq	%r13, %rdi
	movq	%rax, flag(%rip)
	call	strchr@PLT
	testq	%rax, %rax
	je	.L440
	leaq	1(%rax), %r8
	movb	$0, (%rax)
	movq	%r8, arg(%rip)
.L366:
	movq	opts(%rip), %r14
	movq	(%r14), %r15
	testq	%r15, %r15
	je	.L367
.L369:
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%r8, -80(%rbp)
	call	strcmp@PLT
	movq	-80(%rbp), %r8
	testl	%eax, %eax
	jne	.L441
	movl	12(%r14), %eax
	testl	%eax, %eax
	je	.L405
	cmpl	$45, %eax
	je	.L405
	testq	%r8, %r8
	je	.L442
.L373:
	leal	-47(%rax), %edx
	cmpl	$70, %edx
	ja	.L375
	leaq	.L377(%rip), %rcx
	movslq	(%rcx,%rdx,4), %rdx
	addq	%rcx, %rdx
	notrack jmp	*%rdx
	.section	.rodata
	.align 4
	.align 4
.L377:
	.long	.L383-.L377
	.long	.L375-.L377
	.long	.L375-.L377
	.long	.L375-.L377
	.long	.L375-.L377
	.long	.L375-.L377
	.long	.L375-.L377
	.long	.L375-.L377
	.long	.L375-.L377
	.long	.L375-.L377
	.long	.L375-.L377
	.long	.L375-.L377
	.long	.L375-.L377
	.long	.L375-.L377
	.long	.L375-.L377
	.long	.L375-.L377
	.long	.L375-.L377
	.long	.L375-.L377
	.long	.L375-.L377
	.long	.L375-.L377
	.long	.L375-.L377
	.long	.L375-.L377
	.long	.L380-.L377
	.long	.L380-.L377
	.long	.L375-.L377
	.long	.L375-.L377
	.long	.L375-.L377
	.long	.L375-.L377
	.long	.L375-.L377
	.long	.L375-.L377
	.long	.L382-.L377
	.long	.L375-.L377
	.long	.L375-.L377
	.long	.L375-.L377
	.long	.L375-.L377
	.long	.L375-.L377
	.long	.L375-.L377
	.long	.L375-.L377
	.long	.L381-.L377
	.long	.L375-.L377
	.long	.L375-.L377
	.long	.L375-.L377
	.long	.L375-.L377
	.long	.L375-.L377
	.long	.L375-.L377
	.long	.L375-.L377
	.long	.L375-.L377
	.long	.L375-.L377
	.long	.L375-.L377
	.long	.L375-.L377
	.long	.L375-.L377
	.long	.L375-.L377
	.long	.L380-.L377
	.long	.L375-.L377
	.long	.L375-.L377
	.long	.L380-.L377
	.long	.L375-.L377
	.long	.L375-.L377
	.long	.L375-.L377
	.long	.L375-.L377
	.long	.L375-.L377
	.long	.L379-.L377
	.long	.L375-.L377
	.long	.L378-.L377
	.long	.L375-.L377
	.long	.L378-.L377
	.long	.L375-.L377
	.long	.L375-.L377
	.long	.L375-.L377
	.long	.L375-.L377
	.long	.L376-.L377
	.text
	.p2align 4,,10
	.p2align 3
.L439:
	leaq	2(%rax), %r13
	addq	$1, %rax
	jmp	.L364
	.p2align 4,,10
	.p2align 3
.L440:
	movq	$0, arg(%rip)
	xorl	%r8d, %r8d
	jmp	.L366
.L376:
	movq	%r8, -72(%rbp)
	call	__errno_location@PLT
	movq	-72(%rbp), %r8
	xorl	%edx, %edx
	leaq	-64(%rbp), %rsi
	movl	(%rax), %r12d
	movl	$0, (%rax)
	movq	%rax, %rbx
	movq	%r8, %rdi
	call	strtoul@PLT
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	cmpb	$0, (%rdx)
	jne	.L396
.L435:
	cmpq	%r8, %rdx
	je	.L396
	cmpq	$-1, %rax
	je	.L423
.L397:
	testq	%rax, %rax
	jne	.L398
	movl	(%rbx), %eax
	testl	%eax, %eax
	jne	.L396
.L398:
	movl	%r12d, (%rbx)
	.p2align 4,,10
	.p2align 3
.L375:
	movl	8(%r14), %eax
	jmp	.L358
	.p2align 4,,10
	.p2align 3
.L405:
	testq	%r8, %r8
	je	.L375
	movq	bio_err(%rip), %rdi
	movq	%r13, %rcx
	leaq	prog(%rip), %rdx
	xorl	%eax, %eax
	leaq	.LC29(%rip), %rsi
	call	BIO_printf@PLT
	movl	$-1, %eax
	jmp	.L358
	.p2align 4,,10
	.p2align 3
.L441:
	movq	24(%r14), %r15
	addq	$24, %r14
	testq	%r15, %r15
	jne	.L369
.L367:
	movq	unknown(%rip), %rax
	testq	%rax, %rax
	je	.L400
	movq	%r13, dunno(%rip)
	movl	8(%rax), %eax
	jmp	.L358
.L380:
	movl	$10, %esi
	cmpl	$99, %eax
	je	.L399
	movl	$18, %esi
	cmpl	$69, %eax
	je	.L399
	cmpl	$70, %eax
	movl	$2, %esi
	movl	$1982, %eax
	cmovne	%rax, %rsi
.L399:
	leaq	-64(%rbp), %rdx
	movq	%r8, %rdi
	call	opt_format
	testl	%eax, %eax
	jne	.L375
	movq	(%r14), %r8
	movq	arg(%rip), %rcx
	leaq	prog(%rip), %rdx
	leaq	.LC34(%rip), %rsi
	movq	bio_err(%rip), %rdi
	call	BIO_printf@PLT
	orl	$-1, %eax
	jmp	.L358
	.p2align 4,,10
	.p2align 3
.L378:
	leaq	-64(%rbp), %rsi
	movq	%r8, %rdi
	call	opt_int
	testl	%eax, %eax
	je	.L385
	cmpl	$112, 12(%r14)
	jne	.L375
	movl	-64(%rbp), %edx
	testl	%edx, %edx
	jg	.L375
.L385:
	movq	(%r14), %r8
	movq	arg(%rip), %rcx
	xorl	%eax, %eax
	leaq	prog(%rip), %rdx
	movq	bio_err(%rip), %rdi
	leaq	.LC32(%rip), %rsi
	call	BIO_printf@PLT
	orl	$-1, %eax
	jmp	.L358
	.p2align 4,,10
	.p2align 3
.L383:
	movq	%r8, %rdi
	call	app_isdir@PLT
	testl	%eax, %eax
	jg	.L375
	movq	arg(%rip), %rcx
	movq	bio_err(%rip), %rdi
	leaq	prog(%rip), %rdx
	xorl	%eax, %eax
	leaq	.LC31(%rip), %rsi
	call	BIO_printf@PLT
	orl	$-1, %eax
	jmp	.L358
	.p2align 4,,10
	.p2align 3
.L381:
	movq	%r8, -72(%rbp)
	call	__errno_location@PLT
	movq	-72(%rbp), %r8
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movl	(%rax), %r12d
	movl	$0, (%rax)
	leaq	-64(%rbp), %rsi
	movq	%rax, %rbx
	movq	%r8, %rdi
	call	__strtoul_internal@PLT
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	cmpb	$0, (%rdx)
	je	.L435
	.p2align 4,,10
	.p2align 3
.L396:
	movq	%r8, %rdi
	call	opt_number_error
	movl	%r12d, (%rbx)
	movq	(%r14), %r8
	xorl	%eax, %eax
	movq	arg(%rip), %rcx
	movq	bio_err(%rip), %rdi
	leaq	prog(%rip), %rdx
	leaq	.LC33(%rip), %rsi
	call	BIO_printf@PLT
	movl	$-1, %eax
	jmp	.L358
.L379:
	movq	%r8, -72(%rbp)
	call	__errno_location@PLT
	movq	-72(%rbp), %r8
	xorl	%edx, %edx
	leaq	-64(%rbp), %rsi
	movl	(%rax), %r12d
	movl	$0, (%rax)
	movq	%rax, %rbx
	movq	%r8, %rdi
	call	strtol@PLT
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	cmpb	$0, (%rdx)
	jne	.L396
.L436:
	cmpq	%r8, %rdx
	je	.L396
	movabsq	$9223372036854775807, %rdx
	addq	%rax, %rdx
	cmpq	$-3, %rdx
	jbe	.L397
.L423:
	cmpl	$34, (%rbx)
	jne	.L398
	jmp	.L396
.L382:
	movq	%r8, -72(%rbp)
	call	__errno_location@PLT
	movq	-72(%rbp), %r8
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movl	(%rax), %r12d
	movl	$0, (%rax)
	leaq	-64(%rbp), %rsi
	movq	%rax, %rbx
	movq	%r8, %rdi
	call	__strtol_internal@PLT
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	cmpb	$0, (%rdx)
	je	.L436
	jmp	.L396
	.p2align 4,,10
	.p2align 3
.L442:
	movq	-72(%rbp), %rsi
	movq	8(%r12,%rsi), %r8
	testq	%r8, %r8
	je	.L443
	addl	$2, %ebx
	movq	%r8, arg(%rip)
	movl	%ebx, opt_index(%rip)
	jmp	.L373
.L438:
	call	__stack_chk_fail@PLT
.L400:
	movq	bio_err(%rip), %rdi
	movq	%r13, %rcx
	leaq	prog(%rip), %rdx
	xorl	%eax, %eax
	leaq	.LC35(%rip), %rsi
	call	BIO_printf@PLT
	orl	$-1, %eax
	jmp	.L358
.L443:
	movq	bio_err(%rip), %rdi
	movq	%r15, %rcx
	leaq	prog(%rip), %rdx
	xorl	%eax, %eax
	leaq	.LC30(%rip), %rsi
	call	BIO_printf@PLT
	orl	$-1, %eax
	jmp	.L358
	.cfi_endproc
.LFE1439:
	.size	opt_next, .-opt_next
	.p2align 4
	.globl	opt_arg
	.type	opt_arg, @function
opt_arg:
.LFB1440:
	.cfi_startproc
	endbr64
	movq	arg(%rip), %rax
	ret
	.cfi_endproc
.LFE1440:
	.size	opt_arg, .-opt_arg
	.p2align 4
	.globl	opt_flag
	.type	opt_flag, @function
opt_flag:
.LFB1441:
	.cfi_startproc
	endbr64
	movq	flag(%rip), %rax
	ret
	.cfi_endproc
.LFE1441:
	.size	opt_flag, .-opt_flag
	.p2align 4
	.globl	opt_unknown
	.type	opt_unknown, @function
opt_unknown:
.LFB1442:
	.cfi_startproc
	endbr64
	movq	dunno(%rip), %rax
	ret
	.cfi_endproc
.LFE1442:
	.size	opt_unknown, .-opt_unknown
	.p2align 4
	.globl	opt_rest
	.type	opt_rest, @function
opt_rest:
.LFB1443:
	.cfi_startproc
	endbr64
	movslq	opt_index(%rip), %rdx
	movq	argv(%rip), %rax
	leaq	(%rax,%rdx,8), %rax
	ret
	.cfi_endproc
.LFE1443:
	.size	opt_rest, .-opt_rest
	.p2align 4
	.globl	opt_num_rest
	.type	opt_num_rest, @function
opt_num_rest:
.LFB1444:
	.cfi_startproc
	endbr64
	movslq	opt_index(%rip), %rdx
	movq	argv(%rip), %rax
	xorl	%r8d, %r8d
	leaq	(%rax,%rdx,8), %rax
	cmpq	$0, (%rax)
	je	.L448
.L450:
	addq	$8, %rax
	addl	$1, %r8d
	cmpq	$0, (%rax)
	jne	.L450
.L448:
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE1444:
	.size	opt_num_rest, .-opt_num_rest
	.section	.rodata.str1.1
.LC36:
	.string	"(No additional info)"
.LC37:
	.string	"parm"
.LC38:
	.string	""
.LC39:
	.string	"dir"
.LC40:
	.string	"infile"
.LC41:
	.string	"outfile"
.LC42:
	.string	"+int"
.LC43:
	.string	"int"
.LC44:
	.string	"long"
.LC45:
	.string	"ulong"
.LC46:
	.string	"PEM|DER|ENGINE"
.LC47:
	.string	"PEM|DER"
.LC48:
	.string	"format"
.LC49:
	.string	"intmax"
.LC50:
	.string	"uintmax"
.LC51:
	.string	"val"
	.section	.rodata.str1.8
	.align 8
.LC52:
	.string	"Usage: %s [options]\nValid options are:\n"
	.section	.rodata.str1.1
.LC53:
	.string	"%s  %s\n"
.LC54:
	.string	"%s\n"
	.text
	.p2align 4
	.globl	opt_help
	.type	opt_help, @function
opt_help:
.LFB1446:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%r15, %r15
	je	.L500
	movq	%rdi, %r13
	movl	$5, %ebx
	leaq	.L460(%rip), %r14
	movq	%r15, %rdi
	.p2align 4,,10
	.p2align 3
.L469:
	leaq	OPT_MORE_STR(%rip), %rax
	cmpq	%rax, %rdi
	je	.L455
	call	strlen@PLT
	movl	12(%r13), %edx
	addl	$2, %eax
	cmpl	$45, %edx
	je	.L456
	testl	%edx, %edx
	je	.L501
	subl	$47, %edx
	cmpl	$70, %edx
	ja	.L502
	movslq	(%r14,%rdx,4), %rdx
	addq	%r14, %rdx
	notrack jmp	*%rdx
	.section	.rodata
	.align 4
	.align 4
.L460:
	.long	.L461-.L460
	.long	.L502-.L460
	.long	.L502-.L460
	.long	.L502-.L460
	.long	.L502-.L460
	.long	.L502-.L460
	.long	.L502-.L460
	.long	.L502-.L460
	.long	.L502-.L460
	.long	.L502-.L460
	.long	.L502-.L460
	.long	.L502-.L460
	.long	.L502-.L460
	.long	.L503-.L460
	.long	.L502-.L460
	.long	.L464-.L460
	.long	.L502-.L460
	.long	.L502-.L460
	.long	.L502-.L460
	.long	.L502-.L460
	.long	.L502-.L460
	.long	.L502-.L460
	.long	.L466-.L460
	.long	.L464-.L460
	.long	.L502-.L460
	.long	.L502-.L460
	.long	.L502-.L460
	.long	.L502-.L460
	.long	.L502-.L460
	.long	.L502-.L460
	.long	.L503-.L460
	.long	.L502-.L460
	.long	.L502-.L460
	.long	.L502-.L460
	.long	.L502-.L460
	.long	.L502-.L460
	.long	.L502-.L460
	.long	.L502-.L460
	.long	.L464-.L460
	.long	.L502-.L460
	.long	.L502-.L460
	.long	.L502-.L460
	.long	.L502-.L460
	.long	.L502-.L460
	.long	.L502-.L460
	.long	.L502-.L460
	.long	.L502-.L460
	.long	.L502-.L460
	.long	.L502-.L460
	.long	.L502-.L460
	.long	.L502-.L460
	.long	.L502-.L460
	.long	.L502-.L460
	.long	.L502-.L460
	.long	.L502-.L460
	.long	.L503-.L460
	.long	.L502-.L460
	.long	.L502-.L460
	.long	.L502-.L460
	.long	.L502-.L460
	.long	.L502-.L460
	.long	.L502-.L460
	.long	.L502-.L460
	.long	.L461-.L460
	.long	.L502-.L460
	.long	.L502-.L460
	.long	.L502-.L460
	.long	.L502-.L460
	.long	.L461-.L460
	.long	.L502-.L460
	.long	.L459-.L460
	.text
.L503:
	movl	$7, %edx
.L457:
	addl	%edx, %eax
.L456:
	cmpl	$29, %eax
	jg	.L455
	cmpl	%ebx, %eax
	cmovg	%eax, %ebx
.L455:
	movq	24(%r13), %rdi
	addq	$24, %r13
	testq	%rdi, %rdi
	jne	.L469
	leaq	OPT_HELP_STR(%rip), %r14
	cmpq	%r14, %r15
	jne	.L454
	.p2align 4,,10
	.p2align 3
.L497:
	movq	16(%r12), %r13
	leaq	.LC36(%rip), %rax
	testq	%r13, %r13
	cmove	%rax, %r13
	cmpq	%r14, %r15
	je	.L517
	movdqa	.LC55(%rip), %xmm0
	movb	$0, -64(%rbp)
	leaq	-144(%rbp), %r15
	leaq	OPT_MORE_STR(%rip), %rax
	movaps	%xmm0, -144(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	cmpq	%rax, (%r12)
	je	.L496
	movq	(%r12), %rsi
	movl	$11552, %eax
	movw	%ax, -144(%rbp)
	cmpb	$0, (%rsi)
	jne	.L518
	cmpl	$45, 12(%r12)
	movb	$42, -142(%rbp)
	je	.L478
	leaq	-141(%rbp), %rax
.L476:
	movb	$32, (%rax)
	cmpl	$117, 12(%r12)
	leaq	1(%rax), %rdx
	ja	.L479
	movl	12(%r12), %eax
	leaq	.L481(%rip), %rcx
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L481:
	.long	.L506-.L481
	.long	.L479-.L481
	.long	.L479-.L481
	.long	.L479-.L481
	.long	.L479-.L481
	.long	.L479-.L481
	.long	.L479-.L481
	.long	.L479-.L481
	.long	.L479-.L481
	.long	.L479-.L481
	.long	.L479-.L481
	.long	.L479-.L481
	.long	.L479-.L481
	.long	.L479-.L481
	.long	.L479-.L481
	.long	.L479-.L481
	.long	.L479-.L481
	.long	.L479-.L481
	.long	.L479-.L481
	.long	.L479-.L481
	.long	.L479-.L481
	.long	.L479-.L481
	.long	.L479-.L481
	.long	.L479-.L481
	.long	.L479-.L481
	.long	.L479-.L481
	.long	.L479-.L481
	.long	.L479-.L481
	.long	.L479-.L481
	.long	.L479-.L481
	.long	.L479-.L481
	.long	.L479-.L481
	.long	.L479-.L481
	.long	.L479-.L481
	.long	.L479-.L481
	.long	.L479-.L481
	.long	.L479-.L481
	.long	.L479-.L481
	.long	.L479-.L481
	.long	.L479-.L481
	.long	.L479-.L481
	.long	.L479-.L481
	.long	.L479-.L481
	.long	.L479-.L481
	.long	.L479-.L481
	.long	.L506-.L481
	.long	.L479-.L481
	.long	.L493-.L481
	.long	.L479-.L481
	.long	.L479-.L481
	.long	.L479-.L481
	.long	.L479-.L481
	.long	.L479-.L481
	.long	.L479-.L481
	.long	.L479-.L481
	.long	.L479-.L481
	.long	.L479-.L481
	.long	.L479-.L481
	.long	.L479-.L481
	.long	.L479-.L481
	.long	.L492-.L481
	.long	.L479-.L481
	.long	.L491-.L481
	.long	.L479-.L481
	.long	.L479-.L481
	.long	.L479-.L481
	.long	.L479-.L481
	.long	.L479-.L481
	.long	.L479-.L481
	.long	.L490-.L481
	.long	.L489-.L481
	.long	.L479-.L481
	.long	.L479-.L481
	.long	.L479-.L481
	.long	.L479-.L481
	.long	.L479-.L481
	.long	.L479-.L481
	.long	.L488-.L481
	.long	.L479-.L481
	.long	.L479-.L481
	.long	.L479-.L481
	.long	.L479-.L481
	.long	.L479-.L481
	.long	.L479-.L481
	.long	.L479-.L481
	.long	.L487-.L481
	.long	.L479-.L481
	.long	.L479-.L481
	.long	.L479-.L481
	.long	.L479-.L481
	.long	.L479-.L481
	.long	.L479-.L481
	.long	.L479-.L481
	.long	.L479-.L481
	.long	.L479-.L481
	.long	.L479-.L481
	.long	.L479-.L481
	.long	.L479-.L481
	.long	.L479-.L481
	.long	.L479-.L481
	.long	.L479-.L481
	.long	.L479-.L481
	.long	.L486-.L481
	.long	.L479-.L481
	.long	.L479-.L481
	.long	.L479-.L481
	.long	.L479-.L481
	.long	.L479-.L481
	.long	.L485-.L481
	.long	.L479-.L481
	.long	.L484-.L481
	.long	.L479-.L481
	.long	.L483-.L481
	.long	.L479-.L481
	.long	.L479-.L481
	.long	.L482-.L481
	.long	.L479-.L481
	.long	.L480-.L481
	.text
.L461:
	movl	$4, %edx
	jmp	.L457
.L502:
	movl	$5, %edx
	jmp	.L457
.L464:
	movl	$8, %edx
	jmp	.L457
	.p2align 4,,10
	.p2align 3
.L500:
	movl	$5, %ebx
.L454:
	movq	bio_err(%rip), %rdi
	leaq	prog(%rip), %rdx
	leaq	.LC52(%rip), %rsi
	xorl	%eax, %eax
	leaq	OPT_HELP_STR(%rip), %r14
	call	BIO_printf@PLT
	movq	(%r12), %r15
	testq	%r15, %r15
	jne	.L497
	.p2align 4,,10
	.p2align 3
.L453:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L519
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L518:
	.cfi_restore_state
	leaq	-142(%rbp), %rcx
	movl	$79, %edx
	movq	%rcx, %rdi
	movq	%rcx, -152(%rbp)
	call	__strcpy_chk@PLT
	movq	%rax, %rdi
	call	strlen@PLT
	movq	-152(%rbp), %rcx
	addq	%rcx, %rax
	cmpl	$45, 12(%r12)
	jne	.L476
.L477:
	movq	%rax, %rdx
	subq	%r15, %rdx
	cmpl	$29, %edx
	jg	.L495
	movb	$32, (%rax)
.L496:
	movslq	%ebx, %rax
	movq	bio_err(%rip), %rdi
	movq	%r13, %rcx
	movq	%r15, %rdx
	movb	$0, -144(%rbp,%rax)
	leaq	.LC53(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L473:
	movq	24(%r12), %r15
	addq	$24, %r12
	testq	%r15, %r15
	jne	.L497
	jmp	.L453
	.p2align 4,,10
	.p2align 3
.L478:
	movb	$32, -141(%rbp)
	jmp	.L496
	.p2align 4,,10
	.p2align 3
.L495:
	movb	$0, (%rax)
	movq	bio_err(%rip), %rdi
	movq	%r15, %rdx
	xorl	%eax, %eax
	leaq	.LC54(%rip), %rsi
	call	BIO_printf@PLT
	movdqa	.LC55(%rip), %xmm0
	movb	$32, 80(%r15)
	movaps	%xmm0, (%r15)
	movaps	%xmm0, 16(%r15)
	movaps	%xmm0, 32(%r15)
	movaps	%xmm0, 48(%r15)
	movaps	%xmm0, 64(%r15)
	jmp	.L496
	.p2align 4,,10
	.p2align 3
.L517:
	movq	bio_err(%rip), %rdi
	leaq	prog(%rip), %rdx
	movq	%r13, %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L473
.L466:
	movl	$15, %edx
	jmp	.L457
.L459:
	movl	$6, %edx
	jmp	.L457
	.p2align 4,,10
	.p2align 3
.L501:
	movl	$1, %edx
	jmp	.L457
	.p2align 4,,10
	.p2align 3
.L479:
	leaq	.LC37(%rip), %rsi
	.p2align 4,,10
	.p2align 3
.L494:
	movq	%rdx, %rdi
	movq	%rdx, -152(%rbp)
	call	strcpy@PLT
	movq	%rax, %rdi
	call	strlen@PLT
	movq	-152(%rbp), %rdx
	addq	%rdx, %rax
	jmp	.L477
.L506:
	leaq	.LC38(%rip), %rsi
	jmp	.L494
.L493:
	leaq	.LC39(%rip), %rsi
	jmp	.L494
.L492:
	leaq	.LC40(%rip), %rsi
	jmp	.L494
.L491:
	leaq	.LC41(%rip), %rsi
	jmp	.L494
.L490:
	leaq	.LC46(%rip), %rsi
	jmp	.L494
.L489:
	leaq	.LC47(%rip), %rsi
	jmp	.L494
.L488:
	leaq	.LC49(%rip), %rsi
	jmp	.L494
.L487:
	leaq	.LC50(%rip), %rsi
	jmp	.L494
.L486:
	leaq	.LC48(%rip), %rsi
	jmp	.L494
.L485:
	leaq	.LC44(%rip), %rsi
	jmp	.L494
.L484:
	leaq	.LC43(%rip), %rsi
	jmp	.L494
.L483:
	leaq	.LC42(%rip), %rsi
	jmp	.L494
.L480:
	leaq	.LC45(%rip), %rsi
	jmp	.L494
.L482:
	leaq	.LC51(%rip), %rsi
	jmp	.L494
.L519:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1446:
	.size	opt_help, .-opt_help
	.section	.rodata.str1.1
.LC56:
	.string	"PEM/DER"
.LC57:
	.string	"smime"
.LC58:
	.string	"engine"
.LC59:
	.string	"msblob"
.LC60:
	.string	"text"
.LC61:
	.string	"http"
	.section	.data.rel.local,"aw"
	.align 32
	.type	formats, @object
	.size	formats, 160
formats:
	.quad	.LC56
	.long	2
	.zero	4
	.quad	.LC20
	.long	4
	.zero	4
	.quad	.LC57
	.long	8
	.zero	4
	.quad	.LC58
	.long	16
	.zero	4
	.quad	.LC59
	.long	32
	.zero	4
	.quad	.LC12
	.long	128
	.zero	4
	.quad	.LC60
	.long	256
	.zero	4
	.quad	.LC61
	.long	512
	.zero	4
	.quad	.LC16
	.long	1024
	.zero	4
	.quad	0
	.zero	8
	.local	prog
	.comm	prog,40,32
	.local	opts
	.comm	opts,8,8
	.local	unknown
	.comm	unknown,8,8
	.local	dunno
	.comm	dunno,8,8
	.local	flag
	.comm	flag,8,8
	.local	arg
	.comm	arg,8,8
	.local	opt_index
	.comm	opt_index,4,4
	.local	argv
	.comm	argv,8,8
	.globl	OPT_MORE_STR
	.section	.rodata
	.type	OPT_MORE_STR, @object
	.size	OPT_MORE_STR, 4
OPT_MORE_STR:
	.string	"---"
	.globl	OPT_HELP_STR
	.type	OPT_HELP_STR, @object
	.size	OPT_HELP_STR, 3
OPT_HELP_STR:
	.string	"--"
	.section	.data.rel.ro.local,"aw"
	.align 8
.LC7:
	.quad	.LC0
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC55:
	.quad	2314885530818453536
	.quad	2314885530818453536
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
