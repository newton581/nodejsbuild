	.file	"crl.c"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"%s: Use -help for summary.\n"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"Error initialising X509 store\n"
	.align 8
.LC2:
	.string	"Error getting CRL issuer certificate\n"
	.align 8
.LC3:
	.string	"Error getting CRL issuer public key\n"
	.section	.rodata.str1.1
.LC4:
	.string	"verify failure\n"
.LC5:
	.string	"verify OK\n"
.LC6:
	.string	"Missing CRL signing key\n"
.LC7:
	.string	"CRL signing key"
.LC8:
	.string	"Error creating delta CRL\n"
.LC9:
	.string	"issuer="
.LC10:
	.string	"crlNumber="
.LC11:
	.string	"<NONE>"
.LC12:
	.string	"\n"
.LC13:
	.string	"%08lx\n"
.LC14:
	.string	"lastUpdate="
.LC15:
	.string	"nextUpdate="
.LC16:
	.string	"NONE"
.LC17:
	.string	"out of memory\n"
.LC18:
	.string	"%s Fingerprint="
.LC19:
	.string	"%02X%c"
.LC20:
	.string	"unable to write CRL\n"
	.text
	.p2align 4
	.globl	crl_main
	.type	crl_main, @function
crl_main:
.LFB1435:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%edi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	.L5(%rip), %rbx
	subq	$216, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	EVP_sha1@PLT
	movq	%r13, %rsi
	movl	%r12d, %edi
	leaq	crl_options(%rip), %rdx
	movq	%rax, -144(%rbp)
	xorl	%r12d, %r12d
	xorl	%r13d, %r13d
	movl	$32773, -156(%rbp)
	movl	$32773, -152(%rbp)
	movl	$32773, -148(%rbp)
	call	opt_init@PLT
	movl	$0, -168(%rbp)
	movl	$0, -252(%rbp)
	movq	%rax, %r14
	movl	$0, -248(%rbp)
	movl	$0, -244(%rbp)
	movl	$0, -224(%rbp)
	movl	$0, -220(%rbp)
	movl	$0, -164(%rbp)
	movl	$0, -192(%rbp)
	movl	$0, -188(%rbp)
	movl	$0, -184(%rbp)
	movl	$0, -180(%rbp)
	movq	$0, -240(%rbp)
	movq	$0, -232(%rbp)
	movq	$0, -208(%rbp)
	movq	$0, -200(%rbp)
	movq	$0, -216(%rbp)
	movq	$0, -176(%rbp)
.L2:
	call	opt_next@PLT
	testl	%eax, %eax
	je	.L119
.L34:
	addl	$1, %eax
	cmpl	$26, %eax
	ja	.L2
	movslq	(%rbx,%rax,4), %rax
	addq	%rbx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L5:
	.long	.L30-.L5
	.long	.L2-.L5
	.long	.L29-.L5
	.long	.L28-.L5
	.long	.L27-.L5
	.long	.L26-.L5
	.long	.L25-.L5
	.long	.L24-.L5
	.long	.L23-.L5
	.long	.L22-.L5
	.long	.L21-.L5
	.long	.L20-.L5
	.long	.L19-.L5
	.long	.L18-.L5
	.long	.L17-.L5
	.long	.L16-.L5
	.long	.L15-.L5
	.long	.L14-.L5
	.long	.L13-.L5
	.long	.L12-.L5
	.long	.L11-.L5
	.long	.L10-.L5
	.long	.L9-.L5
	.long	.L8-.L5
	.long	.L7-.L5
	.long	.L6-.L5
	.long	.L4-.L5
	.text
.L13:
	movl	$1, %r12d
	call	opt_next@PLT
	testl	%eax, %eax
	jne	.L34
.L119:
	call	opt_num_rest@PLT
	testl	%eax, %eax
	je	.L120
.L30:
	movq	bio_err(%rip), %rdi
	movq	%r14, %rdx
	xorl	%eax, %eax
	xorl	%ebx, %ebx
	leaq	.LC0(%rip), %rsi
	xorl	%r15d, %r15d
	xorl	%r14d, %r14d
	call	BIO_printf@PLT
	movq	$0, -176(%rbp)
.L31:
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	movl	$1, -164(%rbp)
.L32:
	movq	%r15, %rdi
	call	BIO_free_all@PLT
	movq	%r14, %rdi
	call	X509_CRL_free@PLT
	movq	-176(%rbp), %rdi
	call	X509_STORE_CTX_free@PLT
	movq	%rbx, %rdi
	call	X509_STORE_free@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L121
	movl	-164(%rbp), %eax
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L4:
	.cfi_restore_state
	call	opt_unknown@PLT
	leaq	-144(%rbp), %rsi
	movq	%rax, %rdi
	call	opt_md@PLT
	testl	%eax, %eax
	jne	.L2
	jmp	.L30
.L6:
	call	opt_arg@PLT
	movq	%rax, %rdi
	call	set_nameopt@PLT
	testl	%eax, %eax
	jne	.L2
	jmp	.L30
.L7:
	leal	1(%r13), %eax
	movl	%eax, -164(%rbp)
	movl	%eax, %r13d
	jmp	.L2
.L8:
	addl	$1, %r13d
	movl	%r13d, -168(%rbp)
	jmp	.L2
.L9:
	leal	1(%r13), %eax
	movl	%eax, -180(%rbp)
	movl	%eax, %r13d
	jmp	.L2
.L10:
	movl	$1, -248(%rbp)
	jmp	.L2
.L11:
	movl	$1, %r15d
	jmp	.L2
.L12:
	movl	$1, -252(%rbp)
	jmp	.L2
.L14:
	call	opt_arg@PLT
	movl	$1, %r15d
	movq	%rax, -232(%rbp)
	jmp	.L2
.L15:
	call	opt_arg@PLT
	movl	$1, %r15d
	movq	%rax, -240(%rbp)
	jmp	.L2
.L16:
	call	opt_arg@PLT
	movq	%rax, -200(%rbp)
	jmp	.L2
.L17:
	movl	$1, -220(%rbp)
	jmp	.L2
.L18:
	addl	$1, %r13d
	movl	%r13d, -244(%rbp)
	jmp	.L2
.L19:
	addl	$1, %r13d
	movl	%r13d, -224(%rbp)
	jmp	.L2
.L20:
	leal	1(%r13), %eax
	movl	%eax, -192(%rbp)
	movl	%eax, %r13d
	jmp	.L2
.L21:
	leal	1(%r13), %eax
	movl	%eax, -188(%rbp)
	movl	%eax, %r13d
	jmp	.L2
.L22:
	leal	1(%r13), %eax
	movl	%eax, -184(%rbp)
	movl	%eax, %r13d
	jmp	.L2
.L23:
	call	opt_arg@PLT
	movq	%rax, -208(%rbp)
	jmp	.L2
.L24:
	call	opt_arg@PLT
	leaq	-148(%rbp), %rdx
	movl	$2, %esi
	movq	%rax, %rdi
	call	opt_format@PLT
	testl	%eax, %eax
	jne	.L2
	jmp	.L30
.L25:
	call	opt_arg@PLT
	movq	%rax, -216(%rbp)
	jmp	.L2
.L26:
	call	opt_arg@PLT
	leaq	-152(%rbp), %rdx
	movl	$2, %esi
	movq	%rax, %rdi
	call	opt_format@PLT
	testl	%eax, %eax
	jne	.L2
	jmp	.L30
.L27:
	call	opt_arg@PLT
	movq	%rax, -176(%rbp)
	jmp	.L2
.L28:
	call	opt_arg@PLT
	leaq	-156(%rbp), %rdx
	movl	$2, %esi
	movq	%rax, %rdi
	call	opt_format@PLT
	testl	%eax, %eax
	jne	.L2
	jmp	.L30
.L29:
	leaq	crl_options(%rip), %rdi
	xorl	%ebx, %ebx
	xorl	%r15d, %r15d
	xorl	%r14d, %r14d
	call	opt_help@PLT
	movl	$0, -164(%rbp)
	movq	$0, -176(%rbp)
	jmp	.L32
.L120:
	movl	-156(%rbp), %esi
	movq	-176(%rbp), %rdi
	xorl	%ebx, %ebx
	call	load_crl@PLT
	movq	$0, -176(%rbp)
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L73
	testl	%r15d, %r15d
	jne	.L122
.L35:
	cmpq	$0, -200(%rbp)
	je	.L41
	cmpq	$0, -208(%rbp)
	je	.L123
	movl	-156(%rbp), %esi
	movq	-200(%rbp), %rdi
	call	load_crl@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L31
	movl	-148(%rbp), %esi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	-208(%rbp), %rdi
	leaq	.LC7(%rip), %r9
	call	load_key@PLT
	testq	%rax, %rax
	je	.L124
	movq	-144(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%rax, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%rax, -200(%rbp)
	call	X509_CRL_diff@PLT
	movq	%r15, %rdi
	movq	%rax, %r12
	call	X509_CRL_free@PLT
	movq	-200(%rbp), %r9
	movq	%r9, %rdi
	call	EVP_PKEY_free@PLT
	testq	%r12, %r12
	je	.L44
	movq	%r14, %rdi
	movq	%r12, %r14
	call	X509_CRL_free@PLT
.L41:
	movl	-220(%rbp), %ecx
	testl	%ecx, %ecx
	jne	.L125
.L45:
	movl	$1, %r15d
	testl	%r13d, %r13d
	je	.L64
	movq	%rbx, -208(%rbp)
	movl	-224(%rbp), %r12d
	movl	%r15d, %ebx
	movl	-244(%rbp), %r15d
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L48:
	cmpl	%ebx, %r15d
	je	.L126
.L49:
	cmpl	%ebx, -180(%rbp)
	je	.L127
.L52:
	cmpl	%ebx, -168(%rbp)
	je	.L128
.L53:
	cmpl	%ebx, -188(%rbp)
	je	.L129
.L54:
	cmpl	%ebx, -192(%rbp)
	je	.L130
.L55:
	cmpl	%ebx, %r12d
	je	.L131
.L58:
	addl	$1, %ebx
	cmpl	%ebx, %r13d
	jl	.L132
.L46:
	cmpl	%ebx, -184(%rbp)
	jne	.L48
	call	get_nameopt@PLT
	movq	%r14, %rdi
	movq	%rax, -200(%rbp)
	call	X509_CRL_get_issuer@PLT
	movq	-200(%rbp), %rcx
	movq	bio_out(%rip), %rdi
	leaq	.LC9(%rip), %rsi
	movq	%rax, %rdx
	call	print_name@PLT
	jmp	.L48
.L130:
	movq	bio_out(%rip), %rdi
	xorl	%eax, %eax
	leaq	.LC15(%rip), %rsi
	call	BIO_printf@PLT
	movq	%r14, %rdi
	call	X509_CRL_get0_nextUpdate@PLT
	testq	%rax, %rax
	je	.L56
	movq	%r14, %rdi
	call	X509_CRL_get0_nextUpdate@PLT
	movq	bio_out(%rip), %rdi
	movq	%rax, %rsi
	call	ASN1_TIME_print@PLT
.L57:
	movq	bio_out(%rip), %rdi
	leaq	.LC12(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L55
.L129:
	movq	bio_out(%rip), %rdi
	leaq	.LC14(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	%r14, %rdi
	call	X509_CRL_get0_lastUpdate@PLT
	movq	bio_out(%rip), %rdi
	movq	%rax, %rsi
	call	ASN1_TIME_print@PLT
	movq	bio_out(%rip), %rdi
	leaq	.LC12(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L54
.L127:
	movq	%r14, %rdi
	call	X509_CRL_get_issuer@PLT
	movq	%rax, %rdi
	call	X509_NAME_hash@PLT
	movq	bio_out(%rip), %rdi
	leaq	.LC13(%rip), %rsi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L52
.L126:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$88, %esi
	movq	%r14, %rdi
	call	X509_CRL_get_ext_d2i@PLT
	movq	bio_out(%rip), %rdi
	leaq	.LC10(%rip), %rsi
	movq	%rax, -200(%rbp)
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	-200(%rbp), %r9
	testq	%r9, %r9
	je	.L50
	movq	bio_out(%rip), %rdi
	movq	%r9, %rsi
	call	i2a_ASN1_INTEGER@PLT
	movq	-200(%rbp), %r9
	movq	%r9, %rdi
	call	ASN1_INTEGER_free@PLT
.L51:
	movq	bio_out(%rip), %rdi
	leaq	.LC12(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L49
.L128:
	movq	%r14, %rdi
	call	X509_CRL_get_issuer@PLT
	movq	%rax, %rdi
	call	X509_NAME_hash_old@PLT
	movq	bio_out(%rip), %rdi
	leaq	.LC13(%rip), %rsi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L53
.L132:
	movq	-208(%rbp), %rbx
.L64:
	movl	-152(%rbp), %edx
	movq	-216(%rbp), %rdi
	movl	$119, %esi
	call	bio_open_default@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L31
	movl	-248(%rbp), %edx
	testl	%edx, %edx
	jne	.L133
.L65:
	movl	-164(%rbp), %eax
	testl	%eax, %eax
	jne	.L76
	cmpl	$4, -152(%rbp)
	movq	%r14, %rsi
	movq	%r15, %rdi
	je	.L134
	call	PEM_write_bio_X509_CRL@PLT
.L67:
	testl	%eax, %eax
	jne	.L32
	movq	bio_err(%rip), %rdi
	leaq	.LC20(%rip), %rsi
	call	BIO_printf@PLT
	jmp	.L31
.L122:
	movl	-252(%rbp), %edx
	movq	-240(%rbp), %rsi
	movl	%r12d, %ecx
	movq	-232(%rbp), %rdi
	call	setup_verify@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L71
	call	X509_LOOKUP_file@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	X509_STORE_add_lookup@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L72
	call	X509_STORE_CTX_new@PLT
	movq	%rax, -176(%rbp)
	testq	%rax, %rax
	je	.L37
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rbx, %rsi
	movq	%rax, %rdi
	movq	%rax, %r15
	call	X509_STORE_CTX_init@PLT
	testl	%eax, %eax
	je	.L37
	movq	%r14, %rdi
	call	X509_CRL_get_issuer@PLT
	movl	$1, %esi
	movq	%r15, %rdi
	movq	%rax, %rdx
	call	X509_STORE_CTX_get_obj_by_subject@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L135
	movq	%rax, %rdi
	call	X509_OBJECT_get0_X509@PLT
	movq	%rax, %rdi
	call	X509_get_pubkey@PLT
	movq	%r12, %rdi
	movq	%rax, %r15
	call	X509_OBJECT_free@PLT
	testq	%r15, %r15
	je	.L136
	movq	%r14, %rdi
	movq	%r15, %rsi
	call	X509_CRL_verify@PLT
	movq	%r15, %rdi
	movl	%eax, %r12d
	call	EVP_PKEY_free@PLT
	testl	%r12d, %r12d
	js	.L73
	movq	bio_err(%rip), %rdi
	jne	.L40
	leaq	.LC4(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L73:
	xorl	%r15d, %r15d
	jmp	.L31
.L131:
	leaq	-128(%rbp), %rax
	movq	-144(%rbp), %rsi
	leaq	-136(%rbp), %rcx
	movq	%r14, %rdi
	movq	%rax, %rdx
	movq	%rax, -200(%rbp)
	call	X509_CRL_digest@PLT
	testl	%eax, %eax
	je	.L137
	movq	-144(%rbp), %rdi
	call	EVP_MD_type@PLT
	movl	%eax, %edi
	call	OBJ_nid2sn@PLT
	movq	bio_out(%rip), %rdi
	leaq	.LC18(%rip), %rsi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movl	-136(%rbp), %eax
	xorl	%r9d, %r9d
	testl	%eax, %eax
	jle	.L58
	movl	%ebx, -220(%rbp)
	movq	%r9, %rbx
	.p2align 4,,10
	.p2align 3
.L60:
	leal	1(%rbx), %edx
	movl	$58, %ecx
	cmpl	%edx, %eax
	movq	-200(%rbp), %rax
	movzbl	(%rax,%rbx), %edx
	jne	.L117
	movl	$10, %ecx
.L117:
	movq	bio_out(%rip), %rdi
	leaq	.LC19(%rip), %rsi
	xorl	%eax, %eax
	addq	$1, %rbx
	call	BIO_printf@PLT
	movl	-136(%rbp), %eax
	cmpl	%ebx, %eax
	jg	.L60
	movl	-220(%rbp), %ebx
	jmp	.L58
.L56:
	movq	bio_out(%rip), %rdi
	leaq	.LC16(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L57
.L50:
	movq	bio_out(%rip), %rdi
	leaq	.LC11(%rip), %rsi
	call	BIO_puts@PLT
	jmp	.L51
.L125:
	movq	%r14, %rdi
	leaq	-136(%rbp), %rsi
	xorl	%edx, %edx
	call	X509_CRL_get0_signature@PLT
	movq	-136(%rbp), %rdi
	call	corrupt_signature@PLT
	jmp	.L45
.L76:
	movl	$0, -164(%rbp)
	jmp	.L32
.L133:
	call	get_nameopt@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%rax, %rdx
	call	X509_CRL_print_ex@PLT
	jmp	.L65
.L37:
	movq	bio_err(%rip), %rdi
	leaq	.LC1(%rip), %rsi
	xorl	%eax, %eax
	xorl	%r15d, %r15d
	call	BIO_printf@PLT
	jmp	.L31
.L71:
	movq	%rax, -176(%rbp)
	xorl	%r15d, %r15d
	jmp	.L31
.L123:
	movq	bio_err(%rip), %rdi
	leaq	.LC6(%rip), %rsi
	xorl	%r15d, %r15d
	call	BIO_puts@PLT
	jmp	.L31
.L72:
	movq	%rax, -176(%rbp)
	jmp	.L31
.L134:
	call	i2d_X509_CRL_bio@PLT
	jmp	.L67
.L124:
	movq	%r15, %rdi
	xorl	%r15d, %r15d
	call	X509_CRL_free@PLT
	jmp	.L31
.L44:
	movq	bio_err(%rip), %rdi
	leaq	.LC8(%rip), %rsi
	xorl	%r15d, %r15d
	call	BIO_puts@PLT
	jmp	.L31
.L40:
	leaq	.LC5(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L35
.L136:
	movq	bio_err(%rip), %rdi
	leaq	.LC3(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L31
.L137:
	movq	bio_err(%rip), %rdi
	leaq	.LC17(%rip), %rsi
	movq	-208(%rbp), %rbx
	xorl	%r15d, %r15d
	call	BIO_printf@PLT
	jmp	.L31
.L135:
	movq	bio_err(%rip), %rdi
	leaq	.LC2(%rip), %rsi
	xorl	%eax, %eax
	xorl	%r15d, %r15d
	call	BIO_printf@PLT
	jmp	.L31
.L121:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1435:
	.size	crl_main, .-crl_main
	.globl	crl_options
	.section	.rodata.str1.1
.LC21:
	.string	"help"
.LC22:
	.string	"Display this summary"
.LC23:
	.string	"inform"
.LC24:
	.string	"Input format; default PEM"
.LC25:
	.string	"in"
.LC26:
	.string	"Input file - default stdin"
.LC27:
	.string	"outform"
.LC28:
	.string	"Output format - default PEM"
.LC29:
	.string	"out"
.LC30:
	.string	"output file - default stdout"
.LC31:
	.string	"keyform"
	.section	.rodata.str1.8
	.align 8
.LC32:
	.string	"Private key file format (PEM or ENGINE)"
	.section	.rodata.str1.1
.LC33:
	.string	"key"
	.section	.rodata.str1.8
	.align 8
.LC34:
	.string	"CRL signing Private key to use"
	.section	.rodata.str1.1
.LC35:
	.string	"issuer"
.LC36:
	.string	"Print issuer DN"
.LC37:
	.string	"lastupdate"
.LC38:
	.string	"Set lastUpdate field"
.LC39:
	.string	"nextupdate"
.LC40:
	.string	"Set nextUpdate field"
.LC41:
	.string	"noout"
.LC42:
	.string	"No CRL output"
.LC43:
	.string	"fingerprint"
.LC44:
	.string	"Print the crl fingerprint"
.LC45:
	.string	"crlnumber"
.LC46:
	.string	"Print CRL number"
.LC47:
	.string	"badsig"
	.section	.rodata.str1.8
	.align 8
.LC48:
	.string	"Corrupt last byte of loaded CRL signature (for test)"
	.section	.rodata.str1.1
.LC49:
	.string	"gendelta"
	.section	.rodata.str1.8
	.align 8
.LC50:
	.string	"Other CRL to compare/diff to the Input one"
	.section	.rodata.str1.1
.LC51:
	.string	"CApath"
	.section	.rodata.str1.8
	.align 8
.LC52:
	.string	"Verify CRL using certificates in dir"
	.section	.rodata.str1.1
.LC53:
	.string	"CAfile"
	.section	.rodata.str1.8
	.align 8
.LC54:
	.string	"Verify CRL using certificates in file name"
	.section	.rodata.str1.1
.LC55:
	.string	"no-CAfile"
	.section	.rodata.str1.8
	.align 8
.LC56:
	.string	"Do not load the default certificates file"
	.section	.rodata.str1.1
.LC57:
	.string	"no-CApath"
	.section	.rodata.str1.8
	.align 8
.LC58:
	.string	"Do not load certificates from the default certificates directory"
	.section	.rodata.str1.1
.LC59:
	.string	"verify"
.LC60:
	.string	"Verify CRL signature"
.LC61:
	.string	"text"
	.section	.rodata.str1.8
	.align 8
.LC62:
	.string	"Print out a text format version"
	.section	.rodata.str1.1
.LC63:
	.string	"hash"
.LC64:
	.string	"Print hash value"
.LC65:
	.string	"nameopt"
	.section	.rodata.str1.8
	.align 8
.LC66:
	.string	"Various certificate name options"
	.section	.rodata.str1.1
.LC67:
	.string	""
.LC68:
	.string	"Any supported digest"
.LC69:
	.string	"hash_old"
	.section	.rodata.str1.8
	.align 8
.LC70:
	.string	"Print old-style (MD5) hash value"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	crl_options, @object
	.size	crl_options, 624
crl_options:
	.quad	.LC21
	.long	1
	.long	45
	.quad	.LC22
	.quad	.LC23
	.long	2
	.long	70
	.quad	.LC24
	.quad	.LC25
	.long	3
	.long	60
	.quad	.LC26
	.quad	.LC27
	.long	4
	.long	70
	.quad	.LC28
	.quad	.LC29
	.long	5
	.long	62
	.quad	.LC30
	.quad	.LC31
	.long	6
	.long	70
	.quad	.LC32
	.quad	.LC33
	.long	7
	.long	60
	.quad	.LC34
	.quad	.LC35
	.long	8
	.long	45
	.quad	.LC36
	.quad	.LC37
	.long	9
	.long	45
	.quad	.LC38
	.quad	.LC39
	.long	10
	.long	45
	.quad	.LC40
	.quad	.LC41
	.long	23
	.long	45
	.quad	.LC42
	.quad	.LC43
	.long	11
	.long	45
	.quad	.LC44
	.quad	.LC45
	.long	12
	.long	45
	.quad	.LC46
	.quad	.LC47
	.long	13
	.long	45
	.quad	.LC48
	.quad	.LC49
	.long	14
	.long	60
	.quad	.LC50
	.quad	.LC51
	.long	15
	.long	47
	.quad	.LC52
	.quad	.LC53
	.long	16
	.long	60
	.quad	.LC54
	.quad	.LC55
	.long	18
	.long	45
	.quad	.LC56
	.quad	.LC57
	.long	17
	.long	45
	.quad	.LC58
	.quad	.LC59
	.long	19
	.long	45
	.quad	.LC60
	.quad	.LC61
	.long	20
	.long	45
	.quad	.LC62
	.quad	.LC63
	.long	21
	.long	45
	.quad	.LC64
	.quad	.LC65
	.long	24
	.long	115
	.quad	.LC66
	.quad	.LC67
	.long	25
	.long	45
	.quad	.LC68
	.quad	.LC69
	.long	22
	.long	45
	.quad	.LC70
	.quad	0
	.zero	16
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
