	.file	"ca.c"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"certificateHold"
.LC1:
	.string	"cessationOfOperation"
.LC2:
	.string	"unspecified"
.LC3:
	.string	"keyCompromise"
.LC4:
	.string	"CACompromise"
.LC5:
	.string	"affiliationChanged"
.LC6:
	.string	"superseded"
.LC7:
	.string	"removeFromCRL"
.LC8:
	.string	"keyTime"
.LC9:
	.string	"holdInstruction"
.LC10:
	.string	"CAkeyTime"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC11:
	.string	"../deps/openssl/openssl/apps/ca.c"
	.section	.rodata.str1.1
.LC12:
	.string	"00"
.LC13:
	.string	"Memory allocation failure\n"
	.section	.rodata.str1.8
	.align 8
.LC14:
	.string	"Adding Entry with serial number %s to DB for %s\n"
	.section	.rodata.str1.1
.LC15:
	.string	"V"
.LC16:
	.string	"row exp_data"
.LC17:
	.string	"unknown"
.LC18:
	.string	"row ptr"
.LC19:
	.string	"failed to update database\n"
.LC20:
	.string	"TXT_DB error number %ld\n"
.LC21:
	.string	"ERROR:name does not match %s\n"
	.section	.rodata.str1.8
	.align 8
.LC22:
	.string	"ERROR:Already present, serial number %s\n"
	.align 8
.LC23:
	.string	"ERROR:Already revoked, serial number %s\n"
	.section	.rodata.str1.1
.LC24:
	.string	"Revoking Certificate %s.\n"
.LC25:
	.string	"Unknown CRL reason %s\n"
.LC26:
	.string	"Invalid object identifier %s\n"
	.section	.rodata.str1.8
	.align 8
.LC27:
	.string	"Invalid time format %s. Need YYYYMMDDHHMMSSZ\n"
	.section	.rodata.str1.1
.LC28:
	.string	"revocation reason"
.LC29:
	.string	","
	.section	.rodata.str1.8
	.align 8
.LC30:
	.string	"Error in revocation arguments\n"
	.text
	.p2align 4
	.type	do_revoke, @function
do_revoke:
.LFB1445:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -120(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	X509_get_subject_name@PLT
	xorl	%esi, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	call	X509_NAME_oneline@PLT
	movq	%r15, %rdi
	movq	%rax, -72(%rbp)
	call	X509_get_serialNumber@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	ASN1_INTEGER_to_BN@PLT
	testq	%rax, %rax
	je	.L39
	movq	%rax, %rdi
	movq	%rax, %r12
	call	BN_is_zero@PLT
	testl	%eax, %eax
	jne	.L75
	movq	%r12, %rdi
	call	BN_bn2hex@PLT
	movq	%rax, -88(%rbp)
.L4:
	movq	%r12, %rdi
	call	BN_free@PLT
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L7
	cmpb	$0, (%rdi)
	je	.L6
.L9:
	cmpq	$0, -88(%rbp)
	je	.L7
	movq	-120(%rbp), %rax
	leaq	-112(%rbp), %rbx
	movl	$3, %esi
	movq	%rbx, %rdx
	movq	8(%rax), %rdi
	call	TXT_DB_get_by_index@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L76
	movq	%rax, %rsi
	movq	%rbx, %rdi
	call	index_name_cmp@PLT
	testl	%eax, %eax
	jne	.L77
	movq	bio_err(%rip), %rdi
	cmpl	$-1, %r13d
	je	.L78
	movq	(%r12), %rax
	cmpb	$82, (%rax)
	je	.L79
	movq	24(%r12), %rdx
	xorl	%eax, %eax
	leaq	.LC24(%rip), %rsi
	call	BIO_printf@PLT
	cmpl	$2, %r13d
	je	.L20
	jg	.L21
	cmpl	$1, %r13d
	jne	.L23
	leaq	.LC2(%rip), %rsi
	movq	%r14, %rdi
	call	strcasecmp@PLT
	testl	%eax, %eax
	je	.L41
	leaq	.LC3(%rip), %rsi
	movq	%r14, %rdi
	call	strcasecmp@PLT
	testl	%eax, %eax
	je	.L42
	leaq	.LC4(%rip), %rsi
	movq	%r14, %rdi
	call	strcasecmp@PLT
	testl	%eax, %eax
	je	.L43
	leaq	.LC5(%rip), %rsi
	movq	%r14, %rdi
	call	strcasecmp@PLT
	testl	%eax, %eax
	je	.L44
	leaq	.LC6(%rip), %rsi
	movq	%r14, %rdi
	call	strcasecmp@PLT
	testl	%eax, %eax
	je	.L45
	leaq	.LC1(%rip), %rsi
	movq	%r14, %rdi
	call	strcasecmp@PLT
	testl	%eax, %eax
	je	.L46
	leaq	.LC0(%rip), %rsi
	movq	%r14, %rdi
	call	strcasecmp@PLT
	testl	%eax, %eax
	je	.L47
	leaq	.LC7(%rip), %rsi
	movq	%r14, %rdi
	call	strcasecmp@PLT
	testl	%eax, %eax
	je	.L48
	movq	bio_err(%rip), %rdi
	movq	%r14, %rdx
	leaq	.LC25(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L26:
	movq	bio_err(%rip), %rdi
	leaq	.LC30(%rip), %rsi
	xorl	%eax, %eax
	movl	$-1, %r12d
	call	BIO_printf@PLT
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L75:
	movl	$2067, %edx
	leaq	.LC11(%rip), %rsi
	leaq	.LC12(%rip), %rdi
	call	CRYPTO_strdup@PLT
	movq	%rax, -88(%rbp)
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L21:
	leal	-3(%r13), %eax
	cmpl	$1, %eax
	ja	.L23
	xorl	%edi, %edi
	movq	%r14, %rsi
	call	ASN1_GENERALIZEDTIME_set_string@PLT
	testl	%eax, %eax
	je	.L80
	cmpl	$3, %r13d
	leaq	.LC8(%rip), %r15
	leaq	.LC10(%rip), %rax
	cmove	%r15, %rax
	movq	%rax, -120(%rbp)
.L27:
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	X509_gmtime_adj@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L26
	movq	-120(%rbp), %rdi
	call	strlen@PLT
	movl	(%r15), %edx
	leal	2(%rdx,%rax), %edx
	testq	%r14, %r14
	je	.L35
	movq	%r14, %rdi
	movl	%edx, -128(%rbp)
	call	strlen@PLT
	movl	-128(%rbp), %edx
	leaq	.LC28(%rip), %rsi
	leal	1(%rdx,%rax), %edx
	movl	%edx, %edi
	movl	%edx, -128(%rbp)
	call	app_malloc@PLT
	movslq	-128(%rbp), %rdx
	movq	8(%r15), %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	movq	%rdx, -128(%rbp)
	call	OPENSSL_strlcpy@PLT
	movq	-128(%rbp), %rdx
	leaq	.LC29(%rip), %rsi
	movq	%r13, %rdi
	call	OPENSSL_strlcat@PLT
	movq	-128(%rbp), %rdx
	movq	-120(%rbp), %rsi
	movq	%r13, %rdi
	call	OPENSSL_strlcat@PLT
	movq	-128(%rbp), %rdx
	leaq	.LC29(%rip), %rsi
	movq	%r13, %rdi
	movq	%rdx, -120(%rbp)
	call	OPENSSL_strlcat@PLT
	movq	-120(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	OPENSSL_strlcat@PLT
.L30:
	movq	%r15, %rdi
	call	ASN1_UTCTIME_free@PLT
	testq	%r13, %r13
	je	.L26
	movq	(%r12), %rax
	movb	$82, (%rax)
	movq	(%r12), %rax
	movb	$0, 1(%rax)
	movq	%r13, 16(%r12)
	movl	$1, %r12d
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L6:
	movl	$2073, %edx
	leaq	.LC11(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-88(%rbp), %rdi
	movl	$2074, %edx
	leaq	.LC11(%rip), %rsi
	call	CRYPTO_strdup@PLT
	movq	%rax, -72(%rbp)
	testq	%rax, %rax
	jne	.L9
	.p2align 4,,10
	.p2align 3
.L7:
	movq	bio_err(%rip), %rdi
	leaq	.LC13(%rip), %rsi
	xorl	%eax, %eax
	leaq	-112(%rbp), %rbx
	movl	$-1, %r12d
	call	BIO_printf@PLT
.L2:
	leaq	-64(%rbp), %r14
	leaq	.LC11(%rip), %r13
	.p2align 4,,10
	.p2align 3
.L33:
	movq	(%rbx), %rdi
	movl	$2153, %edx
	movq	%r13, %rsi
	addq	$8, %rbx
	call	CRYPTO_free@PLT
	cmpq	%rbx, %r14
	jne	.L33
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L81
	addq	$88, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L76:
	.cfi_restore_state
	movq	-72(%rbp), %rcx
	movq	-88(%rbp), %rdx
	leaq	.LC14(%rip), %rsi
	xorl	%eax, %eax
	movq	bio_err(%rip), %rdi
	call	BIO_printf@PLT
	movl	$2091, %edx
	leaq	.LC11(%rip), %rsi
	leaq	.LC15(%rip), %rdi
	call	CRYPTO_strdup@PLT
	movq	%r15, %rdi
	movq	%rax, -112(%rbp)
	call	X509_get0_notAfter@PLT
	leaq	.LC16(%rip), %rsi
	movq	%rax, %r12
	movl	(%rax), %eax
	leal	1(%rax), %edi
	call	app_malloc@PLT
	movslq	(%r12), %rdx
	movq	8(%r12), %rsi
	movq	%rax, %rdi
	movq	%rax, -104(%rbp)
	call	memcpy@PLT
	movslq	(%r12), %rax
	movq	-104(%rbp), %rdx
	leaq	.LC11(%rip), %rsi
	leaq	.LC17(%rip), %rdi
	movb	$0, (%rdx,%rax)
	movl	$2097, %edx
	movq	$0, -96(%rbp)
	call	CRYPTO_strdup@PLT
	cmpq	$0, -112(%rbp)
	movq	%rax, -80(%rbp)
	je	.L51
	testq	%rax, %rax
	je	.L51
	leaq	.LC18(%rip), %rsi
	movl	$56, %edi
	call	app_malloc@PLT
	movq	%rax, %r12
	leaq	-97(%rbp), %rax
	subq	%r12, %rax
	cmpq	$30, %rax
	jbe	.L13
	movdqa	-112(%rbp), %xmm1
	movups	%xmm1, (%r12)
	movdqa	-96(%rbp), %xmm2
	movups	%xmm2, 16(%r12)
	movdqa	-80(%rbp), %xmm3
	movups	%xmm3, 32(%r12)
.L14:
	movq	$0, 48(%r12)
	movq	-120(%rbp), %rax
	movq	%r12, %rsi
	movq	8(%rax), %rdi
	call	TXT_DB_insert@PLT
	testl	%eax, %eax
	je	.L15
	pxor	%xmm0, %xmm0
	movl	$1, %r12d
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	cmpl	$-1, %r13d
	je	.L2
	movq	-120(%rbp), %rsi
	movq	%r14, %rcx
	movl	%r13d, %edx
	movq	%r15, %rdi
	call	do_revoke
	movl	%eax, %r12d
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L20:
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	OBJ_txt2obj@PLT
	movq	%rax, %rdi
	movq	%rax, %r13
	call	ASN1_OBJECT_free@PLT
	leaq	.LC9(%rip), %rax
	movq	%rax, -120(%rbp)
	testq	%r13, %r13
	jne	.L27
	movq	bio_err(%rip), %rdi
	movq	%r14, %rdx
	leaq	.LC26(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L23:
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	X509_gmtime_adj@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L26
	movl	(%rax), %eax
	leaq	.LC28(%rip), %rsi
	leal	1(%rax), %r14d
	movl	%r14d, %edi
	call	app_malloc@PLT
	movq	8(%r15), %rsi
	movslq	%r14d, %rdx
	movq	%rax, %rdi
	movq	%rax, %r13
	call	OPENSSL_strlcpy@PLT
	jmp	.L30
.L41:
	leaq	.LC2(%rip), %rax
	movq	%rax, -120(%rbp)
.L25:
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	X509_gmtime_adj@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L26
	movq	-120(%rbp), %rdi
	call	strlen@PLT
	movl	(%r15), %edx
	leal	2(%rdx,%rax), %edx
.L35:
	movl	%edx, %edi
	leaq	.LC28(%rip), %rsi
	movl	%edx, -128(%rbp)
	call	app_malloc@PLT
	movslq	-128(%rbp), %r14
	movq	8(%r15), %rsi
	movq	%rax, %r13
	movq	%rax, %rdi
	movq	%r14, %rdx
	call	OPENSSL_strlcpy@PLT
	movq	%r14, %rdx
	movq	%r13, %rdi
	leaq	.LC29(%rip), %rsi
	call	OPENSSL_strlcat@PLT
	movq	-120(%rbp), %rsi
	movq	%r14, %rdx
	movq	%r13, %rdi
	call	OPENSSL_strlcat@PLT
	jmp	.L30
.L13:
	movq	-112(%rbp), %rax
	movq	%rax, (%r12)
	movq	-104(%rbp), %rax
	movq	%rax, 8(%r12)
	movq	-96(%rbp), %rax
	movq	%rax, 16(%r12)
	movq	-88(%rbp), %rax
	movq	%rax, 24(%r12)
	movq	-80(%rbp), %rax
	movq	%rax, 32(%r12)
	movq	-72(%rbp), %rax
	movq	%rax, 40(%r12)
	jmp	.L14
.L80:
	movq	bio_err(%rip), %rdi
	movq	%r14, %rdx
	leaq	.LC27(%rip), %rsi
	call	BIO_printf@PLT
	jmp	.L26
.L39:
	movl	$-1, %r12d
	leaq	-112(%rbp), %rbx
	jmp	.L2
.L42:
	leaq	.LC3(%rip), %rax
	movq	%rax, -120(%rbp)
	jmp	.L25
.L43:
	leaq	.LC4(%rip), %rax
	movq	%rax, -120(%rbp)
	jmp	.L25
.L44:
	leaq	.LC5(%rip), %rax
	movq	%rax, -120(%rbp)
	jmp	.L25
.L45:
	leaq	.LC6(%rip), %rax
	movq	%rax, -120(%rbp)
	jmp	.L25
.L46:
	leaq	.LC1(%rip), %rax
	movq	%rax, -120(%rbp)
	jmp	.L25
.L48:
	leaq	.LC7(%rip), %rax
	movq	%rax, -120(%rbp)
	jmp	.L25
.L47:
	leaq	.LC0(%rip), %rax
	movq	%rax, -120(%rbp)
	jmp	.L25
.L51:
	movq	bio_err(%rip), %rdi
	leaq	.LC13(%rip), %rsi
	xorl	%eax, %eax
	movl	$-1, %r12d
	call	BIO_printf@PLT
	jmp	.L2
.L79:
	movq	-88(%rbp), %rdx
	leaq	.LC23(%rip), %rsi
	xorl	%eax, %eax
	movl	$-1, %r12d
	call	BIO_printf@PLT
	jmp	.L2
.L77:
	movq	-72(%rbp), %rdx
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
	movl	$-1, %r12d
	leaq	.LC21(%rip), %rsi
	call	BIO_printf@PLT
	jmp	.L2
.L15:
	movq	bio_err(%rip), %rdi
	leaq	.LC19(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	-120(%rbp), %rax
	movq	bio_err(%rip), %rdi
	leaq	.LC20(%rip), %rsi
	movq	8(%rax), %rax
	movq	32(%rax), %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	%r12, %rdi
	movl	$2112, %edx
	leaq	.LC11(%rip), %rsi
	call	CRYPTO_free@PLT
	movl	$-1, %r12d
	jmp	.L2
.L78:
	movq	-88(%rbp), %rdx
	leaq	.LC22(%rip), %rsi
	xorl	%eax, %eax
	movl	$-1, %r12d
	call	BIO_printf@PLT
	jmp	.L2
.L81:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1445:
	.size	do_revoke, .-do_revoke
	.section	.rodata.str1.8
	.align 8
.LC31:
	.string	"variable lookup failed for %s::%s\n"
	.text
	.p2align 4
	.type	lookup_conf, @function
lookup_conf:
.LFB1438:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -40
	call	NCONF_get_string@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L85
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L85:
	.cfi_restore_state
	movq	bio_err(%rip), %rdi
	movq	%r14, %rcx
	movq	%r13, %rdx
	xorl	%eax, %eax
	leaq	.LC31(%rip), %rsi
	call	BIO_printf@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1438:
	.size	lookup_conf, .-lookup_conf
	.section	.rodata.str1.1
.LC32:
	.string	"NULL"
	.section	.rodata.str1.8
	.align 8
.LC33:
	.string	"\ninvalid type, Data base error\n"
	.section	.rodata.str1.1
.LC34:
	.string	"Valid"
.LC35:
	.string	"Revoked"
.LC36:
	.string	"Expired"
.LC37:
	.string	"undef"
	.section	.rodata.str1.8
	.align 8
.LC38:
	.string	"The Subject's Distinguished Name is as follows\n"
	.align 8
.LC39:
	.string	"\nemailAddress type needs to be of type IA5STRING\n"
	.align 8
.LC40:
	.string	"\nThe string contains characters that are illegal for the ASN.1 type\n"
	.section	.rodata.str1.1
.LC41:
	.string	"PRINTABLE:'"
.LC42:
	.string	"T61STRING:'"
.LC43:
	.string	"IA5STRING:'"
.LC44:
	.string	"UNIVERSALSTRING:'"
.LC45:
	.string	"ASN.1 %2d:'"
.LC46:
	.string	"'\n"
.LC47:
	.string	"%c"
.LC48:
	.string	"\\0x%02X"
.LC49:
	.string	"^%c"
	.section	.rodata.str1.8
	.align 8
.LC50:
	.string	"%s:unknown object type in 'policy' configuration\n"
	.section	.rodata.str1.1
.LC51:
	.string	"optional"
.LC52:
	.string	"supplied"
	.section	.rodata.str1.8
	.align 8
.LC53:
	.string	"The %s field needed to be supplied and was missing\n"
	.section	.rodata.str1.1
.LC54:
	.string	"match"
	.section	.rodata.str1.8
	.align 8
.LC55:
	.string	"The mandatory %s field was missing\n"
	.align 8
.LC56:
	.string	"The %s field does not exist in the CA certificate,\nthe 'policy' is misconfigured\n"
	.align 8
.LC57:
	.string	"The %s field is different between\nCA certificate (%s) and the request (%s)\n"
	.align 8
.LC58:
	.string	"%s:invalid type in 'policy' configuration\n"
	.align 8
.LC59:
	.string	"Everything appears to be ok, creating and signing the certificate\n"
	.align 8
.LC60:
	.string	"Extra configuration file found\n"
	.align 8
.LC61:
	.string	"ERROR: adding extensions in section %s\n"
	.align 8
.LC62:
	.string	"Successfully added extensions from file.\n"
	.align 8
.LC63:
	.string	"Successfully added extensions from config\n"
	.align 8
.LC64:
	.string	"ERROR: adding extensions from request\n"
	.align 8
.LC65:
	.string	"The subject name appears to be ok, checking data base for clashes\n"
	.align 8
.LC66:
	.string	"ERROR:There is already a certificate for %s\n"
	.align 8
.LC67:
	.string	"ERROR:Serial number %s has already been issued,\n"
	.align 8
.LC68:
	.string	"      check the database/serial_file for corruption\n"
	.section	.rodata.str1.1
.LC69:
	.string	"Type          :%s\n"
.LC70:
	.string	"Was revoked on:%s\n"
.LC71:
	.string	"Expires on    :%s\n"
.LC72:
	.string	"Serial Number :%s\n"
.LC73:
	.string	"File name     :%s\n"
.LC74:
	.string	"Subject Name  :%s\n"
.LC75:
	.string	"Certificate Details:\n"
	.section	.rodata.str1.8
	.align 8
.LC76:
	.string	"Certificate is to be certified until "
	.section	.rodata.str1.1
.LC77:
	.string	" (%ld days)"
.LC78:
	.string	"\n"
.LC79:
	.string	"Sign the certificate? [y/n]:"
	.section	.rodata.str1.8
	.align 8
.LC80:
	.string	"CERTIFICATE WILL NOT BE CERTIFIED: I/O error\n"
	.align 8
.LC81:
	.string	"CERTIFICATE WILL NOT BE CERTIFIED\n"
	.section	.rodata.str1.1
.LC82:
	.string	"row expdate"
.LC83:
	.string	"row space"
	.section	.rodata.str1.8
	.align 8
.LC84:
	.string	"The matching entry has the following details\n"
	.text
	.p2align 4
	.type	do_body.isra.0, @function
do_body.isra.0:
.LFB1455:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$360, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rbp), %rax
	movq	%rdi, -280(%rbp)
	movq	%rsi, -320(%rbp)
	movq	40(%rbp), %rdi
	movq	%rax, -352(%rbp)
	movq	24(%rbp), %rax
	movq	%rdx, -288(%rbp)
	movq	%rax, -304(%rbp)
	movq	32(%rbp), %rax
	movq	%rcx, -336(%rbp)
	movq	%rax, -312(%rbp)
	movq	72(%rbp), %rax
	movq	%r8, -344(%rbp)
	movq	%rax, -360(%rbp)
	movq	80(%rbp), %rax
	movq	%r9, -240(%rbp)
	movq	%rax, -328(%rbp)
	movq	112(%rbp), %rax
	movq	%rax, -272(%rbp)
	movq	120(%rbp), %rax
	movq	%rax, -296(%rbp)
	movq	128(%rbp), %rax
	movq	%rax, -368(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -144(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	testq	%rdi, %rdi
	je	.L87
	movl	56(%rbp), %edx
	movq	48(%rbp), %rsi
	call	parse_name@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L320
	movq	-272(%rbp), %rdi
	movq	%rax, %rsi
	call	X509_REQ_set_subject_name@PLT
	movq	%r12, %rdi
	call	X509_NAME_free@PLT
.L87:
	movl	152(%rbp), %eax
	testl	%eax, %eax
	jne	.L321
.L90:
	movq	-272(%rbp), %rdi
	xorl	%r13d, %r13d
	leaq	.LC49(%rip), %r14
	call	X509_REQ_get_subject_name@PLT
	movq	%rax, %r12
	leaq	-96(%rbp), %rax
	movq	%rax, -216(%rbp)
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L327:
	movl	4(%rbx), %eax
	cmpl	$28, %eax
	je	.L322
.L93:
	cmpl	$22, %eax
	je	.L323
	cmpl	$48, %edx
	je	.L324
.L97:
	cmpl	$30, %eax
	je	.L103
	cmpl	$12, %eax
	jne	.L96
.L103:
	movl	152(%rbp), %eax
	testl	%eax, %eax
	jne	.L325
.L101:
	addl	$1, %r13d
.L91:
	movq	%r12, %rdi
	call	X509_NAME_entry_count@PLT
	cmpl	%eax, %r13d
	jge	.L326
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	X509_NAME_get_entry@PLT
	movq	%rax, %r15
	movq	%rax, %rdi
	call	X509_NAME_ENTRY_get_data@PLT
	movq	%r15, %rdi
	movq	%rax, %rbx
	call	X509_NAME_ENTRY_get_object@PLT
	movq	%rax, %rdi
	movq	%rax, %r15
	call	OBJ_obj2nid@PLT
	movl	%eax, %edx
	movl	msie_hack(%rip), %eax
	testl	%eax, %eax
	jne	.L327
	cmpl	$48, %edx
	jne	.L100
	movl	64(%rbp), %eax
	testl	%eax, %eax
	je	.L101
.L100:
	movl	4(%rbx), %eax
	cmpl	$48, %edx
	jne	.L97
	cmpl	$22, %eax
	jne	.L206
.L96:
	movq	8(%rbx), %rdi
	movl	(%rbx), %esi
	call	ASN1_PRINTABLE_type@PLT
	cmpl	$20, %eax
	je	.L328
.L307:
	cmpl	$22, %eax
	jne	.L103
	cmpl	$19, 4(%rbx)
	jne	.L103
.L104:
	leaq	.LC40(%rip), %rsi
	jmp	.L319
	.p2align 4,,10
	.p2align 3
.L325:
	movq	bio_err(%rip), %rdi
	movq	%r15, %rsi
	call	i2a_ASN1_OBJECT@PLT
	movl	$22, %edx
	subl	%eax, %edx
	testl	%edx, %edx
	jle	.L208
	movl	$21, %edx
	movq	-216(%rbp), %rdi
	movl	$32, %esi
	subl	%eax, %edx
	movslq	%edx, %rdx
	leaq	1(%rdx), %r15
	movq	%r15, %rdx
	call	memset@PLT
	movq	-216(%rbp), %rax
	leaq	(%rax,%r15), %rdx
	movq	%rax, %rsi
.L105:
	movl	$58, %r15d
	movq	bio_err(%rip), %rdi
	movw	%r15w, (%rdx)
	call	BIO_puts@PLT
	movl	4(%rbx), %edx
	cmpl	$19, %edx
	je	.L329
	movq	bio_err(%rip), %rdi
	cmpl	$20, %edx
	je	.L330
	cmpl	$22, %edx
	je	.L331
	cmpl	$28, %edx
	je	.L332
	leaq	.LC45(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L107:
	movl	(%rbx), %eax
	movq	8(%rbx), %r15
	leal	-1(%rax), %edx
	leaq	1(%r15,%rdx), %rbx
	testl	%eax, %eax
	jg	.L115
	jmp	.L116
	.p2align 4,,10
	.p2align 3
.L112:
	testb	%dl, %dl
	js	.L333
	addl	$64, %edx
	movq	%r14, %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L113:
	addq	$1, %r15
	cmpq	%r15, %rbx
	je	.L116
.L115:
	movsbl	(%r15), %edx
	movq	bio_err(%rip), %rdi
	leal	-32(%rdx), %ecx
	cmpb	$94, %cl
	ja	.L112
	leaq	.LC47(%rip), %rsi
	xorl	%eax, %eax
	addq	$1, %r15
	call	BIO_printf@PLT
	cmpq	%r15, %rbx
	jne	.L115
.L116:
	movq	bio_err(%rip), %rdi
	leaq	.LC46(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L333:
	leaq	.LC48(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L323:
	cmpl	$48, %edx
	je	.L95
	movq	8(%rbx), %rdi
	movl	(%rbx), %esi
	movl	$20, 4(%rbx)
	call	ASN1_PRINTABLE_type@PLT
	cmpl	$20, %eax
	jne	.L307
.L328:
	cmpl	$20, 4(%rbx)
	je	.L103
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L324:
	cmpl	$19, %eax
	jne	.L98
	movl	64(%rbp), %eax
	movl	$22, 4(%rbx)
	testl	%eax, %eax
	jne	.L96
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L322:
	movq	%rbx, %rdi
	movl	%edx, -224(%rbp)
	call	ASN1_UNIVERSALSTRING_to_string@PLT
	movl	4(%rbx), %eax
	movl	-224(%rbp), %edx
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L95:
	movl	64(%rbp), %edx
	testl	%edx, %edx
	jne	.L96
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L98:
	movl	64(%rbp), %eax
	testl	%eax, %eax
	je	.L101
.L206:
	leaq	.LC39(%rip), %rsi
.L319:
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	$0, -224(%rbp)
	movq	$0, -264(%rbp)
.L315:
	xorl	%r15d, %r15d
.L316:
	xorl	%r12d, %r12d
.L318:
	movl	$-1, -216(%rbp)
	leaq	-96(%rbp), %rbx
	leaq	-144(%rbp), %r13
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L329:
	movq	bio_err(%rip), %rdi
	leaq	.LC41(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L332:
	leaq	.LC44(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L330:
	leaq	.LC42(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L326:
	call	X509_NAME_new@PLT
	movq	%rax, -264(%rbp)
	testq	%rax, %rax
	je	.L334
	movl	168(%rbp), %r14d
	testl	%r14d, %r14d
	jne	.L335
	movq	-288(%rbp), %rdi
	call	X509_get_subject_name@PLT
	movq	%rax, %rdi
	call	X509_NAME_dup@PLT
	movq	%rax, %r15
.L120:
	testq	%r15, %r15
	je	.L211
	movl	$0, -224(%rbp)
	movq	$0, -384(%rbp)
	movq	$0, -376(%rbp)
	movq	%r12, -216(%rbp)
	jmp	.L121
	.p2align 4,,10
	.p2align 3
.L339:
	cmpl	$-1, %r13d
	je	.L336
.L125:
	addl	$1, -224(%rbp)
.L121:
	movq	-240(%rbp), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, -224(%rbp)
	jge	.L337
	movl	-224(%rbp), %esi
	movq	-240(%rbp), %rdi
	call	OPENSSL_sk_value@PLT
	movq	8(%rax), %rdi
	movq	%rax, -232(%rbp)
	call	OBJ_txt2nid@PLT
	movl	%eax, %edi
	testl	%eax, %eax
	je	.L338
	call	OBJ_nid2obj@PLT
	movl	$-1, %r13d
	movq	%rax, %r12
.L123:
	movq	-216(%rbp), %rdi
	movl	%r13d, %edx
	movq	%r12, %rsi
	call	X509_NAME_get_index_by_OBJ@PLT
	movl	%eax, %ebx
	testl	%eax, %eax
	js	.L339
	movq	-216(%rbp), %rdi
	movl	%eax, %esi
	call	X509_NAME_get_entry@PLT
	movl	$9, %ecx
	leaq	.LC51(%rip), %rdi
	movq	%rax, %r14
	movq	-232(%rbp), %rax
	movq	16(%rax), %r13
	movq	%r13, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L127
	testq	%r14, %r14
	je	.L129
.L128:
	movq	-264(%rbp), %rdi
	xorl	%ecx, %ecx
	movl	$-1, %edx
	movq	%r14, %rsi
	call	X509_NAME_add_entry@PLT
	testl	%eax, %eax
	je	.L205
.L129:
	movl	%ebx, %r13d
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L331:
	leaq	.LC43(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L208:
	movq	-216(%rbp), %rdx
	movq	%rdx, %rsi
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L335:
	movq	%r12, %rdi
	call	X509_NAME_dup@PLT
	movq	%rax, %r15
	jmp	.L120
	.p2align 4,,10
	.p2align 3
.L127:
	movl	$9, %ecx
	movq	%r13, %rsi
	leaq	.LC52(%rip), %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L130
	testq	%r14, %r14
	jne	.L128
.L196:
	movq	-232(%rbp), %rax
	movq	bio_err(%rip), %rdi
	leaq	.LC53(%rip), %rsi
	movq	8(%rax), %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L211
	.p2align 4,,10
	.p2align 3
.L336:
	movq	-232(%rbp), %rax
	movl	$9, %ecx
	leaq	.LC51(%rip), %rdi
	movq	16(%rax), %r13
	movq	%r13, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L125
	movl	$9, %ecx
	leaq	.LC52(%rip), %rdi
	movq	%r13, %rsi
	movq	%r13, %r14
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L196
	movl	$6, %ecx
	leaq	.LC54(%rip), %rdi
	movq	%r14, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L131
.L132:
	movq	-232(%rbp), %rax
	movq	bio_err(%rip), %rdi
	leaq	.LC55(%rip), %rsi
	movq	8(%rax), %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L211:
	xorl	%r12d, %r12d
	leaq	-96(%rbp), %rbx
	leaq	-144(%rbp), %r13
	movq	$0, -224(%rbp)
	movl	$-1, -216(%rbp)
.L89:
	movq	%r13, %r14
	leaq	.LC11(%rip), %r13
	.p2align 4,,10
	.p2align 3
.L191:
	movq	(%r14), %rdi
	movl	$1886, %edx
	movq	%r13, %rsi
	addq	$8, %r14
	call	CRYPTO_free@PLT
	cmpq	%r14, %rbx
	jne	.L191
	movl	$1888, %edx
	leaq	.LC11(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
	movq	%r15, %rdi
	call	X509_NAME_free@PLT
	movq	-264(%rbp), %rdi
	call	X509_NAME_free@PLT
	movq	-224(%rbp), %rdi
	call	X509_free@PLT
.L86:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L340
	movl	-216(%rbp), %eax
	addq	$360, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L321:
	.cfi_restore_state
	movq	bio_err(%rip), %rdi
	leaq	.LC38(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L90
.L320:
	movq	bio_err(%rip), %rdi
	xorl	%r15d, %r15d
	leaq	-96(%rbp), %rbx
	leaq	-144(%rbp), %r13
	call	ERR_print_errors@PLT
	movq	$0, -224(%rbp)
	movq	$0, -264(%rbp)
	movl	$-1, -216(%rbp)
	jmp	.L89
.L338:
	movq	-232(%rbp), %rax
	movq	bio_err(%rip), %rdi
	leaq	.LC50(%rip), %rsi
	movq	8(%rax), %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L211
.L337:
	movl	preserve(%rip), %r13d
	movq	-216(%rbp), %r12
	testl	%r13d, %r13d
	jne	.L341
.L139:
	movl	104(%rbp), %ebx
	testl	%ebx, %ebx
	jne	.L342
.L140:
	call	X509_new@PLT
	movq	%rax, -224(%rbp)
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L316
	movq	%rax, %rdi
	call	X509_get_serialNumber@PLT
	movq	-312(%rbp), %rdi
	movq	%rax, %rsi
	call	BN_to_ASN1_INTEGER@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L318
	movl	168(%rbp), %r11d
	testl	%r11d, %r11d
	je	.L141
	movq	-264(%rbp), %rsi
	movq	%rbx, %rdi
	call	X509_set_issuer_name@PLT
	testl	%eax, %eax
	je	.L316
.L143:
	movl	88(%rbp), %ecx
	movq	-328(%rbp), %rdx
	movq	-360(%rbp), %rsi
	movq	-224(%rbp), %rdi
	call	set_cert_times@PLT
	testl	%eax, %eax
	je	.L316
	cmpq	$0, -328(%rbp)
	je	.L144
	movq	-224(%rbp), %rdi
	call	X509_get0_notAfter@PLT
	xorl	%edx, %edx
	xorl	%esi, %esi
	leaq	-208(%rbp), %rdi
	movq	%rax, %rcx
	call	ASN1_TIME_diff@PLT
	testl	%eax, %eax
	je	.L316
	movslq	-208(%rbp), %rax
	movq	%rax, 88(%rbp)
.L144:
	movq	-224(%rbp), %rbx
	movq	-264(%rbp), %rsi
	movq	%rbx, %rdi
	call	X509_set_subject_name@PLT
	testl	%eax, %eax
	je	.L316
	movq	-272(%rbp), %r14
	movq	%r14, %rdi
	call	X509_REQ_get0_pubkey@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	X509_set_pubkey@PLT
	testl	%eax, %eax
	je	.L316
	cmpq	$0, -296(%rbp)
	je	.L146
	movl	168(%rbp), %r10d
	leaq	-208(%rbp), %r12
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	testl	%r10d, %r10d
	je	.L147
	movq	%r14, %rcx
	movq	%rbx, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	X509V3_set_ctx@PLT
.L148:
	movq	extconf(%rip), %rsi
	testq	%rsi, %rsi
	je	.L149
	movl	104(%rbp), %r8d
	testl	%r8d, %r8d
	jne	.L343
	movq	%r12, %rdi
	call	X509V3_set_nconf@PLT
	movq	-224(%rbp), %rcx
	movq	-296(%rbp), %rdx
	movq	%r12, %rsi
	movq	extconf(%rip), %rdi
	call	X509V3_EXT_add_nconf@PLT
	testl	%eax, %eax
	je	.L201
.L146:
	movl	160(%rbp), %edx
	movq	-272(%rbp), %rsi
	movq	-224(%rbp), %rdi
	call	copy_extensions@PLT
	testl	%eax, %eax
	je	.L344
	movq	-224(%rbp), %rdi
	call	X509_get0_extensions@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L160
	call	OPENSSL_sk_num@PLT
	testl	%eax, %eax
	jg	.L345
.L160:
	movl	104(%rbp), %edi
	testl	%edi, %edi
	jne	.L346
.L159:
	movl	64(%rbp), %esi
	testl	%esi, %esi
	jne	.L161
	movq	-264(%rbp), %rdi
	movl	$-1, %r13d
	call	X509_NAME_dup@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L162
	jmp	.L317
	.p2align 4,,10
	.p2align 3
.L163:
	movq	%r12, %rdi
	leal	-1(%rsi), %r13d
	call	X509_NAME_delete_entry@PLT
	movq	%rax, %rdi
	call	X509_NAME_ENTRY_free@PLT
.L162:
	movl	$48, %esi
	movl	%r13d, %edx
	movq	%r12, %rdi
	call	X509_NAME_get_index_by_NID@PLT
	movl	%eax, %esi
	testl	%eax, %eax
	jns	.L163
	movq	-224(%rbp), %rdi
	movq	%r12, %rsi
	call	X509_set_subject_name@PLT
	movq	%r12, %rdi
	testl	%eax, %eax
	je	.L347
	call	X509_NAME_free@PLT
.L161:
	movq	-224(%rbp), %rdi
	call	X509_get_subject_name@PLT
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	X509_NAME_oneline@PLT
	movq	%rax, -104(%rbp)
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L317
	movq	-312(%rbp), %rdi
	call	BN_is_zero@PLT
	testl	%eax, %eax
	je	.L166
	movl	$1736, %edx
	leaq	.LC11(%rip), %rsi
	leaq	.LC12(%rip), %rdi
	call	CRYPTO_strdup@PLT
	movq	%rax, -120(%rbp)
	movq	%rax, %r12
.L167:
	testq	%r12, %r12
	je	.L317
	movq	-104(%rbp), %rdi
	cmpb	$0, (%rdi)
	je	.L348
.L169:
	movq	-352(%rbp), %rax
	leaq	-144(%rbp), %r13
	movl	(%rax), %ecx
	testl	%ecx, %ecx
	je	.L170
	movq	-304(%rbp), %rax
	movq	%r13, %rdx
	movl	$5, %esi
	movq	(%rax), %rdi
	call	TXT_DB_get_by_index@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L170
	movq	-104(%rbp), %rdx
	movq	bio_err(%rip), %rdi
	leaq	.LC66(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L171:
	movq	bio_err(%rip), %rdi
	leaq	.LC84(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	(%r12), %rax
	leaq	.LC36(%rip), %rdx
	movzbl	(%rax), %eax
	cmpb	$69, %al
	je	.L173
	leaq	.LC35(%rip), %rdx
	cmpb	$82, %al
	je	.L173
	cmpb	$86, %al
	leaq	.LC34(%rip), %rdx
	leaq	.LC33(%rip), %rax
	cmovne	%rax, %rdx
.L173:
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
	leaq	.LC69(%rip), %rsi
	call	BIO_printf@PLT
	movq	(%r12), %rax
	cmpb	$82, (%rax)
	je	.L349
.L174:
	movq	8(%r12), %rdx
	testq	%rdx, %rdx
	je	.L350
.L176:
	movq	bio_err(%rip), %rdi
	leaq	.LC71(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	24(%r12), %rdx
	testq	%rdx, %rdx
	je	.L351
.L177:
	movq	bio_err(%rip), %rdi
	leaq	.LC72(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	32(%r12), %rdx
	testq	%rdx, %rdx
	je	.L352
.L178:
	movq	bio_err(%rip), %rdi
	leaq	.LC73(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	40(%r12), %rdx
	testq	%rdx, %rdx
	je	.L353
.L179:
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
	xorl	%r12d, %r12d
	leaq	-96(%rbp), %rbx
	leaq	.LC74(%rip), %rsi
	call	BIO_printf@PLT
	movl	$-1, -216(%rbp)
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L334:
	movq	bio_err(%rip), %rdi
	leaq	.LC13(%rip), %rsi
	call	BIO_printf@PLT
	movq	$0, -224(%rbp)
	jmp	.L315
.L342:
	movq	bio_err(%rip), %rdi
	leaq	.LC59(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L140
.L356:
	movq	%rbx, -376(%rbp)
	movq	-248(%rbp), %r12
	movq	%r14, -384(%rbp)
	movl	-388(%rbp), %ebx
	testq	%r13, %r13
	je	.L129
	movq	-264(%rbp), %rdi
	xorl	%ecx, %ecx
	orl	$-1, %edx
	movq	%r13, %rsi
	call	X509_NAME_add_entry@PLT
	testl	%eax, %eax
	jne	.L129
.L205:
	movq	bio_err(%rip), %rdi
	leaq	.LC13(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L211
.L341:
	movq	-264(%rbp), %rdi
	call	X509_NAME_free@PLT
	movq	%r12, %rdi
	call	X509_NAME_dup@PLT
	movq	%rax, -264(%rbp)
	testq	%rax, %rax
	jne	.L139
	jmp	.L211
.L141:
	movq	-288(%rbp), %rdi
	call	X509_get_subject_name@PLT
	movq	-224(%rbp), %rdi
	movq	%rax, %rsi
	call	X509_set_issuer_name@PLT
	testl	%eax, %eax
	jne	.L143
	jmp	.L316
.L309:
	movq	%r13, %r14
.L131:
	movq	bio_err(%rip), %rdi
	movq	%r14, %rdx
	leaq	.LC58(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L211
.L344:
	movq	bio_err(%rip), %rdi
	leaq	.LC64(%rip), %rsi
	xorl	%r12d, %r12d
	leaq	-96(%rbp), %rbx
	leaq	-144(%rbp), %r13
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	movl	$-1, -216(%rbp)
	jmp	.L89
.L147:
	movq	-272(%rbp), %rcx
	movq	-224(%rbp), %rdx
	movq	%r12, %rdi
	movq	-288(%rbp), %rsi
	call	X509V3_set_ctx@PLT
	jmp	.L148
.L149:
	movq	-368(%rbp), %rbx
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	X509V3_set_nconf@PLT
	movq	-224(%rbp), %rcx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	-296(%rbp), %rdx
	call	X509V3_EXT_add_nconf@PLT
	testl	%eax, %eax
	je	.L354
	cmpl	$0, 104(%rbp)
	je	.L146
	movq	bio_err(%rip), %rdi
	leaq	.LC63(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L146
.L170:
	movq	-304(%rbp), %rax
	movq	%r13, %rdx
	movl	$3, %esi
	movq	(%rax), %rdi
	call	TXT_DB_get_by_index@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L172
	movq	-120(%rbp), %rdx
	movq	bio_err(%rip), %rdi
	leaq	.LC67(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	leaq	.LC68(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L171
.L340:
	call	__stack_chk_fail@PLT
.L166:
	movq	-312(%rbp), %rdi
	call	BN_bn2hex@PLT
	movq	%rax, -120(%rbp)
	movq	%rax, %r12
	jmp	.L167
.L346:
	movq	bio_err(%rip), %rdi
	leaq	.LC65(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L159
.L348:
	movl	$1750, %edx
	leaq	.LC11(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-120(%rbp), %rdi
	movl	$1751, %edx
	leaq	.LC11(%rip), %rsi
	call	CRYPTO_strdup@PLT
	movq	%rax, -104(%rbp)
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L169
.L317:
	movq	bio_err(%rip), %rdi
	leaq	.LC13(%rip), %rsi
	xorl	%eax, %eax
	leaq	-96(%rbp), %rbx
	leaq	-144(%rbp), %r13
	call	BIO_printf@PLT
	movl	$-1, -216(%rbp)
	jmp	.L89
.L347:
	call	X509_NAME_free@PLT
	xorl	%r12d, %r12d
	movl	$-1, -216(%rbp)
	leaq	-96(%rbp), %rbx
	leaq	-144(%rbp), %r13
	jmp	.L89
.L201:
	movq	-296(%rbp), %rdx
	movq	bio_err(%rip), %rdi
	leaq	.LC61(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	jmp	.L316
.L343:
	movq	bio_err(%rip), %rdi
	leaq	.LC60(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	extconf(%rip), %rsi
	movq	%r12, %rdi
	call	X509V3_set_nconf@PLT
	movq	-224(%rbp), %rcx
	movq	-296(%rbp), %rdx
	movq	%r12, %rsi
	movq	extconf(%rip), %rdi
	call	X509V3_EXT_add_nconf@PLT
	testl	%eax, %eax
	je	.L201
	movq	bio_err(%rip), %rdi
	leaq	.LC62(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L146
.L354:
	movq	-296(%rbp), %rdx
	movq	bio_err(%rip), %rdi
	leaq	.LC61(%rip), %rsi
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	jmp	.L316
.L352:
	leaq	.LC37(%rip), %rdx
	jmp	.L178
.L351:
	leaq	.LC37(%rip), %rdx
	jmp	.L177
.L350:
	leaq	.LC37(%rip), %rdx
	jmp	.L176
.L349:
	movq	8(%r12), %rdx
	testq	%rdx, %rdx
	je	.L355
.L175:
	movq	bio_err(%rip), %rdi
	leaq	.LC70(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L174
.L345:
	movq	-224(%rbp), %rdi
	movl	$2, %esi
	call	X509_set_version@PLT
	testl	%eax, %eax
	jne	.L160
	jmp	.L316
.L130:
	leaq	.LC54(%rip), %rsi
	movq	%r13, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L309
	testq	%r14, %r14
	je	.L132
	orl	$-1, %edx
	movq	%r12, -248(%rbp)
	movl	%ebx, -388(%rbp)
	movl	%edx, %r12d
	movq	-376(%rbp), %rbx
	movq	%r14, -256(%rbp)
	movq	-384(%rbp), %r14
	jmp	.L133
	.p2align 4,,10
	.p2align 3
.L300:
	movl	%r12d, %esi
	movq	%r15, %rdi
	call	X509_NAME_get_entry@PLT
	movq	-256(%rbp), %rdi
	movq	%rax, %r13
	call	X509_NAME_ENTRY_get_data@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	X509_NAME_ENTRY_get_data@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	movq	%rax, %r14
	call	ASN1_STRING_cmp@PLT
	testl	%eax, %eax
	je	.L356
.L133:
	movq	-248(%rbp), %rsi
	movl	%r12d, %edx
	movq	%r15, %rdi
	movl	%r12d, %r13d
	call	X509_NAME_get_index_by_OBJ@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	jns	.L300
	cmpl	$-1, %r13d
	je	.L357
	testl	%eax, %eax
	jns	.L300
	movq	%rbx, -376(%rbp)
	leaq	.LC32(%rip), %r8
	movq	%r14, %rax
	movq	%r14, -384(%rbp)
	testq	%rbx, %rbx
	je	.L194
	movq	8(%rbx), %r8
	movq	%r14, %rax
.L194:
	leaq	.LC32(%rip), %rcx
	testq	%rax, %rax
	je	.L137
	movq	8(%rax), %rcx
.L137:
	movq	-232(%rbp), %rax
	movq	bio_err(%rip), %rdi
	leaq	.LC57(%rip), %rsi
	movq	8(%rax), %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L211
	.p2align 4,,10
	.p2align 3
.L357:
	movq	-232(%rbp), %rax
	movq	bio_err(%rip), %rdi
	leaq	.LC56(%rip), %rsi
	movq	8(%rax), %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L211
.L353:
	leaq	.LC37(%rip), %rdx
	jmp	.L179
.L355:
	leaq	.LC37(%rip), %rdx
	jmp	.L175
.L172:
	cmpl	$0, 152(%rbp)
	je	.L358
.L200:
	movq	bio_err(%rip), %rdi
	leaq	.LC76(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	-224(%rbp), %rdi
	call	X509_get0_notAfter@PLT
	movq	bio_err(%rip), %rdi
	movq	%rax, %rsi
	call	ASN1_TIME_print@PLT
	cmpq	$0, 88(%rbp)
	jne	.L359
.L180:
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
	leaq	.LC78(%rip), %rsi
	call	BIO_printf@PLT
	cmpl	$0, 96(%rbp)
	jne	.L181
	movq	bio_err(%rip), %rdi
	leaq	.LC79(%rip), %rsi
	xorl	%eax, %eax
	leaq	-96(%rbp), %rbx
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movl	$11, %esi
	call	BIO_ctrl@PLT
	movq	stdin(%rip), %rdx
	movq	%rbx, %rdi
	movb	$0, -96(%rbp)
	movl	$25, %esi
	call	fgets@PLT
	testq	%rax, %rax
	je	.L360
	movzbl	-96(%rbp), %eax
	andl	$-33, %eax
	cmpb	$89, %al
	jne	.L361
.L181:
	movq	-224(%rbp), %rdi
	call	X509_get0_pubkey@PLT
	movq	%rax, %rdi
	movq	%rax, %r14
	call	EVP_PKEY_missing_parameters@PLT
	testl	%eax, %eax
	jne	.L362
.L184:
	movq	-224(%rbp), %rbx
	movq	-344(%rbp), %rcx
	movq	-336(%rbp), %rdx
	movq	-320(%rbp), %rsi
	movq	%rbx, %rdi
	call	do_X509_sign@PLT
	testl	%eax, %eax
	je	.L223
	movl	$1858, %edx
	leaq	.LC11(%rip), %rsi
	leaq	.LC15(%rip), %rdi
	call	CRYPTO_strdup@PLT
	movq	%rbx, %rdi
	movq	%rax, -144(%rbp)
	call	X509_get0_notAfter@PLT
	leaq	.LC82(%rip), %rsi
	movq	%rax, %rbx
	movl	(%rax), %eax
	leal	1(%rax), %edi
	call	app_malloc@PLT
	movslq	(%rbx), %rdx
	movq	8(%rbx), %rsi
	movq	%rax, %rdi
	movq	%rax, -136(%rbp)
	call	memcpy@PLT
	movslq	(%rbx), %rax
	movq	-136(%rbp), %rdx
	leaq	.LC11(%rip), %rsi
	leaq	.LC17(%rip), %rdi
	movb	$0, (%rdx,%rax)
	movl	$1864, %edx
	movq	$0, -128(%rbp)
	call	CRYPTO_strdup@PLT
	cmpq	$0, -144(%rbp)
	movq	%rax, -112(%rbp)
	je	.L186
	cmpq	$0, -136(%rbp)
	je	.L186
	testq	%rax, %rax
	je	.L186
	cmpq	$0, -104(%rbp)
	je	.L186
	leaq	.LC83(%rip), %rsi
	movl	$56, %edi
	call	app_malloc@PLT
	movq	%rax, %r12
	xorl	%eax, %eax
.L188:
	movq	(%rax,%r13), %rdx
	movq	%rdx, (%r12,%rax)
	addq	$8, %rax
	cmpq	$48, %rax
	jne	.L188
	movq	-304(%rbp), %rax
	movq	%r12, %rsi
	movq	$0, 48(%r12)
	movq	(%rax), %rdi
	call	TXT_DB_insert@PLT
	testl	%eax, %eax
	je	.L363
	movl	$1888, %edx
	leaq	.LC11(%rip), %rsi
	xorl	%edi, %edi
	call	CRYPTO_free@PLT
	movq	%r15, %rdi
	call	X509_NAME_free@PLT
	movq	-264(%rbp), %rdi
	call	X509_NAME_free@PLT
	movq	-280(%rbp), %rax
	movq	-224(%rbp), %rcx
	movl	$1, -216(%rbp)
	movq	%rcx, (%rax)
	jmp	.L86
.L359:
	movq	88(%rbp), %rdx
	movq	bio_err(%rip), %rdi
	leaq	.LC77(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L180
.L186:
	movq	bio_err(%rip), %rdi
	leaq	.LC13(%rip), %rsi
	xorl	%eax, %eax
	leaq	-96(%rbp), %rbx
	call	BIO_printf@PLT
	movl	$-1, -216(%rbp)
	jmp	.L89
.L363:
	movq	bio_err(%rip), %rdi
	leaq	.LC19(%rip), %rsi
	leaq	-96(%rbp), %rbx
	call	BIO_printf@PLT
	movq	-304(%rbp), %rax
	movq	bio_err(%rip), %rdi
	leaq	.LC20(%rip), %rsi
	movq	(%rax), %rax
	movq	32(%rax), %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movl	$-1, -216(%rbp)
	jmp	.L89
.L223:
	movl	$-1, -216(%rbp)
	leaq	-96(%rbp), %rbx
	jmp	.L89
.L362:
	movq	-320(%rbp), %rdi
	call	EVP_PKEY_missing_parameters@PLT
	testl	%eax, %eax
	jne	.L184
	movq	-320(%rbp), %rsi
	movq	%r14, %rdi
	call	EVP_PKEY_copy_parameters@PLT
	jmp	.L184
.L358:
	movq	bio_err(%rip), %rdi
	leaq	.LC75(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	136(%rbp), %rcx
	movq	144(%rbp), %rdx
	movq	-224(%rbp), %rsi
	movq	bio_err(%rip), %rdi
	orq	$520, %rcx
	call	X509_print_ex@PLT
	jmp	.L200
.L361:
	movq	bio_err(%rip), %rdi
	leaq	.LC81(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movl	$0, -216(%rbp)
	jmp	.L89
.L360:
	movq	bio_err(%rip), %rdi
	leaq	.LC80(%rip), %rsi
	xorl	%r12d, %r12d
	call	BIO_printf@PLT
	movl	$0, -216(%rbp)
	jmp	.L89
	.cfi_endproc
.LFE1455:
	.size	do_body.isra.0, .-do_body.isra.0
	.section	.rodata.str1.1
.LC85:
	.string	"r"
	.section	.rodata.str1.8
	.align 8
.LC86:
	.string	"Error reading certificate request in %s\n"
	.align 8
.LC87:
	.string	"Check that the request matches the signature\n"
	.align 8
.LC88:
	.string	"Certificate request and CA private key do not match\n"
	.section	.rodata.str1.1
.LC89:
	.string	"error unpacking public key\n"
	.section	.rodata.str1.8
	.align 8
.LC90:
	.string	"Signature verification problems....\n"
	.align 8
.LC91:
	.string	"Signature did not match the certificate request\n"
	.section	.rodata.str1.1
.LC92:
	.string	"Signature ok\n"
	.text
	.p2align 4
	.type	certify, @function
certify:
.LFB1439:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	leaq	.LC85(%rip), %rsi
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$40, %rsp
	movq	%rdi, -56(%rbp)
	movq	%r14, %rdi
	movq	%rcx, -64(%rbp)
	movq	%r8, -72(%rbp)
	call	BIO_new_file@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L380
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	PEM_read_bio_X509_REQ@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L381
	movl	120(%rbp), %edx
	movq	bio_err(%rip), %rdi
	testl	%edx, %edx
	jne	.L382
.L368:
	leaq	.LC87(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movl	160(%rbp), %eax
	testl	%eax, %eax
	jne	.L383
.L369:
	movq	%r12, %rdi
	call	X509_REQ_get0_pubkey@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L384
	movq	%r12, %rdi
	call	X509_REQ_verify@PLT
	movl	%eax, %r14d
	testl	%eax, %eax
	js	.L385
	movq	bio_err(%rip), %rdi
	je	.L386
	leaq	.LC92(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movl	160(%rbp), %eax
	movq	%rbx, %r8
	movq	%r15, %rsi
	pushq	%rax
	movl	152(%rbp), %eax
	pushq	%rax
	movl	144(%rbp), %eax
	pushq	%rax
	movl	120(%rbp), %eax
	pushq	136(%rbp)
	pushq	128(%rbp)
	pushq	112(%rbp)
	pushq	104(%rbp)
	pushq	%r12
	pushq	%rax
	movl	96(%rbp), %eax
	pushq	%rax
	movl	64(%rbp), %eax
	pushq	88(%rbp)
	pushq	80(%rbp)
	pushq	72(%rbp)
	pushq	%rax
	movl	56(%rbp), %eax
	pushq	%rax
	pushq	48(%rbp)
	pushq	40(%rbp)
	movq	24(%rbp), %rax
	pushq	32(%rbp)
	movq	16(%rbp), %r9
	addq	$8, %rax
	movq	-72(%rbp), %rcx
	movq	-64(%rbp), %rdx
	pushq	%rax
	movq	-56(%rbp), %rdi
	pushq	24(%rbp)
	call	do_body.isra.0
	addq	$160, %rsp
	movl	%eax, %r14d
.L366:
	movq	%r12, %rdi
	call	X509_REQ_free@PLT
	movq	%r13, %rdi
	call	BIO_free@PLT
	leaq	-40(%rbp), %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L383:
	.cfi_restore_state
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	X509_REQ_check_private_key@PLT
	movl	%eax, %r14d
	testl	%eax, %eax
	jne	.L369
	movq	bio_err(%rip), %rdi
	leaq	.LC88(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L366
	.p2align 4,,10
	.p2align 3
.L386:
	leaq	.LC91(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	jmp	.L366
	.p2align 4,,10
	.p2align 3
.L382:
	movq	136(%rbp), %rdx
	xorl	%ecx, %ecx
	movq	%rax, %rsi
	call	X509_REQ_print_ex@PLT
	movq	bio_err(%rip), %rdi
	jmp	.L368
	.p2align 4,,10
	.p2align 3
.L385:
	movq	bio_err(%rip), %rdi
	leaq	.LC90(%rip), %rsi
	xorl	%eax, %eax
	xorl	%r14d, %r14d
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	jmp	.L366
.L380:
	movq	bio_err(%rip), %rdi
	xorl	%r12d, %r12d
	movl	$-1, %r14d
	call	ERR_print_errors@PLT
	jmp	.L366
.L384:
	movq	bio_err(%rip), %rdi
	leaq	.LC89(%rip), %rsi
	xorl	%eax, %eax
	movl	$-1, %r14d
	call	BIO_printf@PLT
	jmp	.L366
.L381:
	movq	bio_err(%rip), %rdi
	movq	%r14, %rdx
	leaq	.LC86(%rip), %rsi
	xorl	%eax, %eax
	movl	$-1, %r14d
	call	BIO_printf@PLT
	jmp	.L366
	.cfi_endproc
.LFE1439:
	.size	certify, .-certify
	.section	.rodata.str1.1
.LC93:
	.string	"memory allocation failure\n"
.LC94:
	.string	"invalid revocation date %s\n"
.LC95:
	.string	"invalid reason code %s\n"
.LC96:
	.string	"missing hold instruction\n"
.LC97:
	.string	"invalid object identifier %s\n"
.LC98:
	.string	"missing compromised time\n"
.LC99:
	.string	"invalid compromised time %s\n"
	.text
	.p2align 4
	.globl	unpack_revinfo
	.type	unpack_revinfo, @function
unpack_revinfo:
.LFB1451:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r8, %rdi
	subq	$40, %rsp
	movq	%rsi, -56(%rbp)
	leaq	.LC11(%rip), %rsi
	movq	%rdx, -72(%rbp)
	movl	$2508, %edx
	movq	%rcx, -64(%rbp)
	call	CRYPTO_strdup@PLT
	leaq	.LC93(%rip), %rsi
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L434
	movl	$44, %esi
	movq	%rax, %rdi
	call	strchr@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L390
	movb	$0, (%rax)
	addq	$1, %r15
	movl	$44, %esi
	movq	%r15, %rdi
	call	strchr@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L391
	movb	$0, (%rax)
	addq	$1, %r13
.L391:
	testq	%rbx, %rbx
	je	.L394
.L404:
	call	ASN1_UTCTIME_new@PLT
	movq	%rax, (%rbx)
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L435
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	ASN1_UTCTIME_set_string@PLT
	movl	%eax, %ebx
	testl	%eax, %eax
	je	.L436
	testq	%r15, %r15
	je	.L405
.L394:
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	strcasecmp@PLT
	testl	%eax, %eax
	je	.L410
	leaq	.LC3(%rip), %rsi
	movq	%r15, %rdi
	call	strcasecmp@PLT
	testl	%eax, %eax
	je	.L411
	leaq	.LC4(%rip), %rsi
	movq	%r15, %rdi
	call	strcasecmp@PLT
	testl	%eax, %eax
	je	.L412
	leaq	.LC5(%rip), %rsi
	movq	%r15, %rdi
	call	strcasecmp@PLT
	testl	%eax, %eax
	je	.L413
	leaq	.LC6(%rip), %rsi
	movq	%r15, %rdi
	call	strcasecmp@PLT
	testl	%eax, %eax
	je	.L414
	leaq	.LC1(%rip), %rsi
	movq	%r15, %rdi
	call	strcasecmp@PLT
	testl	%eax, %eax
	je	.L415
	leaq	.LC0(%rip), %rsi
	movq	%r15, %rdi
	call	strcasecmp@PLT
	testl	%eax, %eax
	je	.L416
	leaq	.LC7(%rip), %rsi
	movq	%r15, %rdi
	call	strcasecmp@PLT
	testl	%eax, %eax
	je	.L417
	leaq	.LC9(%rip), %rsi
	movq	%r15, %rdi
	call	strcasecmp@PLT
	movl	%eax, %ebx
	testl	%eax, %eax
	je	.L397
	leaq	.LC8(%rip), %rsi
	movq	%r15, %rdi
	call	strcasecmp@PLT
	testl	%eax, %eax
	je	.L418
	leaq	.LC10(%rip), %rsi
	movq	%r15, %rdi
	call	strcasecmp@PLT
	testl	%eax, %eax
	je	.L419
	movq	bio_err(%rip), %rdi
	movq	%r15, %rdx
	xorl	%eax, %eax
	xorl	%r14d, %r14d
	leaq	.LC95(%rip), %rsi
	xorl	%ebx, %ebx
	call	BIO_printf@PLT
.L389:
	movq	%r12, %rdi
	movl	$2602, %edx
	leaq	.LC11(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%r14, %rdi
	call	ASN1_GENERALIZEDTIME_free@PLT
	addq	$40, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L390:
	.cfi_restore_state
	xorl	%r13d, %r13d
	testq	%rbx, %rbx
	jne	.L404
.L405:
	xorl	%r14d, %r14d
	movl	$-1, %eax
.L395:
	movq	-56(%rbp), %rcx
	testq	%rcx, %rcx
	je	.L403
	movl	%eax, (%rcx)
.L403:
	movq	-64(%rbp), %rax
	movl	$1, %ebx
	testq	%rax, %rax
	je	.L389
	movq	%r14, (%rax)
	xorl	%r14d, %r14d
	jmp	.L389
.L439:
	leaq	.LC98(%rip), %rsi
	.p2align 4,,10
	.p2align 3
.L434:
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	call	BIO_printf@PLT
	jmp	.L389
.L397:
	testq	%r13, %r13
	je	.L437
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	OBJ_txt2obj@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L438
	movq	-72(%rbp), %rax
	testq	%rax, %rax
	je	.L400
	movq	%r14, (%rax)
	xorl	%r14d, %r14d
	movl	$6, %eax
	jmp	.L395
	.p2align 4,,10
	.p2align 3
.L435:
	movq	bio_err(%rip), %rdi
	leaq	.LC93(%rip), %rsi
	xorl	%eax, %eax
	xorl	%ebx, %ebx
	call	BIO_printf@PLT
	jmp	.L389
	.p2align 4,,10
	.p2align 3
.L436:
	movq	bio_err(%rip), %rdi
	movq	%r12, %rdx
	xorl	%eax, %eax
	xorl	%r14d, %r14d
	leaq	.LC94(%rip), %rsi
	call	BIO_printf@PLT
	jmp	.L389
.L410:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L396:
	xorl	%r14d, %r14d
	jmp	.L395
.L411:
	movl	$1, %eax
	jmp	.L396
.L412:
	movl	$2, %eax
	jmp	.L396
.L413:
	movl	$3, %eax
	jmp	.L396
.L414:
	movl	$4, %eax
	jmp	.L396
.L415:
	movl	$5, %eax
	jmp	.L396
.L416:
	movl	$6, %eax
	jmp	.L396
.L417:
	xorl	%r14d, %r14d
	movl	$8, %eax
	jmp	.L395
.L437:
	movq	bio_err(%rip), %rdi
	leaq	.LC96(%rip), %rsi
	xorl	%eax, %eax
	xorl	%r14d, %r14d
	call	BIO_printf@PLT
	jmp	.L389
.L418:
	movl	$9, %r15d
.L398:
	testq	%r13, %r13
	je	.L439
	call	ASN1_GENERALIZEDTIME_new@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L435
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	ASN1_GENERALIZEDTIME_set_string@PLT
	movl	%eax, %ebx
	testl	%eax, %eax
	je	.L440
	xorl	%eax, %eax
	cmpl	$9, %r15d
	setne	%al
	addl	$1, %eax
	jmp	.L395
.L419:
	movl	$10, %r15d
	jmp	.L398
.L400:
	movq	%r14, %rdi
	xorl	%r14d, %r14d
	call	ASN1_OBJECT_free@PLT
	movl	$6, %eax
	jmp	.L395
.L440:
	movq	bio_err(%rip), %rdi
	movq	%r13, %rdx
	leaq	.LC99(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L389
.L438:
	movq	bio_err(%rip), %rdi
	movq	%r13, %rdx
	leaq	.LC97(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L389
	.cfi_endproc
.LFE1451:
	.size	unpack_revinfo, .-unpack_revinfo
	.section	.rodata.str1.1
.LC100:
	.string	"default"
.LC101:
	.string	"today"
.LC102:
	.string	"%s: Use -help for summary.\n"
.LC103:
	.string	"Using configuration from %s\n"
.LC104:
	.string	"default_ca"
.LC105:
	.string	"ca"
.LC106:
	.string	"oid_file"
.LC107:
	.string	"string_mask"
	.section	.rodata.str1.8
	.align 8
.LC108:
	.string	"Invalid global string mask setting %s\n"
	.section	.rodata.str1.1
.LC109:
	.string	"utf8"
.LC110:
	.string	"yes"
.LC111:
	.string	"unique_subject"
.LC112:
	.string	"database"
.LC113:
	.string	"row serial#"
.LC114:
	.string	"Serial %s not present in db.\n"
.LC115:
	.string	"%s=Valid (%c)\n"
.LC116:
	.string	"%s=Revoked (%c)\n"
.LC117:
	.string	"%s=Expired (%c)\n"
.LC118:
	.string	"%s=Suspended (%c)\n"
.LC119:
	.string	"%s=Unknown (%c).\n"
.LC120:
	.string	"Error verifying serial %s!\n"
.LC121:
	.string	"private_key"
.LC122:
	.string	"Error getting password\n"
.LC123:
	.string	"CA private key"
.LC124:
	.string	"certificate"
.LC125:
	.string	"CA certificate"
	.section	.rodata.str1.8
	.align 8
.LC126:
	.string	"CA certificate and CA private key do not match\n"
	.section	.rodata.str1.1
.LC127:
	.string	"preserve"
.LC128:
	.string	"msie_hack"
.LC129:
	.string	"name_opt"
.LC130:
	.string	"Invalid name options: \"%s\"\n"
.LC131:
	.string	"cert_opt"
	.section	.rodata.str1.8
	.align 8
.LC132:
	.string	"Invalid certificate options: \"%s\"\n"
	.section	.rodata.str1.1
.LC133:
	.string	"copy_extensions"
	.section	.rodata.str1.8
	.align 8
.LC134:
	.string	"Invalid extension copy option: \"%s\"\n"
	.section	.rodata.str1.1
.LC135:
	.string	"new_certs_dir"
	.section	.rodata.str1.8
	.align 8
.LC136:
	.string	"there needs to be defined a directory for new certificate to be placed in\n"
	.section	.rodata.str1.1
.LC137:
	.string	"%s: %s is not a directory\n"
	.section	.rodata.str1.8
	.align 8
.LC138:
	.string	"entry %d: not revoked yet, but has a revocation date\n"
	.align 8
.LC139:
	.string	"entry %d: invalid expiry date\n"
	.align 8
.LC140:
	.string	"entry %d: bad serial number length (%d)\n"
	.align 8
.LC141:
	.string	"entry %d: bad char 0%o '%c' in serial number\n"
	.align 8
.LC142:
	.string	"%d entries loaded from the database\n"
	.section	.rodata.str1.1
.LC143:
	.string	"generating index\n"
.LC144:
	.string	"Updating %s ...\n"
.LC145:
	.string	"time string"
.LC146:
	.string	"49"
.LC147:
	.string	"%s=Expired\n"
.LC148:
	.string	"Malloc failure\n"
	.section	.rodata.str1.8
	.align 8
.LC149:
	.string	"No entries found to mark expired\n"
	.section	.rodata.str1.1
.LC150:
	.string	"new"
.LC151:
	.string	"old"
	.section	.rodata.str1.8
	.align 8
.LC152:
	.string	"Done. %d entries marked as expired\n"
	.align 8
.LC153:
	.string	"Successfully loaded extensions file %s\n"
	.section	.rodata.str1.1
.LC154:
	.string	"extensions"
.LC155:
	.string	"default_md"
.LC156:
	.string	"no default digest\n"
.LC157:
	.string	"email_in_dn"
.LC158:
	.string	"no"
.LC159:
	.string	"message digest is %s\n"
.LC160:
	.string	"policy is %s\n"
.LC161:
	.string	"rand_serial"
.LC162:
	.string	"serial"
.LC163:
	.string	"x509_extensions"
	.section	.rodata.str1.8
	.align 8
.LC164:
	.string	"Error Loading extension section %s\n"
	.section	.rodata.str1.1
.LC165:
	.string	"default_startdate"
	.section	.rodata.str1.8
	.align 8
.LC166:
	.string	"start date is invalid, it should be YYMMDDHHMMSSZ or YYYYMMDDHHMMSSZ\n"
	.section	.rodata.str1.1
.LC167:
	.string	"default_enddate"
	.section	.rodata.str1.8
	.align 8
.LC168:
	.string	"end date is invalid, it should be YYMMDDHHMMSSZ or YYYYMMDDHHMMSSZ\n"
	.section	.rodata.str1.1
.LC169:
	.string	"default_days"
	.section	.rodata.str1.8
	.align 8
.LC170:
	.string	"cannot lookup how many days to certify for\n"
	.align 8
.LC171:
	.string	"error generating serial number\n"
	.align 8
.LC172:
	.string	"error while loading serial number\n"
	.section	.rodata.str1.1
.LC173:
	.string	"next serial number is 00\n"
.LC174:
	.string	"next serial number is %s\n"
	.section	.rodata.str1.8
	.align 8
.LC175:
	.string	"unable to find 'section' for %s\n"
	.section	.rodata.str1.1
.LC176:
	.string	"error on line %ld of %s\n"
	.section	.rodata.str1.8
	.align 8
.LC177:
	.string	"no name/value pairs found in %s\n"
	.section	.rodata.str1.1
.LC178:
	.string	"SPKAC"
	.section	.rodata.str1.8
	.align 8
.LC179:
	.string	"unable to load Netscape SPKAC structure\n"
	.align 8
.LC180:
	.string	"Netscape SPKAC structure not found in %s\n"
	.align 8
.LC181:
	.string	"Check that the SPKAC request matches the signature\n"
	.align 8
.LC182:
	.string	"error unpacking SPKAC public key\n"
	.align 8
.LC183:
	.string	"signature verification failed on SPKAC public key\n"
	.align 8
.LC184:
	.string	"Signature did not match the certificate\n"
	.align 8
.LC185:
	.string	"\n%d out of %d certificate requests certified, commit? [y/n]"
	.align 8
.LC186:
	.string	"CERTIFICATION CANCELED: I/O error\n"
	.section	.rodata.str1.1
.LC187:
	.string	"CERTIFICATION CANCELED\n"
	.section	.rodata.str1.8
	.align 8
.LC188:
	.string	"Write out database with %d new entries\n"
	.section	.rodata.str1.1
.LC189:
	.string	"/"
.LC190:
	.string	"writing new certificates\n"
	.section	.rodata.str1.8
	.align 8
.LC191:
	.string	"certificate file name too long\n"
	.section	.rodata.str1.1
.LC192:
	.string	"writing %s\n"
.LC193:
	.string	"w"
.LC194:
	.string	"Data Base Updated\n"
.LC195:
	.string	"crl_extensions"
	.section	.rodata.str1.8
	.align 8
.LC196:
	.string	"Error Loading CRL extension section %s\n"
	.section	.rodata.str1.1
.LC197:
	.string	"crlnumber"
	.section	.rodata.str1.8
	.align 8
.LC198:
	.string	"error while loading CRL number\n"
	.section	.rodata.str1.1
.LC199:
	.string	"default_crl_days"
.LC200:
	.string	"default_crl_hours"
	.section	.rodata.str1.8
	.align 8
.LC201:
	.string	"cannot lookup how long until the next CRL is issued\n"
	.section	.rodata.str1.1
.LC202:
	.string	"making CRL\n"
.LC203:
	.string	"error setting CRL nextUpdate\n"
.LC204:
	.string	"signing CRL\n"
.LC205:
	.string	"no input files\n"
.LC206:
	.string	" in entry %d\n"
.LC207:
	.string	"policy"
	.text
	.p2align 4
	.globl	ca_main
	.type	ca_main, @function
ca_main:
.LFB1437:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$632, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	ca_options(%rip), %rdx
	xorl	%r13d, %r13d
	xorl	%r12d, %r12d
	movq	default_config_file(%rip), %r15
	movl	$4097, %r14d
	leaq	.L447(%rip), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	xorl	%eax, %eax
	movb	$0, -4161(%rbp)
	movq	$0, -4368(%rbp)
	movq	$0, -4360(%rbp)
	movq	$0, -4352(%rbp)
	movq	$0, -4171(%rbp)
	movw	%ax, -4163(%rbp)
	movl	$0, -4384(%rbp)
	movl	$32773, -4380(%rbp)
	movq	$0, -4344(%rbp)
	movq	$0, -4336(%rbp)
	movq	$0, -4328(%rbp)
	movq	$0, -4320(%rbp)
	movq	$0, -4312(%rbp)
	call	opt_init@PLT
	movl	$0, -4644(%rbp)
	movq	%rax, -4656(%rbp)
	movq	$0, -4616(%rbp)
	movl	$0, -4504(%rbp)
	movl	$0, -4608(%rbp)
	movl	$0, -4472(%rbp)
	movl	$0, -4544(%rbp)
	movl	$1, -4540(%rbp)
	movl	$0, -4560(%rbp)
	movl	$0, -4556(%rbp)
	movl	$0, -4632(%rbp)
	movl	$0, -4448(%rbp)
	movl	$0, -4640(%rbp)
	movq	$0, -4528(%rbp)
	movq	$0, -4512(%rbp)
	movq	$0, -4568(%rbp)
	movq	$0, -4432(%rbp)
	movq	$0, -4624(%rbp)
	movq	$0, -4584(%rbp)
	movq	$0, -4552(%rbp)
	movq	$0, -4592(%rbp)
	movq	$0, -4600(%rbp)
	movq	$0, -4488(%rbp)
	movq	$0, -4576(%rbp)
	movq	$0, -4496(%rbp)
	movq	$0, -4480(%rbp)
	movq	$0, -4520(%rbp)
	movq	$0, -4416(%rbp)
	movq	$0, -4424(%rbp)
	movq	$0, -4536(%rbp)
	movq	$0, -4440(%rbp)
	movq	$0, -4408(%rbp)
	movq	$0, -4456(%rbp)
	movq	$0, -4464(%rbp)
.L442:
	call	opt_next@PLT
	movl	%eax, %edi
	testl	%eax, %eax
	je	.L457
	cmpl	$44, %edi
	jg	.L443
	cmpl	$-1, %edi
	jl	.L442
	addl	$1, %edi
	cmpl	$45, %edi
	ja	.L442
	movslq	(%rbx,%rdi,4), %rax
	addq	%rbx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L447:
	.long	.L490-.L447
	.long	.L442-.L447
	.long	.L489-.L447
	.long	.L488-.L447
	.long	.L487-.L447
	.long	.L486-.L447
	.long	.L485-.L447
	.long	.L484-.L447
	.long	.L741-.L447
	.long	.L483-.L447
	.long	.L482-.L447
	.long	.L481-.L447
	.long	.L480-.L447
	.long	.L479-.L447
	.long	.L478-.L447
	.long	.L477-.L447
	.long	.L476-.L447
	.long	.L475-.L447
	.long	.L474-.L447
	.long	.L473-.L447
	.long	.L472-.L447
	.long	.L471-.L447
	.long	.L470-.L447
	.long	.L469-.L447
	.long	.L468-.L447
	.long	.L467-.L447
	.long	.L466-.L447
	.long	.L465-.L447
	.long	.L464-.L447
	.long	.L463-.L447
	.long	.L462-.L447
	.long	.L461-.L447
	.long	.L460-.L447
	.long	.L459-.L447
	.long	.L458-.L447
	.long	.L742-.L447
	.long	.L456-.L447
	.long	.L455-.L447
	.long	.L454-.L447
	.long	.L453-.L447
	.long	.L452-.L447
	.long	.L451-.L447
	.long	.L450-.L447
	.long	.L449-.L447
	.long	.L448-.L447
	.long	.L446-.L447
	.text
.L742:
	movl	$1, %r12d
.L457:
	call	opt_num_rest@PLT
	movl	%eax, -4648(%rbp)
	call	opt_rest@PLT
	movq	bio_err(%rip), %rdi
	movq	%r15, %rdx
	leaq	.LC103(%rip), %rsi
	movq	%rax, -4664(%rbp)
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	%r15, %rdi
	call	app_load_config@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L981
	cmpq	%r15, default_config_file(%rip)
	je	.L500
	movq	%rax, %rdi
	call	app_load_modules@PLT
	movl	%eax, -4468(%rbp)
	testl	%eax, %eax
	je	.L955
.L500:
	cmpq	$0, -4408(%rbp)
	je	.L982
.L501:
	movq	%rbx, %rdi
	leaq	.LC106(%rip), %rdx
	xorl	%esi, %esi
	call	NCONF_get_string@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L731
	leaq	.LC85(%rip), %rsi
	call	BIO_new_file@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L731
	movq	%r15, %rdi
	call	OBJ_create_objects@PLT
	movq	%r15, %rdi
	call	BIO_free@PLT
.L503:
	movq	%rbx, %rdi
	call	add_oid_section@PLT
	movl	%eax, -4468(%rbp)
	testl	%eax, %eax
	je	.L983
	leaq	.LC105(%rip), %rsi
	movq	%rbx, %rdi
	call	app_RAND_load_conf@PLT
	movq	-4408(%rbp), %rsi
	leaq	.LC107(%rip), %rdx
	movq	%rbx, %rdi
	call	NCONF_get_string@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L984
	movq	%rax, %rdi
	call	ASN1_STRING_set_default_mask_asc@PLT
	movl	%eax, -4468(%rbp)
	testl	%eax, %eax
	je	.L985
.L506:
	cmpq	$4096, %r14
	je	.L510
	movq	-4408(%rbp), %rsi
	leaq	.LC109(%rip), %rdx
	movq	%rbx, %rdi
	call	NCONF_get_string@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L986
	movl	$4, %ecx
	leaq	.LC110(%rip), %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L987
.L510:
	movq	$4096, -4672(%rbp)
.L508:
	movq	-4408(%rbp), %rsi
	movq	%rbx, %rdi
	leaq	.LC111(%rip), %rdx
	movl	$1, -4388(%rbp)
	call	NCONF_get_string@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L511
	movl	$1, %esi
	call	parse_yesno@PLT
	movl	%eax, -4388(%rbp)
.L512:
	cmpq	$0, -4432(%rbp)
	je	.L513
	movq	-4408(%rbp), %rsi
	leaq	.LC112(%rip), %rdx
	movq	%rbx, %rdi
	call	NCONF_get_string@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L988
	leaq	-4388(%rbp), %rsi
	movq	%rax, %rdi
	call	load_index@PLT
	movq	%rax, -4416(%rbp)
	testq	%rax, %rax
	je	.L989
	movq	%rax, %rdi
	call	index_index@PLT
	testl	%eax, %eax
	jg	.L515
.L980:
	movq	bio_err(%rip), %rdi
.L743:
	movl	$0, -4468(%rbp)
	xorl	%r15d, %r15d
	xorl	%r14d, %r14d
	movq	$0, -4408(%rbp)
	movq	$0, -4440(%rbp)
	movq	$0, -4432(%rbp)
	movq	$0, -4424(%rbp)
	jmp	.L494
.L741:
	movl	$4096, %r14d
	jmp	.L442
.L487:
	movl	$1, %r13d
	jmp	.L442
	.p2align 4,,10
	.p2align 3
.L443:
	cmpl	$1502, %edi
	jg	.L491
	cmpl	$1500, %edi
	jle	.L442
	call	opt_rand@PLT
	testl	%eax, %eax
	jne	.L442
.L954:
	movl	%eax, -4468(%rbp)
	movq	bio_err(%rip), %rdi
	xorl	%r15d, %r15d
	jmp	.L952
	.p2align 4,,10
	.p2align 3
.L491:
	leal	-1504(%rdi), %eax
	cmpl	$3, %eax
	ja	.L442
	movl	%edi, -4468(%rbp)
	call	opt_arg@PLT
	movl	-4468(%rbp), %edi
	movq	%rax, -4624(%rbp)
	leal	-1503(%rdi), %eax
	movl	%eax, -4644(%rbp)
	jmp	.L442
.L475:
	call	opt_arg@PLT
	leaq	-4380(%rbp), %rdx
	movl	$1982, %esi
	movq	%rax, %rdi
	call	opt_format@PLT
	testl	%eax, %eax
	jne	.L442
.L490:
	movq	bio_err(%rip), %rdi
	movq	-4656(%rbp), %rdx
	xorl	%eax, %eax
	xorl	%r15d, %r15d
	leaq	.LC102(%rip), %rsi
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	movl	$0, -4468(%rbp)
.L952:
	movq	$0, -4408(%rbp)
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	movq	$0, -4440(%rbp)
	movq	$0, -4416(%rbp)
	movq	$0, -4432(%rbp)
	movq	$0, -4424(%rbp)
.L494:
	call	ERR_print_errors@PLT
	movl	$1, -4448(%rbp)
.L651:
	movq	-4432(%rbp), %rdi
	call	BIO_free_all@PLT
	xorl	%edi, %edi
	call	BIO_free_all@PLT
	xorl	%edi, %edi
	call	BIO_free_all@PLT
	movq	X509_free@GOTPCREL(%rip), %rsi
	movq	-4440(%rbp), %rdi
	call	OPENSSL_sk_pop_free@PLT
	movl	-4468(%rbp), %eax
	testl	%eax, %eax
	je	.L495
	movq	-4352(%rbp), %rdi
	movl	$1241, %edx
	leaq	.LC11(%rip), %rsi
	call	CRYPTO_free@PLT
.L495:
	movq	-4368(%rbp), %rdi
	call	BN_free@PLT
	movq	%r14, %rdi
	call	BN_free@PLT
	movq	-4416(%rbp), %rdi
	call	free_index@PLT
	movq	-4456(%rbp), %rdi
	call	OPENSSL_sk_free@PLT
	movq	-4424(%rbp), %rdi
	call	EVP_PKEY_free@PLT
	movq	%r15, %rdi
	call	X509_free@PLT
	movq	-4408(%rbp), %rdi
	call	X509_CRL_free@PLT
	movq	%rbx, %rdi
	call	NCONF_free@PLT
	movq	extconf(%rip), %rdi
	call	NCONF_free@PLT
	movq	-4464(%rbp), %rdi
	call	release_engine@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L990
	movl	-4448(%rbp), %eax
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L446:
	.cfi_restore_state
	movl	$1, -4608(%rbp)
	jmp	.L442
.L448:
	call	opt_arg@PLT
	movq	%rax, -4520(%rbp)
	jmp	.L442
.L486:
	call	opt_arg@PLT
	movq	%rax, %r15
	jmp	.L442
.L488:
	call	opt_arg@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	setup_engine@PLT
	movq	%rax, -4464(%rbp)
	jmp	.L442
.L449:
	movl	$1, -4632(%rbp)
	jmp	.L442
.L450:
	call	opt_arg@PLT
	movq	%rax, -4432(%rbp)
	jmp	.L442
.L451:
	call	opt_arg@PLT
	movq	%rax, -4600(%rbp)
	jmp	.L442
.L452:
	call	opt_arg@PLT
	movq	%rax, -4488(%rbp)
	jmp	.L442
.L453:
	call	opt_arg@PLT
	movl	$2, -4472(%rbp)
	movq	%rax, -4480(%rbp)
	jmp	.L442
.L454:
	call	opt_arg@PLT
	movl	$1, -4472(%rbp)
	movq	%rax, -4480(%rbp)
	jmp	.L442
.L455:
	call	opt_arg@PLT
	movl	$1, %r12d
	movq	%rax, -4496(%rbp)
	jmp	.L442
.L456:
	call	opt_arg@PLT
	movl	$1, %r12d
	movq	%rax, -4576(%rbp)
	jmp	.L442
.L458:
	call	opt_arg@PLT
	movl	$10, %edx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	strtol@PLT
	movq	%rax, -4616(%rbp)
	jmp	.L442
.L459:
	call	opt_arg@PLT
	movl	$10, %edx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	strtol@PLT
	movq	%rax, -4336(%rbp)
	jmp	.L442
.L460:
	call	opt_arg@PLT
	movl	$10, %edx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	strtol@PLT
	movq	%rax, -4344(%rbp)
	jmp	.L442
.L461:
	movl	$1, msie_hack(%rip)
	jmp	.L442
.L462:
	movl	$1, -4544(%rbp)
	jmp	.L442
.L463:
	movl	$0, -4540(%rbp)
	jmp	.L442
.L464:
	movl	$1, preserve(%rip)
	jmp	.L442
.L465:
	movl	$1, -4448(%rbp)
	jmp	.L442
.L466:
	movl	$1, -4560(%rbp)
	jmp	.L442
.L467:
	cmpq	$0, -4456(%rbp)
	je	.L991
.L497:
	call	opt_arg@PLT
	movq	-4456(%rbp), %rdi
	movq	%rax, %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	jne	.L442
	jmp	.L954
.L468:
	call	opt_arg@PLT
	movq	%rax, -4552(%rbp)
	jmp	.L442
.L469:
	call	opt_arg@PLT
	movq	%rax, -4584(%rbp)
	jmp	.L442
.L470:
	call	opt_arg@PLT
	movl	$1, %r12d
	movq	%rax, -4480(%rbp)
	jmp	.L442
.L471:
	movl	$1, -4504(%rbp)
	jmp	.L442
.L472:
	call	opt_arg@PLT
	movq	%rax, -4416(%rbp)
	jmp	.L442
.L473:
	call	opt_arg@PLT
	movq	%rax, -4352(%rbp)
	jmp	.L442
.L474:
	call	opt_arg@PLT
	movq	%rax, -4592(%rbp)
	jmp	.L442
.L476:
	call	opt_arg@PLT
	movq	%rax, -4424(%rbp)
	jmp	.L442
.L477:
	call	opt_arg@PLT
	movq	%rax, -4536(%rbp)
	jmp	.L442
.L478:
	call	opt_arg@PLT
	movq	%rax, -4440(%rbp)
	jmp	.L442
.L479:
	call	opt_arg@PLT
	movl	$10, %edx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	strtol@PLT
	cltq
	movq	%rax, -4328(%rbp)
	jmp	.L442
.L480:
	call	opt_arg@PLT
	movq	%rax, -4528(%rbp)
	jmp	.L442
.L481:
	call	opt_arg@PLT
	movq	%rax, -4512(%rbp)
	jmp	.L442
.L482:
	movl	$1, -4556(%rbp)
	jmp	.L442
.L483:
	movl	$1, -4640(%rbp)
	jmp	.L442
.L484:
	call	opt_arg@PLT
	movq	%rax, -4568(%rbp)
	jmp	.L442
.L485:
	call	opt_arg@PLT
	movq	%rax, -4408(%rbp)
	jmp	.L442
.L489:
	leaq	ca_options(%rip), %rdi
	xorl	%r15d, %r15d
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	call	opt_help@PLT
	xorl	%edi, %edi
	call	BIO_free_all@PLT
	xorl	%edi, %edi
	call	BIO_free_all@PLT
	xorl	%edi, %edi
	call	BIO_free_all@PLT
	movq	X509_free@GOTPCREL(%rip), %rsi
	xorl	%edi, %edi
	call	OPENSSL_sk_pop_free@PLT
	movl	$0, -4448(%rbp)
	movq	$0, -4408(%rbp)
	movq	$0, -4416(%rbp)
	movq	$0, -4424(%rbp)
	jmp	.L495
.L981:
	movq	$0, -4408(%rbp)
	movq	bio_err(%rip), %rdi
	xorl	%r15d, %r15d
.L959:
	movq	$0, -4440(%rbp)
	xorl	%r14d, %r14d
	movq	$0, -4416(%rbp)
	movq	$0, -4432(%rbp)
	movq	$0, -4424(%rbp)
	movl	$0, -4468(%rbp)
	jmp	.L494
.L985:
	movq	bio_err(%rip), %rdi
	movq	%r15, %rdx
	leaq	.LC108(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	.p2align 4,,10
	.p2align 3
.L955:
	movq	$0, -4408(%rbp)
	xorl	%r15d, %r15d
	movq	bio_err(%rip), %rdi
	xorl	%r14d, %r14d
	movq	$0, -4440(%rbp)
	movq	$0, -4416(%rbp)
	movq	$0, -4432(%rbp)
	movq	$0, -4424(%rbp)
	jmp	.L494
.L731:
	call	ERR_clear_error@PLT
	jmp	.L503
.L983:
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	jmp	.L955
.L982:
	leaq	.LC104(%rip), %rdx
	leaq	.LC105(%rip), %rsi
	movq	%rbx, %rdi
	call	NCONF_get_string@PLT
	movq	%rax, -4408(%rbp)
	testq	%rax, %rax
	jne	.L501
	movq	bio_err(%rip), %rdi
	leaq	.LC104(%rip), %rcx
	xorl	%r15d, %r15d
	leaq	.LC105(%rip), %rdx
	leaq	.LC31(%rip), %rsi
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	jmp	.L959
.L513:
	cmpq	$0, -4424(%rbp)
	je	.L992
.L525:
	movq	-4352(%rbp), %r14
	movl	$0, -4468(%rbp)
	testq	%r14, %r14
	je	.L993
.L526:
	movq	-4464(%rbp), %r8
	movl	-4380(%rbp), %esi
	movq	%r14, %rcx
	xorl	%edx, %edx
	movq	-4424(%rbp), %rdi
	leaq	.LC123(%rip), %r9
	call	load_key@PLT
	movq	-4352(%rbp), %r14
	movq	%rax, -4424(%rbp)
	testq	%r14, %r14
	je	.L528
	movq	%r14, %rdi
	call	strlen@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	OPENSSL_cleanse@PLT
.L528:
	xorl	%r15d, %r15d
	cmpq	$0, -4424(%rbp)
	movq	bio_err(%rip), %rdi
	je	.L958
	movl	-4504(%rbp), %r14d
	cmpq	$0, -4496(%rbp)
	setne	-4592(%rbp)
	movzbl	-4592(%rbp), %eax
	testl	%r14d, %r14d
	je	.L530
	testb	%al, %al
	jne	.L530
	cmpq	$0, -4576(%rbp)
	jne	.L530
	movl	-4544(%rbp), %r11d
	xorl	%r15d, %r15d
	movq	$0, -4696(%rbp)
	testl	%r11d, %r11d
	jne	.L530
.L531:
	leaq	.LC127(%rip), %rdx
	leaq	.LC105(%rip), %rsi
	movq	%rbx, %rdi
	call	NCONF_get_string@PLT
	testq	%rax, %rax
	je	.L994
	movzbl	(%rax), %eax
	andl	$-33, %eax
	cmpb	$89, %al
	jne	.L537
	movl	$1, preserve(%rip)
.L537:
	leaq	.LC128(%rip), %rdx
	leaq	.LC105(%rip), %rsi
	movq	%rbx, %rdi
	call	NCONF_get_string@PLT
	testq	%rax, %rax
	je	.L995
	movzbl	(%rax), %eax
	andl	$-33, %eax
	cmpb	$89, %al
	jne	.L539
	movl	$1, msie_hack(%rip)
.L539:
	movq	-4408(%rbp), %rsi
	leaq	.LC129(%rip), %rdx
	movq	%rbx, %rdi
	call	NCONF_get_string@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L747
	movq	%rax, %rdi
	call	set_nameopt@PLT
	movl	$0, -4676(%rbp)
	testl	%eax, %eax
	je	.L996
.L540:
	movq	-4408(%rbp), %rsi
	leaq	.LC131(%rip), %rdx
	movq	%rbx, %rdi
	call	NCONF_get_string@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L541
	leaq	-4320(%rbp), %rdi
	movq	%rax, %rsi
	call	set_cert_ex@PLT
	movl	$0, -4676(%rbp)
	testl	%eax, %eax
	je	.L997
.L542:
	movq	-4408(%rbp), %rsi
	leaq	.LC133(%rip), %rdx
	movq	%rbx, %rdi
	call	NCONF_get_string@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L543
	leaq	-4384(%rbp), %rdi
	movq	%rax, %rsi
	call	set_ext_copy@PLT
	testl	%eax, %eax
	je	.L998
.L544:
	cmpq	$0, -4552(%rbp)
	jne	.L545
	testb	$1, %r12b
	je	.L545
	movq	-4408(%rbp), %rsi
	leaq	.LC135(%rip), %rdx
	movq	%rbx, %rdi
	call	NCONF_get_string@PLT
	movq	%rax, -4552(%rbp)
	testq	%rax, %rax
	je	.L999
	movq	%rax, %rdi
	call	app_isdir@PLT
	testl	%eax, %eax
	jle	.L1000
.L545:
	movq	-4408(%rbp), %rsi
	leaq	.LC112(%rip), %rdx
	movq	%rbx, %rdi
	call	NCONF_get_string@PLT
	movq	%rax, -4656(%rbp)
	testq	%rax, %rax
	je	.L1001
	movq	%rax, %rdi
	leaq	-4388(%rbp), %rsi
	xorl	%r14d, %r14d
	call	load_index@PLT
	leaq	-4296(%rbp), %rcx
	movq	%rax, -4416(%rbp)
	movq	%rcx, -4704(%rbp)
	testq	%rax, %rax
	je	.L976
	movq	%rbx, -4688(%rbp)
.L727:
	movq	-4416(%rbp), %rax
	movq	8(%rax), %rax
	movq	8(%rax), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r14d
	jge	.L1002
	movq	-4416(%rbp), %rax
	movl	%r14d, %esi
	movq	8(%rax), %rax
	movq	8(%rax), %rdi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %rbx
	movq	(%rax), %rax
	cmpb	$82, (%rax)
	je	.L548
	movq	16(%rbx), %rax
	cmpb	$0, (%rax)
	jne	.L1003
.L549:
	movq	8(%rbx), %rsi
	xorl	%edi, %edi
	call	ASN1_TIME_set_string@PLT
	testl	%eax, %eax
	je	.L1004
	movq	24(%rbx), %rbx
	movq	%rbx, %rdi
	call	strlen@PLT
	cmpb	$45, (%rbx)
	movl	%eax, %r8d
	jne	.L552
	addq	$1, %rbx
	leal	-1(%rax), %r8d
.L552:
	cmpl	$1, %r8d
	jle	.L553
	testb	$1, %r8b
	jne	.L553
	movzbl	(%rbx), %edx
	testb	%dl, %dl
	je	.L555
	movb	%dl, -4712(%rbp)
	call	__ctype_b_loc@PLT
	movzbl	-4712(%rbp), %edx
	movq	(%rax), %rsi
	jmp	.L557
	.p2align 4,,10
	.p2align 3
.L556:
	movzbl	1(%rbx), %edx
	addq	$1, %rbx
	testb	%dl, %dl
	je	.L555
.L557:
	movzbl	%dl, %eax
	testb	$16, 1(%rsi,%rax,2)
	jne	.L556
	movsbl	%dl, %ecx
	movq	bio_err(%rip), %rdi
	leal	1(%r14), %edx
	xorl	%eax, %eax
	movl	%ecx, %r8d
	leaq	.LC141(%rip), %rsi
	movq	-4688(%rbp), %rbx
	xorl	%r14d, %r14d
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	movq	$0, -4408(%rbp)
	movq	$0, -4440(%rbp)
	jmp	.L494
.L511:
	call	ERR_clear_error@PLT
	jmp	.L512
.L991:
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, -4456(%rbp)
	testq	%rax, %rax
	jne	.L497
	movq	$0, -4408(%rbp)
	xorl	%r15d, %r15d
	xorl	%r14d, %r14d
	movq	bio_err(%rip), %rdi
	movq	$0, -4440(%rbp)
	xorl	%ebx, %ebx
	movq	$0, -4416(%rbp)
	movq	$0, -4432(%rbp)
	movq	$0, -4424(%rbp)
	movl	$0, -4468(%rbp)
	jmp	.L494
.L987:
	movq	$4097, -4672(%rbp)
	jmp	.L508
.L989:
	movq	$0, -4408(%rbp)
	xorl	%r15d, %r15d
	movq	bio_err(%rip), %rdi
	xorl	%r14d, %r14d
	movq	$0, -4440(%rbp)
	movq	$0, -4432(%rbp)
	movq	$0, -4424(%rbp)
	movl	$0, -4468(%rbp)
	jmp	.L494
.L530:
	cmpq	$0, -4416(%rbp)
	je	.L1005
.L533:
	movq	-4416(%rbp), %rdi
	leaq	.LC125(%rip), %rdx
	movl	$32773, %esi
	call	load_cert@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L962
	movq	-4424(%rbp), %rsi
	movq	%rax, %rdi
	call	X509_check_private_key@PLT
	testl	%eax, %eax
	je	.L1006
	movl	-4504(%rbp), %r10d
	movl	$0, %eax
	testl	%r10d, %r10d
	cmove	%r15, %rax
	movq	%rax, -4696(%rbp)
	jmp	.L531
.L984:
	call	ERR_clear_error@PLT
	jmp	.L506
.L1001:
	movq	-4408(%rbp), %rdx
	movq	bio_err(%rip), %rdi
	leaq	.LC112(%rip), %rcx
	leaq	.LC31(%rip), %rsi
	call	BIO_printf@PLT
.L962:
	movq	bio_err(%rip), %rdi
.L958:
	movq	$0, -4408(%rbp)
	xorl	%r14d, %r14d
	movq	$0, -4440(%rbp)
	movq	$0, -4416(%rbp)
	movq	$0, -4432(%rbp)
	jmp	.L494
.L993:
	movq	-4592(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	leaq	-4352(%rbp), %rdx
	call	app_passwd@PLT
	testl	%eax, %eax
	je	.L527
	movl	$1, -4468(%rbp)
	movq	-4352(%rbp), %r14
	jmp	.L526
.L515:
	movq	-4432(%rbp), %r15
	movq	%r15, %rdi
	call	strlen@PLT
	pxor	%xmm0, %xmm0
	leaq	.LC113(%rip), %rsi
	leal	2(%rax), %edi
	movq	%rax, %r12
	movaps	%xmm0, -4224(%rbp)
	movaps	%xmm0, -4208(%rbp)
	movaps	%xmm0, -4192(%rbp)
	call	app_malloc@PLT
	movq	%rax, -4200(%rbp)
	movq	%rax, %rdi
	testb	$1, %r12b
	je	.L516
	movb	$48, (%rax)
	movq	-4200(%rbp), %rax
	movq	%r12, %rdx
	movq	%r15, %rsi
	leaq	1(%rax), %rdi
	call	memcpy@PLT
	movq	-4200(%rbp), %rax
	movb	$0, 1(%rax,%r12)
.L517:
	movq	-4200(%rbp), %rdi
	leaq	-4224(%rbp), %r12
	call	make_uppercase@PLT
	movq	-4416(%rbp), %rax
	movq	%r12, %rdx
	movl	$3, %esi
	movq	8(%rax), %rdi
	call	TXT_DB_get_by_index@PLT
	testq	%rax, %rax
	je	.L1007
	movq	(%rax), %rax
	movq	-4200(%rbp), %rdx
	movq	bio_err(%rip), %rdi
	movzbl	(%rax), %eax
	cmpb	$86, %al
	je	.L1008
	cmpb	$82, %al
	je	.L1009
	cmpb	$69, %al
	je	.L1010
	cmpb	$83, %al
	je	.L1011
	movsbl	%al, %ecx
	leaq	.LC119(%rip), %rsi
	xorl	%eax, %eax
	movl	$-1, %r13d
	call	BIO_printf@PLT
.L519:
	movq	%r12, %r14
	leaq	.LC11(%rip), %r15
	leaq	-4176(%rbp), %r12
.L524:
	movq	(%r14), %rdi
	movl	$2219, %edx
	movq	%r15, %rsi
	addq	$8, %r14
	call	CRYPTO_free@PLT
	cmpq	%r12, %r14
	jne	.L524
	movq	bio_err(%rip), %rdi
	cmpl	$1, %r13d
	je	.L743
	movq	-4432(%rbp), %rdx
	leaq	.LC120(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L980
.L988:
	movq	bio_err(%rip), %rdi
	movq	-4408(%rbp), %rdx
	xorl	%eax, %eax
	xorl	%r15d, %r15d
	leaq	.LC112(%rip), %rcx
	leaq	.LC31(%rip), %rsi
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	movq	$0, -4408(%rbp)
	movq	$0, -4440(%rbp)
	movq	$0, -4416(%rbp)
	movq	$0, -4432(%rbp)
	movq	$0, -4424(%rbp)
	movl	$0, -4468(%rbp)
	jmp	.L494
.L986:
	call	ERR_clear_error@PLT
	movq	$4097, -4672(%rbp)
	jmp	.L508
.L1005:
	movq	-4408(%rbp), %rsi
	leaq	.LC124(%rip), %rdx
	movq	%rbx, %rdi
	call	NCONF_get_string@PLT
	movq	%rax, -4416(%rbp)
	testq	%rax, %rax
	jne	.L533
	movq	bio_err(%rip), %rdi
	xorl	%r15d, %r15d
	xorl	%r14d, %r14d
	movq	-4408(%rbp), %rdx
	leaq	.LC124(%rip), %rcx
	leaq	.LC31(%rip), %rsi
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	movq	$0, -4408(%rbp)
	movq	$0, -4440(%rbp)
	movq	$0, -4432(%rbp)
	jmp	.L494
.L992:
	movq	-4408(%rbp), %rsi
	leaq	.LC121(%rip), %rdx
	movq	%rbx, %rdi
	call	NCONF_get_string@PLT
	movq	%rax, -4424(%rbp)
	testq	%rax, %rax
	jne	.L525
	movq	bio_err(%rip), %rdi
	xorl	%r15d, %r15d
	xorl	%r14d, %r14d
	movq	-4408(%rbp), %rdx
	leaq	.LC121(%rip), %rcx
	leaq	.LC31(%rip), %rsi
	call	BIO_printf@PLT
	movq	-4424(%rbp), %rax
	movq	$0, -4408(%rbp)
	movq	$0, -4440(%rbp)
	movq	bio_err(%rip), %rdi
	movq	$0, -4416(%rbp)
	movq	%rax, -4432(%rbp)
	movl	$0, -4468(%rbp)
	jmp	.L494
.L747:
	movl	$1, -4676(%rbp)
	jmp	.L540
.L555:
	addl	$1, %r14d
	jmp	.L727
.L541:
	call	ERR_clear_error@PLT
	jmp	.L542
.L516:
	movq	-4432(%rbp), %rsi
	movq	%r12, %rdx
	call	memcpy@PLT
	movq	-4200(%rbp), %rax
	movb	$0, (%rax,%r12)
	jmp	.L517
.L543:
	call	ERR_clear_error@PLT
	jmp	.L544
.L548:
	movq	16(%rbx), %r8
	movq	-4704(%rbp), %rcx
	leaq	-4304(%rbp), %rdx
	leaq	-4372(%rbp), %rsi
	leaq	-4288(%rbp), %rdi
	movl	$-1, -4372(%rbp)
	movq	$0, -4304(%rbp)
	movq	$0, -4296(%rbp)
	movq	$0, -4288(%rbp)
	call	unpack_revinfo
	testl	%eax, %eax
	je	.L550
	movl	$2447, %edx
	leaq	.LC11(%rip), %rsi
	xorl	%edi, %edi
	call	CRYPTO_free@PLT
	movq	-4304(%rbp), %rdi
	call	ASN1_OBJECT_free@PLT
	movq	-4296(%rbp), %rdi
	call	ASN1_GENERALIZEDTIME_free@PLT
	xorl	%edi, %edi
	call	ASN1_ENUMERATED_free@PLT
	movq	-4288(%rbp), %rdi
	call	ASN1_TIME_free@PLT
	jmp	.L549
.L995:
	call	ERR_clear_error@PLT
	jmp	.L539
.L994:
	call	ERR_clear_error@PLT
	jmp	.L537
.L1006:
	movq	bio_err(%rip), %rdi
	leaq	.LC126(%rip), %rsi
	call	BIO_printf@PLT
.L963:
	movq	$0, -4408(%rbp)
	movq	bio_err(%rip), %rdi
	xorl	%r14d, %r14d
	movq	$0, -4440(%rbp)
	movq	$0, -4416(%rbp)
	jmp	.L494
.L996:
	movq	bio_err(%rip), %rdi
	movq	%r14, %rdx
	leaq	.LC130(%rip), %rsi
	call	BIO_printf@PLT
	jmp	.L963
.L997:
	movq	bio_err(%rip), %rdi
	movq	%r14, %rdx
	leaq	.LC132(%rip), %rsi
	call	BIO_printf@PLT
	jmp	.L963
.L527:
	movq	bio_err(%rip), %rdi
	leaq	.LC122(%rip), %rsi
	xorl	%eax, %eax
	xorl	%r15d, %r15d
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	movq	$0, -4408(%rbp)
	movq	$0, -4440(%rbp)
	movq	$0, -4416(%rbp)
	movq	$0, -4432(%rbp)
	movq	$0, -4424(%rbp)
	movl	$1, -4468(%rbp)
	jmp	.L494
.L998:
	movq	bio_err(%rip), %rdi
	movq	%r14, %rdx
	leaq	.LC134(%rip), %rsi
	call	BIO_printf@PLT
	jmp	.L963
.L1030:
	movq	-4408(%rbp), %rsi
	leaq	.LC167(%rip), %rdx
	movq	%rbx, %rdi
	call	NCONF_get_string@PLT
	movq	%rax, -4528(%rbp)
	testq	%rax, %rax
	jne	.L596
	call	ERR_clear_error@PLT
	cmpq	$0, -4328(%rbp)
	jne	.L599
	movq	-4408(%rbp), %rsi
	leaq	-4328(%rbp), %rcx
	leaq	.LC169(%rip), %rdx
	movq	%rbx, %rdi
	call	NCONF_get_number_e@PLT
	testl	%eax, %eax
	jne	.L735
	movq	$0, -4328(%rbp)
.L600:
	movq	bio_err(%rip), %rdi
	leaq	.LC170(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L976:
	movq	$0, -4408(%rbp)
	movq	bio_err(%rip), %rdi
	xorl	%r14d, %r14d
	movq	$0, -4440(%rbp)
	movq	$0, -4432(%rbp)
	jmp	.L494
.L1008:
	movl	$86, %ecx
	leaq	.LC115(%rip), %rsi
	xorl	%eax, %eax
	movl	$1, %r13d
	call	BIO_printf@PLT
	jmp	.L519
.L1007:
	movq	-4200(%rbp), %rdx
	movq	bio_err(%rip), %rdi
	leaq	.LC114(%rip), %rsi
	movl	$-1, %r13d
	call	BIO_printf@PLT
	jmp	.L519
.L1011:
	movl	$83, %ecx
	leaq	.LC118(%rip), %rsi
	xorl	%eax, %eax
	movl	$1, %r13d
	call	BIO_printf@PLT
	jmp	.L519
.L999:
	movq	bio_err(%rip), %rdi
	leaq	.LC136(%rip), %rsi
	call	BIO_printf@PLT
	jmp	.L962
.L1000:
	movq	-4552(%rbp), %r14
	movq	-4656(%rbp), %rdx
	leaq	.LC137(%rip), %rsi
	xorl	%eax, %eax
	movq	bio_err(%rip), %rdi
	movq	%r14, %rcx
	call	BIO_printf@PLT
	movq	%r14, %rdi
	call	perror@PLT
	jmp	.L963
.L553:
	movq	bio_err(%rip), %rdi
	leal	1(%r14), %edx
	movl	%r8d, %ecx
	xorl	%eax, %eax
	leaq	.LC140(%rip), %rsi
	movq	-4688(%rbp), %rbx
	xorl	%r14d, %r14d
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	movq	$0, -4408(%rbp)
	movq	$0, -4440(%rbp)
	jmp	.L494
.L1010:
	movl	$69, %ecx
	leaq	.LC117(%rip), %rsi
	xorl	%eax, %eax
	movl	$1, %r13d
	call	BIO_printf@PLT
	jmp	.L519
.L1009:
	movl	$82, %ecx
	leaq	.LC116(%rip), %rsi
	xorl	%eax, %eax
	movl	$1, %r13d
	call	BIO_printf@PLT
	jmp	.L519
.L990:
	call	__stack_chk_fail@PLT
.L1004:
	movq	-4688(%rbp), %rbx
	movq	bio_err(%rip), %rdi
	leal	1(%r14), %edx
	leaq	.LC139(%rip), %rsi
.L966:
	call	BIO_printf@PLT
.L975:
	movq	$0, -4408(%rbp)
	movq	bio_err(%rip), %rdi
	xorl	%r14d, %r14d
	movq	$0, -4440(%rbp)
	jmp	.L494
.L1003:
	movq	-4688(%rbp), %rbx
	leal	1(%r14), %edx
	leaq	.LC138(%rip), %rsi
.L967:
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
	jmp	.L966
.L1002:
	movq	-4688(%rbp), %rbx
	testl	%r13d, %r13d
	jne	.L1012
.L559:
	movq	-4416(%rbp), %rdi
	call	index_index@PLT
	testl	%eax, %eax
	jle	.L975
	cmpl	$0, -4632(%rbp)
	je	.L562
	testl	%r13d, %r13d
	jne	.L1013
.L563:
	call	ASN1_UTCTIME_new@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L564
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	X509_gmtime_adj@PLT
	testq	%rax, %rax
	je	.L1014
	movl	(%r14), %eax
	leaq	.LC145(%rip), %rsi
	leal	1(%rax), %edi
	call	app_malloc@PLT
	movslq	(%r14), %rdx
	movq	8(%r14), %rsi
	movq	%rax, %rdi
	movq	%rax, -4688(%rbp)
	call	memcpy@PLT
	movq	-4688(%rbp), %rcx
	movslq	(%r14), %rax
	movl	$2, %edx
	leaq	.LC146(%rip), %rsi
	movb	$0, (%rcx,%rax)
	movq	%rcx, %rdi
	call	strncmp@PLT
	movq	%rbx, -4728(%rbp)
	movl	%eax, -4704(%rbp)
	xorl	%eax, %eax
	movl	$0, -4632(%rbp)
	movl	%eax, %ebx
	movl	%r12d, -4712(%rbp)
	movl	%r13d, -4720(%rbp)
	jmp	.L566
.L567:
	addl	$1, %ebx
.L566:
	movq	-4416(%rbp), %rax
	movq	8(%rax), %rax
	movq	8(%rax), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jge	.L1015
	movq	-4416(%rbp), %rax
	movl	%ebx, %esi
	movq	8(%rax), %rax
	movq	8(%rax), %rdi
	call	OPENSSL_sk_value@PLT
	movq	(%rax), %r13
	movq	%rax, %r12
	cmpb	$86, 0(%r13)
	jne	.L567
	movq	8(%rax), %rdi
	movl	$2, %edx
	leaq	.LC146(%rip), %rsi
	movq	%rdi, -4736(%rbp)
	call	strncmp@PLT
	movq	-4736(%rbp), %rdi
	testl	%eax, %eax
	jle	.L568
	cmpl	$0, -4704(%rbp)
	jg	.L715
.L569:
	movb	$69, 0(%r13)
	movq	(%r12), %rax
	leaq	.LC147(%rip), %rsi
	movb	$0, 1(%rax)
	movq	24(%r12), %rdx
	xorl	%eax, %eax
	movq	bio_err(%rip), %rdi
	addl	$1, -4632(%rbp)
	call	BIO_printf@PLT
	jmp	.L567
.L550:
	movl	$2447, %edx
	leaq	.LC11(%rip), %rsi
	xorl	%edi, %edi
	movq	-4688(%rbp), %rbx
	call	CRYPTO_free@PLT
	movq	-4304(%rbp), %rdi
	call	ASN1_OBJECT_free@PLT
	movq	-4296(%rbp), %rdi
	call	ASN1_GENERALIZEDTIME_free@PLT
	xorl	%edi, %edi
	call	ASN1_ENUMERATED_free@PLT
	movq	-4288(%rbp), %rdi
	call	ASN1_TIME_free@PLT
	leal	1(%r14), %edx
	leaq	.LC206(%rip), %rsi
	jmp	.L967
.L1015:
	movq	%r14, %rdi
	movl	-4712(%rbp), %r12d
	movl	-4720(%rbp), %r13d
	movq	-4728(%rbp), %rbx
	call	ASN1_UTCTIME_free@PLT
	movq	-4688(%rbp), %rdi
	movl	$2281, %edx
	leaq	.LC11(%rip), %rsi
	call	CRYPTO_free@PLT
	cmpl	$0, -4632(%rbp)
	jne	.L1016
	testl	%r13d, %r13d
	jne	.L1017
.L562:
	movq	-4600(%rbp), %rax
	testq	%rax, %rax
	je	.L576
	movq	%rax, %rdi
	call	app_load_config@PLT
	movq	%rax, extconf(%rip)
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L979
	testl	%r13d, %r13d
	jne	.L1018
.L578:
	cmpq	$0, -4488(%rbp)
	je	.L1019
.L576:
	movl	-4544(%rbp), %eax
	orl	%r12d, %eax
	movl	%eax, -4600(%rbp)
	je	.L579
	cmpq	$0, -4584(%rbp)
	je	.L761
	cmpb	$0, -4592(%rbp)
	je	.L761
	movl	-4600(%rbp), %eax
	movl	%eax, -4448(%rbp)
.L579:
	movq	-4424(%rbp), %rdi
	leaq	-4376(%rbp), %rsi
	call	EVP_PKEY_get_default_digest_nid@PLT
	movl	%eax, %r14d
	cmpl	$2, %eax
	je	.L1020
.L581:
	cmpq	$0, -4440(%rbp)
	je	.L1021
.L583:
	movq	-4440(%rbp), %rdi
	leaq	.LC100(%rip), %rsi
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L584
	testl	%r14d, %r14d
	jle	.L1022
	movl	-4376(%rbp), %edi
	call	OBJ_nid2sn@PLT
	movq	%rax, -4440(%rbp)
.L584:
	movq	-4440(%rbp), %rdi
	leaq	-4360(%rbp), %rsi
	call	opt_md@PLT
	testl	%eax, %eax
	je	.L975
.L582:
	testl	%r12d, %r12d
	je	.L751
	cmpl	$1, -4540(%rbp)
	je	.L1023
.L587:
	testl	%r13d, %r13d
	jne	.L1024
	cmpq	$0, -4536(%rbp)
	je	.L1025
.L737:
	movq	-4408(%rbp), %rsi
	leaq	.LC161(%rip), %rdx
	movq	%rbx, %rdi
	call	NCONF_get_string@PLT
	testq	%rax, %rax
	je	.L1026
	movl	%r12d, -4608(%rbp)
	movq	$0, -4632(%rbp)
.L591:
	movq	extconf(%rip), %r14
	testq	%r14, %r14
	je	.L1027
.L592:
	cmpq	$0, -4512(%rbp)
	je	.L1028
.L594:
	movq	-4512(%rbp), %rsi
	xorl	%edi, %edi
	call	ASN1_TIME_set_string_X509@PLT
	testl	%eax, %eax
	je	.L1029
.L595:
	cmpq	$0, -4528(%rbp)
	je	.L1030
.L596:
	movq	-4528(%rbp), %rsi
	xorl	%edi, %edi
	call	ASN1_TIME_set_string_X509@PLT
	testl	%eax, %eax
	je	.L1031
	cmpq	$0, -4328(%rbp)
	je	.L1032
.L599:
	cmpl	$0, -4608(%rbp)
	je	.L601
	call	BN_new@PLT
	movq	%rax, -4368(%rbp)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L603
	xorl	%esi, %esi
	call	rand_serial@PLT
	testl	%eax, %eax
	je	.L603
.L604:
	movq	-4536(%rbp), %rsi
	movq	%rbx, %rdi
	call	NCONF_get_section@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L1033
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, -4440(%rbp)
	testq	%rax, %rax
	je	.L1034
	cmpq	$0, -4496(%rbp)
	je	.L753
	movl	-4384(%rbp), %ecx
	movl	%ecx, -4688(%rbp)
	call	get_nameopt@PLT
	movq	-4320(%rbp), %rcx
	movq	-4496(%rbp), %rsi
	xorl	%edi, %edi
	leaq	-4288(%rbp), %rdx
	movq	%rax, -4704(%rbp)
	movq	%rcx, -4712(%rbp)
	movq	-4328(%rbp), %rcx
	movq	%rcx, -4720(%rbp)
	movq	-4368(%rbp), %rcx
	movq	%rcx, -4728(%rbp)
	movq	-4360(%rbp), %rcx
	movq	%rcx, -4736(%rbp)
	call	CONF_load@PLT
	movq	%rax, -4608(%rbp)
	testq	%rax, %rax
	je	.L1035
	movq	-4608(%rbp), %rdi
	leaq	.LC100(%rip), %rsi
	call	CONF_get_section@PLT
	movq	%rax, %rdi
	movq	%rax, -4744(%rbp)
	call	OPENSSL_sk_num@PLT
	testl	%eax, %eax
	je	.L1036
	call	X509_REQ_new@PLT
	movq	%rax, -4592(%rbp)
	testq	%rax, %rax
	je	.L1037
	movq	-4592(%rbp), %rdi
	call	X509_REQ_get_subject_name@PLT
	movl	%r12d, -4680(%rbp)
	movq	$0, -4536(%rbp)
	movq	%rax, -4752(%rbp)
	xorl	%eax, %eax
	movq	%rbx, -4640(%rbp)
	movl	%eax, %ebx
.L622:
	movq	-4744(%rbp), %r12
	movq	%r12, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%ebx, %eax
	jle	.L616
	movq	%r12, %rdi
	movl	%ebx, %esi
	call	OPENSSL_sk_value@PLT
	movq	8(%rax), %rdi
	movq	%rax, %r12
	movzbl	(%rdi), %eax
	testb	%al, %al
	je	.L617
	movq	%rdi, %rcx
	jmp	.L619
.L618:
	testb	%al, %al
	je	.L617
.L619:
	movl	%eax, %edx
	andl	$-3, %edx
	cmpb	$44, %dl
	sete	%sil
	cmpb	$58, %al
	movzbl	1(%rcx), %eax
	sete	%dl
	addq	$1, %rcx
	orb	%dl, %sil
	je	.L618
	testb	%al, %al
	cmovne	%rcx, %rdi
.L617:
	movq	16(%r12), %rcx
	movq	%rdi, -4760(%rbp)
	movq	%rcx, -4768(%rbp)
	call	OBJ_txt2nid@PLT
	movq	-4768(%rbp), %rcx
	testl	%eax, %eax
	movl	%eax, %esi
	jne	.L620
	movq	-4760(%rbp), %rdi
	leaq	.LC178(%rip), %rsi
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L621
	movq	16(%r12), %rdi
	orl	$-1, %esi
	call	NETSCAPE_SPKI_b64_decode@PLT
	movq	%rax, -4536(%rbp)
	testq	%rax, %rax
	je	.L1038
.L621:
	addl	$1, %ebx
	jmp	.L622
.L1021:
	movq	-4408(%rbp), %rsi
	leaq	.LC155(%rip), %rdx
	movq	%rbx, %rdi
	call	lookup_conf
	movq	%rax, -4440(%rbp)
	testq	%rax, %rax
	jne	.L583
.L973:
	movq	$0, -4408(%rbp)
	movq	bio_err(%rip), %rdi
	xorl	%r14d, %r14d
	movq	$0, -4432(%rbp)
	jmp	.L494
.L620:
	pushq	%rdi
	movl	-4672(%rbp), %edx
	orl	$-1, %r9d
	orl	$-1, %r8d
	pushq	$0
	movq	-4752(%rbp), %rdi
	call	X509_NAME_add_entry_by_NID@PLT
	popq	%r8
	popq	%r9
	testl	%eax, %eax
	jne	.L621
	movq	-4640(%rbp), %rbx
.L613:
	movq	-4592(%rbp), %rdi
	call	X509_REQ_free@PLT
	movq	-4608(%rbp), %rdi
	call	CONF_free@PLT
	movq	-4536(%rbp), %rdi
	call	NETSCAPE_SPKI_free@PLT
	xorl	%edi, %edi
	call	X509_NAME_ENTRY_free@PLT
.L978:
	movq	$0, -4408(%rbp)
	movq	bio_err(%rip), %rdi
	xorl	%r14d, %r14d
	jmp	.L494
.L1038:
	movq	bio_err(%rip), %rdi
	leaq	.LC179(%rip), %rsi
	movq	-4640(%rbp), %rbx
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	jmp	.L613
.L616:
	cmpq	$0, -4536(%rbp)
	movl	-4680(%rbp), %r12d
	movq	-4640(%rbp), %rbx
	movq	bio_err(%rip), %rdi
	je	.L1039
	xorl	%eax, %eax
	leaq	.LC181(%rip), %rsi
	call	BIO_printf@PLT
	movq	-4536(%rbp), %rdi
	call	NETSCAPE_SPKI_get_pubkey@PLT
	testq	%rax, %rax
	je	.L1040
	movq	-4536(%rbp), %rdi
	movq	%rax, %rsi
	movq	%rax, -4496(%rbp)
	call	NETSCAPE_SPKI_verify@PLT
	movq	-4496(%rbp), %r8
	testl	%eax, %eax
	jle	.L1041
	movq	bio_err(%rip), %rdi
	leaq	.LC92(%rip), %rsi
	xorl	%eax, %eax
	movq	%r8, -4496(%rbp)
	call	BIO_printf@PLT
	movq	-4496(%rbp), %r8
	movq	-4592(%rbp), %rdi
	movq	%r8, %rsi
	call	X509_REQ_set_pubkey@PLT
	movq	-4496(%rbp), %r8
	movq	%r8, %rdi
	call	EVP_PKEY_free@PLT
	movl	-4688(%rbp), %eax
	pushq	$0
	movq	%r14, %r9
	movq	%r15, %rdx
	leaq	-4312(%rbp), %rdi
	pushq	%rax
	movl	-4676(%rbp), %eax
	pushq	%rax
	movl	-4540(%rbp), %eax
	pushq	-4704(%rbp)
	pushq	-4712(%rbp)
	pushq	%rbx
	pushq	-4488(%rbp)
	pushq	-4592(%rbp)
	pushq	%r13
	pushq	$1
	pushq	-4720(%rbp)
	pushq	-4528(%rbp)
	pushq	-4512(%rbp)
	pushq	%rax
	movl	-4556(%rbp), %eax
	pushq	%rax
	pushq	-4672(%rbp)
	pushq	-4568(%rbp)
	pushq	-4728(%rbp)
	movq	-4416(%rbp), %rcx
	movq	-4456(%rbp), %r8
	movq	-4424(%rbp), %rsi
	leaq	8(%rcx), %rax
	pushq	%rax
	pushq	%rcx
	movq	-4736(%rbp), %rcx
	call	do_body.isra.0
	movq	-4592(%rbp), %rdi
	addq	$160, %rsp
	movl	%eax, -4496(%rbp)
	call	X509_REQ_free@PLT
	movq	-4608(%rbp), %rdi
	call	CONF_free@PLT
	movq	-4536(%rbp), %rdi
	call	NETSCAPE_SPKI_free@PLT
	xorl	%edi, %edi
	call	X509_NAME_ENTRY_free@PLT
	cmpl	$0, -4496(%rbp)
	js	.L978
	jne	.L1042
.L611:
	cmpq	$0, -4576(%rbp)
	je	.L628
	movl	-4384(%rbp), %ecx
	addl	$1, %r12d
	movl	%ecx, -4592(%rbp)
	call	get_nameopt@PLT
	movq	-4320(%rbp), %rcx
	movl	$32773, %esi
	movq	-4576(%rbp), %rdx
	movq	%rax, -4608(%rbp)
	movq	%rcx, -4640(%rbp)
	movq	-4328(%rbp), %rcx
	movq	%rdx, %rdi
	movq	%rcx, -4688(%rbp)
	movq	-4368(%rbp), %rcx
	movq	%rcx, -4704(%rbp)
	movq	-4360(%rbp), %rcx
	movq	%rcx, -4712(%rbp)
	call	load_cert@PLT
	movq	%rax, -4536(%rbp)
	testq	%rax, %rax
	je	.L632
	testl	%r13d, %r13d
	jne	.L1043
.L630:
	movq	bio_err(%rip), %rdi
	leaq	.LC87(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	-4536(%rbp), %rdi
	call	X509_get0_pubkey@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1044
	movq	-4536(%rbp), %rdi
	call	X509_verify@PLT
	testl	%eax, %eax
	js	.L1045
	movq	bio_err(%rip), %rdi
	je	.L1046
	leaq	.LC92(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	-4536(%rbp), %rdi
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	X509_to_X509_REQ@PLT
	movq	%rax, %r10
	testq	%rax, %rax
	je	.L632
	movl	-4592(%rbp), %eax
	pushq	$0
	movq	%r14, %r9
	movq	%r15, %rdx
	movq	%r10, -4592(%rbp)
	leaq	-4312(%rbp), %rdi
	pushq	%rax
	movl	-4676(%rbp), %eax
	pushq	%rax
	movl	-4448(%rbp), %eax
	pushq	-4608(%rbp)
	pushq	-4640(%rbp)
	pushq	%rbx
	pushq	-4488(%rbp)
	pushq	%r10
	pushq	%r13
	pushq	%rax
	movl	-4540(%rbp), %eax
	pushq	-4688(%rbp)
	pushq	-4528(%rbp)
	pushq	-4512(%rbp)
	pushq	%rax
	movl	-4556(%rbp), %eax
	pushq	%rax
	pushq	-4672(%rbp)
	pushq	-4568(%rbp)
	pushq	-4704(%rbp)
	movq	-4416(%rbp), %rcx
	movq	-4456(%rbp), %r8
	movq	-4424(%rbp), %rsi
	leaq	8(%rcx), %rax
	pushq	%rax
	pushq	%rcx
	movq	-4712(%rbp), %rcx
	call	do_body.isra.0
	movq	-4592(%rbp), %r10
	addq	$160, %rsp
	movl	%eax, -4576(%rbp)
	movq	%r10, %rdi
	call	X509_REQ_free@PLT
	movq	-4536(%rbp), %rdi
	call	X509_free@PLT
	movl	-4576(%rbp), %eax
	testl	%eax, %eax
	js	.L978
	je	.L628
	movq	bio_err(%rip), %rdi
	leaq	.LC78(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	-4368(%rbp), %rdi
	movl	$1, %esi
	call	BN_add_word@PLT
	testl	%eax, %eax
	je	.L978
	movq	-4312(%rbp), %rsi
	movq	-4440(%rbp), %rdi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L638
	addl	$1, -4496(%rbp)
.L628:
	cmpq	$0, -4480(%rbp)
	je	.L639
	movl	-4384(%rbp), %edx
	addl	$1, %r12d
	movl	%edx, -4536(%rbp)
	call	get_nameopt@PLT
	movl	-4536(%rbp), %edx
	leaq	-4312(%rbp), %rdi
	pushq	%rcx
	movl	-4504(%rbp), %ecx
	pushq	%rcx
	movl	-4676(%rbp), %ecx
	pushq	%rdx
	pushq	%rcx
	movl	-4448(%rbp), %ecx
	pushq	%rax
	pushq	-4320(%rbp)
	pushq	%r13
	pushq	%rbx
	pushq	-4488(%rbp)
	pushq	%rcx
	movl	-4540(%rbp), %ecx
	pushq	-4328(%rbp)
	pushq	-4528(%rbp)
	pushq	-4512(%rbp)
	pushq	%rcx
	movl	-4556(%rbp), %ecx
	pushq	%rcx
	pushq	-4672(%rbp)
	pushq	-4568(%rbp)
	pushq	-4368(%rbp)
	pushq	-4416(%rbp)
	movq	-4456(%rbp), %r9
	pushq	%r14
	movq	-4360(%rbp), %r8
	movq	-4696(%rbp), %rcx
	movq	-4424(%rbp), %rdx
	movq	-4480(%rbp), %rsi
	call	certify
	addq	$160, %rsp
	testl	%eax, %eax
	js	.L978
	jne	.L1047
.L639:
	xorl	%ecx, %ecx
	leaq	-4312(%rbp), %rax
	movq	%r15, -4536(%rbp)
	movq	%rax, -4576(%rbp)
	movq	%rcx, %r15
	jmp	.L646
.L644:
	addq	$1, %r15
.L646:
	leal	(%r12,%r15), %ecx
	cmpl	%r15d, -4648(%rbp)
	jle	.L1048
	movl	-4384(%rbp), %edx
	movl	%edx, -4592(%rbp)
	call	get_nameopt@PLT
	movq	-4664(%rbp), %rcx
	movq	(%rcx,%r15,8), %rsi
	movl	-4504(%rbp), %ecx
	pushq	%rdx
	movl	-4592(%rbp), %edx
	pushq	%rcx
	movl	-4676(%rbp), %ecx
	pushq	%rdx
	pushq	%rcx
	pushq	%rax
	movl	-4448(%rbp), %eax
	pushq	-4320(%rbp)
	pushq	%r13
	pushq	%rbx
	pushq	-4488(%rbp)
	pushq	%rax
	movl	-4540(%rbp), %eax
	pushq	-4328(%rbp)
	pushq	-4528(%rbp)
	pushq	-4512(%rbp)
	pushq	%rax
	movl	-4556(%rbp), %eax
	pushq	%rax
	pushq	-4672(%rbp)
	pushq	-4568(%rbp)
	pushq	-4368(%rbp)
	movq	-4456(%rbp), %r9
	pushq	-4416(%rbp)
	movq	-4360(%rbp), %r8
	pushq	%r14
	movq	-4696(%rbp), %rcx
	movq	-4424(%rbp), %rdx
	movq	-4576(%rbp), %rdi
	call	certify
	addq	$160, %rsp
	testl	%eax, %eax
	js	.L1049
	je	.L644
	movq	bio_err(%rip), %rdi
	leaq	.LC78(%rip), %rsi
	xorl	%eax, %eax
	addl	$1, -4496(%rbp)
	call	BIO_printf@PLT
	movq	-4368(%rbp), %rdi
	movl	$1, %esi
	call	BN_add_word@PLT
	testl	%eax, %eax
	je	.L1050
	movq	-4312(%rbp), %rsi
	movq	-4440(%rbp), %rdi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	jne	.L644
	movq	bio_err(%rip), %rdi
	leaq	.LC13(%rip), %rsi
	movq	-4536(%rbp), %r15
	call	BIO_printf@PLT
.L968:
	movq	-4312(%rbp), %rdi
	call	X509_free@PLT
	jmp	.L978
.L1041:
	movq	%r8, %rdi
	call	EVP_PKEY_free@PLT
	movq	bio_err(%rip), %rdi
	leaq	.LC183(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L613
.L1042:
	movq	bio_err(%rip), %rdi
	leaq	.LC78(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	-4368(%rbp), %rdi
	movl	$1, %esi
	call	BN_add_word@PLT
	testl	%eax, %eax
	je	.L978
	movq	-4312(%rbp), %rsi
	movq	-4440(%rbp), %rdi
	call	OPENSSL_sk_push@PLT
	movl	%r12d, -4496(%rbp)
	testl	%eax, %eax
	jne	.L611
	movq	bio_err(%rip), %rdi
	leaq	.LC13(%rip), %rsi
	xorl	%r14d, %r14d
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	movq	$0, -4408(%rbp)
	jmp	.L494
.L1047:
	movq	bio_err(%rip), %rdi
	leaq	.LC78(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	-4368(%rbp), %rdi
	movl	$1, %esi
	call	BN_add_word@PLT
	testl	%eax, %eax
	je	.L978
	movq	-4312(%rbp), %rsi
	movq	-4440(%rbp), %rdi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L642
	addl	$1, -4496(%rbp)
	jmp	.L639
.L1040:
	movq	bio_err(%rip), %rdi
	leaq	.LC182(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L613
.L1049:
	movq	$0, -4408(%rbp)
	movq	-4536(%rbp), %r15
	xorl	%r14d, %r14d
	movq	bio_err(%rip), %rdi
	jmp	.L494
.L1048:
	movq	-4440(%rbp), %rdi
	movl	%ecx, -4488(%rbp)
	movq	-4536(%rbp), %r15
	call	OPENSSL_sk_num@PLT
	testl	%eax, %eax
	jle	.L648
	cmpl	$0, -4448(%rbp)
	jne	.L649
	movl	-4488(%rbp), %ecx
	movl	-4496(%rbp), %edx
	leaq	.LC185(%rip), %rsi
	xorl	%eax, %eax
	movq	bio_err(%rip), %rdi
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	call	BIO_ctrl@PLT
	movq	stdin(%rip), %rdx
	leaq	-4171(%rbp), %rdi
	movl	$11, %esi
	movb	$0, -4171(%rbp)
	call	fgets@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L1051
	movzbl	-4171(%rbp), %eax
	andl	$-33, %eax
	cmpb	$89, %al
	jne	.L1052
.L649:
	movq	-4440(%rbp), %rdi
	call	OPENSSL_sk_num@PLT
	movq	bio_err(%rip), %rdi
	leaq	.LC188(%rip), %rsi
	movl	%eax, %edx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	-4632(%rbp), %rax
	testq	%rax, %rax
	je	.L652
	movq	-4368(%rbp), %rdx
	xorl	%ecx, %ecx
	leaq	.LC150(%rip), %rsi
	movq	%rax, %rdi
	call	save_serial@PLT
	testl	%eax, %eax
	je	.L978
.L652:
	movq	-4416(%rbp), %rdx
	movq	-4656(%rbp), %rdi
	leaq	.LC150(%rip), %rsi
	call	save_index@PLT
	testl	%eax, %eax
	je	.L978
.L648:
	leaq	-4160(%rbp), %rax
	movq	-4552(%rbp), %rsi
	movl	$4096, %edx
	movq	%rax, %r14
	movq	%rax, %rdi
	movq	%rax, -4448(%rbp)
	call	OPENSSL_strlcpy@PLT
	movl	$4096, %edx
	leaq	.LC189(%rip), %rsi
	movq	%r14, %rdi
	call	OPENSSL_strlcat@PLT
	movq	%rax, -4512(%rbp)
	testl	%r13d, %r13d
	jne	.L1053
.L653:
	cmpl	$1, -4600(%rbp)
	movq	%rbx, -4488(%rbp)
	movl	%r13d, %ebx
	sbbl	%eax, %eax
	xorl	%edx, %edx
	movq	%r15, -4496(%rbp)
	andl	$32765, %eax
	movl	%edx, %r15d
	addl	$4, %eax
	movl	%eax, -4528(%rbp)
	movq	-4448(%rbp), %rax
	addq	-4512(%rbp), %rax
	movq	%rax, -4504(%rbp)
	addq	$2, %rax
	movq	%rax, -4536(%rbp)
	jmp	.L664
.L655:
	movq	-4504(%rbp), %rcx
	movq	-4536(%rbp), %rax
	movw	$12336, (%rcx)
.L657:
	movl	$1835364398, (%rax)
	movb	$0, 4(%rax)
	testl	%ebx, %ebx
	jne	.L1054
.L658:
	movl	-4528(%rbp), %edx
	movq	-4584(%rbp), %rdi
	movl	$119, %esi
	call	bio_open_default@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L1055
	movq	-4448(%rbp), %rdi
	leaq	.LC193(%rip), %rsi
	call	BIO_new_file@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1056
	cmpl	$0, -4560(%rbp)
	movq	%r12, %rsi
	movq	%rax, %rdi
	je	.L1057
	call	PEM_write_bio_X509@PLT
	cmpl	$0, -4600(%rbp)
	jne	.L716
.L717:
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	PEM_write_bio_X509@PLT
.L663:
	movq	%r13, %rdi
	addl	$1, %r15d
	call	BIO_free_all@PLT
	movq	%r14, %rdi
	call	BIO_free_all@PLT
.L664:
	movq	-4440(%rbp), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r15d
	jge	.L1058
	movq	-4440(%rbp), %rdi
	movl	%r15d, %esi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	X509_get_serialNumber@PLT
	movq	%rax, %r14
	movq	%rax, %rdi
	call	ASN1_STRING_get0_data@PLT
	movq	%r14, %rdi
	movq	%rax, %r13
	call	ASN1_STRING_length@PLT
	movl	$1, %edx
	testl	%eax, %eax
	cmovg	%eax, %edx
	leal	5(%rdx,%rdx), %edx
	movslq	%edx, %rdx
	addq	-4512(%rbp), %rdx
	cmpq	$4096, %rdx
	ja	.L1059
	testl	%eax, %eax
	jle	.L655
	movq	-4504(%rbp), %rsi
	xorl	%edx, %edx
.L656:
	movzbl	0(%r13,%rdx), %ecx
	leaq	HEX_DIGITS.24968(%rip), %rdi
	shrb	$4, %cl
	andl	$15, %ecx
	movzbl	(%rdi,%rcx), %ecx
	movb	%cl, (%rsi,%rdx,2)
	movzbl	0(%r13,%rdx), %ecx
	andl	$15, %ecx
	movzbl	(%rdi,%rcx), %ecx
	movb	%cl, 1(%rsi,%rdx,2)
	addq	$1, %rdx
	cmpl	%edx, %eax
	jg	.L656
	movq	-4504(%rbp), %rcx
	subl	$1, %eax
	leaq	2(%rcx,%rax,2), %rax
	jmp	.L657
.L1052:
	movq	bio_err(%rip), %rdi
	leaq	.LC187(%rip), %rsi
	xorl	%eax, %eax
	xorl	%r14d, %r14d
	call	BIO_printf@PLT
	movq	$0, -4408(%rbp)
	jmp	.L651
.L642:
	leaq	.LC13(%rip), %rsi
.L964:
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L978
.L716:
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	i2d_X509_bio@PLT
	jmp	.L663
.L1051:
	movq	bio_err(%rip), %rdi
	leaq	.LC186(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	$0, -4408(%rbp)
	movq	$0, -4432(%rbp)
	jmp	.L651
.L1050:
	movq	-4536(%rbp), %r15
	jmp	.L968
.L1057:
	call	X509_print@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	PEM_write_bio_X509@PLT
	cmpl	$0, -4600(%rbp)
	jne	.L716
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	X509_print@PLT
	jmp	.L717
.L1056:
	movq	-4448(%rbp), %rdi
	movq	-4488(%rbp), %rbx
	movq	-4496(%rbp), %r15
	call	perror@PLT
	movq	%r14, -4432(%rbp)
	xorl	%r14d, %r14d
	movq	bio_err(%rip), %rdi
	movq	$0, -4408(%rbp)
	jmp	.L494
.L1055:
	movq	-4488(%rbp), %rbx
	movq	-4496(%rbp), %r15
	movq	$0, -4408(%rbp)
	movq	$0, -4432(%rbp)
	movq	bio_err(%rip), %rdi
	jmp	.L494
.L1054:
	movq	-4448(%rbp), %rdx
	movq	bio_err(%rip), %rdi
	leaq	.LC192(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L658
.L1059:
	movq	-4488(%rbp), %rbx
	movq	-4496(%rbp), %r15
	leaq	.LC191(%rip), %rsi
	jmp	.L964
.L1058:
	movq	-4440(%rbp), %rdi
	movl	%ebx, %r13d
	movq	-4496(%rbp), %r15
	movq	-4488(%rbp), %rbx
	call	OPENSSL_sk_num@PLT
	testl	%eax, %eax
	je	.L586
	movq	-4632(%rbp), %rax
	testq	%rax, %rax
	je	.L666
	leaq	.LC151(%rip), %rdx
	leaq	.LC150(%rip), %rsi
	movq	%rax, %rdi
	call	rotate_serial@PLT
	testl	%eax, %eax
	je	.L978
.L666:
	movq	-4656(%rbp), %rdi
	leaq	.LC151(%rip), %rdx
	leaq	.LC150(%rip), %rsi
	call	rotate_index@PLT
	testl	%eax, %eax
	je	.L978
	movq	bio_err(%rip), %rdi
	leaq	.LC194(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L586:
	cmpl	$0, -4544(%rbp)
	je	.L755
	cmpq	$0, -4520(%rbp)
	je	.L1060
.L669:
	leaq	-4288(%rbp), %r12
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	movl	$1, %r9d
	movq	%r12, %rdi
	call	X509V3_set_ctx@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	X509V3_set_nconf@PLT
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	-4520(%rbp), %rdx
	call	X509V3_EXT_add_nconf@PLT
	testl	%eax, %eax
	je	.L1061
.L670:
	movq	-4408(%rbp), %rsi
	movq	%rbx, %rdi
	leaq	.LC197(%rip), %rdx
	call	NCONF_get_string@PLT
	movq	%rax, -4496(%rbp)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L756
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	load_serial@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L1062
.L671:
	movq	-4344(%rbp), %rax
	orq	-4336(%rbp), %rax
	orq	-4616(%rbp), %rax
	je	.L1063
.L672:
	testl	%r13d, %r13d
	jne	.L1064
.L675:
	call	X509_CRL_new@PLT
	movq	%rax, -4408(%rbp)
	testq	%rax, %rax
	je	.L956
	movq	%r15, %rdi
	call	X509_get_subject_name@PLT
	movq	-4408(%rbp), %rdi
	movq	%rax, %rsi
	call	X509_CRL_set_issuer_name@PLT
	testl	%eax, %eax
	je	.L957
	call	ASN1_TIME_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L679
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	X509_gmtime_adj@PLT
	testq	%rax, %rax
	je	.L679
	movq	-4408(%rbp), %rdi
	movq	%r12, %rsi
	call	X509_CRL_set1_lastUpdate@PLT
	testl	%eax, %eax
	je	.L679
	movl	-4344(%rbp), %esi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	imulq	$3600, -4336(%rbp), %rdx
	addq	-4616(%rbp), %rdx
	call	X509_time_adj_ex@PLT
	testq	%rax, %rax
	je	.L679
	movq	-4408(%rbp), %rdi
	movq	%r12, %rsi
	call	X509_CRL_set1_nextUpdate@PLT
	movq	%r12, %rdi
	call	ASN1_TIME_free@PLT
	xorl	%ecx, %ecx
	leaq	-4296(%rbp), %rax
	movq	%r14, -4488(%rbp)
	movl	$0, -4504(%rbp)
	movl	%ecx, %r14d
	movq	%rax, -4512(%rbp)
	movl	%r13d, -4528(%rbp)
	movq	%rbx, -4448(%rbp)
	jmp	.L680
.L681:
	addl	$1, %r14d
.L680:
	movq	-4416(%rbp), %rax
	movq	8(%rax), %rax
	movq	8(%rax), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r14d
	jge	.L1065
	movq	-4416(%rbp), %rax
	movl	%r14d, %esi
	movq	8(%rax), %rax
	movq	8(%rax), %rdi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %rbx
	movq	(%rax), %rax
	cmpb	$82, (%rax)
	jne	.L681
	call	X509_REVOKED_new@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L953
	movq	16(%rbx), %r8
	movq	-4512(%rbp), %rcx
	leaq	-4304(%rbp), %rdx
	leaq	-4372(%rbp), %rsi
	leaq	-4288(%rbp), %rdi
	movl	$-1, -4372(%rbp)
	movq	$0, -4304(%rbp)
	movq	$0, -4296(%rbp)
	movq	$0, -4288(%rbp)
	call	unpack_revinfo
	testl	%eax, %eax
	je	.L683
	movq	-4288(%rbp), %rsi
	movq	%r13, %rdi
	call	X509_REVOKED_set_revocationDate@PLT
	testl	%eax, %eax
	je	.L683
	xorl	%r12d, %r12d
	cmpl	$-1, -4372(%rbp)
	je	.L684
	call	ASN1_ENUMERATED_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L683
	movslq	-4372(%rbp), %rsi
	movq	%rax, %rdi
	call	ASN1_ENUMERATED_set@PLT
	testl	%eax, %eax
	je	.L946
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r12, %rdx
	movl	$141, %esi
	movq	%r13, %rdi
	call	X509_REVOKED_add1_ext_i2d@PLT
	testl	%eax, %eax
	je	.L946
.L684:
	movq	-4296(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L689
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$142, %esi
	movq	%r13, %rdi
	call	X509_REVOKED_add1_ext_i2d@PLT
	testl	%eax, %eax
	je	.L946
.L689:
	movq	-4304(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L688
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$430, %esi
	movq	%r13, %rdi
	call	X509_REVOKED_add1_ext_i2d@PLT
	testl	%eax, %eax
	je	.L946
.L688:
	cmpl	$-1, -4372(%rbp)
	je	.L1066
	movl	$2447, %edx
	leaq	.LC11(%rip), %rsi
	xorl	%edi, %edi
	call	CRYPTO_free@PLT
	movq	-4304(%rbp), %rdi
	call	ASN1_OBJECT_free@PLT
	movq	-4296(%rbp), %rdi
	call	ASN1_GENERALIZEDTIME_free@PLT
	movq	%r12, %rdi
	call	ASN1_ENUMERATED_free@PLT
	movq	-4288(%rbp), %rdi
	call	ASN1_TIME_free@PLT
	movl	-4544(%rbp), %eax
	movl	%eax, -4504(%rbp)
.L733:
	movq	24(%rbx), %rsi
	leaq	-4368(%rbp), %rdi
	call	BN_hex2bn@PLT
	testl	%eax, %eax
	je	.L1067
	movq	-4368(%rbp), %rdi
	xorl	%esi, %esi
	call	BN_to_ASN1_INTEGER@PLT
	movq	-4368(%rbp), %rdi
	movq	%rax, %r12
	call	BN_free@PLT
	movq	$0, -4368(%rbp)
	testq	%r12, %r12
	je	.L953
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	X509_REVOKED_set_serialNumber@PLT
	movq	%r12, %rdi
	call	ASN1_INTEGER_free@PLT
	movq	-4408(%rbp), %rdi
	movq	%r13, %rsi
	call	X509_CRL_add0_revoked@PLT
	jmp	.L681
.L1053:
	movq	bio_err(%rip), %rdi
	leaq	.LC190(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L653
.L683:
	movq	-4448(%rbp), %rbx
	movq	-4488(%rbp), %r14
	xorl	%r12d, %r12d
.L685:
	movl	$2447, %edx
	leaq	.LC11(%rip), %rsi
	xorl	%edi, %edi
	call	CRYPTO_free@PLT
	movq	-4304(%rbp), %rdi
	call	ASN1_OBJECT_free@PLT
	movq	-4296(%rbp), %rdi
	call	ASN1_GENERALIZEDTIME_free@PLT
	movq	%r12, %rdi
	call	ASN1_ENUMERATED_free@PLT
	movq	-4288(%rbp), %rdi
	call	ASN1_TIME_free@PLT
.L957:
	movq	bio_err(%rip), %rdi
	jmp	.L494
.L953:
	movq	-4448(%rbp), %rbx
	movq	-4488(%rbp), %r14
.L956:
	movq	$0, -4432(%rbp)
	movq	bio_err(%rip), %rdi
	jmp	.L494
.L1064:
	movq	bio_err(%rip), %rdi
	leaq	.LC202(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L675
.L1063:
	movq	-4408(%rbp), %rsi
	leaq	-4344(%rbp), %rcx
	leaq	.LC199(%rip), %rdx
	movq	%rbx, %rdi
	call	NCONF_get_number_e@PLT
	testl	%eax, %eax
	jne	.L673
	movq	$0, -4344(%rbp)
.L673:
	movq	-4408(%rbp), %rsi
	leaq	-4336(%rbp), %rcx
	leaq	.LC200(%rip), %rdx
	movq	%rbx, %rdi
	call	NCONF_get_number_e@PLT
	testl	%eax, %eax
	jne	.L674
	movq	$0, -4336(%rbp)
.L674:
	call	ERR_clear_error@PLT
	movq	-4344(%rbp), %rax
	orq	-4336(%rbp), %rax
	jne	.L672
	movq	bio_err(%rip), %rdi
	leaq	.LC201(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	movq	$0, -4408(%rbp)
	jmp	.L494
.L1062:
	movq	bio_err(%rip), %rdi
	leaq	.LC198(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	movq	$0, -4408(%rbp)
	movq	$0, -4432(%rbp)
	jmp	.L494
.L679:
	movq	bio_err(%rip), %rdi
	leaq	.LC203(%rip), %rsi
	call	BIO_puts@PLT
	movq	%r12, %rdi
	call	ASN1_TIME_free@PLT
	movq	bio_err(%rip), %rdi
	jmp	.L494
.L1039:
	movq	-4496(%rbp), %rdx
	leaq	.LC180(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L613
.L1037:
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	movq	$0, -4536(%rbp)
	jmp	.L613
.L638:
	movq	bio_err(%rip), %rdi
	leaq	.LC13(%rip), %rsi
	xorl	%eax, %eax
	xorl	%r14d, %r14d
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	movq	$0, -4408(%rbp)
	jmp	.L494
.L1046:
	leaq	.LC184(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L634:
	xorl	%edi, %edi
	call	X509_REQ_free@PLT
	movq	-4536(%rbp), %rdi
	call	X509_free@PLT
	jmp	.L628
.L1045:
	movq	bio_err(%rip), %rdi
	leaq	.LC90(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L634
.L1044:
	movq	bio_err(%rip), %rdi
	leaq	.LC89(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L632:
	xorl	%edi, %edi
	call	X509_REQ_free@PLT
	movq	-4536(%rbp), %rdi
	call	X509_free@PLT
	jmp	.L978
.L1043:
	movq	bio_err(%rip), %rdi
	movq	%rax, %rsi
	call	X509_print@PLT
	jmp	.L630
.L756:
	xorl	%r14d, %r14d
	jmp	.L671
.L1061:
	movq	-4520(%rbp), %rdx
	movq	bio_err(%rip), %rdi
	leaq	.LC196(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L978
.L1060:
	movq	-4408(%rbp), %rsi
	leaq	.LC195(%rip), %rdx
	movq	%rbx, %rdi
	call	NCONF_get_string@PLT
	movq	%rax, -4520(%rbp)
	testq	%rax, %rax
	jne	.L669
	call	ERR_clear_error@PLT
	jmp	.L670
.L755:
	movq	$0, -4408(%rbp)
	xorl	%r12d, %r12d
.L668:
	cmpl	$0, -4472(%rbp)
	je	.L759
	cmpq	$0, -4480(%rbp)
	je	.L1068
	movq	-4480(%rbp), %rdx
	movl	$32773, %esi
	movq	%rdx, %rdi
	call	load_cert@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L1069
	cmpl	$2, -4472(%rbp)
	movl	$-1, %eax
	movq	%r14, %rdi
	cmovne	-4644(%rbp), %eax
	movq	-4624(%rbp), %rcx
	movq	-4416(%rbp), %rsi
	movl	%eax, %edx
	call	do_revoke
	testl	%eax, %eax
	jle	.L960
	movq	%r14, %rdi
	call	X509_free@PLT
	movq	-4416(%rbp), %rdx
	movq	-4656(%rbp), %rdi
	leaq	.LC150(%rip), %rsi
	call	save_index@PLT
	testl	%eax, %eax
	je	.L960
	movq	-4656(%rbp), %rdi
	leaq	.LC151(%rip), %rdx
	leaq	.LC150(%rip), %rsi
	call	rotate_index@PLT
	testl	%eax, %eax
	je	.L960
	movq	bio_err(%rip), %rdi
	leaq	.LC194(%rip), %rsi
	xorl	%eax, %eax
	xorl	%r14d, %r14d
	call	BIO_printf@PLT
	movq	%r12, -4432(%rbp)
	movl	$0, -4448(%rbp)
	jmp	.L651
.L1067:
	movq	-4448(%rbp), %rbx
	movq	-4488(%rbp), %r14
	jmp	.L957
.L1066:
	movl	$2447, %edx
	leaq	.LC11(%rip), %rsi
	xorl	%edi, %edi
	call	CRYPTO_free@PLT
	movq	-4304(%rbp), %rdi
	call	ASN1_OBJECT_free@PLT
	movq	-4296(%rbp), %rdi
	call	ASN1_GENERALIZEDTIME_free@PLT
	movq	%r12, %rdi
	call	ASN1_ENUMERATED_free@PLT
	movq	-4288(%rbp), %rdi
	call	ASN1_TIME_free@PLT
	jmp	.L733
.L1065:
	movl	-4528(%rbp), %r13d
	movq	-4408(%rbp), %rdi
	movq	-4448(%rbp), %rbx
	movq	-4488(%rbp), %r14
	call	X509_CRL_sort@PLT
	testl	%r13d, %r13d
	jne	.L1070
.L695:
	movq	-4520(%rbp), %rax
	orq	-4496(%rbp), %rax
	jne	.L1071
	cmpl	$0, -4504(%rbp)
	je	.L704
	movq	-4408(%rbp), %rdi
	movl	$1, %esi
	call	X509_CRL_set_version@PLT
	testl	%eax, %eax
	je	.L956
.L704:
	movq	%r14, %rdi
	call	BN_free@PLT
	movq	-4456(%rbp), %rcx
	movq	-4360(%rbp), %rdx
	movq	-4424(%rbp), %rsi
	movq	-4408(%rbp), %rdi
	call	do_X509_CRL_sign@PLT
	testl	%eax, %eax
	je	.L1072
	cmpl	$1, -4600(%rbp)
	movq	-4584(%rbp), %rdi
	movl	$119, %esi
	sbbl	%edx, %edx
	andl	$32765, %edx
	addl	$4, %edx
	call	bio_open_default@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1073
	movq	-4408(%rbp), %rsi
	movq	%rax, %rdi
	call	PEM_write_bio_X509_CRL@PLT
	movq	-4496(%rbp), %rax
	testq	%rax, %rax
	je	.L668
	leaq	.LC151(%rip), %rdx
	leaq	.LC150(%rip), %rsi
	movq	%rax, %rdi
	call	rotate_serial@PLT
	testl	%eax, %eax
	jne	.L668
.L960:
	movq	%r12, -4432(%rbp)
	movq	bio_err(%rip), %rdi
	xorl	%r14d, %r14d
	jmp	.L494
.L1070:
	movq	bio_err(%rip), %rdi
	leaq	.LC204(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L695
.L946:
	movq	-4448(%rbp), %rbx
	movq	-4488(%rbp), %r14
	jmp	.L685
.L1071:
	movq	-4408(%rbp), %r8
	xorl	%r9d, %r9d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	leaq	-4288(%rbp), %r12
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	X509V3_set_ctx@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	X509V3_set_nconf@PLT
	cmpq	$0, -4520(%rbp)
	je	.L701
	movq	-4408(%rbp), %rcx
	movq	-4520(%rbp), %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	X509V3_EXT_CRL_add_nconf@PLT
	testl	%eax, %eax
	je	.L957
	cmpq	$0, -4496(%rbp)
	jne	.L701
	movq	-4408(%rbp), %rdi
	movl	$1, %esi
	call	X509_CRL_set_version@PLT
	testl	%eax, %eax
	je	.L957
	jmp	.L704
.L1069:
	movq	%r12, -4432(%rbp)
	movq	bio_err(%rip), %rdi
	jmp	.L494
.L701:
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	BN_to_ASN1_INTEGER@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L957
	movq	-4408(%rbp), %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rax, %rdx
	movl	$88, %esi
	call	X509_CRL_add1_ext_i2d@PLT
	movq	%r12, %rdi
	call	ASN1_INTEGER_free@PLT
	movl	$1, %esi
	movq	%r14, %rdi
	call	BN_add_word@PLT
	testl	%eax, %eax
	je	.L957
	movq	-4408(%rbp), %rdi
	movl	$1, %esi
	call	X509_CRL_set_version@PLT
	testl	%eax, %eax
	je	.L957
	movq	-4496(%rbp), %rax
	testq	%rax, %rax
	je	.L704
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	leaq	.LC150(%rip), %rsi
	movq	%rax, %rdi
	call	save_serial@PLT
	testl	%eax, %eax
	jne	.L704
	jmp	.L957
.L1068:
	movq	bio_err(%rip), %rdi
	leaq	.LC205(%rip), %rsi
	xorl	%eax, %eax
	xorl	%r14d, %r14d
	call	BIO_printf@PLT
	movq	%r12, -4432(%rbp)
	movq	bio_err(%rip), %rdi
	jmp	.L494
.L759:
	movl	$0, -4448(%rbp)
	xorl	%r14d, %r14d
	movq	%r12, -4432(%rbp)
	jmp	.L651
.L1073:
	movq	$0, -4432(%rbp)
	movq	bio_err(%rip), %rdi
	xorl	%r14d, %r14d
	jmp	.L494
.L1072:
	movq	bio_err(%rip), %rdi
	xorl	%r14d, %r14d
	jmp	.L494
.L1036:
	movq	-4496(%rbp), %rdx
	movq	bio_err(%rip), %rdi
	leaq	.LC177(%rip), %rsi
	call	BIO_printf@PLT
	movq	$0, -4536(%rbp)
	movq	$0, -4592(%rbp)
	jmp	.L613
.L1035:
	movq	-4496(%rbp), %rcx
	movq	-4288(%rbp), %rdx
	leaq	.LC176(%rip), %rsi
	movq	bio_err(%rip), %rdi
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	movq	$0, -4536(%rbp)
	movq	$0, -4592(%rbp)
	jmp	.L613
.L753:
	movl	$0, -4496(%rbp)
	xorl	%r12d, %r12d
	jmp	.L611
.L1034:
	movq	bio_err(%rip), %rdi
	leaq	.LC13(%rip), %rsi
	call	BIO_printf@PLT
	jmp	.L973
.L1033:
	movq	-4536(%rbp), %rdx
	leaq	.LC175(%rip), %rsi
.L977:
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L979:
	movq	$0, -4408(%rbp)
	movq	bio_err(%rip), %rdi
	movq	$0, -4440(%rbp)
	movq	$0, -4432(%rbp)
	jmp	.L494
.L603:
	leaq	.LC171(%rip), %rsi
.L971:
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L975
.L601:
	movl	-4640(%rbp), %esi
	movq	-4632(%rbp), %rdi
	xorl	%edx, %edx
	call	load_serial@PLT
	movq	%rax, -4368(%rbp)
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L1074
	testl	%r13d, %r13d
	je	.L604
	movq	%rax, %rdi
	call	BN_is_zero@PLT
	testl	%eax, %eax
	je	.L607
	movq	bio_err(%rip), %rdi
	leaq	.LC173(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L604
.L1032:
	movq	-4408(%rbp), %rsi
	leaq	-4328(%rbp), %rcx
	leaq	.LC169(%rip), %rdx
	movq	%rbx, %rdi
	call	NCONF_get_number_e@PLT
	testl	%eax, %eax
	jne	.L599
	movq	$0, -4328(%rbp)
	jmp	.L599
.L607:
	movq	-4368(%rbp), %rdi
	call	BN_bn2hex@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L979
	movq	bio_err(%rip), %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	leaq	.LC174(%rip), %rsi
	call	BIO_printf@PLT
	movl	$856, %edx
	leaq	.LC11(%rip), %rsi
	movq	%r14, %rdi
	call	CRYPTO_free@PLT
	jmp	.L604
.L1074:
	movq	bio_err(%rip), %rdi
	leaq	.LC172(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L979
.L1031:
	leaq	.LC168(%rip), %rsi
	jmp	.L971
.L1029:
	leaq	.LC166(%rip), %rsi
	jmp	.L971
.L1028:
	movq	-4408(%rbp), %rsi
	leaq	.LC165(%rip), %rdx
	movq	%rbx, %rdi
	call	NCONF_get_string@PLT
	movq	%rax, -4512(%rbp)
	testq	%rax, %rax
	jne	.L594
	call	ERR_clear_error@PLT
	leaq	.LC101(%rip), %rax
	movq	%rax, -4512(%rbp)
	jmp	.L595
.L735:
	cmpq	$0, -4328(%rbp)
	je	.L600
	jmp	.L599
.L1027:
	cmpq	$0, -4488(%rbp)
	je	.L1075
.L593:
	leaq	-4288(%rbp), %r10
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	movq	%r10, %rdi
	movl	$1, %r9d
	movq	%r10, -4440(%rbp)
	call	X509V3_set_ctx@PLT
	movq	-4440(%rbp), %r10
	movq	%rbx, %rsi
	movq	%r10, %rdi
	call	X509V3_set_nconf@PLT
	movq	-4440(%rbp), %r10
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	movq	-4488(%rbp), %rdx
	movq	%r10, %rsi
	call	X509V3_EXT_add_nconf@PLT
	testl	%eax, %eax
	jne	.L592
	movq	-4488(%rbp), %rdx
	leaq	.LC164(%rip), %rsi
	jmp	.L977
.L1026:
	movq	-4408(%rbp), %rsi
	leaq	.LC162(%rip), %rdx
	movq	%rbx, %rdi
	call	lookup_conf
	movq	%rax, -4632(%rbp)
	testq	%rax, %rax
	jne	.L591
	jmp	.L976
.L1075:
	movq	-4408(%rbp), %rsi
	leaq	.LC163(%rip), %rdx
	movq	%rbx, %rdi
	call	NCONF_get_string@PLT
	movq	%rax, -4488(%rbp)
	testq	%rax, %rax
	jne	.L593
	call	ERR_clear_error@PLT
	jmp	.L592
.L1025:
	movq	-4408(%rbp), %rsi
	leaq	.LC207(%rip), %rdx
	movq	%rbx, %rdi
	call	lookup_conf
	movq	%rax, -4536(%rbp)
	testq	%rax, %rax
	jne	.L737
	jmp	.L975
.L1024:
	movq	-4360(%rbp), %rdi
	call	EVP_MD_type@PLT
	movl	%eax, %edi
	call	OBJ_nid2ln@PLT
	movq	bio_err(%rip), %rdi
	leaq	.LC159(%rip), %rsi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	cmpq	$0, -4536(%rbp)
	je	.L1076
.L590:
	movq	-4536(%rbp), %rdx
	movq	bio_err(%rip), %rdi
	leaq	.LC160(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L737
.L1023:
	movq	-4408(%rbp), %rsi
	movq	%rbx, %rdi
	leaq	.LC157(%rip), %rdx
	call	NCONF_get_string@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L587
	leaq	.LC158(%rip), %rsi
	call	strcmp@PLT
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
	movl	%eax, -4540(%rbp)
	jmp	.L587
.L1018:
	movq	-4600(%rbp), %rdx
	movq	bio_err(%rip), %rdi
	leaq	.LC153(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L578
.L1019:
	movq	extconf(%rip), %rdi
	leaq	.LC154(%rip), %rdx
	leaq	.LC100(%rip), %rsi
	call	NCONF_get_string@PLT
	movq	%rax, -4488(%rbp)
	testq	%rax, %rax
	jne	.L576
	leaq	.LC100(%rip), %rax
	movq	%rax, -4488(%rbp)
	jmp	.L576
.L761:
	movl	$0, -4600(%rbp)
	jmp	.L579
.L1020:
	cmpl	$0, -4376(%rbp)
	jne	.L581
	call	EVP_md_null@PLT
	movq	%rax, -4360(%rbp)
	jmp	.L582
.L1022:
	movq	bio_err(%rip), %rdi
	leaq	.LC156(%rip), %rsi
	call	BIO_puts@PLT
	jmp	.L975
.L751:
	movq	$0, -4440(%rbp)
	jmp	.L586
.L1076:
	movq	-4408(%rbp), %rsi
	leaq	.LC207(%rip), %rdx
	movq	%rbx, %rdi
	call	lookup_conf
	movq	%rax, -4536(%rbp)
	testq	%rax, %rax
	jne	.L590
	jmp	.L975
.L1017:
	movq	bio_err(%rip), %rdi
	leaq	.LC149(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L562
.L1016:
	movq	-4416(%rbp), %rdx
	movq	-4656(%rbp), %rdi
	leaq	.LC150(%rip), %rsi
	call	save_index@PLT
	testl	%eax, %eax
	je	.L975
	movq	-4656(%rbp), %rdi
	leaq	.LC151(%rip), %rdx
	leaq	.LC150(%rip), %rsi
	call	rotate_index@PLT
	testl	%eax, %eax
	je	.L975
	testl	%r13d, %r13d
	je	.L562
	movl	-4632(%rbp), %edx
	movq	bio_err(%rip), %rdi
	leaq	.LC152(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L562
.L1014:
	movq	%r14, %rdi
	call	ASN1_UTCTIME_free@PLT
.L564:
	leaq	.LC148(%rip), %rsi
	jmp	.L971
.L1013:
	movq	-4656(%rbp), %rdx
	movq	bio_err(%rip), %rdi
	leaq	.LC144(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L563
.L568:
	cmpl	$0, -4704(%rbp)
	jg	.L567
.L715:
	movq	-4688(%rbp), %rsi
	call	strcmp@PLT
	testl	%eax, %eax
	jg	.L567
	jmp	.L569
.L1012:
	movq	-4416(%rbp), %r14
	movq	bio_out(%rip), %rdi
	movq	8(%r14), %rsi
	call	TXT_DB_write@PLT
	movq	8(%r14), %rax
	movq	8(%rax), %rdi
	call	OPENSSL_sk_num@PLT
	movq	bio_err(%rip), %rdi
	leaq	.LC142(%rip), %rsi
	movl	%eax, %edx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	leaq	.LC143(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L559
	.cfi_endproc
.LFE1437:
	.size	ca_main, .-ca_main
	.section	.rodata
	.align 16
	.type	HEX_DIGITS.24968, @object
	.size	HEX_DIGITS.24968, 17
HEX_DIGITS.24968:
	.string	"0123456789ABCDEF"
	.globl	ca_options
	.section	.rodata.str1.1
.LC208:
	.string	"help"
.LC209:
	.string	"Display this summary"
.LC210:
	.string	"verbose"
	.section	.rodata.str1.8
	.align 8
.LC211:
	.string	"Verbose output during processing"
	.section	.rodata.str1.1
.LC212:
	.string	"config"
.LC213:
	.string	"A config file"
.LC214:
	.string	"name"
	.section	.rodata.str1.8
	.align 8
.LC215:
	.string	"The particular CA definition to use"
	.section	.rodata.str1.1
.LC216:
	.string	"subj"
	.section	.rodata.str1.8
	.align 8
.LC217:
	.string	"Use arg instead of request's subject"
	.align 8
.LC218:
	.string	"Input characters are UTF8 (default ASCII)"
	.section	.rodata.str1.1
.LC219:
	.string	"create_serial"
	.section	.rodata.str1.8
	.align 8
.LC220:
	.string	"If reading serial fails, create a new random serial"
	.align 8
.LC221:
	.string	"Always create a random serial; do not store it"
	.section	.rodata.str1.1
.LC222:
	.string	"multivalue-rdn"
	.section	.rodata.str1.8
	.align 8
.LC223:
	.string	"Enable support for multivalued RDNs"
	.section	.rodata.str1.1
.LC224:
	.string	"startdate"
.LC225:
	.string	"Cert notBefore, YYMMDDHHMMSSZ"
.LC226:
	.string	"enddate"
	.section	.rodata.str1.8
	.align 8
.LC227:
	.string	"YYMMDDHHMMSSZ cert notAfter (overrides -days)"
	.section	.rodata.str1.1
.LC228:
	.string	"days"
	.section	.rodata.str1.8
	.align 8
.LC229:
	.string	"Number of days to certify the cert for"
	.section	.rodata.str1.1
.LC230:
	.string	"md"
	.section	.rodata.str1.8
	.align 8
.LC231:
	.string	"md to use; one of md2, md5, sha or sha1"
	.section	.rodata.str1.1
.LC232:
	.string	"The CA 'policy' to support"
.LC233:
	.string	"keyfile"
.LC234:
	.string	"Private key"
.LC235:
	.string	"keyform"
	.section	.rodata.str1.8
	.align 8
.LC236:
	.string	"Private key file format (PEM or ENGINE)"
	.section	.rodata.str1.1
.LC237:
	.string	"passin"
.LC238:
	.string	"Input file pass phrase source"
.LC239:
	.string	"key"
	.section	.rodata.str1.8
	.align 8
.LC240:
	.string	"Key to decode the private key if it is encrypted"
	.section	.rodata.str1.1
.LC241:
	.string	"cert"
.LC242:
	.string	"The CA cert"
.LC243:
	.string	"selfsign"
	.section	.rodata.str1.8
	.align 8
.LC244:
	.string	"Sign a cert with the key associated with it"
	.section	.rodata.str1.1
.LC245:
	.string	"in"
	.section	.rodata.str1.8
	.align 8
.LC246:
	.string	"The input PEM encoded cert request(s)"
	.section	.rodata.str1.1
.LC247:
	.string	"out"
	.section	.rodata.str1.8
	.align 8
.LC248:
	.string	"Where to put the output file(s)"
	.section	.rodata.str1.1
.LC249:
	.string	"outdir"
.LC250:
	.string	"Where to put output cert"
.LC251:
	.string	"sigopt"
	.section	.rodata.str1.8
	.align 8
.LC252:
	.string	"Signature parameter in n:v form"
	.section	.rodata.str1.1
.LC253:
	.string	"notext"
	.section	.rodata.str1.8
	.align 8
.LC254:
	.string	"Do not print the generated certificate"
	.section	.rodata.str1.1
.LC255:
	.string	"batch"
.LC256:
	.string	"Don't ask questions"
.LC257:
	.string	"preserveDN"
.LC258:
	.string	"Don't re-order the DN"
.LC259:
	.string	"noemailDN"
	.section	.rodata.str1.8
	.align 8
.LC260:
	.string	"Don't add the EMAIL field to the DN"
	.section	.rodata.str1.1
.LC261:
	.string	"gencrl"
.LC262:
	.string	"Generate a new CRL"
	.section	.rodata.str1.8
	.align 8
.LC263:
	.string	"msie modifications to handle all those universal strings"
	.section	.rodata.str1.1
.LC264:
	.string	"crldays"
	.section	.rodata.str1.8
	.align 8
.LC265:
	.string	"Days until the next CRL is due"
	.section	.rodata.str1.1
.LC266:
	.string	"crlhours"
	.section	.rodata.str1.8
	.align 8
.LC267:
	.string	"Hours until the next CRL is due"
	.section	.rodata.str1.1
.LC268:
	.string	"crlsec"
	.section	.rodata.str1.8
	.align 8
.LC269:
	.string	"Seconds until the next CRL is due"
	.section	.rodata.str1.1
.LC270:
	.string	"infiles"
	.section	.rodata.str1.8
	.align 8
.LC271:
	.string	"The last argument, requests to process"
	.section	.rodata.str1.1
.LC272:
	.string	"ss_cert"
	.section	.rodata.str1.8
	.align 8
.LC273:
	.string	"File contains a self signed cert to sign"
	.section	.rodata.str1.1
.LC274:
	.string	"spkac"
	.section	.rodata.str1.8
	.align 8
.LC275:
	.string	"File contains DN and signed public key and challenge"
	.section	.rodata.str1.1
.LC276:
	.string	"revoke"
.LC277:
	.string	"Revoke a cert (given in file)"
.LC278:
	.string	"valid"
	.section	.rodata.str1.8
	.align 8
.LC279:
	.string	"Add a Valid(not-revoked) DB entry about a cert (given in file)"
	.align 8
.LC280:
	.string	"Extension section (override value in config file)"
	.section	.rodata.str1.1
.LC281:
	.string	"extfile"
	.section	.rodata.str1.8
	.align 8
.LC282:
	.string	"Configuration file with X509v3 extensions to add"
	.section	.rodata.str1.1
.LC283:
	.string	"status"
	.section	.rodata.str1.8
	.align 8
.LC284:
	.string	"Shows cert status given the serial number"
	.section	.rodata.str1.1
.LC285:
	.string	"updatedb"
.LC286:
	.string	"Updates db for expired cert"
.LC287:
	.string	"crlexts"
	.section	.rodata.str1.8
	.align 8
.LC288:
	.string	"CRL extension section (override value in config file)"
	.section	.rodata.str1.1
.LC289:
	.string	"crl_reason"
.LC290:
	.string	"crl_hold"
	.section	.rodata.str1.8
	.align 8
.LC291:
	.string	"the hold instruction, an OID. Sets revocation reason to certificateHold"
	.section	.rodata.str1.1
.LC292:
	.string	"crl_compromise"
	.section	.rodata.str1.8
	.align 8
.LC293:
	.string	"sets compromise time to val and the revocation reason to keyCompromise"
	.section	.rodata.str1.1
.LC294:
	.string	"crl_CA_compromise"
	.section	.rodata.str1.8
	.align 8
.LC295:
	.string	"sets compromise time to val and the revocation reason to CACompromise"
	.section	.rodata.str1.1
.LC296:
	.string	"rand"
	.section	.rodata.str1.8
	.align 8
.LC297:
	.string	"Load the file(s) into the random number generator"
	.section	.rodata.str1.1
.LC298:
	.string	"writerand"
	.section	.rodata.str1.8
	.align 8
.LC299:
	.string	"Write random data to the specified file"
	.section	.rodata.str1.1
.LC300:
	.string	"engine"
	.section	.rodata.str1.8
	.align 8
.LC301:
	.string	"Use engine, possibly a hardware device"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	ca_options, @object
	.size	ca_options, 1224
ca_options:
	.quad	.LC208
	.long	1
	.long	45
	.quad	.LC209
	.quad	.LC210
	.long	3
	.long	45
	.quad	.LC211
	.quad	.LC212
	.long	4
	.long	115
	.quad	.LC213
	.quad	.LC214
	.long	5
	.long	115
	.quad	.LC215
	.quad	.LC216
	.long	6
	.long	115
	.quad	.LC217
	.quad	.LC109
	.long	7
	.long	45
	.quad	.LC218
	.quad	.LC219
	.long	8
	.long	45
	.quad	.LC220
	.quad	.LC161
	.long	44
	.long	45
	.quad	.LC221
	.quad	.LC222
	.long	9
	.long	45
	.quad	.LC223
	.quad	.LC224
	.long	10
	.long	115
	.quad	.LC225
	.quad	.LC226
	.long	11
	.long	115
	.quad	.LC227
	.quad	.LC228
	.long	12
	.long	112
	.quad	.LC229
	.quad	.LC230
	.long	13
	.long	115
	.quad	.LC231
	.quad	.LC207
	.long	14
	.long	115
	.quad	.LC232
	.quad	.LC233
	.long	15
	.long	115
	.quad	.LC234
	.quad	.LC235
	.long	16
	.long	102
	.quad	.LC236
	.quad	.LC237
	.long	17
	.long	115
	.quad	.LC238
	.quad	.LC239
	.long	18
	.long	115
	.quad	.LC240
	.quad	.LC241
	.long	19
	.long	60
	.quad	.LC242
	.quad	.LC243
	.long	20
	.long	45
	.quad	.LC244
	.quad	.LC245
	.long	21
	.long	60
	.quad	.LC246
	.quad	.LC247
	.long	22
	.long	62
	.quad	.LC248
	.quad	.LC249
	.long	23
	.long	47
	.quad	.LC250
	.quad	.LC251
	.long	24
	.long	115
	.quad	.LC252
	.quad	.LC253
	.long	25
	.long	45
	.quad	.LC254
	.quad	.LC255
	.long	26
	.long	45
	.quad	.LC256
	.quad	.LC257
	.long	27
	.long	45
	.quad	.LC258
	.quad	.LC259
	.long	28
	.long	45
	.quad	.LC260
	.quad	.LC261
	.long	29
	.long	45
	.quad	.LC262
	.quad	.LC128
	.long	30
	.long	45
	.quad	.LC263
	.quad	.LC264
	.long	31
	.long	112
	.quad	.LC265
	.quad	.LC266
	.long	32
	.long	112
	.quad	.LC267
	.quad	.LC268
	.long	33
	.long	112
	.quad	.LC269
	.quad	.LC270
	.long	34
	.long	45
	.quad	.LC271
	.quad	.LC272
	.long	35
	.long	60
	.quad	.LC273
	.quad	.LC274
	.long	36
	.long	60
	.quad	.LC275
	.quad	.LC276
	.long	37
	.long	60
	.quad	.LC277
	.quad	.LC278
	.long	38
	.long	115
	.quad	.LC279
	.quad	.LC154
	.long	39
	.long	115
	.quad	.LC280
	.quad	.LC281
	.long	40
	.long	60
	.quad	.LC282
	.quad	.LC283
	.long	41
	.long	115
	.quad	.LC284
	.quad	.LC285
	.long	42
	.long	45
	.quad	.LC286
	.quad	.LC287
	.long	43
	.long	115
	.quad	.LC288
	.quad	.LC289
	.long	1504
	.long	115
	.quad	.LC28
	.quad	.LC290
	.long	1505
	.long	115
	.quad	.LC291
	.quad	.LC292
	.long	1506
	.long	115
	.quad	.LC293
	.quad	.LC294
	.long	1507
	.long	115
	.quad	.LC295
	.quad	.LC296
	.long	1501
	.long	115
	.quad	.LC297
	.quad	.LC298
	.long	1502
	.long	62
	.quad	.LC299
	.quad	.LC300
	.long	2
	.long	115
	.quad	.LC301
	.quad	0
	.zero	16
	.local	msie_hack
	.comm	msie_hack,4,4
	.local	preserve
	.comm	preserve,4,4
	.local	extconf
	.comm	extconf,8,8
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
