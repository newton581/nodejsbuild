	.file	"app_rand.c"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"RANDFILE"
.LC1:
	.string	"Can't load %s into RNG\n"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"../deps/openssl/openssl/apps/app_rand.c"
	.text
	.p2align 4
	.globl	app_RAND_load_conf
	.type	app_RAND_load_conf, @function
app_RAND_load_conf:
.LFB1413:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	NCONF_get_string@PLT
	testq	%rax, %rax
	je	.L7
	movq	$-1, %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	RAND_load_file@PLT
	testl	%eax, %eax
	js	.L8
	cmpq	$0, save_rand_file(%rip)
	je	.L9
.L1:
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	.cfi_restore_state
	movq	bio_err(%rip), %rdi
	movq	%r12, %rdx
	leaq	.LC1(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	cmpq	$0, save_rand_file(%rip)
	jne	.L1
.L9:
	movq	%r12, %rdi
	movl	$31, %edx
	leaq	.LC2(%rip), %rsi
	call	CRYPTO_strdup@PLT
	movq	%rax, save_rand_file(%rip)
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	ERR_clear_error@PLT
	.cfi_endproc
.LFE1413:
	.size	app_RAND_load_conf, .-app_RAND_load_conf
	.section	.rodata.str1.1
.LC3:
	.string	"Cannot write random bytes:\n"
	.text
	.p2align 4
	.globl	app_RAND_write
	.type	app_RAND_write, @function
app_RAND_write:
.LFB1415:
	.cfi_startproc
	endbr64
	movq	save_rand_file(%rip), %rdi
	testq	%rdi, %rdi
	je	.L17
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	RAND_write_file@PLT
	cmpl	$-1, %eax
	je	.L20
.L12:
	movq	save_rand_file(%rip), %rdi
	movl	$68, %edx
	leaq	.LC2(%rip), %rsi
	call	CRYPTO_free@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	movq	$0, save_rand_file(%rip)
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	movq	bio_err(%rip), %rdi
	leaq	.LC3(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	jmp	.L12
	.cfi_endproc
.LFE1415:
	.size	app_RAND_write, .-app_RAND_write
	.p2align 4
	.globl	opt_rand
	.type	opt_rand, @function
opt_rand:
.LFB1416:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	cmpl	$1501, %edi
	je	.L22
	cmpl	$1502, %edi
	jne	.L52
	movq	save_rand_file(%rip), %rdi
	movl	$88, %edx
	leaq	.LC2(%rip), %rsi
	call	CRYPTO_free@PLT
	call	opt_arg@PLT
	movl	$89, %edx
	leaq	.LC2(%rip), %rsi
	movq	%rax, %rdi
	call	CRYPTO_strdup@PLT
	movq	%rax, save_rand_file(%rip)
.L52:
	movl	$1, %r13d
.L21:
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	.cfi_restore_state
	call	opt_arg@PLT
	movl	$1, %r13d
	movq	%rax, %r12
	movzbl	(%rax), %eax
.L31:
	movq	%r12, %rbx
	cmpb	$58, %al
	je	.L25
	testb	%al, %al
	jne	.L26
.L25:
	testb	%al, %al
	je	.L28
	movb	$0, (%rbx)
	movq	$-1, %rsi
	movq	%r12, %rdi
	call	RAND_load_file@PLT
	testl	%eax, %eax
	js	.L54
	movzbl	1(%rbx), %eax
	leaq	1(%rbx), %r12
	testb	%al, %al
	jne	.L31
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L55:
	.cfi_restore_state
	cmpb	$58, %al
	je	.L25
.L26:
	movzbl	1(%rbx), %eax
	addq	$1, %rbx
	testb	%al, %al
	jne	.L55
.L28:
	movb	$0, (%rbx)
	movq	$-1, %rsi
	movq	%r12, %rdi
	call	RAND_load_file@PLT
	testl	%eax, %eax
	jns	.L21
	movq	bio_err(%rip), %rdi
	movq	%r12, %rdx
	xorl	%eax, %eax
	xorl	%r13d, %r13d
	leaq	.LC1(%rip), %rsi
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	jmp	.L21
.L54:
	movq	bio_err(%rip), %rdi
	movq	%r12, %rdx
	xorl	%eax, %eax
	leaq	1(%rbx), %r12
	leaq	.LC1(%rip), %rsi
	xorl	%r13d, %r13d
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	movzbl	1(%rbx), %eax
	testb	%al, %al
	je	.L21
	jmp	.L31
	.cfi_endproc
.LFE1416:
	.size	opt_rand, .-opt_rand
	.local	save_rand_file
	.comm	save_rand_file,8,8
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
