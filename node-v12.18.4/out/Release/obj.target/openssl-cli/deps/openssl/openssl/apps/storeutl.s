	.file	"storeutl.c"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	""
.LC1:
	.string	"%*s"
	.text
	.p2align 4
	.type	indent_printf, @function
indent_printf:
.LFB1427:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edi, %r10d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$216, %rsp
	.cfi_offset 3, -40
	movq	%rcx, -184(%rbp)
	movq	%r8, -176(%rbp)
	movq	%r9, -168(%rbp)
	testb	%al, %al
	je	.L2
	movaps	%xmm0, -160(%rbp)
	movaps	%xmm1, -144(%rbp)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm3, -112(%rbp)
	movaps	%xmm4, -96(%rbp)
	movaps	%xmm5, -80(%rbp)
	movaps	%xmm6, -64(%rbp)
	movaps	%xmm7, -48(%rbp)
.L2:
	movq	%fs:40, %rax
	movq	%rax, -216(%rbp)
	xorl	%eax, %eax
	leaq	16(%rbp), %rax
	leaq	.LC0(%rip), %rcx
	movl	%r10d, %edx
	movq	%rax, -232(%rbp)
	movq	%r12, %rdi
	leaq	-208(%rbp), %rax
	leaq	.LC1(%rip), %rsi
	movq	%rax, -224(%rbp)
	xorl	%eax, %eax
	movl	$24, -240(%rbp)
	movl	$48, -236(%rbp)
	call	BIO_printf@PLT
	leaq	-240(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	BIO_vprintf@PLT
	addl	%ebx, %eax
	movq	-216(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L6
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L6:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1427:
	.size	indent_printf, .-indent_printf
	.section	.rodata.str1.1
.LC2:
	.string	"Couldn't open file or uri %s\n"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"%s: the store scheme doesn't support the given search criteria.\n"
	.align 8
.LC4:
	.string	"ERROR: OSSL_STORE_load() returned NULL without eof or error indications\n"
	.align 8
.LC5:
	.string	"       This is an error in the loader\n"
	.section	.rodata.str1.1
.LC6:
	.string	"%d: %s: %s\n"
.LC7:
	.string	"%s\n"
.LC8:
	.string	"%d: %s\n"
.LC9:
	.string	"!!! Unknown code\n"
.LC10:
	.string	"Total found: %d\n"
	.text
	.p2align 4
	.type	process, @function
process:
.LFB1428:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%ecx, -56(%rbp)
	xorl	%ecx, %ecx
	movl	40(%rbp), %r15d
	movl	%r8d, -60(%rbp)
	xorl	%r8d, %r8d
	movq	%rsi, -96(%rbp)
	movq	%rdx, -104(%rbp)
	movq	%r9, -88(%rbp)
	call	OSSL_STORE_open@PLT
	testq	%rax, %rax
	je	.L62
	movq	%rax, %r12
	movl	-56(%rbp), %eax
	testl	%eax, %eax
	jne	.L63
.L10:
	movl	-60(%rbp), %eax
	testl	%eax, %eax
	jne	.L12
.L15:
	movl	$0, -52(%rbp)
	xorl	%r13d, %r13d
	.p2align 4,,10
	.p2align 3
.L13:
	movq	%r12, %rdi
	call	OSSL_STORE_load@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L16
	movq	%rax, %rdi
	call	OSSL_STORE_INFO_get_type@PLT
	movl	%eax, %ebx
	movl	%ebx, %edi
	call	OSSL_STORE_INFO_type_string@PLT
	movq	%rax, %r8
	cmpl	$1, %ebx
	je	.L64
	movq	bio_out(%rip), %rsi
	xorl	%eax, %eax
	movl	%r13d, %ecx
	movl	%r15d, %edi
	leaq	.LC8(%rip), %rdx
	call	indent_printf
	cmpl	$5, %ebx
	ja	.L25
	leaq	.L27(%rip), %rcx
	movslq	(%rcx,%rbx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L27:
	.long	.L25-.L27
	.long	.L25-.L27
	.long	.L30-.L27
	.long	.L29-.L27
	.long	.L28-.L27
	.long	.L26-.L27
	.text
	.p2align 4,,10
	.p2align 3
.L16:
	movq	%r12, %rdi
	call	OSSL_STORE_eof@PLT
	testl	%eax, %eax
	jne	.L23
	movq	%r12, %rdi
	call	OSSL_STORE_error@PLT
	testl	%eax, %eax
	je	.L19
	movl	32(%rbp), %eax
	testl	%eax, %eax
	jne	.L65
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
.L21:
	addl	$1, -52(%rbp)
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L64:
	movq	%r14, %rdi
	movq	%rax, -80(%rbp)
	call	OSSL_STORE_INFO_get0_NAME@PLT
	movq	%r14, %rdi
	movq	%rax, -72(%rbp)
	call	OSSL_STORE_INFO_get0_NAME_description@PLT
	movq	-72(%rbp), %r9
	movq	-80(%rbp), %r8
	movl	%r13d, %ecx
	movq	bio_out(%rip), %rsi
	movq	%rax, %rbx
	leaq	.LC6(%rip), %rdx
	xorl	%eax, %eax
	movl	%r15d, %edi
	call	indent_printf
	testq	%rbx, %rbx
	je	.L24
	movq	bio_out(%rip), %rsi
	movq	%rbx, %rcx
	movl	%r15d, %edi
	xorl	%eax, %eax
	leaq	.LC7(%rip), %rdx
	call	indent_printf
.L24:
	movl	32(%rbp), %eax
	testl	%eax, %eax
	jne	.L66
.L31:
	movq	%r14, %rdi
	addl	$1, %r13d
	call	OSSL_STORE_INFO_free@PLT
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L19:
	movq	bio_err(%rip), %rdi
	leaq	.LC4(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	leaq	.LC5(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	addl	$1, -52(%rbp)
.L23:
	movq	48(%rbp), %rsi
	movl	%r13d, %ecx
	leaq	.LC10(%rip), %rdx
	xorl	%eax, %eax
	movl	%r15d, %edi
	call	indent_printf
.L11:
	movq	%r12, %rdi
	call	OSSL_STORE_close@PLT
	testl	%eax, %eax
	je	.L67
.L7:
	movl	-52(%rbp), %eax
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L28:
	.cfi_restore_state
	movl	16(%rbp), %esi
	testl	%esi, %esi
	jne	.L68
.L34:
	movl	24(%rbp), %ecx
	testl	%ecx, %ecx
	jne	.L31
	movq	%r14, %rdi
	addl	$1, %r13d
	call	OSSL_STORE_INFO_get0_CERT@PLT
	movq	48(%rbp), %rdi
	movq	%rax, %rsi
	call	PEM_write_bio_X509@PLT
	movq	%r14, %rdi
	call	OSSL_STORE_INFO_free@PLT
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L29:
	movl	16(%rbp), %r10d
	testl	%r10d, %r10d
	jne	.L69
.L33:
	movl	24(%rbp), %r9d
	testl	%r9d, %r9d
	jne	.L31
	movq	%r14, %rdi
	addl	$1, %r13d
	call	OSSL_STORE_INFO_get0_PKEY@PLT
	subq	$8, %rsp
	movq	48(%rbp), %rdi
	xorl	%r8d, %r8d
	pushq	$0
	movq	%rax, %rsi
	xorl	%r9d, %r9d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	call	PEM_write_bio_PrivateKey@PLT
	popq	%rdi
	movq	%r14, %rdi
	popq	%r8
	call	OSSL_STORE_INFO_free@PLT
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L30:
	movl	16(%rbp), %ebx
	testl	%ebx, %ebx
	jne	.L70
.L32:
	movl	24(%rbp), %r11d
	testl	%r11d, %r11d
	jne	.L31
	movq	%r14, %rdi
	addl	$1, %r13d
	call	OSSL_STORE_INFO_get0_PARAMS@PLT
	movq	48(%rbp), %rdi
	movq	%rax, %rsi
	call	PEM_write_bio_Parameters@PLT
	movq	%r14, %rdi
	call	OSSL_STORE_INFO_free@PLT
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L26:
	movl	16(%rbp), %edx
	testl	%edx, %edx
	jne	.L71
.L35:
	movl	24(%rbp), %eax
	testl	%eax, %eax
	jne	.L31
	movq	%r14, %rdi
	addl	$1, %r13d
	call	OSSL_STORE_INFO_get0_CRL@PLT
	movq	48(%rbp), %rdi
	movq	%rax, %rsi
	call	PEM_write_bio_X509_CRL@PLT
	movq	%r14, %rdi
	call	OSSL_STORE_INFO_free@PLT
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L65:
	call	ERR_clear_error@PLT
	jmp	.L21
.L25:
	movq	bio_err(%rip), %rdi
	leaq	.LC9(%rip), %rsi
	xorl	%eax, %eax
	addl	$1, %r13d
	call	BIO_printf@PLT
	movq	%r14, %rdi
	addl	$1, -52(%rbp)
	call	OSSL_STORE_INFO_free@PLT
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L67:
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	addl	$1, -52(%rbp)
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L63:
	movl	-56(%rbp), %esi
	movq	%r12, %rdi
	call	OSSL_STORE_expect@PLT
	testl	%eax, %eax
	jne	.L10
.L61:
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	movl	$1, -52(%rbp)
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L12:
	movl	-60(%rbp), %esi
	movq	%r12, %rdi
	call	OSSL_STORE_supports_search@PLT
	testl	%eax, %eax
	je	.L72
	movq	-88(%rbp), %rsi
	movq	%r12, %rdi
	call	OSSL_STORE_find@PLT
	testl	%eax, %eax
	jne	.L15
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L66:
	movq	%r14, %rdi
	addl	$1, %r13d
	call	OSSL_STORE_INFO_get0_NAME@PLT
	movq	-88(%rbp), %r9
	movl	-60(%rbp), %r8d
	pushq	56(%rbp)
	movq	%rax, %rdi
	leal	2(%r15), %eax
	movl	-56(%rbp), %ecx
	pushq	48(%rbp)
	movq	-104(%rbp), %rdx
	pushq	%rax
	movl	24(%rbp), %eax
	movq	-96(%rbp), %rsi
	pushq	$1
	pushq	%rax
	movl	16(%rbp), %eax
	pushq	%rax
	call	process
	addq	$48, %rsp
	movq	%r14, %rdi
	addl	%eax, -52(%rbp)
	call	OSSL_STORE_INFO_free@PLT
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L72:
	movq	56(%rbp), %rdx
	movq	bio_err(%rip), %rdi
	leaq	.LC3(%rip), %rsi
	call	BIO_printf@PLT
	movl	$1, -52(%rbp)
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L69:
	movq	%r14, %rdi
	call	OSSL_STORE_INFO_get0_PKEY@PLT
	movq	48(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	call	EVP_PKEY_print_private@PLT
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L68:
	movq	%r14, %rdi
	call	OSSL_STORE_INFO_get0_CERT@PLT
	movq	48(%rbp), %rdi
	movq	%rax, %rsi
	call	X509_print@PLT
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L71:
	movq	%r14, %rdi
	call	OSSL_STORE_INFO_get0_CRL@PLT
	movq	48(%rbp), %rdi
	movq	%rax, %rsi
	call	X509_CRL_print@PLT
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L70:
	movq	%r14, %rdi
	call	OSSL_STORE_INFO_get0_PARAMS@PLT
	movq	48(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	call	EVP_PKEY_print_params@PLT
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L62:
	movq	bio_err(%rip), %rdi
	movq	%r13, %rdx
	leaq	.LC2(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	movl	$1, -52(%rbp)
	jmp	.L7
	.cfi_endproc
.LFE1428:
	.size	process, .-process
	.section	.rodata.str1.1
.LC11:
	.string	"%s: Use -help for summary.\n"
	.section	.rodata.str1.8
	.align 8
.LC12:
	.string	"%s: only one search type can be given.\n"
	.section	.rodata.str1.1
.LC13:
	.string	"%s: criterion already given.\n"
.LC14:
	.string	"%s: subject already given.\n"
	.section	.rodata.str1.8
	.align 8
.LC15:
	.string	"%s: can't parse subject argument.\n"
	.section	.rodata.str1.1
.LC16:
	.string	"%s: issuer already given.\n"
	.section	.rodata.str1.8
	.align 8
.LC17:
	.string	"%s: can't parse issuer argument.\n"
	.align 8
.LC18:
	.string	"%s: serial number already given.\n"
	.align 8
.LC19:
	.string	"%s: can't parse serial number argument.\n"
	.align 8
.LC20:
	.string	"%s: fingerprint already given.\n"
	.align 8
.LC21:
	.string	"%s: can't parse fingerprint argument.\n"
	.section	.rodata.str1.1
.LC22:
	.string	"%s: alias already given.\n"
	.section	.rodata.str1.8
	.align 8
.LC23:
	.string	"../deps/openssl/openssl/apps/storeutl.c"
	.align 8
.LC24:
	.string	"%s: can't parse alias argument.\n"
	.align 8
.LC25:
	.string	"%s: No URI given, nothing to do...\n"
	.align 8
.LC26:
	.string	"%s: Unknown extra parameters after URI\n"
	.align 8
.LC27:
	.string	"%s: both -issuer and -serial must be given.\n"
	.section	.rodata.str1.1
.LC28:
	.string	"Error getting passwords\n"
	.text
	.p2align 4
	.globl	storeutl_main
	.type	storeutl_main, @function
storeutl_main:
.LFB1426:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	storeutl_options(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	xorl	%r14d, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	xorl	%r13d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	.L77(%rip), %rbx
	subq	$152, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	call	opt_init@PLT
	movq	$0, -88(%rbp)
	movq	%rax, -144(%rbp)
	movq	$0, -120(%rbp)
	movq	$0, -176(%rbp)
	movq	$0, -112(%rbp)
	movq	$0, -104(%rbp)
	movl	$0, -136(%rbp)
	movq	$0, -128(%rbp)
	movq	$0, -152(%rbp)
	movq	$0, -160(%rbp)
	movl	$0, -168(%rbp)
	movl	$0, -164(%rbp)
.L74:
	call	opt_next@PLT
	testl	%eax, %eax
	je	.L141
.L108:
	leal	1(%rax), %edx
	cmpl	$17, %edx
	ja	.L74
	movslq	(%rbx,%rdx,4), %rdx
	addq	%rbx, %rdx
	notrack jmp	*%rdx
	.section	.rodata
	.align 4
	.align 4
.L77:
	.long	.L91-.L77
	.long	.L74-.L77
	.long	.L90-.L77
	.long	.L89-.L77
	.long	.L88-.L77
	.long	.L87-.L77
	.long	.L86-.L77
	.long	.L85-.L77
	.long	.L84-.L77
	.long	.L83-.L77
	.long	.L83-.L77
	.long	.L83-.L77
	.long	.L82-.L77
	.long	.L81-.L77
	.long	.L80-.L77
	.long	.L79-.L77
	.long	.L78-.L77
	.long	.L76-.L77
	.text
.L86:
	movl	$1, %r12d
	call	opt_next@PLT
	testl	%eax, %eax
	jne	.L108
.L141:
	call	opt_num_rest@PLT
	movl	%eax, %ebx
	call	opt_rest@PLT
	movq	%rax, -184(%rbp)
	testl	%ebx, %ebx
	je	.L142
	cmpl	$1, %ebx
	jg	.L143
	testl	%r13d, %r13d
	je	.L125
	cmpl	$3, %r13d
	je	.L112
	cmpl	$4, %r13d
	je	.L113
	cmpl	$2, %r13d
	je	.L114
	movq	-104(%rbp), %rdi
	call	OSSL_STORE_SEARCH_by_name@PLT
	movq	%rax, %r9
	testq	%rax, %rax
	je	.L139
.L111:
	movq	-152(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	leaq	-96(%rbp), %rdx
	movq	%r9, -176(%rbp)
	call	app_passwd@PLT
	movq	-176(%rbp), %r9
	testl	%eax, %eax
	je	.L144
	movq	-96(%rbp), %rax
	movl	$32769, %edx
	movq	-160(%rbp), %rdi
	movl	$119, %esi
	movq	%r9, -152(%rbp)
	movl	$1, %ebx
	movq	%rax, -80(%rbp)
	movq	-184(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -72(%rbp)
	call	bio_open_default@PLT
	movq	-152(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %r10
	je	.L92
	movq	%r9, -160(%rbp)
	movq	%rax, -152(%rbp)
	call	get_ui_method@PLT
	movq	-152(%rbp), %r10
	leaq	-80(%rbp), %rdx
	movl	-168(%rbp), %ecx
	pushq	-144(%rbp)
	movq	%rax, %rsi
	movq	-184(%rbp), %rax
	movl	%r13d, %r8d
	movq	-160(%rbp), %r9
	pushq	%r10
	pushq	$0
	movq	(%rax), %rdi
	pushq	%rcx
	movl	-164(%rbp), %ecx
	pushq	%r12
	pushq	%rcx
	movl	-136(%rbp), %ecx
	movq	%r9, -144(%rbp)
	call	process
	movq	-152(%rbp), %r10
	movq	-144(%rbp), %r9
	addq	$48, %rsp
	movl	%eax, %ebx
	jmp	.L92
.L142:
	movq	-144(%rbp), %rdx
	movq	bio_err(%rip), %rdi
	leaq	.LC25(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L91:
	movq	-144(%rbp), %rdx
	leaq	.LC11(%rip), %rsi
.L137:
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
	movl	$1, %ebx
	call	BIO_printf@PLT
	xorl	%r9d, %r9d
	xorl	%r10d, %r10d
.L92:
	movq	-112(%rbp), %rdi
	movl	$311, %edx
	leaq	.LC23(%rip), %rsi
	movq	%r10, -136(%rbp)
	movq	%r9, -144(%rbp)
	call	CRYPTO_free@PLT
	movq	-120(%rbp), %rdi
	movl	$312, %edx
	leaq	.LC23(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%r15, %rdi
	call	ASN1_INTEGER_free@PLT
	movq	-104(%rbp), %rdi
	call	X509_NAME_free@PLT
	movq	%r14, %rdi
	call	X509_NAME_free@PLT
	movq	-144(%rbp), %r9
	movq	%r9, %rdi
	call	OSSL_STORE_SEARCH_free@PLT
	movq	-136(%rbp), %r10
	movq	%r10, %rdi
	call	BIO_free_all@PLT
	movq	-96(%rbp), %rdi
	movl	$318, %edx
	leaq	.LC23(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-128(%rbp), %rdi
	call	release_engine@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L145
	leaq	-40(%rbp), %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L76:
	.cfi_restore_state
	call	opt_unknown@PLT
	leaq	-88(%rbp), %rsi
	movq	%rax, %rdi
	call	opt_md@PLT
	testl	%eax, %eax
	jne	.L74
	jmp	.L91
.L78:
	testl	%r13d, %r13d
	jne	.L140
	cmpq	$0, -120(%rbp)
	jne	.L146
	call	opt_arg@PLT
	movl	$230, %edx
	leaq	.LC23(%rip), %rsi
	movq	%rax, %rdi
	call	CRYPTO_strdup@PLT
	movq	%rax, -120(%rbp)
	testq	%rax, %rax
	je	.L147
	movl	$4, %r13d
	jmp	.L74
.L79:
	testl	%r13d, %r13d
	jne	.L140
	cmpq	$0, -112(%rbp)
	jne	.L148
	movq	$0, -80(%rbp)
	call	opt_arg@PLT
	leaq	-80(%rbp), %rsi
	movq	%rax, %rdi
	call	OPENSSL_hexstr2buf@PLT
	movq	%rax, -112(%rbp)
	testq	%rax, %rax
	je	.L149
	movq	-80(%rbp), %rax
	movl	$3, %r13d
	movq	%rax, -176(%rbp)
	jmp	.L74
.L80:
	testl	%r13d, %r13d
	jne	.L140
	testq	%r15, %r15
	jne	.L150
	call	opt_arg@PLT
	xorl	%edi, %edi
	movq	%rax, %rsi
	call	s2i_ASN1_INTEGER@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L151
.L123:
	movl	$2, %r13d
	jmp	.L74
.L81:
	testl	%r13d, %r13d
	jne	.L140
	testq	%r14, %r14
	jne	.L152
	call	opt_arg@PLT
	movl	$1, %edx
	movl	$4096, %esi
	movq	%rax, %rdi
	call	parse_name@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	jne	.L123
	movq	-144(%rbp), %rdx
	leaq	.LC17(%rip), %rsi
	jmp	.L137
.L82:
	testl	%r13d, %r13d
	jne	.L140
	cmpq	$0, -104(%rbp)
	jne	.L153
	call	opt_arg@PLT
	movl	$1, %edx
	movl	$4096, %esi
	movq	%rax, %rdi
	call	parse_name@PLT
	movq	%rax, -104(%rbp)
	testq	%rax, %rax
	je	.L154
	movl	$1, %r13d
	jmp	.L74
.L83:
	movl	-136(%rbp), %edx
	testl	%edx, %edx
	jne	.L93
	cmpl	$8, %eax
	je	.L119
	cmpl	$9, %eax
	setne	%al
	movzbl	%al, %eax
	leal	3(%rax,%rax), %eax
	movl	%eax, -136(%rbp)
	jmp	.L74
.L84:
	movl	$1, -168(%rbp)
	jmp	.L74
.L85:
	movl	$1, -164(%rbp)
	jmp	.L74
.L87:
	call	opt_arg@PLT
	movq	%rax, -152(%rbp)
	jmp	.L74
.L88:
	call	opt_arg@PLT
	movq	%rax, -160(%rbp)
	jmp	.L74
.L89:
	call	opt_arg@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	setup_engine@PLT
	movq	%rax, -128(%rbp)
	jmp	.L74
.L90:
	leaq	storeutl_options(%rip), %rdi
	xorl	%ebx, %ebx
	call	opt_help@PLT
	xorl	%r9d, %r9d
	xorl	%r10d, %r10d
	jmp	.L92
.L125:
	xorl	%r9d, %r9d
	jmp	.L111
.L140:
	movq	-144(%rbp), %rdx
	leaq	.LC13(%rip), %rsi
	jmp	.L137
.L143:
	movq	-144(%rbp), %rdx
	movq	bio_err(%rip), %rdi
	leaq	.LC26(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L91
.L144:
	movq	bio_err(%rip), %rdi
	leaq	.LC28(%rip), %rsi
	movl	$1, %ebx
	movq	%r9, -136(%rbp)
	call	BIO_printf@PLT
	movq	-136(%rbp), %r9
	xorl	%r10d, %r10d
	jmp	.L92
.L119:
	movl	$4, -136(%rbp)
	jmp	.L74
.L114:
	testq	%r14, %r14
	je	.L127
	testq	%r15, %r15
	je	.L127
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	OSSL_STORE_SEARCH_by_issuer_serial@PLT
	movq	%rax, %r9
	testq	%rax, %rax
	jne	.L111
.L139:
	movq	bio_err(%rip), %rdi
	movq	%r9, -136(%rbp)
	movl	$1, %ebx
	call	ERR_print_errors@PLT
	movq	-136(%rbp), %r9
	xorl	%r10d, %r10d
	jmp	.L92
.L113:
	movq	-120(%rbp), %rdi
	call	OSSL_STORE_SEARCH_by_alias@PLT
	movq	%rax, %r9
	testq	%rax, %rax
	jne	.L111
	jmp	.L139
	.p2align 4,,10
	.p2align 3
.L112:
	movq	-176(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	-88(%rbp), %rdi
	call	OSSL_STORE_SEARCH_by_key_fingerprint@PLT
	movq	%rax, %r9
	testq	%rax, %rax
	jne	.L111
	jmp	.L139
.L152:
	movq	-144(%rbp), %rdx
	leaq	.LC16(%rip), %rsi
	jmp	.L137
.L146:
	movq	-144(%rbp), %rdx
	leaq	.LC22(%rip), %rsi
	jmp	.L137
.L147:
	movq	-144(%rbp), %rdx
	leaq	.LC24(%rip), %rsi
.L138:
	movq	bio_err(%rip), %rdi
	movl	$1, %ebx
	call	BIO_printf@PLT
	xorl	%r9d, %r9d
	xorl	%r10d, %r10d
	jmp	.L92
.L148:
	movq	-144(%rbp), %rdx
	leaq	.LC20(%rip), %rsi
	jmp	.L137
.L93:
	movq	-144(%rbp), %rdx
	leaq	.LC12(%rip), %rsi
	jmp	.L137
.L151:
	movq	-144(%rbp), %rdx
	leaq	.LC19(%rip), %rsi
	jmp	.L137
.L149:
	movq	-144(%rbp), %rdx
	leaq	.LC21(%rip), %rsi
	jmp	.L138
.L150:
	movq	-144(%rbp), %rdx
	leaq	.LC18(%rip), %rsi
	jmp	.L137
.L153:
	movq	-144(%rbp), %rdx
	leaq	.LC14(%rip), %rsi
	jmp	.L137
.L154:
	movq	-144(%rbp), %rdx
	leaq	.LC15(%rip), %rsi
	jmp	.L138
.L127:
	movq	-144(%rbp), %rdx
	leaq	.LC27(%rip), %rsi
	jmp	.L137
.L145:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1426:
	.size	storeutl_main, .-storeutl_main
	.globl	storeutl_options
	.section	.rodata.str1.8
	.align 8
.LC29:
	.string	"Usage: %s [options] uri\nValid options are:\n"
	.section	.rodata.str1.1
.LC30:
	.string	"help"
.LC31:
	.string	"Display this summary"
.LC32:
	.string	"out"
.LC33:
	.string	"Output file - default stdout"
.LC34:
	.string	"passin"
.LC35:
	.string	"Input file pass phrase source"
.LC36:
	.string	"text"
	.section	.rodata.str1.8
	.align 8
.LC37:
	.string	"Print a text form of the objects"
	.section	.rodata.str1.1
.LC38:
	.string	"noout"
.LC39:
	.string	"No PEM output, just status"
.LC40:
	.string	"certs"
.LC41:
	.string	"Search for certificates only"
.LC42:
	.string	"keys"
.LC43:
	.string	"Search for keys only"
.LC44:
	.string	"crls"
.LC45:
	.string	"Search for CRLs only"
.LC46:
	.string	"subject"
.LC47:
	.string	"Search by subject"
.LC48:
	.string	"issuer"
	.section	.rodata.str1.8
	.align 8
.LC49:
	.string	"Search by issuer and serial, issuer name"
	.section	.rodata.str1.1
.LC50:
	.string	"serial"
	.section	.rodata.str1.8
	.align 8
.LC51:
	.string	"Search by issuer and serial, serial number"
	.section	.rodata.str1.1
.LC52:
	.string	"fingerprint"
	.section	.rodata.str1.8
	.align 8
.LC53:
	.string	"Search by public key fingerprint, given in hex"
	.section	.rodata.str1.1
.LC54:
	.string	"alias"
.LC55:
	.string	"Search by alias"
.LC56:
	.string	"Any supported digest"
.LC57:
	.string	"engine"
	.section	.rodata.str1.8
	.align 8
.LC58:
	.string	"Use engine, possibly a hardware device"
	.section	.rodata.str1.1
.LC59:
	.string	"r"
.LC60:
	.string	"Recurse through names"
	.section	.data.rel.ro,"aw"
	.align 32
	.type	storeutl_options, @object
	.size	storeutl_options, 432
storeutl_options:
	.quad	OPT_HELP_STR
	.long	1
	.long	45
	.quad	.LC29
	.quad	.LC30
	.long	1
	.long	45
	.quad	.LC31
	.quad	.LC32
	.long	3
	.long	62
	.quad	.LC33
	.quad	.LC34
	.long	4
	.long	115
	.quad	.LC35
	.quad	.LC36
	.long	6
	.long	45
	.quad	.LC37
	.quad	.LC38
	.long	5
	.long	45
	.quad	.LC39
	.quad	.LC40
	.long	8
	.long	45
	.quad	.LC41
	.quad	.LC42
	.long	9
	.long	45
	.quad	.LC43
	.quad	.LC44
	.long	10
	.long	45
	.quad	.LC45
	.quad	.LC46
	.long	11
	.long	115
	.quad	.LC47
	.quad	.LC48
	.long	12
	.long	115
	.quad	.LC49
	.quad	.LC50
	.long	13
	.long	115
	.quad	.LC51
	.quad	.LC52
	.long	14
	.long	115
	.quad	.LC53
	.quad	.LC54
	.long	15
	.long	115
	.quad	.LC55
	.quad	.LC0
	.long	16
	.long	45
	.quad	.LC56
	.quad	.LC57
	.long	2
	.long	115
	.quad	.LC58
	.quad	.LC59
	.long	7
	.long	45
	.quad	.LC60
	.quad	0
	.zero	16
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
