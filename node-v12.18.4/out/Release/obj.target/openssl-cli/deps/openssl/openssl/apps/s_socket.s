	.file	"s_socket.c"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	""
.LC1:
	.string	":"
.LC2:
	.string	"unix "
.LC3:
	.string	"IPv6 "
.LC4:
	.string	"IPv4 "
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC5:
	.string	"../deps/openssl/openssl/apps/s_socket.c"
	.align 8
.LC6:
	.string	"assertion failed: (family == AF_UNSPEC || family == BIO_ADDRINFO_family(ai)) && (type == 0 || type == BIO_ADDRINFO_socktype(ai)) && (protocol == 0 || protocol == BIO_ADDRINFO_protocol(ai))"
	.align 8
.LC7:
	.string	"Can't bind %saddress for %s%s%s\n"
	.text
	.p2align 4
	.globl	init_client
	.type	init_client, @function
init_client:
.LFB1545:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%r9d, %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$88, %rsp
	movq	%rdi, -88(%rbp)
	movq	%rcx, -96(%rbp)
	movq	%r8, -104(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -72(%rbp)
	movq	$0, -64(%rbp)
	call	BIO_sock_init@PLT
	cmpl	$1, %eax
	je	.L62
.L1:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L63
	leaq	-40(%rbp), %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L62:
	.cfi_restore_state
	subq	$8, %rsp
	movl	%eax, %r12d
	leaq	-72(%rbp), %rax
	movl	24(%rbp), %r9d
	pushq	%rax
	movl	16(%rbp), %r8d
	movl	%r15d, %ecx
	xorl	%edx, %edx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	BIO_lookup_ex@PLT
	popq	%r8
	popq	%r9
	testl	%eax, %eax
	je	.L64
	movq	-96(%rbp), %rax
	orq	-104(%rbp), %rax
	jne	.L65
.L4:
	movq	-72(%rbp), %r13
	testq	%r13, %r13
	je	.L30
	cmpl	$6, 24(%rbp)
	movl	$16, %edx
	movl	$0, %eax
	movl	$0, -108(%rbp)
	cmove	%edx, %eax
	xorl	%ebx, %ebx
	movl	%eax, -112(%rbp)
	testl	%r15d, %r15d
	jne	.L7
.L10:
	movl	16(%rbp), %ecx
	testl	%ecx, %ecx
	jne	.L66
.L9:
	movl	24(%rbp), %edx
	testl	%edx, %edx
	jne	.L67
.L12:
	movq	-64(%rbp), %r14
	testq	%r14, %r14
	je	.L14
	.p2align 4,,10
	.p2align 3
.L16:
	movq	%r14, %rdi
	call	BIO_ADDRINFO_family@PLT
	movq	%r13, %rdi
	movl	%eax, %ebx
	call	BIO_ADDRINFO_family@PLT
	cmpl	%eax, %ebx
	je	.L15
	movq	%r14, %rdi
	call	BIO_ADDRINFO_next@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	jne	.L16
	xorl	%ebx, %ebx
.L17:
	movq	%r13, %rdi
	call	BIO_ADDRINFO_next@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L6
	testl	%r15d, %r15d
	je	.L10
.L7:
	movq	%r13, %rdi
	call	BIO_ADDRINFO_family@PLT
	cmpl	%r15d, %eax
	je	.L10
.L13:
	movl	$97, %edx
	leaq	.LC5(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	call	OPENSSL_die@PLT
	.p2align 4,,10
	.p2align 3
.L15:
	movq	%r13, %rdi
	addl	$1, -108(%rbp)
	call	BIO_ADDRINFO_protocol@PLT
	movq	%r13, %rdi
	movl	%eax, -116(%rbp)
	call	BIO_ADDRINFO_socktype@PLT
	movq	%r13, %rdi
	movl	%eax, %ebx
	call	BIO_ADDRINFO_family@PLT
	movl	-116(%rbp), %edx
	xorl	%ecx, %ecx
	movl	%ebx, %esi
	movl	%eax, %edi
	call	BIO_socket@PLT
	movq	-88(%rbp), %rcx
	movl	%eax, (%rcx)
	cmpl	$-1, %eax
	je	.L68
.L27:
	movq	%r14, %rdi
	call	BIO_ADDRINFO_address@PLT
	movl	$1, %edx
	movq	%rax, %rsi
	movq	-88(%rbp), %rax
	movl	(%rax), %edi
	call	BIO_bind@PLT
	testl	%eax, %eax
	je	.L69
.L18:
	movq	%r13, %rdi
	call	BIO_ADDRINFO_address@PLT
	movl	-112(%rbp), %edx
	movq	%rax, %rsi
	movq	-88(%rbp), %rax
	movl	(%rax), %edi
	call	BIO_connect@PLT
	testl	%eax, %eax
	je	.L70
.L6:
	movq	-88(%rbp), %rax
	cmpl	$-1, (%rax)
	jne	.L21
.L19:
	cmpq	$0, -64(%rbp)
	je	.L22
	movl	-108(%rbp), %eax
	testl	%eax, %eax
	jne	.L22
	cmpq	$0, -104(%rbp)
	leaq	.LC1(%rip), %r12
	je	.L71
.L23:
	movq	-96(%rbp), %rax
	leaq	.LC0(%rip), %rbx
	movq	-72(%rbp), %rdi
	testq	%rax, %rax
	cmove	%rbx, %rax
	movq	%rax, -96(%rbp)
	call	BIO_ADDRINFO_family@PLT
	leaq	.LC3(%rip), %rdx
	cmpl	$10, %eax
	je	.L25
	movq	-72(%rbp), %rdi
	call	BIO_ADDRINFO_family@PLT
	leaq	.LC4(%rip), %rdx
	cmpl	$2, %eax
	je	.L25
	movq	-72(%rbp), %rdi
	call	BIO_ADDRINFO_family@PLT
	leaq	.LC2(%rip), %rdx
	cmpl	$1, %eax
	cmovne	%rbx, %rdx
.L25:
	movq	-104(%rbp), %r9
	movq	-96(%rbp), %rcx
	movq	%r12, %r8
	xorl	%eax, %eax
	movq	bio_err(%rip), %rdi
	leaq	.LC7(%rip), %rsi
	call	BIO_printf@PLT
	call	ERR_clear_error@PLT
.L22:
	movq	bio_err(%rip), %rdi
	xorl	%r12d, %r12d
	call	ERR_print_errors@PLT
.L5:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L26
	call	BIO_ADDRINFO_free@PLT
.L26:
	movq	-72(%rbp), %rdi
	call	BIO_ADDRINFO_free@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L65:
	subq	$8, %rsp
	leaq	-64(%rbp), %rax
	movq	-104(%rbp), %rsi
	movq	-96(%rbp), %rdi
	pushq	%rax
	movl	24(%rbp), %r9d
	movl	%r15d, %ecx
	xorl	%edx, %edx
	movl	16(%rbp), %r8d
	call	BIO_lookup_ex@PLT
	popq	%rsi
	popq	%rdi
	testl	%eax, %eax
	jne	.L4
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L68:
	movq	%r14, %rbx
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L14:
	movq	%r13, %rdi
	call	BIO_ADDRINFO_protocol@PLT
	movq	%r13, %rdi
	movl	%eax, -120(%rbp)
	call	BIO_ADDRINFO_socktype@PLT
	movq	%r13, %rdi
	movl	%eax, -116(%rbp)
	call	BIO_ADDRINFO_family@PLT
	movl	-120(%rbp), %edx
	movl	-116(%rbp), %esi
	xorl	%ecx, %ecx
	movl	%eax, %edi
	call	BIO_socket@PLT
	movq	-88(%rbp), %rcx
	movl	%eax, (%rcx)
	cmpl	$-1, %eax
	je	.L17
	testq	%rbx, %rbx
	je	.L18
	movq	%rbx, %r14
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L67:
	movq	%r13, %rdi
	call	BIO_ADDRINFO_protocol@PLT
	cmpl	24(%rbp), %eax
	je	.L12
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L66:
	movq	%r13, %rdi
	call	BIO_ADDRINFO_socktype@PLT
	cmpl	16(%rbp), %eax
	je	.L9
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L21:
	call	ERR_clear_error@PLT
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L64:
	movq	bio_err(%rip), %rdi
	xorl	%r12d, %r12d
	call	ERR_print_errors@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L70:
	movq	-88(%rbp), %rbx
	movl	(%rbx), %edi
	call	BIO_closesocket@PLT
	movl	$-1, (%rbx)
	movq	%r14, %rbx
	jmp	.L17
.L71:
	leaq	.LC0(%rip), %rax
	movq	%rax, -104(%rbp)
	movq	%rax, %r12
	jmp	.L23
.L30:
	movl	$0, -108(%rbp)
	jmp	.L6
.L69:
	movq	-88(%rbp), %rbx
	movl	(%rbx), %edi
	call	BIO_closesocket@PLT
	movl	$-1, (%rbx)
	jmp	.L19
.L63:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1545:
	.size	init_client, .-init_client
	.section	.rodata.str1.1
.LC8:
	.string	"ACCEPT %s:%s\n"
.LC9:
	.string	"ACCEPT [%s]:%s\n"
	.section	.rodata.str1.8
	.align 8
.LC10:
	.string	"assertion failed: (family == AF_UNSPEC || family == BIO_ADDRINFO_family(res)) && (type == 0 || type == BIO_ADDRINFO_socktype(res)) && (protocol == 0 || protocol == BIO_ADDRINFO_protocol(res))"
	.section	.rodata.str1.1
.LC11:
	.string	"ACCEPT\n"
	.text
	.p2align 4
	.globl	do_server
	.type	do_server, @function
do_server:
.LFB1546:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movl	%r8d, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movl	%r9d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$312, %rsp
	.cfi_offset 3, -56
	movq	16(%rbp), %rax
	movq	%rdi, -336(%rbp)
	movq	%rsi, -328(%rbp)
	movq	%rax, -296(%rbp)
	movq	24(%rbp), %rax
	movl	%ecx, -308(%rbp)
	movq	%rax, -304(%rbp)
	movq	40(%rbp), %rax
	movq	%rax, -320(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -280(%rbp)
	call	BIO_sock_init@PLT
	cmpl	$1, %eax
	je	.L145
.L72:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L146
	leaq	-40(%rbp), %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L145:
	.cfi_restore_state
	subq	$8, %rsp
	movl	-308(%rbp), %ecx
	movq	%r12, %rsi
	movl	%r13d, %r9d
	leaq	-280(%rbp), %rax
	movq	-328(%rbp), %rdi
	movl	%r14d, %r8d
	movl	$1, %edx
	pushq	%rax
	call	BIO_lookup_ex@PLT
	popq	%rcx
	popq	%rsi
	movl	%eax, %r15d
	testl	%eax, %eax
	je	.L147
	movl	-308(%rbp), %edx
	movq	-280(%rbp), %rdi
	testl	%edx, %edx
	jne	.L148
.L75:
	testl	%r14d, %r14d
	jne	.L149
.L77:
	testl	%r13d, %r13d
	je	.L78
	call	BIO_ADDRINFO_protocol@PLT
	cmpl	%r13d, %eax
	jne	.L76
	movq	-280(%rbp), %rdi
.L78:
	call	BIO_ADDRINFO_family@PLT
	movq	-280(%rbp), %rdi
	xorl	%r12d, %r12d
	movl	%eax, %ebx
	call	BIO_ADDRINFO_socktype@PLT
	movq	-280(%rbp), %rdi
	movl	%eax, %r15d
	call	BIO_ADDRINFO_protocol@PLT
	movq	-280(%rbp), %rdi
	movl	%eax, -312(%rbp)
	call	BIO_ADDRINFO_address@PLT
	movq	-280(%rbp), %rdi
	movq	%rax, -344(%rbp)
	call	BIO_ADDRINFO_next@PLT
	cmpl	$10, %ebx
	sete	%r12b
	movq	%rax, %rdi
	leal	1(%r12,%r12), %r12d
	testq	%rax, %rax
	je	.L80
	movq	%rax, -352(%rbp)
	call	BIO_ADDRINFO_socktype@PLT
	movq	-352(%rbp), %rdi
	cmpl	%r15d, %eax
	je	.L150
.L80:
	movl	-312(%rbp), %edx
	movl	%ebx, %edi
	xorl	%ecx, %ecx
	movl	%r15d, %esi
	call	BIO_socket@PLT
	movl	%eax, %ebx
	cmpl	$-1, %eax
	je	.L82
	movl	%r12d, %edx
	movq	-344(%rbp), %r12
	movl	%eax, %edi
	movq	%r12, %rsi
	call	BIO_listen@PLT
	movl	%eax, %r15d
	testl	%eax, %eax
	jne	.L151
	movq	-280(%rbp), %rdi
	call	BIO_ADDRINFO_free@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	movl	%ebx, %edi
	call	BIO_closesocket@PLT
.L92:
	cmpl	$1, -308(%rbp)
	je	.L152
.L104:
	movq	ourpeer(%rip), %rdi
	call	BIO_ADDR_free@PLT
	movq	$0, ourpeer(%rip)
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L151:
	movq	%r12, %rdi
	call	BIO_ADDR_rawport@PLT
	movq	-280(%rbp), %rdi
	movl	%eax, %r12d
	call	BIO_ADDRINFO_free@PLT
	movq	$0, -280(%rbp)
	testw	%r12w, %r12w
	jne	.L84
	call	BIO_ADDR_new@PLT
	movq	%rax, -272(%rbp)
	testq	%rax, %rax
	je	.L85
	xorl	%esi, %esi
	leaq	-272(%rbp), %rdx
	movl	%ebx, %edi
	call	BIO_sock_info@PLT
	testl	%eax, %eax
	jne	.L153
.L85:
	xorl	%r15d, %r15d
	xorl	%r12d, %r12d
.L86:
	movq	-320(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	call	BIO_ctrl@PLT
	movl	$308, %edx
	leaq	.LC5(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
	movq	%r15, %rdi
	movl	$309, %edx
	leaq	.LC5(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-272(%rbp), %rdi
	call	BIO_ADDR_free@PLT
.L143:
	movl	%ebx, %edi
	xorl	%r15d, %r15d
	call	BIO_closesocket@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L148:
	call	BIO_ADDRINFO_family@PLT
	cmpl	-308(%rbp), %eax
	jne	.L76
	movq	-280(%rbp), %rdi
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L149:
	call	BIO_ADDRINFO_socktype@PLT
	cmpl	%r14d, %eax
	jne	.L76
	movq	-280(%rbp), %rdi
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L147:
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L82:
	movq	-280(%rbp), %rdi
	xorl	%r15d, %r15d
	call	BIO_ADDRINFO_free@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L152:
	movq	-328(%rbp), %rdi
	call	unlink@PLT
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L84:
	movq	-320(%rbp), %r15
	leaq	.LC11(%rip), %rsi
	xorl	%eax, %eax
	movq	%r15, %rdi
	call	BIO_printf@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	movq	%r15, %rdi
	call	BIO_ctrl@PLT
.L105:
	movq	-336(%rbp), %rax
	testq	%rax, %rax
	je	.L103
	movl	%ebx, (%rax)
	.p2align 4,,10
	.p2align 3
.L103:
	cmpl	$1, %r14d
	je	.L154
.L90:
	movq	-304(%rbp), %rcx
	movl	%r13d, %edx
	movl	%r14d, %esi
	movl	%ebx, %edi
	movq	-296(%rbp), %rax
	call	*%rax
	movl	%eax, %r15d
.L96:
	cmpl	$-1, 32(%rbp)
	je	.L99
	subl	$1, 32(%rbp)
	testl	%r15d, %r15d
	js	.L100
	movl	32(%rbp), %eax
	testl	%eax, %eax
	jne	.L103
.L100:
	movl	%ebx, %edi
	call	BIO_closesocket@PLT
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L99:
	testl	%r15d, %r15d
	js	.L100
	cmpl	$1, %r14d
	jne	.L90
.L154:
	movq	ourpeer(%rip), %rdi
	call	BIO_ADDR_free@PLT
	call	BIO_ADDR_new@PLT
	movq	%rax, ourpeer(%rip)
	movq	%rax, %rsi
	testq	%rax, %rax
	jne	.L91
	jmp	.L143
	.p2align 4,,10
	.p2align 3
.L155:
	call	BIO_sock_should_retry@PLT
	testl	%eax, %eax
	je	.L94
	movq	ourpeer(%rip), %rsi
.L91:
	movl	%ebx, %edi
	xorl	%edx, %edx
	call	BIO_accept_ex@PLT
	movl	%eax, %edi
	testl	%eax, %eax
	js	.L155
	movl	%eax, %r12d
	movl	$1, %esi
	call	BIO_set_tcp_ndelay@PLT
	movl	%r13d, %edx
	movl	$1, %esi
	movl	%r12d, %edi
	movq	-304(%rbp), %rcx
	movq	-296(%rbp), %rax
	call	*%rax
	movl	$1, %esi
	movl	%r12d, %edi
	movl	%eax, %r15d
	call	shutdown@PLT
	movl	%r12d, %ecx
	movl	$1, %eax
	movdqa	.LC12(%rip), %xmm0
	salq	%cl, %rax
	leaq	-272(%rbp), %rdx
	movslq	%r12d, %rcx
	movl	%ebx, -336(%rbp)
	movq	%rax, -320(%rbp)
	leal	1(%r12), %eax
	movq	%rcx, %rbx
	leaq	-256(%rbp), %rsi
	movl	%r15d, -312(%rbp)
	movq	%rdx, %r15
	movl	%r14d, -344(%rbp)
	movl	%eax, %r14d
	movl	%r13d, -352(%rbp)
	movq	%rsi, %r13
	movaps	%xmm0, -272(%rbp)
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L95:
	leaq	-128(%rbp), %rsi
	movl	$64, %edx
	movl	%r12d, %edi
	call	read@PLT
	testq	%rax, %rax
	jle	.L142
.L97:
	movl	$16, %ecx
	movq	%r13, %rdi
	xorl	%eax, %eax
#APP
# 367 "../deps/openssl/openssl/apps/s_socket.c" 1
	cld; rep; stosq
# 0 "" 2
#NO_APP
	movq	%rbx, %rdi
	call	__fdelt_chk@PLT
	xorl	%ecx, %ecx
	movq	%r15, %r8
	movq	%r13, %rsi
	movq	-320(%rbp), %rdx
	movl	%r14d, %edi
	orq	%rdx, -256(%rbp,%rax,8)
	xorl	%edx, %edx
	call	select@PLT
	testl	%eax, %eax
	jg	.L95
.L142:
	movl	%r12d, %edi
	movl	-336(%rbp), %ebx
	movl	-312(%rbp), %r15d
	movl	-344(%rbp), %r14d
	movl	-352(%rbp), %r13d
	call	BIO_closesocket@PLT
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L150:
	call	BIO_ADDRINFO_protocol@PLT
	cmpl	-312(%rbp), %eax
	movq	-352(%rbp), %rdi
	jne	.L80
	cmpl	$2, %ebx
	je	.L156
	cmpl	$10, %ebx
	jne	.L80
	call	BIO_ADDRINFO_family@PLT
	cmpl	$2, %eax
	movl	$1, %eax
	cmove	%eax, %r12d
	jmp	.L80
.L153:
	movq	-272(%rbp), %rdi
	movl	$1, %esi
	call	BIO_ADDR_hostname_string@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L85
	movq	-272(%rbp), %rdi
	movl	$1, %esi
	call	BIO_ADDR_service_string@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L86
	movl	$58, %esi
	movq	%r12, %rdi
	call	strchr@PLT
	leaq	.LC8(%rip), %rsi
	movq	%r15, %rcx
	movq	%r12, %rdx
	testq	%rax, %rax
	leaq	.LC9(%rip), %rax
	movq	-320(%rbp), %rdi
	cmovne	%rax, %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L86
	movq	-320(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	call	BIO_ctrl@PLT
	movq	%r12, %rdi
	movl	$308, %edx
	leaq	.LC5(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%r15, %rdi
	movl	$309, %edx
	leaq	.LC5(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-272(%rbp), %rdi
	call	BIO_ADDR_free@PLT
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L94:
	movq	bio_err(%rip), %rdi
	movl	%eax, %r15d
	call	ERR_print_errors@PLT
	movl	%ebx, %edi
	call	BIO_closesocket@PLT
	jmp	.L92
.L156:
	call	BIO_ADDRINFO_family@PLT
	movq	-352(%rbp), %rdi
	cmpl	$10, %eax
	jne	.L80
	call	BIO_ADDRINFO_address@PLT
	movl	$10, %ebx
	movq	%rax, -344(%rbp)
	jmp	.L80
.L146:
	call	__stack_chk_fail@PLT
.L76:
	movl	$231, %edx
	leaq	.LC5(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	call	OPENSSL_die@PLT
	.cfi_endproc
.LFE1546:
	.size	do_server, .-do_server
	.globl	ourpeer
	.bss
	.align 8
	.type	ourpeer, @object
	.size	ourpeer, 8
ourpeer:
	.zero	8
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC12:
	.quad	0
	.quad	500000
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
