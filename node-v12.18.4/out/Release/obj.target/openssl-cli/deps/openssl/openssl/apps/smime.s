	.file	"smime.c"
	.text
	.p2align 4
	.type	smime_cb, @function
smime_cb:
.LFB1437:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%edi, %r13d
	movq	%rsi, %rdi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	call	X509_STORE_CTX_get_error@PLT
	cmpl	$43, %eax
	je	.L2
	testl	%eax, %eax
	jne	.L3
	cmpl	$2, %r13d
	jne	.L3
.L2:
	movq	%r12, %rdi
	call	policies_print@PLT
.L3:
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1437:
	.size	smime_cb, .-smime_cb
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"\r\n"
.LC1:
	.string	"\n"
.LC2:
	.string	"%s: Use -help for summary.\n"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"%s: Must have -signer before -inkey\n"
	.align 8
.LC4:
	.string	"Multiple signers or keys not allowed\n"
	.align 8
.LC5:
	.string	"Illegal -inkey without -signer\n"
	.align 8
.LC6:
	.string	"No signer certificate specified\n"
	.align 8
.LC7:
	.string	"No recipient certificate or key specified\n"
	.align 8
.LC8:
	.string	"No recipient(s) certificate(s) specified\n"
	.section	.rodata.str1.1
.LC9:
	.string	"Error getting password\n"
.LC10:
	.string	"recipient certificate file"
.LC11:
	.string	"certificate file"
.LC12:
	.string	"signing key file"
	.section	.rodata.str1.8
	.align 8
.LC13:
	.string	"Bad input format for PKCS#7 file\n"
	.section	.rodata.str1.1
.LC14:
	.string	"Error reading S/MIME message\n"
.LC15:
	.string	"rb"
.LC16:
	.string	"Can't read content file %s\n"
.LC17:
	.string	"signer certificate"
	.section	.rodata.str1.8
	.align 8
.LC18:
	.string	"Error creating PKCS#7 structure\n"
	.align 8
.LC19:
	.string	"Error decrypting PKCS#7 structure\n"
	.section	.rodata.str1.1
.LC20:
	.string	"Verification successful\n"
.LC21:
	.string	"w"
.LC22:
	.string	"Error writing signers to %s\n"
.LC23:
	.string	"Verification failure\n"
.LC24:
	.string	"To: %s%s"
.LC25:
	.string	"From: %s%s"
.LC26:
	.string	"Subject: %s%s"
	.section	.rodata.str1.8
	.align 8
.LC27:
	.string	"Bad output format for PKCS#7 file\n"
	.section	.rodata.str1.1
.LC28:
	.string	"Error writing output\n"
	.section	.rodata.str1.8
	.align 8
.LC29:
	.string	"../deps/openssl/openssl/apps/smime.c"
	.text
	.p2align 4
	.globl	smime_main
	.type	smime_main, @function
smime_main:
.LFB1435:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%edi, %r12d
	pushq	%rbx
	subq	$264, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	$0, -80(%rbp)
	movq	$0, -72(%rbp)
	movq	$0, -64(%rbp)
	movl	$32775, -108(%rbp)
	movl	$32775, -104(%rbp)
	movl	$32773, -100(%rbp)
	call	X509_VERIFY_PARAM_new@PLT
	movl	$1, %r10d
	movq	%rax, -136(%rbp)
	testq	%rax, %rax
	jne	.L264
.L9:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L265
	addq	$264, %rsp
	movl	%r10d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L264:
	.cfi_restore_state
	movq	%r13, %rsi
	movl	%r12d, %edi
	leaq	smime_options(%rip), %rdx
	xorl	%ebx, %ebx
	call	opt_init@PLT
	xorl	%r14d, %r14d
	xorl	%r15d, %r15d
	movq	$0, -176(%rbp)
	movq	%rax, -192(%rbp)
	leaq	.LC1(%rip), %rax
	leaq	.L17(%rip), %r13
	movq	%rax, -224(%rbp)
	leaq	.L27(%rip), %r12
	movl	$0, -284(%rbp)
	movl	$0, -208(%rbp)
	movl	$64, -128(%rbp)
	movq	$0, -248(%rbp)
	movq	$0, -256(%rbp)
	movq	$0, -264(%rbp)
	movq	$0, -184(%rbp)
	movq	$0, -200(%rbp)
	movq	$0, -168(%rbp)
	movq	$0, -240(%rbp)
	movq	$0, -216(%rbp)
	movq	$0, -232(%rbp)
	movq	$0, -120(%rbp)
	movq	$0, -144(%rbp)
	movq	$0, -280(%rbp)
	movq	$0, -272(%rbp)
	movq	$0, -160(%rbp)
	movq	$0, -152(%rbp)
.L11:
	call	opt_next@PLT
	testl	%eax, %eax
	je	.L266
	cmpl	$34, %eax
	jg	.L12
	cmpl	$-1, %eax
	jl	.L11
	addl	$1, %eax
	cmpl	$35, %eax
	ja	.L11
	movslq	(%r12,%rax,4), %rax
	addq	%r12, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L27:
	.long	.L59-.L27
	.long	.L11-.L27
	.long	.L58-.L27
	.long	.L57-.L27
	.long	.L150-.L27
	.long	.L56-.L27
	.long	.L55-.L27
	.long	.L54-.L27
	.long	.L53-.L27
	.long	.L52-.L27
	.long	.L51-.L27
	.long	.L50-.L27
	.long	.L49-.L27
	.long	.L48-.L27
	.long	.L47-.L27
	.long	.L46-.L27
	.long	.L45-.L27
	.long	.L44-.L27
	.long	.L43-.L27
	.long	.L42-.L27
	.long	.L42-.L27
	.long	.L41-.L27
	.long	.L40-.L27
	.long	.L39-.L27
	.long	.L38-.L27
	.long	.L37-.L27
	.long	.L36-.L27
	.long	.L35-.L27
	.long	.L34-.L27
	.long	.L33-.L27
	.long	.L32-.L27
	.long	.L31-.L27
	.long	.L30-.L27
	.long	.L29-.L27
	.long	.L28-.L27
	.long	.L26-.L27
	.text
.L150:
	movl	$34, %ebx
	jmp	.L11
.L57:
	movl	$17, %ebx
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L12:
	cmpl	$1502, %eax
	jle	.L267
	leal	-2001(%rax), %edx
	cmpl	$38, %edx
	ja	.L11
	movslq	0(%r13,%rdx,4), %rdx
	addq	%r13, %rdx
	notrack jmp	*%rdx
	.section	.rodata
	.align 4
	.align 4
.L17:
	.long	.L24-.L17
	.long	.L24-.L17
	.long	.L24-.L17
	.long	.L24-.L17
	.long	.L24-.L17
	.long	.L24-.L17
	.long	.L24-.L17
	.long	.L24-.L17
	.long	.L24-.L17
	.long	.L24-.L17
	.long	.L24-.L17
	.long	.L24-.L17
	.long	.L24-.L17
	.long	.L24-.L17
	.long	.L24-.L17
	.long	.L24-.L17
	.long	.L24-.L17
	.long	.L24-.L17
	.long	.L24-.L17
	.long	.L24-.L17
	.long	.L24-.L17
	.long	.L24-.L17
	.long	.L24-.L17
	.long	.L24-.L17
	.long	.L24-.L17
	.long	.L24-.L17
	.long	.L24-.L17
	.long	.L24-.L17
	.long	.L24-.L17
	.long	.L24-.L17
	.long	.L11-.L17
	.long	.L23-.L17
	.long	.L22-.L17
	.long	.L149-.L17
	.long	.L21-.L17
	.long	.L20-.L17
	.long	.L19-.L17
	.long	.L18-.L17
	.long	.L16-.L17
	.text
.L149:
	movl	$1, %r15d
	jmp	.L11
.L22:
	movl	$1, %r14d
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L267:
	cmpl	$1500, %eax
	jle	.L11
	movl	%eax, %edi
	call	opt_rand@PLT
	testl	%eax, %eax
	jne	.L11
	movl	%eax, %r10d
	jmp	.L157
.L75:
	cmpq	$0, -168(%rbp)
	jne	.L254
	cmpq	$0, -152(%rbp)
	jne	.L80
	movq	bio_err(%rip), %rdi
	leaq	.LC6(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L59:
	movq	-192(%rbp), %rdx
	movq	bio_err(%rip), %rdi
	leaq	.LC2(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	xorl	%r10d, %r10d
.L157:
	movq	$0, -120(%rbp)
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	xorl	%r12d, %r12d
	movq	$0, -192(%rbp)
	movq	$0, -200(%rbp)
	movq	$0, -144(%rbp)
	movq	$0, -184(%rbp)
.L63:
	movq	X509_free@GOTPCREL(%rip), %r15
	movq	-200(%rbp), %rdi
	movl	%r10d, -128(%rbp)
	movq	%r15, %rsi
	call	OPENSSL_sk_pop_free@PLT
	movq	-88(%rbp), %rdi
	movq	%r15, %rsi
	call	OPENSSL_sk_pop_free@PLT
	movq	-136(%rbp), %rdi
	call	X509_VERIFY_PARAM_free@PLT
	movq	-152(%rbp), %rdi
	call	OPENSSL_sk_free@PLT
	movq	-160(%rbp), %rdi
	call	OPENSSL_sk_free@PLT
	movq	%r14, %rdi
	call	X509_STORE_free@PLT
	xorl	%edi, %edi
	call	X509_free@PLT
	movq	-192(%rbp), %rdi
	call	X509_free@PLT
	movq	%r13, %rdi
	call	X509_free@PLT
	movq	-120(%rbp), %rdi
	call	EVP_PKEY_free@PLT
	movq	-144(%rbp), %rdi
	call	PKCS7_free@PLT
	movq	-176(%rbp), %rdi
	call	release_engine@PLT
	movq	%r12, %rdi
	call	BIO_free@PLT
	movq	-96(%rbp), %rdi
	call	BIO_free@PLT
	movq	-184(%rbp), %rdi
	call	BIO_free_all@PLT
	movq	-64(%rbp), %rdi
	movl	$612, %edx
	leaq	.LC29(%rip), %rsi
	call	CRYPTO_free@PLT
	movl	-128(%rbp), %r10d
	jmp	.L9
.L16:
	call	opt_arg@PLT
	movq	%rax, -232(%rbp)
	jmp	.L11
.L18:
	call	opt_arg@PLT
	leaq	-104(%rbp), %rdx
	movl	$10, %esi
	movq	%rax, %rdi
	call	opt_format@PLT
	testl	%eax, %eax
	jne	.L11
	jmp	.L59
.L19:
	call	opt_arg@PLT
	movq	%rax, -240(%rbp)
	jmp	.L11
.L20:
	call	opt_arg@PLT
	leaq	-108(%rbp), %rdx
	movl	$10, %esi
	movq	%rax, %rdi
	call	opt_format@PLT
	testl	%eax, %eax
	jne	.L11
	jmp	.L59
.L21:
	call	opt_arg@PLT
	movq	%rax, -216(%rbp)
	jmp	.L11
.L23:
	call	opt_arg@PLT
	movq	%rax, -280(%rbp)
	jmp	.L11
.L24:
	movq	-136(%rbp), %rsi
	movl	%eax, %edi
	call	opt_verify@PLT
	testl	%eax, %eax
	je	.L59
	addl	$1, -284(%rbp)
	jmp	.L11
.L26:
	call	opt_arg@PLT
	movq	%rax, -272(%rbp)
	jmp	.L11
.L28:
	call	opt_arg@PLT
	movq	%rax, -144(%rbp)
	jmp	.L11
.L29:
	call	opt_arg@PLT
	leaq	-100(%rbp), %rdx
	movl	$1982, %esi
	movq	%rax, %rdi
	call	opt_format@PLT
	testl	%eax, %eax
	jne	.L11
	jmp	.L59
.L30:
	cmpq	$0, -120(%rbp)
	je	.L68
	cmpq	$0, -168(%rbp)
	je	.L268
	cmpq	$0, -152(%rbp)
	je	.L269
.L70:
	movq	-168(%rbp), %rsi
	movq	-152(%rbp), %rdi
	call	OPENSSL_sk_push@PLT
	cmpq	$0, -160(%rbp)
	je	.L270
.L71:
	movq	-120(%rbp), %rsi
	movq	-160(%rbp), %rdi
	call	OPENSSL_sk_push@PLT
	movq	$0, -168(%rbp)
.L68:
	call	opt_arg@PLT
	movq	%rax, -120(%rbp)
	jmp	.L11
.L31:
	call	opt_unknown@PLT
	leaq	-80(%rbp), %rsi
	movq	%rax, %rdi
	call	opt_cipher@PLT
	testl	%eax, %eax
	jne	.L11
	jmp	.L59
.L32:
	call	opt_arg@PLT
	leaq	-72(%rbp), %rsi
	movq	%rax, %rdi
	call	opt_md@PLT
	testl	%eax, %eax
	jne	.L11
	jmp	.L59
.L33:
	call	opt_arg@PLT
	movq	%rax, -200(%rbp)
	jmp	.L11
.L34:
	cmpq	$0, -168(%rbp)
	je	.L64
	cmpq	$0, -152(%rbp)
	je	.L271
.L65:
	movq	-168(%rbp), %rsi
	movq	-152(%rbp), %rdi
	call	OPENSSL_sk_push@PLT
	movq	-120(%rbp), %rcx
	movq	-168(%rbp), %rax
	testq	%rcx, %rcx
	cmovne	%rcx, %rax
	cmpq	$0, -160(%rbp)
	movq	%rax, -120(%rbp)
	je	.L272
.L67:
	movq	-120(%rbp), %rsi
	movq	-160(%rbp), %rdi
	call	OPENSSL_sk_push@PLT
	movq	$0, -120(%rbp)
.L64:
	call	opt_arg@PLT
	movq	%rax, -168(%rbp)
	jmp	.L11
.L35:
	call	opt_arg@PLT
	movq	%rax, -248(%rbp)
	jmp	.L11
.L36:
	call	opt_arg@PLT
	movq	%rax, -256(%rbp)
	jmp	.L11
.L37:
	call	opt_arg@PLT
	movq	%rax, -264(%rbp)
	jmp	.L11
.L38:
	call	opt_arg@PLT
	movq	%rax, -184(%rbp)
	jmp	.L11
.L39:
	call	opt_arg@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	setup_engine@PLT
	movq	%rax, -176(%rbp)
	jmp	.L11
.L40:
	leaq	.LC0(%rip), %rax
	orl	$2048, -128(%rbp)
	movq	%rax, -224(%rbp)
	jmp	.L11
.L41:
	movl	$0, -208(%rbp)
	jmp	.L11
.L42:
	movl	$1, -208(%rbp)
	jmp	.L11
.L43:
	orl	$4, -128(%rbp)
	jmp	.L11
.L44:
	orl	$128, -128(%rbp)
	jmp	.L11
.L45:
	orl	$512, -128(%rbp)
	jmp	.L11
.L46:
	andl	$-65, -128(%rbp)
	jmp	.L11
.L47:
	orl	$256, -128(%rbp)
	jmp	.L11
.L48:
	orl	$2, -128(%rbp)
	jmp	.L11
.L49:
	orl	$8, -128(%rbp)
	jmp	.L11
.L50:
	orl	$32, -128(%rbp)
	jmp	.L11
.L51:
	orl	$16, -128(%rbp)
	jmp	.L11
.L52:
	orl	$1, -128(%rbp)
	jmp	.L11
.L53:
	movl	$53, %ebx
	jmp	.L11
.L54:
	movl	$36, %ebx
	jmp	.L11
.L55:
	movl	$118, %ebx
	jmp	.L11
.L56:
	movl	$83, %ebx
	jmp	.L11
.L58:
	leaq	smime_options(%rip), %rdi
	call	opt_help@PLT
	xorl	%r10d, %r10d
	jmp	.L157
	.p2align 4,,10
	.p2align 3
.L266:
	movl	%eax, -296(%rbp)
	call	opt_num_rest@PLT
	movl	%eax, %r12d
	call	opt_rest@PLT
	movl	-296(%rbp), %r10d
	movq	%rax, %r13
	movl	%ebx, %eax
	andl	$64, %eax
	movl	%eax, -288(%rbp)
	jne	.L73
	movq	-152(%rbp), %rax
	orq	-160(%rbp), %rax
	movq	%rax, -296(%rbp)
	jne	.L273
	cmpl	$34, %ebx
	je	.L274
	cmpl	$17, %ebx
	je	.L275
	testl	%ebx, %ebx
	je	.L276
.L85:
	movq	-184(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	leaq	-64(%rbp), %rdx
	movl	%r10d, -152(%rbp)
	call	app_passwd@PLT
	movl	-152(%rbp), %r10d
	testl	%eax, %eax
	je	.L277
	andl	$-65, -128(%rbp)
	movq	$0, -160(%rbp)
	movq	$0, -152(%rbp)
.L83:
	movl	%ebx, %eax
	andl	$32, %eax
	movl	%eax, -184(%rbp)
	testb	$16, %bl
	jne	.L87
	testb	$-128, -128(%rbp)
	je	.L88
	movl	$2, -104(%rbp)
	testl	%eax, %eax
	jne	.L89
.L91:
	movl	$2, -108(%rbp)
.L90:
	cmpl	$17, %ebx
	jne	.L89
	cmpq	$0, -80(%rbp)
	je	.L278
.L92:
	movl	%r10d, -120(%rbp)
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, -200(%rbp)
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L160
	movq	0(%r13), %rdi
	movl	-120(%rbp), %r10d
	testq	%rdi, %rdi
	jne	.L95
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L280:
	movq	%r12, %rdi
	addq	$8, %r13
	call	OPENSSL_sk_push@PLT
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L279
.L95:
	movl	$32773, %esi
	leaq	.LC10(%rip), %rdx
	call	load_cert@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	jne	.L280
	movq	$0, -120(%rbp)
	movq	%rax, %r12
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	movq	$0, -192(%rbp)
	movl	$2, %r10d
	movq	$0, -144(%rbp)
	movq	$0, -184(%rbp)
.L93:
	movq	bio_err(%rip), %rdi
	movl	%r10d, -128(%rbp)
	call	ERR_print_errors@PLT
	movl	-128(%rbp), %r10d
	jmp	.L63
.L73:
	cmpq	$0, -120(%rbp)
	je	.L75
	cmpq	$0, -168(%rbp)
	je	.L281
.L254:
	cmpq	$0, -152(%rbp)
	je	.L282
.L77:
	movq	-168(%rbp), %rsi
	movq	-152(%rbp), %rdi
	movl	%r10d, -192(%rbp)
	call	OPENSSL_sk_push@PLT
	cmpq	$0, -160(%rbp)
	movl	-192(%rbp), %r10d
	je	.L283
.L78:
	movq	-120(%rbp), %rax
	movq	-168(%rbp), %rcx
	movl	%r10d, -192(%rbp)
	movq	-160(%rbp), %rdi
	testq	%rax, %rax
	cmovne	%rax, %rcx
	movq	%rcx, %rsi
	call	OPENSSL_sk_push@PLT
	movl	-192(%rbp), %r10d
.L80:
	movq	-184(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	leaq	-64(%rbp), %rdx
	movl	%r10d, -120(%rbp)
	call	app_passwd@PLT
	movl	-120(%rbp), %r10d
	testl	%eax, %eax
	je	.L158
	movq	$0, -168(%rbp)
	movq	$0, -120(%rbp)
	jmp	.L83
.L88:
	movl	-184(%rbp), %eax
	testl	%eax, %eax
	je	.L90
.L89:
	movq	-144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L96
	xorl	%ecx, %ecx
	leaq	-88(%rbp), %rsi
	leaq	.LC11(%rip), %r8
	movl	$32773, %edx
	movl	%r10d, -144(%rbp)
	call	load_certs@PLT
	movl	-144(%rbp), %r10d
	testl	%eax, %eax
	je	.L284
.L96:
	movq	-200(%rbp), %r13
	cmpl	$34, %ebx
	sete	%r12b
	testq	%r13, %r13
	setne	%al
	andb	%r12b, %al
	movb	%al, -296(%rbp)
	jne	.L285
	cmpl	$34, %ebx
	je	.L99
	cmpl	$83, %ebx
	je	.L286
	movq	$0, -200(%rbp)
	movq	$0, -192(%rbp)
	movq	$0, -120(%rbp)
.L102:
	movl	-108(%rbp), %edx
	movq	-216(%rbp), %rdi
	movl	$114, %esi
	movl	%r10d, -144(%rbp)
	call	bio_open_default@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L167
	movl	-184(%rbp), %r8d
	movl	-144(%rbp), %r10d
	testl	%r8d, %r8d
	je	.L168
.L145:
	movl	-108(%rbp), %eax
	movl	%r10d, -184(%rbp)
	cmpl	$32775, %eax
	je	.L287
	cmpl	$32773, %eax
	je	.L288
	cmpl	$4, %eax
	jne	.L107
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	d2i_PKCS7_bio@PLT
	movl	-184(%rbp), %r10d
	movq	%rax, -144(%rbp)
.L105:
	cmpq	$0, -144(%rbp)
	je	.L289
	movq	-232(%rbp), %r13
	testq	%r13, %r13
	je	.L103
	movq	-96(%rbp), %rdi
	movl	%r10d, -216(%rbp)
	call	BIO_free@PLT
	leaq	.LC15(%rip), %rsi
	movq	%r13, %rdi
	call	BIO_new_file@PLT
	movl	-216(%rbp), %r10d
	testq	%rax, %rax
	movq	%rax, -184(%rbp)
	movq	%rax, -96(%rbp)
	je	.L290
.L103:
	movl	-104(%rbp), %edx
	movq	-240(%rbp), %rdi
	movl	$119, %esi
	movl	%r10d, -216(%rbp)
	call	bio_open_default@PLT
	movq	%rax, -184(%rbp)
	testq	%rax, %rax
	je	.L169
	cmpl	$36, %ebx
	movl	-216(%rbp), %r10d
	je	.L291
	cmpl	$17, %ebx
	je	.L292
	movl	-288(%rbp), %edx
	testl	%edx, %edx
	je	.L171
	cmpl	$83, %ebx
	je	.L293
	orl	$32768, -128(%rbp)
.L119:
	movl	%ebx, -216(%rbp)
	movq	-152(%rbp), %r13
	xorl	%r15d, %r15d
	movq	%r12, -208(%rbp)
	movq	-168(%rbp), %rbx
	movl	%r10d, -232(%rbp)
	movq	-120(%rbp), %r12
	jmp	.L121
.L122:
	movl	%r15d, %esi
	movq	%r13, %rdi
	call	OPENSSL_sk_value@PLT
	movq	-160(%rbp), %rdi
	movl	%r15d, %esi
	movq	%rax, %rbx
	call	OPENSSL_sk_value@PLT
	leaq	.LC17(%rip), %rdx
	movl	$32773, %esi
	movq	%rbx, %rdi
	movq	%rax, -120(%rbp)
	call	load_cert@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L175
	movq	-120(%rbp), %r11
	movl	-100(%rbp), %esi
	leaq	.LC12(%rip), %r9
	xorl	%edx, %edx
	movq	-176(%rbp), %r8
	movq	-64(%rbp), %rcx
	movq	%r11, %rdi
	call	load_key@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L175
	movl	-128(%rbp), %r8d
	movq	-72(%rbp), %rcx
	movq	%rax, %rdx
	movq	%r14, %rsi
	movq	-144(%rbp), %rdi
	call	PKCS7_sign_add_signer@PLT
	testq	%rax, %rax
	je	.L176
	movq	%r14, %rdi
	addl	$1, %r15d
	call	X509_free@PLT
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	EVP_PKEY_free@PLT
.L121:
	movq	%r13, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r15d
	jl	.L122
	movq	%r12, -120(%rbp)
	movl	-232(%rbp), %r10d
	xorl	%r14d, %r14d
	movq	%rbx, -168(%rbp)
	movl	-216(%rbp), %ebx
	movq	-208(%rbp), %r12
	cmpl	$83, %ebx
	je	.L294
.L114:
	cmpq	$0, -144(%rbp)
	je	.L295
	cmpb	$0, -296(%rbp)
	jne	.L296
	cmpl	$36, %ebx
	jne	.L125
	movl	-128(%rbp), %r15d
	movq	-144(%rbp), %rbx
	movq	%r14, %rdx
	movl	%r10d, -208(%rbp)
	movq	-184(%rbp), %r8
	movq	-96(%rbp), %rcx
	movq	-88(%rbp), %rsi
	movl	%r15d, %r9d
	movq	%rbx, %rdi
	call	PKCS7_verify@PLT
	testl	%eax, %eax
	je	.L126
	movq	bio_err(%rip), %rdi
	leaq	.LC20(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	-88(%rbp), %rsi
	movl	%r15d, %edx
	movq	%rbx, %rdi
	call	PKCS7_get0_signers@PLT
	movq	-168(%rbp), %rdi
	movl	-208(%rbp), %r10d
	movq	%rax, %r15
	testq	%rdi, %rdi
	je	.L127
	leaq	.LC21(%rip), %rsi
	call	BIO_new_file@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L128
	movl	-208(%rbp), %r10d
	movq	%r12, -128(%rbp)
	xorl	%ebx, %ebx
	movq	%r15, %r12
	movl	%r10d, %r15d
	jmp	.L129
.L130:
	movl	%ebx, %esi
	movq	%r12, %rdi
	addl	$1, %ebx
	call	OPENSSL_sk_value@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	PEM_write_bio_X509@PLT
.L129:
	movq	%r12, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jl	.L130
	movq	%r13, %rdi
	movl	%r15d, -168(%rbp)
	movq	%r12, %r15
	movq	-128(%rbp), %r12
	call	BIO_free@PLT
	movl	-168(%rbp), %r10d
.L127:
	movq	%r15, %rdi
	movl	%r10d, -128(%rbp)
	xorl	%r13d, %r13d
	call	OPENSSL_sk_free@PLT
	movl	-128(%rbp), %r10d
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L271:
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, -152(%rbp)
	testq	%rax, %rax
	jne	.L65
.L155:
	movq	$0, -120(%rbp)
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	xorl	%r12d, %r12d
	movq	$0, -192(%rbp)
	xorl	%r10d, %r10d
	movq	$0, -200(%rbp)
	movq	$0, -144(%rbp)
	movq	$0, -184(%rbp)
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L272:
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, -160(%rbp)
	testq	%rax, %rax
	jne	.L67
	jmp	.L155
	.p2align 4,,10
	.p2align 3
.L269:
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, -152(%rbp)
	testq	%rax, %rax
	jne	.L70
	jmp	.L155
	.p2align 4,,10
	.p2align 3
.L270:
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, -160(%rbp)
	testq	%rax, %rax
	jne	.L71
	jmp	.L155
	.p2align 4,,10
	.p2align 3
.L87:
	movl	-184(%rbp), %r9d
	testl	%r9d, %r9d
	jne	.L89
	testb	$-128, -128(%rbp)
	je	.L90
	jmp	.L91
.L274:
	movq	-120(%rbp), %rax
	orq	-200(%rbp), %rax
	movq	%rax, -152(%rbp)
	jne	.L85
	movq	bio_err(%rip), %rdi
	leaq	.LC7(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	$0, -160(%rbp)
	jmp	.L59
.L285:
	movq	%r13, %rdi
	leaq	.LC10(%rip), %rdx
	movl	$32773, %esi
	movl	%r10d, -144(%rbp)
	call	load_cert@PLT
	movq	-120(%rbp), %rcx
	movl	-144(%rbp), %r10d
	movq	%rax, -192(%rbp)
	testq	%rcx, %rcx
	cmovne	%rcx, %r13
	testq	%rax, %rax
	movq	%r13, -120(%rbp)
	je	.L297
.L101:
	movq	-64(%rbp), %rcx
	movl	-100(%rbp), %esi
	leaq	.LC12(%rip), %r9
	xorl	%edx, %edx
	movq	-176(%rbp), %r8
	movq	-120(%rbp), %rdi
	movl	%r10d, -144(%rbp)
	call	load_key@PLT
	movq	%rax, -120(%rbp)
	testq	%rax, %rax
	je	.L166
	movb	%r12b, -296(%rbp)
	movl	-144(%rbp), %r10d
	movq	$0, -200(%rbp)
	jmp	.L102
.L273:
	movq	bio_err(%rip), %rdi
	leaq	.LC4(%rip), %rsi
	call	BIO_puts@PLT
	jmp	.L59
.L275:
	testl	%r12d, %r12d
	jne	.L85
	movq	bio_err(%rip), %rdi
	leaq	.LC8(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	$0, -160(%rbp)
	movq	$0, -152(%rbp)
	jmp	.L59
.L281:
	movq	bio_err(%rip), %rdi
	leaq	.LC5(%rip), %rsi
	call	BIO_puts@PLT
	jmp	.L59
.L277:
	movq	$0, -160(%rbp)
.L82:
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
	movl	%r10d, -128(%rbp)
	xorl	%r14d, %r14d
	leaq	.LC9(%rip), %rsi
	xorl	%r13d, %r13d
	xorl	%r12d, %r12d
	call	BIO_printf@PLT
	movq	-296(%rbp), %rax
	movl	-128(%rbp), %r10d
	movq	$0, -192(%rbp)
	movq	$0, -200(%rbp)
	movq	%rax, -152(%rbp)
	movq	$0, -144(%rbp)
	movq	$0, -120(%rbp)
	movq	$0, -184(%rbp)
	jmp	.L63
.L286:
	movq	$0, -192(%rbp)
	cmpq	$0, -120(%rbp)
	jne	.L101
	cmpq	$0, -168(%rbp)
	jne	.L298
	movq	-168(%rbp), %rax
	movq	$0, -200(%rbp)
	movq	%rax, -192(%rbp)
	movq	%rax, -120(%rbp)
	jmp	.L102
.L282:
	movl	%r10d, -192(%rbp)
	call	OPENSSL_sk_new_null@PLT
	movl	-192(%rbp), %r10d
	testq	%rax, %rax
	movq	%rax, -152(%rbp)
	jne	.L77
	jmp	.L157
.L279:
	movl	-120(%rbp), %r10d
.L94:
	cmpq	$0, -144(%rbp)
	je	.L299
	movq	-144(%rbp), %rdi
	xorl	%ecx, %ecx
	leaq	-88(%rbp), %rsi
	leaq	.LC11(%rip), %r8
	movl	$32773, %edx
	movl	%r10d, -300(%rbp)
	call	load_certs@PLT
	testl	%eax, %eax
	je	.L146
	movb	$0, -296(%rbp)
	movl	-300(%rbp), %r10d
	movq	$0, -192(%rbp)
	movq	$0, -120(%rbp)
	jmp	.L102
.L276:
	movq	$0, -160(%rbp)
	movq	$0, -152(%rbp)
	jmp	.L59
.L107:
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	leaq	.LC13(%rip), %rsi
	call	BIO_printf@PLT
	movl	$2, %r10d
	movq	$0, -144(%rbp)
	movq	$0, -184(%rbp)
	jmp	.L93
.L168:
	movq	$0, -144(%rbp)
	jmp	.L103
.L167:
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	movl	$2, %r10d
	movq	$0, -144(%rbp)
	movq	$0, -184(%rbp)
	jmp	.L93
.L158:
	movq	-152(%rbp), %rax
	movq	%rax, -296(%rbp)
	jmp	.L82
.L292:
	movl	-128(%rbp), %ecx
	movl	-208(%rbp), %esi
	movl	%r10d, -168(%rbp)
	movq	-80(%rbp), %rdx
	movq	-200(%rbp), %rdi
	movl	%ecx, %eax
	orb	$16, %ah
	testl	%esi, %esi
	movq	%r12, %rsi
	cmove	%ecx, %eax
	movl	%eax, %ecx
	movl	%eax, -128(%rbp)
	call	PKCS7_encrypt@PLT
	movl	-168(%rbp), %r10d
	movq	%rax, -144(%rbp)
.L113:
	cmpq	$0, -144(%rbp)
	je	.L141
	xorl	%r14d, %r14d
.L131:
	movq	-264(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L132
	movq	-224(%rbp), %rcx
	movq	-184(%rbp), %rdi
	leaq	.LC24(%rip), %rsi
	xorl	%eax, %eax
	movl	%r10d, -168(%rbp)
	call	BIO_printf@PLT
	movl	-168(%rbp), %r10d
.L132:
	movq	-256(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L133
	movq	-224(%rbp), %rcx
	movq	-184(%rbp), %rdi
	leaq	.LC25(%rip), %rsi
	xorl	%eax, %eax
	movl	%r10d, -168(%rbp)
	call	BIO_printf@PLT
	movl	-168(%rbp), %r10d
.L133:
	movq	-248(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L134
	movq	-224(%rbp), %rcx
	movq	-184(%rbp), %rdi
	leaq	.LC26(%rip), %rsi
	xorl	%eax, %eax
	movl	%r10d, -168(%rbp)
	call	BIO_printf@PLT
	movl	-168(%rbp), %r10d
.L134:
	movl	-104(%rbp), %eax
	movl	%r10d, -168(%rbp)
	cmpl	$32775, %eax
	je	.L300
	cmpl	$32773, %eax
	je	.L301
	cmpl	$4, %eax
	jne	.L139
	movl	-128(%rbp), %ecx
	movq	-144(%rbp), %rsi
	movq	%r12, %rdx
	movq	-184(%rbp), %rdi
	call	i2d_PKCS7_bio_stream@PLT
	movl	-168(%rbp), %r10d
.L137:
	xorl	%r13d, %r13d
	testl	%eax, %eax
	jne	.L63
	movq	bio_err(%rip), %rdi
	leaq	.LC28(%rip), %rsi
	call	BIO_printf@PLT
	movl	$3, %r10d
	jmp	.L93
.L284:
	movq	$0, -200(%rbp)
.L146:
	movq	bio_err(%rip), %rdi
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	xorl	%r12d, %r12d
	call	ERR_print_errors@PLT
	movq	$0, -120(%rbp)
	movl	$2, %r10d
	movq	$0, -192(%rbp)
	movq	$0, -144(%rbp)
	movq	$0, -184(%rbp)
	jmp	.L93
.L283:
	call	OPENSSL_sk_new_null@PLT
	movl	-192(%rbp), %r10d
	testq	%rax, %rax
	movq	%rax, -160(%rbp)
	jne	.L78
	jmp	.L157
.L287:
	leaq	-96(%rbp), %rsi
	movq	%r12, %rdi
	call	SMIME_read_PKCS7@PLT
	movl	-184(%rbp), %r10d
	movq	%rax, -144(%rbp)
	jmp	.L105
.L299:
	movl	-108(%rbp), %edx
	movq	-216(%rbp), %rdi
	movl	$114, %esi
	movl	%r10d, -184(%rbp)
	call	bio_open_default@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L182
	movq	$0, -120(%rbp)
	movl	-184(%rbp), %r10d
	movq	$0, -192(%rbp)
	movb	$0, -296(%rbp)
	jmp	.L103
.L291:
	movq	-280(%rbp), %rsi
	movq	-272(%rbp), %rdi
	movl	%r14d, %edx
	movl	%r15d, %ecx
	movl	%r10d, -208(%rbp)
	call	setup_verify@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L170
	movq	%rax, %rdi
	leaq	smime_cb(%rip), %rsi
	call	X509_STORE_set_verify_cb@PLT
	movl	-284(%rbp), %edi
	movl	-208(%rbp), %r10d
	testl	%edi, %edi
	je	.L114
	movq	-136(%rbp), %rsi
	movq	%r14, %rdi
	call	X509_STORE_set1_param@PLT
	movl	-208(%rbp), %r10d
	jmp	.L114
.L169:
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	movl	$2, %r10d
	jmp	.L93
.L288:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	PEM_read_bio_PKCS7@PLT
	movl	-184(%rbp), %r10d
	movq	%rax, -144(%rbp)
	jmp	.L105
.L160:
	movq	$0, -192(%rbp)
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
.L263:
	movq	$0, -144(%rbp)
	xorl	%r12d, %r12d
	movl	$2, %r10d
	movq	$0, -120(%rbp)
	movq	$0, -184(%rbp)
	jmp	.L93
.L278:
	movl	%r10d, -120(%rbp)
	call	EVP_des_ede3_cbc@PLT
	movl	-120(%rbp), %r10d
	movq	%rax, -80(%rbp)
	jmp	.L92
.L171:
	xorl	%r14d, %r14d
	jmp	.L114
.L175:
	movq	%r12, -120(%rbp)
	movq	%r14, %r13
	xorl	%r14d, %r14d
	movq	-208(%rbp), %r12
	movl	$3, %r10d
	jmp	.L93
.L296:
	movl	-128(%rbp), %r8d
	movq	-184(%rbp), %rcx
	xorl	%r13d, %r13d
	movl	%r10d, -168(%rbp)
	movq	-192(%rbp), %rdx
	movq	-120(%rbp), %rsi
	movq	-144(%rbp), %rdi
	call	PKCS7_decrypt@PLT
	movl	-168(%rbp), %r10d
	testl	%eax, %eax
	jne	.L63
	movq	bio_err(%rip), %rdi
	leaq	.LC19(%rip), %rsi
	call	BIO_printf@PLT
	movl	$4, %r10d
	jmp	.L93
.L300:
	movl	-128(%rbp), %ecx
	cmpl	$118, %ebx
	je	.L302
	movq	-144(%rbp), %rsi
	movq	-184(%rbp), %rdi
	movq	%r12, %rdx
	call	SMIME_write_PKCS7@PLT
	movl	-168(%rbp), %r10d
	jmp	.L137
.L166:
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	xorl	%r12d, %r12d
	movl	$2, %r10d
	movq	$0, -200(%rbp)
	movq	$0, -144(%rbp)
	movq	$0, -184(%rbp)
	jmp	.L93
.L268:
	movq	-192(%rbp), %rdx
	movq	bio_err(%rip), %rdi
	leaq	.LC3(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L59
.L293:
	testb	$64, -128(%rbp)
	je	.L116
	cmpl	$32775, -104(%rbp)
	je	.L260
.L117:
	movl	-128(%rbp), %r15d
	movq	-88(%rbp), %rdx
	movq	%r12, %rcx
	xorl	%esi, %esi
	xorl	%edi, %edi
	movl	%r10d, -208(%rbp)
	movl	%r15d, %r14d
	orl	$16384, %r14d
	movl	%r14d, %r8d
	call	PKCS7_sign@PLT
	movq	%rax, -144(%rbp)
	testq	%rax, %rax
	je	.L172
	andl	$2, %r15d
	movl	-208(%rbp), %r10d
	je	.L261
	xorl	%r15d, %r15d
	movl	%r10d, %r13d
	jmp	.L118
.L120:
	movq	-88(%rbp), %rdi
	movl	%r15d, %esi
	addl	$1, %r15d
	call	OPENSSL_sk_value@PLT
	movq	-144(%rbp), %rdi
	movq	%rax, %rsi
	call	PKCS7_add_certificate@PLT
.L118:
	movq	-88(%rbp), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r15d
	jl	.L120
	movl	%r13d, %r10d
.L261:
	movl	%r14d, -128(%rbp)
	jmp	.L119
.L125:
	cmpl	$53, %ebx
	jne	.L131
	movq	-144(%rbp), %rsi
	movq	-184(%rbp), %rdi
	movl	%r10d, -128(%rbp)
	xorl	%r13d, %r13d
	call	PEM_write_bio_PKCS7@PLT
	movl	-128(%rbp), %r10d
	jmp	.L63
.L139:
	leaq	.LC27(%rip), %rsi
.L262:
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
	xorl	%r13d, %r13d
	call	BIO_printf@PLT
	movl	$4, %r10d
	jmp	.L93
.L116:
	cmpl	$0, -208(%rbp)
	je	.L117
.L260:
	orl	$4096, -128(%rbp)
	jmp	.L117
.L297:
	movq	bio_err(%rip), %rdi
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	call	ERR_print_errors@PLT
	movq	$0, -200(%rbp)
	jmp	.L263
.L295:
	movq	%r14, -144(%rbp)
.L141:
	movq	bio_err(%rip), %rdi
	leaq	.LC18(%rip), %rsi
	xorl	%eax, %eax
	xorl	%r13d, %r13d
	call	BIO_printf@PLT
	movq	-144(%rbp), %r14
	movl	$3, %r10d
	movq	$0, -144(%rbp)
	jmp	.L93
.L99:
	cmpq	$0, -120(%rbp)
	je	.L142
	movq	$0, -192(%rbp)
	jmp	.L101
.L176:
	movq	%r12, -120(%rbp)
	movq	%r14, %r13
	movq	%rax, %r14
	movq	-208(%rbp), %r12
	movl	$3, %r10d
	jmp	.L93
.L289:
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	leaq	.LC14(%rip), %rsi
	call	BIO_printf@PLT
	movl	$2, %r10d
	movq	$0, -184(%rbp)
	jmp	.L93
.L170:
	xorl	%r13d, %r13d
	movl	$2, %r10d
	jmp	.L93
.L172:
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	movl	$3, %r10d
	jmp	.L93
.L301:
	movl	-128(%rbp), %ecx
	movq	-144(%rbp), %rsi
	movq	%r12, %rdx
	movq	-184(%rbp), %rdi
	call	PEM_write_bio_PKCS7_stream@PLT
	movl	-168(%rbp), %r10d
	jmp	.L137
.L142:
	movl	-108(%rbp), %edx
	movq	-216(%rbp), %rdi
	movl	$114, %esi
	movl	%r10d, -144(%rbp)
	call	bio_open_default@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L183
	movb	$1, -296(%rbp)
	movl	-144(%rbp), %r10d
	movq	$0, -192(%rbp)
	movq	$0, -200(%rbp)
	jmp	.L145
.L128:
	movq	-168(%rbp), %rdx
	movq	bio_err(%rip), %rdi
	leaq	.LC22(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movl	$5, %r10d
	jmp	.L93
.L265:
	call	__stack_chk_fail@PLT
.L182:
	movq	$0, -120(%rbp)
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	movl	$2, %r10d
	movq	$0, -192(%rbp)
	movq	$0, -184(%rbp)
	jmp	.L93
.L294:
	testl	$4096, -128(%rbp)
	jne	.L113
	movl	-128(%rbp), %edx
	movq	-144(%rbp), %rdi
	movq	%r12, %rsi
	movl	%r10d, -168(%rbp)
	call	PKCS7_final@PLT
	movl	-168(%rbp), %r10d
	testl	%eax, %eax
	jne	.L113
	movl	$3, %r10d
	xorl	%r13d, %r13d
	jmp	.L93
.L290:
	movq	-232(%rbp), %rdx
	movq	bio_err(%rip), %rdi
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	leaq	.LC16(%rip), %rsi
	call	BIO_printf@PLT
	movl	$2, %r10d
	jmp	.L93
.L302:
	movq	-96(%rbp), %rdx
	movq	-144(%rbp), %rsi
	movq	-184(%rbp), %rdi
	call	SMIME_write_PKCS7@PLT
	movl	-168(%rbp), %r10d
	jmp	.L137
.L126:
	leaq	.LC23(%rip), %rsi
	jmp	.L262
.L298:
	movq	-120(%rbp), %rax
	movq	%rax, -192(%rbp)
	movq	-168(%rbp), %rax
	movq	%rax, -120(%rbp)
	jmp	.L101
.L183:
	movq	$0, -192(%rbp)
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	movl	$2, %r10d
	movq	$0, -200(%rbp)
	movq	$0, -144(%rbp)
	movq	$0, -184(%rbp)
	jmp	.L93
	.cfi_endproc
.LFE1435:
	.size	smime_main, .-smime_main
	.globl	smime_options
	.section	.rodata.str1.8
	.align 8
.LC30:
	.string	"Usage: %s [options] cert.pem...\n"
	.align 8
.LC31:
	.string	"  cert.pem... recipient certs for encryption\n"
	.section	.rodata.str1.1
.LC32:
	.string	"Valid options are:\n"
.LC33:
	.string	"help"
.LC34:
	.string	"Display this summary"
.LC35:
	.string	"encrypt"
.LC36:
	.string	"Encrypt message"
.LC37:
	.string	"decrypt"
.LC38:
	.string	"Decrypt encrypted message"
.LC39:
	.string	"sign"
.LC40:
	.string	"Sign message"
.LC41:
	.string	"verify"
.LC42:
	.string	"Verify signed message"
.LC43:
	.string	"pk7out"
.LC44:
	.string	"Output PKCS#7 structure"
.LC45:
	.string	"nointern"
	.section	.rodata.str1.8
	.align 8
.LC46:
	.string	"Don't search certificates in message for signer"
	.section	.rodata.str1.1
.LC47:
	.string	"nosigs"
	.section	.rodata.str1.8
	.align 8
.LC48:
	.string	"Don't verify message signature"
	.section	.rodata.str1.1
.LC49:
	.string	"noverify"
	.section	.rodata.str1.8
	.align 8
.LC50:
	.string	"Don't verify signers certificate"
	.section	.rodata.str1.1
.LC51:
	.string	"nocerts"
	.section	.rodata.str1.8
	.align 8
.LC52:
	.string	"Don't include signers certificate when signing"
	.section	.rodata.str1.1
.LC53:
	.string	"nodetach"
.LC54:
	.string	"Use opaque signing"
.LC55:
	.string	"noattr"
	.section	.rodata.str1.8
	.align 8
.LC56:
	.string	"Don't include any signed attributes"
	.section	.rodata.str1.1
.LC57:
	.string	"binary"
	.section	.rodata.str1.8
	.align 8
.LC58:
	.string	"Don't translate message to text"
	.section	.rodata.str1.1
.LC59:
	.string	"certfile"
.LC60:
	.string	"Other certificates file"
.LC61:
	.string	"signer"
.LC62:
	.string	"Signer certificate file"
.LC63:
	.string	"recip"
	.section	.rodata.str1.8
	.align 8
.LC64:
	.string	"Recipient certificate file for decryption"
	.section	.rodata.str1.1
.LC65:
	.string	"in"
.LC66:
	.string	"Input file"
.LC67:
	.string	"inform"
	.section	.rodata.str1.8
	.align 8
.LC68:
	.string	"Input format SMIME (default), PEM or DER"
	.section	.rodata.str1.1
.LC69:
	.string	"inkey"
	.section	.rodata.str1.8
	.align 8
.LC70:
	.string	"Input private key (if not signer or recipient)"
	.section	.rodata.str1.1
.LC71:
	.string	"keyform"
	.section	.rodata.str1.8
	.align 8
.LC72:
	.string	"Input private key format (PEM or ENGINE)"
	.section	.rodata.str1.1
.LC73:
	.string	"out"
.LC74:
	.string	"Output file"
.LC75:
	.string	"outform"
	.section	.rodata.str1.8
	.align 8
.LC76:
	.string	"Output format SMIME (default), PEM or DER"
	.section	.rodata.str1.1
.LC77:
	.string	"content"
	.section	.rodata.str1.8
	.align 8
.LC78:
	.string	"Supply or override content for detached signature"
	.section	.rodata.str1.1
.LC79:
	.string	"to"
.LC80:
	.string	"To address"
.LC81:
	.string	"from"
.LC82:
	.string	"From address"
.LC83:
	.string	"subject"
.LC84:
	.string	"Subject"
.LC85:
	.string	"text"
	.section	.rodata.str1.8
	.align 8
.LC86:
	.string	"Include or delete text MIME headers"
	.section	.rodata.str1.1
.LC87:
	.string	"CApath"
	.section	.rodata.str1.8
	.align 8
.LC88:
	.string	"Trusted certificates directory"
	.section	.rodata.str1.1
.LC89:
	.string	"CAfile"
.LC90:
	.string	"Trusted certificates file"
.LC91:
	.string	"no-CAfile"
	.section	.rodata.str1.8
	.align 8
.LC92:
	.string	"Do not load the default certificates file"
	.section	.rodata.str1.1
.LC93:
	.string	"no-CApath"
	.section	.rodata.str1.8
	.align 8
.LC94:
	.string	"Do not load certificates from the default certificates directory"
	.section	.rodata.str1.1
.LC95:
	.string	"resign"
.LC96:
	.string	"Resign a signed message"
.LC97:
	.string	"nochain"
	.section	.rodata.str1.8
	.align 8
.LC98:
	.string	"set PKCS7_NOCHAIN so certificates contained in the message are not used as untrusted CAs"
	.section	.rodata.str1.1
.LC99:
	.string	"nosmimecap"
	.section	.rodata.str1.8
	.align 8
.LC100:
	.string	"Omit the SMIMECapabilities attribute"
	.section	.rodata.str1.1
.LC101:
	.string	"stream"
.LC102:
	.string	"Enable CMS streaming"
.LC103:
	.string	"indef"
.LC104:
	.string	"Same as -stream"
.LC105:
	.string	"noindef"
.LC106:
	.string	"Disable CMS streaming"
.LC107:
	.string	"crlfeol"
	.section	.rodata.str1.8
	.align 8
.LC108:
	.string	"Use CRLF as EOL termination instead of CR only"
	.section	.rodata.str1.1
.LC109:
	.string	"rand"
	.section	.rodata.str1.8
	.align 8
.LC110:
	.string	"Load the file(s) into the random number generator"
	.section	.rodata.str1.1
.LC111:
	.string	"writerand"
	.section	.rodata.str1.8
	.align 8
.LC112:
	.string	"Write random data to the specified file"
	.section	.rodata.str1.1
.LC113:
	.string	"passin"
.LC114:
	.string	"Input file pass phrase source"
.LC115:
	.string	"md"
	.section	.rodata.str1.8
	.align 8
.LC116:
	.string	"Digest algorithm to use when signing or resigning"
	.section	.rodata.str1.1
.LC117:
	.string	""
.LC118:
	.string	"Any supported cipher"
.LC119:
	.string	"policy"
	.section	.rodata.str1.8
	.align 8
.LC120:
	.string	"adds policy to the acceptable policy set"
	.section	.rodata.str1.1
.LC121:
	.string	"purpose"
.LC122:
	.string	"certificate chain purpose"
.LC123:
	.string	"verify_name"
.LC124:
	.string	"verification policy name"
.LC125:
	.string	"verify_depth"
.LC126:
	.string	"chain depth limit"
.LC127:
	.string	"auth_level"
	.section	.rodata.str1.8
	.align 8
.LC128:
	.string	"chain authentication security level"
	.section	.rodata.str1.1
.LC129:
	.string	"attime"
.LC130:
	.string	"verification epoch time"
.LC131:
	.string	"verify_hostname"
.LC132:
	.string	"expected peer hostname"
.LC133:
	.string	"verify_email"
.LC134:
	.string	"expected peer email"
.LC135:
	.string	"verify_ip"
.LC136:
	.string	"expected peer IP address"
.LC137:
	.string	"ignore_critical"
	.section	.rodata.str1.8
	.align 8
.LC138:
	.string	"permit unhandled critical extensions"
	.section	.rodata.str1.1
.LC139:
	.string	"issuer_checks"
.LC140:
	.string	"(deprecated)"
.LC141:
	.string	"crl_check"
	.section	.rodata.str1.8
	.align 8
.LC142:
	.string	"check leaf certificate revocation"
	.section	.rodata.str1.1
.LC143:
	.string	"crl_check_all"
.LC144:
	.string	"check full chain revocation"
.LC145:
	.string	"policy_check"
.LC146:
	.string	"perform rfc5280 policy checks"
.LC147:
	.string	"explicit_policy"
	.section	.rodata.str1.8
	.align 8
.LC148:
	.string	"set policy variable require-explicit-policy"
	.section	.rodata.str1.1
.LC149:
	.string	"inhibit_any"
	.section	.rodata.str1.8
	.align 8
.LC150:
	.string	"set policy variable inhibit-any-policy"
	.section	.rodata.str1.1
.LC151:
	.string	"inhibit_map"
	.section	.rodata.str1.8
	.align 8
.LC152:
	.string	"set policy variable inhibit-policy-mapping"
	.section	.rodata.str1.1
.LC153:
	.string	"x509_strict"
	.section	.rodata.str1.8
	.align 8
.LC154:
	.string	"disable certificate compatibility work-arounds"
	.section	.rodata.str1.1
.LC155:
	.string	"extended_crl"
.LC156:
	.string	"enable extended CRL features"
.LC157:
	.string	"use_deltas"
.LC158:
	.string	"use delta CRLs"
.LC159:
	.string	"policy_print"
	.section	.rodata.str1.8
	.align 8
.LC160:
	.string	"print policy processing diagnostics"
	.section	.rodata.str1.1
.LC161:
	.string	"check_ss_sig"
.LC162:
	.string	"check root CA self-signatures"
.LC163:
	.string	"trusted_first"
	.section	.rodata.str1.8
	.align 8
.LC164:
	.string	"search trust store first (default)"
	.section	.rodata.str1.1
.LC165:
	.string	"suiteB_128_only"
.LC166:
	.string	"Suite B 128-bit-only mode"
.LC167:
	.string	"suiteB_128"
	.section	.rodata.str1.8
	.align 8
.LC168:
	.string	"Suite B 128-bit mode allowing 192-bit algorithms"
	.section	.rodata.str1.1
.LC169:
	.string	"suiteB_192"
.LC170:
	.string	"Suite B 192-bit-only mode"
.LC171:
	.string	"partial_chain"
	.section	.rodata.str1.8
	.align 8
.LC172:
	.string	"accept chains anchored by intermediate trust-store CAs"
	.section	.rodata.str1.1
.LC173:
	.string	"no_alt_chains"
.LC174:
	.string	"no_check_time"
	.section	.rodata.str1.8
	.align 8
.LC175:
	.string	"ignore certificate validity time"
	.section	.rodata.str1.1
.LC176:
	.string	"allow_proxy_certs"
	.section	.rodata.str1.8
	.align 8
.LC177:
	.string	"allow the use of proxy certificates"
	.section	.rodata.str1.1
.LC178:
	.string	"engine"
	.section	.rodata.str1.8
	.align 8
.LC179:
	.string	"Use engine, possibly a hardware device"
	.section	.data.rel.ro,"aw"
	.align 32
	.type	smime_options, @object
	.size	smime_options, 1872
smime_options:
	.quad	OPT_HELP_STR
	.long	1
	.long	45
	.quad	.LC30
	.quad	OPT_HELP_STR
	.long	1
	.long	45
	.quad	.LC31
	.quad	OPT_HELP_STR
	.long	1
	.long	45
	.quad	.LC32
	.quad	.LC33
	.long	1
	.long	45
	.quad	.LC34
	.quad	.LC35
	.long	2
	.long	45
	.quad	.LC36
	.quad	.LC37
	.long	3
	.long	45
	.quad	.LC38
	.quad	.LC39
	.long	4
	.long	45
	.quad	.LC40
	.quad	.LC41
	.long	6
	.long	45
	.quad	.LC42
	.quad	.LC43
	.long	7
	.long	45
	.quad	.LC44
	.quad	.LC45
	.long	9
	.long	45
	.quad	.LC46
	.quad	.LC47
	.long	17
	.long	45
	.quad	.LC48
	.quad	.LC49
	.long	10
	.long	45
	.quad	.LC50
	.quad	.LC51
	.long	12
	.long	45
	.quad	.LC52
	.quad	.LC53
	.long	14
	.long	45
	.quad	.LC54
	.quad	.LC55
	.long	13
	.long	45
	.quad	.LC56
	.quad	.LC57
	.long	16
	.long	45
	.quad	.LC58
	.quad	.LC59
	.long	33
	.long	60
	.quad	.LC60
	.quad	.LC61
	.long	27
	.long	115
	.quad	.LC62
	.quad	.LC63
	.long	28
	.long	60
	.quad	.LC64
	.quad	.LC65
	.long	2035
	.long	60
	.quad	.LC66
	.quad	.LC67
	.long	2036
	.long	99
	.quad	.LC68
	.quad	.LC69
	.long	31
	.long	115
	.quad	.LC70
	.quad	.LC71
	.long	32
	.long	102
	.quad	.LC72
	.quad	.LC73
	.long	2037
	.long	62
	.quad	.LC74
	.quad	.LC75
	.long	2038
	.long	99
	.quad	.LC76
	.quad	.LC77
	.long	2039
	.long	60
	.quad	.LC78
	.quad	.LC79
	.long	24
	.long	115
	.quad	.LC80
	.quad	.LC81
	.long	25
	.long	115
	.quad	.LC82
	.quad	.LC83
	.long	26
	.long	115
	.quad	.LC84
	.quad	.LC85
	.long	8
	.long	45
	.quad	.LC86
	.quad	.LC87
	.long	2032
	.long	47
	.quad	.LC88
	.quad	.LC89
	.long	34
	.long	60
	.quad	.LC90
	.quad	.LC91
	.long	2033
	.long	45
	.quad	.LC92
	.quad	.LC93
	.long	2034
	.long	45
	.quad	.LC94
	.quad	.LC95
	.long	5
	.long	45
	.quad	.LC96
	.quad	.LC97
	.long	11
	.long	45
	.quad	.LC98
	.quad	.LC99
	.long	15
	.long	45
	.quad	.LC100
	.quad	.LC101
	.long	18
	.long	45
	.quad	.LC102
	.quad	.LC103
	.long	19
	.long	45
	.quad	.LC104
	.quad	.LC105
	.long	20
	.long	45
	.quad	.LC106
	.quad	.LC107
	.long	21
	.long	45
	.quad	.LC108
	.quad	.LC109
	.long	1501
	.long	115
	.quad	.LC110
	.quad	.LC111
	.long	1502
	.long	62
	.quad	.LC112
	.quad	.LC113
	.long	23
	.long	115
	.quad	.LC114
	.quad	.LC115
	.long	29
	.long	115
	.quad	.LC116
	.quad	.LC117
	.long	30
	.long	45
	.quad	.LC118
	.quad	.LC119
	.long	2001
	.long	115
	.quad	.LC120
	.quad	.LC121
	.long	2002
	.long	115
	.quad	.LC122
	.quad	.LC123
	.long	2003
	.long	115
	.quad	.LC124
	.quad	.LC125
	.long	2004
	.long	110
	.quad	.LC126
	.quad	.LC127
	.long	2029
	.long	110
	.quad	.LC128
	.quad	.LC129
	.long	2005
	.long	77
	.quad	.LC130
	.quad	.LC131
	.long	2006
	.long	115
	.quad	.LC132
	.quad	.LC133
	.long	2007
	.long	115
	.quad	.LC134
	.quad	.LC135
	.long	2008
	.long	115
	.quad	.LC136
	.quad	.LC137
	.long	2009
	.long	45
	.quad	.LC138
	.quad	.LC139
	.long	2010
	.long	45
	.quad	.LC140
	.quad	.LC141
	.long	2011
	.long	45
	.quad	.LC142
	.quad	.LC143
	.long	2012
	.long	45
	.quad	.LC144
	.quad	.LC145
	.long	2013
	.long	45
	.quad	.LC146
	.quad	.LC147
	.long	2014
	.long	45
	.quad	.LC148
	.quad	.LC149
	.long	2015
	.long	45
	.quad	.LC150
	.quad	.LC151
	.long	2016
	.long	45
	.quad	.LC152
	.quad	.LC153
	.long	2017
	.long	45
	.quad	.LC154
	.quad	.LC155
	.long	2018
	.long	45
	.quad	.LC156
	.quad	.LC157
	.long	2019
	.long	45
	.quad	.LC158
	.quad	.LC159
	.long	2020
	.long	45
	.quad	.LC160
	.quad	.LC161
	.long	2021
	.long	45
	.quad	.LC162
	.quad	.LC163
	.long	2022
	.long	45
	.quad	.LC164
	.quad	.LC165
	.long	2023
	.long	45
	.quad	.LC166
	.quad	.LC167
	.long	2024
	.long	45
	.quad	.LC168
	.quad	.LC169
	.long	2025
	.long	45
	.quad	.LC170
	.quad	.LC171
	.long	2026
	.long	45
	.quad	.LC172
	.quad	.LC173
	.long	2027
	.long	45
	.quad	.LC140
	.quad	.LC174
	.long	2028
	.long	45
	.quad	.LC175
	.quad	.LC176
	.long	2030
	.long	45
	.quad	.LC177
	.quad	.LC178
	.long	22
	.long	115
	.quad	.LC179
	.quad	0
	.zero	16
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
