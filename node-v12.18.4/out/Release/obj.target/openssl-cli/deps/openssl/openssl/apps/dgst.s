	.file	"dgst.c"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"rsa"
.LC1:
	.string	"RSA"
.LC2:
	.string	"-%-25s"
.LC3:
	.string	"\n"
.LC4:
	.string	" "
	.text
	.p2align 4
	.type	show_digests, @function
show_digests:
.LFB1438:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	leaq	.LC0(%rip), %rsi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %r12
	movq	%r12, %rdi
	call	strstr@PLT
	testq	%rax, %rax
	je	.L14
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	.cfi_restore_state
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	strstr@PLT
	testq	%rax, %rax
	jne	.L1
	call	__ctype_b_loc@PLT
	movzbl	(%r12), %edx
	movq	(%rax), %rax
	testb	$2, 1(%rax,%rdx,2)
	je	.L1
	movq	%r12, %rdi
	call	EVP_get_digestbyname@PLT
	testq	%rax, %rax
	je	.L1
	movq	8(%rbx), %rdx
	movq	0(%r13), %rdi
	leaq	.LC2(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movl	8(%r13), %eax
	addl	$1, %eax
	movl	%eax, 8(%r13)
	cmpl	$3, %eax
	je	.L15
	movq	0(%r13), %rdi
	addq	$8, %rsp
	leaq	.LC4(%rip), %rsi
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	BIO_printf@PLT
	.p2align 4,,10
	.p2align 3
.L15:
	.cfi_restore_state
	movq	0(%r13), %rdi
	leaq	.LC3(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movl	$0, 8(%r13)
	jmp	.L1
	.cfi_endproc
.LFE1438:
	.size	show_digests, .-show_digests
	.section	.rodata.str1.1
.LC5:
	.string	"Read Error in %s\n"
.LC6:
	.string	"Verified OK\n"
.LC7:
	.string	"Verification Failure\n"
.LC8:
	.string	"Error Verifying Data\n"
.LC9:
	.string	"Signature buffer"
.LC10:
	.string	"Error Signing Data\n"
.LC13:
	.string	"\\"
.LC14:
	.string	" *%s\n"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC15:
	.string	"../deps/openssl/openssl/apps/dgst.c"
	.section	.rodata.str1.1
.LC16:
	.string	"%02x"
.LC17:
	.string	"-%s"
.LC18:
	.string	"(%s)= "
.LC19:
	.string	"%s(%s)= "
.LC20:
	.string	":"
	.text
	.p2align 4
	.globl	do_fp
	.type	do_fp, @function
do_fp:
.LFB1440:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	32(%rbp), %rax
	movq	16(%rbp), %rbx
	movl	%ecx, -92(%rbp)
	movl	%r8d, -96(%rbp)
	movq	48(%rbp), %r15
	movq	%rax, -104(%rbp)
	movq	40(%rbp), %rax
	movq	%r9, -88(%rbp)
	movq	%rax, -112(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$8192, -72(%rbp)
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L18:
	je	.L23
.L17:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$10, %esi
	movq	%r14, %rdi
	call	BIO_ctrl@PLT
	testl	%eax, %eax
	jne	.L22
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$2, %esi
	movq	%r14, %rdi
	call	BIO_ctrl@PLT
	testl	%eax, %eax
	jne	.L23
.L22:
	movl	$8192, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	BIO_read@PLT
	testl	%eax, %eax
	jns	.L18
	movq	bio_err(%rip), %rdi
	movq	%r15, %rdx
	leaq	.LC5(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	movl	$1, %eax
.L16:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L90
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	.cfi_restore_state
	testq	%rbx, %rbx
	je	.L91
	xorl	%edx, %edx
	movl	$120, %esi
	leaq	-64(%rbp), %rcx
	movq	%r14, %rdi
	call	BIO_ctrl@PLT
	movl	24(%rbp), %edx
	movq	-64(%rbp), %rdi
	movq	%rbx, %rsi
	call	EVP_DigestVerifyFinal@PLT
	testl	%eax, %eax
	jg	.L92
	jne	.L26
	leaq	.LC7(%rip), %rsi
	movq	%r13, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movl	$1, %eax
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L91:
	cmpq	$0, -88(%rbp)
	je	.L27
	leaq	-64(%rbp), %rcx
	xorl	%edx, %edx
	movl	$120, %esi
	movq	%r14, %rdi
	call	BIO_ctrl@PLT
	movq	-88(%rbp), %rdi
	call	EVP_PKEY_size@PLT
	movl	%eax, %edi
	cmpl	$8192, %eax
	jg	.L93
.L28:
	movq	-64(%rbp), %rdi
	leaq	-72(%rbp), %rdx
	movq	%r12, %rsi
	call	EVP_DigestSignFinal@PLT
	testl	%eax, %eax
	je	.L94
.L31:
	movl	-96(%rbp), %esi
	testl	%esi, %esi
	jne	.L95
	cmpl	$2, -92(%rbp)
	je	.L96
	movq	-104(%rbp), %rsi
	testq	%rsi, %rsi
	je	.L60
	movq	%r13, %rdi
	call	BIO_puts@PLT
	movq	-112(%rbp), %rax
	testq	%rax, %rax
	je	.L61
	movq	%rax, %rdx
	leaq	.LC17(%rip), %rsi
	movq	%r13, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L61:
	movq	%r15, %rdx
	leaq	.LC18(%rip), %rsi
	movq	%r13, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L62:
	movl	-72(%rbp), %edx
	testl	%edx, %edx
	jle	.L63
	movl	-92(%rbp), %eax
	xorl	%r14d, %r14d
	leaq	.LC16(%rip), %r15
	testl	%eax, %eax
	jne	.L64
	.p2align 4,,10
	.p2align 3
.L65:
	movzbl	(%r12,%r14), %edx
	movq	%r15, %rsi
	movq	%r13, %rdi
	xorl	%eax, %eax
	addq	$1, %r14
	call	BIO_printf@PLT
	cmpl	%r14d, -72(%rbp)
	jg	.L65
.L63:
	leaq	.LC3(%rip), %rsi
	movq	%r13, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	xorl	%eax, %eax
.L30:
	testq	%rbx, %rbx
	je	.L16
	movq	-72(%rbp), %rsi
	movl	$593, %ecx
	movq	%rbx, %rdi
	movl	%eax, -88(%rbp)
	leaq	.LC15(%rip), %rdx
	call	CRYPTO_clear_free@PLT
	movl	-88(%rbp), %eax
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L26:
	movq	bio_err(%rip), %rdi
	leaq	.LC8(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	movl	$1, %eax
	jmp	.L16
.L27:
	movl	$8192, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	BIO_gets@PLT
	movslq	%eax, %rdx
	movq	%rdx, -72(%rbp)
	testl	%eax, %eax
	jns	.L31
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	movl	$1, %eax
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L67:
	leaq	.LC20(%rip), %rsi
	movq	%r13, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L64:
	movzbl	(%r12,%r14), %edx
	movq	%r15, %rsi
	movq	%r13, %rdi
	xorl	%eax, %eax
	addq	$1, %r14
	call	BIO_printf@PLT
	cmpl	%r14d, -72(%rbp)
	jg	.L67
	jmp	.L63
.L92:
	leaq	.LC6(%rip), %rsi
	movq	%r13, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	xorl	%eax, %eax
	jmp	.L16
.L95:
	movl	-72(%rbp), %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	BIO_write@PLT
	xorl	%eax, %eax
	jmp	.L30
.L93:
	cltq
	leaq	.LC9(%rip), %rsi
	movq	%rax, -72(%rbp)
	call	app_malloc@PLT
	movq	%rax, %r12
	movq	%rax, %rbx
	jmp	.L28
.L60:
	movq	-112(%rbp), %rax
	testq	%rax, %rax
	je	.L61
	movq	%rax, %rdx
	movq	%r15, %rcx
	leaq	.LC19(%rip), %rsi
	movq	%r13, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L62
.L94:
	movq	bio_err(%rip), %rdi
	leaq	.LC10(%rip), %rsi
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	movl	$1, %eax
	jmp	.L30
.L96:
	movq	%r15, %rdi
	call	strlen@PLT
	movq	%rax, %r14
	leaq	1(%rax), %rdi
	testq	%rax, %rax
	je	.L34
	leaq	-1(%rax), %rax
	cmpq	$14, %rax
	jbe	.L70
	movq	%r14, %rdx
	pxor	%xmm4, %xmm4
	pxor	%xmm6, %xmm6
	movq	%r15, %rax
	andq	$-16, %rdx
	movdqa	.LC12(%rip), %xmm7
	pxor	%xmm5, %xmm5
	pxor	%xmm3, %xmm3
	movdqa	.LC11(%rip), %xmm8
	addq	%r15, %rdx
	.p2align 4,,10
	.p2align 3
.L36:
	movdqu	(%rax), %xmm0
	addq	$16, %rax
	pcmpeqb	%xmm8, %xmm0
	pand	%xmm7, %xmm0
	movdqa	%xmm0, %xmm2
	punpckhbw	%xmm6, %xmm0
	punpcklbw	%xmm6, %xmm2
	movdqa	%xmm0, %xmm9
	punpckhwd	%xmm5, %xmm0
	movdqa	%xmm2, %xmm1
	punpckhwd	%xmm5, %xmm2
	punpcklwd	%xmm5, %xmm9
	punpcklwd	%xmm5, %xmm1
	movdqa	%xmm1, %xmm10
	punpckldq	%xmm3, %xmm1
	punpckhdq	%xmm3, %xmm10
	paddq	%xmm10, %xmm1
	movdqa	%xmm2, %xmm10
	punpckhdq	%xmm3, %xmm2
	punpckldq	%xmm3, %xmm10
	paddq	%xmm10, %xmm2
	paddq	%xmm2, %xmm1
	movdqa	%xmm9, %xmm2
	punpckhdq	%xmm3, %xmm9
	punpckldq	%xmm3, %xmm2
	paddq	%xmm2, %xmm9
	movdqa	%xmm0, %xmm2
	punpckhdq	%xmm3, %xmm0
	punpckldq	%xmm3, %xmm2
	paddq	%xmm0, %xmm4
	paddq	%xmm2, %xmm9
	paddq	%xmm9, %xmm1
	paddq	%xmm1, %xmm4
	cmpq	%rdx, %rax
	jne	.L36
	movdqa	%xmm4, %xmm0
	movq	%r14, %rdx
	psrldq	$8, %xmm0
	andq	$-16, %rdx
	paddq	%xmm0, %xmm4
	movq	%xmm4, %rax
	testb	$15, %r14b
	je	.L37
.L35:
	xorl	%ecx, %ecx
	cmpb	$10, (%r15,%rdx)
	sete	%cl
	addq	%rcx, %rax
	leaq	1(%rdx), %rcx
	cmpq	%rcx, %r14
	jbe	.L37
	cmpb	$10, 1(%r15,%rdx)
	jne	.L39
	addq	$1, %rax
.L39:
	leaq	2(%rdx), %rcx
	cmpq	%rcx, %r14
	jbe	.L37
	cmpb	$10, 2(%r15,%rdx)
	jne	.L40
	addq	$1, %rax
.L40:
	leaq	3(%rdx), %rcx
	cmpq	%rcx, %r14
	jbe	.L37
	cmpb	$10, 3(%r15,%rdx)
	jne	.L41
	addq	$1, %rax
.L41:
	leaq	4(%rdx), %rcx
	cmpq	%rcx, %r14
	jbe	.L37
	cmpb	$10, 4(%r15,%rdx)
	jne	.L42
	addq	$1, %rax
.L42:
	leaq	5(%rdx), %rcx
	cmpq	%rcx, %r14
	jbe	.L37
	cmpb	$10, 5(%r15,%rdx)
	jne	.L43
	addq	$1, %rax
.L43:
	leaq	6(%rdx), %rcx
	cmpq	%rcx, %r14
	jbe	.L37
	cmpb	$10, 6(%r15,%rdx)
	jne	.L44
	addq	$1, %rax
.L44:
	leaq	7(%rdx), %rcx
	cmpq	%rcx, %r14
	jbe	.L37
	cmpb	$10, 7(%r15,%rdx)
	jne	.L45
	addq	$1, %rax
.L45:
	leaq	8(%rdx), %rcx
	cmpq	%rcx, %r14
	jbe	.L37
	cmpb	$10, 8(%r15,%rdx)
	jne	.L46
	addq	$1, %rax
.L46:
	leaq	9(%rdx), %rcx
	cmpq	%rcx, %r14
	jbe	.L37
	cmpb	$10, 9(%r15,%rdx)
	jne	.L47
	addq	$1, %rax
.L47:
	leaq	10(%rdx), %rcx
	cmpq	%rcx, %r14
	jbe	.L37
	cmpb	$10, 10(%r15,%rdx)
	jne	.L48
	addq	$1, %rax
.L48:
	leaq	11(%rdx), %rcx
	cmpq	%rcx, %r14
	jbe	.L37
	cmpb	$10, 11(%r15,%rdx)
	jne	.L49
	addq	$1, %rax
.L49:
	leaq	12(%rdx), %rcx
	cmpq	%rcx, %r14
	jbe	.L37
	cmpb	$10, 12(%r15,%rdx)
	jne	.L50
	addq	$1, %rax
.L50:
	leaq	13(%rdx), %rcx
	cmpq	%rcx, %r14
	jbe	.L37
	cmpb	$10, 13(%r15,%rdx)
	jne	.L51
	addq	$1, %rax
.L51:
	leaq	14(%rdx), %rcx
	cmpq	%rcx, %r14
	jbe	.L37
	cmpb	$10, 14(%r15,%rdx)
	jne	.L37
	addq	$1, %rax
.L37:
	movq	%r15, %rsi
	addq	%rax, %rdi
	call	app_malloc@PLT
	leaq	(%r15,%r14), %rsi
	xorl	%r10d, %r10d
	movq	%rax, %r8
	xorl	%eax, %eax
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L54:
	movb	%cl, (%rdi)
	movq	%rdx, %rax
.L55:
	addq	$1, %r15
	cmpq	%rsi, %r15
	je	.L97
.L56:
	movzbl	(%r15), %ecx
	leaq	1(%rax), %rdx
	leaq	(%r8,%rax), %rdi
	leaq	(%r8,%rdx), %r9
	cmpb	$10, %cl
	jne	.L54
	addq	$2, %rax
	movb	$92, (%rdi)
	movl	$1, %r10d
	movb	$110, (%r9)
	leaq	(%r8,%rax), %r9
	jmp	.L55
.L97:
	movb	$0, (%r9)
	cmpl	$1, %r10d
	je	.L98
.L57:
	movl	-72(%rbp), %ecx
	xorl	%r14d, %r14d
	leaq	.LC16(%rip), %r15
	testl	%ecx, %ecx
	jle	.L59
	.p2align 4,,10
	.p2align 3
.L58:
	movzbl	(%r12,%r14), %edx
	xorl	%eax, %eax
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%r8, -88(%rbp)
	addq	$1, %r14
	call	BIO_printf@PLT
	cmpl	%r14d, -72(%rbp)
	movq	-88(%rbp), %r8
	jg	.L58
.L59:
	movq	%r8, %rdx
	leaq	.LC14(%rip), %rsi
	movq	%r13, %rdi
	xorl	%eax, %eax
	movq	%r8, -88(%rbp)
	call	BIO_printf@PLT
	movq	-88(%rbp), %r8
	movl	$570, %edx
	leaq	.LC15(%rip), %rsi
	movq	%r8, %rdi
	call	CRYPTO_free@PLT
	xorl	%eax, %eax
	jmp	.L30
.L98:
	leaq	.LC13(%rip), %rsi
	movq	%r13, %rdi
	movq	%r8, -88(%rbp)
	call	BIO_puts@PLT
	movq	-88(%rbp), %r8
	jmp	.L57
.L34:
	movq	%r15, %rsi
	movl	$1, %edi
	call	app_malloc@PLT
	movb	$0, (%rax)
	movq	%rax, %r8
	jmp	.L57
.L70:
	xorl	%eax, %eax
	xorl	%edx, %edx
	jmp	.L35
.L90:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1440:
	.size	do_fp, .-do_fp
	.section	.rodata.str1.1
.LC21:
	.string	"etaonrishdlcupfm"
.LC22:
	.string	"I/O buffer"
.LC23:
	.string	"%s: Use -help for summary.\n"
.LC24:
	.string	"Supported digests:\n"
	.section	.rodata.str1.8
	.align 8
.LC25:
	.string	"%s: Can only sign or verify one file.\n"
	.align 8
.LC26:
	.string	"No signature to verify: use the -signature option\n"
	.section	.rodata.str1.1
.LC27:
	.string	"Error getting password\n"
	.section	.rodata.str1.8
	.align 8
.LC28:
	.string	"MAC and Signing key cannot both be specified\n"
	.section	.rodata.str1.1
.LC29:
	.string	"key file"
	.section	.rodata.str1.8
	.align 8
.LC30:
	.string	"Key type not supported for this operation\n"
	.section	.rodata.str1.1
.LC31:
	.string	"MAC parameter error \"%s\"\n"
.LC32:
	.string	"Error generating key\n"
.LC33:
	.string	"Error getting context\n"
.LC34:
	.string	"Error setting context\n"
.LC35:
	.string	"parameter error \"%s\"\n"
.LC36:
	.string	"Error setting digest\n"
.LC37:
	.string	"rb"
	.section	.rodata.str1.8
	.align 8
.LC38:
	.string	"Error opening signature file %s\n"
	.section	.rodata.str1.1
.LC39:
	.string	"signature buffer"
	.section	.rodata.str1.8
	.align 8
.LC40:
	.string	"Error reading signature file %s\n"
	.section	.rodata.str1.1
.LC41:
	.string	"stdin"
	.text
	.p2align 4
	.globl	dgst_main
	.type	dgst_main, @function
dgst_main:
.LFB1437:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-96(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	.L105(%rip), %rbx
	subq	$232, %rsp
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -112(%rbp)
	movq	$0, -104(%rbp)
	movl	$32773, -116(%rbp)
	call	opt_progname@PLT
	leaq	.LC22(%rip), %rsi
	movl	$8192, %edi
	movq	%rax, %r14
	call	app_malloc@PLT
	movq	%r14, %rdi
	movq	%rax, -160(%rbp)
	call	EVP_get_digestbyname@PLT
	movq	%r12, %rsi
	movl	%r13d, %edi
	leaq	dgst_options(%rip), %rdx
	movq	%rax, -240(%rbp)
	xorl	%r12d, %r12d
	xorl	%r13d, %r13d
	call	opt_init@PLT
	movl	$0, -200(%rbp)
	movl	$0, -176(%rbp)
	movq	%rax, %r14
	movl	$0, -168(%rbp)
	movl	$-1, -164(%rbp)
	movl	$0, -208(%rbp)
	movq	$0, -192(%rbp)
	movq	$0, -232(%rbp)
	movq	$0, -216(%rbp)
	movq	$0, -224(%rbp)
	movq	$0, -184(%rbp)
	movq	$0, -136(%rbp)
	movq	$0, -144(%rbp)
	movq	$0, -152(%rbp)
.L100:
	call	opt_next@PLT
	testl	%eax, %eax
	je	.L267
.L131:
	cmpl	$22, %eax
	jg	.L101
	cmpl	$-1, %eax
	jl	.L100
	addl	$1, %eax
	cmpl	$23, %eax
	ja	.L100
	movslq	(%rbx,%rax,4), %rax
	addq	%rbx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L105:
	.long	.L126-.L105
	.long	.L100-.L105
	.long	.L125-.L105
	.long	.L124-.L105
	.long	.L123-.L105
	.long	.L188-.L105
	.long	.L122-.L105
	.long	.L121-.L105
	.long	.L120-.L105
	.long	.L119-.L105
	.long	.L118-.L105
	.long	.L117-.L105
	.long	.L116-.L105
	.long	.L115-.L105
	.long	.L114-.L105
	.long	.L113-.L105
	.long	.L112-.L105
	.long	.L111-.L105
	.long	.L110-.L105
	.long	.L109-.L105
	.long	.L108-.L105
	.long	.L107-.L105
	.long	.L106-.L105
	.long	.L104-.L105
	.text
.L188:
	movl	$2, %r12d
	call	opt_next@PLT
	testl	%eax, %eax
	jne	.L131
.L267:
	movl	%eax, -248(%rbp)
	call	opt_num_rest@PLT
	movl	%eax, %ebx
	movl	%eax, -252(%rbp)
	call	opt_rest@PLT
	testq	%r13, %r13
	movl	-248(%rbp), %r10d
	setne	-241(%rbp)
	cmpl	$1, %ebx
	movq	%rax, -264(%rbp)
	movzbl	-241(%rbp), %eax
	jle	.L132
	testb	%al, %al
	jne	.L268
.L132:
	cmpq	$0, -192(%rbp)
	jne	.L133
	movl	-176(%rbp), %r11d
	testl	%r11d, %r11d
	jne	.L269
.L133:
	movl	%r10d, -248(%rbp)
	movl	-200(%rbp), %r10d
	movl	$0, %eax
	testl	%r10d, %r10d
	cmovne	-152(%rbp), %rax
	movq	%rax, -272(%rbp)
	call	BIO_s_file@PLT
	movq	%rax, %rdi
	call	BIO_new@PLT
	movq	%rax, %r14
	call	BIO_f_md@PLT
	movq	%rax, %rdi
	call	BIO_new@PLT
	movq	%rax, %r15
	testq	%r14, %r14
	je	.L200
	testq	%rax, %rax
	movl	-248(%rbp), %r10d
	je	.L200
	movl	-208(%rbp), %r9d
	testl	%r9d, %r9d
	jne	.L270
.L137:
	movq	-216(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	leaq	-104(%rbp), %rdx
	movl	%r10d, -200(%rbp)
	call	app_passwd@PLT
	movl	-200(%rbp), %r10d
	testl	%eax, %eax
	je	.L271
	cmpl	$-1, -164(%rbp)
	je	.L272
	movl	-164(%rbp), %r8d
	movl	$32769, %edx
	testl	%r8d, %r8d
	je	.L140
	movl	$1, -164(%rbp)
	movl	$2, %edx
.L140:
	movq	-232(%rbp), %rdi
	movl	$119, %esi
	movl	%r10d, -200(%rbp)
	call	bio_open_default@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L266
	xorl	%edx, %edx
	movzbl	-241(%rbp), %eax
	cmpq	$0, -224(%rbp)
	leaq	.LC28(%rip), %rsi
	setne	%dl
	movl	-200(%rbp), %r10d
	addl	%edx, %eax
	xorl	%edx, %edx
	cmpq	$0, -184(%rbp)
	setne	%dl
	addl	%edx, %eax
	cmpl	$1, %eax
	jg	.L265
	testq	%r13, %r13
	je	.L142
	movl	-168(%rbp), %edi
	movl	%r10d, -200(%rbp)
	leaq	.LC29(%rip), %r9
	movl	-116(%rbp), %esi
	movq	-152(%rbp), %r8
	testl	%edi, %edi
	je	.L143
	movq	%r13, %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	call	load_pubkey@PLT
	movl	-200(%rbp), %r10d
	movq	%rax, -112(%rbp)
	movq	%rax, %r13
.L144:
	movl	%r10d, -200(%rbp)
	testq	%r13, %r13
	je	.L196
	movq	%r13, %rdi
	call	EVP_PKEY_id@PLT
	movl	-200(%rbp), %r10d
	subl	$1087, %eax
	cmpl	$1, %eax
	jbe	.L273
.L142:
	movq	-224(%rbp), %rsi
	testq	%rsi, %rsi
	je	.L145
	movq	-272(%rbp), %rdx
	xorl	%ecx, %ecx
	leaq	-80(%rbp), %rdi
	movl	%r10d, -200(%rbp)
	movq	$0, -80(%rbp)
	call	init_gen_str@PLT
	movl	-200(%rbp), %r10d
	testl	%eax, %eax
	je	.L263
	xorl	%r13d, %r13d
	cmpq	$0, -136(%rbp)
	je	.L153
	movl	%r12d, -208(%rbp)
	movl	%r13d, %r12d
	movq	%rbx, -200(%rbp)
	movq	-136(%rbp), %rbx
	movl	%r10d, -216(%rbp)
	jmp	.L148
.L152:
	movl	%r12d, %esi
	movq	%rbx, %rdi
	call	OPENSSL_sk_value@PLT
	movq	-80(%rbp), %rdi
	movq	%rax, %rsi
	movq	%rax, %r13
	call	pkey_ctrl_string@PLT
	testl	%eax, %eax
	jle	.L274
	addl	$1, %r12d
.L148:
	movq	%rbx, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r12d
	jl	.L152
	movl	-208(%rbp), %r12d
	movq	-200(%rbp), %rbx
	movl	-216(%rbp), %r10d
.L153:
	movq	-80(%rbp), %rdi
	leaq	-112(%rbp), %rsi
	movl	%r10d, -200(%rbp)
	call	EVP_PKEY_keygen@PLT
	movl	-200(%rbp), %r10d
	testl	%eax, %eax
	jle	.L275
	movq	-80(%rbp), %rdi
	movl	%r10d, -200(%rbp)
	call	EVP_PKEY_CTX_free@PLT
	movl	-200(%rbp), %r10d
.L145:
	movq	-184(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L154
	movq	-272(%rbp), %rsi
	movq	$-1, %rcx
	movl	$855, %edi
	movl	%r10d, -184(%rbp)
	call	EVP_PKEY_new_raw_private_key@PLT
	movl	-184(%rbp), %r10d
	testq	%rax, %rax
	movq	%rax, -112(%rbp)
	movq	%rax, %r13
	je	.L196
.L157:
	xorl	%edx, %edx
	leaq	-88(%rbp), %rcx
	movl	$120, %esi
	movq	%r15, %rdi
	movl	%r10d, -184(%rbp)
	movq	$0, -88(%rbp)
	movq	$0, -80(%rbp)
	call	BIO_ctrl@PLT
	movl	-184(%rbp), %r10d
	testq	%rax, %rax
	je	.L276
	movl	-176(%rbp), %esi
	leaq	-80(%rbp), %rax
	movq	-112(%rbp), %r8
	movl	%r10d, -176(%rbp)
	movq	%rax, -184(%rbp)
	movq	-88(%rbp), %rdi
	testl	%esi, %esi
	movq	-272(%rbp), %rcx
	movq	-240(%rbp), %rdx
	movq	%rax, %rsi
	je	.L160
	call	EVP_DigestVerifyInit@PLT
	movl	-176(%rbp), %r10d
.L161:
	testl	%eax, %eax
	je	.L277
	xorl	%r13d, %r13d
	cmpq	$0, -144(%rbp)
	je	.L164
	movl	%r12d, -200(%rbp)
	movl	%r13d, %r12d
	movq	%rbx, -176(%rbp)
	movq	-144(%rbp), %rbx
	movl	%r10d, -208(%rbp)
	jmp	.L163
.L166:
	movl	%r12d, %esi
	movq	%rbx, %rdi
	call	OPENSSL_sk_value@PLT
	movq	-80(%rbp), %rdi
	movq	%rax, %rsi
	movq	%rax, %r13
	call	pkey_ctrl_string@PLT
	testl	%eax, %eax
	jle	.L278
	addl	$1, %r12d
.L163:
	movq	%rbx, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r12d
	jl	.L166
	movl	-200(%rbp), %r12d
	movq	-176(%rbp), %rbx
	movl	-208(%rbp), %r10d
.L164:
	movq	-192(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L198
	movq	-112(%rbp), %r13
	testq	%r13, %r13
	je	.L199
	leaq	.LC37(%rip), %rsi
	movl	%r10d, -176(%rbp)
	call	BIO_new_file@PLT
	movl	-176(%rbp), %r10d
	testq	%rax, %rax
	je	.L279
	movq	-112(%rbp), %rdi
	movl	%r10d, -208(%rbp)
	movq	%rax, -200(%rbp)
	call	EVP_PKEY_size@PLT
	leaq	.LC39(%rip), %rsi
	movl	%eax, %edi
	movl	%eax, -176(%rbp)
	call	app_malloc@PLT
	movq	-200(%rbp), %r8
	movl	-176(%rbp), %edx
	movq	%rax, %rsi
	movq	%rax, %r13
	movq	%r8, %rdi
	call	BIO_read@PLT
	movq	-200(%rbp), %r8
	movl	%eax, -176(%rbp)
	movq	%r8, %rdi
	call	BIO_free@PLT
	movl	-176(%rbp), %ecx
	movl	-208(%rbp), %r10d
	testl	%ecx, %ecx
	jle	.L280
.L172:
	movq	%r14, %rsi
	movq	%r15, %rdi
	movl	%r10d, -192(%rbp)
	call	BIO_push@PLT
	cmpq	$0, -240(%rbp)
	movl	-192(%rbp), %r10d
	movq	%rax, -200(%rbp)
	je	.L281
.L174:
	movl	-252(%rbp), %edx
	testl	%edx, %edx
	je	.L282
	movl	-164(%rbp), %eax
	movq	$0, -80(%rbp)
	testl	%eax, %eax
	jne	.L181
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L179
	movl	%r10d, -192(%rbp)
	call	EVP_PKEY_get0_asn1@PLT
	movl	-192(%rbp), %r10d
	testq	%rax, %rax
	movq	%rax, %r9
	je	.L179
	movq	-184(%rbp), %r8
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	EVP_PKEY_asn1_get0_info@PLT
	movl	-192(%rbp), %r10d
.L179:
	movq	-240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L181
	movl	%r10d, -192(%rbp)
	call	EVP_MD_type@PLT
	movl	%eax, %edi
	call	OBJ_nid2sn@PLT
	movl	-192(%rbp), %r10d
	movq	%rax, -184(%rbp)
.L177:
	movl	-252(%rbp), %eax
	testl	%eax, %eax
	jle	.L128
	movq	-264(%rbp), %r11
	subl	$1, %eax
	movq	%rbx, -208(%rbp)
	movq	%r13, -216(%rbp)
	movl	%r10d, %r13d
	leaq	8(%r11,%rax,8), %rax
	movq	%r11, %rbx
	movq	%rax, -192(%rbp)
	jmp	.L186
.L183:
	movl	-176(%rbp), %eax
	subq	$8, %rsp
	pushq	(%rbx)
	movl	%r12d, %ecx
	movq	-200(%rbp), %rdx
	pushq	-184(%rbp)
	movq	-160(%rbp), %rsi
	pushq	-80(%rbp)
	movq	-208(%rbp), %rdi
	pushq	%rax
	pushq	-216(%rbp)
	movq	-112(%rbp), %r9
	movl	-164(%rbp), %r8d
	call	do_fp
	addq	$48, %rsp
	movl	$1, %esi
	movq	%r15, %rdi
	testl	%eax, %eax
	cmovne	%eax, %r13d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	call	BIO_ctrl@PLT
.L184:
	addq	$8, %rbx
	cmpq	%rbx, -192(%rbp)
	je	.L283
.L186:
	movq	(%rbx), %rcx
	movl	$3, %edx
	movl	$108, %esi
	movq	%r14, %rdi
	call	BIO_ctrl@PLT
	testl	%eax, %eax
	jg	.L183
	movq	(%rbx), %rdi
	addl	$1, %r13d
	call	perror@PLT
	jmp	.L184
.L123:
	movl	$1, %r12d
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L101:
	leal	-1501(%rax), %edx
	cmpl	$1, %edx
	ja	.L100
	movl	%eax, %edi
	call	opt_rand@PLT
	testl	%eax, %eax
	jne	.L100
	jmp	.L189
.L286:
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, -144(%rbp)
	testq	%rax, %rax
	jne	.L129
.L126:
	movq	bio_err(%rip), %rdi
	movq	%r14, %rdx
	leaq	.LC23(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L189:
	xorl	%r13d, %r13d
	movl	$1, %r10d
	xorl	%ebx, %ebx
	xorl	%r15d, %r15d
	xorl	%r14d, %r14d
.L128:
	movq	-160(%rbp), %rdi
	movl	$421, %ecx
	leaq	.LC15(%rip), %rdx
	movl	$8192, %esi
	movl	%r10d, -164(%rbp)
	call	CRYPTO_clear_free@PLT
	movq	%r14, %rdi
	call	BIO_free@PLT
	movq	-104(%rbp), %rdi
	movl	$423, %edx
	leaq	.LC15(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%rbx, %rdi
	call	BIO_free_all@PLT
	movq	-112(%rbp), %rdi
	call	EVP_PKEY_free@PLT
	movq	-144(%rbp), %rdi
	call	OPENSSL_sk_free@PLT
	movq	-136(%rbp), %rdi
	call	OPENSSL_sk_free@PLT
	movl	$428, %edx
	leaq	.LC15(%rip), %rsi
	movq	%r13, %rdi
	call	CRYPTO_free@PLT
	movq	%r15, %rdi
	call	BIO_free@PLT
	movq	-152(%rbp), %rdi
	call	release_engine@PLT
	movl	-164(%rbp), %r10d
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L284
	leaq	-40(%rbp), %rsp
	movl	%r10d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L104:
	.cfi_restore_state
	call	opt_unknown@PLT
	movq	%r15, %rsi
	movq	%rax, %rdi
	call	opt_md@PLT
	testl	%eax, %eax
	je	.L126
	movq	-96(%rbp), %rax
	movq	%rax, -240(%rbp)
	jmp	.L100
.L106:
	cmpq	$0, -136(%rbp)
	je	.L285
.L130:
	call	opt_arg@PLT
	movq	-136(%rbp), %rdi
	movq	%rax, %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	jne	.L100
	jmp	.L126
.L107:
	cmpq	$0, -144(%rbp)
	je	.L286
.L129:
	call	opt_arg@PLT
	movq	-144(%rbp), %rdi
	movq	%rax, %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	jne	.L100
	jmp	.L126
.L108:
	call	opt_arg@PLT
	movq	%rax, -224(%rbp)
	jmp	.L100
.L109:
	call	opt_arg@PLT
	movq	%rax, -184(%rbp)
	jmp	.L100
.L110:
	leaq	.LC21(%rip), %rax
	movq	%rax, -184(%rbp)
	jmp	.L100
.L111:
	movl	$1, -208(%rbp)
	jmp	.L100
.L112:
	movl	$1, -164(%rbp)
	jmp	.L100
.L113:
	movl	$0, -164(%rbp)
	jmp	.L100
.L114:
	movl	$1, -200(%rbp)
	jmp	.L100
.L115:
	call	opt_arg@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	setup_engine@PLT
	movq	%rax, -152(%rbp)
	jmp	.L100
.L116:
	call	opt_arg@PLT
	leaq	-116(%rbp), %rdx
	movl	$1982, %esi
	movq	%rax, %rdi
	call	opt_format@PLT
	testl	%eax, %eax
	jne	.L100
	jmp	.L126
.L117:
	call	opt_arg@PLT
	movq	%rax, -192(%rbp)
	jmp	.L100
.L118:
	call	opt_arg@PLT
	movl	$1, -176(%rbp)
	movq	%rax, %r13
	jmp	.L100
.L119:
	call	opt_arg@PLT
	movl	$1, -176(%rbp)
	movl	$1, -168(%rbp)
	movq	%rax, %r13
	jmp	.L100
.L120:
	call	opt_arg@PLT
	movq	%rax, -216(%rbp)
	jmp	.L100
.L121:
	call	opt_arg@PLT
	movq	%rax, %r13
	jmp	.L100
.L122:
	call	opt_arg@PLT
	movq	%rax, -232(%rbp)
	jmp	.L100
.L124:
	movq	bio_out(%rip), %rdi
	leaq	.LC24(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_out(%rip), %rax
	leaq	show_digests(%rip), %rsi
	movl	$1, %edi
	leaq	-80(%rbp), %rdx
	movl	$0, -72(%rbp)
	movq	%rax, -80(%rbp)
	call	OBJ_NAME_do_all_sorted@PLT
	movq	bio_out(%rip), %rdi
	leaq	.LC3(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L264:
	xorl	%r13d, %r13d
	xorl	%r10d, %r10d
	xorl	%ebx, %ebx
	xorl	%r15d, %r15d
	xorl	%r14d, %r14d
	jmp	.L128
.L125:
	leaq	dgst_options(%rip), %rdi
	call	opt_help@PLT
	jmp	.L264
.L270:
	movq	BIO_debug_callback@GOTPCREL(%rip), %rsi
	movq	%r14, %rdi
	movl	%r10d, -200(%rbp)
	call	BIO_set_callback@PLT
	movq	bio_err(%rip), %rsi
	movq	%r14, %rdi
	call	BIO_set_callback_arg@PLT
	movl	-200(%rbp), %r10d
	jmp	.L137
.L269:
	movq	bio_err(%rip), %rdi
	leaq	.LC26(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L189
.L272:
	cmpq	$1, %r13
	sbbl	%eax, %eax
	addl	$1, %eax
	cmpq	$1, %r13
	sbbl	%edx, %edx
	movl	%eax, -164(%rbp)
	andl	$32767, %edx
	addl	$2, %edx
	jmp	.L140
.L285:
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, -136(%rbp)
	testq	%rax, %rax
	jne	.L130
	jmp	.L126
.L273:
	leaq	.LC30(%rip), %rsi
.L265:
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L266:
	xorl	%r13d, %r13d
	movl	$1, %r10d
	jmp	.L128
.L268:
	movq	bio_err(%rip), %rdi
	movq	%r14, %rdx
	leaq	.LC25(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L189
.L200:
	movq	bio_err(%rip), %rdi
	xorl	%r13d, %r13d
	xorl	%ebx, %ebx
	call	ERR_print_errors@PLT
	movl	$1, %r10d
	jmp	.L128
.L271:
	movq	bio_err(%rip), %rdi
	leaq	.LC27(%rip), %rsi
	xorl	%r13d, %r13d
	xorl	%ebx, %ebx
	call	BIO_printf@PLT
	movl	$1, %r10d
	jmp	.L128
.L143:
	movq	-104(%rbp), %rcx
	movq	%r13, %rdi
	xorl	%edx, %edx
	call	load_key@PLT
	movl	-200(%rbp), %r10d
	movq	%rax, -112(%rbp)
	movq	%rax, %r13
	jmp	.L144
.L154:
	movq	-112(%rbp), %r13
	testq	%r13, %r13
	jne	.L157
	leaq	-80(%rbp), %rax
	xorl	%edx, %edx
	movl	$120, %esi
	movq	%r15, %rdi
	movq	%rax, %rcx
	movl	%r10d, -176(%rbp)
	movq	$0, -80(%rbp)
	movq	%rax, -184(%rbp)
	call	BIO_ctrl@PLT
	movl	-176(%rbp), %r10d
	testq	%rax, %rax
	je	.L287
	cmpq	$0, -240(%rbp)
	je	.L288
.L170:
	movq	-272(%rbp), %rdx
	movq	-240(%rbp), %rsi
	movl	%r10d, -176(%rbp)
	movq	-80(%rbp), %rdi
	call	EVP_DigestInit_ex@PLT
	movl	-176(%rbp), %r10d
	testl	%eax, %eax
	jne	.L164
	movq	bio_err(%rip), %rdi
	leaq	.LC36(%rip), %rsi
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
.L196:
	movl	$1, %r10d
	jmp	.L128
.L275:
	movq	bio_err(%rip), %rdi
	leaq	.LC32(%rip), %rsi
	call	BIO_puts@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
.L263:
	movq	-80(%rbp), %rdi
	call	EVP_PKEY_CTX_free@PLT
	jmp	.L266
.L160:
	call	EVP_DigestSignInit@PLT
	movl	-176(%rbp), %r10d
	jmp	.L161
.L283:
	movl	%r13d, %r10d
	movq	-208(%rbp), %rbx
	movq	-216(%rbp), %r13
	jmp	.L128
.L274:
	movq	bio_err(%rip), %rdi
	movq	%r13, %rdx
	leaq	.LC31(%rip), %rsi
	xorl	%eax, %eax
	movq	-200(%rbp), %rbx
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	movq	-80(%rbp), %rdi
	call	EVP_PKEY_CTX_free@PLT
	jmp	.L266
.L181:
	movq	$0, -184(%rbp)
	jmp	.L177
.L199:
	movl	$0, -176(%rbp)
	jmp	.L172
.L276:
	movq	bio_err(%rip), %rdi
	leaq	.LC33(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	jmp	.L266
.L282:
	movq	stdin(%rip), %rcx
	xorl	%edx, %edx
	movl	$106, %esi
	movq	%r14, %rdi
	call	BIO_ctrl@PLT
	subq	$8, %rsp
	movq	-112(%rbp), %r9
	movl	%r12d, %ecx
	leaq	.LC41(%rip), %rax
	movl	-164(%rbp), %r8d
	movq	%rbx, %rdi
	movq	-200(%rbp), %rdx
	pushq	%rax
	movl	-176(%rbp), %eax
	pushq	$0
	movq	-160(%rbp), %rsi
	pushq	$0
	pushq	%rax
	pushq	%r13
	call	do_fp
	addq	$48, %rsp
	movl	%eax, %r10d
	jmp	.L128
.L278:
	movq	bio_err(%rip), %rdi
	movq	%r13, %rdx
	leaq	.LC35(%rip), %rsi
	xorl	%eax, %eax
	movq	-176(%rbp), %rbx
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	jmp	.L266
.L277:
	movq	bio_err(%rip), %rdi
	leaq	.LC34(%rip), %rsi
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	jmp	.L266
.L198:
	movl	$0, -176(%rbp)
	xorl	%r13d, %r13d
	jmp	.L172
.L281:
	movq	-184(%rbp), %rcx
	xorl	%edx, %edx
	movl	$120, %esi
	movq	%r15, %rdi
	call	BIO_ctrl@PLT
	movq	-80(%rbp), %rdi
	call	EVP_MD_CTX_md@PLT
	movl	-192(%rbp), %r10d
	movq	%rax, -240(%rbp)
	jmp	.L174
.L287:
	movq	bio_err(%rip), %rdi
	leaq	.LC33(%rip), %rsi
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	jmp	.L196
.L288:
	movl	%r10d, -176(%rbp)
	call	EVP_sha256@PLT
	movl	-176(%rbp), %r10d
	movq	%rax, -240(%rbp)
	jmp	.L170
.L280:
	movq	-192(%rbp), %rdx
	movq	bio_err(%rip), %rdi
	leaq	.LC40(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	movl	$1, %r10d
	jmp	.L128
.L279:
	movq	-192(%rbp), %rdx
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
	xorl	%r13d, %r13d
	leaq	.LC38(%rip), %rsi
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	movl	$1, %r10d
	jmp	.L128
.L284:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1437:
	.size	dgst_main, .-dgst_main
	.globl	dgst_options
	.section	.rodata.str1.8
	.align 8
.LC42:
	.string	"Usage: %s [options] [file...]\n"
	.align 8
.LC43:
	.string	"  file... files to digest (default is stdin)\n"
	.section	.rodata.str1.1
.LC44:
	.string	"help"
.LC45:
	.string	"Display this summary"
.LC46:
	.string	"list"
.LC47:
	.string	"List digests"
.LC48:
	.string	"c"
	.section	.rodata.str1.8
	.align 8
.LC49:
	.string	"Print the digest with separating colons"
	.section	.rodata.str1.1
.LC50:
	.string	"r"
	.section	.rodata.str1.8
	.align 8
.LC51:
	.string	"Print the digest in coreutils format"
	.section	.rodata.str1.1
.LC52:
	.string	"out"
	.section	.rodata.str1.8
	.align 8
.LC53:
	.string	"Output to filename rather than stdout"
	.section	.rodata.str1.1
.LC54:
	.string	"passin"
.LC55:
	.string	"Input file pass phrase source"
.LC56:
	.string	"sign"
.LC57:
	.string	"Sign digest using private key"
.LC58:
	.string	"verify"
	.section	.rodata.str1.8
	.align 8
.LC59:
	.string	"Verify a signature using public key"
	.section	.rodata.str1.1
.LC60:
	.string	"prverify"
	.section	.rodata.str1.8
	.align 8
.LC61:
	.string	"Verify a signature using private key"
	.section	.rodata.str1.1
.LC62:
	.string	"signature"
.LC63:
	.string	"File with signature to verify"
.LC64:
	.string	"keyform"
	.section	.rodata.str1.8
	.align 8
.LC65:
	.string	"Key file format (PEM or ENGINE)"
	.section	.rodata.str1.1
.LC66:
	.string	"hex"
.LC67:
	.string	"Print as hex dump"
.LC68:
	.string	"binary"
.LC69:
	.string	"Print in binary form"
.LC70:
	.string	"d"
.LC71:
	.string	"Print debug info"
.LC72:
	.string	"debug"
.LC73:
	.string	"fips-fingerprint"
	.section	.rodata.str1.8
	.align 8
.LC74:
	.string	"Compute HMAC with the key used in OpenSSL-FIPS fingerprint"
	.section	.rodata.str1.1
.LC75:
	.string	"hmac"
.LC76:
	.string	"Create hashed MAC with key"
.LC77:
	.string	"mac"
	.section	.rodata.str1.8
	.align 8
.LC78:
	.string	"Create MAC (not necessarily HMAC)"
	.section	.rodata.str1.1
.LC79:
	.string	"sigopt"
	.section	.rodata.str1.8
	.align 8
.LC80:
	.string	"Signature parameter in n:v form"
	.section	.rodata.str1.1
.LC81:
	.string	"macopt"
	.section	.rodata.str1.8
	.align 8
.LC82:
	.string	"MAC algorithm parameters in n:v form or key"
	.section	.rodata.str1.1
.LC83:
	.string	""
.LC84:
	.string	"Any supported digest"
.LC85:
	.string	"rand"
	.section	.rodata.str1.8
	.align 8
.LC86:
	.string	"Load the file(s) into the random number generator"
	.section	.rodata.str1.1
.LC87:
	.string	"writerand"
	.section	.rodata.str1.8
	.align 8
.LC88:
	.string	"Write random data to the specified file"
	.section	.rodata.str1.1
.LC89:
	.string	"engine"
	.section	.rodata.str1.8
	.align 8
.LC90:
	.string	"Use engine e, possibly a hardware device"
	.section	.rodata.str1.1
.LC91:
	.string	"engine_impl"
	.section	.rodata.str1.8
	.align 8
.LC92:
	.string	"Also use engine given by -engine for digest operations"
	.section	.data.rel.ro,"aw"
	.align 32
	.type	dgst_options, @object
	.size	dgst_options, 672
dgst_options:
	.quad	OPT_HELP_STR
	.long	1
	.long	45
	.quad	.LC42
	.quad	OPT_HELP_STR
	.long	1
	.long	45
	.quad	.LC43
	.quad	.LC44
	.long	1
	.long	45
	.quad	.LC45
	.quad	.LC46
	.long	2
	.long	45
	.quad	.LC47
	.quad	.LC48
	.long	3
	.long	45
	.quad	.LC49
	.quad	.LC50
	.long	4
	.long	45
	.quad	.LC51
	.quad	.LC52
	.long	5
	.long	62
	.quad	.LC53
	.quad	.LC54
	.long	7
	.long	115
	.quad	.LC55
	.quad	.LC56
	.long	6
	.long	115
	.quad	.LC57
	.quad	.LC58
	.long	8
	.long	115
	.quad	.LC59
	.quad	.LC60
	.long	9
	.long	115
	.quad	.LC61
	.quad	.LC62
	.long	10
	.long	60
	.quad	.LC63
	.quad	.LC64
	.long	11
	.long	102
	.quad	.LC65
	.quad	.LC66
	.long	14
	.long	45
	.quad	.LC67
	.quad	.LC68
	.long	15
	.long	45
	.quad	.LC69
	.quad	.LC70
	.long	16
	.long	45
	.quad	.LC71
	.quad	.LC72
	.long	16
	.long	45
	.quad	.LC71
	.quad	.LC73
	.long	17
	.long	45
	.quad	.LC74
	.quad	.LC75
	.long	18
	.long	115
	.quad	.LC76
	.quad	.LC77
	.long	19
	.long	115
	.quad	.LC78
	.quad	.LC79
	.long	20
	.long	115
	.quad	.LC80
	.quad	.LC81
	.long	21
	.long	115
	.quad	.LC82
	.quad	.LC83
	.long	22
	.long	45
	.quad	.LC84
	.quad	.LC85
	.long	1501
	.long	115
	.quad	.LC86
	.quad	.LC87
	.long	1502
	.long	62
	.quad	.LC88
	.quad	.LC89
	.long	12
	.long	115
	.quad	.LC90
	.quad	.LC91
	.long	13
	.long	45
	.quad	.LC92
	.quad	0
	.zero	16
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC11:
	.byte	10
	.byte	10
	.byte	10
	.byte	10
	.byte	10
	.byte	10
	.byte	10
	.byte	10
	.byte	10
	.byte	10
	.byte	10
	.byte	10
	.byte	10
	.byte	10
	.byte	10
	.byte	10
	.align 16
.LC12:
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
