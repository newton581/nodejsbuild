	.file	"srp.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"Validating\n   user=\"%s\"\n srp_verifier=\"%s\"\n srp_usersalt=\"%s\"\n g=\"%s\"\n N=\"%s\"\n"
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"Pass %s\n"
	.section	.rodata.str1.8
	.align 8
.LC2:
	.string	"../deps/openssl/openssl/apps/srp.c"
	.align 8
.LC3:
	.string	"assertion failed: srp_usersalt != NULL"
	.align 8
.LC4:
	.string	"Internal error validating SRP verifier\n"
	.text
	.p2align 4
	.type	srp_verify_user, @function
srp_verify_user:
.LFB1513:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%r9, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-1088(%rbp), %r15
	movq	%rsi, %r14
	movl	$1024, %esi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	leaq	-1104(%rbp), %rcx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$1096, %rsp
	movq	%rdi, -1136(%rbp)
	movq	%r15, %rdi
	movq	%rdx, -1128(%rbp)
	xorl	%edx, %edx
	movhps	-1136(%rbp), %xmm0
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -1104(%rbp)
	movq	$0, -1112(%rbp)
	call	password_callback@PLT
	testl	%eax, %eax
	jle	.L10
	movl	16(%rbp), %ecx
	movslq	%eax, %r12
	movq	-1128(%rbp), %r8
	movb	$0, -1088(%rbp,%r12)
	testl	%ecx, %ecx
	jne	.L19
.L3:
	testq	%r8, %r8
	je	.L20
	movq	-1136(%rbp), %rdi
	movq	%r13, %r9
	movq	%rbx, %r8
	movq	%r15, %rsi
	leaq	-1112(%rbp), %rcx
	leaq	-1128(%rbp), %rdx
	call	SRP_create_verifier@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L21
	movq	-1112(%rbp), %rbx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	strcmp@PLT
	movl	$148, %edx
	leaq	.LC2(%rip), %rsi
	movq	%rbx, %rdi
	testl	%eax, %eax
	movl	$0, %eax
	cmovne	%rax, %r13
	call	CRYPTO_free@PLT
.L7:
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	OPENSSL_cleanse@PLT
.L1:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L22
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	.cfi_restore_state
	subq	$8, %rsp
	movq	-1136(%rbp), %rdx
	xorl	%eax, %eax
	movq	%r13, %r9
	pushq	%rbx
	movq	bio_err(%rip), %rdi
	movq	%r14, %rcx
	leaq	.LC0(%rip), %rsi
	call	BIO_printf@PLT
	cmpl	$1, 16(%rbp)
	popq	%rax
	popq	%rdx
	jle	.L17
	movq	bio_err(%rip), %rdi
	movq	%r15, %rdx
	leaq	.LC1(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L17:
	movq	-1128(%rbp), %r8
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L10:
	xorl	%r13d, %r13d
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L21:
	movq	bio_err(%rip), %rdi
	leaq	.LC4(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L7
.L22:
	call	__stack_chk_fail@PLT
.L20:
	movl	$141, %edx
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	call	OPENSSL_die@PLT
	.cfi_endproc
.LFE1513:
	.size	srp_verify_user, .-srp_verify_user
	.section	.rodata.str1.8
	.align 8
.LC5:
	.string	"Creating\n user=\"%s\"\n g=\"%s\"\n N=\"%s\"\n"
	.align 8
.LC6:
	.string	"Internal error creating SRP verifier\n"
	.align 8
.LC7:
	.string	"gNid=%s salt =\"%s\"\n verifier =\"%s\"\n"
	.text
	.p2align 4
	.type	srp_create_user, @function
srp_create_user:
.LFB1514:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%r9, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-1088(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	leaq	-1104(%rbp), %rcx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movl	$1024, %esi
	subq	$1112, %rsp
	movq	%rdi, -1128(%rbp)
	movq	%r14, %rdi
	movq	%rdx, -1144(%rbp)
	movl	$1, %edx
	movq	%r8, -1136(%rbp)
	movhps	-1128(%rbp), %xmm0
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -1104(%rbp)
	movq	$0, -1112(%rbp)
	call	password_callback@PLT
	testl	%eax, %eax
	jle	.L23
	movslq	%eax, %r12
	movl	16(%rbp), %eax
	movq	-1136(%rbp), %r8
	movb	$0, -1088(%rbp,%r12)
	testl	%eax, %eax
	jne	.L34
.L25:
	movq	-1128(%rbp), %rdi
	movq	%r13, %r9
	movq	%rbx, %rcx
	movq	%r14, %rsi
	leaq	-1112(%rbp), %rdx
	call	SRP_create_verifier@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L35
	movq	-1112(%rbp), %rax
	movq	-1144(%rbp), %rsi
	movq	%rax, (%rsi)
.L27:
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	OPENSSL_cleanse@PLT
	cmpl	$1, 16(%rbp)
	jg	.L36
.L23:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L37
	addq	$1112, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L36:
	.cfi_restore_state
	movq	(%rbx), %r8
	movq	-1112(%rbp), %rcx
	movq	%r15, %rdx
	xorl	%eax, %eax
	movq	bio_err(%rip), %rdi
	leaq	.LC7(%rip), %rsi
	call	BIO_printf@PLT
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L34:
	movq	-1128(%rbp), %rdx
	movq	bio_err(%rip), %rdi
	movq	%r13, %rcx
	xorl	%eax, %eax
	leaq	.LC5(%rip), %rsi
	call	BIO_printf@PLT
	movq	-1136(%rbp), %r8
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L35:
	movq	bio_err(%rip), %rdi
	leaq	.LC6(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L27
.L37:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1514:
	.size	srp_create_user, .-srp_create_user
	.p2align 4
	.type	get_index, @function
get_index:
.LFB1507:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	testq	%rsi, %rsi
	je	.L45
	movq	%rdi, %rbx
	movq	%rsi, %r13
	xorl	%r12d, %r12d
	cmpb	$73, %dl
	jne	.L42
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L47:
	movq	8(%rbx), %rax
	movl	%r12d, %esi
	movq	8(%rax), %rdi
	call	OPENSSL_sk_value@PLT
	movq	(%rax), %rdx
	cmpb	$73, (%rdx)
	je	.L46
	movq	24(%rax), %rsi
	movq	%r13, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L38
.L46:
	addl	$1, %r12d
.L42:
	movq	8(%rbx), %rax
	movq	8(%rax), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r12d
	jl	.L47
.L45:
	movl	$-1, %r12d
.L38:
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L56:
	.cfi_restore_state
	movq	24(%rax), %rsi
	movq	%r13, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L38
.L43:
	addl	$1, %r12d
.L41:
	movq	8(%rbx), %rax
	movq	8(%rax), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r12d
	jge	.L45
	movq	8(%rbx), %rax
	movl	%r12d, %esi
	movq	8(%rax), %rdi
	call	OPENSSL_sk_value@PLT
	movq	(%rax), %rdx
	cmpb	$73, (%rdx)
	jne	.L43
	jmp	.L56
	.cfi_endproc
.LFE1507:
	.size	get_index, .-get_index
	.section	.rodata.str1.1
.LC8:
	.string	"User entry"
.LC9:
	.string	"%s \"%s\"\n"
.LC10:
	.string	"  %d = \"%s\"\n"
.LC11:
	.string	"g N entry"
	.text
	.p2align 4
	.type	print_user.part.0, @function
print_user.part.0:
.LFB1521:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movq	8(%rdi), %rax
	movq	8(%rax), %rdi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %r14
	movq	(%rax), %rax
	cmpb	$73, (%rax)
	je	.L57
	testl	%r13d, %r13d
	jns	.L65
.L59:
	movq	32(%r14), %rsi
	movl	$73, %edx
	movq	%r12, %rdi
	call	get_index
	movl	%eax, %esi
	testl	%eax, %eax
	jns	.L66
.L57:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L66:
	.cfi_restore_state
	movq	8(%r12), %rax
	xorl	%ebx, %ebx
	leaq	.LC10(%rip), %r13
	movq	8(%rax), %rdi
	call	OPENSSL_sk_value@PLT
	movq	bio_err(%rip), %rdi
	leaq	.LC11(%rip), %rdx
	leaq	.LC9(%rip), %rsi
	movq	24(%rax), %rcx
	movq	%rax, %r12
	xorl	%eax, %eax
	call	BIO_printf@PLT
	.p2align 4,,10
	.p2align 3
.L61:
	movq	(%r12,%rbx,8), %rcx
	movq	bio_err(%rip), %rdi
	movl	%ebx, %edx
	movq	%r13, %rsi
	xorl	%eax, %eax
	addq	$1, %rbx
	call	BIO_printf@PLT
	cmpq	$6, %rbx
	jne	.L61
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L65:
	.cfi_restore_state
	movq	8(%r12), %rax
	movl	%r13d, %esi
	xorl	%ebx, %ebx
	leaq	.LC10(%rip), %r13
	movq	8(%rax), %rdi
	call	OPENSSL_sk_value@PLT
	movq	bio_err(%rip), %rdi
	leaq	.LC8(%rip), %rdx
	leaq	.LC9(%rip), %rsi
	movq	24(%rax), %rcx
	movq	%rax, %r15
	xorl	%eax, %eax
	call	BIO_printf@PLT
	.p2align 4,,10
	.p2align 3
.L60:
	movq	(%r15,%rbx,8), %rcx
	movq	bio_err(%rip), %rdi
	movl	%ebx, %edx
	movq	%r13, %rsi
	xorl	%eax, %eax
	addq	$1, %rbx
	call	BIO_printf@PLT
	cmpq	$6, %rbx
	jne	.L60
	jmp	.L59
	.cfi_endproc
.LFE1521:
	.size	print_user.part.0, .-print_user.part.0
	.section	.rodata.str1.1
.LC12:
	.string	"%s: Use -help for summary.\n"
	.section	.rodata.str1.8
	.align 8
.LC13:
	.string	"%s: Only one of -add/-delete/-modify/-list\n"
	.align 8
.LC14:
	.string	"-srpvfile and -configfile cannot be specified together.\n"
	.align 8
.LC15:
	.string	"Exactly one of the options -add, -delete, -modify -list must be specified.\n"
	.section	.rodata.str1.1
.LC16:
	.string	"Need at least one user.\n"
	.section	.rodata.str1.8
	.align 8
.LC17:
	.string	"-passin, -passout arguments only valid with one user.\n"
	.section	.rodata.str1.1
.LC18:
	.string	"Error getting passwords\n"
.LC19:
	.string	"Using configuration from %s\n"
	.section	.rodata.str1.8
	.align 8
.LC20:
	.string	"trying to read default_srp in srp\n"
	.section	.rodata.str1.1
.LC21:
	.string	"default_srp"
.LC22:
	.string	"srp"
	.section	.rodata.str1.8
	.align 8
.LC23:
	.string	"variable lookup failed for %s::%s\n"
	.align 8
.LC24:
	.string	"trying to read srpvfile in section \"%s\"\n"
	.section	.rodata.str1.1
.LC25:
	.string	"srpvfile"
	.section	.rodata.str1.8
	.align 8
.LC26:
	.string	"Trying to read SRP verifier file \"%s\"\n"
	.section	.rodata.str1.1
.LC27:
	.string	"Database initialised\n"
.LC28:
	.string	"Default g and N"
	.section	.rodata.str1.8
	.align 8
.LC29:
	.string	"No g and N value for index \"%s\"\n"
	.align 8
.LC30:
	.string	"Database has no g N information.\n"
	.section	.rodata.str1.1
.LC31:
	.string	"Starting user processing\n"
.LC32:
	.string	"Processing user \"%s\"\n"
.LC33:
	.string	"List all users\n"
	.section	.rodata.str1.8
	.align 8
.LC34:
	.string	"user \"%s\" does not exist, ignored. t\n"
	.section	.rodata.str1.1
.LC35:
	.string	"user \"%s\" reactivated.\n"
	.section	.rodata.str1.8
	.align 8
.LC36:
	.string	"Cannot create srp verifier for user \"%s\", operation abandoned .\n"
	.section	.rodata.str1.1
.LC37:
	.string	"v"
.LC38:
	.string	"row pointers"
.LC39:
	.string	"failed to update srpvfile\n"
.LC40:
	.string	"TXT_DB error number %ld\n"
	.section	.rodata.str1.8
	.align 8
.LC41:
	.string	"user \"%s\" does not exist, operation ignored.\n"
	.align 8
.LC42:
	.string	"user \"%s\" already updated, operation ignored.\n"
	.align 8
.LC43:
	.string	"Verifying password for user \"%s\"\n"
	.align 8
.LC44:
	.string	"Invalid password for user \"%s\", operation abandoned.\n"
	.section	.rodata.str1.1
.LC45:
	.string	"Password for user \"%s\" ok.\n"
	.section	.rodata.str1.8
	.align 8
.LC46:
	.string	"Cannot create srp verifier for user \"%s\", operation abandoned.\n"
	.align 8
.LC47:
	.string	"user \"%s\" does not exist, operation ignored. t\n"
	.section	.rodata.str1.1
.LC48:
	.string	"user \"%s\" revoked. t\n"
.LC49:
	.string	"User procession done.\n"
.LC50:
	.string	"Temporary srpvfile created.\n"
.LC51:
	.string	"old"
.LC52:
	.string	"new"
.LC53:
	.string	"srpvfile updated.\n"
.LC54:
	.string	"User errors %d.\n"
	.section	.rodata.str1.8
	.align 8
.LC55:
	.string	"SRP terminating with code %d.\n"
	.section	.rodata.str1.1
.LC56:
	.string	"Trying to update srpvfile.\n"
	.text
	.p2align 4
	.globl	srp_main
	.type	srp_main, @function
srp_main:
.LFB1515:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	srp_options(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	xorl	%r14d, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	xorl	%r13d, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	.L72(%rip), %rbx
	subq	$184, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -128(%rbp)
	movq	$0, -120(%rbp)
	call	opt_init@PLT
	movq	$0, -152(%rbp)
	movq	$0, -168(%rbp)
	movq	%rax, %r12
	movq	$0, -184(%rbp)
	movq	$0, -176(%rbp)
	movq	$0, -160(%rbp)
	movl	$-1, -136(%rbp)
	movl	$0, -132(%rbp)
	movq	$0, -144(%rbp)
.L68:
	call	opt_next@PLT
	testl	%eax, %eax
	je	.L337
.L89:
	cmpl	$14, %eax
	jg	.L69
	cmpl	$-1, %eax
	jl	.L68
	leal	1(%rax), %edx
	cmpl	$15, %edx
	ja	.L68
	movslq	(%rbx,%rdx,4), %rdx
	addq	%rbx, %rdx
	notrack jmp	*%rdx
	.section	.rodata
	.align 4
	.align 4
.L72:
	.long	.L83-.L72
	.long	.L68-.L72
	.long	.L82-.L72
	.long	.L81-.L72
	.long	.L80-.L72
	.long	.L79-.L72
	.long	.L78-.L72
	.long	.L77-.L72
	.long	.L77-.L72
	.long	.L77-.L72
	.long	.L77-.L72
	.long	.L76-.L72
	.long	.L75-.L72
	.long	.L74-.L72
	.long	.L73-.L72
	.long	.L71-.L72
	.text
	.p2align 4,,10
	.p2align 3
.L69:
	leal	-1501(%rax), %edx
	cmpl	$1, %edx
	ja	.L68
	movl	%eax, %edi
	call	opt_rand@PLT
	testl	%eax, %eax
	jne	.L68
	movl	$1, %r12d
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
.L88:
	cmpl	$0, -132(%rbp)
	je	.L169
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L348:
	movq	bio_err(%rip), %rdi
	leaq	.LC16(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L83:
	movq	bio_err(%rip), %rdi
	movq	%r12, %rdx
	xorl	%eax, %eax
	xorl	%r14d, %r14d
	leaq	.LC12(%rip), %rsi
	xorl	%r13d, %r13d
	call	BIO_printf@PLT
.L85:
	movl	-132(%rbp), %esi
	testl	%esi, %esi
	jne	.L212
.L321:
	movq	-128(%rbp), %rdi
	movl	$600, %edx
	leaq	.LC2(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-120(%rbp), %rdi
	movl	$601, %edx
	leaq	.LC2(%rip), %rsi
	call	CRYPTO_free@PLT
	.p2align 4,,10
	.p2align 3
.L171:
	movq	bio_err(%rip), %rdi
	movl	$1, %r12d
	call	ERR_print_errors@PLT
	jmp	.L170
.L71:
	call	opt_arg@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	setup_engine@PLT
	movq	%rax, -144(%rbp)
	call	opt_next@PLT
	testl	%eax, %eax
	jne	.L89
	.p2align 4,,10
	.p2align 3
.L337:
	movl	%eax, %r15d
	call	opt_num_rest@PLT
	movl	%eax, -200(%rbp)
	call	opt_rest@PLT
	cmpq	$0, -152(%rbp)
	movq	%rax, -192(%rbp)
	je	.L90
	cmpq	$0, -168(%rbp)
	jne	.L338
.L90:
	cmpl	$-1, -136(%rbp)
	je	.L339
	movl	-136(%rbp), %eax
	subl	$6, %eax
	cmpl	$2, %eax
	jbe	.L340
	xorl	%ebx, %ebx
.L92:
	movq	-160(%rbp), %rax
	orq	%r14, %rax
	je	.L94
	cmpl	$1, -200(%rbp)
	jne	.L341
.L94:
	movq	-160(%rbp), %rsi
	leaq	-120(%rbp), %rcx
	leaq	-128(%rbp), %rdx
	movq	%r14, %rdi
	call	app_passwd@PLT
	testl	%eax, %eax
	je	.L342
	cmpq	$0, -152(%rbp)
	je	.L343
	movl	-132(%rbp), %eax
	xorl	%r14d, %r14d
	testl	%eax, %eax
	jne	.L109
.L110:
	movq	-152(%rbp), %rdi
	xorl	%esi, %esi
	call	load_index@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L321
.L180:
	movq	8(%r13), %rax
	movq	%r14, -208(%rbp)
	xorl	%r12d, %r12d
	movl	$-1, -160(%rbp)
	movq	8(%rax), %rdi
	movl	$-1, %eax
	movl	%r15d, -168(%rbp)
	movq	%rbx, -200(%rbp)
	movl	%eax, %r14d
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L112:
	movq	8(%r13), %rax
	movq	8(%rax), %rdi
.L114:
	addl	$1, %r12d
.L111:
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r12d
	jge	.L344
	movq	8(%r13), %rax
	movl	%r12d, %esi
	movq	8(%rax), %rdi
	call	OPENSSL_sk_value@PLT
	movq	(%rax), %rdx
	cmpb	$73, (%rdx)
	jne	.L112
	testl	%r14d, %r14d
	jns	.L113
	movq	-176(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L113
	movq	24(%rax), %rsi
	call	strcmp@PLT
	testl	%eax, %eax
	cmove	%r12d, %r14d
.L113:
	movq	8(%r13), %rax
	cmpl	$1, -132(%rbp)
	movl	%r12d, -160(%rbp)
	movq	8(%rax), %rdi
	jle	.L114
	movl	%r12d, %esi
	xorl	%r15d, %r15d
	call	OPENSSL_sk_value@PLT
	movq	bio_err(%rip), %rdi
	leaq	.LC11(%rip), %rdx
	leaq	.LC9(%rip), %rsi
	movq	24(%rax), %rcx
	movq	%rax, %rbx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	.p2align 4,,10
	.p2align 3
.L115:
	movq	(%rbx,%r15,8), %rcx
	movq	bio_err(%rip), %rdi
	movl	%r15d, %edx
	xorl	%eax, %eax
	leaq	.LC10(%rip), %rsi
	addq	$1, %r15
	call	BIO_printf@PLT
	cmpq	$6, %r15
	jne	.L115
	movq	8(%r13), %rax
	movl	%r12d, -160(%rbp)
	movq	8(%rax), %rdi
	jmp	.L114
.L73:
	call	opt_arg@PLT
	movq	%rax, -160(%rbp)
	jmp	.L68
.L74:
	call	opt_arg@PLT
	movq	%rax, %r14
	jmp	.L68
.L75:
	call	opt_arg@PLT
	movq	%rax, -184(%rbp)
	jmp	.L68
.L76:
	call	opt_arg@PLT
	movq	%rax, -176(%rbp)
	jmp	.L68
.L77:
	cmpl	$-1, -136(%rbp)
	jne	.L345
	movl	%eax, -136(%rbp)
	jmp	.L68
.L78:
	call	opt_arg@PLT
	movq	%rax, -152(%rbp)
	jmp	.L68
.L79:
	call	opt_arg@PLT
	movq	%rax, %r13
	jmp	.L68
.L80:
	call	opt_arg@PLT
	movq	%rax, -168(%rbp)
	jmp	.L68
.L81:
	addl	$1, -132(%rbp)
	jmp	.L68
.L82:
	leaq	srp_options(%rip), %rdi
	call	opt_help@PLT
	movl	-132(%rbp), %esi
	testl	%esi, %esi
	jne	.L346
	movq	-128(%rbp), %rdi
	movl	$600, %edx
	xorl	%r12d, %r12d
	xorl	%r14d, %r14d
	leaq	.LC2(%rip), %rsi
	xorl	%r13d, %r13d
	call	CRYPTO_free@PLT
	movq	-120(%rbp), %rdi
	movl	$601, %edx
	leaq	.LC2(%rip), %rsi
	call	CRYPTO_free@PLT
.L170:
	movq	%r14, %rdi
	call	NCONF_free@PLT
	movq	%r13, %rdi
	call	free_index@PLT
	movq	-144(%rbp), %rdi
	call	release_engine@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L347
	leaq	-40(%rbp), %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L346:
	.cfi_restore_state
	xorl	%r12d, %r12d
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
.L86:
	movq	bio_err(%rip), %rdi
	movl	%r12d, %edx
	leaq	.LC55(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L169:
	movq	-128(%rbp), %rdi
	movl	$600, %edx
	leaq	.LC2(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-120(%rbp), %rdi
	movl	$601, %edx
	leaq	.LC2(%rip), %rsi
	call	CRYPTO_free@PLT
	testl	%r12d, %r12d
	je	.L170
	jmp	.L171
.L340:
	movl	-200(%rbp), %ecx
	testl	%ecx, %ecx
	je	.L348
	movq	-192(%rbp), %rax
	movq	(%rax), %rbx
	addq	$8, %rax
	movq	%rax, -192(%rbp)
	jmp	.L92
.L373:
	movq	bio_err(%rip), %rdi
	leaq	.LC20(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	leaq	.LC21(%rip), %rdx
	leaq	.LC22(%rip), %rsi
	movq	%r14, %rdi
	call	NCONF_get_string@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L105
	leaq	.LC22(%rip), %rsi
	movq	%r14, %rdi
	movq	%r12, %r13
	call	app_RAND_load_conf@PLT
.L181:
	movq	bio_err(%rip), %rdi
	movq	%r13, %rdx
	leaq	.LC24(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	leaq	.LC25(%rip), %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	NCONF_get_string@PLT
	testq	%rax, %rax
	je	.L108
	movq	%rax, -152(%rbp)
.L109:
	movq	-152(%rbp), %r13
	movq	bio_err(%rip), %rdi
	leaq	.LC26(%rip), %rsi
	xorl	%eax, %eax
	movq	%r13, %rdx
	call	BIO_printf@PLT
	movq	%r13, %rdi
	xorl	%esi, %esi
	call	load_index@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	jne	.L180
.L212:
	movl	$1, %r12d
	jmp	.L86
.L344:
	movl	-132(%rbp), %eax
	movl	%r14d, %r12d
	movl	-168(%rbp), %r15d
	movq	-200(%rbp), %rbx
	movq	-208(%rbp), %r14
	testl	%eax, %eax
	jne	.L349
	testl	%r12d, %r12d
	jns	.L350
	movl	-160(%rbp), %edx
	testl	%edx, %edx
	jg	.L351
.L122:
	movq	$0, -208(%rbp)
.L119:
	movl	-136(%rbp), %ecx
	movl	-132(%rbp), %r11d
	movl	%r15d, -212(%rbp)
	movq	%r14, -200(%rbp)
	movq	-192(%rbp), %r14
	cmpl	$9, %ecx
	movl	%ecx, %r15d
	sete	-136(%rbp)
	testl	%r11d, %r11d
	movzbl	-136(%rbp), %esi
	movl	$0, -136(%rbp)
	setg	%al
	xorl	%r12d, %r12d
	orl	%esi, %eax
	movl	%r12d, -192(%rbp)
	movq	%rbx, %r12
	movl	%esi, %ebx
	movb	%al, -168(%rbp)
	jmp	.L156
	.p2align 4,,10
	.p2align 3
.L354:
	testb	%al, %al
	je	.L123
	movq	bio_err(%rip), %rdi
	movq	%r12, %rdx
	leaq	.LC32(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movl	$85, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	get_index
	movl	%eax, %r8d
	testl	%eax, %eax
	js	.L352
.L124:
	movl	%r8d, %esi
	movq	%r13, %rdi
	movl	%r8d, -160(%rbp)
	call	print_user.part.0
	movl	-160(%rbp), %r8d
.L126:
	cmpl	$9, %r15d
	jne	.L128
	testq	%r12, %r12
	je	.L353
	testl	%r8d, %r8d
	js	.L187
.L132:
	movq	(%r14), %r12
	addq	$8, %r14
	testq	%r12, %r12
	je	.L335
.L156:
	testq	%r12, %r12
	setne	%al
	testb	%bl, %bl
	jne	.L157
	testb	%al, %al
	je	.L335
.L157:
	cmpl	$1, -132(%rbp)
	jg	.L354
.L123:
	movl	$85, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	get_index
	movl	%eax, %r8d
	testl	%eax, %eax
	js	.L126
	cmpb	$0, -168(%rbp)
	jne	.L124
	cmpl	$6, %r15d
	je	.L185
	cmpl	$8, %r15d
	je	.L143
.L142:
	cmpl	$7, %r15d
	jne	.L132
	testl	%r8d, %r8d
	js	.L355
	movq	8(%r13), %rax
	movl	%r8d, %esi
	movq	8(%rax), %rdi
	call	OPENSSL_sk_value@PLT
	movq	bio_err(%rip), %rdi
	movq	%r12, %rdx
	leaq	.LC48(%rip), %rsi
	movq	%rax, -160(%rbp)
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	-160(%rbp), %rcx
	movl	$1, -192(%rbp)
	movq	(%rcx), %rax
	movb	$82, (%rax)
	jmp	.L132
	.p2align 4,,10
	.p2align 3
.L128:
	cmpl	$6, %r15d
	je	.L356
.L133:
	cmpl	$8, %r15d
	jne	.L142
	testl	%r8d, %r8d
	jns	.L143
	movq	bio_err(%rip), %rdi
	movq	%r12, %rdx
	leaq	.LC41(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	addl	$1, -136(%rbp)
	jmp	.L132
	.p2align 4,,10
	.p2align 3
.L352:
	cmpl	$9, %r15d
	je	.L187
	cmpl	$6, %r15d
	jne	.L133
.L134:
	movq	-208(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	$0, -72(%rbp)
	movq	-120(%rbp), %r9
	movups	%xmm0, -104(%rbp)
	testq	%rax, %rax
	je	.L204
	movq	8(%rax), %r8
	movq	16(%rax), %rcx
.L135:
	subq	$8, %rsp
	leaq	-96(%rbp), %rdx
	leaq	-104(%rbp), %rsi
	movq	%r12, %rdi
	movl	-132(%rbp), %eax
	pushq	%rax
	call	srp_create_user
	popq	%r10
	popq	%r11
	testq	%rax, %rax
	je	.L357
	movl	$442, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -160(%rbp)
	call	CRYPTO_strdup@PLT
	movl	$443, %edx
	leaq	.LC2(%rip), %rsi
	leaq	.LC37(%rip), %rdi
	movq	%rax, -88(%rbp)
	call	CRYPTO_strdup@PLT
	movq	-160(%rbp), %r8
	movl	$444, %edx
	leaq	.LC2(%rip), %rsi
	movq	%rax, -112(%rbp)
	movq	%r8, %rdi
	call	CRYPTO_strdup@PLT
	movq	-88(%rbp), %rdi
	movq	%rax, -80(%rbp)
	testq	%rdi, %rdi
	je	.L332
	testq	%rax, %rax
	je	.L332
	cmpq	$0, -112(%rbp)
	je	.L332
	cmpq	$0, -104(%rbp)
	je	.L332
	cmpq	$0, -96(%rbp)
	je	.L332
	movq	-184(%rbp), %rax
	testq	%rax, %rax
	je	.L139
	movl	$452, %edx
	leaq	.LC2(%rip), %rsi
	movq	%rax, %rdi
	call	CRYPTO_strdup@PLT
	movq	%rax, -72(%rbp)
	testq	%rax, %rax
	je	.L358
.L139:
	leaq	.LC38(%rip), %rsi
	movl	$56, %edi
	call	app_malloc@PLT
	movq	8(%r13), %rdi
	movq	%rax, %r12
	movq	-112(%rbp), %rax
	movq	$0, 48(%r12)
	movq	%r12, %rsi
	movq	%rax, (%r12)
	movq	-104(%rbp), %rax
	movq	%rax, 8(%r12)
	movq	-96(%rbp), %rax
	movq	%rax, 16(%r12)
	movq	-88(%rbp), %rax
	movq	%rax, 24(%r12)
	movq	-80(%rbp), %rax
	movq	%rax, 32(%r12)
	movq	-72(%rbp), %rax
	movq	%rax, 40(%r12)
	call	TXT_DB_insert@PLT
	testl	%eax, %eax
	je	.L140
.L210:
	movl	$1, -192(%rbp)
	jmp	.L132
.L356:
	testl	%r8d, %r8d
	js	.L134
.L185:
	movq	8(%r13), %rax
	movl	%r8d, %esi
	movq	8(%rax), %rdi
	call	OPENSSL_sk_value@PLT
	movq	bio_err(%rip), %rdi
	movq	%r12, %rdx
	leaq	.LC35(%rip), %rsi
	movq	%rax, -160(%rbp)
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	-160(%rbp), %rcx
	movl	$1, -192(%rbp)
	movq	(%rcx), %rax
	movb	$86, (%rax)
	jmp	.L132
.L143:
	movq	8(%r13), %rax
	movl	%r8d, %esi
	movl	%r8d, -160(%rbp)
	movq	8(%rax), %rdi
	call	OPENSSL_sk_value@PLT
	movl	-160(%rbp), %r8d
	movq	%rax, %r11
	movq	(%rax), %rax
	movzbl	(%rax), %eax
	cmpb	$118, %al
	je	.L359
	cmpb	$86, %al
	je	.L360
.L145:
	movl	-132(%rbp), %ecx
	testl	%ecx, %ecx
	jne	.L361
.L151:
	movq	-208(%rbp), %rax
	movq	-120(%rbp), %r9
	testq	%rax, %rax
	je	.L205
	movq	8(%rax), %r8
	movq	16(%rax), %rcx
.L152:
	subq	$8, %rsp
	leaq	16(%r11), %rdx
	leaq	8(%r11), %rsi
	movq	%r12, %rdi
	movl	-132(%rbp), %eax
	movq	%r11, -160(%rbp)
	pushq	%rax
	call	srp_create_user
	movq	-160(%rbp), %r11
	movq	%rax, %rdi
	popq	%rax
	popq	%rdx
	testq	%rdi, %rdi
	je	.L362
	movq	(%r11), %rax
	movl	$527, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r11, -160(%rbp)
	movb	$118, (%rax)
	call	CRYPTO_strdup@PLT
	movq	-160(%rbp), %r11
	cmpq	$0, 24(%r11)
	movq	%rax, 32(%r11)
	je	.L211
	testq	%rax, %rax
	je	.L211
	cmpq	$0, (%r11)
	je	.L211
	cmpq	$0, 8(%r11)
	je	.L211
	cmpq	$0, 16(%r11)
	je	.L211
	movq	-184(%rbp), %rax
	testq	%rax, %rax
	je	.L210
	movl	$535, %edx
	leaq	.LC2(%rip), %rsi
	movq	%rax, %rdi
	call	CRYPTO_strdup@PLT
	movq	-160(%rbp), %r11
	movq	%rax, 40(%r11)
	testq	%rax, %rax
	jne	.L210
.L211:
	movq	-200(%rbp), %r14
.L336:
	movl	$1, %r12d
.L141:
	movl	-136(%rbp), %r8d
	testl	%r8d, %r8d
	je	.L88
	movl	-132(%rbp), %edi
	testl	%edi, %edi
	je	.L169
.L174:
	movl	-136(%rbp), %edx
	movq	bio_err(%rip), %rdi
	leaq	.LC54(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L86
.L187:
	movq	bio_err(%rip), %rdi
	movq	%r12, %rdx
	leaq	.LC34(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	addl	$1, -136(%rbp)
	jmp	.L132
.L338:
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	leaq	.LC14(%rip), %rsi
	call	BIO_printf@PLT
	jmp	.L85
.L355:
	movq	bio_err(%rip), %rdi
	movq	%r12, %rdx
	leaq	.LC47(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	addl	$1, -136(%rbp)
	jmp	.L132
.L335:
	movl	-132(%rbp), %r10d
	movl	-212(%rbp), %r15d
	movq	-200(%rbp), %r14
	movl	-192(%rbp), %r12d
	testl	%r10d, %r10d
	jne	.L363
	testl	%r12d, %r12d
	jne	.L364
.L164:
	movl	-136(%rbp), %r9d
	xorl	%r12d, %r12d
	testl	%r9d, %r9d
	setne	%r12b
	jmp	.L141
.L204:
	movq	-176(%rbp), %rcx
	xorl	%r8d, %r8d
	jmp	.L135
.L341:
	movq	bio_err(%rip), %rdi
	leaq	.LC17(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L83
.L343:
	movq	-168(%rbp), %rax
	movl	-132(%rbp), %edx
	testq	%rax, %rax
	cmove	default_config_file(%rip), %rax
	movq	%rax, -168(%rbp)
	testl	%edx, %edx
	jne	.L365
	movq	%rax, %rdi
	call	app_load_config@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L366
.L182:
	movq	-168(%rbp), %rax
	cmpq	%rax, default_config_file(%rip)
	je	.L102
	movq	%r14, %rdi
	call	app_load_modules@PLT
	testl	%eax, %eax
	je	.L367
.L102:
	testq	%r13, %r13
	je	.L368
	leaq	.LC22(%rip), %rsi
	movq	%r14, %rdi
	call	app_RAND_load_conf@PLT
	movl	-132(%rbp), %eax
	testl	%eax, %eax
	jne	.L181
.L107:
	leaq	.LC25(%rip), %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	NCONF_get_string@PLT
	movq	%rax, -152(%rbp)
	testq	%rax, %rax
	jne	.L110
.L108:
	movq	bio_err(%rip), %rdi
	movq	%r13, %rdx
	xorl	%eax, %eax
	xorl	%r13d, %r13d
	leaq	.LC25(%rip), %rcx
	leaq	.LC23(%rip), %rsi
	call	BIO_printf@PLT
	jmp	.L85
.L342:
	movq	bio_err(%rip), %rdi
	leaq	.LC18(%rip), %rsi
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	call	BIO_printf@PLT
	jmp	.L85
.L353:
	movq	bio_err(%rip), %rdi
	leaq	.LC33(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L131:
	movl	%r12d, %esi
	movq	%r13, %rdi
	addl	$1, %r12d
	call	print_user.part.0
.L130:
	movq	8(%r13), %rax
	movq	8(%rax), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r12d
	jl	.L131
	jmp	.L132
.L360:
	movl	-132(%rbp), %r9d
	testl	%r9d, %r9d
	jne	.L369
.L146:
	movq	32(%r11), %rsi
	movl	$73, %edx
	movq	%r13, %rdi
	movl	%r8d, -192(%rbp)
	movq	%r11, -160(%rbp)
	call	get_index
	movq	-160(%rbp), %r11
	movl	-192(%rbp), %r8d
	testl	%eax, %eax
	jns	.L147
	movq	-128(%rbp), %r9
.L148:
	movq	32(%r11), %rcx
	xorl	%r8d, %r8d
.L149:
	subq	$8, %rsp
	movq	8(%r11), %rsi
	movq	16(%r11), %rdx
	movq	%r12, %rdi
	movl	-132(%rbp), %eax
	movq	%r11, -160(%rbp)
	pushq	%rax
	call	srp_verify_user
	popq	%rsi
	movq	-160(%rbp), %r11
	testq	%rax, %rax
	popq	%rdi
	jne	.L145
	movq	bio_err(%rip), %rdi
	movq	%r12, %rdx
	leaq	.LC44(%rip), %rsi
	movq	-200(%rbp), %r14
	call	BIO_printf@PLT
	addl	$1, -136(%rbp)
.L150:
	cmpl	$0, -132(%rbp)
	je	.L321
	movl	$1, %r12d
	jmp	.L174
	.p2align 4,,10
	.p2align 3
.L339:
	movq	bio_err(%rip), %rdi
	leaq	.LC15(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L83
.L361:
	movq	bio_err(%rip), %rdi
	movq	%r12, %rdx
	leaq	.LC45(%rip), %rsi
	xorl	%eax, %eax
	movq	%r11, -160(%rbp)
	call	BIO_printf@PLT
	movq	-160(%rbp), %r11
	jmp	.L151
.L140:
	movq	bio_err(%rip), %rdi
	leaq	.LC39(%rip), %rsi
	xorl	%eax, %eax
	movq	-200(%rbp), %r14
	call	BIO_printf@PLT
	movq	8(%r13), %rax
	movq	bio_err(%rip), %rdi
	leaq	.LC40(%rip), %rsi
	movq	32(%rax), %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	%r12, %rdi
	movl	$104, %edx
	leaq	.LC2(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-88(%rbp), %rdi
.L138:
	movl	$454, %edx
	leaq	.LC2(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-80(%rbp), %rdi
	movl	$455, %edx
	leaq	.LC2(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-72(%rbp), %rdi
	movl	$456, %edx
	leaq	.LC2(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-112(%rbp), %rdi
	movl	$457, %edx
	leaq	.LC2(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-104(%rbp), %rdi
	movl	$458, %edx
	leaq	.LC2(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-96(%rbp), %rdi
	movl	$459, %edx
	leaq	.LC2(%rip), %rsi
	call	CRYPTO_free@PLT
	jmp	.L336
.L349:
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
	leaq	.LC27(%rip), %rsi
	call	BIO_printf@PLT
	testl	%r12d, %r12d
	jns	.L370
	movl	-160(%rbp), %eax
	testl	%eax, %eax
	jg	.L371
.L191:
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
	leaq	.LC30(%rip), %rsi
	call	BIO_printf@PLT
	cmpl	$1, -132(%rbp)
	jle	.L122
	movq	$0, -208(%rbp)
.L121:
	movq	bio_err(%rip), %rdi
	leaq	.LC31(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L119
.L205:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	jmp	.L152
.L350:
	movq	8(%r13), %rax
	movl	%r12d, %esi
	movq	8(%rax), %rdi
	call	OPENSSL_sk_value@PLT
	movq	%rax, -208(%rbp)
	jmp	.L119
.L345:
	movq	bio_err(%rip), %rdi
	movq	%r12, %rdx
	leaq	.LC13(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L83
.L359:
	movq	bio_err(%rip), %rdi
	movq	%r12, %rdx
	leaq	.LC42(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	addl	$1, -136(%rbp)
	jmp	.L132
.L332:
	movq	-200(%rbp), %r14
	jmp	.L138
.L363:
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
	leaq	.LC49(%rip), %rsi
	call	BIO_printf@PLT
	testl	%r12d, %r12d
	jne	.L159
	jmp	.L164
	.p2align 4,,10
	.p2align 3
.L166:
	movq	8(%r13), %rax
	movl	%r15d, %esi
	movq	8(%rax), %rdi
	call	OPENSSL_sk_value@PLT
	movq	(%rax), %rax
	cmpb	$118, (%rax)
	jne	.L165
	movb	$86, (%rax)
	movl	%r15d, %esi
	movq	%r13, %rdi
	call	print_user.part.0
.L165:
	addl	$1, %r15d
.L159:
	movq	8(%r13), %rax
	movq	8(%rax), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r15d
	jl	.L166
	movq	bio_err(%rip), %rdi
	leaq	.LC56(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	-152(%rbp), %rdi
	movq	%r13, %rdx
	leaq	.LC52(%rip), %rsi
	call	save_index@PLT
	testl	%eax, %eax
	je	.L168
	movq	bio_err(%rip), %rdi
	leaq	.LC50(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	-152(%rbp), %rdi
	leaq	.LC51(%rip), %rdx
	leaq	.LC52(%rip), %rsi
	call	rotate_index@PLT
	testl	%eax, %eax
	je	.L168
	movq	bio_err(%rip), %rdi
	leaq	.LC53(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L164
.L364:
	xorl	%ebx, %ebx
	jmp	.L163
.L372:
	movq	8(%r13), %rax
	movl	%ebx, %esi
	movq	8(%rax), %rdi
	call	OPENSSL_sk_value@PLT
	movq	(%rax), %rax
	cmpb	$118, (%rax)
	jne	.L162
	movb	$86, (%rax)
.L162:
	addl	$1, %ebx
.L163:
	movq	8(%r13), %rax
	movq	8(%rax), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%ebx, %eax
	jg	.L372
	movq	-152(%rbp), %rdi
	movq	%r13, %rdx
	leaq	.LC52(%rip), %rsi
	call	save_index@PLT
	testl	%eax, %eax
	je	.L321
	movq	-152(%rbp), %rdi
	leaq	.LC51(%rip), %rdx
	leaq	.LC52(%rip), %rsi
	call	rotate_index@PLT
	testl	%eax, %eax
	jne	.L164
	jmp	.L321
.L351:
	movq	-176(%rbp), %rdi
	call	SRP_get_default_gN@PLT
	testq	%rax, %rax
	jne	.L122
.L192:
	movq	-176(%rbp), %rdx
	movq	bio_err(%rip), %rdi
	leaq	.LC29(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L85
.L365:
	movq	bio_err(%rip), %rdi
	movq	%rax, %r14
	movq	%rax, %rdx
	xorl	%eax, %eax
	leaq	.LC19(%rip), %rsi
	call	BIO_printf@PLT
	movq	%r14, %rdi
	call	app_load_config@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	jne	.L182
	xorl	%r13d, %r13d
	movl	$1, %r12d
	jmp	.L86
.L147:
	movq	8(%r13), %rax
	movl	%r8d, %esi
	movq	%r11, -160(%rbp)
	movq	8(%rax), %rdi
	call	OPENSSL_sk_value@PLT
	movq	-128(%rbp), %r9
	movq	-160(%rbp), %r11
	testq	%rax, %rax
	je	.L148
	movq	8(%rax), %r8
	movq	16(%rax), %rcx
	jmp	.L149
.L368:
	movl	-132(%rbp), %eax
	testl	%eax, %eax
	jne	.L373
	leaq	.LC21(%rip), %rdx
	leaq	.LC22(%rip), %rsi
	movq	%r14, %rdi
	call	NCONF_get_string@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L105
	leaq	.LC22(%rip), %rsi
	movq	%r14, %rdi
	movq	%r12, %r13
	call	app_RAND_load_conf@PLT
	jmp	.L107
.L370:
	movq	8(%r13), %rax
	movl	%r12d, %esi
	movq	8(%rax), %rdi
	call	OPENSSL_sk_value@PLT
	cmpl	$1, -132(%rbp)
	movq	%rax, -208(%rbp)
	je	.L119
	movq	8(%r13), %rax
	movl	%r12d, %esi
	xorl	%r12d, %r12d
	movq	8(%rax), %rdi
	call	OPENSSL_sk_value@PLT
	movq	bio_err(%rip), %rdi
	leaq	.LC28(%rip), %rdx
	leaq	.LC9(%rip), %rsi
	movq	24(%rax), %rcx
	movq	%rax, -168(%rbp)
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	-168(%rbp), %r8
	movq	%rbx, -160(%rbp)
	movq	%r8, %rbx
.L120:
	movq	(%rbx,%r12,8), %rcx
	movq	bio_err(%rip), %rdi
	movl	%r12d, %edx
	xorl	%eax, %eax
	leaq	.LC10(%rip), %rsi
	addq	$1, %r12
	call	BIO_printf@PLT
	cmpq	$6, %r12
	jne	.L120
	movq	-160(%rbp), %rbx
	jmp	.L121
.L369:
	movq	bio_err(%rip), %rdi
	movq	%r12, %rdx
	leaq	.LC43(%rip), %rsi
	xorl	%eax, %eax
	movq	%r11, -192(%rbp)
	movl	%r8d, -160(%rbp)
	call	BIO_printf@PLT
	movq	-192(%rbp), %r11
	movl	-160(%rbp), %r8d
	jmp	.L146
.L371:
	movq	-176(%rbp), %rdi
	call	SRP_get_default_gN@PLT
	testq	%rax, %rax
	jne	.L191
	jmp	.L192
.L367:
	xorl	%r13d, %r13d
	jmp	.L85
.L366:
	xorl	%r13d, %r13d
	jmp	.L321
.L168:
	movl	-136(%rbp), %ecx
	testl	%ecx, %ecx
	jne	.L174
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L357:
	movq	bio_err(%rip), %rdi
	movq	%r12, %rdx
	leaq	.LC36(%rip), %rsi
	xorl	%eax, %eax
	movq	-200(%rbp), %r14
	call	BIO_printf@PLT
	addl	$1, -136(%rbp)
	jmp	.L336
.L105:
	movq	bio_err(%rip), %rdi
	leaq	.LC21(%rip), %rcx
	leaq	.LC22(%rip), %rdx
	xorl	%eax, %eax
	leaq	.LC23(%rip), %rsi
	call	BIO_printf@PLT
	jmp	.L85
.L347:
	call	__stack_chk_fail@PLT
.L362:
	movq	bio_err(%rip), %rdi
	movq	%r12, %rdx
	leaq	.LC46(%rip), %rsi
	xorl	%eax, %eax
	movq	-200(%rbp), %r14
	call	BIO_printf@PLT
	addl	$1, -136(%rbp)
	jmp	.L150
.L358:
	movq	-200(%rbp), %r14
	movq	-88(%rbp), %rdi
	jmp	.L138
	.cfi_endproc
.LFE1515:
	.size	srp_main, .-srp_main
	.globl	srp_options
	.section	.rodata.str1.1
.LC57:
	.string	"help"
.LC58:
	.string	"Display this summary"
.LC59:
	.string	"verbose"
.LC60:
	.string	"Talk a lot while doing things"
.LC61:
	.string	"config"
.LC62:
	.string	"A config file"
.LC63:
	.string	"name"
	.section	.rodata.str1.8
	.align 8
.LC64:
	.string	"The particular srp definition to use"
	.section	.rodata.str1.1
.LC65:
	.string	"The srp verifier file name"
.LC66:
	.string	"add"
.LC67:
	.string	"Add a user and srp verifier"
.LC68:
	.string	"modify"
	.section	.rodata.str1.8
	.align 8
.LC69:
	.string	"Modify the srp verifier of an existing user"
	.section	.rodata.str1.1
.LC70:
	.string	"delete"
	.section	.rodata.str1.8
	.align 8
.LC71:
	.string	"Delete user from verifier file"
	.section	.rodata.str1.1
.LC72:
	.string	"list"
.LC73:
	.string	"List users"
.LC74:
	.string	"gn"
	.section	.rodata.str1.8
	.align 8
.LC75:
	.string	"Set g and N values to be used for new verifier"
	.section	.rodata.str1.1
.LC76:
	.string	"userinfo"
	.section	.rodata.str1.8
	.align 8
.LC77:
	.string	"Additional info to be set for user"
	.section	.rodata.str1.1
.LC78:
	.string	"passin"
.LC79:
	.string	"Input file pass phrase source"
.LC80:
	.string	"passout"
	.section	.rodata.str1.8
	.align 8
.LC81:
	.string	"Output file pass phrase source"
	.section	.rodata.str1.1
.LC82:
	.string	"rand"
	.section	.rodata.str1.8
	.align 8
.LC83:
	.string	"Load the file(s) into the random number generator"
	.section	.rodata.str1.1
.LC84:
	.string	"writerand"
	.section	.rodata.str1.8
	.align 8
.LC85:
	.string	"Write random data to the specified file"
	.section	.rodata.str1.1
.LC86:
	.string	"engine"
	.section	.rodata.str1.8
	.align 8
.LC87:
	.string	"Use engine, possibly a hardware device"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	srp_options, @object
	.size	srp_options, 408
srp_options:
	.quad	.LC57
	.long	1
	.long	45
	.quad	.LC58
	.quad	.LC59
	.long	2
	.long	45
	.quad	.LC60
	.quad	.LC61
	.long	3
	.long	60
	.quad	.LC62
	.quad	.LC63
	.long	4
	.long	115
	.quad	.LC64
	.quad	.LC25
	.long	5
	.long	60
	.quad	.LC65
	.quad	.LC66
	.long	6
	.long	45
	.quad	.LC67
	.quad	.LC68
	.long	8
	.long	45
	.quad	.LC69
	.quad	.LC70
	.long	7
	.long	45
	.quad	.LC71
	.quad	.LC72
	.long	9
	.long	45
	.quad	.LC73
	.quad	.LC74
	.long	10
	.long	115
	.quad	.LC75
	.quad	.LC76
	.long	11
	.long	115
	.quad	.LC77
	.quad	.LC78
	.long	12
	.long	115
	.quad	.LC79
	.quad	.LC80
	.long	13
	.long	115
	.quad	.LC81
	.quad	.LC82
	.long	1501
	.long	115
	.quad	.LC83
	.quad	.LC84
	.long	1502
	.long	62
	.quad	.LC85
	.quad	.LC86
	.long	14
	.long	115
	.quad	.LC87
	.quad	0
	.zero	16
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
