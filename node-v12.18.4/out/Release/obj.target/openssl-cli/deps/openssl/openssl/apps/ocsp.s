	.file	"ocsp.c"
	.text
	.p2align 4
	.type	noteterm, @function
noteterm:
.LFB1567:
	.cfi_startproc
	endbr64
	movl	%edi, termsig(%rip)
	ret
	.cfi_endproc
.LFE1567:
	.size	noteterm, .-noteterm
	.p2align 4
	.type	socket_timeout, @function
socket_timeout:
.LFB1576:
	.cfi_startproc
	endbr64
	movl	acfd(%rip), %edi
	cmpl	$-1, %edi
	jne	.L5
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	xorl	%esi, %esi
	jmp	shutdown@PLT
	.cfi_endproc
.LFE1576:
	.size	socket_timeout, .-socket_timeout
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"%.*s"
	.text
	.p2align 4
	.type	print_syslog, @function
print_syslog:
.LFB1564:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r9
	movq	%rdi, %r8
	leaq	.LC0(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movl	$1000, %ebx
	subq	$8, %rsp
	cmpq	$1000, %rsi
	movl	(%r9), %edi
	cmovbe	%rsi, %rbx
	xorl	%eax, %eax
	movl	$1, %esi
	movl	%ebx, %ecx
	call	__syslog_chk@PLT
	addq	$8, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1564:
	.size	print_syslog, .-print_syslog
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"../deps/openssl/openssl/apps/ocsp.c"
	.text
	.p2align 4
	.type	killall, @function
killall:
.LFB1566:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	multi(%rip), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	%edi, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	testl	%eax, %eax
	jle	.L9
	subl	$1, %eax
	movq	%rsi, %rbx
	leaq	(%rsi,%rax,4), %r14
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L10:
	leaq	4(%rbx), %rax
	cmpq	%r14, %rbx
	je	.L9
.L12:
	movq	%rax, %rbx
.L11:
	movl	(%rbx), %edi
	testl	%edi, %edi
	je	.L10
	movl	$15, %esi
	call	kill@PLT
	leaq	4(%rbx), %rax
	cmpq	%r14, %rbx
	jne	.L12
.L9:
	movl	$893, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
	movl	$1, %edi
	call	sleep@PLT
	movl	%r13d, %edi
	call	exit@PLT
	.cfi_endproc
.LFE1566:
	.size	killall, .-killall
	.section	.rodata.str1.1
.LC2:
	.string	"%s: "
.LC3:
	.string	"\n"
.LC4:
	.string	"%s"
	.text
	.p2align 4
	.type	log_message, @function
log_message:
.LFB1563:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	subq	$1272, %rsp
	movl	%edi, -1284(%rbp)
	movq	%rdx, -192(%rbp)
	movq	%rcx, -184(%rbp)
	movq	%r8, -176(%rbp)
	movq	%r9, -168(%rbp)
	testb	%al, %al
	je	.L18
	movaps	%xmm0, -160(%rbp)
	movaps	%xmm1, -144(%rbp)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm3, -112(%rbp)
	movaps	%xmm4, -96(%rbp)
	movaps	%xmm5, -80(%rbp)
	movaps	%xmm6, -64(%rbp)
	movaps	%xmm7, -48(%rbp)
.L18:
	movq	%fs:40, %rax
	movq	%rax, -216(%rbp)
	xorl	%eax, %eax
	movl	multi(%rip), %edx
	leaq	16(%rbp), %rax
	movl	$16, -1272(%rbp)
	movq	%rax, -1264(%rbp)
	leaq	-208(%rbp), %rax
	leaq	-1272(%rbp), %r13
	movl	$48, -1268(%rbp)
	movq	%rax, -1256(%rbp)
	testl	%edx, %edx
	jne	.L19
.L23:
	movq	prog(%rip), %rdx
	movq	bio_err(%rip), %rdi
	leaq	.LC2(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	movq	%r12, %rsi
	movq	%r13, %rdx
	call	BIO_vprintf@PLT
	movq	bio_err(%rip), %rdi
	leaq	.LC3(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L17:
	movq	-216(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L26
	addq	$1272, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	.cfi_restore_state
	movq	%r13, %r9
	movq	%r12, %r8
	movl	$1024, %ecx
	movl	$1, %edx
	leaq	-1248(%rbp), %r14
	movl	$1024, %esi
	movq	%r14, %rdi
	call	__vsnprintf_chk@PLT
	testl	%eax, %eax
	jg	.L27
	cmpl	$2, -1284(%rbp)
	jg	.L28
.L22:
	movl	multi(%rip), %eax
	testl	%eax, %eax
	jne	.L17
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L27:
	movl	-1284(%rbp), %edi
	xorl	%eax, %eax
	movq	%r14, %rcx
	movl	$1, %esi
	leaq	.LC4(%rip), %rdx
	call	__syslog_chk@PLT
	cmpl	$2, -1284(%rbp)
	jle	.L22
.L28:
	leaq	-1284(%rbp), %rsi
	leaq	print_syslog(%rip), %rdi
	call	ERR_print_errors_cb@PLT
	jmp	.L22
.L26:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1563:
	.size	log_message, .-log_message
	.section	.rodata.str1.1
.LC5:
	.string	"GET "
	.section	.rodata.str1.8
	.align 8
.LC6:
	.string	"Invalid request -- bad URL: %s"
	.section	.rodata.str1.1
.LC7:
	.string	" HTTP/1."
	.section	.rodata.str1.8
	.align 8
.LC8:
	.string	"Invalid request -- bad HTTP version: %s"
	.align 8
.LC9:
	.string	"Invalid request -- bad URL encoding: %s"
	.align 8
.LC10:
	.string	"Could not allocate base64 bio: %s"
	.section	.rodata.str1.1
.LC11:
	.string	"POST "
	.section	.rodata.str1.8
	.align 8
.LC12:
	.string	"Invalid request -- bad HTTP verb: %s"
	.section	.rodata.str1.1
.LC13:
	.string	"Error parsing OCSP request"
	.text
	.p2align 4
	.type	do_responder.constprop.0, @function
do_responder.constprop.0:
.LFB1582:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, (%rdi)
	movq	%rdx, %r12
	movq	%rdi, %r13
	movq	%rsi, %r14
	movl	%ecx, %ebx
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movl	$101, %esi
	movq	%r12, %rdi
	call	BIO_ctrl@PLT
	movq	%rax, %r8
	xorl	%eax, %eax
	testq	%r8, %r8
	jle	.L29
	movq	%r12, %rdi
	call	BIO_pop@PLT
	movl	$2, %edx
	movl	$124, %esi
	movq	%rax, (%r14)
	movq	%rax, %rdi
	movq	%rax, %r12
	call	BIO_ptr_ctrl@PLT
	movq	%rax, -4168(%rbp)
	testl	%ebx, %ebx
	jg	.L87
	leaq	-2112(%rbp), %rsi
	movl	$2048, %edx
	movq	%r12, %rdi
	call	BIO_gets@PLT
	testl	%eax, %eax
	jle	.L33
	cmpl	$542393671, -2112(%rbp)
	je	.L88
.L34:
	cmpl	$1414745936, -2112(%rbp)
	je	.L89
.L53:
	movq	-4168(%rbp), %rdx
	leaq	.LC12(%rip), %rsi
	movl	$6, %edi
	xorl	%eax, %eax
	call	log_message
.L40:
	testl	%ebx, %ebx
	jle	.L33
.L60:
	xorl	%edi, %edi
	call	alarm@PLT
.L33:
	movl	$-1, acfd(%rip)
	movl	$1, %eax
.L29:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L90
	addq	$4152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L87:
	.cfi_restore_state
	xorl	%edx, %edx
	movl	$105, %esi
	leaq	acfd(%rip), %rcx
	movq	%r12, %rdi
	call	BIO_ctrl@PLT
	movl	%ebx, %edi
	call	alarm@PLT
	leaq	-2112(%rbp), %rsi
	movl	$2048, %edx
	movq	%r12, %rdi
	call	BIO_gets@PLT
	testl	%eax, %eax
	jle	.L60
	cmpl	$542393671, -2112(%rbp)
	jne	.L34
.L88:
	movzbl	-2108(%rbp), %eax
	leaq	-2108(%rbp), %rdx
	cmpb	$32, %al
	je	.L38
.L37:
	cmpb	$47, %al
	jne	.L91
	leaq	1(%rdx), %rax
	movq	%rax, -4184(%rbp)
	testb	$-33, 1(%rdx)
	je	.L41
	.p2align 4,,10
	.p2align 3
.L42:
	addq	$1, %rax
	testb	$-33, (%rax)
	jne	.L42
.L41:
	movl	$8, %ecx
	leaq	.LC7(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%cl
	sbbb	$0, %cl
	testb	%cl, %cl
	jne	.L92
	movb	$0, (%rax)
	movzbl	1(%rdx), %eax
	testb	%al, %al
	je	.L40
	movq	-4184(%rbp), %r15
	addq	$2, %rdx
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L94:
	movb	%al, -1(%rdx)
	movq	%rdx, %rsi
	movq	%r15, %rax
.L46:
	leaq	1(%rax), %r15
	movzbl	1(%rax), %eax
	addq	$1, %rdx
	testb	%al, %al
	je	.L93
.L49:
	cmpb	$37, %al
	jne	.L94
	movq	%rdx, -4176(%rbp)
	call	__ctype_b_loc@PLT
	movzbl	1(%r15), %esi
	movq	-4176(%rbp), %rdx
	movq	(%rax), %rax
	movq	%rsi, %rdi
	testb	$16, 1(%rax,%rsi,2)
	je	.L48
	movzbl	2(%r15), %esi
	movq	%rdx, -4176(%rbp)
	testb	$16, 1(%rax,%rsi,2)
	je	.L48
	call	OPENSSL_hexchar2int@PLT
	movzbl	2(%r15), %edi
	movl	%eax, %r14d
	call	OPENSSL_hexchar2int@PLT
	sall	$4, %r14d
	movq	-4176(%rbp), %rdx
	orl	%eax, %r14d
	leaq	2(%r15), %rax
	movb	%r14b, -1(%rdx)
	movq	%rdx, %rsi
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L89:
	xorl	%r15d, %r15d
	cmpb	$32, -2108(%rbp)
	jne	.L53
.L52:
	leaq	-4160(%rbp), %r14
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L95:
	cmpb	$10, %al
	je	.L67
.L55:
	movl	$2048, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	BIO_gets@PLT
	testl	%eax, %eax
	jle	.L40
	movzbl	-4160(%rbp), %eax
	cmpb	$13, %al
	jne	.L95
.L67:
	xorl	%edi, %edi
	call	alarm@PLT
	xorl	%ecx, %ecx
	testq	%r15, %r15
	je	.L57
	movq	OCSP_REQUEST_new@GOTPCREL(%rip), %rdi
	movq	d2i_OCSP_REQUEST@GOTPCREL(%rip), %rsi
	movq	%r15, %rdx
	call	ASN1_d2i_bio@PLT
	movq	%r15, %rdi
	movq	%rax, %rbx
	call	BIO_free_all@PLT
.L58:
	testq	%rbx, %rbx
	je	.L96
.L59:
	movq	%rbx, 0(%r13)
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L38:
	movzbl	1(%rdx), %eax
	addq	$1, %rdx
	cmpb	$32, %al
	jne	.L37
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L91:
	movq	-4168(%rbp), %rdx
	leaq	.LC6(%rip), %rsi
	movl	$6, %edi
	xorl	%eax, %eax
	call	log_message
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L93:
	movq	-4184(%rbp), %rdi
	movb	$0, (%rsi)
	subq	%rdi, %rsi
	testl	%esi, %esi
	jle	.L48
	call	BIO_new_mem_buf@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L51
	call	BIO_f_base64@PLT
	movq	%rax, %rdi
	call	BIO_new@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L51
	movl	$256, %esi
	movq	%rax, -4168(%rbp)
	call	BIO_set_flags@PLT
	movq	-4168(%rbp), %rdi
	movq	%r14, %rsi
	call	BIO_push@PLT
	movq	%rax, %r15
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L92:
	movq	-4168(%rbp), %rdx
	leaq	.LC8(%rip), %rsi
	movl	$6, %edi
	xorl	%eax, %eax
	call	log_message
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L57:
	movq	d2i_OCSP_REQUEST@GOTPCREL(%rip), %rsi
	movq	OCSP_REQUEST_new@GOTPCREL(%rip), %rdi
	movq	%r12, %rdx
	call	ASN1_d2i_bio@PLT
	movq	%rax, %rbx
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L48:
	movq	-4168(%rbp), %rdx
	leaq	.LC9(%rip), %rsi
	movl	$6, %edi
	xorl	%eax, %eax
	call	log_message
	jmp	.L40
.L51:
	movq	-4168(%rbp), %rdx
	leaq	.LC10(%rip), %rsi
	movl	$3, %edi
	xorl	%eax, %eax
	call	log_message
	jmp	.L40
.L96:
	leaq	.LC13(%rip), %rsi
	movl	$3, %edi
	xorl	%eax, %eax
	call	log_message
	jmp	.L59
.L90:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1582:
	.size	do_responder.constprop.0, .-do_responder.constprop.0
	.section	.rodata.str1.1
.LC14:
	.string	"Error creating connect BIO\n"
.LC15:
	.string	"Error creating SSL context.\n"
.LC16:
	.string	"Error connecting BIO\n"
.LC17:
	.string	"Can't get connection fd\n"
.LC18:
	.string	"Timeout on connect\n"
.LC19:
	.string	"host"
.LC20:
	.string	"Host"
.LC21:
	.string	"Unexpected retry condition\n"
.LC22:
	.string	"Timeout on request\n"
.LC23:
	.string	"Select error\n"
	.section	.rodata.str1.8
	.align 8
.LC24:
	.string	"Error querying OCSP responder\n"
	.text
	.p2align 4
	.globl	process_responder
	.type	process_responder, @function
process_responder:
.LFB1580:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%r8d, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$232, %rsp
	movq	%rdi, -256(%rbp)
	movq	%rsi, %rdi
	movq	%rsi, -264(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	BIO_new_connect@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L157
	testq	%r15, %r15
	je	.L100
	movq	%r15, %rcx
	movl	$1, %edx
	movl	$100, %esi
	movq	%rax, %rdi
	call	BIO_ctrl@PLT
.L100:
	xorl	%r15d, %r15d
	cmpl	$1, %r14d
	je	.L158
.L101:
	movq	$0, -216(%rbp)
	xorl	%ecx, %ecx
	cmpl	$-1, 16(%rbp)
	jne	.L159
	xorl	%edx, %edx
	movl	$101, %esi
	movq	%r12, %rdi
	call	BIO_ctrl@PLT
	testl	%eax, %eax
	jle	.L126
.L104:
	xorl	%edx, %edx
	leaq	-220(%rbp), %rcx
	movl	$105, %esi
	movq	%r12, %rdi
	call	BIO_ctrl@PLT
	testq	%rax, %rax
	js	.L124
.L107:
	movq	%r13, %rsi
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	OCSP_sendreq_new@PLT
	xorl	%r14d, %r14d
	movl	$1, -228(%rbp)
	movq	%rax, %r13
	testq	%rax, %rax
	jne	.L109
	jmp	.L106
	.p2align 4,,10
	.p2align 3
.L161:
	movq	16(%rax), %r8
	movq	8(%rax), %rsi
	movq	%r13, %rdi
	movq	%r8, %rdx
	call	OCSP_REQ_CTX_add1_header@PLT
	testl	%eax, %eax
	je	.L108
.L111:
	addl	$1, %r14d
.L109:
	movq	%rbx, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r14d
	jge	.L160
	movl	%r14d, %esi
	movq	%rbx, %rdi
	call	OPENSSL_sk_value@PLT
	cmpl	$1, -228(%rbp)
	jne	.L161
	movq	8(%rax), %rsi
	leaq	.LC19(%rip), %rdi
	movq	%rax, -248(%rbp)
	movq	%rsi, -240(%rbp)
	call	strcasecmp@PLT
	movq	-248(%rbp), %rdx
	movq	-240(%rbp), %rsi
	movq	%r13, %rdi
	testl	%eax, %eax
	setne	%al
	movq	16(%rdx), %rdx
	movzbl	%al, %eax
	movl	%eax, -228(%rbp)
	call	OCSP_REQ_CTX_add1_header@PLT
	testl	%eax, %eax
	jne	.L111
	.p2align 4,,10
	.p2align 3
.L108:
	movq	%r13, %rdi
	call	OCSP_REQ_CTX_free@PLT
	movq	-216(%rbp), %r13
	testq	%r13, %r13
	je	.L106
.L99:
	movq	%r12, %rdi
	call	BIO_free_all@PLT
	movq	%r15, %rdi
	call	SSL_CTX_free@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L162
	addq	$232, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L159:
	.cfi_restore_state
	movl	$1, %edx
	movl	$102, %esi
	movq	%r12, %rdi
	call	BIO_ctrl@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$101, %esi
	movq	%r12, %rdi
	call	BIO_ctrl@PLT
	testl	%eax, %eax
	jg	.L104
	movl	$8, %esi
	movq	%r12, %rdi
	call	BIO_test_flags@PLT
	testl	%eax, %eax
	je	.L126
	xorl	%edx, %edx
	leaq	-220(%rbp), %rcx
	movl	$105, %esi
	movq	%r12, %rdi
	call	BIO_ctrl@PLT
	testq	%rax, %rax
	js	.L124
	leaq	-192(%rbp), %r14
	xorl	%eax, %eax
	movl	$16, %ecx
	movq	%r14, %rdi
#APP
# 1523 "../deps/openssl/openssl/apps/ocsp.c" 1
	cld; rep; stosq
# 0 "" 2
#NO_APP
	movslq	-220(%rbp), %rdi
	call	__fdelt_chk@PLT
	movl	-220(%rbp), %edi
	xorl	%esi, %esi
	movq	%r14, %rdx
	movq	$0, -200(%rbp)
	movq	%rax, %r8
	movl	%edi, %eax
	sarl	$31, %eax
	shrl	$26, %eax
	leal	(%rdi,%rax), %ecx
	addl	$1, %edi
	andl	$63, %ecx
	subl	%eax, %ecx
	movl	$1, %eax
	salq	%cl, %rax
	orq	%rax, -192(%rbp,%r8,8)
	movslq	16(%rbp), %rax
	xorl	%ecx, %ecx
	leaq	-208(%rbp), %r8
	movq	%rax, -208(%rbp)
	call	select@PLT
	testl	%eax, %eax
	jne	.L107
	movq	bio_err(%rip), %rdi
	leaq	.LC18(%rip), %rsi
	call	BIO_puts@PLT
	jmp	.L106
	.p2align 4,,10
	.p2align 3
.L158:
	call	TLS_client_method@PLT
	movq	%rax, %rdi
	call	SSL_CTX_new@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L163
	xorl	%ecx, %ecx
	movl	$4, %edx
	movl	$33, %esi
	movq	%rax, %rdi
	call	SSL_CTX_ctrl@PLT
	movl	$1, %esi
	movq	%r15, %rdi
	call	BIO_new_ssl@PLT
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	BIO_push@PLT
	movq	%rax, %r12
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L160:
	cmpl	$1, -228(%rbp)
	je	.L113
.L115:
	movq	-256(%rbp), %rsi
	movq	%r13, %rdi
	leaq	-216(%rbp), %rbx
	call	OCSP_REQ_CTX_set1_req@PLT
	testl	%eax, %eax
	je	.L108
	cmpl	$-1, 16(%rbp)
	jne	.L164
	.p2align 4,,10
	.p2align 3
.L116:
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	OCSP_sendreq_nbio@PLT
	cmpl	$-1, %eax
	jne	.L108
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	OCSP_sendreq_nbio@PLT
	cmpl	$-1, %eax
	je	.L116
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L167:
	movl	-220(%rbp), %eax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	leaq	-208(%rbp), %r8
	leal	1(%rax), %edi
	call	select@PLT
.L120:
	testl	%eax, %eax
	je	.L165
	cmpl	$-1, %eax
	je	.L166
	cmpl	$-1, 16(%rbp)
	je	.L116
.L164:
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	OCSP_sendreq_nbio@PLT
	cmpl	$-1, %eax
	jne	.L108
	leaq	-192(%rbp), %r14
	movl	$16, %ecx
	xorl	%eax, %eax
	movq	%r14, %rdi
#APP
# 1558 "../deps/openssl/openssl/apps/ocsp.c" 1
	cld; rep; stosq
# 0 "" 2
#NO_APP
	movslq	-220(%rbp), %rdi
	call	__fdelt_chk@PLT
	movl	-220(%rbp), %ecx
	movl	$1, %esi
	movq	%r12, %rdi
	movq	$0, -200(%rbp)
	movq	%rax, %r8
	movl	%ecx, %eax
	sarl	$31, %eax
	shrl	$26, %eax
	addl	%eax, %ecx
	andl	$63, %ecx
	subl	%eax, %ecx
	movl	$1, %eax
	salq	%cl, %rax
	orq	%rax, -192(%rbp,%r8,8)
	movslq	16(%rbp), %rax
	movq	%rax, -208(%rbp)
	call	BIO_test_flags@PLT
	testl	%eax, %eax
	jne	.L167
	movl	$2, %esi
	movq	%r12, %rdi
	call	BIO_test_flags@PLT
	testl	%eax, %eax
	je	.L121
	movl	-220(%rbp), %eax
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	xorl	%esi, %esi
	leaq	-208(%rbp), %r8
	leal	1(%rax), %edi
	call	select@PLT
	jmp	.L120
	.p2align 4,,10
	.p2align 3
.L113:
	movq	-264(%rbp), %rdx
	leaq	.LC20(%rip), %rsi
	movq	%r13, %rdi
	call	OCSP_REQ_CTX_add1_header@PLT
	testl	%eax, %eax
	jne	.L115
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L124:
	movq	bio_err(%rip), %rdi
	leaq	.LC17(%rip), %rsi
	xorl	%r13d, %r13d
	call	BIO_puts@PLT
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L126:
	movq	bio_err(%rip), %rdi
	leaq	.LC16(%rip), %rsi
	call	BIO_puts@PLT
.L106:
	movq	bio_err(%rip), %rdi
	leaq	.LC24(%rip), %rsi
	xorl	%eax, %eax
	xorl	%r13d, %r13d
	call	BIO_printf@PLT
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L165:
	movq	bio_err(%rip), %rdi
	leaq	.LC22(%rip), %rsi
	call	BIO_puts@PLT
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L166:
	movq	bio_err(%rip), %rdi
	leaq	.LC23(%rip), %rsi
	call	BIO_puts@PLT
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L157:
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
	xorl	%r13d, %r13d
	xorl	%r15d, %r15d
	leaq	.LC14(%rip), %rsi
	call	BIO_printf@PLT
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L163:
	movq	bio_err(%rip), %rdi
	leaq	.LC15(%rip), %rsi
	xorl	%eax, %eax
	xorl	%r13d, %r13d
	call	BIO_printf@PLT
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L121:
	movq	bio_err(%rip), %rdi
	leaq	.LC21(%rip), %rsi
	call	BIO_puts@PLT
	jmp	.L108
.L162:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1580:
	.size	process_responder, .-process_responder
	.section	.rodata.str1.1
.LC25:
	.string	" (core dumped)"
.LC26:
	.string	""
.LC27:
	.string	"/"
.LC28:
	.string	"%s: Use -help for summary.\n"
.LC29:
	.string	"%s Error parsing URL\n"
.LC30:
	.string	"issuer certificate"
.LC31:
	.string	"certificate"
	.section	.rodata.str1.8
	.align 8
.LC32:
	.string	"No issuer certificate specified\n"
	.section	.rodata.str1.1
.LC33:
	.string	"Error Creating OCSP request\n"
	.section	.rodata.str1.8
	.align 8
.LC34:
	.string	"Error converting serial number %s\n"
	.align 8
.LC35:
	.string	"Missing = in header key=value\n"
	.align 8
.LC36:
	.string	"%s: Digest must be before -cert or -serial\n"
	.section	.rodata.str1.1
.LC37:
	.string	"Error reading OCSP request\n"
.LC38:
	.string	"Error setting up accept BIO"
.LC39:
	.string	"Error starting accept"
.LC40:
	.string	"responder certificate"
	.section	.rodata.str1.8
	.align 8
.LC41:
	.string	"Error loading responder certificate\n"
	.section	.rodata.str1.1
.LC42:
	.string	"CA certificate"
.LC43:
	.string	"responder private key"
.LC44:
	.string	"responder other certificates"
	.section	.rodata.str1.8
	.align 8
.LC45:
	.string	"Responder mode requires certificate, key, and CA.\n"
	.align 8
.LC46:
	.string	"fatal: error detaching from parent process group: %s"
	.section	.rodata.str1.1
.LC47:
	.string	"child PID array"
	.section	.rodata.str1.8
	.align 8
.LC48:
	.string	"fatal: internal error: no matching child slot for pid: %ld"
	.align 8
.LC49:
	.string	"child process: %ld, exit status: %d"
	.align 8
.LC50:
	.string	"child process: %ld, term signal %d%s"
	.section	.rodata.str1.1
.LC51:
	.string	"fatal: waitpid(): %s"
.LC52:
	.string	"fatal: RAND_poll() failed"
.LC53:
	.string	"terminating on signal: %d"
	.section	.rodata.str1.8
	.align 8
.LC54:
	.string	"waiting for OCSP client connections..."
	.section	.rodata.str1.1
.LC55:
	.string	"index file changed, reloading"
	.section	.rodata.str1.8
	.align 8
.LC56:
	.string	"error reloading updated index: %s"
	.align 8
.LC57:
	.string	"Need an OCSP request for this operation!\n"
	.section	.rodata.str1.1
.LC58:
	.string	"signer certificate"
	.section	.rodata.str1.8
	.align 8
.LC59:
	.string	"Error loading signer certificate\n"
	.section	.rodata.str1.1
.LC60:
	.string	"signer private key"
.LC61:
	.string	"signer certificates"
.LC62:
	.string	"Error signing OCSP request\n"
.LC63:
	.string	"assertion failed: bn"
.LC64:
	.string	"00"
.LC65:
	.string	"parameter error \"%s\"\n"
.LC66:
	.string	"Error reading OCSP response\n"
.LC67:
	.string	"Responder Error: %s (%d)\n"
.LC68:
	.string	"validator certificate"
.LC69:
	.string	"Error parsing response\n"
	.section	.rodata.str1.8
	.align 8
.LC70:
	.string	"WARNING: no nonce in response\n"
	.section	.rodata.str1.1
.LC71:
	.string	"Nonce Verify error\n"
.LC72:
	.string	"Response Verify Failure\n"
.LC73:
	.string	"Response verify OK\n"
.LC74:
	.string	"ERROR: No Status found.\n"
	.section	.rodata.str1.8
	.align 8
.LC75:
	.string	"WARNING: Status times invalid.\n"
	.section	.rodata.str1.1
.LC76:
	.string	"%s\n"
.LC77:
	.string	"\tThis Update: "
.LC78:
	.string	"\tNext Update: "
.LC79:
	.string	"\tReason: %s\n"
.LC80:
	.string	"\tRevocation Time: "
	.section	.rodata.str1.8
	.align 8
.LC81:
	.string	"fatal: internal error: no free child slots"
	.text
	.p2align 4
	.globl	ocsp_main
	.type	ocsp_main, @function
ocsp_main:
.LFB1562:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%edi, %r12d
	pushq	%rbx
	subq	$808, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	.LC27(%rip), %rax
	movq	$0, -448(%rbp)
	movq	$0, -440(%rbp)
	movq	$0, -432(%rbp)
	movq	$0, -424(%rbp)
	movq	$0, -416(%rbp)
	movq	$0, -408(%rbp)
	movq	$0, -400(%rbp)
	movq	$0, -392(%rbp)
	movq	$0, -384(%rbp)
	movq	$0, -376(%rbp)
	movq	$0, -368(%rbp)
	movq	%rax, -360(%rbp)
	movl	$-1, -464(%rbp)
	movl	$-1, -460(%rbp)
	movl	$0, -456(%rbp)
	movq	$300, -352(%rbp)
	movq	$-1, -344(%rbp)
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, -520(%rbp)
	testq	%rax, %rax
	je	.L171
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, -512(%rbp)
	testq	%rax, %rax
	je	.L171
	call	X509_VERIFY_PARAM_new@PLT
	movq	%rax, -552(%rbp)
	testq	%rax, %rax
	je	.L392
	movq	%r13, %rsi
	movl	%r12d, %edi
	leaq	ocsp_options(%rip), %rdx
	xorl	%r15d, %r15d
	call	opt_init@PLT
	xorl	%r13d, %r13d
	xorl	%r14d, %r14d
	xorl	%r12d, %r12d
	movq	%rax, prog(%rip)
	leaq	.L178(%rip), %rbx
	movq	$0, -672(%rbp)
	movq	$0, -624(%rbp)
	movq	$0, -720(%rbp)
	movl	$-1, -648(%rbp)
	movl	$0, -640(%rbp)
	movl	$0, -644(%rbp)
	movl	$-1, -632(%rbp)
	movl	$0, -724(%rbp)
	movl	$0, -752(%rbp)
	movl	$1, -636(%rbp)
	movl	$0, -748(%rbp)
	movl	$0, -728(%rbp)
	movq	$0, -592(%rbp)
	movq	$0, -600(%rbp)
	movq	$0, -608(%rbp)
	movq	$0, -656(%rbp)
	movq	$0, -680(%rbp)
	movq	$0, -544(%rbp)
	movq	$0, -664(%rbp)
	movq	$0, -688(%rbp)
	movq	$0, -504(%rbp)
	movq	$0, -704(%rbp)
	movq	$0, -696(%rbp)
	movq	$0, -712(%rbp)
	movq	$0, -488(%rbp)
	movq	$0, -480(%rbp)
	movq	$0, -560(%rbp)
	movq	$0, -528(%rbp)
	movq	$0, -744(%rbp)
	movq	$0, -736(%rbp)
	movq	$0, -536(%rbp)
	movq	$0, -472(%rbp)
	movq	$0, -568(%rbp)
	movq	$0, -584(%rbp)
.L173:
	call	opt_next@PLT
	testl	%eax, %eax
	je	.L563
.L254:
	cmpl	$54, %eax
	jg	.L174
	cmpl	$-1, %eax
	jl	.L173
	addl	$1, %eax
	cmpl	$55, %eax
	ja	.L173
	movslq	(%rbx,%rax,4), %rax
	addq	%rbx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L178:
	.long	.L231-.L178
	.long	.L173-.L178
	.long	.L230-.L178
	.long	.L229-.L178
	.long	.L228-.L178
	.long	.L227-.L178
	.long	.L226-.L178
	.long	.L225-.L178
	.long	.L224-.L178
	.long	.L393-.L178
	.long	.L223-.L178
	.long	.L222-.L178
	.long	.L221-.L178
	.long	.L220-.L178
	.long	.L219-.L178
	.long	.L218-.L178
	.long	.L217-.L178
	.long	.L216-.L178
	.long	.L215-.L178
	.long	.L214-.L178
	.long	.L544-.L178
	.long	.L212-.L178
	.long	.L211-.L178
	.long	.L210-.L178
	.long	.L209-.L178
	.long	.L208-.L178
	.long	.L207-.L178
	.long	.L206-.L178
	.long	.L205-.L178
	.long	.L204-.L178
	.long	.L203-.L178
	.long	.L202-.L178
	.long	.L201-.L178
	.long	.L200-.L178
	.long	.L199-.L178
	.long	.L198-.L178
	.long	.L197-.L178
	.long	.L196-.L178
	.long	.L195-.L178
	.long	.L194-.L178
	.long	.L193-.L178
	.long	.L192-.L178
	.long	.L191-.L178
	.long	.L190-.L178
	.long	.L189-.L178
	.long	.L188-.L178
	.long	.L187-.L178
	.long	.L186-.L178
	.long	.L185-.L178
	.long	.L184-.L178
	.long	.L183-.L178
	.long	.L182-.L178
	.long	.L181-.L178
	.long	.L180-.L178
	.long	.L179-.L178
	.long	.L177-.L178
	.text
	.p2align 4,,10
	.p2align 3
.L171:
	movq	$0, -472(%rbp)
	xorl	%r14d, %r14d
	movl	$1, -496(%rbp)
	movq	$0, -592(%rbp)
	movq	$0, -600(%rbp)
	movq	$0, -608(%rbp)
	movq	$0, -552(%rbp)
	movq	$0, -488(%rbp)
	movq	$0, -616(%rbp)
	movq	$0, -544(%rbp)
	movq	$0, -536(%rbp)
	movq	$0, -568(%rbp)
	movq	$0, -512(%rbp)
	movq	$0, -504(%rbp)
	movq	$0, -560(%rbp)
	movq	$0, -480(%rbp)
	movq	$0, -584(%rbp)
	movq	$0, -576(%rbp)
	movq	$0, -528(%rbp)
.L170:
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	movq	-544(%rbp), %rdi
	call	X509_free@PLT
	movq	-488(%rbp), %rdi
	call	X509_STORE_free@PLT
	movq	-552(%rbp), %rdi
	call	X509_VERIFY_PARAM_free@PLT
	movq	-584(%rbp), %rdi
	call	OPENSSL_sk_free@PLT
	movq	-560(%rbp), %rdi
	call	EVP_PKEY_free@PLT
	movq	-504(%rbp), %rdi
	call	EVP_PKEY_free@PLT
	movq	-536(%rbp), %rdi
	call	X509_free@PLT
	movq	X509_free@GOTPCREL(%rip), %r12
	movq	-568(%rbp), %rdi
	movq	%r12, %rsi
	call	OPENSSL_sk_pop_free@PLT
	movq	-616(%rbp), %rdi
	call	X509_free@PLT
	movq	-384(%rbp), %rdi
	movq	%r12, %rsi
	call	OPENSSL_sk_pop_free@PLT
	movq	-480(%rbp), %rdi
	call	free_index@PLT
	movq	-448(%rbp), %rdi
	call	BIO_free_all@PLT
	movq	-528(%rbp), %rdi
	call	BIO_free_all@PLT
	movq	-576(%rbp), %rdi
	call	BIO_free_all@PLT
	movq	-424(%rbp), %rdi
	call	OCSP_REQUEST_free@PLT
	movq	-472(%rbp), %rdi
	call	OCSP_RESPONSE_free@PLT
	movq	%r14, %rdi
	call	OCSP_BASICRESP_free@PLT
	movq	-520(%rbp), %rdi
	call	OPENSSL_sk_free@PLT
	movq	-512(%rbp), %rdi
	call	OPENSSL_sk_free@PLT
	movq	-408(%rbp), %rdi
	movq	%r12, %rsi
	call	OPENSSL_sk_pop_free@PLT
	movq	-400(%rbp), %rdi
	movq	%r12, %rsi
	call	OPENSSL_sk_pop_free@PLT
	movq	X509V3_conf_free@GOTPCREL(%rip), %rsi
	movq	-416(%rbp), %rdi
	call	OPENSSL_sk_pop_free@PLT
	movq	-608(%rbp), %rdi
	movl	$827, %edx
	leaq	.LC1(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-600(%rbp), %rdi
	movl	$828, %edx
	leaq	.LC1(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-592(%rbp), %rdi
	movl	$829, %edx
	leaq	.LC1(%rip), %rsi
	call	CRYPTO_free@PLT
.L168:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L564
	movl	-496(%rbp), %eax
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L393:
	.cfi_restore_state
	movl	$1, %r13d
	call	opt_next@PLT
	testl	%eax, %eax
	jne	.L254
.L563:
	testl	%r12d, %r12d
	jne	.L255
	call	opt_num_rest@PLT
	movl	%eax, -496(%rbp)
	testl	%eax, %eax
	jne	.L231
	cmpq	$0, -424(%rbp)
	je	.L565
.L257:
	movq	-528(%rbp), %rdi
	movl	$32769, %edx
	movl	$119, %esi
	call	bio_open_default@PLT
	movq	%rax, -576(%rbp)
	testq	%rax, %rax
	je	.L405
	cmpq	$0, -424(%rbp)
	je	.L566
.L258:
	movq	$0, -528(%rbp)
.L264:
	movq	-504(%rbp), %rax
	testq	%rax, %rax
	je	.L265
	testq	%r14, %r14
	leaq	.LC40(%rip), %rdx
	movl	$32773, %esi
	movq	%rax, %rdi
	cmove	%rax, %r14
	call	load_cert@PLT
	movq	%rax, -616(%rbp)
	testq	%rax, %rax
	je	.L567
	movq	-560(%rbp), %rdi
	xorl	%ecx, %ecx
	leaq	-384(%rbp), %rsi
	leaq	.LC42(%rip), %r8
	movl	$32773, %edx
	call	load_certs@PLT
	testl	%eax, %eax
	je	.L411
	cmpq	$0, -544(%rbp)
	je	.L270
	movq	-544(%rbp), %rdi
	xorl	%ecx, %ecx
	leaq	-392(%rbp), %rsi
	leaq	.LC44(%rip), %r8
	movl	$32773, %edx
	call	load_certs@PLT
	testl	%eax, %eax
	je	.L411
.L270:
	leaq	.LC43(%rip), %r9
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$32773, %esi
	movq	%r14, %rdi
	call	load_key@PLT
	movq	%rax, -504(%rbp)
	testq	%rax, %rax
	je	.L568
	cmpq	$0, -704(%rbp)
	je	.L546
	cmpq	$0, -384(%rbp)
	je	.L385
	movq	-704(%rbp), %rdi
	xorl	%esi, %esi
	call	load_index@PLT
	movq	%rax, -480(%rbp)
	testq	%rax, %rax
	je	.L412
	movq	%rax, %rdi
	call	index_index@PLT
	testl	%eax, %eax
	jle	.L413
.L388:
	movl	multi(%rip), %r8d
	testl	%r8d, %r8d
	je	.L271
	cmpq	$0, -528(%rbp)
	je	.L272
	movq	prog(%rip), %rdi
	movl	$1, %esi
	movl	$24, %edx
	call	openlog@PLT
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	setpgid@PLT
	testl	%eax, %eax
	jne	.L569
	movl	multi(%rip), %eax
	leaq	.LC47(%rip), %rsi
	leal	0(,%rax,4), %edi
	call	app_malloc@PLT
	movq	%rax, %r14
	movl	multi(%rip), %eax
	testl	%eax, %eax
	jle	.L277
	subl	$1, %eax
	xorl	%esi, %esi
	movq	%r14, %rdi
	leaq	4(,%rax,4), %rdx
	call	memset@PLT
.L277:
	leaq	noteterm(%rip), %rsi
	movl	$2, %edi
	call	signal@PLT
	leaq	noteterm(%rip), %rsi
	movl	$15, %edi
	call	signal@PLT
	movl	termsig(%rip), %ecx
	testl	%ecx, %ecx
	jne	.L276
	xorl	%r12d, %r12d
	leaq	-296(%rbp), %rbx
	.p2align 4,,10
	.p2align 3
.L288:
	cmpl	multi(%rip), %r12d
	jl	.L290
	xorl	%edx, %edx
	movq	%rbx, %rsi
	movl	$-1, %edi
	call	waitpid@PLT
	testl	%eax, %eax
	jg	.L570
	call	__errno_location@PLT
	movl	(%rax), %edi
	cmpl	$4, %edi
	jne	.L287
.L295:
	movl	termsig(%rip), %ecx
	testl	%ecx, %ecx
	je	.L288
.L276:
	movl	$1, %esi
	movl	$6, %edi
	leaq	.LC53(%rip), %rdx
	xorl	%eax, %eax
	call	__syslog_chk@PLT
	movq	%r14, %rsi
	xorl	%edi, %edi
	call	killall
.L224:
	movl	$1, %r15d
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L174:
	cmpl	$2032, %eax
	je	.L232
	jle	.L571
	cmpl	$2033, %eax
	jne	.L173
	call	opt_arg@PLT
	movl	$10, %edx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	strtol@PLT
	movl	%eax, multi(%rip)
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L571:
	leal	-2001(%rax), %edx
	cmpl	$29, %edx
	ja	.L173
	movq	-552(%rbp), %rsi
	movl	%eax, %edi
	call	opt_verify@PLT
	testl	%eax, %eax
	je	.L552
	addl	$1, -752(%rbp)
	jmp	.L173
.L204:
	call	opt_arg@PLT
	movq	%rax, -664(%rbp)
.L544:
	orq	$512, -624(%rbp)
	jmp	.L173
.L255:
	movq	prog(%rip), %rdx
	movq	bio_err(%rip), %rdi
	leaq	.LC36(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L231:
	movq	prog(%rip), %rdx
	movq	bio_err(%rip), %rdi
	leaq	.LC28(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	$0, -472(%rbp)
	movl	$1, -496(%rbp)
.L553:
	movq	$0, -488(%rbp)
	xorl	%r14d, %r14d
	movq	$0, -616(%rbp)
	movq	$0, -544(%rbp)
	movq	$0, -504(%rbp)
	movq	$0, -560(%rbp)
	movq	$0, -480(%rbp)
	movq	$0, -576(%rbp)
	movq	$0, -528(%rbp)
	jmp	.L170
.L216:
	orq	$8, -624(%rbp)
	jmp	.L173
.L217:
	orq	$16, -624(%rbp)
	jmp	.L173
.L218:
	orq	$4, -624(%rbp)
	jmp	.L173
.L219:
	movq	$1, -720(%rbp)
	jmp	.L173
.L220:
	orq	$1024, -672(%rbp)
	jmp	.L173
.L221:
	orq	$1, -672(%rbp)
	jmp	.L173
.L222:
	movl	$0, -636(%rbp)
	jmp	.L173
.L223:
	movl	$2, -636(%rbp)
	jmp	.L173
.L214:
	orq	$32, -624(%rbp)
	jmp	.L173
.L215:
	orq	$256, -624(%rbp)
	jmp	.L173
.L229:
	call	opt_arg@PLT
	movq	%rax, -528(%rbp)
	jmp	.L173
.L230:
	leaq	ocsp_options(%rip), %rdi
	call	opt_help@PLT
	movq	$0, -472(%rbp)
	movl	$0, -496(%rbp)
	jmp	.L553
.L225:
	call	opt_arg@PLT
	movq	%rax, -368(%rbp)
	jmp	.L173
.L226:
	call	opt_arg@PLT
	movq	%rax, -376(%rbp)
	jmp	.L173
.L227:
	movq	-608(%rbp), %rdi
	movl	$294, %edx
	leaq	.LC1(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-600(%rbp), %rdi
	movl	$295, %edx
	leaq	.LC1(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-592(%rbp), %rdi
	movl	$296, %edx
	leaq	.LC1(%rip), %rsi
	call	CRYPTO_free@PLT
	call	opt_arg@PLT
	leaq	-360(%rbp), %rcx
	leaq	-368(%rbp), %rdx
	movq	%rax, %rdi
	leaq	-376(%rbp), %rsi
	leaq	-460(%rbp), %r8
	call	OCSP_parse_url@PLT
	testl	%eax, %eax
	je	.L572
	movq	-376(%rbp), %rax
	movq	%rax, -608(%rbp)
	movq	-368(%rbp), %rax
	movq	%rax, -600(%rbp)
	movq	-360(%rbp), %rax
	movq	%rax, -592(%rbp)
	jmp	.L173
.L228:
	call	opt_arg@PLT
	movl	$10, %edx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	strtol@PLT
	movl	%eax, -648(%rbp)
	jmp	.L173
.L177:
	call	opt_arg@PLT
	movl	$61, %esi
	movq	%rax, %rdi
	movq	%rax, -496(%rbp)
	call	strchr@PLT
	movq	-496(%rbp), %rdi
	testq	%rax, %rax
	je	.L573
	movb	$0, (%rax)
	leaq	-416(%rbp), %rdx
	leaq	1(%rax), %rsi
	call	X509V3_add_value@PLT
	testl	%eax, %eax
	jne	.L173
	jmp	.L552
.L179:
	cmpq	$0, -584(%rbp)
	je	.L574
.L251:
	call	opt_arg@PLT
	movq	-584(%rbp), %rdi
	movq	%rax, %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	jne	.L173
	jmp	.L552
.L180:
	call	opt_arg@PLT
	leaq	-432(%rbp), %rsi
	movq	%rax, %rdi
	call	opt_md@PLT
	testl	%eax, %eax
	jne	.L173
	jmp	.L552
.L181:
	call	opt_arg@PLT
	movq	%rax, -544(%rbp)
	jmp	.L173
.L182:
	call	opt_arg@PLT
	movq	%rax, %r14
	jmp	.L173
.L183:
	call	opt_arg@PLT
	movq	%rax, -504(%rbp)
	jmp	.L173
.L184:
	call	opt_arg@PLT
	movl	$10, %edx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	strtol@PLT
	movl	%eax, -632(%rbp)
	jmp	.L173
.L185:
	call	opt_arg@PLT
	leaq	-464(%rbp), %rsi
	movq	%rax, %rdi
	call	opt_int@PLT
	jmp	.L173
.L186:
	call	opt_arg@PLT
	leaq	-456(%rbp), %rsi
	movq	%rax, %rdi
	call	opt_int@PLT
	movl	-632(%rbp), %ecx
	movl	$0, %eax
	cmpl	$-1, %ecx
	cmovne	%ecx, %eax
	movl	%eax, -632(%rbp)
	jmp	.L173
.L187:
	call	opt_arg@PLT
	movq	%rax, -560(%rbp)
	jmp	.L173
.L188:
	call	opt_arg@PLT
	movq	%rax, -704(%rbp)
	jmp	.L173
.L189:
	movq	-440(%rbp), %r12
	testq	%r12, %r12
	je	.L575
.L244:
	call	opt_arg@PLT
	cmpq	$0, -472(%rbp)
	movq	%rax, -496(%rbp)
	je	.L558
	cmpq	$0, -424(%rbp)
	je	.L246
.L249:
	movq	-472(%rbp), %rdi
	call	X509_get_subject_name@PLT
	movq	-472(%rbp), %rdi
	movq	%rax, -616(%rbp)
	call	X509_get0_pubkey_bitstr@PLT
	movq	-496(%rbp), %rsi
	xorl	%edi, %edi
	movq	%rax, -576(%rbp)
	call	s2i_ASN1_INTEGER@PLT
	movq	-576(%rbp), %rdx
	movq	-616(%rbp), %r9
	testq	%rax, %rax
	je	.L576
	movq	%r12, %rdi
	movq	%rax, %rcx
	movq	%r9, %rsi
	movq	%rax, -496(%rbp)
	call	OCSP_cert_id_new@PLT
	movq	-496(%rbp), %r8
	movq	%rax, %r12
	movq	%r8, %rdi
	call	ASN1_INTEGER_free@PLT
	testq	%r12, %r12
	je	.L241
.L562:
	movq	-512(%rbp), %rdi
	movq	%r12, %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	jne	.L577
.L241:
	movq	bio_err(%rip), %rdi
	leaq	.LC33(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L552:
	movq	$0, -472(%rbp)
	movl	$1, -496(%rbp)
	jmp	.L553
.L190:
	movq	-536(%rbp), %rdi
	call	X509_free@PLT
	call	opt_arg@PLT
	leaq	.LC31(%rip), %rdx
	movl	$32773, %esi
	movq	%rax, %rdi
	call	load_cert@PLT
	movq	%rax, -536(%rbp)
	testq	%rax, %rax
	je	.L396
	movq	-440(%rbp), %r12
	testq	%r12, %r12
	je	.L578
.L238:
	cmpq	$0, -472(%rbp)
	je	.L558
	cmpq	$0, -424(%rbp)
	je	.L240
.L243:
	movq	-472(%rbp), %rdx
	movq	-536(%rbp), %rsi
	movq	%r12, %rdi
	call	OCSP_cert_to_id@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L562
	jmp	.L241
.L191:
	call	opt_arg@PLT
	leaq	.LC30(%rip), %rdx
	movl	$32773, %esi
	movq	%rax, %rdi
	call	load_cert@PLT
	movq	%rax, -472(%rbp)
	testq	%rax, %rax
	je	.L551
	cmpq	$0, -568(%rbp)
	je	.L579
.L237:
	movq	-472(%rbp), %rsi
	movq	-568(%rbp), %rdi
	call	OPENSSL_sk_push@PLT
	jmp	.L173
.L192:
	call	opt_arg@PLT
	movq	%rax, -360(%rbp)
	jmp	.L173
.L193:
	call	opt_arg@PLT
	movq	%rax, -696(%rbp)
	jmp	.L173
.L194:
	call	opt_arg@PLT
	movq	%rax, -712(%rbp)
	jmp	.L173
.L195:
	call	opt_arg@PLT
	movq	%rax, -656(%rbp)
	jmp	.L173
.L196:
	call	opt_arg@PLT
	leaq	-344(%rbp), %rsi
	movq	%rax, %rdi
	call	opt_long@PLT
	jmp	.L173
.L197:
	call	opt_arg@PLT
	leaq	-352(%rbp), %rsi
	movq	%rax, %rdi
	call	opt_long@PLT
	jmp	.L173
.L198:
	movl	$1, -748(%rbp)
	jmp	.L173
.L199:
	movl	$1, -728(%rbp)
	jmp	.L173
.L200:
	call	opt_arg@PLT
	movq	%rax, -744(%rbp)
	jmp	.L173
.L201:
	call	opt_arg@PLT
	movq	%rax, -736(%rbp)
	jmp	.L173
.L202:
	call	opt_arg@PLT
	movq	%rax, -664(%rbp)
	jmp	.L173
.L203:
	call	opt_arg@PLT
	movq	%rax, -688(%rbp)
	jmp	.L173
.L205:
	call	opt_arg@PLT
	movq	%rax, -680(%rbp)
	jmp	.L173
.L206:
	call	opt_arg@PLT
	movq	%rax, -488(%rbp)
	jmp	.L173
.L207:
	call	opt_arg@PLT
	movq	%rax, -480(%rbp)
	jmp	.L173
.L208:
	movl	$1, -640(%rbp)
	jmp	.L173
.L209:
	movl	$1, -644(%rbp)
	jmp	.L173
.L210:
	movl	$1, -640(%rbp)
	movl	$1, -644(%rbp)
	jmp	.L173
.L211:
	movl	$1, -724(%rbp)
	jmp	.L173
.L212:
	orq	$2, -624(%rbp)
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L392:
	movl	$1, -496(%rbp)
	jmp	.L168
	.p2align 4,,10
	.p2align 3
.L232:
	testl	%r12d, %r12d
	jne	.L255
	call	opt_unknown@PLT
	leaq	-440(%rbp), %rsi
	movq	%rax, %rdi
	call	opt_md@PLT
	testl	%eax, %eax
	je	.L231
	movl	$1, %r12d
	jmp	.L173
.L577:
	movq	-424(%rbp), %rdi
	movq	%r12, %rsi
	call	OCSP_request_add0_id@PLT
	testq	%rax, %rax
	je	.L241
	call	opt_arg@PLT
	movq	-520(%rbp), %rdi
	movq	%rax, %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L552
	xorl	%r12d, %r12d
	jmp	.L173
.L579:
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, -568(%rbp)
	testq	%rax, %rax
	jne	.L237
.L396:
	movq	$0, -472(%rbp)
.L551:
	movq	$0, -488(%rbp)
	xorl	%r14d, %r14d
	movq	$0, -616(%rbp)
	movq	$0, -544(%rbp)
	movq	$0, -504(%rbp)
	movq	$0, -560(%rbp)
	movq	$0, -480(%rbp)
	movq	$0, -576(%rbp)
	movq	$0, -528(%rbp)
	movl	$1, -496(%rbp)
	jmp	.L170
.L246:
	call	OCSP_REQUEST_new@PLT
	movq	%rax, -424(%rbp)
	testq	%rax, %rax
	je	.L241
	jmp	.L249
	.p2align 4,,10
	.p2align 3
.L240:
	call	OCSP_REQUEST_new@PLT
	movq	%rax, -424(%rbp)
	testq	%rax, %rax
	je	.L241
	jmp	.L243
	.p2align 4,,10
	.p2align 3
.L565:
	movq	-480(%rbp), %rax
	orq	-488(%rbp), %rax
	jne	.L257
	cmpq	$0, -368(%rbp)
	je	.L231
	cmpq	$0, -704(%rbp)
	jne	.L257
	jmp	.L231
.L558:
	movq	bio_err(%rip), %rdi
	leaq	.LC32(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L551
.L575:
	call	EVP_sha1@PLT
	movq	%rax, -440(%rbp)
	movq	%rax, %r12
	jmp	.L244
.L574:
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, -584(%rbp)
	testq	%rax, %rax
	jne	.L251
	jmp	.L396
.L578:
	call	EVP_sha1@PLT
	movq	%rax, -440(%rbp)
	movq	%rax, %r12
	jmp	.L238
.L576:
	movq	-496(%rbp), %rdx
	movq	bio_err(%rip), %rdi
	xorl	%r14d, %r14d
	movq	%rax, -528(%rbp)
	leaq	.LC34(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	$0, -472(%rbp)
	movq	$0, -488(%rbp)
	movq	$0, -616(%rbp)
	movq	$0, -544(%rbp)
	movq	$0, -504(%rbp)
	movq	$0, -560(%rbp)
	movq	$0, -480(%rbp)
	movq	$0, -576(%rbp)
	movl	$1, -496(%rbp)
	jmp	.L170
.L566:
	movl	-636(%rbp), %ecx
	movl	$0, %eax
	movq	-480(%rbp), %rdi
	cmpl	$2, %ecx
	cmove	%ecx, %eax
	movl	%eax, -636(%rbp)
	testq	%rdi, %rdi
	je	.L537
	movl	$4, %edx
	movl	$114, %esi
	call	bio_open_default@PLT
	movq	%rax, -472(%rbp)
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L406
	movq	d2i_OCSP_REQUEST@GOTPCREL(%rip), %rsi
	movq	OCSP_REQUEST_new@GOTPCREL(%rip), %rdi
	xorl	%ecx, %ecx
	movq	%rax, %rdx
	call	ASN1_d2i_bio@PLT
	movq	%rbx, %rdi
	movq	%rax, -424(%rbp)
	call	BIO_free@PLT
	movq	-424(%rbp), %rax
	movq	%rax, -472(%rbp)
	testq	%rax, %rax
	jne	.L258
	movq	bio_err(%rip), %rdi
	leaq	.LC37(%rip), %rsi
	call	BIO_printf@PLT
.L406:
	movq	$0, -488(%rbp)
	xorl	%r14d, %r14d
	movq	$0, -616(%rbp)
	movq	$0, -544(%rbp)
	movq	$0, -504(%rbp)
	movq	$0, -560(%rbp)
	movq	$0, -480(%rbp)
	movq	$0, -528(%rbp)
	movl	$1, -496(%rbp)
	jmp	.L170
.L537:
	movq	-368(%rbp), %rax
	movq	%rax, -528(%rbp)
	testq	%rax, %rax
	je	.L264
	call	BIO_f_buffer@PLT
	movq	%rax, %rdi
	call	BIO_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L407
	call	BIO_s_accept@PLT
	movq	%rax, %rdi
	call	BIO_new@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L263
	xorl	%ecx, %ecx
	movl	$1, %edx
	movl	$131, %esi
	movq	%rax, %rdi
	call	BIO_ctrl@PLT
	testq	%rax, %rax
	js	.L263
	movq	-528(%rbp), %rcx
	movl	$1, %edx
	movl	$118, %esi
	movq	%rbx, %rdi
	call	BIO_ctrl@PLT
	testq	%rax, %rax
	js	.L263
	movq	%r12, %rcx
	movl	$3, %edx
	movl	$118, %esi
	movq	%rbx, %rdi
	call	BIO_ctrl@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$101, %esi
	movq	%rbx, %rdi
	call	BIO_ctrl@PLT
	movq	%rbx, -528(%rbp)
	testq	%rax, %rax
	jg	.L264
	leaq	.LC39(%rip), %rsi
	movl	$3, %edi
	xorl	%eax, %eax
	xorl	%r12d, %r12d
	call	log_message
.L261:
	movq	%rbx, %rdi
	xorl	%r14d, %r14d
	call	BIO_free_all@PLT
	movq	%r12, %rdi
	call	BIO_free@PLT
	movq	$0, -472(%rbp)
	movq	$0, -488(%rbp)
	movq	$0, -616(%rbp)
	movq	$0, -544(%rbp)
	movq	$0, -504(%rbp)
	movq	$0, -560(%rbp)
	movq	$0, -528(%rbp)
	movl	$1, -496(%rbp)
	jmp	.L170
.L265:
	movq	-704(%rbp), %rax
	movq	$0, -616(%rbp)
	testq	%rax, %rax
	jne	.L385
	movq	%rax, -504(%rbp)
.L546:
	movq	$0, -480(%rbp)
	jmp	.L388
.L411:
	movq	$0, -472(%rbp)
	xorl	%r14d, %r14d
	movl	$1, -496(%rbp)
	movq	$0, -488(%rbp)
	movq	$0, -544(%rbp)
	movq	$0, -504(%rbp)
	movq	$0, -560(%rbp)
	movq	$0, -480(%rbp)
	jmp	.L170
.L271:
	cmpq	$0, -528(%rbp)
	je	.L272
	movl	-648(%rbp), %eax
	testl	%eax, %eax
	jle	.L580
.L300:
	leaq	socket_timeout(%rip), %rsi
	movl	$14, %edi
	call	signal@PLT
.L301:
	leaq	.LC54(%rip), %rsi
	movl	$6, %edi
	xorl	%eax, %eax
	call	log_message
.L272:
	movq	-704(%rbp), %rax
	orq	-680(%rbp), %rax
	movq	$0, -544(%rbp)
	orq	-712(%rbp), %rax
	movl	%r13d, -836(%rbp)
	movq	%rax, -832(%rbp)
	movq	$0, -560(%rbp)
	movl	%r15d, -768(%rbp)
	.p2align 4,,10
	.p2align 3
.L304:
	cmpq	$0, -528(%rbp)
	je	.L305
	movq	-480(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L312
	movq	16(%rbx), %rsi
	leaq	-288(%rbp), %rdx
	movl	$1, %edi
	call	__xstat@PLT
	cmpl	$-1, %eax
	je	.L312
	movq	-200(%rbp), %rax
	cmpq	%rax, 112(%rbx)
	jne	.L308
	movq	-184(%rbp), %rax
	cmpq	%rax, 128(%rbx)
	jne	.L308
	movq	-280(%rbp), %rax
	cmpq	%rax, 32(%rbx)
	jne	.L308
	movq	-288(%rbp), %rax
	cmpq	%rax, 24(%rbx)
	je	.L312
.L308:
	leaq	.LC55(%rip), %rdx
	movl	$1, %esi
	movl	$6, %edi
	xorl	%eax, %eax
	call	__syslog_chk@PLT
	movq	-704(%rbp), %rdi
	xorl	%esi, %esi
	call	load_index@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L311
	movq	%rax, %rdi
	call	index_index@PLT
	testl	%eax, %eax
	jg	.L581
.L311:
	movq	%r12, %rdi
	call	free_index@PLT
	movq	-704(%rbp), %rdx
	leaq	.LC56(%rip), %rsi
	xorl	%eax, %eax
	movl	$3, %edi
	call	log_message
.L312:
	movl	-648(%rbp), %ecx
	movq	-528(%rbp), %rdx
	leaq	-448(%rbp), %rsi
	leaq	-424(%rbp), %rdi
	movq	$0, -424(%rbp)
	call	do_responder.constprop.0
	testl	%eax, %eax
	je	.L304
	movq	-424(%rbp), %r12
	testq	%r12, %r12
	je	.L582
.L314:
	movl	-636(%rbp), %eax
	testl	%eax, %eax
	jne	.L319
.L322:
	cmpq	$0, -680(%rbp)
	je	.L321
	movq	-656(%rbp), %rax
	movq	-680(%rbp), %rdi
	movl	$32773, %esi
	leaq	.LC58(%rip), %rdx
	testq	%rax, %rax
	cmove	%rdi, %rax
	movq	%rax, -656(%rbp)
	call	load_cert@PLT
	movq	%rax, -544(%rbp)
	testq	%rax, %rax
	je	.L583
	cmpq	$0, -688(%rbp)
	je	.L327
	movq	-688(%rbp), %rdi
	xorl	%ecx, %ecx
	leaq	-408(%rbp), %rsi
	leaq	.LC61(%rip), %r8
	movl	$32773, %edx
	call	load_certs@PLT
	testl	%eax, %eax
	je	.L419
.L327:
	movq	-656(%rbp), %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	leaq	.LC60(%rip), %r9
	movl	$32773, %esi
	call	load_key@PLT
	movq	%rax, -560(%rbp)
	testq	%rax, %rax
	je	.L425
	movq	-720(%rbp), %r9
	movq	-408(%rbp), %r8
	xorl	%ecx, %ecx
	movq	%rax, %rdx
	movq	-544(%rbp), %rsi
	movq	-424(%rbp), %rdi
	call	OCSP_request_sign@PLT
	testl	%eax, %eax
	je	.L584
.L321:
	movl	-644(%rbp), %eax
	testl	%eax, %eax
	je	.L328
	movq	-424(%rbp), %rsi
	testq	%rsi, %rsi
	je	.L328
	movq	-576(%rbp), %rdi
	xorl	%edx, %edx
	call	OCSP_REQUEST_print@PLT
.L328:
	movq	-712(%rbp), %rax
	testq	%rax, %rax
	je	.L318
	movl	$4, %edx
	movl	$119, %esi
	movq	%rax, %rdi
	call	bio_open_default@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L420
	movq	i2d_OCSP_REQUEST@GOTPCREL(%rip), %rdi
	movq	-424(%rbp), %rdx
	movq	%rax, %rsi
	call	ASN1_i2d_bio@PLT
	movq	%r12, %rdi
	call	BIO_free@PLT
.L318:
	cmpq	$0, -480(%rbp)
	je	.L329
	movq	-392(%rbp), %rax
	movq	-424(%rbp), %rdi
	movq	$0, -336(%rbp)
	movl	-456(%rbp), %ebx
	movq	-384(%rbp), %r12
	movq	%rax, -808(%rbp)
	movq	-432(%rbp), %rax
	movq	%rdi, -760(%rbp)
	movq	%rax, -816(%rbp)
	movq	bio_err(%rip), %rax
	movq	%rax, -824(%rbp)
	call	OCSP_request_onereq_count@PLT
	movl	%eax, -764(%rbp)
	testl	%eax, %eax
	jle	.L585
	call	OCSP_BASICRESP_new@PLT
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	%rax, -784(%rbp)
	call	X509_gmtime_adj@PLT
	movl	-632(%rbp), %esi
	movq	$0, -792(%rbp)
	movq	%rax, -800(%rbp)
	cmpl	$-1, %esi
	je	.L332
	imull	$60, %ebx, %edx
	xorl	%ecx, %ecx
	xorl	%edi, %edi
	movslq	%edx, %rdx
	call	X509_time_adj_ex@PLT
	movq	%rax, -792(%rbp)
.L332:
	leaq	-320(%rbp), %rax
	movl	$0, -628(%rbp)
	movq	%rax, -776(%rbp)
	.p2align 4,,10
	.p2align 3
.L346:
	movl	-628(%rbp), %esi
	movq	-760(%rbp), %rdi
	call	OCSP_request_onereq_get0@PLT
	movq	%rax, %rdi
	call	OCSP_onereq_get0_id@PLT
	movq	-776(%rbp), %rsi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %r8
	xorl	%edi, %edi
	movq	%rax, %r13
	call	OCSP_id_get0_info@PLT
	movq	-320(%rbp), %rdi
	call	OBJ_obj2nid@PLT
	movl	%eax, %edi
	call	OBJ_nid2sn@PLT
	movq	%rax, %rdi
	call	EVP_get_digestbyname@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L548
	movq	%rax, -472(%rbp)
	xorl	%ebx, %ebx
	xorl	%r15d, %r15d
	jmp	.L333
	.p2align 4,,10
	.p2align 3
.L586:
	testl	%ebx, %ebx
	jne	.L336
	movl	%r15d, %esi
	movq	%r12, %rdi
	xorl	%ebx, %ebx
	call	OPENSSL_sk_value@PLT
	movq	-472(%rbp), %rdi
	xorl	%esi, %esi
	movq	%rax, %rdx
	call	OCSP_cert_to_id@PLT
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%rax, %r14
	call	OCSP_id_issuer_cmp@PLT
	movq	%r14, %rdi
	testl	%eax, %eax
	sete	%bl
	addl	$1, %r15d
	call	OCSP_CERTID_free@PLT
.L333:
	movq	%r12, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r15d
	jl	.L586
	testl	%ebx, %ebx
	jne	.L336
.L547:
	subq	$8, %rsp
	pushq	-792(%rbp)
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	-800(%rbp), %r9
	movl	$2, %edx
	movq	%r13, %rsi
	movq	-784(%rbp), %rdi
	call	OCSP_basic_add1_status@PLT
	popq	%r15
	popq	%rax
.L337:
	addl	$1, -628(%rbp)
	movl	-628(%rbp), %eax
	cmpl	%eax, -764(%rbp)
	jne	.L346
	movq	-760(%rbp), %rsi
	movq	-784(%rbp), %rdi
	call	OCSP_copy_nonce@PLT
	call	EVP_MD_CTX_new@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L548
	movq	-504(%rbp), %r8
	movq	-816(%rbp), %rdx
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	leaq	-336(%rbp), %rsi
	call	EVP_DigestSignInit@PLT
	testl	%eax, %eax
	je	.L548
	movq	-584(%rbp), %r13
	xorl	%ebx, %ebx
	jmp	.L349
.L351:
	movl	%ebx, %esi
	movq	%r13, %rdi
	call	OPENSSL_sk_value@PLT
	movq	-336(%rbp), %rdi
	movq	%rax, %rsi
	movq	%rax, %r12
	call	pkey_ctrl_string@PLT
	testl	%eax, %eax
	jle	.L587
	addl	$1, %ebx
.L349:
	movq	%r13, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jl	.L351
	movq	-672(%rbp), %r8
	movq	-808(%rbp), %rcx
	movq	%r14, %rdx
	movq	-616(%rbp), %rsi
	movq	-784(%rbp), %rdi
	call	OCSP_basic_sign_ctx@PLT
	testl	%eax, %eax
	je	.L588
	movl	-724(%rbp), %r10d
	testl	%r10d, %r10d
	jne	.L589
.L353:
	movq	-784(%rbp), %rsi
	xorl	%edi, %edi
	call	OCSP_response_create@PLT
	movq	%rax, %r12
.L331:
	movq	%r14, %rdi
	call	EVP_MD_CTX_free@PLT
	movq	-800(%rbp), %rdi
	call	ASN1_TIME_free@PLT
	movq	-792(%rbp), %rdi
	call	ASN1_TIME_free@PLT
	movq	-784(%rbp), %rdi
	call	OCSP_BASICRESP_free@PLT
	movq	-448(%rbp), %r13
	testq	%r13, %r13
	je	.L316
	movabsq	$3471766442030158920, %rax
	movdqa	.LC82(%rip), %xmm0
	movb	$0, -64(%rbp)
	movabsq	$957946345412375072, %rdx
	movq	%rax, -144(%rbp)
	movabsq	$8389754706581209866, %rax
	movq	%rdx, -136(%rbp)
	movabsq	$6998658028286866477, %rdx
	movq	%rax, -128(%rbp)
	movabsq	$7598805550879240304, %rax
	movq	%rdx, -120(%rbp)
	movabsq	$3274243800002686575, %rdx
	movq	%rax, -112(%rbp)
	movabsq	$7310308045059482994, %rax
	movq	%rdx, -104(%rbp)
	movabsq	$7954892334481738253, %rdx
	movq	%rax, -96(%rbp)
	movq	%rdx, -88(%rbp)
	movaps	%xmm0, -80(%rbp)
.L549:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	i2d_OCSP_RESPONSE@PLT
	leaq	-144(%rbp), %rsi
	movq	%r13, %rdi
	movl	%eax, %edx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	i2d_OCSP_RESPONSE@GOTPCREL(%rip), %rdi
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	ASN1_i2d_bio@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	movq	%r13, %rdi
	call	BIO_ctrl@PLT
.L316:
	movq	-696(%rbp), %rax
	testq	%rax, %rax
	je	.L355
	movl	$4, %edx
	movl	$119, %esi
	movq	%rax, %rdi
	call	bio_open_default@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L427
	movq	i2d_OCSP_RESPONSE@GOTPCREL(%rip), %rdi
	movq	%r12, %rdx
	movq	%rax, %rsi
	call	ASN1_i2d_bio@PLT
	movq	%r14, %rdi
	call	BIO_free@PLT
.L355:
	movq	%r12, %rdi
	call	OCSP_response_status@PLT
	movl	%eax, %r13d
	testl	%eax, %eax
	jne	.L356
.L359:
	movl	-640(%rbp), %edi
	testl	%edi, %edi
	jne	.L590
.L358:
	movq	-448(%rbp), %r14
	testq	%r14, %r14
	je	.L360
	movl	-464(%rbp), %eax
	cmpl	$-1, %eax
	je	.L362
	subl	$1, %eax
	movl	%eax, -464(%rbp)
	testl	%eax, %eax
	jle	.L591
.L362:
	movq	%r14, %rdi
	call	BIO_free_all@PLT
	movq	-424(%rbp), %rdi
	movq	$0, -448(%rbp)
	call	OCSP_REQUEST_free@PLT
	movq	%r12, %rdi
	movq	$0, -424(%rbp)
	call	OCSP_RESPONSE_free@PLT
	jmp	.L304
	.p2align 4,,10
	.p2align 3
.L336:
	leaq	-328(%rbp), %rcx
	movq	%r13, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	OCSP_id_get0_info@PLT
	movq	-328(%rbp), %rdi
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	movaps	%xmm0, -288(%rbp)
	movaps	%xmm0, -272(%rbp)
	movaps	%xmm0, -256(%rbp)
	call	ASN1_INTEGER_to_BN@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L592
	movq	%rax, %rdi
	call	BN_is_zero@PLT
	testl	%eax, %eax
	je	.L339
	movl	$1277, %edx
	leaq	.LC1(%rip), %rsi
	leaq	.LC64(%rip), %rdi
	call	CRYPTO_strdup@PLT
	movq	%rax, %rbx
.L340:
	movq	%r14, %rdi
	movq	%rbx, -264(%rbp)
	call	BN_free@PLT
	movq	-480(%rbp), %rax
	movl	$3, %esi
	leaq	-288(%rbp), %rdx
	movq	8(%rax), %rdi
	call	TXT_DB_get_by_index@PLT
	movl	$1283, %edx
	leaq	.LC1(%rip), %rsi
	movq	%rbx, %rdi
	movq	%rax, %r14
	call	CRYPTO_free@PLT
	testq	%r14, %r14
	je	.L547
	movq	(%r14), %rax
	movzbl	(%rax), %eax
	cmpb	$86, %al
	je	.L593
	cmpb	$82, %al
	jne	.L337
	movq	16(%r14), %r8
	leaq	-296(%rbp), %rcx
	leaq	-312(%rbp), %rdx
	movq	$0, -312(%rbp)
	leaq	-452(%rbp), %rsi
	leaq	-304(%rbp), %rdi
	movq	$0, -304(%rbp)
	movq	$0, -296(%rbp)
	movl	$-1, -452(%rbp)
	call	unpack_revinfo@PLT
	subq	$8, %rsp
	movq	%r13, %rsi
	pushq	-792(%rbp)
	movq	-784(%rbp), %rdi
	movl	$1, %edx
	movq	-800(%rbp), %r9
	movq	-304(%rbp), %r8
	movl	-452(%rbp), %ecx
	call	OCSP_basic_add1_status@PLT
	movq	-296(%rbp), %rdx
	popq	%r11
	movq	%rax, %rdi
	popq	%rbx
	testq	%rdx, %rdx
	je	.L344
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$142, %esi
	call	OCSP_SINGLERESP_add1_ext_i2d@PLT
	movq	-312(%rbp), %r9
.L345:
	movq	%r9, %rdi
	call	ASN1_OBJECT_free@PLT
	movq	-304(%rbp), %rdi
	call	ASN1_TIME_free@PLT
	movq	-296(%rbp), %rdi
	call	ASN1_GENERALIZEDTIME_free@PLT
	jmp	.L337
	.p2align 4,,10
	.p2align 3
.L339:
	movq	%r14, %rdi
	call	BN_bn2hex@PLT
	movq	%rax, %rbx
	jmp	.L340
.L305:
	movq	-424(%rbp), %r12
	testq	%r12, %r12
	jne	.L314
	cmpq	$0, -832(%rbp)
	jne	.L317
	cmpq	$0, -376(%rbp)
	jne	.L317
	movl	-636(%rbp), %eax
	testl	%eax, %eax
	je	.L318
.L317:
	movq	bio_err(%rip), %rdi
	leaq	.LC57(%rip), %rsi
	xorl	%eax, %eax
	xorl	%r14d, %r14d
	movq	%r12, -472(%rbp)
	call	BIO_printf@PLT
	movq	$0, -488(%rbp)
	movq	$0, -528(%rbp)
	movl	$1, -496(%rbp)
	jmp	.L170
	.p2align 4,,10
	.p2align 3
.L329:
	movq	-376(%rbp), %rsi
	testq	%rsi, %rsi
	je	.L354
	movl	-648(%rbp), %eax
	subq	$8, %rsp
	movq	-416(%rbp), %r9
	movl	-460(%rbp), %r8d
	movq	-368(%rbp), %rcx
	movq	-360(%rbp), %rdx
	movq	-424(%rbp), %rdi
	pushq	%rax
	call	process_responder
	popq	%r8
	popq	%r9
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L316
.L420:
	movq	%r12, -472(%rbp)
	xorl	%r14d, %r14d
	movq	$0, -488(%rbp)
	movl	$1, -496(%rbp)
	jmp	.L170
	.p2align 4,,10
	.p2align 3
.L593:
	subq	$8, %rsp
	pushq	-792(%rbp)
	movq	%r13, %rsi
	xorl	%r8d, %r8d
	movq	-800(%rbp), %r9
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	-784(%rbp), %rdi
	call	OCSP_basic_add1_status@PLT
	popq	%r13
	popq	%r14
	jmp	.L337
.L587:
	movq	-824(%rbp), %rdi
	movq	%r12, %rdx
	leaq	.LC65(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
.L548:
	xorl	%esi, %esi
	movl	$2, %edi
	call	OCSP_response_create@PLT
	movq	%rax, %r12
	jmp	.L331
.L590:
	movq	-576(%rbp), %rdi
	xorl	%edx, %edx
	movq	%r12, %rsi
	call	OCSP_RESPONSE_print@PLT
	jmp	.L358
.L356:
	movslq	%eax, %rdi
	call	OCSP_response_status_str@PLT
	movq	-576(%rbp), %rdi
	leaq	.LC67(%rip), %rsi
	movl	%r13d, %ecx
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movl	-768(%rbp), %esi
	testl	%esi, %esi
	jne	.L359
	movq	%r12, -472(%rbp)
	xorl	%r14d, %r14d
	movl	$1, -496(%rbp)
	movq	$0, -488(%rbp)
	jmp	.L170
	.p2align 4,,10
	.p2align 3
.L319:
	xorl	%esi, %esi
	movl	$-1, %edx
	movq	%r12, %rdi
	call	OCSP_request_add1_nonce@PLT
	testl	%eax, %eax
	jne	.L322
.L419:
	movq	$0, -472(%rbp)
	xorl	%r14d, %r14d
	movl	$1, -496(%rbp)
	movq	$0, -488(%rbp)
	jmp	.L170
	.p2align 4,,10
	.p2align 3
.L354:
	movq	-488(%rbp), %rax
	testq	%rax, %rax
	je	.L424
	movl	$4, %edx
	movl	$114, %esi
	movq	%rax, %rdi
	call	bio_open_default@PLT
	movq	%rax, -480(%rbp)
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L425
	movq	OCSP_RESPONSE_new@GOTPCREL(%rip), %rdi
	movq	d2i_OCSP_RESPONSE@GOTPCREL(%rip), %rsi
	xorl	%ecx, %ecx
	movq	%rax, %rdx
	call	ASN1_d2i_bio@PLT
	movq	%rbx, %rdi
	movq	%rax, %r12
	call	BIO_free@PLT
	testq	%r12, %r12
	je	.L594
	movq	$0, -480(%rbp)
	jmp	.L316
.L405:
	movq	$0, -472(%rbp)
	jmp	.L406
.L570:
	testl	%r12d, %r12d
	jle	.L415
	movq	%r14, %rcx
	xorl	%edx, %edx
	jmp	.L281
.L280:
	addl	$1, %edx
	addq	$4, %rcx
	cmpl	%edx, %r12d
	je	.L279
.L281:
	cmpl	(%rcx), %eax
	jne	.L280
	movl	$0, (%rcx)
	subl	$1, %r12d
.L279:
	cmpl	%edx, multi(%rip)
	jle	.L595
	movl	-296(%rbp), %edx
	testl	%edx, %edx
	je	.L283
	movl	%edx, %r8d
	andl	$127, %r8d
	je	.L596
	leal	1(%r8), %ecx
	subb	$1, %cl
	jle	.L285
	andb	$-128, %dl
	movslq	%eax, %rcx
	movl	$1, %esi
	movl	$4, %edi
	leaq	.LC25(%rip), %rax
	leaq	.LC26(%rip), %r9
	cmovne	%rax, %r9
	leaq	.LC50(%rip), %rdx
	xorl	%eax, %eax
	call	__syslog_chk@PLT
.L285:
	movl	$1, %edi
	call	sleep@PLT
.L283:
	movl	termsig(%rip), %ecx
	testl	%ecx, %ecx
	jne	.L276
.L290:
	call	fork@PLT
	movl	%eax, %edx
	cmpl	$-1, %eax
	je	.L291
	testl	%eax, %eax
	je	.L292
	movl	multi(%rip), %ecx
	testl	%ecx, %ecx
	jle	.L294
	subl	$1, %ecx
	movq	%r14, %rax
	leaq	4(%r14,%rcx,4), %rcx
	jmp	.L299
.L298:
	addq	$4, %rax
	cmpq	%rax, %rcx
	je	.L294
.L299:
	movl	(%rax), %esi
	testl	%esi, %esi
	jne	.L298
	movl	%edx, (%rax)
	addl	$1, %r12d
	jmp	.L295
.L585:
	xorl	%esi, %esi
	movl	$1, %edi
	xorl	%r14d, %r14d
	call	OCSP_response_create@PLT
	movq	$0, -784(%rbp)
	movq	$0, -792(%rbp)
	movq	%rax, %r12
	movq	$0, -800(%rbp)
	jmp	.L331
.L344:
	movq	-312(%rbp), %r9
	testq	%r9, %r9
	je	.L345
	movq	%r9, %rdx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$430, %esi
	call	OCSP_SINGLERESP_add1_ext_i2d@PLT
	movq	-312(%rbp), %r9
	jmp	.L345
.L567:
	movq	bio_err(%rip), %rdi
	leaq	.LC41(%rip), %rsi
	xorl	%r14d, %r14d
	call	BIO_printf@PLT
	movq	$0, -472(%rbp)
	movq	$0, -488(%rbp)
	movq	$0, -544(%rbp)
	movq	$0, -504(%rbp)
	movq	$0, -560(%rbp)
	movq	$0, -480(%rbp)
	movl	$1, -496(%rbp)
	jmp	.L170
.L385:
	movq	bio_err(%rip), %rdi
	leaq	.LC45(%rip), %rsi
	xorl	%eax, %eax
	xorl	%r14d, %r14d
	call	BIO_printf@PLT
	movq	$0, -472(%rbp)
	movl	$1, -496(%rbp)
	movq	$0, -488(%rbp)
	movq	$0, -544(%rbp)
	movq	$0, -560(%rbp)
	movq	$0, -480(%rbp)
	jmp	.L170
.L581:
	movq	-480(%rbp), %rdi
	call	free_index@PLT
	movq	%r12, -480(%rbp)
	jmp	.L312
.L291:
	movl	$30, %edi
	call	sleep@PLT
	jmp	.L295
.L582:
	xorl	%esi, %esi
	movl	$1, %edi
	call	OCSP_response_create@PLT
	movq	-448(%rbp), %r13
	movb	$0, -64(%rbp)
	movabsq	$957946345412375072, %rdx
	movq	%rax, %r12
	movq	%rdx, -136(%rbp)
	movabsq	$3471766442030158920, %rax
	movabsq	$6998658028286866477, %rdx
	movq	%rax, -144(%rbp)
	movdqa	.LC82(%rip), %xmm0
	movabsq	$8389754706581209866, %rax
	movq	%rax, -128(%rbp)
	movabsq	$7598805550879240304, %rax
	movq	%rdx, -120(%rbp)
	movabsq	$3274243800002686575, %rdx
	movq	%rax, -112(%rbp)
	movabsq	$7310308045059482994, %rax
	movq	%rdx, -104(%rbp)
	movabsq	$7954892334481738253, %rdx
	movq	%rax, -96(%rbp)
	movq	%rdx, -88(%rbp)
	movaps	%xmm0, -80(%rbp)
	testq	%r13, %r13
	jne	.L549
	jmp	.L316
.L292:
	movl	$981, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r14, %rdi
	call	CRYPTO_free@PLT
	xorl	%esi, %esi
	movl	$2, %edi
	call	signal@PLT
	movl	$15, %edi
	xorl	%esi, %esi
	call	signal@PLT
	movl	termsig(%rip), %edi
	testl	%edi, %edi
	je	.L296
	xorl	%edi, %edi
	call	_exit@PLT
.L263:
	leaq	.LC38(%rip), %rsi
	movl	$3, %edi
	xorl	%eax, %eax
	call	log_message
	jmp	.L261
.L412:
	movq	$0, -472(%rbp)
	xorl	%r14d, %r14d
	movq	$0, -488(%rbp)
	movq	$0, -544(%rbp)
	movq	$0, -560(%rbp)
	movl	$1, -496(%rbp)
	jmp	.L170
.L568:
	movq	$0, -472(%rbp)
	xorl	%r14d, %r14d
	movq	$0, -488(%rbp)
	movq	$0, -544(%rbp)
	movq	$0, -560(%rbp)
	movq	$0, -480(%rbp)
	movl	$1, -496(%rbp)
	jmp	.L170
.L572:
	movq	prog(%rip), %rdx
	movq	bio_err(%rip), %rdi
	leaq	.LC29(%rip), %rsi
	call	BIO_printf@PLT
	movq	$0, -472(%rbp)
	movl	$1, -496(%rbp)
	movq	$0, -592(%rbp)
	movq	$0, -600(%rbp)
	movq	$0, -608(%rbp)
	jmp	.L553
.L573:
	movq	bio_err(%rip), %rdi
	leaq	.LC35(%rip), %rsi
	call	BIO_printf@PLT
	jmp	.L231
.L413:
	movq	$0, -472(%rbp)
	xorl	%r14d, %r14d
	movl	$1, -496(%rbp)
	movq	$0, -488(%rbp)
	movq	$0, -544(%rbp)
	movq	$0, -560(%rbp)
	jmp	.L170
.L580:
	cmpq	$0, -528(%rbp)
	jne	.L301
	jmp	.L272
.L407:
	xorl	%ebx, %ebx
	jmp	.L261
.L296:
	call	RAND_poll@PLT
	testl	%eax, %eax
	jle	.L597
	movl	-648(%rbp), %edx
	testl	%edx, %edx
	jle	.L301
	jmp	.L300
.L425:
	movq	$0, -472(%rbp)
	xorl	%r14d, %r14d
	movq	$0, -488(%rbp)
	movl	$1, -496(%rbp)
	jmp	.L170
.L360:
	cmpq	$0, -704(%rbp)
	movq	%r12, -472(%rbp)
	movq	$0, -488(%rbp)
	movl	-836(%rbp), %r13d
	jne	.L170
	movl	-748(%rbp), %ecx
	movl	-728(%rbp), %edx
	movq	-744(%rbp), %rsi
	movq	-736(%rbp), %rdi
	call	setup_verify@PLT
	movq	%rax, -488(%rbp)
	testq	%rax, %rax
	je	.L432
	cmpl	$0, -752(%rbp)
	jne	.L598
.L363:
	cmpq	$0, -664(%rbp)
	je	.L367
	movq	-664(%rbp), %rdi
	xorl	%ecx, %ecx
	leaq	-400(%rbp), %rsi
	leaq	.LC68(%rip), %r8
	movl	$32773, %edx
	call	load_certs@PLT
	testl	%eax, %eax
	je	.L432
.L367:
	movq	-472(%rbp), %rdi
	call	OCSP_response_get1_basic@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L599
	testl	%r13d, %r13d
	jne	.L433
	movq	-424(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L371
	movq	%rax, %rsi
	call	OCSP_check_nonce@PLT
	testl	%eax, %eax
	jle	.L600
.L371:
	movq	-488(%rbp), %rbx
	movq	-624(%rbp), %rcx
	movq	%r14, %rdi
	movq	-400(%rbp), %rsi
	movq	%rbx, %rdx
	call	OCSP_basic_verify@PLT
	testl	%eax, %eax
	jg	.L374
	movq	-568(%rbp), %rsi
	testq	%rsi, %rsi
	je	.L372
	movl	$512, %ecx
	movq	%rbx, %rdx
	movq	%r14, %rdi
	call	OCSP_basic_verify@PLT
	testl	%eax, %eax
	jg	.L373
.L375:
	movq	bio_err(%rip), %rdi
	leaq	.LC72(%rip), %rsi
	xorl	%eax, %eax
	movl	$1, %r13d
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
.L368:
	cmpq	$0, -424(%rbp)
	je	.L554
	movq	-520(%rbp), %rdi
	movq	-344(%rbp), %r12
	movq	-352(%rbp), %rbx
	call	OPENSSL_sk_num@PLT
	testl	%eax, %eax
	je	.L554
	movq	-512(%rbp), %rdi
	call	OPENSSL_sk_num@PLT
	testl	%eax, %eax
	je	.L554
	movq	%rbx, -624(%rbp)
	leaq	-304(%rbp), %r15
	movl	-496(%rbp), %ebx
	movl	%r13d, -628(%rbp)
	movq	-576(%rbp), %r13
	jmp	.L378
.L379:
	movq	-624(%rbp), %rdx
	movq	-296(%rbp), %rsi
	movq	%r12, %rcx
	movq	-304(%rbp), %rdi
	call	OCSP_check_validity@PLT
	testl	%eax, %eax
	je	.L601
.L381:
	movslq	-328(%rbp), %rdi
	call	OCSP_cert_status_str@PLT
	leaq	.LC76(%rip), %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	leaq	.LC77(%rip), %rsi
	movq	%r13, %rdi
	call	BIO_puts@PLT
	movq	-304(%rbp), %rsi
	movq	%r13, %rdi
	call	ASN1_GENERALIZEDTIME_print@PLT
	leaq	.LC3(%rip), %rsi
	movq	%r13, %rdi
	call	BIO_puts@PLT
	cmpq	$0, -296(%rbp)
	je	.L382
	leaq	.LC78(%rip), %rsi
	movq	%r13, %rdi
	call	BIO_puts@PLT
	movq	-296(%rbp), %rsi
	movq	%r13, %rdi
	call	ASN1_GENERALIZEDTIME_print@PLT
	leaq	.LC3(%rip), %rsi
	movq	%r13, %rdi
	call	BIO_puts@PLT
.L382:
	cmpl	$1, -328(%rbp)
	jne	.L380
	movl	-320(%rbp), %eax
	cmpl	$-1, %eax
	je	.L383
	movslq	%eax, %rdi
	call	OCSP_crl_reason_str@PLT
	leaq	.LC79(%rip), %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L383:
	leaq	.LC80(%rip), %rsi
	movq	%r13, %rdi
	call	BIO_puts@PLT
	movq	-312(%rbp), %rsi
	movq	%r13, %rdi
	call	ASN1_GENERALIZEDTIME_print@PLT
	leaq	.LC3(%rip), %rsi
	movq	%r13, %rdi
	call	BIO_puts@PLT
.L380:
	addl	$1, %ebx
.L378:
	movq	-512(%rbp), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jge	.L602
	movq	-512(%rbp), %rdi
	movl	%ebx, %esi
	call	OPENSSL_sk_value@PLT
	movq	-520(%rbp), %rdi
	movl	%ebx, %esi
	movq	%rax, -496(%rbp)
	call	OPENSSL_sk_value@PLT
	leaq	.LC2(%rip), %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	-496(%rbp), %r10
	movq	%r15, %r9
	movq	%r14, %rdi
	pushq	%rax
	leaq	-296(%rbp), %rax
	leaq	-320(%rbp), %rcx
	pushq	%rax
	leaq	-328(%rbp), %rdx
	leaq	-312(%rbp), %r8
	movq	%r10, %rsi
	call	OCSP_resp_find_status@PLT
	popq	%rdx
	popq	%rcx
	testl	%eax, %eax
	jne	.L379
	leaq	.LC74(%rip), %rsi
	movq	%r13, %rdi
	call	BIO_puts@PLT
	jmp	.L380
.L591:
	movq	%r12, -472(%rbp)
	xorl	%r14d, %r14d
	movq	$0, -488(%rbp)
	jmp	.L170
.L427:
	movq	%r12, -472(%rbp)
	movq	$0, -488(%rbp)
	movl	$1, -496(%rbp)
	jmp	.L170
.L589:
	movq	-784(%rbp), %rdi
	call	OCSP_resp_get0_signature@PLT
	movq	%rax, %rdi
	call	corrupt_signature@PLT
	jmp	.L353
.L588:
	movq	-784(%rbp), %rsi
	movl	$2, %edi
	call	OCSP_response_create@PLT
	movq	%rax, %r12
	jmp	.L331
.L583:
	movq	bio_err(%rip), %rdi
	leaq	.LC59(%rip), %rsi
	xorl	%r14d, %r14d
	call	BIO_printf@PLT
	movq	$0, -472(%rbp)
	movq	$0, -488(%rbp)
	movl	$1, -496(%rbp)
	jmp	.L170
.L564:
	call	__stack_chk_fail@PLT
.L596:
	movzbl	%dh, %edx
	movslq	%eax, %rcx
	movl	$1, %esi
	movl	$4, %edi
	movl	%edx, %r8d
	xorl	%eax, %eax
	leaq	.LC49(%rip), %rdx
	call	__syslog_chk@PLT
	jmp	.L285
.L584:
	movq	bio_err(%rip), %rdi
	leaq	.LC62(%rip), %rsi
	xorl	%r14d, %r14d
	call	BIO_printf@PLT
	movq	$0, -472(%rbp)
	movl	$1, -496(%rbp)
	movq	$0, -488(%rbp)
	jmp	.L170
.L597:
	movl	$3, %edi
	leaq	.LC52(%rip), %rdx
	movl	$1, %esi
	xorl	%eax, %eax
	call	__syslog_chk@PLT
	movl	$1, %edi
	call	_exit@PLT
.L595:
	movslq	%eax, %rcx
	leaq	.LC48(%rip), %rdx
.L555:
	movl	$1, %esi
	movl	$3, %edi
	xorl	%eax, %eax
	call	__syslog_chk@PLT
	movq	%r14, %rsi
	movl	$1, %edi
	call	killall
.L415:
	xorl	%edx, %edx
	jmp	.L279
.L287:
	call	strerror@PLT
	leaq	.LC51(%rip), %rdx
	movq	%rax, %rcx
	jmp	.L555
.L592:
	movl	$1274, %edx
	leaq	.LC1(%rip), %rsi
	leaq	.LC63(%rip), %rdi
	call	OPENSSL_die@PLT
.L372:
	testl	%eax, %eax
	jle	.L375
.L374:
	movq	bio_err(%rip), %rdi
	leaq	.LC73(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L368
.L433:
	xorl	%r13d, %r13d
	jmp	.L368
.L599:
	movq	bio_err(%rip), %rdi
	leaq	.LC69(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movl	$1, -496(%rbp)
	jmp	.L170
.L432:
	movl	$1, -496(%rbp)
	xorl	%r14d, %r14d
	jmp	.L170
.L569:
	call	__errno_location@PLT
	movl	(%rax), %edi
	call	strerror@PLT
	movl	$3, %edi
	movl	$1, %esi
	leaq	.LC46(%rip), %rdx
	movq	%rax, %rcx
	xorl	%eax, %eax
	call	__syslog_chk@PLT
	movl	$1, %edi
	call	exit@PLT
.L598:
	movq	-552(%rbp), %rsi
	movq	%rax, %rdi
	call	X509_STORE_set1_param@PLT
	jmp	.L363
.L600:
	addl	$1, %eax
	movq	bio_err(%rip), %rdi
	jne	.L370
	leaq	.LC70(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L371
.L601:
	movq	%r13, %rdi
	leaq	.LC75(%rip), %rsi
	call	BIO_puts@PLT
	movq	%r13, %rdi
	call	ERR_print_errors@PLT
	jmp	.L381
.L602:
	movl	-628(%rbp), %r13d
.L554:
	movl	%r13d, -496(%rbp)
	jmp	.L170
.L373:
	call	ERR_clear_error@PLT
	jmp	.L374
.L594:
	movq	bio_err(%rip), %rdi
	leaq	.LC66(%rip), %rsi
	xorl	%eax, %eax
	xorl	%r14d, %r14d
	movq	%r12, -472(%rbp)
	call	BIO_printf@PLT
	movq	$0, -488(%rbp)
	movq	$0, -480(%rbp)
	movl	$1, -496(%rbp)
	jmp	.L170
.L424:
	movq	-488(%rbp), %rax
	xorl	%r14d, %r14d
	movq	$0, -472(%rbp)
	movq	%rax, -480(%rbp)
	jmp	.L170
.L294:
	movl	$1, %esi
	movl	$3, %edi
	leaq	.LC81(%rip), %rdx
	xorl	%eax, %eax
	call	__syslog_chk@PLT
	movq	%r14, %rsi
	movl	$1, %edi
	call	killall
.L370:
	leaq	.LC71(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movl	$1, -496(%rbp)
	jmp	.L170
	.cfi_endproc
.LFE1562:
	.size	ocsp_main, .-ocsp_main
	.local	termsig
	.comm	termsig,4,4
	.globl	ocsp_options
	.section	.rodata.str1.1
.LC83:
	.string	"help"
.LC84:
	.string	"Display this summary"
.LC85:
	.string	"out"
.LC86:
	.string	"Output filename"
.LC87:
	.string	"timeout"
	.section	.rodata.str1.8
	.align 8
.LC88:
	.string	"Connection timeout (in seconds) to the OCSP responder"
	.section	.rodata.str1.1
.LC89:
	.string	"url"
.LC90:
	.string	"Responder URL"
	.section	.rodata.str1.8
	.align 8
.LC91:
	.string	"TCP/IP hostname:port to connect to"
	.section	.rodata.str1.1
.LC92:
	.string	"port"
.LC93:
	.string	"Port to run responder on"
.LC94:
	.string	"ignore_err"
	.section	.rodata.str1.8
	.align 8
.LC95:
	.string	"Ignore error on OCSP request or response and continue running"
	.section	.rodata.str1.1
.LC96:
	.string	"noverify"
.LC97:
	.string	"Don't verify response at all"
.LC98:
	.string	"nonce"
.LC99:
	.string	"Add OCSP nonce to request"
.LC100:
	.string	"no_nonce"
	.section	.rodata.str1.8
	.align 8
.LC101:
	.string	"Don't add OCSP nonce to request"
	.section	.rodata.str1.1
.LC102:
	.string	"resp_no_certs"
	.section	.rodata.str1.8
	.align 8
.LC103:
	.string	"Don't include any certificates in response"
	.section	.rodata.str1.1
.LC104:
	.string	"resp_key_id"
	.section	.rodata.str1.8
	.align 8
.LC105:
	.string	"Identify response by signing certificate key ID"
	.section	.rodata.str1.1
.LC106:
	.string	"multi"
	.section	.rodata.str1.8
	.align 8
.LC107:
	.string	"run multiple responder processes"
	.section	.rodata.str1.1
.LC108:
	.string	"no_certs"
	.section	.rodata.str1.8
	.align 8
.LC109:
	.string	"Don't include any certificates in signed request"
	.section	.rodata.str1.1
.LC110:
	.string	"no_signature_verify"
	.section	.rodata.str1.8
	.align 8
.LC111:
	.string	"Don't check signature on response"
	.section	.rodata.str1.1
.LC112:
	.string	"no_cert_verify"
	.section	.rodata.str1.8
	.align 8
.LC113:
	.string	"Don't check signing certificate"
	.section	.rodata.str1.1
.LC114:
	.string	"no_chain"
.LC115:
	.string	"Don't chain verify response"
.LC116:
	.string	"no_cert_checks"
	.section	.rodata.str1.8
	.align 8
.LC117:
	.string	"Don't do additional checks on signing certificate"
	.section	.rodata.str1.1
.LC118:
	.string	"no_explicit"
	.section	.rodata.str1.8
	.align 8
.LC119:
	.string	"Do not explicitly check the chain, just verify the root"
	.section	.rodata.str1.1
.LC120:
	.string	"trust_other"
	.section	.rodata.str1.8
	.align 8
.LC121:
	.string	"Don't verify additional certificates"
	.section	.rodata.str1.1
.LC122:
	.string	"no_intern"
	.section	.rodata.str1.8
	.align 8
.LC123:
	.string	"Don't search certificates contained in response for signer"
	.section	.rodata.str1.1
.LC124:
	.string	"badsig"
	.section	.rodata.str1.8
	.align 8
.LC125:
	.string	"Corrupt last byte of loaded OSCP response signature (for test)"
	.section	.rodata.str1.1
.LC126:
	.string	"text"
	.section	.rodata.str1.8
	.align 8
.LC127:
	.string	"Print text form of request and response"
	.section	.rodata.str1.1
.LC128:
	.string	"req_text"
.LC129:
	.string	"Print text form of request"
.LC130:
	.string	"resp_text"
.LC131:
	.string	"Print text form of response"
.LC132:
	.string	"reqin"
	.section	.rodata.str1.8
	.align 8
.LC133:
	.string	"File with the DER-encoded request"
	.section	.rodata.str1.1
.LC134:
	.string	"respin"
	.section	.rodata.str1.8
	.align 8
.LC135:
	.string	"File with the DER-encoded response"
	.section	.rodata.str1.1
.LC136:
	.string	"signer"
	.section	.rodata.str1.8
	.align 8
.LC137:
	.string	"Certificate to sign OCSP request with"
	.section	.rodata.str1.1
.LC138:
	.string	"VAfile"
.LC139:
	.string	"Validator certificates file"
.LC140:
	.string	"sign_other"
	.section	.rodata.str1.8
	.align 8
.LC141:
	.string	"Additional certificates to include in signed request"
	.section	.rodata.str1.1
.LC142:
	.string	"verify_other"
	.section	.rodata.str1.8
	.align 8
.LC143:
	.string	"Additional certificates to search for signer"
	.section	.rodata.str1.1
.LC144:
	.string	"CAfile"
.LC145:
	.string	"Trusted certificates file"
.LC146:
	.string	"CApath"
	.section	.rodata.str1.8
	.align 8
.LC147:
	.string	"Trusted certificates directory"
	.section	.rodata.str1.1
.LC148:
	.string	"no-CAfile"
	.section	.rodata.str1.8
	.align 8
.LC149:
	.string	"Do not load the default certificates file"
	.section	.rodata.str1.1
.LC150:
	.string	"no-CApath"
	.section	.rodata.str1.8
	.align 8
.LC151:
	.string	"Do not load certificates from the default certificates directory"
	.section	.rodata.str1.1
.LC152:
	.string	"validity_period"
	.section	.rodata.str1.8
	.align 8
.LC153:
	.string	"Maximum validity discrepancy in seconds"
	.section	.rodata.str1.1
.LC154:
	.string	"status_age"
.LC155:
	.string	"Maximum status age in seconds"
.LC156:
	.string	"signkey"
	.section	.rodata.str1.8
	.align 8
.LC157:
	.string	"Private key to sign OCSP request with"
	.section	.rodata.str1.1
.LC158:
	.string	"reqout"
	.section	.rodata.str1.8
	.align 8
.LC159:
	.string	"Output file for the DER-encoded request"
	.section	.rodata.str1.1
.LC160:
	.string	"respout"
	.section	.rodata.str1.8
	.align 8
.LC161:
	.string	"Output file for the DER-encoded response"
	.section	.rodata.str1.1
.LC162:
	.string	"path"
.LC163:
	.string	"Path to use in OCSP request"
.LC164:
	.string	"issuer"
.LC165:
	.string	"Issuer certificate"
.LC166:
	.string	"cert"
.LC167:
	.string	"Certificate to check"
.LC168:
	.string	"serial"
.LC169:
	.string	"Serial number to check"
.LC170:
	.string	"index"
.LC171:
	.string	"Certificate status index file"
.LC172:
	.string	"CA"
.LC173:
	.string	"nmin"
	.section	.rodata.str1.8
	.align 8
.LC174:
	.string	"Number of minutes before next update"
	.section	.rodata.str1.1
.LC175:
	.string	"nrequest"
	.section	.rodata.str1.8
	.align 8
.LC176:
	.string	"Number of requests to accept (default unlimited)"
	.section	.rodata.str1.1
.LC177:
	.string	"ndays"
	.section	.rodata.str1.8
	.align 8
.LC178:
	.string	"Number of days before next update"
	.section	.rodata.str1.1
.LC179:
	.string	"rsigner"
	.section	.rodata.str1.8
	.align 8
.LC180:
	.string	"Responder certificate to sign responses with"
	.section	.rodata.str1.1
.LC181:
	.string	"rkey"
	.section	.rodata.str1.8
	.align 8
.LC182:
	.string	"Responder key to sign responses with"
	.section	.rodata.str1.1
.LC183:
	.string	"rother"
	.section	.rodata.str1.8
	.align 8
.LC184:
	.string	"Other certificates to include in response"
	.section	.rodata.str1.1
.LC185:
	.string	"rmd"
	.section	.rodata.str1.8
	.align 8
.LC186:
	.string	"Digest Algorithm to use in signature of OCSP response"
	.section	.rodata.str1.1
.LC187:
	.string	"rsigopt"
	.section	.rodata.str1.8
	.align 8
.LC188:
	.string	"OCSP response signature parameter in n:v form"
	.section	.rodata.str1.1
.LC189:
	.string	"header"
.LC190:
	.string	"key=value header to add"
	.section	.rodata.str1.8
	.align 8
.LC191:
	.string	"Any supported digest algorithm (sha1,sha256, ... )"
	.section	.rodata.str1.1
.LC192:
	.string	"policy"
	.section	.rodata.str1.8
	.align 8
.LC193:
	.string	"adds policy to the acceptable policy set"
	.section	.rodata.str1.1
.LC194:
	.string	"purpose"
.LC195:
	.string	"certificate chain purpose"
.LC196:
	.string	"verify_name"
.LC197:
	.string	"verification policy name"
.LC198:
	.string	"verify_depth"
.LC199:
	.string	"chain depth limit"
.LC200:
	.string	"auth_level"
	.section	.rodata.str1.8
	.align 8
.LC201:
	.string	"chain authentication security level"
	.section	.rodata.str1.1
.LC202:
	.string	"attime"
.LC203:
	.string	"verification epoch time"
.LC204:
	.string	"verify_hostname"
.LC205:
	.string	"expected peer hostname"
.LC206:
	.string	"verify_email"
.LC207:
	.string	"expected peer email"
.LC208:
	.string	"verify_ip"
.LC209:
	.string	"expected peer IP address"
.LC210:
	.string	"ignore_critical"
	.section	.rodata.str1.8
	.align 8
.LC211:
	.string	"permit unhandled critical extensions"
	.section	.rodata.str1.1
.LC212:
	.string	"issuer_checks"
.LC213:
	.string	"(deprecated)"
.LC214:
	.string	"crl_check"
	.section	.rodata.str1.8
	.align 8
.LC215:
	.string	"check leaf certificate revocation"
	.section	.rodata.str1.1
.LC216:
	.string	"crl_check_all"
.LC217:
	.string	"check full chain revocation"
.LC218:
	.string	"policy_check"
.LC219:
	.string	"perform rfc5280 policy checks"
.LC220:
	.string	"explicit_policy"
	.section	.rodata.str1.8
	.align 8
.LC221:
	.string	"set policy variable require-explicit-policy"
	.section	.rodata.str1.1
.LC222:
	.string	"inhibit_any"
	.section	.rodata.str1.8
	.align 8
.LC223:
	.string	"set policy variable inhibit-any-policy"
	.section	.rodata.str1.1
.LC224:
	.string	"inhibit_map"
	.section	.rodata.str1.8
	.align 8
.LC225:
	.string	"set policy variable inhibit-policy-mapping"
	.section	.rodata.str1.1
.LC226:
	.string	"x509_strict"
	.section	.rodata.str1.8
	.align 8
.LC227:
	.string	"disable certificate compatibility work-arounds"
	.section	.rodata.str1.1
.LC228:
	.string	"extended_crl"
.LC229:
	.string	"enable extended CRL features"
.LC230:
	.string	"use_deltas"
.LC231:
	.string	"use delta CRLs"
.LC232:
	.string	"policy_print"
	.section	.rodata.str1.8
	.align 8
.LC233:
	.string	"print policy processing diagnostics"
	.section	.rodata.str1.1
.LC234:
	.string	"check_ss_sig"
.LC235:
	.string	"check root CA self-signatures"
.LC236:
	.string	"trusted_first"
	.section	.rodata.str1.8
	.align 8
.LC237:
	.string	"search trust store first (default)"
	.section	.rodata.str1.1
.LC238:
	.string	"suiteB_128_only"
.LC239:
	.string	"Suite B 128-bit-only mode"
.LC240:
	.string	"suiteB_128"
	.section	.rodata.str1.8
	.align 8
.LC241:
	.string	"Suite B 128-bit mode allowing 192-bit algorithms"
	.section	.rodata.str1.1
.LC242:
	.string	"suiteB_192"
.LC243:
	.string	"Suite B 192-bit-only mode"
.LC244:
	.string	"partial_chain"
	.section	.rodata.str1.8
	.align 8
.LC245:
	.string	"accept chains anchored by intermediate trust-store CAs"
	.section	.rodata.str1.1
.LC246:
	.string	"no_alt_chains"
.LC247:
	.string	"no_check_time"
	.section	.rodata.str1.8
	.align 8
.LC248:
	.string	"ignore certificate validity time"
	.section	.rodata.str1.1
.LC249:
	.string	"allow_proxy_certs"
	.section	.rodata.str1.8
	.align 8
.LC250:
	.string	"allow the use of proxy certificates"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	ocsp_options, @object
	.size	ocsp_options, 2088
ocsp_options:
	.quad	.LC83
	.long	1
	.long	45
	.quad	.LC84
	.quad	.LC85
	.long	2
	.long	62
	.quad	.LC86
	.quad	.LC87
	.long	3
	.long	112
	.quad	.LC88
	.quad	.LC89
	.long	4
	.long	115
	.quad	.LC90
	.quad	.LC19
	.long	5
	.long	115
	.quad	.LC91
	.quad	.LC92
	.long	6
	.long	112
	.quad	.LC93
	.quad	.LC94
	.long	7
	.long	45
	.quad	.LC95
	.quad	.LC96
	.long	8
	.long	45
	.quad	.LC97
	.quad	.LC98
	.long	9
	.long	45
	.quad	.LC99
	.quad	.LC100
	.long	10
	.long	45
	.quad	.LC101
	.quad	.LC102
	.long	11
	.long	45
	.quad	.LC103
	.quad	.LC104
	.long	12
	.long	45
	.quad	.LC105
	.quad	.LC106
	.long	2033
	.long	112
	.quad	.LC107
	.quad	.LC108
	.long	13
	.long	45
	.quad	.LC109
	.quad	.LC110
	.long	14
	.long	45
	.quad	.LC111
	.quad	.LC112
	.long	15
	.long	45
	.quad	.LC113
	.quad	.LC114
	.long	16
	.long	45
	.quad	.LC115
	.quad	.LC116
	.long	17
	.long	45
	.quad	.LC117
	.quad	.LC118
	.long	18
	.long	45
	.quad	.LC119
	.quad	.LC120
	.long	19
	.long	45
	.quad	.LC121
	.quad	.LC122
	.long	20
	.long	45
	.quad	.LC123
	.quad	.LC124
	.long	21
	.long	45
	.quad	.LC125
	.quad	.LC126
	.long	22
	.long	45
	.quad	.LC127
	.quad	.LC128
	.long	23
	.long	45
	.quad	.LC129
	.quad	.LC130
	.long	24
	.long	45
	.quad	.LC131
	.quad	.LC132
	.long	25
	.long	115
	.quad	.LC133
	.quad	.LC134
	.long	26
	.long	115
	.quad	.LC135
	.quad	.LC136
	.long	27
	.long	60
	.quad	.LC137
	.quad	.LC138
	.long	28
	.long	60
	.quad	.LC139
	.quad	.LC140
	.long	29
	.long	60
	.quad	.LC141
	.quad	.LC142
	.long	30
	.long	60
	.quad	.LC143
	.quad	.LC144
	.long	31
	.long	60
	.quad	.LC145
	.quad	.LC146
	.long	32
	.long	60
	.quad	.LC147
	.quad	.LC148
	.long	33
	.long	45
	.quad	.LC149
	.quad	.LC150
	.long	34
	.long	45
	.quad	.LC151
	.quad	.LC152
	.long	35
	.long	117
	.quad	.LC153
	.quad	.LC154
	.long	36
	.long	112
	.quad	.LC155
	.quad	.LC156
	.long	37
	.long	115
	.quad	.LC157
	.quad	.LC158
	.long	38
	.long	115
	.quad	.LC159
	.quad	.LC160
	.long	39
	.long	115
	.quad	.LC161
	.quad	.LC162
	.long	40
	.long	115
	.quad	.LC163
	.quad	.LC164
	.long	41
	.long	60
	.quad	.LC165
	.quad	.LC166
	.long	42
	.long	60
	.quad	.LC167
	.quad	.LC168
	.long	43
	.long	115
	.quad	.LC169
	.quad	.LC170
	.long	44
	.long	60
	.quad	.LC171
	.quad	.LC172
	.long	45
	.long	60
	.quad	.LC42
	.quad	.LC173
	.long	46
	.long	112
	.quad	.LC174
	.quad	.LC175
	.long	47
	.long	112
	.quad	.LC176
	.quad	.LC177
	.long	48
	.long	112
	.quad	.LC178
	.quad	.LC179
	.long	49
	.long	60
	.quad	.LC180
	.quad	.LC181
	.long	50
	.long	60
	.quad	.LC182
	.quad	.LC183
	.long	51
	.long	60
	.quad	.LC184
	.quad	.LC185
	.long	52
	.long	115
	.quad	.LC186
	.quad	.LC187
	.long	53
	.long	115
	.quad	.LC188
	.quad	.LC189
	.long	54
	.long	115
	.quad	.LC190
	.quad	.LC26
	.long	2032
	.long	45
	.quad	.LC191
	.quad	.LC192
	.long	2001
	.long	115
	.quad	.LC193
	.quad	.LC194
	.long	2002
	.long	115
	.quad	.LC195
	.quad	.LC196
	.long	2003
	.long	115
	.quad	.LC197
	.quad	.LC198
	.long	2004
	.long	110
	.quad	.LC199
	.quad	.LC200
	.long	2029
	.long	110
	.quad	.LC201
	.quad	.LC202
	.long	2005
	.long	77
	.quad	.LC203
	.quad	.LC204
	.long	2006
	.long	115
	.quad	.LC205
	.quad	.LC206
	.long	2007
	.long	115
	.quad	.LC207
	.quad	.LC208
	.long	2008
	.long	115
	.quad	.LC209
	.quad	.LC210
	.long	2009
	.long	45
	.quad	.LC211
	.quad	.LC212
	.long	2010
	.long	45
	.quad	.LC213
	.quad	.LC214
	.long	2011
	.long	45
	.quad	.LC215
	.quad	.LC216
	.long	2012
	.long	45
	.quad	.LC217
	.quad	.LC218
	.long	2013
	.long	45
	.quad	.LC219
	.quad	.LC220
	.long	2014
	.long	45
	.quad	.LC221
	.quad	.LC222
	.long	2015
	.long	45
	.quad	.LC223
	.quad	.LC224
	.long	2016
	.long	45
	.quad	.LC225
	.quad	.LC226
	.long	2017
	.long	45
	.quad	.LC227
	.quad	.LC228
	.long	2018
	.long	45
	.quad	.LC229
	.quad	.LC230
	.long	2019
	.long	45
	.quad	.LC231
	.quad	.LC232
	.long	2020
	.long	45
	.quad	.LC233
	.quad	.LC234
	.long	2021
	.long	45
	.quad	.LC235
	.quad	.LC236
	.long	2022
	.long	45
	.quad	.LC237
	.quad	.LC238
	.long	2023
	.long	45
	.quad	.LC239
	.quad	.LC240
	.long	2024
	.long	45
	.quad	.LC241
	.quad	.LC242
	.long	2025
	.long	45
	.quad	.LC243
	.quad	.LC244
	.long	2026
	.long	45
	.quad	.LC245
	.quad	.LC246
	.long	2027
	.long	45
	.quad	.LC213
	.quad	.LC247
	.long	2028
	.long	45
	.quad	.LC248
	.quad	.LC249
	.long	2030
	.long	45
	.quad	.LC250
	.quad	0
	.zero	16
	.data
	.align 4
	.type	acfd, @object
	.size	acfd, 4
acfd:
	.long	-1
	.local	multi
	.comm	multi,4,4
	.local	prog
	.comm	prog,8,8
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC82:
	.quad	7526754601086496116
	.quad	724246167707525178
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
