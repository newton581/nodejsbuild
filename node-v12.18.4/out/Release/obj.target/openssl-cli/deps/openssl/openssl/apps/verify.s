	.file	"verify.c"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"[CRL path] "
.LC1:
	.string	""
.LC2:
	.string	"\n"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"%serror %d at %d depth lookup: %s\n"
	.text
	.p2align 4
	.type	cb, @function
cb:
.LFB1437:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edi, %r14d
	movq	%rsi, %rdi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	call	X509_STORE_CTX_get_error@PLT
	movq	%r13, %rdi
	movl	%eax, %ebx
	call	X509_STORE_CTX_get_current_cert@PLT
	testl	%r14d, %r14d
	je	.L23
	testl	%ebx, %ebx
	jne	.L9
	cmpl	$2, %r14d
	jne	.L9
	movq	%r13, %rdi
	call	policies_print@PLT
.L9:
	movl	v_verbose(%rip), %eax
	testl	%eax, %eax
	je	.L24
.L5:
	addq	$8, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	.cfi_restore_state
	call	ERR_clear_error@PLT
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L23:
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L3
	call	get_nameopt@PLT
	movq	%r12, %rdi
	movq	%rax, %r15
	call	X509_get_subject_name@PLT
	movq	bio_err(%rip), %rdi
	movq	%r15, %rcx
	xorl	%edx, %edx
	movq	%rax, %rsi
	call	X509_NAME_print_ex@PLT
	movq	bio_err(%rip), %rdi
	leaq	.LC2(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L3:
	movslq	%ebx, %rdi
	call	X509_verify_cert_error_string@PLT
	movq	%r13, %rdi
	movq	%rax, %r15
	call	X509_STORE_CTX_get_error_depth@PLT
	movq	%r13, %rdi
	movl	%eax, %r12d
	call	X509_STORE_CTX_get0_parent_ctx@PLT
	movl	%ebx, %ecx
	movq	%r15, %r9
	movl	%r12d, %r8d
	testq	%rax, %rax
	leaq	.LC0(%rip), %rdx
	leaq	.LC1(%rip), %rax
	movq	bio_err(%rip), %rdi
	cmove	%rax, %rdx
	leaq	.LC3(%rip), %rsi
	xorl	%eax, %eax
	subl	$10, %ebx
	call	BIO_printf@PLT
	cmpl	$33, %ebx
	ja	.L5
	leaq	.L7(%rip), %rdx
	movslq	(%rdx,%rbx,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L7:
	.long	.L21-.L7
	.long	.L21-.L7
	.long	.L21-.L7
	.long	.L5-.L7
	.long	.L5-.L7
	.long	.L5-.L7
	.long	.L5-.L7
	.long	.L5-.L7
	.long	.L21-.L7
	.long	.L5-.L7
	.long	.L5-.L7
	.long	.L5-.L7
	.long	.L5-.L7
	.long	.L5-.L7
	.long	.L21-.L7
	.long	.L21-.L7
	.long	.L21-.L7
	.long	.L5-.L7
	.long	.L5-.L7
	.long	.L5-.L7
	.long	.L5-.L7
	.long	.L5-.L7
	.long	.L5-.L7
	.long	.L5-.L7
	.long	.L21-.L7
	.long	.L5-.L7
	.long	.L5-.L7
	.long	.L21-.L7
	.long	.L5-.L7
	.long	.L5-.L7
	.long	.L5-.L7
	.long	.L5-.L7
	.long	.L5-.L7
	.long	.L6-.L7
	.text
.L6:
	movq	%r13, %rdi
	call	policies_print@PLT
.L21:
	movl	$1, %r14d
	jmp	.L5
	.cfi_endproc
.LFE1437:
	.size	cb, .-cb
	.section	.rodata.str1.1
.LC4:
	.string	"stdin"
.LC5:
	.string	"certificate file"
	.section	.rodata.str1.8
	.align 8
.LC6:
	.string	"error %s: X.509 store context allocation failed\n"
	.align 8
.LC7:
	.string	"error %s: X.509 store context initialization failed\n"
	.section	.rodata.str1.1
.LC8:
	.string	"%s: OK\n"
.LC9:
	.string	"Chain:"
.LC10:
	.string	"depth=%d: "
.LC11:
	.string	" (untrusted)"
	.section	.rodata.str1.8
	.align 8
.LC12:
	.string	"error %s: verification failed\n"
	.text
	.p2align 4
	.type	check, @function
check:
.LFB1436:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movl	$32773, %esi
	pushq	%rbx
	movq	%r12, %rdi
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$40, %rsp
	movq	%rdx, -56(%rbp)
	leaq	.LC5(%rip), %rdx
	movq	%rcx, -64(%rbp)
	movl	%r9d, -68(%rbp)
	call	load_cert@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L27
	call	X509_STORE_CTX_new@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L59
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	X509_STORE_set_flags@PLT
	movq	-56(%rbp), %rcx
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	X509_STORE_CTX_init@PLT
	testl	%eax, %eax
	je	.L60
	movq	-64(%rbp), %rax
	testq	%rax, %rax
	je	.L32
	movq	%rax, %rsi
	movq	%r14, %rdi
	call	X509_STORE_CTX_set0_trusted_stack@PLT
.L32:
	testq	%rbx, %rbx
	je	.L33
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	X509_STORE_CTX_set0_crls@PLT
.L33:
	movq	%r14, %rdi
	call	X509_verify_cert@PLT
	testl	%eax, %eax
	jg	.L61
	movq	%r12, %rdx
	testq	%r12, %r12
	je	.L62
.L58:
	movl	$1, %edi
	leaq	.LC12(%rip), %rsi
	xorl	%eax, %eax
	call	__printf_chk@PLT
	movq	%r14, %rdi
	call	X509_STORE_CTX_free@PLT
.L27:
	movq	bio_err(%rip), %rdi
	xorl	%r12d, %r12d
	call	ERR_print_errors@PLT
.L41:
	movq	%r13, %rdi
	call	X509_free@PLT
	addq	$40, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L59:
	.cfi_restore_state
	testq	%r12, %r12
	leaq	.LC4(%rip), %rax
	movl	$1, %edi
	cmove	%rax, %r12
	leaq	.LC6(%rip), %rsi
	xorl	%eax, %eax
	movq	%r12, %rdx
	call	__printf_chk@PLT
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L62:
	leaq	.LC4(%rip), %rdx
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L61:
	movq	%r14, %rdi
	call	X509_STORE_CTX_get_error@PLT
	movl	%eax, %ebx
	testl	%eax, %eax
	jne	.L35
	testq	%r12, %r12
	leaq	.LC4(%rip), %rax
	movl	$1, %edi
	cmove	%rax, %r12
	leaq	.LC8(%rip), %rsi
	xorl	%eax, %eax
	movq	%r12, %rdx
	call	__printf_chk@PLT
	movl	-68(%rbp), %eax
	testl	%eax, %eax
	jne	.L63
.L37:
	movq	%r14, %rdi
	movl	$1, %r12d
	call	X509_STORE_CTX_free@PLT
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L60:
	movq	%r14, %rdi
	call	X509_STORE_CTX_free@PLT
	testq	%r12, %r12
	movl	$1, %edi
	leaq	.LC4(%rip), %rax
	cmove	%rax, %r12
	leaq	.LC7(%rip), %rsi
	xorl	%eax, %eax
	movq	%r12, %rdx
	call	__printf_chk@PLT
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L35:
	testq	%r12, %r12
	je	.L64
	movq	%r12, %rdx
	leaq	.LC12(%rip), %rsi
	movl	$1, %edi
	xorl	%eax, %eax
	call	__printf_chk@PLT
	movq	%r14, %rdi
	xorl	%r12d, %r12d
	call	X509_STORE_CTX_free@PLT
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L63:
	movq	%r14, %rdi
	call	X509_STORE_CTX_get1_chain@PLT
	movq	%r14, %rdi
	movq	%rax, %r12
	call	X509_STORE_CTX_get_num_untrusted@PLT
	leaq	.LC9(%rip), %rdi
	movl	%eax, -64(%rbp)
	call	puts@PLT
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L39:
	movl	$10, %edi
	addl	$1, %ebx
	call	putchar@PLT
.L38:
	movq	%r12, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jge	.L65
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	OPENSSL_sk_value@PLT
	movl	%ebx, %edx
	movl	$1, %edi
	leaq	.LC10(%rip), %rsi
	movq	%rax, %r15
	xorl	%eax, %eax
	call	__printf_chk@PLT
	call	get_nameopt@PLT
	movq	%r15, %rdi
	movq	%rax, -56(%rbp)
	call	X509_get_subject_name@PLT
	movq	-56(%rbp), %rcx
	movq	stdout(%rip), %rdi
	xorl	%edx, %edx
	movq	%rax, %rsi
	call	X509_NAME_print_ex_fp@PLT
	cmpl	-64(%rbp), %ebx
	jge	.L39
	leaq	.LC11(%rip), %rsi
	movl	$1, %edi
	xorl	%eax, %eax
	call	__printf_chk@PLT
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L65:
	movq	X509_free@GOTPCREL(%rip), %rsi
	movq	%r12, %rdi
	call	OPENSSL_sk_pop_free@PLT
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L64:
	leaq	.LC4(%rip), %rdx
	leaq	.LC12(%rip), %rsi
	movl	$1, %edi
	xorl	%eax, %eax
	call	__printf_chk@PLT
	movq	%r14, %rdi
	call	X509_STORE_CTX_free@PLT
	jmp	.L41
	.cfi_endproc
.LFE1436:
	.size	check, .-check
	.section	.rodata.str1.1
.LC13:
	.string	"%s: Use -help for summary.\n"
.LC14:
	.string	"Recognized usages:\n"
.LC15:
	.string	"\t%-10s\t%s\n"
.LC16:
	.string	"Recognized verify names:\n"
.LC17:
	.string	"\t%-10s\n"
.LC18:
	.string	"untrusted certificates"
.LC19:
	.string	"trusted certificates"
.LC20:
	.string	"other CRLs"
	.section	.rodata.str1.8
	.align 8
.LC21:
	.string	"%s: Cannot use -trusted with -CAfile or -CApath\n"
	.text
	.p2align 4
	.globl	verify_main
	.type	verify_main, @function
verify_main:
.LFB1435:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%edi, %r12d
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movq	$0, -72(%rbp)
	movq	$0, -64(%rbp)
	call	X509_VERIFY_PARAM_new@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L67
	movq	%r13, %rsi
	movl	%r12d, %edi
	leaq	verify_options(%rip), %rdx
	xorl	%r15d, %r15d
	call	opt_init@PLT
	movl	$0, -112(%rbp)
	xorl	%r12d, %r12d
	xorl	%r13d, %r13d
	movq	%rax, -120(%rbp)
	leaq	.L73(%rip), %rbx
	movl	$0, -108(%rbp)
	movq	$0, -104(%rbp)
	movq	$0, -96(%rbp)
	movq	$0, -88(%rbp)
.L68:
	call	opt_next@PLT
	testl	%eax, %eax
	je	.L133
	cmpl	$11, %eax
	jg	.L69
	cmpl	$-1, %eax
	jl	.L68
	addl	$1, %eax
	cmpl	$12, %eax
	ja	.L68
	movslq	(%rbx,%rax,4), %rax
	addq	%rbx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L73:
	.long	.L83-.L73
	.long	.L68-.L73
	.long	.L82-.L73
	.long	.L81-.L73
	.long	.L80-.L73
	.long	.L79-.L73
	.long	.L130-.L73
	.long	.L104-.L73
	.long	.L77-.L73
	.long	.L76-.L73
	.long	.L75-.L73
	.long	.L74-.L73
	.long	.L72-.L73
	.text
.L104:
	movl	$1, %r12d
	jmp	.L68
.L76:
	call	opt_arg@PLT
	xorl	%ecx, %ecx
	leaq	-72(%rbp), %rsi
	movl	$32773, %edx
	movq	%rax, %rdi
	leaq	.LC19(%rip), %r8
	call	load_certs@PLT
	testl	%eax, %eax
	je	.L132
	movl	$1, %r12d
.L130:
	movl	$1, %r13d
	jmp	.L68
	.p2align 4,,10
	.p2align 3
.L69:
	cmpl	$2032, %eax
	je	.L84
	jle	.L134
	cmpl	$2033, %eax
	jne	.L68
	movl	$1, v_verbose(%rip)
	jmp	.L68
	.p2align 4,,10
	.p2align 3
.L134:
	leal	-2001(%rax), %edx
	cmpl	$29, %edx
	ja	.L68
	movq	%r14, %rsi
	movl	%eax, %edi
	call	opt_verify@PLT
	testl	%eax, %eax
	je	.L132
	movl	%r15d, %eax
	addl	$1, %eax
	movl	%eax, %r15d
	jmp	.L68
.L72:
	movl	$1, -112(%rbp)
	jmp	.L68
.L74:
	movl	$1, -108(%rbp)
	jmp	.L68
.L75:
	call	opt_arg@PLT
	xorl	%ecx, %ecx
	leaq	-64(%rbp), %rsi
	movl	$32773, %edx
	movq	%rax, %rdi
	leaq	.LC20(%rip), %r8
	call	load_crls@PLT
	testl	%eax, %eax
	jne	.L68
	jmp	.L132
.L77:
	call	opt_arg@PLT
	xorl	%ecx, %ecx
	leaq	-80(%rbp), %rsi
	movl	$32773, %edx
	movq	%rax, %rdi
	leaq	.LC18(%rip), %r8
	call	load_certs@PLT
	testl	%eax, %eax
	jne	.L68
	jmp	.L132
.L79:
	call	opt_arg@PLT
	movq	%rax, -104(%rbp)
	jmp	.L68
.L80:
	call	opt_arg@PLT
	movq	%rax, -96(%rbp)
	jmp	.L68
.L81:
	call	opt_arg@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	setup_engine@PLT
	movq	%rax, -88(%rbp)
	testq	%rax, %rax
	jne	.L68
	.p2align 4,,10
	.p2align 3
.L67:
	movq	$0, -88(%rbp)
	movl	$1, %r10d
	xorl	%r12d, %r12d
	jmp	.L88
.L82:
	leaq	verify_options(%rip), %rdi
	xorl	%r13d, %r13d
	leaq	.LC15(%rip), %rbx
	call	opt_help@PLT
	movq	bio_err(%rip), %rdi
	leaq	.LC14(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L90:
	movl	%r13d, %edi
	addl	$1, %r13d
	call	X509_PURPOSE_get0@PLT
	movq	%rax, %r12
	movq	%rax, %rdi
	call	X509_PURPOSE_get0_name@PLT
	movq	%r12, %rdi
	movq	%rax, %r15
	call	X509_PURPOSE_get0_sname@PLT
	movq	bio_err(%rip), %rdi
	movq	%r15, %rcx
	movq	%rbx, %rsi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L89:
	call	X509_PURPOSE_get_count@PLT
	cmpl	%r13d, %eax
	jg	.L90
	movq	bio_err(%rip), %rdi
	leaq	.LC16(%rip), %rsi
	xorl	%eax, %eax
	xorl	%ebx, %ebx
	leaq	.LC17(%rip), %r12
	call	BIO_printf@PLT
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L92:
	movl	%ebx, %edi
	addl	$1, %ebx
	call	X509_VERIFY_PARAM_get0@PLT
	movq	%rax, %rdi
	call	X509_VERIFY_PARAM_get0_name@PLT
	movq	bio_err(%rip), %rdi
	movq	%r12, %rsi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L91:
	call	X509_VERIFY_PARAM_get_count@PLT
	cmpl	%ebx, %eax
	jg	.L92
	xorl	%r10d, %r10d
	xorl	%r12d, %r12d
	jmp	.L88
.L83:
	movq	-120(%rbp), %rdx
	leaq	.LC13(%rip), %rsi
.L131:
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L132:
	movl	$1, %r10d
	xorl	%r12d, %r12d
.L88:
	movq	%r14, %rdi
	movl	%r10d, -96(%rbp)
	call	X509_VERIFY_PARAM_free@PLT
	movq	%r12, %rdi
	call	X509_STORE_free@PLT
	movq	X509_free@GOTPCREL(%rip), %r12
	movq	-80(%rbp), %rdi
	movq	%r12, %rsi
	call	OPENSSL_sk_pop_free@PLT
	movq	-72(%rbp), %rdi
	movq	%r12, %rsi
	call	OPENSSL_sk_pop_free@PLT
	movq	X509_CRL_free@GOTPCREL(%rip), %rsi
	movq	-64(%rbp), %rdi
	call	OPENSSL_sk_pop_free@PLT
	movq	-88(%rbp), %rdi
	call	release_engine@PLT
	movl	-96(%rbp), %r10d
.L66:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L135
	addq	$104, %rsp
	movl	%r10d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L84:
	.cfi_restore_state
	call	opt_arg@PLT
	movq	%rax, %rdi
	call	set_nameopt@PLT
	testl	%eax, %eax
	jne	.L68
	jmp	.L132
	.p2align 4,,10
	.p2align 3
.L133:
	movl	%eax, -124(%rbp)
	call	opt_num_rest@PLT
	movl	%eax, %ebx
	call	opt_rest@PLT
	cmpq	$0, -72(%rbp)
	movl	-124(%rbp), %r10d
	movq	%rax, -136(%rbp)
	je	.L94
	movq	-96(%rbp), %rax
	orq	-104(%rbp), %rax
	jne	.L136
.L94:
	movq	-96(%rbp), %rsi
	movq	-104(%rbp), %rdi
	movl	%r12d, %edx
	movl	%r13d, %ecx
	movl	%r10d, -120(%rbp)
	call	setup_verify@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L110
	leaq	cb(%rip), %rsi
	movq	%rax, %rdi
	call	X509_STORE_set_verify_cb@PLT
	testl	%r15d, %r15d
	movl	-120(%rbp), %r10d
	jne	.L137
.L95:
	movl	%r10d, -96(%rbp)
	call	ERR_clear_error@PLT
	movl	-108(%rbp), %eax
	movl	-96(%rbp), %r10d
	testl	%eax, %eax
	jne	.L138
.L96:
	movq	-64(%rbp), %r8
	movq	-72(%rbp), %rcx
	movq	-80(%rbp), %rdx
	testl	%ebx, %ebx
	jle	.L97
	movq	-136(%rbp), %r13
	leal	-1(%rbx), %eax
	movq	%r14, -96(%rbp)
	xorl	%ebx, %ebx
	movl	%r10d, -104(%rbp)
	movl	-112(%rbp), %r15d
	leaq	8(%r13,%rax,8), %rax
	movq	%r13, %r14
	movq	%rax, %r13
	jmp	.L98
	.p2align 4,,10
	.p2align 3
.L139:
	movq	-64(%rbp), %r8
	movq	-72(%rbp), %rcx
	movq	-80(%rbp), %rdx
.L98:
	movq	(%r14), %rsi
	movl	%r15d, %r9d
	movq	%r12, %rdi
	call	check
	cmpl	$1, %eax
	movl	$-1, %eax
	cmovne	%eax, %ebx
	addq	$8, %r14
	cmpq	%r14, %r13
	jne	.L139
.L101:
	endbr64
	movq	-96(%rbp), %r14
	movl	-104(%rbp), %r10d
	movq	%r14, %rdi
	movl	%r10d, -96(%rbp)
	call	X509_VERIFY_PARAM_free@PLT
	movq	%r12, %rdi
	call	X509_STORE_free@PLT
	movq	X509_free@GOTPCREL(%rip), %r12
	movq	-80(%rbp), %rdi
	movq	%r12, %rsi
	call	OPENSSL_sk_pop_free@PLT
	movq	-72(%rbp), %rdi
	movq	%r12, %rsi
	call	OPENSSL_sk_pop_free@PLT
	movq	X509_CRL_free@GOTPCREL(%rip), %rsi
	movq	-64(%rbp), %rdi
	call	OPENSSL_sk_pop_free@PLT
	movq	-88(%rbp), %rdi
	call	release_engine@PLT
	movl	-96(%rbp), %r10d
	cmpl	$-1, %ebx
	movl	$2, %eax
	cmove	%eax, %r10d
	jmp	.L66
.L138:
	movq	%r12, %rdi
	call	store_setup_crl_download@PLT
	movl	-96(%rbp), %r10d
	jmp	.L96
.L137:
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	%r10d, -96(%rbp)
	call	X509_STORE_set1_param@PLT
	movl	-96(%rbp), %r10d
	jmp	.L95
.L97:
	movl	-112(%rbp), %r9d
	xorl	%esi, %esi
	movq	%r12, %rdi
	movl	%r10d, -96(%rbp)
	call	check
	movl	-96(%rbp), %r10d
	cmpl	$1, %eax
	je	.L88
	movq	%r14, %rdi
	call	X509_VERIFY_PARAM_free@PLT
	movq	%r12, %rdi
	call	X509_STORE_free@PLT
	movq	X509_free@GOTPCREL(%rip), %r12
	movq	-80(%rbp), %rdi
	movq	%r12, %rsi
	call	OPENSSL_sk_pop_free@PLT
	movq	-72(%rbp), %rdi
	movq	%r12, %rsi
	call	OPENSSL_sk_pop_free@PLT
	movq	X509_CRL_free@GOTPCREL(%rip), %rsi
	movq	-64(%rbp), %rdi
	call	OPENSSL_sk_pop_free@PLT
	movq	-88(%rbp), %rdi
	call	release_engine@PLT
	movl	$2, %r10d
	jmp	.L66
.L110:
	movl	$1, %r10d
	jmp	.L88
.L136:
	movq	-120(%rbp), %rdx
	leaq	.LC21(%rip), %rsi
	jmp	.L131
.L135:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1435:
	.size	verify_main, .-verify_main
	.globl	verify_options
	.section	.rodata.str1.8
	.align 8
.LC22:
	.string	"Usage: %s [options] cert.pem...\n"
	.section	.rodata.str1.1
.LC23:
	.string	"Valid options are:\n"
.LC24:
	.string	"help"
.LC25:
	.string	"Display this summary"
.LC26:
	.string	"verbose"
	.section	.rodata.str1.8
	.align 8
.LC27:
	.string	"Print extra information about the operations being performed."
	.section	.rodata.str1.1
.LC28:
	.string	"CApath"
	.section	.rodata.str1.8
	.align 8
.LC29:
	.string	"A directory of trusted certificates"
	.section	.rodata.str1.1
.LC30:
	.string	"CAfile"
	.section	.rodata.str1.8
	.align 8
.LC31:
	.string	"A file of trusted certificates"
	.section	.rodata.str1.1
.LC32:
	.string	"no-CAfile"
	.section	.rodata.str1.8
	.align 8
.LC33:
	.string	"Do not load the default certificates file"
	.section	.rodata.str1.1
.LC34:
	.string	"no-CApath"
	.section	.rodata.str1.8
	.align 8
.LC35:
	.string	"Do not load certificates from the default certificates directory"
	.section	.rodata.str1.1
.LC36:
	.string	"untrusted"
	.section	.rodata.str1.8
	.align 8
.LC37:
	.string	"A file of untrusted certificates"
	.section	.rodata.str1.1
.LC38:
	.string	"trusted"
.LC39:
	.string	"CRLfile"
	.section	.rodata.str1.8
	.align 8
.LC40:
	.string	"File containing one or more CRL's (in PEM format) to load"
	.section	.rodata.str1.1
.LC41:
	.string	"crl_download"
	.section	.rodata.str1.8
	.align 8
.LC42:
	.string	"Attempt to download CRL information for this certificate"
	.section	.rodata.str1.1
.LC43:
	.string	"show_chain"
	.section	.rodata.str1.8
	.align 8
.LC44:
	.string	"Display information about the certificate chain"
	.section	.rodata.str1.1
.LC45:
	.string	"nameopt"
	.section	.rodata.str1.8
	.align 8
.LC46:
	.string	"Various certificate name options"
	.section	.rodata.str1.1
.LC47:
	.string	"policy"
	.section	.rodata.str1.8
	.align 8
.LC48:
	.string	"adds policy to the acceptable policy set"
	.section	.rodata.str1.1
.LC49:
	.string	"purpose"
.LC50:
	.string	"certificate chain purpose"
.LC51:
	.string	"verify_name"
.LC52:
	.string	"verification policy name"
.LC53:
	.string	"verify_depth"
.LC54:
	.string	"chain depth limit"
.LC55:
	.string	"auth_level"
	.section	.rodata.str1.8
	.align 8
.LC56:
	.string	"chain authentication security level"
	.section	.rodata.str1.1
.LC57:
	.string	"attime"
.LC58:
	.string	"verification epoch time"
.LC59:
	.string	"verify_hostname"
.LC60:
	.string	"expected peer hostname"
.LC61:
	.string	"verify_email"
.LC62:
	.string	"expected peer email"
.LC63:
	.string	"verify_ip"
.LC64:
	.string	"expected peer IP address"
.LC65:
	.string	"ignore_critical"
	.section	.rodata.str1.8
	.align 8
.LC66:
	.string	"permit unhandled critical extensions"
	.section	.rodata.str1.1
.LC67:
	.string	"issuer_checks"
.LC68:
	.string	"(deprecated)"
.LC69:
	.string	"crl_check"
	.section	.rodata.str1.8
	.align 8
.LC70:
	.string	"check leaf certificate revocation"
	.section	.rodata.str1.1
.LC71:
	.string	"crl_check_all"
.LC72:
	.string	"check full chain revocation"
.LC73:
	.string	"policy_check"
.LC74:
	.string	"perform rfc5280 policy checks"
.LC75:
	.string	"explicit_policy"
	.section	.rodata.str1.8
	.align 8
.LC76:
	.string	"set policy variable require-explicit-policy"
	.section	.rodata.str1.1
.LC77:
	.string	"inhibit_any"
	.section	.rodata.str1.8
	.align 8
.LC78:
	.string	"set policy variable inhibit-any-policy"
	.section	.rodata.str1.1
.LC79:
	.string	"inhibit_map"
	.section	.rodata.str1.8
	.align 8
.LC80:
	.string	"set policy variable inhibit-policy-mapping"
	.section	.rodata.str1.1
.LC81:
	.string	"x509_strict"
	.section	.rodata.str1.8
	.align 8
.LC82:
	.string	"disable certificate compatibility work-arounds"
	.section	.rodata.str1.1
.LC83:
	.string	"extended_crl"
.LC84:
	.string	"enable extended CRL features"
.LC85:
	.string	"use_deltas"
.LC86:
	.string	"use delta CRLs"
.LC87:
	.string	"policy_print"
	.section	.rodata.str1.8
	.align 8
.LC88:
	.string	"print policy processing diagnostics"
	.section	.rodata.str1.1
.LC89:
	.string	"check_ss_sig"
.LC90:
	.string	"check root CA self-signatures"
.LC91:
	.string	"trusted_first"
	.section	.rodata.str1.8
	.align 8
.LC92:
	.string	"search trust store first (default)"
	.section	.rodata.str1.1
.LC93:
	.string	"suiteB_128_only"
.LC94:
	.string	"Suite B 128-bit-only mode"
.LC95:
	.string	"suiteB_128"
	.section	.rodata.str1.8
	.align 8
.LC96:
	.string	"Suite B 128-bit mode allowing 192-bit algorithms"
	.section	.rodata.str1.1
.LC97:
	.string	"suiteB_192"
.LC98:
	.string	"Suite B 192-bit-only mode"
.LC99:
	.string	"partial_chain"
	.section	.rodata.str1.8
	.align 8
.LC100:
	.string	"accept chains anchored by intermediate trust-store CAs"
	.section	.rodata.str1.1
.LC101:
	.string	"no_alt_chains"
.LC102:
	.string	"no_check_time"
	.section	.rodata.str1.8
	.align 8
.LC103:
	.string	"ignore certificate validity time"
	.section	.rodata.str1.1
.LC104:
	.string	"allow_proxy_certs"
	.section	.rodata.str1.8
	.align 8
.LC105:
	.string	"allow the use of proxy certificates"
	.section	.rodata.str1.1
.LC106:
	.string	"engine"
	.section	.rodata.str1.8
	.align 8
.LC107:
	.string	"Use engine, possibly a hardware device"
	.section	.data.rel.ro,"aw"
	.align 32
	.type	verify_options, @object
	.size	verify_options, 1104
verify_options:
	.quad	OPT_HELP_STR
	.long	1
	.long	45
	.quad	.LC22
	.quad	OPT_HELP_STR
	.long	1
	.long	45
	.quad	.LC23
	.quad	.LC24
	.long	1
	.long	45
	.quad	.LC25
	.quad	.LC26
	.long	2033
	.long	45
	.quad	.LC27
	.quad	.LC28
	.long	3
	.long	47
	.quad	.LC29
	.quad	.LC30
	.long	4
	.long	60
	.quad	.LC31
	.quad	.LC32
	.long	6
	.long	45
	.quad	.LC33
	.quad	.LC34
	.long	5
	.long	45
	.quad	.LC35
	.quad	.LC36
	.long	7
	.long	60
	.quad	.LC37
	.quad	.LC38
	.long	8
	.long	60
	.quad	.LC31
	.quad	.LC39
	.long	9
	.long	60
	.quad	.LC40
	.quad	.LC41
	.long	10
	.long	45
	.quad	.LC42
	.quad	.LC43
	.long	11
	.long	45
	.quad	.LC44
	.quad	.LC45
	.long	2032
	.long	115
	.quad	.LC46
	.quad	.LC47
	.long	2001
	.long	115
	.quad	.LC48
	.quad	.LC49
	.long	2002
	.long	115
	.quad	.LC50
	.quad	.LC51
	.long	2003
	.long	115
	.quad	.LC52
	.quad	.LC53
	.long	2004
	.long	110
	.quad	.LC54
	.quad	.LC55
	.long	2029
	.long	110
	.quad	.LC56
	.quad	.LC57
	.long	2005
	.long	77
	.quad	.LC58
	.quad	.LC59
	.long	2006
	.long	115
	.quad	.LC60
	.quad	.LC61
	.long	2007
	.long	115
	.quad	.LC62
	.quad	.LC63
	.long	2008
	.long	115
	.quad	.LC64
	.quad	.LC65
	.long	2009
	.long	45
	.quad	.LC66
	.quad	.LC67
	.long	2010
	.long	45
	.quad	.LC68
	.quad	.LC69
	.long	2011
	.long	45
	.quad	.LC70
	.quad	.LC71
	.long	2012
	.long	45
	.quad	.LC72
	.quad	.LC73
	.long	2013
	.long	45
	.quad	.LC74
	.quad	.LC75
	.long	2014
	.long	45
	.quad	.LC76
	.quad	.LC77
	.long	2015
	.long	45
	.quad	.LC78
	.quad	.LC79
	.long	2016
	.long	45
	.quad	.LC80
	.quad	.LC81
	.long	2017
	.long	45
	.quad	.LC82
	.quad	.LC83
	.long	2018
	.long	45
	.quad	.LC84
	.quad	.LC85
	.long	2019
	.long	45
	.quad	.LC86
	.quad	.LC87
	.long	2020
	.long	45
	.quad	.LC88
	.quad	.LC89
	.long	2021
	.long	45
	.quad	.LC90
	.quad	.LC91
	.long	2022
	.long	45
	.quad	.LC92
	.quad	.LC93
	.long	2023
	.long	45
	.quad	.LC94
	.quad	.LC95
	.long	2024
	.long	45
	.quad	.LC96
	.quad	.LC97
	.long	2025
	.long	45
	.quad	.LC98
	.quad	.LC99
	.long	2026
	.long	45
	.quad	.LC100
	.quad	.LC101
	.long	2027
	.long	45
	.quad	.LC68
	.quad	.LC102
	.long	2028
	.long	45
	.quad	.LC103
	.quad	.LC104
	.long	2030
	.long	45
	.quad	.LC105
	.quad	.LC106
	.long	2
	.long	115
	.quad	.LC107
	.quad	0
	.zero	16
	.local	v_verbose
	.comm	v_verbose,4,4
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
