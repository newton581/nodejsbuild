	.file	"rsa.c"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"%s: Use -help for summary.\n"
.LC1:
	.string	"Error getting passwords\n"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"Only private keys can be checked\n"
	.section	.rodata.str1.1
.LC3:
	.string	"Public Key"
.LC4:
	.string	"Private Key"
.LC5:
	.string	"Modulus="
.LC6:
	.string	"\n"
.LC7:
	.string	"RSA key ok\n"
.LC8:
	.string	"RSA key error: %s\n"
.LC9:
	.string	"writing RSA key\n"
	.section	.rodata.str1.8
	.align 8
.LC10:
	.string	"PVK form impossible with public key input\n"
	.align 8
.LC11:
	.string	"bad output format specified for outfile\n"
	.section	.rodata.str1.1
.LC12:
	.string	"unable to write key\n"
	.section	.rodata.str1.8
	.align 8
.LC13:
	.string	"../deps/openssl/openssl/apps/rsa.c"
	.text
	.p2align 4
	.globl	rsa_main
	.type	rsa_main, @function
rsa_main:
.LFB1435:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	rsa_options(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	xorl	%r14d, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	.L5(%rip), %rbx
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -88(%rbp)
	movq	$0, -80(%rbp)
	movq	$0, -72(%rbp)
	movl	$32773, -96(%rbp)
	movl	$32773, -92(%rbp)
	call	opt_init@PLT
	movl	$0, -104(%rbp)
	movl	$2, -152(%rbp)
	movq	%rax, %r13
	movl	$0, -148(%rbp)
	movl	$0, -124(%rbp)
	movl	$0, -128(%rbp)
	movl	$0, -112(%rbp)
	movq	$0, -120(%rbp)
	movq	$0, -144(%rbp)
	movq	$0, -136(%rbp)
.L2:
	call	opt_next@PLT
	testl	%eax, %eax
	je	.L111
.L26:
	leal	1(%rax), %edx
	cmpl	$21, %edx
	ja	.L2
	movslq	(%rbx,%rdx,4), %rdx
	addq	%rbx, %rdx
	notrack jmp	*%rdx
	.section	.rodata
	.align 4
	.align 4
.L5:
	.long	.L23-.L5
	.long	.L2-.L5
	.long	.L22-.L5
	.long	.L21-.L5
	.long	.L20-.L5
	.long	.L19-.L5
	.long	.L18-.L5
	.long	.L17-.L5
	.long	.L16-.L5
	.long	.L15-.L5
	.long	.L14-.L5
	.long	.L13-.L5
	.long	.L12-.L5
	.long	.L11-.L5
	.long	.L10-.L5
	.long	.L10-.L5
	.long	.L10-.L5
	.long	.L9-.L5
	.long	.L8-.L5
	.long	.L7-.L5
	.long	.L6-.L5
	.long	.L4-.L5
	.text
.L16:
	movl	$1, %r12d
	call	opt_next@PLT
	testl	%eax, %eax
	jne	.L26
.L111:
	call	opt_num_rest@PLT
	movl	%eax, %ebx
	testl	%eax, %eax
	jne	.L23
	testl	%r12d, %r12d
	jne	.L64
	movl	-112(%rbp), %r11d
	testl	%r11d, %r11d
	je	.L64
	movq	-120(%rbp), %rdi
	leaq	-72(%rbp), %rcx
	leaq	-80(%rbp), %rdx
	movq	%r15, %rsi
	call	app_passwd@PLT
	testl	%eax, %eax
	jne	.L112
.L57:
	leaq	.LC1(%rip), %rsi
.L109:
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
	xorl	%r13d, %r13d
	xorl	%r15d, %r15d
	movl	$1, %ebx
	call	BIO_printf@PLT
	jmp	.L24
.L20:
	call	opt_arg@PLT
	leaq	-92(%rbp), %rdx
	movl	$1982, %esi
	movq	%rax, %rdi
	call	opt_format@PLT
	testl	%eax, %eax
	jne	.L2
.L23:
	movq	bio_err(%rip), %rdi
	movq	%r13, %rdx
	xorl	%eax, %eax
	xorl	%r13d, %r13d
	leaq	.LC0(%rip), %rsi
	movl	$1, %ebx
	xorl	%r15d, %r15d
	call	BIO_printf@PLT
.L24:
	movq	%r14, %rdi
	call	release_engine@PLT
	movq	%r15, %rdi
	call	BIO_free_all@PLT
	movq	%r13, %rdi
	call	RSA_free@PLT
	movq	-80(%rbp), %rdi
	movl	$308, %edx
	leaq	.LC13(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-72(%rbp), %rdi
	movl	$309, %edx
	leaq	.LC13(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L113
	leaq	-40(%rbp), %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L4:
	.cfi_restore_state
	call	opt_unknown@PLT
	leaq	-88(%rbp), %rsi
	movq	%rax, %rdi
	call	opt_cipher@PLT
	testl	%eax, %eax
	jne	.L2
	jmp	.L23
.L6:
	movl	$1, -128(%rbp)
	jmp	.L2
.L7:
	movl	$1, -148(%rbp)
	jmp	.L2
.L8:
	movl	$1, -112(%rbp)
	jmp	.L2
.L9:
	movl	$1, -124(%rbp)
	jmp	.L2
.L10:
	subl	$13, %eax
	movl	%eax, -152(%rbp)
	jmp	.L2
.L11:
	movl	$2, -104(%rbp)
	jmp	.L2
.L12:
	movl	$2, %r12d
	jmp	.L2
.L13:
	call	opt_arg@PLT
	movq	%rax, -120(%rbp)
	jmp	.L2
.L14:
	call	opt_arg@PLT
	movq	%rax, %r15
	jmp	.L2
.L15:
	movl	$1, -104(%rbp)
	jmp	.L2
.L17:
	call	opt_arg@PLT
	movq	%rax, -144(%rbp)
	jmp	.L2
.L18:
	call	opt_arg@PLT
	movq	%rax, -136(%rbp)
	jmp	.L2
.L19:
	call	opt_arg@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	setup_engine@PLT
	movq	%rax, %r14
	jmp	.L2
.L21:
	call	opt_arg@PLT
	leaq	-96(%rbp), %rdx
	movl	$1982, %esi
	movq	%rax, %rdi
	call	opt_format@PLT
	testl	%eax, %eax
	jne	.L2
	jmp	.L23
.L22:
	leaq	rsa_options(%rip), %rdi
	xorl	%ebx, %ebx
	xorl	%r13d, %r13d
	xorl	%r15d, %r15d
	call	opt_help@PLT
	jmp	.L24
.L64:
	movl	-124(%rbp), %eax
	orl	-104(%rbp), %eax
	leaq	-72(%rbp), %rcx
	leaq	-80(%rbp), %rdx
	sete	%al
	movq	-120(%rbp), %rdi
	movq	%r15, %rsi
	movzbl	%al, %eax
	movl	%eax, -156(%rbp)
	call	app_passwd@PLT
	testl	%eax, %eax
	je	.L57
	testl	%r12d, %r12d
	je	.L30
	movl	-128(%rbp), %r10d
	testl	%r10d, %r10d
	jne	.L114
.L30:
	movl	-96(%rbp), %esi
	movl	%esi, %r10d
	testl	%r12d, %r12d
	je	.L31
	cmpl	$2, %r12d
	je	.L115
.L32:
	movq	-80(%rbp), %rcx
	movq	-136(%rbp), %rdi
	movq	%r14, %r8
	movl	$1, %edx
	leaq	.LC3(%rip), %r9
	call	load_pubkey@PLT
	movq	%rax, %r15
.L33:
	testq	%r15, %r15
	je	.L116
	movq	%r15, %rdi
	call	EVP_PKEY_get1_RSA@PLT
	movq	%r15, %rdi
	movq	%rax, %r13
	call	EVP_PKEY_free@PLT
	testq	%r13, %r13
	je	.L35
	movl	-156(%rbp), %edx
	movl	-92(%rbp), %esi
	movq	-144(%rbp), %rdi
	call	bio_open_owner@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L61
	movl	-112(%rbp), %r9d
	testl	%r9d, %r9d
	jne	.L117
.L37:
	movl	-148(%rbp), %r8d
	testl	%r8d, %r8d
	jne	.L118
.L38:
	movl	-128(%rbp), %edi
	testl	%edi, %edi
	jne	.L119
.L41:
	movl	-124(%rbp), %esi
	testl	%esi, %esi
	jne	.L24
	movq	bio_err(%rip), %rdi
	leaq	.LC9(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movl	-92(%rbp), %eax
	cmpl	$4, %eax
	je	.L120
	cmpl	$32773, %eax
	je	.L121
	subl	$11, %eax
	cmpl	$1, %eax
	ja	.L52
	call	EVP_PKEY_new@PLT
	movl	$1, %ebx
	testq	%rax, %rax
	je	.L24
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%rax, -112(%rbp)
	call	EVP_PKEY_set1_RSA@PLT
	cmpl	$12, -92(%rbp)
	movq	-112(%rbp), %r9
	je	.L122
	movl	-104(%rbp), %eax
	movq	%r9, %rsi
	movq	%r9, -104(%rbp)
	movq	%r15, %rdi
	orl	%r12d, %eax
	je	.L56
	call	i2b_PublicKey_bio@PLT
	movq	-104(%rbp), %r9
.L55:
	movq	%r9, %rdi
	movl	%eax, -104(%rbp)
	call	EVP_PKEY_free@PLT
	movl	-104(%rbp), %eax
.L48:
	xorl	%ebx, %ebx
	testl	%eax, %eax
	jg	.L24
	movq	bio_err(%rip), %rdi
	leaq	.LC12(%rip), %rsi
	xorl	%eax, %eax
	movl	$1, %ebx
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	jmp	.L24
.L112:
	movl	$1, -156(%rbp)
	movl	-96(%rbp), %r10d
.L31:
	movq	-80(%rbp), %rcx
	movq	%r14, %r8
	movl	$1, %edx
	movl	%r10d, %esi
	movq	-136(%rbp), %rdi
	leaq	.LC4(%rip), %r9
	call	load_key@PLT
	movq	%rax, %r15
	jmp	.L33
.L116:
	xorl	%edi, %edi
	call	EVP_PKEY_free@PLT
.L35:
	movq	bio_err(%rip), %rdi
	movl	$1, %ebx
	xorl	%r13d, %r13d
	xorl	%r15d, %r15d
	call	ERR_print_errors@PLT
	jmp	.L24
.L114:
	leaq	.LC2(%rip), %rsi
	jmp	.L109
.L115:
	movl	$32777, %esi
	cmpl	$32773, %r10d
	je	.L32
	cmpl	$4, %r10d
	movl	$10, %esi
	movl	$-1, %eax
	cmovne	%eax, %esi
	jmp	.L32
.L117:
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	RSA_print@PLT
	testl	%eax, %eax
	jne	.L37
	movq	-144(%rbp), %rdi
	call	perror@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	movl	-112(%rbp), %ebx
	jmp	.L24
.L119:
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	RSA_check_key_ex@PLT
	cmpl	$1, %eax
	je	.L123
	testl	%eax, %eax
	je	.L42
	cmpl	$-1, %eax
	jne	.L41
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	movl	-128(%rbp), %ebx
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L124:
	shrl	$24, %eax
	cmpl	$4, %eax
	jne	.L41
	movq	%rdi, %rax
	shrq	$12, %rax
	andl	$4095, %eax
	cmpl	$160, %eax
	jne	.L41
	movl	%edi, %eax
	andl	$4095, %eax
	cmpl	$65, %eax
	je	.L41
	call	ERR_reason_error_string@PLT
	leaq	.LC8(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	call	ERR_get_error@PLT
.L42:
	call	ERR_peek_error@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L124
	jmp	.L41
.L118:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	leaq	-64(%rbp), %rsi
	movq	%r13, %rdi
	call	RSA_get0_key@PLT
	movq	%r15, %rdi
	leaq	.LC5(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	-64(%rbp), %rsi
	movq	%r15, %rdi
	call	BN_print@PLT
	leaq	.LC6(%rip), %rsi
	movq	%r15, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L38
.L61:
	movl	$1, %ebx
	jmp	.L24
.L120:
	movl	-104(%rbp), %eax
	movq	%r13, %rsi
	movq	%r15, %rdi
	orl	%eax, %r12d
	je	.L46
	cmpl	$2, %eax
	je	.L125
	call	i2d_RSA_PUBKEY_bio@PLT
	jmp	.L48
.L121:
	movl	-104(%rbp), %eax
	orl	%eax, %r12d
	je	.L50
	movq	%r13, %rsi
	movq	%r15, %rdi
	cmpl	$2, %eax
	je	.L126
	call	PEM_write_bio_RSA_PUBKEY@PLT
	jmp	.L48
.L52:
	movq	bio_err(%rip), %rdi
	leaq	.LC11(%rip), %rsi
	xorl	%eax, %eax
	movl	$1, %ebx
	call	BIO_printf@PLT
	jmp	.L24
.L123:
	leaq	.LC7(%rip), %rsi
	movq	%r15, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L41
.L46:
	call	i2d_RSAPrivateKey_bio@PLT
	jmp	.L48
.L125:
	call	i2d_RSAPublicKey_bio@PLT
	jmp	.L48
.L50:
	subq	$8, %rsp
	pushq	-72(%rbp)
	movq	-88(%rbp), %rdx
	xorl	%ecx, %ecx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	PEM_write_bio_RSAPrivateKey@PLT
	popq	%rdx
	popq	%rcx
	jmp	.L48
.L122:
	testl	%r12d, %r12d
	jne	.L127
	movq	-72(%rbp), %r8
	movl	-152(%rbp), %edx
	movq	%r9, %rsi
	xorl	%ecx, %ecx
	movq	%r15, %rdi
	movq	%r9, -104(%rbp)
	call	i2b_PVK_bio@PLT
	movq	-104(%rbp), %r9
	jmp	.L55
.L126:
	call	PEM_write_bio_RSAPublicKey@PLT
	jmp	.L48
.L127:
	movq	bio_err(%rip), %rdi
	leaq	.LC10(%rip), %rsi
	xorl	%eax, %eax
	movq	%r9, -104(%rbp)
	call	BIO_printf@PLT
	movq	-104(%rbp), %r9
	movq	%r9, %rdi
	call	EVP_PKEY_free@PLT
	jmp	.L24
.L56:
	call	i2b_PrivateKey_bio@PLT
	movq	-104(%rbp), %r9
	jmp	.L55
.L113:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1435:
	.size	rsa_main, .-rsa_main
	.globl	rsa_options
	.section	.rodata.str1.1
.LC14:
	.string	"help"
.LC15:
	.string	"Display this summary"
.LC16:
	.string	"inform"
.LC17:
	.string	"Input format, one of DER PEM"
.LC18:
	.string	"outform"
	.section	.rodata.str1.8
	.align 8
.LC19:
	.string	"Output format, one of DER PEM PVK"
	.section	.rodata.str1.1
.LC20:
	.string	"in"
.LC21:
	.string	"Input file"
.LC22:
	.string	"out"
.LC23:
	.string	"Output file"
.LC24:
	.string	"pubin"
	.section	.rodata.str1.8
	.align 8
.LC25:
	.string	"Expect a public key in input file"
	.section	.rodata.str1.1
.LC26:
	.string	"pubout"
.LC27:
	.string	"Output a public key"
.LC28:
	.string	"passout"
	.section	.rodata.str1.8
	.align 8
.LC29:
	.string	"Output file pass phrase source"
	.section	.rodata.str1.1
.LC30:
	.string	"passin"
.LC31:
	.string	"Input file pass phrase source"
.LC32:
	.string	"RSAPublicKey_in"
.LC33:
	.string	"Input is an RSAPublicKey"
.LC34:
	.string	"RSAPublicKey_out"
.LC35:
	.string	"Output is an RSAPublicKey"
.LC36:
	.string	"noout"
.LC37:
	.string	"Don't print key out"
.LC38:
	.string	"text"
.LC39:
	.string	"Print the key in text"
.LC40:
	.string	"modulus"
.LC41:
	.string	"Print the RSA key modulus"
.LC42:
	.string	"check"
.LC43:
	.string	"Verify key consistency"
.LC44:
	.string	""
.LC45:
	.string	"Any supported cipher"
.LC46:
	.string	"pvk-strong"
	.section	.rodata.str1.8
	.align 8
.LC47:
	.string	"Enable 'Strong' PVK encoding level (default)"
	.section	.rodata.str1.1
.LC48:
	.string	"pvk-weak"
	.section	.rodata.str1.8
	.align 8
.LC49:
	.string	"Enable 'Weak' PVK encoding level"
	.section	.rodata.str1.1
.LC50:
	.string	"pvk-none"
.LC51:
	.string	"Don't enforce PVK encoding"
.LC52:
	.string	"engine"
	.section	.rodata.str1.8
	.align 8
.LC53:
	.string	"Use engine, possibly a hardware device"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	rsa_options, @object
	.size	rsa_options, 504
rsa_options:
	.quad	.LC14
	.long	1
	.long	45
	.quad	.LC15
	.quad	.LC16
	.long	2
	.long	102
	.quad	.LC17
	.quad	.LC18
	.long	3
	.long	102
	.quad	.LC19
	.quad	.LC20
	.long	5
	.long	115
	.quad	.LC21
	.quad	.LC22
	.long	6
	.long	62
	.quad	.LC23
	.quad	.LC24
	.long	7
	.long	45
	.quad	.LC25
	.quad	.LC26
	.long	8
	.long	45
	.quad	.LC27
	.quad	.LC28
	.long	9
	.long	115
	.quad	.LC29
	.quad	.LC30
	.long	10
	.long	115
	.quad	.LC31
	.quad	.LC32
	.long	11
	.long	45
	.quad	.LC33
	.quad	.LC34
	.long	12
	.long	45
	.quad	.LC35
	.quad	.LC36
	.long	16
	.long	45
	.quad	.LC37
	.quad	.LC38
	.long	17
	.long	45
	.quad	.LC39
	.quad	.LC40
	.long	18
	.long	45
	.quad	.LC41
	.quad	.LC42
	.long	19
	.long	45
	.quad	.LC43
	.quad	.LC44
	.long	20
	.long	45
	.quad	.LC45
	.quad	.LC46
	.long	15
	.long	45
	.quad	.LC47
	.quad	.LC48
	.long	14
	.long	45
	.quad	.LC49
	.quad	.LC50
	.long	13
	.long	45
	.quad	.LC51
	.quad	.LC52
	.long	4
	.long	115
	.quad	.LC53
	.quad	0
	.zero	16
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
