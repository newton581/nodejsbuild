	.file	"cms.c"
	.text
	.p2align 4
	.type	cms_cb, @function
cms_cb:
.LFB1533:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%edi, %r13d
	movq	%rsi, %rdi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	call	X509_STORE_CTX_get_error@PLT
	movl	%eax, verify_err(%rip)
	cmpl	$43, %eax
	je	.L2
	testl	%eax, %eax
	jne	.L3
	cmpl	$2, %r13d
	jne	.L3
.L2:
	movq	%r12, %rdi
	call	policies_print@PLT
.L3:
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1533:
	.size	cms_cb, .-cms_cb
	.p2align 4
	.type	make_names_stack, @function
make_names_stack:
.LFB1536:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L10
	xorl	%ebx, %ebx
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L13:
	movl	%ebx, %esi
	movq	%r13, %rdi
	call	OPENSSL_sk_value@PLT
	xorl	%r9d, %r9d
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%rax, %r8
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	a2i_GENERAL_NAME@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L10
	call	GENERAL_NAMES_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L12
	movq	%r15, %rsi
	movq	%rax, %rdi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L12
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L15
	addl	$1, %ebx
.L11:
	movq	%r13, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jl	.L13
.L9:
	addq	$8, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	.cfi_restore_state
	xorl	%r15d, %r15d
.L12:
	movq	GENERAL_NAMES_free@GOTPCREL(%rip), %rsi
	movq	%r14, %rdi
	xorl	%r14d, %r14d
	call	OPENSSL_sk_pop_free@PLT
	movq	%r12, %rdi
	call	GENERAL_NAMES_free@PLT
	movq	%r15, %rdi
	call	GENERAL_NAME_free@PLT
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L10:
	xorl	%r15d, %r15d
	xorl	%r12d, %r12d
	jmp	.L12
	.cfi_endproc
.LFE1536:
	.size	make_names_stack, .-make_names_stack
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"    "
.LC1:
	.string	"\n"
	.text
	.p2align 4
	.type	gnames_stack_print, @function
gnames_stack_print:
.LFB1534:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	xorl	%r14d, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	leaq	.LC0(%rip), %r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -56(%rbp)
	movq	-56(%rbp), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r14d
	jge	.L35
	.p2align 4,,10
	.p2align 3
.L33:
	movq	-56(%rbp), %rdi
	movl	%r14d, %esi
	xorl	%ebx, %ebx
	call	OPENSSL_sk_value@PLT
	movq	%rax, %r12
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L32:
	movl	%ebx, %esi
	movq	%r12, %rdi
	addl	$1, %ebx
	call	OPENSSL_sk_value@PLT
	movq	bio_err(%rip), %rdi
	movq	%r13, %rsi
	movq	%rax, %r15
	call	BIO_puts@PLT
	movq	bio_err(%rip), %rdi
	movq	%r15, %rsi
	call	GENERAL_NAME_print@PLT
	movq	bio_err(%rip), %rdi
	leaq	.LC1(%rip), %rsi
	call	BIO_puts@PLT
.L31:
	movq	%r12, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jl	.L32
	movq	-56(%rbp), %rdi
	addl	$1, %r14d
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r14d
	jl	.L33
.L35:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1534:
	.size	gnames_stack_print, .-gnames_stack_print
	.section	.rodata.str1.1
.LC2:
	.string	"\r\n"
.LC3:
	.string	"rb"
.LC4:
	.string	"r"
.LC5:
	.string	"%s: Use -help for summary.\n"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC6:
	.string	"Invalid key (supplied twice) %s\n"
	.section	.rodata.str1.1
.LC7:
	.string	"Invalid key %s\n"
	.section	.rodata.str1.8
	.align 8
.LC8:
	.string	"Invalid id (supplied twice) %s\n"
	.section	.rodata.str1.1
.LC9:
	.string	"Invalid id %s\n"
	.section	.rodata.str1.8
	.align 8
.LC10:
	.string	"Invalid OID (supplied twice) %s\n"
	.section	.rodata.str1.1
.LC11:
	.string	"Invalid OID %s\n"
	.section	.rodata.str1.8
	.align 8
.LC12:
	.string	"Illegal -inkey without -signer\n"
	.section	.rodata.str1.1
.LC13:
	.string	"recipient certificate file"
.LC14:
	.string	"No key specified\n"
.LC15:
	.string	"key param buffer"
	.section	.rodata.str1.8
	.align 8
.LC16:
	.string	"No Signed Receipts Recipients\n"
	.align 8
.LC17:
	.string	"Signed receipts only allowed with -sign\n"
	.align 8
.LC18:
	.string	"Multiple signers or keys not allowed\n"
	.align 8
.LC19:
	.string	"No signer certificate specified\n"
	.align 8
.LC20:
	.string	"No recipient certificate or key specified\n"
	.align 8
.LC21:
	.string	"No recipient(s) certificate(s) specified\n"
	.align 8
.LC22:
	.string	"No operation option (-encrypt|-decrypt|-sign|-verify|...) specified.\n"
	.section	.rodata.str1.1
.LC23:
	.string	"Error getting password\n"
.LC24:
	.string	"No secret key id\n"
.LC25:
	.string	"certificate file"
	.section	.rodata.str1.8
	.align 8
.LC26:
	.string	"receipt signer certificate file"
	.section	.rodata.str1.1
.LC27:
	.string	"signing key file"
	.section	.rodata.str1.8
	.align 8
.LC28:
	.string	"Bad input format for CMS file\n"
	.section	.rodata.str1.1
.LC29:
	.string	"Error reading S/MIME message\n"
.LC30:
	.string	"Can't read content file %s\n"
.LC31:
	.string	"w"
.LC32:
	.string	"Error writing certs to %s\n"
.LC33:
	.string	"Can't open receipt file %s\n"
.LC34:
	.string	"Bad input format for receipt\n"
.LC35:
	.string	"Error reading receipt\n"
.LC36:
	.string	"parameter error \"%s\"\n"
	.section	.rodata.str1.8
	.align 8
.LC37:
	.string	"../deps/openssl/openssl/apps/cms.c"
	.align 8
.LC38:
	.string	"Signed Receipt Request Creation Error\n"
	.section	.rodata.str1.1
.LC39:
	.string	"signer certificate"
.LC40:
	.string	"Error creating CMS structure\n"
	.section	.rodata.str1.8
	.align 8
.LC41:
	.string	"Error decrypting CMS using secret key\n"
	.align 8
.LC42:
	.string	"Error decrypting CMS using private key\n"
	.align 8
.LC43:
	.string	"Error decrypting CMS using password\n"
	.align 8
.LC44:
	.string	"Error decrypting CMS structure\n"
	.section	.rodata.str1.1
.LC45:
	.string	"Verification successful\n"
.LC46:
	.string	"Verification failure\n"
.LC47:
	.string	"Error writing signers to %s\n"
.LC48:
	.string	"Signer %d:\n"
.LC49:
	.string	"  No Receipt Request\n"
	.section	.rodata.str1.8
	.align 8
.LC50:
	.string	"  Receipt Request Parse Error\n"
	.section	.rodata.str1.1
.LC51:
	.string	"  Signed Content ID:\n"
.LC52:
	.string	"  Receipts From"
.LC53:
	.string	" List:\n"
.LC54:
	.string	": First Tier\n"
.LC55:
	.string	": All\n"
.LC56:
	.string	" Unknown (%d)\n"
.LC57:
	.string	"  Receipts To:\n"
.LC58:
	.string	"To: %s%s"
.LC59:
	.string	"From: %s%s"
.LC60:
	.string	"Subject: %s%s"
	.section	.rodata.str1.8
	.align 8
.LC61:
	.string	"Bad output format for CMS file\n"
	.text
	.p2align 4
	.globl	cms_main
	.type	cms_main, @function
cms_main:
.LFB1531:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	$1, %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%edi, %r12d
	pushq	%rbx
	subq	$456, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -128(%rbp)
	movq	$0, -136(%rbp)
	movq	$0, -120(%rbp)
	movq	$0, -112(%rbp)
	movq	$0, -104(%rbp)
	movl	$32775, -156(%rbp)
	movl	$32775, -152(%rbp)
	movl	$32775, -148(%rbp)
	movl	$32773, -144(%rbp)
	call	X509_VERIFY_PARAM_new@PLT
	movq	%rax, -168(%rbp)
	testq	%rax, %rax
	jne	.L504
.L36:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L505
	leaq	-40(%rbp), %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L504:
	.cfi_restore_state
	movq	%r13, %rsi
	movl	%r12d, %edi
	leaq	cms_options(%rip), %rdx
	xorl	%ebx, %ebx
	call	opt_init@PLT
	movl	$64, %r14d
	movq	$0, -256(%rbp)
	xorl	%r12d, %r12d
	movq	%rax, -288(%rbp)
	leaq	.LC1(%rip), %rax
	leaq	.L43(%rip), %r13
	movq	%rax, -384(%rbp)
	leaq	-128(%rbp), %rax
	movq	$0, -240(%rbp)
	movq	$0, -360(%rbp)
	movq	$0, -416(%rbp)
	movq	$0, -368(%rbp)
	movl	$0, -344(%rbp)
	movl	$-1, -232(%rbp)
	movl	$0, -476(%rbp)
	movl	$0, -420(%rbp)
	movl	$0, -448(%rbp)
	movl	$0, -340(%rbp)
	movq	$0, -248(%rbp)
	movq	$0, -472(%rbp)
	movq	$0, -464(%rbp)
	movq	$0, -456(%rbp)
	movq	$0, -352(%rbp)
	movq	$0, -208(%rbp)
	movq	$0, -264(%rbp)
	movq	$0, -376(%rbp)
	movq	$0, -408(%rbp)
	movq	$0, -296(%rbp)
	movl	$0, -444(%rbp)
	movl	$0, -424(%rbp)
	movq	$0, -392(%rbp)
	movq	$0, -440(%rbp)
	movq	$0, -432(%rbp)
	movq	$0, -400(%rbp)
	movq	$0, -200(%rbp)
	movq	$0, -176(%rbp)
	movq	$0, -280(%rbp)
	movq	$0, -184(%rbp)
	movq	$0, -192(%rbp)
	movq	$0, -224(%rbp)
	movq	$0, -216(%rbp)
	movq	$0, -336(%rbp)
	movq	$0, -320(%rbp)
	movq	$0, -328(%rbp)
	movq	$0, -272(%rbp)
	movq	%rax, -304(%rbp)
.L38:
	call	opt_next@PLT
	testl	%eax, %eax
	je	.L506
.L148:
	cmpl	$73, %eax
	jg	.L39
	cmpl	$-1, %eax
	jl	.L38
	addl	$1, %eax
	cmpl	$74, %eax
	ja	.L38
	movslq	0(%r13,%rax,4), %rax
	addq	%r13, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L43:
	.long	.L115-.L43
	.long	.L38-.L43
	.long	.L114-.L43
	.long	.L113-.L43
	.long	.L112-.L43
	.long	.L111-.L43
	.long	.L110-.L43
	.long	.L109-.L43
	.long	.L296-.L43
	.long	.L108-.L43
	.long	.L107-.L43
	.long	.L106-.L43
	.long	.L105-.L43
	.long	.L104-.L43
	.long	.L103-.L43
	.long	.L102-.L43
	.long	.L101-.L43
	.long	.L100-.L43
	.long	.L99-.L43
	.long	.L98-.L43
	.long	.L97-.L43
	.long	.L96-.L43
	.long	.L95-.L43
	.long	.L94-.L43
	.long	.L93-.L43
	.long	.L92-.L43
	.long	.L91-.L43
	.long	.L90-.L43
	.long	.L89-.L43
	.long	.L88-.L43
	.long	.L87-.L43
	.long	.L86-.L43
	.long	.L85-.L43
	.long	.L84-.L43
	.long	.L83-.L43
	.long	.L82-.L43
	.long	.L81-.L43
	.long	.L80-.L43
	.long	.L79-.L43
	.long	.L78-.L43
	.long	.L77-.L43
	.long	.L493-.L43
	.long	.L75-.L43
	.long	.L74-.L43
	.long	.L73-.L43
	.long	.L72-.L43
	.long	.L71-.L43
	.long	.L70-.L43
	.long	.L69-.L43
	.long	.L68-.L43
	.long	.L67-.L43
	.long	.L66-.L43
	.long	.L65-.L43
	.long	.L64-.L43
	.long	.L63-.L43
	.long	.L62-.L43
	.long	.L61-.L43
	.long	.L60-.L43
	.long	.L59-.L43
	.long	.L58-.L43
	.long	.L57-.L43
	.long	.L56-.L43
	.long	.L55-.L43
	.long	.L54-.L43
	.long	.L53-.L43
	.long	.L52-.L43
	.long	.L51-.L43
	.long	.L50-.L43
	.long	.L49-.L43
	.long	.L48-.L43
	.long	.L47-.L43
	.long	.L46-.L43
	.long	.L45-.L43
	.long	.L44-.L43
	.long	.L42-.L43
	.text
.L296:
	movl	$34, %ebx
	call	opt_next@PLT
	testl	%eax, %eax
	jne	.L148
.L506:
	movl	%eax, %r15d
	call	opt_num_rest@PLT
	call	opt_rest@PLT
	cmpl	$-1, -232(%rbp)
	movq	%rax, %r13
	jne	.L360
	cmpq	$0, -224(%rbp)
	jne	.L360
	movl	%ebx, %eax
	andl	$64, %eax
	movl	%eax, -480(%rbp)
	jne	.L152
	cmpq	$0, -216(%rbp)
	jne	.L289
	movq	-192(%rbp), %rax
	orq	-184(%rbp), %rax
	movq	%rax, -248(%rbp)
	jne	.L507
	cmpl	$34, %ebx
	je	.L508
	cmpl	$17, %ebx
	je	.L509
	testl	%ebx, %ebx
	je	.L510
.L164:
	movq	-264(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	leaq	-104(%rbp), %rdx
	call	app_passwd@PLT
	testl	%eax, %eax
	je	.L511
	movq	$0, -184(%rbp)
	andl	$-65, %r14d
	movq	$0, -192(%rbp)
.L166:
	movl	%ebx, %eax
	andl	$32, %eax
	movl	%eax, -248(%rbp)
	testb	$16, %bl
	jne	.L167
	testb	$-128, %r14b
	je	.L168
	movl	$2, -152(%rbp)
	testl	%eax, %eax
	jne	.L169
.L171:
	movl	$2, -156(%rbp)
.L170:
	cmpl	$17, %ebx
	jne	.L169
	cmpq	$0, -128(%rbp)
	je	.L512
.L172:
	cmpq	$0, -240(%rbp)
	setne	%dl
	cmpq	$0, -256(%rbp)
	sete	%al
	andb	%al, %dl
	movb	%dl, -481(%rbp)
	jne	.L513
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L174
	cmpq	$0, -280(%rbp)
	je	.L514
.L174:
	testq	%rdi, %rdi
	je	.L176
	movl	%r14d, -264(%rbp)
	movq	-280(%rbp), %r14
	jmp	.L175
	.p2align 4,,10
	.p2align 3
.L516:
	movq	%r14, %rdi
	addq	$8, %r13
	call	OPENSSL_sk_push@PLT
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L515
.L175:
	movl	$32773, %esi
	leaq	.LC13(%rip), %rdx
	call	load_cert@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	jne	.L516
	movq	%rax, -264(%rbp)
	xorl	%r8d, %r8d
	movq	$0, -288(%rbp)
	movq	$0, -304(%rbp)
	movq	$0, -312(%rbp)
	movq	$0, -176(%rbp)
.L500:
	movq	$0, -232(%rbp)
	movl	$2, %r15d
	movq	$0, -200(%rbp)
	movq	$0, -296(%rbp)
	movq	$0, -248(%rbp)
	jmp	.L481
.L109:
	movl	$17, %ebx
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L39:
	cmpl	$2030, %eax
	jg	.L116
	cmpl	$2000, %eax
	jg	.L117
	leal	-1501(%rax), %edx
	cmpl	$1, %edx
	ja	.L38
	movl	%eax, %edi
	call	opt_rand@PLT
	testl	%eax, %eax
	jne	.L38
	jmp	.L312
	.p2align 4,,10
	.p2align 3
.L116:
	cmpl	$2032, %eax
	jne	.L38
	call	opt_unknown@PLT
	movq	-304(%rbp), %rsi
	movq	%rax, %rdi
	call	opt_cipher@PLT
	testl	%eax, %eax
	jne	.L38
	jmp	.L312
.L65:
	movl	$1, -448(%rbp)
.L493:
	movl	$1, -340(%rbp)
	jmp	.L38
.L509:
	cmpq	$0, 0(%r13)
	jne	.L164
	movq	-360(%rbp), %rax
	orq	-240(%rbp), %rax
	orq	-280(%rbp), %rax
	movq	%rax, -192(%rbp)
	jne	.L164
	movq	bio_err(%rip), %rdi
	leaq	.LC21(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	$0, -224(%rbp)
	movq	$0, -240(%rbp)
	movq	$0, -280(%rbp)
	movq	$0, -216(%rbp)
	movq	$0, -184(%rbp)
.L115:
	movq	-288(%rbp), %rdx
	movq	bio_err(%rip), %rdi
	leaq	.LC5(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L312:
	xorl	%r8d, %r8d
	movl	$1, %r15d
.L498:
	movq	$0, -288(%rbp)
	movq	$0, -304(%rbp)
	movq	$0, -312(%rbp)
	movq	$0, -176(%rbp)
	movq	$0, -232(%rbp)
	movq	$0, -200(%rbp)
	movq	$0, -296(%rbp)
	movq	$0, -248(%rbp)
	movq	$0, -264(%rbp)
.L481:
	movq	bio_err(%rip), %rdi
	movq	%r8, -208(%rbp)
	call	ERR_print_errors@PLT
	movq	-208(%rbp), %r8
.L284:
	movq	X509_free@GOTPCREL(%rip), %r13
	movq	-280(%rbp), %rdi
	movq	%r8, -208(%rbp)
	leaq	.LC37(%rip), %rbx
	movq	%r13, %rsi
	call	OPENSSL_sk_pop_free@PLT
	movq	-112(%rbp), %rdi
	movq	%r13, %rsi
	call	OPENSSL_sk_pop_free@PLT
	movq	-168(%rbp), %rdi
	call	X509_VERIFY_PARAM_free@PLT
	movq	-192(%rbp), %rdi
	call	OPENSSL_sk_free@PLT
	movq	-184(%rbp), %rdi
	call	OPENSSL_sk_free@PLT
	movq	-240(%rbp), %rdi
	movl	$1085, %edx
	leaq	.LC37(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-256(%rbp), %rdi
	movl	$1086, %edx
	leaq	.LC37(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-208(%rbp), %r8
	movl	$1087, %edx
	leaq	.LC37(%rip), %rsi
	movq	%r8, %rdi
	call	CRYPTO_free@PLT
	movq	-272(%rbp), %rdi
	call	ASN1_OBJECT_free@PLT
	movq	-232(%rbp), %rdi
	call	CMS_ReceiptRequest_free@PLT
	movq	-216(%rbp), %rdi
	call	OPENSSL_sk_free@PLT
	movq	-224(%rbp), %rdi
	call	OPENSSL_sk_free@PLT
	testq	%r12, %r12
	je	.L286
	.p2align 4,,10
	.p2align 3
.L285:
	movq	8(%r12), %rdi
	call	OPENSSL_sk_free@PLT
	movq	%r12, %rdi
	movq	16(%r12), %r12
	movq	%rbx, %rsi
	movl	$1096, %edx
	call	CRYPTO_free@PLT
	testq	%r12, %r12
	jne	.L285
.L286:
	movq	-288(%rbp), %rdi
	call	X509_STORE_free@PLT
	xorl	%edi, %edi
	call	X509_free@PLT
	movq	-312(%rbp), %rdi
	call	X509_free@PLT
	movq	-304(%rbp), %rdi
	call	X509_free@PLT
	movq	-176(%rbp), %rdi
	call	EVP_PKEY_free@PLT
	movq	-200(%rbp), %rdi
	call	CMS_ContentInfo_free@PLT
	movq	-328(%rbp), %rdi
	call	CMS_ContentInfo_free@PLT
	movq	-320(%rbp), %rdi
	call	release_engine@PLT
	movq	-296(%rbp), %rdi
	call	BIO_free@PLT
	movq	-264(%rbp), %rdi
	call	BIO_free@PLT
	movq	-136(%rbp), %rdi
	call	BIO_free@PLT
	movq	-248(%rbp), %rdi
	call	BIO_free_all@PLT
	movq	-104(%rbp), %rdi
	movl	$1111, %edx
	leaq	.LC37(%rip), %rsi
	call	CRYPTO_free@PLT
	jmp	.L36
.L78:
	andl	$-4097, %r14d
	jmp	.L38
.L79:
	orl	$4096, %r14d
	jmp	.L38
.L80:
	orl	$8, %r14d
	jmp	.L38
.L81:
	orl	$4, %r14d
	jmp	.L38
.L82:
	orl	$12, %r14d
	jmp	.L38
.L83:
	orl	$65536, %r14d
	jmp	.L38
.L84:
	orb	$-128, %r14b
	jmp	.L38
.L85:
	orl	$512, %r14d
	jmp	.L38
.L86:
	andl	$-65, %r14d
	jmp	.L38
.L87:
	orl	$256, %r14d
	jmp	.L38
.L77:
	leaq	.LC2(%rip), %rax
	orl	$2048, %r14d
	movq	%rax, -384(%rbp)
	jmp	.L38
.L42:
	call	opt_arg@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	setup_engine@PLT
	movq	%rax, -320(%rbp)
	jmp	.L38
.L113:
	call	opt_arg@PLT
	leaq	-156(%rbp), %rdx
	movl	$10, %esi
	movq	%rax, %rdi
	call	opt_format@PLT
	testl	%eax, %eax
	jne	.L38
	jmp	.L115
.L114:
	leaq	cms_options(%rip), %rdi
	xorl	%r15d, %r15d
	call	opt_help@PLT
	xorl	%r8d, %r8d
	movq	$0, -288(%rbp)
	movq	$0, -304(%rbp)
	movq	$0, -312(%rbp)
	movq	$0, -176(%rbp)
	movq	$0, -232(%rbp)
	movq	$0, -200(%rbp)
	movq	$0, -296(%rbp)
	movq	$0, -248(%rbp)
	movq	$0, -264(%rbp)
	jmp	.L284
.L104:
	movl	$1, -344(%rbp)
	jmp	.L38
.L105:
	movl	$36, %ebx
	jmp	.L38
.L106:
	movl	$118, %ebx
	jmp	.L38
.L107:
	movl	$63, %ebx
	jmp	.L38
.L108:
	movl	$83, %ebx
	jmp	.L38
.L110:
	call	opt_arg@PLT
	movq	%rax, -408(%rbp)
	jmp	.L38
.L111:
	call	opt_arg@PLT
	movq	%rax, -296(%rbp)
	jmp	.L38
.L112:
	call	opt_arg@PLT
	leaq	-152(%rbp), %rdx
	movl	$10, %esi
	movq	%rax, %rdi
	call	opt_format@PLT
	testl	%eax, %eax
	jne	.L38
	jmp	.L115
.L88:
	orl	$2, %r14d
	jmp	.L38
.L89:
	orl	$32, %r14d
	jmp	.L38
.L90:
	orl	$16, %r14d
	jmp	.L38
.L91:
	orl	$524288, %r14d
	jmp	.L38
.L92:
	orl	$1, %r14d
	jmp	.L38
.L93:
	orl	$131072, %r14d
	jmp	.L38
.L94:
	movl	$30, %ebx
	jmp	.L38
.L95:
	movl	$45, %ebx
	jmp	.L38
.L96:
	movl	$43, %ebx
	jmp	.L38
.L97:
	movl	$28, %ebx
	jmp	.L38
.L98:
	movl	$26, %ebx
	jmp	.L38
.L99:
	movl	$41, %ebx
	jmp	.L38
.L100:
	movl	$24, %ebx
	jmp	.L38
.L101:
	movl	$39, %ebx
	jmp	.L38
.L102:
	movl	$53, %ebx
	jmp	.L38
.L103:
	call	opt_arg@PLT
	movl	$48, %ebx
	movq	%rax, -376(%rbp)
	jmp	.L38
.L44:
	call	EVP_des_ede3_wrap@PLT
	movq	%rax, -336(%rbp)
	jmp	.L38
.L45:
	call	EVP_aes_256_wrap@PLT
	movq	%rax, -336(%rbp)
	jmp	.L38
.L46:
	call	EVP_aes_192_wrap@PLT
	movq	%rax, -336(%rbp)
	jmp	.L38
.L47:
	call	EVP_aes_128_wrap@PLT
	movq	%rax, -336(%rbp)
	jmp	.L38
.L48:
	cmpq	$0, -216(%rbp)
	je	.L517
.L124:
	call	opt_arg@PLT
	movq	-216(%rbp), %rdi
	movq	%rax, %rsi
	call	OPENSSL_sk_push@PLT
	jmp	.L38
.L49:
	cmpq	$0, -224(%rbp)
	je	.L518
.L123:
	call	opt_arg@PLT
	movq	-224(%rbp), %rdi
	movq	%rax, %rsi
	call	OPENSSL_sk_push@PLT
	jmp	.L38
.L50:
	cmpl	$17, %ebx
	je	.L519
	movq	-200(%rbp), %rax
	orq	-208(%rbp), %rax
	jne	.L143
	cmpq	$0, -184(%rbp)
	je	.L141
	movl	$-1, %r15d
.L144:
	movq	-184(%rbp), %rdi
	call	OPENSSL_sk_num@PLT
	leal	(%rax,%r15), %edx
.L142:
	testl	%edx, %edx
	js	.L141
.L145:
	movq	-248(%rbp), %rax
	testq	%rax, %rax
	je	.L146
	cmpl	%edx, (%rax)
	je	.L147
.L146:
	leaq	.LC15(%rip), %rsi
	movl	$24, %edi
	movl	%edx, -312(%rbp)
	call	app_malloc@PLT
	movl	-312(%rbp), %edx
	movq	%rax, %r15
	movl	%edx, (%rax)
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, 8(%r15)
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L309
	movq	$0, 16(%r15)
	testq	%r12, %r12
	je	.L310
	movq	-248(%rbp), %rax
	movq	%r15, -248(%rbp)
	movq	%r15, 16(%rax)
.L147:
	call	opt_arg@PLT
	movq	%rax, %rsi
	movq	-248(%rbp), %rax
	movq	8(%rax), %rdi
	call	OPENSSL_sk_push@PLT
	jmp	.L38
.L51:
	call	opt_arg@PLT
	leaq	-144(%rbp), %rdx
	movl	$1982, %esi
	movq	%rax, %rdi
	call	opt_format@PLT
	testl	%eax, %eax
	jne	.L38
	jmp	.L115
.L52:
	cmpq	$0, -200(%rbp)
	je	.L134
	cmpq	$0, -208(%rbp)
	je	.L520
	cmpq	$0, -192(%rbp)
	je	.L521
.L136:
	movq	-208(%rbp), %rsi
	movq	-192(%rbp), %rdi
	call	OPENSSL_sk_push@PLT
	cmpq	$0, -184(%rbp)
	je	.L522
.L137:
	movq	-200(%rbp), %rsi
	movq	-184(%rbp), %rdi
	call	OPENSSL_sk_push@PLT
	movq	$0, -208(%rbp)
.L134:
	call	opt_arg@PLT
	movq	%rax, -200(%rbp)
	jmp	.L38
.L53:
	call	opt_arg@PLT
	leaq	-120(%rbp), %rsi
	movq	%rax, %rdi
	call	opt_md@PLT
	testl	%eax, %eax
	jne	.L38
	jmp	.L312
.L54:
	call	opt_arg@PLT
	movq	%rax, -392(%rbp)
	jmp	.L38
.L55:
	cmpl	$17, %ebx
	jne	.L138
	cmpq	$0, -280(%rbp)
	je	.L523
.L139:
	call	opt_arg@PLT
	movl	$32773, %esi
	leaq	.LC13(%rip), %rdx
	movq	%rax, %rdi
	call	load_cert@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L309
	movq	-280(%rbp), %rdi
	call	OPENSSL_sk_push@PLT
	jmp	.L38
.L56:
	cmpq	$0, -208(%rbp)
	je	.L130
	cmpq	$0, -192(%rbp)
	je	.L524
.L131:
	movq	-208(%rbp), %r15
	movq	-192(%rbp), %rdi
	movq	%r15, %rsi
	call	OPENSSL_sk_push@PLT
	movq	-200(%rbp), %rcx
	movq	%r15, %rax
	testq	%rcx, %rcx
	cmovne	%rcx, %rax
	cmpq	$0, -184(%rbp)
	movq	%rax, -200(%rbp)
	je	.L525
.L133:
	movq	-200(%rbp), %rsi
	movq	-184(%rbp), %rdi
	call	OPENSSL_sk_push@PLT
	movq	$0, -200(%rbp)
.L130:
	call	opt_arg@PLT
	movq	%rax, -208(%rbp)
	jmp	.L38
.L57:
	call	opt_arg@PLT
	movq	%rax, -472(%rbp)
	jmp	.L38
.L58:
	call	opt_arg@PLT
	movq	%rax, -464(%rbp)
	jmp	.L38
.L59:
	call	opt_arg@PLT
	movq	%rax, -456(%rbp)
	jmp	.L38
.L60:
	call	opt_arg@PLT
	movq	%rax, -264(%rbp)
	jmp	.L38
.L61:
	cmpq	$0, -272(%rbp)
	jne	.L526
	call	opt_arg@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	OBJ_txt2obj@PLT
	movq	%rax, -272(%rbp)
	testq	%rax, %rax
	jne	.L38
	call	opt_arg@PLT
	movq	bio_err(%rip), %rdi
	leaq	.LC11(%rip), %rsi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L115
.L62:
	call	opt_arg@PLT
	movq	%rax, -360(%rbp)
	jmp	.L38
.L63:
	cmpq	$0, -256(%rbp)
	jne	.L527
	call	opt_arg@PLT
	leaq	-96(%rbp), %rsi
	movq	%rax, %rdi
	call	OPENSSL_hexstr2buf@PLT
	movq	%rax, -256(%rbp)
	testq	%rax, %rax
	je	.L528
	movq	-96(%rbp), %rax
	movq	%rax, -416(%rbp)
	jmp	.L38
.L64:
	cmpq	$0, -240(%rbp)
	jne	.L529
	call	opt_arg@PLT
	leaq	-96(%rbp), %rsi
	movq	%rax, %rdi
	call	OPENSSL_hexstr2buf@PLT
	movq	%rax, -240(%rbp)
	testq	%rax, %rax
	je	.L530
	movq	-96(%rbp), %rax
	movq	%rax, -368(%rbp)
	jmp	.L38
.L66:
	call	opt_arg@PLT
	movq	%rax, -400(%rbp)
	jmp	.L38
.L67:
	movl	$1, -424(%rbp)
	jmp	.L38
.L68:
	movl	$1, -444(%rbp)
	jmp	.L38
.L69:
	call	opt_arg@PLT
	movq	%rax, -440(%rbp)
	jmp	.L38
.L70:
	call	opt_arg@PLT
	movq	%rax, -432(%rbp)
	jmp	.L38
.L71:
	call	opt_arg@PLT
	movq	%rax, -176(%rbp)
	jmp	.L38
.L72:
	movl	-148(%rbp), %eax
	cmpl	$32775, %eax
	je	.L531
	cmpl	$32773, %eax
	je	.L532
	cmpl	$4, %eax
	jne	.L38
	call	opt_arg@PLT
	leaq	-148(%rbp), %rdx
	movl	$10, %esi
	movq	%rax, %rdi
	call	opt_format@PLT
	testl	%eax, %eax
	jne	.L38
	jmp	.L115
.L73:
	movl	$1, -232(%rbp)
	jmp	.L38
.L74:
	movl	$0, -232(%rbp)
	jmp	.L38
.L75:
	movl	$1, -476(%rbp)
	jmp	.L38
.L360:
	cmpq	$0, -216(%rbp)
	je	.L533
	movl	%ebx, %eax
	andl	$64, %eax
	movl	%eax, -480(%rbp)
	je	.L289
.L152:
	cmpq	$0, -200(%rbp)
	je	.L156
	cmpq	$0, -208(%rbp)
	je	.L534
.L482:
	cmpq	$0, -192(%rbp)
	je	.L535
.L159:
	movq	-208(%rbp), %rsi
	movq	-192(%rbp), %rdi
	call	OPENSSL_sk_push@PLT
	cmpq	$0, -184(%rbp)
	je	.L536
.L160:
	movq	-200(%rbp), %rax
	movq	-208(%rbp), %rcx
	movq	-184(%rbp), %rdi
	testq	%rax, %rax
	cmovne	%rax, %rcx
	movq	%rcx, %rsi
	call	OPENSSL_sk_push@PLT
.L162:
	movq	-264(%rbp), %rdi
	leaq	-104(%rbp), %rdx
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	call	app_passwd@PLT
	movq	$0, -208(%rbp)
	movq	$0, -200(%rbp)
	testl	%eax, %eax
	jne	.L166
	movq	-192(%rbp), %rax
	movq	%rax, -248(%rbp)
.L287:
	movq	bio_err(%rip), %rdi
	leaq	.LC23(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	-248(%rbp), %rax
	movq	%rax, -192(%rbp)
	jmp	.L312
	.p2align 4,,10
	.p2align 3
.L117:
	movq	-168(%rbp), %rsi
	movl	%eax, %edi
	call	opt_verify@PLT
	testl	%eax, %eax
	je	.L312
	addl	$1, -420(%rbp)
	jmp	.L38
.L138:
	call	opt_arg@PLT
	movq	%rax, -352(%rbp)
	jmp	.L38
.L143:
	xorl	%r15d, %r15d
	cmpq	$0, -184(%rbp)
	jne	.L144
	xorl	%edx, %edx
	jmp	.L145
.L156:
	cmpq	$0, -208(%rbp)
	jne	.L482
	cmpq	$0, -192(%rbp)
	jne	.L162
	movq	bio_err(%rip), %rdi
	leaq	.LC19(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L115
.L524:
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, -192(%rbp)
	testq	%rax, %rax
	jne	.L131
.L314:
	movq	$0, -288(%rbp)
	xorl	%r8d, %r8d
	movl	$1, %r15d
	movq	$0, -304(%rbp)
	movq	$0, -312(%rbp)
	movq	$0, -176(%rbp)
	movq	$0, -232(%rbp)
	movq	$0, -200(%rbp)
	movq	$0, -296(%rbp)
	movq	$0, -248(%rbp)
	movq	$0, -264(%rbp)
	jmp	.L481
.L525:
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, -184(%rbp)
	testq	%rax, %rax
	jne	.L133
	jmp	.L314
	.p2align 4,,10
	.p2align 3
.L522:
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, -184(%rbp)
	testq	%rax, %rax
	jne	.L137
	jmp	.L314
	.p2align 4,,10
	.p2align 3
.L521:
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, -192(%rbp)
	testq	%rax, %rax
	jne	.L136
	jmp	.L314
	.p2align 4,,10
	.p2align 3
.L289:
	movq	bio_err(%rip), %rdi
	leaq	.LC17(%rip), %rsi
	call	BIO_puts@PLT
	jmp	.L115
.L168:
	movl	-248(%rbp), %eax
	testl	%eax, %eax
	je	.L170
.L169:
	movq	-176(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L177
	xorl	%ecx, %ecx
	leaq	-112(%rbp), %rsi
	movl	$32773, %edx
	leaq	.LC25(%rip), %r8
	call	load_certs@PLT
	testl	%eax, %eax
	je	.L290
.L177:
	cmpq	$0, -352(%rbp)
	setne	%dl
	cmpl	$34, %ebx
	sete	%al
	andb	%al, %dl
	movb	%dl, -481(%rbp)
	jne	.L537
	cmpl	$63, %ebx
	je	.L538
	cmpl	$34, %ebx
	je	.L539
.L182:
	movq	$0, -304(%rbp)
	cmpl	$83, %ebx
	sete	%dl
	cmpl	$63, %ebx
	sete	%al
	orb	%al, %dl
	movb	%dl, -481(%rbp)
	jne	.L181
	movq	$0, -312(%rbp)
	movq	$0, -176(%rbp)
.L185:
	movl	-156(%rbp), %edx
	movq	-296(%rbp), %rdi
	movl	$114, %esi
	call	bio_open_default@PLT
	movq	%rax, -264(%rbp)
	testq	%rax, %rax
	je	.L324
	movl	-248(%rbp), %r11d
	testl	%r11d, %r11d
	je	.L325
	movl	-156(%rbp), %eax
	cmpl	$32775, %eax
	je	.L540
	cmpl	$32773, %eax
	je	.L541
	cmpl	$4, %eax
	jne	.L190
	movq	-264(%rbp), %rdi
	xorl	%esi, %esi
	call	d2i_CMS_bio@PLT
	movq	%rax, -200(%rbp)
.L188:
	cmpq	$0, -200(%rbp)
	je	.L542
	movq	-400(%rbp), %r13
	testq	%r13, %r13
	je	.L192
	movq	-136(%rbp), %rdi
	call	BIO_free@PLT
	leaq	.LC3(%rip), %rsi
	movq	%r13, %rdi
	call	BIO_new_file@PLT
	movq	%rax, -248(%rbp)
	movq	%rax, -136(%rbp)
	testq	%rax, %rax
	je	.L543
.L192:
	cmpq	$0, -392(%rbp)
	je	.L186
	movq	-200(%rbp), %rdi
	call	CMS_get1_certs@PLT
	movq	-392(%rbp), %rdi
	leaq	.LC31(%rip), %rsi
	movq	%rax, %r13
	call	BIO_new_file@PLT
	movq	%rax, -248(%rbp)
	testq	%rax, %rax
	je	.L193
	xorl	%edx, %edx
	movq	%r12, -288(%rbp)
	movq	%rax, %r12
	movl	%ebx, -296(%rbp)
	movl	%edx, %ebx
	jmp	.L194
.L195:
	movl	%ebx, %esi
	movq	%r13, %rdi
	addl	$1, %ebx
	call	OPENSSL_sk_value@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	PEM_write_bio_X509@PLT
.L194:
	movq	%r13, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jl	.L195
	movq	-248(%rbp), %rdi
	movq	-288(%rbp), %r12
	movl	-296(%rbp), %ebx
	call	BIO_free@PLT
	movq	X509_free@GOTPCREL(%rip), %rsi
	movq	%r13, %rdi
	call	OPENSSL_sk_pop_free@PLT
.L186:
	movq	-376(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L326
	cmpl	$4, -148(%rbp)
	leaq	.LC4(%rip), %rax
	leaq	.LC3(%rip), %rsi
	cmovne	%rax, %rsi
	call	BIO_new_file@PLT
	movq	%rax, -296(%rbp)
	testq	%rax, %rax
	je	.L544
	movl	-148(%rbp), %eax
	cmpl	$32775, %eax
	je	.L545
	cmpl	$32773, %eax
	je	.L546
	cmpl	$4, %eax
	jne	.L202
	movq	-296(%rbp), %rdi
	xorl	%esi, %esi
	call	d2i_CMS_bio@PLT
	movq	%rax, -328(%rbp)
.L200:
	cmpq	$0, -328(%rbp)
	je	.L547
.L196:
	movl	-152(%rbp), %edx
	movq	-408(%rbp), %rdi
	movl	$119, %esi
	call	bio_open_default@PLT
	movq	%rax, -248(%rbp)
	testq	%rax, %rax
	je	.L328
	movq	$0, -288(%rbp)
	cmpl	$36, %ebx
	sete	%dl
	cmpl	$48, %ebx
	sete	%al
	orb	%al, %dl
	movb	%dl, -392(%rbp)
	jne	.L548
.L203:
	cmpl	$24, %ebx
	je	.L549
	cmpl	$26, %ebx
	je	.L550
	cmpl	$28, %ebx
	je	.L551
	cmpl	$17, %ebx
	je	.L552
	cmpl	$30, %ebx
	je	.L553
	cmpl	$63, %ebx
	je	.L554
	movl	-480(%rbp), %ecx
	testl	%ecx, %ecx
	je	.L339
	cmpl	$83, %ebx
	je	.L555
	movq	$0, -232(%rbp)
	orl	$32768, %r14d
.L230:
	movl	%r14d, %eax
	movq	%r12, -336(%rbp)
	xorl	%ecx, %ecx
	orl	$262144, %eax
	movl	%r14d, -376(%rbp)
	movl	%eax, -400(%rbp)
	movl	%r15d, -420(%rbp)
	movl	%ebx, -408(%rbp)
	movl	%ecx, %ebx
.L234:
	movq	-192(%rbp), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jge	.L556
	movq	-192(%rbp), %rdi
	movl	%ebx, %esi
	call	OPENSSL_sk_value@PLT
	movq	-184(%rbp), %rdi
	movl	%ebx, %esi
	movq	%rax, %r14
	movq	%rax, -208(%rbp)
	call	OPENSSL_sk_value@PLT
	leaq	.LC39(%rip), %rdx
	movl	$32773, %esi
	movq	%r14, %rdi
	movq	%rax, %r12
	call	load_cert@PLT
	movq	%rax, -304(%rbp)
	testq	%rax, %rax
	je	.L344
	movq	-320(%rbp), %r8
	movq	-104(%rbp), %rcx
	xorl	%edx, %edx
	movq	%r12, %rdi
	movl	-144(%rbp), %esi
	leaq	.LC27(%rip), %r9
	call	load_key@PLT
	movq	%rax, -176(%rbp)
	testq	%rax, %rax
	je	.L344
	movq	-336(%rbp), %rax
	testq	%rax, %rax
	je	.L235
	movq	%rax, %r12
	jmp	.L238
	.p2align 4,,10
	.p2align 3
.L236:
	movq	16(%r12), %r12
	testq	%r12, %r12
	je	.L235
.L238:
	cmpl	%ebx, (%r12)
	jne	.L236
	movl	-400(%rbp), %r8d
	movq	-120(%rbp), %rcx
	movq	-176(%rbp), %rdx
	movq	-304(%rbp), %rsi
	movq	-200(%rbp), %rdi
	call	CMS_add1_signer@PLT
	movq	%rax, -352(%rbp)
	testq	%rax, %rax
	je	.L557
	movq	%rax, %rdi
	xorl	%r14d, %r14d
	call	CMS_SignerInfo_get0_pkey_ctx@PLT
	movq	8(%r12), %r12
	movq	%rax, %r15
	movq	%r12, %rdi
	call	OPENSSL_sk_num@PLT
	testl	%eax, %eax
	jg	.L240
	jmp	.L239
	.p2align 4,,10
	.p2align 3
.L242:
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	OPENSSL_sk_value@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	movq	%rax, %r13
	call	pkey_ctrl_string@PLT
	testl	%eax, %eax
	jle	.L558
	addl	$1, %r14d
.L240:
	movq	%r12, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r14d
	jl	.L242
.L239:
	cmpq	$0, -232(%rbp)
	je	.L244
	movq	-232(%rbp), %rsi
	movq	-352(%rbp), %rdi
	call	CMS_add1_ReceiptRequest@PLT
	testl	%eax, %eax
	je	.L559
.L244:
	movq	-304(%rbp), %rdi
	addl	$1, %ebx
	call	X509_free@PLT
	movq	-176(%rbp), %rdi
	call	EVP_PKEY_free@PLT
	movq	$0, -304(%rbp)
	movq	$0, -176(%rbp)
	jmp	.L234
.L167:
	movl	-248(%rbp), %eax
	testl	%eax, %eax
	jne	.L169
	testb	$-128, %r14b
	jne	.L171
	jmp	.L170
.L519:
	movq	-280(%rbp), %rax
	testq	%rax, %rax
	je	.L141
	movq	%rax, %rdi
	call	OPENSSL_sk_num@PLT
	leal	-1(%rax), %edx
	jmp	.L142
.L518:
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, -224(%rbp)
	testq	%rax, %rax
	jne	.L123
	jmp	.L314
	.p2align 4,,10
	.p2align 3
.L517:
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, -216(%rbp)
	testq	%rax, %rax
	jne	.L124
	jmp	.L314
	.p2align 4,,10
	.p2align 3
.L508:
	movq	-200(%rbp), %rax
	orq	-352(%rbp), %rax
	jne	.L164
	movq	-360(%rbp), %rax
	orq	-240(%rbp), %rax
	movq	$0, -200(%rbp)
	movq	%rax, -192(%rbp)
	jne	.L164
	movq	bio_err(%rip), %rdi
	leaq	.LC20(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	-192(%rbp), %rax
	movq	$0, -224(%rbp)
	movq	$0, -240(%rbp)
	movq	%rax, -216(%rbp)
	movq	$0, -184(%rbp)
	jmp	.L115
.L310:
	movq	%r15, -248(%rbp)
	movq	%r15, %r12
	jmp	.L147
.L531:
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	SMIME_read_CMS@PLT
	movq	%rax, -328(%rbp)
	jmp	.L38
.L325:
	movq	$0, -200(%rbp)
	jmp	.L186
.L537:
	movq	-352(%rbp), %rdi
	leaq	.LC13(%rip), %rdx
	movl	$32773, %esi
	call	load_cert@PLT
	movq	%rax, -312(%rbp)
	testq	%rax, %rax
	je	.L560
	cmpq	$0, -200(%rbp)
	je	.L561
.L292:
	movb	$1, -481(%rbp)
	movq	$0, -304(%rbp)
.L184:
	movq	-320(%rbp), %r8
	movq	-104(%rbp), %rcx
	xorl	%edx, %edx
	leaq	.LC27(%rip), %r9
	movl	-144(%rbp), %esi
	movq	-200(%rbp), %rdi
	call	load_key@PLT
	movq	%rax, -176(%rbp)
	testq	%rax, %rax
	jne	.L185
	movq	$0, -288(%rbp)
	xorl	%r8d, %r8d
.L494:
	movq	$0, -232(%rbp)
	movl	$2, %r15d
	movq	$0, -200(%rbp)
	movq	$0, -296(%rbp)
	movq	$0, -248(%rbp)
	movq	$0, -264(%rbp)
	jmp	.L481
.L181:
	cmpq	$0, -200(%rbp)
	movb	$0, -481(%rbp)
	movq	$0, -312(%rbp)
	jne	.L184
	movq	-208(%rbp), %rax
	movq	%rax, -176(%rbp)
.L183:
	cmpq	$0, -176(%rbp)
	movq	$0, -312(%rbp)
	je	.L185
.L291:
	movq	-176(%rbp), %rax
	movq	%rax, -200(%rbp)
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L511:
	movq	$0, -184(%rbp)
	jmp	.L287
.L532:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	PEM_read_bio_CMS@PLT
	movq	%rax, -328(%rbp)
	jmp	.L38
.L523:
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, -280(%rbp)
	testq	%rax, %rax
	jne	.L139
	jmp	.L314
	.p2align 4,,10
	.p2align 3
.L507:
	movq	bio_err(%rip), %rdi
	leaq	.LC18(%rip), %rsi
	call	BIO_puts@PLT
	movq	$0, -224(%rbp)
	jmp	.L115
.L534:
	movq	bio_err(%rip), %rdi
	leaq	.LC12(%rip), %rsi
	call	BIO_puts@PLT
	jmp	.L115
.L535:
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, -192(%rbp)
	testq	%rax, %rax
	jne	.L159
	jmp	.L314
.L515:
	movl	-264(%rbp), %r14d
.L176:
	cmpq	$0, -176(%rbp)
	je	.L562
	movq	-176(%rbp), %rdi
	xorl	%ecx, %ecx
	leaq	-112(%rbp), %rsi
	leaq	.LC25(%rip), %r8
	movl	$32773, %edx
	call	load_certs@PLT
	testl	%eax, %eax
	jne	.L182
.L290:
	movq	bio_err(%rip), %rdi
	movl	$2, %r15d
	call	ERR_print_errors@PLT
	xorl	%r8d, %r8d
	jmp	.L498
.L538:
	movq	-208(%rbp), %rdi
	leaq	.LC26(%rip), %rdx
	movl	$32773, %esi
	call	load_cert@PLT
	movq	%rax, -304(%rbp)
	testq	%rax, %rax
	jne	.L181
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	xorl	%r8d, %r8d
	movq	$0, -288(%rbp)
	movq	$0, -312(%rbp)
	movq	$0, -176(%rbp)
	jmp	.L494
.L324:
	movq	$0, -288(%rbp)
	xorl	%r8d, %r8d
	jmp	.L500
.L533:
	movq	bio_err(%rip), %rdi
	leaq	.LC16(%rip), %rsi
	call	BIO_puts@PLT
	jmp	.L115
.L235:
	movl	-376(%rbp), %r8d
	movq	-120(%rbp), %rcx
	movq	-176(%rbp), %rdx
	movq	-304(%rbp), %rsi
	movq	-200(%rbp), %rdi
	call	CMS_add1_signer@PLT
	movq	%rax, -352(%rbp)
	testq	%rax, %rax
	jne	.L239
	movq	-336(%rbp), %r12
	movq	-352(%rbp), %r8
	movl	$3, %r15d
	jmp	.L481
.L202:
	movq	bio_err(%rip), %rdi
	leaq	.LC34(%rip), %rsi
	xorl	%eax, %eax
	movl	$2, %r15d
	call	BIO_printf@PLT
	xorl	%r8d, %r8d
	movq	$0, -288(%rbp)
	movq	$0, -232(%rbp)
	movq	$0, -248(%rbp)
	jmp	.L481
.L190:
	movq	bio_err(%rip), %rdi
	leaq	.LC28(%rip), %rsi
	xorl	%eax, %eax
	movl	$2, %r15d
	call	BIO_printf@PLT
	xorl	%r8d, %r8d
	movq	$0, -288(%rbp)
	movq	$0, -232(%rbp)
	movq	$0, -200(%rbp)
	movq	$0, -296(%rbp)
	movq	$0, -248(%rbp)
	jmp	.L481
.L513:
	movq	bio_err(%rip), %rdi
	leaq	.LC24(%rip), %rsi
	xorl	%eax, %eax
	movl	$2, %r15d
	call	BIO_printf@PLT
	xorl	%r8d, %r8d
	movq	$0, -256(%rbp)
	jmp	.L498
.L536:
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, -184(%rbp)
	testq	%rax, %rax
	jne	.L160
	jmp	.L314
.L510:
	movq	bio_err(%rip), %rdi
	leaq	.LC22(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	$0, -224(%rbp)
	movq	$0, -184(%rbp)
	movq	$0, -192(%rbp)
	jmp	.L115
.L326:
	movq	$0, -296(%rbp)
	jmp	.L196
.L512:
	call	EVP_des_ede3_cbc@PLT
	movq	%rax, -128(%rbp)
	jmp	.L172
.L562:
	movq	$0, -304(%rbp)
	movq	$0, -312(%rbp)
	jmp	.L185
.L548:
	movl	-444(%rbp), %ecx
	movl	-424(%rbp), %edx
	movq	-440(%rbp), %rsi
	movq	-432(%rbp), %rdi
	call	setup_verify@PLT
	movq	%rax, -288(%rbp)
	testq	%rax, %rax
	je	.L330
	leaq	cms_cb(%rip), %rsi
	movq	%rax, %rdi
	call	X509_STORE_set_verify_cb@PLT
	movl	-420(%rbp), %r10d
	testl	%r10d, %r10d
	je	.L203
	movq	-168(%rbp), %rsi
	movq	-288(%rbp), %rdi
	call	X509_STORE_set1_param@PLT
	jmp	.L203
.L545:
	movq	-296(%rbp), %rdi
	xorl	%esi, %esi
	call	SMIME_read_CMS@PLT
	movq	%rax, -328(%rbp)
	jmp	.L200
.L540:
	movq	-264(%rbp), %rdi
	leaq	-136(%rbp), %rsi
	call	SMIME_read_CMS@PLT
	movq	%rax, -200(%rbp)
	jmp	.L188
.L541:
	movq	-264(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	PEM_read_bio_CMS@PLT
	movq	%rax, -200(%rbp)
	jmp	.L188
.L546:
	movq	-296(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	PEM_read_bio_CMS@PLT
	movq	%rax, -328(%rbp)
	jmp	.L200
.L558:
	movq	bio_err(%rip), %rdi
	movq	%r13, %rdx
	leaq	.LC36(%rip), %rsi
	xorl	%eax, %eax
	movq	-336(%rbp), %r12
	movl	$3, %r15d
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	xorl	%r8d, %r8d
	jmp	.L481
.L328:
	movq	$0, -288(%rbp)
	xorl	%r8d, %r8d
	movl	$2, %r15d
	movq	$0, -232(%rbp)
	jmp	.L481
.L514:
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, -280(%rbp)
	testq	%rax, %rax
	je	.L317
	movq	0(%r13), %rdi
	jmp	.L174
.L549:
	movq	-264(%rbp), %rdi
	movl	%r14d, %esi
	call	CMS_data_create@PLT
	movq	$0, -232(%rbp)
	movq	%rax, -200(%rbp)
.L205:
	cmpq	$0, -200(%rbp)
	je	.L563
	cmpb	$0, -481(%rbp)
	jne	.L564
	cmpl	$39, %ebx
	je	.L565
	cmpl	$43, %ebx
	je	.L566
	cmpl	$41, %ebx
	je	.L567
	cmpl	$45, %ebx
	je	.L568
	cmpl	$36, %ebx
	je	.L569
	cmpb	$0, -392(%rbp)
	jne	.L570
.L273:
	movl	-340(%rbp), %edx
	testl	%edx, %edx
	je	.L275
	cmpl	$0, -448(%rbp)
	jne	.L571
.L355:
	xorl	%r15d, %r15d
.L503:
	xorl	%r8d, %r8d
	jmp	.L284
.L564:
	testl	$131072, %r14d
	jne	.L572
.L248:
	movq	-240(%rbp), %rax
	testq	%rax, %rax
	je	.L249
	movq	-416(%rbp), %r8
	movq	-256(%rbp), %rcx
	movq	%rax, %rsi
	movq	-368(%rbp), %rdx
	movq	-200(%rbp), %rdi
	call	CMS_decrypt_set1_key@PLT
	testl	%eax, %eax
	je	.L573
.L249:
	movq	-176(%rbp), %rax
	testq	%rax, %rax
	je	.L250
	movq	-312(%rbp), %rdx
	movq	-200(%rbp), %rdi
	movq	%rax, %rsi
	call	CMS_decrypt_set1_pkey@PLT
	testl	%eax, %eax
	je	.L574
.L250:
	movq	-360(%rbp), %rax
	testq	%rax, %rax
	je	.L251
	movq	-200(%rbp), %rdi
	movq	$-1, %rdx
	movq	%rax, %rsi
	call	CMS_decrypt_set1_password@PLT
	testl	%eax, %eax
	je	.L575
.L251:
	movq	-248(%rbp), %r8
	movl	%r14d, %r9d
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	-136(%rbp), %rcx
	movq	-200(%rbp), %rdi
	call	CMS_decrypt@PLT
	xorl	%r8d, %r8d
	testl	%eax, %eax
	jne	.L284
	movq	bio_err(%rip), %rdi
	leaq	.LC44(%rip), %rsi
	movq	%r8, -208(%rbp)
	movl	$4, %r15d
	call	BIO_printf@PLT
	movq	-208(%rbp), %r8
	jmp	.L481
.L550:
	movq	-120(%rbp), %rsi
	movq	-264(%rbp), %rdi
	movl	%r14d, %edx
	call	CMS_digest_create@PLT
	movq	$0, -232(%rbp)
	movq	%rax, -200(%rbp)
	jmp	.L205
.L529:
	call	opt_arg@PLT
	movq	bio_err(%rip), %rdi
	leaq	.LC6(%rip), %rsi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L115
.L193:
	movq	-392(%rbp), %rdx
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
	movl	$5, %r15d
	leaq	.LC32(%rip), %rsi
	call	BIO_printf@PLT
	xorl	%r8d, %r8d
	movq	$0, -288(%rbp)
	movq	$0, -232(%rbp)
	movq	$0, -296(%rbp)
	jmp	.L481
.L544:
	movq	-376(%rbp), %rdx
	movq	bio_err(%rip), %rdi
	leaq	.LC33(%rip), %rsi
	call	BIO_printf@PLT
.L499:
	movq	$0, -288(%rbp)
	xorl	%r8d, %r8d
	movl	$2, %r15d
	movq	$0, -232(%rbp)
	movq	$0, -248(%rbp)
	jmp	.L481
.L527:
	call	opt_arg@PLT
	movq	bio_err(%rip), %rdi
	leaq	.LC8(%rip), %rsi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L115
.L141:
	movq	bio_err(%rip), %rdi
	leaq	.LC14(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L115
.L526:
	call	opt_arg@PLT
	movq	bio_err(%rip), %rdi
	leaq	.LC10(%rip), %rsi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L115
.L309:
	movq	%rsi, -264(%rbp)
	xorl	%r8d, %r8d
	movl	$1, %r15d
	movq	$0, -288(%rbp)
	movq	$0, -304(%rbp)
	movq	$0, -312(%rbp)
	movq	$0, -176(%rbp)
	movq	$0, -232(%rbp)
	movq	$0, -200(%rbp)
	movq	$0, -296(%rbp)
	movq	$0, -248(%rbp)
	jmp	.L481
.L539:
	cmpq	$0, -200(%rbp)
	jne	.L576
	movq	-352(%rbp), %rax
	movb	$1, -481(%rbp)
	movq	$0, -304(%rbp)
	movq	%rax, -176(%rbp)
	jmp	.L183
.L560:
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	xorl	%r8d, %r8d
	movq	$0, -288(%rbp)
	movq	$0, -304(%rbp)
	movq	$0, -176(%rbp)
	jmp	.L494
.L547:
	movq	bio_err(%rip), %rdi
	leaq	.LC35(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L499
.L330:
	movq	$0, -232(%rbp)
	xorl	%r8d, %r8d
	movl	$2, %r15d
	jmp	.L481
.L542:
	movq	bio_err(%rip), %rdi
	leaq	.LC29(%rip), %rsi
	xorl	%eax, %eax
	movl	$2, %r15d
	call	BIO_printf@PLT
	xorl	%r8d, %r8d
	movq	$0, -288(%rbp)
	movq	$0, -232(%rbp)
	movq	$0, -296(%rbp)
	movq	$0, -248(%rbp)
	jmp	.L481
.L551:
	movq	-264(%rbp), %rdi
	movl	%r14d, %edx
	movl	$-1, %esi
	call	CMS_compress@PLT
	movq	$0, -232(%rbp)
	movq	%rax, -200(%rbp)
	jmp	.L205
.L528:
	call	opt_arg@PLT
	movq	bio_err(%rip), %rdi
	leaq	.LC9(%rip), %rsi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L115
.L530:
	call	opt_arg@PLT
	movq	bio_err(%rip), %rdi
	leaq	.LC7(%rip), %rsi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L314
.L520:
	movq	bio_err(%rip), %rdi
	leaq	.LC12(%rip), %rsi
	call	BIO_puts@PLT
	jmp	.L314
.L339:
	movq	$0, -232(%rbp)
	jmp	.L205
.L552:
	movl	%r14d, %eax
	movq	-128(%rbp), %rdx
	movq	-264(%rbp), %rsi
	xorl	%edi, %edi
	orb	$64, %ah
	movl	%eax, %ecx
	movl	%eax, -352(%rbp)
	call	CMS_encrypt@PLT
	movq	%rax, -200(%rbp)
	testq	%rax, %rax
	je	.L340
	movl	%r14d, %eax
	movq	%r12, -208(%rbp)
	xorl	%ecx, %ecx
	orl	$278528, %eax
	movl	%r14d, -392(%rbp)
	movl	%eax, -376(%rbp)
	movl	%r15d, -408(%rbp)
	movl	%ebx, -400(%rbp)
	movl	%ecx, %ebx
.L209:
	movq	-280(%rbp), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jge	.L577
	movq	-280(%rbp), %rdi
	movl	%ebx, %esi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %rsi
	movq	-208(%rbp), %rax
	testq	%rax, %rax
	je	.L210
	movq	%rax, %r13
	jmp	.L213
	.p2align 4,,10
	.p2align 3
.L211:
	movq	16(%r13), %r13
	testq	%r13, %r13
	je	.L210
.L213:
	cmpl	%ebx, 0(%r13)
	jne	.L211
	movl	-376(%rbp), %edx
	movq	-200(%rbp), %rdi
	call	CMS_add1_recipient_cert@PLT
	movq	%rax, -232(%rbp)
	testq	%rax, %rax
	je	.L501
	movq	%rax, %rdi
	xorl	%r14d, %r14d
	call	CMS_RecipientInfo_get0_pkey_ctx@PLT
	movq	8(%r13), %r13
	movq	%rax, %r15
	movq	%r13, %rdi
	call	OPENSSL_sk_num@PLT
	testl	%eax, %eax
	jg	.L215
	jmp	.L214
	.p2align 4,,10
	.p2align 3
.L217:
	movl	%r14d, %esi
	movq	%r13, %rdi
	call	OPENSSL_sk_value@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	movq	%rax, %r12
	call	pkey_ctrl_string@PLT
	testl	%eax, %eax
	jle	.L578
	addl	$1, %r14d
.L215:
	movq	%r13, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r14d
	jl	.L217
.L214:
	movq	-232(%rbp), %rdi
	call	CMS_RecipientInfo_type@PLT
	cmpl	$1, %eax
	jne	.L218
	cmpq	$0, -336(%rbp)
	jne	.L579
.L218:
	addl	$1, %ebx
	jmp	.L209
.L210:
	movl	-352(%rbp), %edx
	movq	-200(%rbp), %rdi
	call	CMS_add1_recipient_cert@PLT
	movq	%rax, -232(%rbp)
	testq	%rax, %rax
	jne	.L214
.L501:
	movq	-208(%rbp), %r12
.L495:
	xorl	%r8d, %r8d
	movl	$3, %r15d
	jmp	.L481
.L578:
	movq	bio_err(%rip), %rdi
	movq	%r12, %rdx
	leaq	.LC36(%rip), %rsi
	xorl	%eax, %eax
	movq	-208(%rbp), %r12
	movl	$3, %r15d
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	xorl	%r8d, %r8d
	movq	$0, -232(%rbp)
	jmp	.L481
.L579:
	movq	-232(%rbp), %rdi
	call	CMS_RecipientInfo_kari_get0_ctx@PLT
	movq	-336(%rbp), %rsi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rdi
	xorl	%r8d, %r8d
	call	EVP_EncryptInit_ex@PLT
	jmp	.L218
.L565:
	movq	-248(%rbp), %rsi
	movq	-200(%rbp), %rdi
	movl	%r14d, %edx
	call	CMS_data@PLT
	xorl	%r8d, %r8d
	testl	%eax, %eax
	jne	.L284
.L497:
	movl	$4, %r15d
	jmp	.L481
.L566:
	movq	-248(%rbp), %rdx
	movq	-136(%rbp), %rsi
	movl	%r14d, %ecx
	movq	-200(%rbp), %rdi
	call	CMS_uncompress@PLT
	xorl	%r8d, %r8d
	testl	%eax, %eax
	jne	.L284
	jmp	.L497
.L568:
	movq	-248(%rbp), %r8
	movq	-136(%rbp), %rcx
	movl	%r14d, %r9d
	movq	-368(%rbp), %rdx
	movq	-240(%rbp), %rsi
	movq	-200(%rbp), %rdi
	call	CMS_EncryptedData_decrypt@PLT
	xorl	%r8d, %r8d
	testl	%eax, %eax
	jne	.L284
	jmp	.L497
.L567:
	movq	-248(%rbp), %rdx
	movq	-136(%rbp), %rsi
	movl	%r14d, %ecx
	movq	-200(%rbp), %rdi
	call	CMS_digest_verify@PLT
	testl	%eax, %eax
	jle	.L274
.L502:
	movq	bio_err(%rip), %rdi
	leaq	.LC45(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	xorl	%r8d, %r8d
	jmp	.L284
.L543:
	movq	-400(%rbp), %rdx
	movq	bio_err(%rip), %rdi
	leaq	.LC30(%rip), %rsi
	movl	$2, %r15d
	call	BIO_printf@PLT
	xorl	%r8d, %r8d
	movq	$0, -288(%rbp)
	movq	$0, -232(%rbp)
	movq	$0, -296(%rbp)
	jmp	.L481
.L572:
	movq	-200(%rbp), %rdi
	movl	%r14d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	CMS_decrypt@PLT
	jmp	.L248
.L563:
	movq	bio_err(%rip), %rdi
	leaq	.LC40(%rip), %rsi
	xorl	%eax, %eax
	movl	$3, %r15d
	call	BIO_printf@PLT
	xorl	%r8d, %r8d
	jmp	.L481
.L505:
	call	__stack_chk_fail@PLT
.L577:
	movq	-240(%rbp), %rax
	movq	-208(%rbp), %r12
	movl	-392(%rbp), %r14d
	movl	-400(%rbp), %ebx
	movl	-408(%rbp), %r15d
	testq	%rax, %rax
	je	.L220
	pushq	%r9
	movq	-256(%rbp), %r8
	movq	%rax, %rdx
	xorl	%esi, %esi
	movq	-416(%rbp), %r9
	movq	-368(%rbp), %rcx
	pushq	$0
	pushq	$0
	movq	-200(%rbp), %rdi
	pushq	$0
	call	CMS_add0_recipient_key@PLT
	addq	$32, %rsp
	movq	%rax, -232(%rbp)
	testq	%rax, %rax
	je	.L495
	movq	$0, -256(%rbp)
.L220:
	cmpq	$0, -360(%rbp)
	je	.L224
	movq	-360(%rbp), %rdi
	movl	$858, %edx
	leaq	.LC37(%rip), %rsi
	call	CRYPTO_strdup@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L334
	pushq	%rsi
	movq	-200(%rbp), %rdi
	orq	$-1, %r9
	xorl	%ecx, %ecx
	pushq	$0
	xorl	%edx, %edx
	orl	$-1, %esi
	movq	%rax, -208(%rbp)
	call	CMS_add0_recipient_password@PLT
	popq	%rdi
	popq	%r8
	testq	%rax, %rax
	movq	%rax, -232(%rbp)
	movq	-208(%rbp), %r8
	je	.L580
.L224:
	andl	$4096, %r14d
	je	.L222
	movq	$0, -240(%rbp)
.L223:
	movq	$0, -232(%rbp)
	movl	-352(%rbp), %r14d
	jmp	.L273
.L554:
	movq	-200(%rbp), %rdi
	call	CMS_get0_SignerInfos@PLT
	movq	%rax, -232(%rbp)
	testq	%rax, %rax
	je	.L495
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	OPENSSL_sk_value@PLT
	movq	-112(%rbp), %rcx
	movq	-176(%rbp), %rdx
	movl	%r14d, %r8d
	movq	-304(%rbp), %rsi
	movq	%rax, %rdi
	call	CMS_sign_receipt@PLT
	movq	%rax, -232(%rbp)
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L495
	movq	-200(%rbp), %rdi
	call	CMS_ContentInfo_free@PLT
	movl	%r14d, -352(%rbp)
	movq	%r13, -200(%rbp)
	jmp	.L223
.L317:
	movq	$0, -288(%rbp)
	xorl	%r8d, %r8d
	movq	$0, -304(%rbp)
	movq	$0, -312(%rbp)
	movq	$0, -176(%rbp)
	jmp	.L494
.L344:
	movq	-336(%rbp), %r12
	xorl	%r8d, %r8d
	movl	$2, %r15d
	jmp	.L481
.L555:
	testb	$64, %r14b
	je	.L228
	cmpl	$32775, -152(%rbp)
	jne	.L228
	orl	$4096, %r14d
.L228:
	movq	-264(%rbp), %rcx
	movq	-112(%rbp), %rdx
	xorl	%edi, %edi
	xorl	%esi, %esi
	orl	$16384, %r14d
	movl	%r14d, %r8d
	call	CMS_sign@PLT
	movq	%rax, -200(%rbp)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L340
	movq	-272(%rbp), %rax
	testq	%rax, %rax
	je	.L229
	movq	%rax, %rsi
	call	CMS_set1_eContentType@PLT
.L229:
	movq	-216(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L341
	call	make_names_stack
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L231
	movq	-224(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L342
	call	make_names_stack
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L231
.L232:
	movl	-232(%rbp), %edx
	movq	%r13, %r8
	orl	$-1, %esi
	xorl	%edi, %edi
	call	CMS_ReceiptRequest_create0@PLT
	movq	%rax, -232(%rbp)
	testq	%rax, %rax
	jne	.L230
.L233:
	movq	bio_err(%rip), %rdi
	leaq	.LC38(%rip), %rsi
	movl	$3, %r15d
	call	BIO_puts@PLT
	xorl	%r8d, %r8d
	movq	$0, -232(%rbp)
	jmp	.L481
.L275:
	movl	-152(%rbp), %eax
	cmpl	$32775, %eax
	je	.L581
	cmpl	$32773, %eax
	je	.L582
	cmpl	$4, %eax
	jne	.L283
	movq	-264(%rbp), %rdx
	movq	-200(%rbp), %rsi
	movl	%r14d, %ecx
	movq	-248(%rbp), %rdi
	call	i2d_CMS_bio_stream@PLT
.L281:
	testl	%eax, %eax
	jg	.L355
	xorl	%r8d, %r8d
	movl	$6, %r15d
	jmp	.L481
.L570:
	movq	-288(%rbp), %rcx
	movq	-112(%rbp), %rdx
	movl	%r14d, %r8d
	movq	-200(%rbp), %rsi
	movq	-328(%rbp), %rdi
	call	CMS_verify_receipt@PLT
	testl	%eax, %eax
	jg	.L502
.L274:
	leaq	.LC46(%rip), %rsi
.L496:
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	xorl	%r8d, %r8d
	jmp	.L497
.L569:
	movq	-248(%rbp), %r8
	movq	-136(%rbp), %rcx
	movl	%r14d, %r9d
	movq	-288(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	-200(%rbp), %rdi
	call	CMS_verify@PLT
	testl	%eax, %eax
	jle	.L258
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
	leaq	.LC45(%rip), %rsi
	call	BIO_printf@PLT
	cmpq	$0, -208(%rbp)
	je	.L260
	movq	-200(%rbp), %rdi
	call	CMS_get0_signers@PLT
	movq	-208(%rbp), %rdi
	leaq	.LC31(%rip), %rsi
	movq	%rax, %r13
	call	BIO_new_file@PLT
	testq	%rax, %rax
	je	.L261
	xorl	%ebx, %ebx
	movq	%rax, %r14
	jmp	.L262
.L263:
	movl	%ebx, %esi
	movq	%r13, %rdi
	addl	$1, %ebx
	call	OPENSSL_sk_value@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	PEM_write_bio_X509@PLT
.L262:
	movq	%r13, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jl	.L263
	movq	%r14, %rdi
	call	BIO_free@PLT
	movq	%r13, %rdi
	call	OPENSSL_sk_free@PLT
.L260:
	cmpl	$0, -476(%rbp)
	je	.L355
	movq	-200(%rbp), %rdi
	xorl	%ebx, %ebx
	leaq	-88(%rbp), %r14
	call	CMS_get0_SignerInfos@PLT
	movq	%rax, %r13
	jmp	.L264
.L265:
	js	.L583
	movq	-88(%rbp), %rdi
	leaq	-80(%rbp), %r8
	leaq	-72(%rbp), %rcx
	leaq	-140(%rbp), %rdx
	leaq	-64(%rbp), %rsi
	call	CMS_ReceiptRequest_get0_values@PLT
	movq	bio_err(%rip), %rdi
	leaq	.LC51(%rip), %rsi
	call	BIO_puts@PLT
	movq	-64(%rbp), %rdi
	call	ASN1_STRING_length@PLT
	movq	-64(%rbp), %rdi
	movl	%eax, -208(%rbp)
	call	ASN1_STRING_get0_data@PLT
	movl	-208(%rbp), %edx
	movl	$4, %ecx
	movq	bio_err(%rip), %rdi
	movq	%rax, %rsi
	call	BIO_dump_indent@PLT
	movq	bio_err(%rip), %rdi
	leaq	.LC52(%rip), %rsi
	call	BIO_puts@PLT
	cmpq	$0, -72(%rbp)
	je	.L268
	movq	bio_err(%rip), %rdi
	leaq	.LC53(%rip), %rsi
	call	BIO_puts@PLT
	movq	-72(%rbp), %rdi
	call	gnames_stack_print
.L269:
	movq	bio_err(%rip), %rdi
	leaq	.LC57(%rip), %rsi
	call	BIO_puts@PLT
	movq	-80(%rbp), %rdi
	call	gnames_stack_print
.L266:
	movq	-88(%rbp), %rdi
	call	CMS_ReceiptRequest_free@PLT
.L264:
	movq	%r13, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jge	.L503
	movl	%ebx, %esi
	movq	%r13, %rdi
	addl	$1, %ebx
	call	OPENSSL_sk_value@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	CMS_get1_ReceiptRequest@PLT
	movq	bio_err(%rip), %rdi
	movl	%ebx, %edx
	leaq	.LC48(%rip), %rsi
	movl	%eax, -208(%rbp)
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movl	-208(%rbp), %ecx
	testl	%ecx, %ecx
	jne	.L265
	movq	bio_err(%rip), %rdi
	leaq	.LC49(%rip), %rsi
	call	BIO_puts@PLT
	jmp	.L266
.L553:
	movq	-368(%rbp), %rcx
	movq	-128(%rbp), %rsi
	movl	%r14d, %r8d
	movq	-240(%rbp), %rdx
	movq	-264(%rbp), %rdi
	call	CMS_EncryptedData_encrypt@PLT
	movq	$0, -232(%rbp)
	movq	%rax, -200(%rbp)
	jmp	.L205
.L222:
	movl	-352(%rbp), %ecx
	movq	-264(%rbp), %rsi
	xorl	%edx, %edx
	movq	-200(%rbp), %rdi
	call	CMS_final@PLT
	movq	$0, -240(%rbp)
	testl	%eax, %eax
	jne	.L223
	movq	$0, -232(%rbp)
	xorl	%r8d, %r8d
	movl	$3, %r15d
	jmp	.L481
.L556:
	movl	-408(%rbp), %ebx
	movq	-336(%rbp), %r12
	movl	-376(%rbp), %r14d
	movl	-420(%rbp), %r15d
	cmpl	$83, %ebx
	jne	.L205
	testl	$4096, %r14d
	jne	.L205
	movq	-264(%rbp), %rsi
	movq	-200(%rbp), %rdi
	xorl	%edx, %edx
	movl	%r14d, %ecx
	call	CMS_final@PLT
	testl	%eax, %eax
	jne	.L205
	jmp	.L495
.L283:
	leaq	.LC61(%rip), %rsi
	jmp	.L496
.L582:
	movq	-264(%rbp), %rdx
	movq	-200(%rbp), %rsi
	movl	%r14d, %ecx
	movq	-248(%rbp), %rdi
	call	PEM_write_bio_CMS_stream@PLT
	jmp	.L281
.L581:
	movq	-456(%rbp), %rax
	testq	%rax, %rax
	je	.L277
	movq	-384(%rbp), %rcx
	movq	-248(%rbp), %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	leaq	.LC58(%rip), %rsi
	call	BIO_printf@PLT
.L277:
	movq	-464(%rbp), %rax
	testq	%rax, %rax
	je	.L278
	movq	-384(%rbp), %rcx
	movq	-248(%rbp), %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	leaq	.LC59(%rip), %rsi
	call	BIO_printf@PLT
.L278:
	movq	-472(%rbp), %rax
	testq	%rax, %rax
	je	.L279
	movq	-384(%rbp), %rcx
	movq	-248(%rbp), %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	leaq	.LC60(%rip), %rsi
	call	BIO_printf@PLT
.L279:
	movl	%r14d, %ecx
	cmpl	$118, %ebx
	je	.L584
	movq	-264(%rbp), %rdx
	movq	-200(%rbp), %rsi
	movq	-248(%rbp), %rdi
	call	SMIME_write_CMS@PLT
	jmp	.L281
.L573:
	movq	bio_err(%rip), %rdi
	leaq	.LC41(%rip), %rsi
	movl	$4, %r15d
	call	BIO_puts@PLT
	xorl	%r8d, %r8d
	jmp	.L481
.L340:
	movq	$0, -232(%rbp)
	xorl	%r8d, %r8d
	movl	$3, %r15d
	jmp	.L481
.L559:
	movq	-336(%rbp), %r12
	xorl	%r8d, %r8d
	movl	$3, %r15d
	jmp	.L481
.L584:
	movq	-136(%rbp), %rdx
	movq	-200(%rbp), %rsi
	movq	-248(%rbp), %rdi
	call	SMIME_write_CMS@PLT
	jmp	.L281
.L574:
	movq	bio_err(%rip), %rdi
	leaq	.LC42(%rip), %rsi
	movl	$4, %r15d
	call	BIO_puts@PLT
	xorl	%r8d, %r8d
	jmp	.L481
.L557:
	movq	-336(%rbp), %r12
	movq	%rax, %r8
	movl	$3, %r15d
	jmp	.L481
.L575:
	movq	bio_err(%rip), %rdi
	leaq	.LC43(%rip), %rsi
	movl	$4, %r15d
	call	BIO_puts@PLT
	xorl	%r8d, %r8d
	jmp	.L481
.L580:
	movq	$0, -240(%rbp)
	movl	$3, %r15d
	jmp	.L481
.L334:
	movq	$0, -240(%rbp)
	movl	$3, %r15d
	movq	$0, -232(%rbp)
	jmp	.L481
.L561:
	movq	-352(%rbp), %rax
	movq	$0, -304(%rbp)
	movq	%rax, -176(%rbp)
	jmp	.L291
.L576:
	movq	$0, -312(%rbp)
	jmp	.L292
.L571:
	movq	-200(%rbp), %rsi
	movq	-248(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	call	CMS_ContentInfo_print_ctx@PLT
	xorl	%r8d, %r8d
	jmp	.L284
.L342:
	xorl	%ecx, %ecx
	jmp	.L232
.L231:
	movq	GENERAL_NAMES_free@GOTPCREL(%rip), %rsi
	movq	%r13, %rdi
	call	OPENSSL_sk_pop_free@PLT
	jmp	.L233
.L341:
	movq	$0, -232(%rbp)
	jmp	.L230
.L261:
	movq	-208(%rbp), %rdx
	movq	bio_err(%rip), %rdi
	movq	%rax, -336(%rbp)
	xorl	%eax, %eax
	leaq	.LC47(%rip), %rsi
	movl	$5, %r15d
	call	BIO_printf@PLT
	movq	-336(%rbp), %r8
	jmp	.L481
.L258:
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
	leaq	.LC46(%rip), %rsi
	call	BIO_printf@PLT
	xorl	%r8d, %r8d
	cmpl	$0, -344(%rbp)
	je	.L497
.L120:
	endbr64
	movl	verify_err(%rip), %r15d
	addl	$32, %r15d
	je	.L284
	jmp	.L481
.L268:
	movl	-140(%rbp), %edx
	movq	bio_err(%rip), %rdi
	cmpl	$1, %edx
	je	.L585
	testl	%edx, %edx
	jne	.L271
	leaq	.LC55(%rip), %rsi
	call	BIO_puts@PLT
	jmp	.L269
.L583:
	movq	bio_err(%rip), %rdi
	leaq	.LC50(%rip), %rsi
	call	BIO_puts@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	jmp	.L266
.L271:
	leaq	.LC56(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L269
.L585:
	leaq	.LC54(%rip), %rsi
	call	BIO_puts@PLT
	jmp	.L269
	.cfi_endproc
.LFE1531:
	.size	cms_main, .-cms_main
	.globl	cms_options
	.section	.rodata.str1.8
	.align 8
.LC62:
	.string	"Usage: %s [options] cert.pem...\n"
	.align 8
.LC63:
	.string	"  cert.pem... recipient certs for encryption\n"
	.section	.rodata.str1.1
.LC64:
	.string	"Valid options are:\n"
.LC65:
	.string	"help"
.LC66:
	.string	"Display this summary"
.LC67:
	.string	"inform"
	.section	.rodata.str1.8
	.align 8
.LC68:
	.string	"Input format SMIME (default), PEM or DER"
	.section	.rodata.str1.1
.LC69:
	.string	"outform"
	.section	.rodata.str1.8
	.align 8
.LC70:
	.string	"Output format SMIME (default), PEM or DER"
	.section	.rodata.str1.1
.LC71:
	.string	"in"
.LC72:
	.string	"Input file"
.LC73:
	.string	"out"
.LC74:
	.string	"Output file"
.LC75:
	.string	"encrypt"
.LC76:
	.string	"Encrypt message"
.LC77:
	.string	"decrypt"
.LC78:
	.string	"Decrypt encrypted message"
.LC79:
	.string	"sign"
.LC80:
	.string	"Sign message"
.LC81:
	.string	"sign_receipt"
	.section	.rodata.str1.8
	.align 8
.LC82:
	.string	"Generate a signed receipt for the message"
	.section	.rodata.str1.1
.LC83:
	.string	"resign"
.LC84:
	.string	"Resign a signed message"
.LC85:
	.string	"verify"
.LC86:
	.string	"Verify signed message"
.LC87:
	.string	"verify_retcode"
.LC88:
	.string	"verify_receipt"
.LC89:
	.string	"cmsout"
.LC90:
	.string	"Output CMS structure"
.LC91:
	.string	"data_out"
.LC92:
	.string	"data_create"
.LC93:
	.string	"digest_verify"
.LC94:
	.string	"digest_create"
.LC95:
	.string	"compress"
.LC96:
	.string	"uncompress"
.LC97:
	.string	"EncryptedData_decrypt"
.LC98:
	.string	"EncryptedData_encrypt"
.LC99:
	.string	"debug_decrypt"
.LC100:
	.string	"text"
	.section	.rodata.str1.8
	.align 8
.LC101:
	.string	"Include or delete text MIME headers"
	.section	.rodata.str1.1
.LC102:
	.string	"asciicrlf"
.LC103:
	.string	"nointern"
	.section	.rodata.str1.8
	.align 8
.LC104:
	.string	"Don't search certificates in message for signer"
	.section	.rodata.str1.1
.LC105:
	.string	"noverify"
	.section	.rodata.str1.8
	.align 8
.LC106:
	.string	"Don't verify signers certificate"
	.section	.rodata.str1.1
.LC107:
	.string	"nocerts"
	.section	.rodata.str1.8
	.align 8
.LC108:
	.string	"Don't include signers certificate when signing"
	.section	.rodata.str1.1
.LC109:
	.string	"noattr"
	.section	.rodata.str1.8
	.align 8
.LC110:
	.string	"Don't include any signed attributes"
	.section	.rodata.str1.1
.LC111:
	.string	"nodetach"
.LC112:
	.string	"Use opaque signing"
.LC113:
	.string	"nosmimecap"
	.section	.rodata.str1.8
	.align 8
.LC114:
	.string	"Omit the SMIMECapabilities attribute"
	.section	.rodata.str1.1
.LC115:
	.string	"binary"
	.section	.rodata.str1.8
	.align 8
.LC116:
	.string	"Don't translate message to text"
	.section	.rodata.str1.1
.LC117:
	.string	"keyid"
.LC118:
	.string	"Use subject key identifier"
.LC119:
	.string	"nosigs"
	.section	.rodata.str1.8
	.align 8
.LC120:
	.string	"Don't verify message signature"
	.section	.rodata.str1.1
.LC121:
	.string	"no_content_verify"
.LC122:
	.string	"no_attr_verify"
.LC123:
	.string	"stream"
.LC124:
	.string	"Enable CMS streaming"
.LC125:
	.string	"indef"
.LC126:
	.string	"Same as -stream"
.LC127:
	.string	"noindef"
.LC128:
	.string	"Disable CMS streaming"
.LC129:
	.string	"crlfeol"
	.section	.rodata.str1.8
	.align 8
.LC130:
	.string	"Use CRLF as EOL termination instead of CR only"
	.section	.rodata.str1.1
.LC131:
	.string	"noout"
	.section	.rodata.str1.8
	.align 8
.LC132:
	.string	"For the -cmsout operation do not output the parsed CMS structure"
	.section	.rodata.str1.1
.LC133:
	.string	"receipt_request_print"
.LC134:
	.string	"Print CMS Receipt Request"
.LC135:
	.string	"receipt_request_all"
.LC136:
	.string	"receipt_request_first"
.LC137:
	.string	"rctform"
.LC138:
	.string	"Receipt file format"
.LC139:
	.string	"certfile"
.LC140:
	.string	"Other certificates file"
.LC141:
	.string	"CAfile"
.LC142:
	.string	"Trusted certificates file"
.LC143:
	.string	"CApath"
	.section	.rodata.str1.8
	.align 8
.LC144:
	.string	"trusted certificates directory"
	.section	.rodata.str1.1
.LC145:
	.string	"no-CAfile"
	.section	.rodata.str1.8
	.align 8
.LC146:
	.string	"Do not load the default certificates file"
	.section	.rodata.str1.1
.LC147:
	.string	"no-CApath"
	.section	.rodata.str1.8
	.align 8
.LC148:
	.string	"Do not load certificates from the default certificates directory"
	.section	.rodata.str1.1
.LC149:
	.string	"content"
	.section	.rodata.str1.8
	.align 8
.LC150:
	.string	"Supply or override content for detached signature"
	.section	.rodata.str1.1
.LC151:
	.string	"print"
	.section	.rodata.str1.8
	.align 8
.LC152:
	.string	"For the -cmsout operation print out all fields of the CMS structure"
	.section	.rodata.str1.1
.LC153:
	.string	"secretkey"
.LC154:
	.string	"secretkeyid"
.LC155:
	.string	"pwri_password"
.LC156:
	.string	"econtent_type"
.LC157:
	.string	"passin"
.LC158:
	.string	"Input file pass phrase source"
.LC159:
	.string	"to"
.LC160:
	.string	"To address"
.LC161:
	.string	"from"
.LC162:
	.string	"From address"
.LC163:
	.string	"subject"
.LC164:
	.string	"Subject"
.LC165:
	.string	"signer"
.LC166:
	.string	"Signer certificate file"
.LC167:
	.string	"recip"
	.section	.rodata.str1.8
	.align 8
.LC168:
	.string	"Recipient cert file for decryption"
	.section	.rodata.str1.1
.LC169:
	.string	"certsout"
.LC170:
	.string	"Certificate output file"
.LC171:
	.string	"md"
	.section	.rodata.str1.8
	.align 8
.LC172:
	.string	"Digest algorithm to use when signing or resigning"
	.section	.rodata.str1.1
.LC173:
	.string	"inkey"
	.section	.rodata.str1.8
	.align 8
.LC174:
	.string	"Input private key (if not signer or recipient)"
	.section	.rodata.str1.1
.LC175:
	.string	"keyform"
	.section	.rodata.str1.8
	.align 8
.LC176:
	.string	"Input private key format (PEM or ENGINE)"
	.section	.rodata.str1.1
.LC177:
	.string	"keyopt"
	.section	.rodata.str1.8
	.align 8
.LC178:
	.string	"Set public key parameters as n:v pairs"
	.section	.rodata.str1.1
.LC179:
	.string	"receipt_request_from"
.LC180:
	.string	"receipt_request_to"
.LC181:
	.string	""
.LC182:
	.string	"Any supported cipher"
.LC183:
	.string	"rand"
	.section	.rodata.str1.8
	.align 8
.LC184:
	.string	"Load the file(s) into the random number generator"
	.section	.rodata.str1.1
.LC185:
	.string	"writerand"
	.section	.rodata.str1.8
	.align 8
.LC186:
	.string	"Write random data to the specified file"
	.section	.rodata.str1.1
.LC187:
	.string	"policy"
	.section	.rodata.str1.8
	.align 8
.LC188:
	.string	"adds policy to the acceptable policy set"
	.section	.rodata.str1.1
.LC189:
	.string	"purpose"
.LC190:
	.string	"certificate chain purpose"
.LC191:
	.string	"verify_name"
.LC192:
	.string	"verification policy name"
.LC193:
	.string	"verify_depth"
.LC194:
	.string	"chain depth limit"
.LC195:
	.string	"auth_level"
	.section	.rodata.str1.8
	.align 8
.LC196:
	.string	"chain authentication security level"
	.section	.rodata.str1.1
.LC197:
	.string	"attime"
.LC198:
	.string	"verification epoch time"
.LC199:
	.string	"verify_hostname"
.LC200:
	.string	"expected peer hostname"
.LC201:
	.string	"verify_email"
.LC202:
	.string	"expected peer email"
.LC203:
	.string	"verify_ip"
.LC204:
	.string	"expected peer IP address"
.LC205:
	.string	"ignore_critical"
	.section	.rodata.str1.8
	.align 8
.LC206:
	.string	"permit unhandled critical extensions"
	.section	.rodata.str1.1
.LC207:
	.string	"issuer_checks"
.LC208:
	.string	"(deprecated)"
.LC209:
	.string	"crl_check"
	.section	.rodata.str1.8
	.align 8
.LC210:
	.string	"check leaf certificate revocation"
	.section	.rodata.str1.1
.LC211:
	.string	"crl_check_all"
.LC212:
	.string	"check full chain revocation"
.LC213:
	.string	"policy_check"
.LC214:
	.string	"perform rfc5280 policy checks"
.LC215:
	.string	"explicit_policy"
	.section	.rodata.str1.8
	.align 8
.LC216:
	.string	"set policy variable require-explicit-policy"
	.section	.rodata.str1.1
.LC217:
	.string	"inhibit_any"
	.section	.rodata.str1.8
	.align 8
.LC218:
	.string	"set policy variable inhibit-any-policy"
	.section	.rodata.str1.1
.LC219:
	.string	"inhibit_map"
	.section	.rodata.str1.8
	.align 8
.LC220:
	.string	"set policy variable inhibit-policy-mapping"
	.section	.rodata.str1.1
.LC221:
	.string	"x509_strict"
	.section	.rodata.str1.8
	.align 8
.LC222:
	.string	"disable certificate compatibility work-arounds"
	.section	.rodata.str1.1
.LC223:
	.string	"extended_crl"
.LC224:
	.string	"enable extended CRL features"
.LC225:
	.string	"use_deltas"
.LC226:
	.string	"use delta CRLs"
.LC227:
	.string	"policy_print"
	.section	.rodata.str1.8
	.align 8
.LC228:
	.string	"print policy processing diagnostics"
	.section	.rodata.str1.1
.LC229:
	.string	"check_ss_sig"
.LC230:
	.string	"check root CA self-signatures"
.LC231:
	.string	"trusted_first"
	.section	.rodata.str1.8
	.align 8
.LC232:
	.string	"search trust store first (default)"
	.section	.rodata.str1.1
.LC233:
	.string	"suiteB_128_only"
.LC234:
	.string	"Suite B 128-bit-only mode"
.LC235:
	.string	"suiteB_128"
	.section	.rodata.str1.8
	.align 8
.LC236:
	.string	"Suite B 128-bit mode allowing 192-bit algorithms"
	.section	.rodata.str1.1
.LC237:
	.string	"suiteB_192"
.LC238:
	.string	"Suite B 192-bit-only mode"
.LC239:
	.string	"partial_chain"
	.section	.rodata.str1.8
	.align 8
.LC240:
	.string	"accept chains anchored by intermediate trust-store CAs"
	.section	.rodata.str1.1
.LC241:
	.string	"no_alt_chains"
.LC242:
	.string	"no_check_time"
	.section	.rodata.str1.8
	.align 8
.LC243:
	.string	"ignore certificate validity time"
	.section	.rodata.str1.1
.LC244:
	.string	"allow_proxy_certs"
	.section	.rodata.str1.8
	.align 8
.LC245:
	.string	"allow the use of proxy certificates"
	.section	.rodata.str1.1
.LC246:
	.string	"aes128-wrap"
.LC247:
	.string	"Use AES128 to wrap key"
.LC248:
	.string	"aes192-wrap"
.LC249:
	.string	"Use AES192 to wrap key"
.LC250:
	.string	"aes256-wrap"
.LC251:
	.string	"Use AES256 to wrap key"
.LC252:
	.string	"des3-wrap"
.LC253:
	.string	"Use 3DES-EDE to wrap key"
.LC254:
	.string	"engine"
	.section	.rodata.str1.8
	.align 8
.LC255:
	.string	"Use engine e, possibly a hardware device"
	.section	.data.rel.ro,"aw"
	.align 32
	.type	cms_options, @object
	.size	cms_options, 2664
cms_options:
	.quad	OPT_HELP_STR
	.long	1
	.long	45
	.quad	.LC62
	.quad	OPT_HELP_STR
	.long	1
	.long	45
	.quad	.LC63
	.quad	OPT_HELP_STR
	.long	1
	.long	45
	.quad	.LC64
	.quad	.LC65
	.long	1
	.long	45
	.quad	.LC66
	.quad	.LC67
	.long	2
	.long	99
	.quad	.LC68
	.quad	.LC69
	.long	3
	.long	99
	.quad	.LC70
	.quad	.LC71
	.long	4
	.long	60
	.quad	.LC72
	.quad	.LC73
	.long	5
	.long	62
	.quad	.LC74
	.quad	.LC75
	.long	6
	.long	45
	.quad	.LC76
	.quad	.LC77
	.long	7
	.long	45
	.quad	.LC78
	.quad	.LC79
	.long	8
	.long	45
	.quad	.LC80
	.quad	.LC81
	.long	9
	.long	45
	.quad	.LC82
	.quad	.LC83
	.long	10
	.long	45
	.quad	.LC84
	.quad	.LC85
	.long	11
	.long	45
	.quad	.LC86
	.quad	.LC87
	.long	12
	.long	45
	.zero	8
	.quad	.LC88
	.long	13
	.long	60
	.zero	8
	.quad	.LC89
	.long	14
	.long	45
	.quad	.LC90
	.quad	.LC91
	.long	15
	.long	45
	.zero	8
	.quad	.LC92
	.long	16
	.long	45
	.zero	8
	.quad	.LC93
	.long	17
	.long	45
	.zero	8
	.quad	.LC94
	.long	18
	.long	45
	.zero	8
	.quad	.LC95
	.long	19
	.long	45
	.zero	8
	.quad	.LC96
	.long	20
	.long	45
	.zero	8
	.quad	.LC97
	.long	21
	.long	45
	.zero	8
	.quad	.LC98
	.long	22
	.long	45
	.zero	8
	.quad	.LC99
	.long	23
	.long	45
	.zero	8
	.quad	.LC100
	.long	24
	.long	45
	.quad	.LC101
	.quad	.LC102
	.long	25
	.long	45
	.zero	8
	.quad	.LC103
	.long	26
	.long	45
	.quad	.LC104
	.quad	.LC105
	.long	27
	.long	45
	.quad	.LC106
	.quad	.LC107
	.long	28
	.long	45
	.quad	.LC108
	.quad	.LC109
	.long	29
	.long	45
	.quad	.LC110
	.quad	.LC111
	.long	30
	.long	45
	.quad	.LC112
	.quad	.LC113
	.long	31
	.long	45
	.quad	.LC114
	.quad	.LC115
	.long	32
	.long	45
	.quad	.LC116
	.quad	.LC117
	.long	33
	.long	45
	.quad	.LC118
	.quad	.LC119
	.long	34
	.long	45
	.quad	.LC120
	.quad	.LC121
	.long	35
	.long	45
	.zero	8
	.quad	.LC122
	.long	36
	.long	45
	.zero	8
	.quad	.LC123
	.long	37
	.long	45
	.quad	.LC124
	.quad	.LC125
	.long	37
	.long	45
	.quad	.LC126
	.quad	.LC127
	.long	38
	.long	45
	.quad	.LC128
	.quad	.LC129
	.long	39
	.long	45
	.quad	.LC130
	.quad	.LC131
	.long	40
	.long	45
	.quad	.LC132
	.quad	.LC133
	.long	41
	.long	45
	.quad	.LC134
	.quad	.LC135
	.long	42
	.long	45
	.zero	8
	.quad	.LC136
	.long	43
	.long	45
	.zero	8
	.quad	.LC137
	.long	44
	.long	70
	.quad	.LC138
	.quad	.LC139
	.long	45
	.long	60
	.quad	.LC140
	.quad	.LC141
	.long	46
	.long	60
	.quad	.LC142
	.quad	.LC143
	.long	47
	.long	47
	.quad	.LC144
	.quad	.LC145
	.long	49
	.long	45
	.quad	.LC146
	.quad	.LC147
	.long	48
	.long	45
	.quad	.LC148
	.quad	.LC149
	.long	50
	.long	60
	.quad	.LC150
	.quad	.LC151
	.long	51
	.long	45
	.quad	.LC152
	.quad	.LC153
	.long	52
	.long	115
	.zero	8
	.quad	.LC154
	.long	53
	.long	115
	.zero	8
	.quad	.LC155
	.long	54
	.long	115
	.zero	8
	.quad	.LC156
	.long	55
	.long	115
	.zero	8
	.quad	.LC157
	.long	56
	.long	115
	.quad	.LC158
	.quad	.LC159
	.long	57
	.long	115
	.quad	.LC160
	.quad	.LC161
	.long	58
	.long	115
	.quad	.LC162
	.quad	.LC163
	.long	59
	.long	115
	.quad	.LC164
	.quad	.LC165
	.long	60
	.long	115
	.quad	.LC166
	.quad	.LC167
	.long	61
	.long	60
	.quad	.LC168
	.quad	.LC169
	.long	62
	.long	62
	.quad	.LC170
	.quad	.LC171
	.long	63
	.long	115
	.quad	.LC172
	.quad	.LC173
	.long	64
	.long	115
	.quad	.LC174
	.quad	.LC175
	.long	65
	.long	102
	.quad	.LC176
	.quad	.LC177
	.long	66
	.long	115
	.quad	.LC178
	.quad	.LC179
	.long	67
	.long	115
	.zero	8
	.quad	.LC180
	.long	68
	.long	115
	.zero	8
	.quad	.LC181
	.long	2032
	.long	45
	.quad	.LC182
	.quad	.LC183
	.long	1501
	.long	115
	.quad	.LC184
	.quad	.LC185
	.long	1502
	.long	62
	.quad	.LC186
	.quad	.LC187
	.long	2001
	.long	115
	.quad	.LC188
	.quad	.LC189
	.long	2002
	.long	115
	.quad	.LC190
	.quad	.LC191
	.long	2003
	.long	115
	.quad	.LC192
	.quad	.LC193
	.long	2004
	.long	110
	.quad	.LC194
	.quad	.LC195
	.long	2029
	.long	110
	.quad	.LC196
	.quad	.LC197
	.long	2005
	.long	77
	.quad	.LC198
	.quad	.LC199
	.long	2006
	.long	115
	.quad	.LC200
	.quad	.LC201
	.long	2007
	.long	115
	.quad	.LC202
	.quad	.LC203
	.long	2008
	.long	115
	.quad	.LC204
	.quad	.LC205
	.long	2009
	.long	45
	.quad	.LC206
	.quad	.LC207
	.long	2010
	.long	45
	.quad	.LC208
	.quad	.LC209
	.long	2011
	.long	45
	.quad	.LC210
	.quad	.LC211
	.long	2012
	.long	45
	.quad	.LC212
	.quad	.LC213
	.long	2013
	.long	45
	.quad	.LC214
	.quad	.LC215
	.long	2014
	.long	45
	.quad	.LC216
	.quad	.LC217
	.long	2015
	.long	45
	.quad	.LC218
	.quad	.LC219
	.long	2016
	.long	45
	.quad	.LC220
	.quad	.LC221
	.long	2017
	.long	45
	.quad	.LC222
	.quad	.LC223
	.long	2018
	.long	45
	.quad	.LC224
	.quad	.LC225
	.long	2019
	.long	45
	.quad	.LC226
	.quad	.LC227
	.long	2020
	.long	45
	.quad	.LC228
	.quad	.LC229
	.long	2021
	.long	45
	.quad	.LC230
	.quad	.LC231
	.long	2022
	.long	45
	.quad	.LC232
	.quad	.LC233
	.long	2023
	.long	45
	.quad	.LC234
	.quad	.LC235
	.long	2024
	.long	45
	.quad	.LC236
	.quad	.LC237
	.long	2025
	.long	45
	.quad	.LC238
	.quad	.LC239
	.long	2026
	.long	45
	.quad	.LC240
	.quad	.LC241
	.long	2027
	.long	45
	.quad	.LC208
	.quad	.LC242
	.long	2028
	.long	45
	.quad	.LC243
	.quad	.LC244
	.long	2030
	.long	45
	.quad	.LC245
	.quad	.LC246
	.long	69
	.long	45
	.quad	.LC247
	.quad	.LC248
	.long	70
	.long	45
	.quad	.LC249
	.quad	.LC250
	.long	71
	.long	45
	.quad	.LC251
	.quad	.LC252
	.long	72
	.long	45
	.quad	.LC253
	.quad	.LC254
	.long	73
	.long	115
	.quad	.LC255
	.quad	0
	.zero	16
	.local	verify_err
	.comm	verify_err,4,4
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
