	.file	"pkcs12.c"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"%s"
.LC1:
	.string	", <unsupported parameters>"
.LC2:
	.string	", %s, %s"
.LC3:
	.string	", Iteration %ld, PRF %s"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	", Salt length: %d, Cost(N): %ld, Block size(r): %ld, Parallelism(p): %ld"
	.section	.rodata.str1.1
.LC5:
	.string	", Iteration %ld"
.LC6:
	.string	"\n"
	.text
	.p2align 4
	.type	alg_print, @function
alg_print:
.LFB1464:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-64(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-76(%rbp), %r13
	movq	%r14, %rdx
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-72(%rbp), %r12
	movq	%r13, %rsi
	pushq	%rbx
	movq	%r12, %rdi
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	X509_ALGOR_get0@PLT
	movq	-72(%rbp), %rdi
	call	OBJ_obj2nid@PLT
	movl	%eax, %edi
	movl	%eax, %ebx
	call	OBJ_nid2ln@PLT
	movq	bio_err(%rip), %rdi
	leaq	.LC0(%rip), %rsi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	cmpl	$161, %ebx
	je	.L32
	cmpl	$16, -76(%rbp)
	je	.L13
.L14:
	movq	bio_err(%rip), %rdi
	leaq	.LC1(%rip), %rsi
	call	BIO_puts@PLT
.L4:
	movq	bio_err(%rip), %rdi
	leaq	.LC6(%rip), %rsi
	call	BIO_puts@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L33
	addq	$40, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	.cfi_restore_state
	movq	-64(%rbp), %rdi
	leaq	PBEPARAM_it(%rip), %rsi
	call	ASN1_item_unpack@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L14
	movq	8(%rax), %rdi
	call	ASN1_INTEGER_get@PLT
	movq	bio_err(%rip), %rdi
	leaq	.LC5(%rip), %rsi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	%r12, %rdi
	call	PBEPARAM_free@PLT
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L32:
	cmpl	$16, -76(%rbp)
	jne	.L14
	movq	-64(%rbp), %rdi
	leaq	PBE2PARAM_it(%rip), %rsi
	call	ASN1_item_unpack@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L14
	movq	(%rax), %rcx
	movq	%r13, %rsi
	movq	%r14, %rdx
	movq	%r12, %rdi
	call	X509_ALGOR_get0@PLT
	movq	-72(%rbp), %rdi
	call	OBJ_obj2nid@PLT
	movq	8(%r15), %rcx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	X509_ALGOR_get0@PLT
	movq	-72(%rbp), %rdi
	call	OBJ_obj2nid@PLT
	movl	%eax, %edi
	call	OBJ_nid2sn@PLT
	movl	%ebx, %edi
	movq	%rax, %r13
	call	OBJ_nid2ln@PLT
	movq	bio_err(%rip), %rdi
	movq	%r13, %rcx
	leaq	.LC2(%rip), %rsi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	cmpl	$69, %ebx
	je	.L34
	cmpl	$973, %ebx
	je	.L35
.L10:
	movq	%r15, %rdi
	call	PBE2PARAM_free@PLT
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L34:
	cmpl	$16, -76(%rbp)
	jne	.L14
	movq	-64(%rbp), %rdi
	leaq	PBKDF2PARAM_it(%rip), %rsi
	call	ASN1_item_unpack@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L14
	movq	24(%rax), %rcx
	movl	$163, %edi
	testq	%rcx, %rcx
	je	.L9
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	X509_ALGOR_get0@PLT
	movq	-72(%rbp), %rdi
	call	OBJ_obj2nid@PLT
	movl	%eax, %edi
.L9:
	call	OBJ_nid2sn@PLT
	movq	8(%r13), %rdi
	movq	%rax, %r12
	call	ASN1_INTEGER_get@PLT
	movq	bio_err(%rip), %rdi
	movq	%r12, %rcx
	leaq	.LC3(%rip), %rsi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	%r13, %rdi
	call	PBKDF2PARAM_free@PLT
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L35:
	cmpl	$16, -76(%rbp)
	jne	.L14
	movq	-64(%rbp), %rdi
	leaq	SCRYPT_PARAMS_it(%rip), %rsi
	call	ASN1_item_unpack@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L14
	movq	24(%rax), %rdi
	call	ASN1_INTEGER_get@PLT
	movq	16(%r12), %rdi
	movq	%rax, %r14
	call	ASN1_INTEGER_get@PLT
	movq	8(%r12), %rdi
	movq	%rax, %rbx
	call	ASN1_INTEGER_get@PLT
	movq	(%r12), %rdi
	movq	%rax, %r13
	call	ASN1_STRING_length@PLT
	movq	%r14, %r9
	movq	%rbx, %r8
	movq	%r13, %rcx
	movq	bio_err(%rip), %rdi
	movl	%eax, %edx
	leaq	.LC4(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	%r12, %rdi
	call	SCRYPT_PARAMS_free@PLT
	jmp	.L10
.L33:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1464:
	.size	alg_print, .-alg_print
	.p2align 4
	.globl	cert_load
	.type	cert_load, @function
cert_load:
.LFB1465:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L38:
	movq	%r13, %rdi
	movl	$1, %r12d
	call	OPENSSL_sk_push@PLT
.L37:
	xorl	%esi, %esi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rbx, %rdi
	call	PEM_read_bio_X509@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	jne	.L38
	testl	%r12d, %r12d
	jne	.L44
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L44:
	.cfi_restore_state
	call	ERR_clear_error@PLT
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1465:
	.size	cert_load, .-cert_load
	.section	.rodata.str1.1
.LC7:
	.string	"%s\n"
	.section	.rodata.str1.8
	.align 8
.LC8:
	.string	"../deps/openssl/openssl/apps/pkcs12.c"
	.section	.rodata.str1.1
.LC9:
	.string	"%02X "
.LC10:
	.string	"<Unsupported tag %d>\n"
	.text
	.p2align 4
	.globl	print_attribute
	.type	print_attribute, @function
print_attribute:
.LFB1466:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	(%rsi), %edx
	cmpl	$4, %edx
	je	.L46
	cmpl	$30, %edx
	je	.L47
	cmpl	$3, %edx
	je	.L57
	popq	%rbx
	leaq	.LC10(%rip), %rsi
	popq	%r12
	xorl	%eax, %eax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	BIO_printf@PLT
	.p2align 4,,10
	.p2align 3
.L46:
	.cfi_restore_state
	movq	8(%rsi), %rax
	movl	(%rax), %edx
	movq	8(%rax), %rbx
	testl	%edx, %edx
	jle	.L50
	leal	-1(%rdx), %eax
	leaq	.LC9(%rip), %r13
	leaq	1(%rbx,%rax), %r14
	.p2align 4,,10
	.p2align 3
.L51:
	movzbl	(%rbx), %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	addq	$1, %rbx
	call	BIO_printf@PLT
	cmpq	%r14, %rbx
	jne	.L51
.L50:
	popq	%rbx
	movq	%r12, %rdi
	leaq	.LC6(%rip), %rsi
	popq	%r12
	xorl	%eax, %eax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	BIO_printf@PLT
	.p2align 4,,10
	.p2align 3
.L57:
	.cfi_restore_state
	movq	8(%rsi), %rax
	movl	(%rax), %edx
	movq	8(%rax), %rbx
	testl	%edx, %edx
	jle	.L50
	leal	-1(%rdx), %eax
	leaq	.LC9(%rip), %r13
	leaq	1(%rbx,%rax), %r14
	.p2align 4,,10
	.p2align 3
.L52:
	movzbl	(%rbx), %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	addq	$1, %rbx
	call	BIO_printf@PLT
	cmpq	%rbx, %r14
	jne	.L52
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L47:
	movq	8(%rsi), %rax
	movq	8(%rax), %rdi
	movl	(%rax), %esi
	call	OPENSSL_uni2asc@PLT
	movq	%r12, %rdi
	leaq	.LC7(%rip), %rsi
	movq	%rax, %r13
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	popq	%rbx
	movq	%r13, %rdi
	popq	%r12
	movl	$889, %edx
	popq	%r13
	leaq	.LC8(%rip), %rsi
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_free@PLT
	.cfi_endproc
.LFE1466:
	.size	print_attribute, .-print_attribute
	.section	.rodata.str1.1
.LC11:
	.string	"%s: <No Attributes>\n"
.LC12:
	.string	"%s: <Empty Attributes>\n"
.LC13:
	.string	"    "
.LC14:
	.string	": "
.LC15:
	.string	"%s: "
.LC16:
	.string	"<No Values>\n"
	.text
	.p2align 4
	.globl	print_attribs
	.type	print_attribs, @function
print_attribs:
.LFB1467:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	testq	%rsi, %rsi
	je	.L73
	movq	%rsi, %rdi
	movq	%rsi, %r14
	call	OPENSSL_sk_num@PLT
	movq	%r13, %rdx
	testl	%eax, %eax
	je	.L74
	leaq	.LC7(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	xorl	%r13d, %r13d
	call	BIO_printf@PLT
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L76:
	movq	-56(%rbp), %r9
	movq	%r12, %rdi
	movq	%r9, %rsi
	call	i2a_ASN1_OBJECT@PLT
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC14(%rip), %rsi
	call	BIO_printf@PLT
	movq	%rbx, %rdi
	call	X509_ATTRIBUTE_count@PLT
	testl	%eax, %eax
	jne	.L75
.L71:
	leaq	.LC16(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L68:
	addl	$1, %r13d
.L62:
	movq	%r14, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r13d
	jge	.L60
	movl	%r13d, %esi
	movq	%r14, %rdi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %rdi
	movq	%rax, %rbx
	call	X509_ATTRIBUTE_get0_object@PLT
	movq	%rax, %rdi
	movq	%rax, -56(%rbp)
	call	OBJ_obj2nid@PLT
	leaq	.LC13(%rip), %rsi
	movq	%r12, %rdi
	movl	%eax, %r15d
	xorl	%eax, %eax
	call	BIO_printf@PLT
	testl	%r15d, %r15d
	je	.L76
	movl	%r15d, %edi
	call	OBJ_nid2ln@PLT
	movq	%r12, %rdi
	leaq	.LC15(%rip), %rsi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	%rbx, %rdi
	call	X509_ATTRIBUTE_count@PLT
	testl	%eax, %eax
	je	.L71
.L75:
	xorl	%r15d, %r15d
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L67:
	movl	%r15d, %esi
	movq	%rbx, %rdi
	addl	$1, %r15d
	call	X509_ATTRIBUTE_get0_type@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	print_attribute
.L65:
	movq	%rbx, %rdi
	call	X509_ATTRIBUTE_count@PLT
	cmpl	%eax, %r15d
	jl	.L67
	jmp	.L68
	.p2align 4,,10
	.p2align 3
.L73:
	leaq	.LC11(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L60:
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L74:
	.cfi_restore_state
	leaq	.LC12(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_printf@PLT
	jmp	.L60
	.cfi_endproc
.LFE1467:
	.size	print_attribs, .-print_attribs
	.section	.rodata.str1.1
.LC17:
	.string	"Key bag\n"
.LC18:
	.string	"Bag Attributes"
.LC19:
	.string	"Key Attributes"
.LC20:
	.string	"Shrouded Keybag: "
.LC21:
	.string	"Certificate bag\n"
.LC22:
	.string	"Safe Contents bag\n"
	.section	.rodata.str1.8
	.align 8
.LC23:
	.string	"Warning unsupported bag type: "
	.text
	.p2align 4
	.globl	dump_certs_pkeys_bag
	.type	dump_certs_pkeys_bag, @function
dump_certs_pkeys_bag:
.LFB1462:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%ecx, %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%rsi, %rdi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%r8d, %ebx
	subq	$56, %rsp
	movq	16(%rbp), %rax
	movq	%rdx, -88(%rbp)
	movq	%r9, -72(%rbp)
	movq	%rax, -80(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	PKCS12_SAFEBAG_get0_attrs@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	call	PKCS12_SAFEBAG_get_nid@PLT
	cmpl	$152, %eax
	je	.L78
	jg	.L79
	cmpl	$150, %eax
	je	.L80
	cmpl	$151, %eax
	jne	.L82
	testb	$4, %bl
	jne	.L122
.L88:
	andl	$1, %ebx
	jne	.L121
	leaq	.LC18(%rip), %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	print_attribs
	movq	-88(%rbp), %rsi
	movq	%r12, %rdi
	movl	%r15d, %edx
	call	PKCS12_decrypt_skey@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L90
	movq	%rax, %rdi
	call	EVP_PKCS82PKEY@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L123
	call	PKCS8_pkey_get0_attrs@PLT
	leaq	.LC19(%rip), %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	print_attribs
	movq	%r12, %rdi
	call	PKCS8_PRIV_KEY_INFO_free@PLT
	subq	$8, %rsp
	pushq	-72(%rbp)
	movq	-80(%rbp), %rdx
	movq	%r14, %rsi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r13, %rdi
	call	PEM_write_bio_PrivateKey@PLT
	movq	%r14, %rdi
	movl	%eax, %ebx
	call	EVP_PKEY_free@PLT
	popq	%rsi
	popq	%rdi
	.p2align 4,,10
	.p2align 3
.L77:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L124
	leaq	-40(%rbp), %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L79:
	.cfi_restore_state
	cmpl	$155, %eax
	jne	.L82
	testb	$4, %bl
	jne	.L125
.L95:
	movq	%r14, %rsi
	leaq	.LC18(%rip), %rdx
	movq	%r13, %rdi
	xorl	%r14d, %r14d
	call	print_attribs
	movq	%r12, %rdi
	call	PKCS12_SAFEBAG_get0_safes@PLT
	movq	%rax, %r12
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L97:
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	OPENSSL_sk_value@PLT
	subq	$8, %rsp
	pushq	-80(%rbp)
	movq	-88(%rbp), %rdx
	movq	-72(%rbp), %r9
	movl	%r15d, %ecx
	movq	%rax, %rsi
	movl	%ebx, %r8d
	movq	%r13, %rdi
	call	dump_certs_pkeys_bag
	popq	%rdx
	popq	%rcx
	testl	%eax, %eax
	je	.L90
	addl	$1, %r14d
.L96:
	movq	%r12, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r14d
	jl	.L97
.L121:
	movl	$1, %ebx
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L125:
	movq	bio_err(%rip), %rdi
	leaq	.LC22(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L122:
	movq	bio_err(%rip), %rdi
	leaq	.LC20(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	%r12, %rdi
	call	PKCS12_SAFEBAG_get0_pkcs8@PLT
	leaq	-64(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rax, %rdi
	call	X509_SIG_get0@PLT
	movq	-64(%rbp), %rdi
	call	alg_print
	jmp	.L88
	.p2align 4,,10
	.p2align 3
.L82:
	movq	bio_err(%rip), %rdi
	leaq	.LC23(%rip), %rsi
	xorl	%eax, %eax
	movl	$1, %ebx
	call	BIO_printf@PLT
	movq	%r12, %rdi
	call	PKCS12_SAFEBAG_get0_type@PLT
	movq	bio_err(%rip), %rdi
	movq	%rax, %rsi
	call	i2a_ASN1_OBJECT@PLT
	movq	bio_err(%rip), %rdi
	leaq	.LC6(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L78:
	testb	$4, %bl
	jne	.L126
.L92:
	testb	$2, %bl
	jne	.L121
	movl	$157, %esi
	movq	%r12, %rdi
	call	PKCS12_SAFEBAG_get0_attr@PLT
	testq	%rax, %rax
	je	.L93
	andl	$16, %ebx
	jne	.L121
.L94:
	movq	%r13, %rdi
	leaq	.LC18(%rip), %rdx
	movq	%r14, %rsi
	call	print_attribs
	movq	%r12, %rdi
	call	PKCS12_SAFEBAG_get_bag_nid@PLT
	cmpl	$158, %eax
	jne	.L121
	movq	%r12, %rdi
	call	PKCS12_SAFEBAG_get1_cert@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L90
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	dump_cert_text@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	PEM_write_bio_X509@PLT
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	X509_free@PLT
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L80:
	testb	$4, %bl
	jne	.L127
.L84:
	andl	$1, %ebx
	jne	.L121
	movq	%r14, %rsi
	leaq	.LC18(%rip), %rdx
	movq	%r13, %rdi
	call	print_attribs
	movq	%r12, %rdi
	call	PKCS12_SAFEBAG_get0_p8inf@PLT
	movq	%rax, %rdi
	movq	%rax, %r14
	call	EVP_PKCS82PKEY@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L90
	movq	%r14, %rdi
	call	PKCS8_pkey_get0_attrs@PLT
	leaq	.LC19(%rip), %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	print_attribs
	subq	$8, %rsp
	pushq	-72(%rbp)
	movq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	PEM_write_bio_PrivateKey@PLT
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	EVP_PKEY_free@PLT
	popq	%r8
	popq	%r9
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L90:
	xorl	%ebx, %ebx
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L126:
	movq	bio_err(%rip), %rdi
	leaq	.LC21(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L127:
	movq	bio_err(%rip), %rdi
	leaq	.LC17(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L93:
	andl	$8, %ebx
	je	.L94
	jmp	.L121
	.p2align 4,,10
	.p2align 3
.L123:
	call	PKCS8_PRIV_KEY_INFO_free@PLT
	jmp	.L77
.L124:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1462:
	.size	dump_certs_pkeys_bag, .-dump_certs_pkeys_bag
	.p2align 4
	.globl	dump_certs_pkeys_bags
	.type	dump_certs_pkeys_bags, @function
dump_certs_pkeys_bags:
.LFB1461:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%ecx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movl	%r8d, -52(%rbp)
	movq	%r9, -64(%rbp)
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L131:
	movl	%r12d, %esi
	movq	%rbx, %rdi
	call	OPENSSL_sk_value@PLT
	subq	$8, %rsp
	pushq	16(%rbp)
	movq	-64(%rbp), %r9
	movl	-52(%rbp), %r8d
	movl	%r13d, %ecx
	movq	%r14, %rdx
	movq	%rax, %rsi
	movq	%r15, %rdi
	call	dump_certs_pkeys_bag
	popq	%rdx
	popq	%rcx
	testl	%eax, %eax
	je	.L128
	addl	$1, %r12d
.L129:
	movq	%rbx, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r12d
	jl	.L131
	movl	$1, %eax
.L128:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1461:
	.size	dump_certs_pkeys_bags, .-dump_certs_pkeys_bags
	.section	.rodata.str1.1
.LC24:
	.string	"PKCS7 Data\n"
.LC25:
	.string	"PKCS7 Encrypted data: "
	.text
	.p2align 4
	.globl	dump_certs_keys_p12
	.type	dump_certs_keys_p12, @function
dump_certs_keys_p12:
.LFB1460:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%ecx, %r14d
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%r8d, %ebx
	subq	$40, %rsp
	movq	%rdi, -64(%rbp)
	movq	%rsi, %rdi
	movq	%rdx, -56(%rbp)
	movl	%r8d, -68(%rbp)
	movq	%r9, -80(%rbp)
	call	PKCS12_unpack_authsafes@PLT
	testq	%rax, %rax
	je	.L149
	movq	%rax, %r12
	andl	$4, %ebx
	movq	%r12, %rdi
	movl	%ebx, -72(%rbp)
	xorl	%ebx, %ebx
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jge	.L159
	.p2align 4,,10
	.p2align 3
.L148:
	movq	%r12, %rdi
	movl	%ebx, %esi
	call	OPENSSL_sk_value@PLT
	movq	24(%rax), %rdi
	movq	%rax, %r13
	call	OBJ_obj2nid@PLT
	cmpl	$21, %eax
	je	.L160
	cmpl	$26, %eax
	jne	.L141
	movl	-72(%rbp), %esi
	testl	%esi, %esi
	jne	.L161
.L142:
	movq	-56(%rbp), %rsi
	movl	%r14d, %edx
	movq	%r13, %rdi
	call	PKCS12_unpack_p7encdata@PLT
	movq	%rax, %r15
.L140:
	testq	%r15, %r15
	je	.L150
.L162:
	movq	%r12, %rax
	xorl	%r13d, %r13d
	movl	%ebx, %r12d
	movq	%r15, %rbx
	movq	%rax, %r15
	jmp	.L144
	.p2align 4,,10
	.p2align 3
.L146:
	movl	%r13d, %esi
	movq	%rbx, %rdi
	call	OPENSSL_sk_value@PLT
	subq	$8, %rsp
	pushq	16(%rbp)
	movq	-56(%rbp), %rdx
	movq	-80(%rbp), %r9
	movl	%r14d, %ecx
	movq	%rax, %rsi
	movl	-68(%rbp), %r8d
	movq	-64(%rbp), %rdi
	call	dump_certs_pkeys_bag
	popq	%rdx
	popq	%rcx
	testl	%eax, %eax
	je	.L145
	addl	$1, %r13d
.L144:
	movq	%rbx, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r13d
	jl	.L146
	movq	%r15, %rax
	movq	PKCS12_SAFEBAG_free@GOTPCREL(%rip), %rsi
	movq	%rbx, %r15
	movl	%r12d, %ebx
	movq	%r15, %rdi
	movq	%rax, %r12
	call	OPENSSL_sk_pop_free@PLT
.L141:
	movq	%r12, %rdi
	addl	$1, %ebx
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jl	.L148
.L159:
	movl	$1, %eax
.L143:
	movq	PKCS7_free@GOTPCREL(%rip), %rsi
	movq	%r12, %rdi
	movl	%eax, -56(%rbp)
	call	OPENSSL_sk_pop_free@PLT
	movl	-56(%rbp), %eax
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L160:
	.cfi_restore_state
	movq	%r13, %rdi
	call	PKCS12_unpack_p7data@PLT
	movl	-72(%rbp), %edi
	movq	%rax, %r15
	testl	%edi, %edi
	je	.L140
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
	leaq	.LC24(%rip), %rsi
	call	BIO_printf@PLT
	testq	%r15, %r15
	jne	.L162
	.p2align 4,,10
	.p2align 3
.L150:
	xorl	%eax, %eax
	jmp	.L143
	.p2align 4,,10
	.p2align 3
.L161:
	movq	bio_err(%rip), %rdi
	leaq	.LC25(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	32(%r13), %rax
	movq	8(%rax), %rax
	movq	8(%rax), %rdi
	call	alg_print
	jmp	.L142
	.p2align 4,,10
	.p2align 3
.L145:
	movq	PKCS12_SAFEBAG_free@GOTPCREL(%rip), %rsi
	movq	%rbx, %rdi
	movl	%eax, -56(%rbp)
	movq	%r15, %r12
	call	OPENSSL_sk_pop_free@PLT
	movl	-56(%rbp), %eax
	jmp	.L143
	.p2align 4,,10
	.p2align 3
.L149:
	leaq	-40(%rbp), %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1460:
	.size	dump_certs_keys_p12, .-dump_certs_keys_p12
	.section	.rodata.str1.1
.LC26:
	.string	"%s: Use -help for summary.\n"
.LC27:
	.string	"NONE"
.LC28:
	.string	"Unknown PBE algorithm %s\n"
.LC29:
	.string	"Error getting passwords\n"
	.section	.rodata.str1.8
	.align 8
.LC30:
	.string	"Option -twopass cannot be used with -passout or -password\n"
	.align 8
.LC31:
	.string	"Option -twopass cannot be used with -passin or -password\n"
	.section	.rodata.str1.1
.LC32:
	.string	"Enter MAC Password:"
.LC33:
	.string	"Can't read Password\n"
.LC34:
	.string	"Nothing to do!\n"
.LC35:
	.string	"private key"
	.section	.rodata.str1.8
	.align 8
.LC36:
	.string	"No certificate matches private key\n"
	.section	.rodata.str1.1
.LC37:
	.string	"certificates from certfile"
.LC38:
	.string	"Error %s getting chain.\n"
.LC39:
	.string	"Enter Export Password:"
.LC40:
	.string	"Enter Import Password:"
.LC41:
	.string	"MAC: "
.LC42:
	.string	", Iteration %ld\n"
	.section	.rodata.str1.8
	.align 8
.LC43:
	.string	"MAC length: %ld, salt length: %ld\n"
	.align 8
.LC44:
	.string	"Mac verify error: invalid password?\n"
	.align 8
.LC45:
	.string	"Warning: using broken algorithm\n"
	.align 8
.LC46:
	.string	"Error outputting keys and certificates\n"
	.section	.rodata.str1.1
.LC47:
	.string	"certificates"
	.text
	.p2align 4
	.globl	pkcs12_main
	.type	pkcs12_main, @function
pkcs12_main:
.LFB1459:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$264, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	$254, %ecx
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	xorl	%ebx, %ebx
	xorl	%r14d, %r14d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	%edi, %r12d
	leaq	-4144(%rbp), %rdi
	movq	%rsi, %r13
	movaps	%xmm0, -4160(%rbp)
	movaps	%xmm0, -2112(%rbp)
	movq	$0, -4224(%rbp)
	movq	$0, -4216(%rbp)
	rep stosq
	leaq	-2096(%rbp), %rdi
	movl	$254, %ecx
	rep stosq
	call	EVP_des_ede3_cbc@PLT
	movq	%r13, %rsi
	movl	%r12d, %edi
	leaq	pkcs12_options(%rip), %rdx
	movq	%rax, -4208(%rbp)
	xorl	%r13d, %r13d
	leaq	.L169(%rip), %r12
	call	opt_init@PLT
	movl	$146, -4264(%rbp)
	movq	%rax, -4288(%rbp)
	movl	$149, -4248(%rbp)
	movq	$0, -4232(%rbp)
	movl	$0, -4364(%rbp)
	movl	$0, -4368(%rbp)
	movq	$0, -4384(%rbp)
	movq	$0, -4376(%rbp)
	movq	$0, -4360(%rbp)
	movq	$0, -4256(%rbp)
	movq	$0, -4280(%rbp)
	movq	$0, -4272(%rbp)
	movl	$0, -4352(%rbp)
	movl	$1, -4348(%rbp)
	movl	$2048, -4240(%rbp)
	movl	$2048, -4344(%rbp)
	movl	$0, -4260(%rbp)
	movl	$0, -4340(%rbp)
	movq	$0, -4312(%rbp)
	movq	$0, -4336(%rbp)
	movq	$0, -4304(%rbp)
	movq	$0, -4320(%rbp)
	movq	$0, -4328(%rbp)
	movq	$0, -4296(%rbp)
.L164:
	call	opt_next@PLT
	testl	%eax, %eax
	je	.L380
.L212:
	cmpl	$39, %eax
	jg	.L165
	cmpl	$-1, %eax
	jl	.L164
	addl	$1, %eax
	cmpl	$40, %eax
	ja	.L164
	movslq	(%r12,%rax,4), %rax
	addq	%r12, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L169:
	.long	.L281-.L169
	.long	.L164-.L169
	.long	.L206-.L169
	.long	.L205-.L169
	.long	.L204-.L169
	.long	.L203-.L169
	.long	.L282-.L169
	.long	.L202-.L169
	.long	.L201-.L169
	.long	.L200-.L169
	.long	.L199-.L169
	.long	.L198-.L169
	.long	.L197-.L169
	.long	.L196-.L169
	.long	.L195-.L169
	.long	.L194-.L169
	.long	.L193-.L169
	.long	.L192-.L169
	.long	.L191-.L169
	.long	.L190-.L169
	.long	.L189-.L169
	.long	.L188-.L169
	.long	.L187-.L169
	.long	.L186-.L169
	.long	.L185-.L169
	.long	.L184-.L169
	.long	.L183-.L169
	.long	.L182-.L169
	.long	.L181-.L169
	.long	.L180-.L169
	.long	.L179-.L169
	.long	.L178-.L169
	.long	.L177-.L169
	.long	.L176-.L169
	.long	.L175-.L169
	.long	.L174-.L169
	.long	.L173-.L169
	.long	.L172-.L169
	.long	.L171-.L169
	.long	.L170-.L169
	.long	.L168-.L169
	.text
.L282:
	movl	$128, %ebx
	call	opt_next@PLT
	testl	%eax, %eax
	jne	.L212
.L380:
	call	opt_num_rest@PLT
	testl	%eax, %eax
	jne	.L281
	cmpq	$0, -4256(%rbp)
	je	.L213
	testl	%r14d, %r14d
	je	.L381
	movq	-4256(%rbp), %rsi
	movq	-4272(%rbp), %rdi
	leaq	-4216(%rbp), %rcx
	leaq	-4224(%rbp), %rdx
	call	app_passwd@PLT
	testl	%eax, %eax
	je	.L274
.L275:
	movq	-4216(%rbp), %r12
.L218:
	testq	%r12, %r12
	je	.L219
	movl	-4260(%rbp), %r9d
	movq	%r12, -4392(%rbp)
	movl	$1, -4396(%rbp)
	testl	%r9d, %r9d
	jne	.L382
.L220:
	testl	%r14d, %r14d
	je	.L224
	movq	$0, -4176(%rbp)
	movl	%r13d, %eax
	movq	$0, -4168(%rbp)
	andl	$3, %eax
	cmpl	$3, %eax
	je	.L383
	movl	%r13d, %eax
	andl	$1, %eax
	movl	%eax, -4280(%rbp)
	andl	$2, %r13d
	jne	.L227
	movl	-4280(%rbp), %edx
	testl	%edx, %edx
	jne	.L368
	movl	-4340(%rbp), %eax
	movl	%eax, -4280(%rbp)
.L271:
	movq	-4320(%rbp), %rax
	movq	-4232(%rbp), %r8
	leaq	.LC35(%rip), %r9
	movl	$1, %edx
	movq	-4224(%rbp), %rcx
	movl	$32773, %esi
	testq	%rax, %rax
	cmove	-4296(%rbp), %rax
	movq	%rax, %rdi
	call	load_key@PLT
	movq	%rax, -4272(%rbp)
	testq	%rax, %rax
	je	.L379
	testl	%r13d, %r13d
	je	.L231
.L375:
	movq	$0, -4256(%rbp)
.L232:
	movq	-4304(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L236
	xorl	%ecx, %ecx
	leaq	-4176(%rbp), %rsi
	movl	$32773, %edx
	leaq	.LC37(%rip), %r8
	call	load_certs@PLT
	testl	%eax, %eax
	je	.L378
.L236:
	movl	-4280(%rbp), %edi
	testl	%edi, %edi
	je	.L376
	movl	-4368(%rbp), %ecx
	movl	-4364(%rbp), %edx
	movq	-4376(%rbp), %rsi
	movq	-4384(%rbp), %rdi
	call	setup_verify@PLT
	movq	%rax, %r10
	testq	%rax, %rax
	je	.L289
	movq	%rax, -4296(%rbp)
	call	X509_STORE_CTX_new@PLT
	movq	-4296(%rbp), %r10
	testq	%rax, %rax
	movq	%rax, %rdi
	je	.L239
	movq	-4256(%rbp), %rdx
	xorl	%ecx, %ecx
	movq	%r10, %rsi
	movq	%r10, -4304(%rbp)
	movq	%rax, -4296(%rbp)
	call	X509_STORE_CTX_init@PLT
	movq	-4296(%rbp), %rdi
	movq	-4304(%rbp), %r10
	testl	%eax, %eax
	je	.L240
	call	X509_verify_cert@PLT
	movq	-4296(%rbp), %rdi
	movq	-4304(%rbp), %r10
	testl	%eax, %eax
	jle	.L241
	call	X509_STORE_CTX_get1_chain@PLT
	movq	-4296(%rbp), %rdi
	movq	%rax, %r13
	call	X509_STORE_CTX_free@PLT
	movq	-4304(%rbp), %r10
	movq	%r10, %rdi
	call	X509_STORE_free@PLT
	movq	%r13, %rax
	movl	%ebx, -4296(%rbp)
	movq	%r12, %r13
	movl	-4280(%rbp), %ebx
	movq	%rax, %r12
	jmp	.L242
.L246:
	movl	%ebx, %esi
	movq	%r12, %rdi
	addl	$1, %ebx
	call	OPENSSL_sk_value@PLT
	movq	-4176(%rbp), %rdi
	movq	%rax, %rsi
	call	OPENSSL_sk_push@PLT
.L242:
	movq	%r12, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jl	.L246
	movq	%r12, %rax
	xorl	%esi, %esi
	movl	-4296(%rbp), %ebx
	movq	%r13, %r12
	movq	%rax, %rdi
	movq	%rax, %r13
	call	OPENSSL_sk_value@PLT
	movq	%rax, %rdi
	call	X509_free@PLT
	movq	%r13, %rdi
	call	OPENSSL_sk_free@PLT
	jmp	.L376
.L203:
	movl	$16, %ebx
	jmp	.L164
.L205:
	call	opt_unknown@PLT
	leaq	-4208(%rbp), %rsi
	movq	%rax, %rdi
	call	opt_cipher@PLT
	testl	%eax, %eax
	jne	.L164
.L281:
	xorl	%ebx, %ebx
.L207:
	movq	-4288(%rbp), %rdx
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
	xorl	%r12d, %r12d
	leaq	.LC26(%rip), %rsi
	movl	$1, %r14d
	call	BIO_printf@PLT
	xorl	%r10d, %r10d
	xorl	%r11d, %r11d
.L209:
	movq	%rbx, %rdi
	movq	%r10, -4248(%rbp)
	movq	%r11, -4240(%rbp)
	call	PKCS12_free@PLT
	movq	-4232(%rbp), %rdi
	call	release_engine@PLT
	movq	-4240(%rbp), %r11
	movq	%r11, %rdi
	call	BIO_free@PLT
	movq	-4248(%rbp), %r10
	movq	%r10, %rdi
	call	BIO_free_all@PLT
	movq	%r15, %rdi
	call	OPENSSL_sk_free@PLT
	movl	$590, %edx
	leaq	.LC8(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
	movq	-4224(%rbp), %rdi
	movl	$591, %edx
	leaq	.LC8(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-4216(%rbp), %rdi
	movl	$592, %edx
	leaq	.LC8(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L384
	leaq	-40(%rbp), %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L165:
	.cfi_restore_state
	leal	-1501(%rax), %edx
	cmpl	$1, %edx
	ja	.L164
	movl	%eax, %edi
	call	opt_rand@PLT
	testl	%eax, %eax
	jne	.L164
	jmp	.L286
.L168:
	call	opt_arg@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	setup_engine@PLT
	movq	%rax, -4232(%rbp)
	jmp	.L164
.L170:
	movl	$1, -4364(%rbp)
	jmp	.L164
.L171:
	movl	$1, -4368(%rbp)
	jmp	.L164
.L172:
	call	opt_arg@PLT
	movq	%rax, -4384(%rbp)
	jmp	.L164
.L173:
	call	opt_arg@PLT
	movq	%rax, -4376(%rbp)
	jmp	.L164
.L174:
	call	opt_arg@PLT
	movq	%rax, -4256(%rbp)
	jmp	.L164
.L175:
	call	opt_arg@PLT
	movq	%rax, -4280(%rbp)
	jmp	.L164
.L176:
	call	opt_arg@PLT
	movq	%rax, -4272(%rbp)
	jmp	.L164
.L177:
	call	opt_arg@PLT
	movq	%rax, -4328(%rbp)
	jmp	.L164
.L178:
	call	opt_arg@PLT
	movq	%rax, -4296(%rbp)
	jmp	.L164
.L179:
	testq	%r15, %r15
	je	.L385
.L211:
	call	opt_arg@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	OPENSSL_sk_push@PLT
	jmp	.L164
.L180:
	call	opt_arg@PLT
	movq	%rax, -4312(%rbp)
	jmp	.L164
.L181:
	call	opt_arg@PLT
	movq	%rax, -4336(%rbp)
	jmp	.L164
.L182:
	call	opt_arg@PLT
	movq	%rax, -4304(%rbp)
	jmp	.L164
.L183:
	call	opt_arg@PLT
	movq	%rax, -4320(%rbp)
	jmp	.L164
.L184:
	call	opt_arg@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L281
	movq	%rax, %rsi
	movl	$5, %ecx
	leaq	.LC27(%rip), %rdi
	movl	$-1, -4264(%rbp)
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L164
	movq	%r8, %rdi
	movq	%r8, -4392(%rbp)
	call	OBJ_txt2nid@PLT
	movq	-4392(%rbp), %r8
	testl	%eax, %eax
	movl	%eax, -4264(%rbp)
	jne	.L164
.L374:
	movq	bio_err(%rip), %rdi
	movq	%r8, %rdx
	leaq	.LC28(%rip), %rsi
	xorl	%ebx, %ebx
	call	BIO_printf@PLT
	jmp	.L207
.L185:
	call	opt_arg@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L281
	movq	%rax, %rsi
	movl	$5, %ecx
	leaq	.LC27(%rip), %rdi
	movl	$-1, -4248(%rbp)
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L164
	movq	%r8, %rdi
	movq	%r8, -4392(%rbp)
	call	OBJ_txt2nid@PLT
	movq	-4392(%rbp), %r8
	testl	%eax, %eax
	movl	%eax, -4248(%rbp)
	jne	.L164
	jmp	.L374
.L186:
	call	opt_arg@PLT
	movq	%rax, -4360(%rbp)
	jmp	.L164
.L187:
	movq	$0, -4208(%rbp)
	jmp	.L164
.L188:
	movl	$1, -4352(%rbp)
	jmp	.L164
.L189:
	movl	$-1, -4240(%rbp)
	jmp	.L164
.L190:
	movl	$1, -4240(%rbp)
	jmp	.L164
.L191:
	movl	$2048, -4240(%rbp)
	jmp	.L164
.L192:
	movl	$1, -4344(%rbp)
	jmp	.L164
.L193:
	movl	$1, %r14d
	jmp	.L164
.L194:
	movl	$146, -4248(%rbp)
	jmp	.L164
.L195:
	movl	$0, -4348(%rbp)
	jmp	.L164
.L196:
	movl	$1, -4260(%rbp)
	jmp	.L164
.L197:
	movl	$1, -4340(%rbp)
	jmp	.L164
.L198:
	orl	$4, %r13d
	jmp	.L164
.L199:
	orl	$3, %r13d
	jmp	.L164
.L200:
	orl	$16, %r13d
	jmp	.L164
.L201:
	orl	$8, %r13d
	jmp	.L164
.L202:
	orl	$2, %r13d
	jmp	.L164
.L204:
	orl	$1, %r13d
	jmp	.L164
.L206:
	leaq	pkcs12_options(%rip), %rdi
	xorl	%ebx, %ebx
	xorl	%r12d, %r12d
	xorl	%r14d, %r14d
	call	opt_help@PLT
	xorl	%r10d, %r10d
	xorl	%r11d, %r11d
	jmp	.L209
.L385:
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	jne	.L211
	jmp	.L286
	.p2align 4,,10
	.p2align 3
.L381:
	movq	-4280(%rbp), %rsi
	movq	-4256(%rbp), %rdi
	leaq	-4216(%rbp), %rcx
	leaq	-4224(%rbp), %rdx
	call	app_passwd@PLT
	testl	%eax, %eax
	je	.L274
.L217:
	movq	-4224(%rbp), %r12
	jmp	.L218
.L213:
	movq	-4280(%rbp), %rsi
	movq	-4272(%rbp), %rdi
	leaq	-4216(%rbp), %rcx
	leaq	-4224(%rbp), %rdx
	call	app_passwd@PLT
	testl	%eax, %eax
	je	.L274
	testl	%r14d, %r14d
	je	.L217
	jmp	.L275
.L227:
	movl	-4280(%rbp), %eax
	testl	%eax, %eax
	je	.L271
	cmpq	$0, -4304(%rbp)
	je	.L386
	movq	-4304(%rbp), %rdi
	xorl	%ecx, %ecx
	leaq	-4176(%rbp), %rsi
	leaq	.LC37(%rip), %r8
	movl	$32773, %edx
	call	load_certs@PLT
	testl	%eax, %eax
	je	.L300
	movq	$0, -4272(%rbp)
	movq	$0, -4256(%rbp)
.L376:
	cmpq	$0, -4272(%rbp)
	setne	%al
	cmpq	$0, -4312(%rbp)
	setne	%dl
	andl	%eax, %edx
	andb	-4352(%rbp), %al
	movb	%dl, -4280(%rbp)
	movb	%al, -4296(%rbp)
.L238:
	movq	%r12, -4304(%rbp)
	xorl	%r13d, %r13d
	jmp	.L247
	.p2align 4,,10
	.p2align 3
.L248:
	movl	%r13d, %esi
	movq	%r15, %rdi
	call	OPENSSL_sk_value@PLT
	movq	-4176(%rbp), %rdi
	movl	%r13d, %esi
	addl	$1, %r13d
	movq	%rax, %r12
	call	OPENSSL_sk_value@PLT
	movl	$-1, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	X509_alias_set1@PLT
.L247:
	movq	%r15, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r13d
	jl	.L248
	cmpb	$0, -4280(%rbp)
	movq	-4304(%rbp), %r12
	jne	.L387
.L249:
	cmpb	$0, -4296(%rbp)
	jne	.L388
.L250:
	movl	-4396(%rbp), %esi
	testl	%esi, %esi
	je	.L389
.L251:
	movl	-4260(%rbp), %ecx
	testl	%ecx, %ecx
	je	.L390
.L252:
	movl	-4344(%rbp), %eax
	movl	-4264(%rbp), %r9d
	pushq	%rbx
	movq	-4176(%rbp), %r8
	movq	-4256(%rbp), %rcx
	pushq	$-1
	movq	-4272(%rbp), %rdx
	movq	-4336(%rbp), %rsi
	pushq	%rax
	movl	-4248(%rbp), %eax
	movq	-4392(%rbp), %rdi
	pushq	%rax
	call	PKCS12_create@PLT
	addq	$32, %rsp
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L391
	movq	-4360(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L254
	leaq	-4168(%rbp), %rsi
	call	opt_md@PLT
	testl	%eax, %eax
	je	.L207
.L254:
	movl	-4240(%rbp), %eax
	cmpl	$-1, %eax
	je	.L255
	subq	$8, %rsp
	pushq	-4168(%rbp)
	movl	%eax, %r9d
	xorl	%r8d, %r8d
	movl	$-1, %edx
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	PKCS12_set_mac@PLT
	popq	%rax
	popq	%rdx
.L255:
	movq	-4328(%rbp), %rdi
	movl	$1, %edx
	movl	$6, %esi
	call	bio_open_owner@PLT
	movq	%rax, %r10
	testq	%rax, %rax
	je	.L256
	movq	%rbx, %rsi
	movq	%rax, %rdi
	movq	%rax, -4240(%rbp)
	xorl	%r14d, %r14d
	call	i2d_PKCS12_bio@PLT
	movq	-4240(%rbp), %r10
.L226:
	movq	-4272(%rbp), %rdi
	movq	%r10, -4240(%rbp)
	call	EVP_PKEY_free@PLT
	movq	X509_free@GOTPCREL(%rip), %rsi
	movq	-4176(%rbp), %rdi
	call	OPENSSL_sk_pop_free@PLT
	movq	-4256(%rbp), %rdi
	call	X509_free@PLT
	movq	-4240(%rbp), %r10
.L256:
	xorl	%r11d, %r11d
	xorl	%r12d, %r12d
	jmp	.L209
.L274:
	movq	bio_err(%rip), %rdi
	leaq	.LC29(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L286:
	xorl	%ebx, %ebx
	xorl	%r10d, %r10d
	xorl	%r11d, %r11d
	xorl	%r12d, %r12d
	movl	$1, %r14d
	jmp	.L209
.L219:
	movl	-4260(%rbp), %r8d
	leaq	-2112(%rbp), %rdi
	testl	%r8d, %r8d
	je	.L223
	movl	%r14d, %ecx
	leaq	.LC32(%rip), %rdx
	movl	$2048, %esi
	movq	%rdi, -4256(%rbp)
	call	EVP_read_pw_string@PLT
	movq	-4256(%rbp), %rdi
	testl	%eax, %eax
	jne	.L392
.L223:
	leaq	-4160(%rbp), %rax
	movq	%rdi, %r12
	movl	$0, -4396(%rbp)
	movq	%rax, -4392(%rbp)
	jmp	.L220
.L382:
	movq	bio_err(%rip), %rdi
	testl	%r14d, %r14d
	je	.L221
	leaq	.LC30(%rip), %rsi
	xorl	%eax, %eax
	xorl	%ebx, %ebx
	xorl	%r12d, %r12d
	call	BIO_printf@PLT
	xorl	%r10d, %r10d
	xorl	%r11d, %r11d
	jmp	.L209
.L224:
	movq	-4296(%rbp), %rdi
	movl	$6, %edx
	movl	$114, %esi
	call	bio_open_default@PLT
	movq	%rax, %r11
	testq	%rax, %rax
	je	.L290
	movq	-4328(%rbp), %rdi
	movl	$1, %edx
	movl	$32773, %esi
	movq	%rax, -4240(%rbp)
	call	bio_open_owner@PLT
	movq	-4240(%rbp), %r11
	testq	%rax, %rax
	movq	%rax, %r10
	je	.L291
	xorl	%esi, %esi
	movq	%r11, %rdi
	movq	%rax, -4248(%rbp)
	call	d2i_PKCS12_bio@PLT
	movq	-4240(%rbp), %r11
	movq	-4248(%rbp), %r10
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.L393
	movl	-4396(%rbp), %eax
	testl	%eax, %eax
	je	.L394
.L258:
	movl	-4260(%rbp), %eax
	testl	%eax, %eax
	je	.L395
.L259:
	testb	$4, %r13b
	jne	.L396
.L261:
	movl	-4348(%rbp), %r9d
	testl	%r9d, %r9d
	je	.L297
	cmpb	$0, (%r12)
	jne	.L267
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	movq	%r10, -4248(%rbp)
	movq	%r11, -4240(%rbp)
	call	PKCS12_verify_mac@PLT
	movq	-4240(%rbp), %r11
	movq	-4248(%rbp), %r10
	testl	%eax, %eax
	je	.L267
	movl	-4260(%rbp), %r8d
	movq	-4392(%rbp), %rax
	xorl	%r12d, %r12d
	testl	%r8d, %r8d
	cmove	%r12, %rax
	movq	%rax, -4392(%rbp)
.L266:
	subq	$8, %rsp
	pushq	-4208(%rbp)
	movq	%rbx, %rsi
	movq	%r10, %rdi
	movq	-4216(%rbp), %r9
	movl	$-1, %ecx
	movl	%r13d, %r8d
	movq	%r11, -4248(%rbp)
	movq	-4392(%rbp), %rdx
	movq	%r10, -4240(%rbp)
	call	dump_certs_keys_p12
	popq	%rcx
	popq	%rsi
	testl	%eax, %eax
	movq	-4240(%rbp), %r10
	movq	-4248(%rbp), %r11
	jne	.L209
	movq	bio_err(%rip), %rdi
	leaq	.LC46(%rip), %rsi
	movq	%r10, -4248(%rbp)
	movl	$1, %r14d
	movq	%r11, -4240(%rbp)
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	movq	-4240(%rbp), %r11
	movq	-4248(%rbp), %r10
	jmp	.L209
.L300:
	movl	-4280(%rbp), %r14d
.L379:
	movq	$0, -4256(%rbp)
	movq	$0, -4272(%rbp)
.L378:
	xorl	%ebx, %ebx
	xorl	%r10d, %r10d
	jmp	.L226
.L221:
	leaq	.LC31(%rip), %rsi
	xorl	%eax, %eax
	xorl	%ebx, %ebx
	xorl	%r12d, %r12d
	call	BIO_printf@PLT
	movl	-4260(%rbp), %r14d
	xorl	%r10d, %r10d
	xorl	%r11d, %r11d
	jmp	.L209
.L368:
	movq	-4296(%rbp), %rdi
	xorl	%ecx, %ecx
	leaq	-4176(%rbp), %rsi
	leaq	.LC47(%rip), %r8
	movl	$32773, %edx
	call	load_certs@PLT
	testl	%eax, %eax
	je	.L379
	movl	-4340(%rbp), %eax
	movq	$0, -4272(%rbp)
	movl	%eax, -4280(%rbp)
	jmp	.L375
.L389:
	leaq	-4160(%rbp), %rdi
	movl	$1, %ecx
	movl	$2048, %esi
	leaq	.LC39(%rip), %rdx
	call	EVP_read_pw_string@PLT
	testl	%eax, %eax
	je	.L251
	movq	bio_err(%rip), %rdi
	leaq	.LC33(%rip), %rsi
	xorl	%eax, %eax
	xorl	%ebx, %ebx
	call	BIO_printf@PLT
	xorl	%r10d, %r10d
	jmp	.L226
.L388:
	movq	-4272(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$856, %esi
	movl	$-1, %r8d
	call	EVP_PKEY_add1_attr_by_NID@PLT
	jmp	.L250
.L387:
	movl	$-1, %r8d
	movl	$4097, %edx
	movl	$417, %esi
	movq	-4312(%rbp), %rcx
	movq	-4272(%rbp), %rdi
	call	EVP_PKEY_add1_attr_by_NID@PLT
	jmp	.L249
.L290:
	xorl	%ebx, %ebx
	xorl	%r10d, %r10d
	xorl	%r12d, %r12d
	movl	$1, %r14d
	jmp	.L209
.L297:
	xorl	%r12d, %r12d
	jmp	.L266
.L390:
	leaq	-4160(%rbp), %rsi
	leaq	-2112(%rbp), %rdi
	movl	$2048, %edx
	call	OPENSSL_strlcpy@PLT
	jmp	.L252
.L383:
	movq	bio_err(%rip), %rdi
	leaq	.LC34(%rip), %rsi
	xorl	%eax, %eax
	xorl	%ebx, %ebx
	call	BIO_printf@PLT
	xorl	%r10d, %r10d
	movq	$0, -4256(%rbp)
	movq	$0, -4272(%rbp)
	jmp	.L226
.L392:
	movq	bio_err(%rip), %rdi
	leaq	.LC33(%rip), %rsi
	xorl	%eax, %eax
	xorl	%ebx, %ebx
	call	BIO_printf@PLT
	movl	-4260(%rbp), %r14d
	xorl	%r10d, %r10d
	xorl	%r11d, %r11d
	jmp	.L209
.L291:
	xorl	%ebx, %ebx
	xorl	%r12d, %r12d
	movl	$1, %r14d
	jmp	.L209
.L267:
	movl	$-1, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%r10, -4248(%rbp)
	movq	%r11, -4240(%rbp)
	call	PKCS12_verify_mac@PLT
	movq	-4240(%rbp), %r11
	movq	-4248(%rbp), %r10
	testl	%eax, %eax
	jne	.L297
	xorl	%edx, %edx
	movq	%r12, %rdi
	movl	$-1, %esi
	leaq	-4168(%rbp), %rcx
	call	OPENSSL_asc2uni@PLT
	movq	-4240(%rbp), %r11
	movq	-4248(%rbp), %r10
	testq	%rax, %rax
	movq	%rax, %rdi
	je	.L268
	movl	-4168(%rbp), %esi
	movq	%r10, -4256(%rbp)
	movq	%r11, -4248(%rbp)
	movq	%rax, -4240(%rbp)
	call	OPENSSL_uni2utf8@PLT
	movq	-4240(%rbp), %rdi
	movl	$564, %edx
	leaq	.LC8(%rip), %rsi
	movq	%rax, %r12
	call	CRYPTO_free@PLT
	movl	$-1, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	PKCS12_verify_mac@PLT
	movq	-4248(%rbp), %r11
	movq	-4256(%rbp), %r10
	testl	%eax, %eax
	movq	%r10, -4248(%rbp)
	movq	%r11, -4240(%rbp)
	je	.L397
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
	leaq	.LC45(%rip), %rsi
	call	BIO_printf@PLT
	movl	-4260(%rbp), %edi
	movq	-4392(%rbp), %rax
	movq	-4240(%rbp), %r11
	movq	-4248(%rbp), %r10
	testl	%edi, %edi
	cmove	%r12, %rax
	movq	%rax, -4392(%rbp)
	jmp	.L266
.L386:
	movq	$0, -4272(%rbp)
	movq	$0, -4256(%rbp)
	movb	$0, -4296(%rbp)
	movb	$0, -4280(%rbp)
	jmp	.L238
.L394:
	xorl	%ecx, %ecx
	leaq	-4160(%rbp), %rdi
	leaq	.LC40(%rip), %rdx
	movl	$2048, %esi
	movq	%r10, -4248(%rbp)
	movq	%r11, -4240(%rbp)
	call	EVP_read_pw_string@PLT
	movq	-4240(%rbp), %r11
	movq	-4248(%rbp), %r10
	testl	%eax, %eax
	je	.L258
	movq	bio_err(%rip), %rdi
	leaq	.LC33(%rip), %rsi
	xorl	%eax, %eax
	xorl	%r12d, %r12d
	movl	$1, %r14d
	call	BIO_printf@PLT
	movq	-4240(%rbp), %r11
	movq	-4248(%rbp), %r10
	jmp	.L209
.L289:
	movl	-4280(%rbp), %r14d
	xorl	%ebx, %ebx
	jmp	.L226
.L396:
	movq	%rbx, %rdi
	movq	%r10, -4248(%rbp)
	movq	%r11, -4240(%rbp)
	call	PKCS12_mac_present@PLT
	movq	-4240(%rbp), %r11
	movq	-4248(%rbp), %r10
	testl	%eax, %eax
	je	.L261
	movq	%rbx, %r8
	leaq	-4200(%rbp), %rcx
	leaq	-4168(%rbp), %rdx
	leaq	-4192(%rbp), %rsi
	leaq	-4176(%rbp), %rdi
	call	PKCS12_get0_mac@PLT
	movq	-4192(%rbp), %rcx
	xorl	%edx, %edx
	xorl	%esi, %esi
	leaq	-4184(%rbp), %rdi
	call	X509_ALGOR_get0@PLT
	movq	bio_err(%rip), %rdi
	leaq	.LC41(%rip), %rsi
	call	BIO_puts@PLT
	movq	bio_err(%rip), %rdi
	movq	-4184(%rbp), %rsi
	call	i2a_ASN1_OBJECT@PLT
	movq	-4200(%rbp), %rdi
	movl	$1, %edx
	movq	-4240(%rbp), %r11
	movq	-4248(%rbp), %r10
	testq	%rdi, %rdi
	je	.L263
	call	ASN1_INTEGER_get@PLT
	movq	-4248(%rbp), %r10
	movq	-4240(%rbp), %r11
	movq	%rax, %rdx
.L263:
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
	leaq	.LC42(%rip), %rsi
	movq	%r10, -4248(%rbp)
	movq	%r11, -4240(%rbp)
	call	BIO_printf@PLT
	movq	-4168(%rbp), %rdi
	xorl	%ecx, %ecx
	movq	-4240(%rbp), %r11
	movq	-4248(%rbp), %r10
	testq	%rdi, %rdi
	je	.L264
	call	ASN1_STRING_length@PLT
	movq	-4248(%rbp), %r10
	movq	-4240(%rbp), %r11
	movslq	%eax, %rcx
.L264:
	movq	-4176(%rbp), %rdi
	xorl	%edx, %edx
	testq	%rdi, %rdi
	je	.L265
	movq	%rcx, -4256(%rbp)
	movq	%r10, -4248(%rbp)
	movq	%r11, -4240(%rbp)
	call	ASN1_STRING_length@PLT
	movq	-4256(%rbp), %rcx
	movq	-4248(%rbp), %r10
	movq	-4240(%rbp), %r11
	movslq	%eax, %rdx
.L265:
	movq	bio_err(%rip), %rdi
	leaq	.LC43(%rip), %rsi
	xorl	%eax, %eax
	movq	%r10, -4248(%rbp)
	movq	%r11, -4240(%rbp)
	call	BIO_printf@PLT
	movq	-4248(%rbp), %r10
	movq	-4240(%rbp), %r11
	jmp	.L261
.L395:
	leaq	-4160(%rbp), %rsi
	leaq	-2112(%rbp), %rdi
	movl	$2048, %edx
	movq	%r10, -4248(%rbp)
	movq	%r11, -4240(%rbp)
	call	OPENSSL_strlcpy@PLT
	movq	-4248(%rbp), %r10
	movq	-4240(%rbp), %r11
	jmp	.L259
.L231:
	movq	-4296(%rbp), %rdi
	xorl	%ecx, %ecx
	leaq	-4176(%rbp), %rsi
	leaq	.LC47(%rip), %r8
	movl	$32773, %edx
	call	load_certs@PLT
	testl	%eax, %eax
	je	.L398
	movl	%ebx, -4256(%rbp)
	movq	-4272(%rbp), %rbx
	movq	%r12, -4296(%rbp)
	movl	%r13d, %r12d
	jmp	.L277
	.p2align 4,,10
	.p2align 3
.L234:
	movq	-4176(%rbp), %rdi
	movl	%r12d, %esi
	call	OPENSSL_sk_value@PLT
	movq	%rbx, %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	X509_check_private_key@PLT
	testl	%eax, %eax
	jne	.L399
	addl	$1, %r12d
.L277:
	movq	-4176(%rbp), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r12d
	jl	.L234
.L235:
	movq	bio_err(%rip), %rdi
	leaq	.LC36(%rip), %rsi
	xorl	%eax, %eax
	xorl	%ebx, %ebx
	call	BIO_printf@PLT
	xorl	%r10d, %r10d
	movq	$0, -4256(%rbp)
	jmp	.L226
.L399:
	movq	%r13, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movl	-4256(%rbp), %ebx
	movq	%rax, %rdi
	movl	%r12d, %r13d
	movq	%rax, -4256(%rbp)
	movq	-4296(%rbp), %r12
	call	X509_keyid_set1@PLT
	movq	-4256(%rbp), %rdi
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	X509_alias_set1@PLT
	movq	-4176(%rbp), %rdi
	movl	%r13d, %esi
	call	OPENSSL_sk_delete@PLT
	cmpq	$0, -4256(%rbp)
	jne	.L232
	jmp	.L235
	.p2align 4,,10
	.p2align 3
.L240:
	movq	%r10, -4240(%rbp)
.L377:
	call	X509_STORE_CTX_free@PLT
	movq	-4240(%rbp), %r10
	movq	%r10, %rdi
	call	X509_STORE_free@PLT
.L244:
	movq	bio_err(%rip), %rdi
	xorl	%ebx, %ebx
	call	ERR_print_errors@PLT
	movl	-4280(%rbp), %r14d
	xorl	%r10d, %r10d
	jmp	.L226
.L239:
	xorl	%edi, %edi
	movq	%r10, -4240(%rbp)
	call	X509_STORE_CTX_free@PLT
	movq	-4240(%rbp), %r10
	movq	%r10, %rdi
	call	X509_STORE_free@PLT
	jmp	.L244
.L391:
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	xorl	%r10d, %r10d
	jmp	.L226
.L393:
	movq	bio_err(%rip), %rdi
	xorl	%r12d, %r12d
	movl	$1, %r14d
	call	ERR_print_errors@PLT
	movq	-4240(%rbp), %r11
	movq	-4248(%rbp), %r10
	jmp	.L209
.L241:
	movq	%r10, -4248(%rbp)
	movq	%rdi, -4240(%rbp)
	call	X509_STORE_CTX_get_error@PLT
	movq	-4248(%rbp), %r10
	movq	-4240(%rbp), %rdi
	testl	%eax, %eax
	movl	%eax, %ebx
	movq	%r10, -4240(%rbp)
	je	.L377
.L243:
	endbr64
	call	X509_STORE_CTX_free@PLT
	movq	-4240(%rbp), %r10
	movq	%r10, %rdi
	call	X509_STORE_free@PLT
	cmpl	$1, %ebx
	je	.L244
	movslq	%ebx, %rdi
	xorl	%ebx, %ebx
	call	X509_verify_cert_error_string@PLT
	movq	bio_err(%rip), %rdi
	leaq	.LC38(%rip), %rsi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movl	-4280(%rbp), %r14d
	xorl	%r10d, %r10d
	jmp	.L226
.L397:
	movq	bio_err(%rip), %rdi
	leaq	.LC44(%rip), %rsi
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	movq	-4240(%rbp), %r11
	movq	-4248(%rbp), %r10
	movq	%r12, %rdi
.L268:
	movl	-4348(%rbp), %r14d
	movq	%rdi, %r12
	jmp	.L209
.L398:
	movq	$0, -4256(%rbp)
	xorl	%ebx, %ebx
	xorl	%r10d, %r10d
	jmp	.L226
.L384:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1459:
	.size	pkcs12_main, .-pkcs12_main
	.p2align 4
	.globl	hex_prin
	.type	hex_prin, @function
hex_prin:
.LFB1468:
	.cfi_startproc
	endbr64
	testl	%edx, %edx
	jle	.L405
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leal	-1(%rdx), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	leaq	1(%rsi,%rax), %r14
	pushq	%r13
	.cfi_offset 13, -32
	leaq	.LC9(%rip), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	.p2align 4,,10
	.p2align 3
.L402:
	movzbl	(%rbx), %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	addq	$1, %rbx
	call	BIO_printf@PLT
	cmpq	%r14, %rbx
	jne	.L402
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L405:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	ret
	.cfi_endproc
.LFE1468:
	.size	hex_prin, .-hex_prin
	.globl	pkcs12_options
	.section	.rodata.str1.1
.LC48:
	.string	"help"
.LC49:
	.string	"Display this summary"
.LC50:
	.string	"nokeys"
.LC51:
	.string	"Don't output private keys"
.LC52:
	.string	"keyex"
.LC53:
	.string	"Set MS key exchange type"
.LC54:
	.string	"keysig"
.LC55:
	.string	"Set MS key signature type"
.LC56:
	.string	"nocerts"
.LC57:
	.string	"Don't output certificates"
.LC58:
	.string	"clcerts"
	.section	.rodata.str1.8
	.align 8
.LC59:
	.string	"Only output client certificates"
	.section	.rodata.str1.1
.LC60:
	.string	"cacerts"
.LC61:
	.string	"Only output CA certificates"
.LC62:
	.string	"noout"
	.section	.rodata.str1.8
	.align 8
.LC63:
	.string	"Don't output anything, just verify"
	.section	.rodata.str1.1
.LC64:
	.string	"info"
	.section	.rodata.str1.8
	.align 8
.LC65:
	.string	"Print info about PKCS#12 structure"
	.section	.rodata.str1.1
.LC66:
	.string	"chain"
.LC67:
	.string	"Add certificate chain"
.LC68:
	.string	"twopass"
	.section	.rodata.str1.8
	.align 8
.LC69:
	.string	"Separate MAC, encryption passwords"
	.section	.rodata.str1.1
.LC70:
	.string	"nomacver"
.LC71:
	.string	"Don't verify MAC"
.LC72:
	.string	"descert"
	.section	.rodata.str1.8
	.align 8
.LC73:
	.string	"Encrypt output with 3DES (default RC2-40)"
	.section	.rodata.str1.1
.LC74:
	.string	"certpbe"
	.section	.rodata.str1.8
	.align 8
.LC75:
	.string	"Certificate PBE algorithm (default RC2-40)"
	.section	.rodata.str1.1
.LC76:
	.string	"export"
.LC77:
	.string	"Output PKCS12 file"
.LC78:
	.string	"noiter"
	.section	.rodata.str1.8
	.align 8
.LC79:
	.string	"Don't use encryption iteration"
	.section	.rodata.str1.1
.LC80:
	.string	"maciter"
.LC81:
	.string	"Use MAC iteration"
.LC82:
	.string	"nomaciter"
.LC83:
	.string	"Don't use MAC iteration"
.LC84:
	.string	"nomac"
.LC85:
	.string	"Don't generate MAC"
.LC86:
	.string	"LMK"
	.section	.rodata.str1.8
	.align 8
.LC87:
	.string	"Add local machine keyset attribute to private key"
	.section	.rodata.str1.1
.LC88:
	.string	"nodes"
.LC89:
	.string	"Don't encrypt private keys"
.LC90:
	.string	"macalg"
	.section	.rodata.str1.8
	.align 8
.LC91:
	.string	"Digest algorithm used in MAC (default SHA1)"
	.section	.rodata.str1.1
.LC92:
	.string	"keypbe"
	.section	.rodata.str1.8
	.align 8
.LC93:
	.string	"Private key PBE algorithm (default 3DES)"
	.section	.rodata.str1.1
.LC94:
	.string	"rand"
	.section	.rodata.str1.8
	.align 8
.LC95:
	.string	"Load the file(s) into the random number generator"
	.section	.rodata.str1.1
.LC96:
	.string	"writerand"
	.section	.rodata.str1.8
	.align 8
.LC97:
	.string	"Write random data to the specified file"
	.section	.rodata.str1.1
.LC98:
	.string	"inkey"
.LC99:
	.string	"Private key if not infile"
.LC100:
	.string	"certfile"
.LC101:
	.string	"Load certs from file"
.LC102:
	.string	"name"
.LC103:
	.string	"Use name as friendly name"
.LC104:
	.string	"CSP"
.LC105:
	.string	"Microsoft CSP name"
.LC106:
	.string	"caname"
	.section	.rodata.str1.8
	.align 8
.LC107:
	.string	"Use name as CA friendly name (can be repeated)"
	.section	.rodata.str1.1
.LC108:
	.string	"in"
.LC109:
	.string	"Input filename"
.LC110:
	.string	"out"
.LC111:
	.string	"Output filename"
.LC112:
	.string	"passin"
.LC113:
	.string	"Input file pass phrase source"
.LC114:
	.string	"passout"
	.section	.rodata.str1.8
	.align 8
.LC115:
	.string	"Output file pass phrase source"
	.section	.rodata.str1.1
.LC116:
	.string	"password"
	.section	.rodata.str1.8
	.align 8
.LC117:
	.string	"Set import/export password source"
	.section	.rodata.str1.1
.LC118:
	.string	"CApath"
.LC119:
	.string	"PEM-format directory of CA's"
.LC120:
	.string	"CAfile"
.LC121:
	.string	"PEM-format file of CA's"
.LC122:
	.string	"no-CAfile"
	.section	.rodata.str1.8
	.align 8
.LC123:
	.string	"Do not load the default certificates file"
	.section	.rodata.str1.1
.LC124:
	.string	"no-CApath"
	.section	.rodata.str1.8
	.align 8
.LC125:
	.string	"Do not load certificates from the default certificates directory"
	.section	.rodata.str1.1
.LC126:
	.string	""
.LC127:
	.string	"Any supported cipher"
.LC128:
	.string	"engine"
	.section	.rodata.str1.8
	.align 8
.LC129:
	.string	"Use engine, possibly a hardware device"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	pkcs12_options, @object
	.size	pkcs12_options, 1008
pkcs12_options:
	.quad	.LC48
	.long	1
	.long	45
	.quad	.LC49
	.quad	.LC50
	.long	3
	.long	45
	.quad	.LC51
	.quad	.LC52
	.long	4
	.long	45
	.quad	.LC53
	.quad	.LC54
	.long	5
	.long	45
	.quad	.LC55
	.quad	.LC56
	.long	6
	.long	45
	.quad	.LC57
	.quad	.LC58
	.long	7
	.long	45
	.quad	.LC59
	.quad	.LC60
	.long	8
	.long	45
	.quad	.LC61
	.quad	.LC62
	.long	9
	.long	45
	.quad	.LC63
	.quad	.LC64
	.long	10
	.long	45
	.quad	.LC65
	.quad	.LC66
	.long	11
	.long	45
	.quad	.LC67
	.quad	.LC68
	.long	12
	.long	45
	.quad	.LC69
	.quad	.LC70
	.long	13
	.long	45
	.quad	.LC71
	.quad	.LC72
	.long	14
	.long	45
	.quad	.LC73
	.quad	.LC74
	.long	23
	.long	115
	.quad	.LC75
	.quad	.LC76
	.long	15
	.long	45
	.quad	.LC77
	.quad	.LC78
	.long	16
	.long	45
	.quad	.LC79
	.quad	.LC80
	.long	17
	.long	45
	.quad	.LC81
	.quad	.LC82
	.long	18
	.long	45
	.quad	.LC83
	.quad	.LC84
	.long	19
	.long	45
	.quad	.LC85
	.quad	.LC86
	.long	20
	.long	45
	.quad	.LC87
	.quad	.LC88
	.long	21
	.long	45
	.quad	.LC89
	.quad	.LC90
	.long	22
	.long	115
	.quad	.LC91
	.quad	.LC92
	.long	24
	.long	115
	.quad	.LC93
	.quad	.LC94
	.long	1501
	.long	115
	.quad	.LC95
	.quad	.LC96
	.long	1502
	.long	62
	.quad	.LC97
	.quad	.LC98
	.long	25
	.long	115
	.quad	.LC99
	.quad	.LC100
	.long	26
	.long	60
	.quad	.LC101
	.quad	.LC102
	.long	27
	.long	115
	.quad	.LC103
	.quad	.LC104
	.long	28
	.long	115
	.quad	.LC105
	.quad	.LC106
	.long	29
	.long	115
	.quad	.LC107
	.quad	.LC108
	.long	30
	.long	60
	.quad	.LC109
	.quad	.LC110
	.long	31
	.long	62
	.quad	.LC111
	.quad	.LC112
	.long	32
	.long	115
	.quad	.LC113
	.quad	.LC114
	.long	33
	.long	115
	.quad	.LC115
	.quad	.LC116
	.long	34
	.long	115
	.quad	.LC117
	.quad	.LC118
	.long	35
	.long	47
	.quad	.LC119
	.quad	.LC120
	.long	36
	.long	60
	.quad	.LC121
	.quad	.LC122
	.long	38
	.long	45
	.quad	.LC123
	.quad	.LC124
	.long	37
	.long	45
	.quad	.LC125
	.quad	.LC126
	.long	2
	.long	45
	.quad	.LC127
	.quad	.LC128
	.long	39
	.long	115
	.quad	.LC129
	.quad	0
	.zero	16
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
