	.file	"crl2p7.c"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"%s: Use -help for summary.\n"
.LC1:
	.string	"unable to load CRL\n"
.LC2:
	.string	"r"
.LC3:
	.string	"error opening the file, %s\n"
.LC4:
	.string	"error reading the file, %s\n"
.LC5:
	.string	"error loading certificates\n"
.LC6:
	.string	"unable to write pkcs7 object\n"
	.text
	.p2align 4
	.globl	crl2pkcs7_main
	.type	crl2pkcs7_main, @function
crl2pkcs7_main:
.LFB1435:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	crl2pkcs7_options(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	.L5(%rip), %r13
	pushq	%r12
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	xorl	%ebx, %ebx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$32773, -64(%rbp)
	movl	$32773, -60(%rbp)
	call	opt_init@PLT
	movq	$0, -72(%rbp)
	movq	%rax, %r14
.L2:
	call	opt_next@PLT
	testl	%eax, %eax
	je	.L64
	addl	$1, %eax
	cmpl	$8, %eax
	ja	.L2
	movslq	0(%r13,%rax,4), %rax
	addq	%r13, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L5:
	.long	.L12-.L5
	.long	.L2-.L5
	.long	.L11-.L5
	.long	.L10-.L5
	.long	.L9-.L5
	.long	.L8-.L5
	.long	.L7-.L5
	.long	.L6-.L5
	.long	.L4-.L5
	.text
.L6:
	movl	$1, %ebx
	jmp	.L2
.L10:
	call	opt_arg@PLT
	leaq	-64(%rbp), %rdx
	movl	$2, %esi
	movq	%rax, %rdi
	call	opt_format@PLT
	testl	%eax, %eax
	jne	.L2
.L12:
	movq	bio_err(%rip), %rdi
	movq	%r14, %rdx
	leaq	.LC0(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L36:
	movl	$1, %r13d
	xorl	%r14d, %r14d
	xorl	%r15d, %r15d
	xorl	%r8d, %r8d
	xorl	%ebx, %ebx
.L13:
	movq	%r12, %rdi
	movq	%r8, -72(%rbp)
	call	OPENSSL_sk_free@PLT
	movq	%rbx, %rdi
	call	BIO_free@PLT
	movq	-72(%rbp), %r8
	movq	%r8, %rdi
	call	BIO_free_all@PLT
	movq	%r15, %rdi
	call	PKCS7_free@PLT
	movq	%r14, %rdi
	call	X509_CRL_free@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L65
	addq	$72, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L4:
	.cfi_restore_state
	testq	%r12, %r12
	je	.L66
.L15:
	call	opt_arg@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	jne	.L2
	jmp	.L36
.L7:
	call	opt_arg@PLT
	movq	%rax, -72(%rbp)
	jmp	.L2
.L8:
	call	opt_arg@PLT
	movq	%rax, %r15
	jmp	.L2
.L9:
	call	opt_arg@PLT
	leaq	-60(%rbp), %rdx
	movl	$2, %esi
	movq	%rax, %rdi
	call	opt_format@PLT
	testl	%eax, %eax
	jne	.L2
	jmp	.L12
.L11:
	leaq	crl2pkcs7_options(%rip), %rdi
	xorl	%r13d, %r13d
	xorl	%r14d, %r14d
	xorl	%r15d, %r15d
	call	opt_help@PLT
	xorl	%ebx, %ebx
	xorl	%r8d, %r8d
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L66:
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L15
	xorl	%r14d, %r14d
	xorl	%r15d, %r15d
	xorl	%r8d, %r8d
	xorl	%ebx, %ebx
	movl	$1, %r13d
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L64:
	call	opt_num_rest@PLT
	movl	%eax, %r13d
	testl	%eax, %eax
	jne	.L12
	testl	%ebx, %ebx
	je	.L67
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
.L17:
	call	PKCS7_new@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L43
	call	PKCS7_SIGNED_new@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L62
	movl	$22, %edi
	movq	%rax, -80(%rbp)
	call	OBJ_nid2obj@PLT
	movq	-80(%rbp), %r8
	movl	$21, %edi
	movq	%rax, %xmm0
	movq	%r8, %xmm1
	movq	%r8, -88(%rbp)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 24(%r15)
	movq	40(%r8), %rdx
	movq	%rdx, -80(%rbp)
	call	OBJ_nid2obj@PLT
	movq	-80(%rbp), %rdx
	movq	-88(%rbp), %r8
	movl	$1, %esi
	movq	%rax, 24(%rdx)
	movq	(%r8), %rdi
	movq	%r8, -80(%rbp)
	call	ASN1_INTEGER_set@PLT
	testl	%eax, %eax
	je	.L41
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L43
	movq	-80(%rbp), %r8
	movq	%rax, 24(%r8)
	testq	%r14, %r14
	je	.L22
	movq	%r14, %rsi
	call	OPENSSL_sk_push@PLT
	movq	-80(%rbp), %r8
.L22:
	movq	%r8, -80(%rbp)
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L43
	movq	-80(%rbp), %r8
	movl	$0, -92(%rbp)
	movq	%rax, 16(%r8)
	testq	%r12, %r12
	je	.L24
	.p2align 4,,10
	.p2align 3
.L23:
	movq	%r12, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, -92(%rbp)
	jge	.L24
	movl	-92(%rbp), %esi
	movq	%r12, %rdi
	call	OPENSSL_sk_value@PLT
	leaq	.LC2(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, -88(%rbp)
	call	BIO_new_file@PLT
	movq	%rax, -104(%rbp)
	testq	%rax, %rax
	je	.L68
	movq	%rax, %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	PEM_X509_INFO_read_bio@PLT
	movq	%rax, -80(%rbp)
	testq	%rax, %rax
	jne	.L27
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L29:
	movq	-80(%rbp), %rdi
	call	OPENSSL_sk_shift@PLT
	movq	(%rax), %rsi
	movq	%rax, %r8
	testq	%rsi, %rsi
	je	.L28
	movq	%r14, %rdi
	movq	%rax, -88(%rbp)
	call	OPENSSL_sk_push@PLT
	movq	-88(%rbp), %r8
	movq	$0, (%r8)
.L28:
	movq	%r8, %rdi
	call	X509_INFO_free@PLT
.L27:
	movq	-80(%rbp), %rdi
	call	OPENSSL_sk_num@PLT
	testl	%eax, %eax
	jne	.L29
.L30:
	endbr64
	movq	-104(%rbp), %rdi
	call	BIO_free@PLT
	movq	-80(%rbp), %rdi
	call	OPENSSL_sk_free@PLT
	addl	$1, -92(%rbp)
	jmp	.L23
.L67:
	movl	-64(%rbp), %edx
	movl	$114, %esi
	movq	%r15, %rdi
	call	bio_open_default@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L38
	movl	-64(%rbp), %eax
	cmpl	$4, %eax
	je	.L70
	cmpl	$32773, %eax
	je	.L20
.L21:
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
	xorl	%r14d, %r14d
	xorl	%r15d, %r15d
	leaq	.LC1(%rip), %rsi
	movl	$1, %r13d
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	xorl	%r8d, %r8d
	jmp	.L13
.L43:
	xorl	%r8d, %r8d
	movl	$1, %r13d
	jmp	.L13
.L41:
	movl	$1, %r13d
	xorl	%r8d, %r8d
	jmp	.L13
.L45:
	xorl	%r14d, %r14d
.L62:
	movl	$1, %r13d
	jmp	.L13
.L20:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	PEM_read_bio_X509_CRL@PLT
	movq	%rax, %r14
.L19:
	testq	%r14, %r14
	jne	.L17
	jmp	.L21
.L38:
	xorl	%r14d, %r14d
	xorl	%r15d, %r15d
	xorl	%r8d, %r8d
	movl	$1, %r13d
	jmp	.L13
.L70:
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	d2i_X509_CRL_bio@PLT
	movq	%rax, %r14
	jmp	.L19
.L24:
	movl	-60(%rbp), %edx
	movq	-72(%rbp), %rdi
	movl	$119, %esi
	call	bio_open_default@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L45
	movl	-60(%rbp), %eax
	cmpl	$4, %eax
	je	.L71
	cmpl	$32773, %eax
	je	.L72
.L33:
	movl	-92(%rbp), %eax
	xorl	%r14d, %r14d
	testl	%eax, %eax
	jne	.L13
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
	movq	%r8, -72(%rbp)
	movl	$1, %r13d
	leaq	.LC6(%rip), %rsi
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	movq	-72(%rbp), %r8
	jmp	.L13
.L65:
	call	__stack_chk_fail@PLT
.L72:
	movq	%r8, %rdi
	movq	%r15, %rsi
	movq	%r8, -72(%rbp)
	call	PEM_write_bio_PKCS7@PLT
	movq	-72(%rbp), %r8
	movl	%eax, -92(%rbp)
	jmp	.L33
.L71:
	movq	%r8, %rdi
	movq	%r15, %rsi
	movq	%r8, -72(%rbp)
	call	i2d_PKCS7_bio@PLT
	movq	-72(%rbp), %r8
	movl	%eax, -92(%rbp)
	jmp	.L33
.L68:
	movq	-88(%rbp), %rdx
	movq	bio_err(%rip), %rdi
	leaq	.LC3(%rip), %rsi
	call	BIO_printf@PLT
	xorl	%edi, %edi
	call	BIO_free@PLT
	xorl	%edi, %edi
	call	OPENSSL_sk_free@PLT
.L26:
	movq	bio_err(%rip), %rdi
	leaq	.LC5(%rip), %rsi
	xorl	%eax, %eax
	xorl	%r14d, %r14d
	movl	$1, %r13d
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	xorl	%r8d, %r8d
	jmp	.L13
.L69:
	movq	-88(%rbp), %rdx
	movq	bio_err(%rip), %rdi
	leaq	.LC4(%rip), %rsi
	call	BIO_printf@PLT
	movq	-104(%rbp), %rdi
	call	BIO_free@PLT
	xorl	%edi, %edi
	call	OPENSSL_sk_free@PLT
	jmp	.L26
	.cfi_endproc
.LFE1435:
	.size	crl2pkcs7_main, .-crl2pkcs7_main
	.globl	crl2pkcs7_options
	.section	.rodata.str1.1
.LC7:
	.string	"help"
.LC8:
	.string	"Display this summary"
.LC9:
	.string	"inform"
.LC10:
	.string	"Input format - DER or PEM"
.LC11:
	.string	"outform"
.LC12:
	.string	"Output format - DER or PEM"
.LC13:
	.string	"in"
.LC14:
	.string	"Input file"
.LC15:
	.string	"out"
.LC16:
	.string	"Output file"
.LC17:
	.string	"nocrl"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC18:
	.string	"No crl to load, just certs from '-certfile'"
	.section	.rodata.str1.1
.LC19:
	.string	"certfile"
	.section	.rodata.str1.8
	.align 8
.LC20:
	.string	"File of chain of certs to a trusted CA; can be repeated"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	crl2pkcs7_options, @object
	.size	crl2pkcs7_options, 192
crl2pkcs7_options:
	.quad	.LC7
	.long	1
	.long	45
	.quad	.LC8
	.quad	.LC9
	.long	2
	.long	70
	.quad	.LC10
	.quad	.LC11
	.long	3
	.long	70
	.quad	.LC12
	.quad	.LC13
	.long	4
	.long	60
	.quad	.LC14
	.quad	.LC15
	.long	5
	.long	62
	.quad	.LC16
	.quad	.LC17
	.long	6
	.long	45
	.quad	.LC18
	.quad	.LC19
	.long	7
	.long	60
	.quad	.LC20
	.quad	0
	.zero	16
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
