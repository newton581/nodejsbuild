	.file	"errstr.c"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"%s: Use -help for summary.\n"
.LC1:
	.string	"%lx"
.LC2:
	.string	"%s\n"
	.text
	.p2align 4
	.globl	errstr_main
	.type	errstr_main, @function
errstr_main:
.LFB1555:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	errstr_options(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$288, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	opt_init@PLT
	movq	%rax, %r13
.L2:
	call	opt_next@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	je	.L20
	cmpl	$-1, %r12d
	je	.L3
	cmpl	$1, %r12d
	jne	.L2
	leaq	errstr_options(%rip), %rdi
	xorl	%r12d, %r12d
	call	opt_help@PLT
.L5:
.L1:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L21
	addq	$288, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	.cfi_restore_state
	movq	bio_err(%rip), %rdi
	movq	%r13, %rdx
	leaq	.LC0(%rip), %rsi
	xorl	%eax, %eax
	movl	$1, %r12d
	call	BIO_printf@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L20:
	call	opt_rest@PLT
	movq	(%rax), %rdi
	movq	%rax, %r13
	testq	%rdi, %rdi
	je	.L1
	leaq	-312(%rbp), %rbx
	leaq	-304(%rbp), %r14
	.p2align 4,,10
	.p2align 3
.L10:
	xorl	%eax, %eax
	movq	%rbx, %rdx
	leaq	.LC1(%rip), %rsi
	call	__isoc99_sscanf@PLT
	testl	%eax, %eax
	jne	.L7
	movq	8(%r13), %rdi
	addq	$8, %r13
	addl	$1, %r12d
	testq	%rdi, %rdi
	jne	.L10
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L7:
	xorl	%esi, %esi
	movl	$2097154, %edi
	addq	$8, %r13
	call	OPENSSL_init_ssl@PLT
	movq	-312(%rbp), %rdi
	movl	$256, %edx
	movq	%r14, %rsi
	call	ERR_error_string_n@PLT
	movq	bio_out(%rip), %rdi
	movq	%r14, %rdx
	xorl	%eax, %eax
	leaq	.LC2(%rip), %rsi
	call	BIO_printf@PLT
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	jne	.L10
	jmp	.L1
.L21:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1555:
	.size	errstr_main, .-errstr_main
	.globl	errstr_options
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"Usage: %s [options] errnum...\n"
	.section	.rodata.str1.1
.LC4:
	.string	"  errnum  Error number\n"
.LC5:
	.string	"help"
.LC6:
	.string	"Display this summary"
	.section	.data.rel.ro,"aw"
	.align 32
	.type	errstr_options, @object
	.size	errstr_options, 96
errstr_options:
	.quad	OPT_HELP_STR
	.long	1
	.long	45
	.quad	.LC3
	.quad	OPT_HELP_STR
	.long	1
	.long	45
	.quad	.LC4
	.quad	.LC5
	.long	1
	.long	45
	.quad	.LC6
	.quad	0
	.zero	16
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
