	.file	"ciphers.c"
	.text
	.p2align 4
	.type	dummy_psk, @function
dummy_psk:
.LFB1555:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1555:
	.size	dummy_psk, .-dummy_psk
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	""
	.text
	.p2align 4
	.type	dummy_srp, @function
dummy_srp:
.LFB1556:
	.cfi_startproc
	endbr64
	leaq	.LC0(%rip), %rax
	ret
	.cfi_endproc
.LFE1556:
	.size	dummy_srp, .-dummy_srp
	.section	.rodata.str1.1
.LC1:
	.string	"UNKNOWN"
.LC2:
	.string	"%s: Use -help for summary.\n"
.LC3:
	.string	"OpenSSL cipher name: %s\n"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"Error setting TLSv1.3 ciphersuites\n"
	.section	.rodata.str1.1
.LC5:
	.string	"Error in cipher list\n"
.LC6:
	.string	"\n"
.LC7:
	.string	":"
.LC8:
	.string	"%s"
.LC9:
	.string	"          0x%02X,0x%02X - "
	.section	.rodata.str1.8
	.align 8
.LC10:
	.string	"0x%02X,0x%02X,0x%02X,0x%02X - "
	.section	.rodata.str1.1
.LC11:
	.string	"%s - "
	.text
	.p2align 4
	.globl	ciphers_main
	.type	ciphers_main, @function
ciphers_main:
.LFB1557:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	xorl	%r14d, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%edi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	.L8(%rip), %rbx
	subq	$584, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	TLS_server_method@PLT
	movq	%r13, %rsi
	movl	%r12d, %edi
	leaq	ciphers_options(%rip), %rdx
	movq	%rax, -624(%rbp)
	xorl	%r12d, %r12d
	call	opt_init@PLT
	movq	$0, -608(%rbp)
	movq	$0, -584(%rbp)
	movq	%rax, %r13
	movl	$0, -592(%rbp)
	movl	$0, -600(%rbp)
	movl	$0, -616(%rbp)
	movl	$0, -588(%rbp)
	movl	$0, -612(%rbp)
.L5:
	call	opt_next@PLT
	testl	%eax, %eax
	je	.L90
.L24:
	addl	$1, %eax
	cmpl	$15, %eax
	ja	.L5
	movslq	(%rbx,%rax,4), %rax
	addq	%rbx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L8:
	.long	.L22-.L8
	.long	.L5-.L8
	.long	.L21-.L8
	.long	.L20-.L8
	.long	.L19-.L8
	.long	.L18-.L8
	.long	.L17-.L8
	.long	.L16-.L8
	.long	.L15-.L8
	.long	.L14-.L8
	.long	.L13-.L8
	.long	.L12-.L8
	.long	.L11-.L8
	.long	.L88-.L8
	.long	.L9-.L8
	.long	.L7-.L8
	.text
.L9:
	movl	$1, -612(%rbp)
.L88:
	movl	$1, %r12d
	call	opt_next@PLT
	testl	%eax, %eax
	jne	.L24
.L90:
	movl	%eax, %r15d
	call	opt_rest@PLT
	movq	%rax, %rbx
	call	opt_num_rest@PLT
	cmpl	$1, %eax
	je	.L91
	testl	%eax, %eax
	jne	.L22
	xorl	%ebx, %ebx
.L26:
	movq	-584(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L27
	call	OPENSSL_cipher_name@PLT
	movq	bio_out(%rip), %rdi
	leaq	.LC3(%rip), %rsi
	movq	%rax, %rdx
	jmp	.L89
.L22:
	movq	bio_err(%rip), %rdi
	movq	%r13, %rdx
	leaq	.LC2(%rip), %rsi
.L89:
	xorl	%eax, %eax
	movl	$1, %r15d
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	call	BIO_printf@PLT
	movq	$0, -584(%rbp)
.L23:
	movl	-588(%rbp), %eax
	testl	%eax, %eax
	jne	.L92
.L47:
	movq	%r13, %rdi
	call	SSL_CTX_free@PLT
	movq	-584(%rbp), %rdi
	call	SSL_free@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L93
	addq	$584, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L7:
	.cfi_restore_state
	movl	$1, -588(%rbp)
	jmp	.L5
.L11:
	call	opt_arg@PLT
	movq	%rax, -608(%rbp)
	jmp	.L5
.L12:
	movl	$1, -592(%rbp)
	jmp	.L5
.L13:
	movl	$1, -600(%rbp)
	jmp	.L5
.L14:
	movl	$772, %r14d
	jmp	.L5
.L15:
	movl	$771, %r14d
	jmp	.L5
.L16:
	movl	$770, %r14d
	jmp	.L5
.L17:
	movl	$769, %r14d
	jmp	.L5
.L18:
	movl	$768, %r14d
	jmp	.L5
.L19:
	call	opt_arg@PLT
	movq	%rax, -584(%rbp)
	jmp	.L5
.L20:
	movl	$1, -616(%rbp)
	movl	$1, %r12d
	jmp	.L5
.L21:
	leaq	ciphers_options(%rip), %rdi
	xorl	%r15d, %r15d
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	call	opt_help@PLT
	movl	-588(%rbp), %eax
	movq	$0, -584(%rbp)
	testl	%eax, %eax
	je	.L47
.L92:
	movq	%r14, %rdi
	call	OPENSSL_sk_free@PLT
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L91:
	movq	(%rbx), %rbx
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L27:
	movq	-624(%rbp), %rdi
	call	SSL_CTX_new@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L32
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movl	$123, %esi
	movq	%rax, %rdi
	call	SSL_CTX_ctrl@PLT
	testq	%rax, %rax
	jne	.L94
.L32:
	movq	bio_err(%rip), %rdi
	xorl	%r14d, %r14d
	movl	$1, %r15d
	call	ERR_print_errors@PLT
	jmp	.L23
.L94:
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movl	$124, %esi
	movq	%r13, %rdi
	call	SSL_CTX_ctrl@PLT
	testq	%rax, %rax
	je	.L32
	movl	-600(%rbp), %esi
	testl	%esi, %esi
	jne	.L95
.L29:
	movl	-592(%rbp), %ecx
	testl	%ecx, %ecx
	jne	.L96
.L30:
	movq	-608(%rbp), %rax
	testq	%rax, %rax
	je	.L31
	movq	%rax, %rsi
	movq	%r13, %rdi
	call	SSL_CTX_set_ciphersuites@PLT
	testl	%eax, %eax
	je	.L97
.L31:
	testq	%rbx, %rbx
	je	.L33
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	SSL_CTX_set_cipher_list@PLT
	testl	%eax, %eax
	je	.L98
.L33:
	movq	%r13, %rdi
	call	SSL_new@PLT
	movq	%rax, -600(%rbp)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L32
	movl	-588(%rbp), %edx
	testl	%edx, %edx
	je	.L34
	call	SSL_get1_supported_ciphers@PLT
	movq	%rax, %r14
.L35:
	xorl	%ebx, %ebx
	testl	%r12d, %r12d
	je	.L49
	movq	%r13, -584(%rbp)
	movl	-616(%rbp), %r12d
	movl	%r15d, -592(%rbp)
	movl	-612(%rbp), %r13d
	jmp	.L37
.L42:
	testl	%r12d, %r12d
	jne	.L99
.L44:
	leaq	-576(%rbp), %rsi
	movl	$512, %edx
	movq	%r15, %rdi
	addl	$1, %ebx
	call	SSL_CIPHER_description@PLT
	movq	bio_out(%rip), %rdi
	movq	%rax, %rsi
	call	BIO_puts@PLT
.L37:
	movq	%r14, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jge	.L100
	movl	%ebx, %esi
	movq	%r14, %rdi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %r15
	testl	%r13d, %r13d
	je	.L42
	movq	%rax, %rdi
	call	SSL_CIPHER_get_id@PLT
	movl	%eax, %esi
	movzbl	%ah, %edx
	movzbl	%al, %r9d
	andl	$-16777216, %eax
	cmpl	$50331648, %eax
	je	.L101
	movq	%rsi, %rcx
	movq	bio_out(%rip), %rdi
	shrq	$24, %rsi
	xorl	%eax, %eax
	shrq	$16, %rcx
	movl	%edx, %r8d
	movl	%esi, %edx
	movzbl	%cl, %ecx
	leaq	.LC10(%rip), %rsi
	call	BIO_printf@PLT
	jmp	.L42
.L99:
	movq	%r15, %rdi
	call	SSL_CIPHER_standard_name@PLT
	movq	bio_out(%rip), %rdi
	leaq	.LC11(%rip), %rsi
	testq	%rax, %rax
	movq	%rax, %rdx
	leaq	.LC1(%rip), %rax
	cmove	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L44
.L96:
	leaq	dummy_srp(%rip), %rsi
	movq	%r13, %rdi
	call	SSL_CTX_set_srp_client_pwd_callback@PLT
	jmp	.L30
.L95:
	leaq	dummy_psk(%rip), %rsi
	movq	%r13, %rdi
	call	SSL_CTX_set_psk_client_callback@PLT
	jmp	.L29
.L100:
	movq	-600(%rbp), %rax
	movq	-584(%rbp), %r13
	movl	-592(%rbp), %r15d
	movq	%rax, -584(%rbp)
	jmp	.L23
.L49:
	leaq	.LC7(%rip), %r12
	jmp	.L36
.L39:
	movq	%r15, %rdx
	leaq	.LC8(%rip), %rsi
	xorl	%eax, %eax
	addl	$1, %ebx
	call	BIO_printf@PLT
.L36:
	movq	%r14, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jge	.L41
	movl	%ebx, %esi
	movq	%r14, %rdi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %rdi
	call	SSL_CIPHER_get_name@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L41
	movq	bio_out(%rip), %rdi
	testl	%ebx, %ebx
	je	.L39
	movq	%r12, %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_out(%rip), %rdi
	jmp	.L39
.L41:
	movq	bio_out(%rip), %rdi
	leaq	.LC6(%rip), %rsi
	xorl	%eax, %eax
	xorl	%r15d, %r15d
	call	BIO_printf@PLT
	movq	-600(%rbp), %rax
	movq	%rax, -584(%rbp)
	jmp	.L23
.L34:
	call	SSL_get_ciphers@PLT
	movq	%rax, %r14
	jmp	.L35
.L101:
	movq	bio_out(%rip), %rdi
	movl	%r9d, %ecx
	leaq	.LC9(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L42
.L98:
	movq	bio_err(%rip), %rdi
	leaq	.LC5(%rip), %rsi
	call	BIO_printf@PLT
	jmp	.L32
.L97:
	movq	bio_err(%rip), %rdi
	leaq	.LC4(%rip), %rsi
	call	BIO_printf@PLT
	jmp	.L32
.L93:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1557:
	.size	ciphers_main, .-ciphers_main
	.globl	ciphers_options
	.section	.rodata.str1.1
.LC12:
	.string	"help"
.LC13:
	.string	"Display this summary"
.LC14:
	.string	"v"
	.section	.rodata.str1.8
	.align 8
.LC15:
	.string	"Verbose listing of the SSL/TLS ciphers"
	.section	.rodata.str1.1
.LC16:
	.string	"V"
.LC17:
	.string	"Even more verbose"
.LC18:
	.string	"s"
.LC19:
	.string	"Only supported ciphers"
.LC20:
	.string	"tls1"
.LC21:
	.string	"TLS1 mode"
.LC22:
	.string	"tls1_1"
.LC23:
	.string	"TLS1.1 mode"
.LC24:
	.string	"tls1_2"
.LC25:
	.string	"TLS1.2 mode"
.LC26:
	.string	"tls1_3"
.LC27:
	.string	"TLS1.3 mode"
.LC28:
	.string	"stdname"
.LC29:
	.string	"Show standard cipher names"
.LC30:
	.string	"psk"
	.section	.rodata.str1.8
	.align 8
.LC31:
	.string	"include ciphersuites requiring PSK"
	.section	.rodata.str1.1
.LC32:
	.string	"srp"
	.section	.rodata.str1.8
	.align 8
.LC33:
	.string	"include ciphersuites requiring SRP"
	.section	.rodata.str1.1
.LC34:
	.string	"convert"
	.section	.rodata.str1.8
	.align 8
.LC35:
	.string	"Convert standard name into OpenSSL name"
	.section	.rodata.str1.1
.LC36:
	.string	"ciphersuites"
	.section	.rodata.str1.8
	.align 8
.LC37:
	.string	"Configure the TLSv1.3 ciphersuites to use"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	ciphers_options, @object
	.size	ciphers_options, 336
ciphers_options:
	.quad	.LC12
	.long	1
	.long	45
	.quad	.LC13
	.quad	.LC14
	.long	12
	.long	45
	.quad	.LC15
	.quad	.LC16
	.long	13
	.long	45
	.quad	.LC17
	.quad	.LC18
	.long	14
	.long	45
	.quad	.LC19
	.quad	.LC20
	.long	5
	.long	45
	.quad	.LC21
	.quad	.LC22
	.long	6
	.long	45
	.quad	.LC23
	.quad	.LC24
	.long	7
	.long	45
	.quad	.LC25
	.quad	.LC26
	.long	8
	.long	45
	.quad	.LC27
	.quad	.LC28
	.long	2
	.long	45
	.quad	.LC29
	.quad	.LC30
	.long	9
	.long	45
	.quad	.LC31
	.quad	.LC32
	.long	10
	.long	45
	.quad	.LC33
	.quad	.LC34
	.long	3
	.long	115
	.quad	.LC35
	.quad	.LC36
	.long	11
	.long	115
	.quad	.LC37
	.quad	0
	.zero	16
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
