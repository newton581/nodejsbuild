	.file	"ec.c"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"%s: Use -help for summary.\n"
.LC1:
	.string	"Error getting passwords\n"
.LC2:
	.string	"read EC key\n"
.LC3:
	.string	"Public Key"
.LC4:
	.string	"Private Key"
.LC5:
	.string	"unable to load Key\n"
.LC6:
	.string	"EC Key valid.\n"
.LC7:
	.string	"EC Key Invalid!\n"
.LC8:
	.string	"writing EC key\n"
.LC9:
	.string	"unable to write private key\n"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC10:
	.string	"../deps/openssl/openssl/apps/ec.c"
	.text
	.p2align 4
	.globl	ec_main
	.type	ec_main, @function
ec_main:
.LFB1435:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	ec_options(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	xorl	%r14d, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	.L5(%rip), %rbx
	subq	$136, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movq	$0, -72(%rbp)
	movq	$0, -64(%rbp)
	movl	$32773, -92(%rbp)
	movl	$32773, -88(%rbp)
	call	opt_init@PLT
	movl	$0, -112(%rbp)
	movl	$0, -156(%rbp)
	movq	%rax, %r13
	movl	$0, -136(%rbp)
	movl	$0, -132(%rbp)
	movl	$0, -104(%rbp)
	movl	$0, -164(%rbp)
	movl	$0, -160(%rbp)
	movl	$1, -172(%rbp)
	movq	$0, -128(%rbp)
	movq	$0, -120(%rbp)
	movq	$0, -152(%rbp)
	movq	$0, -144(%rbp)
	movl	$4, -168(%rbp)
.L2:
	call	opt_next@PLT
	testl	%eax, %eax
	je	.L91
.L26:
	addl	$1, %eax
	cmpl	$19, %eax
	ja	.L2
	movslq	(%rbx,%rax,4), %rax
	addq	%rbx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L5:
	.long	.L23-.L5
	.long	.L2-.L5
	.long	.L22-.L5
	.long	.L21-.L5
	.long	.L20-.L5
	.long	.L19-.L5
	.long	.L18-.L5
	.long	.L17-.L5
	.long	.L16-.L5
	.long	.L15-.L5
	.long	.L14-.L5
	.long	.L13-.L5
	.long	.L12-.L5
	.long	.L11-.L5
	.long	.L10-.L5
	.long	.L9-.L5
	.long	.L8-.L5
	.long	.L7-.L5
	.long	.L6-.L5
	.long	.L4-.L5
	.text
.L16:
	movl	$1, %r12d
	call	opt_next@PLT
	testl	%eax, %eax
	jne	.L26
.L91:
	call	opt_num_rest@PLT
	movl	%eax, %ebx
	testl	%eax, %eax
	jne	.L23
	movl	-132(%rbp), %ecx
	movl	-112(%rbp), %eax
	movl	%r15d, %edx
	xorl	$1, %edx
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdi
	orl	%r15d, %ecx
	orl	%ecx, %eax
	movl	%ecx, -176(%rbp)
	movl	%edx, %ecx
	leaq	-72(%rbp), %rdx
	xorl	$1, %eax
	andl	-104(%rbp), %ecx
	cmovne	%ecx, %eax
	leaq	-64(%rbp), %rcx
	movl	%eax, -132(%rbp)
	call	app_passwd@PLT
	testl	%eax, %eax
	je	.L92
	movl	-92(%rbp), %edx
	cmpl	$8, %edx
	je	.L52
	movq	-144(%rbp), %rdi
	movl	$114, %esi
	call	bio_open_default@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L93
.L29:
	movq	bio_err(%rip), %rdi
	leaq	.LC2(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movl	-92(%rbp), %eax
	cmpl	$4, %eax
	je	.L94
	cmpl	$8, %eax
	je	.L95
	testl	%r15d, %r15d
	je	.L37
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	PEM_read_bio_EC_PUBKEY@PLT
	movq	%rax, %r15
.L32:
	testq	%r15, %r15
	je	.L38
	movl	-132(%rbp), %edx
	movl	-88(%rbp), %esi
	movq	-152(%rbp), %rdi
	call	bio_open_owner@PLT
	movq	%rax, %r10
	testq	%rax, %rax
	je	.L54
	movq	%r15, %rdi
	movq	%rax, -120(%rbp)
	call	EC_KEY_get0_group@PLT
	movq	-120(%rbp), %r10
	movq	%rax, -128(%rbp)
	movl	-160(%rbp), %eax
	testl	%eax, %eax
	jne	.L96
.L39:
	movl	-164(%rbp), %eax
	testl	%eax, %eax
	jne	.L97
.L40:
	movl	-136(%rbp), %eax
	testl	%eax, %eax
	jne	.L98
.L41:
	movl	-104(%rbp), %eax
	testl	%eax, %eax
	jne	.L99
.L42:
	movl	-156(%rbp), %r11d
	testl	%r11d, %r11d
	jne	.L100
.L43:
	testl	%r12d, %r12d
	jne	.L24
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
	leaq	.LC8(%rip), %rsi
	movq	%r10, -104(%rbp)
	call	BIO_printf@PLT
	cmpl	$4, -88(%rbp)
	movq	-104(%rbp), %r10
	je	.L101
	movl	-112(%rbp), %edi
	testl	%edi, %edi
	jne	.L102
	movl	-176(%rbp), %esi
	testl	%esi, %esi
	je	.L50
	movq	%r10, %rdi
	movq	%r15, %rsi
	movq	%r10, -104(%rbp)
	call	PEM_write_bio_EC_PUBKEY@PLT
	movq	-104(%rbp), %r10
	movl	%eax, -84(%rbp)
.L47:
	xorl	%ebx, %ebx
	testl	%eax, %eax
	jne	.L24
	movq	bio_err(%rip), %rdi
	leaq	.LC9(%rip), %rsi
	movq	%r10, -104(%rbp)
	movl	$1, %ebx
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	movq	-104(%rbp), %r10
	jmp	.L24
.L7:
	call	opt_unknown@PLT
	leaq	-80(%rbp), %rsi
	movq	%rax, %rdi
	call	opt_cipher@PLT
	testl	%eax, %eax
	jne	.L2
.L23:
	movq	bio_err(%rip), %rdi
	movq	%r13, %rdx
	xorl	%eax, %eax
	xorl	%r15d, %r15d
	leaq	.LC0(%rip), %rsi
	movl	$1, %ebx
	xorl	%r13d, %r13d
	call	BIO_printf@PLT
	xorl	%r10d, %r10d
.L24:
	movq	%r13, %rdi
	movq	%r10, -104(%rbp)
	call	BIO_free@PLT
	movq	-104(%rbp), %r10
	movq	%r10, %rdi
	call	BIO_free_all@PLT
	movq	%r15, %rdi
	call	EC_KEY_free@PLT
	movq	%r14, %rdi
	call	release_engine@PLT
	movq	-72(%rbp), %rdi
	movl	$275, %edx
	leaq	.LC10(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-64(%rbp), %rdi
	movl	$276, %edx
	leaq	.LC10(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L103
	leaq	-40(%rbp), %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L4:
	.cfi_restore_state
	movl	$1, -156(%rbp)
	jmp	.L2
.L6:
	movl	$1, -136(%rbp)
	jmp	.L2
.L8:
	call	opt_arg@PLT
	leaq	-84(%rbp), %rdx
	leaq	conv_forms(%rip), %rsi
	movq	%rax, %rdi
	call	opt_pair@PLT
	testl	%eax, %eax
	je	.L23
	movl	$1, -160(%rbp)
	movl	-84(%rbp), %eax
	movl	%eax, -168(%rbp)
	jmp	.L2
.L9:
	call	opt_arg@PLT
	leaq	-84(%rbp), %rdx
	leaq	param_enc(%rip), %rsi
	movq	%rax, %rdi
	call	opt_pair@PLT
	testl	%eax, %eax
	je	.L23
	movl	$1, -164(%rbp)
	movl	-84(%rbp), %eax
	movl	%eax, -172(%rbp)
	jmp	.L2
.L10:
	call	opt_arg@PLT
	movq	%rax, -128(%rbp)
	jmp	.L2
.L11:
	call	opt_arg@PLT
	movq	%rax, -120(%rbp)
	jmp	.L2
.L12:
	movl	$1, -132(%rbp)
	jmp	.L2
.L13:
	movl	$1, %r15d
	jmp	.L2
.L14:
	movl	$1, -112(%rbp)
	jmp	.L2
.L15:
	movl	$1, -104(%rbp)
	jmp	.L2
.L17:
	call	opt_arg@PLT
	movq	%rax, -152(%rbp)
	jmp	.L2
.L18:
	call	opt_arg@PLT
	movq	%rax, -144(%rbp)
	jmp	.L2
.L19:
	call	opt_arg@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	setup_engine@PLT
	movq	%rax, %r14
	jmp	.L2
.L20:
	call	opt_arg@PLT
	leaq	-88(%rbp), %rdx
	movl	$2, %esi
	movq	%rax, %rdi
	call	opt_format@PLT
	testl	%eax, %eax
	jne	.L2
	jmp	.L23
.L21:
	call	opt_arg@PLT
	leaq	-92(%rbp), %rdx
	movl	$1982, %esi
	movq	%rax, %rdi
	call	opt_format@PLT
	testl	%eax, %eax
	jne	.L2
	jmp	.L23
.L22:
	leaq	ec_options(%rip), %rdi
	xorl	%ebx, %ebx
	xorl	%r15d, %r15d
	xorl	%r13d, %r13d
	call	opt_help@PLT
	xorl	%r10d, %r10d
	jmp	.L24
.L94:
	xorl	%esi, %esi
	movq	%r13, %rdi
	testl	%r15d, %r15d
	je	.L31
	call	d2i_EC_PUBKEY_bio@PLT
	movq	%rax, %r15
	jmp	.L32
.L93:
	xorl	%r15d, %r15d
	xorl	%r10d, %r10d
	movl	$1, %ebx
	jmp	.L24
.L52:
	xorl	%r13d, %r13d
	jmp	.L29
.L38:
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
	movl	$1, %ebx
	xorl	%r15d, %r15d
	leaq	.LC5(%rip), %rsi
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	xorl	%r10d, %r10d
	jmp	.L24
.L92:
	movq	bio_err(%rip), %rdi
	leaq	.LC1(%rip), %rsi
	xorl	%r15d, %r15d
	xorl	%r13d, %r13d
	movl	$1, %ebx
	call	BIO_printf@PLT
	xorl	%r10d, %r10d
	jmp	.L24
.L95:
	movq	-72(%rbp), %rcx
	testl	%r15d, %r15d
	je	.L34
	movq	-144(%rbp), %rdi
	leaq	.LC3(%rip), %r9
	movq	%r14, %r8
	movl	$1, %edx
	movl	$8, %esi
	call	load_pubkey@PLT
	movq	%rax, %rdi
.L35:
	testq	%rdi, %rdi
	je	.L38
	movq	%rdi, -120(%rbp)
	call	EVP_PKEY_get1_EC_KEY@PLT
	movq	-120(%rbp), %rdi
	movq	%rax, %r15
	call	EVP_PKEY_free@PLT
	jmp	.L32
.L37:
	movq	-72(%rbp), %rcx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	PEM_read_bio_ECPrivateKey@PLT
	movq	%rax, %r15
	jmp	.L32
.L54:
	movl	$1, %ebx
	jmp	.L24
.L31:
	call	d2i_ECPrivateKey_bio@PLT
	movq	%rax, %r15
	jmp	.L32
.L99:
	xorl	%edx, %edx
	movq	%r10, %rdi
	movq	%r15, %rsi
	movq	%r10, -120(%rbp)
	call	EC_KEY_print@PLT
	movq	-120(%rbp), %r10
	testl	%eax, %eax
	jne	.L42
	movq	-152(%rbp), %rdi
	movq	%r10, -112(%rbp)
	call	perror@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	movl	-104(%rbp), %ebx
	movq	-112(%rbp), %r10
	jmp	.L24
.L98:
	movl	$2, %esi
	movq	%r15, %rdi
	movq	%r10, -120(%rbp)
	call	EC_KEY_set_enc_flags@PLT
	movq	-120(%rbp), %r10
	jmp	.L41
.L97:
	movl	-172(%rbp), %esi
	movq	%r15, %rdi
	movq	%r10, -120(%rbp)
	call	EC_KEY_set_asn1_flag@PLT
	movq	-120(%rbp), %r10
	jmp	.L40
.L96:
	movl	-168(%rbp), %esi
	movq	%r15, %rdi
	call	EC_KEY_set_conv_form@PLT
	movq	-120(%rbp), %r10
	jmp	.L39
.L100:
	movq	%r15, %rdi
	movq	%r10, -104(%rbp)
	call	EC_KEY_check_key@PLT
	movq	-104(%rbp), %r10
	cmpl	$1, %eax
	je	.L104
	movq	bio_err(%rip), %rdi
	leaq	.LC7(%rip), %rsi
	xorl	%eax, %eax
	movq	%r10, -104(%rbp)
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	movq	-104(%rbp), %r10
	jmp	.L43
.L34:
	movq	-144(%rbp), %rdi
	leaq	.LC4(%rip), %r9
	movq	%r14, %r8
	movl	$1, %edx
	movl	$8, %esi
	call	load_key@PLT
	movq	%rax, %rdi
	jmp	.L35
.L101:
	movl	-112(%rbp), %r9d
	testl	%r9d, %r9d
	jne	.L105
	movl	-176(%rbp), %r8d
	movq	%r10, -104(%rbp)
	movq	%r15, %rsi
	movq	%r10, %rdi
	testl	%r8d, %r8d
	je	.L48
	call	i2d_EC_PUBKEY_bio@PLT
	movq	-104(%rbp), %r10
	movl	%eax, -84(%rbp)
	jmp	.L47
.L50:
	subq	$8, %rsp
	pushq	-64(%rbp)
	movq	-80(%rbp), %rdx
	xorl	%ecx, %ecx
	movq	%r10, %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r15, %rsi
	movq	%r10, -104(%rbp)
	call	PEM_write_bio_ECPrivateKey@PLT
	popq	%rdx
	movq	-104(%rbp), %r10
	movl	%eax, -84(%rbp)
	popq	%rcx
	jmp	.L47
.L102:
	movq	-128(%rbp), %rsi
	movq	%r10, %rdi
	movq	%r10, -104(%rbp)
	call	PEM_write_bio_ECPKParameters@PLT
	movq	-104(%rbp), %r10
	movl	%eax, -84(%rbp)
	jmp	.L47
.L104:
	movq	bio_err(%rip), %rdi
	leaq	.LC6(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	-104(%rbp), %r10
	jmp	.L43
.L48:
	call	i2d_ECPrivateKey_bio@PLT
	movq	-104(%rbp), %r10
	movl	%eax, -84(%rbp)
	jmp	.L47
.L105:
	movq	-128(%rbp), %rdx
	movq	i2d_ECPKParameters@GOTPCREL(%rip), %rdi
	movq	%r10, %rsi
	call	ASN1_i2d_bio@PLT
	movq	-104(%rbp), %r10
	movl	%eax, -84(%rbp)
	jmp	.L47
.L103:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1435:
	.size	ec_main, .-ec_main
	.globl	ec_options
	.section	.rodata.str1.1
.LC11:
	.string	"help"
.LC12:
	.string	"Display this summary"
.LC13:
	.string	"in"
.LC14:
	.string	"Input file"
.LC15:
	.string	"inform"
.LC16:
	.string	"Input format - DER or PEM"
.LC17:
	.string	"out"
.LC18:
	.string	"Output file"
.LC19:
	.string	"outform"
.LC20:
	.string	"Output format - DER or PEM"
.LC21:
	.string	"noout"
.LC22:
	.string	"Don't print key out"
.LC23:
	.string	"text"
.LC24:
	.string	"Print the key"
.LC25:
	.string	"param_out"
	.section	.rodata.str1.8
	.align 8
.LC26:
	.string	"Print the elliptic curve parameters"
	.section	.rodata.str1.1
.LC27:
	.string	"pubin"
	.section	.rodata.str1.8
	.align 8
.LC28:
	.string	"Expect a public key in input file"
	.section	.rodata.str1.1
.LC29:
	.string	"pubout"
	.section	.rodata.str1.8
	.align 8
.LC30:
	.string	"Output public key, not private"
	.section	.rodata.str1.1
.LC31:
	.string	"no_public"
	.section	.rodata.str1.8
	.align 8
.LC32:
	.string	"exclude public key from private key"
	.section	.rodata.str1.1
.LC33:
	.string	"check"
.LC34:
	.string	"check key consistency"
.LC35:
	.string	"passin"
.LC36:
	.string	"Input file pass phrase source"
.LC37:
	.string	"passout"
	.section	.rodata.str1.8
	.align 8
.LC38:
	.string	"Output file pass phrase source"
	.section	.rodata.str1.1
.LC39:
	.string	"param_enc"
	.section	.rodata.str1.8
	.align 8
.LC40:
	.string	"Specifies the way the ec parameters are encoded"
	.section	.rodata.str1.1
.LC41:
	.string	"conv_form"
	.section	.rodata.str1.8
	.align 8
.LC42:
	.string	"Specifies the point conversion form "
	.section	.rodata.str1.1
.LC43:
	.string	""
.LC44:
	.string	"Any supported cipher"
.LC45:
	.string	"engine"
	.section	.rodata.str1.8
	.align 8
.LC46:
	.string	"Use engine, possibly a hardware device"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	ec_options, @object
	.size	ec_options, 456
ec_options:
	.quad	.LC11
	.long	1
	.long	45
	.quad	.LC12
	.quad	.LC13
	.long	5
	.long	115
	.quad	.LC14
	.quad	.LC15
	.long	2
	.long	102
	.quad	.LC16
	.quad	.LC17
	.long	6
	.long	62
	.quad	.LC18
	.quad	.LC19
	.long	3
	.long	70
	.quad	.LC20
	.quad	.LC21
	.long	7
	.long	45
	.quad	.LC22
	.quad	.LC23
	.long	8
	.long	45
	.quad	.LC24
	.quad	.LC25
	.long	9
	.long	45
	.quad	.LC26
	.quad	.LC27
	.long	10
	.long	45
	.quad	.LC28
	.quad	.LC29
	.long	11
	.long	45
	.quad	.LC30
	.quad	.LC31
	.long	17
	.long	45
	.quad	.LC32
	.quad	.LC33
	.long	18
	.long	45
	.quad	.LC34
	.quad	.LC35
	.long	12
	.long	115
	.quad	.LC36
	.quad	.LC37
	.long	13
	.long	115
	.quad	.LC38
	.quad	.LC39
	.long	14
	.long	115
	.quad	.LC40
	.quad	.LC41
	.long	15
	.long	115
	.quad	.LC42
	.quad	.LC43
	.long	16
	.long	45
	.quad	.LC44
	.quad	.LC45
	.long	4
	.long	115
	.quad	.LC46
	.quad	0
	.zero	16
	.section	.rodata.str1.1
.LC47:
	.string	"named_curve"
.LC48:
	.string	"explicit"
	.section	.data.rel.local,"aw"
	.align 32
	.type	param_enc, @object
	.size	param_enc, 48
param_enc:
	.quad	.LC47
	.long	1
	.zero	4
	.quad	.LC48
	.long	0
	.zero	4
	.quad	0
	.zero	8
	.section	.rodata.str1.1
.LC49:
	.string	"compressed"
.LC50:
	.string	"uncompressed"
.LC51:
	.string	"hybrid"
	.section	.data.rel.local
	.align 32
	.type	conv_forms, @object
	.size	conv_forms, 64
conv_forms:
	.quad	.LC49
	.long	2
	.zero	4
	.quad	.LC50
	.long	4
	.zero	4
	.quad	.LC51
	.long	6
	.zero	4
	.quad	0
	.zero	8
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
