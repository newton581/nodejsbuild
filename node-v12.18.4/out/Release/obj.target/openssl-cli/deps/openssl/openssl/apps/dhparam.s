	.file	"dhparam.c"
	.text
	.p2align 4
	.type	dh_cb, @function
dh_cb:
.LFB1436:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	$63, %eax
	cmpl	$3, %edi
	ja	.L2
	movslq	%edi, %rdi
	leaq	symbols.24596(%rip), %rax
	movzbl	(%rax,%rdi), %eax
.L2:
	movq	%r12, %rdi
	movb	%al, -25(%rbp)
	call	BN_GENCB_get_arg@PLT
	leaq	-25(%rbp), %rsi
	movl	$1, %edx
	movq	%rax, %rdi
	call	BIO_write@PLT
	movq	%r12, %rdi
	call	BN_GENCB_get_arg@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	movq	%rax, %rdi
	call	BIO_ctrl@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L8
	addq	$24, %rsp
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L8:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1436:
	.size	dh_cb, .-dh_cb
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"%s: Use -help for summary.\n"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"generator may not be chosen for DSA parameters\n"
	.align 8
.LC2:
	.string	"Generating DSA parameters, %d bit long prime\n"
	.align 8
.LC3:
	.string	"Generating DH parameters, %d bit long safe prime, generator %d\n"
	.align 8
.LC4:
	.string	"This is going to take a long time\n"
	.align 8
.LC5:
	.string	"unable to load DSA parameters\n"
	.section	.rodata.str1.1
.LC6:
	.string	"unable to load DH parameters\n"
	.section	.rodata.str1.8
	.align 8
.LC7:
	.string	"WARNING: p value is not prime\n"
	.align 8
.LC8:
	.string	"WARNING: p value is not a safe prime\n"
	.align 8
.LC9:
	.string	"WARNING: q value is not a prime\n"
	.section	.rodata.str1.1
.LC10:
	.string	"WARNING: q value is invalid\n"
.LC11:
	.string	"WARNING: j value is invalid\n"
	.section	.rodata.str1.8
	.align 8
.LC12:
	.string	"WARNING: unable to check the generator value\n"
	.align 8
.LC13:
	.string	"WARNING: the g value is not a generator\n"
	.align 8
.LC14:
	.string	"DH parameters appear to be ok.\n"
	.align 8
.LC15:
	.string	"ERROR: Invalid parameters generated\n"
	.section	.rodata.str1.1
.LC16:
	.string	"print a BN"
.LC17:
	.string	"static DH *get_dh%d(void)\n{\n"
.LC18:
	.string	"dhp"
.LC19:
	.string	"dhg"
	.section	.rodata.str1.8
	.align 8
.LC20:
	.string	"    DH *dh = DH_new();\n    BIGNUM *p, *g;\n\n    if (dh == NULL)\n        return NULL;\n"
	.align 8
.LC21:
	.string	"    p = BN_bin2bn(dhp_%d, sizeof(dhp_%d), NULL);\n"
	.align 8
.LC22:
	.string	"    g = BN_bin2bn(dhg_%d, sizeof(dhg_%d), NULL);\n"
	.align 8
.LC23:
	.string	"    if (p == NULL || g == NULL\n            || !DH_set0_pqg(dh, p, NULL, g)) {\n        DH_free(dh);\n        BN_free(p);\n        BN_free(g);\n        return NULL;\n    }\n"
	.align 8
.LC24:
	.string	"    if (!DH_set_length(dh, %ld)) {\n        DH_free(dh);\n        return NULL;\n    }\n"
	.section	.rodata.str1.1
.LC25:
	.string	"    return dh;\n}\n"
	.section	.rodata.str1.8
	.align 8
.LC26:
	.string	"../deps/openssl/openssl/apps/dhparam.c"
	.align 8
.LC27:
	.string	"unable to write DH parameters\n"
	.text
	.p2align 4
	.globl	dhparam_main
	.type	dhparam_main, @function
dhparam_main:
.LFB1435:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	dhparam_options(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	xorl	%r13d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	.L15(%rip), %rbx
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$0, -84(%rbp)
	movl	$32773, -80(%rbp)
	movl	$32773, -76(%rbp)
	call	opt_init@PLT
	movl	$0, -124(%rbp)
	movl	$0, -112(%rbp)
	movq	%rax, %r14
	movl	$0, -128(%rbp)
	movq	$0, -104(%rbp)
	movq	$0, -120(%rbp)
	movq	$0, -136(%rbp)
.L10:
	call	opt_next@PLT
	testl	%eax, %eax
	je	.L144
.L31:
	cmpl	$13, %eax
	jg	.L11
	cmpl	$-1, %eax
	jl	.L10
	addl	$1, %eax
	cmpl	$14, %eax
	ja	.L10
	movslq	(%rbx,%rax,4), %rax
	addq	%rbx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L15:
	.long	.L27-.L15
	.long	.L10-.L15
	.long	.L26-.L15
	.long	.L25-.L15
	.long	.L24-.L15
	.long	.L23-.L15
	.long	.L22-.L15
	.long	.L21-.L15
	.long	.L20-.L15
	.long	.L80-.L15
	.long	.L19-.L15
	.long	.L18-.L15
	.long	.L17-.L15
	.long	.L16-.L15
	.long	.L14-.L15
	.text
.L80:
	movl	$1, %r13d
	call	opt_next@PLT
	testl	%eax, %eax
	jne	.L31
.L144:
	call	opt_num_rest@PLT
	call	opt_rest@PLT
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L32
	leaq	-84(%rbp), %rsi
	call	opt_int@PLT
	testl	%eax, %eax
	je	.L33
	movl	-84(%rbp), %eax
	testl	%eax, %eax
	jle	.L33
	movl	-112(%rbp), %eax
	testl	%eax, %eax
	je	.L35
.L37:
	testl	%r15d, %r15d
	je	.L35
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	leaq	.LC1(%rip), %rsi
	call	BIO_printf@PLT
	xorl	%r9d, %r9d
	jmp	.L29
.L20:
	movl	$1, %r12d
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L11:
	leal	-1501(%rax), %edx
	cmpl	$1, %edx
	ja	.L10
	movl	%eax, %edi
	call	opt_rand@PLT
	testl	%eax, %eax
	jne	.L10
.L33:
	movl	$1, %r15d
	xorl	%r9d, %r9d
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L29
.L25:
	call	opt_arg@PLT
	leaq	-80(%rbp), %rdx
	movl	$2, %esi
	movq	%rax, %rdi
	call	opt_format@PLT
	testl	%eax, %eax
	jne	.L10
.L27:
	movq	bio_err(%rip), %rdi
	movq	%r14, %rdx
	xorl	%eax, %eax
	xorl	%r14d, %r14d
	leaq	.LC0(%rip), %rsi
	movl	$1, %r15d
	xorl	%ebx, %ebx
	call	BIO_printf@PLT
	xorl	%r9d, %r9d
.L29:
	movq	%rbx, %rdi
	movq	%r9, -112(%rbp)
	call	BIO_free@PLT
	movq	%r14, %rdi
	call	BIO_free_all@PLT
	movq	-112(%rbp), %r9
	movq	%r9, %rdi
	call	DH_free@PLT
	movq	-104(%rbp), %rdi
	call	release_engine@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L145
	leaq	-40(%rbp), %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L14:
	.cfi_restore_state
	movl	$5, -112(%rbp)
	jmp	.L10
.L16:
	movl	$2, -112(%rbp)
	jmp	.L10
.L17:
	movl	$1, -128(%rbp)
	jmp	.L10
.L18:
	movl	$1, %r15d
	jmp	.L10
.L19:
	movl	$1, -124(%rbp)
	jmp	.L10
.L21:
	call	opt_arg@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	setup_engine@PLT
	movq	%rax, -104(%rbp)
	jmp	.L10
.L22:
	call	opt_arg@PLT
	movq	%rax, -120(%rbp)
	jmp	.L10
.L23:
	call	opt_arg@PLT
	movq	%rax, -136(%rbp)
	jmp	.L10
.L24:
	call	opt_arg@PLT
	leaq	-76(%rbp), %rdx
	movl	$2, %esi
	movq	%rax, %rdi
	call	opt_format@PLT
	testl	%eax, %eax
	jne	.L10
	jmp	.L27
.L26:
	leaq	dhparam_options(%rip), %rdi
	xorl	%r15d, %r15d
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	call	opt_help@PLT
	xorl	%r9d, %r9d
	jmp	.L29
.L32:
	movl	-112(%rbp), %r14d
	testl	%r14d, %r14d
	je	.L35
	movl	-84(%rbp), %ebx
	testl	%ebx, %ebx
	jne	.L37
	movl	$2048, -84(%rbp)
	jmp	.L37
.L35:
	movl	-76(%rbp), %edx
	movq	-120(%rbp), %rdi
	movl	$119, %esi
	call	bio_open_default@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L33
	movl	-84(%rbp), %r11d
	testl	%r11d, %r11d
	je	.L38
	movl	-112(%rbp), %ecx
	movl	$2, %eax
	testl	%ecx, %ecx
	cmovne	%ecx, %eax
	movl	%eax, -112(%rbp)
	call	BN_GENCB_new@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L146
	movq	bio_err(%rip), %rdx
	leaq	dh_cb(%rip), %rsi
	movq	%rax, %rdi
	call	BN_GENCB_set@PLT
	testl	%r15d, %r15d
	je	.L41
	call	DSA_new@PLT
	movl	-84(%rbp), %edx
	movq	bio_err(%rip), %rdi
	leaq	.LC2(%rip), %rsi
	movq	%rax, -112(%rbp)
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	-112(%rbp), %r10
	testq	%r10, %r10
	je	.L43
	subq	$8, %rsp
	movl	-84(%rbp), %esi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	%rbx
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r10, %rdi
	movq	%r10, -112(%rbp)
	call	DSA_generate_parameters_ex@PLT
	popq	%r9
	popq	%r10
	testl	%eax, %eax
	movq	-112(%rbp), %r10
	je	.L43
	movq	%r10, %rdi
	movq	%r10, -120(%rbp)
	call	DSA_dup_DH@PLT
	movq	-120(%rbp), %r10
	movq	%rax, -112(%rbp)
	movq	%r10, %rdi
	call	DSA_free@PLT
	movq	-112(%rbp), %r9
	testq	%r9, %r9
	je	.L147
.L44:
	movq	%rbx, %rdi
	movq	%r9, -112(%rbp)
	xorl	%ebx, %ebx
	call	BN_GENCB_free@PLT
	movq	-112(%rbp), %r9
.L47:
	testl	%r13d, %r13d
	jne	.L148
.L56:
	testl	%r12d, %r12d
	jne	.L149
.L57:
	movl	-128(%rbp), %esi
	testl	%esi, %esi
	jne	.L150
.L67:
	movl	-124(%rbp), %ecx
	xorl	%r15d, %r15d
	testl	%ecx, %ecx
	jne	.L29
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	movq	%r9, %rdi
	movq	%r9, -112(%rbp)
	leaq	-64(%rbp), %rdx
	call	DH_get0_pqg@PLT
	cmpl	$4, -76(%rbp)
	movq	-112(%rbp), %r9
	je	.L151
	cmpq	$0, -64(%rbp)
	movq	%r9, -112(%rbp)
	movq	%r9, %rsi
	movq	%r14, %rdi
	je	.L72
	call	PEM_write_bio_DHxparams@PLT
	movq	-112(%rbp), %r9
	movl	%eax, -88(%rbp)
.L71:
	xorl	%r15d, %r15d
	testl	%eax, %eax
	jne	.L29
	movq	bio_err(%rip), %rdi
	movq	%r9, -112(%rbp)
	leaq	.LC27(%rip), %rsi
	movl	$1, %r15d
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	movq	-112(%rbp), %r9
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L38:
	movl	-80(%rbp), %edx
	movq	-136(%rbp), %rdi
	movl	$114, %esi
	call	bio_open_default@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L152
	movl	-80(%rbp), %eax
	xorl	%ecx, %ecx
	testl	%r15d, %r15d
	je	.L48
	cmpl	$4, %eax
	je	.L153
	movq	%rbx, %rdi
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	PEM_read_bio_DSAparams@PLT
	movq	%rax, %rdi
.L50:
	testq	%rdi, %rdi
	je	.L154
	movq	%rdi, -120(%rbp)
	call	DSA_dup_DH@PLT
	movq	-120(%rbp), %rdi
	movq	%rax, -112(%rbp)
	call	DSA_free@PLT
	movq	-112(%rbp), %r9
	testq	%r9, %r9
	jne	.L47
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	movq	-112(%rbp), %r9
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L41:
	call	DH_new@PLT
	movl	-112(%rbp), %ecx
	movl	-84(%rbp), %edx
	leaq	.LC3(%rip), %rsi
	movq	bio_err(%rip), %rdi
	movq	%rax, -120(%rbp)
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	leaq	.LC4(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	-120(%rbp), %r9
	testq	%r9, %r9
	je	.L46
	movl	-112(%rbp), %edx
	movl	-84(%rbp), %esi
	movq	%r9, %rdi
	movq	%rbx, %rcx
	movq	%r9, -112(%rbp)
	call	DH_generate_parameters_ex@PLT
	movq	-112(%rbp), %r9
	testl	%eax, %eax
	jne	.L44
.L46:
	movq	%rbx, %rdi
	movq	%r9, -112(%rbp)
	movl	$1, %r15d
	xorl	%ebx, %ebx
	call	BN_GENCB_free@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	movq	-112(%rbp), %r9
	jmp	.L29
.L48:
	cmpl	$4, %eax
	je	.L155
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	PEM_read_bio_DHparams@PLT
	movq	%rax, %r9
.L54:
	testq	%r9, %r9
	jne	.L47
.L55:
	movq	bio_err(%rip), %rdi
	leaq	.LC6(%rip), %rsi
	xorl	%eax, %eax
	movl	$1, %r15d
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	xorl	%r9d, %r9d
	jmp	.L29
.L149:
	movq	%r9, %rdi
	leaq	-88(%rbp), %rsi
	movq	%r9, -112(%rbp)
	call	DH_check@PLT
	movq	-112(%rbp), %r9
	testl	%eax, %eax
	je	.L156
	movl	-88(%rbp), %eax
	testb	$1, %al
	jne	.L157
.L59:
	testb	$2, %al
	jne	.L158
.L60:
	testb	$16, %al
	jne	.L159
.L61:
	testb	$32, %al
	jne	.L160
.L62:
	testb	$64, %al
	jne	.L161
.L63:
	testb	$4, %al
	jne	.L162
.L64:
	testb	$8, %al
	jne	.L163
.L65:
	testl	%eax, %eax
	je	.L164
	movl	-84(%rbp), %edx
	testl	%edx, %edx
	je	.L57
.L77:
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
	movq	%r9, -112(%rbp)
	movl	%r12d, %r15d
	leaq	.LC15(%rip), %rsi
	call	BIO_printf@PLT
	movq	-112(%rbp), %r9
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L148:
	movq	%r9, %rsi
	movq	%r14, %rdi
	movq	%r9, -112(%rbp)
	call	DHparams_print@PLT
	movq	-112(%rbp), %r9
	jmp	.L56
.L150:
	movq	%r9, %rdi
	movq	%r9, -112(%rbp)
	call	DH_size@PLT
	movq	-112(%rbp), %r9
	movl	%eax, %r12d
	movq	%r9, %rdi
	call	DH_bits@PLT
	movq	-112(%rbp), %r9
	leaq	-64(%rbp), %rcx
	xorl	%edx, %edx
	leaq	-72(%rbp), %rsi
	movl	%eax, %r13d
	movq	%r9, %rdi
	call	DH_get0_pqg@PLT
	movl	%r12d, %edi
	leaq	.LC16(%rip), %rsi
	call	app_malloc@PLT
	movl	%r13d, %edx
	leaq	.LC17(%rip), %rsi
	movq	%r14, %rdi
	movq	%rax, %r12
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	-72(%rbp), %rsi
	movq	%r12, %r8
	movl	%r13d, %ecx
	leaq	.LC18(%rip), %rdx
	movq	%r14, %rdi
	call	print_bignum_var@PLT
	movq	-64(%rbp), %rsi
	movq	%r12, %r8
	movl	%r13d, %ecx
	leaq	.LC19(%rip), %rdx
	movq	%r14, %rdi
	call	print_bignum_var@PLT
	leaq	.LC20(%rip), %rsi
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movl	%r13d, %ecx
	movl	%r13d, %edx
	movq	%r14, %rdi
	leaq	.LC21(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movl	%r13d, %ecx
	movl	%r13d, %edx
	movq	%r14, %rdi
	leaq	.LC22(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	leaq	.LC23(%rip), %rsi
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	-112(%rbp), %r9
	movq	%r9, %rdi
	call	DH_get_length@PLT
	movq	-112(%rbp), %r9
	testq	%rax, %rax
	jle	.L68
	movq	%r9, %rdi
	call	DH_get_length@PLT
	leaq	.LC24(%rip), %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	-112(%rbp), %r9
.L68:
	leaq	.LC25(%rip), %rsi
	movq	%r14, %rdi
	xorl	%eax, %eax
	movq	%r9, -112(%rbp)
	call	BIO_printf@PLT
	movl	$335, %edx
	leaq	.LC26(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
	movq	-112(%rbp), %r9
	jmp	.L67
.L43:
	movq	%r10, %rdi
	call	DSA_free@PLT
	movq	%rbx, %rdi
	xorl	%ebx, %ebx
	call	BN_GENCB_free@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	xorl	%r9d, %r9d
	jmp	.L29
.L146:
	movq	bio_err(%rip), %rdi
	movl	$1, %r15d
	call	ERR_print_errors@PLT
	xorl	%r9d, %r9d
	jmp	.L29
.L152:
	xorl	%r9d, %r9d
	movl	$1, %r15d
	jmp	.L29
.L155:
	movq	DH_new@GOTPCREL(%rip), %r15
	movq	d2i_DHparams@GOTPCREL(%rip), %rsi
	movq	%rbx, %rdx
	movq	%r15, %rdi
	call	ASN1_d2i_bio@PLT
	movq	%rax, %r9
	testq	%rax, %rax
	jne	.L47
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %esi
	movq	%rbx, %rdi
	call	BIO_ctrl@PLT
	testl	%eax, %eax
	jne	.L55
	movq	d2i_DHxparams@GOTPCREL(%rip), %rsi
	xorl	%ecx, %ecx
	movq	%rbx, %rdx
	movq	%r15, %rdi
	call	ASN1_d2i_bio@PLT
	movq	%rax, %r9
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L153:
	movq	DSA_new@GOTPCREL(%rip), %rdi
	movq	d2i_DSAparams@GOTPCREL(%rip), %rsi
	movq	%rbx, %rdx
	call	ASN1_d2i_bio@PLT
	movq	%rax, %rdi
	jmp	.L50
.L164:
	movq	bio_err(%rip), %rdi
	leaq	.LC14(%rip), %rsi
	movq	%r9, -112(%rbp)
	call	BIO_printf@PLT
	movl	-84(%rbp), %r8d
	movq	-112(%rbp), %r9
	testl	%r8d, %r8d
	je	.L57
	movl	-88(%rbp), %edi
	testl	%edi, %edi
	je	.L57
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L151:
	cmpq	$0, -64(%rbp)
	movq	%r9, %rdx
	je	.L70
	movq	i2d_DHxparams@GOTPCREL(%rip), %rdi
	movq	%r14, %rsi
	call	ASN1_i2d_bio@PLT
	movq	-112(%rbp), %r9
	movl	%eax, -88(%rbp)
	jmp	.L71
.L163:
	movq	bio_err(%rip), %rdi
	leaq	.LC13(%rip), %rsi
	xorl	%eax, %eax
	movq	%r9, -112(%rbp)
	call	BIO_printf@PLT
	movl	-88(%rbp), %eax
	movq	-112(%rbp), %r9
	jmp	.L65
.L159:
	movq	bio_err(%rip), %rdi
	leaq	.LC9(%rip), %rsi
	xorl	%eax, %eax
	movq	%r9, -112(%rbp)
	call	BIO_printf@PLT
	movl	-88(%rbp), %eax
	movq	-112(%rbp), %r9
	jmp	.L61
.L158:
	movq	bio_err(%rip), %rdi
	leaq	.LC8(%rip), %rsi
	xorl	%eax, %eax
	movq	%r9, -112(%rbp)
	call	BIO_printf@PLT
	movl	-88(%rbp), %eax
	movq	-112(%rbp), %r9
	jmp	.L60
.L157:
	movq	bio_err(%rip), %rdi
	leaq	.LC7(%rip), %rsi
	xorl	%eax, %eax
	movq	%r9, -112(%rbp)
	call	BIO_printf@PLT
	movl	-88(%rbp), %eax
	movq	-112(%rbp), %r9
	jmp	.L59
.L161:
	movq	bio_err(%rip), %rdi
	leaq	.LC11(%rip), %rsi
	xorl	%eax, %eax
	movq	%r9, -112(%rbp)
	call	BIO_printf@PLT
	movl	-88(%rbp), %eax
	movq	-112(%rbp), %r9
	jmp	.L63
.L160:
	movq	bio_err(%rip), %rdi
	leaq	.LC10(%rip), %rsi
	xorl	%eax, %eax
	movq	%r9, -112(%rbp)
	call	BIO_printf@PLT
	movl	-88(%rbp), %eax
	movq	-112(%rbp), %r9
	jmp	.L62
.L162:
	movq	bio_err(%rip), %rdi
	leaq	.LC12(%rip), %rsi
	xorl	%eax, %eax
	movq	%r9, -112(%rbp)
	call	BIO_printf@PLT
	movl	-88(%rbp), %eax
	movq	-112(%rbp), %r9
	jmp	.L64
.L156:
	movq	bio_err(%rip), %rdi
	movl	%r12d, %r15d
	call	ERR_print_errors@PLT
	movq	-112(%rbp), %r9
	jmp	.L29
.L154:
	movq	bio_err(%rip), %rdi
	leaq	.LC5(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	xorl	%r9d, %r9d
	jmp	.L29
.L72:
	call	PEM_write_bio_DHparams@PLT
	movq	-112(%rbp), %r9
	movl	%eax, -88(%rbp)
	jmp	.L71
.L145:
	call	__stack_chk_fail@PLT
.L70:
	movq	i2d_DHparams@GOTPCREL(%rip), %rdi
	movq	%r14, %rsi
	movq	%r9, -112(%rbp)
	call	ASN1_i2d_bio@PLT
	movq	-112(%rbp), %r9
	movl	%eax, -88(%rbp)
	jmp	.L71
.L147:
	movq	%rbx, %rdi
	xorl	%ebx, %ebx
	call	BN_GENCB_free@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	movq	-112(%rbp), %r9
	jmp	.L29
	.cfi_endproc
.LFE1435:
	.size	dhparam_main, .-dhparam_main
	.section	.rodata
	.type	symbols.24596, @object
	.size	symbols.24596, 5
symbols.24596:
	.string	".+*\n"
	.globl	dhparam_options
	.section	.rodata.str1.1
.LC28:
	.string	"Usage: %s [flags] [numbits]\n"
.LC29:
	.string	"Valid options are:\n"
.LC30:
	.string	"help"
.LC31:
	.string	"Display this summary"
.LC32:
	.string	"in"
.LC33:
	.string	"Input file"
.LC34:
	.string	"inform"
.LC35:
	.string	"Input format, DER or PEM"
.LC36:
	.string	"outform"
.LC37:
	.string	"Output format, DER or PEM"
.LC38:
	.string	"out"
.LC39:
	.string	"Output file"
.LC40:
	.string	"check"
.LC41:
	.string	"Check the DH parameters"
.LC42:
	.string	"text"
	.section	.rodata.str1.8
	.align 8
.LC43:
	.string	"Print a text form of the DH parameters"
	.section	.rodata.str1.1
.LC44:
	.string	"noout"
	.section	.rodata.str1.8
	.align 8
.LC45:
	.string	"Don't output any DH parameters"
	.section	.rodata.str1.1
.LC46:
	.string	"rand"
	.section	.rodata.str1.8
	.align 8
.LC47:
	.string	"Load the file(s) into the random number generator"
	.section	.rodata.str1.1
.LC48:
	.string	"writerand"
	.section	.rodata.str1.8
	.align 8
.LC49:
	.string	"Write random data to the specified file"
	.section	.rodata.str1.1
.LC50:
	.string	"C"
.LC51:
	.string	"Print C code"
.LC52:
	.string	"2"
	.section	.rodata.str1.8
	.align 8
.LC53:
	.string	"Generate parameters using 2 as the generator value"
	.section	.rodata.str1.1
.LC54:
	.string	"5"
	.section	.rodata.str1.8
	.align 8
.LC55:
	.string	"Generate parameters using 5 as the generator value"
	.section	.rodata.str1.1
.LC56:
	.string	"dsaparam"
	.section	.rodata.str1.8
	.align 8
.LC57:
	.string	"Read or generate DSA parameters, convert to DH"
	.section	.rodata.str1.1
.LC58:
	.string	"engine"
	.section	.rodata.str1.8
	.align 8
.LC59:
	.string	"Use engine e, possibly a hardware device"
	.section	.data.rel.ro,"aw"
	.align 32
	.type	dhparam_options, @object
	.size	dhparam_options, 432
dhparam_options:
	.quad	OPT_HELP_STR
	.long	1
	.long	45
	.quad	.LC28
	.quad	OPT_HELP_STR
	.long	1
	.long	45
	.quad	.LC29
	.quad	.LC30
	.long	1
	.long	45
	.quad	.LC31
	.quad	.LC32
	.long	4
	.long	60
	.quad	.LC33
	.quad	.LC34
	.long	2
	.long	70
	.quad	.LC35
	.quad	.LC36
	.long	3
	.long	70
	.quad	.LC37
	.quad	.LC38
	.long	5
	.long	62
	.quad	.LC39
	.quad	.LC40
	.long	7
	.long	45
	.quad	.LC41
	.quad	.LC42
	.long	8
	.long	45
	.quad	.LC43
	.quad	.LC44
	.long	9
	.long	45
	.quad	.LC45
	.quad	.LC46
	.long	1501
	.long	115
	.quad	.LC47
	.quad	.LC48
	.long	1502
	.long	62
	.quad	.LC49
	.quad	.LC50
	.long	11
	.long	45
	.quad	.LC51
	.quad	.LC52
	.long	12
	.long	45
	.quad	.LC53
	.quad	.LC54
	.long	13
	.long	45
	.quad	.LC55
	.quad	.LC56
	.long	10
	.long	45
	.quad	.LC57
	.quad	.LC58
	.long	6
	.long	115
	.quad	.LC59
	.quad	0
	.zero	16
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
