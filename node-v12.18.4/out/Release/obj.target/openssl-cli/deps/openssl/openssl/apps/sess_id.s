	.file	"sess_id.c"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"%s: Use -help for summary.\n"
.LC1:
	.string	"unable to load SSL_SESSION\n"
.LC2:
	.string	"Context too long\n"
.LC3:
	.string	"Error setting id context\n"
.LC4:
	.string	"No certificate present\n"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC5:
	.string	"bad output format specified for outfile\n"
	.section	.rodata.str1.1
.LC6:
	.string	"unable to write SSL_SESSION\n"
.LC7:
	.string	"unable to write X509\n"
	.text
	.p2align 4
	.globl	sess_id_main
	.type	sess_id_main, @function
sess_id_main:
.LFB1555:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	sess_id_options(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	xorl	%r14d, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	.L5(%rip), %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$32773, -64(%rbp)
	movl	$32773, -60(%rbp)
	call	opt_init@PLT
	movl	$0, -84(%rbp)
	movq	$0, -72(%rbp)
	movq	%rax, %r13
	movq	$0, -96(%rbp)
	movq	$0, -80(%rbp)
.L2:
	call	opt_next@PLT
	testl	%eax, %eax
	je	.L61
.L17:
	addl	$1, %eax
	cmpl	$10, %eax
	ja	.L2
	movslq	(%rbx,%rax,4), %rax
	addq	%rbx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L5:
	.long	.L14-.L5
	.long	.L2-.L5
	.long	.L13-.L5
	.long	.L12-.L5
	.long	.L11-.L5
	.long	.L10-.L5
	.long	.L9-.L5
	.long	.L8-.L5
	.long	.L7-.L5
	.long	.L6-.L5
	.long	.L4-.L5
	.text
.L12:
	call	opt_arg@PLT
	leaq	-64(%rbp), %rdx
	movl	$2, %esi
	movq	%rax, %rdi
	call	opt_format@PLT
	testl	%eax, %eax
	jne	.L2
.L14:
	movq	bio_err(%rip), %rdi
	movq	%r13, %rdx
	xorl	%eax, %eax
	xorl	%r13d, %r13d
	leaq	.LC0(%rip), %rsi
	movl	$1, %r12d
	xorl	%ebx, %ebx
	call	BIO_printf@PLT
.L15:
	movq	%r13, %rdi
	call	BIO_free_all@PLT
	movq	%rbx, %rdi
	call	SSL_SESSION_free@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L62
	addq	$56, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L4:
	.cfi_restore_state
	call	opt_arg@PLT
	movq	%rax, -72(%rbp)
	call	opt_next@PLT
	testl	%eax, %eax
	jne	.L17
	.p2align 4,,10
	.p2align 3
.L61:
	call	opt_num_rest@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	jne	.L14
	movl	-64(%rbp), %ebx
	movq	-80(%rbp), %rdi
	movl	$114, %esi
	movl	%ebx, %edx
	call	bio_open_default@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L63
	xorl	%ecx, %ecx
	cmpl	$4, %ebx
	je	.L64
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	PEM_read_bio_SSL_SESSION@PLT
	movq	%rax, %rbx
.L20:
	testq	%rbx, %rbx
	je	.L65
.L21:
	endbr64
	movq	%r13, %rdi
	call	BIO_free@PLT
	movq	%rbx, %rdi
	call	SSL_SESSION_get0_peer@PLT
	movq	%rax, -80(%rbp)
	movq	-72(%rbp), %rax
	testq	%rax, %rax
	je	.L22
	movq	%rax, %rdi
	call	strlen@PLT
	cmpq	$32, %rax
	ja	.L66
	movq	-72(%rbp), %rsi
	movl	%eax, %edx
	movq	%rbx, %rdi
	call	SSL_SESSION_set1_id_context@PLT
	testl	%eax, %eax
	je	.L67
.L22:
	testl	%r14d, %r14d
	sete	%cl
	testl	%r15d, %r15d
	jne	.L39
	xorl	%r13d, %r13d
	testb	%cl, %cl
	je	.L15
.L39:
	movl	-60(%rbp), %edx
	movq	-96(%rbp), %rdi
	movl	$119, %esi
	movb	%cl, -72(%rbp)
	call	bio_open_default@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L37
	testl	%r15d, %r15d
	movzbl	-72(%rbp), %ecx
	je	.L25
	movq	%rbx, %rsi
	movq	%rax, %rdi
	call	SSL_SESSION_print@PLT
	movl	-84(%rbp), %eax
	movzbl	-72(%rbp), %ecx
	testl	%eax, %eax
	je	.L25
	cmpq	$0, -80(%rbp)
	je	.L68
	movq	-80(%rbp), %rsi
	movq	%r13, %rdi
	movb	%cl, -72(%rbp)
	call	X509_print@PLT
	movzbl	-72(%rbp), %ecx
.L27:
	cmpq	$0, -80(%rbp)
	je	.L15
	testb	%cl, %cl
	je	.L15
	movl	-60(%rbp), %eax
	cmpl	$4, %eax
	je	.L69
	cmpl	$32773, %eax
	jne	.L34
	movq	-80(%rbp), %rsi
	movq	%r13, %rdi
	call	PEM_write_bio_X509@PLT
.L33:
	testl	%eax, %eax
	jne	.L15
	movq	bio_err(%rip), %rdi
	leaq	.LC7(%rip), %rsi
	movl	$1, %r12d
	call	BIO_printf@PLT
	jmp	.L15
.L6:
	leal	1(%r12), %r14d
	movl	%r14d, %r12d
	jmp	.L2
.L7:
	leal	1(%r12), %eax
	movl	%eax, -84(%rbp)
	movl	%eax, %r12d
	jmp	.L2
.L8:
	leal	1(%r12), %r15d
	movl	%r15d, %r12d
	jmp	.L2
.L9:
	call	opt_arg@PLT
	movq	%rax, -96(%rbp)
	jmp	.L2
.L10:
	call	opt_arg@PLT
	movq	%rax, -80(%rbp)
	jmp	.L2
.L11:
	call	opt_arg@PLT
	leaq	-60(%rbp), %rdx
	movl	$130, %esi
	movq	%rax, %rdi
	call	opt_format@PLT
	testl	%eax, %eax
	jne	.L2
	jmp	.L14
.L13:
	leaq	sess_id_options(%rip), %rdi
	xorl	%r12d, %r12d
	xorl	%r13d, %r13d
	xorl	%ebx, %ebx
	call	opt_help@PLT
	jmp	.L15
.L63:
	xorl	%edi, %edi
	xorl	%ebx, %ebx
	movl	$1, %r12d
	call	BIO_free@PLT
	jmp	.L15
.L64:
	movq	d2i_SSL_SESSION@GOTPCREL(%rip), %rsi
	movq	SSL_SESSION_new@GOTPCREL(%rip), %rdi
	movq	%rax, %rdx
	call	ASN1_d2i_bio@PLT
	movq	%rax, %rbx
	jmp	.L20
.L25:
	movl	-84(%rbp), %eax
	orl	%r14d, %eax
	jne	.L27
	movl	-60(%rbp), %eax
	cmpl	$4, %eax
	je	.L70
	cmpl	$32773, %eax
	je	.L71
	cmpl	$14, %eax
	jne	.L34
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	SSL_SESSION_print_keylog@PLT
.L29:
	xorl	%r12d, %r12d
	testl	%eax, %eax
	jne	.L15
	movq	bio_err(%rip), %rdi
	leaq	.LC6(%rip), %rsi
	movl	$1, %r12d
	call	BIO_printf@PLT
	jmp	.L15
.L66:
	movq	bio_err(%rip), %rdi
	leaq	.LC2(%rip), %rsi
	xorl	%eax, %eax
	xorl	%r13d, %r13d
	movl	$1, %r12d
	call	BIO_printf@PLT
	jmp	.L15
.L34:
	movq	bio_err(%rip), %rdi
	leaq	.LC5(%rip), %rsi
	xorl	%eax, %eax
	movl	$1, %r12d
	call	BIO_printf@PLT
	jmp	.L15
.L67:
	movq	bio_err(%rip), %rdi
	leaq	.LC3(%rip), %rsi
	movl	$1, %r12d
	xorl	%r13d, %r13d
	call	BIO_printf@PLT
	jmp	.L15
.L70:
	movq	i2d_SSL_SESSION@GOTPCREL(%rip), %rdi
	movq	%rbx, %rdx
	movq	%r13, %rsi
	call	ASN1_i2d_bio@PLT
	jmp	.L29
.L71:
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	PEM_write_bio_SSL_SESSION@PLT
	jmp	.L29
.L65:
	movq	bio_err(%rip), %rdi
	leaq	.LC1(%rip), %rsi
	xorl	%eax, %eax
	movl	$1, %r12d
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	movq	%r13, %rdi
	xorl	%r13d, %r13d
	call	BIO_free@PLT
	jmp	.L15
.L37:
	movl	$1, %r12d
	jmp	.L15
.L69:
	movq	-80(%rbp), %rsi
	movq	%r13, %rdi
	call	i2d_X509_bio@PLT
	jmp	.L33
.L68:
	leaq	.LC4(%rip), %rsi
	movq	%r13, %rdi
	call	BIO_puts@PLT
	jmp	.L15
.L62:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1555:
	.size	sess_id_main, .-sess_id_main
	.globl	sess_id_options
	.section	.rodata.str1.1
.LC8:
	.string	"help"
.LC9:
	.string	"Display this summary"
.LC10:
	.string	"inform"
	.section	.rodata.str1.8
	.align 8
.LC11:
	.string	"Input format - default PEM (DER or PEM)"
	.section	.rodata.str1.1
.LC12:
	.string	"outform"
	.section	.rodata.str1.8
	.align 8
.LC13:
	.string	"Output format - default PEM (PEM, DER or NSS)"
	.section	.rodata.str1.1
.LC14:
	.string	"in"
.LC15:
	.string	"Input file - default stdin"
.LC16:
	.string	"out"
.LC17:
	.string	"Output file - default stdout"
.LC18:
	.string	"text"
.LC19:
	.string	"Print ssl session id details"
.LC20:
	.string	"cert"
.LC21:
	.string	"Output certificate "
.LC22:
	.string	"noout"
	.section	.rodata.str1.8
	.align 8
.LC23:
	.string	"Don't output the encoded session info"
	.section	.rodata.str1.1
.LC24:
	.string	"context"
.LC25:
	.string	"Set the session ID context"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	sess_id_options, @object
	.size	sess_id_options, 240
sess_id_options:
	.quad	.LC8
	.long	1
	.long	45
	.quad	.LC9
	.quad	.LC10
	.long	2
	.long	70
	.quad	.LC11
	.quad	.LC12
	.long	3
	.long	102
	.quad	.LC13
	.quad	.LC14
	.long	4
	.long	115
	.quad	.LC15
	.quad	.LC16
	.long	5
	.long	62
	.quad	.LC17
	.quad	.LC18
	.long	6
	.long	45
	.quad	.LC19
	.quad	.LC20
	.long	7
	.long	45
	.quad	.LC21
	.quad	.LC22
	.long	8
	.long	45
	.quad	.LC23
	.quad	.LC24
	.long	9
	.long	115
	.quad	.LC25
	.quad	0
	.zero	16
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
