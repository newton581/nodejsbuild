	.file	"pkey.c"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"%s: Use -help for summary.\n"
.LC1:
	.string	"Error getting passwords\n"
.LC2:
	.string	"Public Key"
.LC3:
	.string	"key"
.LC4:
	.string	"Key is valid\n"
.LC5:
	.string	"Key is invalid\n"
.LC6:
	.string	"Detailed error: %s\n"
.LC7:
	.string	"Bad format specified for key\n"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC8:
	.string	"../deps/openssl/openssl/apps/pkey.c"
	.text
	.p2align 4
	.globl	pkey_main
	.type	pkey_main, @function
pkey_main:
.LFB1435:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	pkey_options(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	xorl	%r14d, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	leaq	.L5(%rip), %r13
	pushq	%r12
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	xorl	%ebx, %ebx
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movq	$0, -72(%rbp)
	movq	$0, -64(%rbp)
	movl	$32773, -88(%rbp)
	movl	$32773, -84(%rbp)
	call	opt_init@PLT
	movl	$0, -112(%rbp)
	movq	%rax, -136(%rbp)
	movl	$0, -156(%rbp)
	movl	$0, -160(%rbp)
	movl	$0, -108(%rbp)
	movq	$0, -128(%rbp)
	movq	$0, -120(%rbp)
	movq	$0, -144(%rbp)
	movq	$0, -152(%rbp)
	movq	$0, -104(%rbp)
.L2:
	call	opt_next@PLT
	testl	%eax, %eax
	je	.L81
.L26:
	addl	$1, %eax
	cmpl	$18, %eax
	ja	.L2
	movslq	0(%r13,%rax,4), %rax
	addq	%r13, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L5:
	.long	.L22-.L5
	.long	.L2-.L5
	.long	.L21-.L5
	.long	.L20-.L5
	.long	.L19-.L5
	.long	.L18-.L5
	.long	.L17-.L5
	.long	.L16-.L5
	.long	.L15-.L5
	.long	.L14-.L5
	.long	.L13-.L5
	.long	.L12-.L5
	.long	.L11-.L5
	.long	.L10-.L5
	.long	.L9-.L5
	.long	.L8-.L5
	.long	.L7-.L5
	.long	.L6-.L5
	.long	.L4-.L5
	.text
.L13:
	movl	$1, %ebx
	movl	$1, %r12d
	movl	$1, %r14d
	call	opt_next@PLT
	testl	%eax, %eax
	jne	.L26
.L81:
	call	opt_num_rest@PLT
	testl	%eax, %eax
	jne	.L22
	movl	-108(%rbp), %eax
	movl	%ebx, %r13d
	movq	-128(%rbp), %rsi
	leaq	-64(%rbp), %rcx
	xorl	$1, %r13d
	movq	-120(%rbp), %rdi
	leaq	-72(%rbp), %rdx
	orl	%r12d, %eax
	xorl	$1, %eax
	andl	%r15d, %r13d
	cmove	%eax, %r13d
	call	app_passwd@PLT
	testl	%eax, %eax
	je	.L82
	movl	-84(%rbp), %esi
	movq	-144(%rbp), %rdi
	movl	%r13d, %edx
	call	bio_open_owner@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L47
	movq	-72(%rbp), %rcx
	movl	-88(%rbp), %esi
	testl	%r14d, %r14d
	jne	.L83
	movq	-104(%rbp), %r8
	movq	-152(%rbp), %rdi
	movl	$1, %edx
	leaq	.LC3(%rip), %r9
	call	load_key@PLT
	movq	%rax, %r14
.L30:
	testq	%r14, %r14
	je	.L23
	movl	-156(%rbp), %eax
	orl	-112(%rbp), %eax
	jne	.L84
.L31:
	movl	-108(%rbp), %r9d
	testl	%r9d, %r9d
	jne	.L39
	movl	-84(%rbp), %eax
	cmpl	$32773, %eax
	je	.L85
	cmpl	$4, %eax
	jne	.L43
	movq	%r14, %rsi
	movq	%r13, %rdi
	testl	%r12d, %r12d
	je	.L44
	call	i2d_PUBKEY_bio@PLT
	testl	%eax, %eax
	je	.L23
.L39:
	testl	%r15d, %r15d
	je	.L24
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	testl	%ebx, %ebx
	je	.L45
	call	EVP_PKEY_print_public@PLT
	xorl	%r15d, %r15d
	testl	%eax, %eax
	jg	.L24
	jmp	.L23
.L20:
	call	opt_arg@PLT
	leaq	-88(%rbp), %rdx
	movl	$1982, %esi
	movq	%rax, %rdi
	call	opt_format@PLT
	testl	%eax, %eax
	jne	.L2
.L22:
	movq	-136(%rbp), %rdx
	xorl	%eax, %eax
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	movq	bio_err(%rip), %rdi
	leaq	.LC0(%rip), %rsi
	call	BIO_printf@PLT
.L23:
	movq	bio_err(%rip), %rdi
	movl	$1, %r15d
	call	ERR_print_errors@PLT
.L24:
	movq	%r14, %rdi
	call	EVP_PKEY_free@PLT
	movq	-104(%rbp), %rdi
	call	release_engine@PLT
	movq	%r13, %rdi
	call	BIO_free_all@PLT
	xorl	%edi, %edi
	call	BIO_free@PLT
	movq	-72(%rbp), %rdi
	movl	$239, %edx
	leaq	.LC8(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-64(%rbp), %rdi
	movl	$240, %edx
	leaq	.LC8(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L86
	leaq	-40(%rbp), %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L4:
	.cfi_restore_state
	movl	$1, -156(%rbp)
	jmp	.L2
.L6:
	movl	$1, -112(%rbp)
	jmp	.L2
.L7:
	movl	$1, -160(%rbp)
	jmp	.L2
.L8:
	call	opt_unknown@PLT
	leaq	-80(%rbp), %rsi
	movq	%rax, %rdi
	call	opt_cipher@PLT
	testl	%eax, %eax
	jne	.L2
	jmp	.L22
.L9:
	movl	$1, -108(%rbp)
	jmp	.L2
.L10:
	movl	$1, %r15d
	jmp	.L2
.L11:
	movl	$1, %r15d
	movl	$1, %ebx
	jmp	.L2
.L12:
	movl	$1, %r12d
	jmp	.L2
.L14:
	call	opt_arg@PLT
	movq	%rax, -144(%rbp)
	jmp	.L2
.L15:
	call	opt_arg@PLT
	movq	%rax, -152(%rbp)
	jmp	.L2
.L16:
	call	opt_arg@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	setup_engine@PLT
	movq	%rax, -104(%rbp)
	jmp	.L2
.L17:
	call	opt_arg@PLT
	movq	%rax, -128(%rbp)
	jmp	.L2
.L18:
	call	opt_arg@PLT
	movq	%rax, -120(%rbp)
	jmp	.L2
.L19:
	call	opt_arg@PLT
	leaq	-84(%rbp), %rdx
	movl	$2, %esi
	movq	%rax, %rdi
	call	opt_format@PLT
	testl	%eax, %eax
	jne	.L2
	jmp	.L22
.L21:
	leaq	pkey_options(%rip), %rdi
	xorl	%r15d, %r15d
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	call	opt_help@PLT
	jmp	.L24
.L83:
	movq	-104(%rbp), %r8
	movq	-152(%rbp), %rdi
	movl	$1, %edx
	leaq	.LC2(%rip), %r9
	call	load_pubkey@PLT
	movq	%rax, %r14
	jmp	.L30
.L82:
	movq	bio_err(%rip), %rdi
	leaq	.LC1(%rip), %rsi
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	call	BIO_printf@PLT
	jmp	.L23
.L84:
	movq	-104(%rbp), %rsi
	movq	%r14, %rdi
	call	EVP_PKEY_CTX_new@PLT
	movq	%rax, -120(%rbp)
	testq	%rax, %rax
	je	.L87
	movl	-112(%rbp), %r10d
	movq	%rax, %rdi
	testl	%r10d, %r10d
	je	.L33
	call	EVP_PKEY_check@PLT
.L34:
	cmpl	$1, %eax
	je	.L88
	leaq	.LC5(%rip), %rsi
	movq	%r13, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L37
.L38:
	call	ERR_reason_error_string@PLT
	leaq	.LC6(%rip), %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	call	ERR_get_error@PLT
.L37:
	call	ERR_peek_error@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L38
.L36:
	movq	-120(%rbp), %rdi
	call	EVP_PKEY_CTX_free@PLT
	jmp	.L31
.L47:
	xorl	%r14d, %r14d
	jmp	.L23
.L45:
	call	EVP_PKEY_print_private@PLT
	xorl	%r15d, %r15d
	testl	%eax, %eax
	jg	.L24
	jmp	.L23
.L85:
	testl	%r12d, %r12d
	jne	.L89
	movl	-160(%rbp), %r8d
	movq	-64(%rbp), %rax
	movq	-80(%rbp), %rdx
	testl	%r8d, %r8d
	je	.L42
	subq	$8, %rsp
	movq	%r14, %rsi
	movq	%r13, %rdi
	xorl	%r9d, %r9d
	pushq	%rax
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	call	PEM_write_bio_PrivateKey_traditional@PLT
	popq	%rsi
	popq	%rdi
	testl	%eax, %eax
	jne	.L39
	jmp	.L23
.L33:
	call	EVP_PKEY_public_check@PLT
	jmp	.L34
.L88:
	leaq	.LC4(%rip), %rsi
	movq	%r13, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L36
.L43:
	movq	bio_err(%rip), %rdi
	leaq	.LC7(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L23
.L44:
	call	i2d_PrivateKey_bio@PLT
	testl	%eax, %eax
	jne	.L39
	jmp	.L23
.L42:
	subq	$8, %rsp
	xorl	%ecx, %ecx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	%rax
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	PEM_write_bio_PrivateKey@PLT
	popq	%rdx
	popq	%rcx
	testl	%eax, %eax
	jne	.L39
	jmp	.L23
.L89:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	PEM_write_bio_PUBKEY@PLT
	testl	%eax, %eax
	jne	.L39
	jmp	.L23
.L87:
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	jmp	.L23
.L86:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1435:
	.size	pkey_main, .-pkey_main
	.globl	pkey_options
	.section	.rodata.str1.1
.LC9:
	.string	"help"
.LC10:
	.string	"Display this summary"
.LC11:
	.string	"inform"
.LC12:
	.string	"Input format (DER or PEM)"
.LC13:
	.string	"outform"
.LC14:
	.string	"Output format (DER or PEM)"
.LC15:
	.string	"passin"
.LC16:
	.string	"Input file pass phrase source"
.LC17:
	.string	"passout"
	.section	.rodata.str1.8
	.align 8
.LC18:
	.string	"Output file pass phrase source"
	.section	.rodata.str1.1
.LC19:
	.string	"in"
.LC20:
	.string	"Input key"
.LC21:
	.string	"out"
.LC22:
	.string	"Output file"
.LC23:
	.string	"pubin"
	.section	.rodata.str1.8
	.align 8
.LC24:
	.string	"Read public key from input (default is private key)"
	.section	.rodata.str1.1
.LC25:
	.string	"pubout"
	.section	.rodata.str1.8
	.align 8
.LC26:
	.string	"Output public key, not private"
	.section	.rodata.str1.1
.LC27:
	.string	"text_pub"
	.section	.rodata.str1.8
	.align 8
.LC28:
	.string	"Only output public key components"
	.section	.rodata.str1.1
.LC29:
	.string	"text"
.LC30:
	.string	"Output in plaintext as well"
.LC31:
	.string	"noout"
.LC32:
	.string	"Don't output the key"
.LC33:
	.string	""
.LC34:
	.string	"Any supported cipher"
.LC35:
	.string	"traditional"
	.section	.rodata.str1.8
	.align 8
.LC36:
	.string	"Use traditional format for private keys"
	.section	.rodata.str1.1
.LC37:
	.string	"engine"
	.section	.rodata.str1.8
	.align 8
.LC38:
	.string	"Use engine, possibly a hardware device"
	.section	.rodata.str1.1
.LC39:
	.string	"check"
.LC40:
	.string	"Check key consistency"
.LC41:
	.string	"pubcheck"
.LC42:
	.string	"Check public key consistency"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	pkey_options, @object
	.size	pkey_options, 432
pkey_options:
	.quad	.LC9
	.long	1
	.long	45
	.quad	.LC10
	.quad	.LC11
	.long	2
	.long	102
	.quad	.LC12
	.quad	.LC13
	.long	3
	.long	70
	.quad	.LC14
	.quad	.LC15
	.long	4
	.long	115
	.quad	.LC16
	.quad	.LC17
	.long	5
	.long	115
	.quad	.LC18
	.quad	.LC19
	.long	7
	.long	115
	.quad	.LC20
	.quad	.LC21
	.long	8
	.long	62
	.quad	.LC22
	.quad	.LC23
	.long	9
	.long	45
	.quad	.LC24
	.quad	.LC25
	.long	10
	.long	45
	.quad	.LC26
	.quad	.LC27
	.long	11
	.long	45
	.quad	.LC28
	.quad	.LC29
	.long	12
	.long	45
	.quad	.LC30
	.quad	.LC31
	.long	13
	.long	45
	.quad	.LC32
	.quad	.LC33
	.long	14
	.long	45
	.quad	.LC34
	.quad	.LC35
	.long	15
	.long	45
	.quad	.LC36
	.quad	.LC37
	.long	6
	.long	115
	.quad	.LC38
	.quad	.LC39
	.long	16
	.long	45
	.quad	.LC40
	.quad	.LC41
	.long	17
	.long	45
	.quad	.LC42
	.quad	0
	.zero	16
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
