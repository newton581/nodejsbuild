	.file	"dsa.c"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"%s: Use -help for summary.\n"
.LC1:
	.string	"Error getting passwords\n"
.LC2:
	.string	"read DSA key\n"
.LC3:
	.string	"Public Key"
.LC4:
	.string	"Private Key"
.LC5:
	.string	"unable to load Key\n"
.LC6:
	.string	"Public Key="
.LC7:
	.string	"\n"
.LC8:
	.string	"writing DSA key\n"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC9:
	.string	"PVK form impossible with public key input\n"
	.align 8
.LC10:
	.string	"bad output format specified for outfile\n"
	.section	.rodata.str1.1
.LC11:
	.string	"unable to write private key\n"
	.section	.rodata.str1.8
	.align 8
.LC12:
	.string	"../deps/openssl/openssl/apps/dsa.c"
	.text
	.p2align 4
	.globl	dsa_main
	.type	dsa_main, @function
dsa_main:
.LFB1435:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	dsa_options(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	xorl	%r14d, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	.L5(%rip), %rbx
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -88(%rbp)
	movq	$0, -80(%rbp)
	movq	$0, -72(%rbp)
	movl	$32773, -96(%rbp)
	movl	$32773, -92(%rbp)
	call	opt_init@PLT
	movl	$0, -100(%rbp)
	movl	$2, -144(%rbp)
	movq	%rax, %r13
	movl	$0, -140(%rbp)
	movl	$0, -104(%rbp)
	movq	$0, -120(%rbp)
	movq	$0, -112(%rbp)
	movq	$0, -136(%rbp)
	movq	$0, -128(%rbp)
.L2:
	call	opt_next@PLT
	testl	%eax, %eax
	je	.L73
.L23:
	leal	1(%rax), %edx
	cmpl	$18, %edx
	ja	.L2
	movslq	(%rbx,%rdx,4), %rdx
	addq	%rbx, %rdx
	notrack jmp	*%rdx
	.section	.rodata
	.align 4
	.align 4
.L5:
	.long	.L20-.L5
	.long	.L2-.L5
	.long	.L19-.L5
	.long	.L18-.L5
	.long	.L17-.L5
	.long	.L16-.L5
	.long	.L15-.L5
	.long	.L14-.L5
	.long	.L13-.L5
	.long	.L13-.L5
	.long	.L13-.L5
	.long	.L12-.L5
	.long	.L11-.L5
	.long	.L10-.L5
	.long	.L9-.L5
	.long	.L8-.L5
	.long	.L7-.L5
	.long	.L6-.L5
	.long	.L4-.L5
	.text
.L12:
	movl	$1, %r12d
	call	opt_next@PLT
	testl	%eax, %eax
	jne	.L23
.L73:
	call	opt_num_rest@PLT
	movl	%eax, %ebx
	testl	%eax, %eax
	jne	.L20
	movl	-100(%rbp), %eax
	movq	-120(%rbp), %rsi
	leaq	-72(%rbp), %rcx
	leaq	-80(%rbp), %rdx
	movq	-112(%rbp), %rdi
	orl	%eax, %r15d
	xorl	$1, %eax
	movl	%r15d, -148(%rbp)
	movl	%eax, %r15d
	andl	-104(%rbp), %r15d
	jne	.L24
	call	app_passwd@PLT
	testl	%eax, %eax
	je	.L42
	movq	bio_err(%rip), %rdi
	leaq	.LC2(%rip), %rsi
	xorl	%eax, %eax
	movl	-148(%rbp), %r15d
	call	BIO_printf@PLT
	movl	-100(%rbp), %r11d
	xorl	$1, %r15d
	testl	%r11d, %r11d
	je	.L26
	movq	-128(%rbp), %rdi
	movq	-80(%rbp), %rcx
	movq	%r14, %r8
	movl	$1, %edx
	movl	-96(%rbp), %esi
	leaq	.LC3(%rip), %r9
	call	load_pubkey@PLT
	movq	%rax, %rdi
.L27:
	testq	%rdi, %rdi
	je	.L29
	movq	%rdi, -112(%rbp)
	call	EVP_PKEY_get1_DSA@PLT
	movq	-112(%rbp), %rdi
	movq	%rax, %r13
	call	EVP_PKEY_free@PLT
	testq	%r13, %r13
	je	.L29
	movl	-92(%rbp), %esi
	movq	-136(%rbp), %rdi
	movl	%r15d, %edx
	call	bio_open_owner@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L46
	movl	-104(%rbp), %r10d
	testl	%r10d, %r10d
	jne	.L74
.L30:
	movl	-140(%rbp), %r9d
	testl	%r9d, %r9d
	jne	.L75
.L31:
	testl	%r12d, %r12d
	jne	.L21
	movq	bio_err(%rip), %rdi
	leaq	.LC8(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movl	-92(%rbp), %eax
	cmpl	$4, %eax
	je	.L76
	cmpl	$32773, %eax
	je	.L77
	subl	$11, %eax
	cmpl	$1, %eax
	ja	.L37
	call	EVP_PKEY_new@PLT
	movl	$1, %ebx
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L21
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	EVP_PKEY_set1_DSA@PLT
	cmpl	$12, -92(%rbp)
	je	.L78
	movl	-148(%rbp), %eax
	movq	%r12, %rsi
	movq	%r15, %rdi
	testl	%eax, %eax
	je	.L41
	call	i2b_PublicKey_bio@PLT
.L40:
	movq	%r12, %rdi
	movl	%eax, -100(%rbp)
	call	EVP_PKEY_free@PLT
	movl	-100(%rbp), %eax
.L34:
	xorl	%ebx, %ebx
	testl	%eax, %eax
	jg	.L21
	movq	bio_err(%rip), %rdi
	leaq	.LC11(%rip), %rsi
	xorl	%eax, %eax
	movl	$1, %ebx
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	jmp	.L21
.L18:
	call	opt_arg@PLT
	leaq	-96(%rbp), %rdx
	movl	$1982, %esi
	movq	%rax, %rdi
	call	opt_format@PLT
	testl	%eax, %eax
	jne	.L2
.L20:
	movq	bio_err(%rip), %rdi
	movq	%r13, %rdx
	xorl	%eax, %eax
	xorl	%ebx, %ebx
	leaq	.LC0(%rip), %rsi
	xorl	%r13d, %r13d
	xorl	%r15d, %r15d
	call	BIO_printf@PLT
.L21:
	movq	%r15, %rdi
	call	BIO_free_all@PLT
	movq	%r13, %rdi
	call	DSA_free@PLT
	movq	%r14, %rdi
	call	release_engine@PLT
	movq	-80(%rbp), %rdi
	movl	$257, %edx
	leaq	.LC12(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-72(%rbp), %rdi
	movl	$258, %edx
	leaq	.LC12(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L79
	leaq	-40(%rbp), %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L4:
	.cfi_restore_state
	call	opt_arg@PLT
	movq	%rax, -120(%rbp)
	jmp	.L2
.L6:
	call	opt_arg@PLT
	movq	%rax, -112(%rbp)
	jmp	.L2
.L7:
	call	opt_unknown@PLT
	leaq	-88(%rbp), %rsi
	movq	%rax, %rdi
	call	opt_cipher@PLT
	testl	%eax, %eax
	jne	.L2
	movl	$1, %ebx
	xorl	%r13d, %r13d
	xorl	%r15d, %r15d
	jmp	.L21
.L8:
	movl	$1, %r15d
	jmp	.L2
.L9:
	movl	$1, -100(%rbp)
	jmp	.L2
.L10:
	movl	$1, -140(%rbp)
	jmp	.L2
.L11:
	movl	$1, -104(%rbp)
	jmp	.L2
.L13:
	subl	$7, %eax
	movl	%eax, -144(%rbp)
	jmp	.L2
.L14:
	call	opt_arg@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	setup_engine@PLT
	movq	%rax, %r14
	jmp	.L2
.L15:
	call	opt_arg@PLT
	movq	%rax, -136(%rbp)
	jmp	.L2
.L16:
	call	opt_arg@PLT
	movq	%rax, -128(%rbp)
	jmp	.L2
.L17:
	call	opt_arg@PLT
	leaq	-92(%rbp), %rdx
	movl	$1982, %esi
	movq	%rax, %rdi
	call	opt_format@PLT
	testl	%eax, %eax
	jne	.L2
	jmp	.L20
.L19:
	leaq	dsa_options(%rip), %rdi
	xorl	%ebx, %ebx
	xorl	%r13d, %r13d
	xorl	%r15d, %r15d
	call	opt_help@PLT
	jmp	.L21
.L24:
	call	app_passwd@PLT
	testl	%eax, %eax
	je	.L42
	movq	bio_err(%rip), %rdi
	leaq	.LC2(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L26:
	movq	-128(%rbp), %rdi
	movq	-80(%rbp), %rcx
	movq	%r14, %r8
	movl	$1, %edx
	movl	-96(%rbp), %esi
	leaq	.LC4(%rip), %r9
	call	load_key@PLT
	movq	%rax, %rdi
	jmp	.L27
.L42:
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
	xorl	%r13d, %r13d
	xorl	%r15d, %r15d
	leaq	.LC1(%rip), %rsi
	movl	$1, %ebx
	call	BIO_printf@PLT
	jmp	.L21
.L29:
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
	xorl	%r13d, %r13d
	xorl	%r15d, %r15d
	leaq	.LC5(%rip), %rsi
	movl	$1, %ebx
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	jmp	.L21
.L74:
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	DSA_print@PLT
	testl	%eax, %eax
	jne	.L30
	movq	-136(%rbp), %rdi
	call	perror@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	movl	-104(%rbp), %ebx
	jmp	.L21
.L75:
	xorl	%edx, %edx
	leaq	-64(%rbp), %rsi
	movq	%r13, %rdi
	movq	$0, -64(%rbp)
	call	DSA_get0_key@PLT
	movq	%r15, %rdi
	leaq	.LC6(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	-64(%rbp), %rsi
	movq	%r15, %rdi
	call	BN_print@PLT
	leaq	.LC7(%rip), %rsi
	movq	%r15, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L31
.L46:
	movl	$1, %ebx
	jmp	.L21
.L76:
	movl	-148(%rbp), %r8d
	movq	%r13, %rsi
	movq	%r15, %rdi
	testl	%r8d, %r8d
	je	.L33
	call	i2d_DSA_PUBKEY_bio@PLT
	jmp	.L34
.L77:
	movl	-148(%rbp), %esi
	testl	%esi, %esi
	je	.L36
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	PEM_write_bio_DSA_PUBKEY@PLT
	jmp	.L34
.L33:
	call	i2d_DSAPrivateKey_bio@PLT
	jmp	.L34
.L37:
	movq	bio_err(%rip), %rdi
	leaq	.LC10(%rip), %rsi
	xorl	%eax, %eax
	movl	$1, %ebx
	call	BIO_printf@PLT
	jmp	.L21
.L36:
	subq	$8, %rsp
	pushq	-72(%rbp)
	movq	-88(%rbp), %rdx
	xorl	%ecx, %ecx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	PEM_write_bio_DSAPrivateKey@PLT
	popq	%rdx
	popq	%rcx
	jmp	.L34
.L78:
	cmpl	$0, -100(%rbp)
	jne	.L80
	movq	-72(%rbp), %r8
	movl	-144(%rbp), %edx
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	i2b_PVK_bio@PLT
	jmp	.L40
.L41:
	call	i2b_PrivateKey_bio@PLT
	jmp	.L40
.L79:
	call	__stack_chk_fail@PLT
.L80:
	movq	bio_err(%rip), %rdi
	leaq	.LC9(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	%r12, %rdi
	call	EVP_PKEY_free@PLT
	movl	-100(%rbp), %ebx
	jmp	.L21
	.cfi_endproc
.LFE1435:
	.size	dsa_main, .-dsa_main
	.globl	dsa_options
	.section	.rodata.str1.1
.LC13:
	.string	"help"
.LC14:
	.string	"Display this summary"
.LC15:
	.string	"inform"
.LC16:
	.string	"Input format, DER PEM PVK"
.LC17:
	.string	"outform"
.LC18:
	.string	"Output format, DER PEM PVK"
.LC19:
	.string	"in"
.LC20:
	.string	"Input key"
.LC21:
	.string	"out"
.LC22:
	.string	"Output file"
.LC23:
	.string	"noout"
.LC24:
	.string	"Don't print key out"
.LC25:
	.string	"text"
.LC26:
	.string	"Print the key in text"
.LC27:
	.string	"modulus"
.LC28:
	.string	"Print the DSA public value"
.LC29:
	.string	"pubin"
	.section	.rodata.str1.8
	.align 8
.LC30:
	.string	"Expect a public key in input file"
	.section	.rodata.str1.1
.LC31:
	.string	"pubout"
	.section	.rodata.str1.8
	.align 8
.LC32:
	.string	"Output public key, not private"
	.section	.rodata.str1.1
.LC33:
	.string	"passin"
.LC34:
	.string	"Input file pass phrase source"
.LC35:
	.string	"passout"
	.section	.rodata.str1.8
	.align 8
.LC36:
	.string	"Output file pass phrase source"
	.section	.rodata.str1.1
.LC37:
	.string	""
.LC38:
	.string	"Any supported cipher"
.LC39:
	.string	"pvk-strong"
	.section	.rodata.str1.8
	.align 8
.LC40:
	.string	"Enable 'Strong' PVK encoding level (default)"
	.section	.rodata.str1.1
.LC41:
	.string	"pvk-weak"
	.section	.rodata.str1.8
	.align 8
.LC42:
	.string	"Enable 'Weak' PVK encoding level"
	.section	.rodata.str1.1
.LC43:
	.string	"pvk-none"
.LC44:
	.string	"Don't enforce PVK encoding"
.LC45:
	.string	"engine"
	.section	.rodata.str1.8
	.align 8
.LC46:
	.string	"Use engine e, possibly a hardware device"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	dsa_options, @object
	.size	dsa_options, 432
dsa_options:
	.quad	.LC13
	.long	1
	.long	45
	.quad	.LC14
	.quad	.LC15
	.long	2
	.long	102
	.quad	.LC16
	.quad	.LC17
	.long	3
	.long	102
	.quad	.LC18
	.quad	.LC19
	.long	4
	.long	115
	.quad	.LC20
	.quad	.LC21
	.long	5
	.long	62
	.quad	.LC22
	.quad	.LC23
	.long	10
	.long	45
	.quad	.LC24
	.quad	.LC25
	.long	11
	.long	45
	.quad	.LC26
	.quad	.LC27
	.long	12
	.long	45
	.quad	.LC28
	.quad	.LC29
	.long	13
	.long	45
	.quad	.LC30
	.quad	.LC31
	.long	14
	.long	45
	.quad	.LC32
	.quad	.LC33
	.long	16
	.long	115
	.quad	.LC34
	.quad	.LC35
	.long	17
	.long	115
	.quad	.LC36
	.quad	.LC37
	.long	15
	.long	45
	.quad	.LC38
	.quad	.LC39
	.long	9
	.long	45
	.quad	.LC40
	.quad	.LC41
	.long	8
	.long	45
	.quad	.LC42
	.quad	.LC43
	.long	7
	.long	45
	.quad	.LC44
	.quad	.LC45
	.long	6
	.long	115
	.quad	.LC46
	.quad	0
	.zero	16
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
