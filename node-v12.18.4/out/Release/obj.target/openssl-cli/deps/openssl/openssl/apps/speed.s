	.file	"speed.c"
	.text
	.p2align 4
	.type	alarmed, @function
alarmed:
.LFB1436:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	alarmed(%rip), %rsi
	movl	$14, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	signal@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	movl	$0, run(%rip)
	ret
	.cfi_endproc
.LFE1436:
	.size	alarmed, .-alarmed
	.p2align 4
	.type	RAND_bytes_loop, @function
RAND_bytes_loop:
.LFB1458:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	run(%rip), %r12d
	pushq	%rbx
	.cfi_offset 3, -32
	movq	(%rdi), %rax
	movq	16(%rax), %rbx
	testl	%r12d, %r12d
	je	.L4
	xorl	%r12d, %r12d
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L15:
	cmpl	$2147483647, %r12d
	je	.L4
.L6:
	movl	testnum(%rip), %edx
	movq	lengths(%rip), %rax
	movq	%rbx, %rdi
	addl	$1, %r12d
	movl	(%rax,%rdx,4), %esi
	call	RAND_bytes@PLT
	movl	run(%rip), %eax
	testl	%eax, %eax
	jne	.L15
.L4:
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1458:
	.size	RAND_bytes_loop, .-RAND_bytes_loop
	.p2align 4
	.type	ECDH_EVP_derive_key_loop, @function
ECDH_EVP_derive_key_loop:
.LFB1469:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	testnum(%rip), %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movl	run(%rip), %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movq	(%rdi), %rax
	movq	328(%rax,%rdx,8), %rbx
	movq	536(%rax), %r13
	leaq	552(%rax,%rdx,8), %r14
	testl	%r12d, %r12d
	je	.L16
	xorl	%r12d, %r12d
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L27:
	cmpl	$2147483647, %r12d
	je	.L16
.L18:
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	addl	$1, %r12d
	call	EVP_PKEY_derive@PLT
	movl	run(%rip), %eax
	testl	%eax, %eax
	jne	.L27
.L16:
	popq	%rbx
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1469:
	.size	ECDH_EVP_derive_key_loop, .-ECDH_EVP_derive_key_loop
	.p2align 4
	.type	EVP_Digest_loop, @function
EVP_Digest_loop:
.LFB1462:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movl	run(%rip), %r12d
	movq	16(%rax), %rbx
	testl	%r12d, %r12d
	je	.L28
	xorl	%r12d, %r12d
	leaq	-112(%rbp), %r13
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L41:
	movl	run(%rip), %eax
	addl	$1, %r12d
	testl	%eax, %eax
	je	.L28
	cmpl	$2147483647, %r12d
	je	.L28
.L30:
	movl	testnum(%rip), %edx
	xorl	%r9d, %r9d
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	movq	lengths(%rip), %rax
	movq	evp_md(%rip), %r8
	movslq	(%rax,%rdx,4), %rsi
	movq	%r13, %rdx
	call	EVP_Digest@PLT
	testl	%eax, %eax
	jne	.L41
	movl	$-1, %r12d
.L28:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L42
	addq	$88, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L42:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1462:
	.size	EVP_Digest_loop, .-EVP_Digest_loop
	.p2align 4
	.type	EVP_Update_loop_aead, @function
EVP_Update_loop_aead:
.LFB1461:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	decrypt(%rip), %r14d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	$204, -93(%rbp)
	movq	$204, -80(%rbp)
	movq	16(%rax), %r12
	movq	744(%rax), %rbx
	movb	$0, -81(%rbp)
	movl	$0, -85(%rbp)
	movq	$0, -72(%rbp)
	testl	%r14d, %r14d
	jne	.L44
	movl	run(%rip), %eax
	leaq	-93(%rbp), %rcx
	leaq	-100(%rbp), %r13
	movq	%rcx, -120(%rbp)
	leaq	iv(%rip), %r15
	testl	%eax, %eax
	jne	.L45
	.p2align 4,,10
	.p2align 3
.L43:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L64
	addq	$88, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L65:
	.cfi_restore_state
	cmpl	$2147483647, %r14d
	je	.L48
.L45:
	movq	%r15, %r8
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	addl	$1, %r14d
	call	EVP_EncryptInit_ex@PLT
	movq	-120(%rbp), %rcx
	movq	%r13, %rdx
	xorl	%esi, %esi
	movl	$13, %r8d
	movq	%rbx, %rdi
	call	EVP_EncryptUpdate@PLT
	movq	%r12, %rcx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movl	testnum(%rip), %edx
	movq	lengths(%rip), %rax
	movl	(%rax,%rdx,4), %r8d
	movq	%r13, %rdx
	call	EVP_EncryptUpdate@PLT
	movslq	-100(%rbp), %rsi
	movq	%r13, %rdx
	movq	%rbx, %rdi
	addq	%r12, %rsi
	call	EVP_EncryptFinal_ex@PLT
	movl	run(%rip), %eax
	testl	%eax, %eax
	jne	.L65
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L44:
	movl	run(%rip), %r14d
	testl	%r14d, %r14d
	je	.L43
	leaq	-93(%rbp), %rax
	xorl	%r14d, %r14d
	leaq	-80(%rbp), %r15
	movq	%rax, -120(%rbp)
	leaq	-100(%rbp), %r13
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L66:
	cmpl	$2147483647, %r14d
	je	.L48
.L47:
	leaq	iv(%rip), %r8
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	addl	$1, %r14d
	call	EVP_DecryptInit_ex@PLT
	movq	%r15, %rcx
	movl	$16, %edx
	movq	%rbx, %rdi
	movl	$17, %esi
	call	EVP_CIPHER_CTX_ctrl@PLT
	movq	-120(%rbp), %rcx
	movq	%r13, %rdx
	xorl	%esi, %esi
	movl	$13, %r8d
	movq	%rbx, %rdi
	call	EVP_DecryptUpdate@PLT
	movq	%r12, %rcx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movl	testnum(%rip), %edx
	movq	lengths(%rip), %rax
	movl	(%rax,%rdx,4), %r8d
	movq	%r13, %rdx
	call	EVP_DecryptUpdate@PLT
	movslq	-100(%rbp), %rsi
	movq	%r13, %rdx
	movq	%rbx, %rdi
	addq	%r12, %rsi
	call	EVP_DecryptFinal_ex@PLT
	movl	run(%rip), %eax
	testl	%eax, %eax
	jne	.L66
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L48:
	movl	$2147483647, %r14d
	jmp	.L43
.L64:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1461:
	.size	EVP_Update_loop_aead, .-EVP_Update_loop_aead
	.p2align 4
	.type	EVP_Update_loop_ccm, @function
EVP_Update_loop_ccm:
.LFB1460:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	decrypt(%rip), %r12d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	16(%rax), %r15
	movq	744(%rax), %r13
	testl	%r12d, %r12d
	jne	.L68
	movl	run(%rip), %eax
	leaq	-72(%rbp), %r14
	testl	%eax, %eax
	jne	.L69
	.p2align 4,,10
	.p2align 3
.L70:
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	EVP_EncryptFinal_ex@PLT
.L67:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L92
	addq	$56, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L93:
	.cfi_restore_state
	cmpl	$2147483647, %r12d
	je	.L74
.L69:
	movl	testnum(%rip), %edx
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	movq	%r13, %rdi
	movq	lengths(%rip), %rax
	addl	$1, %r12d
	movl	(%rax,%rdx,4), %r8d
	movq	%r14, %rdx
	call	EVP_EncryptUpdate@PLT
	movq	%r15, %rcx
	movq	%r15, %rsi
	movq	%r13, %rdi
	movl	testnum(%rip), %edx
	movq	lengths(%rip), %rax
	movl	(%rax,%rdx,4), %r8d
	movq	%r14, %rdx
	call	EVP_EncryptUpdate@PLT
	movl	run(%rip), %eax
	testl	%eax, %eax
	jne	.L93
	movl	decrypt(%rip), %eax
	testl	%eax, %eax
	je	.L70
.L71:
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	EVP_DecryptFinal_ex@PLT
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L68:
	movl	run(%rip), %r12d
	leaq	-72(%rbp), %r14
	testl	%r12d, %r12d
	je	.L71
	leaq	-68(%rbp), %rax
	xorl	%r12d, %r12d
	leaq	-72(%rbp), %r14
	movq	%rax, -88(%rbp)
	leaq	iv(%rip), %rbx
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L94:
	cmpl	$2147483647, %r12d
	je	.L74
.L73:
	movq	-88(%rbp), %rcx
	movl	$12, %edx
	movq	%r13, %rdi
	addl	$1, %r12d
	movl	$17, %esi
	call	EVP_CIPHER_CTX_ctrl@PLT
	movq	%rbx, %r8
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	EVP_DecryptInit_ex@PLT
	movq	%r15, %rcx
	movq	%r15, %rsi
	movq	%r13, %rdi
	movl	testnum(%rip), %edx
	movq	lengths(%rip), %rax
	movl	(%rax,%rdx,4), %r8d
	movq	%r14, %rdx
	call	EVP_DecryptUpdate@PLT
	movl	run(%rip), %eax
	testl	%eax, %eax
	jne	.L94
	movl	decrypt(%rip), %eax
	testl	%eax, %eax
	je	.L70
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L74:
	movl	decrypt(%rip), %eax
	movl	$2147483647, %r12d
	testl	%eax, %eax
	je	.L70
	jmp	.L71
.L92:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1460:
	.size	EVP_Update_loop_ccm, .-EVP_Update_loop_ccm
	.p2align 4
	.type	EVP_Update_loop, @function
EVP_Update_loop:
.LFB1459:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	decrypt(%rip), %r12d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	16(%rax), %r13
	movq	744(%rax), %r14
	testl	%r12d, %r12d
	jne	.L96
	movl	run(%rip), %eax
	leaq	-60(%rbp), %r15
	leaq	iv(%rip), %rbx
	testl	%eax, %eax
	jne	.L97
	.p2align 4,,10
	.p2align 3
.L98:
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	EVP_EncryptFinal_ex@PLT
.L95:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L128
	addq	$24, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L129:
	.cfi_restore_state
	cmpl	$2147483647, %r12d
	je	.L104
.L97:
	movl	testnum(%rip), %edx
	movq	%r13, %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	lengths(%rip), %rax
	movl	(%rax,%rdx,4), %r8d
	movq	%r15, %rdx
	call	EVP_EncryptUpdate@PLT
	cmpl	$1, %eax
	je	.L103
	movl	$-1, %r9d
	movq	%rbx, %r8
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	EVP_CipherInit_ex@PLT
.L103:
	movl	run(%rip), %eax
	addl	$1, %r12d
	testl	%eax, %eax
	jne	.L129
	movl	decrypt(%rip), %eax
	testl	%eax, %eax
	je	.L98
.L99:
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	EVP_DecryptFinal_ex@PLT
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L96:
	movl	run(%rip), %r12d
	leaq	-60(%rbp), %r15
	testl	%r12d, %r12d
	je	.L99
	xorl	%r12d, %r12d
	leaq	-60(%rbp), %r15
	leaq	iv(%rip), %rbx
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L130:
	cmpl	$2147483647, %r12d
	je	.L104
.L102:
	movl	testnum(%rip), %edx
	movq	%r13, %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	lengths(%rip), %rax
	movl	(%rax,%rdx,4), %r8d
	movq	%r15, %rdx
	call	EVP_DecryptUpdate@PLT
	cmpl	$1, %eax
	je	.L100
	movl	$-1, %r9d
	movq	%rbx, %r8
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	EVP_CipherInit_ex@PLT
.L100:
	movl	run(%rip), %eax
	addl	$1, %r12d
	testl	%eax, %eax
	jne	.L130
	movl	decrypt(%rip), %eax
	testl	%eax, %eax
	je	.L98
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L104:
	movl	decrypt(%rip), %eax
	movl	$2147483647, %r12d
	testl	%eax, %eax
	je	.L98
	jmp	.L99
.L128:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1459:
	.size	EVP_Update_loop, .-EVP_Update_loop
	.p2align 4
	.type	CRYPTO_gcm128_aad_loop, @function
CRYPTO_gcm128_aad_loop:
.LFB1457:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	(%rdi), %rax
	movl	run(%rip), %r12d
	movq	16(%rax), %rbx
	movq	760(%rax), %r13
	testl	%r12d, %r12d
	je	.L131
	xorl	%r12d, %r12d
	jmp	.L133
	.p2align 4,,10
	.p2align 3
.L142:
	cmpl	$2147483647, %r12d
	je	.L131
.L133:
	movl	testnum(%rip), %edx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	addl	$1, %r12d
	movq	lengths(%rip), %rax
	movslq	(%rax,%rdx,4), %rdx
	call	CRYPTO_gcm128_aad@PLT
	movl	run(%rip), %eax
	testl	%eax, %eax
	jne	.L142
.L131:
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1457:
	.size	CRYPTO_gcm128_aad_loop, .-CRYPTO_gcm128_aad_loop
	.p2align 4
	.type	AES_ige_256_encrypt_loop, @function
AES_ige_256_encrypt_loop:
.LFB1456:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movl	run(%rip), %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movq	(%rdi), %rax
	movq	16(%rax), %rbx
	movq	24(%rax), %r13
	testl	%r12d, %r12d
	je	.L143
	xorl	%r12d, %r12d
	leaq	iv(%rip), %r14
	jmp	.L145
	.p2align 4,,10
	.p2align 3
.L154:
	cmpl	$2147483647, %r12d
	je	.L143
.L145:
	movq	lengths(%rip), %rax
	movq	%r14, %r8
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movl	testnum(%rip), %edx
	movl	$1, %r9d
	leaq	aes_ks3(%rip), %rcx
	addl	$1, %r12d
	movslq	(%rax,%rdx,4), %rdx
	call	AES_ige_encrypt@PLT
	movl	run(%rip), %eax
	testl	%eax, %eax
	jne	.L154
.L143:
	popq	%rbx
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1456:
	.size	AES_ige_256_encrypt_loop, .-AES_ige_256_encrypt_loop
	.p2align 4
	.type	AES_ige_192_encrypt_loop, @function
AES_ige_192_encrypt_loop:
.LFB1455:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movl	run(%rip), %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movq	(%rdi), %rax
	movq	16(%rax), %rbx
	movq	24(%rax), %r13
	testl	%r12d, %r12d
	je	.L155
	xorl	%r12d, %r12d
	leaq	iv(%rip), %r14
	jmp	.L157
	.p2align 4,,10
	.p2align 3
.L166:
	cmpl	$2147483647, %r12d
	je	.L155
.L157:
	movq	lengths(%rip), %rax
	movq	%r14, %r8
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movl	testnum(%rip), %edx
	movl	$1, %r9d
	leaq	aes_ks2(%rip), %rcx
	addl	$1, %r12d
	movslq	(%rax,%rdx,4), %rdx
	call	AES_ige_encrypt@PLT
	movl	run(%rip), %eax
	testl	%eax, %eax
	jne	.L166
.L155:
	popq	%rbx
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1455:
	.size	AES_ige_192_encrypt_loop, .-AES_ige_192_encrypt_loop
	.p2align 4
	.type	AES_ige_128_encrypt_loop, @function
AES_ige_128_encrypt_loop:
.LFB1454:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movl	run(%rip), %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movq	(%rdi), %rax
	movq	16(%rax), %rbx
	movq	24(%rax), %r13
	testl	%r12d, %r12d
	je	.L167
	xorl	%r12d, %r12d
	leaq	iv(%rip), %r14
	jmp	.L169
	.p2align 4,,10
	.p2align 3
.L178:
	cmpl	$2147483647, %r12d
	je	.L167
.L169:
	movq	lengths(%rip), %rax
	movq	%r14, %r8
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movl	testnum(%rip), %edx
	movl	$1, %r9d
	leaq	aes_ks1(%rip), %rcx
	addl	$1, %r12d
	movslq	(%rax,%rdx,4), %rdx
	call	AES_ige_encrypt@PLT
	movl	run(%rip), %eax
	testl	%eax, %eax
	jne	.L178
.L167:
	popq	%rbx
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1454:
	.size	AES_ige_128_encrypt_loop, .-AES_ige_128_encrypt_loop
	.p2align 4
	.type	AES_cbc_256_encrypt_loop, @function
AES_cbc_256_encrypt_loop:
.LFB1453:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	(%rdi), %rax
	movl	run(%rip), %r12d
	movq	16(%rax), %rbx
	testl	%r12d, %r12d
	je	.L179
	xorl	%r12d, %r12d
	leaq	iv(%rip), %r13
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L190:
	cmpl	$2147483647, %r12d
	je	.L179
.L181:
	movq	lengths(%rip), %rax
	movq	%r13, %r8
	movq	%rbx, %rsi
	movq	%rbx, %rdi
	movl	testnum(%rip), %edx
	movl	$1, %r9d
	leaq	aes_ks3(%rip), %rcx
	addl	$1, %r12d
	movslq	(%rax,%rdx,4), %rdx
	call	AES_cbc_encrypt@PLT
	movl	run(%rip), %eax
	testl	%eax, %eax
	jne	.L190
.L179:
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1453:
	.size	AES_cbc_256_encrypt_loop, .-AES_cbc_256_encrypt_loop
	.p2align 4
	.type	AES_cbc_192_encrypt_loop, @function
AES_cbc_192_encrypt_loop:
.LFB1452:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	(%rdi), %rax
	movl	run(%rip), %r12d
	movq	16(%rax), %rbx
	testl	%r12d, %r12d
	je	.L191
	xorl	%r12d, %r12d
	leaq	iv(%rip), %r13
	jmp	.L193
	.p2align 4,,10
	.p2align 3
.L202:
	cmpl	$2147483647, %r12d
	je	.L191
.L193:
	movq	lengths(%rip), %rax
	movq	%r13, %r8
	movq	%rbx, %rsi
	movq	%rbx, %rdi
	movl	testnum(%rip), %edx
	movl	$1, %r9d
	leaq	aes_ks2(%rip), %rcx
	addl	$1, %r12d
	movslq	(%rax,%rdx,4), %rdx
	call	AES_cbc_encrypt@PLT
	movl	run(%rip), %eax
	testl	%eax, %eax
	jne	.L202
.L191:
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1452:
	.size	AES_cbc_192_encrypt_loop, .-AES_cbc_192_encrypt_loop
	.p2align 4
	.type	AES_cbc_128_encrypt_loop, @function
AES_cbc_128_encrypt_loop:
.LFB1451:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	(%rdi), %rax
	movl	run(%rip), %r12d
	movq	16(%rax), %rbx
	testl	%r12d, %r12d
	je	.L203
	xorl	%r12d, %r12d
	leaq	iv(%rip), %r13
	jmp	.L205
	.p2align 4,,10
	.p2align 3
.L214:
	cmpl	$2147483647, %r12d
	je	.L203
.L205:
	movq	lengths(%rip), %rax
	movq	%r13, %r8
	movq	%rbx, %rsi
	movq	%rbx, %rdi
	movl	testnum(%rip), %edx
	movl	$1, %r9d
	leaq	aes_ks1(%rip), %rcx
	addl	$1, %r12d
	movslq	(%rax,%rdx,4), %rdx
	call	AES_cbc_encrypt@PLT
	movl	run(%rip), %eax
	testl	%eax, %eax
	jne	.L214
.L203:
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1451:
	.size	AES_cbc_128_encrypt_loop, .-AES_cbc_128_encrypt_loop
	.p2align 4
	.type	DES_ede3_cbc_encrypt_loop, @function
DES_ede3_cbc_encrypt_loop:
.LFB1450:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movl	run(%rip), %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movq	(%rdi), %rax
	movq	16(%rax), %rbx
	testl	%r12d, %r12d
	je	.L215
	xorl	%r12d, %r12d
	leaq	DES_iv(%rip), %r13
	leaq	sch3(%rip), %r14
	jmp	.L217
	.p2align 4,,10
	.p2align 3
.L226:
	cmpl	$2147483647, %r12d
	je	.L215
.L217:
	movq	lengths(%rip), %rax
	movq	%r14, %r9
	movq	%rbx, %rsi
	movq	%rbx, %rdi
	movl	testnum(%rip), %edx
	leaq	sch(%rip), %rcx
	leaq	sch2(%rip), %r8
	addl	$1, %r12d
	movslq	(%rax,%rdx,4), %rdx
	pushq	$1
	pushq	%r13
	call	DES_ede3_cbc_encrypt@PLT
	movl	run(%rip), %eax
	popq	%rdx
	popq	%rcx
	testl	%eax, %eax
	jne	.L226
.L215:
	leaq	-32(%rbp), %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1450:
	.size	DES_ede3_cbc_encrypt_loop, .-DES_ede3_cbc_encrypt_loop
	.p2align 4
	.type	DES_ncbc_encrypt_loop, @function
DES_ncbc_encrypt_loop:
.LFB1449:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	(%rdi), %rax
	movl	run(%rip), %r12d
	movq	16(%rax), %rbx
	testl	%r12d, %r12d
	je	.L227
	xorl	%r12d, %r12d
	leaq	DES_iv(%rip), %r13
	jmp	.L229
	.p2align 4,,10
	.p2align 3
.L238:
	cmpl	$2147483647, %r12d
	je	.L227
.L229:
	movq	lengths(%rip), %rax
	movq	%r13, %r8
	movq	%rbx, %rsi
	movq	%rbx, %rdi
	movl	testnum(%rip), %edx
	movl	$1, %r9d
	leaq	sch(%rip), %rcx
	addl	$1, %r12d
	movslq	(%rax,%rdx,4), %rdx
	call	DES_ncbc_encrypt@PLT
	movl	run(%rip), %eax
	testl	%eax, %eax
	jne	.L238
.L227:
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1449:
	.size	DES_ncbc_encrypt_loop, .-DES_ncbc_encrypt_loop
	.p2align 4
	.type	RC4_loop, @function
RC4_loop:
.LFB1448:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	(%rdi), %rax
	movl	run(%rip), %r12d
	movq	16(%rax), %rbx
	testl	%r12d, %r12d
	je	.L239
	xorl	%r12d, %r12d
	leaq	rc4_ks(%rip), %r13
	jmp	.L241
	.p2align 4,,10
	.p2align 3
.L250:
	cmpl	$2147483647, %r12d
	je	.L239
.L241:
	movl	testnum(%rip), %edx
	movq	%rbx, %rcx
	movq	%r13, %rdi
	addl	$1, %r12d
	movq	lengths(%rip), %rax
	movslq	(%rax,%rdx,4), %rsi
	movq	%rbx, %rdx
	call	RC4@PLT
	movl	run(%rip), %eax
	testl	%eax, %eax
	jne	.L250
.L239:
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1448:
	.size	RC4_loop, .-RC4_loop
	.p2align 4
	.type	EVP_Digest_RMD160_loop, @function
EVP_Digest_RMD160_loop:
.LFB1447:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movl	run(%rip), %r12d
	movq	16(%rax), %rbx
	testl	%r12d, %r12d
	je	.L251
	xorl	%r12d, %r12d
	leaq	-64(%rbp), %r13
	jmp	.L253
	.p2align 4,,10
	.p2align 3
.L264:
	movl	run(%rip), %eax
	addl	$1, %r12d
	testl	%eax, %eax
	je	.L251
	cmpl	$2147483647, %r12d
	je	.L251
.L253:
	call	EVP_ripemd160@PLT
	movl	testnum(%rip), %edx
	xorl	%r9d, %r9d
	xorl	%ecx, %ecx
	movq	%rax, %r8
	movq	lengths(%rip), %rax
	movq	%rbx, %rdi
	movslq	(%rax,%rdx,4), %rsi
	movq	%r13, %rdx
	call	EVP_Digest@PLT
	testl	%eax, %eax
	jne	.L264
	movl	$-1, %r12d
.L251:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L265
	addq	$40, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L265:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1447:
	.size	EVP_Digest_RMD160_loop, .-EVP_Digest_RMD160_loop
	.p2align 4
	.type	WHIRLPOOL_loop, @function
WHIRLPOOL_loop:
.LFB1446:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movl	run(%rip), %r12d
	movq	16(%rax), %rbx
	testl	%r12d, %r12d
	je	.L266
	xorl	%r12d, %r12d
	leaq	-112(%rbp), %r13
	jmp	.L268
	.p2align 4,,10
	.p2align 3
.L278:
	cmpl	$2147483647, %r12d
	je	.L266
.L268:
	movl	testnum(%rip), %edx
	movq	lengths(%rip), %rax
	movq	%rbx, %rdi
	addl	$1, %r12d
	movslq	(%rax,%rdx,4), %rsi
	movq	%r13, %rdx
	call	WHIRLPOOL@PLT
	movl	run(%rip), %eax
	testl	%eax, %eax
	jne	.L278
.L266:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L279
	addq	$88, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L279:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1446:
	.size	WHIRLPOOL_loop, .-WHIRLPOOL_loop
	.p2align 4
	.type	SHA512_loop, @function
SHA512_loop:
.LFB1445:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movl	run(%rip), %r12d
	movq	16(%rax), %rbx
	testl	%r12d, %r12d
	je	.L280
	xorl	%r12d, %r12d
	leaq	-112(%rbp), %r13
	jmp	.L282
	.p2align 4,,10
	.p2align 3
.L292:
	cmpl	$2147483647, %r12d
	je	.L280
.L282:
	movl	testnum(%rip), %edx
	movq	lengths(%rip), %rax
	movq	%rbx, %rdi
	addl	$1, %r12d
	movslq	(%rax,%rdx,4), %rsi
	movq	%r13, %rdx
	call	SHA512@PLT
	movl	run(%rip), %eax
	testl	%eax, %eax
	jne	.L292
.L280:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L293
	addq	$88, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L293:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1445:
	.size	SHA512_loop, .-SHA512_loop
	.p2align 4
	.type	SHA256_loop, @function
SHA256_loop:
.LFB1444:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movl	run(%rip), %r12d
	movq	16(%rax), %rbx
	testl	%r12d, %r12d
	je	.L294
	xorl	%r12d, %r12d
	leaq	-80(%rbp), %r13
	jmp	.L296
	.p2align 4,,10
	.p2align 3
.L306:
	cmpl	$2147483647, %r12d
	je	.L294
.L296:
	movl	testnum(%rip), %edx
	movq	lengths(%rip), %rax
	movq	%rbx, %rdi
	addl	$1, %r12d
	movslq	(%rax,%rdx,4), %rsi
	movq	%r13, %rdx
	call	SHA256@PLT
	movl	run(%rip), %eax
	testl	%eax, %eax
	jne	.L306
.L294:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L307
	addq	$56, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L307:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1444:
	.size	SHA256_loop, .-SHA256_loop
	.p2align 4
	.type	SHA1_loop, @function
SHA1_loop:
.LFB1443:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movl	run(%rip), %r12d
	movq	16(%rax), %rbx
	testl	%r12d, %r12d
	je	.L308
	xorl	%r12d, %r12d
	leaq	-64(%rbp), %r13
	jmp	.L310
	.p2align 4,,10
	.p2align 3
.L320:
	cmpl	$2147483647, %r12d
	je	.L308
.L310:
	movl	testnum(%rip), %edx
	movq	lengths(%rip), %rax
	movq	%rbx, %rdi
	addl	$1, %r12d
	movslq	(%rax,%rdx,4), %rsi
	movq	%r13, %rdx
	call	SHA1@PLT
	movl	run(%rip), %eax
	testl	%eax, %eax
	jne	.L320
.L308:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L321
	addq	$40, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L321:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1443:
	.size	SHA1_loop, .-SHA1_loop
	.p2align 4
	.type	HMAC_loop, @function
HMAC_loop:
.LFB1442:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movl	run(%rip), %r12d
	movq	16(%rax), %r13
	movq	752(%rax), %rbx
	testl	%r12d, %r12d
	je	.L322
	xorl	%r12d, %r12d
	leaq	-64(%rbp), %r14
	jmp	.L324
	.p2align 4,,10
	.p2align 3
.L334:
	cmpl	$2147483647, %r12d
	je	.L322
.L324:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	addl	$1, %r12d
	call	HMAC_Init_ex@PLT
	movl	testnum(%rip), %edx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movq	lengths(%rip), %rax
	movslq	(%rax,%rdx,4), %rdx
	call	HMAC_Update@PLT
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	HMAC_Final@PLT
	movl	run(%rip), %eax
	testl	%eax, %eax
	jne	.L334
.L322:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L335
	addq	$32, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L335:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1442:
	.size	HMAC_loop, .-HMAC_loop
	.p2align 4
	.type	MD5_loop, @function
MD5_loop:
.LFB1441:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movl	run(%rip), %r12d
	movq	16(%rax), %rbx
	testl	%r12d, %r12d
	je	.L336
	xorl	%r12d, %r12d
	leaq	-64(%rbp), %r13
	jmp	.L338
	.p2align 4,,10
	.p2align 3
.L348:
	cmpl	$2147483647, %r12d
	je	.L336
.L338:
	movl	testnum(%rip), %edx
	movq	lengths(%rip), %rax
	movq	%rbx, %rdi
	addl	$1, %r12d
	movslq	(%rax,%rdx,4), %rsi
	movq	%r13, %rdx
	call	MD5@PLT
	movl	run(%rip), %eax
	testl	%eax, %eax
	jne	.L348
.L336:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L349
	addq	$40, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L349:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1441:
	.size	MD5_loop, .-MD5_loop
	.p2align 4
	.type	EVP_Digest_MD4_loop, @function
EVP_Digest_MD4_loop:
.LFB1440:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movl	run(%rip), %r12d
	movq	16(%rax), %rbx
	testl	%r12d, %r12d
	je	.L350
	xorl	%r12d, %r12d
	leaq	-64(%rbp), %r13
	jmp	.L352
	.p2align 4,,10
	.p2align 3
.L363:
	movl	run(%rip), %eax
	addl	$1, %r12d
	testl	%eax, %eax
	je	.L350
	cmpl	$2147483647, %r12d
	je	.L350
.L352:
	call	EVP_md4@PLT
	movl	testnum(%rip), %edx
	xorl	%r9d, %r9d
	xorl	%ecx, %ecx
	movq	%rax, %r8
	movq	lengths(%rip), %rax
	movq	%rbx, %rdi
	movslq	(%rax,%rdx,4), %rsi
	movq	%r13, %rdx
	call	EVP_Digest@PLT
	testl	%eax, %eax
	jne	.L363
	movl	$-1, %r12d
.L350:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L364
	addq	$40, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L364:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1440:
	.size	EVP_Digest_MD4_loop, .-EVP_Digest_MD4_loop
	.p2align 4
	.type	EVP_Digest_MDC2_loop, @function
EVP_Digest_MDC2_loop:
.LFB1439:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movl	run(%rip), %r12d
	movq	16(%rax), %rbx
	testl	%r12d, %r12d
	je	.L365
	xorl	%r12d, %r12d
	leaq	-64(%rbp), %r13
	jmp	.L367
	.p2align 4,,10
	.p2align 3
.L378:
	movl	run(%rip), %eax
	addl	$1, %r12d
	testl	%eax, %eax
	je	.L365
	cmpl	$2147483647, %r12d
	je	.L365
.L367:
	call	EVP_mdc2@PLT
	movl	testnum(%rip), %edx
	xorl	%r9d, %r9d
	xorl	%ecx, %ecx
	movq	%rax, %r8
	movq	lengths(%rip), %rax
	movq	%rbx, %rdi
	movslq	(%rax,%rdx,4), %rsi
	movq	%r13, %rdx
	call	EVP_Digest@PLT
	testl	%eax, %eax
	jne	.L378
	movl	$-1, %r12d
.L365:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L379
	addq	$40, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L379:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1439:
	.size	EVP_Digest_MDC2_loop, .-EVP_Digest_MDC2_loop
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"Failure in the job\n"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"Too many fds in ASYNC_WAIT_CTX\n"
	.align 8
.LC2:
	.string	"Error: max_fd (%d) must be smaller than FD_SETSIZE (%d). Decrease the value of async_jobs\n"
	.section	.rodata.str1.1
.LC3:
	.string	"Failure in the select\n"
	.text
	.p2align 4
	.type	run_benchmark, @function
run_benchmark:
.LFB1472:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$232, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -232(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$0, -216(%rbp)
	movl	$0, -212(%rbp)
	movq	$0, -208(%rbp)
	testl	%edi, %edi
	je	.L381
	jle	.L447
	leaq	-216(%rbp), %r15
	leal	-1(%rdi), %eax
	xorl	%ebx, %ebx
	movl	$0, -236(%rbp)
	movl	$0, -252(%rbp)
	leaq	3(%rax,%rax,2), %r13
	leaq	-200(%rbp), %r12
	movq	%r15, -264(%rbp)
	salq	$8, %r13
	.p2align 4,,10
	.p2align 3
.L390:
	movq	-232(%rbp), %rdi
	movq	%r12, %r8
	movq	%r14, %rcx
	movq	%r15, %rdx
	movl	$8, %r9d
	addq	%rbx, %rdi
	movq	8(%rdi), %rsi
	movq	%rdi, -200(%rbp)
	call	ASYNC_start_job@PLT
	cmpl	$2, %eax
	je	.L384
	jg	.L385
	cmpl	$1, %eax
	ja	.L386
	movq	bio_err(%rip), %rdi
	leaq	.LC0(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
.L389:
	movl	-236(%rbp), %eax
	testl	%eax, %eax
	je	.L415
	movl	$1, -240(%rbp)
.L414:
	leaq	-192(%rbp), %rax
	leaq	-208(%rbp), %rbx
	movq	%rax, -248(%rbp)
	.p2align 4,,10
	.p2align 3
.L413:
	movl	$16, %ecx
	movq	-248(%rbp), %rdi
	xorl	%eax, %eax
#APP
# 1284 "../deps/openssl/openssl/apps/speed.c" 1
	cld; rep; stosq
# 0 "" 2
#NO_APP
	xorl	%r15d, %r15d
	xorl	%r12d, %r12d
	.p2align 4,,10
	.p2align 3
.L395:
	movq	-232(%rbp), %rax
	addq	%r15, %rax
	cmpq	$0, (%rax)
	je	.L391
	movq	8(%rax), %rdi
	xorl	%esi, %esi
	movq	%rbx, %rdx
	call	ASYNC_WAIT_CTX_get_all_fds@PLT
	testl	%eax, %eax
	je	.L392
	cmpq	$1, -208(%rbp)
	ja	.L392
	movq	-232(%rbp), %rax
	movq	%rbx, %rdx
	leaq	-212(%rbp), %rsi
	movq	8(%rax,%r15), %rdi
	call	ASYNC_WAIT_CTX_get_all_fds@PLT
	movslq	-212(%rbp), %rdi
	call	__fdelt_chk@PLT
	movq	%rax, %r8
	movl	-212(%rbp), %eax
	cltd
	shrl	$26, %edx
	leal	(%rax,%rdx), %ecx
	andl	$63, %ecx
	subl	%edx, %ecx
	movl	$1, %edx
	salq	%cl, %rdx
	orq	%rdx, -192(%rbp,%r8,8)
	cmpl	%eax, %r12d
	cmovl	%eax, %r12d
.L391:
	addq	$768, %r15
	cmpq	%r15, %r13
	jne	.L395
	cmpl	$1023, %r12d
	jg	.L448
.L396:
	movq	-248(%rbp), %rsi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	leal	1(%r12), %edi
	call	select@PLT
	cmpl	$-1, %eax
	je	.L449
	testl	%eax, %eax
	je	.L413
	xorl	%r12d, %r12d
	movq	%r12, %r15
	movq	-264(%rbp), %r12
	jmp	.L410
	.p2align 4,,10
	.p2align 3
.L450:
	testl	%eax, %eax
	js	.L400
	movq	-232(%rbp), %rax
	movq	bio_err(%rip), %rdi
	leaq	.LC0(%rip), %rsi
	subl	$1, -236(%rbp)
	movq	$0, (%rax,%r15)
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	movl	$1, -240(%rbp)
	.p2align 4,,10
	.p2align 3
.L400:
	addq	$768, %r15
	cmpq	%r15, %r13
	je	.L403
.L410:
	movq	-232(%rbp), %rax
	addq	%r15, %rax
	cmpq	$0, (%rax)
	je	.L400
	movq	8(%rax), %rdi
	xorl	%esi, %esi
	movq	%rbx, %rdx
	call	ASYNC_WAIT_CTX_get_all_fds@PLT
	testl	%eax, %eax
	je	.L401
	cmpq	$1, -208(%rbp)
	ja	.L401
	movq	-232(%rbp), %rax
	leaq	-212(%rbp), %rsi
	movq	%rbx, %rdx
	movq	8(%rax,%r15), %rdi
	call	ASYNC_WAIT_CTX_get_all_fds@PLT
	cmpq	$1, -208(%rbp)
	jne	.L408
	movslq	-212(%rbp), %rdi
	call	__fdelt_chk@PLT
	movq	%rax, %r8
	movl	-212(%rbp), %eax
	cltd
	shrl	$26, %edx
	leal	(%rax,%rdx), %ecx
	movl	$1, %eax
	andl	$63, %ecx
	subl	%edx, %ecx
	salq	%cl, %rax
	testq	%rax, -192(%rbp,%r8,8)
	je	.L400
.L408:
	movq	-232(%rbp), %rdi
	movl	$768, %r9d
	movq	%r14, %rcx
	movq	%r12, %rdx
	addq	%r15, %rdi
	movq	8(%rdi), %rsi
	movq	%rdi, %r8
	call	ASYNC_start_job@PLT
	cmpl	$1, %eax
	jle	.L450
	cmpl	$3, %eax
	jne	.L400
	movl	-216(%rbp), %eax
	cmpl	$-1, %eax
	je	.L417
	addl	%eax, -252(%rbp)
.L409:
	movq	-232(%rbp), %rax
	subl	$1, -236(%rbp)
	movq	$0, (%rax,%r15)
	addq	$768, %r15
	cmpq	%r15, %r13
	jne	.L410
	.p2align 4,,10
	.p2align 3
.L403:
	movl	-236(%rbp), %ecx
	testl	%ecx, %ecx
	jg	.L413
.L452:
	movl	-240(%rbp), %edx
	testl	%edx, %edx
	jne	.L415
	.p2align 4,,10
	.p2align 3
.L380:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L451
	movl	-252(%rbp), %eax
	addq	$232, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L385:
	.cfi_restore_state
	cmpl	$3, %eax
	jne	.L386
	movl	-216(%rbp), %eax
	cmpl	$-1, %eax
	je	.L389
	addl	%eax, -252(%rbp)
	.p2align 4,,10
	.p2align 3
.L386:
	addq	$768, %rbx
	cmpq	%r13, %rbx
	jne	.L390
	cmpl	$0, -236(%rbp)
	je	.L380
	movl	$0, -240(%rbp)
	jmp	.L414
	.p2align 4,,10
	.p2align 3
.L392:
	movq	bio_err(%rip), %rdi
	leaq	.LC1(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	movl	$1, -240(%rbp)
	cmpl	$1023, %r12d
	jle	.L396
.L448:
	movq	bio_err(%rip), %rdi
	movl	$1024, %ecx
	movl	%r12d, %edx
	xorl	%eax, %eax
	leaq	.LC2(%rip), %rsi
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	jmp	.L415
	.p2align 4,,10
	.p2align 3
.L449:
	call	__errno_location@PLT
	cmpl	$4, (%rax)
	je	.L413
	movq	bio_err(%rip), %rdi
	leaq	.LC3(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
.L415:
	movl	$-1, -252(%rbp)
	jmp	.L380
	.p2align 4,,10
	.p2align 3
.L417:
	movl	$1, -240(%rbp)
	jmp	.L409
	.p2align 4,,10
	.p2align 3
.L384:
	addl	$1, -236(%rbp)
	jmp	.L386
	.p2align 4,,10
	.p2align 3
.L401:
	movq	bio_err(%rip), %rdi
	leaq	.LC1(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	movl	-236(%rbp), %ecx
	movl	$1, -240(%rbp)
	testl	%ecx, %ecx
	jg	.L413
	jmp	.L452
.L381:
	leaq	-232(%rbp), %rdi
	call	*%rsi
	movl	%eax, -252(%rbp)
	jmp	.L380
.L447:
	movl	$0, -252(%rbp)
	jmp	.L380
.L451:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1472:
	.size	run_benchmark, .-run_benchmark
	.section	.rodata.str1.1
.LC4:
	.string	"RSA sign failure\n"
	.text
	.p2align 4
	.type	RSA_sign_loop, @function
RSA_sign_loop:
.LFB1463:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rbx
	movl	run(%rip), %r12d
	movq	16(%rbx), %r13
	movq	24(%rbx), %r14
	leaq	56(%rbx), %r15
	addq	$72, %rbx
	testl	%r12d, %r12d
	je	.L453
	xorl	%r12d, %r12d
	jmp	.L456
	.p2align 4,,10
	.p2align 3
.L455:
	movl	run(%rip), %eax
	addl	$1, %r12d
	testl	%eax, %eax
	je	.L453
	cmpl	$2147483647, %r12d
	je	.L453
.L456:
	movl	testnum(%rip), %eax
	movq	%r15, %r8
	movq	%r14, %rcx
	movq	%r13, %rsi
	movl	$36, %edx
	movl	$114, %edi
	movq	(%rbx,%rax,8), %r9
	call	RSA_sign@PLT
	testl	%eax, %eax
	jne	.L455
	movq	bio_err(%rip), %rdi
	leaq	.LC4(%rip), %rsi
	movl	$-1, %r12d
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
.L453:
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1463:
	.size	RSA_sign_loop, .-RSA_sign_loop
	.section	.rodata.str1.1
.LC5:
	.string	"RSA verify failure\n"
	.text
	.p2align 4
	.type	RSA_verify_loop, @function
RSA_verify_loop:
.LFB1464:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rbx
	movl	run(%rip), %r12d
	movq	16(%rbx), %r13
	movq	24(%rbx), %r14
	addq	$72, %rbx
	movl	-16(%rbx), %r15d
	testl	%r12d, %r12d
	je	.L465
	xorl	%r12d, %r12d
	jmp	.L468
	.p2align 4,,10
	.p2align 3
.L467:
	movl	run(%rip), %eax
	addl	$1, %r12d
	testl	%eax, %eax
	je	.L465
	cmpl	$2147483647, %r12d
	je	.L465
.L468:
	movl	testnum(%rip), %eax
	movl	%r15d, %r8d
	movq	%r14, %rcx
	movq	%r13, %rsi
	movl	$36, %edx
	movl	$114, %edi
	movq	(%rbx,%rax,8), %r9
	call	RSA_verify@PLT
	testl	%eax, %eax
	jg	.L467
	movq	bio_err(%rip), %rdi
	leaq	.LC5(%rip), %rsi
	xorl	%eax, %eax
	movl	$-1, %r12d
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
.L465:
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1464:
	.size	RSA_verify_loop, .-RSA_verify_loop
	.section	.rodata.str1.1
.LC6:
	.string	"DSA sign failure\n"
	.text
	.p2align 4
	.type	DSA_sign_loop, @function
DSA_sign_loop:
.LFB1465:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %r15
	movl	run(%rip), %r12d
	movq	16(%r15), %rbx
	movq	24(%r15), %r13
	leaq	128(%r15), %r14
	addq	$56, %r15
	testl	%r12d, %r12d
	je	.L477
	xorl	%r12d, %r12d
	jmp	.L480
	.p2align 4,,10
	.p2align 3
.L479:
	movl	run(%rip), %eax
	addl	$1, %r12d
	testl	%eax, %eax
	je	.L477
	cmpl	$2147483647, %r12d
	je	.L477
.L480:
	movl	testnum(%rip), %eax
	xorl	%edi, %edi
	movq	%r15, %r8
	movq	%r13, %rcx
	movl	$20, %edx
	movq	%rbx, %rsi
	movq	(%r14,%rax,8), %r9
	call	DSA_sign@PLT
	testl	%eax, %eax
	jne	.L479
	movq	bio_err(%rip), %rdi
	leaq	.LC6(%rip), %rsi
	movl	$-1, %r12d
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
.L477:
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1465:
	.size	DSA_sign_loop, .-DSA_sign_loop
	.section	.rodata.str1.1
.LC7:
	.string	"DSA verify failure\n"
	.text
	.p2align 4
	.type	DSA_verify_loop, @function
DSA_verify_loop:
.LFB1466:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rax
	movl	run(%rip), %r12d
	movq	16(%rax), %rbx
	movq	24(%rax), %r13
	movl	56(%rax), %r15d
	testl	%r12d, %r12d
	je	.L489
	leaq	128(%rax), %r14
	xorl	%r12d, %r12d
	jmp	.L492
	.p2align 4,,10
	.p2align 3
.L491:
	movl	run(%rip), %eax
	addl	$1, %r12d
	testl	%eax, %eax
	je	.L489
	cmpl	$2147483647, %r12d
	je	.L489
.L492:
	movl	testnum(%rip), %eax
	xorl	%edi, %edi
	movl	%r15d, %r8d
	movq	%r13, %rcx
	movl	$20, %edx
	movq	%rbx, %rsi
	movq	(%r14,%rax,8), %r9
	call	DSA_verify@PLT
	testl	%eax, %eax
	jg	.L491
	movq	bio_err(%rip), %rdi
	leaq	.LC7(%rip), %rsi
	xorl	%eax, %eax
	movl	$-1, %r12d
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
.L489:
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1466:
	.size	DSA_verify_loop, .-DSA_verify_loop
	.section	.rodata.str1.1
.LC8:
	.string	"ECDSA sign failure\n"
	.text
	.p2align 4
	.type	ECDSA_sign_loop, @function
ECDSA_sign_loop:
.LFB1467:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %r15
	movl	run(%rip), %r12d
	movq	16(%r15), %rbx
	movq	24(%r15), %r14
	leaq	152(%r15), %r13
	addq	$56, %r15
	testl	%r12d, %r12d
	je	.L501
	xorl	%r12d, %r12d
	jmp	.L504
	.p2align 4,,10
	.p2align 3
.L503:
	movl	run(%rip), %eax
	addl	$1, %r12d
	testl	%eax, %eax
	je	.L501
	cmpl	$2147483647, %r12d
	je	.L501
.L504:
	movl	testnum(%rip), %eax
	xorl	%edi, %edi
	movq	%r15, %r8
	movq	%r14, %rcx
	movl	$20, %edx
	movq	%rbx, %rsi
	movq	0(%r13,%rax,8), %r9
	call	ECDSA_sign@PLT
	testl	%eax, %eax
	jne	.L503
	movq	bio_err(%rip), %rdi
	leaq	.LC8(%rip), %rsi
	movl	$-1, %r12d
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
.L501:
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1467:
	.size	ECDSA_sign_loop, .-ECDSA_sign_loop
	.section	.rodata.str1.1
.LC9:
	.string	"ECDSA verify failure\n"
	.text
	.p2align 4
	.type	ECDSA_verify_loop, @function
ECDSA_verify_loop:
.LFB1468:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rax
	movl	run(%rip), %r12d
	movq	16(%rax), %rbx
	movq	24(%rax), %r14
	movl	56(%rax), %r15d
	testl	%r12d, %r12d
	je	.L513
	leaq	152(%rax), %r13
	xorl	%r12d, %r12d
	jmp	.L516
	.p2align 4,,10
	.p2align 3
.L515:
	movl	run(%rip), %eax
	addl	$1, %r12d
	testl	%eax, %eax
	je	.L513
	cmpl	$2147483647, %r12d
	je	.L513
.L516:
	movl	testnum(%rip), %eax
	xorl	%edi, %edi
	movl	%r15d, %r8d
	movq	%r14, %rcx
	movl	$20, %edx
	movq	%rbx, %rsi
	movq	0(%r13,%rax,8), %r9
	call	ECDSA_verify@PLT
	cmpl	$1, %eax
	je	.L515
	movq	bio_err(%rip), %rdi
	leaq	.LC9(%rip), %rsi
	xorl	%eax, %eax
	movl	$-1, %r12d
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
.L513:
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1468:
	.size	ECDSA_verify_loop, .-ECDSA_verify_loop
	.section	.rodata.str1.1
.LC10:
	.string	"EdDSA sign failure\n"
	.text
	.p2align 4
	.type	EdDSA_sign_loop, @function
EdDSA_sign_loop:
.LFB1470:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rbx
	movl	run(%rip), %r12d
	movq	16(%rbx), %r13
	movq	24(%rbx), %r15
	leaq	520(%rbx), %r14
	addq	$64, %rbx
	testl	%r12d, %r12d
	je	.L525
	xorl	%r12d, %r12d
	jmp	.L528
	.p2align 4,,10
	.p2align 3
.L527:
	movl	run(%rip), %eax
	addl	$1, %r12d
	testl	%eax, %eax
	je	.L525
	cmpl	$2147483647, %r12d
	je	.L525
.L528:
	movl	testnum(%rip), %eax
	movq	%r13, %rcx
	movq	%rbx, %rdx
	movq	%r15, %rsi
	movl	$20, %r8d
	movq	(%r14,%rax,8), %rdi
	call	EVP_DigestSign@PLT
	testl	%eax, %eax
	jne	.L527
	movq	bio_err(%rip), %rdi
	leaq	.LC10(%rip), %rsi
	movl	$-1, %r12d
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
.L525:
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1470:
	.size	EdDSA_sign_loop, .-EdDSA_sign_loop
	.section	.rodata.str1.1
.LC11:
	.string	"EdDSA verify failure\n"
	.text
	.p2align 4
	.type	EdDSA_verify_loop, @function
EdDSA_verify_loop:
.LFB1471:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rax
	movl	run(%rip), %r12d
	movq	16(%rax), %rbx
	movq	24(%rax), %r14
	movq	64(%rax), %r15
	testl	%r12d, %r12d
	je	.L537
	leaq	520(%rax), %r13
	xorl	%r12d, %r12d
	jmp	.L540
	.p2align 4,,10
	.p2align 3
.L539:
	movl	run(%rip), %eax
	addl	$1, %r12d
	testl	%eax, %eax
	je	.L537
	cmpl	$2147483647, %r12d
	je	.L537
.L540:
	movl	testnum(%rip), %eax
	movq	%rbx, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movl	$20, %r8d
	movq	0(%r13,%rax,8), %rdi
	call	EVP_DigestVerify@PLT
	cmpl	$1, %eax
	je	.L539
	movq	bio_err(%rip), %rdi
	leaq	.LC11(%rip), %rsi
	xorl	%eax, %eax
	movl	$-1, %r12d
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
.L537:
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1471:
	.size	EdDSA_verify_loop, .-EdDSA_verify_loop
	.section	.rodata.str1.1
.LC12:
	.string	"+DTP:%d:%s:%s:%d\n"
	.section	.rodata.str1.8
	.align 8
.LC13:
	.string	"Doing %u bits %s %s's for %ds: "
	.text
	.p2align 4
	.type	pkey_print_message.isra.0, @function
pkey_print_message.isra.0:
.LFB1489:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	leaq	.LC13(%rip), %rsi
	movl	%ecx, %r9d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%ecx, %r12d
	movq	%rdi, %rcx
	subq	$8, %rsp
	movl	mr(%rip), %eax
	movq	bio_err(%rip), %rdi
	testl	%eax, %eax
	leaq	.LC12(%rip), %rax
	cmovne	%rax, %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	call	BIO_ctrl@PLT
	movl	%r12d, %edi
	movl	$1, run(%rip)
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	alarm@PLT
	.cfi_endproc
.LFE1489:
	.size	pkey_print_message.isra.0, .-pkey_print_message.isra.0
	.p2align 4
	.globl	get_dsa
	.type	get_dsa, @function
get_dsa:
.LFB1435:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	cmpl	$1024, %edi
	je	.L559
	cmpl	$2048, %edi
	je	.L560
	xorl	%r8d, %r8d
	cmpl	$512, %edi
	je	.L572
.L553:
	addq	$40, %rsp
	movq	%r8, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L560:
	.cfi_restore_state
	leaq	dsa2048_q(%rip), %rax
	movl	$256, %r13d
	movl	$256, %ebx
	movq	%rax, -64(%rbp)
	leaq	dsa2048_g(%rip), %rax
	leaq	dsa2048_p(%rip), %r14
	movq	%rax, -56(%rbp)
	leaq	dsa2048_pub(%rip), %r15
	leaq	dsa2048_priv(%rip), %r12
.L554:
	call	DSA_new@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L553
	xorl	%edx, %edx
	movq	%r12, %rdi
	movl	$20, %esi
	movq	%rax, -72(%rbp)
	call	BN_bin2bn@PLT
	xorl	%edx, %edx
	movl	%ebx, %esi
	movq	%r15, %rdi
	movq	%rax, %r12
	call	BN_bin2bn@PLT
	xorl	%edx, %edx
	movl	%r13d, %esi
	movq	%r14, %rdi
	movq	%rax, %r15
	call	BN_bin2bn@PLT
	movq	-64(%rbp), %rdi
	xorl	%edx, %edx
	movl	$20, %esi
	movq	%rax, %r14
	call	BN_bin2bn@PLT
	movq	-56(%rbp), %rdi
	xorl	%edx, %edx
	movl	%r13d, %esi
	movq	%rax, %rbx
	call	BN_bin2bn@PLT
	testq	%r12, %r12
	movq	-72(%rbp), %r8
	movq	%rax, %r13
	je	.L558
	testq	%r15, %r15
	je	.L558
	testq	%r14, %r14
	sete	%dl
	testq	%rbx, %rbx
	sete	%al
	orb	%al, %dl
	jne	.L558
	testq	%r13, %r13
	je	.L558
	movq	%r8, %rdi
	movq	%r13, %rcx
	movq	%rbx, %rdx
	movq	%r14, %rsi
	movq	%r8, -56(%rbp)
	call	DSA_set0_pqg@PLT
	movq	-56(%rbp), %r8
	testl	%eax, %eax
	je	.L558
	movq	%r8, %rdi
	movq	%r12, %rdx
	movq	%r15, %rsi
	call	DSA_set0_key@PLT
	movq	-56(%rbp), %r8
	testl	%eax, %eax
	jne	.L553
	.p2align 4,,10
	.p2align 3
.L558:
	movq	%r8, %rdi
	call	DSA_free@PLT
	movq	%r12, %rdi
	call	BN_free@PLT
	movq	%r15, %rdi
	call	BN_free@PLT
	movq	%r14, %rdi
	call	BN_free@PLT
	movq	%rbx, %rdi
	call	BN_free@PLT
	movq	%r13, %rdi
	call	BN_free@PLT
	xorl	%r8d, %r8d
	jmp	.L553
	.p2align 4,,10
	.p2align 3
.L572:
	leaq	dsa512_q(%rip), %rax
	movl	$64, %r13d
	movl	$65, %ebx
	movq	%rax, -64(%rbp)
	leaq	dsa512_g(%rip), %rax
	leaq	dsa512_p(%rip), %r14
	movq	%rax, -56(%rbp)
	leaq	dsa512_pub(%rip), %r15
	leaq	dsa512_priv(%rip), %r12
	jmp	.L554
	.p2align 4,,10
	.p2align 3
.L559:
	leaq	dsa1024_q(%rip), %rax
	movl	$128, %r13d
	movl	$128, %ebx
	movq	%rax, -64(%rbp)
	leaq	dsa1024_g(%rip), %rax
	leaq	dsa1024_p(%rip), %r14
	movq	%rax, -56(%rbp)
	leaq	dsa1024_pub(%rip), %r15
	leaq	dsa1024_priv(%rip), %r12
	jmp	.L554
	.cfi_endproc
.LFE1435:
	.size	get_dsa, .-get_dsa
	.section	.rodata.str1.1
.LC14:
	.string	"+DT:%s:%d:%d\n"
	.section	.rodata.str1.8
	.align 8
.LC15:
	.string	"Doing %s for %ds on %d size blocks: "
	.section	.rodata.str1.1
.LC16:
	.string	"+R:%d:%s:%f\n"
.LC17:
	.string	"%d %s's in %.2fs\n"
.LC18:
	.string	"+R1:%ld:%d:%.2f\n"
	.section	.rodata.str1.8
	.align 8
.LC19:
	.string	"%ld %u bits private RSA's in %.2fs\n"
	.section	.rodata.str1.1
.LC20:
	.string	"+R2:%ld:%d:%.2f\n"
	.section	.rodata.str1.8
	.align 8
.LC21:
	.string	"%ld %u bits public RSA's in %.2fs\n"
	.section	.rodata.str1.1
.LC22:
	.string	"+R3:%ld:%u:%.2f\n"
	.section	.rodata.str1.8
	.align 8
.LC23:
	.string	"%ld %u bits DSA signs in %.2fs\n"
	.section	.rodata.str1.1
.LC24:
	.string	"+R4:%ld:%u:%.2f\n"
	.section	.rodata.str1.8
	.align 8
.LC25:
	.string	"%ld %u bits DSA verify in %.2fs\n"
	.section	.rodata.str1.1
.LC26:
	.string	"+R5:%ld:%u:%.2f\n"
	.section	.rodata.str1.8
	.align 8
.LC27:
	.string	"%ld %u bits ECDSA signs in %.2fs \n"
	.section	.rodata.str1.1
.LC28:
	.string	"+R6:%ld:%u:%.2f\n"
	.section	.rodata.str1.8
	.align 8
.LC29:
	.string	"%ld %u bits ECDSA verify in %.2fs\n"
	.section	.rodata.str1.1
.LC30:
	.string	"+R7:%ld:%d:%.2f\n"
	.section	.rodata.str1.8
	.align 8
.LC31:
	.string	"%ld %u-bits ECDH ops in %.2fs\n"
	.section	.rodata.str1.1
.LC32:
	.string	"+R8:%ld:%u:%s:%.2f\n"
	.section	.rodata.str1.8
	.align 8
.LC33:
	.string	"%ld %u bits %s signs in %.2fs \n"
	.section	.rodata.str1.1
.LC34:
	.string	"+R9:%ld:%u:%s:%.2f\n"
	.section	.rodata.str1.8
	.align 8
.LC35:
	.string	"%ld %u bits %s verify in %.2fs\n"
	.section	.rodata.str1.1
.LC36:
	.string	" %11.2f "
.LC37:
	.string	":%.2f"
.LC38:
	.string	"%s: Use -help for summary.\n"
	.section	.rodata.str1.8
	.align 8
.LC39:
	.string	"%s: %s is an unknown cipher or digest\n"
	.align 8
.LC40:
	.string	"%s: async_jobs specified but async not supported\n"
	.section	.rodata.str1.1
.LC41:
	.string	"%s: too many async_jobs\n"
.LC42:
	.string	"%s: Maximum offset is %d\n"
.LC43:
	.string	"des"
.LC44:
	.string	"sha"
.LC45:
	.string	"openssl"
.LC46:
	.string	"rsa"
.LC47:
	.string	"rsa512"
.LC48:
	.string	"rsa1024"
.LC49:
	.string	"rsa2048"
.LC50:
	.string	"rsa3072"
.LC51:
	.string	"rsa4096"
.LC52:
	.string	"rsa7680"
.LC53:
	.string	"rsa15360"
.LC55:
	.string	"dsa"
.LC56:
	.string	"dsa512"
.LC57:
	.string	"dsa1024"
.LC58:
	.string	"dsa2048"
.LC59:
	.string	"aes"
.LC60:
	.string	"camellia"
.LC61:
	.string	"ecdsa"
.LC62:
	.string	"ecdh"
.LC63:
	.string	"eddsa"
.LC64:
	.string	"%s: Unknown algorithm %s\n"
	.section	.rodata.str1.8
	.align 8
.LC65:
	.string	"-aead can be used only with an AEAD cipher\n"
	.section	.rodata.str1.1
.LC66:
	.string	"%s is not an AEAD cipher\n"
	.section	.rodata.str1.8
	.align 8
.LC67:
	.string	"-mb can be used only with a multi-block capable cipher\n"
	.align 8
.LC68:
	.string	"%s is not a multi-block capable\n"
	.align 8
.LC69:
	.string	"Async mode is not supported with -mb"
	.align 8
.LC70:
	.string	"Error creating the ASYNC job pool\n"
	.section	.rodata.str1.1
.LC71:
	.string	"array of loopargs"
	.section	.rodata.str1.8
	.align 8
.LC72:
	.string	"Error creating the ASYNC_WAIT_CTX\n"
	.section	.rodata.str1.1
.LC73:
	.string	"input buffer"
.LC74:
	.string	"ECDH secret a"
.LC75:
	.string	"ECDH secret b"
.LC76:
	.string	"fd buffer for do_multi"
.LC77:
	.string	"pipe failure\n"
.LC78:
	.string	"Forked child %d\n"
.LC79:
	.string	"dup failed\n"
.LC80:
	.string	"r"
	.section	.rodata.str1.8
	.align 8
.LC81:
	.string	"Don't understand line '%s' from child %d\n"
	.section	.rodata.str1.1
.LC82:
	.string	"Got: %s from %d\n"
.LC83:
	.string	"+F:"
.LC84:
	.string	"+F2:"
.LC85:
	.string	"+F3:"
.LC86:
	.string	"+F4:"
.LC87:
	.string	"+F5:"
.LC88:
	.string	"+F6:"
.LC89:
	.string	"+H:"
	.section	.rodata.str1.8
	.align 8
.LC90:
	.string	"Unknown type '%s' from child %d\n"
	.align 8
.LC91:
	.string	"You have chosen to measure elapsed time instead of user CPU time.\n"
	.align 8
.LC92:
	.string	"internal error loading RSA key number %d\n"
	.section	.rodata.str1.1
.LC93:
	.string	"EVP error!\n"
	.section	.rodata.str1.8
	.align 8
.LC94:
	.string	"HMAC malloc failure, exiting..."
	.section	.rodata.str1.1
.LC95:
	.string	"0123456789ab"
	.section	.rodata.str1.8
	.align 8
.LC96:
	.string	"Async mode is not supported with %s\n"
	.align 8
.LC97:
	.string	"Async mode is not supported, exiting..."
	.section	.rodata.str1.1
.LC98:
	.string	"multiblock input buffer"
.LC99:
	.string	"multiblock output buffer"
.LC100:
	.string	"evp_cipher key"
	.section	.rodata.str1.8
	.align 8
.LC101:
	.string	"../deps/openssl/openssl/apps/speed.c"
	.section	.rodata.str1.1
.LC102:
	.string	"evp"
.LC103:
	.string	"+H"
.LC104:
	.string	":%d"
.LC105:
	.string	"+F:%d:%s"
	.section	.rodata.str1.8
	.align 8
.LC106:
	.string	"The 'numbers' are in 1000s of bytes per second processed.\n"
	.section	.rodata.str1.1
.LC107:
	.string	"type                    "
.LC108:
	.string	"%7d bytes"
.LC109:
	.string	"%-24s"
.LC112:
	.string	" %11.2fk"
.LC113:
	.string	"\nEVP_CIPHER_CTX_new failure\n"
.LC114:
	.string	"\nEVP_CipherInit_ex failure\n"
	.section	.rodata.str1.8
	.align 8
.LC115:
	.string	"Generate multi-prime RSA key for %s\n"
	.align 8
.LC116:
	.string	"RSA sign failure.  No RSA sign will be done.\n"
	.section	.rodata.str1.1
.LC117:
	.string	"private"
	.section	.rodata.str1.8
	.align 8
.LC118:
	.string	"RSA verify failure.  No RSA verify will be done.\n"
	.section	.rodata.str1.1
.LC119:
	.string	"public"
	.section	.rodata.str1.8
	.align 8
.LC120:
	.string	"DSA sign failure.  No DSA sign will be done.\n"
	.section	.rodata.str1.1
.LC121:
	.string	"sign"
	.section	.rodata.str1.8
	.align 8
.LC122:
	.string	"DSA verify failure.  No DSA verify will be done.\n"
	.section	.rodata.str1.1
.LC123:
	.string	"verify"
	.section	.rodata.str1.8
	.align 8
.LC124:
	.string	"ECDSA verify failure.  No ECDSA verify will be done.\n"
	.align 8
.LC125:
	.string	"WARNING: the error queue contains previous unhandled errors.\n"
	.align 8
.LC126:
	.string	"Unhandled error in the error queue during ECDH init.\n"
	.section	.rodata.str1.1
.LC127:
	.string	"ECDH EC params init failure.\n"
.LC128:
	.string	"ECDH keygen failure.\n"
.LC129:
	.string	"ECDH key generation failure.\n"
.LC130:
	.string	"ECDH computation failure.\n"
	.section	.rodata.str1.8
	.align 8
.LC131:
	.string	"ECDH computations don't match.\n"
	.section	.rodata.str1.1
.LC132:
	.string	""
.LC133:
	.string	"EdDSA failure.\n"
	.section	.rodata.str1.8
	.align 8
.LC134:
	.string	"EdDSA verify failure.  No EdDSA verify will be done.\n"
	.section	.rodata.str1.1
.LC135:
	.string	"options:"
.LC136:
	.string	"%s "
.LC137:
	.string	"\n%s\n"
	.section	.rodata.str1.8
	.align 8
.LC138:
	.string	"The 'numbers' are in 1000s of bytes per second processed."
	.section	.rodata.str1.1
.LC139:
	.string	"type        "
.LC140:
	.string	"+F:%u:%s"
.LC141:
	.string	"%-13s"
.LC142:
	.string	" "
	.section	.rodata.str1.8
	.align 8
.LC143:
	.string	"%18ssign    verify    sign/s verify/s\n"
	.section	.rodata.str1.1
.LC144:
	.string	"+F2:%u:%u:%f:%f\n"
	.section	.rodata.str1.8
	.align 8
.LC146:
	.string	"rsa %4u bits %8.6fs %8.6fs %8.1f %8.1f\n"
	.section	.rodata.str1.1
.LC147:
	.string	"+F3:%u:%u:%f:%f\n"
	.section	.rodata.str1.8
	.align 8
.LC148:
	.string	"dsa %4u bits %8.6fs %8.6fs %8.1f %8.1f\n"
	.align 8
.LC149:
	.string	"%30ssign    verify    sign/s verify/s\n"
	.section	.rodata.str1.1
.LC150:
	.string	"+F4:%u:%u:%f:%f\n"
	.section	.rodata.str1.8
	.align 8
.LC151:
	.string	"%4u bits ecdsa (%s) %8.4fs %8.4fs %8.1f %8.1f\n"
	.section	.rodata.str1.1
.LC152:
	.string	"%30sop      op/s\n"
.LC153:
	.string	"+F5:%u:%u:%f:%f\n"
	.section	.rodata.str1.8
	.align 8
.LC154:
	.string	"%4u bits ecdh (%s) %8.4fs %8.1f\n"
	.section	.rodata.str1.1
.LC155:
	.string	"+F6:%u:%u:%s:%f:%f\n"
	.section	.rodata.str1.8
	.align 8
.LC156:
	.string	"%4u bits EdDSA (%s) %8.4fs %8.4fs %8.1f %8.1f\n"
	.align 8
.LC157:
	.string	"ECDSA sign failure.  No ECDSA sign will be done.\n"
	.section	.rodata.str1.1
.LC158:
	.string	"ECDSA failure.\n"
	.section	.rodata.str1.8
	.align 8
.LC159:
	.string	"EdDSA sign failure.  No EdDSA sign will be done.\n"
	.text
	.p2align 4
	.globl	speed_main
	.type	speed_main, @function
speed_main:
.LFB1473:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$3736, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	$15, %ecx
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	leaq	speed_options(%rip), %rdx
	xorl	%r12d, %r12d
	leaq	.L579(%rip), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	%edi, %r8d
	leaq	-7280(%rbp), %rdi
	movl	$0, -7604(%rbp)
	movq	%rdi, -7720(%rbp)
	movl	$0, -7600(%rbp)
	movq	$0, -7488(%rbp)
	movl	$0, -7480(%rbp)
	movl	$2, -7596(%rbp)
	rep stosq
	movl	$11, %ecx
	movq	$0, -7516(%rbp)
	movl	$0, -7508(%rbp)
	movq	$0, -7524(%rbp)
	movaps	%xmm0, -7504(%rbp)
	movl	$0, (%rdi)
	leaq	-7472(%rbp), %rdi
	movq	%rdi, -7728(%rbp)
	rep stosq
	leaq	-7376(%rbp), %rdi
	movl	$12, %ecx
	movq	%rdi, -7736(%rbp)
	rep stosq
	movl	%r8d, %edi
	call	opt_init@PLT
	movl	$10, -7664(%rbp)
	movq	%rax, -7624(%rbp)
	movl	$3, -7648(%rbp)
	movl	$0, -7696(%rbp)
	movl	$0, -7628(%rbp)
	movl	$6, -7640(%rbp)
	movq	$0, -7656(%rbp)
	movq	$0, -7680(%rbp)
.L574:
	call	opt_next@PLT
	movl	%eax, %edi
	testl	%eax, %eax
	je	.L1884
.L601:
	cmpl	$10, %edi
	jg	.L575
	cmpl	$-1, %edi
	jl	.L574
	addl	$1, %edi
	cmpl	$11, %edi
	ja	.L574
	leaq	.L585(%rip), %rdx
	movslq	(%rdx,%rdi,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L585:
	.long	.L595-.L585
	.long	.L574-.L585
	.long	.L594-.L585
	.long	.L593-.L585
	.long	.L592-.L585
	.long	.L591-.L585
	.long	.L590-.L585
	.long	.L589-.L585
	.long	.L588-.L585
	.long	.L587-.L585
	.long	.L586-.L585
	.long	.L584-.L585
	.text
.L587:
	call	opt_next@PLT
	movl	$1, %r12d
	movl	%eax, %edi
	testl	%eax, %eax
	jne	.L601
.L1884:
	movl	%eax, %r15d
	call	opt_num_rest@PLT
	movl	%eax, -7752(%rbp)
	call	opt_rest@PLT
	movq	eddsa_choices(%rip), %rcx
	movq	(%rax), %rbx
	movq	%rcx, -7672(%rbp)
	movq	16+eddsa_choices(%rip), %rcx
	movq	%rcx, -7688(%rbp)
	testq	%rbx, %rbx
	je	.L627
	movl	%r12d, -7704(%rbp)
	movq	%rbx, %r12
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L626:
	leaq	doit_choices(%rip), %r14
	.p2align 4,,10
	.p2align 3
.L607:
	movq	(%r14), %rsi
	movq	%r12, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L1885
	addq	$16, %r14
	leaq	544+doit_choices(%rip), %rax
	cmpq	%rax, %r14
	jne	.L607
	movl	$4, %ecx
	movq	%r12, %rsi
	leaq	.LC43(%rip), %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L1886
	movl	$4, %ecx
	leaq	.LC44(%rip), %rdi
	movq	%r12, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L1887
	movl	$8, %ecx
	leaq	.LC45(%rip), %rdi
	movq	%r12, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L606
	movl	$4, %ecx
	leaq	.LC46(%rip), %rdi
	movq	%r12, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L611
	movl	$7, %ecx
	leaq	.LC47(%rip), %rdi
	movq	%r12, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L1208
	movl	$8, %ecx
	leaq	.LC48(%rip), %rdi
	movq	%r12, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L1209
	movl	$8, %ecx
	leaq	.LC49(%rip), %rdi
	movq	%r12, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L1210
	movl	$8, %ecx
	leaq	.LC50(%rip), %rdi
	movq	%r12, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L1211
	movl	$8, %ecx
	leaq	.LC51(%rip), %rdi
	movq	%r12, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L1212
	movl	$8, %ecx
	leaq	.LC52(%rip), %rdi
	movq	%r12, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L1213
	movl	$9, %ecx
	leaq	.LC53(%rip), %rdi
	movq	%r12, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L1888
	movl	$4, %ecx
	leaq	.LC55(%rip), %rdi
	movq	%r12, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L1889
	movabsq	$4294967297, %rax
	movl	$1, -7508(%rbp)
	movq	%rax, -7516(%rbp)
	.p2align 4,,10
	.p2align 3
.L606:
	movq	8(%rbx), %r12
	addq	$8, %rbx
	testq	%r12, %r12
	jne	.L626
	movl	-7704(%rbp), %r12d
.L627:
	testl	%r13d, %r13d
	je	.L1890
	cmpq	$0, -7656(%rbp)
	je	.L1891
	movq	-7656(%rbp), %rdi
	call	EVP_CIPHER_flags@PLT
	testl	$2097152, %eax
	je	.L1892
	testl	%r12d, %r12d
	jne	.L631
.L630:
	movl	-7628(%rbp), %eax
	testl	%eax, %eax
	je	.L633
	movl	%eax, %edi
	movq	%rdi, %rsi
	call	ASYNC_init_thread@PLT
	movl	%eax, -7672(%rbp)
	testl	%eax, %eax
	je	.L1893
.L634:
	movl	-7628(%rbp), %eax
	movl	$1, %ebx
	leaq	.LC71(%rip), %rsi
	testl	%eax, %eax
	cmovne	%rax, %rbx
	leal	(%rbx,%rbx,2), %edi
	sall	$8, %edi
	call	app_malloc@PLT
	xorl	%esi, %esi
	movq	%rax, %rcx
	leaq	(%rbx,%rbx,2), %rax
	movq	%rax, %r14
	movq	%rcx, %rdi
	movq	%rcx, -7624(%rbp)
	salq	$8, %r14
	movq	%r14, %rdx
	movq	%r14, -7688(%rbp)
	call	memset@PLT
	movl	-7640(%rbp), %eax
	movq	-7624(%rbp), %rcx
	movl	%r12d, -7744(%rbp)
	leal	-1(%rax), %ebx
	leaq	0(,%rbx,4), %rax
	leaq	8(%rcx), %rbx
	movq	%rax, -7704(%rbp)
	leaq	8(%rcx,%r14), %rax
	movl	$36, %r14d
	movq	%rbx, -7760(%rbp)
	movq	%rax, -7712(%rbp)
.L637:
	movl	-7628(%rbp), %r12d
	testl	%r12d, %r12d
	je	.L635
	call	ASYNC_WAIT_CTX_new@PLT
	movq	%rax, (%rbx)
	testq	%rax, %rax
	je	.L1894
.L635:
	movq	-7704(%rbp), %rcx
	movq	lengths(%rip), %rax
	leaq	.LC73(%rip), %rsi
	movl	(%rax,%rcx), %eax
	cmpl	$36, %eax
	cmovl	%r14d, %eax
	addq	$768, %rbx
	leal	64(%rax), %r12d
	movl	%r12d, %edi
	call	app_malloc@PLT
	movl	%r12d, %edi
	leaq	.LC73(%rip), %rsi
	movslq	%r12d, %r12
	movq	%rax, -744(%rbx)
	call	app_malloc@PLT
	movq	-744(%rbx), %rdi
	movq	%r12, %rdx
	xorl	%esi, %esi
	movq	%rax, -736(%rbx)
	call	memset@PLT
	movq	-736(%rbx), %rdi
	movq	%r12, %rdx
	xorl	%esi, %esi
	call	memset@PLT
	movslq	-7604(%rbp), %rax
	movl	$256, %edi
	movdqu	-744(%rbx), %xmm5
	leaq	.LC74(%rip), %rsi
	movq	%rax, %xmm0
	punpcklqdq	%xmm0, %xmm0
	paddq	%xmm5, %xmm0
	movups	%xmm0, -760(%rbx)
	call	app_malloc@PLT
	leaq	.LC75(%rip), %rsi
	movl	$256, %edi
	movq	%rax, -240(%rbx)
	call	app_malloc@PLT
	movq	%rax, -232(%rbx)
	cmpq	%rbx, -7712(%rbp)
	jne	.L637
	movl	-7696(%rbp), %ebx
	movl	-7744(%rbp), %r12d
	testl	%ebx, %ebx
	je	.L638
	leal	0(,%rbx,4), %edi
	leaq	.LC76(%rip), %rsi
	call	app_malloc@PLT
	leal	-1(%rbx), %ecx
	movq	%rax, -7696(%rbp)
	movl	%ebx, %eax
	xorl	%ebx, %ebx
	movq	%rcx, -7704(%rbp)
	leaq	-5536(%rbp), %rcx
	movq	%rcx, -7744(%rbp)
	testl	%eax, %eax
	jg	.L644
	jmp	.L791
	.p2align 4,,10
	.p2align 3
.L1897:
	movl	-5532(%rbp), %edi
	call	close@PLT
	movl	-5536(%rbp), %eax
	movl	%ebx, %edx
	movq	-7696(%rbp), %rcx
	leaq	.LC78(%rip), %rsi
	movl	$1, %edi
	movl	%eax, (%rcx,%rbx,4)
	xorl	%eax, %eax
	call	__printf_chk@PLT
	leaq	1(%rbx), %rax
	cmpq	%rbx, -7704(%rbp)
	je	.L1895
	movq	%rax, %rbx
.L644:
	movq	-7744(%rbp), %rdi
	call	pipe@PLT
	cmpl	$-1, %eax
	je	.L1896
	movq	stdout(%rip), %rdi
	call	fflush@PLT
	movq	bio_err(%rip), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	call	BIO_ctrl@PLT
	call	fork@PLT
	testl	%eax, %eax
	jne	.L1897
	movl	-5536(%rbp), %edi
	call	close@PLT
	movl	$1, %edi
	call	close@PLT
	movl	-5532(%rbp), %edi
	call	dup@PLT
	cmpl	$-1, %eax
	je	.L1898
	movl	-5532(%rbp), %edi
	call	close@PLT
	movq	-7696(%rbp), %rdi
	movl	$1, mr(%rip)
	movl	$0, usertime(%rip)
	call	free@PLT
.L638:
	movq	-7680(%rbp), %rdi
	xorl	%esi, %esi
	call	setup_engine@PLT
	movq	%rax, -7696(%rbp)
	movl	-7752(%rbp), %eax
	orl	-7192(%rbp), %eax
	je	.L1796
	movdqa	.LC54(%rip), %xmm1
.L792:
	pxor	%xmm2, %xmm2
	pcmpeqd	%xmm3, %xmm3
	movl	-7168(%rbp), %ecx
	movdqa	%xmm2, %xmm0
	pcmpeqd	-7280(%rbp), %xmm0
	cmpl	$1, %ecx
	movl	%ecx, -7840(%rbp)
	movl	-7164(%rbp), %ecx
	movl	%ecx, -7836(%rbp)
	pandn	%xmm1, %xmm0
	movdqa	%xmm2, %xmm1
	pcmpeqd	-7264(%rbp), %xmm1
	pandn	%xmm3, %xmm1
	psubd	%xmm1, %xmm0
	movdqa	%xmm2, %xmm1
	pcmpeqd	-7248(%rbp), %xmm1
	pandn	%xmm3, %xmm1
	psubd	%xmm1, %xmm0
	movdqa	%xmm2, %xmm1
	pcmpeqd	-7232(%rbp), %xmm1
	pandn	%xmm3, %xmm1
	psubd	%xmm1, %xmm0
	movdqa	%xmm2, %xmm1
	pcmpeqd	-7216(%rbp), %xmm1
	pandn	%xmm3, %xmm1
	psubd	%xmm1, %xmm0
	movdqa	%xmm2, %xmm1
	pcmpeqd	-7200(%rbp), %xmm1
	pcmpeqd	-7184(%rbp), %xmm2
	pandn	%xmm3, %xmm1
	psubd	%xmm1, %xmm0
	movdqa	%xmm2, %xmm1
	pandn	%xmm3, %xmm1
	psubd	%xmm1, %xmm0
	movdqa	%xmm0, %xmm1
	psrldq	$8, %xmm1
	paddd	%xmm1, %xmm0
	movdqa	%xmm0, %xmm1
	psrldq	$4, %xmm1
	paddd	%xmm1, %xmm0
	movd	%xmm0, %eax
	sbbl	$-1, %eax
	cmpl	$1, %ecx
	movl	-7160(%rbp), %ecx
	sbbl	$-1, %eax
	cmpl	$1, %ecx
	movl	%ecx, -7632(%rbp)
	sbbl	$-1, %eax
	movl	%eax, -7752(%rbp)
	movl	usertime(%rip), %eax
	orl	mr(%rip), %eax
	je	.L1899
.L800:
	movq	-7624(%rbp), %rcx
	movq	-7688(%rbp), %rax
	movl	%r13d, -7744(%rbp)
	leaq	rsa_data.27420(%rip), %r8
	movl	%r12d, -7704(%rbp)
	addq	%rcx, %rax
	movq	%rcx, %r12
	movq	%rax, -7680(%rbp)
.L806:
	cmpl	$2, -7596(%rbp)
	jg	.L1873
	xorl	%ebx, %ebx
	leaq	rsa_data_length.27421(%rip), %r14
	leaq	-7568(%rbp), %r13
.L805:
	movq	(%r8,%rbx,8), %rax
	movslq	(%r14,%rbx,4), %rdx
	xorl	%edi, %edi
	movq	%r13, %rsi
	movq	%rax, -7568(%rbp)
	call	d2i_RSAPrivateKey@PLT
	leaq	rsa_data.27420(%rip), %r8
	testq	%rax, %rax
	movq	%rax, 72(%r12,%rbx,8)
	je	.L1900
	addq	$1, %rbx
	cmpq	$7, %rbx
	jne	.L805
	addq	$768, %r12
	cmpq	%r12, -7680(%rbp)
	jne	.L806
.L1873:
	movq	-7624(%rbp), %rcx
	movl	-7704(%rbp), %r12d
	movl	-7744(%rbp), %r13d
	leaq	128(%rcx), %rax
	movq	%rax, -7704(%rbp)
	movq	%rax, %rbx
	movq	-7688(%rbp), %rax
	leaq	128(%rcx,%rax), %r14
.L803:
	movl	$512, %edi
	addq	$768, %rbx
	call	get_dsa
	movl	$1024, %edi
	movq	%rax, -768(%rbx)
	call	get_dsa
	movl	$2048, %edi
	movq	%rax, -760(%rbx)
	call	get_dsa
	movq	%rax, -752(%rbx)
	cmpq	%rbx, %r14
	jne	.L803
	leaq	sch(%rip), %rsi
	leaq	key.27416(%rip), %rdi
	call	DES_set_key_unchecked@PLT
	leaq	sch2(%rip), %rsi
	leaq	key2.27417(%rip), %rdi
	call	DES_set_key_unchecked@PLT
	leaq	sch3(%rip), %rsi
	leaq	key3.27418(%rip), %rdi
	call	DES_set_key_unchecked@PLT
	leaq	aes_ks1(%rip), %rdx
	movl	$128, %esi
	leaq	key16.27408(%rip), %rdi
	call	AES_set_encrypt_key@PLT
	leaq	aes_ks2(%rip), %rdx
	movl	$192, %esi
	leaq	key24.27409(%rip), %rdi
	call	AES_set_encrypt_key@PLT
	leaq	aes_ks3(%rip), %rdx
	movl	$256, %esi
	leaq	key32.27410(%rip), %rdi
	call	AES_set_encrypt_key@PLT
	leaq	-6400(%rbp), %rax
	movl	$128, %esi
	leaq	key16.27408(%rip), %rdi
	movq	%rax, %rdx
	movq	%rax, -7824(%rbp)
	call	Camellia_set_key@PLT
	leaq	-6112(%rbp), %rax
	movl	$192, %esi
	leaq	ckey24.27411(%rip), %rdi
	movq	%rax, %rdx
	movq	%rax, -7856(%rbp)
	call	Camellia_set_key@PLT
	leaq	-5824(%rbp), %rax
	movl	$256, %esi
	leaq	ckey32.27412(%rip), %rdi
	movq	%rax, %rdx
	movq	%rax, -7832(%rbp)
	call	Camellia_set_key@PLT
	leaq	-6880(%rbp), %rax
	leaq	key16.27408(%rip), %rdi
	movq	%rax, %rsi
	movq	%rax, -7816(%rbp)
	call	IDEA_set_encrypt_key@PLT
	leaq	-7152(%rbp), %rax
	leaq	key16.27408(%rip), %rdi
	movq	%rax, %rsi
	movq	%rax, -7800(%rbp)
	call	SEED_set_key@PLT
	leaq	key16.27408(%rip), %rdx
	movl	$16, %esi
	leaq	rc4_ks(%rip), %rdi
	call	RC4_set_key@PLT
	movl	$128, %ecx
	movl	$16, %esi
	leaq	-6656(%rbp), %rax
	movq	%rax, %rdi
	leaq	key16.27408(%rip), %rdx
	movq	%rax, -7848(%rbp)
	call	RC2_set_key@PLT
	leaq	-5536(%rbp), %rax
	movl	$16, %esi
	leaq	key16.27408(%rip), %rdx
	movq	%rax, %rdi
	movq	%rax, -7744(%rbp)
	call	BF_set_key@PLT
	leaq	-7024(%rbp), %rax
	movl	$16, %esi
	leaq	key16.27408(%rip), %rdx
	movq	%rax, %rdi
	movq	%rax, -7808(%rbp)
	call	CAST_set_key@PLT
	leaq	alarmed(%rip), %rsi
	movl	$14, %edi
	call	signal@PLT
	movl	-7276(%rbp), %ebx
	testl	%ebx, %ebx
	je	.L815
	movl	$0, testnum(%rip)
	movq	lengths(%rip), %rcx
	xorl	%eax, %eax
.L814:
	movl	mr(%rip), %r10d
	movl	-7648(%rbp), %ebx
	leaq	.LC14(%rip), %rdx
	leaq	.LC15(%rip), %rsi
	movl	(%rcx,%rax,4), %r8d
	movq	bio_err(%rip), %rdi
	testl	%r10d, %r10d
	movl	%ebx, %ecx
	cmovne	%rdx, %rsi
	movq	8+names(%rip), %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	call	BIO_ctrl@PLT
	movl	%ebx, %edi
	movl	$1, run(%rip)
	call	alarm@PLT
	movl	usertime(%rip), %esi
	xorl	%edi, %edi
	call	app_tminterval@PLT
	movq	-7624(%rbp), %rdx
	movl	-7628(%rbp), %edi
	leaq	EVP_Digest_MDC2_loop(%rip), %rsi
	call	run_benchmark
	movl	usertime(%rip), %esi
	movl	$1, %edi
	movl	%eax, -7784(%rbp)
	call	app_tminterval@PLT
	xorl	%edi, %edi
	movsd	%xmm0, -7776(%rbp)
	call	alarm@PLT
	movl	-7784(%rbp), %edx
	movslq	testnum(%rip), %rbx
	cmpl	$-1, %edx
	je	.L1881
	movl	mr(%rip), %r9d
	leaq	.LC17(%rip), %rax
	leaq	.LC16(%rip), %rsi
	movsd	-7776(%rbp), %xmm0
	movq	8+names(%rip), %rcx
	movq	bio_err(%rip), %rdi
	movl	%edx, -7784(%rbp)
	leaq	results(%rip), %r14
	testl	%r9d, %r9d
	cmove	%rax, %rsi
	movl	$1, %eax
	call	BIO_printf@PLT
	movq	lengths(%rip), %rcx
	pxor	%xmm1, %xmm1
	movl	-7784(%rbp), %edx
	pxor	%xmm0, %xmm0
	movl	testnum(%rip), %eax
	cvtsi2sdl	(%rcx,%rbx,4), %xmm1
	cvtsi2sdl	%edx, %xmm0
	divsd	-7776(%rbp), %xmm0
	addl	$1, %eax
	movl	%eax, testnum(%rip)
	mulsd	%xmm1, %xmm0
	movsd	%xmm0, 48(%r14,%rbx,8)
	cmpl	-7640(%rbp), %eax
	jb	.L814
.L815:
	movl	-7272(%rbp), %r11d
	testl	%r11d, %r11d
	je	.L810
	movl	$0, testnum(%rip)
	movq	lengths(%rip), %rcx
	xorl	%eax, %eax
.L821:
	movl	mr(%rip), %edi
	movl	-7648(%rbp), %ebx
	leaq	.LC14(%rip), %rdx
	leaq	.LC15(%rip), %rsi
	movl	(%rcx,%rax,4), %r8d
	testl	%edi, %edi
	movq	bio_err(%rip), %rdi
	movl	%ebx, %ecx
	cmovne	%rdx, %rsi
	movq	16+names(%rip), %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	call	BIO_ctrl@PLT
	movl	%ebx, %edi
	movl	$1, run(%rip)
	call	alarm@PLT
	movl	usertime(%rip), %esi
	xorl	%edi, %edi
	call	app_tminterval@PLT
	movq	-7624(%rbp), %rdx
	movl	-7628(%rbp), %edi
	leaq	EVP_Digest_MD4_loop(%rip), %rsi
	call	run_benchmark
	movl	usertime(%rip), %esi
	movl	$1, %edi
	movl	%eax, -7784(%rbp)
	call	app_tminterval@PLT
	xorl	%edi, %edi
	movsd	%xmm0, -7776(%rbp)
	call	alarm@PLT
	movl	-7784(%rbp), %edx
	movslq	testnum(%rip), %rbx
	cmpl	$-1, %edx
	je	.L1881
	movl	mr(%rip), %esi
	leaq	.LC17(%rip), %rax
	movsd	-7776(%rbp), %xmm0
	movl	%edx, -7784(%rbp)
	movq	16+names(%rip), %rcx
	movq	bio_err(%rip), %rdi
	leaq	results(%rip), %r14
	testl	%esi, %esi
	leaq	.LC16(%rip), %rsi
	cmove	%rax, %rsi
	movl	$1, %eax
	call	BIO_printf@PLT
	movq	lengths(%rip), %rcx
	pxor	%xmm1, %xmm1
	movl	-7784(%rbp), %edx
	pxor	%xmm0, %xmm0
	movl	testnum(%rip), %eax
	cvtsi2sdl	(%rcx,%rbx,4), %xmm1
	cvtsi2sdl	%edx, %xmm0
	divsd	-7776(%rbp), %xmm0
	addl	$1, %eax
	movl	%eax, testnum(%rip)
	mulsd	%xmm1, %xmm0
	movsd	%xmm0, 96(%r14,%rbx,8)
	cmpl	-7640(%rbp), %eax
	jb	.L821
.L810:
	movl	-7268(%rbp), %r8d
	testl	%r8d, %r8d
	je	.L817
	movl	$0, testnum(%rip)
	movq	lengths(%rip), %rcx
	xorl	%eax, %eax
.L827:
	movl	mr(%rip), %edx
	leaq	.LC15(%rip), %rsi
	movl	(%rcx,%rax,4), %r8d
	movl	-7648(%rbp), %ebx
	movq	bio_err(%rip), %rdi
	testl	%edx, %edx
	leaq	.LC14(%rip), %rdx
	cmovne	%rdx, %rsi
	movq	24+names(%rip), %rdx
	movl	%ebx, %ecx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	call	BIO_ctrl@PLT
	movl	%ebx, %edi
	movl	$1, run(%rip)
	call	alarm@PLT
	movl	usertime(%rip), %esi
	xorl	%edi, %edi
	call	app_tminterval@PLT
	movq	-7624(%rbp), %rdx
	movl	-7628(%rbp), %edi
	leaq	MD5_loop(%rip), %rsi
	call	run_benchmark
	movl	usertime(%rip), %esi
	movl	$1, %edi
	movl	%eax, -7784(%rbp)
	call	app_tminterval@PLT
	xorl	%edi, %edi
	movsd	%xmm0, -7776(%rbp)
	call	alarm@PLT
	movl	-7784(%rbp), %edx
	movslq	testnum(%rip), %rbx
	cmpl	$-1, %edx
	je	.L1881
	movl	mr(%rip), %r14d
	leaq	.LC17(%rip), %rax
	leaq	.LC16(%rip), %rsi
	movsd	-7776(%rbp), %xmm0
	movq	24+names(%rip), %rcx
	movq	bio_err(%rip), %rdi
	movl	%edx, -7784(%rbp)
	testl	%r14d, %r14d
	leaq	results(%rip), %r14
	cmove	%rax, %rsi
	movl	$1, %eax
	call	BIO_printf@PLT
	movq	lengths(%rip), %rcx
	pxor	%xmm1, %xmm1
	movl	-7784(%rbp), %edx
	pxor	%xmm0, %xmm0
	movl	testnum(%rip), %eax
	cvtsi2sdl	(%rcx,%rbx,4), %xmm1
	cvtsi2sdl	%edx, %xmm0
	divsd	-7776(%rbp), %xmm0
	addl	$1, %eax
	movl	%eax, testnum(%rip)
	mulsd	%xmm1, %xmm0
	movsd	%xmm0, 144(%r14,%rbx,8)
	cmpl	-7640(%rbp), %eax
	jb	.L827
.L817:
	movl	-7264(%rbp), %ecx
	testl	%ecx, %ecx
	je	.L823
	movq	-7624(%rbp), %rax
	movq	-7688(%rbp), %rcx
	leaq	hmac_key.27526(%rip), %r14
	leaq	752(%rax), %rbx
	leaq	752(%rax,%rcx), %rax
	movq	%rbx, -7784(%rbp)
	movq	%rax, -7776(%rbp)
.L831:
	call	HMAC_CTX_new@PLT
	movq	%rax, (%rbx)
	testq	%rax, %rax
	je	.L1901
	call	EVP_md5@PLT
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movl	$16, %edx
	movq	%rax, %rcx
	movq	%r14, %rsi
	addq	$768, %rbx
	call	HMAC_Init_ex@PLT
	cmpq	%rbx, -7776(%rbp)
	jne	.L831
	movl	$0, testnum(%rip)
	movq	lengths(%rip), %rcx
	xorl	%eax, %eax
.L835:
	movl	mr(%rip), %r10d
	movl	-7648(%rbp), %ebx
	leaq	.LC14(%rip), %rdx
	leaq	.LC15(%rip), %rsi
	movl	(%rcx,%rax,4), %r8d
	movq	bio_err(%rip), %rdi
	testl	%r10d, %r10d
	movl	%ebx, %ecx
	cmovne	%rdx, %rsi
	movq	32+names(%rip), %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	call	BIO_ctrl@PLT
	movl	%ebx, %edi
	movl	$1, run(%rip)
	call	alarm@PLT
	movl	usertime(%rip), %esi
	xorl	%edi, %edi
	call	app_tminterval@PLT
	movq	-7624(%rbp), %rdx
	movl	-7628(%rbp), %edi
	leaq	HMAC_loop(%rip), %rsi
	call	run_benchmark
	movl	usertime(%rip), %esi
	movl	$1, %edi
	movl	%eax, -7860(%rbp)
	call	app_tminterval@PLT
	xorl	%edi, %edi
	movsd	%xmm0, -7792(%rbp)
	call	alarm@PLT
	movl	-7860(%rbp), %edx
	movslq	testnum(%rip), %rbx
	cmpl	$-1, %edx
	je	.L1881
	movl	mr(%rip), %r9d
	leaq	.LC17(%rip), %rax
	leaq	.LC16(%rip), %rsi
	movsd	-7792(%rbp), %xmm0
	movq	32+names(%rip), %rcx
	movq	bio_err(%rip), %rdi
	movl	%edx, -7860(%rbp)
	leaq	results(%rip), %r14
	testl	%r9d, %r9d
	cmove	%rax, %rsi
	movl	$1, %eax
	call	BIO_printf@PLT
	movq	lengths(%rip), %rcx
	pxor	%xmm1, %xmm1
	movl	-7860(%rbp), %edx
	pxor	%xmm0, %xmm0
	movl	testnum(%rip), %eax
	cvtsi2sdl	(%rcx,%rbx,4), %xmm1
	cvtsi2sdl	%edx, %xmm0
	divsd	-7792(%rbp), %xmm0
	addl	$1, %eax
	movl	%eax, testnum(%rip)
	mulsd	%xmm1, %xmm0
	movsd	%xmm0, 192(%r14,%rbx,8)
	cmpl	-7640(%rbp), %eax
	jb	.L835
.L836:
	movq	-7784(%rbp), %rbx
	movq	(%rbx), %rdi
	call	HMAC_CTX_free@PLT
	movq	%rbx, %rax
	addq	$768, %rax
	movq	%rax, -7784(%rbp)
	cmpq	%rax, -7776(%rbp)
	jne	.L836
.L823:
	movl	-7260(%rbp), %r11d
	testl	%r11d, %r11d
	je	.L829
	movl	$0, testnum(%rip)
	movq	lengths(%rip), %rcx
	xorl	%eax, %eax
.L842:
	movl	mr(%rip), %edi
	movl	-7648(%rbp), %ebx
	leaq	.LC14(%rip), %rdx
	leaq	.LC15(%rip), %rsi
	movl	(%rcx,%rax,4), %r8d
	testl	%edi, %edi
	movq	bio_err(%rip), %rdi
	movl	%ebx, %ecx
	cmovne	%rdx, %rsi
	movq	40+names(%rip), %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	call	BIO_ctrl@PLT
	movl	%ebx, %edi
	movl	$1, run(%rip)
	call	alarm@PLT
	movl	usertime(%rip), %esi
	xorl	%edi, %edi
	call	app_tminterval@PLT
	movq	-7624(%rbp), %rdx
	movl	-7628(%rbp), %edi
	leaq	SHA1_loop(%rip), %rsi
	call	run_benchmark
	movl	usertime(%rip), %esi
	movl	$1, %edi
	movl	%eax, -7784(%rbp)
	call	app_tminterval@PLT
	xorl	%edi, %edi
	movsd	%xmm0, -7776(%rbp)
	call	alarm@PLT
	movl	-7784(%rbp), %edx
	movslq	testnum(%rip), %rbx
	cmpl	$-1, %edx
	je	.L1881
	movl	mr(%rip), %esi
	leaq	.LC17(%rip), %rax
	movsd	-7776(%rbp), %xmm0
	movl	%edx, -7784(%rbp)
	movq	40+names(%rip), %rcx
	movq	bio_err(%rip), %rdi
	leaq	results(%rip), %r14
	testl	%esi, %esi
	leaq	.LC16(%rip), %rsi
	cmove	%rax, %rsi
	movl	$1, %eax
	call	BIO_printf@PLT
	movq	lengths(%rip), %rcx
	pxor	%xmm1, %xmm1
	movl	-7784(%rbp), %edx
	pxor	%xmm0, %xmm0
	movl	testnum(%rip), %eax
	cvtsi2sdl	(%rcx,%rbx,4), %xmm1
	cvtsi2sdl	%edx, %xmm0
	divsd	-7776(%rbp), %xmm0
	addl	$1, %eax
	movl	%eax, testnum(%rip)
	mulsd	%xmm1, %xmm0
	movsd	%xmm0, 240(%r14,%rbx,8)
	cmpl	-7640(%rbp), %eax
	jb	.L842
.L829:
	movl	-7188(%rbp), %r8d
	testl	%r8d, %r8d
	je	.L838
	movl	$0, testnum(%rip)
	movq	lengths(%rip), %rcx
	xorl	%eax, %eax
.L848:
	movl	mr(%rip), %edx
	leaq	.LC15(%rip), %rsi
	movl	(%rcx,%rax,4), %r8d
	movl	-7648(%rbp), %ebx
	movq	bio_err(%rip), %rdi
	testl	%edx, %edx
	leaq	.LC14(%rip), %rdx
	cmovne	%rdx, %rsi
	movq	184+names(%rip), %rdx
	movl	%ebx, %ecx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	call	BIO_ctrl@PLT
	movl	%ebx, %edi
	movl	$1, run(%rip)
	call	alarm@PLT
	movl	usertime(%rip), %esi
	xorl	%edi, %edi
	call	app_tminterval@PLT
	movq	-7624(%rbp), %rdx
	movl	-7628(%rbp), %edi
	leaq	SHA256_loop(%rip), %rsi
	call	run_benchmark
	movl	usertime(%rip), %esi
	movl	$1, %edi
	movl	%eax, -7784(%rbp)
	call	app_tminterval@PLT
	xorl	%edi, %edi
	movsd	%xmm0, -7776(%rbp)
	call	alarm@PLT
	movl	-7784(%rbp), %edx
	movslq	testnum(%rip), %rbx
	cmpl	$-1, %edx
	je	.L1881
	movl	mr(%rip), %r14d
	leaq	.LC17(%rip), %rax
	leaq	.LC16(%rip), %rsi
	movsd	-7776(%rbp), %xmm0
	movq	184+names(%rip), %rcx
	movq	bio_err(%rip), %rdi
	movl	%edx, -7784(%rbp)
	testl	%r14d, %r14d
	leaq	results(%rip), %r14
	cmove	%rax, %rsi
	movl	$1, %eax
	call	BIO_printf@PLT
	movq	lengths(%rip), %rcx
	pxor	%xmm1, %xmm1
	movl	-7784(%rbp), %edx
	pxor	%xmm0, %xmm0
	movl	testnum(%rip), %eax
	cvtsi2sdl	(%rcx,%rbx,4), %xmm1
	cvtsi2sdl	%edx, %xmm0
	divsd	-7776(%rbp), %xmm0
	addl	$1, %eax
	movl	%eax, testnum(%rip)
	mulsd	%xmm1, %xmm0
	movsd	%xmm0, 1104(%r14,%rbx,8)
	cmpl	-7640(%rbp), %eax
	jb	.L848
.L838:
	movl	-7184(%rbp), %ecx
	testl	%ecx, %ecx
	je	.L844
	movl	$0, testnum(%rip)
	movq	lengths(%rip), %rcx
	xorl	%eax, %eax
.L854:
	movl	mr(%rip), %r10d
	movl	-7648(%rbp), %ebx
	leaq	.LC14(%rip), %rdx
	leaq	.LC15(%rip), %rsi
	movl	(%rcx,%rax,4), %r8d
	movq	bio_err(%rip), %rdi
	testl	%r10d, %r10d
	movl	%ebx, %ecx
	cmovne	%rdx, %rsi
	movq	192+names(%rip), %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	call	BIO_ctrl@PLT
	movl	%ebx, %edi
	movl	$1, run(%rip)
	call	alarm@PLT
	movl	usertime(%rip), %esi
	xorl	%edi, %edi
	call	app_tminterval@PLT
	movq	-7624(%rbp), %rdx
	movl	-7628(%rbp), %edi
	leaq	SHA512_loop(%rip), %rsi
	call	run_benchmark
	movl	usertime(%rip), %esi
	movl	$1, %edi
	movl	%eax, -7784(%rbp)
	call	app_tminterval@PLT
	xorl	%edi, %edi
	movsd	%xmm0, -7776(%rbp)
	call	alarm@PLT
	movl	-7784(%rbp), %edx
	movslq	testnum(%rip), %rbx
	cmpl	$-1, %edx
	je	.L1881
	movl	mr(%rip), %r9d
	leaq	.LC17(%rip), %rax
	leaq	.LC16(%rip), %rsi
	movsd	-7776(%rbp), %xmm0
	movq	192+names(%rip), %rcx
	movq	bio_err(%rip), %rdi
	movl	%edx, -7784(%rbp)
	leaq	results(%rip), %r14
	testl	%r9d, %r9d
	cmove	%rax, %rsi
	movl	$1, %eax
	call	BIO_printf@PLT
	movq	lengths(%rip), %rcx
	pxor	%xmm1, %xmm1
	movl	-7784(%rbp), %edx
	pxor	%xmm0, %xmm0
	movl	testnum(%rip), %eax
	cvtsi2sdl	(%rcx,%rbx,4), %xmm1
	cvtsi2sdl	%edx, %xmm0
	divsd	-7776(%rbp), %xmm0
	addl	$1, %eax
	movl	%eax, testnum(%rip)
	mulsd	%xmm1, %xmm0
	movsd	%xmm0, 1152(%r14,%rbx,8)
	cmpl	-7640(%rbp), %eax
	jb	.L854
.L844:
	movl	-7180(%rbp), %r11d
	testl	%r11d, %r11d
	je	.L850
	movl	$0, testnum(%rip)
	movq	lengths(%rip), %rcx
	xorl	%eax, %eax
.L860:
	movl	mr(%rip), %edi
	movl	-7648(%rbp), %ebx
	leaq	.LC14(%rip), %rdx
	leaq	.LC15(%rip), %rsi
	movl	(%rcx,%rax,4), %r8d
	testl	%edi, %edi
	movq	bio_err(%rip), %rdi
	movl	%ebx, %ecx
	cmovne	%rdx, %rsi
	movq	200+names(%rip), %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	call	BIO_ctrl@PLT
	movl	%ebx, %edi
	movl	$1, run(%rip)
	call	alarm@PLT
	movl	usertime(%rip), %esi
	xorl	%edi, %edi
	call	app_tminterval@PLT
	movq	-7624(%rbp), %rdx
	movl	-7628(%rbp), %edi
	leaq	WHIRLPOOL_loop(%rip), %rsi
	call	run_benchmark
	movl	usertime(%rip), %esi
	movl	$1, %edi
	movl	%eax, -7784(%rbp)
	call	app_tminterval@PLT
	xorl	%edi, %edi
	movsd	%xmm0, -7776(%rbp)
	call	alarm@PLT
	movl	-7784(%rbp), %edx
	movslq	testnum(%rip), %rbx
	cmpl	$-1, %edx
	je	.L1881
	movl	mr(%rip), %esi
	leaq	.LC17(%rip), %rax
	movsd	-7776(%rbp), %xmm0
	movl	%edx, -7784(%rbp)
	movq	200+names(%rip), %rcx
	movq	bio_err(%rip), %rdi
	leaq	results(%rip), %r14
	testl	%esi, %esi
	leaq	.LC16(%rip), %rsi
	cmove	%rax, %rsi
	movl	$1, %eax
	call	BIO_printf@PLT
	movq	lengths(%rip), %rcx
	pxor	%xmm1, %xmm1
	movl	-7784(%rbp), %edx
	pxor	%xmm0, %xmm0
	movl	testnum(%rip), %eax
	cvtsi2sdl	(%rcx,%rbx,4), %xmm1
	cvtsi2sdl	%edx, %xmm0
	divsd	-7776(%rbp), %xmm0
	addl	$1, %eax
	movl	%eax, testnum(%rip)
	mulsd	%xmm1, %xmm0
	movsd	%xmm0, 1200(%r14,%rbx,8)
	cmpl	-7640(%rbp), %eax
	jb	.L860
.L850:
	movl	-7256(%rbp), %r8d
	testl	%r8d, %r8d
	je	.L856
	movl	$0, testnum(%rip)
	movq	lengths(%rip), %rcx
	xorl	%eax, %eax
.L866:
	movl	mr(%rip), %edx
	leaq	.LC15(%rip), %rsi
	movl	(%rcx,%rax,4), %r8d
	movl	-7648(%rbp), %ebx
	movq	bio_err(%rip), %rdi
	testl	%edx, %edx
	leaq	.LC14(%rip), %rdx
	cmovne	%rdx, %rsi
	movq	48+names(%rip), %rdx
	movl	%ebx, %ecx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	call	BIO_ctrl@PLT
	movl	%ebx, %edi
	movl	$1, run(%rip)
	call	alarm@PLT
	movl	usertime(%rip), %esi
	xorl	%edi, %edi
	call	app_tminterval@PLT
	movq	-7624(%rbp), %rdx
	movl	-7628(%rbp), %edi
	leaq	EVP_Digest_RMD160_loop(%rip), %rsi
	call	run_benchmark
	movl	usertime(%rip), %esi
	movl	$1, %edi
	movl	%eax, -7784(%rbp)
	call	app_tminterval@PLT
	xorl	%edi, %edi
	movsd	%xmm0, -7776(%rbp)
	call	alarm@PLT
	movl	-7784(%rbp), %edx
	movslq	testnum(%rip), %rbx
	cmpl	$-1, %edx
	je	.L1881
	movl	mr(%rip), %r14d
	leaq	.LC17(%rip), %rax
	leaq	.LC16(%rip), %rsi
	movsd	-7776(%rbp), %xmm0
	movq	48+names(%rip), %rcx
	movq	bio_err(%rip), %rdi
	movl	%edx, -7784(%rbp)
	testl	%r14d, %r14d
	leaq	results(%rip), %r14
	cmove	%rax, %rsi
	movl	$1, %eax
	call	BIO_printf@PLT
	movq	lengths(%rip), %rcx
	pxor	%xmm1, %xmm1
	movl	-7784(%rbp), %edx
	pxor	%xmm0, %xmm0
	movl	testnum(%rip), %eax
	cvtsi2sdl	(%rcx,%rbx,4), %xmm1
	cvtsi2sdl	%edx, %xmm0
	divsd	-7776(%rbp), %xmm0
	addl	$1, %eax
	movl	%eax, testnum(%rip)
	mulsd	%xmm1, %xmm0
	movsd	%xmm0, 288(%r14,%rbx,8)
	cmpl	-7640(%rbp), %eax
	jb	.L866
.L856:
	movl	-7252(%rbp), %ecx
	testl	%ecx, %ecx
	je	.L862
	movl	$0, testnum(%rip)
	movq	lengths(%rip), %rcx
	xorl	%eax, %eax
.L872:
	movl	mr(%rip), %r10d
	movl	-7648(%rbp), %ebx
	leaq	.LC14(%rip), %rdx
	leaq	.LC15(%rip), %rsi
	movl	(%rcx,%rax,4), %r8d
	movq	bio_err(%rip), %rdi
	testl	%r10d, %r10d
	movl	%ebx, %ecx
	cmovne	%rdx, %rsi
	movq	56+names(%rip), %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	call	BIO_ctrl@PLT
	movl	%ebx, %edi
	movl	$1, run(%rip)
	call	alarm@PLT
	movl	usertime(%rip), %esi
	xorl	%edi, %edi
	call	app_tminterval@PLT
	movq	-7624(%rbp), %rdx
	movl	-7628(%rbp), %edi
	leaq	RC4_loop(%rip), %rsi
	call	run_benchmark
	movl	usertime(%rip), %esi
	movl	$1, %edi
	movl	%eax, -7784(%rbp)
	call	app_tminterval@PLT
	xorl	%edi, %edi
	movsd	%xmm0, -7776(%rbp)
	call	alarm@PLT
	movl	-7784(%rbp), %edx
	movslq	testnum(%rip), %rbx
	cmpl	$-1, %edx
	je	.L1881
	movl	mr(%rip), %r9d
	leaq	.LC17(%rip), %rax
	leaq	.LC16(%rip), %rsi
	movsd	-7776(%rbp), %xmm0
	movq	56+names(%rip), %rcx
	movq	bio_err(%rip), %rdi
	movl	%edx, -7784(%rbp)
	leaq	results(%rip), %r14
	testl	%r9d, %r9d
	cmove	%rax, %rsi
	movl	$1, %eax
	call	BIO_printf@PLT
	movq	lengths(%rip), %rcx
	pxor	%xmm1, %xmm1
	movl	-7784(%rbp), %edx
	pxor	%xmm0, %xmm0
	movl	testnum(%rip), %eax
	cvtsi2sdl	(%rcx,%rbx,4), %xmm1
	cvtsi2sdl	%edx, %xmm0
	divsd	-7776(%rbp), %xmm0
	addl	$1, %eax
	movl	%eax, testnum(%rip)
	mulsd	%xmm1, %xmm0
	movsd	%xmm0, 336(%r14,%rbx,8)
	cmpl	-7640(%rbp), %eax
	jb	.L872
.L862:
	movl	-7248(%rbp), %r11d
	testl	%r11d, %r11d
	je	.L868
	movl	$0, testnum(%rip)
	movq	lengths(%rip), %rcx
	xorl	%eax, %eax
.L878:
	movl	mr(%rip), %edi
	movl	-7648(%rbp), %ebx
	leaq	.LC14(%rip), %rdx
	leaq	.LC15(%rip), %rsi
	movl	(%rcx,%rax,4), %r8d
	testl	%edi, %edi
	movq	bio_err(%rip), %rdi
	movl	%ebx, %ecx
	cmovne	%rdx, %rsi
	movq	64+names(%rip), %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	call	BIO_ctrl@PLT
	movl	%ebx, %edi
	movl	$1, run(%rip)
	call	alarm@PLT
	movl	usertime(%rip), %esi
	xorl	%edi, %edi
	call	app_tminterval@PLT
	movq	-7624(%rbp), %rdx
	movl	-7628(%rbp), %edi
	leaq	DES_ncbc_encrypt_loop(%rip), %rsi
	call	run_benchmark
	movl	usertime(%rip), %esi
	movl	$1, %edi
	movl	%eax, -7784(%rbp)
	call	app_tminterval@PLT
	xorl	%edi, %edi
	movsd	%xmm0, -7776(%rbp)
	call	alarm@PLT
	movl	-7784(%rbp), %edx
	movslq	testnum(%rip), %rbx
	cmpl	$-1, %edx
	je	.L1881
	movl	mr(%rip), %esi
	leaq	.LC17(%rip), %rax
	movsd	-7776(%rbp), %xmm0
	movl	%edx, -7784(%rbp)
	movq	64+names(%rip), %rcx
	movq	bio_err(%rip), %rdi
	leaq	results(%rip), %r14
	testl	%esi, %esi
	leaq	.LC16(%rip), %rsi
	cmove	%rax, %rsi
	movl	$1, %eax
	call	BIO_printf@PLT
	movq	lengths(%rip), %rcx
	pxor	%xmm1, %xmm1
	movl	-7784(%rbp), %edx
	pxor	%xmm0, %xmm0
	movl	testnum(%rip), %eax
	cvtsi2sdl	(%rcx,%rbx,4), %xmm1
	cvtsi2sdl	%edx, %xmm0
	divsd	-7776(%rbp), %xmm0
	addl	$1, %eax
	movl	%eax, testnum(%rip)
	mulsd	%xmm1, %xmm0
	movsd	%xmm0, 384(%r14,%rbx,8)
	cmpl	-7640(%rbp), %eax
	jb	.L878
.L868:
	movl	-7244(%rbp), %r8d
	testl	%r8d, %r8d
	je	.L874
	movl	$0, testnum(%rip)
	movq	lengths(%rip), %rcx
	xorl	%eax, %eax
.L884:
	movl	mr(%rip), %edx
	leaq	.LC15(%rip), %rsi
	movl	(%rcx,%rax,4), %r8d
	movl	-7648(%rbp), %ebx
	movq	bio_err(%rip), %rdi
	testl	%edx, %edx
	leaq	.LC14(%rip), %rdx
	cmovne	%rdx, %rsi
	movq	72+names(%rip), %rdx
	movl	%ebx, %ecx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	call	BIO_ctrl@PLT
	movl	%ebx, %edi
	movl	$1, run(%rip)
	call	alarm@PLT
	movl	usertime(%rip), %esi
	xorl	%edi, %edi
	call	app_tminterval@PLT
	movq	-7624(%rbp), %rdx
	movl	-7628(%rbp), %edi
	leaq	DES_ede3_cbc_encrypt_loop(%rip), %rsi
	call	run_benchmark
	movl	usertime(%rip), %esi
	movl	$1, %edi
	movl	%eax, -7784(%rbp)
	call	app_tminterval@PLT
	xorl	%edi, %edi
	movsd	%xmm0, -7776(%rbp)
	call	alarm@PLT
	movl	-7784(%rbp), %edx
	movslq	testnum(%rip), %rbx
	cmpl	$-1, %edx
	je	.L1881
	movl	mr(%rip), %r14d
	leaq	.LC17(%rip), %rax
	leaq	.LC16(%rip), %rsi
	movsd	-7776(%rbp), %xmm0
	movq	72+names(%rip), %rcx
	movq	bio_err(%rip), %rdi
	movl	%edx, -7784(%rbp)
	testl	%r14d, %r14d
	leaq	results(%rip), %r14
	cmove	%rax, %rsi
	movl	$1, %eax
	call	BIO_printf@PLT
	movq	lengths(%rip), %rcx
	pxor	%xmm1, %xmm1
	movl	-7784(%rbp), %edx
	pxor	%xmm0, %xmm0
	movl	testnum(%rip), %eax
	cvtsi2sdl	(%rcx,%rbx,4), %xmm1
	cvtsi2sdl	%edx, %xmm0
	divsd	-7776(%rbp), %xmm0
	addl	$1, %eax
	movl	%eax, testnum(%rip)
	mulsd	%xmm1, %xmm0
	movsd	%xmm0, 432(%r14,%rbx,8)
	cmpl	-7640(%rbp), %eax
	jb	.L884
.L874:
	movl	-7216(%rbp), %ecx
	testl	%ecx, %ecx
	je	.L880
	movl	$0, testnum(%rip)
	movq	lengths(%rip), %rcx
	xorl	%eax, %eax
.L890:
	movl	mr(%rip), %r10d
	movl	-7648(%rbp), %ebx
	leaq	.LC14(%rip), %rdx
	leaq	.LC15(%rip), %rsi
	movl	(%rcx,%rax,4), %r8d
	movq	bio_err(%rip), %rdi
	testl	%r10d, %r10d
	movl	%ebx, %ecx
	cmovne	%rdx, %rsi
	movq	128+names(%rip), %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	call	BIO_ctrl@PLT
	movl	%ebx, %edi
	movl	$1, run(%rip)
	call	alarm@PLT
	movl	usertime(%rip), %esi
	xorl	%edi, %edi
	call	app_tminterval@PLT
	movq	-7624(%rbp), %rdx
	movl	-7628(%rbp), %edi
	leaq	AES_cbc_128_encrypt_loop(%rip), %rsi
	call	run_benchmark
	movl	usertime(%rip), %esi
	movl	$1, %edi
	movl	%eax, -7784(%rbp)
	call	app_tminterval@PLT
	xorl	%edi, %edi
	movsd	%xmm0, -7776(%rbp)
	call	alarm@PLT
	movl	-7784(%rbp), %edx
	movslq	testnum(%rip), %rbx
	cmpl	$-1, %edx
	je	.L1881
	movl	mr(%rip), %r9d
	leaq	.LC17(%rip), %rax
	leaq	.LC16(%rip), %rsi
	movsd	-7776(%rbp), %xmm0
	movq	128+names(%rip), %rcx
	movq	bio_err(%rip), %rdi
	movl	%edx, -7784(%rbp)
	leaq	results(%rip), %r14
	testl	%r9d, %r9d
	cmove	%rax, %rsi
	movl	$1, %eax
	call	BIO_printf@PLT
	movq	lengths(%rip), %rcx
	pxor	%xmm1, %xmm1
	movl	-7784(%rbp), %edx
	pxor	%xmm0, %xmm0
	movl	testnum(%rip), %eax
	cvtsi2sdl	(%rcx,%rbx,4), %xmm1
	cvtsi2sdl	%edx, %xmm0
	divsd	-7776(%rbp), %xmm0
	addl	$1, %eax
	movl	%eax, testnum(%rip)
	mulsd	%xmm1, %xmm0
	movsd	%xmm0, 768(%r14,%rbx,8)
	cmpl	-7640(%rbp), %eax
	jb	.L890
.L880:
	movl	-7212(%rbp), %r11d
	testl	%r11d, %r11d
	je	.L886
	movl	$0, testnum(%rip)
	movq	lengths(%rip), %rcx
	xorl	%eax, %eax
.L896:
	movl	mr(%rip), %edi
	movl	-7648(%rbp), %ebx
	leaq	.LC14(%rip), %rdx
	leaq	.LC15(%rip), %rsi
	movl	(%rcx,%rax,4), %r8d
	testl	%edi, %edi
	movq	bio_err(%rip), %rdi
	movl	%ebx, %ecx
	cmovne	%rdx, %rsi
	movq	136+names(%rip), %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	call	BIO_ctrl@PLT
	movl	%ebx, %edi
	movl	$1, run(%rip)
	call	alarm@PLT
	movl	usertime(%rip), %esi
	xorl	%edi, %edi
	call	app_tminterval@PLT
	movq	-7624(%rbp), %rdx
	movl	-7628(%rbp), %edi
	leaq	AES_cbc_192_encrypt_loop(%rip), %rsi
	call	run_benchmark
	movl	usertime(%rip), %esi
	movl	$1, %edi
	movl	%eax, -7784(%rbp)
	call	app_tminterval@PLT
	xorl	%edi, %edi
	movsd	%xmm0, -7776(%rbp)
	call	alarm@PLT
	movl	-7784(%rbp), %edx
	movslq	testnum(%rip), %rbx
	cmpl	$-1, %edx
	je	.L1881
	movl	mr(%rip), %esi
	leaq	.LC17(%rip), %rax
	movsd	-7776(%rbp), %xmm0
	movl	%edx, -7784(%rbp)
	movq	136+names(%rip), %rcx
	movq	bio_err(%rip), %rdi
	leaq	results(%rip), %r14
	testl	%esi, %esi
	leaq	.LC16(%rip), %rsi
	cmove	%rax, %rsi
	movl	$1, %eax
	call	BIO_printf@PLT
	movq	lengths(%rip), %rcx
	pxor	%xmm1, %xmm1
	movl	-7784(%rbp), %edx
	pxor	%xmm0, %xmm0
	movl	testnum(%rip), %eax
	cvtsi2sdl	(%rcx,%rbx,4), %xmm1
	cvtsi2sdl	%edx, %xmm0
	divsd	-7776(%rbp), %xmm0
	addl	$1, %eax
	movl	%eax, testnum(%rip)
	mulsd	%xmm1, %xmm0
	movsd	%xmm0, 816(%r14,%rbx,8)
	cmpl	-7640(%rbp), %eax
	jb	.L896
.L886:
	movl	-7208(%rbp), %r8d
	testl	%r8d, %r8d
	je	.L892
	movl	$0, testnum(%rip)
	movq	lengths(%rip), %rcx
	xorl	%eax, %eax
.L902:
	movl	mr(%rip), %edx
	leaq	.LC15(%rip), %rsi
	movl	(%rcx,%rax,4), %r8d
	movl	-7648(%rbp), %ebx
	movq	bio_err(%rip), %rdi
	testl	%edx, %edx
	leaq	.LC14(%rip), %rdx
	cmovne	%rdx, %rsi
	movq	144+names(%rip), %rdx
	movl	%ebx, %ecx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	call	BIO_ctrl@PLT
	movl	%ebx, %edi
	movl	$1, run(%rip)
	call	alarm@PLT
	movl	usertime(%rip), %esi
	xorl	%edi, %edi
	call	app_tminterval@PLT
	movq	-7624(%rbp), %rdx
	movl	-7628(%rbp), %edi
	leaq	AES_cbc_256_encrypt_loop(%rip), %rsi
	call	run_benchmark
	movl	usertime(%rip), %esi
	movl	$1, %edi
	movl	%eax, -7784(%rbp)
	call	app_tminterval@PLT
	xorl	%edi, %edi
	movsd	%xmm0, -7776(%rbp)
	call	alarm@PLT
	movl	-7784(%rbp), %edx
	movslq	testnum(%rip), %rbx
	cmpl	$-1, %edx
	je	.L1881
	movl	mr(%rip), %r14d
	leaq	.LC17(%rip), %rax
	leaq	.LC16(%rip), %rsi
	movsd	-7776(%rbp), %xmm0
	movq	144+names(%rip), %rcx
	movq	bio_err(%rip), %rdi
	movl	%edx, -7784(%rbp)
	testl	%r14d, %r14d
	leaq	results(%rip), %r14
	cmove	%rax, %rsi
	movl	$1, %eax
	call	BIO_printf@PLT
	movq	lengths(%rip), %rcx
	pxor	%xmm1, %xmm1
	movl	-7784(%rbp), %edx
	pxor	%xmm0, %xmm0
	movl	testnum(%rip), %eax
	cvtsi2sdl	(%rcx,%rbx,4), %xmm1
	cvtsi2sdl	%edx, %xmm0
	divsd	-7776(%rbp), %xmm0
	addl	$1, %eax
	movl	%eax, testnum(%rip)
	mulsd	%xmm1, %xmm0
	movsd	%xmm0, 864(%r14,%rbx,8)
	cmpl	-7640(%rbp), %eax
	jb	.L902
.L892:
	movl	-7176(%rbp), %ecx
	testl	%ecx, %ecx
	je	.L898
	movl	%r12d, -7784(%rbp)
	movq	lengths(%rip), %rcx
	xorl	%eax, %eax
	leaq	.LC14(%rip), %r14
	movl	$0, testnum(%rip)
.L908:
	movl	mr(%rip), %r10d
	leaq	.LC15(%rip), %rsi
	movl	(%rcx,%rax,4), %r8d
	movl	-7648(%rbp), %ebx
	movq	208+names(%rip), %rdx
	testl	%r10d, %r10d
	movq	bio_err(%rip), %rdi
	cmovne	%r14, %rsi
	movl	%ebx, %ecx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	call	BIO_ctrl@PLT
	movl	%ebx, %edi
	movl	$1, run(%rip)
	call	alarm@PLT
	movl	usertime(%rip), %esi
	xorl	%edi, %edi
	call	app_tminterval@PLT
	movq	-7624(%rbp), %rdx
	movl	-7628(%rbp), %edi
	leaq	AES_ige_128_encrypt_loop(%rip), %rsi
	call	run_benchmark
	movl	usertime(%rip), %esi
	movl	$1, %edi
	movl	%eax, %r12d
	call	app_tminterval@PLT
	xorl	%edi, %edi
	movsd	%xmm0, -7776(%rbp)
	call	alarm@PLT
	movslq	testnum(%rip), %rbx
	cmpl	$-1, %r12d
	je	.L1881
	movl	mr(%rip), %r9d
	leaq	.LC17(%rip), %rax
	leaq	.LC16(%rip), %rsi
	movsd	-7776(%rbp), %xmm0
	movq	208+names(%rip), %rcx
	movq	bio_err(%rip), %rdi
	movl	%r12d, %edx
	testl	%r9d, %r9d
	cmove	%rax, %rsi
	movl	$1, %eax
	call	BIO_printf@PLT
	movq	lengths(%rip), %rcx
	pxor	%xmm1, %xmm1
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%r12d, %xmm0
	divsd	-7776(%rbp), %xmm0
	leaq	results(%rip), %rax
	cvtsi2sdl	(%rcx,%rbx,4), %xmm1
	mulsd	%xmm1, %xmm0
	movsd	%xmm0, 1248(%rax,%rbx,8)
	movl	testnum(%rip), %eax
	addl	$1, %eax
	movl	%eax, testnum(%rip)
	cmpl	-7640(%rbp), %eax
	jb	.L908
	movl	-7784(%rbp), %r12d
.L898:
	movl	-7172(%rbp), %r11d
	testl	%r11d, %r11d
	je	.L904
	movl	%r12d, -7784(%rbp)
	movq	lengths(%rip), %rcx
	xorl	%eax, %eax
	leaq	.LC14(%rip), %r14
	movl	$0, testnum(%rip)
.L914:
	movl	mr(%rip), %edi
	leaq	.LC15(%rip), %rsi
	movl	(%rcx,%rax,4), %r8d
	movl	-7648(%rbp), %ebx
	movq	216+names(%rip), %rdx
	testl	%edi, %edi
	movq	bio_err(%rip), %rdi
	cmovne	%r14, %rsi
	movl	%ebx, %ecx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	call	BIO_ctrl@PLT
	movl	%ebx, %edi
	movl	$1, run(%rip)
	call	alarm@PLT
	movl	usertime(%rip), %esi
	xorl	%edi, %edi
	call	app_tminterval@PLT
	movq	-7624(%rbp), %rdx
	movl	-7628(%rbp), %edi
	leaq	AES_ige_192_encrypt_loop(%rip), %rsi
	call	run_benchmark
	movl	usertime(%rip), %esi
	movl	$1, %edi
	movl	%eax, %r12d
	call	app_tminterval@PLT
	xorl	%edi, %edi
	movsd	%xmm0, -7776(%rbp)
	call	alarm@PLT
	movslq	testnum(%rip), %rbx
	cmpl	$-1, %r12d
	je	.L1881
	movl	mr(%rip), %esi
	leaq	.LC17(%rip), %rax
	movsd	-7776(%rbp), %xmm0
	movl	%r12d, %edx
	movq	216+names(%rip), %rcx
	movq	bio_err(%rip), %rdi
	testl	%esi, %esi
	leaq	.LC16(%rip), %rsi
	cmove	%rax, %rsi
	movl	$1, %eax
	call	BIO_printf@PLT
	movq	lengths(%rip), %rcx
	pxor	%xmm1, %xmm1
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%r12d, %xmm0
	divsd	-7776(%rbp), %xmm0
	leaq	results(%rip), %rax
	cvtsi2sdl	(%rcx,%rbx,4), %xmm1
	mulsd	%xmm1, %xmm0
	movsd	%xmm0, 1296(%rax,%rbx,8)
	movl	testnum(%rip), %eax
	addl	$1, %eax
	movl	%eax, testnum(%rip)
	cmpl	-7640(%rbp), %eax
	jb	.L914
	movl	-7784(%rbp), %r12d
.L904:
	movl	-7840(%rbp), %r8d
	testl	%r8d, %r8d
	je	.L910
	movl	%r12d, -7784(%rbp)
	movq	lengths(%rip), %rcx
	xorl	%eax, %eax
	leaq	.LC14(%rip), %r14
	movl	$0, testnum(%rip)
.L920:
	movl	mr(%rip), %edx
	leaq	.LC15(%rip), %rsi
	movl	(%rcx,%rax,4), %r8d
	movl	-7648(%rbp), %ebx
	movq	bio_err(%rip), %rdi
	testl	%edx, %edx
	movq	224+names(%rip), %rdx
	cmovne	%r14, %rsi
	movl	%ebx, %ecx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	call	BIO_ctrl@PLT
	movl	%ebx, %edi
	movl	$1, run(%rip)
	call	alarm@PLT
	movl	usertime(%rip), %esi
	xorl	%edi, %edi
	call	app_tminterval@PLT
	movq	-7624(%rbp), %rdx
	movl	-7628(%rbp), %edi
	leaq	AES_ige_256_encrypt_loop(%rip), %rsi
	call	run_benchmark
	movl	usertime(%rip), %esi
	movl	$1, %edi
	movl	%eax, %r12d
	call	app_tminterval@PLT
	xorl	%edi, %edi
	movsd	%xmm0, -7776(%rbp)
	call	alarm@PLT
	movslq	testnum(%rip), %rbx
	cmpl	$-1, %r12d
	je	.L1881
	movl	mr(%rip), %eax
	leaq	.LC16(%rip), %rsi
	movsd	-7776(%rbp), %xmm0
	movl	%r12d, %edx
	movq	224+names(%rip), %rcx
	movq	bio_err(%rip), %rdi
	testl	%eax, %eax
	leaq	.LC17(%rip), %rax
	cmove	%rax, %rsi
	movl	$1, %eax
	call	BIO_printf@PLT
	movq	lengths(%rip), %rcx
	pxor	%xmm1, %xmm1
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%r12d, %xmm0
	divsd	-7776(%rbp), %xmm0
	leaq	results(%rip), %rax
	cvtsi2sdl	(%rcx,%rbx,4), %xmm1
	mulsd	%xmm1, %xmm0
	movsd	%xmm0, 1344(%rax,%rbx,8)
	movl	testnum(%rip), %eax
	addl	$1, %eax
	movl	%eax, testnum(%rip)
	cmpl	-7640(%rbp), %eax
	jb	.L920
	movl	-7784(%rbp), %r12d
.L910:
	movl	-7836(%rbp), %ecx
	testl	%ecx, %ecx
	je	.L916
	movq	-7624(%rbp), %rax
	movq	-7688(%rbp), %rcx
	leaq	760(%rax), %r14
	leaq	760(%rax,%rcx), %rax
	movq	%rax, -7776(%rbp)
	movq	%r14, %rbx
	leaq	aes_ks1(%rip), %rcx
.L923:
	movq	AES_encrypt@GOTPCREL(%rip), %rsi
	movq	%rcx, %rdi
	addq	$768, %rbx
	call	CRYPTO_gcm128_new@PLT
	movl	$12, %edx
	leaq	.LC95(%rip), %rsi
	movq	%rax, -768(%rbx)
	movq	%rax, %rdi
	call	CRYPTO_gcm128_setiv@PLT
	cmpq	%rbx, -7776(%rbp)
	leaq	aes_ks1(%rip), %rcx
	jne	.L923
	movl	%r12d, -7792(%rbp)
	movq	lengths(%rip), %rcx
	xorl	%eax, %eax
	movl	$0, testnum(%rip)
.L927:
	movl	mr(%rip), %r11d
	movl	-7648(%rbp), %ebx
	leaq	.LC14(%rip), %rdx
	leaq	.LC15(%rip), %rsi
	movl	(%rcx,%rax,4), %r8d
	movq	bio_err(%rip), %rdi
	testl	%r11d, %r11d
	movl	%ebx, %ecx
	cmovne	%rdx, %rsi
	movq	232+names(%rip), %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	call	BIO_ctrl@PLT
	movl	%ebx, %edi
	movl	$1, run(%rip)
	call	alarm@PLT
	movl	usertime(%rip), %esi
	xorl	%edi, %edi
	call	app_tminterval@PLT
	movq	-7624(%rbp), %rdx
	movl	-7628(%rbp), %edi
	leaq	CRYPTO_gcm128_aad_loop(%rip), %rsi
	call	run_benchmark
	movl	usertime(%rip), %esi
	movl	$1, %edi
	movl	%eax, %r12d
	call	app_tminterval@PLT
	xorl	%edi, %edi
	movsd	%xmm0, -7784(%rbp)
	call	alarm@PLT
	movslq	testnum(%rip), %rbx
	cmpl	$-1, %r12d
	je	.L1881
	movl	mr(%rip), %r10d
	leaq	.LC17(%rip), %rax
	leaq	.LC16(%rip), %rsi
	movsd	-7784(%rbp), %xmm0
	movq	232+names(%rip), %rcx
	movq	bio_err(%rip), %rdi
	movl	%r12d, %edx
	testl	%r10d, %r10d
	cmove	%rax, %rsi
	movl	$1, %eax
	call	BIO_printf@PLT
	movq	lengths(%rip), %rcx
	pxor	%xmm1, %xmm1
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%r12d, %xmm0
	divsd	-7784(%rbp), %xmm0
	leaq	results(%rip), %rax
	cvtsi2sdl	(%rcx,%rbx,4), %xmm1
	mulsd	%xmm1, %xmm0
	movsd	%xmm0, 1392(%rax,%rbx,8)
	movl	testnum(%rip), %eax
	addl	$1, %eax
	movl	%eax, testnum(%rip)
	cmpl	-7640(%rbp), %eax
	jb	.L927
	movl	-7792(%rbp), %r12d
.L928:
	movq	(%r14), %rdi
	addq	$768, %r14
	call	CRYPTO_gcm128_release@PLT
	cmpq	%r14, -7776(%rbp)
	jne	.L928
.L916:
	movl	-7204(%rbp), %ebx
	testl	%ebx, %ebx
	je	.L922
	movl	-7628(%rbp), %r8d
	testl	%r8d, %r8d
	jne	.L1902
.L931:
	movl	$0, testnum(%rip)
	movl	-7672(%rbp), %edi
	testl	%edi, %edi
	jne	.L922
	movl	%r12d, -7784(%rbp)
	movq	lengths(%rip), %rcx
	xorl	%eax, %eax
	leaq	results(%rip), %r14
.L937:
	movl	mr(%rip), %esi
	leaq	.LC14(%rip), %rdx
	movl	(%rcx,%rax,4), %r8d
	movl	-7648(%rbp), %ebx
	movq	bio_err(%rip), %rdi
	testl	%esi, %esi
	leaq	.LC15(%rip), %rsi
	cmovne	%rdx, %rsi
	movq	152+names(%rip), %rdx
	movl	%ebx, %ecx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	call	BIO_ctrl@PLT
	movl	%ebx, %edi
	movl	$1, run(%rip)
	call	alarm@PLT
	movl	usertime(%rip), %esi
	xorl	%edi, %edi
	call	app_tminterval@PLT
	movl	run(%rip), %r12d
	testl	%r12d, %r12d
	je	.L933
	xorl	%r12d, %r12d
	leaq	iv(%rip), %rbx
	jmp	.L935
	.p2align 4,,10
	.p2align 3
.L1903:
	cmpq	$2147483647, %r12
	je	.L933
.L935:
	movq	-7624(%rbp), %rax
	movl	testnum(%rip), %ecx
	movq	%rbx, %r8
	addq	$1, %r12
	movq	lengths(%rip), %rdx
	movl	$1, %r9d
	movq	16(%rax), %rdi
	movslq	(%rdx,%rcx,4), %rdx
	movq	-7824(%rbp), %rcx
	movq	%rdi, %rsi
	call	Camellia_cbc_encrypt@PLT
	movl	run(%rip), %edx
	testl	%edx, %edx
	jne	.L1903
.L933:
	movl	usertime(%rip), %esi
	movl	$1, %edi
	call	app_tminterval@PLT
	xorl	%edi, %edi
	movsd	%xmm0, -7776(%rbp)
	call	alarm@PLT
	movl	mr(%rip), %edx
	leaq	.LC17(%rip), %rax
	leaq	.LC16(%rip), %rsi
	movsd	-7776(%rbp), %xmm0
	movq	152+names(%rip), %rcx
	testl	%edx, %edx
	movq	bio_err(%rip), %rdi
	movl	%r12d, %edx
	movslq	testnum(%rip), %rbx
	cmove	%rax, %rsi
	movl	$1, %eax
	call	BIO_printf@PLT
	movq	lengths(%rip), %rcx
	pxor	%xmm1, %xmm1
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%r12d, %xmm0
	divsd	-7776(%rbp), %xmm0
	movl	testnum(%rip), %eax
	cvtsi2sdl	(%rcx,%rbx,4), %xmm1
	addl	$1, %eax
	movl	%eax, testnum(%rip)
	mulsd	%xmm1, %xmm0
	movsd	%xmm0, 912(%r14,%rbx,8)
	cmpl	-7640(%rbp), %eax
	jb	.L937
	movl	-7784(%rbp), %r12d
.L922:
	movl	-7200(%rbp), %r9d
	testl	%r9d, %r9d
	je	.L930
	movl	-7628(%rbp), %eax
	testl	%eax, %eax
	jne	.L1904
.L938:
	movl	$0, testnum(%rip)
	movl	-7672(%rbp), %eax
	testl	%eax, %eax
	jne	.L930
	movl	%r13d, -7792(%rbp)
	movq	lengths(%rip), %rcx
	xorl	%edx, %edx
	movl	%r12d, -7784(%rbp)
	movq	-7624(%rbp), %rbx
	movq	-7856(%rbp), %r12
.L945:
	movl	mr(%rip), %eax
	leaq	.LC15(%rip), %rsi
	movl	(%rcx,%rdx,4), %r8d
	movl	-7648(%rbp), %r14d
	movq	160+names(%rip), %rdx
	testl	%eax, %eax
	leaq	.LC14(%rip), %rax
	movq	bio_err(%rip), %rdi
	cmovne	%rax, %rsi
	movl	%r14d, %ecx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	call	BIO_ctrl@PLT
	movl	%r14d, %edi
	movl	$1, run(%rip)
	call	alarm@PLT
	movl	-7628(%rbp), %eax
	testl	%eax, %eax
	jne	.L1883
	movl	usertime(%rip), %esi
	xorl	%edi, %edi
	call	app_tminterval@PLT
	movl	run(%rip), %r13d
	testl	%r13d, %r13d
	je	.L941
	xorl	%r13d, %r13d
	leaq	iv(%rip), %r14
	jmp	.L943
	.p2align 4,,10
	.p2align 3
.L1905:
	cmpq	$2147483647, %r13
	je	.L941
.L943:
	movl	testnum(%rip), %edx
	movq	16(%rbx), %rdi
	movq	%r14, %r8
	movq	%r12, %rcx
	movq	lengths(%rip), %rax
	movl	$1, %r9d
	addq	$1, %r13
	movq	%rdi, %rsi
	movslq	(%rax,%rdx,4), %rdx
	call	Camellia_cbc_encrypt@PLT
	movl	run(%rip), %eax
	testl	%eax, %eax
	jne	.L1905
.L941:
	movl	usertime(%rip), %esi
	movl	$1, %edi
	call	app_tminterval@PLT
	xorl	%edi, %edi
	movsd	%xmm0, -7776(%rbp)
	call	alarm@PLT
	movl	mr(%rip), %eax
	movl	%r13d, %edx
	leaq	.LC16(%rip), %rsi
	movsd	-7776(%rbp), %xmm0
	movq	160+names(%rip), %rcx
	testl	%eax, %eax
	leaq	.LC17(%rip), %rax
	movq	bio_err(%rip), %rdi
	movl	testnum(%rip), %r14d
	cmove	%rax, %rsi
	movl	$1, %eax
	call	BIO_printf@PLT
	movslq	%r14d, %rsi
	pxor	%xmm1, %xmm1
	pxor	%xmm0, %xmm0
	movq	lengths(%rip), %rax
	cvtsi2sdl	%r13d, %xmm0
	divsd	-7776(%rbp), %xmm0
	leaq	results(%rip), %rdi
	cvtsi2sdl	(%rax,%rsi,4), %xmm1
	movq	%rax, %rcx
	mulsd	%xmm1, %xmm0
	movsd	%xmm0, 960(%rdi,%rsi,8)
	movl	testnum(%rip), %esi
	leal	1(%rsi), %edx
	movl	%edx, testnum(%rip)
	cmpl	-7640(%rbp), %edx
	jb	.L945
	movl	-7196(%rbp), %esi
	movl	-7784(%rbp), %r12d
	movq	%rdi, %r14
	movl	-7792(%rbp), %r13d
	testl	%esi, %esi
	je	.L1906
	movl	$0, testnum(%rip)
.L1205:
	movl	%r12d, -7784(%rbp)
	xorl	%edx, %edx
.L957:
	movl	mr(%rip), %r9d
	movl	-7648(%rbp), %ebx
	leaq	.LC14(%rip), %rcx
	leaq	.LC15(%rip), %rsi
	movl	(%rax,%rdx,4), %r8d
	movq	bio_err(%rip), %rdi
	testl	%r9d, %r9d
	movq	168+names(%rip), %rdx
	cmovne	%rcx, %rsi
	movl	%ebx, %ecx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	call	BIO_ctrl@PLT
	movl	%ebx, %edi
	movl	$1, run(%rip)
	call	alarm@PLT
	movl	usertime(%rip), %esi
	xorl	%edi, %edi
	call	app_tminterval@PLT
	movl	run(%rip), %r12d
	testl	%r12d, %r12d
	je	.L953
	xorl	%r12d, %r12d
	leaq	iv(%rip), %rbx
	jmp	.L955
	.p2align 4,,10
	.p2align 3
.L1907:
	cmpq	$2147483647, %r12
	je	.L953
.L955:
	movq	-7624(%rbp), %rax
	movl	testnum(%rip), %ecx
	movq	%rbx, %r8
	addq	$1, %r12
	movq	lengths(%rip), %rdx
	movl	$1, %r9d
	movq	16(%rax), %rdi
	movslq	(%rdx,%rcx,4), %rdx
	movq	-7832(%rbp), %rcx
	movq	%rdi, %rsi
	call	Camellia_cbc_encrypt@PLT
	movl	run(%rip), %edx
	testl	%edx, %edx
	jne	.L1907
.L953:
	movl	usertime(%rip), %esi
	movl	$1, %edi
	call	app_tminterval@PLT
	xorl	%edi, %edi
	movsd	%xmm0, -7776(%rbp)
	call	alarm@PLT
	movl	mr(%rip), %r8d
	movl	%r12d, %edx
	leaq	.LC17(%rip), %rax
	leaq	.LC16(%rip), %rsi
	movsd	-7776(%rbp), %xmm0
	movq	168+names(%rip), %rcx
	testl	%r8d, %r8d
	movq	bio_err(%rip), %rdi
	movslq	testnum(%rip), %rbx
	cmove	%rax, %rsi
	movl	$1, %eax
	call	BIO_printf@PLT
	movq	lengths(%rip), %rax
	pxor	%xmm1, %xmm1
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%r12d, %xmm0
	divsd	-7776(%rbp), %xmm0
	movl	testnum(%rip), %ecx
	cvtsi2sdl	(%rax,%rbx,4), %xmm1
	leal	1(%rcx), %edx
	movl	%edx, testnum(%rip)
	mulsd	%xmm1, %xmm0
	movsd	%xmm0, 1008(%r14,%rbx,8)
	cmpl	-7640(%rbp), %edx
	jb	.L957
	movl	-7784(%rbp), %r12d
.L951:
	movl	-7240(%rbp), %ebx
	testl	%ebx, %ebx
	je	.L949
	movl	-7628(%rbp), %esi
	testl	%esi, %esi
	je	.L960
	movq	80+names(%rip), %rdx
	movq	bio_err(%rip), %rdi
	leaq	.LC96(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movl	$0, -7240(%rbp)
.L960:
	movl	$0, testnum(%rip)
	movl	-7672(%rbp), %ecx
	testl	%ecx, %ecx
	jne	.L949
	movl	%r12d, -7784(%rbp)
	movq	lengths(%rip), %rcx
	xorl	%eax, %eax
	leaq	results(%rip), %r14
.L966:
	movl	mr(%rip), %edx
	leaq	.LC15(%rip), %rsi
	movl	(%rcx,%rax,4), %r8d
	movl	-7648(%rbp), %ebx
	movq	bio_err(%rip), %rdi
	testl	%edx, %edx
	leaq	.LC14(%rip), %rdx
	cmovne	%rdx, %rsi
	movq	80+names(%rip), %rdx
	movl	%ebx, %ecx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	call	BIO_ctrl@PLT
	movl	%ebx, %edi
	movl	$1, run(%rip)
	call	alarm@PLT
	movl	usertime(%rip), %esi
	xorl	%edi, %edi
	call	app_tminterval@PLT
	movl	run(%rip), %r12d
	testl	%r12d, %r12d
	je	.L962
	xorl	%r12d, %r12d
	leaq	iv(%rip), %rbx
	jmp	.L964
	.p2align 4,,10
	.p2align 3
.L1908:
	cmpq	$2147483647, %r12
	je	.L962
.L964:
	movq	-7624(%rbp), %rax
	movl	testnum(%rip), %ecx
	movq	%rbx, %r8
	addq	$1, %r12
	movq	lengths(%rip), %rdx
	movl	$1, %r9d
	movq	16(%rax), %rdi
	movslq	(%rdx,%rcx,4), %rdx
	movq	-7816(%rbp), %rcx
	movq	%rdi, %rsi
	call	IDEA_cbc_encrypt@PLT
	movl	run(%rip), %edx
	testl	%edx, %edx
	jne	.L1908
.L962:
	movl	usertime(%rip), %esi
	movl	$1, %edi
	call	app_tminterval@PLT
	xorl	%edi, %edi
	movsd	%xmm0, -7776(%rbp)
	call	alarm@PLT
	movl	mr(%rip), %eax
	movl	%r12d, %edx
	leaq	.LC16(%rip), %rsi
	movsd	-7776(%rbp), %xmm0
	movq	80+names(%rip), %rcx
	testl	%eax, %eax
	leaq	.LC17(%rip), %rax
	movq	bio_err(%rip), %rdi
	movslq	testnum(%rip), %rbx
	cmove	%rax, %rsi
	movl	$1, %eax
	call	BIO_printf@PLT
	movq	lengths(%rip), %rcx
	pxor	%xmm1, %xmm1
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%r12d, %xmm0
	divsd	-7776(%rbp), %xmm0
	movl	testnum(%rip), %eax
	cvtsi2sdl	(%rcx,%rbx,4), %xmm1
	addl	$1, %eax
	movl	%eax, testnum(%rip)
	mulsd	%xmm1, %xmm0
	movsd	%xmm0, 480(%r14,%rbx,8)
	cmpl	-7640(%rbp), %eax
	jb	.L966
	movl	-7784(%rbp), %r12d
.L949:
	movl	-7236(%rbp), %edi
	testl	%edi, %edi
	je	.L959
	movl	-7628(%rbp), %r10d
	testl	%r10d, %r10d
	je	.L970
	movq	88+names(%rip), %rdx
	movq	bio_err(%rip), %rdi
	leaq	.LC96(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movl	$0, -7236(%rbp)
.L970:
	movl	-7672(%rbp), %r9d
	movl	$0, testnum(%rip)
	testl	%r9d, %r9d
	jne	.L959
	movl	%r12d, -7784(%rbp)
	movq	lengths(%rip), %rcx
	xorl	%eax, %eax
	leaq	results(%rip), %r14
.L976:
	movl	mr(%rip), %r8d
	movl	-7648(%rbp), %ebx
	leaq	.LC14(%rip), %rdx
	leaq	.LC15(%rip), %rsi
	movq	bio_err(%rip), %rdi
	testl	%r8d, %r8d
	movl	(%rcx,%rax,4), %r8d
	movl	%ebx, %ecx
	cmovne	%rdx, %rsi
	movq	88+names(%rip), %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	call	BIO_ctrl@PLT
	movl	%ebx, %edi
	movl	$1, run(%rip)
	call	alarm@PLT
	movl	usertime(%rip), %esi
	xorl	%edi, %edi
	call	app_tminterval@PLT
	movl	run(%rip), %r12d
	testl	%r12d, %r12d
	je	.L972
	xorl	%r12d, %r12d
	leaq	iv(%rip), %rbx
	jmp	.L974
	.p2align 4,,10
	.p2align 3
.L1909:
	cmpq	$2147483647, %r12
	je	.L972
.L974:
	movq	-7624(%rbp), %rax
	movl	testnum(%rip), %ecx
	movq	%rbx, %r8
	addq	$1, %r12
	movq	lengths(%rip), %rdx
	movl	$1, %r9d
	movq	16(%rax), %rdi
	movslq	(%rdx,%rcx,4), %rdx
	movq	-7800(%rbp), %rcx
	movq	%rdi, %rsi
	call	SEED_cbc_encrypt@PLT
	movl	run(%rip), %edx
	testl	%edx, %edx
	jne	.L1909
.L972:
	movl	usertime(%rip), %esi
	movl	$1, %edi
	call	app_tminterval@PLT
	xorl	%edi, %edi
	movsd	%xmm0, -7776(%rbp)
	call	alarm@PLT
	movl	mr(%rip), %edi
	movl	%r12d, %edx
	leaq	.LC17(%rip), %rax
	leaq	.LC16(%rip), %rsi
	movsd	-7776(%rbp), %xmm0
	movq	88+names(%rip), %rcx
	testl	%edi, %edi
	movq	bio_err(%rip), %rdi
	movslq	testnum(%rip), %rbx
	cmove	%rax, %rsi
	movl	$1, %eax
	call	BIO_printf@PLT
	movq	lengths(%rip), %rcx
	pxor	%xmm1, %xmm1
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%r12d, %xmm0
	divsd	-7776(%rbp), %xmm0
	movl	testnum(%rip), %eax
	cvtsi2sdl	(%rcx,%rbx,4), %xmm1
	addl	$1, %eax
	movl	%eax, testnum(%rip)
	mulsd	%xmm1, %xmm0
	movsd	%xmm0, 528(%r14,%rbx,8)
	cmpl	-7640(%rbp), %eax
	jb	.L976
	movl	-7784(%rbp), %r12d
.L959:
	movl	-7232(%rbp), %r11d
	testl	%r11d, %r11d
	je	.L968
	movl	-7628(%rbp), %esi
	testl	%esi, %esi
	jne	.L1910
.L977:
	movl	$0, testnum(%rip)
	movl	-7672(%rbp), %ecx
	testl	%ecx, %ecx
	jne	.L968
	movl	%r13d, -7792(%rbp)
	movq	lengths(%rip), %rcx
	xorl	%edx, %edx
	movl	%r12d, -7784(%rbp)
	movq	-7624(%rbp), %rbx
	movq	-7848(%rbp), %r12
.L984:
	movl	mr(%rip), %eax
	leaq	.LC15(%rip), %rsi
	movl	(%rcx,%rdx,4), %r8d
	movl	-7648(%rbp), %r14d
	movq	96+names(%rip), %rdx
	testl	%eax, %eax
	leaq	.LC14(%rip), %rax
	movq	bio_err(%rip), %rdi
	cmovne	%rax, %rsi
	movl	%r14d, %ecx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movl	$11, %esi
	call	BIO_ctrl@PLT
	movl	%r14d, %edi
	movl	$1, run(%rip)
	call	alarm@PLT
	movl	-7628(%rbp), %edx
	testl	%edx, %edx
	jne	.L1883
	movl	usertime(%rip), %esi
	xorl	%edi, %edi
	call	app_tminterval@PLT
	movl	run(%rip), %r13d
	testl	%r13d, %r13d
	je	.L980
	xorl	%r13d, %r13d
	leaq	iv(%rip), %r14
	jmp	.L982
	.p2align 4,,10
	.p2align 3
.L1911:
	cmpq	$2147483647, %r13
	je	.L980
.L982:
	movl	testnum(%rip), %edx
	movq	16(%rbx), %rdi
	movq	%r14, %r8
	movq	%r12, %rcx
	movq	lengths(%rip), %rax
	movl	$1, %r9d
	addq	$1, %r13
	movq	%rdi, %rsi
	movslq	(%rax,%rdx,4), %rdx
	call	RC2_cbc_encrypt@PLT
	movl	run(%rip), %eax
	testl	%eax, %eax
	jne	.L1911
.L980:
	movl	usertime(%rip), %esi
	movl	$1, %edi
	call	app_tminterval@PLT
	xorl	%edi, %edi
	movsd	%xmm0, -7776(%rbp)
	call	alarm@PLT
	movl	mr(%rip), %eax
	movl	%r13d, %edx
	leaq	.LC16(%rip), %rsi
	movsd	-7776(%rbp), %xmm0
	movq	96+names(%rip), %rcx
	testl	%eax, %eax
	leaq	.LC17(%rip), %rax
	movq	bio_err(%rip), %rdi
	movl	testnum(%rip), %r14d
	cmove	%rax, %rsi
	movl	$1, %eax
	call	BIO_printf@PLT
	movslq	%r14d, %rsi
	pxor	%xmm1, %xmm1
	pxor	%xmm0, %xmm0
	movq	lengths(%rip), %rax
	cvtsi2sdl	%r13d, %xmm0
	divsd	-7776(%rbp), %xmm0
	leaq	results(%rip), %rdi
	cvtsi2sdl	(%rax,%rsi,4), %xmm1
	movq	%rax, %rcx
	mulsd	%xmm1, %xmm0
	movsd	%xmm0, 576(%rdi,%rsi,8)
	movl	testnum(%rip), %esi
	leal	1(%rsi), %edx
	movl	%edx, testnum(%rip)
	cmpl	-7640(%rbp), %edx
	jb	.L984
	movq	%rdi, %r14
	movl	-7224(%rbp), %edi
	movl	-7784(%rbp), %r12d
	movl	-7792(%rbp), %r13d
	testl	%edi, %edi
	je	.L1912
	movl	$0, testnum(%rip)
.L1203:
	movl	%r12d, -7784(%rbp)
	xorl	%edx, %edx
.L996:
	movl	mr(%rip), %ebx
	leaq	.LC14(%rip), %rcx
	movl	(%rax,%rdx,4), %r8d
	leaq	.LC15(%rip), %rsi
	movq	112+names(%rip), %rdx
	movq	bio_err(%rip), %rdi
	testl	%ebx, %ebx
	movl	-7648(%rbp), %ebx
	cmovne	%rcx, %rsi
	xorl	%eax, %eax
	movl	%ebx, %ecx
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	call	BIO_ctrl@PLT
	movl	%ebx, %edi
	movl	$1, run(%rip)
	call	alarm@PLT
	movl	usertime(%rip), %esi
	xorl	%edi, %edi
	call	app_tminterval@PLT
	movl	run(%rip), %r12d
	testl	%r12d, %r12d
	je	.L992
	xorl	%r12d, %r12d
	leaq	iv(%rip), %rbx
	jmp	.L994
	.p2align 4,,10
	.p2align 3
.L1913:
	cmpq	$2147483647, %r12
	je	.L992
.L994:
	movq	-7624(%rbp), %rax
	movl	testnum(%rip), %ecx
	movq	%rbx, %r8
	addq	$1, %r12
	movq	lengths(%rip), %rdx
	movl	$1, %r9d
	movq	16(%rax), %rdi
	movslq	(%rdx,%rcx,4), %rdx
	movq	-7744(%rbp), %rcx
	movq	%rdi, %rsi
	call	BF_cbc_encrypt@PLT
	movl	run(%rip), %edx
	testl	%edx, %edx
	jne	.L1913
.L992:
	movl	usertime(%rip), %esi
	movl	$1, %edi
	call	app_tminterval@PLT
	xorl	%edi, %edi
	movsd	%xmm0, -7776(%rbp)
	call	alarm@PLT
	movl	mr(%rip), %r11d
	movl	%r12d, %edx
	leaq	.LC17(%rip), %rax
	leaq	.LC16(%rip), %rsi
	movsd	-7776(%rbp), %xmm0
	movq	112+names(%rip), %rcx
	testl	%r11d, %r11d
	movq	bio_err(%rip), %rdi
	movslq	testnum(%rip), %rbx
	cmove	%rax, %rsi
	movl	$1, %eax
	call	BIO_printf@PLT
	movq	lengths(%rip), %rax
	pxor	%xmm1, %xmm1
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%r12d, %xmm0
	divsd	-7776(%rbp), %xmm0
	movl	testnum(%rip), %ecx
	cvtsi2sdl	(%rax,%rbx,4), %xmm1
	leal	1(%rcx), %edx
	movl	%edx, testnum(%rip)
	mulsd	%xmm1, %xmm0
	movsd	%xmm0, 672(%r14,%rbx,8)
	cmpl	-7640(%rbp), %edx
	jb	.L996
	movl	-7784(%rbp), %r12d
.L990:
	movl	-7220(%rbp), %eax
	testl	%eax, %eax
	je	.L988
	movl	-7628(%rbp), %r9d
	testl	%r9d, %r9d
	je	.L999
	movq	120+names(%rip), %rdx
	movq	bio_err(%rip), %rdi
	leaq	.LC96(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movl	$0, -7220(%rbp)
.L999:
	movl	-7672(%rbp), %r8d
	movl	$0, testnum(%rip)
	testl	%r8d, %r8d
	jne	.L988
	movl	%r12d, -7776(%rbp)
	movq	lengths(%rip), %rcx
	xorl	%eax, %eax
	leaq	results(%rip), %r14
.L1005:
	movl	mr(%rip), %edi
	movl	-7648(%rbp), %ebx
	leaq	.LC14(%rip), %rdx
	leaq	.LC15(%rip), %rsi
	movl	(%rcx,%rax,4), %r8d
	testl	%edi, %edi
	movq	bio_err(%rip), %rdi
	movl	%ebx, %ecx
	cmovne	%rdx, %rsi
	movq	120+names(%rip), %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	call	BIO_ctrl@PLT
	movl	%ebx, %edi
	movl	$1, run(%rip)
	call	alarm@PLT
	movl	usertime(%rip), %esi
	xorl	%edi, %edi
	call	app_tminterval@PLT
	movl	run(%rip), %r12d
	testl	%r12d, %r12d
	je	.L1001
	xorl	%r12d, %r12d
	leaq	iv(%rip), %rbx
	jmp	.L1003
	.p2align 4,,10
	.p2align 3
.L1914:
	cmpq	$2147483647, %r12
	je	.L1001
.L1003:
	movq	-7624(%rbp), %rax
	movl	testnum(%rip), %ecx
	movq	%rbx, %r8
	addq	$1, %r12
	movq	lengths(%rip), %rdx
	movl	$1, %r9d
	movq	16(%rax), %rdi
	movslq	(%rdx,%rcx,4), %rdx
	movq	-7808(%rbp), %rcx
	movq	%rdi, %rsi
	call	CAST_cbc_encrypt@PLT
	movl	run(%rip), %edx
	testl	%edx, %edx
	jne	.L1914
.L1001:
	movl	usertime(%rip), %esi
	movl	$1, %edi
	call	app_tminterval@PLT
	xorl	%edi, %edi
	movsd	%xmm0, -7744(%rbp)
	call	alarm@PLT
	movl	mr(%rip), %esi
	movl	%r12d, %edx
	leaq	.LC17(%rip), %rax
	movsd	-7744(%rbp), %xmm0
	movq	120+names(%rip), %rcx
	testl	%esi, %esi
	leaq	.LC16(%rip), %rsi
	movq	bio_err(%rip), %rdi
	movslq	testnum(%rip), %rbx
	cmove	%rax, %rsi
	movl	$1, %eax
	call	BIO_printf@PLT
	movq	lengths(%rip), %rcx
	pxor	%xmm1, %xmm1
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%r12d, %xmm0
	divsd	-7744(%rbp), %xmm0
	movl	testnum(%rip), %eax
	cvtsi2sdl	(%rcx,%rbx,4), %xmm1
	addl	$1, %eax
	movl	%eax, testnum(%rip)
	mulsd	%xmm1, %xmm0
	movsd	%xmm0, 720(%r14,%rbx,8)
	cmpl	-7640(%rbp), %eax
	jb	.L1005
	movl	-7776(%rbp), %r12d
.L988:
	movl	-7632(%rbp), %r10d
	testl	%r10d, %r10d
	je	.L998
	movl	%r12d, -7776(%rbp)
	movq	lengths(%rip), %rcx
	xorl	%eax, %eax
	leaq	.LC14(%rip), %r14
	movl	$0, testnum(%rip)
.L1011:
	movl	mr(%rip), %edx
	leaq	.LC15(%rip), %rsi
	movl	(%rcx,%rax,4), %r8d
	movl	-7648(%rbp), %ebx
	movq	bio_err(%rip), %rdi
	testl	%edx, %edx
	movq	240+names(%rip), %rdx
	cmovne	%r14, %rsi
	movl	%ebx, %ecx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	call	BIO_ctrl@PLT
	movl	%ebx, %edi
	movl	$1, run(%rip)
	call	alarm@PLT
	movl	usertime(%rip), %esi
	xorl	%edi, %edi
	call	app_tminterval@PLT
	movq	-7624(%rbp), %rdx
	movl	-7628(%rbp), %edi
	leaq	RAND_bytes_loop(%rip), %rsi
	call	run_benchmark
	movl	usertime(%rip), %esi
	movl	$1, %edi
	movl	%eax, %r12d
	call	app_tminterval@PLT
	xorl	%edi, %edi
	movsd	%xmm0, -7744(%rbp)
	call	alarm@PLT
	movslq	testnum(%rip), %rbx
	cmpl	$-1, %r12d
	je	.L1881
	movl	mr(%rip), %eax
	leaq	.LC16(%rip), %rsi
	movsd	-7744(%rbp), %xmm0
	movl	%r12d, %edx
	movq	240+names(%rip), %rcx
	movq	bio_err(%rip), %rdi
	testl	%eax, %eax
	leaq	.LC17(%rip), %rax
	cmove	%rax, %rsi
	movl	$1, %eax
	call	BIO_printf@PLT
	movq	lengths(%rip), %rcx
	pxor	%xmm1, %xmm1
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%r12d, %xmm0
	divsd	-7744(%rbp), %xmm0
	leaq	results(%rip), %rax
	cvtsi2sdl	(%rcx,%rbx,4), %xmm1
	mulsd	%xmm1, %xmm0
	movsd	%xmm0, 1440(%rax,%rbx,8)
	movl	testnum(%rip), %eax
	addl	$1, %eax
	movl	%eax, testnum(%rip)
	cmpl	-7640(%rbp), %eax
	jb	.L1011
	movl	-7776(%rbp), %r12d
.L998:
	movl	-7192(%rbp), %ecx
	testl	%ecx, %ecx
	je	.L1007
	movq	-7656(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1012
	testl	%r12d, %r12d
	je	.L1013
	call	EVP_CIPHER_flags@PLT
	testl	$4194304, %eax
	jne	.L1915
.L1013:
	movq	-7656(%rbp), %rbx
	movq	%rbx, %rdi
	call	EVP_CIPHER_nid@PLT
	movl	%eax, %edi
	call	OBJ_nid2ln@PLT
	movq	%rbx, %rdi
	movq	%rax, 176+names(%rip)
	call	EVP_CIPHER_flags@PLT
	leaq	EVP_Update_loop_ccm(%rip), %rcx
	andl	$983047, %eax
	movq	%rcx, -7744(%rbp)
	cmpq	$7, %rax
	je	.L1033
	leaq	EVP_Update_loop(%rip), %rax
	movq	%rax, -7744(%rbp)
	testl	%r13d, %r13d
	jne	.L1916
.L1033:
	movq	-7624(%rbp), %rcx
	movq	-7688(%rbp), %rax
	movl	$0, testnum(%rip)
	movl	%r15d, -7792(%rbp)
	movq	lengths(%rip), %rdx
	leaq	744(%rcx,%rax), %r13
	addq	$744, %rcx
	xorl	%eax, %eax
	movq	-7656(%rbp), %r14
	movq	%rcx, -7784(%rbp)
.L1042:
	movl	mr(%rip), %r9d
	movl	-7648(%rbp), %ebx
	leaq	.LC14(%rip), %rcx
	leaq	.LC15(%rip), %rsi
	movl	(%rdx,%rax,4), %r8d
	movq	bio_err(%rip), %rdi
	leaq	iv(%rip), %r12
	testl	%r9d, %r9d
	movq	176+names(%rip), %rdx
	cmovne	%rcx, %rsi
	movl	%ebx, %ecx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	call	BIO_ctrl@PLT
	movl	%ebx, %edi
	movl	$1, run(%rip)
	call	alarm@PLT
	movq	-7784(%rbp), %rbx
	movq	%rbx, -7776(%rbp)
.L1038:
	call	EVP_CIPHER_CTX_new@PLT
	movq	%rax, (%rbx)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1917
	movl	decrypt(%rip), %r8d
	xorl	%r9d, %r9d
	movq	%r14, %rsi
	testl	%r8d, %r8d
	movq	%r12, %r8
	sete	%r9b
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	call	EVP_CipherInit_ex@PLT
	testl	%eax, %eax
	je	.L1882
	movq	(%rbx), %rdi
	xorl	%esi, %esi
	call	EVP_CIPHER_CTX_set_padding@PLT
	movq	(%rbx), %rdi
	call	EVP_CIPHER_CTX_key_length@PLT
	leaq	.LC100(%rip), %rsi
	movl	%eax, %edi
	movl	%eax, %r15d
	call	app_malloc@PLT
	movq	(%rbx), %rdi
	movq	%rax, -696(%rbx)
	movq	%rax, %rsi
	call	EVP_CIPHER_CTX_rand_key@PLT
	movq	-696(%rbx), %rcx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	movl	$-1, %r9d
	call	EVP_CipherInit_ex@PLT
	testl	%eax, %eax
	je	.L1882
	movq	-696(%rbx), %rdi
	movslq	%r15d, %rsi
	movl	$2652, %ecx
	leaq	.LC101(%rip), %rdx
	addq	$768, %rbx
	call	CRYPTO_clear_free@PLT
	cmpq	%rbx, %r13
	jne	.L1038
	movl	usertime(%rip), %esi
	xorl	%edi, %edi
	call	app_tminterval@PLT
	movq	-7624(%rbp), %rdx
	movq	-7744(%rbp), %rsi
	movl	-7628(%rbp), %edi
	call	run_benchmark
	movl	usertime(%rip), %esi
	movl	$1, %edi
	movl	%eax, %ebx
	call	app_tminterval@PLT
	xorl	%edi, %edi
	movsd	%xmm0, -7656(%rbp)
	call	alarm@PLT
	movq	-7776(%rbp), %r12
.L1039:
	movq	(%r12), %rdi
	addq	$768, %r12
	call	EVP_CIPHER_CTX_free@PLT
	cmpq	%r12, %r13
	jne	.L1039
	movslq	testnum(%rip), %r12
	cmpl	$-1, %ebx
	je	.L1881
	movl	mr(%rip), %esi
	leaq	.LC17(%rip), %rax
	movl	%ebx, %edx
	movsd	-7656(%rbp), %xmm0
	movq	176+names(%rip), %rcx
	movq	bio_err(%rip), %rdi
	testl	%esi, %esi
	leaq	.LC16(%rip), %rsi
	cmove	%rax, %rsi
	movl	$1, %eax
	call	BIO_printf@PLT
	movq	lengths(%rip), %rdx
	pxor	%xmm1, %xmm1
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%ebx, %xmm0
	divsd	-7656(%rbp), %xmm0
	leaq	results(%rip), %rax
	cvtsi2sdl	(%rdx,%r12,4), %xmm1
	mulsd	%xmm1, %xmm0
	movsd	%xmm0, 1056(%rax,%r12,8)
	movl	testnum(%rip), %eax
	addl	$1, %eax
	movl	%eax, testnum(%rip)
	cmpl	-7640(%rbp), %eax
	jb	.L1042
	movl	-7792(%rbp), %r15d
.L1007:
	movq	-7624(%rbp), %rax
	movq	-7688(%rbp), %rcx
	leaq	16(%rax), %r13
	leaq	16(%rax,%rcx), %rax
	movq	%rax, -7656(%rbp)
	movq	%r13, %rbx
.L1047:
	movq	(%rbx), %rdi
	movl	$36, %esi
	call	RAND_bytes@PLT
	testl	%eax, %eax
	jle	.L1361
	addq	$768, %rbx
	cmpq	%rbx, -7656(%rbp)
	jne	.L1047
	movl	$0, testnum(%rip)
	xorl	%eax, %eax
	xorl	%edx, %edx
	movl	-7504(%rbp,%rax,4), %r14d
	leal	1(%rdx), %eax
	testl	%r14d, %r14d
	jne	.L1918
.L1049:
	movl	%eax, testnum(%rip)
	cmpl	$6, %eax
	ja	.L1068
	movl	-7504(%rbp,%rax,4), %r14d
	movl	%eax, %edx
	leal	1(%rdx), %eax
	testl	%r14d, %r14d
	je	.L1049
.L1918:
	movq	-7624(%rbp), %r12
.L1048:
	cmpl	$2, -7596(%rbp)
	jle	.L1051
	call	BN_new@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L1361
	movl	$65537, %esi
	movq	%rax, %rdi
	call	BN_set_word@PLT
	testl	%eax, %eax
	je	.L1878
	movl	testnum(%rip), %eax
	leaq	rsa_choices(%rip), %rcx
	movq	bio_err(%rip), %rdi
	leaq	.LC115(%rip), %rsi
	salq	$4, %rax
	movq	(%rcx,%rax), %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movl	testnum(%rip), %ebx
	call	RSA_new@PLT
	movq	%rax, 72(%r12,%rbx,8)
	movl	testnum(%rip), %eax
	movq	72(%r12,%rax,8), %rdi
	testq	%rdi, %rdi
	je	.L1878
	leaq	rsa_bits.27419(%rip), %rdx
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	(%rdx,%rax,4), %esi
	movl	-7596(%rbp), %edx
	call	RSA_generate_multi_prime_key@PLT
	movq	%r14, %rdi
	testl	%eax, %eax
	je	.L1919
	call	BN_free@PLT
.L1051:
	movl	testnum(%rip), %eax
	movq	24(%r12), %rcx
	leaq	56(%r12), %r8
	movl	$36, %edx
	movq	16(%r12), %rsi
	movl	$114, %edi
	movq	72(%r12,%rax,8), %r9
	call	RSA_sign@PLT
	testl	%eax, %eax
	je	.L1055
	addq	$768, %r12
	cmpq	%r12, -7680(%rbp)
	jne	.L1048
	movl	mr(%rip), %r11d
	movl	testnum(%rip), %eax
	leaq	rsa_bits.27419(%rip), %r12
	leaq	.LC12(%rip), %rsi
	movl	-7664(%rbp), %ebx
	movq	bio_err(%rip), %rdi
	leaq	.LC46(%rip), %r8
	leaq	.LC117(%rip), %rcx
	testl	%r11d, %r11d
	movl	(%r12,%rax,4), %edx
	leaq	.LC13(%rip), %rax
	cmove	%rax, %rsi
	movl	%ebx, %r9d
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	call	BIO_ctrl@PLT
	movl	%ebx, %edi
	movl	$1, run(%rip)
	call	alarm@PLT
	movl	usertime(%rip), %esi
	xorl	%edi, %edi
	call	app_tminterval@PLT
	movq	-7624(%rbp), %rdx
	movl	-7628(%rbp), %edi
	leaq	RSA_sign_loop(%rip), %rsi
	call	run_benchmark
	movl	usertime(%rip), %esi
	movl	$1, %edi
	movslq	%eax, %rbx
	call	app_tminterval@PLT
	xorl	%edi, %edi
	movsd	%xmm0, -7648(%rbp)
	call	alarm@PLT
	movl	testnum(%rip), %eax
	movq	%rbx, %rdx
	leaq	.LC18(%rip), %rsi
	movsd	-7648(%rbp), %xmm0
	movq	bio_err(%rip), %rdi
	movl	(%r12,%rax,4), %ecx
	movl	mr(%rip), %r12d
	leaq	.LC19(%rip), %rax
	testl	%r12d, %r12d
	cmove	%rax, %rsi
	movl	$1, %eax
	call	BIO_printf@PLT
	movl	testnum(%rip), %eax
	leaq	rsa_results(%rip), %rcx
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%ebx, %xmm0
	divsd	-7648(%rbp), %xmm0
	movq	%rax, %rdx
	salq	$4, %rdx
	movsd	%xmm0, (%rcx,%rdx)
.L1057:
	movq	-7624(%rbp), %r12
.L1061:
	movq	24(%r12), %rcx
	movq	16(%r12), %rsi
	movl	$36, %edx
	movl	$114, %edi
	movq	72(%r12,%rax,8), %r9
	movl	56(%r12), %r8d
	call	RSA_verify@PLT
	testl	%eax, %eax
	jle	.L1060
	movl	testnum(%rip), %eax
	addq	$768, %r12
	cmpq	%r12, -7680(%rbp)
	jne	.L1061
	movl	mr(%rip), %r9d
	leaq	rsa_bits.27419(%rip), %r12
	movl	-7664(%rbp), %r14d
	leaq	.LC12(%rip), %rsi
	movl	(%r12,%rax,4), %edx
	leaq	.LC13(%rip), %rax
	movq	bio_err(%rip), %rdi
	leaq	.LC46(%rip), %r8
	testl	%r9d, %r9d
	leaq	.LC119(%rip), %rcx
	movl	%r14d, %r9d
	cmove	%rax, %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	call	BIO_ctrl@PLT
	movl	%r14d, %edi
	movl	$1, run(%rip)
	call	alarm@PLT
	movl	usertime(%rip), %esi
	xorl	%edi, %edi
	call	app_tminterval@PLT
	movq	-7624(%rbp), %rdx
	movl	-7628(%rbp), %edi
	leaq	RSA_verify_loop(%rip), %rsi
	call	run_benchmark
	movl	usertime(%rip), %esi
	movl	$1, %edi
	movslq	%eax, %rdx
	movq	%rdx, %r14
	movq	%rdx, -7744(%rbp)
	call	app_tminterval@PLT
	xorl	%edi, %edi
	movsd	%xmm0, -7648(%rbp)
	call	alarm@PLT
	movl	testnum(%rip), %eax
	movl	mr(%rip), %r10d
	leaq	.LC20(%rip), %rsi
	movsd	-7648(%rbp), %xmm0
	movq	-7744(%rbp), %rdx
	movl	(%r12,%rax,4), %ecx
	testl	%r10d, %r10d
	leaq	.LC21(%rip), %rax
	movq	bio_err(%rip), %rdi
	cmove	%rax, %rsi
	movl	$1, %eax
	call	BIO_printf@PLT
	movl	testnum(%rip), %eax
	leaq	rsa_results(%rip), %rcx
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%r14d, %xmm0
	divsd	-7648(%rbp), %xmm0
	movq	%rax, %rdx
	salq	$4, %rax
	movsd	%xmm0, 8(%rax,%rcx)
.L1063:
	leal	1(%rdx), %eax
	cmpq	$1, %rbx
	jg	.L1049
	cmpl	$6, %eax
	ja	.L1920
	movl	$5, %ecx
	movl	%ecx, %esi
	subl	%edx, %esi
	leaq	-7504(%rbp,%rax,4), %rdx
	xorl	%eax, %eax
	leaq	4(,%rsi,4), %rcx
	movq	%rdx, %rdi
	movl	%ecx, %ecx
	rep stosb
	movl	$8, %eax
	jmp	.L1049
	.p2align 4,,10
	.p2align 3
.L575:
	leal	-1501(%rdi), %eax
	cmpl	$6, %eax
	ja	.L574
	movslq	(%rbx,%rax,4), %rax
	addq	%rbx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L579:
	.long	.L583-.L579
	.long	.L583-.L579
	.long	.L574-.L579
	.long	.L582-.L579
	.long	.L581-.L579
	.long	.L580-.L579
	.long	.L578-.L579
	.text
.L578:
	movl	$1, %r13d
	jmp	.L574
.L584:
	call	opt_arg@PLT
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%rax, %rdi
	call	strtol@PLT
	movl	%eax, -7628(%rbp)
	movq	%rax, %r14
	call	ASYNC_is_capable@PLT
	testl	%eax, %eax
	je	.L1921
	cmpl	$99999, %r14d
	jbe	.L574
	movq	-7624(%rbp), %rdx
	movq	bio_err(%rip), %rdi
	leaq	.LC41(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L595:
	movq	-7624(%rbp), %rdx
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
	movl	$1, %r15d
	leaq	.LC38(%rip), %rsi
	call	BIO_printf@PLT
.L597:
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	movq	$0, -7696(%rbp)
	movq	$0, -7624(%rbp)
.L1190:
	movq	-7624(%rbp), %rdi
	movl	$3384, %edx
	leaq	.LC101(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-7696(%rbp), %rdi
	call	release_engine@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1922
	addq	$7832, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L580:
	.cfi_restore_state
	call	opt_arg@PLT
	movl	$10, %edx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	strtol@PLT
	movl	$1, -7640(%rbp)
	movl	%eax, -7600(%rbp)
	leaq	-7600(%rbp), %rax
	movq	%rax, lengths(%rip)
	jmp	.L574
.L581:
	call	opt_arg@PLT
	movl	$10, %edx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	strtol@PLT
	movl	%eax, -7648(%rbp)
	movl	%eax, -7664(%rbp)
	jmp	.L574
.L582:
	call	opt_arg@PLT
	leaq	-7596(%rbp), %rsi
	movq	%rax, %rdi
	call	opt_int@PLT
	testl	%eax, %eax
	jne	.L574
.L600:
	movl	$1, %r15d
	jmp	.L597
.L583:
	call	opt_rand@PLT
	testl	%eax, %eax
	jne	.L574
	jmp	.L600
.L586:
	call	opt_arg@PLT
	leaq	-7604(%rbp), %rsi
	movq	%rax, %rdi
	call	opt_int@PLT
	testl	%eax, %eax
	je	.L600
	cmpl	$64, -7604(%rbp)
	jle	.L574
	movq	-7624(%rbp), %rdx
	movq	bio_err(%rip), %rdi
	movl	$64, %ecx
	xorl	%eax, %eax
	leaq	.LC42(%rip), %rsi
	call	BIO_printf@PLT
	jmp	.L595
.L588:
	movl	$1, mr(%rip)
	jmp	.L574
.L589:
	call	opt_arg@PLT
	movl	$10, %edx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	strtol@PLT
	movl	%eax, -7696(%rbp)
	jmp	.L574
.L590:
	call	opt_arg@PLT
	movq	%rax, -7680(%rbp)
	jmp	.L574
.L591:
	movl	$1, decrypt(%rip)
	jmp	.L574
.L592:
	movq	$0, evp_md(%rip)
	call	opt_arg@PLT
	movq	%rax, %rdi
	call	EVP_get_cipherbyname@PLT
	movq	%rax, -7656(%rbp)
	testq	%rax, %rax
	je	.L1923
.L598:
	movl	$1, -7192(%rbp)
	jmp	.L574
.L593:
	movl	$0, usertime(%rip)
	jmp	.L574
.L594:
	leaq	speed_options(%rip), %rdi
	xorl	%r15d, %r15d
	call	opt_help@PLT
	jmp	.L597
.L1885:
	movl	8(%r14), %eax
	movl	$1, -7280(%rbp,%rax,4)
	jmp	.L606
.L1886:
	movabsq	$4294967297, %rax
	movq	%rax, -7248(%rbp)
	jmp	.L606
.L1887:
	movabsq	$4294967297, %rax
	movl	$1, -7260(%rbp)
	movq	%rax, -7188(%rbp)
	jmp	.L606
.L1923:
	call	opt_arg@PLT
	movq	%rax, %rdi
	call	EVP_get_digestbyname@PLT
	movq	%rax, evp_md(%rip)
	testq	%rax, %rax
	jne	.L598
	call	opt_arg@PLT
	movq	-7624(%rbp), %rdx
	movq	bio_err(%rip), %rdi
	leaq	.LC39(%rip), %rsi
	movq	%rax, %rcx
	xorl	%eax, %eax
	movl	$1, %r15d
	call	BIO_printf@PLT
	jmp	.L597
	.p2align 4,,10
	.p2align 3
.L611:
	movdqa	.LC54(%rip), %xmm7
	movabsq	$4294967297, %rax
	movl	$1, -7480(%rbp)
	movq	%rax, -7488(%rbp)
	movaps	%xmm7, -7504(%rbp)
	jmp	.L606
.L1889:
	movl	$7, %ecx
	leaq	.LC56(%rip), %rdi
	movq	%r12, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L1215
	movl	$8, %ecx
	leaq	.LC57(%rip), %rdi
	movq	%r12, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L1216
	movl	$8, %ecx
	leaq	.LC58(%rip), %rdi
	movq	%r12, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L1924
	movl	$4, %ecx
	leaq	.LC59(%rip), %rdi
	movq	%r12, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L1925
	movl	$9, %ecx
	leaq	.LC60(%rip), %rdi
	movq	%r12, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L1926
	movl	$6, %ecx
	leaq	.LC61(%rip), %rdi
	movq	%r12, %rsi
	repz cmpsb
	leaq	ecdsa_choices(%rip), %r14
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L1927
.L619:
	movq	(%r14), %rsi
	movq	%r12, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L1928
	addq	$16, %r14
	leaq	352+ecdsa_choices(%rip), %rax
	cmpq	%rax, %r14
	jne	.L619
	movl	$5, %ecx
	leaq	.LC62(%rip), %rdi
	movq	%r12, %rsi
	repz cmpsb
	leaq	ecdh_choices(%rip), %r14
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L1929
.L621:
	movq	(%r14), %rsi
	movq	%r12, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L1930
	addq	$16, %r14
	leaq	384+ecdh_choices(%rip), %rax
	cmpq	%rax, %r14
	jne	.L621
	movl	$6, %ecx
	leaq	.LC63(%rip), %rdi
	movq	%r12, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L623
	movq	-7672(%rbp), %rsi
	movq	%r12, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L1220
	movq	-7688(%rbp), %rsi
	movq	%r12, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L1931
	movq	-7624(%rbp), %rdx
	movq	bio_err(%rip), %rdi
	movq	%r12, %rcx
	xorl	%eax, %eax
	leaq	.LC64(%rip), %rsi
	movl	$1, %r15d
	call	BIO_printf@PLT
	jmp	.L597
	.p2align 4,,10
	.p2align 3
.L1890:
	testl	%r12d, %r12d
	je	.L630
	cmpq	$0, -7656(%rbp)
	je	.L1932
.L631:
	movq	-7656(%rbp), %rdi
	call	EVP_CIPHER_flags@PLT
	testl	$4194304, %eax
	je	.L1933
	movl	-7628(%rbp), %r14d
	testl	%r14d, %r14d
	jne	.L1934
.L633:
	movl	$0, -7672(%rbp)
	jmp	.L634
.L1892:
	movq	-7656(%rbp), %rdi
	movl	%r13d, %r15d
	call	EVP_CIPHER_nid@PLT
	movl	%eax, %edi
	call	OBJ_nid2ln@PLT
	movq	bio_err(%rip), %rdi
	leaq	.LC66(%rip), %rsi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L597
.L1900:
	movq	bio_err(%rip), %rdi
	movl	%ebx, %edx
	leaq	.LC92(%rip), %rsi
	movl	$1, %r15d
	call	BIO_printf@PLT
	movq	-7624(%rbp), %rax
	subq	$-128, %rax
	movq	%rax, -7704(%rbp)
.L636:
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	movq	-7624(%rbp), %rax
	movq	-7688(%rbp), %rcx
	movl	%r15d, -7640(%rbp)
	movq	-7704(%rbp), %r13
	leaq	328(%rax), %r12
	leaq	520(%rax), %rbx
	leaq	520(%rax,%rcx), %r14
	.p2align 4,,10
	.p2align 3
.L1187:
	movq	-488(%rbx), %rdi
	movl	$3353, %edx
	leaq	.LC101(%rip), %rsi
	leaq	-448(%rbx), %r15
	call	CRYPTO_free@PLT
	movq	-480(%rbx), %rdi
	movl	$3354, %edx
	leaq	.LC101(%rip), %rsi
	call	CRYPTO_free@PLT
	.p2align 4,,10
	.p2align 3
.L1184:
	movq	(%r15), %rdi
	addq	$8, %r15
	call	RSA_free@PLT
	cmpq	%r13, %r15
	jne	.L1184
	movq	-392(%rbx), %rdi
	leaq	-368(%rbx), %r15
	call	DSA_free@PLT
	movq	-384(%rbx), %rdi
	call	DSA_free@PLT
	movq	-376(%rbx), %rdi
	call	DSA_free@PLT
	.p2align 4,,10
	.p2align 3
.L1185:
	movq	(%r15), %rdi
	addq	$8, %r15
	call	EC_KEY_free@PLT
	cmpq	%r15, %r12
	jne	.L1185
	.p2align 4,,10
	.p2align 3
.L1186:
	movq	(%r15), %rdi
	addq	$8, %r15
	call	EVP_PKEY_CTX_free@PLT
	cmpq	%rbx, %r15
	jne	.L1186
	movq	(%r15), %rdi
	leaq	768(%r15), %rbx
	addq	$768, %r12
	addq	$768, %r13
	call	EVP_MD_CTX_free@PLT
	movq	8(%r15), %rdi
	call	EVP_MD_CTX_free@PLT
	movq	16(%r15), %rdi
	movl	$3371, %edx
	leaq	.LC101(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	24(%r15), %rdi
	movl	$3372, %edx
	leaq	.LC101(%rip), %rsi
	call	CRYPTO_free@PLT
	cmpq	%rbx, %r14
	jne	.L1187
	movl	-7628(%rbp), %r8d
	movl	-7640(%rbp), %r15d
	testl	%r8d, %r8d
	je	.L1197
	movq	-7760(%rbp), %rbx
	movq	-7712(%rbp), %r12
	.p2align 4,,10
	.p2align 3
.L1189:
	movq	(%rbx), %rdi
	addq	$768, %rbx
	call	ASYNC_WAIT_CTX_free@PLT
	cmpq	%rbx, %r12
	jne	.L1189
.L1197:
	movl	-7672(%rbp), %r9d
	testl	%r9d, %r9d
	je	.L1190
	call	ASYNC_cleanup_thread@PLT
	jmp	.L1190
.L1796:
	xorl	%eax, %eax
	jmp	.L794
	.p2align 4,,10
	.p2align 3
.L795:
	addq	$1, %rax
.L794:
	cmpq	$22, %rax
	je	.L795
	movq	-7720(%rbp), %rcx
	movl	$1, (%rcx,%rax,4)
	cmpl	$30, %eax
	jne	.L795
	movdqa	.LC54(%rip), %xmm1
	movabsq	$4294967297, %rax
	movl	$1, -7480(%rbp)
	movq	%rax, -7488(%rbp)
	movq	%rax, -7516(%rbp)
	movl	$1, -7508(%rbp)
	movq	%rax, -7392(%rbp)
	movq	%rax, -7524(%rbp)
	movaps	%xmm1, -7504(%rbp)
	movaps	%xmm1, -7472(%rbp)
	movaps	%xmm1, -7456(%rbp)
	movaps	%xmm1, -7440(%rbp)
	movaps	%xmm1, -7424(%rbp)
	movaps	%xmm1, -7408(%rbp)
	movaps	%xmm1, -7376(%rbp)
	movaps	%xmm1, -7360(%rbp)
	movaps	%xmm1, -7344(%rbp)
	movaps	%xmm1, -7328(%rbp)
	movaps	%xmm1, -7312(%rbp)
	movaps	%xmm1, -7296(%rbp)
	jmp	.L792
.L1949:
	movl	-7744(%rbp), %r15d
.L791:
	movq	-7696(%rbp), %rdi
	leaq	-7524(%rbp), %r14
	call	free@PLT
	movl	mr(%rip), %eax
	movq	$0, -7696(%rbp)
	movl	%eax, -7752(%rbp)
	testl	%eax, %eax
	movq	-7624(%rbp), %rax
	leaq	128(%rax), %rax
	movq	%rax, -7704(%rbp)
	leaq	-7516(%rbp), %rax
	movq	%rax, -7744(%rbp)
	je	.L640
.L1138:
	xorl	%r13d, %r13d
	jmp	.L1153
	.p2align 4,,10
	.p2align 3
.L1145:
	addq	$1, %r13
	cmpq	$31, %r13
	je	.L1935
.L1153:
	movq	-7720(%rbp), %rax
	movl	%r13d, %ebx
	movl	(%rax,%r13,4), %eax
	testl	%eax, %eax
	je	.L1145
	leaq	names(%rip), %rax
	movq	(%rax,%r13,8), %rdx
	movl	mr(%rip), %eax
	testl	%eax, %eax
	je	.L1146
	movq	%rdx, %rcx
	leaq	.LC140(%rip), %rsi
	movl	%r13d, %edx
	xorl	%eax, %eax
	movl	$1, %edi
	call	__printf_chk@PLT
.L1147:
	movsd	.LC110(%rip), %xmm4
	leaq	(%rbx,%rbx,2), %rbx
	xorl	%eax, %eax
	leaq	results(%rip), %r12
	movl	$0, testnum(%rip)
	addq	%rbx, %rbx
	movsd	%xmm4, -7648(%rbp)
	jmp	.L1152
.L1938:
	leaq	.LC37(%rip), %rsi
	testl	%eax, %eax
	je	.L1936
.L1150:
	movl	$1, %edi
	movl	$1, %eax
	call	__printf_chk@PLT
.L1151:
	movl	testnum(%rip), %eax
	addl	$1, %eax
	movl	%eax, testnum(%rip)
	cmpl	-7640(%rbp), %eax
	jnb	.L1937
.L1152:
	addq	%rbx, %rax
	movsd	(%r12,%rax,8), %xmm0
	comisd	-7648(%rbp), %xmm0
	movl	mr(%rip), %eax
	ja	.L1938
	testl	%eax, %eax
	leaq	.LC36(%rip), %rsi
	leaq	.LC37(%rip), %rax
	cmovne	%rax, %rsi
	jmp	.L1150
.L1936:
	leaq	.LC112(%rip), %rsi
	movl	$1, %edi
	movl	$1, %eax
	divsd	.LC111(%rip), %xmm0
	call	__printf_chk@PLT
	jmp	.L1151
.L1937:
	movl	$10, %edi
	call	putchar@PLT
	jmp	.L1145
.L1146:
	leaq	.LC141(%rip), %rsi
	movl	$1, %edi
	xorl	%eax, %eax
	call	__printf_chk@PLT
	jmp	.L1147
.L1932:
	movq	bio_err(%rip), %rdi
	leaq	.LC67(%rip), %rsi
	xorl	%eax, %eax
	movl	%r12d, %r15d
	call	BIO_printf@PLT
	jmp	.L597
.L1894:
	movq	bio_err(%rip), %rdi
	leaq	.LC72(%rip), %rsi
	movq	%rax, -7696(%rbp)
	movl	$1, %r15d
	call	BIO_printf@PLT
	movq	-7624(%rbp), %rax
	subq	$-128, %rax
	movq	%rax, -7704(%rbp)
	jmp	.L636
.L1895:
	leaq	rsa_results(%rip), %rax
	movl	%r15d, -7744(%rbp)
	leaq	-1088(%rbp), %r14
	movq	%rax, -7680(%rbp)
	leaq	dsa_results(%rip), %rax
	movq	%rax, -7656(%rbp)
	leaq	ecdsa_results(%rip), %rax
	movq	$0, -7664(%rbp)
	movq	%rax, -7648(%rbp)
	movq	-7664(%rbp), %rax
.L790:
	movq	-7696(%rbp), %rcx
	leaq	.LC80(%rip), %rsi
	movl	%eax, %r13d
	movl	(%rcx,%rax,4), %edi
	call	fdopen@PLT
	movq	%rax, %r12
.L647:
	movq	%r12, %rdx
	movl	$1024, %esi
	movq	%r14, %rdi
	call	fgets@PLT
	testq	%rax, %rax
	je	.L1939
	movl	$10, %esi
	movq	%r14, %rdi
	call	strchr@PLT
	testq	%rax, %rax
	je	.L648
	movb	$0, (%rax)
.L648:
	cmpb	$43, -1088(%rbp)
	movl	%r13d, %ecx
	movq	%r14, %rdx
	je	.L649
	movq	bio_err(%rip), %rdi
	leaq	.LC81(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L647
.L649:
	xorl	%eax, %eax
	leaq	.LC82(%rip), %rsi
	movl	$1, %edi
	call	__printf_chk@PLT
	cmpw	$17963, (%r14)
	je	.L1940
.L651:
	cmpl	$976373291, (%r14)
	je	.L1941
	cmpl	$976438827, (%r14)
	je	.L1942
	cmpl	$976504363, (%r14)
	je	.L1943
	cmpl	$976569899, (%r14)
	je	.L1944
	cmpl	$976635435, (%r14)
	je	.L1945
	cmpw	$18475, (%r14)
	je	.L1946
.L786:
	movq	bio_err(%rip), %rdi
	movl	%r13d, %ecx
	movq	%r14, %rdx
	xorl	%eax, %eax
	leaq	.LC90(%rip), %rsi
	call	BIO_printf@PLT
	jmp	.L647
.L1940:
	cmpb	$58, 2(%r14)
	jne	.L651
	movzbl	-1085(%rbp), %esi
	xorl	%r8d, %r8d
	testb	%sil, %sil
	je	.L1947
	leaq	-1344(%rbp), %rdx
	xorl	%eax, %eax
	movl	$32, %ecx
	movq	%rdx, %rdi
	leaq	sep.27834(%rip), %rdx
	rep stosq
	movzbl	sep.27834(%rip), %eax
	movb	$1, -1344(%rbp)
	testb	%al, %al
	je	.L658
.L655:
	addq	$1, %rdx
	movb	$1, -1344(%rbp,%rax)
	movzbl	(%rdx), %eax
	testb	%al, %al
	jne	.L655
.L658:
	leaq	-1085(%rbp), %rdx
	cmpb	$0, -1344(%rbp,%rsi)
	movq	%rdx, %r15
	jne	.L657
.L656:
	movzbl	1(%r15), %ecx
	addq	$1, %r15
	cmpb	$0, -1344(%rbp,%rcx)
	je	.L656
	movq	%rdx, %rdi
	testb	%cl, %cl
	jne	.L657
.L654:
	xorl	%esi, %esi
	movl	$10, %edx
	movl	%r8d, -7752(%rbp)
	call	strtol@PLT
	movzbl	(%r15), %esi
	movl	-7752(%rbp), %r8d
	movq	%rax, %r9
	testb	%sil, %sil
	je	.L659
	leaq	-1344(%rbp), %rdx
	xorl	%eax, %eax
	movl	$32, %ecx
	movq	%rdx, %rdi
	leaq	sep.27834(%rip), %rdx
	rep stosq
	movzbl	sep.27834(%rip), %eax
	movb	$1, -1344(%rbp)
	testb	%al, %al
	je	.L663
.L660:
	addq	$1, %rdx
	movb	$1, -1344(%rbp,%rax)
	movzbl	(%rdx), %eax
	testb	%al, %al
	jne	.L660
.L663:
	cmpb	$0, -1344(%rbp,%rsi)
	jne	.L662
.L661:
	movzbl	1(%r15), %eax
	addq	$1, %r15
	cmpb	$0, -1344(%rbp,%rax)
	movq	%rax, %rsi
	je	.L661
	testb	%al, %al
	jne	.L662
.L659:
	movslq	%r9d, %rax
	leaq	results(%rip), %rdx
	movq	%r12, -7752(%rbp)
	movl	%r8d, %r12d
	leaq	(%rax,%rax,2), %rax
	salq	$4, %rax
	leaq	(%rdx,%rax), %rbx
.L670:
	xorl	%edi, %edi
	testb	%sil, %sil
	je	.L664
	leaq	-1344(%rbp), %rdx
	xorl	%eax, %eax
	movl	$32, %ecx
	movq	%rdx, %rdi
	leaq	sep.27834(%rip), %rdx
	rep stosq
	movzbl	sep.27834(%rip), %eax
	movb	$1, -1344(%rbp)
	testb	%al, %al
	je	.L668
	.p2align 4,,10
	.p2align 3
.L665:
	addq	$1, %rdx
	movb	$1, -1344(%rbp,%rax)
	movzbl	(%rdx), %eax
	testb	%al, %al
	jne	.L665
.L668:
	cmpb	$0, -1344(%rbp,%rsi)
	movq	%r15, %rax
	jne	.L667
	.p2align 4,,10
	.p2align 3
.L666:
	movzbl	1(%rax), %ecx
	addq	$1, %rax
	cmpb	$0, -1344(%rbp,%rcx)
	je	.L666
	testb	%cl, %cl
	jne	.L667
	movq	%r15, %rdi
	movq	%rax, %r15
.L664:
	xorl	%esi, %esi
	addl	$1, %r12d
	addq	$8, %rbx
	call	strtod@PLT
	addsd	-8(%rbx), %xmm0
	movsd	%xmm0, -8(%rbx)
	cmpl	%r12d, -7640(%rbp)
	jle	.L1823
	movzbl	(%r15), %esi
	jmp	.L670
.L667:
	movq	%r15, %rdi
	movb	$0, (%rax)
	leaq	1(%rax), %r15
	jmp	.L664
.L1941:
	movzbl	-1084(%rbp), %esi
	testb	%sil, %sil
	je	.L1948
	leaq	-1344(%rbp), %rdx
	xorl	%eax, %eax
	movl	$32, %ecx
	movq	%rdx, %rdi
	leaq	sep.27834(%rip), %rdx
	rep stosq
	movzbl	sep.27834(%rip), %eax
	movb	$1, -1344(%rbp)
	testb	%al, %al
	je	.L678
.L675:
	addq	$1, %rdx
	movb	$1, -1344(%rbp,%rax)
	movzbl	(%rdx), %eax
	testb	%al, %al
	jne	.L675
.L678:
	leaq	-1084(%rbp), %rax
	cmpb	$0, -1344(%rbp,%rsi)
	movq	%rax, %r15
	jne	.L677
.L676:
	movzbl	1(%r15), %ecx
	addq	$1, %r15
	cmpb	$0, -1344(%rbp,%rcx)
	je	.L676
	movq	%rax, %rdi
	testb	%cl, %cl
	jne	.L677
.L674:
	xorl	%esi, %esi
	movl	$10, %edx
	call	strtol@PLT
	movzbl	(%r15), %esi
	movq	%rax, %rbx
	testb	%sil, %sil
	je	.L1869
	leaq	-1344(%rbp), %rdx
	xorl	%eax, %eax
	movl	$32, %ecx
	movq	%rdx, %rdi
	rep stosq
	movzbl	sep.27834(%rip), %eax
	movb	$1, -1344(%rbp)
	leaq	sep.27834(%rip), %rcx
	testb	%al, %al
	je	.L683
.L680:
	addq	$1, %rcx
	movb	$1, -1344(%rbp,%rax)
	movzbl	(%rcx), %eax
	testb	%al, %al
	jne	.L680
.L683:
	cmpb	$0, -1344(%rbp,%rsi)
	jne	.L682
.L681:
	movzbl	1(%r15), %ecx
	addq	$1, %r15
	cmpb	$0, -1344(%rbp,%rcx)
	je	.L681
	testb	%cl, %cl
	jne	.L682
.L1869:
	xorl	%r8d, %r8d
.L684:
	xorl	%esi, %esi
	movq	%r8, %rdi
	movslq	%ebx, %rbx
	call	strtod@PLT
	movq	%rbx, %rax
	movzbl	(%r15), %esi
	salq	$4, %rax
	addq	-7680(%rbp), %rax
	addsd	(%rax), %xmm0
	movsd	%xmm0, (%rax)
	testb	%sil, %sil
	je	.L1241
	leaq	-1344(%rbp), %rdx
	xorl	%eax, %eax
	movl	$32, %ecx
	movq	%rdx, %rdi
	leaq	sep.27834(%rip), %rdx
	rep stosq
	movzbl	sep.27834(%rip), %eax
	movb	$1, -1344(%rbp)
	testb	%al, %al
	je	.L693
.L690:
	addq	$1, %rdx
	movb	$1, -1344(%rbp,%rax)
	movzbl	(%rdx), %eax
	testb	%al, %al
	jne	.L690
.L693:
	cmpb	$0, -1344(%rbp,%rsi)
	movq	%r15, %rax
	jne	.L692
.L691:
	movzbl	1(%rax), %ecx
	addq	$1, %rax
	cmpb	$0, -1344(%rbp,%rcx)
	je	.L691
	testb	%cl, %cl
	jne	.L692
.L689:
	xorl	%esi, %esi
	movq	%r15, %rdi
	salq	$4, %rbx
	call	strtod@PLT
	movq	-7680(%rbp), %rax
	addq	%rbx, %rax
	addsd	8(%rax), %xmm0
	movsd	%xmm0, 8(%rax)
	jmp	.L647
.L1939:
	movq	%r12, %rdi
	call	fclose@PLT
	movq	-7664(%rbp), %rcx
	leaq	1(%rcx), %rax
	cmpq	%rcx, -7704(%rbp)
	je	.L1949
	movq	%rax, -7664(%rbp)
	jmp	.L790
.L1942:
	movzbl	-1084(%rbp), %esi
	testb	%sil, %sil
	je	.L1950
	leaq	-1344(%rbp), %rdx
	xorl	%eax, %eax
	movl	$32, %ecx
	movq	%rdx, %rdi
	leaq	sep.27834(%rip), %rdx
	rep stosq
	movzbl	sep.27834(%rip), %eax
	movb	$1, -1344(%rbp)
	testb	%al, %al
	je	.L701
.L698:
	addq	$1, %rdx
	movb	$1, -1344(%rbp,%rax)
	movzbl	(%rdx), %eax
	testb	%al, %al
	jne	.L698
.L701:
	leaq	-1084(%rbp), %rax
	cmpb	$0, -1344(%rbp,%rsi)
	movq	%rax, %r15
	jne	.L700
.L699:
	movzbl	1(%r15), %ecx
	addq	$1, %r15
	cmpb	$0, -1344(%rbp,%rcx)
	je	.L699
	movq	%rax, %rdi
	testb	%cl, %cl
	jne	.L700
.L697:
	xorl	%esi, %esi
	movl	$10, %edx
	call	strtol@PLT
	movzbl	(%r15), %esi
	movq	%rax, %rbx
	testb	%sil, %sil
	je	.L1870
	leaq	-1344(%rbp), %rdx
	xorl	%eax, %eax
	movl	$32, %ecx
	movq	%rdx, %rdi
	rep stosq
	movzbl	sep.27834(%rip), %eax
	movb	$1, -1344(%rbp)
	leaq	sep.27834(%rip), %rcx
	testb	%al, %al
	je	.L706
.L703:
	addq	$1, %rcx
	movb	$1, -1344(%rbp,%rax)
	movzbl	(%rcx), %eax
	testb	%al, %al
	jne	.L703
.L706:
	cmpb	$0, -1344(%rbp,%rsi)
	jne	.L705
.L704:
	movzbl	1(%r15), %ecx
	addq	$1, %r15
	cmpb	$0, -1344(%rbp,%rcx)
	je	.L704
	testb	%cl, %cl
	jne	.L705
.L1870:
	xorl	%r8d, %r8d
.L707:
	xorl	%esi, %esi
	movq	%r8, %rdi
	movslq	%ebx, %rbx
	call	strtod@PLT
	movq	%rbx, %rax
	movzbl	(%r15), %esi
	salq	$4, %rax
	addq	-7656(%rbp), %rax
	addsd	(%rax), %xmm0
	movsd	%xmm0, (%rax)
	testb	%sil, %sil
	je	.L1252
	leaq	-1344(%rbp), %rdx
	xorl	%eax, %eax
	movl	$32, %ecx
	movq	%rdx, %rdi
	leaq	sep.27834(%rip), %rdx
	rep stosq
	movzbl	sep.27834(%rip), %eax
	movb	$1, -1344(%rbp)
	testb	%al, %al
	je	.L716
.L713:
	addq	$1, %rdx
	movb	$1, -1344(%rbp,%rax)
	movzbl	(%rdx), %eax
	testb	%al, %al
	jne	.L713
.L716:
	cmpb	$0, -1344(%rbp,%rsi)
	movq	%r15, %rax
	jne	.L715
.L714:
	movzbl	1(%rax), %ecx
	addq	$1, %rax
	cmpb	$0, -1344(%rbp,%rcx)
	je	.L714
	testb	%cl, %cl
	jne	.L715
.L712:
	xorl	%esi, %esi
	movq	%r15, %rdi
	salq	$4, %rbx
	call	strtod@PLT
	movq	-7656(%rbp), %rax
	addq	%rbx, %rax
	addsd	8(%rax), %xmm0
	movsd	%xmm0, 8(%rax)
	jmp	.L647
.L682:
	movzbl	1(%r15), %esi
	movb	$0, (%r15)
	leaq	1(%r15), %r8
	testb	%sil, %sil
	je	.L1238
	xorl	%eax, %eax
	movq	%rdx, %rdi
	movl	$32, %ecx
	rep stosq
	movzbl	sep.27834(%rip), %eax
	movb	$1, -1344(%rbp)
	leaq	sep.27834(%rip), %rdx
	testb	%al, %al
	je	.L688
.L685:
	addq	$1, %rdx
	movb	$1, -1344(%rbp,%rax)
	movzbl	(%rdx), %eax
	testb	%al, %al
	jne	.L685
.L688:
	cmpb	$0, -1344(%rbp,%rsi)
	movq	%r8, %r15
	jne	.L687
.L686:
	movzbl	1(%r15), %edx
	addq	$1, %r15
	cmpb	$0, -1344(%rbp,%rdx)
	je	.L686
	testb	%dl, %dl
	je	.L684
.L687:
	movb	$0, (%r15)
	addq	$1, %r15
	jmp	.L684
.L705:
	movzbl	1(%r15), %esi
	movb	$0, (%r15)
	leaq	1(%r15), %r8
	testb	%sil, %sil
	je	.L1249
	xorl	%eax, %eax
	movq	%rdx, %rdi
	movl	$32, %ecx
	rep stosq
	movzbl	sep.27834(%rip), %eax
	movb	$1, -1344(%rbp)
	leaq	sep.27834(%rip), %rdx
	testb	%al, %al
	je	.L711
.L708:
	addq	$1, %rdx
	movb	$1, -1344(%rbp,%rax)
	movzbl	(%rdx), %eax
	testb	%al, %al
	jne	.L708
.L711:
	cmpb	$0, -1344(%rbp,%rsi)
	movq	%r8, %r15
	jne	.L710
.L709:
	movzbl	1(%r15), %edx
	addq	$1, %r15
	cmpb	$0, -1344(%rbp,%rdx)
	je	.L709
	testb	%dl, %dl
	je	.L707
.L710:
	movb	$0, (%r15)
	addq	$1, %r15
	jmp	.L707
.L1823:
	movq	-7752(%rbp), %r12
	jmp	.L647
.L1943:
	movzbl	-1084(%rbp), %edx
	testb	%dl, %dl
	je	.L1951
	leaq	-1344(%rbp), %rsi
	xorl	%eax, %eax
	movl	$32, %ecx
	movq	%rsi, %rdi
	rep stosq
	movzbl	sep.27834(%rip), %eax
	movb	$1, -1344(%rbp)
	leaq	sep.27834(%rip), %rcx
	testb	%al, %al
	je	.L724
.L721:
	addq	$1, %rcx
	movb	$1, -1344(%rbp,%rax)
	movzbl	(%rcx), %eax
	testb	%al, %al
	jne	.L721
.L724:
	leaq	-1084(%rbp), %rax
	cmpb	$0, -1344(%rbp,%rdx)
	movq	%rax, %r15
	jne	.L723
.L722:
	movzbl	1(%r15), %ecx
	addq	$1, %r15
	cmpb	$0, -1344(%rbp,%rcx)
	je	.L722
	movq	%rax, %rdi
	testb	%cl, %cl
	jne	.L723
.L720:
	xorl	%esi, %esi
	movl	$10, %edx
	call	strtol@PLT
	movzbl	(%r15), %esi
	movq	%rax, %rbx
	testb	%sil, %sil
	je	.L1871
	leaq	-1344(%rbp), %rdx
	xorl	%eax, %eax
	movl	$32, %ecx
	movq	%rdx, %rdi
	rep stosq
	movzbl	sep.27834(%rip), %eax
	movb	$1, -1344(%rbp)
	leaq	sep.27834(%rip), %rcx
	testb	%al, %al
	je	.L729
.L726:
	addq	$1, %rcx
	movb	$1, -1344(%rbp,%rax)
	movzbl	(%rcx), %eax
	testb	%al, %al
	jne	.L726
.L729:
	cmpb	$0, -1344(%rbp,%rsi)
	jne	.L728
.L727:
	movzbl	1(%r15), %ecx
	addq	$1, %r15
	cmpb	$0, -1344(%rbp,%rcx)
	je	.L727
	testb	%cl, %cl
	jne	.L728
.L1871:
	xorl	%r8d, %r8d
.L730:
	xorl	%esi, %esi
	movq	%r8, %rdi
	movslq	%ebx, %rbx
	call	strtod@PLT
	movq	%rbx, %rax
	movzbl	(%r15), %edx
	salq	$4, %rax
	addq	-7648(%rbp), %rax
	addsd	(%rax), %xmm0
	movsd	%xmm0, (%rax)
	testb	%dl, %dl
	je	.L1263
	leaq	-1344(%rbp), %rsi
	xorl	%eax, %eax
	movl	$32, %ecx
	movq	%rsi, %rdi
	rep stosq
	movzbl	sep.27834(%rip), %eax
	movb	$1, -1344(%rbp)
	leaq	sep.27834(%rip), %rcx
	testb	%al, %al
	je	.L739
.L736:
	addq	$1, %rcx
	movb	$1, -1344(%rbp,%rax)
	movzbl	(%rcx), %eax
	testb	%al, %al
	jne	.L736
.L739:
	cmpb	$0, -1344(%rbp,%rdx)
	movq	%r15, %rax
	jne	.L738
.L737:
	movzbl	1(%rax), %ecx
	addq	$1, %rax
	cmpb	$0, -1344(%rbp,%rcx)
	je	.L737
	testb	%cl, %cl
	jne	.L738
.L735:
	xorl	%esi, %esi
	movq	%r15, %rdi
	salq	$4, %rbx
	call	strtod@PLT
	movq	-7648(%rbp), %rax
	addq	%rbx, %rax
	addsd	8(%rax), %xmm0
	movsd	%xmm0, 8(%rax)
	jmp	.L647
.L728:
	movzbl	1(%r15), %esi
	movb	$0, (%r15)
	leaq	1(%r15), %r8
	testb	%sil, %sil
	je	.L1260
	xorl	%eax, %eax
	movq	%rdx, %rdi
	movl	$32, %ecx
	rep stosq
	movzbl	sep.27834(%rip), %eax
	movb	$1, -1344(%rbp)
	leaq	sep.27834(%rip), %rdx
	testb	%al, %al
	je	.L734
.L731:
	addq	$1, %rdx
	movb	$1, -1344(%rbp,%rax)
	movzbl	(%rdx), %eax
	testb	%al, %al
	jne	.L731
.L734:
	cmpb	$0, -1344(%rbp,%rsi)
	movq	%r8, %r15
	jne	.L733
.L732:
	movzbl	1(%r15), %edx
	addq	$1, %r15
	cmpb	$0, -1344(%rbp,%rdx)
	je	.L732
	testb	%dl, %dl
	je	.L730
.L733:
	movb	$0, (%r15)
	addq	$1, %r15
	jmp	.L730
.L1944:
	movzbl	-1084(%rbp), %edx
	testb	%dl, %dl
	je	.L1952
	leaq	-1344(%rbp), %rsi
	xorl	%eax, %eax
	movl	$32, %ecx
	movq	%rsi, %rdi
	rep stosq
	movzbl	sep.27834(%rip), %eax
	movb	$1, -1344(%rbp)
	leaq	sep.27834(%rip), %rcx
	testb	%al, %al
	je	.L747
.L744:
	addq	$1, %rcx
	movb	$1, -1344(%rbp,%rax)
	movzbl	(%rcx), %eax
	testb	%al, %al
	jne	.L744
.L747:
	leaq	-1084(%rbp), %rax
	cmpb	$0, -1344(%rbp,%rdx)
	movq	%rax, %r15
	jne	.L746
.L745:
	movzbl	1(%r15), %ecx
	addq	$1, %r15
	cmpb	$0, -1344(%rbp,%rcx)
	je	.L745
	movq	%rax, %rdi
	testb	%cl, %cl
	jne	.L746
.L743:
	xorl	%esi, %esi
	movl	$10, %edx
	call	strtol@PLT
	movzbl	(%r15), %esi
	movq	%rax, %rbx
	testb	%sil, %sil
	je	.L1271
	leaq	-1344(%rbp), %rdx
	xorl	%eax, %eax
	movl	$32, %ecx
	movq	%rdx, %rdi
	rep stosq
	movzbl	sep.27834(%rip), %eax
	movb	$1, -1344(%rbp)
	leaq	sep.27834(%rip), %rcx
	testb	%al, %al
	je	.L752
.L749:
	addq	$1, %rcx
	movb	$1, -1344(%rbp,%rax)
	movzbl	(%rcx), %eax
	testb	%al, %al
	jne	.L749
.L752:
	cmpb	$0, -1344(%rbp,%rsi)
	jne	.L751
.L750:
	movzbl	1(%r15), %ecx
	addq	$1, %r15
	cmpb	$0, -1344(%rbp,%rcx)
	je	.L750
	testb	%cl, %cl
	jne	.L751
.L1271:
	xorl	%r8d, %r8d
.L753:
	xorl	%esi, %esi
	movq	%r8, %rdi
	movslq	%ebx, %rbx
	call	strtod@PLT
	leaq	ecdh_results(%rip), %rax
	addsd	(%rax,%rbx,8), %xmm0
	movsd	%xmm0, (%rax,%rbx,8)
	jmp	.L647
.L677:
	movb	$0, (%r15)
	movq	%rax, %rdi
	addq	$1, %r15
	jmp	.L674
.L1948:
	leaq	-1084(%rbp), %r15
	xorl	%edi, %edi
	jmp	.L674
.L692:
	movb	$0, (%rax)
	jmp	.L689
.L1241:
	xorl	%r15d, %r15d
	jmp	.L689
.L715:
	movb	$0, (%rax)
	jmp	.L712
.L1252:
	xorl	%r15d, %r15d
	jmp	.L712
.L751:
	movzbl	1(%r15), %esi
	movb	$0, (%r15)
	leaq	1(%r15), %r8
	testb	%sil, %sil
	je	.L1271
	xorl	%eax, %eax
	movq	%rdx, %rdi
	movl	$32, %ecx
	rep stosq
	movzbl	sep.27834(%rip), %eax
	movb	$1, -1344(%rbp)
	leaq	sep.27834(%rip), %rdx
	testb	%al, %al
	je	.L757
.L754:
	addq	$1, %rdx
	movb	$1, -1344(%rbp,%rax)
	movzbl	(%rdx), %eax
	testb	%al, %al
	jne	.L754
.L757:
	cmpb	$0, -1344(%rbp,%rsi)
	movq	%r8, %rax
	jne	.L756
.L755:
	movzbl	1(%rax), %ecx
	addq	$1, %rax
	cmpb	$0, -1344(%rbp,%rcx)
	je	.L755
	testb	%cl, %cl
	je	.L753
.L756:
	movb	$0, (%rax)
	jmp	.L753
.L746:
	movb	$0, (%r15)
	movq	%rax, %rdi
	addq	$1, %r15
	jmp	.L743
.L1952:
	leaq	-1084(%rbp), %r15
	xorl	%edi, %edi
	jmp	.L743
.L738:
	movb	$0, (%rax)
	jmp	.L735
.L1263:
	xorl	%r15d, %r15d
	jmp	.L735
.L723:
	movb	$0, (%r15)
	movq	%rax, %rdi
	addq	$1, %r15
	jmp	.L720
.L1951:
	leaq	-1084(%rbp), %r15
	xorl	%edi, %edi
	jmp	.L720
.L662:
	movb	$0, (%r15)
	movzbl	1(%r15), %esi
	addq	$1, %r15
	jmp	.L659
.L700:
	movb	$0, (%r15)
	movq	%rax, %rdi
	addq	$1, %r15
	jmp	.L697
.L657:
	movb	$0, (%r15)
	movq	%rdx, %rdi
	addq	$1, %r15
	jmp	.L654
.L1947:
	leaq	-1085(%rbp), %r15
	xorl	%edi, %edi
	jmp	.L654
.L1946:
	cmpb	$58, 2(%r14)
	jne	.L786
	jmp	.L647
.L1945:
	movzbl	-1084(%rbp), %edx
	testb	%dl, %dl
	je	.L1953
	leaq	-1344(%rbp), %rsi
	xorl	%eax, %eax
	movl	$32, %ecx
	movq	%rsi, %rdi
	rep stosq
	movzbl	sep.27834(%rip), %eax
	movb	$1, -1344(%rbp)
	leaq	sep.27834(%rip), %rcx
	testb	%al, %al
	je	.L765
.L762:
	addq	$1, %rcx
	movb	$1, -1344(%rbp,%rax)
	movzbl	(%rcx), %eax
	testb	%al, %al
	jne	.L762
.L765:
	leaq	-1084(%rbp), %rax
	cmpb	$0, -1344(%rbp,%rdx)
	movq	%rax, %r8
	jne	.L764
.L763:
	movzbl	1(%r8), %ecx
	addq	$1, %r8
	cmpb	$0, -1344(%rbp,%rcx)
	je	.L763
	movq	%rax, %rdi
	testb	%cl, %cl
	jne	.L764
.L761:
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%r8, -7752(%rbp)
	call	strtol@PLT
	movq	-7752(%rbp), %r8
	movq	%rax, %rbx
	movzbl	(%r8), %esi
	testb	%sil, %sil
	je	.L1280
	leaq	-1344(%rbp), %rdx
	xorl	%eax, %eax
	movl	$32, %ecx
	movq	%rdx, %rdi
	rep stosq
	movzbl	sep.27834(%rip), %eax
	movb	$1, -1344(%rbp)
	leaq	sep.27834(%rip), %rcx
	testb	%al, %al
	je	.L770
.L767:
	addq	$1, %rcx
	movb	$1, -1344(%rbp,%rax)
	movzbl	(%rcx), %eax
	testb	%al, %al
	jne	.L767
.L770:
	cmpb	$0, -1344(%rbp,%rsi)
	jne	.L769
.L768:
	movzbl	1(%r8), %ecx
	addq	$1, %r8
	cmpb	$0, -1344(%rbp,%rcx)
	je	.L768
	testb	%cl, %cl
	jne	.L769
.L1280:
	movq	%r8, %r15
.L1872:
	xorl	%r8d, %r8d
.L776:
	xorl	%esi, %esi
	movq	%r8, %rdi
	movslq	%ebx, %rbx
	call	strtod@PLT
	movq	%rbx, %rax
	leaq	eddsa_results(%rip), %rcx
	movzbl	(%r15), %edx
	salq	$4, %rax
	addq	%rcx, %rax
	addsd	(%rax), %xmm0
	movsd	%xmm0, (%rax)
	testb	%dl, %dl
	je	.L1283
	leaq	-1344(%rbp), %rsi
	xorl	%eax, %eax
	movl	$32, %ecx
	movq	%rsi, %rdi
	rep stosq
	movzbl	sep.27834(%rip), %eax
	movb	$1, -1344(%rbp)
	leaq	sep.27834(%rip), %rcx
	testb	%al, %al
	je	.L785
.L782:
	addq	$1, %rcx
	movb	$1, -1344(%rbp,%rax)
	movzbl	(%rcx), %eax
	testb	%al, %al
	jne	.L782
.L785:
	cmpb	$0, -1344(%rbp,%rdx)
	movq	%r15, %rax
	jne	.L784
.L783:
	movzbl	1(%rax), %ecx
	addq	$1, %rax
	cmpb	$0, -1344(%rbp,%rcx)
	je	.L783
	testb	%cl, %cl
	jne	.L784
.L781:
	xorl	%esi, %esi
	movq	%r15, %rdi
	salq	$4, %rbx
	call	strtod@PLT
	leaq	eddsa_results(%rip), %rax
	addq	%rbx, %rax
	addsd	8(%rax), %xmm0
	movsd	%xmm0, 8(%rax)
	jmp	.L647
.L769:
	movzbl	1(%r8), %esi
	movb	$0, (%r8)
	leaq	1(%r8), %r15
	testb	%sil, %sil
	je	.L1872
	xorl	%eax, %eax
	movl	$32, %ecx
	movq	%rdx, %rdi
	rep stosq
	movzbl	sep.27834(%rip), %eax
	movb	$1, -1344(%rbp)
	leaq	sep.27834(%rip), %rcx
	testb	%al, %al
	je	.L775
.L772:
	addq	$1, %rcx
	movb	$1, -1344(%rbp,%rax)
	movzbl	(%rcx), %eax
	testb	%al, %al
	jne	.L772
.L775:
	cmpb	$0, -1344(%rbp,%rsi)
	jne	.L774
.L773:
	movzbl	1(%r15), %ecx
	addq	$1, %r15
	cmpb	$0, -1344(%rbp,%rcx)
	je	.L773
	testb	%cl, %cl
	je	.L1872
.L774:
	movzbl	1(%r15), %esi
	movb	$0, (%r15)
	leaq	1(%r15), %r8
	testb	%sil, %sil
	je	.L1280
	xorl	%eax, %eax
	movq	%rdx, %rdi
	movl	$32, %ecx
	rep stosq
	movzbl	sep.27834(%rip), %eax
	movb	$1, -1344(%rbp)
	leaq	sep.27834(%rip), %rdx
	testb	%al, %al
	je	.L780
.L777:
	addq	$1, %rdx
	movb	$1, -1344(%rbp,%rax)
	movzbl	(%rdx), %eax
	testb	%al, %al
	jne	.L777
.L780:
	cmpb	$0, -1344(%rbp,%rsi)
	movq	%r8, %r15
	jne	.L779
.L778:
	movzbl	1(%r15), %edx
	addq	$1, %r15
	cmpb	$0, -1344(%rbp,%rdx)
	je	.L778
	testb	%dl, %dl
	je	.L776
.L779:
	movb	$0, (%r15)
	addq	$1, %r15
	jmp	.L776
.L1950:
	leaq	-1084(%rbp), %r15
	xorl	%edi, %edi
	jmp	.L697
.L1209:
	movl	$1, %eax
.L612:
	movl	$1, -7504(%rbp,%rax,4)
	jmp	.L606
.L1208:
	xorl	%eax, %eax
	jmp	.L612
.L1213:
	movl	$5, %eax
	jmp	.L612
.L1212:
	movl	$4, %eax
	jmp	.L612
.L1211:
	movl	$3, %eax
	jmp	.L612
.L1210:
	movl	$2, %eax
	jmp	.L612
.L1891:
	movq	bio_err(%rip), %rdi
	leaq	.LC65(%rip), %rsi
	xorl	%eax, %eax
	movl	%r13d, %r15d
	call	BIO_printf@PLT
	jmp	.L597
.L1215:
	xorl	%eax, %eax
.L615:
	movl	$2, -7516(%rbp,%rax,4)
	jmp	.L606
.L1216:
	movl	$1, %eax
	jmp	.L615
.L1934:
	movq	bio_err(%rip), %rdi
	leaq	.LC69(%rip), %rsi
	xorl	%eax, %eax
	movl	$1, %r15d
	call	BIO_printf@PLT
	jmp	.L597
.L1893:
	movq	bio_err(%rip), %rdi
	leaq	.LC70(%rip), %rsi
	movl	$1, %r15d
	call	BIO_printf@PLT
	jmp	.L597
.L1888:
	movl	$6, %eax
	jmp	.L612
.L1928:
	movl	8(%r14), %eax
	movl	$2, -7472(%rbp,%rax,4)
	jmp	.L606
.L1930:
	movl	8(%r14), %eax
	movl	$2, -7376(%rbp,%rax,4)
	jmp	.L606
.L1929:
	movdqa	.LC54(%rip), %xmm3
	movaps	%xmm3, -7376(%rbp)
	movaps	%xmm3, -7360(%rbp)
	movaps	%xmm3, -7344(%rbp)
	movaps	%xmm3, -7328(%rbp)
	movaps	%xmm3, -7312(%rbp)
	movaps	%xmm3, -7296(%rbp)
	jmp	.L606
.L1931:
	leaq	16+eddsa_choices(%rip), %rax
.L624:
	movl	8(%rax), %eax
	movl	$2, -7524(%rbp,%rax,4)
	jmp	.L606
.L1220:
	leaq	eddsa_choices(%rip), %rax
	jmp	.L624
.L623:
	movabsq	$4294967297, %rax
	movq	%rax, -7524(%rbp)
	jmp	.L606
.L1927:
	movdqa	.LC54(%rip), %xmm6
	movabsq	$4294967297, %rax
	movq	%rax, -7392(%rbp)
	movaps	%xmm6, -7472(%rbp)
	movaps	%xmm6, -7456(%rbp)
	movaps	%xmm6, -7440(%rbp)
	movaps	%xmm6, -7424(%rbp)
	movaps	%xmm6, -7408(%rbp)
	jmp	.L606
.L1926:
	movabsq	$4294967297, %rax
	movl	$1, -7204(%rbp)
	movq	%rax, -7200(%rbp)
	jmp	.L606
.L1925:
	movabsq	$4294967297, %rax
	movl	$1, -7208(%rbp)
	movq	%rax, -7216(%rbp)
	jmp	.L606
.L1924:
	movl	$2, %eax
	jmp	.L615
.L1953:
	leaq	-1084(%rbp), %r8
	xorl	%edi, %edi
	jmp	.L761
.L1249:
	movq	%r8, %r15
	jmp	.L1870
.L1238:
	movq	%r8, %r15
	jmp	.L1869
.L1260:
	movq	%r8, %r15
	jmp	.L1871
	.p2align 4,,10
	.p2align 3
.L784:
	movb	$0, (%rax)
	jmp	.L781
.L1283:
	xorl	%r15d, %r15d
	jmp	.L781
.L1921:
	movq	-7624(%rbp), %rdx
	movq	bio_err(%rip), %rdi
	leaq	.LC40(%rip), %rsi
	call	BIO_printf@PLT
	jmp	.L595
.L1922:
	call	__stack_chk_fail@PLT
	.p2align 4,,10
	.p2align 3
.L764:
	movb	$0, (%r8)
	movq	%rax, %rdi
	addq	$1, %r8
	jmp	.L761
.L1933:
	movq	-7656(%rbp), %rdi
	movl	$1, %r15d
	call	EVP_CIPHER_nid@PLT
	movl	%eax, %edi
	call	OBJ_nid2ln@PLT
	movq	bio_err(%rip), %rdi
	leaq	.LC68(%rip), %rsi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L597
.L640:
	xorl	%edi, %edi
	call	OpenSSL_version@PLT
	movq	%rax, %rdi
	call	puts@PLT
	movl	$2, %edi
	call	OpenSSL_version@PLT
	movq	%rax, %rdi
	call	puts@PLT
	leaq	.LC135(%rip), %rsi
	movl	$1, %edi
	xorl	%eax, %eax
	call	__printf_chk@PLT
	call	BN_options@PLT
	leaq	.LC136(%rip), %rsi
	movl	$1, %edi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	__printf_chk@PLT
	call	RC4_options@PLT
	leaq	.LC136(%rip), %rsi
	movl	$1, %edi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	__printf_chk@PLT
	call	DES_options@PLT
	leaq	.LC136(%rip), %rsi
	movl	$1, %edi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	__printf_chk@PLT
	call	AES_options@PLT
	leaq	.LC136(%rip), %rsi
	movl	$1, %edi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	__printf_chk@PLT
	call	IDEA_options@PLT
	leaq	.LC136(%rip), %rsi
	movl	$1, %edi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	__printf_chk@PLT
	call	BF_options@PLT
	leaq	.LC136(%rip), %rsi
	movl	$1, %edi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	__printf_chk@PLT
	movl	$1, %edi
	call	OpenSSL_version@PLT
	leaq	.LC137(%rip), %rsi
	movl	$1, %edi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	__printf_chk@PLT
	movl	-7752(%rbp), %edx
	testl	%edx, %edx
	je	.L1138
	movl	mr(%rip), %eax
	testl	%eax, %eax
	jne	.L1198
	leaq	.LC138(%rip), %rdi
	call	puts@PLT
	leaq	.LC139(%rip), %rsi
	movl	$1, %edi
	xorl	%eax, %eax
	call	__printf_chk@PLT
.L1140:
	movl	$0, testnum(%rip)
	xorl	%eax, %eax
	leaq	.LC108(%rip), %rbx
.L1144:
	movq	lengths(%rip), %rdx
	leaq	.LC104(%rip), %rsi
	movl	(%rdx,%rax,4), %edx
	movl	mr(%rip), %eax
	testl	%eax, %eax
	jne	.L1877
	movq	%rbx, %rsi
.L1877:
	movl	$1, %edi
	xorl	%eax, %eax
	call	__printf_chk@PLT
	movl	testnum(%rip), %eax
	addl	$1, %eax
	movl	%eax, testnum(%rip)
	cmpl	-7640(%rbp), %eax
	jb	.L1144
	movl	$10, %edi
	call	putchar@PLT
	jmp	.L1138
.L1970:
.L1118:
	endbr64
	cmpl	$0, mr(%rip)
	je	.L640
	cmpl	$0, -7752(%rbp)
	je	.L1138
.L1198:
	leaq	.LC103(%rip), %rsi
	movl	$1, %edi
	xorl	%eax, %eax
	call	__printf_chk@PLT
	jmp	.L1140
.L1935:
	leaq	-7504(%rbp), %rax
	leaq	rsa_results(%rip), %rbx
	xorl	%r13d, %r13d
	movl	$1, testnum(%rip)
	movq	%rax, -7640(%rbp)
	leaq	rsa_bits.27419(%rip), %r12
	jmp	.L1159
.L1955:
	testl	%eax, %eax
	je	.L1156
	movsd	8(%rbx), %xmm1
	movsd	(%rbx), %xmm2
.L1157:
	movapd	%xmm2, %xmm0
	movl	%r8d, %edx
	movl	$1, %edi
	movl	$2, %eax
	leaq	.LC144(%rip), %rsi
	call	__printf_chk@PLT
.L1154:
	addq	$1, %r13
	addq	$16, %rbx
	cmpq	$7, %r13
	je	.L1954
.L1159:
	movq	-7640(%rbp), %rax
	movl	%r13d, %r8d
	movl	(%rax,%r13,4), %eax
	testl	%eax, %eax
	je	.L1154
	movl	testnum(%rip), %r11d
	movl	mr(%rip), %eax
	movl	(%r12,%r13,4), %ecx
	testl	%r11d, %r11d
	jne	.L1955
.L1155:
	movsd	8(%rbx), %xmm1
	movsd	(%rbx), %xmm2
	testl	%eax, %eax
	jne	.L1157
	movapd	%xmm1, %xmm3
	movl	%ecx, %edx
	movl	$1, %edi
	movl	$4, %eax
	movsd	.LC145(%rip), %xmm0
	leaq	.LC146(%rip), %rsi
	movapd	%xmm0, %xmm4
	divsd	%xmm2, %xmm4
	divsd	%xmm1, %xmm0
	movapd	%xmm0, %xmm1
	movapd	%xmm4, %xmm0
	call	__printf_chk@PLT
	jmp	.L1154
.L1156:
	leaq	.LC142(%rip), %rdx
	leaq	.LC143(%rip), %rsi
	movl	$1, %edi
	xorl	%eax, %eax
	movl	%ecx, -7656(%rbp)
	movl	%r13d, -7648(%rbp)
	call	__printf_chk@PLT
	movl	mr(%rip), %eax
	movl	$0, testnum(%rip)
	movl	-7656(%rbp), %ecx
	movl	-7648(%rbp), %r8d
	jmp	.L1155
.L1954:
	movl	$1, testnum(%rip)
	leaq	dsa_results(%rip), %r13
	xorl	%r12d, %r12d
	leaq	dsa_bits.27424(%rip), %rbx
.L1165:
	movq	-7744(%rbp), %rax
	movl	%r12d, %r8d
	movl	(%rax,%r12,4), %r10d
	testl	%r10d, %r10d
	je	.L1160
	movl	testnum(%rip), %r9d
	movl	mr(%rip), %eax
	movl	(%rbx,%r12,4), %ecx
	testl	%r9d, %r9d
	je	.L1161
	testl	%eax, %eax
	je	.L1162
	movsd	8(%r13), %xmm1
	movsd	0(%r13), %xmm2
.L1163:
	movapd	%xmm2, %xmm0
	movl	%r8d, %edx
	movl	$1, %edi
	movl	$2, %eax
	leaq	.LC147(%rip), %rsi
	call	__printf_chk@PLT
.L1160:
	addq	$1, %r12
	addq	$16, %r13
	cmpq	$3, %r12
	jne	.L1165
	leaq	test_curves.27430(%rip), %rbx
	xorl	%r12d, %r12d
	movl	$1, testnum(%rip)
	leaq	ecdsa_results(%rip), %r13
	movq	%rbx, %rax
	movq	%rbx, -7640(%rbp)
	movq	%r12, %rbx
	movq	%rax, %r12
	jmp	.L1171
.L1957:
	testl	%eax, %eax
	je	.L1168
	movsd	8(%r13), %xmm1
	movsd	0(%r13), %xmm2
.L1169:
	movapd	%xmm2, %xmm0
	movl	%r9d, %ecx
	movl	%r10d, %edx
	movl	$1, %edi
	leaq	.LC150(%rip), %rsi
	movl	$2, %eax
	call	__printf_chk@PLT
.L1166:
	addq	$1, %rbx
	addq	$16, -7640(%rbp)
	addq	$16, %r13
	cmpq	$22, %rbx
	je	.L1956
.L1171:
	movq	-7728(%rbp), %rax
	movl	%ebx, %r10d
	movl	(%rax,%rbx,4), %edi
	testl	%edi, %edi
	je	.L1166
	movq	-7640(%rbp), %rcx
	movl	testnum(%rip), %esi
	movl	mr(%rip), %eax
	movl	12(%rcx), %r9d
	testl	%esi, %esi
	jne	.L1957
.L1167:
	movsd	8(%r13), %xmm1
	movsd	0(%r13), %xmm2
	testl	%eax, %eax
	jne	.L1169
	movsd	.LC145(%rip), %xmm5
	movapd	%xmm1, %xmm3
	movl	%r9d, %edx
	movl	$1, %edi
	movsd	.LC145(%rip), %xmm0
	movq	-7640(%rbp), %rax
	leaq	.LC151(%rip), %rsi
	divsd	%xmm1, %xmm5
	movq	(%rax), %rcx
	movl	$4, %eax
	divsd	%xmm2, %xmm0
	movapd	%xmm5, %xmm1
	call	__printf_chk@PLT
	jmp	.L1166
.L1162:
	leaq	.LC142(%rip), %rdx
	leaq	.LC143(%rip), %rsi
	movl	$1, %edi
	xorl	%eax, %eax
	movl	%ecx, -7648(%rbp)
	movl	%r12d, -7640(%rbp)
	call	__printf_chk@PLT
	movl	mr(%rip), %eax
	movl	$0, testnum(%rip)
	movl	-7648(%rbp), %ecx
	movl	-7640(%rbp), %r8d
.L1161:
	movsd	8(%r13), %xmm1
	movsd	0(%r13), %xmm2
	testl	%eax, %eax
	jne	.L1163
	movapd	%xmm1, %xmm3
	movl	%ecx, %edx
	movl	$1, %edi
	movl	$4, %eax
	movsd	.LC145(%rip), %xmm0
	leaq	.LC148(%rip), %rsi
	movapd	%xmm0, %xmm4
	divsd	%xmm2, %xmm4
	divsd	%xmm1, %xmm0
	movapd	%xmm0, %xmm1
	movapd	%xmm4, %xmm0
	call	__printf_chk@PLT
	jmp	.L1160
.L1168:
	leaq	.LC142(%rip), %rdx
	leaq	.LC149(%rip), %rsi
	movl	$1, %edi
	xorl	%eax, %eax
	movl	%r9d, -7656(%rbp)
	movl	%ebx, -7648(%rbp)
	call	__printf_chk@PLT
	movl	mr(%rip), %eax
	movl	$0, testnum(%rip)
	movl	-7656(%rbp), %r9d
	movl	-7648(%rbp), %r10d
	jmp	.L1167
.L1956:
	movl	$1, testnum(%rip)
	movq	%r12, %rbx
	xorl	%r13d, %r13d
	leaq	ecdh_results(%rip), %r12
	jmp	.L1177
.L1959:
	testl	%eax, %eax
	je	.L1174
	movsd	(%r12,%r13,8), %xmm2
	movsd	.LC145(%rip), %xmm0
	divsd	%xmm2, %xmm0
.L1175:
	movapd	%xmm0, %xmm1
	movl	%r8d, %ecx
	movapd	%xmm2, %xmm0
	movl	%r9d, %edx
	leaq	.LC153(%rip), %rsi
	movl	$1, %edi
	movl	$2, %eax
	call	__printf_chk@PLT
.L1172:
	addq	$1, %r13
	addq	$16, %rbx
	cmpq	$24, %r13
	je	.L1958
.L1177:
	movq	-7736(%rbp), %rax
	movl	%r13d, %r9d
	movl	(%rax,%r13,4), %ecx
	testl	%ecx, %ecx
	je	.L1172
	movl	testnum(%rip), %edx
	movl	mr(%rip), %eax
	movl	12(%rbx), %r8d
	testl	%edx, %edx
	jne	.L1959
.L1173:
	movsd	(%r12,%r13,8), %xmm2
	movsd	.LC145(%rip), %xmm0
	divsd	%xmm2, %xmm0
	testl	%eax, %eax
	jne	.L1175
	movq	(%rbx), %rcx
	movapd	%xmm2, %xmm1
	movl	%r8d, %edx
	movl	$1, %edi
	leaq	.LC154(%rip), %rsi
	movl	$2, %eax
	call	__printf_chk@PLT
	jmp	.L1172
.L1174:
	leaq	.LC142(%rip), %rdx
	leaq	.LC152(%rip), %rsi
	movl	$1, %edi
	xorl	%eax, %eax
	movl	%r8d, -7648(%rbp)
	movl	%r13d, -7640(%rbp)
	call	__printf_chk@PLT
	movl	mr(%rip), %eax
	movl	$0, testnum(%rip)
	movl	-7648(%rbp), %r8d
	movl	-7640(%rbp), %r9d
	jmp	.L1173
.L1958:
	xorl	%r13d, %r13d
	leaq	test_ed_curves.27436(%rip), %rbx
	movl	$1, testnum(%rip)
	leaq	8+eddsa_results(%rip), %r12
	movl	(%r14,%r13,4), %eax
	movl	%r13d, %r9d
	testl	%eax, %eax
	je	.L1178
.L1960:
	movl	testnum(%rip), %r11d
	movl	mr(%rip), %eax
	movq	(%rbx), %r8
	movl	12(%rbx), %r10d
	testl	%r11d, %r11d
	je	.L1179
	testl	%eax, %eax
	je	.L1180
	movq	%r13, %rax
	leaq	eddsa_results(%rip), %rdx
	salq	$4, %rax
	movsd	(%r12,%rax), %xmm1
	movsd	(%rdx,%rax), %xmm2
.L1181:
	movapd	%xmm2, %xmm0
	movl	%r10d, %ecx
	movl	%r9d, %edx
	movl	$1, %edi
	leaq	.LC155(%rip), %rsi
	movl	$2, %eax
	call	__printf_chk@PLT
.L1178:
	addq	$24, %rbx
	cmpq	$1, %r13
	je	.L636
	movl	$1, %r13d
	movl	(%r14,%r13,4), %eax
	movl	%r13d, %r9d
	testl	%eax, %eax
	jne	.L1960
	jmp	.L1178
.L1180:
	leaq	.LC142(%rip), %rdx
	leaq	.LC149(%rip), %rsi
	movl	$1, %edi
	xorl	%eax, %eax
	movl	%r10d, -7656(%rbp)
	movq	%r8, -7648(%rbp)
	movl	%r13d, -7640(%rbp)
	call	__printf_chk@PLT
	movl	mr(%rip), %eax
	movl	$0, testnum(%rip)
	movl	-7656(%rbp), %r10d
	movq	-7648(%rbp), %r8
	movl	-7640(%rbp), %r9d
.L1179:
	movq	%r13, %rdx
	leaq	eddsa_results(%rip), %rcx
	salq	$4, %rdx
	movsd	(%r12,%rdx), %xmm1
	movsd	(%rcx,%rdx), %xmm2
	testl	%eax, %eax
	jne	.L1181
	movapd	%xmm1, %xmm3
	movq	%r8, %rcx
	movl	%r10d, %edx
	movl	$1, %edi
	movsd	.LC145(%rip), %xmm0
	leaq	.LC156(%rip), %rsi
	movl	$4, %eax
	movapd	%xmm0, %xmm4
	divsd	%xmm2, %xmm4
	divsd	%xmm1, %xmm0
	movapd	%xmm0, %xmm1
	movapd	%xmm4, %xmm0
	call	__printf_chk@PLT
	jmp	.L1178
.L1896:
	leaq	.LC77(%rip), %rsi
.L1879:
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movl	$1, %edi
	call	exit@PLT
.L1898:
	leaq	.LC79(%rip), %rsi
	jmp	.L1879
	.p2align 4,,10
	.p2align 3
.L1899:
	movq	bio_err(%rip), %rdi
	leaq	.LC91(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L800
.L1012:
	movq	evp_md(%rip), %rdi
	testq	%rdi, %rdi
	je	.L1007
	call	EVP_MD_type@PLT
	leaq	.LC14(%rip), %r12
	movl	%eax, %edi
	call	OBJ_nid2ln@PLT
	movq	lengths(%rip), %rdx
	movl	$0, testnum(%rip)
	movq	%rax, 176+names(%rip)
	xorl	%eax, %eax
	jmp	.L1046
.L1044:
	movl	mr(%rip), %eax
	leaq	.LC16(%rip), %rsi
	movsd	-7656(%rbp), %xmm0
	movl	%r13d, %edx
	movq	176+names(%rip), %rcx
	movq	bio_err(%rip), %rdi
	leaq	results(%rip), %r14
	testl	%eax, %eax
	leaq	.LC17(%rip), %rax
	cmove	%rax, %rsi
	movl	$1, %eax
	call	BIO_printf@PLT
	movq	lengths(%rip), %rdx
	pxor	%xmm1, %xmm1
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%r13d, %xmm0
	divsd	-7656(%rbp), %xmm0
	movl	testnum(%rip), %eax
	cvtsi2sdl	(%rdx,%rbx,4), %xmm1
	addl	$1, %eax
	movl	%eax, testnum(%rip)
	mulsd	%xmm1, %xmm0
	movsd	%xmm0, 1056(%r14,%rbx,8)
	cmpl	-7640(%rbp), %eax
	jnb	.L1007
.L1046:
	movl	mr(%rip), %ecx
	leaq	.LC15(%rip), %rsi
	movl	(%rdx,%rax,4), %r8d
	movl	-7648(%rbp), %ebx
	movq	176+names(%rip), %rdx
	testl	%ecx, %ecx
	movq	bio_err(%rip), %rdi
	cmovne	%r12, %rsi
	movl	%ebx, %ecx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	call	BIO_ctrl@PLT
	movl	%ebx, %edi
	movl	$1, run(%rip)
	call	alarm@PLT
	movl	usertime(%rip), %esi
	xorl	%edi, %edi
	call	app_tminterval@PLT
	movq	-7624(%rbp), %rdx
	movl	-7628(%rbp), %edi
	leaq	EVP_Digest_loop(%rip), %rsi
	call	run_benchmark
	movl	usertime(%rip), %esi
	movl	$1, %edi
	movl	%eax, %r13d
	call	app_tminterval@PLT
	xorl	%edi, %edi
	movsd	%xmm0, -7656(%rbp)
	call	alarm@PLT
	movslq	testnum(%rip), %rbx
	cmpl	$-1, %r13d
	jne	.L1044
.L1881:
	movq	bio_err(%rip), %rdi
	leaq	.LC93(%rip), %rsi
	call	BIO_puts@PLT
	movl	$1, %edi
	call	exit@PLT
.L1901:
	movq	bio_err(%rip), %rdi
	leaq	.LC94(%rip), %rsi
	call	BIO_printf@PLT
	movl	$1, %edi
	call	exit@PLT
	.p2align 4,,10
	.p2align 3
.L1912:
	movl	-7220(%rbp), %ecx
	testl	%ecx, %ecx
	jne	.L999
	jmp	.L988
.L1915:
	movl	-7600(%rbp), %eax
	movl	%eax, -7576(%rbp)
	testl	%eax, %eax
	je	.L1343
	leaq	-7576(%rbp), %rax
	movq	%rax, -7664(%rbp)
.L1014:
	movq	-7664(%rbp), %r14
	movslq	%r12d, %rax
	leaq	.LC98(%rip), %rsi
	leaq	-4(%r14,%rax,4), %rbx
	movl	(%rbx), %edi
	call	app_malloc@PLT
	movl	(%rbx), %edi
	leaq	.LC99(%rip), %rsi
	movq	%rax, -7720(%rbp)
	addl	$1024, %edi
	call	app_malloc@PLT
	movq	%rax, -7680(%rbp)
	call	EVP_CIPHER_CTX_new@PLT
	movq	-7656(%rbp), %rsi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %r13
	leaq	-1344(%rbp), %r8
	movq	%rax, %rdi
	call	EVP_EncryptInit_ex@PLT
	movq	%r13, %rdi
	call	EVP_CIPHER_CTX_key_length@PLT
	leaq	.LC100(%rip), %rsi
	movl	%eax, %edi
	movl	%eax, %ebx
	call	app_malloc@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	movq	%rax, -7640(%rbp)
	call	EVP_CIPHER_CTX_rand_key@PLT
	movq	-7640(%rbp), %r9
	xorl	%edx, %edx
	xorl	%esi, %esi
	xorl	%r8d, %r8d
	movq	%r13, %rdi
	movq	%r9, %rcx
	call	EVP_EncryptInit_ex@PLT
	movq	-7640(%rbp), %r9
	movslq	%ebx, %rsi
	movl	$3637, %ecx
	leaq	.LC101(%rip), %rdx
	movq	%r9, %rdi
	call	CRYPTO_clear_free@PLT
	leaq	-1088(%rbp), %rcx
	movl	$32, %edx
	movq	%r13, %rdi
	movl	$23, %esi
	call	EVP_CIPHER_CTX_ctrl@PLT
	movq	-7656(%rbp), %rdi
	call	EVP_CIPHER_nid@PLT
	movl	%eax, %edi
	call	OBJ_nid2ln@PLT
	movq	%r14, -7640(%rbp)
	movq	%rax, -7728(%rbp)
	leaq	1056+results(%rip), %rax
	movq	%rax, -7656(%rbp)
	leal	-1(%r12), %eax
	leaq	4(%r14,%rax,4), %rax
	movq	%rax, -7736(%rbp)
.L1023:
	movl	mr(%rip), %r11d
	leaq	.LC14(%rip), %rax
	leaq	.LC15(%rip), %rsi
	movl	-7648(%rbp), %ebx
	movq	-7728(%rbp), %rdx
	movq	bio_err(%rip), %rdi
	testl	%r11d, %r11d
	movl	%ebx, %ecx
	cmovne	%rax, %rsi
	movq	-7640(%rbp), %rax
	movl	(%rax), %r8d
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	call	BIO_ctrl@PLT
	movl	%ebx, %edi
	movl	$1, run(%rip)
	call	alarm@PLT
	movl	usertime(%rip), %esi
	xorl	%edi, %edi
	call	app_tminterval@PLT
	movl	run(%rip), %ebx
	testl	%ebx, %ebx
	je	.L1016
	movq	-7680(%rbp), %xmm7
	leaq	-7568(%rbp), %rax
	xorl	%ebx, %ebx
	movq	%rax, -7752(%rbp)
	leaq	-1357(%rbp), %rax
	movhps	-7720(%rbp), %xmm7
	movq	%rax, -7744(%rbp)
	movaps	%xmm7, -7776(%rbp)
	jmp	.L1019
.L1961:
	movdqa	-7776(%rbp), %xmm7
	movq	-7752(%rbp), %rcx
	movl	$32, %edx
	movq	%r13, %rdi
	movl	$26, %esi
	movq	%r14, -7552(%rbp)
	movaps	%xmm7, -7568(%rbp)
	call	EVP_CIPHER_CTX_ctrl@PLT
.L1018:
	movl	run(%rip), %eax
	addl	$1, %ebx
	testl	%eax, %eax
	je	.L1016
	cmpl	$2147483647, %ebx
	je	.L1016
.L1019:
	movq	-7640(%rbp), %rax
	movq	-7752(%rbp), %rcx
	movl	$32, %edx
	movq	%r13, %rdi
	movl	$25, %esi
	movl	$131863, -1349(%rbp)
	movslq	(%rax), %r14
	movq	-7744(%rbp), %rax
	movb	$0, -1345(%rbp)
	movq	$0, -7568(%rbp)
	movq	$0, (%rax)
	movq	%rax, -7560(%rbp)
	movq	%r14, -7552(%rbp)
	movl	$8, -7544(%rbp)
	call	EVP_CIPHER_CTX_ctrl@PLT
	testl	%eax, %eax
	jg	.L1961
	movq	-7680(%rbp), %rdi
	addq	$16, %r14
	movl	$16, %esi
	call	RAND_bytes@PLT
	movl	%r14d, %eax
	movq	%r13, %rdi
	movl	$13, %edx
	movq	-7744(%rbp), %rcx
	rolw	$8, %ax
	movl	$22, %esi
	movw	%ax, -1346(%rbp)
	call	EVP_CIPHER_CTX_ctrl@PLT
	movq	-7720(%rbp), %rdx
	movq	-7680(%rbp), %rsi
	movq	%r13, %rdi
	leal	(%rax,%r14), %ecx
	call	EVP_Cipher@PLT
	jmp	.L1018
.L1361:
	movl	$1, %r15d
	jmp	.L636
.L1910:
	movq	96+names(%rip), %rdx
	movq	bio_err(%rip), %rdi
	leaq	.LC96(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movl	$0, -7232(%rbp)
	jmp	.L977
.L968:
	movl	-7224(%rbp), %eax
	testl	%eax, %eax
	je	.L990
	movl	-7628(%rbp), %eax
	testl	%eax, %eax
	je	.L989
	movq	112+names(%rip), %rdx
	movq	bio_err(%rip), %rdi
	leaq	.LC96(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movl	$0, -7224(%rbp)
.L989:
	movl	-7672(%rbp), %r14d
	movl	$0, testnum(%rip)
	testl	%r14d, %r14d
	jne	.L990
	movq	lengths(%rip), %rax
	leaq	results(%rip), %r14
	jmp	.L1203
.L1906:
	movl	-7240(%rbp), %edx
	testl	%edx, %edx
	jne	.L960
	movl	-7236(%rbp), %eax
	testl	%eax, %eax
	jne	.L970
	jmp	.L959
.L1883:
	leaq	.LC97(%rip), %rsi
	jmp	.L1879
	.p2align 4,,10
	.p2align 3
.L1904:
	movq	160+names(%rip), %rdx
	movq	bio_err(%rip), %rdi
	leaq	.LC96(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movl	$0, -7200(%rbp)
	jmp	.L938
.L930:
	movl	-7196(%rbp), %r14d
	testl	%r14d, %r14d
	je	.L951
	movl	-7628(%rbp), %r11d
	testl	%r11d, %r11d
	je	.L950
	movq	168+names(%rip), %rdx
	movq	bio_err(%rip), %rdi
	leaq	.LC96(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movl	$0, -7196(%rbp)
.L950:
	movl	-7672(%rbp), %r10d
	movl	$0, testnum(%rip)
	testl	%r10d, %r10d
	jne	.L951
	movq	lengths(%rip), %rax
	leaq	results(%rip), %r14
	jmp	.L1205
.L1902:
	movq	152+names(%rip), %rdx
	movq	bio_err(%rip), %rdi
	leaq	.LC96(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movl	$0, -7204(%rbp)
	jmp	.L931
.L1882:
	movq	bio_err(%rip), %rdi
	leaq	.LC114(%rip), %rsi
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	movl	$1, %edi
	call	exit@PLT
.L1917:
	leaq	.LC113(%rip), %rsi
	jmp	.L1879
.L1916:
	movq	-7656(%rbp), %rdi
	call	EVP_CIPHER_flags@PLT
	testl	$2097152, %eax
	je	.L1033
	leaq	EVP_Update_loop_aead(%rip), %rax
	movq	%rax, -7744(%rbp)
	leaq	lengths_list(%rip), %rax
	cmpq	%rax, lengths(%rip)
	jne	.L1033
	leaq	aead_lengths_list(%rip), %rax
	movl	$6, -7640(%rbp)
	movq	%rax, lengths(%rip)
	jmp	.L1033
	.p2align 4,,10
	.p2align 3
.L1016:
	movl	usertime(%rip), %esi
	movl	$1, %edi
	call	app_tminterval@PLT
	xorl	%edi, %edi
	movsd	%xmm0, -7744(%rbp)
	call	alarm@PLT
	movl	mr(%rip), %r10d
	leaq	.LC102(%rip), %rcx
	movl	%ebx, %edx
	movsd	-7744(%rbp), %xmm1
	leaq	.LC16(%rip), %rsi
	testl	%r10d, %r10d
	movapd	%xmm1, %xmm0
	jne	.L1874
	movsd	%xmm1, -7744(%rbp)
	leaq	.LC102(%rip), %rcx
	leaq	.LC17(%rip), %rsi
.L1874:
	movq	bio_err(%rip), %rdi
	movl	$1, %eax
	call	BIO_printf@PLT
	pxor	%xmm0, %xmm0
	movsd	-7744(%rbp), %xmm1
	movq	-7640(%rbp), %rax
	cvtsi2sdl	%ebx, %xmm0
	movq	-7656(%rbp), %rcx
	addq	$4, %rax
	addq	$8, %rcx
	divsd	%xmm1, %xmm0
	pxor	%xmm1, %xmm1
	cvtsi2sdl	-4(%rax), %xmm1
	mulsd	%xmm1, %xmm0
	movsd	%xmm0, -8(%rcx)
	movq	%rax, -7640(%rbp)
	movq	%rcx, -7656(%rbp)
	cmpq	%rax, -7736(%rbp)
	jne	.L1023
	cmpl	$0, mr(%rip)
	movq	stdout(%rip), %rcx
	je	.L1024
	movl	$2, %edx
	movl	$1, %esi
	leaq	.LC103(%rip), %rdi
	xorl	%ebx, %ebx
	call	fwrite@PLT
	leaq	.LC104(%rip), %r14
.L1025:
	movq	-7664(%rbp), %rax
	movq	stdout(%rip), %rdi
	movq	%r14, %rdx
	movl	$1, %esi
	movl	(%rax,%rbx,4), %ecx
	xorl	%eax, %eax
	addq	$1, %rbx
	call	__fprintf_chk@PLT
	cmpl	%ebx, %r12d
	jg	.L1025
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	xorl	%ebx, %ebx
	leaq	.LC37(%rip), %r14
	call	fputc@PLT
	movq	-7728(%rbp), %r8
	xorl	%eax, %eax
	movq	stdout(%rip), %rdi
	movl	$22, %ecx
	leaq	.LC105(%rip), %rdx
	movl	$1, %esi
	call	__fprintf_chk@PLT
.L1026:
	leaq	1056+results(%rip), %rax
	movq	stdout(%rip), %rdi
	movq	%r14, %rdx
	movl	$1, %esi
	movsd	(%rax,%rbx,8), %xmm0
	movl	$1, %eax
	addq	$1, %rbx
	call	__fprintf_chk@PLT
	cmpl	%ebx, %r12d
	jg	.L1026
.L1875:
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	call	fputc@PLT
	movq	-7720(%rbp), %rdi
	movl	$3716, %edx
	leaq	.LC101(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-7680(%rbp), %rdi
	movl	$3717, %edx
	leaq	.LC101(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%r13, %rdi
	call	EVP_CIPHER_CTX_free@PLT
	jmp	.L636
.L1343:
	leaq	mblengths_list.27867(%rip), %rax
	movl	$5, %r12d
	movq	%rax, -7664(%rbp)
	jmp	.L1014
.L1024:
	movl	$58, %edx
	movl	$1, %esi
	leaq	.LC106(%rip), %rdi
	xorl	%ebx, %ebx
	call	fwrite@PLT
	movl	$24, %edx
	movq	stdout(%rip), %rcx
	movl	$1, %esi
	leaq	.LC107(%rip), %rdi
	leaq	.LC108(%rip), %r14
	call	fwrite@PLT
.L1028:
	movq	-7664(%rbp), %rax
	movq	stdout(%rip), %rdi
	movq	%r14, %rdx
	movl	$1, %esi
	movl	(%rax,%rbx,4), %ecx
	xorl	%eax, %eax
	addq	$1, %rbx
	call	__fprintf_chk@PLT
	cmpl	%ebx, %r12d
	jg	.L1028
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	xorl	%ebx, %ebx
	call	fputc@PLT
	movq	-7728(%rbp), %rcx
	movl	$1, %esi
	xorl	%eax, %eax
	movq	stdout(%rip), %rdi
	leaq	.LC109(%rip), %rdx
	call	__fprintf_chk@PLT
	movsd	.LC110(%rip), %xmm6
	movsd	%xmm6, -7640(%rbp)
.L1032:
	leaq	1056+results(%rip), %rax
	movq	stdout(%rip), %rdi
	movsd	(%rax,%rbx,8), %xmm0
	comisd	-7640(%rbp), %xmm0
	jbe	.L1799
	leaq	.LC112(%rip), %rdx
	movl	$1, %esi
	movl	$1, %eax
	divsd	.LC111(%rip), %xmm0
	call	__fprintf_chk@PLT
.L1031:
	addq	$1, %rbx
	cmpl	%ebx, %r12d
	jg	.L1032
	jmp	.L1875
	.p2align 4,,10
	.p2align 3
.L1068:
	movq	0(%r13), %rdi
	movl	$36, %esi
	call	RAND_bytes@PLT
	testl	%eax, %eax
	jle	.L1361
	addq	$768, %r13
	cmpq	%r13, -7656(%rbp)
	jne	.L1068
	leaq	dsa_results(%rip), %rcx
	xorl	%eax, %eax
	leaq	dsa_bits.27424(%rip), %rbx
	movl	$0, testnum(%rip)
	movq	%rcx, -7656(%rbp)
	leaq	-7516(%rbp), %rcx
	movq	%rcx, -7744(%rbp)
	jmp	.L1081
.L1070:
	movl	testnum(%rip), %eax
	addl	$1, %eax
	movl	%eax, testnum(%rip)
	cmpl	$2, %eax
	ja	.L1962
.L1081:
	movl	-7516(%rbp,%rax,4), %r8d
	testl	%r8d, %r8d
	je	.L1070
	movq	-7624(%rbp), %r12
.L1069:
	movl	testnum(%rip), %eax
	movq	24(%r12), %rcx
	xorl	%edi, %edi
	leaq	56(%r12), %r8
	movq	16(%r12), %rsi
	movl	$20, %edx
	movq	128(%r12,%rax,8), %r9
	call	DSA_sign@PLT
	testl	%eax, %eax
	je	.L1071
	addq	$768, %r12
	cmpq	%r12, -7680(%rbp)
	jne	.L1069
	movl	testnum(%rip), %eax
	movl	-7664(%rbp), %ecx
	leaq	.LC55(%rip), %rsi
	leaq	.LC121(%rip), %rdi
	movl	(%rbx,%rax,4), %edx
	call	pkey_print_message.isra.0
	movl	usertime(%rip), %esi
	xorl	%edi, %edi
	call	app_tminterval@PLT
	movq	-7624(%rbp), %rdx
	movl	-7628(%rbp), %edi
	leaq	DSA_sign_loop(%rip), %rsi
	call	run_benchmark
	movl	usertime(%rip), %esi
	movl	$1, %edi
	movslq	%eax, %r12
	call	app_tminterval@PLT
	xorl	%edi, %edi
	movsd	%xmm0, -7648(%rbp)
	call	alarm@PLT
	movl	testnum(%rip), %eax
	movq	%r12, %rdx
	cmpl	$0, mr(%rip)
	leaq	.LC22(%rip), %rsi
	movsd	-7648(%rbp), %xmm0
	movq	bio_err(%rip), %rdi
	movl	(%rbx,%rax,4), %ecx
	leaq	.LC23(%rip), %rax
	cmove	%rax, %rsi
	movl	$1, %eax
	call	BIO_printf@PLT
	movl	testnum(%rip), %eax
	movq	-7656(%rbp), %rcx
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%r12d, %xmm0
	divsd	-7648(%rbp), %xmm0
	movq	%rax, %rdx
	salq	$4, %rdx
	movsd	%xmm0, (%rcx,%rdx)
.L1073:
	movq	-7624(%rbp), %r13
.L1076:
	movq	24(%r13), %rcx
	movq	16(%r13), %rsi
	xorl	%edi, %edi
	movl	$20, %edx
	movq	128(%r13,%rax,8), %r9
	movl	56(%r13), %r8d
	call	DSA_verify@PLT
	testl	%eax, %eax
	jle	.L1075
	movl	testnum(%rip), %eax
	addq	$768, %r13
	cmpq	%r13, -7680(%rbp)
	jne	.L1076
	movl	-7664(%rbp), %ecx
	movl	(%rbx,%rax,4), %edx
	leaq	.LC55(%rip), %rsi
	leaq	.LC123(%rip), %rdi
	call	pkey_print_message.isra.0
	movl	usertime(%rip), %esi
	xorl	%edi, %edi
	call	app_tminterval@PLT
	movq	-7624(%rbp), %rdx
	movl	-7628(%rbp), %edi
	leaq	DSA_verify_loop(%rip), %rsi
	call	run_benchmark
	movl	usertime(%rip), %esi
	movl	$1, %edi
	movslq	%eax, %r14
	call	app_tminterval@PLT
	xorl	%edi, %edi
	movsd	%xmm0, -7648(%rbp)
	call	alarm@PLT
	movl	testnum(%rip), %eax
	movq	%r14, %rdx
	cmpl	$0, mr(%rip)
	leaq	.LC24(%rip), %rsi
	movsd	-7648(%rbp), %xmm0
	movq	bio_err(%rip), %rdi
	movl	(%rbx,%rax,4), %ecx
	leaq	.LC25(%rip), %rax
	cmove	%rax, %rsi
	movl	$1, %eax
	call	BIO_printf@PLT
	movl	testnum(%rip), %eax
	movq	-7656(%rbp), %rcx
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%r14d, %xmm0
	divsd	-7648(%rbp), %xmm0
	salq	$4, %rax
	movsd	%xmm0, 8(%rax,%rcx)
.L1078:
	subq	$1, %r12
	jg	.L1070
	movl	testnum(%rip), %ecx
	leal	1(%rcx), %eax
	movl	%eax, testnum(%rip)
	cmpl	$2, %eax
	ja	.L1070
	movq	-7744(%rbp), %rsi
	movl	$1, %edx
	movl	$3, testnum(%rip)
	subl	%ecx, %edx
	leaq	4(,%rdx,4), %rcx
	leaq	(%rsi,%rax,4), %rdx
	xorl	%eax, %eax
	movl	%ecx, %ecx
	movq	%rdx, %rdi
	rep stosb
	jmp	.L1070
.L1799:
	leaq	.LC36(%rip), %rdx
	movl	$1, %esi
	movl	$1, %eax
	call	__fprintf_chk@PLT
	jmp	.L1031
.L1919:
	call	BN_free@PLT
	movl	$1, %r15d
	jmp	.L636
.L1878:
	movq	%r14, %rdi
	movl	$1, %r15d
	call	BN_free@PLT
	jmp	.L636
.L1055:
	movq	bio_err(%rip), %rdi
	leaq	.LC116(%rip), %rsi
	xorl	%eax, %eax
	movl	$1, %ebx
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	movl	testnum(%rip), %eax
	jmp	.L1057
.L1060:
	movq	bio_err(%rip), %rdi
	leaq	.LC118(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	movl	testnum(%rip), %eax
	movl	$0, -7504(%rbp,%rax,4)
	movq	%rax, %rdx
	jmp	.L1063
.L1920:
	leal	2(%rdx), %eax
	jmp	.L1049
.L1962:
	leaq	ecdsa_results(%rip), %rax
	xorl	%ebx, %ebx
	leaq	.LC28(%rip), %r12
	movl	$0, testnum(%rip)
	movq	%rax, -7648(%rbp)
	jmp	.L1096
.L1083:
	movl	testnum(%rip), %eax
	leal	1(%rax), %ebx
	movl	%ebx, testnum(%rip)
	cmpl	$21, %ebx
	ja	.L1963
.L1096:
	movl	-7472(%rbp,%rbx,4), %edi
	testl	%edi, %edi
	je	.L1083
	movq	-7624(%rbp), %r13
.L1082:
	movq	%rbx, %rax
	leaq	test_curves.27430(%rip), %rcx
	salq	$4, %rax
	movl	8(%rcx,%rax), %edi
	call	EC_KEY_new_by_curve_name@PLT
	movq	%rax, 152(%r13,%rbx,8)
	movl	testnum(%rip), %ebx
	cmpq	$0, 152(%r13,%rbx,8)
	je	.L1084
	addq	$768, %r13
	cmpq	%r13, -7680(%rbp)
	jne	.L1082
	movq	-7624(%rbp), %rbx
	jmp	.L1086
.L1965:
	addq	$768, %rbx
	cmpq	%rbx, -7680(%rbp)
	je	.L1964
.L1086:
	movl	testnum(%rip), %eax
	xorl	%esi, %esi
	movq	152(%rbx,%rax,8), %rdi
	call	EC_KEY_precompute_mult@PLT
	movl	testnum(%rip), %eax
	movq	152(%rbx,%rax,8), %rdi
	call	EC_KEY_generate_key@PLT
	movq	24(%rbx), %rcx
	movq	16(%rbx), %rsi
	xorl	%edi, %edi
	movl	testnum(%rip), %eax
	leaq	56(%rbx), %r8
	movl	$20, %edx
	movq	152(%rbx,%rax,8), %r9
	call	ECDSA_sign@PLT
	testl	%eax, %eax
	jne	.L1965
	movq	bio_err(%rip), %rdi
	leaq	.LC157(%rip), %rsi
	xorl	%eax, %eax
	movl	$1, %ebx
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	movl	testnum(%rip), %eax
	jmp	.L1193
.L1963:
	movl	$0, testnum(%rip)
	xorl	%eax, %eax
	jmp	.L1115
.L1098:
	movl	testnum(%rip), %eax
	addl	$1, %eax
	movl	%eax, testnum(%rip)
	cmpl	$23, %eax
	ja	.L1966
.L1115:
	movl	-7376(%rbp,%rax,4), %esi
	testl	%esi, %esi
	je	.L1098
	leaq	-7568(%rbp), %rax
	movq	-7624(%rbp), %r12
	movq	%rax, -7648(%rbp)
.L1097:
	movq	$0, -7592(%rbp)
	movq	$0, -7584(%rbp)
	call	ERR_peek_error@PLT
	testq	%rax, %rax
	jne	.L1967
.L1099:
	movl	testnum(%rip), %eax
	leaq	test_curves.27430(%rip), %rcx
	xorl	%esi, %esi
	salq	$4, %rax
	movl	8(%rcx,%rax), %edi
	call	EVP_PKEY_CTX_new_id@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1968
.L1100:
	movq	%r13, %rdi
	call	EVP_PKEY_keygen_init@PLT
	testl	%eax, %eax
	jle	.L1107
	leaq	-7592(%rbp), %rsi
	movq	%r13, %rdi
	call	EVP_PKEY_keygen@PLT
	testl	%eax, %eax
	jle	.L1109
	leaq	-7584(%rbp), %rsi
	movq	%r13, %rdi
	call	EVP_PKEY_keygen@PLT
	testl	%eax, %eax
	jle	.L1109
	movq	-7592(%rbp), %rdi
	xorl	%esi, %esi
	call	EVP_PKEY_CTX_new@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L1109
	movq	%rax, %rdi
	call	EVP_PKEY_derive_init@PLT
	testl	%eax, %eax
	jle	.L1109
	movq	-7584(%rbp), %rsi
	movq	%rbx, %rdi
	call	EVP_PKEY_derive_set_peer@PLT
	testl	%eax, %eax
	jle	.L1109
	leaq	-7576(%rbp), %rax
	xorl	%esi, %esi
	movq	%rbx, %rdi
	movq	%rax, %rdx
	movq	%rax, -7656(%rbp)
	call	EVP_PKEY_derive@PLT
	testl	%eax, %eax
	jle	.L1109
	movq	-7576(%rbp), %rax
	subq	$1, %rax
	cmpq	$255, %rax
	ja	.L1109
	movq	-7584(%rbp), %rdi
	xorl	%esi, %esi
	call	EVP_PKEY_CTX_new@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L1111
	movq	%rax, %rdi
	call	EVP_PKEY_derive_init@PLT
	testl	%eax, %eax
	je	.L1111
	movq	-7592(%rbp), %rsi
	movq	%r14, %rdi
	call	EVP_PKEY_derive_set_peer@PLT
	testl	%eax, %eax
	je	.L1111
	movq	-7648(%rbp), %rdx
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	EVP_PKEY_derive@PLT
	testl	%eax, %eax
	je	.L1111
	movq	536(%r12), %rsi
	movq	-7656(%rbp), %rdx
	movq	%rbx, %rdi
	call	EVP_PKEY_derive@PLT
	testl	%eax, %eax
	je	.L1111
	movq	544(%r12), %rsi
	movq	-7648(%rbp), %rdx
	movq	%r14, %rdi
	call	EVP_PKEY_derive@PLT
	testl	%eax, %eax
	je	.L1111
	movq	-7576(%rbp), %rdx
	cmpq	%rdx, -7568(%rbp)
	jne	.L1111
	movq	544(%r12), %rsi
	movq	536(%r12), %rdi
	call	CRYPTO_memcmp@PLT
	testl	%eax, %eax
	jne	.L1969
	movl	testnum(%rip), %eax
	movq	-7592(%rbp), %rdi
	leaq	(%r12,%rax,8), %rax
	addq	$768, %r12
	movq	%rbx, 328(%rax)
	movq	-7576(%rbp), %rdx
	movq	%rdx, 552(%rax)
	call	EVP_PKEY_free@PLT
	movq	-7584(%rbp), %rdi
	call	EVP_PKEY_free@PLT
	movq	%r13, %rdi
	call	EVP_PKEY_CTX_free@PLT
	movq	%r14, %rdi
	call	EVP_PKEY_CTX_free@PLT
	cmpq	%r12, -7680(%rbp)
	jne	.L1097
.L1103:
	movl	testnum(%rip), %eax
	leaq	test_curves.27430(%rip), %r14
	movl	-7664(%rbp), %ecx
	leaq	.LC62(%rip), %rsi
	leaq	.LC132(%rip), %rdi
	salq	$4, %rax
	movl	12(%r14,%rax), %edx
	call	pkey_print_message.isra.0
	movl	usertime(%rip), %esi
	xorl	%edi, %edi
	call	app_tminterval@PLT
	movq	-7624(%rbp), %rdx
	movl	-7628(%rbp), %edi
	leaq	ECDH_EVP_derive_key_loop(%rip), %rsi
	call	run_benchmark
	movl	usertime(%rip), %esi
	movl	$1, %edi
	movslq	%eax, %rbx
	call	app_tminterval@PLT
	xorl	%edi, %edi
	movsd	%xmm0, -7648(%rbp)
	call	alarm@PLT
	movl	testnum(%rip), %eax
	movq	%rbx, %rdx
	leaq	.LC31(%rip), %rsi
	movsd	-7648(%rbp), %xmm0
	movq	bio_err(%rip), %rdi
	salq	$4, %rax
	cmpl	$0, mr(%rip)
	movl	12(%r14,%rax), %ecx
	leaq	.LC30(%rip), %rax
	cmovne	%rax, %rsi
	movl	$1, %eax
	call	BIO_printf@PLT
	movl	testnum(%rip), %edx
	pxor	%xmm0, %xmm0
	leaq	ecdh_results(%rip), %rax
	cvtsi2sdl	%ebx, %xmm0
	subq	$1, %rbx
	divsd	-7648(%rbp), %xmm0
	movsd	%xmm0, (%rax,%rdx,8)
	jg	.L1098
.L1105:
	movl	testnum(%rip), %ecx
	leal	1(%rcx), %eax
	movl	%eax, testnum(%rip)
	cmpl	$23, %eax
	ja	.L1098
	movq	-7736(%rbp), %rsi
	movl	$22, %edx
	movl	$24, testnum(%rip)
	subl	%ecx, %edx
	leaq	4(,%rdx,4), %rcx
	leaq	(%rsi,%rax,4), %rdx
	xorl	%eax, %eax
	movl	%ecx, %ecx
	movq	%rdx, %rdi
	rep stosb
	jmp	.L1098
.L1966:
	xorl	%eax, %eax
	leaq	test_ed_curves.27436(%rip), %r12
	movl	$0, testnum(%rip)
	leaq	-7524(%rbp), %r14
	jmp	.L1117
.L1124:
	movl	testnum(%rip), %eax
	addl	$1, %eax
	movl	%eax, testnum(%rip)
	cmpl	$1, %eax
	ja	.L1970
.L1117:
	movl	-7524(%rbp,%rax,4), %ecx
	movq	$0, -7568(%rbp)
	testl	%ecx, %ecx
	je	.L1124
	leaq	-7568(%rbp), %rax
	movq	-7624(%rbp), %r13
	movq	%rax, -7648(%rbp)
.L1116:
	movl	testnum(%rip), %ebx
	call	EVP_MD_CTX_new@PLT
	movq	%rax, 520(%r13,%rbx,8)
	movl	testnum(%rip), %eax
	cmpq	$0, 520(%r13,%rax,8)
	je	.L1119
	leaq	(%rax,%rax,2), %rax
	xorl	%esi, %esi
	movl	8(%r12,%rax,8), %edi
	call	EVP_PKEY_CTX_new_id@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L1120
	movq	%rax, %rdi
	call	EVP_PKEY_keygen_init@PLT
	testl	%eax, %eax
	jle	.L1120
	movq	-7648(%rbp), %rsi
	movq	%rbx, %rdi
	call	EVP_PKEY_keygen@PLT
	testl	%eax, %eax
	jle	.L1120
	movq	%rbx, %rdi
	call	EVP_PKEY_CTX_free@PLT
	movl	testnum(%rip), %eax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	-7568(%rbp), %r8
	xorl	%esi, %esi
	movq	520(%r13,%rax,8), %rdi
	call	EVP_DigestSignInit@PLT
	movq	-7568(%rbp), %rdi
	testl	%eax, %eax
	je	.L1971
	call	EVP_PKEY_free@PLT
	addq	$768, %r13
	cmpq	%r13, -7680(%rbp)
	jne	.L1116
	movq	-7624(%rbp), %rbx
.L1123:
	movl	testnum(%rip), %eax
	movq	16(%rbx), %rcx
	movl	$20, %r8d
	movq	24(%rbx), %rsi
	imulq	$24, %rax, %rdx
	movq	520(%rbx,%rax,8), %rdi
	movq	16(%r12,%rdx), %rdx
	movq	%rdx, 64(%rbx)
	leaq	64(%rbx), %rdx
	call	EVP_DigestSign@PLT
	testl	%eax, %eax
	je	.L1125
	addq	$768, %rbx
	cmpq	%rbx, -7680(%rbp)
	jne	.L1123
	movl	testnum(%rip), %eax
	movl	-7664(%rbp), %ecx
	leaq	.LC121(%rip), %rdi
	imulq	$24, %rax, %rax
	addq	%r12, %rax
	movl	12(%rax), %edx
	movq	(%rax), %rsi
	call	pkey_print_message.isra.0
	movl	usertime(%rip), %esi
	xorl	%edi, %edi
	call	app_tminterval@PLT
	movq	-7624(%rbp), %rdx
	movl	-7628(%rbp), %edi
	leaq	EdDSA_sign_loop(%rip), %rsi
	call	run_benchmark
	movl	usertime(%rip), %esi
	movl	$1, %edi
	movslq	%eax, %rbx
	call	app_tminterval@PLT
	xorl	%edi, %edi
	movsd	%xmm0, -7648(%rbp)
	call	alarm@PLT
	movl	testnum(%rip), %eax
	leaq	.LC33(%rip), %rdx
	leaq	.LC32(%rip), %rsi
	movsd	-7648(%rbp), %xmm0
	movq	bio_err(%rip), %rdi
	imulq	$24, %rax, %rax
	addq	%r12, %rax
	cmpl	$0, mr(%rip)
	movl	12(%rax), %ecx
	cmove	%rdx, %rsi
	movq	(%rax), %r8
	movq	%rbx, %rdx
	movl	$1, %eax
	call	BIO_printf@PLT
	movl	testnum(%rip), %eax
	pxor	%xmm0, %xmm0
	leaq	eddsa_results(%rip), %rcx
	cvtsi2sdl	%ebx, %xmm0
	divsd	-7648(%rbp), %xmm0
	movq	%rax, %rdx
	salq	$4, %rdx
	movsd	%xmm0, (%rcx,%rdx)
.L1194:
	movq	-7624(%rbp), %r13
.L1128:
	movq	16(%r13), %rcx
	movq	64(%r13), %rdx
	movl	$20, %r8d
	movq	24(%r13), %rsi
	movq	520(%r13,%rax,8), %rdi
	call	EVP_DigestVerify@PLT
	subl	$1, %eax
	jne	.L1127
	movl	testnum(%rip), %eax
	addq	$768, %r13
	cmpq	%r13, -7680(%rbp)
	jne	.L1128
	imulq	$24, %rax, %rax
	movl	-7664(%rbp), %ecx
	leaq	.LC123(%rip), %rdi
	addq	%r12, %rax
	movl	12(%rax), %edx
	movq	(%rax), %rsi
	call	pkey_print_message.isra.0
	movl	usertime(%rip), %esi
	xorl	%edi, %edi
	call	app_tminterval@PLT
	movq	-7624(%rbp), %rdx
	movl	-7628(%rbp), %edi
	leaq	EdDSA_verify_loop(%rip), %rsi
	call	run_benchmark
	movl	usertime(%rip), %esi
	movl	$1, %edi
	movslq	%eax, %rdx
	movq	%rdx, %r13
	movq	%rdx, -7656(%rbp)
	call	app_tminterval@PLT
	xorl	%edi, %edi
	movsd	%xmm0, -7648(%rbp)
	call	alarm@PLT
	movl	testnum(%rip), %eax
	leaq	.LC34(%rip), %rdx
	leaq	.LC35(%rip), %rsi
	movsd	-7648(%rbp), %xmm0
	movq	bio_err(%rip), %rdi
	imulq	$24, %rax, %rax
	addq	%r12, %rax
	cmpl	$0, mr(%rip)
	cmovne	%rdx, %rsi
	movl	12(%rax), %ecx
	movq	(%rax), %r8
	movl	$1, %eax
	movq	-7656(%rbp), %rdx
	call	BIO_printf@PLT
	movl	testnum(%rip), %eax
	pxor	%xmm0, %xmm0
	leaq	eddsa_results(%rip), %rdx
	cvtsi2sdl	%r13d, %xmm0
	divsd	-7648(%rbp), %xmm0
	salq	$4, %rax
	movsd	%xmm0, 8(%rdx,%rax)
.L1130:
	subq	$1, %rbx
	jg	.L1124
	movl	testnum(%rip), %edi
	xorl	%esi, %esi
	leal	1(%rdi), %eax
	movl	%eax, testnum(%rip)
	movq	%rax, %rdx
	leaq	(%r14,%rax,4), %rcx
	jmp	.L1133
.L1134:
	movl	$0, (%rcx)
	addl	$1, %eax
	addq	$4, %rcx
	movl	$1, %esi
.L1133:
	cmpl	$1, %eax
	jbe	.L1134
	movl	$1, %eax
	movl	$0, %ecx
	subl	%edi, %eax
	cmpl	$2, %edx
	cmova	%ecx, %eax
	addl	%edx, %eax
	testb	%sil, %sil
	je	.L1124
	movl	%eax, testnum(%rip)
	jmp	.L1124
.L1971:
	call	EVP_PKEY_free@PLT
.L1119:
	movq	bio_err(%rip), %rdi
	leaq	.LC133(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	jmp	.L1124
.L1120:
	movq	%rbx, %rdi
	call	EVP_PKEY_CTX_free@PLT
	jmp	.L1119
.L1125:
	movq	bio_err(%rip), %rdi
	leaq	.LC159(%rip), %rsi
	xorl	%eax, %eax
	movl	$1, %ebx
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	movl	testnum(%rip), %eax
	jmp	.L1194
.L1127:
	movq	bio_err(%rip), %rdi
	leaq	.LC134(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	movl	testnum(%rip), %eax
	movl	$0, -7524(%rbp,%rax,4)
	jmp	.L1130
.L1969:
	leaq	.LC131(%rip), %rsi
.L1876:
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	jmp	.L1105
.L1111:
	leaq	.LC130(%rip), %rsi
	jmp	.L1876
.L1109:
	leaq	.LC129(%rip), %rsi
	jmp	.L1876
.L1968:
	movq	$0, -7568(%rbp)
	call	ERR_peek_error@PLT
	movq	%rax, %rbx
	call	ERR_peek_last_error@PLT
	cmpq	%rbx, %rax
	je	.L1972
.L1101:
	call	ERR_peek_error@PLT
	testq	%rax, %rax
	jne	.L1973
	xorl	%esi, %esi
	movl	$408, %edi
	call	EVP_PKEY_CTX_new_id@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L1106
	movq	%rax, %rdi
	call	EVP_PKEY_paramgen_init@PLT
	testl	%eax, %eax
	je	.L1106
	xorl	%r9d, %r9d
	movl	$6, %edx
	movl	$408, %esi
	movq	%r14, %rdi
	movl	testnum(%rip), %eax
	leaq	test_curves.27430(%rip), %rcx
	salq	$4, %rax
	movl	8(%rcx,%rax), %r8d
	movl	$4097, %ecx
	call	EVP_PKEY_CTX_ctrl@PLT
	testl	%eax, %eax
	je	.L1106
	movq	-7648(%rbp), %rsi
	movq	%r14, %rdi
	call	EVP_PKEY_paramgen@PLT
	testl	%eax, %eax
	je	.L1106
	movq	-7568(%rbp), %rdi
	xorl	%esi, %esi
	call	EVP_PKEY_CTX_new@PLT
	movq	-7568(%rbp), %rdi
	movq	%rax, %r13
	call	EVP_PKEY_free@PLT
	movq	%r14, %rdi
	movq	$0, -7568(%rbp)
	call	EVP_PKEY_CTX_free@PLT
	testq	%r13, %r13
	jne	.L1100
.L1107:
	leaq	.LC128(%rip), %rsi
	jmp	.L1876
.L1967:
	movq	bio_err(%rip), %rdi
	leaq	.LC125(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	jmp	.L1099
.L1106:
	leaq	.LC127(%rip), %rsi
	jmp	.L1876
.L1973:
	movq	bio_err(%rip), %rdi
	leaq	.LC126(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	jmp	.L1103
.L1972:
	movl	%eax, %edx
	shrl	$24, %edx
	cmpl	$6, %edx
	jne	.L1101
	movq	%rax, %rdx
	shrq	$12, %rdx
	andl	$4095, %edx
	cmpl	$157, %edx
	jne	.L1101
	andl	$4095, %eax
	cmpl	$156, %eax
	jne	.L1101
	call	ERR_get_error@PLT
	jmp	.L1101
.L1071:
	movq	bio_err(%rip), %rdi
	leaq	.LC120(%rip), %rsi
	xorl	%eax, %eax
	movl	$1, %r12d
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	movl	testnum(%rip), %eax
	jmp	.L1073
.L1075:
	movq	bio_err(%rip), %rdi
	leaq	.LC122(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	movl	testnum(%rip), %eax
	movl	$0, -7516(%rbp,%rax,4)
	jmp	.L1078
.L1084:
	movq	bio_err(%rip), %rdi
	leaq	.LC158(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	jmp	.L1083
.L1964:
	movl	testnum(%rip), %eax
	leaq	test_curves.27430(%rip), %r14
	movl	-7664(%rbp), %ecx
	leaq	.LC61(%rip), %rsi
	leaq	.LC121(%rip), %rdi
	salq	$4, %rax
	movl	12(%r14,%rax), %edx
	call	pkey_print_message.isra.0
	movl	usertime(%rip), %esi
	xorl	%edi, %edi
	call	app_tminterval@PLT
	movq	-7624(%rbp), %rdx
	movl	-7628(%rbp), %edi
	leaq	ECDSA_sign_loop(%rip), %rsi
	call	run_benchmark
	movl	usertime(%rip), %esi
	movl	$1, %edi
	movslq	%eax, %rbx
	call	app_tminterval@PLT
	xorl	%edi, %edi
	movsd	%xmm0, -7656(%rbp)
	call	alarm@PLT
	movl	testnum(%rip), %eax
	movq	%rbx, %rdx
	leaq	.LC26(%rip), %rsi
	movsd	-7656(%rbp), %xmm0
	movq	bio_err(%rip), %rdi
	salq	$4, %rax
	cmpl	$0, mr(%rip)
	movl	12(%r14,%rax), %ecx
	leaq	.LC27(%rip), %rax
	cmove	%rax, %rsi
	movl	$1, %eax
	call	BIO_printf@PLT
	movl	testnum(%rip), %eax
	movq	-7648(%rbp), %rcx
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%ebx, %xmm0
	divsd	-7656(%rbp), %xmm0
	movq	%rax, %rdx
	salq	$4, %rdx
	movsd	%xmm0, (%rcx,%rdx)
.L1193:
	movq	-7624(%rbp), %r13
.L1089:
	movq	24(%r13), %rcx
	movq	16(%r13), %rsi
	xorl	%edi, %edi
	movl	$20, %edx
	movq	152(%r13,%rax,8), %r9
	movl	56(%r13), %r8d
	call	ECDSA_verify@PLT
	cmpl	$1, %eax
	jne	.L1088
	movl	testnum(%rip), %eax
	addq	$768, %r13
	cmpq	%r13, -7680(%rbp)
	jne	.L1089
	salq	$4, %rax
	leaq	test_curves.27430(%rip), %rsi
	movl	-7664(%rbp), %ecx
	leaq	.LC123(%rip), %rdi
	movl	12(%rsi,%rax), %edx
	leaq	.LC61(%rip), %rsi
	call	pkey_print_message.isra.0
	movl	usertime(%rip), %esi
	xorl	%edi, %edi
	call	app_tminterval@PLT
	movq	-7624(%rbp), %rdx
	movl	-7628(%rbp), %edi
	leaq	ECDSA_verify_loop(%rip), %rsi
	call	run_benchmark
	movl	usertime(%rip), %esi
	movl	$1, %edi
	movslq	%eax, %r14
	call	app_tminterval@PLT
	xorl	%edi, %edi
	movsd	%xmm0, -7656(%rbp)
	call	alarm@PLT
	movl	testnum(%rip), %eax
	movq	%r14, %rdx
	leaq	test_curves.27430(%rip), %rsi
	movsd	-7656(%rbp), %xmm0
	movq	bio_err(%rip), %rdi
	salq	$4, %rax
	cmpl	$0, mr(%rip)
	movl	12(%rsi,%rax), %ecx
	leaq	.LC29(%rip), %rsi
	movl	$1, %eax
	cmovne	%r12, %rsi
	call	BIO_printf@PLT
	movl	testnum(%rip), %eax
	movq	-7648(%rbp), %rcx
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%r14d, %xmm0
	divsd	-7656(%rbp), %xmm0
	salq	$4, %rax
	movsd	%xmm0, 8(%rax,%rcx)
.L1091:
	subq	$1, %rbx
	jg	.L1083
	movl	testnum(%rip), %edi
	movq	-7728(%rbp), %rcx
	xorl	%esi, %esi
	leal	1(%rdi), %eax
	movl	%eax, testnum(%rip)
	movq	%rax, %rdx
	leaq	(%rcx,%rax,4), %rcx
	jmp	.L1094
.L1095:
	movl	$0, (%rcx)
	addl	$1, %eax
	addq	$4, %rcx
	movl	$1, %esi
.L1094:
	cmpl	$21, %eax
	jbe	.L1095
	movl	$21, %eax
	movl	$0, %ecx
	subl	%edi, %eax
	cmpl	$22, %edx
	cmova	%ecx, %eax
	addl	%edx, %eax
	testb	%sil, %sil
	je	.L1083
	movl	%eax, testnum(%rip)
	jmp	.L1083
.L1088:
	movq	bio_err(%rip), %rdi
	leaq	.LC124(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	movl	testnum(%rip), %eax
	movl	$0, -7472(%rbp,%rax,4)
	jmp	.L1091
	.cfi_endproc
.LFE1473:
	.size	speed_main, .-speed_main
	.section	.rodata
	.align 16
	.type	mblengths_list.27867, @object
	.size	mblengths_list.27867, 20
mblengths_list.27867:
	.long	8192
	.long	16384
	.long	32768
	.long	65536
	.long	131072
	.data
	.type	sep.27834, @object
	.size	sep.27834, 2
sep.27834:
	.string	":"
	.section	.rodata.str1.1
.LC160:
	.string	"Ed25519"
.LC161:
	.string	"Ed448"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	test_ed_curves.27436, @object
	.size	test_ed_curves.27436, 48
test_ed_curves.27436:
	.quad	.LC160
	.long	1087
	.long	253
	.quad	64
	.quad	.LC161
	.long	1088
	.long	456
	.quad	114
	.section	.rodata.str1.1
.LC162:
	.string	"secp160r1"
.LC163:
	.string	"nistp192"
.LC164:
	.string	"nistp224"
.LC165:
	.string	"nistp256"
.LC166:
	.string	"nistp384"
.LC167:
	.string	"nistp521"
.LC168:
	.string	"nistk163"
.LC169:
	.string	"nistk233"
.LC170:
	.string	"nistk283"
.LC171:
	.string	"nistk409"
.LC172:
	.string	"nistk571"
.LC173:
	.string	"nistb163"
.LC174:
	.string	"nistb233"
.LC175:
	.string	"nistb283"
.LC176:
	.string	"nistb409"
.LC177:
	.string	"nistb571"
.LC178:
	.string	"brainpoolP256r1"
.LC179:
	.string	"brainpoolP256t1"
.LC180:
	.string	"brainpoolP384r1"
.LC181:
	.string	"brainpoolP384t1"
.LC182:
	.string	"brainpoolP512r1"
.LC183:
	.string	"brainpoolP512t1"
.LC184:
	.string	"X25519"
.LC185:
	.string	"X448"
	.section	.data.rel.ro.local
	.align 32
	.type	test_curves.27430, @object
	.size	test_curves.27430, 384
test_curves.27430:
	.quad	.LC162
	.long	709
	.long	160
	.quad	.LC163
	.long	409
	.long	192
	.quad	.LC164
	.long	713
	.long	224
	.quad	.LC165
	.long	415
	.long	256
	.quad	.LC166
	.long	715
	.long	384
	.quad	.LC167
	.long	716
	.long	521
	.quad	.LC168
	.long	721
	.long	163
	.quad	.LC169
	.long	726
	.long	233
	.quad	.LC170
	.long	729
	.long	283
	.quad	.LC171
	.long	731
	.long	409
	.quad	.LC172
	.long	733
	.long	571
	.quad	.LC173
	.long	723
	.long	163
	.quad	.LC174
	.long	727
	.long	233
	.quad	.LC175
	.long	730
	.long	283
	.quad	.LC176
	.long	732
	.long	409
	.quad	.LC177
	.long	734
	.long	571
	.quad	.LC178
	.long	927
	.long	256
	.quad	.LC179
	.long	928
	.long	256
	.quad	.LC180
	.long	931
	.long	384
	.quad	.LC181
	.long	932
	.long	384
	.quad	.LC182
	.long	933
	.long	512
	.quad	.LC183
	.long	934
	.long	512
	.quad	.LC184
	.long	1034
	.long	253
	.quad	.LC185
	.long	1035
	.long	448
	.section	.rodata
	.align 8
	.type	dsa_bits.27424, @object
	.size	dsa_bits.27424, 12
dsa_bits.27424:
	.long	512
	.long	1024
	.long	2048
	.align 16
	.type	rsa_bits.27419, @object
	.size	rsa_bits.27419, 28
rsa_bits.27419:
	.long	512
	.long	1024
	.long	2048
	.long	3072
	.long	4096
	.long	7680
	.long	15360
	.align 16
	.type	hmac_key.27526, @object
	.size	hmac_key.27526, 17
hmac_key.27526:
	.string	"This is a key..."
	.align 32
	.type	ckey32.27412, @object
	.size	ckey32.27412, 32
ckey32.27412:
	.ascii	"\0224Vx\232\274\336\3604Vx\232\274\336\360\022Vx\232\274\336"
	.ascii	"\360\0224x\232\274\336\360\0224V"
	.align 16
	.type	ckey24.27411, @object
	.size	ckey24.27411, 24
ckey24.27411:
	.ascii	"\0224Vx\232\274\336\3604Vx\232\274\336\360\022Vx\232\274\336"
	.ascii	"\360\0224"
	.align 32
	.type	key32.27410, @object
	.size	key32.27410, 32
key32.27410:
	.ascii	"\0224Vx\232\274\336\3604Vx\232\274\336\360\022Vx\232\274\336"
	.ascii	"\360\0224x\232\274\336\360\0224V"
	.align 16
	.type	key24.27409, @object
	.size	key24.27409, 24
key24.27409:
	.ascii	"\0224Vx\232\274\336\3604Vx\232\274\336\360\022Vx\232\274\336"
	.ascii	"\360\0224"
	.align 16
	.type	key16.27408, @object
	.size	key16.27408, 16
key16.27408:
	.ascii	"\0224Vx\232\274\336\3604Vx\232\274\336\360\022"
	.data
	.align 8
	.type	key3.27418, @object
	.size	key3.27418, 8
key3.27418:
	.ascii	"Vx\232\274\336\360\0224"
	.align 8
	.type	key2.27417, @object
	.size	key2.27417, 8
key2.27417:
	.ascii	"4Vx\232\274\336\360\022"
	.align 8
	.type	key.27416, @object
	.size	key.27416, 8
key.27416:
	.ascii	"\0224Vx\232\274\336\360"
	.section	.rodata
	.align 16
	.type	rsa_data_length.27421, @object
	.size	rsa_data_length.27421, 28
rsa_data_length.27421:
	.long	318
	.long	608
	.long	1191
	.long	1767
	.long	2349
	.long	4365
	.long	8684
	.section	.data.rel.ro.local
	.align 32
	.type	rsa_data.27420, @object
	.size	rsa_data.27420, 56
rsa_data.27420:
	.quad	test512
	.quad	test1024
	.quad	test2048
	.quad	test3072
	.quad	test4096
	.quad	test7680
	.quad	test15360
	.local	evp_md
	.comm	evp_md,8,8
	.local	decrypt
	.comm	decrypt,4,4
	.local	aes_ks3
	.comm	aes_ks3,244,32
	.local	aes_ks2
	.comm	aes_ks2,244,32
	.local	aes_ks1
	.comm	aes_ks1,244,32
	.local	iv
	.comm	iv,32,32
	.local	sch3
	.comm	sch3,128,32
	.local	sch2
	.comm	sch2,128,32
	.local	sch
	.comm	sch,128,32
	.local	DES_iv
	.comm	DES_iv,8,8
	.local	rc4_ks
	.comm	rc4_ks,1032,32
	.local	testnum
	.comm	testnum,4,4
	.local	eddsa_results
	.comm	eddsa_results,32,32
	.section	.rodata.str1.1
.LC186:
	.string	"ed25519"
.LC187:
	.string	"ed448"
	.section	.data.rel.local,"aw"
	.align 32
	.type	eddsa_choices, @object
	.size	eddsa_choices, 32
eddsa_choices:
	.quad	.LC186
	.long	0
	.zero	4
	.quad	.LC187
	.long	1
	.zero	4
	.local	ecdh_results
	.comm	ecdh_results,192,32
	.section	.rodata.str1.1
.LC188:
	.string	"ecdhp160"
.LC189:
	.string	"ecdhp192"
.LC190:
	.string	"ecdhp224"
.LC191:
	.string	"ecdhp256"
.LC192:
	.string	"ecdhp384"
.LC193:
	.string	"ecdhp521"
.LC194:
	.string	"ecdhk163"
.LC195:
	.string	"ecdhk233"
.LC196:
	.string	"ecdhk283"
.LC197:
	.string	"ecdhk409"
.LC198:
	.string	"ecdhk571"
.LC199:
	.string	"ecdhb163"
.LC200:
	.string	"ecdhb233"
.LC201:
	.string	"ecdhb283"
.LC202:
	.string	"ecdhb409"
.LC203:
	.string	"ecdhb571"
.LC204:
	.string	"ecdhbrp256r1"
.LC205:
	.string	"ecdhbrp256t1"
.LC206:
	.string	"ecdhbrp384r1"
.LC207:
	.string	"ecdhbrp384t1"
.LC208:
	.string	"ecdhbrp512r1"
.LC209:
	.string	"ecdhbrp512t1"
.LC210:
	.string	"ecdhx25519"
.LC211:
	.string	"ecdhx448"
	.section	.data.rel.ro.local
	.align 32
	.type	ecdh_choices, @object
	.size	ecdh_choices, 384
ecdh_choices:
	.quad	.LC188
	.long	0
	.zero	4
	.quad	.LC189
	.long	1
	.zero	4
	.quad	.LC190
	.long	2
	.zero	4
	.quad	.LC191
	.long	3
	.zero	4
	.quad	.LC192
	.long	4
	.zero	4
	.quad	.LC193
	.long	5
	.zero	4
	.quad	.LC194
	.long	6
	.zero	4
	.quad	.LC195
	.long	7
	.zero	4
	.quad	.LC196
	.long	8
	.zero	4
	.quad	.LC197
	.long	9
	.zero	4
	.quad	.LC198
	.long	10
	.zero	4
	.quad	.LC199
	.long	11
	.zero	4
	.quad	.LC200
	.long	12
	.zero	4
	.quad	.LC201
	.long	13
	.zero	4
	.quad	.LC202
	.long	14
	.zero	4
	.quad	.LC203
	.long	15
	.zero	4
	.quad	.LC204
	.long	16
	.zero	4
	.quad	.LC205
	.long	17
	.zero	4
	.quad	.LC206
	.long	18
	.zero	4
	.quad	.LC207
	.long	19
	.zero	4
	.quad	.LC208
	.long	20
	.zero	4
	.quad	.LC209
	.long	21
	.zero	4
	.quad	.LC210
	.long	22
	.zero	4
	.quad	.LC211
	.long	23
	.zero	4
	.local	ecdsa_results
	.comm	ecdsa_results,352,32
	.section	.rodata.str1.1
.LC212:
	.string	"ecdsap160"
.LC213:
	.string	"ecdsap192"
.LC214:
	.string	"ecdsap224"
.LC215:
	.string	"ecdsap256"
.LC216:
	.string	"ecdsap384"
.LC217:
	.string	"ecdsap521"
.LC218:
	.string	"ecdsak163"
.LC219:
	.string	"ecdsak233"
.LC220:
	.string	"ecdsak283"
.LC221:
	.string	"ecdsak409"
.LC222:
	.string	"ecdsak571"
.LC223:
	.string	"ecdsab163"
.LC224:
	.string	"ecdsab233"
.LC225:
	.string	"ecdsab283"
.LC226:
	.string	"ecdsab409"
.LC227:
	.string	"ecdsab571"
.LC228:
	.string	"ecdsabrp256r1"
.LC229:
	.string	"ecdsabrp256t1"
.LC230:
	.string	"ecdsabrp384r1"
.LC231:
	.string	"ecdsabrp384t1"
.LC232:
	.string	"ecdsabrp512r1"
.LC233:
	.string	"ecdsabrp512t1"
	.section	.data.rel.local
	.align 32
	.type	ecdsa_choices, @object
	.size	ecdsa_choices, 352
ecdsa_choices:
	.quad	.LC212
	.long	0
	.zero	4
	.quad	.LC213
	.long	1
	.zero	4
	.quad	.LC214
	.long	2
	.zero	4
	.quad	.LC215
	.long	3
	.zero	4
	.quad	.LC216
	.long	4
	.zero	4
	.quad	.LC217
	.long	5
	.zero	4
	.quad	.LC218
	.long	6
	.zero	4
	.quad	.LC219
	.long	7
	.zero	4
	.quad	.LC220
	.long	8
	.zero	4
	.quad	.LC221
	.long	9
	.zero	4
	.quad	.LC222
	.long	10
	.zero	4
	.quad	.LC223
	.long	11
	.zero	4
	.quad	.LC224
	.long	12
	.zero	4
	.quad	.LC225
	.long	13
	.zero	4
	.quad	.LC226
	.long	14
	.zero	4
	.quad	.LC227
	.long	15
	.zero	4
	.quad	.LC228
	.long	16
	.zero	4
	.quad	.LC229
	.long	17
	.zero	4
	.quad	.LC230
	.long	18
	.zero	4
	.quad	.LC231
	.long	19
	.zero	4
	.quad	.LC232
	.long	20
	.zero	4
	.quad	.LC233
	.long	21
	.zero	4
	.local	rsa_results
	.comm	rsa_results,112,32
	.section	.data.rel.ro.local
	.align 32
	.type	rsa_choices, @object
	.size	rsa_choices, 112
rsa_choices:
	.quad	.LC47
	.long	0
	.zero	4
	.quad	.LC48
	.long	1
	.zero	4
	.quad	.LC49
	.long	2
	.zero	4
	.quad	.LC50
	.long	3
	.zero	4
	.quad	.LC51
	.long	4
	.zero	4
	.quad	.LC52
	.long	5
	.zero	4
	.quad	.LC53
	.long	6
	.zero	4
	.local	dsa_results
	.comm	dsa_results,48,32
	.local	results
	.comm	results,1488,32
	.section	.rodata.str1.1
.LC234:
	.string	"mdc2"
.LC235:
	.string	"md4"
.LC236:
	.string	"md5"
.LC237:
	.string	"hmac"
.LC238:
	.string	"sha1"
.LC239:
	.string	"sha256"
.LC240:
	.string	"sha512"
.LC241:
	.string	"whirlpool"
.LC242:
	.string	"ripemd"
.LC243:
	.string	"rmd160"
.LC244:
	.string	"ripemd160"
.LC245:
	.string	"rc4"
.LC246:
	.string	"des-cbc"
.LC247:
	.string	"des-ede3"
.LC248:
	.string	"aes-128-cbc"
.LC249:
	.string	"aes-192-cbc"
.LC250:
	.string	"aes-256-cbc"
.LC251:
	.string	"aes-128-ige"
.LC252:
	.string	"aes-192-ige"
.LC253:
	.string	"aes-256-ige"
.LC254:
	.string	"rc2-cbc"
.LC255:
	.string	"rc2"
.LC256:
	.string	"idea-cbc"
.LC257:
	.string	"idea"
.LC258:
	.string	"seed-cbc"
.LC259:
	.string	"seed"
.LC260:
	.string	"bf-cbc"
.LC261:
	.string	"blowfish"
.LC262:
	.string	"bf"
.LC263:
	.string	"cast-cbc"
.LC264:
	.string	"cast"
.LC265:
	.string	"cast5"
.LC266:
	.string	"ghash"
.LC267:
	.string	"rand"
	.section	.data.rel.ro.local
	.align 32
	.type	doit_choices, @object
	.size	doit_choices, 544
doit_choices:
	.quad	.LC234
	.long	1
	.zero	4
	.quad	.LC235
	.long	2
	.zero	4
	.quad	.LC236
	.long	3
	.zero	4
	.quad	.LC237
	.long	4
	.zero	4
	.quad	.LC238
	.long	5
	.zero	4
	.quad	.LC239
	.long	23
	.zero	4
	.quad	.LC240
	.long	24
	.zero	4
	.quad	.LC241
	.long	25
	.zero	4
	.quad	.LC242
	.long	6
	.zero	4
	.quad	.LC243
	.long	6
	.zero	4
	.quad	.LC244
	.long	6
	.zero	4
	.quad	.LC245
	.long	7
	.zero	4
	.quad	.LC246
	.long	8
	.zero	4
	.quad	.LC247
	.long	9
	.zero	4
	.quad	.LC248
	.long	16
	.zero	4
	.quad	.LC249
	.long	17
	.zero	4
	.quad	.LC250
	.long	18
	.zero	4
	.quad	.LC251
	.long	26
	.zero	4
	.quad	.LC252
	.long	27
	.zero	4
	.quad	.LC253
	.long	28
	.zero	4
	.quad	.LC254
	.long	12
	.zero	4
	.quad	.LC255
	.long	12
	.zero	4
	.quad	.LC256
	.long	10
	.zero	4
	.quad	.LC257
	.long	10
	.zero	4
	.quad	.LC258
	.long	11
	.zero	4
	.quad	.LC259
	.long	11
	.zero	4
	.quad	.LC260
	.long	14
	.zero	4
	.quad	.LC261
	.long	14
	.zero	4
	.quad	.LC262
	.long	14
	.zero	4
	.quad	.LC263
	.long	15
	.zero	4
	.quad	.LC264
	.long	15
	.zero	4
	.quad	.LC265
	.long	15
	.zero	4
	.quad	.LC266
	.long	29
	.zero	4
	.quad	.LC267
	.long	30
	.zero	4
	.section	.rodata.str1.1
.LC268:
	.string	"md2"
.LC269:
	.string	"hmac(md5)"
.LC270:
	.string	"des cbc"
.LC271:
	.string	"des ede3"
.LC272:
	.string	"idea cbc"
.LC273:
	.string	"seed cbc"
.LC274:
	.string	"rc2 cbc"
.LC275:
	.string	"rc5-32/12 cbc"
.LC276:
	.string	"blowfish cbc"
.LC277:
	.string	"cast cbc"
.LC278:
	.string	"aes-128 cbc"
.LC279:
	.string	"aes-192 cbc"
.LC280:
	.string	"aes-256 cbc"
.LC281:
	.string	"camellia-128 cbc"
.LC282:
	.string	"camellia-192 cbc"
.LC283:
	.string	"camellia-256 cbc"
.LC284:
	.string	"aes-128 ige"
.LC285:
	.string	"aes-192 ige"
.LC286:
	.string	"aes-256 ige"
	.section	.data.rel.local
	.align 32
	.type	names, @object
	.size	names, 248
names:
	.quad	.LC268
	.quad	.LC234
	.quad	.LC235
	.quad	.LC236
	.quad	.LC269
	.quad	.LC238
	.quad	.LC243
	.quad	.LC245
	.quad	.LC270
	.quad	.LC271
	.quad	.LC272
	.quad	.LC273
	.quad	.LC274
	.quad	.LC275
	.quad	.LC276
	.quad	.LC277
	.quad	.LC278
	.quad	.LC279
	.quad	.LC280
	.quad	.LC281
	.quad	.LC282
	.quad	.LC283
	.quad	.LC102
	.quad	.LC239
	.quad	.LC240
	.quad	.LC241
	.quad	.LC284
	.quad	.LC285
	.quad	.LC286
	.quad	.LC266
	.quad	.LC267
	.globl	speed_options
	.section	.rodata.str1.8
	.align 8
.LC287:
	.string	"Usage: %s [options] ciphers...\n"
	.section	.rodata.str1.1
.LC288:
	.string	"Valid options are:\n"
.LC289:
	.string	"help"
.LC290:
	.string	"Display this summary"
	.section	.rodata.str1.8
	.align 8
.LC291:
	.string	"Use EVP-named cipher or digest"
	.section	.rodata.str1.1
.LC292:
	.string	"decrypt"
	.section	.rodata.str1.8
	.align 8
.LC293:
	.string	"Time decryption instead of encryption (only EVP)"
	.section	.rodata.str1.1
.LC294:
	.string	"aead"
	.section	.rodata.str1.8
	.align 8
.LC295:
	.string	"Benchmark EVP-named AEAD cipher in TLS-like sequence"
	.section	.rodata.str1.1
.LC296:
	.string	"mb"
	.section	.rodata.str1.8
	.align 8
.LC297:
	.string	"Enable (tls1>=1) multi-block mode on EVP-named cipher"
	.section	.rodata.str1.1
.LC298:
	.string	"mr"
	.section	.rodata.str1.8
	.align 8
.LC299:
	.string	"Produce machine readable output"
	.section	.rodata.str1.1
.LC300:
	.string	"multi"
.LC301:
	.string	"Run benchmarks in parallel"
.LC302:
	.string	"async_jobs"
	.section	.rodata.str1.8
	.align 8
.LC303:
	.string	"Enable async mode and start specified number of jobs"
	.align 8
.LC304:
	.string	"Load the file(s) into the random number generator"
	.section	.rodata.str1.1
.LC305:
	.string	"writerand"
	.section	.rodata.str1.8
	.align 8
.LC306:
	.string	"Write random data to the specified file"
	.section	.rodata.str1.1
.LC307:
	.string	"engine"
	.section	.rodata.str1.8
	.align 8
.LC308:
	.string	"Use engine, possibly a hardware device"
	.section	.rodata.str1.1
.LC309:
	.string	"elapsed"
	.section	.rodata.str1.8
	.align 8
.LC310:
	.string	"Use wall-clock time instead of CPU user time as divisor"
	.section	.rodata.str1.1
.LC311:
	.string	"primes"
	.section	.rodata.str1.8
	.align 8
.LC312:
	.string	"Specify number of primes (for RSA only)"
	.section	.rodata.str1.1
.LC313:
	.string	"seconds"
	.section	.rodata.str1.8
	.align 8
.LC314:
	.string	"Run benchmarks for specified amount of seconds"
	.section	.rodata.str1.1
.LC315:
	.string	"bytes"
	.section	.rodata.str1.8
	.align 8
.LC316:
	.string	"Run [non-PKI] benchmarks on custom-sized buffer"
	.section	.rodata.str1.1
.LC317:
	.string	"misalign"
	.section	.rodata.str1.8
	.align 8
.LC318:
	.string	"Use specified offset to mis-align buffers"
	.section	.data.rel.ro,"aw"
	.align 32
	.type	speed_options, @object
	.size	speed_options, 456
speed_options:
	.quad	OPT_HELP_STR
	.long	1
	.long	45
	.quad	.LC287
	.quad	OPT_HELP_STR
	.long	1
	.long	45
	.quad	.LC288
	.quad	.LC289
	.long	1
	.long	45
	.quad	.LC290
	.quad	.LC102
	.long	3
	.long	115
	.quad	.LC291
	.quad	.LC292
	.long	4
	.long	45
	.quad	.LC293
	.quad	.LC294
	.long	1507
	.long	45
	.quad	.LC295
	.quad	.LC296
	.long	8
	.long	45
	.quad	.LC297
	.quad	.LC298
	.long	7
	.long	45
	.quad	.LC299
	.quad	.LC300
	.long	6
	.long	112
	.quad	.LC301
	.quad	.LC302
	.long	10
	.long	112
	.quad	.LC303
	.quad	.LC267
	.long	1501
	.long	115
	.quad	.LC304
	.quad	.LC305
	.long	1502
	.long	62
	.quad	.LC306
	.quad	.LC307
	.long	5
	.long	115
	.quad	.LC308
	.quad	.LC309
	.long	2
	.long	45
	.quad	.LC310
	.quad	.LC311
	.long	1504
	.long	112
	.quad	.LC312
	.quad	.LC313
	.long	1505
	.long	112
	.quad	.LC314
	.quad	.LC315
	.long	1506
	.long	112
	.quad	.LC316
	.quad	.LC317
	.long	9
	.long	112
	.quad	.LC318
	.quad	0
	.zero	16
	.section	.rodata
	.align 16
	.type	aead_lengths_list, @object
	.size	aead_lengths_list, 24
aead_lengths_list:
	.long	2
	.long	31
	.long	136
	.long	1024
	.long	8192
	.long	16384
	.section	.data.rel.local
	.align 8
	.type	lengths, @object
	.size	lengths, 8
lengths:
	.quad	lengths_list
	.section	.rodata
	.align 16
	.type	lengths_list, @object
	.size	lengths_list, 24
lengths_list:
	.long	16
	.long	64
	.long	256
	.long	1024
	.long	8192
	.long	16384
	.data
	.align 4
	.type	usertime, @object
	.size	usertime, 4
usertime:
	.long	1
	.local	mr
	.comm	mr,4,4
	.local	run
	.comm	run,4,4
	.align 32
	.type	dsa2048_g, @object
	.size	dsa2048_g, 256
dsa2048_g:
	.string	",x\026Y4c\364\363\222\374\265\245O\023\336/\034\244<\256\2558?~\220\277\226\246\256%\220r\365\216\200\f9\034\331\354\272\220[:\350Xl\2360B7\0021\202\274j\337j\t)\343\300F\321\313\205\354\f0^\352\3109\216\"\237\"\020\3224ah7=.J[\232\365\301H\306\366\334c\032\323\226d\2724\311\321\240\321\256l/H\027\223\024C\355\360!0\031\303\033_\336\243\360px\030\341\250\344\356."
	.string	"\245\344\263\027\310\f}nB\334\267F"
	.ascii	"6M\324F\252=<F\211@\277\035\204w\nu\363\207\035\bL\246\321\251"
	.ascii	"\034\036\022\036\341\3070(v\245\177l\205\226+o\333\200f&\256"
	.ascii	"\365\223\307\216\256\232\355\344\312\004\352;r\357\334\207\355"
	.ascii	"\r\245LJ\335q\"dYiN\216\277C\334\253\216f\273\001\266\364\347"
	.ascii	"\375\322\255\2376\301\240)\231\321\226pY\006x5\275eUR\236\370"
	.ascii	"\262\3458"
	.align 16
	.type	dsa2048_q, @object
	.size	dsa2048_q, 20
dsa2048_q:
	.ascii	"\227\3473M\323\224>\013\333bt\306\241\b\335\031\243u\027\033"
	.align 32
	.type	dsa2048_p, @object
	.size	dsa2048_p, 256
dsa2048_p:
	.string	"\240%\372\255\364\216\271\345\231\363]oO\2034\342~\317o\2770\257o\201\353\370\304\023\331\240]\213\\\216\334\302\035\013A2\260\037\376\357\f\302\242~h\\(!\351\365\261X\022cL\031N\377\002K\222\355\322\007\021M\214X\026\\U\216\255\243g}\271\206n\013\346To@\256\016gL\371\022[<\bz\367\374g\206i\347\n\224@\277\213v\376&\321\362\241\032\204\241CV(\274\232_\327;i\211\2126,Q\337\022w/W{\240\252\335\177\241b;@{h\032\217\r8\273!]\030\374\017F\367\243\260\035#\303\322\307rQ\030\337F\225y\331\275\265\031\002,\207\334\347W\202~\361\213\006="
	.ascii	"\245{k&'\221\017jw\344\325\004\344\022,B\377\322\210\273\323"
	.ascii	"\222\240\371\310Qd\024\\\330\371lG\202\264\034\177\t\270\360"
	.ascii	"%\203\035??\005\263!\n]\247\330T\303e}\303\260\035\277\256\370"
	.ascii	"h\317\233"
	.align 32
	.type	dsa2048_pub, @object
	.size	dsa2048_pub, 256
dsa2048_pub:
	.string	"\027\217\250\021\204\222\354\203G\307j\260\222\257Z 7\243dy\322\320=\315\340a\210\210!\314t]\316LQG\360\305\\L\202z\257r\255\271\340S\362x\267\360\265H\177\212:\030\321\237\213}\245G\267\225\253\230\370{tPV\216W\360\356\365\267\272\253\205\206\371+\357AV\240\244\237\2678"
	.string	"F\n\246\361\374\037\330N\205D\222C!]n\314\302\313&1\r!\304\275\215$\274\331\030\031\327\334\361\347\223PH\003,\256.\347I\210_\223W'\2316\264 \253\374\247+\362\331\230\327\3244\235\226PX\232\352T\363\356\365c\024\356\205\203tv\341R\225\303\367\353\004\004{\247(\033\314\352JN\204\332\330\234y\330\233f\211/\317\254\327y\371\251\330E\023x\271"
	.ascii	"\024\311~\"Q\206g\260\237&\021#\3108\327p\035\025\216MO\225\227"
	.ascii	"@\241\302~\001\030r\364\020\346\215R\026\177\362\311\3703\213"
	.ascii	"3\267\316"
	.align 16
	.type	dsa2048_priv, @object
	.size	dsa2048_priv, 20
dsa2048_priv:
	.ascii	"2g\222\366\304\342\342\350\240\213kE\f\212v\260\356\317\221\247"
	.align 32
	.type	dsa1024_g, @object
	.size	dsa1024_g, 128
dsa1024_g:
	.ascii	"M\337L\003\246\221\212\365\031oPF%\231\345ho0\343i\341\345\263"
	.ascii	"]\230\273(\206H\374\336\231\004?_\210\f\234s$\r ]\271*\232?\030"
	.ascii	"\226'\344b\207\301{tbS\374a'\250z\221\t\235\266\361M\234T\017"
	.ascii	"X\006\356It\007\316U~#\316\026\366\312\334Za\001~\311q\265M\366"
	.ascii	"\3344)\207h\366^ \223\263\333\365\344\tlA\027\225\222\353\001"
	.ascii	"\265s\245j~\3302\355\016\002\270"
	.align 16
	.type	dsa1024_q, @object
	.size	dsa1024_q, 20
dsa1024_q:
	.ascii	"\367\0071\355\372l\006\003\325\205\212\034\254\234e\347Pfeo"
	.align 32
	.type	dsa1024_p, @object
	.size	dsa1024_p, 128
dsa1024_p:
	.ascii	"\247?n\205\277Aj)}\360\237G\0310\220\232\t\035\332j3\036\305"
	.ascii	"=\206\226\263\025\340S.\217\340Y\202s\220>u1\231GzR\373\205\344"
	.ascii	"\331\246{8\233h\212\204\233\207\306\036\265~\206KS[Y\317qe\031"
	.ascii	"\210n\316f\256k\2106\373\354(\334\302\327\245\273\345,9&K\332"
	.ascii	"\232p\030\2257\225\020V#\366\025\355\272\004^\3369O\375\267C"
	.ascii	"\037\265\244eo\315\200\021\344p\225[P\315I"
	.align 32
	.type	dsa1024_pub, @object
	.size	dsa1024_pub, 128
dsa1024_pub:
	.ascii	"<N\234*\177\026\301%\353\254xc\220\024\214\213\364hC<-\356eP"
	.ascii	"}\234\217\214\212Q\326\021+\231\257\036\220\227\265\323\246 "
	.ascii	"%\326\376C\002\325\221}\247\214\333\311\205\2436H\367h\252`\261"
	.ascii	"\367\005h:\243?\323\031\202\330\202zw\373\357\364\025\n\353\006"
	.ascii	"\004\177S\007\f\274\313-\203\333>\321(\245\2411\340g\372P\336"
	.ascii	"\233\007\203~,\013\303\023Pa\345\255\2756\270\227N@}\350\203"
	.ascii	"\r\274K"
	.align 16
	.type	dsa1024_priv, @object
	.size	dsa1024_priv, 20
dsa1024_priv:
	.ascii	"}!\332\273b\025G6\007g\022\350\214\252\034\3158\022a\030"
	.align 32
	.type	dsa512_g, @object
	.size	dsa512_g, 64
dsa512_g:
	.ascii	"\203>\210\345\305\211s\316;l\001I\277\263\307\237\n\352D\221"
	.ascii	"\3450\252\331\276[_\267\020\327\211\267\216t\373\317)\036\353"
	.ascii	"\250,TQ\270\020\336\240\316/\314$k\220w\336\242h\246R\022\242"
	.ascii	"\003\235 "
	.align 16
	.type	dsa512_q, @object
	.size	dsa512_q, 20
dsa512_q:
	.ascii	"\373S\357P\264@\2221V\206Sz\350\213\"\232I\373q\217"
	.align 32
	.type	dsa512_p, @object
	.size	dsa512_p, 64
dsa512_p:
	.ascii	"\235\033i\216&\333\362+\021p\031\206\366\031\310\370\031\362"
	.ascii	"\030S\224F\006\320bP3K\002<R0\003\213;\371_\321$\006O{L\272\252"
	.ascii	"@\233\375\226\34473\273-Z\327Z\021@f\242v}1"
	.align 32
	.type	dsa512_pub, @object
	.size	dsa512_pub, 65
dsa512_pub:
	.string	""
	.ascii	"\225\247\r\354\223h\272_\367_\007\362;\255k\001\334\276\354\336"
	.ascii	"\004z:'\263\354I\375\bC=~\250,^{\273\374\364n\353l\260n\370\002"
	.ascii	"\022\2148]\203V}\356S\005>$\204\276\272\nk\310"
	.align 16
	.type	dsa512_priv, @object
	.size	dsa512_priv, 20
dsa512_priv:
	.ascii	"e\345\3078`$\265\211\324\234\353L\234\035z\"\275\321\302\322"
	.align 32
	.type	test15360, @object
	.size	test15360, 8684
test15360:
	.string	"0\202!\350\002\001"
	.string	"\002\202\007\201"
	.string	"\255?\252\334\214\205\313`\322\3650\241\017&\354\337\374\2219\275>\217\231d\036Q\322'^v\315\2063\007\371\275;\006\303<\205\313~\221\024\260\013w\"0q\270\273t035V4G\020\217\210\342o\334;\351X\235\f\334\217pAz\022\322\2325\276\nW\023\f\351\277wT"
	.string	"t\267\032>\247\351\266\347O\036\244\300|Lf\305\316\255\226\033\342\032\361=\213P\317\342\025!m\203\225"
	.string	"\356\227\304\256\3118bl\262\347\177\025\n\253\206\271\331\212\370\353\210]\334\f\036\305\346\241{\277\361\002\343\255\370\355\027\237\203\0211;\255\264\371\215\035V\233\254hU\nt \356W\347\034m\005\241N\245\021\231\264\206\333X\347\366\266O\222XW\233t\004\345\321\035|K\270\037]\016\223\356D\030\266X\016\241\013\216.\231Lr\221\372\372\342\"\005]+-\330`\325\033\bV+\265!\333\032\346\2509\242\364X\313\322\371\316\300\036\033\371\2477\312\243wn\261\2573\265m_3.\0324\333B\276_\371\t\267\237\324\t\373\207\023<\342'\270\363\035~\222\335\207\206Ui\233U\315\357zq]\201:\331\367\177\336\340\222\331x\017\035C\261\036)\301I\266^\205\203\331\004\375y\330G\003.\205\031\375c\347\244\213\300\224\016\267T\227\326D]c\022\377\335\336,"
	.string	"\016\311\312~\242e%\260\035\251 O\335\352:\265\350\017\363\262\267"
	.string	"J\350\244\203I\275x\337\254,7\201\263\363\267\023\223>\262yU\362\330\234\367\362\361\325l\234\377\354\364\352\b<e5\267\t\003m\231\035[s\006a\264\360\305\333>\340\035\250[z[[\234\021u\203\035\364s'\363y\362\202\326(EX#l)\323PQ\0338\357\211\220\204\242L5{0^\275\032\325\337\315\315t?.\001\3523\007t\373\206u \016O\277e\324\025\031o\2157\315\266oP\235^\004\201}\354\326\273@\033\340\365\325\206&\305A\204\016>s\267\244\276*\376\327\344M\\-j\004\346\335(\240uL\340#,\255\354\252r\375\003\300e\372\304<%\020\256?\t\226N\377\376\307\344\236\354\265n\354\363z\203z\213\273\221\215\253<M\1774w\276\f\207\362\303\326\313\314\372\036\257!$\351\252\211a\fz\034}"
	.string	"\207i0\240\264;\226\034"
	.string	"\024\007\270?Yb:?\373h\270\201}J\235\034\242\007\243\261B{\372\233\274\2240~\352\347@~\324\0173;W\332\213md\325\344\221\203\360=\256\213\221\360\315\261\240\340\r\341\273\"x\037:\345S(\3605\256q\346\375c\262\234?\335\225{\304\351/\331\223:\020B\034\220\253\373\323\002\351Y\274S~\363\341R\025\246X\236\301\246\016.5\007:\303\037\252X\347\3063j9K!\025=\222N^\371\001\326\017(a\025\337\355ou\304\217\313\026U\t\307$\262\fI%\215^\361\016\340\342\304\314\037N`\\^\306\177h\177\333\032\001g\007\261V\223\362&\201\3003\270H\371,\\\030)\355\340l\240\254\322\220KR\207\273\265\005\330V\305\270\217?IR\232\242\320@\200[\026\025\274t\216"
	.string	"\020\257\373m\272\313\274\346\023u\316'\256\205Wl\300\212\204o4\026\3245\322\314U"
	.string	"\301\330(,\234\204x\277\360;\r\237\201\324\357\231wS\322\216CR\3602~\272\277\266\016\235\233"
	.string	"\320PUgZ,\213\233)\373AtL\267\330\230\242\373s\007\226\357\315G\023\035\342\261\254\363\317G\230{o\3662DAx\t\216d\f\277\342\017\214D/NU\340\306\375\005t\030\032\271\372\313\323\372iPc\316+\357\222\017\021\324\233Sl\355\305\013|\275\241]\337\253\317\252\203^\250\305\376\221+#\0379=qt\277\242\361\332/)\002\233\352H,\257\347\251\365h\253\217\030\271{(\360\222\373\007\327\275C\315\177\374\271_$\370H.\276B\207\2008x\236\214Rm\372.F5zY\210\271>\313y\264\212\236\325\3200\214\262\f\235\215-d\013\366\353\361\336\352t\374\274\001\030HN5\002\203\001\262P\240D\0310"
	.string	"\022J\240mk\213\361\316\332.\0265R&\371\276\2617\374\n\213o\006\021{\367\250@\275\215\224\244\242\340\266\337b\300o\263]\204\271\252/\301;\313 \306hi\025t\274\333C\234J\374r\301\365\207\200\350l\325\301.4^\226v\b>E\344\240Jz\301g8\3621\037{\017T\275\r\037\236\216\231\213X\331\224\207\252\213\202]^\350P\364\362\307\351\205k\322\357\023\301\355W*\305\326]\244;)\272\253\033\252!A\351\334G\210\357\f\374\262\334\367\333UMp\307\342\212\212\341\336\317\345\312#6)\345\374Tf\332\351\253X \262\216\262}]\270\307lHS+G\340\022"
	.ascii	"\016\376\245\2234\371>\246?V\252Ce\273Zp>b\254?[\220\002P]\005"
	.ascii	"\250\325g\032b\354\324\336)\004\254m\025]\240\354\362W\023\016"
	.ascii	"\027\226\f2j\305\340\250\377\205\244\243\343"
	.string	"\0165]\321(\204\252\304\204\315%c\205\202>\0220\027WE\270\2644\001:\242wa\310=\037\305\016J\273\366\240]yK\310\363\234\207\005/\352%(\221iw|\272\352Ju.+\027\203P2CO\315\361w\261\"\n\213iX\t5\007maJ\215\030en\233b\007\320j\2229\005\200\024\372\034\223\204\f\265\214A\221NH\360\362\272\035s/\036\241U\303\002\214\261\3627\246\232k\315E.\b\220&c\221\377\"^\315\256\233\031\036\020bN\037-\201iOA\345\224\377~\314\0256\036)Y7\347d@\027\0322\272\001&0\200`\007\206n\324\263\342D\0263\362L\204\016\261J\307\222\246\243B6\005>t\250\261\305cY\r\0366E+6^\312\253\227I\323\253\256c\n\321\003W\210\244\244<\332\025I\032]\346^\271\202#\300\203\226\3768\013\200\016\336\"\353]\344V2\276\340\300nic'N"
	.ascii	"X\200p\331\314N\256l^jC\201\375E\262\244l\360\234f\\}\\xU3K<"
	.ascii	";\035\030Xyj\002\354\316Si\300\027\355W\257q[B\033I\330\350\226"
	.ascii	"\200\266H\033|\370t\034\261\304\020\267\364\227"
	.string	"~k\217T\2727\2715\236{\027\026\233\2119\256O.\030e\264v \232X\342Wn\034?\216\232\273\330\374L\326-\301\246F\254\023\036\247\367\035(:\364\326H\373\345\263\204\224G\222\256\232X\305\254#\033\265\315\226\322^\262A\374\232\256\031\361{KS\033\372\245\fIm\377\364Q\210\031\004\331\205\216\342:b1\\n\350M\004\035\330\302{Q\347Y\274\205\\\304\314\255\313\223i\030\344q\236c3\231\266;#\021\027z=o\271k\361\362\247\003\375\360\315[\265\332\232\331\225\002v\3308\323\275\240J\232\253p\336\306\371\245\031\234\304\371\007M\352\025\302\221MT\251,\312\337\252\321\304\300\030w(*,\303|&\275\330\rQ\241M\255vv\252\251E\202Ov\373\032\323q<U\242\\\340\326\3325\276%#&Q\306\264\363>,T\t\307o\245\b\201\272u\332\313M\005\335\312\223H0\350J\037\375\002\003\001"
	.ascii	"\001\002\202\007\200%/\274I\370\263\2432\3265 \312\001I\226\240"
	.ascii	"\201B\336\304\333\017\321\231\346\324#*\246!\023\376Q'\316\030"
	.ascii	"*\372I\237\315\f\037\317\236D'A\334\t\317\357\031\365W\1776\\"
	.ascii	"\231"
	.string	"~\003t\373\251\266\336\353\321+_\022j\2513,*\272\255\217\302'Wj\327@\367OL\232\260:].\371\361\352\275\202\252\275\346\031\026\325\003^C\375\210q\325\267x\276\200\017\311\177:\217\341D\324\017\316&\257e\340\365\004SV\227O\364\301D\215\367\210UG\026\257?\216B\337\274\024\303\346\237\riT[|I\317\277BO\307d\212\345\204\207 \233\375p%8\323\264\227x\361O?\017\273\234\243\027\325NK\254\202\232s\267\305\354\020z{\333w,\261\363\217\303\2451\0212U5\265w\322b\031F\222\224\273a\0170\224\212\366\3670\340\242\214\033\377\214)D\264\267\266_MR\306\007\341(\214\256\210\212\"\275\3276\344\217\321\353eT\031_\272\373\374\221\241\244\270\244-\205 \304\345\247N\333\244\305\314/7A)G\025\377\004\200\b7\316\305\343Z?\203\273\003\236\376\354\344\021A\022\023\362"
	.ascii	"\345\032\002I\353\333W\344\316\240?\375<s+\222Dy\236\022O\372"
	.ascii	"\344Sb\362\260\342\212\360\223\250\035\356\215XzL)\221)\301\244"
	.ascii	"\325\3467\033u[\266kv.\313\275\251\276L.!\2468\336f/Q\352L\272"
	.ascii	"?J\376z\025\263r&\272\317\236\033\003\246\252eh\323\214\025\027"
	.ascii	"\351\021\030<\266\370\002T\230I\3725<\315\254\310+\032c\223\003"
	.ascii	"\005\241A\276\022\312\025Grcw&\320\347\217\rn\234\254\007\276"
	.ascii	"\003\"\3209c\215\233\306 \201\265g\025\366\260\343\271>\267?"
	.ascii	"\217F\311t\020\036S\361\3240Mnr\264s\034\266y\202`.*}\202\225"
	.ascii	"\265|MD\313\330\212\027\350P)\330:\353)\301\203\017\331\257\314"
	.ascii	"\372\352:G]3\037\3503[\210\216\333\325\036\257J_\300\372\360"
	.ascii	"\265\243[\3328\2678^\316\201D\367fbd\035\004\360\212O\242\200"
	.ascii	"v\203#\211ak\303\267\356\265\0063\255c\004x\311\3362\336\317"
	.ascii	"\030\271\260;\356\nX\352\255\274\036w\240\223\367\256\236\266"
	.ascii	"1Y\216\261\003\217\273\244%\f.\327\342b\\\361h\351v\327#\024"
	.ascii	"E\257\313\tP\005?\240\371\303\236\211\005\250;TU2t\221F\301,"
	.ascii	"\226~`\255\372\273\315\t{9\020\202\212\300Z\r\253\263qE\2559"
	.ascii	"\216\354M\221\215\332\215\372\260\255D<\311!V\"\374\323\272\267"
	.ascii	"<\343\215\332Y4B\335\004[\216+\307\224\325B\340Jo5Z'\202\330"
	.ascii	"\202@\356\017\246\357\344p\3430\267-\324\273'\262\277\255IE\274"
	.ascii	"\353\276\267\330\343\261\363\353A \233!T\303\250\257\237 \\\025"
	.ascii	"\216%\274\274i\221\376\332\255\3457}\260Q\024\256\2175\025\n"
	.ascii	"\324I\247\331 p\244\362\364$fR\321\245\"\352)\331\262\202\215"
	.ascii	"6fun\325\214T\b!\362\356x\307\037\234c]\210V\321\240\2003`U#"
	.ascii	"r\326\260\032P\336%p\265wB\370\031\030\025\217\375\fjF\037\277"
	.ascii	"\347`\221\347\273%cf\377\021\227\273\375:\027\224w\264\305!\272"
	.ascii	"0\224\335\345\353\035\001\272\371\2600\333\021\223\267\372y\350"
	.ascii	"^\2639\364Qh1\316\351\016\223\336\377\354'\275\246\032L\340\222"
	.ascii	"\\\324\007\322\241\335\022\203\322\232y\263<\373\007\343\030"
	.ascii	"\032\243$\200\264\314\364\306\245l%\327\231\0320\360\251\374"
	.ascii	".\203D\254dv4\260\246o Z\024\362\007\247oM\253\365\374\235\326"
	.ascii	">\202H1%G\311\016\035\333\230\221V\365\376f\215H\360Ll,\226T"
	.ascii	"C\354v\362\341vh\310\341\336\r\216o\374\025\325\223\222\376\312"
	.ascii	"\2330a\003\013\312\231/\323\025\351f\201\275V\027\024J.\3614"
	.ascii	"\204U\235\300+\247J\356\361|g\307\363\b\036mk[\314\201\221\\"
	.ascii	"\224\032\200\332:\3166\005\260z\350\320\264W\234\371\352\363"
	.ascii	"&\035\313\370\335e\257\367\315\367\241=\374\232;\b\271\372<\026"
	.ascii	"IJ\361\272M1\335^O=f\"\033\b\221}\306\257\025\007<\241\367\007"
	.ascii	"\375>\220\273oz\351\341/\271\356\221\216\030\314\215\035\"\240"
	.ascii	"\240(%\374\324\224\323\252\317\316\320\205\202o \237U\016\345"
	.ascii	"r\r\027>4\307,\n\024E'\342\307/\206\241U>x\003\351x.\323\231"
	.ascii	"\356\240\024\370\343l\353?\232\363\025\316\325v\366:\2060v\371"
	.ascii	"\2100\365JPX\200\351\331\324\2714B\246N\234\032\007\026\236\356"
	.ascii	"\344\210\004\216\250\347\315\350G\036TE\322e\330\356K\275\320"
	.ascii	"\205\252\373\006S\221~\340Y Wj\356\330\237w\177\327@c\273!uv"
	.ascii	"\021'\317\005\273A0\230\277\334_\306\244\0360\241S\3246\177."
	.ascii	"\206\327\331\225)\325F\030`'\344o\313\364\342\376\377>\377\025"
	.ascii	"\306\3621\371*\310\005N|.\222\310AO\236#!Mt\370\303D9\302iK."
	.ascii	"v^D\022e1\230\276\n\020\021\022,g=\205.\323\227T\036\266\255"
	.ascii	"\331E\021S\004|?\364\311\254\202\033\204\364 k\361\365r\004$"
	.ascii	"\301\323BCR\235-\323\211\216\330(\271\242\264\355\274v\207Ug"
	.ascii	"9\331\267 j\354\354\270\024Q\221\271\226\017z:\022\336\024;\203"
	.ascii	"\317A[]\3773h\333Sd\223\261\303\212F\250D\234\024\022l\222o\256"
	.ascii	"\303E\262\241g\201<\"G\375\244zy\250\n\373z\221n\351S\354\230"
	.ascii	"\202W\255\0058U\301\316:\004M\022r7J6T?g\212\356\331\363\200"
	.ascii	"\325\327\270\374nO`+Z\244\305\005\333\345\t\343\353\242Q30\226"
	.ascii	"F\001&\2178\311\2272-\264Y\025\0258ff\376\313\356\301\366N\267"
	.ascii	"\337{c\346?\340\034\227\355\206\363\322\255B) (\246YX}\217\\"
	.ascii	"C\007\321~\203\272\234\033\376\027\236\310\tc\232-a3QF\001\250"
	.ascii	"\351C\036N\376a\032(\021epC\237\374!\035v{@\b\030\323\350\302"
	.ascii	"\343\214\347'\302\354\260\b>k\217wm\236\246\253\316\232\370\217"
	.ascii	"w\263\364\350\213\347\331\241\225@k\312!\230\377\334\334\226"
	.ascii	"\303\b\201r\232\335\342\317\225\231\246\243^\236%`\243\3039\367"
	.ascii	"Tl\362u\2518\0228MB\350\354\023%\240\370\004\270\366f\013V\341"
	.string	"\373&\003\346\245\361M\177\245\235Xq\330\307j\276\334\220\211\2616\264\266\264\273\257nC\020\246\352\356\022\313\b,Nf\360\037\364\277\323\353cH\320\276\212\355$\333\017#\035.0\227\017\330\306;\004/3x n\2613\003'\254\n7\0251\357MC\314\240I\200\343\214\300\363\367-7\035\323\220_\2551\265\225\027iK\354\204\235+\215\335\233X\004\272(\016(\301Tl\260%\fO\230G\367\223\302\256/m)\234=\343\265\343(C\024\346\222Ly\220YuwVC\332\254\251B\327\312\225s&T\037:\2127d\327\317\3411\367@Y\375\377\352r\375\304\336\343M\212\365\200\300a!\275\275\216B\325L\344\364x1\312\361\354|{\205j\005T\2768T/\037\332\237\230\342y\327B\312\272\205!\342\313+\256JN5\373\317=\305\256'0\251E\346;C>5\343\362\rS2+\366\346\307\325\002\202\003\301"
	.ascii	"\324\004\233\357]X\260\243\252\322\253Se\231\003IHM\365\337]"
	.ascii	"\026\024\021`E\033\377J`+7c\366\247\212\250\377\b\227\b\374\273"
	.ascii	"\263 \243\315\331X\333\026\033\210\002\036\017C\233\026~\276"
	.ascii	"\261\234\023\020\334\241V\377\243\377^i0\356~v_\204\224\353\217"
	.ascii	"X\370\317\273\231n\360\3302\366\316Ho|\310\217\323\206\"I\237"
	.ascii	"\336\021\005\244\334\222\373\017\372\tM\027\032\342vg@\251[\033"
	.ascii	"TfH\367\303Y\324\317U\320\177"
	.string	";\260\242\330\354\267\210\347\2600rBe\342\221\247\233\366\007ERQ\252\27625\344\210#\347\313<\034\373\013\226\325\263\222\206y[G\223\326\275\307!\027\320\311\307i\204\200\230\257,c\321\357n\312\20402\203-I\273\037*\376@|\003\324E\334\376\224\371\3446G\372~.\223\003\370\025\371\316\303[v\020\354\211\214\316%\245w\232\305\036\335\007\033[\254o\333\224\205\337\002\"\321\251\001\216c\241\356\224\234\333\264\032C\341\037N/hP\f/[\305\033\341\215K\340c\215z0\276\267.\002\306\002\254\250\270e\306(\356\344\354\231\241\232\375\037\265\205z\224\026\342\347t\006T\033\320\257XNP~\326\3441\322\f\327\235\342"
	.string	"0\276&0H\231\230XTZ\304\nl\241\006\3518\346y9"
	.string	"\236\266\343\367\001\317/\202^\303!\033y\223\265\3449\2352\235r\244\250\311\220\316\257\300"
	.ascii	"\255 \207&\307\323_.\360^\370\213\205\243\306f\330/\206\376}"
	.ascii	"\215\"\245mh>\207n\367\361\360\007\304\343\361\204\304\223B\006"
	.ascii	" \200d\263R\\\245\317\356\376\244\tA\276\252xRv?\367\350\241"
	.ascii	"k\n\274\"\276\337r{\352\220C\356\302\013&\334\002&\247P\004z"
	.ascii	"\006\221\256\223\325\322\311\241\341\374\271\214\224\312\250"
	.ascii	"\034,W\227>P\355\223Ez,Y{4\217\315\326\027\223\330\336\350\260"
	.ascii	"\236'\025\305\273\245\273\3020\233\307'\002\030\330\333\244\204"
	.ascii	"7d\367\367\361\310\206Ld\227\b\351N\016\266\222\351L{\177\341"
	.ascii	"\314\240q\2474HF\2737\316\260M9\250\016\253\366/|\210\256\317"
	.ascii	"\220\306\001\323[7\351\261(B\024\277Y5\004\253Fn\250)\342zw\016"
	.ascii	"\007g\344+\003\322\0026\026\327\201]8\234h\234\365\236I}\231"
	.ascii	"\375\315\035\322\337<6\031\205\252\2610z!\261\203\026\317\321"
	.ascii	"u\245\235\327\301`\250\333\036\271>\234\022B\350GI\030\237\\"
	.ascii	"\022\321i\325}\250<\3325\212lc\270b\212a\372\362a\021\036\266"
	.ascii	"\363\\b\235\247b\f\207\223\342#l=\251,K\325\177\376r'6\006\313"
	.ascii	"e8\357\023Wj\311\306OQ\320\220"
	.string	"\006\240#e\225\316\026\217\215\262\371\177<,0Z8\361byK\345\327\n?\203_F&\227\267\b\214[\270\002(\362M\337\223\227\305\224K\016B\3035\221kiav\177\224\317\013\2013\377\363\f\307\001\224\224\251\355\315K\310\313\221\371zG\315y<\246\336R\322G\\\020b\273\3452\336\203\317\250R\263\347\371\354\0274\2773]\262NV\367)\331\\\033\203\001\273\271+\225R\b\253\244Q\003\241\373jP\315\250\235\225o~\261\200\036\235\201\001&Ax6<\212D\364\230\210\034]\006\323\322\262X}\241E\033\277\214\366j\372\375\b)>\221W\361= \355In\234F\325\b\215\233\370\357\243:\230\313\264\313[0% \314\004\241\353\353\356\0336\205\301\223\026Z1\337\326\016s\236cn\226\220T\322\302Si\223\325T\312\330\204\367\217\232\321\200\rW\250&\276Ed\325+\273E\265\b\2717W\002\202\003\301"
	.ascii	"\3210.\267\233\347]\023t\037R\362\002\030\351\007\207\236\355"
	.ascii	"\336\203\222\317sa!\304b0l\2426\275\342\305\031\366\337Q{\312"
	.ascii	"\324\344Q\203I'\335\275\260\020y9\335\016=e\255m\243\225R\205"
	.ascii	"\333\030\224`\252\300\310\213\333\376\371\360\206\3713\212\327"
	.ascii	"\276\215C\203M\344\027+FTD\033\276RdG\002lJd\264?!/\273\343r"
	.ascii	"|&\024\337\200P\324\224\351\306}q\330\257\373t63\276Xc\255\313"
	.ascii	"\337\300s\236\031\260e\341\321\020D\361\360\b\243\t%\353\325"
	.ascii	"\313\335\230\335\274\t,\357\301\215C\025A\302\241\2047"
	.string	"pZ\325\365\262j\037\273\3140\271\331\3076!\363i>\2218M\245\304\367\204\2204\016G~&\362\230%&\332\360NU\352M\233\212J\341\037\240\007\220\236Yd\256\331\326~r\241\304\352}\275\037}+\331,\334\213\300\332R\f\321\320V\267\223\307&yq\320\r\256\252\247\344\301Y'h\227\232\377=6\007Uw\007\227i\363\231\221?c\375p\214\241\353\305!\243\376\231\226\0217\271\346\223\370\320\261\243Wz\250c\335\tV\260;\246Y\307\211T\026\351-x}\257N\n[b;\013\313$\211N\034=\341\275Z>\305\375\025=\b83^7L\343\343\351\304\035+\324X%X#\216\306\203\232\363\232x\351\247\312\327\335\211 n\002\352k7t\332\240\302Z+\200\034(\221\rPd\360\022\347\304~\335(;&\232\3649V\244rM\313g<h\262o\360\320\025\220\310\b\273\013\bk\212\336AW\274c\016"
	.ascii	"\215\370\335\223\316X{\250\271d&\006\347q#\017A\361\267\256Y"
	.ascii	".\320s\305\331\334\016\034\002Xi\263\025m\226+\333{;l82k\330"
	.ascii	"\b\262\275\247IC\353\220Bp\305\272\315JD\217\203\r\027QZ\225"
	.ascii	"\242W\232\026\031\221\273\220\\*\026\350&\020<\267\020\\\370"
	.ascii	"\305\025+pui\272{=\013W\2549\022.\326\331\023t\216\250\013\027"
	.ascii	"\341\003z\272\035\007\221\214*:\215\340*\224\324\0265d\213\222"
	.ascii	",/\244\030\376?\002\031\214\271\353\257\001\006\2507\177\342"
	.ascii	"D\020\316\353\215\320s\304\036=,\257w\262\357\345\225\213\337"
	.ascii	"\002\374\223\270\251'\210\035\035\202\237\266\344\022\005y\266"
	.ascii	"\034A\r\301SI\217=\311\255\204\313\013\210~\376sY!d\305PS\334"
	.ascii	"\230\306C\270\365\303\241\365\262\330\206\351\256\230\371;\231"
	.ascii	"\300\347\327J\355\254\211\204\260\216\323\253\354\003\002\022"
	.ascii	"KD\027M\230&\036Q\305"
	.string	"\273\315\334P\253\2037I\220\0364\255\201\"l\344\335\031\001\t%-\236R\220r\241h=\fI\231\031uZ\312\bi\241\322\210\214\352\317\234\274#\255?\271\374\2710\r\326\331e\f~\231h5&\007\321U\277\216\336\347\347\001\313\312\n9.\314\031\354w\363\253\262\346\016T\006\001Pw\323a6\005\220\344\330\304\035\365\307\372e\360Fj_\247\303\214o\004\177\317\227\271h\2221\t\002\237\"\311\370\346~\250\225[k\376\234Nc-\214\032L\213\024y\b\325\226v\321\264/\256]\221\210|\335\322\006\206\317\n\203o\332\312q|\347\3454\250\232S\215\245\252]\265\027\2014o\276\273\266X\"\220\200\366\234\034\260y\217\222[}\034q_\264\2076\276\201\215J\374(r\201\257_\275_\231\343\3117\260n\255p\226\372\343\231\367\b\024!!\267\032\252\350\007\266\375\243z-\223d\217\211,qIq\270E\312\340|"
	.ascii	"\215\275\270\034:\224\242\247m\n.\204\257\275\253\005\225d\213"
	.ascii	"\005\310\311N\352\265\226JG\335\362\313\002\202\003\300Y\263"
	.ascii	"\331\205\334\250\271\223\205\242\274y\374rP\301\240\245\333q"
	.ascii	"5\2411\274hN\325\031\236\0162:\255@\236\202<\036+4;\3112a\007"
	.ascii	"^F\251\276\276s\f\022\357Rh\202\342\013\022t\374\020\\\300\265"
	.ascii	"\230M\206\273\214@\025\241nFs.\326\231kP\253\004\032_\364\372"
	.ascii	"\313K\255\304^b\247H\324R\205\334*\205\233\356\b\245\252\252"
	.ascii	"\350D\360\355\211!\344\264\253<\rS~S\335\254G\332wy_xz\200\204"
	.ascii	"FP\252\333;\214k\332\260\254\n\323L\344n\207\321\262Z\325\230"
	.ascii	"\256\313~\302\031\334Sd\206L{\340c\"\2244\255\025\334\330\250"
	.ascii	"_\306X\366r4\335\373\205\212\331\243\373;\255]\360\032\013\250"
	.ascii	"\221\347}&'8\370\340I\033"
	.string	"V\305[\343\034{\243Sm\"\372\327c_\360\313\222I\001T\345w[\323\253\316\270:[\270\007@FQ\344Y\242EA\314\201l\343\246\263\2400Jg\020\355\300\212\315\374\245D\233Y\031JC\215\354"
	.ascii	"\330m\371\360-\331U\374\005\342\022HM\326}\354A\304\236\342\355"
	.ascii	"\204\024)\016[\201\013\260\207\212\3235\\\255\333\314\241<\313"
	.ascii	"\213#Ui\361\203\204\2016\256\325\363\230\266\262\265\241ym\200"
	.ascii	"\217.%qN\026\377\240|\244b\214D\205d\220|\254\0206\362\362\373"
	.ascii	" +\241'\320\314'\375\260\272>7\261\250\235<\202c\320\026mz\335"
	.ascii	".\352\345\207\326dr\333`S8\030f\035%\366\b\222\177h[y\007\336"
	.ascii	"\223\356\370\217\316(\317\261[CQ\337\365\254\350\234\225\024"
	.ascii	"\212g\341%\376\021\242@\370\335\317\365\027\224\266\210\020\242"
	.ascii	"\220X\357\257s\370|\233 0y\312?\251\"@\375\314\260]\r\227k\300"
	.ascii	"u53\305vEn\233x\347\264\004\263\272;\223\261\251\217\241$]\034"
	.ascii	"\016f\300\306\314\326\267\210\235\270E\343\252\311l\3757\334"
	.ascii	"\205\325I\375\357\353\371z?zO\206I\252\237\b\022\013\0215\\\325"
	.ascii	"\323\332\024P\003,$&\016)\030\314\035\n|\224\213\300\240?\352"
	.ascii	"\370\370\251\035e1o;\246\320\374&\260N:f\3472\020.\204G\255\251"
	.ascii	"\030\374\243\213t\204O\324%\223\017\333.\256\210\216(\370\017"
	.ascii	"\252`\324\276\255f\f\r\001\275\215\304\374H\357x\0244\356\263"
	.ascii	"\274\324\273\037|\022\\\233\353w>,n1Y\346x\305\350\244\335\361"
	.ascii	"\357]'E1\023\320!\241\023\316\254~\273\3732\353v1\304\272\337"
	.ascii	"\373Z\033\311\236t\240\236&\202\325n\035\303\016\321m\333C\263"
	.ascii	"\013\024\313\361\255b4I\270\323\b\312\223\361B\262K#y\223\336"
	.ascii	"\030X\363f\372\334\253\3123\"+\\\214\022\301{.Rr\247xJI\241S"
	.ascii	"\002v-.\370C<\350\372\267\3779\355t\236\021a3\336*U\346J\347"
	.ascii	"\227\246\262\303@ARf\317\277\370\216\b\352\226M\003\311\276<"
	.ascii	"N6\214oM\036\3151mS\352\236\360\2165\2277T\351\017\270#%i[\265"
	.ascii	"\377\303Z-\020j\300\270\356\r1[\344i@b\247\033\026"
	.string	"\372\326\270\272\310j\243)\335\233M\327\226\3571t\2547\020\2210\f\025?\t\266}\"\373\214o\303\223\243\230\246#\244U\340\236#\006\251x\351\263\210\311\267\203\005F\021:\n\271t[\240\265\006\226\206\266\364\235\r\206C\250@K\b\223|\255\260P\264\320\347\255\320T^\025\257\2554\022\206\263); \311\255\353\302e\363\\-\345\377\375\201y\365\021o\367\312\fv\360\324\002\235\267v9m2j\2700\244\001\314\020\357\261\016A\"\202[\"\3132\031.\243\n\316\005\335\350JX\222\341\002\202\003\300\"\017\225[\302\037\336\360\336\364\206\275\357\007}R\003\214&1\027\375\\\227\355\325\340\263\030-h\020?\304\337\321\005x\201=\005\336\272:g\205\016\337\265\026(\350\204:q* \027(\005\375\267M\"J\223FV'C\300:\026\377=a\314\313\316\254\250S:\r\364-\322s\362d\240\036`S\354\r\377\340"
	.ascii	"\020\373\244W\323\374\344\340\354D\013\034\0059\244\023\207)"
	.ascii	"\021\235\352\351d\251\034v:e\013\375\355wFO\315\013c\304\203"
	.ascii	"\013Vy\323g\001\021\002\331P\330#\364\266\002L\256\265\311h\033"
	.ascii	"\2073\273\334d\01624\262%\252v\335~\303FQ\034\301\320\005\tl"
	.ascii	"'\323\3173z\271&$#J\223\237K\226\307\342\262QBM]\331su\316#("
	.ascii	"V^\347\226X\004\3753\223\bAb\002~\311\306Ud\031\3329\270]\tG"
	.ascii	"\363\335w\356\3525s\225\333\030M\321\376\356"
	.string	"@1*\"\221i\326\355\234T\024saa\347\0354\226G\377(zH\243\364\315d#\342R/ \217\004\263\334\360)g\210vy\333\206\247\225\360\025\201\273\230\356\377U|\260\356ge\375\362)\017\205Q\371\254\\UZ\336@bXU\237\tL.(u\274H\342\227\205\263\203\353!I!\324\355tO\301l4\214\021\260\223A\231#.\244\301\2374td\273\327O\217\237:\fO^\335A\007\361\375Z\235\346w\330~q{\255\367v\023q\220\263\017F\216\356{3\227]!;\240X\236\267\2070\217\301#,\336\367\r\251\326P\3535z\202\253\"I\206\324a\307\302Nw\374\026\013\257\201jG\352\254~QLV0!FA\303\222`\231O\2106;'\264\262~D/\335\225\344^\026\037\2472k`$\017\362\3465<\f>\265\326\335c\342v58y\277\245#\244\335\353\001H\320`\206\0218_\236k"
	.string	"g\322[A\n^\023\017\241\236\220\205\246\177\345K\236\223N[\037Gb\260#\276\202\251\331\266.\375\261\020\312\340\311]\366\205\030l\234\035\037|\366U\t\200\317\254\3767jO\226\252@y\213J\362\226y\022\032&\207\0065M\324>\0249\345l9\017\204\263_\355\364\377\211R\005"
	.ascii	"\361\321\303\317T\020$|\246\265\225\250n\023>J@l\371c\220DR\007"
	.ascii	"S\267Q\331\030G.\260N\017\t\231:\227&S\246\002\006\016\223\341"
	.ascii	"\013\305\251\024\323\326\212)u\315\266{d|\335~\264\n\207HJ\033"
	.ascii	"\016tL\323\016\226\016S\304={\034\207j\025\330w\272\346\240/"
	.ascii	",\032\235\336y\375\253D\200\3607\232;\370\336=)\313\211dKW\347"
	.ascii	"k"
	.string	"\204\t'\027/\262\272=\t\311<\211\346\031s\203\367\306\031\030\226\262}\036\237p\037\374\037\342\265i\036\364e\221\316K\334tI!d\2133P\322\3013b[\336\nr\276\300\005Q\025\200\3552:d\242sh[\026\317p\\\230\345gE`W+G\n\"s\303V3>\024\035\f\321\003\b\222!+\251nk\371\f\036\206\335\265\273\244\245\202\231\230I6\354\230\230\225\254\302\240\037\245~g\321\317j\364\026\bz\215\013\256\022Q\346\216\346\315\241\252m\344T\324i\033\tj\272^\013\021\234\203\263\\g\273-\370f\0343\270\"X\020\226\351\231\257\013*\361\340\313V\373m\004@\3547g\036\bz\034\351\330T\367\324\307<E#+v\322b\302S\316\376\002\304\331\366<\355IG!\371\003:\240\026:\376\f/T~\205){\300\257\250]1%\332\247\343\222\033d\001\033?nG\305Z\204R\027\002\202\003\301"
	.string	"\201\231.rAn\206\353oB\3218n\252\032\325\n\255Q\261\316\3265\2764\330\301\344_\337.\344\220\362a!F\306\376\253\017l\227x\315U\206\203a\231I\024\206\306\206\361Af\3119R\231I\007\326\235\267@4_\347:\372\225\353\241\003\267Rq\2230\013QX\202\007/D\251O\233\033\363\326!=h\357?\257\302o\240\325+\270s\204g6\213\244%\340\206\331\024\\l\330a\341\nl\257\273\234\366t\312Z\004\254\205\301\033M\362\007\266\036\227{u\337\233\2121\306\220\325\2159\302T\364\342\203W\022\031\365\262\322S\201m\360\t\311\200\213\007|Y\315x"
	.string	"\326D\177\344\333w\002"
	.ascii	"%y\221\311\336\320\355?\37476\352\360VP\3478\312\341g\022\226"
	.ascii	"U>\377\227\345\247\003[r\200\326\245#9x\007\310\203\031t\373"
	.ascii	"y\302\236\275\371\257\t\017\275=4\350D\211\261\361+\245\377\""
	.ascii	"\311G\3421\265k\212e_\201_\211\260\003]S\016\335\373\345p\252"
	.ascii	"\3227M\241"
	.string	"|\362\344\177\361J\257\022\321\203\334\262\236\301\225=\004\237\243\255\314x\024\232\371X9\b\025\332\033\224P-D\300#\0346_\026\b\243\337\236O\273\007\315\343\214\277\361\303>\230\370IyX\311\017G\300\253/!c\366\346\376\212\352\2742c\312u\370\244\033l\376\232nh\037HY\3734C\020\325\r\200T\313g!\307\023\2058\f\371@..J\005\236Q\256\335\272#\203f*\277\177\312\234l-k}hR\201V/\352\371\347\361U\026\374)\342\245\036\n\006\340\205N\246] \235+\242\255\252\326\233\322\230)E\\U\300\221\242e\315\254\306\032S\241F\023\371\376\032\366\337\245\032X|\201.FF\367/\326\252!\260\016~\254\270\306vb\202;\n6\276\227\026\325yU\025d*\276\031N\223;D|\342\374\030N\2037\373&xm$kH!g\336\365"
	.string	"\"\232\354@\026\226\212?\325\246^\003\204\273\025MUq"
	.string	"\220\302\226%\001\253\346GDo\371S\200+\250\203\310\024w\023"
	.ascii	"f\356~z\240(e\3631\266\254\327\207\204)\355[\315t\300\211Q\021"
	.ascii	"\232\325{\340\234\320\215r\343w\332\n\302\334o\255I\003\372\346"
	.ascii	"~\246$2\346\217\331p\372Yp\251\243\b}\211\304\226a\302\365\345"
	.ascii	"\265;\r\354\270\234\356\tw'\2755f\220\236F\367\275\246\3051\324"
	.ascii	"jR\027]\n\016,4zj!\254B\3601\336H\340'\320y\311\006\224{QK["
	.string	"\002j\031\272qE\234\337\3460\236\252\255\241\207\3667\336\242\227h -Z\334\335\221c_y\332\231 :K\345C\016\022pW\221\372\356\304\266\266\261\361\006\275\317\215*\005\300\007#\204\205\357\234\273o_J\232'\237\2372\227\350$\271d,9\377/K\304~e\376\273\\\240\262n\304\266\223+Q\236.\037\330\317`\340u\025\371\240g\231\210+v\316AB\020)\211\277\312\267a\b\224\356\240\263:\t\305o\004\371\033\265d\231\b\344\314\316\337qe\212mb\336v\035mkx\"2c\335S}\354\355\235\202\251,\\\212\027\335\205\371\322\254n\230`.\b\324\006v\364\227\312\261rP[\203\352\2739\017\030\263\270\003\356|\204\251i\315\035\275\342\267\316\342o\003IRg\240\033#C\222,|;e\350a\231\336\265\361cs\222lp\213\203\020\264\006,\231\022s\354\207\222\tg\226\326\234\2375HH;D"
	.string	"s\034Y\353\201{\321\332v\317\302M\361\242[/_\221)n\b7\326\252\322\370O^"
	.ascii	"\026R"
	.align 32
	.type	test7680, @object
	.size	test7680, 4365
test7680:
	.string	"0\202\021\t\002\001"
	.string	"\002\202\003\301"
	.string	"\343'F\231\265\027\253\372e\005z\006\201\024\316C!I\017\b\361p\264\301\020\321\207\370)\2216f-\276{\035\242\013 8\331\216x'\317\265EX=\364\332\360\334!\027R\315h\342\201\254\210a\020\274\260\177\344\363x\267(l_\\\302\215=\260\207A\025.\t_\352\006\177\3515\030\220P\255\366\271\3753\002\032\231\236\245},;$\34715s\232\260\376\003\374\306\230x\331f\225\245\022\274\036\202\274\361\3051\315\246\261\f\002\277\177\267\257_\326\355\367\301Y\206:5\225T!\215j\263\321+q\365\361f"
	.ascii	"\261\210\356;\244AR\032\365\0162\266\277R\253QU\2212O\257\221"
	.ascii	"\254\367\377\216;+a\351m\035h\200\220y4\226\312IC|\211N^1\265"
	.ascii	"\316\001\233\t\257\222\006$\347\"5\314\242\013\373[\207eq\377"
	.ascii	"d>\371\3503\240\303N\262A\230T\353\023\231\3732x~\332O\323Fj"
	.ascii	"\265x\201?\004\023_g\257\210\245\236\r\305\363\347LQ\365QJ\244"
	.ascii	"Xd\331\2422T6\3168\330\302\016\r`\2162\177\220\212\274\210\276"
	.ascii	"j\300G\017\002A\377;~\305\2463\035\031\321\325gl\277\026\260"
	.ascii	"~\200\020\277\177\335\320\364\303\224,\232,\332iN\326{@M*'\313"
	.ascii	"Z\345-?}Q\235\237p\336"
	.string	"P\261\323\3228M\034\312\302\036\200\3206\202\004\346\027y\237.\311\355+\325\033\372}\032\200\265\016/\005\276J\033\376\n\255\001\336\221\310\371\201\276\307\257\347\207\355\235\270l\255e\355^\323g\214b:\347\375g\340\273W\257V\353JXn\255\362\276\303p)\370\353hE\240\275\315\245\264\331\001\267D\353\227\363\fV\344&\320\245\261\243In\210\362\"\342{X:\331R\244\261L\\|\360\210{\237\006\3512N\362d\203\213\242\352\035%\361\215\026\213\340\253\322\351\344k}v\230\"S1k\314\361\345\035\327\245\260\352k8\024\f\006\020'\3303\363\232\256\224\335\013\264m\345\221\335\361\017'\244\224U\360\336\007)\346?&\031\241\335\321\006\231\332T#<\365\\.\226\251!#%.o\361\371\021T\345{\271\037\021\342\236ka\213\243\213\301 \233\373Q\357\273\271\366\257f\263,%\357v\313\277z\223/\341\027V\301"
	.ascii	"3\265\331\221\0051\314r\315J\223\232\343!B\236\270Nl'\223\360"
	.ascii	"\177\"\333\345\263\243\367\347\200\273\221\312\367\350R\270\021"
	.ascii	"df%\224\370o\013;\267\377\200\2366\351\210.\253\005\277\231\237"
	.ascii	"+O\306\261\023[\006\377\n{\274\177\007\2405\302-D>\255D\313G"
	.ascii	"\030&q{\027\311m\265K\317\337\024,l\337!"
	.string	"\316\223I4iI\375>q[\372\007\305~^T\032<\246)\265\277\r\361\306\244a\326\027\035\360\242x\217\274~\f\264\360\036\005\352\265\255h\225\013'\264)|p*\232\n9\324v\267r0^\256\234JU\307F\327_\276\020a%\030z\237\323\005=o\232\036\354+\003\340Ij\234\326\333\302\241\341\n\2731B\310CN|\251|`\352\276\361\213\350\262\220\203\024!\344\263\r|c<\230U\306D\246\250\036B\267\211\250\275\2704=\t\200\231s\237\257\027V\362s>\036n\351\030\240[i\316\375=w\201\225;\361\336&\351'\357\222*\227\334\225\245\243\260\373\226\211O\346\301B\013\375\264m\n\237\2331\330!8\212\356\266\\\022\250\264\007yA\247\177\023t\255\013\356(R\254/M0\034\305\246\245aB\275\341O\323\354f\362c\364\223\3335-;q%\t\336\332F\332\342\247\243\337\315\277X\005%\002\003\001"
	.ascii	"\001\002\202\003\300_\325\025\033\t\344\247\300\246\330\r\250"
	.ascii	"*\323\035F\003\007\360\230"
	.string	"\344K\231f\216r\347\273Q\306\032\2766\364R\272\250\277\252\343q\035\203!\300\246\210O\367+\223&\344\247\355P\030\252\364L\242\376\222|\336.Tv\302%\036\230\246H\0019o\037$\227\233d\225\034\215c\215Do\235\337\364\032\245\232\036\323l\256\251\214?\373/x\366\246\326\006\323\267&\377\036\333\215\3147M\\\342\303\245u\346\371\264L\204o\236XU\310\001\3722\322n+E\362\306H\255@\330\271<\033\370\367\202\323\016s\343\261[\202qw?o6\232\340\354Q\370_\204\222\356\270~\347\032\024P\202zM\346\326\243v$\212_\376\031\335\327\367[\256\030\004\220\315\\\345d\350\004\261\006\245\335\370\235q\023\2526\177a'\364\254\225}\032\231}\340\325\234Z\255\232\377T\260\261UE-\031XR(\335\340\265eR\227E\360+\230\037al\235\252Y\205\371\227{\275\353\225\201\373)\214\360R\337\355\356\262"
	.ascii	"25\024\250\244\312\221\377\030\267\226\373"
	.string	"2b\251\240\320wC\365\231\321\356\350\255\032,\324\353\341\365\001Ax\300'\031P.\272\"\321\353\263\245'\013\354\371&~\037\347\027\2379\250r\"cyj\234\211U\232\264aA\274\252\0247)\003\300RN1D\217.\027\201\210\364\316\332A\270\325\024\221\214\312\322\r\231\006\t\302\267\350\256\372\001\352\231bh\266\337\310'\256\277\260\233[\032\242\342Zz\345K\222\037\377s\256\026@xB(\273\023^\274qzx>\330\033\302,\326\334\3729r\370\242,\213\034]\253\270\007\307\256)\223h\277a\351\2447\203}\023\307\030\360}\244 G\024h\225FVm\325{\341Q\217\226\301{5\tz\211\016\337\022\325\341\234*\224\225C\223H\246#\346\330\362\270\016\272ma\003\257@c+/\356aL\304p=x\301O\216\013\233\0065mm\2037\2739}\1773\223\304\353\216\374\332\360T\376\035\304\323\203\231\337e\356"
	.ascii	"}\206'\324:k\346\202\216X-\0038\357l\202\207\030;G\347\274\341"
	.ascii	"XpMF\2264`\226\025\t<\204@\257\2002u\307#l\373\035Ws\031\t\350"
	.ascii	"\032L\002\\~N\276u\370s\377-T\031U\365\364\033\311\274\302\031"
	.ascii	"\313\267Nj\r\377\312}\320\210\221\213\233!\244\242C\r\274\236"
	.ascii	"s}T}\225\314c^\301\270\346'\377 \007\350n~\362\017Z\t\357\345"
	.ascii	"M\2009\225\325\364\356;\312|s\3709Z\301\035}\224r2\255X\342\374"
	.ascii	"qnf\252\241Y\326\254\253\276\214S\231\315\350-\265\263FX.\026"
	.string	"\327M\213}J\261L\205\221\033WT\370\024Y\333\304,\234\bm=\327\366\246\346\263*\347)\034\253\264\355\023\031\370\266`\222DS\324\251~\272!\242\334n\245^SY<Ra{_\031\255\310mh\215z\311\326\357\353gO\312\347\366)6\227\373>7\225\205qp\366c\206*)\327\232\226v\247G\230N\0061\257\363O*e\220jK\216Cy\342\335\316\b\034\001\3548A\335\031\330\3636\0035\003\257\034E<\254\023\2526\026Hw\263\276\243\263\235\177 \312te\254\223\247T\255\310h\016\370D\037\255,\267\232\232\007\345\315\207\340\024\265\257\323\327\317\023\237;\275\376)\013r\365LT\224\307f\354\250A\226=\027\355\031\300\202>_\232\221\376\321/\270\224\252Xh\2251\207W\232u\224M8}V\202\201\234\2714+\347@\331<w[\225Q\006\021A\343\213\2672\353\341\005\033\020\250\016\241\002\202\001\341"
	.string	"\37284\376U\207qbG"
	.ascii	"3dgpyv\337\376\303(8\337\220\324\300\356\230\277\235\233\205"
	.ascii	"\330ae\245p\365\322,\277/\265Uy\222\023\272M<9\277\3251\023"
	.string	"z1\364\213\316\370\320\323\233\342\3561\333\272\314\032\272\034\215\356\352\313\323Z\255\207\326\371\025/n"
	.ascii	"\006t%\215\377\310\246\021\034\350\026\032\336S\005\271SU(\203"
	.ascii	"=\276a\f\304\230}\366\3546\303\345\347\035\024d\313\rb]z\315"
	.ascii	"\210\374fN\3716G\225\030:H*\377b\217l\342\302\351\323jE\\\365"
	.ascii	"\211S\\\276\317\255\207\"\2341H\333\330\344\3458\256\302\260"
	.ascii	"\322\272\2670S-\2615\361X\017\212\006Qv\271,2\340\321\252\202"
	.ascii	"4iq\034_5\250\235\021\254\023\333{\366\223\343\271\275\331\262"
	.ascii	"\206\377a\210+r\\\204\341\fr\253D\377#"
	.string	"\023\257\321Z\323\352s\376\325\244}\236N\254\003\223r\024-\226o\356\264\315N\253\352q\223\201\340=\315a\226%v\275\304\265\335|\361\271\341,X\033\244FK\022WX\252:\256\211\243\263\317\037\215g\337m~\216\372\305\tsFVU\220\353wN\026Oh{\037a#\354\251q03%\307N&.N+\302d_\365\217zK\034\006\263\221\366\233Q\267\260dr\004\345\372\024/\355a)\003s\031\025n,\213\016\354M\361\343oX|\311Hg?Q\265\267&F\247%yU\376:D\264D\374\270\0244G\327\243\016v\347\203\232\002\303\317+\331\203\223\325\356\231tEb#\246\002\311\300\020p\n\231)\fy\004Lw!\226\360\245\027\"\276\253\233\327B\323\351\300BD}\235\311=\3716\227\033uR\217\351\271\214\247d\031[]`\264B\225\311\333\202\003\306\260(rd\003AM\217\306\320\315\002\202\001\341"
	.ascii	"\350f\247\371\017Z!\374\210N\221\325J\360\3642\345\r\363\006"
	.ascii	"\225\320NG\f\004fw\375\270\223\r\377\217\227\240J67\246^\225"
	.ascii	"y\310\262!\230\201\361\270\364R\257<\214\206\205UV\374\220\343"
	.ascii	"2P|T\007\236\355\374\324\271\\\230\"\373r\327\203\360\321a\020"
	.ascii	"\275h]r\301\316\222Cw\237\270\215\216\362\343bJ\223\003\323\331"
	.ascii	"\001\250\231o\243Lmz\362\236\216k\274\344\235\216\347%\206\244"
	.ascii	"\251\302\357\337\273n=KW\225\201oh?\031\250\377Z\bz\344LN\264"
	.ascii	"\352\364\310/\357\214^\315b\034\214\223`]\243\021d\013\353m!"
	.ascii	"\274:[\\\f\247\212\306\250\341H\201\001\265e\253.\2768\224\367"
	.ascii	"\2463\301n\013\2108\347\033\004\232\020-\035?__\310\357\315\305"
	.ascii	"\026\334\204\300f\340\243\374\372\226\307\267\354O@\n\305"
	.string	"\276m9J~\221O\341\003\3229\274\207i\241\360m\021\365\264\235\256vk\306\277\344G\274M\023\210\250\203\365\256\035\373MLD\003\330\244.M\370_E\224X\327\331KG\330\3745\005\355\264\266\3026.\272\322z\272i4\277\361\241^\027q\211\323TW\005+\202\343\nd\\;\214k\307\020\212\265\323\327\220\353\333\035\240\277k\352\3151z\215d\314X\300\007\244n\024\013\363\352>\207\237|\270\034\"&\212}\220\335W(8\314\016q\222\211\356y\210\274\005!\332B\222Rf\254J\345\365nG\325\2727\323|\211\324\330o\336cD\265\210\335\2610\264m\315\277\3104'Y}y\334\226[\216\300\207\300N@\007\023\221k:\022\003dp\257\200$\034\\\373\365\300t^\257\006\030\004gJ\275\254\327\312\276N\241\031H}\246Y\366\032bPSF\244[\234Z\375\211\235\324\336\364\247=\210s\245\271\002\202\001\341"
	.ascii	"\347pY\303\355\304k\241\245^\220*\214j\302N\253\374\356\362#"
	.ascii	"8\326\263\223\b\236\f\216q-\251\350\334\245\334\007\343\2613"
	.ascii	"\335\242\362>\222X\340\367S\177n\352x\2145xCc\225\273\033\034"
	.ascii	"\277\221u\024t\323 \272\217\356\235q\241\207\212$\323aS\373\354"
	.ascii	"\026\204\276M9\335\n\254\316 \234\257\212\023\370\"/\324\231"
	.ascii	"\210t\272\026:c\377LZ\003Zo\254)3\245P\321\332\355'\313grc\205"
	.ascii	"\374\360\310\210\277\205\357K\376\256\331\325\273\206\244v\350"
	.ascii	"\177\264"
	.string	"\333\261\356\032\177\231\327\233oz\224\\\354,`\201\255\247\276\200.\237\246\300\373\tm+\253\244\025\307yF$\211\\2\271\207\251T\036\022\220\216\002\200\214\370\333/\274\230\033\242xs\211\003\227\343\t\b\213u\317\334#\220Y\357[\230$\270\350\317u\360/\267\243\346\027\006\360R\376!\n\026\216\370\341\256%\021]\214\225\033OE\270\250\315\346\371\312\240T\223\225\206o\344\223\"\017\362\317\275#\260\364\217\231\247g\231\005\023\037\353\210\370\342;\271I5\211O\270\00676\332u%\017\n\252\302l>\261-\026\363\027\333\342\02629\222K_\300_n\320\034~\300Q\331\263\3427\307\340@\023}\006\315\315r\266S-~`I\3761\341\320\016L\230\223\340\366\362\372\231\177e\330\025\306:\270Mc!x\344\031k\275\336@[\214\372Iu#\217\024\302;\243\233\305\200\032\243`\327\027'\360\030\017\272\002\367z\355\244"
	.ascii	"w\336K\335\371\327>u\355\032C&q\033\274r\365pr\003p%\207\201"
	.ascii	"j\222-\267\002\360\020ye\235N\021}\\[7\252\264\372CfHlgd\236"
	.ascii	"\025u6\347%U\007\177t\037,(v\347\233=\221\013\315j\035Z\352c"
	.ascii	"\320\371\002\202\001\340>1\362\364)\222\242\223\325\332\311\026"
	.ascii	"~\366\3333\237\257K\001\321(-:\300Q\221&\275\245\036\335\331"
	.ascii	".\021\223\031)G]c\344\266\361\352\022)\241e\022mx\217c1\354r"
	.ascii	"Tsr&HWW\310\336('\365b\373\177\033\363\2571\001\374\001Xz\200"
	.ascii	"r\235n\007\314Eg\306&\376%\245\233d\315E\34318\005\0076\005F"
	.ascii	"\234\301\216\277Nq_\352\345\f\232A\310\224\314\361s\0060Tv#\267"
	.ascii	"\"z\216\346B\241\2402\022\351\b\034Fy\f\202z\225y\277\203\200"
	.ascii	"\353\253=2\305\336b\353\220)s\005\310\n\261Q\361#\335\036\365"
	.ascii	"\002>t\274$\f`6*(M\346\206\230|\331\341\254!3\252\251\213\266"
	.ascii	"\212\033\367T\024\363\rO\315|\365\302m\302\360\342\374c\036\246"
	.ascii	"\251\251\331s*\325\n8\330\300\267\341"
	.string	"Q\344#7\367\205f\016?\032\214\317\022\242Gos\221!\343\223ktO\305\241\3472\367\206\335\032n\226\3322\035\335\372B\325\324\375\256z\241\355=y\376\210\204C\247\354\363z\023\252\241\202\002\203\031C\nFx\007\331M\377\254g\326)\211\376+\253_\232\207\231\200\257pJj\271Z\302\254\177\242\307\255\342\037\354\305\022\027\b\207\217 \225\276\257b,\302?\211V\330P\226\227r\342\222\341*\330\204\2371\343\006\330\345\221c\031\341'\255\342\362\n^x\213\033\0231K\275w\262\326\\\222\201P\0027\322\346\353fk\252\374\315T]\270\003\207\350\372\262\336\313\370nX\336\313\tT\212\237F\243~\215\025\377\033\r\211\304\032!1^\355\013g<p\355\222H\357\354\360w\302yl\006\t\252\253\366L\315\372~J\210\334\250\233\323i\224\210\t\0350C\236,\313\001\035J;\004\354\016\261\336\t\255)\002\202\001\341"
	.ascii	"\237\002\023z\320\251\212z\240\005\273Do\257\367\343\3245\357"
	.ascii	"s9\325\340\242\017\032%\250\367\302\245\354W\370\r*\266d\003"
	.ascii	"\214\"\017\347\230\241\022\376$\357a(\237\247\"km\253\215}*\213"
	.ascii	"\256\213\375\313\325\013y\033\211\313[z\214\334\350\215\3355"
	.ascii	"\237\006id\022\353Fy\337\202,\211u\236z\354\255\345\2101\372"
	.ascii	"\206\223\312\361-\233bZ\351C\t\363\214\345\307"
	.string	"\300\316\206\347\333\307M'\325\356v\31650G\357"
	.ascii	"\033i\232?\245*\311\007\253\231\272*\347\373\251N\271\256,P\374"
	.ascii	"5I\346\227x<\261Y\327\035NN\352\336\240\320\304\035\261\323S"
	.ascii	"\036\371\277\263j\027\264\332\314'\031\3065\350(\323\343v:\334"
	.ascii	"\320u\310\264"
	.string	"l\276\204*E\321C\"T\327\305\320\327s5k\250\372\255`\300d\301X\211\t\201\n\013\3523\221\260\357SPA\256\331\356\276\236\360\013\240|\277?\311K\340H\330\020\325.\316\360|\330\005\336\t~\214cL\333\213\221\315\177\266k\255\316\261\027l\367\b\r|\332O\n\007\320\256r<gJDTG\316\341\027\007\022\336R\357\357L+B}\t\20064\334Eo\260-\253\240\fX\2565\323\2337\301\035\353\376\303\004\311\035\347=\026d\355\365\350\337\231\244\373\255y\210\325\214b3\2365\246\177\235\266\032@m\303\211]{\342\310\323\026\023\007\2328\"3\003\254p>\3162V\013XV\270\351\330B5l\271\002\263d\353\252\t?\254f\b\264_>\264\3549\261\231\344]\0352\024\301H\217le\2074P\244\364\233[.\265y\r\021b\2445\234o\222\320h\007\335i\205H\343]\0204\257\352ArZq"
	.string	"\370\346G\177\240o\221\226@"
	.ascii	"@p\373c\317\3116\004\034;\021\b)\201\237"
	.align 32
	.type	test4096, @object
	.size	test4096, 2349
test4096:
	.string	"0\202\t)\002\001"
	.string	"\002\202\002\001"
	.string	"\300q\254\032\023\210\202C;QWq\215\266+\202e!S_()O\215|\212\271D\263(AO\323\372j\370\271(P9gS,<\327\313\226A@2\273\353p\256\037\260e\367:\331\"\375\020\256\275\002\342\335\363\302y<\306\374u\273\257N:6\302O\352%\337\023\026K \376Ki\026\304\177\032C\246\027\033\271\n\363\t\206(\211\317,\320\324\201\257\306m\346!\215\356\357\352\334\267\306;c\237\016\255\211x#\030\277p~\204\3407\354\333\216\234>j\031\314\231r\346\265}m\372\345\323\344\220\265\262\262\022pN\312\370\020\370\243\024\302H\031\353`\231\273*\037\261z\261=$\373\240)\332\275\033\327\244\277\357`-\"\312e\230\361\304\341\311\002k\026(/\241\252y"
	.string	"\332\334|C\367B<\240\357h\367\337\271i\373\216\001\355\001B\265NW\246&\270\320{Vm\003\306@\214\214*U\327\2345"
	.ascii	"\224\223\354\003\353"
	.string	"\"\357w\273y\023?\025\241\217\312\337\375\323\270\341\324\314\t?<,\333\321I\1778\007\203m\353\bf\351\006D\022\254\225\"\220#g\324\b\314\364\267\334\314\207\324\254i5L\26596\315\244\322\225\312\r\305\332\302\305\"2(\b\343\322\21380\334\214uOj\354z\254\026>\250\324jE\341\250O.\2004\252T\033\002\225}\212m\314y\312\362\244.\215\373\376\025Q\020\016M\210\261\307\364y\333\360\264VD7\312Z\301\214H\254\256H\200\203\001?\336\331\323,QF\261A\266\306\221r\371\203U\033\214\272\363s\345,tP:\276\305/\247\262m\214\236\023w\243\023\315m\214E\341\374\013\267i\351'\274e\303\372\233\320\357\376\350\037\263^4\364\214\352\374\323\201\277=0\262\264\001\350C\017\272\002#Bv\2021s\221\355\007Fa\r9\203@\316z\324\333\200,\037\r\3214\324\222\343\324\361\302\001\002\003\001"
	.string	"\001\002\202\002\001"
	.string	"\227l\332n\352O\317\257\367L\331\361\220"
	.string	"w\333\362\227vr\271\267G\321\234\335\313J3n\311uv\346\344\2451\214w\023\264)\315\365R\027\357\363\b"
	.string	"\343\275.\274\324R\210\3510u\013\002\365\315\211\flW\031'=\036\205\264\301/\035\222"
	.string	"\\v)K\244\341\022\263\310\t\376\016xra\313ao9\221\225N\325>\307\217\270\3666\376\234\223\2328%z\364J\022\324\240\023\275\371\035\022>!9\373r\340\005=\303\345P\250]\205\243\352_\034\262?\352m\003\221U\330\031\n!\022\026\331\022\304\346\007\030[&\244\256\355+\267\246\355\370\255\354w\346\177Ov"
	.ascii	"\300\372\025\222\264,\"\302\353j\255\024\005\262\345\212\236"
	.ascii	"\205\203\314\004\361VxD^\336\340`\032ey1#\005\273\001\377\335"
	.ascii	".\267\263\252t\340\245\224\257K\336X\017U\3363\366\343\32646"
	.ascii	"W\326y\221.\276;\331N\266\235!\\\323H\024\177J\304`"
	.string	"\251)\370S\177\210\021-\265\305-o\356\205\013\367\215\232\276\260B\362.q\257\0311m\354\315o+#\337\264@\257,\n\303\033}}\003\035K\363\265\340\205\330\337\221k\ni\367\362if[\361\317F}\351p\372m~uN\251w\346\214\002\367\024M\245A\217?\301b\036q^8\264\326\346\341K\302,0\203\201oI.\226\346\311\232\367]\t\240U\002\245:%#\320\222\303\243\343\016\022/M\357\363UZ\276\346\031\2061\253u\232\323\360,\305A\222\331\037_\021\214u\034c\320\002\200,h\313\223\373QsI\264`\332\342&\257\251F\022\270\354P\335\022\006_\316Y\346\366\034\340T\020\255\366\315\230\314\017\373\313A\024\235\355\344\264t_\t`\307\022\366{<\217\247 \274\344\261\357\353\244\223\305\006\312\232'\235\207\363\336\312\345\347\366\034\001e[\373\031yn\b&\305\310(\016\266;\007\b\301\002\202\001\001"
	.ascii	"\350\034s\246"
	.string	"\270\340\016m\215\033\271S\355X\224\346\035`\024\\vC\304X\031\304$\350\274\033;\013\023$ET\016\3147\360\340c}\303\367\373\201t\201\304\017\032!H\257\316\301\304\224\030\006D\215\323\322\"-->Z1\334\225\216\364A\374X\311@\222\027_\343\332\254\236?\034*kX_Hx \261\257$\233< \213\223%\236\346k\274\023B\024l61\377z\321\301\032&\024\177\251v\247\f\370\314\355\007j\322\337b\356\n|\204\313I\220\262\003\r\242\202\006w\361\315g\362G!\002?C!\360F0bQr\261\347H\306g\022\315\236\326\025\345!\355\372\2170\246A\376\266\372\2174\024\031\350\021\367\245w>\267\3719\007\214g*\253{\b\370\260\006\250\352/\217\372\314\314@\316\363pO?\177\342\f\352vJ5NG\255+\247\227]tC\227\220\322\373\331\371\226\0013\005\355{\003\005\255\370I\003\002\202\001\001"
	.string	"\324@\027f\020\222\225\310\354b\251z\313\223\216\346S\324\200H'KA\316a\337\277\224\244=q\003\013\355%q\230\244\326\325JW\365l\033\332!}5E\263\363j\331\323C\350\\T\034\203\033\264_\362\227$.\334@\336\222#Y\216\274\322\241\362\340L\335\013\321\347\256e\274\265\365[\230\351\327\302\267\016Uq\016<\n$k\246\346\024a\021\3753B\231+\204wt\222\221\365yy\317\255\216\004\357\200\036W\364\024\3655\tt\262\023qXk\3522]\363\323vH9\020#\204\235\276\222wJ\355p>\032\242l\263\201"
	.string	"\303\311\344R\310$\210\fA\255\207Z\352\243z\205\034^1\177\3035\306\372\020\310u\020\304\226\231\347\376\001\264t\333\264\021\303\310\214\366\367;fP\374\333\353\312G\205\211\341e\331b4<p\330.\264/e<J\246*\347\307\330A\217\212C\277B\362M\274\374\236'\225\373u\377\253\002\202\001"
	.ascii	"A/D"
	.string	"Wm\022\027[2\306\267lWz\212\016y\357r\250h\332-8\344\273\215\366\002e\317V\023\341\032\3139\200\246\2612\003\036\335\2735\331\254C\2111\b\220\222^5={\234o\206\313\027\335\205\344\3555\b\216\301\364\005\330h\306c<\367\377\367G39\305>\267\016X5\235\201\352\370j,\034Zhxd\021k\301>Nz\275\204\313\017\302\266\205\035\323v\305\223ji\211V4\334J\233\274\377\250\rn5\234`\247#0\307\006d9\213\224\211\356\272\177`\215\372\266\227v\334QJ<\353:\024, `iJ\206\376\214!\204IT\263 \341\001\177X\337\177\265!Q\214G\237\221\353\227>\362T\317\026F\371\331\266\347d\311\320T\352/\241\317\245\177(\215\204\354\3259\003v[-\216C\362\001$\311o\300\365io}\265\205\322_\177x@\007\177\t\025\265\037(e\020\344\031\250\306\236\215\334\313\002\202\001"
	.string	"\023\001\356V\200\223p"
	.ascii	"\177R\322\224\241\230\204J\222%L\233\251\221.\302y\267\\\343"
	.ascii	"\305\325\216\302T\026\027\255U\233%v\022cP\"/XXyk\004\343\371"
	.ascii	"\237\217\004Ag\224\245\037\254\212\025\234&\020l\370\031Wa\327"
	.ascii	":}1\260-8\275\224b\255\304\3726BB\360$ge\235\213\013|o\202D\032"
	.ascii	"\214\310\311\253\273LE\374{8\3560\341\374\357\215\274X\337+]"
	.ascii	"\rT\340IM\227\231\217\"\250\203\276@\273P.x(\017\225x\214\217"
	.ascii	"\230$V\302\227\363,C\322\003\202f\201r_S\026\354\261\261\004"
	.ascii	"^@ H"
	.string	"{?\002\227j\353\226\022!5\376\037G\300\225\352\305\212\b\204O^c\224`\017q[\177J\354O`\306\272J$\361 \213\247.:\316\215\340'\035\265\216\264!\305\342\246\026\nQ\203U\210\3210\021c\325\327\215\256\026\022\202\304\205"
	.string	"N'\203\245|\220.\345\242\243\323Lc\002\202\001\001"
	.string	"\206\b\230\230\245"
	.ascii	"\0059w\331f\263\317\312\240q\263P\316=\261\223\2255\304\324."
	.ascii	"\220\337\017\374`\301\224haC\312\232#J\036Er\231\265\036a\215"
	.ascii	"w\017\240\273\327w\264*\025\021\210-\263Va^j\355\244FJ?P\021"
	.ascii	"\326\272\266\327\225eS\303\241\217\340\243\365\034\375\257nC"
	.ascii	"\327\027\247\323\201\033\244\337\340\227\212F\003\323F\016\203"
	.ascii	"HN\322\002\313\300\255y\225\214\226\272@4\021q^\351\021\371\305"
	.ascii	"J^\221\235\365\222O\353\306p\002-=\004\252\351:\216\325\250\255"
	.ascii	"\367\316\r\026\262\354\n\234\365\2249\271\212\374\036\371\314"
	.ascii	"\362_!1trkd\2565a\215\r\313\347\3329\312\363!f\013\225\327\n"
	.ascii	"|\312\241\251Z\350\254\340qT\257(\317\325p\211\340\363\236Cl"
	.ascii	"\215{\231\001hM\241EF\fC\274\314,\335\305F\310N\016\276\355\271"
	.ascii	"&\253.\333\353\217\377\333\260\306U\257\370*\221\235PD!\027"
	.align 32
	.type	test3072, @object
	.size	test3072, 1767
test3072:
	.string	"0\202\006\343\002\001"
	.string	"\002\202\001\201"
	.string	"\274;#\3003\247\213\252\312\243\214\224\362LR\b\205\200\3746\025\372\003\006\266\326?`\212\211\r\272\032Q\013\022\352qw\366:0!=$\370.\320\027:\205\224%B\211\377jh\337\037\206\256\245\273\232y\366i\224\376\336\376\316\033.\256\035\221\313\271\361-\330"
	.string	"\202Q\216\371\375\254\361\016\177\267\225\2055\371\313\276_\323X\343\241T\2360\261\215\001\227\202\006\216w\373\316P/\277\361\377W\nB\003\375\016\272\036\312\205\301\233\245\235\t\016\351\273\305sG\r9<d\006\232y?P\207\234\030-b\001\374\355\301X(!\224\036\371-\226O\320\274\361\340\212\372M\266xJ\336\027Y\260\"\240\232\323p\266\302\276\274\226\312A_XN\316\357dE\335?\201\222\314@y\374\031\342\274w/C\373\216\255\202J\013\261\274\t\212\200\303\017\357\322\006\323K\f\177\256`?.R\264\344\302\\\246q\300\023\234\312\246\r\023\327\267\024\224?\r\213\006p/\025\202\215GE\246"
	.string	"\212\024\221\336/P\027\343\0354)\214\344Wt*:\202e&\367\215\314\033\217\257\345\205\345\276\205\326\267\004\350\365\324t\342T\024\335X\317\037\021\212\237\202\242\001\371\302\337{\204\261\330[p\273$\347\320*u=U\254E\351\253\306\204\212\347m&\022\211\265g\350F\235F\032\372-\300[`F\213\2672\003\377u\356\237<\335\2665N\202\275\231sQ\002\003\001"
	.string	"\001\002\202\001\200B\356\244\237\313\276`#\263:\304\332\221\356!\235v\033\217\223\213\355\002\366x=f\373\345G&\342nI3.\336\276\312q{\357qbT\253\013\272c\b$G\261\230\037\211\373D\237R\216\211\273\325!\361\fv.\315\022nx\313\241\245\270N\007\253n\337fW\207\377\210_\314\234\232{\025_*\203\333\325\237ej\235\264\225\374\340\""
	.string	"\036\242\215VZ\236\n;\020\007$\354U\314\257\207;\326\215\244\206\200\030B\333\235$\303\227;\211Z\003\263\nr\321x\360\310\200\260\235<\256^\n[n\207\323=%.\0033\001\375\261\245\331X\001\271\257\3662j8\3479c<\374\fA\220(@\003\315\373\336\200t!\252\256X\351\227\030\205X=+\326a\366\350\274m*\363\270\352\214dD\306\323\237"
	.string	"{\262R\030\021\004\226\267\005\273\3028[\247\n\204\266O\002c\244W"
	.string	"\343\336\344\362\263U\331"
	.string	"\251\322\\i\237\345\200O#|\331\247wJ\273\tmE\002\3172\220\375\020\266\263\223\331;\035Wf\265\263\261nS_\004`)\315\350\270\253b\2023@\307\370d`\016\253\006>\240\243b\021?g]$\236`)\334L\325\023\356=\267\204\223'\265j\371\360\335P\254F<\346\325\354\367\267\237#9\234\210\214Zb?\215J\327\353^\036I\370\251S\021u\320C\036\307)\"\200\037\305\203\215 \004\207\177W\214\365\241\002\201\301"
	.string	"\367\252\365\245"
	.string	"\333\326\021\374\007m\"$+K\305g\0177\245\333\2178\342\005C\232D\005?\251\254L\230<r8\303\2113XsQ\314]/\217m?\241\"\236\373\232\264\270y\225\257\203\317Z\267\024\024\fQ\212\021\346\326!\036\027\023\323iz:\325\257?\270%\001\313+\346\374\003\330\324\367 \340!\357\032\312a\353\216\226E\216\\\346\201\013-\0052\371Ab\2643\230\020:\315\360z\213\032H\327;\001\365\030e\217<\3021;\323\247\027_|\f\347%\030Z\b\341\t\211\023\247\305\022\253\2100\315\006\371\272o\312\234\212\332>S\220\327\026.\374\274\255\326=\300fL\002=1\375l\333\034\337\2263#\002\201\301"
	.ascii	"\302\220G\304\373Y\360\305\024u)\372w\241\215\324\220\241\r?"
	.ascii	"\026\210\343L\217\217\030\214\234\212\325\247A\231\363\200\216"
	.ascii	"\261\270c\330?\225\320\320+\365\346\223\350\376\320s\325\275"
	.ascii	"\264\356Q\031j\020\312\310\272\244M\204T8\027\265\320\250u\""
	.ascii	"\305\033a\246Q\210c\360O\321\210\331\026I0\341\250G\3110\035"
	.ascii	"\\u\330\211\266\035E\330\017\224\211\263\344Q\372!\377o\2660"
	.ascii	"o3$\274\t\230\351 \002\013\336\377\305\006\266(\243\241\007\350"
	.ascii	"\341\322\302\361\321#kL:\256\205\354\371\377\247\233%\270\225"
	.string	"\035\250\024\201OyO\3269]\346_\3224T\213\036@L\025ZE\316\f\260\337\241\027\270\260j\202\245\227\222p\373\002\201\300wFD+\004\360\332u\252\324\300\3002\177\017l\260'i\373\\s\353G\036\225\342\023d\033\266\321\035\312+B/\b,i'\355\321\265\004#\305\205-\241\242\224\302CMI\222t~$\222\225\363\231\235\326\030\346\317\234E\377\211\b@*\016\240(\371\203\376\301\346@\250\342)\311\260\350\232\027\262#~\3642\b\311\203\262\025\270\305\311\003\321\235\332>\250\277\325\267}ec\224]]\224\264\317\215\007\013p\205\216\316\003\013*\215\263<F\300/\307rl\234]\007\017E;kf2\253\027\203\330L,\204q\031\217\252\n\377\274\367B\020\350\256M&\257\335\0063)f!]\365\256\027\007\037\207\236\256'\035\325\002\201\300V\027O\232\212\371\336>\346q}\224\265\260\307\270b\022\321p\264"
	.string	"\370J\335O\0356\302\341\357\356%j"
	.string	"\304F\337\276\316wV\223m%_\376[\373\340\3427\314\271\254J\316\025\026\240\3073c\244\252\245\036C\301\332C\372C@)\225|+6S\347}\tM\330R\254t_\b\201!\\:Z\316\363%\266\036!vL|qPq\252'\002[#\006\013![\307(\243=\215%\233*-\235\241\034\035\313}x\370\006~ \177$*\\\244\004\377*h\340\346\243\330oVs\241:N\311#\241\207\"jtx?D\034w\023\345Q\357\211"
	.string	"<jJZ\216\3650\242\223~\222\233\205U\257\376$\257W\002\201\301"
	.ascii	"\244\302jYE\352q}L\257\257\326U\227s\305\241<\366Y#\266\037^"
	.ascii	"\234\226\017\227f\202\221H6p\002g\3364\246\225{QCf\244\026EY"
	.ascii	"\022\3335\031K\277\035\253\363?\264\264of\260g\306w,F\250\003"
	.ascii	"d\232\023\235@\"Vv\032|\036\342\332\177\t\317\020\343\362\364"
	.ascii	"*;F\307a\233\357J\030`\2142q\271\335\254\240\306\215?\253\303"
	.ascii	"!,\353\221\217\307C\r\fg\236\253\346\215\266-A\312C\330\3130"
	.ascii	"\373;@\r\020\233\261U\223s\213`\357\300\356\300\246zy\220\375"
	.ascii	"L%\324Og\276\367\206<]+}\227=\242\221\245\006i\366z\270w\346"
	.ascii	"p\251\330\206K\246\317g\0353\317\376>"
	.align 32
	.type	test2048, @object
	.size	test2048, 1191
test2048:
	.string	"0\202\004\243\002\001"
	.string	"\002\202\001\001"
	.ascii	"\300\300\316"
	.string	"><Sg?O\305/\244\302Z/X\375'Rj\350\317JsG\215%\017_\003&x\357\360\"\022\323\336G\262\034\0138c\032l\205z\200\306\217\240A\257b\304g2\210\370\246\234\365#\035\344\254?)\371\354\341\213&\003,\262\253\363}\265\312I\300\217\034\3373:`\332<\260\026\370\251\022\217d\254#\fid\227]\231\324\t\203\233a\323\254\360\336\335^\237D\224\333:M\227\350R)\367\333\224\007E\220x\0361\013\200\367W\255\034y\305\3132\260\316\315t\263\342\224\305x/4\032E\367\214R\245\274\215\354\321/1;\360IY^\210\235\025\22252\301\347a\354PH|\272\005\371\370\370\247\214\203\350f[\353\376\330O\335m6\300\262\220\017\270R\371\004\233@,'\3266\216\302\033D\363\222\325\025\236\232\274\363}\003\327\002\024 \351\020\222\375\371\374\217\345\030\341\225\314\236`\246\3728M\002\003\001"
	.string	"\001\002\202\001"
	.string	""
	.string	"\303\303\r\264'\220\215K\277\270\204\252\320\270\307]\231\276U\366>|I \313\212\216\031\016f$\254\257\0033\227\353\225\325;\017@V\004P\321\346\276\204\013%\323\234\342\203l\365b]\272+}=zl\341\322\016T\223\200\001\221Q\t\350[\216G\275d\344\016\003\203U\317Z7\360%\265}!\327i\337o\302\317\020\311\212@\237zp\300\350\350\300\346\232\025\n\215NF\313z\333\263\313\203\002\304\360\253\353\002\001\016#\374\035\304\275\324\252]1F\231\316\236\370\004u\020g\304SGD\372\302%s~\320\216Y\321\262Z\364\307\030\222/9\253\315\243\265\302\271\307\271\033\237H\372\023\306\230M\312\204\234\006\312\347\211\001\004\304l\375)Y5\347\363\335\316dY\277!\023\251\237\016\305\377\2753"
	.string	"\354\254k\021\357Q^\255\007\025\336\270_\306\271\243\"eF\203\024\337\320\361D\212\341\234#3\264\2273\346k\201\002\201\201"
	.string	"\354\022\247Ytj\336>\255\3306\200P\242\325!\201\007\361\320\221\362l\022/\235\032&\3700e\337\350\300\233j0\230\202\207\354\242V\207bo\347\237\366V\346q\217I\206\223ZM4X\376\331\004\023\257y\267\255\021\3210\232\024\006\240\372\267U\334lZL,YV\366\350\235\257\nx\231\006\006\236\347\234QUC\374;l\013\277-A\247\257\267\340\350(\030\264\023\321\346\227\320\237j\200\312\335\032~\025\002\201\201"
	.string	"\321\006\f\037\343\320\253\326\312|\274}\0235\316'\315\330IQcd\017\312\006\022\374\007>\257am\342S9'\256\303\021\236\224\001O\343\363g\371w\371\347\225:o\342 s>\244z(\324a\227\366\027\240#\020+\316\204W~%\037\364\250T\322e\224\314\225\n\2530\301Y\037a\216\271k\327N\271\203Cy\205\021\274\017\256% \005\274\322H\241h\t\204\366\022\232f\271+\273v\003\027FN\227Y\002\201\200\tL\372\326\345eHxC\265\037"
	.string	"\223,\267$\350\306}ZpE\222\310l\243\315\341\367)@\372?[GD9\301\350r\236z\016\332\252\240*\t\375T\223#\2527\205[\314\324\371\330\377\301a\r\275~\030$sm@r\361\223\tH\227l\204\220\250F\024\0019\021\345<A'2u$\355\241\331\022)\212(q\211\215\3120\260\001\304/\202\031\024Lp\034\270#.\350\220I\227\222\227kz\235\271\002\201\200\017\016\241v\366\241D\217\257|v\323\207\273\273\203\020\210\001\030\024\321\323uY$\252\365\026\245\351\235\321\314\356\364\025\331\305~'\351DI\006r\271\374\323\212\304,6}\022\233Z\252\334\205\356n\255T\263\364\3741\241\006:pW\f\363\225[>\350\375\032O\366x\223Fj\3271\264\204d\205\t8\211\222\224\034\277\342<*\340\377\231\243\360+1\3026\315`\277\235-t2\350\234\223n\273\221{\375\331\002\201\201"
	.ascii	"\242q%8\353*\3517\315\376D\316\220?R\207\204R\033\256\215\"\224"
	.ascii	"\3168\346\004\210v\205\232\323\024\t\345i\232\377X\222\002j}"
	.ascii	"|\036,\375\250\3122\024O\r\204\r7C\277\344]\022\310$\221'\215"
	.ascii	"F\331TS\347bq\250+qA\215u\370:\240a)F\246\345\202\372:\331\b"
	.ascii	"\372\374c\375k0\274\364N\236\214%\f\266U\347<\324N\013\375\213"
	.ascii	"\303\016\035\234DW\217\037\206\367\325\033\344\225"
	.align 32
	.type	test1024, @object
	.size	test1024, 608
test1024:
	.string	"0\202\002\\\002\001"
	.string	"\002\201\201"
	.string	"\334\230C\350=C[\344\005\315\320\251>\313\203u\366\265\245\237k\3514A)\030\372jUMp\374\354\256\2078\n \251\300EwnW`W\364\355\226\"\313\217\3413:\027\037\3557\245o\353\246\274\022\200\035S\275p\353!v>\311/\032E$\202\377\315Y2\006.\022;#x\355\022=\340\215\371gO7NG\002L-\300O\037\263\224\341A.-\220\020\374\202\221\213\017\"\324\362\374,\253SU\002\003\001"
	.string	"\001\002\201\200+\314?\217X\272\213"
	.string	"\026\366\352:\3600\320\005\027\332\260\353\232-O&\260\3268\301\353\365\330=\037p\367\177\364\342\317QQy\210\372\3502\016{-\227\362\372\272'\305\234\331\305\353\212yR<d4}\302\317(\307N\325C\013\321\246\312m\003-r#\274m\005\372\026\t/.\\\266\356t\335\322H\2166\f\006=M\345\020\202\353j\363K\237\326\355\021\261n\354\364\376\216u\224 /\313\254F\361\002A"
	.string	"\371\214\243\205\261\335)\257e\3013\363\225\305Rh\013\324\361\345\016\002\237O\372w\334F\236\307\246\344\026)\332\260\007\317[\251\022\212\335c\n\336.\214f\213\214\334\031\243~\364;\320\032\214\244\302\341\323\002A"
	.string	"\342L\005\362\004\206NaC\333\260\271\226\206R,\312\215{\253\013\023\r~8[\342.{\016\347\031\2318\347\362!\275\205\205\343\375(w 1q,\320\377\373.\257\205\264\206\312\363\273\312\252\017\2257\002@\016A\232\225\350\263Y\316Ka\3365\3548y\234\270\020RAc\253\202\256o"
	.string	"\251\364\336\335I\013~\270\245e\251\f\217\217\371\0375\306\222\270^\260f\253R@\300\2666j}\200F\004\002\345\237A\002A"
	.string	"\300\255\314N!\356\035$\221\373\247\200\215\232\266\263.\217\302\341\202\337i\030\264q\377\246e\336\355\204\215B\267\263!iV\034\007`Q)\004\3774\006\335\271g,|\004\223\016F\025\273*\267\033\347\207\002@x\332]\007Q\f\026z\237) \204\rB\372\327"
	.string	"\330w~\260\260k\326[S\270\233z\315\307+\270jc\251\373o\244r\277L]"
	.ascii	"\024\272\372Y\210\355\344\340\214\242\354\024~-\342\360FI\225"
	.ascii	"E"
	.align 32
	.type	test512, @object
	.size	test512, 318
test512:
	.string	"0\202\001:\002\001"
	.string	"\002A"
	.string	"\3263\271\310\373O<}\300\001\206\320\347\240U\362\225\223\314O\267[g[\224h\3114\025\336\245.\0343\302n\3744^q\023\267\326\356\330\245e\005r\207\250\260w\376W\365\374_U\203\207\335WI\002\003\001"
	.string	"\001\002A"
	.string	"\247\367\221\305\017\204W\334\007\367j\177`R\263r\361f\037}\227;\236\266\n\217\214\317B#"
	.string	"\004\324(\016\034\220\304\021%%\245\223\245/p\002\337\201\234I\003\240\370mT.&\336\252\205Y\2501\002!"
	.string	"\353G\327;\366\303\335ZF\305\271+\232\240\t\217\246\373\363xz3p\235\017Bk\023h$\323\025\002!"
	.string	"\351\020\260\263\r\342\202hw\212n|\332\274>S\203\373\326\"\347\265\256n\200\332"
	.ascii	"U\227\301\320e\002 L\370s\261jI)a\037F\020\r\363\307\347X\327"
	.ascii	"\210\025^\224\233\277{\242BXEA\f\313\001\002 \022\021\2721W\235"
	.ascii	"=\021\016[\214/_\342\002O\005G\214\025\216\263V?\270\373\255"
	.ascii	"\324\364\374\020\305\002 \030\241)\231[\331\310\324\374Iz*!,"
	.ascii	"I\344O\353\357Q\361\253m\373K\024\351KR\265\202,"
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC54:
	.long	1
	.long	1
	.long	1
	.long	1
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC110:
	.long	0
	.long	1086556160
	.align 8
.LC111:
	.long	0
	.long	1083129856
	.align 8
.LC145:
	.long	0
	.long	1072693248
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
