	.file	"engine.c"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"engine buffer"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"../deps/openssl/openssl/apps/engine.c"
	.text
	.p2align 4
	.type	append_buf, @function
append_buf:
.LFB1555:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movq	%rdx, %rdi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	call	strlen@PLT
	movq	(%r14), %r12
	movq	%rax, %rdx
	testq	%r12, %r12
	je	.L22
	leal	1(%rax), %ecx
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	movl	%ecx, -52(%rbp)
	call	strlen@PLT
	movl	(%r15), %esi
	movl	-52(%rbp), %ecx
	testl	%eax, %eax
	movq	-64(%rbp), %rdx
	movq	%rax, %rbx
	jle	.L4
	leal	2(%rcx,%rax), %eax
	cmpl	%esi, %eax
	jg	.L23
.L6:
	movslq	%ebx, %rbx
	movl	$8236, %eax
	addq	%r12, %rbx
	movw	%ax, (%rbx)
	leaq	2(%rbx), %r12
.L3:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	strcpy@PLT
	movl	$1, %eax
.L1:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	.cfi_restore_state
	cmpl	%esi, %ecx
	jle	.L3
	leal	511(%rdx), %esi
	addl	$256, %edx
	movq	%r12, %rdi
	movl	$63, %ecx
	cmovs	%esi, %edx
	xorb	%dl, %dl
	movl	%edx, (%r15)
	movslq	%edx, %rsi
	leaq	.LC1(%rip), %rdx
	call	CRYPTO_realloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L9
	movq	%r12, (%r14)
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L22:
	addl	$256, %edx
	leal	511(%rax), %edi
	leaq	.LC0(%rip), %rsi
	cmovns	%edx, %edi
	xorb	%dil, %dil
	movl	%edi, (%r15)
	call	app_malloc@PLT
	movq	%rax, (%r14)
	movq	%rax, %r12
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L23:
	leal	510(%rax), %esi
	addl	$255, %eax
	movq	%r12, %rdi
	movl	$63, %ecx
	cmovs	%esi, %eax
	leaq	.LC1(%rip), %rdx
	xorb	%al, %al
	movl	%eax, (%r15)
	movslq	%eax, %rsi
	call	CRYPTO_realloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L9
	movq	%r12, (%r14)
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L9:
	movq	(%r14), %rdi
	movl	$65, %edx
	leaq	.LC1(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, (%r14)
	xorl	%eax, %eax
	jmp	.L1
	.cfi_endproc
.LFE1555:
	.size	append_buf, .-append_buf
	.section	.rodata.str1.1
.LC2:
	.string	"STORE(%s)"
	.text
	.p2align 4
	.type	util_store_cap, @function
util_store_cap:
.LFB1559:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	subq	$272, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	OSSL_STORE_LOADER_get0_engine@PLT
	cmpq	%rax, (%rbx)
	je	.L29
.L24:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L30
	addq	$272, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	.cfi_restore_state
	movq	%r12, %rdi
	leaq	-288(%rbp), %r12
	call	OSSL_STORE_LOADER_get0_scheme@PLT
	leaq	.LC2(%rip), %rdx
	movl	$256, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	xorl	%eax, %eax
	call	BIO_snprintf@PLT
	movq	16(%rbx), %rsi
	movq	8(%rbx), %rdi
	movq	%r12, %rdx
	call	append_buf
	testl	%eax, %eax
	jne	.L24
	movl	$0, 24(%rbx)
	jmp	.L24
.L30:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1559:
	.size	util_store_cap, .-util_store_cap
	.section	.rodata.str1.8
	.align 8
.LC3:
	.string	"[Error]: internal stack error\n"
	.section	.rodata.str1.1
.LC4:
	.string	"[Success]: %s\n"
	.section	.rodata.str1.8
	.align 8
.LC5:
	.string	"[Error]: command name too long\n"
	.section	.rodata.str1.1
.LC6:
	.string	"[Failure]: %s\n"
	.text
	.p2align 4
	.type	util_do_cmds.isra.0, @function
util_do_cmds.isra.0:
.LFB1562:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$312, %rsp
	.cfi_offset 3, -56
	movq	%rdi, -352(%rbp)
	movq	%rsi, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	OPENSSL_sk_num@PLT
	movl	%eax, -340(%rbp)
	testl	%eax, %eax
	js	.L32
	movl	$0, %ebx
	je	.L31
	leaq	-320(%rbp), %r13
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L37:
	movq	%r15, %rdx
	leaq	.LC6(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	%r12, %rdi
	addl	$1, %ebx
	call	ERR_print_errors@PLT
	cmpl	%ebx, -340(%rbp)
	je	.L31
.L33:
	movl	%ebx, %esi
	movq	%r14, %rdi
	call	OPENSSL_sk_value@PLT
	movl	$58, %esi
	movq	%rax, %rdi
	movq	%rax, %r15
	call	strchr@PLT
	testq	%rax, %rax
	je	.L49
	movq	%rax, %r10
	subq	%r15, %r10
	cmpl	$254, %r10d
	jg	.L50
	movslq	%r10d, %rdx
	movl	$256, %ecx
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%r10, -328(%rbp)
	movq	%rax, -336(%rbp)
	call	__memcpy_chk@PLT
	movq	-336(%rbp), %r8
	movq	-328(%rbp), %r10
	xorl	%ecx, %ecx
	movq	-352(%rbp), %rdi
	movq	%r13, %rsi
	movb	$0, -320(%rbp,%r10)
	leaq	1(%r8), %rdx
	call	ENGINE_ctrl_cmd_string@PLT
	testl	%eax, %eax
	je	.L37
.L40:
	movq	%r15, %rdx
	leaq	.LC4(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	addl	$1, %ebx
	cmpl	%ebx, -340(%rbp)
	jne	.L33
.L31:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L51
	addq	$312, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L49:
	.cfi_restore_state
	movq	-352(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r15, %rsi
	call	ENGINE_ctrl_cmd_string@PLT
	testl	%eax, %eax
	je	.L37
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L50:
	leaq	.LC5(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L32:
	leaq	.LC3(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L31
.L51:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1562:
	.size	util_do_cmds.isra.0, .-util_do_cmds.isra.0
	.section	.rodata.str1.1
.LC7:
	.string	"<no description>"
.LC8:
	.string	"%s: Use -help for summary.\n"
	.section	.rodata.str1.8
	.align 8
.LC9:
	.string	"%s: Cannot mix flags and engine names.\n"
	.section	.rodata.str1.1
.LC10:
	.string	"(%s) %s\n"
.LC11:
	.string	"Loaded: (%s) %s\n"
.LC12:
	.string	"RSA"
.LC13:
	.string	"DSA"
.LC14:
	.string	"DH"
.LC15:
	.string	"RAND"
.LC16:
	.string	" [%s]\n"
.LC17:
	.string	"     "
.LC18:
	.string	"%s"
.LC19:
	.string	"[ available ]\n"
.LC20:
	.string	"[ unavailable ]\n"
.LC21:
	.string	"name buffer"
.LC22:
	.string	"description buffer"
.LC23:
	.string	", "
.LC24:
	.string	"\n"
.LC25:
	.string	"%s: %s\n"
.LC26:
	.string	"%s%s(input flags): "
.LC27:
	.string	"<no flags>\n"
.LC28:
	.string	"[Internal] "
.LC29:
	.string	"NUMERIC"
.LC30:
	.string	"|"
.LC31:
	.string	"STRING"
.LC32:
	.string	"NO_INPUT"
.LC33:
	.string	"<0x%04X>"
.LC34:
	.string	"  <illegal flags!>"
	.text
	.p2align 4
	.globl	engine_main
	.type	engine_main, @function
engine_main:
.LFB1560:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edi, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, %r15
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, %rbx
	movq	%rax, -136(%rbp)
	call	OPENSSL_sk_new_null@PLT
	movl	$32769, %edi
	movq	%rax, -144(%rbp)
	movq	%rax, %r12
	call	dup_bio_out@PLT
	testq	%r15, %r15
	sete	%dl
	testq	%rbx, %rbx
	movq	%rax, -160(%rbp)
	sete	%al
	orb	%al, %dl
	jne	.L252
	testq	%r12, %r12
	je	.L252
	movq	8(%r14), %rsi
	movq	(%r14), %r12
	testq	%rsi, %rsi
	jne	.L54
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L56:
	movq	%r15, %rdi
	addq	$8, %r14
	subl	$1, %r13d
	call	OPENSSL_sk_push@PLT
	movq	8(%r14), %rsi
	testq	%rsi, %rsi
	je	.L55
.L54:
	cmpb	$45, (%rsi)
	jne	.L56
.L55:
	movq	%r12, (%r14)
	movq	%r14, %rsi
	leaq	engine_options(%rip), %rdx
	movl	%r13d, %edi
	xorl	%r14d, %r14d
	leaq	.L61(%rip), %rbx
	call	opt_init@PLT
	movl	$0, -168(%rbp)
	movl	$0, -164(%rbp)
	movl	$0, -152(%rbp)
.L57:
	call	opt_next@PLT
	testl	%eax, %eax
	je	.L255
	cmpl	$6, %eax
	jg	.L58
	cmpl	$-1, %eax
	jl	.L57
	addl	$1, %eax
	cmpl	$7, %eax
	ja	.L57
	movslq	(%rbx,%rax,4), %rax
	addq	%rbx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L61:
	.long	.L251-.L61
	.long	.L57-.L61
	.long	.L66-.L61
	.long	.L65-.L61
	.long	.L64-.L61
	.long	.L63-.L61
	.long	.L62-.L61
	.long	.L60-.L61
	.text
.L65:
	movl	$1, %r14d
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L58:
	leal	-100(%rax), %edx
	cmpl	$3, %edx
	ja	.L57
	movl	-152(%rbp), %ecx
	subl	$99, %eax
	cmpl	%eax, %ecx
	cmovge	%ecx, %eax
	movl	%eax, -152(%rbp)
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L255:
	call	opt_num_rest@PLT
	call	opt_rest@PLT
	movq	(%rax), %rsi
	movq	%rax, %rbx
	testq	%rsi, %rsi
	jne	.L70
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L73:
	movq	%r15, %rdi
	addq	$8, %rbx
	call	OPENSSL_sk_push@PLT
	movq	(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L74
.L70:
	cmpb	$45, (%rsi)
	jne	.L73
	movq	bio_err(%rip), %rdi
	movq	%r12, %rdx
	leaq	.LC9(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L251:
	movq	bio_err(%rip), %rdi
	movq	%r12, %rdx
	leaq	.LC8(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L252:
	movl	$1, -148(%rbp)
.L53:
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	movq	%r15, %rdi
	call	OPENSSL_sk_free@PLT
	movq	-136(%rbp), %rdi
	call	OPENSSL_sk_free@PLT
	movq	-144(%rbp), %rdi
	call	OPENSSL_sk_free@PLT
	movq	-160(%rbp), %rdi
	call	BIO_free_all@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L256
	movl	-148(%rbp), %eax
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L63:
	.cfi_restore_state
	addl	$1, -168(%rbp)
.L64:
	addl	$1, -164(%rbp)
	jmp	.L57
.L66:
	leaq	engine_options(%rip), %rdi
	call	opt_help@PLT
	movl	$0, -148(%rbp)
	jmp	.L53
.L60:
	call	opt_arg@PLT
	movq	-144(%rbp), %rdi
	movq	%rax, %rsi
	call	OPENSSL_sk_push@PLT
	jmp	.L57
.L62:
	call	opt_arg@PLT
	movq	-136(%rbp), %rdi
	movq	%rax, %rsi
	call	OPENSSL_sk_push@PLT
	jmp	.L57
.L74:
	movq	%r15, %rdi
	call	OPENSSL_sk_num@PLT
	testl	%eax, %eax
	je	.L257
.L72:
	movl	$0, -172(%rbp)
	movl	$0, -148(%rbp)
	movq	%r15, -184(%rbp)
	movl	%r14d, -200(%rbp)
	movq	-160(%rbp), %r14
	.p2align 4,,10
	.p2align 3
.L75:
	movq	-184(%rbp), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, -172(%rbp)
	jge	.L244
	movl	-172(%rbp), %esi
	movq	-184(%rbp), %rdi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %rdi
	movq	%rax, %r13
	call	ENGINE_by_id@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L77
	movq	%rax, %rdi
	call	ENGINE_get_name@PLT
	movq	%r13, %rdx
	leaq	.LC10(%rip), %rsi
	movq	%r14, %rdi
	movq	%rax, %rcx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	-136(%rbp), %rsi
	movq	%r14, %rdx
	movq	%r12, %rdi
	call	util_do_cmds.isra.0
	movq	%r12, %rdi
	call	ENGINE_get_id@PLT
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L258
.L78:
	movl	-200(%rbp), %ecx
	testl	%ecx, %ecx
	jne	.L259
.L79:
	movl	-164(%rbp), %eax
	testl	%eax, %eax
	jne	.L260
.L102:
	movl	-152(%rbp), %eax
	testl	%eax, %eax
	je	.L136
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$10, %esi
	movq	%r12, %rdi
	call	ENGINE_ctrl@PLT
	testl	%eax, %eax
	je	.L136
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	movq	%r12, %rdi
	call	ENGINE_ctrl@PLT
	movslq	%eax, %r13
	testl	%r13d, %r13d
	jle	.L136
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, -208(%rbp)
	testq	%rax, %rax
	je	.L247
	movl	$0, -192(%rbp)
	movq	%r14, %rbx
	jmp	.L132
	.p2align 4,,10
	.p2align 3
.L262:
	testl	%eax, %eax
	je	.L149
	xorl	%r14d, %r14d
	xorl	%r15d, %r15d
.L107:
	movl	$213, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r15, %rdi
	call	CRYPTO_free@PLT
	movl	$215, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r14, %rdi
	call	CRYPTO_free@PLT
	movq	%r13, %rdx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$12, %esi
	movq	%r12, %rdi
	call	ENGINE_ctrl@PLT
	movslq	%eax, %r13
	testl	%r13d, %r13d
	jle	.L261
.L132:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movl	$18, %esi
	movq	%r12, %rdi
	call	ENGINE_ctrl@PLT
	movl	%eax, -176(%rbp)
	testl	%eax, %eax
	js	.L247
	andl	$8, %eax
	cmpl	$4, -152(%rbp)
	movl	%eax, -196(%rbp)
	jne	.L262
.L149:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movl	$14, %esi
	movq	%r12, %rdi
	call	ENGINE_ctrl@PLT
	testl	%eax, %eax
	jle	.L247
	leal	1(%rax), %edi
	leaq	.LC21(%rip), %rsi
	call	app_malloc@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rdx
	movl	$15, %esi
	movq	%rax, %rcx
	movq	%r12, %rdi
	movq	%rax, %r15
	call	ENGINE_ctrl@PLT
	testl	%eax, %eax
	jle	.L144
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movl	$16, %esi
	movq	%r12, %rdi
	call	ENGINE_ctrl@PLT
	testl	%eax, %eax
	js	.L144
	movl	$0, %r14d
	jne	.L263
.L110:
	movl	-192(%rbp), %eax
	testl	%eax, %eax
	jne	.L111
	leaq	.LC17(%rip), %rsi
	movq	%rbx, %rdi
	call	BIO_puts@PLT
	movl	%eax, %ecx
.L112:
	cmpl	$1, -152(%rbp)
	je	.L264
	testq	%r14, %r14
	leaq	.LC7(%rip), %rcx
	movq	%r15, %rdx
	movq	%rbx, %rdi
	cmovne	%r14, %rcx
	leaq	.LC25(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	cmpl	$2, -152(%rbp)
	movl	$0, -192(%rbp)
	je	.L107
	leaq	.LC17(%rip), %rcx
	leaq	.LC26(%rip), %rsi
	movq	%rbx, %rdi
	xorl	%eax, %eax
	movq	%rcx, %rdx
	call	BIO_printf@PLT
	movl	-176(%rbp), %eax
	testl	%eax, %eax
	je	.L265
	movl	-196(%rbp), %r11d
	testl	%r11d, %r11d
	jne	.L266
.L117:
	movl	-176(%rbp), %eax
	movl	%eax, %ecx
	movl	%eax, %edx
	andl	$4, %ecx
	andl	$2, %edx
	movl	%ecx, -176(%rbp)
	movl	%eax, %ecx
	andl	$-16, %ecx
	movl	%ecx, -192(%rbp)
	testb	$1, %al
	jne	.L118
	testl	%edx, %edx
	je	.L267
	movl	$0, -196(%rbp)
.L119:
	leaq	.LC31(%rip), %rsi
	movq	%rbx, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movl	-176(%rbp), %r9d
	testl	%r9d, %r9d
	jne	.L122
	movl	-192(%rbp), %eax
	testl	%eax, %eax
	je	.L139
	leaq	.LC30(%rip), %rsi
	movq	%rbx, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movl	-192(%rbp), %edx
	movq	%rbx, %rdi
	xorl	%eax, %eax
	leaq	.LC33(%rip), %rsi
	call	BIO_printf@PLT
.L139:
	movl	-196(%rbp), %esi
	testl	%esi, %esi
	jne	.L128
.L131:
	leaq	.LC24(%rip), %rsi
	movq	%rbx, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movl	$0, -192(%rbp)
	jmp	.L107
.L261:
	movl	-192(%rbp), %ecx
	movq	%rbx, %r14
	testl	%ecx, %ecx
	jle	.L133
	leaq	.LC24(%rip), %rsi
	movq	%rbx, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L133:
	movq	-208(%rbp), %rdi
	call	OPENSSL_sk_free@PLT
	movl	$225, %edx
	leaq	.LC1(%rip), %rsi
	xorl	%edi, %edi
	call	CRYPTO_free@PLT
	movl	$226, %edx
	leaq	.LC1(%rip), %rsi
	xorl	%edi, %edi
	call	CRYPTO_free@PLT
	.p2align 4,,10
	.p2align 3
.L136:
	movq	%r12, %rdi
	call	ENGINE_free@PLT
.L134:
	addl	$1, -172(%rbp)
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L77:
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	addl	$1, -148(%rbp)
	movl	-148(%rbp), %ecx
	movl	$127, %eax
	cmpl	$127, %ecx
	cmovle	%ecx, %eax
	movl	%eax, -148(%rbp)
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L111:
	leaq	.LC23(%rip), %rsi
	movq	%rbx, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movl	-192(%rbp), %ecx
	addl	%eax, %ecx
	jmp	.L112
.L258:
	movq	%r12, %rdi
	call	ENGINE_get_name@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	ENGINE_get_id@PLT
	movq	%r13, %rcx
	leaq	.LC11(%rip), %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L78
.L259:
	movq	%r12, %rdi
	movl	$256, -116(%rbp)
	leaq	-116(%rbp), %rbx
	leaq	-112(%rbp), %r13
	movq	$0, -112(%rbp)
	call	ENGINE_get_RSA@PLT
	testq	%rax, %rax
	je	.L84
	leaq	.LC12(%rip), %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	append_buf
	testl	%eax, %eax
	je	.L244
.L84:
	movq	%r12, %rdi
	call	ENGINE_get_DSA@PLT
	testq	%rax, %rax
	je	.L82
	leaq	.LC13(%rip), %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	append_buf
	testl	%eax, %eax
	je	.L244
.L82:
	movq	%r12, %rdi
	call	ENGINE_get_DH@PLT
	testq	%rax, %rax
	je	.L86
	leaq	.LC14(%rip), %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	append_buf
	testl	%eax, %eax
	je	.L244
.L86:
	movq	%r12, %rdi
	call	ENGINE_get_RAND@PLT
	testq	%rax, %rax
	je	.L88
	leaq	.LC15(%rip), %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	append_buf
	testl	%eax, %eax
	je	.L244
.L88:
	movq	%r12, %rdi
	call	ENGINE_get_ciphers@PLT
	testq	%rax, %rax
	je	.L89
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	leaq	-104(%rbp), %rdx
	movq	%r12, %rdi
	call	*%rax
	testl	%eax, %eax
	jle	.L89
	subl	$1, %eax
	xorl	%r15d, %r15d
	movq	%r12, -192(%rbp)
	leaq	4(,%rax,4), %rax
	movq	%r15, %r12
	movq	%rax, %r15
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L269:
	addq	$4, %r12
	cmpq	%r15, %r12
	je	.L268
.L93:
	movq	-104(%rbp), %rax
	movl	(%rax,%r12), %edi
	call	OBJ_nid2sn@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	append_buf
	testl	%eax, %eax
	jne	.L269
.L244:
	movq	-184(%rbp), %r15
	jmp	.L53
.L264:
	cmpl	$5, %ecx
	jle	.L114
	movq	%r15, %rdi
	movl	%ecx, -192(%rbp)
	call	strlen@PLT
	movl	-192(%rbp), %ecx
	addl	%ecx, %eax
	cmpl	$78, %eax
	jg	.L270
.L114:
	movq	%r15, %rdx
	leaq	.LC18(%rip), %rsi
	movq	%rbx, %rdi
	xorl	%eax, %eax
	movl	%ecx, -192(%rbp)
	call	BIO_printf@PLT
	movl	-192(%rbp), %ecx
	addl	%ecx, %eax
	movl	%eax, -192(%rbp)
	jmp	.L107
.L263:
	leal	1(%rax), %edi
	leaq	.LC22(%rip), %rsi
	call	app_malloc@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rdx
	movl	$17, %esi
	movq	%rax, %rcx
	movq	%r12, %rdi
	movq	%rax, %r14
	call	ENGINE_ctrl@PLT
	testl	%eax, %eax
	jg	.L110
	movq	%r15, %rbx
	movq	-184(%rbp), %r15
.L109:
	movq	-208(%rbp), %rdi
	call	OPENSSL_sk_free@PLT
	movq	%rbx, %rdi
	movl	$225, %edx
	leaq	.LC1(%rip), %rsi
	call	CRYPTO_free@PLT
	movl	$226, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r14, %rdi
	call	CRYPTO_free@PLT
	jmp	.L53
.L260:
	movq	%r14, %rdi
	xorl	%eax, %eax
	leaq	.LC17(%rip), %rdx
	leaq	.LC18(%rip), %rsi
	call	BIO_printf@PLT
	movq	%r12, %rdi
	call	ENGINE_init@PLT
	testl	%eax, %eax
	je	.L103
	movq	%r14, %rdi
	leaq	.LC19(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	-144(%rbp), %rsi
	movq	%r12, %rdi
	movq	%r14, %rdx
	call	util_do_cmds.isra.0
	movq	%r12, %rdi
	call	ENGINE_finish@PLT
	jmp	.L102
.L268:
	movq	-192(%rbp), %r12
.L89:
	movq	%r12, %rdi
	call	ENGINE_get_digests@PLT
	testq	%rax, %rax
	je	.L91
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	leaq	-104(%rbp), %rdx
	movq	%r12, %rdi
	call	*%rax
	testl	%eax, %eax
	jle	.L91
	subl	$1, %eax
	movq	%r12, -192(%rbp)
	xorl	%r15d, %r15d
	leaq	4(,%rax,4), %rax
	movq	%rax, %r12
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L272:
	addq	$4, %r15
	cmpq	%r15, %r12
	je	.L271
.L96:
	movq	-104(%rbp), %rax
	movl	(%rax,%r15), %edi
	call	OBJ_nid2sn@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	append_buf
	testl	%eax, %eax
	jne	.L272
	jmp	.L244
.L271:
	movq	-192(%rbp), %r12
.L91:
	movq	%r12, %rdi
	call	ENGINE_get_pkey_meths@PLT
	testq	%rax, %rax
	je	.L94
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	leaq	-104(%rbp), %rdx
	movq	%r12, %rdi
	call	*%rax
	testl	%eax, %eax
	jle	.L94
	subl	$1, %eax
	movq	%r12, -192(%rbp)
	xorl	%r15d, %r15d
	leaq	4(,%rax,4), %rax
	movq	%rax, %r12
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L274:
	addq	$4, %r15
	cmpq	%r12, %r15
	je	.L273
.L99:
	movq	-104(%rbp), %rax
	movl	(%rax,%r15), %edi
	call	OBJ_nid2sn@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	append_buf
	testl	%eax, %eax
	jne	.L274
	jmp	.L244
.L103:
	leaq	.LC20(%rip), %rsi
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movl	-168(%rbp), %eax
	testl	%eax, %eax
	jne	.L275
.L104:
	call	ERR_clear_error@PLT
	jmp	.L102
.L273:
	movq	-192(%rbp), %r12
.L94:
	leaq	-96(%rbp), %rsi
	leaq	util_store_cap(%rip), %rdi
	movq	%r12, -96(%rbp)
	movq	%r13, -88(%rbp)
	movq	%rbx, -80(%rbp)
	movl	$1, -72(%rbp)
	call	OSSL_STORE_do_all_loaders@PLT
	movl	-72(%rbp), %edx
	testl	%edx, %edx
	je	.L244
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L101
	cmpb	$0, (%rdi)
	jne	.L276
.L101:
	movl	$450, %edx
	leaq	.LC1(%rip), %rsi
	call	CRYPTO_free@PLT
	jmp	.L79
.L257:
	call	ENGINE_get_first@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L72
	.p2align 4,,10
	.p2align 3
.L76:
	movq	%r12, %rdi
	call	ENGINE_get_id@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	OPENSSL_sk_push@PLT
	movq	%r12, %rdi
	call	ENGINE_get_next@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L76
	jmp	.L72
.L247:
	movq	-184(%rbp), %r15
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L109
.L267:
	movl	-176(%rbp), %r8d
	testl	%r8d, %r8d
	je	.L277
	leaq	.LC32(%rip), %rsi
	movq	%rbx, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L254:
	movl	-192(%rbp), %edx
	testl	%edx, %edx
	je	.L131
	movq	%rbx, %rdi
	leaq	.LC30(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movl	-192(%rbp), %edx
	movq	%rbx, %rdi
	xorl	%eax, %eax
	leaq	.LC33(%rip), %rsi
	call	BIO_printf@PLT
	jmp	.L131
.L275:
	movq	stdout(%rip), %rdi
	call	ERR_print_errors_fp@PLT
	jmp	.L104
.L118:
	leaq	.LC29(%rip), %rsi
	movq	%rbx, %rdi
	xorl	%eax, %eax
	movl	%edx, -196(%rbp)
	call	BIO_printf@PLT
	movl	-196(%rbp), %edx
	testl	%edx, %edx
	jne	.L121
	movl	-176(%rbp), %r10d
	testl	%r10d, %r10d
	je	.L254
.L122:
	leaq	.LC30(%rip), %rsi
	movq	%rbx, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	%rbx, %rdi
	leaq	.LC32(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movl	-192(%rbp), %edi
	testl	%edi, %edi
	je	.L128
	leaq	.LC30(%rip), %rsi
	movq	%rbx, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movl	-192(%rbp), %edx
	movq	%rbx, %rdi
	xorl	%eax, %eax
	leaq	.LC33(%rip), %rsi
	call	BIO_printf@PLT
.L128:
	leaq	.LC34(%rip), %rsi
	movq	%rbx, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L131
.L266:
	leaq	.LC28(%rip), %rsi
	movq	%rbx, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L117
.L270:
	leaq	.LC24(%rip), %rsi
	movq	%rbx, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	leaq	.LC17(%rip), %rsi
	movq	%rbx, %rdi
	call	BIO_puts@PLT
	movl	%eax, %ecx
	jmp	.L114
.L144:
	movq	%r15, %rbx
	xorl	%r14d, %r14d
	movq	-184(%rbp), %r15
	jmp	.L109
.L265:
	leaq	.LC27(%rip), %rsi
	movq	%rbx, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L107
.L121:
	leaq	.LC30(%rip), %rsi
	movq	%rbx, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movl	$1, -196(%rbp)
	jmp	.L119
.L277:
	testl	%ecx, %ecx
	je	.L131
	movl	%ecx, %edx
	leaq	.LC33(%rip), %rsi
	movq	%rbx, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L131
.L276:
	movq	%rdi, %rdx
	leaq	.LC16(%rip), %rsi
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	-112(%rbp), %rdi
	jmp	.L101
.L256:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1560:
	.size	engine_main, .-engine_main
	.globl	engine_options
	.section	.rodata.str1.8
	.align 8
.LC35:
	.string	"Usage: %s [options] engine...\n"
	.section	.rodata.str1.1
.LC36:
	.string	"  engine... Engines to load\n"
.LC37:
	.string	"help"
.LC38:
	.string	"Display this summary"
.LC39:
	.string	"v"
	.section	.rodata.str1.8
	.align 8
.LC40:
	.string	"List 'control commands' For each specified engine"
	.section	.rodata.str1.1
.LC41:
	.string	"vv"
	.section	.rodata.str1.8
	.align 8
.LC42:
	.string	"Also display each command's description"
	.section	.rodata.str1.1
.LC43:
	.string	"vvv"
	.section	.rodata.str1.8
	.align 8
.LC44:
	.string	"Also add the input flags for each command"
	.section	.rodata.str1.1
.LC45:
	.string	"vvvv"
	.section	.rodata.str1.8
	.align 8
.LC46:
	.string	"Also show internal input flags"
	.section	.rodata.str1.1
.LC47:
	.string	"c"
	.section	.rodata.str1.8
	.align 8
.LC48:
	.string	"List the capabilities of specified engine"
	.section	.rodata.str1.1
.LC49:
	.string	"t"
	.section	.rodata.str1.8
	.align 8
.LC50:
	.string	"Check that specified engine is available"
	.section	.rodata.str1.1
.LC51:
	.string	"tt"
	.section	.rodata.str1.8
	.align 8
.LC52:
	.string	"Display error trace for unavailable engines"
	.section	.rodata.str1.1
.LC53:
	.string	"pre"
	.section	.rodata.str1.8
	.align 8
.LC54:
	.string	"Run command against the ENGINE before loading it"
	.section	.rodata.str1.1
.LC55:
	.string	"post"
	.section	.rodata.str1.8
	.align 8
.LC56:
	.string	"Run command against the ENGINE after loading it"
	.align 8
.LC57:
	.string	"Commands are like \"SO_PATH:/lib/libdriver.so\""
	.section	.data.rel.ro,"aw"
	.align 32
	.type	engine_options, @object
	.size	engine_options, 336
engine_options:
	.quad	OPT_HELP_STR
	.long	1
	.long	45
	.quad	.LC35
	.quad	OPT_HELP_STR
	.long	1
	.long	45
	.quad	.LC36
	.quad	.LC37
	.long	1
	.long	45
	.quad	.LC38
	.quad	.LC39
	.long	100
	.long	45
	.quad	.LC40
	.quad	.LC41
	.long	101
	.long	45
	.quad	.LC42
	.quad	.LC43
	.long	102
	.long	45
	.quad	.LC44
	.quad	.LC45
	.long	103
	.long	45
	.quad	.LC46
	.quad	.LC47
	.long	2
	.long	45
	.quad	.LC48
	.quad	.LC49
	.long	3
	.long	45
	.quad	.LC50
	.quad	.LC51
	.long	4
	.long	45
	.quad	.LC52
	.quad	.LC53
	.long	5
	.long	115
	.quad	.LC54
	.quad	.LC55
	.long	6
	.long	115
	.quad	.LC56
	.quad	OPT_MORE_STR
	.long	0
	.long	1
	.quad	.LC57
	.quad	0
	.zero	16
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
