	.file	"version.c"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"%s: Use -help for summary.\n"
.LC1:
	.string	"Extra parameters given.\n"
.LC2:
	.string	"OpenSSL 1.1.1g  21 Apr 2020"
.LC3:
	.string	"%s (Library: %s)\n"
.LC4:
	.string	"options:  "
.LC5:
	.string	"%s "
.LC6:
	.string	"Seeding source:"
.LC7:
	.string	" os-specific"
	.text
	.p2align 4
	.globl	version_main
	.type	version_main, @function
version_main:
.LFB1435:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	version_options(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	leaq	.L5(%rip), %r14
	pushq	%r13
	.cfi_offset 13, -40
	xorl	%r13d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	xorl	%ebx, %ebx
	subq	$40, %rsp
	call	opt_init@PLT
	movl	$0, -68(%rbp)
	movq	%rax, -80(%rbp)
	movl	$0, -64(%rbp)
	movl	$0, -60(%rbp)
	movl	$0, -56(%rbp)
	movl	$0, -52(%rbp)
.L2:
	call	opt_next@PLT
	testl	%eax, %eax
	je	.L53
.L17:
	addl	$1, %eax
	cmpl	$11, %eax
	ja	.L2
	movslq	(%r14,%rax,4), %rax
	addq	%r14, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L5:
	.long	.L15-.L5
	.long	.L2-.L5
	.long	.L14-.L5
	.long	.L13-.L5
	.long	.L12-.L5
	.long	.L11-.L5
	.long	.L10-.L5
	.long	.L9-.L5
	.long	.L8-.L5
	.long	.L7-.L5
	.long	.L6-.L5
	.long	.L4-.L5
	.text
.L13:
	movl	$1, %r12d
	movl	$1, %ebx
	call	opt_next@PLT
	testl	%eax, %eax
	jne	.L17
.L53:
	call	opt_num_rest@PLT
	testl	%eax, %eax
	jne	.L54
	movzbl	-56(%rbp), %eax
	xorl	$1, %ebx
	orb	%bl, %al
	je	.L19
	call	OpenSSL_version_num@PLT
	xorl	%edi, %edi
	cmpq	$269488255, %rax
	je	.L55
	call	OpenSSL_version@PLT
	leaq	.LC2(%rip), %rdx
	movl	$1, %edi
	leaq	.LC3(%rip), %rsi
	movq	%rax, %rcx
	xorl	%eax, %eax
	call	__printf_chk@PLT
.L19:
	testl	%r12d, %r12d
	jne	.L56
.L21:
	testl	%r15d, %r15d
	jne	.L57
.L22:
	movl	-60(%rbp), %esi
	testl	%esi, %esi
	jne	.L58
.L23:
	movl	-52(%rbp), %ecx
	testl	%ecx, %ecx
	jne	.L59
.L24:
	movl	-64(%rbp), %edx
	testl	%edx, %edx
	jne	.L60
.L25:
	movl	-68(%rbp), %eax
	testl	%eax, %eax
	jne	.L61
.L26:
	testl	%r13d, %r13d
	je	.L1
.L16:
	endbr64
	leaq	.LC6(%rip), %rsi
	movl	$1, %edi
	xorl	%eax, %eax
	xorl	%r13d, %r13d
	call	__printf_chk@PLT
	leaq	.LC7(%rip), %rsi
	movl	$1, %edi
	xorl	%eax, %eax
	call	__printf_chk@PLT
	movl	$10, %edi
	call	putchar@PLT
	jmp	.L1
.L54:
	movq	bio_err(%rip), %rdi
	leaq	.LC1(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L15:
	movq	-80(%rbp), %rdx
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
	movl	$1, %r13d
	leaq	.LC0(%rip), %rsi
	call	BIO_printf@PLT
.L1:
	addq	$40, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L4:
	.cfi_restore_state
	movl	$1, %r13d
	movl	$1, %ebx
	jmp	.L2
.L6:
	movl	$1, -68(%rbp)
	movl	$1, %r15d
	movl	$1, %r12d
	movl	$1, %r13d
	movl	$1, -64(%rbp)
	movl	$1, -60(%rbp)
	movl	$1, -56(%rbp)
	movl	$1, -52(%rbp)
	jmp	.L2
.L7:
	movl	$1, -56(%rbp)
	movl	$1, %ebx
	jmp	.L2
.L8:
	movl	$1, %r15d
	movl	$1, %ebx
	jmp	.L2
.L9:
	movl	$1, -60(%rbp)
	movl	$1, %ebx
	jmp	.L2
.L10:
	movl	$1, -52(%rbp)
	movl	$1, %ebx
	jmp	.L2
.L11:
	movl	$1, -68(%rbp)
	movl	$1, %ebx
	jmp	.L2
.L12:
	movl	$1, -64(%rbp)
	movl	$1, %ebx
	jmp	.L2
.L14:
	leaq	version_options(%rip), %rdi
	xorl	%r13d, %r13d
	call	opt_help@PLT
	jmp	.L1
.L61:
	movl	$5, %edi
	call	OpenSSL_version@PLT
	movq	%rax, %rdi
	call	puts@PLT
	jmp	.L26
.L60:
	movl	$4, %edi
	call	OpenSSL_version@PLT
	movq	%rax, %rdi
	call	puts@PLT
	jmp	.L25
.L59:
	movl	$1, %edi
	call	OpenSSL_version@PLT
	movq	%rax, %rdi
	call	puts@PLT
	jmp	.L24
.L58:
	leaq	.LC4(%rip), %rsi
	movl	$1, %edi
	xorl	%eax, %eax
	call	__printf_chk@PLT
	call	BN_options@PLT
	leaq	.LC5(%rip), %rsi
	movl	$1, %edi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	__printf_chk@PLT
	call	RC4_options@PLT
	leaq	.LC5(%rip), %rsi
	movl	$1, %edi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	__printf_chk@PLT
	call	DES_options@PLT
	leaq	.LC5(%rip), %rsi
	movl	$1, %edi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	__printf_chk@PLT
	call	IDEA_options@PLT
	leaq	.LC5(%rip), %rsi
	movl	$1, %edi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	__printf_chk@PLT
	call	BF_options@PLT
	leaq	.LC5(%rip), %rsi
	movl	$1, %edi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	__printf_chk@PLT
	movl	$10, %edi
	call	putchar@PLT
	jmp	.L23
.L57:
	movl	$3, %edi
	call	OpenSSL_version@PLT
	movq	%rax, %rdi
	call	puts@PLT
	jmp	.L22
.L56:
	movl	$2, %edi
	call	OpenSSL_version@PLT
	movq	%rax, %rdi
	call	puts@PLT
	jmp	.L21
.L55:
	call	OpenSSL_version@PLT
	movq	%rax, %rdi
	call	puts@PLT
	jmp	.L19
	.cfi_endproc
.LFE1435:
	.size	version_main, .-version_main
	.globl	version_options
	.section	.rodata.str1.1
.LC8:
	.string	"help"
.LC9:
	.string	"Display this summary"
.LC10:
	.string	"a"
.LC11:
	.string	"Show all data"
.LC12:
	.string	"b"
.LC13:
	.string	"Show build date"
.LC14:
	.string	"d"
.LC15:
	.string	"Show configuration directory"
.LC16:
	.string	"e"
.LC17:
	.string	"Show engines directory"
.LC18:
	.string	"f"
.LC19:
	.string	"Show compiler flags used"
.LC20:
	.string	"o"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC21:
	.string	"Show some internal datatype options"
	.section	.rodata.str1.1
.LC22:
	.string	"p"
.LC23:
	.string	"Show target build platform"
.LC24:
	.string	"r"
.LC25:
	.string	"Show random seeding options"
.LC26:
	.string	"v"
.LC27:
	.string	"Show library version"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	version_options, @object
	.size	version_options, 264
version_options:
	.quad	.LC8
	.long	1
	.long	45
	.quad	.LC9
	.quad	.LC10
	.long	9
	.long	45
	.quad	.LC11
	.quad	.LC12
	.long	2
	.long	45
	.quad	.LC13
	.quad	.LC14
	.long	3
	.long	45
	.quad	.LC15
	.quad	.LC16
	.long	4
	.long	45
	.quad	.LC17
	.quad	.LC18
	.long	5
	.long	45
	.quad	.LC19
	.quad	.LC20
	.long	6
	.long	45
	.quad	.LC21
	.quad	.LC22
	.long	7
	.long	45
	.quad	.LC23
	.quad	.LC24
	.long	10
	.long	45
	.quad	.LC25
	.quad	.LC26
	.long	8
	.long	45
	.quad	.LC27
	.quad	0
	.zero	16
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
