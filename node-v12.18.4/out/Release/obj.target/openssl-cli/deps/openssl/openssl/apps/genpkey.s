	.file	"genpkey.c"
	.text
	.p2align 4
	.type	genpkey_cb, @function
genpkey_cb:
.LFB1438:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	subq	$16, %rsp
	.cfi_offset 12, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movb	$42, -25(%rbp)
	call	EVP_PKEY_CTX_get_app_data@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	movq	%rax, %r12
	call	EVP_PKEY_CTX_get_keygen_info@PLT
	testl	%eax, %eax
	jne	.L2
	movb	$46, -25(%rbp)
.L3:
	leaq	-25(%rbp), %rsi
	movl	$1, %edx
	movq	%r12, %rdi
	call	BIO_write@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	movq	%r12, %rdi
	call	BIO_ctrl@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L12
	addq	$16, %rsp
	movl	$1, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	.cfi_restore_state
	cmpl	$1, %eax
	jne	.L13
	movb	$43, -25(%rbp)
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L13:
	cmpl	$2, %eax
	jne	.L14
	movb	$42, -25(%rbp)
	jmp	.L3
.L14:
	cmpl	$3, %eax
	jne	.L3
	movb	$10, -25(%rbp)
	jmp	.L3
.L12:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1438:
	.size	genpkey_cb, .-genpkey_cb
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"Algorithm already set!\n"
.LC1:
	.string	"Algorithm %s not found\n"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"Error initializing %s context\n"
	.text
	.p2align 4
	.globl	init_gen_str
	.type	init_gen_str, @function
init_gen_str:
.LFB1437:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	$0, (%rdi)
	movq	$0, -64(%rbp)
	je	.L16
	movq	bio_err(%rip), %rdi
	leaq	.LC0(%rip), %rsi
	call	BIO_puts@PLT
	xorl	%eax, %eax
.L15:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L34
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	.cfi_restore_state
	movq	%rdi, %rbx
	movq	%rdx, %r13
	leaq	-64(%rbp), %rdi
	movl	$-1, %edx
	movq	%rsi, %r12
	movl	%ecx, %r14d
	call	EVP_PKEY_asn1_find_str@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	jne	.L19
	testq	%r13, %r13
	jne	.L35
	testq	%r15, %r15
	je	.L36
.L19:
	call	ERR_clear_error@PLT
	movq	%r15, %r9
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	leaq	-68(%rbp), %rdi
	call	EVP_PKEY_asn1_get0_info@PLT
	movq	-64(%rbp), %rdi
	call	ENGINE_finish@PLT
	movl	-68(%rbp), %edi
	movq	%r13, %rsi
	call	EVP_PKEY_CTX_new_id@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L20
	movq	%rax, %rdi
	testl	%r14d, %r14d
	jne	.L37
	call	EVP_PKEY_keygen_init@PLT
	testl	%eax, %eax
	jle	.L20
.L22:
	movq	%r13, (%rbx)
	movl	$1, %eax
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L37:
	call	EVP_PKEY_paramgen_init@PLT
	testl	%eax, %eax
	jg	.L22
.L20:
	movq	bio_err(%rip), %rdi
	movq	%r12, %rdx
	leaq	.LC2(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	movq	%r13, %rdi
	call	EVP_PKEY_CTX_free@PLT
	xorl	%eax, %eax
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L35:
	movl	$-1, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	ENGINE_get_pkey_asn1_meth_str@PLT
	movq	%rax, %r15
	testq	%r15, %r15
	jne	.L19
.L36:
	movq	bio_err(%rip), %rdi
	movq	%r12, %rdx
	leaq	.LC1(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	xorl	%eax, %eax
	jmp	.L15
.L34:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1437:
	.size	init_gen_str, .-init_gen_str
	.section	.rodata.str1.1
.LC3:
	.string	"%s: Use -help for summary.\n"
.LC4:
	.string	"Parameters already set!\n"
.LC5:
	.string	"r"
.LC6:
	.string	"Can't open parameter file %s\n"
	.section	.rodata.str1.8
	.align 8
.LC7:
	.string	"Error reading parameter file %s\n"
	.section	.rodata.str1.1
.LC8:
	.string	"Error initializing context\n"
.LC9:
	.string	"%s: No keytype specified.\n"
	.section	.rodata.str1.8
	.align 8
.LC10:
	.string	"%s: Error setting %s parameter:\n"
	.align 8
.LC11:
	.string	"%s: cipher mode not supported\n"
	.section	.rodata.str1.1
.LC12:
	.string	"Error getting password\n"
.LC13:
	.string	"Error generating parameters\n"
.LC14:
	.string	"Error generating key\n"
.LC15:
	.string	"Bad format specified for key\n"
.LC16:
	.string	"Error writing key\n"
.LC17:
	.string	"Error printing key\n"
	.section	.rodata.str1.8
	.align 8
.LC18:
	.string	"../deps/openssl/openssl/apps/genpkey.c"
	.text
	.p2align 4
	.globl	genpkey_main
	.type	genpkey_main, @function
genpkey_main:
.LFB1435:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	genpkey_options(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	xorl	%r13d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	.L42(%rip), %rbx
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -88(%rbp)
	movq	$0, -80(%rbp)
	movq	$0, -72(%rbp)
	movq	$0, -64(%rbp)
	movl	$32773, -92(%rbp)
	call	opt_init@PLT
	movq	$0, -112(%rbp)
	movq	%rax, -104(%rbp)
	movq	$0, -120(%rbp)
.L39:
	call	opt_next@PLT
	testl	%eax, %eax
	je	.L105
.L63:
	addl	$1, %eax
	cmpl	$12, %eax
	ja	.L39
	movslq	(%rbx,%rax,4), %rax
	addq	%rbx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L42:
	.long	.L53-.L42
	.long	.L39-.L42
	.long	.L52-.L42
	.long	.L51-.L42
	.long	.L50-.L42
	.long	.L49-.L42
	.long	.L48-.L42
	.long	.L47-.L42
	.long	.L46-.L42
	.long	.L45-.L42
	.long	.L44-.L42
	.long	.L43-.L42
	.long	.L41-.L42
	.text
.L43:
	movl	$1, %r12d
	call	opt_next@PLT
	testl	%eax, %eax
	jne	.L63
.L105:
	call	opt_num_rest@PLT
	testl	%eax, %eax
	jne	.L53
	cmpq	$0, -80(%rbp)
	je	.L53
	movq	-112(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	leaq	-72(%rbp), %rdx
	call	app_passwd@PLT
	testl	%eax, %eax
	je	.L106
	movl	-92(%rbp), %esi
	movq	-120(%rbp), %rdi
	movl	%r15d, %edx
	xorl	$1, %edx
	call	bio_open_owner@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L104
	movq	-80(%rbp), %rdi
	leaq	genpkey_cb(%rip), %rsi
	call	EVP_PKEY_CTX_set_cb@PLT
	movq	bio_err(%rip), %rsi
	movq	-80(%rbp), %rdi
	call	EVP_PKEY_CTX_set_app_data@PLT
	movq	-80(%rbp), %rdi
	leaq	-88(%rbp), %rsi
	testl	%r15d, %r15d
	je	.L66
	call	EVP_PKEY_paramgen@PLT
	testl	%eax, %eax
	jle	.L107
	movq	-88(%rbp), %rsi
	movq	%r14, %rdi
	call	PEM_write_bio_Parameters@PLT
.L69:
	testl	%eax, %eax
	jle	.L108
.L71:
	movq	-88(%rbp), %rdi
	testl	%r12d, %r12d
	je	.L54
	movq	%rdi, %rsi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rdi
	testl	%r15d, %r15d
	je	.L72
	call	EVP_PKEY_print_params@PLT
.L73:
	testl	%eax, %eax
	jle	.L74
.L100:
	movq	-88(%rbp), %rdi
	xorl	%r12d, %r12d
	jmp	.L54
.L110:
	movq	-104(%rbp), %rdx
	movq	bio_err(%rip), %rdi
	leaq	.LC9(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L53:
	movq	-104(%rbp), %rdx
	leaq	.LC3(%rip), %rsi
.L99:
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
	movl	$1, %r12d
	xorl	%r14d, %r14d
	call	BIO_printf@PLT
	movq	-88(%rbp), %rdi
.L54:
	call	EVP_PKEY_free@PLT
	movq	-80(%rbp), %rdi
	call	EVP_PKEY_CTX_free@PLT
	movq	%r14, %rdi
	call	BIO_free_all@PLT
	xorl	%edi, %edi
	call	BIO_free@PLT
	movq	%r13, %rdi
	call	release_engine@PLT
	movq	-72(%rbp), %rdi
	movl	$205, %edx
	leaq	.LC18(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L109
	leaq	-40(%rbp), %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L41:
	.cfi_restore_state
	call	opt_unknown@PLT
	leaq	-64(%rbp), %rsi
	movq	%rax, %rdi
	call	opt_cipher@PLT
	testl	%eax, %eax
	je	.L53
	testl	%r15d, %r15d
	jne	.L53
	movq	-64(%rbp), %rdi
	call	EVP_CIPHER_flags@PLT
	andl	$983047, %eax
	cmpq	$6, %rax
	je	.L62
	movq	-64(%rbp), %rdi
	call	EVP_CIPHER_flags@PLT
	andl	$983047, %eax
	cmpq	$7, %rax
	je	.L62
	movq	-64(%rbp), %rdi
	call	EVP_CIPHER_flags@PLT
	andl	$983047, %eax
	cmpq	$65537, %rax
	je	.L62
	movq	-64(%rbp), %rdi
	call	EVP_CIPHER_flags@PLT
	andl	$983047, %eax
	cmpq	$65539, %rax
	je	.L62
	xorl	%r15d, %r15d
	jmp	.L39
.L44:
	cmpq	$0, -80(%rbp)
	jne	.L53
	movl	$1, %r15d
	jmp	.L39
.L45:
	cmpq	$0, -80(%rbp)
	je	.L110
	call	opt_arg@PLT
	movq	-80(%rbp), %rdi
	movq	%rax, %rsi
	call	pkey_ctrl_string@PLT
	testl	%eax, %eax
	jg	.L39
	call	opt_arg@PLT
	movq	bio_err(%rip), %rdi
	movq	-104(%rbp), %rdx
	leaq	.LC10(%rip), %rsi
	movq	%rax, %rcx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
.L102:
	movq	-88(%rbp), %rdi
	movl	$1, %r12d
	xorl	%r14d, %r14d
	jmp	.L54
.L46:
	call	opt_arg@PLT
	leaq	-80(%rbp), %rdi
	movl	%r15d, %ecx
	movq	%r13, %rdx
	movq	%rax, %rsi
	call	init_gen_str
	testl	%eax, %eax
	jne	.L39
	jmp	.L102
.L47:
	cmpl	$1, %r15d
	je	.L53
	call	opt_arg@PLT
	movq	-80(%rbp), %r14
	testq	%r14, %r14
	je	.L56
	leaq	.LC4(%rip), %rsi
.L101:
	movq	bio_err(%rip), %rdi
	movl	$1, %r12d
	xorl	%r14d, %r14d
	call	BIO_puts@PLT
	movq	-88(%rbp), %rdi
	jmp	.L54
.L48:
	call	opt_arg@PLT
	movq	%rax, -112(%rbp)
	jmp	.L39
.L49:
	call	opt_arg@PLT
	movq	%rax, -120(%rbp)
	jmp	.L39
.L50:
	call	opt_arg@PLT
	leaq	-92(%rbp), %rdx
	movl	$2, %esi
	movq	%rax, %rdi
	call	opt_format@PLT
	testl	%eax, %eax
	jne	.L39
	jmp	.L53
.L51:
	call	opt_arg@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	setup_engine@PLT
	movq	%rax, %r13
	jmp	.L39
.L52:
	leaq	genpkey_options(%rip), %rdi
	xorl	%r12d, %r12d
	xorl	%r14d, %r14d
	call	opt_help@PLT
	movq	-88(%rbp), %rdi
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L56:
	movq	%rax, %rdi
	leaq	.LC5(%rip), %rsi
	movq	%rax, -128(%rbp)
	call	BIO_new_file@PLT
	movq	-128(%rbp), %rdx
	testq	%rax, %rax
	movq	%rax, %rdi
	je	.L111
	xorl	%esi, %esi
	movq	%rdx, -144(%rbp)
	movq	%rax, -136(%rbp)
	call	PEM_read_bio_Parameters@PLT
	movq	-136(%rbp), %rdi
	movq	%rax, -128(%rbp)
	call	BIO_free@PLT
	cmpq	$0, -128(%rbp)
	movq	-144(%rbp), %rdx
	je	.L112
	movq	-128(%rbp), %rdi
	movq	%r13, %rsi
	call	EVP_PKEY_CTX_new@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L59
	movq	%rax, %rdi
	movq	%rax, -136(%rbp)
	call	EVP_PKEY_keygen_init@PLT
	movq	-136(%rbp), %r8
	testl	%eax, %eax
	jle	.L59
	movq	-128(%rbp), %rdi
	call	EVP_PKEY_free@PLT
	movq	-136(%rbp), %r8
	movq	%r8, -80(%rbp)
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L59:
	movq	bio_err(%rip), %rdi
	movq	%r8, -104(%rbp)
	leaq	.LC8(%rip), %rsi
	movl	$1, %r12d
	call	BIO_puts@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	movq	-104(%rbp), %r8
	movq	%r8, %rdi
	call	EVP_PKEY_CTX_free@PLT
	movq	-128(%rbp), %rdi
	call	EVP_PKEY_free@PLT
	movq	-88(%rbp), %rdi
	jmp	.L54
.L111:
	leaq	.LC6(%rip), %rsi
.L103:
	movq	bio_err(%rip), %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L104:
	movq	-88(%rbp), %rdi
	movl	$1, %r12d
	jmp	.L54
.L62:
	movq	-104(%rbp), %rdx
	leaq	.LC11(%rip), %rsi
	jmp	.L99
.L112:
	leaq	.LC7(%rip), %rsi
	jmp	.L103
.L106:
	leaq	.LC12(%rip), %rsi
	jmp	.L101
.L66:
	call	EVP_PKEY_keygen@PLT
	testl	%eax, %eax
	jle	.L113
	movl	-92(%rbp), %eax
	cmpl	$32773, %eax
	je	.L114
	cmpl	$4, %eax
	jne	.L70
	movq	-88(%rbp), %rsi
	movq	%r14, %rdi
	call	i2d_PrivateKey_bio@PLT
	jmp	.L69
.L70:
	movq	bio_err(%rip), %rdi
	leaq	.LC15(%rip), %rsi
	xorl	%eax, %eax
	movl	$1, %r12d
	call	BIO_printf@PLT
	movq	-88(%rbp), %rdi
	jmp	.L54
.L108:
	movq	bio_err(%rip), %rdi
	leaq	.LC16(%rip), %rsi
	call	BIO_puts@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	jmp	.L71
.L72:
	call	EVP_PKEY_print_private@PLT
	jmp	.L73
.L114:
	subq	$8, %rsp
	pushq	-72(%rbp)
	movq	-64(%rbp), %rdx
	xorl	%ecx, %ecx
	movq	-88(%rbp), %rsi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	call	PEM_write_bio_PrivateKey@PLT
	popq	%rdx
	popq	%rcx
	jmp	.L69
.L74:
	movq	bio_err(%rip), %rdi
	leaq	.LC17(%rip), %rsi
	call	BIO_puts@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	jmp	.L100
.L113:
	movq	bio_err(%rip), %rdi
	leaq	.LC14(%rip), %rsi
	movl	$1, %r12d
	call	BIO_puts@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	movq	-88(%rbp), %rdi
	jmp	.L54
.L107:
	movq	bio_err(%rip), %rdi
	leaq	.LC13(%rip), %rsi
	movl	%r15d, %r12d
	call	BIO_puts@PLT
	movq	bio_err(%rip), %rdi
	call	ERR_print_errors@PLT
	movq	-88(%rbp), %rdi
	jmp	.L54
.L109:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1435:
	.size	genpkey_main, .-genpkey_main
	.globl	genpkey_options
	.section	.rodata.str1.1
.LC19:
	.string	"help"
.LC20:
	.string	"Display this summary"
.LC21:
	.string	"out"
.LC22:
	.string	"Output file"
.LC23:
	.string	"outform"
.LC24:
	.string	"output format (DER or PEM)"
.LC25:
	.string	"pass"
	.section	.rodata.str1.8
	.align 8
.LC26:
	.string	"Output file pass phrase source"
	.section	.rodata.str1.1
.LC27:
	.string	"paramfile"
.LC28:
	.string	"Parameters file"
.LC29:
	.string	"algorithm"
.LC30:
	.string	"The public key algorithm"
.LC31:
	.string	"pkeyopt"
	.section	.rodata.str1.8
	.align 8
.LC32:
	.string	"Set the public key algorithm option as opt:value"
	.section	.rodata.str1.1
.LC33:
	.string	"genparam"
.LC34:
	.string	"Generate parameters, not key"
.LC35:
	.string	"text"
.LC36:
	.string	"Print the in text"
.LC37:
	.string	""
	.section	.rodata.str1.8
	.align 8
.LC38:
	.string	"Cipher to use to encrypt the key"
	.section	.rodata.str1.1
.LC39:
	.string	"engine"
	.section	.rodata.str1.8
	.align 8
.LC40:
	.string	"Use engine, possibly a hardware device"
	.align 8
.LC41:
	.string	"Order of options may be important!  See the documentation.\n"
	.section	.data.rel.ro,"aw"
	.align 32
	.type	genpkey_options, @object
	.size	genpkey_options, 312
genpkey_options:
	.quad	.LC19
	.long	1
	.long	45
	.quad	.LC20
	.quad	.LC21
	.long	4
	.long	62
	.quad	.LC22
	.quad	.LC23
	.long	3
	.long	70
	.quad	.LC24
	.quad	.LC25
	.long	5
	.long	115
	.quad	.LC26
	.quad	.LC27
	.long	6
	.long	60
	.quad	.LC28
	.quad	.LC29
	.long	7
	.long	115
	.quad	.LC30
	.quad	.LC31
	.long	8
	.long	115
	.quad	.LC32
	.quad	.LC33
	.long	9
	.long	45
	.quad	.LC34
	.quad	.LC35
	.long	10
	.long	45
	.quad	.LC36
	.quad	.LC37
	.long	11
	.long	45
	.quad	.LC38
	.quad	.LC39
	.long	2
	.long	115
	.quad	.LC40
	.quad	OPT_HELP_STR
	.long	1
	.long	1
	.quad	.LC41
	.quad	0
	.zero	16
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
