	.file	"bf_prefix.c"
	.text
	.p2align 4
	.type	prefix_callback_ctrl, @function
prefix_callback_ctrl:
.LFB1428:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movl	%esi, %r12d
	call	BIO_next@PLT
	movq	%r13, %rdx
	movl	%r12d, %esi
	popq	%r12
	movq	%rax, %rdi
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	BIO_callback_ctrl@PLT
	.cfi_endproc
.LFE1428:
	.size	prefix_callback_ctrl, .-prefix_callback_ctrl
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/apps/bf_prefix.c"
	.text
	.p2align 4
	.type	prefix_destroy, @function
prefix_destroy:
.LFB1424:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	BIO_get_data@PLT
	movl	$71, %edx
	leaq	.LC0(%rip), %rsi
	movq	(%rax), %rdi
	movq	%rax, %r12
	call	CRYPTO_free@PLT
	movq	%r12, %rdi
	movl	$72, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	addq	$8, %rsp
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1424:
	.size	prefix_destroy, .-prefix_destroy
	.p2align 4
	.type	prefix_gets, @function
prefix_gets:
.LFB1429:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	call	BIO_next@PLT
	movl	%r13d, %edx
	movq	%r12, %rsi
	popq	%r12
	movq	%rax, %rdi
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	BIO_gets@PLT
	.cfi_endproc
.LFE1429:
	.size	prefix_gets, .-prefix_gets
	.p2align 4
	.type	prefix_puts, @function
prefix_puts:
.LFB1430:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	movq	%rsi, %rdi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	call	strlen@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%r12
	movq	%rax, %rdx
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	BIO_write@PLT
	.cfi_endproc
.LFE1430:
	.size	prefix_puts, .-prefix_puts
	.p2align 4
	.type	prefix_read, @function
prefix_read:
.LFB1425:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	subq	$8, %rsp
	call	BIO_next@PLT
	addq	$8, %rsp
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%rax, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	BIO_read_ex@PLT
	.cfi_endproc
.LFE1425:
	.size	prefix_read, .-prefix_read
	.p2align 4
	.type	prefix_write, @function
prefix_write:
.LFB1426:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	BIO_get_data@PLT
	movq	%rax, -80(%rbp)
	testq	%rax, %rax
	je	.L28
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L14
	cmpb	$0, (%rax)
	je	.L14
	leaq	-64(%rbp), %rax
	movq	$0, (%r14)
	movq	%rax, -72(%rbp)
.L42:
	testq	%r13, %r13
	je	.L43
	movq	-80(%rbp), %rax
	movl	8(%rax), %edx
	testl	%edx, %edx
	jne	.L44
.L18:
	xorl	%ebx, %ebx
.L26:
	movzbl	(%r12,%rbx), %eax
	addq	$1, %rbx
	movb	%al, -88(%rbp)
	cmpb	$10, %al
	je	.L23
	cmpq	%r13, %rbx
	jne	.L26
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L46:
	movq	-64(%rbp), %rax
	addq	%rax, (%r14)
	addq	%rax, %r12
	subq	%rax, %r13
	subq	%rax, %rbx
	je	.L45
.L23:
	movq	%r15, %rdi
	movq	$0, -64(%rbp)
	call	BIO_next@PLT
	movq	-72(%rbp), %rcx
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	BIO_write_ex@PLT
	testl	%eax, %eax
	jne	.L46
.L12:
	movq	-56(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L47
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L44:
	.cfi_restore_state
	movq	(%rax), %rsi
	movq	%rsi, %rdi
	movq	%rsi, -88(%rbp)
	call	strlen@PLT
	movq	%r15, %rdi
	movq	%rax, %rbx
	call	BIO_next@PLT
	movq	-72(%rbp), %rcx
	movq	-88(%rbp), %rsi
	movq	%rbx, %rdx
	movq	%rax, %rdi
	call	BIO_write_ex@PLT
	testl	%eax, %eax
	je	.L12
	movq	-80(%rbp), %rax
	movl	$0, 8(%rax)
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L14:
	testq	%r13, %r13
	je	.L16
	xorl	%eax, %eax
	movq	-80(%rbp), %rcx
	cmpb	$10, -1(%r12,%r13)
	sete	%al
	movl	%eax, 8(%rcx)
.L16:
	movq	%r15, %rdi
	call	BIO_next@PLT
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	BIO_write_ex@PLT
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L45:
	cmpb	$10, -88(%rbp)
	jne	.L42
	movq	-80(%rbp), %rax
	movl	$1, 8(%rax)
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L28:
	xorl	%eax, %eax
	jmp	.L12
.L43:
	movl	$1, %eax
	jmp	.L12
.L47:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1426:
	.size	prefix_write, .-prefix_write
	.p2align 4
	.type	prefix_create, @function
prefix_create:
.LFB1423:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rsi
	movl	$55, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movl	$16, %edi
	subq	$8, %rsp
	call	CRYPTO_zalloc@PLT
	movq	%rax, %rsi
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L48
	movq	$0, (%rsi)
	movq	%r12, %rdi
	movl	$1, 8(%rsi)
	call	BIO_set_data@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	call	BIO_set_init@PLT
	movl	$1, %eax
.L48:
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1423:
	.size	prefix_create, .-prefix_create
	.p2align 4
	.type	prefix_ctrl, @function
prefix_ctrl:
.LFB1427:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	cmpl	$32768, %esi
	je	.L63
	movl	%esi, %r12d
	movq	%rdx, %r15
	call	BIO_next@PLT
	testq	%rax, %rax
	je	.L58
	movq	%r13, %rdi
	call	BIO_next@PLT
	movq	%r14, %rcx
	movq	%r15, %rdx
	movl	%r12d, %esi
	movq	%rax, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	BIO_ctrl@PLT
	.p2align 4,,10
	.p2align 3
.L63:
	.cfi_restore_state
	call	BIO_get_data@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L56
.L58:
	popq	%r12
	xorl	%eax, %eax
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L56:
	.cfi_restore_state
	movq	(%rax), %rdi
	movl	$151, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%r14, %rdi
	movl	$152, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_strdup@PLT
	testq	%rax, %rax
	movq	%rax, (%r12)
	popq	%r12
	setne	%al
	popq	%r13
	popq	%r14
	movzbl	%al, %eax
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1427:
	.size	prefix_ctrl, .-prefix_ctrl
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"Prefix filter"
	.text
	.p2align 4
	.globl	apps_bf_prefix
	.type	apps_bf_prefix, @function
apps_bf_prefix:
.LFB1422:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	prefix_meth(%rip), %r12
	testq	%r12, %r12
	je	.L79
.L64:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L79:
	.cfi_restore_state
	movl	$512, %edi
	leaq	.LC1(%rip), %rsi
	call	BIO_meth_new@PLT
	movq	%rax, prefix_meth(%rip)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L66
	leaq	prefix_create(%rip), %rsi
	call	BIO_meth_set_create@PLT
	testl	%eax, %eax
	jne	.L67
.L74:
	movq	prefix_meth(%rip), %rdi
.L66:
	call	BIO_meth_free@PLT
	movq	%r12, %rax
	movq	$0, prefix_meth(%rip)
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L67:
	.cfi_restore_state
	movq	prefix_meth(%rip), %rdi
	leaq	prefix_destroy(%rip), %rsi
	call	BIO_meth_set_destroy@PLT
	testl	%eax, %eax
	je	.L74
	movq	prefix_meth(%rip), %rdi
	leaq	prefix_write(%rip), %rsi
	call	BIO_meth_set_write_ex@PLT
	testl	%eax, %eax
	je	.L74
	movq	prefix_meth(%rip), %rdi
	leaq	prefix_read(%rip), %rsi
	call	BIO_meth_set_read_ex@PLT
	testl	%eax, %eax
	je	.L74
	movq	prefix_meth(%rip), %rdi
	leaq	prefix_puts(%rip), %rsi
	call	BIO_meth_set_puts@PLT
	testl	%eax, %eax
	je	.L74
	movq	prefix_meth(%rip), %rdi
	leaq	prefix_gets(%rip), %rsi
	call	BIO_meth_set_gets@PLT
	testl	%eax, %eax
	je	.L74
	movq	prefix_meth(%rip), %rdi
	leaq	prefix_ctrl(%rip), %rsi
	call	BIO_meth_set_ctrl@PLT
	testl	%eax, %eax
	je	.L74
	movq	prefix_meth(%rip), %rdi
	leaq	prefix_callback_ctrl(%rip), %rsi
	call	BIO_meth_set_callback_ctrl@PLT
	testl	%eax, %eax
	je	.L74
	movq	prefix_meth(%rip), %r12
	jmp	.L64
	.cfi_endproc
.LFE1422:
	.size	apps_bf_prefix, .-apps_bf_prefix
	.local	prefix_meth
	.comm	prefix_meth,8,8
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
