	.file	"x_int64.c"
	.text
	.p2align 4
	.type	uint64_clear, @function
uint64_clear:
.LFB493:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	$0, (%rax)
	ret
	.cfi_endproc
.LFE493:
	.size	uint64_clear, .-uint64_clear
	.p2align 4
	.type	uint32_clear, @function
uint32_clear:
.LFB499:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movl	$0, (%rax)
	ret
	.cfi_endproc
.LFE499:
	.size	uint32_clear, .-uint32_clear
	.p2align 4
	.type	uint32_i2c, @function
uint32_i2c:
.LFB500:
	.cfi_startproc
	endbr64
	movq	%rdi, %r8
	movq	%rsi, %rdi
	movq	(%r8), %rax
	movl	(%rax), %esi
	movq	40(%rcx), %rax
	testl	%esi, %esi
	jne	.L10
	testb	$1, %al
	jne	.L5
.L10:
	xorl	%edx, %edx
	testb	$2, %al
	je	.L7
	testl	%esi, %esi
	jns	.L7
	negl	%esi
	movl	$1, %edx
.L7:
	movl	%esi, %esi
	jmp	i2c_uint64_int@PLT
.L5:
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE500:
	.size	uint32_i2c, .-uint32_i2c
	.p2align 4
	.type	uint64_i2c, @function
uint64_i2c:
.LFB494:
	.cfi_startproc
	endbr64
	movq	%rdi, %r8
	movq	%rsi, %rdi
	movq	(%r8), %rax
	movq	(%rax), %rsi
	movq	40(%rcx), %rax
	testq	%rsi, %rsi
	jne	.L21
	testb	$1, %al
	jne	.L16
.L21:
	xorl	%edx, %edx
	testb	$2, %al
	je	.L18
	testq	%rsi, %rsi
	jns	.L18
	negq	%rsi
	movl	$1, %edx
.L18:
	jmp	i2c_uint64_int@PLT
.L16:
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE494:
	.size	uint64_i2c, .-uint64_i2c
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/asn1/x_int64.c"
	.text
	.p2align 4
	.type	uint32_free, @function
uint32_free:
.LFB498:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$135, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rdi
	call	CRYPTO_free@PLT
	movq	$0, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE498:
	.size	uint32_free, .-uint32_free
	.p2align 4
	.type	uint64_free, @function
uint64_free:
.LFB492:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$40, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rdi
	call	CRYPTO_free@PLT
	movq	$0, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE492:
	.size	uint64_free, .-uint64_free
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"%d\n"
.LC2:
	.string	"%u\n"
	.text
	.p2align 4
	.type	uint32_print, @function
uint32_print:
.LFB502:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rax
	movq	%rdx, %r8
	movl	(%rax), %edx
	testb	$2, 40(%r8)
	jne	.L32
	leaq	.LC2(%rip), %rsi
	xorl	%eax, %eax
	jmp	BIO_printf@PLT
	.p2align 4,,10
	.p2align 3
.L32:
	leaq	.LC1(%rip), %rsi
	xorl	%eax, %eax
	jmp	BIO_printf@PLT
	.cfi_endproc
.LFE502:
	.size	uint32_print, .-uint32_print
	.section	.rodata.str1.1
.LC3:
	.string	"%jd\n"
.LC4:
	.string	"%ju\n"
	.text
	.p2align 4
	.type	uint64_print, @function
uint64_print:
.LFB496:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rax
	movq	%rdx, %r8
	movq	(%rax), %rdx
	testb	$2, 40(%r8)
	jne	.L35
	leaq	.LC4(%rip), %rsi
	xorl	%eax, %eax
	jmp	BIO_printf@PLT
	.p2align 4,,10
	.p2align 3
.L35:
	leaq	.LC3(%rip), %rsi
	xorl	%eax, %eax
	jmp	BIO_printf@PLT
	.cfi_endproc
.LFE496:
	.size	uint64_print, .-uint64_print
	.p2align 4
	.type	uint64_new, @function
uint64_new:
.LFB491:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$31, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$8, %edi
	subq	$8, %rsp
	call	CRYPTO_zalloc@PLT
	movl	$1, %r8d
	movq	%rax, (%rbx)
	testq	%rax, %rax
	je	.L41
.L36:
	addq	$8, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L41:
	.cfi_restore_state
	movl	$32, %r8d
	movl	$65, %edx
	movl	$141, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%r8d, %r8d
	jmp	.L36
	.cfi_endproc
.LFE491:
	.size	uint64_new, .-uint64_new
	.p2align 4
	.type	uint32_new, @function
uint32_new:
.LFB497:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$126, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$4, %edi
	subq	$8, %rsp
	call	CRYPTO_zalloc@PLT
	movl	$1, %r8d
	movq	%rax, (%rbx)
	testq	%rax, %rax
	je	.L47
.L42:
	addq	$8, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L47:
	.cfi_restore_state
	movl	$127, %r8d
	movl	$65, %edx
	movl	$139, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%r8d, %r8d
	jmp	.L42
	.cfi_endproc
.LFE497:
	.size	uint32_new, .-uint32_new
	.p2align 4
	.type	uint64_c2i, @function
uint64_c2i:
.LFB495:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%r9, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movl	%edx, %ebx
	subq	$48, %rsp
	movq	%rsi, -72(%rbp)
	movq	(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -48(%rbp)
	movl	$0, -52(%rbp)
	testq	%r13, %r13
	je	.L61
.L49:
	testl	%ebx, %ebx
	jne	.L62
.L51:
	movq	-48(%rbp), %rdx
.L54:
	movq	%rdx, 0(%r13)
	movl	$1, %eax
.L48:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L63
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L62:
	.cfi_restore_state
	movslq	%ebx, %rcx
	leaq	-72(%rbp), %rdx
	leaq	-52(%rbp), %rsi
	leaq	-48(%rbp), %rdi
	call	c2i_uint64_int@PLT
	testl	%eax, %eax
	je	.L48
	movl	-52(%rbp), %eax
	testb	$2, 40(%r14)
	je	.L64
	movq	-48(%rbp), %rdx
	testl	%eax, %eax
	jne	.L55
	testq	%rdx, %rdx
	jns	.L54
	movl	$102, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$223, %edx
	movl	%eax, -76(%rbp)
	movl	$112, %esi
	movl	$13, %edi
	call	ERR_put_error@PLT
	movl	-76(%rbp), %eax
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L64:
	testl	%eax, %eax
	je	.L51
	movl	$97, %r8d
	movl	$226, %edx
	movl	$112, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L61:
	movq	%rdi, %r12
	movl	$31, %edx
	leaq	.LC0(%rip), %rsi
	movl	$8, %edi
	call	CRYPTO_zalloc@PLT
	movq	%rax, (%r12)
	movq	%rax, %r13
	testq	%rax, %rax
	jne	.L49
	movl	$32, %r8d
	movl	$65, %edx
	movl	$141, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L55:
	negq	%rdx
	jmp	.L54
.L63:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE495:
	.size	uint64_c2i, .-uint64_c2i
	.p2align 4
	.type	uint32_c2i, @function
uint32_c2i:
.LFB501:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%r9, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movl	%edx, %ebx
	subq	$48, %rsp
	movq	%rsi, -72(%rbp)
	movq	(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -48(%rbp)
	movl	$0, -52(%rbp)
	testq	%r13, %r13
	je	.L82
.L66:
	testl	%ebx, %ebx
	jne	.L83
	movq	-48(%rbp), %rax
.L72:
	movl	%eax, 0(%r13)
	movl	$1, %eax
.L65:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L84
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L83:
	.cfi_restore_state
	movslq	%ebx, %rcx
	leaq	-72(%rbp), %rdx
	leaq	-52(%rbp), %rsi
	leaq	-48(%rbp), %rdi
	call	c2i_uint64_int@PLT
	testl	%eax, %eax
	je	.L65
	movl	-52(%rbp), %edx
	testb	$2, 40(%r14)
	je	.L85
	movq	-48(%rbp), %rax
	testl	%edx, %edx
	je	.L86
	movl	$2147483648, %edx
	cmpq	%rdx, %rax
	ja	.L87
	negq	%rax
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L85:
	testl	%edx, %edx
	jne	.L88
	movq	-48(%rbp), %rax
	movl	$4294967295, %edx
	cmpq	%rdx, %rax
	jbe	.L72
.L73:
	movl	$212, %r8d
	movl	$223, %edx
	movl	$105, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L82:
	movq	%rdi, %r12
	movl	$31, %edx
	leaq	.LC0(%rip), %rsi
	movl	$8, %edi
	call	CRYPTO_zalloc@PLT
	movq	%rax, (%r12)
	movq	%rax, %r13
	testq	%rax, %rax
	jne	.L66
	movl	$32, %r8d
	movl	$65, %edx
	movl	$141, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L86:
	cmpq	$2147483647, %rax
	jbe	.L72
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L88:
	movl	$200, %r8d
	movl	$226, %edx
	movl	$105, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L87:
	movl	$205, %r8d
	movl	$224, %edx
	movl	$105, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L65
.L84:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE501:
	.size	uint32_c2i, .-uint32_c2i
	.globl	ZUINT64_it
	.section	.rodata.str1.1
.LC5:
	.string	"ZUINT64"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	ZUINT64_it, @object
	.size	ZUINT64_it, 56
ZUINT64_it:
	.byte	0
	.zero	7
	.quad	2
	.quad	0
	.quad	0
	.quad	uint64_pf
	.quad	1
	.quad	.LC5
	.globl	ZINT64_it
	.section	.rodata.str1.1
.LC6:
	.string	"ZINT64"
	.section	.data.rel.ro.local
	.align 32
	.type	ZINT64_it, @object
	.size	ZINT64_it, 56
ZINT64_it:
	.byte	0
	.zero	7
	.quad	2
	.quad	0
	.quad	0
	.quad	uint64_pf
	.quad	3
	.quad	.LC6
	.globl	ZUINT32_it
	.section	.rodata.str1.1
.LC7:
	.string	"ZUINT32"
	.section	.data.rel.ro.local
	.align 32
	.type	ZUINT32_it, @object
	.size	ZUINT32_it, 56
ZUINT32_it:
	.byte	0
	.zero	7
	.quad	2
	.quad	0
	.quad	0
	.quad	uint32_pf
	.quad	1
	.quad	.LC7
	.globl	ZINT32_it
	.section	.rodata.str1.1
.LC8:
	.string	"ZINT32"
	.section	.data.rel.ro.local
	.align 32
	.type	ZINT32_it, @object
	.size	ZINT32_it, 56
ZINT32_it:
	.byte	0
	.zero	7
	.quad	2
	.quad	0
	.quad	0
	.quad	uint32_pf
	.quad	3
	.quad	.LC8
	.globl	UINT64_it
	.section	.rodata.str1.1
.LC9:
	.string	"UINT64"
	.section	.data.rel.ro.local
	.align 32
	.type	UINT64_it, @object
	.size	UINT64_it, 56
UINT64_it:
	.byte	0
	.zero	7
	.quad	2
	.quad	0
	.quad	0
	.quad	uint64_pf
	.quad	0
	.quad	.LC9
	.globl	INT64_it
	.section	.rodata.str1.1
.LC10:
	.string	"INT64"
	.section	.data.rel.ro.local
	.align 32
	.type	INT64_it, @object
	.size	INT64_it, 56
INT64_it:
	.byte	0
	.zero	7
	.quad	2
	.quad	0
	.quad	0
	.quad	uint64_pf
	.quad	2
	.quad	.LC10
	.globl	UINT32_it
	.section	.rodata.str1.1
.LC11:
	.string	"UINT32"
	.section	.data.rel.ro.local
	.align 32
	.type	UINT32_it, @object
	.size	UINT32_it, 56
UINT32_it:
	.byte	0
	.zero	7
	.quad	2
	.quad	0
	.quad	0
	.quad	uint32_pf
	.quad	0
	.quad	.LC11
	.globl	INT32_it
	.section	.rodata.str1.1
.LC12:
	.string	"INT32"
	.section	.data.rel.ro.local
	.align 32
	.type	INT32_it, @object
	.size	INT32_it, 56
INT32_it:
	.byte	0
	.zero	7
	.quad	2
	.quad	0
	.quad	0
	.quad	uint32_pf
	.quad	2
	.quad	.LC12
	.section	.data.rel.local,"aw"
	.align 32
	.type	uint64_pf, @object
	.size	uint64_pf, 64
uint64_pf:
	.quad	0
	.quad	0
	.quad	uint64_new
	.quad	uint64_free
	.quad	uint64_clear
	.quad	uint64_c2i
	.quad	uint64_i2c
	.quad	uint64_print
	.align 32
	.type	uint32_pf, @object
	.size	uint32_pf, 64
uint32_pf:
	.quad	0
	.quad	0
	.quad	uint32_new
	.quad	uint32_free
	.quad	uint32_clear
	.quad	uint32_c2i
	.quad	uint32_i2c
	.quad	uint32_print
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
