	.file	"eng_rdrand.c"
	.text
	.p2align 4
	.type	random_status, @function
random_status:
.LFB767:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE767:
	.size	random_status, .-random_status
	.p2align 4
	.type	rdrand_init, @function
rdrand_init:
.LFB768:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE768:
	.size	rdrand_init, .-rdrand_init
	.p2align 4
	.type	get_random_bytes, @function
get_random_bytes:
.LFB766:
	.cfi_startproc
	endbr64
	testl	%esi, %esi
	js	.L7
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movslq	%esi, %rbx
	movq	%rbx, %rsi
	subq	$8, %rsp
	call	OPENSSL_ia32_rdrand_bytes@PLT
	cmpq	%rax, %rbx
	sete	%al
	addq	$8, %rsp
	movzbl	%al, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	.cfi_restore 3
	.cfi_restore 6
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE766:
	.size	get_random_bytes, .-get_random_bytes
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"rdrand"
.LC1:
	.string	"Intel RDRAND engine"
	.text
	.p2align 4
	.globl	engine_load_rdrand_int
	.type	engine_load_rdrand_int, @function
engine_load_rdrand_int:
.LFB771:
	.cfi_startproc
	endbr64
	testb	$64, 7+OPENSSL_ia32cap_P(%rip)
	jne	.L39
	ret
	.p2align 4,,10
	.p2align 3
.L39:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	ENGINE_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L12
	leaq	.LC0(%rip), %rsi
	movq	%rax, %rdi
	call	ENGINE_set_id@PLT
	testl	%eax, %eax
	jne	.L40
.L16:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	ENGINE_free@PLT
	.p2align 4,,10
	.p2align 3
.L40:
	.cfi_restore_state
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	ENGINE_set_name@PLT
	testl	%eax, %eax
	je	.L16
	movl	$8, %esi
	movq	%r12, %rdi
	call	ENGINE_set_flags@PLT
	testl	%eax, %eax
	je	.L16
	leaq	rdrand_init(%rip), %rsi
	movq	%r12, %rdi
	call	ENGINE_set_init_function@PLT
	testl	%eax, %eax
	je	.L16
	leaq	rdrand_meth(%rip), %rsi
	movq	%r12, %rdi
	call	ENGINE_set_RAND@PLT
	testl	%eax, %eax
	je	.L16
	movq	%r12, %rdi
	call	ENGINE_add@PLT
	movq	%r12, %rdi
	call	ENGINE_free@PLT
	addq	$8, %rsp
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	ERR_clear_error@PLT
	.p2align 4,,10
	.p2align 3
.L12:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE771:
	.size	engine_load_rdrand_int, .-engine_load_rdrand_int
	.section	.data.rel.local,"aw"
	.align 32
	.type	rdrand_meth, @object
	.size	rdrand_meth, 48
rdrand_meth:
	.quad	0
	.quad	get_random_bytes
	.quad	0
	.quad	0
	.quad	get_random_bytes
	.quad	random_status
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
