	.file	"p12_init.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/pkcs12/p12_init.c"
	.text
	.p2align 4
	.globl	PKCS12_init
	.type	PKCS12_init, @function
PKCS12_init:
.LFB803:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movl	%edi, %ebx
	subq	$8, %rsp
	call	PKCS12_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L11
	movq	(%rax), %rdi
	movl	$3, %esi
	call	ASN1_INTEGER_set@PLT
	testl	%eax, %eax
	je	.L4
	movq	16(%r12), %r13
	movl	%ebx, %edi
	call	OBJ_nid2obj@PLT
	movq	%rax, 24(%r13)
	cmpl	$21, %ebx
	jne	.L5
	movq	16(%r12), %rbx
	call	ASN1_OCTET_STRING_new@PLT
	movq	%rax, 32(%rbx)
	testq	%rax, %rax
	je	.L12
.L1:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	.cfi_restore_state
	movl	$36, %r8d
	movl	$119, %edx
	movl	$109, %esi
	movl	$35, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L4:
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	PKCS12_free@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	.cfi_restore_state
	movl	$22, %r8d
	movl	$65, %edx
	movl	$109, %esi
	movl	$35, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L12:
	movl	$31, %r8d
	movl	$65, %edx
	movl	$109, %esi
	movl	$35, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L4
	.cfi_endproc
.LFE803:
	.size	PKCS12_init, .-PKCS12_init
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
