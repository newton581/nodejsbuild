	.file	"ui_lib.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/ui/ui_lib.c"
	.text
	.p2align 4
	.type	free_string, @function
free_string:
.LFB805:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	testb	$1, 64(%rdi)
	jne	.L7
.L3:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$70, %edx
	popq	%r12
	leaq	.LC0(%rip), %rsi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_free@PLT
	.p2align 4,,10
	.p2align 3
.L7:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	movl	$55, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	cmpl	$3, (%r12)
	jne	.L3
	movq	40(%r12), %rdi
	movl	$58, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	48(%r12), %rdi
	movl	$59, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	56(%r12), %rdi
	movl	$60, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	jmp	.L3
	.cfi_endproc
.LFE805:
	.size	free_string, .-free_string
	.p2align 4
	.type	print_error, @function
print_error:
.LFB827:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rdi, %r8
	movq	%rdx, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$80, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	(%rdx), %rax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -16(%rbp)
	movaps	%xmm0, -64(%rbp)
	movaps	%xmm0, -48(%rbp)
	movaps	%xmm0, -32(%rbp)
	movq	16(%rax), %rdx
	xorl	%eax, %eax
	movl	$5, -80(%rbp)
	movq	%r8, -72(%rbp)
	testq	%rdx, %rdx
	je	.L8
	leaq	-80(%rbp), %rsi
	call	*%rdx
	testl	%eax, %eax
	setle	%al
	movzbl	%al, %eax
	negl	%eax
.L8:
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L15
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L15:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE827:
	.size	print_error, .-print_error
	.p2align 4
	.type	general_allocate_boolean.constprop.0, @function
general_allocate_boolean.constprop.0:
.LFB870:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -56(%rbp)
	movq	%rdx, -72(%rbp)
	movl	%r9d, -60(%rbp)
	testq	%rcx, %rcx
	je	.L44
	movq	%r8, %r12
	testq	%r8, %r8
	je	.L19
	movq	%rsi, %r15
	movsbl	(%rcx), %esi
	movq	%rcx, %r13
	movq	%rcx, %rbx
	leaq	.LC0(%rip), %r14
	testb	%sil, %sil
	je	.L25
	.p2align 4,,10
	.p2align 3
.L20:
	movq	%r12, %rdi
	call	strchr@PLT
	testq	%rax, %rax
	je	.L23
	movl	$108, %esi
	movl	$166, %r8d
	movq	%r14, %rcx
	movl	$104, %edx
	movl	$40, %edi
	addq	$1, %rbx
	call	ERR_put_error@PLT
	movsbl	(%rbx), %esi
	testb	%sil, %sil
	jne	.L20
.L25:
	testq	%r15, %r15
	je	.L45
	cmpq	$0, 24(%rbp)
	je	.L46
	movl	$109, %edx
	leaq	.LC0(%rip), %rsi
	movl	$72, %edi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r14
	movl	$-1, %eax
	testq	%r14, %r14
	je	.L16
	movl	-60(%rbp), %eax
	movq	%r15, 8(%r14)
	movl	$3, (%r14)
	movl	%eax, 64(%r14)
	movl	16(%rbp), %eax
	movl	%eax, 16(%r14)
	movq	24(%rbp), %rax
	movq	%rax, 24(%r14)
	movq	-56(%rbp), %rax
	movq	8(%rax), %rdi
	testq	%rdi, %rdi
	je	.L47
.L27:
	movq	-72(%rbp), %xmm0
	movq	%r13, %xmm1
	movq	%r12, 56(%r14)
	movq	%r14, %rsi
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 40(%r14)
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	jle	.L48
.L16:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	.cfi_restore_state
	movsbl	1(%rbx), %esi
	addq	$1, %rbx
	testb	%sil, %sil
	jne	.L20
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L48:
	subl	$1, %eax
	testb	$1, 64(%r14)
	jne	.L49
.L31:
	movq	%r14, %rdi
	movl	$70, %edx
	leaq	.LC0(%rip), %rsi
	movl	%eax, -56(%rbp)
	call	CRYPTO_free@PLT
	movl	-56(%rbp), %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L49:
	.cfi_restore_state
	movq	8(%r14), %rdi
	movl	$55, %edx
	leaq	.LC0(%rip), %rsi
	movl	%eax, -56(%rbp)
	call	CRYPTO_free@PLT
	cmpl	$3, (%r14)
	movl	-56(%rbp), %eax
	jne	.L31
	movq	40(%r14), %rdi
	movl	$58, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	48(%r14), %rdi
	movl	$59, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	56(%r14), %rdi
	movl	$60, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movl	-56(%rbp), %eax
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L47:
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, %rdi
	movq	-56(%rbp), %rax
	movq	%rdi, 8(%rax)
	testq	%rdi, %rdi
	jne	.L27
	testb	$1, 64(%r14)
	jne	.L50
.L29:
	movl	$70, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r14, %rdi
	call	CRYPTO_free@PLT
	movl	$-1, %eax
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L45:
	movl	$105, %r8d
	movl	$67, %edx
	movl	$109, %esi
	movl	$40, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-1, %eax
	jmp	.L16
.L46:
	movl	$108, %r8d
	movl	$105, %edx
	movl	$109, %esi
	movl	$40, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-1, %eax
	jmp	.L16
.L19:
	movl	$162, %r8d
.L43:
	movl	$67, %edx
	movl	$108, %esi
	movl	$40, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-1, %eax
	jmp	.L16
.L44:
	movl	$160, %r8d
	jmp	.L43
.L50:
	movq	8(%r14), %rdi
	movl	$55, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	cmpl	$3, (%r14)
	jne	.L29
	movq	40(%r14), %rdi
	movl	$58, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	48(%r14), %rdi
	movl	$59, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	56(%r14), %rdi
	movl	$60, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	jmp	.L29
	.cfi_endproc
.LFE870:
	.size	general_allocate_boolean.constprop.0, .-general_allocate_boolean.constprop.0
	.p2align 4
	.globl	UI_new
	.type	UI_new, @function
UI_new:
.LFB803:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$25, %edx
	movl	$48, %edi
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	CRYPTO_zalloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L57
	call	CRYPTO_THREAD_lock_new@PLT
	movq	%rax, 40(%r12)
	testq	%rax, %rax
	je	.L58
	call	UI_get_default_method@PLT
	testq	%rax, %rax
	je	.L59
.L55:
	movq	%rax, (%r12)
	leaq	24(%r12), %rdx
	movq	%r12, %rsi
	movl	$11, %edi
	call	CRYPTO_new_ex_data@PLT
	testl	%eax, %eax
	je	.L60
.L51:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L59:
	.cfi_restore_state
	call	UI_null@PLT
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L57:
	movl	$28, %r8d
	movl	$65, %edx
	movl	$104, %esi
	movl	$40, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L60:
	movq	%r12, %rdi
	movl	$46, %edx
	leaq	.LC0(%rip), %rsi
	xorl	%r12d, %r12d
	call	CRYPTO_free@PLT
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L58:
	movl	$34, %r8d
	movl	$65, %edx
	movl	$104, %esi
	movl	$40, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	movl	$35, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	jmp	.L51
	.cfi_endproc
.LFE803:
	.size	UI_new, .-UI_new
	.p2align 4
	.globl	UI_new_method
	.type	UI_new_method, @function
UI_new_method:
.LFB804:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$25, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movl	$48, %edi
	call	CRYPTO_zalloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L67
	call	CRYPTO_THREAD_lock_new@PLT
	movq	%rax, 40(%r12)
	testq	%rax, %rax
	je	.L68
	testq	%rbx, %rbx
	je	.L69
.L65:
	movq	%rbx, (%r12)
	leaq	24(%r12), %rdx
	movq	%r12, %rsi
	movl	$11, %edi
	call	CRYPTO_new_ex_data@PLT
	testl	%eax, %eax
	je	.L70
.L61:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L69:
	.cfi_restore_state
	call	UI_get_default_method@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	jne	.L65
	call	UI_null@PLT
	movq	%rax, %rbx
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L67:
	movl	$28, %r8d
	movl	$65, %edx
	movl	$104, %esi
	movl	$40, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L70:
	movq	%r12, %rdi
	movl	$46, %edx
	leaq	.LC0(%rip), %rsi
	xorl	%r12d, %r12d
	call	CRYPTO_free@PLT
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L68:
	movl	$34, %r8d
	movl	$65, %edx
	movl	$104, %esi
	movl	$40, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	movl	$35, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	jmp	.L61
	.cfi_endproc
.LFE804:
	.size	UI_new_method, .-UI_new_method
	.p2align 4
	.globl	UI_free
	.type	UI_free, @function
UI_free:
.LFB806:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L71
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	testb	$2, 32(%rdi)
	je	.L73
	movq	(%rdi), %rax
	movq	16(%rdi), %rsi
	call	*56(%rax)
.L73:
	movq	8(%r12), %rdi
	leaq	free_string(%rip), %rsi
	call	OPENSSL_sk_pop_free@PLT
	leaq	24(%r12), %rdx
	movq	%r12, %rsi
	movl	$11, %edi
	call	CRYPTO_free_ex_data@PLT
	movq	40(%r12), %rdi
	call	CRYPTO_THREAD_lock_free@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$83, %edx
	popq	%r12
	.cfi_restore 12
	leaq	.LC0(%rip), %rsi
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_free@PLT
	.p2align 4,,10
	.p2align 3
.L71:
	ret
	.cfi_endproc
.LFE806:
	.size	UI_free, .-UI_free
	.p2align 4
	.globl	UI_add_input_string
	.type	UI_add_input_string, @function
UI_add_input_string:
.LFB811:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%rsi, %rsi
	je	.L94
	testq	%rcx, %rcx
	je	.L95
	movl	%edx, -52(%rbp)
	movq	%rdi, %r13
	movq	%rsi, %rbx
	movl	$109, %edx
	movl	$72, %edi
	movq	%rcx, -64(%rbp)
	movl	%r8d, %r15d
	movl	%r9d, %r14d
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r12
	movl	$-1, %eax
	testq	%r12, %r12
	je	.L80
	movl	-52(%rbp), %r11d
	movq	-64(%rbp), %rcx
	movq	%rbx, 8(%r12)
	movq	8(%r13), %rdi
	movl	$1, (%r12)
	movl	$0, 64(%r12)
	movl	%r11d, 16(%r12)
	movq	%rcx, 24(%r12)
	testq	%rdi, %rdi
	je	.L96
.L84:
	movl	%r15d, 40(%r12)
	movq	%r12, %rsi
	movl	%r14d, 44(%r12)
	movq	$0, 48(%r12)
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	jle	.L97
.L80:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L97:
	.cfi_restore_state
	subl	$1, %eax
	testb	$1, 64(%r12)
	jne	.L98
.L88:
	movq	%r12, %rdi
	movl	$70, %edx
	leaq	.LC0(%rip), %rsi
	movl	%eax, -52(%rbp)
	call	CRYPTO_free@PLT
	movl	-52(%rbp), %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L98:
	.cfi_restore_state
	movq	8(%r12), %rdi
	movl	$55, %edx
	leaq	.LC0(%rip), %rsi
	movl	%eax, -52(%rbp)
	call	CRYPTO_free@PLT
	cmpl	$3, (%r12)
	movl	-52(%rbp), %eax
	jne	.L88
	movq	40(%r12), %rdi
	movl	$58, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	48(%r12), %rdi
	movl	$59, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	56(%r12), %rdi
	movl	$60, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movl	-52(%rbp), %eax
	jmp	.L88
	.p2align 4,,10
	.p2align 3
.L96:
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, 8(%r13)
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L84
	testb	$1, 64(%r12)
	jne	.L99
.L86:
	movl	$70, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
	movl	$-1, %eax
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L94:
	movl	$105, %r8d
	movl	$67, %edx
	movl	$109, %esi
	movl	$40, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-1, %eax
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L95:
	movl	$108, %r8d
	movl	$105, %edx
	movl	$109, %esi
	movl	$40, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-1, %eax
	jmp	.L80
.L99:
	movq	8(%r12), %rdi
	movl	$55, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	cmpl	$3, (%r12)
	jne	.L86
	movq	40(%r12), %rdi
	movl	$58, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	48(%r12), %rdi
	movl	$59, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	56(%r12), %rdi
	movl	$60, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	jmp	.L86
	.cfi_endproc
.LFE811:
	.size	UI_add_input_string, .-UI_add_input_string
	.p2align 4
	.globl	UI_dup_input_string
	.type	UI_dup_input_string, @function
UI_dup_input_string:
.LFB812:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%r9d, -52(%rbp)
	testq	%rsi, %rsi
	je	.L101
	movq	%rdi, %r12
	movl	%edx, %r13d
	movq	%rsi, %rdi
	movl	$213, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rcx, %rbx
	movl	%r8d, %r15d
	call	CRYPTO_strdup@PLT
	testq	%rax, %rax
	je	.L115
	testq	%rbx, %rbx
	je	.L116
	movl	$109, %edx
	leaq	.LC0(%rip), %rsi
	movl	$72, %edi
	movq	%rax, -64(%rbp)
	call	CRYPTO_malloc@PLT
	movq	%rax, %r14
	movl	$-1, %eax
	testq	%r14, %r14
	je	.L100
	movq	-64(%rbp), %rcx
	movq	8(%r12), %rdi
	movl	%r13d, 16(%r14)
	movl	$1, 64(%r14)
	movq	%rcx, 8(%r14)
	movl	$1, (%r14)
	movq	%rbx, 24(%r14)
	testq	%rdi, %rdi
	je	.L117
.L105:
	movl	-52(%rbp), %eax
	movl	%r15d, 40(%r14)
	movq	%r14, %rsi
	movq	$0, 48(%r14)
	movl	%eax, 44(%r14)
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	jle	.L118
.L100:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L101:
	.cfi_restore_state
	movl	$105, %r8d
	movl	$67, %edx
	movl	$109, %esi
	movl	$40, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$24, %rsp
	movl	$-1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L118:
	.cfi_restore_state
	subl	$1, %eax
	testb	$1, 64(%r14)
	jne	.L119
.L109:
	movq	%r14, %rdi
	movl	$70, %edx
	leaq	.LC0(%rip), %rsi
	movl	%eax, -52(%rbp)
	call	CRYPTO_free@PLT
	movl	-52(%rbp), %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L119:
	.cfi_restore_state
	movq	8(%r14), %rdi
	movl	$55, %edx
	leaq	.LC0(%rip), %rsi
	movl	%eax, -52(%rbp)
	call	CRYPTO_free@PLT
	cmpl	$3, (%r14)
	movl	-52(%rbp), %eax
	jne	.L109
	movq	40(%r14), %rdi
	movl	$58, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	48(%r14), %rdi
	movl	$59, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	56(%r14), %rdi
	movl	$60, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movl	-52(%rbp), %eax
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L117:
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, 8(%r12)
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L105
	testb	$1, 64(%r14)
	jne	.L120
.L107:
	movl	$70, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r14, %rdi
	call	CRYPTO_free@PLT
	movl	$-1, %eax
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L115:
	movl	$215, %r8d
	movl	$65, %edx
	movl	$103, %esi
	movl	$40, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L116:
	movl	$108, %r8d
	movl	$105, %edx
	movl	$109, %esi
	movl	$40, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-1, %eax
	jmp	.L100
.L120:
	movq	8(%r14), %rdi
	movl	$55, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	cmpl	$3, (%r14)
	jne	.L107
	movq	40(%r14), %rdi
	movl	$58, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	48(%r14), %rdi
	movl	$59, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	56(%r14), %rdi
	movl	$60, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	jmp	.L107
	.cfi_endproc
.LFE812:
	.size	UI_dup_input_string, .-UI_dup_input_string
	.p2align 4
	.globl	UI_add_verify_string
	.type	UI_add_verify_string, @function
UI_add_verify_string:
.LFB813:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%rsi, %rsi
	je	.L135
	testq	%rcx, %rcx
	je	.L136
	movl	%edx, -52(%rbp)
	movq	%rdi, %r13
	movq	%rsi, %rbx
	movl	$109, %edx
	movl	$72, %edi
	movq	%rcx, -64(%rbp)
	movl	%r8d, %r15d
	movl	%r9d, %r14d
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r12
	movl	$-1, %eax
	testq	%r12, %r12
	je	.L121
	movl	-52(%rbp), %r11d
	movq	-64(%rbp), %rcx
	movq	%rbx, 8(%r12)
	movq	8(%r13), %rdi
	movl	$2, (%r12)
	movl	$0, 64(%r12)
	movl	%r11d, 16(%r12)
	movq	%rcx, 24(%r12)
	testq	%rdi, %rdi
	je	.L137
.L125:
	movq	16(%rbp), %rax
	movl	%r15d, 40(%r12)
	movq	%r12, %rsi
	movl	%r14d, 44(%r12)
	movq	%rax, 48(%r12)
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	jle	.L138
.L121:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L138:
	.cfi_restore_state
	subl	$1, %eax
	testb	$1, 64(%r12)
	jne	.L139
.L129:
	movq	%r12, %rdi
	movl	$70, %edx
	leaq	.LC0(%rip), %rsi
	movl	%eax, -52(%rbp)
	call	CRYPTO_free@PLT
	movl	-52(%rbp), %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L139:
	.cfi_restore_state
	movq	8(%r12), %rdi
	movl	$55, %edx
	leaq	.LC0(%rip), %rsi
	movl	%eax, -52(%rbp)
	call	CRYPTO_free@PLT
	cmpl	$3, (%r12)
	movl	-52(%rbp), %eax
	jne	.L129
	movq	40(%r12), %rdi
	movl	$58, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	48(%r12), %rdi
	movl	$59, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	56(%r12), %rdi
	movl	$60, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movl	-52(%rbp), %eax
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L137:
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, 8(%r13)
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L125
	testb	$1, 64(%r12)
	jne	.L140
.L127:
	movl	$70, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
	movl	$-1, %eax
	jmp	.L121
	.p2align 4,,10
	.p2align 3
.L135:
	movl	$105, %r8d
	movl	$67, %edx
	movl	$109, %esi
	movl	$40, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-1, %eax
	jmp	.L121
	.p2align 4,,10
	.p2align 3
.L136:
	movl	$108, %r8d
	movl	$105, %edx
	movl	$109, %esi
	movl	$40, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-1, %eax
	jmp	.L121
.L140:
	movq	8(%r12), %rdi
	movl	$55, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	cmpl	$3, (%r12)
	jne	.L127
	movq	40(%r12), %rdi
	movl	$58, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	48(%r12), %rdi
	movl	$59, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	56(%r12), %rdi
	movl	$60, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	jmp	.L127
	.cfi_endproc
.LFE813:
	.size	UI_add_verify_string, .-UI_add_verify_string
	.p2align 4
	.globl	UI_dup_verify_string
	.type	UI_dup_verify_string, @function
UI_dup_verify_string:
.LFB814:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%r9d, -52(%rbp)
	testq	%rsi, %rsi
	je	.L142
	movq	%rdi, %r12
	movl	%edx, %r13d
	movq	%rsi, %rdi
	movl	$241, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rcx, %rbx
	movl	%r8d, %r15d
	call	CRYPTO_strdup@PLT
	testq	%rax, %rax
	je	.L156
	testq	%rbx, %rbx
	je	.L157
	movl	$109, %edx
	leaq	.LC0(%rip), %rsi
	movl	$72, %edi
	movq	%rax, -64(%rbp)
	call	CRYPTO_malloc@PLT
	movq	%rax, %r14
	movl	$-1, %eax
	testq	%r14, %r14
	je	.L141
	movq	-64(%rbp), %rcx
	movq	8(%r12), %rdi
	movl	%r13d, 16(%r14)
	movl	$1, 64(%r14)
	movq	%rcx, 8(%r14)
	movl	$2, (%r14)
	movq	%rbx, 24(%r14)
	testq	%rdi, %rdi
	je	.L158
.L146:
	movl	-52(%rbp), %eax
	movl	%r15d, 40(%r14)
	movq	%r14, %rsi
	movl	%eax, 44(%r14)
	movq	16(%rbp), %rax
	movq	%rax, 48(%r14)
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	jle	.L159
.L141:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L142:
	.cfi_restore_state
	movl	$105, %r8d
	movl	$67, %edx
	movl	$109, %esi
	movl	$40, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$24, %rsp
	movl	$-1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L159:
	.cfi_restore_state
	subl	$1, %eax
	testb	$1, 64(%r14)
	jne	.L160
.L150:
	movq	%r14, %rdi
	movl	$70, %edx
	leaq	.LC0(%rip), %rsi
	movl	%eax, -52(%rbp)
	call	CRYPTO_free@PLT
	movl	-52(%rbp), %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L160:
	.cfi_restore_state
	movq	8(%r14), %rdi
	movl	$55, %edx
	leaq	.LC0(%rip), %rsi
	movl	%eax, -52(%rbp)
	call	CRYPTO_free@PLT
	cmpl	$3, (%r14)
	movl	-52(%rbp), %eax
	jne	.L150
	movq	40(%r14), %rdi
	movl	$58, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	48(%r14), %rdi
	movl	$59, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	56(%r14), %rdi
	movl	$60, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movl	-52(%rbp), %eax
	jmp	.L150
	.p2align 4,,10
	.p2align 3
.L158:
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, 8(%r12)
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L146
	testb	$1, 64(%r14)
	jne	.L161
.L148:
	movl	$70, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r14, %rdi
	call	CRYPTO_free@PLT
	movl	$-1, %eax
	jmp	.L141
	.p2align 4,,10
	.p2align 3
.L157:
	movl	$108, %r8d
	movl	$105, %edx
	movl	$109, %esi
	movl	$40, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-1, %eax
	jmp	.L141
.L156:
	movl	$243, %r8d
	movl	$65, %edx
	movl	$106, %esi
	movl	$40, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-1, %eax
	jmp	.L141
.L161:
	movq	8(%r14), %rdi
	movl	$55, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	cmpl	$3, (%r14)
	jne	.L148
	movq	40(%r14), %rdi
	movl	$58, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	48(%r14), %rdi
	movl	$59, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	56(%r14), %rdi
	movl	$60, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	jmp	.L148
	.cfi_endproc
.LFE814:
	.size	UI_dup_verify_string, .-UI_dup_verify_string
	.p2align 4
	.globl	UI_add_input_boolean
	.type	UI_add_input_boolean, @function
UI_add_input_boolean:
.LFB815:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	16(%rbp)
	pushq	%r9
	xorl	%r9d, %r9d
	call	general_allocate_boolean.constprop.0
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE815:
	.size	UI_add_input_boolean, .-UI_add_input_boolean
	.p2align 4
	.globl	UI_dup_input_boolean
	.type	UI_dup_input_boolean, @function
UI_dup_input_boolean:
.LFB816:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$24, %rsp
	movl	%r9d, -52(%rbp)
	testq	%rsi, %rsi
	je	.L165
	movq	%r13, %rdi
	movl	$272, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_strdup@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L185
.L165:
	testq	%r14, %r14
	je	.L167
	movq	%r14, %rdi
	movl	$280, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_strdup@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L186
.L167:
	testq	%rbx, %rbx
	je	.L168
	movq	%rbx, %rdi
	movl	$288, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_strdup@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L187
.L168:
	testq	%r12, %r12
	je	.L169
	movq	%r12, %rdi
	movl	$296, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_strdup@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L188
.L169:
	movl	-52(%rbp), %eax
	pushq	16(%rbp)
	movq	%rbx, %rcx
	movq	%r14, %rdx
	movl	$1, %r9d
	movq	%r12, %r8
	movq	%r13, %rsi
	movq	%r15, %rdi
	pushq	%rax
	call	general_allocate_boolean.constprop.0
	popq	%rdx
	popq	%rcx
.L164:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L188:
	.cfi_restore_state
	movl	$298, %r8d
.L184:
	movl	$65, %edx
	movl	$110, %esi
	movl	$40, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L166:
	movl	$307, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r13, %rdi
	call	CRYPTO_free@PLT
	movl	$308, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r14, %rdi
	call	CRYPTO_free@PLT
	movl	$309, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rbx, %rdi
	call	CRYPTO_free@PLT
	movl	$310, %edx
	leaq	.LC0(%rip), %rsi
	xorl	%edi, %edi
	call	CRYPTO_free@PLT
	movl	$-1, %eax
	jmp	.L164
.L187:
	movl	$290, %r8d
	jmp	.L184
.L186:
	movl	$282, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%ebx, %ebx
	movl	$110, %esi
	movl	$40, %edi
	call	ERR_put_error@PLT
	jmp	.L166
.L185:
	movl	$274, %r8d
	movl	$65, %edx
	xorl	%ebx, %ebx
	xorl	%r14d, %r14d
	leaq	.LC0(%rip), %rcx
	movl	$110, %esi
	movl	$40, %edi
	call	ERR_put_error@PLT
	jmp	.L166
	.cfi_endproc
.LFE816:
	.size	UI_dup_input_boolean, .-UI_dup_input_boolean
	.p2align 4
	.globl	UI_add_info_string
	.type	UI_add_info_string, @function
UI_add_info_string:
.LFB817:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 14, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	testq	%rsi, %rsi
	je	.L202
	movq	%rdi, %r14
	movq	%rsi, %rbx
	movl	$109, %edx
	movl	$72, %edi
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r12
	movl	$-1, %eax
	testq	%r12, %r12
	je	.L189
	movq	8(%r14), %rdi
	movq	%rbx, 8(%r12)
	movl	$0, 64(%r12)
	movl	$0, 16(%r12)
	movl	$4, (%r12)
	movq	$0, 24(%r12)
	testq	%rdi, %rdi
	je	.L203
.L192:
	movq	$0, 40(%r12)
	movq	%r12, %rsi
	movq	$0, 48(%r12)
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	jle	.L204
.L189:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L204:
	.cfi_restore_state
	subl	$1, %eax
	testb	$1, 64(%r12)
	jne	.L205
.L196:
	movq	%r12, %rdi
	movl	$70, %edx
	leaq	.LC0(%rip), %rsi
	movl	%eax, -36(%rbp)
	call	CRYPTO_free@PLT
	movl	-36(%rbp), %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L205:
	.cfi_restore_state
	movq	8(%r12), %rdi
	movl	$55, %edx
	leaq	.LC0(%rip), %rsi
	movl	%eax, -36(%rbp)
	call	CRYPTO_free@PLT
	cmpl	$3, (%r12)
	movl	-36(%rbp), %eax
	jne	.L196
	movq	40(%r12), %rdi
	movl	$58, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	48(%r12), %rdi
	movl	$59, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	56(%r12), %rdi
	movl	$60, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movl	-36(%rbp), %eax
	jmp	.L196
	.p2align 4,,10
	.p2align 3
.L203:
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, 8(%r14)
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L192
	testb	$1, 64(%r12)
	jne	.L206
.L194:
	movl	$70, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
	movl	$-1, %eax
	jmp	.L189
	.p2align 4,,10
	.p2align 3
.L202:
	movl	$105, %r8d
	movl	$67, %edx
	movl	$109, %esi
	movl	$40, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-1, %eax
	jmp	.L189
.L206:
	movq	8(%r12), %rdi
	movl	$55, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	cmpl	$3, (%r12)
	jne	.L194
	movq	40(%r12), %rdi
	movl	$58, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	48(%r12), %rdi
	movl	$59, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	56(%r12), %rdi
	movl	$60, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	jmp	.L194
	.cfi_endproc
.LFE817:
	.size	UI_add_info_string, .-UI_add_info_string
	.p2align 4
	.globl	UI_dup_info_string
	.type	UI_dup_info_string, @function
UI_dup_info_string:
.LFB818:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 14, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	testq	%rsi, %rsi
	je	.L208
	movq	%rdi, %rbx
	movl	$325, %edx
	movq	%rsi, %rdi
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_strdup@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L221
	movl	$109, %edx
	leaq	.LC0(%rip), %rsi
	movl	$72, %edi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r12
	movl	$-1, %eax
	testq	%r12, %r12
	je	.L207
	movq	8(%rbx), %rdi
	movq	%r14, 8(%r12)
	movl	$1, 64(%r12)
	movl	$0, 16(%r12)
	movl	$4, (%r12)
	movq	$0, 24(%r12)
	testq	%rdi, %rdi
	je	.L222
.L211:
	movq	$0, 40(%r12)
	movq	%r12, %rsi
	movq	$0, 48(%r12)
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	jle	.L223
.L207:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L208:
	.cfi_restore_state
	movl	$105, %r8d
	movl	$67, %edx
	movl	$109, %esi
	movl	$40, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$24, %rsp
	movl	$-1, %eax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L223:
	.cfi_restore_state
	subl	$1, %eax
	testb	$1, 64(%r12)
	jne	.L224
.L215:
	movq	%r12, %rdi
	movl	$70, %edx
	leaq	.LC0(%rip), %rsi
	movl	%eax, -36(%rbp)
	call	CRYPTO_free@PLT
	movl	-36(%rbp), %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L224:
	.cfi_restore_state
	movq	8(%r12), %rdi
	movl	$55, %edx
	leaq	.LC0(%rip), %rsi
	movl	%eax, -36(%rbp)
	call	CRYPTO_free@PLT
	cmpl	$3, (%r12)
	movl	-36(%rbp), %eax
	jne	.L215
	movq	40(%r12), %rdi
	movl	$58, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	48(%r12), %rdi
	movl	$59, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	56(%r12), %rdi
	movl	$60, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movl	-36(%rbp), %eax
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L222:
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, 8(%rbx)
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L211
	testb	$1, 64(%r12)
	jne	.L225
.L213:
	movl	$70, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
	movl	$-1, %eax
	jmp	.L207
.L221:
	movl	$327, %r8d
	movl	$65, %edx
	movl	$102, %esi
	movl	$40, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-1, %eax
	jmp	.L207
.L225:
	movq	8(%r12), %rdi
	movl	$55, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	cmpl	$3, (%r12)
	jne	.L213
	movq	40(%r12), %rdi
	movl	$58, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	48(%r12), %rdi
	movl	$59, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	56(%r12), %rdi
	movl	$60, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	jmp	.L213
	.cfi_endproc
.LFE818:
	.size	UI_dup_info_string, .-UI_dup_info_string
	.p2align 4
	.globl	UI_add_error_string
	.type	UI_add_error_string, @function
UI_add_error_string:
.LFB819:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 14, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	testq	%rsi, %rsi
	je	.L239
	movq	%rdi, %r14
	movq	%rsi, %rbx
	movl	$109, %edx
	movl	$72, %edi
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r12
	movl	$-1, %eax
	testq	%r12, %r12
	je	.L226
	movq	8(%r14), %rdi
	movq	%rbx, 8(%r12)
	movl	$0, 64(%r12)
	movl	$0, 16(%r12)
	movl	$5, (%r12)
	movq	$0, 24(%r12)
	testq	%rdi, %rdi
	je	.L240
.L229:
	movq	$0, 40(%r12)
	movq	%r12, %rsi
	movq	$0, 48(%r12)
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	jle	.L241
.L226:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L241:
	.cfi_restore_state
	subl	$1, %eax
	testb	$1, 64(%r12)
	jne	.L242
.L233:
	movq	%r12, %rdi
	movl	$70, %edx
	leaq	.LC0(%rip), %rsi
	movl	%eax, -36(%rbp)
	call	CRYPTO_free@PLT
	movl	-36(%rbp), %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L242:
	.cfi_restore_state
	movq	8(%r12), %rdi
	movl	$55, %edx
	leaq	.LC0(%rip), %rsi
	movl	%eax, -36(%rbp)
	call	CRYPTO_free@PLT
	cmpl	$3, (%r12)
	movl	-36(%rbp), %eax
	jne	.L233
	movq	40(%r12), %rdi
	movl	$58, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	48(%r12), %rdi
	movl	$59, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	56(%r12), %rdi
	movl	$60, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movl	-36(%rbp), %eax
	jmp	.L233
	.p2align 4,,10
	.p2align 3
.L240:
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, 8(%r14)
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L229
	testb	$1, 64(%r12)
	jne	.L243
.L231:
	movl	$70, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
	movl	$-1, %eax
	jmp	.L226
	.p2align 4,,10
	.p2align 3
.L239:
	movl	$105, %r8d
	movl	$67, %edx
	movl	$109, %esi
	movl	$40, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-1, %eax
	jmp	.L226
.L243:
	movq	8(%r12), %rdi
	movl	$55, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	cmpl	$3, (%r12)
	jne	.L231
	movq	40(%r12), %rdi
	movl	$58, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	48(%r12), %rdi
	movl	$59, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	56(%r12), %rdi
	movl	$60, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	jmp	.L231
	.cfi_endproc
.LFE819:
	.size	UI_add_error_string, .-UI_add_error_string
	.p2align 4
	.globl	UI_dup_error_string
	.type	UI_dup_error_string, @function
UI_dup_error_string:
.LFB820:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 14, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	testq	%rsi, %rsi
	je	.L245
	movq	%rdi, %rbx
	movl	$347, %edx
	movq	%rsi, %rdi
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_strdup@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L258
	movl	$109, %edx
	leaq	.LC0(%rip), %rsi
	movl	$72, %edi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r12
	movl	$-1, %eax
	testq	%r12, %r12
	je	.L244
	movq	8(%rbx), %rdi
	movq	%r14, 8(%r12)
	movl	$1, 64(%r12)
	movl	$0, 16(%r12)
	movl	$5, (%r12)
	movq	$0, 24(%r12)
	testq	%rdi, %rdi
	je	.L259
.L248:
	movq	$0, 40(%r12)
	movq	%r12, %rsi
	movq	$0, 48(%r12)
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	jle	.L260
.L244:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L245:
	.cfi_restore_state
	movl	$105, %r8d
	movl	$67, %edx
	movl	$109, %esi
	movl	$40, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$24, %rsp
	movl	$-1, %eax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L260:
	.cfi_restore_state
	subl	$1, %eax
	testb	$1, 64(%r12)
	jne	.L261
.L252:
	movq	%r12, %rdi
	movl	$70, %edx
	leaq	.LC0(%rip), %rsi
	movl	%eax, -36(%rbp)
	call	CRYPTO_free@PLT
	movl	-36(%rbp), %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L261:
	.cfi_restore_state
	movq	8(%r12), %rdi
	movl	$55, %edx
	leaq	.LC0(%rip), %rsi
	movl	%eax, -36(%rbp)
	call	CRYPTO_free@PLT
	cmpl	$3, (%r12)
	movl	-36(%rbp), %eax
	jne	.L252
	movq	40(%r12), %rdi
	movl	$58, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	48(%r12), %rdi
	movl	$59, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	56(%r12), %rdi
	movl	$60, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movl	-36(%rbp), %eax
	jmp	.L252
	.p2align 4,,10
	.p2align 3
.L259:
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, 8(%rbx)
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L248
	testb	$1, 64(%r12)
	jne	.L262
.L250:
	movl	$70, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
	movl	$-1, %eax
	jmp	.L244
.L258:
	movl	$349, %r8d
	movl	$65, %edx
	movl	$101, %esi
	movl	$40, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-1, %eax
	jmp	.L244
.L262:
	movq	8(%r12), %rdi
	movl	$55, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	cmpl	$3, (%r12)
	jne	.L250
	movq	40(%r12), %rdi
	movl	$58, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	48(%r12), %rdi
	movl	$59, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	56(%r12), %rdi
	movl	$60, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	jmp	.L250
	.cfi_endproc
.LFE820:
	.size	UI_dup_error_string, .-UI_dup_error_string
	.p2align 4
	.globl	UI_construct_prompt
	.type	UI_construct_prompt, @function
UI_construct_prompt:
.LFB821:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	subq	$32, %rsp
	.cfi_offset 12, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	64(%rax), %rax
	testq	%rax, %rax
	je	.L264
	call	*%rax
	movq	%rax, %r12
.L263:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L281
	addq	$32, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L264:
	.cfi_restore_state
	movl	$8306, %eax
	movl	$32, %edx
	movl	$58, %ecx
	movl	$1702129221, -47(%rbp)
	movw	%ax, -43(%rbp)
	movb	$0, -41(%rbp)
	movl	$1919903264, -53(%rbp)
	movw	%dx, -49(%rbp)
	movw	%cx, -55(%rbp)
	testq	%rsi, %rsi
	je	.L266
	movq	%rsi, %rdi
	call	strlen@PLT
	movq	%rax, %r15
	testq	%r14, %r14
	je	.L267
	movq	%r14, %rdi
	call	strlen@PLT
	movl	$377, %edx
	leaq	.LC0(%rip), %rsi
	leal	13(%r15,%rax), %r15d
	movslq	%r15d, %r15
	movq	%r15, %rdi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L270
	leaq	-47(%rbp), %rsi
	movq	%r15, %rdx
	movq	%rax, %rdi
	call	OPENSSL_strlcpy@PLT
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	OPENSSL_strlcat@PLT
	leaq	-53(%rbp), %rsi
	movq	%r15, %rdx
	movq	%r12, %rdi
	call	OPENSSL_strlcat@PLT
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	OPENSSL_strlcat@PLT
.L269:
	leaq	-55(%rbp), %rsi
	movq	%r15, %rdx
	movq	%r12, %rdi
	call	OPENSSL_strlcat@PLT
	jmp	.L263
	.p2align 4,,10
	.p2align 3
.L267:
	addl	$8, %r15d
	movl	$377, %edx
	leaq	.LC0(%rip), %rsi
	movslq	%r15d, %r15
	movq	%r15, %rdi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L270
	leaq	-47(%rbp), %rsi
	movq	%r15, %rdx
	movq	%r12, %rdi
	call	OPENSSL_strlcpy@PLT
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	OPENSSL_strlcat@PLT
	jmp	.L269
	.p2align 4,,10
	.p2align 3
.L270:
	movl	$378, %r8d
	movl	$65, %edx
	movl	$121, %esi
	movl	$40, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L266:
	xorl	%r12d, %r12d
	jmp	.L263
.L281:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE821:
	.size	UI_construct_prompt, .-UI_construct_prompt
	.p2align 4
	.globl	UI_add_user_data
	.type	UI_add_user_data, @function
UI_add_user_data:
.LFB822:
	.cfi_startproc
	endbr64
	movl	32(%rdi), %eax
	movq	16(%rdi), %r8
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	testb	$2, %al
	jne	.L288
.L283:
	andl	$-3, %eax
	movq	%r12, 16(%rbx)
	movl	%eax, 32(%rbx)
	popq	%rbx
	movq	%r8, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L288:
	.cfi_restore_state
	movq	(%rdi), %rax
	movq	%r8, %rsi
	call	*56(%rax)
	movl	32(%rbx), %eax
	xorl	%r8d, %r8d
	jmp	.L283
	.cfi_endproc
.LFE822:
	.size	UI_add_user_data, .-UI_add_user_data
	.p2align 4
	.globl	UI_dup_user_data
	.type	UI_dup_user_data, @function
UI_dup_user_data:
.LFB823:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %rax
	movq	48(%rax), %rdx
	testq	%rdx, %rdx
	je	.L290
	cmpq	$0, 56(%rax)
	je	.L290
	movq	%rdi, %rbx
	call	*%rdx
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L302
	movl	32(%rbx), %edx
	testb	$2, %dl
	jne	.L303
.L294:
	orl	$2, %edx
	movq	%r12, 16(%rbx)
	xorl	%eax, %eax
	movl	%edx, 32(%rbx)
.L289:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L303:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	16(%rbx), %rsi
	movq	%rbx, %rdi
	call	*56(%rax)
	movl	32(%rbx), %edx
	jmp	.L294
.L290:
	movl	$411, %r8d
	movl	$112, %edx
	movl	$118, %esi
	movl	$40, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-1, %eax
	jmp	.L289
.L302:
	movl	$417, %r8d
	movl	$65, %edx
	movl	$118, %esi
	movl	$40, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-1, %eax
	jmp	.L289
	.cfi_endproc
.LFE823:
	.size	UI_dup_user_data, .-UI_dup_user_data
	.p2align 4
	.globl	UI_get0_user_data
	.type	UI_get0_user_data, @function
UI_get0_user_data:
.LFB824:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	ret
	.cfi_endproc
.LFE824:
	.size	UI_get0_user_data, .-UI_get0_user_data
	.p2align 4
	.globl	UI_get0_result
	.type	UI_get0_result, @function
UI_get0_result:
.LFB825:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	testl	%esi, %esi
	js	.L311
	movq	%rdi, %rbx
	movq	8(%rdi), %rdi
	movl	%esi, %r12d
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r12d
	jge	.L312
	movq	8(%rbx), %rdi
	movl	%r12d, %esi
	call	OPENSSL_sk_value@PLT
	xorl	%r8d, %r8d
	movl	(%rax), %ecx
	leal	-1(%rcx), %edx
	cmpl	$1, %edx
	ja	.L305
	movq	24(%rax), %r8
.L305:
	popq	%rbx
	movq	%r8, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L312:
	.cfi_restore_state
	movl	$439, %r8d
	movl	$102, %edx
	movl	$107, %esi
	movl	$40, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%r8d, %r8d
	popq	%rbx
	popq	%r12
	movq	%r8, %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L311:
	.cfi_restore_state
	movl	$435, %r8d
	movl	$103, %edx
	movl	$107, %esi
	movl	$40, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%r8d, %r8d
	popq	%rbx
	popq	%r12
	movq	%r8, %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE825:
	.size	UI_get0_result, .-UI_get0_result
	.p2align 4
	.globl	UI_get_result_length
	.type	UI_get_result_length, @function
UI_get_result_length:
.LFB826:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	testl	%esi, %esi
	js	.L319
	movq	%rdi, %rbx
	movq	8(%rdi), %rdi
	movl	%esi, %r12d
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r12d
	jge	.L320
	movq	8(%rbx), %rdi
	movl	%r12d, %esi
	call	OPENSSL_sk_value@PLT
	movl	(%rax), %ecx
	leal	-1(%rcx), %edx
	cmpl	$1, %edx
	ja	.L317
	movl	32(%rax), %eax
.L313:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L317:
	.cfi_restore_state
	movl	$-1, %eax
	jmp	.L313
	.p2align 4,,10
	.p2align 3
.L320:
	movl	$452, %r8d
	movl	$102, %edx
	movl	$119, %esi
	movl	$40, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-1, %eax
	jmp	.L313
.L319:
	movl	$448, %r8d
	movl	$103, %edx
	movl	$119, %esi
	movl	$40, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-1, %eax
	jmp	.L313
	.cfi_endproc
.LFE826:
	.size	UI_get_result_length, .-UI_get_result_length
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"flushing"
.LC2:
	.string	"reading strings"
.LC3:
	.string	"closing session"
.LC4:
	.string	"opening session"
.LC5:
	.string	"writing strings"
.LC6:
	.string	"processing"
.LC7:
	.string	"while "
	.text
	.p2align 4
	.globl	UI_process
	.type	UI_process, @function
UI_process:
.LFB828:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	(%rdi), %rax
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L326
	call	*%rax
	testl	%eax, %eax
	jle	.L357
.L326:
	testb	$1, 33(%r12)
	jne	.L358
.L324:
	movq	8(%r12), %rdi
	xorl	%ebx, %ebx
	jmp	.L327
	.p2align 4,,10
	.p2align 3
.L329:
	movq	16(%rax), %r13
	movq	8(%r12), %rdi
	testq	%r13, %r13
	je	.L328
	movl	%ebx, %esi
	call	OPENSSL_sk_value@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	*%r13
	testl	%eax, %eax
	jle	.L344
	movq	8(%r12), %rdi
.L328:
	addl	$1, %ebx
.L327:
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	movq	(%r12), %rax
	jl	.L329
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L330
	movq	%r12, %rdi
	call	*%rax
	cmpl	$-1, %eax
	je	.L336
	testl	%eax, %eax
	je	.L345
.L330:
	movq	8(%r12), %rdi
	xorl	%ebx, %ebx
.L333:
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jge	.L359
	movq	(%r12), %rax
	movq	8(%r12), %rdi
	movq	32(%rax), %r13
	testq	%r13, %r13
	je	.L335
	movl	%ebx, %esi
	call	OPENSSL_sk_value@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	*%r13
	cmpl	$-1, %eax
	je	.L336
	testl	%eax, %eax
	je	.L346
	movq	8(%r12), %rdi
.L335:
	addl	$1, %ebx
	jmp	.L333
	.p2align 4,,10
	.p2align 3
.L344:
	leaq	.LC5(%rip), %r14
.L325:
	movq	(%r12), %rax
	movq	40(%rax), %rax
	testq	%rax, %rax
	je	.L339
	movq	%r12, %rdi
	call	*%rax
.L339:
	movl	$545, %r8d
	movl	$107, %edx
	movl	$113, %esi
	movl	$40, %edi
	leaq	.LC0(%rip), %rcx
	movl	$-1, %r13d
	call	ERR_put_error@PLT
	movq	%r14, %rdx
	movl	$2, %edi
	xorl	%eax, %eax
	leaq	.LC7(%rip), %rsi
	call	ERR_add_error_data@PLT
.L321:
	popq	%rbx
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L358:
	.cfi_restore_state
	movq	%r12, %rsi
	leaq	print_error(%rip), %rdi
	call	ERR_print_errors_cb@PLT
	jmp	.L324
	.p2align 4,,10
	.p2align 3
.L336:
	andl	$-2, 32(%r12)
	leaq	.LC6(%rip), %r14
	movl	$-2, %r13d
.L334:
	movq	(%r12), %rax
	movq	40(%rax), %rax
	testq	%rax, %rax
	je	.L321
	movq	%r12, %rdi
	call	*%rax
	testl	%eax, %eax
	jg	.L321
	testq	%r14, %r14
	jne	.L339
	leaq	.LC3(%rip), %r14
	jmp	.L339
	.p2align 4,,10
	.p2align 3
.L346:
.L332:
	endbr64
	leaq	.LC2(%rip), %r14
	jmp	.L325
	.p2align 4,,10
	.p2align 3
.L345:
	leaq	.LC1(%rip), %r14
	jmp	.L325
	.p2align 4,,10
	.p2align 3
.L357:
	leaq	.LC4(%rip), %r14
	jmp	.L325
.L359:
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	jmp	.L334
	.cfi_endproc
.LFE828:
	.size	UI_process, .-UI_process
	.p2align 4
	.globl	UI_ctrl
	.type	UI_ctrl, @function
UI_ctrl:
.LFB829:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testq	%rdi, %rdi
	je	.L369
	cmpl	$1, %esi
	je	.L363
	cmpl	$2, %esi
	je	.L364
	movl	$572, %r8d
	movl	$106, %edx
	movl	$111, %esi
	movl	$40, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-1, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L364:
	.cfi_restore_state
	movl	32(%rdi), %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	andl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L363:
	.cfi_restore_state
	movl	32(%rdi), %ecx
	movl	%ecx, %eax
	movl	%ecx, %esi
	orb	$1, %ch
	shrl	$8, %eax
	andl	$-257, %esi
	andl	$1, %eax
	testq	%rdx, %rdx
	cmove	%esi, %ecx
	movl	%ecx, 32(%rdi)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L369:
	.cfi_restore_state
	movl	$554, %r8d
	movl	$67, %edx
	movl	$111, %esi
	movl	$40, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-1, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE829:
	.size	UI_ctrl, .-UI_ctrl
	.p2align 4
	.globl	UI_set_ex_data
	.type	UI_set_ex_data, @function
UI_set_ex_data:
.LFB830:
	.cfi_startproc
	endbr64
	addq	$24, %rdi
	jmp	CRYPTO_set_ex_data@PLT
	.cfi_endproc
.LFE830:
	.size	UI_set_ex_data, .-UI_set_ex_data
	.p2align 4
	.globl	UI_get_ex_data
	.type	UI_get_ex_data, @function
UI_get_ex_data:
.LFB831:
	.cfi_startproc
	endbr64
	addq	$24, %rdi
	jmp	CRYPTO_get_ex_data@PLT
	.cfi_endproc
.LFE831:
	.size	UI_get_ex_data, .-UI_get_ex_data
	.p2align 4
	.globl	UI_get_method
	.type	UI_get_method, @function
UI_get_method:
.LFB832:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE832:
	.size	UI_get_method, .-UI_get_method
	.p2align 4
	.globl	UI_set_method
	.type	UI_set_method, @function
UI_set_method:
.LFB833:
	.cfi_startproc
	endbr64
	movq	%rsi, (%rdi)
	movq	%rsi, %rax
	ret
	.cfi_endproc
.LFE833:
	.size	UI_set_method, .-UI_set_method
	.p2align 4
	.globl	UI_create_method
	.type	UI_create_method, @function
UI_create_method:
.LFB834:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$601, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	movl	$80, %edi
	pushq	%r12
	.cfi_offset 12, -32
	call	CRYPTO_zalloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L375
	movl	$602, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r13, %rdi
	call	CRYPTO_strdup@PLT
	movq	%rax, (%r12)
	testq	%rax, %rax
	je	.L376
	leaq	72(%r12), %rdx
	movq	%r12, %rsi
	movl	$14, %edi
	call	CRYPTO_new_ex_data@PLT
	testl	%eax, %eax
	je	.L385
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L385:
	.cfi_restore_state
	movq	(%r12), %rax
.L376:
	movl	$606, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rax, %rdi
	call	CRYPTO_free@PLT
.L375:
	movq	%r12, %rdi
	movl	$607, %edx
	leaq	.LC0(%rip), %rsi
	xorl	%r12d, %r12d
	call	CRYPTO_free@PLT
	movl	$608, %r8d
	movl	$65, %edx
	leaq	.LC0(%rip), %rcx
	movl	$112, %esi
	movl	$40, %edi
	call	ERR_put_error@PLT
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE834:
	.size	UI_create_method, .-UI_create_method
	.p2align 4
	.globl	UI_destroy_method
	.type	UI_destroy_method, @function
UI_destroy_method:
.LFB835:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L386
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	72(%rdi), %rdx
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movl	$14, %edi
	subq	$8, %rsp
	call	CRYPTO_free_ex_data@PLT
	movq	(%r12), %rdi
	movl	$625, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, (%r12)
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$627, %edx
	popq	%r12
	.cfi_restore 12
	leaq	.LC0(%rip), %rsi
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_free@PLT
	.p2align 4,,10
	.p2align 3
.L386:
	ret
	.cfi_endproc
.LFE835:
	.size	UI_destroy_method, .-UI_destroy_method
	.p2align 4
	.globl	UI_method_set_opener
	.type	UI_method_set_opener, @function
UI_method_set_opener:
.LFB836:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L393
	movq	%rsi, 8(%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L393:
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE836:
	.size	UI_method_set_opener, .-UI_method_set_opener
	.p2align 4
	.globl	UI_method_set_writer
	.type	UI_method_set_writer, @function
UI_method_set_writer:
.LFB837:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L396
	movq	%rsi, 16(%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L396:
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE837:
	.size	UI_method_set_writer, .-UI_method_set_writer
	.p2align 4
	.globl	UI_method_set_flusher
	.type	UI_method_set_flusher, @function
UI_method_set_flusher:
.LFB838:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L399
	movq	%rsi, 24(%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L399:
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE838:
	.size	UI_method_set_flusher, .-UI_method_set_flusher
	.p2align 4
	.globl	UI_method_set_reader
	.type	UI_method_set_reader, @function
UI_method_set_reader:
.LFB839:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L402
	movq	%rsi, 32(%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L402:
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE839:
	.size	UI_method_set_reader, .-UI_method_set_reader
	.p2align 4
	.globl	UI_method_set_closer
	.type	UI_method_set_closer, @function
UI_method_set_closer:
.LFB840:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L405
	movq	%rsi, 40(%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L405:
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE840:
	.size	UI_method_set_closer, .-UI_method_set_closer
	.p2align 4
	.globl	UI_method_set_data_duplicator
	.type	UI_method_set_data_duplicator, @function
UI_method_set_data_duplicator:
.LFB841:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdx, -8(%rbp)
	testq	%rdi, %rdi
	je	.L408
	movq	%rsi, %xmm0
	xorl	%eax, %eax
	movhps	-8(%rbp), %xmm0
	movups	%xmm0, 48(%rdi)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L408:
	.cfi_restore_state
	movl	$-1, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE841:
	.size	UI_method_set_data_duplicator, .-UI_method_set_data_duplicator
	.p2align 4
	.globl	UI_method_set_prompt_constructor
	.type	UI_method_set_prompt_constructor, @function
UI_method_set_prompt_constructor:
.LFB842:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L412
	movq	%rsi, 64(%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L412:
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE842:
	.size	UI_method_set_prompt_constructor, .-UI_method_set_prompt_constructor
	.p2align 4
	.globl	UI_method_set_ex_data
	.type	UI_method_set_ex_data, @function
UI_method_set_ex_data:
.LFB843:
	.cfi_startproc
	endbr64
	addq	$72, %rdi
	jmp	CRYPTO_set_ex_data@PLT
	.cfi_endproc
.LFE843:
	.size	UI_method_set_ex_data, .-UI_method_set_ex_data
	.p2align 4
	.globl	UI_method_get_opener
	.type	UI_method_get_opener, @function
UI_method_get_opener:
.LFB844:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L414
	movq	8(%rdi), %rax
.L414:
	ret
	.cfi_endproc
.LFE844:
	.size	UI_method_get_opener, .-UI_method_get_opener
	.p2align 4
	.globl	UI_method_get_writer
	.type	UI_method_get_writer, @function
UI_method_get_writer:
.LFB845:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L418
	movq	16(%rdi), %rax
.L418:
	ret
	.cfi_endproc
.LFE845:
	.size	UI_method_get_writer, .-UI_method_get_writer
	.p2align 4
	.globl	UI_method_get_flusher
	.type	UI_method_get_flusher, @function
UI_method_get_flusher:
.LFB846:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L422
	movq	24(%rdi), %rax
.L422:
	ret
	.cfi_endproc
.LFE846:
	.size	UI_method_get_flusher, .-UI_method_get_flusher
	.p2align 4
	.globl	UI_method_get_reader
	.type	UI_method_get_reader, @function
UI_method_get_reader:
.LFB847:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L426
	movq	32(%rdi), %rax
.L426:
	ret
	.cfi_endproc
.LFE847:
	.size	UI_method_get_reader, .-UI_method_get_reader
	.p2align 4
	.globl	UI_method_get_closer
	.type	UI_method_get_closer, @function
UI_method_get_closer:
.LFB848:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L430
	movq	40(%rdi), %rax
.L430:
	ret
	.cfi_endproc
.LFE848:
	.size	UI_method_get_closer, .-UI_method_get_closer
	.p2align 4
	.globl	UI_method_get_prompt_constructor
	.type	UI_method_get_prompt_constructor, @function
UI_method_get_prompt_constructor:
.LFB849:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L434
	movq	64(%rdi), %rax
.L434:
	ret
	.cfi_endproc
.LFE849:
	.size	UI_method_get_prompt_constructor, .-UI_method_get_prompt_constructor
	.p2align 4
	.globl	UI_method_get_data_duplicator
	.type	UI_method_get_data_duplicator, @function
UI_method_get_data_duplicator:
.LFB850:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L438
	movq	48(%rdi), %rax
.L438:
	ret
	.cfi_endproc
.LFE850:
	.size	UI_method_get_data_duplicator, .-UI_method_get_data_duplicator
	.p2align 4
	.globl	UI_method_get_data_destructor
	.type	UI_method_get_data_destructor, @function
UI_method_get_data_destructor:
.LFB851:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L442
	movq	56(%rdi), %rax
.L442:
	ret
	.cfi_endproc
.LFE851:
	.size	UI_method_get_data_destructor, .-UI_method_get_data_destructor
	.p2align 4
	.globl	UI_method_get_ex_data
	.type	UI_method_get_ex_data, @function
UI_method_get_ex_data:
.LFB852:
	.cfi_startproc
	endbr64
	addq	$72, %rdi
	jmp	CRYPTO_get_ex_data@PLT
	.cfi_endproc
.LFE852:
	.size	UI_method_get_ex_data, .-UI_method_get_ex_data
	.p2align 4
	.globl	UI_get_string_type
	.type	UI_get_string_type, @function
UI_get_string_type:
.LFB853:
	.cfi_startproc
	endbr64
	movl	(%rdi), %eax
	ret
	.cfi_endproc
.LFE853:
	.size	UI_get_string_type, .-UI_get_string_type
	.p2align 4
	.globl	UI_get_input_flags
	.type	UI_get_input_flags, @function
UI_get_input_flags:
.LFB854:
	.cfi_startproc
	endbr64
	movl	16(%rdi), %eax
	ret
	.cfi_endproc
.LFE854:
	.size	UI_get_input_flags, .-UI_get_input_flags
	.p2align 4
	.globl	UI_get0_output_string
	.type	UI_get0_output_string, @function
UI_get0_output_string:
.LFB855:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	ret
	.cfi_endproc
.LFE855:
	.size	UI_get0_output_string, .-UI_get0_output_string
	.p2align 4
	.globl	UI_get0_action_string
	.type	UI_get0_action_string, @function
UI_get0_action_string:
.LFB856:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpl	$3, (%rdi)
	jne	.L450
	movq	40(%rdi), %rax
.L450:
	ret
	.cfi_endproc
.LFE856:
	.size	UI_get0_action_string, .-UI_get0_action_string
	.p2align 4
	.globl	UI_get0_result_string
	.type	UI_get0_result_string, @function
UI_get0_result_string:
.LFB857:
	.cfi_startproc
	endbr64
	movl	(%rdi), %eax
	xorl	%r8d, %r8d
	subl	$1, %eax
	cmpl	$1, %eax
	ja	.L453
	movq	24(%rdi), %r8
.L453:
	movq	%r8, %rax
	ret
	.cfi_endproc
.LFE857:
	.size	UI_get0_result_string, .-UI_get0_result_string
	.p2align 4
	.globl	UI_get_result_string_length
	.type	UI_get_result_string_length, @function
UI_get_result_string_length:
.LFB858:
	.cfi_startproc
	endbr64
	movl	(%rdi), %eax
	subl	$1, %eax
	cmpl	$1, %eax
	ja	.L458
	movl	32(%rdi), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L458:
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE858:
	.size	UI_get_result_string_length, .-UI_get_result_string_length
	.p2align 4
	.globl	UI_get0_test_string
	.type	UI_get0_test_string, @function
UI_get0_test_string:
.LFB859:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpl	$2, (%rdi)
	jne	.L459
	movq	48(%rdi), %rax
.L459:
	ret
	.cfi_endproc
.LFE859:
	.size	UI_get0_test_string, .-UI_get0_test_string
	.p2align 4
	.globl	UI_get_result_minsize
	.type	UI_get_result_minsize, @function
UI_get_result_minsize:
.LFB860:
	.cfi_startproc
	endbr64
	movl	(%rdi), %eax
	subl	$1, %eax
	cmpl	$1, %eax
	ja	.L464
	movl	40(%rdi), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L464:
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE860:
	.size	UI_get_result_minsize, .-UI_get_result_minsize
	.p2align 4
	.globl	UI_get_result_maxsize
	.type	UI_get_result_maxsize, @function
UI_get_result_maxsize:
.LFB861:
	.cfi_startproc
	endbr64
	movl	(%rdi), %eax
	subl	$1, %eax
	cmpl	$1, %eax
	ja	.L467
	movl	44(%rdi), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L467:
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE861:
	.size	UI_get_result_maxsize, .-UI_get_result_maxsize
	.section	.rodata.str1.1
.LC8:
	.string	"%d"
.LC9:
	.string	" characters"
.LC10:
	.string	" to "
.LC11:
	.string	"You must type in "
	.text
	.p2align 4
	.globl	UI_set_result_ex
	.type	UI_set_result_ex, @function
UI_set_result_ex:
.LFB863:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	(%rsi), %eax
	andl	$-2, 32(%rdi)
	cmpl	$2, %eax
	jbe	.L491
	cmpl	$3, %eax
	jne	.L484
	movq	24(%rsi), %rax
	testq	%rax, %rax
	je	.L492
	movb	$0, (%rax)
	movzbl	(%rdx), %esi
	testb	%sil, %sil
	je	.L484
	movq	48(%rbx), %r14
	jmp	.L482
	.p2align 4,,10
	.p2align 3
.L480:
	movq	56(%rbx), %r15
	movl	%r12d, %esi
	movq	%r15, %rdi
	call	strchr@PLT
	testq	%rax, %rax
	jne	.L493
	movzbl	1(%r13), %esi
	addq	$1, %r13
	testb	%sil, %sil
	je	.L484
.L482:
	movsbl	%sil, %r12d
	movq	%r14, %rdi
	movl	%r12d, %esi
	call	strchr@PLT
	testq	%rax, %rax
	je	.L480
	movq	24(%rbx), %rax
	movzbl	(%r14), %edx
	movb	%dl, (%rax)
	xorl	%eax, %eax
	jmp	.L468
	.p2align 4,,10
	.p2align 3
.L491:
	testl	%eax, %eax
	jne	.L470
.L468:
	movq	-56(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L494
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L484:
	.cfi_restore_state
	xorl	%eax, %eax
	jmp	.L468
	.p2align 4,,10
	.p2align 3
.L470:
	movl	%ecx, %r14d
	leaq	-82(%rbp), %r15
	movl	40(%rsi), %ecx
	xorl	%eax, %eax
	leaq	.LC8(%rip), %rdx
	movl	$13, %esi
	movq	%rdi, %r12
	movq	%r15, %rdi
	call	BIO_snprintf@PLT
	leaq	-69(%rbp), %r10
	movl	44(%rbx), %ecx
	xorl	%eax, %eax
	movq	%r10, %rdi
	leaq	.LC8(%rip), %rdx
	movl	$13, %esi
	movq	%r10, -104(%rbp)
	call	BIO_snprintf@PLT
	cmpl	%r14d, 40(%rbx)
	movq	-104(%rbp), %r10
	jg	.L495
	cmpl	%r14d, 44(%rbx)
	jl	.L496
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L497
	movslq	%r14d, %r12
	movq	%r13, %rsi
	movq	%r12, %rdx
	call	memcpy@PLT
	cmpl	%r14d, 44(%rbx)
	jl	.L478
	movq	24(%rbx), %rax
	movb	$0, (%rax,%r12)
.L478:
	movq	%r12, 32(%rbx)
	xorl	%eax, %eax
	jmp	.L468
	.p2align 4,,10
	.p2align 3
.L493:
	movq	24(%rbx), %rax
	movzbl	(%r15), %edx
	movb	%dl, (%rax)
	xorl	%eax, %eax
	jmp	.L468
.L492:
	movl	$934, %r8d
.L490:
	movl	$105, %edx
	movl	$120, %esi
	movl	$40, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L489:
	movl	$-1, %eax
	jmp	.L468
.L495:
	orl	$1, 32(%r12)
	movl	$905, %r8d
	movl	$101, %edx
	leaq	.LC0(%rip), %rcx
.L488:
	movl	$120, %esi
	movl	$40, %edi
	call	ERR_put_error@PLT
	movq	-104(%rbp), %r10
	movq	%r15, %rdx
	xorl	%eax, %eax
	leaq	.LC9(%rip), %r9
	leaq	.LC10(%rip), %rcx
	movl	$5, %edi
	movq	%r10, %r8
	leaq	.LC11(%rip), %rsi
	call	ERR_add_error_data@PLT
	jmp	.L489
.L496:
	movq	%r10, -104(%rbp)
	movl	$912, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$100, %edx
	orl	$1, 32(%r12)
	jmp	.L488
.L497:
	movl	$920, %r8d
	jmp	.L490
.L494:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE863:
	.size	UI_set_result_ex, .-UI_set_result_ex
	.p2align 4
	.globl	UI_set_result
	.type	UI_set_result, @function
UI_set_result:
.LFB862:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	movq	%rdx, %rdi
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	subq	$8, %rsp
	call	strlen@PLT
	addq	$8, %rsp
	movq	%r12, %rdx
	movq	%r14, %rsi
	popq	%r12
	movq	%r13, %rdi
	movq	%rax, %rcx
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	UI_set_result_ex
	.cfi_endproc
.LFE862:
	.size	UI_set_result, .-UI_set_result
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
