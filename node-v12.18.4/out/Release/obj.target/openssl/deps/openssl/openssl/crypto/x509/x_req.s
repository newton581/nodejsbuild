	.file	"x_req.c"
	.text
	.p2align 4
	.type	rinf_cb, @function
rinf_cb:
.LFB805:
	.cfi_startproc
	endbr64
	cmpl	$1, %edi
	je	.L10
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	(%rsi), %rbx
	call	OPENSSL_sk_new_null@PLT
	testq	%rax, %rax
	movq	%rax, 48(%rbx)
	setne	%al
	addq	$8, %rsp
	movzbl	%al, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE805:
	.size	rinf_cb, .-rinf_cb
	.p2align 4
	.globl	d2i_X509_REQ_INFO
	.type	d2i_X509_REQ_INFO, @function
d2i_X509_REQ_INFO:
.LFB806:
	.cfi_startproc
	endbr64
	leaq	X509_REQ_INFO_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE806:
	.size	d2i_X509_REQ_INFO, .-d2i_X509_REQ_INFO
	.p2align 4
	.globl	i2d_X509_REQ_INFO
	.type	i2d_X509_REQ_INFO, @function
i2d_X509_REQ_INFO:
.LFB807:
	.cfi_startproc
	endbr64
	leaq	X509_REQ_INFO_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE807:
	.size	i2d_X509_REQ_INFO, .-i2d_X509_REQ_INFO
	.p2align 4
	.globl	X509_REQ_INFO_new
	.type	X509_REQ_INFO_new, @function
X509_REQ_INFO_new:
.LFB808:
	.cfi_startproc
	endbr64
	leaq	X509_REQ_INFO_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE808:
	.size	X509_REQ_INFO_new, .-X509_REQ_INFO_new
	.p2align 4
	.globl	X509_REQ_INFO_free
	.type	X509_REQ_INFO_free, @function
X509_REQ_INFO_free:
.LFB809:
	.cfi_startproc
	endbr64
	leaq	X509_REQ_INFO_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE809:
	.size	X509_REQ_INFO_free, .-X509_REQ_INFO_free
	.p2align 4
	.globl	d2i_X509_REQ
	.type	d2i_X509_REQ, @function
d2i_X509_REQ:
.LFB810:
	.cfi_startproc
	endbr64
	leaq	X509_REQ_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE810:
	.size	d2i_X509_REQ, .-d2i_X509_REQ
	.p2align 4
	.globl	i2d_X509_REQ
	.type	i2d_X509_REQ, @function
i2d_X509_REQ:
.LFB811:
	.cfi_startproc
	endbr64
	leaq	X509_REQ_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE811:
	.size	i2d_X509_REQ, .-i2d_X509_REQ
	.p2align 4
	.globl	X509_REQ_new
	.type	X509_REQ_new, @function
X509_REQ_new:
.LFB812:
	.cfi_startproc
	endbr64
	leaq	X509_REQ_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE812:
	.size	X509_REQ_new, .-X509_REQ_new
	.p2align 4
	.globl	X509_REQ_free
	.type	X509_REQ_free, @function
X509_REQ_free:
.LFB813:
	.cfi_startproc
	endbr64
	leaq	X509_REQ_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE813:
	.size	X509_REQ_free, .-X509_REQ_free
	.p2align 4
	.globl	X509_REQ_dup
	.type	X509_REQ_dup, @function
X509_REQ_dup:
.LFB814:
	.cfi_startproc
	endbr64
	movq	%rdi, %rsi
	leaq	X509_REQ_it(%rip), %rdi
	jmp	ASN1_item_dup@PLT
	.cfi_endproc
.LFE814:
	.size	X509_REQ_dup, .-X509_REQ_dup
	.globl	X509_REQ_it
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"X509_REQ"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	X509_REQ_it, @object
	.size	X509_REQ_it, 56
X509_REQ_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	X509_REQ_seq_tt
	.quad	3
	.quad	X509_REQ_aux
	.quad	96
	.quad	.LC0
	.section	.rodata.str1.1
.LC1:
	.string	"req_info"
.LC2:
	.string	"sig_alg"
.LC3:
	.string	"signature"
	.section	.data.rel.ro,"aw"
	.align 32
	.type	X509_REQ_seq_tt, @object
	.size	X509_REQ_seq_tt, 120
X509_REQ_seq_tt:
	.quad	4096
	.quad	0
	.quad	0
	.quad	.LC1
	.quad	X509_REQ_INFO_it
	.quad	4096
	.quad	0
	.quad	56
	.quad	.LC2
	.quad	X509_ALGOR_it
	.quad	0
	.quad	0
	.quad	72
	.quad	.LC3
	.quad	ASN1_BIT_STRING_it
	.section	.rodata
	.align 32
	.type	X509_REQ_aux, @object
	.size	X509_REQ_aux, 40
X509_REQ_aux:
	.quad	0
	.long	1
	.long	80
	.long	88
	.zero	4
	.quad	0
	.long	0
	.zero	4
	.globl	X509_REQ_INFO_it
	.section	.rodata.str1.1
.LC4:
	.string	"X509_REQ_INFO"
	.section	.data.rel.ro.local
	.align 32
	.type	X509_REQ_INFO_it, @object
	.size	X509_REQ_INFO_it, 56
X509_REQ_INFO_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	X509_REQ_INFO_seq_tt
	.quad	4
	.quad	X509_REQ_INFO_aux
	.quad	56
	.quad	.LC4
	.section	.rodata.str1.1
.LC5:
	.string	"version"
.LC6:
	.string	"subject"
.LC7:
	.string	"pubkey"
.LC8:
	.string	"attributes"
	.section	.data.rel.ro
	.align 32
	.type	X509_REQ_INFO_seq_tt, @object
	.size	X509_REQ_INFO_seq_tt, 160
X509_REQ_INFO_seq_tt:
	.quad	0
	.quad	0
	.quad	24
	.quad	.LC5
	.quad	ASN1_INTEGER_it
	.quad	0
	.quad	0
	.quad	32
	.quad	.LC6
	.quad	X509_NAME_it
	.quad	0
	.quad	0
	.quad	40
	.quad	.LC7
	.quad	X509_PUBKEY_it
	.quad	139
	.quad	0
	.quad	48
	.quad	.LC8
	.quad	X509_ATTRIBUTE_it
	.section	.data.rel.ro.local
	.align 32
	.type	X509_REQ_INFO_aux, @object
	.size	X509_REQ_INFO_aux, 40
X509_REQ_INFO_aux:
	.quad	0
	.long	2
	.long	0
	.long	0
	.zero	4
	.quad	rinf_cb
	.long	0
	.zero	4
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
