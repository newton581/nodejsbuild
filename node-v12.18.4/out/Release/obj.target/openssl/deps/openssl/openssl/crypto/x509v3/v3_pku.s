	.file	"v3_pku.c"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	""
.LC1:
	.string	"%*s"
.LC2:
	.string	"Not Before: "
.LC3:
	.string	", "
.LC4:
	.string	"Not After: "
	.text
	.p2align 4
	.type	i2r_PKEY_USAGE_PERIOD, @function
i2r_PKEY_USAGE_PERIOD:
.LFB1324:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	movl	%ecx, %edx
	pushq	%rbx
	.cfi_offset 3, -32
	leaq	.LC0(%rip), %rcx
	movq	%rsi, %rbx
	movq	%r12, %rdi
	leaq	.LC1(%rip), %rsi
	call	BIO_printf@PLT
	cmpq	$0, (%rbx)
	je	.L2
	movl	$12, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_write@PLT
	movq	(%rbx), %rsi
	movq	%r12, %rdi
	call	ASN1_GENERALIZEDTIME_print@PLT
	cmpq	$0, 8(%rbx)
	je	.L4
	movl	$2, %edx
	leaq	.LC3(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_write@PLT
.L2:
	cmpq	$0, 8(%rbx)
	je	.L4
	leaq	.LC4(%rip), %rsi
	movq	%r12, %rdi
	movl	$11, %edx
	call	BIO_write@PLT
	movq	8(%rbx), %rsi
	movq	%r12, %rdi
	call	ASN1_GENERALIZEDTIME_print@PLT
.L4:
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1324:
	.size	i2r_PKEY_USAGE_PERIOD, .-i2r_PKEY_USAGE_PERIOD
	.p2align 4
	.globl	d2i_PKEY_USAGE_PERIOD
	.type	d2i_PKEY_USAGE_PERIOD, @function
d2i_PKEY_USAGE_PERIOD:
.LFB1320:
	.cfi_startproc
	endbr64
	leaq	PKEY_USAGE_PERIOD_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE1320:
	.size	d2i_PKEY_USAGE_PERIOD, .-d2i_PKEY_USAGE_PERIOD
	.p2align 4
	.globl	i2d_PKEY_USAGE_PERIOD
	.type	i2d_PKEY_USAGE_PERIOD, @function
i2d_PKEY_USAGE_PERIOD:
.LFB1321:
	.cfi_startproc
	endbr64
	leaq	PKEY_USAGE_PERIOD_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE1321:
	.size	i2d_PKEY_USAGE_PERIOD, .-i2d_PKEY_USAGE_PERIOD
	.p2align 4
	.globl	PKEY_USAGE_PERIOD_new
	.type	PKEY_USAGE_PERIOD_new, @function
PKEY_USAGE_PERIOD_new:
.LFB1322:
	.cfi_startproc
	endbr64
	leaq	PKEY_USAGE_PERIOD_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE1322:
	.size	PKEY_USAGE_PERIOD_new, .-PKEY_USAGE_PERIOD_new
	.p2align 4
	.globl	PKEY_USAGE_PERIOD_free
	.type	PKEY_USAGE_PERIOD_free, @function
PKEY_USAGE_PERIOD_free:
.LFB1323:
	.cfi_startproc
	endbr64
	leaq	PKEY_USAGE_PERIOD_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE1323:
	.size	PKEY_USAGE_PERIOD_free, .-PKEY_USAGE_PERIOD_free
	.globl	PKEY_USAGE_PERIOD_it
	.section	.rodata.str1.1
.LC5:
	.string	"PKEY_USAGE_PERIOD"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	PKEY_USAGE_PERIOD_it, @object
	.size	PKEY_USAGE_PERIOD_it, 56
PKEY_USAGE_PERIOD_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	PKEY_USAGE_PERIOD_seq_tt
	.quad	2
	.quad	0
	.quad	16
	.quad	.LC5
	.section	.rodata.str1.1
.LC6:
	.string	"notBefore"
.LC7:
	.string	"notAfter"
	.section	.data.rel.ro,"aw"
	.align 32
	.type	PKEY_USAGE_PERIOD_seq_tt, @object
	.size	PKEY_USAGE_PERIOD_seq_tt, 80
PKEY_USAGE_PERIOD_seq_tt:
	.quad	137
	.quad	0
	.quad	0
	.quad	.LC6
	.quad	ASN1_GENERALIZEDTIME_it
	.quad	137
	.quad	1
	.quad	8
	.quad	.LC7
	.quad	ASN1_GENERALIZEDTIME_it
	.globl	v3_pkey_usage_period
	.section	.data.rel.ro.local
	.align 32
	.type	v3_pkey_usage_period, @object
	.size	v3_pkey_usage_period, 104
v3_pkey_usage_period:
	.long	84
	.long	0
	.quad	PKEY_USAGE_PERIOD_it
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	i2r_PKEY_USAGE_PERIOD
	.quad	0
	.quad	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
