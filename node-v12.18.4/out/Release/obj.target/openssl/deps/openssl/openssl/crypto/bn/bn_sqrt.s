	.file	"bn_sqrt.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/bn/bn_sqrt.c"
	.text
	.p2align 4
	.globl	BN_mod_sqrt
	.type	BN_mod_sqrt, @function
BN_mod_sqrt:
.LFB252:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%rdx, %rdi
	subq	$88, %rsp
	call	BN_is_odd@PLT
	testl	%eax, %eax
	je	.L5
	movl	$1, %esi
	movq	%r14, %rdi
	call	BN_abs_is_word@PLT
	testl	%eax, %eax
	je	.L219
.L5:
	movl	$2, %esi
	movq	%r14, %rdi
	call	BN_abs_is_word@PLT
	testl	%eax, %eax
	je	.L220
	testq	%rbx, %rbx
	je	.L221
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	BN_is_bit_set@PLT
.L210:
	movslq	%eax, %rsi
	movq	%rbx, %rdi
	movq	%rbx, %r15
	call	BN_set_word@PLT
	testl	%eax, %eax
	je	.L215
.L1:
	addq	$88, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L223:
	.cfi_restore_state
	call	BN_new@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L214
	movq	%r12, %rdi
	call	BN_is_one@PLT
.L217:
	movslq	%eax, %rsi
	movq	%r15, %rdi
	call	BN_set_word@PLT
	testl	%eax, %eax
	jne	.L1
	movq	%r15, %rdi
	call	BN_free@PLT
.L215:
	xorl	%r15d, %r15d
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L221:
	call	BN_new@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	jne	.L222
.L214:
	xorl	%r15d, %r15d
.L45:
	movq	%r13, %rdi
	call	BN_CTX_end@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L219:
	movq	%r12, %rdi
	call	BN_is_zero@PLT
	testl	%eax, %eax
	je	.L12
.L15:
	testq	%rbx, %rbx
	je	.L223
	movq	%r12, %rdi
	call	BN_is_one@PLT
	jmp	.L210
	.p2align 4,,10
	.p2align 3
.L220:
	movl	$41, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$112, %edx
	xorl	%r15d, %r15d
	movl	$121, %esi
	movl	$3, %edi
	call	ERR_put_error@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L12:
	movq	%r12, %rdi
	call	BN_is_one@PLT
	testl	%eax, %eax
	jne	.L15
	movq	%r13, %rdi
	call	BN_CTX_start@PLT
	movq	%r13, %rdi
	call	BN_CTX_get@PLT
	movq	%r13, %rdi
	movq	%rax, -56(%rbp)
	call	BN_CTX_get@PLT
	movq	%r13, %rdi
	movq	%rax, -88(%rbp)
	call	BN_CTX_get@PLT
	movq	%r13, %rdi
	movq	%rax, -64(%rbp)
	call	BN_CTX_get@PLT
	movq	%r13, %rdi
	movq	%rax, -96(%rbp)
	call	BN_CTX_get@PLT
	movq	%r13, %rdi
	movq	%rax, -72(%rbp)
	call	BN_CTX_get@PLT
	movq	%rax, -80(%rbp)
	testq	%rax, %rax
	je	.L214
	testq	%rbx, %rbx
	je	.L224
	movq	-56(%rbp), %rdi
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	%r12, %rsi
	call	BN_nnmod@PLT
	testl	%eax, %eax
	je	.L214
	movq	%rbx, %r15
.L57:
	movl	$1, %r12d
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L23:
	addl	$1, %r12d
.L22:
	movl	%r12d, %esi
	movq	%r14, %rdi
	call	BN_is_bit_set@PLT
	testl	%eax, %eax
	je	.L23
	cmpl	$1, %r12d
	je	.L225
	cmpl	$2, %r12d
	je	.L226
	movq	-64(%rbp), %rdi
	movq	%r14, %rsi
	call	BN_copy@PLT
	testq	%rax, %rax
	je	.L26
	movq	-64(%rbp), %rax
	movl	$2, %r8d
	movl	%r12d, -116(%rbp)
	movq	%r15, -104(%rbp)
	movq	-80(%rbp), %r12
	movq	%rbx, -112(%rbp)
	movq	%rax, %r15
	movq	%r8, %rbx
	movl	$0, 16(%rax)
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L34:
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	BN_kronecker@PLT
	cmpl	$-1, %eax
	jl	.L201
	testl	%eax, %eax
	je	.L227
	cmpl	$1, %eax
	jne	.L38
	addq	$1, %rbx
	cmpq	$82, %rbx
	je	.L228
.L39:
	cmpq	$21, %rbx
	ja	.L30
.L33:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	BN_set_word@PLT
	testl	%eax, %eax
	jne	.L34
.L201:
	movq	-104(%rbp), %r15
	movq	-112(%rbp), %rbx
.L26:
	cmpq	%r15, %rbx
	je	.L214
.L56:
	movq	%r15, %rdi
	call	BN_clear_free@PLT
	jmp	.L214
.L224:
	call	BN_new@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L214
	movq	-56(%rbp), %rdi
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	%r12, %rsi
	call	BN_nnmod@PLT
	testl	%eax, %eax
	jne	.L57
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L226:
	movq	-96(%rbp), %r12
	movq	-56(%rbp), %rsi
	movq	%r14, %rdx
	movq	%r12, %rdi
	call	BN_mod_lshift1_quick@PLT
	testl	%eax, %eax
	je	.L26
	movq	-64(%rbp), %rdi
	movl	$3, %edx
	movq	%r14, %rsi
	call	BN_rshift@PLT
	testl	%eax, %eax
	je	.L26
	movq	-64(%rbp), %rax
	movq	-88(%rbp), %rdi
	movq	%r13, %r8
	movq	%r14, %rcx
	movq	%r12, %rsi
	movl	$0, 16(%rax)
	movq	%rax, %rdx
	call	BN_mod_exp@PLT
	testl	%eax, %eax
	je	.L26
	movq	-88(%rbp), %rsi
	movq	-80(%rbp), %rdi
	movq	%r13, %rcx
	movq	%r14, %rdx
	call	BN_mod_sqr@PLT
	testl	%eax, %eax
	je	.L26
	movq	-80(%rbp), %rdx
	movq	%r13, %r8
	movq	%r14, %rcx
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	BN_mod_mul@PLT
	testl	%eax, %eax
	je	.L26
	movl	$1, %esi
	movq	%r12, %rdi
	call	BN_sub_word@PLT
	testl	%eax, %eax
	je	.L26
	movq	-88(%rbp), %rdx
	movq	-56(%rbp), %rsi
	movq	%r13, %r8
	movq	%r14, %rcx
	movq	-72(%rbp), %rdi
	call	BN_mod_mul@PLT
	testl	%eax, %eax
	je	.L26
	movq	%r12, %rdx
	movq	-72(%rbp), %r12
	movq	%r13, %r8
	movq	%r14, %rcx
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	BN_mod_mul@PLT
	testl	%eax, %eax
	je	.L26
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	BN_copy@PLT
	testq	%rax, %rax
	je	.L26
.L28:
	movq	-72(%rbp), %rdi
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	%r15, %rsi
	call	BN_mod_sqr@PLT
	testl	%eax, %eax
	je	.L26
	movq	-56(%rbp), %rsi
	movq	-72(%rbp), %rdi
	call	BN_cmp@PLT
	testl	%eax, %eax
	je	.L45
	movl	$345, %r8d
.L211:
	movl	$111, %edx
	movl	$121, %esi
	movl	$3, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L225:
	movq	-64(%rbp), %r12
	movl	$2, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	BN_rshift@PLT
	testl	%eax, %eax
	je	.L26
	movl	$0, 16(%r12)
	movl	$1, %esi
	movq	%r12, %rdi
	call	BN_add_word@PLT
	testl	%eax, %eax
	je	.L26
	movq	-56(%rbp), %rsi
	movq	%r13, %r8
	movq	%r14, %rcx
	movq	%r12, %rdx
	movq	%r15, %rdi
	call	BN_mod_exp@PLT
	testl	%eax, %eax
	je	.L26
	jmp	.L28
.L38:
	addl	$1, %eax
	movl	-116(%rbp), %r12d
	movq	-104(%rbp), %r15
	movq	-112(%rbp), %rbx
	jne	.L40
	movq	-64(%rbp), %rsi
	movl	%r12d, %edx
	movq	%rsi, %rdi
	call	BN_rshift@PLT
	testl	%eax, %eax
	je	.L26
	movq	-80(%rbp), %rsi
	movq	-64(%rbp), %rdx
	movq	%r13, %r8
	movq	%r14, %rcx
	movq	%rsi, %rdi
	call	BN_mod_exp@PLT
	testl	%eax, %eax
	je	.L26
	movq	-80(%rbp), %rdi
	call	BN_is_one@PLT
	testl	%eax, %eax
	jne	.L229
	movq	-64(%rbp), %rsi
	movq	-96(%rbp), %rdi
	call	BN_rshift1@PLT
	testl	%eax, %eax
	je	.L26
	movq	-96(%rbp), %rdi
	call	BN_is_zero@PLT
	testl	%eax, %eax
	je	.L43
	movq	-56(%rbp), %rsi
	movq	-96(%rbp), %rdi
	movq	%r13, %rcx
	movq	%r14, %rdx
	call	BN_nnmod@PLT
	testl	%eax, %eax
	je	.L26
	movq	-96(%rbp), %rdi
	call	BN_is_zero@PLT
	testl	%eax, %eax
	jne	.L213
	movq	-72(%rbp), %rdi
	movl	$1, %esi
	call	BN_set_word@PLT
	testl	%eax, %eax
	je	.L26
.L46:
	movq	-72(%rbp), %rsi
	movq	-88(%rbp), %rdi
	movq	%r13, %rcx
	movq	%r14, %rdx
	call	BN_mod_sqr@PLT
	testl	%eax, %eax
	je	.L26
	movq	-88(%rbp), %rdi
	movq	-56(%rbp), %rdx
	movq	%r13, %r8
	movq	%r14, %rcx
	movq	%rdi, %rsi
	call	BN_mod_mul@PLT
	testl	%eax, %eax
	je	.L26
	movq	-72(%rbp), %rsi
	movq	-56(%rbp), %rdx
	movq	%r13, %r8
	movq	%r14, %rcx
	movq	%rsi, %rdi
	call	BN_mod_mul@PLT
	testl	%eax, %eax
	je	.L26
	movq	%r15, -64(%rbp)
	movq	%rbx, -104(%rbp)
	movq	-96(%rbp), %rbx
.L53:
	movq	-88(%rbp), %rdi
	call	BN_is_one@PLT
	testl	%eax, %eax
	jne	.L230
	movq	-88(%rbp), %rsi
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	%rbx, %rdi
	call	BN_mod_sqr@PLT
	testl	%eax, %eax
	je	.L208
	movl	$1, %r15d
	jmp	.L48
.L50:
	addl	$1, %r15d
	cmpl	%r15d, %r12d
	je	.L231
	movq	%r13, %r8
	movq	%r14, %rcx
	movq	%rbx, %rdx
	movq	%rbx, %rsi
	movq	%rbx, %rdi
	call	BN_mod_mul@PLT
	testl	%eax, %eax
	je	.L208
.L48:
	movq	%rbx, %rdi
	call	BN_is_one@PLT
	testl	%eax, %eax
	je	.L50
	movq	-80(%rbp), %rsi
	movq	%rbx, %rdi
	call	BN_copy@PLT
	testq	%rax, %rax
	je	.L208
	subl	%r15d, %r12d
	subl	$1, %r12d
	jmp	.L51
.L52:
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	%rbx, %rsi
	movq	%rbx, %rdi
	call	BN_mod_sqr@PLT
	testl	%eax, %eax
	je	.L208
	subl	$1, %r12d
.L51:
	testl	%r12d, %r12d
	jg	.L52
	movq	-80(%rbp), %r12
	movq	%r13, %r8
	movq	%r14, %rcx
	movq	%rbx, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	BN_mod_mul@PLT
	testl	%eax, %eax
	je	.L208
	movq	-72(%rbp), %rsi
	movq	%r13, %r8
	movq	%r14, %rcx
	movq	%rbx, %rdx
	movq	%rsi, %rdi
	call	BN_mod_mul@PLT
	testl	%eax, %eax
	je	.L208
	movq	-88(%rbp), %rsi
	movq	%r13, %r8
	movq	%r14, %rcx
	movq	%r12, %rdx
	movq	%rsi, %rdi
	call	BN_mod_mul@PLT
	testl	%eax, %eax
	je	.L208
	movl	%r15d, %r12d
	jmp	.L53
.L227:
	movq	-104(%rbp), %r15
	movq	-112(%rbp), %rbx
	movl	$200, %r8d
.L212:
	movl	$112, %edx
	movl	$121, %esi
	movl	$3, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L26
.L228:
	movq	-104(%rbp), %r15
	movq	-112(%rbp), %rbx
.L40:
	movl	$212, %r8d
	movl	$113, %edx
	movl	$121, %esi
	movl	$3, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L26
.L30:
	movq	%r14, %rdi
	call	BN_num_bits@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	movl	%eax, %esi
	call	BN_priv_rand@PLT
	testl	%eax, %eax
	je	.L201
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	BN_ucmp@PLT
	testl	%eax, %eax
	js	.L36
	cmpl	$0, 16(%r14)
	jne	.L58
	movq	BN_sub@GOTPCREL(%rip), %rax
.L35:
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	*%rax
	testl	%eax, %eax
	je	.L201
.L36:
	movq	%r12, %rdi
	call	BN_is_zero@PLT
	testl	%eax, %eax
	je	.L34
	jmp	.L33
.L222:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	BN_is_bit_set@PLT
	jmp	.L217
.L58:
	movq	BN_add@GOTPCREL(%rip), %rax
	jmp	.L35
.L229:
	movl	$227, %r8d
	jmp	.L212
.L43:
	movq	-96(%rbp), %rdx
	movq	-56(%rbp), %rsi
	movq	%r13, %r8
	movq	%r14, %rcx
	movq	-72(%rbp), %rdi
	call	BN_mod_exp@PLT
	testl	%eax, %eax
	je	.L26
	movq	-72(%rbp), %rdi
	call	BN_is_zero@PLT
	testl	%eax, %eax
	je	.L46
.L213:
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	BN_set_word@PLT
	jmp	.L45
.L231:
	movq	-64(%rbp), %r15
	movq	-104(%rbp), %rbx
	movl	$311, %r8d
	jmp	.L211
.L208:
	movq	-64(%rbp), %r15
	movq	-104(%rbp), %rbx
	jmp	.L26
.L230:
	movq	-64(%rbp), %r15
	movq	-72(%rbp), %rsi
	movq	-104(%rbp), %rbx
	movq	%r15, %rdi
	call	BN_copy@PLT
	testq	%rax, %rax
	jne	.L28
	jmp	.L26
	.cfi_endproc
.LFE252:
	.size	BN_mod_sqrt, .-BN_mod_sqrt
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
