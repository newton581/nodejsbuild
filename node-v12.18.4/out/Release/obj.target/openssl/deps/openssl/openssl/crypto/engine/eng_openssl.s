	.file	"eng_openssl.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"(TEST_ENG_OPENSSL_PKEY)Loading Private key %s\n"
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"r"
	.text
	.p2align 4
	.type	openssl_load_privkey, @function
openssl_load_privkey:
.LFB1338:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	stderr(%rip), %rdi
	movq	%rsi, %rcx
	xorl	%eax, %eax
	leaq	.LC0(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	movl	$1, %esi
	call	__fprintf_chk@PLT
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_new_file@PLT
	testq	%rax, %rax
	je	.L3
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	PEM_read_bio_PrivateKey@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	BIO_free@PLT
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	.cfi_restore_state
	xorl	%r13d, %r13d
	popq	%r12
	movq	%r13, %rax
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1338:
	.size	openssl_load_privkey, .-openssl_load_privkey
	.p2align 4
	.type	test_sha1_final, @function
test_sha1_final:
.LFB1333:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	subq	$8, %rsp
	call	EVP_MD_CTX_md_data@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	SHA1_Final@PLT
	.cfi_endproc
.LFE1333:
	.size	test_sha1_final, .-test_sha1_final
	.p2align 4
	.type	test_sha1_update, @function
test_sha1_update:
.LFB1332:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	call	EVP_MD_CTX_md_data@PLT
	movq	%r13, %rdx
	movq	%r12, %rsi
	popq	%r12
	movq	%rax, %rdi
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	SHA1_Update@PLT
	.cfi_endproc
.LFE1332:
	.size	test_sha1_update, .-test_sha1_update
	.p2align 4
	.type	test_sha1_init, @function
test_sha1_init:
.LFB1331:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	EVP_MD_CTX_md_data@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	movq	%rax, %rdi
	jmp	SHA1_Init@PLT
	.cfi_endproc
.LFE1331:
	.size	test_sha1_init, .-test_sha1_init
	.p2align 4
	.type	test_r4_cipher, @function
test_r4_cipher:
.LFB1325:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	r4_cipher(%rip), %r12
	testq	%r12, %r12
	je	.L33
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L33:
	.cfi_restore_state
	movl	$16, %edx
	movl	$1, %esi
	movl	$5, %edi
	call	EVP_CIPHER_meth_new@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L16
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	EVP_CIPHER_meth_set_iv_length@PLT
	testl	%eax, %eax
	jne	.L34
.L16:
	movq	%r13, %rdi
	call	EVP_CIPHER_meth_free@PLT
.L15:
	movq	%r12, %rax
	movq	%r12, r4_cipher(%rip)
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L34:
	.cfi_restore_state
	movl	$8, %esi
	movq	%r13, %rdi
	call	EVP_CIPHER_meth_set_flags@PLT
	testl	%eax, %eax
	je	.L16
	leaq	test_rc4_init_key(%rip), %rsi
	movq	%r13, %rdi
	call	EVP_CIPHER_meth_set_init@PLT
	testl	%eax, %eax
	je	.L16
	leaq	test_rc4_cipher(%rip), %rsi
	movq	%r13, %rdi
	call	EVP_CIPHER_meth_set_do_cipher@PLT
	testl	%eax, %eax
	je	.L16
	movl	$1048, %esi
	movq	%r13, %rdi
	call	EVP_CIPHER_meth_set_impl_ctx_size@PLT
	testl	%eax, %eax
	je	.L16
	movq	%r13, %r12
	jmp	.L15
	.cfi_endproc
.LFE1325:
	.size	test_r4_cipher, .-test_r4_cipher
	.p2align 4
	.type	openssl_destroy, @function
openssl_destroy:
.LFB1339:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	sha1_md(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	EVP_MD_meth_free@PLT
	movq	r4_cipher(%rip), %rdi
	movq	$0, sha1_md(%rip)
	call	EVP_CIPHER_meth_free@PLT
	movq	r4_40_cipher(%rip), %rdi
	movq	$0, r4_cipher(%rip)
	call	EVP_CIPHER_meth_free@PLT
	movl	$1, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	movq	$0, r4_40_cipher(%rip)
	ret
	.cfi_endproc
.LFE1339:
	.size	openssl_destroy, .-openssl_destroy
	.p2align 4
	.type	test_rc4_cipher, @function
test_rc4_cipher:
.LFB1324:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rcx, %r12
	subq	$8, %rsp
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r12, %rsi
	leaq	16(%rax), %rdi
	call	RC4@PLT
	addq	$8, %rsp
	movl	$1, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1324:
	.size	test_rc4_cipher, .-test_rc4_cipher
	.section	.rodata.str1.8
	.align 8
.LC2:
	.string	"(TEST_ENG_OPENSSL_RC4) test_init_key() called\n"
	.text
	.p2align 4
	.type	test_rc4_init_key, @function
test_rc4_init_key:
.LFB1323:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	stderr(%rip), %rcx
	movl	$46, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	movl	$1, %esi
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	leaq	.LC2(%rip), %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	call	fwrite@PLT
	movq	%r12, %rdi
	call	EVP_CIPHER_CTX_key_length@PLT
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movslq	%ebx, %rdx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	%r12, %rdi
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	call	EVP_CIPHER_CTX_key_length@PLT
	movq	%r12, %rdi
	movl	%eax, %r13d
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	%r14, %rdx
	movl	%r13d, %esi
	leaq	16(%rax), %rdi
	call	RC4_set_key@PLT
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1323:
	.size	test_rc4_init_key, .-test_rc4_init_key
	.p2align 4
	.type	openssl_ciphers, @function
openssl_ciphers:
.LFB1330:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testq	%rsi, %rsi
	je	.L91
	movq	%rsi, %rbx
	cmpl	$5, %ecx
	je	.L92
	cmpl	$97, %ecx
	je	.L93
	movq	$0, (%rsi)
	xorl	%eax, %eax
.L41:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L93:
	.cfi_restore_state
	movq	r4_40_cipher(%rip), %r12
	testq	%r12, %r12
	je	.L94
.L53:
	movq	%r12, (%rbx)
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L92:
	.cfi_restore_state
	call	test_r4_cipher
	movq	%rax, (%rbx)
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L91:
	.cfi_restore_state
	movl	init.21692(%rip), %eax
	movq	%rdx, %r12
	testl	%eax, %eax
	je	.L43
	movl	pos.21691(%rip), %eax
	leaq	cipher_nids.21690(%rip), %rbx
.L44:
	movq	%rbx, (%r12)
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L94:
	movl	$5, %edx
	movl	$1, %esi
	movl	$5, %edi
	call	EVP_CIPHER_meth_new@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L56
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	EVP_CIPHER_meth_set_iv_length@PLT
	testl	%eax, %eax
	jne	.L95
.L56:
	movq	%r13, %rdi
	call	EVP_CIPHER_meth_free@PLT
.L55:
	movq	%r12, r4_40_cipher(%rip)
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L43:
	call	test_r4_cipher
	leaq	cipher_nids.21690(%rip), %rbx
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L45
	movslq	pos.21691(%rip), %r13
	leal	1(%r13), %eax
	movl	%eax, pos.21691(%rip)
	call	EVP_CIPHER_nid@PLT
	movl	%eax, (%rbx,%r13,4)
.L45:
	movq	r4_40_cipher(%rip), %r13
	testq	%r13, %r13
	je	.L96
.L46:
	movslq	pos.21691(%rip), %r14
	movq	%r13, %rdi
	leal	1(%r14), %eax
	movl	%eax, pos.21691(%rip)
	call	EVP_CIPHER_nid@PLT
	movl	%eax, (%rbx,%r14,4)
.L48:
	movslq	pos.21691(%rip), %rdx
	movl	$1, init.21692(%rip)
	movl	$0, (%rbx,%rdx,4)
	movq	%rdx, %rax
	jmp	.L44
.L95:
	movl	$8, %esi
	movq	%r13, %rdi
	call	EVP_CIPHER_meth_set_flags@PLT
	testl	%eax, %eax
	je	.L56
	leaq	test_rc4_init_key(%rip), %rsi
	movq	%r13, %rdi
	call	EVP_CIPHER_meth_set_init@PLT
	testl	%eax, %eax
	je	.L56
	leaq	test_rc4_cipher(%rip), %rsi
	movq	%r13, %rdi
	call	EVP_CIPHER_meth_set_do_cipher@PLT
	testl	%eax, %eax
	je	.L56
	movl	$1048, %esi
	movq	%r13, %rdi
	call	EVP_CIPHER_meth_set_impl_ctx_size@PLT
	testl	%eax, %eax
	je	.L56
	movq	%r13, %r12
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L96:
	movl	$5, %edx
	movl	$1, %esi
	movl	$5, %edi
	call	EVP_CIPHER_meth_new@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L49
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	EVP_CIPHER_meth_set_iv_length@PLT
	testl	%eax, %eax
	jne	.L97
.L49:
	movq	%r13, %rdi
	call	EVP_CIPHER_meth_free@PLT
	movq	$0, r4_40_cipher(%rip)
	jmp	.L48
.L97:
	movl	$8, %esi
	movq	%r13, %rdi
	call	EVP_CIPHER_meth_set_flags@PLT
	testl	%eax, %eax
	je	.L49
	leaq	test_rc4_init_key(%rip), %rsi
	movq	%r13, %rdi
	call	EVP_CIPHER_meth_set_init@PLT
	testl	%eax, %eax
	je	.L49
	leaq	test_rc4_cipher(%rip), %rsi
	movq	%r13, %rdi
	call	EVP_CIPHER_meth_set_do_cipher@PLT
	testl	%eax, %eax
	je	.L49
	movl	$1048, %esi
	movq	%r13, %rdi
	call	EVP_CIPHER_meth_set_impl_ctx_size@PLT
	testl	%eax, %eax
	je	.L49
	movq	%r13, r4_40_cipher(%rip)
	jmp	.L46
	.cfi_endproc
.LFE1330:
	.size	openssl_ciphers, .-openssl_ciphers
	.p2align 4
	.type	openssl_digests, @function
openssl_digests:
.LFB1337:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	testq	%rsi, %rsi
	je	.L155
	movq	%rsi, %rbx
	cmpl	$64, %ecx
	je	.L156
	movq	$0, (%rsi)
	xorl	%eax, %eax
.L98:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L156:
	.cfi_restore_state
	movq	sha1_md(%rip), %r12
	testq	%r12, %r12
	je	.L157
.L108:
	movq	%r12, (%rbx)
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L155:
	.cfi_restore_state
	movl	init.21725(%rip), %eax
	testl	%eax, %eax
	je	.L100
	movl	pos.21724(%rip), %eax
	leaq	digest_nids.21723(%rip), %rcx
.L101:
	movq	%rcx, (%rdx)
	jmp	.L98
	.p2align 4,,10
	.p2align 3
.L157:
	movl	$65, %esi
	movl	$64, %edi
	call	EVP_MD_meth_new@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L111
	movl	$20, %esi
	movq	%rax, %rdi
	call	EVP_MD_meth_set_result_size@PLT
	testl	%eax, %eax
	je	.L111
	movl	$64, %esi
	movq	%r13, %rdi
	call	EVP_MD_meth_set_input_blocksize@PLT
	testl	%eax, %eax
	je	.L111
	movl	$104, %esi
	movq	%r13, %rdi
	call	EVP_MD_meth_set_app_datasize@PLT
	testl	%eax, %eax
	je	.L111
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	EVP_MD_meth_set_flags@PLT
	testl	%eax, %eax
	je	.L111
	leaq	test_sha1_init(%rip), %rsi
	movq	%r13, %rdi
	call	EVP_MD_meth_set_init@PLT
	testl	%eax, %eax
	je	.L111
	leaq	test_sha1_update(%rip), %rsi
	movq	%r13, %rdi
	call	EVP_MD_meth_set_update@PLT
	testl	%eax, %eax
	je	.L111
	leaq	test_sha1_final(%rip), %rsi
	movq	%r13, %rdi
	call	EVP_MD_meth_set_final@PLT
	testl	%eax, %eax
	je	.L111
	movq	%r13, %r12
	jmp	.L110
	.p2align 4,,10
	.p2align 3
.L111:
	movq	%r13, %rdi
	call	EVP_MD_meth_free@PLT
.L110:
	movq	%r12, sha1_md(%rip)
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L100:
	movq	sha1_md(%rip), %r12
	testq	%r12, %r12
	je	.L158
.L102:
	movslq	pos.21724(%rip), %rbx
	movq	%r12, %rdi
	movq	%rdx, -40(%rbp)
	leal	1(%rbx), %eax
	movl	%eax, pos.21724(%rip)
	call	EVP_MD_type@PLT
	leaq	digest_nids.21723(%rip), %rcx
	movq	-40(%rbp), %rdx
	movl	%eax, (%rcx,%rbx,4)
.L104:
	movslq	pos.21724(%rip), %rsi
	movl	$1, init.21725(%rip)
	movl	$0, (%rcx,%rsi,4)
	movq	%rsi, %rax
	jmp	.L101
.L158:
	movl	$65, %esi
	movl	$64, %edi
	movq	%rdx, -40(%rbp)
	call	EVP_MD_meth_new@PLT
	movq	-40(%rbp), %rdx
	testq	%rax, %rax
	movq	%rax, %r12
	je	.L105
	movl	$20, %esi
	movq	%rax, %rdi
	movq	%rdx, -40(%rbp)
	call	EVP_MD_meth_set_result_size@PLT
	movq	-40(%rbp), %rdx
	testl	%eax, %eax
	jne	.L159
.L105:
	movq	%r12, %rdi
	movq	%rdx, -40(%rbp)
	call	EVP_MD_meth_free@PLT
	movq	-40(%rbp), %rdx
	leaq	digest_nids.21723(%rip), %rcx
	movq	$0, sha1_md(%rip)
	jmp	.L104
.L159:
	movl	$64, %esi
	movq	%r12, %rdi
	call	EVP_MD_meth_set_input_blocksize@PLT
	movq	-40(%rbp), %rdx
	testl	%eax, %eax
	je	.L105
	movl	$104, %esi
	movq	%r12, %rdi
	call	EVP_MD_meth_set_app_datasize@PLT
	movq	-40(%rbp), %rdx
	testl	%eax, %eax
	je	.L105
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	EVP_MD_meth_set_flags@PLT
	movq	-40(%rbp), %rdx
	testl	%eax, %eax
	je	.L105
	leaq	test_sha1_init(%rip), %rsi
	movq	%r12, %rdi
	call	EVP_MD_meth_set_init@PLT
	movq	-40(%rbp), %rdx
	testl	%eax, %eax
	je	.L105
	leaq	test_sha1_update(%rip), %rsi
	movq	%r12, %rdi
	call	EVP_MD_meth_set_update@PLT
	movq	-40(%rbp), %rdx
	testl	%eax, %eax
	je	.L105
	leaq	test_sha1_final(%rip), %rsi
	movq	%r12, %rdi
	call	EVP_MD_meth_set_final@PLT
	movq	-40(%rbp), %rdx
	testl	%eax, %eax
	je	.L105
	movq	%r12, sha1_md(%rip)
	jmp	.L102
	.cfi_endproc
.LFE1337:
	.size	openssl_digests, .-openssl_digests
	.section	.rodata.str1.1
.LC3:
	.string	"openssl"
.LC4:
	.string	"Software engine support"
	.text
	.p2align 4
	.globl	engine_load_openssl_int
	.type	engine_load_openssl_int, @function
engine_load_openssl_int:
.LFB1322:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	ENGINE_new@PLT
	testq	%rax, %rax
	je	.L160
	leaq	.LC3(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	ENGINE_set_id@PLT
	testl	%eax, %eax
	jne	.L197
.L162:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	ENGINE_free@PLT
	.p2align 4,,10
	.p2align 3
.L197:
	.cfi_restore_state
	leaq	.LC4(%rip), %rsi
	movq	%r12, %rdi
	call	ENGINE_set_name@PLT
	testl	%eax, %eax
	je	.L162
	leaq	openssl_destroy(%rip), %rsi
	movq	%r12, %rdi
	call	ENGINE_set_destroy_function@PLT
	testl	%eax, %eax
	je	.L162
	call	RSA_get_default_method@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	ENGINE_set_RSA@PLT
	testl	%eax, %eax
	je	.L162
	call	DSA_get_default_method@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	ENGINE_set_DSA@PLT
	testl	%eax, %eax
	je	.L162
	call	EC_KEY_OpenSSL@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	ENGINE_set_EC@PLT
	testl	%eax, %eax
	je	.L162
	call	DH_get_default_method@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	ENGINE_set_DH@PLT
	testl	%eax, %eax
	je	.L162
	call	RAND_OpenSSL@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	ENGINE_set_RAND@PLT
	testl	%eax, %eax
	je	.L162
	leaq	openssl_ciphers(%rip), %rsi
	movq	%r12, %rdi
	call	ENGINE_set_ciphers@PLT
	testl	%eax, %eax
	je	.L162
	leaq	openssl_digests(%rip), %rsi
	movq	%r12, %rdi
	call	ENGINE_set_digests@PLT
	testl	%eax, %eax
	je	.L162
	leaq	openssl_load_privkey(%rip), %rsi
	movq	%r12, %rdi
	call	ENGINE_set_load_privkey_function@PLT
	testl	%eax, %eax
	je	.L162
	movq	%r12, %rdi
	call	ENGINE_add@PLT
	movq	%r12, %rdi
	call	ENGINE_free@PLT
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	ERR_clear_error@PLT
.L160:
	.cfi_restore_state
	popq	%rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1322:
	.size	engine_load_openssl_int, .-engine_load_openssl_int
	.local	cipher_nids.21690
	.comm	cipher_nids.21690,16,16
	.local	pos.21691
	.comm	pos.21691,4,4
	.local	init.21692
	.comm	init.21692,4,4
	.local	digest_nids.21723
	.comm	digest_nids.21723,8,8
	.local	pos.21724
	.comm	pos.21724,4,4
	.local	init.21725
	.comm	init.21725,4,4
	.local	sha1_md
	.comm	sha1_md,8,8
	.local	r4_40_cipher
	.comm	r4_40_cipher,8,8
	.local	r4_cipher
	.comm	r4_cipher,8,8
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
