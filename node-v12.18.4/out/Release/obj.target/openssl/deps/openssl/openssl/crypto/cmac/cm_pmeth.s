	.file	"cm_pmeth.c"
	.text
	.p2align 4
	.type	pkey_cmac_ctrl, @function
pkey_cmac_ctrl:
.LFB1329:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	40(%rdi), %r12
	cmpl	$6, %esi
	je	.L2
	cmpl	$12, %esi
	je	.L3
	movl	$-2, %eax
	cmpl	$1, %esi
	je	.L18
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	.cfi_restore_state
	testq	%rcx, %rcx
	je	.L9
	testl	%edx, %edx
	js	.L9
	movslq	%edx, %rdx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r9, %rsi
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L18:
	movq	16(%rdi), %rax
	testq	%rax, %rax
	je	.L10
	movq	40(%rax), %rsi
	movq	%r12, %rdi
	call	CMAC_CTX_copy@PLT
	testl	%eax, %eax
	je	.L9
.L10:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L3:
	movq	8(%rdi), %r8
.L15:
	xorl	%edx, %edx
	xorl	%esi, %esi
.L16:
	movq	%r12, %rdi
	call	CMAC_Init@PLT
	testl	%eax, %eax
	setne	%al
	addq	$8, %rsp
	movzbl	%al, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1329:
	.size	pkey_cmac_ctrl, .-pkey_cmac_ctrl
	.p2align 4
	.type	cmac_signctx, @function
cmac_signctx:
.LFB1328:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rdi
	jmp	CMAC_Final@PLT
	.cfi_endproc
.LFE1328:
	.size	cmac_signctx, .-cmac_signctx
	.p2align 4
	.type	cmac_signctx_init, @function
cmac_signctx_init:
.LFB1327:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	movl	$256, %esi
	movq	%r12, %rdi
	subq	$8, %rsp
	call	EVP_MD_CTX_set_flags@PLT
	movq	%r12, %rdi
	leaq	int_update(%rip), %rsi
	call	EVP_MD_CTX_set_update_fn@PLT
	addq	$8, %rsp
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1327:
	.size	cmac_signctx_init, .-cmac_signctx_init
	.p2align 4
	.type	int_update, @function
int_update:
.LFB1326:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	call	EVP_MD_CTX_pkey_ctx@PLT
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	40(%rax), %rdi
	call	CMAC_Update@PLT
	popq	%r12
	popq	%r13
	testl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	setne	%al
	movzbl	%al, %eax
	ret
	.cfi_endproc
.LFE1326:
	.size	int_update, .-int_update
	.p2align 4
	.type	pkey_cmac_init, @function
pkey_cmac_init:
.LFB1322:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	CMAC_CTX_new@PLT
	movq	%rax, 40(%rbx)
	testq	%rax, %rax
	je	.L26
	movl	$0, 72(%rbx)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1322:
	.size	pkey_cmac_init, .-pkey_cmac_init
	.p2align 4
	.type	pkey_cmac_copy, @function
pkey_cmac_copy:
.LFB1323:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	call	CMAC_CTX_new@PLT
	movq	%rax, 40(%rbx)
	testq	%rax, %rax
	je	.L30
	movl	$0, 72(%rbx)
	movq	40(%r12), %rsi
	movq	%rax, %rdi
	call	CMAC_CTX_copy@PLT
	popq	%rbx
	popq	%r12
	testl	%eax, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	setne	%al
	movzbl	%al, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	.cfi_restore_state
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1323:
	.size	pkey_cmac_copy, .-pkey_cmac_copy
	.p2align 4
	.type	pkey_cmac_cleanup, @function
pkey_cmac_cleanup:
.LFB1324:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rdi
	jmp	CMAC_CTX_free@PLT
	.cfi_endproc
.LFE1324:
	.size	pkey_cmac_cleanup, .-pkey_cmac_cleanup
	.p2align 4
	.type	pkey_cmac_keygen, @function
pkey_cmac_keygen:
.LFB1325:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	call	CMAC_CTX_new@PLT
	movq	%rax, %r12
	xorl	%eax, %eax
	testq	%r12, %r12
	je	.L33
	movq	40(%rbx), %rsi
	movq	%r12, %rdi
	call	CMAC_CTX_copy@PLT
	testl	%eax, %eax
	je	.L39
	movq	%r12, %rdx
	movl	$894, %esi
	movq	%r13, %rdi
	call	EVP_PKEY_assign@PLT
	movl	$1, %eax
.L33:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L39:
	.cfi_restore_state
	movq	%r12, %rdi
	movl	%eax, -36(%rbp)
	call	CMAC_CTX_free@PLT
	movl	-36(%rbp), %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1325:
	.size	pkey_cmac_keygen, .-pkey_cmac_keygen
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"cipher"
.LC1:
	.string	"key"
.LC2:
	.string	"hexkey"
	.text
	.p2align 4
	.type	pkey_cmac_ctrl_str, @function
pkey_cmac_ctrl_str:
.LFB1330:
	.cfi_startproc
	endbr64
	testq	%rdx, %rdx
	je	.L53
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$7, %ecx
	movq	%rsi, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	.LC0(%rip), %rdi
	subq	$8, %rsp
	repz cmpsb
	seta	%cl
	sbbb	$0, %cl
	testb	%cl, %cl
	je	.L54
	movl	$4, %ecx
	leaq	.LC1(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%cl
	sbbb	$0, %cl
	testb	%cl, %cl
	je	.L55
	movq	%rax, %rsi
	movl	$7, %ecx
	leaq	.LC2(%rip), %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L46
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$6, %esi
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	EVP_PKEY_CTX_hex2ctrl@PLT
	.p2align 4,,10
	.p2align 3
.L54:
	.cfi_restore_state
	movq	%rdx, %rdi
	call	EVP_get_cipherbyname@PLT
	movq	%rax, %rcx
	xorl	%eax, %eax
	testq	%rcx, %rcx
	je	.L40
	movq	40(%r12), %rdi
	movq	8(%r12), %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	CMAC_Init@PLT
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
.L40:
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L55:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$6, %esi
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	EVP_PKEY_CTX_str2ctrl@PLT
	.p2align 4,,10
	.p2align 3
.L53:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L46:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	movl	$-2, %eax
	jmp	.L40
	.cfi_endproc
.LFE1330:
	.size	pkey_cmac_ctrl_str, .-pkey_cmac_ctrl_str
	.globl	cmac_pkey_meth
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	cmac_pkey_meth, @object
	.size	cmac_pkey_meth, 256
cmac_pkey_meth:
	.long	894
	.long	4
	.quad	pkey_cmac_init
	.quad	pkey_cmac_copy
	.quad	pkey_cmac_cleanup
	.quad	0
	.quad	0
	.quad	0
	.quad	pkey_cmac_keygen
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	cmac_signctx_init
	.quad	cmac_signctx
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	pkey_cmac_ctrl
	.quad	pkey_cmac_ctrl_str
	.zero	48
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
