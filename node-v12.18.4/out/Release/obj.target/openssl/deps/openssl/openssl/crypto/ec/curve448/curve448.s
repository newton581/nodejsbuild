	.file	"curve448.c"
	.text
	.p2align 4
	.type	recode_wnaf, @function
recode_wnaf:
.LFB225:
	.cfi_startproc
	leal	1(%rdx), %ecx
	movl	$446, %eax
	xorl	%edx, %edx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	divl	%ecx
	movl	$1, %r9d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	movq	%rdi, %r8
	pushq	%r13
	sall	%cl, %r9d
	pushq	%r12
	leal	-1(%r9), %r11d
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movl	$1, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	movzwl	(%rsi), %edx
	movl	$4294967295, %esi
	leal	2(%rax), %ecx
	leal	3(%rax), %r13d
	addl	$1, %eax
	movq	%rsi, (%rdi,%rcx,8)
	.p2align 4,,10
	.p2align 3
.L9:
	cmpl	$27, %r12d
	ja	.L2
.L29:
	movl	%r12d, %esi
	movl	%r12d, %ecx
	shrl	$2, %esi
	andl	$3, %ecx
	movq	(%rbx,%rsi,8), %rsi
	sall	$4, %ecx
	shrq	%cl, %rsi
	sall	$16, %esi
	addq	%rsi, %rdx
	testw	%dx, %dx
	je	.L27
.L3:
	movl	%r12d, %r10d
	leal	-1(%rax), %esi
	sall	$4, %r10d
	movslq	%esi, %rsi
	subl	$16, %r10d
	.p2align 4,,10
	.p2align 3
.L7:
	xorl	%ecx, %ecx
	movl	%edx, %edi
	movl	%r11d, %eax
	rep bsfl	%edx, %ecx
	shrl	%cl, %edi
	andl	%edi, %eax
	movl	%eax, %r14d
	subl	%r9d, %r14d
	testl	%edi, %r9d
	cmovne	%r14d, %eax
	movl	%eax, %edi
	movl	%eax, 12(%r8,%rsi,8)
	movl	%esi, %eax
	sall	%cl, %edi
	addl	%r10d, %ecx
	movslq	%edi, %rdi
	movl	%ecx, 8(%r8,%rsi,8)
	subq	$1, %rsi
	subq	%rdi, %rdx
	testw	%dx, %dx
	jne	.L7
	addl	$1, %r12d
	shrq	$16, %rdx
	cmpl	$30, %r12d
	jne	.L9
.L30:
	addl	$1, %eax
	movl	$0, %edx
	subl	%eax, %r13d
	leal	-1(%r13), %esi
	movq	%rsi, %r9
	je	.L28
	.p2align 4,,10
	.p2align 3
.L11:
	leal	(%rax,%rdx), %ecx
	movq	(%r8,%rcx,8), %rcx
	movq	%rcx, (%r8,%rdx,8)
	movq	%rdx, %rcx
	addq	$1, %rdx
	cmpq	%rcx, %rsi
	jne	.L11
	popq	%rbx
	movl	%r9d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L27:
	.cfi_restore_state
	addl	$1, %r12d
	shrq	$16, %rdx
	cmpl	$27, %r12d
	jbe	.L29
.L2:
	testw	%dx, %dx
	jne	.L3
	addl	$1, %r12d
	shrq	$16, %rdx
	cmpl	$30, %r12d
	jne	.L9
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L28:
	movl	$-1, %r9d
	popq	%rbx
	popq	%r12
	movl	%r9d, %eax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE225:
	.size	recode_wnaf, .-recode_wnaf
	.p2align 4
	.type	point_double_internal.constprop.0, @function
point_double_internal.constprop.0:
.LFB236:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-320(%rbp), %r15
	leaq	-256(%rbp), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	leaq	-192(%rbp), %rdi
	leaq	192(%rbx), %r13
	subq	$280, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	gf_sqr@PLT
	leaq	64(%r12), %rsi
	movq	%r15, %rdi
	call	gf_sqr@PLT
	movdqa	-192(%rbp), %xmm0
	movq	%r13, %rsi
	movq	%r14, %rdi
	paddd	-320(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	movdqa	-176(%rbp), %xmm0
	paddd	-304(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	movdqa	-160(%rbp), %xmm0
	paddd	-288(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movdqa	-144(%rbp), %xmm0
	paddd	-272(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	movdqa	64(%r12), %xmm0
	paddd	(%r12), %xmm0
	movaps	%xmm0, 192(%rbx)
	movdqa	80(%r12), %xmm0
	paddd	16(%r12), %xmm0
	movaps	%xmm0, 208(%rbx)
	movdqa	96(%r12), %xmm0
	paddd	32(%r12), %xmm0
	movaps	%xmm0, 224(%rbx)
	movdqa	112(%r12), %xmm0
	paddd	48(%r12), %xmm0
	movaps	%xmm0, 240(%rbx)
	call	gf_sqr@PLT
	movdqa	-256(%rbp), %xmm0
	psubd	-128(%rbp), %xmm0
	movaps	%xmm0, -256(%rbp)
	movd	%xmm0, %eax
	movdqa	-240(%rbp), %xmm0
	psubd	-112(%rbp), %xmm0
	addl	$805306365, -252(%rbp)
	addl	$805306365, %eax
	movaps	%xmm0, -240(%rbp)
	movdqa	-224(%rbp), %xmm0
	psubd	-96(%rbp), %xmm0
	addl	$805306365, -248(%rbp)
	movaps	%xmm0, -224(%rbp)
	movdqa	-208(%rbp), %xmm0
	psubd	-80(%rbp), %xmm0
	addl	$805306365, -244(%rbp)
	addl	$805306365, -240(%rbp)
	addl	$805306365, -236(%rbp)
	addl	$805306365, -232(%rbp)
	addl	$805306365, -228(%rbp)
	addl	$805306362, -224(%rbp)
	addl	$805306365, -220(%rbp)
	addl	$805306365, -216(%rbp)
	movaps	%xmm0, -208(%rbp)
	addl	$805306365, -212(%rbp)
	movl	-248(%rbp), %ecx
	addl	$805306365, -208(%rbp)
	movdqa	.LC0(%rip), %xmm1
	movl	-244(%rbp), %edx
	movl	%ecx, %edi
	movdqu	-244(%rbp), %xmm7
	andl	$268435455, %ecx
	addl	$805306365, -204(%rbp)
	shrl	$28, %edi
	addl	$805306365, -200(%rbp)
	movdqu	-212(%rbp), %xmm5
	andl	$268435455, %edx
	addl	$805306365, -196(%rbp)
	movl	-196(%rbp), %esi
	addl	%edi, %edx
	pshufd	$27, -208(%rbp), %xmm0
	pshufd	$27, %xmm5, %xmm2
	pand	%xmm1, %xmm0
	movl	%edx, -244(%rbp)
	movl	-252(%rbp), %edx
	shrl	$28, %esi
	psrld	$28, %xmm2
	addl	%esi, -224(%rbp)
	movdqu	-228(%rbp), %xmm6
	paddd	%xmm2, %xmm0
	movl	%edx, %edi
	andl	$268435455, %edx
	pshufd	$27, %xmm0, %xmm0
	pshufd	$27, %xmm6, %xmm2
	shrl	$28, %edi
	movaps	%xmm0, -208(%rbp)
	psrld	$28, %xmm2
	addl	%edi, %ecx
	pshufd	$27, -224(%rbp), %xmm0
	pand	%xmm1, %xmm0
	movl	%ecx, -248(%rbp)
	movl	%eax, %ecx
	movq	%rbx, %rdi
	paddd	%xmm2, %xmm0
	pshufd	$27, %xmm7, %xmm2
	shrl	$28, %ecx
	pshufd	$27, %xmm0, %xmm0
	psrld	$28, %xmm2
	addl	%ecx, %edx
	andl	$268435455, %eax
	movaps	%xmm0, -224(%rbp)
	addl	%esi, %eax
	pshufd	$27, -240(%rbp), %xmm0
	pand	%xmm1, %xmm0
	paddd	%xmm2, %xmm0
	movl	%edx, -252(%rbp)
	pshufd	$27, %xmm0, %xmm0
	movl	%eax, -256(%rbp)
	movaps	%xmm0, -240(%rbp)
	movdqa	-320(%rbp), %xmm0
	psubd	-192(%rbp), %xmm0
	movaps	%xmm0, 192(%rbx)
	movdqa	-304(%rbp), %xmm0
	psubd	-176(%rbp), %xmm0
	addl	$536870910, 192(%rbx)
	movaps	%xmm0, 208(%rbx)
	movdqa	-288(%rbp), %xmm0
	psubd	-160(%rbp), %xmm0
	addl	$536870910, 196(%rbx)
	movaps	%xmm0, 224(%rbx)
	movdqa	-272(%rbp), %xmm0
	psubd	-144(%rbp), %xmm0
	addl	$536870910, 200(%rbx)
	addl	$536870910, 204(%rbx)
	addl	$536870910, 208(%rbx)
	addl	$536870910, 212(%rbx)
	addl	$536870910, 216(%rbx)
	addl	$536870910, 220(%rbx)
	addl	$536870908, 224(%rbx)
	addl	$536870910, 228(%rbx)
	addl	$536870910, 232(%rbx)
	movaps	%xmm0, 240(%rbx)
	addl	$536870910, 236(%rbx)
	movl	200(%rbx), %edx
	movl	204(%rbx), %ecx
	addl	$536870910, 240(%rbx)
	movl	%edx, %eax
	andl	$268435455, %edx
	addl	$536870910, 244(%rbx)
	movdqu	204(%rbx), %xmm7
	shrl	$28, %eax
	andl	$268435455, %ecx
	addl	$536870910, 248(%rbx)
	movdqu	236(%rbx), %xmm5
	addl	$536870910, 252(%rbx)
	addl	%eax, %ecx
	movl	252(%rbx), %esi
	pshufd	$27, 240(%rbx), %xmm0
	movl	%ecx, 204(%rbx)
	movl	196(%rbx), %ecx
	pshufd	$27, %xmm5, %xmm2
	pand	%xmm1, %xmm0
	shrl	$28, %esi
	psrld	$28, %xmm2
	addl	%esi, 224(%rbx)
	movdqu	220(%rbx), %xmm6
	movl	%ecx, %eax
	paddd	%xmm2, %xmm0
	andl	$268435455, %ecx
	shrl	$28, %eax
	pshufd	$27, %xmm6, %xmm2
	pshufd	$27, %xmm0, %xmm0
	addl	%eax, %edx
	movl	192(%rbx), %eax
	movaps	%xmm0, 240(%rbx)
	psrld	$28, %xmm2
	pshufd	$27, 224(%rbx), %xmm0
	pand	%xmm1, %xmm0
	movl	%edx, 200(%rbx)
	paddd	%xmm2, %xmm0
	movl	%eax, %edx
	pshufd	$27, %xmm7, %xmm2
	andl	$268435455, %eax
	shrl	$28, %edx
	psrld	$28, %xmm2
	pshufd	$27, %xmm0, %xmm0
	addl	%esi, %eax
	addl	%edx, %ecx
	movaps	%xmm0, 224(%rbx)
	pshufd	$27, 208(%rbx), %xmm0
	pand	%xmm1, %xmm0
	paddd	%xmm2, %xmm0
	movl	%ecx, 196(%rbx)
	leaq	128(%r12), %rsi
	movl	%eax, 192(%rbx)
	pshufd	$27, %xmm0, %xmm0
	leaq	-128(%rbp), %r12
	movaps	%xmm0, 208(%rbx)
	call	gf_sqr@PLT
	movdqa	(%rbx), %xmm0
	movdqa	16(%rbx), %xmm4
	movdqa	32(%rbx), %xmm3
	movdqa	48(%rbx), %xmm2
	pslld	$1, %xmm0
	pslld	$1, %xmm4
	movaps	%xmm0, 128(%rbx)
	pslld	$1, %xmm3
	psubd	192(%rbx), %xmm0
	pslld	$1, %xmm2
	movaps	%xmm4, 144(%rbx)
	psubd	208(%rbx), %xmm4
	movaps	%xmm3, 160(%rbx)
	psubd	224(%rbx), %xmm3
	movd	%xmm0, %eax
	movaps	%xmm2, 176(%rbx)
	psubd	240(%rbx), %xmm2
	addl	$1073741820, %eax
	movaps	%xmm4, -304(%rbp)
	movaps	%xmm3, -288(%rbp)
	movaps	%xmm0, -320(%rbp)
	addl	$1073741820, -304(%rbp)
	addl	$1073741820, -316(%rbp)
	addl	$1073741820, -312(%rbp)
	addl	$1073741820, -308(%rbp)
	addl	$1073741820, -300(%rbp)
	addl	$1073741820, -296(%rbp)
	addl	$1073741820, -292(%rbp)
	addl	$1073741816, -288(%rbp)
	movaps	%xmm2, -272(%rbp)
	addl	$1073741820, -284(%rbp)
	movl	-312(%rbp), %edx
	addl	$1073741820, -276(%rbp)
	movl	-308(%rbp), %ecx
	addl	$1073741820, -272(%rbp)
	movl	%edx, %edi
	movdqa	.LC0(%rip), %xmm1
	andl	$268435455, %edx
	addl	$1073741820, -268(%rbp)
	shrl	$28, %edi
	andl	$268435455, %ecx
	movdqu	-308(%rbp), %xmm7
	addl	$1073741820, -264(%rbp)
	movdqu	-276(%rbp), %xmm5
	addl	%edi, %ecx
	addl	$1073741820, -260(%rbp)
	movl	-260(%rbp), %esi
	pshufd	$27, -272(%rbp), %xmm0
	pand	%xmm1, %xmm0
	pshufd	$27, %xmm5, %xmm2
	movl	%ecx, -308(%rbp)
	movl	-316(%rbp), %ecx
	addl	$1073741820, -280(%rbp)
	shrl	$28, %esi
	psrld	$28, %xmm2
	addl	%esi, -288(%rbp)
	movdqu	-292(%rbp), %xmm6
	paddd	%xmm2, %xmm0
	movl	%ecx, %edi
	pshufd	$27, %xmm0, %xmm0
	shrl	$28, %edi
	andl	$268435455, %ecx
	pshufd	$27, %xmm6, %xmm2
	movaps	%xmm0, -272(%rbp)
	addl	%edi, %edx
	pshufd	$27, -288(%rbp), %xmm0
	psrld	$28, %xmm2
	pand	%xmm1, %xmm0
	movl	%edx, -312(%rbp)
	movl	%eax, %edx
	paddd	%xmm2, %xmm0
	shrl	$28, %edx
	andl	$268435455, %eax
	movq	%rbx, %rdi
	pshufd	$27, %xmm0, %xmm0
	addl	%esi, %eax
	addl	%edx, %ecx
	movq	%r15, %rsi
	movaps	%xmm0, -288(%rbp)
	pshufd	$27, %xmm7, %xmm0
	movq	%r14, %rdx
	pshufd	$27, -304(%rbp), %xmm2
	psrld	$28, %xmm0
	pand	%xmm2, %xmm1
	movl	%ecx, -316(%rbp)
	paddd	%xmm1, %xmm0
	pshufd	$27, %xmm0, %xmm0
	movaps	%xmm0, -304(%rbp)
	movl	%eax, -320(%rbp)
	call	gf_mul@PLT
	leaq	128(%rbx), %rdi
	movq	%r15, %rdx
	movq	%r13, %rsi
	call	gf_mul@PLT
	leaq	64(%rbx), %rdi
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	gf_mul@PLT
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	gf_mul@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L34
	addq	$280, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L34:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE236:
	.size	point_double_internal.constprop.0, .-point_double_internal.constprop.0
	.p2align 4
	.type	add_niels_to_pt, @function
add_niels_to_pt:
.LFB212:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-256(%rbp), %r15
	leaq	-192(%rbp), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	leaq	64(%r12), %r13
	subq	$232, %rsp
	movl	%edx, -268(%rbp)
	movdqa	64(%rdi), %xmm0
	psubd	(%rdi), %xmm0
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -192(%rbp)
	movd	%xmm0, %eax
	movdqa	80(%rdi), %xmm0
	psubd	16(%rdi), %xmm0
	addl	$536870910, %eax
	addl	$536870910, -188(%rbp)
	addl	$536870910, -184(%rbp)
	movaps	%xmm0, -176(%rbp)
	movdqa	96(%rdi), %xmm0
	psubd	32(%rdi), %xmm0
	addl	$536870910, -180(%rbp)
	movaps	%xmm0, -160(%rbp)
	movdqa	112(%rdi), %xmm0
	psubd	48(%rdi), %xmm0
	addl	$536870910, -176(%rbp)
	addl	$536870910, -172(%rbp)
	movaps	%xmm0, -144(%rbp)
	addl	$536870910, -168(%rbp)
	movdqa	.LC0(%rip), %xmm1
	addl	$536870910, -148(%rbp)
	movdqu	-180(%rbp), %xmm7
	addl	$536870910, -144(%rbp)
	addl	$536870910, -140(%rbp)
	addl	$536870910, -136(%rbp)
	movdqu	-148(%rbp), %xmm5
	addl	$536870910, -132(%rbp)
	movl	-132(%rbp), %edi
	pshufd	$27, -144(%rbp), %xmm0
	pand	%xmm1, %xmm0
	addl	$536870908, -160(%rbp)
	pshufd	$27, %xmm5, %xmm2
	addl	$536870910, -164(%rbp)
	shrl	$28, %edi
	psrld	$28, %xmm2
	addl	%edi, -160(%rbp)
	paddd	%xmm2, %xmm0
	addl	$536870910, -156(%rbp)
	pshufd	$27, %xmm0, %xmm0
	addl	$536870910, -152(%rbp)
	movdqu	-164(%rbp), %xmm6
	movaps	%xmm0, -144(%rbp)
	pshufd	$27, -160(%rbp), %xmm0
	pand	%xmm1, %xmm0
	pshufd	$27, %xmm6, %xmm2
	psrld	$28, %xmm2
	paddd	%xmm2, %xmm0
	pshufd	$27, %xmm7, %xmm2
	pshufd	$27, %xmm0, %xmm0
	psrld	$28, %xmm2
	movaps	%xmm0, -160(%rbp)
	pshufd	$27, -176(%rbp), %xmm0
	pand	%xmm1, %xmm0
	paddd	%xmm2, %xmm0
	pshufd	$27, %xmm0, %xmm0
	movaps	%xmm0, -176(%rbp)
	movl	-184(%rbp), %esi
	movl	-180(%rbp), %edx
	movl	%esi, %r8d
	andl	$268435455, %edx
	andl	$268435455, %esi
	shrl	$28, %r8d
	addl	%r8d, %edx
	movl	%edx, -180(%rbp)
	movl	-188(%rbp), %edx
	movl	%edx, %r8d
	andl	$268435455, %edx
	shrl	$28, %r8d
	addl	%r8d, %esi
	movl	%esi, -184(%rbp)
	movl	%eax, %esi
	andl	$268435455, %eax
	shrl	$28, %esi
	addl	%edi, %eax
	movq	%r15, %rdi
	addl	%esi, %edx
	movq	%rbx, %rsi
	movl	%eax, -192(%rbp)
	movl	%edx, -188(%rbp)
	movq	%r14, %rdx
	call	gf_mul@PLT
	leaq	64(%rbx), %rsi
	movq	%r14, %rdx
	movq	%r13, %rdi
	movdqa	(%r12), %xmm0
	paddd	64(%r12), %xmm0
	movaps	%xmm0, -192(%rbp)
	movdqa	16(%r12), %xmm0
	paddd	80(%r12), %xmm0
	movaps	%xmm0, -176(%rbp)
	movdqa	32(%r12), %xmm0
	paddd	96(%r12), %xmm0
	movaps	%xmm0, -160(%rbp)
	movdqa	48(%r12), %xmm0
	paddd	112(%r12), %xmm0
	movaps	%xmm0, -144(%rbp)
	call	gf_mul@PLT
	leaq	192(%r12), %r8
	movq	%r12, %rdi
	leaq	128(%rbx), %rsi
	movq	%r8, %rdx
	movq	%r8, -264(%rbp)
	call	gf_mul@PLT
	movdqa	-256(%rbp), %xmm0
	paddd	64(%r12), %xmm0
	movaps	%xmm0, -128(%rbp)
	movdqa	-240(%rbp), %xmm0
	paddd	80(%r12), %xmm0
	movaps	%xmm0, -112(%rbp)
	movdqa	-224(%rbp), %xmm0
	paddd	96(%r12), %xmm0
	movaps	%xmm0, -96(%rbp)
	movdqa	-208(%rbp), %xmm0
	paddd	112(%r12), %xmm0
	movaps	%xmm0, -80(%rbp)
	movdqa	64(%r12), %xmm0
	psubd	-256(%rbp), %xmm0
	movaps	%xmm0, -192(%rbp)
	movd	%xmm0, %eax
	movdqa	80(%r12), %xmm0
	psubd	-240(%rbp), %xmm0
	addl	$536870910, -188(%rbp)
	addl	$536870910, %eax
	movaps	%xmm0, -176(%rbp)
	movdqa	96(%r12), %xmm0
	psubd	-224(%rbp), %xmm0
	addl	$536870910, -184(%rbp)
	movaps	%xmm0, -160(%rbp)
	movdqa	112(%r12), %xmm0
	psubd	-208(%rbp), %xmm0
	addl	$536870910, -180(%rbp)
	addl	$536870910, -176(%rbp)
	movaps	%xmm0, -144(%rbp)
	addl	$536870910, -172(%rbp)
	movdqa	.LC0(%rip), %xmm1
	addl	$536870910, -148(%rbp)
	addl	$536870910, -144(%rbp)
	addl	$536870910, -140(%rbp)
	addl	$536870910, -136(%rbp)
	movdqu	-148(%rbp), %xmm5
	addl	$536870910, -132(%rbp)
	movl	-132(%rbp), %esi
	pshufd	$27, -144(%rbp), %xmm0
	pand	%xmm1, %xmm0
	addl	$536870908, -160(%rbp)
	pshufd	$27, %xmm5, %xmm2
	addl	$536870910, -156(%rbp)
	shrl	$28, %esi
	psrld	$28, %xmm2
	addl	%esi, -160(%rbp)
	paddd	%xmm2, %xmm0
	addl	$536870910, -152(%rbp)
	pshufd	$27, %xmm0, %xmm0
	addl	$536870910, -164(%rbp)
	movdqu	-164(%rbp), %xmm6
	movaps	%xmm0, -144(%rbp)
	pshufd	$27, -160(%rbp), %xmm0
	pand	%xmm1, %xmm0
	addl	$536870910, -168(%rbp)
	pshufd	$27, %xmm6, %xmm2
	psrld	$28, %xmm2
	paddd	%xmm2, %xmm0
	pshufd	$27, %xmm0, %xmm0
	movaps	%xmm0, -160(%rbp)
	pshufd	$27, -176(%rbp), %xmm0
	movdqu	-180(%rbp), %xmm7
	pand	%xmm1, %xmm0
	movl	-184(%rbp), %ecx
	movl	-180(%rbp), %edx
	movdqa	160(%r12), %xmm4
	pshufd	$27, %xmm7, %xmm2
	movdqa	176(%r12), %xmm3
	psrld	$28, %xmm2
	movl	%ecx, %edi
	andl	$268435455, %edx
	andl	$268435455, %ecx
	shrl	$28, %edi
	paddd	%xmm2, %xmm0
	addl	%edi, %edx
	pshufd	$27, %xmm0, %xmm0
	movl	%edx, -180(%rbp)
	movl	-188(%rbp), %edx
	movaps	%xmm0, -176(%rbp)
	movdqa	128(%r12), %xmm0
	psubd	(%r12), %xmm0
	movl	%edx, %edi
	andl	$268435455, %edx
	movaps	%xmm0, 64(%r12)
	shrl	$28, %edi
	movdqa	144(%r12), %xmm0
	psubd	16(%r12), %xmm0
	addl	$536870910, 64(%r12)
	addl	%edi, %ecx
	leaq	128(%r12), %rdi
	movaps	%xmm0, 80(%r12)
	movdqa	%xmm4, %xmm0
	psubd	32(%r12), %xmm0
	movl	%ecx, -184(%rbp)
	movl	%eax, %ecx
	andl	$268435455, %eax
	shrl	$28, %ecx
	movaps	%xmm0, 96(%r12)
	movdqa	%xmm3, %xmm0
	addl	%esi, %eax
	psubd	48(%r12), %xmm0
	addl	%ecx, %edx
	movl	%eax, -192(%rbp)
	addl	$536870910, 68(%r12)
	movaps	%xmm0, 112(%r12)
	addl	$536870910, 72(%r12)
	addl	$536870910, 76(%r12)
	addl	$536870910, 80(%r12)
	movl	%edx, -188(%rbp)
	addl	$536870910, 84(%r12)
	addl	$536870910, 108(%r12)
	addl	$536870910, 112(%r12)
	addl	$536870910, 116(%r12)
	addl	$536870910, 120(%r12)
	movdqu	108(%r12), %xmm5
	addl	$536870910, 124(%r12)
	movl	124(%r12), %ecx
	pshufd	$27, 112(%r12), %xmm0
	pand	%xmm1, %xmm0
	addl	$536870908, 96(%r12)
	pshufd	$27, %xmm5, %xmm2
	addl	$536870910, 100(%r12)
	shrl	$28, %ecx
	psrld	$28, %xmm2
	addl	%ecx, 96(%r12)
	paddd	%xmm2, %xmm0
	addl	$536870910, 104(%r12)
	pshufd	$27, %xmm0, %xmm0
	addl	$536870910, 92(%r12)
	movdqu	92(%r12), %xmm6
	movaps	%xmm0, 112(%r12)
	pshufd	$27, 96(%r12), %xmm0
	pand	%xmm1, %xmm0
	addl	$536870910, 88(%r12)
	pshufd	$27, %xmm6, %xmm2
	movdqu	76(%r12), %xmm7
	psrld	$28, %xmm2
	paddd	%xmm2, %xmm0
	pshufd	$27, 80(%r12), %xmm2
	pand	%xmm2, %xmm1
	pshufd	$27, %xmm0, %xmm0
	movaps	%xmm0, 96(%r12)
	pshufd	$27, %xmm7, %xmm0
	psrld	$28, %xmm0
	paddd	%xmm1, %xmm0
	pshufd	$27, %xmm0, %xmm0
	movaps	%xmm0, 80(%r12)
	movl	72(%r12), %eax
	movl	76(%r12), %edx
	movdqa	(%r12), %xmm0
	paddd	32(%r12), %xmm4
	movl	%eax, %esi
	andl	$268435455, %edx
	andl	$268435455, %eax
	shrl	$28, %esi
	paddd	48(%r12), %xmm3
	paddd	128(%r12), %xmm0
	movaps	%xmm4, -224(%rbp)
	addl	%esi, %edx
	movl	%edx, 76(%r12)
	movl	68(%r12), %edx
	movaps	%xmm0, -256(%rbp)
	movdqa	16(%r12), %xmm0
	paddd	144(%r12), %xmm0
	movl	%edx, %esi
	andl	$268435455, %edx
	movaps	%xmm3, -208(%rbp)
	shrl	$28, %esi
	movaps	%xmm0, -240(%rbp)
	addl	%esi, %eax
	movl	%eax, 72(%r12)
	movl	64(%r12), %eax
	movl	%eax, %esi
	andl	$268435455, %eax
	shrl	$28, %esi
	addl	%ecx, %eax
	addl	%esi, %edx
	movl	%eax, 64(%r12)
	movq	%r15, %rsi
	movl	%edx, 68(%r12)
	movq	%r13, %rdx
	call	gf_mul@PLT
	movq	%r12, %rdi
	movq	%r14, %rdx
	movq	%r13, %rsi
	call	gf_mul@PLT
	leaq	-128(%rbp), %r12
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%r12, %rdx
	call	gf_mul@PLT
	movl	-268(%rbp), %r9d
	movq	-264(%rbp), %r8
	testl	%r9d, %r9d
	je	.L39
.L35:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L40
	addq	$232, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L39:
	.cfi_restore_state
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r8, %rdi
	call	gf_mul@PLT
	jmp	.L35
.L40:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE212:
	.size	add_niels_to_pt, .-add_niels_to_pt
	.p2align 4
	.type	sub_niels_from_pt, @function
sub_niels_from_pt:
.LFB213:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-256(%rbp), %r15
	leaq	-192(%rbp), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	leaq	64(%r12), %r13
	subq	$232, %rsp
	movl	%edx, -268(%rbp)
	movdqa	64(%rdi), %xmm0
	psubd	(%rdi), %xmm0
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -192(%rbp)
	movd	%xmm0, %eax
	movdqa	80(%rdi), %xmm0
	psubd	16(%rdi), %xmm0
	addl	$536870910, %eax
	addl	$536870910, -188(%rbp)
	addl	$536870910, -184(%rbp)
	movaps	%xmm0, -176(%rbp)
	movdqa	96(%rdi), %xmm0
	psubd	32(%rdi), %xmm0
	addl	$536870910, -180(%rbp)
	movaps	%xmm0, -160(%rbp)
	movdqa	112(%rdi), %xmm0
	psubd	48(%rdi), %xmm0
	addl	$536870910, -176(%rbp)
	addl	$536870910, -172(%rbp)
	movaps	%xmm0, -144(%rbp)
	addl	$536870910, -168(%rbp)
	movdqa	.LC0(%rip), %xmm1
	addl	$536870910, -148(%rbp)
	movdqu	-180(%rbp), %xmm7
	addl	$536870910, -144(%rbp)
	addl	$536870910, -140(%rbp)
	addl	$536870910, -136(%rbp)
	movdqu	-148(%rbp), %xmm5
	addl	$536870910, -132(%rbp)
	movl	-132(%rbp), %edi
	pshufd	$27, -144(%rbp), %xmm0
	pand	%xmm1, %xmm0
	addl	$536870908, -160(%rbp)
	pshufd	$27, %xmm5, %xmm2
	addl	$536870910, -164(%rbp)
	shrl	$28, %edi
	psrld	$28, %xmm2
	addl	%edi, -160(%rbp)
	paddd	%xmm2, %xmm0
	addl	$536870910, -156(%rbp)
	pshufd	$27, %xmm0, %xmm0
	addl	$536870910, -152(%rbp)
	movdqu	-164(%rbp), %xmm6
	movaps	%xmm0, -144(%rbp)
	pshufd	$27, -160(%rbp), %xmm0
	pand	%xmm1, %xmm0
	pshufd	$27, %xmm6, %xmm2
	psrld	$28, %xmm2
	paddd	%xmm2, %xmm0
	pshufd	$27, %xmm7, %xmm2
	pshufd	$27, %xmm0, %xmm0
	psrld	$28, %xmm2
	movaps	%xmm0, -160(%rbp)
	pshufd	$27, -176(%rbp), %xmm0
	pand	%xmm1, %xmm0
	paddd	%xmm2, %xmm0
	pshufd	$27, %xmm0, %xmm0
	movaps	%xmm0, -176(%rbp)
	movl	-184(%rbp), %esi
	movl	-180(%rbp), %edx
	movl	%esi, %r8d
	andl	$268435455, %edx
	andl	$268435455, %esi
	shrl	$28, %r8d
	addl	%r8d, %edx
	movl	%edx, -180(%rbp)
	movl	-188(%rbp), %edx
	movl	%edx, %r8d
	andl	$268435455, %edx
	shrl	$28, %r8d
	addl	%r8d, %esi
	movl	%esi, -184(%rbp)
	movl	%eax, %esi
	andl	$268435455, %eax
	shrl	$28, %esi
	addl	%edi, %eax
	movq	%r15, %rdi
	addl	%esi, %edx
	leaq	64(%rbx), %rsi
	movl	%eax, -192(%rbp)
	movl	%edx, -188(%rbp)
	movq	%r14, %rdx
	call	gf_mul@PLT
	movq	%r14, %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movdqa	(%r12), %xmm0
	paddd	64(%r12), %xmm0
	movaps	%xmm0, -192(%rbp)
	movdqa	16(%r12), %xmm0
	paddd	80(%r12), %xmm0
	movaps	%xmm0, -176(%rbp)
	movdqa	32(%r12), %xmm0
	paddd	96(%r12), %xmm0
	movaps	%xmm0, -160(%rbp)
	movdqa	48(%r12), %xmm0
	paddd	112(%r12), %xmm0
	movaps	%xmm0, -144(%rbp)
	call	gf_mul@PLT
	leaq	192(%r12), %r8
	movq	%r12, %rdi
	leaq	128(%rbx), %rsi
	movq	%r8, %rdx
	movq	%r8, -264(%rbp)
	call	gf_mul@PLT
	movdqa	-256(%rbp), %xmm0
	paddd	64(%r12), %xmm0
	movaps	%xmm0, -128(%rbp)
	movdqa	-240(%rbp), %xmm0
	paddd	80(%r12), %xmm0
	movaps	%xmm0, -112(%rbp)
	movdqa	-224(%rbp), %xmm0
	paddd	96(%r12), %xmm0
	movaps	%xmm0, -96(%rbp)
	movdqa	-208(%rbp), %xmm0
	paddd	112(%r12), %xmm0
	movaps	%xmm0, -80(%rbp)
	movdqa	64(%r12), %xmm0
	psubd	-256(%rbp), %xmm0
	movaps	%xmm0, -192(%rbp)
	movd	%xmm0, %eax
	movdqa	80(%r12), %xmm0
	psubd	-240(%rbp), %xmm0
	addl	$536870910, -188(%rbp)
	addl	$536870910, %eax
	movaps	%xmm0, -176(%rbp)
	movdqa	96(%r12), %xmm0
	psubd	-224(%rbp), %xmm0
	addl	$536870910, -184(%rbp)
	movaps	%xmm0, -160(%rbp)
	movdqa	112(%r12), %xmm0
	psubd	-208(%rbp), %xmm0
	addl	$536870910, -180(%rbp)
	addl	$536870910, -176(%rbp)
	movaps	%xmm0, -144(%rbp)
	addl	$536870910, -172(%rbp)
	movdqa	.LC0(%rip), %xmm1
	addl	$536870910, -148(%rbp)
	addl	$536870910, -144(%rbp)
	addl	$536870910, -140(%rbp)
	addl	$536870910, -136(%rbp)
	movdqu	-148(%rbp), %xmm5
	addl	$536870910, -132(%rbp)
	movl	-132(%rbp), %esi
	pshufd	$27, -144(%rbp), %xmm0
	pand	%xmm1, %xmm0
	addl	$536870908, -160(%rbp)
	pshufd	$27, %xmm5, %xmm2
	addl	$536870910, -156(%rbp)
	shrl	$28, %esi
	psrld	$28, %xmm2
	addl	%esi, -160(%rbp)
	paddd	%xmm2, %xmm0
	addl	$536870910, -152(%rbp)
	pshufd	$27, %xmm0, %xmm0
	addl	$536870910, -164(%rbp)
	movdqu	-164(%rbp), %xmm6
	movaps	%xmm0, -144(%rbp)
	pshufd	$27, -160(%rbp), %xmm0
	pand	%xmm1, %xmm0
	addl	$536870910, -168(%rbp)
	pshufd	$27, %xmm6, %xmm2
	psrld	$28, %xmm2
	paddd	%xmm2, %xmm0
	pshufd	$27, %xmm0, %xmm0
	movaps	%xmm0, -160(%rbp)
	pshufd	$27, -176(%rbp), %xmm0
	movdqu	-180(%rbp), %xmm7
	pand	%xmm1, %xmm0
	movl	-184(%rbp), %ecx
	movl	-180(%rbp), %edx
	pshufd	$27, %xmm7, %xmm2
	movdqa	32(%r12), %xmm4
	movdqa	144(%r12), %xmm3
	psrld	$28, %xmm2
	movl	%ecx, %edi
	andl	$268435455, %edx
	andl	$268435455, %ecx
	paddd	%xmm2, %xmm0
	shrl	$28, %edi
	movdqa	(%r12), %xmm2
	pshufd	$27, %xmm0, %xmm0
	addl	%edi, %edx
	movaps	%xmm0, -176(%rbp)
	movdqa	128(%r12), %xmm0
	movl	%edx, -180(%rbp)
	movl	-188(%rbp), %edx
	paddd	%xmm0, %xmm2
	psubd	(%r12), %xmm0
	movaps	%xmm2, 64(%r12)
	movdqa	16(%r12), %xmm2
	movl	%edx, %edi
	andl	$268435455, %edx
	shrl	$28, %edi
	movaps	%xmm0, -256(%rbp)
	paddd	%xmm3, %xmm2
	addl	%edi, %ecx
	psubd	16(%r12), %xmm3
	movaps	%xmm2, 80(%r12)
	movdqa	160(%r12), %xmm2
	movl	%ecx, -184(%rbp)
	movl	%eax, %ecx
	andl	$268435455, %eax
	addl	%esi, %eax
	paddd	%xmm2, %xmm4
	shrl	$28, %ecx
	psubd	32(%r12), %xmm2
	movaps	%xmm4, 96(%r12)
	movdqa	48(%r12), %xmm4
	addl	%ecx, %edx
	paddd	176(%r12), %xmm4
	movl	%eax, -192(%rbp)
	movd	%xmm0, %eax
	movdqa	176(%r12), %xmm0
	psubd	48(%r12), %xmm0
	movaps	%xmm4, 112(%r12)
	addl	$536870910, %eax
	movaps	%xmm3, -240(%rbp)
	addl	$536870910, -252(%rbp)
	movl	%edx, -188(%rbp)
	movaps	%xmm2, -224(%rbp)
	movaps	%xmm0, -208(%rbp)
	addl	$536870910, -248(%rbp)
	addl	$536870910, -208(%rbp)
	addl	$536870910, -204(%rbp)
	addl	$536870910, -200(%rbp)
	addl	$536870910, -212(%rbp)
	movdqu	-212(%rbp), %xmm5
	addl	$536870910, -196(%rbp)
	movl	-196(%rbp), %esi
	pshufd	$27, -208(%rbp), %xmm0
	pand	%xmm1, %xmm0
	addl	$536870908, -224(%rbp)
	pshufd	$27, %xmm5, %xmm2
	psrld	$28, %xmm2
	shrl	$28, %esi
	addl	$536870910, -244(%rbp)
	paddd	%xmm2, %xmm0
	addl	%esi, -224(%rbp)
	addl	$536870910, -240(%rbp)
	pshufd	$27, %xmm0, %xmm0
	addl	$536870910, -236(%rbp)
	addl	$536870910, -232(%rbp)
	addl	$536870910, -228(%rbp)
	addl	$536870910, -220(%rbp)
	addl	$536870910, -216(%rbp)
	movaps	%xmm0, -208(%rbp)
	movl	-248(%rbp), %edx
	movl	-244(%rbp), %ecx
	pshufd	$27, -224(%rbp), %xmm0
	movdqu	-228(%rbp), %xmm6
	movdqu	-244(%rbp), %xmm7
	pand	%xmm1, %xmm0
	movl	%edx, %edi
	andl	$268435455, %ecx
	andl	$268435455, %edx
	shrl	$28, %edi
	pshufd	$27, %xmm6, %xmm2
	addl	%edi, %ecx
	psrld	$28, %xmm2
	movl	%ecx, -244(%rbp)
	movl	-252(%rbp), %ecx
	paddd	%xmm2, %xmm0
	pshufd	$27, -240(%rbp), %xmm2
	pshufd	$27, %xmm0, %xmm0
	pand	%xmm2, %xmm1
	movl	%ecx, %edi
	movaps	%xmm0, -224(%rbp)
	pshufd	$27, %xmm7, %xmm0
	andl	$268435455, %ecx
	shrl	$28, %edi
	psrld	$28, %xmm0
	addl	%edi, %edx
	paddd	%xmm1, %xmm0
	leaq	128(%r12), %rdi
	movl	%edx, -248(%rbp)
	movl	%eax, %edx
	andl	$268435455, %eax
	pshufd	$27, %xmm0, %xmm0
	shrl	$28, %edx
	addl	%esi, %eax
	movq	%r15, %rsi
	movaps	%xmm0, -240(%rbp)
	addl	%edx, %ecx
	movq	%r13, %rdx
	movl	%eax, -256(%rbp)
	movl	%ecx, -252(%rbp)
	call	gf_mul@PLT
	movq	%r12, %rdi
	movq	%r14, %rdx
	movq	%r13, %rsi
	call	gf_mul@PLT
	leaq	-128(%rbp), %r12
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%r12, %rdx
	call	gf_mul@PLT
	movl	-268(%rbp), %r9d
	movq	-264(%rbp), %r8
	testl	%r9d, %r9d
	je	.L45
.L41:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L46
	addq	$232, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L45:
	.cfi_restore_state
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r8, %rdi
	call	gf_mul@PLT
	jmp	.L41
.L46:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE213:
	.size	sub_niels_from_pt, .-sub_niels_from_pt
	.p2align 4
	.globl	curve448_point_double
	.type	curve448_point_double, @function
curve448_point_double:
.LFB207:
	.cfi_startproc
	endbr64
	jmp	point_double_internal.constprop.0
	.cfi_endproc
.LFE207:
	.size	curve448_point_double, .-curve448_point_double
	.p2align 4
	.globl	curve448_point_eq
	.type	curve448_point_eq, @function
curve448_point_eq:
.LFB216:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-176(%rbp), %r13
	leaq	-112(%rbp), %r14
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	leaq	64(%rdi), %rsi
	movq	%r13, %rdi
	movq	%rbx, %rdx
	subq	$144, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	gf_mul@PLT
	leaq	64(%rbx), %rsi
	movq	%r12, %rdx
	movq	%r14, %rdi
	call	gf_mul@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	gf_eq@PLT
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L51
	addq	$144, %rsp
	cltq
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L51:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE216:
	.size	curve448_point_eq, .-curve448_point_eq
	.p2align 4
	.globl	curve448_point_valid
	.type	curve448_point_valid, @function
curve448_point_valid:
.LFB217:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	64(%rdi), %r9
	movq	%rdi, %rsi
	movq	%r9, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-256(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%r14, %rdi
	pushq	%r12
	leaq	128(%r13), %r15
	.cfi_offset 12, -48
	leaq	-192(%rbp), %r12
	pushq	%rbx
	subq	$232, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r9, -272(%rbp)
	call	gf_mul@PLT
	leaq	192(%r13), %r8
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%r8, %rdx
	movq	%r8, -264(%rbp)
	call	gf_mul@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	gf_eq@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	leaq	-128(%rbp), %r13
	movl	%eax, %ebx
	call	gf_sqr@PLT
	movq	-272(%rbp), %r9
	movq	%r12, %rdi
	movq	%r9, %rsi
	call	gf_sqr@PLT
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	gf_sub@PLT
	movq	-264(%rbp), %r8
	movq	%r12, %rdi
	movq	%r8, %rsi
	call	gf_sqr@PLT
	movl	$39082, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	gf_mulw_unsigned@PLT
	movq	%r13, %rdx
	leaq	ZERO(%rip), %rsi
	movq	%r13, %rdi
	call	gf_sub@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	gf_sqr@PLT
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	gf_add@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	gf_eq@PLT
	leaq	ZERO(%rip), %rsi
	movq	%r15, %rdi
	andl	%eax, %ebx
	call	gf_eq@PLT
	notl	%eax
	andl	%ebx, %eax
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L55
	addq	$232, %rsp
	cltq
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L55:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE217:
	.size	curve448_point_valid, .-curve448_point_valid
	.p2align 4
	.globl	curve448_precomputed_scalarmul
	.type	curve448_precomputed_scalarmul, @function
curve448_precomputed_scalarmul:
.LFB219:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-64(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movq	%rdx, %rsi
	leaq	precomputed_scalarmul_adjustment(%rip), %rdx
	subq	$408, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-384(%rbp), %rax
	movq	%rax, %r14
	movq	%rax, %rdi
	movq	%rax, -448(%rbp)
	call	curve448_scalar_add@PLT
	movq	%r14, %rsi
	movq	%r14, %rdi
	leaq	-256(%rbp), %r14
	call	curve448_scalar_halve@PLT
	leaq	-128(%rbp), %rax
	movq	%r12, -424(%rbp)
	movq	%rax, -408(%rbp)
	leaq	-320(%rbp), %rax
	movq	%rax, -416(%rbp)
	leaq	3072(%rbx), %rax
	movl	$18, -432(%rbp)
	movq	%rax, -440(%rbp)
	.p2align 4,,10
	.p2align 3
.L57:
	movl	-432(%rbp), %eax
	movq	-440(%rbp), %r12
	cmpl	$18, %eax
	leal	71(%rax), %r8d
	setne	-397(%rbp)
	cmpl	$1, %eax
	setne	-428(%rbp)
	xorl	%r10d, %r10d
.L64:
	leal	-54(%r8), %ecx
	leal	-36(%r8), %r11d
	movl	%ecx, %eax
	leal	-72(%r8), %edi
	leal	-18(%r8), %esi
	shrl	$6, %eax
	movq	-384(%rbp,%rax,8), %rdx
	movl	%r11d, %eax
	shrl	$6, %eax
	movq	-384(%rbp,%rax,8), %rax
	shrq	%cl, %rdx
	movl	%r11d, %ecx
	addq	%rdx, %rdx
	shrq	%cl, %rax
	andl	$2, %edx
	movl	%edi, %ecx
	salq	$2, %rax
	andl	$4, %eax
	orl	%eax, %edx
	movl	%edi, %eax
	shrl	$6, %eax
	movq	-384(%rbp,%rax,8), %rax
	shrq	%cl, %rax
	movl	%esi, %ecx
	andl	$1, %eax
	orl	%edx, %eax
	movl	%esi, %edx
	shrl	$6, %edx
	movq	-384(%rbp,%rdx,8), %rdi
	shrq	%cl, %rdi
	leaq	0(,%rdi,8), %rdx
	andl	$8, %edx
	orl	%eax, %edx
	cmpl	$445, %r8d
	ja	.L65
	movl	%r8d, %eax
	movl	%r8d, %ecx
	shrl	$6, %eax
	movq	-384(%rbp,%rax,8), %rax
	shrq	%cl, %rax
	salq	$4, %rax
	andl	$16, %eax
	orl	%eax, %edx
.L65:
	movl	$24, %ecx
	movq	%r14, %rdi
	xorl	%eax, %eax
	movl	%edx, %r15d
	rep stosq
	sarl	$4, %r15d
	movq	-424(%rbp), %r9
	leaq	-3072(%r12), %rsi
	leal	-1(%r15), %ebx
	xorl	%ebx, %edx
	andl	$15, %edx
	.p2align 4,,10
	.p2align 3
.L59:
	movq	%rdx, %rax
	subq	$1, %rdx
	movq	%rsi, %rdi
	movq	%r14, %rcx
	notq	%rax
	andq	%rdx, %rax
	sarq	$63, %rax
	movzbl	%al, %eax
	.p2align 4,,10
	.p2align 3
.L58:
	movzbl	(%rdi), %r11d
	addq	$1, %rdi
	andl	%eax, %r11d
	orb	%r11b, (%rcx)
	addq	$1, %rcx
	cmpq	%rcx, %r9
	jne	.L58
	addq	$192, %rsi
	cmpq	%r12, %rsi
	jne	.L59
	movd	%ebx, %xmm5
	movdqa	-192(%rbp), %xmm3
	movdqa	-176(%rbp), %xmm2
	leaq	ZERO(%rip), %rsi
	pxor	-256(%rbp), %xmm3
	pshufd	$0, %xmm5, %xmm4
	negl	%r15d
	movdqa	-160(%rbp), %xmm1
	pxor	-240(%rbp), %xmm2
	pxor	-224(%rbp), %xmm1
	movl	%r10d, -396(%rbp)
	movdqa	-144(%rbp), %xmm0
	pxor	-208(%rbp), %xmm0
	pand	%xmm4, %xmm3
	movl	%r8d, -392(%rbp)
	pand	%xmm4, %xmm2
	pand	%xmm4, %xmm1
	movq	-408(%rbp), %rdx
	movq	-416(%rbp), %rdi
	pand	%xmm4, %xmm0
	movdqa	-256(%rbp), %xmm4
	pxor	%xmm3, %xmm4
	pxor	-192(%rbp), %xmm3
	movaps	%xmm4, -256(%rbp)
	movdqa	-240(%rbp), %xmm4
	movaps	%xmm3, -192(%rbp)
	pxor	%xmm2, %xmm4
	pxor	-176(%rbp), %xmm2
	movaps	%xmm4, -240(%rbp)
	movdqa	-224(%rbp), %xmm4
	movaps	%xmm2, -176(%rbp)
	pxor	%xmm1, %xmm4
	pxor	-160(%rbp), %xmm1
	movaps	%xmm4, -224(%rbp)
	movdqa	-208(%rbp), %xmm4
	movaps	%xmm1, -160(%rbp)
	pxor	%xmm0, %xmm4
	pxor	-144(%rbp), %xmm0
	movaps	%xmm4, -208(%rbp)
	movaps	%xmm0, -144(%rbp)
	call	gf_sub@PLT
	movl	-272(%rbp), %eax
	movl	-80(%rbp), %edx
	andl	%ebx, %eax
	andl	%r15d, %edx
	movl	-72(%rbp), %ecx
	orl	%edx, %eax
	movl	-76(%rbp), %edx
	movd	%eax, %xmm4
	movl	-268(%rbp), %eax
	andl	%r15d, %ecx
	andl	%r15d, %edx
	andl	%ebx, %eax
	orl	%edx, %eax
	movl	-264(%rbp), %edx
	movd	%eax, %xmm7
	movl	-288(%rbp), %eax
	andl	%ebx, %edx
	punpckldq	%xmm7, %xmm4
	orl	%ecx, %edx
	movl	-68(%rbp), %ecx
	andl	%ebx, %eax
	movd	%edx, %xmm0
	movl	-260(%rbp), %edx
	andl	%r15d, %ecx
	andl	%ebx, %edx
	orl	%edx, %ecx
	movl	-96(%rbp), %edx
	movd	%ecx, %xmm6
	movl	-88(%rbp), %ecx
	andl	%r15d, %edx
	punpckldq	%xmm6, %xmm0
	orl	%edx, %eax
	punpcklqdq	%xmm0, %xmm4
	movl	-92(%rbp), %edx
	andl	%r15d, %ecx
	movd	%eax, %xmm0
	movl	-284(%rbp), %eax
	andl	%r15d, %edx
	andl	%ebx, %eax
	orl	%edx, %eax
	movl	-280(%rbp), %edx
	movd	%eax, %xmm6
	movl	-304(%rbp), %eax
	andl	%ebx, %edx
	punpckldq	%xmm6, %xmm0
	orl	%ecx, %edx
	movl	-84(%rbp), %ecx
	andl	%ebx, %eax
	movd	%edx, %xmm1
	movl	-276(%rbp), %edx
	andl	%r15d, %ecx
	andl	%ebx, %edx
	orl	%edx, %ecx
	movl	-112(%rbp), %edx
	movd	%ecx, %xmm5
	movl	-104(%rbp), %ecx
	andl	%r15d, %edx
	punpckldq	%xmm5, %xmm1
	orl	%edx, %eax
	punpcklqdq	%xmm1, %xmm0
	movl	-108(%rbp), %edx
	movd	%eax, %xmm1
	movl	-300(%rbp), %eax
	andl	%ebx, %eax
	andl	%r15d, %edx
	andl	%r15d, %ecx
	orl	%edx, %eax
	movl	-296(%rbp), %edx
	movd	%eax, %xmm5
	movl	-128(%rbp), %eax
	andl	%ebx, %edx
	punpckldq	%xmm5, %xmm1
	orl	%ecx, %edx
	movl	-100(%rbp), %ecx
	andl	%r15d, %eax
	movd	%edx, %xmm2
	movl	-292(%rbp), %edx
	andl	%r15d, %ecx
	andl	%ebx, %edx
	orl	%edx, %ecx
	movl	-320(%rbp), %edx
	movd	%ecx, %xmm7
	movl	-120(%rbp), %ecx
	andl	%ebx, %edx
	punpckldq	%xmm7, %xmm2
	orl	%edx, %eax
	punpcklqdq	%xmm2, %xmm1
	movl	-124(%rbp), %edx
	andl	%r15d, %ecx
	movd	%eax, %xmm2
	movl	-316(%rbp), %eax
	andl	%r15d, %edx
	andl	-116(%rbp), %r15d
	andl	%ebx, %eax
	orl	%edx, %eax
	movl	-312(%rbp), %edx
	movd	%eax, %xmm7
	andl	%ebx, %edx
	andl	-308(%rbp), %ebx
	punpckldq	%xmm7, %xmm2
	orl	%r15d, %ebx
	orl	%ecx, %edx
	movd	%ebx, %xmm6
	movd	%edx, %xmm3
	punpckldq	%xmm6, %xmm3
	punpcklqdq	%xmm3, %xmm2
	movaps	%xmm2, -128(%rbp)
	movl	-396(%rbp), %r10d
	movl	-392(%rbp), %r8d
	movaps	%xmm1, -112(%rbp)
	testl	%r10d, %r10d
	movaps	%xmm0, -96(%rbp)
	leal	1(%r10), %ebx
	movaps	%xmm4, -80(%rbp)
	jne	.L68
	cmpb	$0, -397(%rbp)
	jne	.L68
	leaq	64(%r13), %r15
	leaq	-192(%rbp), %rsi
	movq	%r14, %rdx
	movl	%r8d, -396(%rbp)
	movq	%r15, %rdi
	movq	%rsi, -392(%rbp)
	call	gf_add@PLT
	movq	-392(%rbp), %rsi
	movq	%r14, %rdx
	movq	%r13, %rdi
	call	gf_sub@PLT
	leaq	192(%r13), %rdi
	movq	%r13, %rdx
	movq	%r15, %rsi
	call	gf_mul@PLT
	movdqa	ONE(%rip), %xmm5
	movdqa	16+ONE(%rip), %xmm6
	movdqa	32+ONE(%rip), %xmm7
	movl	-396(%rbp), %r8d
	movaps	%xmm5, 128(%r13)
	movdqa	48+ONE(%rip), %xmm5
	movaps	%xmm6, 144(%r13)
	movaps	%xmm7, 160(%r13)
	movaps	%xmm5, 176(%r13)
.L62:
	addl	$90, %r8d
	addq	$3072, %r12
	movl	%ebx, %r10d
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L68:
	xorl	%edx, %edx
	cmpl	$4, %r10d
	movq	%r14, %rsi
	movq	%r13, %rdi
	sete	%dl
	andl	-428(%rbp), %edx
	movl	%r8d, -392(%rbp)
	call	add_niels_to_pt
	cmpl	$5, %ebx
	movl	-392(%rbp), %r8d
	jne	.L62
	subl	$1, -432(%rbp)
	je	.L73
	movq	%r13, %rsi
	movq	%r13, %rdi
	call	point_double_internal.constprop.0
	jmp	.L57
.L73:
	movl	$192, %esi
	movq	%r14, %rdi
	call	OPENSSL_cleanse@PLT
	movq	-448(%rbp), %rdi
	movl	$56, %esi
	call	OPENSSL_cleanse@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L74
	addq	$408, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L74:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE219:
	.size	curve448_precomputed_scalarmul, .-curve448_precomputed_scalarmul
	.p2align 4
	.globl	curve448_point_mul_by_ratio_and_encode_like_eddsa
	.type	curve448_point_mul_by_ratio_and_encode_like_eddsa, @function
curve448_point_mul_by_ratio_and_encode_like_eddsa:
.LFB220:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	leaq	-320(%rbp), %r9
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-704(%rbp), %r14
	pushq	%r13
	leaq	-384(%rbp), %r15
	.cfi_offset 13, -40
	leaq	-576(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-512(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-256(%rbp), %rbx
	subq	$696, %rsp
	movdqa	(%rsi), %xmm0
	movq	%rdi, -728(%rbp)
	movq	%r14, %rdi
	movdqa	16(%rsi), %xmm1
	movdqa	32(%rsi), %xmm2
	movdqa	64(%rsi), %xmm4
	movdqa	80(%rsi), %xmm5
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movdqa	96(%rsi), %xmm6
	movdqa	112(%rsi), %xmm7
	movaps	%xmm0, -320(%rbp)
	movdqa	48(%rsi), %xmm3
	movdqa	128(%rsi), %xmm0
	movaps	%xmm1, -304(%rbp)
	movaps	%xmm2, -288(%rbp)
	movdqa	144(%rsi), %xmm1
	movdqa	160(%rsi), %xmm2
	movaps	%xmm0, -192(%rbp)
	movaps	%xmm1, -176(%rbp)
	movaps	%xmm2, -160(%rbp)
	movaps	%xmm4, -256(%rbp)
	movaps	%xmm5, -240(%rbp)
	movaps	%xmm6, -224(%rbp)
	movaps	%xmm7, -208(%rbp)
	movaps	%xmm3, -272(%rbp)
	movdqa	176(%rsi), %xmm3
	movdqa	192(%rsi), %xmm4
	movdqa	208(%rsi), %xmm5
	movdqa	224(%rsi), %xmm6
	movq	%r9, -712(%rbp)
	movdqa	240(%rsi), %xmm7
	movq	%r9, %rsi
	movaps	%xmm4, -128(%rbp)
	movaps	%xmm5, -112(%rbp)
	movaps	%xmm6, -96(%rbp)
	movaps	%xmm7, -80(%rbp)
	movaps	%xmm3, -144(%rbp)
	call	gf_sqr@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	gf_sqr@PLT
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	gf_add@PLT
	movq	-712(%rbp), %r9
	movq	%rbx, %rsi
	movq	%r13, %rdi
	leaq	-640(%rbp), %rbx
	movq	%r9, %rdx
	movq	%r9, -720(%rbp)
	call	gf_add@PLT
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	gf_sqr@PLT
	movq	%r15, %rdx
	movq	%rbx, %rsi
	movq	%rbx, %rdi
	call	gf_sub@PLT
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	gf_sub@PLT
	leaq	-192(%rbp), %rsi
	movq	%r14, %rdi
	call	gf_sqr@PLT
	movq	%r14, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	gf_add@PLT
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	gf_sub@PLT
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	gf_mul@PLT
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	gf_mul@PLT
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	gf_mul@PLT
	movl	$64, %esi
	movq	%r15, %rdi
	call	OPENSSL_cleanse@PLT
	leaq	-448(%rbp), %r8
	movq	%r13, %rsi
	movq	%r8, %rdi
	movq	%r8, -712(%rbp)
	call	gf_sqr@PLT
	movq	-712(%rbp), %r8
	movq	%r15, %rdi
	movq	%r8, %rsi
	call	gf_isr@PLT
	movq	-712(%rbp), %r8
	movq	%r15, %rsi
	movq	%r8, %rdi
	call	gf_sqr@PLT
	movq	-712(%rbp), %r8
	movq	%r13, %rdx
	movq	%r15, %rdi
	movq	%r8, %rsi
	call	gf_mul@PLT
	movdqa	-384(%rbp), %xmm0
	movq	%r13, %rdx
	movdqa	-368(%rbp), %xmm1
	movdqa	-352(%rbp), %xmm2
	movdqa	-336(%rbp), %xmm3
	movq	%r14, %rsi
	movq	%r12, %rdi
	movaps	%xmm0, -576(%rbp)
	movaps	%xmm1, -560(%rbp)
	movaps	%xmm2, -544(%rbp)
	movaps	%xmm3, -528(%rbp)
	call	gf_mul@PLT
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	gf_mul@PLT
	movq	-728(%rbp), %rcx
	movl	$1, %edx
	movq	%r14, %rsi
	movb	$0, 56(%rcx)
	movq	%rcx, %rdi
	movq	%rcx, -712(%rbp)
	call	gf_serialize@PLT
	movq	%r12, %rdi
	call	gf_lobit@PLT
	movq	-712(%rbp), %rcx
	movl	$64, %esi
	movq	%r14, %rdi
	andl	$128, %eax
	orb	%al, 56(%rcx)
	call	OPENSSL_cleanse@PLT
	movl	$64, %esi
	movq	%rbx, %rdi
	call	OPENSSL_cleanse@PLT
	movl	$64, %esi
	movq	%r13, %rdi
	call	OPENSSL_cleanse@PLT
	movl	$64, %esi
	movq	%r12, %rdi
	call	OPENSSL_cleanse@PLT
	movq	-720(%rbp), %r9
	movl	$256, %esi
	movq	%r9, %rdi
	call	OPENSSL_cleanse@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L78
	addq	$696, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L78:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE220:
	.size	curve448_point_mul_by_ratio_and_encode_like_eddsa, .-curve448_point_mul_by_ratio_and_encode_like_eddsa
	.p2align 4
	.globl	curve448_point_decode_like_eddsa_and_mul_by_ratio
	.type	curve448_point_decode_like_eddsa_and_mul_by_ratio, @function
curve448_point_decode_like_eddsa_and_mul_by_ratio:
.LFB221:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%ecx, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	leaq	-128(%rbp), %r8
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	64(%rdi), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r14, %rdi
	pushq	%rbx
	leaq	192(%r12), %r13
	subq	$392, %rsp
	.cfi_offset 3, -56
	movzbl	56(%rsi), %edx
	movdqu	(%rsi), %xmm0
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movdqu	16(%rsi), %xmm1
	movdqu	32(%rsi), %xmm2
	movq	%r8, -424(%rbp)
	movq	48(%rsi), %rax
	movl	%edx, %r9d
	andl	$127, %edx
	movq	%r8, %rsi
	andl	$-128, %r9d
	movb	%dl, -72(%rbp)
	movl	$1, %edx
	movaps	%xmm0, -128(%rbp)
	movzbl	%r9b, %r15d
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm2, -96(%rbp)
	movq	%rax, -80(%rbp)
	call	gf_deserialize@PLT
	movzbl	-72(%rbp), %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%r14, -392(%rbp)
	leaq	128(%r12), %r14
	movl	%eax, %ebx
	movl	%ecx, -400(%rbp)
	call	gf_sqr@PLT
	movq	%r12, %rdx
	leaq	ONE(%rip), %rsi
	movq	%r14, %rdi
	call	gf_sub@PLT
	movl	$39081, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	gf_mulw_unsigned@PLT
	movq	%r13, %rdx
	leaq	ZERO(%rip), %rsi
	movq	%r13, %rdi
	call	gf_sub@PLT
	movq	%r13, %rdx
	leaq	ONE(%rip), %rsi
	movq	%r13, %rdi
	call	gf_sub@PLT
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	gf_mul@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	gf_isr@PLT
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	-400(%rbp), %ecx
	andl	%eax, %ebx
	movl	%ecx, %r10d
	subl	$1, %ecx
	notl	%r10d
	andl	%ecx, %r10d
	sarl	$31, %r10d
	andl	%ebx, %r10d
	movl	%r10d, -412(%rbp)
	call	gf_mul@PLT
	movq	%r12, %rdi
	call	gf_lobit@PLT
	leal	-1(%r15), %r9d
	movq	%r12, %rdx
	leaq	ZERO(%rip), %rsi
	movl	%eax, %r11d
	movl	%r15d, %eax
	leaq	-192(%rbp), %r15
	notl	%eax
	movq	%r15, %rdi
	andl	%r9d, %eax
	sarl	$31, %eax
	xorl	%r11d, %eax
	movl	%eax, %ebx
	movl	%eax, -400(%rbp)
	call	gf_sub@PLT
	movl	(%r12), %ecx
	notl	%ebx
	movq	%r12, %rsi
	movl	-192(%rbp), %edx
	movl	-400(%rbp), %eax
	leaq	-256(%rbp), %r9
	andl	%eax, %ecx
	andl	%ebx, %edx
	movq	%r9, %rdi
	orl	%ecx, %edx
	movl	4(%r12), %ecx
	movl	%edx, (%r12)
	movl	-188(%rbp), %edx
	andl	%eax, %ecx
	andl	%ebx, %edx
	orl	%ecx, %edx
	movl	8(%r12), %ecx
	movl	%edx, 4(%r12)
	movl	-184(%rbp), %edx
	andl	%ebx, %edx
	andl	%eax, %ecx
	orl	%ecx, %edx
	movl	12(%r12), %ecx
	movl	%edx, 8(%r12)
	movl	-180(%rbp), %edx
	andl	%eax, %ecx
	andl	%ebx, %edx
	orl	%ecx, %edx
	movl	16(%r12), %ecx
	movl	%edx, 12(%r12)
	movl	-176(%rbp), %edx
	andl	%eax, %ecx
	andl	%ebx, %edx
	orl	%ecx, %edx
	movl	20(%r12), %ecx
	movl	%edx, 16(%r12)
	movl	-172(%rbp), %edx
	andl	%eax, %ecx
	andl	%ebx, %edx
	orl	%ecx, %edx
	movl	24(%r12), %ecx
	movl	%edx, 20(%r12)
	movl	-168(%rbp), %edx
	andl	%eax, %ecx
	andl	%ebx, %edx
	orl	%ecx, %edx
	movl	28(%r12), %ecx
	movl	%edx, 24(%r12)
	movl	-164(%rbp), %edx
	andl	%eax, %ecx
	andl	%ebx, %edx
	orl	%ecx, %edx
	movl	32(%r12), %ecx
	movl	%edx, 28(%r12)
	movl	-160(%rbp), %edx
	andl	%eax, %ecx
	andl	%ebx, %edx
	orl	%ecx, %edx
	movl	36(%r12), %ecx
	movl	%edx, 32(%r12)
	movl	-156(%rbp), %edx
	andl	%eax, %ecx
	andl	%ebx, %edx
	orl	%ecx, %edx
	movl	%edx, 36(%r12)
	movl	-152(%rbp), %edx
	movl	40(%r12), %ecx
	movdqa	ONE(%rip), %xmm3
	movq	%r9, -400(%rbp)
	andl	%ebx, %edx
	movdqa	16+ONE(%rip), %xmm4
	movdqa	32+ONE(%rip), %xmm5
	andl	%eax, %ecx
	movdqa	48+ONE(%rip), %xmm6
	movaps	%xmm3, 128(%r12)
	movaps	%xmm4, 144(%r12)
	orl	%ecx, %edx
	movl	44(%r12), %ecx
	movl	%edx, 40(%r12)
	movl	-148(%rbp), %edx
	movaps	%xmm5, 160(%r12)
	andl	%eax, %ecx
	movaps	%xmm6, 176(%r12)
	andl	%ebx, %edx
	orl	%ecx, %edx
	movl	48(%r12), %ecx
	movl	%edx, 44(%r12)
	movl	-144(%rbp), %edx
	andl	%eax, %ecx
	andl	%ebx, %edx
	orl	%ecx, %edx
	movl	52(%r12), %ecx
	movl	%edx, 48(%r12)
	movl	-140(%rbp), %edx
	andl	%eax, %ecx
	andl	%ebx, %edx
	orl	%ecx, %edx
	movl	56(%r12), %ecx
	movl	%edx, 52(%r12)
	movl	-136(%rbp), %edx
	andl	%eax, %ecx
	andl	60(%r12), %eax
	andl	%ebx, %edx
	andl	-132(%rbp), %ebx
	orl	%ecx, %edx
	orl	%ebx, %eax
	leaq	-384(%rbp), %rbx
	movl	%eax, 60(%r12)
	movl	%edx, 56(%r12)
	call	gf_sqr@PLT
	movq	-392(%rbp), %rsi
	movq	%rbx, %rdi
	call	gf_sqr@PLT
	movq	-400(%rbp), %r9
	movq	%rbx, %rdx
	movq	%r15, %rdi
	movq	%r9, %rsi
	movq	%r9, -408(%rbp)
	call	gf_add@PLT
	movq	-392(%rbp), %rsi
	movq	%r12, %rdx
	movq	%r13, %rdi
	call	gf_add@PLT
	leaq	-320(%rbp), %r11
	movq	%r13, %rsi
	movq	%r11, %rdi
	movq	%r11, -400(%rbp)
	call	gf_sqr@PLT
	movq	-400(%rbp), %r11
	movq	%r15, %rdx
	movq	%r11, %rsi
	movq	%r11, %rdi
	call	gf_sub@PLT
	movq	-408(%rbp), %r9
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%r9, %rdx
	call	gf_sub@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	gf_sqr@PLT
	movq	%r12, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	gf_add@PLT
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	gf_sub@PLT
	movq	-400(%rbp), %r11
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%r11, %rdx
	call	gf_mul@PLT
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	gf_mul@PLT
	movq	-392(%rbp), %rdi
	movq	%r15, %rdx
	movq	%r13, %rsi
	call	gf_mul@PLT
	movq	-400(%rbp), %r11
	movq	%r15, %rdx
	movq	%r13, %rdi
	movq	%r11, %rsi
	movq	%r11, -392(%rbp)
	call	gf_mul@PLT
	movl	$64, %esi
	movq	%rbx, %rdi
	call	OPENSSL_cleanse@PLT
	movq	-392(%rbp), %r11
	movl	$64, %esi
	movq	%r11, %rdi
	call	OPENSSL_cleanse@PLT
	movq	-408(%rbp), %r9
	movl	$64, %esi
	movq	%r9, %rdi
	call	OPENSSL_cleanse@PLT
	movl	$64, %esi
	movq	%r15, %rdi
	call	OPENSSL_cleanse@PLT
	movq	-424(%rbp), %r8
	movl	$57, %esi
	movq	%r8, %rdi
	call	OPENSSL_cleanse@PLT
	movl	-412(%rbp), %r10d
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L82
	addq	$392, %rsp
	movl	%r10d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L82:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE221:
	.size	curve448_point_decode_like_eddsa_and_mul_by_ratio, .-curve448_point_decode_like_eddsa_and_mul_by_ratio
	.p2align 4
	.globl	x448_int
	.type	x448_int, @function
x448_int:
.LFB222:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%ecx, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-512(%rbp), %r15
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	-320(%rbp), %rbx
	subq	$904, %rsp
	movq	%rdi, -928(%rbp)
	movq	%rdx, -920(%rbp)
	movl	$1, %edx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-640(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -912(%rbp)
	call	gf_deserialize@PLT
	movdqa	ZERO(%rip), %xmm4
	xorl	%eax, %eax
	movdqa	16+ZERO(%rip), %xmm5
	movdqa	32+ZERO(%rip), %xmm6
	movdqa	48+ZERO(%rip), %xmm7
	movq	%rbx, -656(%rbp)
	leaq	-576(%rbp), %rbx
	movdqa	ONE(%rip), %xmm3
	movdqa	16+ONE(%rip), %xmm2
	movaps	%xmm4, -512(%rbp)
	movl	%eax, %r14d
	movdqa	32+ONE(%rip), %xmm1
	movdqa	48+ONE(%rip), %xmm0
	movaps	%xmm5, -496(%rbp)
	movdqa	-640(%rbp), %xmm4
	movdqa	-624(%rbp), %xmm5
	movaps	%xmm6, -480(%rbp)
	movaps	%xmm7, -464(%rbp)
	movdqa	-608(%rbp), %xmm6
	movdqa	-592(%rbp), %xmm7
	movq	%rbx, -752(%rbp)
	leaq	-256(%rbp), %rbx
	movl	$447, -644(%rbp)
	movq	%r15, -936(%rbp)
	movaps	%xmm3, -576(%rbp)
	movaps	%xmm2, -560(%rbp)
	movaps	%xmm1, -544(%rbp)
	movaps	%xmm0, -528(%rbp)
	movaps	%xmm4, -448(%rbp)
	movaps	%xmm5, -432(%rbp)
	movaps	%xmm6, -416(%rbp)
	movaps	%xmm7, -400(%rbp)
	movaps	%xmm3, -384(%rbp)
	movaps	%xmm2, -368(%rbp)
	movaps	%xmm1, -352(%rbp)
	movaps	%xmm0, -336(%rbp)
	movq	%rbx, -664(%rbp)
	leaq	-448(%rbp), %rbx
	movq	%rbx, -760(%rbp)
	leaq	-384(%rbp), %rbx
	movq	%rbx, -904(%rbp)
	movl	-644(%rbp), %ebx
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L89:
	movl	-764(%rbp), %r14d
	movl	%ecx, %ebx
.L86:
	movl	%ebx, %eax
	movq	-920(%rbp), %rdi
	sarl	$3, %eax
	cltq
	movzbl	(%rdi,%rax), %edx
	leal	7(%rbx), %eax
	cmpl	$14, %eax
	jbe	.L91
	cmpl	$447, -644(%rbp)
	movl	$255, %eax
	movl	%ebx, %ecx
	cmove	%eax, %edx
.L85:
	andl	$7, %ecx
	sarl	%cl, %edx
	movl	-576(%rbp), %ecx
	andl	$1, %edx
	negl	%edx
	movl	%ecx, %eax
	movl	%ecx, %ebx
	movl	-572(%rbp), %ecx
	xorl	%edx, %r14d
	movl	%edx, -764(%rbp)
	movl	-448(%rbp), %edx
	xorl	%edx, %eax
	movl	%edx, %edi
	movl	-444(%rbp), %edx
	andl	%r14d, %eax
	xorl	%eax, %ebx
	xorl	%eax, %edi
	movl	%ecx, %eax
	movl	%edx, %esi
	xorl	%edx, %eax
	movl	%edi, -836(%rbp)
	movl	-440(%rbp), %edx
	movl	%edi, -448(%rbp)
	movl	%ecx, %edi
	movl	-568(%rbp), %ecx
	andl	%r14d, %eax
	xorl	%eax, %edi
	xorl	%eax, %esi
	movl	%ebx, -768(%rbp)
	movl	%ecx, %eax
	movl	%esi, -840(%rbp)
	xorl	%edx, %eax
	movl	%esi, -444(%rbp)
	movl	%ecx, %esi
	movl	-564(%rbp), %ecx
	andl	%r14d, %eax
	movl	%ebx, -576(%rbp)
	xorl	%eax, %edx
	movl	%ecx, %r9d
	xorl	%eax, %esi
	movl	%edi, -712(%rbp)
	movl	%edx, -844(%rbp)
	movl	%edx, -440(%rbp)
	movl	-436(%rbp), %edx
	movl	%edi, -572(%rbp)
	xorl	%edx, %r9d
	movl	%edx, %r11d
	movl	-432(%rbp), %edx
	movl	%esi, -716(%rbp)
	movl	%r9d, %eax
	movl	%ecx, %r9d
	movl	-560(%rbp), %ecx
	movl	%esi, -568(%rbp)
	andl	%r14d, %eax
	movl	%edx, %r8d
	movl	%ecx, %r10d
	xorl	%eax, %r9d
	xorl	%eax, %r11d
	xorl	%edx, %r10d
	movl	-556(%rbp), %edx
	movl	%r9d, -720(%rbp)
	movl	%r10d, %eax
	movl	%ecx, %r10d
	movl	%r9d, -564(%rbp)
	andl	%r14d, %eax
	movl	%r11d, -848(%rbp)
	movl	%edx, %r13d
	xorl	%eax, %r10d
	xorl	%eax, %r8d
	movl	%r11d, -436(%rbp)
	movl	%r10d, -724(%rbp)
	movl	%r10d, -560(%rbp)
	movl	%r8d, -852(%rbp)
	movl	%r8d, -432(%rbp)
	movl	-428(%rbp), %ecx
	movl	-548(%rbp), %esi
	movl	-540(%rbp), %edi
	xorl	%ecx, %r13d
	movl	%r13d, %eax
	movl	%ecx, %r13d
	movl	-552(%rbp), %ecx
	andl	%r14d, %eax
	xorl	%eax, %edx
	movl	%ecx, %r12d
	xorl	%eax, %r13d
	movl	%ecx, %r8d
	movl	%edx, -728(%rbp)
	movl	%edx, -556(%rbp)
	movl	-424(%rbp), %edx
	movl	%r13d, -856(%rbp)
	xorl	%edx, %r12d
	movl	%r13d, -428(%rbp)
	movl	%esi, %r13d
	movl	%r12d, %eax
	movl	%edx, %r12d
	movl	-420(%rbp), %edx
	andl	%r14d, %eax
	xorl	%eax, %r8d
	xorl	%eax, %r12d
	movl	%esi, %eax
	movl	%edx, %ecx
	xorl	%edx, %eax
	movl	-416(%rbp), %edx
	movl	%r12d, -860(%rbp)
	andl	%r14d, %eax
	movl	%r12d, -424(%rbp)
	xorl	%eax, %ecx
	xorl	%eax, %r13d
	movl	%edx, %ebx
	movl	%r8d, -732(%rbp)
	movl	%ecx, -864(%rbp)
	movl	%ecx, -420(%rbp)
	movl	-544(%rbp), %ecx
	movl	%r8d, -552(%rbp)
	movl	-536(%rbp), %r8d
	movl	%ecx, %eax
	movl	%ecx, %r12d
	movl	%r13d, -736(%rbp)
	xorl	%edx, %eax
	movl	-412(%rbp), %edx
	movl	%r13d, -548(%rbp)
	andl	%r14d, %eax
	xorl	%eax, %r12d
	xorl	%eax, %ebx
	movl	%edi, %eax
	movl	%edx, %r10d
	xorl	%edx, %eax
	movl	-408(%rbp), %edx
	movl	%r12d, -740(%rbp)
	andl	%r14d, %eax
	movl	%r12d, -544(%rbp)
	xorl	%eax, %edi
	xorl	%eax, %r10d
	movl	%r8d, %eax
	movl	%ebx, -868(%rbp)
	xorl	%edx, %eax
	movl	%ebx, -416(%rbp)
	andl	%r14d, %eax
	movl	%edi, -540(%rbp)
	xorl	%eax, %r8d
	xorl	%eax, %edx
	movl	%r10d, -872(%rbp)
	movl	%r10d, -412(%rbp)
	movl	%r8d, -536(%rbp)
	movl	%edx, -876(%rbp)
	movl	%edx, -408(%rbp)
	movl	-532(%rbp), %r9d
	movl	-404(%rbp), %edx
	movl	-528(%rbp), %r10d
	movl	%r9d, %eax
	movl	-524(%rbp), %r11d
	xorl	%edx, %eax
	andl	%r14d, %eax
	xorl	%eax, %edx
	xorl	%eax, %r9d
	movl	%r10d, %eax
	movl	%edx, -880(%rbp)
	movl	%edx, -404(%rbp)
	movl	-400(%rbp), %edx
	movl	%r9d, -532(%rbp)
	xorl	%edx, %eax
	movl	%edx, %ecx
	movl	-396(%rbp), %edx
	andl	%r14d, %eax
	xorl	%eax, %r10d
	xorl	%eax, %ecx
	movl	%r11d, %eax
	movl	%edx, %ebx
	xorl	%edx, %eax
	movl	-392(%rbp), %edx
	movl	%ecx, -884(%rbp)
	andl	%r14d, %eax
	movl	%ecx, -400(%rbp)
	movl	-512(%rbp), %ecx
	xorl	%eax, %ebx
	xorl	%eax, %r11d
	movl	%edx, %esi
	movl	%r10d, -528(%rbp)
	movl	%ebx, -888(%rbp)
	movl	%ebx, -396(%rbp)
	movl	-520(%rbp), %ebx
	movl	%r11d, -524(%rbp)
	movl	%ebx, %eax
	xorl	%edx, %eax
	movl	-516(%rbp), %edx
	andl	%r14d, %eax
	xorl	%eax, %ebx
	xorl	%eax, %esi
	movl	-388(%rbp), %eax
	movl	%edx, %r12d
	movl	%esi, -892(%rbp)
	xorl	%eax, %r12d
	movl	%esi, -392(%rbp)
	movl	%eax, %esi
	andl	%r14d, %r12d
	movl	%ebx, -520(%rbp)
	xorl	%r12d, %edx
	xorl	%r12d, %esi
	movl	%ecx, %r12d
	movl	%edx, -744(%rbp)
	movl	%edx, -516(%rbp)
	movl	-384(%rbp), %edx
	movl	%esi, -896(%rbp)
	xorl	%edx, %r12d
	movl	%esi, -388(%rbp)
	movl	-508(%rbp), %esi
	movl	%r12d, %eax
	movl	%edx, %r12d
	movl	-380(%rbp), %edx
	andl	%r14d, %eax
	xorl	%eax, %ecx
	xorl	%eax, %r12d
	movl	%esi, %eax
	movl	%edx, %r13d
	xorl	%edx, %eax
	movl	%r12d, -772(%rbp)
	andl	%r14d, %eax
	movl	%r12d, -384(%rbp)
	xorl	%eax, %esi
	xorl	%eax, %r13d
	movl	%esi, -648(%rbp)
	movl	-504(%rbp), %esi
	movl	-376(%rbp), %edx
	movl	%r13d, -776(%rbp)
	movl	%esi, %eax
	movl	%r13d, -380(%rbp)
	xorl	%edx, %eax
	andl	%r14d, %eax
	xorl	%eax, %esi
	xorl	%eax, %edx
	movl	%esi, -668(%rbp)
	movl	-500(%rbp), %esi
	movl	%edx, -780(%rbp)
	movl	%edx, -376(%rbp)
	movl	-372(%rbp), %edx
	movl	%esi, %r12d
	movl	%esi, %r13d
	movl	-496(%rbp), %esi
	xorl	%edx, %r12d
	movl	%r12d, %eax
	movl	%esi, %r12d
	andl	%r14d, %eax
	xorl	%eax, %edx
	xorl	%eax, %r13d
	movl	%esi, %eax
	movl	-492(%rbp), %esi
	movl	%edx, -784(%rbp)
	movl	%edx, -372(%rbp)
	movl	-368(%rbp), %edx
	movl	%r13d, -672(%rbp)
	xorl	%edx, %eax
	andl	%r14d, %eax
	xorl	%eax, %edx
	xorl	%eax, %r12d
	movl	%esi, %eax
	movl	%edx, -788(%rbp)
	movl	%edx, -368(%rbp)
	movl	-364(%rbp), %edx
	movl	%r12d, -676(%rbp)
	movl	-484(%rbp), %r12d
	xorl	%edx, %eax
	andl	%r14d, %eax
	xorl	%eax, %esi
	xorl	%eax, %edx
	movl	%esi, -680(%rbp)
	movl	-488(%rbp), %esi
	movl	%edx, -792(%rbp)
	movl	%edx, -364(%rbp)
	movl	-360(%rbp), %edx
	movl	%esi, %r13d
	xorl	%edx, %r13d
	movl	%r13d, %eax
	andl	%r14d, %eax
	xorl	%eax, %esi
	xorl	%eax, %edx
	movl	%r12d, %eax
	movl	%edx, -796(%rbp)
	movl	%edx, -360(%rbp)
	movl	-356(%rbp), %edx
	xorl	%edx, %eax
	andl	%r14d, %eax
	xorl	%eax, %r12d
	xorl	%eax, %edx
	movl	%r12d, -684(%rbp)
	movl	-480(%rbp), %r12d
	movl	%edx, -800(%rbp)
	movl	%edx, -356(%rbp)
	movl	-352(%rbp), %edx
	movl	%r12d, %eax
	movl	%r12d, %r13d
	xorl	%edx, %eax
	movl	%edx, %r12d
	movl	-348(%rbp), %edx
	andl	%r14d, %eax
	xorl	%eax, %r12d
	xorl	%eax, %r13d
	movl	%r12d, -804(%rbp)
	movl	%r12d, -352(%rbp)
	movl	-476(%rbp), %r12d
	movl	%r13d, -688(%rbp)
	movl	%r12d, %eax
	movl	%r12d, %r13d
	movl	-472(%rbp), %r12d
	xorl	%edx, %eax
	andl	%r14d, %eax
	xorl	%eax, %edx
	xorl	%eax, %r13d
	movl	%r12d, %eax
	movl	%edx, -808(%rbp)
	movl	%edx, -348(%rbp)
	movl	-344(%rbp), %edx
	movl	%r13d, -692(%rbp)
	xorl	%edx, %eax
	andl	%r14d, %eax
	xorl	%eax, %r12d
	xorl	%eax, %edx
	movl	%r12d, -696(%rbp)
	movl	-468(%rbp), %r12d
	movl	%edx, -812(%rbp)
	movl	%edx, -344(%rbp)
	movl	-340(%rbp), %edx
	movl	%r12d, %eax
	xorl	%edx, %eax
	andl	%r14d, %eax
	xorl	%eax, %r12d
	xorl	%eax, %edx
	movl	%r12d, -700(%rbp)
	movl	-464(%rbp), %r12d
	movl	%edx, -816(%rbp)
	movl	%edx, -340(%rbp)
	movl	-336(%rbp), %edx
	movl	%r12d, %eax
	movl	%r12d, %r13d
	movl	-460(%rbp), %r12d
	xorl	%edx, %eax
	andl	%r14d, %eax
	xorl	%eax, %edx
	xorl	%eax, %r13d
	movl	%r12d, %eax
	movl	%edx, -820(%rbp)
	movl	%edx, -336(%rbp)
	movl	-332(%rbp), %edx
	movl	%r13d, -704(%rbp)
	xorl	%edx, %eax
	andl	%r14d, %eax
	xorl	%eax, %r12d
	xorl	%eax, %edx
	movl	%r12d, -708(%rbp)
	movl	-456(%rbp), %r12d
	movl	%edx, -824(%rbp)
	movl	%edx, -332(%rbp)
	movl	-328(%rbp), %edx
	movl	%r12d, %eax
	xorl	%edx, %eax
	andl	%r14d, %eax
	xorl	%eax, %r12d
	xorl	%eax, %edx
	movl	%edx, -828(%rbp)
	movl	%edx, -328(%rbp)
	movl	-452(%rbp), %r13d
	movl	-324(%rbp), %eax
	movl	%r13d, %edx
	xorl	%eax, %edx
	andl	%edx, %r14d
	movl	%eax, %edx
	xorl	%r14d, %edx
	xorl	%r14d, %r13d
	movl	-672(%rbp), %r14d
	addl	-720(%rbp), %r14d
	movl	%edx, -832(%rbp)
	movl	%edx, -324(%rbp)
	movl	-768(%rbp), %edx
	movl	%r14d, -308(%rbp)
	movl	-680(%rbp), %r14d
	leal	(%rcx,%rdx), %eax
	addl	-728(%rbp), %r14d
	movl	%eax, -320(%rbp)
	movl	-648(%rbp), %eax
	addl	-712(%rbp), %eax
	movl	%r14d, -300(%rbp)
	movl	%eax, -316(%rbp)
	movl	-668(%rbp), %eax
	addl	-716(%rbp), %eax
	movl	-692(%rbp), %r14d
	movl	%eax, -312(%rbp)
	movl	-676(%rbp), %eax
	addl	-724(%rbp), %eax
	movl	%eax, -304(%rbp)
	movl	-732(%rbp), %eax
	addl	%esi, %eax
	movl	%eax, -296(%rbp)
	movl	-684(%rbp), %eax
	addl	-736(%rbp), %eax
	movl	%eax, -292(%rbp)
	movl	-688(%rbp), %eax
	addl	-740(%rbp), %eax
	movl	%eax, -288(%rbp)
	leal	(%r14,%rdi), %eax
	addl	$536870910, %edi
	movl	-696(%rbp), %r14d
	movl	%eax, -284(%rbp)
	leal	(%r14,%r8), %eax
	addl	$536870910, %r8d
	movl	%eax, -280(%rbp)
	movl	-700(%rbp), %r14d
	subl	-692(%rbp), %edi
	subl	-696(%rbp), %r8d
	leal	(%r14,%r9), %eax
	movl	-704(%rbp), %r14d
	addl	$536870910, %r9d
	subl	-700(%rbp), %r9d
	movl	%eax, -276(%rbp)
	leal	(%r14,%r10), %eax
	movl	-708(%rbp), %r14d
	movl	%eax, -272(%rbp)
	leal	(%r14,%r11), %eax
	leal	536870910(%rdx), %r14d
	movl	-716(%rbp), %edx
	movl	%eax, -268(%rbp)
	leal	(%r12,%rbx), %eax
	subl	%ecx, %r14d
	movl	-732(%rbp), %ecx
	movl	%eax, -264(%rbp)
	movl	-744(%rbp), %eax
	addl	$536870910, %ecx
	movl	%r14d, -768(%rbp)
	addl	%r13d, %eax
	subl	%esi, %ecx
	movl	-736(%rbp), %esi
	movl	%eax, -260(%rbp)
	movl	-712(%rbp), %eax
	addl	$536870910, %esi
	subl	-684(%rbp), %esi
	addl	$536870910, %eax
	subl	-648(%rbp), %eax
	movl	%eax, -712(%rbp)
	leal	536870910(%rdx), %eax
	movl	-720(%rbp), %edx
	subl	-668(%rbp), %eax
	movl	%eax, -668(%rbp)
	leal	536870910(%rdx), %eax
	movl	%eax, %edx
	subl	-672(%rbp), %edx
	movl	%edx, -672(%rbp)
	movl	-724(%rbp), %edx
	leal	536870910(%rdx), %eax
	movl	-728(%rbp), %edx
	subl	-676(%rbp), %eax
	addl	$536870910, %edx
	subl	-680(%rbp), %edx
	addl	$536870910, %r10d
	addl	$536870910, %ebx
	subl	%r12d, %ebx
	movl	-744(%rbp), %r12d
	subl	-704(%rbp), %r10d
	addl	$536870910, %r11d
	subl	-708(%rbp), %r11d
	addl	$536870910, %r12d
	subl	%r13d, %r12d
	movl	%r12d, %r13d
	andl	$268435455, %r12d
	shrl	$28, %r13d
	movl	%r13d, -648(%rbp)
	movl	%r13d, %r14d
	subl	-688(%rbp), %r14d
	movl	%r14d, %r13d
	movl	-740(%rbp), %r14d
	leal	536870908(%r13,%r14), %r13d
	movl	%ebx, %r14d
	andl	$268435455, %ebx
	shrl	$28, %r14d
	addl	%r14d, %r12d
	movl	%r11d, %r14d
	andl	$268435455, %r11d
	shrl	$28, %r14d
	movl	%r12d, -196(%rbp)
	addl	%r14d, %ebx
	movl	%r10d, %r14d
	andl	$268435455, %r10d
	shrl	$28, %r14d
	movl	%ebx, -200(%rbp)
	movl	-712(%rbp), %ebx
	addl	%r14d, %r11d
	movl	%r9d, %r14d
	andl	$268435455, %r9d
	shrl	$28, %r14d
	movl	%r11d, -204(%rbp)
	movl	-848(%rbp), %r11d
	addl	%r14d, %r10d
	movl	%r8d, %r14d
	andl	$268435455, %r8d
	shrl	$28, %r14d
	movl	%r10d, -208(%rbp)
	addl	%r14d, %r9d
	movl	-648(%rbp), %r14d
	movl	%r9d, -212(%rbp)
	movl	%edi, %r9d
	andl	$268435455, %edi
	shrl	$28, %r9d
	addl	%r9d, %r8d
	movl	%r13d, %r9d
	andl	$268435455, %r13d
	shrl	$28, %r9d
	movl	%r8d, -216(%rbp)
	addl	%r9d, %edi
	movl	%esi, %r9d
	shrl	$28, %r9d
	andl	$268435455, %esi
	movl	%edi, -220(%rbp)
	movl	-668(%rbp), %edi
	addl	%r9d, %r13d
	movl	%ecx, %r9d
	andl	$268435455, %ecx
	shrl	$28, %r9d
	movl	%r13d, -224(%rbp)
	addl	%r9d, %esi
	movl	%edx, %r9d
	andl	$268435455, %edx
	shrl	$28, %r9d
	movl	%esi, -228(%rbp)
	movl	-672(%rbp), %esi
	addl	%r9d, %ecx
	movl	%eax, %r9d
	andl	$268435455, %eax
	shrl	$28, %r9d
	movl	%ecx, -232(%rbp)
	movl	-768(%rbp), %ecx
	addl	%r9d, %edx
	movl	%edx, -236(%rbp)
	movl	%esi, %edx
	andl	$268435455, %esi
	movl	%ecx, %r9d
	shrl	$28, %edx
	shrl	$28, %r9d
	addl	%edx, %eax
	movl	%edi, %edx
	andl	$268435455, %edi
	shrl	$28, %edx
	movl	%eax, -240(%rbp)
	movl	%esi, %eax
	movl	-840(%rbp), %esi
	addl	%edx, %eax
	movl	%ebx, %edx
	shrl	$28, %edx
	movl	%eax, -244(%rbp)
	movl	%edi, %eax
	movl	-836(%rbp), %edi
	addl	%edx, %eax
	movl	-844(%rbp), %edx
	movl	%eax, -248(%rbp)
	movl	%ebx, %eax
	andl	$268435455, %eax
	addl	%r9d, %eax
	movl	%ecx, %r9d
	andl	$268435455, %r9d
	movl	%eax, -252(%rbp)
	leal	536870910(%rsi), %eax
	subl	-776(%rbp), %eax
	addl	%r9d, %r14d
	movl	%eax, -668(%rbp)
	leal	536870910(%rdx), %eax
	movl	%r14d, -256(%rbp)
	leal	536870910(%rdi), %r14d
	movl	%eax, %edi
	leal	536870910(%r11), %eax
	subl	-780(%rbp), %edi
	subl	-772(%rbp), %r14d
	movl	%eax, %esi
	movl	%edi, -672(%rbp)
	subl	-784(%rbp), %esi
	movl	-856(%rbp), %r13d
	movl	-852(%rbp), %r8d
	movl	%esi, -676(%rbp)
	movl	-860(%rbp), %r12d
	movl	-864(%rbp), %r9d
	leal	536870910(%r13), %edx
	movl	-892(%rbp), %r13d
	subl	-792(%rbp), %edx
	leal	536870910(%r8), %eax
	leal	536870910(%r12), %ecx
	subl	-788(%rbp), %eax
	subl	-796(%rbp), %ecx
	movl	%edx, -680(%rbp)
	leal	536870910(%r13), %ebx
	movl	-896(%rbp), %r13d
	leal	536870910(%r9), %esi
	subl	-800(%rbp), %esi
	movl	-872(%rbp), %r10d
	movl	-884(%rbp), %r11d
	leal	536870910(%r13), %r12d
	subl	-832(%rbp), %r12d
	movl	-868(%rbp), %edx
	movl	%r12d, %r13d
	subl	-828(%rbp), %ebx
	leal	536870910(%r10), %edi
	andl	$268435455, %r12d
	shrl	$28, %r13d
	leal	536870910(%r11), %r10d
	movl	-888(%rbp), %r11d
	movl	-876(%rbp), %r9d
	movl	%r13d, -648(%rbp)
	subl	-804(%rbp), %r13d
	leal	536870908(%r13,%rdx), %r13d
	movl	%ebx, %edx
	addl	$536870910, %r11d
	subl	-824(%rbp), %r11d
	shrl	$28, %edx
	andl	$268435455, %ebx
	subl	-820(%rbp), %r10d
	subl	-808(%rbp), %edi
	addl	%edx, %r12d
	leal	536870910(%r9), %r8d
	movl	%esi, %edx
	movl	-880(%rbp), %r9d
	movl	%r12d, -452(%rbp)
	movl	%r11d, %r12d
	andl	$268435455, %r11d
	subl	-812(%rbp), %r8d
	shrl	$28, %r12d
	addl	$536870910, %r9d
	subl	-816(%rbp), %r9d
	addl	%r12d, %ebx
	movl	%ebx, -456(%rbp)
	movl	%r10d, %ebx
	andl	$268435455, %r10d
	shrl	$28, %ebx
	addl	%ebx, %r11d
	movl	%r11d, -460(%rbp)
	movl	%r9d, %r11d
	andl	$268435455, %r9d
	shrl	$28, %r11d
	addl	%r11d, %r10d
	movl	%eax, %r11d
	movl	%r10d, -464(%rbp)
	movl	%r8d, %r10d
	andl	$268435455, %r8d
	shrl	$28, %r10d
	addl	%r10d, %r9d
	movl	%r9d, -468(%rbp)
	movl	%edi, %r9d
	andl	$268435455, %edi
	movl	-668(%rbp), %ebx
	shrl	$28, %r9d
	addl	%r9d, %r8d
	movl	%r8d, -472(%rbp)
	movl	%r13d, %r8d
	shrl	$28, %r8d
	addl	%r8d, %edi
	shrl	$28, %edx
	andl	$268435455, %r13d
	andl	$268435455, %esi
	addl	%edx, %r13d
	movl	%ecx, %edx
	shrl	$28, %r11d
	andl	$268435455, %eax
	movl	%edi, -476(%rbp)
	movl	-680(%rbp), %edi
	shrl	$28, %edx
	andl	$268435455, %ecx
	addl	%edx, %esi
	movl	%r13d, -480(%rbp)
	movl	-648(%rbp), %r13d
	movl	%edi, %edx
	andl	$268435455, %edi
	movl	%esi, -484(%rbp)
	movl	-676(%rbp), %esi
	shrl	$28, %edx
	addl	%edx, %ecx
	movl	%edi, %edx
	movl	-672(%rbp), %edi
	addl	%r11d, %edx
	movl	%ecx, -488(%rbp)
	movl	%edx, -492(%rbp)
	movl	%esi, %edx
	andl	$268435455, %esi
	shrl	$28, %edx
	addl	%edx, %eax
	movl	%edi, %edx
	andl	$268435455, %edi
	shrl	$28, %edx
	movl	%eax, -496(%rbp)
	movl	%esi, %eax
	movq	-656(%rbp), %rsi
	addl	%edx, %eax
	movl	%ebx, %edx
	shrl	$28, %edx
	movl	%eax, -500(%rbp)
	movl	%edi, %eax
	movq	-752(%rbp), %rdi
	addl	%edx, %eax
	movl	%r14d, %edx
	movl	%eax, -504(%rbp)
	movl	%ebx, %eax
	shrl	$28, %edx
	andl	$268435455, %eax
	addl	%edx, %eax
	movq	%r15, %rdx
	movl	%eax, -508(%rbp)
	movl	%r14d, %eax
	andl	$268435455, %eax
	leal	(%rax,%r13), %r14d
	movl	%r14d, -512(%rbp)
	call	gf_mul@PLT
	movdqa	-432(%rbp), %xmm2
	movq	%r15, %rdx
	paddd	-368(%rbp), %xmm2
	movdqa	-416(%rbp), %xmm1
	movdqa	-400(%rbp), %xmm0
	paddd	-352(%rbp), %xmm1
	paddd	-336(%rbp), %xmm0
	movaps	%xmm2, -496(%rbp)
	movdqa	-448(%rbp), %xmm3
	movq	-664(%rbp), %rsi
	paddd	-384(%rbp), %xmm3
	movq	-760(%rbp), %rdi
	movaps	%xmm1, -480(%rbp)
	movaps	%xmm0, -464(%rbp)
	movaps	%xmm3, -512(%rbp)
	call	gf_mul@PLT
	movl	-556(%rbp), %r11d
	movl	-576(%rbp), %eax
	movl	-568(%rbp), %edx
	leal	536870910(%r11), %ecx
	movl	-552(%rbp), %r11d
	leal	536870910(%rax), %r14d
	movl	-572(%rbp), %eax
	subl	-448(%rbp), %r14d
	leal	536870910(%r11), %esi
	movl	-548(%rbp), %r11d
	addl	$536870910, %eax
	subl	-444(%rbp), %eax
	movl	%eax, -668(%rbp)
	leal	536870910(%rdx), %eax
	movl	-564(%rbp), %edx
	leal	536870910(%r11), %edi
	movl	-540(%rbp), %r11d
	movl	%eax, %r9d
	subl	-440(%rbp), %r9d
	leal	536870910(%rdx), %eax
	movl	%r9d, -672(%rbp)
	movl	-560(%rbp), %edx
	leal	536870910(%r11), %r8d
	movl	-536(%rbp), %r11d
	movl	%eax, %r10d
	subl	-436(%rbp), %r10d
	movl	%r10d, -676(%rbp)
	subl	-428(%rbp), %ecx
	addl	$536870910, %edx
	leal	536870910(%r11), %r9d
	movl	-532(%rbp), %r11d
	subl	-432(%rbp), %edx
	subl	-424(%rbp), %esi
	subl	-420(%rbp), %edi
	leal	536870910(%r11), %r10d
	movl	-528(%rbp), %r11d
	subl	-412(%rbp), %r8d
	subl	-404(%rbp), %r10d
	subl	-408(%rbp), %r9d
	addl	$536870910, %r11d
	subl	-400(%rbp), %r11d
	movl	%r11d, -680(%rbp)
	movl	-524(%rbp), %r11d
	leal	536870910(%r11), %ebx
	movl	-520(%rbp), %r11d
	subl	-396(%rbp), %ebx
	leal	536870910(%r11), %r12d
	subl	-392(%rbp), %r12d
	movl	-516(%rbp), %r11d
	leal	536870910(%r11), %r13d
	subl	-388(%rbp), %r13d
	movl	-544(%rbp), %r11d
	movl	%r13d, %eax
	andl	$268435455, %r13d
	shrl	$28, %eax
	movl	%eax, -648(%rbp)
	leal	536870908(%r11), %eax
	movl	%r12d, %r11d
	andl	$268435455, %r12d
	shrl	$28, %r11d
	subl	-416(%rbp), %eax
	addl	-648(%rbp), %eax
	addl	%r11d, %r13d
	movl	%r13d, -324(%rbp)
	movl	%ebx, %r13d
	andl	$268435455, %ebx
	shrl	$28, %r13d
	addl	%r13d, %r12d
	movl	-680(%rbp), %r13d
	movl	%r12d, -328(%rbp)
	movl	%r13d, %r11d
	shrl	$28, %r11d
	addl	%r11d, %ebx
	movl	%r13d, %r11d
	movl	%r10d, %r13d
	andl	$268435455, %r10d
	shrl	$28, %r13d
	andl	$268435455, %r11d
	movl	%ebx, -332(%rbp)
	addl	%r13d, %r11d
	movq	-904(%rbp), %r13
	movl	%r11d, -336(%rbp)
	movl	%r9d, %r11d
	shrl	$28, %r11d
	addl	%r11d, %r10d
	movl	%r8d, %r11d
	andl	$268435455, %r9d
	andl	$268435455, %r8d
	shrl	$28, %r11d
	movl	%r10d, -340(%rbp)
	movl	-676(%rbp), %r10d
	addl	%r11d, %r9d
	movl	%eax, %r11d
	andl	$268435455, %eax
	shrl	$28, %r11d
	movl	%r9d, -344(%rbp)
	movl	-672(%rbp), %r9d
	addl	%r11d, %r8d
	movl	%esi, %r11d
	andl	$268435455, %esi
	movl	%r8d, -348(%rbp)
	movl	%edi, %r8d
	shrl	$28, %r11d
	andl	$268435455, %edi
	shrl	$28, %r8d
	addl	%r11d, %edi
	movl	%ecx, %r11d
	andl	$268435455, %ecx
	addl	%r8d, %eax
	shrl	$28, %r11d
	movl	-668(%rbp), %r8d
	movl	%edi, -356(%rbp)
	movl	%eax, -352(%rbp)
	movl	%r10d, %eax
	addl	%r11d, %esi
	movl	%edx, %r11d
	shrl	$28, %eax
	andl	$268435455, %edx
	shrl	$28, %r11d
	movq	%r15, %rdi
	addl	%eax, %edx
	andl	$268435455, %r10d
	addl	%r11d, %ecx
	movl	%esi, -360(%rbp)
	movl	%edx, -368(%rbp)
	movl	%r9d, %edx
	movl	%r10d, %eax
	movq	%r13, %rsi
	shrl	$28, %edx
	andl	$268435455, %r9d
	movl	%ecx, -364(%rbp)
	addl	%edx, %eax
	movl	%r8d, %edx
	shrl	$28, %edx
	movl	%eax, -372(%rbp)
	movl	%r9d, %eax
	addl	%edx, %eax
	movl	%r14d, %edx
	movl	%eax, -376(%rbp)
	movl	%r8d, %eax
	shrl	$28, %edx
	andl	$268435455, %eax
	addl	%edx, %eax
	movl	-648(%rbp), %edx
	movl	%eax, -380(%rbp)
	movl	%r14d, %eax
	andl	$268435455, %eax
	leal	(%rax,%rdx), %r14d
	movl	%r14d, -384(%rbp)
	call	gf_sqr@PLT
	movq	-912(%rbp), %rsi
	movq	%r15, %rdx
	movq	%r13, %rdi
	call	gf_mul@PLT
	movdqa	-432(%rbp), %xmm2
	movq	%r15, %rsi
	paddd	-560(%rbp), %xmm2
	movdqa	-544(%rbp), %xmm1
	movdqa	-528(%rbp), %xmm0
	paddd	-416(%rbp), %xmm1
	paddd	-400(%rbp), %xmm0
	movaps	%xmm2, -496(%rbp)
	movdqa	-448(%rbp), %xmm3
	movq	-760(%rbp), %rdi
	paddd	-576(%rbp), %xmm3
	movaps	%xmm1, -480(%rbp)
	movaps	%xmm0, -464(%rbp)
	movaps	%xmm3, -512(%rbp)
	call	gf_sqr@PLT
	movq	-656(%rbp), %rbx
	movq	%r15, %rdi
	movq	%rbx, %rsi
	call	gf_sqr@PLT
	movq	-664(%rbp), %rsi
	movq	%rbx, %rdi
	call	gf_sqr@PLT
	movq	-752(%rbp), %rdi
	movq	%rbx, %rdx
	movq	%r15, %rsi
	movq	%rbx, -656(%rbp)
	call	gf_mul@PLT
	movl	-496(%rbp), %r10d
	movl	-512(%rbp), %eax
	movl	-504(%rbp), %r8d
	leal	536870910(%r10), %edx
	movl	-492(%rbp), %r10d
	leal	536870910(%rax), %r14d
	movl	-508(%rbp), %eax
	subl	-320(%rbp), %r14d
	subl	-304(%rbp), %edx
	leal	536870910(%r10), %ecx
	movl	-488(%rbp), %r10d
	addl	$536870910, %eax
	subl	-316(%rbp), %eax
	movl	%eax, -668(%rbp)
	leal	536870910(%r8), %eax
	movl	-500(%rbp), %r8d
	leal	536870910(%r10), %esi
	movl	%eax, %r11d
	movl	-484(%rbp), %r10d
	subl	-312(%rbp), %r11d
	leal	536870910(%r8), %eax
	movl	%r11d, -672(%rbp)
	subl	-300(%rbp), %ecx
	leal	536870910(%r10), %edi
	movl	%eax, %r8d
	movl	-476(%rbp), %r10d
	subl	-308(%rbp), %r8d
	movl	%r8d, -676(%rbp)
	subl	-296(%rbp), %esi
	leal	536870910(%r10), %r8d
	movl	-472(%rbp), %r10d
	subl	-292(%rbp), %edi
	subl	-284(%rbp), %r8d
	leal	536870910(%r10), %r9d
	subl	-280(%rbp), %r9d
	movl	-468(%rbp), %r10d
	movl	%r9d, -680(%rbp)
	movl	-464(%rbp), %r9d
	addl	$536870910, %r10d
	subl	-276(%rbp), %r10d
	leal	536870910(%r9), %r11d
	movl	-460(%rbp), %r9d
	subl	-272(%rbp), %r11d
	leal	536870910(%r9), %ebx
	movl	-456(%rbp), %r9d
	subl	-268(%rbp), %ebx
	leal	536870910(%r9), %r12d
	subl	-264(%rbp), %r12d
	movl	-452(%rbp), %r9d
	leal	536870910(%r9), %r13d
	subl	-260(%rbp), %r13d
	movl	-480(%rbp), %r9d
	movl	%r13d, %eax
	andl	$268435455, %r13d
	shrl	$28, %eax
	movl	%eax, -648(%rbp)
	leal	536870908(%r9), %eax
	movl	%r12d, %r9d
	andl	$268435455, %r12d
	shrl	$28, %r9d
	subl	-288(%rbp), %eax
	addl	-648(%rbp), %eax
	addl	%r9d, %r13d
	movl	%r10d, %r9d
	movl	%r13d, -196(%rbp)
	movl	%ebx, %r13d
	andl	$268435455, %ebx
	shrl	$28, %r13d
	addl	%r13d, %r12d
	movl	%r12d, -200(%rbp)
	movl	%r11d, %r12d
	shrl	$28, %r12d
	shrl	$28, %r9d
	andl	$268435455, %r11d
	andl	$268435455, %r10d
	addl	%r12d, %ebx
	addl	%r9d, %r11d
	movl	%ebx, -204(%rbp)
	movl	-680(%rbp), %ebx
	movl	%r11d, -208(%rbp)
	movl	-672(%rbp), %r11d
	movl	%ebx, %r9d
	andl	$268435455, %ebx
	shrl	$28, %r9d
	addl	%r9d, %r10d
	movl	%ebx, %r9d
	movq	-656(%rbp), %rbx
	movl	%r10d, -212(%rbp)
	movl	%r8d, %r10d
	andl	$268435455, %r8d
	shrl	$28, %r10d
	addl	%r10d, %r9d
	movl	%edi, %r10d
	andl	$268435455, %edi
	movl	%r9d, -216(%rbp)
	movl	%eax, %r9d
	shrl	$28, %r10d
	andl	$268435455, %eax
	shrl	$28, %r9d
	addl	%r10d, %eax
	movl	%esi, %r10d
	andl	$268435455, %esi
	addl	%r9d, %r8d
	shrl	$28, %r10d
	movl	%eax, -224(%rbp)
	movl	%r8d, -220(%rbp)
	movl	-676(%rbp), %r8d
	addl	%r10d, %edi
	movl	%ecx, %r10d
	shrl	$28, %r10d
	movl	%edi, -228(%rbp)
	movl	-668(%rbp), %edi
	andl	$268435455, %ecx
	movl	%r8d, %eax
	addl	%r10d, %esi
	movl	%edx, %r10d
	andl	$268435455, %edx
	shrl	$28, %eax
	shrl	$28, %r10d
	andl	$268435455, %r8d
	movl	%esi, -232(%rbp)
	addl	%eax, %edx
	movl	%r8d, %eax
	addl	%r10d, %ecx
	movl	%edx, -240(%rbp)
	movl	%r11d, %edx
	shrl	$28, %edx
	movl	%ecx, -236(%rbp)
	addl	%edx, %eax
	andl	$268435455, %r11d
	movl	%edi, %edx
	movl	%eax, -244(%rbp)
	shrl	$28, %edx
	movl	%r11d, %eax
	addl	%edx, %eax
	movl	%r14d, %edx
	movl	%eax, -248(%rbp)
	movl	%edi, %eax
	shrl	$28, %edx
	movl	-648(%rbp), %edi
	andl	$268435455, %eax
	addl	%edx, %eax
	movl	$39081, %edx
	movl	%eax, -252(%rbp)
	movl	%r14d, %eax
	andl	$268435455, %eax
	leal	(%rax,%rdi), %r14d
	movq	%rbx, %rdi
	movl	%r14d, -256(%rbp)
	movq	-664(%rbp), %r14
	movq	%r14, %rsi
	call	gf_mulw_unsigned@PLT
	movdqa	-304(%rbp), %xmm2
	movq	%rbx, %rdx
	movdqa	-288(%rbp), %xmm1
	movdqa	-272(%rbp), %xmm0
	movdqa	-320(%rbp), %xmm3
	movq	%r14, %rsi
	movq	%r15, %rdi
	paddd	-496(%rbp), %xmm2
	paddd	-480(%rbp), %xmm1
	paddd	-464(%rbp), %xmm0
	paddd	-512(%rbp), %xmm3
	movaps	%xmm2, -304(%rbp)
	movaps	%xmm3, -320(%rbp)
	movaps	%xmm1, -288(%rbp)
	movaps	%xmm0, -272(%rbp)
	call	gf_mul@PLT
	subl	$1, -644(%rbp)
	movl	-644(%rbp), %ecx
	cmpl	$-1, %ecx
	jne	.L89
	movd	-764(%rbp), %xmm4
	movdqa	-448(%rbp), %xmm1
	leaq	-128(%rbp), %r13
	leaq	-192(%rbp), %r12
	pxor	-576(%rbp), %xmm1
	movdqa	-576(%rbp), %xmm2
	movq	%r12, %rdi
	pshufd	$0, %xmm4, %xmm0
	pand	%xmm0, %xmm1
	pxor	%xmm1, %xmm2
	pxor	-448(%rbp), %xmm1
	movaps	%xmm2, -576(%rbp)
	movdqa	-560(%rbp), %xmm2
	movaps	%xmm1, -448(%rbp)
	movdqa	-432(%rbp), %xmm1
	pxor	-560(%rbp), %xmm1
	pand	%xmm0, %xmm1
	pxor	%xmm1, %xmm2
	pxor	-432(%rbp), %xmm1
	movaps	%xmm2, -560(%rbp)
	movdqa	-544(%rbp), %xmm2
	movaps	%xmm1, -432(%rbp)
	movdqa	-416(%rbp), %xmm1
	pxor	-544(%rbp), %xmm1
	pand	%xmm0, %xmm1
	pxor	%xmm1, %xmm2
	pxor	-416(%rbp), %xmm1
	movaps	%xmm2, -544(%rbp)
	movdqa	-528(%rbp), %xmm2
	movaps	%xmm1, -416(%rbp)
	movdqa	-400(%rbp), %xmm1
	pxor	-528(%rbp), %xmm1
	pand	%xmm0, %xmm1
	pxor	%xmm1, %xmm2
	pxor	-400(%rbp), %xmm1
	movaps	%xmm2, -528(%rbp)
	movdqa	-512(%rbp), %xmm2
	movaps	%xmm1, -400(%rbp)
	movdqa	-512(%rbp), %xmm1
	pxor	-384(%rbp), %xmm1
	pand	%xmm0, %xmm1
	pxor	%xmm1, %xmm2
	pxor	-384(%rbp), %xmm1
	movaps	%xmm2, -512(%rbp)
	movaps	%xmm1, -384(%rbp)
	movdqa	-368(%rbp), %xmm1
	pxor	-496(%rbp), %xmm1
	movdqa	-496(%rbp), %xmm2
	movq	-936(%rbp), %r14
	pand	%xmm0, %xmm1
	pxor	%xmm1, %xmm2
	pxor	-368(%rbp), %xmm1
	movq	%r14, %rsi
	movaps	%xmm2, -496(%rbp)
	movdqa	-480(%rbp), %xmm2
	movaps	%xmm1, -368(%rbp)
	movdqa	-352(%rbp), %xmm1
	pxor	-480(%rbp), %xmm1
	pand	%xmm0, %xmm1
	pxor	%xmm1, %xmm2
	pxor	-352(%rbp), %xmm1
	movaps	%xmm2, -480(%rbp)
	movaps	%xmm1, -352(%rbp)
	movdqa	-336(%rbp), %xmm1
	pxor	-464(%rbp), %xmm1
	pand	%xmm1, %xmm0
	movdqa	-464(%rbp), %xmm1
	pxor	%xmm0, %xmm1
	pxor	-336(%rbp), %xmm0
	movaps	%xmm1, -464(%rbp)
	movaps	%xmm0, -336(%rbp)
	call	gf_sqr@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	gf_isr@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	gf_sqr@PLT
	movq	%r13, %rdi
	movq	%r14, %rdx
	movq	%r12, %rsi
	call	gf_mul@PLT
	movq	-912(%rbp), %r15
	movdqa	-128(%rbp), %xmm5
	movq	%r14, %rdx
	movq	-752(%rbp), %r13
	movdqa	-112(%rbp), %xmm6
	movdqa	-96(%rbp), %xmm7
	movdqa	-80(%rbp), %xmm4
	movq	%r15, %rdi
	movaps	%xmm5, -512(%rbp)
	movq	%r13, %rsi
	movaps	%xmm6, -496(%rbp)
	movaps	%xmm7, -480(%rbp)
	movaps	%xmm4, -464(%rbp)
	call	gf_mul@PLT
	movq	-928(%rbp), %rdi
	movl	$1, %edx
	movq	%r15, %rsi
	call	gf_serialize@PLT
	leaq	ZERO(%rip), %rsi
	movq	%r15, %rdi
	call	gf_eq@PLT
	movl	$64, %esi
	movq	%r15, %rdi
	movl	%eax, %ebx
	call	OPENSSL_cleanse@PLT
	movl	$64, %esi
	movq	%r13, %rdi
	call	OPENSSL_cleanse@PLT
	movl	$64, %esi
	movq	%r14, %rdi
	call	OPENSSL_cleanse@PLT
	movq	-760(%rbp), %rdi
	movl	$64, %esi
	call	OPENSSL_cleanse@PLT
	movq	-904(%rbp), %rdi
	movl	$64, %esi
	call	OPENSSL_cleanse@PLT
	movq	-656(%rbp), %rdi
	movl	$64, %esi
	call	OPENSSL_cleanse@PLT
	movq	-664(%rbp), %rdi
	movl	$64, %esi
	call	OPENSSL_cleanse@PLT
	movl	%ebx, %eax
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	notl	%eax
	jne	.L92
	addq	$904, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L91:
	.cfi_restore_state
	andl	$252, %edx
	movl	%ebx, %ecx
	jmp	.L85
.L92:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE222:
	.size	x448_int, .-x448_int
	.p2align 4
	.globl	curve448_point_mul_by_ratio_and_encode_like_x448
	.type	curve448_point_mul_by_ratio_and_encode_like_x448, @function
curve448_point_mul_by_ratio_and_encode_like_x448:
.LFB223:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-368(%rbp), %r15
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-304(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-432(%rbp), %r12
	movq	%r12, %rdi
	subq	$400, %rsp
	movdqa	(%rsi), %xmm0
	movdqa	16(%rsi), %xmm1
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movdqa	32(%rsi), %xmm2
	movdqa	48(%rsi), %xmm3
	movdqa	80(%rsi), %xmm5
	movdqa	96(%rsi), %xmm6
	movaps	%xmm0, -304(%rbp)
	movdqa	112(%rsi), %xmm7
	movdqa	128(%rsi), %xmm0
	movaps	%xmm1, -288(%rbp)
	movdqa	64(%rsi), %xmm4
	movdqa	144(%rsi), %xmm1
	movaps	%xmm2, -272(%rbp)
	movaps	%xmm3, -256(%rbp)
	movdqa	160(%rsi), %xmm2
	movdqa	176(%rsi), %xmm3
	movaps	%xmm0, -176(%rbp)
	movaps	%xmm1, -160(%rbp)
	movaps	%xmm2, -144(%rbp)
	movaps	%xmm3, -128(%rbp)
	movaps	%xmm5, -224(%rbp)
	movaps	%xmm6, -208(%rbp)
	movaps	%xmm7, -192(%rbp)
	movaps	%xmm4, -240(%rbp)
	movdqa	192(%rsi), %xmm4
	movdqa	208(%rsi), %xmm5
	movdqa	224(%rsi), %xmm6
	movdqa	240(%rsi), %xmm7
	movq	%r13, %rsi
	movaps	%xmm4, -112(%rbp)
	movaps	%xmm5, -96(%rbp)
	movaps	%xmm6, -80(%rbp)
	movaps	%xmm7, -64(%rbp)
	call	gf_sqr@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	gf_isr@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	gf_sqr@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	%r13, %rdx
	call	gf_mul@PLT
	leaq	-240(%rbp), %r12
	movdqa	-368(%rbp), %xmm0
	movdqa	-352(%rbp), %xmm1
	movdqa	-336(%rbp), %xmm2
	movdqa	-320(%rbp), %xmm3
	movq	%r12, %rdx
	leaq	-112(%rbp), %rsi
	leaq	-176(%rbp), %r15
	movaps	%xmm0, -112(%rbp)
	movq	%r15, %rdi
	movaps	%xmm1, -96(%rbp)
	movaps	%xmm2, -80(%rbp)
	movaps	%xmm3, -64(%rbp)
	call	gf_mul@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	gf_sqr@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	gf_serialize@PLT
	movl	$256, %esi
	movq	%r13, %rdi
	call	OPENSSL_cleanse@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L96
	addq	$400, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L96:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE223:
	.size	curve448_point_mul_by_ratio_and_encode_like_x448, .-curve448_point_mul_by_ratio_and_encode_like_x448
	.p2align 4
	.globl	x448_derive_public_key
	.type	x448_derive_public_key, @function
x448_derive_public_key:
.LFB224:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$56, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	leaq	-688(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-368(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-816(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	.cfi_offset 3, -56
	leaq	-624(%rbp), %rbx
	subq	$776, %rsp
	movdqu	(%rsi), %xmm0
	movdqu	16(%rsi), %xmm1
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	48(%rsi), %rax
	movdqu	32(%rsi), %xmm2
	leaq	-112(%rbp), %rsi
	movaps	%xmm0, -112(%rbp)
	movq	%rax, -64(%rbp)
	shrq	$56, %rax
	orl	$-128, %eax
	movaps	%xmm1, -96(%rbp)
	movb	%al, -57(%rbp)
	movaps	%xmm2, -80(%rbp)
	andb	$-4, -112(%rbp)
	call	curve448_scalar_decode_long@PLT
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	curve448_scalar_halve@PLT
	movq	curve448_precomputed_base(%rip), %rsi
	movq	%r12, %rdx
	movq	%rbx, %rdi
	leaq	-752(%rbp), %r12
	call	curve448_precomputed_scalarmul
	movdqa	-624(%rbp), %xmm3
	movq	%r12, %rdi
	movdqa	-608(%rbp), %xmm4
	movdqa	-592(%rbp), %xmm5
	movdqa	-576(%rbp), %xmm6
	movq	%r13, %rsi
	movdqa	-560(%rbp), %xmm7
	movdqa	-544(%rbp), %xmm0
	movaps	%xmm3, -368(%rbp)
	movdqa	-528(%rbp), %xmm1
	movdqa	-512(%rbp), %xmm2
	movaps	%xmm4, -352(%rbp)
	movdqa	-496(%rbp), %xmm3
	movdqa	-480(%rbp), %xmm4
	movaps	%xmm5, -336(%rbp)
	movaps	%xmm6, -320(%rbp)
	movdqa	-464(%rbp), %xmm5
	movdqa	-448(%rbp), %xmm6
	movaps	%xmm7, -304(%rbp)
	movdqa	-432(%rbp), %xmm7
	movaps	%xmm0, -288(%rbp)
	movdqa	-416(%rbp), %xmm0
	movaps	%xmm1, -272(%rbp)
	movdqa	-400(%rbp), %xmm1
	movaps	%xmm2, -256(%rbp)
	movdqa	-384(%rbp), %xmm2
	movaps	%xmm7, -176(%rbp)
	movaps	%xmm0, -160(%rbp)
	movaps	%xmm1, -144(%rbp)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm3, -240(%rbp)
	movaps	%xmm4, -224(%rbp)
	movaps	%xmm5, -208(%rbp)
	movaps	%xmm6, -192(%rbp)
	call	gf_sqr@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	gf_isr@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	gf_sqr@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	%r13, %rdx
	call	gf_mul@PLT
	leaq	-304(%rbp), %r12
	movdqa	-688(%rbp), %xmm3
	movdqa	-672(%rbp), %xmm4
	movdqa	-656(%rbp), %xmm5
	movdqa	-640(%rbp), %xmm6
	movq	%r12, %rdx
	leaq	-240(%rbp), %r15
	leaq	-176(%rbp), %rsi
	movq	%r15, %rdi
	movaps	%xmm3, -176(%rbp)
	movaps	%xmm4, -160(%rbp)
	movaps	%xmm5, -144(%rbp)
	movaps	%xmm6, -128(%rbp)
	call	gf_mul@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	gf_sqr@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	gf_serialize@PLT
	movl	$256, %esi
	movq	%r13, %rdi
	call	OPENSSL_cleanse@PLT
	movl	$256, %esi
	movq	%rbx, %rdi
	call	OPENSSL_cleanse@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L100
	addq	$776, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L100:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE224:
	.size	x448_derive_public_key, .-x448_derive_public_key
	.p2align 4
	.globl	curve448_base_double_scalarmul_non_secret
	.type	curve448_base_double_scalarmul_non_secret, @function
curve448_base_double_scalarmul_non_secret:
.LFB227:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$312, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-3648(%rbp), %rax
	movq	%rdx, %r12
	movq	%rdi, %r15
	movl	$5, %edx
	movq	%rax, %rdi
	movq	%rcx, %r13
	movq	%rax, -4440(%rbp)
	call	recode_wnaf
	movq	%r13, %rsi
	leaq	-3024(%rbp), %rax
	movl	$3, %edx
	movq	%rax, %rdi
	leaq	64(%r12), %r13
	movq	%rax, -4448(%rbp)
	leaq	-1920(%rbp), %rbx
	call	recode_wnaf
	leaq	-2112(%rbp), %rax
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%rax, -4432(%rbp)
	leaq	-4160(%rbp), %r14
	call	gf_sub@PLT
	movq	%r13, %rdx
	movq	%r12, %rsi
	leaq	-1984(%rbp), %r13
	leaq	-2048(%rbp), %rdi
	call	gf_add@PLT
	leaq	192(%r12), %rsi
	movq	%r13, %rdi
	movl	$78164, %edx
	call	gf_mulw_unsigned@PLT
	movq	%r13, %rdx
	movq	%r13, %rdi
	leaq	ZERO(%rip), %rsi
	call	gf_sub@PLT
	leaq	128(%r12), %rsi
	movq	%rbx, %rdi
	leaq	-3968(%rbp), %r13
	movq	%rsi, %rdx
	call	gf_add@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	leaq	-4096(%rbp), %r12
	call	point_double_internal.constprop.0
	leaq	-3904(%rbp), %rax
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -4312(%rbp)
	call	gf_sub@PLT
	movq	%r14, %rsi
	movq	%r14, -4400(%rbp)
	movq	%r12, %rdx
	leaq	-3776(%rbp), %r14
	leaq	-3840(%rbp), %rdi
	call	gf_add@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movl	$78164, %edx
	call	gf_mulw_unsigned@PLT
	movq	%r14, %rdx
	movq	%r14, %rdi
	leaq	ZERO(%rip), %rsi
	call	gf_sub@PLT
	leaq	-4032(%rbp), %r14
	leaq	-3712(%rbp), %rax
	movq	%r14, %rdx
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -4328(%rbp)
	call	gf_add@PLT
	leaq	-4224(%rbp), %rax
	movq	%rbx, %rdx
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -4304(%rbp)
	call	gf_mul@PLT
	movq	-4400(%rbp), %rbx
	xorl	%edx, %edx
	movdqa	-4224(%rbp), %xmm4
	movdqa	-4208(%rbp), %xmm5
	movdqa	-4192(%rbp), %xmm6
	movdqa	-4176(%rbp), %xmm7
	movq	-4432(%rbp), %rsi
	movq	%rbx, %rdi
	movaps	%xmm4, -4032(%rbp)
	movaps	%xmm5, -4016(%rbp)
	movaps	%xmm6, -4000(%rbp)
	movaps	%xmm7, -3984(%rbp)
	call	add_niels_to_pt
	movq	%rbx, %rdx
	leaq	-1856(%rbp), %rdi
	movq	%r12, %rsi
	call	gf_sub@PLT
	movq	%rbx, %rsi
	movq	%rbx, -4400(%rbp)
	movq	%r12, %rdx
	leaq	-1728(%rbp), %rbx
	leaq	-1792(%rbp), %rdi
	call	gf_add@PLT
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movl	$78164, %edx
	call	gf_mulw_unsigned@PLT
	movq	%rbx, %rdx
	movq	%rbx, %rdi
	leaq	ZERO(%rip), %rsi
	call	gf_sub@PLT
	movq	%r14, %rdx
	movq	%r14, %rsi
	leaq	-1664(%rbp), %rdi
	call	gf_add@PLT
	leaq	-64(%rbp), %rax
	movq	%r15, -4336(%rbp)
	movq	-4400(%rbp), %r15
	movq	%rax, -4320(%rbp)
	movq	%r13, %rax
	leaq	-1600(%rbp), %rbx
	movq	%r14, %r13
	movq	%rax, %r14
	.p2align 4,,10
	.p2align 3
.L102:
	movq	-4328(%rbp), %rdx
	movq	-4304(%rbp), %rdi
	movq	%r13, %rsi
	call	gf_mul@PLT
	movdqa	-4224(%rbp), %xmm4
	xorl	%edx, %edx
	movdqa	-4208(%rbp), %xmm5
	movdqa	-4192(%rbp), %xmm6
	movdqa	-4176(%rbp), %xmm7
	movq	%r15, %rdi
	movq	-4312(%rbp), %rsi
	movaps	%xmm4, -4032(%rbp)
	movaps	%xmm5, -4016(%rbp)
	movaps	%xmm6, -4000(%rbp)
	movaps	%xmm7, -3984(%rbp)
	call	add_niels_to_pt
	movq	%rbx, %rdi
	movq	%r15, %rdx
	movq	%r12, %rsi
	call	gf_sub@PLT
	leaq	64(%rbx), %rdi
	movq	%r12, %rdx
	movq	%r15, %rsi
	call	gf_add@PLT
	leaq	128(%rbx), %rdi
	movl	$78164, %edx
	movq	%r14, %rsi
	movq	%rdi, -4296(%rbp)
	call	gf_mulw_unsigned@PLT
	movq	-4296(%rbp), %rdi
	leaq	ZERO(%rip), %rsi
	movq	%rdi, %rdx
	call	gf_sub@PLT
	leaq	192(%rbx), %rdi
	movq	%r13, %rdx
	movq	%r13, %rsi
	call	gf_add@PLT
	addq	$256, %rbx
	cmpq	%rbx, -4320(%rbp)
	jne	.L102
	movq	-4400(%rbp), %rdi
	movl	$256, %esi
	movq	-4336(%rbp), %r15
	call	OPENSSL_cleanse@PLT
	movq	-4312(%rbp), %rdi
	movl	$256, %esi
	call	OPENSSL_cleanse@PLT
	movl	-3024(%rbp), %ebx
	testl	%ebx, %ebx
	js	.L127
	leaq	64(%r15), %rax
	movl	-3648(%rbp), %r13d
	movq	%rax, -4360(%rbp)
	leaq	192(%r15), %rax
	movq	%rax, -4344(%rbp)
	cmpl	%ebx, %r13d
	jl	.L128
	je	.L129
	movl	-3644(%rbp), %eax
	leaq	64(%r15), %rbx
	movq	%rbx, %rdi
	sarl	%eax
	cltq
	leaq	(%rax,%rax,2), %r12
	salq	$6, %r12
	addq	curve448_wnaf_base(%rip), %r12
	leaq	64(%r12), %r14
	movq	%r12, %rdx
	movq	%r14, %rsi
	call	gf_add@PLT
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	gf_sub@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdx
	leaq	192(%r15), %rdi
	call	gf_mul@PLT
	movdqa	ONE(%rip), %xmm4
	movl	%r13d, %ebx
	movdqa	16+ONE(%rip), %xmm5
	movdqa	32+ONE(%rip), %xmm6
	movdqa	48+ONE(%rip), %xmm7
	movl	$0, -4404(%rbp)
	movl	$1, -4392(%rbp)
	movaps	%xmm4, 128(%r15)
	movaps	%xmm5, 144(%r15)
	movaps	%xmm6, 160(%r15)
	movaps	%xmm7, 176(%r15)
.L106:
	leaq	128(%r15), %rax
	subl	$1, %ebx
	movq	%rax, -4336(%rbp)
	leaq	-4288(%rbp), %rax
	movq	%rax, -4368(%rbp)
	movl	%ebx, -4296(%rbp)
	jns	.L117
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L111:
	movl	-4296(%rbp), %ecx
	cmpl	%ecx, -4348(%rbp)
	je	.L130
.L114:
	subl	$1, -4296(%rbp)
	movl	-4296(%rbp), %eax
	cmpl	$-1, %eax
	je	.L118
.L117:
	movslq	-4404(%rbp), %rax
	movl	-4296(%rbp), %ecx
	movl	$0, -4328(%rbp)
	movl	-3024(%rbp,%rax,8), %esi
	movq	%rax, -4424(%rbp)
	movslq	-4392(%rbp), %rax
	movl	%esi, -4352(%rbp)
	movl	-3648(%rbp,%rax,8), %edi
	movq	%rax, -4416(%rbp)
	movl	%edi, -4348(%rbp)
	testl	%ecx, %ecx
	je	.L109
	cmpl	%ecx, %esi
	sete	%al
	cmpl	%ecx, %edi
	sete	%dl
	orl	%edx, %eax
	xorl	$1, %eax
	movzbl	%al, %eax
	movl	%eax, -4328(%rbp)
.L109:
	movq	-4400(%rbp), %rdi
	movq	%r15, %rsi
	call	gf_sqr@PLT
	movq	-4360(%rbp), %rsi
	movq	-4368(%rbp), %rdi
	call	gf_sqr@PLT
	movdqa	-4272(%rbp), %xmm2
	movdqa	-4256(%rbp), %xmm1
	paddd	-4144(%rbp), %xmm2
	paddd	-4128(%rbp), %xmm1
	movdqa	-4240(%rbp), %xmm0
	movdqa	-4288(%rbp), %xmm3
	paddd	-4112(%rbp), %xmm0
	paddd	-4160(%rbp), %xmm3
	movaps	%xmm2, -3888(%rbp)
	movaps	%xmm1, -3872(%rbp)
	movdqa	16(%r15), %xmm2
	movdqa	32(%r15), %xmm1
	paddd	80(%r15), %xmm2
	paddd	96(%r15), %xmm1
	movaps	%xmm3, -3904(%rbp)
	movaps	%xmm0, -3856(%rbp)
	movdqa	(%r15), %xmm3
	movdqa	48(%r15), %xmm0
	paddd	64(%r15), %xmm3
	paddd	112(%r15), %xmm0
	movaps	%xmm2, 208(%r15)
	movq	-4344(%rbp), %rsi
	movq	-4304(%rbp), %rdi
	movaps	%xmm1, 224(%r15)
	movaps	%xmm3, 192(%r15)
	movaps	%xmm0, 240(%r15)
	call	gf_sqr@PLT
	movl	-4224(%rbp), %eax
	movl	-4212(%rbp), %r10d
	movl	-4208(%rbp), %r9d
	movl	-4200(%rbp), %r11d
	leal	805306365(%rax), %r13d
	movl	-4220(%rbp), %eax
	subl	-3904(%rbp), %r13d
	movl	%r13d, %r14d
	leal	805306365(%r11), %ecx
	subl	-3880(%rbp), %ecx
	movl	-4184(%rbp), %r11d
	addl	$805306365, %eax
	subl	-3900(%rbp), %eax
	movl	%eax, %r13d
	movl	-4216(%rbp), %eax
	addl	$805306365, %eax
	subl	-3896(%rbp), %eax
	movl	%eax, -4372(%rbp)
	leal	805306365(%r10), %eax
	movl	-4204(%rbp), %r10d
	movl	%eax, %ebx
	leal	805306365(%r9), %eax
	movl	-4196(%rbp), %r9d
	subl	-3892(%rbp), %ebx
	leal	805306365(%r10), %edx
	movl	%eax, %r8d
	movl	-4188(%rbp), %r10d
	subl	-3888(%rbp), %r8d
	leal	805306365(%r9), %esi
	movl	-4180(%rbp), %r9d
	movl	%r8d, -4380(%rbp)
	leal	805306365(%r11), %r8d
	leal	805306365(%r10), %edi
	movl	-4172(%rbp), %r11d
	movl	-4176(%rbp), %r10d
	movl	%ebx, -4376(%rbp)
	addl	$805306365, %r9d
	subl	-3860(%rbp), %r9d
	subl	-3884(%rbp), %edx
	movl	%r9d, -4384(%rbp)
	movl	-4168(%rbp), %r9d
	addl	$805306365, %r10d
	addl	$805306365, %r11d
	subl	-3876(%rbp), %esi
	subl	-3868(%rbp), %edi
	subl	-3864(%rbp), %r8d
	subl	-3856(%rbp), %r10d
	leal	805306365(%r9), %ebx
	subl	-3852(%rbp), %r11d
	subl	-3848(%rbp), %ebx
	movl	-4164(%rbp), %r9d
	leal	805306365(%r9), %r12d
	subl	-3844(%rbp), %r12d
	movl	-4192(%rbp), %r9d
	movl	%r12d, %eax
	andl	$268435455, %r12d
	shrl	$28, %eax
	movl	%eax, -4320(%rbp)
	leal	805306362(%r9), %eax
	movl	%ebx, %r9d
	andl	$268435455, %ebx
	shrl	$28, %r9d
	subl	-3872(%rbp), %eax
	addl	-4320(%rbp), %eax
	addl	%r9d, %r12d
	movl	%r10d, %r9d
	movl	%r12d, -4164(%rbp)
	movl	%r11d, %r12d
	shrl	$28, %r12d
	addl	%r12d, %ebx
	andl	$268435455, %r11d
	shrl	$28, %r9d
	andl	$268435455, %r10d
	movl	%ebx, -4168(%rbp)
	movl	-4384(%rbp), %ebx
	addl	%r9d, %r11d
	movl	%r11d, -4172(%rbp)
	movl	%ebx, %r11d
	andl	$268435455, %ebx
	shrl	$28, %r11d
	movl	%ebx, %r9d
	movl	-4376(%rbp), %ebx
	addl	%r11d, %r10d
	movl	%eax, %r11d
	andl	$268435455, %eax
	movl	%r10d, -4176(%rbp)
	movl	%r8d, %r10d
	shrl	$28, %r11d
	andl	$268435455, %r8d
	shrl	$28, %r10d
	addl	%r10d, %r9d
	movl	%esi, %r10d
	andl	$268435455, %esi
	movl	%r9d, -4180(%rbp)
	movl	%edi, %r9d
	shrl	$28, %r10d
	andl	$268435455, %edi
	shrl	$28, %r9d
	addl	%r10d, %eax
	movl	%edx, %r10d
	andl	$268435455, %edx
	addl	%r9d, %r8d
	movl	%eax, -4192(%rbp)
	addl	%r11d, %edi
	shrl	$28, %r10d
	movl	%r8d, -4184(%rbp)
	movl	-4380(%rbp), %r8d
	movl	%ecx, %r11d
	andl	$268435455, %ecx
	addl	%r10d, %ecx
	shrl	$28, %r11d
	movl	%edi, -4188(%rbp)
	movl	%r8d, %eax
	andl	$268435455, %r8d
	movl	%ecx, -4200(%rbp)
	addl	%r11d, %esi
	shrl	$28, %eax
	movl	-4372(%rbp), %ecx
	movl	%esi, -4196(%rbp)
	addl	%eax, %edx
	movl	%r8d, %eax
	movl	%edx, -4204(%rbp)
	movl	%ebx, %edx
	andl	$268435455, %ebx
	shrl	$28, %edx
	addl	%edx, %eax
	movl	%r13d, %edx
	movl	%eax, -4208(%rbp)
	movl	%ebx, %eax
	movl	%ecx, %ebx
	shrl	$28, %ebx
	addl	%ebx, %eax
	shrl	$28, %edx
	andl	$268435455, %r13d
	movl	%eax, -4212(%rbp)
	movl	%ecx, %eax
	movl	-4320(%rbp), %ecx
	andl	$268435455, %eax
	addl	%edx, %eax
	movl	%r14d, %edx
	shrl	$28, %edx
	movl	%eax, -4216(%rbp)
	movl	%r13d, %eax
	addl	%edx, %eax
	movl	%eax, -4220(%rbp)
	movl	%r14d, %eax
	andl	$268435455, %eax
	leal	(%rax,%rcx), %r13d
	movl	-4288(%rbp), %eax
	movl	%r13d, -4224(%rbp)
	leal	536870910(%rax), %r13d
	movl	-4284(%rbp), %eax
	subl	-4160(%rbp), %r13d
	movl	%r13d, %r14d
	addl	$536870910, %eax
	subl	-4156(%rbp), %eax
	movl	%eax, %r13d
	movl	-4280(%rbp), %eax
	addl	$536870910, %eax
	subl	-4152(%rbp), %eax
	movl	-4276(%rbp), %r8d
	movl	%eax, -4372(%rbp)
	movl	-4264(%rbp), %r10d
	leal	536870910(%r8), %eax
	movl	-4272(%rbp), %r8d
	movl	-4260(%rbp), %r11d
	movl	%eax, %ebx
	leal	536870910(%r10), %ecx
	movl	-4248(%rbp), %r10d
	subl	-4148(%rbp), %ebx
	leal	536870910(%r8), %eax
	movl	-4268(%rbp), %r8d
	movl	%ebx, -4376(%rbp)
	leal	536870910(%r11), %esi
	movl	%eax, %r9d
	movl	-4244(%rbp), %r11d
	subl	-4144(%rbp), %r9d
	leal	536870910(%r8), %edx
	movl	-4252(%rbp), %r8d
	movl	%r9d, -4380(%rbp)
	leal	536870910(%r11), %r9d
	movl	-4236(%rbp), %r11d
	subl	-4140(%rbp), %edx
	leal	536870910(%r8), %edi
	leal	536870910(%r10), %r8d
	subl	-4120(%rbp), %r8d
	subl	-4136(%rbp), %ecx
	movl	%r8d, -4384(%rbp)
	movl	-4240(%rbp), %r8d
	addl	$536870910, %r11d
	subl	-4108(%rbp), %r11d
	subl	-4132(%rbp), %esi
	leal	536870910(%r8), %r10d
	movl	-4232(%rbp), %r8d
	subl	-4112(%rbp), %r10d
	subl	-4124(%rbp), %edi
	subl	-4116(%rbp), %r9d
	leal	536870910(%r8), %ebx
	movl	-4228(%rbp), %r8d
	subl	-4104(%rbp), %ebx
	leal	536870910(%r8), %r12d
	subl	-4100(%rbp), %r12d
	movl	-4256(%rbp), %r8d
	movl	%r12d, %eax
	andl	$268435455, %r12d
	shrl	$28, %eax
	movl	%eax, -4320(%rbp)
	leal	536870908(%r8), %eax
	movl	%ebx, %r8d
	subl	-4128(%rbp), %eax
	shrl	$28, %r8d
	addl	-4320(%rbp), %eax
	addl	%r8d, %r12d
	movl	%r10d, %r8d
	andl	$268435455, %ebx
	andl	$268435455, %r10d
	shrl	$28, %r8d
	movl	%r12d, 252(%r15)
	movl	%r11d, %r12d
	andl	$268435455, %r11d
	addl	%r8d, %r11d
	shrl	$28, %r12d
	addl	%r12d, %ebx
	movl	%r11d, 244(%r15)
	movl	%r9d, %r11d
	andl	$268435455, %r9d
	shrl	$28, %r11d
	movl	%ebx, 248(%r15)
	movl	-4384(%rbp), %ebx
	addl	%r11d, %r10d
	movl	%edi, %r11d
	andl	$268435455, %edi
	movl	%r10d, 240(%r15)
	movl	%ebx, %r10d
	andl	$268435455, %ebx
	shrl	$28, %r11d
	shrl	$28, %r10d
	movl	%ebx, %r8d
	movl	-4376(%rbp), %ebx
	addl	%r10d, %r9d
	addl	%r11d, %r8d
	movl	%esi, %r10d
	movl	%edx, %r11d
	shrl	$28, %r10d
	andl	$268435455, %edx
	shrl	$28, %r11d
	movl	%r9d, 236(%r15)
	movl	%r8d, 232(%r15)
	movl	-4380(%rbp), %r9d
	movl	%eax, %r8d
	andl	$268435455, %eax
	addl	%r10d, %eax
	shrl	$28, %r8d
	andl	$268435455, %esi
	movl	%eax, 224(%r15)
	movl	%r9d, %eax
	addl	%r8d, %edi
	movl	%ecx, %r8d
	shrl	$28, %eax
	andl	$268435455, %ecx
	shrl	$28, %r8d
	andl	$268435455, %r9d
	addl	%eax, %edx
	addl	%r11d, %ecx
	movl	%r9d, %eax
	addl	%r8d, %esi
	movl	%edx, 212(%r15)
	movl	%ebx, %edx
	movl	%ecx, 216(%r15)
	shrl	$28, %edx
	movl	-4372(%rbp), %ecx
	addl	%edx, %eax
	andl	$268435455, %ebx
	movl	%r13d, %edx
	andl	$268435455, %r13d
	movl	%eax, 208(%r15)
	movl	%ebx, %eax
	movl	%ecx, %ebx
	shrl	$28, %edx
	shrl	$28, %ebx
	movl	%edi, 228(%r15)
	movq	%r15, %rdi
	addl	%ebx, %eax
	movl	%esi, 220(%r15)
	movq	-4336(%rbp), %rsi
	movl	%eax, 204(%r15)
	movl	%ecx, %eax
	movl	-4320(%rbp), %ecx
	andl	$268435455, %eax
	addl	%edx, %eax
	movl	%r14d, %edx
	shrl	$28, %edx
	movl	%eax, 200(%r15)
	movl	%r13d, %eax
	addl	%edx, %eax
	movl	%eax, 196(%r15)
	movl	%r14d, %eax
	andl	$268435455, %eax
	leal	(%rax,%rcx), %r13d
	movl	%r13d, 192(%r15)
	call	gf_sqr@PLT
	movl	(%r15), %eax
	movl	12(%r15), %r10d
	movl	16(%r15), %r9d
	movl	24(%r15), %r11d
	leal	(%rax,%rax), %r14d
	movl	4(%r15), %eax
	leal	(%r10,%r10), %r13d
	movl	20(%r15), %r10d
	movl	8(%r15), %ecx
	leal	(%r9,%r9), %r12d
	leal	(%r11,%r11), %edx
	movl	28(%r15), %r9d
	addl	%eax, %eax
	movl	36(%r15), %r11d
	movl	%r14d, 128(%r15)
	movl	%eax, -4372(%rbp)
	addl	%ecx, %ecx
	movl	%eax, 132(%r15)
	leal	(%r10,%r10), %eax
	movl	32(%r15), %r10d
	leal	(%r11,%r11), %esi
	movl	%ecx, -4376(%rbp)
	movl	48(%r15), %r11d
	leal	(%r10,%r10), %ebx
	movl	%ecx, 136(%r15)
	movl	44(%r15), %r10d
	leal	(%r9,%r9), %ecx
	movl	40(%r15), %r9d
	movl	%ebx, -4388(%rbp)
	leal	(%r10,%r10), %r8d
	movl	52(%r15), %r10d
	movl	%ebx, 160(%r15)
	leal	(%r9,%r9), %edi
	leal	(%r11,%r11), %r9d
	movl	56(%r15), %r11d
	movl	%r13d, 140(%r15)
	addl	%r10d, %r10d
	movl	%r12d, 144(%r15)
	leal	(%r11,%r11), %ebx
	movl	%eax, 148(%r15)
	movl	%edx, 152(%r15)
	movl	%ecx, 156(%r15)
	movl	%esi, 164(%r15)
	movl	%edi, 168(%r15)
	movl	%r8d, 172(%r15)
	movl	%r9d, 176(%r15)
	movl	%r10d, 180(%r15)
	movl	%ebx, 184(%r15)
	subl	192(%r15), %r14d
	addl	$1073741820, %r14d
	movl	60(%r15), %r11d
	subl	204(%r15), %r13d
	movl	%r14d, -4320(%rbp)
	movl	-4372(%rbp), %r14d
	addl	$1073741820, %r13d
	subl	196(%r15), %r14d
	subl	208(%r15), %r12d
	addl	%r11d, %r11d
	addl	$1073741820, %r14d
	subl	212(%r15), %eax
	subl	216(%r15), %edx
	addl	$1073741820, %r12d
	subl	220(%r15), %ecx
	movl	%r14d, -4372(%rbp)
	addl	$1073741820, %eax
	addl	$1073741820, %edx
	movl	-4376(%rbp), %r14d
	subl	200(%r15), %r14d
	addl	$1073741820, %ecx
	movl	%r11d, 188(%r15)
	addl	$1073741820, %r14d
	subl	228(%r15), %esi
	subl	248(%r15), %ebx
	movl	%eax, -4384(%rbp)
	addl	$1073741820, %ebx
	subl	252(%r15), %r11d
	subl	244(%r15), %r10d
	movl	%r12d, -4380(%rbp)
	addl	$1073741820, %r11d
	movl	%ebx, %eax
	addl	$1073741820, %r10d
	subl	240(%r15), %r9d
	shrl	$28, %eax
	movl	%r11d, %r12d
	andl	$268435455, %r11d
	addl	$1073741820, %r9d
	addl	%eax, %r11d
	movl	%r10d, %eax
	andl	$268435455, %ebx
	subl	236(%r15), %r8d
	shrl	$28, %eax
	addl	$1073741820, %r8d
	shrl	$28, %r12d
	andl	$268435455, %r10d
	addl	%eax, %ebx
	movl	%r9d, %eax
	subl	232(%r15), %edi
	andl	$268435455, %r9d
	shrl	$28, %eax
	addl	$1073741820, %edi
	movl	%r13d, -4376(%rbp)
	movl	-4388(%rbp), %r13d
	addl	%eax, %r10d
	movl	%r8d, %eax
	subl	224(%r15), %r13d
	andl	$268435455, %r8d
	shrl	$28, %eax
	addl	$1073741820, %esi
	leal	1073741816(%r12,%r13), %r13d
	movl	%r11d, -4228(%rbp)
	addl	%eax, %r9d
	movl	%r13d, %eax
	movl	%ebx, -4232(%rbp)
	movl	%r9d, -4240(%rbp)
	movl	%edi, %r9d
	andl	$268435455, %edi
	shrl	$28, %r9d
	movl	%r10d, -4236(%rbp)
	addl	%r9d, %r8d
	movl	%r8d, -4244(%rbp)
	movl	%esi, %r8d
	shrl	$28, %r8d
	shrl	$28, %eax
	andl	$268435455, %esi
	andl	$268435455, %r13d
	addl	%eax, %esi
	movl	%ecx, %eax
	addl	%r8d, %edi
	andl	$268435455, %ecx
	shrl	$28, %eax
	movl	%edi, -4248(%rbp)
	movq	%r15, %rdi
	movl	%esi, -4252(%rbp)
	addl	%eax, %r13d
	movl	%edx, %eax
	movl	-4384(%rbp), %ebx
	shrl	$28, %eax
	movl	-4380(%rbp), %r11d
	andl	$268435455, %edx
	movl	-4376(%rbp), %r10d
	addl	%eax, %ecx
	movl	%ebx, %eax
	movl	%r13d, -4256(%rbp)
	shrl	$28, %eax
	movl	%ecx, -4260(%rbp)
	movl	-4320(%rbp), %ecx
	addl	%eax, %edx
	movl	%ebx, %eax
	movl	-4372(%rbp), %ebx
	movl	%edx, -4264(%rbp)
	movl	%r11d, %edx
	andl	$268435455, %eax
	shrl	$28, %edx
	addl	%edx, %eax
	movl	%r10d, %edx
	andl	$268435455, %r10d
	movl	%eax, -4268(%rbp)
	movl	%r11d, %eax
	shrl	$28, %edx
	movl	%r10d, %r13d
	andl	$268435455, %eax
	addl	%edx, %eax
	movq	-4304(%rbp), %rdx
	movl	%eax, -4272(%rbp)
	movl	%r14d, %eax
	andl	$268435455, %r14d
	shrl	$28, %eax
	addl	%eax, %r13d
	movl	%ebx, %eax
	andl	$268435455, %ebx
	shrl	$28, %eax
	movl	%r13d, -4276(%rbp)
	addl	%eax, %r14d
	movl	%ecx, %eax
	andl	$268435455, %ecx
	shrl	$28, %eax
	movl	%r14d, -4280(%rbp)
	movl	%ebx, %r14d
	addl	%ecx, %r12d
	addl	%eax, %r14d
	movl	%r12d, -4288(%rbp)
	movl	%r14d, -4284(%rbp)
	movq	-4368(%rbp), %r14
	movq	%r14, %rsi
	call	gf_mul@PLT
	movq	-4344(%rbp), %rbx
	movq	-4336(%rbp), %rdi
	movq	%r14, %rdx
	movq	%rbx, %rsi
	call	gf_mul@PLT
	movq	-4312(%rbp), %rdx
	movq	-4360(%rbp), %rdi
	movq	%rbx, %rsi
	call	gf_mul@PLT
	movl	-4328(%rbp), %eax
	testl	%eax, %eax
	je	.L131
.L110:
	movl	-4296(%rbp), %esi
	cmpl	%esi, -4352(%rbp)
	jne	.L111
	movq	-4424(%rbp), %rax
	testl	%esi, %esi
	setne	%dl
	xorl	%r13d, %r13d
	cmpl	%esi, -4348(%rbp)
	movl	-3020(%rbp,%rax,8), %eax
	setne	%r13b
	andl	%edx, %r13d
	testl	%eax, %eax
	jle	.L112
	sarl	%eax
	movq	-4336(%rbp), %rsi
	movq	-4312(%rbp), %rdi
	movslq	%eax, %r12
	salq	$8, %r12
	addq	-4432(%rbp), %r12
	leaq	192(%r12), %rdx
	call	gf_mul@PLT
	movdqa	-3904(%rbp), %xmm4
	movl	%r13d, %edx
	movdqa	-3888(%rbp), %xmm5
	movdqa	-3872(%rbp), %xmm6
	movdqa	-3856(%rbp), %xmm7
	movq	%r12, %rsi
	movq	%r15, %rdi
	movaps	%xmm4, 128(%r15)
	movaps	%xmm5, 144(%r15)
	movaps	%xmm6, 160(%r15)
	movaps	%xmm7, 176(%r15)
	call	add_niels_to_pt
.L113:
	addl	$1, -4404(%rbp)
	movl	-4296(%rbp), %ecx
	cmpl	%ecx, -4348(%rbp)
	jne	.L114
.L130:
	movq	-4416(%rbp), %rax
	movq	curve448_wnaf_base(%rip), %rdx
	movl	-3644(%rbp,%rax,8), %eax
	testl	%eax, %eax
	jle	.L115
	sarl	%eax
	movq	%r15, %rdi
	cltq
	leaq	(%rax,%rax,2), %rsi
	salq	$6, %rsi
	addq	%rdx, %rsi
	movl	%ecx, %edx
	call	add_niels_to_pt
.L116:
	addl	$1, -4392(%rbp)
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L128:
	movl	-3020(%rbp), %r12d
	movq	-4312(%rbp), %rdi
	leaq	64(%r15), %r14
	sarl	%r12d
	movslq	%r12d, %r12
	salq	$8, %r12
	addq	-4432(%rbp), %r12
	leaq	64(%r12), %r13
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	gf_add@PLT
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	gf_sub@PLT
	movq	-4312(%rbp), %r13
	movq	%r14, %rsi
	addq	$192, %r12
	leaq	192(%r15), %rdi
	movq	%r13, %rdx
	call	gf_mul@PLT
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	gf_mul@PLT
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	gf_mul@PLT
	leaq	128(%r15), %rdi
	movq	%r12, %rsi
	call	gf_sqr@PLT
	movl	$1, -4404(%rbp)
	movl	$0, -4392(%rbp)
	jmp	.L106
	.p2align 4,,10
	.p2align 3
.L131:
	movq	-4312(%rbp), %rdx
	movq	-4304(%rbp), %rsi
	movq	-4344(%rbp), %rdi
	call	gf_mul@PLT
	jmp	.L110
	.p2align 4,,10
	.p2align 3
.L112:
	negl	%eax
	movq	-4336(%rbp), %rsi
	movq	-4312(%rbp), %rdi
	movl	%eax, %r12d
	sarl	%r12d
	movslq	%r12d, %r12
	salq	$8, %r12
	addq	-4432(%rbp), %r12
	leaq	192(%r12), %rdx
	call	gf_mul@PLT
	movdqa	-3904(%rbp), %xmm4
	movl	%r13d, %edx
	movdqa	-3888(%rbp), %xmm5
	movdqa	-3872(%rbp), %xmm6
	movdqa	-3856(%rbp), %xmm7
	movq	%r12, %rsi
	movq	%r15, %rdi
	movaps	%xmm4, 128(%r15)
	movaps	%xmm5, 144(%r15)
	movaps	%xmm6, 160(%r15)
	movaps	%xmm7, 176(%r15)
	call	sub_niels_from_pt
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L115:
	negl	%eax
	movq	%r15, %rdi
	sarl	%eax
	cltq
	leaq	(%rax,%rax,2), %rsi
	salq	$6, %rsi
	addq	%rdx, %rsi
	movl	-4296(%rbp), %edx
	call	sub_niels_from_pt
	jmp	.L116
	.p2align 4,,10
	.p2align 3
.L118:
	movq	-4448(%rbp), %rdi
	movl	$912, %esi
	call	OPENSSL_cleanse@PLT
	movq	-4440(%rbp), %rdi
	movl	$616, %esi
	call	OPENSSL_cleanse@PLT
	movq	-4432(%rbp), %rdi
	movl	$2048, %esi
	call	OPENSSL_cleanse@PLT
.L101:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L132
	addq	$4408, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L127:
	.cfi_restore_state
	movdqa	curve448_point_identity(%rip), %xmm4
	movdqa	16+curve448_point_identity(%rip), %xmm5
	movdqa	32+curve448_point_identity(%rip), %xmm6
	movdqa	48+curve448_point_identity(%rip), %xmm7
	movaps	%xmm4, (%r15)
	movdqa	64+curve448_point_identity(%rip), %xmm4
	movaps	%xmm5, 16(%r15)
	movdqa	80+curve448_point_identity(%rip), %xmm5
	movaps	%xmm6, 32(%r15)
	movdqa	96+curve448_point_identity(%rip), %xmm6
	movaps	%xmm7, 48(%r15)
	movdqa	112+curve448_point_identity(%rip), %xmm7
	movaps	%xmm4, 64(%r15)
	movdqa	128+curve448_point_identity(%rip), %xmm4
	movaps	%xmm5, 80(%r15)
	movdqa	144+curve448_point_identity(%rip), %xmm5
	movaps	%xmm6, 96(%r15)
	movdqa	160+curve448_point_identity(%rip), %xmm6
	movaps	%xmm7, 112(%r15)
	movdqa	176+curve448_point_identity(%rip), %xmm7
	movaps	%xmm4, 128(%r15)
	movdqa	192+curve448_point_identity(%rip), %xmm4
	movaps	%xmm5, 144(%r15)
	movdqa	208+curve448_point_identity(%rip), %xmm5
	movaps	%xmm6, 160(%r15)
	movdqa	224+curve448_point_identity(%rip), %xmm6
	movaps	%xmm7, 176(%r15)
	movdqa	240+curve448_point_identity(%rip), %xmm7
	movaps	%xmm4, 192(%r15)
	movaps	%xmm5, 208(%r15)
	movaps	%xmm6, 224(%r15)
	movaps	%xmm7, 240(%r15)
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L129:
	movl	-3020(%rbp), %r12d
	movq	-4312(%rbp), %rdi
	leaq	64(%r15), %r14
	sarl	%r12d
	movslq	%r12d, %r12
	salq	$8, %r12
	addq	-4432(%rbp), %r12
	leaq	64(%r12), %r13
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	gf_add@PLT
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	gf_sub@PLT
	movq	-4312(%rbp), %r13
	movq	%r14, %rsi
	addq	$192, %r12
	leaq	192(%r15), %rdi
	movq	%r13, %rdx
	call	gf_mul@PLT
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	gf_mul@PLT
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	gf_mul@PLT
	leaq	128(%r15), %rdi
	movq	%r12, %rsi
	call	gf_sqr@PLT
	movl	-3644(%rbp), %eax
	movl	%ebx, %edx
	movq	%r15, %rdi
	sarl	%eax
	cltq
	leaq	(%rax,%rax,2), %rsi
	salq	$6, %rsi
	addq	curve448_wnaf_base(%rip), %rsi
	call	add_niels_to_pt
	movl	$1, -4404(%rbp)
	movl	$1, -4392(%rbp)
	jmp	.L106
.L132:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE227:
	.size	curve448_base_double_scalarmul_non_secret, .-curve448_base_double_scalarmul_non_secret
	.p2align 4
	.globl	curve448_point_destroy
	.type	curve448_point_destroy, @function
curve448_point_destroy:
.LFB228:
	.cfi_startproc
	endbr64
	movl	$256, %esi
	jmp	OPENSSL_cleanse@PLT
	.cfi_endproc
.LFE228:
	.size	curve448_point_destroy, .-curve448_point_destroy
	.p2align 4
	.globl	X448
	.type	X448, @function
X448:
.LFB229:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rdx, %rsi
	movq	%r8, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	x448_int
	popq	%rbp
	.cfi_def_cfa 7, 8
	cmpl	$-1, %eax
	sete	%al
	movzbl	%al, %eax
	ret
	.cfi_endproc
.LFE229:
	.size	X448, .-X448
	.p2align 4
	.globl	X448_public_from_private
	.type	X448_public_from_private, @function
X448_public_from_private:
.LFB230:
	.cfi_startproc
	endbr64
	jmp	x448_derive_public_key
	.cfi_endproc
.LFE230:
	.size	X448_public_from_private, .-X448_public_from_private
	.globl	curve448_point_identity
	.section	.rodata
	.align 32
	.type	curve448_point_identity, @object
	.size	curve448_point_identity, 256
curve448_point_identity:
	.long	0
	.zero	60
	.long	1
	.zero	60
	.long	1
	.zero	60
	.long	0
	.zero	60
	.align 32
	.type	precomputed_scalarmul_adjustment, @object
	.size	precomputed_scalarmul_adjustment, 56
precomputed_scalarmul_adjustment:
	.quad	-4002619432236240689
	.quad	-1642731020532381011
	.quad	-4966824541560203523
	.quad	35221520739
	.zero	24
	.align 32
	.type	ONE, @object
	.size	ONE, 64
ONE:
	.long	1
	.zero	60
	.align 32
	.type	ZERO, @object
	.size	ZERO, 64
ZERO:
	.zero	64
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	268435455
	.long	268435455
	.long	268435455
	.long	268435455
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
