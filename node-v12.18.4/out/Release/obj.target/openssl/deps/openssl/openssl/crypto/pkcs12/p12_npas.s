	.file	"p12_npas.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/pkcs12/p12_npas.c"
	.text
	.p2align 4
	.globl	PKCS12_newpass
	.type	PKCS12_newpass, @function
PKCS12_newpass:
.LFB766:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$200, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -176(%rbp)
	movq	%rdx, -160(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L79
	movl	$-1, %edx
	movq	%rsi, %r12
	call	PKCS12_verify_mac@PLT
	testl	%eax, %eax
	je	.L80
	movq	-176(%rbp), %rdi
	movq	$0, -144(%rbp)
	call	PKCS12_unpack_authsafes@PLT
	movq	%rax, -168(%rbp)
	testq	%rax, %rax
	je	.L5
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, -224(%rbp)
	testq	%rax, %rax
	je	.L5
	movq	PKCS12_SAFEBAG_free@GOTPCREL(%rip), %rax
	movl	$0, -196(%rbp)
	movl	$0, -192(%rbp)
	movq	%rax, -232(%rbp)
	leaq	-136(%rbp), %rax
	movl	$0, -188(%rbp)
	movl	$0, -148(%rbp)
	movq	%rax, -240(%rbp)
	movq	%r12, -184(%rbp)
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L9:
	addl	$1, -148(%rbp)
.L6:
	movq	-168(%rbp), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, -148(%rbp)
	jge	.L81
	movl	-148(%rbp), %esi
	movq	-168(%rbp), %rdi
	call	OPENSSL_sk_value@PLT
	movq	24(%rax), %rdi
	movq	%rax, %r12
	call	OBJ_obj2nid@PLT
	movl	%eax, -152(%rbp)
	cmpl	$21, %eax
	je	.L82
	cmpl	$26, -152(%rbp)
	jne	.L9
	movq	-184(%rbp), %rsi
	movq	%r12, %rdi
	movl	$-1, %edx
	call	PKCS12_unpack_p7encdata@PLT
	leaq	PBEPARAM_it(%rip), %rdi
	movq	%rax, %r14
	movq	32(%r12), %rax
	movq	8(%rax), %rax
	movq	8(%rax), %rbx
	movq	8(%rbx), %rsi
	call	ASN1_TYPE_unpack_sequence@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L10
	movq	(%rbx), %rdi
	call	OBJ_obj2nid@PLT
	movq	8(%r12), %rdi
	movl	%eax, -188(%rbp)
	call	ASN1_INTEGER_get@PLT
	movq	%r12, %rdi
	movl	%eax, -192(%rbp)
	movq	(%r12), %rax
	movl	(%rax), %eax
	movl	%eax, -196(%rbp)
	call	PBEPARAM_free@PLT
.L8:
	testq	%r14, %r14
	je	.L10
	xorl	%r13d, %r13d
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L17:
	addl	$1, %r13d
.L11:
	movq	%r14, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r13d
	jge	.L83
	movq	%r14, %rdi
	movl	%r13d, %esi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %rdi
	movq	%rax, %rbx
	call	PKCS12_SAFEBAG_get_nid@PLT
	cmpl	$151, %eax
	jne	.L17
	movq	8(%rbx), %rdi
	movq	-184(%rbp), %rsi
	movl	$-1, %edx
	call	PKCS8_decrypt@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L10
	movq	8(%rbx), %rdi
	movq	-240(%rbp), %rsi
	xorl	%edx, %edx
	call	X509_SIG_get0@PLT
	movq	-136(%rbp), %rdx
	leaq	PBEPARAM_it(%rip), %rdi
	movq	8(%rdx), %rsi
	movq	%rdx, -208(%rbp)
	call	ASN1_TYPE_unpack_sequence@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L10
	movq	-208(%rbp), %rdx
	movq	(%rdx), %rdi
	call	OBJ_obj2nid@PLT
	movq	8(%r12), %rdi
	movl	%eax, -200(%rbp)
	call	ASN1_INTEGER_get@PLT
	movq	(%r12), %rdx
	movq	%r12, %rdi
	movq	%rax, -216(%rbp)
	movl	(%rdx), %r9d
	movl	%r9d, -208(%rbp)
	call	PBEPARAM_free@PLT
	movq	-216(%rbp), %rax
	pushq	%r15
	xorl	%esi, %esi
	movl	-200(%rbp), %r10d
	movl	-208(%rbp), %r9d
	movl	$-1, %ecx
	xorl	%r8d, %r8d
	pushq	%rax
	movq	-160(%rbp), %rdx
	movl	%r10d, %edi
	call	PKCS8_encrypt@PLT
	movq	%r15, %rdi
	movq	%rax, %r12
	call	PKCS8_PRIV_KEY_INFO_free@PLT
	popq	%rcx
	popq	%rsi
	testq	%r12, %r12
	je	.L10
	movq	8(%rbx), %rdi
	call	X509_SIG_free@PLT
	movq	%r12, 8(%rbx)
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L5:
	movq	PKCS12_SAFEBAG_free@GOTPCREL(%rip), %rax
	xorl	%r14d, %r14d
	movq	$0, -224(%rbp)
	movq	%rax, -232(%rbp)
	.p2align 4,,10
	.p2align 3
.L10:
	movq	-232(%rbp), %rsi
	movq	%r14, %rdi
	call	OPENSSL_sk_pop_free@PLT
	movq	PKCS7_free@GOTPCREL(%rip), %r12
	movq	-168(%rbp), %rdi
	movq	%r12, %rsi
	call	OPENSSL_sk_pop_free@PLT
	movq	-224(%rbp), %rdi
	movq	%r12, %rsi
	call	OPENSSL_sk_pop_free@PLT
	movl	$50, %r8d
	movl	$114, %edx
	leaq	.LC0(%rip), %rcx
	movl	$128, %esi
	movl	$35, %edi
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L1:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L84
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L83:
	.cfi_restore_state
	cmpl	$21, -152(%rbp)
	je	.L85
	subq	$8, %rsp
	movq	-160(%rbp), %rsi
	xorl	%ecx, %ecx
	movl	-192(%rbp), %r9d
	pushq	%r14
	movl	-188(%rbp), %edi
	movl	$-1, %edx
	movl	-196(%rbp), %r8d
	call	PKCS12_pack_p7encdata@PLT
	movq	%rax, %rsi
	popq	%rax
	popq	%rdx
.L20:
	testq	%rsi, %rsi
	je	.L10
	movq	-224(%rbp), %rdi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L10
	movq	-232(%rbp), %rsi
	movq	%r14, %rdi
	call	OPENSSL_sk_pop_free@PLT
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L82:
	movq	%r12, %rdi
	call	PKCS12_unpack_p7data@PLT
	movq	%rax, %r14
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L85:
	movq	%r14, %rdi
	call	PKCS12_pack_p7data@PLT
	movq	%rax, %rsi
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L80:
	movl	$45, %r8d
	movl	$113, %edx
	movl	$128, %esi
	movl	$35, %edi
	leaq	.LC0(%rip), %rcx
	movl	%eax, -148(%rbp)
	call	ERR_put_error@PLT
	movl	-148(%rbp), %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L79:
	movl	$37, %r8d
	movl	$105, %edx
	movl	$128, %esi
	movl	$35, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L81:
	movq	-176(%rbp), %r15
	movq	16(%r15), %rbx
	movq	32(%rbx), %r14
	call	ASN1_OCTET_STRING_new@PLT
	movq	%rax, 32(%rbx)
	testq	%rax, %rax
	je	.L22
	movq	-224(%rbp), %rbx
	movq	%r15, %rdi
	movq	%rbx, %rsi
	call	PKCS12_pack_authsafes@PLT
	testl	%eax, %eax
	jne	.L86
.L22:
	testq	%r14, %r14
	je	.L10
	movq	-176(%rbp), %rbx
	movq	16(%rbx), %rax
	movq	32(%rax), %rdi
	call	ASN1_OCTET_STRING_free@PLT
	movq	16(%rbx), %rax
	movq	%r14, 32(%rax)
	xorl	%r14d, %r14d
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L86:
	leaq	-128(%rbp), %r12
	movq	-160(%rbp), %rsi
	movl	$-1, %edx
	movq	%r15, %rdi
	leaq	-136(%rbp), %r8
	movq	%r12, %rcx
	call	PKCS12_gen_mac@PLT
	testl	%eax, %eax
	je	.L22
	movq	8(%r15), %rax
	xorl	%esi, %esi
	leaq	-144(%rbp), %rdx
	movq	(%rax), %rdi
	call	X509_SIG_getm@PLT
	movl	-136(%rbp), %edx
	movq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	ASN1_OCTET_STRING_set@PLT
	testl	%eax, %eax
	je	.L22
	movq	%r14, %rdi
	call	ASN1_OCTET_STRING_free@PLT
	movq	-232(%rbp), %rsi
	xorl	%edi, %edi
	call	OPENSSL_sk_pop_free@PLT
	movq	PKCS7_free@GOTPCREL(%rip), %r12
	movq	-168(%rbp), %rdi
	movq	%r12, %rsi
	call	OPENSSL_sk_pop_free@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	OPENSSL_sk_pop_free@PLT
	movl	$1, %eax
	jmp	.L1
.L84:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE766:
	.size	PKCS12_newpass, .-PKCS12_newpass
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
