	.file	"asn1_gen.c"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	""
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"../deps/openssl/openssl/crypto/asn1/asn1_gen.c"
	.section	.rodata.str1.1
.LC2:
	.string	"string="
	.text
	.p2align 4
	.type	generate_v3, @function
generate_v3:
.LFB1298:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	leaq	-576(%rbp), %r8
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movl	$44, %esi
	pushq	%r12
	pushq	%rbx
	subq	$648, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, -676(%rbp)
	movl	$1, %edx
	movq	%rcx, -664(%rbp)
	leaq	asn1_cb(%rip), %rcx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$0, -72(%rbp)
	movq	$0, -640(%rbp)
	movq	$0, -624(%rbp)
	movq	$-1, -576(%rbp)
	movl	$1, -564(%rbp)
	call	CONF_parse_list@PLT
	testl	%eax, %eax
	jne	.L109
	movl	-568(%rbp), %r15d
	movl	%eax, %ebx
	leal	-16(%r15), %eax
	cmpl	$1, %eax
	ja	.L4
	testq	%r13, %r13
	je	.L110
	cmpl	$49, -676(%rbp)
	jg	.L111
	movq	$0, -608(%rbp)
	movq	-560(%rbp), %r14
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, -672(%rbp)
	testq	%rax, %rax
	je	.L7
	testq	%r14, %r14
	je	.L8
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	X509V3_get_section@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L7
	movl	-676(%rbp), %eax
	xorl	%r12d, %r12d
	addl	$1, %eax
	movl	%eax, -676(%rbp)
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L11:
	movl	%r12d, %esi
	movq	%r14, %rdi
	call	OPENSSL_sk_value@PLT
	movq	-664(%rbp), %rcx
	movl	-676(%rbp), %edx
	movq	%r13, %rsi
	movq	16(%rax), %rdi
	call	generate_v3
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L10
	movq	-672(%rbp), %rdi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L10
	addl	$1, %r12d
.L9:
	movq	%r14, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r12d
	jl	.L11
.L8:
	movq	-672(%rbp), %rdi
	leaq	-608(%rbp), %rsi
	cmpl	$17, %r15d
	je	.L112
	call	i2d_ASN1_SEQUENCE_ANY@PLT
	movl	%eax, %r12d
.L13:
	testl	%r12d, %r12d
	js	.L10
	call	ASN1_TYPE_new@PLT
	testq	%rax, %rax
	movq	%rax, -664(%rbp)
	je	.L10
	movl	%r15d, %edi
	call	ASN1_STRING_type_new@PLT
	movq	-664(%rbp), %r10
	movq	%rax, 8(%r10)
	testq	%rax, %rax
	je	.L14
	movq	-608(%rbp), %rdx
	movl	%r15d, (%r10)
	xorl	%edi, %edi
	movl	%r12d, (%rax)
	movq	%rdx, 8(%rax)
	movq	$0, -608(%rbp)
.L15:
	movl	$458, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r10, -664(%rbp)
	call	CRYPTO_free@PLT
	movq	ASN1_TYPE_free@GOTPCREL(%rip), %rsi
	movq	-672(%rbp), %rdi
	call	OPENSSL_sk_pop_free@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	X509V3_section_free@PLT
	movq	-664(%rbp), %r10
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L4:
	movl	-564(%rbp), %r13d
	movq	-560(%rbp), %r12
	call	ASN1_TYPE_new@PLT
	movq	%rax, %r10
	testq	%rax, %rax
	je	.L113
	testq	%r12, %r12
	je	.L114
	cmpl	$30, %r15d
	ja	.L43
	leaq	.L61(%rip), %rcx
	movl	%r15d, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L61:
	.long	.L43-.L61
	.long	.L22-.L61
	.long	.L25-.L61
	.long	.L35-.L61
	.long	.L35-.L61
	.long	.L19-.L61
	.long	.L27-.L61
	.long	.L43-.L61
	.long	.L43-.L61
	.long	.L43-.L61
	.long	.L25-.L61
	.long	.L43-.L61
	.long	.L33-.L61
	.long	.L43-.L61
	.long	.L43-.L61
	.long	.L43-.L61
	.long	.L43-.L61
	.long	.L43-.L61
	.long	.L33-.L61
	.long	.L33-.L61
	.long	.L33-.L61
	.long	.L43-.L61
	.long	.L33-.L61
	.long	.L29-.L61
	.long	.L29-.L61
	.long	.L43-.L61
	.long	.L33-.L61
	.long	.L33-.L61
	.long	.L33-.L61
	.long	.L43-.L61
	.long	.L33-.L61
	.text
	.p2align 4,,10
	.p2align 3
.L114:
	cmpl	$30, %r15d
	ja	.L68
	leaq	.L62(%rip), %rcx
	movl	%r15d, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L62:
	.long	.L68-.L62
	.long	.L69-.L62
	.long	.L70-.L62
	.long	.L71-.L62
	.long	.L71-.L62
	.long	.L42-.L62
	.long	.L72-.L62
	.long	.L68-.L62
	.long	.L68-.L62
	.long	.L68-.L62
	.long	.L70-.L62
	.long	.L68-.L62
	.long	.L73-.L62
	.long	.L68-.L62
	.long	.L68-.L62
	.long	.L68-.L62
	.long	.L68-.L62
	.long	.L68-.L62
	.long	.L73-.L62
	.long	.L73-.L62
	.long	.L73-.L62
	.long	.L68-.L62
	.long	.L73-.L62
	.long	.L74-.L62
	.long	.L74-.L62
	.long	.L68-.L62
	.long	.L73-.L62
	.long	.L73-.L62
	.long	.L73-.L62
	.long	.L68-.L62
	.long	.L73-.L62
	.text
.L74:
	leaq	.LC0(%rip), %r12
.L29:
	movq	%r10, -664(%rbp)
	cmpl	$1, %r13d
	jne	.L115
	call	ASN1_STRING_new@PLT
	movq	-664(%rbp), %r10
	movl	$648, %r8d
	movq	%rax, %rdi
	movq	%rax, 8(%r10)
	testq	%rax, %rax
	je	.L107
	movl	$-1, %edx
	movq	%r12, %rsi
	movq	%r10, -664(%rbp)
	call	ASN1_STRING_set@PLT
	movq	-664(%rbp), %r10
	testl	%eax, %eax
	je	.L116
	movq	8(%r10), %rdi
	movq	%r10, -664(%rbp)
	movl	%r15d, 4(%rdi)
	call	ASN1_TIME_check@PLT
	movq	-664(%rbp), %r10
	testl	%eax, %eax
	jne	.L42
	movl	$657, %r8d
	movl	$184, %edx
	movl	$179, %esi
	movl	$13, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	movq	-664(%rbp), %r10
	.p2align 4,,10
	.p2align 3
.L24:
	movq	%r12, %rdx
	leaq	.LC2(%rip), %rsi
	movl	$2, %edi
	xorl	%eax, %eax
	movq	%r10, -664(%rbp)
	call	ERR_add_error_data@PLT
	movq	-664(%rbp), %r10
.L21:
	movq	%r10, %rdi
	call	ASN1_TYPE_free@PLT
	xorl	%r10d, %r10d
	.p2align 4,,10
	.p2align 3
.L1:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L117
	addq	$648, %rsp
	movq	%r10, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L73:
	.cfi_restore_state
	leaq	.LC0(%rip), %r12
.L33:
	cmpl	$1, %r13d
	je	.L64
	movl	$4096, %r14d
	cmpl	$2, %r13d
	jne	.L118
.L34:
	movl	%r15d, %edi
	movq	%r10, -664(%rbp)
	call	ASN1_tag2bit@PLT
	movl	%r14d, %ecx
	movl	$-1, %edx
	movq	%r12, %rsi
	movq	-664(%rbp), %r10
	movq	%rax, %r8
	leaq	8(%r10), %rdi
	call	ASN1_mbstring_copy@PLT
	movq	-664(%rbp), %r10
	movl	$683, %r8d
	testl	%eax, %eax
	jg	.L42
.L107:
	movl	$65, %edx
	movl	$179, %esi
	movl	$13, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	movq	-664(%rbp), %r10
	jmp	.L24
.L72:
	leaq	.LC0(%rip), %r12
.L27:
	movq	%r10, -664(%rbp)
	cmpl	$1, %r13d
	jne	.L119
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	OBJ_txt2obj@PLT
	movq	-664(%rbp), %r10
	movq	%rax, 8(%r10)
	testq	%rax, %rax
	jne	.L42
	movl	$636, %r8d
	movl	$183, %edx
	movl	$179, %esi
	movl	$13, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	movq	-664(%rbp), %r10
	jmp	.L24
.L71:
	leaq	.LC0(%rip), %r12
.L35:
	movq	%r10, -664(%rbp)
	call	ASN1_STRING_new@PLT
	movq	-664(%rbp), %r10
	movq	%rax, %r8
	movq	%rax, 8(%r10)
	testq	%rax, %rax
	je	.L120
	movq	%r10, -664(%rbp)
	cmpl	$3, %r13d
	je	.L121
	cmpl	$1, %r13d
	je	.L122
	cmpl	$4, %r13d
	jne	.L41
	cmpl	$3, %r15d
	jne	.L41
	leaq	bitstr_cb(%rip), %rcx
	movl	$1, %edx
	movl	$44, %esi
	movq	%r12, %rdi
	call	CONF_parse_list@PLT
	movq	-664(%rbp), %r10
	testl	%eax, %eax
	jne	.L42
	movl	$710, %r8d
	movl	$188, %edx
	movl	$179, %esi
	movl	$13, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	movq	-664(%rbp), %r10
	jmp	.L24
.L70:
	leaq	.LC0(%rip), %r12
.L25:
	movq	%r10, -664(%rbp)
	cmpl	$1, %r13d
	jne	.L123
	movq	%r12, %rsi
	xorl	%edi, %edi
	call	s2i_ASN1_INTEGER@PLT
	movq	-664(%rbp), %r10
	movq	%rax, 8(%r10)
	testq	%rax, %rax
	jne	.L42
	movl	$625, %r8d
	movl	$180, %edx
	movl	$179, %esi
	movl	$13, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	movq	-664(%rbp), %r10
	jmp	.L24
.L69:
	leaq	.LC0(%rip), %r12
.L22:
	cmpl	$1, %r13d
	jne	.L124
	pxor	%xmm0, %xmm0
	leaq	8(%r10), %rsi
	leaq	-608(%rbp), %rdi
	movq	%r10, -664(%rbp)
	movq	%r12, -592(%rbp)
	movaps	%xmm0, -608(%rbp)
	call	X509V3_get_value_bool@PLT
	movq	-664(%rbp), %r10
	testl	%eax, %eax
	jne	.L42
	movl	$612, %r8d
	movl	$176, %edx
	movl	$179, %esi
	movl	$13, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	movq	-664(%rbp), %r10
	jmp	.L24
.L68:
	leaq	.LC0(%rip), %r12
.L43:
	movl	$729, %r8d
	movl	$196, %edx
	movl	$179, %esi
	movl	$13, %edi
	leaq	.LC1(%rip), %rcx
	movq	%r10, -664(%rbp)
	call	ERR_put_error@PLT
	movq	-664(%rbp), %r10
	jmp	.L24
.L121:
	leaq	-616(%rbp), %r14
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	OPENSSL_hexstr2buf@PLT
	movq	-664(%rbp), %r10
	testq	%rax, %rax
	je	.L125
	movq	8(%r10), %rdx
	movq	%rax, 8(%rdx)
	movq	-616(%rbp), %rax
	movl	%r15d, 4(%rdx)
	movl	%eax, (%rdx)
.L39:
	cmpl	$3, %r15d
	jne	.L42
	movq	8(%r10), %rdx
	movq	16(%rdx), %rax
	andq	$-16, %rax
	orq	$8, %rax
	movq	%rax, 16(%rdx)
.L42:
	movl	%r15d, (%r10)
.L44:
	cmpl	$-1, -576(%rbp)
	jne	.L45
	movl	-72(%rbp), %ecx
	testl	%ecx, %ecx
	je	.L1
.L45:
	movq	%r10, %rdi
	leaq	-640(%rbp), %rsi
	movq	%r10, -664(%rbp)
	call	i2d_ASN1_TYPE@PLT
	movq	-664(%rbp), %r10
	movl	%eax, %r15d
	movq	%r10, %rdi
	call	ASN1_TYPE_free@PLT
	movq	-640(%rbp), %rax
	cmpl	$-1, -576(%rbp)
	movq	%rax, -632(%rbp)
	je	.L66
	leaq	-624(%rbp), %rsi
	leaq	-644(%rbp), %rcx
	movslq	%r15d, %r8
	leaq	-648(%rbp), %rdx
	leaq	-632(%rbp), %rdi
	call	ASN1_get_object@PLT
	movl	%eax, %esi
	movl	%eax, %r14d
	andl	$128, %esi
	jne	.L53
	movq	-632(%rbp), %rax
	subq	-640(%rbp), %rax
	subl	%eax, %r15d
	movl	%r15d, -672(%rbp)
	testb	$1, %r14b
	je	.L49
	movq	$0, -624(%rbp)
	movl	$2, -664(%rbp)
.L50:
	movl	-576(%rbp), %edx
	xorl	%edi, %edi
	call	ASN1_object_size@PLT
	movl	%eax, %r15d
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L66:
	movl	$0, -664(%rbp)
	movl	%r15d, -672(%rbp)
.L46:
	movslq	-72(%rbp), %rdx
	movq	%rdx, %rax
	leaq	(%rdx,%rdx,2), %rdx
	leaq	-576(%rbp,%rdx,8), %r12
	testl	%eax, %eax
	jle	.L51
	xorl	%r13d, %r13d
	.p2align 4,,10
	.p2align 3
.L52:
	movl	12(%r12), %esi
	movl	(%r12), %edx
	xorl	%edi, %edi
	addl	$1, %r13d
	subq	$24, %r12
	addl	%r15d, %esi
	movslq	%esi, %rax
	movq	%rax, 40(%r12)
	call	ASN1_object_size@PLT
	movl	%eax, %r15d
	cmpl	%r13d, -72(%rbp)
	jg	.L52
.L51:
	movslq	%r15d, %r13
	movl	$195, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r13, %rdi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L53
	movl	-72(%rbp), %edx
	movq	%rax, -616(%rbp)
	leaq	-552(%rbp), %r15
	leaq	-616(%rbp), %r14
	testl	%edx, %edx
	jle	.L59
	.p2align 4,,10
	.p2align 3
.L54:
	movl	8(%r15), %esi
	movl	4(%r15), %r8d
	movq	%r14, %rdi
	movl	(%r15), %ecx
	movl	16(%r15), %edx
	call	ASN1_put_object@PLT
	movl	12(%r15), %eax
	testl	%eax, %eax
	je	.L57
	movq	-616(%rbp), %rdx
	addl	$1, %ebx
	addq	$24, %r15
	leaq	1(%rdx), %rcx
	movq	%rcx, -616(%rbp)
	movb	$0, (%rdx)
	cmpl	-72(%rbp), %ebx
	jl	.L54
.L59:
	movl	-576(%rbp), %ecx
	cmpl	$-1, %ecx
	je	.L56
	movl	-572(%rbp), %r8d
	testl	%r8d, %r8d
	je	.L126
.L60:
	movl	-624(%rbp), %edx
	movl	-664(%rbp), %esi
	leaq	-616(%rbp), %rdi
	call	ASN1_put_object@PLT
.L56:
	movslq	-672(%rbp), %rdx
	movq	-632(%rbp), %rsi
	movq	-616(%rbp), %rdi
	call	memcpy@PLT
	leaq	-608(%rbp), %rsi
	movq	%r13, %rdx
	xorl	%edi, %edi
	movq	%r12, -608(%rbp)
	call	d2i_ASN1_TYPE@PLT
	movq	%rax, %r10
.L48:
	movq	-640(%rbp), %rdi
	movl	$233, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r10, -664(%rbp)
	call	CRYPTO_free@PLT
	movl	$234, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
	movq	-664(%rbp), %r10
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L57:
	addl	$1, %ebx
	addq	$24, %r15
	cmpl	%ebx, -72(%rbp)
	jg	.L54
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L126:
	leal	-16(%rcx), %eax
	cmpl	$1, %eax
	movl	$32, %eax
	cmova	-664(%rbp), %eax
	movl	%eax, -664(%rbp)
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L53:
	xorl	%r12d, %r12d
	xorl	%r10d, %r10d
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L109:
	movq	-664(%rbp), %rax
	xorl	%r10d, %r10d
	movl	$194, (%rax)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L111:
	movq	-664(%rbp), %rax
	xorl	%r10d, %r10d
	movl	$181, (%rax)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L49:
	andl	$32, %r14d
	movl	-624(%rbp), %esi
	movl	%r14d, -664(%rbp)
	jmp	.L50
.L19:
	cmpb	$0, (%r12)
	je	.L42
	movl	$598, %r8d
	movl	$182, %edx
	movl	$179, %esi
	movl	$13, %edi
	leaq	.LC1(%rip), %rcx
	movq	%r10, -664(%rbp)
	call	ERR_put_error@PLT
	movq	-664(%rbp), %r10
	jmp	.L21
.L115:
	movl	$644, %r8d
	movl	$193, %edx
	movl	$179, %esi
	movl	$13, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	movq	-664(%rbp), %r10
	jmp	.L21
.L119:
	movl	$632, %r8d
	movl	$191, %edx
	movl	$179, %esi
	movl	$13, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	movq	-664(%rbp), %r10
	jmp	.L21
.L124:
	movl	$605, %r8d
	movl	$190, %edx
	movl	$179, %esi
	movl	$13, %edi
	leaq	.LC1(%rip), %rcx
	movq	%r10, -664(%rbp)
	call	ERR_put_error@PLT
	movq	-664(%rbp), %r10
	jmp	.L21
.L123:
	movl	$620, %r8d
	movl	$185, %edx
	movl	$179, %esi
	movl	$13, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	movq	-664(%rbp), %r10
	jmp	.L21
.L64:
	movl	$4097, %r14d
	jmp	.L34
.L120:
	movl	$692, %r8d
	movl	$65, %edx
	movl	$179, %esi
	movl	$13, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	movq	-664(%rbp), %r10
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L112:
	call	i2d_ASN1_SET_ANY@PLT
	movl	%eax, %r12d
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L110:
	movq	-664(%rbp), %rax
	xorl	%r10d, %r10d
	movl	$192, (%rax)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L7:
	xorl	%r14d, %r14d
.L10:
	movq	-608(%rbp), %rdi
	movl	$458, %edx
	leaq	.LC1(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	ASN1_TYPE_free@GOTPCREL(%rip), %rsi
	movq	-672(%rbp), %rdi
	call	OPENSSL_sk_pop_free@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	X509V3_section_free@PLT
	xorl	%r10d, %r10d
	jmp	.L1
.L14:
	movq	-608(%rbp), %rdi
	jmp	.L15
.L113:
	movl	$587, %r8d
	movl	$65, %edx
	movl	$179, %esi
	movl	$13, %edi
	leaq	.LC1(%rip), %rcx
	movq	%rax, -664(%rbp)
	call	ERR_put_error@PLT
	movq	-664(%rbp), %r10
	jmp	.L1
.L122:
	movl	$-1, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	ASN1_STRING_set@PLT
	movq	-664(%rbp), %r10
	jmp	.L39
.L118:
	movl	$677, %r8d
	movl	$177, %edx
	movl	$179, %esi
	movl	$13, %edi
	leaq	.LC1(%rip), %rcx
	movq	%r10, -664(%rbp)
	call	ERR_put_error@PLT
	movq	-664(%rbp), %r10
	jmp	.L21
.L116:
	movl	$652, %r8d
	jmp	.L107
.L41:
	movl	$716, %r8d
	movl	$175, %edx
	movl	$179, %esi
	movl	$13, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	movq	-664(%rbp), %r10
	jmp	.L21
.L125:
	movl	$698, %r8d
	movl	$178, %edx
	movl	$179, %esi
	movl	$13, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	movq	-664(%rbp), %r10
	jmp	.L24
.L117:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1298:
	.size	generate_v3, .-generate_v3
	.section	.rodata.str1.1
.LC3:
	.string	"Char="
	.text
	.p2align 4
	.type	parse_tagging.part.0, @function
parse_tagging.part.0:
.LFB1309:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdx, %r13
	movl	$10, %edx
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	leaq	-56(%rbp), %rsi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	strtoul@PLT
	movq	-56(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L128
	cmpb	$0, (%rdx)
	jne	.L146
.L129:
	testq	%rax, %rax
	js	.L137
	movl	%eax, 0(%r13)
	movq	%rdx, %rax
	subq	%rbx, %rax
	cmpl	%eax, %r12d
	je	.L136
	movzbl	(%rdx), %eax
	cmpb	$80, %al
	je	.L131
	jg	.L132
	cmpb	$65, %al
	je	.L133
	cmpb	$67, %al
	jne	.L135
.L136:
	movl	$128, (%r14)
	movl	$1, %r8d
.L127:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L147
	addq	$32, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L146:
	.cfi_restore_state
	movslq	%r12d, %rcx
	xorl	%r8d, %r8d
	addq	%rbx, %rcx
	cmpq	%rcx, %rdx
	jbe	.L129
	jmp	.L127
	.p2align 4,,10
	.p2align 3
.L132:
	cmpb	$85, %al
	jne	.L135
	movl	$0, (%r14)
	movl	$1, %r8d
	jmp	.L127
	.p2align 4,,10
	.p2align 3
.L128:
	testq	%rax, %rax
	js	.L137
	movl	%eax, 0(%r13)
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L137:
	movl	$360, %r8d
	movl	$187, %edx
	movl	$182, %esi
	movl	$13, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%r8d, %r8d
	jmp	.L127
	.p2align 4,,10
	.p2align 3
.L135:
	movl	$391, %r8d
	leaq	.LC1(%rip), %rcx
	movl	$186, %edx
	movb	%al, -42(%rbp)
	movl	$182, %esi
	movl	$13, %edi
	movb	$0, -41(%rbp)
	call	ERR_put_error@PLT
	leaq	-42(%rbp), %rdx
	movl	$2, %edi
	xorl	%eax, %eax
	leaq	.LC3(%rip), %rsi
	call	ERR_add_error_data@PLT
	xorl	%r8d, %r8d
	jmp	.L127
	.p2align 4,,10
	.p2align 3
.L133:
	movl	$64, (%r14)
	movl	$1, %r8d
	jmp	.L127
	.p2align 4,,10
	.p2align 3
.L131:
	movl	$192, (%r14)
	movl	$1, %r8d
	jmp	.L127
.L147:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1309:
	.size	parse_tagging.part.0, .-parse_tagging.part.0
	.p2align 4
	.type	bitstr_cb, @function
bitstr_cb:
.LFB1305:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L160
	movslq	%esi, %r12
	movq	%rdx, %r13
	leaq	-48(%rbp), %rsi
	movl	$10, %edx
	movq	%rdi, %rbx
	call	strtoul@PLT
	movq	%rax, %rsi
	movq	-48(%rbp), %rax
	testq	%rax, %rax
	je	.L151
	cmpb	$0, (%rax)
	je	.L151
	addq	%r12, %rbx
	cmpq	%rbx, %rax
	je	.L151
.L160:
	xorl	%eax, %eax
.L148:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L161
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L151:
	.cfi_restore_state
	testq	%rsi, %rsi
	js	.L162
	movl	$1, %edx
	movq	%r13, %rdi
	call	ASN1_BIT_STRING_set_bit@PLT
	testl	%eax, %eax
	je	.L163
	movl	$1, %eax
	jmp	.L148
	.p2align 4,,10
	.p2align 3
.L162:
	movl	$755, %r8d
	movl	$187, %edx
	movl	$180, %esi
	movl	$13, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L160
	.p2align 4,,10
	.p2align 3
.L163:
	movl	$759, %r8d
	leaq	.LC1(%rip), %rcx
	movl	$65, %edx
	movl	%eax, -52(%rbp)
	movl	$180, %esi
	movl	$13, %edi
	call	ERR_put_error@PLT
	movl	-52(%rbp), %eax
	jmp	.L148
.L161:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1305:
	.size	bitstr_cb, .-bitstr_cb
	.p2align 4
	.type	mask_cb, @function
mask_cb:
.LFB1306:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L184
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	cmpl	$3, %esi
	jne	.L167
	cmpb	$68, (%rdi)
	jne	.L169
	cmpb	$73, 1(%rdi)
	jne	.L169
	cmpb	$82, 2(%rdi)
	jne	.L169
	orq	$10502, (%rdx)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L167:
	.cfi_restore_state
	cmpl	$-1, %esi
	jne	.L169
	call	strlen@PLT
	movl	%eax, %r12d
.L169:
	leaq	tnst.20314(%rip), %rbx
	movl	$4, %eax
	movslq	%r12d, %r15
	movq	%rbx, tntmp.20313(%rip)
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L170:
	addq	$16, %rbx
	leaq	784+tnst.20314(%rip), %rax
	movq	%rbx, tntmp.20313(%rip)
	cmpq	%rax, %rbx
	je	.L171
	movl	8(%rbx), %eax
.L173:
	cmpl	%eax, %r12d
	jne	.L170
	movq	(%rbx), %rdi
	movq	%r15, %rdx
	movq	%r14, %rsi
	call	strncmp@PLT
	testl	%eax, %eax
	jne	.L170
	movl	12(%rbx), %edi
	testl	%edi, %edi
	jne	.L185
.L171:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L185:
	.cfi_restore_state
	testl	$65536, %edi
	jne	.L171
	call	ASN1_tag2bit@PLT
	testq	%rax, %rax
	je	.L171
	orq	%rax, 0(%r13)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L184:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1306:
	.size	mask_cb, .-mask_cb
	.section	.rodata.str1.1
.LC4:
	.string	"tag="
.LC5:
	.string	"ASCII"
.LC6:
	.string	"UTF8"
.LC7:
	.string	"BITLIST"
	.text
	.p2align 4
	.type	asn1_cb, @function
asn1_cb:
.LFB1299:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -72(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L223
	movq	%rdi, %r13
	movl	%esi, %ebx
	testl	%esi, %esi
	jle	.L224
	leal	-1(%rsi), %eax
	movq	%rdi, %r15
	leaq	1(%rdi,%rax), %rdx
	jmp	.L190
	.p2align 4,,10
	.p2align 3
.L189:
	cmpq	%r15, %rdx
	je	.L260
.L190:
	movzbl	(%r15), %eax
	movq	%r15, %r12
	addq	$1, %r15
	cmpb	$58, %al
	jne	.L189
	movq	%r15, %rax
	subq	%r13, %rax
	subl	%eax, %ebx
	movl	%r12d, %eax
	subl	%r13d, %eax
	movl	%ebx, -80(%rbp)
	movl	%eax, -76(%rbp)
.L188:
	movl	%eax, %ebx
	cmpl	$-1, %eax
	jne	.L191
	movq	%r13, %rdi
	call	strlen@PLT
	movl	%eax, %ebx
.L191:
	leaq	tnst.20314(%rip), %rcx
	movl	$4, %eax
	movslq	%ebx, %r14
	movq	%rcx, tntmp.20313(%rip)
	movq	%rcx, %r12
	jmp	.L195
	.p2align 4,,10
	.p2align 3
.L192:
	addq	$16, %r12
	leaq	784+tnst.20314(%rip), %rax
	movq	%r12, tntmp.20313(%rip)
	cmpq	%rax, %r12
	je	.L193
	movl	8(%r12), %eax
.L195:
	cmpl	%eax, %ebx
	jne	.L192
	movq	(%r12), %rdi
	movq	%r14, %rdx
	movq	%r13, %rsi
	call	strncmp@PLT
	testl	%eax, %eax
	jne	.L192
	movl	12(%r12), %edx
	cmpl	$-1, %edx
	je	.L193
	movl	%edx, %r10d
	andl	$65536, %r10d
	jne	.L197
	movq	-72(%rbp), %rax
	movl	%edx, 8(%rax)
	movq	%r15, 16(%rax)
	testq	%r15, %r15
	je	.L261
.L186:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L262
	addq	$40, %rsp
	movl	%r10d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L197:
	.cfi_restore_state
	subl	$65537, %edx
	cmpl	$7, %edx
	ja	.L226
	leaq	.L199(%rip), %rcx
	movslq	(%rcx,%rdx,4), %rdx
	addq	%rcx, %rdx
	notrack jmp	*%rdx
	.section	.rodata
	.align 4
	.align 4
.L199:
	.long	.L205-.L199
	.long	.L204-.L199
	.long	.L226-.L199
	.long	.L203-.L199
	.long	.L202-.L199
	.long	.L201-.L199
	.long	.L200-.L199
	.long	.L198-.L199
	.text
.L205:
	movq	-72(%rbp), %rax
	cmpl	$-1, (%rax)
	jne	.L263
	testq	%r15, %r15
	je	.L223
	movl	-80(%rbp), %esi
	movq	%rax, %rdx
	leaq	4(%rax), %rcx
	movq	%r15, %rdi
	call	parse_tagging.part.0
	testl	%eax, %eax
	je	.L223
.L226:
	movl	$1, %r10d
	jmp	.L186
	.p2align 4,,10
	.p2align 3
.L193:
	movl	$266, %r8d
	movl	$194, %edx
	movl	$177, %esi
	movl	$13, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	movq	%r13, %rdx
	movl	$2, %edi
	xorl	%eax, %eax
	leaq	.LC4(%rip), %rsi
	call	ERR_add_error_data@PLT
	movl	$-1, %r10d
	jmp	.L186
	.p2align 4,,10
	.p2align 3
.L260:
	movl	%ebx, -76(%rbp)
	xorl	%r15d, %r15d
	movl	$0, -80(%rbp)
	jmp	.L191
	.p2align 4,,10
	.p2align 3
.L223:
	movl	$-1, %r10d
	jmp	.L186
	.p2align 4,,10
	.p2align 3
.L261:
	movslq	-76(%rbp), %r12
	cmpb	$0, 0(%r13,%r12)
	je	.L186
	movl	$277, %r8d
	movl	$189, %edx
	movl	$177, %esi
	movl	$13, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-1, %r10d
	jmp	.L186
.L198:
	movl	$325, %r8d
	testq	%r15, %r15
	je	.L258
	movl	$5, %ecx
	leaq	.LC5(%rip), %rdi
	movq	%r15, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L217
	movq	-72(%rbp), %rax
	movl	$1, %r10d
	movl	$1, 12(%rax)
	jmp	.L186
.L204:
	testq	%r15, %r15
	je	.L223
	movl	-80(%rbp), %esi
	leaq	-60(%rbp), %rcx
	leaq	-64(%rbp), %rdx
	movq	%r15, %rdi
	call	parse_tagging.part.0
	testl	%eax, %eax
	je	.L223
	movq	-72(%rbp), %rax
	cmpl	$-1, (%rax)
	je	.L209
	movl	$472, %r8d
	movl	$179, %edx
	movl	$176, %esi
	movl	$13, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-1, %r10d
	jmp	.L186
.L203:
	movq	-72(%rbp), %rdi
	movslq	504(%rdi), %rdx
	movl	(%rdi), %ecx
	cmpl	$20, %edx
	je	.L211
	leal	1(%rdx), %esi
	movl	%esi, 504(%rdi)
	cmpl	$-1, %ecx
	je	.L229
	movl	4(%rdi), %eax
	movq	$-1, (%rdi)
	movq	%rdi, %rsi
.L214:
	leaq	(%rdx,%rdx,2), %rdx
	movl	$1, %r10d
	leaq	(%rsi,%rdx,8), %rdx
	movl	%eax, 28(%rdx)
	movabsq	$4294967296, %rax
	movl	%ecx, 24(%rdx)
	movq	%rax, 32(%rdx)
	jmp	.L186
.L201:
	movq	-72(%rbp), %rdi
	movslq	504(%rdi), %rdx
	movl	(%rdi), %ecx
	cmpl	$20, %edx
	je	.L211
	leal	1(%rdx), %esi
	movl	%esi, 504(%rdi)
	cmpl	$-1, %ecx
	je	.L227
.L259:
	movl	4(%rdi), %eax
	movq	$-1, (%rdi)
	movq	%rdi, %rsi
.L213:
	leaq	(%rdx,%rdx,2), %rdx
	movl	$1, %r10d
	leaq	(%rsi,%rdx,8), %rdx
	movl	%eax, 28(%rdx)
	movl	%ecx, 24(%rdx)
	movq	$1, 32(%rdx)
	jmp	.L186
.L200:
	movq	-72(%rbp), %rdi
	movslq	504(%rdi), %rdx
	movl	(%rdi), %ecx
	cmpl	$20, %edx
	je	.L211
	leal	1(%rdx), %esi
	movl	%esi, 504(%rdi)
	cmpl	$-1, %ecx
	jne	.L259
	movl	$17, %ecx
	movq	%rdi, %rsi
	jmp	.L213
.L202:
	movq	-72(%rbp), %rdi
	movslq	504(%rdi), %rdx
	movl	(%rdi), %ecx
	cmpl	$20, %edx
	je	.L211
	leal	1(%rdx), %esi
	movl	%esi, 504(%rdi)
	cmpl	$-1, %ecx
	je	.L230
	movl	4(%rdi), %eax
	movq	$-1, (%rdi)
	movq	%rdi, %rsi
.L215:
	leaq	(%rdx,%rdx,2), %rdx
	movl	$1, %r10d
	leaq	(%rsi,%rdx,8), %rdx
	movl	%eax, 28(%rdx)
	movl	%ecx, 24(%rdx)
	movq	$0, 32(%rdx)
	jmp	.L186
.L224:
	movl	%esi, -76(%rbp)
	xorl	%r15d, %r15d
	movl	%esi, %eax
	movl	$0, -80(%rbp)
	jmp	.L188
.L211:
	movl	$477, %r8d
	movl	$174, %edx
	movl	$176, %esi
	movl	$13, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-1, %r10d
	jmp	.L186
.L217:
	movl	$4, %ecx
	leaq	.LC6(%rip), %rdi
	movq	%r15, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L218
	movq	-72(%rbp), %rax
	movl	$1, %r10d
	movl	$2, 12(%rax)
	jmp	.L186
.L263:
	movl	$288, %r8d
	movl	$181, %edx
	movl	$177, %esi
	movl	$13, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-1, %r10d
	jmp	.L186
.L230:
	movl	$4, %ecx
	movq	%rdi, %rsi
	jmp	.L215
.L229:
	movl	$3, %ecx
	movq	%rdi, %rsi
	jmp	.L214
.L227:
	movl	$16, %ecx
	movq	%rdi, %rsi
	jmp	.L213
.L218:
	cmpb	$72, (%r15)
	jne	.L220
	cmpb	$69, 1(%r15)
	jne	.L220
	cmpb	$88, 2(%r15)
	jne	.L220
	movq	-72(%rbp), %rax
	movl	$1, %r10d
	movl	$3, 12(%rax)
	jmp	.L186
.L209:
	movslq	504(%rax), %rax
	cmpl	$20, %eax
	je	.L211
	movq	-72(%rbp), %rsi
	leal	1(%rax), %edx
	leaq	(%rax,%rax,2), %rax
	movl	$1, %r10d
	movl	%edx, 504(%rsi)
	movl	-64(%rbp), %edx
	leaq	(%rsi,%rax,8), %rax
	movq	$1, 32(%rax)
	movl	%edx, 24(%rax)
	movl	-60(%rbp), %edx
	movl	%edx, 28(%rax)
	jmp	.L186
.L220:
	movl	$7, %ecx
	leaq	.LC7(%rip), %rdi
	movq	%r15, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L221
	movq	-72(%rbp), %rax
	movl	$1, %r10d
	movl	$4, 12(%rax)
	jmp	.L186
.L221:
	movl	$337, %r8d
.L258:
	movl	$160, %edx
	movl	$177, %esi
	movl	$13, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-1, %r10d
	jmp	.L186
.L262:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1299:
	.size	asn1_cb, .-asn1_cb
	.p2align 4
	.globl	ASN1_generate_nconf
	.type	ASN1_generate_nconf, @function
ASN1_generate_nconf:
.LFB1296:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$96, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L278
	leaq	-80(%rbp), %r13
	movq	%r13, %rdi
	call	X509V3_set_nconf@PLT
	xorl	%edx, %edx
	leaq	-84(%rbp), %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$0, -84(%rbp)
	call	generate_v3
	movl	-84(%rbp), %edx
	testl	%edx, %edx
	jne	.L277
.L264:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L279
	addq	$96, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L278:
	.cfi_restore_state
	xorl	%edx, %edx
	leaq	-80(%rbp), %rcx
	movl	$0, -80(%rbp)
	call	generate_v3
	movl	-80(%rbp), %edx
	testl	%edx, %edx
	je	.L264
	.p2align 4,,10
	.p2align 3
.L277:
	movl	$94, %r8d
	leaq	.LC1(%rip), %rcx
	movl	$178, %esi
	movl	$13, %edi
	movq	%rax, -104(%rbp)
	call	ERR_put_error@PLT
	movq	-104(%rbp), %rax
	jmp	.L264
.L279:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1296:
	.size	ASN1_generate_nconf, .-ASN1_generate_nconf
	.p2align 4
	.globl	ASN1_generate_v3
	.type	ASN1_generate_v3, @function
ASN1_generate_v3:
.LFB1297:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-12(%rbp), %rcx
	movl	$0, -12(%rbp)
	call	generate_v3
	movl	-12(%rbp), %edx
	testl	%edx, %edx
	jne	.L287
.L280:
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L288
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L287:
	.cfi_restore_state
	movl	$94, %r8d
	leaq	.LC1(%rip), %rcx
	movl	$178, %esi
	movl	$13, %edi
	movq	%rax, -24(%rbp)
	call	ERR_put_error@PLT
	movq	-24(%rbp), %rax
	jmp	.L280
.L288:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1297:
	.size	ASN1_generate_v3, .-ASN1_generate_v3
	.p2align 4
	.globl	ASN1_str2mask
	.type	ASN1_str2mask, @function
ASN1_str2mask:
.LFB1307:
	.cfi_startproc
	endbr64
	movq	$0, (%rsi)
	movq	%rsi, %r8
	movl	$1, %edx
	leaq	mask_cb(%rip), %rcx
	movl	$124, %esi
	jmp	CONF_parse_list@PLT
	.cfi_endproc
.LFE1307:
	.size	ASN1_str2mask, .-ASN1_str2mask
	.section	.rodata.str1.1
.LC8:
	.string	"BOOL"
.LC9:
	.string	"BOOLEAN"
.LC10:
	.string	"NULL"
.LC11:
	.string	"INT"
.LC12:
	.string	"INTEGER"
.LC13:
	.string	"ENUM"
.LC14:
	.string	"ENUMERATED"
.LC15:
	.string	"OID"
.LC16:
	.string	"OBJECT"
.LC17:
	.string	"UTCTIME"
.LC18:
	.string	"UTC"
.LC19:
	.string	"GENERALIZEDTIME"
.LC20:
	.string	"GENTIME"
.LC21:
	.string	"OCT"
.LC22:
	.string	"OCTETSTRING"
.LC23:
	.string	"BITSTR"
.LC24:
	.string	"BITSTRING"
.LC25:
	.string	"UNIVERSALSTRING"
.LC26:
	.string	"UNIV"
.LC27:
	.string	"IA5"
.LC28:
	.string	"IA5STRING"
.LC29:
	.string	"UTF8String"
.LC30:
	.string	"BMP"
.LC31:
	.string	"BMPSTRING"
.LC32:
	.string	"VISIBLESTRING"
.LC33:
	.string	"VISIBLE"
.LC34:
	.string	"PRINTABLESTRING"
.LC35:
	.string	"PRINTABLE"
.LC36:
	.string	"T61"
.LC37:
	.string	"T61STRING"
.LC38:
	.string	"TELETEXSTRING"
.LC39:
	.string	"GeneralString"
.LC40:
	.string	"GENSTR"
.LC41:
	.string	"NUMERIC"
.LC42:
	.string	"NUMERICSTRING"
.LC43:
	.string	"SEQUENCE"
.LC44:
	.string	"SEQ"
.LC45:
	.string	"SET"
.LC46:
	.string	"EXP"
.LC47:
	.string	"EXPLICIT"
.LC48:
	.string	"IMP"
.LC49:
	.string	"IMPLICIT"
.LC50:
	.string	"OCTWRAP"
.LC51:
	.string	"SEQWRAP"
.LC52:
	.string	"SETWRAP"
.LC53:
	.string	"BITWRAP"
.LC54:
	.string	"FORM"
.LC55:
	.string	"FORMAT"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	tnst.20314, @object
	.size	tnst.20314, 784
tnst.20314:
	.quad	.LC8
	.long	4
	.long	1
	.quad	.LC9
	.long	7
	.long	1
	.quad	.LC10
	.long	4
	.long	5
	.quad	.LC11
	.long	3
	.long	2
	.quad	.LC12
	.long	7
	.long	2
	.quad	.LC13
	.long	4
	.long	10
	.quad	.LC14
	.long	10
	.long	10
	.quad	.LC15
	.long	3
	.long	6
	.quad	.LC16
	.long	6
	.long	6
	.quad	.LC17
	.long	7
	.long	23
	.quad	.LC18
	.long	3
	.long	23
	.quad	.LC19
	.long	15
	.long	24
	.quad	.LC20
	.long	7
	.long	24
	.quad	.LC21
	.long	3
	.long	4
	.quad	.LC22
	.long	11
	.long	4
	.quad	.LC23
	.long	6
	.long	3
	.quad	.LC24
	.long	9
	.long	3
	.quad	.LC25
	.long	15
	.long	28
	.quad	.LC26
	.long	4
	.long	28
	.quad	.LC27
	.long	3
	.long	22
	.quad	.LC28
	.long	9
	.long	22
	.quad	.LC6
	.long	4
	.long	12
	.quad	.LC29
	.long	10
	.long	12
	.quad	.LC30
	.long	3
	.long	30
	.quad	.LC31
	.long	9
	.long	30
	.quad	.LC32
	.long	13
	.long	26
	.quad	.LC33
	.long	7
	.long	26
	.quad	.LC34
	.long	15
	.long	19
	.quad	.LC35
	.long	9
	.long	19
	.quad	.LC36
	.long	3
	.long	20
	.quad	.LC37
	.long	9
	.long	20
	.quad	.LC38
	.long	13
	.long	20
	.quad	.LC39
	.long	13
	.long	27
	.quad	.LC40
	.long	6
	.long	27
	.quad	.LC41
	.long	7
	.long	18
	.quad	.LC42
	.long	13
	.long	18
	.quad	.LC43
	.long	8
	.long	16
	.quad	.LC44
	.long	3
	.long	16
	.quad	.LC45
	.long	3
	.long	17
	.quad	.LC46
	.long	3
	.long	65538
	.quad	.LC47
	.long	8
	.long	65538
	.quad	.LC48
	.long	3
	.long	65537
	.quad	.LC49
	.long	8
	.long	65537
	.quad	.LC50
	.long	7
	.long	65541
	.quad	.LC51
	.long	7
	.long	65542
	.quad	.LC52
	.long	7
	.long	65543
	.quad	.LC53
	.long	7
	.long	65540
	.quad	.LC54
	.long	4
	.long	65544
	.quad	.LC55
	.long	6
	.long	65544
	.local	tntmp.20313
	.comm	tntmp.20313,8,8
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
