	.file	"conf_api.c"
	.text
	.p2align 4
	.type	conf_value_cmp, @function
conf_value_cmp:
.LFB352:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	movq	(%rdi), %rdi
	movq	(%rsi), %rsi
	cmpq	%rsi, %rdi
	je	.L2
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L1
.L2:
	movq	8(%r12), %rdi
	movq	8(%rbx), %rsi
	testq	%rdi, %rdi
	je	.L4
	testq	%rsi, %rsi
	je	.L5
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	strcmp@PLT
	.p2align 4,,10
	.p2align 3
.L5:
	.cfi_restore_state
	movl	$1, %eax
.L1:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	.cfi_restore_state
	xorl	%eax, %eax
	testq	%rsi, %rsi
	popq	%rbx
	popq	%r12
	setne	%al
	popq	%rbp
	.cfi_def_cfa 7, 8
	negl	%eax
	ret
	.cfi_endproc
.LFE352:
	.size	conf_value_cmp, .-conf_value_cmp
	.p2align 4
	.type	conf_value_hash, @function
conf_value_hash:
.LFB351:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	(%rdi), %rdi
	call	OPENSSL_LH_strhash@PLT
	movq	8(%r12), %rdi
	movq	%rax, %rbx
	call	OPENSSL_LH_strhash@PLT
	movq	%rax, %r8
	leaq	0(,%rbx,4), %rax
	popq	%rbx
	popq	%r12
	xorq	%r8, %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE351:
	.size	conf_value_hash, .-conf_value_hash
	.p2align 4
	.type	value_free_hash, @function
value_free_hash:
.LFB356:
	.cfi_startproc
	endbr64
	movq	%rdi, %r8
	movq	%rsi, %rdi
	cmpq	$0, 8(%r8)
	je	.L10
	movq	%r8, %rsi
	jmp	OPENSSL_LH_delete@PLT
	.p2align 4,,10
	.p2align 3
.L10:
	ret
	.cfi_endproc
.LFE356:
	.size	value_free_hash, .-value_free_hash
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/conf/conf_api.c"
	.text
	.p2align 4
	.type	value_free_stack_doall, @function
value_free_stack_doall:
.LFB357:
	.cfi_startproc
	endbr64
	cmpq	$0, 8(%rdi)
	je	.L20
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r15
	movq	%r15, %rdi
	call	OPENSSL_sk_num@PLT
	subl	$1, %eax
	movl	%eax, %ebx
	js	.L14
	leaq	.LC0(%rip), %r14
	.p2align 4,,10
	.p2align 3
.L15:
	movl	%ebx, %esi
	movq	%r15, %rdi
	subl	$1, %ebx
	call	OPENSSL_sk_value@PLT
	movl	$179, %edx
	movq	%r14, %rsi
	movq	16(%rax), %rdi
	movq	%rax, %r12
	call	CRYPTO_free@PLT
	movq	8(%r12), %rdi
	movl	$180, %edx
	movq	%r14, %rsi
	call	CRYPTO_free@PLT
	movl	$181, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
	cmpl	$-1, %ebx
	jne	.L15
.L14:
	movq	%r15, %rdi
	call	OPENSSL_sk_free@PLT
	movq	0(%r13), %rdi
	movl	$184, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	addq	$8, %rsp
	movq	%r13, %rdi
	movl	$185, %edx
	popq	%rbx
	.cfi_restore 3
	leaq	.LC0(%rip), %rsi
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_free@PLT
	.cfi_endproc
.LFE357:
	.size	value_free_stack_doall, .-value_free_stack_doall
	.p2align 4
	.globl	_CONF_get_section
	.type	_CONF_get_section, @function
_CONF_get_section:
.LFB347:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L24
	testq	%rsi, %rsi
	je	.L24
	movq	16(%rdi), %rdi
	movq	%rsi, -32(%rbp)
	leaq	-32(%rbp), %rsi
	movq	$0, -24(%rbp)
	call	OPENSSL_LH_retrieve@PLT
.L21:
	movq	-8(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L26
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	.cfi_restore_state
	xorl	%eax, %eax
	jmp	.L21
.L26:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE347:
	.size	_CONF_get_section, .-_CONF_get_section
	.p2align 4
	.globl	_CONF_get_section_values
	.type	_CONF_get_section_values, @function
_CONF_get_section_values:
.LFB348:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L32
	testq	%rsi, %rsi
	je	.L32
	movq	16(%rdi), %rdi
	movq	%rsi, -32(%rbp)
	leaq	-32(%rbp), %rsi
	movq	$0, -24(%rbp)
	call	OPENSSL_LH_retrieve@PLT
	testq	%rax, %rax
	je	.L27
	movq	16(%rax), %rax
.L27:
	movq	-8(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L37
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	.cfi_restore_state
	xorl	%eax, %eax
	jmp	.L27
.L37:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE348:
	.size	_CONF_get_section_values, .-_CONF_get_section_values
	.p2align 4
	.globl	_CONF_add_string
	.type	_CONF_add_string, @function
_CONF_add_string:
.LFB349:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	(%rsi), %rax
	movq	%rdi, %rbx
	movq	16(%rsi), %r14
	movq	%rdx, %rsi
	movq	%rax, (%rdx)
	movq	%r14, %rdi
	call	OPENSSL_sk_push@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	jne	.L46
.L38:
	popq	%rbx
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L46:
	.cfi_restore_state
	movq	16(%rbx), %rdi
	movq	%r13, %rsi
	movl	$1, %r12d
	call	OPENSSL_LH_insert@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L38
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	OPENSSL_sk_delete_ptr@PLT
	movq	8(%r13), %rdi
	movl	$63, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	16(%r13), %rdi
	movl	$64, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%r13, %rdi
	movl	$65, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	popq	%rbx
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE349:
	.size	_CONF_add_string, .-_CONF_add_string
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"ENV"
.LC2:
	.string	"default"
	.text
	.p2align 4
	.globl	_CONF_get_string
	.type	_CONF_get_string, @function
_CONF_get_string:
.LFB350:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L54
	movq	%rdi, %r13
	movq	%rdx, %r12
	testq	%rdi, %rdi
	je	.L50
	movq	16(%rdi), %rdi
	movq	%rsi, %rbx
	leaq	-64(%rbp), %r14
	testq	%rsi, %rsi
	je	.L51
	movq	%rsi, %xmm0
	movq	%rdx, %xmm1
	leaq	-64(%rbp), %r14
	punpcklqdq	%xmm1, %xmm0
	movq	%r14, %rsi
	movaps	%xmm0, -64(%rbp)
	call	OPENSSL_LH_retrieve@PLT
	testq	%rax, %rax
	je	.L52
.L64:
	movq	16(%rax), %rax
.L47:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L65
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L52:
	.cfi_restore_state
	movl	$4, %ecx
	leaq	.LC1(%rip), %rdi
	movq	%rbx, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L53
.L63:
	movq	16(%r13), %rdi
.L51:
	leaq	.LC2(%rip), %rax
	movq	%r14, %rsi
	movq	%r12, -56(%rbp)
	movq	%rax, -64(%rbp)
	call	OPENSSL_LH_retrieve@PLT
	testq	%rax, %rax
	jne	.L64
.L54:
	xorl	%eax, %eax
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L53:
	movq	%r12, %rdi
	call	ossl_safe_getenv@PLT
	testq	%rax, %rax
	jne	.L47
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L50:
	movq	%rdx, %rdi
	call	ossl_safe_getenv@PLT
	jmp	.L47
.L65:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE350:
	.size	_CONF_get_string, .-_CONF_get_string
	.p2align 4
	.globl	_CONF_new_data
	.type	_CONF_new_data, @function
_CONF_new_data:
.LFB353:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L68
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpq	$0, 16(%rdi)
	je	.L74
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L68:
	.cfi_restore 3
	.cfi_restore 6
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L74:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	leaq	conf_value_cmp(%rip), %rsi
	leaq	conf_value_hash(%rip), %rdi
	call	OPENSSL_LH_new@PLT
	testq	%rax, %rax
	movq	%rax, 16(%rbx)
	setne	%al
	addq	$8, %rsp
	movzbl	%al, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE353:
	.size	_CONF_new_data, .-_CONF_new_data
	.p2align 4
	.globl	_CONF_free_data
	.type	_CONF_free_data, @function
_CONF_free_data:
.LFB355:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L84
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L75
	xorl	%esi, %esi
	call	OPENSSL_LH_set_down_load@PLT
	movq	16(%rbx), %rdi
	leaq	value_free_hash(%rip), %rsi
	movq	%rdi, %rdx
	call	OPENSSL_LH_doall_arg@PLT
	movq	16(%rbx), %rdi
	leaq	value_free_stack_doall(%rip), %rsi
	call	OPENSSL_LH_doall@PLT
	movq	16(%rbx), %rdi
	addq	$8, %rsp
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	OPENSSL_LH_free@PLT
	.p2align 4,,10
	.p2align 3
.L75:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L84:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.cfi_endproc
.LFE355:
	.size	_CONF_free_data, .-_CONF_free_data
	.p2align 4
	.globl	_CONF_new_section
	.type	_CONF_new_section, @function
_CONF_new_section:
.LFB358:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	OPENSSL_sk_new_null@PLT
	testq	%rax, %rax
	je	.L97
	movl	$197, %edx
	leaq	.LC0(%rip), %rsi
	movl	$24, %edi
	movq	%rax, %r13
	call	CRYPTO_malloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L98
	movq	%r15, %rdi
	call	strlen@PLT
	movl	$200, %edx
	leaq	.LC0(%rip), %rsi
	leal	1(%rax), %r14d
	movslq	%r14d, %r14
	movq	%r14, %rdi
	call	CRYPTO_malloc@PLT
	movq	%rax, (%r12)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L91
	movq	%r15, %rsi
	movq	%r14, %rdx
	call	memcpy@PLT
	movq	%r13, 16(%r12)
	movq	16(%rbx), %rdi
	movq	%r12, %rsi
	movq	$0, 8(%r12)
	call	OPENSSL_LH_insert@PLT
	testq	%rax, %rax
	jne	.L91
	movq	16(%rbx), %rdi
	call	OPENSSL_LH_error@PLT
	testl	%eax, %eax
	jle	.L87
	.p2align 4,,10
	.p2align 3
.L91:
	movq	%r13, %rdi
	call	OPENSSL_sk_free@PLT
	movq	(%r12), %rdi
	movl	$215, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
.L89:
	movq	%r12, %rdi
	movl	$216, %edx
	leaq	.LC0(%rip), %rsi
	xorl	%r12d, %r12d
	call	CRYPTO_free@PLT
.L87:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L97:
	.cfi_restore_state
	xorl	%edi, %edi
	xorl	%r12d, %r12d
	call	OPENSSL_sk_free@PLT
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L98:
	movq	%r13, %rdi
	call	OPENSSL_sk_free@PLT
	jmp	.L89
	.cfi_endproc
.LFE358:
	.size	_CONF_new_section, .-_CONF_new_section
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
