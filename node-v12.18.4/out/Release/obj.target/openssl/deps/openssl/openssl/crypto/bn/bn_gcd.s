	.file	"bn_gcd.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/bn/bn_gcd.c"
	.text
	.p2align 4
	.type	int_bn_mod_inverse.part.0, @function
int_bn_mod_inverse.part.0:
.LFB256:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	movl	$4, %esi
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -112(%rbp)
	movq	%r15, %rdi
	movq	%rdx, -128(%rbp)
	movq	%r8, -120(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	BN_get_flags@PLT
	testl	%eax, %eax
	jne	.L5
	movq	-128(%rbp), %rdi
	movl	$4, %esi
	call	BN_get_flags@PLT
	testl	%eax, %eax
	je	.L273
.L5:
	movq	%r13, %rdi
	call	BN_CTX_start@PLT
	movq	%r13, %rdi
	call	BN_CTX_get@PLT
	movq	%r13, %rdi
	movq	%rax, %r12
	call	BN_CTX_get@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	BN_CTX_get@PLT
	movq	%r13, %rdi
	movq	%rax, -88(%rbp)
	call	BN_CTX_get@PLT
	movq	%r13, %rdi
	movq	%rax, -120(%rbp)
	call	BN_CTX_get@PLT
	movq	%r13, %rdi
	movq	%rax, -104(%rbp)
	call	BN_CTX_get@PLT
	movq	%r13, %rdi
	movq	%rax, %r14
	call	BN_CTX_get@PLT
	movq	%rax, -96(%rbp)
	testq	%rax, %rax
	je	.L25
	cmpq	$0, -112(%rbp)
	je	.L274
	movq	-88(%rbp), %rdi
	movl	$1, %esi
	call	BN_set_word@PLT
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	BN_set_word@PLT
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	BN_copy@PLT
	testq	%rax, %rax
	je	.L269
	movq	-128(%rbp), %rsi
	movq	%r12, %rdi
	call	BN_copy@PLT
	testq	%rax, %rax
	je	.L269
	movq	-112(%rbp), %rax
	movq	%rax, -96(%rbp)
.L74:
	movl	$0, 16(%r12)
	movl	16(%rbx), %edi
	testl	%edi, %edi
	jne	.L13
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	BN_ucmp@PLT
	testl	%eax, %eax
	js	.L14
.L13:
	leaq	-80(%rbp), %r15
	movq	%r15, %rdi
	call	bn_init@PLT
	movl	$4, %edx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	BN_with_flags@PLT
	movq	%r13, %rcx
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	BN_nnmod@PLT
	testl	%eax, %eax
	je	.L25
.L14:
	movl	$-1, -136(%rbp)
	leaq	-80(%rbp), %r15
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L17:
	movq	%r15, %rdi
	call	bn_init@PLT
	movl	$4, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	BN_with_flags@PLT
	movq	-104(%rbp), %rsi
	movq	-120(%rbp), %rdi
	movq	%r13, %r8
	movq	%rbx, %rcx
	movq	%r15, %rdx
	call	BN_div@PLT
	testl	%eax, %eax
	je	.L25
	movq	-88(%rbp), %rdx
	movq	-120(%rbp), %rsi
	movq	%r13, %rcx
	movq	%r12, %rdi
	call	BN_mul@PLT
	testl	%eax, %eax
	je	.L25
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	BN_add@PLT
	testl	%eax, %eax
	je	.L25
	movq	%r12, %rax
	negl	-136(%rbp)
	movq	%rbx, %r12
	movq	-104(%rbp), %rbx
	movq	%r14, -104(%rbp)
	movq	-88(%rbp), %r14
	movq	%rax, -88(%rbp)
.L15:
	movq	%rbx, %rdi
	call	BN_is_zero@PLT
	testl	%eax, %eax
	je	.L17
	cmpl	$-1, -136(%rbp)
	je	.L18
.L21:
	movq	%r12, %rdi
	call	BN_is_one@PLT
	testl	%eax, %eax
	je	.L275
	movl	16(%r14), %esi
	testl	%esi, %esi
	jne	.L22
	movq	-128(%rbp), %rsi
	movq	%r14, %rdi
	call	BN_ucmp@PLT
	testl	%eax, %eax
	jns	.L22
	movq	-96(%rbp), %rdi
	movq	%r14, %rsi
	call	BN_copy@PLT
	testq	%rax, %rax
	jne	.L67
	.p2align 4,,10
	.p2align 3
.L25:
	cmpq	$0, -112(%rbp)
	je	.L27
.L269:
	movq	$0, -96(%rbp)
.L67:
	movq	%r13, %rdi
	call	BN_CTX_end@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L276
	movq	-96(%rbp), %rax
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L273:
	.cfi_restore_state
	movq	%r13, %rdi
	call	BN_CTX_start@PLT
	movq	%r13, %rdi
	call	BN_CTX_get@PLT
	movq	%r13, %rdi
	movq	%rax, %r12
	call	BN_CTX_get@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	BN_CTX_get@PLT
	movq	%r13, %rdi
	movq	%rax, -104(%rbp)
	call	BN_CTX_get@PLT
	movq	%r13, %rdi
	movq	%rax, %r14
	call	BN_CTX_get@PLT
	movq	%r13, %rdi
	movq	%rax, -152(%rbp)
	call	BN_CTX_get@PLT
	movq	%r13, %rdi
	movq	%rax, -88(%rbp)
	call	BN_CTX_get@PLT
	movq	%rax, -136(%rbp)
	testq	%rax, %rax
	je	.L77
	cmpq	$0, -112(%rbp)
	je	.L277
	movq	-104(%rbp), %rdi
	movl	$1, %esi
	call	BN_set_word@PLT
	movq	-88(%rbp), %rdi
	xorl	%esi, %esi
	call	BN_set_word@PLT
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	BN_copy@PLT
	testq	%rax, %rax
	je	.L269
	movq	-128(%rbp), %rsi
	movq	%r12, %rdi
	call	BN_copy@PLT
	testq	%rax, %rax
	je	.L269
	movq	-112(%rbp), %rax
	movq	%rax, -96(%rbp)
.L75:
	movl	$0, 16(%r12)
	movl	16(%rbx), %ecx
	testl	%ecx, %ecx
	jne	.L31
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	BN_ucmp@PLT
	testl	%eax, %eax
	js	.L30
.L31:
	movq	%r13, %rcx
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movq	%rbx, %rdi
	call	BN_nnmod@PLT
	testl	%eax, %eax
	je	.L25
.L30:
	movq	-128(%rbp), %rdi
	call	BN_is_odd@PLT
	testl	%eax, %eax
	jne	.L32
.L35:
	movq	-152(%rbp), %r15
	movl	$-1, -140(%rbp)
	movq	%r13, -152(%rbp)
	movq	-104(%rbp), %r13
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L52:
	movq	-88(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	BN_add@PLT
	testl	%eax, %eax
	je	.L266
.L59:
	negl	-140(%rbp)
	movq	%r12, %rax
	movq	%rbx, %r12
	movq	%r15, %rbx
	movq	-88(%rbp), %r15
	movq	%r13, -88(%rbp)
	movq	%rax, %r13
.L33:
	movq	%rbx, %rdi
	call	BN_is_zero@PLT
	testl	%eax, %eax
	jne	.L278
	movq	%r12, %rdi
	call	BN_num_bits@PLT
	movq	%rbx, %rdi
	movl	%eax, -104(%rbp)
	call	BN_num_bits@PLT
	cmpl	%eax, -104(%rbp)
	je	.L271
	movq	%r12, %rdi
	call	BN_num_bits@PLT
	movq	%rbx, %rdi
	movl	%eax, -104(%rbp)
	call	BN_num_bits@PLT
	addl	$1, %eax
	cmpl	%eax, -104(%rbp)
	je	.L279
	movq	-152(%rbp), %r8
	movq	%rbx, %rcx
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	BN_div@PLT
	testl	%eax, %eax
	je	.L266
.L56:
	movq	%r14, %rdi
	call	BN_is_one@PLT
	testl	%eax, %eax
	jne	.L52
	movl	$2, %esi
	movq	%r14, %rdi
	call	BN_is_word@PLT
	testl	%eax, %eax
	je	.L58
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	BN_lshift1@PLT
	testl	%eax, %eax
	je	.L266
.L61:
	movq	-88(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	BN_add@PLT
	testl	%eax, %eax
	jne	.L59
	.p2align 4,,10
	.p2align 3
.L266:
	movq	-152(%rbp), %r13
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L277:
	call	BN_new@PLT
	movq	%rax, -96(%rbp)
	testq	%rax, %rax
	jne	.L280
.L27:
	movq	-96(%rbp), %rdi
	call	BN_free@PLT
	jmp	.L269
	.p2align 4,,10
	.p2align 3
.L22:
	movq	-128(%rbp), %rdx
	movq	-96(%rbp), %rdi
	movq	%r13, %rcx
	movq	%r14, %rsi
	call	BN_nnmod@PLT
	testl	%eax, %eax
	jne	.L67
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L271:
	movl	$1, %esi
	movq	%r14, %rdi
	call	BN_set_word@PLT
	testl	%eax, %eax
	je	.L266
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	BN_sub@PLT
	testl	%eax, %eax
	jne	.L56
	jmp	.L266
	.p2align 4,,10
	.p2align 3
.L58:
	movl	$4, %esi
	movq	%r14, %rdi
	call	BN_is_word@PLT
	testl	%eax, %eax
	jne	.L281
	cmpl	$1, 8(%r14)
	je	.L282
	movq	-152(%rbp), %rcx
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	BN_mul@PLT
	testl	%eax, %eax
	jne	.L61
	jmp	.L266
	.p2align 4,,10
	.p2align 3
.L279:
	movq	-136(%rbp), %rdi
	movq	%rbx, %rsi
	call	BN_lshift1@PLT
	testl	%eax, %eax
	je	.L266
	movq	-136(%rbp), %rsi
	movq	%r12, %rdi
	call	BN_ucmp@PLT
	testl	%eax, %eax
	js	.L271
	movq	-136(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	BN_sub@PLT
	testl	%eax, %eax
	je	.L266
	movq	-136(%rbp), %rsi
	movq	%rbx, %rdx
	movq	%r14, %rdi
	call	BN_add@PLT
	testl	%eax, %eax
	je	.L266
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	BN_ucmp@PLT
	testl	%eax, %eax
	js	.L283
	movl	$3, %esi
	movq	%r14, %rdi
	call	BN_set_word@PLT
	testl	%eax, %eax
	je	.L266
	movq	%rbx, %rdx
	movq	%r15, %rsi
	movq	%r15, %rdi
	call	BN_sub@PLT
	testl	%eax, %eax
	jne	.L56
	jmp	.L266
	.p2align 4,,10
	.p2align 3
.L274:
	call	BN_new@PLT
	movq	%rax, -96(%rbp)
	testq	%rax, %rax
	je	.L27
	movq	-88(%rbp), %rdi
	movl	$1, %esi
	call	BN_set_word@PLT
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	BN_set_word@PLT
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	BN_copy@PLT
	testq	%rax, %rax
	je	.L27
	movq	-128(%rbp), %rsi
	movq	%r12, %rdi
	call	BN_copy@PLT
	testq	%rax, %rax
	jne	.L74
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L77:
	movq	$0, -96(%rbp)
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L281:
	movl	$2, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	BN_lshift@PLT
	testl	%eax, %eax
	jne	.L61
	jmp	.L266
	.p2align 4,,10
	.p2align 3
.L282:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	BN_copy@PLT
	testq	%rax, %rax
	je	.L266
	movq	(%r14), %rax
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	BN_mul_word@PLT
	testl	%eax, %eax
	jne	.L61
	jmp	.L266
	.p2align 4,,10
	.p2align 3
.L18:
	movq	-128(%rbp), %rsi
	movq	%r14, %rdx
	movq	%r14, %rdi
	call	BN_sub@PLT
	testl	%eax, %eax
	jne	.L21
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L283:
	movl	$2, %esi
	movq	%r14, %rdi
	call	BN_set_word@PLT
	testl	%eax, %eax
	jne	.L56
	jmp	.L266
	.p2align 4,,10
	.p2align 3
.L32:
	movq	-128(%rbp), %rdi
	call	BN_num_bits@PLT
	cmpl	$2048, %eax
	jg	.L35
	movq	%r12, -136(%rbp)
	movq	-104(%rbp), %r12
	movq	%r13, -104(%rbp)
	movq	-88(%rbp), %r14
.L34:
	movq	%rbx, %rdi
	call	BN_is_zero@PLT
	movl	%eax, %r15d
	testl	%eax, %eax
	jne	.L284
	xorl	%r13d, %r13d
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L39:
	movq	%r12, %rdi
	addl	$1, %r13d
	call	BN_is_odd@PLT
	testl	%eax, %eax
	je	.L38
	movq	-128(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	BN_uadd@PLT
	testl	%eax, %eax
	je	.L256
.L38:
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	BN_rshift1@PLT
	testl	%eax, %eax
	je	.L256
.L37:
	movl	%r13d, %esi
	movq	%rbx, %rdi
	call	BN_is_bit_set@PLT
	testl	%eax, %eax
	je	.L39
	testl	%r13d, %r13d
	je	.L42
	jmp	.L285
	.p2align 4,,10
	.p2align 3
.L44:
	movq	%r14, %rdi
	addl	$1, %r15d
	call	BN_is_odd@PLT
	testl	%eax, %eax
	je	.L43
	movq	-128(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r14, %rdi
	call	BN_uadd@PLT
	testl	%eax, %eax
	je	.L256
.L43:
	movq	%r14, %rsi
	movq	%r14, %rdi
	call	BN_rshift1@PLT
	testl	%eax, %eax
	je	.L256
.L42:
	movq	-136(%rbp), %rdi
	movl	%r15d, %esi
	call	BN_is_bit_set@PLT
	testl	%eax, %eax
	je	.L44
	testl	%r15d, %r15d
	je	.L48
	movq	-136(%rbp), %rsi
	movl	%r15d, %edx
	movq	%rsi, %rdi
	call	BN_rshift@PLT
	testl	%eax, %eax
	je	.L256
.L48:
	movq	-136(%rbp), %rsi
	movq	%rbx, %rdi
	call	BN_ucmp@PLT
	testl	%eax, %eax
	js	.L286
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	BN_uadd@PLT
	testl	%eax, %eax
	je	.L256
	movq	-136(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%rbx, %rdi
	call	BN_usub@PLT
	testl	%eax, %eax
	jne	.L34
	.p2align 4,,10
	.p2align 3
.L256:
	movq	-104(%rbp), %r13
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L278:
	cmpl	$-1, -140(%rbp)
	movq	-152(%rbp), %r13
	je	.L50
.L64:
	movq	%r12, %rdi
	call	BN_is_one@PLT
	testl	%eax, %eax
	je	.L65
	movq	-88(%rbp), %rax
	movl	16(%rax), %edx
	testl	%edx, %edx
	jne	.L66
	movq	-128(%rbp), %rsi
	movq	%rax, %rdi
	call	BN_ucmp@PLT
	testl	%eax, %eax
	js	.L287
.L66:
	movq	-128(%rbp), %rdx
	movq	-88(%rbp), %rsi
	movq	%r13, %rcx
	movq	-96(%rbp), %rdi
	call	BN_nnmod@PLT
	testl	%eax, %eax
	jne	.L67
	jmp	.L25
.L275:
.L3:
	endbr64
	movl	$507, %r8d
	movl	$108, %edx
	movl	$139, %esi
	movl	$3, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L25
.L284:
	movq	-136(%rbp), %r12
	movq	-104(%rbp), %r13
.L50:
	movq	-88(%rbp), %rdx
	movq	-128(%rbp), %rsi
	movq	%rdx, %rdi
	call	BN_sub@PLT
	testl	%eax, %eax
	jne	.L64
	jmp	.L25
.L65:
	movq	-120(%rbp), %rax
	testq	%rax, %rax
	je	.L25
	movl	$1, (%rax)
	jmp	.L25
.L285:
	movl	%r13d, %edx
	movq	%rbx, %rsi
	movq	%rbx, %rdi
	call	BN_rshift@PLT
	testl	%eax, %eax
	jne	.L42
	jmp	.L256
	.p2align 4,,10
	.p2align 3
.L287:
	movq	-88(%rbp), %rsi
	movq	-96(%rbp), %rdi
	call	BN_copy@PLT
	testq	%rax, %rax
	jne	.L67
	jmp	.L25
.L286:
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r14, %rdi
	call	BN_uadd@PLT
	testl	%eax, %eax
	je	.L256
	movq	-136(%rbp), %rsi
	movq	%rbx, %rdx
	movq	%rsi, %rdi
	call	BN_usub@PLT
	testl	%eax, %eax
	jne	.L34
	jmp	.L256
.L276:
	call	__stack_chk_fail@PLT
.L280:
	movq	-104(%rbp), %rdi
	movl	$1, %esi
	call	BN_set_word@PLT
	movq	-88(%rbp), %rdi
	xorl	%esi, %esi
	call	BN_set_word@PLT
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	BN_copy@PLT
	testq	%rax, %rax
	je	.L27
	movq	-128(%rbp), %rsi
	movq	%r12, %rdi
	call	BN_copy@PLT
	testq	%rax, %rax
	jne	.L75
	jmp	.L27
	.cfi_endproc
.LFE256:
	.size	int_bn_mod_inverse.part.0, .-int_bn_mod_inverse.part.0
	.p2align 4
	.globl	BN_mod_inverse
	.type	BN_mod_inverse, @function
BN_mod_inverse:
.LFB252:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	movl	$1, %esi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%rdx, %rdi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	BN_abs_is_word@PLT
	testl	%eax, %eax
	jne	.L291
	movq	%r12, %rdi
	call	BN_is_zero@PLT
	testl	%eax, %eax
	jne	.L291
	movq	%r12, %rdx
	leaq	-44(%rbp), %r8
	movq	%r15, %rcx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movl	$0, -44(%rbp)
	call	int_bn_mod_inverse.part.0
	movl	-44(%rbp), %edx
	testl	%edx, %edx
	jne	.L290
.L288:
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L295
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L291:
	.cfi_restore_state
	movl	$1, -44(%rbp)
	xorl	%eax, %eax
.L290:
	movl	$25, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$108, %edx
	movl	$110, %esi
	movl	$3, %edi
	movq	%rax, -56(%rbp)
	call	ERR_put_error@PLT
	movq	-56(%rbp), %rax
	jmp	.L288
.L295:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE252:
	.size	BN_mod_inverse, .-BN_mod_inverse
	.p2align 4
	.globl	int_bn_mod_inverse
	.type	int_bn_mod_inverse, @function
int_bn_mod_inverse:
.LFB253:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	movl	$1, %esi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%rdx, %rdi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$8, %rsp
	call	BN_abs_is_word@PLT
	testl	%eax, %eax
	jne	.L300
	movq	%r12, %rdi
	call	BN_is_zero@PLT
	testl	%eax, %eax
	jne	.L300
	testq	%rbx, %rbx
	je	.L302
	movl	$0, (%rbx)
.L302:
	addq	$8, %rsp
	movq	%rbx, %r8
	movq	%r15, %rcx
	movq	%r12, %rdx
	popq	%rbx
	movq	%r14, %rsi
	popq	%r12
	movq	%r13, %rdi
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	int_bn_mod_inverse.part.0
	.p2align 4,,10
	.p2align 3
.L300:
	.cfi_restore_state
	testq	%rbx, %rbx
	je	.L296
	movl	$1, (%rbx)
.L296:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE253:
	.size	int_bn_mod_inverse, .-int_bn_mod_inverse
	.p2align 4
	.globl	BN_gcd
	.type	BN_gcd, @function
BN_gcd:
.LFB255:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%rdx, %rdi
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	call	BN_is_zero@PLT
	movq	%r15, %rsi
	testl	%eax, %eax
	jne	.L355
	movq	%r15, %rdi
	call	BN_is_zero@PLT
	movl	%eax, %ebx
	testl	%eax, %eax
	jne	.L356
	movq	%r13, %rdi
	call	BN_CTX_start@PLT
	movq	%r13, %rdi
	call	BN_CTX_get@PLT
	movq	%r13, %rdi
	movq	%rax, -56(%rbp)
	call	BN_CTX_get@PLT
	testq	%rax, %rax
	je	.L313
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -64(%rbp)
	call	BN_lshift1@PLT
	testl	%eax, %eax
	jne	.L357
.L313:
	movq	%r13, %rdi
	call	BN_CTX_end@PLT
	jmp	.L308
.L356:
	movq	%r14, %rsi
.L355:
	movq	%r12, %rdi
	xorl	%ebx, %ebx
	call	BN_copy@PLT
	movl	$0, 16(%r12)
	testq	%rax, %rax
	setne	%bl
.L308:
	addq	$56, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L357:
	.cfi_restore_state
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	BN_lshift1@PLT
	testl	%eax, %eax
	je	.L313
	movl	12(%r12), %eax
	movq	-64(%rbp), %r10
	testl	%eax, %eax
	jle	.L321
	movl	12(%r10), %edi
	leal	-1(%rax), %r11d
	xorl	%r15d, %r15d
	xorl	%esi, %esi
	movl	$1, %ecx
	.p2align 4,,10
	.p2align 3
.L316:
	cmpl	%esi, %edi
	jle	.L315
	movq	(%r10), %rdx
	movq	(%r12), %rax
	movq	(%rax,%rsi,8), %rax
	orq	(%rdx,%rsi,8), %rax
	movl	$64, %edx
	notq	%rax
	.p2align 4,,10
	.p2align 3
.L317:
	andl	%eax, %ecx
	shrq	%rax
	addl	%ecx, %r15d
	subl	$1, %edx
	jne	.L317
	leaq	1(%rsi), %rax
	cmpq	%rsi, %r11
	je	.L315
	movq	%rax, %rsi
	jmp	.L316
.L321:
	xorl	%r15d, %r15d
.L315:
	movl	%r15d, %edx
	movq	%r12, %rsi
	movq	%r12, %rdi
	movq	%r10, -64(%rbp)
	call	BN_rshift@PLT
	testl	%eax, %eax
	je	.L313
	movq	-64(%rbp), %r10
	movl	%r15d, %edx
	movq	%r10, %rsi
	movq	%r10, %rdi
	call	BN_rshift@PLT
	testl	%eax, %eax
	je	.L313
	movq	-64(%rbp), %r10
	movq	%r12, %rdi
	movl	8(%r10), %eax
	cmpl	%eax, 8(%r12)
	cmovge	8(%r12), %eax
	leal	1(%rax), %r14d
	movl	%r14d, %esi
	call	bn_wexpand@PLT
	testq	%rax, %rax
	je	.L313
	movq	-64(%rbp), %r10
	movl	%r14d, %esi
	movq	%r10, %rdi
	call	bn_wexpand@PLT
	testq	%rax, %rax
	je	.L313
	movq	-56(%rbp), %rdi
	movl	%r14d, %esi
	call	bn_wexpand@PLT
	testq	%rax, %rax
	je	.L313
	movq	(%r12), %rax
	movq	-64(%rbp), %r10
	movl	%r14d, %ecx
	movq	%r12, %rsi
	movq	(%rax), %rdi
	movq	%r10, %rdx
	movq	%r10, -72(%rbp)
	notq	%rdi
	andl	$1, %edi
	call	BN_consttime_swap@PLT
	movq	%r12, %rdi
	call	BN_num_bits@PLT
	movq	-72(%rbp), %r10
	movl	%eax, -64(%rbp)
	movq	%r10, %rdi
	movq	%r10, -88(%rbp)
	call	BN_num_bits@PLT
	movl	-64(%rbp), %edx
	cmpl	%eax, %edx
	cmovge	%edx, %eax
	leal	(%rax,%rax,2), %eax
	cmpl	$-3, %eax
	jl	.L319
	addl	$4, %eax
	movl	$0, -64(%rbp)
	movq	-88(%rbp), %r10
	movl	%eax, -76(%rbp)
	movl	$1, -72(%rbp)
	jmp	.L320
.L358:
	movq	-88(%rbp), %r10
	movq	-56(%rbp), %rdx
	movl	%r14d, %ecx
	movl	8(%r10), %eax
	movq	%r10, %rsi
	leal	-1(%rax), %edi
	movq	(%r10), %rax
	sarl	$31, %edi
	notl	%edi
	movslq	%edi, %rdi
	andq	(%rax), %rdi
	andl	$1, %edi
	call	BN_consttime_swap@PLT
	movq	-88(%rbp), %r10
	movq	%r10, %rsi
	movq	%r10, %rdi
	call	BN_rshift1@PLT
	testl	%eax, %eax
	je	.L313
	addl	$1, -64(%rbp)
	movl	-64(%rbp), %eax
	cmpl	-76(%rbp), %eax
	movq	-88(%rbp), %r10
	je	.L319
.L320:
	movl	-72(%rbp), %esi
	movl	8(%r10), %eax
	movq	%r10, -88(%rbp)
	movq	(%r10), %rcx
	movl	%esi, %edx
	subl	$1, %eax
	negl	%edx
	sarl	$31, %eax
	notl	%eax
	andl	(%rcx), %eax
	movl	%edx, %ecx
	shrl	$31, %ecx
	andl	%ecx, %eax
	xorl	%eax, 16(%r12)
	leal	-1(%rax), %ecx
	movslq	%eax, %rdi
	andl	%esi, %ecx
	movl	%eax, %esi
	negl	%esi
	andl	%esi, %edx
	movq	%r12, %rsi
	orl	%ecx, %edx
	movl	%r14d, %ecx
	movl	%edx, -72(%rbp)
	movq	%r10, %rdx
	call	BN_consttime_swap@PLT
	movl	-72(%rbp), %edx
	movq	-88(%rbp), %r10
	movq	-56(%rbp), %rdi
	leal	1(%rdx), %eax
	movq	%r10, %rsi
	movq	%r12, %rdx
	movl	%eax, -72(%rbp)
	call	BN_add@PLT
	testl	%eax, %eax
	jne	.L358
	jmp	.L313
.L319:
	movl	$0, 16(%r12)
	movl	%r15d, %edx
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	BN_lshift@PLT
	testl	%eax, %eax
	je	.L313
	movq	%r12, %rsi
	movq	%r12, %rdi
	xorl	%ebx, %ebx
	call	BN_rshift1@PLT
	testl	%eax, %eax
	setne	%bl
	jmp	.L313
	.cfi_endproc
.LFE255:
	.size	BN_gcd, .-BN_gcd
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
