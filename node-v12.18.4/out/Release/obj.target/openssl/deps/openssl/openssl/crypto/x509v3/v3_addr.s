	.file	"v3_addr.c"
	.text
	.p2align 4
	.globl	IPAddressFamily_free
	.type	IPAddressFamily_free, @function
IPAddressFamily_free:
.LFB1337:
	.cfi_startproc
	endbr64
	leaq	IPAddressFamily_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE1337:
	.size	IPAddressFamily_free, .-IPAddressFamily_free
	.p2align 4
	.type	IPAddressFamily_cmp, @function
IPAddressFamily_cmp:
.LFB1357:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rsi), %rdx
	movq	(%rdi), %rax
	movq	(%rdx), %rcx
	movq	(%rax), %rax
	movl	(%rcx), %r12d
	movq	8(%rcx), %rsi
	movslq	(%rax), %rbx
	movq	8(%rax), %rdi
	movslq	%r12d, %rdx
	cmpl	%r12d, %ebx
	cmovle	%rbx, %rdx
	subl	%r12d, %ebx
	call	memcmp@PLT
	testl	%eax, %eax
	cmove	%ebx, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1357:
	.size	IPAddressFamily_cmp, .-IPAddressFamily_cmp
	.p2align 4
	.type	make_IPAddressFamily, @function
make_IPAddressFamily:
.LFB1350:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	rolw	$8, %si
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	$2, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movw	%si, -59(%rbp)
	testq	%rdx, %rdx
	je	.L7
	movl	(%rdx), %eax
	movl	$3, %r14d
	movb	%al, -57(%rbp)
.L7:
	xorl	%ebx, %ebx
	movslq	%r14d, %r15
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L9:
	addl	$1, %ebx
.L8:
	movq	%r13, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jge	.L38
	movl	%ebx, %esi
	movq	%r13, %rdi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %r12
	movq	(%rax), %rax
	cmpl	%r14d, (%rax)
	jne	.L9
	movq	8(%rax), %rdi
	leaq	-59(%rbp), %rsi
	movq	%r15, %rdx
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L9
.L6:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L39
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L38:
	.cfi_restore_state
	leaq	IPAddressFamily_it(%rip), %rdi
	call	ASN1_item_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L16
	cmpq	$0, 8(%rax)
	je	.L13
.L17:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L40
	leaq	-59(%rbp), %rsi
	movl	%r14d, %edx
	call	ASN1_OCTET_STRING_set@PLT
	testl	%eax, %eax
	je	.L16
.L41:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	jne	.L6
.L16:
	movq	%r12, %rdi
	leaq	IPAddressFamily_it(%rip), %rsi
	xorl	%r12d, %r12d
	call	ASN1_item_free@PLT
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L40:
	call	ASN1_OCTET_STRING_new@PLT
	movq	%rax, (%r12)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L16
	leaq	-59(%rbp), %rsi
	movl	%r14d, %edx
	call	ASN1_OCTET_STRING_set@PLT
	testl	%eax, %eax
	jne	.L41
	jmp	.L16
.L13:
	leaq	IPAddressChoice_it(%rip), %rdi
	call	ASN1_item_new@PLT
	movq	%rax, 8(%r12)
	testq	%rax, %rax
	jne	.L17
	jmp	.L16
.L39:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1350:
	.size	make_IPAddressFamily, .-make_IPAddressFamily
	.p2align 4
	.type	addr_expand, @function
addr_expand:
.LFB1340:
	.cfi_startproc
	movl	(%rsi), %eax
	cmpl	%edx, %eax
	jbe	.L56
	xorl	%r8d, %r8d
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L56:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%ecx, %r13d
	xorl	%ecx, %ecx
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movl	%edx, %ebx
	movslq	%eax, %rdx
	subq	$8, %rsp
	testl	%eax, %eax
	jne	.L57
.L44:
	subl	%eax, %ebx
	movzbl	%r13b, %esi
	addq	%rcx, %rdi
	movslq	%ebx, %rdx
	call	memset@PLT
	addq	$8, %rsp
	movl	$1, %r8d
	popq	%rbx
	movl	%r8d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L57:
	.cfi_restore_state
	movq	8(%rsi), %rsi
	call	memcpy@PLT
	movq	%rax, %rdi
	movq	16(%r12), %rax
	andl	$7, %eax
	je	.L55
	movslq	(%r12), %rdx
	movl	$8, %ecx
	subl	%eax, %ecx
	movl	$255, %eax
	leaq	-1(%rdi,%rdx), %rdx
	sarl	%cl, %eax
	movzbl	(%rdx), %ecx
	testb	%r13b, %r13b
	jne	.L46
	notl	%eax
	andl	%ecx, %eax
	movb	%al, (%rdx)
	movslq	(%r12), %rcx
	movq	%rcx, %rax
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L46:
	orl	%ecx, %eax
	movb	%al, (%rdx)
.L55:
	movslq	(%r12), %rcx
	movq	%rcx, %rax
	jmp	.L44
	.cfi_endproc
.LFE1340:
	.size	addr_expand, .-addr_expand
	.p2align 4
	.type	make_addressPrefix, @function
make_addressPrefix:
.LFB1348:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	leaq	IPAddressOrRange_it(%rip), %rdi
	pushq	%r12
	.cfi_offset 12, -48
	leal	14(%rdx), %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$8, %rsp
	addl	$7, %eax
	cmovns	%eax, %r12d
	sarl	$3, %r12d
	call	ASN1_item_new@PLT
	testq	%rax, %rax
	je	.L63
	movq	8(%rax), %rdi
	movl	$0, (%rax)
	movq	%rax, %r14
	testq	%rdi, %rdi
	je	.L71
.L60:
	movl	%r12d, %edx
	movq	%r15, %rsi
	call	ASN1_BIT_STRING_set@PLT
	testl	%eax, %eax
	je	.L61
	movl	%ebx, %eax
	movq	8(%r14), %rdx
	sarl	$31, %eax
	shrl	$29, %eax
	leal	(%rbx,%rax), %ecx
	andl	$7, %ecx
	subl	%eax, %ecx
	movq	16(%rdx), %rax
	andq	$-8, %rax
	orq	$8, %rax
	movq	%rax, 16(%rdx)
	testl	%ecx, %ecx
	jle	.L62
	movq	8(%rdx), %rdx
	movl	$255, %eax
	movslq	%r12d, %r12
	sarl	%cl, %eax
	notl	%eax
	andb	%al, -1(%rdx,%r12)
	movl	$8, %eax
	movq	8(%r14), %rdx
	subl	%ecx, %eax
	cltq
	orq	%rax, 16(%rdx)
.L62:
	movq	%r14, 0(%r13)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L71:
	.cfi_restore_state
	call	ASN1_BIT_STRING_new@PLT
	movq	%rax, 8(%r14)
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L60
.L61:
	movq	%r14, %rdi
	leaq	IPAddressOrRange_it(%rip), %rsi
	call	ASN1_item_free@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L63:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1348:
	.size	make_addressPrefix, .-make_addressPrefix
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	":"
.LC1:
	.string	""
.LC2:
	.string	"[%d]"
.LC3:
	.string	"%d.%d.%d.%d"
.LC4:
	.string	"%x%s"
.LC5:
	.string	"%s%02x"
	.text
	.p2align 4
	.type	i2r_address, @function
i2r_address:
.LFB1341:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	(%rcx), %eax
	testl	%eax, %eax
	js	.L78
	movq	%rdi, %r14
	movq	%rcx, %r12
	cmpl	$1, %esi
	je	.L75
	cmpl	$2, %esi
	je	.L76
	testl	%eax, %eax
	je	.L90
	movq	8(%rcx), %rax
	xorl	%ebx, %ebx
	leaq	.LC1(%rip), %rdx
	leaq	.LC0(%rip), %r13
	movzbl	(%rax), %ecx
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L89:
	movq	8(%r12), %rax
	movq	%r13, %rdx
	movzbl	(%rax,%rbx), %ecx
.L91:
	leaq	.LC5(%rip), %rsi
	movq	%r14, %rdi
	xorl	%eax, %eax
	addq	$1, %rbx
	call	BIO_printf@PLT
	cmpl	%ebx, (%r12)
	jg	.L89
.L90:
	movq	16(%r12), %rdx
	leaq	.LC2(%rip), %rsi
	movq	%r14, %rdi
	xorl	%eax, %eax
	andl	$7, %edx
	call	BIO_printf@PLT
	movl	$1, %eax
.L72:
	movq	-56(%rbp), %rdi
	xorq	%fs:40, %rdi
	jne	.L119
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L76:
	.cfi_restore_state
	leaq	-80(%rbp), %rax
	movzbl	%dl, %ecx
	movq	%r12, %rsi
	movl	$16, %edx
	movq	%rax, %rdi
	movq	%rax, -88(%rbp)
	call	addr_expand
	testl	%eax, %eax
	je	.L78
	cmpb	$0, -65(%rbp)
	jne	.L93
	cmpb	$0, -66(%rbp)
	jne	.L93
	cmpb	$0, -67(%rbp)
	jne	.L96
	cmpb	$0, -68(%rbp)
	jne	.L96
	cmpb	$0, -69(%rbp)
	jne	.L98
	cmpb	$0, -70(%rbp)
	jne	.L98
	cmpb	$0, -71(%rbp)
	jne	.L100
	cmpb	$0, -72(%rbp)
	jne	.L100
	cmpb	$0, -73(%rbp)
	jne	.L102
	cmpb	$0, -74(%rbp)
	jne	.L102
	cmpb	$0, -75(%rbp)
	jne	.L104
	cmpb	$0, -76(%rbp)
	jne	.L104
	cmpb	$0, -77(%rbp)
	jne	.L106
	cmpb	$0, -78(%rbp)
	jne	.L106
	cmpb	$0, -79(%rbp)
	jne	.L117
	cmpb	$0, -80(%rbp)
	jne	.L117
	leaq	.LC0(%rip), %rsi
	movq	%r14, %rdi
	call	BIO_puts@PLT
	leaq	.LC0(%rip), %rsi
	movq	%r14, %rdi
	call	BIO_puts@PLT
	movl	$1, %eax
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L75:
	movzbl	%dl, %ecx
	leaq	-80(%rbp), %rdi
	movl	$4, %edx
	movq	%r12, %rsi
	call	addr_expand
	testl	%eax, %eax
	jne	.L120
.L78:
	xorl	%eax, %eax
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L120:
	movzbl	-79(%rbp), %ecx
	movzbl	-80(%rbp), %edx
	movq	%r14, %rdi
	xorl	%eax, %eax
	movzbl	-77(%rbp), %r9d
	movzbl	-78(%rbp), %r8d
	leaq	.LC3(%rip), %rsi
	call	BIO_printf@PLT
	movl	$1, %eax
	jmp	.L72
.L93:
	movl	$16, -100(%rbp)
	.p2align 4,,10
	.p2align 3
.L79:
	movl	-100(%rbp), %eax
	movl	$13, %r12d
	movq	-88(%rbp), %r15
	leaq	.LC0(%rip), %rbx
	subl	$1, %eax
	movl	%eax, -96(%rbp)
	cmpl	$13, %eax
	cmovle	%eax, %r12d
	xorl	%r13d, %r13d
	.p2align 4,,10
	.p2align 3
.L83:
	movzbl	(%r15), %edx
	movzbl	1(%r15), %eax
	movq	%rbx, %rcx
	movq	%r14, %rdi
	leaq	.LC4(%rip), %rsi
	addl	$2, %r13d
	addq	$2, %r15
	sall	$8, %edx
	orl	%eax, %edx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	cmpl	%r12d, %r13d
	jle	.L83
	cmpl	-100(%rbp), %r13d
	jge	.L86
	movq	-96(%rbp), %rbx
	movq	-88(%rbp), %rdi
	movslq	%r13d, %rax
	leaq	.LC1(%rip), %r12
	subq	%r13, %rbx
	leaq	(%rdi,%rax), %r15
	leaq	2(%rdi,%rax), %rax
	andl	$4294967294, %ebx
	addq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L85:
	movzbl	(%r15), %edx
	movzbl	1(%r15), %eax
	movq	%r12, %rcx
	movq	%r14, %rdi
	leaq	.LC4(%rip), %rsi
	addq	$2, %r15
	sall	$8, %edx
	orl	%eax, %edx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	cmpq	%rbx, %r15
	jne	.L85
.L86:
	movl	-96(%rbp), %r13d
	shrl	%r13d
	cmpl	$7, %r13d
	jne	.L88
.L118:
	movl	$1, %eax
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L88:
	leaq	.LC0(%rip), %rsi
	movq	%r14, %rdi
	call	BIO_puts@PLT
	jmp	.L118
.L96:
	movl	$14, -100(%rbp)
	jmp	.L79
.L98:
	movl	$12, -100(%rbp)
	jmp	.L79
.L100:
	movl	$10, -100(%rbp)
	jmp	.L79
.L102:
	movl	$8, -100(%rbp)
	jmp	.L79
.L104:
	movl	$6, -100(%rbp)
	jmp	.L79
.L106:
	movl	$4, -100(%rbp)
	jmp	.L79
.L117:
	movl	$2, -100(%rbp)
	jmp	.L79
.L119:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1341:
	.size	i2r_address, .-i2r_address
	.section	.rodata.str1.1
.LC6:
	.string	"%*sIPv4"
.LC7:
	.string	"%*sIPv6"
.LC8:
	.string	"%*sUnknown AFI %u"
.LC9:
	.string	" (Unicast)"
.LC10:
	.string	" (Multicast)"
.LC11:
	.string	" (Unicast/Multicast)"
.LC12:
	.string	" (MPLS)"
.LC13:
	.string	" (Tunnel)"
.LC14:
	.string	" (VPLS)"
.LC15:
	.string	" (BGP MDT)"
.LC16:
	.string	" (MPLS-labeled VPN)"
.LC17:
	.string	" (Unknown SAFI %u)"
.LC18:
	.string	": inherit\n"
.LC19:
	.string	":\n"
.LC20:
	.string	"%*s"
.LC21:
	.string	"/%d\n"
.LC22:
	.string	"-"
.LC23:
	.string	"\n"
	.text
	.p2align 4
	.type	i2r_IPAddrBlocks, @function
i2r_IPAddrBlocks:
.LFB1343:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	movq	%r14, %rdi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$40, %rsp
	movl	%ecx, -52(%rbp)
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r15d
	jge	.L162
	.p2align 4,,10
	.p2align 3
.L150:
	movl	%r15d, %esi
	movq	%r14, %rdi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L154
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L154
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L154
	cmpl	$1, (%rax)
	jle	.L154
	movzwl	(%rdx), %eax
	rolw	$8, %ax
	movzwl	%ax, %r13d
	cmpw	$1, %ax
	jne	.L163
	movl	-52(%rbp), %edx
	leaq	.LC1(%rip), %rcx
	movq	%rbx, %rdi
	xorl	%eax, %eax
	leaq	.LC6(%rip), %rsi
	movl	$1, %r13d
	call	BIO_printf@PLT
.L126:
	movq	(%r12), %rax
	cmpl	$2, (%rax)
	jle	.L127
	movq	8(%rax), %rax
	movzbl	2(%rax), %edx
	cmpb	$64, %dl
	je	.L128
	ja	.L129
	cmpb	$3, %dl
	je	.L130
	ja	.L131
	cmpb	$1, %dl
	je	.L132
	cmpb	$2, %dl
	jne	.L134
	leaq	.LC10(%rip), %rsi
	movq	%rbx, %rdi
	call	BIO_puts@PLT
	.p2align 4,,10
	.p2align 3
.L127:
	movq	8(%r12), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	je	.L139
.L166:
	cmpl	$1, %eax
	jne	.L141
	movq	%rbx, %rdi
	leaq	.LC19(%rip), %rsi
	call	BIO_puts@PLT
	movq	8(%r12), %rax
	movl	-52(%rbp), %ecx
	movl	%r15d, -60(%rbp)
	xorl	%r12d, %r12d
	movq	%r14, -72(%rbp)
	movq	8(%rax), %rax
	addl	$2, %ecx
	movl	%ecx, -56(%rbp)
	movq	%rax, %r15
	movq	%r15, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r12d
	jge	.L164
.L149:
	movl	%r12d, %esi
	movq	%r15, %rdi
	call	OPENSSL_sk_value@PLT
	movl	-56(%rbp), %edx
	leaq	.LC1(%rip), %rcx
	movq	%rbx, %rdi
	movq	%rax, %r14
	leaq	.LC20(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movl	(%r14), %eax
	testl	%eax, %eax
	je	.L143
	cmpl	$1, %eax
	jne	.L145
	movq	8(%r14), %rax
	xorl	%edx, %edx
	movl	%r13d, %esi
	movq	%rbx, %rdi
	movq	(%rax), %rcx
	call	i2r_address
	testl	%eax, %eax
	je	.L148
	leaq	.LC22(%rip), %rsi
	movq	%rbx, %rdi
	call	BIO_puts@PLT
	movq	8(%r14), %rax
	movl	%r13d, %esi
	movq	%rbx, %rdi
	movl	$255, %edx
	movq	8(%rax), %rcx
	call	i2r_address
	testl	%eax, %eax
	jne	.L165
.L148:
	addq	$40, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L165:
	.cfi_restore_state
	leaq	.LC23(%rip), %rsi
	movq	%rbx, %rdi
	call	BIO_puts@PLT
	.p2align 4,,10
	.p2align 3
.L145:
	movq	%r15, %rdi
	addl	$1, %r12d
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r12d
	jl	.L149
.L164:
	movl	-60(%rbp), %r15d
	movq	-72(%rbp), %r14
	.p2align 4,,10
	.p2align 3
.L141:
	addl	$1, %r15d
.L167:
	movq	%r14, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r15d
	jl	.L150
.L162:
	addq	$40, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L154:
	.cfi_restore_state
	xorl	%r13d, %r13d
.L123:
	movl	-52(%rbp), %edx
	movl	%r13d, %r8d
	leaq	.LC1(%rip), %rcx
	movq	%rbx, %rdi
	leaq	.LC8(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L131:
	cmpb	$4, %dl
	jne	.L134
	leaq	.LC12(%rip), %rsi
	movq	%rbx, %rdi
	call	BIO_puts@PLT
	movq	8(%r12), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L166
	.p2align 4,,10
	.p2align 3
.L139:
	leaq	.LC18(%rip), %rsi
	movq	%rbx, %rdi
	addl	$1, %r15d
	call	BIO_puts@PLT
	jmp	.L167
	.p2align 4,,10
	.p2align 3
.L129:
	cmpb	$66, %dl
	je	.L136
	cmpb	$-128, %dl
	jne	.L168
	leaq	.LC16(%rip), %rsi
	movq	%rbx, %rdi
	call	BIO_puts@PLT
	jmp	.L127
.L168:
	cmpb	$65, %dl
	jne	.L134
	leaq	.LC14(%rip), %rsi
	movq	%rbx, %rdi
	call	BIO_puts@PLT
	jmp	.L127
	.p2align 4,,10
	.p2align 3
.L163:
	cmpl	$2, %r13d
	jne	.L123
	movl	-52(%rbp), %edx
	leaq	.LC1(%rip), %rcx
	movq	%rbx, %rdi
	xorl	%eax, %eax
	leaq	.LC7(%rip), %rsi
	movl	$2, %r13d
	call	BIO_printf@PLT
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L143:
	movq	8(%r14), %rcx
	xorl	%edx, %edx
	movl	%r13d, %esi
	movq	%rbx, %rdi
	call	i2r_address
	testl	%eax, %eax
	je	.L148
	movq	8(%r14), %rax
	leaq	.LC21(%rip), %rsi
	movq	%rbx, %rdi
	movl	(%rax), %ecx
	movq	16(%rax), %rax
	leal	0(,%rcx,8), %edx
	andl	$7, %eax
	subl	%eax, %edx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L145
	.p2align 4,,10
	.p2align 3
.L136:
	leaq	.LC15(%rip), %rsi
	movq	%rbx, %rdi
	call	BIO_puts@PLT
	jmp	.L127
	.p2align 4,,10
	.p2align 3
.L134:
	leaq	.LC17(%rip), %rsi
	movq	%rbx, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L127
	.p2align 4,,10
	.p2align 3
.L132:
	leaq	.LC9(%rip), %rsi
	movq	%rbx, %rdi
	call	BIO_puts@PLT
	jmp	.L127
	.p2align 4,,10
	.p2align 3
.L130:
	leaq	.LC11(%rip), %rsi
	movq	%rbx, %rdi
	call	BIO_puts@PLT
	jmp	.L127
	.p2align 4,,10
	.p2align 3
.L128:
	leaq	.LC13(%rip), %rsi
	movq	%rbx, %rdi
	call	BIO_puts@PLT
	jmp	.L127
	.cfi_endproc
.LFE1343:
	.size	i2r_IPAddrBlocks, .-i2r_IPAddrBlocks
	.p2align 4
	.type	extract_min_max, @function
extract_min_max:
.LFB1355:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%ecx, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	testq	%rsi, %rsi
	sete	%cl
	testq	%rdx, %rdx
	sete	%al
	orb	%al, %cl
	jne	.L183
	movq	%rdi, %rbx
	testq	%rdi, %rdi
	je	.L183
	movl	(%rbx), %eax
	movq	%rsi, %rdi
	movq	%rdx, %r12
	testl	%eax, %eax
	je	.L173
	cmpl	$1, %eax
	jne	.L183
	movq	8(%rbx), %rax
	xorl	%ecx, %ecx
	movl	%r13d, %edx
	movq	(%rax), %rsi
	call	addr_expand
	testl	%eax, %eax
	je	.L183
	movq	8(%rbx), %rax
	movq	8(%rax), %rsi
.L184:
	movl	%r13d, %edx
	movq	%r12, %rdi
	movl	$255, %ecx
	call	addr_expand
	testl	%eax, %eax
	setne	%al
	addq	$8, %rsp
	popq	%rbx
	movzbl	%al, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L183:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L173:
	.cfi_restore_state
	movq	8(%rbx), %rsi
	xorl	%ecx, %ecx
	movl	%r13d, %edx
	call	addr_expand
	testl	%eax, %eax
	je	.L183
	movq	8(%rbx), %rsi
	jmp	.L184
	.cfi_endproc
.LFE1355:
	.size	extract_min_max, .-extract_min_max
	.p2align 4
	.type	v6IPAddressOrRange_cmp, @function
v6IPAddressOrRange_cmp:
.LFB1346:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	(%rdi), %rbx
	movq	(%rsi), %r12
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	(%rbx), %eax
	testl	%eax, %eax
	je	.L186
	cmpl	$1, %eax
	jne	.L206
	movq	8(%rbx), %rax
	xorl	%ecx, %ecx
	leaq	-80(%rbp), %rdi
	movl	$16, %edx
	movq	(%rax), %rsi
	call	addr_expand
	testl	%eax, %eax
	je	.L191
	movl	(%r12), %eax
	movl	$128, %ebx
	testl	%eax, %eax
	jne	.L207
.L192:
	leaq	-64(%rbp), %r13
	movq	8(%r12), %rsi
	xorl	%ecx, %ecx
	movl	$16, %edx
	movq	%r13, %rdi
	call	addr_expand
	testl	%eax, %eax
	je	.L191
	movq	8(%r12), %rax
	movl	(%rax), %ecx
	movq	16(%rax), %rax
	leal	0(,%rcx,8), %r12d
	andl	$7, %eax
	subl	%eax, %r12d
	jmp	.L194
	.p2align 4,,10
	.p2align 3
.L206:
	xorl	%ebx, %ebx
.L188:
	movl	(%r12), %eax
	testl	%eax, %eax
	je	.L192
.L207:
	cmpl	$1, %eax
	je	.L193
	xorl	%r12d, %r12d
	leaq	-64(%rbp), %r13
.L194:
	leaq	-80(%rbp), %rdi
	movl	$16, %edx
	movq	%r13, %rsi
	subl	%r12d, %ebx
	call	memcmp@PLT
	testl	%eax, %eax
	cmove	%ebx, %eax
.L185:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L208
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L193:
	.cfi_restore_state
	movq	8(%r12), %rax
	leaq	-64(%rbp), %r13
	xorl	%ecx, %ecx
	movl	$16, %edx
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	addr_expand
	testl	%eax, %eax
	je	.L191
	movl	$128, %r12d
	jmp	.L194
	.p2align 4,,10
	.p2align 3
.L186:
	movq	8(%rbx), %rsi
	xorl	%ecx, %ecx
	leaq	-80(%rbp), %rdi
	movl	$16, %edx
	call	addr_expand
	testl	%eax, %eax
	je	.L191
	movq	8(%rbx), %rax
	movl	(%rax), %edx
	movq	16(%rax), %rax
	leal	0(,%rdx,8), %ebx
	andl	$7, %eax
	subl	%eax, %ebx
	jmp	.L188
	.p2align 4,,10
	.p2align 3
.L191:
	movl	$-1, %eax
	jmp	.L185
.L208:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1346:
	.size	v6IPAddressOrRange_cmp, .-v6IPAddressOrRange_cmp
	.p2align 4
	.type	range_should_be_prefix, @function
range_should_be_prefix:
.LFB1347:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edx, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	memcmp@PLT
	testl	%eax, %eax
	jle	.L250
	leal	-1(%r13), %eax
	testl	%r13d, %r13d
	je	.L211
	movzbl	(%rbx), %edi
	cmpb	%dil, (%r12)
	jne	.L228
	movzbl	1(%r12), %edi
	cmpb	%dil, 1(%rbx)
	jne	.L229
	movzbl	2(%r12), %edi
	cmpb	%dil, 2(%rbx)
	jne	.L230
	movzbl	3(%r12), %edi
	cmpb	%dil, 3(%rbx)
	jne	.L231
	cmpl	$4, %r13d
	je	.L213
	movzbl	4(%r12), %edi
	cmpb	%dil, 4(%rbx)
	jne	.L232
	movzbl	5(%r12), %edi
	cmpb	%dil, 5(%rbx)
	jne	.L233
	movzbl	6(%r12), %edi
	cmpb	%dil, 6(%rbx)
	jne	.L234
	movzbl	7(%r12), %edi
	cmpb	%dil, 7(%rbx)
	jne	.L235
	movzbl	8(%r12), %edi
	cmpb	%dil, 8(%rbx)
	jne	.L236
	movzbl	9(%r12), %edi
	cmpb	%dil, 9(%rbx)
	jne	.L237
	movzbl	10(%r12), %edi
	cmpb	%dil, 10(%rbx)
	jne	.L238
	movzbl	11(%r12), %esi
	cmpb	%sil, 11(%rbx)
	jne	.L239
	movzbl	12(%r12), %edi
	cmpb	%dil, 12(%rbx)
	jne	.L240
	movzbl	13(%r12), %esi
	cmpb	%sil, 13(%rbx)
	jne	.L241
	movzbl	14(%r12), %edi
	cmpb	%dil, 14(%rbx)
	jne	.L242
	cmpl	$16, %r13d
	jne	.L243
	movzbl	15(%r12), %esi
	movl	$15, %edx
	cmpb	%sil, 15(%rbx)
	cmovne	%edx, %r13d
.L213:
	cltq
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L258:
	cmpb	$-1, (%r12,%rax)
	jne	.L216
	subq	$1, %rax
	cmpl	$-1, %eax
	je	.L257
.L215:
	cmpb	$0, (%rbx,%rax)
	movl	%eax, %edx
	je	.L258
.L216:
	cmpl	%edx, %r13d
	jl	.L250
	jg	.L257
	movslq	%r13d, %rax
	movzbl	(%rbx,%rax), %r8d
	movzbl	(%r12,%rax), %edi
	movl	%r8d, %esi
	xorl	%edi, %esi
	cmpb	$31, %sil
	ja	.L219
	movl	$-1, %eax
	cmpb	%dil, %r8b
	je	.L209
	leaq	.L221(%rip), %rcx
	movzbl	%sil, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L221:
	.long	.L250-.L221
	.long	.L247-.L221
	.long	.L250-.L221
	.long	.L224-.L221
	.long	.L250-.L221
	.long	.L250-.L221
	.long	.L250-.L221
	.long	.L223-.L221
	.long	.L250-.L221
	.long	.L250-.L221
	.long	.L250-.L221
	.long	.L250-.L221
	.long	.L250-.L221
	.long	.L250-.L221
	.long	.L250-.L221
	.long	.L222-.L221
	.long	.L250-.L221
	.long	.L250-.L221
	.long	.L250-.L221
	.long	.L250-.L221
	.long	.L250-.L221
	.long	.L250-.L221
	.long	.L250-.L221
	.long	.L250-.L221
	.long	.L250-.L221
	.long	.L250-.L221
	.long	.L250-.L221
	.long	.L250-.L221
	.long	.L250-.L221
	.long	.L250-.L221
	.long	.L250-.L221
	.long	.L220-.L221
	.text
.L250:
	movl	$-1, %eax
.L209:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L224:
	.cfi_restore_state
	movl	$6, %eax
.L225:
	testb	%sil, %r8b
	jne	.L250
	andl	%esi, %edi
	leal	(%rax,%r13,8), %eax
	movl	$-1, %edx
	cmpb	%dil, %sil
	cmovne	%edx, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L247:
	.cfi_restore_state
	movl	$7, %eax
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L219:
	cmpb	$63, %sil
	je	.L248
	cmpb	$127, %sil
	jne	.L250
	movl	$1, %eax
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L248:
	movl	$2, %eax
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L257:
	addq	$8, %rsp
	leal	0(,%r13,8), %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L220:
	.cfi_restore_state
	movl	$3, %eax
	jmp	.L225
.L223:
	movl	$5, %eax
	jmp	.L225
.L222:
	movl	$4, %eax
	jmp	.L225
.L211:
	cmpl	$-1, %eax
	jne	.L213
	xorl	%eax, %eax
	jmp	.L209
.L228:
	xorl	%r13d, %r13d
	jmp	.L213
.L234:
	movl	$6, %r13d
	jmp	.L213
.L242:
	movl	$14, %r13d
	jmp	.L213
.L229:
	movl	$1, %r13d
	jmp	.L213
.L230:
	movl	$2, %r13d
	jmp	.L213
.L231:
	movl	$3, %r13d
	jmp	.L213
.L232:
	movl	$4, %r13d
	jmp	.L213
.L233:
	movl	$5, %r13d
	jmp	.L213
.L235:
	movl	$7, %r13d
	jmp	.L213
.L236:
	movl	$8, %r13d
	jmp	.L213
.L237:
	movl	$9, %r13d
	jmp	.L213
.L238:
	movl	$10, %r13d
	jmp	.L213
.L239:
	movl	$11, %r13d
	jmp	.L213
.L240:
	movl	$12, %r13d
	jmp	.L213
.L241:
	movl	$13, %r13d
	jmp	.L213
.L243:
	movl	$15, %r13d
	jmp	.L213
	.cfi_endproc
.LFE1347:
	.size	range_should_be_prefix, .-range_should_be_prefix
	.p2align 4
	.type	make_addressRange.part.0, @function
make_addressRange.part.0:
.LFB1372:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%ecx, %r12d
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	%rdi, -56(%rbp)
	leaq	IPAddressOrRange_it(%rip), %rdi
	call	ASN1_item_new@PLT
	movq	%rax, %r15
	xorl	%eax, %eax
	testq	%r15, %r15
	je	.L259
	movl	$1, (%r15)
	leaq	IPAddressRange_it(%rip), %rdi
	call	ASN1_item_new@PLT
	movq	%rax, 8(%r15)
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L261
	cmpq	$0, (%rax)
	je	.L342
.L262:
	cmpq	$0, 8(%rcx)
	je	.L343
.L263:
	testl	%r12d, %r12d
	je	.L264
	movslq	%r12d, %r9
	leaq	-1(%r13,%r9), %rbx
	cmpb	$0, (%rbx)
	jne	.L282
	leal	-1(%r12), %edx
	movslq	%edx, %rax
	leaq	-1(%r13,%rax), %rbx
	cmpb	$0, (%rbx)
	jne	.L265
	leal	-2(%r12), %edx
	movslq	%edx, %rax
	leaq	-1(%r13,%rax), %rbx
	cmpb	$0, (%rbx)
	jne	.L265
	leal	-3(%r12), %edx
	movslq	%edx, %rax
	leaq	-1(%r13,%rax), %rbx
	cmpb	$0, (%rbx)
	jne	.L265
	movl	%r12d, %edx
	subl	$4, %edx
	je	.L266
	movslq	%edx, %rax
	leaq	-1(%r13,%rax), %rbx
	cmpb	$0, (%rbx)
	jne	.L265
	leal	-5(%r12), %edx
	movslq	%edx, %rax
	leaq	-1(%r13,%rax), %rbx
	cmpb	$0, (%rbx)
	jne	.L265
	leal	-6(%r12), %edx
	movslq	%edx, %rax
	leaq	-1(%r13,%rax), %rbx
	cmpb	$0, (%rbx)
	jne	.L265
	leal	-7(%r12), %edx
	movslq	%edx, %rax
	leaq	-1(%r13,%rax), %rbx
	cmpb	$0, (%rbx)
	jne	.L265
	leal	-8(%r12), %edx
	movslq	%edx, %rax
	leaq	-1(%r13,%rax), %rbx
	cmpb	$0, (%rbx)
	jne	.L265
	leal	-9(%r12), %edx
	movslq	%edx, %rax
	leaq	-1(%r13,%rax), %rbx
	cmpb	$0, (%rbx)
	jne	.L265
	leal	-10(%r12), %edx
	movslq	%edx, %rax
	leaq	-1(%r13,%rax), %rbx
	cmpb	$0, (%rbx)
	jne	.L265
	leal	-11(%r12), %edx
	movslq	%edx, %rax
	leaq	-1(%r13,%rax), %rbx
	cmpb	$0, (%rbx)
	jne	.L265
	leal	-12(%r12), %edx
	movslq	%edx, %rax
	leaq	-1(%r13,%rax), %rbx
	cmpb	$0, (%rbx)
	jne	.L265
	leal	-13(%r12), %edx
	movslq	%edx, %rax
	leaq	-1(%r13,%rax), %rbx
	cmpb	$0, (%rbx)
	jne	.L265
	leal	-14(%r12), %edx
	movslq	%edx, %rax
	leaq	-1(%r13,%rax), %rbx
	cmpb	$0, (%rbx)
	jne	.L265
	cmpb	$0, 0(%r13)
	jne	.L283
.L266:
	movq	(%rcx), %rdi
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r9, -64(%rbp)
	call	ASN1_BIT_STRING_set@PLT
	movq	-64(%rbp), %r9
	testl	%eax, %eax
	jne	.L338
.L261:
	leaq	IPAddressOrRange_it(%rip), %rsi
	movq	%r15, %rdi
	call	ASN1_item_free@PLT
	xorl	%eax, %eax
.L259:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L283:
	.cfi_restore_state
	movq	%r13, %rbx
	movl	$1, %edx
	.p2align 4,,10
	.p2align 3
.L265:
	movq	(%rcx), %rdi
	movq	%r13, %rsi
	movq	%r9, -64(%rbp)
	call	ASN1_BIT_STRING_set@PLT
	movq	-64(%rbp), %r9
	testl	%eax, %eax
	je	.L261
	movq	8(%r15), %r10
	movl	$1, %ecx
	movl	$255, %edx
	movq	(%r10), %r11
	movq	16(%r11), %rdi
	andq	$-8, %rdi
	orq	$8, %rdi
	movq	%rdi, 16(%r11)
	movzbl	(%rbx), %esi
	movl	%esi, %eax
	testb	$127, %al
	je	.L344
	.p2align 4,,10
	.p2align 3
.L268:
	addl	$1, %ecx
	movl	%edx, %eax
	shrl	%cl, %eax
	testl	%esi, %eax
	jne	.L268
	movl	$8, %eax
	subl	%ecx, %eax
	cltq
.L276:
	orq	%rax, %rdi
	movq	%rdi, 16(%r11)
.L278:
	leaq	-1(%r14,%r9), %rbx
	cmpb	$-1, (%rbx)
	jne	.L269
	leal	-1(%r12), %eax
	movslq	%eax, %rdx
	leaq	-1(%r14,%rdx), %rbx
	cmpb	$-1, (%rbx)
	jne	.L296
	leal	-2(%r12), %eax
	movslq	%eax, %rdx
	leaq	-1(%r14,%rdx), %rbx
	cmpb	$-1, (%rbx)
	jne	.L296
	leal	-3(%r12), %eax
	movslq	%eax, %rdx
	leaq	-1(%r14,%rdx), %rbx
	cmpb	$-1, (%rbx)
	jne	.L296
	movl	%r12d, %eax
	subl	$4, %eax
	je	.L270
	movslq	%eax, %rdx
	leaq	-1(%r14,%rdx), %rbx
	cmpb	$-1, (%rbx)
	jne	.L296
	leal	-5(%r12), %eax
	movslq	%eax, %rdx
	leaq	-1(%r14,%rdx), %rbx
	cmpb	$-1, (%rbx)
	jne	.L296
	leal	-6(%r12), %eax
	movslq	%eax, %rdx
	leaq	-1(%r14,%rdx), %rbx
	cmpb	$-1, (%rbx)
	jne	.L296
	leal	-7(%r12), %eax
	movslq	%eax, %rdx
	leaq	-1(%r14,%rdx), %rbx
	cmpb	$-1, (%rbx)
	jne	.L296
	leal	-8(%r12), %eax
	movslq	%eax, %rdx
	leaq	-1(%r14,%rdx), %rbx
	cmpb	$-1, (%rbx)
	jne	.L296
	leal	-9(%r12), %eax
	movslq	%eax, %rdx
	leaq	-1(%r14,%rdx), %rbx
	cmpb	$-1, (%rbx)
	jne	.L296
	leal	-10(%r12), %eax
	movslq	%eax, %rdx
	leaq	-1(%r14,%rdx), %rbx
	cmpb	$-1, (%rbx)
	jne	.L296
	leal	-11(%r12), %eax
	movslq	%eax, %rdx
	leaq	-1(%r14,%rdx), %rbx
	cmpb	$-1, (%rbx)
	jne	.L296
	leal	-12(%r12), %eax
	movslq	%eax, %rdx
	leaq	-1(%r14,%rdx), %rbx
	cmpb	$-1, (%rbx)
	jne	.L296
	leal	-13(%r12), %eax
	movslq	%eax, %rdx
	leaq	-1(%r14,%rdx), %rbx
	cmpb	$-1, (%rbx)
	jne	.L296
	subl	$14, %r12d
	movslq	%r12d, %rax
	leaq	-1(%r14,%rax), %rbx
	cmpb	$-1, (%rbx)
	jne	.L269
	cmpb	$-1, (%r14)
	movq	%r14, %rbx
	movl	$1, %r12d
	jne	.L269
.L270:
	movq	8(%r10), %rdi
	xorl	%edx, %edx
	movq	%r14, %rsi
	call	ASN1_BIT_STRING_set@PLT
	testl	%eax, %eax
	je	.L261
	movq	8(%r15), %rax
	movq	8(%rax), %rdx
	movq	16(%rdx), %rax
	andq	$-8, %rax
	orq	$8, %rax
	movq	%rax, 16(%rdx)
.L272:
	movq	-56(%rbp), %rax
	movq	%r15, (%rax)
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L338:
	.cfi_restore_state
	movq	8(%r15), %r10
	movq	(%r10), %rdx
	movq	16(%rdx), %rax
	andq	$-8, %rax
	orq	$8, %rax
	movq	%rax, 16(%rdx)
	jmp	.L278
	.p2align 4,,10
	.p2align 3
.L342:
	movq	%rax, -64(%rbp)
	call	ASN1_BIT_STRING_new@PLT
	movq	-64(%rbp), %rcx
	movq	%rax, (%rcx)
	testq	%rax, %rax
	je	.L261
	movq	8(%r15), %rcx
	jmp	.L262
	.p2align 4,,10
	.p2align 3
.L343:
	movq	%rcx, -64(%rbp)
	call	ASN1_BIT_STRING_new@PLT
	movq	-64(%rbp), %rcx
	movq	%rax, 8(%rcx)
	testq	%rax, %rax
	je	.L261
	movq	8(%r15), %rcx
	jmp	.L263
	.p2align 4,,10
	.p2align 3
.L296:
	movl	%eax, %r12d
.L269:
	movq	8(%r10), %rdi
	movl	%r12d, %edx
	movq	%r14, %rsi
	call	ASN1_BIT_STRING_set@PLT
	testl	%eax, %eax
	je	.L261
	movq	8(%r15), %rax
	movl	$1, %ecx
	movl	$255, %esi
	movq	8(%rax), %r9
	movq	16(%r9), %rax
	andq	$-8, %rax
	orq	$8, %rax
	movq	%rax, 16(%r9)
	movzbl	(%rbx), %edi
	movq	%rax, %r10
	movl	%edi, %eax
	andl	$127, %eax
	cmpb	$127, %al
	je	.L345
	.p2align 4,,10
	.p2align 3
.L271:
	addl	$1, %ecx
	movl	%esi, %eax
	shrl	%cl, %eax
	movl	%eax, %edx
	andl	%edi, %edx
	cmpl	%edx, %eax
	jne	.L271
	movl	$8, %edx
	subl	%ecx, %edx
	movslq	%edx, %rax
.L274:
	orq	%r10, %rax
	movq	%rax, 16(%r9)
	jmp	.L272
.L282:
	movl	%r12d, %edx
	jmp	.L265
.L344:
	movl	$7, %eax
	jmp	.L276
.L264:
	movq	(%rcx), %rdi
	xorl	%edx, %edx
	movq	%r13, %rsi
	call	ASN1_BIT_STRING_set@PLT
	testl	%eax, %eax
	je	.L261
	movq	8(%r15), %r10
	movq	(%r10), %rdx
	movq	16(%rdx), %rax
	andq	$-8, %rax
	orq	$8, %rax
	movq	%rax, 16(%rdx)
	jmp	.L270
.L345:
	movl	$7, %eax
	jmp	.L274
	.cfi_endproc
.LFE1372:
	.size	make_addressRange.part.0, .-make_addressRange.part.0
	.p2align 4
	.type	v4IPAddressOrRange_cmp, @function
v4IPAddressOrRange_cmp:
.LFB1345:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	(%rdi), %rbx
	movq	(%rsi), %r12
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	(%rbx), %eax
	testl	%eax, %eax
	je	.L347
	cmpl	$1, %eax
	jne	.L367
	movq	8(%rbx), %rax
	xorl	%ecx, %ecx
	leaq	-80(%rbp), %rdi
	movl	$4, %edx
	movq	(%rax), %rsi
	call	addr_expand
	testl	%eax, %eax
	je	.L352
	movl	(%r12), %eax
	movl	$32, %ebx
	testl	%eax, %eax
	jne	.L368
.L353:
	leaq	-64(%rbp), %r13
	movq	8(%r12), %rsi
	xorl	%ecx, %ecx
	movl	$4, %edx
	movq	%r13, %rdi
	call	addr_expand
	testl	%eax, %eax
	je	.L352
	movq	8(%r12), %rax
	movl	(%rax), %ecx
	movq	16(%rax), %rax
	leal	0(,%rcx,8), %r12d
	andl	$7, %eax
	subl	%eax, %r12d
	jmp	.L355
	.p2align 4,,10
	.p2align 3
.L367:
	xorl	%ebx, %ebx
.L349:
	movl	(%r12), %eax
	testl	%eax, %eax
	je	.L353
.L368:
	cmpl	$1, %eax
	je	.L354
	xorl	%r12d, %r12d
	leaq	-64(%rbp), %r13
.L355:
	leaq	-80(%rbp), %rdi
	movl	$4, %edx
	movq	%r13, %rsi
	subl	%r12d, %ebx
	call	memcmp@PLT
	testl	%eax, %eax
	cmove	%ebx, %eax
.L346:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L369
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L354:
	.cfi_restore_state
	movq	8(%r12), %rax
	leaq	-64(%rbp), %r13
	xorl	%ecx, %ecx
	movl	$4, %edx
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	addr_expand
	testl	%eax, %eax
	je	.L352
	movl	$32, %r12d
	jmp	.L355
	.p2align 4,,10
	.p2align 3
.L347:
	movq	8(%rbx), %rsi
	xorl	%ecx, %ecx
	leaq	-80(%rbp), %rdi
	movl	$4, %edx
	call	addr_expand
	testl	%eax, %eax
	je	.L352
	movq	8(%rbx), %rax
	movl	(%rax), %edx
	movq	16(%rax), %rax
	leal	0(,%rdx,8), %ebx
	andl	$7, %eax
	subl	%eax, %ebx
	jmp	.L349
	.p2align 4,,10
	.p2align 3
.L352:
	movl	$-1, %eax
	jmp	.L346
.L369:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1345:
	.size	v4IPAddressOrRange_cmp, .-v4IPAddressOrRange_cmp
	.p2align 4
	.type	addr_contains.part.0, @function
addr_contains.part.0:
.LFB1371:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	leaq	-80(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	xorl	%r13d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	xorl	%ebx, %ebx
	subq	$120, %rsp
	movq	%rsi, -144(%rbp)
	movq	-144(%rbp), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-96(%rbp), %rax
	movq	%rax, -152(%rbp)
	leaq	-128(%rbp), %rax
	movq	%rax, -160(%rbp)
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jge	.L400
	.p2align 4,,10
	.p2align 3
.L381:
	movq	-144(%rbp), %rdi
	movl	%ebx, %esi
	call	OPENSSL_sk_value@PLT
	movq	-152(%rbp), %rsi
	movl	%r12d, %ecx
	movq	%r15, %rdx
	movq	%rax, %rdi
	call	extract_min_max
	testl	%eax, %eax
	je	.L383
.L380:
	movq	%r14, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%r13d, %eax
	jle	.L399
	movl	%r13d, %esi
	movq	%r14, %rdi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L399
	movl	(%rax), %eax
	testl	%eax, %eax
	je	.L375
	cmpl	$1, %eax
	jne	.L399
	movq	8(%r8), %rax
	movq	-160(%rbp), %rdi
	xorl	%ecx, %ecx
	movl	%r12d, %edx
	movq	%r8, -136(%rbp)
	movq	(%rax), %rsi
	call	addr_expand
	movq	-136(%rbp), %r8
	testl	%eax, %eax
	jne	.L401
	.p2align 4,,10
	.p2align 3
.L399:
	xorl	%eax, %eax
.L370:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L402
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L401:
	.cfi_restore_state
	movq	8(%r8), %rax
	movq	8(%rax), %rsi
.L398:
	leaq	-112(%rbp), %rdi
	movl	$255, %ecx
	movl	%r12d, %edx
	movq	%rdi, -136(%rbp)
	call	addr_expand
	movq	-136(%rbp), %rdi
	testl	%eax, %eax
	je	.L399
	movslq	%r12d, %rdx
	movq	%r15, %rsi
	movq	%rdx, -136(%rbp)
	call	memcmp@PLT
	movq	-136(%rbp), %rdx
	testl	%eax, %eax
	js	.L403
	movq	-152(%rbp), %rsi
	movq	-160(%rbp), %rdi
	call	memcmp@PLT
	testl	%eax, %eax
	jg	.L399
	movq	-144(%rbp), %rdi
	addl	$1, %ebx
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jl	.L381
.L400:
	movl	$1, %eax
	jmp	.L370
	.p2align 4,,10
	.p2align 3
.L403:
	addl	$1, %r13d
	jmp	.L380
	.p2align 4,,10
	.p2align 3
.L375:
	movq	8(%r8), %rsi
	movq	-160(%rbp), %rdi
	xorl	%ecx, %ecx
	movl	%r12d, %edx
	movq	%r8, -136(%rbp)
	call	addr_expand
	movq	-136(%rbp), %r8
	testl	%eax, %eax
	je	.L399
	movq	8(%r8), %rsi
	jmp	.L398
	.p2align 4,,10
	.p2align 3
.L383:
	movl	$-1, %eax
	jmp	.L370
.L402:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1371:
	.size	addr_contains.part.0, .-addr_contains.part.0
	.p2align 4
	.globl	d2i_IPAddressRange
	.type	d2i_IPAddressRange, @function
d2i_IPAddressRange:
.LFB1322:
	.cfi_startproc
	endbr64
	leaq	IPAddressRange_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE1322:
	.size	d2i_IPAddressRange, .-d2i_IPAddressRange
	.p2align 4
	.globl	i2d_IPAddressRange
	.type	i2d_IPAddressRange, @function
i2d_IPAddressRange:
.LFB1323:
	.cfi_startproc
	endbr64
	leaq	IPAddressRange_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE1323:
	.size	i2d_IPAddressRange, .-i2d_IPAddressRange
	.p2align 4
	.globl	IPAddressRange_new
	.type	IPAddressRange_new, @function
IPAddressRange_new:
.LFB1324:
	.cfi_startproc
	endbr64
	leaq	IPAddressRange_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE1324:
	.size	IPAddressRange_new, .-IPAddressRange_new
	.p2align 4
	.globl	IPAddressRange_free
	.type	IPAddressRange_free, @function
IPAddressRange_free:
.LFB1325:
	.cfi_startproc
	endbr64
	leaq	IPAddressRange_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE1325:
	.size	IPAddressRange_free, .-IPAddressRange_free
	.p2align 4
	.globl	d2i_IPAddressOrRange
	.type	d2i_IPAddressOrRange, @function
d2i_IPAddressOrRange:
.LFB1326:
	.cfi_startproc
	endbr64
	leaq	IPAddressOrRange_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE1326:
	.size	d2i_IPAddressOrRange, .-d2i_IPAddressOrRange
	.p2align 4
	.globl	i2d_IPAddressOrRange
	.type	i2d_IPAddressOrRange, @function
i2d_IPAddressOrRange:
.LFB1327:
	.cfi_startproc
	endbr64
	leaq	IPAddressOrRange_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE1327:
	.size	i2d_IPAddressOrRange, .-i2d_IPAddressOrRange
	.p2align 4
	.globl	IPAddressOrRange_new
	.type	IPAddressOrRange_new, @function
IPAddressOrRange_new:
.LFB1328:
	.cfi_startproc
	endbr64
	leaq	IPAddressOrRange_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE1328:
	.size	IPAddressOrRange_new, .-IPAddressOrRange_new
	.p2align 4
	.globl	IPAddressOrRange_free
	.type	IPAddressOrRange_free, @function
IPAddressOrRange_free:
.LFB1329:
	.cfi_startproc
	endbr64
	leaq	IPAddressOrRange_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE1329:
	.size	IPAddressOrRange_free, .-IPAddressOrRange_free
	.p2align 4
	.globl	d2i_IPAddressChoice
	.type	d2i_IPAddressChoice, @function
d2i_IPAddressChoice:
.LFB1330:
	.cfi_startproc
	endbr64
	leaq	IPAddressChoice_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE1330:
	.size	d2i_IPAddressChoice, .-d2i_IPAddressChoice
	.p2align 4
	.globl	i2d_IPAddressChoice
	.type	i2d_IPAddressChoice, @function
i2d_IPAddressChoice:
.LFB1331:
	.cfi_startproc
	endbr64
	leaq	IPAddressChoice_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE1331:
	.size	i2d_IPAddressChoice, .-i2d_IPAddressChoice
	.p2align 4
	.globl	IPAddressChoice_new
	.type	IPAddressChoice_new, @function
IPAddressChoice_new:
.LFB1332:
	.cfi_startproc
	endbr64
	leaq	IPAddressChoice_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE1332:
	.size	IPAddressChoice_new, .-IPAddressChoice_new
	.p2align 4
	.globl	IPAddressChoice_free
	.type	IPAddressChoice_free, @function
IPAddressChoice_free:
.LFB1333:
	.cfi_startproc
	endbr64
	leaq	IPAddressChoice_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE1333:
	.size	IPAddressChoice_free, .-IPAddressChoice_free
	.p2align 4
	.globl	d2i_IPAddressFamily
	.type	d2i_IPAddressFamily, @function
d2i_IPAddressFamily:
.LFB1334:
	.cfi_startproc
	endbr64
	leaq	IPAddressFamily_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE1334:
	.size	d2i_IPAddressFamily, .-d2i_IPAddressFamily
	.p2align 4
	.globl	i2d_IPAddressFamily
	.type	i2d_IPAddressFamily, @function
i2d_IPAddressFamily:
.LFB1335:
	.cfi_startproc
	endbr64
	leaq	IPAddressFamily_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE1335:
	.size	i2d_IPAddressFamily, .-i2d_IPAddressFamily
	.p2align 4
	.globl	IPAddressFamily_new
	.type	IPAddressFamily_new, @function
IPAddressFamily_new:
.LFB1336:
	.cfi_startproc
	endbr64
	leaq	IPAddressFamily_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE1336:
	.size	IPAddressFamily_new, .-IPAddressFamily_new
	.p2align 4
	.globl	X509v3_addr_get_afi
	.type	X509v3_addr_get_afi, @function
X509v3_addr_get_afi:
.LFB1339:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L422
	movq	(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L422
	movq	8(%rdx), %rcx
	xorl	%eax, %eax
	testq	%rcx, %rcx
	je	.L419
	cmpl	$1, (%rdx)
	jle	.L419
	movzwl	(%rcx), %eax
	rolw	$8, %ax
	movzwl	%ax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L419:
	ret
	.p2align 4,,10
	.p2align 3
.L422:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1339:
	.size	X509v3_addr_get_afi, .-X509v3_addr_get_afi
	.p2align 4
	.globl	X509v3_addr_add_inherit
	.type	X509v3_addr_add_inherit, @function
X509v3_addr_add_inherit:
.LFB1351:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	call	make_IPAddressFamily
	testq	%rax, %rax
	je	.L428
	movq	8(%rax), %r12
	movq	%rax, %rbx
	testq	%r12, %r12
	je	.L428
	movl	(%r12), %eax
	movq	8(%r12), %rdx
	cmpl	$1, %eax
	je	.L441
	testl	%eax, %eax
	jne	.L431
	movl	$1, %eax
	testq	%rdx, %rdx
	je	.L430
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L441:
	.cfi_restore_state
	testq	%rdx, %rdx
	je	.L430
.L428:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L431:
	.cfi_restore_state
	testq	%rdx, %rdx
	je	.L430
.L432:
	movl	$0, (%r12)
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L430:
	.cfi_restore_state
	call	ASN1_NULL_new@PLT
	movq	%rax, 8(%r12)
	testq	%rax, %rax
	je	.L428
	movq	8(%rbx), %r12
	jmp	.L432
	.cfi_endproc
.LFE1351:
	.size	X509v3_addr_add_inherit, .-X509v3_addr_add_inherit
	.p2align 4
	.globl	X509v3_addr_add_prefix
	.type	X509v3_addr_add_prefix, @function
X509v3_addr_add_prefix:
.LFB1353:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%r8d, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	make_IPAddressFamily
	testq	%rax, %rax
	je	.L445
	movq	%rax, %rbx
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L445
	movl	(%rax), %edx
	testl	%edx, %edx
	jne	.L446
	cmpq	$0, 8(%rax)
	jne	.L445
.L447:
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L445
	cmpl	$1, %r12d
	je	.L449
	cmpl	$2, %r12d
	jne	.L451
	leaq	v6IPAddressOrRange_cmp(%rip), %rsi
	movq	%rax, %rdi
	call	OPENSSL_sk_set_cmp_func@PLT
.L451:
	movq	8(%rbx), %rax
	movl	$1, (%rax)
	movq	%r15, 8(%rax)
.L448:
	leaq	-64(%rbp), %rdi
	movl	%r14d, %edx
	movq	%r13, %rsi
	call	make_addressPrefix
	testl	%eax, %eax
	je	.L445
	movq	-64(%rbp), %rsi
	movq	%r15, %rdi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	movl	%eax, -68(%rbp)
	je	.L466
	movl	$1, %eax
	jmp	.L442
	.p2align 4,,10
	.p2align 3
.L445:
	xorl	%eax, %eax
.L442:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L467
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L446:
	.cfi_restore_state
	cmpl	$1, %edx
	jne	.L447
	movq	8(%rax), %r15
	testq	%r15, %r15
	jne	.L448
	jmp	.L447
	.p2align 4,,10
	.p2align 3
.L466:
	movq	-64(%rbp), %rdi
	leaq	IPAddressOrRange_it(%rip), %rsi
	call	ASN1_item_free@PLT
	movl	-68(%rbp), %eax
	jmp	.L442
	.p2align 4,,10
	.p2align 3
.L449:
	leaq	v4IPAddressOrRange_cmp(%rip), %rsi
	movq	%rax, %rdi
	call	OPENSSL_sk_set_cmp_func@PLT
	jmp	.L451
.L467:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1353:
	.size	X509v3_addr_add_prefix, .-X509v3_addr_add_prefix
	.p2align 4
	.globl	X509v3_addr_add_range
	.type	X509v3_addr_add_range, @function
X509v3_addr_add_range:
.LFB1354:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	make_IPAddressFamily
	testq	%rax, %rax
	je	.L469
	movq	%rax, %r13
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L469
	movl	(%rax), %edx
	testl	%edx, %edx
	jne	.L470
	cmpq	$0, 8(%rax)
	jne	.L469
.L471:
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L469
	cmpl	$1, %ebx
	je	.L473
	cmpl	$2, %ebx
	jne	.L475
	leaq	v6IPAddressOrRange_cmp(%rip), %rsi
	movq	%rax, %rdi
	call	OPENSSL_sk_set_cmp_func@PLT
.L475:
	movq	8(%r13), %rax
	movl	$1, (%rax)
	movq	%r12, 8(%rax)
.L472:
	movl	$4, %r13d
	cmpl	$1, %ebx
	je	.L479
	xorl	%r13d, %r13d
	cmpl	$2, %ebx
	sete	%r13b
	sall	$4, %r13d
.L479:
	movl	%r13d, %edx
	movq	%r14, %rdi
	movq	%r15, %rsi
	call	range_should_be_prefix
	leaq	-64(%rbp), %rdi
	movl	%eax, %edx
	testl	%eax, %eax
	jns	.L499
	movl	%r13d, %ecx
	movq	%r15, %rdx
	movq	%r14, %rsi
	call	make_addressRange.part.0
.L477:
	testl	%eax, %eax
	je	.L468
	movq	-64(%rbp), %rsi
	movq	%r12, %rdi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	movl	%eax, -68(%rbp)
	je	.L500
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L468:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L501
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L470:
	.cfi_restore_state
	cmpl	$1, %edx
	jne	.L471
	movq	8(%rax), %r12
	testq	%r12, %r12
	jne	.L472
	jmp	.L471
	.p2align 4,,10
	.p2align 3
.L469:
	xorl	%eax, %eax
	jmp	.L468
	.p2align 4,,10
	.p2align 3
.L499:
	movq	%r14, %rsi
	call	make_addressPrefix
	jmp	.L477
	.p2align 4,,10
	.p2align 3
.L500:
	movq	-64(%rbp), %rdi
	leaq	IPAddressOrRange_it(%rip), %rsi
	call	ASN1_item_free@PLT
	movl	-68(%rbp), %eax
	jmp	.L468
	.p2align 4,,10
	.p2align 3
.L473:
	leaq	v4IPAddressOrRange_cmp(%rip), %rsi
	movq	%rax, %rdi
	call	OPENSSL_sk_set_cmp_func@PLT
	jmp	.L475
.L501:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1354:
	.size	X509v3_addr_add_range, .-X509v3_addr_add_range
	.p2align 4
	.globl	X509v3_addr_get_range
	.type	X509v3_addr_get_range, @function
X509v3_addr_get_range:
.LFB1356:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r9
	movq	%rcx, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	cmpl	$1, %esi
	je	.L506
	xorl	%r12d, %r12d
	cmpl	$2, %esi
	jne	.L502
	movl	$16, %r12d
.L503:
	testq	%rdi, %rdi
	je	.L505
	testq	%r9, %r9
	je	.L505
	testq	%rdx, %rdx
	je	.L505
	cmpl	%r8d, %r12d
	jg	.L505
	cmpl	$1, (%rdi)
	jbe	.L509
.L505:
	xorl	%r12d, %r12d
.L502:
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L506:
	.cfi_restore_state
	movl	$4, %r12d
	jmp	.L503
	.p2align 4,,10
	.p2align 3
.L509:
	movl	%r12d, %ecx
	movq	%r9, %rsi
	call	extract_min_max
	testl	%eax, %eax
	jne	.L502
	jmp	.L505
	.cfi_endproc
.LFE1356:
	.size	X509v3_addr_get_range, .-X509v3_addr_get_range
	.p2align 4
	.globl	X509v3_addr_is_canonical
	.type	X509v3_addr_is_canonical, @function
X509v3_addr_is_canonical:
.LFB1358:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$200, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L528
	movq	%rdi, %r14
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L607:
	movq	%r14, %rdi
	call	OPENSSL_sk_num@PLT
	subl	$1, %eax
	cmpl	%ebx, %eax
	jle	.L610
	movl	%ebx, %esi
	movq	%r14, %rdi
	addl	$1, %ebx
	call	OPENSSL_sk_value@PLT
	movl	%ebx, %esi
	movq	%r14, %rdi
	movq	%rax, %r12
	call	OPENSSL_sk_value@PLT
	movq	%rax, %r8
	movq	(%r12), %rax
	movq	(%r8), %rcx
	movslq	(%rax), %r13
	movq	8(%rax), %rdi
	movl	(%rcx), %r12d
	movq	8(%rcx), %rsi
	cmpl	%r12d, %r13d
	movslq	%r12d, %rdx
	cmovle	%r13, %rdx
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L513
	cmpl	%r12d, %r13d
	js	.L607
	.p2align 4,,10
	.p2align 3
.L510:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L611
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L513:
	.cfi_restore_state
	js	.L607
.L529:
	xorl	%eax, %eax
	jmp	.L510
	.p2align 4,,10
	.p2align 3
.L610:
	leaq	-112(%rbp), %rax
	movq	%r14, -176(%rbp)
	movl	$0, -164(%rbp)
	movq	%rax, -136(%rbp)
.L515:
	movq	-176(%rbp), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, -164(%rbp)
	jge	.L528
	movl	-164(%rbp), %esi
	movq	-176(%rbp), %rdi
	call	OPENSSL_sk_value@PLT
	testq	%rax, %rax
	je	.L529
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L530
	movq	8(%rdx), %rcx
	xorl	%r12d, %r12d
	testq	%rcx, %rcx
	je	.L518
	cmpl	$1, (%rdx)
	jle	.L518
	movzwl	(%rcx), %edx
	movl	$4, %r12d
	movl	%edx, %ecx
	rolw	$8, %cx
	cmpw	$256, %dx
	je	.L518
	cmpw	$2, %cx
	movl	$16, %r12d
	movl	$0, %ebx
	cmovne	%ebx, %r12d
.L518:
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L529
	movl	(%rax), %edx
	testl	%edx, %edx
	je	.L520
	cmpl	$1, %edx
	jne	.L529
	movq	8(%rax), %r15
	movq	%r15, %rdi
	call	OPENSSL_sk_num@PLT
	testl	%eax, %eax
	je	.L529
	leal	-1(%r12), %eax
	xorl	%ebx, %ebx
	movl	%r12d, %r14d
	movl	%eax, -168(%rbp)
	leal	-2(%r12), %eax
	movl	%eax, -180(%rbp)
	leal	-3(%r12), %eax
	movl	%eax, -184(%rbp)
	leal	-4(%r12), %eax
	movl	%eax, -188(%rbp)
	leal	-5(%r12), %eax
	movl	%eax, -192(%rbp)
	leal	-6(%r12), %eax
	movl	%eax, -196(%rbp)
	leal	-7(%r12), %eax
	movl	%eax, -200(%rbp)
	leal	-8(%r12), %eax
	movl	%eax, -204(%rbp)
	leal	-9(%r12), %eax
	movl	%eax, -208(%rbp)
	leal	-10(%r12), %eax
	movl	%eax, -212(%rbp)
	leal	-11(%r12), %eax
	movl	%eax, -216(%rbp)
	leal	-12(%r12), %eax
	movl	%eax, -220(%rbp)
	leal	-13(%r12), %eax
	movl	%eax, -224(%rbp)
	leal	-14(%r12), %eax
	movl	%eax, -228(%rbp)
	leal	-15(%r12), %eax
	movl	%eax, -232(%rbp)
	leal	-16(%r12), %eax
	movl	%eax, -236(%rbp)
	.p2align 4,,10
	.p2align 3
.L582:
	movq	%r15, %rdi
	call	OPENSSL_sk_num@PLT
	subl	$1, %eax
	cmpl	%eax, %ebx
	jge	.L612
	movl	%ebx, %esi
	movq	%r15, %rdi
	leaq	-128(%rbp), %r13
	addl	$1, %ebx
	call	OPENSSL_sk_value@PLT
	movl	%ebx, %esi
	movq	%r15, %rdi
	movq	%rax, %r12
	movq	%rax, -160(%rbp)
	call	OPENSSL_sk_value@PLT
	movl	%r14d, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	-136(%rbp), %rdx
	movq	%rax, -144(%rbp)
	call	extract_min_max
	testl	%eax, %eax
	je	.L529
	movq	-144(%rbp), %r8
	leaq	-80(%rbp), %rax
	leaq	-96(%rbp), %r12
	movl	%r14d, %ecx
	movq	%rax, %rdx
	movq	%r12, %rsi
	movq	%rax, -152(%rbp)
	movq	%r8, %rdi
	call	extract_min_max
	testl	%eax, %eax
	je	.L529
	movslq	%r14d, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rdx, -144(%rbp)
	call	memcmp@PLT
	testl	%eax, %eax
	jns	.L529
	movq	-144(%rbp), %rdx
	movq	-136(%rbp), %rsi
	movq	%r13, %rdi
	call	memcmp@PLT
	testl	%eax, %eax
	jg	.L529
	movq	-144(%rbp), %rdx
	movq	-152(%rbp), %rsi
	movq	%r12, %rdi
	call	memcmp@PLT
	testl	%eax, %eax
	jg	.L529
	movl	-168(%rbp), %eax
	cmpl	$-1, %eax
	je	.L529
	movslq	%eax, %rcx
	movq	-144(%rbp), %rdx
	movzbl	-96(%rbp,%rcx), %eax
	leal	-1(%rax), %esi
	testb	%al, %al
	movb	%sil, -96(%rbp,%rcx)
	jne	.L522
	movl	-180(%rbp), %eax
	cmpl	$-1, %eax
	je	.L522
	movslq	%eax, %rcx
	movzbl	-96(%rbp,%rcx), %eax
	leal	-1(%rax), %esi
	movb	%sil, -96(%rbp,%rcx)
	testb	%al, %al
	jne	.L522
	movl	-184(%rbp), %eax
	cmpl	$-1, %eax
	je	.L522
	movslq	%eax, %rcx
	movzbl	-96(%rbp,%rcx), %eax
	leal	-1(%rax), %esi
	movb	%sil, -96(%rbp,%rcx)
	testb	%al, %al
	jne	.L522
	movl	-188(%rbp), %eax
	cmpl	$-1, %eax
	je	.L522
	movslq	%eax, %rcx
	movzbl	-96(%rbp,%rcx), %eax
	leal	-1(%rax), %esi
	movb	%sil, -96(%rbp,%rcx)
	testb	%al, %al
	jne	.L522
	movl	-192(%rbp), %eax
	cmpl	$-1, %eax
	je	.L522
	movslq	%eax, %rcx
	movzbl	-96(%rbp,%rcx), %eax
	leal	-1(%rax), %esi
	movb	%sil, -96(%rbp,%rcx)
	testb	%al, %al
	jne	.L522
	movl	-196(%rbp), %eax
	cmpl	$-1, %eax
	je	.L522
	movslq	%eax, %rcx
	movzbl	-96(%rbp,%rcx), %eax
	leal	-1(%rax), %esi
	movb	%sil, -96(%rbp,%rcx)
	testb	%al, %al
	jne	.L522
	movl	-200(%rbp), %eax
	cmpl	$-1, %eax
	je	.L522
	movslq	%eax, %rcx
	movzbl	-96(%rbp,%rcx), %eax
	leal	-1(%rax), %esi
	movb	%sil, -96(%rbp,%rcx)
	testb	%al, %al
	jne	.L522
	movl	-204(%rbp), %eax
	cmpl	$-1, %eax
	je	.L522
	movslq	%eax, %rcx
	movzbl	-96(%rbp,%rcx), %eax
	leal	-1(%rax), %esi
	movb	%sil, -96(%rbp,%rcx)
	testb	%al, %al
	jne	.L522
	movl	-208(%rbp), %eax
	cmpl	$-1, %eax
	je	.L522
	movslq	%eax, %rcx
	movzbl	-96(%rbp,%rcx), %eax
	leal	-1(%rax), %esi
	movb	%sil, -96(%rbp,%rcx)
	testb	%al, %al
	jne	.L522
	movl	-212(%rbp), %eax
	cmpl	$-1, %eax
	je	.L522
	movslq	%eax, %rcx
	movzbl	-96(%rbp,%rcx), %eax
	leal	-1(%rax), %esi
	movb	%sil, -96(%rbp,%rcx)
	testb	%al, %al
	jne	.L522
	movl	-216(%rbp), %eax
	cmpl	$-1, %eax
	je	.L522
	movslq	%eax, %rcx
	movzbl	-96(%rbp,%rcx), %eax
	leal	-1(%rax), %esi
	movb	%sil, -96(%rbp,%rcx)
	testb	%al, %al
	jne	.L522
	movl	-220(%rbp), %eax
	cmpl	$-1, %eax
	je	.L522
	movslq	%eax, %rcx
	movzbl	-96(%rbp,%rcx), %eax
	leal	-1(%rax), %esi
	movb	%sil, -96(%rbp,%rcx)
	testb	%al, %al
	jne	.L522
	movl	-224(%rbp), %eax
	cmpl	$-1, %eax
	je	.L522
	movslq	%eax, %rcx
	movzbl	-96(%rbp,%rcx), %eax
	leal	-1(%rax), %esi
	movb	%sil, -96(%rbp,%rcx)
	testb	%al, %al
	jne	.L522
	movl	-228(%rbp), %eax
	cmpl	$-1, %eax
	je	.L522
	movslq	%eax, %rcx
	movzbl	-96(%rbp,%rcx), %eax
	leal	-1(%rax), %esi
	movb	%sil, -96(%rbp,%rcx)
	testb	%al, %al
	jne	.L522
	movl	-232(%rbp), %eax
	cmpl	$-1, %eax
	je	.L522
	movslq	%eax, %rcx
	movzbl	-96(%rbp,%rcx), %eax
	leal	-1(%rax), %esi
	movb	%sil, -96(%rbp,%rcx)
	testb	%al, %al
	jne	.L522
	movslq	-236(%rbp), %rax
	cmpl	$-1, %eax
	je	.L522
	subb	$1, -96(%rbp,%rax)
	.p2align 4,,10
	.p2align 3
.L522:
	movq	-136(%rbp), %rdi
	movq	%r12, %rsi
	call	memcmp@PLT
	testl	%eax, %eax
	jns	.L529
	movq	-160(%rbp), %rax
	cmpl	$1, (%rax)
	jne	.L582
	movq	-136(%rbp), %rsi
	movl	%r14d, %edx
	movq	%r13, %rdi
	call	range_should_be_prefix
	testl	%eax, %eax
	js	.L582
	xorl	%eax, %eax
	jmp	.L510
.L612:
	movq	%r15, %rdi
	call	OPENSSL_sk_num@PLT
	movq	%r15, %rdi
	leal	-1(%rax), %esi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L520
	cmpl	$1, (%rax)
	jne	.L520
	movq	-136(%rbp), %rbx
	leaq	-128(%rbp), %r13
	movl	%r14d, %ecx
	movq	%r13, %rsi
	movq	%rbx, %rdx
	call	extract_min_max
	testl	%eax, %eax
	je	.L529
	movslq	%r14d, %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	memcmp@PLT
	testl	%eax, %eax
	jg	.L529
	movl	%r14d, %edx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	range_should_be_prefix
	testl	%eax, %eax
	jns	.L529
	.p2align 4,,10
	.p2align 3
.L520:
	addl	$1, -164(%rbp)
	jmp	.L515
	.p2align 4,,10
	.p2align 3
.L528:
	movl	$1, %eax
	jmp	.L510
.L530:
	xorl	%r12d, %r12d
	jmp	.L518
.L611:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1358:
	.size	X509v3_addr_is_canonical, .-X509v3_addr_is_canonical
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC24:
	.string	"../deps/openssl/openssl/crypto/x509v3/v3_addr.c"
	.text
	.p2align 4
	.type	addr_validate_path_internal.part.0, @function
addr_validate_path_internal.part.0:
.LFB1374:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -64(%rbp)
	testq	%rdx, %rdx
	je	.L698
	movl	$0, -52(%rbp)
	movq	%rdx, %r13
	xorl	%ebx, %ebx
	movl	$-1, %r12d
.L614:
	movq	%r13, %rdi
	call	X509v3_addr_is_canonical
	testl	%eax, %eax
	jne	.L616
	testq	%r14, %r14
	je	.L695
	movl	$41, 176(%r14)
	movq	%r14, %rsi
	xorl	%edi, %edi
	movl	%r12d, 172(%r14)
	movq	%rbx, 184(%r14)
	call	*56(%r14)
	movl	%eax, -56(%rbp)
	testl	%eax, %eax
	je	.L695
	movq	%r13, %rdi
	leaq	IPAddressFamily_cmp(%rip), %rsi
	call	OPENSSL_sk_set_cmp_func@PLT
	movq	%r13, %rdi
	call	OPENSSL_sk_dup@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L699
.L621:
	movq	-64(%rbp), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, -52(%rbp)
	jge	.L700
	.p2align 4,,10
	.p2align 3
.L636:
	movl	-52(%rbp), %r15d
	movq	-64(%rbp), %rdi
	movl	%r15d, %esi
	call	OPENSSL_sk_value@PLT
	movq	288(%rax), %rdi
	movq	%rax, %rbx
	call	X509v3_addr_is_canonical
	testl	%eax, %eax
	jne	.L622
	testq	%r14, %r14
	je	.L659
	movl	$41, 176(%r14)
	movq	%r14, %rsi
	xorl	%edi, %edi
	movl	%r15d, 172(%r14)
	movq	%rbx, 184(%r14)
	call	*56(%r14)
	movl	%eax, -56(%rbp)
	testl	%eax, %eax
	je	.L615
.L622:
	movq	288(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L701
	leaq	IPAddressFamily_cmp(%rip), %rsi
	xorl	%r12d, %r12d
	call	OPENSSL_sk_set_cmp_func@PLT
	jmp	.L628
	.p2align 4,,10
	.p2align 3
.L629:
	movq	8(%rax), %rax
	cmpl	$1, (%rax)
	je	.L702
.L630:
	addl	$1, %r12d
.L628:
	movq	%r13, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r12d
	jge	.L626
	movl	%r12d, %esi
	movq	%r13, %rdi
	call	OPENSSL_sk_value@PLT
	movq	288(%rbx), %rdi
	movq	%rax, %rsi
	movq	%rax, %r15
	call	OPENSSL_sk_find@PLT
	movq	288(%rbx), %rdi
	movl	%eax, %esi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	jne	.L629
	movq	8(%r15), %rax
	cmpl	$1, (%rax)
	jne	.L630
.L697:
	testq	%r14, %r14
	je	.L659
	movl	-52(%rbp), %eax
	movq	%rbx, 184(%r14)
	movq	%r14, %rsi
	xorl	%edi, %edi
	movl	$46, 176(%r14)
	movl	%eax, 172(%r14)
	call	*56(%r14)
	movl	%eax, -56(%rbp)
	testl	%eax, %eax
	je	.L615
.L626:
	movq	-64(%rbp), %rdi
	addl	$1, -52(%rbp)
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, -52(%rbp)
	jl	.L636
.L700:
	movq	288(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L615
	xorl	%r12d, %r12d
	testq	%r14, %r14
	jne	.L637
	jmp	.L640
	.p2align 4,,10
	.p2align 3
.L641:
	movq	288(%rbx), %rdi
	addl	$1, %r12d
.L637:
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r12d
	jge	.L615
	movq	288(%rbx), %rdi
	movl	%r12d, %esi
	call	OPENSSL_sk_value@PLT
	movq	8(%rax), %rdx
	movl	(%rdx), %edx
	testl	%edx, %edx
	jne	.L641
	movq	%rax, %rsi
	movq	%r13, %rdi
	call	OPENSSL_sk_find@PLT
	testl	%eax, %eax
	js	.L641
	movl	-52(%rbp), %eax
	movq	%rbx, 184(%r14)
	movq	%r14, %rsi
	xorl	%edi, %edi
	movl	$46, 176(%r14)
	movl	%eax, 172(%r14)
	call	*56(%r14)
	movl	%eax, -56(%rbp)
	testl	%eax, %eax
	jne	.L641
	jmp	.L615
	.p2align 4,,10
	.p2align 3
.L616:
	movq	%r13, %rdi
	leaq	IPAddressFamily_cmp(%rip), %rsi
	call	OPENSSL_sk_set_cmp_func@PLT
	movq	%r13, %rdi
	call	OPENSSL_sk_dup@PLT
	movl	$1, -56(%rbp)
	movq	%rax, %r13
	testq	%rax, %rax
	jne	.L621
	movl	$1211, %r8d
	movl	$65, %edx
	movl	$166, %esi
	movl	$34, %edi
	leaq	.LC24(%rip), %rcx
	call	ERR_put_error@PLT
	testq	%r14, %r14
	je	.L695
.L643:
	movl	$17, 176(%r14)
	.p2align 4,,10
	.p2align 3
.L695:
	movl	$0, -56(%rbp)
	xorl	%r13d, %r13d
.L615:
	movq	%r13, %rdi
	call	OPENSSL_sk_free@PLT
	movl	-56(%rbp), %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L702:
	.cfi_restore_state
	movq	8(%r15), %rcx
	movl	(%rcx), %esi
	testl	%esi, %esi
	je	.L633
	movq	(%r15), %rsi
	testq	%rsi, %rsi
	je	.L652
	movq	8(%rsi), %rdi
	xorl	%edx, %edx
	testq	%rdi, %rdi
	je	.L632
	cmpl	$1, (%rsi)
	jle	.L632
	movzwl	(%rdi), %esi
	movl	$4, %edx
	movl	%esi, %edi
	rolw	$8, %di
	cmpw	$256, %si
	je	.L632
	xorl	%edx, %edx
	cmpw	$2, %di
	sete	%dl
	sall	$4, %edx
.L632:
	movq	8(%rcx), %rsi
	movq	8(%rax), %rdi
	testq	%rsi, %rsi
	je	.L633
	cmpq	%rdi, %rsi
	je	.L633
	movq	%r8, -72(%rbp)
	testq	%rdi, %rdi
	je	.L634
	call	addr_contains.part.0
	movq	-72(%rbp), %r8
	testl	%eax, %eax
	jne	.L633
.L634:
	testq	%r14, %r14
	je	.L659
	movl	-52(%rbp), %eax
	movq	%rbx, 184(%r14)
	movq	%r14, %rsi
	xorl	%edi, %edi
	movl	$46, 176(%r14)
	movl	%eax, 172(%r14)
	call	*56(%r14)
	movl	%eax, -56(%rbp)
	testl	%eax, %eax
	jne	.L630
	jmp	.L615
	.p2align 4,,10
	.p2align 3
.L633:
	movq	%r8, %rdx
	movl	%r12d, %esi
	movq	%r13, %rdi
	call	OPENSSL_sk_set@PLT
	jmp	.L630
	.p2align 4,,10
	.p2align 3
.L701:
	movq	%r13, %rdi
	xorl	%r12d, %r12d
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r12d
	jge	.L626
.L627:
	movq	%r13, %rdi
	movl	%r12d, %esi
	call	OPENSSL_sk_value@PLT
	movq	8(%rax), %rax
	movl	(%rax), %edi
	testl	%edi, %edi
	jne	.L697
	movq	%r13, %rdi
	addl	$1, %r12d
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r12d
	jl	.L627
	jmp	.L626
	.p2align 4,,10
	.p2align 3
.L652:
	xorl	%edx, %edx
	jmp	.L632
	.p2align 4,,10
	.p2align 3
.L698:
	movq	%rsi, %rdi
	xorl	%esi, %esi
	call	OPENSSL_sk_value@PLT
	movq	288(%rax), %r13
	movq	%rax, %rbx
	testq	%r13, %r13
	je	.L646
	movl	$1, -52(%rbp)
	xorl	%r12d, %r12d
	jmp	.L614
.L639:
	movq	288(%rbx), %rdi
	addl	$1, %r12d
.L640:
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r12d
	jge	.L615
	movq	288(%rbx), %rdi
	movl	%r12d, %esi
	call	OPENSSL_sk_value@PLT
	movq	8(%rax), %rdx
	movl	(%rdx), %ecx
	testl	%ecx, %ecx
	jne	.L639
	movq	%rax, %rsi
	movq	%r13, %rdi
	call	OPENSSL_sk_find@PLT
	testl	%eax, %eax
	js	.L639
.L659:
	movl	$0, -56(%rbp)
	jmp	.L615
.L646:
	movl	$1, -56(%rbp)
	jmp	.L615
.L699:
	movl	$1211, %r8d
	movl	$65, %edx
	movl	$166, %esi
	movl	$34, %edi
	leaq	.LC24(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L643
	.cfi_endproc
.LFE1374:
	.size	addr_validate_path_internal.part.0, .-addr_validate_path_internal.part.0
	.p2align 4
	.globl	X509v3_addr_canonize
	.type	X509v3_addr_canonize, @function
X509v3_addr_canonize:
.LFB1360:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-112(%rbp), %r15
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%r15, %rbx
	subq	$232, %rsp
	movq	%rdi, -176(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-80(%rbp), %rax
	movl	$0, -152(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L704
	.p2align 4,,10
	.p2align 3
.L722:
	addl	$1, -152(%rbp)
.L704:
	movq	-176(%rbp), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, -152(%rbp)
	jge	.L793
	movl	-152(%rbp), %esi
	movq	-176(%rbp), %rdi
	call	OPENSSL_sk_value@PLT
	movq	8(%rax), %rdx
	cmpl	$1, (%rdx)
	jne	.L722
	movq	(%rax), %rax
	movq	8(%rdx), %r13
	testq	%rax, %rax
	je	.L727
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L727
	cmpl	$1, (%rax)
	jle	.L727
	movzwl	(%rdx), %edx
	movl	%edx, %eax
	rolw	$8, %ax
	cmpw	$256, %dx
	jne	.L794
	movl	$3, %r15d
	movl	$4, %r14d
	.p2align 4,,10
	.p2align 3
.L706:
	movq	%r13, %rdi
	call	OPENSSL_sk_sort@PLT
	leal	-1(%r15), %eax
	movl	%r15d, -192(%rbp)
	movl	%eax, -196(%rbp)
	leal	-2(%r15), %eax
	movl	%eax, -200(%rbp)
	leal	-3(%r15), %eax
	movl	%eax, -212(%rbp)
	leal	-4(%r15), %eax
	movl	%eax, -216(%rbp)
	leal	-5(%r15), %eax
	movl	%eax, -220(%rbp)
	leal	-6(%r15), %eax
	movl	%eax, -224(%rbp)
	leal	-7(%r15), %eax
	movl	%eax, -228(%rbp)
	leal	-8(%r15), %eax
	movl	%eax, -232(%rbp)
	leal	-9(%r15), %eax
	movl	%eax, -236(%rbp)
	leal	-10(%r15), %eax
	movl	%eax, -244(%rbp)
	leal	-11(%r15), %eax
	movl	%eax, -248(%rbp)
	leal	-12(%r15), %eax
	movl	%eax, -252(%rbp)
	leal	-13(%r15), %eax
	movl	%eax, -256(%rbp)
	leal	-14(%r15), %eax
	movl	%eax, -260(%rbp)
	leal	-15(%r15), %eax
	movl	%eax, -240(%rbp)
	movl	$0, -148(%rbp)
.L707:
	movq	%r13, %rdi
	call	OPENSSL_sk_num@PLT
	subl	$1, %eax
	cmpl	%eax, -148(%rbp)
	jge	.L795
	movl	-148(%rbp), %r15d
	movq	%r13, %rdi
	movl	%r15d, %esi
	call	OPENSSL_sk_value@PLT
	leal	1(%r15), %edx
	movq	%r13, %rdi
	leaq	-128(%rbp), %r15
	movl	%edx, %esi
	movq	%rax, %r12
	movl	%edx, -188(%rbp)
	movq	%rax, -208(%rbp)
	call	OPENSSL_sk_value@PLT
	movl	%r14d, %ecx
	movq	%rbx, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, -168(%rbp)
	call	extract_min_max
	testl	%eax, %eax
	je	.L721
	leaq	-96(%rbp), %r12
	movq	-160(%rbp), %rdx
	movq	-168(%rbp), %rdi
	movl	%r14d, %ecx
	movq	%r12, %rsi
	call	extract_min_max
	testl	%eax, %eax
	je	.L721
	movslq	%r14d, %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movq	%rdx, -184(%rbp)
	call	memcmp@PLT
	testl	%eax, %eax
	jg	.L721
	movq	-184(%rbp), %rdx
	movq	-160(%rbp), %rsi
	movq	%r12, %rdi
	call	memcmp@PLT
	testl	%eax, %eax
	jg	.L721
	movq	-184(%rbp), %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	memcmp@PLT
	testl	%eax, %eax
	jns	.L721
	movl	-192(%rbp), %eax
	cmpl	$-1, %eax
	je	.L709
	movslq	%eax, %rcx
	movq	-184(%rbp), %rdx
	movzbl	-96(%rbp,%rcx), %eax
	leal	-1(%rax), %esi
	testb	%al, %al
	movb	%sil, -96(%rbp,%rcx)
	jne	.L710
	movl	-196(%rbp), %eax
	cmpl	$-1, %eax
	je	.L710
	movslq	%eax, %rcx
	movzbl	-96(%rbp,%rcx), %eax
	leal	-1(%rax), %esi
	movb	%sil, -96(%rbp,%rcx)
	testb	%al, %al
	jne	.L710
	movl	-200(%rbp), %eax
	cmpl	$-1, %eax
	je	.L710
	movslq	%eax, %rcx
	movzbl	-96(%rbp,%rcx), %eax
	leal	-1(%rax), %esi
	movb	%sil, -96(%rbp,%rcx)
	testb	%al, %al
	jne	.L710
	movl	-212(%rbp), %eax
	cmpl	$-1, %eax
	je	.L710
	movslq	%eax, %rcx
	movzbl	-96(%rbp,%rcx), %eax
	leal	-1(%rax), %esi
	movb	%sil, -96(%rbp,%rcx)
	testb	%al, %al
	jne	.L710
	movl	-216(%rbp), %eax
	cmpl	$-1, %eax
	je	.L710
	movslq	%eax, %rcx
	movzbl	-96(%rbp,%rcx), %eax
	leal	-1(%rax), %esi
	movb	%sil, -96(%rbp,%rcx)
	testb	%al, %al
	jne	.L710
	movl	-220(%rbp), %eax
	cmpl	$-1, %eax
	je	.L710
	movslq	%eax, %rcx
	movzbl	-96(%rbp,%rcx), %eax
	leal	-1(%rax), %esi
	movb	%sil, -96(%rbp,%rcx)
	testb	%al, %al
	jne	.L710
	movl	-224(%rbp), %eax
	cmpl	$-1, %eax
	je	.L710
	movslq	%eax, %rcx
	movzbl	-96(%rbp,%rcx), %eax
	leal	-1(%rax), %esi
	movb	%sil, -96(%rbp,%rcx)
	testb	%al, %al
	jne	.L710
	movl	-228(%rbp), %eax
	cmpl	$-1, %eax
	je	.L710
	movslq	%eax, %rcx
	movzbl	-96(%rbp,%rcx), %eax
	leal	-1(%rax), %esi
	movb	%sil, -96(%rbp,%rcx)
	testb	%al, %al
	jne	.L710
	movl	-232(%rbp), %eax
	cmpl	$-1, %eax
	je	.L710
	movslq	%eax, %rcx
	movzbl	-96(%rbp,%rcx), %eax
	leal	-1(%rax), %esi
	movb	%sil, -96(%rbp,%rcx)
	testb	%al, %al
	jne	.L710
	movl	-236(%rbp), %eax
	cmpl	$-1, %eax
	je	.L710
	movslq	%eax, %rcx
	movzbl	-96(%rbp,%rcx), %eax
	leal	-1(%rax), %esi
	movb	%sil, -96(%rbp,%rcx)
	testb	%al, %al
	jne	.L710
	movl	-244(%rbp), %eax
	cmpl	$-1, %eax
	je	.L710
	movslq	%eax, %rcx
	movzbl	-96(%rbp,%rcx), %eax
	leal	-1(%rax), %esi
	movb	%sil, -96(%rbp,%rcx)
	testb	%al, %al
	jne	.L710
	movl	-248(%rbp), %eax
	cmpl	$-1, %eax
	je	.L710
	movslq	%eax, %rcx
	movzbl	-96(%rbp,%rcx), %eax
	leal	-1(%rax), %esi
	movb	%sil, -96(%rbp,%rcx)
	testb	%al, %al
	jne	.L710
	movl	-252(%rbp), %eax
	cmpl	$-1, %eax
	je	.L710
	movslq	%eax, %rcx
	movzbl	-96(%rbp,%rcx), %eax
	leal	-1(%rax), %esi
	movb	%sil, -96(%rbp,%rcx)
	testb	%al, %al
	jne	.L710
	movl	-256(%rbp), %eax
	cmpl	$-1, %eax
	je	.L710
	movslq	%eax, %rcx
	movzbl	-96(%rbp,%rcx), %eax
	leal	-1(%rax), %esi
	movb	%sil, -96(%rbp,%rcx)
	testb	%al, %al
	jne	.L710
	movl	-260(%rbp), %eax
	cmpl	$-1, %eax
	je	.L710
	movslq	%eax, %rcx
	movzbl	-96(%rbp,%rcx), %eax
	leal	-1(%rax), %esi
	movb	%sil, -96(%rbp,%rcx)
	testb	%al, %al
	jne	.L710
	cmpl	$-1, -240(%rbp)
	je	.L710
	movslq	-240(%rbp), %rax
	subb	$1, -96(%rbp,%rax)
	.p2align 4,,10
	.p2align 3
.L710:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	memcmp@PLT
	testl	%eax, %eax
	je	.L709
	movl	-188(%rbp), %eax
	movl	%eax, -148(%rbp)
	jmp	.L707
	.p2align 4,,10
	.p2align 3
.L709:
	movq	-160(%rbp), %rsi
	movl	%r14d, %edx
	movq	%r15, %rdi
	call	range_should_be_prefix
	movl	%eax, %edx
	testl	%eax, %eax
	jns	.L796
	movq	-160(%rbp), %rdx
	leaq	-136(%rbp), %rdi
	movl	%r14d, %ecx
	movq	%r15, %rsi
	call	make_addressRange.part.0
.L715:
	testl	%eax, %eax
	jne	.L797
.L721:
	xorl	%eax, %eax
.L703:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L798
	addq	$232, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L797:
	.cfi_restore_state
	movq	-136(%rbp), %rdx
	movl	-148(%rbp), %esi
	movq	%r13, %rdi
	call	OPENSSL_sk_set@PLT
	movl	-188(%rbp), %esi
	movq	%r13, %rdi
	call	OPENSSL_sk_delete@PLT
	movq	-208(%rbp), %rdi
	leaq	IPAddressOrRange_it(%rip), %rsi
	call	ASN1_item_free@PLT
	movq	-168(%rbp), %rdi
	leaq	IPAddressOrRange_it(%rip), %rsi
	call	ASN1_item_free@PLT
	jmp	.L707
	.p2align 4,,10
	.p2align 3
.L727:
	movl	$-1, %r15d
	xorl	%r14d, %r14d
	jmp	.L706
	.p2align 4,,10
	.p2align 3
.L794:
	cmpw	$2, %ax
	movl	$-1, %ecx
	movl	$15, %edx
	movl	$16, %eax
	cmovne	%ecx, %edx
	movl	$0, %r14d
	cmove	%eax, %r14d
	movl	%edx, %r15d
	jmp	.L706
	.p2align 4,,10
	.p2align 3
.L796:
	leaq	-136(%rbp), %rdi
	movq	%r15, %rsi
	call	make_addressPrefix
	jmp	.L715
	.p2align 4,,10
	.p2align 3
.L793:
	movq	-176(%rbp), %rbx
	leaq	IPAddressFamily_cmp(%rip), %rsi
	movq	%rbx, %rdi
	call	OPENSSL_sk_set_cmp_func@PLT
	movq	%rbx, %rdi
	call	OPENSSL_sk_sort@PLT
	movq	%rbx, %rdi
	call	X509v3_addr_is_canonical
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
	jmp	.L703
	.p2align 4,,10
	.p2align 3
.L795:
	movq	%r13, %rdi
	call	OPENSSL_sk_num@PLT
	movq	%r13, %rdi
	leal	-1(%rax), %esi
	call	OPENSSL_sk_value@PLT
	testq	%rax, %rax
	je	.L722
	cmpl	$1, (%rax)
	jne	.L722
	movq	-160(%rbp), %r15
	leaq	-96(%rbp), %r12
	movl	%r14d, %ecx
	movq	%rax, %rdi
	movq	%r12, %rsi
	movq	%r15, %rdx
	call	extract_min_max
	testl	%eax, %eax
	je	.L721
	movslq	%r14d, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	memcmp@PLT
	testl	%eax, %eax
	jle	.L722
	jmp	.L721
.L798:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1360:
	.size	X509v3_addr_canonize, .-X509v3_addr_canonize
	.section	.rodata.str1.1
.LC25:
	.string	"IPv4"
.LC26:
	.string	"IPv6"
.LC27:
	.string	"IPv4-SAFI"
.LC28:
	.string	"IPv6-SAFI"
.LC29:
	.string	",value:"
.LC30:
	.string	",name:"
.LC31:
	.string	"section:"
.LC32:
	.string	" \t"
.LC33:
	.string	"inherit"
	.text
	.p2align 4
	.type	v2i_IPAddrBlocks, @function
v2i_IPAddrBlocks:
.LFB1361:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	IPAddressFamily_cmp(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	OPENSSL_sk_new@PLT
	movq	%rax, -136(%rbp)
	testq	%rax, %rax
	je	.L859
.L800:
	movq	%r14, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r12d
	jge	.L860
	movl	%r12d, %esi
	movq	%r14, %rdi
	leaq	v4addr_chars.21060(%rip), %r15
	call	OPENSSL_sk_value@PLT
	leaq	.LC25(%rip), %rsi
	movq	8(%rax), %rdi
	movq	%rax, %rbx
	call	name_cmp@PLT
	movl	$1, -124(%rbp)
	movl	$4, -120(%rbp)
	testl	%eax, %eax
	jne	.L861
.L802:
	movq	16(%rbx), %rdi
	movl	$952, %edx
	leaq	.LC24(%rip), %rsi
	call	CRYPTO_strdup@PLT
	xorl	%r10d, %r10d
	movq	%rax, %r13
.L807:
	testq	%r13, %r13
	je	.L862
	movl	$8, %ecx
	movq	%r13, %rsi
	leaq	.LC33(%rip), %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L809
	movl	-124(%rbp), %esi
	movq	-136(%rbp), %rdi
	movq	%r10, %rdx
	call	make_IPAddressFamily
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L810
	movq	8(%rax), %r15
	testq	%r15, %r15
	je	.L810
	movl	(%r15), %eax
	movq	8(%r15), %rdx
	cmpl	$1, %eax
	je	.L863
	testl	%eax, %eax
	jne	.L813
	testq	%rdx, %rdx
	je	.L812
.L814:
	movl	$970, %edx
	leaq	.LC24(%rip), %rsi
	movq	%r13, %rdi
	call	CRYPTO_free@PLT
.L816:
	addl	$1, %r12d
	jmp	.L800
	.p2align 4,,10
	.p2align 3
.L861:
	movq	8(%rbx), %rdi
	leaq	.LC26(%rip), %rsi
	leaq	v6addr_chars.21061(%rip), %r15
	call	name_cmp@PLT
	movl	$2, -124(%rbp)
	movl	$16, -120(%rbp)
	testl	%eax, %eax
	je	.L802
	movq	8(%rbx), %rdi
	leaq	.LC27(%rip), %rsi
	leaq	v4addr_chars.21060(%rip), %r15
	call	name_cmp@PLT
	movl	$1, -124(%rbp)
	movl	$4, -120(%rbp)
	testl	%eax, %eax
	jne	.L864
.L803:
	movq	16(%rbx), %rdi
	leaq	-104(%rbp), %rsi
	xorl	%edx, %edx
	call	strtoul@PLT
	movq	-104(%rbp), %rdx
	leaq	.LC32(%rip), %rsi
	movl	%eax, -108(%rbp)
	movq	%rax, %r13
	movq	%rdx, %rdi
	movq	%rdx, -144(%rbp)
	call	strspn@PLT
	movq	-144(%rbp), %rdx
	addq	%rax, %rdx
	cmpl	$255, %r13d
	ja	.L865
	leaq	1(%rdx), %r13
	movq	%r13, -104(%rbp)
	cmpb	$58, (%rdx)
	jne	.L806
	movq	%r13, %rdi
	leaq	.LC32(%rip), %rsi
	call	strspn@PLT
	movl	$950, %edx
	leaq	.LC24(%rip), %rsi
	leaq	0(%r13,%rax), %rdi
	movq	%rdi, -104(%rbp)
	call	CRYPTO_strdup@PLT
	leaq	-108(%rbp), %r10
	movq	%rax, %r13
	jmp	.L807
	.p2align 4,,10
	.p2align 3
.L809:
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%r10, -160(%rbp)
	call	strspn@PLT
	leaq	.LC32(%rip), %rsi
	movslq	%eax, %rdx
	movq	%rax, -152(%rbp)
	addq	%r13, %rdx
	movq	%rdx, %rdi
	movq	%rdx, -144(%rbp)
	call	strspn@PLT
	movq	-152(%rbp), %rcx
	leaq	-96(%rbp), %r11
	movq	%r13, %rsi
	movq	-144(%rbp), %rdx
	movq	%r11, %rdi
	movq	%r11, -144(%rbp)
	addl	%ecx, %eax
	leal	1(%rax), %ecx
	cltq
	movl	%ecx, -128(%rbp)
	movzbl	0(%r13,%rax), %ecx
	movb	$0, (%rdx)
	movb	%cl, -152(%rbp)
	call	a2i_ipadd@PLT
	cmpl	-120(%rbp), %eax
	movq	-144(%rbp), %r11
	movzbl	-152(%rbp), %ecx
	movq	-160(%rbp), %r10
	jne	.L866
	cmpb	$45, %cl
	je	.L818
	cmpb	$47, %cl
	je	.L819
	testb	%cl, %cl
	je	.L867
	movq	%r13, %r15
	movl	$1033, %r8d
.L857:
	leaq	.LC24(%rip), %rcx
	movl	$116, %edx
	jmp	.L858
	.p2align 4,,10
	.p2align 3
.L863:
	testq	%rdx, %rdx
	jne	.L810
.L812:
	movq	%rcx, -120(%rbp)
	call	ASN1_NULL_new@PLT
	movq	%rax, 8(%r15)
	testq	%rax, %rax
	jne	.L868
	.p2align 4,,10
	.p2align 3
.L810:
	movq	%r13, %r15
	movl	$965, %r8d
	movl	$165, %edx
	leaq	.LC24(%rip), %rcx
.L858:
	movl	$159, %esi
	movl	$34, %edi
	call	ERR_put_error@PLT
	subq	$8, %rsp
	movq	8(%rbx), %r8
	pushq	16(%rbx)
	movq	(%rbx), %rdx
	xorl	%eax, %eax
	leaq	.LC29(%rip), %r9
	leaq	.LC30(%rip), %rcx
	leaq	.LC31(%rip), %rsi
	movl	$6, %edi
	call	ERR_add_error_data@PLT
	popq	%rax
	popq	%rdx
.L829:
	movl	$1051, %edx
	leaq	.LC24(%rip), %rsi
	movq	%r15, %rdi
	call	CRYPTO_free@PLT
	movq	-136(%rbp), %rdi
	leaq	IPAddressFamily_free(%rip), %rsi
	call	OPENSSL_sk_pop_free@PLT
	movq	$0, -136(%rbp)
.L799:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L869
	movq	-136(%rbp), %rax
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L813:
	.cfi_restore_state
	testq	%rdx, %rdx
	je	.L812
.L815:
	movl	$0, (%r15)
	jmp	.L814
	.p2align 4,,10
	.p2align 3
.L867:
	movl	-120(%rbp), %r8d
	movl	-124(%rbp), %esi
	movq	%r11, %rcx
	movq	%r10, %rdx
	movq	-136(%rbp), %rdi
	sall	$3, %r8d
	call	X509v3_addr_add_prefix
	testl	%eax, %eax
	je	.L870
.L824:
	movl	$1039, %edx
	leaq	.LC24(%rip), %rsi
	movq	%r13, %rdi
	call	CRYPTO_free@PLT
	jmp	.L816
	.p2align 4,,10
	.p2align 3
.L819:
	movslq	-128(%rbp), %r15
	movl	$10, %edx
	leaq	-104(%rbp), %rsi
	movq	%r11, -144(%rbp)
	movq	%r10, -120(%rbp)
	addq	%r13, %r15
	movq	%r15, %rdi
	call	strtoul@PLT
	movq	-104(%rbp), %rdx
	cmpq	%rdx, %r15
	je	.L822
	cmpb	$0, (%rdx)
	movq	-120(%rbp), %r10
	movq	-144(%rbp), %r11
	jne	.L822
	movl	-124(%rbp), %esi
	movq	-136(%rbp), %rdi
	movl	%eax, %r8d
	movq	%r11, %rcx
	movq	%r10, %rdx
	call	X509v3_addr_add_prefix
	testl	%eax, %eax
	jne	.L824
	movq	%r13, %r15
	movl	$996, %r8d
	.p2align 4,,10
	.p2align 3
.L855:
	movl	$65, %edx
	movl	$159, %esi
	movl	$34, %edi
	leaq	.LC24(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L829
	.p2align 4,,10
	.p2align 3
.L818:
	movslq	-128(%rbp), %rdi
	leaq	.LC32(%rip), %rsi
	movq	%r11, -168(%rbp)
	movq	%r10, -160(%rbp)
	addq	%r13, %rdi
	call	strspn@PLT
	movl	-128(%rbp), %ecx
	movq	%r15, %rsi
	leal	(%rcx,%rax), %edx
	movslq	%edx, %r8
	movl	%edx, -144(%rbp)
	addq	%r13, %r8
	movq	%r8, %rdi
	movq	%r8, -152(%rbp)
	call	strspn@PLT
	movl	-144(%rbp), %edx
	addl	%edx, %eax
	cmpl	%eax, %edx
	je	.L825
	cltq
	movq	-152(%rbp), %r8
	movq	-160(%rbp), %r10
	cmpb	$0, 0(%r13,%rax)
	movq	-168(%rbp), %r11
	jne	.L825
	leaq	-80(%rbp), %r15
	movq	%r8, %rsi
	movq	%r11, -152(%rbp)
	movq	%r15, %rdi
	movq	%r10, -144(%rbp)
	call	a2i_ipadd@PLT
	cmpl	-120(%rbp), %eax
	movq	-144(%rbp), %r10
	movq	-152(%rbp), %r11
	jne	.L871
	movslq	-120(%rbp), %rdx
	movq	%r11, %rdi
	movq	%r15, %rsi
	movq	%r10, -144(%rbp)
	movq	%r11, -120(%rbp)
	call	memcmp@PLT
	movq	-120(%rbp), %r11
	movq	-144(%rbp), %r10
	testl	%eax, %eax
	jg	.L872
	movl	-124(%rbp), %esi
	movq	-136(%rbp), %rdi
	movq	%r15, %r8
	movq	%r11, %rcx
	movq	%r10, %rdx
	call	X509v3_addr_add_range
	testl	%eax, %eax
	jne	.L824
	movq	%r13, %r15
	movl	$1022, %r8d
	jmp	.L855
	.p2align 4,,10
	.p2align 3
.L864:
	movq	8(%rbx), %rdi
	leaq	.LC28(%rip), %rsi
	call	name_cmp@PLT
	testl	%eax, %eax
	jne	.L873
	movl	$2, -124(%rbp)
	leaq	v6addr_chars.21061(%rip), %r15
	movl	$16, -120(%rbp)
	jmp	.L803
	.p2align 4,,10
	.p2align 3
.L868:
	movq	-120(%rbp), %rcx
	movq	8(%rcx), %r15
	jmp	.L815
.L866:
	movq	%r13, %r15
	movl	$981, %r8d
	movl	$166, %edx
	leaq	.LC24(%rip), %rcx
	jmp	.L858
	.p2align 4,,10
	.p2align 3
.L859:
	movl	$898, %r8d
	movl	$65, %edx
	movl	$159, %esi
	movl	$34, %edi
	leaq	.LC24(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L799
	.p2align 4,,10
	.p2align 3
.L860:
	movq	-136(%rbp), %rdi
	call	X509v3_addr_canonize
	testl	%eax, %eax
	jne	.L799
	xorl	%r15d, %r15d
	jmp	.L829
.L865:
	movq	%rdx, -104(%rbp)
.L806:
	movl	$945, %r8d
	leaq	.LC24(%rip), %rcx
	movl	$164, %edx
.L856:
	movl	$159, %esi
	movl	$34, %edi
	xorl	%r15d, %r15d
	call	ERR_put_error@PLT
	subq	$8, %rsp
	movq	8(%rbx), %r8
	pushq	16(%rbx)
	movq	(%rbx), %rdx
	leaq	.LC30(%rip), %rcx
	xorl	%eax, %eax
	leaq	.LC31(%rip), %rsi
	leaq	.LC29(%rip), %r9
	movl	$6, %edi
	call	ERR_add_error_data@PLT
	popq	%rcx
	popq	%rsi
	jmp	.L829
	.p2align 4,,10
	.p2align 3
.L862:
	movq	%r13, %r15
	movl	$955, %r8d
	jmp	.L855
.L825:
	movq	%r13, %r15
	movl	$1004, %r8d
	jmp	.L857
.L822:
	movq	%r13, %r15
	movl	$990, %r8d
	jmp	.L857
.L870:
	movq	%r13, %r15
	movl	$1028, %r8d
	jmp	.L855
.L871:
	movq	%r13, %r15
	movl	$1010, %r8d
	movl	$166, %edx
	leaq	.LC24(%rip), %rcx
	jmp	.L858
.L872:
	movq	%r13, %r15
	movl	$1016, %r8d
	jmp	.L857
.L873:
	movl	$920, %r8d
	leaq	.LC24(%rip), %rcx
	movl	$115, %edx
	jmp	.L856
.L869:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1361:
	.size	v2i_IPAddrBlocks, .-v2i_IPAddrBlocks
	.p2align 4
	.globl	X509v3_addr_inherits
	.type	X509v3_addr_inherits, @function
X509v3_addr_inherits:
.LFB1362:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L886
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	xorl	%ebx, %ebx
	jmp	.L875
	.p2align 4,,10
	.p2align 3
.L877:
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	OPENSSL_sk_value@PLT
	movq	8(%rax), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	je	.L880
	addl	$1, %ebx
.L875:
	movq	%r12, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jl	.L877
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L880:
	.cfi_restore_state
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L886:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1362:
	.size	X509v3_addr_inherits, .-X509v3_addr_inherits
	.p2align 4
	.globl	X509v3_addr_subset
	.type	X509v3_addr_subset, @function
X509v3_addr_subset:
.LFB1364:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L923
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	cmpq	%rsi, %rdi
	je	.L903
	xorl	%r13d, %r13d
	testq	%rsi, %rsi
	jne	.L891
.L892:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L893:
	.cfi_restore_state
	movl	%r13d, %esi
	movq	%rbx, %rdi
	call	OPENSSL_sk_value@PLT
	movq	8(%rax), %rax
	movl	(%rax), %edx
	testl	%edx, %edx
	je	.L892
	addl	$1, %r13d
.L891:
	movq	%rbx, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r13d
	jl	.L893
	xorl	%r13d, %r13d
	jmp	.L894
	.p2align 4,,10
	.p2align 3
.L895:
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	OPENSSL_sk_value@PLT
	movq	8(%rax), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	je	.L892
	addl	$1, %r13d
.L894:
	movq	%r12, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r13d
	jl	.L895
	leaq	IPAddressFamily_cmp(%rip), %rsi
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	call	OPENSSL_sk_set_cmp_func@PLT
	jmp	.L900
	.p2align 4,,10
	.p2align 3
.L926:
	movq	8(%rcx), %rsi
	xorl	%edx, %edx
	testq	%rsi, %rsi
	je	.L897
	cmpl	$1, (%rcx)
	jle	.L897
	movzwl	(%rsi), %ecx
	movl	$4, %edx
	movl	%ecx, %esi
	rolw	$8, %si
	cmpw	$256, %cx
	je	.L897
	xorl	%edx, %edx
	cmpw	$2, %si
	sete	%dl
	sall	$4, %edx
.L897:
	movq	8(%r14), %rcx
	movq	8(%rax), %rax
	movq	8(%rcx), %rsi
	movq	8(%rax), %rdi
	testq	%rsi, %rsi
	je	.L901
	cmpq	%rdi, %rsi
	je	.L901
	testq	%rdi, %rdi
	je	.L892
	call	addr_contains.part.0
	testl	%eax, %eax
	je	.L892
.L901:
	addl	$1, %r13d
.L900:
	movq	%rbx, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r13d
	jge	.L903
	movl	%r13d, %esi
	movq	%rbx, %rdi
	call	OPENSSL_sk_value@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %r14
	call	OPENSSL_sk_find@PLT
	movq	%r12, %rdi
	movl	%eax, %esi
	call	OPENSSL_sk_value@PLT
	testq	%rax, %rax
	je	.L892
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	jne	.L926
	xorl	%edx, %edx
	jmp	.L897
	.p2align 4,,10
	.p2align 3
.L903:
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L923:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE1364:
	.size	X509v3_addr_subset, .-X509v3_addr_subset
	.p2align 4
	.globl	X509v3_addr_validate_path
	.type	X509v3_addr_validate_path, @function
X509v3_addr_validate_path:
.LFB1366:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	152(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L930
	call	OPENSSL_sk_num@PLT
	testl	%eax, %eax
	je	.L930
	cmpq	$0, 56(%r12)
	je	.L930
	movq	152(%r12), %r13
	testq	%r13, %r13
	je	.L930
	movq	%r13, %rdi
	call	OPENSSL_sk_num@PLT
	testl	%eax, %eax
	jle	.L930
	cmpq	$0, 56(%r12)
	je	.L930
	movq	%r13, %rsi
	movq	%r12, %rdi
	xorl	%edx, %edx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	addr_validate_path_internal.part.0
	.p2align 4,,10
	.p2align 3
.L930:
	.cfi_restore_state
	movl	$1, 176(%r12)
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1366:
	.size	X509v3_addr_validate_path, .-X509v3_addr_validate_path
	.p2align 4
	.globl	X509v3_addr_validate_resource_set
	.type	X509v3_addr_validate_resource_set, @function
X509v3_addr_validate_resource_set:
.LFB1367:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L947
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	testq	%rdi, %rdi
	je	.L942
	movq	%rsi, %r12
	movl	%edx, %ebx
	call	OPENSSL_sk_num@PLT
	testl	%eax, %eax
	je	.L942
	testl	%ebx, %ebx
	je	.L943
.L946:
	movq	%r13, %rdi
	call	OPENSSL_sk_num@PLT
	testl	%eax, %eax
	jle	.L942
	addq	$8, %rsp
	movq	%r12, %rdx
	movq	%r13, %rsi
	xorl	%edi, %edi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	addr_validate_path_internal.part.0
	.p2align 4,,10
	.p2align 3
.L942:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L945:
	.cfi_restore_state
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	OPENSSL_sk_value@PLT
	movq	8(%rax), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	je	.L942
	addl	$1, %ebx
.L943:
	movq	%r12, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jl	.L945
	jmp	.L946
	.p2align 4,,10
	.p2align 3
.L947:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE1367:
	.size	X509v3_addr_validate_resource_set, .-X509v3_addr_validate_resource_set
	.section	.rodata
	.align 16
	.type	v6addr_chars.21061, @object
	.size	v6addr_chars.21061, 25
v6addr_chars.21061:
	.string	"0123456789.:abcdefABCDEF"
	.align 8
	.type	v4addr_chars.21060, @object
	.size	v4addr_chars.21060, 12
v4addr_chars.21060:
	.string	"0123456789."
	.globl	v3_addr
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	v3_addr, @object
	.size	v3_addr, 104
v3_addr:
	.long	290
	.long	0
	.quad	IPAddrBlocks_it
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	v2i_IPAddrBlocks
	.quad	i2r_IPAddrBlocks
	.quad	0
	.quad	0
	.section	.rodata.str1.1
.LC34:
	.string	"IPAddrBlocks"
	.section	.data.rel.ro.local
	.align 32
	.type	IPAddrBlocks_it, @object
	.size	IPAddrBlocks_it, 56
IPAddrBlocks_it:
	.byte	0
	.zero	7
	.quad	-1
	.quad	IPAddrBlocks_item_tt
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC34
	.align 32
	.type	IPAddrBlocks_item_tt, @object
	.size	IPAddrBlocks_item_tt, 40
IPAddrBlocks_item_tt:
	.quad	4
	.quad	0
	.quad	0
	.quad	.LC34
	.quad	IPAddressFamily_it
	.globl	IPAddressFamily_it
	.section	.rodata.str1.1
.LC35:
	.string	"IPAddressFamily"
	.section	.data.rel.ro.local
	.align 32
	.type	IPAddressFamily_it, @object
	.size	IPAddressFamily_it, 56
IPAddressFamily_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	IPAddressFamily_seq_tt
	.quad	2
	.quad	0
	.quad	16
	.quad	.LC35
	.section	.rodata.str1.1
.LC36:
	.string	"addressFamily"
.LC37:
	.string	"ipAddressChoice"
	.section	.data.rel.ro,"aw"
	.align 32
	.type	IPAddressFamily_seq_tt, @object
	.size	IPAddressFamily_seq_tt, 80
IPAddressFamily_seq_tt:
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC36
	.quad	ASN1_OCTET_STRING_it
	.quad	0
	.quad	0
	.quad	8
	.quad	.LC37
	.quad	IPAddressChoice_it
	.globl	IPAddressChoice_it
	.section	.rodata.str1.1
.LC38:
	.string	"IPAddressChoice"
	.section	.data.rel.ro.local
	.align 32
	.type	IPAddressChoice_it, @object
	.size	IPAddressChoice_it, 56
IPAddressChoice_it:
	.byte	2
	.zero	7
	.quad	0
	.quad	IPAddressChoice_ch_tt
	.quad	2
	.quad	0
	.quad	16
	.quad	.LC38
	.section	.rodata.str1.1
.LC39:
	.string	"u.inherit"
.LC40:
	.string	"u.addressesOrRanges"
	.section	.data.rel.ro
	.align 32
	.type	IPAddressChoice_ch_tt, @object
	.size	IPAddressChoice_ch_tt, 80
IPAddressChoice_ch_tt:
	.quad	0
	.quad	0
	.quad	8
	.quad	.LC39
	.quad	ASN1_NULL_it
	.quad	4
	.quad	0
	.quad	8
	.quad	.LC40
	.quad	IPAddressOrRange_it
	.globl	IPAddressOrRange_it
	.section	.rodata.str1.1
.LC41:
	.string	"IPAddressOrRange"
	.section	.data.rel.ro.local
	.align 32
	.type	IPAddressOrRange_it, @object
	.size	IPAddressOrRange_it, 56
IPAddressOrRange_it:
	.byte	2
	.zero	7
	.quad	0
	.quad	IPAddressOrRange_ch_tt
	.quad	2
	.quad	0
	.quad	16
	.quad	.LC41
	.section	.rodata.str1.1
.LC42:
	.string	"u.addressPrefix"
.LC43:
	.string	"u.addressRange"
	.section	.data.rel.ro
	.align 32
	.type	IPAddressOrRange_ch_tt, @object
	.size	IPAddressOrRange_ch_tt, 80
IPAddressOrRange_ch_tt:
	.quad	0
	.quad	0
	.quad	8
	.quad	.LC42
	.quad	ASN1_BIT_STRING_it
	.quad	0
	.quad	0
	.quad	8
	.quad	.LC43
	.quad	IPAddressRange_it
	.globl	IPAddressRange_it
	.section	.rodata.str1.1
.LC44:
	.string	"IPAddressRange"
	.section	.data.rel.ro.local
	.align 32
	.type	IPAddressRange_it, @object
	.size	IPAddressRange_it, 56
IPAddressRange_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	IPAddressRange_seq_tt
	.quad	2
	.quad	0
	.quad	16
	.quad	.LC44
	.section	.rodata.str1.1
.LC45:
	.string	"min"
.LC46:
	.string	"max"
	.section	.data.rel.ro
	.align 32
	.type	IPAddressRange_seq_tt, @object
	.size	IPAddressRange_seq_tt, 80
IPAddressRange_seq_tt:
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC45
	.quad	ASN1_BIT_STRING_it
	.quad	0
	.quad	0
	.quad	8
	.quad	.LC46
	.quad	ASN1_BIT_STRING_it
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
