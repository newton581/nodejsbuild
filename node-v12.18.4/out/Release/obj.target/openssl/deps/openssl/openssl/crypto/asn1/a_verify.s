	.file	"a_verify.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/asn1/a_verify.c"
	.text
	.p2align 4
	.globl	ASN1_verify
	.type	ASN1_verify, @function
ASN1_verify:
.LFB829:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$56, %rsp
	movq	%r8, -80(%rbp)
	movq	%rdi, -72(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	EVP_MD_CTX_new@PLT
	movl	$35, %r8d
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L17
	movq	(%r15), %rdi
	call	OBJ_obj2nid@PLT
	movl	%eax, %edi
	call	OBJ_nid2sn@PLT
	movq	%rax, %rdi
	call	EVP_get_digestbyname@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L20
	cmpl	$3, 4(%rbx)
	je	.L21
.L5:
	xorl	%esi, %esi
	movq	-72(%rbp), %rax
	movq	%r14, %rdi
	call	*%rax
	testl	%eax, %eax
	jle	.L22
	movslq	%eax, %r13
	movl	$55, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r13, %rdi
	call	CRYPTO_malloc@PLT
	testq	%rax, %rax
	je	.L23
	movq	%rax, -64(%rbp)
	leaq	-64(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, -88(%rbp)
	movq	-72(%rbp), %rax
	call	*%rax
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	EVP_DigestInit_ex@PLT
	movq	-88(%rbp), %r8
	testl	%eax, %eax
	jne	.L8
	movl	$66, %ecx
	leaq	.LC0(%rip), %rdx
	movq	%r13, %rsi
	movq	%r8, %rdi
	call	CRYPTO_clear_free@PLT
.L9:
	movl	$69, %r8d
.L18:
	leaq	.LC0(%rip), %rcx
	movl	$6, %edx
	movl	$137, %esi
	xorl	%r13d, %r13d
	movl	$13, %edi
	call	ERR_put_error@PLT
.L3:
	movq	%r12, %rdi
	call	EVP_MD_CTX_free@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L24
	addq	$56, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	.cfi_restore_state
	testb	$7, 16(%rbx)
	je	.L5
	movl	$46, %r8d
	movl	$220, %edx
	movl	$137, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	movl	$-1, %r13d
	call	ERR_put_error@PLT
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L23:
	movl	$57, %r8d
.L17:
	movl	$65, %edx
	movl	$137, %esi
	movl	$13, %edi
	movl	$-1, %r13d
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L22:
	movl	$52, %r8d
	movl	$68, %edx
	movl	$137, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	movl	$-1, %r13d
	call	ERR_put_error@PLT
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L8:
	movq	%r8, %rsi
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%r8, -72(%rbp)
	call	EVP_DigestUpdate@PLT
	movq	-72(%rbp), %r8
	movl	$66, %ecx
	movq	%r13, %rsi
	testl	%eax, %eax
	leaq	.LC0(%rip), %rdx
	movq	%r8, %rdi
	jne	.L25
	call	CRYPTO_clear_free@PLT
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L25:
	call	CRYPTO_clear_free@PLT
	movq	8(%rbx), %rsi
	movq	-80(%rbp), %rcx
	movq	%r12, %rdi
	movl	(%rbx), %edx
	call	EVP_VerifyFinal@PLT
	testl	%eax, %eax
	jle	.L11
	movl	$1, %r13d
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L20:
	movl	$41, %r8d
	movl	$161, %edx
	movl	$137, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	movl	$-1, %r13d
	call	ERR_put_error@PLT
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L11:
	movl	$76, %r8d
	jmp	.L18
.L24:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE829:
	.size	ASN1_verify, .-ASN1_verify
	.p2align 4
	.globl	ASN1_item_verify
	.type	ASN1_item_verify, @function
ASN1_item_verify:
.LFB830:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, -88(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -64(%rbp)
	testq	%r8, %r8
	je	.L53
	cmpl	$3, 4(%rdx)
	movq	%rdi, %r15
	movq	%rsi, %r14
	movq	%rdx, %rbx
	movq	%r8, %r13
	jne	.L29
	testb	$7, 16(%rdx)
	jne	.L54
.L29:
	call	EVP_MD_CTX_new@PLT
	movl	$109, %r8d
	movl	$65, %edx
	leaq	.LC0(%rip), %rcx
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L51
	movq	(%r14), %rdi
	call	OBJ_obj2nid@PLT
	leaq	-68(%rbp), %rdx
	leaq	-72(%rbp), %rsi
	movl	%eax, %edi
	call	OBJ_find_sigid_algs@PLT
	testl	%eax, %eax
	je	.L55
	movl	-72(%rbp), %edi
	testl	%edi, %edi
	jne	.L33
	movq	16(%r13), %rax
	testq	%rax, %rax
	je	.L34
	movq	200(%rax), %rax
	testq	%rax, %rax
	je	.L34
	movq	%r13, %r9
	movq	%r14, %rcx
	movq	-88(%rbp), %rdx
	movq	%rbx, %r8
	movq	%r15, %rsi
	movq	%r12, %rdi
	xorl	%r14d, %r14d
	call	*%rax
	movl	%eax, %r13d
	cmpl	$2, %eax
	jne	.L31
.L36:
	movq	-88(%rbp), %rdi
	movq	%r15, %rdx
	leaq	-64(%rbp), %rsi
	call	ASN1_item_i2d@PLT
	movl	$158, %r8d
	movl	$68, %edx
	leaq	.LC0(%rip), %rcx
	testl	%eax, %eax
	jle	.L51
	movq	-64(%rbp), %rcx
	testq	%rcx, %rcx
	je	.L56
	movslq	%eax, %r14
	movslq	(%rbx), %rdx
	movq	8(%rbx), %rsi
	movq	%r12, %rdi
	movq	%r14, %r8
	call	EVP_DigestVerify@PLT
	movl	%eax, %r13d
	testl	%eax, %eax
	jle	.L57
	movl	$1, %r13d
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L55:
	movl	$115, %r8d
.L52:
	leaq	.LC0(%rip), %rcx
	movl	$199, %edx
.L51:
	movl	$197, %esi
	movl	$13, %edi
	xorl	%r14d, %r14d
	movl	$-1, %r13d
	call	ERR_put_error@PLT
.L31:
	movq	-64(%rbp), %rdi
	movl	$175, %ecx
	leaq	.LC0(%rip), %rdx
	movq	%r14, %rsi
	call	CRYPTO_clear_free@PLT
	movq	%r12, %rdi
	call	EVP_MD_CTX_free@PLT
.L26:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L58
	addq	$56, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L33:
	.cfi_restore_state
	call	OBJ_nid2sn@PLT
	movq	%rax, %rdi
	call	EVP_get_digestbyname@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L59
	movl	-68(%rbp), %edi
	call	EVP_PKEY_type@PLT
	movq	16(%r13), %rdx
	cmpl	(%rdx), %eax
	je	.L38
	movl	$144, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$200, %edx
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L34:
	movl	$120, %r8d
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L38:
	movq	%r13, %r8
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	EVP_DigestVerifyInit@PLT
	movl	%eax, %r13d
	testl	%eax, %eax
	jne	.L36
	movl	$149, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$6, %edx
	xorl	%r14d, %r14d
	movl	$197, %esi
	movl	$13, %edi
	call	ERR_put_error@PLT
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L59:
	movl	$137, %r8d
	movl	$161, %edx
	movl	$197, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	movl	$-1, %r13d
	call	ERR_put_error@PLT
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L56:
	movl	$162, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L57:
	movl	$170, %r8d
	movl	$6, %edx
	movl	$197, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L31
.L54:
	movl	$103, %r8d
	movl	$220, %edx
	movl	$197, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	movl	$-1, %r13d
	call	ERR_put_error@PLT
	jmp	.L26
.L53:
	movl	$98, %r8d
	movl	$67, %edx
	movl	$197, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	movl	$-1, %r13d
	call	ERR_put_error@PLT
	jmp	.L26
.L58:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE830:
	.size	ASN1_item_verify, .-ASN1_item_verify
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
