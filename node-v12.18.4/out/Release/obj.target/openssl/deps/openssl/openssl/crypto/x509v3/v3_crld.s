	.file	"v3_crld.c"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	""
.LC1:
	.string	"%*s%s:\n%*s"
.LC2:
	.string	", "
.LC3:
	.string	"<EMPTY>\n"
.LC4:
	.string	"\n"
	.text
	.p2align 4
	.type	print_reasons, @function
print_reasons:
.LFB1325:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	xorl	%eax, %eax
	leaq	.LC1(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	$1, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	movl	%ecx, %edx
	leaq	.LC0(%rip), %rcx
	pushq	%rbx
	.cfi_offset 3, -48
	leal	2(%rdx), %r9d
	leaq	reason_flags(%rip), %rbx
	subq	$8, %rsp
	pushq	%rcx
	call	BIO_printf@PLT
	popq	%rax
	popq	%rdx
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L3:
	movq	8(%rbx), %rsi
	movq	%r13, %rdi
	xorl	%r14d, %r14d
	call	BIO_puts@PLT
.L2:
	addq	$24, %rbx
	cmpq	$0, 8(%rbx)
	je	.L13
.L4:
	movl	(%rbx), %esi
	movq	%r12, %rdi
	call	ASN1_BIT_STRING_get_bit@PLT
	testl	%eax, %eax
	je	.L2
	testl	%r14d, %r14d
	jne	.L3
	leaq	.LC2(%rip), %rsi
	movq	%r13, %rdi
	call	BIO_puts@PLT
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L13:
	testl	%r14d, %r14d
	je	.L5
	leaq	.LC3(%rip), %rsi
	movq	%r13, %rdi
	call	BIO_puts@PLT
.L6:
	leaq	-32(%rbp), %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	.cfi_restore_state
	leaq	.LC4(%rip), %rsi
	movq	%r13, %rdi
	call	BIO_puts@PLT
	jmp	.L6
	.cfi_endproc
.LFE1325:
	.size	print_reasons, .-print_reasons
	.p2align 4
	.type	dpn_cb, @function
dpn_cb:
.LFB1328:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rax
	cmpl	$1, %edi
	je	.L15
	cmpl	$3, %edi
	jne	.L19
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	16(%rax), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	X509_NAME_free@PLT
	movl	$1, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	.cfi_restore 6
	movq	$0, 16(%rax)
.L19:
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE1328:
	.size	dpn_cb, .-dpn_cb
	.p2align 4
	.type	set_reasons, @function
set_reasons:
.LFB1324:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%rdi, -56(%rbp)
	movq	%rsi, %rdi
	call	X509V3_parse_list@PLT
	testq	%rax, %rax
	je	.L31
	cmpq	$0, (%rbx)
	movq	%rax, %r14
	je	.L32
	.p2align 4,,10
	.p2align 3
.L26:
	xorl	%r12d, %r12d
.L24:
	movq	X509V3_conf_free@GOTPCREL(%rip), %rsi
	movq	%r14, %rdi
	call	OPENSSL_sk_pop_free@PLT
.L21:
	addq	$24, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L32:
	.cfi_restore_state
	movq	%r14, %rdi
	xorl	%r12d, %r12d
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r12d
	jge	.L42
	.p2align 4,,10
	.p2align 3
.L30:
	movl	%r12d, %esi
	movq	%r14, %rdi
	call	OPENSSL_sk_value@PLT
	movq	8(%rax), %rbx
	movq	-56(%rbp), %rax
	movq	(%rax), %r13
	testq	%r13, %r13
	je	.L43
.L25:
	leaq	reason_flags(%rip), %r15
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L27:
	addq	$24, %r15
	cmpq	$0, 8(%r15)
	je	.L26
.L29:
	movq	16(%r15), %rdi
	movq	%rbx, %rsi
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L27
	movl	(%r15), %esi
	movl	$1, %edx
	movq	%r13, %rdi
	call	ASN1_BIT_STRING_set_bit@PLT
	testl	%eax, %eax
	je	.L26
	cmpq	$0, 8(%r15)
	je	.L26
	movq	%r14, %rdi
	addl	$1, %r12d
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r12d
	jl	.L30
.L42:
	movl	$1, %r12d
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L43:
	call	ASN1_BIT_STRING_new@PLT
	movq	%rax, %r13
	movq	-56(%rbp), %rax
	movq	%r13, (%rax)
	testq	%r13, %r13
	jne	.L25
	jmp	.L26
.L31:
	xorl	%r12d, %r12d
	jmp	.L21
	.cfi_endproc
.LFE1324:
	.size	set_reasons, .-set_reasons
	.p2align 4
	.globl	DIST_POINT_free
	.type	DIST_POINT_free, @function
DIST_POINT_free:
.LFB1336:
	.cfi_startproc
	endbr64
	leaq	DIST_POINT_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE1336:
	.size	DIST_POINT_free, .-DIST_POINT_free
	.section	.rodata.str1.1
.LC5:
	.string	"%*sFull Name:\n"
.LC6:
	.string	"%*s"
.LC7:
	.string	"%*sRelative Name:\n%*s"
.LC8:
	.string	"%*sOnly User Certificates\n"
.LC9:
	.string	"%*sOnly CA Certificates\n"
.LC10:
	.string	"%*sIndirect CRL\n"
.LC11:
	.string	"Only Some Reasons"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC12:
	.string	"%*sOnly Attribute Certificates\n"
	.section	.rodata.str1.1
.LC13:
	.string	"%*s<EMPTY>\n"
	.text
	.p2align 4
	.type	i2r_idp, @function
i2r_idp:
.LFB1348:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%ecx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$72, %rsp
	movq	(%rsi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%r15, %r15
	je	.L47
	movl	(%r15), %r14d
	testl	%r14d, %r14d
	jne	.L48
	leaq	.LC0(%rip), %rcx
	movl	%r13d, %edx
	leaq	.LC5(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_printf@PLT
	leal	2(%r13), %eax
	movq	8(%r15), %r15
	movl	%eax, -100(%rbp)
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L50:
	movl	-100(%rbp), %edx
	leaq	.LC0(%rip), %rcx
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC6(%rip), %rsi
	call	BIO_printf@PLT
	movl	%r14d, %esi
	movq	%r15, %rdi
	addl	$1, %r14d
	call	OPENSSL_sk_value@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	GENERAL_NAME_print@PLT
	leaq	.LC4(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_puts@PLT
.L49:
	movq	%r15, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r14d
	jl	.L50
.L47:
	movl	8(%rbx), %r10d
	testl	%r10d, %r10d
	jle	.L52
	leaq	.LC0(%rip), %rcx
	movl	%r13d, %edx
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC8(%rip), %rsi
	call	BIO_printf@PLT
.L52:
	movl	12(%rbx), %r9d
	testl	%r9d, %r9d
	jle	.L53
	leaq	.LC0(%rip), %rcx
	movl	%r13d, %edx
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC9(%rip), %rsi
	call	BIO_printf@PLT
.L53:
	movl	24(%rbx), %r8d
	testl	%r8d, %r8d
	jle	.L54
	leaq	.LC0(%rip), %rcx
	movl	%r13d, %edx
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC10(%rip), %rsi
	call	BIO_printf@PLT
.L54:
	movq	16(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L55
	movl	%r13d, %ecx
	leaq	.LC11(%rip), %rsi
	movq	%r12, %rdi
	call	print_reasons
.L55:
	movl	28(%rbx), %edi
	testl	%edi, %edi
	jle	.L56
	leaq	.LC0(%rip), %rcx
	movl	%r13d, %edx
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC12(%rip), %rsi
	call	BIO_printf@PLT
.L56:
	cmpq	$0, (%rbx)
	je	.L63
.L57:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L64
	addq	$72, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L48:
	.cfi_restore_state
	movq	8(%r15), %rax
	leal	2(%rcx), %r8d
	movl	%r13d, %edx
	movq	%r12, %rdi
	leaq	.LC0(%rip), %r9
	leaq	.LC7(%rip), %rsi
	movq	%r9, %rcx
	movq	%rax, -96(%rbp)
	xorl	%eax, %eax
	call	BIO_printf@PLT
	leaq	-96(%rbp), %rsi
	movq	%r12, %rdi
	xorl	%edx, %edx
	movl	$8520479, %ecx
	call	X509_NAME_print_ex@PLT
	leaq	.LC4(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_puts@PLT
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L63:
	movl	8(%rbx), %esi
	testl	%esi, %esi
	jg	.L57
	movl	12(%rbx), %ecx
	testl	%ecx, %ecx
	jg	.L57
	movl	24(%rbx), %edx
	testl	%edx, %edx
	jg	.L57
	cmpq	$0, 16(%rbx)
	jne	.L57
	movl	28(%rbx), %eax
	testl	%eax, %eax
	jg	.L57
	leaq	.LC0(%rip), %rcx
	movl	%r13d, %edx
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC13(%rip), %rsi
	call	BIO_printf@PLT
	jmp	.L57
.L64:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1348:
	.size	i2r_idp, .-i2r_idp
	.section	.rodata.str1.1
.LC14:
	.string	"fullname"
	.section	.rodata.str1.8
	.align 8
.LC15:
	.string	"../deps/openssl/openssl/crypto/x509v3/v3_crld.c"
	.section	.rodata.str1.1
.LC16:
	.string	"relativename"
	.text
	.p2align 4
	.type	set_dist_point_name.isra.0, @function
set_dist_point_name.isra.0:
.LFB1354:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rcx, %r13
	movl	$9, %ecx
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movq	%rdx, %rsi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	leaq	.LC14(%rip), %rdi
	subq	$24, %rsp
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L90
	movl	$13, %ecx
	leaq	.LC16(%rip), %rdi
	movq	%rdx, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	xorl	%r14d, %r14d
	testb	%al, %al
	je	.L91
.L65:
	addq	$24, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L91:
	.cfi_restore_state
	call	X509_NAME_new@PLT
	movl	$-1, %r14d
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L65
	movq	0(%r13), %rsi
	movq	%r12, %rdi
	call	X509V3_get_section@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L92
	movl	$4097, %edx
	movq	%rax, %rsi
	movq	%r15, %rdi
	xorl	%r14d, %r14d
	call	X509V3_NAME_from_section@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, -52(%rbp)
	call	X509V3_section_free@PLT
	movq	(%r15), %r13
	movq	$0, (%r15)
	movq	%r15, %rdi
	call	X509_NAME_free@PLT
	movl	-52(%rbp), %eax
	testl	%eax, %eax
	je	.L70
	movq	%r13, %rdi
	call	OPENSSL_sk_num@PLT
	testl	%eax, %eax
	jle	.L70
	movq	%r13, %rdi
	call	OPENSSL_sk_num@PLT
	movq	%r13, %rdi
	leal	-1(%rax), %esi
	call	OPENSSL_sk_value@PLT
	movl	16(%rax), %eax
	testl	%eax, %eax
	je	.L73
	movl	$101, %r8d
	movl	$161, %edx
	movl	$158, %esi
	movl	$34, %edi
	leaq	.LC15(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L90:
	movq	0(%r13), %r15
	cmpb	$64, (%r15)
	je	.L93
	movq	%r15, %rdi
	call	X509V3_parse_list@PLT
	movq	%rax, %r13
.L68:
	testq	%r13, %r13
	je	.L94
	xorl	%edi, %edi
	movq	%r13, %rdx
	movq	%r12, %rsi
	call	v2i_GENERAL_NAMES@PLT
	cmpb	$64, (%r15)
	movq	%rax, %r14
	je	.L95
	movq	X509V3_conf_free@GOTPCREL(%rip), %rsi
	movq	%r13, %rdi
	call	OPENSSL_sk_pop_free@PLT
.L72:
	xorl	%r13d, %r13d
	testq	%r14, %r14
	je	.L70
.L73:
	cmpq	$0, (%rbx)
	je	.L76
	movl	$109, %r8d
	movl	$160, %edx
	movl	$158, %esi
	movl	$34, %edi
	leaq	.LC15(%rip), %rcx
	call	ERR_put_error@PLT
.L70:
	movq	GENERAL_NAME_free@GOTPCREL(%rip), %rsi
	movq	%r14, %rdi
	movl	$-1, %r14d
	call	OPENSSL_sk_pop_free@PLT
	movq	X509_NAME_ENTRY_free@GOTPCREL(%rip), %rsi
	movq	%r13, %rdi
	call	OPENSSL_sk_pop_free@PLT
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L95:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	X509V3_section_free@PLT
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L76:
	leaq	DIST_POINT_NAME_it(%rip), %rdi
	call	ASN1_item_new@PLT
	movq	%rax, (%rbx)
	testq	%rax, %rax
	je	.L70
	testq	%r14, %r14
	je	.L77
	movq	%r14, 8(%rax)
	movl	$1, %r14d
	movl	$0, (%rax)
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L93:
	leaq	1(%r15), %rsi
	movq	%r12, %rdi
	call	X509V3_get_section@PLT
	movq	%rax, %r13
	jmp	.L68
	.p2align 4,,10
	.p2align 3
.L92:
	movl	$85, %r8d
	movl	$150, %edx
	movl	$158, %esi
	movl	$34, %edi
	leaq	.LC15(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L77:
	movl	$1, (%rax)
	movl	$1, %r14d
	movq	%r13, 8(%rax)
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L94:
	movl	$55, %r8d
	leaq	.LC15(%rip), %rcx
	movl	$150, %edx
	xorl	%r14d, %r14d
	movl	$156, %esi
	movl	$34, %edi
	call	ERR_put_error@PLT
	jmp	.L70
	.cfi_endproc
.LFE1354:
	.size	set_dist_point_name.isra.0, .-set_dist_point_name.isra.0
	.section	.rodata.str1.1
.LC17:
	.string	"onlyuser"
.LC18:
	.string	"onlyCA"
.LC19:
	.string	"onlyAA"
.LC20:
	.string	"indirectCRL"
.LC21:
	.string	"onlysomereasons"
.LC22:
	.string	",value:"
.LC23:
	.string	",name:"
.LC24:
	.string	"section:"
	.text
	.p2align 4
	.type	v2i_idp, @function
v2i_idp:
.LFB1345:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	ISSUING_DIST_POINT_it(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	xorl	%ebx, %ebx
	subq	$24, %rsp
	movq	%rsi, -64(%rbp)
	call	ASN1_item_new@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	jne	.L97
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L101:
	movl	$7, %ecx
	movq	%r15, %rsi
	leaq	.LC18(%rip), %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L130
	movl	$7, %ecx
	movq	%r15, %rsi
	leaq	.LC19(%rip), %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L131
	movl	$12, %ecx
	movq	%r15, %rsi
	leaq	.LC20(%rip), %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L132
	movl	$16, %ecx
	leaq	.LC21(%rip), %rdi
	movq	%r15, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L105
	movq	-56(%rbp), %rsi
	leaq	16(%r13), %rdi
	call	set_reasons
	testl	%eax, %eax
	je	.L98
	.p2align 4,,10
	.p2align 3
.L99:
	addl	$1, %ebx
.L97:
	movq	%r12, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jge	.L96
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	OPENSSL_sk_value@PLT
	movq	-64(%rbp), %rsi
	movq	%r13, %rdi
	movq	8(%rax), %r15
	movq	%rax, %r14
	movq	16(%rax), %rax
	leaq	16(%r14), %rcx
	movq	%r15, %rdx
	movq	%rax, -56(%rbp)
	call	set_dist_point_name.isra.0
	testl	%eax, %eax
	jg	.L99
	jne	.L98
	movl	$9, %ecx
	movq	%r15, %rsi
	leaq	.LC17(%rip), %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L101
	leaq	8(%r13), %rsi
	movq	%r14, %rdi
	call	X509V3_get_value_bool@PLT
	testl	%eax, %eax
	jne	.L99
	.p2align 4,,10
	.p2align 3
.L98:
	movq	%r13, %rdi
	leaq	ISSUING_DIST_POINT_it(%rip), %rsi
	xorl	%r13d, %r13d
	call	ASN1_item_free@PLT
.L96:
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L130:
	.cfi_restore_state
	leaq	12(%r13), %rsi
	movq	%r14, %rdi
	call	X509V3_get_value_bool@PLT
	testl	%eax, %eax
	jne	.L99
	jmp	.L98
	.p2align 4,,10
	.p2align 3
.L131:
	leaq	28(%r13), %rsi
	movq	%r14, %rdi
	call	X509V3_get_value_bool@PLT
	testl	%eax, %eax
	jne	.L99
	jmp	.L98
	.p2align 4,,10
	.p2align 3
.L132:
	leaq	24(%r13), %rsi
	movq	%r14, %rdi
	call	X509V3_get_value_bool@PLT
	testl	%eax, %eax
	jne	.L99
	jmp	.L98
.L105:
	movl	$395, %r8d
	movl	$106, %edx
	movl	$157, %esi
	movl	$34, %edi
	leaq	.LC15(%rip), %rcx
	call	ERR_put_error@PLT
	subq	$8, %rsp
	movq	8(%r14), %r8
	xorl	%eax, %eax
	pushq	16(%r14)
	movq	(%r14), %rdx
	leaq	.LC22(%rip), %r9
	leaq	.LC23(%rip), %rcx
	leaq	.LC24(%rip), %rsi
	movl	$6, %edi
	call	ERR_add_error_data@PLT
	popq	%rax
	popq	%rdx
	jmp	.L98
.L129:
	movl	$403, %r8d
	movl	$65, %edx
	movl	$157, %esi
	movl	$34, %edi
	leaq	.LC15(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L98
	.cfi_endproc
.LFE1345:
	.size	v2i_idp, .-v2i_idp
	.section	.rodata.str1.1
.LC25:
	.string	"Reasons"
.LC26:
	.string	"%*sCRL Issuer:\n"
	.text
	.p2align 4
	.type	i2r_crldp, @function
i2r_crldp:
.LFB1349:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leal	2(%rcx), %r15d
	pushq	%r14
	.cfi_offset 14, -32
	xorl	%r14d, %r14d
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$88, %rsp
	movq	%rsi, -112(%rbp)
	movq	-112(%rbp), %rdi
	movl	%ecx, -116(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r14d
	jge	.L152
	.p2align 4,,10
	.p2align 3
.L146:
	leaq	.LC4(%rip), %rsi
	movq	%rbx, %rdi
	call	BIO_puts@PLT
	movq	-112(%rbp), %rdi
	movl	%r14d, %esi
	call	OPENSSL_sk_value@PLT
	movq	(%rax), %r8
	movq	%rax, %r12
	testq	%r8, %r8
	je	.L136
	movl	(%r8), %r13d
	testl	%r13d, %r13d
	jne	.L137
	movl	-116(%rbp), %edx
	leaq	.LC0(%rip), %rcx
	movq	%rbx, %rdi
	xorl	%eax, %eax
	leaq	.LC5(%rip), %rsi
	movq	%r8, -104(%rbp)
	call	BIO_printf@PLT
	movq	-104(%rbp), %r8
	movq	8(%r8), %rax
	movq	%rax, -104(%rbp)
	jmp	.L138
	.p2align 4,,10
	.p2align 3
.L139:
	leaq	.LC0(%rip), %rcx
	movl	%r15d, %edx
	movq	%rbx, %rdi
	xorl	%eax, %eax
	leaq	.LC6(%rip), %rsi
	call	BIO_printf@PLT
	movq	-104(%rbp), %rdi
	movl	%r13d, %esi
	addl	$1, %r13d
	call	OPENSSL_sk_value@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	GENERAL_NAME_print@PLT
	leaq	.LC4(%rip), %rsi
	movq	%rbx, %rdi
	call	BIO_puts@PLT
.L138:
	movq	-104(%rbp), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r13d
	jl	.L139
.L136:
	movq	8(%r12), %rdx
	testq	%rdx, %rdx
	je	.L141
	movl	-116(%rbp), %ecx
	leaq	.LC25(%rip), %rsi
	movq	%rbx, %rdi
	call	print_reasons
.L141:
	cmpq	$0, 16(%r12)
	je	.L145
	movl	-116(%rbp), %edx
	leaq	.LC0(%rip), %rcx
	movq	%rbx, %rdi
	xorl	%eax, %eax
	leaq	.LC26(%rip), %rsi
	call	BIO_printf@PLT
	movq	16(%r12), %r13
	xorl	%r12d, %r12d
	jmp	.L143
	.p2align 4,,10
	.p2align 3
.L144:
	leaq	.LC0(%rip), %rcx
	movl	%r15d, %edx
	movq	%rbx, %rdi
	xorl	%eax, %eax
	leaq	.LC6(%rip), %rsi
	call	BIO_printf@PLT
	movl	%r12d, %esi
	movq	%r13, %rdi
	addl	$1, %r12d
	call	OPENSSL_sk_value@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	GENERAL_NAME_print@PLT
	leaq	.LC4(%rip), %rsi
	movq	%rbx, %rdi
	call	BIO_puts@PLT
.L143:
	movq	%r13, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r12d
	jl	.L144
.L145:
	movq	-112(%rbp), %rdi
	addl	$1, %r14d
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r14d
	jl	.L146
.L152:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L153
	addq	$88, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L137:
	.cfi_restore_state
	movq	8(%r8), %rax
	leaq	.LC0(%rip), %r9
	movl	-116(%rbp), %edx
	movl	%r15d, %r8d
	movq	%r9, %rcx
	movq	%rbx, %rdi
	leaq	.LC7(%rip), %rsi
	movq	%rax, -96(%rbp)
	xorl	%eax, %eax
	call	BIO_printf@PLT
	leaq	-96(%rbp), %rsi
	movq	%rbx, %rdi
	xorl	%edx, %edx
	movl	$8520479, %ecx
	call	X509_NAME_print_ex@PLT
	leaq	.LC4(%rip), %rsi
	movq	%rbx, %rdi
	call	BIO_puts@PLT
	jmp	.L136
.L153:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1349:
	.size	i2r_crldp, .-i2r_crldp
	.section	.rodata.str1.1
.LC27:
	.string	"reasons"
.LC28:
	.string	"CRLissuer"
	.text
	.p2align 4
	.type	v2i_crld, @function
v2i_crld:
.LFB1327:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -96(%rbp)
	movq	%rdx, %rdi
	movq	%rdx, -88(%rbp)
	call	OPENSSL_sk_num@PLT
	xorl	%edi, %edi
	movl	%eax, %esi
	movl	%eax, -56(%rbp)
	movl	%eax, %ebx
	call	OPENSSL_sk_new_reserve@PLT
	movq	%rax, -64(%rbp)
	testq	%rax, %rax
	je	.L175
	testl	%ebx, %ebx
	jle	.L154
	movl	$0, -52(%rbp)
	jmp	.L174
	.p2align 4,,10
	.p2align 3
.L157:
	movq	-96(%rbp), %rdi
	movq	%r13, %rsi
	call	v2i_GENERAL_NAME@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L173
	call	GENERAL_NAMES_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L155
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L155
	leaq	DIST_POINT_it(%rip), %rdi
	call	ASN1_item_new@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L155
	movq	-64(%rbp), %rdi
	movq	%rax, %rsi
	call	OPENSSL_sk_push@PLT
	leaq	DIST_POINT_NAME_it(%rip), %rdi
	call	ASN1_item_new@PLT
	movq	%rax, (%r14)
	testq	%rax, %rax
	je	.L176
	movq	%r12, 8(%rax)
	movl	$0, (%rax)
.L172:
	addl	$1, -52(%rbp)
	movl	-52(%rbp), %eax
	cmpl	-56(%rbp), %eax
	je	.L154
.L174:
	movl	-52(%rbp), %esi
	movq	-88(%rbp), %rdi
	call	OPENSSL_sk_value@PLT
	movq	16(%rax), %r12
	movq	%rax, %rdx
	testq	%r12, %r12
	jne	.L157
	movq	8(%rax), %rsi
	movq	%r13, %rdi
	call	X509V3_get_section@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L173
	leaq	DIST_POINT_it(%rip), %rdi
	call	ASN1_item_new@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L160
	leaq	8(%rax), %rax
	movq	%r12, -104(%rbp)
	xorl	%ebx, %ebx
	movq	%rax, -72(%rbp)
	jmp	.L161
	.p2align 4,,10
	.p2align 3
.L163:
	movq	%rax, %rsi
	movl	$10, %ecx
	leaq	.LC28(%rip), %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L206
.L164:
	addl	$1, %ebx
.L161:
	movq	%r14, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jge	.L207
	movl	%ebx, %esi
	movq	%r14, %rdi
	call	OPENSSL_sk_value@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	8(%rax), %rdx
	leaq	16(%rax), %rcx
	movq	%rax, %r12
	call	set_dist_point_name.isra.0
	testl	%eax, %eax
	jg	.L164
	jne	.L201
	movq	8(%r12), %rax
	movl	$8, %ecx
	leaq	.LC27(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	jne	.L163
	movq	16(%r12), %rsi
	movq	-72(%rbp), %rdi
	call	set_reasons
	testl	%eax, %eax
	jne	.L164
.L201:
	movq	-104(%rbp), %r12
.L160:
	leaq	DIST_POINT_it(%rip), %rsi
	movq	%r15, %rdi
	call	ASN1_item_free@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	xorl	%r14d, %r14d
	call	X509V3_section_free@PLT
.L159:
	movq	%r14, %rdi
	call	GENERAL_NAME_free@PLT
	movq	%r12, %rdi
	call	GENERAL_NAMES_free@PLT
	movq	-64(%rbp), %rdi
	leaq	DIST_POINT_free(%rip), %rsi
	call	OPENSSL_sk_pop_free@PLT
	movq	$0, -64(%rbp)
.L154:
	movq	-64(%rbp), %rax
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L206:
	.cfi_restore_state
	movq	16(%r12), %r12
	cmpb	$64, (%r12)
	je	.L208
	movq	%r12, %rdi
	call	X509V3_parse_list@PLT
	movq	%rax, %r8
	testq	%r8, %r8
	je	.L209
.L167:
	movq	%r8, %rdx
	xorl	%edi, %edi
	movq	%r13, %rsi
	movq	%r8, -80(%rbp)
	call	v2i_GENERAL_NAMES@PLT
	cmpb	$64, (%r12)
	movq	-80(%rbp), %r8
	movq	%rax, -80(%rbp)
	je	.L210
	movq	X509V3_conf_free@GOTPCREL(%rip), %rsi
	movq	%r8, %rdi
	call	OPENSSL_sk_pop_free@PLT
	movq	-80(%rbp), %rax
.L169:
	movq	%rax, 16(%r15)
	testq	%rax, %rax
	jne	.L164
	jmp	.L201
	.p2align 4,,10
	.p2align 3
.L207:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	X509V3_section_free@PLT
	movq	-64(%rbp), %rdi
	movq	%r15, %rsi
	call	OPENSSL_sk_push@PLT
	jmp	.L172
	.p2align 4,,10
	.p2align 3
.L208:
	leaq	1(%r12), %rsi
	movq	%r13, %rdi
	call	X509V3_get_section@PLT
	movq	%rax, %r8
	testq	%r8, %r8
	jne	.L167
.L209:
	movl	$55, %r8d
	leaq	.LC15(%rip), %rcx
	movl	$150, %edx
	movl	$156, %esi
	movl	$34, %edi
	movq	-104(%rbp), %r12
	call	ERR_put_error@PLT
	movq	$0, 16(%r15)
	jmp	.L160
	.p2align 4,,10
	.p2align 3
.L210:
	movq	%r8, %rsi
	movq	%r13, %rdi
	call	X509V3_section_free@PLT
	movq	-80(%rbp), %rax
	jmp	.L169
.L175:
	xorl	%r14d, %r14d
	xorl	%r12d, %r12d
	.p2align 4,,10
	.p2align 3
.L155:
	movl	$285, %r8d
	movl	$65, %edx
	movl	$134, %esi
	movl	$34, %edi
	leaq	.LC15(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L159
	.p2align 4,,10
	.p2align 3
.L173:
	xorl	%r14d, %r14d
	xorl	%r12d, %r12d
	jmp	.L159
	.p2align 4,,10
	.p2align 3
.L176:
	xorl	%r14d, %r14d
	jmp	.L155
	.cfi_endproc
.LFE1327:
	.size	v2i_crld, .-v2i_crld
	.p2align 4
	.globl	d2i_DIST_POINT_NAME
	.type	d2i_DIST_POINT_NAME, @function
d2i_DIST_POINT_NAME:
.LFB1329:
	.cfi_startproc
	endbr64
	leaq	DIST_POINT_NAME_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE1329:
	.size	d2i_DIST_POINT_NAME, .-d2i_DIST_POINT_NAME
	.p2align 4
	.globl	i2d_DIST_POINT_NAME
	.type	i2d_DIST_POINT_NAME, @function
i2d_DIST_POINT_NAME:
.LFB1330:
	.cfi_startproc
	endbr64
	leaq	DIST_POINT_NAME_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE1330:
	.size	i2d_DIST_POINT_NAME, .-i2d_DIST_POINT_NAME
	.p2align 4
	.globl	DIST_POINT_NAME_new
	.type	DIST_POINT_NAME_new, @function
DIST_POINT_NAME_new:
.LFB1331:
	.cfi_startproc
	endbr64
	leaq	DIST_POINT_NAME_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE1331:
	.size	DIST_POINT_NAME_new, .-DIST_POINT_NAME_new
	.p2align 4
	.globl	DIST_POINT_NAME_free
	.type	DIST_POINT_NAME_free, @function
DIST_POINT_NAME_free:
.LFB1332:
	.cfi_startproc
	endbr64
	leaq	DIST_POINT_NAME_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE1332:
	.size	DIST_POINT_NAME_free, .-DIST_POINT_NAME_free
	.p2align 4
	.globl	d2i_DIST_POINT
	.type	d2i_DIST_POINT, @function
d2i_DIST_POINT:
.LFB1333:
	.cfi_startproc
	endbr64
	leaq	DIST_POINT_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE1333:
	.size	d2i_DIST_POINT, .-d2i_DIST_POINT
	.p2align 4
	.globl	i2d_DIST_POINT
	.type	i2d_DIST_POINT, @function
i2d_DIST_POINT:
.LFB1334:
	.cfi_startproc
	endbr64
	leaq	DIST_POINT_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE1334:
	.size	i2d_DIST_POINT, .-i2d_DIST_POINT
	.p2align 4
	.globl	DIST_POINT_new
	.type	DIST_POINT_new, @function
DIST_POINT_new:
.LFB1335:
	.cfi_startproc
	endbr64
	leaq	DIST_POINT_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE1335:
	.size	DIST_POINT_new, .-DIST_POINT_new
	.p2align 4
	.globl	d2i_CRL_DIST_POINTS
	.type	d2i_CRL_DIST_POINTS, @function
d2i_CRL_DIST_POINTS:
.LFB1337:
	.cfi_startproc
	endbr64
	leaq	CRL_DIST_POINTS_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE1337:
	.size	d2i_CRL_DIST_POINTS, .-d2i_CRL_DIST_POINTS
	.p2align 4
	.globl	i2d_CRL_DIST_POINTS
	.type	i2d_CRL_DIST_POINTS, @function
i2d_CRL_DIST_POINTS:
.LFB1338:
	.cfi_startproc
	endbr64
	leaq	CRL_DIST_POINTS_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE1338:
	.size	i2d_CRL_DIST_POINTS, .-i2d_CRL_DIST_POINTS
	.p2align 4
	.globl	CRL_DIST_POINTS_new
	.type	CRL_DIST_POINTS_new, @function
CRL_DIST_POINTS_new:
.LFB1339:
	.cfi_startproc
	endbr64
	leaq	CRL_DIST_POINTS_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE1339:
	.size	CRL_DIST_POINTS_new, .-CRL_DIST_POINTS_new
	.p2align 4
	.globl	CRL_DIST_POINTS_free
	.type	CRL_DIST_POINTS_free, @function
CRL_DIST_POINTS_free:
.LFB1340:
	.cfi_startproc
	endbr64
	leaq	CRL_DIST_POINTS_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE1340:
	.size	CRL_DIST_POINTS_free, .-CRL_DIST_POINTS_free
	.p2align 4
	.globl	d2i_ISSUING_DIST_POINT
	.type	d2i_ISSUING_DIST_POINT, @function
d2i_ISSUING_DIST_POINT:
.LFB1341:
	.cfi_startproc
	endbr64
	leaq	ISSUING_DIST_POINT_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE1341:
	.size	d2i_ISSUING_DIST_POINT, .-d2i_ISSUING_DIST_POINT
	.p2align 4
	.globl	i2d_ISSUING_DIST_POINT
	.type	i2d_ISSUING_DIST_POINT, @function
i2d_ISSUING_DIST_POINT:
.LFB1342:
	.cfi_startproc
	endbr64
	leaq	ISSUING_DIST_POINT_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE1342:
	.size	i2d_ISSUING_DIST_POINT, .-i2d_ISSUING_DIST_POINT
	.p2align 4
	.globl	ISSUING_DIST_POINT_new
	.type	ISSUING_DIST_POINT_new, @function
ISSUING_DIST_POINT_new:
.LFB1343:
	.cfi_startproc
	endbr64
	leaq	ISSUING_DIST_POINT_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE1343:
	.size	ISSUING_DIST_POINT_new, .-ISSUING_DIST_POINT_new
	.p2align 4
	.globl	ISSUING_DIST_POINT_free
	.type	ISSUING_DIST_POINT_free, @function
ISSUING_DIST_POINT_free:
.LFB1344:
	.cfi_startproc
	endbr64
	leaq	ISSUING_DIST_POINT_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE1344:
	.size	ISSUING_DIST_POINT_free, .-ISSUING_DIST_POINT_free
	.p2align 4
	.globl	DIST_POINT_set_dpname
	.type	DIST_POINT_set_dpname, @function
DIST_POINT_set_dpname:
.LFB1350:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L237
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -40
	cmpl	$1, (%rdi)
	je	.L238
.L229:
	movl	$1, %eax
.L226:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L238:
	.cfi_restore_state
	movq	8(%rdi), %r13
	movq	%rsi, %rdi
	call	X509_NAME_dup@PLT
	movq	%rax, 16(%r12)
	testq	%rax, %rax
	je	.L233
	xorl	%ebx, %ebx
	jmp	.L230
	.p2align 4,,10
	.p2align 3
.L232:
	movl	%ebx, %esi
	movq	%r13, %rdi
	call	OPENSSL_sk_value@PLT
	xorl	%ecx, %ecx
	movq	16(%r12), %rdi
	testl	%ebx, %ebx
	sete	%cl
	movq	%rax, %rsi
	movl	$-1, %edx
	call	X509_NAME_add_entry@PLT
	testl	%eax, %eax
	je	.L239
	addl	$1, %ebx
.L230:
	movq	%r13, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jl	.L232
	movq	16(%r12), %rdi
	xorl	%esi, %esi
	call	i2d_X509_NAME@PLT
	testl	%eax, %eax
	jns	.L229
	movq	16(%r12), %rdi
	call	X509_NAME_free@PLT
	xorl	%eax, %eax
	movq	$0, 16(%r12)
	jmp	.L226
	.p2align 4,,10
	.p2align 3
.L237:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L239:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movq	16(%r12), %rdi
	movl	%eax, -36(%rbp)
	call	X509_NAME_free@PLT
	movl	-36(%rbp), %eax
	movq	$0, 16(%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L233:
	.cfi_restore_state
	xorl	%eax, %eax
	jmp	.L226
	.cfi_endproc
.LFE1350:
	.size	DIST_POINT_set_dpname, .-DIST_POINT_set_dpname
	.globl	v3_idp
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	v3_idp, @object
	.size	v3_idp, 104
v3_idp:
	.long	770
	.long	4
	.quad	ISSUING_DIST_POINT_it
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	v2i_idp
	.quad	i2r_idp
	.quad	0
	.quad	0
	.globl	ISSUING_DIST_POINT_it
	.section	.rodata.str1.1
.LC29:
	.string	"ISSUING_DIST_POINT"
	.section	.data.rel.ro.local
	.align 32
	.type	ISSUING_DIST_POINT_it, @object
	.size	ISSUING_DIST_POINT_it, 56
ISSUING_DIST_POINT_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	ISSUING_DIST_POINT_seq_tt
	.quad	6
	.quad	0
	.quad	32
	.quad	.LC29
	.section	.rodata.str1.1
.LC30:
	.string	"distpoint"
.LC31:
	.string	"onlyattr"
	.section	.data.rel.ro,"aw"
	.align 32
	.type	ISSUING_DIST_POINT_seq_tt, @object
	.size	ISSUING_DIST_POINT_seq_tt, 240
ISSUING_DIST_POINT_seq_tt:
	.quad	145
	.quad	0
	.quad	0
	.quad	.LC30
	.quad	DIST_POINT_NAME_it
	.quad	137
	.quad	1
	.quad	8
	.quad	.LC17
	.quad	ASN1_FBOOLEAN_it
	.quad	137
	.quad	2
	.quad	12
	.quad	.LC18
	.quad	ASN1_FBOOLEAN_it
	.quad	137
	.quad	3
	.quad	16
	.quad	.LC21
	.quad	ASN1_BIT_STRING_it
	.quad	137
	.quad	4
	.quad	24
	.quad	.LC20
	.quad	ASN1_FBOOLEAN_it
	.quad	137
	.quad	5
	.quad	28
	.quad	.LC31
	.quad	ASN1_FBOOLEAN_it
	.globl	CRL_DIST_POINTS_it
	.section	.rodata.str1.1
.LC32:
	.string	"CRL_DIST_POINTS"
	.section	.data.rel.ro.local
	.align 32
	.type	CRL_DIST_POINTS_it, @object
	.size	CRL_DIST_POINTS_it, 56
CRL_DIST_POINTS_it:
	.byte	0
	.zero	7
	.quad	-1
	.quad	CRL_DIST_POINTS_item_tt
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC32
	.section	.rodata.str1.1
.LC33:
	.string	"CRLDistributionPoints"
	.section	.data.rel.ro.local
	.align 32
	.type	CRL_DIST_POINTS_item_tt, @object
	.size	CRL_DIST_POINTS_item_tt, 40
CRL_DIST_POINTS_item_tt:
	.quad	4
	.quad	0
	.quad	0
	.quad	.LC33
	.quad	DIST_POINT_it
	.globl	DIST_POINT_it
	.section	.rodata.str1.1
.LC34:
	.string	"DIST_POINT"
	.section	.data.rel.ro.local
	.align 32
	.type	DIST_POINT_it, @object
	.size	DIST_POINT_it, 56
DIST_POINT_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	DIST_POINT_seq_tt
	.quad	3
	.quad	0
	.quad	32
	.quad	.LC34
	.section	.data.rel.ro
	.align 32
	.type	DIST_POINT_seq_tt, @object
	.size	DIST_POINT_seq_tt, 120
DIST_POINT_seq_tt:
	.quad	145
	.quad	0
	.quad	0
	.quad	.LC30
	.quad	DIST_POINT_NAME_it
	.quad	137
	.quad	1
	.quad	8
	.quad	.LC27
	.quad	ASN1_BIT_STRING_it
	.quad	141
	.quad	2
	.quad	16
	.quad	.LC28
	.quad	GENERAL_NAME_it
	.globl	DIST_POINT_NAME_it
	.section	.rodata.str1.1
.LC35:
	.string	"DIST_POINT_NAME"
	.section	.data.rel.ro.local
	.align 32
	.type	DIST_POINT_NAME_it, @object
	.size	DIST_POINT_NAME_it, 56
DIST_POINT_NAME_it:
	.byte	2
	.zero	7
	.quad	0
	.quad	DIST_POINT_NAME_ch_tt
	.quad	2
	.quad	DIST_POINT_NAME_aux
	.quad	24
	.quad	.LC35
	.section	.rodata.str1.1
.LC36:
	.string	"name.fullname"
.LC37:
	.string	"name.relativename"
	.section	.data.rel.ro
	.align 32
	.type	DIST_POINT_NAME_ch_tt, @object
	.size	DIST_POINT_NAME_ch_tt, 80
DIST_POINT_NAME_ch_tt:
	.quad	140
	.quad	0
	.quad	8
	.quad	.LC36
	.quad	GENERAL_NAME_it
	.quad	138
	.quad	1
	.quad	8
	.quad	.LC37
	.quad	X509_NAME_ENTRY_it
	.section	.data.rel.ro.local
	.align 32
	.type	DIST_POINT_NAME_aux, @object
	.size	DIST_POINT_NAME_aux, 40
DIST_POINT_NAME_aux:
	.quad	0
	.long	0
	.long	0
	.long	0
	.zero	4
	.quad	dpn_cb
	.long	0
	.zero	4
	.section	.rodata.str1.1
.LC38:
	.string	"Unused"
.LC39:
	.string	"unused"
.LC40:
	.string	"Key Compromise"
.LC41:
	.string	"keyCompromise"
.LC42:
	.string	"CA Compromise"
.LC43:
	.string	"CACompromise"
.LC44:
	.string	"Affiliation Changed"
.LC45:
	.string	"affiliationChanged"
.LC46:
	.string	"Superseded"
.LC47:
	.string	"superseded"
.LC48:
	.string	"Cessation Of Operation"
.LC49:
	.string	"cessationOfOperation"
.LC50:
	.string	"Certificate Hold"
.LC51:
	.string	"certificateHold"
.LC52:
	.string	"Privilege Withdrawn"
.LC53:
	.string	"privilegeWithdrawn"
.LC54:
	.string	"AA Compromise"
.LC55:
	.string	"AACompromise"
	.section	.data.rel.ro.local
	.align 32
	.type	reason_flags, @object
	.size	reason_flags, 240
reason_flags:
	.long	0
	.zero	4
	.quad	.LC38
	.quad	.LC39
	.long	1
	.zero	4
	.quad	.LC40
	.quad	.LC41
	.long	2
	.zero	4
	.quad	.LC42
	.quad	.LC43
	.long	3
	.zero	4
	.quad	.LC44
	.quad	.LC45
	.long	4
	.zero	4
	.quad	.LC46
	.quad	.LC47
	.long	5
	.zero	4
	.quad	.LC48
	.quad	.LC49
	.long	6
	.zero	4
	.quad	.LC50
	.quad	.LC51
	.long	7
	.zero	4
	.quad	.LC52
	.quad	.LC53
	.long	8
	.zero	4
	.quad	.LC54
	.quad	.LC55
	.long	-1
	.zero	4
	.quad	0
	.quad	0
	.globl	v3_freshest_crl
	.align 32
	.type	v3_freshest_crl, @object
	.size	v3_freshest_crl, 104
v3_freshest_crl:
	.long	857
	.long	0
	.quad	CRL_DIST_POINTS_it
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	v2i_crld
	.quad	i2r_crldp
	.quad	0
	.quad	0
	.globl	v3_crld
	.align 32
	.type	v3_crld, @object
	.size	v3_crld, 104
v3_crld:
	.long	103
	.long	0
	.quad	CRL_DIST_POINTS_it
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	v2i_crld
	.quad	i2r_crldp
	.quad	0
	.quad	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
