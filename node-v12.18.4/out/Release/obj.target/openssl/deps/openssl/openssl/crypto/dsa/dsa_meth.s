	.file	"dsa_meth.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/dsa/dsa_meth.c"
	.text
	.p2align 4
	.globl	DSA_meth_new
	.type	DSA_meth_new, @function
DSA_meth_new:
.LFB384:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$24, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	movl	$96, %edi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movl	%esi, %ebx
	leaq	.LC0(%rip), %rsi
	subq	$8, %rsp
	call	CRYPTO_zalloc@PLT
	testq	%rax, %rax
	je	.L2
	movl	%ebx, 64(%rax)
	movl	$29, %edx
	movq	%r13, %rdi
	movq	%rax, %r12
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_strdup@PLT
	movq	%rax, (%r12)
	testq	%rax, %rax
	je	.L9
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	.cfi_restore_state
	movl	$33, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
.L2:
	movl	$36, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r12d, %r12d
	movl	$128, %esi
	movl	$10, %edi
	call	ERR_put_error@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE384:
	.size	DSA_meth_new, .-DSA_meth_new
	.p2align 4
	.globl	DSA_meth_free
	.type	DSA_meth_free, @function
DSA_meth_free:
.LFB385:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L10
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$43, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	(%rdi), %rdi
	call	CRYPTO_free@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$44, %edx
	popq	%r12
	.cfi_restore 12
	leaq	.LC0(%rip), %rsi
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_free@PLT
	.p2align 4,,10
	.p2align 3
.L10:
	ret
	.cfi_endproc
.LFE385:
	.size	DSA_meth_free, .-DSA_meth_free
	.p2align 4
	.globl	DSA_meth_dup
	.type	DSA_meth_dup, @function
DSA_meth_dup:
.LFB386:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$50, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movl	$96, %edi
	call	CRYPTO_malloc@PLT
	testq	%rax, %rax
	je	.L16
	movdqu	(%rbx), %xmm0
	movl	$55, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rax, %r12
	movups	%xmm0, (%rax)
	movdqu	16(%rbx), %xmm1
	movups	%xmm1, 16(%rax)
	movdqu	32(%rbx), %xmm2
	movups	%xmm2, 32(%rax)
	movdqu	48(%rbx), %xmm3
	movups	%xmm3, 48(%rax)
	movdqu	64(%rbx), %xmm4
	movups	%xmm4, 64(%rax)
	movdqu	80(%rbx), %xmm5
	movups	%xmm5, 80(%rax)
	movq	(%rbx), %rdi
	call	CRYPTO_strdup@PLT
	movq	%rax, (%r12)
	testq	%rax, %rax
	je	.L22
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	.cfi_restore_state
	movl	$59, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
.L16:
	xorl	%r12d, %r12d
	movl	$62, %r8d
	movl	$65, %edx
	movl	$127, %esi
	leaq	.LC0(%rip), %rcx
	movl	$10, %edi
	call	ERR_put_error@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE386:
	.size	DSA_meth_dup, .-DSA_meth_dup
	.p2align 4
	.globl	DSA_meth_get0_name
	.type	DSA_meth_get0_name, @function
DSA_meth_get0_name:
.LFB387:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE387:
	.size	DSA_meth_get0_name, .-DSA_meth_get0_name
	.p2align 4
	.globl	DSA_meth_set1_name
	.type	DSA_meth_set1_name, @function
DSA_meth_set1_name:
.LFB388:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$73, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	.cfi_offset 3, -32
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_strdup@PLT
	testq	%rax, %rax
	je	.L28
	movq	(%r12), %rdi
	movq	%rax, %rbx
	movl	$80, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%rbx, (%r12)
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L28:
	.cfi_restore_state
	movl	$76, %r8d
	movl	$65, %edx
	movl	$129, %esi
	movl	$10, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE388:
	.size	DSA_meth_set1_name, .-DSA_meth_set1_name
	.p2align 4
	.globl	DSA_meth_get_flags
	.type	DSA_meth_get_flags, @function
DSA_meth_get_flags:
.LFB389:
	.cfi_startproc
	endbr64
	movl	64(%rdi), %eax
	ret
	.cfi_endproc
.LFE389:
	.size	DSA_meth_get_flags, .-DSA_meth_get_flags
	.p2align 4
	.globl	DSA_meth_set_flags
	.type	DSA_meth_set_flags, @function
DSA_meth_set_flags:
.LFB390:
	.cfi_startproc
	endbr64
	movl	%esi, 64(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE390:
	.size	DSA_meth_set_flags, .-DSA_meth_set_flags
	.p2align 4
	.globl	DSA_meth_get0_app_data
	.type	DSA_meth_get0_app_data, @function
DSA_meth_get0_app_data:
.LFB391:
	.cfi_startproc
	endbr64
	movq	72(%rdi), %rax
	ret
	.cfi_endproc
.LFE391:
	.size	DSA_meth_get0_app_data, .-DSA_meth_get0_app_data
	.p2align 4
	.globl	DSA_meth_set0_app_data
	.type	DSA_meth_set0_app_data, @function
DSA_meth_set0_app_data:
.LFB392:
	.cfi_startproc
	endbr64
	movq	%rsi, 72(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE392:
	.size	DSA_meth_set0_app_data, .-DSA_meth_set0_app_data
	.p2align 4
	.globl	DSA_meth_get_sign
	.type	DSA_meth_get_sign, @function
DSA_meth_get_sign:
.LFB393:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	ret
	.cfi_endproc
.LFE393:
	.size	DSA_meth_get_sign, .-DSA_meth_get_sign
	.p2align 4
	.globl	DSA_meth_set_sign
	.type	DSA_meth_set_sign, @function
DSA_meth_set_sign:
.LFB394:
	.cfi_startproc
	endbr64
	movq	%rsi, 8(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE394:
	.size	DSA_meth_set_sign, .-DSA_meth_set_sign
	.p2align 4
	.globl	DSA_meth_get_sign_setup
	.type	DSA_meth_get_sign_setup, @function
DSA_meth_get_sign_setup:
.LFB395:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	ret
	.cfi_endproc
.LFE395:
	.size	DSA_meth_get_sign_setup, .-DSA_meth_get_sign_setup
	.p2align 4
	.globl	DSA_meth_set_sign_setup
	.type	DSA_meth_set_sign_setup, @function
DSA_meth_set_sign_setup:
.LFB396:
	.cfi_startproc
	endbr64
	movq	%rsi, 16(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE396:
	.size	DSA_meth_set_sign_setup, .-DSA_meth_set_sign_setup
	.p2align 4
	.globl	DSA_meth_get_verify
	.type	DSA_meth_get_verify, @function
DSA_meth_get_verify:
.LFB397:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	ret
	.cfi_endproc
.LFE397:
	.size	DSA_meth_get_verify, .-DSA_meth_get_verify
	.p2align 4
	.globl	DSA_meth_set_verify
	.type	DSA_meth_set_verify, @function
DSA_meth_set_verify:
.LFB398:
	.cfi_startproc
	endbr64
	movq	%rsi, 24(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE398:
	.size	DSA_meth_set_verify, .-DSA_meth_set_verify
	.p2align 4
	.globl	DSA_meth_get_mod_exp
	.type	DSA_meth_get_mod_exp, @function
DSA_meth_get_mod_exp:
.LFB399:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	ret
	.cfi_endproc
.LFE399:
	.size	DSA_meth_get_mod_exp, .-DSA_meth_get_mod_exp
	.p2align 4
	.globl	DSA_meth_set_mod_exp
	.type	DSA_meth_set_mod_exp, @function
DSA_meth_set_mod_exp:
.LFB400:
	.cfi_startproc
	endbr64
	movq	%rsi, 32(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE400:
	.size	DSA_meth_set_mod_exp, .-DSA_meth_set_mod_exp
	.p2align 4
	.globl	DSA_meth_get_bn_mod_exp
	.type	DSA_meth_get_bn_mod_exp, @function
DSA_meth_get_bn_mod_exp:
.LFB401:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	ret
	.cfi_endproc
.LFE401:
	.size	DSA_meth_get_bn_mod_exp, .-DSA_meth_get_bn_mod_exp
	.p2align 4
	.globl	DSA_meth_set_bn_mod_exp
	.type	DSA_meth_set_bn_mod_exp, @function
DSA_meth_set_bn_mod_exp:
.LFB402:
	.cfi_startproc
	endbr64
	movq	%rsi, 40(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE402:
	.size	DSA_meth_set_bn_mod_exp, .-DSA_meth_set_bn_mod_exp
	.p2align 4
	.globl	DSA_meth_get_init
	.type	DSA_meth_get_init, @function
DSA_meth_get_init:
.LFB403:
	.cfi_startproc
	endbr64
	movq	48(%rdi), %rax
	ret
	.cfi_endproc
.LFE403:
	.size	DSA_meth_get_init, .-DSA_meth_get_init
	.p2align 4
	.globl	DSA_meth_set_init
	.type	DSA_meth_set_init, @function
DSA_meth_set_init:
.LFB404:
	.cfi_startproc
	endbr64
	movq	%rsi, 48(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE404:
	.size	DSA_meth_set_init, .-DSA_meth_set_init
	.p2align 4
	.globl	DSA_meth_get_finish
	.type	DSA_meth_get_finish, @function
DSA_meth_get_finish:
.LFB405:
	.cfi_startproc
	endbr64
	movq	56(%rdi), %rax
	ret
	.cfi_endproc
.LFE405:
	.size	DSA_meth_get_finish, .-DSA_meth_get_finish
	.p2align 4
	.globl	DSA_meth_set_finish
	.type	DSA_meth_set_finish, @function
DSA_meth_set_finish:
.LFB406:
	.cfi_startproc
	endbr64
	movq	%rsi, 56(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE406:
	.size	DSA_meth_set_finish, .-DSA_meth_set_finish
	.p2align 4
	.globl	DSA_meth_get_paramgen
	.type	DSA_meth_get_paramgen, @function
DSA_meth_get_paramgen:
.LFB407:
	.cfi_startproc
	endbr64
	movq	80(%rdi), %rax
	ret
	.cfi_endproc
.LFE407:
	.size	DSA_meth_get_paramgen, .-DSA_meth_get_paramgen
	.p2align 4
	.globl	DSA_meth_set_paramgen
	.type	DSA_meth_set_paramgen, @function
DSA_meth_set_paramgen:
.LFB408:
	.cfi_startproc
	endbr64
	movq	%rsi, 80(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE408:
	.size	DSA_meth_set_paramgen, .-DSA_meth_set_paramgen
	.p2align 4
	.globl	DSA_meth_get_keygen
	.type	DSA_meth_get_keygen, @function
DSA_meth_get_keygen:
.LFB409:
	.cfi_startproc
	endbr64
	movq	88(%rdi), %rax
	ret
	.cfi_endproc
.LFE409:
	.size	DSA_meth_get_keygen, .-DSA_meth_get_keygen
	.p2align 4
	.globl	DSA_meth_set_keygen
	.type	DSA_meth_set_keygen, @function
DSA_meth_set_keygen:
.LFB410:
	.cfi_startproc
	endbr64
	movq	%rsi, 88(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE410:
	.size	DSA_meth_set_keygen, .-DSA_meth_set_keygen
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
