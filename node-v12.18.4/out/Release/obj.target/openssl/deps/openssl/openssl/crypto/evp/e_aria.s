	.file	"e_aria.c"
	.text
	.p2align 4
	.type	aria_128_ecb_cipher, @function
aria_128_ecb_cipher:
.LFB478:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	call	EVP_CIPHER_CTX_cipher@PLT
	movslq	4(%rax), %r15
	cmpq	%r12, %r15
	ja	.L2
	subq	%r15, %r12
	movq	%r12, -56(%rbp)
	xorl	%r12d, %r12d
	.p2align 4,,10
	.p2align 3
.L3:
	movq	%rbx, %rdi
	call	EVP_CIPHER_CTX_encrypting@PLT
	movq	%rbx, %rdi
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	leaq	0(%r13,%r12), %rsi
	leaq	(%r14,%r12), %rdi
	addq	%r15, %r12
	movq	%rax, %rdx
	call	aria_encrypt@PLT
	cmpq	%r12, -56(%rbp)
	jnb	.L3
.L2:
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE478:
	.size	aria_128_ecb_cipher, .-aria_128_ecb_cipher
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/evp/e_aria.c"
	.text
	.p2align 4
	.type	aria_init_key, @function
aria_init_key:
.LFB469:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%ecx, %ebx
	call	EVP_CIPHER_CTX_cipher@PLT
	movq	%rax, %rdi
	call	EVP_CIPHER_flags@PLT
	movq	%r13, %rdi
	testl	%ebx, %ebx
	jne	.L8
	andl	$983047, %eax
	subl	$1, %eax
	cmpl	$1, %eax
	jbe	.L9
.L8:
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	%r13, %rdi
	movq	%rax, %r14
	call	EVP_CIPHER_CTX_key_length@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	leal	0(,%rax,8), %esi
	call	aria_set_encrypt_key@PLT
	movl	$1, %r8d
	testl	%eax, %eax
	js	.L14
.L7:
	popq	%rbx
	movl	%r8d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	.cfi_restore_state
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	%r13, %rdi
	movq	%rax, %r14
	call	EVP_CIPHER_CTX_key_length@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	leal	0(,%rax,8), %esi
	call	aria_set_decrypt_key@PLT
	movl	$1, %r8d
	testl	%eax, %eax
	jns	.L7
.L14:
	movl	$73, %r8d
	movl	$176, %edx
	movl	$185, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%r8d, %r8d
	popq	%rbx
	popq	%r12
	movl	%r8d, %eax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE469:
	.size	aria_init_key, .-aria_init_key
	.p2align 4
	.type	aria_128_cfb128_cipher, @function
aria_128_cfb128_cipher:
.LFB477:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movabsq	$4611686018427387903, %rax
	cmpq	%rax, %rcx
	ja	.L21
	testq	%rcx, %rcx
	je	.L18
	call	EVP_CIPHER_CTX_num@PLT
	movq	%r14, %rdi
	movl	%eax, -60(%rbp)
	call	EVP_CIPHER_CTX_encrypting@PLT
	movq	%r14, %rdi
	movl	%eax, -72(%rbp)
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movl	-72(%rbp), %edx
	leaq	-60(%rbp), %r9
	movq	%rbx, %r8
	pushq	aria_encrypt@GOTPCREL(%rip)
	movq	%rax, %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	pushq	%rdx
	movq	%r15, %rdx
	call	CRYPTO_cfb128_encrypt@PLT
	movl	-60(%rbp), %esi
	movq	%r14, %rdi
	call	EVP_CIPHER_CTX_set_num@PLT
	popq	%rax
	popq	%rdx
.L18:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L28
	leaq	-40(%rbp), %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	.cfi_restore_state
	movabsq	$4611686018427387904, %rbx
	leaq	-60(%rbp), %rax
	movq	%rax, -88(%rbp)
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L29:
	cmpq	%rbx, %r15
	jb	.L18
.L16:
	movq	%r14, %rdi
	subq	%rbx, %r15
	call	EVP_CIPHER_CTX_num@PLT
	movq	%r14, %rdi
	movl	%eax, -60(%rbp)
	call	EVP_CIPHER_CTX_encrypting@PLT
	movq	%r14, %rdi
	movl	%eax, -76(%rbp)
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movq	%r14, %rdi
	movq	%rax, -72(%rbp)
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movl	-76(%rbp), %edx
	movq	-88(%rbp), %r9
	movq	%r13, %rsi
	pushq	aria_encrypt@GOTPCREL(%rip)
	movq	-72(%rbp), %r8
	movq	%rax, %rcx
	movq	%r12, %rdi
	pushq	%rdx
	movq	%rbx, %rdx
	addq	%rbx, %r12
	addq	%rbx, %r13
	call	CRYPTO_cfb128_encrypt@PLT
	movl	-60(%rbp), %esi
	movq	%r14, %rdi
	call	EVP_CIPHER_CTX_set_num@PLT
	cmpq	%r15, %rbx
	popq	%rcx
	popq	%rsi
	cmova	%r15, %rbx
	testq	%r15, %r15
	jne	.L29
	jmp	.L18
.L28:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE477:
	.size	aria_128_cfb128_cipher, .-aria_128_cfb128_cipher
	.p2align 4
	.type	aria_192_cfb128_cipher, @function
aria_192_cfb128_cipher:
.LFB538:
	.cfi_startproc
	endbr64
	jmp	aria_128_cfb128_cipher
	.cfi_endproc
.LFE538:
	.size	aria_192_cfb128_cipher, .-aria_192_cfb128_cipher
	.p2align 4
	.type	aria_256_cfb128_cipher, @function
aria_256_cfb128_cipher:
.LFB540:
	.cfi_startproc
	endbr64
	jmp	aria_128_cfb128_cipher
	.cfi_endproc
.LFE540:
	.size	aria_256_cfb128_cipher, .-aria_256_cfb128_cipher
	.p2align 4
	.type	aria_128_ofb_cipher, @function
aria_128_ofb_cipher:
.LFB479:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	%rdx, -96(%rbp)
	movabsq	$4611686018427387903, %rdx
	movq	%rsi, -88(%rbp)
	movq	%fs:40, %rbx
	movq	%rbx, -56(%rbp)
	xorl	%ebx, %ebx
	movq	%rcx, %rbx
	cmpq	%rdx, %rcx
	jbe	.L33
	addq	%rcx, %rax
	movq	%rcx, -104(%rbp)
	leaq	(%rsi,%rcx), %rbx
	movq	%rcx, %r13
	movq	%rax, -80(%rbp)
	leaq	-60(%rbp), %r15
	movq	%rbx, -72(%rbp)
	movq	%rdx, %rbx
.L34:
	movq	%r12, %rdi
	call	EVP_CIPHER_CTX_num@PLT
	movq	%r12, %rdi
	movl	%eax, -60(%rbp)
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	subq	$8, %rsp
	movq	-72(%rbp), %rsi
	movq	%r15, %r9
	pushq	aria_encrypt@GOTPCREL(%rip)
	movq	-80(%rbp), %rdi
	movq	%rax, %rcx
	movq	%r14, %r8
	movabsq	$4611686018427387904, %rdx
	subq	%r13, %rsi
	subq	%r13, %rdi
	call	CRYPTO_ofb128_encrypt@PLT
	movl	-60(%rbp), %esi
	movq	%r12, %rdi
	call	EVP_CIPHER_CTX_set_num@PLT
	popq	%rcx
	popq	%rsi
	movabsq	$-4611686018427387904, %rax
	addq	%rax, %r13
	cmpq	%rbx, %r13
	ja	.L34
	movq	-104(%rbp), %r13
	leaq	0(%r13,%rax), %r11
	andq	%r13, %rbx
	shrq	$62, %r11
	addq	$1, %r11
	salq	$62, %r11
	addq	%r11, -96(%rbp)
	addq	%r11, -88(%rbp)
.L33:
	testq	%rbx, %rbx
	jne	.L44
.L35:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L45
	leaq	-40(%rbp), %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L44:
	.cfi_restore_state
	movq	%r12, %rdi
	call	EVP_CIPHER_CTX_num@PLT
	movq	%r12, %rdi
	movl	%eax, -60(%rbp)
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	subq	$8, %rsp
	movq	-88(%rbp), %rsi
	movq	%rbx, %rdx
	pushq	aria_encrypt@GOTPCREL(%rip)
	movq	-96(%rbp), %rdi
	movq	%rax, %rcx
	movq	%r14, %r8
	leaq	-60(%rbp), %r9
	call	CRYPTO_ofb128_encrypt@PLT
	movl	-60(%rbp), %esi
	movq	%r12, %rdi
	call	EVP_CIPHER_CTX_set_num@PLT
	popq	%rax
	popq	%rdx
	jmp	.L35
.L45:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE479:
	.size	aria_128_ofb_cipher, .-aria_128_ofb_cipher
	.p2align 4
	.type	aria_192_ofb_cipher, @function
aria_192_ofb_cipher:
.LFB550:
	.cfi_startproc
	endbr64
	jmp	aria_128_ofb_cipher
	.cfi_endproc
.LFE550:
	.size	aria_192_ofb_cipher, .-aria_192_ofb_cipher
	.p2align 4
	.type	aria_256_ofb_cipher, @function
aria_256_ofb_cipher:
.LFB552:
	.cfi_startproc
	endbr64
	jmp	aria_128_ofb_cipher
	.cfi_endproc
.LFE552:
	.size	aria_256_ofb_cipher, .-aria_256_ofb_cipher
	.p2align 4
	.type	aria_128_cfb1_cipher, @function
aria_128_cfb1_cipher:
.LFB500:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movabsq	$576460752303423487, %rax
	cmpq	%rax, %rcx
	ja	.L55
	movq	%rcx, %r14
	testq	%rcx, %rcx
	je	.L50
.L49:
	leaq	-60(%rbp), %rax
	movq	%rax, -96(%rbp)
	.p2align 4,,10
	.p2align 3
.L52:
	movq	%rbx, %rdi
	call	EVP_CIPHER_CTX_num@PLT
	movq	%rbx, %rdi
	movl	%eax, -60(%rbp)
	call	EVP_CIPHER_CTX_encrypting@PLT
	movq	%rbx, %rdi
	movl	%eax, -84(%rbp)
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movq	%rbx, %rdi
	movq	%rax, -80(%rbp)
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movl	$8192, %esi
	movq	%rbx, %rdi
	movq	%rax, -72(%rbp)
	call	EVP_CIPHER_CTX_test_flags@PLT
	movl	-84(%rbp), %r9d
	movq	-80(%rbp), %r8
	movq	%r12, %rsi
	testl	%eax, %eax
	pushq	aria_encrypt@GOTPCREL(%rip)
	movq	-72(%rbp), %rcx
	movq	%r13, %rdi
	pushq	%r9
	leaq	0(,%r14,8), %rdx
	cmovne	%r14, %rdx
	movq	-96(%rbp), %r9
	subq	%r14, %r15
	addq	%r14, %r13
	addq	%r14, %r12
	call	CRYPTO_cfb128_1_encrypt@PLT
	movl	-60(%rbp), %esi
	movq	%rbx, %rdi
	call	EVP_CIPHER_CTX_set_num@PLT
	cmpq	%r15, %r14
	popq	%rax
	popq	%rdx
	cmova	%r15, %r14
	testq	%r15, %r15
	je	.L50
	cmpq	%r15, %r14
	jbe	.L52
.L50:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L65
	leaq	-40(%rbp), %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L55:
	.cfi_restore_state
	movabsq	$576460752303423488, %r14
	jmp	.L49
.L65:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE500:
	.size	aria_128_cfb1_cipher, .-aria_128_cfb1_cipher
	.p2align 4
	.type	aria_192_cfb1_cipher, @function
aria_192_cfb1_cipher:
.LFB554:
	.cfi_startproc
	endbr64
	jmp	aria_128_cfb1_cipher
	.cfi_endproc
.LFE554:
	.size	aria_192_cfb1_cipher, .-aria_192_cfb1_cipher
	.p2align 4
	.type	aria_256_cfb1_cipher, @function
aria_256_cfb1_cipher:
.LFB556:
	.cfi_startproc
	endbr64
	jmp	aria_128_cfb1_cipher
	.cfi_endproc
.LFE556:
	.size	aria_256_cfb1_cipher, .-aria_256_cfb1_cipher
	.p2align 4
	.type	aria_128_cfb8_cipher, @function
aria_128_cfb8_cipher:
.LFB506:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movabsq	$4611686018427387903, %rax
	cmpq	%rax, %rcx
	ja	.L74
	testq	%rcx, %rcx
	je	.L71
	call	EVP_CIPHER_CTX_num@PLT
	movq	%r14, %rdi
	movl	%eax, -60(%rbp)
	call	EVP_CIPHER_CTX_encrypting@PLT
	movq	%r14, %rdi
	movl	%eax, -72(%rbp)
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movl	-72(%rbp), %edx
	leaq	-60(%rbp), %r9
	movq	%rbx, %r8
	pushq	aria_encrypt@GOTPCREL(%rip)
	movq	%rax, %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	pushq	%rdx
	movq	%r15, %rdx
	call	CRYPTO_cfb128_8_encrypt@PLT
	movl	-60(%rbp), %esi
	movq	%r14, %rdi
	call	EVP_CIPHER_CTX_set_num@PLT
	popq	%rax
	popq	%rdx
.L71:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L81
	leaq	-40(%rbp), %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L74:
	.cfi_restore_state
	movabsq	$4611686018427387904, %rbx
	leaq	-60(%rbp), %rax
	movq	%rax, -88(%rbp)
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L82:
	cmpq	%rbx, %r15
	jb	.L71
.L69:
	movq	%r14, %rdi
	subq	%rbx, %r15
	call	EVP_CIPHER_CTX_num@PLT
	movq	%r14, %rdi
	movl	%eax, -60(%rbp)
	call	EVP_CIPHER_CTX_encrypting@PLT
	movq	%r14, %rdi
	movl	%eax, -76(%rbp)
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movq	%r14, %rdi
	movq	%rax, -72(%rbp)
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movl	-76(%rbp), %edx
	movq	-88(%rbp), %r9
	movq	%r13, %rsi
	pushq	aria_encrypt@GOTPCREL(%rip)
	movq	-72(%rbp), %r8
	movq	%rax, %rcx
	movq	%r12, %rdi
	pushq	%rdx
	movq	%rbx, %rdx
	addq	%rbx, %r12
	addq	%rbx, %r13
	call	CRYPTO_cfb128_8_encrypt@PLT
	movl	-60(%rbp), %esi
	movq	%r14, %rdi
	call	EVP_CIPHER_CTX_set_num@PLT
	cmpq	%r15, %rbx
	popq	%rcx
	popq	%rsi
	cmova	%r15, %rbx
	testq	%r15, %r15
	jne	.L82
	jmp	.L71
.L81:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE506:
	.size	aria_128_cfb8_cipher, .-aria_128_cfb8_cipher
	.p2align 4
	.type	aria_192_cfb8_cipher, @function
aria_192_cfb8_cipher:
.LFB542:
	.cfi_startproc
	endbr64
	jmp	aria_128_cfb8_cipher
	.cfi_endproc
.LFE542:
	.size	aria_192_cfb8_cipher, .-aria_192_cfb8_cipher
	.p2align 4
	.type	aria_256_cfb8_cipher, @function
aria_256_cfb8_cipher:
.LFB544:
	.cfi_startproc
	endbr64
	jmp	aria_128_cfb8_cipher
	.cfi_endproc
.LFE544:
	.size	aria_256_cfb8_cipher, .-aria_256_cfb8_cipher
	.p2align 4
	.type	aria_ctr_cipher, @function
aria_ctr_cipher:
.LFB512:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	EVP_CIPHER_CTX_num@PLT
	movq	%r12, %rdi
	movl	%eax, -60(%rbp)
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	EVP_CIPHER_CTX_buf_noconst@PLT
	movq	%r12, %rdi
	movq	%rax, -72(%rbp)
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movq	-72(%rbp), %r9
	movq	%rbx, %rcx
	movq	%r15, %rdx
	movq	%rax, %r8
	leaq	-60(%rbp), %rax
	pushq	aria_encrypt@GOTPCREL(%rip)
	movq	%r14, %rsi
	pushq	%rax
	movq	%r13, %rdi
	call	CRYPTO_ctr128_encrypt@PLT
	movl	-60(%rbp), %esi
	movq	%r12, %rdi
	call	EVP_CIPHER_CTX_set_num@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L88
	leaq	-40(%rbp), %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L88:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE512:
	.size	aria_ctr_cipher, .-aria_ctr_cipher
	.p2align 4
	.type	aria_gcm_cleanup, @function
aria_gcm_cleanup:
.LFB521:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	%r12, %rdi
	movq	728(%rax), %r13
	movq	%rax, %rbx
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	cmpq	%rax, %r13
	je	.L90
	movq	728(%rbx), %rdi
	movl	$498, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
.L90:
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE521:
	.size	aria_gcm_cleanup, .-aria_gcm_cleanup
	.p2align 4
	.type	aria_gcm_ctrl, @function
aria_gcm_ctrl:
.LFB518:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$8, %rsp
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	cmpl	$37, %ebx
	ja	.L132
	leaq	.L95(%rip), %rdx
	movq	%rax, %r12
	movslq	(%rdx,%rbx,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L95:
	.long	.L104-.L95
	.long	.L132-.L95
	.long	.L132-.L95
	.long	.L132-.L95
	.long	.L132-.L95
	.long	.L132-.L95
	.long	.L132-.L95
	.long	.L132-.L95
	.long	.L103-.L95
	.long	.L102-.L95
	.long	.L132-.L95
	.long	.L132-.L95
	.long	.L132-.L95
	.long	.L132-.L95
	.long	.L132-.L95
	.long	.L132-.L95
	.long	.L101-.L95
	.long	.L100-.L95
	.long	.L99-.L95
	.long	.L98-.L95
	.long	.L132-.L95
	.long	.L132-.L95
	.long	.L97-.L95
	.long	.L132-.L95
	.long	.L96-.L95
	.long	.L132-.L95
	.long	.L132-.L95
	.long	.L132-.L95
	.long	.L132-.L95
	.long	.L132-.L95
	.long	.L132-.L95
	.long	.L132-.L95
	.long	.L132-.L95
	.long	.L132-.L95
	.long	.L132-.L95
	.long	.L132-.L95
	.long	.L132-.L95
	.long	.L94-.L95
	.text
	.p2align 4,,10
	.p2align 3
.L132:
	movl	$-1, %eax
.L92:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L94:
	.cfi_restore_state
	movl	736(%r12), %eax
	movl	%eax, (%r14)
	movl	$1, %eax
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L104:
	movq	$0, 280(%r12)
	movq	(%r15), %rdi
	call	EVP_CIPHER_iv_length@PLT
	movq	%r15, %rdi
	movl	%eax, 736(%r12)
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movl	$-1, 740(%r12)
	movq	%rax, 728(%r12)
	movabsq	$-4294967296, %rax
	movq	%rax, 744(%r12)
	movl	$1, %eax
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L103:
	movq	%r14, %rdi
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	%rax, %rbx
	movq	672(%r12), %rax
	testq	%rax, %rax
	je	.L129
	cmpq	%r12, %rax
	je	.L160
	.p2align 4,,10
	.p2align 3
.L155:
	xorl	%eax, %eax
.L161:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L102:
	.cfi_restore_state
	testl	%r13d, %r13d
	jle	.L155
	cmpl	$16, %r13d
	jle	.L106
	cmpl	%r13d, 736(%r12)
	jge	.L106
	movq	728(%r12), %rbx
	movq	%r15, %rdi
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	cmpq	%rax, %rbx
	je	.L107
	movq	728(%r12), %rdi
	movl	$268, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
.L107:
	movslq	%r13d, %rdi
	movl	$269, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_malloc@PLT
	movl	$270, %r8d
	movq	%rax, 728(%r12)
	testq	%rax, %rax
	jne	.L106
.L156:
	movl	$65, %edx
	movl	$197, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L161
	.p2align 4,,10
	.p2align 3
.L106:
	movl	%r13d, 736(%r12)
	movl	$1, %eax
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L101:
	leal	-1(%r13), %eax
	cmpl	$15, %eax
	ja	.L155
	movq	%r15, %rdi
	call	EVP_CIPHER_CTX_encrypting@PLT
	testl	%eax, %eax
	je	.L155
	movl	740(%r12), %edi
	testl	%edi, %edi
	js	.L155
	movq	%r15, %rdi
	call	EVP_CIPHER_CTX_buf_noconst@PLT
	cmpl	$8, %r13d
	jnb	.L115
	testb	$4, %r13b
	jne	.L162
	testl	%r13d, %r13d
	je	.L159
	movzbl	(%rax), %edx
	movb	%dl, (%r14)
	testb	$2, %r13b
	jne	.L163
	.p2align 4,,10
	.p2align 3
.L159:
	movl	$1, %eax
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L100:
	leal	-1(%r13), %eax
	cmpl	$15, %eax
	ja	.L155
	movq	%r15, %rdi
	call	EVP_CIPHER_CTX_encrypting@PLT
	testl	%eax, %eax
	jne	.L155
	movq	%r15, %rdi
	call	EVP_CIPHER_CTX_buf_noconst@PLT
	cmpl	$8, %r13d
	jnb	.L109
	testb	$4, %r13b
	jne	.L164
	testl	%r13d, %r13d
	je	.L110
	movzbl	(%r14), %edx
	movb	%dl, (%rax)
	testb	$2, %r13b
	jne	.L165
.L110:
	movl	%r13d, 740(%r12)
	movl	$1, %eax
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L99:
	cmpl	$-1, %r13d
	je	.L166
	cmpl	$3, %r13d
	jle	.L155
	movl	736(%r12), %eax
	subl	%r13d, %eax
	cmpl	$7, %eax
	jle	.L155
	movq	728(%r12), %rdi
	movslq	%r13d, %rbx
	movq	%r14, %rsi
	movq	%rbx, %rdx
	call	memcpy@PLT
	movq	%r15, %rdi
	call	EVP_CIPHER_CTX_encrypting@PLT
	testl	%eax, %eax
	jne	.L122
.L123:
	movl	$1, 744(%r12)
	movl	$1, %eax
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L98:
	movl	744(%r12), %esi
	testl	%esi, %esi
	je	.L155
	movl	280(%r12), %ecx
	testl	%ecx, %ecx
	je	.L155
	movslq	736(%r12), %rdx
	movq	728(%r12), %rsi
	leaq	288(%r12), %rdi
	call	CRYPTO_gcm128_setiv@PLT
	movslq	736(%r12), %rdx
	testl	%r13d, %r13d
	jle	.L125
	cmpl	%r13d, %edx
	jl	.L125
	movslq	%edx, %rsi
	movslq	%r13d, %rdx
.L126:
	subq	%rdx, %rsi
	movq	%r14, %rdi
	addq	728(%r12), %rsi
	call	memcpy@PLT
	movslq	736(%r12), %rax
	movq	728(%r12), %rdx
	leaq	-1(%rdx,%rax), %rsi
	leaq	-8(%rax), %rdi
	addb	$1, (%rsi)
	jne	.L158
	leaq	-2(%rdx,%rax), %rsi
	addb	$1, (%rsi)
	jne	.L158
	leaq	-3(%rdx,%rax), %rsi
	addb	$1, (%rsi)
	jne	.L158
	leaq	-4(%rdx,%rax), %rsi
	addb	$1, (%rsi)
	jne	.L158
	leaq	-5(%rdx,%rax), %rsi
	addb	$1, (%rsi)
	jne	.L158
	leaq	-6(%rdx,%rax), %rsi
	addb	$1, (%rsi)
	jne	.L158
	leaq	-7(%rdx,%rax), %rcx
	addb	$1, (%rcx)
	jne	.L158
	addb	$1, (%rdx,%rdi)
	.p2align 4,,10
	.p2align 3
.L158:
	movl	$1, 284(%r12)
	jmp	.L159
	.p2align 4,,10
	.p2align 3
.L97:
	cmpl	$13, %r13d
	jne	.L155
	movq	%r15, %rdi
	call	EVP_CIPHER_CTX_buf_noconst@PLT
	movq	(%r14), %rdx
	movq	%r15, %rdi
	movq	%rdx, (%rax)
	movl	8(%r14), %edx
	movl	%edx, 8(%rax)
	movzbl	12(%r14), %edx
	movb	%dl, 12(%rax)
	movl	$13, 748(%r12)
	call	EVP_CIPHER_CTX_buf_noconst@PLT
	movq	%r15, %rdi
	movzbl	11(%rax), %ebx
	call	EVP_CIPHER_CTX_buf_noconst@PLT
	sall	$8, %ebx
	movl	%ebx, %r12d
	movzbl	12(%rax), %ebx
	orl	%r12d, %ebx
	cmpl	$7, %ebx
	jle	.L155
	movq	%r15, %rdi
	leal	-8(%rbx), %r12d
	call	EVP_CIPHER_CTX_encrypting@PLT
	testl	%eax, %eax
	jne	.L128
	cmpl	$15, %r12d
	jbe	.L155
	leal	-24(%rbx), %r12d
.L128:
	movq	%r15, %rdi
	call	EVP_CIPHER_CTX_buf_noconst@PLT
	movl	%r12d, %ecx
	movq	%r15, %rdi
	movb	%ch, 11(%rax)
	call	EVP_CIPHER_CTX_buf_noconst@PLT
	movb	%r12b, 12(%rax)
	movl	$16, %eax
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L96:
	movl	744(%r12), %edx
	testl	%edx, %edx
	je	.L155
	movl	280(%r12), %eax
	testl	%eax, %eax
	je	.L155
	movq	%r15, %rdi
	call	EVP_CIPHER_CTX_encrypting@PLT
	testl	%eax, %eax
	jne	.L155
	movslq	736(%r12), %rdi
	movslq	%r13d, %rdx
	movq	%r14, %rsi
	subq	%rdx, %rdi
	addq	728(%r12), %rdi
	call	memcpy@PLT
	movslq	736(%r12), %rdx
	movq	728(%r12), %rsi
	leaq	288(%r12), %rdi
	call	CRYPTO_gcm128_setiv@PLT
	jmp	.L158
	.p2align 4,,10
	.p2align 3
.L160:
	movq	%rbx, 672(%rbx)
.L129:
	movq	728(%r12), %r13
	movq	%r15, %rdi
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	cmpq	%rax, %r13
	je	.L167
	movslq	736(%r12), %rdi
	movl	$378, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_malloc@PLT
	movq	%rax, 728(%rbx)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L168
	movslq	736(%r12), %rdx
	movq	728(%r12), %rsi
	call	memcpy@PLT
	jmp	.L159
	.p2align 4,,10
	.p2align 3
.L125:
	movslq	%edx, %rsi
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L167:
	movq	%r14, %rdi
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movq	%rax, 728(%rbx)
	movl	$1, %eax
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L109:
	movq	(%r14), %rdx
	leaq	8(%rax), %rdi
	andq	$-8, %rdi
	movq	%rdx, (%rax)
	movl	%r13d, %edx
	movq	-8(%r14,%rdx), %rcx
	movq	%rcx, -8(%rax,%rdx)
	subq	%rdi, %rax
	subq	%rax, %r14
	addl	%r13d, %eax
	andl	$-8, %eax
	cmpl	$8, %eax
	jb	.L110
	andl	$-8, %eax
	xorl	%edx, %edx
.L113:
	movl	%edx, %esi
	addl	$8, %edx
	movq	(%r14,%rsi), %r8
	movq	%r8, (%rdi,%rsi)
	cmpl	%eax, %edx
	jb	.L113
	jmp	.L110
	.p2align 4,,10
	.p2align 3
.L166:
	movq	728(%r12), %rdi
	movslq	736(%r12), %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movl	$1, %eax
	movl	$1, 744(%r12)
	jmp	.L92
.L122:
	movl	736(%r12), %esi
	addq	728(%r12), %rbx
	movq	%rbx, %rdi
	subl	%r13d, %esi
	call	RAND_bytes@PLT
	testl	%eax, %eax
	jg	.L123
	xorl	%eax, %eax
	jmp	.L161
.L115:
	movq	(%rax), %rdx
	leaq	8(%r14), %rsi
	andq	$-8, %rsi
	movq	%rdx, (%r14)
	movl	%r13d, %edx
	movq	-8(%rax,%rdx), %rcx
	movq	%rcx, -8(%r14,%rdx)
	subq	%rsi, %r14
	addl	%r14d, %r13d
	subq	%r14, %rax
	andl	$-8, %r13d
	cmpl	$8, %r13d
	jb	.L159
	andl	$-8, %r13d
	xorl	%edx, %edx
.L119:
	movl	%edx, %ecx
	addl	$8, %edx
	movq	(%rax,%rcx), %rdi
	movq	%rdi, (%rsi,%rcx)
	cmpl	%r13d, %edx
	jb	.L119
	jmp	.L159
.L168:
	movl	$379, %r8d
	jmp	.L156
.L164:
	movl	(%r14), %edx
	movl	%edx, (%rax)
	movl	%r13d, %edx
	movl	-4(%r14,%rdx), %ecx
	movl	%ecx, -4(%rax,%rdx)
	jmp	.L110
.L162:
	movl	(%rax), %edx
	movl	%r13d, %r13d
	movl	%edx, (%r14)
	movl	-4(%rax,%r13), %eax
	movl	%eax, -4(%r14,%r13)
	jmp	.L159
.L165:
	movl	%r13d, %edx
	movzwl	-2(%r14,%rdx), %ecx
	movw	%cx, -2(%rax,%rdx)
	jmp	.L110
.L163:
	movl	%r13d, %r13d
	movzwl	-2(%rax,%r13), %eax
	movw	%ax, -2(%r14,%r13)
	jmp	.L159
	.cfi_endproc
.LFE518:
	.size	aria_gcm_ctrl, .-aria_gcm_ctrl
	.p2align 4
	.type	aria_gcm_cipher, @function
aria_gcm_cipher:
.LFB520:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movl	280(%rax), %ecx
	testl	%ecx, %ecx
	je	.L173
	movl	748(%rax), %edx
	movq	%rax, %r15
	testl	%edx, %edx
	jns	.L189
	movl	284(%rax), %eax
	testl	%eax, %eax
	je	.L173
	testq	%r13, %r13
	je	.L180
	addq	$288, %r15
	testq	%r14, %r14
	je	.L190
	movq	%rbx, %rdi
	call	EVP_CIPHER_CTX_encrypting@PLT
	movq	%r12, %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	testl	%eax, %eax
	je	.L182
	call	CRYPTO_gcm128_encrypt@PLT
	testl	%eax, %eax
	jne	.L173
.L183:
	movl	%r12d, %eax
.L169:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L189:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	%rax, %r15
	cmpq	%r14, %r13
	jne	.L173
	cmpq	$23, %r12
	jbe	.L173
	movq	%rbx, %rdi
	call	EVP_CIPHER_CTX_encrypting@PLT
	movq	%r14, %rcx
	movl	$8, %edx
	movq	%rbx, %rdi
	cmpl	$1, %eax
	sbbl	%esi, %esi
	andl	$5, %esi
	addl	$19, %esi
	call	EVP_CIPHER_CTX_ctrl@PLT
	testl	%eax, %eax
	jle	.L188
	movslq	748(%r15), %rdx
	movq	%rbx, %rdi
	movq	%rdx, -56(%rbp)
	call	EVP_CIPHER_CTX_buf_noconst@PLT
	movq	-56(%rbp), %rdx
	movq	%rax, %rsi
	leaq	288(%r15), %rax
	movq	%rax, %rdi
	movq	%rax, -56(%rbp)
	call	CRYPTO_gcm128_aad@PLT
	testl	%eax, %eax
	jne	.L188
	movq	%rbx, %rdi
	addq	$8, %r13
	addq	$8, %r14
	subq	$24, %r12
	call	EVP_CIPHER_CTX_encrypting@PLT
	movq	%r12, %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	testl	%eax, %eax
	jne	.L191
	movq	-56(%rbp), %rdi
	call	CRYPTO_gcm128_decrypt@PLT
	testl	%eax, %eax
	jne	.L188
	movq	%rbx, %rdi
	addq	%r12, %r13
	call	EVP_CIPHER_CTX_buf_noconst@PLT
	movq	-56(%rbp), %rdi
	movl	$16, %edx
	movq	%rax, %rsi
	call	CRYPTO_gcm128_tag@PLT
	movq	%rbx, %rdi
	call	EVP_CIPHER_CTX_buf_noconst@PLT
	movl	$16, %edx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	CRYPTO_memcmp@PLT
	movl	%eax, %r8d
	movl	%r12d, %eax
	testl	%r8d, %r8d
	je	.L176
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	OPENSSL_cleanse@PLT
	.p2align 4,,10
	.p2align 3
.L188:
	movl	$-1, %eax
.L176:
	movl	$0, 284(%r15)
	movl	$-1, 748(%r15)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L182:
	.cfi_restore_state
	call	CRYPTO_gcm128_decrypt@PLT
	testl	%eax, %eax
	je	.L183
	.p2align 4,,10
	.p2align 3
.L173:
	movl	$-1, %eax
	jmp	.L169
	.p2align 4,,10
	.p2align 3
.L190:
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	CRYPTO_gcm128_aad@PLT
	testl	%eax, %eax
	je	.L183
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L180:
	movq	%rbx, %rdi
	call	EVP_CIPHER_CTX_encrypting@PLT
	testl	%eax, %eax
	jne	.L184
	movl	740(%r15), %r12d
	testl	%r12d, %r12d
	js	.L173
	movq	%rbx, %rdi
	call	EVP_CIPHER_CTX_buf_noconst@PLT
	movslq	%r12d, %rdx
	leaq	288(%r15), %rdi
	movq	%rax, %rsi
	call	CRYPTO_gcm128_finish@PLT
	testl	%eax, %eax
	jne	.L173
	movl	$0, 284(%r15)
	jmp	.L169
	.p2align 4,,10
	.p2align 3
.L191:
	movq	-56(%rbp), %rbx
	movq	%rbx, %rdi
	call	CRYPTO_gcm128_encrypt@PLT
	testl	%eax, %eax
	jne	.L188
	leaq	(%r14,%r12), %rsi
	movl	$16, %edx
	movq	%rbx, %rdi
	call	CRYPTO_gcm128_tag@PLT
	leal	24(%r12), %eax
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L184:
	movq	%rbx, %rdi
	call	EVP_CIPHER_CTX_buf_noconst@PLT
	leaq	288(%r15), %rdi
	movl	$16, %edx
	movq	%rax, %rsi
	call	CRYPTO_gcm128_tag@PLT
	xorl	%eax, %eax
	movl	$16, 740(%r15)
	movl	$0, 284(%r15)
	jmp	.L169
	.cfi_endproc
.LFE520:
	.size	aria_gcm_cipher, .-aria_gcm_cipher
	.p2align 4
	.type	aria_gcm_init_key, @function
aria_gcm_init_key:
.LFB517:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	%r14, %rcx
	orq	%r12, %rcx
	movq	%rax, %rbx
	movl	$1, %eax
	je	.L192
	testq	%r12, %r12
	je	.L194
	movq	%r13, %rdi
	leaq	288(%rbx), %r13
	call	EVP_CIPHER_CTX_key_length@PLT
	movq	%r12, %rdi
	movq	%rbx, %rdx
	leal	0(,%rax,8), %esi
	call	aria_set_encrypt_key@PLT
	movq	aria_encrypt@GOTPCREL(%rip), %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movl	%eax, %r12d
	call	CRYPTO_gcm128_init@PLT
	testl	%r12d, %r12d
	js	.L206
	testq	%r14, %r14
	je	.L207
.L196:
	movslq	736(%rbx), %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	CRYPTO_gcm128_setiv@PLT
	movl	$1, 284(%rbx)
.L197:
	movl	$1, 280(%rbx)
	movl	$1, %eax
.L192:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L194:
	.cfi_restore_state
	movl	280(%rbx), %eax
	movslq	736(%rbx), %rdx
	testl	%eax, %eax
	jne	.L208
	movq	728(%rbx), %rdi
	movq	%r14, %rsi
	call	memcpy@PLT
.L199:
	movl	$1, 284(%rbx)
	movl	$1, %eax
	movl	$0, 744(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L208:
	.cfi_restore_state
	leaq	288(%rbx), %rdi
	movq	%r14, %rsi
	call	CRYPTO_gcm128_setiv@PLT
	jmp	.L199
	.p2align 4,,10
	.p2align 3
.L207:
	movl	284(%rbx), %edx
	testl	%edx, %edx
	je	.L197
	movq	728(%rbx), %r14
	testq	%r14, %r14
	je	.L197
	jmp	.L196
	.p2align 4,,10
	.p2align 3
.L206:
	movl	$221, %r8d
	movl	$176, %edx
	movl	$176, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L192
	.cfi_endproc
.LFE517:
	.size	aria_gcm_init_key, .-aria_gcm_init_key
	.p2align 4
	.type	aria_ccm_ctrl, @function
aria_ccm_ctrl:
.LFB523:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$8, %rsp
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	cmpl	$37, %ebx
	ja	.L232
	leaq	.L212(%rip), %rdx
	movq	%rax, %r15
	movslq	(%rdx,%rbx,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L212:
	.long	.L220-.L212
	.long	.L232-.L212
	.long	.L232-.L212
	.long	.L232-.L212
	.long	.L232-.L212
	.long	.L232-.L212
	.long	.L232-.L212
	.long	.L232-.L212
	.long	.L219-.L212
	.long	.L218-.L212
	.long	.L232-.L212
	.long	.L232-.L212
	.long	.L232-.L212
	.long	.L232-.L212
	.long	.L232-.L212
	.long	.L232-.L212
	.long	.L217-.L212
	.long	.L216-.L212
	.long	.L215-.L212
	.long	.L232-.L212
	.long	.L214-.L212
	.long	.L232-.L212
	.long	.L213-.L212
	.long	.L232-.L212
	.long	.L232-.L212
	.long	.L232-.L212
	.long	.L232-.L212
	.long	.L232-.L212
	.long	.L232-.L212
	.long	.L232-.L212
	.long	.L232-.L212
	.long	.L232-.L212
	.long	.L232-.L212
	.long	.L232-.L212
	.long	.L232-.L212
	.long	.L232-.L212
	.long	.L232-.L212
	.long	.L211-.L212
	.text
	.p2align 4,,10
	.p2align 3
.L232:
	movl	$-1, %eax
.L209:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L218:
	.cfi_restore_state
	movl	$15, %eax
	subl	%r13d, %eax
	movl	%eax, %r13d
.L214:
	leal	-2(%r13), %eax
	cmpl	$6, %eax
	ja	.L222
	movl	%r13d, 296(%r15)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L213:
	.cfi_restore_state
	cmpl	$13, %r13d
	je	.L221
.L222:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L211:
	.cfi_restore_state
	movl	$15, %eax
	subl	296(%r15), %eax
	movl	%eax, (%r14)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L220:
	.cfi_restore_state
	movabsq	$51539607560, %rax
	pxor	%xmm0, %xmm0
	movl	$-1, 304(%r15)
	movq	%rax, 296(%r15)
	movl	$1, %eax
	movups	%xmm0, 280(%r15)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L219:
	.cfi_restore_state
	movq	%r14, %rdi
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	360(%r15), %rcx
	movq	%rax, %rdx
	movl	$1, %eax
	testq	%rcx, %rcx
	je	.L209
	cmpq	%r15, %rcx
	jne	.L222
	movq	%rdx, 360(%rdx)
	jmp	.L209
	.p2align 4,,10
	.p2align 3
.L217:
	movq	%r12, %rdi
	call	EVP_CIPHER_CTX_encrypting@PLT
	testl	%eax, %eax
	je	.L222
	movl	288(%r15), %eax
	testl	%eax, %eax
	je	.L222
	movslq	%r13d, %rdx
	leaq	312(%r15), %rdi
	movq	%r14, %rsi
	call	CRYPTO_ccm128_tag@PLT
	testq	%rax, %rax
	je	.L222
	movl	$0, 284(%r15)
	movl	$1, %eax
	movq	$0, 288(%r15)
	jmp	.L209
	.p2align 4,,10
	.p2align 3
.L216:
	testb	$1, %r13b
	jne	.L222
	leal	-4(%r13), %eax
	cmpl	$12, %eax
	ja	.L222
	movq	%r12, %rdi
	call	EVP_CIPHER_CTX_encrypting@PLT
	testl	%eax, %eax
	je	.L224
	testq	%r14, %r14
	jne	.L222
.L225:
	movl	%r13d, 300(%r15)
	movl	$1, %eax
	jmp	.L209
	.p2align 4,,10
	.p2align 3
.L215:
	cmpl	$4, %r13d
	jne	.L222
	movq	%r12, %rdi
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movl	(%r14), %edx
	movl	%edx, (%rax)
	movl	$1, %eax
	jmp	.L209
	.p2align 4,,10
	.p2align 3
.L221:
	movq	%r12, %rdi
	call	EVP_CIPHER_CTX_buf_noconst@PLT
	movq	(%r14), %rdx
	movq	%r12, %rdi
	movq	%rdx, (%rax)
	movl	8(%r14), %edx
	movl	%edx, 8(%rax)
	movzbl	12(%r14), %edx
	movb	%dl, 12(%rax)
	movl	$13, 304(%r15)
	call	EVP_CIPHER_CTX_buf_noconst@PLT
	movq	%r12, %rdi
	movzbl	11(%rax), %eax
	sall	$8, %eax
	movl	%eax, %ebx
	call	EVP_CIPHER_CTX_buf_noconst@PLT
	movzbl	12(%rax), %eax
	orl	%ebx, %eax
	cmpw	$7, %ax
	jbe	.L222
	movq	%r12, %rdi
	leal	-8(%rax), %ebx
	call	EVP_CIPHER_CTX_encrypting@PLT
	testl	%eax, %eax
	jne	.L223
	movl	300(%r15), %eax
	movzwl	%bx, %edx
	cmpl	%eax, %edx
	jl	.L222
	subl	%eax, %ebx
.L223:
	movq	%r12, %rdi
	call	EVP_CIPHER_CTX_buf_noconst@PLT
	movl	%ebx, %edx
	movq	%r12, %rdi
	shrw	$8, %dx
	movb	%dl, 11(%rax)
	call	EVP_CIPHER_CTX_buf_noconst@PLT
	movb	%bl, 12(%rax)
	movl	300(%r15), %eax
	jmp	.L209
	.p2align 4,,10
	.p2align 3
.L224:
	testq	%r14, %r14
	je	.L225
	movl	$1, 288(%r15)
	movq	%r12, %rdi
	call	EVP_CIPHER_CTX_buf_noconst@PLT
	cmpl	$8, %r13d
	jnb	.L226
	testb	$4, %r13b
	jne	.L252
	testl	%r13d, %r13d
	je	.L225
	movzbl	(%r14), %edx
	movb	%dl, (%rax)
	testb	$2, %r13b
	je	.L225
	movl	%r13d, %edx
	movzwl	-2(%r14,%rdx), %ecx
	movw	%cx, -2(%rax,%rdx)
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L226:
	movq	(%r14), %rdx
	leaq	8(%rax), %rdi
	andq	$-8, %rdi
	movq	%rdx, (%rax)
	movl	%r13d, %edx
	movq	-8(%r14,%rdx), %rcx
	movq	%rcx, -8(%rax,%rdx)
	subq	%rdi, %rax
	subq	%rax, %r14
	addl	%r13d, %eax
	andl	$-8, %eax
	cmpl	$8, %eax
	jb	.L225
	andl	$-8, %eax
	xorl	%edx, %edx
.L230:
	movl	%edx, %esi
	addl	$8, %edx
	movq	(%r14,%rsi), %r8
	movq	%r8, (%rdi,%rsi)
	cmpl	%eax, %edx
	jb	.L230
	jmp	.L225
.L252:
	movl	(%r14), %edx
	movl	%edx, (%rax)
	movl	%r13d, %edx
	movl	-4(%r14,%rdx), %ecx
	movl	%ecx, -4(%rax,%rdx)
	jmp	.L225
	.cfi_endproc
.LFE523:
	.size	aria_ccm_ctrl, .-aria_ccm_ctrl
	.p2align 4
	.type	aria_ccm_cipher, @function
aria_ccm_cipher:
.LFB525:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movl	280(%rax), %r8d
	testl	%r8d, %r8d
	je	.L306
	movl	304(%rax), %edi
	movq	%rax, %rbx
	testl	%edi, %edi
	jns	.L308
	testq	%r12, %r12
	jne	.L285
	xorl	%eax, %eax
	testq	%r13, %r13
	jne	.L253
.L285:
	movl	284(%rbx), %esi
	testl	%esi, %esi
	je	.L306
	leaq	312(%rbx), %rax
	movq	%rax, -88(%rbp)
	testq	%r13, %r13
	je	.L309
	movq	%r15, %rdi
	call	EVP_CIPHER_CTX_encrypting@PLT
	testl	%eax, %eax
	jne	.L270
	movl	288(%rbx), %edx
	testl	%edx, %edx
	je	.L306
.L270:
	movl	292(%rbx), %eax
	testl	%eax, %eax
	jne	.L271
	movl	$15, %edx
	subl	296(%rbx), %edx
	movq	%r15, %rdi
	movslq	%edx, %rdx
	movq	%rdx, -88(%rbp)
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movq	-88(%rbp), %rdx
	movq	%r14, %rcx
	leaq	312(%rbx), %rdi
	movq	%rax, %rsi
	call	CRYPTO_ccm128_setiv@PLT
	testl	%eax, %eax
	jne	.L306
	movl	$1, 292(%rbx)
.L271:
	movq	%r15, %rdi
	call	EVP_CIPHER_CTX_encrypting@PLT
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	368(%rbx), %r8
	leaq	312(%rbx), %rdi
	testl	%eax, %eax
	je	.L272
	testq	%r8, %r8
	je	.L273
	call	CRYPTO_ccm128_encrypt_ccm64@PLT
	testl	%eax, %eax
	setne	%al
.L274:
	testb	%al, %al
	jne	.L306
	movl	$1, 288(%rbx)
.L307:
	movl	%r14d, %eax
.L253:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L310
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L308:
	.cfi_restore_state
	movq	%r15, %rdi
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	%rax, %rbx
	leaq	312(%rax), %rax
	movq	%rax, -88(%rbp)
	cmpq	%r13, %r12
	jne	.L306
	movslq	300(%rbx), %rax
	addq	$8, %rax
	cmpq	%rax, %r14
	jb	.L306
	movq	%r15, %rdi
	call	EVP_CIPHER_CTX_encrypting@PLT
	testl	%eax, %eax
	jne	.L311
.L258:
	movq	%r15, %rdi
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movq	(%r12), %rdx
	movq	%r15, %rdi
	movq	%rdx, 4(%rax)
	movl	300(%rbx), %eax
	movl	$15, %edx
	subl	296(%rbx), %edx
	addl	$8, %eax
	movslq	%edx, %rdx
	cltq
	movq	%rdx, -88(%rbp)
	subq	%rax, %r14
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movq	-88(%rbp), %rdx
	movq	%r14, %rcx
	leaq	312(%rbx), %rdi
	movq	%rax, %rsi
	call	CRYPTO_ccm128_setiv@PLT
	testl	%eax, %eax
	jne	.L306
	movslq	304(%rbx), %rdx
	movq	%r15, %rdi
	addq	$8, %r12
	addq	$8, %r13
	movq	%rdx, -88(%rbp)
	call	EVP_CIPHER_CTX_buf_noconst@PLT
	movq	-88(%rbp), %rdx
	leaq	312(%rbx), %rdi
	movq	%rax, %rsi
	call	CRYPTO_ccm128_aad@PLT
	movq	%r15, %rdi
	call	EVP_CIPHER_CTX_encrypting@PLT
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	368(%rbx), %r8
	leaq	312(%rbx), %rdi
	testl	%eax, %eax
	je	.L259
	testq	%r8, %r8
	je	.L260
	call	CRYPTO_ccm128_encrypt_ccm64@PLT
	testl	%eax, %eax
	setne	%al
.L261:
	testb	%al, %al
	jne	.L306
	movslq	300(%rbx), %rdx
	leaq	0(%r13,%r14), %rsi
	leaq	312(%rbx), %rdi
	call	CRYPTO_ccm128_tag@PLT
	testq	%rax, %rax
	je	.L306
	movl	300(%rbx), %eax
	leal	8(%rax,%r14), %eax
	jmp	.L253
	.p2align 4,,10
	.p2align 3
.L309:
	testq	%r12, %r12
	je	.L312
	movl	292(%rbx), %ecx
	testl	%ecx, %ecx
	jne	.L286
	testq	%r14, %r14
	jne	.L306
.L286:
	movq	-88(%rbp), %rdi
	movq	%r14, %rdx
	movq	%r12, %rsi
	call	CRYPTO_ccm128_aad@PLT
	movl	%r14d, %eax
	jmp	.L253
	.p2align 4,,10
	.p2align 3
.L259:
	testq	%r8, %r8
	je	.L262
	call	CRYPTO_ccm128_decrypt_ccm64@PLT
	testl	%eax, %eax
	sete	%al
.L263:
	testb	%al, %al
	je	.L264
	leaq	-80(%rbp), %r15
	movslq	300(%rbx), %rdx
	leaq	312(%rbx), %rdi
	movq	%r15, %rsi
	call	CRYPTO_ccm128_tag@PLT
	testq	%rax, %rax
	je	.L264
	movslq	300(%rbx), %rdx
	leaq	(%r12,%r14), %rsi
	movq	%r15, %rdi
	call	CRYPTO_memcmp@PLT
	testl	%eax, %eax
	je	.L307
.L264:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	OPENSSL_cleanse@PLT
	.p2align 4,,10
	.p2align 3
.L306:
	movl	$-1, %eax
	jmp	.L253
	.p2align 4,,10
	.p2align 3
.L272:
	testq	%r8, %r8
	je	.L275
	call	CRYPTO_ccm128_decrypt_ccm64@PLT
	testl	%eax, %eax
	sete	%al
.L276:
	testb	%al, %al
	je	.L278
	leaq	-80(%rbp), %r12
	movslq	300(%rbx), %rdx
	leaq	312(%rbx), %rdi
	movq	%r12, %rsi
	call	CRYPTO_ccm128_tag@PLT
	testq	%rax, %rax
	je	.L278
	movslq	300(%rbx), %rdx
	movq	%r15, %rdi
	movq	%rdx, -88(%rbp)
	call	EVP_CIPHER_CTX_buf_noconst@PLT
	movq	-88(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	CRYPTO_memcmp@PLT
	testl	%eax, %eax
	jne	.L278
	movl	%r14d, %eax
	cmpl	$-1, %r14d
	jne	.L282
	.p2align 4,,10
	.p2align 3
.L278:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	OPENSSL_cleanse@PLT
	movl	$-1, %eax
.L282:
	movl	$0, 284(%rbx)
	movq	$0, 288(%rbx)
	jmp	.L253
	.p2align 4,,10
	.p2align 3
.L311:
	movq	%r15, %rdi
	call	EVP_CIPHER_CTX_buf_noconst@PLT
	movq	(%rax), %rax
	movq	%rax, (%r12)
	jmp	.L258
	.p2align 4,,10
	.p2align 3
.L312:
	movq	%r15, %rdi
	movl	$15, %r12d
	subl	296(%rbx), %r12d
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movslq	%r12d, %r12
	movq	%r14, %rcx
	leaq	312(%rbx), %rdi
	movq	%rax, %rsi
	movq	%r12, %rdx
	call	CRYPTO_ccm128_setiv@PLT
	testl	%eax, %eax
	jne	.L306
	movl	$1, 292(%rbx)
	movl	%r14d, %eax
	jmp	.L253
	.p2align 4,,10
	.p2align 3
.L273:
	call	CRYPTO_ccm128_encrypt@PLT
	testl	%eax, %eax
	setne	%al
	jmp	.L274
	.p2align 4,,10
	.p2align 3
.L260:
	call	CRYPTO_ccm128_encrypt@PLT
	testl	%eax, %eax
	setne	%al
	jmp	.L261
	.p2align 4,,10
	.p2align 3
.L262:
	call	CRYPTO_ccm128_decrypt@PLT
	testl	%eax, %eax
	sete	%al
	jmp	.L263
	.p2align 4,,10
	.p2align 3
.L275:
	call	CRYPTO_ccm128_decrypt@PLT
	testl	%eax, %eax
	sete	%al
	jmp	.L276
.L310:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE525:
	.size	aria_ccm_cipher, .-aria_ccm_cipher
	.p2align 4
	.type	aria_ccm_init_key, @function
aria_ccm_init_key:
.LFB522:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	%rax, %rbx
	movq	%r13, %rax
	orq	%r12, %rax
	je	.L326
	testq	%r12, %r12
	je	.L316
	movq	%r14, %rdi
	call	EVP_CIPHER_CTX_key_length@PLT
	movq	%r12, %rdi
	movq	%rbx, %rdx
	leal	0(,%rax,8), %esi
	call	aria_set_encrypt_key@PLT
	movl	296(%rbx), %edx
	movq	%rbx, %rcx
	movl	300(%rbx), %esi
	movq	aria_encrypt@GOTPCREL(%rip), %r8
	movl	%eax, %r12d
	leaq	312(%rbx), %rdi
	call	CRYPTO_ccm128_init@PLT
	testl	%r12d, %r12d
	js	.L327
	movq	$0, 368(%rbx)
	movl	$1, 280(%rbx)
.L316:
	testq	%r13, %r13
	je	.L326
	movq	%r14, %rdi
	movl	$15, %r12d
	subl	296(%rbx), %r12d
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movslq	%r12d, %r12
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%r12, %rdx
	call	memcpy@PLT
	movl	$1, 284(%rbx)
.L326:
	movl	$1, %eax
.L313:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L327:
	.cfi_restore_state
	movl	$518, %r8d
	movl	$176, %edx
	movl	$175, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L313
	.cfi_endproc
.LFE522:
	.size	aria_ccm_init_key, .-aria_ccm_init_key
	.p2align 4
	.type	aria_128_cbc_cipher, @function
aria_128_cbc_cipher:
.LFB476:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	%rdx, -96(%rbp)
	movabsq	$4611686018427387903, %rdx
	movq	%rsi, -88(%rbp)
	cmpq	%rdx, %rcx
	jbe	.L329
	addq	%rcx, %rsi
	addq	%rcx, %rax
	movq	%rdx, -64(%rbp)
	movq	%rcx, %r14
	movq	%rsi, -72(%rbp)
	movq	%rax, -80(%rbp)
	movq	%rcx, -104(%rbp)
.L332:
	movq	%r12, %rdi
	movq	-72(%rbp), %r15
	movq	-80(%rbp), %r13
	call	EVP_CIPHER_CTX_encrypting@PLT
	movq	%r12, %rdi
	movl	%eax, -52(%rbp)
	subq	%r14, %r15
	subq	%r14, %r13
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movl	-52(%rbp), %edx
	movq	%rbx, %r8
	movq	%r15, %rsi
	movq	aria_encrypt@GOTPCREL(%rip), %r9
	movq	%rax, %rcx
	movq	%r13, %rdi
	testl	%edx, %edx
	movabsq	$4611686018427387904, %rdx
	je	.L330
	call	CRYPTO_cbc128_encrypt@PLT
.L331:
	movabsq	$-4611686018427387904, %rax
	addq	%rax, %r14
	cmpq	-64(%rbp), %r14
	ja	.L332
	movq	-104(%rbp), %rbx
	movq	-64(%rbp), %r13
	andq	%rbx, %r13
	addq	%rax, %rbx
	shrq	$62, %rbx
	addq	$1, %rbx
	salq	$62, %rbx
	addq	%rbx, -96(%rbp)
	addq	%rbx, -88(%rbp)
.L329:
	testq	%r13, %r13
	jne	.L342
.L333:
	addq	$72, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L330:
	.cfi_restore_state
	call	CRYPTO_cbc128_decrypt@PLT
	jmp	.L331
	.p2align 4,,10
	.p2align 3
.L342:
	movq	%r12, %rdi
	call	EVP_CIPHER_CTX_encrypting@PLT
	movq	%r12, %rdi
	movl	%eax, %r14d
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	-88(%rbp), %rsi
	movq	-96(%rbp), %rdi
	movq	%rbx, %r8
	movq	aria_encrypt@GOTPCREL(%rip), %r9
	movq	%rax, %rcx
	movq	%r13, %rdx
	testl	%r14d, %r14d
	je	.L334
	call	CRYPTO_cbc128_encrypt@PLT
	jmp	.L333
	.p2align 4,,10
	.p2align 3
.L334:
	call	CRYPTO_cbc128_decrypt@PLT
	jmp	.L333
	.cfi_endproc
.LFE476:
	.size	aria_128_cbc_cipher, .-aria_128_cbc_cipher
	.p2align 4
	.type	aria_192_cbc_cipher, @function
aria_192_cbc_cipher:
.LFB534:
	.cfi_startproc
	endbr64
	jmp	aria_128_cbc_cipher
	.cfi_endproc
.LFE534:
	.size	aria_192_cbc_cipher, .-aria_192_cbc_cipher
	.p2align 4
	.type	aria_256_cbc_cipher, @function
aria_256_cbc_cipher:
.LFB536:
	.cfi_startproc
	endbr64
	jmp	aria_128_cbc_cipher
	.cfi_endproc
.LFE536:
	.size	aria_256_cbc_cipher, .-aria_256_cbc_cipher
	.p2align 4
	.type	aria_192_ecb_cipher, @function
aria_192_ecb_cipher:
.LFB546:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	call	EVP_CIPHER_CTX_cipher@PLT
	movslq	4(%rax), %r15
	cmpq	%r15, %r12
	jb	.L346
	subq	%r15, %r12
	movq	%r12, -56(%rbp)
	xorl	%r12d, %r12d
	.p2align 4,,10
	.p2align 3
.L347:
	movq	%rbx, %rdi
	call	EVP_CIPHER_CTX_encrypting@PLT
	movq	%rbx, %rdi
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	leaq	0(%r13,%r12), %rsi
	leaq	(%r14,%r12), %rdi
	addq	%r15, %r12
	movq	%rax, %rdx
	call	aria_encrypt@PLT
	cmpq	%r12, -56(%rbp)
	jnb	.L347
.L346:
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE546:
	.size	aria_192_ecb_cipher, .-aria_192_ecb_cipher
	.p2align 4
	.type	aria_256_ecb_cipher, @function
aria_256_ecb_cipher:
.LFB548:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	call	EVP_CIPHER_CTX_cipher@PLT
	movslq	4(%rax), %r15
	cmpq	%r15, %r12
	jb	.L351
	subq	%r15, %r12
	movq	%r12, -56(%rbp)
	xorl	%r12d, %r12d
	.p2align 4,,10
	.p2align 3
.L352:
	movq	%rbx, %rdi
	call	EVP_CIPHER_CTX_encrypting@PLT
	movq	%rbx, %rdi
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	leaq	0(%r13,%r12), %rsi
	leaq	(%r14,%r12), %rdi
	addq	%r15, %r12
	movq	%rax, %rdx
	call	aria_encrypt@PLT
	cmpq	%r12, -56(%rbp)
	jnb	.L352
.L351:
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE548:
	.size	aria_256_ecb_cipher, .-aria_256_ecb_cipher
	.p2align 4
	.globl	EVP_aria_128_cbc
	.type	EVP_aria_128_cbc, @function
EVP_aria_128_cbc:
.LFB480:
	.cfi_startproc
	endbr64
	leaq	aria_128_cbc(%rip), %rax
	ret
	.cfi_endproc
.LFE480:
	.size	EVP_aria_128_cbc, .-EVP_aria_128_cbc
	.p2align 4
	.globl	EVP_aria_128_cfb128
	.type	EVP_aria_128_cfb128, @function
EVP_aria_128_cfb128:
.LFB481:
	.cfi_startproc
	endbr64
	leaq	aria_128_cfb128(%rip), %rax
	ret
	.cfi_endproc
.LFE481:
	.size	EVP_aria_128_cfb128, .-EVP_aria_128_cfb128
	.p2align 4
	.globl	EVP_aria_128_ofb
	.type	EVP_aria_128_ofb, @function
EVP_aria_128_ofb:
.LFB482:
	.cfi_startproc
	endbr64
	leaq	aria_128_ofb(%rip), %rax
	ret
	.cfi_endproc
.LFE482:
	.size	EVP_aria_128_ofb, .-EVP_aria_128_ofb
	.p2align 4
	.globl	EVP_aria_128_ecb
	.type	EVP_aria_128_ecb, @function
EVP_aria_128_ecb:
.LFB483:
	.cfi_startproc
	endbr64
	leaq	aria_128_ecb(%rip), %rax
	ret
	.cfi_endproc
.LFE483:
	.size	EVP_aria_128_ecb, .-EVP_aria_128_ecb
	.p2align 4
	.globl	EVP_aria_192_cbc
	.type	EVP_aria_192_cbc, @function
EVP_aria_192_cbc:
.LFB488:
	.cfi_startproc
	endbr64
	leaq	aria_192_cbc(%rip), %rax
	ret
	.cfi_endproc
.LFE488:
	.size	EVP_aria_192_cbc, .-EVP_aria_192_cbc
	.p2align 4
	.globl	EVP_aria_192_cfb128
	.type	EVP_aria_192_cfb128, @function
EVP_aria_192_cfb128:
.LFB489:
	.cfi_startproc
	endbr64
	leaq	aria_192_cfb128(%rip), %rax
	ret
	.cfi_endproc
.LFE489:
	.size	EVP_aria_192_cfb128, .-EVP_aria_192_cfb128
	.p2align 4
	.globl	EVP_aria_192_ofb
	.type	EVP_aria_192_ofb, @function
EVP_aria_192_ofb:
.LFB490:
	.cfi_startproc
	endbr64
	leaq	aria_192_ofb(%rip), %rax
	ret
	.cfi_endproc
.LFE490:
	.size	EVP_aria_192_ofb, .-EVP_aria_192_ofb
	.p2align 4
	.globl	EVP_aria_192_ecb
	.type	EVP_aria_192_ecb, @function
EVP_aria_192_ecb:
.LFB491:
	.cfi_startproc
	endbr64
	leaq	aria_192_ecb(%rip), %rax
	ret
	.cfi_endproc
.LFE491:
	.size	EVP_aria_192_ecb, .-EVP_aria_192_ecb
	.p2align 4
	.globl	EVP_aria_256_cbc
	.type	EVP_aria_256_cbc, @function
EVP_aria_256_cbc:
.LFB496:
	.cfi_startproc
	endbr64
	leaq	aria_256_cbc(%rip), %rax
	ret
	.cfi_endproc
.LFE496:
	.size	EVP_aria_256_cbc, .-EVP_aria_256_cbc
	.p2align 4
	.globl	EVP_aria_256_cfb128
	.type	EVP_aria_256_cfb128, @function
EVP_aria_256_cfb128:
.LFB497:
	.cfi_startproc
	endbr64
	leaq	aria_256_cfb128(%rip), %rax
	ret
	.cfi_endproc
.LFE497:
	.size	EVP_aria_256_cfb128, .-EVP_aria_256_cfb128
	.p2align 4
	.globl	EVP_aria_256_ofb
	.type	EVP_aria_256_ofb, @function
EVP_aria_256_ofb:
.LFB498:
	.cfi_startproc
	endbr64
	leaq	aria_256_ofb(%rip), %rax
	ret
	.cfi_endproc
.LFE498:
	.size	EVP_aria_256_ofb, .-EVP_aria_256_ofb
	.p2align 4
	.globl	EVP_aria_256_ecb
	.type	EVP_aria_256_ecb, @function
EVP_aria_256_ecb:
.LFB499:
	.cfi_startproc
	endbr64
	leaq	aria_256_ecb(%rip), %rax
	ret
	.cfi_endproc
.LFE499:
	.size	EVP_aria_256_ecb, .-EVP_aria_256_ecb
	.p2align 4
	.globl	EVP_aria_128_cfb1
	.type	EVP_aria_128_cfb1, @function
EVP_aria_128_cfb1:
.LFB501:
	.cfi_startproc
	endbr64
	leaq	aria_128_cfb1(%rip), %rax
	ret
	.cfi_endproc
.LFE501:
	.size	EVP_aria_128_cfb1, .-EVP_aria_128_cfb1
	.p2align 4
	.globl	EVP_aria_192_cfb1
	.type	EVP_aria_192_cfb1, @function
EVP_aria_192_cfb1:
.LFB503:
	.cfi_startproc
	endbr64
	leaq	aria_192_cfb1(%rip), %rax
	ret
	.cfi_endproc
.LFE503:
	.size	EVP_aria_192_cfb1, .-EVP_aria_192_cfb1
	.p2align 4
	.globl	EVP_aria_256_cfb1
	.type	EVP_aria_256_cfb1, @function
EVP_aria_256_cfb1:
.LFB505:
	.cfi_startproc
	endbr64
	leaq	aria_256_cfb1(%rip), %rax
	ret
	.cfi_endproc
.LFE505:
	.size	EVP_aria_256_cfb1, .-EVP_aria_256_cfb1
	.p2align 4
	.globl	EVP_aria_128_cfb8
	.type	EVP_aria_128_cfb8, @function
EVP_aria_128_cfb8:
.LFB507:
	.cfi_startproc
	endbr64
	leaq	aria_128_cfb8(%rip), %rax
	ret
	.cfi_endproc
.LFE507:
	.size	EVP_aria_128_cfb8, .-EVP_aria_128_cfb8
	.p2align 4
	.globl	EVP_aria_192_cfb8
	.type	EVP_aria_192_cfb8, @function
EVP_aria_192_cfb8:
.LFB509:
	.cfi_startproc
	endbr64
	leaq	aria_192_cfb8(%rip), %rax
	ret
	.cfi_endproc
.LFE509:
	.size	EVP_aria_192_cfb8, .-EVP_aria_192_cfb8
	.p2align 4
	.globl	EVP_aria_256_cfb8
	.type	EVP_aria_256_cfb8, @function
EVP_aria_256_cfb8:
.LFB511:
	.cfi_startproc
	endbr64
	leaq	aria_256_cfb8(%rip), %rax
	ret
	.cfi_endproc
.LFE511:
	.size	EVP_aria_256_cfb8, .-EVP_aria_256_cfb8
	.p2align 4
	.globl	EVP_aria_128_ctr
	.type	EVP_aria_128_ctr, @function
EVP_aria_128_ctr:
.LFB513:
	.cfi_startproc
	endbr64
	leaq	aria_128_ctr(%rip), %rax
	ret
	.cfi_endproc
.LFE513:
	.size	EVP_aria_128_ctr, .-EVP_aria_128_ctr
	.p2align 4
	.globl	EVP_aria_192_ctr
	.type	EVP_aria_192_ctr, @function
EVP_aria_192_ctr:
.LFB514:
	.cfi_startproc
	endbr64
	leaq	aria_192_ctr(%rip), %rax
	ret
	.cfi_endproc
.LFE514:
	.size	EVP_aria_192_ctr, .-EVP_aria_192_ctr
	.p2align 4
	.globl	EVP_aria_256_ctr
	.type	EVP_aria_256_ctr, @function
EVP_aria_256_ctr:
.LFB515:
	.cfi_startproc
	endbr64
	leaq	aria_256_ctr(%rip), %rax
	ret
	.cfi_endproc
.LFE515:
	.size	EVP_aria_256_ctr, .-EVP_aria_256_ctr
	.p2align 4
	.globl	EVP_aria_128_gcm
	.type	EVP_aria_128_gcm, @function
EVP_aria_128_gcm:
.LFB526:
	.cfi_startproc
	endbr64
	leaq	aria_128_gcm(%rip), %rax
	ret
	.cfi_endproc
.LFE526:
	.size	EVP_aria_128_gcm, .-EVP_aria_128_gcm
	.p2align 4
	.globl	EVP_aria_192_gcm
	.type	EVP_aria_192_gcm, @function
EVP_aria_192_gcm:
.LFB527:
	.cfi_startproc
	endbr64
	leaq	aria_192_gcm(%rip), %rax
	ret
	.cfi_endproc
.LFE527:
	.size	EVP_aria_192_gcm, .-EVP_aria_192_gcm
	.p2align 4
	.globl	EVP_aria_256_gcm
	.type	EVP_aria_256_gcm, @function
EVP_aria_256_gcm:
.LFB528:
	.cfi_startproc
	endbr64
	leaq	aria_256_gcm(%rip), %rax
	ret
	.cfi_endproc
.LFE528:
	.size	EVP_aria_256_gcm, .-EVP_aria_256_gcm
	.p2align 4
	.globl	EVP_aria_128_ccm
	.type	EVP_aria_128_ccm, @function
EVP_aria_128_ccm:
.LFB529:
	.cfi_startproc
	endbr64
	leaq	aria_128_ccm(%rip), %rax
	ret
	.cfi_endproc
.LFE529:
	.size	EVP_aria_128_ccm, .-EVP_aria_128_ccm
	.p2align 4
	.globl	EVP_aria_192_ccm
	.type	EVP_aria_192_ccm, @function
EVP_aria_192_ccm:
.LFB530:
	.cfi_startproc
	endbr64
	leaq	aria_192_ccm(%rip), %rax
	ret
	.cfi_endproc
.LFE530:
	.size	EVP_aria_192_ccm, .-EVP_aria_192_ccm
	.p2align 4
	.globl	EVP_aria_256_ccm
	.type	EVP_aria_256_ccm, @function
EVP_aria_256_ccm:
.LFB531:
	.cfi_startproc
	endbr64
	leaq	aria_256_ccm(%rip), %rax
	ret
	.cfi_endproc
.LFE531:
	.size	EVP_aria_256_ccm, .-EVP_aria_256_ccm
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	aria_256_ccm, @object
	.size	aria_256_ccm, 88
aria_256_ccm:
	.long	1122
	.long	1
	.long	32
	.long	12
	.quad	3153015
	.quad	aria_ccm_init_key
	.quad	aria_ccm_cipher
	.quad	0
	.long	376
	.zero	4
	.quad	0
	.quad	0
	.quad	aria_ccm_ctrl
	.quad	0
	.align 32
	.type	aria_192_ccm, @object
	.size	aria_192_ccm, 88
aria_192_ccm:
	.long	1121
	.long	1
	.long	24
	.long	12
	.quad	3153015
	.quad	aria_ccm_init_key
	.quad	aria_ccm_cipher
	.quad	0
	.long	376
	.zero	4
	.quad	0
	.quad	0
	.quad	aria_ccm_ctrl
	.quad	0
	.align 32
	.type	aria_128_ccm, @object
	.size	aria_128_ccm, 88
aria_128_ccm:
	.long	1120
	.long	1
	.long	16
	.long	12
	.quad	3153015
	.quad	aria_ccm_init_key
	.quad	aria_ccm_cipher
	.quad	0
	.long	376
	.zero	4
	.quad	0
	.quad	0
	.quad	aria_ccm_ctrl
	.quad	0
	.align 32
	.type	aria_256_gcm, @object
	.size	aria_256_gcm, 88
aria_256_gcm:
	.long	1125
	.long	1
	.long	32
	.long	12
	.quad	3153014
	.quad	aria_gcm_init_key
	.quad	aria_gcm_cipher
	.quad	aria_gcm_cleanup
	.long	752
	.zero	4
	.quad	0
	.quad	0
	.quad	aria_gcm_ctrl
	.quad	0
	.align 32
	.type	aria_192_gcm, @object
	.size	aria_192_gcm, 88
aria_192_gcm:
	.long	1124
	.long	1
	.long	24
	.long	12
	.quad	3153014
	.quad	aria_gcm_init_key
	.quad	aria_gcm_cipher
	.quad	aria_gcm_cleanup
	.long	752
	.zero	4
	.quad	0
	.quad	0
	.quad	aria_gcm_ctrl
	.quad	0
	.align 32
	.type	aria_128_gcm, @object
	.size	aria_128_gcm, 88
aria_128_gcm:
	.long	1123
	.long	1
	.long	16
	.long	12
	.quad	3153014
	.quad	aria_gcm_init_key
	.quad	aria_gcm_cipher
	.quad	aria_gcm_cleanup
	.long	752
	.zero	4
	.quad	0
	.quad	0
	.quad	aria_gcm_ctrl
	.quad	0
	.align 32
	.type	aria_256_ctr, @object
	.size	aria_256_ctr, 88
aria_256_ctr:
	.long	1079
	.long	1
	.long	32
	.long	16
	.quad	5
	.quad	aria_init_key
	.quad	aria_ctr_cipher
	.quad	0
	.long	276
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.align 32
	.type	aria_192_ctr, @object
	.size	aria_192_ctr, 88
aria_192_ctr:
	.long	1074
	.long	1
	.long	24
	.long	16
	.quad	5
	.quad	aria_init_key
	.quad	aria_ctr_cipher
	.quad	0
	.long	276
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.align 32
	.type	aria_128_ctr, @object
	.size	aria_128_ctr, 88
aria_128_ctr:
	.long	1069
	.long	1
	.long	16
	.long	16
	.quad	5
	.quad	aria_init_key
	.quad	aria_ctr_cipher
	.quad	0
	.long	276
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.align 32
	.type	aria_256_cfb8, @object
	.size	aria_256_cfb8, 88
aria_256_cfb8:
	.long	1085
	.long	1
	.long	32
	.long	16
	.quad	4099
	.quad	aria_init_key
	.quad	aria_256_cfb8_cipher
	.quad	0
	.long	276
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.align 32
	.type	aria_192_cfb8, @object
	.size	aria_192_cfb8, 88
aria_192_cfb8:
	.long	1084
	.long	1
	.long	24
	.long	16
	.quad	4099
	.quad	aria_init_key
	.quad	aria_192_cfb8_cipher
	.quad	0
	.long	276
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.align 32
	.type	aria_128_cfb8, @object
	.size	aria_128_cfb8, 88
aria_128_cfb8:
	.long	1083
	.long	1
	.long	16
	.long	16
	.quad	4099
	.quad	aria_init_key
	.quad	aria_128_cfb8_cipher
	.quad	0
	.long	276
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.align 32
	.type	aria_256_cfb1, @object
	.size	aria_256_cfb1, 88
aria_256_cfb1:
	.long	1082
	.long	1
	.long	32
	.long	16
	.quad	4099
	.quad	aria_init_key
	.quad	aria_256_cfb1_cipher
	.quad	0
	.long	276
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.align 32
	.type	aria_192_cfb1, @object
	.size	aria_192_cfb1, 88
aria_192_cfb1:
	.long	1081
	.long	1
	.long	24
	.long	16
	.quad	4099
	.quad	aria_init_key
	.quad	aria_192_cfb1_cipher
	.quad	0
	.long	276
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.align 32
	.type	aria_128_cfb1, @object
	.size	aria_128_cfb1, 88
aria_128_cfb1:
	.long	1080
	.long	1
	.long	16
	.long	16
	.quad	4099
	.quad	aria_init_key
	.quad	aria_128_cfb1_cipher
	.quad	0
	.long	276
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.section	.data.rel.ro,"aw"
	.align 32
	.type	aria_256_ecb, @object
	.size	aria_256_ecb, 88
aria_256_ecb:
	.long	1075
	.long	16
	.long	32
	.long	0
	.quad	1
	.quad	aria_init_key
	.quad	aria_256_ecb_cipher
	.quad	0
	.long	276
	.zero	4
	.quad	EVP_CIPHER_set_asn1_iv
	.quad	EVP_CIPHER_get_asn1_iv
	.quad	0
	.quad	0
	.align 32
	.type	aria_256_ofb, @object
	.size	aria_256_ofb, 88
aria_256_ofb:
	.long	1078
	.long	1
	.long	32
	.long	16
	.quad	4
	.quad	aria_init_key
	.quad	aria_256_ofb_cipher
	.quad	0
	.long	276
	.zero	4
	.quad	EVP_CIPHER_set_asn1_iv
	.quad	EVP_CIPHER_get_asn1_iv
	.quad	0
	.quad	0
	.align 32
	.type	aria_256_cfb128, @object
	.size	aria_256_cfb128, 88
aria_256_cfb128:
	.long	1077
	.long	1
	.long	32
	.long	16
	.quad	3
	.quad	aria_init_key
	.quad	aria_256_cfb128_cipher
	.quad	0
	.long	276
	.zero	4
	.quad	EVP_CIPHER_set_asn1_iv
	.quad	EVP_CIPHER_get_asn1_iv
	.quad	0
	.quad	0
	.align 32
	.type	aria_256_cbc, @object
	.size	aria_256_cbc, 88
aria_256_cbc:
	.long	1076
	.long	16
	.long	32
	.long	16
	.quad	2
	.quad	aria_init_key
	.quad	aria_256_cbc_cipher
	.quad	0
	.long	276
	.zero	4
	.quad	EVP_CIPHER_set_asn1_iv
	.quad	EVP_CIPHER_get_asn1_iv
	.quad	0
	.quad	0
	.align 32
	.type	aria_192_ecb, @object
	.size	aria_192_ecb, 88
aria_192_ecb:
	.long	1070
	.long	16
	.long	24
	.long	0
	.quad	1
	.quad	aria_init_key
	.quad	aria_192_ecb_cipher
	.quad	0
	.long	276
	.zero	4
	.quad	EVP_CIPHER_set_asn1_iv
	.quad	EVP_CIPHER_get_asn1_iv
	.quad	0
	.quad	0
	.align 32
	.type	aria_192_ofb, @object
	.size	aria_192_ofb, 88
aria_192_ofb:
	.long	1073
	.long	1
	.long	24
	.long	16
	.quad	4
	.quad	aria_init_key
	.quad	aria_192_ofb_cipher
	.quad	0
	.long	276
	.zero	4
	.quad	EVP_CIPHER_set_asn1_iv
	.quad	EVP_CIPHER_get_asn1_iv
	.quad	0
	.quad	0
	.align 32
	.type	aria_192_cfb128, @object
	.size	aria_192_cfb128, 88
aria_192_cfb128:
	.long	1072
	.long	1
	.long	24
	.long	16
	.quad	3
	.quad	aria_init_key
	.quad	aria_192_cfb128_cipher
	.quad	0
	.long	276
	.zero	4
	.quad	EVP_CIPHER_set_asn1_iv
	.quad	EVP_CIPHER_get_asn1_iv
	.quad	0
	.quad	0
	.align 32
	.type	aria_192_cbc, @object
	.size	aria_192_cbc, 88
aria_192_cbc:
	.long	1071
	.long	16
	.long	24
	.long	16
	.quad	2
	.quad	aria_init_key
	.quad	aria_192_cbc_cipher
	.quad	0
	.long	276
	.zero	4
	.quad	EVP_CIPHER_set_asn1_iv
	.quad	EVP_CIPHER_get_asn1_iv
	.quad	0
	.quad	0
	.align 32
	.type	aria_128_ecb, @object
	.size	aria_128_ecb, 88
aria_128_ecb:
	.long	1065
	.long	16
	.long	16
	.long	0
	.quad	1
	.quad	aria_init_key
	.quad	aria_128_ecb_cipher
	.quad	0
	.long	276
	.zero	4
	.quad	EVP_CIPHER_set_asn1_iv
	.quad	EVP_CIPHER_get_asn1_iv
	.quad	0
	.quad	0
	.align 32
	.type	aria_128_ofb, @object
	.size	aria_128_ofb, 88
aria_128_ofb:
	.long	1068
	.long	1
	.long	16
	.long	16
	.quad	4
	.quad	aria_init_key
	.quad	aria_128_ofb_cipher
	.quad	0
	.long	276
	.zero	4
	.quad	EVP_CIPHER_set_asn1_iv
	.quad	EVP_CIPHER_get_asn1_iv
	.quad	0
	.quad	0
	.align 32
	.type	aria_128_cfb128, @object
	.size	aria_128_cfb128, 88
aria_128_cfb128:
	.long	1067
	.long	1
	.long	16
	.long	16
	.quad	3
	.quad	aria_init_key
	.quad	aria_128_cfb128_cipher
	.quad	0
	.long	276
	.zero	4
	.quad	EVP_CIPHER_set_asn1_iv
	.quad	EVP_CIPHER_get_asn1_iv
	.quad	0
	.quad	0
	.align 32
	.type	aria_128_cbc, @object
	.size	aria_128_cbc, 88
aria_128_cbc:
	.long	1066
	.long	16
	.long	16
	.long	16
	.quad	2
	.quad	aria_init_key
	.quad	aria_128_cbc_cipher
	.quad	0
	.long	276
	.zero	4
	.quad	EVP_CIPHER_set_asn1_iv
	.quad	EVP_CIPHER_get_asn1_iv
	.quad	0
	.quad	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
