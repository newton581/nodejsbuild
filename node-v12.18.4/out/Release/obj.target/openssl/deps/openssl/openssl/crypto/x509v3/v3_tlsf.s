	.file	"v3_tlsf.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/x509v3/v3_tlsf.c"
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"status_request"
.LC2:
	.string	"status_request_v2"
.LC3:
	.string	",value:"
.LC4:
	.string	",name:"
.LC5:
	.string	"section:"
	.text
	.p2align 4
	.type	v2i_TLS_FEATURE, @function
v2i_TLS_FEATURE:
.LFB1334:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	xorl	%r13d, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	jne	.L2
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L4:
	leaq	.LC1(%rip), %rsi
	movq	%rbx, %rdi
	call	strcasecmp@PLT
	testl	%eax, %eax
	je	.L15
	leaq	.LC2(%rip), %rsi
	movq	%rbx, %rdi
	call	strcasecmp@PLT
	testl	%eax, %eax
	je	.L16
	leaq	-64(%rbp), %rsi
	movl	$10, %edx
	movq	%rbx, %rdi
	call	strtol@PLT
	movq	%rax, %rsi
	movq	-64(%rbp), %rax
	cmpb	$0, (%rax)
	jne	.L6
	cmpq	%rbx, %rax
	je	.L6
	cmpq	$65535, %rsi
	ja	.L6
.L8:
	movq	%rsi, -72(%rbp)
	call	ASN1_INTEGER_new@PLT
	movq	-72(%rbp), %rsi
	testq	%rax, %rax
	movq	%rax, %r12
	je	.L11
	movq	%rax, %rdi
	call	ASN1_INTEGER_set@PLT
	testl	%eax, %eax
	je	.L11
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	jle	.L11
	addl	$1, %r13d
.L2:
	movq	%r14, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r13d
	jge	.L1
	movl	%r13d, %esi
	movq	%r14, %rdi
	call	OPENSSL_sk_value@PLT
	movq	16(%rax), %rbx
	movq	%rax, %r12
	testq	%rbx, %rbx
	jne	.L4
	movq	8(%rax), %rbx
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L11:
	movl	$128, %r8d
	movl	$65, %edx
	movl	$165, %esi
	movl	$34, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L9:
	movq	ASN1_INTEGER_free@GOTPCREL(%rip), %rsi
	movq	%r15, %rdi
	xorl	%r15d, %r15d
	call	OPENSSL_sk_pop_free@PLT
.L1:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L26
	leaq	-40(%rbp), %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	.cfi_restore_state
	movl	$5, %esi
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L16:
	movl	$17, %esi
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L6:
	movl	$119, %r8d
	movl	$143, %edx
	movl	$165, %esi
	movl	$34, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	subq	$8, %rsp
	movq	8(%r12), %r8
	xorl	%eax, %eax
	pushq	16(%r12)
	movq	(%r12), %rdx
	leaq	.LC3(%rip), %r9
	leaq	.LC4(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	movl	$6, %edi
	call	ERR_add_error_data@PLT
	popq	%rax
	popq	%rdx
	jmp	.L9
.L25:
	movl	$99, %r8d
	movl	$65, %edx
	movl	$165, %esi
	movl	$34, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L1
.L26:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1334:
	.size	v2i_TLS_FEATURE, .-v2i_TLS_FEATURE
	.p2align 4
	.type	i2v_TLS_FEATURE, @function
i2v_TLS_FEATURE:
.LFB1333:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	.LC1(%rip), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-56(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	xorl	%ebx, %ebx
	subq	$24, %rsp
	movq	%rdx, -56(%rbp)
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L31:
	movq	%r12, %rdi
	movl	%ebx, %esi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %rdi
	movq	%rax, %r15
	call	ASN1_INTEGER_get@PLT
	cmpq	$5, %rax
	je	.L32
	cmpq	$17, %rax
	je	.L33
	movq	%r13, %rdx
	movq	%r15, %rsi
	xorl	%edi, %edi
	call	X509V3_add_value_int@PLT
.L30:
	addl	$1, %ebx
.L28:
	movq	%r12, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jl	.L31
	movq	-56(%rbp), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	.cfi_restore_state
	movq	%r14, %rsi
.L29:
	movq	%r13, %rdx
	xorl	%edi, %edi
	call	X509V3_add_value@PLT
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L33:
	leaq	.LC2(%rip), %rsi
	jmp	.L29
	.cfi_endproc
.LFE1333:
	.size	i2v_TLS_FEATURE, .-i2v_TLS_FEATURE
	.p2align 4
	.globl	TLS_FEATURE_new
	.type	TLS_FEATURE_new, @function
TLS_FEATURE_new:
.LFB1331:
	.cfi_startproc
	endbr64
	leaq	TLS_FEATURE_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE1331:
	.size	TLS_FEATURE_new, .-TLS_FEATURE_new
	.p2align 4
	.globl	TLS_FEATURE_free
	.type	TLS_FEATURE_free, @function
TLS_FEATURE_free:
.LFB1332:
	.cfi_startproc
	endbr64
	leaq	TLS_FEATURE_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE1332:
	.size	TLS_FEATURE_free, .-TLS_FEATURE_free
	.globl	v3_tls_feature
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	v3_tls_feature, @object
	.size	v3_tls_feature, 104
v3_tls_feature:
	.long	1020
	.long	0
	.quad	TLS_FEATURE_it
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	i2v_TLS_FEATURE
	.quad	v2i_TLS_FEATURE
	.quad	0
	.quad	0
	.quad	0
	.section	.rodata.str1.1
.LC6:
	.string	"TLS_FEATURE"
	.section	.data.rel.ro.local
	.align 32
	.type	TLS_FEATURE_it, @object
	.size	TLS_FEATURE_it, 56
TLS_FEATURE_it:
	.byte	0
	.zero	7
	.quad	-1
	.quad	TLS_FEATURE_item_tt
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC6
	.section	.data.rel.ro,"aw"
	.align 32
	.type	TLS_FEATURE_item_tt, @object
	.size	TLS_FEATURE_item_tt, 40
TLS_FEATURE_item_tt:
	.quad	4
	.quad	0
	.quad	0
	.quad	.LC6
	.quad	ASN1_INTEGER_it
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
