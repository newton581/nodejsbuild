	.file	"m_md5_sha1.c"
	.text
	.p2align 4
	.type	update, @function
update:
.LFB806:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	call	EVP_MD_CTX_md_data@PLT
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, %rbx
	call	MD5_Update@PLT
	testl	%eax, %eax
	jne	.L5
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	.cfi_restore_state
	addq	$8, %rsp
	leaq	92(%rbx), %rdi
	movq	%r13, %rdx
	movq	%r12, %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	SHA1_Update@PLT
	.cfi_endproc
.LFE806:
	.size	update, .-update
	.p2align 4
	.type	final, @function
final:
.LFB807:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	call	EVP_MD_CTX_md_data@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	movq	%rax, %r12
	call	MD5_Final@PLT
	testl	%eax, %eax
	jne	.L9
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	.cfi_restore_state
	leaq	92(%r12), %rsi
	leaq	16(%rbx), %rdi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	SHA1_Final@PLT
	.cfi_endproc
.LFE807:
	.size	final, .-final
	.p2align 4
	.type	init, @function
init:
.LFB805:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	call	EVP_MD_CTX_md_data@PLT
	movq	%rax, %rdi
	movq	%rax, %rbx
	call	MD5_Init@PLT
	testl	%eax, %eax
	jne	.L13
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	.cfi_restore_state
	addq	$8, %rsp
	leaq	92(%rbx), %rdi
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	SHA1_Init@PLT
	.cfi_endproc
.LFE805:
	.size	init, .-init
	.p2align 4
	.type	ctrl, @function
ctrl:
.LFB808:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$29, %esi
	jne	.L20
	movq	%rdi, %r15
	testq	%rdi, %rdi
	je	.L14
	movl	%edx, %ebx
	movq	%rcx, %r12
	call	EVP_MD_CTX_md_data@PLT
	movq	%rax, %r13
	cmpl	$48, %ebx
	je	.L16
.L18:
	xorl	%eax, %eax
.L14:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L60
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	.cfi_restore_state
	movq	%r15, %rdi
	call	EVP_MD_CTX_md_data@PLT
	movl	$48, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, %rbx
	call	MD5_Update@PLT
	testl	%eax, %eax
	je	.L18
	leaq	92(%rbx), %rdi
	movl	$48, %edx
	movq	%r12, %rsi
	call	SHA1_Update@PLT
	testl	%eax, %eax
	jle	.L18
	movdqa	.LC0(%rip), %xmm0
	leaq	-112(%rbp), %r14
	movl	$48, %edx
	movq	%r13, %rdi
	movq	%r14, %rsi
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	MD5_Update@PLT
	testl	%eax, %eax
	je	.L18
	leaq	-160(%rbp), %rax
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%rax, -168(%rbp)
	call	MD5_Final@PLT
	testl	%eax, %eax
	je	.L18
	leaq	92(%r13), %rbx
	movl	$40, %edx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	SHA1_Update@PLT
	testl	%eax, %eax
	je	.L18
	leaq	-144(%rbp), %rcx
	movq	%rbx, %rsi
	movq	%rcx, %rdi
	movq	%rcx, -176(%rbp)
	call	SHA1_Final@PLT
	testl	%eax, %eax
	je	.L18
	movq	%r15, %rdi
	call	EVP_MD_CTX_md_data@PLT
	movq	%rax, %rdi
	movq	%rax, -184(%rbp)
	call	MD5_Init@PLT
	testl	%eax, %eax
	je	.L18
	movq	-184(%rbp), %rdi
	addq	$92, %rdi
	call	SHA1_Init@PLT
	testl	%eax, %eax
	je	.L18
	movq	%r15, %rdi
	call	EVP_MD_CTX_md_data@PLT
	movl	$48, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -184(%rbp)
	call	MD5_Update@PLT
	testl	%eax, %eax
	je	.L18
	movq	-184(%rbp), %rdi
	movl	$48, %edx
	movq	%r12, %rsi
	addq	$92, %rdi
	call	SHA1_Update@PLT
	testl	%eax, %eax
	jle	.L18
	movdqa	.LC1(%rip), %xmm0
	movl	$48, %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	MD5_Update@PLT
	testl	%eax, %eax
	je	.L18
	movq	-168(%rbp), %r15
	movl	$16, %edx
	movq	%r13, %rdi
	movq	%r15, %rsi
	call	MD5_Update@PLT
	testl	%eax, %eax
	je	.L18
	movl	$40, %edx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	SHA1_Update@PLT
	testl	%eax, %eax
	je	.L18
	movq	-176(%rbp), %r14
	movl	$20, %edx
	movq	%rbx, %rdi
	movq	%r14, %rsi
	call	SHA1_Update@PLT
	testl	%eax, %eax
	je	.L18
	movl	$16, %esi
	movq	%r15, %rdi
	call	OPENSSL_cleanse@PLT
	movl	$20, %esi
	movq	%r14, %rdi
	call	OPENSSL_cleanse@PLT
	movl	$1, %eax
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L20:
	movl	$-2, %eax
	jmp	.L14
.L60:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE808:
	.size	ctrl, .-ctrl
	.p2align 4
	.globl	EVP_md5_sha1
	.type	EVP_md5_sha1, @function
EVP_md5_sha1:
.LFB809:
	.cfi_startproc
	endbr64
	leaq	md5_sha1_md(%rip), %rax
	ret
	.cfi_endproc
.LFE809:
	.size	EVP_md5_sha1, .-EVP_md5_sha1
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	md5_sha1_md, @object
	.size	md5_sha1_md, 80
md5_sha1_md:
	.long	114
	.long	114
	.long	36
	.zero	4
	.quad	0
	.quad	init
	.quad	update
	.quad	final
	.quad	0
	.quad	0
	.long	64
	.long	196
	.quad	ctrl
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.quad	3906369333256140342
	.quad	3906369333256140342
	.align 16
.LC1:
	.quad	6655295901103053916
	.quad	6655295901103053916
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
