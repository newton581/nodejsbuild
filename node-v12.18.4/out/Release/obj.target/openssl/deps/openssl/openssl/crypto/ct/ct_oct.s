	.file	"ct_oct.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/ct/ct_oct.c"
	.text
	.p2align 4
	.globl	o2i_SCT_signature
	.type	o2i_SCT_signature, @function
o2i_SCT_signature:
.LFB1307:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rdi), %eax
	testl	%eax, %eax
	jne	.L11
	movq	%rdx, %r13
	cmpq	$4, %rdx
	jbe	.L12
	movq	(%rsi), %rbx
	movq	%rdi, %r14
	movq	%rsi, %r15
	movzbl	(%rbx), %eax
	movb	%al, 64(%rdi)
	movzbl	1(%rbx), %eax
	movb	%al, 65(%rdi)
	call	SCT_get_signature_nid@PLT
	testl	%eax, %eax
	je	.L13
	movzwl	2(%rbx), %r12d
	addq	$4, %rbx
	movq	%r13, %rcx
	movq	%rbx, %rax
	subq	(%r15), %rax
	rolw	$8, %r12w
	subq	%rax, %rcx
	movzwl	%r12w, %r12d
	cmpq	%rcx, %r12
	ja	.L14
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movq	%rcx, -56(%rbp)
	call	SCT_set1_signature@PLT
	cmpl	$1, %eax
	jne	.L7
	movq	-56(%rbp), %rcx
	addq	%r12, %rbx
	movl	%r13d, %eax
	movq	%rbx, (%r15)
	subq	%r12, %rcx
	subl	%ecx, %eax
.L1:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	.cfi_restore_state
	movl	$-1, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L12:
	movl	$42, %r8d
.L9:
	movl	$107, %edx
	movl	$112, %esi
	movl	$50, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-1, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L14:
	movl	$58, %r8d
	jmp	.L9
.L11:
	movl	$31, %r8d
	movl	$103, %edx
	movl	$112, %esi
	movl	$50, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-1, %eax
	jmp	.L1
.L13:
	movl	$51, %r8d
	jmp	.L9
	.cfi_endproc
.LFE1307:
	.size	o2i_SCT_signature, .-o2i_SCT_signature
	.p2align 4
	.globl	o2i_SCT
	.type	o2i_SCT, @function
o2i_SCT:
.LFB1308:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-1(%rdx), %rax
	cmpq	$65534, %rax
	ja	.L46
	movq	%rdi, %r14
	movq	%rsi, %r13
	movq	%rdx, %rbx
	call	SCT_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L17
	movq	0(%r13), %rdi
	movq	%rdi, -64(%rbp)
	movzbl	(%rdi), %eax
	movl	%eax, (%r12)
	testl	%eax, %eax
	jne	.L18
	movl	$99, %r8d
	cmpq	$42, %rbx
	jbe	.L45
	addq	$1, %rdi
	movl	$104, %ecx
	leaq	.LC0(%rip), %rdx
	movl	$32, %esi
	movq	%rdi, -64(%rbp)
	subq	$43, %rbx
	call	CRYPTO_memdup@PLT
	movq	%rax, 24(%r12)
	testq	%rax, %rax
	je	.L17
	movq	$32, 32(%r12)
	movq	-64(%rbp), %rdi
	movzbl	32(%rdi), %edx
	addq	$42, %rdi
	salq	$56, %rdx
	movq	%rdx, 40(%r12)
	movzbl	-9(%rdi), %eax
	salq	$48, %rax
	orq	%rax, %rdx
	movq	%rdx, 40(%r12)
	movzbl	-8(%rdi), %eax
	salq	$40, %rax
	orq	%rdx, %rax
	movq	%rax, 40(%r12)
	movzbl	-7(%rdi), %edx
	salq	$32, %rdx
	orq	%rdx, %rax
	movq	%rax, 40(%r12)
	movzbl	-6(%rdi), %edx
	salq	$24, %rdx
	orq	%rax, %rdx
	movq	%rdx, 40(%r12)
	movzbl	-5(%rdi), %eax
	salq	$16, %rax
	orq	%rax, %rdx
	movq	%rdx, 40(%r12)
	movzbl	-4(%rdi), %eax
	salq	$8, %rax
	orq	%rdx, %rax
	movq	%rax, 40(%r12)
	movzbl	-3(%rdi), %edx
	orq	%rdx, %rax
	movq	%rax, 40(%r12)
	movzwl	-2(%rdi), %r15d
	movq	%rdi, -64(%rbp)
	rolw	$8, %r15w
	movzwl	%r15w, %r15d
	cmpq	%r15, %rbx
	jb	.L47
	testq	%r15, %r15
	jne	.L48
.L21:
	movq	%r15, 56(%r12)
	addq	%r15, %rdi
	subq	%r15, %rbx
	leaq	-64(%rbp), %rsi
	movq	%rdi, -64(%rbp)
	movq	%rbx, %rdx
	movq	%r12, %rdi
	call	o2i_SCT_signature
	movl	$128, %r8d
	testl	%eax, %eax
	jle	.L45
	cltq
	subq	%rax, %rbx
	addq	-64(%rbp), %rbx
	movq	%rbx, 0(%r13)
.L23:
	testq	%r14, %r14
	je	.L15
	movq	(%r14), %rdi
	call	SCT_free@PLT
	movq	%r12, (%r14)
.L15:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L49
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L46:
	.cfi_restore_state
	movl	$76, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$104, %edx
	xorl	%r12d, %r12d
	movl	$110, %esi
	movl	$50, %edi
	call	ERR_put_error@PLT
.L17:
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	SCT_free@PLT
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L18:
	movl	$135, %ecx
	leaq	.LC0(%rip), %rdx
	movq	%rbx, %rsi
	call	CRYPTO_memdup@PLT
	movq	%rax, 8(%r12)
	testq	%rax, %rax
	je	.L17
	movq	%rbx, 16(%r12)
	addq	-64(%rbp), %rbx
	movq	%rbx, 0(%r13)
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L48:
	movl	$118, %ecx
	leaq	.LC0(%rip), %rdx
	movq	%r15, %rsi
	call	CRYPTO_memdup@PLT
	movq	%rax, 48(%r12)
	testq	%rax, %rax
	je	.L17
	movq	-64(%rbp), %rdi
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L47:
	movl	$114, %r8d
.L45:
	movl	$104, %edx
	movl	$110, %esi
	movl	$50, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	SCT_free@PLT
	jmp	.L15
.L49:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1308:
	.size	o2i_SCT, .-o2i_SCT
	.p2align 4
	.globl	i2o_SCT_signature
	.type	i2o_SCT_signature, @function
i2o_SCT_signature:
.LFB1309:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	SCT_signature_is_complete@PLT
	testl	%eax, %eax
	je	.L63
	movl	(%rbx), %eax
	testl	%eax, %eax
	jne	.L64
	movq	80(%rbx), %rax
	leaq	4(%rax), %r12
	testq	%r13, %r13
	je	.L54
	movq	0(%r13), %rax
	testq	%rax, %rax
	je	.L55
	leaq	(%rax,%r12), %rdx
	movq	%rdx, 0(%r13)
.L56:
	movzbl	64(%rbx), %edx
	leaq	4(%rax), %rdi
	movb	%dl, (%rax)
	movzbl	65(%rbx), %edx
	movb	%dl, 1(%rax)
	movq	80(%rbx), %rdx
	shrq	$8, %rdx
	movb	%dl, 2(%rax)
	movq	80(%rbx), %rdx
	movb	%dl, 3(%rax)
	movq	72(%rbx), %rsi
	movq	80(%rbx), %rdx
	call	memcpy@PLT
.L54:
	movl	%r12d, %eax
.L50:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L55:
	.cfi_restore_state
	movl	$180, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_malloc@PLT
	testq	%rax, %rax
	je	.L65
	movq	%rax, 0(%r13)
	jmp	.L56
.L63:
	movl	$159, %r8d
	movl	$107, %edx
	movl	$109, %esi
	movl	$50, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L52:
	movl	$196, %edx
	leaq	.LC0(%rip), %rsi
	xorl	%edi, %edi
	call	CRYPTO_free@PLT
	movl	$-1, %eax
	jmp	.L50
.L64:
	movl	$164, %r8d
	movl	$103, %edx
	movl	$109, %esi
	movl	$50, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L52
.L65:
	movl	$182, %r8d
	movl	$65, %edx
	movl	$109, %esi
	movl	$50, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L52
	.cfi_endproc
.LFE1309:
	.size	i2o_SCT_signature, .-i2o_SCT_signature
	.p2align 4
	.globl	i2o_SCT
	.type	i2o_SCT, @function
i2o_SCT:
.LFB1310:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -48(%rbp)
	call	SCT_is_complete@PLT
	testl	%eax, %eax
	je	.L84
	movl	(%r12), %edx
	testl	%edx, %edx
	je	.L85
	movq	16(%r12), %rbx
.L70:
	testq	%r13, %r13
	je	.L78
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L73
	leaq	(%rdi,%rbx), %rax
	movq	%rdi, -48(%rbp)
	xorl	%r14d, %r14d
	movq	%rax, 0(%r13)
.L74:
	testl	%edx, %edx
	jne	.L76
	leaq	1(%rdi), %rax
	movq	%rax, -48(%rbp)
	movb	$0, (%rdi)
	movq	24(%r12), %rdx
	movq	-48(%rbp), %rax
	movdqu	(%rdx), %xmm0
	movups	%xmm0, (%rax)
	movdqu	16(%rdx), %xmm1
	movups	%xmm1, 16(%rax)
	movq	-48(%rbp), %rax
	movzbl	47(%r12), %edx
	movb	%dl, 32(%rax)
	movzwl	46(%r12), %edx
	leaq	42(%rax), %rcx
	movq	%rcx, -48(%rbp)
	movb	%dl, 33(%rax)
	movq	40(%r12), %rdx
	shrq	$40, %rdx
	movb	%dl, 34(%rax)
	movl	44(%r12), %edx
	movb	%dl, 35(%rax)
	movq	40(%r12), %rdx
	shrq	$24, %rdx
	movb	%dl, 36(%rax)
	movq	40(%r12), %rdx
	shrq	$16, %rdx
	movb	%dl, 37(%rax)
	movq	40(%r12), %rdx
	shrq	$8, %rdx
	movb	%dl, 38(%rax)
	movq	40(%r12), %rdx
	movb	%dl, 39(%rax)
	movq	56(%r12), %rdx
	shrq	$8, %rdx
	movb	%dl, 40(%rax)
	movq	56(%r12), %rdx
	movb	%dl, 41(%rax)
	movq	56(%r12), %rdx
	testq	%rdx, %rdx
	jne	.L86
.L77:
	leaq	-48(%rbp), %rsi
	movq	%r12, %rdi
	call	i2o_SCT_signature
	testl	%eax, %eax
	jle	.L68
.L78:
	movl	%ebx, %eax
.L66:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L87
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L85:
	.cfi_restore_state
	movq	80(%r12), %rbx
	addq	56(%r12), %rbx
	addq	$47, %rbx
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L76:
	movq	8(%r12), %rsi
	movq	%rbx, %rdx
	call	memcpy@PLT
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L73:
	movl	$227, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rbx, %rdi
	call	CRYPTO_malloc@PLT
	movq	%rax, -48(%rbp)
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L88
	movq	%rax, 0(%r13)
	movl	(%r12), %edx
	movq	%rax, %rdi
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L86:
	movq	48(%r12), %rsi
	movq	%rcx, %rdi
	call	memcpy@PLT
	movq	%rax, %rcx
	movq	56(%r12), %rax
	addq	%rcx, %rax
	movq	%rax, -48(%rbp)
	jmp	.L77
.L88:
	movl	$229, %r8d
	movl	$65, %edx
	movl	$107, %esi
	movl	$50, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L68:
	movl	$253, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r14, %rdi
	call	CRYPTO_free@PLT
	movl	$-1, %eax
	jmp	.L66
.L84:
	movl	$206, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$106, %edx
	xorl	%r14d, %r14d
	movl	$107, %esi
	movl	$50, %edi
	call	ERR_put_error@PLT
	jmp	.L68
.L87:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1310:
	.size	i2o_SCT, .-i2o_SCT
	.p2align 4
	.globl	o2i_SCT_LIST
	.type	o2i_SCT_LIST, @function
o2i_SCT_LIST:
.LFB1311:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	subq	$2, %rdx
	movl	$264, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	cmpq	$65533, %rdx
	ja	.L123
	movq	(%rsi), %rax
	movq	%rsi, %r12
	movzwl	(%rax), %ebx
	addq	$2, %rax
	movq	%rax, (%rsi)
	rolw	$8, %bx
	movzwl	%bx, %ebx
	cmpq	%rbx, %rdx
	jne	.L124
	movq	%rdi, %r14
	testq	%rdi, %rdi
	je	.L93
	movq	(%rdi), %r13
	testq	%r13, %r13
	jne	.L94
.L93:
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	jne	.L101
	.p2align 4,,10
	.p2align 3
.L122:
	xorl	%r13d, %r13d
.L89:
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L96:
	.cfi_restore_state
	call	SCT_free@PLT
.L94:
	movq	%r13, %rdi
	call	OPENSSL_sk_pop@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L96
	testq	%rbx, %rbx
	je	.L125
	.p2align 4,,10
	.p2align 3
.L102:
	cmpq	$1, %rbx
	jbe	.L126
	movq	(%r12), %rax
	subq	$2, %rbx
	movzwl	(%rax), %edx
	addq	$2, %rax
	movq	%rax, (%r12)
	rolw	$8, %dx
	movzwl	%dx, %edx
	testq	%rdx, %rdx
	je	.L105
	cmpq	%rbx, %rdx
	ja	.L105
	movq	%r12, %rsi
	xorl	%edi, %edi
	subq	%rdx, %rbx
	call	o2i_SCT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L98
	movq	%rax, %rsi
	movq	%r13, %rdi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L127
.L101:
	testq	%rbx, %rbx
	jne	.L102
.L125:
	testq	%r14, %r14
	je	.L89
	cmpq	$0, (%r14)
	jne	.L89
	movq	%r13, (%r14)
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L126:
	movl	$291, %r8d
.L121:
	movl	$105, %edx
	movl	$111, %esi
	movl	$50, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L98:
	testq	%r14, %r14
	je	.L103
	cmpq	$0, (%r14)
	jne	.L122
.L103:
	movq	%r13, %rdi
	call	SCT_LIST_free@PLT
	jmp	.L122
	.p2align 4,,10
	.p2align 3
.L124:
	movl	$270, %r8d
.L123:
	leaq	.LC0(%rip), %rcx
	movl	$105, %edx
	movl	$111, %esi
	xorl	%r13d, %r13d
	movl	$50, %edi
	call	ERR_put_error@PLT
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L105:
	movl	$298, %r8d
	jmp	.L121
	.p2align 4,,10
	.p2align 3
.L127:
	movq	%r15, %rdi
	call	SCT_free@PLT
	jmp	.L98
	.cfi_endproc
.LFE1311:
	.size	o2i_SCT_LIST, .-o2i_SCT_LIST
	.p2align 4
	.globl	i2o_SCT_LIST
	.type	i2o_SCT_LIST, @function
i2o_SCT_LIST:
.LFB1312:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -80(%rbp)
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	movq	$0, -64(%rbp)
	testq	%rsi, %rsi
	je	.L146
	movq	(%rsi), %rax
	movl	$0, -68(%rbp)
	testq	%rax, %rax
	je	.L158
.L130:
	addq	$2, %rax
	movl	$2, %ebx
	leaq	-64(%rbp), %r13
	xorl	%r15d, %r15d
	movq	%rax, -64(%rbp)
	jmp	.L133
	.p2align 4,,10
	.p2align 3
.L140:
	movq	-64(%rbp), %r14
	movl	%r15d, %esi
	movq	%r12, %rdi
	leaq	2(%r14), %rax
	movq	%rax, -64(%rbp)
	call	OPENSSL_sk_value@PLT
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	i2o_SCT
	cmpl	$-1, %eax
	je	.L139
	movl	%eax, %esi
	addl	$2, %eax
	addl	$1, %r15d
	rolw	$8, %si
	cltq
	movw	%si, (%r14)
	addq	%rax, %rbx
.L133:
	movq	%r12, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r15d
	jl	.L140
	cmpq	$65535, %rbx
	ja	.L139
	movq	-80(%rbp), %rcx
	leaq	-2(%rbx), %rax
	movl	-68(%rbp), %esi
	shrq	$8, %rax
	movq	(%rcx), %rdx
	movq	%rdx, -64(%rbp)
	movb	%al, (%rdx)
	movq	-64(%rbp), %rax
	leal	-2(%rbx), %edx
	movb	%dl, 1(%rax)
	testl	%esi, %esi
	jne	.L142
	addq	%rbx, (%rcx)
.L142:
	movl	%ebx, %eax
.L128:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L159
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L139:
	.cfi_restore_state
	movl	-68(%rbp), %edx
	movl	$-1, %eax
	testl	%edx, %edx
	je	.L128
	movq	-80(%rbp), %rbx
	movl	$370, %edx
	leaq	.LC0(%rip), %rsi
	movl	%eax, -68(%rbp)
	movq	(%rbx), %rdi
	call	CRYPTO_free@PLT
	movq	$0, (%rbx)
	movl	-68(%rbp), %eax
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L146:
	movl	$2, %ebx
	xorl	%r13d, %r13d
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L160:
	movq	16(%r14), %rdx
	movl	%edx, %eax
	cmpl	$-1, %edx
	je	.L137
.L138:
	addl	$2, %eax
	addl	$1, %r13d
	cltq
	addq	%rax, %rbx
.L129:
	movq	%r12, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%r13d, %eax
	jle	.L134
	movq	%r12, %rdi
	movl	%r13d, %esi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %rdi
	movq	%rax, %r14
	call	SCT_is_complete@PLT
	testl	%eax, %eax
	je	.L135
	movl	(%r14), %edi
	testl	%edi, %edi
	jne	.L160
	movq	56(%r14), %rdx
	addq	80(%r14), %rdx
	addq	$47, %rdx
	movl	%edx, %eax
	cmpl	$-1, %edx
	jne	.L138
.L137:
	movl	$-1, %eax
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L134:
	cmpq	$65535, %rbx
	ja	.L137
	movl	%ebx, %eax
	jmp	.L128
.L158:
	xorl	%esi, %esi
	call	i2o_SCT_LIST
	cmpl	$-1, %eax
	je	.L161
	movslq	%eax, %rdi
	movl	$333, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_malloc@PLT
	movq	-80(%rbp), %rcx
	movq	%rax, (%rcx)
	testq	%rax, %rax
	je	.L162
	movl	$1, -68(%rbp)
	jmp	.L130
.L135:
	movl	$206, %r8d
	movl	$106, %edx
	movl	$107, %esi
	movl	$50, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$253, %edx
	leaq	.LC0(%rip), %rsi
	xorl	%edi, %edi
	call	CRYPTO_free@PLT
	movl	$-1, %eax
	jmp	.L128
.L162:
	movl	$334, %r8d
	movl	$65, %edx
	movl	$108, %esi
	movl	$50, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	orl	$-1, %eax
	jmp	.L128
.L161:
	movl	$330, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$105, %edx
	movl	%eax, -68(%rbp)
	movl	$108, %esi
	movl	$50, %edi
	call	ERR_put_error@PLT
	movl	-68(%rbp), %eax
	jmp	.L128
.L159:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1312:
	.size	i2o_SCT_LIST, .-i2o_SCT_LIST
	.p2align 4
	.globl	d2i_SCT_LIST
	.type	d2i_SCT_LIST, @function
d2i_SCT_LIST:
.LFB1313:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-64(%rbp), %r15
	movq	%rdi, %r14
	leaq	-72(%rbp), %rdi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	%r15, %rsi
	movq	$0, -72(%rbp)
	movq	%rax, -64(%rbp)
	call	d2i_ASN1_OCTET_STRING@PLT
	testq	%rax, %rax
	je	.L173
	movq	-72(%rbp), %rax
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	8(%rax), %rdx
	movq	%rdx, -64(%rbp)
	movslq	(%rax), %rdx
	call	o2i_SCT_LIST
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L165
	addq	%r13, (%rbx)
.L165:
	movq	-72(%rbp), %rdi
	call	ASN1_OCTET_STRING_free@PLT
.L163:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L175
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L173:
	.cfi_restore_state
	movq	%rax, %r12
	jmp	.L163
.L175:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1313:
	.size	d2i_SCT_LIST, .-d2i_SCT_LIST
	.p2align 4
	.globl	i2d_SCT_LIST
	.type	i2d_SCT_LIST, @function
i2d_SCT_LIST:
.LFB1314:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	leaq	-56(%rbp), %rsi
	pushq	%r12
	subq	$40, %rsp
	.cfi_offset 12, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -56(%rbp)
	call	i2o_SCT_LIST
	movl	%eax, -64(%rbp)
	cmpl	$-1, %eax
	je	.L182
	leaq	-64(%rbp), %r14
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	i2d_ASN1_OCTET_STRING@PLT
	movq	-56(%rbp), %rdi
	movl	$405, %edx
	leaq	.LC0(%rip), %rsi
	movl	%eax, %r12d
	call	CRYPTO_free@PLT
.L176:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L184
	addq	$40, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L182:
	.cfi_restore_state
	movl	%eax, %r12d
	jmp	.L176
.L184:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1314:
	.size	i2d_SCT_LIST, .-i2d_SCT_LIST
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
