	.file	"ocsp_asn.c"
	.text
	.p2align 4
	.globl	d2i_OCSP_SIGNATURE
	.type	d2i_OCSP_SIGNATURE, @function
d2i_OCSP_SIGNATURE:
.LFB1355:
	.cfi_startproc
	endbr64
	leaq	OCSP_SIGNATURE_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE1355:
	.size	d2i_OCSP_SIGNATURE, .-d2i_OCSP_SIGNATURE
	.p2align 4
	.globl	i2d_OCSP_SIGNATURE
	.type	i2d_OCSP_SIGNATURE, @function
i2d_OCSP_SIGNATURE:
.LFB1356:
	.cfi_startproc
	endbr64
	leaq	OCSP_SIGNATURE_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE1356:
	.size	i2d_OCSP_SIGNATURE, .-i2d_OCSP_SIGNATURE
	.p2align 4
	.globl	OCSP_SIGNATURE_new
	.type	OCSP_SIGNATURE_new, @function
OCSP_SIGNATURE_new:
.LFB1357:
	.cfi_startproc
	endbr64
	leaq	OCSP_SIGNATURE_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE1357:
	.size	OCSP_SIGNATURE_new, .-OCSP_SIGNATURE_new
	.p2align 4
	.globl	OCSP_SIGNATURE_free
	.type	OCSP_SIGNATURE_free, @function
OCSP_SIGNATURE_free:
.LFB1358:
	.cfi_startproc
	endbr64
	leaq	OCSP_SIGNATURE_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE1358:
	.size	OCSP_SIGNATURE_free, .-OCSP_SIGNATURE_free
	.p2align 4
	.globl	d2i_OCSP_CERTID
	.type	d2i_OCSP_CERTID, @function
d2i_OCSP_CERTID:
.LFB1359:
	.cfi_startproc
	endbr64
	leaq	OCSP_CERTID_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE1359:
	.size	d2i_OCSP_CERTID, .-d2i_OCSP_CERTID
	.p2align 4
	.globl	i2d_OCSP_CERTID
	.type	i2d_OCSP_CERTID, @function
i2d_OCSP_CERTID:
.LFB1360:
	.cfi_startproc
	endbr64
	leaq	OCSP_CERTID_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE1360:
	.size	i2d_OCSP_CERTID, .-i2d_OCSP_CERTID
	.p2align 4
	.globl	OCSP_CERTID_new
	.type	OCSP_CERTID_new, @function
OCSP_CERTID_new:
.LFB1361:
	.cfi_startproc
	endbr64
	leaq	OCSP_CERTID_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE1361:
	.size	OCSP_CERTID_new, .-OCSP_CERTID_new
	.p2align 4
	.globl	OCSP_CERTID_free
	.type	OCSP_CERTID_free, @function
OCSP_CERTID_free:
.LFB1362:
	.cfi_startproc
	endbr64
	leaq	OCSP_CERTID_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE1362:
	.size	OCSP_CERTID_free, .-OCSP_CERTID_free
	.p2align 4
	.globl	d2i_OCSP_ONEREQ
	.type	d2i_OCSP_ONEREQ, @function
d2i_OCSP_ONEREQ:
.LFB1363:
	.cfi_startproc
	endbr64
	leaq	OCSP_ONEREQ_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE1363:
	.size	d2i_OCSP_ONEREQ, .-d2i_OCSP_ONEREQ
	.p2align 4
	.globl	i2d_OCSP_ONEREQ
	.type	i2d_OCSP_ONEREQ, @function
i2d_OCSP_ONEREQ:
.LFB1364:
	.cfi_startproc
	endbr64
	leaq	OCSP_ONEREQ_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE1364:
	.size	i2d_OCSP_ONEREQ, .-i2d_OCSP_ONEREQ
	.p2align 4
	.globl	OCSP_ONEREQ_new
	.type	OCSP_ONEREQ_new, @function
OCSP_ONEREQ_new:
.LFB1365:
	.cfi_startproc
	endbr64
	leaq	OCSP_ONEREQ_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE1365:
	.size	OCSP_ONEREQ_new, .-OCSP_ONEREQ_new
	.p2align 4
	.globl	OCSP_ONEREQ_free
	.type	OCSP_ONEREQ_free, @function
OCSP_ONEREQ_free:
.LFB1366:
	.cfi_startproc
	endbr64
	leaq	OCSP_ONEREQ_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE1366:
	.size	OCSP_ONEREQ_free, .-OCSP_ONEREQ_free
	.p2align 4
	.globl	d2i_OCSP_REQINFO
	.type	d2i_OCSP_REQINFO, @function
d2i_OCSP_REQINFO:
.LFB1367:
	.cfi_startproc
	endbr64
	leaq	OCSP_REQINFO_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE1367:
	.size	d2i_OCSP_REQINFO, .-d2i_OCSP_REQINFO
	.p2align 4
	.globl	i2d_OCSP_REQINFO
	.type	i2d_OCSP_REQINFO, @function
i2d_OCSP_REQINFO:
.LFB1368:
	.cfi_startproc
	endbr64
	leaq	OCSP_REQINFO_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE1368:
	.size	i2d_OCSP_REQINFO, .-i2d_OCSP_REQINFO
	.p2align 4
	.globl	OCSP_REQINFO_new
	.type	OCSP_REQINFO_new, @function
OCSP_REQINFO_new:
.LFB1369:
	.cfi_startproc
	endbr64
	leaq	OCSP_REQINFO_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE1369:
	.size	OCSP_REQINFO_new, .-OCSP_REQINFO_new
	.p2align 4
	.globl	OCSP_REQINFO_free
	.type	OCSP_REQINFO_free, @function
OCSP_REQINFO_free:
.LFB1370:
	.cfi_startproc
	endbr64
	leaq	OCSP_REQINFO_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE1370:
	.size	OCSP_REQINFO_free, .-OCSP_REQINFO_free
	.p2align 4
	.globl	d2i_OCSP_REQUEST
	.type	d2i_OCSP_REQUEST, @function
d2i_OCSP_REQUEST:
.LFB1371:
	.cfi_startproc
	endbr64
	leaq	OCSP_REQUEST_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE1371:
	.size	d2i_OCSP_REQUEST, .-d2i_OCSP_REQUEST
	.p2align 4
	.globl	i2d_OCSP_REQUEST
	.type	i2d_OCSP_REQUEST, @function
i2d_OCSP_REQUEST:
.LFB1372:
	.cfi_startproc
	endbr64
	leaq	OCSP_REQUEST_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE1372:
	.size	i2d_OCSP_REQUEST, .-i2d_OCSP_REQUEST
	.p2align 4
	.globl	OCSP_REQUEST_new
	.type	OCSP_REQUEST_new, @function
OCSP_REQUEST_new:
.LFB1373:
	.cfi_startproc
	endbr64
	leaq	OCSP_REQUEST_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE1373:
	.size	OCSP_REQUEST_new, .-OCSP_REQUEST_new
	.p2align 4
	.globl	OCSP_REQUEST_free
	.type	OCSP_REQUEST_free, @function
OCSP_REQUEST_free:
.LFB1374:
	.cfi_startproc
	endbr64
	leaq	OCSP_REQUEST_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE1374:
	.size	OCSP_REQUEST_free, .-OCSP_REQUEST_free
	.p2align 4
	.globl	d2i_OCSP_RESPBYTES
	.type	d2i_OCSP_RESPBYTES, @function
d2i_OCSP_RESPBYTES:
.LFB1375:
	.cfi_startproc
	endbr64
	leaq	OCSP_RESPBYTES_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE1375:
	.size	d2i_OCSP_RESPBYTES, .-d2i_OCSP_RESPBYTES
	.p2align 4
	.globl	i2d_OCSP_RESPBYTES
	.type	i2d_OCSP_RESPBYTES, @function
i2d_OCSP_RESPBYTES:
.LFB1376:
	.cfi_startproc
	endbr64
	leaq	OCSP_RESPBYTES_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE1376:
	.size	i2d_OCSP_RESPBYTES, .-i2d_OCSP_RESPBYTES
	.p2align 4
	.globl	OCSP_RESPBYTES_new
	.type	OCSP_RESPBYTES_new, @function
OCSP_RESPBYTES_new:
.LFB1377:
	.cfi_startproc
	endbr64
	leaq	OCSP_RESPBYTES_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE1377:
	.size	OCSP_RESPBYTES_new, .-OCSP_RESPBYTES_new
	.p2align 4
	.globl	OCSP_RESPBYTES_free
	.type	OCSP_RESPBYTES_free, @function
OCSP_RESPBYTES_free:
.LFB1378:
	.cfi_startproc
	endbr64
	leaq	OCSP_RESPBYTES_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE1378:
	.size	OCSP_RESPBYTES_free, .-OCSP_RESPBYTES_free
	.p2align 4
	.globl	d2i_OCSP_RESPONSE
	.type	d2i_OCSP_RESPONSE, @function
d2i_OCSP_RESPONSE:
.LFB1379:
	.cfi_startproc
	endbr64
	leaq	OCSP_RESPONSE_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE1379:
	.size	d2i_OCSP_RESPONSE, .-d2i_OCSP_RESPONSE
	.p2align 4
	.globl	i2d_OCSP_RESPONSE
	.type	i2d_OCSP_RESPONSE, @function
i2d_OCSP_RESPONSE:
.LFB1380:
	.cfi_startproc
	endbr64
	leaq	OCSP_RESPONSE_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE1380:
	.size	i2d_OCSP_RESPONSE, .-i2d_OCSP_RESPONSE
	.p2align 4
	.globl	OCSP_RESPONSE_new
	.type	OCSP_RESPONSE_new, @function
OCSP_RESPONSE_new:
.LFB1381:
	.cfi_startproc
	endbr64
	leaq	OCSP_RESPONSE_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE1381:
	.size	OCSP_RESPONSE_new, .-OCSP_RESPONSE_new
	.p2align 4
	.globl	OCSP_RESPONSE_free
	.type	OCSP_RESPONSE_free, @function
OCSP_RESPONSE_free:
.LFB1382:
	.cfi_startproc
	endbr64
	leaq	OCSP_RESPONSE_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE1382:
	.size	OCSP_RESPONSE_free, .-OCSP_RESPONSE_free
	.p2align 4
	.globl	d2i_OCSP_RESPID
	.type	d2i_OCSP_RESPID, @function
d2i_OCSP_RESPID:
.LFB1383:
	.cfi_startproc
	endbr64
	leaq	OCSP_RESPID_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE1383:
	.size	d2i_OCSP_RESPID, .-d2i_OCSP_RESPID
	.p2align 4
	.globl	i2d_OCSP_RESPID
	.type	i2d_OCSP_RESPID, @function
i2d_OCSP_RESPID:
.LFB1384:
	.cfi_startproc
	endbr64
	leaq	OCSP_RESPID_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE1384:
	.size	i2d_OCSP_RESPID, .-i2d_OCSP_RESPID
	.p2align 4
	.globl	OCSP_RESPID_new
	.type	OCSP_RESPID_new, @function
OCSP_RESPID_new:
.LFB1385:
	.cfi_startproc
	endbr64
	leaq	OCSP_RESPID_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE1385:
	.size	OCSP_RESPID_new, .-OCSP_RESPID_new
	.p2align 4
	.globl	OCSP_RESPID_free
	.type	OCSP_RESPID_free, @function
OCSP_RESPID_free:
.LFB1386:
	.cfi_startproc
	endbr64
	leaq	OCSP_RESPID_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE1386:
	.size	OCSP_RESPID_free, .-OCSP_RESPID_free
	.p2align 4
	.globl	d2i_OCSP_REVOKEDINFO
	.type	d2i_OCSP_REVOKEDINFO, @function
d2i_OCSP_REVOKEDINFO:
.LFB1387:
	.cfi_startproc
	endbr64
	leaq	OCSP_REVOKEDINFO_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE1387:
	.size	d2i_OCSP_REVOKEDINFO, .-d2i_OCSP_REVOKEDINFO
	.p2align 4
	.globl	i2d_OCSP_REVOKEDINFO
	.type	i2d_OCSP_REVOKEDINFO, @function
i2d_OCSP_REVOKEDINFO:
.LFB1388:
	.cfi_startproc
	endbr64
	leaq	OCSP_REVOKEDINFO_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE1388:
	.size	i2d_OCSP_REVOKEDINFO, .-i2d_OCSP_REVOKEDINFO
	.p2align 4
	.globl	OCSP_REVOKEDINFO_new
	.type	OCSP_REVOKEDINFO_new, @function
OCSP_REVOKEDINFO_new:
.LFB1389:
	.cfi_startproc
	endbr64
	leaq	OCSP_REVOKEDINFO_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE1389:
	.size	OCSP_REVOKEDINFO_new, .-OCSP_REVOKEDINFO_new
	.p2align 4
	.globl	OCSP_REVOKEDINFO_free
	.type	OCSP_REVOKEDINFO_free, @function
OCSP_REVOKEDINFO_free:
.LFB1390:
	.cfi_startproc
	endbr64
	leaq	OCSP_REVOKEDINFO_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE1390:
	.size	OCSP_REVOKEDINFO_free, .-OCSP_REVOKEDINFO_free
	.p2align 4
	.globl	d2i_OCSP_CERTSTATUS
	.type	d2i_OCSP_CERTSTATUS, @function
d2i_OCSP_CERTSTATUS:
.LFB1391:
	.cfi_startproc
	endbr64
	leaq	OCSP_CERTSTATUS_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE1391:
	.size	d2i_OCSP_CERTSTATUS, .-d2i_OCSP_CERTSTATUS
	.p2align 4
	.globl	i2d_OCSP_CERTSTATUS
	.type	i2d_OCSP_CERTSTATUS, @function
i2d_OCSP_CERTSTATUS:
.LFB1392:
	.cfi_startproc
	endbr64
	leaq	OCSP_CERTSTATUS_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE1392:
	.size	i2d_OCSP_CERTSTATUS, .-i2d_OCSP_CERTSTATUS
	.p2align 4
	.globl	OCSP_CERTSTATUS_new
	.type	OCSP_CERTSTATUS_new, @function
OCSP_CERTSTATUS_new:
.LFB1393:
	.cfi_startproc
	endbr64
	leaq	OCSP_CERTSTATUS_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE1393:
	.size	OCSP_CERTSTATUS_new, .-OCSP_CERTSTATUS_new
	.p2align 4
	.globl	OCSP_CERTSTATUS_free
	.type	OCSP_CERTSTATUS_free, @function
OCSP_CERTSTATUS_free:
.LFB1394:
	.cfi_startproc
	endbr64
	leaq	OCSP_CERTSTATUS_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE1394:
	.size	OCSP_CERTSTATUS_free, .-OCSP_CERTSTATUS_free
	.p2align 4
	.globl	d2i_OCSP_SINGLERESP
	.type	d2i_OCSP_SINGLERESP, @function
d2i_OCSP_SINGLERESP:
.LFB1395:
	.cfi_startproc
	endbr64
	leaq	OCSP_SINGLERESP_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE1395:
	.size	d2i_OCSP_SINGLERESP, .-d2i_OCSP_SINGLERESP
	.p2align 4
	.globl	i2d_OCSP_SINGLERESP
	.type	i2d_OCSP_SINGLERESP, @function
i2d_OCSP_SINGLERESP:
.LFB1396:
	.cfi_startproc
	endbr64
	leaq	OCSP_SINGLERESP_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE1396:
	.size	i2d_OCSP_SINGLERESP, .-i2d_OCSP_SINGLERESP
	.p2align 4
	.globl	OCSP_SINGLERESP_new
	.type	OCSP_SINGLERESP_new, @function
OCSP_SINGLERESP_new:
.LFB1397:
	.cfi_startproc
	endbr64
	leaq	OCSP_SINGLERESP_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE1397:
	.size	OCSP_SINGLERESP_new, .-OCSP_SINGLERESP_new
	.p2align 4
	.globl	OCSP_SINGLERESP_free
	.type	OCSP_SINGLERESP_free, @function
OCSP_SINGLERESP_free:
.LFB1398:
	.cfi_startproc
	endbr64
	leaq	OCSP_SINGLERESP_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE1398:
	.size	OCSP_SINGLERESP_free, .-OCSP_SINGLERESP_free
	.p2align 4
	.globl	d2i_OCSP_RESPDATA
	.type	d2i_OCSP_RESPDATA, @function
d2i_OCSP_RESPDATA:
.LFB1399:
	.cfi_startproc
	endbr64
	leaq	OCSP_RESPDATA_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE1399:
	.size	d2i_OCSP_RESPDATA, .-d2i_OCSP_RESPDATA
	.p2align 4
	.globl	i2d_OCSP_RESPDATA
	.type	i2d_OCSP_RESPDATA, @function
i2d_OCSP_RESPDATA:
.LFB1400:
	.cfi_startproc
	endbr64
	leaq	OCSP_RESPDATA_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE1400:
	.size	i2d_OCSP_RESPDATA, .-i2d_OCSP_RESPDATA
	.p2align 4
	.globl	OCSP_RESPDATA_new
	.type	OCSP_RESPDATA_new, @function
OCSP_RESPDATA_new:
.LFB1401:
	.cfi_startproc
	endbr64
	leaq	OCSP_RESPDATA_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE1401:
	.size	OCSP_RESPDATA_new, .-OCSP_RESPDATA_new
	.p2align 4
	.globl	OCSP_RESPDATA_free
	.type	OCSP_RESPDATA_free, @function
OCSP_RESPDATA_free:
.LFB1402:
	.cfi_startproc
	endbr64
	leaq	OCSP_RESPDATA_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE1402:
	.size	OCSP_RESPDATA_free, .-OCSP_RESPDATA_free
	.p2align 4
	.globl	d2i_OCSP_BASICRESP
	.type	d2i_OCSP_BASICRESP, @function
d2i_OCSP_BASICRESP:
.LFB1403:
	.cfi_startproc
	endbr64
	leaq	OCSP_BASICRESP_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE1403:
	.size	d2i_OCSP_BASICRESP, .-d2i_OCSP_BASICRESP
	.p2align 4
	.globl	i2d_OCSP_BASICRESP
	.type	i2d_OCSP_BASICRESP, @function
i2d_OCSP_BASICRESP:
.LFB1404:
	.cfi_startproc
	endbr64
	leaq	OCSP_BASICRESP_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE1404:
	.size	i2d_OCSP_BASICRESP, .-i2d_OCSP_BASICRESP
	.p2align 4
	.globl	OCSP_BASICRESP_new
	.type	OCSP_BASICRESP_new, @function
OCSP_BASICRESP_new:
.LFB1405:
	.cfi_startproc
	endbr64
	leaq	OCSP_BASICRESP_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE1405:
	.size	OCSP_BASICRESP_new, .-OCSP_BASICRESP_new
	.p2align 4
	.globl	OCSP_BASICRESP_free
	.type	OCSP_BASICRESP_free, @function
OCSP_BASICRESP_free:
.LFB1406:
	.cfi_startproc
	endbr64
	leaq	OCSP_BASICRESP_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE1406:
	.size	OCSP_BASICRESP_free, .-OCSP_BASICRESP_free
	.p2align 4
	.globl	d2i_OCSP_CRLID
	.type	d2i_OCSP_CRLID, @function
d2i_OCSP_CRLID:
.LFB1407:
	.cfi_startproc
	endbr64
	leaq	OCSP_CRLID_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE1407:
	.size	d2i_OCSP_CRLID, .-d2i_OCSP_CRLID
	.p2align 4
	.globl	i2d_OCSP_CRLID
	.type	i2d_OCSP_CRLID, @function
i2d_OCSP_CRLID:
.LFB1408:
	.cfi_startproc
	endbr64
	leaq	OCSP_CRLID_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE1408:
	.size	i2d_OCSP_CRLID, .-i2d_OCSP_CRLID
	.p2align 4
	.globl	OCSP_CRLID_new
	.type	OCSP_CRLID_new, @function
OCSP_CRLID_new:
.LFB1409:
	.cfi_startproc
	endbr64
	leaq	OCSP_CRLID_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE1409:
	.size	OCSP_CRLID_new, .-OCSP_CRLID_new
	.p2align 4
	.globl	OCSP_CRLID_free
	.type	OCSP_CRLID_free, @function
OCSP_CRLID_free:
.LFB1410:
	.cfi_startproc
	endbr64
	leaq	OCSP_CRLID_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE1410:
	.size	OCSP_CRLID_free, .-OCSP_CRLID_free
	.p2align 4
	.globl	d2i_OCSP_SERVICELOC
	.type	d2i_OCSP_SERVICELOC, @function
d2i_OCSP_SERVICELOC:
.LFB1411:
	.cfi_startproc
	endbr64
	leaq	OCSP_SERVICELOC_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE1411:
	.size	d2i_OCSP_SERVICELOC, .-d2i_OCSP_SERVICELOC
	.p2align 4
	.globl	i2d_OCSP_SERVICELOC
	.type	i2d_OCSP_SERVICELOC, @function
i2d_OCSP_SERVICELOC:
.LFB1412:
	.cfi_startproc
	endbr64
	leaq	OCSP_SERVICELOC_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE1412:
	.size	i2d_OCSP_SERVICELOC, .-i2d_OCSP_SERVICELOC
	.p2align 4
	.globl	OCSP_SERVICELOC_new
	.type	OCSP_SERVICELOC_new, @function
OCSP_SERVICELOC_new:
.LFB1413:
	.cfi_startproc
	endbr64
	leaq	OCSP_SERVICELOC_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE1413:
	.size	OCSP_SERVICELOC_new, .-OCSP_SERVICELOC_new
	.p2align 4
	.globl	OCSP_SERVICELOC_free
	.type	OCSP_SERVICELOC_free, @function
OCSP_SERVICELOC_free:
.LFB1414:
	.cfi_startproc
	endbr64
	leaq	OCSP_SERVICELOC_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE1414:
	.size	OCSP_SERVICELOC_free, .-OCSP_SERVICELOC_free
	.globl	OCSP_SERVICELOC_it
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"OCSP_SERVICELOC"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	OCSP_SERVICELOC_it, @object
	.size	OCSP_SERVICELOC_it, 56
OCSP_SERVICELOC_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	OCSP_SERVICELOC_seq_tt
	.quad	2
	.quad	0
	.quad	16
	.quad	.LC0
	.section	.rodata.str1.1
.LC1:
	.string	"issuer"
.LC2:
	.string	"locator"
	.section	.data.rel.ro,"aw"
	.align 32
	.type	OCSP_SERVICELOC_seq_tt, @object
	.size	OCSP_SERVICELOC_seq_tt, 80
OCSP_SERVICELOC_seq_tt:
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC1
	.quad	X509_NAME_it
	.quad	5
	.quad	0
	.quad	8
	.quad	.LC2
	.quad	ACCESS_DESCRIPTION_it
	.globl	OCSP_CRLID_it
	.section	.rodata.str1.1
.LC3:
	.string	"OCSP_CRLID"
	.section	.data.rel.ro.local
	.align 32
	.type	OCSP_CRLID_it, @object
	.size	OCSP_CRLID_it, 56
OCSP_CRLID_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	OCSP_CRLID_seq_tt
	.quad	3
	.quad	0
	.quad	24
	.quad	.LC3
	.section	.rodata.str1.1
.LC4:
	.string	"crlUrl"
.LC5:
	.string	"crlNum"
.LC6:
	.string	"crlTime"
	.section	.data.rel.ro
	.align 32
	.type	OCSP_CRLID_seq_tt, @object
	.size	OCSP_CRLID_seq_tt, 120
OCSP_CRLID_seq_tt:
	.quad	145
	.quad	0
	.quad	0
	.quad	.LC4
	.quad	ASN1_IA5STRING_it
	.quad	145
	.quad	1
	.quad	8
	.quad	.LC5
	.quad	ASN1_INTEGER_it
	.quad	145
	.quad	2
	.quad	16
	.quad	.LC6
	.quad	ASN1_GENERALIZEDTIME_it
	.globl	OCSP_BASICRESP_it
	.section	.rodata.str1.1
.LC7:
	.string	"OCSP_BASICRESP"
	.section	.data.rel.ro.local
	.align 32
	.type	OCSP_BASICRESP_it, @object
	.size	OCSP_BASICRESP_it, 56
OCSP_BASICRESP_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	OCSP_BASICRESP_seq_tt
	.quad	4
	.quad	0
	.quad	80
	.quad	.LC7
	.section	.rodata.str1.1
.LC8:
	.string	"tbsResponseData"
.LC9:
	.string	"signatureAlgorithm"
.LC10:
	.string	"signature"
.LC11:
	.string	"certs"
	.section	.data.rel.ro
	.align 32
	.type	OCSP_BASICRESP_seq_tt, @object
	.size	OCSP_BASICRESP_seq_tt, 160
OCSP_BASICRESP_seq_tt:
	.quad	4096
	.quad	0
	.quad	0
	.quad	.LC8
	.quad	OCSP_RESPDATA_it
	.quad	4096
	.quad	0
	.quad	48
	.quad	.LC9
	.quad	X509_ALGOR_it
	.quad	0
	.quad	0
	.quad	64
	.quad	.LC10
	.quad	ASN1_BIT_STRING_it
	.quad	149
	.quad	0
	.quad	72
	.quad	.LC11
	.quad	X509_it
	.globl	OCSP_RESPDATA_it
	.section	.rodata.str1.1
.LC12:
	.string	"OCSP_RESPDATA"
	.section	.data.rel.ro.local
	.align 32
	.type	OCSP_RESPDATA_it, @object
	.size	OCSP_RESPDATA_it, 56
OCSP_RESPDATA_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	OCSP_RESPDATA_seq_tt
	.quad	5
	.quad	0
	.quad	48
	.quad	.LC12
	.section	.rodata.str1.1
.LC13:
	.string	"version"
.LC14:
	.string	"responderId"
.LC15:
	.string	"producedAt"
.LC16:
	.string	"responses"
.LC17:
	.string	"responseExtensions"
	.section	.data.rel.ro
	.align 32
	.type	OCSP_RESPDATA_seq_tt, @object
	.size	OCSP_RESPDATA_seq_tt, 200
OCSP_RESPDATA_seq_tt:
	.quad	145
	.quad	0
	.quad	0
	.quad	.LC13
	.quad	ASN1_INTEGER_it
	.quad	4096
	.quad	0
	.quad	8
	.quad	.LC14
	.quad	OCSP_RESPID_it
	.quad	0
	.quad	0
	.quad	24
	.quad	.LC15
	.quad	ASN1_GENERALIZEDTIME_it
	.quad	4
	.quad	0
	.quad	32
	.quad	.LC16
	.quad	OCSP_SINGLERESP_it
	.quad	149
	.quad	1
	.quad	40
	.quad	.LC17
	.quad	X509_EXTENSION_it
	.globl	OCSP_SINGLERESP_it
	.section	.rodata.str1.1
.LC18:
	.string	"OCSP_SINGLERESP"
	.section	.data.rel.ro.local
	.align 32
	.type	OCSP_SINGLERESP_it, @object
	.size	OCSP_SINGLERESP_it, 56
OCSP_SINGLERESP_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	OCSP_SINGLERESP_seq_tt
	.quad	5
	.quad	0
	.quad	40
	.quad	.LC18
	.section	.rodata.str1.1
.LC19:
	.string	"certId"
.LC20:
	.string	"certStatus"
.LC21:
	.string	"thisUpdate"
.LC22:
	.string	"nextUpdate"
.LC23:
	.string	"singleExtensions"
	.section	.data.rel.ro
	.align 32
	.type	OCSP_SINGLERESP_seq_tt, @object
	.size	OCSP_SINGLERESP_seq_tt, 200
OCSP_SINGLERESP_seq_tt:
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC19
	.quad	OCSP_CERTID_it
	.quad	0
	.quad	0
	.quad	8
	.quad	.LC20
	.quad	OCSP_CERTSTATUS_it
	.quad	0
	.quad	0
	.quad	16
	.quad	.LC21
	.quad	ASN1_GENERALIZEDTIME_it
	.quad	145
	.quad	0
	.quad	24
	.quad	.LC22
	.quad	ASN1_GENERALIZEDTIME_it
	.quad	149
	.quad	1
	.quad	32
	.quad	.LC23
	.quad	X509_EXTENSION_it
	.globl	OCSP_CERTSTATUS_it
	.section	.rodata.str1.1
.LC24:
	.string	"OCSP_CERTSTATUS"
	.section	.data.rel.ro.local
	.align 32
	.type	OCSP_CERTSTATUS_it, @object
	.size	OCSP_CERTSTATUS_it, 56
OCSP_CERTSTATUS_it:
	.byte	2
	.zero	7
	.quad	0
	.quad	OCSP_CERTSTATUS_ch_tt
	.quad	3
	.quad	0
	.quad	16
	.quad	.LC24
	.section	.rodata.str1.1
.LC25:
	.string	"value.good"
.LC26:
	.string	"value.revoked"
.LC27:
	.string	"value.unknown"
	.section	.data.rel.ro
	.align 32
	.type	OCSP_CERTSTATUS_ch_tt, @object
	.size	OCSP_CERTSTATUS_ch_tt, 120
OCSP_CERTSTATUS_ch_tt:
	.quad	136
	.quad	0
	.quad	8
	.quad	.LC25
	.quad	ASN1_NULL_it
	.quad	136
	.quad	1
	.quad	8
	.quad	.LC26
	.quad	OCSP_REVOKEDINFO_it
	.quad	136
	.quad	2
	.quad	8
	.quad	.LC27
	.quad	ASN1_NULL_it
	.globl	OCSP_REVOKEDINFO_it
	.section	.rodata.str1.1
.LC28:
	.string	"OCSP_REVOKEDINFO"
	.section	.data.rel.ro.local
	.align 32
	.type	OCSP_REVOKEDINFO_it, @object
	.size	OCSP_REVOKEDINFO_it, 56
OCSP_REVOKEDINFO_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	OCSP_REVOKEDINFO_seq_tt
	.quad	2
	.quad	0
	.quad	16
	.quad	.LC28
	.section	.rodata.str1.1
.LC29:
	.string	"revocationTime"
.LC30:
	.string	"revocationReason"
	.section	.data.rel.ro
	.align 32
	.type	OCSP_REVOKEDINFO_seq_tt, @object
	.size	OCSP_REVOKEDINFO_seq_tt, 80
OCSP_REVOKEDINFO_seq_tt:
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC29
	.quad	ASN1_GENERALIZEDTIME_it
	.quad	145
	.quad	0
	.quad	8
	.quad	.LC30
	.quad	ASN1_ENUMERATED_it
	.globl	OCSP_RESPID_it
	.section	.rodata.str1.1
.LC31:
	.string	"OCSP_RESPID"
	.section	.data.rel.ro.local
	.align 32
	.type	OCSP_RESPID_it, @object
	.size	OCSP_RESPID_it, 56
OCSP_RESPID_it:
	.byte	2
	.zero	7
	.quad	0
	.quad	OCSP_RESPID_ch_tt
	.quad	2
	.quad	0
	.quad	16
	.quad	.LC31
	.section	.rodata.str1.1
.LC32:
	.string	"value.byName"
.LC33:
	.string	"value.byKey"
	.section	.data.rel.ro
	.align 32
	.type	OCSP_RESPID_ch_tt, @object
	.size	OCSP_RESPID_ch_tt, 80
OCSP_RESPID_ch_tt:
	.quad	144
	.quad	1
	.quad	8
	.quad	.LC32
	.quad	X509_NAME_it
	.quad	144
	.quad	2
	.quad	8
	.quad	.LC33
	.quad	ASN1_OCTET_STRING_it
	.globl	OCSP_RESPONSE_it
	.section	.rodata.str1.1
.LC34:
	.string	"OCSP_RESPONSE"
	.section	.data.rel.ro.local
	.align 32
	.type	OCSP_RESPONSE_it, @object
	.size	OCSP_RESPONSE_it, 56
OCSP_RESPONSE_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	OCSP_RESPONSE_seq_tt
	.quad	2
	.quad	0
	.quad	16
	.quad	.LC34
	.section	.rodata.str1.1
.LC35:
	.string	"responseStatus"
.LC36:
	.string	"responseBytes"
	.section	.data.rel.ro
	.align 32
	.type	OCSP_RESPONSE_seq_tt, @object
	.size	OCSP_RESPONSE_seq_tt, 80
OCSP_RESPONSE_seq_tt:
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC35
	.quad	ASN1_ENUMERATED_it
	.quad	145
	.quad	0
	.quad	8
	.quad	.LC36
	.quad	OCSP_RESPBYTES_it
	.globl	OCSP_RESPBYTES_it
	.section	.rodata.str1.1
.LC37:
	.string	"OCSP_RESPBYTES"
	.section	.data.rel.ro.local
	.align 32
	.type	OCSP_RESPBYTES_it, @object
	.size	OCSP_RESPBYTES_it, 56
OCSP_RESPBYTES_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	OCSP_RESPBYTES_seq_tt
	.quad	2
	.quad	0
	.quad	16
	.quad	.LC37
	.section	.rodata.str1.1
.LC38:
	.string	"responseType"
.LC39:
	.string	"response"
	.section	.data.rel.ro
	.align 32
	.type	OCSP_RESPBYTES_seq_tt, @object
	.size	OCSP_RESPBYTES_seq_tt, 80
OCSP_RESPBYTES_seq_tt:
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC38
	.quad	ASN1_OBJECT_it
	.quad	0
	.quad	0
	.quad	8
	.quad	.LC39
	.quad	ASN1_OCTET_STRING_it
	.globl	OCSP_REQUEST_it
	.section	.rodata.str1.1
.LC40:
	.string	"OCSP_REQUEST"
	.section	.data.rel.ro.local
	.align 32
	.type	OCSP_REQUEST_it, @object
	.size	OCSP_REQUEST_it, 56
OCSP_REQUEST_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	OCSP_REQUEST_seq_tt
	.quad	2
	.quad	0
	.quad	40
	.quad	.LC40
	.section	.rodata.str1.1
.LC41:
	.string	"tbsRequest"
.LC42:
	.string	"optionalSignature"
	.section	.data.rel.ro.local
	.align 32
	.type	OCSP_REQUEST_seq_tt, @object
	.size	OCSP_REQUEST_seq_tt, 80
OCSP_REQUEST_seq_tt:
	.quad	4096
	.quad	0
	.quad	0
	.quad	.LC41
	.quad	OCSP_REQINFO_it
	.quad	145
	.quad	0
	.quad	32
	.quad	.LC42
	.quad	OCSP_SIGNATURE_it
	.globl	OCSP_REQINFO_it
	.section	.rodata.str1.1
.LC43:
	.string	"OCSP_REQINFO"
	.section	.data.rel.ro.local
	.align 32
	.type	OCSP_REQINFO_it, @object
	.size	OCSP_REQINFO_it, 56
OCSP_REQINFO_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	OCSP_REQINFO_seq_tt
	.quad	4
	.quad	0
	.quad	32
	.quad	.LC43
	.section	.rodata.str1.1
.LC44:
	.string	"requestorName"
.LC45:
	.string	"requestList"
.LC46:
	.string	"requestExtensions"
	.section	.data.rel.ro
	.align 32
	.type	OCSP_REQINFO_seq_tt, @object
	.size	OCSP_REQINFO_seq_tt, 160
OCSP_REQINFO_seq_tt:
	.quad	145
	.quad	0
	.quad	0
	.quad	.LC13
	.quad	ASN1_INTEGER_it
	.quad	145
	.quad	1
	.quad	8
	.quad	.LC44
	.quad	GENERAL_NAME_it
	.quad	4
	.quad	0
	.quad	16
	.quad	.LC45
	.quad	OCSP_ONEREQ_it
	.quad	149
	.quad	2
	.quad	24
	.quad	.LC46
	.quad	X509_EXTENSION_it
	.globl	OCSP_ONEREQ_it
	.section	.rodata.str1.1
.LC47:
	.string	"OCSP_ONEREQ"
	.section	.data.rel.ro.local
	.align 32
	.type	OCSP_ONEREQ_it, @object
	.size	OCSP_ONEREQ_it, 56
OCSP_ONEREQ_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	OCSP_ONEREQ_seq_tt
	.quad	2
	.quad	0
	.quad	16
	.quad	.LC47
	.section	.rodata.str1.1
.LC48:
	.string	"reqCert"
.LC49:
	.string	"singleRequestExtensions"
	.section	.data.rel.ro
	.align 32
	.type	OCSP_ONEREQ_seq_tt, @object
	.size	OCSP_ONEREQ_seq_tt, 80
OCSP_ONEREQ_seq_tt:
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC48
	.quad	OCSP_CERTID_it
	.quad	149
	.quad	0
	.quad	8
	.quad	.LC49
	.quad	X509_EXTENSION_it
	.globl	OCSP_CERTID_it
	.section	.rodata.str1.1
.LC50:
	.string	"OCSP_CERTID"
	.section	.data.rel.ro.local
	.align 32
	.type	OCSP_CERTID_it, @object
	.size	OCSP_CERTID_it, 56
OCSP_CERTID_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	OCSP_CERTID_seq_tt
	.quad	4
	.quad	0
	.quad	88
	.quad	.LC50
	.section	.rodata.str1.1
.LC51:
	.string	"hashAlgorithm"
.LC52:
	.string	"issuerNameHash"
.LC53:
	.string	"issuerKeyHash"
.LC54:
	.string	"serialNumber"
	.section	.data.rel.ro
	.align 32
	.type	OCSP_CERTID_seq_tt, @object
	.size	OCSP_CERTID_seq_tt, 160
OCSP_CERTID_seq_tt:
	.quad	4096
	.quad	0
	.quad	0
	.quad	.LC51
	.quad	X509_ALGOR_it
	.quad	4096
	.quad	0
	.quad	16
	.quad	.LC52
	.quad	ASN1_OCTET_STRING_it
	.quad	4096
	.quad	0
	.quad	40
	.quad	.LC53
	.quad	ASN1_OCTET_STRING_it
	.quad	4096
	.quad	0
	.quad	64
	.quad	.LC54
	.quad	ASN1_INTEGER_it
	.globl	OCSP_SIGNATURE_it
	.section	.rodata.str1.1
.LC55:
	.string	"OCSP_SIGNATURE"
	.section	.data.rel.ro.local
	.align 32
	.type	OCSP_SIGNATURE_it, @object
	.size	OCSP_SIGNATURE_it, 56
OCSP_SIGNATURE_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	OCSP_SIGNATURE_seq_tt
	.quad	3
	.quad	0
	.quad	32
	.quad	.LC55
	.section	.data.rel.ro
	.align 32
	.type	OCSP_SIGNATURE_seq_tt, @object
	.size	OCSP_SIGNATURE_seq_tt, 120
OCSP_SIGNATURE_seq_tt:
	.quad	4096
	.quad	0
	.quad	0
	.quad	.LC9
	.quad	X509_ALGOR_it
	.quad	0
	.quad	0
	.quad	16
	.quad	.LC10
	.quad	ASN1_BIT_STRING_it
	.quad	149
	.quad	0
	.quad	24
	.quad	.LC11
	.quad	X509_it
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
