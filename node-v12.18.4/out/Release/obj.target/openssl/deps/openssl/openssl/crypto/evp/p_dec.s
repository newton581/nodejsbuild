	.file	"p_dec.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/evp/p_dec.c"
	.text
	.p2align 4
	.globl	EVP_PKEY_decrypt_old
	.type	EVP_PKEY_decrypt_old, @function
EVP_PKEY_decrypt_old:
.LFB779:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	movq	%rcx, %rdi
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	call	EVP_PKEY_id@PLT
	cmpl	$6, %eax
	jne	.L5
	movq	%r12, %rdi
	call	EVP_PKEY_get0_RSA@PLT
	popq	%r12
	movq	%r15, %rdx
	movq	%r14, %rsi
	movl	%r13d, %edi
	movq	%rax, %rcx
	popq	%r13
	movl	$1, %r8d
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	RSA_private_decrypt@PLT
	.p2align 4,,10
	.p2align 3
.L5:
	.cfi_restore_state
.L3:
	endbr64
	movl	$25, %r8d
	movl	$106, %edx
	movl	$151, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	popq	%r12
	movl	$-1, %eax
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE779:
	.size	EVP_PKEY_decrypt_old, .-EVP_PKEY_decrypt_old
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
