	.file	"ccm128.c"
	.text
	.p2align 4
	.globl	CRYPTO_ccm128_init
	.type	CRYPTO_ccm128_init, @function
CRYPTO_ccm128_init:
.LFB151:
	.cfi_startproc
	endbr64
	leal	-8(,%rsi,4), %eax
	subl	$1, %edx
	pxor	%xmm0, %xmm0
	movq	$0, 32(%rdi)
	andl	$56, %eax
	andl	$7, %edx
	movups	%xmm0, (%rdi)
	orl	%edx, %eax
	movq	%r8, 40(%rdi)
	movb	%al, (%rdi)
	movq	%rcx, 48(%rdi)
	ret
	.cfi_endproc
.LFE151:
	.size	CRYPTO_ccm128_init, .-CRYPTO_ccm128_init
	.p2align 4
	.globl	CRYPTO_ccm128_setiv
	.type	CRYPTO_ccm128_setiv, @function
CRYPTO_ccm128_setiv:
.LFB152:
	.cfi_startproc
	endbr64
	movq	%rdx, %r9
	movzbl	(%rdi), %edx
	movl	$14, %eax
	movl	%edx, %r8d
	movl	%edx, %r10d
	andl	$7, %r8d
	andl	$7, %r10d
	subl	%r8d, %eax
	movl	%eax, %r8d
	cmpq	%r9, %r8
	ja	.L13
	cmpb	$2, %r10b
	ja	.L21
	movq	$0, 8(%rdi)
.L6:
	andl	$-65, %edx
	bswap	%ecx
	movl	%ecx, 12(%rdi)
	movb	%dl, (%rdi)
	leaq	1(%rdi), %rdx
	cmpl	$8, %eax
	jnb	.L7
	testb	$4, %al
	jne	.L22
	testl	%eax, %eax
	je	.L8
	movzbl	(%rsi), %ecx
	movb	%cl, 1(%rdi)
	testb	$2, %al
	jne	.L23
.L8:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	movq	%rcx, %r9
	shrq	$56, %r9
	movb	%r9b, 8(%rdi)
	movq	%rcx, %r9
	shrq	$48, %r9
	movb	%r9b, 9(%rdi)
	movq	%rcx, %r9
	shrq	$40, %r9
	movb	%r9b, 10(%rdi)
	movq	%rcx, %r9
	shrq	$32, %r9
	movb	%r9b, 11(%rdi)
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L7:
	movq	(%rsi), %rcx
	addq	$9, %rdi
	movq	%rcx, -8(%rdi)
	movq	-8(%rsi,%r8), %rcx
	andq	$-8, %rdi
	movq	%rcx, -8(%rdx,%r8)
	subq	%rdi, %rdx
	addl	%edx, %eax
	subq	%rdx, %rsi
	andl	$-8, %eax
	cmpl	$8, %eax
	jb	.L8
	andl	$-8, %eax
	xorl	%edx, %edx
.L11:
	movl	%edx, %ecx
	addl	$8, %edx
	movq	(%rsi,%rcx), %r8
	movq	%r8, (%rdi,%rcx)
	cmpl	%eax, %edx
	jb	.L11
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	movl	(%rsi), %eax
	movl	%eax, 1(%rdi)
	movl	-4(%rsi,%r8), %eax
	movl	%eax, -4(%rdx,%r8)
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L23:
	movzwl	-2(%rsi,%r8), %eax
	movw	%ax, -2(%rdx,%r8)
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L13:
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE152:
	.size	CRYPTO_ccm128_setiv, .-CRYPTO_ccm128_setiv
	.p2align 4
	.globl	CRYPTO_ccm128_aad
	.type	CRYPTO_ccm128_aad, @function
CRYPTO_ccm128_aad:
.LFB153:
	.cfi_startproc
	endbr64
	testq	%rdx, %rdx
	je	.L44
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	16(%rdi), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movq	%r14, %rsi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	48(%rdi), %rdx
	orb	$64, (%rdi)
	movq	40(%rdi), %r15
	call	*%r15
	movq	%r12, %rax
	addq	$1, 32(%rbx)
	movl	%r12d, %edx
	shrq	$8, %rax
	cmpq	$65279, %r12
	ja	.L26
	xorb	%r12b, 17(%rbx)
	xorb	%al, 16(%rbx)
	movl	$2, %eax
.L27:
	movq	48(%rbx), %r8
	.p2align 4,,10
	.p2align 3
.L29:
	cmpl	$15, %eax
	ja	.L33
.L47:
	testq	%r12, %r12
	je	.L33
	movl	%eax, %ecx
	addl	$1, %eax
	addq	$1, %r13
	subq	$1, %r12
	movzbl	16(%rbx,%rcx), %edx
	xorb	-1(%r13), %dl
	movb	%dl, 16(%rbx,%rcx)
	cmpl	$15, %eax
	jbe	.L47
.L33:
	movq	%r8, %rdx
	movq	%r14, %rsi
	movq	%r14, %rdi
	call	*%r15
	addq	$1, 32(%rbx)
	testq	%r12, %r12
	je	.L24
	movq	48(%rbx), %r8
	xorl	%eax, %eax
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L26:
	movzbl	16(%rbx), %ecx
	movq	%r12, %r10
	movq	%r12, %rdi
	movzbl	17(%rbx), %esi
	shrq	$16, %r10
	shrq	$24, %rdi
	movzbl	18(%rbx), %r9d
	movzbl	19(%rbx), %r8d
	movq	%r10, -64(%rbp)
	notl	%ecx
	movl	$4294967295, %r10d
	movzbl	21(%rbx), %r11d
	movq	%rdi, -56(%rbp)
	movzbl	20(%rbx), %edi
	movb	%cl, 16(%rbx)
	cmpq	%r10, %r12
	jbe	.L28
	movq	%r12, %rcx
	notl	%esi
	xorb	%al, 24(%rbx)
	movl	$10, %eax
	shrq	$56, %rcx
	movb	%sil, 17(%rbx)
	movzbl	-56(%rbp), %esi
	xorl	%ecx, %r9d
	movq	%r12, %rcx
	xorb	%sil, 22(%rbx)
	movzbl	-64(%rbp), %esi
	shrq	$48, %rcx
	xorb	%sil, 23(%rbx)
	xorl	%ecx, %r8d
	movq	%r12, %rcx
	xorb	%r12b, 25(%rbx)
	shrq	$40, %rcx
	movb	%r9b, 18(%rbx)
	xorl	%ecx, %edi
	movq	%r12, %rcx
	movb	%r8b, 19(%rbx)
	shrq	$32, %rcx
	movb	%dil, 20(%rbx)
	xorl	%ecx, %r11d
	movb	%r11b, 21(%rbx)
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L24:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L44:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L28:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	xorl	%edi, %eax
	xorl	$-2, %esi
	xorb	-56(%rbp), %r9b
	xorb	-64(%rbp), %r8b
	xorl	%r11d, %edx
	movb	%al, 20(%rbx)
	movl	$6, %eax
	movb	%sil, 17(%rbx)
	movb	%r9b, 18(%rbx)
	movb	%r8b, 19(%rbx)
	movb	%dl, 21(%rbx)
	jmp	.L27
	.cfi_endproc
.LFE153:
	.size	CRYPTO_ccm128_aad, .-CRYPTO_ccm128_aad
	.p2align 4
	.globl	CRYPTO_ccm128_encrypt
	.type	CRYPTO_ccm128_encrypt, @function
CRYPTO_ccm128_encrypt:
.LFB155:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -56
	movq	40(%rdi), %rbx
	movq	48(%rdi), %r15
	movq	%rsi, -120(%rbp)
	movq	%rcx, -112(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	(%rdi), %eax
	movb	%al, -121(%rbp)
	testb	$64, %al
	jne	.L49
	leaq	16(%rdi), %rsi
	movq	%r15, %rdx
	call	*%rbx
	addq	$1, 32(%r12)
.L49:
	movzbl	-121(%rbp), %ecx
	movl	$15, %edx
	movl	%ecx, %esi
	movl	%ecx, %eax
	andl	$7, %esi
	andl	$7, %eax
	subl	%esi, %edx
	movb	%al, -122(%rbp)
	movb	%al, (%r12)
	movl	%edx, -128(%rbp)
	cmpl	$15, %edx
	je	.L71
	movl	%edx, %ecx
	movzbl	(%r12,%rcx), %edx
	movb	$0, (%r12,%rcx)
	movl	$16, %ecx
	subl	%esi, %ecx
	salq	$8, %rdx
	cmpl	$15, %ecx
	je	.L50
	movzbl	(%r12,%rcx), %edi
	movb	$0, (%r12,%rcx)
	movl	$17, %ecx
	subl	%esi, %ecx
	orq	%rdi, %rdx
	salq	$8, %rdx
	cmpl	$15, %ecx
	je	.L50
	movzbl	(%r12,%rcx), %edi
	movb	$0, (%r12,%rcx)
	movl	$18, %ecx
	subl	%esi, %ecx
	orq	%rdi, %rdx
	salq	$8, %rdx
	cmpl	$15, %ecx
	je	.L50
	movzbl	(%r12,%rcx), %edi
	movb	$0, (%r12,%rcx)
	movl	$19, %ecx
	subl	%esi, %ecx
	orq	%rdi, %rdx
	salq	$8, %rdx
	cmpl	$15, %ecx
	je	.L50
	movzbl	(%r12,%rcx), %edi
	movb	$0, (%r12,%rcx)
	movl	$20, %ecx
	subl	%esi, %ecx
	orq	%rdi, %rdx
	salq	$8, %rdx
	cmpl	$15, %ecx
	je	.L50
	movzbl	(%r12,%rcx), %edi
	movb	$0, (%r12,%rcx)
	orq	%rdi, %rdx
	salq	$8, %rdx
	cmpl	$6, %esi
	je	.L50
	movzbl	14(%r12), %ecx
	movb	$0, 14(%r12)
	orq	%rcx, %rdx
	salq	$8, %rdx
.L50:
	movzbl	15(%r12), %ecx
	movq	-112(%rbp), %rax
	movb	$1, 15(%r12)
	orq	%rcx, %rdx
	cmpq	%rax, %rdx
	jne	.L72
	movabsq	$2305843009213693952, %rcx
	leaq	15(%rax), %rdx
	shrq	$3, %rdx
	orq	$1, %rdx
	addq	32(%r12), %rdx
	movq	%rdx, 32(%r12)
	cmpq	%rcx, %rdx
	ja	.L73
	cmpq	$15, %rax
	jbe	.L74
	leaq	-16(%rax), %rdx
	movq	-120(%rbp), %rcx
	leaq	-80(%rbp), %r13
	andq	$-16, %rdx
	leaq	16(%r12), %r9
	movq	%r13, -96(%rbp)
	leaq	16(%rdx), %rax
	movq	%r9, %r13
	movq	%rax, -136(%rbp)
	addq	%r14, %rax
	movq	%rax, -104(%rbp)
	movq	%r14, %rax
	movq	%rcx, %r14
	.p2align 4,,10
	.p2align 3
.L57:
	movq	(%r14), %rdx
	movq	%rax, -88(%rbp)
	movq	%r13, %rsi
	movq	%r13, %rdi
	xorq	%rdx, 16(%r12)
	movq	8(%r14), %rdx
	xorq	%rdx, 24(%r12)
	movq	%r15, %rdx
	call	*%rbx
	movq	-96(%rbp), %rsi
	movq	%r15, %rdx
	movq	%r12, %rdi
	call	*%rbx
	addb	$1, 15(%r12)
	movq	-88(%rbp), %rax
	jne	.L55
	addb	$1, 14(%r12)
	jne	.L55
	addb	$1, 13(%r12)
	jne	.L55
	addb	$1, 12(%r12)
	jne	.L55
	addb	$1, 11(%r12)
	jne	.L55
	addb	$1, 10(%r12)
	jne	.L55
	addb	$1, 9(%r12)
	jne	.L55
	addb	$1, 8(%r12)
.L55:
	movq	-80(%rbp), %rdx
	xorq	(%r14), %rdx
	addq	$16, %rax
	addq	$16, %r14
	movq	%rdx, -16(%rax)
	movq	-72(%rbp), %rdx
	xorq	-8(%r14), %rdx
	movq	%rdx, -8(%rax)
	cmpq	%rax, -104(%rbp)
	jne	.L57
	movq	-136(%rbp), %rcx
	andq	$15, -112(%rbp)
	addq	%rcx, -120(%rbp)
	movq	-96(%rbp), %r13
.L53:
	cmpq	$0, -112(%rbp)
	jne	.L171
.L65:
	movzbl	-122(%rbp), %edx
	movl	-128(%rbp), %ecx
	xorl	%r8d, %r8d
	addq	$1, %rdx
	addq	%r12, %rcx
	cmpl	$8, %edx
	jb	.L172
	leaq	8(%rcx), %rsi
	movq	$0, (%rcx)
	movq	$0, -8(%rdx,%rcx)
	andq	$-8, %rsi
	subq	%rsi, %rcx
	addl	%ecx, %edx
	andl	$-8, %edx
	cmpl	$8, %edx
	jb	.L59
	andl	$-8, %edx
	xorl	%ecx, %ecx
.L62:
	movl	%ecx, %eax
	addl	$8, %ecx
	movq	%r8, (%rsi,%rax)
	cmpl	%edx, %ecx
	jb	.L62
.L59:
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	*%rbx
	movdqu	16(%r12), %xmm0
	movzbl	-121(%rbp), %eax
	pxor	-80(%rbp), %xmm0
	movb	%al, (%r12)
	xorl	%eax, %eax
	movups	%xmm0, 16(%r12)
.L48:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L173
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L172:
	.cfi_restore_state
	testb	$4, %dl
	jne	.L174
	testl	%edx, %edx
	je	.L59
	movb	$0, (%rcx)
	testb	$2, %dl
	je	.L59
	xorl	%eax, %eax
	movw	%ax, -2(%rdx,%rcx)
	jmp	.L59
.L71:
	xorl	%edx, %edx
	jmp	.L50
.L74:
	movq	%r14, -104(%rbp)
	leaq	-80(%rbp), %r13
	jmp	.L53
.L171:
	movq	-120(%rbp), %rax
	movzbl	(%rax), %edx
	xorb	%dl, 16(%r12)
	cmpq	$1, -112(%rbp)
	je	.L64
	movq	-112(%rbp), %rcx
	movzbl	1(%rax), %edx
	xorb	%dl, 17(%r12)
	cmpq	$2, %rcx
	je	.L64
	movzbl	2(%rax), %edx
	xorb	%dl, 18(%r12)
	cmpq	$3, %rcx
	je	.L64
	movzbl	3(%rax), %edx
	xorb	%dl, 19(%r12)
	cmpq	$4, %rcx
	je	.L64
	movzbl	4(%rax), %edx
	xorb	%dl, 20(%r12)
	cmpq	$5, %rcx
	je	.L64
	movzbl	5(%rax), %edx
	xorb	%dl, 21(%r12)
	cmpq	$6, %rcx
	je	.L64
	movzbl	6(%rax), %edx
	xorb	%dl, 22(%r12)
	cmpq	$7, %rcx
	je	.L64
	movzbl	7(%rax), %edx
	xorb	%dl, 23(%r12)
	cmpq	$8, %rcx
	je	.L64
	movzbl	8(%rax), %edx
	xorb	%dl, 24(%r12)
	cmpq	$9, %rcx
	je	.L64
	movzbl	9(%rax), %edx
	xorb	%dl, 25(%r12)
	cmpq	$10, %rcx
	je	.L64
	movzbl	10(%rax), %edx
	xorb	%dl, 26(%r12)
	cmpq	$11, %rcx
	je	.L64
	movzbl	11(%rax), %edx
	xorb	%dl, 27(%r12)
	cmpq	$12, %rcx
	je	.L64
	movzbl	12(%rax), %edx
	xorb	%dl, 28(%r12)
	cmpq	$13, %rcx
	je	.L64
	movzbl	13(%rax), %edx
	xorb	%dl, 29(%r12)
	cmpq	$15, %rcx
	jne	.L64
	movzbl	14(%rax), %edx
	xorb	%dl, 30(%r12)
.L64:
	leaq	16(%r12), %rdi
	movq	%r15, %rdx
	movq	%rdi, %rsi
	call	*%rbx
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	*%rbx
	movq	-120(%rbp), %rax
	movzbl	-80(%rbp), %edx
	xorb	(%rax), %dl
	movq	-104(%rbp), %rax
	cmpq	$1, -112(%rbp)
	movb	%dl, (%rax)
	je	.L65
	movq	%rax, %rcx
	movq	-120(%rbp), %rax
	movq	-112(%rbp), %rdi
	movzbl	-79(%rbp), %edx
	xorb	1(%rax), %dl
	movb	%dl, 1(%rcx)
	cmpq	$2, %rdi
	je	.L65
	movzbl	-78(%rbp), %edx
	xorb	2(%rax), %dl
	movb	%dl, 2(%rcx)
	cmpq	$3, %rdi
	je	.L65
	movzbl	-77(%rbp), %edx
	xorb	3(%rax), %dl
	movb	%dl, 3(%rcx)
	cmpq	$4, %rdi
	je	.L65
	movzbl	-76(%rbp), %edx
	xorb	4(%rax), %dl
	movb	%dl, 4(%rcx)
	cmpq	$5, %rdi
	je	.L65
	movzbl	-75(%rbp), %edx
	xorb	5(%rax), %dl
	movb	%dl, 5(%rcx)
	cmpq	$6, %rdi
	je	.L65
	movzbl	-74(%rbp), %edx
	xorb	6(%rax), %dl
	movb	%dl, 6(%rcx)
	cmpq	$7, %rdi
	je	.L65
	movzbl	-73(%rbp), %edx
	xorb	7(%rax), %dl
	movb	%dl, 7(%rcx)
	cmpq	$8, %rdi
	je	.L65
	movzbl	-72(%rbp), %edx
	xorb	8(%rax), %dl
	movb	%dl, 8(%rcx)
	cmpq	$9, %rdi
	je	.L65
	movzbl	-71(%rbp), %edx
	xorb	9(%rax), %dl
	movb	%dl, 9(%rcx)
	cmpq	$10, %rdi
	je	.L65
	movzbl	-70(%rbp), %edx
	xorb	10(%rax), %dl
	movb	%dl, 10(%rcx)
	cmpq	$11, %rdi
	je	.L65
	movzbl	-69(%rbp), %edx
	xorb	11(%rax), %dl
	movb	%dl, 11(%rcx)
	cmpq	$12, %rdi
	je	.L65
	movzbl	-68(%rbp), %edx
	xorb	12(%rax), %dl
	movb	%dl, 12(%rcx)
	cmpq	$13, %rdi
	je	.L65
	movzbl	-67(%rbp), %edx
	xorb	13(%rax), %dl
	movb	%dl, 13(%rcx)
	cmpq	$15, %rdi
	jne	.L65
	movzbl	-66(%rbp), %edx
	xorb	14(%rax), %dl
	movb	%dl, 14(%rcx)
	jmp	.L65
.L174:
	movl	$0, (%rcx)
	movl	$0, -4(%rdx,%rcx)
	jmp	.L59
.L72:
	movl	$-1, %eax
	jmp	.L48
.L73:
	movl	$-2, %eax
	jmp	.L48
.L173:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE155:
	.size	CRYPTO_ccm128_encrypt, .-CRYPTO_ccm128_encrypt
	.p2align 4
	.globl	CRYPTO_ccm128_decrypt
	.type	CRYPTO_ccm128_decrypt, @function
CRYPTO_ccm128_decrypt:
.LFB156:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -56
	movq	40(%rdi), %rbx
	movq	48(%rdi), %r14
	movq	%rdx, -120(%rbp)
	movq	%rcx, -112(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	(%rdi), %eax
	movq	%rbx, -88(%rbp)
	movb	%al, -121(%rbp)
	testb	$64, %al
	jne	.L176
	leaq	16(%rdi), %rsi
	movq	%r14, %rdx
	call	*%rbx
.L176:
	movzbl	-121(%rbp), %ebx
	movl	$15, %edx
	movl	%ebx, %esi
	movl	%ebx, %eax
	andl	$7, %esi
	andl	$7, %eax
	subl	%esi, %edx
	movb	%al, -122(%rbp)
	movb	%al, (%r15)
	movl	%edx, -128(%rbp)
	cmpl	$15, %edx
	je	.L194
	movl	%edx, %ecx
	movzbl	(%r15,%rcx), %edx
	movb	$0, (%r15,%rcx)
	movl	$16, %ecx
	subl	%esi, %ecx
	salq	$8, %rdx
	cmpl	$15, %ecx
	je	.L177
	movzbl	(%r15,%rcx), %edi
	movb	$0, (%r15,%rcx)
	movl	$17, %ecx
	subl	%esi, %ecx
	orq	%rdi, %rdx
	salq	$8, %rdx
	cmpl	$15, %ecx
	je	.L177
	movzbl	(%r15,%rcx), %edi
	movb	$0, (%r15,%rcx)
	movl	$18, %ecx
	subl	%esi, %ecx
	orq	%rdi, %rdx
	salq	$8, %rdx
	cmpl	$15, %ecx
	je	.L177
	movzbl	(%r15,%rcx), %edi
	movb	$0, (%r15,%rcx)
	movl	$19, %ecx
	subl	%esi, %ecx
	orq	%rdi, %rdx
	salq	$8, %rdx
	cmpl	$15, %ecx
	je	.L177
	movzbl	(%r15,%rcx), %edi
	movb	$0, (%r15,%rcx)
	movl	$20, %ecx
	subl	%esi, %ecx
	orq	%rdi, %rdx
	salq	$8, %rdx
	cmpl	$15, %ecx
	je	.L177
	movzbl	(%r15,%rcx), %edi
	movb	$0, (%r15,%rcx)
	orq	%rdi, %rdx
	salq	$8, %rdx
	cmpl	$6, %esi
	je	.L177
	movzbl	14(%r15), %ecx
	movb	$0, 14(%r15)
	orq	%rcx, %rdx
	salq	$8, %rdx
.L177:
	movzbl	15(%r15), %ecx
	movq	-112(%rbp), %rax
	movb	$1, 15(%r15)
	orq	%rcx, %rdx
	cmpq	%rax, %rdx
	jne	.L195
	cmpq	$15, %rax
	jbe	.L196
	leaq	-16(%rax), %rdx
	movq	-120(%rbp), %rbx
	leaq	16(%r15), %r13
	andq	$-16, %rdx
	leaq	16(%rdx), %rax
	movq	%rax, -136(%rbp)
	addq	%r12, %rax
	movq	%rax, -104(%rbp)
	leaq	-80(%rbp), %rax
	movq	%rax, -96(%rbp)
	.p2align 4,,10
	.p2align 3
.L184:
	movq	-96(%rbp), %rsi
	movq	-88(%rbp), %rax
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	*%rax
	addb	$1, 15(%r15)
	jne	.L182
	addb	$1, 14(%r15)
	jne	.L182
	addb	$1, 13(%r15)
	jne	.L182
	addb	$1, 12(%r15)
	jne	.L182
	addb	$1, 11(%r15)
	jne	.L182
	addb	$1, 10(%r15)
	jne	.L182
	addb	$1, 9(%r15)
	jne	.L182
	addb	$1, 8(%r15)
.L182:
	movq	-80(%rbp), %rdx
	xorq	(%r12), %rdx
	movq	%r13, %rsi
	movq	%r13, %rdi
	movq	%rdx, (%rbx)
	movq	-88(%rbp), %rax
	addq	$16, %r12
	addq	$16, %rbx
	xorq	%rdx, 16(%r15)
	movq	-72(%rbp), %rdx
	xorq	-8(%r12), %rdx
	movq	%rdx, -8(%rbx)
	xorq	%rdx, 24(%r15)
	movq	%r14, %rdx
	call	*%rax
	cmpq	-104(%rbp), %r12
	jne	.L184
	movq	-136(%rbp), %rdi
	andq	$15, -112(%rbp)
	addq	%rdi, -120(%rbp)
.L180:
	movq	-112(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L185
	movq	-96(%rbp), %rsi
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	-88(%rbp), %rax
	call	*%rax
	movq	-104(%rbp), %rdi
	movq	-120(%rbp), %rsi
	movzbl	-80(%rbp), %edx
	xorb	(%rdi), %dl
	movb	%dl, (%rsi)
	xorb	%dl, 16(%r15)
	cmpq	$1, %rbx
	je	.L186
	movzbl	-79(%rbp), %edx
	xorb	1(%rdi), %dl
	movb	%dl, 1(%rsi)
	xorb	%dl, 17(%r15)
	cmpq	$2, %rbx
	je	.L186
	movzbl	-78(%rbp), %edx
	xorb	2(%rdi), %dl
	movb	%dl, 2(%rsi)
	xorb	%dl, 18(%r15)
	cmpq	$3, %rbx
	je	.L186
	movzbl	-77(%rbp), %edx
	xorb	3(%rdi), %dl
	movb	%dl, 3(%rsi)
	xorb	%dl, 19(%r15)
	cmpq	$4, %rbx
	je	.L186
	movzbl	-76(%rbp), %edx
	xorb	4(%rdi), %dl
	movb	%dl, 4(%rsi)
	xorb	%dl, 20(%r15)
	cmpq	$5, %rbx
	je	.L186
	movzbl	-75(%rbp), %edx
	xorb	5(%rdi), %dl
	movb	%dl, 5(%rsi)
	xorb	%dl, 21(%r15)
	cmpq	$6, %rbx
	je	.L186
	movzbl	-74(%rbp), %edx
	xorb	6(%rdi), %dl
	movb	%dl, 6(%rsi)
	xorb	%dl, 22(%r15)
	cmpq	$7, %rbx
	je	.L186
	movzbl	-73(%rbp), %edx
	xorb	7(%rdi), %dl
	movb	%dl, 7(%rsi)
	xorb	%dl, 23(%r15)
	cmpq	$8, %rbx
	je	.L186
	movzbl	-72(%rbp), %edx
	xorb	8(%rdi), %dl
	movb	%dl, 8(%rsi)
	xorb	%dl, 24(%r15)
	cmpq	$9, %rbx
	je	.L186
	movzbl	-71(%rbp), %edx
	xorb	9(%rdi), %dl
	movb	%dl, 9(%rsi)
	xorb	%dl, 25(%r15)
	cmpq	$10, %rbx
	je	.L186
	movzbl	-70(%rbp), %edx
	xorb	10(%rdi), %dl
	movb	%dl, 10(%rsi)
	xorb	%dl, 26(%r15)
	cmpq	$11, %rbx
	je	.L186
	movzbl	-69(%rbp), %edx
	xorb	11(%rdi), %dl
	movb	%dl, 11(%rsi)
	xorb	%dl, 27(%r15)
	cmpq	$12, %rbx
	je	.L186
	movzbl	-68(%rbp), %edx
	xorb	12(%rdi), %dl
	movb	%dl, 12(%rsi)
	xorb	%dl, 28(%r15)
	cmpq	$13, %rbx
	je	.L186
	movzbl	-67(%rbp), %edx
	xorb	13(%rdi), %dl
	movb	%dl, 13(%rsi)
	xorb	%dl, 29(%r15)
	cmpq	$15, %rbx
	jne	.L186
	movzbl	-66(%rbp), %edx
	xorb	14(%rdi), %dl
	movb	%dl, 14(%rsi)
	xorb	%dl, 30(%r15)
.L186:
	leaq	16(%r15), %rdi
	movq	-88(%rbp), %rax
	movq	%r14, %rdx
	movq	%rdi, %rsi
	call	*%rax
.L185:
	movzbl	-122(%rbp), %edx
	movl	-128(%rbp), %ecx
	xorl	%r8d, %r8d
	addq	$1, %rdx
	addq	%r15, %rcx
	cmpl	$8, %edx
	jb	.L263
	leaq	8(%rcx), %rsi
	movq	$0, (%rcx)
	movq	$0, -8(%rdx,%rcx)
	andq	$-8, %rsi
	subq	%rsi, %rcx
	addl	%ecx, %edx
	andl	$-8, %edx
	cmpl	$8, %edx
	jb	.L188
	andl	$-8, %edx
	xorl	%ecx, %ecx
.L191:
	movl	%ecx, %eax
	addl	$8, %ecx
	movq	%r8, (%rsi,%rax)
	cmpl	%edx, %ecx
	jb	.L191
.L188:
	movq	-88(%rbp), %rax
	movq	-96(%rbp), %rsi
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	*%rax
	movdqu	16(%r15), %xmm0
	movzbl	-121(%rbp), %eax
	pxor	-80(%rbp), %xmm0
	movb	%al, (%r15)
	xorl	%eax, %eax
	movups	%xmm0, 16(%r15)
.L175:
	movq	-56(%rbp), %rdi
	xorq	%fs:40, %rdi
	jne	.L264
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L263:
	.cfi_restore_state
	testb	$4, %dl
	jne	.L265
	testl	%edx, %edx
	je	.L188
	movb	$0, (%rcx)
	testb	$2, %dl
	je	.L188
	xorl	%eax, %eax
	movw	%ax, -2(%rdx,%rcx)
	jmp	.L188
.L194:
	xorl	%edx, %edx
	jmp	.L177
.L196:
	leaq	-80(%rbp), %rax
	movq	%r12, -104(%rbp)
	movq	%rax, -96(%rbp)
	jmp	.L180
.L265:
	movl	$0, (%rcx)
	movl	$0, -4(%rdx,%rcx)
	jmp	.L188
.L195:
	movl	$-1, %eax
	jmp	.L175
.L264:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE156:
	.size	CRYPTO_ccm128_decrypt, .-CRYPTO_ccm128_decrypt
	.p2align 4
	.globl	CRYPTO_ccm128_encrypt_ccm64
	.type	CRYPTO_ccm128_encrypt_ccm64, @function
CRYPTO_ccm128_encrypt_ccm64:
.LFB158:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rax
	movq	%rdx, %r10
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$104, %rsp
	movq	40(%rbx), %rcx
	movzbl	(%rbx), %r14d
	movq	%r8, -96(%rbp)
	movq	48(%rbx), %r15
	movq	%fs:40, %rdi
	movq	%rdi, -56(%rbp)
	xorl	%edi, %edi
	movq	%rcx, -88(%rbp)
	testb	$64, %r14b
	jne	.L267
	movq	%rdx, -112(%rbp)
	movq	%rbx, %rdi
	movq	%r15, %rdx
	movq	%rsi, -104(%rbp)
	leaq	16(%rbx), %rsi
	call	*%rcx
	addq	$1, 32(%rbx)
	movq	-112(%rbp), %r10
	movq	-104(%rbp), %rax
.L267:
	movl	%r14d, %esi
	movl	%r14d, %r11d
	movl	$15, %r12d
	andl	$7, %esi
	andl	$7, %r11d
	subl	%esi, %r12d
	movb	%r11b, (%rbx)
	cmpl	$15, %r12d
	je	.L292
	movl	%r12d, %ecx
	movzbl	(%rbx,%rcx), %edx
	movb	$0, (%rbx,%rcx)
	movl	$16, %ecx
	subl	%esi, %ecx
	salq	$8, %rdx
	cmpl	$15, %ecx
	je	.L268
	movzbl	(%rbx,%rcx), %edi
	movb	$0, (%rbx,%rcx)
	movl	$17, %ecx
	subl	%esi, %ecx
	orq	%rdi, %rdx
	salq	$8, %rdx
	cmpl	$15, %ecx
	je	.L268
	movzbl	(%rbx,%rcx), %edi
	movb	$0, (%rbx,%rcx)
	movl	$18, %ecx
	subl	%esi, %ecx
	orq	%rdi, %rdx
	salq	$8, %rdx
	cmpl	$15, %ecx
	je	.L268
	movzbl	(%rbx,%rcx), %edi
	movb	$0, (%rbx,%rcx)
	movl	$19, %ecx
	subl	%esi, %ecx
	orq	%rdi, %rdx
	salq	$8, %rdx
	cmpl	$15, %ecx
	je	.L268
	movzbl	(%rbx,%rcx), %edi
	movb	$0, (%rbx,%rcx)
	movl	$20, %ecx
	subl	%esi, %ecx
	orq	%rdi, %rdx
	salq	$8, %rdx
	cmpl	$15, %ecx
	je	.L268
	movzbl	(%rbx,%rcx), %edi
	movb	$0, (%rbx,%rcx)
	orq	%rdi, %rdx
	salq	$8, %rdx
	cmpl	$6, %esi
	je	.L268
	movzbl	14(%rbx), %ecx
	movb	$0, 14(%rbx)
	orq	%rcx, %rdx
	salq	$8, %rdx
.L268:
	movzbl	15(%rbx), %ecx
	movb	$1, 15(%rbx)
	orq	%rcx, %rdx
	cmpq	%r13, %rdx
	jne	.L293
	movabsq	$2305843009213693952, %rcx
	leaq	15(%r13), %rdx
	shrq	$3, %rdx
	orq	$1, %rdx
	addq	32(%rbx), %rdx
	movq	%rdx, 32(%rbx)
	cmpq	%rcx, %rdx
	ja	.L294
	cmpq	$15, %r13
	jbe	.L271
	movq	%r13, %rdx
	leaq	16(%rbx), %r9
	movq	%r10, -112(%rbp)
	movq	%rbx, %r8
	shrq	$4, %rdx
	movq	%r9, -128(%rbp)
	movq	%r15, %rcx
	movq	%r10, %rsi
	movb	%r11b, -136(%rbp)
	movq	%rax, %rdi
	movq	%rdx, -120(%rbp)
	movq	%rax, -104(%rbp)
	movq	-96(%rbp), %rax
	call	*%rax
	movq	-104(%rbp), %rax
	movq	-112(%rbp), %r10
	movq	%r13, %rcx
	andq	$-16, %rcx
	movq	%r13, %r8
	movq	-120(%rbp), %rdx
	movq	-128(%rbp), %r9
	addq	%rcx, %rax
	addq	%rcx, %r10
	subq	%rcx, %r8
	movzbl	-136(%rbp), %r11d
	jne	.L272
	leaq	-80(%rbp), %rsi
.L280:
	movzbl	%r11b, %eax
	addq	%rbx, %r12
	xorl	%r8d, %r8d
	addq	$1, %rax
	cmpl	$8, %eax
	jnb	.L273
	testb	$4, %al
	jne	.L401
	testl	%eax, %eax
	je	.L274
	movb	$0, (%r12)
	testb	$2, %al
	jne	.L402
.L274:
	movq	-88(%rbp), %rax
	movq	%r15, %rdx
	movq	%rbx, %rdi
	call	*%rax
	movdqu	16(%rbx), %xmm0
	movb	%r14b, (%rbx)
	xorl	%eax, %eax
	pxor	-80(%rbp), %xmm0
	movups	%xmm0, 16(%rbx)
.L266:
	movq	-56(%rbp), %rdi
	xorq	%fs:40, %rdi
	jne	.L403
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L271:
	.cfi_restore_state
	leaq	-80(%rbp), %rsi
	testq	%r13, %r13
	je	.L280
	leaq	16(%rbx), %r9
	xorl	%ecx, %ecx
.L288:
	movzbl	(%rax), %edx
	xorb	%dl, 16(%rbx)
	cmpq	$1, %r13
	je	.L295
	movzbl	1(%rax), %edx
	xorb	%dl, 17(%rbx)
	cmpq	$2, %r13
	je	.L296
	movzbl	2(%rax), %edx
	xorb	%dl, 18(%rbx)
	cmpq	$3, %r13
	je	.L297
	movzbl	3(%rax), %edx
	xorb	%dl, 19(%rbx)
	cmpq	$4, %r13
	je	.L298
	movzbl	4(%rax), %edx
	xorb	%dl, 20(%rbx)
	cmpq	$5, %r13
	je	.L299
	movzbl	5(%rax), %edx
	xorb	%dl, 21(%rbx)
	cmpq	$6, %r13
	je	.L300
	movzbl	6(%rax), %edx
	xorb	%dl, 22(%rbx)
	cmpq	$7, %r13
	je	.L301
	movzbl	7(%rax), %edx
	xorb	%dl, 23(%rbx)
	cmpq	$8, %r13
	je	.L302
	movzbl	8(%rax), %edx
	xorb	%dl, 24(%rbx)
	cmpq	$9, %r13
	je	.L303
	movzbl	9(%rax), %edx
	xorb	%dl, 25(%rbx)
	cmpq	$10, %r13
	je	.L304
	movzbl	10(%rax), %edx
	xorb	%dl, 26(%rbx)
	cmpq	$11, %r13
	je	.L305
	movzbl	11(%rax), %edx
	xorb	%dl, 27(%rbx)
	cmpq	$12, %r13
	je	.L306
	movzbl	12(%rax), %edx
	xorb	%dl, 28(%rbx)
	cmpq	$13, %r13
	je	.L307
	movzbl	13(%rax), %edx
	xorb	%dl, 29(%rbx)
	cmpq	$14, %r13
	je	.L308
	movzbl	14(%rax), %edx
	xorb	%dl, 30(%rbx)
	testb	%cl, %cl
	je	.L282
	movzbl	15(%rax), %edx
	movq	%r13, %r8
	xorb	%dl, 31(%rbx)
	jmp	.L281
	.p2align 4,,10
	.p2align 3
.L273:
	leaq	8(%r12), %rcx
	movq	$0, (%r12)
	movq	$0, -8(%rax,%r12)
	andq	$-8, %rcx
	subq	%rcx, %r12
	addl	%r12d, %eax
	andl	$-8, %eax
	cmpl	$8, %eax
	jb	.L274
	andl	$-8, %eax
	xorl	%edx, %edx
.L277:
	movl	%edx, %edi
	addl	$8, %edx
	movq	%r8, (%rcx,%rdi)
	cmpl	%eax, %edx
	jb	.L277
	jmp	.L274
	.p2align 4,,10
	.p2align 3
.L272:
	movzbl	15(%rbx), %ecx
	movzbl	%dl, %edx
	movq	%r13, %rsi
	shrq	$12, %rsi
	addq	%rcx, %rdx
	movb	%dl, 15(%rbx)
	shrq	$8, %rdx
	movq	%rdx, %rdi
	orq	%rsi, %rdi
	je	.L279
	movzbl	14(%rbx), %ecx
	movzbl	%sil, %esi
	addq	%rsi, %rcx
	movq	%r13, %rsi
	addq	%rcx, %rdx
	shrq	$20, %rsi
	movb	%dl, 14(%rbx)
	shrq	$8, %rdx
	movq	%rdx, %rdi
	orq	%rsi, %rdi
	je	.L279
	movzbl	13(%rbx), %ecx
	movzbl	%sil, %esi
	addq	%rsi, %rcx
	movq	%r13, %rsi
	addq	%rcx, %rdx
	shrq	$28, %rsi
	movb	%dl, 13(%rbx)
	shrq	$8, %rdx
	movq	%rdx, %rdi
	orq	%rsi, %rdi
	je	.L279
	movzbl	12(%rbx), %ecx
	movzbl	%sil, %esi
	addq	%rsi, %rcx
	movq	%r13, %rsi
	addq	%rcx, %rdx
	shrq	$36, %rsi
	movb	%dl, 12(%rbx)
	shrq	$8, %rdx
	movq	%rdx, %rdi
	orq	%rsi, %rdi
	je	.L279
	movzbl	11(%rbx), %ecx
	movzbl	%sil, %esi
	addq	%rsi, %rcx
	movq	%r13, %rsi
	addq	%rcx, %rdx
	shrq	$44, %rsi
	movb	%dl, 11(%rbx)
	shrq	$8, %rdx
	movq	%rdx, %rdi
	orq	%rsi, %rdi
	je	.L279
	movzbl	10(%rbx), %ecx
	movzbl	%sil, %esi
	addq	%rsi, %rcx
	movq	%r13, %rsi
	addq	%rcx, %rdx
	shrq	$52, %rsi
	movb	%dl, 10(%rbx)
	shrq	$8, %rdx
	movq	%rdx, %rdi
	orq	%rsi, %rdi
	je	.L279
	movzbl	9(%rbx), %ecx
	movzbl	%sil, %esi
	shrq	$60, %r13
	addq	%rsi, %rcx
	addq	%rcx, %rdx
	movb	%dl, 9(%rbx)
	shrq	$8, %rdx
	movq	%rdx, %rdi
	orq	%r13, %rdi
	je	.L279
	movzbl	8(%rbx), %esi
	addq	%rsi, %r13
	addq	%r13, %rdx
	movb	%dl, 8(%rbx)
.L279:
	cmpq	$15, %r8
	leaq	32(%rbx), %rcx
	seta	%dl
	cmpq	%rcx, %rax
	setnb	%cl
	cmpq	%rax, %rbx
	setnb	%sil
	orl	%esi, %ecx
	andb	%dl, %cl
	je	.L404
	movdqu	16(%rbx), %xmm0
	movdqu	(%rax), %xmm1
	pxor	%xmm1, %xmm0
	movups	%xmm0, 16(%rbx)
.L281:
	movq	%r10, -136(%rbp)
	movq	%r9, %rsi
	movq	%r9, %rdi
	movq	%r15, %rdx
	movq	%rax, -128(%rbp)
	movq	-88(%rbp), %r13
	movb	%r11b, -120(%rbp)
	movq	%r8, -112(%rbp)
	movb	%cl, -104(%rbp)
	call	*%r13
	leaq	-80(%rbp), %rsi
	movq	%r15, %rdx
	movq	%rbx, %rdi
	movq	%rsi, -96(%rbp)
	call	*%r13
	movq	-136(%rbp), %r10
	movq	-128(%rbp), %rax
	movq	-96(%rbp), %rsi
	movzbl	-104(%rbp), %ecx
	leaq	15(%r10), %rdx
	movq	-112(%rbp), %r8
	movzbl	-120(%rbp), %r11d
	movq	%rdx, %rdi
	subq	%rax, %rdi
	cmpq	$30, %rdi
	jbe	.L286
	testb	%cl, %cl
	je	.L286
	movdqu	(%rax), %xmm0
	pxor	-80(%rbp), %xmm0
	movups	%xmm0, (%r10)
	jmp	.L280
	.p2align 4,,10
	.p2align 3
.L292:
	xorl	%edx, %edx
	jmp	.L268
.L286:
	movzbl	-80(%rbp), %edi
	xorb	(%rax), %dil
	movb	%dil, (%r10)
	cmpq	$1, %r8
	je	.L280
.L290:
	movzbl	-79(%rbp), %edi
	xorb	1(%rax), %dil
	movb	%dil, 1(%r10)
	cmpq	$2, %r8
	je	.L280
	movzbl	-78(%rbp), %edi
	xorb	2(%rax), %dil
	movb	%dil, 2(%r10)
	cmpq	$3, %r8
	je	.L280
	movzbl	-77(%rbp), %edi
	xorb	3(%rax), %dil
	movb	%dil, 3(%r10)
	cmpq	$4, %r8
	je	.L280
	movzbl	-76(%rbp), %edi
	xorb	4(%rax), %dil
	movb	%dil, 4(%r10)
	cmpq	$5, %r8
	je	.L280
	movzbl	-75(%rbp), %edi
	xorb	5(%rax), %dil
	movb	%dil, 5(%r10)
	cmpq	$6, %r8
	je	.L280
	movzbl	-74(%rbp), %edi
	xorb	6(%rax), %dil
	movb	%dil, 6(%r10)
	cmpq	$7, %r8
	je	.L280
	movzbl	-73(%rbp), %edi
	xorb	7(%rax), %dil
	movb	%dil, 7(%r10)
	cmpq	$8, %r8
	je	.L280
	movzbl	-72(%rbp), %edi
	xorb	8(%rax), %dil
	movb	%dil, 8(%r10)
	cmpq	$9, %r8
	je	.L280
	movzbl	-71(%rbp), %edi
	xorb	9(%rax), %dil
	movb	%dil, 9(%r10)
	cmpq	$10, %r8
	je	.L280
	movzbl	-70(%rbp), %edi
	xorb	10(%rax), %dil
	movb	%dil, 10(%r10)
	cmpq	$11, %r8
	je	.L280
	movzbl	-69(%rbp), %edi
	xorb	11(%rax), %dil
	movb	%dil, 11(%r10)
	cmpq	$12, %r8
	je	.L280
	movzbl	-68(%rbp), %edi
	xorb	12(%rax), %dil
	movb	%dil, 12(%r10)
	cmpq	$13, %r8
	je	.L280
	movzbl	-67(%rbp), %edi
	xorb	13(%rax), %dil
	movb	%dil, 13(%r10)
	cmpq	$14, %r8
	je	.L280
	movzbl	-66(%rbp), %edi
	xorb	14(%rax), %dil
	movb	%dil, 14(%r10)
	testb	%cl, %cl
	je	.L280
	movzbl	15(%rax), %eax
	xorb	-65(%rbp), %al
	movb	%al, (%rdx)
	jmp	.L280
.L402:
	xorl	%edx, %edx
	movw	%dx, -2(%rax,%r12)
	jmp	.L274
.L295:
	movl	$1, %r8d
	jmp	.L281
.L299:
	movl	$5, %r8d
	jmp	.L281
.L401:
	movl	$0, (%r12)
	movl	$0, -4(%rax,%r12)
	jmp	.L274
.L293:
	movl	$-1, %eax
	jmp	.L266
.L294:
	movl	$-2, %eax
	jmp	.L266
.L296:
	movl	$2, %r8d
	jmp	.L281
.L297:
	movl	$3, %r8d
	jmp	.L281
.L298:
	movl	$4, %r8d
	jmp	.L281
.L300:
	movl	$6, %r8d
	jmp	.L281
.L301:
	movl	$7, %r8d
	jmp	.L281
.L302:
	movl	$8, %r8d
	jmp	.L281
.L303:
	movl	$9, %r8d
	jmp	.L281
.L304:
	movl	$10, %r8d
	jmp	.L281
.L305:
	movl	$11, %r8d
	jmp	.L281
.L307:
	movl	$13, %r8d
	jmp	.L281
.L306:
	movl	$12, %r8d
	jmp	.L281
.L282:
	movq	%r10, -128(%rbp)
	movq	%r9, %rsi
	movq	%r9, %rdi
	movq	%r15, %rdx
	movb	%r11b, -112(%rbp)
	movb	%cl, -104(%rbp)
	movq	%rax, -120(%rbp)
	movq	-88(%rbp), %rax
	call	*%rax
	leaq	-80(%rbp), %rsi
	movq	-88(%rbp), %rax
	movq	%r15, %rdx
	movq	%rsi, -96(%rbp)
	movq	%rbx, %rdi
	call	*%rax
	movq	-120(%rbp), %rax
	movq	-128(%rbp), %r10
	movq	%r13, %r8
	movzbl	-80(%rbp), %edi
	movq	-96(%rbp), %rsi
	xorb	(%rax), %dil
	movzbl	-104(%rbp), %ecx
	leaq	15(%r10), %rdx
	movb	%dil, (%r10)
	movzbl	-112(%rbp), %r11d
	jmp	.L290
.L308:
	movl	$14, %r8d
	jmp	.L281
.L404:
	movq	%r8, %r13
	movl	%edx, %ecx
	jmp	.L288
.L403:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE158:
	.size	CRYPTO_ccm128_encrypt_ccm64, .-CRYPTO_ccm128_encrypt_ccm64
	.p2align 4
	.globl	CRYPTO_ccm128_decrypt_ccm64
	.type	CRYPTO_ccm128_decrypt_ccm64, @function
CRYPTO_ccm128_decrypt_ccm64:
.LFB159:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r10
	movq	%rdx, %rax
	movq	%r8, %r11
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$104, %rsp
	movq	40(%rbx), %rcx
	movzbl	(%rbx), %r14d
	movq	%fs:40, %rdi
	movq	%rdi, -56(%rbp)
	xorl	%edi, %edi
	movq	48(%rbx), %r15
	movq	%rcx, -88(%rbp)
	testb	$64, %r14b
	jne	.L406
	movq	%rdx, -104(%rbp)
	movq	%rbx, %rdi
	movq	%r15, %rdx
	movq	%rsi, -96(%rbp)
	leaq	16(%rbx), %rsi
	movq	%r8, -112(%rbp)
	call	*%rcx
	movq	-112(%rbp), %r11
	movq	-104(%rbp), %rax
	movq	-96(%rbp), %r10
.L406:
	movl	%r14d, %edi
	movl	%r14d, %ecx
	movl	$15, %r12d
	andl	$7, %edi
	andl	$7, %ecx
	subl	%edi, %r12d
	movb	%cl, -96(%rbp)
	movb	%cl, (%rbx)
	cmpl	$15, %r12d
	je	.L427
	movl	%r12d, %esi
	movzbl	(%rbx,%rsi), %edx
	movb	$0, (%rbx,%rsi)
	movl	$16, %esi
	subl	%edi, %esi
	salq	$8, %rdx
	cmpl	$15, %esi
	je	.L407
	movzbl	(%rbx,%rsi), %r8d
	movb	$0, (%rbx,%rsi)
	movl	$17, %esi
	subl	%edi, %esi
	orq	%r8, %rdx
	salq	$8, %rdx
	cmpl	$15, %esi
	je	.L407
	movzbl	(%rbx,%rsi), %r8d
	movb	$0, (%rbx,%rsi)
	movl	$18, %esi
	subl	%edi, %esi
	orq	%r8, %rdx
	salq	$8, %rdx
	cmpl	$15, %esi
	je	.L407
	movzbl	(%rbx,%rsi), %r8d
	movb	$0, (%rbx,%rsi)
	movl	$19, %esi
	subl	%edi, %esi
	orq	%r8, %rdx
	salq	$8, %rdx
	cmpl	$15, %esi
	je	.L407
	movzbl	(%rbx,%rsi), %r8d
	movb	$0, (%rbx,%rsi)
	movl	$20, %esi
	subl	%edi, %esi
	orq	%r8, %rdx
	salq	$8, %rdx
	cmpl	$15, %esi
	je	.L407
	movzbl	(%rbx,%rsi), %r8d
	movb	$0, (%rbx,%rsi)
	orq	%r8, %rdx
	salq	$8, %rdx
	cmpl	$6, %edi
	je	.L407
	movzbl	14(%rbx), %esi
	movb	$0, 14(%rbx)
	orq	%rsi, %rdx
	salq	$8, %rdx
.L407:
	movzbl	15(%rbx), %esi
	movb	$1, 15(%rbx)
	orq	%rsi, %rdx
	cmpq	%r13, %rdx
	jne	.L428
	cmpq	$15, %r13
	jbe	.L410
	movq	%r13, %rdx
	leaq	16(%rbx), %r9
	movq	%rax, %rsi
	movq	%rax, -112(%rbp)
	shrq	$4, %rdx
	movq	%r9, -128(%rbp)
	movq	%rbx, %r8
	movq	%r15, %rcx
	movq	%rdx, -120(%rbp)
	movq	%r10, %rdi
	movq	%r10, -104(%rbp)
	call	*%r11
	movq	-104(%rbp), %r10
	movq	-112(%rbp), %rax
	movq	%r13, %rsi
	andq	$-16, %rsi
	movq	%r13, %rcx
	movq	-120(%rbp), %rdx
	movq	-128(%rbp), %r9
	addq	%rsi, %r10
	addq	%rsi, %rax
	leaq	-80(%rbp), %r8
	subq	%rsi, %rcx
	movq	%rcx, -104(%rbp)
	je	.L412
	movzbl	15(%rbx), %esi
	movzbl	%dl, %edx
	movq	%r13, %rdi
	shrq	$12, %rdi
	addq	%rsi, %rdx
	movq	%rdi, %rcx
	movb	%dl, 15(%rbx)
	shrq	$8, %rdx
	orq	%rdx, %rcx
	je	.L413
	movzbl	14(%rbx), %esi
	movzbl	%dil, %edi
	addq	%rdi, %rsi
	movq	%r13, %rdi
	addq	%rsi, %rdx
	shrq	$20, %rdi
	movb	%dl, 14(%rbx)
	shrq	$8, %rdx
	movq	%rdx, %rcx
	orq	%rdi, %rcx
	je	.L413
	movzbl	13(%rbx), %esi
	movzbl	%dil, %edi
	addq	%rdi, %rsi
	movq	%r13, %rdi
	addq	%rsi, %rdx
	shrq	$28, %rdi
	movb	%dl, 13(%rbx)
	shrq	$8, %rdx
	movq	%rdx, %rcx
	orq	%rdi, %rcx
	je	.L413
	movzbl	12(%rbx), %esi
	movzbl	%dil, %edi
	addq	%rdi, %rsi
	movq	%r13, %rdi
	addq	%rsi, %rdx
	shrq	$36, %rdi
	movb	%dl, 12(%rbx)
	shrq	$8, %rdx
	movq	%rdx, %rcx
	orq	%rdi, %rcx
	je	.L413
	movzbl	11(%rbx), %esi
	movzbl	%dil, %edi
	addq	%rdi, %rsi
	movq	%r13, %rdi
	addq	%rsi, %rdx
	shrq	$44, %rdi
	movb	%dl, 11(%rbx)
	shrq	$8, %rdx
	movq	%rdx, %rcx
	orq	%rdi, %rcx
	je	.L413
	movzbl	10(%rbx), %esi
	movzbl	%dil, %edi
	addq	%rdi, %rsi
	movq	%r13, %rdi
	addq	%rsi, %rdx
	shrq	$52, %rdi
	movb	%dl, 10(%rbx)
	shrq	$8, %rdx
	movq	%rdx, %rcx
	orq	%rdi, %rcx
	je	.L413
	movzbl	9(%rbx), %esi
	movzbl	%dil, %edi
	shrq	$60, %r13
	addq	%rdi, %rsi
	addq	%rsi, %rdx
	movb	%dl, 9(%rbx)
	shrq	$8, %rdx
	movq	%rdx, %rcx
	orq	%r13, %rcx
	je	.L413
	movzbl	8(%rbx), %esi
	addq	%rsi, %r13
	addq	%r13, %rdx
	movb	%dl, 8(%rbx)
.L413:
	leaq	-80(%rbp), %r8
	movq	%r10, -128(%rbp)
	movq	%r15, %rdx
	movq	%rbx, %rdi
	movq	%r8, %rsi
	movq	%rax, -136(%rbp)
	movq	-88(%rbp), %rax
	movq	%r9, -120(%rbp)
	movq	%r8, -112(%rbp)
	call	*%rax
	movq	-128(%rbp), %r10
	leaq	32(%rbx), %rsi
	cmpq	$15, -104(%rbp)
	seta	%cl
	movq	-136(%rbp), %rax
	movq	-112(%rbp), %r8
	cmpq	%rsi, %r10
	movq	-120(%rbp), %r9
	setnb	%r11b
	cmpq	%r10, %rbx
	leaq	15(%rax), %rdi
	setnb	%dl
	orl	%r11d, %edx
	cmpq	%rsi, %rax
	setnb	%sil
	cmpq	%rax, %rbx
	setnb	%r11b
	orl	%r11d, %esi
	andl	%edx, %esi
	testb	%cl, %sil
	je	.L429
	movq	%rdi, %rdx
	subq	%r10, %rdx
	cmpq	$30, %rdx
	jbe	.L429
	movdqu	(%r10), %xmm0
	pxor	-80(%rbp), %xmm0
	movups	%xmm0, (%rax)
	movdqu	16(%rbx), %xmm1
	pxor	%xmm1, %xmm0
	movups	%xmm0, 16(%rbx)
.L415:
	movq	%r8, -104(%rbp)
	movq	-88(%rbp), %rax
	movq	%r15, %rdx
	movq	%r9, %rsi
	movq	%r9, %rdi
	call	*%rax
	movq	-104(%rbp), %r8
.L412:
	movzbl	-96(%rbp), %eax
	addq	%rbx, %r12
	xorl	%r9d, %r9d
	addq	$1, %rax
	cmpl	$8, %eax
	jnb	.L416
	testb	$4, %al
	jne	.L522
	testl	%eax, %eax
	je	.L417
	movb	$0, (%r12)
	testb	$2, %al
	jne	.L523
.L417:
	movq	-88(%rbp), %rax
	movq	%r15, %rdx
	movq	%r8, %rsi
	movq	%rbx, %rdi
	call	*%rax
	movdqu	16(%rbx), %xmm0
	movb	%r14b, (%rbx)
	xorl	%eax, %eax
	pxor	-80(%rbp), %xmm0
	movups	%xmm0, 16(%rbx)
.L405:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L524
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L410:
	.cfi_restore_state
	movq	%rax, -112(%rbp)
	leaq	-80(%rbp), %r8
	movq	%r10, -104(%rbp)
	testq	%r13, %r13
	je	.L412
	leaq	16(%rbx), %r9
	movq	%r8, -120(%rbp)
	movq	-88(%rbp), %rax
	movq	%r8, %rsi
	movq	%r9, -128(%rbp)
	movq	%rbx, %rdi
	movq	%r15, %rdx
	call	*%rax
	movq	-112(%rbp), %rax
	movq	-104(%rbp), %r10
	xorl	%ecx, %ecx
	movq	-128(%rbp), %r9
	movq	-120(%rbp), %r8
	leaq	15(%rax), %rdi
.L424:
	movzbl	-80(%rbp), %edx
	xorb	(%r10), %dl
	movb	%dl, (%rax)
	xorb	%dl, 16(%rbx)
	cmpq	$1, %r13
	je	.L415
	movzbl	-79(%rbp), %edx
	xorb	1(%r10), %dl
	movb	%dl, 1(%rax)
	xorb	%dl, 17(%rbx)
	cmpq	$2, %r13
	je	.L415
	movzbl	-78(%rbp), %edx
	xorb	2(%r10), %dl
	movb	%dl, 2(%rax)
	xorb	%dl, 18(%rbx)
	cmpq	$3, %r13
	je	.L415
	movzbl	-77(%rbp), %edx
	xorb	3(%r10), %dl
	movb	%dl, 3(%rax)
	xorb	%dl, 19(%rbx)
	cmpq	$4, %r13
	je	.L415
	movzbl	-76(%rbp), %edx
	xorb	4(%r10), %dl
	movb	%dl, 4(%rax)
	xorb	%dl, 20(%rbx)
	cmpq	$5, %r13
	je	.L415
	movzbl	-75(%rbp), %edx
	xorb	5(%r10), %dl
	movb	%dl, 5(%rax)
	xorb	%dl, 21(%rbx)
	cmpq	$6, %r13
	je	.L415
	movzbl	-74(%rbp), %edx
	xorb	6(%r10), %dl
	movb	%dl, 6(%rax)
	xorb	%dl, 22(%rbx)
	cmpq	$7, %r13
	je	.L415
	movzbl	-73(%rbp), %edx
	xorb	7(%r10), %dl
	movb	%dl, 7(%rax)
	xorb	%dl, 23(%rbx)
	cmpq	$8, %r13
	je	.L415
	movzbl	-72(%rbp), %edx
	xorb	8(%r10), %dl
	movb	%dl, 8(%rax)
	xorb	%dl, 24(%rbx)
	cmpq	$9, %r13
	je	.L415
	movzbl	-71(%rbp), %edx
	xorb	9(%r10), %dl
	movb	%dl, 9(%rax)
	xorb	%dl, 25(%rbx)
	cmpq	$10, %r13
	je	.L415
	movzbl	-70(%rbp), %edx
	xorb	10(%r10), %dl
	movb	%dl, 10(%rax)
	xorb	%dl, 26(%rbx)
	cmpq	$11, %r13
	je	.L415
	movzbl	-69(%rbp), %edx
	xorb	11(%r10), %dl
	movb	%dl, 11(%rax)
	xorb	%dl, 27(%rbx)
	cmpq	$12, %r13
	je	.L415
	movzbl	-68(%rbp), %edx
	xorb	12(%r10), %dl
	movb	%dl, 12(%rax)
	xorb	%dl, 28(%rbx)
	cmpq	$13, %r13
	je	.L415
	movzbl	-67(%rbp), %edx
	xorb	13(%r10), %dl
	movb	%dl, 13(%rax)
	xorb	%dl, 29(%rbx)
	cmpq	$14, %r13
	je	.L415
	movzbl	-66(%rbp), %edx
	xorb	14(%r10), %dl
	movb	%dl, 14(%rax)
	xorb	%dl, 30(%rbx)
	testb	%cl, %cl
	je	.L415
	movzbl	15(%r10), %eax
	xorb	-65(%rbp), %al
	movb	%al, (%rdi)
	xorb	%al, 31(%rbx)
	jmp	.L415
	.p2align 4,,10
	.p2align 3
.L416:
	leaq	8(%r12), %rsi
	movq	$0, (%r12)
	movq	$0, -8(%rax,%r12)
	andq	$-8, %rsi
	subq	%rsi, %r12
	addl	%r12d, %eax
	andl	$-8, %eax
	cmpl	$8, %eax
	jb	.L417
	andl	$-8, %eax
	xorl	%edx, %edx
.L420:
	movl	%edx, %ecx
	addl	$8, %edx
	movq	%r9, (%rsi,%rcx)
	cmpl	%eax, %edx
	jb	.L420
	jmp	.L417
	.p2align 4,,10
	.p2align 3
.L427:
	xorl	%edx, %edx
	jmp	.L407
.L523:
	xorl	%edx, %edx
	movw	%dx, -2(%rax,%r12)
	jmp	.L417
.L522:
	movl	$0, (%r12)
	movl	$0, -4(%rax,%r12)
	jmp	.L417
.L428:
	movl	$-1, %eax
	jmp	.L405
.L524:
	call	__stack_chk_fail@PLT
.L429:
	movq	-104(%rbp), %r13
	jmp	.L424
	.cfi_endproc
.LFE159:
	.size	CRYPTO_ccm128_decrypt_ccm64, .-CRYPTO_ccm128_decrypt_ccm64
	.p2align 4
	.globl	CRYPTO_ccm128_tag
	.type	CRYPTO_ccm128_tag, @function
CRYPTO_ccm128_tag:
.LFB160:
	.cfi_startproc
	endbr64
	movzbl	(%rdi), %eax
	shrb	$3, %al
	andl	$7, %eax
	leal	2(%rax,%rax), %r8d
	movq	%r8, %rax
	cmpq	%rdx, %r8
	je	.L541
	xorl	%r8d, %r8d
.L525:
	movq	%r8, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L541:
	leaq	16(%rdi), %rdx
	cmpl	$8, %r8d
	jb	.L542
	movq	16(%rdi), %rcx
	leaq	8(%rsi), %rdi
	andq	$-8, %rdi
	movq	%rcx, (%rsi)
	movq	-8(%rdx,%r8), %rcx
	movq	%rcx, -8(%rsi,%r8)
	subq	%rdi, %rsi
	movq	%rdx, %rcx
	addl	%esi, %eax
	subq	%rsi, %rcx
	andl	$-8, %eax
	cmpl	$8, %eax
	jb	.L525
	andl	$-8, %eax
	xorl	%edx, %edx
.L531:
	movl	%edx, %esi
	addl	$8, %edx
	movq	(%rcx,%rsi), %r9
	movq	%r9, (%rdi,%rsi)
	cmpl	%eax, %edx
	jb	.L531
	jmp	.L525
	.p2align 4,,10
	.p2align 3
.L542:
	testb	$4, %al
	jne	.L543
	testl	%r8d, %r8d
	je	.L525
	movzbl	16(%rdi), %ecx
	movb	%cl, (%rsi)
	testb	$2, %al
	je	.L525
	movzwl	-2(%rdx,%r8), %eax
	movw	%ax, -2(%rsi,%r8)
	jmp	.L525
	.p2align 4,,10
	.p2align 3
.L543:
	movl	16(%rdi), %eax
	movl	%eax, (%rsi)
	movl	-4(%rdx,%r8), %eax
	movl	%eax, -4(%rsi,%r8)
	jmp	.L525
	.cfi_endproc
.LFE160:
	.size	CRYPTO_ccm128_tag, .-CRYPTO_ccm128_tag
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
