	.file	"bio_asn1.c"
	.text
	.p2align 4
	.type	asn1_bio_callback_ctrl, @function
asn1_bio_callback_ctrl:
.LFB429:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movl	%esi, %r12d
	call	BIO_next@PLT
	testq	%rax, %rax
	je	.L2
	movq	%r13, %rdx
	movl	%r12d, %esi
	movq	%rax, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	BIO_callback_ctrl@PLT
	.p2align 4,,10
	.p2align 3
.L2:
	.cfi_restore_state
	popq	%r12
	xorl	%eax, %eax
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE429:
	.size	asn1_bio_callback_ctrl, .-asn1_bio_callback_ctrl
	.p2align 4
	.type	asn1_bio_gets, @function
asn1_bio_gets:
.LFB428:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	call	BIO_next@PLT
	testq	%rax, %rax
	je	.L6
	movl	%r13d, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	BIO_gets@PLT
	.p2align 4,,10
	.p2align 3
.L6:
	.cfi_restore_state
	popq	%r12
	xorl	%eax, %eax
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE428:
	.size	asn1_bio_gets, .-asn1_bio_gets
	.p2align 4
	.type	asn1_bio_read, @function
asn1_bio_read:
.LFB426:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	call	BIO_next@PLT
	testq	%rax, %rax
	je	.L9
	movl	%r13d, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	BIO_read@PLT
	.p2align 4,,10
	.p2align 3
.L9:
	.cfi_restore_state
	popq	%r12
	xorl	%eax, %eax
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE426:
	.size	asn1_bio_read, .-asn1_bio_read
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/asn1/bio_asn1.c"
	.text
	.p2align 4
	.type	asn1_bio_free, @function
asn1_bio_free:
.LFB422:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L21
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	BIO_get_data@PLT
	movq	%rax, %r13
	xorl	%eax, %eax
	testq	%r13, %r13
	je	.L11
	movq	8(%r13), %rdi
	movl	$141, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movl	$142, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r13, %rdi
	call	CRYPTO_free@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	BIO_set_data@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	BIO_set_init@PLT
	movl	$1, %eax
.L11:
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE422:
	.size	asn1_bio_free, .-asn1_bio_free
	.p2align 4
	.type	asn1_bio_ctrl, @function
asn1_bio_ctrl:
.LFB430:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	call	BIO_get_data@PLT
	testq	%rax, %rax
	je	.L59
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	BIO_next@PLT
	movq	%rax, %r8
	cmpl	$11, %r13d
	je	.L25
	leal	-149(%r13), %eax
	cmpl	$5, %eax
	ja	.L26
	leaq	.L28(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L28:
	.long	.L33-.L28
	.long	.L32-.L28
	.long	.L31-.L28
	.long	.L30-.L28
	.long	.L29-.L28
	.long	.L27-.L28
	.text
	.p2align 4,,10
	.p2align 3
.L25:
	testq	%rax, %rax
	je	.L59
	movl	(%rbx), %eax
	cmpl	$2, %eax
	je	.L61
	cmpl	$5, %eax
	je	.L62
	cmpl	$6, %eax
	je	.L39
.L41:
	movl	$15, %esi
	movq	%r12, %rdi
	call	BIO_clear_flags@PLT
	.p2align 4,,10
	.p2align 3
.L59:
	xorl	%eax, %eax
.L22:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L29:
	.cfi_restore_state
	movq	%r14, 88(%rbx)
	movl	$1, %eax
	jmp	.L22
.L27:
	movq	88(%rbx), %rax
	movq	%rax, (%r14)
	movl	$1, %eax
	jmp	.L22
.L33:
	movdqu	(%r14), %xmm0
	movl	$1, %eax
	movups	%xmm0, 40(%rbx)
	jmp	.L22
.L32:
	movdqu	40(%rbx), %xmm1
	movl	$1, %eax
	movups	%xmm1, (%r14)
	jmp	.L22
.L31:
	movdqu	(%r14), %xmm2
	movl	$1, %eax
	movups	%xmm2, 56(%rbx)
	jmp	.L22
.L30:
	movdqu	56(%rbx), %xmm3
	movl	$1, %eax
	movups	%xmm3, (%r14)
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L26:
	testq	%r8, %r8
	je	.L59
	movq	%r14, %rcx
	movq	%r15, %rdx
	movl	%r13d, %esi
.L60:
	addq	$40, %rsp
	movq	%r8, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	BIO_ctrl@PLT
	.p2align 4,,10
	.p2align 3
.L62:
	.cfi_restore_state
	movl	80(%rbx), %r13d
	testl	%r13d, %r13d
	jle	.L41
.L38:
	movq	64(%rbx), %rax
	movq	%rax, -72(%rbp)
	movl	84(%rbx), %eax
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L63:
	movl	80(%rbx), %r13d
	movq	-64(%rbp), %r8
	subl	%eax, %r13d
	testl	%r13d, %r13d
	movl	%r13d, 80(%rbx)
	jle	.L43
	addl	84(%rbx), %eax
	movl	%eax, 84(%rbx)
.L44:
	movq	%r12, %rdi
	cltq
	addq	72(%rbx), %rax
	movq	%r8, -64(%rbp)
	movq	%rax, -56(%rbp)
	call	BIO_next@PLT
	movq	-56(%rbp), %rsi
	movl	%r13d, %edx
	movq	%rax, %rdi
	call	BIO_write@PLT
	testl	%eax, %eax
	jg	.L63
	cltq
	jmp	.L22
.L43:
	movq	-72(%rbp), %rax
	testq	%rax, %rax
	je	.L45
	movq	%r8, -56(%rbp)
	leaq	88(%rbx), %rcx
	leaq	80(%rbx), %rdx
	movq	%r12, %rdi
	leaq	72(%rbx), %rsi
	call	*%rax
	movq	-56(%rbp), %r8
.L45:
	movl	$6, (%rbx)
	movl	$0, 84(%rbx)
.L39:
	movq	%r14, %rcx
	movq	%r15, %rdx
	movl	$11, %esi
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L61:
	movq	56(%rbx), %rax
	testq	%rax, %rax
	je	.L36
	movq	%r8, -56(%rbp)
	leaq	88(%rbx), %rcx
	leaq	80(%rbx), %rdx
	movq	%r12, %rdi
	leaq	72(%rbx), %rsi
	call	*%rax
	movq	-56(%rbp), %r8
	testl	%eax, %eax
	je	.L41
.L36:
	movl	80(%rbx), %r13d
	testl	%r13d, %r13d
	jle	.L37
	movl	$5, (%rbx)
	jmp	.L38
.L37:
	movl	$6, (%rbx)
	jmp	.L39
	.cfi_endproc
.LFE430:
	.size	asn1_bio_ctrl, .-asn1_bio_ctrl
	.p2align 4
	.type	asn1_bio_new, @function
asn1_bio_new:
.LFB420:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$103, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	xorl	%r14d, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	movl	$96, %edi
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -40
	call	CRYPTO_zalloc@PLT
	testq	%rax, %rax
	je	.L64
	movl	$119, %edx
	leaq	.LC0(%rip), %rsi
	movl	$20, %edi
	movq	%rax, %r12
	call	CRYPTO_malloc@PLT
	movq	%rax, 8(%r12)
	testq	%rax, %rax
	je	.L70
	movabsq	$17179869184, %rax
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	$1, %r14d
	movq	%rax, 32(%r12)
	movl	$20, 16(%r12)
	movl	$0, (%r12)
	call	BIO_set_data@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	call	BIO_set_init@PLT
.L64:
	addq	$8, %rsp
	movl	%r14d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L70:
	.cfi_restore_state
	movl	$120, %r8d
	movl	$65, %edx
	movl	$113, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	%r12, %rdi
	movl	$108, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	addq	$8, %rsp
	movl	%r14d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE420:
	.size	asn1_bio_new, .-asn1_bio_new
	.p2align 4
	.type	asn1_bio_write, @function
asn1_bio_write:
.LFB423:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$72, %rsp
	movq	%rsi, -88(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	BIO_get_data@PLT
	movq	%r12, %rdi
	movq	%rax, %r15
	call	BIO_next@PLT
	movq	%rax, -80(%rbp)
	testq	%r14, %r14
	je	.L106
	testl	%ebx, %ebx
	js	.L106
	testq	%r15, %r15
	je	.L106
	cmpq	$0, -80(%rbp)
	je	.L106
	movl	(%r15), %esi
	movl	$0, -92(%rbp)
	leaq	.L79(%rip), %r13
	movq	%rsi, %rcx
.L77:
	cmpl	$6, %ecx
	ja	.L77
.L104:
	movslq	0(%r13,%rsi,4), %rax
	addq	%r13, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L79:
	.long	.L84-.L79
	.long	.L83-.L79
	.long	.L82-.L79
	.long	.L81-.L79
	.long	.L80-.L79
	.long	.L78-.L79
	.long	.L78-.L79
	.text
	.p2align 4,,10
	.p2align 3
.L78:
	movl	$15, %esi
	movq	%r12, %rdi
	call	BIO_clear_flags@PLT
.L106:
	xorl	%eax, %eax
.L71:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L107
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L80:
	.cfi_restore_state
	movq	-88(%rbp), %r14
	cmpl	%ebx, 28(%r15)
	movl	%ebx, %edx
	movq	-80(%rbp), %rdi
	cmovle	28(%r15), %edx
	movq	%r14, %rsi
	call	BIO_write@PLT
	testl	%eax, %eax
	jle	.L88
	movl	28(%r15), %edx
	movslq	%eax, %rcx
	addl	%eax, -92(%rbp)
	subl	%eax, %ebx
	addq	%rcx, %r14
	subl	%eax, %edx
	movq	%r14, -88(%rbp)
	movl	%edx, 28(%r15)
	testl	%edx, %edx
	je	.L108
.L93:
	testl	%ebx, %ebx
	je	.L94
	movl	(%r15), %esi
	movq	%rsi, %rcx
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L81:
	movl	24(%r15), %edx
	movq	-80(%rbp), %rdi
	movslq	20(%r15), %rsi
	addq	8(%r15), %rsi
	call	BIO_write@PLT
	testl	%eax, %eax
	jle	.L88
	subl	%eax, 24(%r15)
	je	.L92
	movl	(%r15), %esi
	addl	%eax, 20(%r15)
	movq	%rsi, %rcx
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L83:
	movl	80(%r15), %r14d
	testl	%r14d, %r14d
	jle	.L77
	movq	48(%r15), %rax
	movq	%rax, -104(%rbp)
	movl	84(%r15), %eax
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L109:
	movl	80(%r15), %r14d
	subl	%eax, %r14d
	movl	%r14d, 80(%r15)
	testl	%r14d, %r14d
	jle	.L89
	addl	84(%r15), %eax
	movl	%eax, 84(%r15)
.L90:
	movq	%r12, %rdi
	cltq
	addq	72(%r15), %rax
	movq	%rax, -72(%rbp)
	call	BIO_next@PLT
	movq	-72(%rbp), %rsi
	movl	%r14d, %edx
	movq	%rax, %rdi
	call	BIO_write@PLT
	testl	%eax, %eax
	jg	.L109
.L88:
	movl	$15, %esi
	movq	%r12, %rdi
	movl	%eax, -72(%rbp)
	call	BIO_clear_flags@PLT
	movq	%r12, %rdi
	call	BIO_copy_next_retry@PLT
	movl	-92(%rbp), %ebx
	movl	-72(%rbp), %eax
	testl	%ebx, %ebx
	cmovne	%ebx, %eax
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L84:
	movq	40(%r15), %rax
	testq	%rax, %rax
	je	.L85
	leaq	88(%r15), %rcx
	leaq	80(%r15), %rdx
	movq	%r12, %rdi
	leaq	72(%r15), %rsi
	call	*%rax
	testl	%eax, %eax
	je	.L110
.L85:
	movl	80(%r15), %eax
	testl	%eax, %eax
	jle	.L86
	movl	$1, (%r15)
	movl	$1, %ecx
	movl	$1, %esi
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L82:
	movl	36(%r15), %edx
	movl	%ebx, %esi
	xorl	%edi, %edi
	call	ASN1_object_size@PLT
	subl	%ebx, %eax
	movl	%eax, 24(%r15)
	cmpl	16(%r15), %eax
	jg	.L106
	movq	8(%r15), %rax
	movl	36(%r15), %ecx
	xorl	%esi, %esi
	leaq	-64(%rbp), %rdi
	movl	32(%r15), %r8d
	movl	%ebx, %edx
	movq	%rax, -64(%rbp)
	call	ASN1_put_object@PLT
	movl	%ebx, 28(%r15)
	movl	$3, %ecx
	movl	$3, %esi
	movl	$3, (%r15)
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L92:
	movl	$0, 20(%r15)
	movl	$4, %ecx
	movl	$4, %esi
	movl	$4, (%r15)
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L108:
	movl	$2, (%r15)
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L86:
	movl	$2, (%r15)
	movl	$2, %ecx
	movl	$2, %esi
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L89:
	movq	-104(%rbp), %rax
	testq	%rax, %rax
	je	.L91
	leaq	88(%r15), %rcx
	leaq	80(%r15), %rdx
	movq	%r12, %rdi
	leaq	72(%r15), %rsi
	call	*%rax
.L91:
	movl	$2, (%r15)
	movl	$2, %ecx
	movl	$2, %esi
	movl	$0, 84(%r15)
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L110:
	movl	$15, %esi
	movq	%r12, %rdi
	movl	%eax, -72(%rbp)
	call	BIO_clear_flags@PLT
	movl	-72(%rbp), %eax
	jmp	.L71
.L94:
	movl	$15, %esi
	movq	%r12, %rdi
	call	BIO_clear_flags@PLT
	movq	%r12, %rdi
	call	BIO_copy_next_retry@PLT
	movl	-92(%rbp), %eax
	jmp	.L71
.L107:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE423:
	.size	asn1_bio_write, .-asn1_bio_write
	.p2align 4
	.type	asn1_bio_puts, @function
asn1_bio_puts:
.LFB427:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	movq	%rsi, %rdi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	call	strlen@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%r12
	movq	%rax, %rdx
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	asn1_bio_write
	.cfi_endproc
.LFE427:
	.size	asn1_bio_puts, .-asn1_bio_puts
	.p2align 4
	.globl	BIO_f_asn1
	.type	BIO_f_asn1, @function
BIO_f_asn1:
.LFB419:
	.cfi_startproc
	endbr64
	leaq	methods_asn1(%rip), %rax
	ret
	.cfi_endproc
.LFE419:
	.size	BIO_f_asn1, .-BIO_f_asn1
	.p2align 4
	.globl	BIO_asn1_set_prefix
	.type	BIO_asn1_set_prefix, @function
BIO_asn1_set_prefix:
.LFB433:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %xmm1
	movq	%rsi, %xmm0
	xorl	%edx, %edx
	punpcklqdq	%xmm1, %xmm0
	movl	$149, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-32(%rbp), %rcx
	movaps	%xmm0, -32(%rbp)
	call	BIO_ctrl@PLT
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L117
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L117:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE433:
	.size	BIO_asn1_set_prefix, .-BIO_asn1_set_prefix
	.p2align 4
	.globl	BIO_asn1_get_prefix
	.type	BIO_asn1_get_prefix, @function
BIO_asn1_get_prefix:
.LFB434:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	movl	$150, %esi
	pushq	%rbx
	leaq	-48(%rbp), %rcx
	.cfi_offset 3, -32
	movq	%rdx, %rbx
	xorl	%edx, %edx
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	BIO_ctrl@PLT
	testl	%eax, %eax
	jle	.L118
	movq	-48(%rbp), %rdx
	movq	%rdx, (%r12)
	movq	-40(%rbp), %rdx
	movq	%rdx, (%rbx)
.L118:
	movq	-24(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L122
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L122:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE434:
	.size	BIO_asn1_get_prefix, .-BIO_asn1_get_prefix
	.p2align 4
	.globl	BIO_asn1_set_suffix
	.type	BIO_asn1_set_suffix, @function
BIO_asn1_set_suffix:
.LFB435:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %xmm1
	movq	%rsi, %xmm0
	xorl	%edx, %edx
	punpcklqdq	%xmm1, %xmm0
	movl	$151, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-32(%rbp), %rcx
	movaps	%xmm0, -32(%rbp)
	call	BIO_ctrl@PLT
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L126
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L126:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE435:
	.size	BIO_asn1_set_suffix, .-BIO_asn1_set_suffix
	.p2align 4
	.globl	BIO_asn1_get_suffix
	.type	BIO_asn1_get_suffix, @function
BIO_asn1_get_suffix:
.LFB436:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	movl	$152, %esi
	pushq	%rbx
	leaq	-48(%rbp), %rcx
	.cfi_offset 3, -32
	movq	%rdx, %rbx
	xorl	%edx, %edx
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	BIO_ctrl@PLT
	testl	%eax, %eax
	jle	.L127
	movq	-48(%rbp), %rdx
	movq	%rdx, (%r12)
	movq	-40(%rbp), %rdx
	movq	%rdx, (%rbx)
.L127:
	movq	-24(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L131
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L131:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE436:
	.size	BIO_asn1_get_suffix, .-BIO_asn1_get_suffix
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"asn1"
	.section	.data.rel.ro,"aw"
	.align 32
	.type	methods_asn1, @object
	.size	methods_asn1, 96
methods_asn1:
	.long	534
	.zero	4
	.quad	.LC1
	.quad	bwrite_conv
	.quad	asn1_bio_write
	.quad	bread_conv
	.quad	asn1_bio_read
	.quad	asn1_bio_puts
	.quad	asn1_bio_gets
	.quad	asn1_bio_ctrl
	.quad	asn1_bio_new
	.quad	asn1_bio_free
	.quad	asn1_bio_callback_ctrl
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
