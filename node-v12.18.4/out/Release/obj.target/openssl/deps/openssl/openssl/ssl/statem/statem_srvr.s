	.file	"statem_srvr.c"
	.text
	.p2align 4
	.type	tls_construct_encrypted_extensions, @function
tls_construct_encrypted_extensions:
.LFB1108:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$1024, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	tls_construct_extensions@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
	ret
	.cfi_endproc
.LFE1108:
	.size	tls_construct_encrypted_extensions, .-tls_construct_encrypted_extensions
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/ssl/statem/statem_srvr.c"
	.text
	.p2align 4
	.globl	tls_construct_server_hello
	.type	tls_construct_server_hello, @function
tls_construct_server_hello:
.LFB1087:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	192(%rax), %rdx
	testb	$8, 96(%rdx)
	jne	.L5
	movl	(%rax), %eax
	cmpl	$771, %eax
	jle	.L5
	cmpl	$65536, %eax
	jne	.L23
.L5:
	cmpl	$1, 1248(%r12)
	je	.L23
	movl	(%r12), %esi
	xorl	%ebx, %ebx
.L6:
	movl	$2, %edx
	movq	%r13, %rdi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L11
	cmpl	$1, 1248(%r12)
	leaq	hrrrandom(%rip), %rsi
	je	.L10
	movq	168(%r12), %rax
	leaq	152(%rax), %rsi
.L10:
	movl	$32, %edx
	movq	%r13, %rdi
	call	WPACKET_memcpy@PLT
	testl	%eax, %eax
	je	.L11
	movq	1296(%r12), %rsi
	movl	432(%rsi), %edx
	testl	%edx, %edx
	je	.L52
.L12:
	movq	$0, 336(%rsi)
.L13:
	testl	%ebx, %ebx
	je	.L14
	movq	1368(%r12), %rdx
	leaq	1336(%r12), %rsi
	cmpq	$32, %rdx
	ja	.L53
.L16:
	movl	$1, %ecx
	movq	%r13, %rdi
	call	WPACKET_sub_memcpy__@PLT
	testl	%eax, %eax
	je	.L18
	movq	168(%r12), %rax
	leaq	-48(%rbp), %rdx
	movq	%r13, %rsi
	movq	568(%rax), %rdi
	movq	8(%r12), %rax
	call	*152(%rax)
	testl	%eax, %eax
	jne	.L54
.L18:
	movl	$2437, %r9d
.L51:
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movl	$491, %edx
	movq	%r12, %rdi
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
.L4:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L55
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L52:
	.cfi_restore_state
	movq	1440(%r12), %rax
	testb	$2, 72(%rax)
	jne	.L13
	movl	200(%r12), %eax
	testl	%eax, %eax
	jne	.L13
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L23:
	movl	$771, %esi
	movl	$1, %ebx
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L11:
	movl	$2382, %r9d
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L14:
	movq	336(%rsi), %rdx
	addq	$344, %rsi
	cmpq	$32, %rdx
	jbe	.L16
.L53:
	movl	$2419, %r9d
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L54:
	xorl	%esi, %esi
	movl	$1, %edx
	movq	%r13, %rdi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L18
	cmpl	$1, 1248(%r12)
	movl	$2048, %edx
	je	.L19
	movq	8(%r12), %rax
	movl	$256, %edx
	movq	192(%rax), %rcx
	testb	$8, 96(%rcx)
	jne	.L19
	movl	(%rax), %eax
	cmpl	$771, %eax
	jle	.L27
	movl	$512, %edx
	cmpl	$65536, %eax
	jne	.L19
.L27:
	movl	$256, %edx
.L19:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	tls_construct_extensions@PLT
	testl	%eax, %eax
	je	.L4
	cmpl	$1, 1248(%r12)
	je	.L56
	movl	$1, %eax
	testb	$1, 1376(%r12)
	jne	.L4
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	ssl3_digest_cached_records@PLT
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L56:
	movq	1296(%r12), %rdi
	call	SSL_SESSION_free@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	$0, 1296(%r12)
	xorl	%esi, %esi
	movq	%r12, %rdi
	movl	$0, 200(%r12)
	call	create_synthetic_message_hash@PLT
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
	jmp	.L4
.L55:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1087:
	.size	tls_construct_server_hello, .-tls_construct_server_hello
	.p2align 4
	.type	create_ticket_prequel, @function
create_ticket_prequel:
.LFB1101:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	subq	$16, %rsp
	movl	200(%rdi), %eax
	testl	%eax, %eax
	je	.L58
	movq	8(%rdi), %rax
	movq	192(%rax), %rdx
	movl	96(%rdx), %esi
	andl	$8, %esi
	jne	.L65
	movl	(%rax), %eax
	cmpl	$771, %eax
	jle	.L59
	cmpl	$65536, %eax
	je	.L59
	.p2align 4,,10
	.p2align 3
.L58:
	movq	1296(%r13), %rax
	movl	480(%rax), %esi
.L59:
	movl	$4, %edx
	movq	%r12, %rdi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L84
	movq	8(%r13), %rax
	movq	192(%rax), %rdx
	testb	$8, 96(%rdx)
	jne	.L62
	movl	(%rax), %eax
	cmpl	$771, %eax
	jle	.L62
	cmpl	$65536, %eax
	jne	.L85
.L62:
	movl	$2, %esi
	movq	%r12, %rdi
	call	WPACKET_start_sub_packet_len__@PLT
	testl	%eax, %eax
	je	.L86
	movl	$1, %eax
.L57:
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L65:
	.cfi_restore_state
	xorl	%esi, %esi
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L84:
	movl	%eax, -36(%rbp)
	movl	$3822, %r9d
.L83:
	movq	%r13, %rdi
	movl	$68, %ecx
	movl	$638, %edx
	movl	$80, %esi
	leaq	.LC0(%rip), %r8
	call	ossl_statem_fatal@PLT
	movl	-36(%rbp), %eax
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L85:
	.cfi_restore_state
	movl	$4, %edx
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L64
	movl	$1, %ecx
	movl	$8, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	WPACKET_sub_memcpy__@PLT
	testl	%eax, %eax
	jne	.L62
.L64:
	movl	$3830, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movq	%r13, %rdi
	movl	$638, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L86:
	movl	%eax, -36(%rbp)
	movl	$3838, %r9d
	jmp	.L83
	.cfi_endproc
.LFE1101:
	.size	create_ticket_prequel, .-create_ticket_prequel
	.p2align 4
	.globl	tls_construct_server_key_exchange
	.type	tls_construct_server_key_exchange, @function
tls_construct_server_key_exchange:
.LFB1089:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$200, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	168(%rdi), %rax
	movq	$0, -168(%rbp)
	movq	728(%rax), %rax
	movq	%rax, -192(%rbp)
	call	EVP_MD_CTX_new@PLT
	leaq	-144(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -160(%rbp)
	movq	%rax, %r14
	call	WPACKET_get_total_written@PLT
	movl	$2506, %r9d
	movl	$68, %ecx
	leaq	.LC0(%rip), %r8
	testl	%eax, %eax
	je	.L220
	testq	%r14, %r14
	je	.L223
	movq	168(%r13), %rdx
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	568(%rdx), %rax
	movl	28(%rax), %ecx
	movaps	%xmm0, -80(%rbp)
	movl	%ecx, -180(%rbp)
	testb	$72, %cl
	je	.L224
	movl	$0, -212(%rbp)
	movq	$0, -224(%rbp)
.L91:
	testb	$68, 32(%rax)
	jne	.L151
	testl	$456, 28(%rax)
	je	.L225
.L151:
	movq	$0, -192(%rbp)
.L107:
	testl	$456, -180(%rbp)
	je	.L108
	movq	1168(%r13), %rax
	movq	512(%rax), %r15
	testq	%r15, %r15
	je	.L152
	movq	%r15, %rdi
	call	strlen@PLT
	movq	%rax, %rdx
	cmpq	$128, %rax
	ja	.L110
.L109:
	movl	$2, %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	WPACKET_sub_memcpy__@PLT
	testl	%eax, %eax
	jne	.L108
.L110:
	movl	$2687, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	.p2align 4,,10
	.p2align 3
.L220:
	movl	$492, %edx
	movl	$80, %esi
	movq	%r13, %rdi
	call	ossl_statem_fatal@PLT
.L221:
	xorl	%r15d, %r15d
.L89:
	movq	%r15, %rdi
	call	EVP_PKEY_free@PLT
	movq	-168(%rbp), %rdi
	movl	$2843, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%r14, %rdi
	call	EVP_MD_CTX_free@PLT
	xorl	%eax, %eax
.L87:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L226
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L224:
	.cfi_restore_state
	andl	$258, %ecx
	je	.L92
	movq	1168(%r13), %rax
	movl	24(%rax), %ecx
	testl	%ecx, %ecx
	jne	.L227
	movq	8(%rax), %rbx
	xorl	%r15d, %r15d
	testq	%rbx, %rbx
	je	.L228
.L96:
	movq	%rbx, %rdi
	call	EVP_PKEY_security_bits@PLT
	xorl	%ecx, %ecx
	movq	%rbx, %r8
	movl	$262151, %esi
	movl	%eax, %edx
	movq	%r13, %rdi
	call	ssl_security@PLT
	testl	%eax, %eax
	je	.L229
	movq	168(%r13), %rdx
	movl	$2572, %r9d
	cmpq	$0, 576(%rdx)
	je	.L230
.L222:
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movl	$492, %edx
	movq	%r13, %rdi
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L108:
	movl	-180(%rbp), %eax
	movq	%r13, -208(%rbp)
	xorl	%r15d, %r15d
	movl	%eax, %ebx
	andl	$258, %eax
	andl	$32, %ebx
	movl	%eax, -184(%rbp)
	leaq	-104(%rbp), %rax
	movl	%ebx, -200(%rbp)
	movq	%rax, %r13
	leaq	-96(%rbp), %rbx
.L121:
	cmpq	$0, (%rbx)
	je	.L217
	cmpl	$2, %r15d
	je	.L231
	movl	$2, %esi
	movq	%r12, %rdi
	call	WPACKET_start_sub_packet_len__@PLT
	testl	%eax, %eax
	je	.L215
.L117:
	movq	(%rbx), %rdi
	call	BN_num_bits@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	leal	14(%rax), %esi
	addl	$7, %eax
	cmovns	%eax, %esi
	sarl	$3, %esi
	movslq	%esi, %rsi
	call	WPACKET_allocate_bytes@PLT
	testl	%eax, %eax
	je	.L120
	movq	%r12, %rdi
	call	WPACKET_close@PLT
	testl	%eax, %eax
	je	.L120
	movq	(%rbx), %rdi
	movq	-104(%rbp), %rsi
	addl	$1, %r15d
	addq	$8, %rbx
	call	BN_bn2bin@PLT
	cmpl	$4, %r15d
	jne	.L121
.L217:
	movq	-208(%rbp), %r13
	testb	$-124, -180(%rbp)
	jne	.L232
.L123:
	cmpq	$0, -192(%rbp)
	je	.L128
	movq	168(%r13), %rax
	movq	736(%rax), %rax
	movq	8(%rax), %rbx
	testq	%rbx, %rbx
	je	.L131
	movq	-192(%rbp), %rdi
	leaq	-136(%rbp), %rsi
	call	tls1_lookup_md@PLT
	testl	%eax, %eax
	je	.L131
	leaq	-152(%rbp), %rsi
	movq	%r12, %rdi
	call	WPACKET_get_length@PLT
	testl	%eax, %eax
	je	.L233
	movq	8(%r13), %rax
	movq	192(%rax), %rax
	testb	$2, 96(%rax)
	je	.L133
	movq	-192(%rbp), %rax
	movl	$2, %edx
	movq	%r12, %rdi
	movzwl	8(%rax), %esi
	call	WPACKET_put_bytes__@PLT
	movl	$2790, %r9d
	testl	%eax, %eax
	je	.L219
.L133:
	movq	%rbx, %rdi
	call	EVP_PKEY_size@PLT
	leaq	-128(%rbp), %rdx
	movl	$2, %ecx
	movq	%r12, %rdi
	movslq	%eax, %rsi
	movq	%rsi, -104(%rbp)
	call	WPACKET_sub_reserve_bytes__@PLT
	testl	%eax, %eax
	je	.L135
	movq	-136(%rbp), %rdx
	xorl	%ecx, %ecx
	movq	%rbx, %r8
	movq	%r14, %rdi
	leaq	-160(%rbp), %rsi
	call	EVP_DigestSignInit@PLT
	testl	%eax, %eax
	jle	.L135
	movq	-192(%rbp), %rax
	cmpl	$912, 20(%rax)
	je	.L234
.L136:
	movq	136(%r13), %rax
	movq	-152(%rbp), %rcx
	leaq	-112(%rbp), %rsi
	movq	%r13, %rdi
	movq	-144(%rbp), %rdx
	addq	8(%rax), %rdx
	call	construct_key_exchange_tbs@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L221
	movq	-128(%rbp), %rsi
	movq	-112(%rbp), %rcx
	leaq	-104(%rbp), %rdx
	movq	%r14, %rdi
	call	EVP_DigestSign@PLT
	movq	-112(%rbp), %rdi
	movl	$2826, %edx
	leaq	.LC0(%rip), %rsi
	movl	%eax, %ebx
	call	CRYPTO_free@PLT
	testl	%ebx, %ebx
	jle	.L140
	movq	-104(%rbp), %rsi
	leaq	-120(%rbp), %rdx
	movl	$2, %ecx
	movq	%r12, %rdi
	call	WPACKET_sub_allocate_bytes__@PLT
	testl	%eax, %eax
	je	.L140
	movq	-120(%rbp), %rax
	cmpq	%rax, -128(%rbp)
	je	.L128
.L140:
	movl	$2829, %r9d
	jmp	.L219
	.p2align 4,,10
	.p2align 3
.L231:
	movl	-200(%rbp), %edx
	testl	%edx, %edx
	jne	.L235
	movl	$2, %esi
	movq	%r12, %rdi
	call	WPACKET_start_sub_packet_len__@PLT
	testl	%eax, %eax
	je	.L215
.L144:
	movl	-184(%rbp), %eax
	testl	%eax, %eax
	je	.L117
	movq	-96(%rbp), %rdi
	call	BN_num_bits@PLT
	movq	-80(%rbp), %rdi
	movl	%eax, -232(%rbp)
	call	BN_num_bits@PLT
	movl	-232(%rbp), %edx
	leal	14(%rdx), %r8d
	addl	$7, %edx
	cmovns	%edx, %r8d
	leal	14(%rax), %edx
	sarl	$3, %r8d
	addl	$7, %eax
	cmovs	%edx, %eax
	sarl	$3, %eax
	subl	%eax, %r8d
	movslq	%r8d, %r8
	testq	%r8, %r8
	je	.L117
	movq	%r8, %rsi
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%r8, -232(%rbp)
	call	WPACKET_allocate_bytes@PLT
	movq	-232(%rbp), %r8
	testl	%eax, %eax
	je	.L236
	movq	-104(%rbp), %rdi
	movq	%r8, %rdx
	xorl	%esi, %esi
	call	memset@PLT
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L92:
	testb	$-124, -180(%rbp)
	je	.L100
	movq	576(%rdx), %r15
	testq	%r15, %r15
	je	.L101
	movl	$2603, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L225:
	cmpq	$0, -192(%rbp)
	jne	.L107
	movl	$68, %ecx
	movl	$492, %edx
	movq	%r13, %rdi
	xorl	%r15d, %r15d
	movl	$2670, %r9d
	leaq	.LC0(%rip), %r8
	movl	$50, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L232:
	movl	$1, %edx
	movl	$3, %esi
	movq	%r12, %rdi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L127
	xorl	%esi, %esi
	movl	$1, %edx
	movq	%r12, %rdi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	jne	.L237
.L127:
	movl	$2756, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L235:
	movl	$1, %esi
	movq	%r12, %rdi
	call	WPACKET_start_sub_packet_len__@PLT
	testl	%eax, %eax
	jne	.L144
	.p2align 4,,10
	.p2align 3
.L215:
	movq	-208(%rbp), %r13
	movl	$2707, %r9d
	jmp	.L219
	.p2align 4,,10
	.p2align 3
.L100:
	testb	$32, -180(%rbp)
	je	.L104
	movq	2016(%r13), %rdx
	testq	%rdx, %rdx
	je	.L105
	movq	2024(%r13), %rsi
	testq	%rsi, %rsi
	je	.L105
	movq	2032(%r13), %rcx
	testq	%rcx, %rcx
	je	.L105
	movq	2040(%r13), %rdi
	testq	%rdi, %rdi
	je	.L105
	movq	%rdx, %xmm0
	movq	%rsi, %xmm1
	movq	%rdi, %xmm2
	movl	$0, -212(%rbp)
	movq	$0, -224(%rbp)
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	%rcx, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -80(%rbp)
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L131:
	movl	$2776, %r9d
.L219:
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movl	$492, %edx
	movq	%r13, %rdi
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L221
	.p2align 4,,10
	.p2align 3
.L223:
	movl	$2512, %r9d
	leaq	.LC0(%rip), %r8
	movl	$65, %ecx
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L227:
	movq	%r13, %rdi
	call	ssl_get_auto_dh@PLT
	movq	%rax, %rbx
	call	EVP_PKEY_new@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L153
	testq	%rbx, %rbx
	je	.L153
	movq	%rbx, %rdx
	movl	$28, %esi
	movq	%rax, %rdi
	movq	%r15, %rbx
	call	EVP_PKEY_assign@PLT
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L101:
	movl	$-2, %esi
	movq	%r13, %rdi
	call	tls1_shared_group@PLT
	movzwl	%ax, %eax
	movl	%eax, -212(%rbp)
	testl	%eax, %eax
	je	.L238
	movq	168(%r13), %rbx
	movq	%r13, %rdi
	movl	%eax, %esi
	call	ssl_generate_pkey_group@PLT
	movq	%rax, 576(%rbx)
	movq	168(%r13), %rax
	movq	576(%rax), %rdi
	testq	%rdi, %rdi
	je	.L89
	leaq	-168(%rbp), %rsi
	call	EVP_PKEY_get1_tls_encodedpoint@PLT
	pxor	%xmm0, %xmm0
	testq	%rax, %rax
	movq	%rax, -224(%rbp)
	je	.L239
	movq	168(%r13), %rax
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	movq	568(%rax), %rax
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L152:
	xorl	%edx, %edx
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L237:
	movl	-212(%rbp), %esi
	movl	$1, %edx
	movq	%r12, %rdi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L127
	movq	-224(%rbp), %rdx
	movq	-168(%rbp), %rsi
	movl	$1, %ecx
	movq	%r12, %rdi
	call	WPACKET_sub_memcpy__@PLT
	testl	%eax, %eax
	je	.L127
	movq	-168(%rbp), %rdi
	movl	$2761, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, -168(%rbp)
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L233:
	movl	$2783, %r9d
	jmp	.L219
	.p2align 4,,10
	.p2align 3
.L153:
	movq	%rbx, %rdi
	call	DH_free@PLT
	movl	$2537, %r9d
	jmp	.L222
	.p2align 4,,10
	.p2align 3
.L105:
	movl	$2648, %r9d
	leaq	.LC0(%rip), %r8
	movl	$358, %ecx
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L228:
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L97
	movl	$1024, %edx
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	*%rax
	movq	%rax, %rdi
	call	ssl_dh_to_pkey@PLT
	movq	%rax, %r15
	movq	%rax, %rbx
	testq	%rax, %rax
	jne	.L96
	movl	$2551, %r9d
	jmp	.L222
	.p2align 4,,10
	.p2align 3
.L230:
	movq	%rbx, %rdi
	movq	%rdx, -200(%rbp)
	call	ssl_generate_pkey@PLT
	movq	-200(%rbp), %rdx
	movq	%rax, 576(%rdx)
	movq	168(%r13), %rax
	movq	576(%rax), %rdi
	testq	%rdi, %rdi
	je	.L89
	call	EVP_PKEY_get0_DH@PLT
	movl	$2586, %r9d
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L222
	movq	%r15, %rdi
	call	EVP_PKEY_free@PLT
	leaq	-96(%rbp), %rsi
	leaq	-88(%rbp), %rcx
	xorl	%edx, %edx
	movq	%rbx, %rdi
	call	DH_get0_pqg@PLT
	leaq	-80(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	call	DH_get0_key@PLT
	movq	168(%r13), %rax
	movl	$0, -212(%rbp)
	movq	$0, -224(%rbp)
	movq	568(%rax), %rax
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L120:
	movq	-208(%rbp), %r13
	movl	$2735, %r9d
	jmp	.L219
	.p2align 4,,10
	.p2align 3
.L128:
	movq	%r14, %rdi
	call	EVP_MD_CTX_free@PLT
	movl	$1, %eax
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L97:
	movl	$2559, %r9d
	leaq	.LC0(%rip), %r8
	movl	$171, %ecx
	movq	%r13, %rdi
	movl	$492, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L229:
	movl	$2566, %r9d
	leaq	.LC0(%rip), %r8
	movl	$394, %ecx
	movq	%r13, %rdi
	movl	$492, %edx
	movl	$40, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L135:
	movl	$2804, %r9d
	jmp	.L219
	.p2align 4,,10
	.p2align 3
.L104:
	movl	$2660, %r9d
	leaq	.LC0(%rip), %r8
	movl	$250, %ecx
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L238:
	movl	$2612, %r9d
	leaq	.LC0(%rip), %r8
	movl	$315, %ecx
	movq	%r13, %rdi
	movl	$492, %edx
	movl	$40, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L89
.L234:
	movq	-160(%rbp), %rdi
	xorl	%r8d, %r8d
	movl	$6, %ecx
	movl	$4097, %edx
	movl	$-1, %esi
	call	RSA_pkey_ctx_ctrl@PLT
	testl	%eax, %eax
	jle	.L138
	movq	-160(%rbp), %rdi
	xorl	%r8d, %r8d
	movl	$-1, %ecx
	movl	$4098, %edx
	movl	$24, %esi
	call	RSA_pkey_ctx_ctrl@PLT
	testl	%eax, %eax
	jg	.L136
.L138:
	movl	$2812, %r9d
	leaq	.LC0(%rip), %r8
	movl	$6, %ecx
	movq	%r13, %rdi
	movl	$492, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L221
.L239:
	movl	$2628, %r9d
	leaq	.LC0(%rip), %r8
	movl	$16, %ecx
	movq	%r13, %rdi
	movl	$492, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L89
.L236:
	movq	-208(%rbp), %r13
	movl	$2724, %r9d
	jmp	.L219
.L226:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1089:
	.size	tls_construct_server_key_exchange, .-tls_construct_server_key_exchange
	.p2align 4
	.globl	tls_construct_certificate_request
	.type	tls_construct_certificate_request, @function
tls_construct_certificate_request:
.LFB1090:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	192(%rax), %rdx
	testb	$8, 96(%rdx)
	jne	.L241
	movl	(%rax), %eax
	cmpl	$771, %eax
	jle	.L241
	cmpl	$65536, %eax
	je	.L241
	cmpl	$3, 1936(%rdi)
	je	.L291
	xorl	%esi, %esi
	movl	$1, %edx
	movq	%r13, %rdi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L292
.L246:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$16384, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	tls_construct_extensions@PLT
	testl	%eax, %eax
	je	.L290
.L253:
	movq	168(%r12), %rax
	addl	$1, 1960(%r12)
	movl	$1, 672(%rax)
	movl	$1, %eax
	jmp	.L240
	.p2align 4,,10
	.p2align 3
.L241:
	movl	$1, %esi
	movq	%r13, %rdi
	call	WPACKET_start_sub_packet_len__@PLT
	testl	%eax, %eax
	je	.L249
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	ssl3_get_req_cert_type@PLT
	testl	%eax, %eax
	jne	.L293
.L249:
	movl	$2890, %r9d
.L289:
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movl	$372, %edx
	movq	%r12, %rdi
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
.L290:
	xorl	%eax, %eax
.L240:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L294
	addq	$40, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L293:
	.cfi_restore_state
	movq	%r13, %rdi
	call	WPACKET_close@PLT
	testl	%eax, %eax
	je	.L249
	movq	8(%r12), %rax
	movq	192(%rax), %rax
	testb	$2, 96(%rax)
	je	.L250
	movl	$1, %esi
	movq	%r12, %rdi
	leaq	-48(%rbp), %rdx
	call	tls12_get_psigalgs@PLT
	movl	$2, %esi
	movq	%r13, %rdi
	movq	%rax, %r14
	call	WPACKET_start_sub_packet_len__@PLT
	testl	%eax, %eax
	je	.L252
	movl	$1, %esi
	movq	%r13, %rdi
	call	WPACKET_set_flags@PLT
	testl	%eax, %eax
	jne	.L295
.L252:
	movl	$2903, %r9d
	jmp	.L289
	.p2align 4,,10
	.p2align 3
.L291:
	movq	1944(%rdi), %rdi
	movl	$2854, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movl	$32, %edi
	movq	$32, 1952(%r12)
	movl	$2856, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_malloc@PLT
	movq	%rax, 1944(%r12)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L245
	movl	1952(%r12), %esi
	call	RAND_bytes@PLT
	testl	%eax, %eax
	jle	.L245
	movq	1952(%r12), %rdx
	movq	1944(%r12), %rsi
	movl	$1, %ecx
	movq	%r13, %rdi
	call	WPACKET_sub_memcpy__@PLT
	testl	%eax, %eax
	je	.L245
	movq	%r12, %rdi
	call	tls13_restore_handshake_digest_for_pha@PLT
	testl	%eax, %eax
	jne	.L246
	jmp	.L290
	.p2align 4,,10
	.p2align 3
.L245:
	movl	$2859, %r9d
	jmp	.L289
	.p2align 4,,10
	.p2align 3
.L295:
	movq	-48(%rbp), %rdx
	movq	%r14, %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	tls12_copy_sigalgs@PLT
	testl	%eax, %eax
	je	.L252
	movq	%r13, %rdi
	call	WPACKET_close@PLT
	testl	%eax, %eax
	je	.L252
	.p2align 4,,10
	.p2align 3
.L250:
	movq	%r12, %rdi
	call	get_ca_names@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	construct_ca_names@PLT
	testl	%eax, %eax
	jne	.L253
	jmp	.L290
	.p2align 4,,10
	.p2align 3
.L292:
	movl	$68, %ecx
	movl	$372, %edx
	movq	%r12, %rdi
	movl	%eax, -52(%rbp)
	movl	$2871, %r9d
	leaq	.LC0(%rip), %r8
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	movl	-52(%rbp), %eax
	jmp	.L240
.L294:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1090:
	.size	tls_construct_certificate_request, .-tls_construct_certificate_request
	.section	.rodata.str1.8
	.align 8
.LC1:
	.string	"../deps/openssl/openssl/ssl/statem/../packet_local.h"
	.text
	.p2align 4
	.type	tls_process_cke_psk_preamble, @function
tls_process_cke_psk_preamble:
.LFB1091:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$272, %rsp
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	8(%rsi), %rax
	cmpq	$1, %rax
	jbe	.L297
	movq	(%rsi), %rdx
	subq	$2, %rax
	movzwl	(%rdx), %r12d
	rolw	$8, %r12w
	movzwl	%r12w, %r12d
	cmpq	%r12, %rax
	jb	.L297
	leaq	2(%rdx), %r14
	subq	%r12, %rax
	leaq	(%r14,%r12), %rdx
	movq	%rax, 8(%rsi)
	movq	%rdx, (%rsi)
	cmpq	$128, %r12
	ja	.L311
	cmpq	$0, 1416(%rdi)
	movl	$2939, %r9d
	movl	$225, %ecx
	leaq	.LC0(%rip), %r8
	je	.L310
	movq	1296(%rdi), %rbx
	movl	$449, %edx
	leaq	.LC1(%rip), %rsi
	movq	424(%rbx), %rdi
	call	CRYPTO_free@PLT
	movl	$452, %ecx
	movq	%r12, %rsi
	movq	%r14, %rdi
	leaq	.LC1(%rip), %rdx
	call	CRYPTO_strndup@PLT
	movl	$2945, %r9d
	movl	$68, %ecx
	leaq	.LC0(%rip), %r8
	movq	%rax, 424(%rbx)
	testq	%rax, %rax
	je	.L310
	movq	1296(%r13), %rax
	leaq	-304(%rbp), %r12
	movl	$256, %ecx
	movq	%r13, %rdi
	movq	%r12, %rdx
	movq	424(%rax), %rsi
	call	*1416(%r13)
	movl	$2954, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movl	%eax, %ebx
	cmpl	$256, %eax
	ja	.L310
	testq	%rbx, %rbx
	je	.L312
	movq	168(%r13), %rax
	movl	$2967, %edx
	leaq	.LC0(%rip), %rsi
	movq	712(%rax), %rdi
	call	CRYPTO_free@PLT
	movl	$2968, %ecx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	168(%r13), %r14
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_memdup@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, 712(%r14)
	call	OPENSSL_cleanse@PLT
	movq	168(%r13), %rax
	cmpq	$0, 712(%rax)
	je	.L313
	movq	%rbx, 720(%rax)
	movl	$1, %eax
	jmp	.L296
	.p2align 4,,10
	.p2align 3
.L297:
	movl	$2929, %r9d
	leaq	.LC0(%rip), %r8
	movl	$159, %ecx
.L309:
	movl	$414, %edx
	movl	$50, %esi
	movq	%r13, %rdi
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
.L296:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L314
	addq	$272, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L312:
	.cfi_restore_state
	movl	$2961, %r9d
	leaq	.LC0(%rip), %r8
	movl	$223, %ecx
	movq	%r13, %rdi
	movl	$414, %edx
	movl	$115, %esi
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	jmp	.L296
	.p2align 4,,10
	.p2align 3
.L313:
	movl	$2972, %r9d
	leaq	.LC0(%rip), %r8
	movl	$65, %ecx
	.p2align 4,,10
	.p2align 3
.L310:
	movl	$414, %edx
	movl	$80, %esi
	movq	%r13, %rdi
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	jmp	.L296
	.p2align 4,,10
	.p2align 3
.L311:
	movl	$2934, %r9d
	leaq	.LC0(%rip), %r8
	movl	$146, %ecx
	jmp	.L309
.L314:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1091:
	.size	tls_process_cke_psk_preamble, .-tls_process_cke_psk_preamble
	.p2align 4
	.globl	tls_construct_server_certificate
	.type	tls_construct_server_certificate, @function
tls_construct_server_certificate:
.LFB1100:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	168(%rdi), %rax
	movq	736(%rax), %r14
	testq	%r14, %r14
	je	.L326
	movq	8(%rdi), %rax
	movq	%rsi, %r13
	movq	192(%rax), %rdx
	testb	$8, 96(%rdx)
	jne	.L318
	movl	(%rax), %eax
	cmpl	$771, %eax
	jle	.L318
	cmpl	$65536, %eax
	jne	.L327
.L318:
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	ssl3_output_cert_chain@PLT
	testq	%rax, %rax
	setne	%al
	movzbl	%al, %eax
.L315:
	addq	$24, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L327:
	.cfi_restore_state
	xorl	%esi, %esi
	movl	$1, %edx
	movq	%r13, %rdi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	jne	.L318
	movl	$68, %ecx
	movl	$490, %edx
	movq	%r12, %rdi
	movl	%eax, -36(%rbp)
	movl	$3798, %r9d
	leaq	.LC0(%rip), %r8
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	movl	-36(%rbp), %eax
	jmp	.L315
	.p2align 4,,10
	.p2align 3
.L326:
	movl	$3788, %r9d
	movl	$68, %ecx
	movl	$490, %edx
	movl	$80, %esi
	leaq	.LC0(%rip), %r8
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	jmp	.L315
	.cfi_endproc
.LFE1100:
	.size	tls_construct_server_certificate, .-tls_construct_server_certificate
	.p2align 4
	.globl	tls_construct_new_session_ticket
	.type	tls_construct_new_session_ticket, @function
tls_construct_new_session_ticket:
.LFB1104:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$200, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -224(%rbp)
	movq	8(%rdi), %rdx
	movq	1904(%rdi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	192(%rdx), %rax
	movq	%rbx, -208(%rbp)
	movl	$0, -108(%rbp)
	movl	96(%rax), %eax
	andl	$8, %eax
	movl	%eax, -196(%rbp)
	jne	.L329
	movl	(%rdx), %eax
	cmpl	$65536, %eax
	je	.L330
	cmpl	$771, %eax
	jg	.L479
.L330:
	movq	968(%rbx), %rax
	testq	%rax, %rax
	je	.L375
.L341:
	movq	984(%rbx), %rsi
	movq	%r15, %rdi
	call	*%rax
	testl	%eax, %eax
	je	.L477
.L344:
	movq	8(%r15), %rax
	movl	-108(%rbp), %ecx
	movq	192(%rax), %rdx
	movl	%ecx, -196(%rbp)
	testb	$8, 96(%rdx)
	jne	.L469
	movl	(%rax), %eax
	cmpl	$771, %eax
	jle	.L472
	cmpl	$65536, %eax
	je	.L472
	movl	1492(%r15), %eax
	testb	$64, %ah
	jne	.L348
	movl	6176(%r15), %edx
	testl	%edx, %edx
	je	.L469
	testl	$16777216, %eax
	jne	.L469
.L348:
	movq	-224(%rbp), %rbx
	movl	-196(%rbp), %edx
	leaq	-104(%rbp), %rcx
	movq	%r15, %rdi
	movq	%rbx, %rsi
	call	create_ticket_prequel
	testl	%eax, %eax
	jne	.L480
	.p2align 4,,10
	.p2align 3
.L477:
	xorl	%r12d, %r12d
.L328:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L481
	leaq	-40(%rbp), %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L329:
	.cfi_restore_state
	movq	968(%rbx), %rax
	testq	%rax, %rax
	jne	.L341
	movl	$0, -196(%rbp)
	jmp	.L347
	.p2align 4,,10
	.p2align 3
.L472:
	movq	1904(%r15), %rbx
.L375:
	movq	%rbx, -208(%rbp)
.L347:
	movq	1296(%r15), %rdi
	xorl	%esi, %esi
	call	i2d_SSL_SESSION@PLT
	movl	%eax, %ebx
	testl	%eax, %eax
	je	.L380
	cmpl	$65280, %eax
	jg	.L380
	movslq	%eax, %r14
	movl	$3874, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r14, %rdi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L482
	call	EVP_CIPHER_CTX_new@PLT
	movq	%rax, %r13
	call	HMAC_CTX_new@PLT
	movq	%rax, -216(%rbp)
	testq	%r13, %r13
	je	.L381
	testq	%rax, %rax
	je	.L381
	leaq	-176(%rbp), %rax
	movq	1296(%r15), %rdi
	movq	%r12, -176(%rbp)
	movq	%rax, %rsi
	movq	%rax, -232(%rbp)
	call	i2d_SSL_SESSION@PLT
	testl	%eax, %eax
	je	.L483
	movq	%r14, %rdx
	leaq	-136(%rbp), %rsi
	xorl	%edi, %edi
	movq	%r12, -136(%rbp)
	call	d2i_SSL_SESSION@PLT
	movl	$3902, %r9d
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L475
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	i2d_SSL_SESSION@PLT
	movl	%eax, -200(%rbp)
	testl	%eax, %eax
	je	.L382
	cmpl	%eax, %ebx
	jl	.L382
	movq	-232(%rbp), %rsi
	movq	%r14, %rdi
	movq	%r12, -176(%rbp)
	call	i2d_SSL_SESSION@PLT
	testl	%eax, %eax
	je	.L484
	movq	%r14, %rdi
	call	SSL_SESSION_free@PLT
	movq	-208(%rbp), %rax
	movq	552(%rax), %rax
	testq	%rax, %rax
	je	.L364
	leaq	-96(%rbp), %r14
	leaq	-80(%rbp), %rsi
	movq	%r13, %rcx
	movq	%r15, %rdi
	movq	-216(%rbp), %r8
	movl	$1, %r9d
	movq	%r14, %rdx
	call	*%rax
	testl	%eax, %eax
	je	.L485
	movl	$3949, %r9d
	leaq	.LC0(%rip), %r8
	movl	$234, %ecx
	js	.L473
	movq	%r13, %rdi
	call	EVP_CIPHER_CTX_iv_length@PLT
	movl	%eax, %ebx
.L369:
	movl	-196(%rbp), %edx
	movq	-224(%rbp), %rsi
	leaq	-104(%rbp), %rcx
	movq	%r15, %rdi
	call	create_ticket_prequel
	movl	%eax, %r11d
	testl	%eax, %eax
	je	.L355
	movq	-224(%rbp), %rdi
	leaq	-128(%rbp), %rsi
	call	WPACKET_get_total_written@PLT
	testl	%eax, %eax
	je	.L373
	movq	-224(%rbp), %rdi
	leaq	-80(%rbp), %rsi
	movl	$16, %edx
	call	WPACKET_memcpy@PLT
	testl	%eax, %eax
	je	.L373
	movq	%r14, %rsi
	movq	-224(%rbp), %r14
	movslq	%ebx, %rdx
	movq	%r14, %rdi
	call	WPACKET_memcpy@PLT
	testl	%eax, %eax
	je	.L373
	movl	-200(%rbp), %eax
	leaq	-168(%rbp), %rdx
	movq	%r14, %rdi
	movq	%r14, -224(%rbp)
	leal	32(%rax), %ebx
	movslq	%ebx, %rsi
	call	WPACKET_reserve_bytes@PLT
	testl	%eax, %eax
	je	.L373
	movl	-200(%rbp), %r8d
	movq	-168(%rbp), %rsi
	movq	%r12, %rcx
	movq	%r13, %rdi
	leaq	-188(%rbp), %rdx
	call	EVP_EncryptUpdate@PLT
	testl	%eax, %eax
	je	.L373
	leaq	-160(%rbp), %r14
	movslq	-188(%rbp), %rsi
	movq	-224(%rbp), %rdi
	movq	%r14, %rdx
	call	WPACKET_allocate_bytes@PLT
	testl	%eax, %eax
	je	.L373
	movq	-168(%rbp), %rax
	cmpq	-160(%rbp), %rax
	je	.L486
	.p2align 4,,10
	.p2align 3
.L373:
	movl	$4001, %r9d
.L475:
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	jmp	.L473
	.p2align 4,,10
	.p2align 3
.L380:
	movl	$68, %ecx
	movl	$637, %edx
	movq	%r15, %rdi
	xorl	%r13d, %r13d
	movl	$3870, %r9d
	leaq	.LC0(%rip), %r8
	movl	$80, %esi
	xorl	%r12d, %r12d
	call	ossl_statem_fatal@PLT
	xorl	%r11d, %r11d
	movq	$0, -216(%rbp)
.L355:
	movl	$4015, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movl	%r11d, -196(%rbp)
	call	CRYPTO_free@PLT
	movq	%r13, %rdi
	call	EVP_CIPHER_CTX_free@PLT
	movq	-216(%rbp), %rdi
	call	HMAC_CTX_free@PLT
	movl	-196(%rbp), %r11d
	testl	%r11d, %r11d
	je	.L477
.L352:
	movq	8(%r15), %rax
	movl	$1, %r12d
	movq	192(%rax), %rdx
	testb	$8, 96(%rdx)
	jne	.L328
	movl	(%rax), %eax
	cmpl	$771, %eax
	jle	.L379
	cmpl	$65536, %eax
	jne	.L487
.L379:
	movl	$1, %r12d
	jmp	.L328
	.p2align 4,,10
	.p2align 3
.L479:
	call	ssl_handshake_md@PLT
	movq	%rax, %rdi
	movq	%rax, %r13
	call	EVP_MD_size@PLT
	movl	$4060, %r9d
	movslq	%eax, %r12
	testl	%r12d, %r12d
	js	.L476
	cmpq	$0, 6232(%r15)
	movq	1296(%r15), %r14
	jne	.L333
	movl	200(%r15), %ecx
	testl	%ecx, %ecx
	je	.L334
.L333:
	movq	%r14, %rdi
	xorl	%esi, %esi
	call	ssl_session_dup@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L477
	movq	1296(%r15), %rdi
	call	SSL_SESSION_free@PLT
	movq	%r14, 1296(%r15)
.L334:
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	ssl_generate_session_id@PLT
	testl	%eax, %eax
	je	.L477
	leaq	-108(%rbp), %rdi
	movl	$4, %esi
	call	RAND_bytes@PLT
	testl	%eax, %eax
	jle	.L488
	movq	1296(%r15), %rax
	movl	-108(%rbp), %edx
	movq	%r13, %rsi
	movq	%r15, %rdi
	leaq	-104(%rbp), %r9
	movl	$10, %r8d
	leaq	nonce_label.25141(%rip), %rcx
	movl	%edx, 576(%rax)
	addq	$80, %rax
	movq	6240(%r15), %rdx
	pushq	$1
	pushq	%r12
	bswap	%rdx
	pushq	%rax
	pushq	$8
	movq	%rdx, -104(%rbp)
	leaq	508(%r15), %rdx
	call	tls13_hkdf_expand@PLT
	addq	$32, %rsp
	testl	%eax, %eax
	je	.L477
	movq	1296(%r15), %r13
	xorl	%edi, %edi
	movq	%r12, 8(%r13)
	call	time@PLT
	movq	%rax, 488(%r13)
	movq	168(%r15), %rax
	cmpq	$0, 992(%rax)
	movq	1296(%r15), %rax
	je	.L339
	movq	584(%rax), %rdi
	movl	$4116, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	168(%r15), %rax
	movq	1296(%r15), %r12
	movl	$4118, %ecx
	leaq	.LC0(%rip), %rdx
	movq	1000(%rax), %rsi
	movq	992(%rax), %rdi
	call	CRYPTO_memdup@PLT
	movl	$4120, %r9d
	movl	$65, %ecx
	leaq	.LC0(%rip), %r8
	movq	%rax, 584(%r12)
	movq	1296(%r15), %rax
	cmpq	$0, 584(%rax)
	je	.L478
	movq	168(%r15), %rdx
	movq	1000(%rdx), %rdx
	movq	%rdx, 592(%rax)
.L339:
	movl	6176(%r15), %edx
	movl	%edx, 580(%rax)
	movq	968(%rbx), %rax
	testq	%rax, %rax
	jne	.L341
	jmp	.L344
	.p2align 4,,10
	.p2align 3
.L483:
	movl	%eax, -196(%rbp)
	movl	$3891, %r9d
.L474:
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movl	$637, %edx
	movq	%r15, %rdi
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	movl	-196(%rbp), %r11d
	jmp	.L355
	.p2align 4,,10
	.p2align 3
.L469:
	movq	1904(%r15), %rax
	movq	%rax, -208(%rbp)
	jmp	.L347
	.p2align 4,,10
	.p2align 3
.L381:
	movl	$3884, %r9d
	leaq	.LC0(%rip), %r8
	movl	$65, %ecx
.L473:
	movl	$637, %edx
	movl	$80, %esi
	movq	%r15, %rdi
	call	ossl_statem_fatal@PLT
	xorl	%r11d, %r11d
	jmp	.L355
	.p2align 4,,10
	.p2align 3
.L482:
	movl	$65, %ecx
	movl	$637, %edx
	movq	%r15, %rdi
	xorl	%r13d, %r13d
	movl	$3876, %r9d
	leaq	.LC0(%rip), %r8
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	xorl	%r11d, %r11d
	movq	$0, -216(%rbp)
	jmp	.L355
	.p2align 4,,10
	.p2align 3
.L487:
	movq	-224(%rbp), %rsi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r15, %rdi
	movl	$8192, %edx
	call	tls_construct_extensions@PLT
	testl	%eax, %eax
	je	.L477
	movdqu	6232(%r15), %xmm0
	paddq	.LC2(%rip), %xmm0
	movl	$2, %esi
	movq	%r15, %rdi
	movups	%xmm0, 6232(%r15)
	call	ssl_update_cache@PLT
	jmp	.L328
	.p2align 4,,10
	.p2align 3
.L480:
	movq	1296(%r15), %rsi
	movq	%rbx, %rdi
	movq	336(%rsi), %rdx
	addq	$344, %rsi
	call	WPACKET_memcpy@PLT
	testl	%eax, %eax
	je	.L351
	movq	-224(%rbp), %rdi
	call	WPACKET_close@PLT
	testl	%eax, %eax
	jne	.L352
.L351:
	movl	$68, %ecx
	movl	$636, %edx
	movq	%r15, %rdi
	xorl	%r12d, %r12d
	movl	$4032, %r9d
	leaq	.LC0(%rip), %r8
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L328
.L364:
	call	EVP_aes_256_cbc@PLT
	leaq	-96(%rbp), %r14
	movq	%rax, %rdi
	movq	%rax, -232(%rbp)
	call	EVP_CIPHER_iv_length@PLT
	movq	%r14, %rdi
	movl	%eax, %esi
	movl	%eax, %ebx
	call	RAND_bytes@PLT
	movq	-232(%rbp), %r9
	testl	%eax, %eax
	jle	.L371
	movq	-208(%rbp), %rax
	xorl	%edx, %edx
	movq	%r14, %r8
	movq	%r9, %rsi
	movq	%r13, %rdi
	movq	544(%rax), %rcx
	movq	%rcx, -232(%rbp)
	addq	$32, %rcx
	call	EVP_EncryptInit_ex@PLT
	testl	%eax, %eax
	je	.L371
	call	EVP_sha256@PLT
	movq	-216(%rbp), %rdi
	xorl	%r8d, %r8d
	movl	$32, %edx
	movq	%rax, %rcx
	movq	-208(%rbp), %rax
	movq	544(%rax), %rsi
	call	HMAC_Init_ex@PLT
	testl	%eax, %eax
	je	.L371
	movq	-208(%rbp), %rax
	movdqu	528(%rax), %xmm1
	movaps	%xmm1, -80(%rbp)
	jmp	.L369
	.p2align 4,,10
	.p2align 3
.L382:
	movl	$3910, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movq	%r15, %rdi
	movl	$637, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	movq	%r14, %rdi
	call	SSL_SESSION_free@PLT
	xorl	%r11d, %r11d
	jmp	.L355
	.p2align 4,,10
	.p2align 3
.L484:
	movl	$3917, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movq	%r15, %rdi
	movl	$637, %edx
	movl	$80, %esi
	movl	%eax, -196(%rbp)
	call	ossl_statem_fatal@PLT
	movq	%r14, %rdi
	call	SSL_SESSION_free@PLT
	movl	-196(%rbp), %r11d
	jmp	.L355
.L488:
	movl	$4089, %r9d
.L476:
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
.L478:
	movl	$428, %edx
	movl	$80, %esi
	movq	%r15, %rdi
	call	ossl_statem_fatal@PLT
	jmp	.L477
	.p2align 4,,10
	.p2align 3
.L485:
	movq	-224(%rbp), %rdi
	xorl	%esi, %esi
	movl	$4, %edx
	movl	%eax, -196(%rbp)
	call	WPACKET_put_bytes__@PLT
	movl	-196(%rbp), %r11d
	testl	%eax, %eax
	je	.L367
	movq	-224(%rbp), %rdi
	xorl	%esi, %esi
	movl	$2, %edx
	movl	%r11d, -196(%rbp)
	call	WPACKET_put_bytes__@PLT
	movl	-196(%rbp), %r11d
	testl	%eax, %eax
	jne	.L489
.L367:
	movl	%r11d, -196(%rbp)
	movl	$3938, %r9d
	jmp	.L474
.L371:
	movl	$3964, %r9d
	jmp	.L475
.L489:
	movl	$3943, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
	movq	%r13, %rdi
	call	EVP_CIPHER_CTX_free@PLT
	movq	-216(%rbp), %rdi
	call	HMAC_CTX_free@PLT
	jmp	.L352
.L481:
	call	__stack_chk_fail@PLT
.L486:
	movslq	-188(%rbp), %rsi
	leaq	-184(%rbp), %rdx
	movq	%r13, %rdi
	addq	%rax, %rsi
	call	EVP_EncryptFinal@PLT
	testl	%eax, %eax
	je	.L373
	movq	%r14, %rdx
	movq	-224(%rbp), %r14
	movslq	-184(%rbp), %rsi
	movq	%r14, %rdi
	call	WPACKET_allocate_bytes@PLT
	testl	%eax, %eax
	je	.L373
	movslq	-188(%rbp), %rdx
	movq	%rdx, %rax
	addq	-168(%rbp), %rdx
	cmpq	%rdx, -160(%rbp)
	jne	.L373
	addl	-184(%rbp), %eax
	cmpl	%eax, %ebx
	jl	.L373
	leaq	-120(%rbp), %rsi
	movq	%r14, %rdi
	movq	%r14, %rbx
	call	WPACKET_get_total_written@PLT
	testl	%eax, %eax
	je	.L373
	movq	-128(%rbp), %rax
	movq	-120(%rbp), %rdx
	movq	136(%r15), %rcx
	movq	-216(%rbp), %r14
	subq	%rax, %rdx
	addq	8(%rcx), %rax
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	HMAC_Update@PLT
	testl	%eax, %eax
	je	.L373
	leaq	-152(%rbp), %rdx
	movl	$64, %esi
	movq	%rbx, %rdi
	call	WPACKET_reserve_bytes@PLT
	testl	%eax, %eax
	je	.L373
	movq	-152(%rbp), %rsi
	leaq	-180(%rbp), %rdx
	movq	%r14, %rdi
	call	HMAC_Final@PLT
	testl	%eax, %eax
	je	.L373
	movl	-180(%rbp), %eax
	cmpl	$64, %eax
	ja	.L373
	leaq	-144(%rbp), %rdx
	movl	%eax, %esi
	movq	%rbx, %rdi
	call	WPACKET_allocate_bytes@PLT
	testl	%eax, %eax
	je	.L373
	movq	-144(%rbp), %rax
	cmpq	%rax, -152(%rbp)
	jne	.L373
	movq	-224(%rbp), %rdi
	call	WPACKET_close@PLT
	testl	%eax, %eax
	je	.L490
	movl	$1, %r11d
	jmp	.L355
.L490:
	movl	%eax, -196(%rbp)
	movl	$4008, %r9d
	jmp	.L474
	.cfi_endproc
.LFE1104:
	.size	tls_construct_new_session_ticket, .-tls_construct_new_session_ticket
	.p2align 4
	.globl	tls_construct_server_done
	.type	tls_construct_server_done, @function
tls_construct_server_done:
.LFB1088:
	.cfi_startproc
	endbr64
	movq	168(%rdi), %rdx
	movl	672(%rdx), %eax
	testl	%eax, %eax
	je	.L499
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L499:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ssl3_digest_cached_records@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
	ret
	.cfi_endproc
.LFE1088:
	.size	tls_construct_server_done, .-tls_construct_server_done
	.p2align 4
	.type	tls_early_post_process_client_hello, @function
tls_early_post_process_client_hello:
.LFB1083:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	1440(%rdi), %rdx
	movq	1856(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$80, -92(%rbp)
	movq	496(%rdx), %rax
	movq	$0, -80(%rbp)
	movq	$0, -72(%rbp)
	movl	$0, -88(%rbp)
	testq	%rax, %rax
	je	.L501
	movq	504(%rdx), %rdx
	leaq	-92(%rbp), %rsi
	call	*%rax
	cmpl	$-1, %eax
	je	.L502
	cmpl	$1, %eax
	je	.L501
	movl	-92(%rbp), %esi
	movl	$234, %ecx
	movl	$521, %edx
	movq	%rbx, %rdi
	movl	$1639, %r9d
	leaq	.LC0(%rip), %r8
	call	ossl_statem_fatal@PLT
.L505:
	movq	-80(%rbp), %rdi
	call	OPENSSL_sk_free@PLT
	movq	-72(%rbp), %rdi
	call	OPENSSL_sk_free@PLT
	movq	648(%r12), %rdi
	movl	$2105, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	1856(%rbx), %rdi
	movl	$2106, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	xorl	%eax, %eax
	movq	$0, 1856(%rbx)
.L500:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L684
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L501:
	.cfi_restore_state
	movq	168(%rbx), %rax
	movdqu	8(%r12), %xmm0
	movups	%xmm0, 184(%rax)
	movdqu	24(%r12), %xmm1
	movups	%xmm1, 200(%rax)
	movl	(%r12), %r13d
	testl	%r13d, %r13d
	je	.L506
	movl	4(%r12), %eax
	cmpl	$2, %eax
	je	.L507
	movl	%eax, %edx
	andl	$65280, %edx
	cmpl	$768, %edx
	jne	.L507
	movl	%eax, 1524(%rbx)
.L506:
	movq	8(%rbx), %rax
	movq	192(%rax), %rdx
	testb	$8, 96(%rdx)
	je	.L685
	cmpl	$131071, (%rax)
	je	.L519
	movl	4(%r12), %eax
	movl	(%rbx), %edx
	cmpl	$256, %eax
	je	.L513
	cmpl	$256, %edx
	movl	$65280, %ecx
	cmove	%ecx, %edx
.L514:
	cmpl	%eax, %edx
	jl	.L686
.L519:
	movq	%rbx, %rdi
	call	SSL_get_options@PLT
	testb	$32, %ah
	je	.L572
	movq	1440(%rbx), %rax
	movq	80(%r12), %rdx
	movq	208(%rax), %rax
	testq	%rax, %rax
	je	.L520
	leaq	88(%r12), %rsi
	movq	%rbx, %rdi
	call	*%rax
	movl	$1704, %r9d
	testl	%eax, %eax
	je	.L681
	movq	176(%rbx), %r13
.L523:
	movl	$1, 264(%r13)
.L572:
	movq	8(%rbx), %rax
	cmpl	$131071, (%rax)
	je	.L687
.L517:
	leaq	344(%r12), %r13
	movl	(%r12), %edx
	movq	%rbx, %rdi
	movl	$0, 200(%rbx)
	movq	%r13, %rsi
	call	ssl_cache_cipherlist@PLT
	testl	%eax, %eax
	je	.L505
	movl	(%r12), %r8d
	leaq	-72(%rbp), %rcx
	leaq	-80(%rbp), %rdx
	movq	%r13, %rsi
	movl	$1, %r9d
	movq	%rbx, %rdi
	call	bytes_to_cipher_list@PLT
	testl	%eax, %eax
	je	.L505
	movq	168(%rbx), %rax
	movq	-72(%rbp), %rdi
	xorl	%r14d, %r14d
	movl	$0, 984(%rax)
	testq	%rdi, %rdi
	jne	.L525
.L533:
	movq	8(%rbx), %rax
	movq	192(%rax), %rdx
	testb	$8, 96(%rdx)
	jne	.L527
	movl	(%rax), %eax
	cmpl	$65536, %eax
	je	.L527
	cmpl	$771, %eax
	jg	.L688
.L527:
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$128, %edx
	movl	$13, %esi
	movq	648(%r12), %rcx
	movq	%rbx, %rdi
	call	tls_parse_extension@PLT
	testl	%eax, %eax
	je	.L505
	movl	(%r12), %r10d
	testl	%r10d, %r10d
	jne	.L537
	movl	60(%rbx), %r9d
	testl	%r9d, %r9d
	je	.L538
	testb	$1, 1494(%rbx)
	jne	.L537
.L538:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	ssl_get_prev_session@PLT
	cmpl	$1, %eax
	je	.L689
	cmpl	$-1, %eax
	je	.L505
.L537:
	movl	$1, %esi
	movq	%rbx, %rdi
	call	ssl_get_new_session@PLT
	testl	%eax, %eax
	je	.L505
	movq	8(%rbx), %r13
	movq	192(%r13), %rax
	testb	$8, 96(%rax)
	jne	.L540
.L539:
	movl	0(%r13), %eax
	cmpl	$771, %eax
	jle	.L540
	cmpl	$65536, %eax
	jne	.L690
.L545:
	cmpl	$771, %eax
	jle	.L540
	cmpl	$65536, %eax
	jne	.L544
.L540:
	movl	200(%rbx), %r8d
	testl	%r8d, %r8d
	jne	.L542
.L544:
	movq	360(%r12), %rdx
	testq	%rdx, %rdx
	je	.L549
	leaq	368(%r12), %rax
	leaq	368(%r12,%rdx), %rdx
	jmp	.L551
	.p2align 4,,10
	.p2align 3
.L691:
	addq	$1, %rax
	cmpq	%rdx, %rax
	je	.L549
.L551:
	cmpb	$0, (%rax)
	jne	.L691
	testb	$64, 1492(%rbx)
	jne	.L692
.L552:
	movq	648(%r12), %rdx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	movl	$1, %r9d
	movl	$128, %esi
	call	tls_parse_all_extensions@PLT
	testl	%eax, %eax
	je	.L505
	movq	168(%rbx), %rax
	movl	-88(%rbp), %r8d
	movl	$32, %ecx
	movq	%rbx, %rdi
	movl	$1, %esi
	leaq	152(%rax), %rdx
	call	ssl_fill_hello_random@PLT
	movl	$1922, %r9d
	testl	%eax, %eax
	jle	.L682
	movq	8(%rbx), %rdx
	movl	200(%rbx), %edi
	movq	192(%rdx), %rax
	movl	96(%rax), %eax
	andl	$8, %eax
	testl	%edi, %edi
	jne	.L558
	cmpl	$768, (%rbx)
	jle	.L558
	testl	%eax, %eax
	jne	.L559
	movl	(%rdx), %eax
	cmpl	$65536, %eax
	je	.L582
	cmpl	$771, %eax
	jle	.L582
.L560:
	movq	168(%rbx), %rax
	movq	$0, 664(%rax)
.L573:
	movl	(%rdx), %eax
	cmpl	$65536, %eax
	je	.L579
	cmpl	$771, %eax
	jle	.L579
	xorl	%eax, %eax
	cmpq	$1, 360(%r12)
	jne	.L693
.L564:
	movq	1296(%rbx), %rcx
	movl	496(%rcx), %esi
	testl	%esi, %esi
	jne	.L694
	movl	200(%rbx), %ecx
	testl	%ecx, %ecx
	je	.L566
	testl	%eax, %eax
	jne	.L568
	movl	(%rdx), %eax
	cmpl	$771, %eax
	jle	.L568
	cmpl	$65536, %eax
	je	.L568
.L566:
	movq	280(%rbx), %rdi
	call	OPENSSL_sk_free@PLT
	movq	-80(%rbp), %rax
	movq	%rax, 280(%rbx)
	testq	%rax, %rax
	je	.L695
	movl	200(%rbx), %eax
	movq	$0, -80(%rbp)
	testl	%eax, %eax
	jne	.L568
	movq	1296(%rbx), %rax
	movq	%rbx, %rdi
	movl	$0, 496(%rax)
	call	tls1_set_server_sigalgs@PLT
	testl	%eax, %eax
	je	.L505
.L568:
	movq	-80(%rbp), %rdi
	call	OPENSSL_sk_free@PLT
	movq	-72(%rbp), %rdi
	call	OPENSSL_sk_free@PLT
	movq	648(%r12), %rdi
	movl	$2098, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	1856(%rbx), %rdi
	movl	$2099, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movl	$1, %eax
	movq	$0, 1856(%rbx)
	jmp	.L500
	.p2align 4,,10
	.p2align 3
.L502:
	movl	$7, 40(%rbx)
	jmp	.L500
	.p2align 4,,10
	.p2align 3
.L507:
	movl	$1659, %r9d
	leaq	.LC0(%rip), %r8
	movl	$252, %ecx
	movq	%rbx, %rdi
	movl	$521, %edx
	movl	$70, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L505
	.p2align 4,,10
	.p2align 3
.L685:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	ssl_choose_server_version@PLT
	movl	%eax, %ecx
	testl	%eax, %eax
	jne	.L510
	movq	8(%rbx), %rax
	movq	192(%rax), %rdx
	testb	$8, 96(%rdx)
	jne	.L519
	movl	(%rax), %eax
	cmpl	$65536, %eax
	je	.L517
	cmpl	$771, %eax
	jle	.L517
	leaq	2112(%rbx), %rdi
	call	RECORD_LAYER_processed_read_pending@PLT
	testl	%eax, %eax
	jne	.L696
	movq	8(%rbx), %rax
	movq	192(%rax), %rax
	testb	$8, 96(%rax)
	je	.L517
	jmp	.L519
	.p2align 4,,10
	.p2align 3
.L698:
	movl	1928(%rbx), %r11d
	testl	%r11d, %r11d
	jne	.L697
	movq	168(%rbx), %rax
	movl	$1, 984(%rax)
.L530:
	movq	-72(%rbp), %rdi
	addl	$1, %r14d
.L525:
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r14d
	jge	.L533
	movq	-72(%rbp), %rdi
	movl	%r14d, %esi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %rdi
	movq	%rax, %r13
	call	SSL_CIPHER_get_id@PLT
	cmpl	$50331903, %eax
	je	.L698
	movq	%r13, %rdi
	call	SSL_CIPHER_get_id@PLT
	cmpl	$50353664, %eax
	jne	.L530
	movq	%rbx, %rdi
	call	ssl_check_version_downgrade@PLT
	testl	%eax, %eax
	jne	.L530
	movl	$1765, %r9d
	leaq	.LC0(%rip), %r8
	movl	$373, %ecx
	movq	%rbx, %rdi
	movl	$521, %edx
	movl	$86, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L505
	.p2align 4,,10
	.p2align 3
.L686:
	movl	$396, %ecx
.L510:
	movq	168(%rbx), %rax
	cmpq	$0, 408(%rax)
	jne	.L699
.L515:
	movl	4(%r12), %eax
	movl	%eax, 1524(%rbx)
	movl	%eax, (%rbx)
.L516:
	movl	$1685, %r9d
	leaq	.LC0(%rip), %r8
	movl	$521, %edx
	movq	%rbx, %rdi
	movl	$70, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L505
	.p2align 4,,10
	.p2align 3
.L699:
	cmpq	$0, 544(%rax)
	jne	.L516
	jmp	.L515
	.p2align 4,,10
	.p2align 3
.L687:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	ssl_choose_server_version@PLT
	testl	%eax, %eax
	je	.L517
	movl	1524(%rbx), %edx
	movl	%eax, %ecx
	movl	$70, %esi
	movq	%rbx, %rdi
	movl	$1725, %r9d
	leaq	.LC0(%rip), %r8
	movl	%edx, (%rbx)
	movl	$521, %edx
	call	ossl_statem_fatal@PLT
	jmp	.L505
	.p2align 4,,10
	.p2align 3
.L520:
	movq	176(%rbx), %r13
	movq	256(%r13), %r8
	cmpq	%rdx, %r8
	jne	.L522
	leaq	88(%r12), %rdi
	movq	%r8, %rdx
	movq	%r13, %rsi
	call	memcmp@PLT
	testl	%eax, %eax
	je	.L523
.L522:
	movl	$1714, %r9d
.L681:
	leaq	.LC0(%rip), %r8
	movl	$308, %ecx
	movl	$521, %edx
	movq	%rbx, %rdi
	movl	$40, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L505
	.p2align 4,,10
	.p2align 3
.L513:
	movl	$65280, %eax
	cmpl	$256, %edx
	jne	.L514
	jmp	.L519
	.p2align 4,,10
	.p2align 3
.L697:
	movl	$1750, %r9d
	leaq	.LC0(%rip), %r8
	movl	$345, %ecx
	movq	%rbx, %rdi
	movl	$521, %edx
	movl	$40, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L505
.L689:
	movl	$1, 200(%rbx)
	movq	8(%rbx), %r13
	movq	192(%r13), %rax
	testb	$8, 96(%rax)
	je	.L539
.L542:
	movq	1296(%rbx), %rax
	xorl	%r13d, %r13d
	movq	504(%rax), %rax
	movl	24(%rax), %r14d
	jmp	.L546
	.p2align 4,,10
	.p2align 3
.L547:
	movq	-80(%rbp), %rdi
	movl	%r13d, %esi
	call	OPENSSL_sk_value@PLT
	cmpl	24(%rax), %r14d
	je	.L544
	addl	$1, %r13d
.L546:
	movq	-80(%rbp), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r13d
	jl	.L547
	movl	$1880, %r9d
	leaq	.LC0(%rip), %r8
	movl	$215, %ecx
	movq	%rbx, %rdi
	movl	$521, %edx
	movl	$47, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L505
.L688:
	movq	%rbx, %rdi
	call	SSL_get_ciphers@PLT
	movq	-80(%rbp), %rsi
	movq	%rbx, %rdi
	movq	%rax, %rdx
	call	ssl3_choose_cipher@PLT
	movl	$1779, %r9d
	testq	%rax, %rax
	je	.L683
	cmpl	$1, 1248(%rbx)
	movq	168(%rbx), %rdx
	je	.L700
.L535:
	movq	%rax, 568(%rdx)
	jmp	.L527
.L696:
	movl	$1692, %r9d
	leaq	.LC0(%rip), %r8
	movl	$182, %ecx
	movq	%rbx, %rdi
	movl	$521, %edx
	movl	$10, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L505
.L582:
	movq	1760(%rbx), %rax
	testq	%rax, %rax
	je	.L560
	movq	1296(%rbx), %rcx
	leaq	-84(%rbp), %rdx
	leaq	-64(%rbp), %r8
	movq	$0, -64(%rbp)
	movl	$256, -84(%rbp)
	movq	1768(%rbx), %r9
	movq	%rbx, %rdi
	leaq	80(%rcx), %rsi
	movq	-80(%rbp), %rcx
	call	*%rax
	testl	%eax, %eax
	je	.L562
	movslq	-84(%rbp), %rax
	testl	%eax, %eax
	jle	.L562
	movq	1296(%rbx), %rdx
	movq	%rax, 8(%rdx)
	movq	-80(%rbp), %rax
	movl	$1, 200(%rbx)
	movq	%rax, 280(%rbx)
	movq	-64(%rbp), %rax
	movq	$0, 464(%rdx)
	movq	$0, -80(%rbp)
	testq	%rax, %rax
	je	.L701
.L563:
	movq	%rax, 504(%rdx)
	movq	288(%rbx), %rdi
	call	OPENSSL_sk_free@PLT
	movq	280(%rbx), %rdi
	call	OPENSSL_sk_dup@PLT
	movq	296(%rbx), %rdi
	movq	%rax, 288(%rbx)
	call	OPENSSL_sk_free@PLT
	movq	280(%rbx), %rdi
	call	OPENSSL_sk_dup@PLT
	movq	%rax, 296(%rbx)
.L562:
	movq	8(%rbx), %rdx
	movq	192(%rdx), %rax
	movl	96(%rax), %eax
	andl	$8, %eax
.L558:
	movq	168(%rbx), %rcx
	movq	$0, 664(%rcx)
	testl	%eax, %eax
	je	.L573
	jmp	.L564
.L692:
	movq	632(%r12), %rax
	cmpq	$5, %rax
	jbe	.L552
	movq	624(%r12), %r15
	leaq	-6(%rax), %r14
	movzwl	4(%r15), %r13d
	rolw	$8, %r13w
	movzwl	%r13w, %r13d
	cmpq	%r13, %r14
	jb	.L552
	cmpw	$0, 2(%r15)
	jne	.L552
	movq	%rbx, %rdi
	call	SSL_client_version@PLT
	sarl	$8, %eax
	cmpl	$3, %eax
	je	.L553
.L555:
	movl	$18, %edx
.L554:
	movq	%r14, %rax
	xorl	%ecx, %ecx
	subq	%r13, %rax
	cmpq	%rdx, %rax
	je	.L702
.L556:
	movq	168(%rbx), %rax
	movb	%cl, 1028(%rax)
	jmp	.L552
.L549:
	movl	$1894, %r9d
	leaq	.LC0(%rip), %r8
	movl	$187, %ecx
	movq	%rbx, %rdi
	movl	$521, %edx
	movl	$50, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L505
.L690:
	movq	1856(%rbx), %rax
	leaq	1336(%rbx), %rdi
	movq	40(%rax), %rdx
	leaq	48(%rax), %rsi
	call	memcpy@PLT
	movq	1856(%rbx), %rax
	movq	40(%rax), %rax
	movq	%rax, 1368(%rbx)
	movq	192(%r13), %rax
	testb	$8, 96(%rax)
	jne	.L540
	movl	0(%r13), %eax
	jmp	.L545
.L700:
	movq	568(%rdx), %rcx
	testq	%rcx, %rcx
	je	.L536
	movl	24(%rax), %esi
	cmpl	%esi, 24(%rcx)
	je	.L535
.L536:
	movl	$1791, %r9d
	leaq	.LC0(%rip), %r8
	movl	$186, %ecx
	movq	%rbx, %rdi
	movl	$521, %edx
	movl	$47, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L505
.L579:
	xorl	%eax, %eax
	jmp	.L564
.L695:
	movl	$2076, %r9d
.L682:
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movl	$521, %edx
	movq	%rbx, %rdi
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L505
.L694:
	movl	$2061, %r9d
	leaq	.LC0(%rip), %r8
	movl	$340, %ecx
	movq	%rbx, %rdi
	movl	$521, %edx
	movl	$40, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L505
.L683:
	leaq	.LC0(%rip), %r8
	movl	$193, %ecx
	movl	$521, %edx
	movq	%rbx, %rdi
	movl	$40, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L505
.L559:
	movq	168(%rbx), %rcx
	movq	$0, 664(%rcx)
	jmp	.L564
.L684:
	call	__stack_chk_fail@PLT
.L693:
	movl	$1986, %r9d
	leaq	.LC0(%rip), %r8
	movl	$341, %ecx
	movq	%rbx, %rdi
	movl	$521, %edx
	movl	$47, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L505
.L702:
	leaq	6(%r15,%r13), %rdi
	leaq	kSafariExtensionsBlock.24850(%rip), %rsi
	call	CRYPTO_memcmp@PLT
	xorl	%ecx, %ecx
	testl	%eax, %eax
	sete	%cl
	jmp	.L556
.L553:
	movq	%rbx, %rdi
	call	SSL_client_version@PLT
	cmpl	$770, %eax
	jle	.L555
	movl	$34, %edx
	jmp	.L554
.L701:
	movq	%rbx, %rdi
	call	SSL_get_ciphers@PLT
	movq	280(%rbx), %rsi
	movq	%rbx, %rdi
	movq	%rax, %rdx
	call	ssl3_choose_cipher@PLT
	movl	$1959, %r9d
	movq	%rax, -64(%rbp)
	testq	%rax, %rax
	je	.L683
	movq	1296(%rbx), %rdx
	jmp	.L563
	.cfi_endproc
.LFE1083:
	.size	tls_early_post_process_client_hello, .-tls_early_post_process_client_hello
	.p2align 4
	.globl	tls_construct_cert_status
	.type	tls_construct_cert_status, @function
tls_construct_cert_status:
.LFB1106:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movl	1608(%rdi), %esi
	movq	%rdi, %r12
	movq	%r13, %rdi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L706
	movq	1656(%r12), %rdx
	movq	1648(%r12), %rsi
	movl	$3, %ecx
	movq	%r13, %rdi
	call	WPACKET_sub_memcpy__@PLT
	testl	%eax, %eax
	je	.L706
	popq	%r12
	movl	$1, %eax
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L706:
	.cfi_restore_state
	movq	%r12, %rdi
	movl	$4184, %r9d
	movl	$68, %ecx
	movl	$494, %edx
	leaq	.LC0(%rip), %r8
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	popq	%r12
	xorl	%eax, %eax
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1106:
	.size	tls_construct_cert_status, .-tls_construct_cert_status
	.p2align 4
	.globl	dtls_construct_hello_verify_request
	.type	dtls_construct_hello_verify_request, @function
dtls_construct_hello_verify_request:
.LFB1080:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	1440(%rdi), %rax
	movq	200(%rax), %rax
	testq	%rax, %rax
	je	.L714
	movq	%rsi, %r13
	leaq	-44(%rbp), %rdx
	movq	176(%rdi), %rsi
	call	*%rax
	testl	%eax, %eax
	je	.L714
	movl	-44(%rbp), %eax
	cmpl	$255, %eax
	jbe	.L726
.L714:
	movl	$1303, %r9d
	leaq	.LC0(%rip), %r8
	movl	$400, %ecx
.L725:
	movl	$385, %edx
	movl	$-1, %esi
	movq	%r12, %rdi
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
.L711:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L727
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L726:
	.cfi_restore_state
	movl	%eax, %r14d
	movl	$2, %edx
	movl	$65279, %esi
	movq	%r13, %rdi
	movq	176(%r12), %r15
	movq	%r14, 256(%r15)
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	jne	.L715
.L716:
	movl	$1311, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	jmp	.L725
	.p2align 4,,10
	.p2align 3
.L715:
	movl	$1, %ecx
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	WPACKET_sub_memcpy__@PLT
	testl	%eax, %eax
	je	.L716
	movl	$1, %eax
	jmp	.L711
.L727:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1080:
	.size	dtls_construct_hello_verify_request, .-dtls_construct_hello_verify_request
	.p2align 4
	.globl	d2i_GOST_KX_MESSAGE
	.type	d2i_GOST_KX_MESSAGE, @function
d2i_GOST_KX_MESSAGE:
.LFB1061:
	.cfi_startproc
	endbr64
	leaq	GOST_KX_MESSAGE_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE1061:
	.size	d2i_GOST_KX_MESSAGE, .-d2i_GOST_KX_MESSAGE
	.p2align 4
	.globl	i2d_GOST_KX_MESSAGE
	.type	i2d_GOST_KX_MESSAGE, @function
i2d_GOST_KX_MESSAGE:
.LFB1062:
	.cfi_startproc
	endbr64
	leaq	GOST_KX_MESSAGE_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE1062:
	.size	i2d_GOST_KX_MESSAGE, .-i2d_GOST_KX_MESSAGE
	.p2align 4
	.globl	GOST_KX_MESSAGE_new
	.type	GOST_KX_MESSAGE_new, @function
GOST_KX_MESSAGE_new:
.LFB1063:
	.cfi_startproc
	endbr64
	leaq	GOST_KX_MESSAGE_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE1063:
	.size	GOST_KX_MESSAGE_new, .-GOST_KX_MESSAGE_new
	.p2align 4
	.globl	GOST_KX_MESSAGE_free
	.type	GOST_KX_MESSAGE_free, @function
GOST_KX_MESSAGE_free:
.LFB1064:
	.cfi_startproc
	endbr64
	leaq	GOST_KX_MESSAGE_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE1064:
	.size	GOST_KX_MESSAGE_free, .-GOST_KX_MESSAGE_free
	.p2align 4
	.globl	ossl_statem_server_read_transition
	.type	ossl_statem_server_read_transition, @function
ossl_statem_server_read_transition:
.LFB1066:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	8(%rdi), %r8
	movq	192(%r8), %rax
	movl	96(%rax), %edx
	movl	92(%rdi), %eax
	movl	%edx, %ecx
	andl	$8, %ecx
	jne	.L733
	movl	(%r8), %edx
	cmpl	$65536, %edx
	je	.L734
	cmpl	$771, %edx
	jg	.L806
.L734:
	cmpl	$36, %eax
	ja	.L736
	leaq	.L768(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L768:
	.long	.L748-.L768
	.long	.L748-.L768
	.long	.L736-.L768
	.long	.L736-.L768
	.long	.L736-.L768
	.long	.L736-.L768
	.long	.L736-.L768
	.long	.L736-.L768
	.long	.L736-.L768
	.long	.L736-.L768
	.long	.L736-.L768
	.long	.L736-.L768
	.long	.L736-.L768
	.long	.L736-.L768
	.long	.L736-.L768
	.long	.L736-.L768
	.long	.L736-.L768
	.long	.L736-.L768
	.long	.L736-.L768
	.long	.L736-.L768
	.long	.L736-.L768
	.long	.L748-.L768
	.long	.L736-.L768
	.long	.L736-.L768
	.long	.L736-.L768
	.long	.L736-.L768
	.long	.L751-.L768
	.long	.L755-.L768
	.long	.L756-.L768
	.long	.L757-.L768
	.long	.L763-.L768
	.long	.L762-.L768
	.long	.L736-.L768
	.long	.L736-.L768
	.long	.L736-.L768
	.long	.L736-.L768
	.long	.L757-.L768
	.text
.L762:
	movq	168(%rdi), %rax
	movl	988(%rax), %eax
	testl	%eax, %eax
	jne	.L807
.L763:
	cmpl	$20, %esi
	je	.L805
.L750:
	testl	%ecx, %ecx
	je	.L736
.L766:
	cmpl	$257, %esi
	je	.L808
.L736:
	movl	$311, %r9d
	movl	$244, %ecx
	movl	$418, %edx
	movl	$10, %esi
	leaq	.LC0(%rip), %r8
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
.L732:
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L733:
	.cfi_restore_state
	cmpl	$36, %eax
	ja	.L766
	leaq	.L767(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L767:
	.long	.L748-.L767
	.long	.L748-.L767
	.long	.L766-.L767
	.long	.L766-.L767
	.long	.L766-.L767
	.long	.L766-.L767
	.long	.L766-.L767
	.long	.L766-.L767
	.long	.L766-.L767
	.long	.L766-.L767
	.long	.L766-.L767
	.long	.L766-.L767
	.long	.L766-.L767
	.long	.L766-.L767
	.long	.L766-.L767
	.long	.L766-.L767
	.long	.L766-.L767
	.long	.L766-.L767
	.long	.L766-.L767
	.long	.L766-.L767
	.long	.L766-.L767
	.long	.L748-.L767
	.long	.L766-.L767
	.long	.L766-.L767
	.long	.L766-.L767
	.long	.L766-.L767
	.long	.L751-.L767
	.long	.L755-.L767
	.long	.L756-.L767
	.long	.L757-.L767
	.long	.L763-.L767
	.long	.L762-.L767
	.long	.L766-.L767
	.long	.L766-.L767
	.long	.L766-.L767
	.long	.L766-.L767
	.long	.L757-.L767
	.text
.L756:
	movq	1296(%rdi), %rax
	cmpq	$0, 440(%rax)
	je	.L757
	movl	116(%rdi), %edx
	testl	%edx, %edx
	je	.L758
.L757:
	cmpl	$257, %esi
	jne	.L736
	movl	$31, 92(%rdi)
	movl	$1, %eax
	jmp	.L732
.L748:
	cmpl	$1, %esi
	jne	.L750
.L749:
	movl	$20, 92(%rdi)
	movl	$1, %eax
	jmp	.L732
.L751:
	movq	168(%rdi), %rax
	movl	672(%rax), %eax
	cmpl	$16, %esi
	je	.L809
	cmpl	$11, %esi
	jne	.L750
	testl	%eax, %eax
	je	.L750
	jmp	.L747
.L755:
	cmpl	$16, %esi
	jne	.L750
.L753:
	movl	$28, 92(%rdi)
	movl	$1, %eax
	jmp	.L732
	.p2align 4,,10
	.p2align 3
.L806:
	cmpl	$1, %eax
	je	.L735
	subl	$27, %eax
	cmpl	$22, %eax
	ja	.L736
	leaq	.L738(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L738:
	.long	.L741-.L738
	.long	.L736-.L738
	.long	.L744-.L738
	.long	.L736-.L738
	.long	.L736-.L738
	.long	.L736-.L738
	.long	.L736-.L738
	.long	.L736-.L738
	.long	.L736-.L738
	.long	.L737-.L738
	.long	.L736-.L738
	.long	.L736-.L738
	.long	.L736-.L738
	.long	.L736-.L738
	.long	.L736-.L738
	.long	.L736-.L738
	.long	.L736-.L738
	.long	.L736-.L738
	.long	.L736-.L738
	.long	.L739-.L738
	.long	.L736-.L738
	.long	.L736-.L738
	.long	.L737-.L738
	.text
	.p2align 4,,10
	.p2align 3
.L808:
	movq	$0, 152(%rdi)
	movl	$3, 40(%rdi)
	call	SSL_get_rbio@PLT
	movl	$15, %esi
	movq	%rax, %r12
	movq	%rax, %rdi
	call	BIO_clear_flags@PLT
	movl	$9, %esi
	movq	%r12, %rdi
	call	BIO_set_flags@PLT
	xorl	%eax, %eax
	jmp	.L732
.L744:
	cmpl	$20, %esi
	jne	.L736
.L805:
	movl	$32, 92(%rdi)
	movl	$1, %eax
	jmp	.L732
.L737:
	movq	168(%rdi), %rax
	movl	672(%rax), %ecx
	testl	%ecx, %ecx
	je	.L744
	cmpl	$11, %esi
	jne	.L736
.L747:
	movl	$27, 92(%rdi)
	movl	$1, %eax
	jmp	.L732
.L741:
	movq	1296(%rdi), %rax
	cmpq	$0, 440(%rax)
	je	.L744
	cmpl	$15, %esi
	jne	.L736
.L759:
	movl	$29, 92(%rdi)
	movl	$1, %eax
	jmp	.L732
.L739:
	cmpl	$1, 1248(%rdi)
	je	.L810
	cmpl	$2, 1816(%rdi)
	jne	.L737
	cmpl	$5, %esi
	jne	.L736
	movl	$49, 92(%rdi)
	movl	$1, %eax
	jmp	.L732
	.p2align 4,,10
	.p2align 3
.L807:
	cmpl	$67, %esi
	jne	.L750
	movl	$30, 92(%rdi)
	movl	$1, %eax
	jmp	.L732
	.p2align 4,,10
	.p2align 3
.L758:
	cmpl	$15, %esi
	jne	.L750
	jmp	.L759
	.p2align 4,,10
	.p2align 3
.L809:
	testl	%eax, %eax
	je	.L753
	cmpl	$768, (%rdi)
	jne	.L736
	movl	1376(%rdi), %eax
	andl	$3, %eax
	cmpl	$3, %eax
	jne	.L753
	movl	$198, %r9d
	movl	$199, %ecx
	movl	$418, %edx
	movl	$40, %esi
	leaq	.LC0(%rip), %r8
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	jmp	.L732
	.p2align 4,,10
	.p2align 3
.L735:
	cmpl	$11, 132(%rdi)
	je	.L736
	cmpl	$11, %esi
	je	.L811
	cmpl	$24, %esi
	jne	.L736
	movl	$44, 92(%rdi)
	movl	$1, %eax
	jmp	.L732
.L810:
	cmpl	$1, %esi
	jne	.L736
	jmp	.L749
.L811:
	cmpl	$4, 1936(%rdi)
	jne	.L736
	jmp	.L747
	.cfi_endproc
.LFE1066:
	.size	ossl_statem_server_read_transition, .-ossl_statem_server_read_transition
	.p2align 4
	.globl	send_certificate_request
	.type	send_certificate_request, @function
send_certificate_request:
.LFB1068:
	.cfi_startproc
	endbr64
	movl	1376(%rdi), %edx
	xorl	%eax, %eax
	testb	$1, %dl
	je	.L812
	movq	8(%rdi), %rcx
	movq	192(%rcx), %rsi
	testb	$8, 96(%rsi)
	je	.L830
.L814:
	movl	1960(%rdi), %eax
	testl	%eax, %eax
	jle	.L815
	xorl	%eax, %eax
	testb	$4, %dl
	jne	.L812
.L815:
	movq	168(%rdi), %rax
	movq	568(%rax), %rax
	movl	32(%rax), %ecx
	testb	$4, %cl
	jne	.L831
.L816:
	xorl	%eax, %eax
	andl	$80, %ecx
	sete	%al
.L812:
	ret
	.p2align 4,,10
	.p2align 3
.L830:
	movl	(%rcx), %ecx
	cmpl	$771, %ecx
	jle	.L814
	cmpl	$65536, %ecx
	je	.L814
	testb	$8, %dl
	je	.L814
	cmpl	$3, 1936(%rdi)
	je	.L814
	ret
	.p2align 4,,10
	.p2align 3
.L831:
	xorl	%eax, %eax
	andl	$2, %edx
	je	.L812
	jmp	.L816
	.cfi_endproc
.LFE1068:
	.size	send_certificate_request, .-send_certificate_request
	.p2align 4
	.globl	ossl_statem_server_write_transition
	.type	ossl_statem_server_write_transition, @function
ossl_statem_server_write_transition:
.LFB1070:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	8(%rdi), %rcx
	movl	92(%rdi), %edx
	movq	192(%rcx), %rax
	movl	96(%rax), %eax
	andl	$8, %eax
	jne	.L833
	movl	(%rcx), %ecx
	cmpl	$65536, %ecx
	je	.L834
	cmpl	$771, %ecx
	jg	.L924
.L834:
	cmpl	$36, %edx
	ja	.L861
	leaq	.L886(%rip), %rcx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L886:
	.long	.L890-.L886
	.long	.L862-.L886
	.long	.L861-.L886
	.long	.L861-.L886
	.long	.L861-.L886
	.long	.L861-.L886
	.long	.L861-.L886
	.long	.L861-.L886
	.long	.L861-.L886
	.long	.L861-.L886
	.long	.L861-.L886
	.long	.L861-.L886
	.long	.L861-.L886
	.long	.L861-.L886
	.long	.L861-.L886
	.long	.L861-.L886
	.long	.L861-.L886
	.long	.L861-.L886
	.long	.L861-.L886
	.long	.L884-.L886
	.long	.L866-.L886
	.long	.L890-.L886
	.long	.L868-.L886
	.long	.L875-.L886
	.long	.L878-.L886
	.long	.L879-.L886
	.long	.L890-.L886
	.long	.L861-.L886
	.long	.L861-.L886
	.long	.L861-.L886
	.long	.L861-.L886
	.long	.L861-.L886
	.long	.L880-.L886
	.long	.L882-.L886
	.long	.L876-.L886
	.long	.L839-.L886
	.long	.L883-.L886
	.text
.L850:
	cmpl	$3, 1936(%r12)
	je	.L873
.L890:
	movl	$2, %eax
.L832:
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L833:
	.cfi_restore_state
	cmpl	$36, %edx
	ja	.L861
	leaq	.L885(%rip), %rcx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L885:
	.long	.L890-.L885
	.long	.L862-.L885
	.long	.L861-.L885
	.long	.L861-.L885
	.long	.L861-.L885
	.long	.L861-.L885
	.long	.L861-.L885
	.long	.L861-.L885
	.long	.L861-.L885
	.long	.L861-.L885
	.long	.L861-.L885
	.long	.L861-.L885
	.long	.L861-.L885
	.long	.L861-.L885
	.long	.L861-.L885
	.long	.L861-.L885
	.long	.L861-.L885
	.long	.L861-.L885
	.long	.L861-.L885
	.long	.L884-.L885
	.long	.L864-.L885
	.long	.L890-.L885
	.long	.L868-.L885
	.long	.L875-.L885
	.long	.L878-.L885
	.long	.L879-.L885
	.long	.L890-.L885
	.long	.L861-.L885
	.long	.L861-.L885
	.long	.L861-.L885
	.long	.L861-.L885
	.long	.L861-.L885
	.long	.L880-.L885
	.long	.L882-.L885
	.long	.L876-.L885
	.long	.L839-.L885
	.long	.L883-.L885
	.text
	.p2align 4,,10
	.p2align 3
.L924:
	cmpl	$46, %edx
	ja	.L835
	leaq	.L837(%rip), %rdi
	movl	%edx, %esi
	movslq	(%rdi,%rsi,4), %rcx
	addq	%rdi, %rcx
	notrack jmp	*%rcx
	.section	.rodata
	.align 4
	.align 4
.L837:
	.long	.L835-.L837
	.long	.L849-.L837
	.long	.L835-.L837
	.long	.L835-.L837
	.long	.L835-.L837
	.long	.L835-.L837
	.long	.L835-.L837
	.long	.L835-.L837
	.long	.L835-.L837
	.long	.L835-.L837
	.long	.L835-.L837
	.long	.L835-.L837
	.long	.L835-.L837
	.long	.L835-.L837
	.long	.L835-.L837
	.long	.L835-.L837
	.long	.L835-.L837
	.long	.L835-.L837
	.long	.L835-.L837
	.long	.L835-.L837
	.long	.L867-.L837
	.long	.L835-.L837
	.long	.L847-.L837
	.long	.L846-.L837
	.long	.L835-.L837
	.long	.L845-.L837
	.long	.L835-.L837
	.long	.L835-.L837
	.long	.L835-.L837
	.long	.L835-.L837
	.long	.L835-.L837
	.long	.L835-.L837
	.long	.L844-.L837
	.long	.L843-.L837
	.long	.L835-.L837
	.long	.L842-.L837
	.long	.L841-.L837
	.long	.L840-.L837
	.long	.L835-.L837
	.long	.L835-.L837
	.long	.L839-.L837
	.long	.L835-.L837
	.long	.L884-.L837
	.long	.L835-.L837
	.long	.L884-.L837
	.long	.L835-.L837
	.long	.L890-.L837
	.text
.L883:
	movl	200(%r12), %eax
	testl	%eax, %eax
	jne	.L890
.L884:
	movl	$1, 92(%r12)
	addq	$24, %rsp
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L875:
	.cfi_restore_state
	movl	1628(%r12), %ecx
	testl	%ecx, %ecx
	jne	.L925
.L876:
	movq	168(%r12), %rax
	movq	568(%rax), %rax
.L923:
	movl	28(%rax), %eax
	testb	$6, %al
	jne	.L871
	testb	$72, %al
	jne	.L926
.L877:
	testl	$416, %eax
	jne	.L871
.L878:
	movq	%r12, %rdi
	call	send_certificate_request
	testl	%eax, %eax
	jne	.L873
.L879:
	movl	$26, 92(%r12)
	addq	$24, %rsp
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L880:
	.cfi_restore_state
	movl	200(%r12), %edx
	testl	%edx, %edx
	jne	.L884
.L881:
	movl	1664(%r12), %esi
	testl	%esi, %esi
	jne	.L870
.L882:
	movl	$35, 92(%r12)
.L915:
	addq	$24, %rsp
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L839:
	.cfi_restore_state
	movl	$36, 92(%r12)
	movl	$1, %eax
	jmp	.L832
.L861:
	movl	$556, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movq	%r12, %rdi
	movl	$604, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	jmp	.L832
.L862:
	cmpl	$19, 96(%r12)
	je	.L927
	movq	%r12, %rdi
	call	tls_setup_handshake@PLT
	movl	%eax, %r8d
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jne	.L890
	jmp	.L832
.L868:
	movl	200(%r12), %edi
	testl	%edi, %edi
	jne	.L881
	movq	168(%r12), %rax
	movq	568(%rax), %rax
	testb	$84, 32(%rax)
	je	.L857
	jmp	.L923
.L866:
	movl	1928(%r12), %r8d
	testl	%r8d, %r8d
	jne	.L867
	movq	168(%r12), %rax
	cmpq	$0, 408(%rax)
	je	.L867
	cmpq	$0, 544(%rax)
	jne	.L884
.L867:
	movl	$22, 92(%r12)
	movl	$1, %eax
	jmp	.L832
	.p2align 4,,10
	.p2align 3
.L926:
	movq	1168(%r12), %rdx
	cmpq	$0, 512(%rdx)
	je	.L877
.L871:
	movl	$24, 92(%r12)
	movl	$1, %eax
	jmp	.L832
.L864:
	movq	176(%r12), %rax
	movl	264(%rax), %r9d
	testl	%r9d, %r9d
	jne	.L866
	movq	%r12, %rdi
	call	SSL_get_options@PLT
	testb	$32, %ah
	je	.L866
	movl	$21, 92(%r12)
	movl	$1, %eax
	jmp	.L832
.L852:
	cmpl	$1, %eax
	jne	.L853
.L841:
	movl	$46, 92(%r12)
	movl	$1, %eax
	jmp	.L832
.L847:
	movl	1248(%r12), %eax
	testb	$16, 1494(%r12)
	je	.L852
	cmpl	$2, %eax
	jne	.L882
.L853:
	movl	$37, 92(%r12)
	movl	$1, %eax
	jmp	.L832
.L842:
	cmpl	$1, 1248(%r12)
	jne	.L853
	jmp	.L841
.L846:
	movl	$40, 92(%r12)
	movl	$1, %eax
	jmp	.L832
.L845:
	cmpl	$3, 1936(%r12)
	je	.L928
	.p2align 4,,10
	.p2align 3
.L857:
	movl	$23, 92(%r12)
	movl	$1, %eax
	jmp	.L832
.L844:
	cmpl	$4, 1936(%r12)
	je	.L929
	movl	1664(%r12), %r11d
	testl	%r11d, %r11d
	je	.L884
.L859:
	movq	6232(%r12), %rax
	cmpq	%rax, 6224(%r12)
	jbe	.L884
	.p2align 4,,10
	.p2align 3
.L870:
	movl	$33, 92(%r12)
	movl	$1, %eax
	jmp	.L832
.L849:
	cmpl	$-1, 1932(%r12)
	je	.L850
	movl	$42, 92(%r12)
	movl	%edx, %eax
	jmp	.L832
.L840:
	movl	200(%r12), %eax
	testl	%eax, %eax
	jne	.L839
	movq	%r12, %rdi
	call	send_certificate_request
	testl	%eax, %eax
	je	.L857
	.p2align 4,,10
	.p2align 3
.L873:
	movl	$25, 92(%r12)
	movl	$1, %eax
	jmp	.L832
.L843:
	movl	200(%r12), %r10d
	testl	%r10d, %r10d
	jne	.L884
	movq	6232(%r12), %rax
	cmpq	%rax, 6224(%r12)
	jbe	.L884
	jmp	.L915
.L835:
	movl	$68, %ecx
	movl	$600, %edx
	movq	%r12, %rdi
	movl	%eax, -20(%rbp)
	movl	$425, %r9d
	leaq	.LC0(%rip), %r8
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	movl	-20(%rbp), %eax
	jmp	.L832
	.p2align 4,,10
	.p2align 3
.L927:
	movq	$19, 92(%r12)
	movl	$1, %eax
	jmp	.L832
	.p2align 4,,10
	.p2align 3
.L925:
	movl	$34, 92(%r12)
	movl	$1, %eax
	jmp	.L832
.L929:
	movl	$2, 1936(%r12)
	jmp	.L859
.L928:
	movl	$1, 92(%r12)
	movl	$1, %eax
	movl	$4, 1936(%r12)
	jmp	.L832
	.cfi_endproc
.LFE1070:
	.size	ossl_statem_server_write_transition, .-ossl_statem_server_write_transition
	.p2align 4
	.globl	ossl_statem_server_pre_work
	.type	ossl_statem_server_pre_work, @function
ossl_statem_server_pre_work:
.LFB1071:
	.cfi_startproc
	endbr64
	cmpl	$46, 92(%rdi)
	ja	.L969
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.L933(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movl	92(%rdi), %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L933:
	.long	.L947-.L933
	.long	.L939-.L933
	.long	.L947-.L933
	.long	.L947-.L933
	.long	.L947-.L933
	.long	.L947-.L933
	.long	.L947-.L933
	.long	.L947-.L933
	.long	.L947-.L933
	.long	.L947-.L933
	.long	.L947-.L933
	.long	.L947-.L933
	.long	.L947-.L933
	.long	.L947-.L933
	.long	.L947-.L933
	.long	.L947-.L933
	.long	.L947-.L933
	.long	.L947-.L933
	.long	.L947-.L933
	.long	.L938-.L933
	.long	.L947-.L933
	.long	.L937-.L933
	.long	.L936-.L933
	.long	.L947-.L933
	.long	.L947-.L933
	.long	.L947-.L933
	.long	.L947-.L933
	.long	.L947-.L933
	.long	.L947-.L933
	.long	.L947-.L933
	.long	.L947-.L933
	.long	.L947-.L933
	.long	.L947-.L933
	.long	.L935-.L933
	.long	.L947-.L933
	.long	.L934-.L933
	.long	.L947-.L933
	.long	.L947-.L933
	.long	.L947-.L933
	.long	.L947-.L933
	.long	.L947-.L933
	.long	.L947-.L933
	.long	.L947-.L933
	.long	.L947-.L933
	.long	.L947-.L933
	.long	.L947-.L933
	.long	.L932-.L933
	.text
	.p2align 4,,10
	.p2align 3
.L938:
	movq	8(%rdi), %rax
	movl	$0, 68(%rdi)
	movq	192(%rax), %rax
	testb	$8, 96(%rax)
	jne	.L940
	.p2align 4,,10
	.p2align 3
.L947:
	movl	$2, %eax
.L930:
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L932:
	.cfi_restore_state
	cmpl	$9, 132(%rdi)
	je	.L939
	movq	168(%rdi), %rax
	movq	(%rax), %rax
	testb	$8, %ah
	je	.L947
.L939:
	leave
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	movl	$1, %ecx
	movl	$1, %edx
	jmp	tls_finish_handshake@PLT
	.p2align 4,,10
	.p2align 3
.L969:
	movl	$2, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L937:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	movq	8(%rdi), %rax
	movl	$0, 68(%rdi)
	movq	192(%rax), %rax
	testb	$8, 96(%rax)
	je	.L947
	movq	%rdi, -8(%rbp)
	call	dtls1_clear_sent_buffer@PLT
	movq	-8(%rbp), %rdi
	movl	$2, %eax
	movl	$0, 120(%rdi)
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L936:
	.cfi_restore_state
	movq	8(%rdi), %rax
	movq	192(%rax), %rax
	testb	$8, 96(%rax)
	je	.L947
	movl	$1, 120(%rdi)
	movl	$2, %eax
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L935:
	.cfi_restore_state
	movq	8(%rdi), %rax
	movq	192(%rax), %rdx
	testb	$8, 96(%rdx)
	jne	.L942
	movl	(%rax), %eax
	cmpl	$771, %eax
	jle	.L947
	cmpl	$65536, %eax
	je	.L947
	cmpq	$0, 6232(%rdi)
	jne	.L947
	leave
	.cfi_remember_state
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	jmp	tls_finish_handshake@PLT
	.p2align 4,,10
	.p2align 3
.L934:
	.cfi_restore_state
	movq	8(%rdi), %rdx
	movq	192(%rdx), %rax
	testb	$8, 96(%rax)
	jne	.L943
	movl	(%rdx), %edx
	cmpl	$771, %edx
	jle	.L943
	cmpl	$65536, %edx
	jne	.L947
.L943:
	movq	1296(%rdi), %rcx
	movq	168(%rdi), %rsi
	movq	504(%rcx), %rdx
	movq	568(%rsi), %rsi
	testq	%rdx, %rdx
	je	.L973
	cmpq	%rsi, %rdx
	jne	.L974
.L946:
	movq	%rdi, -8(%rbp)
	call	*16(%rax)
	movl	%eax, %r8d
	xorl	%eax, %eax
	testl	%r8d, %r8d
	je	.L930
	movq	-8(%rbp), %rdi
	movq	8(%rdi), %rax
	movq	192(%rax), %rax
	testb	$8, 96(%rax)
	je	.L947
.L942:
	movl	$0, 120(%rdi)
	movl	$2, %eax
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L940:
	.cfi_restore_state
	call	dtls1_clear_sent_buffer@PLT
	movl	$2, %eax
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L974:
	.cfi_restore_state
	movl	$750, %r9d
	movl	$68, %ecx
	movl	$640, %edx
	movl	$80, %esi
	leaq	.LC0(%rip), %r8
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L973:
	.cfi_restore_state
	movq	%rsi, 504(%rcx)
	jmp	.L946
	.cfi_endproc
.LFE1071:
	.size	ossl_statem_server_pre_work, .-ossl_statem_server_pre_work
	.p2align 4
	.globl	ossl_statem_server_post_work
	.type	ossl_statem_server_post_work, @function
ossl_statem_server_post_work:
.LFB1073:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	subq	$16, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	92(%rdi), %eax
	movq	$0, 152(%rdi)
	subl	$19, %eax
	cmpl	$23, %eax
	ja	.L1073
	leaq	.L978(%rip), %rdx
	movq	%rdi, %r12
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L978:
	.long	.L986-.L978
	.long	.L1073-.L978
	.long	.L985-.L978
	.long	.L984-.L978
	.long	.L1073-.L978
	.long	.L1073-.L978
	.long	.L983-.L978
	.long	.L982-.L978
	.long	.L1073-.L978
	.long	.L1073-.L978
	.long	.L1073-.L978
	.long	.L1073-.L978
	.long	.L1073-.L978
	.long	.L1073-.L978
	.long	.L981-.L978
	.long	.L1073-.L978
	.long	.L980-.L978
	.long	.L979-.L978
	.long	.L1073-.L978
	.long	.L1073-.L978
	.long	.L1073-.L978
	.long	.L1073-.L978
	.long	.L1073-.L978
	.long	.L977-.L978
	.text
	.p2align 4,,10
	.p2align 3
.L980:
	cmpl	$1, 1248(%rdi)
	je	.L997
	movq	8(%rdi), %rdx
	movq	192(%rdx), %rax
	testb	$8, 96(%rax)
	je	.L996
.L998:
	movl	$34, %esi
	movq	%r12, %rdi
	call	*32(%rax)
	testl	%eax, %eax
	je	.L991
	movq	8(%r12), %rax
	movq	192(%rax), %rax
	testb	$8, 96(%rax)
	je	.L1073
	movl	$2, %esi
	movq	%r12, %rdi
	call	dtls1_reset_seq_numbers@PLT
	.p2align 4,,10
	.p2align 3
.L1073:
	movl	$2, %r13d
.L975:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1074
	addq	$16, %rsp
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1075:
	.cfi_restore_state
	testl	%esi, %esi
	jne	.L1073
	.p2align 4,,10
	.p2align 3
.L982:
	movq	%r12, %rdi
	call	statem_flush@PLT
	cmpl	$1, %eax
	je	.L1073
.L989:
	movl	$3, %r13d
	jmp	.L975
	.p2align 4,,10
	.p2align 3
.L981:
	call	__errno_location@PLT
	movl	$0, (%rax)
	movq	%rax, %r13
	movq	8(%r12), %rax
	movq	192(%rax), %rdx
	testb	$8, 96(%rdx)
	jne	.L1073
	movl	(%rax), %eax
	cmpl	$65536, %eax
	je	.L1073
	cmpl	$771, %eax
	jle	.L1073
	movq	%r12, %rdi
	call	statem_flush@PLT
	cmpl	$1, %eax
	je	.L1073
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	SSL_get_error@PLT
	cmpl	$5, %eax
	jne	.L989
	movl	0(%r13), %eax
	cmpl	$32, %eax
	je	.L1004
	movl	$3, %r13d
	cmpl	$104, %eax
	jne	.L975
.L1004:
	movl	$1, 40(%r12)
	movl	$2, %r13d
	jmp	.L975
	.p2align 4,,10
	.p2align 3
.L979:
	call	statem_flush@PLT
	cmpl	$1, %eax
	jne	.L989
	movq	8(%r12), %rax
	movq	192(%rax), %r9
	movl	96(%r9), %r13d
	andl	$8, %r13d
	jne	.L1073
	movl	(%rax), %eax
	cmpl	$771, %eax
	jle	.L1073
	cmpl	$65536, %eax
	je	.L1073
	xorl	%ecx, %ecx
	leaq	380(%r12), %rdx
	leaq	-32(%rbp), %r8
	movq	%r12, %rdi
	leaq	444(%r12), %rsi
	call	*24(%r9)
	testl	%eax, %eax
	je	.L975
	movq	8(%r12), %rax
	movl	$290, %esi
	movq	%r12, %rdi
	movq	192(%rax), %rax
	call	*32(%rax)
	testl	%eax, %eax
	movl	$2, %eax
	cmovne	%eax, %r13d
	jmp	.L975
	.p2align 4,,10
	.p2align 3
.L977:
	call	statem_flush@PLT
	cmpl	$1, %eax
	jne	.L989
	movl	$1, %esi
	movq	%r12, %rdi
	call	tls13_update_key@PLT
	testl	%eax, %eax
	jne	.L1073
.L991:
	xorl	%r13d, %r13d
	jmp	.L975
	.p2align 4,,10
	.p2align 3
.L986:
	call	statem_flush@PLT
	cmpl	$1, %eax
	jne	.L989
	movq	%r12, %rdi
	call	ssl3_init_finished_mac@PLT
	testl	%eax, %eax
	jne	.L1073
	jmp	.L991
	.p2align 4,,10
	.p2align 3
.L985:
	call	statem_flush@PLT
	cmpl	$1, %eax
	jne	.L989
	cmpl	$256, (%r12)
	je	.L992
	movq	%r12, %rdi
	call	ssl3_init_finished_mac@PLT
	testl	%eax, %eax
	je	.L991
.L992:
	movl	$1, 1520(%r12)
	movl	$2, %r13d
	jmp	.L975
	.p2align 4,,10
	.p2align 3
.L984:
	movq	8(%rdi), %rdx
	movq	192(%rdx), %rax
	testb	$8, 96(%rax)
	jne	.L1073
	movl	(%rdx), %ecx
	cmpl	$771, %ecx
	jle	.L993
	cmpl	$65536, %ecx
	je	.L1073
	movl	1492(%rdi), %esi
	movl	1248(%rdi), %ecx
	andl	$1048576, %esi
	cmpl	$1, %ecx
	je	.L1075
	testl	%esi, %esi
	je	.L1006
.L1005:
	cmpl	$2, %ecx
	jne	.L1073
.L996:
	movl	(%rdx), %edx
	cmpl	$771, %edx
	jle	.L998
	cmpl	$65536, %edx
	je	.L998
.L1006:
	movq	%r12, %rdi
	call	*16(%rax)
	testl	%eax, %eax
	je	.L991
	movq	8(%r12), %rax
	movl	$162, %esi
	movq	%r12, %rdi
	movq	192(%rax), %rax
	call	*32(%rax)
	testl	%eax, %eax
	je	.L991
	cmpl	$2, 1816(%r12)
	je	.L1000
	movq	8(%r12), %rax
	movl	$161, %esi
	movq	%r12, %rdi
	movq	192(%rax), %rax
	call	*32(%rax)
	testl	%eax, %eax
	je	.L991
.L1000:
	movl	$1, 128(%r12)
	movl	$2, %r13d
	jmp	.L975
	.p2align 4,,10
	.p2align 3
.L983:
	cmpl	$3, 1936(%rdi)
	jne	.L1073
	jmp	.L982
	.p2align 4,,10
	.p2align 3
.L993:
	cmpl	$65536, %ecx
	je	.L1073
	cmpl	$771, %ecx
	jle	.L1073
	movl	1248(%rdi), %ecx
	testb	$16, 1494(%rdi)
	jne	.L1005
	cmpl	$1, %ecx
	jne	.L998
	.p2align 4,,10
	.p2align 3
.L997:
	movq	%r12, %rdi
	call	statem_flush@PLT
	testl	%eax, %eax
	jne	.L1073
	jmp	.L989
.L1074:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1073:
	.size	ossl_statem_server_post_work, .-ossl_statem_server_post_work
	.p2align 4
	.globl	ossl_statem_server_construct_message
	.type	ossl_statem_server_construct_message, @function
ossl_statem_server_construct_message:
.LFB1074:
	.cfi_startproc
	endbr64
	movl	92(%rdi), %eax
	subl	$19, %eax
	cmpl	$27, %eax
	ja	.L1077
	leaq	.L1079(%rip), %rsi
	movslq	(%rsi,%rax,4), %rax
	addq	%rsi, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L1079:
	.long	.L1093-.L1079
	.long	.L1077-.L1079
	.long	.L1092-.L1079
	.long	.L1091-.L1079
	.long	.L1090-.L1079
	.long	.L1089-.L1079
	.long	.L1088-.L1079
	.long	.L1087-.L1079
	.long	.L1077-.L1079
	.long	.L1077-.L1079
	.long	.L1077-.L1079
	.long	.L1077-.L1079
	.long	.L1077-.L1079
	.long	.L1077-.L1079
	.long	.L1086-.L1079
	.long	.L1085-.L1079
	.long	.L1084-.L1079
	.long	.L1083-.L1079
	.long	.L1082-.L1079
	.long	.L1077-.L1079
	.long	.L1077-.L1079
	.long	.L1081-.L1079
	.long	.L1077-.L1079
	.long	.L1080-.L1079
	.long	.L1077-.L1079
	.long	.L1077-.L1079
	.long	.L1077-.L1079
	.long	.L1078-.L1079
	.text
	.p2align 4,,10
	.p2align 3
.L1077:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$236, %ecx
	movl	$431, %edx
	movl	$80, %esi
	movl	$1026, %r9d
	leaq	.LC0(%rip), %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1093:
	.cfi_restore 6
	movq	$0, (%rdx)
	movl	$1, %eax
	movl	$0, (%rcx)
	ret
	.p2align 4,,10
	.p2align 3
.L1092:
	leaq	dtls_construct_hello_verify_request(%rip), %rax
	movq	%rax, (%rdx)
	movl	$1, %eax
	movl	$3, (%rcx)
	ret
	.p2align 4,,10
	.p2align 3
.L1091:
	leaq	tls_construct_server_hello(%rip), %rax
	movq	%rax, (%rdx)
	movl	$1, %eax
	movl	$2, (%rcx)
	ret
	.p2align 4,,10
	.p2align 3
.L1090:
	leaq	tls_construct_server_certificate(%rip), %rax
	movq	%rax, (%rdx)
	movl	$1, %eax
	movl	$11, (%rcx)
	ret
	.p2align 4,,10
	.p2align 3
.L1089:
	leaq	tls_construct_server_key_exchange(%rip), %rax
	movq	%rax, (%rdx)
	movl	$1, %eax
	movl	$12, (%rcx)
	ret
	.p2align 4,,10
	.p2align 3
.L1088:
	leaq	tls_construct_certificate_request(%rip), %rax
	movq	%rax, (%rdx)
	movl	$1, %eax
	movl	$13, (%rcx)
	ret
	.p2align 4,,10
	.p2align 3
.L1087:
	leaq	tls_construct_server_done(%rip), %rax
	movq	%rax, (%rdx)
	movl	$1, %eax
	movl	$14, (%rcx)
	ret
	.p2align 4,,10
	.p2align 3
.L1086:
	leaq	tls_construct_new_session_ticket(%rip), %rax
	movq	%rax, (%rdx)
	movl	$1, %eax
	movl	$4, (%rcx)
	ret
	.p2align 4,,10
	.p2align 3
.L1085:
	leaq	tls_construct_cert_status(%rip), %rax
	movq	%rax, (%rdx)
	movl	$1, %eax
	movl	$22, (%rcx)
	ret
	.p2align 4,,10
	.p2align 3
.L1084:
	movq	8(%rdi), %rax
	movq	192(%rax), %rax
	testb	$8, 96(%rax)
	jne	.L1096
	movq	tls_construct_change_cipher_spec@GOTPCREL(%rip), %rax
.L1095:
	movq	%rax, (%rdx)
	movl	$1, %eax
	movl	$257, (%rcx)
	ret
	.p2align 4,,10
	.p2align 3
.L1083:
	movq	tls_construct_finished@GOTPCREL(%rip), %rax
	movq	%rax, (%rdx)
	movl	$1, %eax
	movl	$20, (%rcx)
	ret
	.p2align 4,,10
	.p2align 3
.L1082:
	leaq	tls_construct_encrypted_extensions(%rip), %rax
	movq	%rax, (%rdx)
	movl	$1, %eax
	movl	$8, (%rcx)
	ret
	.p2align 4,,10
	.p2align 3
.L1081:
	movq	tls_construct_cert_verify@GOTPCREL(%rip), %rax
	movq	%rax, (%rdx)
	movl	$1, %eax
	movl	$15, (%rcx)
	ret
	.p2align 4,,10
	.p2align 3
.L1080:
	movq	tls_construct_key_update@GOTPCREL(%rip), %rax
	movq	%rax, (%rdx)
	movl	$1, %eax
	movl	$24, (%rcx)
	ret
	.p2align 4,,10
	.p2align 3
.L1078:
	movq	$0, (%rdx)
	movl	$1, %eax
	movl	$-1, (%rcx)
	ret
	.p2align 4,,10
	.p2align 3
.L1096:
	movq	dtls_construct_change_cipher_spec@GOTPCREL(%rip), %rax
	jmp	.L1095
	.cfi_endproc
.LFE1074:
	.size	ossl_statem_server_construct_message, .-ossl_statem_server_construct_message
	.p2align 4
	.globl	ossl_statem_server_max_message_size
	.type	ossl_statem_server_max_message_size, @function
ossl_statem_server_max_message_size:
.LFB1075:
	.cfi_startproc
	endbr64
	movl	92(%rdi), %eax
	subl	$20, %eax
	cmpl	$24, %eax
	ja	.L1110
	leaq	.L1103(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L1103:
	.long	.L1109-.L1103
	.long	.L1110-.L1103
	.long	.L1110-.L1103
	.long	.L1110-.L1103
	.long	.L1110-.L1103
	.long	.L1110-.L1103
	.long	.L1110-.L1103
	.long	.L1108-.L1103
	.long	.L1107-.L1103
	.long	.L1106-.L1103
	.long	.L1105-.L1103
	.long	.L1102-.L1103
	.long	.L1104-.L1103
	.long	.L1110-.L1103
	.long	.L1110-.L1103
	.long	.L1110-.L1103
	.long	.L1110-.L1103
	.long	.L1110-.L1103
	.long	.L1110-.L1103
	.long	.L1110-.L1103
	.long	.L1110-.L1103
	.long	.L1110-.L1103
	.long	.L1110-.L1103
	.long	.L1110-.L1103
	.long	.L1102-.L1103
	.text
	.p2align 4,,10
	.p2align 3
.L1110:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1102:
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1105:
	movl	$514, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1104:
	movl	$64, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1108:
	movq	1512(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1107:
	movl	$2048, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1106:
	movl	$16384, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1109:
	movl	$131396, %eax
	ret
	.cfi_endproc
.LFE1075:
	.size	ossl_statem_server_max_message_size, .-ossl_statem_server_max_message_size
	.p2align 4
	.globl	dtls_raw_hello_verify_request
	.type	dtls_raw_hello_verify_request, @function
dtls_raw_hello_verify_request:
.LFB1079:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	movl	$2, %edx
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	movl	$65279, %esi
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$8, %rsp
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L1111
	movl	$1, %ecx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	WPACKET_sub_memcpy__@PLT
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
.L1111:
	addq	$8, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1079:
	.size	dtls_raw_hello_verify_request, .-dtls_raw_hello_verify_request
	.p2align 4
	.globl	tls_process_client_hello
	.type	tls_process_client_hello, @function
tls_process_client_hello:
.LFB1082:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$40, %rsp
	movl	1928(%rdi), %ecx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jne	.L1118
	movq	168(%rdi), %rax
	cmpq	$0, 408(%rax)
	jne	.L1226
.L1118:
	movl	$1408, %edx
	leaq	.LC0(%rip), %rsi
	movl	$656, %edi
	call	CRYPTO_zalloc@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1227
	leaq	2112(%r12), %rdi
	call	RECORD_LAYER_is_sslv2_record@PLT
	movl	%eax, 0(%r13)
	testl	%eax, %eax
	jne	.L1228
	movq	8(%rbx), %rdx
.L1132:
	cmpq	$1, %rdx
	jbe	.L1133
	movq	(%rbx), %rcx
	movzbl	(%rcx), %esi
	sall	$8, %esi
	movl	%esi, 4(%r13)
	movzbl	1(%rcx), %edi
	orl	%edi, %esi
	leaq	2(%rcx), %rdi
	movl	%esi, 4(%r13)
	leaq	-2(%rdx), %rsi
	movq	%rdi, (%rbx)
	movq	%rsi, 8(%rbx)
	testl	%eax, %eax
	je	.L1229
	cmpq	$1, %rsi
	jbe	.L1136
	leaq	4(%rcx), %rsi
	movzwl	2(%rcx), %eax
	movq	%rsi, (%rbx)
	leaq	-4(%rdx), %rsi
	movq	%rsi, 8(%rbx)
	cmpq	$1, %rsi
	jbe	.L1136
	leaq	6(%rcx), %rdi
	movzwl	4(%rcx), %esi
	movq	%rdi, (%rbx)
	leaq	-6(%rdx), %rdi
	movq	%rdi, 8(%rbx)
	cmpq	$1, %rdi
	jbe	.L1136
	movzwl	6(%rcx), %edi
	subq	$8, %rdx
	addq	$8, %rcx
	rolw	$8, %si
	movq	%rcx, (%rbx)
	movq	%rdx, 8(%rbx)
	cmpw	$32, %si
	ja	.L1230
	rolw	$8, %ax
	movzwl	%ax, %eax
	cmpq	%rdx, %rax
	ja	.L1141
	movq	%rax, 352(%r13)
	movq	8(%rbx), %rdx
	movzwl	%si, %esi
	movq	%rcx, 344(%r13)
	addq	%rax, %rcx
	subq	%rax, %rdx
	movq	%rcx, (%rbx)
	movq	%rdx, 8(%rbx)
	cmpq	%rdx, %rsi
	ja	.L1141
	rolw	$8, %di
	leaq	48(%r13), %rax
	movzwl	%di, %edx
	cmpl	$8, %esi
	jnb	.L1142
	testb	$4, %sil
	jne	.L1231
	testl	%esi, %esi
	je	.L1143
	movzbl	(%rcx), %r8d
	movb	%r8b, (%rax)
	testb	$2, %sil
	jne	.L1232
.L1143:
	movq	(%rbx), %rcx
	movq	8(%rbx), %rax
	movzwl	%di, %edi
	addq	%rsi, %rcx
	subq	%rsi, %rax
	movq	%rcx, (%rbx)
	movq	%rax, 8(%rbx)
	cmpq	%rax, %rdi
	ja	.L1141
	leaq	(%rcx,%rdi), %r8
	subq	%rdi, %rax
	movq	%r8, (%rbx)
	movq	%rax, 8(%rbx)
	jne	.L1141
	movl	$32, %eax
	cmpl	$32, %edx
	pxor	%xmm0, %xmm0
	movq	%rsi, 40(%r13)
	cmova	%eax, %edx
	leaq	8(%r13), %rsi
	movups	%xmm0, 8(%r13)
	movl	$1514, %r9d
	movups	%xmm0, 24(%r13)
	movl	%edx, %eax
	cmpq	%rax, %rdi
	jb	.L1225
	subq	%rax, %rsi
	addq	$32, %rsi
	cmpl	$8, %edx
	jnb	.L1149
	testb	$4, %dl
	jne	.L1233
	testl	%edx, %edx
	je	.L1150
	movzbl	(%rcx), %edi
	andb	$2, %dl
	movb	%dil, (%rsi)
	jne	.L1234
.L1150:
	movq	$0, 624(%r13)
	movl	$1, %ecx
	leaq	null_compression.24864(%rip), %rdx
	movq	$0, 632(%r13)
.L1155:
	movq	%rcx, 360(%r13)
	leaq	368(%r13), %rsi
	movl	%ecx, %eax
	cmpl	$8, %ecx
	jnb	.L1178
	andl	$4, %ecx
	jne	.L1235
	testl	%eax, %eax
	je	.L1179
	movzbl	(%rdx), %ecx
	movb	%cl, (%rsi)
	testb	$2, %al
	jne	.L1236
.L1179:
	movdqu	624(%r13), %xmm3
	leaq	648(%r13), %rcx
	movq	%r12, %rdi
	leaq	-64(%rbp), %rsi
	movl	$1, %r9d
	leaq	640(%r13), %r8
	movl	$128, %edx
	movaps	%xmm3, -64(%rbp)
	call	tls_collect_extensions@PLT
	testl	%eax, %eax
	je	.L1129
	movq	%r13, 1856(%r12)
	movl	$2, %eax
	jmp	.L1117
	.p2align 4,,10
	.p2align 3
.L1228:
	movq	168(%r12), %rdx
	cmpq	$0, 408(%rdx)
	je	.L1126
	cmpq	$0, 544(%rdx)
	je	.L1126
.L1127:
	movl	$1426, %r9d
	leaq	.LC0(%rip), %r8
	movl	$244, %ecx
	movq	%r12, %rdi
	movl	$381, %edx
	movl	$10, %esi
	call	ossl_statem_fatal@PLT
.L1129:
	movq	648(%r13), %rdi
	movl	$1605, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
.L1120:
	movl	$1606, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r13, %rdi
	call	CRYPTO_free@PLT
	xorl	%eax, %eax
.L1117:
	movq	-40(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1237
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1226:
	.cfi_restore_state
	cmpq	$0, 544(%rax)
	je	.L1118
	movq	8(%rdi), %rdx
	movq	192(%rdx), %rcx
	testb	$8, 96(%rcx)
	jne	.L1119
	movl	(%rdx), %edx
	cmpl	$65536, %edx
	je	.L1119
	cmpl	$771, %edx
	jle	.L1119
	movl	$1393, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	xorl	%r13d, %r13d
	movl	$381, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L1120
	.p2align 4,,10
	.p2align 3
.L1126:
	movl	1248(%r12), %edx
	testl	%edx, %edx
	jne	.L1127
	movq	8(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L1131
	movq	(%rbx), %rcx
	subq	$1, %rdx
	movzbl	(%rcx), %esi
	addq	$1, %rcx
	movq	%rdx, 8(%rbx)
	movq	%rcx, (%rbx)
	cmpb	$1, %sil
	je	.L1132
.L1131:
	movl	$1453, %r9d
.L1225:
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movl	$381, %edx
	movq	%r12, %rdi
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L1129
	.p2align 4,,10
	.p2align 3
.L1229:
	cmpq	$31, %rsi
	jbe	.L1158
	movdqu	2(%rcx), %xmm1
	movups	%xmm1, 8(%r13)
	movdqu	16(%rdi), %xmm2
	movups	%xmm2, 24(%r13)
	movq	(%rbx), %rcx
	leaq	32(%rcx), %rax
	movq	%rax, (%rbx)
	movq	8(%rbx), %rax
	leaq	-32(%rax), %rdx
	movq	%rdx, 8(%rbx)
	testq	%rdx, %rdx
	je	.L1158
	movzbl	32(%rcx), %edx
	subq	$33, %rax
	cmpq	%rdx, %rax
	jb	.L1158
	leaq	33(%rcx), %rsi
	subq	%rdx, %rax
	leaq	(%rsi,%rdx), %rdi
	movq	%rax, 8(%rbx)
	movq	%rdi, (%rbx)
	cmpq	$32, %rdx
	jbe	.L1157
	movq	$0, 40(%r13)
	.p2align 4,,10
	.p2align 3
.L1158:
	movl	$1527, %r9d
.L1223:
	leaq	.LC0(%rip), %r8
	movl	$159, %ecx
	movl	$381, %edx
	movq	%r12, %rdi
	movl	$50, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L1129
	.p2align 4,,10
	.p2align 3
.L1119:
	movl	1492(%r12), %edx
	testl	$1073741824, %edx
	jne	.L1121
	andl	$262144, %edx
	orl	984(%rax), %edx
	je	.L1121
	movl	$1, 1928(%r12)
	movl	$1, 60(%r12)
	jmp	.L1118
	.p2align 4,,10
	.p2align 3
.L1227:
	movl	$1410, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movq	%r12, %rdi
	movl	$381, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L1120
	.p2align 4,,10
	.p2align 3
.L1133:
	movl	$1460, %r9d
	leaq	.LC0(%rip), %r8
	movl	$160, %ecx
	movq	%r12, %rdi
	movl	$381, %edx
	movl	$50, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L1129
	.p2align 4,,10
	.p2align 3
.L1136:
	movl	$1478, %r9d
.L1224:
	leaq	.LC0(%rip), %r8
	movl	$213, %ecx
	movl	$381, %edx
	movq	%r12, %rdi
	movl	$50, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L1129
	.p2align 4,,10
	.p2align 3
.L1121:
	movl	$100, %edx
	movl	$1, %esi
	movq	%r12, %rdi
	call	ssl3_send_alert@PLT
	movl	$1, %eax
	jmp	.L1117
	.p2align 4,,10
	.p2align 3
.L1141:
	movl	$1495, %r9d
	jmp	.L1224
	.p2align 4,,10
	.p2align 3
.L1230:
	movl	$1484, %r9d
	leaq	.LC0(%rip), %r8
	movl	$159, %ecx
	movq	%r12, %rdi
	movl	$381, %edx
	movl	$47, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L1129
	.p2align 4,,10
	.p2align 3
.L1157:
	movq	%rdx, 40(%r13)
	leaq	48(%r13), %rax
	cmpl	$8, %edx
	jnb	.L1159
	testb	$4, %dl
	jne	.L1238
	testl	%edx, %edx
	je	.L1160
	movzbl	33(%rcx), %ecx
	movb	%cl, (%rax)
	testb	$2, %dl
	jne	.L1239
.L1160:
	movq	8(%r12), %rax
	movq	192(%rax), %rax
	testb	$8, 96(%rax)
	je	.L1164
	movq	8(%rbx), %rax
	testq	%rax, %rax
	je	.L1165
	movq	(%rbx), %rcx
	leaq	-1(%rax), %rdx
	movzbl	(%rcx), %eax
	cmpq	%rax, %rdx
	jb	.L1165
	leaq	1(%rcx), %rsi
	subq	%rax, %rdx
	leaq	(%rsi,%rax), %rdi
	movq	%rdx, 8(%rbx)
	leaq	88(%r13), %rdx
	movq	%rdi, (%rbx)
	movq	%rax, 80(%r13)
	cmpl	$8, %eax
	jnb	.L1166
	testb	$4, %al
	jne	.L1240
	testl	%eax, %eax
	je	.L1167
	movzbl	1(%rcx), %ecx
	movb	%cl, (%rdx)
	testb	$2, %al
	jne	.L1241
.L1167:
	movq	%r12, %rdi
	call	SSL_get_options@PLT
	testb	$32, %ah
	je	.L1164
	cmpq	$0, 80(%r13)
	je	.L1242
.L1164:
	movq	8(%rbx), %rax
	cmpq	$1, %rax
	jbe	.L1171
	movq	(%rbx), %rcx
	subq	$2, %rax
	movzwl	(%rcx), %edx
	rolw	$8, %dx
	movzwl	%dx, %edx
	cmpq	%rdx, %rax
	jb	.L1171
	addq	$2, %rcx
	subq	%rdx, %rax
	leaq	(%rcx,%rdx), %rsi
	movq	%rax, 8(%rbx)
	movq	%rsi, (%rbx)
	movq	%rcx, 344(%r13)
	movq	%rdx, 352(%r13)
	movq	8(%rbx), %rax
	testq	%rax, %rax
	je	.L1172
	movq	(%rbx), %rdx
	subq	$1, %rax
	movzbl	(%rdx), %ecx
	cmpq	%rcx, %rax
	jb	.L1172
	addq	$1, %rdx
	subq	%rcx, %rax
	leaq	(%rdx,%rcx), %rdi
	movq	%rax, 8(%rbx)
	movq	%rdi, (%rbx)
	jne	.L1243
	movq	$0, 624(%r13)
	movq	$0, 632(%r13)
	jmp	.L1155
	.p2align 4,,10
	.p2align 3
.L1142:
	movq	(%rcx), %r8
	leaq	56(%r13), %r9
	andq	$-8, %r9
	movq	%r8, 48(%r13)
	movq	-8(%rcx,%rsi), %r8
	movq	%r8, -8(%rax,%rsi)
	subq	%r9, %rax
	subq	%rax, %rcx
	addl	%esi, %eax
	andl	$-8, %eax
	cmpl	$8, %eax
	jb	.L1143
	andl	$-8, %eax
	xorl	%r8d, %r8d
.L1146:
	movl	%r8d, %r10d
	addl	$8, %r8d
	movq	(%rcx,%r10), %r11
	movq	%r11, (%r9,%r10)
	cmpl	%eax, %r8d
	jb	.L1146
	jmp	.L1143
.L1159:
	movq	33(%rcx), %rcx
	leaq	56(%r13), %rdi
	andq	$-8, %rdi
	movq	%rcx, 48(%r13)
	movq	-8(%rsi,%rdx), %rcx
	movq	%rcx, -8(%rax,%rdx)
	subq	%rdi, %rax
	addl	%eax, %edx
	subq	%rax, %rsi
	shrl	$3, %edx
	movl	%edx, %ecx
	rep movsq
	jmp	.L1160
.L1171:
	movl	$1559, %r9d
	jmp	.L1223
.L1165:
	movl	$1534, %r9d
	jmp	.L1223
.L1172:
	movl	$1565, %r9d
	jmp	.L1223
.L1166:
	movq	1(%rcx), %rcx
	leaq	96(%r13), %rdi
	andq	$-8, %rdi
	movq	%rcx, 88(%r13)
	movq	-8(%rsi,%rax), %rcx
	movq	%rcx, -8(%rdx,%rax)
	movq	%rdx, %rcx
	subq	%rdi, %rcx
	subq	%rcx, %rsi
	addl	%eax, %ecx
	shrl	$3, %ecx
	rep movsq
	jmp	.L1167
.L1178:
	movq	(%rdx), %rax
	leaq	376(%r13), %rdi
	andq	$-8, %rdi
	movq	%rax, 368(%r13)
	movq	-8(%rdx,%rcx), %rax
	movq	%rax, -8(%rsi,%rcx)
	movq	%rsi, %rax
	subq	%rdi, %rax
	subq	%rax, %rdx
	addl	%ecx, %eax
	shrl	$3, %eax
	movq	%rdx, %rsi
	movl	%eax, %ecx
	rep movsq
	jmp	.L1179
.L1231:
	movl	(%rcx), %r8d
	movl	%r8d, (%rax)
	movl	-4(%rcx,%rsi), %ecx
	movl	%ecx, -4(%rax,%rsi)
	jmp	.L1143
.L1149:
	movq	(%rcx), %rdi
	movq	%rdi, (%rsi)
	movq	-8(%rcx,%rax), %rdi
	movq	%rdi, -8(%rsi,%rax)
	leaq	8(%rsi), %rdi
	andq	$-8, %rdi
	subq	%rdi, %rsi
	addl	%esi, %edx
	subq	%rsi, %rcx
	andl	$-8, %edx
	cmpl	$8, %edx
	jb	.L1150
	andl	$-8, %edx
	xorl	%eax, %eax
.L1153:
	movl	%eax, %esi
	addl	$8, %eax
	movq	(%rcx,%rsi), %r8
	movq	%r8, (%rdi,%rsi)
	cmpl	%edx, %eax
	jb	.L1153
	jmp	.L1150
.L1243:
	cmpq	$1, %rax
	je	.L1177
	movzwl	(%rdi), %esi
	subq	$2, %rax
	rolw	$8, %si
	movzwl	%si, %esi
	cmpq	%rsi, %rax
	jb	.L1177
	addq	$2, %rdi
	subq	%rsi, %rax
	leaq	(%rdi,%rsi), %r8
	movq	%rax, 8(%rbx)
	movq	%r8, (%rbx)
	movq	%rsi, 632(%r13)
	cmpq	$0, 8(%rbx)
	movq	%rdi, 624(%r13)
	je	.L1155
.L1177:
	movl	$1576, %r9d
	jmp	.L1223
.L1238:
	movl	33(%rcx), %ecx
	movl	%ecx, (%rax)
	movl	-4(%rsi,%rdx), %ecx
	movl	%ecx, -4(%rax,%rdx)
	jmp	.L1160
.L1232:
	movzwl	-2(%rcx,%rsi), %ecx
	movw	%cx, -2(%rax,%rsi)
	jmp	.L1143
.L1237:
	call	__stack_chk_fail@PLT
.L1242:
	movl	$1552, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r13, %rdi
	call	CRYPTO_free@PLT
	movl	$1, %eax
	jmp	.L1117
.L1239:
	movzwl	-2(%rsi,%rdx), %ecx
	movw	%cx, -2(%rax,%rdx)
	jmp	.L1160
.L1241:
	movzwl	-2(%rsi,%rax), %ecx
	movw	%cx, -2(%rdx,%rax)
	jmp	.L1167
.L1235:
	movl	(%rdx), %ecx
	movl	%ecx, (%rsi)
	movl	-4(%rdx,%rax), %edx
	movl	%edx, -4(%rsi,%rax)
	jmp	.L1179
.L1233:
	movl	(%rcx), %edx
	movl	%edx, (%rsi)
	movl	-4(%rcx,%rax), %edx
	movl	%edx, -4(%rsi,%rax)
	jmp	.L1150
.L1240:
	movl	1(%rcx), %ecx
	movl	%ecx, (%rdx)
	movl	-4(%rsi,%rax), %ecx
	movl	%ecx, -4(%rdx,%rax)
	jmp	.L1167
.L1236:
	movzwl	-2(%rdx,%rax), %edx
	movw	%dx, -2(%rsi,%rax)
	jmp	.L1179
.L1234:
	movzwl	-2(%rcx,%rax), %edx
	movw	%dx, -2(%rsi,%rax)
	jmp	.L1150
	.cfi_endproc
.LFE1082:
	.size	tls_process_client_hello, .-tls_process_client_hello
	.p2align 4
	.globl	tls_handle_alpn
	.type	tls_handle_alpn, @function
tls_handle_alpn:
.LFB1085:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -40
	movq	1440(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -48(%rbp)
	movq	616(%rdx), %rax
	movb	$0, -49(%rbp)
	testq	%rax, %rax
	je	.L1245
	movq	168(%rdi), %rsi
	movq	1008(%rsi), %rcx
	testq	%rcx, %rcx
	je	.L1245
	leaq	-49(%rbp), %r11
	leaq	-48(%rbp), %r10
	movq	624(%rdx), %r9
	movl	1016(%rsi), %r8d
	movq	%r11, %rdx
	movq	%r10, %rsi
	call	*%rax
	movl	%eax, %r13d
	testl	%eax, %eax
	je	.L1268
	cmpl	$3, %eax
	jne	.L1269
.L1245:
	movq	1296(%r12), %rax
	cmpq	$0, 584(%rax)
	je	.L1267
	movl	$0, 1820(%r12)
	movl	$1, %r13d
.L1244:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1270
	addq	$40, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1249:
	.cfi_restore_state
	movl	200(%r12), %r13d
	movl	$0, 1820(%r12)
	testl	%r13d, %r13d
	jne	.L1267
	movq	-48(%rbp), %rdi
	movl	$2211, %ecx
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_memdup@PLT
	movl	$2214, %r9d
	movq	%rax, 584(%rbx)
	movq	1296(%r12), %rax
	cmpq	$0, 584(%rax)
	je	.L1266
	movzbl	-49(%rbp), %edx
	movq	%rdx, 592(%rax)
	.p2align 4,,10
	.p2align 3
.L1267:
	movl	$1, %r13d
	jmp	.L1244
	.p2align 4,,10
	.p2align 3
.L1269:
	movl	$235, %ecx
	movl	$562, %edx
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	movl	$2225, %r9d
	leaq	.LC0(%rip), %r8
	movl	$120, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L1244
	.p2align 4,,10
	.p2align 3
.L1268:
	movq	168(%r12), %rax
	movl	$2178, %edx
	leaq	.LC0(%rip), %rsi
	movq	992(%rax), %rdi
	call	CRYPTO_free@PLT
	movzbl	-49(%rbp), %esi
	movq	-48(%rbp), %rdi
	movl	$2179, %ecx
	movq	168(%r12), %rbx
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_memdup@PLT
	movl	$2181, %r9d
	movq	%rax, 992(%rbx)
	movq	168(%r12), %rax
	cmpq	$0, 992(%rax)
	je	.L1266
	movq	1296(%r12), %rbx
	movzbl	-49(%rbp), %esi
	movl	$0, 988(%rax)
	movq	584(%rbx), %r8
	movq	%rsi, 1000(%rax)
	testq	%r8, %r8
	je	.L1249
	cmpq	592(%rbx), %rsi
	je	.L1271
.L1250:
	movl	200(%r12), %r13d
	movl	$2206, %r9d
	movl	$0, 1820(%r12)
	testl	%r13d, %r13d
	jne	.L1267
.L1266:
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movl	$562, %edx
	movq	%r12, %rdi
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L1244
	.p2align 4,,10
	.p2align 3
.L1271:
	movq	-48(%rbp), %rdi
	movq	%rsi, %rdx
	movq	%r8, %rsi
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L1250
	jmp	.L1267
.L1270:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1085:
	.size	tls_handle_alpn, .-tls_handle_alpn
	.p2align 4
	.globl	ossl_statem_server_post_process_message
	.type	ossl_statem_server_post_process_message, @function
ossl_statem_server_post_process_message:
.LFB1077:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	92(%rdi), %eax
	cmpl	$20, %eax
	je	.L1273
	cmpl	$28, %eax
	je	.L1274
	movl	$1237, %r9d
	movl	$68, %ecx
	movl	$601, %edx
	movl	$80, %esi
	leaq	.LC0(%rip), %r8
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
.L1272:
	movq	-24(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1358
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1273:
	.cfi_restore_state
	cmpl	$3, %esi
	je	.L1359
	cmpl	$4, %esi
	je	.L1280
	cmpl	$5, %esi
	je	.L1301
.L1303:
	movl	$1, %eax
	jmp	.L1272
	.p2align 4,,10
	.p2align 3
.L1274:
	movl	116(%rdi), %eax
	testl	%eax, %eax
	jne	.L1307
	movq	1296(%rdi), %rdx
	cmpq	$0, 440(%rdx)
	je	.L1307
	movq	168(%rdi), %rdx
	cmpq	$0, 224(%rdx)
	je	.L1360
	movl	$1, %esi
	call	ssl3_digest_cached_records@PLT
	testl	%eax, %eax
	je	.L1277
.L1309:
	movl	$2, %eax
	jmp	.L1272
	.p2align 4,,10
	.p2align 3
.L1280:
	movl	200(%rdi), %ecx
	testl	%ecx, %ecx
	jne	.L1361
	movq	1168(%rdi), %rdx
	movq	440(%rdx), %rax
	testq	%rax, %rax
	je	.L1285
	movq	%rdi, -40(%rbp)
	movq	448(%rdx), %rsi
	call	*%rax
	movq	-40(%rbp), %rdi
	testl	%eax, %eax
	je	.L1362
	js	.L1363
	movl	$1, 40(%rdi)
.L1285:
	movq	8(%rdi), %rax
	movq	192(%rax), %rdx
	testb	$8, 96(%rdx)
	jne	.L1291
	movl	(%rax), %eax
	cmpl	$771, %eax
	jle	.L1291
	cmpl	$65536, %eax
	jne	.L1283
.L1291:
	movq	%rdi, -40(%rbp)
	call	SSL_get_ciphers@PLT
	movq	-40(%rbp), %rdi
	movq	%rax, %rdx
	movq	280(%rdi), %rsi
	call	ssl3_choose_cipher@PLT
	movq	-40(%rbp), %rdi
	testq	%rax, %rax
	je	.L1364
	movq	168(%rdi), %rdx
	movq	%rax, 568(%rdx)
.L1283:
	movl	200(%rdi), %edx
	testl	%edx, %edx
	jne	.L1293
	movl	$1, %esi
	movq	%rdi, -40(%rbp)
	call	tls_choose_sigalg@PLT
	testl	%eax, %eax
	je	.L1277
	movq	-40(%rbp), %rdi
	movq	2104(%rdi), %rax
	movq	1296(%rdi), %rbx
	testq	%rax, %rax
	je	.L1294
	movq	168(%rdi), %rdx
	xorl	%esi, %esi
	movq	568(%rdx), %rdx
	testb	$6, 28(%rdx)
	setne	%sil
	call	*%rax
	movq	-40(%rbp), %rdi
	movl	%eax, 432(%rbx)
	movq	1296(%rdi), %rbx
.L1294:
	movl	432(%rbx), %eax
	testl	%eax, %eax
	je	.L1293
	movl	$0, 1664(%rdi)
	jmp	.L1293
	.p2align 4,,10
	.p2align 3
.L1307:
	xorl	%esi, %esi
	call	ssl3_digest_cached_records@PLT
	testl	%eax, %eax
	jne	.L1309
.L1277:
	xorl	%eax, %eax
	jmp	.L1272
	.p2align 4,,10
	.p2align 3
.L1361:
	movq	8(%rdi), %rax
	movq	192(%rax), %rdx
	testb	$8, 96(%rdx)
	jne	.L1282
	movl	(%rax), %eax
	cmpl	$771, %eax
	jle	.L1282
	cmpl	$65536, %eax
	jne	.L1283
.L1282:
	movq	1296(%rdi), %rdx
	movq	168(%rdi), %rax
	movq	504(%rdx), %rdx
	movq	%rdx, 568(%rax)
.L1293:
	cmpl	$-1, 1608(%rdi)
	movl	$0, 1628(%rdi)
	je	.L1299
	movq	1440(%rdi), %rax
	testq	%rax, %rax
	je	.L1299
	movq	560(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1299
	movq	168(%rdi), %rcx
	movq	736(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L1299
	movq	1168(%rdi), %rsi
	movq	%rdi, -40(%rbp)
	movq	%rcx, (%rsi)
	movq	568(%rax), %rsi
	call	*%rdx
	movq	-40(%rbp), %rdi
	testl	%eax, %eax
	je	.L1297
	cmpl	$3, %eax
	jne	.L1298
	movl	$0, 1628(%rdi)
	.p2align 4,,10
	.p2align 3
.L1299:
	movq	8(%rdi), %rax
	movq	192(%rax), %rdx
	testb	$8, 96(%rdx)
	jne	.L1300
	movl	(%rax), %eax
	cmpl	$65536, %eax
	je	.L1300
	cmpl	$771, %eax
	jg	.L1301
.L1300:
	movq	%rdi, -40(%rbp)
	call	tls_handle_alpn
	movq	-40(%rbp), %rdi
	testl	%eax, %eax
	je	.L1277
.L1301:
	movq	168(%rdi), %rax
	movl	$112, -28(%rbp)
	movq	568(%rax), %rax
	testb	$32, 28(%rax)
	je	.L1303
	cmpq	$0, 1984(%rdi)
	je	.L1303
	cmpq	$0, 2008(%rdi)
	je	.L1365
	leaq	-28(%rbp), %rsi
	movq	%rdi, -40(%rbp)
	call	SSL_srp_server_param_with_username@PLT
	movq	-40(%rbp), %rdi
	testl	%eax, %eax
	js	.L1305
	cmpl	$2, %eax
	jne	.L1303
	movl	-28(%rbp), %esi
	xorl	%ecx, %ecx
	movl	$1273, %r9d
	movl	$606, %edx
	leaq	.LC0(%rip), %r8
	cmpl	$115, %esi
	setne	%cl
	leal	223(%rcx,%rcx,2), %ecx
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	jmp	.L1272
	.p2align 4,,10
	.p2align 3
.L1359:
	movq	%rdi, -40(%rbp)
	call	tls_early_post_process_client_hello
	testl	%eax, %eax
	je	.L1277
	movq	-40(%rbp), %rdi
	jns	.L1280
	movl	$3, %eax
	jmp	.L1272
.L1364:
	movl	$2282, %r9d
	movl	$193, %ecx
	movl	$378, %edx
	movl	$40, %esi
	leaq	.LC0(%rip), %r8
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	jmp	.L1272
	.p2align 4,,10
	.p2align 3
.L1360:
	movl	$3563, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movl	%eax, -40(%rbp)
	movl	$384, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	movl	-40(%rbp), %eax
	jmp	.L1272
.L1305:
	movl	$4, 40(%rdi)
	movl	$5, %eax
	jmp	.L1272
	.p2align 4,,10
	.p2align 3
.L1298:
	movl	$2151, %r9d
	movl	$226, %ecx
	movl	$563, %edx
	movl	$80, %esi
	leaq	.LC0(%rip), %r8
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	jmp	.L1272
.L1297:
	cmpq	$0, 1648(%rdi)
	je	.L1299
	movl	$1, 1628(%rdi)
	jmp	.L1299
.L1365:
	movl	$1264, %r9d
	movl	$223, %ecx
	movl	$606, %edx
	movl	$115, %esi
	leaq	.LC0(%rip), %r8
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	jmp	.L1272
.L1362:
	movl	$2264, %r9d
	movl	$377, %ecx
	movl	$378, %edx
	movl	$80, %esi
	leaq	.LC0(%rip), %r8
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	jmp	.L1272
.L1363:
	movl	$4, 40(%rdi)
	movl	$4, %eax
	jmp	.L1272
.L1358:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1077:
	.size	ossl_statem_server_post_process_message, .-ossl_statem_server_post_process_message
	.p2align 4
	.globl	tls_post_process_client_hello
	.type	tls_post_process_client_hello, @function
tls_post_process_client_hello:
.LFB1086:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpl	$3, %esi
	je	.L1442
	cmpl	$4, %esi
	je	.L1372
	cmpl	$5, %esi
	je	.L1394
.L1396:
	movl	$1, %eax
.L1366:
	movq	-24(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1443
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1442:
	.cfi_restore_state
	call	tls_early_post_process_client_hello
	testl	%eax, %eax
	je	.L1441
	js	.L1402
.L1372:
	movl	200(%r12), %ecx
	testl	%ecx, %ecx
	jne	.L1444
	movq	1168(%r12), %rdx
	movq	440(%rdx), %rax
	testq	%rax, %rax
	je	.L1377
	movq	448(%rdx), %rsi
	movq	%r12, %rdi
	call	*%rax
	testl	%eax, %eax
	je	.L1445
	js	.L1446
	movl	$1, 40(%r12)
.L1377:
	movq	8(%r12), %rax
	movq	192(%rax), %rdx
	testb	$8, 96(%rdx)
	jne	.L1383
	movl	(%rax), %eax
	cmpl	$771, %eax
	jle	.L1383
	cmpl	$65536, %eax
	jne	.L1375
.L1383:
	movq	%r12, %rdi
	call	SSL_get_ciphers@PLT
	movq	280(%r12), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	ssl3_choose_cipher@PLT
	testq	%rax, %rax
	je	.L1447
	movq	168(%r12), %rdx
	movq	%rax, 568(%rdx)
.L1375:
	movl	200(%r12), %edx
	testl	%edx, %edx
	je	.L1448
.L1385:
	cmpl	$-1, 1608(%r12)
	movl	$0, 1628(%r12)
	je	.L1392
	movq	1440(%r12), %rax
	testq	%rax, %rax
	je	.L1392
	movq	560(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1392
	movq	168(%r12), %rcx
	movq	736(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L1392
	movq	1168(%r12), %rsi
	movq	%r12, %rdi
	movq	%rcx, (%rsi)
	movq	568(%rax), %rsi
	call	*%rdx
	testl	%eax, %eax
	jne	.L1449
	cmpq	$0, 1648(%r12)
	je	.L1392
	movl	$1, 1628(%r12)
	.p2align 4,,10
	.p2align 3
.L1392:
	movq	8(%r12), %rax
	movq	192(%rax), %rdx
	testb	$8, 96(%rdx)
	jne	.L1393
	movl	(%rax), %eax
	cmpl	$65536, %eax
	je	.L1393
	cmpl	$771, %eax
	jg	.L1394
.L1393:
	movq	%r12, %rdi
	call	tls_handle_alpn
	testl	%eax, %eax
	je	.L1441
.L1394:
	movq	168(%r12), %rax
	movl	$112, -28(%rbp)
	movq	568(%rax), %rax
	testb	$32, 28(%rax)
	je	.L1396
	cmpq	$0, 1984(%r12)
	je	.L1396
	cmpq	$0, 2008(%r12)
	je	.L1450
	leaq	-28(%rbp), %rsi
	movq	%r12, %rdi
	call	SSL_srp_server_param_with_username@PLT
	testl	%eax, %eax
	js	.L1398
	cmpl	$2, %eax
	jne	.L1396
	movl	-28(%rbp), %esi
	xorl	%ecx, %ecx
	movl	$1273, %r9d
	movl	$606, %edx
	leaq	.LC0(%rip), %r8
	movq	%r12, %rdi
	cmpl	$115, %esi
	setne	%cl
	leal	223(%rcx,%rcx,2), %ecx
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	jmp	.L1366
	.p2align 4,,10
	.p2align 3
.L1444:
	movq	8(%r12), %rax
	movq	192(%rax), %rdx
	testb	$8, 96(%rdx)
	jne	.L1374
	movl	(%rax), %eax
	cmpl	$771, %eax
	jle	.L1374
	cmpl	$65536, %eax
	jne	.L1375
.L1374:
	movq	1296(%r12), %rdx
	movq	168(%r12), %rax
	movq	504(%rdx), %rdx
	movq	%rdx, 568(%rax)
	jmp	.L1385
	.p2align 4,,10
	.p2align 3
.L1445:
	movl	$2264, %r9d
	leaq	.LC0(%rip), %r8
	movl	$377, %ecx
	movq	%r12, %rdi
	movl	$378, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	.p2align 4,,10
	.p2align 3
.L1441:
	xorl	%eax, %eax
	jmp	.L1366
	.p2align 4,,10
	.p2align 3
.L1449:
	cmpl	$3, %eax
	je	.L1451
	movl	$2151, %r9d
	leaq	.LC0(%rip), %r8
	movl	$226, %ecx
	movq	%r12, %rdi
	movl	$563, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	jmp	.L1366
	.p2align 4,,10
	.p2align 3
.L1398:
	movl	$4, 40(%r12)
	movl	$5, %eax
	jmp	.L1366
	.p2align 4,,10
	.p2align 3
.L1448:
	movl	$1, %esi
	movq	%r12, %rdi
	call	tls_choose_sigalg@PLT
	testl	%eax, %eax
	je	.L1441
	movq	2104(%r12), %rax
	movq	1296(%r12), %rbx
	testq	%rax, %rax
	je	.L1387
	movq	168(%r12), %rdx
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	568(%rdx), %rdx
	testb	$6, 28(%rdx)
	setne	%sil
	call	*%rax
	movl	%eax, 432(%rbx)
	movq	1296(%r12), %rbx
.L1387:
	movl	432(%rbx), %eax
	testl	%eax, %eax
	je	.L1385
	movl	$0, 1664(%r12)
	jmp	.L1385
	.p2align 4,,10
	.p2align 3
.L1451:
	movl	$0, 1628(%r12)
	jmp	.L1392
	.p2align 4,,10
	.p2align 3
.L1402:
	movl	$3, %eax
	jmp	.L1366
	.p2align 4,,10
	.p2align 3
.L1450:
	movl	$1264, %r9d
	leaq	.LC0(%rip), %r8
	movl	$223, %ecx
	movq	%r12, %rdi
	movl	$606, %edx
	movl	$115, %esi
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	jmp	.L1366
	.p2align 4,,10
	.p2align 3
.L1447:
	movl	$2282, %r9d
	leaq	.LC0(%rip), %r8
	movl	$193, %ecx
	movq	%r12, %rdi
	movl	$378, %edx
	movl	$40, %esi
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	jmp	.L1366
	.p2align 4,,10
	.p2align 3
.L1446:
	movl	$4, 40(%r12)
	movl	$4, %eax
	jmp	.L1366
.L1443:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1086:
	.size	tls_post_process_client_hello, .-tls_post_process_client_hello
	.p2align 4
	.globl	tls_process_client_key_exchange
	.type	tls_process_client_key_exchange, @function
tls_process_client_key_exchange:
.LFB1097:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	168(%rdi), %rax
	movq	568(%rax), %rax
	movl	28(%rax), %r13d
	testl	$456, %r13d
	jne	.L1453
.L1456:
	testb	$8, %r13b
	je	.L1538
	cmpq	$0, 8(%r12)
	jne	.L1539
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	ssl_generate_master_secret@PLT
	testl	%eax, %eax
	je	.L1457
.L1537:
	movl	$2, %eax
	jmp	.L1452
	.p2align 4,,10
	.p2align 3
.L1538:
	testb	$65, %r13b
	jne	.L1540
	testl	$258, %r13d
	jne	.L1541
	testb	$-124, %r13b
	jne	.L1542
	testb	$32, %r13b
	jne	.L1543
	andl	$16, %r13d
	je	.L1503
	movl	$3443, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movq	%rbx, %rdi
	movl	$413, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	.p2align 4,,10
	.p2align 3
.L1457:
	movq	168(%rbx), %rax
	movl	$3509, %ecx
	leaq	.LC0(%rip), %rdx
	movq	720(%rax), %rsi
	movq	712(%rax), %rdi
	call	CRYPTO_clear_free@PLT
	movq	168(%rbx), %rax
	movq	$0, 712(%rax)
	xorl	%eax, %eax
.L1452:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1544
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1453:
	.cfi_restore_state
	call	tls_process_cke_psk_preamble
	testl	%eax, %eax
	jne	.L1456
	jmp	.L1457
	.p2align 4,,10
	.p2align 3
.L1541:
	movq	8(%r12), %rax
	cmpq	$1, %rax
	jbe	.L1477
	movq	(%r12), %rdx
	subq	$2, %rax
	movzwl	(%rdx), %r13d
	leaq	2(%rdx), %r14
	movq	%rax, 8(%r12)
	movq	%r14, (%r12)
	rolw	$8, %r13w
	movzwl	%r13w, %edx
	cmpq	%rdx, %rax
	jne	.L1477
	movq	168(%rbx), %rdx
	movq	576(%rdx), %r15
	testq	%r15, %r15
	je	.L1545
	testq	%rax, %rax
	je	.L1546
	movq	$0, 8(%r12)
	addq	%r14, %rax
	movq	%rax, (%r12)
	call	EVP_PKEY_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1481
	movq	%r15, %rsi
	movq	%rax, %rdi
	call	EVP_PKEY_copy_parameters@PLT
	testl	%eax, %eax
	je	.L1481
	movq	%r12, %rdi
	call	EVP_PKEY_get0_DH@PLT
	movzwl	%r13w, %esi
	xorl	%edx, %edx
	movq	%r14, %rdi
	movq	%rax, -120(%rbp)
	call	BN_bin2bn@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1484
	movq	-120(%rbp), %r8
	testq	%r8, %r8
	je	.L1484
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r8, %rdi
	call	DH_set0_key@PLT
	testl	%eax, %eax
	je	.L1484
	movl	$1, %ecx
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	ssl_derive@PLT
	testl	%eax, %eax
	je	.L1530
.L1536:
	movq	168(%rbx), %rax
	movq	576(%rax), %rdi
	call	EVP_PKEY_free@PLT
	movq	168(%rbx), %rax
	movq	%r12, %rdi
	movq	$0, 576(%rax)
	call	EVP_PKEY_free@PLT
	jmp	.L1537
	.p2align 4,,10
	.p2align 3
.L1477:
	movl	$3171, %r9d
	movl	$148, %ecx
	movl	$411, %edx
	leaq	.LC0(%rip), %r8
.L1533:
	movq	%rbx, %rdi
	movl	$50, %esi
	call	ossl_statem_fatal@PLT
	xorl	%edi, %edi
	call	EVP_PKEY_free@PLT
	jmp	.L1457
	.p2align 4,,10
	.p2align 3
.L1539:
	movl	$3464, %r9d
	leaq	.LC0(%rip), %r8
	movl	$159, %ecx
	movq	%rbx, %rdi
	movl	$382, %edx
	movl	$50, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L1457
	.p2align 4,,10
	.p2align 3
.L1540:
	movq	1168(%rbx), %rax
	movq	40(%rax), %rdi
	call	EVP_PKEY_get0_RSA@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L1547
	movl	(%rbx), %eax
	movq	(%r12), %r14
	movq	8(%r12), %r13
	andb	$-3, %ah
	cmpl	$256, %eax
	je	.L1463
	cmpq	$1, %r13
	jbe	.L1465
	movzwl	(%r14), %r9d
	leaq	-2(%r13), %rax
	rolw	$8, %r9w
	movzwl	%r9w, %r13d
	cmpq	%r13, %rax
	jb	.L1465
	addq	$2, %r14
	subq	%r13, %rax
	leaq	(%r14,%r13), %rdx
	movq	%rax, 8(%r12)
	movq	%rdx, (%r12)
	je	.L1463
.L1465:
	movl	$3013, %r9d
	leaq	.LC0(%rip), %r8
	movl	$159, %ecx
	movq	%rbx, %rdi
	movl	$415, %edx
	movl	$50, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L1457
	.p2align 4,,10
	.p2align 3
.L1463:
	movq	%r15, %rdi
	call	RSA_size@PLT
	cmpl	$47, %eax
	jle	.L1548
	movq	%r15, %rdi
	call	RSA_size@PLT
	movl	$3031, %edx
	leaq	.LC0(%rip), %rsi
	movslq	%eax, %rdi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1549
	leaq	-112(%rbp), %rax
	movl	$48, %esi
	movq	%rax, %rdi
	movq	%rax, -120(%rbp)
	call	RAND_priv_bytes@PLT
	testl	%eax, %eax
	jle	.L1550
	movl	$3, %r8d
	movq	%r15, %rcx
	movq	%r12, %rdx
	movq	%r14, %rsi
	movl	%r13d, %edi
	call	RSA_private_decrypt@PLT
	testl	%eax, %eax
	js	.L1551
	movl	$3075, %r9d
	leaq	.LC0(%rip), %r8
	movl	$147, %ecx
	cmpl	$58, %eax
	jle	.L1531
	movzbl	1(%r12), %edx
	movzbl	(%r12), %ecx
	leal	-48(%rax), %esi
	cltq
	movslq	%esi, %rsi
	xorl	$2, %edx
	movl	%ecx, %edi
	leal	-1(%rcx), %r8d
	movzbl	%dl, %edx
	notl	%edi
	movl	%edx, %ecx
	andl	%r8d, %edi
	subl	$1, %edx
	notl	%ecx
	sarl	$31, %edi
	andl	%ecx, %edx
	movl	%edi, %r8d
	leaq	2(%r12), %rcx
	sarl	$31, %edx
	leaq	-49(%r12,%rax), %rdi
	andl	%edx, %r8d
	.p2align 4,,10
	.p2align 3
.L1471:
	movzbl	(%rcx), %edx
	addq	$1, %rcx
	movl	%edx, %eax
	subl	$1, %edx
	notl	%eax
	andl	%edx, %eax
	sarl	$31, %eax
	movzbl	%al, %eax
	notl	%eax
	andl	%eax, %r8d
	cmpq	%rcx, %rdi
	jne	.L1471
	movzbl	-1(%r12,%rsi), %eax
	movl	%eax, %edi
	subl	$1, %eax
	notl	%edi
	andl	%eax, %edi
	movl	1524(%rbx), %eax
	sarl	$31, %edi
	andl	%r8d, %edi
	leaq	(%r12,%rsi), %r8
	movl	%eax, %edx
	movzbl	1(%r12,%rsi), %esi
	movzbl	(%r8), %ecx
	sarl	$8, %edx
	movzbl	%al, %eax
	xorl	%esi, %eax
	xorl	%ecx, %edx
	movl	%edx, %r9d
	subl	$1, %edx
	notl	%r9d
	andl	%r9d, %edx
	movl	%eax, %r9d
	subl	$1, %eax
	notl	%r9d
	sarl	$31, %edx
	andl	%r9d, %eax
	sarl	$31, %eax
	andl	%edx, %eax
	testb	$-128, 1494(%rbx)
	je	.L1472
	movl	(%rbx), %edx
	movl	%edx, %r9d
	movzbl	%dl, %edx
	sarl	$8, %r9d
	xorl	%edx, %esi
	xorl	%r9d, %ecx
	movl	%ecx, %edx
	subl	$1, %ecx
	notl	%edx
	andl	%ecx, %edx
	movl	%esi, %ecx
	subl	$1, %esi
	notl	%ecx
	sarl	$31, %edx
	andl	%ecx, %esi
	sarl	$31, %esi
	andl	%edx, %esi
	orl	%esi, %eax
.L1472:
	andl	%eax, %edi
	movq	-120(%rbp), %rsi
	movq	%r8, %rcx
	leaq	-64(%rbp), %r10
	movzbl	%dil, %edi
	movl	%edi, %r9d
	notl	%r9d
	.p2align 4,,10
	.p2align 3
.L1473:
	movzbl	(%rcx), %eax
	movzbl	(%rsi), %edx
	addq	$1, %rsi
	addq	$1, %rcx
	andl	%edi, %eax
	andl	%r9d, %edx
	orl	%edx, %eax
	movb	%al, -1(%rcx)
	cmpq	%r10, %rsi
	jne	.L1473
	movl	$48, %edx
	movq	%r8, %rsi
	movq	%rbx, %rdi
	xorl	%ecx, %ecx
	call	ssl_generate_master_secret@PLT
	movl	$3149, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	testl	%eax, %eax
	je	.L1532
	call	CRYPTO_free@PLT
	movl	$2, %eax
	jmp	.L1452
	.p2align 4,,10
	.p2align 3
.L1542:
	movq	8(%r12), %rax
	testq	%rax, %rax
	je	.L1552
	movq	168(%rbx), %rdx
	subq	$1, %rax
	movq	576(%rdx), %r14
	movq	(%r12), %rdx
	movzbl	(%rdx), %r13d
	leaq	1(%rdx), %r15
	movq	%rax, 8(%r12)
	movq	%r15, (%r12)
	cmpq	%r13, %rax
	jb	.L1489
	leaq	(%r15,%r13), %rdx
	subq	%r13, %rax
	movq	%rdx, (%r12)
	movq	%rax, 8(%r12)
	jne	.L1489
	testq	%r14, %r14
	je	.L1553
	call	EVP_PKEY_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1492
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	EVP_PKEY_copy_parameters@PLT
	testl	%eax, %eax
	jle	.L1492
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	EVP_PKEY_set1_tls_encodedpoint@PLT
	testl	%eax, %eax
	je	.L1554
	movl	$1, %ecx
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	ssl_derive@PLT
	testl	%eax, %eax
	jne	.L1536
	.p2align 4,,10
	.p2align 3
.L1530:
	movq	%r12, %rdi
	call	EVP_PKEY_free@PLT
	jmp	.L1457
	.p2align 4,,10
	.p2align 3
.L1503:
	movl	$3500, %r9d
	leaq	.LC0(%rip), %r8
	movl	$249, %ecx
	movq	%rbx, %rdi
	movl	$382, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L1457
	.p2align 4,,10
	.p2align 3
.L1489:
	movl	$3252, %r9d
	movl	$159, %ecx
	movl	$412, %edx
	leaq	.LC0(%rip), %r8
	jmp	.L1533
	.p2align 4,,10
	.p2align 3
.L1549:
	movl	$3033, %r9d
	leaq	.LC0(%rip), %r8
	movl	$65, %ecx
	movq	%rbx, %rdi
	movl	$415, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L1457
	.p2align 4,,10
	.p2align 3
.L1543:
	movq	8(%r12), %rax
	cmpq	$1, %rax
	jbe	.L1496
	movq	(%r12), %rdi
	subq	$2, %rax
	movzwl	(%rdi), %esi
	addq	$2, %rdi
	movq	%rax, 8(%r12)
	movq	%rdi, (%r12)
	rolw	$8, %si
	movzwl	%si, %edx
	cmpq	%rdx, %rax
	jb	.L1496
	leaq	(%rdi,%rdx), %rcx
	subq	%rdx, %rax
	movzwl	%si, %esi
	xorl	%edx, %edx
	movq	%rcx, (%r12)
	movq	%rax, 8(%r12)
	call	BN_bin2bn@PLT
	movq	%rax, 2048(%rbx)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1555
	movq	2016(%rbx), %rsi
	call	BN_ucmp@PLT
	testl	%eax, %eax
	jns	.L1500
	movq	2048(%rbx), %rdi
	call	BN_is_zero@PLT
	testl	%eax, %eax
	jne	.L1500
	movq	1296(%rbx), %rax
	movl	$3317, %edx
	leaq	.LC0(%rip), %rsi
	movq	608(%rax), %rdi
	call	CRYPTO_free@PLT
	movq	2008(%rbx), %rdi
	movq	1296(%rbx), %r12
	movl	$3318, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_strdup@PLT
	movq	%rax, 608(%r12)
	movq	1296(%rbx), %rax
	cmpq	$0, 608(%rax)
	je	.L1556
	movq	%rbx, %rdi
	call	srp_generate_server_master_secret@PLT
	testl	%eax, %eax
	jne	.L1537
	jmp	.L1457
	.p2align 4,,10
	.p2align 3
.L1550:
	movl	$3048, %r9d
	movl	$68, %ecx
	movl	$415, %edx
	movl	$80, %esi
	leaq	.LC0(%rip), %r8
.L1528:
	movq	%rbx, %rdi
	call	ossl_statem_fatal@PLT
	movl	$3149, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
.L1532:
	call	CRYPTO_free@PLT
	jmp	.L1457
	.p2align 4,,10
	.p2align 3
.L1548:
	movl	$3026, %r9d
	leaq	.LC0(%rip), %r8
	movl	$120, %ecx
	movq	%rbx, %rdi
	movl	$415, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L1457
	.p2align 4,,10
	.p2align 3
.L1496:
	movl	$3303, %r9d
	leaq	.LC0(%rip), %r8
	movl	$347, %ecx
	movq	%rbx, %rdi
	movl	$416, %edx
	movl	$50, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L1457
	.p2align 4,,10
	.p2align 3
.L1547:
	movl	$3002, %r9d
	leaq	.LC0(%rip), %r8
	movl	$168, %ecx
	movq	%rbx, %rdi
	movl	$415, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L1457
	.p2align 4,,10
	.p2align 3
.L1552:
	movq	%rbx, %rdi
	movl	$3237, %r9d
	movl	$311, %ecx
	movl	$412, %edx
	leaq	.LC0(%rip), %r8
	movl	$40, %esi
	call	ossl_statem_fatal@PLT
	xorl	%edi, %edi
	call	EVP_PKEY_free@PLT
	jmp	.L1457
	.p2align 4,,10
	.p2align 3
.L1481:
	movl	$3195, %r9d
	movl	$130, %ecx
	movl	$411, %edx
	leaq	.LC0(%rip), %r8
.L1535:
	movl	$80, %esi
	movq	%rbx, %rdi
	call	ossl_statem_fatal@PLT
	jmp	.L1530
	.p2align 4,,10
	.p2align 3
.L1500:
	movl	$3313, %r9d
	leaq	.LC0(%rip), %r8
	movl	$371, %ecx
	movq	%rbx, %rdi
	movl	$416, %edx
	movl	$47, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L1457
.L1551:
	movl	$3062, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
.L1531:
	movl	$415, %edx
	movl	$51, %esi
	jmp	.L1528
	.p2align 4,,10
	.p2align 3
.L1546:
	movl	$3183, %r9d
	movl	$171, %ecx
	movl	$411, %edx
	leaq	.LC0(%rip), %r8
	jmp	.L1533
.L1492:
	movl	$3264, %r9d
	leaq	.LC0(%rip), %r8
	movl	$6, %ecx
.L1529:
	movl	$412, %edx
	jmp	.L1535
.L1484:
	movl	$3203, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movq	%rbx, %rdi
	movl	$411, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	movq	%r13, %rdi
	call	BN_free@PLT
	movq	%r12, %rdi
	call	EVP_PKEY_free@PLT
	jmp	.L1457
.L1553:
	movl	$3257, %r9d
	movl	$311, %ecx
	movl	$412, %edx
	leaq	.LC0(%rip), %r8
.L1534:
	movq	%rbx, %rdi
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	xorl	%edi, %edi
	call	EVP_PKEY_free@PLT
	jmp	.L1457
.L1555:
	movl	$3308, %r9d
	leaq	.LC0(%rip), %r8
	movl	$3, %ecx
	movq	%rbx, %rdi
	movl	$416, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L1457
.L1545:
	movl	$3177, %r9d
	movl	$171, %ecx
	movl	$411, %edx
	leaq	.LC0(%rip), %r8
	jmp	.L1534
.L1554:
	movl	$3269, %r9d
	leaq	.LC0(%rip), %r8
	movl	$16, %ecx
	jmp	.L1529
.L1556:
	movl	$3320, %r9d
	leaq	.LC0(%rip), %r8
	movl	$65, %ecx
	movq	%rbx, %rdi
	movl	$416, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L1457
.L1544:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1097:
	.size	tls_process_client_key_exchange, .-tls_process_client_key_exchange
	.p2align 4
	.globl	tls_post_process_client_key_exchange
	.type	tls_post_process_client_key_exchange, @function
tls_post_process_client_key_exchange:
.LFB1098:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movl	116(%rdi), %eax
	testl	%eax, %eax
	jne	.L1558
	movq	1296(%rdi), %rdx
	cmpq	$0, 440(%rdx)
	je	.L1558
	movq	168(%rdi), %rdx
	cmpq	$0, 224(%rdx)
	je	.L1568
	movl	$1, %esi
	call	ssl3_digest_cached_records@PLT
	testl	%eax, %eax
	je	.L1560
.L1563:
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movl	$2, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1558:
	.cfi_restore_state
	xorl	%esi, %esi
	call	ssl3_digest_cached_records@PLT
	testl	%eax, %eax
	jne	.L1563
.L1560:
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1568:
	.cfi_restore_state
	movl	$3563, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movl	%eax, -4(%rbp)
	movl	$384, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	movl	-4(%rbp), %eax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1098:
	.size	tls_post_process_client_key_exchange, .-tls_post_process_client_key_exchange
	.p2align 4
	.globl	tls_process_client_certificate
	.type	tls_process_client_certificate, @function
tls_process_client_certificate:
.LFB1099:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -104(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$0, 128(%rdi)
	call	OPENSSL_sk_new_null@PLT
	movq	-104(%rbp), %rcx
	testq	%rax, %rax
	movq	%rax, %r15
	je	.L1645
	movq	8(%r14), %rax
	movq	8(%rcx), %r13
	movq	(%rcx), %r12
	movq	192(%rax), %rdx
	movl	96(%rdx), %r10d
	andl	$8, %r10d
	jne	.L1572
	movl	(%rax), %eax
	cmpl	$771, %eax
	jle	.L1572
	cmpl	$65536, %eax
	jne	.L1646
.L1572:
	cmpq	$2, %r13
	jbe	.L1575
	movzbl	(%r12), %ebx
	movzbl	1(%r12), %eax
	subq	$3, %r13
	addq	$3, %r12
	salq	$8, %rax
	salq	$16, %rbx
	orq	%rax, %rbx
	movzbl	-1(%r12), %eax
	orq	%rax, %rbx
	cmpq	%r13, %rbx
	ja	.L1575
	subq	%rbx, %r13
	leaq	(%r12,%rbx), %rax
	movq	%rax, (%rcx)
	movq	%r13, 8(%rcx)
	jne	.L1575
	testq	%rbx, %rbx
	je	.L1589
	leaq	-96(%rbp), %rax
	movq	%rax, -104(%rbp)
.L1576:
	cmpq	$2, %rbx
	jbe	.L1579
	movzbl	(%r12), %edx
	movzbl	1(%r12), %eax
	subq	$3, %rbx
	salq	$8, %rax
	salq	$16, %rdx
	orq	%rax, %rdx
	movzbl	2(%r12), %eax
	orq	%rax, %rdx
	cmpq	%rbx, %rdx
	ja	.L1579
	movq	-104(%rbp), %rsi
	addq	$3, %r12
	xorl	%edi, %edi
	subq	%rdx, %rbx
	movq	%r12, -96(%rbp)
	addq	%rdx, %r12
	call	d2i_X509@PLT
	movq	%rax, %r11
	testq	%rax, %rax
	je	.L1647
	cmpq	%r12, -96(%rbp)
	jne	.L1648
	movq	8(%r14), %rax
	movq	192(%rax), %rdx
	movl	96(%rdx), %r10d
	andl	$8, %r10d
	jne	.L1583
	movl	(%rax), %eax
	cmpl	$771, %eax
	jle	.L1583
	cmpl	$65536, %eax
	je	.L1583
	movq	$0, -88(%rbp)
	cmpq	$1, %rbx
	jbe	.L1584
	movzwl	(%r12), %eax
	subq	$2, %rbx
	rolw	$8, %ax
	movzwl	%ax, %eax
	cmpq	%rax, %rbx
	jb	.L1584
	leaq	2(%r12), %rdx
	subq	%rax, %rbx
	xorl	%r9d, %r9d
	testq	%r13, %r13
	sete	%r9b
	movq	%rdx, -80(%rbp)
	leaq	(%rdx,%rax), %r12
	xorl	%r8d, %r8d
	leaq	-88(%rbp), %rcx
	leaq	-80(%rbp), %rsi
	movl	$4096, %edx
	movq	%r14, %rdi
	movl	%r10d, -116(%rbp)
	movq	%r11, -112(%rbp)
	movq	%rax, -72(%rbp)
	call	tls_collect_extensions@PLT
	movq	-112(%rbp), %r11
	movl	-116(%rbp), %r10d
	testl	%eax, %eax
	je	.L1587
	xorl	%r9d, %r9d
	movq	-88(%rbp), %rdx
	testq	%rbx, %rbx
	movq	%r11, %rcx
	sete	%r9b
	movq	%r13, %r8
	movl	$4096, %esi
	movq	%r14, %rdi
	movl	%r10d, -116(%rbp)
	movq	%r11, -112(%rbp)
	call	tls_parse_all_extensions@PLT
	movq	-112(%rbp), %r11
	movl	-116(%rbp), %r10d
	testl	%eax, %eax
	je	.L1587
	movq	-88(%rbp), %rdi
	movl	$3664, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-112(%rbp), %r11
	.p2align 4,,10
	.p2align 3
.L1583:
	movq	%r11, %rsi
	movq	%r15, %rdi
	movq	%r11, -112(%rbp)
	call	OPENSSL_sk_push@PLT
	movq	-112(%rbp), %r11
	testl	%eax, %eax
	je	.L1649
	addq	$1, %r13
	testq	%rbx, %rbx
	jne	.L1576
.L1589:
	movq	%r15, %rdi
	call	OPENSSL_sk_num@PLT
	testl	%eax, %eax
	jg	.L1650
	cmpl	$768, (%r14)
	je	.L1651
	movl	1376(%r14), %eax
	andl	$3, %eax
	cmpl	$3, %eax
	je	.L1652
	movq	168(%r14), %rax
	cmpq	$0, 224(%rax)
	je	.L1592
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	ssl3_digest_cached_records@PLT
	testl	%eax, %eax
	je	.L1641
.L1592:
	cmpl	$4, 1936(%r14)
	movq	1296(%r14), %r11
	je	.L1653
.L1595:
	movq	440(%r11), %rdi
	call	X509_free@PLT
	movq	1296(%r14), %rbx
	movq	%r15, %rdi
	call	OPENSSL_sk_shift@PLT
	movq	1456(%r14), %rdx
	movq	X509_free@GOTPCREL(%rip), %r12
	movq	%rax, 440(%rbx)
	movq	1296(%r14), %rax
	movq	%r12, %rsi
	movq	%rdx, 464(%rax)
	movq	456(%rax), %rdi
	call	OPENSSL_sk_pop_free@PLT
	movq	1296(%r14), %rax
	movq	%r15, 456(%rax)
	movq	8(%r14), %rax
	movq	192(%rax), %rdx
	movl	96(%rdx), %r10d
	andl	$8, %r10d
	jne	.L1642
	movl	(%rax), %eax
	cmpl	$771, %eax
	jle	.L1642
	cmpl	$65536, %eax
	jne	.L1654
.L1642:
	xorl	%r15d, %r15d
	xorl	%r11d, %r11d
	movl	$3, %r10d
	jmp	.L1571
	.p2align 4,,10
	.p2align 3
.L1575:
	movl	$3617, %r9d
	leaq	.LC0(%rip), %r8
	movl	$159, %ecx
.L1644:
	movl	$380, %edx
	movl	$50, %esi
.L1640:
	movq	%r14, %rdi
	call	ossl_statem_fatal@PLT
.L1641:
	movq	X509_free@GOTPCREL(%rip), %r12
	xorl	%r11d, %r11d
	xorl	%r10d, %r10d
.L1571:
	movq	%r11, %rdi
	movl	%r10d, -104(%rbp)
	call	X509_free@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	OPENSSL_sk_pop_free@PLT
	movl	-104(%rbp), %r10d
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1655
	addq	$88, %rsp
	movl	%r10d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1579:
	.cfi_restore_state
	movl	$3625, %r9d
	leaq	.LC0(%rip), %r8
	movl	$135, %ecx
	jmp	.L1644
	.p2align 4,,10
	.p2align 3
.L1646:
	testq	%r13, %r13
	je	.L1573
	movzbl	(%r12), %edx
	subq	$1, %r13
	cmpq	%rdx, %r13
	jb	.L1573
	leaq	1(%r12), %rdi
	movq	1944(%r14), %rsi
	subq	%rdx, %r13
	leaq	(%rdi,%rdx), %r12
	movq	%r13, 8(%rcx)
	movq	%r12, (%rcx)
	testq	%rsi, %rsi
	je	.L1656
	cmpq	%rdx, 1952(%r14)
	je	.L1657
.L1573:
	movl	$3610, %r9d
	leaq	.LC0(%rip), %r8
	movl	$282, %ecx
	movq	%r14, %rdi
	movl	$380, %edx
	movl	$50, %esi
	movl	%r10d, -104(%rbp)
	call	ossl_statem_fatal@PLT
	movq	X509_free@GOTPCREL(%rip), %r12
	movl	-104(%rbp), %r10d
	xorl	%r11d, %r11d
	jmp	.L1571
	.p2align 4,,10
	.p2align 3
.L1645:
	movl	$3601, %r9d
	movl	$65, %ecx
	movl	$380, %edx
	movl	$80, %esi
	leaq	.LC0(%rip), %r8
	jmp	.L1640
	.p2align 4,,10
	.p2align 3
.L1657:
	movl	%r10d, -104(%rbp)
	movq	%rcx, -112(%rbp)
	call	CRYPTO_memcmp@PLT
	movl	-104(%rbp), %r10d
	testl	%eax, %eax
	jne	.L1573
	movq	-112(%rbp), %rcx
	movq	8(%rcx), %r13
	movq	(%rcx), %r12
	jmp	.L1572
	.p2align 4,,10
	.p2align 3
.L1584:
	movl	$3650, %r9d
	leaq	.LC0(%rip), %r8
	movl	$271, %ecx
	movq	%r14, %rdi
	movl	$380, %edx
	movl	$50, %esi
	movl	%r10d, -112(%rbp)
	movq	%r11, -104(%rbp)
	call	ossl_statem_fatal@PLT
	movq	X509_free@GOTPCREL(%rip), %r12
	movq	-104(%rbp), %r11
	movl	-112(%rbp), %r10d
	jmp	.L1571
	.p2align 4,,10
	.p2align 3
.L1656:
	testq	%rdx, %rdx
	je	.L1572
	jmp	.L1573
.L1647:
	movq	%rax, -104(%rbp)
	movl	$3634, %r9d
	movl	$13, %ecx
	leaq	.LC0(%rip), %r8
	movl	$380, %edx
	movl	$50, %esi
.L1638:
	movq	%r14, %rdi
	call	ossl_statem_fatal@PLT
	movq	X509_free@GOTPCREL(%rip), %r12
	movq	-104(%rbp), %r11
	xorl	%r10d, %r10d
	jmp	.L1571
.L1648:
	movq	%rax, -104(%rbp)
	movl	$3639, %r9d
	movl	$135, %ecx
	leaq	.LC0(%rip), %r8
	movl	$380, %edx
	movl	$50, %esi
	jmp	.L1638
.L1650:
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	ssl_verify_cert_chain@PLT
	movl	%eax, %ecx
	testl	%eax, %eax
	jle	.L1658
	cmpl	$1, %eax
	je	.L1594
	movl	$3707, %r9d
	movl	$380, %edx
	movl	$40, %esi
	leaq	.LC0(%rip), %r8
	jmp	.L1640
.L1649:
	movq	%r11, -104(%rbp)
	movl	$3668, %r9d
.L1643:
	movl	$65, %ecx
	movl	$380, %edx
	movl	$80, %esi
	leaq	.LC0(%rip), %r8
	jmp	.L1638
.L1653:
	xorl	%esi, %esi
	movq	%r11, %rdi
	call	ssl_session_dup@PLT
	movl	$3730, %r9d
	movq	%rax, -104(%rbp)
	testq	%rax, %rax
	je	.L1643
	movq	1296(%r14), %rdi
	call	SSL_SESSION_free@PLT
	movq	-104(%rbp), %r11
	movq	%r11, 1296(%r14)
	jmp	.L1595
.L1594:
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %rdi
	call	X509_get0_pubkey@PLT
	testq	%rax, %rax
	jne	.L1592
	movq	%rax, -104(%rbp)
	movl	$3713, %r9d
	movl	$247, %ecx
	leaq	.LC0(%rip), %r8
	movl	$380, %edx
	movl	$40, %esi
	jmp	.L1638
.L1587:
	movq	-88(%rbp), %rdi
	movl	$3661, %edx
	leaq	.LC0(%rip), %rsi
	movl	%r10d, -112(%rbp)
	movq	%r11, -104(%rbp)
	call	CRYPTO_free@PLT
	movq	X509_free@GOTPCREL(%rip), %r12
	movq	-104(%rbp), %r11
	movl	-112(%rbp), %r10d
	jmp	.L1571
.L1651:
	movl	$3679, %r9d
	movl	$176, %ecx
	movl	$380, %edx
	movl	$40, %esi
	leaq	.LC0(%rip), %r8
	jmp	.L1640
.L1658:
	movl	1456(%r14), %edi
	call	ssl_x509err2alert@PLT
	movl	$3701, %r9d
	movl	$134, %ecx
	leaq	.LC0(%rip), %r8
	movl	%eax, %esi
	movl	$380, %edx
	jmp	.L1640
.L1652:
	movl	$3687, %r9d
	movl	$199, %ecx
	movl	$380, %edx
	movl	$116, %esi
	leaq	.LC0(%rip), %r8
	jmp	.L1640
.L1654:
	movl	$1, %esi
	movq	%r14, %rdi
	movl	%r10d, -104(%rbp)
	call	ssl3_digest_cached_records@PLT
	movl	-104(%rbp), %r10d
	testl	%eax, %eax
	je	.L1639
	movq	8(%r14), %rax
	movq	192(%rax), %rdx
	movl	96(%rdx), %r10d
	andl	$8, %r10d
	movl	%r10d, -104(%rbp)
	jne	.L1642
	movl	(%rax), %eax
	cmpl	$771, %eax
	jle	.L1642
	cmpl	$65536, %eax
	je	.L1642
	leaq	1240(%r14), %rcx
	leaq	1176(%r14), %rsi
	movl	$64, %edx
	movq	%r14, %rdi
	call	ssl_handshake_hash@PLT
	movl	-104(%rbp), %r10d
	testl	%eax, %eax
	je	.L1602
	movq	$0, 6232(%r14)
	jmp	.L1642
.L1602:
	xorl	%r15d, %r15d
.L1639:
	xorl	%r11d, %r11d
	jmp	.L1571
.L1655:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1099:
	.size	tls_process_client_certificate, .-tls_process_client_certificate
	.p2align 4
	.globl	tls_construct_cert_status_body
	.type	tls_construct_cert_status_body, @function
tls_construct_cert_status_body:
.LFB1105:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movl	1608(%rdi), %esi
	movq	%rdi, %r12
	movq	%r13, %rdi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L1662
	movq	1656(%r12), %rdx
	movq	1648(%r12), %rsi
	movl	$3, %ecx
	movq	%r13, %rdi
	call	WPACKET_sub_memcpy__@PLT
	testl	%eax, %eax
	je	.L1662
	popq	%r12
	movl	$1, %eax
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1662:
	.cfi_restore_state
	movq	%r12, %rdi
	movl	$4184, %r9d
	movl	$68, %ecx
	movl	$494, %edx
	leaq	.LC0(%rip), %r8
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	popq	%r12
	xorl	%eax, %eax
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1105:
	.size	tls_construct_cert_status_body, .-tls_construct_cert_status_body
	.p2align 4
	.globl	tls_process_next_proto
	.type	tls_process_next_proto, @function
tls_process_next_proto:
.LFB1107:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	8(%rsi), %rax
	testq	%rax, %rax
	je	.L1670
	movq	(%rsi), %rdx
	subq	$1, %rax
	movzbl	(%rdx), %ebx
	cmpq	%rbx, %rax
	jb	.L1670
	leaq	1(%rdx), %r13
	subq	%rbx, %rax
	leaq	0(%r13,%rbx), %rdx
	movq	%rax, 8(%rsi)
	movq	%rdx, (%rsi)
	je	.L1670
	movzbl	(%rdx), %ecx
	subq	$1, %rax
	cmpq	%rcx, %rax
	jb	.L1670
	subq	%rcx, %rax
	leaq	1(%rdx,%rcx), %rdx
	movq	%rdx, (%rsi)
	movq	%rax, 8(%rsi)
	jne	.L1670
	movq	1792(%rdi), %rdi
	movl	$420, %edx
	leaq	.LC1(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 1792(%r12)
	testq	%rbx, %rbx
	jne	.L1679
.L1671:
	movq	%rbx, 1800(%r12)
	movl	$3, %eax
	jmp	.L1667
	.p2align 4,,10
	.p2align 3
.L1670:
	movl	$4222, %r9d
	leaq	.LC0(%rip), %r8
	movl	$159, %ecx
	movq	%r12, %rdi
	movl	$383, %edx
	movl	$50, %esi
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
.L1667:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1679:
	.cfi_restore_state
	movl	$429, %ecx
	leaq	.LC1(%rip), %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	CRYPTO_memdup@PLT
	movq	%rax, 1792(%r12)
	testq	%rax, %rax
	jne	.L1671
	movl	$4229, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movq	%r12, %rdi
	movl	$383, %edx
	movl	$80, %esi
	movq	$0, 1800(%r12)
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	jmp	.L1667
	.cfi_endproc
.LFE1107:
	.size	tls_process_next_proto, .-tls_process_next_proto
	.p2align 4
	.globl	tls_process_end_of_early_data
	.type	tls_process_end_of_early_data, @function
tls_process_end_of_early_data:
.LFB1109:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	cmpq	$0, 8(%rsi)
	jne	.L1687
	movl	132(%rdi), %eax
	subl	$10, %eax
	cmpl	$1, %eax
	ja	.L1688
	leaq	2112(%rdi), %rdi
	call	RECORD_LAYER_processed_read_pending@PLT
	testl	%eax, %eax
	jne	.L1689
	movq	8(%r12), %rax
	movl	$161, %esi
	movq	%r12, %rdi
	movl	$12, 132(%r12)
	movq	192(%rax), %rax
	call	*32(%rax)
	cmpl	$1, %eax
	sbbl	%eax, %eax
	notl	%eax
	andl	$3, %eax
.L1680:
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1688:
	.cfi_restore_state
	movl	$4261, %r9d
	movl	$68, %ecx
	movl	$537, %edx
	movl	$80, %esi
	leaq	.LC0(%rip), %r8
	call	ossl_statem_fatal@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1687:
	.cfi_restore_state
	movl	$4254, %r9d
	movl	$159, %ecx
	movl	$537, %edx
	movl	$50, %esi
	leaq	.LC0(%rip), %r8
	call	ossl_statem_fatal@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1689:
	.cfi_restore_state
	movl	$4271, %r9d
	leaq	.LC0(%rip), %r8
	movl	$182, %ecx
	movq	%r12, %rdi
	movl	$537, %edx
	movl	$10, %esi
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	jmp	.L1680
	.cfi_endproc
.LFE1109:
	.size	tls_process_end_of_early_data, .-tls_process_end_of_early_data
	.p2align 4
	.globl	ossl_statem_server_process_message
	.type	ossl_statem_server_process_message, @function
ossl_statem_server_process_message:
.LFB1076:
	.cfi_startproc
	endbr64
	movl	92(%rdi), %eax
	subl	$20, %eax
	cmpl	$29, %eax
	ja	.L1691
	leaq	.L1693(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L1693:
	.long	.L1701-.L1693
	.long	.L1691-.L1693
	.long	.L1691-.L1693
	.long	.L1691-.L1693
	.long	.L1691-.L1693
	.long	.L1691-.L1693
	.long	.L1691-.L1693
	.long	.L1700-.L1693
	.long	.L1699-.L1693
	.long	.L1698-.L1693
	.long	.L1697-.L1693
	.long	.L1696-.L1693
	.long	.L1695-.L1693
	.long	.L1691-.L1693
	.long	.L1691-.L1693
	.long	.L1691-.L1693
	.long	.L1691-.L1693
	.long	.L1691-.L1693
	.long	.L1691-.L1693
	.long	.L1691-.L1693
	.long	.L1691-.L1693
	.long	.L1691-.L1693
	.long	.L1691-.L1693
	.long	.L1691-.L1693
	.long	.L1694-.L1693
	.long	.L1691-.L1693
	.long	.L1691-.L1693
	.long	.L1691-.L1693
	.long	.L1691-.L1693
	.long	.L1692-.L1693
	.text
	.p2align 4,,10
	.p2align 3
.L1691:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$68, %ecx
	movl	$603, %edx
	movl	$80, %esi
	movl	$1189, %r9d
	leaq	.LC0(%rip), %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1701:
	.cfi_restore 6
	jmp	tls_process_client_hello
	.p2align 4,,10
	.p2align 3
.L1700:
	jmp	tls_process_client_certificate
	.p2align 4,,10
	.p2align 3
.L1699:
	jmp	tls_process_client_key_exchange
	.p2align 4,,10
	.p2align 3
.L1698:
	jmp	tls_process_cert_verify@PLT
	.p2align 4,,10
	.p2align 3
.L1697:
	jmp	tls_process_next_proto
	.p2align 4,,10
	.p2align 3
.L1696:
	jmp	tls_process_change_cipher_spec@PLT
	.p2align 4,,10
	.p2align 3
.L1695:
	jmp	tls_process_finished@PLT
	.p2align 4,,10
	.p2align 3
.L1694:
	jmp	tls_process_key_update@PLT
	.p2align 4,,10
	.p2align 3
.L1692:
	jmp	tls_process_end_of_early_data
	.cfi_endproc
.LFE1076:
	.size	ossl_statem_server_process_message, .-ossl_statem_server_process_message
	.section	.rodata
	.align 8
	.type	nonce_label.25141, @object
	.size	nonce_label.25141, 11
nonce_label.25141:
	.string	"resumption"
	.align 32
	.type	kSafariExtensionsBlock.24850, @object
	.size	kSafariExtensionsBlock.24850, 34
kSafariExtensionsBlock.24850:
	.string	""
	.string	"\n"
	.string	"\b"
	.string	"\006"
	.string	"\027"
	.string	"\030"
	.string	"\031"
	.string	"\013"
	.string	"\002\001"
	.string	""
	.string	"\r"
	.string	"\f"
	.ascii	"\n\005\001\004\001\002\001\004\003\002\003"
	.type	null_compression.24864, @object
	.size	null_compression.24864, 1
null_compression.24864:
	.zero	1
	.globl	GOST_KX_MESSAGE_it
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC3:
	.string	"GOST_KX_MESSAGE"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	GOST_KX_MESSAGE_it, @object
	.size	GOST_KX_MESSAGE_it, 56
GOST_KX_MESSAGE_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	GOST_KX_MESSAGE_seq_tt
	.quad	2
	.quad	0
	.quad	16
	.quad	.LC3
	.section	.rodata.str1.1
.LC4:
	.string	"kxBlob"
.LC5:
	.string	"opaqueBlob"
	.section	.data.rel.ro,"aw"
	.align 32
	.type	GOST_KX_MESSAGE_seq_tt, @object
	.size	GOST_KX_MESSAGE_seq_tt, 80
GOST_KX_MESSAGE_seq_tt:
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC4
	.quad	ASN1_ANY_it
	.quad	1
	.quad	0
	.quad	8
	.quad	.LC5
	.quad	ASN1_ANY_it
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC2:
	.quad	1
	.quad	1
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
