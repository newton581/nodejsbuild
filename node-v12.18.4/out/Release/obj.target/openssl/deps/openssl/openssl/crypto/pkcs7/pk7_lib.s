	.file	"pk7_lib.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/pkcs7/pk7_lib.c"
	.text
	.p2align 4
	.globl	PKCS7_ctrl
	.type	PKCS7_ctrl, @function
PKCS7_ctrl:
.LFB829:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movl	%esi, %ebx
	subq	$8, %rsp
	movq	24(%rdi), %rdi
	call	OBJ_obj2nid@PLT
	cmpl	$1, %ebx
	je	.L2
	cmpl	$2, %ebx
	je	.L3
	movl	$57, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$110, %edx
	xorl	%r13d, %r13d
	movl	$104, %esi
	movl	$33, %edi
	call	ERR_put_error@PLT
.L1:
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	.cfi_restore_state
	cmpl	$22, %eax
	jne	.L5
	movl	%r13d, 20(%r12)
	movslq	%r13d, %r13
	testq	%r13, %r13
	je	.L1
	movq	32(%r12), %rax
	movq	40(%rax), %rax
	movq	24(%rax), %rdi
	call	OBJ_obj2nid@PLT
	cmpl	$21, %eax
	jne	.L1
	movq	32(%r12), %rax
	movq	40(%rax), %rax
	movq	32(%rax), %rdi
	call	ASN1_OCTET_STRING_free@PLT
	movq	32(%r12), %rax
	movq	40(%rax), %rax
	movq	$0, 32(%rax)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L3:
	cmpl	$22, %eax
	jne	.L7
	movq	32(%r12), %rax
	testq	%rax, %rax
	je	.L9
	movq	40(%rax), %rax
	xorl	%r13d, %r13d
	cmpq	$0, 32(%rax)
	sete	%al
	sete	%r13b
	movzbl	%al, %eax
.L8:
	movl	%eax, 20(%r12)
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	.cfi_restore_state
	movl	$36, %r8d
.L15:
	leaq	.LC0(%rip), %rcx
	movl	$104, %edx
	movl	$104, %esi
	xorl	%r13d, %r13d
	movl	$33, %edi
	call	ERR_put_error@PLT
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	.cfi_restore_state
	movl	$50, %r8d
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L9:
	movl	$1, %eax
	movl	$1, %r13d
	jmp	.L8
	.cfi_endproc
.LFE829:
	.size	PKCS7_ctrl, .-PKCS7_ctrl
	.p2align 4
	.globl	PKCS7_set_content
	.type	PKCS7_set_content, @function
PKCS7_set_content:
.LFB831:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	24(%rdi), %rdi
	call	OBJ_obj2nid@PLT
	cmpl	$22, %eax
	je	.L18
	cmpl	$25, %eax
	je	.L19
	movl	$99, %r8d
	movl	$112, %edx
	movl	$109, %esi
	movl	$33, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	.cfi_restore_state
	movq	32(%rbx), %rax
	movq	40(%rax), %rdi
	call	PKCS7_free@PLT
	movq	32(%rbx), %rax
	movq	%r12, 40(%rax)
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	.cfi_restore_state
	movq	32(%rbx), %rax
	movq	16(%rax), %rdi
	call	PKCS7_free@PLT
	movq	32(%rbx), %rax
	movq	%r12, 16(%rax)
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE831:
	.size	PKCS7_set_content, .-PKCS7_set_content
	.p2align 4
	.globl	PKCS7_set_type
	.type	PKCS7_set_type, @function
PKCS7_set_type:
.LFB832:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movl	%esi, %edi
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%esi, %ebx
	subl	$21, %ebx
	subq	$16, %rsp
	call	OBJ_nid2obj@PLT
	cmpl	$5, %ebx
	ja	.L25
	leaq	.L27(%rip), %rcx
	movq	%rax, 24(%r12)
	movslq	(%rcx,%rbx,4), %rdx
	addq	%rcx, %rdx
	notrack jmp	*%rdx
	.section	.rodata
	.align 4
	.align 4
.L27:
	.long	.L32-.L27
	.long	.L31-.L27
	.long	.L30-.L27
	.long	.L29-.L27
	.long	.L28-.L27
	.long	.L26-.L27
	.text
	.p2align 4,,10
	.p2align 3
.L28:
	call	PKCS7_DIGEST_new@PLT
	movq	%rax, 32(%r12)
	testq	%rax, %rax
	je	.L59
	movq	(%rax), %rdi
	xorl	%esi, %esi
	call	ASN1_INTEGER_set@PLT
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
.L24:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	.cfi_restore_state
	call	PKCS7_ENCRYPT_new@PLT
	movq	%rax, 32(%r12)
	testq	%rax, %rax
	je	.L59
	movq	(%rax), %rdi
	xorl	%esi, %esi
	call	ASN1_INTEGER_set@PLT
	testl	%eax, %eax
	jne	.L60
	.p2align 4,,10
	.p2align 3
.L59:
	xorl	%eax, %eax
.L62:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	.cfi_restore_state
	call	ASN1_OCTET_STRING_new@PLT
	testq	%rax, %rax
	movq	%rax, 32(%r12)
	setne	%al
	addq	$16, %rsp
	popq	%rbx
	movzbl	%al, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L31:
	.cfi_restore_state
	call	PKCS7_SIGNED_new@PLT
	movq	%rax, 32(%r12)
	testq	%rax, %rax
	je	.L59
	movq	(%rax), %rdi
	movl	$1, %esi
	call	ASN1_INTEGER_set@PLT
	testl	%eax, %eax
	je	.L61
	movl	$1, %eax
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L30:
	call	PKCS7_ENVELOPE_new@PLT
	movq	%rax, 32(%r12)
	testq	%rax, %rax
	je	.L59
	movq	(%rax), %rdi
	xorl	%esi, %esi
	call	ASN1_INTEGER_set@PLT
	testl	%eax, %eax
	je	.L59
	movq	32(%r12), %rax
	movl	$21, %edi
	movq	16(%rax), %rbx
	call	OBJ_nid2obj@PLT
	movq	%rax, (%rbx)
	movl	$1, %eax
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L29:
	call	PKCS7_SIGN_ENVELOPE_new@PLT
	movq	%rax, 32(%r12)
	testq	%rax, %rax
	je	.L59
	movq	(%rax), %rdi
	movl	$1, %esi
	call	ASN1_INTEGER_set@PLT
	testl	%eax, %eax
	je	.L59
	movq	32(%r12), %rax
	movl	$21, %edi
	movq	40(%rax), %rbx
	call	OBJ_nid2obj@PLT
	movq	%rax, (%rbx)
	movl	$1, %eax
	jmp	.L24
.L25:
	movl	$170, %r8d
	movl	$112, %edx
	movl	$110, %esi
	movl	$33, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L60:
	movq	32(%r12), %rax
	movl	$21, %edi
	movq	8(%rax), %rbx
	call	OBJ_nid2obj@PLT
	movq	%rax, (%rbx)
	movl	$1, %eax
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L61:
	movq	32(%r12), %rdi
	movl	%eax, -20(%rbp)
	call	PKCS7_SIGNED_free@PLT
	movl	-20(%rbp), %eax
	movq	$0, 32(%r12)
	jmp	.L24
	.cfi_endproc
.LFE832:
	.size	PKCS7_set_type, .-PKCS7_set_type
	.p2align 4
	.globl	PKCS7_content_new
	.type	PKCS7_content_new, @function
PKCS7_content_new:
.LFB830:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%esi, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	PKCS7_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L65
	movl	%r13d, %esi
	movq	%rax, %rdi
	call	PKCS7_set_type
	testl	%eax, %eax
	jne	.L76
.L65:
	movq	%r12, %rdi
	call	PKCS7_free@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L76:
	.cfi_restore_state
	movq	24(%rbx), %rdi
	call	OBJ_obj2nid@PLT
	cmpl	$22, %eax
	je	.L67
	cmpl	$25, %eax
	je	.L68
	movl	$99, %r8d
	movl	$112, %edx
	movl	$109, %esi
	movl	$33, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L68:
	movq	32(%rbx), %rax
	movq	16(%rax), %rdi
	call	PKCS7_free@PLT
	movq	32(%rbx), %rax
	movq	%r12, 16(%rax)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L67:
	.cfi_restore_state
	movq	32(%rbx), %rax
	movq	40(%rax), %rdi
	call	PKCS7_free@PLT
	movq	32(%rbx), %rax
	movq	%r12, 40(%rax)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE830:
	.size	PKCS7_content_new, .-PKCS7_content_new
	.p2align 4
	.globl	PKCS7_set0_type_other
	.type	PKCS7_set0_type_other, @function
PKCS7_set0_type_other:
.LFB833:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	%esi, %edi
	subq	$24, %rsp
	movq	%rdx, -24(%rbp)
	call	OBJ_nid2obj@PLT
	movq	%rax, %xmm0
	movl	$1, %eax
	movhps	-24(%rbp), %xmm0
	movups	%xmm0, 24(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE833:
	.size	PKCS7_set0_type_other, .-PKCS7_set0_type_other
	.p2align 4
	.globl	PKCS7_add_signer
	.type	PKCS7_add_signer, @function
PKCS7_add_signer:
.LFB834:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	24(%rdi), %rdi
	call	OBJ_obj2nid@PLT
	cmpl	$22, %eax
	je	.L81
	cmpl	$24, %eax
	je	.L81
	movl	$203, %r8d
	movl	$113, %edx
	movl	$103, %esi
	movl	$33, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
.L79:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L81:
	.cfi_restore_state
	movq	32(%rbx), %rax
	xorl	%ebx, %ebx
	movq	32(%rax), %r15
	movq	8(%rax), %r12
	movq	16(%r14), %rax
	movq	(%rax), %rdi
	call	OBJ_obj2nid@PLT
	movl	%eax, %r13d
	jmp	.L85
	.p2align 4,,10
	.p2align 3
.L87:
	movq	%r12, %rdi
	movl	%ebx, %esi
	call	OPENSSL_sk_value@PLT
	movq	(%rax), %rdi
	call	OBJ_obj2nid@PLT
	cmpl	%r13d, %eax
	je	.L86
	addl	$1, %ebx
.L85:
	movq	%r12, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jl	.L87
	call	X509_ALGOR_new@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L90
	call	ASN1_TYPE_new@PLT
	movq	%rax, 8(%rbx)
	testq	%rax, %rax
	je	.L90
	movl	%r13d, %edi
	call	OBJ_nid2obj@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, (%rbx)
	movq	8(%rbx), %rax
	movl	$5, (%rax)
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L99
.L86:
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	setne	%al
	addq	$24, %rsp
	popq	%rbx
	movzbl	%al, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L90:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	X509_ALGOR_free@PLT
	movl	$222, %r8d
	movl	$65, %edx
	leaq	.LC0(%rip), %rcx
	movl	$103, %esi
	movl	$33, %edi
	call	ERR_put_error@PLT
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L99:
	.cfi_restore_state
	movq	%rbx, %rdi
	movl	%eax, -52(%rbp)
	call	X509_ALGOR_free@PLT
	movl	-52(%rbp), %eax
	jmp	.L79
	.cfi_endproc
.LFE834:
	.size	PKCS7_add_signer, .-PKCS7_add_signer
	.p2align 4
	.globl	PKCS7_add_certificate
	.type	PKCS7_add_certificate, @function
PKCS7_add_certificate:
.LFB835:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	24(%rdi), %rdi
	call	OBJ_obj2nid@PLT
	cmpl	$22, %eax
	je	.L102
	cmpl	$24, %eax
	je	.L102
	movl	$252, %r8d
	movl	$113, %edx
	movl	$100, %esi
	movl	$33, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$16, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L102:
	.cfi_restore_state
	movq	32(%rbx), %rax
	leaq	16(%rax), %rbx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L111
.L106:
	cmpq	$0, (%rbx)
	je	.L112
	movq	%r12, %rdi
	call	X509_up_ref@PLT
	movq	(%rbx), %rdi
	movq	%r12, %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L113
	addq	$16, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L113:
	.cfi_restore_state
	movq	%r12, %rdi
	movl	%eax, -20(%rbp)
	call	X509_free@PLT
	movl	-20(%rbp), %eax
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L112:
	.cfi_restore_state
	movl	$259, %r8d
	movl	$65, %edx
	movl	$100, %esi
	movl	$33, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$16, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L111:
	.cfi_restore_state
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, (%rbx)
	jmp	.L106
	.cfi_endproc
.LFE835:
	.size	PKCS7_add_certificate, .-PKCS7_add_certificate
	.p2align 4
	.globl	PKCS7_add_crl
	.type	PKCS7_add_crl, @function
PKCS7_add_crl:
.LFB836:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	24(%rdi), %rdi
	call	OBJ_obj2nid@PLT
	cmpl	$22, %eax
	je	.L116
	cmpl	$24, %eax
	je	.L116
	movl	$284, %r8d
	movl	$113, %edx
	movl	$101, %esi
	movl	$33, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$16, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L116:
	.cfi_restore_state
	movq	32(%rbx), %rax
	leaq	24(%rax), %rbx
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L125
.L120:
	cmpq	$0, (%rbx)
	je	.L126
	movq	%r12, %rdi
	call	X509_CRL_up_ref@PLT
	movq	(%rbx), %rdi
	movq	%r12, %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L127
	addq	$16, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L127:
	.cfi_restore_state
	movq	%r12, %rdi
	movl	%eax, -20(%rbp)
	call	X509_CRL_free@PLT
	movl	-20(%rbp), %eax
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L126:
	.cfi_restore_state
	movl	$291, %r8d
	movl	$65, %edx
	movl	$101, %esi
	movl	$33, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$16, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L125:
	.cfi_restore_state
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, (%rbx)
	jmp	.L120
	.cfi_endproc
.LFE836:
	.size	PKCS7_add_crl, .-PKCS7_add_crl
	.p2align 4
	.globl	PKCS7_SIGNER_INFO_set
	.type	PKCS7_SIGNER_INFO_set, @function
PKCS7_SIGNER_INFO_set:
.LFB837:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movl	$1, %esi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	(%rdi), %rdi
	call	ASN1_INTEGER_set@PLT
	testl	%eax, %eax
	jne	.L129
.L150:
	xorl	%eax, %eax
.L128:
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L129:
	.cfi_restore_state
	movq	%r13, %rdi
	call	X509_get_issuer_name@PLT
	movq	8(%r12), %rdi
	movq	%rax, %rsi
	call	X509_NAME_set@PLT
	testl	%eax, %eax
	je	.L150
	movq	8(%r12), %rax
	movq	8(%rax), %rdi
	call	ASN1_INTEGER_free@PLT
	movq	%r13, %rdi
	call	X509_get_serialNumber@PLT
	movq	8(%r12), %r13
	movq	%rax, %rdi
	call	ASN1_INTEGER_dup@PLT
	movq	%rax, 8(%r13)
	testq	%rax, %rax
	je	.L150
	movq	%r14, %rdi
	call	EVP_PKEY_up_ref@PLT
	movq	%r14, 56(%r12)
	movq	%r15, %rdi
	call	EVP_MD_type@PLT
	movl	%eax, %edi
	call	OBJ_nid2obj@PLT
	movq	16(%r12), %rdi
	xorl	%ecx, %ecx
	movl	$5, %edx
	movq	%rax, %rsi
	call	X509_ALGOR_set0@PLT
	movq	16(%r14), %rax
	testq	%rax, %rax
	je	.L132
	movq	176(%rax), %rax
	testq	%rax, %rax
	je	.L132
	xorl	%edx, %edx
	movq	%r12, %rcx
	movl	$1, %esi
	movq	%r14, %rdi
	call	*%rax
	movl	%eax, %edx
	movl	$1, %eax
	testl	%edx, %edx
	jg	.L128
	cmpl	$-2, %edx
	jne	.L151
.L132:
	movl	$343, %r8d
	movl	$148, %edx
	movl	$129, %esi
	movl	$33, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L150
	.p2align 4,,10
	.p2align 3
.L151:
	movl	$338, %r8d
	movl	$147, %edx
	movl	$129, %esi
	movl	$33, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L128
	.cfi_endproc
.LFE837:
	.size	PKCS7_SIGNER_INFO_set, .-PKCS7_SIGNER_INFO_set
	.p2align 4
	.globl	PKCS7_add_signature
	.type	PKCS7_add_signature, @function
PKCS7_add_signature:
.LFB838:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rcx, %rcx
	je	.L167
.L153:
	call	PKCS7_SIGNER_INFO_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L156
	movq	%r13, %rcx
	movq	%rbx, %rdx
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	PKCS7_SIGNER_INFO_set
	testl	%eax, %eax
	je	.L156
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	PKCS7_add_signer
	testl	%eax, %eax
	je	.L156
.L152:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L168
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L167:
	.cfi_restore_state
	leaq	-60(%rbp), %rsi
	movq	%rdx, %rdi
	call	EVP_PKEY_get_default_digest_nid@PLT
	testl	%eax, %eax
	jle	.L154
	movl	-60(%rbp), %edi
	call	OBJ_nid2sn@PLT
	movq	%rax, %rdi
	call	EVP_get_digestbyname@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	jne	.L153
	movl	$360, %r8d
	movl	$151, %edx
	movl	$131, %esi
	movl	$33, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	.p2align 4,,10
	.p2align 3
.L154:
	xorl	%r12d, %r12d
	.p2align 4,,10
	.p2align 3
.L156:
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	PKCS7_SIGNER_INFO_free@PLT
	jmp	.L152
.L168:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE838:
	.size	PKCS7_add_signature, .-PKCS7_add_signature
	.p2align 4
	.globl	PKCS7_set_digest
	.type	PKCS7_set_digest, @function
PKCS7_set_digest:
.LFB839:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	24(%rdi), %rdi
	call	OBJ_obj2nid@PLT
	cmpl	$25, %eax
	je	.L174
	movl	$389, %r8d
	movl	$113, %edx
	movl	$126, %esi
	movl	$33, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L174:
	.cfi_restore_state
	movq	32(%rbx), %rax
	movq	8(%rax), %r13
	call	ASN1_TYPE_new@PLT
	movq	%rax, 8(%r13)
	testq	%rax, %rax
	je	.L175
	movq	32(%rbx), %rax
	movq	%r12, %rdi
	movq	8(%rax), %rax
	movq	8(%rax), %rax
	movl	$5, (%rax)
	call	EVP_MD_type@PLT
	movl	%eax, %edi
	movq	32(%rbx), %rax
	movq	8(%rax), %rbx
	call	OBJ_nid2obj@PLT
	movq	%rax, (%rbx)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L175:
	.cfi_restore_state
	movl	$381, %r8d
	movl	$65, %edx
	movl	$126, %esi
	movl	$33, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE839:
	.size	PKCS7_set_digest, .-PKCS7_set_digest
	.p2align 4
	.globl	PKCS7_get_signer_info
	.type	PKCS7_get_signer_info, @function
PKCS7_get_signer_info:
.LFB840:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L185
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpq	$0, 32(%rdi)
	je	.L179
	movq	24(%rdi), %rdi
	call	OBJ_obj2nid@PLT
	cmpl	$22, %eax
	je	.L184
	movq	24(%rbx), %rdi
	call	OBJ_obj2nid@PLT
	cmpl	$24, %eax
	jne	.L179
.L184:
	movq	32(%rbx), %rax
	movq	32(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L179:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L185:
	.cfi_restore 3
	.cfi_restore 6
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE840:
	.size	PKCS7_get_signer_info, .-PKCS7_get_signer_info
	.p2align 4
	.globl	PKCS7_SIGNER_INFO_get0_algs
	.type	PKCS7_SIGNER_INFO_get0_algs, @function
PKCS7_SIGNER_INFO_get0_algs:
.LFB841:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L187
	movq	56(%rdi), %rax
	movq	%rax, (%rsi)
.L187:
	testq	%rdx, %rdx
	je	.L188
	movq	16(%rdi), %rax
	movq	%rax, (%rdx)
.L188:
	testq	%rcx, %rcx
	je	.L186
	movq	32(%rdi), %rax
	movq	%rax, (%rcx)
.L186:
	ret
	.cfi_endproc
.LFE841:
	.size	PKCS7_SIGNER_INFO_get0_algs, .-PKCS7_SIGNER_INFO_get0_algs
	.p2align 4
	.globl	PKCS7_RECIP_INFO_get0_alg
	.type	PKCS7_RECIP_INFO_get0_alg, @function
PKCS7_RECIP_INFO_get0_alg:
.LFB842:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L199
	movq	16(%rdi), %rax
	movq	%rax, (%rsi)
.L199:
	ret
	.cfi_endproc
.LFE842:
	.size	PKCS7_RECIP_INFO_get0_alg, .-PKCS7_RECIP_INFO_get0_alg
	.p2align 4
	.globl	PKCS7_add_recipient_info
	.type	PKCS7_add_recipient_info, @function
PKCS7_add_recipient_info:
.LFB844:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	24(%rdi), %rdi
	call	OBJ_obj2nid@PLT
	cmpl	$23, %eax
	je	.L205
	cmpl	$24, %eax
	jne	.L206
	movq	32(%rbx), %rax
	movq	48(%rax), %rdi
.L207:
	movq	%r12, %rsi
	call	OPENSSL_sk_push@PLT
	popq	%rbx
	popq	%r12
	testl	%eax, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	setne	%al
	movzbl	%al, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L205:
	.cfi_restore_state
	movq	32(%rbx), %rax
	movq	8(%rax), %rdi
	jmp	.L207
	.p2align 4,,10
	.p2align 3
.L206:
	movl	$452, %r8d
	movl	$113, %edx
	movl	$102, %esi
	movl	$33, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE844:
	.size	PKCS7_add_recipient_info, .-PKCS7_add_recipient_info
	.p2align 4
	.globl	PKCS7_RECIP_INFO_set
	.type	PKCS7_RECIP_INFO_set, @function
PKCS7_RECIP_INFO_set:
.LFB845:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	xorl	%esi, %esi
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rdi
	call	ASN1_INTEGER_set@PLT
	testl	%eax, %eax
	jne	.L211
.L231:
	xorl	%eax, %eax
.L210:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L211:
	.cfi_restore_state
	movq	%r12, %rdi
	call	X509_get_issuer_name@PLT
	movq	8(%rbx), %rdi
	movq	%rax, %rsi
	call	X509_NAME_set@PLT
	testl	%eax, %eax
	je	.L231
	movq	8(%rbx), %rax
	movq	8(%rax), %rdi
	call	ASN1_INTEGER_free@PLT
	movq	%r12, %rdi
	call	X509_get_serialNumber@PLT
	movq	8(%rbx), %r13
	movq	%rax, %rdi
	call	ASN1_INTEGER_dup@PLT
	movq	%rax, 8(%r13)
	testq	%rax, %rax
	je	.L231
	movq	%r12, %rdi
	call	X509_get0_pubkey@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L214
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L214
	movq	176(%rax), %rax
	testq	%rax, %rax
	je	.L214
	xorl	%edx, %edx
	movq	%rbx, %rcx
	movl	$2, %esi
	call	*%rax
	cmpl	$-2, %eax
	je	.L233
	testl	%eax, %eax
	jle	.L234
	movq	%r12, %rdi
	call	X509_up_ref@PLT
	movq	%r12, 32(%rbx)
	movl	$1, %eax
	jmp	.L210
	.p2align 4,,10
	.p2align 3
.L214:
	movl	$480, %r8d
.L232:
	movl	$150, %edx
	movl	$130, %esi
	movl	$33, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L231
	.p2align 4,,10
	.p2align 3
.L234:
	movl	$492, %r8d
	movl	$149, %edx
	movl	$130, %esi
	movl	$33, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L210
	.p2align 4,,10
	.p2align 3
.L233:
	movl	$487, %r8d
	jmp	.L232
	.cfi_endproc
.LFE845:
	.size	PKCS7_RECIP_INFO_set, .-PKCS7_RECIP_INFO_set
	.p2align 4
	.globl	PKCS7_add_recipient
	.type	PKCS7_add_recipient, @function
PKCS7_add_recipient:
.LFB843:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	PKCS7_RECIP_INFO_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L237
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	PKCS7_RECIP_INFO_set
	testl	%eax, %eax
	je	.L237
	movq	24(%rbx), %rdi
	call	OBJ_obj2nid@PLT
	cmpl	$23, %eax
	je	.L239
	cmpl	$24, %eax
	jne	.L240
	movq	32(%rbx), %rax
	movq	48(%rax), %rdi
.L241:
	movq	%r12, %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	jne	.L235
	.p2align 4,,10
	.p2align 3
.L237:
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	PKCS7_RECIP_INFO_free@PLT
.L235:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L240:
	.cfi_restore_state
	movl	$452, %r8d
	movl	$113, %edx
	movl	$102, %esi
	movl	$33, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L237
	.p2align 4,,10
	.p2align 3
.L239:
	movq	32(%rbx), %rax
	movq	8(%rax), %rdi
	jmp	.L241
	.cfi_endproc
.LFE843:
	.size	PKCS7_add_recipient, .-PKCS7_add_recipient
	.p2align 4
	.globl	PKCS7_cert_from_signer_info
	.type	PKCS7_cert_from_signer_info, @function
PKCS7_cert_from_signer_info:
.LFB846:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	24(%rdi), %rdi
	call	OBJ_obj2nid@PLT
	cmpl	$22, %eax
	je	.L253
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L253:
	.cfi_restore_state
	movq	8(%r12), %rax
	movq	32(%rbx), %rcx
	popq	%rbx
	popq	%r12
	movq	8(%rax), %rdx
	movq	16(%rcx), %rdi
	movq	(%rax), %rsi
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	X509_find_by_issuer_and_serial@PLT
	.cfi_endproc
.LFE846:
	.size	PKCS7_cert_from_signer_info, .-PKCS7_cert_from_signer_info
	.p2align 4
	.globl	PKCS7_set_cipher
	.type	PKCS7_set_cipher, @function
PKCS7_set_cipher:
.LFB847:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	24(%rdi), %rdi
	call	OBJ_obj2nid@PLT
	cmpl	$23, %eax
	je	.L255
	cmpl	$24, %eax
	jne	.L256
	movq	32(%rbx), %rax
	movq	%r12, %rdi
	movq	40(%rax), %rbx
	call	EVP_CIPHER_type@PLT
	testl	%eax, %eax
	je	.L261
.L259:
	movq	%r12, 24(%rbx)
	addq	$16, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L255:
	.cfi_restore_state
	movq	32(%rbx), %rax
	movq	%r12, %rdi
	movq	16(%rax), %rbx
	call	EVP_CIPHER_type@PLT
	testl	%eax, %eax
	jne	.L259
.L261:
	movl	$538, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$144, %edx
	movl	%eax, -20(%rbp)
	movl	$108, %esi
	movl	$33, %edi
	call	ERR_put_error@PLT
	movl	-20(%rbp), %eax
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L256:
	.cfi_restore_state
	movl	$531, %r8d
	movl	$113, %edx
	movl	$108, %esi
	movl	$33, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$16, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE847:
	.size	PKCS7_set_cipher, .-PKCS7_set_cipher
	.p2align 4
	.globl	PKCS7_stream
	.type	PKCS7_stream, @function
PKCS7_stream:
.LFB848:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	24(%rsi), %rdi
	call	OBJ_obj2nid@PLT
	cmpl	$23, %eax
	je	.L263
	jg	.L264
	cmpl	$21, %eax
	je	.L265
	cmpl	$22, %eax
	jne	.L274
	movq	32(%r12), %rax
	movq	40(%rax), %rax
	movq	32(%rax), %rax
.L269:
	xorl	%r8d, %r8d
	testq	%rax, %rax
	je	.L262
.L270:
	orq	$16, 16(%rax)
	addq	$8, %rax
	movl	$1, %r8d
	movq	%rax, (%rbx)
.L262:
	popq	%rbx
	movl	%r8d, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L264:
	.cfi_restore_state
	cmpl	$24, %eax
	jne	.L274
	movq	32(%r12), %rax
	movq	40(%rax), %rax
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L270
	call	ASN1_OCTET_STRING_new@PLT
	movq	32(%r12), %rdx
	movq	40(%rdx), %rdx
	movq	%rax, 16(%rdx)
	jmp	.L269
	.p2align 4,,10
	.p2align 3
.L274:
	xorl	%r8d, %r8d
	popq	%rbx
	popq	%r12
	movl	%r8d, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L265:
	.cfi_restore_state
	movq	32(%r12), %rax
	xorl	%r8d, %r8d
	testq	%rax, %rax
	jne	.L270
	jmp	.L262
	.p2align 4,,10
	.p2align 3
.L263:
	movq	32(%r12), %rax
	movq	16(%rax), %rax
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L270
	call	ASN1_OCTET_STRING_new@PLT
	movq	32(%r12), %rdx
	movq	16(%rdx), %rdx
	movq	%rax, 16(%rdx)
	jmp	.L269
	.cfi_endproc
.LFE848:
	.size	PKCS7_stream, .-PKCS7_stream
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
