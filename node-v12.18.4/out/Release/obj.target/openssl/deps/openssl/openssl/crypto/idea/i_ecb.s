	.file	"i_ecb.c"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"idea(int)"
	.text
	.p2align 4
	.globl	IDEA_options
	.type	IDEA_options, @function
IDEA_options:
.LFB0:
	.cfi_startproc
	endbr64
	leaq	.LC0(%rip), %rax
	ret
	.cfi_endproc
.LFE0:
	.size	IDEA_options, .-IDEA_options
	.p2align 4
	.globl	IDEA_ecb_encrypt
	.type	IDEA_ecb_encrypt, @function
IDEA_ecb_encrypt:
.LFB1:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rsi, %rbx
	movq	%rdx, %rsi
	subq	$40, %rsp
	movl	4(%rdi), %ecx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	(%rdi), %eax
	leaq	-48(%rbp), %rdi
	bswap	%ecx
	movd	%ecx, %xmm1
	bswap	%eax
	movd	%eax, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -48(%rbp)
	call	IDEA_encrypt@PLT
	movl	-48(%rbp), %eax
	bswap	%eax
	movl	%eax, (%rbx)
	movl	-40(%rbp), %eax
	bswap	%eax
	movl	%eax, 4(%rbx)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L6
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L6:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1:
	.size	IDEA_ecb_encrypt, .-IDEA_ecb_encrypt
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
