	.file	"eng_dyn.c"
	.text
	.p2align 4
	.type	dynamic_init, @function
dynamic_init:
.LFB872:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE872:
	.size	dynamic_init, .-dynamic_init
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/engine/eng_dyn.c"
	.text
	.p2align 4
	.type	int_free_str, @function
int_free_str:
.LFB866:
	.cfi_startproc
	endbr64
	movl	$122, %edx
	leaq	.LC0(%rip), %rsi
	jmp	CRYPTO_free@PLT
	.cfi_endproc
.LFE866:
	.size	int_free_str, .-int_free_str
	.p2align 4
	.type	dynamic_data_ctx_free_func, @function
dynamic_data_ctx_free_func:
.LFB867:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L4
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	subq	$8, %rsp
	movq	(%rsi), %rdi
	call	DSO_free@PLT
	movq	24(%r12), %rdi
	movl	$141, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	40(%r12), %rdi
	movl	$142, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	80(%r12), %rdi
	leaq	int_free_str(%rip), %rsi
	call	OPENSSL_sk_pop_free@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$144, %edx
	popq	%r12
	.cfi_restore 12
	leaq	.LC0(%rip), %rsi
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_free@PLT
	.p2align 4,,10
	.p2align 3
.L4:
	ret
	.cfi_endproc
.LFE867:
	.size	dynamic_data_ctx_free_func, .-dynamic_data_ctx_free_func
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"v_check"
.LC2:
	.string	"bind_engine"
	.text
	.p2align 4
	.type	dynamic_ctrl, @function
dynamic_ctrl:
.LFB874:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$264, %rsp
	movl	dynamic_ex_data_idx(%rip), %esi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%esi, %esi
	js	.L76
	movq	%r12, %rdi
	call	ENGINE_get_ex_data@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L77
.L14:
	cmpq	$0, (%r15)
	jne	.L78
.L20:
	leal	-200(%rbx), %esi
	cmpl	$6, %esi
	ja	.L21
	leaq	.L23(%rip), %rdx
	movslq	(%rdx,%rsi,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L23:
	.long	.L29-.L23
	.long	.L28-.L23
	.long	.L27-.L23
	.long	.L26-.L23
	.long	.L25-.L23
	.long	.L24-.L23
	.long	.L22-.L23
	.text
	.p2align 4,,10
	.p2align 3
.L76:
	leaq	dynamic_data_ctx_free_func(%rip), %r9
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movl	$10, %edi
	call	CRYPTO_get_ex_new_index@PLT
	movl	%eax, %r15d
	cmpl	$-1, %eax
	je	.L79
	movq	global_engine_lock(%rip), %rdi
	call	CRYPTO_THREAD_write_lock@PLT
	movl	dynamic_ex_data_idx(%rip), %ecx
	testl	%ecx, %ecx
	jns	.L13
	movl	%r15d, dynamic_ex_data_idx(%rip)
.L13:
	movq	global_engine_lock(%rip), %rdi
	call	CRYPTO_THREAD_unlock@PLT
	movl	dynamic_ex_data_idx(%rip), %esi
	movq	%r12, %rdi
	call	ENGINE_get_ex_data@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	jne	.L14
.L77:
	movl	$156, %edx
	leaq	.LC0(%rip), %rsi
	movl	$88, %edi
	call	CRYPTO_zalloc@PLT
	testq	%rax, %rax
	je	.L80
	movq	%rax, -296(%rbp)
	call	OPENSSL_sk_new_null@PLT
	movq	-296(%rbp), %r9
	movq	%rax, 80(%r9)
	testq	%rax, %rax
	je	.L81
	leaq	.LC1(%rip), %rcx
	leaq	.LC2(%rip), %rax
	movl	$1, 72(%r9)
	movq	global_engine_lock(%rip), %rdi
	movq	%rcx, %xmm0
	movq	%rax, %xmm1
	movq	%r9, -296(%rbp)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 56(%r9)
	call	CRYPTO_THREAD_write_lock@PLT
	movl	dynamic_ex_data_idx(%rip), %esi
	movq	%r12, %rdi
	call	ENGINE_get_ex_data@PLT
	movq	-296(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %r15
	je	.L82
	movq	global_engine_lock(%rip), %rdi
	movq	%r9, -296(%rbp)
	call	CRYPTO_THREAD_unlock@PLT
	movq	-296(%rbp), %r9
	movq	80(%r9), %rdi
	call	OPENSSL_sk_free@PLT
	movq	-296(%rbp), %r9
	movl	$190, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r9, %rdi
	call	CRYPTO_free@PLT
	cmpq	$0, (%r15)
	je	.L20
	.p2align 4,,10
	.p2align 3
.L78:
	movl	$301, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$100, %edx
	xorl	%r13d, %r13d
	movl	$180, %esi
	movl	$38, %edi
	call	ERR_put_error@PLT
.L9:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L83
	addq	$264, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L22:
	.cfi_restore_state
	call	DSO_new@PLT
	movq	%rax, (%r15)
	testq	%rax, %rax
	je	.L73
	movq	24(%r15), %rsi
	testq	%rsi, %rsi
	je	.L84
.L36:
	cmpl	$2, 72(%r15)
	jne	.L37
.L41:
	movq	80(%r15), %rdi
	call	OPENSSL_sk_num@PLT
	movl	%eax, -296(%rbp)
	testl	%eax, %eax
	jle	.L38
	xorl	%ebx, %ebx
	leaq	.LC0(%rip), %r14
	.p2align 4,,10
	.p2align 3
.L39:
	movq	80(%r15), %rdi
	movl	%ebx, %esi
	call	OPENSSL_sk_value@PLT
	movq	24(%r15), %rsi
	movq	(%r15), %rdi
	movq	%rax, %rdx
	call	DSO_merge@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L38
	movq	(%r15), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	call	DSO_load@PLT
	testq	%rax, %rax
	jne	.L85
	movl	$391, %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	addl	$1, %ebx
	call	CRYPTO_free@PLT
	cmpl	%ebx, -296(%rbp)
	jne	.L39
	.p2align 4,,10
	.p2align 3
.L38:
	movl	$414, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$132, %edx
	xorl	%r13d, %r13d
	movl	$182, %esi
	movl	$38, %edi
	call	ERR_put_error@PLT
	movq	(%r15), %rdi
	call	DSO_free@PLT
	movq	$0, (%r15)
	jmp	.L9
.L29:
	movq	24(%r15), %rdi
	movl	$309, %edx
	leaq	.LC0(%rip), %rsi
	testq	%r13, %r13
	je	.L74
	cmpb	$0, 0(%r13)
	jne	.L31
.L74:
	call	CRYPTO_free@PLT
	xorl	%r13d, %r13d
	movq	$0, 24(%r15)
	jmp	.L9
.L28:
	xorl	%eax, %eax
	testq	%r14, %r14
	movl	$1, %r13d
	setne	%al
	movl	%eax, 32(%r15)
	jmp	.L9
.L27:
	movq	40(%r15), %rdi
	movl	$322, %edx
	leaq	.LC0(%rip), %rsi
	testq	%r13, %r13
	je	.L75
	cmpb	$0, 0(%r13)
	jne	.L33
.L75:
	call	CRYPTO_free@PLT
	xorl	%r13d, %r13d
	movq	$0, 40(%r15)
	jmp	.L9
.L26:
	movl	$330, %r8d
	cmpq	$2, %r14
	ja	.L71
	movl	%r14d, 48(%r15)
	movl	$1, %r13d
	jmp	.L9
.L25:
	cmpq	$2, %r14
	ja	.L86
	movl	%r14d, 72(%r15)
	movl	$1, %r13d
	jmp	.L9
.L24:
	testq	%r13, %r13
	je	.L52
	cmpb	$0, 0(%r13)
	jne	.L53
.L52:
	movl	$347, %r8d
.L71:
	leaq	.LC0(%rip), %rcx
	movl	$143, %edx
	movl	$180, %esi
	xorl	%r13d, %r13d
	movl	$38, %edi
	call	ERR_put_error@PLT
	jmp	.L9
.L21:
	movl	$366, %r8d
	movl	$119, %edx
	movl	$180, %esi
	movl	$38, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L73:
	xorl	%r13d, %r13d
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L53:
	movl	$351, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r13, %rdi
	call	CRYPTO_strdup@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L87
	movq	80(%r15), %rdi
	movq	%rax, %rsi
	call	OPENSSL_sk_push@PLT
	movl	%eax, %r13d
	testl	%eax, %eax
	je	.L88
.L72:
	movl	$1, %r13d
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L84:
	xorl	%r13d, %r13d
	cmpq	$0, 40(%r15)
	je	.L9
	xorl	%ecx, %ecx
	movl	$2, %edx
	movl	$2, %esi
	movq	%rax, %rdi
	call	DSO_ctrl@PLT
	movq	40(%r15), %rsi
	movq	(%r15), %rdi
	call	DSO_convert_filename@PLT
	movq	%rax, 24(%r15)
	movq	%rax, %rsi
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L79:
	movl	$210, %r8d
	movl	$144, %edx
	movl	$181, %esi
	movl	$38, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L12:
	movl	$295, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$112, %edx
	xorl	%r13d, %r13d
	movl	$180, %esi
	movl	$38, %edi
	call	ERR_put_error@PLT
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L31:
	call	CRYPTO_free@PLT
	movq	%r13, %rdi
	xorl	%r13d, %r13d
	movl	$311, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_strdup@PLT
	testq	%rax, %rax
	movq	%rax, 24(%r15)
	setne	%r13b
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L33:
	call	CRYPTO_free@PLT
	movq	%r13, %rdi
	xorl	%r13d, %r13d
	movl	$324, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_strdup@PLT
	testq	%rax, %rax
	movq	%rax, 40(%r15)
	setne	%r13b
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L80:
	movl	$160, %r8d
	movl	$65, %edx
	movl	$183, %esi
	movl	$38, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L81:
	movl	$165, %r8d
	movl	$65, %edx
	movl	$183, %esi
	movl	$38, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	-296(%rbp), %r9
	movl	$166, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r9, %rdi
	call	CRYPTO_free@PLT
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L37:
	movq	(%r15), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	call	DSO_load@PLT
	testq	%rax, %rax
	je	.L89
.L40:
	movq	64(%r15), %rsi
	movq	(%r15), %rdi
	call	DSO_bind_func@PLT
	movq	%rax, 16(%r15)
	testq	%rax, %rax
	je	.L90
	movl	32(%r15), %r13d
	testl	%r13d, %r13d
	jne	.L44
	movq	56(%r15), %rsi
	movq	(%r15), %rdi
	call	DSO_bind_func@PLT
	movq	%rax, 8(%r15)
	testq	%rax, %rax
	je	.L46
	movl	$196608, %edi
	call	*%rax
	cmpq	$196607, %rax
	jbe	.L46
.L44:
	movdqu	(%r12), %xmm2
	movdqu	16(%r12), %xmm3
	leaq	-288(%rbp), %r13
	movdqu	32(%r12), %xmm4
	movdqu	48(%r12), %xmm5
	movdqu	64(%r12), %xmm6
	movdqu	80(%r12), %xmm7
	movaps	%xmm2, -256(%rbp)
	movaps	%xmm3, -240(%rbp)
	movdqu	96(%r12), %xmm2
	movdqu	112(%r12), %xmm3
	movaps	%xmm4, -224(%rbp)
	movdqu	128(%r12), %xmm4
	movaps	%xmm5, -208(%rbp)
	movdqu	144(%r12), %xmm5
	movaps	%xmm6, -192(%rbp)
	movdqu	160(%r12), %xmm6
	movaps	%xmm7, -176(%rbp)
	movdqu	176(%r12), %xmm7
	movaps	%xmm2, -160(%rbp)
	movaps	%xmm3, -144(%rbp)
	movaps	%xmm4, -128(%rbp)
	movaps	%xmm5, -112(%rbp)
	movaps	%xmm6, -96(%rbp)
	movaps	%xmm7, -80(%rbp)
	call	ENGINE_get_static_state@PLT
	leaq	-264(%rbp), %rdx
	leaq	-272(%rbp), %rsi
	leaq	-280(%rbp), %rdi
	movq	%rax, -288(%rbp)
	call	CRYPTO_get_mem_functions@PLT
	movq	%r12, %rdi
	call	engine_set_all_null@PLT
	movq	%r13, %rdx
	movq	40(%r15), %rsi
	movq	%r12, %rdi
	call	*16(%r15)
	movl	%eax, %r13d
	testl	%eax, %eax
	je	.L91
	movl	48(%r15), %eax
	testl	%eax, %eax
	jle	.L72
	movq	%r12, %rdi
	call	ENGINE_add@PLT
	movl	%eax, %r13d
	testl	%eax, %eax
	jne	.L72
	cmpl	$1, 48(%r15)
	jg	.L92
	call	ERR_clear_error@PLT
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L82:
	movl	dynamic_ex_data_idx(%rip), %esi
	movq	%r9, %rdx
	movq	%r12, %rdi
	call	ENGINE_set_ex_data@PLT
	movq	-296(%rbp), %r9
	testl	%eax, %eax
	jne	.L93
	movq	global_engine_lock(%rip), %rdi
	movq	%r9, -296(%rbp)
	call	CRYPTO_THREAD_unlock@PLT
	movq	-296(%rbp), %r9
	movq	80(%r9), %rdi
	call	OPENSSL_sk_free@PLT
	movq	-296(%rbp), %r9
	movl	$190, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r9, %rdi
	call	CRYPTO_free@PLT
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L89:
	movl	72(%r15), %edx
	testl	%edx, %edx
	je	.L38
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L86:
	movl	$339, %r8d
	jmp	.L71
.L93:
	movq	global_engine_lock(%rip), %rdi
	call	CRYPTO_THREAD_unlock@PLT
	movl	$190, %edx
	leaq	.LC0(%rip), %rsi
	xorl	%edi, %edi
	call	CRYPTO_free@PLT
	movq	-296(%rbp), %r9
	movq	%r9, %r15
	jmp	.L14
.L46:
	movq	(%r15), %rdi
	pxor	%xmm0, %xmm0
	movups	%xmm0, 8(%r15)
	call	DSO_free@PLT
	movl	$453, %r8d
	movl	$145, %edx
	movq	$0, (%r15)
	leaq	.LC0(%rip), %rcx
	movl	$182, %esi
	movl	$38, %edi
	call	ERR_put_error@PLT
	jmp	.L9
.L87:
	movl	$353, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r13d, %r13d
	movl	$180, %esi
	movl	$38, %edi
	call	ERR_put_error@PLT
	jmp	.L9
.L88:
	movq	%r12, %rdi
	movl	$357, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movl	$358, %r8d
	movl	$65, %edx
	leaq	.LC0(%rip), %rcx
	movl	$180, %esi
	movl	$38, %edi
	call	ERR_put_error@PLT
	jmp	.L9
.L91:
	pxor	%xmm0, %xmm0
	movq	(%r15), %rdi
	movups	%xmm0, 8(%r15)
	call	DSO_free@PLT
	movl	$485, %r8d
	movl	$109, %edx
	movq	$0, (%r15)
	leaq	.LC0(%rip), %rcx
	movl	$182, %esi
	movl	$38, %edi
	call	ERR_put_error@PLT
	movdqa	-256(%rbp), %xmm2
	movdqa	-240(%rbp), %xmm3
	movdqa	-224(%rbp), %xmm4
	movdqa	-208(%rbp), %xmm5
	movdqa	-192(%rbp), %xmm6
	movdqa	-176(%rbp), %xmm7
	movups	%xmm2, (%r12)
	movups	%xmm3, 16(%r12)
	movdqa	-160(%rbp), %xmm2
	movdqa	-144(%rbp), %xmm3
	movups	%xmm4, 32(%r12)
	movdqa	-128(%rbp), %xmm4
	movups	%xmm5, 48(%r12)
	movdqa	-112(%rbp), %xmm5
	movups	%xmm6, 64(%r12)
	movdqa	-96(%rbp), %xmm6
	movups	%xmm7, 80(%r12)
	movdqa	-80(%rbp), %xmm7
	movups	%xmm2, 96(%r12)
	movups	%xmm3, 112(%r12)
	movups	%xmm4, 128(%r12)
	movups	%xmm5, 144(%r12)
	movups	%xmm6, 160(%r12)
	movups	%xmm7, 176(%r12)
	jmp	.L9
.L85:
	movl	$388, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r13, %rdi
	call	CRYPTO_free@PLT
	jmp	.L40
.L90:
	movq	(%r15), %rdi
	xorl	%r13d, %r13d
	call	DSO_free@PLT
	movl	$427, %r8d
	movl	$104, %edx
	movq	$0, (%r15)
	leaq	.LC0(%rip), %rcx
	movl	$182, %esi
	movl	$38, %edi
	call	ERR_put_error@PLT
	jmp	.L9
.L92:
	movl	$501, %r8d
	movl	$103, %edx
	movl	$182, %esi
	movl	$38, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L9
.L83:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE874:
	.size	dynamic_ctrl, .-dynamic_ctrl
	.p2align 4
	.type	dynamic_finish, @function
dynamic_finish:
.LFB879:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE879:
	.size	dynamic_finish, .-dynamic_finish
	.section	.rodata.str1.1
.LC3:
	.string	"dynamic"
	.section	.rodata.str1.8
	.align 8
.LC4:
	.string	"Dynamic engine loading support"
	.text
	.p2align 4
	.globl	engine_load_dynamic_int
	.type	engine_load_dynamic_int, @function
engine_load_dynamic_int:
.LFB871:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	ENGINE_new@PLT
	testq	%rax, %rax
	je	.L95
	leaq	.LC3(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	ENGINE_set_id@PLT
	testl	%eax, %eax
	je	.L98
	leaq	.LC4(%rip), %rsi
	movq	%r12, %rdi
	call	ENGINE_set_name@PLT
	testl	%eax, %eax
	je	.L98
	leaq	dynamic_init(%rip), %rsi
	movq	%r12, %rdi
	call	ENGINE_set_init_function@PLT
	testl	%eax, %eax
	je	.L98
	leaq	dynamic_finish(%rip), %rsi
	movq	%r12, %rdi
	call	ENGINE_set_finish_function@PLT
	testl	%eax, %eax
	je	.L98
	leaq	dynamic_ctrl(%rip), %rsi
	movq	%r12, %rdi
	call	ENGINE_set_ctrl_function@PLT
	testl	%eax, %eax
	je	.L98
	movl	$4, %esi
	movq	%r12, %rdi
	call	ENGINE_set_flags@PLT
	testl	%eax, %eax
	je	.L98
	leaq	dynamic_cmd_defns(%rip), %rsi
	movq	%r12, %rdi
	call	ENGINE_set_cmd_defns@PLT
	testl	%eax, %eax
	je	.L98
	movq	%r12, %rdi
	call	ENGINE_add@PLT
	movq	%r12, %rdi
	call	ENGINE_free@PLT
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	ERR_clear_error@PLT
	.p2align 4,,10
	.p2align 3
.L98:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	ENGINE_free@PLT
.L95:
	.cfi_restore_state
	popq	%rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE871:
	.size	engine_load_dynamic_int, .-engine_load_dynamic_int
	.data
	.align 4
	.type	dynamic_ex_data_idx, @object
	.size	dynamic_ex_data_idx, 4
dynamic_ex_data_idx:
	.long	-1
	.section	.rodata.str1.1
.LC5:
	.string	"SO_PATH"
	.section	.rodata.str1.8
	.align 8
.LC6:
	.string	"Specifies the path to the new ENGINE shared library"
	.section	.rodata.str1.1
.LC7:
	.string	"NO_VCHECK"
	.section	.rodata.str1.8
	.align 8
.LC8:
	.string	"Specifies to continue even if version checking fails (boolean)"
	.section	.rodata.str1.1
.LC9:
	.string	"ID"
	.section	.rodata.str1.8
	.align 8
.LC10:
	.string	"Specifies an ENGINE id name for loading"
	.section	.rodata.str1.1
.LC11:
	.string	"LIST_ADD"
	.section	.rodata.str1.8
	.align 8
.LC12:
	.string	"Whether to add a loaded ENGINE to the internal list (0=no,1=yes,2=mandatory)"
	.section	.rodata.str1.1
.LC13:
	.string	"DIR_LOAD"
	.section	.rodata.str1.8
	.align 8
.LC14:
	.string	"Specifies whether to load from 'DIR_ADD' directories (0=no,1=yes,2=mandatory)"
	.section	.rodata.str1.1
.LC15:
	.string	"DIR_ADD"
	.section	.rodata.str1.8
	.align 8
.LC16:
	.string	"Adds a directory from which ENGINEs can be loaded"
	.section	.rodata.str1.1
.LC17:
	.string	"LOAD"
	.section	.rodata.str1.8
	.align 8
.LC18:
	.string	"Load up the ENGINE specified by other settings"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	dynamic_cmd_defns, @object
	.size	dynamic_cmd_defns, 256
dynamic_cmd_defns:
	.long	200
	.zero	4
	.quad	.LC5
	.quad	.LC6
	.long	2
	.zero	4
	.long	201
	.zero	4
	.quad	.LC7
	.quad	.LC8
	.long	1
	.zero	4
	.long	202
	.zero	4
	.quad	.LC9
	.quad	.LC10
	.long	2
	.zero	4
	.long	203
	.zero	4
	.quad	.LC11
	.quad	.LC12
	.long	1
	.zero	4
	.long	204
	.zero	4
	.quad	.LC13
	.quad	.LC14
	.long	1
	.zero	4
	.long	205
	.zero	4
	.quad	.LC15
	.quad	.LC16
	.long	2
	.zero	4
	.long	206
	.zero	4
	.quad	.LC17
	.quad	.LC18
	.long	4
	.zero	4
	.long	0
	.zero	4
	.quad	0
	.quad	0
	.long	0
	.zero	4
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
