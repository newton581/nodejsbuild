	.file	"eng_cnf.c"
	.text
	.p2align 4
	.type	int_engine_module_finish, @function
int_engine_module_finish:
.LFB955:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L3:
	call	ENGINE_finish@PLT
.L2:
	movq	initialized_engines(%rip), %rdi
	call	OPENSSL_sk_pop@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L3
	movq	initialized_engines(%rip), %rdi
	call	OPENSSL_sk_free@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	movq	$0, initialized_engines(%rip)
	ret
	.cfi_endproc
.LFE955:
	.size	int_engine_module_finish, .-int_engine_module_finish
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/engine/eng_cnf.c"
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"engine_id"
.LC2:
	.string	"soft_load"
.LC3:
	.string	"dynamic_path"
.LC4:
	.string	"dynamic"
.LC5:
	.string	"SO_PATH"
.LC6:
	.string	"2"
.LC7:
	.string	"LIST_ADD"
.LC8:
	.string	"LOAD"
.LC9:
	.string	"EMPTY"
.LC10:
	.string	"init"
.LC11:
	.string	"default_algorithms"
.LC12:
	.string	", value="
.LC13:
	.string	", name="
.LC14:
	.string	"section="
	.text
	.p2align 4
	.type	int_engine_module_init, @function
int_engine_module_init:
.LFB954:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$72, %rsp
	movq	%rsi, -96(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	CONF_imodule_get_value@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	NCONF_get_section@PLT
	movl	$0, -68(%rbp)
	movq	%rax, -88(%rbp)
	testq	%rax, %rax
	je	.L98
	movq	-88(%rbp), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, -68(%rbp)
	jge	.L99
	.p2align 4,,10
	.p2align 3
.L31:
	movl	-68(%rbp), %esi
	movq	-88(%rbp), %rdi
	call	OPENSSL_sk_value@PLT
	movl	$46, %esi
	movq	$-1, -64(%rbp)
	movq	8(%rax), %r12
	movq	16(%rax), %r15
	movq	%r12, %rdi
	call	strchr@PLT
	movq	-96(%rbp), %rdi
	movq	%r15, %rsi
	leaq	1(%rax), %rdx
	testq	%rax, %rax
	cmovne	%rdx, %r12
	call	NCONF_get_section@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L100
	movl	$0, -72(%rbp)
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	movq	%r15, -104(%rbp)
	movq	%r12, -80(%rbp)
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L105:
	movl	$10, %ecx
	movq	%r12, %rsi
	leaq	.LC2(%rip), %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L36
	movl	$13, %ecx
	movq	%r12, %rsi
	leaq	.LC3(%rip), %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L101
	testq	%r14, %r14
	je	.L102
.L16:
	movq	%r15, %rsi
	movl	$6, %ecx
	leaq	.LC9(%rip), %rdi
	repz cmpsb
	leaq	.LC10(%rip), %rdx
	movl	$5, %ecx
	movq	%r12, %rsi
	movq	%rdx, %rdi
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	movl	$0, %eax
	cmove	%rax, %r15
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L103
	movl	$19, %ecx
	leaq	.LC11(%rip), %rdi
	movq	%r12, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L24
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	ENGINE_set_default_string@PLT
	testl	%eax, %eax
	je	.L93
	.p2align 4,,10
	.p2align 3
.L12:
	addl	$1, %r13d
.L10:
	movq	%rbx, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r13d
	jge	.L104
	movl	%r13d, %esi
	movq	%rbx, %rdi
	call	OPENSSL_sk_value@PLT
	movl	$46, %esi
	movq	8(%rax), %r12
	movq	%rax, %r15
	movq	%rax, -112(%rbp)
	movq	%r12, %rdi
	call	strchr@PLT
	movl	$10, %ecx
	leaq	.LC1(%rip), %rdi
	movq	16(%r15), %r15
	testq	%rax, %rax
	leaq	1(%rax), %rdx
	cmovne	%rdx, %r12
	movq	%r12, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L105
	movq	%r15, -80(%rbp)
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L24:
	xorl	%ecx, %ecx
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	ENGINE_ctrl_cmd_string@PLT
	testl	%eax, %eax
	jne	.L12
	.p2align 4,,10
	.p2align 3
.L93:
	movq	%r14, %r15
.L15:
	movl	$140, %r8d
	movl	$102, %edx
	movl	$188, %esi
	movl	$38, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	-112(%rbp), %rax
	subq	$8, %rsp
	leaq	.LC12(%rip), %r9
	leaq	.LC13(%rip), %rcx
	leaq	.LC14(%rip), %rsi
	movl	$6, %edi
	movq	8(%rax), %r8
	pushq	16(%rax)
	movq	(%rax), %rdx
	xorl	%eax, %eax
	call	ERR_add_error_data@PLT
	popq	%rax
	popq	%rdx
.L30:
	movq	%r15, %rdi
	call	ENGINE_free@PLT
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L6:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L106
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L103:
	.cfi_restore_state
	movq	-104(%rbp), %rsi
	movq	-96(%rbp), %rdi
	leaq	-64(%rbp), %rcx
	call	NCONF_get_number_e@PLT
	testl	%eax, %eax
	je	.L93
	movq	-64(%rbp), %rax
	cmpq	$1, %rax
	je	.L107
	testq	%rax, %rax
	je	.L12
	movl	$121, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$151, %edx
	movq	%r14, %r15
	movl	$188, %esi
	movl	$38, %edi
	call	ERR_put_error@PLT
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L101:
	leaq	.LC4(%rip), %rdi
	call	ENGINE_by_id@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L14
	xorl	%ecx, %ecx
	movq	%r15, %rdx
	leaq	.LC5(%rip), %rsi
	movq	%rax, %rdi
	call	ENGINE_ctrl_cmd_string@PLT
	testl	%eax, %eax
	je	.L93
	xorl	%ecx, %ecx
	leaq	.LC6(%rip), %rdx
	leaq	.LC7(%rip), %rsi
	movq	%r14, %rdi
	call	ENGINE_ctrl_cmd_string@PLT
	testl	%eax, %eax
	je	.L93
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	leaq	.LC8(%rip), %rsi
	movq	%r14, %rdi
	call	ENGINE_ctrl_cmd_string@PLT
	testl	%eax, %eax
	jne	.L12
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L36:
	movl	$1, -72(%rbp)
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L102:
	movq	-80(%rbp), %rdi
	call	ENGINE_by_id@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	jne	.L16
	movl	-72(%rbp), %ecx
	testl	%ecx, %ecx
	jne	.L108
	testq	%rax, %rax
	jne	.L16
.L14:
	xorl	%r15d, %r15d
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L104:
	testq	%r14, %r14
	je	.L26
	cmpq	$-1, -64(%rbp)
	je	.L109
.L26:
	movq	%r14, %rdi
	call	ENGINE_free@PLT
.L18:
	movq	-88(%rbp), %rdi
	addl	$1, -68(%rbp)
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, -68(%rbp)
	jl	.L31
.L99:
	movl	$1, %eax
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L107:
	movq	%r14, %rdi
	call	ENGINE_init@PLT
	testl	%eax, %eax
	je	.L93
	movq	initialized_engines(%rip), %rdi
	testq	%rdi, %rdi
	je	.L110
.L22:
	movq	%r14, %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	jne	.L12
	movq	%r14, %rdi
	movq	%r14, %r15
	call	ENGINE_finish@PLT
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L100:
	movl	$60, %r8d
	movl	$149, %edx
	movl	$188, %esi
	movl	$38, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L109:
	movq	%r14, %rdi
	call	ENGINE_init@PLT
	testl	%eax, %eax
	je	.L94
	movq	initialized_engines(%rip), %rdi
	testq	%rdi, %rdi
	je	.L111
.L28:
	movq	%r14, %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	jne	.L26
.L97:
	movq	%r14, %rdi
	movq	%r14, %r15
	call	ENGINE_finish@PLT
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L94:
	movq	%r14, %r15
.L27:
	movl	$140, %r8d
	movl	$102, %edx
	movl	$188, %esi
	movl	$38, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L110:
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, initialized_engines(%rip)
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L22
	movq	%r14, %rdi
	movq	%r14, %r15
	call	ENGINE_finish@PLT
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L108:
	call	ERR_clear_error@PLT
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L98:
	movl	$164, %r8d
	movl	$148, %edx
	movl	$187, %esi
	movl	$38, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L6
.L111:
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, initialized_engines(%rip)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L97
	jmp	.L28
.L106:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE954:
	.size	int_engine_module_init, .-int_engine_module_init
	.section	.rodata.str1.1
.LC15:
	.string	"engines"
	.text
	.p2align 4
	.globl	ENGINE_add_conf_module
	.type	ENGINE_add_conf_module, @function
ENGINE_add_conf_module:
.LFB956:
	.cfi_startproc
	endbr64
	leaq	int_engine_module_finish(%rip), %rdx
	leaq	int_engine_module_init(%rip), %rsi
	leaq	.LC15(%rip), %rdi
	jmp	CONF_module_add@PLT
	.cfi_endproc
.LFE956:
	.size	ENGINE_add_conf_module, .-ENGINE_add_conf_module
	.local	initialized_engines
	.comm	initialized_engines,8,8
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
