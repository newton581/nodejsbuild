	.file	"t_crl.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"Certificate Revocation List (CRL):\n"
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	""
.LC2:
	.string	"%8sVersion %ld (0x%lx)\n"
.LC3:
	.string	"%8sVersion unknown (%ld)\n"
.LC4:
	.string	"    "
.LC5:
	.string	"%8sIssuer: "
.LC6:
	.string	"\n"
.LC7:
	.string	"%8sLast Update: "
.LC8:
	.string	"\n%8sNext Update: "
.LC9:
	.string	"NONE"
.LC10:
	.string	"CRL extensions"
.LC11:
	.string	"Revoked Certificates:\n"
.LC12:
	.string	"No Revoked Certificates.\n"
.LC13:
	.string	"    Serial Number: "
.LC14:
	.string	"\n        Revocation Date: "
.LC15:
	.string	"CRL entry extensions"
	.text
	.p2align 4
	.globl	X509_CRL_print_ex
	.type	X509_CRL_print_ex, @function
X509_CRL_print_ex:
.LFB1298:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	leaq	.LC0(%rip), %rsi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	%r13, %rdi
	call	X509_CRL_get_version@PLT
	movq	%rax, %rcx
	cmpq	$1, %rax
	jbe	.L13
	leaq	.LC1(%rip), %rdx
	leaq	.LC3(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L3:
	leaq	-72(%rbp), %rdx
	leaq	-64(%rbp), %rsi
	movq	%r13, %rdi
	call	X509_CRL_get0_signature@PLT
	leaq	.LC4(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_puts@PLT
	movq	-72(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	X509_signature_print@PLT
	leaq	.LC1(%rip), %rdx
	leaq	.LC5(%rip), %rsi
	xorl	%eax, %eax
	movq	%r12, %rdi
	call	BIO_printf@PLT
	movq	%r13, %rdi
	call	X509_CRL_get_issuer@PLT
	movq	%r14, %rcx
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	X509_NAME_print_ex@PLT
	leaq	.LC6(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_puts@PLT
	leaq	.LC1(%rip), %rdx
	leaq	.LC7(%rip), %rsi
	xorl	%eax, %eax
	movq	%r12, %rdi
	call	BIO_printf@PLT
	movq	%r13, %rdi
	call	X509_CRL_get0_lastUpdate@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	ASN1_TIME_print@PLT
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC1(%rip), %rdx
	leaq	.LC8(%rip), %rsi
	call	BIO_printf@PLT
	movq	%r13, %rdi
	call	X509_CRL_get0_nextUpdate@PLT
	testq	%rax, %rax
	je	.L4
	movq	%r13, %rdi
	call	X509_CRL_get0_nextUpdate@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	ASN1_TIME_print@PLT
.L5:
	leaq	.LC6(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	%r13, %rdi
	call	X509_CRL_get0_extensions@PLT
	xorl	%ecx, %ecx
	movl	$8, %r8d
	movq	%r12, %rdi
	movq	%rax, %rdx
	leaq	.LC10(%rip), %rsi
	call	X509V3_extensions_print@PLT
	movq	%r13, %rdi
	call	X509_CRL_get_REVOKED@PLT
	movq	%rax, %rdi
	movq	%rax, %rbx
	call	OPENSSL_sk_num@PLT
	testl	%eax, %eax
	jle	.L6
	leaq	.LC11(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L8:
	xorl	%r15d, %r15d
	leaq	.LC13(%rip), %r14
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L9:
	movl	%r15d, %esi
	movq	%rbx, %rdi
	addl	$1, %r15d
	call	OPENSSL_sk_value@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %r13
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	%r13, %rdi
	call	X509_REVOKED_get0_serialNumber@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	i2a_ASN1_INTEGER@PLT
	leaq	.LC14(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	%r13, %rdi
	call	X509_REVOKED_get0_revocationDate@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	ASN1_TIME_print@PLT
	leaq	.LC6(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	%r13, %rdi
	call	X509_REVOKED_get0_extensions@PLT
	movl	$8, %r8d
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movq	%rax, %rdx
	leaq	.LC15(%rip), %rsi
	call	X509V3_extensions_print@PLT
.L7:
	movq	%rbx, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r15d
	jl	.L9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %rsi
	movq	%r12, %rdi
	call	X509_signature_print@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L14
	addq	$40, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	.cfi_restore_state
	leaq	1(%rax), %r9
	movq	%rax, %r8
	movq	%r12, %rdi
	xorl	%eax, %eax
	movq	%r9, %rcx
	leaq	.LC1(%rip), %rdx
	leaq	.LC2(%rip), %rsi
	call	BIO_printf@PLT
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L6:
	leaq	.LC12(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L4:
	leaq	.LC9(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L5
.L14:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1298:
	.size	X509_CRL_print_ex, .-X509_CRL_print_ex
	.p2align 4
	.globl	X509_CRL_print
	.type	X509_CRL_print, @function
X509_CRL_print:
.LFB1297:
	.cfi_startproc
	endbr64
	xorl	%edx, %edx
	jmp	X509_CRL_print_ex
	.cfi_endproc
.LFE1297:
	.size	X509_CRL_print, .-X509_CRL_print
	.section	.rodata.str1.8
	.align 8
.LC16:
	.string	"../deps/openssl/openssl/crypto/x509/t_crl.c"
	.text
	.p2align 4
	.globl	X509_CRL_print_fp
	.type	X509_CRL_print_fp, @function
X509_CRL_print_fp:
.LFB1296:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -40
	call	BIO_s_file@PLT
	movq	%rax, %rdi
	call	BIO_new@PLT
	testq	%rax, %rax
	je	.L20
	movq	%rax, %r12
	movq	%r14, %rcx
	xorl	%edx, %edx
	movl	$106, %esi
	movq	%rax, %rdi
	call	BIO_ctrl@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	xorl	%edx, %edx
	call	X509_CRL_print_ex
	movq	%r12, %rdi
	movl	%eax, %r13d
	call	BIO_free@PLT
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	.cfi_restore_state
	movl	$25, %r8d
	leaq	.LC16(%rip), %rcx
	movl	$7, %edx
	xorl	%r13d, %r13d
	movl	$147, %esi
	movl	$11, %edi
	call	ERR_put_error@PLT
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1296:
	.size	X509_CRL_print_fp, .-X509_CRL_print_fp
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
