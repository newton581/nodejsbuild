	.file	"conf_sap.c"
	.text
	.p2align 4
	.globl	OPENSSL_config
	.type	OPENSSL_config, @function
OPENSSL_config:
.LFB888:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	$0, -16(%rbp)
	movaps	%xmm0, -32(%rbp)
	testq	%rdi, %rdi
	je	.L2
	call	strdup@PLT
	movq	%rax, -24(%rbp)
.L2:
	leaq	-32(%rbp), %rsi
	movl	$64, %edi
	movq	$50, -16(%rbp)
	call	OPENSSL_init_crypto@PLT
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L9
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L9:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE888:
	.size	OPENSSL_config, .-OPENSSL_config
	.p2align 4
	.globl	openssl_config_int
	.type	openssl_config_int, @function
openssl_config_int:
.LFB889:
	.cfi_startproc
	endbr64
	movl	openssl_configured(%rip), %eax
	testl	%eax, %eax
	jne	.L15
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	testq	%rdi, %rdi
	je	.L14
	movq	(%rdi), %r12
	movq	8(%rdi), %r13
	movq	16(%rdi), %r14
.L12:
	call	OPENSSL_load_builtin_modules@PLT
	call	ENGINE_load_builtin_engines@PLT
	call	ERR_clear_error@PLT
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	CONF_modules_load_file@PLT
	movl	$1, openssl_configured(%rip)
	addq	$8, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	xorl	%r13d, %r13d
	xorl	%r12d, %r12d
	movl	$50, %r14d
	jmp	.L12
	.cfi_endproc
.LFE889:
	.size	openssl_config_int, .-openssl_config_int
	.p2align 4
	.globl	openssl_no_config_int
	.type	openssl_no_config_int, @function
openssl_no_config_int:
.LFB890:
	.cfi_startproc
	endbr64
	movl	$1, openssl_configured(%rip)
	ret
	.cfi_endproc
.LFE890:
	.size	openssl_no_config_int, .-openssl_no_config_int
	.local	openssl_configured
	.comm	openssl_configured,4,4
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
