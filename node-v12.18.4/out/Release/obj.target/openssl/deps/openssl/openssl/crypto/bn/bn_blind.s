	.file	"bn_blind.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/bn/bn_blind.c"
	.text
	.p2align 4
	.globl	BN_BLINDING_new
	.type	BN_BLINDING_new, @function
BN_BLINDING_new:
.LFB252:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	movl	$80, %edi
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	leaq	.LC0(%rip), %rsi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	movl	$36, %edx
	pushq	%r12
	.cfi_offset 12, -48
	call	CRYPTO_zalloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L21
	call	CRYPTO_THREAD_lock_new@PLT
	movq	%rax, 72(%r12)
	testq	%rax, %rax
	je	.L22
	call	CRYPTO_THREAD_get_current_id@PLT
	movq	%rax, 32(%r12)
	testq	%r15, %r15
	je	.L9
	movq	%r15, %rdi
	call	BN_dup@PLT
	movq	%rax, (%r12)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L8
.L9:
	testq	%r14, %r14
	je	.L7
	movq	%r14, %rdi
	call	BN_dup@PLT
	movq	%rax, 8(%r12)
	testq	%rax, %rax
	je	.L19
.L7:
	movq	%r13, %rdi
	call	BN_dup@PLT
	movq	%rax, 24(%r12)
	testq	%rax, %rax
	je	.L19
	movl	$4, %esi
	movq	%r13, %rdi
	call	BN_get_flags@PLT
	testl	%eax, %eax
	jne	.L23
.L11:
	movl	$-1, 40(%r12)
.L1:
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	.cfi_restore_state
	movq	24(%r12), %rdi
	movl	$4, %esi
	call	BN_set_flags@PLT
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L19:
	movq	(%r12), %rdi
.L8:
	call	BN_free@PLT
	movq	8(%r12), %rdi
	call	BN_free@PLT
	movq	16(%r12), %rdi
	call	BN_free@PLT
	movq	24(%r12), %rdi
	call	BN_free@PLT
	movq	72(%r12), %rdi
	call	CRYPTO_THREAD_lock_free@PLT
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	movl	$90, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	.cfi_restore_state
	movl	$37, %r8d
	movl	$65, %edx
	movl	$102, %esi
	movl	$3, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L22:
	movl	$43, %r8d
	movl	$65, %edx
	movl	$102, %esi
	movl	$3, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	movl	$44, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	jmp	.L1
	.cfi_endproc
.LFE252:
	.size	BN_BLINDING_new, .-BN_BLINDING_new
	.p2align 4
	.globl	BN_BLINDING_free
	.type	BN_BLINDING_free, @function
BN_BLINDING_free:
.LFB253:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L24
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	(%rdi), %rdi
	call	BN_free@PLT
	movq	8(%r12), %rdi
	call	BN_free@PLT
	movq	16(%r12), %rdi
	call	BN_free@PLT
	movq	24(%r12), %rdi
	call	BN_free@PLT
	movq	72(%r12), %rdi
	call	CRYPTO_THREAD_lock_free@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$90, %edx
	popq	%r12
	.cfi_restore 12
	leaq	.LC0(%rip), %rsi
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_free@PLT
	.p2align 4,,10
	.p2align 3
.L24:
	ret
	.cfi_endproc
.LFE253:
	.size	BN_BLINDING_free, .-BN_BLINDING_free
	.p2align 4
	.globl	BN_BLINDING_invert
	.type	BN_BLINDING_invert, @function
BN_BLINDING_invert:
.LFB257:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	8(%rsi), %rdx
	testq	%rdx, %rdx
	je	.L41
	movq	56(%rsi), %r9
	testq	%r9, %r9
	je	.L32
	movl	8(%rdx), %r10d
	cmpl	%r10d, 12(%rdi)
	jl	.L33
	movslq	8(%rdi), %r12
	movslq	%r10d, %rsi
	movq	%r12, %rcx
	testq	%rsi, %rsi
	je	.L34
	movq	(%rdi), %r11
	movq	%r12, %rax
	movq	%rsi, %r13
	negq	%rax
	subq	%r12, %r13
	leaq	(%r11,%r12,8), %r11
	.p2align 4,,10
	.p2align 3
.L35:
	movq	%rax, %rbx
	sarq	$63, %rbx
	andq	%rbx, (%r11,%rax,8)
	addq	$1, %rax
	cmpq	%rax, %r13
	jne	.L35
.L34:
	subq	%r12, %rsi
	xorl	%r10d, %ecx
	sarq	$63, %rsi
	andl	%esi, %ecx
	xorl	%r10d, %ecx
	movl	%ecx, 8(%rdi)
.L33:
	addq	$8, %rsp
	movq	%r9, %rcx
	movq	%rdi, %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	BN_mod_mul_montgomery@PLT
	.p2align 4,,10
	.p2align 3
.L32:
	.cfi_restore_state
	movq	24(%rsi), %rcx
	addq	$8, %rsp
	movq	%rdi, %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	BN_mod_mul@PLT
	.p2align 4,,10
	.p2align 3
.L41:
	.cfi_restore_state
	movl	$175, %r8d
	movl	$107, %edx
	movl	$101, %esi
	movl	$3, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE257:
	.size	BN_BLINDING_invert, .-BN_BLINDING_invert
	.p2align 4
	.globl	BN_BLINDING_invert_ex
	.type	BN_BLINDING_invert_ex, @function
BN_BLINDING_invert_ex:
.LFB258:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %r8
	movq	%rsi, %r10
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	testq	%rsi, %rsi
	je	.L58
.L44:
	movq	56(%rdx), %r11
	testq	%r11, %r11
	je	.L46
	movl	8(%r10), %r12d
	cmpl	%r12d, 12(%rdi)
	jl	.L47
	movslq	8(%rdi), %r13
	movslq	%r12d, %rbx
	movq	%r13, %rcx
	testq	%rbx, %rbx
	je	.L48
	movq	(%rdi), %rdx
	movq	%r13, %rax
	movq	%rbx, %rsi
	negq	%rax
	subq	%r13, %rsi
	leaq	(%rdx,%r13,8), %rdx
	.p2align 4,,10
	.p2align 3
.L49:
	movq	%rax, %r9
	sarq	$63, %r9
	andq	%r9, (%rdx,%rax,8)
	addq	$1, %rax
	cmpq	%rax, %rsi
	jne	.L49
.L48:
	subq	%r13, %rbx
	xorl	%r12d, %ecx
	sarq	$63, %rbx
	andl	%ebx, %ecx
	xorl	%r12d, %ecx
	movl	%ecx, 8(%rdi)
.L47:
	addq	$8, %rsp
	movq	%r11, %rcx
	movq	%r10, %rdx
	movq	%rdi, %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	BN_mod_mul_montgomery@PLT
	.p2align 4,,10
	.p2align 3
.L58:
	.cfi_restore_state
	movq	8(%rdx), %r10
	testq	%r10, %r10
	jne	.L44
	movl	$175, %r8d
	movl	$107, %edx
	movl	$101, %esi
	movl	$3, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L46:
	.cfi_restore_state
	movq	24(%rdx), %rcx
	addq	$8, %rsp
	movq	%r10, %rdx
	movq	%rdi, %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	BN_mod_mul@PLT
	.cfi_endproc
.LFE258:
	.size	BN_BLINDING_invert_ex, .-BN_BLINDING_invert_ex
	.p2align 4
	.globl	BN_BLINDING_is_current_thread
	.type	BN_BLINDING_is_current_thread, @function
BN_BLINDING_is_current_thread:
.LFB259:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	32(%rdi), %r12
	call	CRYPTO_THREAD_get_current_id@PLT
	addq	$8, %rsp
	movq	%r12, %rsi
	movq	%rax, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_THREAD_compare_id@PLT
	.cfi_endproc
.LFE259:
	.size	BN_BLINDING_is_current_thread, .-BN_BLINDING_is_current_thread
	.p2align 4
	.globl	BN_BLINDING_set_current_thread
	.type	BN_BLINDING_set_current_thread, @function
BN_BLINDING_set_current_thread:
.LFB260:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	CRYPTO_THREAD_get_current_id@PLT
	movq	%rax, 32(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE260:
	.size	BN_BLINDING_set_current_thread, .-BN_BLINDING_set_current_thread
	.p2align 4
	.globl	BN_BLINDING_lock
	.type	BN_BLINDING_lock, @function
BN_BLINDING_lock:
.LFB261:
	.cfi_startproc
	endbr64
	movq	72(%rdi), %rdi
	jmp	CRYPTO_THREAD_write_lock@PLT
	.cfi_endproc
.LFE261:
	.size	BN_BLINDING_lock, .-BN_BLINDING_lock
	.p2align 4
	.globl	BN_BLINDING_unlock
	.type	BN_BLINDING_unlock, @function
BN_BLINDING_unlock:
.LFB262:
	.cfi_startproc
	endbr64
	movq	72(%rdi), %rdi
	jmp	CRYPTO_THREAD_unlock@PLT
	.cfi_endproc
.LFE262:
	.size	BN_BLINDING_unlock, .-BN_BLINDING_unlock
	.p2align 4
	.globl	BN_BLINDING_get_flags
	.type	BN_BLINDING_get_flags, @function
BN_BLINDING_get_flags:
.LFB263:
	.cfi_startproc
	endbr64
	movq	48(%rdi), %rax
	ret
	.cfi_endproc
.LFE263:
	.size	BN_BLINDING_get_flags, .-BN_BLINDING_get_flags
	.p2align 4
	.globl	BN_BLINDING_set_flags
	.type	BN_BLINDING_set_flags, @function
BN_BLINDING_set_flags:
.LFB264:
	.cfi_startproc
	endbr64
	movq	%rsi, 48(%rdi)
	ret
	.cfi_endproc
.LFE264:
	.size	BN_BLINDING_set_flags, .-BN_BLINDING_set_flags
	.p2align 4
	.globl	BN_BLINDING_create_param
	.type	BN_BLINDING_create_param, @function
BN_BLINDING_create_param:
.LFB265:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L133
.L68:
	cmpq	$0, (%r12)
	je	.L70
	cmpq	$0, 8(%r12)
	je	.L134
.L72:
	movq	16(%r12), %rdi
	testq	%r14, %r14
	je	.L76
	movq	%r8, -72(%rbp)
	call	BN_free@PLT
	movq	%r14, %rdi
	call	BN_dup@PLT
	movq	-72(%rbp), %r8
	movq	%rax, 16(%r12)
	movq	%rax, %rdi
.L76:
	testq	%rdi, %rdi
	je	.L84
	testq	%r8, %r8
	je	.L77
	movq	%r8, 64(%r12)
.L77:
	testq	%rbx, %rbx
	je	.L78
	movq	%rbx, 56(%r12)
.L78:
	movl	$33, %ebx
	leaq	-60(%rbp), %r14
	.p2align 4,,10
	.p2align 3
.L83:
	movq	24(%r12), %rsi
	movq	(%r12), %rdi
	call	BN_priv_rand_range@PLT
	testl	%eax, %eax
	je	.L84
	movq	24(%r12), %rdx
	movq	8(%r12), %rdi
	movq	%r14, %r8
	movq	%r13, %rcx
	movq	(%r12), %rsi
	call	int_bn_mod_inverse@PLT
	testq	%rax, %rax
	jne	.L80
	movl	-60(%rbp), %eax
	testl	%eax, %eax
	je	.L84
	subl	$1, %ebx
	jne	.L83
	movl	$285, %r8d
	movl	$113, %edx
	movl	$128, %esi
	movl	$3, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L84:
	testq	%r15, %r15
	je	.L88
.L67:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L135
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L88:
	.cfi_restore_state
	movq	(%r12), %rdi
	call	BN_free@PLT
	movq	8(%r12), %rdi
	call	BN_free@PLT
	movq	16(%r12), %rdi
	call	BN_free@PLT
	movq	24(%r12), %rdi
	call	BN_free@PLT
	movq	72(%r12), %rdi
	call	CRYPTO_THREAD_lock_free@PLT
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	movl	$90, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L133:
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	%r8, -72(%rbp)
	call	BN_BLINDING_new
	movq	-72(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %r12
	je	.L67
	jmp	.L68
	.p2align 4,,10
	.p2align 3
.L80:
	movq	(%r12), %rdi
	movq	64(%r12), %rax
	movq	%r13, %r8
	movq	24(%r12), %rcx
	movq	16(%r12), %rdx
	movq	%rdi, %rsi
	testq	%rax, %rax
	je	.L85
	movq	56(%r12), %r9
	testq	%r9, %r9
	je	.L85
	call	*%rax
	testl	%eax, %eax
	je	.L84
.L87:
	movq	56(%r12), %rdx
	testq	%rdx, %rdx
	je	.L67
	movq	8(%r12), %rdi
	movq	%r13, %rcx
	movq	%rdi, %rsi
	call	bn_to_mont_fixed_top@PLT
	testl	%eax, %eax
	je	.L84
	movq	(%r12), %rdi
	movq	56(%r12), %rdx
	movq	%r13, %rcx
	movq	%rdi, %rsi
	call	bn_to_mont_fixed_top@PLT
	testl	%eax, %eax
	jne	.L67
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L70:
	movq	%r8, -72(%rbp)
	call	BN_new@PLT
	movq	-72(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, (%r12)
	je	.L84
	cmpq	$0, 8(%r12)
	jne	.L72
	.p2align 4,,10
	.p2align 3
.L134:
	movq	%r8, -72(%rbp)
	call	BN_new@PLT
	movq	-72(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, 8(%r12)
	jne	.L72
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L85:
	call	BN_mod_exp@PLT
	testl	%eax, %eax
	jne	.L87
	jmp	.L84
.L135:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE265:
	.size	BN_BLINDING_create_param, .-BN_BLINDING_create_param
	.p2align 4
	.globl	BN_BLINDING_update
	.type	BN_BLINDING_update, @function
BN_BLINDING_update:
.LFB254:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	cmpq	$0, (%rdi)
	movq	%rdi, %rbx
	je	.L137
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L137
	movl	40(%rbx), %eax
	movq	48(%rbx), %rdx
	movq	%rsi, %r12
	cmpl	$-1, %eax
	je	.L159
	addl	$1, %eax
	movl	%eax, 40(%rbx)
	cmpl	$32, %eax
	jne	.L141
	cmpq	$0, 16(%rbx)
	je	.L142
	testb	$2, %dl
	je	.L160
.L142:
	andl	$1, %edx
	movl	$1, %eax
	jne	.L148
.L147:
	movq	56(%rbx), %rcx
	testq	%rcx, %rcx
	je	.L143
	movq	%r12, %r8
	movq	%rdi, %rdx
	movq	%rdi, %rsi
	call	bn_mul_mont_fixed_top@PLT
	testl	%eax, %eax
	jne	.L144
.L145:
	xorl	%eax, %eax
	jmp	.L139
	.p2align 4,,10
	.p2align 3
.L159:
	movl	$1, 40(%rbx)
.L141:
	andl	$1, %edx
	je	.L147
	movl	$1, %eax
.L136:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L137:
	.cfi_restore_state
	movl	$98, %r8d
	movl	$107, %edx
	movl	$103, %esi
	movl	$3, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
.L139:
	cmpl	$32, 40(%rbx)
	jne	.L136
.L148:
	movl	$0, 40(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L144:
	.cfi_restore_state
	movq	(%rbx), %rdi
	movq	56(%rbx), %rcx
	movq	%r12, %r8
	movq	%rdi, %rdx
	movq	%rdi, %rsi
	call	bn_mul_mont_fixed_top@PLT
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
	jmp	.L139
	.p2align 4,,10
	.p2align 3
.L143:
	movq	24(%rbx), %rcx
	movq	%r12, %r8
	movq	%rdi, %rdx
	movq	%rdi, %rsi
	call	BN_mod_mul@PLT
	testl	%eax, %eax
	je	.L145
	movq	(%rbx), %rdi
	movq	24(%rbx), %rcx
	movq	%r12, %r8
	movq	%rdi, %rdx
	movq	%rdi, %rsi
	call	BN_mod_mul@PLT
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
	jmp	.L139
	.p2align 4,,10
	.p2align 3
.L160:
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%rsi, %rcx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	BN_BLINDING_create_param
	testq	%rax, %rax
	setne	%al
	movzbl	%al, %eax
	jmp	.L139
	.cfi_endproc
.LFE254:
	.size	BN_BLINDING_update, .-BN_BLINDING_update
	.p2align 4
	.globl	BN_BLINDING_convert_ex
	.type	BN_BLINDING_convert_ex, @function
BN_BLINDING_convert_ex:
.LFB256:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	cmpq	$0, (%rdx)
	je	.L162
	cmpq	$0, 8(%rdx)
	movq	%rdx, %rbx
	je	.L162
	cmpl	$-1, 40(%rdx)
	movq	%rdi, %r12
	movq	%rsi, %r13
	movq	%rcx, %r14
	jne	.L165
	movl	$0, 40(%rdx)
.L166:
	testq	%r13, %r13
	je	.L171
	movq	8(%rbx), %rsi
	movq	%r13, %rdi
	call	BN_copy@PLT
	testq	%rax, %rax
	je	.L161
.L171:
	movq	56(%rbx), %rcx
	movq	(%rbx), %rdx
	testq	%rcx, %rcx
	je	.L177
	popq	%rbx
	movq	%r14, %r8
	movq	%r12, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	BN_mod_mul_montgomery@PLT
	.p2align 4,,10
	.p2align 3
.L165:
	.cfi_restore_state
	movq	%rcx, %rsi
	movq	%rdx, %rdi
	call	BN_BLINDING_update
	testl	%eax, %eax
	jne	.L166
.L161:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L162:
	.cfi_restore_state
	movl	$141, %r8d
	movl	$107, %edx
	movl	$100, %esi
	movl	$3, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L177:
	.cfi_restore_state
	movq	24(%rbx), %rcx
	movq	%r14, %r8
	popq	%rbx
	movq	%r12, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	BN_mod_mul@PLT
	.cfi_endproc
.LFE256:
	.size	BN_BLINDING_convert_ex, .-BN_BLINDING_convert_ex
	.p2align 4
	.globl	BN_BLINDING_convert
	.type	BN_BLINDING_convert, @function
BN_BLINDING_convert:
.LFB255:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	(%rsi), %rdx
	testq	%rdx, %rdx
	je	.L179
	movq	%rdi, %r12
	movq	8(%rsi), %rdi
	movq	%rsi, %rbx
	testq	%rdi, %rdi
	je	.L179
	movl	40(%rsi), %eax
	cmpl	$-1, %eax
	jne	.L182
.L190:
	movl	$0, 40(%rbx)
.L183:
	movq	56(%rbx), %rcx
	testq	%rcx, %rcx
	je	.L189
	addq	$8, %rsp
	movq	%r13, %r8
	movq	%r12, %rsi
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	BN_mod_mul_montgomery@PLT
	.p2align 4,,10
	.p2align 3
.L182:
	.cfi_restore_state
	addl	$1, %eax
	movq	48(%rsi), %rcx
	movl	%eax, 40(%rsi)
	cmpl	$32, %eax
	je	.L218
	andl	$1, %ecx
	jne	.L183
.L192:
	movq	56(%rbx), %rcx
	testq	%rcx, %rcx
	je	.L188
	movq	%r13, %r8
	movq	%rdi, %rdx
	movq	%rdi, %rsi
	call	bn_mul_mont_fixed_top@PLT
	testl	%eax, %eax
	jne	.L219
.L186:
	cmpl	$32, 40(%rbx)
	jne	.L178
	movl	$0, 40(%rbx)
	jmp	.L178
	.p2align 4,,10
	.p2align 3
.L179:
	movl	$141, %r8d
	movl	$107, %edx
	movl	$100, %esi
	movl	$3, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L178:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L218:
	.cfi_restore_state
	cmpq	$0, 16(%rsi)
	je	.L185
	testb	$2, %cl
	je	.L220
.L185:
	andl	$1, %ecx
	je	.L192
	jmp	.L190
	.p2align 4,,10
	.p2align 3
.L189:
	movq	24(%rbx), %rcx
	addq	$8, %rsp
	movq	%r13, %r8
	movq	%r12, %rsi
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	BN_mod_mul@PLT
	.p2align 4,,10
	.p2align 3
.L219:
	.cfi_restore_state
	movq	(%rbx), %rdi
	movq	56(%rbx), %rcx
	movq	%r13, %r8
	movq	%rdi, %rdx
	movq	%rdi, %rsi
	call	bn_mul_mont_fixed_top@PLT
	testl	%eax, %eax
	je	.L186
.L187:
	cmpl	$32, 40(%rbx)
	movq	(%rbx), %rdx
	jne	.L183
	jmp	.L190
	.p2align 4,,10
	.p2align 3
.L220:
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r13, %rcx
	movq	%rbx, %rdi
	call	BN_BLINDING_create_param
	testq	%rax, %rax
	jne	.L187
	jmp	.L186
	.p2align 4,,10
	.p2align 3
.L188:
	movq	24(%rbx), %rcx
	movq	%r13, %r8
	movq	%rdi, %rdx
	movq	%rdi, %rsi
	call	BN_mod_mul@PLT
	testl	%eax, %eax
	je	.L186
	movq	(%rbx), %rdi
	movq	24(%rbx), %rcx
	movq	%r13, %r8
	movq	%rdi, %rdx
	movq	%rdi, %rsi
	call	BN_mod_mul@PLT
	testl	%eax, %eax
	jne	.L187
	jmp	.L186
	.cfi_endproc
.LFE255:
	.size	BN_BLINDING_convert, .-BN_BLINDING_convert
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
