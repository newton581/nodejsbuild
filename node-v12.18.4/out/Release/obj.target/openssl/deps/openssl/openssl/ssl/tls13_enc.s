	.file	"tls13_enc.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/ssl/tls13_enc.c"
	.text
	.p2align 4
	.globl	tls13_hkdf_expand
	.type	tls13_hkdf_expand, @function
tls13_hkdf_expand:
.LFB1001:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movl	$1036, %edi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	xorl	%esi, %esi
	subq	$456, %rsp
	movq	24(%rbp), %rax
	movq	%rdx, -472(%rbp)
	movq	%rcx, -456(%rbp)
	movq	%r9, -464(%rbp)
	movq	%rax, -480(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	EVP_PKEY_CTX_new_id@PLT
	testq	%rax, %rax
	je	.L22
	movq	%rax, %r15
	cmpq	$249, %r12
	jbe	.L3
	movl	40(%rbp), %edx
	testl	%edx, %edx
	je	.L4
	movl	$57, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movq	%r13, %rdi
	movl	$561, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
.L5:
	movq	%r15, %rdi
	call	EVP_PKEY_CTX_free@PLT
	xorl	%eax, %eax
.L1:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L52
	addq	$456, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	.cfi_restore_state
	movq	%rbx, %rdi
	leaq	-432(%rbp), %r14
	call	EVP_MD_size@PLT
	xorl	%ecx, %ecx
	movl	$323, %edx
	movq	%r14, %rdi
	movl	%eax, -484(%rbp)
	leaq	-384(%rbp), %rax
	movq	%rax, %rsi
	movq	%rax, -496(%rbp)
	call	WPACKET_init_static_len@PLT
	testl	%eax, %eax
	je	.L9
	movl	32(%rbp), %esi
	movl	$2, %edx
	movq	%r14, %rdi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	jne	.L53
.L9:
	movq	%r15, %rdi
	call	EVP_PKEY_CTX_free@PLT
	movq	%r14, %rdi
	call	WPACKET_cleanup@PLT
	movl	40(%rbp), %eax
	testl	%eax, %eax
	je	.L54
	movl	$84, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movq	%r13, %rdi
	movl	$561, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L4:
	movl	$64, %r8d
	movl	$367, %edx
	movl	$561, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L54:
	movl	$87, %r8d
	movl	$68, %edx
	movl	$561, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L53:
	movl	$1, %esi
	movq	%r14, %rdi
	call	WPACKET_start_sub_packet_len__@PLT
	testl	%eax, %eax
	je	.L9
	movl	$6, %edx
	leaq	label_prefix.23304(%rip), %rsi
	movq	%r14, %rdi
	call	WPACKET_memcpy@PLT
	testl	%eax, %eax
	je	.L9
	movq	-456(%rbp), %rsi
	movq	%r12, %rdx
	movq	%r14, %rdi
	call	WPACKET_memcpy@PLT
	testl	%eax, %eax
	je	.L9
	movq	%r14, %rdi
	call	WPACKET_close@PLT
	testl	%eax, %eax
	je	.L9
	movq	-464(%rbp), %rsi
	movl	$0, %eax
	movl	$1, %ecx
	movq	%r14, %rdi
	testq	%rsi, %rsi
	cmovne	16(%rbp), %rax
	movq	%rax, %rdx
	movq	%rax, 16(%rbp)
	call	WPACKET_sub_memcpy__@PLT
	testl	%eax, %eax
	je	.L9
	leaq	-440(%rbp), %rsi
	movq	%r14, %rdi
	call	WPACKET_get_total_written@PLT
	testl	%eax, %eax
	je	.L9
	movq	%r14, %rdi
	call	WPACKET_finish@PLT
	testl	%eax, %eax
	je	.L9
	movq	%r15, %rdi
	call	EVP_PKEY_derive_init@PLT
	testl	%eax, %eax
	jg	.L55
.L12:
	movq	%r15, %rdi
.L50:
	call	EVP_PKEY_CTX_free@PLT
	cmpl	$0, 40(%rbp)
	je	.L20
	movl	$103, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movq	%r13, %rdi
	movl	$561, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	movl	$1, %eax
.L19:
	xorl	$1, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L22:
	xorl	%eax, %eax
	jmp	.L1
.L55:
	xorl	%r9d, %r9d
	orl	$-1, %esi
	movl	$2, %r8d
	movq	%r15, %rdi
	movl	$4103, %ecx
	movl	$1024, %edx
	call	EVP_PKEY_CTX_ctrl@PLT
	testl	%eax, %eax
	jle	.L12
	xorl	%r8d, %r8d
	orl	$-1, %esi
	movq	%rbx, %r9
	movl	$4099, %ecx
	movl	$1024, %edx
	movq	%r15, %rdi
	call	EVP_PKEY_CTX_ctrl@PLT
	testl	%eax, %eax
	jle	.L12
	orl	$-1, %esi
	movl	$4101, %ecx
	movl	$1024, %edx
	movq	%r15, %rdi
	movq	-472(%rbp), %r9
	movl	-484(%rbp), %r8d
	call	EVP_PKEY_CTX_ctrl@PLT
	testl	%eax, %eax
	jle	.L12
	orl	$-1, %esi
	movl	$4102, %ecx
	movl	$1024, %edx
	movq	%r15, %rdi
	movq	-496(%rbp), %r9
	movl	-440(%rbp), %r8d
	call	EVP_PKEY_CTX_ctrl@PLT
	testl	%eax, %eax
	jle	.L12
	movq	-480(%rbp), %rsi
	movq	%r15, %rdi
	leaq	32(%rbp), %rdx
	call	EVP_PKEY_derive@PLT
	movq	%r15, %rdi
	testl	%eax, %eax
	jle	.L50
	call	EVP_PKEY_CTX_free@PLT
	xorl	%eax, %eax
	jmp	.L19
.L52:
	call	__stack_chk_fail@PLT
.L20:
	movl	$106, %r8d
	movl	$68, %edx
	movl	$561, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$1, %eax
	jmp	.L19
	.cfi_endproc
.LFE1001:
	.size	tls13_hkdf_expand, .-tls13_hkdf_expand
	.p2align 4
	.type	derive_secret_key_and_iv, @function
derive_secret_key_and_iv:
.LFB1010:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%rdx, %rdi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$136, %rsp
	movq	40(%rbp), %rax
	movq	16(%rbp), %rcx
	movq	%r9, -144(%rbp)
	movl	%esi, -164(%rbp)
	movq	32(%rbp), %r15
	movq	%rax, -160(%rbp)
	movq	48(%rbp), %rax
	movq	%rcx, -136(%rbp)
	movq	%rax, -152(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	EVP_MD_size@PLT
	movq	-136(%rbp), %rcx
	movq	-144(%rbp), %r9
	testl	%eax, %eax
	js	.L85
	cltq
	pushq	$1
	movq	24(%rbp), %r8
	movq	%rbx, %rdx
	pushq	%rax
	movq	%r12, %rsi
	movq	%r13, %rdi
	leaq	-128(%rbp), %rbx
	pushq	%r15
	pushq	%rax
	call	tls13_hkdf_expand
	addq	$32, %rsp
	testl	%eax, %eax
	jne	.L86
.L58:
	movl	$64, %esi
	movq	%rbx, %rdi
	call	OPENSSL_cleanse@PLT
	xorl	%eax, %eax
.L56:
	movq	-56(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L87
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L86:
	.cfi_restore_state
	movq	%r14, %rdi
	call	EVP_CIPHER_key_length@PLT
	movq	%r14, %rdi
	movslq	%eax, %rbx
	call	EVP_CIPHER_flags@PLT
	andl	$983047, %eax
	cmpq	$7, %rax
	jne	.L60
	movq	168(%r13), %rax
	movq	568(%rax), %rax
	testq	%rax, %rax
	je	.L88
	movl	36(%rax), %eax
.L62:
	movl	$12, -144(%rbp)
	andl	$196608, %eax
	movq	$12, -136(%rbp)
	cmpl	$1, %eax
	sbbl	%esi, %esi
	andl	$8, %esi
	addl	$8, %esi
	cmpl	$1, %eax
	sbbq	%rax, %rax
	movl	%esi, -168(%rbp)
	andl	$8, %eax
	addq	$8, %rax
	movq	%rax, -176(%rbp)
.L63:
	pushq	$1
	xorl	%r9d, %r9d
	movl	$3, %r8d
	movq	%r15, %rdx
	pushq	%rbx
	leaq	-128(%rbp), %rbx
	leaq	keylabel.23318(%rip), %rcx
	movq	%r12, %rsi
	pushq	%rbx
	movq	%r13, %rdi
	pushq	$0
	call	tls13_hkdf_expand
	addq	$32, %rsp
	testl	%eax, %eax
	je	.L58
	pushq	$1
	xorl	%r9d, %r9d
	movl	$2, %r8d
	movq	%r15, %rdx
	pushq	-136(%rbp)
	leaq	ivlabel.23326(%rip), %rcx
	movq	%r12, %rsi
	movq	%r13, %rdi
	pushq	-160(%rbp)
	pushq	$0
	call	tls13_hkdf_expand
	addq	$32, %rsp
	testl	%eax, %eax
	je	.L58
	movl	-164(%rbp), %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	-152(%rbp), %rdi
	movq	%r14, %rsi
	call	EVP_CipherInit_ex@PLT
	testl	%eax, %eax
	jle	.L66
	movl	-144(%rbp), %edx
	movq	-152(%rbp), %rdi
	xorl	%ecx, %ecx
	movl	$9, %esi
	call	EVP_CIPHER_CTX_ctrl@PLT
	testl	%eax, %eax
	je	.L66
	cmpq	$0, -176(%rbp)
	je	.L69
	movl	-168(%rbp), %edx
	movq	-152(%rbp), %rdi
	xorl	%ecx, %ecx
	movl	$17, %esi
	call	EVP_CIPHER_CTX_ctrl@PLT
	testl	%eax, %eax
	je	.L66
.L69:
	movq	-152(%rbp), %rdi
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
	movl	$-1, %r9d
	movq	%rbx, %rcx
	call	EVP_CipherInit_ex@PLT
	testl	%eax, %eax
	jle	.L66
	movl	$1, %eax
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L85:
	movl	$375, %r9d
	leaq	.LC0(%rip), %r8
	movl	$6, %ecx
	movq	%r13, %rdi
	movl	$514, %edx
	movl	$80, %esi
	leaq	-128(%rbp), %rbx
	call	ossl_statem_fatal@PLT
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L60:
	movq	%r14, %rdi
	call	EVP_CIPHER_iv_length@PLT
	movl	$0, -168(%rbp)
	movl	%eax, -144(%rbp)
	cltq
	movq	%rax, -136(%rbp)
	movq	$0, -176(%rbp)
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L66:
	movl	$419, %r9d
	leaq	.LC0(%rip), %r8
	movl	$6, %ecx
	movq	%r13, %rdi
	movl	$514, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L88:
	movq	1296(%r13), %rax
	movq	504(%rax), %rax
	movl	36(%rax), %eax
	jmp	.L62
.L87:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1010:
	.size	derive_secret_key_and_iv, .-derive_secret_key_and_iv
	.p2align 4
	.globl	tls13_derive_key
	.type	tls13_derive_key, @function
tls13_derive_key:
.LFB1002:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r9d, %r9d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	$1
	pushq	%r8
	movl	$3, %r8d
	pushq	%rcx
	leaq	keylabel.23318(%rip), %rcx
	pushq	$0
	call	tls13_hkdf_expand
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1002:
	.size	tls13_derive_key, .-tls13_derive_key
	.p2align 4
	.globl	tls13_derive_iv
	.type	tls13_derive_iv, @function
tls13_derive_iv:
.LFB1003:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r9d, %r9d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	$1
	pushq	%r8
	movl	$2, %r8d
	pushq	%rcx
	leaq	ivlabel.23326(%rip), %rcx
	pushq	$0
	call	tls13_hkdf_expand
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1003:
	.size	tls13_derive_iv, .-tls13_derive_iv
	.p2align 4
	.globl	tls13_derive_finishedkey
	.type	tls13_derive_finishedkey, @function
tls13_derive_finishedkey:
.LFB1004:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r9d, %r9d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	$1
	pushq	%r8
	movl	$8, %r8d
	pushq	%rcx
	leaq	finishedlabel.23334(%rip), %rcx
	pushq	$0
	call	tls13_hkdf_expand
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1004:
	.size	tls13_derive_finishedkey, .-tls13_derive_finishedkey
	.p2align 4
	.globl	tls13_generate_secret
	.type	tls13_generate_secret, @function
tls13_generate_secret:
.LFB1005:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movl	$1036, %edi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	xorl	%esi, %esi
	subq	$216, %rsp
	movq	%r9, -232(%rbp)
	movq	%rdx, -216(%rbp)
	movq	%r8, -224(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	EVP_PKEY_CTX_new_id@PLT
	movl	$183, %r9d
	testq	%rax, %rax
	je	.L112
	movq	%rbx, %rdi
	movq	%rax, %r12
	call	EVP_MD_size@PLT
	testl	%eax, %eax
	js	.L114
	cltq
	movq	%rax, -200(%rbp)
	testq	%r14, %r14
	je	.L115
	cmpq	$0, -216(%rbp)
	je	.L110
.L120:
	call	EVP_MD_CTX_new@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L103
	xorl	%edx, %edx
	movq	%rbx, %rsi
	movq	%rax, %rdi
	call	EVP_DigestInit_ex@PLT
	testl	%eax, %eax
	jle	.L103
	leaq	-128(%rbp), %r9
	xorl	%edx, %edx
	movq	%r15, %rdi
	movq	%r9, %rsi
	movq	%r9, -248(%rbp)
	call	EVP_DigestFinal_ex@PLT
	testl	%eax, %eax
	jle	.L103
	movq	%r15, %rdi
	call	EVP_MD_CTX_free@PLT
	movq	-200(%rbp), %rax
	pushq	$1
	leaq	-192(%rbp), %rcx
	movq	-248(%rbp), %r9
	movq	-216(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	pushq	%rax
	movl	$7, %r8d
	pushq	%rcx
	pushq	%rax
	movq	%rcx, -240(%rbp)
	leaq	derived_secret_label.23348(%rip), %rcx
	call	tls13_hkdf_expand
	addq	$32, %rsp
	testl	%eax, %eax
	je	.L116
	movq	-200(%rbp), %rax
	movq	%r12, %rdi
	movq	-240(%rbp), %r15
	movq	%rax, -216(%rbp)
	call	EVP_PKEY_derive_init@PLT
	testl	%eax, %eax
	jle	.L107
.L105:
	xorl	%r9d, %r9d
	movl	$4103, %ecx
	movl	$1024, %edx
	movq	%r12, %rdi
	movl	$1, %r8d
	movl	$-1, %esi
	call	EVP_PKEY_CTX_ctrl@PLT
	testl	%eax, %eax
	jle	.L107
	xorl	%r8d, %r8d
	movq	%rbx, %r9
	movl	$4099, %ecx
	movl	$1024, %edx
	movl	$-1, %esi
	movq	%r12, %rdi
	call	EVP_PKEY_CTX_ctrl@PLT
	testl	%eax, %eax
	jg	.L117
	.p2align 4,,10
	.p2align 3
.L107:
	movq	%r13, %rdi
	movl	$245, %r9d
	movl	$68, %ecx
	xorl	%r13d, %r13d
	leaq	.LC0(%rip), %r8
	movl	$591, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
.L106:
	movq	%r12, %rdi
	call	EVP_PKEY_CTX_free@PLT
	cmpq	-240(%rbp), %r15
	je	.L118
.L95:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L119
	leaq	-40(%rbp), %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L110:
	.cfi_restore_state
	leaq	-192(%rbp), %rax
	movq	%r12, %rdi
	movq	$0, -216(%rbp)
	leaq	default_zeros(%rip), %r15
	movq	%rax, -240(%rbp)
	call	EVP_PKEY_derive_init@PLT
	testl	%eax, %eax
	jg	.L105
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L115:
	cmpq	$0, -216(%rbp)
	movq	%rax, -224(%rbp)
	leaq	default_zeros(%rip), %r14
	jne	.L120
	jmp	.L110
	.p2align 4,,10
	.p2align 3
.L103:
	movl	$212, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movq	%r13, %rdi
	movl	$591, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	movq	%r15, %rdi
	call	EVP_MD_CTX_free@PLT
	movq	%r12, %rdi
	call	EVP_PKEY_CTX_free@PLT
.L113:
	xorl	%r13d, %r13d
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L114:
	movl	$191, %r9d
.L112:
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movl	$591, %edx
	movq	%r13, %rdi
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L118:
	movq	-200(%rbp), %rsi
	movq	%r15, %rdi
	call	OPENSSL_cleanse@PLT
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L117:
	movq	%r14, %r9
	movl	$4101, %ecx
	movl	$1024, %edx
	movq	%r12, %rdi
	movl	-224(%rbp), %r8d
	movl	$-1, %esi
	call	EVP_PKEY_CTX_ctrl@PLT
	testl	%eax, %eax
	jle	.L107
	movq	%r15, %r9
	movl	$4100, %ecx
	movl	$1024, %edx
	movq	%r12, %rdi
	movl	-216(%rbp), %r8d
	movl	$-1, %esi
	call	EVP_PKEY_CTX_ctrl@PLT
	testl	%eax, %eax
	jle	.L107
	movq	-232(%rbp), %rsi
	leaq	-200(%rbp), %rdx
	movq	%r12, %rdi
	call	EVP_PKEY_derive@PLT
	testl	%eax, %eax
	jle	.L107
	movl	$1, %r13d
	jmp	.L106
	.p2align 4,,10
	.p2align 3
.L116:
	movq	%r12, %rdi
	call	EVP_PKEY_CTX_free@PLT
	jmp	.L113
.L119:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1005:
	.size	tls13_generate_secret, .-tls13_generate_secret
	.p2align 4
	.globl	tls13_generate_handshake_secret
	.type	tls13_generate_handshake_secret, @function
tls13_generate_handshake_secret:
.LFB1006:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$184, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -216(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	ssl_handshake_md@PLT
	xorl	%esi, %esi
	movl	$1036, %edi
	movq	%rax, %rbx
	call	EVP_PKEY_CTX_new_id@PLT
	movl	$183, %r9d
	testq	%rax, %rax
	je	.L141
	movq	%rbx, %rdi
	movq	%rax, %r12
	call	EVP_MD_size@PLT
	testl	%eax, %eax
	js	.L143
	cltq
	movq	%rax, -200(%rbp)
	testq	%r14, %r14
	je	.L144
.L126:
	call	EVP_MD_CTX_new@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L129
	xorl	%edx, %edx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	EVP_DigestInit_ex@PLT
	testl	%eax, %eax
	jle	.L129
	leaq	-128(%rbp), %r9
	xorl	%edx, %edx
	movq	%r15, %rdi
	movq	%r9, %rsi
	movq	%r9, -224(%rbp)
	call	EVP_DigestFinal_ex@PLT
	testl	%eax, %eax
	jle	.L129
	movq	%r15, %rdi
	leaq	-192(%rbp), %r15
	call	EVP_MD_CTX_free@PLT
	pushq	$1
	movl	$7, %r8d
	movq	%rbx, %rsi
	movq	-200(%rbp), %rax
	movq	-224(%rbp), %r9
	movq	%r13, %rdi
	leaq	316(%r13), %rdx
	leaq	derived_secret_label.23348(%rip), %rcx
	pushq	%rax
	pushq	%r15
	pushq	%rax
	call	tls13_hkdf_expand
	addq	$32, %rsp
	testl	%eax, %eax
	je	.L145
	movq	-200(%rbp), %rax
	movq	%r12, %rdi
	movq	%rax, -224(%rbp)
	call	EVP_PKEY_derive_init@PLT
	testl	%eax, %eax
	jle	.L133
	xorl	%r9d, %r9d
	movl	$4103, %ecx
	movl	$1024, %edx
	movq	%r12, %rdi
	movl	$1, %r8d
	movl	$-1, %esi
	call	EVP_PKEY_CTX_ctrl@PLT
	testl	%eax, %eax
	jg	.L146
.L133:
	movq	%r13, %rdi
	movl	$245, %r9d
	movl	$68, %ecx
	xorl	%r13d, %r13d
	leaq	.LC0(%rip), %r8
	movl	$591, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
.L132:
	movq	%r12, %rdi
	call	EVP_PKEY_CTX_free@PLT
	movq	-200(%rbp), %rsi
	movq	%r15, %rdi
	call	OPENSSL_cleanse@PLT
	jmp	.L121
	.p2align 4,,10
	.p2align 3
.L144:
	movq	%rax, -216(%rbp)
	leaq	default_zeros(%rip), %r14
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L129:
	movl	$212, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movq	%r13, %rdi
	movl	$591, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	movq	%r15, %rdi
	call	EVP_MD_CTX_free@PLT
	movq	%r12, %rdi
	call	EVP_PKEY_CTX_free@PLT
.L142:
	xorl	%r13d, %r13d
.L121:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L147
	leaq	-40(%rbp), %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L143:
	.cfi_restore_state
	movl	$191, %r9d
.L141:
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movl	$591, %edx
	movq	%r13, %rdi
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L142
	.p2align 4,,10
	.p2align 3
.L146:
	xorl	%r8d, %r8d
	movq	%rbx, %r9
	movl	$4099, %ecx
	movl	$1024, %edx
	movl	$-1, %esi
	movq	%r12, %rdi
	call	EVP_PKEY_CTX_ctrl@PLT
	testl	%eax, %eax
	jle	.L133
	movq	%r14, %r9
	movl	$4101, %ecx
	movl	$1024, %edx
	movq	%r12, %rdi
	movl	-216(%rbp), %r8d
	movl	$-1, %esi
	call	EVP_PKEY_CTX_ctrl@PLT
	testl	%eax, %eax
	jle	.L133
	movq	%r15, %r9
	movl	$4100, %ecx
	movl	$1024, %edx
	movq	%r12, %rdi
	movl	-224(%rbp), %r8d
	movl	$-1, %esi
	call	EVP_PKEY_CTX_ctrl@PLT
	testl	%eax, %eax
	jle	.L133
	leaq	-200(%rbp), %rdx
	leaq	380(%r13), %rsi
	movq	%r12, %rdi
	call	EVP_PKEY_derive@PLT
	testl	%eax, %eax
	jle	.L133
	movl	$1, %r13d
	jmp	.L132
	.p2align 4,,10
	.p2align 3
.L145:
	movq	%r12, %rdi
	call	EVP_PKEY_CTX_free@PLT
	jmp	.L142
.L147:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1006:
	.size	tls13_generate_handshake_secret, .-tls13_generate_handshake_secret
	.p2align 4
	.globl	tls13_generate_master_secret
	.type	tls13_generate_master_secret, @function
tls13_generate_master_secret:
.LFB1007:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$8, %rsp
	call	ssl_handshake_md@PLT
	movq	%rax, %rdi
	movq	%rax, %r13
	call	EVP_MD_size@PLT
	movq	%rbx, %r9
	movq	%r14, %rdx
	movq	%r13, %rsi
	cltq
	movq	%r12, %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rax, (%r15)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	tls13_generate_secret
	.cfi_endproc
.LFE1007:
	.size	tls13_generate_master_secret, .-tls13_generate_master_secret
	.p2align 4
	.globl	tls13_final_finish_mac
	.type	tls13_final_finish_mac, @function
tls13_final_finish_mac:
.LFB1008:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-200(%rbp), %r15
	leaq	-192(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$200, %rsp
	movq	%rcx, -224(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	ssl_handshake_md@PLT
	movq	%rax, -216(%rbp)
	call	EVP_MD_CTX_new@PLT
	movq	%r15, %rcx
	movl	$64, %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, %r12
	call	ssl_handshake_hash@PLT
	testl	%eax, %eax
	je	.L161
	movq	8(%r13), %rax
	movq	-200(%rbp), %rcx
	movq	192(%rax), %rax
	cmpq	%rbx, 64(%rax)
	je	.L163
	movq	168(%r13), %rax
	cmpq	$0, 408(%rax)
	je	.L154
	cmpq	$0, 544(%rax)
	jne	.L155
.L154:
	leaq	572(%r13), %rdx
	xorl	%esi, %esi
	movl	$855, %edi
	call	EVP_PKEY_new_raw_private_key@PLT
	movq	%rax, %rbx
.L153:
	testq	%rbx, %rbx
	je	.L159
	testq	%r12, %r12
	je	.L159
	movq	-216(%rbp), %rdx
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	movq	%rbx, %r8
	movq	%r12, %rdi
	call	EVP_DigestSignInit@PLT
	testl	%eax, %eax
	jle	.L159
	movq	-200(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	jle	.L159
	movq	-224(%rbp), %rsi
	movq	%r15, %rdx
	movq	%r12, %rdi
	call	EVP_DigestSignFinal@PLT
	testl	%eax, %eax
	jle	.L159
	movq	-200(%rbp), %r13
	jmp	.L151
	.p2align 4,,10
	.p2align 3
.L159:
	movq	%r13, %rdi
	movl	$326, %r9d
	movl	$68, %ecx
	xorl	%r13d, %r13d
	leaq	.LC0(%rip), %r8
	movl	$605, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
.L151:
	movq	%rbx, %rdi
	call	EVP_PKEY_free@PLT
	movq	%r12, %rdi
	call	EVP_MD_CTX_free@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L164
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L161:
	.cfi_restore_state
	xorl	%ebx, %ebx
	xorl	%r13d, %r13d
	jmp	.L151
	.p2align 4,,10
	.p2align 3
.L155:
	movq	%r13, %rdi
	movq	%rcx, -232(%rbp)
	call	ssl_handshake_md@PLT
	leaq	-128(%rbp), %r11
	pushq	$1
	xorl	%r9d, %r9d
	movq	-232(%rbp), %rcx
	movq	%rax, %rsi
	leaq	828(%r13), %rdx
	movl	$8, %r8d
	movq	%r13, %rdi
	movq	%r11, -232(%rbp)
	pushq	%rcx
	leaq	finishedlabel.23334(%rip), %rcx
	pushq	%r11
	pushq	$0
	call	tls13_hkdf_expand
	addq	$32, %rsp
	movq	-232(%rbp), %r11
	testl	%eax, %eax
	je	.L161
	movq	-200(%rbp), %rcx
	movq	%r11, %rdx
	xorl	%esi, %esi
	movl	$855, %edi
	movq	%r11, -232(%rbp)
	call	EVP_PKEY_new_raw_private_key@PLT
	movq	-232(%rbp), %r11
	movl	$64, %esi
	movq	%rax, %rbx
	movq	%r11, %rdi
	call	OPENSSL_cleanse@PLT
	jmp	.L153
	.p2align 4,,10
	.p2align 3
.L163:
	leaq	636(%r13), %rdx
	xorl	%esi, %esi
	movl	$855, %edi
	call	EVP_PKEY_new_raw_private_key@PLT
	movq	%rax, %rbx
	jmp	.L153
.L164:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1008:
	.size	tls13_final_finish_mac, .-tls13_final_finish_mac
	.p2align 4
	.globl	tls13_setup_key_block
	.type	tls13_setup_key_block, @function
tls13_setup_key_block:
.LFB1009:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%ecx, %ecx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdx
	leaq	-40(%rbp), %rsi
	subq	$64, %rsp
	movq	1296(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	168(%r12), %rax
	movq	568(%rax), %rax
	movq	%rax, 504(%rdi)
	pushq	$0
	call	ssl_cipher_get_evp@PLT
	popq	%rdx
	popq	%rcx
	testl	%eax, %eax
	je	.L170
	movq	-40(%rbp), %xmm0
	movq	168(%r12), %rax
	movhps	-32(%rbp), %xmm0
	movups	%xmm0, 632(%rax)
	movl	$1, %eax
.L165:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L171
	movq	-8(%rbp), %r12
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L170:
	.cfi_restore_state
	movl	$138, %ecx
	movl	$441, %edx
	movq	%r12, %rdi
	movl	%eax, -52(%rbp)
	movl	$349, %r9d
	leaq	.LC0(%rip), %r8
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	movl	-52(%rbp), %eax
	jmp	.L165
.L171:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1009:
	.size	tls13_setup_key_block, .-tls13_setup_key_block
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"SERVER_TRAFFIC_SECRET_0"
	.section	.rodata.str1.8
	.align 8
.LC2:
	.string	"SERVER_HANDSHAKE_TRAFFIC_SECRET"
	.section	.rodata.str1.1
.LC3:
	.string	"CLIENT_TRAFFIC_SECRET_0"
	.section	.rodata.str1.8
	.align 8
.LC4:
	.string	"CLIENT_HANDSHAKE_TRAFFIC_SECRET"
	.section	.rodata.str1.1
.LC5:
	.string	"CLIENT_EARLY_TRAFFIC_SECRET"
.LC6:
	.string	"EARLY_EXPORTER_SECRET"
.LC7:
	.string	"EXPORTER_SECRET"
	.text
	.p2align 4
	.globl	tls13_change_cipher_state
	.type	tls13_change_cipher_state, @function
tls13_change_cipher_state:
.LFB1011:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$264, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -208(%rbp)
	andl	$1, %r13d
	je	.L173
	movq	1088(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L174
	call	EVP_CIPHER_CTX_reset@PLT
	movq	1088(%r14), %r12
.L175:
	leaq	1096(%r14), %rax
	leaq	2112(%r14), %rdi
	movq	%rax, -232(%rbp)
	call	RECORD_LAYER_reset_read_sequence@PLT
	movl	%ebx, %eax
	andl	$18, %eax
	cmpl	$18, %eax
	jne	.L279
.L180:
	testb	$64, %bl
	jne	.L280
	testb	$-128, %bl
	jne	.L281
	leaq	.LC3(%rip), %rax
	leaq	444(%r14), %r13
	movq	$0, -264(%rbp)
	movq	%rax, -256(%rbp)
	leaq	700(%r14), %r11
	leaq	client_application_traffic.23411(%rip), %r15
	movq	$0, -248(%rbp)
.L212:
	movq	%r14, %rdi
	movq	%r11, -280(%rbp)
	call	ssl_handshake_md@PLT
	movl	$1, %esi
	movq	%r14, %rdi
	movq	%rax, -272(%rbp)
	movq	168(%r14), %rax
	movq	632(%rax), %rax
	movq	%rax, -240(%rbp)
	call	ssl3_digest_cached_records@PLT
	movq	-272(%rbp), %r10
	movq	-280(%rbp), %r11
	testl	%eax, %eax
	jne	.L198
.L277:
	leaq	-192(%rbp), %r12
.L199:
	xorl	%r13d, %r13d
.L176:
	movl	$64, %esi
	movq	%r12, %rdi
	call	OPENSSL_cleanse@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L282
	leaq	-40(%rbp), %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L173:
	.cfi_restore_state
	movl	$1, 124(%rdi)
	movq	1136(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L178
	call	EVP_CIPHER_CTX_reset@PLT
	movq	1136(%r14), %r12
.L179:
	leaq	1144(%r14), %rax
	leaq	2112(%r14), %rdi
	movq	%rax, -232(%rbp)
	call	RECORD_LAYER_reset_write_sequence@PLT
	movl	%ebx, %eax
	andl	$18, %eax
	cmpl	$18, %eax
	je	.L180
.L279:
	movl	%ebx, %eax
	andl	$33, %eax
	cmpl	$33, %eax
	je	.L180
	testb	$-128, %bl
	jne	.L283
	leaq	.LC1(%rip), %rax
	leaq	444(%r14), %r13
	movq	$0, -264(%rbp)
	movq	%rax, -256(%rbp)
	leaq	server_application_traffic.23413(%rip), %r15
	movq	$0, -248(%rbp)
.L196:
	testb	$64, %bl
	je	.L284
	movq	$0, -240(%rbp)
	leaq	-128(%rbp), %r9
	xorl	%r10d, %r10d
	movq	%r9, %r11
.L197:
	leaq	server_application_traffic.23413(%rip), %rax
	cmpq	%rax, %r15
	je	.L285
	leaq	server_handshake_traffic.23412(%rip), %rax
	cmpq	%rax, %r15
	je	.L202
	leaq	client_application_traffic.23411(%rip), %rax
	cmpq	%rax, %r15
	je	.L203
	movb	$0, -272(%rbp)
	movl	$12, %eax
.L192:
	subq	$8, %rsp
	andl	$2, %ebx
	movq	%r11, %r9
	movq	%r13, %r8
	pushq	%r12
	leaq	-192(%rbp), %r12
	movl	%ebx, %esi
	movq	%r10, %rdx
	pushq	-232(%rbp)
	movq	-240(%rbp), %rcx
	movq	%r14, %rdi
	pushq	%r12
	pushq	%rax
	pushq	%r15
	call	derive_secret_key_and_iv
	movq	-208(%rbp), %rcx
	addq	$48, %rsp
	testl	%eax, %eax
	je	.L199
.L204:
	movq	-256(%rbp), %rsi
	movq	%r12, %rdx
	movq	%r14, %rdi
	call	ssl_log_secret@PLT
	testl	%eax, %eax
	je	.L199
	cmpq	$0, -248(%rbp)
	je	.L210
	movq	%r14, %rdi
	call	ssl_handshake_md@PLT
	pushq	$1
	xorl	%r9d, %r9d
	movl	$8, %r8d
	pushq	-264(%rbp)
	movq	%rax, %rsi
	movq	%r12, %rdx
	movq	%r14, %rdi
	pushq	-248(%rbp)
	leaq	finishedlabel.23334(%rip), %rcx
	pushq	$0
	call	tls13_hkdf_expand
	addq	$32, %rsp
	testl	%eax, %eax
	je	.L199
.L210:
	movl	56(%r14), %eax
	testl	%eax, %eax
	jne	.L209
	cmpb	$0, -272(%rbp)
	jne	.L286
.L209:
	movl	$0, 124(%r14)
	movl	$1, %r13d
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L174:
	call	EVP_CIPHER_CTX_new@PLT
	movq	%rax, 1088(%r14)
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L175
	movl	$65, %ecx
	movl	$440, %edx
	movq	%r14, %rdi
	xorl	%r13d, %r13d
	movl	$472, %r9d
	leaq	.LC0(%rip), %r8
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	leaq	-192(%rbp), %r12
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L178:
	call	EVP_CIPHER_CTX_new@PLT
	movq	%rax, 1136(%r14)
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L179
	movl	$488, %r9d
	leaq	.LC0(%rip), %r8
	movl	$65, %ecx
	movq	%r14, %rdi
	movl	$440, %edx
	movl	$80, %esi
	leaq	-192(%rbp), %r12
	call	ossl_statem_fatal@PLT
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L198:
	leaq	-128(%rbp), %r9
	leaq	-208(%rbp), %rcx
	movl	$64, %edx
	movq	%r14, %rdi
	movq	%r9, %rsi
	movq	%r11, -288(%rbp)
	movq	%r10, -280(%rbp)
	movq	%r9, -272(%rbp)
	call	ssl_handshake_hash@PLT
	movq	-272(%rbp), %r9
	movq	-280(%rbp), %r10
	testl	%eax, %eax
	movq	-288(%rbp), %r11
	jne	.L197
	jmp	.L277
	.p2align 4,,10
	.p2align 3
.L203:
	movq	-208(%rbp), %rdx
	movq	%r14, %rdi
	movq	%r11, -288(%rbp)
	movq	%r10, -280(%rbp)
	movq	%r9, -296(%rbp)
	movq	%rdx, -272(%rbp)
	call	ssl_handshake_md@PLT
	pushq	$1
	movl	$10, %r8d
	movq	%r14, %rdi
	movq	-272(%rbp), %rdx
	movq	%rax, %rsi
	leaq	508(%r14), %rax
	movq	-296(%rbp), %r9
	leaq	resumption_master_secret.23415(%rip), %rcx
	pushq	%rdx
	pushq	%rax
	pushq	%rdx
	movq	%r13, %rdx
	call	tls13_hkdf_expand
	addq	$32, %rsp
	movq	-280(%rbp), %r10
	movq	-288(%rbp), %r11
	testl	%eax, %eax
	je	.L277
	subq	$8, %rsp
	andl	$2, %ebx
	movq	%r11, %r9
	movq	%r13, %r8
	pushq	%r12
	leaq	-192(%rbp), %r12
	movl	%ebx, %esi
	movq	%r10, %rdx
	pushq	-232(%rbp)
	movq	-240(%rbp), %rcx
	movq	%r14, %rdi
	pushq	%r12
	pushq	$12
	pushq	%r15
	call	derive_secret_key_and_iv
	addq	$48, %rsp
	testl	%eax, %eax
	je	.L199
	movq	-208(%rbp), %rcx
	leaq	828(%r14), %rdi
	movq	%r12, %rsi
	movq	%rcx, %rdx
	movq	%rcx, -232(%rbp)
	call	memcpy@PLT
	movb	$0, -272(%rbp)
	movq	-232(%rbp), %rcx
	jmp	.L204
	.p2align 4,,10
	.p2align 3
.L280:
	movq	1296(%r14), %rdi
	call	SSL_SESSION_get0_cipher@PLT
	leaq	-200(%rbp), %rcx
	xorl	%edx, %edx
	movl	$3, %esi
	movq	%rax, %r13
	movq	168(%r14), %rax
	movq	224(%rax), %rdi
	call	BIO_ctrl@PLT
	movq	%rax, -248(%rbp)
	testq	%rax, %rax
	jle	.L287
	cmpl	$2, 132(%r14)
	je	.L288
.L185:
	testq	%r13, %r13
	je	.L289
	call	EVP_MD_CTX_new@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L290
	movq	%r13, %rdi
	call	SSL_CIPHER_get_cipher_nid@PLT
	movl	%eax, %edi
	call	OBJ_nid2sn@PLT
	movq	%rax, %rdi
	call	EVP_get_cipherbyname@PLT
	movl	64(%r13), %edi
	movq	%rax, -240(%rbp)
	call	ssl_md@PLT
	testq	%rax, %rax
	je	.L190
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r15, %rdi
	movq	%rax, -256(%rbp)
	call	EVP_DigestInit_ex@PLT
	testl	%eax, %eax
	jne	.L291
.L190:
	movq	%r14, %rdi
	movl	$561, %r9d
	movl	$68, %ecx
	movl	$440, %edx
	leaq	.LC0(%rip), %r8
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	movq	%r15, %rdi
	call	EVP_MD_CTX_free@PLT
.L184:
	xorl	%r13d, %r13d
	leaq	-192(%rbp), %r12
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L283:
	leaq	636(%r14), %rax
	movq	%r14, %rdi
	leaq	380(%r14), %r13
	movq	%rax, -248(%rbp)
	leaq	server_handshake_traffic.23412(%rip), %r15
	call	ssl_handshake_md@PLT
	movq	%rax, %rdi
	call	EVP_MD_size@PLT
	cltq
	movq	%rax, -264(%rbp)
	leaq	.LC2(%rip), %rax
	movq	%rax, -256(%rbp)
	jmp	.L196
	.p2align 4,,10
	.p2align 3
.L281:
	leaq	572(%r14), %rax
	movq	%r14, %rdi
	leaq	380(%r14), %r13
	movq	%rax, -248(%rbp)
	leaq	client_handshake_traffic.23410(%rip), %r15
	call	ssl_handshake_md@PLT
	movq	%rax, %rdi
	call	EVP_MD_size@PLT
	leaq	764(%r14), %r11
	cltq
	movq	%rax, -264(%rbp)
	leaq	.LC4(%rip), %rax
	movq	%rax, -256(%rbp)
	jmp	.L212
	.p2align 4,,10
	.p2align 3
.L202:
	movq	-208(%rbp), %rdx
	movq	%r9, %rsi
	leaq	764(%r14), %rdi
	movq	%r11, -280(%rbp)
	movq	%r10, -272(%rbp)
	andl	$2, %ebx
	call	memcpy@PLT
	subq	$8, %rsp
	movl	%ebx, %esi
	movq	%r13, %r8
	pushq	%r12
	leaq	-192(%rbp), %r12
	movq	-280(%rbp), %r11
	movq	%r14, %rdi
	pushq	-232(%rbp)
	movq	-272(%rbp), %r10
	movq	-240(%rbp), %rcx
	pushq	%r12
	movq	%r11, %r9
	pushq	$12
	movq	%r10, %rdx
	pushq	%r15
	call	derive_secret_key_and_iv
	addq	$48, %rsp
	testl	%eax, %eax
	je	.L199
.L278:
	movb	$0, -272(%rbp)
	movq	-208(%rbp), %rcx
	jmp	.L204
	.p2align 4,,10
	.p2align 3
.L285:
	movq	-208(%rbp), %rdx
	movq	%r9, %rsi
	leaq	700(%r14), %rdi
	movq	%r11, -280(%rbp)
	movq	%r10, -272(%rbp)
	andl	$2, %ebx
	call	memcpy@PLT
	subq	$8, %rsp
	movl	%ebx, %esi
	movq	%r13, %r8
	pushq	%r12
	leaq	-192(%rbp), %r12
	movq	-280(%rbp), %r11
	movq	%r14, %rdi
	pushq	-232(%rbp)
	movq	-272(%rbp), %r10
	movq	-240(%rbp), %rcx
	pushq	%r12
	movq	%r11, %r9
	pushq	$12
	movq	%r10, %rdx
	pushq	%r15
	movq	%r11, -232(%rbp)
	call	derive_secret_key_and_iv
	addq	$48, %rsp
	movq	-232(%rbp), %r11
	testl	%eax, %eax
	je	.L199
	movq	-208(%rbp), %rbx
	movq	%r12, %rsi
	leaq	892(%r14), %rdi
	movq	%r11, -232(%rbp)
	leaq	956(%r14), %r15
	movq	%rbx, %rdx
	call	memcpy@PLT
	movq	%r14, %rdi
	call	ssl_handshake_md@PLT
	pushq	$1
	movl	$10, %r8d
	movq	%r13, %rdx
	pushq	%rbx
	movq	-232(%rbp), %r11
	movq	%rax, %rsi
	movq	%r14, %rdi
	pushq	%r15
	leaq	exporter_master_secret.23414(%rip), %rcx
	pushq	%rbx
	movq	%r11, %r9
	call	tls13_hkdf_expand
	addq	$32, %rsp
	testl	%eax, %eax
	je	.L199
	movq	-208(%rbp), %rcx
	movq	%r15, %rdx
	leaq	.LC7(%rip), %rsi
	movq	%r14, %rdi
	call	ssl_log_secret@PLT
	testl	%eax, %eax
	jne	.L278
	jmp	.L199
	.p2align 4,,10
	.p2align 3
.L288:
	movl	6176(%r14), %eax
	testl	%eax, %eax
	je	.L185
	movq	1296(%r14), %rdx
	movl	580(%rdx), %edx
	testl	%edx, %edx
	jne	.L185
	movq	1304(%r14), %rdi
	testq	%rdi, %rdi
	je	.L186
	cmpl	580(%rdi), %eax
	je	.L292
.L186:
	movl	$532, %r9d
.L276:
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movl	$440, %edx
	movq	%r14, %rdi
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L291:
	movq	-248(%rbp), %rdx
	movq	-200(%rbp), %rsi
	movq	%r15, %rdi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	je	.L190
	leaq	-128(%rbp), %rax
	leaq	-212(%rbp), %rdx
	movq	%r15, %rdi
	movq	%rax, %rsi
	movq	%rax, -272(%rbp)
	call	EVP_DigestFinal_ex@PLT
	testl	%eax, %eax
	je	.L190
	movl	-212(%rbp), %ecx
	movq	%r15, %rdi
	leaq	1020(%r14), %r15
	leaq	316(%r14), %r13
	movq	%rcx, -208(%rbp)
	call	EVP_MD_CTX_free@PLT
	pushq	$1
	movl	$12, %r8d
	movq	%r13, %rdx
	movq	-208(%rbp), %rax
	movq	-256(%rbp), %r10
	movq	%r14, %rdi
	leaq	early_exporter_master_secret.23416(%rip), %rcx
	movq	-272(%rbp), %r9
	pushq	%rax
	movq	%r10, %rsi
	pushq	%r15
	pushq	%rax
	movq	%r10, -248(%rbp)
	call	tls13_hkdf_expand
	addq	$32, %rsp
	movq	-248(%rbp), %r10
	testl	%eax, %eax
	je	.L293
	movq	-208(%rbp), %rcx
	movq	%r15, %rdx
	leaq	.LC6(%rip), %rsi
	movq	%r14, %rdi
	movq	%r10, -280(%rbp)
	call	ssl_log_secret@PLT
	testl	%eax, %eax
	je	.L184
	leaq	.LC5(%rip), %rax
	movq	-272(%rbp), %r11
	movq	$0, -248(%rbp)
	leaq	client_early_traffic.23409(%rip), %r15
	movq	%rax, -256(%rbp)
	movq	-280(%rbp), %r10
	movl	$11, %eax
	movq	$0, -264(%rbp)
	movb	$1, -272(%rbp)
	jmp	.L192
	.p2align 4,,10
	.p2align 3
.L287:
	movl	$515, %r9d
	leaq	.LC0(%rip), %r8
	movl	$332, %ecx
	movq	%r14, %rdi
	movl	$440, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L286:
	movl	$2, 124(%r14)
	movl	$1, %r13d
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L289:
	movl	$540, %r9d
	leaq	.LC0(%rip), %r8
	movl	$219, %ecx
	movq	%r14, %rdi
	movl	$440, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L290:
	movl	$552, %r9d
	leaq	.LC0(%rip), %r8
	movl	$65, %ecx
	movq	%r14, %rdi
	movl	$440, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L184
.L292:
	call	SSL_SESSION_get0_cipher@PLT
	movq	%rax, %r13
	jmp	.L185
.L293:
	movl	$575, %r9d
	jmp	.L276
.L282:
	call	__stack_chk_fail@PLT
.L284:
	leaq	-128(%rbp), %r11
	jmp	.L212
	.cfi_endproc
.LFE1011:
	.size	tls13_change_cipher_state, .-tls13_change_cipher_state
	.p2align 4
	.globl	tls13_update_key
	.type	tls13_update_key, @function
tls13_update_key:
.LFB1012:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	leaq	892(%rbx), %r13
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	ssl_handshake_md@PLT
	movq	%rax, %rdi
	call	EVP_MD_size@PLT
	movl	%eax, -140(%rbp)
	cmpl	%r12d, 56(%rbx)
	je	.L296
	leaq	828(%rbx), %r13
.L296:
	leaq	2112(%rbx), %rdi
	testl	%r12d, %r12d
	je	.L297
	movl	$1, 124(%rbx)
	movq	1136(%rbx), %r15
	leaq	1144(%rbx), %r14
	call	RECORD_LAYER_reset_write_sequence@PLT
.L298:
	movq	168(%rbx), %rax
	movq	%rbx, %rdi
	movq	632(%rax), %rcx
	movq	%rcx, -136(%rbp)
	call	ssl_handshake_md@PLT
	subq	$8, %rsp
	movl	%r12d, %esi
	xorl	%r9d, %r9d
	pushq	%r15
	movq	%rax, %rdx
	leaq	application_traffic.23442(%rip), %rax
	movq	%r13, %r8
	pushq	%r14
	leaq	-128(%rbp), %r14
	movq	-136(%rbp), %rcx
	movq	%rbx, %rdi
	pushq	%r14
	pushq	$11
	pushq	%rax
	call	derive_secret_key_and_iv
	addq	$48, %rsp
	movl	%eax, %r12d
	testl	%eax, %eax
	je	.L299
	movslq	-140(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movl	$1, %r12d
	call	memcpy@PLT
	movl	$0, 124(%rbx)
.L299:
	movl	$64, %esi
	movq	%r14, %rdi
	call	OPENSSL_cleanse@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L306
	leaq	-40(%rbp), %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L297:
	.cfi_restore_state
	movq	1088(%rbx), %r15
	leaq	1096(%rbx), %r14
	call	RECORD_LAYER_reset_read_sequence@PLT
	jmp	.L298
.L306:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1012:
	.size	tls13_update_key, .-tls13_update_key
	.p2align 4
	.globl	tls13_alert_code
	.type	tls13_alert_code, @function
tls13_alert_code:
.LFB1013:
	.cfi_startproc
	endbr64
	cmpl	$109, %edi
	je	.L308
	cmpl	$116, %edi
	je	.L308
	jmp	tls1_alert_code@PLT
	.p2align 4,,10
	.p2align 3
.L308:
	movl	%edi, %eax
	ret
	.cfi_endproc
.LFE1013:
	.size	tls13_alert_code, .-tls13_alert_code
	.p2align 4
	.globl	tls13_export_keying_material
	.type	tls13_export_keying_material, @function
tls13_export_keying_material:
.LFB1014:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%r9, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$280, %rsp
	movq	%rsi, -288(%rbp)
	movq	%rdx, -296(%rbp)
	movq	%rcx, -280(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	ssl_handshake_md@PLT
	movq	%rax, %r13
	call	EVP_MD_CTX_new@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L312
	movq	%r12, %rdi
	call	ossl_statem_export_allowed@PLT
	testl	%eax, %eax
	je	.L312
	movl	24(%rbp), %eax
	movq	%r13, %rsi
	movq	%r15, %rdi
	testl	%eax, %eax
	movl	$0, %eax
	cmovne	16(%rbp), %rax
	xorl	%edx, %edx
	movq	%rax, 16(%rbp)
	call	EVP_DigestInit_ex@PLT
	testl	%eax, %eax
	jle	.L312
	movq	16(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	jle	.L312
	leaq	-192(%rbp), %r14
	leaq	-264(%rbp), %rdx
	movq	%r15, %rdi
	movq	%r14, %rsi
	call	EVP_DigestFinal_ex@PLT
	testl	%eax, %eax
	jle	.L312
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	EVP_DigestInit_ex@PLT
	testl	%eax, %eax
	jle	.L312
	leaq	-128(%rbp), %r9
	leaq	-260(%rbp), %rdx
	movq	%r15, %rdi
	movq	%r9, %rsi
	movq	%r9, -304(%rbp)
	call	EVP_DigestFinal_ex@PLT
	testl	%eax, %eax
	jle	.L312
	movl	-264(%rbp), %eax
	pushq	$0
	movq	%rbx, %r8
	movq	%r13, %rsi
	leaq	-256(%rbp), %r11
	movq	-304(%rbp), %r9
	movq	%r12, %rdi
	movq	-280(%rbp), %rcx
	pushq	%rax
	movl	-260(%rbp), %eax
	leaq	956(%r12), %rdx
	pushq	%r11
	pushq	%rax
	movq	%r11, -312(%rbp)
	call	tls13_hkdf_expand
	addq	$32, %rsp
	testl	%eax, %eax
	je	.L312
	movl	-264(%rbp), %eax
	pushq	$0
	movq	%r12, %rdi
	movq	%r14, %r9
	pushq	-296(%rbp)
	movl	$8, %r8d
	movq	%r13, %rsi
	xorl	%r12d, %r12d
	pushq	-288(%rbp)
	leaq	exporterlabel.23465(%rip), %rcx
	movq	-312(%rbp), %r11
	pushq	%rax
	movq	%r11, %rdx
	call	tls13_hkdf_expand
	addq	$32, %rsp
	testl	%eax, %eax
	setne	%r12b
	jmp	.L311
	.p2align 4,,10
	.p2align 3
.L312:
	xorl	%r12d, %r12d
.L311:
	movq	%r15, %rdi
	call	EVP_MD_CTX_free@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L322
	leaq	-40(%rbp), %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L322:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1014:
	.size	tls13_export_keying_material, .-tls13_export_keying_material
	.p2align 4
	.globl	tls13_export_keying_material_early
	.type	tls13_export_keying_material_early, @function
tls13_export_keying_material_early:
.LFB1015:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%r9, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$280, %rsp
	movq	%rsi, -288(%rbp)
	movq	%rdx, -296(%rbp)
	movq	%rcx, -280(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	EVP_MD_CTX_new@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L326
	movq	%r12, %rdi
	call	ossl_statem_export_early_allowed@PLT
	testl	%eax, %eax
	je	.L326
	movl	56(%r12), %ecx
	movq	1296(%r12), %rdi
	testl	%ecx, %ecx
	jne	.L327
	movl	6176(%r12), %edx
	testl	%edx, %edx
	jne	.L337
.L327:
	call	SSL_SESSION_get0_cipher@PLT
.L328:
	movl	64(%rax), %edi
	call	ssl_md@PLT
	xorl	%edx, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	movq	%rax, %r15
	call	EVP_DigestInit_ex@PLT
	testl	%eax, %eax
	jle	.L326
	movq	16(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	jle	.L326
	leaq	-192(%rbp), %r13
	leaq	-264(%rbp), %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	EVP_DigestFinal_ex@PLT
	testl	%eax, %eax
	jle	.L326
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	EVP_DigestInit_ex@PLT
	testl	%eax, %eax
	jle	.L326
	leaq	-128(%rbp), %r9
	leaq	-260(%rbp), %rdx
	movq	%r14, %rdi
	movq	%r9, %rsi
	movq	%r9, -304(%rbp)
	call	EVP_DigestFinal_ex@PLT
	testl	%eax, %eax
	jle	.L326
	movl	-264(%rbp), %eax
	pushq	$0
	movq	%rbx, %r8
	movq	%r15, %rsi
	leaq	-256(%rbp), %r11
	movq	-304(%rbp), %r9
	movq	%r12, %rdi
	movq	-280(%rbp), %rcx
	pushq	%rax
	movl	-260(%rbp), %eax
	leaq	1020(%r12), %rdx
	pushq	%r11
	pushq	%rax
	movq	%r11, -312(%rbp)
	call	tls13_hkdf_expand
	addq	$32, %rsp
	testl	%eax, %eax
	je	.L326
	movl	-264(%rbp), %eax
	pushq	$0
	movq	%r12, %rdi
	movq	%r13, %r9
	pushq	-296(%rbp)
	movl	$8, %r8d
	movq	%r15, %rsi
	xorl	%r12d, %r12d
	pushq	-288(%rbp)
	leaq	exporterlabel.23483(%rip), %rcx
	movq	-312(%rbp), %r11
	pushq	%rax
	movq	%r11, %rdx
	call	tls13_hkdf_expand
	addq	$32, %rsp
	testl	%eax, %eax
	setne	%r12b
	jmp	.L325
	.p2align 4,,10
	.p2align 3
.L326:
	xorl	%r12d, %r12d
.L325:
	movq	%r14, %rdi
	call	EVP_MD_CTX_free@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L338
	leaq	-40(%rbp), %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L337:
	.cfi_restore_state
	movl	580(%rdi), %eax
	testl	%eax, %eax
	jne	.L327
	movq	1304(%r12), %rdi
	call	SSL_SESSION_get0_cipher@PLT
	jmp	.L328
.L338:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1015:
	.size	tls13_export_keying_material_early, .-tls13_export_keying_material_early
	.section	.rodata
	.align 8
	.type	exporterlabel.23483, @object
	.size	exporterlabel.23483, 9
exporterlabel.23483:
	.string	"exporter"
	.align 8
	.type	exporterlabel.23465, @object
	.size	exporterlabel.23465, 9
exporterlabel.23465:
	.string	"exporter"
	.align 8
	.type	application_traffic.23442, @object
	.size	application_traffic.23442, 12
application_traffic.23442:
	.string	"traffic upd"
	.align 8
	.type	exporter_master_secret.23414, @object
	.size	exporter_master_secret.23414, 11
exporter_master_secret.23414:
	.string	"exp master"
	.align 8
	.type	resumption_master_secret.23415, @object
	.size	resumption_master_secret.23415, 11
resumption_master_secret.23415:
	.string	"res master"
	.align 8
	.type	server_application_traffic.23413, @object
	.size	server_application_traffic.23413, 13
server_application_traffic.23413:
	.string	"s ap traffic"
	.align 8
	.type	server_handshake_traffic.23412, @object
	.size	server_handshake_traffic.23412, 13
server_handshake_traffic.23412:
	.string	"s hs traffic"
	.align 8
	.type	client_application_traffic.23411, @object
	.size	client_application_traffic.23411, 13
client_application_traffic.23411:
	.string	"c ap traffic"
	.align 8
	.type	client_handshake_traffic.23410, @object
	.size	client_handshake_traffic.23410, 13
client_handshake_traffic.23410:
	.string	"c hs traffic"
	.align 8
	.type	early_exporter_master_secret.23416, @object
	.size	early_exporter_master_secret.23416, 13
early_exporter_master_secret.23416:
	.string	"e exp master"
	.align 8
	.type	client_early_traffic.23409, @object
	.size	client_early_traffic.23409, 12
client_early_traffic.23409:
	.string	"c e traffic"
	.align 8
	.type	derived_secret_label.23348, @object
	.size	derived_secret_label.23348, 8
derived_secret_label.23348:
	.string	"derived"
	.align 8
	.type	finishedlabel.23334, @object
	.size	finishedlabel.23334, 9
finishedlabel.23334:
	.string	"finished"
	.type	ivlabel.23326, @object
	.size	ivlabel.23326, 3
ivlabel.23326:
	.string	"iv"
	.type	keylabel.23318, @object
	.size	keylabel.23318, 4
keylabel.23318:
	.string	"key"
	.type	label_prefix.23304, @object
	.size	label_prefix.23304, 7
label_prefix.23304:
	.string	"tls13 "
	.align 32
	.type	default_zeros, @object
	.size	default_zeros, 64
default_zeros:
	.zero	64
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
