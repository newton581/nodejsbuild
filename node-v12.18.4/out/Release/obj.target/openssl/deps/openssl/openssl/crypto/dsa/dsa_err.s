	.file	"dsa_err.c"
	.text
	.p2align 4
	.globl	ERR_load_DSA_strings
	.type	ERR_load_DSA_strings, @function
ERR_load_DSA_strings:
.LFB203:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$168181760, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_func_error_string@PLT
	testq	%rax, %rax
	je	.L5
	movl	$1, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	.cfi_restore_state
	leaq	DSA_str_functs(%rip), %rdi
	call	ERR_load_strings_const@PLT
	leaq	DSA_str_reasons(%rip), %rdi
	call	ERR_load_strings_const@PLT
	movl	$1, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE203:
	.size	ERR_load_DSA_strings, .-ERR_load_DSA_strings
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"bad q value"
.LC1:
	.string	"bn decode error"
.LC2:
	.string	"bn error"
.LC3:
	.string	"decode error"
.LC4:
	.string	"invalid digest type"
.LC5:
	.string	"invalid parameters"
.LC6:
	.string	"missing parameters"
.LC7:
	.string	"missing private key"
.LC8:
	.string	"modulus too large"
.LC9:
	.string	"no parameters set"
.LC10:
	.string	"parameter encoding error"
.LC11:
	.string	"q not prime"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC12:
	.string	"seed_len is less than the length of q"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	DSA_str_reasons, @object
	.size	DSA_str_reasons, 224
DSA_str_reasons:
	.quad	167772262
	.quad	.LC0
	.quad	167772268
	.quad	.LC1
	.quad	167772269
	.quad	.LC2
	.quad	167772264
	.quad	.LC3
	.quad	167772266
	.quad	.LC4
	.quad	167772272
	.quad	.LC5
	.quad	167772261
	.quad	.LC6
	.quad	167772271
	.quad	.LC7
	.quad	167772263
	.quad	.LC8
	.quad	167772267
	.quad	.LC9
	.quad	167772265
	.quad	.LC10
	.quad	167772273
	.quad	.LC11
	.quad	167772270
	.quad	.LC12
	.quad	0
	.quad	0
	.section	.rodata.str1.1
.LC13:
	.string	"DSAparams_print"
.LC14:
	.string	"DSAparams_print_fp"
.LC15:
	.string	"dsa_builtin_paramgen"
.LC16:
	.string	"dsa_builtin_paramgen2"
.LC17:
	.string	"DSA_do_sign"
.LC18:
	.string	"DSA_do_verify"
.LC19:
	.string	"DSA_meth_dup"
.LC20:
	.string	"DSA_meth_new"
.LC21:
	.string	"DSA_meth_set1_name"
.LC22:
	.string	"DSA_new_method"
.LC23:
	.string	"dsa_param_decode"
.LC24:
	.string	"DSA_print_fp"
.LC25:
	.string	"dsa_priv_decode"
.LC26:
	.string	"dsa_priv_encode"
.LC27:
	.string	"dsa_pub_decode"
.LC28:
	.string	"dsa_pub_encode"
.LC29:
	.string	"DSA_sign"
.LC30:
	.string	"DSA_sign_setup"
.LC31:
	.string	"DSA_SIG_new"
.LC32:
	.string	"old_dsa_priv_decode"
.LC33:
	.string	"pkey_dsa_ctrl"
.LC34:
	.string	"pkey_dsa_ctrl_str"
.LC35:
	.string	"pkey_dsa_keygen"
	.section	.data.rel.ro.local
	.align 32
	.type	DSA_str_functs, @object
	.size	DSA_str_functs, 384
DSA_str_functs:
	.quad	168181760
	.quad	.LC13
	.quad	168185856
	.quad	.LC14
	.quad	168284160
	.quad	.LC15
	.quad	168288256
	.quad	.LC16
	.quad	168230912
	.quad	.LC17
	.quad	168235008
	.quad	.LC18
	.quad	168292352
	.quad	.LC19
	.quad	168296448
	.quad	.LC20
	.quad	168300544
	.quad	.LC21
	.quad	168194048
	.quad	.LC22
	.quad	168259584
	.quad	.LC23
	.quad	168202240
	.quad	.LC24
	.quad	168243200
	.quad	.LC25
	.quad	168247296
	.quad	.LC26
	.quad	168251392
	.quad	.LC27
	.quad	168255488
	.quad	.LC28
	.quad	168206336
	.quad	.LC29
	.quad	168210432
	.quad	.LC30
	.quad	168189952
	.quad	.LC31
	.quad	168271872
	.quad	.LC32
	.quad	168263680
	.quad	.LC33
	.quad	168198144
	.quad	.LC34
	.quad	168267776
	.quad	.LC35
	.quad	0
	.quad	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
