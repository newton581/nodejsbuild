	.file	"x_pkey.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/asn1/x_pkey.c"
	.text
	.p2align 4
	.globl	X509_PKEY_new
	.type	X509_PKEY_new, @function
X509_PKEY_new:
.LFB779:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$20, %edx
	movl	$80, %edi
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	CRYPTO_zalloc@PLT
	testq	%rax, %rax
	je	.L2
	movq	%rax, %r12
	call	X509_ALGOR_new@PLT
	movq	%rax, 8(%r12)
	call	ASN1_OCTET_STRING_new@PLT
	movq	8(%r12), %rdi
	movq	%rax, 16(%r12)
	testq	%rax, %rax
	je	.L3
	testq	%rdi, %rdi
	je	.L3
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	.cfi_restore_state
	call	X509_ALGOR_free@PLT
	movq	16(%r12), %rdi
	call	ASN1_OCTET_STRING_free@PLT
	movq	24(%r12), %rdi
	call	EVP_PKEY_free@PLT
	movl	48(%r12), %eax
	testl	%eax, %eax
	jne	.L12
.L6:
	movl	$46, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
.L2:
	movl	$32, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r12d, %r12d
	movl	$173, %esi
	movl	$13, %edi
	call	ERR_put_error@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	.cfi_restore_state
	movq	40(%r12), %rdi
	movl	$45, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	jmp	.L6
	.cfi_endproc
.LFE779:
	.size	X509_PKEY_new, .-X509_PKEY_new
	.p2align 4
	.globl	X509_PKEY_free
	.type	X509_PKEY_free, @function
X509_PKEY_free:
.LFB780:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L13
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	8(%rdi), %rdi
	call	X509_ALGOR_free@PLT
	movq	16(%r12), %rdi
	call	ASN1_OCTET_STRING_free@PLT
	movq	24(%r12), %rdi
	call	EVP_PKEY_free@PLT
	movl	48(%r12), %eax
	testl	%eax, %eax
	jne	.L19
.L15:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$46, %edx
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	leaq	.LC0(%rip), %rsi
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_free@PLT
	.p2align 4,,10
	.p2align 3
.L19:
	.cfi_restore_state
	movq	40(%r12), %rdi
	movl	$45, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L13:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE780:
	.size	X509_PKEY_free, .-X509_PKEY_free
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
