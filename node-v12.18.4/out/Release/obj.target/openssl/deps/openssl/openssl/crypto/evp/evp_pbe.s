	.file	"evp_pbe.c"
	.text
	.p2align 4
	.type	pbe2_cmp_BSEARCH_CMP_FN, @function
pbe2_cmp_BSEARCH_CMP_FN:
.LFB829:
	.cfi_startproc
	endbr64
	movl	(%rdi), %eax
	subl	(%rsi), %eax
	jne	.L1
	movl	4(%rdi), %eax
	subl	4(%rsi), %eax
.L1:
	ret
	.cfi_endproc
.LFE829:
	.size	pbe2_cmp_BSEARCH_CMP_FN, .-pbe2_cmp_BSEARCH_CMP_FN
	.p2align 4
	.type	pbe_cmp, @function
pbe_cmp:
.LFB831:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rcx
	movq	(%rsi), %rdx
	movl	(%rcx), %eax
	subl	(%rdx), %eax
	jne	.L4
	movl	4(%rcx), %eax
	subl	4(%rdx), %eax
.L4:
	ret
	.cfi_endproc
.LFE831:
	.size	pbe_cmp, .-pbe_cmp
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/evp/evp_pbe.c"
	.text
	.p2align 4
	.type	free_evp_pbe_ctl, @function
free_evp_pbe_ctl:
.LFB835:
	.cfi_startproc
	endbr64
	movl	$240, %edx
	leaq	.LC0(%rip), %rsi
	jmp	CRYPTO_free@PLT
	.cfi_endproc
.LFE835:
	.size	free_evp_pbe_ctl, .-free_evp_pbe_ctl
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"NULL"
.LC2:
	.string	"TYPE="
	.text
	.p2align 4
	.globl	EVP_PBE_CipherInit
	.type	EVP_PBE_CipherInit, @function
EVP_PBE_CipherInit:
.LFB827:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$152, %rsp
	movl	%r9d, -180(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	OBJ_obj2nid@PLT
	testl	%eax, %eax
	je	.L8
	movq	pbe_algs(%rip), %rdi
	movl	%eax, -172(%rbp)
	leaq	-176(%rbp), %r9
	movl	$0, -176(%rbp)
	testq	%rdi, %rdi
	je	.L12
	movq	%r9, %rsi
	movq	%r9, -192(%rbp)
	call	OPENSSL_sk_find@PLT
	movq	pbe_algs(%rip), %rdi
	movl	%eax, %esi
	call	OPENSSL_sk_value@PLT
	movq	-192(%rbp), %r9
	testq	%rax, %rax
	je	.L12
.L11:
	movl	8(%rax), %r9d
	movl	12(%rax), %r10d
	movq	16(%rax), %r12
	testq	%r14, %r14
	je	.L38
	cmpl	$-1, %r13d
	jne	.L13
	movq	%r14, %rdi
	movl	%r10d, -184(%rbp)
	movl	%r9d, -192(%rbp)
	call	strlen@PLT
	movl	-192(%rbp), %r9d
	movl	-184(%rbp), %r10d
	movl	%eax, %r13d
.L13:
	xorl	%r8d, %r8d
	cmpl	$-1, %r9d
	je	.L18
	movl	%r9d, %edi
	movl	%r10d, -192(%rbp)
	call	OBJ_nid2sn@PLT
	movq	%rax, %rdi
	call	EVP_get_cipherbyname@PLT
	movl	-192(%rbp), %r10d
	testq	%rax, %rax
	movq	%rax, %r8
	je	.L39
.L18:
	xorl	%r9d, %r9d
	cmpl	$-1, %r10d
	je	.L19
	movl	%r10d, %edi
	movq	%r8, -192(%rbp)
	call	OBJ_nid2sn@PLT
	movq	%rax, %rdi
	call	EVP_get_digestbyname@PLT
	movq	-192(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %r9
	je	.L40
.L19:
	movl	-180(%rbp), %eax
	subq	$8, %rsp
	movq	%rbx, %rcx
	movl	%r13d, %edx
	movq	%r14, %rsi
	movq	%r15, %rdi
	pushq	%rax
	call	*%r12
	popq	%rdx
	popq	%rcx
	testl	%eax, %eax
	je	.L41
	movl	$1, %eax
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L12:
	leaq	pbe2_cmp_BSEARCH_CMP_FN(%rip), %r8
	movl	$24, %ecx
	movl	$29, %edx
	movq	%r9, %rdi
	leaq	builtin_pbe(%rip), %rsi
	call	OBJ_bsearch_@PLT
	testq	%rax, %rax
	jne	.L11
.L8:
	movl	$95, %r8d
	movl	$121, %edx
	movl	$116, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	leaq	-144(%rbp), %r13
	call	ERR_put_error@PLT
	testq	%r12, %r12
	je	.L42
	movq	%r12, %rdx
	movl	$80, %esi
	movq	%r13, %rdi
	call	i2t_ASN1_OBJECT@PLT
.L16:
	xorl	%eax, %eax
	movq	%r13, %rdx
	movl	$2, %edi
	leaq	.LC2(%rip), %rsi
	call	ERR_add_error_data@PLT
	xorl	%eax, %eax
.L7:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L43
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L42:
	.cfi_restore_state
	movl	$80, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r13, %rdi
	call	OPENSSL_strlcpy@PLT
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L38:
	xorl	%r13d, %r13d
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L41:
	movl	$130, %r8d
	movl	$120, %edx
	movl	$116, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	movl	%eax, -180(%rbp)
	call	ERR_put_error@PLT
	movl	-180(%rbp), %eax
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L39:
	movl	$114, %r8d
	movl	$160, %edx
	movl	$116, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L40:
	movl	$124, %r8d
	movl	$161, %edx
	movl	$116, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L7
.L43:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE827:
	.size	EVP_PBE_CipherInit, .-EVP_PBE_CipherInit
	.p2align 4
	.globl	EVP_PBE_alg_add_type
	.type	EVP_PBE_alg_add_type, @function
EVP_PBE_alg_add_type:
.LFB832:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%r8, %rbx
	subq	$16, %rsp
	cmpq	$0, pbe_algs(%rip)
	movl	%edi, -20(%rbp)
	movl	%esi, -24(%rbp)
	movl	%edx, -28(%rbp)
	movl	%ecx, -32(%rbp)
	je	.L45
.L48:
	movl	$171, %edx
	leaq	.LC0(%rip), %rsi
	movl	$24, %edi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L49
	movd	-28(%rbp), %xmm1
	movd	-32(%rbp), %xmm2
	movq	%rbx, 16(%rax)
	movq	%rax, %rsi
	movd	-20(%rbp), %xmm0
	movd	-24(%rbp), %xmm3
	punpckldq	%xmm2, %xmm1
	movq	pbe_algs(%rip), %rdi
	punpckldq	%xmm3, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rax)
	call	OPENSSL_sk_push@PLT
	movl	%eax, %r8d
	movl	$1, %eax
	testl	%r8d, %r8d
	je	.L57
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L57:
	.cfi_restore_state
	movl	$181, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
.L49:
	movl	$187, %r8d
	movl	$65, %edx
	movl	$160, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$16, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L45:
	.cfi_restore_state
	leaq	pbe_cmp(%rip), %rdi
	call	OPENSSL_sk_new@PLT
	movq	%rax, pbe_algs(%rip)
	testq	%rax, %rax
	jne	.L48
	jmp	.L49
	.cfi_endproc
.LFE832:
	.size	EVP_PBE_alg_add_type, .-EVP_PBE_alg_add_type
	.p2align 4
	.globl	EVP_PBE_alg_add
	.type	EVP_PBE_alg_add, @function
EVP_PBE_alg_add:
.LFB833:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	$-1, %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%edi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$8, %rsp
	testq	%rsi, %rsi
	je	.L59
	movq	%rsi, %rdi
	call	EVP_CIPHER_nid@PLT
	movl	%eax, %r15d
.L59:
	movl	$-1, %r14d
	testq	%r12, %r12
	je	.L60
	movq	%r12, %rdi
	call	EVP_MD_type@PLT
	movl	%eax, %r14d
.L60:
	cmpq	$0, pbe_algs(%rip)
	je	.L61
.L64:
	movl	$171, %edx
	leaq	.LC0(%rip), %rsi
	movl	$24, %edi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L65
	movl	$0, (%rax)
	movq	pbe_algs(%rip), %rdi
	movq	%rax, %rsi
	movl	%r13d, 4(%rax)
	movl	%r15d, 8(%rax)
	movl	%r14d, 12(%rax)
	movq	%rbx, 16(%rax)
	call	OPENSSL_sk_push@PLT
	movl	%eax, %r8d
	movl	$1, %eax
	testl	%r8d, %r8d
	je	.L78
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L78:
	.cfi_restore_state
	movl	$181, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
.L65:
	movl	$187, %r8d
	movl	$65, %edx
	movl	$160, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L61:
	.cfi_restore_state
	leaq	pbe_cmp(%rip), %rdi
	call	OPENSSL_sk_new@PLT
	movq	%rax, pbe_algs(%rip)
	testq	%rax, %rax
	jne	.L64
	jmp	.L65
	.cfi_endproc
.LFE833:
	.size	EVP_PBE_alg_add, .-EVP_PBE_alg_add
	.p2align 4
	.globl	EVP_PBE_find
	.type	EVP_PBE_find, @function
EVP_PBE_find:
.LFB834:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%r8, %rbx
	xorl	%r8d, %r8d
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testl	%esi, %esi
	je	.L79
	movl	%edi, -64(%rbp)
	movq	pbe_algs(%rip), %rdi
	movq	%rdx, %r13
	movq	%rcx, %r12
	movl	%esi, -60(%rbp)
	leaq	-64(%rbp), %r14
	testq	%rdi, %rdi
	je	.L84
	movq	%r14, %rsi
	call	OPENSSL_sk_find@PLT
	movq	pbe_algs(%rip), %rdi
	movl	%eax, %esi
	call	OPENSSL_sk_value@PLT
	testq	%rax, %rax
	je	.L84
.L83:
	testq	%r13, %r13
	je	.L85
	movl	8(%rax), %edx
	movl	%edx, 0(%r13)
.L85:
	testq	%r12, %r12
	je	.L86
	movl	12(%rax), %edx
	movl	%edx, (%r12)
.L86:
	movl	$1, %r8d
	testq	%rbx, %rbx
	je	.L79
	movq	16(%rax), %rax
	movq	%rax, (%rbx)
.L79:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L104
	addq	$32, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L84:
	.cfi_restore_state
	leaq	pbe2_cmp_BSEARCH_CMP_FN(%rip), %r8
	movl	$24, %ecx
	movl	$29, %edx
	movq	%r14, %rdi
	leaq	builtin_pbe(%rip), %rsi
	call	OBJ_bsearch_@PLT
	xorl	%r8d, %r8d
	testq	%rax, %rax
	jne	.L83
	jmp	.L79
.L104:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE834:
	.size	EVP_PBE_find, .-EVP_PBE_find
	.p2align 4
	.globl	EVP_PBE_cleanup
	.type	EVP_PBE_cleanup, @function
EVP_PBE_cleanup:
.LFB836:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	pbe_algs(%rip), %rdi
	leaq	free_evp_pbe_ctl(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	OPENSSL_sk_pop_free@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	movq	$0, pbe_algs(%rip)
	ret
	.cfi_endproc
.LFE836:
	.size	EVP_PBE_cleanup, .-EVP_PBE_cleanup
	.p2align 4
	.globl	EVP_PBE_get
	.type	EVP_PBE_get, @function
EVP_PBE_get:
.LFB837:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpq	$28, %rdx
	ja	.L107
	leaq	(%rdx,%rdx,2), %rdx
	leaq	builtin_pbe(%rip), %rax
	leaq	(%rax,%rdx,8), %rdx
	testq	%rdi, %rdi
	je	.L109
	movl	(%rdx), %eax
	movl	%eax, (%rdi)
.L109:
	movl	$1, %eax
	testq	%rsi, %rsi
	je	.L107
	movl	4(%rdx), %edx
	movl	%edx, (%rsi)
.L107:
	ret
	.cfi_endproc
.LFE837:
	.size	EVP_PBE_get, .-EVP_PBE_get
	.section	.data.rel.ro,"aw"
	.align 32
	.type	builtin_pbe, @object
	.size	builtin_pbe, 696
builtin_pbe:
	.long	0
	.long	9
	.long	31
	.long	3
	.quad	PKCS5_PBE_keyivgen
	.long	0
	.long	10
	.long	31
	.long	4
	.quad	PKCS5_PBE_keyivgen
	.long	0
	.long	68
	.long	166
	.long	64
	.quad	PKCS5_PBE_keyivgen
	.long	0
	.long	69
	.long	-1
	.long	-1
	.quad	PKCS5_v2_PBKDF2_keyivgen
	.long	0
	.long	144
	.long	5
	.long	64
	.quad	PKCS12_PBE_keyivgen
	.long	0
	.long	145
	.long	97
	.long	64
	.quad	PKCS12_PBE_keyivgen
	.long	0
	.long	146
	.long	44
	.long	64
	.quad	PKCS12_PBE_keyivgen
	.long	0
	.long	147
	.long	43
	.long	64
	.quad	PKCS12_PBE_keyivgen
	.long	0
	.long	148
	.long	37
	.long	64
	.quad	PKCS12_PBE_keyivgen
	.long	0
	.long	149
	.long	98
	.long	64
	.quad	PKCS12_PBE_keyivgen
	.long	0
	.long	161
	.long	-1
	.long	-1
	.quad	PKCS5_v2_PBE_keyivgen
	.long	0
	.long	168
	.long	166
	.long	3
	.quad	PKCS5_PBE_keyivgen
	.long	0
	.long	169
	.long	166
	.long	4
	.quad	PKCS5_PBE_keyivgen
	.long	0
	.long	170
	.long	31
	.long	64
	.quad	PKCS5_PBE_keyivgen
	.long	1
	.long	163
	.long	-1
	.long	64
	.quad	0
	.long	1
	.long	780
	.long	-1
	.long	4
	.quad	0
	.long	1
	.long	781
	.long	-1
	.long	64
	.quad	0
	.long	1
	.long	797
	.long	-1
	.long	4
	.quad	0
	.long	1
	.long	798
	.long	-1
	.long	675
	.quad	0
	.long	1
	.long	799
	.long	-1
	.long	672
	.quad	0
	.long	1
	.long	800
	.long	-1
	.long	673
	.quad	0
	.long	1
	.long	801
	.long	-1
	.long	674
	.quad	0
	.long	1
	.long	810
	.long	-1
	.long	809
	.quad	0
	.long	1
	.long	988
	.long	-1
	.long	982
	.quad	0
	.long	1
	.long	989
	.long	-1
	.long	983
	.quad	0
	.long	1
	.long	1193
	.long	-1
	.long	1094
	.quad	0
	.long	1
	.long	1194
	.long	-1
	.long	1095
	.quad	0
	.long	2
	.long	69
	.long	-1
	.long	-1
	.quad	PKCS5_v2_PBKDF2_keyivgen
	.long	2
	.long	973
	.long	-1
	.long	-1
	.quad	PKCS5_v2_scrypt_keyivgen
	.local	pbe_algs
	.comm	pbe_algs,8,8
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
