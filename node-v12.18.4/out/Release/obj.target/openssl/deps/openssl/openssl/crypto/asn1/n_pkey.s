	.file	"n_pkey.c"
	.text
	.p2align 4
	.globl	d2i_NETSCAPE_ENCRYPTED_PKEY
	.type	d2i_NETSCAPE_ENCRYPTED_PKEY, @function
d2i_NETSCAPE_ENCRYPTED_PKEY:
.LFB803:
	.cfi_startproc
	endbr64
	leaq	NETSCAPE_ENCRYPTED_PKEY_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE803:
	.size	d2i_NETSCAPE_ENCRYPTED_PKEY, .-d2i_NETSCAPE_ENCRYPTED_PKEY
	.p2align 4
	.globl	i2d_NETSCAPE_ENCRYPTED_PKEY
	.type	i2d_NETSCAPE_ENCRYPTED_PKEY, @function
i2d_NETSCAPE_ENCRYPTED_PKEY:
.LFB804:
	.cfi_startproc
	endbr64
	leaq	NETSCAPE_ENCRYPTED_PKEY_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE804:
	.size	i2d_NETSCAPE_ENCRYPTED_PKEY, .-i2d_NETSCAPE_ENCRYPTED_PKEY
	.p2align 4
	.globl	NETSCAPE_ENCRYPTED_PKEY_new
	.type	NETSCAPE_ENCRYPTED_PKEY_new, @function
NETSCAPE_ENCRYPTED_PKEY_new:
.LFB805:
	.cfi_startproc
	endbr64
	leaq	NETSCAPE_ENCRYPTED_PKEY_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE805:
	.size	NETSCAPE_ENCRYPTED_PKEY_new, .-NETSCAPE_ENCRYPTED_PKEY_new
	.p2align 4
	.globl	NETSCAPE_ENCRYPTED_PKEY_free
	.type	NETSCAPE_ENCRYPTED_PKEY_free, @function
NETSCAPE_ENCRYPTED_PKEY_free:
.LFB806:
	.cfi_startproc
	endbr64
	leaq	NETSCAPE_ENCRYPTED_PKEY_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE806:
	.size	NETSCAPE_ENCRYPTED_PKEY_free, .-NETSCAPE_ENCRYPTED_PKEY_free
	.p2align 4
	.globl	d2i_NETSCAPE_PKEY
	.type	d2i_NETSCAPE_PKEY, @function
d2i_NETSCAPE_PKEY:
.LFB807:
	.cfi_startproc
	endbr64
	leaq	NETSCAPE_PKEY_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE807:
	.size	d2i_NETSCAPE_PKEY, .-d2i_NETSCAPE_PKEY
	.p2align 4
	.globl	i2d_NETSCAPE_PKEY
	.type	i2d_NETSCAPE_PKEY, @function
i2d_NETSCAPE_PKEY:
.LFB808:
	.cfi_startproc
	endbr64
	leaq	NETSCAPE_PKEY_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE808:
	.size	i2d_NETSCAPE_PKEY, .-i2d_NETSCAPE_PKEY
	.p2align 4
	.globl	NETSCAPE_PKEY_new
	.type	NETSCAPE_PKEY_new, @function
NETSCAPE_PKEY_new:
.LFB809:
	.cfi_startproc
	endbr64
	leaq	NETSCAPE_PKEY_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE809:
	.size	NETSCAPE_PKEY_new, .-NETSCAPE_PKEY_new
	.p2align 4
	.globl	NETSCAPE_PKEY_free
	.type	NETSCAPE_PKEY_free, @function
NETSCAPE_PKEY_free:
.LFB810:
	.cfi_startproc
	endbr64
	leaq	NETSCAPE_PKEY_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE810:
	.size	NETSCAPE_PKEY_free, .-NETSCAPE_PKEY_free
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"NETSCAPE_PKEY"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	NETSCAPE_PKEY_it, @object
	.size	NETSCAPE_PKEY_it, 56
NETSCAPE_PKEY_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	NETSCAPE_PKEY_seq_tt
	.quad	3
	.quad	0
	.quad	24
	.quad	.LC0
	.section	.rodata.str1.1
.LC1:
	.string	"version"
.LC2:
	.string	"algor"
.LC3:
	.string	"private_key"
	.section	.data.rel.ro,"aw"
	.align 32
	.type	NETSCAPE_PKEY_seq_tt, @object
	.size	NETSCAPE_PKEY_seq_tt, 120
NETSCAPE_PKEY_seq_tt:
	.quad	4096
	.quad	0
	.quad	0
	.quad	.LC1
	.quad	INT32_it
	.quad	0
	.quad	0
	.quad	8
	.quad	.LC2
	.quad	X509_ALGOR_it
	.quad	0
	.quad	0
	.quad	16
	.quad	.LC3
	.quad	ASN1_OCTET_STRING_it
	.section	.rodata.str1.1
.LC4:
	.string	"NETSCAPE_ENCRYPTED_PKEY"
	.section	.data.rel.ro.local
	.align 32
	.type	NETSCAPE_ENCRYPTED_PKEY_it, @object
	.size	NETSCAPE_ENCRYPTED_PKEY_it, 56
NETSCAPE_ENCRYPTED_PKEY_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	NETSCAPE_ENCRYPTED_PKEY_seq_tt
	.quad	2
	.quad	NETSCAPE_ENCRYPTED_PKEY_aux
	.quad	16
	.quad	.LC4
	.section	.rodata.str1.1
.LC5:
	.string	"os"
.LC6:
	.string	"enckey"
	.section	.data.rel.ro
	.align 32
	.type	NETSCAPE_ENCRYPTED_PKEY_seq_tt, @object
	.size	NETSCAPE_ENCRYPTED_PKEY_seq_tt, 80
NETSCAPE_ENCRYPTED_PKEY_seq_tt:
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC5
	.quad	ASN1_OCTET_STRING_it
	.quad	0
	.quad	0
	.quad	8
	.quad	.LC6
	.quad	X509_SIG_it
	.section	.rodata
	.align 32
	.type	NETSCAPE_ENCRYPTED_PKEY_aux, @object
	.size	NETSCAPE_ENCRYPTED_PKEY_aux, 40
NETSCAPE_ENCRYPTED_PKEY_aux:
	.quad	0
	.long	4
	.long	0
	.long	0
	.zero	4
	.quad	0
	.long	0
	.zero	4
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
