	.file	"b_sock2.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/bio/b_sock2.c"
	.text
	.p2align 4
	.globl	BIO_socket
	.type	BIO_socket, @function
BIO_socket:
.LFB267:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movl	%edi, %r12d
	subq	$8, %rsp
	call	BIO_sock_init@PLT
	cmpl	$1, %eax
	jne	.L3
	movl	%r12d, %edi
	movl	%r14d, %edx
	movl	%r13d, %esi
	call	socket@PLT
	movl	%eax, %r12d
	cmpl	$-1, %eax
	je	.L6
.L1:
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	.cfi_restore_state
	movl	$-1, %r12d
	jmp	.L1
.L6:
	call	__errno_location@PLT
	movl	$49, %r8d
	movl	$4, %esi
	leaq	.LC0(%rip), %rcx
	movl	(%rax), %edx
	movl	$2, %edi
	call	ERR_put_error@PLT
	movl	$50, %r8d
	movl	$118, %edx
	leaq	.LC0(%rip), %rcx
	movl	$140, %esi
	movl	$32, %edi
	call	ERR_put_error@PLT
	jmp	.L1
	.cfi_endproc
.LFE267:
	.size	BIO_socket, .-BIO_socket
	.p2align 4
	.globl	BIO_connect
	.type	BIO_connect, @function
BIO_connect:
.LFB268:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	$1, -44(%rbp)
	cmpl	$-1, %edi
	je	.L30
	movq	%rsi, %r13
	movl	%edx, %esi
	movl	%edi, %r12d
	movl	%edx, %ebx
	shrl	$3, %esi
	andl	$1, %esi
	call	BIO_socket_nbio@PLT
	testl	%eax, %eax
	je	.L29
	testb	$4, %bl
	jne	.L31
.L11:
	andl	$16, %ebx
	je	.L12
	leaq	-44(%rbp), %rcx
	movl	$4, %r8d
	movl	$1, %edx
	movl	%r12d, %edi
	movl	$6, %esi
	call	setsockopt@PLT
	testl	%eax, %eax
	jne	.L32
.L12:
	movq	%r13, %rdi
	call	BIO_ADDR_sockaddr_size@PLT
	movq	%r13, %rdi
	movl	%eax, %r14d
	call	BIO_ADDR_sockaddr@PLT
	movl	%r12d, %edi
	movl	%r14d, %edx
	movl	$1, %r12d
	movq	%rax, %rsi
	call	connect@PLT
	cmpl	$-1, %eax
	je	.L33
.L7:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L34
	addq	$16, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L31:
	.cfi_restore_state
	leaq	-44(%rbp), %rcx
	movl	$4, %r8d
	movl	$9, %edx
	movl	%r12d, %edi
	movl	$1, %esi
	call	setsockopt@PLT
	testl	%eax, %eax
	je	.L11
	call	__errno_location@PLT
	movl	$92, %r8d
	movl	$14, %esi
	leaq	.LC0(%rip), %rcx
	movl	(%rax), %edx
	movl	$2, %edi
	call	ERR_put_error@PLT
	movl	$93, %r8d
	movl	$137, %edx
	leaq	.LC0(%rip), %rcx
	movl	$138, %esi
	movl	$32, %edi
	call	ERR_put_error@PLT
	.p2align 4,,10
	.p2align 3
.L29:
	xorl	%r12d, %r12d
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L30:
	movl	$82, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$135, %edx
	xorl	%r12d, %r12d
	movl	$138, %esi
	movl	$32, %edi
	call	ERR_put_error@PLT
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L33:
	movl	$-1, %edi
	call	BIO_sock_should_retry@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	jne	.L29
	call	__errno_location@PLT
	movl	$110, %r8d
	movl	$2, %esi
	leaq	.LC0(%rip), %rcx
	movl	(%rax), %edx
	movl	$2, %edi
	call	ERR_put_error@PLT
	movl	$111, %r8d
	movl	$103, %edx
	leaq	.LC0(%rip), %rcx
	movl	$138, %esi
	movl	$32, %edi
	call	ERR_put_error@PLT
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L32:
	call	__errno_location@PLT
	movl	$101, %r8d
	movl	$14, %esi
	xorl	%r12d, %r12d
	movl	(%rax), %edx
	leaq	.LC0(%rip), %rcx
	movl	$2, %edi
	call	ERR_put_error@PLT
	movl	$102, %r8d
	movl	$138, %edx
	leaq	.LC0(%rip), %rcx
	movl	$138, %esi
	movl	$32, %edi
	call	ERR_put_error@PLT
	jmp	.L7
.L34:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE268:
	.size	BIO_connect, .-BIO_connect
	.p2align 4
	.globl	BIO_bind
	.type	BIO_bind, @function
BIO_bind:
.LFB269:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	subq	$24, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	$1, -44(%rbp)
	cmpl	$-1, %edi
	je	.L49
	andl	$1, %edx
	movl	%edi, %r12d
	movq	%rsi, %r13
	jne	.L50
.L38:
	movq	%r13, %rdi
	call	BIO_ADDR_sockaddr_size@PLT
	movq	%r13, %rdi
	movl	%eax, %r14d
	call	BIO_ADDR_sockaddr@PLT
	movl	%r14d, %edx
	movl	%r12d, %edi
	movq	%rax, %rsi
	call	bind@PLT
	movl	%eax, %r8d
	movl	$1, %eax
	testl	%r8d, %r8d
	jne	.L51
.L35:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L52
	addq	$24, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L50:
	.cfi_restore_state
	leaq	-44(%rbp), %rcx
	movl	$4, %r8d
	movl	$2, %edx
	movl	$1, %esi
	call	setsockopt@PLT
	testl	%eax, %eax
	je	.L38
	call	__errno_location@PLT
	movl	$153, %r8d
	movl	$14, %esi
	leaq	.LC0(%rip), %rcx
	movl	(%rax), %edx
	movl	$2, %edi
	call	ERR_put_error@PLT
	movl	$154, %r8d
	movl	$139, %edx
	leaq	.LC0(%rip), %rcx
	movl	$147, %esi
	movl	$32, %edi
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L51:
	call	__errno_location@PLT
	movl	$161, %r8d
	movl	$6, %esi
	leaq	.LC0(%rip), %rcx
	movl	(%rax), %edx
	movl	$2, %edi
	call	ERR_put_error@PLT
	movl	$162, %r8d
	movl	$117, %edx
	leaq	.LC0(%rip), %rcx
	movl	$147, %esi
	movl	$32, %edi
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L49:
	movl	$141, %r8d
	movl	$135, %edx
	movl	$147, %esi
	movl	$32, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L35
.L52:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE269:
	.size	BIO_bind, .-BIO_bind
	.p2align 4
	.globl	BIO_listen
	.type	BIO_listen, @function
BIO_listen:
.LFB270:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	subq	$32, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	$1, -52(%rbp)
	movl	$4, -44(%rbp)
	cmpl	$-1, %edi
	je	.L85
	movq	%rsi, %r14
	movl	%edx, %r15d
	leaq	-48(%rbp), %rcx
	movl	$3, %edx
	leaq	-44(%rbp), %r8
	movl	$1, %esi
	movl	%edi, %r12d
	call	getsockopt@PLT
	movl	%eax, %r13d
	testl	%eax, %eax
	jne	.L56
	cmpl	$4, -44(%rbp)
	je	.L57
.L56:
	call	__errno_location@PLT
	movl	$220, %r8d
	movl	$15, %esi
	xorl	%r13d, %r13d
	movl	(%rax), %edx
	leaq	.LC0(%rip), %rcx
	movl	$2, %edi
	call	ERR_put_error@PLT
	movl	$221, %r8d
	movl	$134, %edx
	leaq	.LC0(%rip), %rcx
	movl	$139, %esi
	movl	$32, %edi
	call	ERR_put_error@PLT
.L53:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L86
	addq	$32, %rsp
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L57:
	.cfi_restore_state
	movl	%r15d, %esi
	movl	%r12d, %edi
	shrl	$3, %esi
	andl	$1, %esi
	call	BIO_socket_nbio@PLT
	testl	%eax, %eax
	je	.L53
	testb	$4, %r15b
	jne	.L87
.L59:
	testb	$16, %r15b
	je	.L60
	leaq	-52(%rbp), %rcx
	movl	$4, %r8d
	movl	$1, %edx
	movl	%r12d, %edi
	movl	$6, %esi
	call	setsockopt@PLT
	testl	%eax, %eax
	jne	.L88
.L60:
	movq	%r14, %rdi
	call	BIO_ADDR_family@PLT
	cmpl	$10, %eax
	je	.L89
.L61:
	movl	%r15d, %edx
	movq	%r14, %rsi
	movl	%r12d, %edi
	call	BIO_bind
	testl	%eax, %eax
	je	.L53
	cmpl	$2, -48(%rbp)
	jne	.L63
.L64:
	movl	$1, %r13d
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L85:
	movl	$213, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$135, %edx
	xorl	%r13d, %r13d
	movl	$139, %esi
	movl	$32, %edi
	call	ERR_put_error@PLT
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L63:
	movl	$4096, %esi
	movl	%r12d, %edi
	call	listen@PLT
	cmpl	$-1, %eax
	jne	.L64
	call	__errno_location@PLT
	movl	$266, %r8d
	movl	$7, %esi
	leaq	.LC0(%rip), %rcx
	movl	(%rax), %edx
	movl	$2, %edi
	call	ERR_put_error@PLT
	movl	$267, %r8d
	movl	$119, %edx
	leaq	.LC0(%rip), %rcx
	movl	$139, %esi
	movl	$32, %edi
	call	ERR_put_error@PLT
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L87:
	leaq	-52(%rbp), %rcx
	movl	$4, %r8d
	movl	$9, %edx
	movl	%r12d, %edi
	movl	$1, %esi
	call	setsockopt@PLT
	testl	%eax, %eax
	je	.L59
	call	__errno_location@PLT
	movl	$231, %r8d
	movl	$14, %esi
	leaq	.LC0(%rip), %rcx
	movl	(%rax), %edx
	movl	$2, %edi
	call	ERR_put_error@PLT
	movl	$232, %r8d
	movl	$137, %edx
	leaq	.LC0(%rip), %rcx
	movl	$139, %esi
	movl	$32, %edi
	call	ERR_put_error@PLT
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L89:
	movl	%r15d, %eax
	leaq	-52(%rbp), %rcx
	movl	$4, %r8d
	movl	%r12d, %edi
	sarl	%eax
	movl	$26, %edx
	movl	$41, %esi
	andl	$1, %eax
	movl	%eax, -52(%rbp)
	call	setsockopt@PLT
	testl	%eax, %eax
	je	.L61
	call	__errno_location@PLT
	movl	$255, %r8d
	movl	$14, %esi
	leaq	.LC0(%rip), %rcx
	movl	(%rax), %edx
	movl	$2, %edi
	call	ERR_put_error@PLT
	movl	$256, %r8d
	movl	$136, %edx
	leaq	.LC0(%rip), %rcx
	movl	$139, %esi
	movl	$32, %edi
	call	ERR_put_error@PLT
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L88:
	call	__errno_location@PLT
	movl	$240, %r8d
	movl	$14, %esi
	leaq	.LC0(%rip), %rcx
	movl	(%rax), %edx
	movl	$2, %edi
	call	ERR_put_error@PLT
	movl	$241, %r8d
	movl	$138, %edx
	leaq	.LC0(%rip), %rcx
	movl	$139, %esi
	movl	$32, %edi
	call	ERR_put_error@PLT
	jmp	.L53
.L86:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE270:
	.size	BIO_listen, .-BIO_listen
	.p2align 4
	.globl	BIO_accept_ex
	.type	BIO_accept_ex, @function
BIO_accept_ex:
.LFB271:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%edi, %r12d
	movq	%rsi, %rdi
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%edx, %ebx
	subq	$144, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	leaq	-144(%rbp), %rax
	movl	$112, -148(%rbp)
	cmove	%rax, %rdi
	call	BIO_ADDR_sockaddr_noconst@PLT
	movl	%r12d, %edi
	leaq	-148(%rbp), %rdx
	movq	%rax, %rsi
	call	accept@PLT
	movl	%eax, %r12d
	cmpl	$-1, %eax
	je	.L97
	shrl	$3, %ebx
	movl	%eax, %edi
	movl	%ebx, %esi
	andl	$1, %esi
	call	BIO_socket_nbio@PLT
	testl	%eax, %eax
	je	.L98
.L90:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L99
	addq	$144, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L97:
	.cfi_restore_state
	movl	$-1, %edi
	call	BIO_sock_should_retry@PLT
	testl	%eax, %eax
	jne	.L90
	call	__errno_location@PLT
	movl	$293, %r8d
	movl	$8, %esi
	leaq	.LC0(%rip), %rcx
	movl	(%rax), %edx
	movl	$2, %edi
	call	ERR_put_error@PLT
	movl	$294, %r8d
	movl	$100, %edx
	leaq	.LC0(%rip), %rcx
	movl	$137, %esi
	movl	$32, %edi
	call	ERR_put_error@PLT
	jmp	.L90
.L98:
	movl	%r12d, %edi
	movl	$-1, %r12d
	call	close@PLT
	jmp	.L90
.L99:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE271:
	.size	BIO_accept_ex, .-BIO_accept_ex
	.p2align 4
	.globl	BIO_closesocket
	.type	BIO_closesocket, @function
BIO_closesocket:
.LFB272:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	close@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	notl	%eax
	shrl	$31, %eax
	ret
	.cfi_endproc
.LFE272:
	.size	BIO_closesocket, .-BIO_closesocket
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
