	.file	"x509_ext.c"
	.text
	.p2align 4
	.globl	X509_CRL_get_ext_count
	.type	X509_CRL_get_ext_count, @function
X509_CRL_get_ext_count:
.LFB1298:
	.cfi_startproc
	endbr64
	movq	56(%rdi), %rdi
	jmp	X509v3_get_ext_count@PLT
	.cfi_endproc
.LFE1298:
	.size	X509_CRL_get_ext_count, .-X509_CRL_get_ext_count
	.p2align 4
	.globl	X509_CRL_get_ext_by_NID
	.type	X509_CRL_get_ext_by_NID, @function
X509_CRL_get_ext_by_NID:
.LFB1299:
	.cfi_startproc
	endbr64
	movq	56(%rdi), %rdi
	jmp	X509v3_get_ext_by_NID@PLT
	.cfi_endproc
.LFE1299:
	.size	X509_CRL_get_ext_by_NID, .-X509_CRL_get_ext_by_NID
	.p2align 4
	.globl	X509_CRL_get_ext_by_OBJ
	.type	X509_CRL_get_ext_by_OBJ, @function
X509_CRL_get_ext_by_OBJ:
.LFB1300:
	.cfi_startproc
	endbr64
	movq	56(%rdi), %rdi
	jmp	X509v3_get_ext_by_OBJ@PLT
	.cfi_endproc
.LFE1300:
	.size	X509_CRL_get_ext_by_OBJ, .-X509_CRL_get_ext_by_OBJ
	.p2align 4
	.globl	X509_CRL_get_ext_by_critical
	.type	X509_CRL_get_ext_by_critical, @function
X509_CRL_get_ext_by_critical:
.LFB1301:
	.cfi_startproc
	endbr64
	movq	56(%rdi), %rdi
	jmp	X509v3_get_ext_by_critical@PLT
	.cfi_endproc
.LFE1301:
	.size	X509_CRL_get_ext_by_critical, .-X509_CRL_get_ext_by_critical
	.p2align 4
	.globl	X509_CRL_get_ext
	.type	X509_CRL_get_ext, @function
X509_CRL_get_ext:
.LFB1302:
	.cfi_startproc
	endbr64
	movq	56(%rdi), %rdi
	jmp	X509v3_get_ext@PLT
	.cfi_endproc
.LFE1302:
	.size	X509_CRL_get_ext, .-X509_CRL_get_ext
	.p2align 4
	.globl	X509_CRL_delete_ext
	.type	X509_CRL_delete_ext, @function
X509_CRL_delete_ext:
.LFB1303:
	.cfi_startproc
	endbr64
	movq	56(%rdi), %rdi
	jmp	X509v3_delete_ext@PLT
	.cfi_endproc
.LFE1303:
	.size	X509_CRL_delete_ext, .-X509_CRL_delete_ext
	.p2align 4
	.globl	X509_CRL_get_ext_d2i
	.type	X509_CRL_get_ext_d2i, @function
X509_CRL_get_ext_d2i:
.LFB1304:
	.cfi_startproc
	endbr64
	movq	56(%rdi), %rdi
	jmp	X509V3_get_d2i@PLT
	.cfi_endproc
.LFE1304:
	.size	X509_CRL_get_ext_d2i, .-X509_CRL_get_ext_d2i
	.p2align 4
	.globl	X509_CRL_add1_ext_i2d
	.type	X509_CRL_add1_ext_i2d, @function
X509_CRL_add1_ext_i2d:
.LFB1305:
	.cfi_startproc
	endbr64
	addq	$56, %rdi
	jmp	X509V3_add1_i2d@PLT
	.cfi_endproc
.LFE1305:
	.size	X509_CRL_add1_ext_i2d, .-X509_CRL_add1_ext_i2d
	.p2align 4
	.globl	X509_CRL_add_ext
	.type	X509_CRL_add_ext, @function
X509_CRL_add_ext:
.LFB1306:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$56, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	X509v3_add_ext@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	testq	%rax, %rax
	setne	%al
	movzbl	%al, %eax
	ret
	.cfi_endproc
.LFE1306:
	.size	X509_CRL_add_ext, .-X509_CRL_add_ext
	.p2align 4
	.globl	X509_get_ext_count
	.type	X509_get_ext_count, @function
X509_get_ext_count:
.LFB1307:
	.cfi_startproc
	endbr64
	movq	104(%rdi), %rdi
	jmp	X509v3_get_ext_count@PLT
	.cfi_endproc
.LFE1307:
	.size	X509_get_ext_count, .-X509_get_ext_count
	.p2align 4
	.globl	X509_get_ext_by_NID
	.type	X509_get_ext_by_NID, @function
X509_get_ext_by_NID:
.LFB1308:
	.cfi_startproc
	endbr64
	movq	104(%rdi), %rdi
	jmp	X509v3_get_ext_by_NID@PLT
	.cfi_endproc
.LFE1308:
	.size	X509_get_ext_by_NID, .-X509_get_ext_by_NID
	.p2align 4
	.globl	X509_get_ext_by_OBJ
	.type	X509_get_ext_by_OBJ, @function
X509_get_ext_by_OBJ:
.LFB1309:
	.cfi_startproc
	endbr64
	movq	104(%rdi), %rdi
	jmp	X509v3_get_ext_by_OBJ@PLT
	.cfi_endproc
.LFE1309:
	.size	X509_get_ext_by_OBJ, .-X509_get_ext_by_OBJ
	.p2align 4
	.globl	X509_get_ext_by_critical
	.type	X509_get_ext_by_critical, @function
X509_get_ext_by_critical:
.LFB1310:
	.cfi_startproc
	endbr64
	movq	104(%rdi), %rdi
	jmp	X509v3_get_ext_by_critical@PLT
	.cfi_endproc
.LFE1310:
	.size	X509_get_ext_by_critical, .-X509_get_ext_by_critical
	.p2align 4
	.globl	X509_get_ext
	.type	X509_get_ext, @function
X509_get_ext:
.LFB1311:
	.cfi_startproc
	endbr64
	movq	104(%rdi), %rdi
	jmp	X509v3_get_ext@PLT
	.cfi_endproc
.LFE1311:
	.size	X509_get_ext, .-X509_get_ext
	.p2align 4
	.globl	X509_delete_ext
	.type	X509_delete_ext, @function
X509_delete_ext:
.LFB1312:
	.cfi_startproc
	endbr64
	movq	104(%rdi), %rdi
	jmp	X509v3_delete_ext@PLT
	.cfi_endproc
.LFE1312:
	.size	X509_delete_ext, .-X509_delete_ext
	.p2align 4
	.globl	X509_add_ext
	.type	X509_add_ext, @function
X509_add_ext:
.LFB1313:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$104, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	X509v3_add_ext@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	testq	%rax, %rax
	setne	%al
	movzbl	%al, %eax
	ret
	.cfi_endproc
.LFE1313:
	.size	X509_add_ext, .-X509_add_ext
	.p2align 4
	.globl	X509_get_ext_d2i
	.type	X509_get_ext_d2i, @function
X509_get_ext_d2i:
.LFB1314:
	.cfi_startproc
	endbr64
	movq	104(%rdi), %rdi
	jmp	X509V3_get_d2i@PLT
	.cfi_endproc
.LFE1314:
	.size	X509_get_ext_d2i, .-X509_get_ext_d2i
	.p2align 4
	.globl	X509_add1_ext_i2d
	.type	X509_add1_ext_i2d, @function
X509_add1_ext_i2d:
.LFB1315:
	.cfi_startproc
	endbr64
	addq	$104, %rdi
	jmp	X509V3_add1_i2d@PLT
	.cfi_endproc
.LFE1315:
	.size	X509_add1_ext_i2d, .-X509_add1_ext_i2d
	.p2align 4
	.globl	X509_REVOKED_get_ext_count
	.type	X509_REVOKED_get_ext_count, @function
X509_REVOKED_get_ext_count:
.LFB1316:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rdi
	jmp	X509v3_get_ext_count@PLT
	.cfi_endproc
.LFE1316:
	.size	X509_REVOKED_get_ext_count, .-X509_REVOKED_get_ext_count
	.p2align 4
	.globl	X509_REVOKED_get_ext_by_NID
	.type	X509_REVOKED_get_ext_by_NID, @function
X509_REVOKED_get_ext_by_NID:
.LFB1317:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rdi
	jmp	X509v3_get_ext_by_NID@PLT
	.cfi_endproc
.LFE1317:
	.size	X509_REVOKED_get_ext_by_NID, .-X509_REVOKED_get_ext_by_NID
	.p2align 4
	.globl	X509_REVOKED_get_ext_by_OBJ
	.type	X509_REVOKED_get_ext_by_OBJ, @function
X509_REVOKED_get_ext_by_OBJ:
.LFB1318:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rdi
	jmp	X509v3_get_ext_by_OBJ@PLT
	.cfi_endproc
.LFE1318:
	.size	X509_REVOKED_get_ext_by_OBJ, .-X509_REVOKED_get_ext_by_OBJ
	.p2align 4
	.globl	X509_REVOKED_get_ext_by_critical
	.type	X509_REVOKED_get_ext_by_critical, @function
X509_REVOKED_get_ext_by_critical:
.LFB1319:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rdi
	jmp	X509v3_get_ext_by_critical@PLT
	.cfi_endproc
.LFE1319:
	.size	X509_REVOKED_get_ext_by_critical, .-X509_REVOKED_get_ext_by_critical
	.p2align 4
	.globl	X509_REVOKED_get_ext
	.type	X509_REVOKED_get_ext, @function
X509_REVOKED_get_ext:
.LFB1320:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rdi
	jmp	X509v3_get_ext@PLT
	.cfi_endproc
.LFE1320:
	.size	X509_REVOKED_get_ext, .-X509_REVOKED_get_ext
	.p2align 4
	.globl	X509_REVOKED_delete_ext
	.type	X509_REVOKED_delete_ext, @function
X509_REVOKED_delete_ext:
.LFB1321:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rdi
	jmp	X509v3_delete_ext@PLT
	.cfi_endproc
.LFE1321:
	.size	X509_REVOKED_delete_ext, .-X509_REVOKED_delete_ext
	.p2align 4
	.globl	X509_REVOKED_add_ext
	.type	X509_REVOKED_add_ext, @function
X509_REVOKED_add_ext:
.LFB1322:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$32, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	X509v3_add_ext@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	testq	%rax, %rax
	setne	%al
	movzbl	%al, %eax
	ret
	.cfi_endproc
.LFE1322:
	.size	X509_REVOKED_add_ext, .-X509_REVOKED_add_ext
	.p2align 4
	.globl	X509_REVOKED_get_ext_d2i
	.type	X509_REVOKED_get_ext_d2i, @function
X509_REVOKED_get_ext_d2i:
.LFB1323:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rdi
	jmp	X509V3_get_d2i@PLT
	.cfi_endproc
.LFE1323:
	.size	X509_REVOKED_get_ext_d2i, .-X509_REVOKED_get_ext_d2i
	.p2align 4
	.globl	X509_REVOKED_add1_ext_i2d
	.type	X509_REVOKED_add1_ext_i2d, @function
X509_REVOKED_add1_ext_i2d:
.LFB1324:
	.cfi_startproc
	endbr64
	addq	$32, %rdi
	jmp	X509V3_add1_i2d@PLT
	.cfi_endproc
.LFE1324:
	.size	X509_REVOKED_add1_ext_i2d, .-X509_REVOKED_add1_ext_i2d
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
