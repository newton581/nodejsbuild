	.file	"async.c"
	.text
	.p2align 4
	.type	async_fibre_swapcontext.constprop.0, @function
async_fibre_swapcontext.constprop.0:
.LFB272:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$968, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movl	$1, 200(%rdi)
	movq	%rsi, -8(%rbp)
	call	_setjmp@PLT
	endbr64
	testl	%eax, %eax
	jne	.L6
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	movl	1168(%rax), %eax
	testl	%eax, %eax
	jne	.L9
	call	setcontext@PLT
.L6:
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movl	$1, %eax
	ret
.L9:
	.cfi_restore_state
	addq	$968, %rdi
	movl	$1, %esi
	call	_longjmp@PLT
	.cfi_endproc
.LFE272:
	.size	async_fibre_swapcontext.constprop.0, .-async_fibre_swapcontext.constprop.0
	.p2align 4
	.globl	async_get_ctx
	.type	async_get_ctx, @function
async_get_ctx:
.LFB249:
	.cfi_startproc
	endbr64
	leaq	ctxkey(%rip), %rdi
	jmp	CRYPTO_THREAD_get_local@PLT
	.cfi_endproc
.LFE249:
	.size	async_get_ctx, .-async_get_ctx
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/async/async.c"
	.text
	.p2align 4
	.globl	async_start_func
	.type	async_start_func, @function
async_start_func:
.LFB255:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	ctxkey(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	.LC0(%rip), %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	call	CRYPTO_THREAD_get_local@PLT
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L12:
	movq	1176(%rbx), %r12
	movq	1184(%r12), %rdi
	call	*1176(%r12)
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movl	$3, 1196(%r12)
	movl	%eax, 1192(%r12)
	call	async_fibre_swapcontext.constprop.0
	testl	%eax, %eax
	jne	.L12
	movl	$162, %r8d
	movq	%r13, %rcx
	movl	$102, %edx
	movl	$104, %esi
	movl	$51, %edi
	call	ERR_put_error@PLT
	jmp	.L12
	.cfi_endproc
.LFE255:
	.size	async_start_func, .-async_start_func
	.p2align 4
	.globl	ASYNC_pause_job
	.type	ASYNC_pause_job, @function
ASYNC_pause_job:
.LFB257:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	ctxkey(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	call	CRYPTO_THREAD_get_local@PLT
	testq	%rax, %rax
	je	.L19
	movq	1176(%rax), %rbx
	movq	%rax, %rsi
	movl	$1, %eax
	testq	%rbx, %rbx
	je	.L16
	movl	1184(%rsi), %edx
	testl	%edx, %edx
	je	.L23
.L16:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	.cfi_restore_state
	movl	$1, 1196(%rbx)
	movq	%rbx, %rdi
	call	async_fibre_swapcontext.constprop.0
	testl	%eax, %eax
	je	.L24
	movq	1200(%rbx), %rdi
	call	async_wait_ctx_reset_counts@PLT
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	.cfi_restore_state
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	.cfi_restore_state
	movl	$275, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$102, %edx
	movl	%eax, -20(%rbp)
	movl	$103, %esi
	movl	$51, %edi
	call	ERR_put_error@PLT
	movl	-20(%rbp), %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE257:
	.size	ASYNC_pause_job, .-ASYNC_pause_job
	.p2align 4
	.globl	async_init
	.type	async_init, @function
async_init:
.LFB259:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	leaq	ctxkey(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	CRYPTO_THREAD_init_local@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	jne	.L32
.L25:
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	.cfi_restore_state
	xorl	%esi, %esi
	leaq	poolkey(%rip), %rdi
	call	CRYPTO_THREAD_init_local@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	je	.L33
	movl	$1, %r12d
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L33:
	.cfi_restore_state
	leaq	ctxkey(%rip), %rdi
	call	CRYPTO_THREAD_cleanup_local@PLT
	jmp	.L25
	.cfi_endproc
.LFE259:
	.size	async_init, .-async_init
	.p2align 4
	.globl	async_deinit
	.type	async_deinit, @function
async_deinit:
.LFB260:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	ctxkey(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	CRYPTO_THREAD_cleanup_local@PLT
	leaq	poolkey(%rip), %rdi
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_THREAD_cleanup_local@PLT
	.cfi_endproc
.LFE260:
	.size	async_deinit, .-async_deinit
	.p2align 4
	.globl	ASYNC_init_thread
	.type	ASYNC_init_thread, @function
ASYNC_init_thread:
.LFB261:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	cmpq	%rdi, %rsi
	ja	.L61
	movq	%rdi, %r12
	movq	%rsi, %rbx
	movl	$256, %edi
	xorl	%esi, %esi
	call	OPENSSL_init_crypto@PLT
	testl	%eax, %eax
	jne	.L39
.L60:
	xorl	%r12d, %r12d
.L36:
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L39:
	.cfi_restore_state
	movl	$1, %edi
	call	ossl_init_thread_start@PLT
	testl	%eax, %eax
	je	.L60
	movl	$332, %edx
	leaq	.LC0(%rip), %rsi
	movl	$24, %edi
	call	CRYPTO_zalloc@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L62
	movl	%ebx, %esi
	xorl	%edi, %edi
	call	OPENSSL_sk_new_reserve@PLT
	movq	%rax, (%r14)
	testq	%rax, %rax
	je	.L63
	movq	%r12, 16(%r14)
	xorl	%r13d, %r13d
	testq	%rbx, %rbx
	je	.L43
	leaq	.LC0(%rip), %r15
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L44:
	movl	$0, 1196(%rax)
	movq	%rax, %rdi
	call	async_fibre_makecontext@PLT
	testl	%eax, %eax
	je	.L45
	movq	(%r14), %rdi
	movq	%r12, %rsi
	addq	$1, %r13
	movq	$0, 1184(%r12)
	call	OPENSSL_sk_push@PLT
	cmpq	%r13, %rbx
	je	.L43
.L46:
	movl	$82, %edx
	movq	%r15, %rsi
	movl	$1208, %edi
	call	CRYPTO_zalloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L44
	movl	$84, %r8d
	movl	$65, %edx
	movl	$102, %esi
	movl	$51, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L43:
	movq	%r13, 8(%r14)
	movq	%r14, %rsi
	leaq	poolkey(%rip), %rdi
	call	CRYPTO_THREAD_set_local@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	je	.L64
	movl	$1, %r12d
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L61:
	movl	$322, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$103, %edx
	xorl	%r12d, %r12d
	movl	$101, %esi
	movl	$51, %edi
	call	ERR_put_error@PLT
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L45:
	movq	1184(%r12), %rdi
	movl	$96, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%r12, %rdi
	call	async_fibre_free@PLT
	movl	$98, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L62:
	movl	$334, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r12d, %r12d
	movl	$101, %esi
	movl	$51, %edi
	call	ERR_put_error@PLT
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L64:
	movl	$51, %edi
	movl	$365, %r8d
	movl	$101, %edx
	movl	$101, %esi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L47
	call	OPENSSL_sk_pop@PLT
	leaq	.LC0(%rip), %rbx
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L65
	.p2align 4,,10
	.p2align 3
.L48:
	movq	1184(%rax), %rdi
	movl	$96, %edx
	movq	%rbx, %rsi
	call	CRYPTO_free@PLT
	movq	%r13, %rdi
	call	async_fibre_free@PLT
	movq	%r13, %rdi
	movl	$98, %edx
	movq	%rbx, %rsi
	call	CRYPTO_free@PLT
	movq	(%r14), %rdi
	call	OPENSSL_sk_pop@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	jne	.L48
.L65:
	movq	(%r14), %rdi
.L47:
	call	OPENSSL_sk_free@PLT
	movl	$373, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r14, %rdi
	call	CRYPTO_free@PLT
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L63:
	movl	$340, %r8d
	movl	$65, %edx
	movl	$101, %esi
	movl	$51, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$341, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r14, %rdi
	call	CRYPTO_free@PLT
	jmp	.L60
	.cfi_endproc
.LFE261:
	.size	ASYNC_init_thread, .-ASYNC_init_thread
	.p2align 4
	.globl	ASYNC_start_job
	.type	ASYNC_start_job, @function
ASYNC_start_job:
.LFB256:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%r9, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movl	$256, %edi
	subq	$40, %rsp
	movq	%rsi, -56(%rbp)
	xorl	%esi, %esi
	movq	%rdx, -64(%rbp)
	call	OPENSSL_init_crypto@PLT
	testl	%eax, %eax
	jne	.L67
.L108:
	xorl	%r8d, %r8d
.L66:
	addq	$40, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L67:
	.cfi_restore_state
	leaq	ctxkey(%rip), %rdi
	call	CRYPTO_THREAD_get_local@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L109
.L69:
	movq	(%rbx), %r10
	testq	%r10, %r10
	je	.L78
	movq	%r10, 1176(%r14)
	testq	%r10, %r10
	je	.L74
.L113:
	movl	1196(%r10), %r8d
	cmpl	$3, %r8d
	je	.L110
	cmpl	$1, %r8d
	je	.L111
	cmpl	$2, %r8d
	jne	.L77
	movq	(%rbx), %rsi
	movq	%r14, %rdi
	movq	%rsi, 1176(%r14)
	call	async_fibre_swapcontext.constprop.0
	testl	%eax, %eax
	je	.L112
.L78:
	movq	1176(%r14), %r10
	testq	%r10, %r10
	jne	.L113
.L74:
	leaq	poolkey(%rip), %rdi
	call	CRYPTO_THREAD_get_local@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L114
.L80:
	movq	(%rcx), %rdi
	movq	%rcx, -72(%rbp)
	call	OPENSSL_sk_pop@PLT
	movq	-72(%rbp), %rcx
	testq	%rax, %rax
	movq	%rax, %r8
	je	.L115
.L82:
	movq	%r8, 1176(%r14)
	testq	%r12, %r12
	je	.L116
	movq	%r13, %rdi
	movl	$227, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r8, -72(%rbp)
	call	CRYPTO_malloc@PLT
	movq	-72(%rbp), %r8
	movq	%rax, 1184(%r8)
	movq	1176(%r14), %rax
	movq	1184(%rax), %rdi
	testq	%rdi, %rdi
	je	.L117
	movq	%r13, %rdx
	movq	%r12, %rsi
	call	memcpy@PLT
	movq	1176(%r14), %r8
.L85:
	movq	-56(%rbp), %rax
	movq	%r15, 1176(%r8)
	movq	%r8, %rsi
	movq	%r14, %rdi
	movq	%rax, 1200(%r8)
	call	async_fibre_swapcontext.constprop.0
	testl	%eax, %eax
	jne	.L78
	movl	$243, %r8d
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L112:
	movl	$207, %r8d
.L104:
	leaq	.LC0(%rip), %rcx
	movl	$102, %edx
.L106:
.L79:
	endbr64
	movl	$105, %esi
	movl	$51, %edi
	call	ERR_put_error@PLT
	movq	1176(%r14), %r13
	leaq	poolkey(%rip), %rdi
	call	CRYPTO_THREAD_get_local@PLT
	movl	$139, %edx
	leaq	.LC0(%rip), %rsi
	movq	1184(%r13), %rdi
	movq	%rax, %r12
	call	CRYPTO_free@PLT
	movq	%r13, %rsi
	movq	$0, 1184(%r13)
	movq	(%r12), %rdi
	call	OPENSSL_sk_push@PLT
	movq	$0, 1176(%r14)
	movq	$0, (%rbx)
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L114:
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	ASYNC_init_thread
	testl	%eax, %eax
	je	.L107
	leaq	poolkey(%rip), %rdi
	call	CRYPTO_THREAD_get_local@PLT
	movq	%rax, %rcx
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L116:
	movq	$0, 1184(%r8)
	jmp	.L85
	.p2align 4,,10
	.p2align 3
.L109:
	movl	$1, %edi
	call	ossl_init_thread_start@PLT
	testl	%eax, %eax
	je	.L108
	movl	$40, %edx
	leaq	.LC0(%rip), %rsi
	movl	$1192, %edi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L118
	movq	$0, 1176(%rax)
	movq	%rax, %rsi
	leaq	ctxkey(%rip), %rdi
	movl	$0, 1184(%rax)
	call	CRYPTO_THREAD_set_local@PLT
	testl	%eax, %eax
	jne	.L69
.L72:
	movl	$54, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r14, %rdi
	call	CRYPTO_free@PLT
	xorl	%r8d, %r8d
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L115:
	movq	16(%rcx), %rax
	testq	%rax, %rax
	je	.L83
	cmpq	8(%rcx), %rax
	ja	.L83
.L107:
	movq	$0, 1176(%r14)
	movl	$1, %r8d
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L83:
	movl	$82, %edx
	leaq	.LC0(%rip), %rsi
	movl	$1208, %edi
	movq	%rcx, -72(%rbp)
	call	CRYPTO_zalloc@PLT
	movq	-72(%rbp), %rcx
	testq	%rax, %rax
	movq	%rax, %r8
	je	.L119
	movl	$0, 1196(%r8)
	movq	%r8, %rdi
	movq	%rcx, -80(%rbp)
	movq	%r8, -72(%rbp)
	call	async_fibre_makecontext@PLT
	movq	-72(%rbp), %r8
	movq	-80(%rbp), %rcx
	testl	%eax, %eax
	je	.L120
	addq	$1, 8(%rcx)
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L119:
	movl	$84, %r8d
	movl	$65, %edx
	movl	$102, %esi
	movl	$51, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$1, %r8d
	movq	$0, 1176(%r14)
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L110:
	movl	1192(%r10), %eax
	movq	-64(%rbp), %rdx
	leaq	poolkey(%rip), %rdi
	movl	%r8d, -72(%rbp)
	movq	%r10, -56(%rbp)
	movl	%eax, (%rdx)
	movq	$0, 1200(%r10)
	call	CRYPTO_THREAD_get_local@PLT
	movq	-56(%rbp), %r10
	movl	$139, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rax, %r12
	movq	1184(%r10), %rdi
	call	CRYPTO_free@PLT
	movq	-56(%rbp), %r10
	movq	(%r12), %rdi
	movq	$0, 1184(%r10)
	movq	%r10, %rsi
	call	OPENSSL_sk_push@PLT
	movl	-72(%rbp), %r8d
	movq	$0, 1176(%r14)
	movq	$0, (%rbx)
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L111:
	movq	%r10, (%rbx)
	movl	$2, %r8d
	movl	$2, 1196(%r10)
	movq	$0, 1176(%r14)
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L77:
	movl	$215, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$68, %edx
	jmp	.L106
.L118:
	movl	$42, %r8d
	movl	$65, %edx
	movl	$100, %esi
	movl	$51, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L72
.L120:
	movq	1184(%r8), %rdi
	movl	$96, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r8, -56(%rbp)
	call	CRYPTO_free@PLT
	movq	-56(%rbp), %r8
	movq	%r8, %rdi
	call	async_fibre_free@PLT
	movq	-56(%rbp), %r8
	movl	$98, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r8, %rdi
	call	CRYPTO_free@PLT
	jmp	.L107
.L117:
	movl	$229, %r8d
	movl	$65, %edx
	movl	$105, %esi
	movl	$51, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	1176(%r14), %r12
	leaq	poolkey(%rip), %rdi
	call	CRYPTO_THREAD_get_local@PLT
	movl	$139, %edx
	leaq	.LC0(%rip), %rsi
	movq	1184(%r12), %rdi
	movq	%rax, %rbx
	call	CRYPTO_free@PLT
	movq	(%rbx), %rdi
	movq	%r12, %rsi
	movq	$0, 1184(%r12)
	call	OPENSSL_sk_push@PLT
	movq	$0, 1176(%r14)
	jmp	.L108
	.cfi_endproc
.LFE256:
	.size	ASYNC_start_job, .-ASYNC_start_job
	.p2align 4
	.globl	async_delete_thread_state
	.type	async_delete_thread_state, @function
async_delete_thread_state:
.LFB262:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	poolkey(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	call	CRYPTO_THREAD_get_local@PLT
	testq	%rax, %rax
	je	.L122
	movq	(%rax), %rdi
	movq	%rax, %r12
	testq	%rdi, %rdi
	je	.L123
	call	OPENSSL_sk_pop@PLT
	leaq	.LC0(%rip), %rbx
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L134
	.p2align 4,,10
	.p2align 3
.L124:
	movq	1184(%rax), %rdi
	movl	$96, %edx
	movq	%rbx, %rsi
	call	CRYPTO_free@PLT
	movq	%r13, %rdi
	call	async_fibre_free@PLT
	movq	%r13, %rdi
	movl	$98, %edx
	movq	%rbx, %rsi
	call	CRYPTO_free@PLT
	movq	(%r12), %rdi
	call	OPENSSL_sk_pop@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	jne	.L124
.L134:
	movq	(%r12), %rdi
.L123:
	call	OPENSSL_sk_free@PLT
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movl	$384, %edx
	call	CRYPTO_free@PLT
	xorl	%esi, %esi
	leaq	poolkey(%rip), %rdi
	call	CRYPTO_THREAD_set_local@PLT
.L122:
	call	async_local_cleanup@PLT
	leaq	ctxkey(%rip), %rdi
	call	CRYPTO_THREAD_get_local@PLT
	xorl	%esi, %esi
	leaq	ctxkey(%rip), %rdi
	movq	%rax, %r12
	call	CRYPTO_THREAD_set_local@PLT
	testl	%eax, %eax
	jne	.L135
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L135:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$73, %edx
	popq	%rbx
	leaq	.LC0(%rip), %rsi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_free@PLT
	.cfi_endproc
.LFE262:
	.size	async_delete_thread_state, .-async_delete_thread_state
	.p2align 4
	.globl	ASYNC_cleanup_thread
	.type	ASYNC_cleanup_thread, @function
ASYNC_cleanup_thread:
.LFB263:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movl	$256, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	OPENSSL_init_crypto@PLT
	testl	%eax, %eax
	jne	.L139
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L139:
	.cfi_restore_state
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	async_delete_thread_state
	.cfi_endproc
.LFE263:
	.size	ASYNC_cleanup_thread, .-ASYNC_cleanup_thread
	.p2align 4
	.globl	ASYNC_get_current_job
	.type	ASYNC_get_current_job, @function
ASYNC_get_current_job:
.LFB264:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movl	$256, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	OPENSSL_init_crypto@PLT
	testl	%eax, %eax
	je	.L143
	leaq	ctxkey(%rip), %rdi
	call	CRYPTO_THREAD_get_local@PLT
	testq	%rax, %rax
	je	.L143
	movq	1176(%rax), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L143:
	.cfi_restore_state
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE264:
	.size	ASYNC_get_current_job, .-ASYNC_get_current_job
	.p2align 4
	.globl	ASYNC_get_wait_ctx
	.type	ASYNC_get_wait_ctx, @function
ASYNC_get_wait_ctx:
.LFB265:
	.cfi_startproc
	endbr64
	movq	1200(%rdi), %rax
	ret
	.cfi_endproc
.LFE265:
	.size	ASYNC_get_wait_ctx, .-ASYNC_get_wait_ctx
	.p2align 4
	.globl	ASYNC_block_pause
	.type	ASYNC_block_pause, @function
ASYNC_block_pause:
.LFB266:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movl	$256, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	OPENSSL_init_crypto@PLT
	testl	%eax, %eax
	jne	.L157
.L149:
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L157:
	.cfi_restore_state
	leaq	ctxkey(%rip), %rdi
	call	CRYPTO_THREAD_get_local@PLT
	testq	%rax, %rax
	je	.L149
	cmpq	$0, 1176(%rax)
	je	.L149
	addl	$1, 1184(%rax)
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE266:
	.size	ASYNC_block_pause, .-ASYNC_block_pause
	.p2align 4
	.globl	ASYNC_unblock_pause
	.type	ASYNC_unblock_pause, @function
ASYNC_unblock_pause:
.LFB267:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movl	$256, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	OPENSSL_init_crypto@PLT
	testl	%eax, %eax
	jne	.L169
.L158:
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L169:
	.cfi_restore_state
	leaq	ctxkey(%rip), %rdi
	call	CRYPTO_THREAD_get_local@PLT
	testq	%rax, %rax
	je	.L158
	cmpq	$0, 1176(%rax)
	je	.L158
	movl	1184(%rax), %edx
	testl	%edx, %edx
	je	.L158
	subl	$1, %edx
	movl	%edx, 1184(%rax)
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE267:
	.size	ASYNC_unblock_pause, .-ASYNC_unblock_pause
	.local	poolkey
	.comm	poolkey,4,4
	.local	ctxkey
	.comm	ctxkey,4,4
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
