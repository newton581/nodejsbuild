	.file	"d2i_pu.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/asn1/d2i_pu.c"
	.text
	.p2align 4
	.globl	d2i_PublicKey
	.type	d2i_PublicKey, @function
d2i_PublicKey:
.LFB445:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edi, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$8, %rsp
	testq	%rsi, %rsi
	je	.L2
	movq	(%rsi), %r12
	testq	%r12, %r12
	je	.L2
.L3:
	movq	%r12, %rdi
	call	EVP_PKEY_id@PLT
	cmpl	%r15d, %eax
	je	.L5
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	EVP_PKEY_set_type@PLT
	testl	%eax, %eax
	je	.L25
.L5:
	movq	%r12, %rdi
	call	EVP_PKEY_id@PLT
	cmpl	$116, %eax
	je	.L7
	cmpl	$408, %eax
	je	.L8
	cmpl	$6, %eax
	je	.L26
	movl	$67, %r8d
	movl	$163, %edx
	movl	$155, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L6:
	testq	%rbx, %rbx
	je	.L11
	cmpq	%r12, (%rbx)
	je	.L12
.L11:
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	EVP_PKEY_free@PLT
.L1:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	.cfi_restore_state
	call	EVP_PKEY_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L3
	movl	$29, %r8d
	movl	$6, %edx
	movl	$155, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L26:
	movq	%r14, %rdx
	movq	%r13, %rsi
	xorl	%edi, %edi
	call	d2i_RSAPublicKey@PLT
	movl	$44, %r8d
	movq	%rax, 40(%r12)
	testq	%rax, %rax
	je	.L23
.L10:
	testq	%rbx, %rbx
	je	.L1
	movq	%r12, (%rbx)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L8:
	leaq	40(%r12), %rdi
	movq	%r14, %rdx
	movq	%r13, %rsi
	call	o2i_ECPublicKey@PLT
	testq	%rax, %rax
	jne	.L10
	movl	$61, %r8d
.L23:
	movl	$13, %edx
	movl	$155, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L7:
	leaq	40(%r12), %rdi
	movq	%r14, %rdx
	movq	%r13, %rsi
	call	d2i_DSAPublicKey@PLT
	movl	$53, %r8d
	testq	%rax, %rax
	jne	.L10
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L25:
	movl	$36, %r8d
	movl	$6, %edx
	movl	$155, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L12:
	xorl	%r12d, %r12d
	jmp	.L1
	.cfi_endproc
.LFE445:
	.size	d2i_PublicKey, .-d2i_PublicKey
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
