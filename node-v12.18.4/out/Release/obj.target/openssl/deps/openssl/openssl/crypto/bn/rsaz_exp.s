	.file	"rsaz_exp.c"
	.text
	.p2align 4
	.globl	RSAZ_1024_mod_exp_avx2
	.type	RSAZ_1024_mod_exp_avx2, @function
RSAZ_1024_mod_exp_avx2:
.LFB140:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$1608, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -5720(%rbp)
	movq	%rsi, %r10
	movq	%r8, %r15
	movq	%rcx, %rsi
	movq	%rdx, -5712(%rbp)
	movq	%r9, %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-5696(%rbp), %rax
	movq	%rax, -5736(%rbp)
	andq	$-64, %rax
	leaq	64(%rax), %r12
	leaq	1024(%rax), %rdi
	leaq	384(%rax), %r14
	leaq	704(%rax), %r13
	movq	%r12, %rax
	movq	%rdi, -5704(%rbp)
	andl	$4095, %eax
	addq	$320, %rax
	shrq	$12, %rax
	je	.L2
	movq	%r13, %rax
	movq	%r14, %r13
	movq	%r12, %r14
	movq	%rax, %r12
.L2:
	movq	%r12, %rdi
	movq	%r10, -5728(%rbp)
	call	rsaz_1024_norm2red_avx2@PLT
	movq	-5728(%rbp), %r10
	movq	%r13, %rdi
	movq	%r10, %rsi
	call	rsaz_1024_norm2red_avx2@PLT
	movq	%r15, %rsi
	movq	-5704(%rbp), %r15
	movq	%r15, %rdi
	call	rsaz_1024_norm2red_avx2@PLT
	movq	%r15, %rdx
	movq	%r15, %rsi
	movq	%r15, %rdi
	movq	%rbx, %r8
	movq	%r12, %rcx
	call	rsaz_1024_mul_avx2@PLT
	movq	%r15, %rsi
	movq	%r15, %rdi
	movq	%rbx, %r8
	movq	%r12, %rcx
	leaq	two80(%rip), %rdx
	call	rsaz_1024_mul_avx2@PLT
	movq	%r15, %rsi
	movq	%rbx, %r8
	movq	%r12, %rcx
	leaq	one(%rip), %rdx
	movq	%r14, %rdi
	call	rsaz_1024_mul_avx2@PLT
	movq	%rbx, %r8
	movq	%r12, %rcx
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r13, %rdi
	call	rsaz_1024_mul_avx2@PLT
	xorl	%edx, %edx
	movq	%r15, %rdi
	movq	%r14, %rsi
	call	rsaz_1024_scatter5_avx2@PLT
	movq	%r15, %rdi
	movl	$1, %edx
	movq	%r13, %rsi
	call	rsaz_1024_scatter5_avx2@PLT
	movq	%rbx, %rcx
	movq	%r12, %rdx
	movq	%r13, %rsi
	movl	$1, %r8d
	movq	%r14, %rdi
	call	rsaz_1024_sqr_avx2@PLT
	movq	%r15, %rdi
	movl	$2, %edx
	movq	%r14, %rsi
	call	rsaz_1024_scatter5_avx2@PLT
	movq	%rbx, %rcx
	movq	%r12, %rdx
	movq	%r14, %rsi
	movl	$1, %r8d
	movq	%r14, %rdi
	call	rsaz_1024_sqr_avx2@PLT
	movq	%r15, %rdi
	movl	$4, %edx
	movq	%r14, %rsi
	call	rsaz_1024_scatter5_avx2@PLT
	movq	%rbx, %rcx
	movq	%r12, %rdx
	movq	%r14, %rsi
	movl	$1, %r8d
	movq	%r14, %rdi
	call	rsaz_1024_sqr_avx2@PLT
	movq	%r15, %rdi
	movl	$8, %edx
	movq	%r14, %rsi
	call	rsaz_1024_scatter5_avx2@PLT
	movq	%rbx, %rcx
	movq	%r12, %rdx
	movq	%r14, %rsi
	movl	$1, %r8d
	movq	%r14, %rdi
	call	rsaz_1024_sqr_avx2@PLT
	movq	%r15, %rdi
	movl	$16, %edx
	movq	%r14, %rsi
	call	rsaz_1024_scatter5_avx2@PLT
	movq	%rbx, %r8
	movq	%r12, %rcx
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r14, %rdi
	call	rsaz_1024_mul_avx2@PLT
	movq	%r15, %rdi
	movl	$17, %edx
	movq	%r14, %rsi
	call	rsaz_1024_scatter5_avx2@PLT
	movq	%r15, %rsi
	movl	$2, %edx
	movq	%r14, %rdi
	call	rsaz_1024_gather5_avx2@PLT
	movq	%rbx, %r8
	movq	%r12, %rcx
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r14, %rdi
	call	rsaz_1024_mul_avx2@PLT
	movq	%r15, %rdi
	movl	$3, %edx
	movq	%r14, %rsi
	call	rsaz_1024_scatter5_avx2@PLT
	movq	%rbx, %rcx
	movq	%r12, %rdx
	movq	%r14, %rsi
	movl	$1, %r8d
	movq	%r14, %rdi
	call	rsaz_1024_sqr_avx2@PLT
	movq	%r15, %rdi
	movl	$6, %edx
	movq	%r14, %rsi
	call	rsaz_1024_scatter5_avx2@PLT
	movq	%rbx, %rcx
	movq	%r12, %rdx
	movq	%r14, %rsi
	movl	$1, %r8d
	movq	%r14, %rdi
	call	rsaz_1024_sqr_avx2@PLT
	movq	%r15, %rdi
	movl	$12, %edx
	movq	%r14, %rsi
	call	rsaz_1024_scatter5_avx2@PLT
	movq	%rbx, %rcx
	movq	%r12, %rdx
	movq	%r14, %rsi
	movl	$1, %r8d
	movq	%r14, %rdi
	call	rsaz_1024_sqr_avx2@PLT
	movq	%r15, %rdi
	movl	$24, %edx
	movq	%r14, %rsi
	call	rsaz_1024_scatter5_avx2@PLT
	movq	%rbx, %r8
	movq	%r12, %rcx
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r14, %rdi
	call	rsaz_1024_mul_avx2@PLT
	movq	%r15, %rdi
	movl	$25, %edx
	movq	%r14, %rsi
	call	rsaz_1024_scatter5_avx2@PLT
	movq	%r15, %rsi
	movl	$4, %edx
	movq	%r14, %rdi
	call	rsaz_1024_gather5_avx2@PLT
	movq	%rbx, %r8
	movq	%r12, %rcx
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r14, %rdi
	call	rsaz_1024_mul_avx2@PLT
	movq	%r15, %rdi
	movl	$5, %edx
	movq	%r14, %rsi
	call	rsaz_1024_scatter5_avx2@PLT
	movq	%rbx, %rcx
	movq	%r12, %rdx
	movq	%r14, %rsi
	movl	$1, %r8d
	movq	%r14, %rdi
	call	rsaz_1024_sqr_avx2@PLT
	movq	%r15, %rdi
	movl	$10, %edx
	movq	%r14, %rsi
	call	rsaz_1024_scatter5_avx2@PLT
	movq	%rbx, %rcx
	movq	%r12, %rdx
	movq	%r14, %rsi
	movl	$1, %r8d
	movq	%r14, %rdi
	call	rsaz_1024_sqr_avx2@PLT
	movq	%r15, %rdi
	movl	$20, %edx
	movq	%r14, %rsi
	call	rsaz_1024_scatter5_avx2@PLT
	movq	%rbx, %r8
	movq	%r12, %rcx
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r14, %rdi
	call	rsaz_1024_mul_avx2@PLT
	movq	%r15, %rdi
	movl	$21, %edx
	movq	%r14, %rsi
	call	rsaz_1024_scatter5_avx2@PLT
	movq	%r15, %rsi
	movl	$6, %edx
	movq	%r14, %rdi
	call	rsaz_1024_gather5_avx2@PLT
	movq	%rbx, %r8
	movq	%r12, %rcx
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r14, %rdi
	call	rsaz_1024_mul_avx2@PLT
	movq	%r15, %rdi
	movl	$7, %edx
	movq	%r14, %rsi
	call	rsaz_1024_scatter5_avx2@PLT
	movq	%rbx, %rcx
	movq	%r12, %rdx
	movq	%r14, %rsi
	movl	$1, %r8d
	movq	%r14, %rdi
	call	rsaz_1024_sqr_avx2@PLT
	movq	%r15, %rdi
	movl	$14, %edx
	movq	%r14, %rsi
	call	rsaz_1024_scatter5_avx2@PLT
	movq	%rbx, %rcx
	movq	%r12, %rdx
	movq	%r14, %rsi
	movl	$1, %r8d
	movq	%r14, %rdi
	call	rsaz_1024_sqr_avx2@PLT
	movq	%r15, %rdi
	movl	$28, %edx
	movq	%r14, %rsi
	call	rsaz_1024_scatter5_avx2@PLT
	movq	%rbx, %r8
	movq	%r12, %rcx
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r14, %rdi
	call	rsaz_1024_mul_avx2@PLT
	movq	%r15, %rdi
	movl	$29, %edx
	movq	%r14, %rsi
	call	rsaz_1024_scatter5_avx2@PLT
	movq	%r15, %rsi
	movl	$8, %edx
	movq	%r14, %rdi
	call	rsaz_1024_gather5_avx2@PLT
	movq	%rbx, %r8
	movq	%r12, %rcx
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r14, %rdi
	call	rsaz_1024_mul_avx2@PLT
	movq	%r15, %rdi
	movl	$9, %edx
	movq	%r14, %rsi
	call	rsaz_1024_scatter5_avx2@PLT
	movq	%rbx, %rcx
	movq	%r12, %rdx
	movq	%r14, %rsi
	movl	$1, %r8d
	movq	%r14, %rdi
	call	rsaz_1024_sqr_avx2@PLT
	movq	%r15, %rdi
	movl	$18, %edx
	movq	%r14, %rsi
	call	rsaz_1024_scatter5_avx2@PLT
	movq	%rbx, %r8
	movq	%r12, %rcx
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r14, %rdi
	call	rsaz_1024_mul_avx2@PLT
	movq	%r15, %rdi
	movl	$19, %edx
	movq	%r14, %rsi
	call	rsaz_1024_scatter5_avx2@PLT
	movq	%r15, %rsi
	movl	$10, %edx
	movq	%r14, %rdi
	call	rsaz_1024_gather5_avx2@PLT
	movq	%rbx, %r8
	movq	%r12, %rcx
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r14, %rdi
	call	rsaz_1024_mul_avx2@PLT
	movq	%r15, %rdi
	movl	$11, %edx
	movq	%r14, %rsi
	call	rsaz_1024_scatter5_avx2@PLT
	movq	%rbx, %rcx
	movq	%r12, %rdx
	movq	%r14, %rsi
	movl	$1, %r8d
	movq	%r14, %rdi
	call	rsaz_1024_sqr_avx2@PLT
	movq	%r15, %rdi
	movl	$22, %edx
	movq	%r14, %rsi
	call	rsaz_1024_scatter5_avx2@PLT
	movq	%rbx, %r8
	movq	%r12, %rcx
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r14, %rdi
	call	rsaz_1024_mul_avx2@PLT
	movq	%r15, %rdi
	movl	$23, %edx
	movq	%r14, %rsi
	call	rsaz_1024_scatter5_avx2@PLT
	movq	%r15, %rsi
	movl	$12, %edx
	movq	%r14, %rdi
	call	rsaz_1024_gather5_avx2@PLT
	movq	%rbx, %r8
	movq	%r12, %rcx
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r14, %rdi
	call	rsaz_1024_mul_avx2@PLT
	movq	%r15, %rdi
	movl	$13, %edx
	movq	%r14, %rsi
	call	rsaz_1024_scatter5_avx2@PLT
	movq	%rbx, %rcx
	movq	%r12, %rdx
	movq	%r14, %rsi
	movl	$1, %r8d
	movq	%r14, %rdi
	call	rsaz_1024_sqr_avx2@PLT
	movq	%r15, %rdi
	movl	$26, %edx
	movq	%r14, %rsi
	call	rsaz_1024_scatter5_avx2@PLT
	movq	%rbx, %r8
	movq	%r12, %rcx
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r14, %rdi
	call	rsaz_1024_mul_avx2@PLT
	movq	%r15, %rdi
	movl	$27, %edx
	movq	%r14, %rsi
	call	rsaz_1024_scatter5_avx2@PLT
	movq	%r15, %rsi
	movl	$14, %edx
	movq	%r14, %rdi
	call	rsaz_1024_gather5_avx2@PLT
	movq	%rbx, %r8
	movq	%r12, %rcx
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r14, %rdi
	call	rsaz_1024_mul_avx2@PLT
	movq	%r15, %rdi
	movl	$15, %edx
	movq	%r14, %rsi
	call	rsaz_1024_scatter5_avx2@PLT
	movq	%rbx, %rcx
	movq	%r12, %rdx
	movq	%r14, %rsi
	movl	$1, %r8d
	movq	%r14, %rdi
	call	rsaz_1024_sqr_avx2@PLT
	movq	%r15, %rdi
	movl	$30, %edx
	movq	%r14, %rsi
	call	rsaz_1024_scatter5_avx2@PLT
	movq	%rbx, %r8
	movq	%r12, %rcx
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r14, %rdi
	call	rsaz_1024_mul_avx2@PLT
	movq	%r15, %rdi
	movl	$31, %edx
	movq	%r14, %rsi
	call	rsaz_1024_scatter5_avx2@PLT
	movq	-5712(%rbp), %rax
	movq	%r15, %rsi
	movq	%r14, %rdi
	movl	$1014, %r15d
	movzbl	127(%rax), %eax
	movl	%eax, %edx
	movb	%al, -5728(%rbp)
	shrb	$3, %dl
	movzbl	%dl, %edx
	call	rsaz_1024_gather5_avx2@PLT
	.p2align 4,,10
	.p2align 3
.L3:
	movl	$5, %r8d
	movq	%rbx, %rcx
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r14, %rdi
	call	rsaz_1024_sqr_avx2@PLT
	movl	%r15d, %ecx
	movq	-5712(%rbp), %rax
	movq	%r13, %rdi
	sarl	$3, %ecx
	movq	-5704(%rbp), %rsi
	movslq	%ecx, %rcx
	movzbl	1(%rax,%rcx), %edx
	movzbl	(%rax,%rcx), %ecx
	sall	$8, %edx
	orl	%ecx, %edx
	movl	%r15d, %ecx
	subl	$5, %r15d
	andl	$7, %ecx
	sarl	%cl, %edx
	andl	$31, %edx
	call	rsaz_1024_gather5_avx2@PLT
	movq	%rbx, %r8
	movq	%r12, %rcx
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r14, %rdi
	call	rsaz_1024_mul_avx2@PLT
	cmpl	$-1, %r15d
	jne	.L3
	movl	$4, %r8d
	movq	%rbx, %rcx
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r14, %rdi
	call	rsaz_1024_sqr_avx2@PLT
	movq	-5712(%rbp), %rax
	movq	-5704(%rbp), %rsi
	movq	%r13, %rdi
	movzbl	(%rax), %edx
	andl	$15, %edx
	call	rsaz_1024_gather5_avx2@PLT
	movq	%rbx, %r8
	movq	%r12, %rcx
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r14, %rdi
	call	rsaz_1024_mul_avx2@PLT
	movq	%rbx, %r8
	movq	%r12, %rcx
	movq	%r14, %rsi
	leaq	one(%rip), %rdx
	movq	%r14, %rdi
	call	rsaz_1024_mul_avx2@PLT
	movq	-5720(%rbp), %rdi
	movq	%r14, %rsi
	call	rsaz_1024_red2norm_avx2@PLT
	movq	-5736(%rbp), %rdi
	movl	$5632, %esi
	call	OPENSSL_cleanse@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L11
	addq	$5704, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L11:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE140:
	.size	RSAZ_1024_mod_exp_avx2, .-RSAZ_1024_mod_exp_avx2
	.p2align 4
	.globl	RSAZ_512_mod_exp
	.type	RSAZ_512_mod_exp, @function
RSAZ_512_mod_exp:
.LFB141:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	subq	$1288, %rsp
	.cfi_offset 3, -56
	movq	%rdi, -1304(%rbp)
	movq	%rsi, -1320(%rbp)
	movq	%rdx, -1296(%rbp)
	movq	%r9, -1288(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-1280(%rbp), %rax
	movq	%rax, %rdx
	movq	%rax, -1312(%rbp)
	andq	$-64, %rdx
	leaq	64(%rdx), %r13
	leaq	1088(%rdx), %rbx
	leaq	1152(%rdx), %r15
	movq	(%rcx), %rdx
	movq	%r13, %rdi
	movq	%r15, %rsi
	negq	%rdx
	movq	%rdx, 1088(%r13)
	movq	8(%rcx), %rdx
	notq	%rdx
	movq	%rdx, 1096(%r13)
	movq	16(%rcx), %rdx
	notq	%rdx
	movq	%rdx, 1104(%r13)
	movq	24(%rcx), %rdx
	notq	%rdx
	movq	%rdx, 1112(%r13)
	movq	32(%rcx), %rdx
	notq	%rdx
	movq	%rdx, 1120(%r13)
	movq	40(%rcx), %rdx
	notq	%rdx
	movq	%rdx, 1128(%r13)
	movq	48(%rcx), %rdx
	notq	%rdx
	movq	%rdx, 1136(%r13)
	movq	56(%rcx), %rdx
	notq	%rdx
	movq	%rdx, 1144(%r13)
	xorl	%edx, %edx
	call	rsaz_512_scatter4@PLT
	movq	%r12, %r8
	movq	%r14, %rcx
	movq	%rbx, %rdi
	movq	-1288(%rbp), %r9
	movq	-1320(%rbp), %r10
	movq	%r9, %rdx
	movq	%r10, %rsi
	call	rsaz_512_mul@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movl	$1, %edx
	call	rsaz_512_scatter4@PLT
	movq	%r12, %rcx
	movq	%rbx, %rsi
	movq	%r14, %rdx
	movl	$1, %r8d
	movq	%r15, %rdi
	call	rsaz_512_sqr@PLT
	movq	%r13, %rdi
	movl	$2, %edx
	movq	%r15, %rsi
	call	rsaz_512_scatter4@PLT
	movl	$3, %r9d
	movq	%r13, %r8
	movq	%r12, %r13
	movq	%rbx, %r12
	movl	%r9d, %ebx
	.p2align 4,,10
	.p2align 3
.L13:
	movl	%ebx, %r9d
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	addl	$1, %ebx
	movq	%r8, -1288(%rbp)
	call	rsaz_512_mul_scatter4@PLT
	cmpl	$16, %ebx
	movq	-1288(%rbp), %r8
	jne	.L13
	movq	-1296(%rbp), %rax
	movq	%r13, %r12
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r8, %r13
	movzbl	63(%rax), %edx
	movl	%edx, %ebx
	shrl	$4, %edx
	call	rsaz_512_gather4@PLT
	movq	%r15, %rsi
	movq	%r15, %rdi
	movq	%r12, %rcx
	movl	$4, %r8d
	movq	%r14, %rdx
	call	rsaz_512_sqr@PLT
	movl	%ebx, %r9d
	movq	%r15, %rsi
	movq	%r15, %rdi
	andl	$15, %r9d
	movq	%r12, %r8
	movq	%r14, %rcx
	movq	%r13, %rdx
	call	rsaz_512_mul_gather4@PLT
	movq	-1296(%rbp), %rax
	movq	%r15, %rbx
	addq	$62, %rax
	movq	%rax, %r15
	.p2align 4,,10
	.p2align 3
.L14:
	movzbl	(%r15), %eax
	movl	$4, %r8d
	movq	%r12, %rcx
	movq	%r14, %rdx
	movq	%rbx, %rsi
	movq	%rbx, %rdi
	movb	%al, -1288(%rbp)
	call	rsaz_512_sqr@PLT
	movq	%r12, %r8
	movq	%r14, %rcx
	movq	%r13, %rdx
	movzbl	-1288(%rbp), %r9d
	movq	%rbx, %rsi
	movq	%rbx, %rdi
	shrl	$4, %r9d
	call	rsaz_512_mul_gather4@PLT
	movq	%r12, %rcx
	movq	%r14, %rdx
	movq	%rbx, %rsi
	movl	$4, %r8d
	movq	%rbx, %rdi
	call	rsaz_512_sqr@PLT
	movq	%r13, %rdx
	movq	%r12, %r8
	movq	%r14, %rcx
	movl	-1288(%rbp), %r9d
	movq	%rbx, %rsi
	movq	%rbx, %rdi
	andl	$15, %r9d
	call	rsaz_512_mul_gather4@PLT
	movq	%r15, %rdx
	subq	$1, %r15
	cmpq	-1296(%rbp), %rdx
	jne	.L14
	movq	-1304(%rbp), %rdi
	movq	%r12, %rcx
	movq	%r14, %rdx
	movq	%rbx, %rsi
	call	rsaz_512_mul_by_one@PLT
	movq	-1312(%rbp), %rdi
	movl	$1216, %esi
	call	OPENSSL_cleanse@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L19
	addq	$1288, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L19:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE141:
	.size	RSAZ_512_mod_exp, .-RSAZ_512_mod_exp
	.section	.rodata
	.align 64
	.type	two80, @object
	.size	two80, 320
two80:
	.quad	0
	.quad	0
	.quad	4194304
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.align 64
	.type	one, @object
	.size	one, 320
one:
	.quad	1
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
