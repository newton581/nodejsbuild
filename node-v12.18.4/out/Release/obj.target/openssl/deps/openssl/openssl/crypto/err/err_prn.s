	.file	"err_prn.c"
	.text
	.p2align 4
	.type	print_bio, @function
print_bio:
.LFB252:
	.cfi_startproc
	endbr64
	movq	%rdi, %r8
	movq	%rdx, %rdi
	movl	%esi, %edx
	movq	%r8, %rsi
	jmp	BIO_write@PLT
	.cfi_endproc
.LFE252:
	.size	print_bio, .-print_bio
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"%lu:%s:%s:%d:%s\n"
.LC1:
	.string	""
	.text
	.p2align 4
	.type	ERR_print_errors_cb.constprop.0, @function
ERR_print_errors_cb.constprop.0:
.LFB255:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$344, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, %rbx
	leaq	-4436(%rbp), %r15
	leaq	-4424(%rbp), %r14
	call	CRYPTO_THREAD_get_current_id@PLT
	leaq	-4440(%rbp), %r13
	movq	%rax, -4472(%rbp)
	leaq	-4432(%rbp), %rax
	movq	%rax, -4464(%rbp)
	.p2align 4,,10
	.p2align 3
.L4:
	movq	-4464(%rbp), %rdi
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	call	ERR_get_error_line_data@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L3
	leaq	-4416(%rbp), %r8
	movl	$256, %edx
	leaq	-4160(%rbp), %r12
	movq	%r8, %rsi
	movq	%r8, -4456(%rbp)
	call	ERR_error_string_n@PLT
	testb	$2, -4436(%rbp)
	movq	-4456(%rbp), %r8
	je	.L5
	movl	-4440(%rbp), %eax
	pushq	-4424(%rbp)
	leaq	.LC0(%rip), %rdx
	movl	$4096, %esi
	movq	-4432(%rbp), %r9
	movq	%r12, %rdi
	pushq	%rax
	movq	-4472(%rbp), %rcx
	xorl	%eax, %eax
	call	BIO_snprintf@PLT
	movq	%r12, %rdx
.L6:
	movl	(%rdx), %ecx
	addq	$4, %rdx
	leal	-16843009(%rcx), %eax
	notl	%ecx
	andl	%ecx, %eax
	andl	$-2139062144, %eax
	je	.L6
.L19:
	movl	%eax, %ecx
	movq	%rbx, %rdi
	shrl	$16, %ecx
	testl	$32896, %eax
	cmove	%ecx, %eax
	leaq	2(%rdx), %rcx
	cmove	%rcx, %rdx
	movl	%eax, %esi
	addb	%al, %sil
	movq	%r12, %rsi
	sbbq	$3, %rdx
	subq	%r12, %rdx
	call	BIO_write@PLT
	popq	%rdx
	popq	%rcx
	testl	%eax, %eax
	jg	.L4
.L3:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L20
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	.cfi_restore_state
	leaq	.LC1(%rip), %rax
	movl	$4096, %esi
	movq	%r12, %rdi
	movq	-4432(%rbp), %r9
	pushq	%rax
	movl	-4440(%rbp), %eax
	leaq	.LC0(%rip), %rdx
	movq	-4472(%rbp), %rcx
	pushq	%rax
	xorl	%eax, %eax
	call	BIO_snprintf@PLT
	movq	%r12, %rdx
.L10:
	movl	(%rdx), %ecx
	addq	$4, %rdx
	leal	-16843009(%rcx), %eax
	notl	%ecx
	andl	%ecx, %eax
	andl	$-2139062144, %eax
	je	.L10
	jmp	.L19
.L20:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE255:
	.size	ERR_print_errors_cb.constprop.0, .-ERR_print_errors_cb.constprop.0
	.p2align 4
	.globl	ERR_print_errors_cb
	.type	ERR_print_errors_cb, @function
ERR_print_errors_cb:
.LFB251:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$344, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -4464(%rbp)
	movq	%rsi, %rbx
	leaq	-4436(%rbp), %r15
	leaq	-4424(%rbp), %r14
	leaq	-4440(%rbp), %r13
	leaq	-4432(%rbp), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	CRYPTO_THREAD_get_current_id@PLT
	movq	%rax, -4472(%rbp)
	.p2align 4,,10
	.p2align 3
.L22:
	movq	%r12, %rdi
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	call	ERR_get_error_line_data@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L21
	leaq	-4416(%rbp), %r8
	movl	$256, %edx
	movq	%r8, %rsi
	movq	%r8, -4456(%rbp)
	call	ERR_error_string_n@PLT
	testb	$2, -4436(%rbp)
	movq	-4456(%rbp), %r8
	leaq	-4160(%rbp), %rdi
	je	.L23
	movl	-4440(%rbp), %eax
	pushq	-4424(%rbp)
	movl	$4096, %esi
	leaq	.LC0(%rip), %rdx
	movq	-4432(%rbp), %r9
	movq	%rdi, -4456(%rbp)
	pushq	%rax
	movq	-4472(%rbp), %rcx
	xorl	%eax, %eax
	call	BIO_snprintf@PLT
	movq	-4456(%rbp), %rdi
	movq	%rdi, %rsi
.L24:
	movl	(%rsi), %edx
	addq	$4, %rsi
	leal	-16843009(%rdx), %eax
	notl	%edx
	andl	%edx, %eax
	andl	$-2139062144, %eax
	je	.L24
.L37:
	movl	%eax, %edx
	shrl	$16, %edx
	testl	$32896, %eax
	cmove	%edx, %eax
	leaq	2(%rsi), %rdx
	cmove	%rdx, %rsi
	movq	%rbx, %rdx
	movl	%eax, %ecx
	addb	%al, %cl
	movq	-4464(%rbp), %rax
	sbbq	$3, %rsi
	subq	%rdi, %rsi
	call	*%rax
	popq	%rdx
	popq	%rcx
	testl	%eax, %eax
	jg	.L22
.L21:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L38
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	.cfi_restore_state
	leaq	.LC1(%rip), %rax
	movq	-4432(%rbp), %r9
	movq	-4472(%rbp), %rcx
	movl	$4096, %esi
	pushq	%rax
	movl	-4440(%rbp), %eax
	leaq	.LC0(%rip), %rdx
	movq	%rdi, -4456(%rbp)
	pushq	%rax
	xorl	%eax, %eax
	call	BIO_snprintf@PLT
	movq	-4456(%rbp), %rdi
	movq	%rdi, %rsi
.L28:
	movl	(%rsi), %edx
	addq	$4, %rsi
	leal	-16843009(%rdx), %eax
	notl	%edx
	andl	%edx, %eax
	andl	$-2139062144, %eax
	je	.L28
	jmp	.L37
.L38:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE251:
	.size	ERR_print_errors_cb, .-ERR_print_errors_cb
	.p2align 4
	.globl	ERR_print_errors
	.type	ERR_print_errors, @function
ERR_print_errors:
.LFB253:
	.cfi_startproc
	endbr64
	jmp	ERR_print_errors_cb.constprop.0
	.cfi_endproc
.LFE253:
	.size	ERR_print_errors, .-ERR_print_errors
	.p2align 4
	.globl	ERR_print_errors_fp
	.type	ERR_print_errors_fp, @function
ERR_print_errors_fp:
.LFB254:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	BIO_new_fp@PLT
	testq	%rax, %rax
	je	.L40
	movq	%rax, %rdi
	movq	%rax, %r12
	call	ERR_print_errors_cb.constprop.0
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	BIO_free@PLT
	.p2align 4,,10
	.p2align 3
.L40:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE254:
	.size	ERR_print_errors_fp, .-ERR_print_errors_fp
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
