	.file	"ctr128.c"
	.text
	.p2align 4
	.globl	CRYPTO_ctr128_encrypt
	.type	CRYPTO_ctr128_encrypt, @function
CRYPTO_ctr128_encrypt:
.LFB153:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%r9, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	movq	%rcx, %rdx
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$40, %rsp
	movq	16(%rbp), %rcx
	movl	(%rcx), %r15d
	testl	%r15d, %r15d
	jne	.L67
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L69:
	movl	%r15d, %ecx
	addq	$1, %r13
	addq	$1, %rax
	addl	$1, %r15d
	movzbl	(%r14,%rcx), %ecx
	xorb	-1(%r13), %cl
	subq	$1, %r12
	movb	%cl, -1(%rax)
	andl	$15, %r15d
	je	.L2
.L67:
	testq	%r12, %r12
	jne	.L69
.L2:
	cmpq	$15, %r12
	jbe	.L9
	leaq	-16(%r12), %rcx
	movq	%r13, %r15
	andq	$-16, %rcx
	addq	$16, %rcx
	movq	%rcx, -80(%rbp)
	addq	%rax, %rcx
	movq	%rcx, -72(%rbp)
	.p2align 4,,10
	.p2align 3
.L6:
	movq	%rax, -64(%rbp)
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movq	%rdx, -56(%rbp)
	call	*24(%rbp)
	movzbl	15(%rbx), %esi
	movl	%esi, %edi
	addl	$1, %esi
	addl	$1, %edi
	movb	%dil, 15(%rbx)
	movl	%esi, %edi
	movzbl	14(%rbx), %esi
	shrl	$8, %edi
	addl	%edi, %esi
	movb	%sil, 14(%rbx)
	movl	%esi, %edi
	movzbl	13(%rbx), %esi
	shrl	$8, %edi
	addl	%edi, %esi
	movb	%sil, 13(%rbx)
	movl	%esi, %edi
	movzbl	12(%rbx), %esi
	shrl	$8, %edi
	addl	%edi, %esi
	movb	%sil, 12(%rbx)
	movl	%esi, %edi
	movzbl	11(%rbx), %esi
	shrl	$8, %edi
	addl	%edi, %esi
	movb	%sil, 11(%rbx)
	movl	%esi, %edi
	movzbl	10(%rbx), %esi
	shrl	$8, %edi
	addl	%edi, %esi
	movb	%sil, 10(%rbx)
	movl	%esi, %edi
	movzbl	9(%rbx), %esi
	shrl	$8, %edi
	addl	%edi, %esi
	movb	%sil, 9(%rbx)
	movl	%esi, %edi
	movzbl	8(%rbx), %esi
	shrl	$8, %edi
	addl	%edi, %esi
	movb	%sil, 8(%rbx)
	movl	%esi, %edi
	movzbl	7(%rbx), %esi
	shrl	$8, %edi
	addl	%edi, %esi
	movb	%sil, 7(%rbx)
	movl	%esi, %edi
	movzbl	6(%rbx), %esi
	shrl	$8, %edi
	addl	%edi, %esi
	movb	%sil, 6(%rbx)
	movl	%esi, %edi
	movzbl	5(%rbx), %esi
	shrl	$8, %edi
	addl	%edi, %esi
	movb	%sil, 5(%rbx)
	movl	%esi, %edi
	movzbl	4(%rbx), %esi
	shrl	$8, %edi
	addl	%edi, %esi
	movb	%sil, 4(%rbx)
	movl	%esi, %edi
	movzbl	3(%rbx), %esi
	shrl	$8, %edi
	addl	%edi, %esi
	movb	%sil, 3(%rbx)
	movl	%esi, %edi
	movzbl	2(%rbx), %esi
	shrl	$8, %edi
	addl	%edi, %esi
	movb	%sil, 2(%rbx)
	movl	%esi, %edi
	movzbl	1(%rbx), %esi
	shrl	$8, %edi
	addl	%edi, %esi
	movb	%sil, 1(%rbx)
	shrl	$8, %esi
	addb	%sil, (%rbx)
	movq	(%r15), %rsi
	addq	$16, %r15
	movq	-64(%rbp), %rax
	xorq	(%r14), %rsi
	movq	-56(%rbp), %rdx
	movq	%rsi, (%rax)
	movq	-8(%r15), %rsi
	addq	$16, %rax
	xorq	8(%r14), %rsi
	movq	%rsi, -8(%rax)
	cmpq	-72(%rbp), %rax
	jne	.L6
	andl	$15, %r12d
	addq	-80(%rbp), %r13
	xorl	%r15d, %r15d
.L5:
	testq	%r12, %r12
	je	.L7
	movq	%rbx, %rdi
	movq	%r14, %rsi
	call	*24(%rbp)
	movzbl	15(%rbx), %eax
	movq	%r12, %rcx
	movl	%eax, %edx
	addl	$1, %eax
	addl	$1, %edx
	shrl	$8, %eax
	movb	%dl, 15(%rbx)
	movl	%eax, %edx
	movzbl	14(%rbx), %eax
	addl	%edx, %eax
	movb	%al, 14(%rbx)
	shrl	$8, %eax
	movl	%eax, %edx
	movzbl	13(%rbx), %eax
	addl	%edx, %eax
	movb	%al, 13(%rbx)
	shrl	$8, %eax
	movl	%eax, %edx
	movzbl	12(%rbx), %eax
	addl	%edx, %eax
	movb	%al, 12(%rbx)
	shrl	$8, %eax
	movl	%eax, %edx
	movzbl	11(%rbx), %eax
	addl	%edx, %eax
	movb	%al, 11(%rbx)
	shrl	$8, %eax
	movl	%eax, %edx
	movzbl	10(%rbx), %eax
	addl	%edx, %eax
	movb	%al, 10(%rbx)
	shrl	$8, %eax
	movl	%eax, %edx
	movzbl	9(%rbx), %eax
	addl	%edx, %eax
	movb	%al, 9(%rbx)
	shrl	$8, %eax
	movl	%eax, %edx
	movzbl	8(%rbx), %eax
	addl	%edx, %eax
	movb	%al, 8(%rbx)
	shrl	$8, %eax
	movl	%eax, %edx
	movzbl	7(%rbx), %eax
	addl	%edx, %eax
	movb	%al, 7(%rbx)
	shrl	$8, %eax
	movl	%eax, %edx
	movzbl	6(%rbx), %eax
	addl	%edx, %eax
	movb	%al, 6(%rbx)
	shrl	$8, %eax
	movl	%eax, %edx
	movzbl	5(%rbx), %eax
	addl	%edx, %eax
	movb	%al, 5(%rbx)
	shrl	$8, %eax
	movl	%eax, %edx
	movzbl	4(%rbx), %eax
	addl	%edx, %eax
	movb	%al, 4(%rbx)
	shrl	$8, %eax
	movl	%eax, %edx
	movzbl	3(%rbx), %eax
	addl	%edx, %eax
	movb	%al, 3(%rbx)
	shrl	$8, %eax
	movl	%eax, %edx
	movzbl	2(%rbx), %eax
	addl	%edx, %eax
	movb	%al, 2(%rbx)
	shrl	$8, %eax
	movl	%eax, %edx
	movzbl	1(%rbx), %eax
	addl	%edx, %eax
	movb	%al, 1(%rbx)
	shrl	$8, %eax
	addb	%al, (%rbx)
	movl	%r15d, %eax
	movzbl	(%r14,%rax), %edx
	movq	-72(%rbp), %rbx
	xorb	0(%r13,%rax), %dl
	movb	%dl, (%rbx,%rax)
	leal	1(%r15), %edx
	subq	$1, %rcx
	je	.L8
	movl	%edx, %eax
	movzbl	(%r14,%rax), %esi
	xorb	0(%r13,%rax), %sil
	movb	%sil, (%rbx,%rax)
	leal	2(%r15), %eax
	cmpq	$2, %r12
	je	.L8
	movzbl	(%r14,%rax), %esi
	xorb	0(%r13,%rax), %sil
	movb	%sil, (%rbx,%rax)
	leal	3(%r15), %eax
	cmpq	$3, %r12
	je	.L8
	movzbl	0(%r13,%rax), %esi
	xorb	(%r14,%rax), %sil
	movb	%sil, (%rbx,%rax)
	leal	4(%r15), %eax
	cmpq	$4, %r12
	je	.L8
	movzbl	0(%r13,%rax), %esi
	xorb	(%r14,%rax), %sil
	movb	%sil, (%rbx,%rax)
	leal	5(%r15), %eax
	cmpq	$5, %r12
	je	.L8
	movzbl	0(%r13,%rax), %esi
	xorb	(%r14,%rax), %sil
	movb	%sil, (%rbx,%rax)
	leal	6(%r15), %eax
	cmpq	$6, %r12
	je	.L8
	movzbl	0(%r13,%rax), %esi
	xorb	(%r14,%rax), %sil
	movb	%sil, (%rbx,%rax)
	leal	7(%r15), %eax
	cmpq	$7, %r12
	je	.L8
	movzbl	0(%r13,%rax), %esi
	xorb	(%r14,%rax), %sil
	movb	%sil, (%rbx,%rax)
	leal	8(%r15), %eax
	cmpq	$8, %r12
	je	.L8
	movzbl	0(%r13,%rax), %esi
	xorb	(%r14,%rax), %sil
	movb	%sil, (%rbx,%rax)
	leal	9(%r15), %eax
	cmpq	$9, %r12
	je	.L8
	movzbl	0(%r13,%rax), %esi
	xorb	(%r14,%rax), %sil
	movb	%sil, (%rbx,%rax)
	leal	10(%r15), %eax
	cmpq	$10, %r12
	je	.L8
	movzbl	0(%r13,%rax), %esi
	xorb	(%r14,%rax), %sil
	movb	%sil, (%rbx,%rax)
	leal	11(%r15), %eax
	cmpq	$11, %r12
	je	.L8
	movzbl	0(%r13,%rax), %esi
	xorb	(%r14,%rax), %sil
	movb	%sil, (%rbx,%rax)
	leal	12(%r15), %eax
	cmpq	$12, %r12
	je	.L8
	movzbl	0(%r13,%rax), %esi
	xorb	(%r14,%rax), %sil
	movb	%sil, (%rbx,%rax)
	leal	13(%r15), %eax
	cmpq	$13, %r12
	je	.L8
	movzbl	0(%r13,%rax), %esi
	xorb	(%r14,%rax), %sil
	movb	%sil, (%rbx,%rax)
	leal	14(%r15), %eax
	cmpq	$14, %r12
	je	.L8
	movzbl	0(%r13,%rax), %esi
	xorb	(%r14,%rax), %sil
	movb	%sil, (%rbx,%rax)
.L8:
	leal	(%rdx,%rcx), %r15d
.L7:
	movq	16(%rbp), %rax
	movl	%r15d, (%rax)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L9:
	.cfi_restore_state
	movq	%rax, -72(%rbp)
	jmp	.L5
	.cfi_endproc
.LFE153:
	.size	CRYPTO_ctr128_encrypt, .-CRYPTO_ctr128_encrypt
	.p2align 4
	.globl	CRYPTO_ctr128_encrypt_ctr32
	.type	CRYPTO_ctr128_encrypt_ctr32, @function
CRYPTO_ctr128_encrypt_ctr32:
.LFB155:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$40, %rsp
	movq	16(%rbp), %rax
	movq	%r9, -80(%rbp)
	movl	(%rax), %r15d
	testl	%r15d, %r15d
	je	.L71
	testq	%rdx, %rdx
	je	.L71
	movq	%r9, %rdx
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L143:
	testq	%r12, %r12
	je	.L71
.L72:
	movl	%r15d, %eax
	addq	$1, %r13
	addq	$1, %r14
	addl	$1, %r15d
	movzbl	(%rdx,%rax), %eax
	xorb	-1(%r13), %al
	subq	$1, %r12
	movb	%al, -1(%r14)
	andl	$15, %r15d
	jne	.L143
.L71:
	movl	12(%rbx), %r10d
#APP
# 163 "../deps/openssl/openssl/crypto/modes/ctr128.c" 1
	bswapl %r10d
# 0 "" 2
#NO_APP
	cmpq	$15, %r12
	jbe	.L74
	xorl	%eax, %eax
	movl	%r15d, -72(%rbp)
	movq	%rbx, %r8
	movl	%r10d, %r15d
#APP
# 186 "../deps/openssl/openssl/crypto/modes/ctr128.c" 1
	bswapl %eax
# 0 "" 2
#NO_APP
	movl	%eax, -68(%rbp)
	movq	%r14, %rax
	movq	%r13, %r14
	movq	%rcx, -64(%rbp)
	movq	%rax, %r13
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L145:
	movq	%r12, %rbx
	shrq	$4, %rbx
	movl	%ebx, %edx
	addl	%r15d, %edx
	movq	%rdx, %r15
	cmpq	%rbx, %rdx
	jnb	.L76
.L146:
	subq	%rdx, %rbx
	movq	%r8, -56(%rbp)
	movq	-64(%rbp), %rcx
	movq	%r13, %rsi
	movq	%rbx, %rdx
	movq	%r14, %rdi
	call	*24(%rbp)
	movl	-68(%rbp), %eax
	movq	-56(%rbp), %r8
	movl	%eax, 12(%r8)
.L77:
	movzbl	11(%r8), %edx
	xorl	%r15d, %r15d
	movl	%edx, %ecx
	addl	$1, %edx
	addl	$1, %ecx
	movb	%cl, 11(%r8)
	movl	%edx, %ecx
	movzbl	10(%r8), %edx
	shrl	$8, %ecx
	addl	%ecx, %edx
	movl	%edx, %ecx
	movb	%dl, 10(%r8)
	movzbl	9(%r8), %edx
	shrl	$8, %ecx
	addl	%ecx, %edx
	movl	%edx, %ecx
	movb	%dl, 9(%r8)
	movzbl	8(%r8), %edx
	shrl	$8, %ecx
	addl	%ecx, %edx
	movl	%edx, %ecx
	movb	%dl, 8(%r8)
	movzbl	7(%r8), %edx
	shrl	$8, %ecx
	addl	%ecx, %edx
	movl	%edx, %ecx
	movb	%dl, 7(%r8)
	movzbl	6(%r8), %edx
	shrl	$8, %ecx
	addl	%ecx, %edx
	movl	%edx, %ecx
	movb	%dl, 6(%r8)
	movzbl	5(%r8), %edx
	shrl	$8, %ecx
	addl	%ecx, %edx
	movl	%edx, %ecx
	movb	%dl, 5(%r8)
	movzbl	4(%r8), %edx
	shrl	$8, %ecx
	addl	%ecx, %edx
	movl	%edx, %ecx
	movb	%dl, 4(%r8)
	movzbl	3(%r8), %edx
	shrl	$8, %ecx
	addl	%ecx, %edx
	movl	%edx, %ecx
	movb	%dl, 3(%r8)
	movzbl	2(%r8), %edx
	shrl	$8, %ecx
	addl	%ecx, %edx
	movl	%edx, %ecx
	movb	%dl, 2(%r8)
	movzbl	1(%r8), %edx
	shrl	$8, %ecx
	addl	%ecx, %edx
	movb	%dl, 1(%r8)
	shrl	$8, %edx
	addb	%dl, (%r8)
.L78:
	salq	$4, %rbx
	subq	%rbx, %r12
	addq	%rbx, %r13
	addq	%rbx, %r14
	cmpq	$15, %r12
	jbe	.L144
.L79:
	movabsq	$4294967311, %rax
	cmpq	%rax, %r12
	jbe	.L145
	movl	$268435456, %edx
	movl	$268435456, %ebx
	addl	%r15d, %edx
	movq	%rdx, %r15
	cmpq	%rbx, %rdx
	jb	.L146
.L76:
	movq	%r8, -56(%rbp)
	movq	%rbx, %rdx
	movq	-64(%rbp), %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	*24(%rbp)
	movq	-56(%rbp), %r8
	movl	%r15d, %edx
#APP
# 186 "../deps/openssl/openssl/crypto/modes/ctr128.c" 1
	bswapl %edx
# 0 "" 2
#NO_APP
	movl	%edx, 12(%r8)
	testl	%r15d, %r15d
	jne	.L78
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L144:
	movq	%r13, %rax
	movl	%r15d, %r10d
	movq	-64(%rbp), %rcx
	movl	-72(%rbp), %r15d
	movq	%r14, %r13
	movq	%r8, %rbx
	movq	%rax, %r14
.L74:
	testq	%r12, %r12
	jne	.L147
.L80:
	movq	16(%rbp), %rax
	movl	%r15d, (%rax)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L147:
	.cfi_restore_state
	movq	-80(%rbp), %rdi
	pxor	%xmm0, %xmm0
	movl	%r10d, -56(%rbp)
	movq	%rbx, %r8
	movl	$1, %edx
	movups	%xmm0, (%rdi)
	movq	%rdi, %rsi
	call	*24(%rbp)
	movl	-56(%rbp), %r10d
	addl	$1, %r10d
	movl	%r10d, %edx
#APP
# 199 "../deps/openssl/openssl/crypto/modes/ctr128.c" 1
	bswapl %edx
# 0 "" 2
#NO_APP
	movl	%edx, 12(%rbx)
	testl	%r10d, %r10d
	je	.L81
.L83:
	movq	-80(%rbp), %rax
	movl	%r15d, %edx
	movq	%r12, %rdi
	leal	1(%r15), %esi
	movzbl	(%rax,%rdx), %ecx
	xorb	0(%r13,%rdx), %cl
	movb	%cl, (%r14,%rdx)
	subq	$1, %rdi
	je	.L82
	movl	%esi, %edx
	movzbl	(%rax,%rdx), %ecx
	xorb	0(%r13,%rdx), %cl
	movb	%cl, (%r14,%rdx)
	leal	2(%r15), %edx
	cmpq	$2, %r12
	je	.L82
	movzbl	(%rax,%rdx), %ecx
	xorb	0(%r13,%rdx), %cl
	movb	%cl, (%r14,%rdx)
	leal	3(%r15), %edx
	cmpq	$3, %r12
	je	.L82
	movzbl	(%rax,%rdx), %ecx
	xorb	0(%r13,%rdx), %cl
	movb	%cl, (%r14,%rdx)
	leal	4(%r15), %edx
	cmpq	$4, %r12
	je	.L82
	movzbl	(%rax,%rdx), %ecx
	xorb	0(%r13,%rdx), %cl
	movb	%cl, (%r14,%rdx)
	leal	5(%r15), %edx
	cmpq	$5, %r12
	je	.L82
	movzbl	(%rax,%rdx), %ecx
	xorb	0(%r13,%rdx), %cl
	movb	%cl, (%r14,%rdx)
	leal	6(%r15), %edx
	cmpq	$6, %r12
	je	.L82
	movzbl	(%rax,%rdx), %ecx
	xorb	0(%r13,%rdx), %cl
	movb	%cl, (%r14,%rdx)
	leal	7(%r15), %edx
	cmpq	$7, %r12
	je	.L82
	movzbl	(%rax,%rdx), %ecx
	xorb	0(%r13,%rdx), %cl
	movb	%cl, (%r14,%rdx)
	leal	8(%r15), %edx
	cmpq	$8, %r12
	je	.L82
	movzbl	(%rax,%rdx), %ecx
	xorb	0(%r13,%rdx), %cl
	movb	%cl, (%r14,%rdx)
	leal	9(%r15), %edx
	cmpq	$9, %r12
	je	.L82
	movzbl	(%rax,%rdx), %ecx
	xorb	0(%r13,%rdx), %cl
	movb	%cl, (%r14,%rdx)
	leal	10(%r15), %edx
	cmpq	$10, %r12
	je	.L82
	movzbl	(%rax,%rdx), %ecx
	xorb	0(%r13,%rdx), %cl
	movb	%cl, (%r14,%rdx)
	leal	11(%r15), %edx
	cmpq	$11, %r12
	je	.L82
	movzbl	(%rax,%rdx), %ecx
	xorb	0(%r13,%rdx), %cl
	movb	%cl, (%r14,%rdx)
	leal	12(%r15), %edx
	cmpq	$12, %r12
	je	.L82
	movzbl	0(%r13,%rdx), %ecx
	xorb	(%rax,%rdx), %cl
	movb	%cl, (%r14,%rdx)
	leal	13(%r15), %edx
	cmpq	$13, %r12
	je	.L82
	movzbl	0(%r13,%rdx), %ecx
	xorb	(%rax,%rdx), %cl
	movb	%cl, (%r14,%rdx)
	leal	14(%r15), %edx
	cmpq	$14, %r12
	je	.L82
	movzbl	0(%r13,%rdx), %ecx
	xorb	(%rax,%rdx), %cl
	movb	%cl, (%r14,%rdx)
	.p2align 4,,10
	.p2align 3
.L82:
	leal	(%rsi,%rdi), %r15d
	jmp	.L80
.L81:
	movzbl	11(%rbx), %edx
	movl	%edx, %ecx
	addl	$1, %edx
	addl	$1, %ecx
	movb	%cl, 11(%rbx)
	movl	%edx, %ecx
	movzbl	10(%rbx), %edx
	shrl	$8, %ecx
	addl	%ecx, %edx
	movl	%edx, %ecx
	movb	%dl, 10(%rbx)
	movzbl	9(%rbx), %edx
	shrl	$8, %ecx
	addl	%ecx, %edx
	movl	%edx, %ecx
	movb	%dl, 9(%rbx)
	movzbl	8(%rbx), %edx
	shrl	$8, %ecx
	addl	%ecx, %edx
	movl	%edx, %ecx
	movb	%dl, 8(%rbx)
	movzbl	7(%rbx), %edx
	shrl	$8, %ecx
	addl	%ecx, %edx
	movl	%edx, %ecx
	movb	%dl, 7(%rbx)
	movzbl	6(%rbx), %edx
	shrl	$8, %ecx
	addl	%ecx, %edx
	movl	%edx, %ecx
	movb	%dl, 6(%rbx)
	movzbl	5(%rbx), %edx
	shrl	$8, %ecx
	addl	%ecx, %edx
	movl	%edx, %ecx
	movb	%dl, 5(%rbx)
	movzbl	4(%rbx), %edx
	shrl	$8, %ecx
	addl	%ecx, %edx
	movl	%edx, %ecx
	movb	%dl, 4(%rbx)
	movzbl	3(%rbx), %edx
	shrl	$8, %ecx
	addl	%ecx, %edx
	movl	%edx, %ecx
	movb	%dl, 3(%rbx)
	movzbl	2(%rbx), %edx
	shrl	$8, %ecx
	addl	%ecx, %edx
	movl	%edx, %ecx
	movb	%dl, 2(%rbx)
	movzbl	1(%rbx), %edx
	shrl	$8, %ecx
	addl	%ecx, %edx
	movb	%dl, 1(%rbx)
	shrl	$8, %edx
	addb	%dl, (%rbx)
	jmp	.L83
	.cfi_endproc
.LFE155:
	.size	CRYPTO_ctr128_encrypt_ctr32, .-CRYPTO_ctr128_encrypt_ctr32
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
