	.file	"ts_rsp_utils.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/ts/ts_rsp_utils.c"
	.text
	.p2align 4
	.globl	TS_RESP_set_status_info
	.type	TS_RESP_set_status_info, @function
TS_RESP_set_status_info:
.LFB1368:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	$1, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpq	%rsi, (%rdi)
	je	.L1
	movq	%rsi, %rdi
	call	TS_STATUS_INFO_dup@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L7
	movq	(%rbx), %rdi
	call	TS_STATUS_INFO_free@PLT
	movq	%r12, (%rbx)
.L1:
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	.cfi_restore_state
	movl	$25, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r13d, %r13d
	movl	$135, %esi
	movl	$47, %edi
	call	ERR_put_error@PLT
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1368:
	.size	TS_RESP_set_status_info, .-TS_RESP_set_status_info
	.p2align 4
	.globl	TS_RESP_get_status_info
	.type	TS_RESP_get_status_info, @function
TS_RESP_get_status_info:
.LFB1369:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE1369:
	.size	TS_RESP_get_status_info, .-TS_RESP_get_status_info
	.p2align 4
	.globl	TS_RESP_set_tst_info
	.type	TS_RESP_set_tst_info, @function
TS_RESP_set_tst_info:
.LFB1370:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rdi
	call	PKCS7_free@PLT
	movq	%r13, 8(%rbx)
	movq	16(%rbx), %rdi
	call	TS_TST_INFO_free@PLT
	movq	%r12, 16(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1370:
	.size	TS_RESP_set_tst_info, .-TS_RESP_set_tst_info
	.p2align 4
	.globl	TS_RESP_get_token
	.type	TS_RESP_get_token, @function
TS_RESP_get_token:
.LFB1371:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	ret
	.cfi_endproc
.LFE1371:
	.size	TS_RESP_get_token, .-TS_RESP_get_token
	.p2align 4
	.globl	TS_RESP_get_tst_info
	.type	TS_RESP_get_tst_info, @function
TS_RESP_get_tst_info:
.LFB1372:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	ret
	.cfi_endproc
.LFE1372:
	.size	TS_RESP_get_tst_info, .-TS_RESP_get_tst_info
	.p2align 4
	.globl	TS_TST_INFO_set_version
	.type	TS_TST_INFO_set_version, @function
TS_TST_INFO_set_version:
.LFB1373:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdi
	jmp	ASN1_INTEGER_set@PLT
	.cfi_endproc
.LFE1373:
	.size	TS_TST_INFO_set_version, .-TS_TST_INFO_set_version
	.p2align 4
	.globl	TS_TST_INFO_get_version
	.type	TS_TST_INFO_get_version, @function
TS_TST_INFO_get_version:
.LFB1374:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdi
	jmp	ASN1_INTEGER_get@PLT
	.cfi_endproc
.LFE1374:
	.size	TS_TST_INFO_get_version, .-TS_TST_INFO_get_version
	.p2align 4
	.globl	TS_TST_INFO_set_policy_id
	.type	TS_TST_INFO_set_policy_id, @function
TS_TST_INFO_set_policy_id:
.LFB1375:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	$1, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpq	%rsi, 8(%rdi)
	je	.L15
	movq	%rsi, %rdi
	call	OBJ_dup@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L20
	movq	8(%rbx), %rdi
	call	ASN1_OBJECT_free@PLT
	movq	%r12, 8(%rbx)
.L15:
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	.cfi_restore_state
	movl	$76, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r13d, %r13d
	movl	$140, %esi
	movl	$47, %edi
	call	ERR_put_error@PLT
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1375:
	.size	TS_TST_INFO_set_policy_id, .-TS_TST_INFO_set_policy_id
	.p2align 4
	.globl	TS_TST_INFO_get_policy_id
	.type	TS_TST_INFO_get_policy_id, @function
TS_TST_INFO_get_policy_id:
.LFB1376:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	ret
	.cfi_endproc
.LFE1376:
	.size	TS_TST_INFO_get_policy_id, .-TS_TST_INFO_get_policy_id
	.p2align 4
	.globl	TS_TST_INFO_set_msg_imprint
	.type	TS_TST_INFO_set_msg_imprint, @function
TS_TST_INFO_set_msg_imprint:
.LFB1377:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	$1, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpq	%rsi, 16(%rdi)
	je	.L22
	movq	%rsi, %rdi
	call	TS_MSG_IMPRINT_dup@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L27
	movq	16(%rbx), %rdi
	call	TS_MSG_IMPRINT_free@PLT
	movq	%r12, 16(%rbx)
.L22:
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L27:
	.cfi_restore_state
	movl	$97, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r13d, %r13d
	movl	$138, %esi
	movl	$47, %edi
	call	ERR_put_error@PLT
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1377:
	.size	TS_TST_INFO_set_msg_imprint, .-TS_TST_INFO_set_msg_imprint
	.p2align 4
	.globl	TS_TST_INFO_get_msg_imprint
	.type	TS_TST_INFO_get_msg_imprint, @function
TS_TST_INFO_get_msg_imprint:
.LFB1378:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	ret
	.cfi_endproc
.LFE1378:
	.size	TS_TST_INFO_get_msg_imprint, .-TS_TST_INFO_get_msg_imprint
	.p2align 4
	.globl	TS_TST_INFO_set_serial
	.type	TS_TST_INFO_set_serial, @function
TS_TST_INFO_set_serial:
.LFB1379:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	$1, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpq	%rsi, 24(%rdi)
	je	.L29
	movq	%rsi, %rdi
	call	ASN1_INTEGER_dup@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L34
	movq	24(%rbx), %rdi
	call	ASN1_INTEGER_free@PLT
	movq	%r12, 24(%rbx)
.L29:
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L34:
	.cfi_restore_state
	movl	$118, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r13d, %r13d
	movl	$141, %esi
	movl	$47, %edi
	call	ERR_put_error@PLT
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1379:
	.size	TS_TST_INFO_set_serial, .-TS_TST_INFO_set_serial
	.p2align 4
	.globl	TS_TST_INFO_get_serial
	.type	TS_TST_INFO_get_serial, @function
TS_TST_INFO_get_serial:
.LFB1380:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	ret
	.cfi_endproc
.LFE1380:
	.size	TS_TST_INFO_get_serial, .-TS_TST_INFO_get_serial
	.p2align 4
	.globl	TS_TST_INFO_set_time
	.type	TS_TST_INFO_set_time, @function
TS_TST_INFO_set_time:
.LFB1381:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	$1, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpq	%rsi, 32(%rdi)
	je	.L36
	movq	%rsi, %rdi
	call	ASN1_STRING_dup@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L41
	movq	32(%rbx), %rdi
	call	ASN1_GENERALIZEDTIME_free@PLT
	movq	%r12, 32(%rbx)
.L36:
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L41:
	.cfi_restore_state
	movl	$139, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r13d, %r13d
	movl	$142, %esi
	movl	$47, %edi
	call	ERR_put_error@PLT
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1381:
	.size	TS_TST_INFO_set_time, .-TS_TST_INFO_set_time
	.p2align 4
	.globl	TS_TST_INFO_get_time
	.type	TS_TST_INFO_get_time, @function
TS_TST_INFO_get_time:
.LFB1382:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	ret
	.cfi_endproc
.LFE1382:
	.size	TS_TST_INFO_get_time, .-TS_TST_INFO_get_time
	.p2align 4
	.globl	TS_TST_INFO_set_accuracy
	.type	TS_TST_INFO_set_accuracy, @function
TS_TST_INFO_set_accuracy:
.LFB1383:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	$1, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpq	%rsi, 40(%rdi)
	je	.L43
	movq	%rsi, %rdi
	call	TS_ACCURACY_dup@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L48
	movq	40(%rbx), %rdi
	call	TS_ACCURACY_free@PLT
	movq	%r12, 40(%rbx)
.L43:
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L48:
	.cfi_restore_state
	movl	$160, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r13d, %r13d
	movl	$137, %esi
	movl	$47, %edi
	call	ERR_put_error@PLT
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1383:
	.size	TS_TST_INFO_set_accuracy, .-TS_TST_INFO_set_accuracy
	.p2align 4
	.globl	TS_TST_INFO_get_accuracy
	.type	TS_TST_INFO_get_accuracy, @function
TS_TST_INFO_get_accuracy:
.LFB1384:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	ret
	.cfi_endproc
.LFE1384:
	.size	TS_TST_INFO_get_accuracy, .-TS_TST_INFO_get_accuracy
	.p2align 4
	.globl	TS_ACCURACY_set_seconds
	.type	TS_ACCURACY_set_seconds, @function
TS_ACCURACY_set_seconds:
.LFB1385:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	$1, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpq	%rsi, (%rdi)
	je	.L50
	movq	%rsi, %rdi
	call	ASN1_INTEGER_dup@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L55
	movq	(%rbx), %rdi
	call	ASN1_INTEGER_free@PLT
	movq	%r12, (%rbx)
.L50:
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L55:
	.cfi_restore_state
	movl	$181, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r13d, %r13d
	movl	$117, %esi
	movl	$47, %edi
	call	ERR_put_error@PLT
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1385:
	.size	TS_ACCURACY_set_seconds, .-TS_ACCURACY_set_seconds
	.p2align 4
	.globl	TS_ACCURACY_get_seconds
	.type	TS_ACCURACY_get_seconds, @function
TS_ACCURACY_get_seconds:
.LFB1386:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE1386:
	.size	TS_ACCURACY_get_seconds, .-TS_ACCURACY_get_seconds
	.p2align 4
	.globl	TS_ACCURACY_set_millis
	.type	TS_ACCURACY_set_millis, @function
TS_ACCURACY_set_millis:
.LFB1387:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	8(%rdi), %rdi
	cmpq	%rsi, %rdi
	je	.L61
	movq	%rsi, %r12
	testq	%rsi, %rsi
	je	.L59
	movq	%rsi, %rdi
	call	ASN1_INTEGER_dup@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L60
	movq	8(%rbx), %rdi
.L59:
	call	ASN1_INTEGER_free@PLT
	movq	%r12, 8(%rbx)
	movl	$1, %eax
.L57:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L61:
	.cfi_restore_state
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L60:
	.cfi_restore_state
	movl	$203, %r8d
	movl	$65, %edx
	movl	$116, %esi
	movl	$47, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L57
	.cfi_endproc
.LFE1387:
	.size	TS_ACCURACY_set_millis, .-TS_ACCURACY_set_millis
	.p2align 4
	.globl	TS_ACCURACY_get_millis
	.type	TS_ACCURACY_get_millis, @function
TS_ACCURACY_get_millis:
.LFB1388:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	ret
	.cfi_endproc
.LFE1388:
	.size	TS_ACCURACY_get_millis, .-TS_ACCURACY_get_millis
	.p2align 4
	.globl	TS_ACCURACY_set_micros
	.type	TS_ACCURACY_set_micros, @function
TS_ACCURACY_set_micros:
.LFB1389:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	16(%rdi), %rdi
	cmpq	%rsi, %rdi
	je	.L71
	movq	%rsi, %r12
	testq	%rsi, %rsi
	je	.L69
	movq	%rsi, %rdi
	call	ASN1_INTEGER_dup@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L70
	movq	16(%rbx), %rdi
.L69:
	call	ASN1_INTEGER_free@PLT
	movq	%r12, 16(%rbx)
	movl	$1, %eax
.L67:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L71:
	.cfi_restore_state
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L70:
	.cfi_restore_state
	movl	$226, %r8d
	movl	$65, %edx
	movl	$115, %esi
	movl	$47, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L67
	.cfi_endproc
.LFE1389:
	.size	TS_ACCURACY_set_micros, .-TS_ACCURACY_set_micros
	.p2align 4
	.globl	TS_ACCURACY_get_micros
	.type	TS_ACCURACY_get_micros, @function
TS_ACCURACY_get_micros:
.LFB1390:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	ret
	.cfi_endproc
.LFE1390:
	.size	TS_ACCURACY_get_micros, .-TS_ACCURACY_get_micros
	.p2align 4
	.globl	TS_TST_INFO_set_ordering
	.type	TS_TST_INFO_set_ordering, @function
TS_TST_INFO_set_ordering:
.LFB1391:
	.cfi_startproc
	endbr64
	testl	%esi, %esi
	movl	$255, %eax
	cmovne	%eax, %esi
	movl	$1, %eax
	movl	%esi, 48(%rdi)
	ret
	.cfi_endproc
.LFE1391:
	.size	TS_TST_INFO_set_ordering, .-TS_TST_INFO_set_ordering
	.p2align 4
	.globl	TS_TST_INFO_get_ordering
	.type	TS_TST_INFO_get_ordering, @function
TS_TST_INFO_get_ordering:
.LFB1392:
	.cfi_startproc
	endbr64
	movl	48(%rdi), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	setne	%al
	ret
	.cfi_endproc
.LFE1392:
	.size	TS_TST_INFO_get_ordering, .-TS_TST_INFO_get_ordering
	.p2align 4
	.globl	TS_TST_INFO_set_nonce
	.type	TS_TST_INFO_set_nonce, @function
TS_TST_INFO_set_nonce:
.LFB1393:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	$1, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpq	%rsi, 56(%rdi)
	je	.L81
	movq	%rsi, %rdi
	call	ASN1_INTEGER_dup@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L86
	movq	56(%rbx), %rdi
	call	ASN1_INTEGER_free@PLT
	movq	%r12, 56(%rbx)
.L81:
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L86:
	.cfi_restore_state
	movl	$259, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r13d, %r13d
	movl	$139, %esi
	movl	$47, %edi
	call	ERR_put_error@PLT
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1393:
	.size	TS_TST_INFO_set_nonce, .-TS_TST_INFO_set_nonce
	.p2align 4
	.globl	TS_TST_INFO_get_nonce
	.type	TS_TST_INFO_get_nonce, @function
TS_TST_INFO_get_nonce:
.LFB1394:
	.cfi_startproc
	endbr64
	movq	56(%rdi), %rax
	ret
	.cfi_endproc
.LFE1394:
	.size	TS_TST_INFO_get_nonce, .-TS_TST_INFO_get_nonce
	.p2align 4
	.globl	TS_TST_INFO_set_tsa
	.type	TS_TST_INFO_set_tsa, @function
TS_TST_INFO_set_tsa:
.LFB1395:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	$1, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpq	%rsi, 64(%rdi)
	je	.L88
	movq	%rsi, %rdi
	call	GENERAL_NAME_dup@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L93
	movq	64(%rbx), %rdi
	call	GENERAL_NAME_free@PLT
	movq	%r12, 64(%rbx)
.L88:
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L93:
	.cfi_restore_state
	movl	$280, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r13d, %r13d
	movl	$143, %esi
	movl	$47, %edi
	call	ERR_put_error@PLT
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1395:
	.size	TS_TST_INFO_set_tsa, .-TS_TST_INFO_set_tsa
	.p2align 4
	.globl	TS_TST_INFO_get_tsa
	.type	TS_TST_INFO_get_tsa, @function
TS_TST_INFO_get_tsa:
.LFB1396:
	.cfi_startproc
	endbr64
	movq	64(%rdi), %rax
	ret
	.cfi_endproc
.LFE1396:
	.size	TS_TST_INFO_get_tsa, .-TS_TST_INFO_get_tsa
	.p2align 4
	.globl	TS_TST_INFO_get_exts
	.type	TS_TST_INFO_get_exts, @function
TS_TST_INFO_get_exts:
.LFB1397:
	.cfi_startproc
	endbr64
	movq	72(%rdi), %rax
	ret
	.cfi_endproc
.LFE1397:
	.size	TS_TST_INFO_get_exts, .-TS_TST_INFO_get_exts
	.p2align 4
	.globl	TS_TST_INFO_ext_free
	.type	TS_TST_INFO_ext_free, @function
TS_TST_INFO_ext_free:
.LFB1398:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L102
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	72(%rdi), %rdi
	movq	X509_EXTENSION_free@GOTPCREL(%rip), %rsi
	call	OPENSSL_sk_pop_free@PLT
	movq	$0, 72(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L102:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.cfi_endproc
.LFE1398:
	.size	TS_TST_INFO_ext_free, .-TS_TST_INFO_ext_free
	.p2align 4
	.globl	TS_TST_INFO_get_ext_count
	.type	TS_TST_INFO_get_ext_count, @function
TS_TST_INFO_get_ext_count:
.LFB1399:
	.cfi_startproc
	endbr64
	movq	72(%rdi), %rdi
	jmp	X509v3_get_ext_count@PLT
	.cfi_endproc
.LFE1399:
	.size	TS_TST_INFO_get_ext_count, .-TS_TST_INFO_get_ext_count
	.p2align 4
	.globl	TS_TST_INFO_get_ext_by_NID
	.type	TS_TST_INFO_get_ext_by_NID, @function
TS_TST_INFO_get_ext_by_NID:
.LFB1400:
	.cfi_startproc
	endbr64
	movq	72(%rdi), %rdi
	jmp	X509v3_get_ext_by_NID@PLT
	.cfi_endproc
.LFE1400:
	.size	TS_TST_INFO_get_ext_by_NID, .-TS_TST_INFO_get_ext_by_NID
	.p2align 4
	.globl	TS_TST_INFO_get_ext_by_OBJ
	.type	TS_TST_INFO_get_ext_by_OBJ, @function
TS_TST_INFO_get_ext_by_OBJ:
.LFB1401:
	.cfi_startproc
	endbr64
	movq	72(%rdi), %rdi
	jmp	X509v3_get_ext_by_OBJ@PLT
	.cfi_endproc
.LFE1401:
	.size	TS_TST_INFO_get_ext_by_OBJ, .-TS_TST_INFO_get_ext_by_OBJ
	.p2align 4
	.globl	TS_TST_INFO_get_ext_by_critical
	.type	TS_TST_INFO_get_ext_by_critical, @function
TS_TST_INFO_get_ext_by_critical:
.LFB1402:
	.cfi_startproc
	endbr64
	movq	72(%rdi), %rdi
	jmp	X509v3_get_ext_by_critical@PLT
	.cfi_endproc
.LFE1402:
	.size	TS_TST_INFO_get_ext_by_critical, .-TS_TST_INFO_get_ext_by_critical
	.p2align 4
	.globl	TS_TST_INFO_get_ext
	.type	TS_TST_INFO_get_ext, @function
TS_TST_INFO_get_ext:
.LFB1403:
	.cfi_startproc
	endbr64
	movq	72(%rdi), %rdi
	jmp	X509v3_get_ext@PLT
	.cfi_endproc
.LFE1403:
	.size	TS_TST_INFO_get_ext, .-TS_TST_INFO_get_ext
	.p2align 4
	.globl	TS_TST_INFO_delete_ext
	.type	TS_TST_INFO_delete_ext, @function
TS_TST_INFO_delete_ext:
.LFB1404:
	.cfi_startproc
	endbr64
	movq	72(%rdi), %rdi
	jmp	X509v3_delete_ext@PLT
	.cfi_endproc
.LFE1404:
	.size	TS_TST_INFO_delete_ext, .-TS_TST_INFO_delete_ext
	.p2align 4
	.globl	TS_TST_INFO_add_ext
	.type	TS_TST_INFO_add_ext, @function
TS_TST_INFO_add_ext:
.LFB1405:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$72, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	X509v3_add_ext@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	testq	%rax, %rax
	setne	%al
	movzbl	%al, %eax
	ret
	.cfi_endproc
.LFE1405:
	.size	TS_TST_INFO_add_ext, .-TS_TST_INFO_add_ext
	.p2align 4
	.globl	TS_TST_INFO_get_ext_d2i
	.type	TS_TST_INFO_get_ext_d2i, @function
TS_TST_INFO_get_ext_d2i:
.LFB1406:
	.cfi_startproc
	endbr64
	movq	72(%rdi), %rdi
	jmp	X509V3_get_d2i@PLT
	.cfi_endproc
.LFE1406:
	.size	TS_TST_INFO_get_ext_d2i, .-TS_TST_INFO_get_ext_d2i
	.p2align 4
	.globl	TS_STATUS_INFO_set_status
	.type	TS_STATUS_INFO_set_status, @function
TS_STATUS_INFO_set_status:
.LFB1407:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdi
	movslq	%esi, %rsi
	jmp	ASN1_INTEGER_set@PLT
	.cfi_endproc
.LFE1407:
	.size	TS_STATUS_INFO_set_status, .-TS_STATUS_INFO_set_status
	.p2align 4
	.globl	TS_STATUS_INFO_get0_status
	.type	TS_STATUS_INFO_get0_status, @function
TS_STATUS_INFO_get0_status:
.LFB1408:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE1408:
	.size	TS_STATUS_INFO_get0_status, .-TS_STATUS_INFO_get0_status
	.p2align 4
	.globl	TS_STATUS_INFO_get0_text
	.type	TS_STATUS_INFO_get0_text, @function
TS_STATUS_INFO_get0_text:
.LFB1409:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	ret
	.cfi_endproc
.LFE1409:
	.size	TS_STATUS_INFO_get0_text, .-TS_STATUS_INFO_get0_text
	.p2align 4
	.globl	TS_STATUS_INFO_get0_failure_info
	.type	TS_STATUS_INFO_get0_failure_info, @function
TS_STATUS_INFO_get0_failure_info:
.LFB1410:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	ret
	.cfi_endproc
.LFE1410:
	.size	TS_STATUS_INFO_get0_failure_info, .-TS_STATUS_INFO_get0_failure_info
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
