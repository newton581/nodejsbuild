	.file	"statem_clnt.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/ssl/statem/statem_clnt.c"
	.align 8
.LC1:
	.string	"No ciphers enabled for max supported SSL/TLS version"
	.text
	.p2align 4
	.globl	tls_construct_client_hello
	.type	tls_construct_client_hello, @function
tls_construct_client_hello:
.LFB1037:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	1296(%rdi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	ssl_set_client_hello_version@PLT
	movl	$1112, %r9d
	leaq	.LC0(%rip), %r8
	movl	%eax, %r12d
	movl	%eax, %ecx
	testl	%eax, %eax
	jne	.L120
	testq	%r14, %r14
	je	.L7
	movl	(%r14), %esi
	xorl	%edx, %edx
	movq	%r15, %rdi
	call	ssl_version_supported@PLT
	testl	%eax, %eax
	jne	.L129
.L7:
	movl	1248(%r15), %edi
	testl	%edi, %edi
	je	.L130
	movq	8(%r15), %rax
	movq	192(%rax), %rax
	testb	$8, 96(%rax)
	je	.L11
	movq	168(%r15), %rax
	leaq	184(%rax), %r9
.L8:
	movq	%r9, %rax
	leaq	32(%r9), %rdx
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L131:
	addq	$1, %rax
	cmpq	%rax, %rdx
	je	.L13
.L12:
	cmpb	$0, (%rax)
	je	.L131
.L11:
	movl	1524(%r15), %esi
	movl	$2, %edx
	movq	%r13, %rdi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L15
	movq	168(%r15), %rax
	movl	$32, %edx
	movq	%r13, %rdi
	leaq	184(%rax), %rsi
	call	WPACKET_memcpy@PLT
	testl	%eax, %eax
	je	.L15
	movl	60(%r15), %ecx
	movl	(%r15), %eax
	testl	%ecx, %ecx
	je	.L132
.L16:
	cmpl	$772, %eax
	je	.L18
.L21:
	movl	$1, %esi
	movq	%r13, %rdi
	call	WPACKET_start_sub_packet_len__@PLT
	testl	%eax, %eax
	je	.L19
.L26:
	movq	%r13, %rdi
	call	WPACKET_close@PLT
	testl	%eax, %eax
	je	.L19
	movq	8(%r15), %rax
	movq	192(%rax), %rax
	testb	$8, 96(%rax)
	je	.L27
	movq	176(%r15), %rsi
	movq	256(%rsi), %rdx
	cmpq	$256, %rdx
	ja	.L29
	movl	$1, %ecx
	movq	%r13, %rdi
	call	WPACKET_sub_memcpy__@PLT
	testl	%eax, %eax
	je	.L29
.L27:
	movl	$2, %esi
	movq	%r13, %rdi
	call	WPACKET_start_sub_packet_len__@PLT
	testl	%eax, %eax
	je	.L133
	movq	%r15, %rdi
	call	SSL_get_ciphers@PLT
	movq	%r15, %rdi
	movq	%rax, %r14
	movl	1928(%r15), %eax
	movl	%eax, -100(%rbp)
	call	ssl_set_client_disabled@PLT
	movl	$3731, %r9d
	movl	$191, %ecx
	leaq	.LC0(%rip), %r8
	testl	%eax, %eax
	je	.L125
	testq	%r14, %r14
	je	.L134
	movl	-100(%rbp), %ecx
	movl	%r12d, -104(%rbp)
	movq	$0, -80(%rbp)
	cmpl	$1, %ecx
	movq	%r13, -88(%rbp)
	sbbq	%rax, %rax
	andq	$-2, %rax
	addq	$65532, %rax
	cmpl	$1, %ecx
	sbbq	%rbx, %rbx
	andq	$-2, %rbx
	addq	$65534, %rbx
	testb	$-128, 1496(%r15)
	cmove	%rbx, %rax
	xorl	%ecx, %ecx
	xorl	%ebx, %ebx
	movq	%rcx, %r12
	movq	%rax, -72(%rbp)
	leaq	-64(%rbp), %rax
	movq	%rax, -96(%rbp)
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L136:
	movl	56(%r13), %edx
	cmpl	$256, %edx
	je	.L40
	cmpl	$256, %eax
	movl	$65280, %ecx
	cmove	%ecx, %eax
.L41:
	cmpl	%edx, %eax
	jge	.L55
	.p2align 4,,10
	.p2align 3
.L38:
	addq	-64(%rbp), %r12
.L36:
	addl	$1, %ebx
.L35:
	movq	%r14, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jge	.L117
	cmpq	-72(%rbp), %r12
	jnb	.L117
	movl	%ebx, %esi
	movq	%r14, %rdi
	call	OPENSSL_sk_value@PLT
	xorl	%ecx, %ecx
	movl	$65537, %edx
	movq	%r15, %rdi
	movq	%rax, %rsi
	movq	%rax, %r13
	call	ssl_cipher_disabled@PLT
	testl	%eax, %eax
	jne	.L36
	movq	8(%r15), %rax
	movq	-96(%rbp), %rdx
	movq	%r13, %rdi
	movq	-88(%rbp), %rsi
	call	*152(%rax)
	testl	%eax, %eax
	je	.L135
	cmpq	$0, -80(%rbp)
	jne	.L38
	movq	8(%r15), %rdx
	movq	168(%r15), %rax
	movq	192(%rdx), %rdx
	movl	832(%rax), %eax
	testb	$8, 96(%rdx)
	jne	.L136
	cmpl	%eax, 48(%r13)
	jl	.L38
	cmpl	%eax, 44(%r13)
	setle	%al
	movzbl	%al, %eax
	movq	%rax, -80(%rbp)
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L130:
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	ssl_get_new_session@PLT
	testl	%eax, %eax
	je	.L121
.L10:
	movq	168(%r15), %rax
	leaq	184(%rax), %r9
	movq	8(%r15), %rax
	movq	192(%rax), %rax
	testb	$8, 96(%rax)
	jne	.L8
	movl	1248(%r15), %esi
	testl	%esi, %esi
	jne	.L11
.L13:
	movq	%r9, %rdx
	xorl	%r8d, %r8d
	movl	$32, %ecx
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	ssl_fill_hello_random@PLT
	movl	$1149, %r9d
	testl	%eax, %eax
	jg	.L11
	.p2align 4,,10
	.p2align 3
.L123:
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movl	$487, %edx
	movq	%r15, %rdi
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L1
.L133:
	movl	$1242, %r9d
.L122:
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	.p2align 4,,10
	.p2align 3
.L120:
	movl	$487, %edx
	movl	$80, %esi
	movq	%r15, %rdi
	call	ossl_statem_fatal@PLT
.L121:
	xorl	%r12d, %r12d
.L1:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L137
	addq	$72, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	.cfi_restore_state
	movl	$1189, %r9d
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L132:
	movq	1296(%r15), %rdx
	cmpl	$772, (%rdx)
	je	.L16
	movq	336(%rdx), %r14
	leaq	344(%rdx), %rbx
	cmpl	$772, %eax
	je	.L138
.L25:
	movl	$1, %esi
	movq	%r13, %rdi
	call	WPACKET_start_sub_packet_len__@PLT
	testl	%eax, %eax
	je	.L19
	testq	%r14, %r14
	je	.L26
.L23:
	movq	%r14, %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	WPACKET_memcpy@PLT
	testl	%eax, %eax
	jne	.L26
.L19:
	movl	$1224, %r9d
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L129:
	movq	%r14, %rdi
	call	SSL_SESSION_is_resumable@PLT
	testl	%eax, %eax
	je	.L7
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L18:
	testb	$16, 1494(%r15)
	je	.L21
	movl	1248(%r15), %edx
	leaq	1336(%r15), %rbx
	movq	$32, 1368(%r15)
	testl	%edx, %edx
	je	.L22
.L127:
	movl	$1, %esi
	movq	%r13, %rdi
	call	WPACKET_start_sub_packet_len__@PLT
	testl	%eax, %eax
	je	.L19
	movl	$32, %r14d
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L29:
	movl	$1234, %r9d
	jmp	.L123
.L139:
	movl	$3820, %r9d
.L124:
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
.L125:
	movl	$425, %edx
	movl	$80, %esi
	movq	%r15, %rdi
	xorl	%r12d, %r12d
	call	ossl_statem_fatal@PLT
	jmp	.L1
.L40:
	movl	$65280, %edx
	cmpl	$256, %eax
	jne	.L41
	movl	$65280, %eax
.L55:
	movl	52(%r13), %edx
	movl	$65280, %ecx
	cmpl	$256, %edx
	cmove	%ecx, %edx
	cmpl	%eax, %edx
	setge	%al
	movzbl	%al, %eax
	movq	%rax, -80(%rbp)
	jmp	.L38
.L117:
	movq	%r12, %rax
	movq	-88(%rbp), %r13
	movl	-104(%rbp), %r12d
	testq	%rax, %rax
	je	.L53
	cmpq	$0, -80(%rbp)
	je	.L53
	movl	-100(%rbp), %eax
	testl	%eax, %eax
	jne	.L47
	movq	8(%r15), %rax
	leaq	-64(%rbp), %rdx
	movq	%r13, %rsi
	leaq	scsv.25347(%rip), %rdi
	call	*152(%rax)
	movl	$3810, %r9d
	testl	%eax, %eax
	je	.L124
.L47:
	testb	$-128, 1496(%r15)
	je	.L48
	movq	8(%r15), %rax
	leaq	-64(%rbp), %rdx
	movq	%r13, %rsi
	leaq	scsv.25348(%rip), %rdi
	call	*152(%rax)
	testl	%eax, %eax
	je	.L139
.L48:
	movq	%r13, %rdi
	call	WPACKET_close@PLT
	movl	$1252, %r9d
	testl	%eax, %eax
	je	.L122
	movl	$1, %esi
	movq	%r13, %rdi
	call	WPACKET_start_sub_packet_len__@PLT
	movl	$1259, %r9d
	testl	%eax, %eax
	je	.L122
	xorl	%esi, %esi
	movl	$1, %edx
	movq	%r13, %rdi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	je	.L52
	movq	%r13, %rdi
	call	WPACKET_close@PLT
	testl	%eax, %eax
	je	.L52
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$128, %edx
	movq	%r13, %rsi
	movq	%r15, %rdi
	xorl	%r12d, %r12d
	call	tls_construct_extensions@PLT
	testl	%eax, %eax
	setne	%r12b
	jmp	.L1
.L138:
	movq	%r14, 1368(%r15)
	leaq	1336(%r15), %rdi
	movq	%r14, %rdx
	movq	%rbx, %rsi
	call	memcpy@PLT
	jmp	.L25
.L22:
	movl	$32, %esi
	movq	%rbx, %rdi
	call	RAND_bytes@PLT
	testl	%eax, %eax
	jg	.L127
	movl	$1204, %r9d
	jmp	.L123
.L135:
	movl	$3772, %r9d
	jmp	.L124
.L52:
	movl	$1281, %r9d
	jmp	.L123
.L53:
	movl	$3794, %r9d
	leaq	.LC0(%rip), %r8
	movl	$181, %ecx
	movq	%r15, %rdi
	movl	$425, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	cmpq	$0, -80(%rbp)
	jne	.L1
	leaq	.LC1(%rip), %rsi
	movl	$1, %edi
	xorl	%eax, %eax
	call	ERR_add_error_data@PLT
	jmp	.L1
.L134:
	movl	$3737, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movq	%r15, %rdi
	movl	$425, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L1
.L137:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1037:
	.size	tls_construct_client_hello, .-tls_construct_client_hello
	.p2align 4
	.type	set_client_ciphersuite, @function
set_client_ciphersuite:
.LFB1039:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	call	ssl_get_cipher_by_char@PLT
	movl	$1333, %r9d
	movl	$248, %ecx
	leaq	.LC0(%rip), %r8
	testq	%rax, %rax
	je	.L168
	movl	$1, %ecx
	movl	$65539, %edx
	movq	%rax, %rsi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	ssl_cipher_disabled@PLT
	movl	%eax, %r13d
	testl	%eax, %eax
	jne	.L170
	movq	%r12, %rdi
	call	ssl_get_ciphers_by_id@PLT
	movq	%rbx, %rsi
	movq	%rax, %rdi
	call	OPENSSL_sk_find@PLT
	movl	$1351, %r9d
	testl	%eax, %eax
	js	.L169
	movq	8(%r12), %rcx
	movq	192(%rcx), %rax
	movl	96(%rax), %eax
	andl	$8, %eax
	jne	.L145
	movl	(%rcx), %edx
	cmpl	$771, %edx
	jle	.L145
	cmpl	$65536, %edx
	je	.L145
	movq	168(%r12), %rdx
	movq	568(%rdx), %rdx
	testq	%rdx, %rdx
	je	.L145
	movl	24(%rbx), %edi
	cmpl	%edi, 24(%rdx)
	jne	.L171
	.p2align 4,,10
	.p2align 3
.L145:
	movq	1296(%r12), %rdx
	movq	504(%rdx), %rsi
	testq	%rsi, %rsi
	je	.L146
	movl	24(%rsi), %edi
	movq	%rdi, 512(%rdx)
.L146:
	movl	200(%r12), %esi
	testl	%esi, %esi
	je	.L147
	movl	24(%rbx), %esi
	cmpq	%rsi, 512(%rdx)
	je	.L147
	testl	%eax, %eax
	jne	.L148
	movl	(%rcx), %eax
	cmpl	$65536, %eax
	je	.L148
	cmpl	$771, %eax
	jle	.L148
	movl	64(%rbx), %edi
	call	ssl_md@PLT
	movq	%rax, %r14
	movq	1296(%r12), %rax
	movq	504(%rax), %rax
	movl	64(%rax), %edi
	call	ssl_md@PLT
	cmpq	%rax, %r14
	je	.L147
	movl	$1379, %r9d
	leaq	.LC0(%rip), %r8
	movl	$218, %ecx
	movq	%r12, %rdi
	movl	$540, %edx
	movl	$47, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L140
	.p2align 4,,10
	.p2align 3
.L147:
	movq	168(%r12), %rax
	movl	$1, %r13d
	movq	%rbx, 568(%rax)
.L140:
	popq	%rbx
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L170:
	.cfi_restore_state
	movl	$1342, %r9d
	leaq	.LC0(%rip), %r8
	movl	$261, %ecx
.L168:
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	movl	$540, %edx
	movl	$47, %esi
	call	ossl_statem_fatal@PLT
	popq	%rbx
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L171:
	.cfi_restore_state
	movl	$1359, %r9d
	.p2align 4,,10
	.p2align 3
.L169:
	leaq	.LC0(%rip), %r8
	movl	$261, %ecx
	movl	$540, %edx
	movq	%r12, %rdi
	movl	$47, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L140
	.p2align 4,,10
	.p2align 3
.L148:
	movl	$1389, %r9d
	leaq	.LC0(%rip), %r8
	movl	$197, %ecx
	movq	%r12, %rdi
	movl	$540, %edx
	movl	$47, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L140
	.cfi_endproc
.LFE1039:
	.size	set_client_ciphersuite, .-set_client_ciphersuite
	.p2align 4
	.globl	tls_construct_client_certificate
	.type	tls_construct_client_certificate, @function
tls_construct_client_certificate:
.LFB1064:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$16, %rsp
	movq	8(%rdi), %rax
	movq	192(%rax), %rdx
	testb	$8, 96(%rdx)
	jne	.L173
	movl	(%rax), %eax
	cmpl	$771, %eax
	jle	.L173
	cmpl	$65536, %eax
	jne	.L197
	.p2align 4,,10
	.p2align 3
.L173:
	movq	168(%r12), %rax
	xorl	%edx, %edx
	cmpl	$2, 584(%rax)
	je	.L176
	movq	1168(%r12), %rax
	movq	(%rax), %rdx
.L176:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	ssl3_output_cert_chain@PLT
	movq	%rax, %r8
	xorl	%eax, %eax
	testq	%r8, %r8
	je	.L172
	movq	8(%r12), %rax
	movq	192(%rax), %rdx
	testb	$8, 96(%rdx)
	jne	.L178
	movl	(%rax), %eax
	cmpl	$65536, %eax
	je	.L178
	cmpl	$771, %eax
	jg	.L198
	.p2align 4,,10
	.p2align 3
.L178:
	movl	$1, %eax
.L172:
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L197:
	.cfi_restore_state
	movq	1944(%rdi), %rsi
	testq	%rsi, %rsi
	je	.L199
	movq	1952(%rdi), %rdx
	movl	$1, %ecx
	movq	%r13, %rdi
	call	WPACKET_sub_memcpy__@PLT
	testl	%eax, %eax
	jne	.L173
	movl	%eax, -20(%rbp)
	movl	$3546, %r9d
.L196:
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movl	$484, %edx
	movq	%r12, %rdi
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	movl	-20(%rbp), %eax
	jmp	.L172
	.p2align 4,,10
	.p2align 3
.L198:
	movq	168(%r12), %rax
	cmpq	$0, 408(%rax)
	je	.L179
	cmpq	$0, 544(%rax)
	jne	.L178
.L179:
	movl	$146, %esi
	movq	%r12, %rdi
	call	*32(%rdx)
	testl	%eax, %eax
	jne	.L178
	movl	$109, %ecx
	movl	$484, %edx
	movq	%r12, %rdi
	movl	%eax, -20(%rbp)
	movl	$3566, %r9d
	leaq	.LC0(%rip), %r8
	movl	$-1, %esi
	call	ossl_statem_fatal@PLT
	movl	-20(%rbp), %eax
	jmp	.L172
	.p2align 4,,10
	.p2align 3
.L199:
	movl	$1, %edx
	movq	%r13, %rdi
	call	WPACKET_put_bytes__@PLT
	testl	%eax, %eax
	jne	.L173
	movl	%eax, -20(%rbp)
	movl	$3541, %r9d
	jmp	.L196
	.cfi_endproc
.LFE1064:
	.size	tls_construct_client_certificate, .-tls_construct_client_certificate
	.p2align 4
	.globl	tls_construct_end_of_early_data
	.type	tls_construct_end_of_early_data, @function
tls_construct_end_of_early_data:
.LFB1071:
	.cfi_startproc
	endbr64
	movl	132(%rdi), %eax
	andl	$-5, %eax
	cmpl	$3, %eax
	jne	.L207
	movl	$7, 132(%rdi)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L207:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$66, %ecx
	movl	$536, %edx
	movl	$80, %esi
	movl	$3834, %r9d
	leaq	.LC0(%rip), %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1071:
	.size	tls_construct_end_of_early_data, .-tls_construct_end_of_early_data
	.p2align 4
	.type	tls_construct_cke_psk_preamble, @function
tls_construct_cke_psk_preamble:
.LFB1054:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$456, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	1408(%rdi), %r10
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%r10, %r10
	je	.L224
	xorl	%eax, %eax
	movl	$16, %ecx
	movq	%rsi, %rbx
	movl	$256, %r9d
	leaq	-464(%rbp), %r14
	leaq	-320(%rbp), %r11
	movq	%r14, %rdi
	movq	%r11, -472(%rbp)
	movq	%r11, %r8
	movq	%r14, %rdx
	rep stosq
	movq	1296(%r13), %rax
	movl	$128, %ecx
	movq	416(%rax), %rsi
	movb	$0, (%rdi)
	movq	%r13, %rdi
	call	*%r10
	movq	-472(%rbp), %r11
	cmpl	$256, %eax
	movl	%eax, %r15d
	ja	.L225
	movq	%r14, %r12
	testq	%r15, %r15
	je	.L226
.L213:
	movl	(%r12), %edx
	addq	$4, %r12
	leal	-16843009(%rdx), %eax
	notl	%edx
	andl	%edx, %eax
	andl	$-2139062144, %eax
	je	.L213
	movl	%eax, %edx
	movq	%r11, %rdi
	movq	%r15, %rsi
	movq	%r11, -480(%rbp)
	shrl	$16, %edx
	testl	$32896, %eax
	cmove	%edx, %eax
	leaq	2(%r12), %rdx
	cmove	%rdx, %r12
	leaq	.LC0(%rip), %rdx
	movl	%eax, %ecx
	addb	%al, %cl
	movl	$2922, %ecx
	sbbq	$3, %r12
	call	CRYPTO_memdup@PLT
	movl	$2923, %edx
	movq	%r14, %rdi
	subq	%r14, %r12
	leaq	.LC0(%rip), %rsi
	movq	%rax, -472(%rbp)
	call	CRYPTO_strdup@PLT
	movq	-480(%rbp), %r11
	cmpq	$0, -472(%rbp)
	movq	%rax, -480(%rbp)
	movq	%r11, -488(%rbp)
	je	.L219
	testq	%rax, %rax
	je	.L219
	movq	168(%r13), %rdx
	leaq	.LC0(%rip), %rsi
	movq	712(%rdx), %rdi
	movl	$2930, %edx
	call	CRYPTO_free@PLT
	movq	168(%r13), %rdx
	movq	-472(%rbp), %rax
	leaq	.LC0(%rip), %rsi
	movq	%rax, 712(%rdx)
	movq	%r15, 720(%rdx)
	movq	1296(%r13), %rdx
	movq	424(%rdx), %rdi
	movl	$2934, %edx
	call	CRYPTO_free@PLT
	movq	%rbx, %rdi
	movl	$2, %ecx
	movq	%r14, %rsi
	movq	1296(%r13), %rdx
	movq	-480(%rbp), %rax
	movq	%rax, 424(%rdx)
	movq	%r12, %rdx
	call	WPACKET_sub_memcpy__@PLT
	movq	-488(%rbp), %r11
	testl	%eax, %eax
	movl	%eax, %ebx
	je	.L227
	movq	$0, -472(%rbp)
	xorl	%eax, %eax
	movl	$1, %ebx
.L210:
	movq	%r11, %rdi
	movq	%r15, %rsi
	movq	%rax, -480(%rbp)
	call	OPENSSL_cleanse@PLT
	movl	$129, %esi
	movq	%r14, %rdi
	call	OPENSSL_cleanse@PLT
	movq	-472(%rbp), %rdi
	movl	$2949, %ecx
	movq	%r15, %rsi
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_clear_free@PLT
	movq	-480(%rbp), %rax
	movl	$2950, %ecx
	movq	%r12, %rsi
	leaq	.LC0(%rip), %rdx
	movq	%rax, %rdi
	call	CRYPTO_clear_free@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L228
	addq	$456, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L226:
	.cfi_restore_state
	movl	$2909, %r9d
	movl	$223, %ecx
	movq	%r13, %rdi
	xorl	%ebx, %ebx
	leaq	.LC0(%rip), %r8
	movl	$407, %edx
	movl	$40, %esi
	xorl	%r12d, %r12d
	movq	%r11, -480(%rbp)
	call	ossl_statem_fatal@PLT
	movq	-480(%rbp), %r11
	xorl	%eax, %eax
	movq	$0, -472(%rbp)
	jmp	.L210
	.p2align 4,,10
	.p2align 3
.L219:
	movl	$2925, %r9d
	movl	$65, %ecx
	movq	%r13, %rdi
	xorl	%ebx, %ebx
	leaq	.LC0(%rip), %r8
	movl	$407, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	movq	-480(%rbp), %rax
	movq	-488(%rbp), %r11
	jmp	.L210
	.p2align 4,,10
	.p2align 3
.L224:
	movl	$2893, %r9d
	movl	$224, %ecx
	xorl	%r15d, %r15d
	xorl	%ebx, %ebx
	leaq	.LC0(%rip), %r8
	movl	$407, %edx
	movl	$80, %esi
	xorl	%r12d, %r12d
	call	ossl_statem_fatal@PLT
	leaq	-464(%rbp), %r14
	xorl	%eax, %eax
	movq	$0, -472(%rbp)
	leaq	-320(%rbp), %r11
	jmp	.L210
	.p2align 4,,10
	.p2align 3
.L225:
	movl	$2905, %r9d
	movl	$68, %ecx
	movq	%r13, %rdi
	xorl	%ebx, %ebx
	leaq	.LC0(%rip), %r8
	movl	$407, %edx
	movl	$40, %esi
	xorl	%r12d, %r12d
	movq	%r11, -480(%rbp)
	call	ossl_statem_fatal@PLT
	movq	-480(%rbp), %r11
	xorl	%eax, %eax
	movq	$0, -472(%rbp)
	jmp	.L210
	.p2align 4,,10
	.p2align 3
.L227:
	movl	$2939, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movq	%r13, %rdi
	movl	$407, %edx
	movl	$80, %esi
	movq	%r11, -480(%rbp)
	call	ossl_statem_fatal@PLT
	movq	-480(%rbp), %r11
	xorl	%eax, %eax
	movq	$0, -472(%rbp)
	jmp	.L210
.L228:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1054:
	.size	tls_construct_cke_psk_preamble, .-tls_construct_cke_psk_preamble
	.p2align 4
	.globl	tls_construct_client_key_exchange
	.type	tls_construct_client_key_exchange, @function
tls_construct_client_key_exchange:
.LFB1060:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	168(%rdi), %rax
	movq	568(%rax), %rax
	movl	28(%rax), %r12d
	testl	$456, %r12d
	jne	.L230
	testb	$65, %r12b
	jne	.L287
.L279:
	testl	$258, %r12d
	jne	.L288
.L249:
	testb	$-124, %r12b
	jne	.L289
	testb	$16, %r12b
	jne	.L290
	testb	$32, %r12b
	jne	.L291
	andl	$8, %r12d
	movl	$1, %eax
	jne	.L229
	movl	$3343, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movq	%rbx, %rdi
	movl	$488, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	.p2align 4,,10
	.p2align 3
.L233:
	movq	168(%rbx), %rax
	movl	$3350, %ecx
	leaq	.LC0(%rip), %rdx
	movq	704(%rax), %rsi
	movq	696(%rax), %rdi
	call	CRYPTO_clear_free@PLT
	movq	168(%rbx), %rax
	movl	$3353, %ecx
	leaq	.LC0(%rip), %rdx
	movq	720(%rax), %rsi
	movq	712(%rax), %rdi
	movq	$0, 696(%rax)
	call	CRYPTO_clear_free@PLT
	movq	168(%rbx), %rax
	movq	$0, 712(%rax)
	xorl	%eax, %eax
.L229:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L292
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L230:
	.cfi_restore_state
	call	tls_construct_cke_psk_preamble
	testl	%eax, %eax
	je	.L233
	testb	$65, %r12b
	je	.L279
.L287:
	movq	1296(%rbx), %rax
	movq	$0, -72(%rbp)
	movl	$2974, %r9d
	movq	440(%rax), %rdi
	testq	%rdi, %rdi
	je	.L284
	call	X509_get0_pubkey@PLT
	movq	%rax, %rdi
	movq	%rax, %r14
	call	EVP_PKEY_get0_RSA@PLT
	testq	%rax, %rax
	je	.L293
	movl	$2987, %edx
	leaq	.LC0(%rip), %rsi
	movl	$48, %edi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L294
	movl	1524(%rbx), %eax
	leaq	2(%r12), %rdi
	movl	$46, %esi
	movzbl	%ah, %eax
	movb	%al, (%r12)
	movl	1524(%rbx), %eax
	movb	%al, 1(%r12)
	call	RAND_bytes@PLT
	movl	$2998, %r9d
	movl	$65, %ecx
	leaq	.LC0(%rip), %r8
	testl	%eax, %eax
	jle	.L281
	cmpl	$768, (%rbx)
	jle	.L240
	movl	$2, %esi
	movq	%r13, %rdi
	call	WPACKET_start_sub_packet_len__@PLT
	testl	%eax, %eax
	je	.L295
.L240:
	movq	%r14, %rdi
	xorl	%esi, %esi
	call	EVP_PKEY_CTX_new@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L242
	movq	%rax, %rdi
	call	EVP_PKEY_encrypt_init@PLT
	testl	%eax, %eax
	jle	.L242
	leaq	-64(%rbp), %r15
	xorl	%esi, %esi
	movl	$48, %r8d
	movq	%r12, %rcx
	movq	%r15, %rdx
	movq	%r14, %rdi
	call	EVP_PKEY_encrypt@PLT
	testl	%eax, %eax
	jle	.L242
	movq	-64(%rbp), %rsi
	leaq	-72(%rbp), %rdx
	movq	%r13, %rdi
	call	WPACKET_allocate_bytes@PLT
	testl	%eax, %eax
	je	.L244
	movq	-72(%rbp), %rsi
	movl	$48, %r8d
	movq	%r12, %rcx
	movq	%r15, %rdx
	movq	%r14, %rdi
	call	EVP_PKEY_encrypt@PLT
	testl	%eax, %eax
	jle	.L244
	movq	%r14, %rdi
	call	EVP_PKEY_CTX_free@PLT
	cmpl	$768, (%rbx)
	jle	.L245
	movq	%r13, %rdi
	call	WPACKET_close@PLT
	movl	$3027, %r9d
	testl	%eax, %eax
	je	.L282
.L245:
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %rsi
	movq	%r12, %rcx
	movq	%rbx, %rdi
	movl	$48, %r8d
	call	ssl_log_rsa_client_key_exchange@PLT
	testl	%eax, %eax
	je	.L265
	movq	168(%rbx), %rax
	movq	%r12, 696(%rax)
	movq	$48, 704(%rax)
	movl	$1, %eax
	jmp	.L229
	.p2align 4,,10
	.p2align 3
.L288:
	movq	168(%rbx), %rax
	movq	$0, -64(%rbp)
	movq	1032(%rax), %r14
	testq	%r14, %r14
	je	.L296
	movq	%r14, %rdi
	call	ssl_generate_pkey@PLT
	movl	$3071, %r9d
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L283
	movq	%rax, %rdi
	call	EVP_PKEY_get0_DH@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L297
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	ssl_derive@PLT
	testl	%eax, %eax
	je	.L285
	xorl	%edx, %edx
	leaq	-72(%rbp), %rsi
	movq	%r15, %rdi
	call	DH_get0_key@PLT
	movq	-72(%rbp), %rdi
	call	BN_num_bits@PLT
	leaq	-64(%rbp), %rdx
	movl	$2, %ecx
	movq	%r13, %rdi
	leal	14(%rax), %esi
	addl	$7, %eax
	cmovns	%eax, %esi
	sarl	$3, %esi
	movslq	%esi, %rsi
	call	WPACKET_sub_allocate_bytes__@PLT
	movl	$3093, %r9d
	testl	%eax, %eax
	je	.L283
	movq	-64(%rbp), %rsi
	movq	-72(%rbp), %rdi
	call	BN_bn2bin@PLT
	movq	%r12, %rdi
	call	EVP_PKEY_free@PLT
	movl	$1, %eax
	jmp	.L229
	.p2align 4,,10
	.p2align 3
.L291:
	movq	2048(%rbx), %rdi
	movq	$0, -64(%rbp)
	testq	%rdi, %rdi
	je	.L262
	call	BN_num_bits@PLT
	leaq	-64(%rbp), %rdx
	movl	$2, %ecx
	movq	%r13, %rdi
	leal	14(%rax), %esi
	addl	$7, %eax
	cmovns	%eax, %esi
	sarl	$3, %esi
	movslq	%esi, %rsi
	call	WPACKET_sub_allocate_bytes__@PLT
	testl	%eax, %eax
	jne	.L298
.L262:
	movl	$3291, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movq	%rbx, %rdi
	movl	$410, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L233
	.p2align 4,,10
	.p2align 3
.L242:
	movl	$3012, %r9d
	leaq	.LC0(%rip), %r8
	movl	$6, %ecx
	movq	%rbx, %rdi
	movl	$409, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
.L239:
	movq	%r12, %rdi
	movl	$3043, %ecx
	movl	$48, %esi
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_clear_free@PLT
	movq	%r14, %rdi
	call	EVP_PKEY_CTX_free@PLT
	jmp	.L233
	.p2align 4,,10
	.p2align 3
.L297:
	movl	$3079, %r9d
.L283:
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movl	$404, %edx
	movq	%rbx, %rdi
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
.L285:
	movq	%r12, %rdi
	call	EVP_PKEY_free@PLT
	jmp	.L233
	.p2align 4,,10
	.p2align 3
.L289:
	movq	168(%rbx), %rax
	movq	$0, -64(%rbp)
	movq	1032(%rax), %r14
	testq	%r14, %r14
	je	.L299
	movq	%r14, %rdi
	call	ssl_generate_pkey@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L300
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movq	%rax, %rsi
	movq	%rbx, %rdi
	call	ssl_derive@PLT
	testl	%eax, %eax
	jne	.L301
.L256:
	movq	-64(%rbp), %rdi
	movl	$3156, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	jmp	.L285
	.p2align 4,,10
	.p2align 3
.L293:
	movl	$2981, %r9d
.L284:
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movl	$409, %edx
	movq	%rbx, %rdi
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L233
	.p2align 4,,10
	.p2align 3
.L290:
	movl	$3277, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movq	%rbx, %rdi
	movl	$406, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L233
	.p2align 4,,10
	.p2align 3
.L301:
	leaq	-64(%rbp), %rsi
	movq	%r12, %rdi
	call	EVP_PKEY_get1_tls_encodedpoint@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L302
	movq	-64(%rbp), %rsi
	movl	$1, %ecx
	movq	%r13, %rdi
	call	WPACKET_sub_memcpy__@PLT
	testl	%eax, %eax
	je	.L303
	movq	-64(%rbp), %rdi
	movl	$3156, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%r12, %rdi
	call	EVP_PKEY_free@PLT
	movl	$1, %eax
	jmp	.L229
	.p2align 4,,10
	.p2align 3
.L298:
	movq	2048(%rbx), %rdi
	movq	-64(%rbp), %rsi
	call	BN_bn2bin@PLT
	movq	1296(%rbx), %rax
	movl	$3297, %edx
	leaq	.LC0(%rip), %rsi
	movq	608(%rax), %rdi
	call	CRYPTO_free@PLT
	movq	2008(%rbx), %rdi
	movl	$3298, %edx
	leaq	.LC0(%rip), %rsi
	movq	1296(%rbx), %r12
	call	CRYPTO_strdup@PLT
	movq	1296(%rbx), %rdx
	movq	%rax, 608(%r12)
	movl	$1, %eax
	cmpq	$0, 608(%rdx)
	jne	.L229
	movl	$3300, %r9d
	leaq	.LC0(%rip), %r8
	movl	$65, %ecx
	movq	%rbx, %rdi
	movl	$410, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L233
	.p2align 4,,10
	.p2align 3
.L295:
	movl	$3005, %r9d
.L282:
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
.L281:
	movl	$409, %edx
	movl	$80, %esi
	movq	%rbx, %rdi
	xorl	%r14d, %r14d
	call	ossl_statem_fatal@PLT
	jmp	.L239
	.p2align 4,,10
	.p2align 3
.L294:
	movl	$2989, %r9d
	leaq	.LC0(%rip), %r8
	movl	$65, %ecx
	movq	%rbx, %rdi
	movl	$409, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L233
	.p2align 4,,10
	.p2align 3
.L296:
	movl	$68, %ecx
	movl	$404, %edx
	movq	%rbx, %rdi
	xorl	%r12d, %r12d
	movl	$3064, %r9d
	leaq	.LC0(%rip), %r8
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L285
	.p2align 4,,10
	.p2align 3
.L299:
	movl	$3122, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movq	%rbx, %rdi
	movl	$405, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L233
	.p2align 4,,10
	.p2align 3
.L244:
	movl	$3018, %r9d
	leaq	.LC0(%rip), %r8
	movl	$119, %ecx
	movq	%rbx, %rdi
	movl	$409, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L239
	.p2align 4,,10
	.p2align 3
.L300:
	movl	$3129, %r9d
	leaq	.LC0(%rip), %r8
	movl	$65, %ecx
	movq	%rbx, %rdi
	movl	$405, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L256
	.p2align 4,,10
	.p2align 3
.L302:
	movl	$3143, %r9d
	leaq	.LC0(%rip), %r8
	movl	$16, %ecx
	movq	%rbx, %rdi
	movl	$405, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L256
.L303:
	movl	$3149, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movq	%rbx, %rdi
	movl	$405, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L256
.L265:
	xorl	%r14d, %r14d
	jmp	.L239
.L292:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1060:
	.size	tls_construct_client_key_exchange, .-tls_construct_client_key_exchange
	.p2align 4
	.globl	tls_construct_next_proto
	.type	tls_construct_next_proto, @function
tls_construct_next_proto:
.LFB1066:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -40
	movq	1800(%rdi), %rbx
	movq	1792(%rdi), %rsi
	movq	%r13, %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -48(%rbp)
	movq	%rbx, %rdx
	call	WPACKET_sub_memcpy__@PLT
	testl	%eax, %eax
	je	.L307
	addq	$2, %rbx
	movl	$32, %eax
	leaq	-48(%rbp), %rdx
	movq	%r13, %rdi
	andl	$31, %ebx
	movl	$1, %ecx
	subq	%rbx, %rax
	movq	%rax, %rsi
	movq	%rax, %rbx
	call	WPACKET_sub_allocate_bytes__@PLT
	testl	%eax, %eax
	jne	.L323
.L307:
	movl	$3637, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movq	%r12, %rdi
	movl	$426, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
.L304:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L324
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L323:
	.cfi_restore_state
	movq	-48(%rbp), %rdx
	xorl	%esi, %esi
	cmpl	$8, %ebx
	jnb	.L308
	testb	$4, %bl
	jne	.L325
	testl	%ebx, %ebx
	je	.L309
	movb	$0, (%rdx)
	testb	$2, %bl
	jne	.L326
.L309:
	movl	$1, %eax
	jmp	.L304
	.p2align 4,,10
	.p2align 3
.L308:
	leaq	8(%rdx), %rcx
	movl	%ebx, %eax
	movq	$0, (%rdx)
	movq	$0, -8(%rdx,%rax)
	andq	$-8, %rcx
	subq	%rcx, %rdx
	addl	%edx, %ebx
	andl	$-8, %ebx
	cmpl	$8, %ebx
	jb	.L309
	andl	$-8, %ebx
	xorl	%eax, %eax
.L312:
	movl	%eax, %edx
	addl	$8, %eax
	movq	%rsi, (%rcx,%rdx)
	cmpl	%ebx, %eax
	jb	.L312
	jmp	.L309
	.p2align 4,,10
	.p2align 3
.L325:
	movl	%ebx, %eax
	movl	$0, (%rdx)
	movl	$0, -4(%rdx,%rax)
	jmp	.L309
.L326:
	movl	%ebx, %eax
	xorl	%ecx, %ecx
	movw	%cx, -2(%rdx,%rax)
	jmp	.L309
.L324:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1066:
	.size	tls_construct_next_proto, .-tls_construct_next_proto
	.p2align 4
	.globl	ossl_statem_client_read_transition
	.type	ossl_statem_client_read_transition, @function
ossl_statem_client_read_transition:
.LFB1028:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$24, %rsp
	.cfi_offset 12, -24
	movq	8(%rdi), %r8
	movq	192(%r8), %rax
	movl	96(%rax), %edx
	movl	92(%rdi), %eax
	movl	%edx, %ecx
	andl	$8, %ecx
	jne	.L328
	movl	(%r8), %edx
	cmpl	$65536, %edx
	je	.L329
	cmpl	$771, %edx
	jg	.L462
.L329:
	cmpl	$46, %eax
	ja	.L385
	leaq	.L386(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L386:
	.long	.L385-.L386
	.long	.L360-.L386
	.long	.L385-.L386
	.long	.L359-.L386
	.long	.L358-.L386
	.long	.L357-.L386
	.long	.L356-.L386
	.long	.L355-.L386
	.long	.L385-.L386
	.long	.L380-.L386
	.long	.L353-.L386
	.long	.L385-.L386
	.long	.L352-.L386
	.long	.L385-.L386
	.long	.L385-.L386
	.long	.L385-.L386
	.long	.L385-.L386
	.long	.L385-.L386
	.long	.L351-.L386
	.long	.L385-.L386
	.long	.L385-.L386
	.long	.L385-.L386
	.long	.L385-.L386
	.long	.L385-.L386
	.long	.L385-.L386
	.long	.L385-.L386
	.long	.L385-.L386
	.long	.L385-.L386
	.long	.L385-.L386
	.long	.L385-.L386
	.long	.L385-.L386
	.long	.L385-.L386
	.long	.L385-.L386
	.long	.L385-.L386
	.long	.L385-.L386
	.long	.L385-.L386
	.long	.L385-.L386
	.long	.L385-.L386
	.long	.L385-.L386
	.long	.L385-.L386
	.long	.L385-.L386
	.long	.L385-.L386
	.long	.L385-.L386
	.long	.L385-.L386
	.long	.L385-.L386
	.long	.L385-.L386
	.long	.L349-.L386
	.text
.L380:
	cmpl	$257, %esi
	je	.L463
.L385:
	movl	$393, %r9d
	movl	$244, %ecx
	movl	$417, %edx
	movl	$10, %esi
	leaq	.LC0(%rip), %r8
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
.L327:
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L328:
	.cfi_restore_state
	cmpl	$46, %eax
	ja	.L387
	leaq	.L350(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L350:
	.long	.L387-.L350
	.long	.L360-.L350
	.long	.L387-.L350
	.long	.L359-.L350
	.long	.L358-.L350
	.long	.L357-.L350
	.long	.L356-.L350
	.long	.L355-.L350
	.long	.L387-.L350
	.long	.L380-.L350
	.long	.L353-.L350
	.long	.L387-.L350
	.long	.L352-.L350
	.long	.L387-.L350
	.long	.L387-.L350
	.long	.L387-.L350
	.long	.L387-.L350
	.long	.L387-.L350
	.long	.L351-.L350
	.long	.L387-.L350
	.long	.L387-.L350
	.long	.L387-.L350
	.long	.L387-.L350
	.long	.L387-.L350
	.long	.L387-.L350
	.long	.L387-.L350
	.long	.L387-.L350
	.long	.L387-.L350
	.long	.L387-.L350
	.long	.L387-.L350
	.long	.L387-.L350
	.long	.L387-.L350
	.long	.L387-.L350
	.long	.L387-.L350
	.long	.L387-.L350
	.long	.L387-.L350
	.long	.L387-.L350
	.long	.L387-.L350
	.long	.L387-.L350
	.long	.L387-.L350
	.long	.L387-.L350
	.long	.L387-.L350
	.long	.L387-.L350
	.long	.L387-.L350
	.long	.L387-.L350
	.long	.L387-.L350
	.long	.L349-.L350
	.text
.L387:
	movl	$1, %eax
.L348:
	cmpl	$257, %esi
	jne	.L385
	testb	%al, %al
	je	.L385
	movq	$0, 152(%rdi)
	movl	$3, 40(%rdi)
	call	SSL_get_rbio@PLT
	movl	$15, %esi
	movq	%rax, %r12
	movq	%rax, %rdi
	call	BIO_clear_flags@PLT
	movl	$9, %esi
	movq	%r12, %rdi
	call	BIO_set_flags@PLT
	xorl	%eax, %eax
	jmp	.L327
	.p2align 4,,10
	.p2align 3
.L462:
	cmpl	$39, %eax
	ja	.L385
	leaq	.L332(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L332:
	.long	.L385-.L332
	.long	.L338-.L332
	.long	.L385-.L332
	.long	.L337-.L332
	.long	.L336-.L332
	.long	.L385-.L332
	.long	.L385-.L332
	.long	.L342-.L332
	.long	.L385-.L332
	.long	.L385-.L332
	.long	.L385-.L332
	.long	.L385-.L332
	.long	.L334-.L332
	.long	.L385-.L332
	.long	.L385-.L332
	.long	.L385-.L332
	.long	.L385-.L332
	.long	.L385-.L332
	.long	.L385-.L332
	.long	.L385-.L332
	.long	.L385-.L332
	.long	.L385-.L332
	.long	.L385-.L332
	.long	.L385-.L332
	.long	.L385-.L332
	.long	.L385-.L332
	.long	.L385-.L332
	.long	.L385-.L332
	.long	.L385-.L332
	.long	.L385-.L332
	.long	.L385-.L332
	.long	.L385-.L332
	.long	.L385-.L332
	.long	.L385-.L332
	.long	.L385-.L332
	.long	.L385-.L332
	.long	.L385-.L332
	.long	.L385-.L332
	.long	.L333-.L332
	.long	.L454-.L332
	.text
.L355:
	cmpl	$14, %esi
	je	.L379
.L457:
	testl	%ecx, %ecx
	setne	%al
	jmp	.L348
.L358:
	movl	1628(%rdi), %edx
	testl	%edx, %edx
	jne	.L464
.L357:
	movq	168(%rdi), %rax
	movq	568(%rax), %rax
	movl	28(%rax), %eax
	testl	$422, %eax
	jne	.L376
	testl	$456, %eax
	je	.L356
	cmpl	$12, %esi
	je	.L371
.L356:
	cmpl	$13, %esi
	jne	.L355
	movq	168(%rdi), %rax
	cmpl	$768, (%rdi)
	movq	568(%rax), %rax
	movl	32(%rax), %eax
	jle	.L378
	testb	$4, %al
	jne	.L385
.L378:
	testb	$80, %al
	je	.L347
	jmp	.L385
.L359:
	movl	200(%rdi), %r8d
	testl	%r8d, %r8d
	je	.L363
.L351:
	movl	1664(%rdi), %eax
	testl	%eax, %eax
	je	.L380
	cmpl	$4, %esi
	jne	.L457
.L381:
	movl	$9, 92(%rdi)
	movl	$1, %eax
	jmp	.L327
.L352:
	cmpl	$2, %esi
	je	.L362
	testl	%ecx, %ecx
	setne	%al
	cmpl	$3, %esi
	jne	.L348
	testb	%al, %al
	je	.L348
.L366:
	movl	$2, 92(%rdi)
	movl	$1, %eax
	jmp	.L327
.L353:
	cmpl	$20, %esi
	jne	.L457
	jmp	.L383
.L349:
	cmpl	$2, %esi
	jne	.L457
	jmp	.L362
.L360:
	testl	%esi, %esi
	jne	.L457
	movl	$41, 92(%rdi)
	movl	$1, %eax
	jmp	.L327
.L454:
	cmpl	$20, %esi
	jne	.L385
.L383:
	movl	$11, 92(%rdi)
	movl	$1, %eax
	jmp	.L327
.L333:
	movl	200(%rdi), %r9d
	testl	%r9d, %r9d
	jne	.L454
	cmpl	$13, %esi
	je	.L347
.L342:
	cmpl	$11, %esi
	jne	.L385
.L458:
	movl	$4, 92(%rdi)
	movl	$1, %eax
	jmp	.L327
.L338:
	cmpl	$4, %esi
	je	.L381
	cmpl	$24, %esi
	je	.L465
	cmpl	$13, %esi
	jne	.L385
	cmpl	$1, 1936(%rdi)
	jne	.L385
	movl	$4, 1936(%rdi)
	movq	%rdi, -24(%rbp)
	call	tls13_restore_handshake_digest_for_pha@PLT
	movq	-24(%rbp), %rdi
	testl	%eax, %eax
	je	.L385
	.p2align 4,,10
	.p2align 3
.L347:
	movl	$7, 92(%rdi)
	movl	$1, %eax
	jmp	.L327
.L334:
	cmpl	$2, %esi
	jne	.L385
	.p2align 4,,10
	.p2align 3
.L362:
	movl	$3, 92(%rdi)
	movl	$1, %eax
	jmp	.L327
.L337:
	cmpl	$8, %esi
	jne	.L385
	movl	$38, 92(%rdi)
	movl	$1, %eax
	jmp	.L327
.L336:
	cmpl	$15, %esi
	jne	.L385
	movl	$39, 92(%rdi)
	movl	$1, %eax
	jmp	.L327
	.p2align 4,,10
	.p2align 3
.L464:
	cmpl	$22, %esi
	jne	.L357
	movl	$5, 92(%rdi)
	movl	$1, %eax
	jmp	.L327
	.p2align 4,,10
	.p2align 3
.L363:
	testl	%ecx, %ecx
	setne	%al
	cmpl	$3, %esi
	jne	.L388
	testb	%al, %al
	jne	.L366
.L388:
	movl	(%rdi), %ecx
	cmpl	$768, %ecx
	jle	.L368
	cmpq	$0, 1760(%rdi)
	je	.L368
	movq	1296(%rdi), %rdx
	cmpq	$0, 552(%rdx)
	je	.L368
	cmpl	$257, %esi
	je	.L466
	.p2align 4,,10
	.p2align 3
.L368:
	movq	168(%rdi), %rdx
	movq	568(%rdx), %rdx
	movl	32(%rdx), %r8d
	testb	$84, %r8b
	jne	.L369
	cmpl	$11, %esi
	jne	.L348
	jmp	.L458
	.p2align 4,,10
	.p2align 3
.L463:
	movl	$10, 92(%rdi)
	movl	$1, %eax
	jmp	.L327
	.p2align 4,,10
	.p2align 3
.L376:
	cmpl	$12, %esi
	jne	.L457
.L371:
	movl	$6, 92(%rdi)
	movl	$1, %eax
	jmp	.L327
	.p2align 4,,10
	.p2align 3
.L369:
	movl	28(%rdx), %edx
	testl	$422, %edx
	jne	.L370
	andl	$456, %edx
	je	.L373
	cmpl	$12, %esi
	je	.L371
.L373:
	cmpl	$13, %esi
	je	.L467
	cmpl	$14, %esi
	jne	.L348
	.p2align 4,,10
	.p2align 3
.L379:
	movl	$8, 92(%rdi)
	movl	$1, %eax
	jmp	.L327
.L370:
	cmpl	$12, %esi
	jne	.L348
	jmp	.L371
	.p2align 4,,10
	.p2align 3
.L467:
	cmpl	$768, %ecx
	jle	.L375
	testb	$4, %r8b
	jne	.L385
.L375:
	andl	$80, %r8d
	je	.L347
	jmp	.L385
.L465:
	movl	$45, 92(%rdi)
	movl	$1, %eax
	jmp	.L327
.L466:
	movl	$1, 200(%rdi)
	movl	$1, %eax
	movl	$10, 92(%rdi)
	jmp	.L327
	.cfi_endproc
.LFE1028:
	.size	ossl_statem_client_read_transition, .-ossl_statem_client_read_transition
	.p2align 4
	.globl	ossl_statem_client_write_transition
	.type	ossl_statem_client_write_transition, @function
ossl_statem_client_write_transition:
.LFB1030:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	8(%rdi), %rcx
	movl	92(%rdi), %edx
	movq	192(%rcx), %rax
	movl	96(%rax), %eax
	andl	$8, %eax
	jne	.L469
	movl	(%rcx), %ecx
	cmpl	$65536, %ecx
	je	.L469
	cmpl	$771, %ecx
	jg	.L536
.L469:
	cmpl	$46, %edx
	ja	.L488
	leaq	.L489(%rip), %rcx
	movslq	(%rcx,%rdx,4), %rdx
	addq	%rcx, %rdx
	notrack jmp	*%rdx
	.section	.rodata
	.align 4
	.align 4
.L489:
	.long	.L503-.L489
	.long	.L501-.L489
	.long	.L503-.L489
	.long	.L499-.L489
	.long	.L488-.L489
	.long	.L488-.L489
	.long	.L488-.L489
	.long	.L488-.L489
	.long	.L498-.L489
	.long	.L488-.L489
	.long	.L488-.L489
	.long	.L497-.L489
	.long	.L496-.L489
	.long	.L495-.L489
	.long	.L494-.L489
	.long	.L493-.L489
	.long	.L492-.L489
	.long	.L507-.L489
	.long	.L491-.L489
	.long	.L488-.L489
	.long	.L488-.L489
	.long	.L488-.L489
	.long	.L488-.L489
	.long	.L488-.L489
	.long	.L488-.L489
	.long	.L488-.L489
	.long	.L488-.L489
	.long	.L488-.L489
	.long	.L488-.L489
	.long	.L488-.L489
	.long	.L488-.L489
	.long	.L488-.L489
	.long	.L488-.L489
	.long	.L488-.L489
	.long	.L488-.L489
	.long	.L488-.L489
	.long	.L488-.L489
	.long	.L488-.L489
	.long	.L488-.L489
	.long	.L488-.L489
	.long	.L488-.L489
	.long	.L490-.L489
	.long	.L488-.L489
	.long	.L488-.L489
	.long	.L488-.L489
	.long	.L488-.L489
	.long	.L513-.L489
	.text
.L513:
	movl	$2, %eax
.L468:
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L501:
	.cfi_restore_state
	movl	1928(%r12), %edi
	movl	$2, %eax
	testl	%edi, %edi
	je	.L468
.L503:
	movl	$12, 92(%r12)
	addq	$24, %rsp
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L536:
	.cfi_restore_state
	cmpl	$48, %edx
	ja	.L470
	leaq	.L472(%rip), %rdi
	movl	%edx, %esi
	movslq	(%rdi,%rsi,4), %rcx
	addq	%rdi, %rcx
	notrack jmp	*%rcx
	.section	.rodata
	.align 4
	.align 4
.L472:
	.long	.L470-.L472
	.long	.L479-.L472
	.long	.L470-.L472
	.long	.L470-.L472
	.long	.L470-.L472
	.long	.L470-.L472
	.long	.L470-.L472
	.long	.L478-.L472
	.long	.L470-.L472
	.long	.L508-.L472
	.long	.L470-.L472
	.long	.L477-.L472
	.long	.L470-.L472
	.long	.L476-.L472
	.long	.L470-.L472
	.long	.L507-.L472
	.long	.L471-.L472
	.long	.L470-.L472
	.long	.L508-.L472
	.long	.L470-.L472
	.long	.L470-.L472
	.long	.L470-.L472
	.long	.L470-.L472
	.long	.L470-.L472
	.long	.L470-.L472
	.long	.L470-.L472
	.long	.L470-.L472
	.long	.L470-.L472
	.long	.L470-.L472
	.long	.L470-.L472
	.long	.L470-.L472
	.long	.L470-.L472
	.long	.L470-.L472
	.long	.L470-.L472
	.long	.L470-.L472
	.long	.L470-.L472
	.long	.L470-.L472
	.long	.L470-.L472
	.long	.L470-.L472
	.long	.L470-.L472
	.long	.L470-.L472
	.long	.L470-.L472
	.long	.L470-.L472
	.long	.L508-.L472
	.long	.L470-.L472
	.long	.L508-.L472
	.long	.L470-.L472
	.long	.L473-.L472
	.long	.L471-.L472
	.text
.L499:
	testb	$16, 1494(%r12)
	je	.L503
	cmpl	$7, 132(%r12)
	je	.L503
.L493:
	movl	$16, 92(%r12)
	addq	$24, %rsp
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L490:
	.cfi_restore_state
	movl	$1, %esi
	movq	%r12, %rdi
	call	ssl3_renegotiate_check@PLT
	testl	%eax, %eax
	jne	.L537
.L508:
	movl	$1, 92(%r12)
.L535:
	addq	$24, %rsp
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L498:
	.cfi_restore_state
	movq	168(%r12), %rax
	movl	584(%rax), %esi
	testl	%esi, %esi
	jne	.L504
.L495:
	movl	$14, 92(%r12)
	movl	$1, %eax
	jmp	.L468
.L507:
	movl	$18, 92(%r12)
	movl	$1, %eax
	jmp	.L468
.L492:
	cmpl	$1, 1248(%r12)
	je	.L503
	cmpl	$2, 132(%r12)
	je	.L506
	testl	%eax, %eax
	jne	.L507
	movq	168(%r12), %rax
	movl	988(%rax), %ecx
	testl	%ecx, %ecx
	je	.L507
	movl	$17, 92(%r12)
	movl	$1, %eax
	jmp	.L468
.L488:
	movl	$512, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movq	%r12, %rdi
	movl	$599, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	jmp	.L468
.L496:
	cmpl	$2, 132(%r12)
	movl	$2, %eax
	jne	.L468
	testb	$16, 1494(%r12)
	jne	.L493
.L506:
	movl	$46, 92(%r12)
	movl	$1, %eax
	jmp	.L468
.L494:
	movq	168(%r12), %rdx
	xorl	%eax, %eax
	cmpl	$1, 584(%rdx)
	setne	%al
	addl	$15, %eax
	movl	%eax, 92(%r12)
	testb	$16, (%rdx)
	jne	.L493
	jmp	.L535
.L491:
	movl	200(%r12), %edx
	movl	$2, %eax
	testl	%edx, %edx
	jne	.L508
	jmp	.L468
.L497:
	movl	200(%r12), %eax
	testl	%eax, %eax
	jne	.L493
	jmp	.L508
.L483:
	testb	$16, 1494(%r12)
	je	.L471
	movl	1248(%r12), %r8d
	testl	%r8d, %r8d
	je	.L493
.L471:
	movq	168(%r12), %rax
	cmpl	$1, 584(%rax)
	sbbl	%eax, %eax
	andl	$5, %eax
	addl	$13, %eax
	movl	%eax, 92(%r12)
	movl	$1, %eax
	jmp	.L468
.L473:
	cmpl	$2, 1816(%r12)
	jne	.L471
	movl	$48, 92(%r12)
	movl	$1, %eax
	jmp	.L468
.L470:
	movl	%eax, -20(%rbp)
	movl	$416, %r9d
.L534:
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movl	$598, %edx
	movq	%r12, %rdi
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	movl	-20(%rbp), %eax
	jmp	.L468
.L476:
	movq	168(%r12), %rax
	cmpl	$1, 584(%rax)
	setne	%al
	movzbl	%al, %eax
	leal	15(%rax,%rax,2), %eax
	movl	%eax, 92(%r12)
	movl	$1, %eax
	jmp	.L468
.L477:
	movl	132(%r12), %eax
	andl	$-5, %eax
	cmpl	$3, %eax
	jne	.L483
	movl	$47, 92(%r12)
	movl	$1, %eax
	jmp	.L468
.L479:
	cmpl	$-1, 1932(%r12)
	movl	$2, %eax
	je	.L468
	movl	$43, 92(%r12)
	movl	%edx, %eax
	jmp	.L468
.L478:
	cmpl	$4, 1936(%r12)
	je	.L504
	testb	$1, 68(%r12)
	jne	.L508
	movl	%eax, -20(%rbp)
	movl	$432, %r9d
	jmp	.L534
	.p2align 4,,10
	.p2align 3
.L504:
	movl	$13, 92(%r12)
	movl	$1, %eax
	jmp	.L468
	.p2align 4,,10
	.p2align 3
.L537:
	movq	%r12, %rdi
	call	tls_setup_handshake@PLT
	testl	%eax, %eax
	jne	.L503
	jmp	.L468
	.cfi_endproc
.LFE1030:
	.size	ossl_statem_client_write_transition, .-ossl_statem_client_write_transition
	.p2align 4
	.globl	ossl_statem_client_pre_work
	.type	ossl_statem_client_pre_work, @function
ossl_statem_client_pre_work:
.LFB1031:
	.cfi_startproc
	endbr64
	movl	92(%rdi), %eax
	cmpl	$16, %eax
	je	.L539
	jbe	.L560
	cmpl	$46, %eax
	je	.L544
	cmpl	$47, %eax
	jne	.L558
	movl	132(%rdi), %eax
	cmpl	$7, %eax
	je	.L558
	testl	%eax, %eax
	je	.L558
.L544:
	movl	$1, %ecx
	xorl	%edx, %edx
	jmp	tls_finish_handshake@PLT
	.p2align 4,,10
	.p2align 3
.L560:
	cmpl	$1, %eax
	je	.L541
	cmpl	$12, %eax
	jne	.L558
	movq	8(%rdi), %rax
	movl	$0, 68(%rdi)
	movq	192(%rax), %rax
	testb	$8, 96(%rax)
	jne	.L546
.L558:
	movl	$2, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L546:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ssl3_init_finished_mac@PLT
	movl	%eax, %r8d
	xorl	%eax, %eax
	testl	%r8d, %r8d
	je	.L538
	movl	$2, %eax
.L538:
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L539:
	.cfi_restore 6
	movq	8(%rdi), %rax
	movq	192(%rax), %rax
	testb	$8, 96(%rax)
	je	.L558
	movl	200(%rdi), %eax
	testl	%eax, %eax
	je	.L558
	movl	$0, 120(%rdi)
	movl	$2, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L541:
	movl	$1, %ecx
	movl	$1, %edx
	jmp	tls_finish_handshake@PLT
	.cfi_endproc
.LFE1031:
	.size	ossl_statem_client_pre_work, .-ossl_statem_client_pre_work
	.p2align 4
	.globl	ossl_statem_client_construct_message
	.type	ossl_statem_client_construct_message, @function
ossl_statem_client_construct_message:
.LFB1033:
	.cfi_startproc
	endbr64
	movl	92(%rdi), %eax
	subl	$12, %eax
	cmpl	$36, %eax
	ja	.L562
	leaq	.L564(%rip), %rsi
	movslq	(%rsi,%rax,4), %rax
	addq	%rsi, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L564:
	.long	.L573-.L564
	.long	.L572-.L564
	.long	.L571-.L564
	.long	.L570-.L564
	.long	.L569-.L564
	.long	.L568-.L564
	.long	.L567-.L564
	.long	.L562-.L564
	.long	.L562-.L564
	.long	.L562-.L564
	.long	.L562-.L564
	.long	.L562-.L564
	.long	.L562-.L564
	.long	.L562-.L564
	.long	.L562-.L564
	.long	.L562-.L564
	.long	.L562-.L564
	.long	.L562-.L564
	.long	.L562-.L564
	.long	.L562-.L564
	.long	.L562-.L564
	.long	.L562-.L564
	.long	.L562-.L564
	.long	.L562-.L564
	.long	.L562-.L564
	.long	.L562-.L564
	.long	.L562-.L564
	.long	.L562-.L564
	.long	.L562-.L564
	.long	.L562-.L564
	.long	.L562-.L564
	.long	.L566-.L564
	.long	.L562-.L564
	.long	.L562-.L564
	.long	.L562-.L564
	.long	.L565-.L564
	.long	.L563-.L564
	.text
	.p2align 4,,10
	.p2align 3
.L562:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$236, %ecx
	movl	$430, %edx
	movl	$80, %esi
	movl	$893, %r9d
	leaq	.LC0(%rip), %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L573:
	.cfi_restore 6
	leaq	tls_construct_client_hello(%rip), %rax
	movq	%rax, (%rdx)
	movl	$1, %eax
	movl	$1, (%rcx)
	ret
	.p2align 4,,10
	.p2align 3
.L572:
	leaq	tls_construct_client_certificate(%rip), %rax
	movq	%rax, (%rdx)
	movl	$1, %eax
	movl	$11, (%rcx)
	ret
	.p2align 4,,10
	.p2align 3
.L571:
	leaq	tls_construct_client_key_exchange(%rip), %rax
	movq	%rax, (%rdx)
	movl	$1, %eax
	movl	$16, (%rcx)
	ret
	.p2align 4,,10
	.p2align 3
.L570:
	movq	tls_construct_cert_verify@GOTPCREL(%rip), %rax
	movq	%rax, (%rdx)
	movl	$1, %eax
	movl	$15, (%rcx)
	ret
	.p2align 4,,10
	.p2align 3
.L569:
	movq	8(%rdi), %rax
	movq	192(%rax), %rax
	testb	$8, 96(%rax)
	jne	.L576
	movq	tls_construct_change_cipher_spec@GOTPCREL(%rip), %rax
.L575:
	movq	%rax, (%rdx)
	movl	$1, %eax
	movl	$257, (%rcx)
	ret
	.p2align 4,,10
	.p2align 3
.L568:
	leaq	tls_construct_next_proto(%rip), %rax
	movq	%rax, (%rdx)
	movl	$1, %eax
	movl	$67, (%rcx)
	ret
	.p2align 4,,10
	.p2align 3
.L567:
	movq	tls_construct_finished@GOTPCREL(%rip), %rax
	movq	%rax, (%rdx)
	movl	$1, %eax
	movl	$20, (%rcx)
	ret
	.p2align 4,,10
	.p2align 3
.L566:
	movq	tls_construct_key_update@GOTPCREL(%rip), %rax
	movq	%rax, (%rdx)
	movl	$1, %eax
	movl	$24, (%rcx)
	ret
	.p2align 4,,10
	.p2align 3
.L565:
	movq	$0, (%rdx)
	movl	$1, %eax
	movl	$-1, (%rcx)
	ret
	.p2align 4,,10
	.p2align 3
.L563:
	leaq	tls_construct_end_of_early_data(%rip), %rax
	movq	%rax, (%rdx)
	movl	$1, %eax
	movl	$5, (%rcx)
	ret
	.p2align 4,,10
	.p2align 3
.L576:
	movq	dtls_construct_change_cipher_spec@GOTPCREL(%rip), %rax
	jmp	.L575
	.cfi_endproc
.LFE1033:
	.size	ossl_statem_client_construct_message, .-ossl_statem_client_construct_message
	.p2align 4
	.globl	ossl_statem_client_max_message_size
	.type	ossl_statem_client_max_message_size, @function
ossl_statem_client_max_message_size:
.LFB1034:
	.cfi_startproc
	endbr64
	cmpl	$45, 92(%rdi)
	ja	.L591
	movl	92(%rdi), %eax
	leaq	.L583(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L583:
	.long	.L591-.L583
	.long	.L591-.L583
	.long	.L590-.L583
	.long	.L585-.L583
	.long	.L588-.L583
	.long	.L584-.L583
	.long	.L589-.L583
	.long	.L588-.L583
	.long	.L591-.L583
	.long	.L584-.L583
	.long	.L587-.L583
	.long	.L586-.L583
	.long	.L591-.L583
	.long	.L591-.L583
	.long	.L591-.L583
	.long	.L591-.L583
	.long	.L591-.L583
	.long	.L591-.L583
	.long	.L591-.L583
	.long	.L591-.L583
	.long	.L591-.L583
	.long	.L591-.L583
	.long	.L591-.L583
	.long	.L591-.L583
	.long	.L591-.L583
	.long	.L591-.L583
	.long	.L591-.L583
	.long	.L591-.L583
	.long	.L591-.L583
	.long	.L591-.L583
	.long	.L591-.L583
	.long	.L591-.L583
	.long	.L591-.L583
	.long	.L591-.L583
	.long	.L591-.L583
	.long	.L591-.L583
	.long	.L591-.L583
	.long	.L591-.L583
	.long	.L585-.L583
	.long	.L584-.L583
	.long	.L591-.L583
	.long	.L591-.L583
	.long	.L591-.L583
	.long	.L591-.L583
	.long	.L591-.L583
	.long	.L582-.L583
	.text
	.p2align 4,,10
	.p2align 3
.L591:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L584:
	movl	$16384, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L588:
	movq	1512(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L590:
	movl	$258, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L589:
	movl	$102400, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L587:
	xorl	%eax, %eax
	cmpl	$256, (%rdi)
	sete	%al
	leaq	1(%rax,%rax), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L586:
	movl	$64, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L582:
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L585:
	movl	$20000, %eax
	ret
	.cfi_endproc
.LFE1034:
	.size	ossl_statem_client_max_message_size, .-ossl_statem_client_max_message_size
	.p2align 4
	.globl	dtls_process_hello_verify
	.type	dtls_process_hello_verify, @function
dtls_process_hello_verify:
.LFB1038:
	.cfi_startproc
	endbr64
	movq	8(%rsi), %rax
	movq	%rdi, %r10
	cmpq	$1, %rax
	jbe	.L594
	movq	(%rsi), %rcx
	leaq	2(%rcx), %rdx
	movq	%rdx, (%rsi)
	leaq	-2(%rax), %rdx
	movq	%rdx, 8(%rsi)
	testq	%rdx, %rdx
	je	.L594
	movzbl	2(%rcx), %edx
	subq	$3, %rax
	cmpq	%rdx, %rax
	jb	.L594
	leaq	3(%rcx), %r8
	subq	%rdx, %rax
	leaq	(%r8,%rdx), %rdi
	movq	%rax, 8(%rsi)
	movq	176(%r10), %rax
	movq	%rdi, (%rsi)
	cmpl	$8, %edx
	jnb	.L595
	testb	$4, %dl
	jne	.L610
	testl	%edx, %edx
	je	.L596
	movzbl	3(%rcx), %ecx
	movb	%cl, (%rax)
	testb	$2, %dl
	jne	.L605
.L609:
	movq	176(%r10), %rax
.L596:
	movq	%rdx, 256(%rax)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L594:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1302, %r9d
	movl	$159, %ecx
	movq	%r10, %rdi
	leaq	.LC0(%rip), %r8
	movl	$386, %edx
	movl	$50, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L595:
	.cfi_restore 6
	movq	3(%rcx), %rcx
	leaq	8(%rax), %rdi
	movq	%r8, %rsi
	andq	$-8, %rdi
	movq	%rcx, (%rax)
	movq	-8(%r8,%rdx), %rcx
	movq	%rcx, -8(%rax,%rdx)
	subq	%rdi, %rax
	subq	%rax, %rsi
	addl	%edx, %eax
	shrl	$3, %eax
	movl	%eax, %ecx
	rep movsq
	jmp	.L609
	.p2align 4,,10
	.p2align 3
.L610:
	movl	3(%rcx), %ecx
	movl	%ecx, (%rax)
	movl	-4(%r8,%rdx), %ecx
	movl	%ecx, -4(%rax,%rdx)
	movq	176(%r10), %rax
	jmp	.L596
.L605:
	movzwl	-2(%r8,%rdx), %ecx
	movw	%cx, -2(%rax,%rdx)
	movq	176(%r10), %rax
	jmp	.L596
	.cfi_endproc
.LFE1038:
	.size	dtls_process_hello_verify, .-dtls_process_hello_verify
	.p2align 4
	.globl	tls_process_server_hello
	.type	tls_process_server_hello, @function
tls_process_server_hello:
.LFB1040:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1414, %r9d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8(%rsi), %rax
	movq	$0, -96(%rbp)
	cmpq	$1, %rax
	jbe	.L781
	movq	(%rsi), %r13
	leaq	-2(%rax), %rdx
	movzwl	0(%r13), %ebx
	leaq	2(%r13), %rcx
	movq	%rdx, 8(%rsi)
	movq	%rcx, (%rsi)
	rolw	$8, %bx
	cmpl	$772, (%rdi)
	jne	.L614
	cmpw	$771, %bx
	je	.L782
.L614:
	movq	168(%r12), %rax
	cmpq	$31, %rdx
	jbe	.L615
	addq	$152, %rax
.L670:
	movdqu	2(%r13), %xmm0
	xorl	%r15d, %r15d
	movups	%xmm0, (%rax)
	movdqu	16(%rcx), %xmm1
	movups	%xmm1, 16(%rax)
	movq	(%rsi), %rax
	leaq	32(%rax), %r13
	movq	8(%rsi), %rax
	movq	%r13, (%rsi)
	subq	$32, %rax
	movq	%rax, 8(%rsi)
.L619:
	testq	%rax, %rax
	je	.L620
	movzbl	0(%r13), %r14d
	subq	$1, %rax
	cmpq	%r14, %rax
	jb	.L620
	addq	$1, %r13
	subq	%r14, %rax
	leaq	0(%r13,%r14), %r10
	movq	%rax, 8(%rsi)
	movq	%r10, (%rsi)
	cmpq	$32, %r14
	ja	.L783
	cmpq	$1, %rax
	jbe	.L623
	leaq	2(%r10), %rdx
	movq	%rdx, (%rsi)
	leaq	-2(%rax), %rdx
	movq	%rdx, 8(%rsi)
	testq	%rdx, %rdx
	je	.L784
	leaq	3(%r10), %rdx
	movzbl	2(%r10), %r11d
	movq	%rdx, (%rsi)
	leaq	-3(%rax), %rdx
	movq	%rdx, 8(%rsi)
	testq	%rdx, %rdx
	jne	.L785
	testl	%r15d, %r15d
	jne	.L628
	movq	$0, -80(%rbp)
	movq	$0, -72(%rbp)
.L629:
	xorl	%r8d, %r8d
	leaq	-96(%rbp), %rcx
	leaq	-80(%rbp), %rsi
	movl	$768, %edx
	movl	$1, %r9d
	movq	%r12, %rdi
	movb	%r11b, -128(%rbp)
	movq	%r10, -120(%rbp)
	call	tls_collect_extensions@PLT
	testl	%eax, %eax
	je	.L613
	movq	-96(%rbp), %rdx
	movzwl	%bx, %esi
	movq	%r12, %rdi
	call	ssl_choose_client_version@PLT
	movq	-120(%rbp), %r10
	movzbl	-128(%rbp), %r11d
	testl	%eax, %eax
	je	.L613
	movq	8(%r12), %rcx
	movzbl	%r11b, %ebx
	movq	192(%rcx), %rax
	testb	$8, 96(%rax)
	je	.L786
.L679:
	movl	$256, %r15d
	movl	$256, %esi
.L646:
	movq	-96(%rbp), %rdx
	movq	%r12, %rdi
	movq	%r10, -120(%rbp)
	call	tls_validate_all_contexts@PLT
	movq	-120(%rbp), %r10
	testl	%eax, %eax
	je	.L787
	movq	8(%r12), %rax
	movl	$0, 200(%r12)
	movq	192(%rax), %rdx
	testb	$8, 96(%rdx)
	jne	.L648
	movl	(%rax), %eax
	cmpl	$771, %eax
	jle	.L648
	cmpl	$65536, %eax
	jne	.L788
.L648:
	movq	1296(%r12), %rcx
	cmpl	$768, (%r12)
	movq	%rcx, %rdx
	jle	.L651
	movq	1760(%r12), %rax
	testq	%rax, %rax
	je	.L651
	cmpq	$0, 552(%rcx)
	je	.L651
	leaq	80(%rcx), %rsi
	movq	%r10, -120(%rbp)
	xorl	%ecx, %ecx
	leaq	-100(%rbp), %rdx
	movq	$0, -88(%rbp)
	movq	1768(%r12), %r9
	leaq	-88(%rbp), %r8
	movq	%r12, %rdi
	movl	$256, -100(%rbp)
	call	*%rax
	testl	%eax, %eax
	je	.L652
	movslq	-100(%rbp), %rax
	testl	%eax, %eax
	jle	.L652
	movq	1296(%r12), %rcx
	movq	-120(%rbp), %r10
	movq	%rax, 8(%rcx)
	movq	-88(%rbp), %rax
	testq	%rax, %rax
	je	.L789
.L653:
	movq	%rax, 504(%rcx)
	movq	%rcx, %rdx
	testq	%r14, %r14
	je	.L655
	cmpq	%r14, 336(%rcx)
	je	.L674
.L655:
	movl	200(%r12), %eax
	testl	%eax, %eax
	je	.L658
.L657:
	movq	%r10, -120(%rbp)
	movq	1256(%r12), %rdx
	cmpq	376(%rcx), %rdx
	jne	.L659
	leaq	384(%rcx), %rdi
	leaq	1264(%r12), %rsi
	movq	%rcx, -128(%rbp)
	call	memcmp@PLT
	movq	-128(%rbp), %rcx
	movq	-120(%rbp), %r10
	testl	%eax, %eax
	jne	.L659
.L660:
	movl	(%rcx), %eax
	cmpl	%eax, (%r12)
	je	.L665
	movl	$1636, %r9d
	leaq	.LC0(%rip), %r8
	movl	$210, %ecx
	movq	%r12, %rdi
	movl	$369, %edx
	movl	$70, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L613
	.p2align 4,,10
	.p2align 3
.L782:
	cmpq	$31, %rdx
	ja	.L790
	.p2align 4,,10
	.p2align 3
.L615:
	movl	$1433, %r9d
.L781:
	leaq	.LC0(%rip), %r8
	movl	$159, %ecx
	movl	$369, %edx
	movq	%r12, %rdi
	movl	$50, %esi
	call	ossl_statem_fatal@PLT
.L613:
	movq	-96(%rbp), %rdi
	movl	$1745, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	xorl	%r11d, %r11d
.L611:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L791
	addq	$104, %rsp
	movl	%r11d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L620:
	.cfi_restore_state
	movl	$1441, %r9d
	jmp	.L781
	.p2align 4,,10
	.p2align 3
.L790:
	movq	8+hrrrandom(%rip), %rdi
	movq	hrrrandom(%rip), %rdx
	xorq	8(%rcx), %rdi
	xorq	2(%r13), %rdx
	orq	%rdx, %rdi
	jne	.L616
	movq	24+hrrrandom(%rip), %rdi
	movq	16+hrrrandom(%rip), %rdx
	xorq	24(%rcx), %rdi
	xorq	16(%rcx), %rdx
	orq	%rdx, %rdi
	jne	.L616
	addq	$34, %r13
	subq	$34, %rax
	movl	$1, %r15d
	movl	$1, 1248(%r12)
	movq	%r13, (%rsi)
	movq	%rax, 8(%rsi)
	jmp	.L619
	.p2align 4,,10
	.p2align 3
.L784:
	movl	$1460, %r9d
	jmp	.L781
	.p2align 4,,10
	.p2align 3
.L616:
	movq	168(%r12), %rax
	addq	$152, %rax
	jmp	.L670
	.p2align 4,,10
	.p2align 3
.L623:
	movl	$1454, %r9d
	jmp	.L781
	.p2align 4,,10
	.p2align 3
.L783:
	movl	$1448, %r9d
	leaq	.LC0(%rip), %r8
	movl	$300, %ecx
	movq	%r12, %rdi
	movl	$369, %edx
	movl	$47, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L613
	.p2align 4,,10
	.p2align 3
.L785:
	cmpq	$1, %rdx
	je	.L628
	movzwl	3(%r10), %edx
	subq	$5, %rax
	rolw	$8, %dx
	movzwl	%dx, %edx
	cmpq	%rdx, %rax
	je	.L792
.L628:
	movl	$1470, %r9d
	leaq	.LC0(%rip), %r8
	movl	$271, %ecx
	movq	%r12, %rdi
	movl	$369, %edx
	movl	$50, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L613
.L792:
	leaq	5(%r10), %rdx
	movq	$0, 8(%rsi)
	leaq	(%rdx,%rax), %rcx
	movq	%rdx, -80(%rbp)
	movq	%rcx, (%rsi)
	movq	%rax, -72(%rbp)
	testl	%r15d, %r15d
	je	.L629
	movq	8(%r12), %rcx
	movq	192(%rcx), %rax
	movl	96(%rax), %ebx
	andl	$8, %ebx
	jne	.L673
	movl	(%rcx), %eax
	cmpl	$65536, %eax
	je	.L780
	cmpl	$771, %eax
	jle	.L780
.L673:
	testl	%r11d, %r11d
	jne	.L793
	movl	%r11d, -132(%rbp)
	movq	%rcx, -128(%rbp)
	movq	%r10, -120(%rbp)
	cmpq	%r14, 1368(%r12)
	jne	.L637
	leaq	1336(%r12), %rsi
	movq	%r14, %rdx
	movq	%r13, %rdi
	call	memcmp@PLT
	movq	-120(%rbp), %r10
	movq	-128(%rbp), %rcx
	testl	%eax, %eax
	movl	-132(%rbp), %r11d
	jne	.L637
	movl	%r11d, -120(%rbp)
	testl	%r15d, %r15d
	je	.L639
	movq	%r10, %rsi
	movq	%r12, %rdi
	call	set_client_ciphersuite
	testl	%eax, %eax
	je	.L613
	movq	1136(%r12), %rdi
	movq	$0, -88(%rbp)
	call	EVP_CIPHER_CTX_free@PLT
	xorl	%r8d, %r8d
	leaq	-88(%rbp), %rcx
	leaq	-80(%rbp), %rsi
	movl	$1, %r9d
	movl	$2048, %edx
	movq	%r12, %rdi
	movq	$0, 1136(%r12)
	call	tls_collect_extensions@PLT
	movl	-120(%rbp), %r11d
	testl	%eax, %eax
	je	.L641
	movq	-88(%rbp), %rdx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$1, %r9d
	movl	$2048, %esi
	movq	%r12, %rdi
	call	tls_parse_all_extensions@PLT
	movl	-120(%rbp), %r11d
	testl	%eax, %eax
	je	.L641
	movq	-88(%rbp), %rdi
	movl	$1769, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, -88(%rbp)
	movl	-120(%rbp), %r11d
	cmpq	$0, 1832(%r12)
	jne	.L643
	movq	168(%r12), %rax
	cmpq	$0, 576(%rax)
	jne	.L794
.L643:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	movl	%r11d, -120(%rbp)
	call	create_synthetic_message_hash@PLT
	movl	-120(%rbp), %r11d
	testl	%eax, %eax
	je	.L641
	movq	152(%r12), %rax
	movq	%r12, %rdi
	leaq	4(%rax), %rdx
	movq	136(%r12), %rax
	movq	8(%rax), %rsi
	call	ssl3_finish_mac@PLT
	movl	-120(%rbp), %r11d
	testl	%eax, %eax
	je	.L641
	movl	$1, %r11d
	jmp	.L611
	.p2align 4,,10
	.p2align 3
.L637:
	movl	$1501, %r9d
	leaq	.LC0(%rip), %r8
	movl	$999, %ecx
	movq	%r12, %rdi
	movl	$369, %edx
	movl	$47, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L613
.L651:
	testq	%r14, %r14
	je	.L658
	cmpq	336(%rcx), %r14
	jne	.L658
.L674:
	leaq	344(%rcx), %rsi
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	%r10, -128(%rbp)
	movq	%rcx, -120(%rbp)
	call	memcmp@PLT
	movq	-120(%rbp), %rcx
	movq	-128(%rbp), %r10
	testl	%eax, %eax
	jne	.L656
	movl	$1, 200(%r12)
	jmp	.L657
.L658:
	cmpq	$0, 336(%rcx)
	jne	.L671
.L661:
	movq	8(%r12), %rcx
	movl	(%r12), %eax
	movq	192(%rcx), %rsi
	movl	%eax, (%rdx)
	testb	$8, 96(%rsi)
	jne	.L662
	movl	(%rcx), %ecx
	cmpl	$771, %ecx
	jle	.L662
	cmpl	$65536, %ecx
	je	.L662
.L665:
	movq	168(%r12), %rdx
	movq	%r10, %rsi
	movq	%r12, %rdi
	movl	%eax, 828(%rdx)
	movl	%eax, 832(%rdx)
	call	set_client_ciphersuite
	testl	%eax, %eax
	je	.L613
	testl	%ebx, %ebx
	jne	.L795
	movq	1296(%r12), %rax
	cmpl	$0, 496(%rax)
	jne	.L796
	movq	-96(%rbp), %rdx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$1, %r9d
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	tls_parse_all_extensions@PLT
	testl	%eax, %eax
	je	.L613
	movq	8(%r12), %rdx
	movq	192(%rdx), %rax
	testb	$8, 96(%rax)
	jne	.L669
	movl	(%rdx), %edx
	cmpl	$65536, %edx
	je	.L669
	cmpl	$771, %edx
	jle	.L669
	movq	%r12, %rdi
	call	*16(%rax)
	testl	%eax, %eax
	je	.L613
	movq	8(%r12), %rax
	movl	$145, %esi
	movq	%r12, %rdi
	movq	192(%rax), %rax
	call	*32(%rax)
	testl	%eax, %eax
	je	.L613
.L669:
	movq	-96(%rbp), %rdi
	movl	$1742, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movl	$3, %r11d
	jmp	.L611
.L786:
	movl	(%rcx), %eax
	cmpl	$771, %eax
	jle	.L679
	cmpl	$65536, %eax
	je	.L679
	movl	%ebx, %r11d
.L780:
	xorl	%ebx, %ebx
	jmp	.L673
.L793:
	movl	$1492, %r9d
	leaq	.LC0(%rip), %r8
	movl	$341, %ecx
	movq	%r12, %rdi
	movl	$369, %edx
	movl	$47, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L613
.L662:
	movq	%r14, 336(%rdx)
	testq	%r14, %r14
	je	.L665
	addq	$344, %rdx
	movq	%r13, %rsi
	movq	%r14, %rcx
	movq	%rdx, %rdi
	rep movsb
	movq	1296(%r12), %rcx
	jmp	.L660
.L787:
	movl	$1523, %r9d
	leaq	.LC0(%rip), %r8
	movl	$110, %ecx
	movq	%r12, %rdi
	movl	$369, %edx
	movl	$47, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L613
.L659:
	movl	$1597, %r9d
	leaq	.LC0(%rip), %r8
	movl	$272, %ecx
	movq	%r12, %rdi
	movl	$369, %edx
	movl	$47, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L613
.L794:
	movl	$1781, %r9d
	leaq	.LC0(%rip), %r8
	movl	$214, %ecx
	movq	%r12, %rdi
	movl	$610, %edx
	movl	$47, %esi
	call	ossl_statem_fatal@PLT
	movl	-120(%rbp), %r11d
.L641:
	movq	-88(%rbp), %rdi
	movl	$1810, %edx
	leaq	.LC0(%rip), %rsi
	movl	%r11d, -120(%rbp)
	call	CRYPTO_free@PLT
	movl	-120(%rbp), %r11d
	jmp	.L611
.L656:
	cmpl	$0, 200(%r12)
	jne	.L657
.L671:
	movq	1904(%r12), %rax
	movq	%r10, -120(%rbp)
	lock addl	$1, 136(%rax)
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	ssl_get_new_session@PLT
	testl	%eax, %eax
	je	.L613
	movq	1296(%r12), %rdx
	movq	-120(%rbp), %r10
	jmp	.L661
.L788:
	leaq	2112(%r12), %rdi
	movq	%r10, -120(%rbp)
	call	RECORD_LAYER_processed_read_pending@PLT
	movq	-120(%rbp), %r10
	testl	%eax, %eax
	jne	.L797
	movq	-96(%rbp), %rcx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$512, %edx
	movl	$25, %esi
	movq	%r12, %rdi
	movq	%r10, -120(%rbp)
	call	tls_parse_extension@PLT
	movq	-120(%rbp), %r10
	testl	%eax, %eax
	je	.L613
	movq	1296(%r12), %rcx
	movq	%rcx, %rdx
	jmp	.L655
.L791:
	call	__stack_chk_fail@PLT
.L652:
	movl	$1580, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movq	%r12, %rdi
	movl	$369, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L613
.L795:
	movl	$1654, %r9d
	leaq	.LC0(%rip), %r8
	movl	$257, %ecx
	movq	%r12, %rdi
	movl	$369, %edx
	movl	$47, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L613
.L796:
	movl	$1663, %r9d
	leaq	.LC0(%rip), %r8
	movl	$340, %ecx
	movq	%r12, %rdi
	movl	$369, %edx
	movl	$40, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L613
.L797:
	movl	$1536, %r9d
	leaq	.LC0(%rip), %r8
	movl	$182, %ecx
	movq	%r12, %rdi
	movl	$369, %edx
	movl	$10, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L613
.L789:
	movq	%r10, %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	ssl_get_cipher_by_char@PLT
	movq	1296(%r12), %rcx
	movq	-120(%rbp), %r10
	jmp	.L653
.L639:
	testl	%ebx, %ebx
	jne	.L678
	movl	(%rcx), %eax
	cmpl	$65536, %eax
	je	.L679
	cmpl	$771, %eax
	jle	.L679
	movl	$512, %r15d
	movl	$512, %esi
	jmp	.L646
.L678:
	xorl	%ebx, %ebx
	movl	$256, %r15d
	movl	$256, %esi
	jmp	.L646
	.cfi_endproc
.LFE1040:
	.size	tls_process_server_hello, .-tls_process_server_hello
	.p2align 4
	.globl	tls_process_server_certificate
	.type	tls_process_server_certificate, @function
tls_process_server_certificate:
.LFB1042:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L860
	movq	8(%r13), %rdx
	movq	8(%rbx), %rax
	movq	192(%rdx), %rcx
	testb	$8, 96(%rcx)
	jne	.L801
	movl	(%rdx), %edx
	cmpl	$771, %edx
	jle	.L801
	cmpl	$65536, %edx
	jne	.L861
	.p2align 4,,10
	.p2align 3
.L801:
	cmpq	$2, %rax
	jbe	.L803
	movq	(%rbx), %rcx
	movzbl	(%rcx), %edx
	movzbl	1(%rcx), %esi
	addq	$3, %rcx
	salq	$8, %rsi
	salq	$16, %rdx
	orq	%rsi, %rdx
	movzbl	-1(%rcx), %esi
	movq	%rcx, (%rbx)
	orq	%rsi, %rdx
	subq	$3, %rax
	movq	%rax, 8(%rbx)
	je	.L803
	cmpq	%rax, %rdx
	je	.L862
.L803:
	movl	$1838, %r9d
	leaq	.LC0(%rip), %r8
	movl	$159, %ecx
.L859:
	movl	$367, %edx
	movl	$50, %esi
	movq	%r13, %rdi
	xorl	%r14d, %r14d
	call	ossl_statem_fatal@PLT
	xorl	%r10d, %r10d
.L800:
	movq	%r10, %rdi
	call	X509_free@PLT
	movq	X509_free@GOTPCREL(%rip), %rsi
	movq	%r12, %rdi
	call	OPENSSL_sk_pop_free@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L863
	addq	$72, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L862:
	.cfi_restore_state
	movq	$0, -104(%rbp)
	leaq	-96(%rbp), %r15
.L814:
	cmpq	$2, %rax
	jbe	.L804
	movq	(%rbx), %rcx
	subq	$3, %rax
	movzbl	(%rcx), %edx
	movzbl	1(%rcx), %esi
	addq	$3, %rcx
	salq	$8, %rsi
	salq	$16, %rdx
	orq	%rsi, %rdx
	movzbl	-1(%rcx), %esi
	movq	%rcx, (%rbx)
	movq	%rax, 8(%rbx)
	orq	%rsi, %rdx
	cmpq	%rax, %rdx
	ja	.L804
	leaq	(%rcx,%rdx), %r14
	subq	%rdx, %rax
	movq	%r15, %rsi
	xorl	%edi, %edi
	movq	%r14, (%rbx)
	movq	%rax, 8(%rbx)
	movq	%rcx, -96(%rbp)
	call	d2i_X509@PLT
	movq	%rax, %r10
	testq	%rax, %rax
	je	.L864
	cmpq	%r14, -96(%rbp)
	jne	.L865
	movq	8(%r13), %rax
	movq	192(%rax), %rdx
	movl	96(%rdx), %r11d
	andl	$8, %r11d
	movl	%r11d, %r14d
	jne	.L808
	movl	(%rax), %eax
	cmpl	$65536, %eax
	je	.L808
	cmpl	$771, %eax
	jle	.L808
	movq	8(%rbx), %rax
	movq	$0, -88(%rbp)
	cmpq	$1, %rax
	jbe	.L809
	movq	(%rbx), %rcx
	leaq	-2(%rax), %rdx
	movzwl	(%rcx), %eax
	rolw	$8, %ax
	movzwl	%ax, %eax
	cmpq	%rax, %rdx
	jb	.L809
	addq	$2, %rcx
	subq	%rax, %rdx
	xorl	%r9d, %r9d
	cmpq	$0, -104(%rbp)
	leaq	(%rcx,%rax), %rsi
	movq	%rdx, 8(%rbx)
	sete	%r9b
	movq	%r13, %rdi
	movq	%rsi, (%rbx)
	xorl	%r8d, %r8d
	leaq	-80(%rbp), %rsi
	movl	$4096, %edx
	movq	%rcx, -80(%rbp)
	leaq	-88(%rbp), %rcx
	movq	%r10, -112(%rbp)
	movq	%rax, -72(%rbp)
	call	tls_collect_extensions@PLT
	movq	-112(%rbp), %r10
	testl	%eax, %eax
	je	.L812
	xorl	%r9d, %r9d
	movq	-104(%rbp), %r8
	cmpq	$0, 8(%rbx)
	movq	%r10, %rcx
	movq	-88(%rbp), %rdx
	sete	%r9b
	movl	$4096, %esi
	movq	%r13, %rdi
	movq	%r10, -112(%rbp)
	call	tls_parse_all_extensions@PLT
	movq	-112(%rbp), %r10
	testl	%eax, %eax
	je	.L812
	movq	-88(%rbp), %rdi
	movl	$1885, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-112(%rbp), %r10
	.p2align 4,,10
	.p2align 3
.L808:
	movq	%r10, %rsi
	movq	%r12, %rdi
	movq	%r10, -112(%rbp)
	call	OPENSSL_sk_push@PLT
	movq	-112(%rbp), %r10
	testl	%eax, %eax
	je	.L866
	movq	8(%rbx), %rax
	addq	$1, -104(%rbp)
	testq	%rax, %rax
	jne	.L814
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	ssl_verify_cert_chain@PLT
	movl	%eax, %r14d
	movl	1376(%r13), %eax
	testl	%eax, %eax
	je	.L815
	testl	%r14d, %r14d
	jle	.L867
.L815:
	call	ERR_clear_error@PLT
	cmpl	$1, %r14d
	jg	.L868
	movq	1296(%r13), %rax
	movq	%r12, %rdi
	xorl	%esi, %esi
	movq	%r12, 456(%rax)
	call	OPENSSL_sk_value@PLT
	movq	%rax, %rdi
	movq	%rax, %rbx
	call	X509_get0_pubkey@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L818
	movq	%rax, %rdi
	call	EVP_PKEY_missing_parameters@PLT
	testl	%eax, %eax
	jne	.L818
	leaq	-80(%rbp), %rsi
	movq	%r12, %rdi
	call	ssl_cert_lookup_by_pkey@PLT
	movq	%rax, %r10
	testq	%rax, %rax
	je	.L869
	movq	8(%r13), %rax
	movq	192(%rax), %rdx
	testb	$8, 96(%rdx)
	jne	.L820
	movl	(%rax), %eax
	cmpl	$65536, %eax
	je	.L820
	cmpl	$771, %eax
	jg	.L821
.L820:
	movq	168(%r13), %rax
	movq	568(%rax), %rax
	movl	32(%rax), %r11d
	andl	4(%r10), %r11d
	movl	%r11d, %r14d
	je	.L870
.L821:
	movq	1296(%r13), %rax
	movq	-80(%rbp), %rdx
	movl	%edx, 448(%rax)
	movq	440(%rax), %rdi
	call	X509_free@PLT
	movq	%rbx, %rdi
	call	X509_up_ref@PLT
	movq	1296(%r13), %rax
	movq	1456(%r13), %rdx
	movq	%rbx, 440(%rax)
	movq	%rdx, 464(%rax)
	movq	8(%r13), %rax
	movq	192(%rax), %rdx
	movl	96(%rdx), %r11d
	andl	$8, %r11d
	movl	%r11d, %r14d
	jne	.L823
	movl	(%rax), %eax
	cmpl	$65536, %eax
	je	.L823
	cmpl	$771, %eax
	jle	.L823
	leaq	1240(%r13), %rcx
	leaq	1176(%r13), %rsi
	movl	$64, %edx
	movq	%r13, %rdi
	call	ssl_handshake_hash@PLT
	testl	%eax, %eax
	jne	.L823
	xorl	%r12d, %r12d
	xorl	%r10d, %r10d
	jmp	.L800
	.p2align 4,,10
	.p2align 3
.L861:
	testq	%rax, %rax
	je	.L803
	movq	(%rbx), %rdx
	subq	$1, %rax
	movzbl	(%rdx), %ecx
	addq	$1, %rdx
	movq	%rax, 8(%rbx)
	movq	%rdx, (%rbx)
	testb	%cl, %cl
	je	.L801
	jmp	.L803
	.p2align 4,,10
	.p2align 3
.L804:
	movl	$1845, %r9d
	leaq	.LC0(%rip), %r8
	movl	$135, %ecx
	jmp	.L859
	.p2align 4,,10
	.p2align 3
.L860:
	movl	$65, %ecx
	movl	$367, %edx
	movq	%r13, %rdi
	xorl	%r14d, %r14d
	movl	$1828, %r9d
	leaq	.LC0(%rip), %r8
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	xorl	%r10d, %r10d
	jmp	.L800
.L809:
	movl	$1870, %r9d
	leaq	.LC0(%rip), %r8
	movl	$271, %ecx
	movq	%r13, %rdi
	movl	$367, %edx
	movl	$50, %esi
	movq	%r10, -104(%rbp)
	call	ossl_statem_fatal@PLT
	movq	-104(%rbp), %r10
	jmp	.L800
.L865:
	movl	$135, %ecx
	movl	$367, %edx
	movq	%r13, %rdi
	xorl	%r14d, %r14d
	movl	$1859, %r9d
	leaq	.LC0(%rip), %r8
	movl	$50, %esi
	movq	%rax, -104(%rbp)
	call	ossl_statem_fatal@PLT
	movq	-104(%rbp), %r10
	jmp	.L800
.L864:
	movl	$13, %ecx
	movl	$367, %edx
	movq	%r13, %rdi
	xorl	%r14d, %r14d
	movl	$1854, %r9d
	leaq	.LC0(%rip), %r8
	movl	$42, %esi
	movq	%rax, -104(%rbp)
	call	ossl_statem_fatal@PLT
	movq	-104(%rbp), %r10
	jmp	.L800
.L868:
	movl	%r14d, %ecx
	movl	$1920, %r9d
	movq	%r13, %rdi
	xorl	%r14d, %r14d
	leaq	.LC0(%rip), %r8
	movl	$367, %edx
	movl	$40, %esi
	call	ossl_statem_fatal@PLT
	xorl	%r10d, %r10d
	jmp	.L800
.L866:
	movl	$65, %ecx
	movl	$367, %edx
	movq	%r13, %rdi
	xorl	%r14d, %r14d
	movl	$1889, %r9d
	leaq	.LC0(%rip), %r8
	movl	$80, %esi
	movq	%r10, -104(%rbp)
	call	ossl_statem_fatal@PLT
	movq	-104(%rbp), %r10
	jmp	.L800
.L818:
	movl	$239, %ecx
	movl	$367, %edx
	movq	%r13, %rdi
	xorl	%r12d, %r12d
	movl	$1937, %r9d
	leaq	.LC0(%rip), %r8
	movl	$80, %esi
	xorl	%r14d, %r14d
	call	ossl_statem_fatal@PLT
	xorl	%r10d, %r10d
	jmp	.L800
.L823:
	xorl	%r12d, %r12d
	xorl	%r10d, %r10d
	movl	$3, %r14d
	jmp	.L800
.L863:
	call	__stack_chk_fail@PLT
.L867:
	movl	1456(%r13), %edi
	xorl	%r14d, %r14d
	call	ssl_x509err2alert@PLT
	movl	$1913, %r9d
	movl	$134, %ecx
	movq	%r13, %rdi
	movl	%eax, %esi
	leaq	.LC0(%rip), %r8
	movl	$367, %edx
	call	ossl_statem_fatal@PLT
	xorl	%r10d, %r10d
	jmp	.L800
.L812:
	movq	-88(%rbp), %rdi
	movl	$1881, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r10, -104(%rbp)
	call	CRYPTO_free@PLT
	movq	-104(%rbp), %r10
	jmp	.L800
.L869:
	movl	$247, %ecx
	movl	$367, %edx
	movq	%r13, %rdi
	xorl	%r12d, %r12d
	movl	$1944, %r9d
	movl	$47, %esi
	movq	%rax, -104(%rbp)
	xorl	%r14d, %r14d
	leaq	.LC0(%rip), %r8
	call	ossl_statem_fatal@PLT
	movq	-104(%rbp), %r10
	jmp	.L800
.L870:
	movl	$383, %ecx
	movl	$367, %edx
	movq	%r13, %rdi
	xorl	%r12d, %r12d
	movl	$1957, %r9d
	leaq	.LC0(%rip), %r8
	movl	$47, %esi
	call	ossl_statem_fatal@PLT
	xorl	%r10d, %r10d
	jmp	.L800
	.cfi_endproc
.LFE1042:
	.size	tls_process_server_certificate, .-tls_process_server_certificate
	.section	.rodata.str1.8
	.align 8
.LC2:
	.string	"../deps/openssl/openssl/ssl/statem/../packet_local.h"
	.text
	.p2align 4
	.globl	tls_process_certificate_request
	.type	tls_process_certificate_request, @function
tls_process_certificate_request:
.LFB1048:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$56, %rsp
	movq	168(%rdi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$0, 816(%r14)
	movups	%xmm0, 784(%r14)
	movups	%xmm0, 800(%r14)
	movq	8(%rdi), %rdx
	movq	192(%rdx), %rax
	movl	96(%rax), %eax
	andl	$8, %eax
	jne	.L872
	movl	%eax, %r13d
	movl	(%rdx), %eax
	cmpl	$65536, %eax
	je	.L872
	cmpl	$771, %eax
	jg	.L939
.L872:
	movq	8(%rbx), %rax
	testq	%rax, %rax
	je	.L882
	movq	(%rbx), %rdx
	subq	$1, %rax
	movzbl	(%rdx), %r13d
	cmpq	%r13, %rax
	jb	.L882
	leaq	1(%rdx), %r15
	subq	%r13, %rax
	leaq	.LC2(%rip), %rsi
	leaq	(%r15,%r13), %rdx
	movq	%rax, 8(%rbx)
	movq	%rdx, (%rbx)
	movq	592(%r14), %rdi
	movl	$420, %edx
	call	CRYPTO_free@PLT
	movq	$0, 592(%r14)
	movq	$0, 600(%r14)
	testq	%r13, %r13
	jne	.L940
.L883:
	movq	8(%r12), %rax
	movq	192(%rax), %rax
	testb	$2, 96(%rax)
	je	.L886
	movq	8(%rbx), %rax
	cmpq	$1, %rax
	jbe	.L887
	movq	(%rbx), %rcx
	leaq	-2(%rax), %rdx
	movzwl	(%rcx), %eax
	rolw	$8, %ax
	movzwl	%ax, %eax
	cmpq	%rax, %rdx
	jb	.L887
	addq	$2, %rcx
	subq	%rax, %rdx
	movq	%r12, %rdi
	movq	%rax, -72(%rbp)
	leaq	(%rcx,%rax), %rsi
	movq	%rdx, 8(%rbx)
	xorl	%edx, %edx
	movq	%rsi, (%rbx)
	leaq	-80(%rbp), %rsi
	movq	%rcx, -80(%rbp)
	call	tls1_save_sigalgs@PLT
	testl	%eax, %eax
	je	.L941
	movq	%r12, %rdi
	call	tls1_process_sigalgs@PLT
	testl	%eax, %eax
	je	.L942
.L886:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	parse_ca_names@PLT
	testl	%eax, %eax
	je	.L937
.L880:
	cmpq	$0, 8(%rbx)
	jne	.L943
	movq	168(%r12), %rax
	movl	$2, %r13d
	movl	$1, 584(%rax)
	movq	8(%r12), %rax
	movq	192(%rax), %rdx
	testb	$8, 96(%rdx)
	jne	.L871
	movl	(%rax), %eax
	cmpl	$65536, %eax
	je	.L896
	cmpl	$771, %eax
	jle	.L896
	xorl	%r13d, %r13d
	cmpl	$4, 1936(%r12)
	setne	%r13b
	addl	$2, %r13d
	jmp	.L871
	.p2align 4,,10
	.p2align 3
.L882:
	movl	$2500, %r9d
.L938:
	leaq	.LC0(%rip), %r8
	movl	$159, %ecx
	movl	$361, %edx
	movq	%r12, %rdi
	movl	$50, %esi
	call	ossl_statem_fatal@PLT
.L937:
	xorl	%r13d, %r13d
.L871:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L944
	addq	$56, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L940:
	.cfi_restore_state
	movl	$429, %ecx
	leaq	.LC2(%rip), %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	CRYPTO_memdup@PLT
	movq	%rax, 592(%r14)
	testq	%rax, %rax
	je	.L884
	movq	%r13, 600(%r14)
	jmp	.L883
	.p2align 4,,10
	.p2align 3
.L939:
	movq	$0, -88(%rbp)
	testb	$1, 68(%rdi)
	jne	.L894
	movq	592(%r14), %rdi
	movl	$2459, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	168(%r12), %rax
	movq	1944(%r12), %rdi
	movl	$2462, %edx
	leaq	.LC0(%rip), %rsi
	movq	$0, 592(%rax)
	movq	$0, 600(%rax)
	call	CRYPTO_free@PLT
	movq	8(%rbx), %rax
	movq	$0, 1944(%r12)
	testq	%rax, %rax
	je	.L874
	movq	(%rbx), %rdx
	subq	$1, %rax
	movzbl	(%rdx), %r14d
	cmpq	%r14, %rax
	jb	.L874
	leaq	1(%rdx), %r15
	subq	%r14, %rax
	leaq	.LC2(%rip), %rsi
	leaq	(%r15,%r14), %rdx
	movq	%rax, 8(%rbx)
	movq	%rdx, (%rbx)
	movq	1944(%r12), %rdi
	movl	$420, %edx
	call	CRYPTO_free@PLT
	movq	$0, 1944(%r12)
	movq	$0, 1952(%r12)
	testq	%r14, %r14
	je	.L875
	movl	$429, %ecx
	leaq	.LC2(%rip), %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	CRYPTO_memdup@PLT
	movq	%rax, 1944(%r12)
	testq	%rax, %rax
	je	.L874
	movq	%r14, 1952(%r12)
.L875:
	movq	8(%rbx), %rax
	cmpq	$1, %rax
	jbe	.L876
	movq	(%rbx), %rcx
	subq	$2, %rax
	movzwl	(%rcx), %edx
	rolw	$8, %dx
	movzwl	%dx, %edx
	cmpq	%rdx, %rax
	jb	.L876
	addq	$2, %rcx
	subq	%rdx, %rax
	movq	%rdx, -72(%rbp)
	xorl	%r8d, %r8d
	leaq	(%rcx,%rdx), %rsi
	movq	%rax, 8(%rbx)
	movl	$1, %r9d
	movq	%r12, %rdi
	movq	%rsi, (%rbx)
	movl	$16384, %edx
	leaq	-80(%rbp), %rsi
	movq	%rcx, -80(%rbp)
	leaq	-88(%rbp), %rcx
	call	tls_collect_extensions@PLT
	testl	%eax, %eax
	je	.L878
	movq	-88(%rbp), %rdx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$1, %r9d
	movl	$16384, %esi
	movq	%r12, %rdi
	call	tls_parse_all_extensions@PLT
	testl	%eax, %eax
	je	.L878
	movq	-88(%rbp), %rdi
	movl	$2488, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%r12, %rdi
	call	tls1_process_sigalgs@PLT
	testl	%eax, %eax
	jne	.L880
	movl	$2490, %r9d
	leaq	.LC0(%rip), %r8
	movl	$271, %ecx
	movq	%r12, %rdi
	movl	$361, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L871
	.p2align 4,,10
	.p2align 3
.L874:
	movl	$2467, %r9d
	leaq	.LC0(%rip), %r8
	movl	$159, %ecx
	movq	%r12, %rdi
	movl	$361, %edx
	movl	$50, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L871
.L887:
	movl	$2517, %r9d
	jmp	.L938
.L894:
	movl	$1, %r13d
	jmp	.L871
.L884:
	movl	$2507, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movq	%r12, %rdi
	movl	$361, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L937
.L896:
	movl	$2, %r13d
	jmp	.L871
.L942:
	movl	$2534, %r9d
	leaq	.LC0(%rip), %r8
	movl	$65, %ecx
	movq	%r12, %rdi
	movl	$361, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L937
.L941:
	movl	$2528, %r9d
	leaq	.LC0(%rip), %r8
	movl	$360, %ecx
	movq	%r12, %rdi
	movl	$361, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L937
.L943:
	movl	$2549, %r9d
	jmp	.L938
.L876:
	movl	$2474, %r9d
	leaq	.LC0(%rip), %r8
	movl	$271, %ecx
	movq	%r12, %rdi
	movl	$361, %edx
	movl	$50, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L871
.L878:
	movq	-88(%rbp), %rdi
	movl	$2485, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	jmp	.L871
.L944:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1048:
	.size	tls_process_certificate_request, .-tls_process_certificate_request
	.p2align 4
	.globl	tls_process_new_session_ticket
	.type	tls_process_new_session_ticket, @function
tls_process_new_session_ticket:
.LFB1049:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8(%rsi), %rax
	movq	$0, -88(%rbp)
	cmpq	$3, %rax
	jbe	.L950
	movq	(%rsi), %r13
	leaq	-4(%rax), %r10
	movq	%rsi, %rbx
	leaq	4(%r13), %r9
	movl	0(%r13), %r15d
	movq	%r10, 8(%rsi)
	movq	%r9, (%rsi)
	movq	8(%rdi), %rsi
	movq	192(%rsi), %rdx
	movl	96(%rdx), %edx
	andl	$8, %edx
	jne	.L948
	movl	(%rsi), %r8d
	cmpl	$65536, %r8d
	je	.L978
	cmpl	$771, %r8d
	jle	.L978
	cmpq	$3, %r10
	jbe	.L950
	leaq	8(%r13), %rdi
	movl	4(%r13), %r14d
	movq	%rdi, (%rbx)
	leaq	-8(%rax), %rdi
	movq	%rdi, 8(%rbx)
	testq	%rdi, %rdi
	je	.L950
	leaq	-9(%rax), %r10
	movzbl	8(%r13), %eax
	movq	%rax, -112(%rbp)
	cmpq	%rax, %r10
	jb	.L950
	leaq	9(%r13), %r9
	subq	%rax, %r10
	bswap	%r14d
	movl	%r14d, -100(%rbp)
	movq	%r9, -120(%rbp)
	addq	%rax, %r9
	movq	%r9, (%rbx)
	movq	%r10, 8(%rbx)
	jmp	.L949
	.p2align 4,,10
	.p2align 3
.L948:
	cmpq	$1, %r10
	ja	.L1025
	.p2align 4,,10
	.p2align 3
.L950:
	movl	$2589, %r9d
.L1028:
	leaq	.LC0(%rip), %r8
	movl	$159, %ecx
	movl	$366, %edx
	movq	%r12, %rdi
	movl	$50, %esi
	call	ossl_statem_fatal@PLT
.L947:
	movq	-88(%rbp), %rdi
	movl	$2746, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	xorl	%eax, %eax
.L945:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1030
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1025:
	.cfi_restore_state
	movzwl	4(%r13), %edi
	addq	$6, %r13
	movl	$0, -100(%rbp)
	movq	%r13, (%rbx)
	leaq	-6(%rax), %r13
	movq	%r13, 8(%rbx)
	rolw	$8, %di
	movq	$0, -112(%rbp)
	movzwl	%di, %edi
	movq	$0, -120(%rbp)
.L972:
	movl	%edi, %eax
	cmpq	%r13, %rax
	jne	.L950
	testl	%edi, %edi
	je	.L970
	movq	1296(%r12), %r14
	testl	%edx, %edx
	je	.L1031
.L954:
	cmpq	$0, 336(%r14)
	je	.L956
.L951:
	movq	%r14, %rdi
	xorl	%esi, %esi
	call	ssl_session_dup@PLT
	movl	$2618, %r9d
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L1029
	movq	1904(%r12), %rdi
	testb	$1, 72(%rdi)
	je	.L958
	movq	8(%r12), %rax
	movq	192(%rax), %rdx
	testb	$8, 96(%rdx)
	jne	.L959
	movl	(%rax), %eax
	cmpl	$771, %eax
	jle	.L959
	cmpl	$65536, %eax
	jne	.L958
.L959:
	movq	1296(%r12), %rsi
	call	SSL_CTX_remove_session@PLT
.L958:
	movq	1296(%r12), %rdi
	call	SSL_SESSION_free@PLT
	movq	%r14, 1296(%r12)
.L956:
	xorl	%edi, %edi
	call	time@PLT
	movl	$2644, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rax, 488(%r14)
	movq	1296(%r12), %rax
	movq	552(%rax), %rdi
	call	CRYPTO_free@PLT
	movq	1296(%r12), %r8
	movq	%r13, %rdi
	movl	$2648, %edx
	leaq	.LC0(%rip), %rsi
	movq	$0, 552(%r8)
	movq	$0, 560(%r8)
	movq	%r8, -128(%rbp)
	call	CRYPTO_malloc@PLT
	movq	-128(%rbp), %r8
	movq	%rax, 552(%r8)
	movq	1296(%r12), %rax
	movq	552(%rax), %rdi
	testq	%rdi, %rdi
	je	.L1032
	movl	$2655, %r9d
	cmpq	%r13, 8(%rbx)
	jb	.L1028
	movq	(%rbx), %rsi
	movq	%r13, %rdx
	leaq	-80(%rbp), %r14
	call	memcpy@PLT
	movq	(%rbx), %rsi
	movq	8(%rbx), %rax
	movl	%r15d, %ecx
	movq	1296(%r12), %rdx
	bswap	%ecx
	movl	%ecx, %ecx
	addq	%r13, %rsi
	subq	%r13, %rax
	movq	%rsi, (%rbx)
	movq	%rax, 8(%rbx)
	movq	%rcx, 568(%rdx)
	movl	-100(%rbp), %ecx
	movq	%r13, 560(%rdx)
	movl	%ecx, 576(%rdx)
	movq	8(%r12), %rdx
	movq	192(%rdx), %rcx
	testb	$8, 96(%rcx)
	jne	.L964
	movl	(%rdx), %edx
	leaq	-80(%rbp), %r14
	cmpl	$65536, %edx
	je	.L964
	cmpl	$771, %edx
	jle	.L964
	cmpq	$1, %rax
	jbe	.L965
	movzwl	(%rsi), %edx
	subq	$2, %rax
	rolw	$8, %dx
	movzwl	%dx, %edx
	cmpq	%rdx, %rax
	je	.L1033
.L965:
	movl	$2669, %r9d
	jmp	.L1028
	.p2align 4,,10
	.p2align 3
.L978:
	movl	$0, -100(%rbp)
	movq	$0, -112(%rbp)
	movq	$0, -120(%rbp)
.L949:
	cmpq	$1, %r10
	jbe	.L950
	movzwl	(%r9), %eax
	leaq	-2(%r10), %r13
	addq	$2, %r9
	movq	%r9, (%rbx)
	rolw	$8, %ax
	movq	%r13, 8(%rbx)
	movzwl	%ax, %edi
	cmpl	$771, %r8d
	jle	.L972
	cmpl	$65536, %r8d
	je	.L972
	testw	%ax, %ax
	je	.L950
	movzwl	%ax, %eax
	cmpq	%r13, %rax
	ja	.L950
	movq	1296(%r12), %r14
	movq	%rax, %r13
	jmp	.L951
	.p2align 4,,10
	.p2align 3
.L1031:
	movl	(%rsi), %eax
	cmpl	$65536, %eax
	je	.L954
	cmpl	$771, %eax
	jle	.L954
	jmp	.L951
	.p2align 4,,10
	.p2align 3
.L970:
	movl	$3, %eax
	jmp	.L945
.L1033:
	addq	$2, %rsi
	xorl	%r8d, %r8d
	leaq	-88(%rbp), %rcx
	movq	%r12, %rdi
	leaq	(%rsi,%rax), %rdx
	movq	%rsi, -80(%rbp)
	movl	$1, %r9d
	movq	%r14, %rsi
	movq	%rdx, (%rbx)
	movl	$8192, %edx
	movq	$0, 8(%rbx)
	movq	%rax, -72(%rbp)
	call	tls_collect_extensions@PLT
	testl	%eax, %eax
	je	.L947
	movq	-88(%rbp), %rdx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$1, %r9d
	movl	$8192, %esi
	movq	%r12, %rdi
	call	tls_parse_all_extensions@PLT
	testl	%eax, %eax
	je	.L947
	.p2align 4,,10
	.p2align 3
.L964:
	call	EVP_sha256@PLT
	xorl	%r9d, %r9d
	movq	%r14, %rcx
	movq	%r13, %rsi
	movq	1296(%r12), %rdx
	movq	%rax, %r8
	movq	552(%rdx), %rdi
	leaq	344(%rdx), %r11
	movq	%r11, %rdx
	call	EVP_Digest@PLT
	testl	%eax, %eax
	je	.L1034
	movq	1296(%r12), %rax
	movl	-80(%rbp), %ecx
	movl	$0, 432(%rax)
	movq	%rcx, 336(%rax)
	movq	8(%r12), %rax
	movq	192(%rax), %rdx
	testb	$8, 96(%rdx)
	jne	.L970
	movl	(%rax), %eax
	cmpl	$771, %eax
	jle	.L970
	cmpl	$65536, %eax
	je	.L970
	movq	%r12, %rdi
	call	ssl_handshake_md@PLT
	movq	%rax, %rdi
	movq	%rax, %r14
	call	EVP_MD_size@PLT
	testl	%eax, %eax
	js	.L1035
	movslq	%eax, %rbx
	movq	1296(%r12), %rax
	pushq	$1
	movq	%r14, %rsi
	pushq	%rbx
	movq	-120(%rbp), %r9
	leaq	508(%r12), %rdx
	movq	%r12, %rdi
	addq	$80, %rax
	movl	$10, %r8d
	leaq	nonce_label.25203(%rip), %rcx
	pushq	%rax
	pushq	-112(%rbp)
	call	tls13_hkdf_expand@PLT
	addq	$32, %rsp
	testl	%eax, %eax
	je	.L947
	movq	-88(%rbp), %rdi
	movq	1296(%r12), %rax
	movl	$2739, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rbx, 8(%rax)
	call	CRYPTO_free@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	call	ssl_update_cache@PLT
	movl	$1, %eax
	jmp	.L945
	.p2align 4,,10
	.p2align 3
.L1032:
	movl	$2650, %r9d
.L1029:
	leaq	.LC0(%rip), %r8
	movl	$65, %ecx
	movl	$366, %edx
	movq	%r12, %rdi
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L947
.L1034:
	movl	$2704, %r9d
	leaq	.LC0(%rip), %r8
	movl	$6, %ecx
	movq	%r12, %rdi
	movl	$366, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L947
.L1035:
	movl	$2720, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movq	%r12, %rdi
	movl	$366, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L947
.L1030:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1049:
	.size	tls_process_new_session_ticket, .-tls_process_new_session_ticket
	.p2align 4
	.globl	tls_process_cert_status_body
	.type	tls_process_cert_status_body, @function
tls_process_cert_status_body:
.LFB1050:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	8(%rsi), %rax
	testq	%rax, %rax
	je	.L1039
	movq	(%rsi), %rdx
	movq	%rsi, %rbx
	leaq	1(%rdx), %rcx
	movzbl	(%rdx), %esi
	movq	%rcx, (%rbx)
	leaq	-1(%rax), %rcx
	movq	%rcx, 8(%rbx)
	cmpb	$1, %sil
	je	.L1047
.L1039:
	movl	$2761, %r9d
	leaq	.LC0(%rip), %r8
	movl	$329, %ecx
.L1045:
	movl	$495, %edx
	movl	$50, %esi
	movq	%r12, %rdi
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
.L1036:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1047:
	.cfi_restore_state
	cmpq	$2, %rcx
	jbe	.L1041
	movzbl	1(%rdx), %r13d
	movzbl	2(%rdx), %ecx
	subq	$4, %rax
	addq	$4, %rdx
	salq	$8, %rcx
	salq	$16, %r13
	orq	%rcx, %r13
	movzbl	-1(%rdx), %ecx
	movq	%rdx, (%rbx)
	movq	%rax, 8(%rbx)
	orq	%rcx, %r13
	cmpq	%rax, %r13
	je	.L1048
.L1041:
	movl	$2767, %r9d
.L1046:
	leaq	.LC0(%rip), %r8
	movl	$159, %ecx
	jmp	.L1045
	.p2align 4,,10
	.p2align 3
.L1048:
	movq	%r13, %rdi
	movl	$2771, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_malloc@PLT
	movq	%rax, 1648(%r12)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1049
	movl	$2778, %r9d
	cmpq	%r13, 8(%rbx)
	jb	.L1046
	movq	(%rbx), %rsi
	movq	%r13, %rdx
	call	memcpy@PLT
	addq	%r13, (%rbx)
	movl	$1, %eax
	subq	%r13, 8(%rbx)
	movq	%r13, 1656(%r12)
	jmp	.L1036
.L1049:
	movl	$2773, %r9d
	leaq	.LC0(%rip), %r8
	movl	$65, %ecx
	movq	%r12, %rdi
	movl	$495, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	jmp	.L1036
	.cfi_endproc
.LFE1050:
	.size	tls_process_cert_status_body, .-tls_process_cert_status_body
	.p2align 4
	.globl	tls_process_cert_status
	.type	tls_process_cert_status, @function
tls_process_cert_status:
.LFB1051:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	8(%rsi), %rax
	testq	%rax, %rax
	je	.L1053
	movq	(%rsi), %rdx
	movq	%rsi, %rbx
	leaq	1(%rdx), %rcx
	movzbl	(%rdx), %esi
	movq	%rcx, (%rbx)
	leaq	-1(%rax), %rcx
	movq	%rcx, 8(%rbx)
	cmpb	$1, %sil
	je	.L1061
.L1053:
	movl	$2761, %r9d
	leaq	.LC0(%rip), %r8
	movl	$329, %ecx
.L1059:
	movl	$495, %edx
	movl	$50, %esi
	movq	%r12, %rdi
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
.L1050:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1061:
	.cfi_restore_state
	cmpq	$2, %rcx
	jbe	.L1055
	movzbl	1(%rdx), %r13d
	movzbl	2(%rdx), %ecx
	subq	$4, %rax
	addq	$4, %rdx
	salq	$8, %rcx
	salq	$16, %r13
	orq	%rcx, %r13
	movzbl	-1(%rdx), %ecx
	movq	%rdx, (%rbx)
	movq	%rax, 8(%rbx)
	orq	%rcx, %r13
	cmpq	%rax, %r13
	je	.L1062
.L1055:
	movl	$2767, %r9d
.L1060:
	leaq	.LC0(%rip), %r8
	movl	$159, %ecx
	jmp	.L1059
	.p2align 4,,10
	.p2align 3
.L1062:
	movq	%r13, %rdi
	movl	$2771, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_malloc@PLT
	movq	%rax, 1648(%r12)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1063
	movl	$2778, %r9d
	cmpq	%r13, 8(%rbx)
	jb	.L1060
	movq	(%rbx), %rsi
	movq	%r13, %rdx
	call	memcpy@PLT
	addq	%r13, (%rbx)
	movl	$3, %eax
	subq	%r13, 8(%rbx)
	movq	%r13, 1656(%r12)
	jmp	.L1050
.L1063:
	movl	$2773, %r9d
	leaq	.LC0(%rip), %r8
	movl	$65, %ecx
	movq	%r12, %rdi
	movl	$495, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	jmp	.L1050
	.cfi_endproc
.LFE1051:
	.size	tls_process_cert_status, .-tls_process_cert_status
	.p2align 4
	.globl	tls_process_initial_server_flight
	.type	tls_process_initial_server_flight, @function
tls_process_initial_server_flight:
.LFB1052:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	168(%rdi), %rax
	movq	568(%rax), %rax
	movl	32(%rax), %ebx
	testb	$-85, %bl
	jne	.L1065
.L1072:
	cmpl	$-1, 1608(%r12)
	je	.L1067
	movq	1440(%r12), %rdx
	movq	560(%rdx), %rax
	testq	%rax, %rax
	je	.L1067
	movq	568(%rdx), %rsi
	movq	%r12, %rdi
	call	*%rax
	testl	%eax, %eax
	je	.L1092
	js	.L1093
.L1067:
	cmpq	$0, 1872(%r12)
	je	.L1076
	movq	%r12, %rdi
	call	ssl_validate_ct@PLT
	testl	%eax, %eax
	je	.L1094
.L1076:
	movl	$1, %eax
.L1064:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1095
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1065:
	.cfi_restore_state
	movl	28(%rax), %r13d
	movq	1296(%rdi), %rax
	movq	440(%rax), %rdi
	call	X509_get0_pubkey@PLT
	leaq	-48(%rbp), %rsi
	movq	%rax, %rdi
	call	ssl_cert_lookup_by_pkey@PLT
	testq	%rax, %rax
	je	.L1068
	movl	4(%rax), %eax
	testl	%eax, %ebx
	je	.L1068
	testb	$8, %al
	jne	.L1096
	testb	$65, %r13b
	je	.L1073
	cmpq	$0, -48(%rbp)
	jne	.L1097
.L1073:
	andl	$2, %r13d
	je	.L1072
	movq	168(%r12), %rax
	cmpq	$0, 1032(%rax)
	jne	.L1072
	movl	$3617, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movq	%r12, %rdi
	movl	$130, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	jmp	.L1064
	.p2align 4,,10
	.p2align 3
.L1094:
	movl	1376(%r12), %eax
	notl	%eax
	andl	$1, %eax
	jmp	.L1064
	.p2align 4,,10
	.p2align 3
.L1096:
	movq	1296(%r12), %rax
	movq	%r12, %rsi
	movq	440(%rax), %rdi
	call	ssl_check_srvr_ecc_cert_and_alg@PLT
	testl	%eax, %eax
	jne	.L1072
	movl	$304, %ecx
	movl	$130, %edx
	movq	%r12, %rdi
	movl	%eax, -52(%rbp)
	movl	$3602, %r9d
	leaq	.LC0(%rip), %r8
	movl	$40, %esi
	call	ossl_statem_fatal@PLT
	movl	-52(%rbp), %eax
	jmp	.L1064
	.p2align 4,,10
	.p2align 3
.L1068:
	movl	$3592, %r9d
	leaq	.LC0(%rip), %r8
	movl	$221, %ecx
.L1091:
	movl	$130, %edx
	movl	$40, %esi
	movq	%r12, %rdi
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	jmp	.L1064
	.p2align 4,,10
	.p2align 3
.L1092:
	movl	$328, %ecx
	movl	$442, %edx
	movq	%r12, %rdi
	movl	%eax, -52(%rbp)
	movl	$2825, %r9d
	leaq	.LC0(%rip), %r8
	movl	$113, %esi
	call	ossl_statem_fatal@PLT
	movl	-52(%rbp), %eax
	jmp	.L1064
	.p2align 4,,10
	.p2align 3
.L1093:
	movl	$2831, %r9d
	leaq	.LC0(%rip), %r8
	movl	$65, %ecx
	movq	%r12, %rdi
	movl	$442, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	jmp	.L1064
	.p2align 4,,10
	.p2align 3
.L1097:
	movl	$3609, %r9d
	leaq	.LC0(%rip), %r8
	movl	$169, %ecx
	jmp	.L1091
.L1095:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1052:
	.size	tls_process_initial_server_flight, .-tls_process_initial_server_flight
	.p2align 4
	.globl	tls_process_server_done
	.type	tls_process_server_done, @function
tls_process_server_done:
.LFB1053:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 8(%rsi)
	jne	.L1132
	movq	168(%rdi), %rax
	movq	568(%rax), %rax
	movl	28(%rax), %ebx
	testb	$32, %bl
	je	.L1101
	call	SRP_Calc_A_param@PLT
	testl	%eax, %eax
	jle	.L1102
	movq	168(%r12), %rax
	movq	568(%rax), %rax
	movl	28(%rax), %ebx
.L1101:
	movl	32(%rax), %r13d
	testb	$-85, %r13b
	jne	.L1103
.L1109:
	cmpl	$-1, 1608(%r12)
	je	.L1105
	movq	1440(%r12), %rdx
	movq	560(%rdx), %rax
	testq	%rax, %rax
	je	.L1105
	movq	568(%rdx), %rsi
	movq	%r12, %rdi
	call	*%rax
	testl	%eax, %eax
	je	.L1133
	js	.L1134
.L1105:
	cmpq	$0, 1872(%r12)
	je	.L1113
	movq	%r12, %rdi
	call	ssl_validate_ct@PLT
	testl	%eax, %eax
	je	.L1135
.L1113:
	movl	$1, %eax
.L1098:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1136
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1103:
	.cfi_restore_state
	movq	1296(%r12), %rax
	movq	440(%rax), %rdi
	call	X509_get0_pubkey@PLT
	leaq	-48(%rbp), %rsi
	movq	%rax, %rdi
	call	ssl_cert_lookup_by_pkey@PLT
	testq	%rax, %rax
	je	.L1106
	movl	4(%rax), %eax
	testl	%eax, %r13d
	je	.L1106
	andl	$8, %eax
	jne	.L1137
	testb	$65, %bl
	je	.L1110
	cmpq	$0, -48(%rbp)
	jne	.L1138
.L1110:
	andl	$2, %ebx
	je	.L1109
	movq	168(%r12), %rdx
	cmpq	$0, 1032(%rdx)
	jne	.L1109
	movl	$68, %ecx
	movl	$130, %edx
	movq	%r12, %rdi
	movl	%eax, -52(%rbp)
	movl	$3617, %r9d
	leaq	.LC0(%rip), %r8
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	movl	-52(%rbp), %eax
	jmp	.L1098
	.p2align 4,,10
	.p2align 3
.L1132:
	movl	$2854, %r9d
	movl	$159, %ecx
	movl	$368, %edx
	movl	$50, %esi
	leaq	.LC0(%rip), %r8
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	jmp	.L1098
	.p2align 4,,10
	.p2align 3
.L1135:
	movl	1376(%r12), %eax
	notl	%eax
	andl	$1, %eax
	jmp	.L1098
	.p2align 4,,10
	.p2align 3
.L1137:
	movq	1296(%r12), %rax
	movq	%r12, %rsi
	movq	440(%rax), %rdi
	call	ssl_check_srvr_ecc_cert_and_alg@PLT
	testl	%eax, %eax
	jne	.L1109
	movl	$3602, %r9d
	leaq	.LC0(%rip), %r8
	movl	$304, %ecx
	jmp	.L1131
	.p2align 4,,10
	.p2align 3
.L1106:
	movl	$3592, %r9d
	leaq	.LC0(%rip), %r8
	movl	$221, %ecx
.L1131:
	movl	$130, %edx
	movl	$40, %esi
	movq	%r12, %rdi
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	jmp	.L1098
	.p2align 4,,10
	.p2align 3
.L1102:
	movl	$2861, %r9d
	leaq	.LC0(%rip), %r8
	movl	$361, %ecx
	movq	%r12, %rdi
	movl	$368, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	jmp	.L1098
	.p2align 4,,10
	.p2align 3
.L1133:
	movl	$2825, %r9d
	leaq	.LC0(%rip), %r8
	movl	$328, %ecx
	movq	%r12, %rdi
	movl	$442, %edx
	movl	$113, %esi
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	jmp	.L1098
	.p2align 4,,10
	.p2align 3
.L1134:
	movl	$2831, %r9d
	leaq	.LC0(%rip), %r8
	movl	$65, %ecx
	movq	%r12, %rdi
	movl	$442, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	jmp	.L1098
	.p2align 4,,10
	.p2align 3
.L1138:
	movl	$169, %ecx
	movl	$130, %edx
	movq	%r12, %rdi
	movl	%eax, -52(%rbp)
	movl	$3609, %r9d
	leaq	.LC0(%rip), %r8
	movl	$40, %esi
	call	ossl_statem_fatal@PLT
	movl	-52(%rbp), %eax
	jmp	.L1098
.L1136:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1053:
	.size	tls_process_server_done, .-tls_process_server_done
	.p2align 4
	.globl	tls_client_key_exchange_post_work
	.type	tls_client_key_exchange_post_work, @function
tls_client_key_exchange_post_work:
.LFB1061:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	168(%rdi), %rax
	movq	696(%rax), %r12
	movq	704(%rax), %r13
	movq	568(%rax), %rax
	movl	28(%rax), %eax
	testb	$32, %al
	jne	.L1149
	testq	%r12, %r12
	je	.L1150
.L1143:
	movl	$1, %ecx
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	ssl_generate_master_secret@PLT
	testl	%eax, %eax
	je	.L1151
.L1144:
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1150:
	.cfi_restore_state
	testb	$8, %al
	jne	.L1143
	movl	$3379, %r9d
	movl	$65, %ecx
	movl	$354, %edx
	movl	$80, %esi
	leaq	.LC0(%rip), %r8
	call	ossl_statem_fatal@PLT
	jmp	.L1141
	.p2align 4,,10
	.p2align 3
.L1151:
	xorl	%r13d, %r13d
	xorl	%r12d, %r12d
.L1141:
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$3427, %ecx
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_clear_free@PLT
	movq	168(%rbx), %rax
	movq	$0, 696(%rax)
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1149:
	.cfi_restore_state
	call	srp_generate_client_master_secret@PLT
	testl	%eax, %eax
	je	.L1141
	jmp	.L1144
	.cfi_endproc
.LFE1061:
	.size	tls_client_key_exchange_post_work, .-tls_client_key_exchange_post_work
	.p2align 4
	.globl	ossl_statem_client_post_work
	.type	ossl_statem_client_post_work, @function
ossl_statem_client_post_work:
.LFB1032:
	.cfi_startproc
	endbr64
	movq	$0, 152(%rdi)
	movl	92(%rdi), %eax
	subl	$12, %eax
	cmpl	$36, %eax
	ja	.L1209
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.L1155(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L1155:
	.long	.L1160-.L1155
	.long	.L1170-.L1155
	.long	.L1159-.L1155
	.long	.L1170-.L1155
	.long	.L1158-.L1155
	.long	.L1170-.L1155
	.long	.L1157-.L1155
	.long	.L1170-.L1155
	.long	.L1170-.L1155
	.long	.L1170-.L1155
	.long	.L1170-.L1155
	.long	.L1170-.L1155
	.long	.L1170-.L1155
	.long	.L1170-.L1155
	.long	.L1170-.L1155
	.long	.L1170-.L1155
	.long	.L1170-.L1155
	.long	.L1170-.L1155
	.long	.L1170-.L1155
	.long	.L1170-.L1155
	.long	.L1170-.L1155
	.long	.L1170-.L1155
	.long	.L1170-.L1155
	.long	.L1170-.L1155
	.long	.L1170-.L1155
	.long	.L1170-.L1155
	.long	.L1170-.L1155
	.long	.L1170-.L1155
	.long	.L1170-.L1155
	.long	.L1170-.L1155
	.long	.L1170-.L1155
	.long	.L1156-.L1155
	.long	.L1170-.L1155
	.long	.L1170-.L1155
	.long	.L1170-.L1155
	.long	.L1170-.L1155
	.long	.L1154-.L1155
	.text
	.p2align 4,,10
	.p2align 3
.L1157:
	call	statem_flush@PLT
	movl	%eax, %r8d
	movl	$4, %eax
	cmpl	$1, %r8d
	jne	.L1152
	movq	8(%r12), %rax
	movq	192(%rax), %rdx
	testb	$8, 96(%rdx)
	jne	.L1170
	movl	(%rax), %eax
	cmpl	$65536, %eax
	je	.L1170
	cmpl	$771, %eax
	jle	.L1170
	movq	%r12, %rdi
	call	tls13_save_handshake_digest_for_pha@PLT
	testl	%eax, %eax
	je	.L1165
	cmpl	$4, 1936(%r12)
	je	.L1170
	movq	8(%r12), %rax
	movl	$274, %esi
	movq	%r12, %rdi
	movq	192(%rax), %rax
	call	*32(%rax)
	testl	%eax, %eax
	je	.L1165
	.p2align 4,,10
	.p2align 3
.L1170:
	movl	$2, %eax
.L1152:
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1209:
	.cfi_restore 6
	.cfi_restore 12
	movl	$2, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1154:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	movq	1136(%rdi), %rdi
	call	EVP_CIPHER_CTX_free@PLT
	movl	$2, %eax
	movq	$0, 1136(%r12)
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1160:
	.cfi_restore_state
	cmpl	$2, 132(%rdi)
	jne	.L1161
	movl	6176(%rdi), %ecx
	testl	%ecx, %ecx
	jne	.L1213
.L1161:
	movq	%r12, %rdi
	call	statem_flush@PLT
	movl	%eax, %r8d
	movl	$3, %eax
	testl	%r8d, %r8d
	je	.L1152
.L1166:
	movq	8(%r12), %rax
	movq	192(%rax), %rax
	testb	$8, 96(%rax)
	je	.L1170
	movl	$1, 1520(%r12)
	movl	$2, %eax
	jmp	.L1152
	.p2align 4,,10
	.p2align 3
.L1159:
	call	tls_client_key_exchange_post_work
	testl	%eax, %eax
	jne	.L1170
.L1165:
	xorl	%eax, %eax
.L1215:
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1158:
	.cfi_restore_state
	movq	8(%rdi), %rdx
	movq	192(%rdx), %rax
	testb	$8, 96(%rax)
	jne	.L1167
	movl	(%rdx), %edx
	cmpl	$771, %edx
	jle	.L1167
	cmpl	$65536, %edx
	jne	.L1170
.L1167:
	cmpl	$1, 1248(%r12)
	je	.L1170
	cmpl	$2, 132(%r12)
	je	.L1214
.L1169:
	movq	168(%r12), %rcx
	movq	1296(%r12), %rdx
	movq	%r12, %rdi
	movq	568(%rcx), %rcx
	movl	$0, 496(%rdx)
	movq	%rcx, 504(%rdx)
	call	*16(%rax)
	testl	%eax, %eax
	je	.L1165
	movq	8(%r12), %rax
	movl	$18, %esi
	movq	%r12, %rdi
	movq	192(%rax), %rax
	call	*32(%rax)
	testl	%eax, %eax
	je	.L1165
	movq	8(%r12), %rax
	movq	192(%rax), %rax
	testb	$8, 96(%rax)
	je	.L1170
	movl	$2, %esi
	movq	%r12, %rdi
	call	dtls1_reset_seq_numbers@PLT
	movl	$2, %eax
	jmp	.L1152
	.p2align 4,,10
	.p2align 3
.L1156:
	call	statem_flush@PLT
	movl	%eax, %r8d
	movl	$3, %eax
	cmpl	$1, %r8d
	jne	.L1152
	movl	$1, %esi
	movq	%r12, %rdi
	call	tls13_update_key@PLT
	testl	%eax, %eax
	jne	.L1170
	xorl	%eax, %eax
	jmp	.L1215
	.p2align 4,,10
	.p2align 3
.L1213:
	testb	$16, 1494(%rdi)
	jne	.L1166
	movl	$82, %esi
	call	tls13_change_cipher_state@PLT
	testl	%eax, %eax
	jne	.L1166
	xorl	%eax, %eax
	jmp	.L1215
	.p2align 4,,10
	.p2align 3
.L1214:
	movl	6176(%r12), %edx
	testl	%edx, %edx
	je	.L1169
	movl	$82, %esi
	movq	%r12, %rdi
	call	tls13_change_cipher_state@PLT
	testl	%eax, %eax
	jne	.L1170
	xorl	%eax, %eax
	jmp	.L1215
	.cfi_endproc
.LFE1032:
	.size	ossl_statem_client_post_work, .-ossl_statem_client_post_work
	.p2align 4
	.globl	tls_prepare_client_certificate
	.type	tls_prepare_client_certificate, @function
tls_prepare_client_certificate:
.LFB1063:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -56(%rbp)
	movq	$0, -48(%rbp)
	cmpl	$3, %esi
	je	.L1272
	cmpl	$4, %esi
	jne	.L1273
.L1225:
	movq	1440(%r12), %rax
	cmpq	$0, 488(%rax)
	je	.L1274
	movq	%r12, %rdi
	leaq	-48(%rbp), %r14
	leaq	-56(%rbp), %r13
	call	SSL_get_client_CA_list@PLT
	xorl	%r9d, %r9d
	movq	%r14, %r8
	movq	%r13, %rcx
	movq	%rax, %rdx
	movq	1440(%r12), %rax
	movq	%r12, %rsi
	movq	488(%rax), %rdi
	pushq	$0
	pushq	$0
	call	ENGINE_load_ssl_client_cert@PLT
	movl	%eax, %ebx
	popq	%rax
	popq	%rdx
	testl	%ebx, %ebx
	jne	.L1233
	movq	1440(%r12), %rax
	movq	192(%rax), %rax
	testq	%rax, %rax
	je	.L1234
.L1231:
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	*%rax
	movl	%eax, %ebx
.L1233:
	testl	%ebx, %ebx
	js	.L1275
	movl	$1, 40(%r12)
	cmpl	$1, %ebx
	je	.L1276
	movq	-56(%rbp), %rdi
	call	X509_free@PLT
	movq	-48(%rbp), %rdi
	call	EVP_PKEY_free@PLT
	testl	%ebx, %ebx
	je	.L1268
.L1240:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	tls_choose_sigalg@PLT
	testl	%eax, %eax
	movq	168(%r12), %rax
	je	.L1232
	cmpq	$0, 728(%rax)
	je	.L1232
	movq	1168(%r12), %rax
	testl	$196609, 28(%rax)
	je	.L1243
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movl	$-2, %r8d
	movq	%r12, %rdi
	call	tls1_check_chain@PLT
	testl	%eax, %eax
	jne	.L1243
.L1268:
	movq	168(%r12), %rax
	jmp	.L1232
	.p2align 4,,10
	.p2align 3
.L1273:
	movl	$3530, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
.L1269:
	movl	$360, %edx
	movl	$80, %esi
	movq	%r12, %rdi
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
.L1216:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1277
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1272:
	.cfi_restore_state
	movq	1168(%rdi), %rdx
	movq	440(%rdx), %rax
	testq	%rax, %rax
	je	.L1218
	movq	448(%rdx), %rsi
	call	*%rax
	testl	%eax, %eax
	js	.L1278
	movl	$3467, %r9d
	leaq	.LC0(%rip), %r8
	movl	$234, %ecx
	je	.L1269
	movl	$1, 40(%r12)
.L1218:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	tls_choose_sigalg@PLT
	testl	%eax, %eax
	je	.L1225
	movq	168(%r12), %rax
	cmpq	$0, 728(%rax)
	je	.L1225
	movq	1168(%r12), %rax
	testl	$196609, 28(%rax)
	jne	.L1226
.L1243:
	cmpl	$4, 1936(%r12)
	jne	.L1270
.L1280:
	movl	$1, %eax
	jmp	.L1216
	.p2align 4,,10
	.p2align 3
.L1274:
	movq	192(%rax), %rax
	leaq	-48(%rbp), %r14
	leaq	-56(%rbp), %r13
	testq	%rax, %rax
	jne	.L1231
.L1234:
	movl	$1, 40(%r12)
.L1266:
	movq	-56(%rbp), %rdi
.L1267:
	call	X509_free@PLT
	movq	-48(%rbp), %rdi
	call	EVP_PKEY_free@PLT
	movq	168(%r12), %rax
.L1232:
	cmpl	$768, (%r12)
	je	.L1279
	movl	$2, 584(%rax)
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	ssl3_digest_cached_records@PLT
	movl	%eax, %r8d
	xorl	%eax, %eax
	testl	%r8d, %r8d
	je	.L1216
	cmpl	$4, 1936(%r12)
	je	.L1280
.L1270:
	movl	$2, %eax
	jmp	.L1216
	.p2align 4,,10
	.p2align 3
.L1276:
	cmpq	$0, -48(%rbp)
	je	.L1237
	movq	-56(%rbp), %rsi
	testq	%rsi, %rsi
	je	.L1237
	movq	%r12, %rdi
	call	SSL_use_certificate@PLT
	testl	%eax, %eax
	je	.L1266
	movq	-48(%rbp), %rsi
	movq	%r12, %rdi
	call	SSL_use_PrivateKey@PLT
	movq	-56(%rbp), %rdi
	testl	%eax, %eax
	je	.L1267
	call	X509_free@PLT
	movq	-48(%rbp), %rdi
	call	EVP_PKEY_free@PLT
	jmp	.L1240
	.p2align 4,,10
	.p2align 3
.L1237:
	movl	$3502, %r8d
	movl	$106, %edx
	movl	$360, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L1266
	.p2align 4,,10
	.p2align 3
.L1275:
	movl	$4, 40(%r12)
	movl	$4, %eax
	jmp	.L1216
	.p2align 4,,10
	.p2align 3
.L1278:
	movl	$4, 40(%r12)
	movl	$3, %eax
	jmp	.L1216
	.p2align 4,,10
	.p2align 3
.L1226:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movl	$-2, %r8d
	movq	%r12, %rdi
	call	tls1_check_chain@PLT
	testl	%eax, %eax
	jne	.L1243
	jmp	.L1225
	.p2align 4,,10
	.p2align 3
.L1279:
	movl	$41, %edx
	movl	$1, %esi
	movq	%r12, %rdi
	movl	$0, 584(%rax)
	call	ssl3_send_alert@PLT
	jmp	.L1270
.L1277:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1063:
	.size	tls_prepare_client_certificate, .-tls_prepare_client_certificate
	.p2align 4
	.globl	ossl_statem_client_post_process_message
	.type	ossl_statem_client_post_process_message, @function
ossl_statem_client_post_process_message:
.LFB1036:
	.cfi_startproc
	endbr64
	movl	92(%rdi), %eax
	andl	$-33, %eax
	cmpl	$7, %eax
	je	.L1288
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$68, %ecx
	movl	$593, %edx
	movl	$80, %esi
	movl	$1087, %r9d
	leaq	.LC0(%rip), %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1288:
	.cfi_restore 6
	jmp	tls_prepare_client_certificate
	.cfi_endproc
.LFE1036:
	.size	ossl_statem_client_post_process_message, .-ossl_statem_client_post_process_message
	.p2align 4
	.globl	ssl3_check_cert_and_algorithm
	.type	ssl3_check_cert_and_algorithm, @function
ssl3_check_cert_and_algorithm:
.LFB1065:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	168(%rdi), %rax
	movq	568(%rax), %rax
	movl	32(%rax), %ebx
	testb	$-85, %bl
	jne	.L1290
.L1295:
	movl	$1, %eax
.L1289:
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L1309
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1290:
	.cfi_restore_state
	movl	28(%rax), %r13d
	movq	1296(%rdi), %rax
	movq	%rdi, %r12
	movq	440(%rax), %rdi
	call	X509_get0_pubkey@PLT
	leaq	-48(%rbp), %rsi
	movq	%rax, %rdi
	call	ssl_cert_lookup_by_pkey@PLT
	testq	%rax, %rax
	je	.L1292
	movl	4(%rax), %eax
	testl	%eax, %ebx
	je	.L1292
	testb	$8, %al
	jne	.L1310
	testb	$65, %r13b
	je	.L1296
	cmpq	$0, -48(%rbp)
	jne	.L1311
.L1296:
	andl	$2, %r13d
	je	.L1295
	movq	168(%r12), %rax
	cmpq	$0, 1032(%rax)
	jne	.L1295
	movl	$3617, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movq	%r12, %rdi
	movl	$130, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	jmp	.L1289
	.p2align 4,,10
	.p2align 3
.L1310:
	movq	1296(%r12), %rax
	movq	%r12, %rsi
	movq	440(%rax), %rdi
	call	ssl_check_srvr_ecc_cert_and_alg@PLT
	testl	%eax, %eax
	jne	.L1295
	movl	$304, %ecx
	movl	$130, %edx
	movq	%r12, %rdi
	movl	%eax, -52(%rbp)
	movl	$3602, %r9d
	leaq	.LC0(%rip), %r8
	movl	$40, %esi
	call	ossl_statem_fatal@PLT
	movl	-52(%rbp), %eax
	jmp	.L1289
	.p2align 4,,10
	.p2align 3
.L1292:
	movl	$3592, %r9d
	leaq	.LC0(%rip), %r8
	movl	$221, %ecx
.L1308:
	movl	$130, %edx
	movl	$40, %esi
	movq	%r12, %rdi
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	jmp	.L1289
	.p2align 4,,10
	.p2align 3
.L1311:
	movl	$3609, %r9d
	leaq	.LC0(%rip), %r8
	movl	$169, %ecx
	jmp	.L1308
.L1309:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1065:
	.size	ssl3_check_cert_and_algorithm, .-ssl3_check_cert_and_algorithm
	.p2align 4
	.globl	tls_process_key_exchange
	.type	tls_process_key_exchange, @function
tls_process_key_exchange:
.LFB1047:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$104, %rsp
	movq	(%rsi), %r14
	movq	8(%rsi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	168(%rdi), %rax
	movq	$0, -80(%rbp)
	movq	568(%rax), %rdx
	movq	1032(%rax), %rdi
	movl	28(%rdx), %r15d
	call	EVP_PKEY_free@PLT
	movq	168(%r12), %rax
	movl	%r15d, %r9d
	movq	$0, 1032(%rax)
	andl	$456, %r9d
	jne	.L1446
.L1313:
	testb	$72, %r15b
	je	.L1447
.L1320:
	movq	168(%r12), %rax
	movq	568(%rax), %rax
	movl	32(%rax), %eax
.L1326:
	andl	$68, %eax
	orl	%r9d, %eax
	jne	.L1374
.L1376:
	movq	%r12, %rdi
	call	ssl3_check_cert_and_algorithm
	movl	$2417, %r9d
	movl	$390, %ecx
	leaq	.LC0(%rip), %r8
	testl	%eax, %eax
	jne	.L1440
	.p2align 4,,10
	.p2align 3
.L1439:
	xorl	%r10d, %r10d
.L1317:
	movq	%r10, %rdi
	call	EVP_MD_CTX_free@PLT
	xorl	%eax, %eax
.L1312:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1448
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1447:
	.cfi_restore_state
	testb	$32, %r15b
	jne	.L1449
	testl	$258, %r15d
	jne	.L1450
	testb	$-124, %r15b
	jne	.L1451
	testl	%r15d, %r15d
	jne	.L1452
	movq	168(%r12), %rax
	movq	568(%rax), %rax
	testb	$68, 32(%rax)
	je	.L1376
.L1374:
	cmpq	$0, 8(%rbx)
	movl	$3, %eax
	je	.L1312
	movl	$2425, %r9d
	leaq	.LC0(%rip), %r8
	movl	$153, %ecx
	.p2align 4,,10
	.p2align 3
.L1440:
	movl	$365, %edx
	movl	$50, %esi
	movq	%r12, %rdi
	call	ossl_statem_fatal@PLT
	jmp	.L1439
	.p2align 4,,10
	.p2align 3
.L1446:
	movq	8(%rbx), %rax
	cmpq	$1, %rax
	jbe	.L1314
	movq	(%rbx), %r10
	subq	$2, %rax
	movzwl	(%r10), %r8d
	rolw	$8, %r8w
	movzwl	%r8w, %r8d
	cmpq	%r8, %rax
	jb	.L1314
	addq	$2, %r10
	subq	%r8, %rax
	leaq	(%r10,%r8), %rdx
	movq	%rax, 8(%rbx)
	movq	%rdx, (%rbx)
	cmpq	$128, %r8
	ja	.L1453
	movq	1296(%r12), %r11
	movq	416(%r11), %rdi
	testq	%r8, %r8
	jne	.L1318
	movl	$2015, %edx
	leaq	.LC0(%rip), %rsi
	movl	%r9d, -88(%rbp)
	call	CRYPTO_free@PLT
	movq	1296(%r12), %rax
	movl	-88(%rbp), %r9d
	movq	$0, 416(%rax)
	jmp	.L1313
	.p2align 4,,10
	.p2align 3
.L1450:
	movq	8(%rbx), %rax
	movl	%r9d, -88(%rbp)
	movl	$0, -64(%rbp)
	cmpq	$1, %rax
	jbe	.L1329
	movq	(%rbx), %rdi
	subq	$2, %rax
	movzwl	(%rdi), %edx
	rolw	$8, %dx
	movzwl	%dx, %ecx
	movl	%edx, -96(%rbp)
	cmpq	%rcx, %rax
	jb	.L1329
	addq	$2, %rdi
	subq	%rcx, %rax
	leaq	(%rdi,%rcx), %r8
	movq	%rdi, -104(%rbp)
	movq	%r8, (%rbx)
	movq	%rax, 8(%rbx)
	cmpq	$1, %rax
	jbe	.L1329
	movzwl	(%r8), %ecx
	subq	$2, %rax
	rolw	$8, %cx
	movzwl	%cx, %r10d
	movl	%ecx, -112(%rbp)
	cmpq	%r10, %rax
	jb	.L1329
	addq	$2, %r8
	subq	%r10, %rax
	leaq	(%r8,%r10), %rsi
	movq	%r8, -128(%rbp)
	movq	%rsi, (%rbx)
	movq	%rax, 8(%rbx)
	cmpq	$1, %rax
	jbe	.L1329
	movzwl	(%rsi), %ecx
	subq	$2, %rax
	rolw	$8, %cx
	movzwl	%cx, %r10d
	movw	%cx, -130(%rbp)
	cmpq	%r10, %rax
	jb	.L1329
	leaq	2(%rsi), %rcx
	subq	%r10, %rax
	leaq	(%rcx,%r10), %rsi
	movq	%rax, 8(%rbx)
	movq	%rsi, (%rbx)
	movq	%rcx, -144(%rbp)
	call	EVP_PKEY_new@PLT
	movq	%rax, -120(%rbp)
	call	DH_new@PLT
	cmpq	$0, -120(%rbp)
	movq	%rax, %r15
	je	.L1330
	testq	%rax, %rax
	movq	-128(%rbp), %r8
	movl	-112(%rbp), %ecx
	movq	-104(%rbp), %rdi
	movl	-96(%rbp), %edx
	movl	-88(%rbp), %r9d
	je	.L1330
	movzwl	%dx, %esi
	xorl	%edx, %edx
	movl	%r9d, -112(%rbp)
	movq	%r8, -96(%rbp)
	movl	%ecx, -88(%rbp)
	call	BN_bin2bn@PLT
	movl	-88(%rbp), %ecx
	movq	-96(%rbp), %r8
	xorl	%edx, %edx
	movq	%rax, -104(%rbp)
	movzwl	%cx, %esi
	movq	%r8, %rdi
	call	BN_bin2bn@PLT
	movzwl	-130(%rbp), %esi
	movq	-144(%rbp), %rdi
	xorl	%edx, %edx
	movq	%rax, -88(%rbp)
	call	BN_bin2bn@PLT
	movq	-104(%rbp), %r10
	movq	%rax, -96(%rbp)
	movq	%rax, %rsi
	testq	%r10, %r10
	sete	%dl
	cmpq	$0, -88(%rbp)
	sete	%al
	orb	%al, %dl
	jne	.L1380
	testq	%rsi, %rsi
	movl	-112(%rbp), %r9d
	je	.L1380
	movq	%rsi, %rdi
	movq	%r10, -112(%rbp)
	movl	%r9d, -104(%rbp)
	call	BN_is_zero@PLT
	movl	-104(%rbp), %r9d
	movq	-112(%rbp), %r10
	testl	%eax, %eax
	jne	.L1454
	movq	-88(%rbp), %rcx
	xorl	%edx, %edx
	movq	%r10, %rsi
	movq	%r15, %rdi
	movl	%r9d, -112(%rbp)
	movq	%r10, -104(%rbp)
	call	DH_set0_pqg@PLT
	movl	-112(%rbp), %r9d
	testl	%eax, %eax
	je	.L1455
	leaq	-64(%rbp), %rsi
	movq	%r15, %rdi
	movl	%r9d, -88(%rbp)
	call	DH_check_params@PLT
	testl	%eax, %eax
	je	.L1337
	movl	-64(%rbp), %eax
	movl	-88(%rbp), %r9d
	testl	%eax, %eax
	jne	.L1337
	movq	-96(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r15, %rdi
	movl	%r9d, -88(%rbp)
	call	DH_set0_key@PLT
	movl	-88(%rbp), %r9d
	testl	%eax, %eax
	je	.L1456
	movq	%r15, %rdi
	movl	%r9d, -88(%rbp)
	call	DH_security_bits@PLT
	xorl	%ecx, %ecx
	movq	%r15, %r8
	movl	$262151, %esi
	movl	%eax, %edx
	movq	%r12, %rdi
	call	ssl_security@PLT
	movl	-88(%rbp), %r9d
	testl	%eax, %eax
	je	.L1457
	movq	-120(%rbp), %rdi
	movq	%r15, %rdx
	movl	$28, %esi
	movl	%r9d, -88(%rbp)
	call	EVP_PKEY_assign@PLT
	movl	-88(%rbp), %r9d
	testl	%eax, %eax
	je	.L1458
	movq	168(%r12), %rax
	movq	-120(%rbp), %rcx
	movq	%rcx, 1032(%rax)
	movq	568(%rax), %rax
	movl	32(%rax), %eax
	testb	$3, %al
	je	.L1326
.L1438:
	movq	1296(%r12), %rax
	movl	%r9d, -88(%rbp)
	movq	440(%rax), %rdi
	call	X509_get0_pubkey@PLT
	movl	-88(%rbp), %r9d
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L1320
	movq	8(%rbx), %rax
	movq	$0, -72(%rbp)
	subq	%rax, %r13
	jb	.L1356
	testq	%r13, %r13
	js	.L1356
	movq	8(%r12), %rdx
	movq	192(%rdx), %rdx
	testb	$2, 96(%rdx)
	je	.L1459
	cmpq	$1, %rax
	jbe	.L1360
	movq	(%rbx), %rdx
	subq	$2, %rax
	movq	%r12, %rdi
	movzwl	(%rdx), %esi
	addq	$2, %rdx
	movq	%rax, 8(%rbx)
	movq	%rdx, (%rbx)
	movq	%r15, %rdx
	rolw	$8, %si
	movzwl	%si, %esi
	call	tls12_check_peer_sigalg@PLT
	testl	%eax, %eax
	jle	.L1439
.L1361:
	movq	168(%r12), %rax
	leaq	-72(%rbp), %rsi
	movq	776(%rax), %rdi
	call	tls1_lookup_md@PLT
	movl	$2340, %r9d
	testl	%eax, %eax
	je	.L1444
	movq	8(%rbx), %rax
	cmpq	$1, %rax
	jbe	.L1364
	movq	(%rbx), %rdx
	subq	$2, %rax
	movzwl	(%rdx), %r9d
	rolw	$8, %r9w
	movzwl	%r9w, %r9d
	cmpq	%r9, %rax
	jb	.L1364
	leaq	2(%rdx), %rcx
	subq	%r9, %rax
	movq	%r9, -88(%rbp)
	leaq	(%rcx,%r9), %rdx
	movq	%rcx, -96(%rbp)
	movq	%rdx, (%rbx)
	movq	%rax, 8(%rbx)
	jne	.L1364
	movq	%r15, %rdi
	call	EVP_PKEY_size@PLT
	movq	-88(%rbp), %r9
	testl	%eax, %eax
	js	.L1460
	cltq
	cmpq	%r9, %rax
	jb	.L1461
	movq	%r9, -88(%rbp)
	call	EVP_MD_CTX_new@PLT
	movq	-88(%rbp), %r9
	testq	%rax, %rax
	je	.L1462
	movq	-72(%rbp), %rdx
	xorl	%ecx, %ecx
	leaq	-80(%rbp), %rsi
	movq	%r15, %r8
	movq	%rax, %rdi
	movq	%r9, -104(%rbp)
	movq	%rax, -88(%rbp)
	call	EVP_DigestVerifyInit@PLT
	movq	-88(%rbp), %r10
	movq	-104(%rbp), %r9
	testl	%eax, %eax
	jle	.L1463
	movq	168(%r12), %rax
	movq	776(%rax), %rax
	testq	%rax, %rax
	je	.L1369
	cmpl	$912, 20(%rax)
	je	.L1464
.L1369:
	leaq	-64(%rbp), %rsi
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%r10, -88(%rbp)
	movq	%r9, -104(%rbp)
	call	construct_key_exchange_tbs@PLT
	movq	-88(%rbp), %r10
	testq	%rax, %rax
	je	.L1317
	movq	-104(%rbp), %r9
	movq	-64(%rbp), %rcx
	movq	%r10, %rdi
	movq	%rax, %r8
	movq	-96(%rbp), %rsi
	movq	%r9, %rdx
	call	EVP_DigestVerify@PLT
	movq	-64(%rbp), %rdi
	movl	$2403, %edx
	leaq	.LC0(%rip), %rsi
	movl	%eax, %ebx
	call	CRYPTO_free@PLT
	testl	%ebx, %ebx
	movq	-88(%rbp), %r10
	jle	.L1465
	movq	%r10, %rdi
	call	EVP_MD_CTX_free@PLT
	movl	$3, %eax
	jmp	.L1312
	.p2align 4,,10
	.p2align 3
.L1314:
	movl	$1996, %r9d
	leaq	.LC0(%rip), %r8
	movl	$159, %ecx
	movq	%r12, %rdi
	movl	$421, %edx
	movl	$50, %esi
	call	ossl_statem_fatal@PLT
	xorl	%r10d, %r10d
	jmp	.L1317
	.p2align 4,,10
	.p2align 3
.L1329:
	movl	$2095, %r9d
	leaq	.LC0(%rip), %r8
	movl	$159, %ecx
	movq	%r12, %rdi
	movl	$419, %edx
	movl	$50, %esi
	call	ossl_statem_fatal@PLT
	xorl	%r10d, %r10d
	jmp	.L1317
	.p2align 4,,10
	.p2align 3
.L1449:
	movq	8(%rbx), %rdx
	movl	%r9d, -88(%rbp)
	cmpq	$1, %rdx
	jbe	.L1322
	movq	(%rbx), %rdi
	subq	$2, %rdx
	movzwl	(%rdi), %eax
	rolw	$8, %ax
	movzwl	%ax, %ecx
	cmpq	%rcx, %rdx
	jb	.L1322
	addq	$2, %rdi
	subq	%rcx, %rdx
	leaq	(%rdi,%rcx), %r8
	movq	%rdx, 8(%rbx)
	movq	%r8, (%rbx)
	cmpq	$1, %rdx
	jbe	.L1322
	movzwl	(%r8), %ecx
	subq	$2, %rdx
	rolw	$8, %cx
	movzwl	%cx, %esi
	movl	%ecx, -96(%rbp)
	cmpq	%rsi, %rdx
	jb	.L1322
	addq	$2, %r8
	subq	%rsi, %rdx
	leaq	(%r8,%rsi), %r10
	movq	%r8, -104(%rbp)
	movq	%r10, (%rbx)
	movq	%rdx, 8(%rbx)
	je	.L1322
	movzbl	(%r10), %esi
	subq	$1, %rdx
	movb	%sil, -112(%rbp)
	cmpq	%rsi, %rdx
	jb	.L1322
	leaq	1(%r10), %rcx
	subq	%rsi, %rdx
	leaq	(%rcx,%rsi), %r15
	movq	%rcx, -120(%rbp)
	movq	%r15, (%rbx)
	movq	%rdx, 8(%rbx)
	cmpq	$1, %rdx
	jbe	.L1322
	movzwl	(%r15), %ecx
	leaq	-2(%rdx), %r10
	rolw	$8, %cx
	movzwl	%cx, %edx
	movw	%cx, -128(%rbp)
	cmpq	%rdx, %r10
	jb	.L1322
	addq	$2, %r15
	subq	%rdx, %r10
	leaq	(%r15,%rdx), %rsi
	movq	%r10, 8(%rbx)
	xorl	%edx, %edx
	movq	%rsi, (%rbx)
	movzwl	%ax, %esi
	call	BN_bin2bn@PLT
	movzbl	-112(%rbp), %r11d
	movq	-104(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, 2016(%r12)
	movl	-96(%rbp), %ecx
	movl	-88(%rbp), %r9d
	je	.L1324
	movzwl	%cx, %esi
	xorl	%edx, %edx
	movq	%r8, %rdi
	movb	%r11b, -88(%rbp)
	movl	%r9d, -96(%rbp)
	call	BN_bin2bn@PLT
	movq	%rax, 2024(%r12)
	testq	%rax, %rax
	je	.L1324
	movzbl	-88(%rbp), %esi
	movq	-120(%rbp), %rdi
	xorl	%edx, %edx
	call	BN_bin2bn@PLT
	movq	%rax, 2032(%r12)
	testq	%rax, %rax
	je	.L1324
	movzwl	-128(%rbp), %esi
	xorl	%edx, %edx
	movq	%r15, %rdi
	call	BN_bin2bn@PLT
	movq	%rax, 2040(%r12)
	testq	%rax, %rax
	je	.L1324
	movq	%r12, %rdi
	call	srp_verify_server_param@PLT
	movl	-96(%rbp), %r9d
	testl	%eax, %eax
	je	.L1439
	movq	168(%r12), %rax
	movq	568(%rax), %rax
	movl	32(%rax), %eax
	testb	$3, %al
	je	.L1326
	jmp	.L1438
	.p2align 4,,10
	.p2align 3
.L1322:
	movl	$2041, %r9d
	leaq	.LC0(%rip), %r8
	movl	$159, %ecx
	movq	%r12, %rdi
	movl	$422, %edx
	movl	$50, %esi
	call	ossl_statem_fatal@PLT
	xorl	%r10d, %r10d
	jmp	.L1317
	.p2align 4,,10
	.p2align 3
.L1451:
	movq	8(%rbx), %rax
	testq	%rax, %rax
	je	.L1344
	movq	(%rbx), %rdx
	leaq	1(%rdx), %rcx
	movzbl	(%rdx), %esi
	movq	%rcx, (%rbx)
	leaq	-1(%rax), %rcx
	movq	%rcx, 8(%rbx)
	cmpq	$1, %rcx
	jbe	.L1344
	movzwl	1(%rdx), %ecx
	subq	$3, %rax
	addq	$3, %rdx
	movq	%rdx, (%rbx)
	movq	%rax, 8(%rbx)
	cmpb	$3, %sil
	jne	.L1346
	rolw	$8, %cx
	movl	$1, %edx
	movq	%r12, %rdi
	movl	%r9d, -88(%rbp)
	movzwl	%cx, %r15d
	movl	%r15d, %esi
	call	tls1_check_group_id@PLT
	testl	%eax, %eax
	je	.L1346
	movq	168(%r12), %rdx
	movl	%r15d, %edi
	movq	%rdx, -96(%rbp)
	call	ssl_generate_param_group@PLT
	movq	-96(%rbp), %rdx
	movl	-88(%rbp), %r9d
	testq	%rax, %rax
	movq	%rax, 1032(%rdx)
	je	.L1466
	movq	8(%rbx), %rax
	movl	%r9d, -88(%rbp)
	testq	%rax, %rax
	je	.L1348
	movq	(%rbx), %rdx
	subq	$1, %rax
	movzbl	(%rdx), %r8d
	cmpq	%r8, %rax
	jb	.L1348
	subq	%r8, %rax
	leaq	1(%rdx), %rsi
	leaq	(%rsi,%r8), %rdx
	movq	%rax, 8(%rbx)
	movq	168(%r12), %rax
	movq	%rdx, (%rbx)
	movq	%r8, %rdx
	movq	1032(%rax), %rdi
	call	EVP_PKEY_set1_tls_encodedpoint@PLT
	movl	-88(%rbp), %r9d
	testl	%eax, %eax
	je	.L1467
	movq	168(%r12), %rax
	movq	568(%rax), %rax
	movl	32(%rax), %eax
	testb	$8, %al
	jne	.L1438
	testb	$1, %al
	je	.L1326
	jmp	.L1438
	.p2align 4,,10
	.p2align 3
.L1318:
	movl	$449, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r11, -112(%rbp)
	movl	%r9d, -88(%rbp)
	movq	%r8, -104(%rbp)
	movq	%r10, -96(%rbp)
	call	CRYPTO_free@PLT
	movq	-104(%rbp), %r8
	movq	-96(%rbp), %r10
	movl	$452, %ecx
	leaq	.LC2(%rip), %rdx
	movq	%r8, %rsi
	movq	%r10, %rdi
	call	CRYPTO_strndup@PLT
	movq	-112(%rbp), %r11
	movl	-88(%rbp), %r9d
	testq	%rax, %rax
	movq	%rax, 416(%r11)
	jne	.L1313
	movl	$2019, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movq	%r12, %rdi
	movl	$421, %edx
	movl	$80, %esi
	movq	%rax, -88(%rbp)
	call	ossl_statem_fatal@PLT
	movq	-88(%rbp), %r10
	jmp	.L1317
	.p2align 4,,10
	.p2align 3
.L1344:
	movl	$2199, %r9d
	leaq	.LC0(%rip), %r8
	movl	$160, %ecx
.L1443:
	movl	$420, %edx
	movl	$50, %esi
	movq	%r12, %rdi
	call	ossl_statem_fatal@PLT
	xorl	%r10d, %r10d
	jmp	.L1317
	.p2align 4,,10
	.p2align 3
.L1356:
	movl	$2316, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	jmp	.L1440
	.p2align 4,,10
	.p2align 3
.L1364:
	movl	$2352, %r9d
	leaq	.LC0(%rip), %r8
	movl	$159, %ecx
	jmp	.L1440
	.p2align 4,,10
	.p2align 3
.L1459:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	tls1_set_peer_legacy_sigalg@PLT
	movl	$2334, %r9d
	testl	%eax, %eax
	jne	.L1361
.L1444:
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movl	$365, %edx
	movq	%r12, %rdi
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	xorl	%r10d, %r10d
	jmp	.L1317
	.p2align 4,,10
	.p2align 3
.L1360:
	movl	$2325, %r9d
	leaq	.LC0(%rip), %r8
	movl	$160, %ecx
	jmp	.L1440
	.p2align 4,,10
	.p2align 3
.L1453:
	movl	$2008, %r9d
	leaq	.LC0(%rip), %r8
	movl	$146, %ecx
	movq	%r12, %rdi
	movl	$421, %edx
	movl	$40, %esi
	call	ossl_statem_fatal@PLT
	xorl	%r10d, %r10d
	jmp	.L1317
	.p2align 4,,10
	.p2align 3
.L1452:
	movl	$2295, %r9d
	leaq	.LC0(%rip), %r8
	movl	$244, %ecx
	movq	%r12, %rdi
	movl	$365, %edx
	movl	$10, %esi
	call	ossl_statem_fatal@PLT
	xorl	%r10d, %r10d
	jmp	.L1317
	.p2align 4,,10
	.p2align 3
.L1346:
	movl	$2209, %r9d
	leaq	.LC0(%rip), %r8
	movl	$378, %ecx
.L1445:
	movl	$420, %edx
	movl	$47, %esi
	movq	%r12, %rdi
	call	ossl_statem_fatal@PLT
	xorl	%r10d, %r10d
	jmp	.L1317
.L1461:
	movl	$2368, %r9d
	leaq	.LC0(%rip), %r8
	movl	$264, %ecx
	jmp	.L1440
.L1324:
	movl	$2059, %r9d
	leaq	.LC0(%rip), %r8
	movl	$3, %ecx
	movq	%r12, %rdi
	movl	$422, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	xorl	%r10d, %r10d
	jmp	.L1317
.L1460:
	movl	$2358, %r9d
	jmp	.L1444
.L1348:
	movl	$2221, %r9d
	leaq	.LC0(%rip), %r8
	movl	$159, %ecx
	jmp	.L1443
.L1330:
	movl	$2104, %r9d
	leaq	.LC0(%rip), %r8
	movl	$65, %ecx
.L1435:
	movl	$419, %edx
	movl	$80, %esi
.L1434:
	movq	%r12, %rdi
	call	ossl_statem_fatal@PLT
	movq	$0, -96(%rbp)
	xorl	%r10d, %r10d
	movq	$0, -88(%rbp)
.L1332:
	movq	%r10, %rdi
	call	BN_free@PLT
	movq	-88(%rbp), %rdi
	call	BN_free@PLT
	movq	-96(%rbp), %rdi
	call	BN_free@PLT
	movq	%r15, %rdi
	call	DH_free@PLT
	movq	-120(%rbp), %rdi
	call	EVP_PKEY_free@PLT
	xorl	%r10d, %r10d
	jmp	.L1317
.L1380:
	movq	%r10, -104(%rbp)
	movl	$2116, %r9d
.L1436:
	leaq	.LC0(%rip), %r8
	movl	$3, %ecx
	movl	$419, %edx
	movq	%r12, %rdi
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	movq	-104(%rbp), %r10
	jmp	.L1332
.L1462:
	movq	%rax, -88(%rbp)
	movl	$2375, %r9d
	movl	$65, %ecx
	leaq	.LC0(%rip), %r8
.L1442:
	movl	$365, %edx
	movl	$80, %esi
	movq	%r12, %rdi
	call	ossl_statem_fatal@PLT
	movq	-88(%rbp), %r10
	jmp	.L1317
.L1463:
	movl	$2381, %r9d
.L1441:
	leaq	.LC0(%rip), %r8
	movl	$6, %ecx
	jmp	.L1442
.L1465:
	movl	$2405, %r9d
	leaq	.LC0(%rip), %r8
	movl	$123, %ecx
	movq	%r12, %rdi
	movl	$365, %edx
	movl	$51, %esi
	call	ossl_statem_fatal@PLT
	movq	-88(%rbp), %r10
	jmp	.L1317
.L1464:
	movq	-80(%rbp), %rdi
	xorl	%r8d, %r8d
	movl	$6, %ecx
	movq	%r9, -104(%rbp)
	movl	$4097, %edx
	movl	$-1, %esi
	movq	%r10, -88(%rbp)
	call	RSA_pkey_ctx_ctrl@PLT
	movq	-88(%rbp), %r10
	movq	-104(%rbp), %r9
	testl	%eax, %eax
	jle	.L1371
	movq	-80(%rbp), %rdi
	xorl	%r8d, %r8d
	movl	$-1, %ecx
	movq	%r9, -104(%rbp)
	movl	$4098, %edx
	movl	$24, %esi
	movq	%r10, -88(%rbp)
	call	RSA_pkey_ctx_ctrl@PLT
	movq	-88(%rbp), %r10
	movq	-104(%rbp), %r9
	testl	%eax, %eax
	jg	.L1369
.L1371:
	movq	%r10, -88(%rbp)
	movl	$2389, %r9d
	jmp	.L1441
.L1454:
	movl	$2123, %r9d
	leaq	.LC0(%rip), %r8
	movl	$102, %ecx
	movq	%r12, %rdi
	movl	$419, %edx
	movl	$47, %esi
	movq	%r10, -104(%rbp)
	call	ossl_statem_fatal@PLT
	movq	-104(%rbp), %r10
	jmp	.L1332
.L1337:
	movl	$2136, %r9d
	leaq	.LC0(%rip), %r8
	movl	$102, %ecx
	movq	%r12, %rdi
	movl	$419, %edx
	movl	$47, %esi
	call	ossl_statem_fatal@PLT
	movq	$0, -88(%rbp)
	xorl	%r10d, %r10d
	jmp	.L1332
.L1455:
	movl	$2129, %r9d
	jmp	.L1436
.L1466:
	movl	$2215, %r9d
	leaq	.LC0(%rip), %r8
	movl	$314, %ecx
	movq	%r12, %rdi
	movl	$420, %edx
	movl	$80, %esi
	movq	%rax, -88(%rbp)
	call	ossl_statem_fatal@PLT
	movq	-88(%rbp), %r10
	jmp	.L1317
.L1467:
	movl	$2229, %r9d
	leaq	.LC0(%rip), %r8
	movl	$306, %ecx
	jmp	.L1445
.L1448:
	call	__stack_chk_fail@PLT
.L1456:
	movl	$2142, %r9d
	leaq	.LC0(%rip), %r8
	movl	$3, %ecx
	movq	%r12, %rdi
	movl	$419, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	movq	$0, -88(%rbp)
	xorl	%r10d, %r10d
	jmp	.L1332
.L1457:
	movl	$2149, %r9d
	movl	$394, %ecx
	movl	$419, %edx
	movl	$40, %esi
	leaq	.LC0(%rip), %r8
	jmp	.L1434
.L1458:
	movl	$2155, %r9d
	leaq	.LC0(%rip), %r8
	movl	$6, %ecx
	jmp	.L1435
	.cfi_endproc
.LFE1047:
	.size	tls_process_key_exchange, .-tls_process_key_exchange
	.p2align 4
	.globl	ossl_statem_client_process_message
	.type	ossl_statem_client_process_message, @function
ossl_statem_client_process_message:
.LFB1035:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$64, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	cmpl	$45, 92(%rdi)
	ja	.L1469
	movl	92(%rdi), %eax
	leaq	.L1471(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L1471:
	.long	.L1469-.L1471
	.long	.L1469-.L1471
	.long	.L1484-.L1471
	.long	.L1483-.L1471
	.long	.L1482-.L1471
	.long	.L1481-.L1471
	.long	.L1480-.L1471
	.long	.L1479-.L1471
	.long	.L1478-.L1471
	.long	.L1477-.L1471
	.long	.L1476-.L1471
	.long	.L1475-.L1471
	.long	.L1469-.L1471
	.long	.L1469-.L1471
	.long	.L1469-.L1471
	.long	.L1469-.L1471
	.long	.L1469-.L1471
	.long	.L1469-.L1471
	.long	.L1469-.L1471
	.long	.L1469-.L1471
	.long	.L1469-.L1471
	.long	.L1469-.L1471
	.long	.L1469-.L1471
	.long	.L1469-.L1471
	.long	.L1469-.L1471
	.long	.L1469-.L1471
	.long	.L1469-.L1471
	.long	.L1469-.L1471
	.long	.L1469-.L1471
	.long	.L1469-.L1471
	.long	.L1469-.L1471
	.long	.L1469-.L1471
	.long	.L1469-.L1471
	.long	.L1469-.L1471
	.long	.L1469-.L1471
	.long	.L1469-.L1471
	.long	.L1469-.L1471
	.long	.L1469-.L1471
	.long	.L1474-.L1471
	.long	.L1473-.L1471
	.long	.L1469-.L1471
	.long	.L1472-.L1471
	.long	.L1469-.L1471
	.long	.L1469-.L1471
	.long	.L1469-.L1471
	.long	.L1470-.L1471
	.text
	.p2align 4,,10
	.p2align 3
.L1469:
	movl	$1027, %r9d
	movl	$68, %ecx
	movl	$594, %edx
	movl	$80, %esi
	leaq	.LC0(%rip), %r8
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L1468:
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1505
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1484:
	.cfi_restore_state
	call	dtls_process_hello_verify
	jmp	.L1468
	.p2align 4,,10
	.p2align 3
.L1483:
	call	tls_process_server_hello
	jmp	.L1468
	.p2align 4,,10
	.p2align 3
.L1482:
	call	tls_process_server_certificate
	jmp	.L1468
	.p2align 4,,10
	.p2align 3
.L1481:
	call	tls_process_cert_status_body
	cmpl	$1, %eax
	sbbl	%eax, %eax
	notl	%eax
	andl	$3, %eax
	jmp	.L1468
	.p2align 4,,10
	.p2align 3
.L1480:
	call	tls_process_key_exchange
	jmp	.L1468
	.p2align 4,,10
	.p2align 3
.L1479:
	call	tls_process_certificate_request
	jmp	.L1468
	.p2align 4,,10
	.p2align 3
.L1478:
	call	tls_process_server_done
	jmp	.L1468
	.p2align 4,,10
	.p2align 3
.L1477:
	call	tls_process_new_session_ticket
	jmp	.L1468
	.p2align 4,,10
	.p2align 3
.L1476:
	call	tls_process_change_cipher_spec@PLT
	jmp	.L1468
	.p2align 4,,10
	.p2align 3
.L1475:
	call	tls_process_finished@PLT
	jmp	.L1468
	.p2align 4,,10
	.p2align 3
.L1474:
	movq	8(%rsi), %rax
	movq	$0, -40(%rbp)
	cmpq	$1, %rax
	jbe	.L1489
	movq	(%rsi), %rcx
	subq	$2, %rax
	movzwl	(%rcx), %edx
	rolw	$8, %dx
	movzwl	%dx, %edx
	cmpq	%rdx, %rax
	je	.L1506
.L1489:
	movl	$3684, %r9d
	movl	$159, %ecx
	movl	$444, %edx
	movl	$50, %esi
	leaq	.LC0(%rip), %r8
	call	ossl_statem_fatal@PLT
.L1492:
	movq	-40(%rbp), %rdi
	movl	$3702, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	xorl	%eax, %eax
	jmp	.L1468
	.p2align 4,,10
	.p2align 3
.L1473:
	call	tls_process_cert_verify@PLT
	jmp	.L1468
	.p2align 4,,10
	.p2align 3
.L1472:
	cmpq	$0, 8(%rsi)
	jne	.L1507
	testb	$64, 1495(%rdi)
	jne	.L1508
	movq	8(%rdi), %rax
	movq	192(%rax), %rax
	testb	$8, 96(%rax)
	je	.L1488
	call	SSL_renegotiate@PLT
	movl	$1, %eax
	jmp	.L1468
	.p2align 4,,10
	.p2align 3
.L1470:
	call	tls_process_key_update@PLT
	jmp	.L1468
	.p2align 4,,10
	.p2align 3
.L1508:
	movl	$100, %edx
	movl	$1, %esi
	call	ssl3_send_alert@PLT
	movl	$1, %eax
	jmp	.L1468
	.p2align 4,,10
	.p2align 3
.L1506:
	addq	$2, %rcx
	xorl	%r8d, %r8d
	movl	$1, %r9d
	movq	$0, 8(%rsi)
	leaq	(%rcx,%rax), %rdx
	movq	%rcx, -32(%rbp)
	leaq	-40(%rbp), %rcx
	movq	%rdx, (%rsi)
	leaq	-32(%rbp), %rsi
	movl	$1024, %edx
	movq	%rax, -24(%rbp)
	movq	%rdi, -56(%rbp)
	call	tls_collect_extensions@PLT
	testl	%eax, %eax
	je	.L1492
	movq	-56(%rbp), %rdi
	movq	-40(%rbp), %rdx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$1, %r9d
	movl	$1024, %esi
	call	tls_parse_all_extensions@PLT
	testl	%eax, %eax
	je	.L1492
	movq	-40(%rbp), %rdi
	movl	$3698, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movl	$3, %eax
	jmp	.L1468
	.p2align 4,,10
	.p2align 3
.L1488:
	call	SSL_renegotiate_abbreviated@PLT
	movl	$1, %eax
	jmp	.L1468
	.p2align 4,,10
	.p2align 3
.L1507:
	movl	$3652, %r9d
	movl	$159, %ecx
	movl	$507, %edx
	movl	$50, %esi
	leaq	.LC0(%rip), %r8
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	jmp	.L1468
.L1505:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1035:
	.size	ossl_statem_client_process_message, .-ossl_statem_client_process_message
	.p2align 4
	.globl	tls_process_hello_req
	.type	tls_process_hello_req, @function
tls_process_hello_req:
.LFB1067:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	cmpq	$0, 8(%rsi)
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	jne	.L1515
	testb	$64, 1495(%rdi)
	jne	.L1516
	movq	8(%rdi), %rax
	movq	192(%rax), %rax
	testb	$8, 96(%rax)
	je	.L1513
	call	SSL_renegotiate@PLT
	movl	$1, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1513:
	.cfi_restore_state
	call	SSL_renegotiate_abbreviated@PLT
	movl	$1, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1515:
	.cfi_restore_state
	movl	$3652, %r9d
	movl	$159, %ecx
	movl	$507, %edx
	movl	$50, %esi
	leaq	.LC0(%rip), %r8
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1516:
	.cfi_restore_state
	movl	$100, %edx
	movl	$1, %esi
	call	ssl3_send_alert@PLT
	movl	$1, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1067:
	.size	tls_process_hello_req, .-tls_process_hello_req
	.p2align 4
	.globl	ssl_do_client_cert_cb
	.type	ssl_do_client_cert_cb, @function
ssl_do_client_cert_cb:
.LFB1069:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	1440(%rdi), %rax
	cmpq	$0, 488(%rax)
	je	.L1518
	call	SSL_get_client_CA_list@PLT
	movq	%r13, %rcx
	xorl	%r9d, %r9d
	movq	%r14, %r8
	movq	%rax, %rdx
	movq	1440(%r12), %rax
	movq	%r12, %rsi
	movq	488(%rax), %rdi
	pushq	$0
	pushq	$0
	call	ENGINE_load_ssl_client_cert@PLT
	popq	%rdx
	popq	%rcx
	testl	%eax, %eax
	jne	.L1517
	movq	1440(%r12), %rax
.L1518:
	movq	192(%rax), %rax
	testq	%rax, %rax
	je	.L1520
	leaq	-24(%rbp), %rsp
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L1520:
	.cfi_restore_state
	xorl	%eax, %eax
.L1517:
	leaq	-24(%rbp), %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1069:
	.size	ssl_do_client_cert_cb, .-ssl_do_client_cert_cb
	.section	.rodata
	.align 8
	.type	nonce_label.25203, @object
	.size	nonce_label.25203, 11
nonce_label.25203:
	.string	"resumption"
	.data
	.align 32
	.type	scsv.25348, @object
	.size	scsv.25348, 80
scsv.25348:
	.long	0
	.zero	4
	.quad	0
	.quad	0
	.long	50353664
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.zero	16
	.align 32
	.type	scsv.25347, @object
	.size	scsv.25347, 80
scsv.25347:
	.long	0
	.zero	4
	.quad	0
	.quad	0
	.long	50331903
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.zero	16
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
