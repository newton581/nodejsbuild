	.file	"ecp_nist.c"
	.text
	.p2align 4
	.globl	ec_GFp_nist_group_copy
	.type	ec_GFp_nist_group_copy, @function
ec_GFp_nist_group_copy:
.LFB378:
	.cfi_startproc
	endbr64
	movq	136(%rsi), %rax
	movq	%rax, 136(%rdi)
	jmp	ec_GFp_simple_group_copy@PLT
	.cfi_endproc
.LFE378:
	.size	ec_GFp_nist_group_copy, .-ec_GFp_nist_group_copy
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/ec/ecp_nist.c"
	.text
	.p2align 4
	.globl	ec_GFp_nist_group_set_curve
	.type	ec_GFp_nist_group_set_curve, @function
ec_GFp_nist_group_set_curve:
.LFB379:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	xorl	%ebx, %ebx
	subq	$24, %rsp
	movq	%rdx, -56(%rbp)
	testq	%r8, %r8
	je	.L17
.L4:
	movq	%r12, %rdi
	call	BN_CTX_start@PLT
	call	BN_get0_nist_prime_192@PLT
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	BN_ucmp@PLT
	testl	%eax, %eax
	jne	.L6
	movq	BN_nist_mod_192@GOTPCREL(%rip), %rax
	movq	%rax, 136(%r14)
.L7:
	movq	-56(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r12, %r8
	movq	%r15, %rcx
	movq	%r14, %rdi
	call	ec_GFp_simple_group_set_curve@PLT
	movl	%eax, %r13d
.L12:
	movq	%r12, %rdi
	call	BN_CTX_end@PLT
	movq	%rbx, %rdi
	call	BN_CTX_free@PLT
.L3:
	addq	$24, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	.cfi_restore_state
	call	BN_get0_nist_prime_224@PLT
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	BN_ucmp@PLT
	testl	%eax, %eax
	jne	.L8
	movq	BN_nist_mod_224@GOTPCREL(%rip), %rax
	movq	%rax, 136(%r14)
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L17:
	call	BN_CTX_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L14
	movq	%rax, %rbx
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L8:
	call	BN_get0_nist_prime_256@PLT
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	BN_ucmp@PLT
	testl	%eax, %eax
	jne	.L9
	movq	BN_nist_mod_256@GOTPCREL(%rip), %rax
	movq	%rax, 136(%r14)
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L14:
	xorl	%r13d, %r13d
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L9:
	call	BN_get0_nist_prime_384@PLT
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	BN_ucmp@PLT
	testl	%eax, %eax
	jne	.L10
	movq	BN_nist_mod_384@GOTPCREL(%rip), %rax
	movq	%rax, 136(%r14)
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L10:
	call	BN_get0_nist_prime_521@PLT
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	BN_ucmp@PLT
	testl	%eax, %eax
	jne	.L11
	movq	BN_nist_mod_521@GOTPCREL(%rip), %rax
	movq	%rax, 136(%r14)
	jmp	.L7
.L11:
	movl	$108, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$135, %edx
	xorl	%r13d, %r13d
	movl	$202, %esi
	movl	$16, %edi
	call	ERR_put_error@PLT
	jmp	.L12
	.cfi_endproc
.LFE379:
	.size	ec_GFp_nist_group_set_curve, .-ec_GFp_nist_group_set_curve
	.p2align 4
	.globl	ec_GFp_nist_field_mul
	.type	ec_GFp_nist_field_mul, @function
ec_GFp_nist_field_mul:
.LFB380:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%rdi, %rdi
	je	.L19
	movq	%rsi, %r13
	testq	%rsi, %rsi
	je	.L19
	movq	%rdx, %r14
	testq	%rdx, %rdx
	je	.L19
	movq	%rcx, %r15
	testq	%rcx, %rcx
	je	.L19
	movq	%r8, %r12
	movq	%rdi, %rbx
	xorl	%r8d, %r8d
	testq	%r12, %r12
	je	.L30
.L23:
	movq	%r14, %rsi
	movq	%r12, %rcx
	movq	%r15, %rdx
	movq	%r13, %rdi
	movq	%r8, -56(%rbp)
	call	BN_mul@PLT
	movq	-56(%rbp), %r8
	testl	%eax, %eax
	movl	%eax, %r14d
	je	.L22
	movq	64(%rbx), %rdx
	movq	%r12, %rcx
	movq	%r13, %rsi
	movq	%r13, %rdi
	call	*136(%rbx)
	xorl	%r14d, %r14d
	movq	-56(%rbp), %r8
	testl	%eax, %eax
	setne	%r14b
.L22:
	movq	%r8, %rdi
	call	BN_CTX_free@PLT
	addq	$24, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	.cfi_restore_state
	movl	$127, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$67, %edx
	xorl	%r14d, %r14d
	movl	$200, %esi
	movl	$16, %edi
	call	ERR_put_error@PLT
	xorl	%r8d, %r8d
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L30:
	movq	%r8, -56(%rbp)
	call	BN_CTX_new@PLT
	movq	-56(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %r12
	je	.L25
	movq	%rax, %r8
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L25:
	xorl	%r14d, %r14d
	jmp	.L22
	.cfi_endproc
.LFE380:
	.size	ec_GFp_nist_field_mul, .-ec_GFp_nist_field_mul
	.p2align 4
	.globl	ec_GFp_nist_field_sqr
	.type	ec_GFp_nist_field_sqr, @function
ec_GFp_nist_field_sqr:
.LFB381:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%rsi, %rsi
	sete	%dl
	testq	%r14, %r14
	sete	%al
	orb	%al, %dl
	jne	.L38
	movq	%rdi, %rbx
	testq	%rdi, %rdi
	je	.L38
	movq	%rsi, %r13
	movq	%rcx, %r12
	xorl	%r15d, %r15d
	testq	%rcx, %rcx
	je	.L43
.L35:
	movq	%r14, %rsi
	movq	%r12, %rdx
	movq	%r13, %rdi
	call	BN_sqr@PLT
	movl	%eax, %r14d
	testl	%eax, %eax
	je	.L34
	movq	64(%rbx), %rdx
	movq	%r12, %rcx
	movq	%r13, %rsi
	movq	%r13, %rdi
	call	*136(%rbx)
	xorl	%r14d, %r14d
	testl	%eax, %eax
	setne	%r14b
.L34:
	movq	%r15, %rdi
	call	BN_CTX_free@PLT
	addq	$8, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L38:
	.cfi_restore_state
	movl	$134, %edx
	movl	$201, %esi
	xorl	%r15d, %r15d
	xorl	%r14d, %r14d
	movl	$152, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$16, %edi
	call	ERR_put_error@PLT
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L43:
	call	BN_CTX_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L37
	movq	%rax, %r15
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L37:
	xorl	%r14d, %r14d
	jmp	.L34
	.cfi_endproc
.LFE381:
	.size	ec_GFp_nist_field_sqr, .-ec_GFp_nist_field_sqr
	.p2align 4
	.globl	EC_GFp_nist_method
	.type	EC_GFp_nist_method, @function
EC_GFp_nist_method:
.LFB377:
	.cfi_startproc
	endbr64
	leaq	ret.9690(%rip), %rax
	ret
	.cfi_endproc
.LFE377:
	.size	EC_GFp_nist_method, .-EC_GFp_nist_method
	.section	.data.rel.ro,"aw"
	.align 32
	.type	ret.9690, @object
	.size	ret.9690, 432
ret.9690:
	.long	1
	.long	406
	.quad	ec_GFp_simple_group_init
	.quad	ec_GFp_simple_group_finish
	.quad	ec_GFp_simple_group_clear_finish
	.quad	ec_GFp_nist_group_copy
	.quad	ec_GFp_nist_group_set_curve
	.quad	ec_GFp_simple_group_get_curve
	.quad	ec_GFp_simple_group_get_degree
	.quad	ec_group_simple_order_bits
	.quad	ec_GFp_simple_group_check_discriminant
	.quad	ec_GFp_simple_point_init
	.quad	ec_GFp_simple_point_finish
	.quad	ec_GFp_simple_point_clear_finish
	.quad	ec_GFp_simple_point_copy
	.quad	ec_GFp_simple_point_set_to_infinity
	.quad	ec_GFp_simple_set_Jprojective_coordinates_GFp
	.quad	ec_GFp_simple_get_Jprojective_coordinates_GFp
	.quad	ec_GFp_simple_point_set_affine_coordinates
	.quad	ec_GFp_simple_point_get_affine_coordinates
	.quad	0
	.quad	0
	.quad	0
	.quad	ec_GFp_simple_add
	.quad	ec_GFp_simple_dbl
	.quad	ec_GFp_simple_invert
	.quad	ec_GFp_simple_is_at_infinity
	.quad	ec_GFp_simple_is_on_curve
	.quad	ec_GFp_simple_cmp
	.quad	ec_GFp_simple_make_affine
	.quad	ec_GFp_simple_points_make_affine
	.quad	0
	.quad	0
	.quad	0
	.quad	ec_GFp_nist_field_mul
	.quad	ec_GFp_nist_field_sqr
	.quad	0
	.quad	ec_GFp_simple_field_inv
	.quad	0
	.quad	0
	.quad	0
	.quad	ec_key_simple_priv2oct
	.quad	ec_key_simple_oct2priv
	.quad	0
	.quad	ec_key_simple_generate_key
	.quad	ec_key_simple_check_key
	.quad	ec_key_simple_generate_public_key
	.quad	0
	.quad	0
	.quad	ecdh_simple_compute_key
	.quad	0
	.quad	ec_GFp_simple_blind_coordinates
	.quad	ec_GFp_simple_ladder_pre
	.quad	ec_GFp_simple_ladder_step
	.quad	ec_GFp_simple_ladder_post
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
