	.file	"dh_pmeth.c"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"dh_paramgen_prime_len"
.LC1:
	.string	"dh_rfc5114"
.LC2:
	.string	"dh_param"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"../deps/openssl/openssl/crypto/dh/dh_pmeth.c"
	.section	.rodata.str1.1
.LC4:
	.string	"dh_paramgen_generator"
.LC5:
	.string	"dh_paramgen_subprime_len"
.LC6:
	.string	"dh_paramgen_type"
.LC7:
	.string	"dh_pad"
	.text
	.p2align 4
	.type	pkey_dh_ctrl_str, @function
pkey_dh_ctrl_str:
.LFB833:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$22, %ecx
	movq	%rsi, %rax
	movq	%rdx, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	.LC0(%rip), %rdi
	subq	$8, %rsp
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L16
	movl	$11, %ecx
	leaq	.LC1(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L17
	movl	$9, %ecx
	leaq	.LC2(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L18
	movl	$22, %ecx
	leaq	.LC4(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L19
	movl	$25, %ecx
	leaq	.LC5(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L20
	movl	$17, %ecx
	leaq	.LC6(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L21
	movq	%rax, %rsi
	movl	$7, %ecx
	leaq	.LC7(%rip), %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L13
	movl	$10, %edx
	movq	%r8, %rdi
	xorl	%esi, %esi
	call	strtol@PLT
	xorl	%r9d, %r9d
	movl	$4112, %ecx
	movl	$1024, %edx
	movl	%eax, %r8d
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L17:
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%r8, %rdi
	movq	40(%r12), %r12
	call	strtol@PLT
	cmpl	$3, %eax
	ja	.L13
	movl	%eax, 32(%r12)
	movl	$1, %eax
.L1:
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	.cfi_restore_state
	movl	$10, %edx
	movq	%r8, %rdi
	xorl	%esi, %esi
	call	strtol@PLT
	xorl	%r9d, %r9d
	movl	$4097, %ecx
	movl	$2, %edx
	movl	%eax, %r8d
.L14:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$28, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	EVP_PKEY_CTX_ctrl@PLT
	.p2align 4,,10
	.p2align 3
.L18:
	.cfi_restore_state
	movq	%r8, %rdi
	movq	40(%r12), %r12
	call	OBJ_sn2nid@PLT
	testl	%eax, %eax
	je	.L22
	movl	%eax, 36(%r12)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	.cfi_restore_state
	movl	$10, %edx
	movq	%r8, %rdi
	xorl	%esi, %esi
	call	strtol@PLT
	xorl	%r9d, %r9d
	movl	$4098, %ecx
	movl	$2, %edx
	movl	%eax, %r8d
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L20:
	movl	$10, %edx
	movq	%r8, %rdi
	xorl	%esi, %esi
	call	strtol@PLT
	xorl	%r9d, %r9d
	movl	$4100, %ecx
	movl	$2, %edx
	movl	%eax, %r8d
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L21:
	movl	$10, %edx
	movq	%r8, %rdi
	xorl	%esi, %esi
	call	strtol@PLT
	xorl	%r9d, %r9d
	movl	$4101, %ecx
	movl	$2, %edx
	movl	%eax, %r8d
	jmp	.L14
.L22:
	movl	$244, %r8d
	movl	$110, %edx
	movl	$120, %esi
	movl	$5, %edi
	leaq	.LC3(%rip), %rcx
	call	ERR_put_error@PLT
	.p2align 4,,10
	.p2align 3
.L13:
	movl	$-2, %eax
	jmp	.L1
	.cfi_endproc
.LFE833:
	.size	pkey_dh_ctrl_str, .-pkey_dh_ctrl_str
	.p2align 4
	.type	pkey_dh_ctrl, @function
pkey_dh_ctrl:
.LFB832:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	40(%rdi), %r12
	cmpl	$2, %esi
	je	.L44
	subl	$4097, %esi
	cmpl	$15, %esi
	ja	.L56
	movslq	%edx, %rbx
	leaq	.L26(%rip), %rdx
	movslq	(%rdx,%rsi,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L26:
	.long	.L41-.L26
	.long	.L40-.L26
	.long	.L39-.L26
	.long	.L38-.L26
	.long	.L37-.L26
	.long	.L36-.L26
	.long	.L35-.L26
	.long	.L34-.L26
	.long	.L33-.L26
	.long	.L32-.L26
	.long	.L31-.L26
	.long	.L30-.L26
	.long	.L29-.L26
	.long	.L28-.L26
	.long	.L27-.L26
	.long	.L25-.L26
	.text
.L27:
	testl	%ebx, %ebx
	jle	.L56
	movl	32(%r12), %eax
	testl	%eax, %eax
	jne	.L56
	movl	%ebx, 36(%r12)
	movl	$1, %eax
.L23:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L25:
	.cfi_restore_state
	movl	%ebx, 16(%r12)
	movl	$1, %eax
	jmp	.L23
.L41:
	cmpl	$255, %ebx
	jle	.L56
	movl	%ebx, (%r12)
	movl	$1, %eax
	jmp	.L23
.L40:
	movl	8(%r12), %ecx
	testl	%ecx, %ecx
	jne	.L56
	movl	%ebx, 4(%r12)
	movl	$1, %eax
	jmp	.L23
.L39:
	leal	-1(%rbx), %eax
	cmpl	$2, %eax
	ja	.L56
	movl	36(%r12), %edx
	testl	%edx, %edx
	jne	.L56
	movl	%ebx, 32(%r12)
	movl	$1, %eax
	jmp	.L23
.L38:
	movl	8(%r12), %esi
	testl	%esi, %esi
	je	.L56
	movl	%ebx, 12(%r12)
	movl	$1, %eax
	jmp	.L23
.L37:
	cmpl	$2, %ebx
	ja	.L56
	movl	%ebx, 8(%r12)
	movl	$1, %eax
	jmp	.L23
.L36:
	cmpl	$-2, %ebx
	je	.L58
	leal	-1(%rbx), %eax
	cmpl	$1, %eax
	ja	.L56
	movb	%bl, 48(%r12)
	movl	$1, %eax
	jmp	.L23
.L35:
	movq	%rcx, 64(%r12)
	movl	$1, %eax
	jmp	.L23
.L34:
	movq	64(%r12), %rax
	movq	%rax, (%rcx)
	movl	$1, %eax
	jmp	.L23
.L33:
	testl	%ebx, %ebx
	jle	.L56
	movq	%rbx, 88(%r12)
	movl	$1, %eax
	jmp	.L23
.L32:
	movq	88(%r12), %rax
	movl	%eax, (%rcx)
	movl	$1, %eax
	jmp	.L23
.L31:
	movq	72(%r12), %rdi
	movl	$195, %edx
	leaq	.LC3(%rip), %rsi
	movq	%rcx, -24(%rbp)
	call	CRYPTO_free@PLT
	movq	-24(%rbp), %rcx
	movq	%rcx, 72(%r12)
	testq	%rcx, %rcx
	je	.L43
	movq	%rbx, 80(%r12)
	movl	$1, %eax
	jmp	.L23
.L30:
	movq	72(%r12), %rax
	movq	%rax, (%rcx)
	movl	80(%r12), %eax
	jmp	.L23
.L29:
	movq	56(%r12), %rdi
	movq	%rcx, -24(%rbp)
	call	ASN1_OBJECT_free@PLT
	movq	-24(%rbp), %rcx
	movl	$1, %eax
	movq	%rcx, 56(%r12)
	jmp	.L23
.L28:
	movq	56(%r12), %rax
	movq	%rax, (%rcx)
	movl	$1, %eax
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L44:
	movl	$1, %eax
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L43:
	movq	$0, 80(%r12)
	movl	$1, %eax
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L58:
	movsbl	48(%r12), %eax
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L56:
	movl	$-2, %eax
	jmp	.L23
	.cfi_endproc
.LFE832:
	.size	pkey_dh_ctrl, .-pkey_dh_ctrl
	.p2align 4
	.type	pkey_dh_cleanup, @function
pkey_dh_cleanup:
.LFB830:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	40(%rdi), %r12
	testq	%r12, %r12
	je	.L59
	movq	72(%r12), %rdi
	movl	$73, %edx
	leaq	.LC3(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	56(%r12), %rdi
	call	ASN1_OBJECT_free@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$75, %edx
	popq	%r12
	leaq	.LC3(%rip), %rsi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_free@PLT
	.p2align 4,,10
	.p2align 3
.L59:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE830:
	.size	pkey_dh_cleanup, .-pkey_dh_cleanup
	.p2align 4
	.type	pkey_dh_derive, @function
pkey_dh_derive:
.LFB837:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %rax
	testq	%rax, %rax
	je	.L63
	movq	%rdx, %rbx
	movq	24(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L63
	movq	40(%rdi), %r13
	movq	40(%rax), %r14
	movq	%rsi, %r12
	movq	40(%rdx), %rax
	movzbl	48(%r13), %edx
	movq	32(%rax), %r8
	cmpb	$1, %dl
	je	.L86
	xorl	%eax, %eax
	cmpb	$2, %dl
	je	.L87
.L62:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L86:
	.cfi_restore_state
	testq	%rsi, %rsi
	je	.L88
	movq	%r8, %rsi
	movl	16(%r13), %r8d
	movq	%r14, %rdx
	movq	%r12, %rdi
	testl	%r8d, %r8d
	jne	.L89
	call	DH_compute_key@PLT
.L69:
	testl	%eax, %eax
	js	.L62
	cltq
	movq	%rax, (%rbx)
	leaq	-40(%rbp), %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L63:
	.cfi_restore_state
	movl	$428, %r8d
	movl	$108, %edx
	movl	$112, %esi
	movl	$5, %edi
	leaq	.LC3(%rip), %rcx
	call	ERR_put_error@PLT
	leaq	-40(%rbp), %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L89:
	.cfi_restore_state
	call	DH_compute_key_padded@PLT
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L87:
	movq	88(%r13), %rdx
	testq	%rdx, %rdx
	je	.L62
	cmpq	$0, 56(%r13)
	je	.L62
	testq	%rsi, %rsi
	je	.L90
	cmpq	(%rbx), %rdx
	jne	.L62
	movq	%r14, %rdi
	movq	%r8, -64(%rbp)
	call	DH_size@PLT
	movl	$462, %edx
	leaq	.LC3(%rip), %rsi
	movslq	%eax, %r10
	movq	%r10, %rdi
	movq	%r10, -56(%rbp)
	call	CRYPTO_malloc@PLT
	movq	-56(%rbp), %r10
	movq	-64(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %r15
	je	.L73
	movq	%r14, %rdx
	movq	%r8, %rsi
	movq	%rax, %rdi
	movq	%r10, -56(%rbp)
	call	DH_compute_key_padded@PLT
	movq	-56(%rbp), %r10
	testl	%eax, %eax
	jle	.L73
	movq	72(%r13), %r9
	movq	56(%r13), %r8
	pushq	64(%r13)
	movq	%r10, %rcx
	pushq	80(%r13)
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	(%rbx), %rsi
	call	DH_KDF_X9_42@PLT
	popq	%rdx
	movq	-56(%rbp), %r10
	testl	%eax, %eax
	popq	%rcx
	je	.L73
	movq	88(%r13), %rax
	movq	%rax, (%rbx)
	movl	$1, %eax
.L72:
	movl	$474, %ecx
	movq	%r10, %rsi
	movq	%r15, %rdi
	movl	%eax, -56(%rbp)
	leaq	.LC3(%rip), %rdx
	call	CRYPTO_clear_free@PLT
	movl	-56(%rbp), %eax
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L88:
	movq	%r14, %rdi
	call	DH_size@PLT
	cltq
	movq	%rax, (%rbx)
	movl	$1, %eax
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L90:
	movq	%rdx, (%rbx)
	movl	$1, %eax
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L73:
	xorl	%eax, %eax
	jmp	.L72
	.cfi_endproc
.LFE837:
	.size	pkey_dh_derive, .-pkey_dh_derive
	.p2align 4
	.type	pkey_dh_paramgen, @function
pkey_dh_paramgen:
.LFB835:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	40(%rdi), %rbx
	movl	32(%rbx), %r12d
	testl	%r12d, %r12d
	je	.L92
	cmpl	$2, %r12d
	je	.L93
	cmpl	$3, %r12d
	je	.L94
	cmpl	$1, %r12d
	je	.L133
	movl	$-2, %r12d
.L91:
	leaq	-40(%rbp), %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L92:
	.cfi_restore_state
	movq	%rdi, %r13
	movl	36(%rbx), %edi
	testl	%edi, %edi
	jne	.L134
	cmpq	$0, 56(%r13)
	je	.L118
	call	BN_GENCB_new@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L91
	movq	%rax, %rdi
	movq	%r13, %rsi
	call	evp_pkey_set_cb_translate@PLT
	movl	8(%rbx), %eax
	testl	%eax, %eax
	je	.L101
.L138:
	cmpl	$2, %eax
	jg	.L132
	movl	(%rbx), %eax
	movl	12(%rbx), %edx
	movq	24(%rbx), %rcx
	movl	%eax, -52(%rbp)
	movl	%edx, -56(%rbp)
	movq	%rcx, -64(%rbp)
	call	DSA_new@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L132
	movslq	-56(%rbp), %rdx
	movq	-64(%rbp), %rcx
	cmpl	$-1, %edx
	je	.L135
	testq	%rcx, %rcx
	je	.L136
.L107:
	movl	8(%rbx), %eax
	cmpl	$1, %eax
	je	.L137
	cmpl	$2, %eax
	jne	.L111
	subq	$8, %rsp
	movslq	-52(%rbp), %rsi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	%r15
	movq	%r13, %rdi
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$-1
	call	dsa_builtin_paramgen2@PLT
	addq	$48, %rsp
.L110:
	testl	%eax, %eax
	jle	.L111
	movq	%r15, %rdi
	call	BN_GENCB_free@PLT
	movq	%r13, %rdi
	call	DSA_dup_DH@PLT
	movq	%r13, %rdi
	movq	%rax, %r15
	call	DSA_free@PLT
	movq	%r15, %rdx
	testq	%r15, %r15
	jne	.L131
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L94:
	call	DH_get_2048_256@PLT
	movq	%rax, %rdx
.L131:
	movl	$920, %esi
	movq	%r14, %rdi
	movl	$1, %r12d
	call	EVP_PKEY_assign@PLT
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L133:
	call	DH_get_1024_160@PLT
	movq	%rax, %rdx
	jmp	.L131
	.p2align 4,,10
	.p2align 3
.L134:
	call	DH_new_by_nid@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L91
	movl	$28, %esi
	movq	%r14, %rdi
	movl	$1, %r12d
	call	EVP_PKEY_assign@PLT
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L118:
	movl	8(%rbx), %eax
	xorl	%r15d, %r15d
	testl	%eax, %eax
	jne	.L138
.L101:
	call	DH_new@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L132
	movl	4(%rbx), %edx
	movl	(%rbx), %esi
	movq	%rax, %rdi
	movq	%r15, %rcx
	call	DH_generate_parameters_ex@PLT
	movq	%r15, %rdi
	movl	%eax, %r12d
	call	BN_GENCB_free@PLT
	testl	%r12d, %r12d
	je	.L113
	movq	%r13, %rdx
	movl	$28, %esi
	movq	%r14, %rdi
	call	EVP_PKEY_assign@PLT
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L93:
	call	DH_get_2048_224@PLT
	movq	%rax, %rdx
	jmp	.L131
	.p2align 4,,10
	.p2align 3
.L111:
	movq	%r13, %rdi
	call	DSA_free@PLT
.L132:
	movq	%r15, %rdi
	xorl	%r12d, %r12d
	call	BN_GENCB_free@PLT
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L113:
	movq	%r13, %rdi
	call	DH_free@PLT
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L136:
	cmpl	$2047, -52(%rbp)
	jle	.L108
.L116:
	movl	%edx, -64(%rbp)
	call	EVP_sha256@PLT
	movslq	-64(%rbp), %rdx
	movq	%rax, %rcx
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L135:
	cmpl	$2047, -52(%rbp)
	jle	.L139
	movl	$256, %edx
	testq	%rcx, %rcx
	jne	.L107
	movl	$256, %edx
	jmp	.L116
	.p2align 4,,10
	.p2align 3
.L137:
	pushq	%r15
	movslq	-52(%rbp), %rsi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	movq	%r13, %rdi
	pushq	$0
	pushq	$0
	call	dsa_builtin_paramgen@PLT
	addq	$32, %rsp
	jmp	.L110
	.p2align 4,,10
	.p2align 3
.L139:
	movl	$160, %edx
	testq	%rcx, %rcx
	jne	.L107
	movl	$160, %edx
.L108:
	movl	%edx, -64(%rbp)
	call	EVP_sha1@PLT
	movslq	-64(%rbp), %rdx
	movq	%rax, %rcx
	jmp	.L107
	.cfi_endproc
.LFE835:
	.size	pkey_dh_paramgen, .-pkey_dh_paramgen
	.p2align 4
	.type	pkey_dh_keygen, @function
pkey_dh_keygen:
.LFB836:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	40(%rdi), %rax
	cmpq	$0, 16(%rbx)
	movl	36(%rax), %edi
	je	.L154
	testl	%edi, %edi
	jne	.L142
	call	DH_new@PLT
	movq	%rax, %rdx
	testq	%rdx, %rdx
	je	.L140
.L156:
	movq	(%rbx), %rax
	movq	%r12, %rdi
	movl	(%rax), %esi
	call	EVP_PKEY_assign@PLT
	movq	16(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L149
	movq	%r12, %rdi
	call	EVP_PKEY_copy_parameters@PLT
	testl	%eax, %eax
	je	.L140
.L149:
	movq	40(%r12), %rdi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	DH_generate_key@PLT
	.p2align 4,,10
	.p2align 3
.L154:
	.cfi_restore_state
	testl	%edi, %edi
	je	.L155
.L142:
	call	DH_new_by_nid@PLT
	movq	%rax, %rdx
	testq	%rdx, %rdx
	jne	.L156
.L140:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L155:
	.cfi_restore_state
	movl	$404, %r8d
	movl	$107, %edx
	movl	$113, %esi
	movl	$5, %edi
	leaq	.LC3(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L140
	.cfi_endproc
.LFE836:
	.size	pkey_dh_keygen, .-pkey_dh_keygen
	.p2align 4
	.type	pkey_dh_init, @function
pkey_dh_init:
.LFB829:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$53, %edx
	leaq	.LC3(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$96, %edi
	subq	$8, %rsp
	call	CRYPTO_zalloc@PLT
	testq	%rax, %rax
	je	.L161
	movabsq	$8589936640, %rcx
	movl	$-1, 12(%rax)
	movq	%rcx, (%rax)
	movb	$1, 48(%rax)
	movl	$2, 72(%rbx)
	movq	%rax, 40(%rbx)
	addq	$40, %rax
	movq	%rax, 64(%rbx)
	movl	$1, %eax
.L157:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L161:
	.cfi_restore_state
	movl	$54, %r8d
	movl	$65, %edx
	movl	$125, %esi
	movl	$5, %edi
	leaq	.LC3(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L157
	.cfi_endproc
.LFE829:
	.size	pkey_dh_init, .-pkey_dh_init
	.p2align 4
	.type	pkey_dh_copy, @function
pkey_dh_copy:
.LFB831:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$53, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	leaq	.LC3(%rip), %rsi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movl	$96, %edi
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	call	CRYPTO_zalloc@PLT
	testq	%rax, %rax
	je	.L175
	movl	$-1, 12(%rax)
	movq	%rax, %rbx
	movabsq	$8589936640, %rax
	movq	%rax, (%rbx)
	leaq	40(%rbx), %rax
	movb	$1, 48(%rbx)
	movq	%rax, 64(%r12)
	movq	%rbx, 40(%r12)
	movl	$2, 72(%r12)
	movq	40(%r13), %r12
	movdqu	(%r12), %xmm0
	movups	%xmm0, (%rbx)
	movl	16(%r12), %eax
	movl	%eax, 16(%rbx)
	movq	24(%r12), %rax
	movq	%rax, 24(%rbx)
	movl	32(%r12), %eax
	movl	%eax, 32(%rbx)
	movl	36(%r12), %eax
	movl	%eax, 36(%rbx)
	movzbl	48(%r12), %eax
	movb	%al, 48(%rbx)
	movq	56(%r12), %rdi
	call	OBJ_dup@PLT
	movq	%rax, 56(%rbx)
	testq	%rax, %rax
	je	.L167
	movq	64(%r12), %rax
	movq	72(%r12), %rdi
	movq	%rax, 64(%rbx)
	testq	%rdi, %rdi
	je	.L166
	movq	80(%r12), %rsi
	movl	$102, %ecx
	leaq	.LC3(%rip), %rdx
	call	CRYPTO_memdup@PLT
	movq	%rax, 72(%rbx)
	testq	%rax, %rax
	je	.L167
	movq	80(%r12), %rax
	movq	%rax, 80(%rbx)
.L166:
	movq	88(%r12), %rax
	movq	%rax, 88(%rbx)
	movl	$1, %eax
.L162:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L167:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L175:
	.cfi_restore_state
	movl	$54, %r8d
	movl	$65, %edx
	movl	$125, %esi
	movl	$5, %edi
	leaq	.LC3(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L162
	.cfi_endproc
.LFE831:
	.size	pkey_dh_copy, .-pkey_dh_copy
	.globl	dhx_pkey_meth
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	dhx_pkey_meth, @object
	.size	dhx_pkey_meth, 256
dhx_pkey_meth:
	.long	920
	.long	0
	.quad	pkey_dh_init
	.quad	pkey_dh_copy
	.quad	pkey_dh_cleanup
	.quad	0
	.quad	pkey_dh_paramgen
	.quad	0
	.quad	pkey_dh_keygen
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	pkey_dh_derive
	.quad	pkey_dh_ctrl
	.quad	pkey_dh_ctrl_str
	.zero	48
	.globl	dh_pkey_meth
	.align 32
	.type	dh_pkey_meth, @object
	.size	dh_pkey_meth, 256
dh_pkey_meth:
	.long	28
	.long	0
	.quad	pkey_dh_init
	.quad	pkey_dh_copy
	.quad	pkey_dh_cleanup
	.quad	0
	.quad	pkey_dh_paramgen
	.quad	0
	.quad	pkey_dh_keygen
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	pkey_dh_derive
	.quad	pkey_dh_ctrl
	.quad	pkey_dh_ctrl_str
	.zero	48
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
