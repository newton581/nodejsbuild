	.file	"obj_err.c"
	.text
	.p2align 4
	.globl	ERR_load_OBJ_strings
	.type	ERR_load_OBJ_strings, @function
ERR_load_OBJ_strings:
.LFB203:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$134647808, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_func_error_string@PLT
	testq	%rax, %rax
	je	.L5
	movl	$1, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	.cfi_restore_state
	leaq	OBJ_str_functs(%rip), %rdi
	call	ERR_load_strings_const@PLT
	leaq	OBJ_str_reasons(%rip), %rdi
	call	ERR_load_strings_const@PLT
	movl	$1, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE203:
	.size	ERR_load_OBJ_strings, .-ERR_load_OBJ_strings
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"oid exists"
.LC1:
	.string	"unknown nid"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	OBJ_str_reasons, @object
	.size	OBJ_str_reasons, 48
OBJ_str_reasons:
	.quad	134217830
	.quad	.LC0
	.quad	134217829
	.quad	.LC1
	.quad	0
	.quad	0
	.section	.rodata.str1.1
.LC2:
	.string	"OBJ_add_object"
.LC3:
	.string	"OBJ_add_sigid"
.LC4:
	.string	"OBJ_create"
.LC5:
	.string	"OBJ_dup"
.LC6:
	.string	"OBJ_NAME_new_index"
.LC7:
	.string	"OBJ_nid2ln"
.LC8:
	.string	"OBJ_nid2obj"
.LC9:
	.string	"OBJ_nid2sn"
.LC10:
	.string	"OBJ_txt2obj"
	.section	.data.rel.ro.local
	.align 32
	.type	OBJ_str_functs, @object
	.size	OBJ_str_functs, 160
OBJ_str_functs:
	.quad	134647808
	.quad	.LC2
	.quad	134656000
	.quad	.LC3
	.quad	134627328
	.quad	.LC4
	.quad	134631424
	.quad	.LC5
	.quad	134651904
	.quad	.LC6
	.quad	134635520
	.quad	.LC7
	.quad	134639616
	.quad	.LC8
	.quad	134643712
	.quad	.LC9
	.quad	134660096
	.quad	.LC10
	.quad	0
	.quad	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
