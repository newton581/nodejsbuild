	.file	"eng_ctrl.c"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	""
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"../deps/openssl/openssl/crypto/engine/eng_ctrl.c"
	.text
	.p2align 4
	.globl	ENGINE_ctrl
	.type	ENGINE_ctrl, @function
ENGINE_ctrl:
.LFB870:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%rdi, %rdi
	je	.L66
	movq	%rdi, %r12
	movq	global_engine_lock(%rip), %rdi
	movl	%esi, %r13d
	movq	%rdx, %r15
	movq	%rcx, %r14
	movq	%r8, %rbx
	call	CRYPTO_THREAD_write_lock@PLT
	movl	156(%r12), %edx
	movl	%edx, -52(%rbp)
	movq	global_engine_lock(%rip), %rdi
	call	CRYPTO_THREAD_unlock@PLT
	movl	-52(%rbp), %edx
	movq	112(%r12), %rax
	testl	%edx, %edx
	jle	.L67
	cmpl	$10, %r13d
	je	.L5
	leal	-11(%r13), %edx
	cmpl	$7, %edx
	ja	.L6
	testq	%rax, %rax
	je	.L68
	movl	152(%r12), %r8d
	andl	$2, %r8d
	je	.L69
.L9:
	addq	$24, %rsp
	movq	%rbx, %r8
	movq	%r14, %rcx
	movq	%r15, %rdx
	popq	%rbx
	movl	%r13d, %esi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L5:
	.cfi_restore_state
	xorl	%r8d, %r8d
	testq	%rax, %rax
	setne	%r8b
.L1:
	addq	$24, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	.cfi_restore_state
	testq	%rax, %rax
	jne	.L9
	movl	$171, %r8d
	movl	$120, %edx
	movl	$142, %esi
	movl	$38, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%r8d, %r8d
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L66:
	movl	$129, %r8d
	movl	$67, %edx
	movl	$142, %esi
	movl	$38, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%r8d, %r8d
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L67:
	movl	$137, %r8d
	movl	$130, %edx
	movl	$142, %esi
	movl	$38, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%r8d, %r8d
	jmp	.L1
.L68:
	movl	$158, %r8d
	movl	$120, %edx
	movl	$142, %esi
	movl	$38, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	orl	$-1, %r8d
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L69:
	cmpl	$11, %r13d
	je	.L70
	movl	%r13d, %eax
	andl	$-3, %eax
	cmpl	$13, %eax
	je	.L36
	cmpl	$17, %r13d
	je	.L36
	movq	144(%r12), %rbx
.L14:
	testq	%rbx, %rbx
	je	.L19
	movl	(%rbx), %edx
	movl	%r15d, %esi
	xorl	%eax, %eax
	movq	%rbx, %rcx
	testl	%edx, %edx
	je	.L20
.L21:
	cmpq	$0, 8(%rcx)
	je	.L20
	cmpl	%edx, %esi
	jbe	.L20
	movl	32(%rcx), %edx
	addq	$32, %rcx
	addl	$1, %eax
	testl	%edx, %edx
	jne	.L21
.L20:
	cmpl	%edx, %r15d
	jne	.L19
	cltq
	subl	$12, %r13d
	salq	$5, %rax
	addq	%rbx, %rax
	cmpl	$6, %r13d
	ja	.L25
	leaq	.L27(%rip), %rcx
	movslq	(%rcx,%r13,4), %rdx
	addq	%rcx, %rdx
	notrack jmp	*%rdx
	.section	.rodata
	.align 4
	.align 4
.L27:
	.long	.L32-.L27
	.long	.L25-.L27
	.long	.L31-.L27
	.long	.L30-.L27
	.long	.L29-.L27
	.long	.L28-.L27
	.long	.L26-.L27
	.text
.L26:
	movl	24(%rax), %r8d
	jmp	.L1
.L28:
	movq	16(%rax), %rsi
	leaq	.LC0(%rip), %rax
	movq	%r14, %rdi
	testq	%rsi, %rsi
	cmove	%rax, %rsi
	call	stpcpy@PLT
	movl	%eax, %r8d
	subl	%r14d, %r8d
	jmp	.L1
.L29:
	movq	16(%rax), %rdi
	leaq	.LC0(%rip), %rax
	testq	%rdi, %rdi
	cmove	%rax, %rdi
	call	strlen@PLT
	movl	%eax, %r8d
	jmp	.L1
.L30:
	movq	8(%rax), %rsi
	movq	%r14, %rdi
	call	stpcpy@PLT
	movl	%eax, %r8d
	subl	%r14d, %r8d
	jmp	.L1
.L31:
	movq	8(%rax), %rdi
	call	strlen@PLT
	movl	%eax, %r8d
	jmp	.L1
.L32:
	movl	32(%rax), %edx
	testl	%edx, %edx
	je	.L1
	cmpq	$0, 40(%rax)
	cmovne	%edx, %r8d
	jmp	.L1
.L25:
	movl	$121, %r8d
	movl	$110, %edx
	movl	$172, %esi
	movl	$38, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-1, %r8d
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L36:
	testq	%r14, %r14
	je	.L71
	movq	144(%r12), %rbx
	cmpl	$13, %r13d
	jne	.L14
	testq	%rbx, %rbx
	je	.L15
	movl	(%rbx), %edx
	movq	%rbx, %r12
	testl	%edx, %edx
	je	.L15
.L16:
	movq	8(%r12), %rdi
	movl	%r8d, -52(%rbp)
	testq	%rdi, %rdi
	je	.L15
	movq	%r14, %rsi
	call	strcmp@PLT
	movslq	-52(%rbp), %r8
	testl	%eax, %eax
	je	.L72
	movl	32(%r12), %eax
	addq	$32, %r12
	addl	$1, %r8d
	testl	%eax, %eax
	jne	.L16
.L15:
	movl	$87, %r8d
	movl	$137, %edx
	movl	$172, %esi
	movl	$38, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	orl	$-1, %r8d
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L70:
	movq	144(%r12), %rax
	testq	%rax, %rax
	je	.L1
	movl	(%rax), %edx
	testl	%edx, %edx
	je	.L1
	cmpq	$0, 8(%rax)
	cmovne	%edx, %r8d
	jmp	.L1
.L72:
	salq	$5, %r8
	movl	(%rbx,%r8), %r8d
	jmp	.L1
.L19:
	movl	$98, %r8d
	movl	$138, %edx
	movl	$172, %esi
	movl	$38, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-1, %r8d
	jmp	.L1
.L71:
	movl	$79, %r8d
	movl	$67, %edx
	movl	$172, %esi
	movl	$38, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	orl	$-1, %r8d
	jmp	.L1
	.cfi_endproc
.LFE870:
	.size	ENGINE_ctrl, .-ENGINE_ctrl
	.p2align 4
	.globl	ENGINE_cmd_is_executable
	.type	ENGINE_cmd_is_executable, @function
ENGINE_cmd_is_executable:
.LFB871:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	testq	%rdi, %rdi
	je	.L101
	movq	%rdi, %r12
	movq	global_engine_lock(%rip), %rdi
	movl	%esi, %ebx
	call	CRYPTO_THREAD_write_lock@PLT
	movl	156(%r12), %r13d
	movq	global_engine_lock(%rip), %rdi
	call	CRYPTO_THREAD_unlock@PLT
	movq	112(%r12), %r9
	testl	%r13d, %r13d
	jle	.L102
	testq	%r9, %r9
	je	.L77
	movl	152(%r12), %eax
	andl	$2, %eax
	je	.L103
	movslq	%ebx, %rdx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$18, %esi
	movq	%r12, %rdi
	call	*%r9
.L86:
	xorl	%r8d, %r8d
	testb	$7, %al
	setne	%r8b
	testl	%eax, %eax
	js	.L85
	addq	$8, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L101:
	.cfi_restore_state
	movl	$129, %r8d
	movl	$67, %edx
	movl	$142, %esi
	movl	$38, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$8, %rsp
	xorl	%r8d, %r8d
	popq	%rbx
	movl	%r8d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L79:
	.cfi_restore_state
	movl	$98, %r8d
	movl	$138, %edx
	movl	$172, %esi
	movl	$38, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
.L85:
	movl	$182, %r8d
	movl	$138, %edx
	movl	$170, %esi
	movl	$38, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$8, %rsp
	xorl	%r8d, %r8d
	popq	%rbx
	movl	%r8d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L102:
	.cfi_restore_state
	movl	$137, %r8d
	movl	$130, %edx
	movl	$142, %esi
	movl	$38, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$8, %rsp
	xorl	%r8d, %r8d
	popq	%rbx
	movl	%r8d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L103:
	.cfi_restore_state
	movq	144(%r12), %rdx
	testq	%rdx, %rdx
	je	.L79
	movl	(%rdx), %ecx
	movq	%rdx, %rsi
	testl	%ecx, %ecx
	je	.L80
.L81:
	cmpq	$0, 8(%rsi)
	je	.L80
	cmpl	%ebx, %ecx
	jnb	.L80
	movl	32(%rsi), %ecx
	addq	$32, %rsi
	addl	$1, %eax
	testl	%ecx, %ecx
	jne	.L81
.L80:
	cmpl	%ebx, %ecx
	jne	.L79
	cltq
	salq	$5, %rax
	movl	24(%rdx,%rax), %eax
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L77:
	movl	$158, %r8d
	movl	$120, %edx
	movl	$142, %esi
	movl	$38, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L85
	.cfi_endproc
.LFE871:
	.size	ENGINE_cmd_is_executable, .-ENGINE_cmd_is_executable
	.p2align 4
	.globl	ENGINE_ctrl_cmd
	.type	ENGINE_ctrl_cmd, @function
ENGINE_ctrl_cmd:
.LFB872:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%rdi, %rdi
	je	.L121
	movq	%rsi, %r13
	testq	%rsi, %rsi
	je	.L121
	cmpq	$0, 112(%rdi)
	movq	%r8, -56(%rbp)
	movq	%rdi, %r12
	movl	%r9d, %ebx
	je	.L109
	movq	global_engine_lock(%rip), %rdi
	movq	%rdx, %r14
	movq	%rcx, %r15
	call	CRYPTO_THREAD_write_lock@PLT
	movl	156(%r12), %edx
	movl	%edx, -64(%rbp)
	movq	global_engine_lock(%rip), %rdi
	call	CRYPTO_THREAD_unlock@PLT
	movl	-64(%rbp), %edx
	movq	112(%r12), %rax
	movq	-56(%rbp), %r10
	testl	%edx, %edx
	jle	.L131
	testq	%rax, %rax
	je	.L111
	movl	152(%r12), %edx
	andl	$2, %edx
	je	.L132
	movq	%r10, -56(%rbp)
	movl	$13, %esi
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	*%rax
	movq	-56(%rbp), %r10
	movl	%eax, %esi
.L120:
	testl	%esi, %esi
	jle	.L109
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%r10, %r8
	call	ENGINE_ctrl
	testl	%eax, %eax
	setg	%al
	addq	$40, %rsp
	popq	%rbx
	movzbl	%al, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L132:
	.cfi_restore_state
	movq	144(%r12), %r8
	testq	%r8, %r8
	je	.L113
	movl	(%r8), %ecx
	testl	%ecx, %ecx
	je	.L113
	movq	%r8, %rcx
.L114:
	movq	8(%rcx), %rdi
	movq	%r10, -80(%rbp)
	movl	%edx, -68(%rbp)
	movq	%r8, -64(%rbp)
	movq	%rcx, -56(%rbp)
	testq	%rdi, %rdi
	je	.L113
	movq	%r13, %rsi
	call	strcmp@PLT
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %r8
	testl	%eax, %eax
	movslq	-68(%rbp), %rdx
	movq	-80(%rbp), %r10
	je	.L133
	movl	32(%rcx), %eax
	addq	$32, %rcx
	addl	$1, %edx
	testl	%eax, %eax
	jne	.L114
.L113:
	movl	$87, %r8d
	movl	$137, %edx
	movl	$172, %esi
	movl	$38, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	.p2align 4,,10
	.p2align 3
.L109:
	testl	%ebx, %ebx
	jne	.L134
	movl	$217, %r8d
	movl	$137, %edx
	movl	$178, %esi
	movl	$38, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$40, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L134:
	.cfi_restore_state
	call	ERR_clear_error@PLT
	addq	$40, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L121:
	.cfi_restore_state
	movl	$199, %r8d
	movl	$67, %edx
	movl	$178, %esi
	movl	$38, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$40, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L131:
	.cfi_restore_state
	movl	$137, %r8d
	movl	$130, %edx
	movl	$142, %esi
	movl	$38, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L109
.L133:
	salq	$5, %rdx
	movl	(%r8,%rdx), %esi
	jmp	.L120
.L111:
	movl	$158, %r8d
	movl	$120, %edx
	movl	$142, %esi
	movl	$38, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L109
	.cfi_endproc
.LFE872:
	.size	ENGINE_ctrl_cmd, .-ENGINE_ctrl_cmd
	.p2align 4
	.globl	ENGINE_ctrl_cmd_string
	.type	ENGINE_ctrl_cmd_string, @function
ENGINE_ctrl_cmd_string:
.LFB873:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L156
	testq	%rsi, %rsi
	je	.L156
	cmpq	$0, 112(%rdi)
	movq	%rdi, %r12
	movl	%ecx, %ebx
	je	.L142
	movq	%rsi, %rcx
	movq	%rdx, %r13
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movl	$13, %esi
	call	ENGINE_ctrl
	movl	%eax, %r14d
	testl	%eax, %eax
	jg	.L166
.L142:
	testl	%ebx, %ebx
	jne	.L140
	movl	$255, %r8d
	movl	$137, %edx
	movl	$171, %esi
	movl	$38, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
.L135:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L167
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L140:
	.cfi_restore_state
	call	ERR_clear_error@PLT
	movl	$1, %eax
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L156:
	movl	$237, %r8d
	movl	$67, %edx
	movl	$171, %esi
	movl	$38, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L166:
	movslq	%eax, %r15
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$18, %esi
	movq	%r15, %rdx
	movq	%r12, %rdi
	call	ENGINE_ctrl
	testl	%eax, %eax
	js	.L168
	testb	$7, %al
	je	.L145
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r15, %rdx
	movl	$18, %esi
	movq	%r12, %rdi
	call	ENGINE_ctrl
	testl	%eax, %eax
	js	.L169
	testb	$4, %al
	jne	.L170
	testq	%r13, %r13
	je	.L171
	testb	$2, %al
	jne	.L172
	testb	$1, %al
	je	.L173
	movl	$10, %edx
	leaq	-64(%rbp), %rsi
	movq	%r13, %rdi
	call	strtol@PLT
	movq	%rax, %rdx
	movq	-64(%rbp), %rax
	cmpq	%r13, %rax
	je	.L153
	cmpb	$0, (%rax)
	jne	.L153
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L165:
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	ENGINE_ctrl
	testl	%eax, %eax
	setg	%al
	movzbl	%al, %eax
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L168:
	movl	$182, %r8d
	movl	$138, %edx
	movl	$170, %esi
	movl	$38, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
.L145:
	movl	$259, %r8d
	movl	$134, %edx
	movl	$171, %esi
	movl	$38, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L170:
	testq	%r13, %r13
	je	.L149
	movl	$279, %r8d
	movl	$136, %edx
	movl	$171, %esi
	movl	$38, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L149:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	jmp	.L165
	.p2align 4,,10
	.p2align 3
.L169:
	movl	$270, %r8d
.L164:
	movl	$110, %edx
	movl	$171, %esi
	movl	$38, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L172:
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	xorl	%edx, %edx
	jmp	.L165
	.p2align 4,,10
	.p2align 3
.L153:
	movl	$319, %r8d
	movl	$133, %edx
	movl	$171, %esi
	movl	$38, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L173:
	movl	$313, %r8d
	jmp	.L164
.L171:
	movl	$295, %r8d
	movl	$135, %edx
	movl	$171, %esi
	movl	$38, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L135
.L167:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE873:
	.size	ENGINE_ctrl_cmd_string, .-ENGINE_ctrl_cmd_string
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
