	.file	"mem_sec.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/mem_sec.c"
	.align 8
.LC1:
	.string	"assertion failed: list >= 0 && list < sh.freelist_size"
	.align 8
.LC2:
	.string	"assertion failed: ((ptr - sh.arena) & ((sh.arena_size >> list) - 1)) == 0"
	.align 8
.LC3:
	.string	"assertion failed: bit > 0 && bit < sh.bittable_size"
	.align 8
.LC4:
	.string	"assertion failed: !TESTBIT(table, bit)"
	.text
	.p2align 4
	.type	sh_setbit, @function
sh_setbit:
.LFB183:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testl	%esi, %esi
	js	.L2
	movq	%rdx, %r8
	movslq	%esi, %rdx
	cmpq	40+sh(%rip), %rdx
	movl	%esi, %ecx
	jge	.L2
	movq	24+sh(%rip), %rsi
	movq	%rdi, %rax
	subq	16+sh(%rip), %rax
	shrq	%cl, %rsi
	leaq	-1(%rsi), %rdx
	testq	%rax, %rdx
	jne	.L13
	xorl	%edx, %edx
	movl	$1, %r9d
	divq	%rsi
	movq	%r9, %rdi
	salq	%cl, %rdi
	movq	%rdi, %rcx
	addq	%rax, %rcx
	je	.L5
	cmpq	%rcx, 72+sh(%rip)
	jbe	.L5
	movq	%rcx, %rdx
	andl	$7, %ecx
	shrq	$3, %rdx
	addq	%r8, %rdx
	movzbl	(%rdx), %esi
	btq	%rcx, %rsi
	movq	%rsi, %rax
	jc	.L14
	salq	%cl, %r9
	orl	%r9d, %eax
	movb	%al, (%rdx)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2:
	.cfi_restore_state
	movl	$332, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	OPENSSL_die@PLT
.L5:
	movl	$335, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	call	OPENSSL_die@PLT
.L14:
	movl	$336, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	call	OPENSSL_die@PLT
.L13:
	movl	$333, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	OPENSSL_die@PLT
	.cfi_endproc
.LFE183:
	.size	sh_setbit, .-sh_setbit
	.section	.rodata.str1.8
	.align 8
.LC5:
	.string	"assertion failed: WITHIN_FREELIST(list)"
	.align 8
.LC6:
	.string	"assertion failed: WITHIN_ARENA(ptr)"
	.align 8
.LC7:
	.string	"assertion failed: temp->next == NULL || WITHIN_ARENA(temp->next)"
	.align 8
.LC8:
	.string	"assertion failed: (char **)temp->next->p_next == list"
	.text
	.p2align 4
	.type	sh_add_to_list, @function
sh_add_to_list:
.LFB184:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	32+sh(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	cmpq	%rdi, %rax
	ja	.L16
	movq	40+sh(%rip), %rdx
	leaq	(%rax,%rdx,8), %rax
	cmpq	%rax, %rdi
	jnb	.L16
	movq	16+sh(%rip), %rdx
	cmpq	%rsi, %rdx
	ja	.L18
	movq	24+sh(%rip), %rcx
	addq	%rdx, %rcx
	cmpq	%rsi, %rcx
	jbe	.L18
	movq	(%rdi), %rax
	movq	%rax, (%rsi)
	testq	%rax, %rax
	je	.L20
	cmpq	%rax, %rcx
	jbe	.L26
	cmpq	%rax, %rdx
	ja	.L26
	movq	%rdi, 8(%rsi)
	cmpq	%rdi, 8(%rax)
	jne	.L30
	movq	%rsi, 8(%rax)
	movq	%rsi, (%rdi)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	.cfi_restore_state
	movq	%rdi, 8(%rsi)
	movq	%rsi, (%rdi)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L18:
	.cfi_restore_state
	movl	$345, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	call	OPENSSL_die@PLT
.L16:
	movl	$344, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	call	OPENSSL_die@PLT
.L26:
	movl	$349, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	call	OPENSSL_die@PLT
.L30:
	movl	$353, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC8(%rip), %rdi
	call	OPENSSL_die@PLT
	.cfi_endproc
.LFE184:
	.size	sh_add_to_list, .-sh_add_to_list
	.section	.rodata.str1.8
	.align 8
.LC9:
	.string	"assertion failed: TESTBIT(table, bit)"
	.text
	.p2align 4
	.type	sh_clearbit, @function
sh_clearbit:
.LFB182:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testl	%esi, %esi
	js	.L32
	movq	%rdx, %r9
	movslq	%esi, %rdx
	cmpq	40+sh(%rip), %rdx
	movl	%esi, %ecx
	jge	.L32
	movq	24+sh(%rip), %rsi
	movq	%rdi, %rax
	subq	16+sh(%rip), %rax
	shrq	%cl, %rsi
	leaq	-1(%rsi), %rdx
	testq	%rax, %rdx
	jne	.L42
	xorl	%edx, %edx
	movl	$1, %r8d
	divq	%rsi
	movq	%r8, %rdi
	salq	%cl, %rdi
	movq	%rdi, %rcx
	addq	%rax, %rcx
	je	.L35
	cmpq	%rcx, 72+sh(%rip)
	jbe	.L35
	movq	%rcx, %rdx
	andl	$7, %ecx
	shrq	$3, %rdx
	addq	%r9, %rdx
	movzbl	(%rdx), %esi
	btq	%rcx, %rsi
	jnc	.L43
	salq	%cl, %r8
	notl	%r8d
	andl	%esi, %r8d
	movb	%r8b, (%rdx)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L32:
	.cfi_restore_state
	movl	$320, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	OPENSSL_die@PLT
.L35:
	movl	$323, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	call	OPENSSL_die@PLT
.L43:
	movl	$324, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC9(%rip), %rdi
	call	OPENSSL_die@PLT
.L42:
	movl	$321, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	OPENSSL_die@PLT
	.cfi_endproc
.LFE182:
	.size	sh_clearbit, .-sh_clearbit
	.p2align 4
	.type	sh_testbit, @function
sh_testbit:
.LFB181:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testl	%esi, %esi
	js	.L45
	movq	%rdx, %r9
	movslq	%esi, %rdx
	cmpq	40+sh(%rip), %rdx
	movl	%esi, %ecx
	jge	.L45
	movq	24+sh(%rip), %rsi
	movq	%rdi, %rax
	subq	16+sh(%rip), %rax
	shrq	%cl, %rsi
	leaq	-1(%rsi), %rdx
	testq	%rax, %rdx
	jne	.L54
	xorl	%edx, %edx
	movl	$1, %r8d
	divq	%rsi
	movq	%r8, %rdi
	salq	%cl, %rdi
	movq	%rdi, %rcx
	addq	%rax, %rcx
	je	.L48
	cmpq	%rcx, 72+sh(%rip)
	jbe	.L48
	movq	%rcx, %rax
	andl	$7, %ecx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	shrq	$3, %rax
	salq	%cl, %r8
	movzbl	(%r9,%rax), %eax
	andl	%r8d, %eax
	ret
.L45:
	.cfi_restore_state
	movl	$309, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	OPENSSL_die@PLT
.L48:
	movl	$312, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	call	OPENSSL_die@PLT
.L54:
	movl	$310, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	OPENSSL_die@PLT
	.cfi_endproc
.LFE181:
	.size	sh_testbit, .-sh_testbit
	.section	.rodata.str1.8
	.align 8
.LC10:
	.string	"assertion failed: (bit & 1) == 0"
	.align 8
.LC11:
	.string	"assertion failed: sh_testbit(ptr, list, sh.bittable)"
	.text
	.p2align 4
	.type	sh_actual_size, @function
sh_actual_size:
.LFB192:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	16+sh(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	cmpq	%rdi, %rax
	ja	.L56
	movq	24+sh(%rip), %r12
	leaq	(%rax,%r12), %rdx
	cmpq	%rdx, %rdi
	jnb	.L56
	leaq	(%rdi,%r12), %rcx
	movq	40+sh(%rip), %rbx
	movq	48+sh(%rip), %rsi
	xorl	%edx, %edx
	subq	%rax, %rcx
	movq	56+sh(%rip), %r8
	movq	%rcx, %rax
	subq	$1, %rbx
	divq	%rsi
	cmpq	%rsi, %rcx
	jb	.L58
	movq	%rax, %rdx
	shrq	$3, %rdx
	jmp	.L68
	.p2align 4,,10
	.p2align 3
.L70:
	testb	$1, %al
	jne	.L69
	movq	%rax, %rdx
	subq	$1, %rbx
	shrq	%rax
	je	.L58
	shrq	$4, %rdx
.L68:
	movzbl	(%r8,%rdx), %ecx
	movl	%eax, %edx
	andl	$7, %edx
	btq	%rdx, %rcx
	jnc	.L70
.L58:
	movq	%r8, %rdx
	movl	%ebx, %esi
	call	sh_testbit
	testl	%eax, %eax
	je	.L71
	movq	%r12, %rax
	movl	%ebx, %ecx
	popq	%rbx
	popq	%r12
	shrq	%cl, %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L69:
	.cfi_restore_state
	movl	$298, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	call	OPENSSL_die@PLT
.L56:
	movl	$639, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	call	OPENSSL_die@PLT
.L71:
	movl	$643, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	call	OPENSSL_die@PLT
	.cfi_endproc
.LFE192:
	.size	sh_actual_size, .-sh_actual_size
	.section	.rodata.str1.8
	.align 8
.LC12:
	.string	"assertion failed: ptr == sh_find_my_buddy(buddy, list)"
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC13:
	.string	"assertion failed: ptr != NULL"
	.section	.rodata.str1.8
	.align 8
.LC14:
	.string	"assertion failed: !sh_testbit(ptr, list, sh.bitmalloc)"
	.align 8
.LC15:
	.string	"assertion failed: WITHIN_FREELIST(temp2->p_next) || WITHIN_ARENA(temp2->p_next)"
	.align 8
.LC16:
	.string	"assertion failed: sh.freelist[list] == ptr"
	.text
	.p2align 4
	.type	sh_free, @function
sh_free:
.LFB191:
	.cfi_startproc
	testq	%rdi, %rdi
	je	.L122
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	16+sh(%rip), %rax
	cmpq	%rdi, %rax
	ja	.L74
	movq	24+sh(%rip), %rcx
	leaq	(%rax,%rcx), %rdx
	cmpq	%rdx, %rdi
	jnb	.L74
	movq	40+sh(%rip), %rsi
	addq	%rdi, %rcx
	xorl	%edx, %edx
	movq	56+sh(%rip), %r8
	subq	%rax, %rcx
	leaq	-1(%rsi), %r12
	movq	48+sh(%rip), %rsi
	movq	%rcx, %rax
	divq	%rsi
	cmpq	%rsi, %rcx
	jb	.L76
	movq	%rax, %rdx
	shrq	$3, %rdx
	jmp	.L127
	.p2align 4,,10
	.p2align 3
.L129:
	testb	$1, %al
	jne	.L128
	movq	%rax, %rdx
	subq	$1, %r12
	shrq	%rax
	je	.L76
	shrq	$4, %rdx
.L127:
	movzbl	(%r8,%rdx), %ecx
	movl	%eax, %edx
	andl	$7, %edx
	btq	%rdx, %rcx
	jnc	.L129
.L76:
	movq	%r8, %rdx
	movl	%r12d, %esi
	movq	%rbx, %rdi
	call	sh_testbit
	testl	%eax, %eax
	je	.L130
	movq	64+sh(%rip), %rdx
	movl	%r12d, %esi
	movq	%rbx, %rdi
	call	sh_clearbit
	movq	32+sh(%rip), %rax
	movq	%rbx, %rsi
	leaq	(%rax,%r12,8), %rdi
	call	sh_add_to_list
	jmp	.L81
	.p2align 4,,10
	.p2align 3
.L137:
	movq	64+sh(%rip), %r8
	movzbl	(%r8,%rdx), %edx
	btq	%rcx, %rdx
	jc	.L72
	movl	%r12d, %ecx
	movq	$-1, %rdx
	movq	%rsi, %r13
	salq	%cl, %rdx
	movq	%rdx, %rcx
	notq	%rcx
	andq	%rcx, %rax
	imulq	%rdi, %rax
	addq	%rax, %r13
	je	.L72
	xorl	%edx, %edx
	xorl	%r10d, %r10d
	divq	%rdi
	addq	%r9, %rax
	movq	%rax, %rdx
	shrq	$3, %rax
	xorq	$1, %rdx
	movzbl	(%r15,%rax), %r11d
	movl	%edx, %r9d
	andl	$7, %r9d
	btq	%r9, %r11
	jnc	.L82
	movzbl	(%r8,%rax), %eax
	btq	%r9, %rax
	jc	.L82
	andq	%rdx, %rcx
	imulq	%rcx, %rdi
	leaq	(%rsi,%rdi), %r10
.L82:
	cmpq	%r10, %rbx
	jne	.L131
	testq	%rbx, %rbx
	je	.L132
	movq	%r8, %rdx
	movl	%r14d, %esi
	movq	%rbx, %rdi
	call	sh_testbit
	testl	%eax, %eax
	jne	.L133
	movq	%r15, %rdx
	movl	%r14d, %esi
	movq	%rbx, %rdi
	call	sh_clearbit
	movq	(%rbx), %rax
	testq	%rax, %rax
	je	.L86
	movq	8(%rbx), %rdx
	movq	%rdx, 8(%rax)
.L86:
	movq	8(%rbx), %rdx
	movq	%rax, (%rdx)
	movq	(%rbx), %rax
	testq	%rax, %rax
	je	.L87
	movq	8(%rax), %rax
	movq	32+sh(%rip), %rdx
	cmpq	%rdx, %rax
	jb	.L88
	movq	40+sh(%rip), %rcx
	leaq	(%rdx,%rcx,8), %rdx
	cmpq	%rdx, %rax
	jb	.L87
.L88:
	movq	16+sh(%rip), %rdx
	cmpq	%rdx, %rax
	jb	.L90
	addq	24+sh(%rip), %rdx
	cmpq	%rdx, %rax
	jnb	.L90
.L87:
	movq	64+sh(%rip), %rdx
	movl	%r14d, %esi
	movq	%rbx, %rdi
	call	sh_testbit
	testl	%eax, %eax
	jne	.L134
	movq	56+sh(%rip), %rdx
	movl	%r14d, %esi
	movq	%r13, %rdi
	call	sh_clearbit
	movq	0(%r13), %rax
	testq	%rax, %rax
	je	.L92
	movq	8(%r13), %rdx
	movq	%rdx, 8(%rax)
.L92:
	movq	8(%r13), %rdx
	movq	%rax, (%rdx)
	movq	0(%r13), %rax
	testq	%rax, %rax
	je	.L93
	movq	8(%rax), %rax
	movq	32+sh(%rip), %rdx
	cmpq	%rdx, %rax
	jb	.L94
	movq	40+sh(%rip), %rcx
	leaq	(%rdx,%rcx,8), %rdx
	cmpq	%rdx, %rax
	jb	.L93
.L94:
	movq	16+sh(%rip), %rdx
	cmpq	%rdx, %rax
	jb	.L90
	addq	24+sh(%rip), %rdx
	cmpq	%rdx, %rax
	jnb	.L90
.L93:
	subq	$1, %r12
	movq	%r13, %rax
	pxor	%xmm0, %xmm0
	cmpq	%r13, %rbx
	cmovnb	%rbx, %rax
	cmova	%r13, %rbx
	movl	%r12d, %esi
	movups	%xmm0, (%rax)
	movq	64+sh(%rip), %rdx
	movq	%rbx, %rdi
	call	sh_testbit
	testl	%eax, %eax
	jne	.L135
	movq	56+sh(%rip), %rdx
	movl	%r12d, %esi
	movq	%rbx, %rdi
	call	sh_setbit
	movq	32+sh(%rip), %rax
	movq	%rbx, %rsi
	leaq	(%rax,%r12,8), %rdi
	call	sh_add_to_list
	movq	32+sh(%rip), %rax
	cmpq	%rbx, (%rax,%r12,8)
	jne	.L136
.L81:
	movq	16+sh(%rip), %rsi
	movl	%r12d, %ecx
	movq	%rbx, %rax
	xorl	%edx, %edx
	movq	24+sh(%rip), %rdi
	movl	$1, %r9d
	movq	56+sh(%rip), %r15
	movl	%r12d, %r14d
	subq	%rsi, %rax
	salq	%cl, %r9
	shrq	%cl, %rdi
	divq	%rdi
	leaq	(%rax,%r9), %rdx
	movq	%rdx, %rax
	shrq	$3, %rdx
	xorq	$1, %rax
	movzbl	(%r15,%rdx), %r8d
	movl	%eax, %ecx
	andl	$7, %ecx
	btq	%rcx, %r8
	jc	.L137
.L72:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
.L122:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
.L90:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movl	$372, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC15(%rip), %rdi
	call	OPENSSL_die@PLT
.L133:
	movl	$614, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC14(%rip), %rdi
	call	OPENSSL_die@PLT
.L132:
	movl	$613, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC13(%rip), %rdi
	call	OPENSSL_die@PLT
.L131:
	movl	$612, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC12(%rip), %rdi
	call	OPENSSL_die@PLT
.L136:
	movl	$631, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC16(%rip), %rdi
	call	OPENSSL_die@PLT
.L135:
	movl	$628, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC14(%rip), %rdi
	call	OPENSSL_die@PLT
.L134:
	movl	$617, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC14(%rip), %rdi
	call	OPENSSL_die@PLT
.L128:
	movl	$298, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	call	OPENSSL_die@PLT
.L130:
	movl	$606, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	call	OPENSSL_die@PLT
.L74:
	movl	$601, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	call	OPENSSL_die@PLT
	.cfi_endproc
.LFE191:
	.size	sh_free, .-sh_free
	.section	.rodata.str1.1
.LC17:
	.string	"assertion failed: size > 0"
	.section	.rodata.str1.8
	.align 8
.LC18:
	.string	"assertion failed: (size & (size - 1)) == 0"
	.section	.rodata.str1.1
.LC19:
	.string	"assertion failed: minsize > 0"
	.section	.rodata.str1.8
	.align 8
.LC20:
	.string	"assertion failed: (minsize & (minsize - 1)) == 0"
	.align 8
.LC21:
	.string	"assertion failed: sh.freelist != NULL"
	.align 8
.LC22:
	.string	"assertion failed: sh.bittable != NULL"
	.align 8
.LC23:
	.string	"assertion failed: sh.bitmalloc != NULL"
	.text
	.p2align 4
	.globl	CRYPTO_secure_malloc_init
	.type	CRYPTO_secure_malloc_init, @function
CRYPTO_secure_malloc_init:
.LFB170:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movl	secure_mem_initialized(%rip), %eax
	testl	%eax, %eax
	je	.L139
.L141:
	addq	$8, %rsp
	xorl	%r13d, %r13d
	popq	%rbx
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L139:
	.cfi_restore_state
	movq	%rdi, %rbx
	movl	%esi, %r12d
	call	CRYPTO_THREAD_lock_new@PLT
	movq	%rax, sec_malloc_lock(%rip)
	testq	%rax, %rax
	je	.L141
	pxor	%xmm0, %xmm0
	movaps	%xmm0, sh(%rip)
	movaps	%xmm0, 16+sh(%rip)
	movaps	%xmm0, 32+sh(%rip)
	movaps	%xmm0, 48+sh(%rip)
	movaps	%xmm0, 64+sh(%rip)
	testq	%rbx, %rbx
	je	.L181
	leaq	-1(%rbx), %rcx
	andq	%rbx, %rcx
	jne	.L182
	testl	%r12d, %r12d
	jle	.L183
	leal	-1(%r12), %r13d
	andl	%r12d, %r13d
	je	.L180
	jmp	.L145
	.p2align 4,,10
	.p2align 3
.L146:
	addl	%r12d, %r12d
.L180:
	cmpl	$15, %r12d
	jle	.L146
	movslq	%r12d, %r12
	xorl	%edx, %edx
	movq	%rbx, %rax
	movq	%rbx, 24+sh(%rip)
	divq	%r12
	movq	%r12, 48+sh(%rip)
	addq	%rax, %rax
	movq	%rax, %rdx
	movq	%rax, 72+sh(%rip)
	shrq	$3, %rdx
	je	.L162
	.p2align 4,,10
	.p2align 3
.L149:
	movq	%rcx, %rdi
	addq	$1, %rcx
	shrq	%rax
	jne	.L149
	movq	%rdi, 40+sh(%rip)
	movl	$410, %edx
	salq	$3, %rdi
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_zalloc@PLT
	movq	%rax, 32+sh(%rip)
	testq	%rax, %rax
	je	.L184
	movq	72+sh(%rip), %rdi
	movl	$415, %edx
	leaq	.LC0(%rip), %rsi
	shrq	$3, %rdi
	call	CRYPTO_zalloc@PLT
	movq	%rax, 56+sh(%rip)
	testq	%rax, %rax
	je	.L185
	movq	72+sh(%rip), %rdi
	movl	$420, %edx
	leaq	.LC0(%rip), %rsi
	shrq	$3, %rdi
	call	CRYPTO_zalloc@PLT
	movq	%rax, 64+sh(%rip)
	testq	%rax, %rax
	je	.L186
	movl	$30, %edi
	call	sysconf@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jle	.L163
	leaq	(%rax,%rax), %rbx
.L153:
	xorl	%edi, %edi
	xorl	%r9d, %r9d
	movl	$-1, %r8d
	movl	$34, %ecx
	movq	24+sh(%rip), %rsi
	movl	$3, %edx
	addq	%rbx, %rsi
	movq	%rsi, 8+sh(%rip)
	call	mmap@PLT
	movq	32+sh(%rip), %rdi
	movq	%rax, sh(%rip)
	cmpq	$-1, %rax
	je	.L148
	movq	56+sh(%rip), %rdx
	leaq	(%rax,%r12), %rdi
	xorl	%esi, %esi
	movq	%rdi, 16+sh(%rip)
	call	sh_setbit
	movq	16+sh(%rip), %rsi
	movq	32+sh(%rip), %rdi
	call	sh_add_to_list
	movq	sh(%rip), %rdi
	xorl	%edx, %edx
	movq	%r12, %rsi
	call	mprotect@PLT
	xorl	%edx, %edx
	movq	%r12, %rsi
	shrl	$31, %eax
	movl	%eax, %r13d
	movq	24+sh(%rip), %rax
	addl	$1, %r13d
	leaq	-1(%rbx,%rax), %rdi
	movq	%r12, %rax
	negq	%rax
	andq	%rax, %rdi
	addq	sh(%rip), %rdi
	call	mprotect@PLT
	movl	$1, %ecx
	movl	$325, %edi
	movq	24+sh(%rip), %rdx
	testl	%eax, %eax
	movq	16+sh(%rip), %rsi
	movl	$2, %eax
	cmovs	%eax, %r13d
	xorl	%eax, %eax
	call	syscall@PLT
	testq	%rax, %rax
	js	.L187
	movq	24+sh(%rip), %rsi
	movq	16+sh(%rip), %rdi
.L158:
	movl	$16, %edx
	call	madvise@PLT
	movl	$1, secure_mem_initialized(%rip)
	testl	%eax, %eax
	movl	$2, %eax
	cmovs	%eax, %r13d
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	movl	%r13d, %eax
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L162:
	.cfi_restore_state
	xorl	%edi, %edi
.L148:
	movl	$502, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	56+sh(%rip), %rdi
	movl	$503, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	64+sh(%rip), %rdi
	movl	$504, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	sh(%rip), %rdi
	testq	%rdi, %rdi
	je	.L160
	movq	8+sh(%rip), %rsi
	testq	%rsi, %rsi
	jne	.L188
.L160:
	movq	sec_malloc_lock(%rip), %rdi
	pxor	%xmm0, %xmm0
	movaps	%xmm0, sh(%rip)
	movaps	%xmm0, 16+sh(%rip)
	movaps	%xmm0, 32+sh(%rip)
	movaps	%xmm0, 48+sh(%rip)
	movaps	%xmm0, 64+sh(%rip)
	call	CRYPTO_THREAD_lock_free@PLT
	movl	%r13d, %eax
	movq	$0, sec_malloc_lock(%rip)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L163:
	.cfi_restore_state
	movl	$4096, %r12d
	movl	$8192, %ebx
	jmp	.L153
	.p2align 4,,10
	.p2align 3
.L187:
	call	__errno_location@PLT
	movq	24+sh(%rip), %rsi
	movq	16+sh(%rip), %rdi
	cmpl	$38, (%rax)
	je	.L189
.L165:
	movl	$2, %r13d
	jmp	.L158
	.p2align 4,,10
	.p2align 3
.L188:
	call	munmap@PLT
	jmp	.L160
.L189:
	call	mlock@PLT
	movq	24+sh(%rip), %rsi
	movq	16+sh(%rip), %rdi
	testl	%eax, %eax
	jns	.L158
	jmp	.L165
.L145:
	movl	$389, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC20(%rip), %rdi
	call	OPENSSL_die@PLT
.L182:
	movl	$387, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC18(%rip), %rdi
	call	OPENSSL_die@PLT
.L183:
	movl	$388, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC19(%rip), %rdi
	call	OPENSSL_die@PLT
.L186:
	movl	$421, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC23(%rip), %rdi
	call	OPENSSL_die@PLT
.L185:
	movl	$416, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC22(%rip), %rdi
	call	OPENSSL_die@PLT
.L184:
	movl	$411, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC21(%rip), %rdi
	call	OPENSSL_die@PLT
.L181:
	movl	$386, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC17(%rip), %rdi
	call	OPENSSL_die@PLT
	.cfi_endproc
.LFE170:
	.size	CRYPTO_secure_malloc_init, .-CRYPTO_secure_malloc_init
	.p2align 4
	.globl	CRYPTO_secure_malloc_done
	.type	CRYPTO_secure_malloc_done, @function
CRYPTO_secure_malloc_done:
.LFB171:
	.cfi_startproc
	endbr64
	cmpq	$0, secure_mem_used(%rip)
	je	.L205
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L205:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	32+sh(%rip), %rdi
	movl	$502, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	CRYPTO_free@PLT
	movq	56+sh(%rip), %rdi
	movl	$503, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	64+sh(%rip), %rdi
	movl	$504, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	sh(%rip), %rdi
	testq	%rdi, %rdi
	je	.L192
	movq	8+sh(%rip), %rsi
	testq	%rsi, %rsi
	jne	.L206
.L192:
	movq	sec_malloc_lock(%rip), %rdi
	pxor	%xmm0, %xmm0
	movl	$0, secure_mem_initialized(%rip)
	movaps	%xmm0, sh(%rip)
	movaps	%xmm0, 16+sh(%rip)
	movaps	%xmm0, 32+sh(%rip)
	movaps	%xmm0, 48+sh(%rip)
	movaps	%xmm0, 64+sh(%rip)
	call	CRYPTO_THREAD_lock_free@PLT
	movl	$1, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	$0, sec_malloc_lock(%rip)
	ret
	.p2align 4,,10
	.p2align 3
.L206:
	.cfi_restore_state
	call	munmap@PLT
	jmp	.L192
	.cfi_endproc
.LFE171:
	.size	CRYPTO_secure_malloc_done, .-CRYPTO_secure_malloc_done
	.p2align 4
	.globl	CRYPTO_secure_malloc_initialized
	.type	CRYPTO_secure_malloc_initialized, @function
CRYPTO_secure_malloc_initialized:
.LFB172:
	.cfi_startproc
	endbr64
	movl	secure_mem_initialized(%rip), %eax
	ret
	.cfi_endproc
.LFE172:
	.size	CRYPTO_secure_malloc_initialized, .-CRYPTO_secure_malloc_initialized
	.section	.rodata.str1.8
	.align 8
.LC24:
	.string	"assertion failed: !sh_testbit(temp, slist, sh.bitmalloc)"
	.align 8
.LC25:
	.string	"assertion failed: temp != sh.freelist[slist]"
	.align 8
.LC26:
	.string	"assertion failed: sh.freelist[slist] == temp"
	.align 8
.LC27:
	.string	"assertion failed: temp-(sh.arena_size >> slist) == sh_find_my_buddy(temp, slist)"
	.align 8
.LC28:
	.string	"assertion failed: sh_testbit(chunk, list, sh.bittable)"
	.align 8
.LC29:
	.string	"assertion failed: WITHIN_ARENA(chunk)"
	.text
	.p2align 4
	.globl	CRYPTO_secure_malloc
	.type	CRYPTO_secure_malloc, @function
CRYPTO_secure_malloc:
.LFB173:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movl	secure_mem_initialized(%rip), %eax
	testl	%eax, %eax
	je	.L258
	movq	sec_malloc_lock(%rip), %rdi
	call	CRYPTO_THREAD_write_lock@PLT
	cmpq	24+sh(%rip), %r12
	ja	.L235
	movq	40+sh(%rip), %rax
	leaq	-1(%rax), %rbx
	movq	48+sh(%rip), %rax
	cmpq	%rax, %r12
	jbe	.L211
	.p2align 4,,10
	.p2align 3
.L212:
	addq	%rax, %rax
	subq	$1, %rbx
	cmpq	%rax, %r12
	ja	.L212
.L211:
	testq	%rbx, %rbx
	js	.L235
	movq	32+sh(%rip), %rdi
	movq	%rbx, %r14
	.p2align 4,,10
	.p2align 3
.L214:
	movq	(%rdi,%r14,8), %r15
	testq	%r15, %r15
	jne	.L213
	subq	$1, %r14
	cmpq	$-1, %r14
	jne	.L214
.L257:
	xorl	%eax, %eax
.L210:
	movq	sec_malloc_lock(%rip), %rdi
	addq	%rax, secure_mem_used(%rip)
	call	CRYPTO_THREAD_unlock@PLT
	addq	$24, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L266:
	.cfi_restore_state
	movq	64+sh(%rip), %rcx
	movzbl	(%rcx,%rax), %eax
	btq	%rdx, %rax
	jc	.L226
	movq	$-1, %rax
	movl	%r14d, %ecx
	salq	%cl, %rax
	notq	%rax
	andq	%r13, %rax
	imulq	%r11, %rax
	addq	%rsi, %rax
	cmpq	%rax, %r10
	jne	.L226
.L213:
	cmpq	%r14, %rbx
	je	.L259
	movq	64+sh(%rip), %rdx
	movl	%r14d, %esi
	movq	%r15, %rdi
	leaq	0(,%r14,8), %r13
	call	sh_testbit
	testl	%eax, %eax
	jne	.L260
	movq	56+sh(%rip), %rdx
	movl	%r14d, %esi
	movq	%r15, %rdi
	call	sh_clearbit
	movq	(%r15), %rax
	testq	%rax, %rax
	je	.L216
	movq	8(%r15), %rdx
	movq	%rdx, 8(%rax)
.L216:
	movq	8(%r15), %rdx
	movq	32+sh(%rip), %rcx
	movq	%rax, (%rdx)
	movq	(%r15), %rax
	testq	%rax, %rax
	je	.L217
	movq	8(%rax), %rdx
	cmpq	%rcx, %rdx
	jb	.L218
	movq	40+sh(%rip), %rax
	leaq	(%rcx,%rax,8), %rax
	cmpq	%rax, %rdx
	jb	.L217
.L218:
	movq	16+sh(%rip), %rax
	cmpq	%rax, %rdx
	jb	.L220
	addq	24+sh(%rip), %rax
	cmpq	%rax, %rdx
	jnb	.L220
.L217:
	cmpq	%r15, (%rcx,%r13)
	je	.L261
	addq	$1, %r14
	movq	64+sh(%rip), %rdx
	movq	%r15, %rdi
	movl	%r14d, %esi
	movl	%r14d, -60(%rbp)
	call	sh_testbit
	testl	%eax, %eax
	jne	.L262
	movq	56+sh(%rip), %rdx
	movl	%r14d, %esi
	movq	%r15, %rdi
	call	sh_setbit
	movq	32+sh(%rip), %rdi
	leaq	8(%r13), %rax
	movq	%r15, %rsi
	movq	%rax, -56(%rbp)
	addq	%rax, %rdi
	call	sh_add_to_list
	movq	32+sh(%rip), %rax
	cmpq	%r15, 8(%rax,%r13)
	jne	.L263
	movq	24+sh(%rip), %rax
	movzbl	-60(%rbp), %ecx
	movl	%r14d, %esi
	movq	64+sh(%rip), %rdx
	shrq	%cl, %rax
	leaq	(%r15,%rax), %r12
	movq	%r12, %rdi
	call	sh_testbit
	testl	%eax, %eax
	jne	.L264
	movq	56+sh(%rip), %rdx
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	sh_setbit
	movq	-56(%rbp), %rdi
	movq	%r12, %rsi
	addq	32+sh(%rip), %rdi
	call	sh_add_to_list
	movq	32+sh(%rip), %rdi
	movq	8(%rdi,%r13), %r15
	cmpq	%r15, %r12
	jne	.L265
	movq	24+sh(%rip), %r11
	movl	%r14d, %ecx
	movq	%r15, %rax
	xorl	%edx, %edx
	movq	16+sh(%rip), %rsi
	movq	%r15, %r10
	shrq	%cl, %r11
	subq	%rsi, %rax
	subq	%r11, %r10
	divq	%r11
	movl	$1, %edx
	salq	%cl, %rdx
	movq	56+sh(%rip), %rcx
	addq	%rdx, %rax
	movq	%rax, %r13
	shrq	$3, %rax
	xorq	$1, %r13
	movzbl	(%rcx,%rax), %ecx
	movl	%r13d, %edx
	andl	$7, %edx
	btq	%rdx, %rcx
	jc	.L266
.L226:
	movl	$577, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC27(%rip), %rdi
	call	OPENSSL_die@PLT
	.p2align 4,,10
	.p2align 3
.L259:
	movq	(%rdi,%rbx,8), %r15
	movq	56+sh(%rip), %rdx
	movl	%ebx, %esi
	movq	%r15, %rdi
	call	sh_testbit
	testl	%eax, %eax
	je	.L267
	movq	64+sh(%rip), %rdx
	movl	%ebx, %esi
	movq	%r15, %rdi
	call	sh_setbit
	movq	(%r15), %rax
	testq	%rax, %rax
	je	.L229
	movq	8(%r15), %rdx
	movq	%rdx, 8(%rax)
.L229:
	movq	8(%r15), %rdx
	movq	%rax, (%rdx)
	movq	(%r15), %rdx
	movq	16+sh(%rip), %rax
	testq	%rdx, %rdx
	je	.L230
	movq	8(%rdx), %rdx
	movq	32+sh(%rip), %rcx
	cmpq	%rcx, %rdx
	jnb	.L268
.L231:
	cmpq	%rax, %rdx
	jb	.L220
	movq	24+sh(%rip), %rcx
	addq	%rax, %rcx
	cmpq	%rcx, %rdx
	jnb	.L220
.L230:
	cmpq	%rax, %r15
	jb	.L232
	addq	24+sh(%rip), %rax
	cmpq	%rax, %r15
	jnb	.L232
	pxor	%xmm0, %xmm0
	movq	%r15, %rdi
	movups	%xmm0, (%r15)
	call	sh_actual_size
	jmp	.L210
	.p2align 4,,10
	.p2align 3
.L258:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_malloc@PLT
	.p2align 4,,10
	.p2align 3
.L268:
	.cfi_restore_state
	movq	40+sh(%rip), %rsi
	leaq	(%rcx,%rsi,8), %rcx
	cmpq	%rcx, %rdx
	jb	.L230
	jmp	.L231
	.p2align 4,,10
	.p2align 3
.L235:
	xorl	%r15d, %r15d
	jmp	.L257
.L220:
	movl	$372, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC15(%rip), %rdi
	call	OPENSSL_die@PLT
.L260:
	movl	$556, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC24(%rip), %rdi
	call	OPENSSL_die@PLT
.L261:
	movl	$559, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC25(%rip), %rdi
	call	OPENSSL_die@PLT
.L262:
	movl	$565, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC24(%rip), %rdi
	call	OPENSSL_die@PLT
.L265:
	movl	$575, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC26(%rip), %rdi
	call	OPENSSL_die@PLT
.L263:
	movl	$568, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC26(%rip), %rdi
	call	OPENSSL_die@PLT
.L264:
	movl	$572, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC24(%rip), %rdi
	call	OPENSSL_die@PLT
.L232:
	movl	$586, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC29(%rip), %rdi
	call	OPENSSL_die@PLT
.L267:
	movl	$582, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC28(%rip), %rdi
	call	OPENSSL_die@PLT
	.cfi_endproc
.LFE173:
	.size	CRYPTO_secure_malloc, .-CRYPTO_secure_malloc
	.p2align 4
	.globl	CRYPTO_secure_zalloc
	.type	CRYPTO_secure_zalloc, @function
CRYPTO_secure_zalloc:
.LFB174:
	.cfi_startproc
	endbr64
	movl	secure_mem_initialized(%rip), %eax
	testl	%eax, %eax
	jne	.L271
	jmp	CRYPTO_zalloc@PLT
	.p2align 4,,10
	.p2align 3
.L271:
	jmp	CRYPTO_secure_malloc
	.cfi_endproc
.LFE174:
	.size	CRYPTO_secure_zalloc, .-CRYPTO_secure_zalloc
	.p2align 4
	.globl	CRYPTO_secure_free
	.type	CRYPTO_secure_free, @function
CRYPTO_secure_free:
.LFB175:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L272
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$8, %rsp
	movl	secure_mem_initialized(%rip), %eax
	testl	%eax, %eax
	jne	.L280
.L274:
	addq	$8, %rsp
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_free@PLT
	.p2align 4,,10
	.p2align 3
.L280:
	.cfi_restore_state
	movq	sec_malloc_lock(%rip), %rdi
	call	CRYPTO_THREAD_write_lock@PLT
	movq	16+sh(%rip), %rax
	movq	sec_malloc_lock(%rip), %rdi
	cmpq	%rax, %r12
	jb	.L276
	addq	24+sh(%rip), %rax
	cmpq	%rax, %r12
	jnb	.L276
	call	CRYPTO_THREAD_unlock@PLT
	movq	sec_malloc_lock(%rip), %rdi
	call	CRYPTO_THREAD_write_lock@PLT
	movq	%r12, %rdi
	call	sh_actual_size
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %r13
	call	OPENSSL_cleanse@PLT
	subq	%r13, secure_mem_used(%rip)
	movq	%r12, %rdi
	call	sh_free
	movq	sec_malloc_lock(%rip), %rdi
	addq	$8, %rsp
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_THREAD_unlock@PLT
	.p2align 4,,10
	.p2align 3
.L276:
	.cfi_restore_state
	call	CRYPTO_THREAD_unlock@PLT
	jmp	.L274
	.p2align 4,,10
	.p2align 3
.L272:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	ret
	.cfi_endproc
.LFE175:
	.size	CRYPTO_secure_free, .-CRYPTO_secure_free
	.p2align 4
	.globl	CRYPTO_secure_clear_free
	.type	CRYPTO_secure_clear_free, @function
CRYPTO_secure_clear_free:
.LFB176:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L281
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	secure_mem_initialized(%rip), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%ecx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	testl	%eax, %eax
	jne	.L289
.L283:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	OPENSSL_cleanse@PLT
	movl	%r15d, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_free@PLT
	.p2align 4,,10
	.p2align 3
.L289:
	.cfi_restore_state
	movq	sec_malloc_lock(%rip), %rdi
	call	CRYPTO_THREAD_write_lock@PLT
	movq	16+sh(%rip), %rax
	movq	sec_malloc_lock(%rip), %rdi
	cmpq	%rax, %r12
	jb	.L285
	addq	24+sh(%rip), %rax
	cmpq	%rax, %r12
	jnb	.L285
	call	CRYPTO_THREAD_unlock@PLT
	movq	sec_malloc_lock(%rip), %rdi
	call	CRYPTO_THREAD_write_lock@PLT
	movq	%r12, %rdi
	call	sh_actual_size
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %r13
	call	OPENSSL_cleanse@PLT
	subq	%r13, secure_mem_used(%rip)
	movq	%r12, %rdi
	call	sh_free
	movq	sec_malloc_lock(%rip), %rdi
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_THREAD_unlock@PLT
	.p2align 4,,10
	.p2align 3
.L285:
	.cfi_restore_state
	call	CRYPTO_THREAD_unlock@PLT
	jmp	.L283
	.p2align 4,,10
	.p2align 3
.L281:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.cfi_endproc
.LFE176:
	.size	CRYPTO_secure_clear_free, .-CRYPTO_secure_clear_free
	.p2align 4
	.globl	CRYPTO_secure_allocated
	.type	CRYPTO_secure_allocated, @function
CRYPTO_secure_allocated:
.LFB177:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	secure_mem_initialized(%rip), %r12d
	pushq	%rbx
	.cfi_offset 3, -32
	testl	%r12d, %r12d
	jne	.L298
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L298:
	.cfi_restore_state
	movq	%rdi, %rbx
	movq	sec_malloc_lock(%rip), %rdi
	xorl	%r12d, %r12d
	call	CRYPTO_THREAD_write_lock@PLT
	movq	16+sh(%rip), %rax
	cmpq	%rax, %rbx
	jb	.L292
	addq	24+sh(%rip), %rax
	xorl	%r12d, %r12d
	cmpq	%rax, %rbx
	setb	%r12b
.L292:
	movq	sec_malloc_lock(%rip), %rdi
	call	CRYPTO_THREAD_unlock@PLT
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE177:
	.size	CRYPTO_secure_allocated, .-CRYPTO_secure_allocated
	.p2align 4
	.globl	CRYPTO_secure_used
	.type	CRYPTO_secure_used, @function
CRYPTO_secure_used:
.LFB178:
	.cfi_startproc
	endbr64
	movq	secure_mem_used(%rip), %rax
	ret
	.cfi_endproc
.LFE178:
	.size	CRYPTO_secure_used, .-CRYPTO_secure_used
	.p2align 4
	.globl	CRYPTO_secure_actual_size
	.type	CRYPTO_secure_actual_size, @function
CRYPTO_secure_actual_size:
.LFB179:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	sec_malloc_lock(%rip), %rdi
	call	CRYPTO_THREAD_write_lock@PLT
	movq	%r12, %rdi
	call	sh_actual_size
	movq	sec_malloc_lock(%rip), %rdi
	movq	%rax, %r12
	call	CRYPTO_THREAD_unlock@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE179:
	.size	CRYPTO_secure_actual_size, .-CRYPTO_secure_actual_size
	.local	sh
	.comm	sh,80,32
	.local	sec_malloc_lock
	.comm	sec_malloc_lock,8,8
	.local	secure_mem_initialized
	.comm	secure_mem_initialized,4,4
	.local	secure_mem_used
	.comm	secure_mem_used,8,8
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
