	.file	"rsa_pmeth.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/rsa/rsa_pmeth.c"
	.text
	.p2align 4
	.type	pkey_rsa_cleanup, @function
pkey_rsa_cleanup:
.LFB1505:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	40(%rdi), %r12
	testq	%r12, %r12
	je	.L1
	movq	8(%r12), %rdi
	call	BN_free@PLT
	movq	56(%r12), %rdi
	movl	$118, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	64(%r12), %rdi
	movl	$119, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$120, %edx
	popq	%r12
	leaq	.LC0(%rip), %rsi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_free@PLT
	.p2align 4,,10
	.p2align 3
.L1:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1505:
	.size	pkey_rsa_cleanup, .-pkey_rsa_cleanup
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"rsa_padding_mode"
.LC2:
	.string	"pkcs1"
.LC3:
	.string	"sslv23"
.LC4:
	.string	"none"
.LC5:
	.string	"oeap"
.LC6:
	.string	"oaep"
.LC7:
	.string	"x931"
.LC8:
	.string	"pss"
.LC9:
	.string	"rsa_pss_saltlen"
.LC10:
	.string	"digest"
.LC11:
	.string	"max"
.LC12:
	.string	"auto"
.LC13:
	.string	"rsa_keygen_bits"
.LC14:
	.string	"rsa_keygen_pubexp"
.LC15:
	.string	"rsa_keygen_primes"
.LC16:
	.string	"rsa_mgf1_md"
.LC17:
	.string	"rsa_pss_keygen_mgf1_md"
.LC18:
	.string	"rsa_pss_keygen_md"
.LC19:
	.string	"rsa_pss_keygen_saltlen"
.LC20:
	.string	"rsa_oaep_md"
.LC21:
	.string	"rsa_oaep_label"
	.text
	.p2align 4
	.type	pkey_rsa_ctrl_str, @function
pkey_rsa_ctrl_str:
.LFB1513:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	subq	$24, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%fs:40, %rcx
	movq	%rcx, -40(%rbp)
	xorl	%ecx, %ecx
	testq	%rdx, %rdx
	je	.L44
	movq	%rdi, %r13
	movl	$17, %ecx
	movq	%rsi, %rax
	movq	%rdx, %r8
	leaq	.LC1(%rip), %rdi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	jne	.L8
	movl	$6, %ecx
	leaq	.LC2(%rip), %rdi
	movq	%r8, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L23
	movl	$7, %ecx
	leaq	.LC3(%rip), %rdi
	movq	%r8, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L24
	movl	$5, %ecx
	leaq	.LC4(%rip), %rdi
	movq	%r8, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L25
	movl	$5, %ecx
	leaq	.LC5(%rip), %rdi
	movq	%r8, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L27
	movl	$5, %ecx
	leaq	.LC6(%rip), %rdi
	movq	%r8, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L27
	movl	$5, %ecx
	leaq	.LC7(%rip), %rdi
	movq	%r8, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L28
	movl	$4, %ecx
	leaq	.LC8(%rip), %rdi
	movq	%r8, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L45
	movl	$6, %ecx
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L8:
	movl	$16, %ecx
	leaq	.LC9(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L46
	movl	$16, %ecx
	leaq	.LC13(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L47
	movl	$18, %ecx
	leaq	.LC14(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L48
	movl	$18, %ecx
	leaq	.LC15(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L49
	movl	$12, %ecx
	leaq	.LC16(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L50
	movq	0(%r13), %rdx
	cmpl	$912, (%rdx)
	je	.L51
.L17:
	movl	$12, %ecx
	leaq	.LC20(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L52
	movq	%rax, %rsi
	movl	$15, %ecx
	leaq	.LC21(%rip), %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	movsbl	%al, %r12d
	testl	%r12d, %r12d
	jne	.L33
	leaq	-48(%rbp), %rsi
	movq	%r8, %rdi
	call	OPENSSL_hexstr2buf@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L5
	movl	-48(%rbp), %r8d
	movq	%rax, %r9
	movl	$4106, %ecx
	movq	%r13, %rdi
	movl	$768, %edx
	movl	$6, %esi
	call	EVP_PKEY_CTX_ctrl@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	jg	.L5
	movl	$694, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r14, %rdi
	call	CRYPTO_free@PLT
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L25:
	movl	$3, %ecx
	.p2align 4,,10
	.p2align 3
.L9:
	xorl	%r8d, %r8d
	movl	$4097, %edx
	movl	$-1, %esi
	movq	%r13, %rdi
	call	RSA_pkey_ctx_ctrl@PLT
	movl	%eax, %r12d
.L5:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L53
	addq	$24, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L48:
	.cfi_restore_state
	leaq	-48(%rbp), %rdi
	movq	%r8, %rsi
	movq	$0, -48(%rbp)
	call	BN_asc2bn@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	je	.L5
	movq	-48(%rbp), %r8
	xorl	%ecx, %ecx
	movl	$4100, %edx
	movl	$4, %esi
	movq	%r13, %rdi
	call	RSA_pkey_ctx_ctrl@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	jg	.L5
	movq	-48(%rbp), %rdi
	call	BN_free@PLT
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L46:
	movl	$7, %ecx
	leaq	.LC10(%rip), %rdi
	movq	%r8, %rsi
	repz cmpsb
	movl	$-1, %ecx
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L11
	movl	$4, %ecx
	leaq	.LC11(%rip), %rdi
	movq	%r8, %rsi
	repz cmpsb
	movl	$-3, %ecx
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L11
	movl	$5, %ecx
	leaq	.LC12(%rip), %rdi
	movq	%r8, %rsi
	repz cmpsb
	movl	$-2, %ecx
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L11
	movl	$10, %edx
	xorl	%esi, %esi
	movq	%r8, %rdi
	call	strtol@PLT
	movl	%eax, %ecx
	.p2align 4,,10
	.p2align 3
.L11:
	xorl	%r8d, %r8d
	movl	$4098, %edx
	movl	$24, %esi
	movq	%r13, %rdi
	call	RSA_pkey_ctx_ctrl@PLT
	movl	%eax, %r12d
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L24:
	movl	$2, %ecx
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L44:
	movl	$593, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$147, %edx
	xorl	%r12d, %r12d
	movl	$144, %esi
	movl	$4, %edi
	call	ERR_put_error@PLT
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L23:
	movl	$1, %ecx
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L27:
	movl	$4, %ecx
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L47:
	movq	%r8, %rdi
	movl	$10, %edx
	xorl	%esi, %esi
	call	strtol@PLT
	xorl	%r8d, %r8d
	movl	$4099, %edx
	movq	%r13, %rdi
	movq	%rax, %rcx
	movl	$4, %esi
	call	RSA_pkey_ctx_ctrl@PLT
	movl	%eax, %r12d
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L28:
	movl	$5, %ecx
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L51:
	movl	$23, %ecx
	leaq	.LC17(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L54
	movl	$18, %ecx
	leaq	.LC18(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L55
	movl	$23, %ecx
	leaq	.LC19(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	jne	.L17
	movq	%r8, %rdi
	movl	$10, %edx
	xorl	%esi, %esi
	call	strtol@PLT
	xorl	%r9d, %r9d
	movl	$4098, %ecx
	movq	%r13, %rdi
	movl	%eax, %r8d
	movl	$4, %edx
	movl	$912, %esi
	call	EVP_PKEY_CTX_ctrl@PLT
	movl	%eax, %r12d
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L49:
	movq	%r8, %rdi
	movl	$10, %edx
	xorl	%esi, %esi
	call	strtol@PLT
	xorl	%r8d, %r8d
	movl	$4109, %edx
	movq	%r13, %rdi
	movq	%rax, %rcx
	movl	$4, %esi
	call	RSA_pkey_ctx_ctrl@PLT
	movl	%eax, %r12d
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L50:
	movq	%r8, %rcx
	movl	$4101, %edx
	movl	$1016, %esi
	movq	%r13, %rdi
	call	EVP_PKEY_CTX_md@PLT
	movl	%eax, %r12d
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L52:
	movq	%r8, %rcx
	movl	$4105, %edx
	movl	$768, %esi
	movq	%r13, %rdi
	call	EVP_PKEY_CTX_md@PLT
	movl	%eax, %r12d
	jmp	.L5
.L54:
	movq	%r8, %rcx
	movl	$4101, %edx
	movl	$4, %esi
	movq	%r13, %rdi
	call	EVP_PKEY_CTX_md@PLT
	movl	%eax, %r12d
	jmp	.L5
.L55:
	movq	%r8, %rcx
	movl	$1, %edx
	movl	$4, %esi
	movq	%r13, %rdi
	call	EVP_PKEY_CTX_md@PLT
	movl	%eax, %r12d
	jmp	.L5
.L33:
	movl	$-2, %r12d
	jmp	.L5
.L45:
	movl	$614, %r8d
	movl	$118, %edx
	movl	$144, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	movl	$-2, %r12d
	call	ERR_put_error@PLT
	jmp	.L5
.L53:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1513:
	.size	pkey_rsa_ctrl_str, .-pkey_rsa_ctrl_str
	.p2align 4
	.type	pkey_rsa_init, @function
pkey_rsa_init:
.LFB1502:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$55, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$80, %edi
	subq	$8, %rsp
	call	CRYPTO_zalloc@PLT
	testq	%rax, %rax
	je	.L59
	movq	(%rbx), %rdx
	movl	$2048, (%rax)
	movl	$2, 16(%rax)
	cmpl	$912, (%rdx)
	movq	$-2, 48(%rax)
	sete	%dl
	movzbl	%dl, %edx
	leal	1(%rdx,%rdx,4), %edx
	movl	%edx, 28(%rax)
	movq	%rax, 40(%rbx)
	addq	$20, %rax
	movq	%rax, 64(%rbx)
	movl	$1, %eax
	movl	$2, 72(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L59:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1502:
	.size	pkey_rsa_init, .-pkey_rsa_init
	.p2align 4
	.type	pkey_pss_init, @function
pkey_pss_init:
.LFB1516:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	cmpl	$912, (%rax)
	jne	.L75
	movq	16(%rdi), %rax
	movl	$1, %r12d
	movq	40(%rax), %r14
	movq	96(%r14), %r8
	testq	%r8, %r8
	je	.L62
	movq	40(%rdi), %r13
	leaq	-60(%rbp), %rcx
	leaq	-48(%rbp), %rdx
	movq	%r8, %rdi
	leaq	-56(%rbp), %rsi
	call	rsa_pss_get_param@PLT
	testl	%eax, %eax
	jne	.L76
.L75:
	xorl	%r12d, %r12d
.L62:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L77
	addq	$32, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L76:
	.cfi_restore_state
	movq	%r14, %rdi
	call	RSA_size@PLT
	movq	-56(%rbp), %rdi
	movl	%eax, %ebx
	call	EVP_MD_size@PLT
	movq	%r14, %rdi
	subl	%eax, %ebx
	call	RSA_bits@PLT
	andl	$7, %eax
	cmpl	$1, %eax
	sete	%al
	movzbl	%al, %eax
	subl	%eax, %ebx
	movl	-60(%rbp), %eax
	cmpl	%ebx, %eax
	jg	.L78
	movq	-56(%rbp), %xmm0
	movl	%eax, 52(%r13)
	movl	%eax, 48(%r13)
	movhps	-48(%rbp), %xmm0
	movups	%xmm0, 32(%r13)
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L78:
	movl	$821, %r8d
	movl	$150, %edx
	movl	$165, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L75
.L77:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1516:
	.size	pkey_pss_init, .-pkey_pss_init
	.p2align 4
	.type	check_padding_md.part.0, @function
check_padding_md.part.0:
.LFB1517:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movl	%esi, %ebx
	subq	$8, %rsp
	call	EVP_MD_type@PLT
	cmpl	$3, %ebx
	je	.L96
	movl	%eax, %edi
	cmpl	$5, %ebx
	je	.L97
	cmpl	$117, %eax
	jg	.L83
	cmpl	$63, %eax
	jg	.L84
	subl	$3, %edi
	cmpl	$1, %edi
	ja	.L85
.L88:
	movl	$1, %eax
.L79:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L83:
	.cfi_restore_state
	cmpl	$675, %eax
	jg	.L86
	movl	$1, %eax
	cmpl	$671, %edi
	jg	.L79
	cmpl	$257, %edi
	jne	.L85
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L84:
	.cfi_restore_state
	movabsq	$10133101309067265, %rax
	subl	$64, %edi
	btq	%rdi, %rax
	jc	.L88
.L85:
	movl	$390, %r8d
	movl	$157, %edx
	movl	$140, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L86:
	.cfi_restore_state
	subl	$1096, %edi
	movl	$1, %eax
	cmpl	$3, %edi
	ja	.L85
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L97:
	.cfi_restore_state
	call	RSA_X931_hash_id@PLT
	movl	%eax, %r8d
	movl	$1, %eax
	cmpl	$-1, %r8d
	jne	.L79
	movl	$366, %r8d
	movl	$142, %edx
	movl	$140, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L96:
	movl	$360, %r8d
	movl	$141, %edx
	movl	$140, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1517:
	.size	check_padding_md.part.0, .-check_padding_md.part.0
	.p2align 4
	.type	pkey_rsa_ctrl, @function
pkey_rsa_ctrl:
.LFB1512:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$8, %rsp
	movq	40(%rdi), %r14
	cmpl	$13, %esi
	jle	.L190
	leal	-4097(%rsi), %eax
	cmpl	$12, %eax
	ja	.L156
	movslq	%edx, %r15
	leaq	.L103(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L103:
	.long	.L112-.L103
	.long	.L108-.L103
	.long	.L111-.L103
	.long	.L110-.L103
	.long	.L107-.L103
	.long	.L109-.L103
	.long	.L108-.L103
	.long	.L107-.L103
	.long	.L105-.L103
	.long	.L106-.L103
	.long	.L105-.L103
	.long	.L104-.L103
	.long	.L102-.L103
	.text
	.p2align 4,,10
	.p2align 3
.L190:
	testl	%esi, %esi
	jg	.L100
.L156:
	movl	$-2, %eax
.L98:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L100:
	.cfi_restore_state
	cmpl	$13, %esi
	ja	.L156
	leaq	.L115(%rip), %rcx
	movl	%esi, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L115:
	.long	.L156-.L115
	.long	.L118-.L115
	.long	.L117-.L115
	.long	.L116-.L115
	.long	.L116-.L115
	.long	.L187-.L115
	.long	.L156-.L115
	.long	.L187-.L115
	.long	.L156-.L115
	.long	.L116-.L115
	.long	.L116-.L115
	.long	.L187-.L115
	.long	.L156-.L115
	.long	.L146-.L115
	.text
.L105:
	cmpl	$4, 28(%r14)
	jne	.L191
	cmpl	$4107, %ebx
	jne	.L137
.L146:
	movq	32(%r14), %rax
.L186:
	movq	%rax, 0(%r13)
.L187:
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L116:
	.cfi_restore_state
	movq	(%rdi), %rax
	cmpl	$912, (%rax)
	jne	.L187
.L117:
	movl	$579, %r8d
	movl	$148, %edx
	movl	$143, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-2, %eax
	jmp	.L98
.L112:
	leal	-1(%r15), %eax
	cmpl	$5, %eax
	ja	.L120
	movq	32(%r14), %rdi
	testq	%rdi, %rdi
	je	.L124
	movl	%r15d, %esi
	call	check_padding_md.part.0
	testl	%eax, %eax
	je	.L183
.L124:
	cmpl	$6, %r15d
	je	.L192
	movq	(%r12), %rax
	cmpl	$912, (%rax)
	je	.L120
	cmpl	$4, %r15d
	jne	.L152
	testl	$768, 32(%r12)
	je	.L120
.L189:
	cmpq	$0, 32(%r14)
	je	.L193
.L152:
	movl	%r15d, 28(%r14)
	movl	$1, %eax
	jmp	.L98
.L108:
	cmpl	$6, 28(%r14)
	jne	.L194
	cmpl	$4103, %ebx
	je	.L195
	cmpl	$-3, %r15d
	jl	.L156
	movl	52(%r14), %ebx
	cmpl	$-1, %ebx
	je	.L128
	cmpl	$-2, %r15d
	je	.L196
	cmpl	$-1, %r15d
	je	.L197
	testl	%r15d, %r15d
	js	.L128
	cmpl	%r15d, %ebx
	jg	.L131
.L128:
	movl	%r15d, 48(%r14)
	movl	$1, %eax
	jmp	.L98
.L111:
	cmpl	$511, %r15d
	jle	.L198
	movl	%r15d, (%r14)
	movl	$1, %eax
	jmp	.L98
.L110:
	testq	%r13, %r13
	je	.L134
	movq	%r13, %rdi
	call	BN_is_odd@PLT
	testl	%eax, %eax
	je	.L134
	movq	%r13, %rdi
	call	BN_is_one@PLT
	testl	%eax, %eax
	jne	.L134
	movq	8(%r14), %rdi
	call	BN_free@PLT
	movq	%r13, 8(%r14)
	movl	$1, %eax
	jmp	.L98
.L102:
	leal	-2(%r15), %eax
	cmpl	$3, %eax
	ja	.L199
	movl	%r15d, 16(%r14)
	movl	$1, %eax
	jmp	.L98
.L104:
	cmpl	$4, 28(%r14)
	jne	.L200
	movq	64(%r14), %rax
	movq	%rax, 0(%r13)
	movl	72(%r14), %eax
	jmp	.L98
.L118:
	testq	%r13, %r13
	je	.L142
	movl	28(%r14), %esi
	movq	%r13, %rdi
	call	check_padding_md.part.0
	testl	%eax, %eax
	je	.L183
.L142:
	cmpl	$-1, 52(%r14)
	je	.L180
	movq	32(%r14), %rdi
	call	EVP_MD_type@PLT
	movq	%r13, %rdi
	movl	%eax, %ebx
	call	EVP_MD_type@PLT
	cmpl	%eax, %ebx
	je	.L187
	movl	$506, %r8d
	movl	$145, %edx
	movl	$143, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L98
.L107:
	movl	28(%r14), %eax
	andl	$-3, %eax
	cmpl	$4, %eax
	jne	.L201
	cmpl	$4104, %ebx
	je	.L202
	cmpl	$-1, 52(%r14)
	je	.L147
	movq	40(%r14), %rdi
	call	EVP_MD_type@PLT
	movq	%r13, %rdi
	movl	%eax, %ebx
	call	EVP_MD_type@PLT
	cmpl	%eax, %ebx
	je	.L187
	movl	$532, %r8d
	movl	$152, %edx
	movl	$143, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L98
.L109:
	movl	28(%r14), %eax
	movl	%eax, 0(%r13)
	movl	$1, %eax
	jmp	.L98
.L106:
	cmpl	$4, 28(%r14)
	jne	.L203
	movq	64(%r14), %rdi
	movl	$544, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	testq	%r13, %r13
	je	.L150
	testl	%r15d, %r15d
	jle	.L150
	movq	%r13, 64(%r14)
	movl	$1, %eax
	movq	%r15, 72(%r14)
	jmp	.L98
	.p2align 4,,10
	.p2align 3
.L137:
	movq	%r13, 32(%r14)
	movl	$1, %eax
	jmp	.L98
	.p2align 4,,10
	.p2align 3
.L202:
	movq	40(%r14), %rax
	testq	%rax, %rax
	jne	.L186
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L195:
	movl	48(%r14), %eax
	movl	%eax, 0(%r13)
	movl	$1, %eax
	jmp	.L98
	.p2align 4,,10
	.p2align 3
.L150:
	movq	$0, 64(%r14)
	movl	$1, %eax
	movq	$0, 72(%r14)
	jmp	.L98
.L197:
	movq	32(%r14), %rdi
	call	EVP_MD_size@PLT
	cmpl	%eax, %ebx
	jle	.L128
.L131:
	movl	$455, %r8d
	movl	$164, %edx
	movl	$143, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	.p2align 4,,10
	.p2align 3
.L183:
	xorl	%eax, %eax
	jmp	.L98
	.p2align 4,,10
	.p2align 3
.L180:
	movq	%r13, 32(%r14)
	movl	%ebx, %eax
	jmp	.L98
	.p2align 4,,10
	.p2align 3
.L147:
	movq	%r13, 40(%r14)
	movl	$1, %eax
	jmp	.L98
	.p2align 4,,10
	.p2align 3
.L192:
	testb	$24, 32(%r12)
	jne	.L189
.L120:
	movl	$427, %r8d
	movl	$144, %edx
	movl	$143, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-2, %eax
	jmp	.L98
.L196:
	cmpl	$16, 32(%r12)
	jne	.L128
	movl	$449, %r8d
.L184:
	movl	$146, %edx
	movl	$143, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-2, %eax
	jmp	.L98
	.p2align 4,,10
	.p2align 3
.L193:
	call	EVP_sha1@PLT
	movq	%rax, 32(%r14)
	jmp	.L152
.L134:
	movl	$473, %r8d
	movl	$101, %edx
	movl	$143, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-2, %eax
	jmp	.L98
.L203:
	movl	$541, %r8d
.L185:
	movl	$141, %edx
	movl	$143, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-2, %eax
	jmp	.L98
.L194:
	movl	$438, %r8d
	jmp	.L184
.L191:
	movl	$491, %r8d
	jmp	.L185
.L200:
	movl	$556, %r8d
	jmp	.L185
.L199:
	movl	$482, %r8d
	movl	$165, %edx
	movl	$143, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-2, %eax
	jmp	.L98
.L201:
	movl	$520, %r8d
	movl	$156, %edx
	movl	$143, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-2, %eax
	jmp	.L98
.L198:
	movl	$465, %r8d
	movl	$120, %edx
	movl	$143, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-2, %eax
	jmp	.L98
	.cfi_endproc
.LFE1512:
	.size	pkey_rsa_ctrl, .-pkey_rsa_ctrl
	.p2align 4
	.type	pkey_rsa_keygen, @function
pkey_rsa_keygen:
.LFB1515:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	40(%rdi), %r13
	cmpq	$0, 8(%r13)
	je	.L205
.L210:
	call	RSA_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L231
	cmpq	$0, 56(%rbx)
	je	.L218
	call	BN_GENCB_new@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L217
	movq	%rbx, %rsi
	movq	%rax, %rdi
	call	evp_pkey_set_cb_translate@PLT
.L211:
	movq	8(%r13), %rcx
	movl	16(%r13), %edx
	movq	%r12, %rdi
	movq	%r15, %r8
	movl	0(%r13), %esi
	call	RSA_generate_multi_prime_key@PLT
	movq	%r15, %rdi
	movl	%eax, %r13d
	call	BN_GENCB_free@PLT
	testl	%r13d, %r13d
	jg	.L232
	movq	%r12, %rdi
	call	RSA_free@PLT
.L204:
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L218:
	.cfi_restore_state
	xorl	%r15d, %r15d
	jmp	.L211
	.p2align 4,,10
	.p2align 3
.L217:
	movq	%r12, %rdi
	call	RSA_free@PLT
.L231:
	xorl	%r13d, %r13d
	jmp	.L204
	.p2align 4,,10
	.p2align 3
.L205:
	call	BN_new@PLT
	movq	%rax, 8(%r13)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L231
	movl	$65537, %esi
	call	BN_set_word@PLT
	testl	%eax, %eax
	jne	.L210
	jmp	.L231
	.p2align 4,,10
	.p2align 3
.L232:
	movq	(%rbx), %rax
	movl	(%rax), %esi
	cmpl	$912, %esi
	jne	.L214
	movq	40(%rbx), %rax
	movq	32(%rax), %rdi
	movq	40(%rax), %r8
	movl	48(%rax), %edx
	testq	%rdi, %rdi
	je	.L233
.L215:
	cmpl	$-2, %edx
	movl	$0, %eax
	cmove	%eax, %edx
.L216:
	movq	%r8, %rsi
	call	rsa_pss_params_create@PLT
	movq	%rax, 96(%r12)
	testq	%rax, %rax
	je	.L217
	movq	(%rbx), %rax
	movl	(%rax), %esi
.L214:
	movq	%r12, %rdx
	movq	%r14, %rdi
	call	EVP_PKEY_assign@PLT
	jmp	.L204
	.p2align 4,,10
	.p2align 3
.L233:
	testq	%r8, %r8
	jne	.L215
	cmpl	$-2, %edx
	jne	.L216
	jmp	.L214
	.cfi_endproc
.LFE1515:
	.size	pkey_rsa_keygen, .-pkey_rsa_keygen
	.p2align 4
	.type	pkey_rsa_copy, @function
pkey_rsa_copy:
.LFB1503:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$55, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	leaq	.LC0(%rip), %rsi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movl	$80, %edi
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	call	CRYPTO_zalloc@PLT
	testq	%rax, %rax
	je	.L240
	movl	$2048, (%rax)
	movq	%rax, %rbx
	movl	$2, 16(%rax)
	movq	(%r12), %rax
	cmpl	$912, (%rax)
	movq	$-2, 48(%rbx)
	sete	%al
	movzbl	%al, %eax
	leal	1(%rax,%rax,4), %eax
	movl	%eax, 28(%rbx)
	leaq	20(%rbx), %rax
	movq	%rax, 64(%r12)
	movq	%rbx, 40(%r12)
	movl	$2, 72(%r12)
	movq	40(%r13), %r12
	movl	(%r12), %eax
	movq	8(%r12), %rdi
	movl	%eax, (%rbx)
	testq	%rdi, %rdi
	je	.L241
	call	BN_dup@PLT
	movq	%rax, 8(%rbx)
	testq	%rax, %rax
	je	.L240
.L241:
	movl	28(%r12), %eax
	cmpq	$0, 64(%r12)
	movl	%eax, 28(%rbx)
	movdqu	32(%r12), %xmm0
	movups	%xmm0, 32(%rbx)
	je	.L250
	movq	64(%rbx), %rdi
	movl	$93, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	72(%r12), %rsi
	movq	64(%r12), %rdi
	movl	$94, %ecx
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_memdup@PLT
	movq	%rax, 64(%rbx)
	testq	%rax, %rax
	je	.L240
	movq	72(%r12), %rax
	movq	%rax, 72(%rbx)
.L250:
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L240:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1503:
	.size	pkey_rsa_copy, .-pkey_rsa_copy
	.p2align 4
	.type	pkey_rsa_encrypt, @function
pkey_rsa_encrypt:
.LFB1509:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$24, %rsp
	movq	40(%rdi), %r13
	movq	16(%rdi), %rax
	movl	28(%r13), %r8d
	movq	40(%rax), %rcx
	cmpl	$4, %r8d
	je	.L258
	movq	%rsi, %rdx
	movl	%r14d, %edi
	movq	%r15, %rsi
	call	RSA_public_encrypt@PLT
.L255:
	testl	%eax, %eax
	js	.L251
	cltq
	movq	%rax, (%rbx)
	movl	$1, %eax
.L251:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L258:
	.cfi_restore_state
	movq	%rdi, %r12
	movq	%rcx, %rdi
	movq	%rsi, -56(%rbp)
	call	RSA_size@PLT
	movq	56(%r13), %rdi
	movq	-56(%rbp), %r10
	movl	%eax, %r11d
	testq	%rdi, %rdi
	je	.L259
.L253:
	movq	72(%r13), %r9
	movq	64(%r13), %r8
	pushq	40(%r13)
	movl	%r14d, %ecx
	pushq	32(%r13)
	movq	%r15, %rdx
	movl	%r11d, %esi
	movq	%r10, -64(%rbp)
	movl	%r11d, -56(%rbp)
	call	RSA_padding_add_PKCS1_OAEP_mgf1@PLT
	popq	%rdx
	popq	%rcx
	testl	%eax, %eax
	je	.L256
	movq	16(%r12), %rax
	movq	-64(%rbp), %r10
	movl	$3, %r8d
	movl	-56(%rbp), %r11d
	movq	56(%r13), %rsi
	movq	40(%rax), %rcx
	movq	%r10, %rdx
	movl	%r11d, %edi
	call	RSA_public_encrypt@PLT
	jmp	.L255
	.p2align 4,,10
	.p2align 3
.L259:
	movq	16(%r12), %rdi
	movq	%r10, -64(%rbp)
	movl	%eax, -56(%rbp)
	call	EVP_PKEY_size@PLT
	movl	$106, %edx
	leaq	.LC0(%rip), %rsi
	movslq	%eax, %rdi
	call	CRYPTO_malloc@PLT
	movl	-56(%rbp), %r11d
	movq	-64(%rbp), %r10
	testq	%rax, %rax
	movq	%rax, 56(%r13)
	movq	%rax, %rdi
	jne	.L253
	movl	$107, %r8d
	movl	$65, %edx
	movl	$167, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-1, %eax
	jmp	.L251
.L256:
	movl	$-1, %eax
	jmp	.L251
	.cfi_endproc
.LFE1509:
	.size	pkey_rsa_encrypt, .-pkey_rsa_encrypt
	.p2align 4
	.type	pkey_rsa_decrypt, @function
pkey_rsa_decrypt:
.LFB1510:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%r8, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$24, %rsp
	movq	40(%rdi), %r14
	movq	16(%rdi), %rdi
	movl	28(%r14), %r8d
	cmpl	$4, %r8d
	je	.L267
	movq	40(%rdi), %rcx
	movq	%rsi, %rdx
	movl	%r9d, %edi
	movq	%r15, %rsi
	call	RSA_private_decrypt@PLT
.L265:
	movslq	%eax, %rsi
	movq	%rsi, %rdx
	sarq	$63, %rdx
	movq	%rdx, %rcx
	notq	%rdx
	andq	(%rbx), %rcx
	andq	%rsi, %rdx
	orq	%rdx, %rcx
	cltd
	movq	%rcx, (%rbx)
	movl	%edx, %r8d
	notl	%edx
	andl	%r8d, %eax
	andl	$1, %edx
	orl	%edx, %eax
.L260:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L267:
	.cfi_restore_state
	movq	56(%r14), %rdx
	testq	%rdx, %rdx
	je	.L268
.L262:
	movq	40(%rdi), %rcx
	movl	$3, %r8d
	movq	%r15, %rsi
	movl	%r9d, %edi
	call	RSA_private_decrypt@PLT
	testl	%eax, %eax
	jle	.L260
	movq	72(%r14), %rcx
	subq	$8, %rsp
	movq	64(%r14), %r9
	movl	%eax, %r8d
	movq	56(%r14), %rdx
	pushq	40(%r14)
	movl	%eax, %esi
	movq	%r13, %rdi
	pushq	32(%r14)
	pushq	%rcx
	movl	%eax, %ecx
	call	RSA_padding_check_PKCS1_OAEP_mgf1@PLT
	addq	$32, %rsp
	jmp	.L265
	.p2align 4,,10
	.p2align 3
.L268:
	movq	%r9, -56(%rbp)
	call	EVP_PKEY_size@PLT
	movl	$106, %edx
	leaq	.LC0(%rip), %rsi
	movslq	%eax, %rdi
	call	CRYPTO_malloc@PLT
	movq	%rax, 56(%r14)
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L263
	movq	16(%r12), %rdi
	movq	-56(%rbp), %r9
	jmp	.L262
.L263:
	movl	$107, %r8d
	movl	$65, %edx
	movl	$167, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-1, %eax
	jmp	.L260
	.cfi_endproc
.LFE1510:
	.size	pkey_rsa_decrypt, .-pkey_rsa_decrypt
	.p2align 4
	.type	pkey_rsa_sign, @function
pkey_rsa_sign:
.LFB1506:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	40(%rdi), %r14
	movq	16(%rdi), %rax
	movq	%rdx, -80(%rbp)
	movq	%r8, -72(%rbp)
	movq	32(%r14), %rdi
	movq	40(%rax), %r15
	movq	%fs:40, %rsi
	movq	%rsi, -56(%rbp)
	xorl	%esi, %esi
	testq	%rdi, %rdi
	je	.L270
	call	EVP_MD_size@PLT
	movslq	%eax, %rcx
	movq	%rcx, -96(%rbp)
	cmpq	-72(%rbp), %rcx
	jne	.L292
	movq	32(%r14), %rdi
	movl	%ecx, -84(%rbp)
	call	EVP_MD_type@PLT
	movl	-84(%rbp), %edx
	cmpl	$95, %eax
	je	.L293
	movl	28(%r14), %eax
	cmpl	$5, %eax
	je	.L294
	cmpl	$1, %eax
	je	.L295
	cmpl	$6, %eax
	jne	.L291
	movq	56(%r14), %rsi
	testq	%rsi, %rsi
	je	.L296
.L282:
	movq	32(%r14), %rcx
	movl	48(%r14), %r9d
	movq	%r12, %rdx
	movq	%r15, %rdi
	movq	40(%r14), %r8
	call	RSA_padding_add_PKCS1_PSS_mgf1@PLT
	testl	%eax, %eax
	je	.L291
	movq	56(%r14), %r12
	movq	%r15, %rdi
	call	RSA_size@PLT
	movl	$3, %r8d
	movq	%r15, %rcx
	movq	%r13, %rdx
	movl	%eax, %edi
	movq	%r12, %rsi
	call	RSA_private_encrypt@PLT
.L275:
	testl	%eax, %eax
	js	.L269
	movq	-80(%rbp), %rbx
	cltq
	movq	%rax, (%rbx)
	movl	$1, %eax
.L269:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L297
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L293:
	.cfi_restore_state
	cmpl	$1, 28(%r14)
	movl	$-1, %eax
	jne	.L269
	xorl	%edi, %edi
	movq	%r15, %r9
	leaq	-60(%rbp), %r8
	movq	%r13, %rcx
	movq	%r12, %rsi
	call	RSA_sign_ASN1_OCTET_STRING@PLT
	testl	%eax, %eax
	jle	.L269
.L290:
	movl	-60(%rbp), %eax
	jmp	.L275
	.p2align 4,,10
	.p2align 3
.L270:
	movl	28(%r14), %r8d
	movl	-72(%rbp), %edi
	movq	%r15, %rcx
	movq	%r13, %rdx
	movq	%r12, %rsi
	call	RSA_private_encrypt@PLT
	jmp	.L275
	.p2align 4,,10
	.p2align 3
.L296:
	movq	16(%rbx), %rdi
	call	EVP_PKEY_size@PLT
	leaq	.LC0(%rip), %rsi
	movl	$106, %edx
	movslq	%eax, %rdi
	call	CRYPTO_malloc@PLT
	movq	%rax, 56(%r14)
	movq	%rax, %rsi
	testq	%rax, %rax
	jne	.L282
	movl	$107, %r8d
	movl	$65, %edx
	movl	$167, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L291:
	movl	$-1, %eax
	jmp	.L269
	.p2align 4,,10
	.p2align 3
.L294:
	movq	16(%rbx), %rdi
	call	EVP_PKEY_size@PLT
	movq	-96(%rbp), %rdx
	cltq
	addq	$1, %rdx
	cmpq	%rdx, %rax
	jb	.L298
	movq	56(%r14), %rdi
	testq	%rdi, %rdi
	je	.L299
.L278:
	movq	-72(%rbp), %rbx
	movq	%r12, %rsi
	movq	%rbx, %rdx
	call	memcpy@PLT
	movq	32(%r14), %rdi
	call	EVP_MD_type@PLT
	movl	%eax, %edi
	call	RSA_X931_hash_id@PLT
	movq	56(%r14), %rdx
	leal	1(%rbx), %edi
	movq	%r15, %rcx
	movl	$5, %r8d
	movb	%al, (%rdx,%rbx)
	movq	56(%r14), %rsi
	movq	%r13, %rdx
	call	RSA_private_encrypt@PLT
	jmp	.L275
	.p2align 4,,10
	.p2align 3
.L295:
	movq	32(%r14), %rdi
	movl	%edx, -72(%rbp)
	call	EVP_MD_type@PLT
	movl	-72(%rbp), %edx
	movq	%r15, %r9
	leaq	-60(%rbp), %r8
	movl	%eax, %edi
	movq	%r13, %rcx
	movq	%r12, %rsi
	call	RSA_sign@PLT
	testl	%eax, %eax
	jg	.L290
	jmp	.L269
	.p2align 4,,10
	.p2align 3
.L299:
	movq	16(%rbx), %rdi
	call	EVP_PKEY_size@PLT
	movl	$106, %edx
	leaq	.LC0(%rip), %rsi
	movslq	%eax, %rdi
	call	CRYPTO_malloc@PLT
	movq	%rax, 56(%r14)
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L278
	movl	$107, %r8d
	movl	$65, %edx
	movl	$167, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$154, %r8d
	movl	$65, %edx
	leaq	.LC0(%rip), %rcx
	movl	$142, %esi
	movl	$4, %edi
	call	ERR_put_error@PLT
	movl	$-1, %eax
	jmp	.L269
	.p2align 4,,10
	.p2align 3
.L292:
	movl	$134, %r8d
	movl	$143, %edx
	movl	$142, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-1, %eax
	jmp	.L269
.L298:
	movl	$150, %r8d
	movl	$120, %edx
	movl	$142, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	orl	$-1, %eax
	jmp	.L269
.L297:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1506:
	.size	pkey_rsa_sign, .-pkey_rsa_sign
	.p2align 4
	.type	pkey_rsa_verifyrecover, @function
pkey_rsa_verifyrecover:
.LFB1507:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	40(%rdi), %r10
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%r10), %rdi
	movl	28(%r10), %r8d
	testq	%rdi, %rdi
	je	.L301
	cmpl	$5, %r8d
	je	.L318
	cmpl	$1, %r8d
	jne	.L313
	movq	16(%rbx), %rax
	movq	40(%rax), %rbx
	call	EVP_MD_type@PLT
	movq	%r13, %rcx
	xorl	%edx, %edx
	movq	%r15, %r9
	pushq	%rbx
	movl	%eax, %edi
	leaq	-64(%rbp), %r8
	xorl	%esi, %esi
	pushq	%r12
	call	int_rsa_verify@PLT
	popq	%rdx
	movl	-64(%rbp), %r12d
	popq	%rcx
	testl	%eax, %eax
	jle	.L319
	testl	%r12d, %r12d
	js	.L300
.L321:
	movslq	%r12d, %r15
.L308:
	movq	%r15, (%r14)
	movl	$1, %r12d
.L300:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L320
	leaq	-40(%rbp), %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L319:
	.cfi_restore_state
	xorl	%r12d, %r12d
	jmp	.L300
	.p2align 4,,10
	.p2align 3
.L301:
	movq	16(%rbx), %rax
	movq	%rsi, %rdx
	movl	%r12d, %edi
	movq	%r15, %rsi
	movq	40(%rax), %rcx
	call	RSA_public_decrypt@PLT
	movl	%eax, %r12d
	testl	%r12d, %r12d
	jns	.L321
	jmp	.L300
	.p2align 4,,10
	.p2align 3
.L318:
	movq	56(%r10), %rdx
	movq	16(%rbx), %rdi
	testq	%rdx, %rdx
	je	.L322
.L303:
	movq	40(%rdi), %rcx
	movl	$5, %r8d
	movl	%r12d, %edi
	movq	%r15, %rsi
	movq	%r10, -72(%rbp)
	xorl	%r12d, %r12d
	call	RSA_public_decrypt@PLT
	testl	%eax, %eax
	jle	.L300
	movq	-72(%rbp), %r10
	leal	-1(%rax), %ebx
	movslq	%ebx, %r15
	movq	56(%r10), %rax
	movq	32(%r10), %rdi
	movq	%r10, -80(%rbp)
	movzbl	(%rax,%r15), %eax
	movl	%eax, -72(%rbp)
	call	EVP_MD_type@PLT
	movl	%eax, %edi
	call	RSA_X931_hash_id@PLT
	cmpl	%eax, -72(%rbp)
	movq	-80(%rbp), %r10
	jne	.L323
	movq	32(%r10), %rdi
	movq	%r10, -72(%rbp)
	call	EVP_MD_size@PLT
	movq	-72(%rbp), %r10
	cmpl	%ebx, %eax
	jne	.L324
	testq	%r13, %r13
	je	.L308
	movq	56(%r10), %rsi
	movq	%r15, %rdx
	movq	%r13, %rdi
	call	memcpy@PLT
	jmp	.L308
	.p2align 4,,10
	.p2align 3
.L323:
	movl	$209, %r8d
	movl	$100, %edx
	movl	$141, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L300
	.p2align 4,,10
	.p2align 3
.L322:
	movq	%r10, -72(%rbp)
	call	EVP_PKEY_size@PLT
	movl	$106, %edx
	leaq	.LC0(%rip), %rsi
	movslq	%eax, %rdi
	call	CRYPTO_malloc@PLT
	movq	-72(%rbp), %r10
	movq	%rax, %rdx
	movq	%rax, 56(%r10)
	testq	%rax, %rax
	je	.L304
	movq	16(%rbx), %rdi
	jmp	.L303
	.p2align 4,,10
	.p2align 3
.L313:
	movl	$-1, %r12d
	jmp	.L300
	.p2align 4,,10
	.p2align 3
.L324:
	movl	$214, %r8d
	movl	$143, %edx
	movl	$141, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L300
.L304:
	movl	$107, %r8d
	movl	$65, %edx
	movl	$167, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	movl	$-1, %r12d
	call	ERR_put_error@PLT
	jmp	.L300
.L320:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1507:
	.size	pkey_rsa_verifyrecover, .-pkey_rsa_verifyrecover
	.p2align 4
	.type	pkey_rsa_verify, @function
pkey_rsa_verify:
.LFB1508:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	40(%rdi), %rbx
	movq	16(%rdi), %r8
	movq	%rdx, -72(%rbp)
	movq	32(%rbx), %rdi
	movq	40(%r8), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L326
	cmpl	$1, 28(%rbx)
	je	.L346
	movq	%r9, -80(%rbp)
	call	EVP_MD_size@PLT
	movq	-80(%rbp), %r9
	cltq
	cmpq	%r12, %rax
	jne	.L347
	movl	28(%rbx), %eax
	cmpl	$5, %eax
	je	.L348
	cmpl	$6, %eax
	jne	.L338
	movq	56(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L349
.L333:
	movl	-72(%rbp), %edi
	movl	$3, %r8d
	movq	%r15, %rcx
	movq	%r13, %rsi
	call	RSA_public_decrypt@PLT
	testl	%eax, %eax
	jle	.L334
	movq	40(%rbx), %rcx
	movq	32(%rbx), %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	movl	48(%rbx), %r9d
	movq	56(%rbx), %r8
	call	RSA_verify_PKCS1_PSS_mgf1@PLT
	testl	%eax, %eax
	setg	%al
	movzbl	%al, %eax
.L325:
	movq	-56(%rbp), %rsi
	xorq	%fs:40, %rsi
	jne	.L350
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L348:
	.cfi_restore_state
	movq	-72(%rbp), %r8
	leaq	-64(%rbp), %rdx
	movq	%r13, %rcx
	xorl	%esi, %esi
	movq	%r9, %rdi
	call	pkey_rsa_verifyrecover
	movl	%eax, %r8d
	movq	-64(%rbp), %rax
	testl	%r8d, %r8d
	jg	.L332
	.p2align 4,,10
	.p2align 3
.L334:
	xorl	%eax, %eax
	jmp	.L325
	.p2align 4,,10
	.p2align 3
.L326:
	movq	56(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L351
.L335:
	movl	28(%rbx), %r8d
	movl	-72(%rbp), %edi
	movq	%r15, %rcx
	movq	%r13, %rsi
	call	RSA_public_decrypt@PLT
	cltq
	movq	%rax, -64(%rbp)
	testq	%rax, %rax
	je	.L334
.L332:
	cmpq	%rax, %r12
	jne	.L334
	movq	56(%rbx), %rsi
	movq	%r12, %rdx
	movq	%r14, %rdi
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L334
	movl	$1, %eax
	jmp	.L325
	.p2align 4,,10
	.p2align 3
.L346:
	call	EVP_MD_type@PLT
	movl	-72(%rbp), %r8d
	movq	%r15, %r9
	movq	%r13, %rcx
	movl	%eax, %edi
	movl	%r12d, %edx
	movq	%r14, %rsi
	call	RSA_verify@PLT
	jmp	.L325
	.p2align 4,,10
	.p2align 3
.L349:
	movq	16(%r9), %rdi
	call	EVP_PKEY_size@PLT
	movl	$106, %edx
	leaq	.LC0(%rip), %rsi
	movslq	%eax, %rdi
	call	CRYPTO_malloc@PLT
	movq	%rax, 56(%rbx)
	movq	%rax, %rdx
	testq	%rax, %rax
	jne	.L333
.L336:
	movl	$107, %r8d
	movl	$65, %edx
	movl	$167, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-1, %eax
	jmp	.L325
	.p2align 4,,10
	.p2align 3
.L351:
	movq	%r8, %rdi
	call	EVP_PKEY_size@PLT
	movl	$106, %edx
	leaq	.LC0(%rip), %rsi
	movslq	%eax, %rdi
	call	CRYPTO_malloc@PLT
	movq	%rax, 56(%rbx)
	movq	%rax, %rdx
	testq	%rax, %rax
	jne	.L335
	jmp	.L336
	.p2align 4,,10
	.p2align 3
.L338:
	movl	$-1, %eax
	jmp	.L325
.L347:
	movl	$254, %r8d
	movl	$143, %edx
	movl	$149, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-1, %eax
	jmp	.L325
.L350:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1508:
	.size	pkey_rsa_verify, .-pkey_rsa_verify
	.globl	rsa_pss_pkey_meth
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	rsa_pss_pkey_meth, @object
	.size	rsa_pss_pkey_meth, 256
rsa_pss_pkey_meth:
	.long	912
	.long	2
	.quad	pkey_rsa_init
	.quad	pkey_rsa_copy
	.quad	pkey_rsa_cleanup
	.quad	0
	.quad	0
	.quad	0
	.quad	pkey_rsa_keygen
	.quad	pkey_pss_init
	.quad	pkey_rsa_sign
	.quad	pkey_pss_init
	.quad	pkey_rsa_verify
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	pkey_rsa_ctrl
	.quad	pkey_rsa_ctrl_str
	.zero	48
	.globl	rsa_pkey_meth
	.align 32
	.type	rsa_pkey_meth, @object
	.size	rsa_pkey_meth, 256
rsa_pkey_meth:
	.long	6
	.long	2
	.quad	pkey_rsa_init
	.quad	pkey_rsa_copy
	.quad	pkey_rsa_cleanup
	.quad	0
	.quad	0
	.quad	0
	.quad	pkey_rsa_keygen
	.quad	0
	.quad	pkey_rsa_sign
	.quad	0
	.quad	pkey_rsa_verify
	.quad	0
	.quad	pkey_rsa_verifyrecover
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	pkey_rsa_encrypt
	.quad	0
	.quad	pkey_rsa_decrypt
	.quad	0
	.quad	0
	.quad	pkey_rsa_ctrl
	.quad	pkey_rsa_ctrl_str
	.zero	48
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
