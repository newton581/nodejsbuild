	.file	"t_bitst.c"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	""
.LC1:
	.string	"%*s"
.LC2:
	.string	", "
.LC3:
	.string	"\n"
	.text
	.p2align 4
	.globl	ASN1_BIT_STRING_name_print
	.type	ASN1_BIT_STRING_name_print, @function
ASN1_BIT_STRING_name_print:
.LFB1296:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	leaq	.LC1(%rip), %rsi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdx, %rbx
	movl	%ecx, %edx
	leaq	.LC0(%rip), %rcx
	call	BIO_printf@PLT
	cmpq	$0, 8(%rbx)
	je	.L2
	movl	$1, %r14d
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L4:
	movq	8(%rbx), %rsi
	movq	%r13, %rdi
	xorl	%r14d, %r14d
	call	BIO_puts@PLT
.L3:
	addq	$24, %rbx
	cmpq	$0, 8(%rbx)
	je	.L2
.L5:
	movl	(%rbx), %esi
	movq	%r12, %rdi
	call	ASN1_BIT_STRING_get_bit@PLT
	testl	%eax, %eax
	je	.L3
	testb	%r14b, %r14b
	jne	.L4
	leaq	.LC2(%rip), %rsi
	movq	%r13, %rdi
	call	BIO_puts@PLT
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L2:
	movq	%r13, %rdi
	leaq	.LC3(%rip), %rsi
	call	BIO_puts@PLT
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1296:
	.size	ASN1_BIT_STRING_name_print, .-ASN1_BIT_STRING_name_print
	.p2align 4
	.globl	ASN1_BIT_STRING_set_asc
	.type	ASN1_BIT_STRING_set_asc, @function
ASN1_BIT_STRING_set_asc:
.LFB1297:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rcx), %r12
	testq	%r12, %r12
	je	.L19
	movq	%rdi, %r13
	movq	%rsi, %r15
	movl	%edx, %r14d
	movq	%rcx, %rbx
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L14:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L16
	movq	32(%rbx), %r12
	addq	$24, %rbx
	testq	%r12, %r12
	je	.L19
.L17:
	movq	16(%rbx), %rdi
	movq	%r15, %rsi
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L14
.L16:
	movl	(%rbx), %esi
	testl	%esi, %esi
	js	.L19
	movl	$1, %eax
	testq	%r13, %r13
	je	.L12
	movl	%r14d, %edx
	movq	%r13, %rdi
	call	ASN1_BIT_STRING_set_bit@PLT
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
.L12:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1297:
	.size	ASN1_BIT_STRING_set_asc, .-ASN1_BIT_STRING_set_asc
	.p2align 4
	.globl	ASN1_BIT_STRING_num_asc
	.type	ASN1_BIT_STRING_num_asc, @function
ASN1_BIT_STRING_num_asc:
.LFB1298:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	8(%rsi), %r12
	testq	%r12, %r12
	je	.L33
	movq	%rdi, %r13
	movq	%rsi, %rbx
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L30:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L31
	movq	32(%rbx), %r12
	addq	$24, %rbx
	testq	%r12, %r12
	je	.L33
.L32:
	movq	16(%rbx), %rdi
	movq	%r13, %rsi
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L30
.L31:
	movl	(%rbx), %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L33:
	.cfi_restore_state
	addq	$8, %rsp
	movl	$-1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1298:
	.size	ASN1_BIT_STRING_num_asc, .-ASN1_BIT_STRING_num_asc
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
