	.file	"ssl_asn1.c"
	.text
	.p2align 4
	.globl	i2d_SSL_SESSION
	.type	i2d_SSL_SESSION, @function
i2d_SSL_SESSION:
.LFB992:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$568, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L21
	movq	%rsi, %r12
	movq	504(%rdi), %rsi
	movq	%rdi, %rbx
	testq	%rsi, %rsi
	je	.L33
	leaq	-232(%rbp), %rdx
	xorl	%eax, %eax
	movl	$22, %ecx
	movl	$1, -240(%rbp)
	movq	%rdx, %rdi
	rep stosq
	movl	(%rbx), %eax
	movl	%eax, -236(%rbp)
	movl	24(%rsi), %eax
.L19:
	rolw	$8, %ax
	leaq	80(%rbx), %rdx
	movq	544(%rbx), %r13
	movl	$2, -592(%rbp)
	movw	%ax, -42(%rbp)
	leaq	-42(%rbp), %rax
	movq	%rax, -584(%rbp)
	leaq	-592(%rbp), %rax
	movq	%rax, -232(%rbp)
	movq	8(%rbx), %rax
	movq	%rdx, -552(%rbp)
	leaq	344(%rbx), %rdx
	movl	%eax, -560(%rbp)
	movq	336(%rbx), %rax
	movq	%rdx, -520(%rbp)
	leaq	384(%rbx), %rdx
	movl	%eax, -528(%rbp)
	leaq	-560(%rbp), %rax
	movq	%rax, %xmm0
	leaq	-528(%rbp), %rax
	movq	$0, -576(%rbp)
	movq	%rax, %xmm1
	movq	376(%rbx), %rax
	movq	$0, -544(%rbp)
	punpcklqdq	%xmm1, %xmm0
	movq	$0, -512(%rbp)
	movl	%eax, -496(%rbp)
	leaq	-496(%rbp), %rax
	movq	%rax, -168(%rbp)
	movq	464(%rbx), %rax
	movups	%xmm0, -216(%rbp)
	movdqu	480(%rbx), %xmm0
	movl	%eax, -160(%rbp)
	movq	440(%rbx), %rax
	shufpd	$1, %xmm0, %xmm0
	movq	%rdx, -488(%rbp)
	movq	$0, -480(%rbp)
	movq	%rax, -176(%rbp)
	movaps	%xmm0, -192(%rbp)
	testq	%r13, %r13
	je	.L5
	movq	%r13, %rdi
	call	strlen@PLT
	movq	%r13, -456(%rbp)
	movl	%eax, -464(%rbp)
	leaq	-464(%rbp), %rax
	movq	$0, -448(%rbp)
	movq	%rax, -152(%rbp)
.L6:
	movq	552(%rbx), %rax
	testq	%rax, %rax
	je	.L7
	movq	560(%rbx), %rdx
	movq	%rax, -424(%rbp)
	leaq	-432(%rbp), %rax
	movq	$0, -416(%rbp)
	movl	%edx, -432(%rbp)
	movq	%rax, -128(%rbp)
.L7:
	movq	568(%rbx), %rax
	testq	%rax, %rax
	je	.L8
	movq	%rax, -144(%rbp)
.L8:
	movl	576(%rbx), %eax
	movq	416(%rbx), %r13
	movl	%eax, -136(%rbp)
	testq	%r13, %r13
	je	.L9
	movq	%r13, %rdi
	call	strlen@PLT
	movq	%r13, -328(%rbp)
	movl	%eax, -336(%rbp)
	leaq	-336(%rbp), %rax
	movq	$0, -320(%rbp)
	movq	%rax, -120(%rbp)
.L10:
	movq	424(%rbx), %r13
	testq	%r13, %r13
	je	.L11
	movq	%r13, %rdi
	call	strlen@PLT
	movq	%r13, -360(%rbp)
	movl	%eax, -368(%rbp)
	leaq	-368(%rbp), %rax
	movq	$0, -352(%rbp)
	movq	%rax, -112(%rbp)
.L12:
	movq	608(%rbx), %r13
	testq	%r13, %r13
	je	.L13
	movq	%r13, %rdi
	call	strlen@PLT
	movq	%r13, -392(%rbp)
	movl	%eax, -400(%rbp)
	leaq	-400(%rbp), %rax
	movq	$0, -384(%rbp)
	movq	%rax, -104(%rbp)
.L14:
	movl	632(%rbx), %eax
	movq	%rax, -96(%rbp)
	movl	580(%rbx), %eax
	movl	%eax, -88(%rbp)
	movq	584(%rbx), %rax
	testq	%rax, %rax
	je	.L34
	movq	592(%rbx), %rdx
	movq	%rax, -296(%rbp)
	leaq	-304(%rbp), %rax
	movq	$0, -288(%rbp)
	movl	%edx, -304(%rbp)
	movq	%rax, -80(%rbp)
.L16:
	movzbl	600(%rbx), %eax
	movl	%eax, -72(%rbp)
	movq	616(%rbx), %rax
	testq	%rax, %rax
	je	.L35
	movq	624(%rbx), %rdx
	movq	%rax, -264(%rbp)
	leaq	-272(%rbp), %rax
	movq	$0, -256(%rbp)
	movl	%edx, -272(%rbp)
	movq	%rax, -64(%rbp)
.L18:
	leaq	-240(%rbp), %rdi
	leaq	SSL_SESSION_ASN1_it(%rip), %rdx
	movq	%r12, %rsi
	call	ASN1_item_i2d@PLT
.L1:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L36
	addq	$568, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L33:
	.cfi_restore_state
	movq	512(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L1
	leaq	-224(%rbp), %r8
	movq	%rsi, %rax
	movl	$21, %ecx
	movl	$1, -240(%rbp)
	movq	%r8, %rdi
	rep stosq
	movl	(%rbx), %eax
	movl	%eax, -236(%rbp)
	movq	%rdx, %rax
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L5:
	movq	$0, -152(%rbp)
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L35:
	movq	$0, -64(%rbp)
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L34:
	movq	$0, -80(%rbp)
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L13:
	movq	$0, -104(%rbp)
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L11:
	movq	$0, -112(%rbp)
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L9:
	movq	$0, -120(%rbp)
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L21:
	xorl	%eax, %eax
	jmp	.L1
.L36:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE992:
	.size	i2d_SSL_SESSION, .-i2d_SSL_SESSION
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/ssl/ssl_asn1.c"
	.text
	.p2align 4
	.globl	d2i_SSL_SESSION
	.type	d2i_SSL_SESSION, @function
d2i_SSL_SESSION:
.LFB995:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	SSL_SESSION_ASN1_it(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	xorl	%edi, %edi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	leaq	-64(%rbp), %rsi
	movq	%rax, -64(%rbp)
	call	ASN1_item_d2i@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L92
	testq	%rbx, %rbx
	je	.L39
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L40
.L89:
	cmpl	$1, 0(%r13)
	jne	.L162
	movl	4(%r13), %eax
	movl	%eax, %edx
	sarl	$8, %edx
	cmpl	$3, %edx
	setne	%cl
	cmpl	$254, %edx
	setne	%dl
	testb	%dl, %cl
	je	.L42
	cmpl	$256, %eax
	jne	.L163
.L42:
	movl	%eax, (%r12)
	movq	8(%r13), %rax
	cmpl	$2, (%rax)
	je	.L43
	movl	$276, %r8d
	movl	$137, %edx
	movl	$103, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L38:
	leaq	SSL_SESSION_ASN1_it(%rip), %rsi
	movq	%r13, %rdi
	call	ASN1_item_free@PLT
	testq	%rbx, %rbx
	je	.L86
.L87:
	cmpq	%r12, (%rbx)
	je	.L95
.L86:
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	SSL_SESSION_free@PLT
.L37:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L164
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L40:
	.cfi_restore_state
	call	SSL_SESSION_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L89
	leaq	SSL_SESSION_ASN1_it(%rip), %rsi
	movq	%r13, %rdi
	call	ASN1_item_free@PLT
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L95:
	xorl	%r12d, %r12d
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L162:
	movl	$262, %r8d
	movl	$254, %edx
	movl	$103, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L43:
	movq	8(%rax), %rax
	movzwl	(%rax), %edi
	rolw	$8, %di
	movzwl	%di, %edi
	orq	$50331648, %rdi
	movq	%rdi, 512(%r12)
	call	ssl3_get_cipher_by_id@PLT
	movq	%rax, 504(%r12)
	testq	%rax, %rax
	je	.L38
	movq	32(%r13), %rdx
	testq	%rdx, %rdx
	je	.L165
	movl	(%rdx), %eax
	cmpl	$32, %eax
	ja	.L38
	movq	8(%rdx), %rsi
	leaq	344(%r12), %rcx
	cmpl	$8, %eax
	jnb	.L46
	testb	$4, %al
	jne	.L166
	testl	%eax, %eax
	je	.L47
	movzbl	(%rsi), %edi
	movb	%dil, (%rcx)
	testb	$2, %al
	jne	.L148
.L158:
	movl	(%rdx), %eax
.L47:
	cltq
	movq	%rax, 336(%r12)
.L45:
	movq	24(%r13), %rdx
	testq	%rdx, %rdx
	je	.L93
	movl	(%rdx), %eax
	cmpl	$256, %eax
	ja	.L38
	movq	8(%rdx), %rsi
	leaq	80(%r12), %rcx
	cmpl	$8, %eax
	jnb	.L53
	testb	$4, %al
	jne	.L167
	testl	%eax, %eax
	je	.L54
	movzbl	(%rsi), %edi
	movb	%dil, (%rcx)
	testb	$2, %al
	jne	.L150
	movl	(%rdx), %eax
.L54:
	cltq
.L52:
	movq	%rax, 8(%r12)
	movq	48(%r13), %rax
	testq	%rax, %rax
	je	.L57
.L160:
	movq	%rax, 488(%r12)
	movq	56(%r13), %rax
	movl	$3, %edx
	movq	440(%r12), %rdi
	testq	%rax, %rax
	cmove	%rdx, %rax
	movq	%rax, 480(%r12)
	call	X509_free@PLT
	movq	64(%r13), %rax
	movq	72(%r13), %rdx
	movq	%rax, 440(%r12)
	movq	$0, 64(%r13)
	testq	%rdx, %rdx
	je	.L168
	movl	(%rdx), %eax
	cmpl	$32, %eax
	ja	.L38
	movq	8(%rdx), %rsi
	leaq	384(%r12), %rcx
	cmpl	$8, %eax
	jnb	.L62
	testb	$4, %al
	jne	.L169
	testl	%eax, %eax
	je	.L63
	movzbl	(%rsi), %edi
	movb	%dil, (%rcx)
	testb	$2, %al
	jne	.L151
.L161:
	movl	(%rdx), %eax
.L63:
	cltq
	movq	%rax, 376(%r12)
.L61:
	movslq	80(%r13), %rax
	movq	88(%r13), %r15
	movl	$213, %edx
	leaq	.LC0(%rip), %rsi
	movq	544(%r12), %rdi
	movq	%rax, 464(%r12)
	call	CRYPTO_free@PLT
	movq	$0, 544(%r12)
	testq	%r15, %r15
	je	.L71
	movslq	(%r15), %rsi
	movq	8(%r15), %rdi
	movl	$217, %ecx
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_strndup@PLT
	movq	%rax, 544(%r12)
	testq	%rax, %rax
	je	.L38
.L71:
	movq	120(%r13), %r15
	movq	416(%r12), %rdi
	movl	$213, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 416(%r12)
	testq	%r15, %r15
	je	.L69
	movslq	(%r15), %rsi
	movq	8(%r15), %rdi
	movl	$217, %ecx
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_strndup@PLT
	movq	%rax, 416(%r12)
	testq	%rax, %rax
	je	.L38
.L69:
	movq	128(%r13), %r15
	movq	424(%r12), %rdi
	movl	$213, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 424(%r12)
	testq	%r15, %r15
	je	.L72
	movslq	(%r15), %rsi
	movq	8(%r15), %rdi
	movl	$217, %ecx
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_strndup@PLT
	movq	%rax, 424(%r12)
	testq	%rax, %rax
	je	.L38
.L72:
	movq	96(%r13), %rax
	movq	552(%r12), %rdi
	movl	$331, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rax, 568(%r12)
	movl	104(%r13), %eax
	movl	%eax, 576(%r12)
	call	CRYPTO_free@PLT
	movq	112(%r13), %rax
	testq	%rax, %rax
	je	.L170
	movq	8(%rax), %rdx
	movq	%rdx, 552(%r12)
	movslq	(%rax), %rdx
	movq	%rdx, 560(%r12)
	movq	$0, 8(%rax)
.L76:
	movq	136(%r13), %r15
	movq	608(%r12), %rdi
	movl	$213, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 608(%r12)
	testq	%r15, %r15
	je	.L80
	movslq	(%r15), %rsi
	movq	8(%r15), %rdi
	movl	$217, %ecx
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_strndup@PLT
	movq	%rax, 608(%r12)
	testq	%rax, %rax
	je	.L38
.L80:
	movq	144(%r13), %rax
	movq	584(%r12), %rdi
	movl	$359, %edx
	leaq	.LC0(%rip), %rsi
	movl	%eax, 632(%r12)
	movl	152(%r13), %eax
	movl	%eax, 580(%r12)
	call	CRYPTO_free@PLT
	movq	160(%r13), %rax
	testq	%rax, %rax
	je	.L171
	movq	8(%rax), %rdx
	movq	%rdx, 584(%r12)
	movslq	(%rax), %rdx
	movq	%rdx, 592(%r12)
	movq	$0, 8(%rax)
.L81:
	movl	168(%r13), %eax
	movq	616(%r12), %rdi
	movl	$371, %edx
	leaq	.LC0(%rip), %rsi
	movb	%al, 600(%r12)
	call	CRYPTO_free@PLT
	movq	176(%r13), %rax
	testq	%rax, %rax
	je	.L82
	movq	8(%rax), %rdx
	movq	%rdx, 616(%r12)
	movslq	(%rax), %rdx
	movq	%rdx, 624(%r12)
	movq	$0, 8(%rax)
.L83:
	leaq	SSL_SESSION_ASN1_it(%rip), %rsi
	movq	%r13, %rdi
	call	ASN1_item_free@PLT
	testq	%rbx, %rbx
	je	.L84
	cmpq	$0, (%rbx)
	je	.L172
.L84:
	movq	-64(%rbp), %rax
	movq	%rax, (%r14)
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L92:
	xorl	%r12d, %r12d
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L39:
	call	SSL_SESSION_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L89
	leaq	SSL_SESSION_ASN1_it(%rip), %rsi
	movq	%r13, %rdi
	call	ASN1_item_free@PLT
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L163:
	movl	$269, %r8d
	movl	$259, %edx
	movl	$103, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L38
.L148:
	movzwl	-2(%rsi,%rax), %esi
	movw	%si, -2(%rcx,%rax)
	movl	(%rdx), %eax
	jmp	.L47
.L150:
	movzwl	-2(%rsi,%rax), %esi
	movw	%si, -2(%rcx,%rax)
	movl	(%rdx), %eax
	jmp	.L54
.L151:
	movzwl	-2(%rsi,%rax), %esi
	movw	%si, -2(%rcx,%rax)
	movl	(%rdx), %eax
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L165:
	movq	$0, 336(%r12)
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L46:
	movq	(%rsi), %rdi
	movq	%rdi, 344(%r12)
	movl	%eax, %edi
	movq	-8(%rsi,%rdi), %r8
	movq	%r8, -8(%rcx,%rdi)
	leaq	352(%r12), %rdi
	andq	$-8, %rdi
	subq	%rdi, %rcx
	addl	%ecx, %eax
	subq	%rcx, %rsi
	andl	$-8, %eax
	cmpl	$8, %eax
	jb	.L158
	andl	$-8, %eax
	xorl	%ecx, %ecx
.L50:
	movl	%ecx, %r8d
	addl	$8, %ecx
	movq	(%rsi,%r8), %r9
	movq	%r9, (%rdi,%r8)
	cmpl	%eax, %ecx
	jb	.L50
	jmp	.L158
	.p2align 4,,10
	.p2align 3
.L57:
	xorl	%edi, %edi
	call	time@PLT
	jmp	.L160
	.p2align 4,,10
	.p2align 3
.L93:
	xorl	%eax, %eax
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L53:
	movq	(%rsi), %rdi
	movq	%rdi, 80(%r12)
	movl	%eax, %edi
	movq	-8(%rsi,%rdi), %r8
	movq	%r8, -8(%rcx,%rdi)
	leaq	88(%r12), %rdi
	andq	$-8, %rdi
	subq	%rdi, %rcx
	subq	%rcx, %rsi
	addl	%eax, %ecx
	shrl	$3, %ecx
	rep movsq
	movl	(%rdx), %eax
	jmp	.L54
.L168:
	movq	$0, 376(%r12)
	jmp	.L61
.L62:
	movq	(%rsi), %rdi
	movq	%rdi, 384(%r12)
	movl	%eax, %edi
	movq	-8(%rsi,%rdi), %r8
	movq	%r8, -8(%rcx,%rdi)
	leaq	392(%r12), %rdi
	andq	$-8, %rdi
	subq	%rdi, %rcx
	addl	%ecx, %eax
	subq	%rcx, %rsi
	andl	$-8, %eax
	cmpl	$8, %eax
	jb	.L161
	andl	$-8, %eax
	xorl	%ecx, %ecx
.L66:
	movl	%ecx, %r8d
	addl	$8, %ecx
	movq	(%rsi,%r8), %r9
	movq	%r9, (%rdi,%r8)
	cmpl	%eax, %ecx
	jb	.L66
	jmp	.L161
.L166:
	movl	(%rsi), %edi
	movl	%edi, (%rcx)
	movl	-4(%rsi,%rax), %esi
	movl	%esi, -4(%rcx,%rax)
	movl	(%rdx), %eax
	jmp	.L47
.L167:
	movl	(%rsi), %edi
	movl	%edi, (%rcx)
	movl	-4(%rsi,%rax), %esi
	movl	%esi, -4(%rcx,%rax)
	movl	(%rdx), %eax
	jmp	.L54
.L170:
	movq	$0, 552(%r12)
	jmp	.L76
.L169:
	movl	(%rsi), %edi
	movl	%edi, (%rcx)
	movl	-4(%rsi,%rax), %esi
	movl	%esi, -4(%rcx,%rax)
	movl	(%rdx), %eax
	jmp	.L63
.L164:
	call	__stack_chk_fail@PLT
.L82:
	movq	$0, 616(%r12)
	movq	$0, 624(%r12)
	jmp	.L83
.L171:
	movq	$0, 584(%r12)
	movq	$0, 592(%r12)
	jmp	.L81
.L172:
	movq	%r12, (%rbx)
	jmp	.L84
	.cfi_endproc
.LFE995:
	.size	d2i_SSL_SESSION, .-d2i_SSL_SESSION
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"SSL_SESSION_ASN1"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	SSL_SESSION_ASN1_it, @object
	.size	SSL_SESSION_ASN1_it, 56
SSL_SESSION_ASN1_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	SSL_SESSION_ASN1_seq_tt
	.quad	24
	.quad	0
	.quad	184
	.quad	.LC1
	.section	.rodata.str1.1
.LC2:
	.string	"version"
.LC3:
	.string	"ssl_version"
.LC4:
	.string	"cipher"
.LC5:
	.string	"session_id"
.LC6:
	.string	"master_key"
.LC7:
	.string	"key_arg"
.LC8:
	.string	"time"
.LC9:
	.string	"timeout"
.LC10:
	.string	"peer"
.LC11:
	.string	"session_id_context"
.LC12:
	.string	"verify_result"
.LC13:
	.string	"tlsext_hostname"
.LC14:
	.string	"psk_identity_hint"
.LC15:
	.string	"psk_identity"
.LC16:
	.string	"tlsext_tick_lifetime_hint"
.LC17:
	.string	"tlsext_tick"
.LC18:
	.string	"comp_id"
.LC19:
	.string	"srp_username"
.LC20:
	.string	"flags"
.LC21:
	.string	"tlsext_tick_age_add"
.LC22:
	.string	"max_early_data"
.LC23:
	.string	"alpn_selected"
.LC24:
	.string	"tlsext_max_fragment_len_mode"
.LC25:
	.string	"ticket_appdata"
	.section	.data.rel.ro,"aw"
	.align 32
	.type	SSL_SESSION_ASN1_seq_tt, @object
	.size	SSL_SESSION_ASN1_seq_tt, 960
SSL_SESSION_ASN1_seq_tt:
	.quad	4096
	.quad	0
	.quad	0
	.quad	.LC2
	.quad	UINT32_it
	.quad	4096
	.quad	0
	.quad	4
	.quad	.LC3
	.quad	INT32_it
	.quad	0
	.quad	0
	.quad	8
	.quad	.LC4
	.quad	ASN1_OCTET_STRING_it
	.quad	0
	.quad	0
	.quad	32
	.quad	.LC5
	.quad	ASN1_OCTET_STRING_it
	.quad	0
	.quad	0
	.quad	24
	.quad	.LC6
	.quad	ASN1_OCTET_STRING_it
	.quad	137
	.quad	0
	.quad	40
	.quad	.LC7
	.quad	ASN1_OCTET_STRING_it
	.quad	4241
	.quad	1
	.quad	48
	.quad	.LC8
	.quad	ZINT64_it
	.quad	4241
	.quad	2
	.quad	56
	.quad	.LC9
	.quad	ZINT64_it
	.quad	145
	.quad	3
	.quad	64
	.quad	.LC10
	.quad	X509_it
	.quad	145
	.quad	4
	.quad	72
	.quad	.LC11
	.quad	ASN1_OCTET_STRING_it
	.quad	4241
	.quad	5
	.quad	80
	.quad	.LC12
	.quad	ZINT32_it
	.quad	145
	.quad	6
	.quad	88
	.quad	.LC13
	.quad	ASN1_OCTET_STRING_it
	.quad	145
	.quad	7
	.quad	120
	.quad	.LC14
	.quad	ASN1_OCTET_STRING_it
	.quad	145
	.quad	8
	.quad	128
	.quad	.LC15
	.quad	ASN1_OCTET_STRING_it
	.quad	4241
	.quad	9
	.quad	96
	.quad	.LC16
	.quad	ZUINT64_it
	.quad	145
	.quad	10
	.quad	112
	.quad	.LC17
	.quad	ASN1_OCTET_STRING_it
	.quad	145
	.quad	11
	.quad	16
	.quad	.LC18
	.quad	ASN1_OCTET_STRING_it
	.quad	145
	.quad	12
	.quad	136
	.quad	.LC19
	.quad	ASN1_OCTET_STRING_it
	.quad	4241
	.quad	13
	.quad	144
	.quad	.LC20
	.quad	ZUINT64_it
	.quad	4241
	.quad	14
	.quad	104
	.quad	.LC21
	.quad	ZUINT32_it
	.quad	4241
	.quad	15
	.quad	152
	.quad	.LC22
	.quad	ZUINT32_it
	.quad	145
	.quad	16
	.quad	160
	.quad	.LC23
	.quad	ASN1_OCTET_STRING_it
	.quad	4241
	.quad	17
	.quad	168
	.quad	.LC24
	.quad	ZUINT32_it
	.quad	145
	.quad	18
	.quad	176
	.quad	.LC25
	.quad	ASN1_OCTET_STRING_it
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
