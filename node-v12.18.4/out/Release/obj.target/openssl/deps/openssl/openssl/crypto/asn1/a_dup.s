	.file	"a_dup.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/asn1/a_dup.c"
	.text
	.p2align 4
	.globl	ASN1_dup
	.type	ASN1_dup, @function
ASN1_dup:
.LFB419:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L5
	movq	%rdi, %r14
	movq	%rdx, %r13
	movq	%rdx, %rdi
	movq	%rsi, %rbx
	xorl	%esi, %esi
	call	*%r14
	movl	$27, %edx
	leaq	.LC0(%rip), %rsi
	leal	10(%rax), %edi
	movslq	%edi, %rdi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L8
	movq	%r13, %rdi
	leaq	-56(%rbp), %rsi
	movq	%rax, -56(%rbp)
	call	*%r14
	leaq	-48(%rbp), %rsi
	xorl	%edi, %edi
	movq	%r12, -48(%rbp)
	movslq	%eax, %rdx
	call	*%rbx
	movl	$36, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	CRYPTO_free@PLT
.L1:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L9
	addq	$32, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	.cfi_restore_state
	xorl	%r13d, %r13d
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L8:
	movl	$29, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r13d, %r13d
	movl	$111, %esi
	movl	$13, %edi
	call	ERR_put_error@PLT
	jmp	.L1
.L9:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE419:
	.size	ASN1_dup, .-ASN1_dup
	.p2align 4
	.globl	ASN1_item_dup
	.type	ASN1_item_dup, @function
ASN1_item_dup:
.LFB420:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	subq	$32, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	$0, -40(%rbp)
	testq	%rsi, %rsi
	je	.L14
	movq	%rdi, %r13
	movq	%rsi, %rdi
	leaq	-40(%rbp), %rsi
	movq	%r13, %rdx
	call	ASN1_item_i2d@PLT
	movq	-40(%rbp), %r12
	movslq	%eax, %rdx
	testq	%r12, %r12
	je	.L16
	leaq	-32(%rbp), %rsi
	xorl	%edi, %edi
	movq	%r13, %rcx
	movq	%r12, -32(%rbp)
	call	ASN1_item_d2i@PLT
	movq	-40(%rbp), %rdi
	movl	$66, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rax, %r12
	call	CRYPTO_free@PLT
.L10:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L17
	addq	$32, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	.cfi_restore_state
	xorl	%r12d, %r12d
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L16:
	movl	$61, %r8d
	movl	$65, %edx
	movl	$191, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L10
.L17:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE420:
	.size	ASN1_item_dup, .-ASN1_item_dup
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
