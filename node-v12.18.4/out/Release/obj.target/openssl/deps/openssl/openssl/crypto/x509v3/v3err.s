	.file	"v3err.c"
	.text
	.p2align 4
	.globl	ERR_load_X509V3_strings
	.type	ERR_load_X509V3_strings, @function
ERR_load_X509V3_strings:
.LFB203:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$571097088, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_func_error_string@PLT
	testq	%rax, %rax
	je	.L5
	movl	$1, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	.cfi_restore_state
	leaq	X509V3_str_functs(%rip), %rdi
	call	ERR_load_strings_const@PLT
	leaq	X509V3_str_reasons(%rip), %rdi
	call	ERR_load_strings_const@PLT
	movl	$1, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE203:
	.size	ERR_load_X509V3_strings, .-ERR_load_X509V3_strings
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"bad ip address"
.LC1:
	.string	"bad object"
.LC2:
	.string	"bn dec2bn error"
.LC3:
	.string	"bn to asn1 integer error"
.LC4:
	.string	"dirname error"
.LC5:
	.string	"distpoint already set"
.LC6:
	.string	"duplicate zone id"
.LC7:
	.string	"error converting zone"
.LC8:
	.string	"error creating extension"
.LC9:
	.string	"error in extension"
.LC10:
	.string	"expected a section name"
.LC11:
	.string	"extension exists"
.LC12:
	.string	"extension name error"
.LC13:
	.string	"extension not found"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC14:
	.string	"extension setting not supported"
	.section	.rodata.str1.1
.LC15:
	.string	"extension value error"
.LC16:
	.string	"illegal empty extension"
.LC17:
	.string	"incorrect policy syntax tag"
.LC18:
	.string	"invalid asnumber"
.LC19:
	.string	"invalid asrange"
.LC20:
	.string	"invalid boolean string"
.LC21:
	.string	"invalid extension string"
.LC22:
	.string	"invalid inheritance"
.LC23:
	.string	"invalid ipaddress"
.LC24:
	.string	"invalid multiple rdns"
.LC25:
	.string	"invalid name"
.LC26:
	.string	"invalid null argument"
.LC27:
	.string	"invalid null name"
.LC28:
	.string	"invalid null value"
.LC29:
	.string	"invalid number"
.LC30:
	.string	"invalid numbers"
.LC31:
	.string	"invalid object identifier"
.LC32:
	.string	"invalid option"
.LC33:
	.string	"invalid policy identifier"
.LC34:
	.string	"invalid proxy policy setting"
.LC35:
	.string	"invalid purpose"
.LC36:
	.string	"invalid safi"
.LC37:
	.string	"invalid section"
.LC38:
	.string	"invalid syntax"
.LC39:
	.string	"issuer decode error"
.LC40:
	.string	"missing value"
.LC41:
	.string	"need organization and numbers"
.LC42:
	.string	"no config database"
.LC43:
	.string	"no issuer certificate"
.LC44:
	.string	"no issuer details"
.LC45:
	.string	"no policy identifier"
	.section	.rodata.str1.8
	.align 8
.LC46:
	.string	"no proxy cert policy language defined"
	.section	.rodata.str1.1
.LC47:
	.string	"no public key"
.LC48:
	.string	"no subject details"
.LC49:
	.string	"operation not defined"
.LC50:
	.string	"othername error"
	.section	.rodata.str1.8
	.align 8
.LC51:
	.string	"policy language already defined"
	.section	.rodata.str1.1
.LC52:
	.string	"policy path length"
	.section	.rodata.str1.8
	.align 8
.LC53:
	.string	"policy path length already defined"
	.align 8
.LC54:
	.string	"policy when proxy language requires no policy"
	.section	.rodata.str1.1
.LC55:
	.string	"section not found"
.LC56:
	.string	"unable to get issuer details"
.LC57:
	.string	"unable to get issuer keyid"
.LC58:
	.string	"unknown bit string argument"
.LC59:
	.string	"unknown extension"
.LC60:
	.string	"unknown extension name"
.LC61:
	.string	"unknown option"
.LC62:
	.string	"unsupported option"
.LC63:
	.string	"unsupported type"
.LC64:
	.string	"user too long"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	X509V3_str_reasons, @object
	.size	X509V3_str_reasons, 1056
X509V3_str_reasons:
	.quad	570425462
	.quad	.LC0
	.quad	570425463
	.quad	.LC1
	.quad	570425444
	.quad	.LC2
	.quad	570425445
	.quad	.LC3
	.quad	570425493
	.quad	.LC4
	.quad	570425504
	.quad	.LC5
	.quad	570425477
	.quad	.LC6
	.quad	570425475
	.quad	.LC7
	.quad	570425488
	.quad	.LC8
	.quad	570425472
	.quad	.LC9
	.quad	570425481
	.quad	.LC10
	.quad	570425489
	.quad	.LC11
	.quad	570425459
	.quad	.LC12
	.quad	570425446
	.quad	.LC13
	.quad	570425447
	.quad	.LC14
	.quad	570425460
	.quad	.LC15
	.quad	570425495
	.quad	.LC16
	.quad	570425496
	.quad	.LC17
	.quad	570425506
	.quad	.LC18
	.quad	570425507
	.quad	.LC19
	.quad	570425448
	.quad	.LC20
	.quad	570425449
	.quad	.LC21
	.quad	570425509
	.quad	.LC22
	.quad	570425510
	.quad	.LC23
	.quad	570425505
	.quad	.LC24
	.quad	570425450
	.quad	.LC25
	.quad	570425451
	.quad	.LC26
	.quad	570425452
	.quad	.LC27
	.quad	570425453
	.quad	.LC28
	.quad	570425484
	.quad	.LC29
	.quad	570425485
	.quad	.LC30
	.quad	570425454
	.quad	.LC31
	.quad	570425482
	.quad	.LC32
	.quad	570425478
	.quad	.LC33
	.quad	570425497
	.quad	.LC34
	.quad	570425490
	.quad	.LC35
	.quad	570425508
	.quad	.LC36
	.quad	570425479
	.quad	.LC37
	.quad	570425487
	.quad	.LC38
	.quad	570425470
	.quad	.LC39
	.quad	570425468
	.quad	.LC40
	.quad	570425486
	.quad	.LC41
	.quad	570425480
	.quad	.LC42
	.quad	570425465
	.quad	.LC43
	.quad	570425471
	.quad	.LC44
	.quad	570425483
	.quad	.LC45
	.quad	570425498
	.quad	.LC46
	.quad	570425458
	.quad	.LC47
	.quad	570425469
	.quad	.LC48
	.quad	570425492
	.quad	.LC49
	.quad	570425491
	.quad	.LC50
	.quad	570425499
	.quad	.LC51
	.quad	570425500
	.quad	.LC52
	.quad	570425501
	.quad	.LC53
	.quad	570425503
	.quad	.LC54
	.quad	570425494
	.quad	.LC55
	.quad	570425466
	.quad	.LC56
	.quad	570425467
	.quad	.LC57
	.quad	570425455
	.quad	.LC58
	.quad	570425473
	.quad	.LC59
	.quad	570425474
	.quad	.LC60
	.quad	570425464
	.quad	.LC61
	.quad	570425461
	.quad	.LC62
	.quad	570425511
	.quad	.LC63
	.quad	570425476
	.quad	.LC64
	.quad	0
	.quad	0
	.section	.rodata.str1.1
.LC65:
	.string	"a2i_GENERAL_NAME"
.LC66:
	.string	"addr_validate_path_internal"
.LC67:
	.string	"ASIdentifierChoice_canonize"
	.section	.rodata.str1.8
	.align 8
.LC68:
	.string	"ASIdentifierChoice_is_canonical"
	.section	.rodata.str1.1
.LC69:
	.string	"bignum_to_string"
.LC70:
	.string	"copy_email"
.LC71:
	.string	"copy_issuer"
.LC72:
	.string	"do_dirname"
.LC73:
	.string	"do_ext_i2d"
.LC74:
	.string	"do_ext_nconf"
.LC75:
	.string	"gnames_from_sectname"
.LC76:
	.string	"i2s_ASN1_ENUMERATED"
.LC77:
	.string	"i2s_ASN1_IA5STRING"
.LC78:
	.string	"i2s_ASN1_INTEGER"
.LC79:
	.string	"i2v_AUTHORITY_INFO_ACCESS"
.LC80:
	.string	"level_add_node"
.LC81:
	.string	"notice_section"
.LC82:
	.string	"nref_nos"
.LC83:
	.string	"policy_cache_create"
.LC84:
	.string	"policy_cache_new"
.LC85:
	.string	"policy_data_new"
.LC86:
	.string	"policy_section"
.LC87:
	.string	"process_pci_value"
.LC88:
	.string	"r2i_certpol"
.LC89:
	.string	"r2i_pci"
.LC90:
	.string	"s2i_ASN1_IA5STRING"
.LC91:
	.string	"s2i_ASN1_INTEGER"
.LC92:
	.string	"s2i_ASN1_OCTET_STRING"
.LC93:
	.string	"s2i_skey_id"
.LC94:
	.string	"set_dist_point_name"
.LC95:
	.string	"SXNET_add_id_asc"
.LC96:
	.string	"SXNET_add_id_INTEGER"
.LC97:
	.string	"SXNET_add_id_ulong"
.LC98:
	.string	"SXNET_get_id_asc"
.LC99:
	.string	"SXNET_get_id_ulong"
.LC100:
	.string	"tree_init"
.LC101:
	.string	"v2i_ASIdentifiers"
.LC102:
	.string	"v2i_ASN1_BIT_STRING"
.LC103:
	.string	"v2i_AUTHORITY_INFO_ACCESS"
.LC104:
	.string	"v2i_AUTHORITY_KEYID"
.LC105:
	.string	"v2i_BASIC_CONSTRAINTS"
.LC106:
	.string	"v2i_crld"
.LC107:
	.string	"v2i_EXTENDED_KEY_USAGE"
.LC108:
	.string	"v2i_GENERAL_NAMES"
.LC109:
	.string	"v2i_GENERAL_NAME_ex"
.LC110:
	.string	"v2i_idp"
.LC111:
	.string	"v2i_IPAddrBlocks"
.LC112:
	.string	"v2i_issuer_alt"
.LC113:
	.string	"v2i_NAME_CONSTRAINTS"
.LC114:
	.string	"v2i_POLICY_CONSTRAINTS"
.LC115:
	.string	"v2i_POLICY_MAPPINGS"
.LC116:
	.string	"v2i_subject_alt"
.LC117:
	.string	"v2i_TLS_FEATURE"
.LC118:
	.string	"v3_generic_extension"
.LC119:
	.string	"X509V3_add1_i2d"
.LC120:
	.string	"X509V3_add_value"
.LC121:
	.string	"X509V3_EXT_add"
.LC122:
	.string	"X509V3_EXT_add_alias"
.LC123:
	.string	"X509V3_EXT_i2d"
.LC124:
	.string	"X509V3_EXT_nconf"
.LC125:
	.string	"X509V3_get_section"
.LC126:
	.string	"X509V3_get_string"
.LC127:
	.string	"X509V3_get_value_bool"
.LC128:
	.string	"X509V3_parse_list"
.LC129:
	.string	"X509_PURPOSE_add"
.LC130:
	.string	"X509_PURPOSE_set"
	.section	.data.rel.ro.local
	.align 32
	.type	X509V3_str_functs, @object
	.size	X509V3_str_functs, 1072
X509V3_str_functs:
	.quad	571097088
	.quad	.LC65
	.quad	571105280
	.quad	.LC66
	.quad	571084800
	.quad	.LC67
	.quad	571088896
	.quad	.LC68
	.quad	571109376
	.quad	.LC69
	.quad	570925056
	.quad	.LC70
	.quad	570929152
	.quad	.LC71
	.quad	571015168
	.quad	.LC72
	.quad	570978304
	.quad	.LC73
	.quad	571043840
	.quad	.LC74
	.quad	571064320
	.quad	.LC75
	.quad	570920960
	.quad	.LC76
	.quad	571035648
	.quad	.LC77
	.quad	570916864
	.quad	.LC78
	.quad	570990592
	.quad	.LC79
	.quad	571113472
	.quad	.LC80
	.quad	570966016
	.quad	.LC81
	.quad	570970112
	.quad	.LC82
	.quad	571117568
	.quad	.LC83
	.quad	571121664
	.quad	.LC84
	.quad	571125760
	.quad	.LC85
	.quad	570961920
	.quad	.LC86
	.quad	571039744
	.quad	.LC87
	.quad	570957824
	.quad	.LC88
	.quad	571060224
	.quad	.LC89
	.quad	570834944
	.quad	.LC90
	.quad	570867712
	.quad	.LC91
	.quad	570884096
	.quad	.LC92
	.quad	570896384
	.quad	.LC93
	.quad	571072512
	.quad	.LC94
	.quad	570937344
	.quad	.LC95
	.quad	570941440
	.quad	.LC96
	.quad	570945536
	.quad	.LC97
	.quad	570949632
	.quad	.LC98
	.quad	570953728
	.quad	.LC99
	.quad	571129856
	.quad	.LC100
	.quad	571092992
	.quad	.LC101
	.quad	570839040
	.quad	.LC102
	.quad	570994688
	.quad	.LC103
	.quad	570912768
	.quad	.LC104
	.quad	570843136
	.quad	.LC105
	.quad	570974208
	.quad	.LC106
	.quad	570847232
	.quad	.LC107
	.quad	570908672
	.quad	.LC108
	.quad	570904576
	.quad	.LC109
	.quad	571068416
	.quad	.LC110
	.quad	571076608
	.quad	.LC111
	.quad	571052032
	.quad	.LC112
	.quad	571027456
	.quad	.LC113
	.quad	571023360
	.quad	.LC114
	.quad	571019264
	.quad	.LC115
	.quad	571056128
	.quad	.LC116
	.quad	571101184
	.quad	.LC117
	.quad	570900480
	.quad	.LC118
	.quad	570998784
	.quad	.LC119
	.quad	570855424
	.quad	.LC120
	.quad	570851328
	.quad	.LC121
	.quad	570859520
	.quad	.LC122
	.quad	570982400
	.quad	.LC123
	.quad	571047936
	.quad	.LC124
	.quad	571006976
	.quad	.LC125
	.quad	571011072
	.quad	.LC126
	.quad	570875904
	.quad	.LC127
	.quad	570871808
	.quad	.LC128
	.quad	570986496
	.quad	.LC129
	.quad	571002880
	.quad	.LC130
	.quad	0
	.quad	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
