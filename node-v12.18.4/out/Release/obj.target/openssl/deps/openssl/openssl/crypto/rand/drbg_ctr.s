	.file	"drbg_ctr.c"
	.text
	.p2align 4
	.type	drbg_ctr_uninstantiate, @function
drbg_ctr_uninstantiate:
.LFB394:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	168(%rdi), %rdi
	call	EVP_CIPHER_CTX_free@PLT
	movq	176(%rbx), %rdi
	call	EVP_CIPHER_CTX_free@PLT
	leaq	168(%rbx), %rdi
	movl	$152, %esi
	call	OPENSSL_cleanse@PLT
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE394:
	.size	drbg_ctr_uninstantiate, .-drbg_ctr_uninstantiate
	.p2align 4
	.type	ctr_BCC_blocks, @function
ctr_BCC_blocks:
.LFB385:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	leaq	104(%rdi), %rsi
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	16(%r12), %rax
	movl	$16, -44(%rbp)
	cmpq	%rsi, %rax
	jbe	.L21
	leaq	120(%rdi), %rax
	cmpq	%rax, %r12
	jb	.L5
.L21:
	movdqu	104(%rbx), %xmm0
	movdqu	(%r12), %xmm1
	pxor	%xmm1, %xmm0
	movups	%xmm0, 104(%rbx)
.L7:
	leaq	-44(%rbp), %r13
	movq	8(%rbx), %rdi
	movl	$16, %r8d
	movq	%rsi, %rcx
	movq	%r13, %rdx
	call	EVP_CipherUpdate@PLT
	testl	%eax, %eax
	je	.L17
	cmpl	$16, -44(%rbp)
	jne	.L17
	leaq	135(%rbx), %rax
	leaq	120(%rbx), %rsi
	subq	%r12, %rax
	cmpq	$30, %rax
	jbe	.L33
	movdqu	120(%rbx), %xmm0
	movdqu	(%r12), %xmm2
	pxor	%xmm2, %xmm0
	movups	%xmm0, 120(%rbx)
.L12:
	movq	8(%rbx), %rdi
	movl	$16, %r8d
	movq	%rsi, %rcx
	movq	%r13, %rdx
	call	EVP_CipherUpdate@PLT
	testl	%eax, %eax
	je	.L17
	cmpl	$16, -44(%rbp)
	je	.L14
	.p2align 4,,10
	.p2align 3
.L17:
	xorl	%eax, %eax
.L4:
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L34
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	.cfi_restore_state
	cmpq	$16, 24(%rbx)
	movl	$1, %eax
	je	.L4
	leaq	151(%rbx), %rax
	leaq	136(%rbx), %rsi
	subq	%r12, %rax
	cmpq	$30, %rax
	jbe	.L15
	movdqu	(%r12), %xmm0
	movdqu	136(%rbx), %xmm3
	pxor	%xmm3, %xmm0
	movups	%xmm0, 136(%rbx)
.L16:
	movq	8(%rbx), %rdi
	movl	$16, %r8d
	movq	%rsi, %rcx
	movq	%r13, %rdx
	call	EVP_CipherUpdate@PLT
	testl	%eax, %eax
	je	.L17
	cmpl	$16, -44(%rbp)
	movl	$1, %eax
	je	.L4
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L5:
	movzbl	(%r12), %eax
	xorb	%al, 104(%rdi)
	movzbl	1(%r12), %eax
	xorb	%al, 105(%rdi)
	movzbl	2(%r12), %eax
	xorb	%al, 106(%rdi)
	movzbl	3(%r12), %eax
	xorb	%al, 107(%rdi)
	movzbl	4(%r12), %eax
	xorb	%al, 108(%rdi)
	movzbl	5(%r12), %eax
	xorb	%al, 109(%rdi)
	movzbl	6(%r12), %eax
	xorb	%al, 110(%rdi)
	movzbl	7(%r12), %eax
	xorb	%al, 111(%rdi)
	movzbl	8(%r12), %eax
	xorb	%al, 112(%rdi)
	movzbl	9(%r12), %eax
	xorb	%al, 113(%rdi)
	movzbl	10(%r12), %eax
	xorb	%al, 114(%rdi)
	movzbl	11(%r12), %eax
	xorb	%al, 115(%rdi)
	movzbl	12(%r12), %eax
	xorb	%al, 116(%rdi)
	movzbl	13(%r12), %eax
	xorb	%al, 117(%rdi)
	movzbl	14(%r12), %eax
	xorb	%al, 118(%rdi)
	movzbl	15(%r12), %eax
	xorb	%al, 119(%rdi)
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L33:
	movzbl	(%r12), %eax
	xorb	%al, 120(%rbx)
	movzbl	1(%r12), %eax
	xorb	%al, 121(%rbx)
	movzbl	2(%r12), %eax
	xorb	%al, 122(%rbx)
	movzbl	3(%r12), %eax
	xorb	%al, 123(%rbx)
	movzbl	4(%r12), %eax
	xorb	%al, 124(%rbx)
	movzbl	5(%r12), %eax
	xorb	%al, 125(%rbx)
	movzbl	6(%r12), %eax
	xorb	%al, 126(%rbx)
	movzbl	7(%r12), %eax
	xorb	%al, 127(%rbx)
	movzbl	8(%r12), %eax
	xorb	%al, 128(%rbx)
	movzbl	9(%r12), %eax
	xorb	%al, 129(%rbx)
	movzbl	10(%r12), %eax
	xorb	%al, 130(%rbx)
	movzbl	11(%r12), %eax
	xorb	%al, 131(%rbx)
	movzbl	12(%r12), %eax
	xorb	%al, 132(%rbx)
	movzbl	13(%r12), %eax
	xorb	%al, 133(%rbx)
	movzbl	14(%r12), %eax
	xorb	%al, 134(%rbx)
	movzbl	15(%r12), %eax
	xorb	%al, 135(%rbx)
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L15:
	movzbl	(%r12), %eax
	xorb	%al, 136(%rbx)
	movzbl	1(%r12), %eax
	xorb	%al, 137(%rbx)
	movzbl	2(%r12), %eax
	xorb	%al, 138(%rbx)
	movzbl	3(%r12), %eax
	xorb	%al, 139(%rbx)
	movzbl	4(%r12), %eax
	xorb	%al, 140(%rbx)
	movzbl	5(%r12), %eax
	xorb	%al, 141(%rbx)
	movzbl	6(%r12), %eax
	xorb	%al, 142(%rbx)
	movzbl	7(%r12), %eax
	xorb	%al, 143(%rbx)
	movzbl	8(%r12), %eax
	xorb	%al, 144(%rbx)
	movzbl	9(%r12), %eax
	xorb	%al, 145(%rbx)
	movzbl	10(%r12), %eax
	xorb	%al, 146(%rbx)
	movzbl	11(%r12), %eax
	xorb	%al, 147(%rbx)
	movzbl	12(%r12), %eax
	xorb	%al, 148(%rbx)
	movzbl	13(%r12), %eax
	xorb	%al, 149(%rbx)
	movzbl	14(%r12), %eax
	xorb	%al, 150(%rbx)
	movzbl	15(%r12), %eax
	xorb	%al, 151(%rbx)
	jmp	.L16
.L34:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE385:
	.size	ctr_BCC_blocks, .-ctr_BCC_blocks
	.p2align 4
	.type	ctr_BCC_update.part.0, @function
ctr_BCC_update.part.0:
.LFB398:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	96(%rdi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%r14, %r14
	je	.L36
	movl	$16, %r15d
	subq	%r14, %r15
	cmpq	%rdx, %r15
	jbe	.L81
.L36:
	cmpq	$15, %r13
	jbe	.L38
	leaq	120(%rbx), %rax
	leaq	104(%rbx), %r15
	movq	%rax, -72(%rbp)
	leaq	-60(%rbp), %r14
	.p2align 4,,10
	.p2align 3
.L46:
	movzbl	(%r12), %eax
	xorb	%al, 104(%rbx)
	movq	%r15, %rcx
	movq	%r14, %rdx
	movzbl	1(%r12), %eax
	xorb	%al, 105(%rbx)
	movl	$16, %r8d
	movq	%r15, %rsi
	movzbl	2(%r12), %eax
	movl	$16, -60(%rbp)
	xorb	%al, 106(%rbx)
	movzbl	3(%r12), %eax
	xorb	%al, 107(%rbx)
	movzbl	4(%r12), %eax
	xorb	%al, 108(%rbx)
	movzbl	5(%r12), %eax
	xorb	%al, 109(%rbx)
	movzbl	6(%r12), %eax
	xorb	%al, 110(%rbx)
	movzbl	7(%r12), %eax
	xorb	%al, 111(%rbx)
	movzbl	8(%r12), %eax
	xorb	%al, 112(%rbx)
	movzbl	9(%r12), %eax
	xorb	%al, 113(%rbx)
	movzbl	10(%r12), %eax
	xorb	%al, 114(%rbx)
	movzbl	11(%r12), %eax
	xorb	%al, 115(%rbx)
	movzbl	12(%r12), %eax
	xorb	%al, 116(%rbx)
	movzbl	13(%r12), %eax
	xorb	%al, 117(%rbx)
	movzbl	14(%r12), %eax
	xorb	%al, 118(%rbx)
	movzbl	15(%r12), %eax
	xorb	%al, 119(%rbx)
	movq	8(%rbx), %rdi
	call	EVP_CipherUpdate@PLT
	testl	%eax, %eax
	je	.L44
	cmpl	$16, -60(%rbp)
	jne	.L44
	movzbl	(%r12), %eax
	xorb	%al, 120(%rbx)
	movl	$16, %r8d
	movq	%r14, %rdx
	movzbl	1(%r12), %eax
	xorb	%al, 121(%rbx)
	movzbl	2(%r12), %eax
	xorb	%al, 122(%rbx)
	movzbl	3(%r12), %eax
	xorb	%al, 123(%rbx)
	movzbl	4(%r12), %eax
	xorb	%al, 124(%rbx)
	movzbl	5(%r12), %eax
	xorb	%al, 125(%rbx)
	movzbl	6(%r12), %eax
	xorb	%al, 126(%rbx)
	movzbl	7(%r12), %eax
	xorb	%al, 127(%rbx)
	movzbl	8(%r12), %eax
	xorb	%al, 128(%rbx)
	movzbl	9(%r12), %eax
	xorb	%al, 129(%rbx)
	movzbl	10(%r12), %eax
	xorb	%al, 130(%rbx)
	movzbl	11(%r12), %eax
	xorb	%al, 131(%rbx)
	movzbl	12(%r12), %eax
	xorb	%al, 132(%rbx)
	movzbl	13(%r12), %eax
	xorb	%al, 133(%rbx)
	movzbl	14(%r12), %eax
	xorb	%al, 134(%rbx)
	movzbl	15(%r12), %eax
	xorb	%al, 135(%rbx)
	movq	-72(%rbp), %rcx
	movq	8(%rbx), %rdi
	movq	%rcx, %rsi
	call	EVP_CipherUpdate@PLT
	testl	%eax, %eax
	je	.L44
	cmpl	$16, -60(%rbp)
	jne	.L44
	cmpq	$16, 24(%rbx)
	je	.L43
	movzbl	(%r12), %eax
	xorb	%al, 136(%rbx)
	leaq	136(%rbx), %rsi
	movq	%r14, %rdx
	movzbl	1(%r12), %eax
	xorb	%al, 137(%rbx)
	movl	$16, %r8d
	movq	%rsi, %rcx
	movzbl	2(%r12), %eax
	xorb	%al, 138(%rbx)
	movzbl	3(%r12), %eax
	xorb	%al, 139(%rbx)
	movzbl	4(%r12), %eax
	xorb	%al, 140(%rbx)
	movzbl	5(%r12), %eax
	xorb	%al, 141(%rbx)
	movzbl	6(%r12), %eax
	xorb	%al, 142(%rbx)
	movzbl	7(%r12), %eax
	xorb	%al, 143(%rbx)
	movzbl	8(%r12), %eax
	xorb	%al, 144(%rbx)
	movzbl	9(%r12), %eax
	xorb	%al, 145(%rbx)
	movzbl	10(%r12), %eax
	xorb	%al, 146(%rbx)
	movzbl	11(%r12), %eax
	xorb	%al, 147(%rbx)
	movzbl	12(%r12), %eax
	xorb	%al, 148(%rbx)
	movzbl	13(%r12), %eax
	xorb	%al, 149(%rbx)
	movzbl	14(%r12), %eax
	xorb	%al, 150(%rbx)
	movzbl	15(%r12), %eax
	xorb	%al, 151(%rbx)
	movq	8(%rbx), %rdi
	call	EVP_CipherUpdate@PLT
	testl	%eax, %eax
	je	.L44
	cmpl	$16, -60(%rbp)
	je	.L43
	.p2align 4,,10
	.p2align 3
.L44:
	xorl	%eax, %eax
.L35:
	movq	-56(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L82
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L43:
	.cfi_restore_state
	subq	$16, %r13
	addq	$16, %r12
	cmpq	$15, %r13
	ja	.L46
.L38:
	movl	$1, %eax
	testq	%r13, %r13
	je	.L35
	movq	96(%rbx), %rax
	leaq	80(%rbx,%rax), %rcx
	movl	%r13d, %eax
	cmpl	$8, %r13d
	jnb	.L47
	testb	$4, %r13b
	jne	.L83
	testl	%r13d, %r13d
	je	.L48
	movzbl	(%r12), %edx
	movb	%dl, (%rcx)
	testb	$2, %al
	je	.L48
	movl	%r13d, %edx
	movzwl	-2(%r12,%rdx), %eax
	movw	%ax, -2(%rcx,%rdx)
	.p2align 4,,10
	.p2align 3
.L48:
	addq	%r13, 96(%rbx)
	movl	$1, %eax
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L81:
	leaq	80(%rdi), %r8
	movq	%r15, %rdx
	leaq	(%r8,%r14), %rdi
	movq	%r8, -72(%rbp)
	call	memcpy@PLT
	movq	-72(%rbp), %r8
	movq	%rbx, %rdi
	movq	%r8, %rsi
	call	ctr_BCC_blocks
	testl	%eax, %eax
	je	.L35
	movq	$0, 96(%rbx)
	leaq	-16(%r14,%r13), %r13
	addq	%r15, %r12
	jmp	.L36
.L47:
	movq	(%r12), %rax
	leaq	8(%rcx), %rsi
	andq	$-8, %rsi
	movq	%rax, (%rcx)
	movl	%r13d, %eax
	movq	-8(%r12,%rax), %rdx
	movq	%rdx, -8(%rcx,%rax)
	subq	%rsi, %rcx
	leal	0(%r13,%rcx), %eax
	subq	%rcx, %r12
	andl	$-8, %eax
	cmpl	$8, %eax
	jb	.L48
	andl	$-8, %eax
	xorl	%edx, %edx
.L51:
	movl	%edx, %ecx
	addl	$8, %edx
	movq	(%r12,%rcx), %rdi
	movq	%rdi, (%rsi,%rcx)
	cmpl	%eax, %edx
	jb	.L51
	jmp	.L48
.L83:
	movl	(%r12), %edx
	movl	%edx, (%rcx)
	movl	%r13d, %edx
	movl	-4(%r12,%rdx), %eax
	movl	%eax, -4(%rcx,%rdx)
	jmp	.L48
.L82:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE398:
	.size	ctr_BCC_update.part.0, .-ctr_BCC_update.part.0
	.p2align 4
	.type	ctr_update, @function
ctr_update:
.LFB390:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	200(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	232(%rdi), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-68(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	movq	%r14, %rcx
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$104, %rsp
	movq	%rsi, -88(%rbp)
	movq	%r15, %rsi
	movq	%rdx, -104(%rbp)
	movq	%r8, -96(%rbp)
	movl	$16, %r8d
	movq	%r9, -128(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	247(%rdi), %eax
	movl	$16, -68(%rbp)
	movl	%eax, %edx
	addl	$1, %eax
	addl	$1, %edx
	shrl	$8, %eax
	movb	%dl, 247(%rdi)
	movl	%eax, %edx
	movzbl	246(%rdi), %eax
	addl	%edx, %eax
	movb	%al, 246(%rdi)
	shrl	$8, %eax
	movl	%eax, %edx
	movzbl	245(%rdi), %eax
	addl	%edx, %eax
	movb	%al, 245(%rdi)
	shrl	$8, %eax
	movl	%eax, %edx
	movzbl	244(%rdi), %eax
	addl	%edx, %eax
	movb	%al, 244(%rdi)
	shrl	$8, %eax
	movl	%eax, %edx
	movzbl	243(%rdi), %eax
	addl	%edx, %eax
	movb	%al, 243(%rdi)
	shrl	$8, %eax
	movl	%eax, %edx
	movzbl	242(%rdi), %eax
	addl	%edx, %eax
	movb	%al, 242(%rdi)
	shrl	$8, %eax
	movl	%eax, %edx
	movzbl	241(%rdi), %eax
	addl	%edx, %eax
	movb	%al, 241(%rdi)
	shrl	$8, %eax
	movl	%eax, %edx
	movzbl	240(%rdi), %eax
	addl	%edx, %eax
	movb	%al, 240(%rdi)
	shrl	$8, %eax
	movl	%eax, %edx
	movzbl	239(%rdi), %eax
	addl	%edx, %eax
	movb	%al, 239(%rdi)
	shrl	$8, %eax
	movl	%eax, %edx
	movzbl	238(%rdi), %eax
	addl	%edx, %eax
	movb	%al, 238(%rdi)
	shrl	$8, %eax
	movl	%eax, %edx
	movzbl	237(%rdi), %eax
	addl	%edx, %eax
	movb	%al, 237(%rdi)
	shrl	$8, %eax
	movl	%eax, %edx
	movzbl	236(%rdi), %eax
	addl	%edx, %eax
	movb	%al, 236(%rdi)
	shrl	$8, %eax
	movl	%eax, %edx
	movzbl	235(%rdi), %eax
	addl	%edx, %eax
	movb	%al, 235(%rdi)
	shrl	$8, %eax
	movl	%eax, %edx
	movzbl	234(%rdi), %eax
	addl	%edx, %eax
	movb	%al, 234(%rdi)
	shrl	$8, %eax
	movl	%eax, %edx
	movzbl	233(%rdi), %eax
	addl	%edx, %eax
	movq	%r13, %rdx
	movb	%al, 233(%rdi)
	shrl	$8, %eax
	addb	%al, 232(%rdi)
	movq	168(%rdi), %rdi
	call	EVP_CipherUpdate@PLT
	testl	%eax, %eax
	je	.L97
	cmpl	$16, -68(%rbp)
	je	.L385
.L97:
	xorl	%eax, %eax
.L84:
	movq	-56(%rbp), %rsi
	xorq	%fs:40, %rsi
	jne	.L386
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L385:
	.cfi_restore_state
	cmpq	$16, 192(%rbx)
	je	.L90
	movzbl	247(%rbx), %eax
	leaq	216(%rbx), %rsi
	movl	$16, %r8d
	movq	%r13, %rdx
	movl	%eax, %ecx
	addl	$1, %eax
	addl	$1, %ecx
	shrl	$8, %eax
	movb	%cl, 247(%rbx)
	movl	%eax, %ecx
	movzbl	246(%rbx), %eax
	addl	%ecx, %eax
	movb	%al, 246(%rbx)
	shrl	$8, %eax
	movl	%eax, %ecx
	movzbl	245(%rbx), %eax
	addl	%ecx, %eax
	movb	%al, 245(%rbx)
	shrl	$8, %eax
	movl	%eax, %ecx
	movzbl	244(%rbx), %eax
	addl	%ecx, %eax
	movb	%al, 244(%rbx)
	shrl	$8, %eax
	movl	%eax, %ecx
	movzbl	243(%rbx), %eax
	addl	%ecx, %eax
	movb	%al, 243(%rbx)
	shrl	$8, %eax
	movl	%eax, %ecx
	movzbl	242(%rbx), %eax
	addl	%ecx, %eax
	movb	%al, 242(%rbx)
	shrl	$8, %eax
	movl	%eax, %ecx
	movzbl	241(%rbx), %eax
	addl	%ecx, %eax
	movb	%al, 241(%rbx)
	shrl	$8, %eax
	movl	%eax, %ecx
	movzbl	240(%rbx), %eax
	addl	%ecx, %eax
	movb	%al, 240(%rbx)
	shrl	$8, %eax
	movl	%eax, %ecx
	movzbl	239(%rbx), %eax
	addl	%ecx, %eax
	movb	%al, 239(%rbx)
	shrl	$8, %eax
	movl	%eax, %ecx
	movzbl	238(%rbx), %eax
	addl	%ecx, %eax
	movb	%al, 238(%rbx)
	shrl	$8, %eax
	movl	%eax, %ecx
	movzbl	237(%rbx), %eax
	addl	%ecx, %eax
	movb	%al, 237(%rbx)
	shrl	$8, %eax
	movl	%eax, %ecx
	movzbl	236(%rbx), %eax
	addl	%ecx, %eax
	movb	%al, 236(%rbx)
	shrl	$8, %eax
	movl	%eax, %ecx
	movzbl	235(%rbx), %eax
	addl	%ecx, %eax
	movb	%al, 235(%rbx)
	shrl	$8, %eax
	movl	%eax, %ecx
	movzbl	234(%rbx), %eax
	addl	%ecx, %eax
	movb	%al, 234(%rbx)
	shrl	$8, %eax
	movl	%eax, %ecx
	movzbl	233(%rbx), %eax
	addl	%ecx, %eax
	movq	%r14, %rcx
	movb	%al, 233(%rbx)
	shrl	$8, %eax
	addb	%al, 232(%rbx)
	movq	168(%rbx), %rdi
	call	EVP_CipherUpdate@PLT
	testl	%eax, %eax
	je	.L97
	cmpl	$16, -68(%rbp)
	jne	.L97
.L90:
	movzbl	247(%rbx), %eax
	movl	$16, %r8d
	movq	%r13, %rdx
	movq	%r14, %rsi
	movl	%eax, %ecx
	addl	$1, %eax
	addl	$1, %ecx
	shrl	$8, %eax
	movb	%cl, 247(%rbx)
	movl	%eax, %ecx
	movzbl	246(%rbx), %eax
	addl	%ecx, %eax
	movb	%al, 246(%rbx)
	shrl	$8, %eax
	movl	%eax, %ecx
	movzbl	245(%rbx), %eax
	addl	%ecx, %eax
	movb	%al, 245(%rbx)
	shrl	$8, %eax
	movl	%eax, %ecx
	movzbl	244(%rbx), %eax
	addl	%ecx, %eax
	movb	%al, 244(%rbx)
	shrl	$8, %eax
	movl	%eax, %ecx
	movzbl	243(%rbx), %eax
	addl	%ecx, %eax
	movb	%al, 243(%rbx)
	shrl	$8, %eax
	movl	%eax, %ecx
	movzbl	242(%rbx), %eax
	addl	%ecx, %eax
	movb	%al, 242(%rbx)
	shrl	$8, %eax
	movl	%eax, %ecx
	movzbl	241(%rbx), %eax
	addl	%ecx, %eax
	movb	%al, 241(%rbx)
	shrl	$8, %eax
	movl	%eax, %ecx
	movzbl	240(%rbx), %eax
	addl	%ecx, %eax
	movb	%al, 240(%rbx)
	shrl	$8, %eax
	movl	%eax, %ecx
	movzbl	239(%rbx), %eax
	addl	%ecx, %eax
	movb	%al, 239(%rbx)
	shrl	$8, %eax
	movl	%eax, %ecx
	movzbl	238(%rbx), %eax
	addl	%ecx, %eax
	movb	%al, 238(%rbx)
	shrl	$8, %eax
	movl	%eax, %ecx
	movzbl	237(%rbx), %eax
	addl	%ecx, %eax
	movb	%al, 237(%rbx)
	shrl	$8, %eax
	movl	%eax, %ecx
	movzbl	236(%rbx), %eax
	addl	%ecx, %eax
	movb	%al, 236(%rbx)
	shrl	$8, %eax
	movl	%eax, %ecx
	movzbl	235(%rbx), %eax
	addl	%ecx, %eax
	movb	%al, 235(%rbx)
	shrl	$8, %eax
	movl	%eax, %ecx
	movzbl	234(%rbx), %eax
	addl	%ecx, %eax
	movb	%al, 234(%rbx)
	shrl	$8, %eax
	movl	%eax, %ecx
	movzbl	233(%rbx), %eax
	addl	%ecx, %eax
	movq	%r14, %rcx
	movb	%al, 233(%rbx)
	shrl	$8, %eax
	addb	%al, 232(%rbx)
	movq	168(%rbx), %rdi
	call	EVP_CipherUpdate@PLT
	testl	%eax, %eax
	je	.L97
	cmpl	$16, -68(%rbp)
	jne	.L97
	movq	192(%rbx), %rax
	cmpq	$24, %rax
	je	.L387
.L91:
	testb	$1, 28(%rbx)
	jne	.L92
	movq	-128(%rbp), %rax
	orq	%r12, %rax
	orq	-88(%rbp), %rax
	je	.L93
	pxor	%xmm0, %xmm0
	leaq	272(%rbx), %rax
	leaq	-60(%rbp), %r9
	movq	176(%rbx), %rdi
	leaq	248(%rbx), %rsi
	movl	$16, %r8d
	movq	%rax, %rcx
	movups	%xmm0, 288(%rbx)
	movups	%xmm0, 304(%rbx)
	movq	%r9, %rdx
	movups	%xmm0, 248(%rbx)
	pxor	%xmm0, %xmm0
	movq	%rsi, -136(%rbp)
	movq	%rax, %rsi
	movups	%xmm0, 272(%rbx)
	movl	$16, -64(%rbp)
	movq	%rax, -112(%rbp)
	movl	$16, -60(%rbp)
	movq	%r9, -144(%rbp)
	call	EVP_CipherUpdate@PLT
	testl	%eax, %eax
	je	.L97
	cmpl	$16, -60(%rbp)
	jne	.L97
	movdqu	288(%rbx), %xmm2
	movb	$1, 251(%rbx)
	leaq	288(%rbx), %rsi
	movl	$16, %r8d
	movdqu	248(%rbx), %xmm0
	movq	-144(%rbp), %r9
	movq	%rsi, %rcx
	movq	%rsi, -136(%rbp)
	movq	176(%rbx), %rdi
	pxor	%xmm2, %xmm0
	movq	%r9, %rdx
	movups	%xmm0, 288(%rbx)
	call	EVP_CipherUpdate@PLT
	testl	%eax, %eax
	je	.L97
	cmpl	$16, -60(%rbp)
	jne	.L97
	movq	192(%rbx), %rdx
	cmpq	$16, %rdx
	je	.L99
	movdqu	304(%rbx), %xmm0
	movb	$2, 251(%rbx)
	leaq	304(%rbx), %rsi
	movl	$16, %r8d
	movdqu	248(%rbx), %xmm3
	movq	-144(%rbp), %r9
	movq	%rsi, %rcx
	movq	176(%rbx), %rdi
	pxor	%xmm3, %xmm0
	movq	%r9, %rdx
	movups	%xmm0, 304(%rbx)
	call	EVP_CipherUpdate@PLT
	testl	%eax, %eax
	je	.L97
	cmpl	$16, -60(%rbp)
	jne	.L97
	movq	192(%rbx), %rdx
.L99:
	cmpq	$0, -88(%rbp)
	je	.L148
	movq	-104(%rbp), %r8
	testq	%r8, %r8
	sete	%cl
.L102:
	cmpq	$0, -128(%rbp)
	movq	%r8, %rax
	je	.L149
	addq	16(%rbp), %rax
.L103:
	testq	%r12, %r12
	je	.L150
	addq	-96(%rbp), %rax
.L104:
	bswap	%eax
	addl	$16, %edx
	movl	%eax, 248(%rbx)
	xorl	%eax, %eax
	leaq	168(%rbx), %rdi
	movw	%ax, 252(%rbx)
	movb	$0, 254(%rbx)
	movq	%rdi, %r13
	movb	%dl, 255(%rbx)
	movq	$8, 264(%rbx)
	testb	%cl, %cl
	jne	.L105
	movq	-88(%rbp), %rsi
	movq	%r8, %rdx
	call	ctr_BCC_update.part.0
	testl	%eax, %eax
	je	.L97
.L105:
	cmpq	$0, -128(%rbp)
	je	.L110
	cmpq	$0, 16(%rbp)
	je	.L110
	movq	16(%rbp), %rdx
	movq	-128(%rbp), %rsi
	movq	%r13, %rdi
	call	ctr_BCC_update.part.0
	testl	%eax, %eax
	je	.L97
.L110:
	testq	%r12, %r12
	je	.L108
	cmpq	$0, -96(%rbp)
	je	.L108
	movq	-96(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	ctr_BCC_update.part.0
	testl	%eax, %eax
	je	.L97
.L108:
	movl	$1, %edx
	leaq	c80.11950(%rip), %rsi
	movq	%r13, %rdi
	call	ctr_BCC_update.part.0
	testl	%eax, %eax
	je	.L97
	movq	264(%rbx), %rax
	testq	%rax, %rax
	jne	.L388
.L111:
	movq	-112(%rbp), %r12
	movq	184(%rbx), %rsi
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movq	168(%rbx), %rdi
	movl	$1, %r9d
	movq	%r12, %rcx
	call	EVP_CipherInit_ex@PLT
	testl	%eax, %eax
	je	.L97
	movq	192(%rbx), %rcx
	movq	%r12, %rsi
	movq	168(%rbx), %rdi
	movl	$16, %r8d
	addq	%r12, %rcx
	leaq	-64(%rbp), %r12
	movq	%r12, %rdx
	call	EVP_CipherUpdate@PLT
	testl	%eax, %eax
	je	.L97
	cmpl	$16, -64(%rbp)
	jne	.L97
	movq	-136(%rbp), %r13
	movq	168(%rbx), %rdi
	movl	$16, %r8d
	movq	%r12, %rdx
	movq	-112(%rbp), %rcx
	movq	%r13, %rsi
	call	EVP_CipherUpdate@PLT
	testl	%eax, %eax
	je	.L97
	cmpl	$16, -64(%rbp)
	jne	.L97
	cmpq	$16, 192(%rbx)
	je	.L93
	movq	168(%rbx), %rdi
	leaq	304(%rbx), %rsi
	movq	%r13, %rcx
	movq	%r12, %rdx
	movl	$16, %r8d
	call	EVP_CipherUpdate@PLT
	testl	%eax, %eax
	je	.L97
	cmpl	$16, -64(%rbp)
	jne	.L97
.L93:
	cmpq	$0, -104(%rbp)
	je	.L114
	movq	144(%rbx), %rcx
	testq	%rcx, %rcx
	je	.L114
	movq	192(%rbx), %rsi
	leaq	272(%rbx), %rdi
	cmpq	%rsi, %rcx
	movq	%rsi, %rax
	cmovbe	%rcx, %rax
	testq	%rax, %rax
	je	.L119
	leaq	216(%rbx), %rdx
	cmpq	%rdx, %rdi
	leaq	288(%rbx), %rdx
	setnb	%r8b
	cmpq	%rdx, %r15
	setnb	%dl
	orb	%dl, %r8b
	je	.L117
	leaq	-1(%rax), %rdx
	cmpq	$14, %rdx
	jbe	.L117
	movdqu	200(%rbx), %xmm0
	movdqu	272(%rbx), %xmm4
	movq	%rax, %rdx
	shrq	$4, %rdx
	pxor	%xmm4, %xmm0
	movups	%xmm0, 200(%rbx)
	cmpq	$1, %rdx
	je	.L118
	movdqu	216(%rbx), %xmm5
	movdqu	288(%rbx), %xmm0
	pxor	%xmm5, %xmm0
	movups	%xmm0, 216(%rbx)
.L118:
	movq	%rax, %rdx
	andq	$-16, %rdx
	testb	$15, %al
	je	.L119
	movzbl	272(%rbx,%rdx), %r8d
	xorb	%r8b, 200(%rbx,%rdx)
	leaq	1(%rdx), %r8
	cmpq	%r8, %rax
	jbe	.L119
	movzbl	273(%rbx,%rdx), %r9d
	xorb	%r9b, 200(%rbx,%r8)
	leaq	2(%rdx), %r8
	cmpq	%r8, %rax
	jbe	.L119
	movzbl	274(%rbx,%rdx), %r9d
	xorb	%r9b, 200(%rbx,%r8)
	leaq	3(%rdx), %r8
	cmpq	%r8, %rax
	jbe	.L119
	movzbl	275(%rbx,%rdx), %r9d
	xorb	%r9b, 200(%rbx,%r8)
	leaq	4(%rdx), %r8
	cmpq	%r8, %rax
	jbe	.L119
	movzbl	276(%rbx,%rdx), %r9d
	xorb	%r9b, 200(%rbx,%r8)
	leaq	5(%rdx), %r8
	cmpq	%r8, %rax
	jbe	.L119
	movzbl	277(%rbx,%rdx), %r9d
	xorb	%r9b, 200(%rbx,%r8)
	leaq	6(%rdx), %r8
	cmpq	%r8, %rax
	jbe	.L119
	movzbl	278(%rbx,%rdx), %r9d
	xorb	%r9b, 200(%rbx,%r8)
	leaq	7(%rdx), %r8
	cmpq	%r8, %rax
	jbe	.L119
	movzbl	279(%rbx,%rdx), %r9d
	xorb	%r9b, 200(%rbx,%r8)
	leaq	8(%rdx), %r8
	cmpq	%r8, %rax
	jbe	.L119
	movzbl	280(%rbx,%rdx), %r9d
	xorb	%r9b, 200(%rbx,%r8)
	leaq	9(%rdx), %r8
	cmpq	%r8, %rax
	jbe	.L119
	movzbl	281(%rbx,%rdx), %r9d
	xorb	%r9b, 200(%rbx,%r8)
	leaq	10(%rdx), %r8
	cmpq	%r8, %rax
	jbe	.L119
	movzbl	282(%rbx,%rdx), %r9d
	xorb	%r9b, 200(%rbx,%r8)
	leaq	11(%rdx), %r8
	cmpq	%r8, %rax
	jbe	.L119
	movzbl	283(%rbx,%rdx), %r9d
	xorb	%r9b, 200(%rbx,%r8)
	leaq	12(%rdx), %r8
	cmpq	%r8, %rax
	jbe	.L119
	movzbl	284(%rbx,%rdx), %r9d
	xorb	%r9b, 200(%rbx,%r8)
	leaq	13(%rdx), %r8
	cmpq	%r8, %rax
	jbe	.L119
	movzbl	285(%rbx,%rdx), %r9d
	addq	$14, %rdx
	xorb	%r9b, 200(%rbx,%r8)
	cmpq	%rdx, %rax
	jbe	.L119
	movzbl	272(%rbx,%rdx), %eax
	xorb	%al, 200(%rbx,%rdx)
.L119:
	cmpq	%rsi, %rcx
	jbe	.L114
	movq	%rcx, %rax
	leaq	288(%rbx,%rsi), %rdx
	leaq	272(%rbx,%rsi), %rcx
	subq	%rsi, %rax
	cmpq	%rdx, %r14
	leaq	248(%rbx), %rdx
	setnb	%r8b
	cmpq	%rdx, %rcx
	setnb	%dl
	orb	%dl, %r8b
	je	.L123
	cmpq	$16, %rax
	movl	$16, %edx
	cmovbe	%rax, %rdx
	subq	$1, %rdx
	cmpq	$14, %rdx
	jbe	.L123
	movdqu	(%rcx), %xmm0
	movdqu	232(%rbx), %xmm6
	pxor	%xmm6, %xmm0
	movups	%xmm0, 232(%rbx)
	jmp	.L114
.L92:
	cmpq	$0, -88(%rbp)
	je	.L129
	cmpq	$0, -104(%rbp)
	je	.L129
	movq	-104(%rbp), %rsi
	cmpq	%rsi, %rax
	cmovbe	%rax, %rsi
	movq	%rsi, %rcx
	testq	%rsi, %rsi
	je	.L133
	movq	-88(%rbp), %rdi
	leaq	16(%rdi), %rdx
	cmpq	%r15, %rdx
	leaq	216(%rbx), %rdx
	setbe	%sil
	cmpq	%rdx, %rdi
	setnb	%dl
	orb	%dl, %sil
	je	.L151
	leaq	-1(%rcx), %rdx
	cmpq	$14, %rdx
	jbe	.L151
	movdqu	(%rdi), %xmm7
	movdqu	200(%rbx), %xmm0
	movq	%rcx, %rdx
	shrq	$4, %rdx
	pxor	%xmm7, %xmm0
	movaps	%xmm7, -128(%rbp)
	movups	%xmm0, 200(%rbx)
	cmpq	$1, %rdx
	je	.L132
	movdqu	16(%rdi), %xmm2
	movdqu	216(%rbx), %xmm0
	pxor	%xmm2, %xmm0
	movaps	%xmm2, -128(%rbp)
	movups	%xmm0, 216(%rbx)
.L132:
	movq	%rcx, %rdx
	andq	$-16, %rdx
	testb	$15, %cl
	je	.L133
	movq	-88(%rbp), %r10
	movzbl	(%r10,%rdx), %esi
	xorb	%sil, 200(%rbx,%rdx)
	leaq	1(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L133
	movzbl	1(%r10,%rdx), %edi
	xorb	%dil, 200(%rbx,%rsi)
	leaq	2(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L133
	movzbl	2(%r10,%rdx), %edi
	xorb	%dil, 200(%rbx,%rsi)
	leaq	3(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L133
	movzbl	3(%r10,%rdx), %edi
	xorb	%dil, 200(%rbx,%rsi)
	leaq	4(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L133
	movzbl	4(%r10,%rdx), %edi
	xorb	%dil, 200(%rbx,%rsi)
	leaq	5(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L133
	movzbl	5(%r10,%rdx), %edi
	xorb	%dil, 200(%rbx,%rsi)
	leaq	6(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L133
	movzbl	6(%r10,%rdx), %edi
	xorb	%dil, 200(%rbx,%rsi)
	leaq	7(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L133
	movzbl	7(%r10,%rdx), %edi
	xorb	%dil, 200(%rbx,%rsi)
	leaq	8(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L133
	movzbl	8(%r10,%rdx), %edi
	xorb	%dil, 200(%rbx,%rsi)
	leaq	9(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L133
	movzbl	9(%r10,%rdx), %edi
	xorb	%dil, 200(%rbx,%rsi)
	leaq	10(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L133
	movzbl	10(%r10,%rdx), %edi
	xorb	%dil, 200(%rbx,%rsi)
	leaq	11(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L133
	movzbl	11(%r10,%rdx), %edi
	xorb	%dil, 200(%rbx,%rsi)
	leaq	12(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L133
	movzbl	12(%r10,%rdx), %edi
	xorb	%dil, 200(%rbx,%rsi)
	leaq	13(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L133
	movzbl	13(%r10,%rdx), %edi
	xorb	%dil, 200(%rbx,%rsi)
	leaq	14(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L133
	movzbl	14(%r10,%rdx), %edx
	xorb	%dl, 200(%rbx,%rsi)
.L133:
	cmpq	-104(%rbp), %rax
	jnb	.L129
	movq	-88(%rbp), %rsi
	movq	-104(%rbp), %rdi
	leaq	16(%rsi,%rax), %rdx
	leaq	(%rsi,%rax), %rcx
	subq	%rax, %rdi
	cmpq	%r14, %rdx
	leaq	248(%rbx), %rdx
	setbe	%sil
	cmpq	%rdx, %rcx
	setnb	%dl
	orb	%dl, %sil
	je	.L136
	cmpq	$16, %rdi
	movl	$16, %edx
	cmovbe	%rdi, %rdx
	subq	$1, %rdx
	cmpq	$14, %rdx
	jbe	.L136
	movdqu	(%rcx), %xmm0
	movdqu	232(%rbx), %xmm4
	pxor	%xmm4, %xmm0
	movups	%xmm0, 232(%rbx)
.L129:
	testq	%r12, %r12
	je	.L114
	cmpq	$0, -96(%rbp)
	je	.L114
	movq	-96(%rbp), %rdi
	cmpq	%rdi, %rax
	cmovbe	%rax, %rdi
	movq	%rdi, %rcx
	testq	%rdi, %rdi
	je	.L142
	leaq	16(%r12), %rdx
	cmpq	%r15, %rdx
	leaq	216(%rbx), %rdx
	setbe	%sil
	cmpq	%rdx, %r12
	setnb	%dl
	orb	%dl, %sil
	je	.L152
	leaq	-1(%rdi), %rdx
	cmpq	$14, %rdx
	jbe	.L152
	movdqu	200(%rbx), %xmm0
	movdqu	(%r12), %xmm5
	movq	%rdi, %rdx
	shrq	$4, %rdx
	pxor	%xmm5, %xmm0
	movups	%xmm0, 200(%rbx)
	cmpq	$1, %rdx
	je	.L141
	movdqu	216(%rbx), %xmm0
	movdqu	16(%r12), %xmm6
	pxor	%xmm6, %xmm0
	movups	%xmm0, 216(%rbx)
.L141:
	movq	%rcx, %rdx
	andq	$-16, %rdx
	testb	$15, %cl
	je	.L142
	movzbl	(%r12,%rdx), %esi
	xorb	%sil, 200(%rbx,%rdx)
	leaq	1(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L142
	movzbl	1(%r12,%rdx), %edi
	xorb	%dil, 200(%rbx,%rsi)
	leaq	2(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L142
	movzbl	2(%r12,%rdx), %edi
	xorb	%dil, 200(%rbx,%rsi)
	leaq	3(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L142
	movzbl	3(%r12,%rdx), %edi
	xorb	%dil, 200(%rbx,%rsi)
	leaq	4(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L142
	movzbl	4(%r12,%rdx), %edi
	xorb	%dil, 200(%rbx,%rsi)
	leaq	5(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L142
	movzbl	5(%r12,%rdx), %edi
	xorb	%dil, 200(%rbx,%rsi)
	leaq	6(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L142
	movzbl	6(%r12,%rdx), %edi
	xorb	%dil, 200(%rbx,%rsi)
	leaq	7(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L142
	movzbl	7(%r12,%rdx), %edi
	xorb	%dil, 200(%rbx,%rsi)
	leaq	8(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L142
	movzbl	8(%r12,%rdx), %edi
	xorb	%dil, 200(%rbx,%rsi)
	leaq	9(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L142
	movzbl	9(%r12,%rdx), %edi
	xorb	%dil, 200(%rbx,%rsi)
	leaq	10(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L142
	movzbl	10(%r12,%rdx), %edi
	xorb	%dil, 200(%rbx,%rsi)
	leaq	11(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L142
	movzbl	11(%r12,%rdx), %edi
	xorb	%dil, 200(%rbx,%rsi)
	leaq	12(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L142
	movzbl	12(%r12,%rdx), %edi
	xorb	%dil, 200(%rbx,%rsi)
	leaq	13(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L142
	movzbl	13(%r12,%rdx), %edi
	xorb	%dil, 200(%rbx,%rsi)
	leaq	14(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L142
	movzbl	14(%r12,%rdx), %edx
	xorb	%dl, 200(%rbx,%rsi)
.L142:
	cmpq	-96(%rbp), %rax
	jnb	.L114
	movq	-96(%rbp), %rdi
	leaq	16(%r12,%rax), %rdx
	leaq	(%r12,%rax), %rcx
	subq	%rax, %rdi
	cmpq	%r14, %rdx
	leaq	248(%rbx), %rdx
	setbe	%sil
	cmpq	%rdx, %rcx
	setnb	%dl
	orb	%dl, %sil
	je	.L145
	cmpq	$16, %rdi
	movl	$16, %edx
	cmovbe	%rdi, %rdx
	subq	$1, %rdx
	cmpq	$14, %rdx
	jbe	.L145
	movdqu	(%rcx), %xmm0
	movdqu	232(%rbx), %xmm7
	pxor	%xmm7, %xmm0
	movups	%xmm0, 232(%rbx)
.L114:
	movq	184(%rbx), %rsi
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movq	%r15, %rcx
	movq	168(%rbx), %rdi
	movl	$1, %r9d
	call	EVP_CipherInit_ex@PLT
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
	jmp	.L84
.L387:
	movdqu	224(%rbx), %xmm1
	movups	%xmm1, 232(%rbx)
	jmp	.L91
.L148:
	movl	$1, %ecx
	xorl	%r8d, %r8d
	jmp	.L102
.L150:
	movq	$0, -96(%rbp)
	jmp	.L104
.L149:
	movq	$0, 16(%rbp)
	jmp	.L103
.L152:
	xorl	%edx, %edx
.L140:
	movzbl	(%r12,%rdx), %esi
	xorb	%sil, 200(%rbx,%rdx)
	addq	$1, %rdx
	cmpq	%rdx, %rcx
	jne	.L140
	jmp	.L142
.L145:
	movzbl	(%rcx), %edx
	xorb	%dl, 232(%rbx)
	cmpq	$1, %rdi
	je	.L114
	movzbl	1(%r12,%rax), %edx
	xorb	%dl, 233(%rbx)
	cmpq	$2, %rdi
	je	.L114
	movzbl	2(%r12,%rax), %edx
	xorb	%dl, 234(%rbx)
	cmpq	$3, %rdi
	je	.L114
	movzbl	3(%r12,%rax), %edx
	xorb	%dl, 235(%rbx)
	cmpq	$4, %rdi
	je	.L114
	movzbl	4(%r12,%rax), %edx
	xorb	%dl, 236(%rbx)
	cmpq	$5, %rdi
	je	.L114
	movzbl	5(%r12,%rax), %edx
	xorb	%dl, 237(%rbx)
	cmpq	$6, %rdi
	je	.L114
	movzbl	6(%r12,%rax), %edx
	xorb	%dl, 238(%rbx)
	cmpq	$7, %rdi
	je	.L114
	movzbl	7(%r12,%rax), %edx
	xorb	%dl, 239(%rbx)
	cmpq	$8, %rdi
	je	.L114
	movzbl	8(%r12,%rax), %edx
	xorb	%dl, 240(%rbx)
	cmpq	$9, %rdi
	je	.L114
	movzbl	9(%r12,%rax), %edx
	xorb	%dl, 241(%rbx)
	cmpq	$10, %rdi
	je	.L114
	movzbl	10(%r12,%rax), %edx
	xorb	%dl, 242(%rbx)
	cmpq	$11, %rdi
	je	.L114
	movzbl	11(%r12,%rax), %edx
	xorb	%dl, 243(%rbx)
	cmpq	$12, %rdi
	je	.L114
	movzbl	12(%r12,%rax), %edx
	xorb	%dl, 244(%rbx)
	cmpq	$13, %rdi
	je	.L114
	movzbl	13(%r12,%rax), %edx
	xorb	%dl, 245(%rbx)
	cmpq	$14, %rdi
	je	.L114
	movzbl	14(%r12,%rax), %edx
	xorb	%dl, 246(%rbx)
	cmpq	$15, %rdi
	jbe	.L114
	movzbl	15(%r12,%rax), %eax
	xorb	%al, 247(%rbx)
	jmp	.L114
.L136:
	movzbl	(%rcx), %edx
	xorb	%dl, 232(%rbx)
	cmpq	$1, %rdi
	je	.L129
	movq	-88(%rbp), %rsi
	movzbl	1(%rsi,%rax), %edx
	xorb	%dl, 233(%rbx)
	cmpq	$2, %rdi
	je	.L129
	movzbl	2(%rsi,%rax), %edx
	xorb	%dl, 234(%rbx)
	cmpq	$3, %rdi
	je	.L129
	movzbl	3(%rsi,%rax), %edx
	xorb	%dl, 235(%rbx)
	cmpq	$4, %rdi
	je	.L129
	movzbl	4(%rsi,%rax), %edx
	xorb	%dl, 236(%rbx)
	cmpq	$5, %rdi
	je	.L129
	movzbl	5(%rsi,%rax), %edx
	xorb	%dl, 237(%rbx)
	cmpq	$6, %rdi
	je	.L129
	movzbl	6(%rsi,%rax), %edx
	xorb	%dl, 238(%rbx)
	cmpq	$7, %rdi
	je	.L129
	movzbl	7(%rsi,%rax), %edx
	xorb	%dl, 239(%rbx)
	cmpq	$8, %rdi
	je	.L129
	movzbl	8(%rsi,%rax), %edx
	xorb	%dl, 240(%rbx)
	cmpq	$9, %rdi
	je	.L129
	movzbl	9(%rsi,%rax), %edx
	xorb	%dl, 241(%rbx)
	cmpq	$10, %rdi
	je	.L129
	movzbl	10(%rsi,%rax), %edx
	xorb	%dl, 242(%rbx)
	cmpq	$11, %rdi
	je	.L129
	movzbl	11(%rsi,%rax), %edx
	xorb	%dl, 243(%rbx)
	cmpq	$12, %rdi
	je	.L129
	movzbl	12(%rsi,%rax), %edx
	xorb	%dl, 244(%rbx)
	cmpq	$13, %rdi
	je	.L129
	movzbl	13(%rsi,%rax), %edx
	xorb	%dl, 245(%rbx)
	cmpq	$14, %rdi
	je	.L129
	movzbl	14(%rsi,%rax), %edx
	xorb	%dl, 246(%rbx)
	cmpq	$15, %rdi
	jbe	.L129
	movzbl	15(%rsi,%rax), %edx
	xorb	%dl, 247(%rbx)
	jmp	.L129
.L151:
	movq	-88(%rbp), %rdi
	xorl	%edx, %edx
.L131:
	movzbl	(%rdi,%rdx), %esi
	xorb	%sil, 200(%rbx,%rdx)
	addq	$1, %rdx
	cmpq	%rdx, %rcx
	jne	.L131
	jmp	.L133
.L117:
	movq	%r15, %rdx
	addq	%r15, %rax
.L121:
	movzbl	72(%rdx), %r8d
	xorb	%r8b, (%rdx)
	addq	$1, %rdx
	cmpq	%rax, %rdx
	jne	.L121
	jmp	.L119
.L388:
	leaq	248(%rbx), %r12
	movl	$16, %edx
	xorl	%esi, %esi
	subq	%rax, %rdx
	leaq	(%r12,%rax), %rdi
	call	memset@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	ctr_BCC_blocks
	testl	%eax, %eax
	jne	.L111
	jmp	.L97
.L123:
	movzbl	272(%rbx,%rsi), %edx
	xorb	%dl, 232(%rbx)
	cmpq	$1, %rax
	je	.L114
	movzbl	1(%rdi,%rsi), %edx
	xorb	%dl, 233(%rbx)
	cmpq	$2, %rax
	je	.L114
	movzbl	2(%rdi,%rsi), %edx
	xorb	%dl, 234(%rbx)
	cmpq	$3, %rax
	je	.L114
	movzbl	3(%rdi,%rsi), %edx
	xorb	%dl, 235(%rbx)
	cmpq	$4, %rax
	je	.L114
	movzbl	4(%rdi,%rsi), %edx
	xorb	%dl, 236(%rbx)
	cmpq	$5, %rax
	je	.L114
	movzbl	5(%rdi,%rsi), %edx
	xorb	%dl, 237(%rbx)
	cmpq	$6, %rax
	je	.L114
	movzbl	6(%rdi,%rsi), %edx
	xorb	%dl, 238(%rbx)
	cmpq	$7, %rax
	je	.L114
	movzbl	7(%rdi,%rsi), %edx
	xorb	%dl, 239(%rbx)
	cmpq	$8, %rax
	je	.L114
	movzbl	8(%rdi,%rsi), %edx
	xorb	%dl, 240(%rbx)
	cmpq	$9, %rax
	je	.L114
	movzbl	9(%rdi,%rsi), %edx
	xorb	%dl, 241(%rbx)
	cmpq	$10, %rax
	je	.L114
	movzbl	10(%rdi,%rsi), %edx
	xorb	%dl, 242(%rbx)
	cmpq	$11, %rax
	je	.L114
	movzbl	11(%rdi,%rsi), %edx
	xorb	%dl, 243(%rbx)
	cmpq	$12, %rax
	je	.L114
	movzbl	12(%rdi,%rsi), %edx
	xorb	%dl, 244(%rbx)
	cmpq	$13, %rax
	je	.L114
	movzbl	13(%rdi,%rsi), %edx
	xorb	%dl, 245(%rbx)
	cmpq	$14, %rax
	je	.L114
	movzbl	14(%rdi,%rsi), %edx
	xorb	%dl, 246(%rbx)
	cmpq	$15, %rax
	jbe	.L114
	movzbl	15(%rdi,%rsi), %eax
	xorb	%al, 247(%rbx)
	jmp	.L114
.L386:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE390:
	.size	ctr_update, .-ctr_update
	.p2align 4
	.type	drbg_ctr_reseed, @function
drbg_ctr_reseed:
.LFB392:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L393
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r9d, %r9d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$8, %rsp
	pushq	$0
	call	ctr_update
	popq	%rdx
	popq	%rcx
	leave
	.cfi_def_cfa 7, 8
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L393:
	.cfi_restore 6
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE392:
	.size	drbg_ctr_reseed, .-drbg_ctr_reseed
	.p2align 4
	.type	drbg_ctr_instantiate, @function
drbg_ctr_instantiate:
.LFB391:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L408
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	leaq	200(%rdi), %rcx
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	xorl	%edx, %edx
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	xorl	%r8d, %r8d
	subq	$24, %rsp
	movups	%xmm0, 200(%rdi)
	movups	%xmm0, 16(%rcx)
	movq	184(%rdi), %rsi
	movups	%xmm0, 232(%rdi)
	movq	168(%rdi), %rdi
	movq	%r9, -56(%rbp)
	movl	$1, %r9d
	call	EVP_CipherInit_ex@PLT
	testl	%eax, %eax
	jne	.L409
	leaq	-40(%rbp), %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L409:
	.cfi_restore_state
	subq	$8, %rsp
	movq	-56(%rbp), %r10
	movq	16(%rbp), %r8
	movq	%r14, %rdx
	pushq	%rbx
	movq	%r15, %r9
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r10, %rcx
	call	ctr_update
	popq	%rdx
	popq	%rcx
	testl	%eax, %eax
	setne	%al
	leaq	-40(%rbp), %rsp
	popq	%rbx
	movzbl	%al, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L408:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE391:
	.size	drbg_ctr_instantiate, .-drbg_ctr_instantiate
	.p2align 4
	.type	drbg_ctr_generate, @function
drbg_ctr_generate:
.LFB393:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$56, %rsp
	movq	%rcx, -80(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rcx, %rcx
	je	.L424
	movq	%r8, %r13
	testq	%r8, %r8
	jne	.L451
.L424:
	movq	$0, -88(%rbp)
.L411:
	movq	%r15, %rax
	leaq	232(%r12), %r13
	leaq	-60(%rbp), %r14
	addq	%r15, %rbx
	andl	$15, %eax
	movq	%rax, -72(%rbp)
	jmp	.L422
	.p2align 4,,10
	.p2align 3
.L413:
	movl	$16, %r8d
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	%r10, %rsi
	call	EVP_CipherUpdate@PLT
	testl	%eax, %eax
	je	.L414
	cmpl	$16, -60(%rbp)
	jne	.L414
	subq	$16, %r15
	je	.L421
.L422:
	movzbl	247(%r12), %eax
	movq	%rbx, %r10
	movl	$16, -60(%rbp)
	subq	%r15, %r10
	movl	%eax, %esi
	addl	$1, %eax
	addl	$1, %esi
	shrl	$8, %eax
	movb	%sil, 247(%r12)
	movl	%eax, %esi
	movzbl	246(%r12), %eax
	addl	%esi, %eax
	movb	%al, 246(%r12)
	shrl	$8, %eax
	movl	%eax, %esi
	movzbl	245(%r12), %eax
	addl	%esi, %eax
	movb	%al, 245(%r12)
	shrl	$8, %eax
	movl	%eax, %esi
	movzbl	244(%r12), %eax
	addl	%esi, %eax
	movb	%al, 244(%r12)
	shrl	$8, %eax
	movl	%eax, %esi
	movzbl	243(%r12), %eax
	addl	%esi, %eax
	movb	%al, 243(%r12)
	shrl	$8, %eax
	movl	%eax, %esi
	movzbl	242(%r12), %eax
	addl	%esi, %eax
	movb	%al, 242(%r12)
	shrl	$8, %eax
	movl	%eax, %esi
	movzbl	241(%r12), %eax
	addl	%esi, %eax
	movb	%al, 241(%r12)
	shrl	$8, %eax
	movl	%eax, %esi
	movzbl	240(%r12), %eax
	addl	%esi, %eax
	movb	%al, 240(%r12)
	shrl	$8, %eax
	movl	%eax, %esi
	movzbl	239(%r12), %eax
	addl	%esi, %eax
	movb	%al, 239(%r12)
	shrl	$8, %eax
	movl	%eax, %esi
	movzbl	238(%r12), %eax
	addl	%esi, %eax
	movb	%al, 238(%r12)
	shrl	$8, %eax
	movl	%eax, %esi
	movzbl	237(%r12), %eax
	addl	%esi, %eax
	movb	%al, 237(%r12)
	shrl	$8, %eax
	movl	%eax, %esi
	movzbl	236(%r12), %eax
	addl	%esi, %eax
	movb	%al, 236(%r12)
	shrl	$8, %eax
	movl	%eax, %esi
	movzbl	235(%r12), %eax
	addl	%esi, %eax
	movb	%al, 235(%r12)
	shrl	$8, %eax
	movl	%eax, %esi
	movzbl	234(%r12), %eax
	addl	%esi, %eax
	movb	%al, 234(%r12)
	shrl	$8, %eax
	movl	%eax, %esi
	movzbl	233(%r12), %eax
	addl	%esi, %eax
	movb	%al, 233(%r12)
	shrl	$8, %eax
	addb	%al, 232(%r12)
	movq	168(%r12), %rdi
	cmpq	-72(%rbp), %r15
	jne	.L413
	movl	$16, %r8d
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	%r10, -72(%rbp)
	leaq	200(%r12), %rbx
	movq	%rbx, %rsi
	call	EVP_CipherUpdate@PLT
	testl	%eax, %eax
	je	.L414
	cmpl	$16, -60(%rbp)
	movq	-72(%rbp), %r10
	jne	.L414
	movl	%r15d, %eax
	cmpl	$8, %r15d
	jnb	.L415
	andl	$4, %r15d
	jne	.L452
	testl	%eax, %eax
	je	.L421
	movzbl	200(%r12), %edx
	movb	%dl, (%r10)
	testb	$2, %al
	je	.L421
	movzwl	-2(%rbx,%rax), %edx
	movw	%dx, -2(%r10,%rax)
.L421:
	subq	$8, %rsp
	movq	-88(%rbp), %rdx
	movq	-80(%rbp), %rsi
	xorl	%ecx, %ecx
	pushq	$0
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	call	ctr_update
	popq	%rdx
	popq	%rcx
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
	jmp	.L410
.L414:
	xorl	%eax, %eax
.L410:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L453
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L451:
	.cfi_restore_state
	subq	$8, %rsp
	movq	%rcx, %r14
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	movq	%r14, %rsi
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	call	ctr_update
	popq	%rsi
	popq	%rdi
	testl	%eax, %eax
	je	.L410
	movzwl	28(%r12), %eax
	movl	$1, %edx
	andw	$1, %ax
	movl	$0, %eax
	cmovne	%r13, %rdx
	cmovne	%r14, %rax
	movq	%rdx, -88(%rbp)
	movq	%rax, -80(%rbp)
	jmp	.L411
.L415:
	movq	200(%r12), %rax
	leaq	8(%r10), %rcx
	andq	$-8, %rcx
	movq	%rax, (%r10)
	movl	%r15d, %eax
	movq	-8(%rbx,%rax), %rdx
	movq	%rdx, -8(%r10,%rax)
	subq	%rcx, %r10
	leal	(%r15,%r10), %eax
	subq	%r10, %rbx
	andl	$-8, %eax
	cmpl	$8, %eax
	jb	.L421
	andl	$-8, %eax
	xorl	%edx, %edx
.L419:
	movl	%edx, %esi
	addl	$8, %edx
	movq	(%rbx,%rsi), %rdi
	movq	%rdi, (%rcx,%rsi)
	cmpl	%eax, %edx
	jb	.L419
	jmp	.L421
.L453:
	call	__stack_chk_fail@PLT
.L452:
	movl	200(%r12), %edx
	movl	%edx, (%r10)
	movl	-4(%rbx,%rax), %edx
	movl	%edx, -4(%r10,%rax)
	jmp	.L421
	.cfi_endproc
.LFE393:
	.size	drbg_ctr_generate, .-drbg_ctr_generate
	.p2align 4
	.globl	drbg_ctr_init
	.type	drbg_ctr_init, @function
drbg_ctr_init:
.LFB395:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	20(%rdi), %eax
	cmpl	$905, %eax
	je	.L455
	cmpl	$906, %eax
	je	.L456
	xorl	%r8d, %r8d
	cmpl	$904, %eax
	je	.L475
	addq	$8, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L455:
	.cfi_restore_state
	call	EVP_aes_192_ecb@PLT
	movl	$40, %r12d
	movl	$192, %r13d
	movq	%rax, 184(%rbx)
	movl	$24, %eax
.L458:
	leaq	drbg_ctr_meth(%rip), %rdx
	cmpq	$0, 168(%rbx)
	movq	%rax, 192(%rbx)
	movq	%rdx, 320(%rbx)
	je	.L459
.L462:
	movl	%r13d, 48(%rbx)
	movq	%r12, 144(%rbx)
	testb	$1, 28(%rbx)
	jne	.L476
	movq	176(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L477
.L463:
	movq	184(%rbx), %rsi
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movl	$1, %r9d
	leaq	df_key.12007(%rip), %rcx
	call	EVP_CipherInit_ex@PLT
	testl	%eax, %eax
	je	.L464
	movq	192(%rbx), %rax
	movq	$2147483647, 72(%rbx)
	movq	$2147483647, 88(%rbx)
	movq	%rax, 64(%rbx)
	shrq	%rax
	movq	%rax, 80(%rbx)
	movq	$2147483647, 96(%rbx)
	movq	$2147483647, 104(%rbx)
	jmp	.L465
	.p2align 4,,10
	.p2align 3
.L459:
	call	EVP_CIPHER_CTX_new@PLT
	movq	%rax, 168(%rbx)
	testq	%rax, %rax
	jne	.L462
.L464:
	addq	$8, %rsp
	xorl	%r8d, %r8d
	popq	%rbx
	movl	%r8d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L476:
	.cfi_restore_state
	movq	%r12, %xmm0
	pxor	%xmm1, %xmm1
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm1, 80(%rbx)
	movups	%xmm0, 64(%rbx)
	movups	%xmm0, 96(%rbx)
.L465:
	movq	$65536, 56(%rbx)
	movl	$1, %r8d
	addq	$8, %rsp
	popq	%rbx
	movl	%r8d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L475:
	.cfi_restore_state
	call	EVP_aes_128_ecb@PLT
	movl	$32, %r12d
	movl	$128, %r13d
	movq	%rax, 184(%rbx)
	movl	$16, %eax
	jmp	.L458
	.p2align 4,,10
	.p2align 3
.L456:
	call	EVP_aes_256_ecb@PLT
	movl	$48, %r12d
	movl	$256, %r13d
	movq	%rax, 184(%rbx)
	movl	$32, %eax
	jmp	.L458
	.p2align 4,,10
	.p2align 3
.L477:
	call	EVP_CIPHER_CTX_new@PLT
	movq	%rax, 176(%rbx)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L464
	jmp	.L463
	.cfi_endproc
.LFE395:
	.size	drbg_ctr_init, .-drbg_ctr_init
	.data
	.type	c80.11950, @object
	.size	c80.11950, 1
c80.11950:
	.byte	-128
	.section	.rodata
	.align 32
	.type	df_key.12007, @object
	.size	df_key.12007, 32
df_key.12007:
	.string	""
	.ascii	"\001\002\003\004\005\006\007\b\t\n\013\f\r\016\017\020\021\022"
	.ascii	"\023\024\025\026\027\030\031\032\033\034\035\036\037"
	.section	.data.rel.local,"aw"
	.align 32
	.type	drbg_ctr_meth, @object
	.size	drbg_ctr_meth, 32
drbg_ctr_meth:
	.quad	drbg_ctr_instantiate
	.quad	drbg_ctr_reseed
	.quad	drbg_ctr_generate
	.quad	drbg_ctr_uninstantiate
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
