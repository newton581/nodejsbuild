	.file	"ec_asn1.c"
	.text
	.p2align 4
	.globl	EC_GROUP_get_basis_type
	.type	EC_GROUP_get_basis_type, @function
EC_GROUP_get_basis_type:
.LFB412:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	EC_GROUP_method_of@PLT
	movq	%rax, %rdi
	call	EC_METHOD_get_field_type@PLT
	movl	%eax, %r8d
	xorl	%eax, %eax
	cmpl	$407, %r8d
	jne	.L1
	movl	72(%rbx), %eax
	testl	%eax, %eax
	jne	.L17
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	.cfi_restore_state
	movl	76(%rbx), %eax
	testl	%eax, %eax
	je	.L1
	movl	80(%rbx), %edx
	movl	$682, %eax
	testl	%edx, %edx
	je	.L1
	movl	84(%rbx), %eax
	testl	%eax, %eax
	je	.L1
	cmpl	$1, 88(%rbx)
	sbbl	%eax, %eax
	andl	$683, %eax
	jmp	.L1
	.cfi_endproc
.LFE412:
	.size	EC_GROUP_get_basis_type, .-EC_GROUP_get_basis_type
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/ec/ec_asn1.c"
	.text
	.p2align 4
	.globl	EC_GROUP_get_trinomial_basis
	.type	EC_GROUP_get_trinomial_basis, @function
EC_GROUP_get_trinomial_basis:
.LFB413:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L30
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	call	EC_GROUP_method_of@PLT
	movq	%rax, %rdi
	call	EC_METHOD_get_field_type@PLT
	cmpl	$407, %eax
	jne	.L20
	movl	72(%rbx), %edx
	testl	%edx, %edx
	je	.L20
	movl	76(%rbx), %edx
	testl	%edx, %edx
	je	.L20
	movl	80(%rbx), %eax
	testl	%eax, %eax
	jne	.L20
	movl	$1, %eax
	testq	%r12, %r12
	je	.L18
	movl	%edx, (%r12)
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L20:
	movl	$51, %r8d
	movl	$66, %edx
	movl	$194, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
.L18:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE413:
	.size	EC_GROUP_get_trinomial_basis, .-EC_GROUP_get_trinomial_basis
	.p2align 4
	.globl	EC_GROUP_get_pentanomial_basis
	.type	EC_GROUP_get_pentanomial_basis, @function
EC_GROUP_get_pentanomial_basis:
.LFB414:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L53
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	call	EC_GROUP_method_of@PLT
	movq	%rax, %rdi
	call	EC_METHOD_get_field_type@PLT
	cmpl	$407, %eax
	jne	.L35
	movl	72(%rbx), %edi
	testl	%edi, %edi
	je	.L35
	movl	76(%rbx), %esi
	testl	%esi, %esi
	je	.L35
	movl	80(%rbx), %ecx
	testl	%ecx, %ecx
	je	.L35
	movl	84(%rbx), %eax
	testl	%eax, %eax
	je	.L35
	movl	88(%rbx), %edx
	testl	%edx, %edx
	jne	.L35
	testq	%r14, %r14
	je	.L37
	movl	%eax, (%r14)
.L37:
	testq	%r13, %r13
	je	.L38
	movl	80(%rbx), %eax
	movl	%eax, 0(%r13)
.L38:
	movl	$1, %eax
	testq	%r12, %r12
	je	.L33
	movl	76(%rbx), %edx
	movl	%edx, (%r12)
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L35:
	movl	$73, %r8d
	movl	$66, %edx
	movl	$193, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
.L33:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L53:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	ret
	.cfi_endproc
.LFE414:
	.size	EC_GROUP_get_pentanomial_basis, .-EC_GROUP_get_pentanomial_basis
	.p2align 4
	.globl	X9_62_PENTANOMIAL_new
	.type	X9_62_PENTANOMIAL_new, @function
X9_62_PENTANOMIAL_new:
.LFB415:
	.cfi_startproc
	endbr64
	leaq	X9_62_PENTANOMIAL_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE415:
	.size	X9_62_PENTANOMIAL_new, .-X9_62_PENTANOMIAL_new
	.p2align 4
	.globl	X9_62_PENTANOMIAL_free
	.type	X9_62_PENTANOMIAL_free, @function
X9_62_PENTANOMIAL_free:
.LFB416:
	.cfi_startproc
	endbr64
	leaq	X9_62_PENTANOMIAL_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE416:
	.size	X9_62_PENTANOMIAL_free, .-X9_62_PENTANOMIAL_free
	.p2align 4
	.globl	X9_62_CHARACTERISTIC_TWO_new
	.type	X9_62_CHARACTERISTIC_TWO_new, @function
X9_62_CHARACTERISTIC_TWO_new:
.LFB417:
	.cfi_startproc
	endbr64
	leaq	X9_62_CHARACTERISTIC_TWO_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE417:
	.size	X9_62_CHARACTERISTIC_TWO_new, .-X9_62_CHARACTERISTIC_TWO_new
	.p2align 4
	.globl	X9_62_CHARACTERISTIC_TWO_free
	.type	X9_62_CHARACTERISTIC_TWO_free, @function
X9_62_CHARACTERISTIC_TWO_free:
.LFB418:
	.cfi_startproc
	endbr64
	leaq	X9_62_CHARACTERISTIC_TWO_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE418:
	.size	X9_62_CHARACTERISTIC_TWO_free, .-X9_62_CHARACTERISTIC_TWO_free
	.p2align 4
	.globl	ECPARAMETERS_new
	.type	ECPARAMETERS_new, @function
ECPARAMETERS_new:
.LFB419:
	.cfi_startproc
	endbr64
	leaq	ECPARAMETERS_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE419:
	.size	ECPARAMETERS_new, .-ECPARAMETERS_new
	.p2align 4
	.globl	ECPARAMETERS_free
	.type	ECPARAMETERS_free, @function
ECPARAMETERS_free:
.LFB420:
	.cfi_startproc
	endbr64
	leaq	ECPARAMETERS_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE420:
	.size	ECPARAMETERS_free, .-ECPARAMETERS_free
	.p2align 4
	.globl	d2i_ECPKPARAMETERS
	.type	d2i_ECPKPARAMETERS, @function
d2i_ECPKPARAMETERS:
.LFB421:
	.cfi_startproc
	endbr64
	leaq	ECPKPARAMETERS_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE421:
	.size	d2i_ECPKPARAMETERS, .-d2i_ECPKPARAMETERS
	.p2align 4
	.globl	i2d_ECPKPARAMETERS
	.type	i2d_ECPKPARAMETERS, @function
i2d_ECPKPARAMETERS:
.LFB422:
	.cfi_startproc
	endbr64
	leaq	ECPKPARAMETERS_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE422:
	.size	i2d_ECPKPARAMETERS, .-i2d_ECPKPARAMETERS
	.p2align 4
	.globl	ECPKPARAMETERS_new
	.type	ECPKPARAMETERS_new, @function
ECPKPARAMETERS_new:
.LFB423:
	.cfi_startproc
	endbr64
	leaq	ECPKPARAMETERS_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE423:
	.size	ECPKPARAMETERS_new, .-ECPKPARAMETERS_new
	.p2align 4
	.globl	ECPKPARAMETERS_free
	.type	ECPKPARAMETERS_free, @function
ECPKPARAMETERS_free:
.LFB424:
	.cfi_startproc
	endbr64
	leaq	ECPKPARAMETERS_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE424:
	.size	ECPKPARAMETERS_free, .-ECPKPARAMETERS_free
	.p2align 4
	.globl	d2i_EC_PRIVATEKEY
	.type	d2i_EC_PRIVATEKEY, @function
d2i_EC_PRIVATEKEY:
.LFB425:
	.cfi_startproc
	endbr64
	leaq	EC_PRIVATEKEY_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE425:
	.size	d2i_EC_PRIVATEKEY, .-d2i_EC_PRIVATEKEY
	.p2align 4
	.globl	i2d_EC_PRIVATEKEY
	.type	i2d_EC_PRIVATEKEY, @function
i2d_EC_PRIVATEKEY:
.LFB426:
	.cfi_startproc
	endbr64
	leaq	EC_PRIVATEKEY_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE426:
	.size	i2d_EC_PRIVATEKEY, .-i2d_EC_PRIVATEKEY
	.p2align 4
	.globl	EC_PRIVATEKEY_new
	.type	EC_PRIVATEKEY_new, @function
EC_PRIVATEKEY_new:
.LFB427:
	.cfi_startproc
	endbr64
	leaq	EC_PRIVATEKEY_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE427:
	.size	EC_PRIVATEKEY_new, .-EC_PRIVATEKEY_new
	.p2align 4
	.globl	EC_PRIVATEKEY_free
	.type	EC_PRIVATEKEY_free, @function
EC_PRIVATEKEY_free:
.LFB428:
	.cfi_startproc
	endbr64
	leaq	EC_PRIVATEKEY_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE428:
	.size	EC_PRIVATEKEY_free, .-EC_PRIVATEKEY_free
	.p2align 4
	.globl	EC_GROUP_get_ecparameters
	.type	EC_GROUP_get_ecparameters, @function
EC_GROUP_get_ecparameters:
.LFB431:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -64(%rbp)
	testq	%rsi, %rsi
	je	.L165
.L71:
	movl	$1, (%r12)
	movq	8(%r12), %r14
	testq	%r13, %r13
	je	.L73
	testq	%r14, %r14
	je	.L73
	movq	(%r14), %rdi
	call	ASN1_OBJECT_free@PLT
	movq	8(%r14), %rdi
	call	ASN1_TYPE_free@PLT
	movq	%r13, %rdi
	call	EC_GROUP_method_of@PLT
	movq	%rax, %rdi
	call	EC_METHOD_get_field_type@PLT
	movl	%eax, %edi
	movl	%eax, %r15d
	call	OBJ_nid2obj@PLT
	movl	$259, %r8d
	movl	$8, %edx
	leaq	.LC0(%rip), %rcx
	movq	%rax, (%r14)
	testq	%rax, %rax
	je	.L159
	cmpl	$406, %r15d
	je	.L166
	cmpl	$407, %r15d
	jne	.L79
	leaq	X9_62_CHARACTERISTIC_TWO_it(%rip), %rdi
	call	ASN1_item_new@PLT
	movl	$294, %r8d
	movq	%rax, 8(%r14)
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L160
	movq	%r13, %rdi
	call	EC_GROUP_get_degree@PLT
	movq	%r13, %rdi
	movl	%eax, (%r15)
	call	EC_GROUP_method_of@PLT
	movq	%rax, %rdi
	call	EC_METHOD_get_field_type@PLT
	cmpl	$407, %eax
	je	.L167
.L81:
	movl	$303, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$16, %edx
	.p2align 4,,10
	.p2align 3
.L159:
	movl	$16, %edi
	movl	$154, %esi
	call	ERR_put_error@PLT
	xorl	%edi, %edi
	call	BN_free@PLT
.L73:
	movl	$464, %r8d
.L162:
	movl	$16, %edx
	movl	$261, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L92:
	testq	%rbx, %rbx
	je	.L72
	xorl	%r12d, %r12d
.L70:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L168
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L165:
	.cfi_restore_state
	leaq	ECPARAMETERS_it(%rip), %rdi
	call	ASN1_item_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L71
	movl	$453, %r8d
	movl	$65, %edx
	movl	$261, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L72:
	movq	%r12, %rdi
	leaq	ECPARAMETERS_it(%rip), %rsi
	xorl	%r12d, %r12d
	call	ASN1_item_free@PLT
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L79:
	movl	$355, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$131, %edx
	jmp	.L159
	.p2align 4,,10
	.p2align 3
.L166:
	call	BN_new@PLT
	movl	$265, %r8d
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L160
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r13, %rdi
	call	EC_GROUP_get_curve@PLT
	movl	$270, %r8d
	movl	$16, %edx
	leaq	.LC0(%rip), %rcx
	testl	%eax, %eax
	je	.L161
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	BN_to_ASN1_INTEGER@PLT
	movq	%rax, 8(%r14)
	testq	%rax, %rax
	je	.L169
	movq	%r15, %rdi
	call	BN_free@PLT
.L91:
	movq	16(%r12), %r15
	testq	%r15, %r15
	je	.L93
	cmpq	$0, (%r15)
	je	.L93
	cmpq	$0, 8(%r15)
	je	.L93
	call	BN_new@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L96
	call	BN_new@PLT
	movq	%rax, -72(%rbp)
	testq	%rax, %rax
	je	.L96
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	movq	%rax, %rcx
	movq	%r14, %rdx
	movq	%r13, %rdi
	call	EC_GROUP_get_curve@PLT
	testl	%eax, %eax
	je	.L170
	movq	%r13, %rdi
	call	EC_GROUP_get_degree@PLT
	movl	$393, %edx
	leaq	.LC0(%rip), %rsi
	cltq
	addq	$7, %rax
	shrq	$3, %rax
	movq	%rax, %rdi
	movq	%rax, -88(%rbp)
	call	CRYPTO_malloc@PLT
	movq	%rax, %r10
	testq	%rax, %rax
	je	.L99
	movq	-88(%rbp), %rdi
	movl	$394, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rax, -96(%rbp)
	call	CRYPTO_malloc@PLT
	movq	-96(%rbp), %r10
	testq	%rax, %rax
	movq	%rax, -80(%rbp)
	je	.L99
	movl	-88(%rbp), %edx
	movq	%r10, %rsi
	movq	%r14, %rdi
	call	BN_bn2binpad@PLT
	movq	-96(%rbp), %r10
	testl	%eax, %eax
	js	.L101
	movl	-88(%rbp), %edx
	movq	-80(%rbp), %rsi
	movq	%r10, -96(%rbp)
	movq	-72(%rbp), %rdi
	call	BN_bn2binpad@PLT
	movq	-96(%rbp), %r10
	testl	%eax, %eax
	js	.L101
	movl	-88(%rbp), %edx
	movq	(%r15), %rdi
	movq	%r10, %rsi
	call	ASN1_OCTET_STRING_set@PLT
	movq	-96(%rbp), %r10
	testl	%eax, %eax
	je	.L103
	movq	8(%r15), %rdi
	movl	-88(%rbp), %edx
	movq	%r10, -96(%rbp)
	movq	-80(%rbp), %rsi
	call	ASN1_OCTET_STRING_set@PLT
	movq	-96(%rbp), %r10
	testl	%eax, %eax
	je	.L103
	movq	48(%r13), %rsi
	movq	16(%r15), %rdi
	testq	%rsi, %rsi
	je	.L104
	testq	%rdi, %rdi
	je	.L171
.L105:
	movq	16(%rdi), %rax
	movl	56(%r13), %edx
	movq	%r10, -88(%rbp)
	andq	$-16, %rax
	orq	$8, %rax
	movq	%rax, 16(%rdi)
	call	ASN1_BIT_STRING_set@PLT
	movq	-88(%rbp), %r10
	testl	%eax, %eax
	je	.L172
.L107:
	movq	%r10, %rdi
	movl	$433, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-80(%rbp), %rdi
	movl	$434, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%r14, %rdi
	call	BN_free@PLT
	movq	-72(%rbp), %rdi
	call	BN_free@PLT
	movq	%r13, %rdi
	call	EC_GROUP_get0_generator@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L173
	movq	%r13, %rdi
	call	EC_GROUP_get_point_conversion_form@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	leaq	-64(%rbp), %rcx
	movl	%eax, %edx
	movq	%r13, %rdi
	call	EC_POINT_point2buf@PLT
	movl	$484, %r8d
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L162
	movq	24(%r12), %rdi
	testq	%rdi, %rdi
	je	.L174
.L109:
	movq	-64(%rbp), %rsi
	movl	%r14d, %edx
	call	ASN1_STRING_set0@PLT
	movq	%r13, %rdi
	call	EC_GROUP_get0_order@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L175
	movq	32(%r12), %r14
	movq	%r14, %rsi
	call	BN_to_ASN1_INTEGER@PLT
	movq	%rax, 32(%r12)
	testq	%rax, %rax
	je	.L176
	movq	%r13, %rdi
	call	EC_GROUP_get0_cofactor@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L70
	movq	40(%r12), %r13
	movq	%r13, %rsi
	call	BN_to_ASN1_INTEGER@PLT
	movq	%rax, 40(%r12)
	testq	%rax, %rax
	jne	.L70
	movq	%r13, 40(%r12)
	movl	$513, %r8d
	.p2align 4,,10
	.p2align 3
.L163:
	movl	$13, %edx
	movl	$261, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L167:
	movl	72(%r13), %r10d
	testl	%r10d, %r10d
	je	.L81
	movl	76(%r13), %r9d
	testl	%r9d, %r9d
	je	.L81
	movl	80(%r13), %r8d
	testl	%r8d, %r8d
	jne	.L177
	movl	$682, %edi
	call	OBJ_nid2obj@PLT
	movq	%rax, 8(%r15)
	testq	%rax, %rax
	je	.L115
	movq	%r13, %rdi
	call	EC_GROUP_method_of@PLT
	movq	%rax, %rdi
	call	EC_METHOD_get_field_type@PLT
	cmpl	$407, %eax
	jne	.L84
	movl	72(%r13), %esi
	testl	%esi, %esi
	je	.L84
	movl	76(%r13), %r14d
	testl	%r14d, %r14d
	je	.L84
	movl	80(%r13), %ecx
	testl	%ecx, %ecx
	jne	.L84
	call	ASN1_INTEGER_new@PLT
	movq	%rax, 16(%r15)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L178
	movl	%r14d, %esi
	call	ASN1_INTEGER_set@PLT
	movl	$324, %r8d
	movl	$13, %edx
	leaq	.LC0(%rip), %rcx
	testl	%eax, %eax
	je	.L159
	xorl	%edi, %edi
	call	BN_free@PLT
	jmp	.L91
.L169:
	movl	$276, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$13, %edx
	.p2align 4,,10
	.p2align 3
.L161:
	movl	$154, %esi
	movl	$16, %edi
	call	ERR_put_error@PLT
	movq	%r15, %rdi
	call	BN_free@PLT
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L96:
	movl	$377, %r8d
	movl	$65, %edx
	movl	$153, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	$0, -80(%rbp)
	xorl	%r10d, %r10d
	movq	$0, -72(%rbp)
.L95:
	movq	%r10, %rdi
	movl	$433, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-80(%rbp), %rdi
	movl	$434, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%r14, %rdi
	call	BN_free@PLT
	movq	-72(%rbp), %rdi
	call	BN_free@PLT
	.p2align 4,,10
	.p2align 3
.L93:
	movl	$470, %r8d
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L174:
	call	ASN1_OCTET_STRING_new@PLT
	movq	%rax, 24(%r12)
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L109
	movq	-64(%rbp), %rdi
	movl	$488, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movl	$489, %r8d
	movl	$65, %edx
	leaq	.LC0(%rip), %rcx
	movl	$261, %esi
	movl	$16, %edi
	call	ERR_put_error@PLT
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L173:
	movl	$476, %r8d
	movl	$113, %edx
	movl	$261, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L92
.L178:
	movl	$320, %r8d
	.p2align 4,,10
	.p2align 3
.L160:
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	jmp	.L159
	.p2align 4,,10
	.p2align 3
.L175:
	movl	$497, %r8d
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L170:
	movl	$383, %r8d
	movl	$16, %edx
	movl	$153, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	$0, -80(%rbp)
	xorl	%r10d, %r10d
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L176:
	movq	%r14, 32(%r12)
	movl	$503, %r8d
	jmp	.L163
	.p2align 4,,10
	.p2align 3
.L99:
	movl	$395, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	movl	$153, %esi
	movl	$16, %edi
	movq	%r10, -88(%rbp)
	call	ERR_put_error@PLT
	movq	$0, -80(%rbp)
	movq	-88(%rbp), %r10
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L101:
	movl	$400, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$3, %edx
	movl	$153, %esi
	movl	$16, %edi
	movq	%r10, -88(%rbp)
	call	ERR_put_error@PLT
	movq	-88(%rbp), %r10
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L103:
	movq	%r10, -88(%rbp)
	movl	$407, %r8d
.L164:
	movl	$13, %edx
	movl	$153, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	-88(%rbp), %r10
	jmp	.L95
.L84:
	movl	$51, %r8d
	movl	$66, %edx
	movl	$194, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%edi, %edi
	call	BN_free@PLT
	jmp	.L73
.L115:
	movl	$308, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$8, %edx
	jmp	.L159
.L177:
	movl	84(%r13), %edi
	testl	%edi, %edi
	je	.L81
	cmpl	$0, 88(%r13)
	jne	.L81
	movl	$683, %edi
	call	OBJ_nid2obj@PLT
	movq	%rax, 8(%r15)
	testq	%rax, %rax
	je	.L115
	movq	%r13, %rdi
	call	EC_GROUP_method_of@PLT
	movq	%rax, %rdi
	call	EC_METHOD_get_field_type@PLT
	cmpl	$407, %eax
	jne	.L88
	movl	72(%r13), %edx
	testl	%edx, %edx
	je	.L88
	movl	76(%r13), %r14d
	testl	%r14d, %r14d
	je	.L88
	movl	80(%r13), %eax
	movl	%eax, -72(%rbp)
	testl	%eax, %eax
	je	.L88
	movl	84(%r13), %eax
	movl	%eax, -80(%rbp)
	testl	%eax, %eax
	je	.L88
	movl	88(%r13), %eax
	testl	%eax, %eax
	jne	.L88
	leaq	X9_62_PENTANOMIAL_it(%rip), %rdi
	call	ASN1_item_new@PLT
	movl	$335, %r8d
	movq	%rax, 16(%r15)
	testq	%rax, %rax
	je	.L160
	movl	-80(%rbp), %ecx
	movl	%r14d, 8(%rax)
	xorl	%edi, %edi
	movl	%ecx, (%rax)
	movl	-72(%rbp), %ecx
	movl	%ecx, 4(%rax)
	call	BN_free@PLT
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L88:
	movl	$73, %r8d
	movl	$66, %edx
	movl	$193, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%edi, %edi
	call	BN_free@PLT
	jmp	.L73
.L171:
	movq	%r10, -88(%rbp)
	call	ASN1_BIT_STRING_new@PLT
	movq	-88(%rbp), %r10
	testq	%rax, %rax
	movq	%rax, 16(%r15)
	movq	%rax, %rdi
	je	.L106
	movq	48(%r13), %rsi
	jmp	.L105
.L104:
	movq	%r10, -88(%rbp)
	call	ASN1_BIT_STRING_free@PLT
	movq	$0, 16(%r15)
	movq	-88(%rbp), %r10
	jmp	.L107
.L172:
	movl	$422, %r8d
	jmp	.L164
.L168:
	call	__stack_chk_fail@PLT
.L106:
	movl	$415, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	movl	$153, %esi
	movl	$16, %edi
	movq	%r10, -88(%rbp)
	call	ERR_put_error@PLT
	movq	-88(%rbp), %r10
	jmp	.L95
	.cfi_endproc
.LFE431:
	.size	EC_GROUP_get_ecparameters, .-EC_GROUP_get_ecparameters
	.p2align 4
	.globl	EC_GROUP_get_ecpkparameters
	.type	EC_GROUP_get_ecpkparameters, @function
EC_GROUP_get_ecpkparameters:
.LFB432:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	testq	%rsi, %rsi
	je	.L197
	movl	(%rsi), %eax
	movq	%rsi, %r12
	testl	%eax, %eax
	je	.L198
	cmpl	$1, %eax
	je	.L199
.L181:
	movq	%r13, %rdi
	call	EC_GROUP_get_asn1_flag@PLT
	testl	%eax, %eax
	je	.L184
.L200:
	movq	%r13, %rdi
	call	EC_GROUP_get_curve_name@PLT
	movl	%eax, %edi
	testl	%eax, %eax
	je	.L186
	movl	$0, (%r12)
	call	OBJ_nid2obj@PLT
	movq	%rax, 8(%r12)
	testq	%rax, %rax
	jne	.L182
.L186:
	movq	%r12, %rdi
	leaq	ECPKPARAMETERS_it(%rip), %rsi
	xorl	%r12d, %r12d
	call	ASN1_item_free@PLT
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L184:
	.cfi_restore_state
	movl	$1, (%r12)
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	EC_GROUP_get_ecparameters
	movq	%rax, 8(%r12)
	testq	%rax, %rax
	je	.L186
.L182:
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L198:
	.cfi_restore_state
	movq	8(%rsi), %rdi
	call	ASN1_OBJECT_free@PLT
	movq	%r13, %rdi
	call	EC_GROUP_get_asn1_flag@PLT
	testl	%eax, %eax
	je	.L184
	jmp	.L200
	.p2align 4,,10
	.p2align 3
.L199:
	movq	8(%rsi), %rdi
	testq	%rdi, %rdi
	je	.L181
	leaq	ECPARAMETERS_it(%rip), %rsi
	call	ASN1_item_free@PLT
	movq	%r13, %rdi
	call	EC_GROUP_get_asn1_flag@PLT
	testl	%eax, %eax
	je	.L184
	jmp	.L200
	.p2align 4,,10
	.p2align 3
.L197:
	leaq	ECPKPARAMETERS_it(%rip), %rdi
	call	ASN1_item_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L181
	movl	$534, %r8d
	movl	$65, %edx
	movl	$262, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L182
	.cfi_endproc
.LFE432:
	.size	EC_GROUP_get_ecpkparameters, .-EC_GROUP_get_ecpkparameters
	.p2align 4
	.globl	EC_GROUP_new_from_ecparameters
	.type	EC_GROUP_new_from_ecparameters, @function
EC_GROUP_new_from_ecparameters:
.LFB433:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rax
	testq	%rax, %rax
	je	.L202
	cmpq	$0, (%rax)
	je	.L202
	cmpq	$0, 8(%rax)
	je	.L202
	movq	16(%rdi), %rax
	movq	%rdi, %rbx
	testq	%rax, %rax
	je	.L205
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	je	.L205
	movq	8(%rcx), %rdi
	testq	%rdi, %rdi
	je	.L205
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L205
	cmpq	$0, 8(%rax)
	je	.L205
	movl	(%rcx), %esi
	xorl	%edx, %edx
	call	BN_bin2bn@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L296
	movq	16(%rbx), %rax
	xorl	%edx, %edx
	movq	8(%rax), %rax
	movq	8(%rax), %rdi
	movl	(%rax), %esi
	call	BN_bin2bn@PLT
	movl	$606, %r8d
	movl	$3, %edx
	leaq	.LC0(%rip), %rcx
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L286
	movq	8(%rbx), %rax
	movq	(%rax), %rdi
	call	OBJ_obj2nid@PLT
	cmpl	$407, %eax
	je	.L297
	cmpl	$406, %eax
	jne	.L224
	movq	8(%rbx), %rax
	movq	8(%rax), %r11
	testq	%r11, %r11
	je	.L298
	xorl	%esi, %esi
	movq	%r11, %rdi
	call	ASN1_INTEGER_to_BN@PLT
	movl	$711, %r8d
	movl	$13, %edx
	leaq	.LC0(%rip), %rcx
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L287
	movq	%rax, %rdi
	call	BN_is_negative@PLT
	testl	%eax, %eax
	jne	.L228
	movq	%r14, %rdi
	call	BN_is_zero@PLT
	testl	%eax, %eax
	jne	.L228
	movq	%r14, %rdi
	call	BN_num_bits@PLT
	movl	%eax, -56(%rbp)
	cmpl	$661, %eax
	jg	.L299
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	EC_GROUP_new_curve_GFp@PLT
	movq	%rax, %r11
.L223:
	testq	%r11, %r11
	je	.L300
	movq	16(%rbx), %rax
	cmpq	$0, 16(%rax)
	je	.L231
	movq	48(%r11), %rdi
	movl	$740, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r11, -64(%rbp)
	call	CRYPTO_free@PLT
	movq	16(%rbx), %rax
	movl	$741, %edx
	leaq	.LC0(%rip), %rsi
	movq	16(%rax), %rax
	movslq	(%rax), %rdi
	call	CRYPTO_malloc@PLT
	movq	-64(%rbp), %r11
	movq	%rax, %r9
	movq	%rax, 48(%r11)
	testq	%rax, %rax
	je	.L301
	movq	16(%rbx), %rax
	movq	%r9, %rdi
	movq	%r11, -64(%rbp)
	movq	16(%rax), %rax
	movslq	(%rax), %rdx
	movq	8(%rax), %rsi
	call	memcpy@PLT
	movq	16(%rbx), %rax
	movq	-64(%rbp), %r11
	movq	16(%rax), %rax
	movslq	(%rax), %rax
	movq	%rax, 56(%r11)
.L231:
	cmpq	$0, 32(%rbx)
	je	.L233
	movq	24(%rbx), %rax
	testq	%rax, %rax
	je	.L233
	cmpq	$0, 8(%rax)
	je	.L233
	movq	%r11, %rdi
	movq	%r11, -64(%rbp)
	call	EC_POINT_new@PLT
	movq	-64(%rbp), %r11
	testq	%rax, %rax
	movq	%rax, %r10
	je	.L256
	movq	%rax, -72(%rbp)
	movq	24(%rbx), %rax
	movq	%r11, %rdi
	movq	8(%rax), %rax
	movzbl	(%rax), %esi
	andl	$-2, %esi
	call	EC_GROUP_set_point_conversion_form@PLT
	movq	24(%rbx), %rax
	movq	-72(%rbp), %r10
	xorl	%r8d, %r8d
	movq	-64(%rbp), %r11
	movslq	(%rax), %rcx
	movq	8(%rax), %rdx
	movq	%r10, %rsi
	movq	%r11, %rdi
	call	EC_POINT_oct2point@PLT
	movq	-64(%rbp), %r11
	movq	-72(%rbp), %r10
	testl	%eax, %eax
	je	.L302
	movq	32(%rbx), %rdi
	movq	%r12, %rsi
	movq	%r10, -72(%rbp)
	movq	%r11, -64(%rbp)
	call	ASN1_INTEGER_to_BN@PLT
	movq	-64(%rbp), %r11
	movq	-72(%rbp), %r10
	testq	%rax, %rax
	movq	%rax, %r12
	je	.L303
	movq	%rax, %rdi
	movq	%r10, -72(%rbp)
	movq	%r11, -64(%rbp)
	call	BN_is_negative@PLT
	movq	-64(%rbp), %r11
	movq	-72(%rbp), %r10
	testl	%eax, %eax
	jne	.L238
	movq	%r12, %rdi
	movq	%r10, -72(%rbp)
	movq	%r11, -64(%rbp)
	call	BN_is_zero@PLT
	movq	-64(%rbp), %r11
	movq	-72(%rbp), %r10
	testl	%eax, %eax
	jne	.L238
	movq	%r12, %rdi
	call	BN_num_bits@PLT
	movl	-56(%rbp), %r15d
	movq	-64(%rbp), %r11
	movq	-72(%rbp), %r10
	addl	$1, %r15d
	cmpl	%r15d, %eax
	jg	.L304
	movq	40(%rbx), %rdi
	movq	%r10, -64(%rbp)
	movq	%r11, -56(%rbp)
	testq	%rdi, %rdi
	je	.L305
	movq	%r13, %rsi
	call	ASN1_INTEGER_to_BN@PLT
	movq	-56(%rbp), %r11
	movq	-64(%rbp), %r10
	testq	%rax, %rax
	movq	%rax, %r13
	je	.L306
.L241:
	movq	%r10, %rsi
	movq	%r11, %rdi
	movq	%r13, %rcx
	movq	%r12, %rdx
	movq	%r10, -64(%rbp)
	movq	%r11, -56(%rbp)
	call	EC_GROUP_set_generator@PLT
	movq	-56(%rbp), %r11
	movq	-64(%rbp), %r10
	testl	%eax, %eax
	je	.L307
	movq	%r10, -64(%rbp)
	movq	%r11, -56(%rbp)
	call	BN_CTX_new@PLT
	movq	-56(%rbp), %r11
	movq	-64(%rbp), %r10
	testq	%rax, %rax
	movq	%rax, %r15
	je	.L308
	movq	%r11, %rdi
	movq	%r10, -64(%rbp)
	movq	%r11, -56(%rbp)
	call	EC_GROUP_dup@PLT
	movq	-56(%rbp), %r11
	movq	-64(%rbp), %r10
	testq	%rax, %rax
	movq	%rax, %r9
	je	.L245
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%r10, -72(%rbp)
	movq	%r11, -64(%rbp)
	movq	%rax, -56(%rbp)
	call	EC_GROUP_set_seed@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %r11
	subq	$1, %rax
	movq	-72(%rbp), %r10
	je	.L309
.L245:
	movq	%r10, -72(%rbp)
	movl	$816, %r8d
	movq	%r9, -64(%rbp)
	movq	%r11, -56(%rbp)
.L290:
	movl	$16, %edx
	movl	$263, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	-56(%rbp), %r11
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r10
	jmp	.L204
	.p2align 4,,10
	.p2align 3
.L205:
	movl	$596, %r8d
.L285:
	leaq	.LC0(%rip), %rcx
	xorl	%r15d, %r15d
	xorl	%r13d, %r13d
	xorl	%r12d, %r12d
	movl	$115, %edx
	movl	$263, %esi
	movl	$16, %edi
	xorl	%r14d, %r14d
	call	ERR_put_error@PLT
	xorl	%r10d, %r10d
	xorl	%r9d, %r9d
	xorl	%r11d, %r11d
.L204:
	movq	%r11, %rdi
	movq	%r10, -72(%rbp)
	movq	%r9, -56(%rbp)
	call	EC_GROUP_free@PLT
	movq	-72(%rbp), %r10
	movq	-56(%rbp), %r9
	movq	$0, -64(%rbp)
.L246:
	movq	%r9, %rdi
	movq	%r10, -56(%rbp)
	call	EC_GROUP_free@PLT
	movq	%r14, %rdi
	call	BN_free@PLT
	movq	%r12, %rdi
	call	BN_free@PLT
	movq	%r13, %rdi
	call	BN_free@PLT
	movq	-56(%rbp), %r10
	movq	%r10, %rdi
	call	EC_POINT_free@PLT
	movq	%r15, %rdi
	call	BN_CTX_free@PLT
	movq	-64(%rbp), %rax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L202:
	.cfi_restore_state
	movl	$583, %r8d
	jmp	.L285
	.p2align 4,,10
	.p2align 3
.L297:
	movq	8(%rbx), %rax
	movl	$626, %r8d
	movl	$143, %edx
	leaq	.LC0(%rip), %rcx
	movq	8(%rax), %r15
	movl	(%r15), %eax
	movl	%eax, -56(%rbp)
	cmpl	$661, %eax
	jg	.L286
	call	BN_new@PLT
	movl	$631, %r8d
	movl	$65, %edx
	leaq	.LC0(%rip), %rcx
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L287
	movq	8(%r15), %rdi
	call	OBJ_obj2nid@PLT
	cmpl	$682, %eax
	je	.L310
	cmpl	$683, %eax
	je	.L311
	movl	$690, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$126, %edx
	cmpl	$681, %eax
	je	.L287
	movl	$694, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$115, %edx
	.p2align 4,,10
	.p2align 3
.L287:
	movl	$263, %esi
	movl	$16, %edi
	call	ERR_put_error@PLT
.L288:
	xorl	%r15d, %r15d
	xorl	%r10d, %r10d
	xorl	%r9d, %r9d
	xorl	%r11d, %r11d
	jmp	.L204
	.p2align 4,,10
	.p2align 3
.L224:
	movl	$729, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$103, %edx
.L286:
	movl	$263, %esi
	movl	$16, %edi
	xorl	%r15d, %r15d
	xorl	%r14d, %r14d
	call	ERR_put_error@PLT
	xorl	%r10d, %r10d
	xorl	%r9d, %r9d
	xorl	%r11d, %r11d
	jmp	.L204
	.p2align 4,,10
	.p2align 3
.L296:
	movl	$3, %edx
	movl	$263, %esi
	xorl	%r15d, %r15d
	xorl	%r13d, %r13d
	movl	$601, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$16, %edi
	xorl	%r14d, %r14d
	call	ERR_put_error@PLT
	xorl	%r10d, %r10d
	xorl	%r9d, %r9d
	xorl	%r11d, %r11d
	jmp	.L204
	.p2align 4,,10
	.p2align 3
.L228:
	movl	$716, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$103, %edx
	jmp	.L287
	.p2align 4,,10
	.p2align 3
.L298:
	movl	$115, %edx
	movl	$263, %esi
	xorl	%r15d, %r15d
	xorl	%r14d, %r14d
	movl	$706, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$16, %edi
	movq	%r11, -56(%rbp)
	call	ERR_put_error@PLT
	movq	-56(%rbp), %r11
	xorl	%r10d, %r10d
	xorl	%r9d, %r9d
	jmp	.L204
.L310:
	movq	16(%r15), %r11
	testq	%r11, %r11
	je	.L312
	movq	%r11, %rdi
	call	ASN1_INTEGER_get@PLT
	movl	(%r15), %esi
	movq	%rax, %rcx
	testq	%rax, %rax
	jle	.L258
	movslq	%esi, %rax
	cmpq	%rcx, %rax
	jle	.L258
	movq	%r14, %rdi
	movq	%rcx, -64(%rbp)
	call	BN_set_bit@PLT
	testl	%eax, %eax
	je	.L288
	movq	-64(%rbp), %rcx
	movq	%r14, %rdi
	movl	%ecx, %esi
	call	BN_set_bit@PLT
	testl	%eax, %eax
	je	.L288
.L295:
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	BN_set_bit@PLT
	testl	%eax, %eax
	je	.L288
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	EC_GROUP_new_curve_GF2m@PLT
	movq	%rax, %r11
	jmp	.L223
	.p2align 4,,10
	.p2align 3
.L311:
	movq	16(%r15), %r11
	testq	%r11, %r11
	je	.L313
	movl	(%r15), %esi
	movl	8(%r11), %eax
	cmpl	%eax, %esi
	jle	.L219
	movl	4(%r11), %edx
	cmpl	%edx, %eax
	jle	.L219
	movl	(%r11), %eax
	testl	%eax, %eax
	jle	.L219
	cmpl	%eax, %edx
	jle	.L219
	movq	%r14, %rdi
	movq	%r11, -64(%rbp)
	call	BN_set_bit@PLT
	testl	%eax, %eax
	je	.L288
	movq	-64(%rbp), %r11
	movq	%r14, %rdi
	movl	(%r11), %esi
	call	BN_set_bit@PLT
	testl	%eax, %eax
	je	.L288
	movq	-64(%rbp), %r11
	movq	%r14, %rdi
	movl	4(%r11), %esi
	call	BN_set_bit@PLT
	testl	%eax, %eax
	je	.L288
	movq	-64(%rbp), %r11
	movq	%r14, %rdi
	movl	8(%r11), %esi
	call	BN_set_bit@PLT
	testl	%eax, %eax
	je	.L288
	jmp	.L295
	.p2align 4,,10
	.p2align 3
.L299:
	movl	$722, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$143, %edx
	jmp	.L287
.L219:
	movl	$673, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$132, %edx
	jmp	.L287
.L258:
	movl	$649, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$137, %edx
	jmp	.L287
.L233:
	movq	%r11, -56(%rbp)
	movl	$751, %r8d
.L289:
	leaq	.LC0(%rip), %rcx
	movl	$115, %edx
.L293:
	movl	$263, %esi
	movl	$16, %edi
	xorl	%r15d, %r15d
	call	ERR_put_error@PLT
	movq	-56(%rbp), %r11
	xorl	%r10d, %r10d
	xorl	%r9d, %r9d
	jmp	.L204
.L312:
	movq	%r11, -56(%rbp)
	movl	$642, %r8d
	jmp	.L289
.L256:
	xorl	%r15d, %r15d
	xorl	%r9d, %r9d
	jmp	.L204
.L238:
	movq	%r10, -64(%rbp)
	movl	$775, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$122, %edx
	movq	%r11, -56(%rbp)
.L291:
	movl	$263, %esi
	movl	$16, %edi
	xorl	%r15d, %r15d
	call	ERR_put_error@PLT
	movq	-56(%rbp), %r11
	movq	-64(%rbp), %r10
	xorl	%r9d, %r9d
	jmp	.L204
.L300:
	movq	%r11, -56(%rbp)
	movl	$734, %r8d
	movl	$16, %edx
	leaq	.LC0(%rip), %rcx
	jmp	.L293
.L302:
	movq	%r10, -64(%rbp)
	movl	$765, %r8d
	movq	%r11, -56(%rbp)
.L292:
	leaq	.LC0(%rip), %rcx
	movl	$16, %edx
	jmp	.L291
.L301:
	movl	$742, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	movl	$263, %esi
	movl	$16, %edi
	movq	%r11, -56(%rbp)
	xorl	%r15d, %r15d
	movq	%rax, -64(%rbp)
	call	ERR_put_error@PLT
	movq	-56(%rbp), %r11
	movq	-64(%rbp), %r9
	xorl	%r10d, %r10d
	jmp	.L204
.L313:
	movq	%r11, -56(%rbp)
	movl	$666, %r8d
	jmp	.L289
.L303:
	movq	%r10, -64(%rbp)
	movl	$771, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$13, %edx
	movq	%r11, -56(%rbp)
	jmp	.L291
.L309:
	xorl	%ecx, %ecx
	movq	%r10, %rsi
	movq	%r9, %rdi
	movq	%r12, %rdx
	movq	%r11, -72(%rbp)
	movq	%r10, -64(%rbp)
	call	EC_GROUP_set_generator@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %r10
	testl	%eax, %eax
	movq	-72(%rbp), %r11
	je	.L245
	movq	%r9, %rdi
	movq	%r15, %rsi
	movq	%r10, -72(%rbp)
	movq	%r11, -64(%rbp)
	call	ec_curve_nid_from_params@PLT
	movq	-64(%rbp), %r11
	movq	-56(%rbp), %r9
	testl	%eax, %eax
	movq	-72(%rbp), %r10
	movl	%eax, %edi
	je	.L246
	movq	%r10, -80(%rbp)
	movq	%r9, -72(%rbp)
	movq	%r11, -56(%rbp)
	call	EC_GROUP_new_by_curve_name@PLT
	movq	-80(%rbp), %r10
	movq	-72(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, -64(%rbp)
	movq	-56(%rbp), %r11
	movq	%r10, -72(%rbp)
	je	.L314
	movq	%r11, %rdi
	movq	%r9, -56(%rbp)
	call	EC_GROUP_free@PLT
	movq	-64(%rbp), %rdi
	xorl	%esi, %esi
	call	EC_GROUP_set_asn1_flag@PLT
	movq	16(%rbx), %rax
	movq	-56(%rbp), %r9
	movq	-72(%rbp), %r10
	cmpq	$0, 16(%rax)
	jne	.L246
	movq	-64(%rbp), %rbx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	EC_GROUP_set_seed@PLT
	movq	-56(%rbp), %r9
	movq	-72(%rbp), %r10
	movq	%rbx, %r11
	subq	$1, %rax
	jne	.L204
	jmp	.L246
.L305:
	movq	%r13, %rdi
	xorl	%r13d, %r13d
	call	BN_free@PLT
	movq	-56(%rbp), %r11
	movq	-64(%rbp), %r10
	jmp	.L241
.L307:
	movl	$793, %r8d
	jmp	.L292
.L304:
	movq	%r10, -64(%rbp)
	movl	$779, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$122, %edx
	movq	%r11, -56(%rbp)
	jmp	.L291
.L314:
	movq	%r9, -64(%rbp)
	movl	$841, %r8d
	jmp	.L290
.L308:
	movl	$810, %r8d
	movl	$3, %edx
	movl	$263, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	-56(%rbp), %r11
	movq	-64(%rbp), %r10
	xorl	%r9d, %r9d
	jmp	.L204
.L306:
	movl	$788, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$13, %edx
	jmp	.L291
	.cfi_endproc
.LFE433:
	.size	EC_GROUP_new_from_ecparameters, .-EC_GROUP_new_from_ecparameters
	.p2align 4
	.globl	EC_GROUP_new_from_ecpkparameters
	.type	EC_GROUP_new_from_ecpkparameters, @function
EC_GROUP_new_from_ecpkparameters:
.LFB434:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	testq	%rdi, %rdi
	je	.L325
	movl	(%rdi), %eax
	testl	%eax, %eax
	je	.L326
	cmpl	$1, %eax
	je	.L327
	xorl	%r12d, %r12d
	cmpl	$2, %eax
	jne	.L328
.L315:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L328:
	.cfi_restore_state
	movl	$916, %r8d
	movl	$115, %edx
	movl	$264, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L326:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	call	OBJ_obj2nid@PLT
	movl	%eax, %edi
	call	EC_GROUP_new_by_curve_name@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L329
	movq	%rax, %rdi
	movl	$1, %esi
	call	EC_GROUP_set_asn1_flag@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L327:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	call	EC_GROUP_new_from_ecparameters
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L330
	movq	%rax, %rdi
	xorl	%esi, %esi
	call	EC_GROUP_set_asn1_flag@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L325:
	.cfi_restore_state
	movl	$893, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$124, %edx
	xorl	%r12d, %r12d
	movl	$264, %esi
	movl	$16, %edi
	call	ERR_put_error@PLT
	jmp	.L315
	.p2align 4,,10
	.p2align 3
.L329:
	movl	$900, %r8d
	movl	$119, %edx
	movl	$264, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L315
	.p2align 4,,10
	.p2align 3
.L330:
	movl	$909, %r8d
	movl	$16, %edx
	movl	$264, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L315
	.cfi_endproc
.LFE434:
	.size	EC_GROUP_new_from_ecpkparameters, .-EC_GROUP_new_from_ecpkparameters
	.p2align 4
	.globl	d2i_ECPKParameters
	.type	d2i_ECPKParameters, @function
d2i_ECPKParameters:
.LFB435:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	ECPKPARAMETERS_it(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	xorl	%edi, %edi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	leaq	-48(%rbp), %rsi
	movq	%rax, -48(%rbp)
	call	ASN1_item_d2i@PLT
	testq	%rax, %rax
	je	.L347
	movq	%rax, %r12
	movl	(%rax), %eax
	testl	%eax, %eax
	je	.L348
	cmpl	$1, %eax
	je	.L349
	cmpl	$2, %eax
	je	.L336
	movl	$916, %r8d
	movl	$115, %edx
	movl	$264, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L336:
	movl	$938, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$127, %edx
	xorl	%r14d, %r14d
	movl	$145, %esi
	movl	$16, %edi
	call	ERR_put_error@PLT
	leaq	ECPKPARAMETERS_it(%rip), %rsi
	movq	%r12, %rdi
	call	ASN1_item_free@PLT
	.p2align 4,,10
	.p2align 3
.L331:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L350
	addq	$16, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L348:
	.cfi_restore_state
	movq	8(%r12), %rdi
	call	OBJ_obj2nid@PLT
	movl	%eax, %edi
	call	EC_GROUP_new_by_curve_name@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L351
	movl	$1, %esi
	movq	%rax, %rdi
	call	EC_GROUP_set_asn1_flag@PLT
.L337:
	testq	%r13, %r13
	je	.L341
	movq	0(%r13), %rdi
	call	EC_GROUP_free@PLT
	movq	%r14, 0(%r13)
.L341:
	leaq	ECPKPARAMETERS_it(%rip), %rsi
	movq	%r12, %rdi
	call	ASN1_item_free@PLT
	movq	-48(%rbp), %rax
	movq	%rax, (%rbx)
	jmp	.L331
	.p2align 4,,10
	.p2align 3
.L349:
	movq	8(%r12), %rdi
	call	EC_GROUP_new_from_ecparameters
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L352
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	EC_GROUP_set_asn1_flag@PLT
	jmp	.L337
	.p2align 4,,10
	.p2align 3
.L347:
	movl	$932, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$117, %edx
	xorl	%r14d, %r14d
	movl	$145, %esi
	movl	$16, %edi
	call	ERR_put_error@PLT
	leaq	ECPKPARAMETERS_it(%rip), %rsi
	xorl	%edi, %edi
	call	ASN1_item_free@PLT
	jmp	.L331
	.p2align 4,,10
	.p2align 3
.L351:
	movl	$900, %r8d
	movl	$119, %edx
	movl	$264, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L336
.L352:
	movl	$909, %r8d
	movl	$16, %edx
	movl	$264, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L336
.L350:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE435:
	.size	d2i_ECPKParameters, .-d2i_ECPKParameters
	.p2align 4
	.globl	i2d_ECPKParameters
	.type	i2d_ECPKParameters, @function
i2d_ECPKParameters:
.LFB436:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	leaq	ECPKPARAMETERS_it(%rip), %rdi
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -40
	call	ASN1_item_new@PLT
	testq	%rax, %rax
	je	.L371
	movq	%r13, %rdi
	movq	%rax, %r12
	call	EC_GROUP_get_asn1_flag@PLT
	testl	%eax, %eax
	jne	.L372
	movl	$1, (%r12)
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	EC_GROUP_get_ecparameters
	movq	%rax, 8(%r12)
	testq	%rax, %rax
	je	.L358
.L359:
	leaq	ECPKPARAMETERS_it(%rip), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	ASN1_item_i2d@PLT
	movl	%eax, %r13d
	testl	%eax, %eax
	je	.L373
	movq	%r12, %rdi
	leaq	ECPKPARAMETERS_it(%rip), %rsi
	call	ASN1_item_free@PLT
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L372:
	.cfi_restore_state
	movq	%r13, %rdi
	call	EC_GROUP_get_curve_name@PLT
	movl	%eax, %edi
	testl	%eax, %eax
	je	.L358
	movl	$0, (%r12)
	call	OBJ_nid2obj@PLT
	movq	%rax, 8(%r12)
	testq	%rax, %rax
	jne	.L359
	.p2align 4,,10
	.p2align 3
.L358:
	leaq	ECPKPARAMETERS_it(%rip), %rsi
	movq	%r12, %rdi
	call	ASN1_item_free@PLT
.L355:
	movl	$958, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$120, %edx
	xorl	%r13d, %r13d
	movl	$191, %esi
	movl	$16, %edi
	call	ERR_put_error@PLT
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L373:
	.cfi_restore_state
	movl	$962, %r8d
	movl	$121, %edx
	movl	$191, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	%r12, %rdi
	leaq	ECPKPARAMETERS_it(%rip), %rsi
	call	ASN1_item_free@PLT
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L371:
	.cfi_restore_state
	movl	$534, %r8d
	movl	$65, %edx
	movl	$262, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L355
	.cfi_endproc
.LFE436:
	.size	i2d_ECPKParameters, .-i2d_ECPKParameters
	.p2align 4
	.globl	d2i_ECPrivateKey
	.type	d2i_ECPrivateKey, @function
d2i_ECPrivateKey:
.LFB437:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	EC_PRIVATEKEY_it(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	xorl	%edi, %edi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	leaq	-64(%rbp), %rsi
	movq	%rax, -64(%rbp)
	call	ASN1_item_d2i@PLT
	testq	%rax, %rax
	je	.L412
	movq	%rax, %r13
	testq	%rbx, %rbx
	je	.L377
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L377
.L378:
	cmpq	$0, 16(%r13)
	movq	24(%r12), %rdi
	je	.L380
	call	EC_GROUP_free@PLT
	movq	16(%r13), %rdi
	call	EC_GROUP_new_from_ecpkparameters
	movq	%rax, 24(%r12)
	movq	%rax, %rdi
.L380:
	movl	$997, %r8d
	testq	%rdi, %rdi
	je	.L411
	movl	0(%r13), %eax
	movq	8(%r13), %rdi
	movl	%eax, 16(%r12)
	testq	%rdi, %rdi
	je	.L382
	movq	%rdi, -72(%rbp)
	call	ASN1_STRING_length@PLT
	movq	-72(%rbp), %rdi
	movl	%eax, %r14d
	call	ASN1_STRING_get0_data@PLT
	movslq	%r14d, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	EC_KEY_oct2priv@PLT
	testl	%eax, %eax
	je	.L379
	movq	32(%r12), %rdi
	call	EC_POINT_clear_free@PLT
	movq	24(%r12), %rdi
	call	EC_POINT_new@PLT
	movl	$1016, %r8d
	movq	%rax, 32(%r12)
	testq	%rax, %rax
	je	.L411
	movq	24(%r13), %rdi
	testq	%rdi, %rdi
	je	.L385
	call	ASN1_STRING_get0_data@PLT
	movq	24(%r13), %rdi
	movq	%rax, %r14
	call	ASN1_STRING_length@PLT
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movslq	%eax, %rdx
	call	EC_KEY_oct2key@PLT
	testl	%eax, %eax
	je	.L413
.L386:
	testq	%rbx, %rbx
	je	.L387
	movq	%r12, (%rbx)
.L387:
	leaq	EC_PRIVATEKEY_it(%rip), %rsi
	movq	%r13, %rdi
	call	ASN1_item_free@PLT
	movq	-64(%rbp), %rax
	movq	%rax, (%r15)
.L374:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L414
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L385:
	.cfi_restore_state
	movq	24(%r12), %rax
	movq	(%rax), %rax
	movq	360(%rax), %rax
	testq	%rax, %rax
	je	.L379
	movq	%r12, %rdi
	call	*%rax
	testl	%eax, %eax
	je	.L379
	orl	$2, 48(%r12)
	jmp	.L386
	.p2align 4,,10
	.p2align 3
.L377:
	call	EC_KEY_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L378
	movl	$985, %r8d
	movl	$65, %edx
	movl	$146, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	.p2align 4,,10
	.p2align 3
.L379:
	testq	%rbx, %rbx
	je	.L388
	cmpq	%r12, (%rbx)
	je	.L389
.L388:
	movq	%r12, %rdi
	call	EC_KEY_free@PLT
.L389:
	leaq	EC_PRIVATEKEY_it(%rip), %rsi
	movq	%r13, %rdi
	xorl	%r12d, %r12d
	call	ASN1_item_free@PLT
	jmp	.L374
	.p2align 4,,10
	.p2align 3
.L413:
	movl	$1027, %r8d
.L411:
	movl	$16, %edx
	movl	$146, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L379
	.p2align 4,,10
	.p2align 3
.L382:
	movl	$1009, %r8d
	movl	$125, %edx
	movl	$146, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L379
	.p2align 4,,10
	.p2align 3
.L412:
	movl	$979, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$16, %edx
	xorl	%r12d, %r12d
	movl	$146, %esi
	movl	$16, %edi
	call	ERR_put_error@PLT
	jmp	.L374
.L414:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE437:
	.size	d2i_ECPrivateKey, .-d2i_ECPrivateKey
	.p2align 4
	.globl	i2d_ECPrivateKey
	.type	i2d_ECPrivateKey, @function
i2d_ECPrivateKey:
.LFB438:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	subq	$48, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -56(%rbp)
	movq	$0, -48(%rbp)
	testq	%rdi, %rdi
	je	.L416
	cmpq	$0, 24(%rdi)
	movq	%rdi, %r12
	je	.L416
	movq	%rsi, %r15
	testb	$2, 48(%rdi)
	jne	.L417
	cmpq	$0, 32(%rdi)
	je	.L416
.L417:
	leaq	EC_PRIVATEKEY_it(%rip), %rdi
	call	ASN1_item_new@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L434
	movl	16(%r12), %eax
	leaq	-56(%rbp), %rsi
	movq	%r12, %rdi
	movl	%eax, (%r14)
	call	EC_KEY_priv2buf@PLT
	movl	$1075, %r8d
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L433
	movq	8(%r14), %rdi
	movq	-56(%rbp), %rsi
	movl	%eax, %edx
	call	ASN1_STRING_set0@PLT
	movl	48(%r12), %eax
	movq	$0, -56(%rbp)
	testb	$1, %al
	je	.L435
.L421:
	testb	$2, %al
	jne	.L423
	call	ASN1_BIT_STRING_new@PLT
	movq	%rax, 24(%r14)
	testq	%rax, %rax
	je	.L436
	movl	52(%r12), %esi
	leaq	-48(%rbp), %rdx
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	call	EC_KEY_key2buf@PLT
	movl	$1101, %r8d
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L433
	movq	24(%r14), %rdi
	movq	-48(%rbp), %rsi
	movq	16(%rdi), %rax
	andq	$-16, %rax
	orq	$8, %rax
	movq	%rax, 16(%rdi)
	call	ASN1_STRING_set0@PLT
	movq	$0, -48(%rbp)
.L423:
	leaq	EC_PRIVATEKEY_it(%rip), %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	ASN1_item_i2d@PLT
	testl	%eax, %eax
	je	.L437
	movq	-56(%rbp), %rdi
	movl	$1117, %ecx
	movq	%r13, %rsi
	movl	%eax, -68(%rbp)
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_clear_free@PLT
	movq	-48(%rbp), %rdi
	movl	$1118, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	leaq	EC_PRIVATEKEY_it(%rip), %rsi
	movq	%r14, %rdi
	call	ASN1_item_free@PLT
	movl	-68(%rbp), %eax
	jmp	.L415
	.p2align 4,,10
	.p2align 3
.L437:
	movl	$1112, %r8d
	.p2align 4,,10
	.p2align 3
.L433:
	movl	$16, %edx
	movl	$192, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L418:
	movq	-56(%rbp), %rdi
	movl	$1117, %ecx
	leaq	.LC0(%rip), %rdx
	movq	%r13, %rsi
	call	CRYPTO_clear_free@PLT
	movq	-48(%rbp), %rdi
	movl	$1118, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	leaq	EC_PRIVATEKEY_it(%rip), %rsi
	movq	%r14, %rdi
	call	ASN1_item_free@PLT
	xorl	%eax, %eax
.L415:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L438
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L416:
	.cfi_restore_state
	movl	$67, %edx
	movl	$192, %esi
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	movl	$1061, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$16, %edi
	call	ERR_put_error@PLT
	jmp	.L418
	.p2align 4,,10
	.p2align 3
.L434:
	movl	$1066, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r13d, %r13d
	movl	$192, %esi
	movl	$16, %edi
	call	ERR_put_error@PLT
	jmp	.L418
	.p2align 4,,10
	.p2align 3
.L435:
	movq	16(%r14), %rsi
	movq	24(%r12), %rdi
	call	EC_GROUP_get_ecpkparameters
	movq	%rax, 16(%r14)
	testq	%rax, %rax
	je	.L422
	movl	48(%r12), %eax
	jmp	.L421
	.p2align 4,,10
	.p2align 3
.L436:
	movl	$1094, %r8d
	movl	$65, %edx
	movl	$192, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L418
	.p2align 4,,10
	.p2align 3
.L422:
	movl	$1086, %r8d
	jmp	.L433
.L438:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE438:
	.size	i2d_ECPrivateKey, .-i2d_ECPrivateKey
	.p2align 4
	.globl	i2d_ECParameters
	.type	i2d_ECParameters, @function
i2d_ECParameters:
.LFB439:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	testq	%rdi, %rdi
	je	.L458
	movq	24(%rdi), %r13
	leaq	ECPKPARAMETERS_it(%rip), %rdi
	movq	%rsi, %r14
	call	ASN1_item_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L459
	movq	%r13, %rdi
	call	EC_GROUP_get_asn1_flag@PLT
	testl	%eax, %eax
	jne	.L460
	movl	$1, (%r12)
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	EC_GROUP_get_ecparameters
	movq	%rax, 8(%r12)
	testq	%rax, %rax
	je	.L446
.L447:
	leaq	ECPKPARAMETERS_it(%rip), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	ASN1_item_i2d@PLT
	movl	%eax, %r13d
	testl	%eax, %eax
	je	.L461
	movq	%r12, %rdi
	leaq	ECPKPARAMETERS_it(%rip), %rsi
	call	ASN1_item_free@PLT
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L460:
	.cfi_restore_state
	movq	%r13, %rdi
	call	EC_GROUP_get_curve_name@PLT
	movl	%eax, %edi
	testl	%eax, %eax
	je	.L446
	movl	$0, (%r12)
	call	OBJ_nid2obj@PLT
	movq	%rax, 8(%r12)
	testq	%rax, %rax
	jne	.L447
	.p2align 4,,10
	.p2align 3
.L446:
	leaq	ECPKPARAMETERS_it(%rip), %rsi
	movq	%r12, %rdi
	call	ASN1_item_free@PLT
.L443:
	movl	$958, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$120, %edx
	xorl	%r13d, %r13d
	movl	$191, %esi
	movl	$16, %edi
	call	ERR_put_error@PLT
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L461:
	.cfi_restore_state
	movl	$962, %r8d
	movl	$121, %edx
	movl	$191, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	%r12, %rdi
	leaq	ECPKPARAMETERS_it(%rip), %rsi
	call	ASN1_item_free@PLT
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L458:
	.cfi_restore_state
	movl	$1126, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$67, %edx
	xorl	%r13d, %r13d
	movl	$190, %esi
	movl	$16, %edi
	call	ERR_put_error@PLT
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L459:
	.cfi_restore_state
	movl	$534, %r8d
	movl	$65, %edx
	movl	$262, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L443
	.cfi_endproc
.LFE439:
	.size	i2d_ECParameters, .-i2d_ECParameters
	.p2align 4
	.globl	d2i_ECParameters
	.type	d2i_ECParameters, @function
d2i_ECParameters:
.LFB440:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L463
	movq	(%rsi), %rax
	movq	%rsi, %rbx
	testq	%rax, %rax
	je	.L463
	movq	%rdi, %r13
	testq	%rdi, %rdi
	je	.L466
	movq	(%rdi), %r12
	testq	%r12, %r12
	je	.L466
.L467:
	leaq	-64(%rbp), %rsi
	leaq	ECPKPARAMETERS_it(%rip), %rcx
	xorl	%edi, %edi
	movq	%rax, -64(%rbp)
	call	ASN1_item_d2i@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L486
	movq	%rax, %rdi
	call	EC_GROUP_new_from_ecpkparameters
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L487
	movq	24(%r12), %rdi
	call	EC_GROUP_free@PLT
	movq	%r15, 24(%r12)
	leaq	ECPKPARAMETERS_it(%rip), %rsi
	movq	%r14, %rdi
	call	ASN1_item_free@PLT
	movq	-64(%rbp), %rax
	movq	%rax, (%rbx)
	testq	%r13, %r13
	je	.L462
	movq	%r12, 0(%r13)
.L462:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L488
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L466:
	.cfi_restore_state
	movq	%rdx, -72(%rbp)
	call	EC_KEY_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L468
	movq	(%rbx), %rax
	movq	-72(%rbp), %rdx
	jmp	.L467
	.p2align 4,,10
	.p2align 3
.L463:
	movl	$1137, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$67, %edx
	xorl	%r12d, %r12d
	movl	$144, %esi
	movl	$16, %edi
	call	ERR_put_error@PLT
	jmp	.L462
	.p2align 4,,10
	.p2align 3
.L486:
	movl	$145, %esi
	movl	$16, %edi
	movl	$932, %r8d
	movl	$117, %edx
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	leaq	ECPKPARAMETERS_it(%rip), %rsi
	xorl	%edi, %edi
	call	ASN1_item_free@PLT
.L470:
	movl	$1150, %r8d
	movl	$16, %edx
	movl	$144, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	testq	%r13, %r13
	je	.L473
	cmpq	%r12, 0(%r13)
	je	.L475
.L473:
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	EC_KEY_free@PLT
	jmp	.L462
	.p2align 4,,10
	.p2align 3
.L487:
	movl	$938, %r8d
	movl	$127, %edx
	movl	$145, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	leaq	ECPKPARAMETERS_it(%rip), %rsi
	movq	%r14, %rdi
	call	ASN1_item_free@PLT
	jmp	.L470
	.p2align 4,,10
	.p2align 3
.L468:
	movl	$1143, %r8d
	movl	$65, %edx
	movl	$144, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L462
.L475:
	xorl	%r12d, %r12d
	jmp	.L462
.L488:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE440:
	.size	d2i_ECParameters, .-d2i_ECParameters
	.p2align 4
	.globl	o2i_ECPublicKey
	.type	o2i_ECPublicKey, @function
o2i_ECPublicKey:
.LFB441:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	testq	%rdi, %rdi
	je	.L490
	movq	(%rdi), %r12
	testq	%r12, %r12
	je	.L490
	cmpq	$0, 24(%r12)
	je	.L490
	movq	%rsi, %rbx
	movq	(%rsi), %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movq	%rdx, %r13
	call	EC_KEY_oct2key@PLT
	testl	%eax, %eax
	je	.L501
	addq	%r13, (%rbx)
.L489:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L490:
	.cfi_restore_state
	movl	$1170, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$67, %edx
	xorl	%r12d, %r12d
	movl	$152, %esi
	movl	$16, %edi
	call	ERR_put_error@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L501:
	.cfi_restore_state
	movl	$1175, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$16, %edx
	xorl	%r12d, %r12d
	movl	$152, %esi
	movl	$16, %edi
	call	ERR_put_error@PLT
	jmp	.L489
	.cfi_endproc
.LFE441:
	.size	o2i_ECPublicKey, .-o2i_ECPublicKey
	.p2align 4
	.globl	i2o_ECPublicKey
	.type	i2o_ECPublicKey, @function
i2o_ECPublicKey:
.LFB442:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	testq	%rdi, %rdi
	je	.L516
	movl	52(%rdi), %edx
	movq	%rdi, %rbx
	movq	%rsi, %r13
	movq	32(%rdi), %rsi
	movq	24(%rdi), %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	call	EC_POINT_point2oct@PLT
	movq	%rax, %r12
	testq	%r13, %r13
	je	.L512
	testq	%rax, %rax
	je	.L512
	movq	0(%r13), %rcx
	testq	%rcx, %rcx
	je	.L517
	movl	52(%rbx), %edx
	movq	32(%rbx), %rsi
	xorl	%r9d, %r9d
	movq	%rax, %r8
	movq	24(%rbx), %rdi
	call	EC_POINT_point2oct@PLT
	testq	%rax, %rax
	je	.L518
	addq	%r12, 0(%r13)
.L512:
	movl	%r12d, %eax
.L502:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L517:
	.cfi_restore_state
	movl	$1200, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rax, %rdi
	call	CRYPTO_malloc@PLT
	movq	%rax, 0(%r13)
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L519
	movl	52(%rbx), %edx
	movq	32(%rbx), %rsi
	xorl	%r9d, %r9d
	movq	%r12, %r8
	movq	24(%rbx), %rdi
	call	EC_POINT_point2oct@PLT
	testq	%rax, %rax
	jne	.L512
	movl	$1208, %r8d
	movl	$16, %edx
	movl	$151, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	0(%r13), %rdi
	movl	$1210, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 0(%r13)
	xorl	%eax, %eax
	jmp	.L502
	.p2align 4,,10
	.p2align 3
.L516:
	movl	$1188, %r8d
	movl	$67, %edx
	movl	$151, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L518:
	.cfi_restore_state
	movl	$1208, %r8d
	movl	$16, %edx
	movl	$151, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L502
	.p2align 4,,10
	.p2align 3
.L519:
	movl	$1201, %r8d
	movl	$65, %edx
	movl	$151, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L502
	.cfi_endproc
.LFE442:
	.size	i2o_ECPublicKey, .-i2o_ECPublicKey
	.p2align 4
	.globl	d2i_ECDSA_SIG
	.type	d2i_ECDSA_SIG, @function
d2i_ECDSA_SIG:
.LFB443:
	.cfi_startproc
	endbr64
	leaq	ECDSA_SIG_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE443:
	.size	d2i_ECDSA_SIG, .-d2i_ECDSA_SIG
	.p2align 4
	.globl	i2d_ECDSA_SIG
	.type	i2d_ECDSA_SIG, @function
i2d_ECDSA_SIG:
.LFB444:
	.cfi_startproc
	endbr64
	leaq	ECDSA_SIG_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE444:
	.size	i2d_ECDSA_SIG, .-i2d_ECDSA_SIG
	.p2align 4
	.globl	ECDSA_SIG_new
	.type	ECDSA_SIG_new, @function
ECDSA_SIG_new:
.LFB445:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1231, %edx
	movl	$16, %edi
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	call	CRYPTO_zalloc@PLT
	testq	%rax, %rax
	je	.L525
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L525:
	.cfi_restore_state
	movl	$1233, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	movl	$265, %esi
	movl	$16, %edi
	movq	%rax, -8(%rbp)
	call	ERR_put_error@PLT
	movq	-8(%rbp), %rax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE445:
	.size	ECDSA_SIG_new, .-ECDSA_SIG_new
	.p2align 4
	.globl	ECDSA_SIG_free
	.type	ECDSA_SIG_free, @function
ECDSA_SIG_free:
.LFB446:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L526
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	(%rdi), %rdi
	call	BN_clear_free@PLT
	movq	8(%r12), %rdi
	call	BN_clear_free@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$1243, %edx
	popq	%r12
	.cfi_restore 12
	leaq	.LC0(%rip), %rsi
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_free@PLT
	.p2align 4,,10
	.p2align 3
.L526:
	ret
	.cfi_endproc
.LFE446:
	.size	ECDSA_SIG_free, .-ECDSA_SIG_free
	.p2align 4
	.globl	ECDSA_SIG_get0
	.type	ECDSA_SIG_get0, @function
ECDSA_SIG_get0:
.LFB447:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L532
	movq	(%rdi), %rax
	movq	%rax, (%rsi)
.L532:
	testq	%rdx, %rdx
	je	.L531
	movq	8(%rdi), %rax
	movq	%rax, (%rdx)
.L531:
	ret
	.cfi_endproc
.LFE447:
	.size	ECDSA_SIG_get0, .-ECDSA_SIG_get0
	.p2align 4
	.globl	ECDSA_SIG_get0_r
	.type	ECDSA_SIG_get0_r, @function
ECDSA_SIG_get0_r:
.LFB448:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE448:
	.size	ECDSA_SIG_get0_r, .-ECDSA_SIG_get0_r
	.p2align 4
	.globl	ECDSA_SIG_get0_s
	.type	ECDSA_SIG_get0_s, @function
ECDSA_SIG_get0_s:
.LFB449:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	ret
	.cfi_endproc
.LFE449:
	.size	ECDSA_SIG_get0_s, .-ECDSA_SIG_get0_s
	.p2align 4
	.globl	ECDSA_SIG_set0
	.type	ECDSA_SIG_set0, @function
ECDSA_SIG_set0:
.LFB450:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L544
	testq	%rdx, %rdx
	je	.L544
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %xmm0
	movq	%rdx, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rdi), %rdi
	movaps	%xmm0, -32(%rbp)
	call	BN_clear_free@PLT
	movq	8(%rbx), %rdi
	call	BN_clear_free@PLT
	movdqa	-32(%rbp), %xmm0
	movl	$1, %eax
	movups	%xmm0, (%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L544:
	.cfi_restore 3
	.cfi_restore 6
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE450:
	.size	ECDSA_SIG_set0, .-ECDSA_SIG_set0
	.p2align 4
	.globl	ECDSA_size
	.type	ECDSA_size, @function
ECDSA_size:
.LFB451:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L552
	call	EC_KEY_get0_group@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L552
	call	EC_GROUP_order_bits@PLT
	testl	%eax, %eax
	je	.L552
	leal	14(%rax), %edx
	addl	$7, %eax
	leaq	-48(%rbp), %rdi
	movb	$-1, -12(%rbp)
	cmovs	%edx, %eax
	xorl	%esi, %esi
	movl	$2, -44(%rbp)
	sarl	$3, %eax
	movl	%eax, -48(%rbp)
	leaq	-12(%rbp), %rax
	movq	%rax, -40(%rbp)
	call	i2d_ASN1_INTEGER@PLT
	movl	$16, %edx
	movl	$1, %edi
	leal	(%rax,%rax), %esi
	call	ASN1_object_size@PLT
	movl	$0, %edx
	testl	%eax, %eax
	cmovs	%edx, %eax
	jmp	.L549
	.p2align 4,,10
	.p2align 3
.L552:
	xorl	%eax, %eax
.L549:
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L561
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L561:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE451:
	.size	ECDSA_size, .-ECDSA_size
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"ECDSA_SIG"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	ECDSA_SIG_it, @object
	.size	ECDSA_SIG_it, 56
ECDSA_SIG_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	ECDSA_SIG_seq_tt
	.quad	2
	.quad	0
	.quad	16
	.quad	.LC1
	.section	.rodata.str1.1
.LC2:
	.string	"r"
.LC3:
	.string	"s"
	.section	.data.rel.ro,"aw"
	.align 32
	.type	ECDSA_SIG_seq_tt, @object
	.size	ECDSA_SIG_seq_tt, 80
ECDSA_SIG_seq_tt:
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC2
	.quad	CBIGNUM_it
	.quad	0
	.quad	0
	.quad	8
	.quad	.LC3
	.quad	CBIGNUM_it
	.section	.rodata.str1.1
.LC4:
	.string	"EC_PRIVATEKEY"
	.section	.data.rel.ro.local
	.align 32
	.type	EC_PRIVATEKEY_it, @object
	.size	EC_PRIVATEKEY_it, 56
EC_PRIVATEKEY_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	EC_PRIVATEKEY_seq_tt
	.quad	4
	.quad	0
	.quad	32
	.quad	.LC4
	.section	.rodata.str1.1
.LC5:
	.string	"version"
.LC6:
	.string	"privateKey"
.LC7:
	.string	"parameters"
.LC8:
	.string	"publicKey"
	.section	.data.rel.ro
	.align 32
	.type	EC_PRIVATEKEY_seq_tt, @object
	.size	EC_PRIVATEKEY_seq_tt, 160
EC_PRIVATEKEY_seq_tt:
	.quad	4096
	.quad	0
	.quad	0
	.quad	.LC5
	.quad	INT32_it
	.quad	0
	.quad	0
	.quad	8
	.quad	.LC6
	.quad	ASN1_OCTET_STRING_it
	.quad	145
	.quad	0
	.quad	16
	.quad	.LC7
	.quad	ECPKPARAMETERS_it
	.quad	145
	.quad	1
	.quad	24
	.quad	.LC8
	.quad	ASN1_BIT_STRING_it
	.globl	ECPKPARAMETERS_it
	.section	.rodata.str1.1
.LC9:
	.string	"ECPKPARAMETERS"
	.section	.data.rel.ro.local
	.align 32
	.type	ECPKPARAMETERS_it, @object
	.size	ECPKPARAMETERS_it, 56
ECPKPARAMETERS_it:
	.byte	2
	.zero	7
	.quad	0
	.quad	ECPKPARAMETERS_ch_tt
	.quad	3
	.quad	0
	.quad	16
	.quad	.LC9
	.section	.rodata.str1.1
.LC10:
	.string	"value.named_curve"
.LC11:
	.string	"value.parameters"
.LC12:
	.string	"value.implicitlyCA"
	.section	.data.rel.ro
	.align 32
	.type	ECPKPARAMETERS_ch_tt, @object
	.size	ECPKPARAMETERS_ch_tt, 120
ECPKPARAMETERS_ch_tt:
	.quad	0
	.quad	0
	.quad	8
	.quad	.LC10
	.quad	ASN1_OBJECT_it
	.quad	0
	.quad	0
	.quad	8
	.quad	.LC11
	.quad	ECPARAMETERS_it
	.quad	0
	.quad	0
	.quad	8
	.quad	.LC12
	.quad	ASN1_NULL_it
	.globl	ECPARAMETERS_it
	.section	.rodata.str1.1
.LC13:
	.string	"ECPARAMETERS"
	.section	.data.rel.ro.local
	.align 32
	.type	ECPARAMETERS_it, @object
	.size	ECPARAMETERS_it, 56
ECPARAMETERS_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	ECPARAMETERS_seq_tt
	.quad	6
	.quad	0
	.quad	48
	.quad	.LC13
	.section	.rodata.str1.1
.LC14:
	.string	"fieldID"
.LC15:
	.string	"curve"
.LC16:
	.string	"base"
.LC17:
	.string	"order"
.LC18:
	.string	"cofactor"
	.section	.data.rel.ro
	.align 32
	.type	ECPARAMETERS_seq_tt, @object
	.size	ECPARAMETERS_seq_tt, 240
ECPARAMETERS_seq_tt:
	.quad	4096
	.quad	0
	.quad	0
	.quad	.LC5
	.quad	INT32_it
	.quad	0
	.quad	0
	.quad	8
	.quad	.LC14
	.quad	X9_62_FIELDID_it
	.quad	0
	.quad	0
	.quad	16
	.quad	.LC15
	.quad	X9_62_CURVE_it
	.quad	0
	.quad	0
	.quad	24
	.quad	.LC16
	.quad	ASN1_OCTET_STRING_it
	.quad	0
	.quad	0
	.quad	32
	.quad	.LC17
	.quad	ASN1_INTEGER_it
	.quad	1
	.quad	0
	.quad	40
	.quad	.LC18
	.quad	ASN1_INTEGER_it
	.section	.rodata.str1.1
.LC19:
	.string	"X9_62_CURVE"
	.section	.data.rel.ro.local
	.align 32
	.type	X9_62_CURVE_it, @object
	.size	X9_62_CURVE_it, 56
X9_62_CURVE_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	X9_62_CURVE_seq_tt
	.quad	3
	.quad	0
	.quad	24
	.quad	.LC19
	.section	.rodata.str1.1
.LC20:
	.string	"a"
.LC21:
	.string	"b"
.LC22:
	.string	"seed"
	.section	.data.rel.ro
	.align 32
	.type	X9_62_CURVE_seq_tt, @object
	.size	X9_62_CURVE_seq_tt, 120
X9_62_CURVE_seq_tt:
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC20
	.quad	ASN1_OCTET_STRING_it
	.quad	0
	.quad	0
	.quad	8
	.quad	.LC21
	.quad	ASN1_OCTET_STRING_it
	.quad	1
	.quad	0
	.quad	16
	.quad	.LC22
	.quad	ASN1_BIT_STRING_it
	.section	.rodata.str1.1
.LC23:
	.string	"X9_62_FIELDID"
	.section	.data.rel.ro.local
	.align 32
	.type	X9_62_FIELDID_it, @object
	.size	X9_62_FIELDID_it, 56
X9_62_FIELDID_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	X9_62_FIELDID_seq_tt
	.quad	2
	.quad	0
	.quad	16
	.quad	.LC23
	.section	.rodata.str1.1
.LC24:
	.string	"fieldType"
	.section	.data.rel.ro
	.align 32
	.type	X9_62_FIELDID_seq_tt, @object
	.size	X9_62_FIELDID_seq_tt, 80
X9_62_FIELDID_seq_tt:
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC24
	.quad	ASN1_OBJECT_it
	.quad	256
	.quad	-1
	.quad	0
	.quad	.LC23
	.quad	X9_62_FIELDID_adb
	.section	.data.rel.ro.local
	.align 32
	.type	X9_62_FIELDID_adb, @object
	.size	X9_62_FIELDID_adb, 56
X9_62_FIELDID_adb:
	.quad	0
	.quad	0
	.quad	0
	.quad	X9_62_FIELDID_adbtbl
	.quad	2
	.quad	fieldID_def_tt
	.quad	0
	.section	.rodata.str1.1
.LC25:
	.string	"p.prime"
.LC26:
	.string	"p.char_two"
	.section	.data.rel.ro
	.align 32
	.type	X9_62_FIELDID_adbtbl, @object
	.size	X9_62_FIELDID_adbtbl, 96
X9_62_FIELDID_adbtbl:
	.quad	406
	.quad	0
	.quad	0
	.quad	8
	.quad	.LC25
	.quad	ASN1_INTEGER_it
	.quad	407
	.quad	0
	.quad	0
	.quad	8
	.quad	.LC26
	.quad	X9_62_CHARACTERISTIC_TWO_it
	.section	.rodata.str1.1
.LC27:
	.string	"p.other"
	.section	.data.rel.ro
	.align 32
	.type	fieldID_def_tt, @object
	.size	fieldID_def_tt, 40
fieldID_def_tt:
	.quad	0
	.quad	0
	.quad	8
	.quad	.LC27
	.quad	ASN1_ANY_it
	.section	.rodata.str1.1
.LC28:
	.string	"X9_62_CHARACTERISTIC_TWO"
	.section	.data.rel.ro.local
	.align 32
	.type	X9_62_CHARACTERISTIC_TWO_it, @object
	.size	X9_62_CHARACTERISTIC_TWO_it, 56
X9_62_CHARACTERISTIC_TWO_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	X9_62_CHARACTERISTIC_TWO_seq_tt
	.quad	3
	.quad	0
	.quad	24
	.quad	.LC28
	.section	.rodata.str1.1
.LC29:
	.string	"m"
.LC30:
	.string	"type"
	.section	.data.rel.ro
	.align 32
	.type	X9_62_CHARACTERISTIC_TWO_seq_tt, @object
	.size	X9_62_CHARACTERISTIC_TWO_seq_tt, 120
X9_62_CHARACTERISTIC_TWO_seq_tt:
	.quad	4096
	.quad	0
	.quad	0
	.quad	.LC29
	.quad	INT32_it
	.quad	0
	.quad	0
	.quad	8
	.quad	.LC30
	.quad	ASN1_OBJECT_it
	.quad	256
	.quad	-1
	.quad	0
	.quad	.LC28
	.quad	X9_62_CHARACTERISTIC_TWO_adb
	.section	.data.rel.ro.local
	.align 32
	.type	X9_62_CHARACTERISTIC_TWO_adb, @object
	.size	X9_62_CHARACTERISTIC_TWO_adb, 56
X9_62_CHARACTERISTIC_TWO_adb:
	.quad	0
	.quad	8
	.quad	0
	.quad	X9_62_CHARACTERISTIC_TWO_adbtbl
	.quad	3
	.quad	char_two_def_tt
	.quad	0
	.section	.rodata.str1.1
.LC31:
	.string	"p.onBasis"
.LC32:
	.string	"p.tpBasis"
.LC33:
	.string	"p.ppBasis"
	.section	.data.rel.ro
	.align 32
	.type	X9_62_CHARACTERISTIC_TWO_adbtbl, @object
	.size	X9_62_CHARACTERISTIC_TWO_adbtbl, 144
X9_62_CHARACTERISTIC_TWO_adbtbl:
	.quad	681
	.quad	0
	.quad	0
	.quad	16
	.quad	.LC31
	.quad	ASN1_NULL_it
	.quad	682
	.quad	0
	.quad	0
	.quad	16
	.quad	.LC32
	.quad	ASN1_INTEGER_it
	.quad	683
	.quad	0
	.quad	0
	.quad	16
	.quad	.LC33
	.quad	X9_62_PENTANOMIAL_it
	.align 32
	.type	char_two_def_tt, @object
	.size	char_two_def_tt, 40
char_two_def_tt:
	.quad	0
	.quad	0
	.quad	16
	.quad	.LC27
	.quad	ASN1_ANY_it
	.section	.rodata.str1.1
.LC34:
	.string	"X9_62_PENTANOMIAL"
	.section	.data.rel.ro.local
	.align 32
	.type	X9_62_PENTANOMIAL_it, @object
	.size	X9_62_PENTANOMIAL_it, 56
X9_62_PENTANOMIAL_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	X9_62_PENTANOMIAL_seq_tt
	.quad	3
	.quad	0
	.quad	12
	.quad	.LC34
	.section	.rodata.str1.1
.LC35:
	.string	"k1"
.LC36:
	.string	"k2"
.LC37:
	.string	"k3"
	.section	.data.rel.ro
	.align 32
	.type	X9_62_PENTANOMIAL_seq_tt, @object
	.size	X9_62_PENTANOMIAL_seq_tt, 120
X9_62_PENTANOMIAL_seq_tt:
	.quad	4096
	.quad	0
	.quad	0
	.quad	.LC35
	.quad	INT32_it
	.quad	4096
	.quad	0
	.quad	4
	.quad	.LC36
	.quad	INT32_it
	.quad	4096
	.quad	0
	.quad	8
	.quad	.LC37
	.quad	INT32_it
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
