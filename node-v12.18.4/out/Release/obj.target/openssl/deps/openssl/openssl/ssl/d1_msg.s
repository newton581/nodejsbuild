	.file	"d1_msg.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/ssl/d1_msg.c"
	.text
	.p2align 4
	.globl	dtls1_write_app_data_bytes
	.type	dtls1_write_app_data_bytes, @function
dtls1_write_app_data_bytes:
.LFB964:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%esi, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$8, %rsp
	call	SSL_in_init@PLT
	testl	%eax, %eax
	jne	.L10
.L2:
	cmpq	$16384, %r13
	ja	.L11
	addq	$8, %rsp
	movq	%rbx, %r8
	movq	%r13, %rcx
	movq	%r15, %rdx
	popq	%rbx
	movl	%r14d, %esi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	dtls1_write_bytes@PLT
	.p2align 4,,10
	.p2align 3
.L10:
	.cfi_restore_state
	movq	%r12, %rdi
	call	ossl_statem_get_in_handshake@PLT
	testl	%eax, %eax
	jne	.L2
	movq	%r12, %rdi
	call	*48(%r12)
	testl	%eax, %eax
	js	.L1
	jne	.L2
	movl	$22, %r8d
	movl	$229, %edx
	movl	$268, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	orl	$-1, %eax
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L11:
	.cfi_restore_state
	movl	$29, %r8d
	movl	$334, %edx
	movl	$268, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-1, %eax
	jmp	.L1
	.cfi_endproc
.LFE964:
	.size	dtls1_write_app_data_bytes, .-dtls1_write_app_data_bytes
	.p2align 4
	.globl	dtls1_dispatch_alert
	.type	dtls1_dispatch_alert, @function
dtls1_dispatch_alert:
.LFB965:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r8d, %r8d
	movl	$2, %ecx
	movl	$21, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	leaq	-26(%rbp), %rdx
	leaq	-40(%rbp), %r9
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	168(%rdi), %rax
	movl	$0, 252(%rax)
	movzwl	256(%rax), %eax
	movw	%ax, -26(%rbp)
	call	do_dtls1_write@PLT
	movl	%eax, %r13d
	testl	%eax, %eax
	jg	.L13
	movq	168(%r12), %rax
	movl	$1, 252(%rax)
.L12:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L22
	leaq	-16(%rbp), %rsp
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	.cfi_restore_state
	movq	24(%r12), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	call	BIO_ctrl@PLT
	movq	184(%r12), %rax
	testq	%rax, %rax
	je	.L15
	movq	168(%r12), %rsi
	subq	$8, %rsp
	movl	$21, %edx
	movq	%r12, %r9
	pushq	192(%r12)
	movl	$2, %r8d
	movl	$1, %edi
	leaq	256(%rsi), %rcx
	movl	(%r12), %esi
	call	*%rax
	popq	%rax
	popq	%rdx
.L15:
	movq	1392(%r12), %rax
	testq	%rax, %rax
	je	.L23
.L16:
	movq	168(%r12), %rdx
	movl	$16392, %esi
	movq	%r12, %rdi
	movzwl	256(%rdx), %edx
	rolw	$8, %dx
	movzwl	%dx, %edx
	call	*%rax
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L23:
	movq	1440(%r12), %rax
	movq	272(%rax), %rax
	testq	%rax, %rax
	jne	.L16
	jmp	.L12
.L22:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE965:
	.size	dtls1_dispatch_alert, .-dtls1_dispatch_alert
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
