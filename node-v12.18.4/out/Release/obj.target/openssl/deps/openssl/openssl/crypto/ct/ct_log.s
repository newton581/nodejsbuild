	.file	"ct_log.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/ct/ct_log.c"
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"description"
.LC2:
	.string	"key"
	.text
	.p2align 4
	.type	ctlog_store_load_log, @function
ctlog_store_load_log:
.LFB919:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -48(%rbp)
	testq	%rdi, %rdi
	je	.L19
	movq	%rdx, %rbx
	movslq	%esi, %rsi
	movl	$167, %ecx
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_strndup@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L11
	movq	8(%rbx), %r14
	leaq	.LC1(%rip), %rdx
	movq	%rax, %rsi
	movq	%r14, %rdi
	call	NCONF_get_string@PLT
	movl	$125, %r8d
	movl	$111, %edx
	leaq	.LC0(%rip), %rcx
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L18
	movq	%r12, %rsi
	leaq	.LC2(%rip), %rdx
	movq	%r14, %rdi
	call	NCONF_get_string@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L21
	movq	%r13, %rdx
	leaq	-48(%rbp), %rdi
	call	CTLOG_new_from_base64@PLT
	movl	$172, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movl	%eax, %r13d
	call	CRYPTO_free@PLT
	testl	%r13d, %r13d
	js	.L1
	je	.L8
	movq	(%rbx), %rax
	movq	-48(%rbp), %rsi
	movq	(%rax), %rdi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L11
.L19:
	movl	$1, %r13d
.L1:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L22
	addq	$16, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	.cfi_restore_state
	movl	$131, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$112, %edx
.L18:
	movl	$119, %esi
	movl	$50, %edi
	call	ERR_put_error@PLT
	movl	$172, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
.L8:
	addq	$1, 16(%rbx)
	jmp	.L19
.L11:
	movq	-48(%rbp), %r12
	testq	%r12, %r12
	je	.L6
	movq	(%r12), %rdi
	movl	$266, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	40(%r12), %rdi
	call	EVP_PKEY_free@PLT
	movl	$268, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
.L6:
	movl	$191, %r8d
	movl	$65, %edx
	movl	$130, %esi
	movl	$50, %edi
	leaq	.LC0(%rip), %rcx
	movl	$-1, %r13d
	call	ERR_put_error@PLT
	jmp	.L1
.L22:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE919:
	.size	ctlog_store_load_log, .-ctlog_store_load_log
	.p2align 4
	.globl	CTLOG_free
	.type	CTLOG_free, @function
CTLOG_free:
.LFB922:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L23
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$266, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	(%rdi), %rdi
	call	CRYPTO_free@PLT
	movq	40(%r12), %rdi
	call	EVP_PKEY_free@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$268, %edx
	popq	%r12
	.cfi_restore 12
	leaq	.LC0(%rip), %rsi
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_free@PLT
	.p2align 4,,10
	.p2align 3
.L23:
	ret
	.cfi_endproc
.LFE922:
	.size	CTLOG_free, .-CTLOG_free
	.p2align 4
	.globl	CTLOG_STORE_new
	.type	CTLOG_STORE_new, @function
CTLOG_STORE_new:
.LFB915:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$94, %edx
	movl	$8, %edi
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	CRYPTO_zalloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L32
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, (%r12)
	testq	%rax, %rax
	je	.L33
.L28:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	.cfi_restore_state
	movl	$97, %r8d
	movl	$65, %edx
	movl	$131, %esi
	movl	$50, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L33:
	movq	%r12, %rdi
	movl	$107, %edx
	leaq	.LC0(%rip), %rsi
	xorl	%r12d, %r12d
	call	CRYPTO_free@PLT
	jmp	.L28
	.cfi_endproc
.LFE915:
	.size	CTLOG_STORE_new, .-CTLOG_STORE_new
	.p2align 4
	.globl	CTLOG_STORE_free
	.type	CTLOG_STORE_free, @function
CTLOG_STORE_free:
.LFB916:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L34
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	CTLOG_free(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	(%rdi), %rdi
	call	OPENSSL_sk_pop_free@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$115, %edx
	popq	%r12
	.cfi_restore 12
	leaq	.LC0(%rip), %rsi
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_free@PLT
	.p2align 4,,10
	.p2align 3
.L34:
	ret
	.cfi_endproc
.LFE916:
	.size	CTLOG_STORE_free, .-CTLOG_STORE_free
	.section	.rodata.str1.1
.LC3:
	.string	"enabled_logs"
	.text
	.p2align 4
	.globl	CTLOG_STORE_load_file
	.type	CTLOG_STORE_load_file, @function
CTLOG_STORE_load_file:
.LFB920:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$59, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	leaq	.LC0(%rip), %rsi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movl	$24, %edi
	call	CRYPTO_zalloc@PLT
	testq	%rax, %rax
	je	.L55
	movq	%rbx, (%rax)
	xorl	%edi, %edi
	movq	%rax, %r12
	xorl	%r14d, %r14d
	call	NCONF_new@PLT
	movq	%rax, 8(%r12)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L43
	xorl	%edx, %edx
	movq	%r13, %rsi
	call	NCONF_load@PLT
	movl	$209, %r8d
	testl	%eax, %eax
	jle	.L54
	movq	8(%r12), %rdi
	leaq	.LC3(%rip), %rdx
	xorl	%esi, %esi
	call	NCONF_get_string@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L56
	movq	%r12, %r8
	movl	$1, %edx
	movl	$44, %esi
	leaq	ctlog_store_load_log(%rip), %rcx
	call	CONF_parse_list@PLT
	testl	%eax, %eax
	je	.L45
	cmpq	$0, 16(%r12)
	jne	.L45
	movq	8(%r12), %rdi
	movl	$1, %r14d
.L43:
	call	NCONF_free@PLT
	movq	%r12, %rdi
	movl	$69, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	popq	%rbx
	movl	%r14d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L45:
	.cfi_restore_state
	movl	$50, %edi
	movl	$221, %r8d
	movl	$109, %edx
	xorl	%r14d, %r14d
	leaq	.LC0(%rip), %rcx
	movl	$123, %esi
	call	ERR_put_error@PLT
	movq	8(%r12), %rdi
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L56:
	movl	$215, %r8d
.L54:
	movl	$50, %edi
	movl	$109, %edx
	movl	$123, %esi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	8(%r12), %rdi
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L55:
	xorl	%r14d, %r14d
	movl	$62, %r8d
	movl	$65, %edx
	movl	$122, %esi
	leaq	.LC0(%rip), %rcx
	movl	$50, %edi
	call	ERR_put_error@PLT
	popq	%rbx
	movl	%r14d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE920:
	.size	CTLOG_STORE_load_file, .-CTLOG_STORE_load_file
	.section	.rodata.str1.1
.LC4:
	.string	"/etc/ssl/ct_log_list.cnf"
.LC5:
	.string	"CTLOG_FILE"
	.text
	.p2align 4
	.globl	CTLOG_STORE_load_default_file
	.type	CTLOG_STORE_load_default_file, @function
CTLOG_STORE_load_default_file:
.LFB918:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	.LC5(%rip), %rdi
	subq	$8, %rsp
	call	ossl_safe_getenv@PLT
	movq	%r12, %rdi
	testq	%rax, %rax
	movq	%rax, %rsi
	leaq	.LC4(%rip), %rax
	cmove	%rax, %rsi
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	CTLOG_STORE_load_file
	.cfi_endproc
.LFE918:
	.size	CTLOG_STORE_load_default_file, .-CTLOG_STORE_load_default_file
	.p2align 4
	.globl	CTLOG_new
	.type	CTLOG_new, @function
CTLOG_new:
.LFB921:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$239, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	leaq	.LC0(%rip), %rsi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movl	$48, %edi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	CRYPTO_zalloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L68
	movl	$246, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r13, %rdi
	call	CRYPTO_strdup@PLT
	movq	%rax, (%r12)
	testq	%rax, %rax
	je	.L69
	leaq	-48(%rbp), %rsi
	movq	%rbx, %rdi
	movq	$0, -48(%rbp)
	call	i2d_PUBKEY@PLT
	testl	%eax, %eax
	jle	.L70
	movq	-48(%rbp), %rdi
	leaq	8(%r12), %rdx
	movslq	%eax, %rsi
	call	SHA256@PLT
	movq	-48(%rbp), %rdi
	movl	$88, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%rbx, 40(%r12)
.L60:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L71
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L70:
	.cfi_restore_state
	movl	$113, %edx
	movl	$125, %esi
	movl	$50, %edi
	movl	$81, %r8d
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	-48(%rbp), %rdi
	movl	$88, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
.L64:
	movq	(%r12), %rdi
	movl	$266, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	40(%r12), %rdi
	call	EVP_PKEY_free@PLT
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	movl	$268, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L68:
	movl	$242, %r8d
	movl	$65, %edx
	movl	$117, %esi
	movl	$50, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L69:
	movl	$248, %r8d
	movl	$65, %edx
	movl	$117, %esi
	movl	$50, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L64
.L71:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE921:
	.size	CTLOG_new, .-CTLOG_new
	.p2align 4
	.globl	CTLOG_get0_name
	.type	CTLOG_get0_name, @function
CTLOG_get0_name:
.LFB923:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE923:
	.size	CTLOG_get0_name, .-CTLOG_get0_name
	.p2align 4
	.globl	CTLOG_get0_log_id
	.type	CTLOG_get0_log_id, @function
CTLOG_get0_log_id:
.LFB924:
	.cfi_startproc
	endbr64
	addq	$8, %rdi
	movq	%rdi, (%rsi)
	movq	$32, (%rdx)
	ret
	.cfi_endproc
.LFE924:
	.size	CTLOG_get0_log_id, .-CTLOG_get0_log_id
	.p2align 4
	.globl	CTLOG_get0_public_key
	.type	CTLOG_get0_public_key, @function
CTLOG_get0_public_key:
.LFB925:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	ret
	.cfi_endproc
.LFE925:
	.size	CTLOG_get0_public_key, .-CTLOG_get0_public_key
	.p2align 4
	.globl	CTLOG_STORE_get0_log_by_id
	.type	CTLOG_STORE_get0_log_by_id, @function
CTLOG_STORE_get0_log_by_id:
.LFB926:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L78:
	movq	(%rbx), %rdi
	movl	%r15d, %esi
	call	OPENSSL_sk_value@PLT
	movq	%r13, %rdx
	movq	%r14, %rsi
	leaq	8(%rax), %rdi
	movq	%rax, %r12
	call	memcmp@PLT
	testl	%eax, %eax
	je	.L75
	addl	$1, %r15d
.L76:
	movq	(%rbx), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r15d
	jl	.L78
	xorl	%r12d, %r12d
.L75:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE926:
	.size	CTLOG_STORE_get0_log_by_id, .-CTLOG_STORE_get0_log_by_id
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
