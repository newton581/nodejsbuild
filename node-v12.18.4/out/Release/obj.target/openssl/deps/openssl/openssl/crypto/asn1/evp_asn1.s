	.file	"evp_asn1.c"
	.text
	.p2align 4
	.globl	ASN1_TYPE_set_octetstring
	.type	ASN1_TYPE_set_octetstring, @function
ASN1_TYPE_set_octetstring:
.LFB443:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	subq	$16, %rsp
	.cfi_offset 12, -48
	call	ASN1_OCTET_STRING_new@PLT
	movq	%rax, %r12
	xorl	%eax, %eax
	testq	%r12, %r12
	je	.L1
	movl	%r15d, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	ASN1_OCTET_STRING_set@PLT
	testl	%eax, %eax
	je	.L8
	movq	%r12, %rdx
	movl	$4, %esi
	movq	%r13, %rdi
	call	ASN1_TYPE_set@PLT
	movl	$1, %eax
.L1:
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	.cfi_restore_state
	movq	%r12, %rdi
	movl	%eax, -36(%rbp)
	call	ASN1_OCTET_STRING_free@PLT
	movl	-36(%rbp), %eax
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE443:
	.size	ASN1_TYPE_set_octetstring, .-ASN1_TYPE_set_octetstring
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/asn1/evp_asn1.c"
	.text
	.p2align 4
	.globl	ASN1_TYPE_get_octetstring
	.type	ASN1_TYPE_get_octetstring, @function
ASN1_TYPE_get_octetstring:
.LFB444:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	cmpl	$4, (%rdi)
	jne	.L10
	movq	%rdi, %r12
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L10
	movl	%edx, %ebx
	movq	%rsi, %r13
	call	ASN1_STRING_get0_data@PLT
	movq	8(%r12), %rdi
	movq	%rax, %r14
	call	ASN1_STRING_length@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	cmpl	%ebx, %eax
	movl	%eax, %r12d
	cmovle	%eax, %ebx
	movslq	%ebx, %rdx
	call	memcpy@PLT
.L9:
	popq	%rbx
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	.cfi_restore_state
	movl	$36, %r8d
	movl	$109, %edx
	movl	$135, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	movl	$-1, %r12d
	call	ERR_put_error@PLT
	jmp	.L9
	.cfi_endproc
.LFE444:
	.size	ASN1_TYPE_get_octetstring, .-ASN1_TYPE_get_octetstring
	.p2align 4
	.globl	ASN1_TYPE_set_int_octetstring
	.type	ASN1_TYPE_set_int_octetstring, @function
ASN1_TYPE_set_int_octetstring:
.LFB445:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$64, %rsp
	movq	%rdi, -56(%rbp)
	leaq	asn1_int_oct_it(%rip), %rdi
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movl	%esi, -48(%rbp)
	leaq	-32(%rbp), %rax
	leaq	-48(%rbp), %rsi
	movq	%rdx, -24(%rbp)
	leaq	-56(%rbp), %rdx
	movq	%rax, -40(%rbp)
	movl	$4, -28(%rbp)
	movl	%ecx, -32(%rbp)
	movq	$0, -16(%rbp)
	call	ASN1_TYPE_pack_sequence@PLT
	testq	%rax, %rax
	setne	%al
	movq	-8(%rbp), %rdi
	xorq	%fs:40, %rdi
	jne	.L17
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movzbl	%al, %eax
	ret
.L17:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE445:
	.size	ASN1_TYPE_set_int_octetstring, .-ASN1_TYPE_set_int_octetstring
	.p2align 4
	.globl	ASN1_TYPE_get_int_octetstring
	.type	ASN1_TYPE_get_int_octetstring, @function
ASN1_TYPE_get_int_octetstring:
.LFB446:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	cmpl	$16, (%rdi)
	jne	.L21
	cmpq	$0, 8(%rdi)
	je	.L21
	movq	%rsi, %r13
	movq	%rdi, %rsi
	leaq	asn1_int_oct_it(%rip), %rdi
	movq	%rdx, %r14
	movl	%ecx, %ebx
	call	ASN1_TYPE_unpack_sequence@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L21
	testq	%r13, %r13
	je	.L22
	movslq	(%rax), %rax
	movq	%rax, 0(%r13)
.L22:
	movq	8(%r12), %rdi
	call	ASN1_STRING_length@PLT
	movl	%eax, %r13d
	testq	%r14, %r14
	je	.L23
	movq	8(%r12), %rdi
	call	ASN1_STRING_get0_data@PLT
	cmpl	%ebx, %r13d
	movq	%r14, %rdi
	cmovle	%r13d, %ebx
	movq	%rax, %rsi
	movslq	%ebx, %rdx
	call	memcpy@PLT
.L23:
	cmpl	$-1, %r13d
	jne	.L24
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L21:
	xorl	%r12d, %r12d
.L20:
	movl	$111, %r8d
	movl	$109, %edx
	movl	$134, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	movl	$-1, %r13d
	call	ERR_put_error@PLT
.L24:
	movq	%r12, %rdi
	leaq	asn1_int_oct_it(%rip), %rsi
	call	ASN1_item_free@PLT
	popq	%rbx
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE446:
	.size	ASN1_TYPE_get_int_octetstring, .-ASN1_TYPE_get_int_octetstring
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"asn1_int_oct"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	asn1_int_oct_it, @object
	.size	asn1_int_oct_it, 56
asn1_int_oct_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	asn1_int_oct_seq_tt
	.quad	2
	.quad	0
	.quad	16
	.quad	.LC1
	.section	.rodata.str1.1
.LC2:
	.string	"num"
.LC3:
	.string	"oct"
	.section	.data.rel.ro,"aw"
	.align 32
	.type	asn1_int_oct_seq_tt, @object
	.size	asn1_int_oct_seq_tt, 80
asn1_int_oct_seq_tt:
	.quad	4096
	.quad	0
	.quad	0
	.quad	.LC2
	.quad	INT32_it
	.quad	0
	.quad	0
	.quad	8
	.quad	.LC3
	.quad	ASN1_OCTET_STRING_it
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
