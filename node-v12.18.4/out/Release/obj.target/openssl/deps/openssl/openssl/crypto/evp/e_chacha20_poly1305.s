	.file	"e_chacha20_poly1305.c"
	.text
	.p2align 4
	.type	chacha_init_key, @function
chacha_init_key:
.LFB469:
	.cfi_startproc
	endbr64
	movq	120(%rdi), %rax
	testq	%rsi, %rsi
	je	.L2
	movzbl	1(%rsi), %ecx
	movzbl	2(%rsi), %edi
	sall	$8, %ecx
	sall	$16, %edi
	orl	%edi, %ecx
	movzbl	(%rsi), %edi
	orl	%edi, %ecx
	movzbl	3(%rsi), %edi
	sall	$24, %edi
	orl	%edi, %ecx
	movl	%ecx, (%rax)
	movzbl	5(%rsi), %ecx
	movzbl	6(%rsi), %edi
	sall	$8, %ecx
	sall	$16, %edi
	orl	%edi, %ecx
	movzbl	4(%rsi), %edi
	orl	%edi, %ecx
	movzbl	7(%rsi), %edi
	sall	$24, %edi
	orl	%edi, %ecx
	movl	%ecx, 4(%rax)
	movzbl	9(%rsi), %ecx
	movzbl	10(%rsi), %edi
	sall	$8, %ecx
	sall	$16, %edi
	orl	%edi, %ecx
	movzbl	8(%rsi), %edi
	orl	%edi, %ecx
	movzbl	11(%rsi), %edi
	sall	$24, %edi
	orl	%edi, %ecx
	movl	%ecx, 8(%rax)
	movzbl	13(%rsi), %ecx
	movzbl	14(%rsi), %edi
	sall	$8, %ecx
	sall	$16, %edi
	orl	%edi, %ecx
	movzbl	12(%rsi), %edi
	orl	%edi, %ecx
	movzbl	15(%rsi), %edi
	sall	$24, %edi
	orl	%edi, %ecx
	movl	%ecx, 12(%rax)
	movzbl	17(%rsi), %ecx
	movzbl	18(%rsi), %edi
	sall	$8, %ecx
	sall	$16, %edi
	orl	%edi, %ecx
	movzbl	16(%rsi), %edi
	orl	%edi, %ecx
	movzbl	19(%rsi), %edi
	sall	$24, %edi
	orl	%edi, %ecx
	movl	%ecx, 16(%rax)
	movzbl	21(%rsi), %ecx
	movzbl	22(%rsi), %edi
	sall	$8, %ecx
	sall	$16, %edi
	orl	%edi, %ecx
	movzbl	20(%rsi), %edi
	orl	%edi, %ecx
	movzbl	23(%rsi), %edi
	sall	$24, %edi
	orl	%edi, %ecx
	movl	%ecx, 20(%rax)
	movzbl	25(%rsi), %ecx
	movzbl	26(%rsi), %edi
	sall	$8, %ecx
	sall	$16, %edi
	orl	%edi, %ecx
	movzbl	24(%rsi), %edi
	orl	%edi, %ecx
	movzbl	27(%rsi), %edi
	sall	$24, %edi
	orl	%edi, %ecx
	movl	%ecx, 24(%rax)
	movzbl	29(%rsi), %ecx
	movzbl	30(%rsi), %edi
	sall	$8, %ecx
	sall	$16, %edi
	orl	%edi, %ecx
	movzbl	28(%rsi), %edi
	movzbl	31(%rsi), %esi
	orl	%edi, %ecx
	sall	$24, %esi
	orl	%esi, %ecx
	movl	%ecx, 28(%rax)
.L2:
	testq	%rdx, %rdx
	je	.L3
	movzbl	2(%rdx), %ecx
	movzbl	1(%rdx), %esi
	sall	$16, %ecx
	sall	$8, %esi
	orl	%esi, %ecx
	movzbl	(%rdx), %esi
	orl	%esi, %ecx
	movzbl	3(%rdx), %esi
	sall	$24, %esi
	orl	%esi, %ecx
	movl	%ecx, 32(%rax)
	movzbl	6(%rdx), %ecx
	movzbl	5(%rdx), %esi
	sall	$16, %ecx
	sall	$8, %esi
	orl	%esi, %ecx
	movzbl	4(%rdx), %esi
	orl	%esi, %ecx
	movzbl	7(%rdx), %esi
	sall	$24, %esi
	orl	%esi, %ecx
	movl	%ecx, 36(%rax)
	movzbl	10(%rdx), %ecx
	movzbl	9(%rdx), %esi
	sall	$16, %ecx
	sall	$8, %esi
	orl	%esi, %ecx
	movzbl	8(%rdx), %esi
	orl	%esi, %ecx
	movzbl	11(%rdx), %esi
	sall	$24, %esi
	orl	%esi, %ecx
	movl	%ecx, 40(%rax)
	movzbl	13(%rdx), %ecx
	movzbl	14(%rdx), %esi
	sall	$8, %ecx
	sall	$16, %esi
	orl	%esi, %ecx
	movzbl	12(%rdx), %esi
	movzbl	15(%rdx), %edx
	orl	%esi, %ecx
	sall	$24, %edx
	orl	%ecx, %edx
	movl	%edx, 44(%rax)
.L3:
	movl	$0, 112(%rax)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE469:
	.size	chacha_init_key, .-chacha_init_key
	.p2align 4
	.type	chacha_cipher, @function
chacha_cipher:
.LFB470:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$40, %rsp
	movq	120(%rdi), %r14
	movl	112(%r14), %eax
	testl	%eax, %eax
	jne	.L12
	movl	32(%r14), %r11d
.L13:
	movl	%ebx, %eax
	movl	%ebx, -72(%rbp)
	andq	$-64, %rbx
	andl	$63, %eax
	movl	%eax, -68(%rbp)
	cmpq	$63, %rbx
	jbe	.L19
	leaq	32(%r14), %r15
	movq	%r12, %rsi
	movq	%r13, %r12
	movq	%rbx, %r13
	movq	%r15, %r8
	movl	%r11d, %r15d
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L62:
	movq	%r13, %rax
	shrq	$6, %rax
	movl	%eax, %edx
	addl	%r15d, %edx
	movq	%rdx, %r15
	cmpq	%rax, %rdx
	jnb	.L21
.L63:
	subq	%rdx, %rax
	movq	%r12, %rdi
	movq	%r14, %rcx
	movq	%r8, -64(%rbp)
	salq	$6, %rax
	movq	%rsi, -56(%rbp)
	movq	%rax, %rdx
	movq	%rax, %rbx
	call	ChaCha20_ctr32@PLT
	movq	-56(%rbp), %rsi
	movq	-64(%rbp), %r8
	subq	%rbx, %r13
	movl	$0, 32(%r14)
	addq	%rbx, %r12
	addq	%rbx, %rsi
.L22:
	addl	$1, 36(%r14)
	xorl	%r15d, %r15d
	cmpq	$63, %r13
	jbe	.L61
.L24:
	movabsq	$17179869247, %rax
	cmpq	%rax, %r13
	jbe	.L62
	movl	$268435456, %edx
	movl	$268435456, %eax
	addl	%r15d, %edx
	movq	%rdx, %r15
	cmpq	%rax, %rdx
	jb	.L63
.L21:
	salq	$6, %rax
	movq	%r12, %rdi
	movq	%r14, %rcx
	movq	%r8, -64(%rbp)
	movq	%rax, %rdx
	movq	%rax, %rbx
	movq	%rsi, -56(%rbp)
	call	ChaCha20_ctr32@PLT
	movq	-56(%rbp), %rsi
	subq	%rbx, %r13
	addq	%rbx, %r12
	movl	%r15d, 32(%r14)
	movq	-64(%rbp), %r8
	addq	%rbx, %rsi
	testl	%r15d, %r15d
	je	.L22
	cmpq	$63, %r13
	ja	.L24
.L61:
	movq	%r12, %r13
	movq	%rsi, %r12
.L19:
	movl	-68(%rbp), %eax
	testl	%eax, %eax
	jne	.L64
.L18:
	addq	$40, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	.cfi_restore_state
	testq	%rcx, %rcx
	jne	.L58
	.p2align 4,,10
	.p2align 3
.L14:
	movl	%eax, 112(%r14)
	testq	%rbx, %rbx
	je	.L18
.L66:
	movl	32(%r14), %r11d
	cmpl	$64, %eax
	jne	.L13
	addl	$1, %r11d
	movl	$0, 112(%r14)
	movl	%r11d, 32(%r14)
	jne	.L13
	addl	$1, 36(%r14)
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L65:
	movl	%eax, %ecx
	movzbl	(%r12), %edx
	addq	$1, %r13
	addq	$1, %r12
	xorb	48(%r14,%rcx), %dl
	addl	$1, %eax
	movb	%dl, -1(%r13)
	subq	$1, %rbx
	je	.L14
.L58:
	cmpl	$63, %eax
	jbe	.L65
	movl	%eax, 112(%r14)
	testq	%rbx, %rbx
	jne	.L66
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L64:
	pxor	%xmm0, %xmm0
	leaq	48(%r14), %rbx
	movl	$64, %edx
	movq	%r14, %rcx
	movq	%rbx, %rsi
	movq	%rbx, %rdi
	movups	%xmm0, 48(%r14)
	leaq	32(%r14), %r8
	movups	%xmm0, 64(%r14)
	movups	%xmm0, 80(%r14)
	movups	%xmm0, 96(%r14)
	call	ChaCha20_ctr32@PLT
	leaq	16(%r13), %rdx
	leaq	64(%r14), %rsi
	movl	-68(%rbp), %edi
	cmpq	%rdx, %rbx
	setnb	%dl
	cmpq	%rsi, %r13
	leal	-1(%rdi), %eax
	setnb	%sil
	orl	%esi, %edx
	cmpl	$14, %eax
	seta	%sil
	testb	%sil, %dl
	je	.L26
	leaq	15(%r13), %rdx
	subq	%r12, %rdx
	cmpq	$30, %rdx
	jbe	.L26
	movdqu	(%r12), %xmm0
	movdqu	48(%r14), %xmm1
	movl	%edi, %eax
	shrl	$4, %eax
	pxor	%xmm1, %xmm0
	movups	%xmm0, 0(%r13)
	cmpl	$1, %eax
	je	.L27
	movdqu	16(%r12), %xmm0
	movdqu	64(%r14), %xmm2
	pxor	%xmm2, %xmm0
	movups	%xmm0, 16(%r13)
	cmpl	$3, %eax
	jne	.L27
	movdqu	80(%r14), %xmm0
	movdqu	32(%r12), %xmm3
	pxor	%xmm3, %xmm0
	movups	%xmm0, 32(%r13)
.L27:
	movl	-72(%rbp), %ecx
	movl	-68(%rbp), %edi
	andl	$48, %ecx
	cmpl	%edi, %ecx
	je	.L30
	movl	%ecx, %eax
	movzbl	(%r12,%rax), %edx
	xorb	48(%r14,%rax), %dl
	movb	%dl, 0(%r13,%rax)
	leal	1(%rcx), %eax
	cmpl	%edi, %eax
	jnb	.L30
	movzbl	(%r12,%rax), %edx
	xorb	48(%r14,%rax), %dl
	movb	%dl, 0(%r13,%rax)
	leal	2(%rcx), %eax
	cmpl	%eax, %edi
	jbe	.L30
	movzbl	(%r12,%rax), %edx
	xorb	48(%r14,%rax), %dl
	movb	%dl, 0(%r13,%rax)
	leal	3(%rcx), %eax
	cmpl	%eax, %edi
	jbe	.L30
	movzbl	(%r12,%rax), %edx
	xorb	48(%r14,%rax), %dl
	movb	%dl, 0(%r13,%rax)
	leal	4(%rcx), %eax
	cmpl	%eax, %edi
	jbe	.L30
	movzbl	(%r12,%rax), %edx
	xorb	48(%r14,%rax), %dl
	movb	%dl, 0(%r13,%rax)
	leal	5(%rcx), %eax
	cmpl	%eax, %edi
	jbe	.L30
	movzbl	(%r12,%rax), %edx
	xorb	48(%r14,%rax), %dl
	movb	%dl, 0(%r13,%rax)
	leal	6(%rcx), %eax
	cmpl	%eax, %edi
	jbe	.L30
	movzbl	(%r12,%rax), %edx
	xorb	48(%r14,%rax), %dl
	movb	%dl, 0(%r13,%rax)
	leal	7(%rcx), %eax
	cmpl	%eax, %edi
	jbe	.L30
	movzbl	(%r12,%rax), %edx
	xorb	48(%r14,%rax), %dl
	movb	%dl, 0(%r13,%rax)
	leal	8(%rcx), %eax
	cmpl	%eax, %edi
	jbe	.L30
	movzbl	(%r12,%rax), %edx
	xorb	48(%r14,%rax), %dl
	movb	%dl, 0(%r13,%rax)
	leal	9(%rcx), %eax
	cmpl	%eax, %edi
	jbe	.L30
	movzbl	(%r12,%rax), %edx
	xorb	48(%r14,%rax), %dl
	movb	%dl, 0(%r13,%rax)
	leal	10(%rcx), %eax
	cmpl	%eax, %edi
	jbe	.L30
	movzbl	(%r12,%rax), %edx
	xorb	48(%r14,%rax), %dl
	movb	%dl, 0(%r13,%rax)
	leal	11(%rcx), %eax
	cmpl	%eax, %edi
	jbe	.L30
	movzbl	(%r12,%rax), %edx
	xorb	48(%r14,%rax), %dl
	movb	%dl, 0(%r13,%rax)
	leal	12(%rcx), %eax
	cmpl	%eax, %edi
	jbe	.L30
	movzbl	(%r12,%rax), %edx
	xorb	48(%r14,%rax), %dl
	movb	%dl, 0(%r13,%rax)
	leal	13(%rcx), %eax
	cmpl	%eax, %edi
	jbe	.L30
	movzbl	(%r12,%rax), %edx
	xorb	48(%r14,%rax), %dl
	movb	%dl, 0(%r13,%rax)
	leal	14(%rcx), %eax
	cmpl	%eax, %edi
	jbe	.L30
	movzbl	(%r12,%rax), %edx
	xorb	48(%r14,%rax), %dl
	movb	%dl, 0(%r13,%rax)
	.p2align 4,,10
	.p2align 3
.L30:
	movl	-68(%rbp), %eax
	movl	%eax, 112(%r14)
	jmp	.L18
.L26:
	movl	%eax, %esi
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L29:
	movzbl	48(%r14,%rax), %edx
	xorb	(%r12,%rax), %dl
	movb	%dl, 0(%r13,%rax)
	movq	%rax, %rdx
	addq	$1, %rax
	cmpq	%rdx, %rsi
	jne	.L29
	jmp	.L30
	.cfi_endproc
.LFE470:
	.size	chacha_cipher, .-chacha_cipher
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/evp/e_chacha20_poly1305.c"
	.text
	.p2align 4
	.type	chacha20_poly1305_ctrl, @function
chacha20_poly1305_ctrl:
.LFB476:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	120(%rdi), %r14
	cmpl	$37, %esi
	ja	.L68
	movl	%esi, %ebx
	movq	%rcx, %r13
	movl	%esi, %ecx
	movq	%rdi, %r12
	leaq	.L70(%rip), %rsi
	movslq	(%rsi,%rcx,4), %rax
	addq	%rsi, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L70:
	.long	.L78-.L70
	.long	.L68-.L70
	.long	.L68-.L70
	.long	.L68-.L70
	.long	.L68-.L70
	.long	.L68-.L70
	.long	.L68-.L70
	.long	.L68-.L70
	.long	.L77-.L70
	.long	.L76-.L70
	.long	.L68-.L70
	.long	.L68-.L70
	.long	.L68-.L70
	.long	.L68-.L70
	.long	.L68-.L70
	.long	.L68-.L70
	.long	.L75-.L70
	.long	.L74-.L70
	.long	.L73-.L70
	.long	.L68-.L70
	.long	.L68-.L70
	.long	.L68-.L70
	.long	.L72-.L70
	.long	.L97-.L70
	.long	.L68-.L70
	.long	.L68-.L70
	.long	.L68-.L70
	.long	.L68-.L70
	.long	.L68-.L70
	.long	.L68-.L70
	.long	.L68-.L70
	.long	.L68-.L70
	.long	.L68-.L70
	.long	.L68-.L70
	.long	.L68-.L70
	.long	.L68-.L70
	.long	.L68-.L70
	.long	.L69-.L70
	.text
	.p2align 4,,10
	.p2align 3
.L68:
	movl	$-1, %eax
.L67:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L75:
	.cfi_restore_state
	leal	-1(%rdx), %eax
	cmpl	$15, %eax
	ja	.L83
	movl	16(%rdi), %esi
	testl	%esi, %esi
	je	.L83
	leaq	132(%r14), %rax
	cmpl	$8, %edx
	jnb	.L90
	testb	$4, %dl
	jne	.L116
	testl	%edx, %edx
	je	.L97
	movzbl	(%rax), %ecx
	movb	%cl, 0(%r13)
	testb	$2, %dl
	je	.L97
	movl	%edx, %edx
	movzwl	-2(%rax,%rdx), %eax
	movw	%ax, -2(%r13,%rdx)
.L97:
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L69:
	.cfi_restore_state
	movl	196(%r14), %eax
	movl	%eax, 0(%r13)
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L78:
	.cfi_restore_state
	testq	%r14, %r14
	je	.L117
.L79:
	pxor	%xmm0, %xmm0
	movl	$1, %eax
	movq	$-1, 200(%r14)
	movups	%xmm0, 168(%r14)
	movdqa	.LC1(%rip), %xmm0
	movups	%xmm0, 184(%r14)
	pxor	%xmm0, %xmm0
	movups	%xmm0, 148(%r14)
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L77:
	testq	%r14, %r14
	je	.L97
	call	Poly1305_ctx_size@PLT
	movl	$529, %ecx
	leaq	.LC0(%rip), %rdx
	movq	%r14, %rdi
	leaq	208(%rax), %rsi
	call	CRYPTO_memdup@PLT
	movq	%rax, 120(%r13)
	testq	%rax, %rax
	jne	.L97
	movl	$531, %r8d
	movl	$173, %edx
	movl	$182, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L76:
	leal	-1(%rdx), %eax
	cmpl	$11, %eax
	ja	.L83
	movl	%edx, 196(%r14)
	movl	$1, %eax
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L72:
	cmpl	$13, %edx
	je	.L118
.L83:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L73:
	.cfi_restore_state
	cmpl	$12, %edx
	jne	.L83
	movl	0(%r13), %eax
	movl	%eax, 36(%r14)
	movl	%eax, 120(%r14)
	movl	4(%r13), %eax
	movl	%eax, 40(%r14)
	movl	%eax, 124(%r14)
	movl	8(%r13), %eax
	movl	%eax, 44(%r14)
	movl	%eax, 128(%r14)
	movl	$1, %eax
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L74:
	leal	-1(%rdx), %eax
	cmpl	$15, %eax
	ja	.L83
	testq	%r13, %r13
	je	.L97
	leaq	132(%r14), %rax
	cmpl	$8, %edx
	jnb	.L84
	testb	$4, %dl
	jne	.L119
	testl	%edx, %edx
	je	.L85
	movzbl	0(%r13), %ecx
	movb	%cl, (%rax)
	testb	$2, %dl
	jne	.L120
.L85:
	movl	%edx, 192(%r14)
	movl	$1, %eax
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L118:
	movq	0(%r13), %rdx
	movq	%rdx, 148(%r14)
	movl	8(%r13), %edx
	movl	%edx, 156(%r14)
	movzbl	12(%r13), %edx
	movb	%dl, 160(%r14)
	movzwl	11(%r13), %eax
	movl	16(%rdi), %ecx
	rolw	$8, %ax
	movzwl	%ax, %edx
	testl	%ecx, %ecx
	jne	.L96
	cmpw	$15, %ax
	jbe	.L83
	subl	$16, %edx
	movl	%edx, %eax
	rolw	$8, %ax
	movw	%ax, 159(%r14)
.L96:
	movl	$0, 188(%r14)
	movl	%edx, %eax
	movq	%rax, 200(%r14)
	movl	120(%r14), %eax
	movl	%eax, 36(%r14)
	movq	124(%r14), %rax
	xorq	148(%r14), %rax
	movq	%rax, 40(%r14)
	movl	$16, %eax
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L117:
	call	Poly1305_ctx_size@PLT
	movl	$509, %edx
	leaq	.LC0(%rip), %rsi
	leaq	208(%rax), %rdi
	call	CRYPTO_zalloc@PLT
	movq	%rax, 120(%r12)
	movq	%rax, %r14
	testq	%rax, %rax
	jne	.L79
	movl	$511, %r8d
	movl	$134, %edx
	movl	$182, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	%ebx, %eax
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L84:
	movq	0(%r13), %rcx
	leaq	140(%r14), %r8
	andq	$-8, %r8
	movq	%rcx, 132(%r14)
	movl	%edx, %ecx
	movq	-8(%r13,%rcx), %rsi
	movq	%rsi, -8(%rax,%rcx)
	subq	%r8, %rax
	movq	%r13, %rcx
	subq	%rax, %rcx
	addl	%edx, %eax
	andl	$-8, %eax
	cmpl	$8, %eax
	jb	.L85
	andl	$-8, %eax
	xorl	%esi, %esi
.L88:
	movl	%esi, %edi
	addl	$8, %esi
	movq	(%rcx,%rdi), %r9
	movq	%r9, (%r8,%rdi)
	cmpl	%eax, %esi
	jb	.L88
	jmp	.L85
	.p2align 4,,10
	.p2align 3
.L90:
	movq	132(%r14), %rcx
	leaq	8(%r13), %rdi
	andq	$-8, %rdi
	movq	%rcx, 0(%r13)
	movl	%edx, %ecx
	movq	-8(%rax,%rcx), %rsi
	movq	%rsi, -8(%r13,%rcx)
	movq	%r13, %rcx
	subq	%rdi, %rcx
	addl	%ecx, %edx
	subq	%rcx, %rax
	andl	$-8, %edx
	movq	%rax, %rsi
	cmpl	$8, %edx
	jb	.L97
	andl	$-8, %edx
	xorl	%eax, %eax
.L94:
	movl	%eax, %ecx
	addl	$8, %eax
	movq	(%rsi,%rcx), %r8
	movq	%r8, (%rdi,%rcx)
	cmpl	%edx, %eax
	jb	.L94
	jmp	.L97
.L119:
	movl	0(%r13), %ecx
	movl	%ecx, (%rax)
	movl	%edx, %ecx
	movl	-4(%r13,%rcx), %esi
	movl	%esi, -4(%rax,%rcx)
	jmp	.L85
.L116:
	movl	(%rax), %ecx
	movl	%edx, %edx
	movl	%ecx, 0(%r13)
	movl	-4(%rax,%rdx), %eax
	movl	%eax, -4(%r13,%rdx)
	jmp	.L97
.L120:
	movl	%edx, %ecx
	movzwl	-2(%r13,%rcx), %esi
	movw	%si, -2(%rax,%rcx)
	jmp	.L85
	.cfi_endproc
.LFE476:
	.size	chacha20_poly1305_ctrl, .-chacha20_poly1305_ctrl
	.p2align 4
	.type	chacha20_poly1305_cleanup, @function
chacha20_poly1305_cleanup:
.LFB475:
	.cfi_startproc
	endbr64
	cmpq	$0, 120(%rdi)
	je	.L124
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	Poly1305_ctx_size@PLT
	movq	120(%rbx), %rdi
	leaq	208(%rax), %rsi
	call	OPENSSL_cleanse@PLT
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L124:
	.cfi_restore 3
	.cfi_restore 6
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE475:
	.size	chacha20_poly1305_cleanup, .-chacha20_poly1305_cleanup
	.p2align 4
	.type	chacha20_poly1305_tls_cipher.isra.0, @function
chacha20_poly1305_tls_cipher.isra.0:
.LFB479:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	subq	$360, %rsp
	.cfi_offset 3, -56
	movq	%rdi, -384(%rbp)
	movq	200(%rsi), %rbx
	movq	%rdx, -360(%rbp)
	movq	%r8, -368(%rbp)
	leaq	16(%rbx), %rax
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	cmpq	%r8, %rax
	jne	.L139
	movl	$0, 32(%rsi)
	movq	%rsi, %r13
	leaq	32(%rsi), %r8
	leaq	208(%rsi), %r14
	cmpq	$192, %rbx
	jbe	.L146
	leaq	-352(%rbp), %r15
	movq	%rsi, %rcx
	movl	$64, %edx
	movq	%r8, -392(%rbp)
	leaq	zero(%rip), %rsi
	movq	%r15, %rdi
	call	ChaCha20_ctr32@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	Poly1305_Init@PLT
	movl	$1, 32(%r13)
	movl	$16, %edx
	movq	%r14, %rdi
	movl	$0, 112(%r13)
	leaq	148(%r13), %rsi
	call	Poly1305_Update@PLT
	movq	-384(%rbp), %rax
	movq	$13, 168(%r13)
	movq	%rbx, 176(%r13)
	movq	-392(%rbp), %r8
	movl	(%rax), %ecx
	testl	%ecx, %ecx
	je	.L133
	movq	-360(%rbp), %rdi
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%r13, %rcx
	call	ChaCha20_ctr32@PLT
	movq	-360(%rbp), %rsi
	movq	%rbx, %rdx
	movq	%r14, %rdi
	call	Poly1305_Update@PLT
.L134:
	addq	%rbx, -360(%rbp)
	addq	%rbx, %r12
	negq	%rbx
	movq	%r14, %rdi
	movq	%rbx, %rdx
	leaq	zero(%rip), %rsi
	andl	$15, %edx
	call	Poly1305_Update@PLT
	leaq	-288(%rbp), %rax
	movl	$16, %edx
	movq	$64, -392(%rbp)
	movq	%rax, %rbx
.L130:
	movdqu	168(%r13), %xmm0
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movups	%xmm0, (%rax)
	call	Poly1305_Update@PLT
	movq	-392(%rbp), %rsi
	movq	%r15, %rdi
	call	OPENSSL_cleanse@PLT
	movq	-384(%rbp), %r15
	leaq	132(%r13), %rsi
	movq	%r14, %rdi
	movl	(%r15), %eax
	testl	%eax, %eax
	cmove	%rbx, %rsi
	call	Poly1305_Final@PLT
	movl	(%r15), %edx
	movq	$-1, 200(%r13)
	testl	%edx, %edx
	je	.L136
	movdqu	132(%r13), %xmm2
	movq	-360(%rbp), %rax
	movaps	%xmm2, -384(%rbp)
	movups	%xmm2, (%rax)
.L137:
	movl	-368(%rbp), %eax
.L127:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L147
	addq	$360, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L146:
	.cfi_restore_state
	leaq	127(%rbx), %rax
	leaq	-352(%rbp), %r15
	movq	%rsi, %rcx
	andq	$-64, %rax
	leaq	zero(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, %rdx
	movq	%rax, -392(%rbp)
	call	ChaCha20_ctr32@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	Poly1305_Init@PLT
	movl	$0, 112(%r13)
	movdqu	148(%r13), %xmm1
	movq	$13, 168(%r13)
	movq	%rbx, 176(%r13)
	movaps	%xmm1, -304(%rbp)
	testq	%rbx, %rbx
	je	.L140
	movq	-384(%rbp), %rax
	movq	-360(%rbp), %rdi
	movq	%rbx, %rcx
	movq	%r12, %rsi
	leaq	-288(%rbp), %rdx
	movl	(%rax), %r8d
	testl	%r8d, %r8d
	jne	.L148
	call	xor128_decrypt_n_pad@PLT
.L132:
	leaq	-304(%rbp), %r8
	movq	%rax, %rdx
	addq	%rbx, -360(%rbp)
	addq	%rbx, %r12
	subq	%r8, %rdx
	movq	%r8, %rbx
	addq	$16, %rdx
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L136:
	movl	$16, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	CRYPTO_memcmp@PLT
	testl	%eax, %eax
	je	.L137
	movq	-368(%rbp), %rax
	movq	-360(%rbp), %rbx
	xorl	%esi, %esi
	subq	%rax, %rbx
	leaq	-16(%rax), %rdx
	leaq	16(%rbx), %rdi
	call	memset@PLT
	movl	$-1, %eax
	jmp	.L127
	.p2align 4,,10
	.p2align 3
.L133:
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%r8, -392(%rbp)
	call	Poly1305_Update@PLT
	movq	%r13, %rcx
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	-392(%rbp), %r8
	movq	-360(%rbp), %rdi
	call	ChaCha20_ctr32@PLT
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L140:
	movl	$32, %edx
	leaq	-288(%rbp), %rax
	leaq	-304(%rbp), %rbx
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L148:
	call	xor128_encrypt_n_pad@PLT
	jmp	.L132
	.p2align 4,,10
	.p2align 3
.L139:
	movl	$-1, %eax
	jmp	.L127
.L147:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE479:
	.size	chacha20_poly1305_tls_cipher.isra.0, .-chacha20_poly1305_tls_cipher.isra.0
	.p2align 4
	.type	chacha20_poly1305_cipher, @function
chacha20_poly1305_cipher:
.LFB474:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	120(%rdi), %rbx
	movq	%rsi, -96(%rbp)
	movq	%rcx, -88(%rbp)
	movl	188(%rbx), %r9d
	movq	200(%rbx), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r9d, %r9d
	jne	.L151
	cmpq	$-1, %r13
	je	.L152
	testq	%rsi, %rsi
	je	.L152
	movq	%rcx, %r8
	leaq	16(%rdi), %rdi
	movq	%rdx, %rcx
	movq	%rsi, %rdx
	movq	%rbx, %rsi
	call	chacha20_poly1305_tls_cipher.isra.0
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L152:
	leaq	48(%rbx), %r15
	leaq	32(%rbx), %r8
	movq	%rbx, %rcx
	movl	$64, %edx
	movl	$0, 32(%rbx)
	leaq	zero(%rip), %rsi
	movq	%r15, %rdi
	call	ChaCha20_ctr32@PLT
	leaq	208(%rbx), %rdi
	movq	%r15, %rsi
	movq	%rdi, -112(%rbp)
	call	Poly1305_Init@PLT
	pxor	%xmm0, %xmm0
	movl	$1, 32(%rbx)
	movl	$0, 112(%rbx)
	movl	$1, 188(%rbx)
	movups	%xmm0, 168(%rbx)
	cmpq	$-1, %r13
	je	.L151
	movq	-112(%rbp), %rdi
	leaq	148(%rbx), %rsi
	movl	$13, %edx
	call	Poly1305_Update@PLT
	movq	$13, 168(%rbx)
	movl	$1, 184(%rbx)
	.p2align 4,,10
	.p2align 3
.L151:
	testq	%r14, %r14
	je	.L155
	cmpq	$0, -96(%rbp)
	je	.L198
	movl	184(%rbx), %r8d
	testl	%r8d, %r8d
	je	.L157
	movq	168(%rbx), %rax
	andl	$15, %eax
	jne	.L199
.L158:
	movl	$0, 184(%rbx)
.L157:
	movq	$-1, 200(%rbx)
	cmpq	$-1, %r13
	je	.L175
	leaq	16(%r13), %rax
	cmpq	-88(%rbp), %rax
	jne	.L176
.L159:
	movl	16(%r12), %edi
	leaq	208(%rbx), %r15
	testl	%edi, %edi
	je	.L160
	movq	-96(%rbp), %rsi
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	%r12, %rdi
	call	chacha_cipher
	movq	-96(%rbp), %rsi
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	Poly1305_Update@PLT
	addq	%r13, 176(%rbx)
	cmpq	-88(%rbp), %r13
	jne	.L200
.L162:
	movl	-88(%rbp), %eax
.L149:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L201
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L160:
	.cfi_restore_state
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	Poly1305_Update@PLT
	movq	-96(%rbp), %rsi
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	%r12, %rdi
	call	chacha_cipher
	addq	%r13, 176(%rbx)
	cmpq	-88(%rbp), %r13
	je	.L162
.L200:
	movl	184(%rbx), %esi
	addq	%r13, -96(%rbp)
	addq	%r13, %r14
	movb	$1, -112(%rbp)
	testl	%esi, %esi
	je	.L163
.L204:
	movq	168(%rbx), %rax
	andl	$15, %eax
	jne	.L202
.L164:
	movl	$0, 184(%rbx)
.L163:
	movq	176(%rbx), %rax
	andl	$15, %eax
	jne	.L203
.L165:
	leaq	168(%rbx), %rsi
	movl	$16, %edx
	movq	%r15, %rdi
	call	Poly1305_Update@PLT
	movl	16(%r12), %ecx
	leaq	-80(%rbp), %rsi
	testl	%ecx, %ecx
	je	.L166
	leaq	132(%rbx), %rsi
.L166:
	movq	%r15, %rdi
	call	Poly1305_Final@PLT
	cmpb	$0, -112(%rbp)
	movl	$0, 188(%rbx)
	je	.L167
	movl	16(%r12), %edx
	testl	%edx, %edx
	je	.L168
	movdqu	132(%rbx), %xmm1
	movq	-96(%rbp), %rax
	movaps	%xmm1, -112(%rbp)
	movups	%xmm1, (%rax)
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L155:
	movl	184(%rbx), %esi
	movb	$0, -112(%rbp)
	leaq	208(%rbx), %r15
	testl	%esi, %esi
	jne	.L204
	movq	176(%rbx), %rax
	andl	$15, %eax
	je	.L165
.L203:
	movl	$16, %edx
	leaq	zero(%rip), %rsi
	movq	%r15, %rdi
	subq	%rax, %rdx
	call	Poly1305_Update@PLT
	jmp	.L165
	.p2align 4,,10
	.p2align 3
.L167:
	movl	16(%r12), %eax
	testl	%eax, %eax
	jne	.L162
	movslq	192(%rbx), %rdx
	leaq	132(%rbx), %rsi
	leaq	-80(%rbp), %rdi
	call	CRYPTO_memcmp@PLT
	testl	%eax, %eax
	je	.L162
.L176:
	movl	$-1, %eax
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L175:
	movq	-88(%rbp), %r13
	jmp	.L159
	.p2align 4,,10
	.p2align 3
.L199:
	movl	$16, %edx
	leaq	208(%rbx), %rdi
	leaq	zero(%rip), %rsi
	subq	%rax, %rdx
	call	Poly1305_Update@PLT
	jmp	.L158
	.p2align 4,,10
	.p2align 3
.L168:
	leaq	-80(%rbp), %rdi
	movl	$16, %edx
	movq	%r14, %rsi
	call	CRYPTO_memcmp@PLT
	testl	%eax, %eax
	je	.L162
	movq	-96(%rbp), %rdi
	movq	%r13, %rdx
	xorl	%esi, %esi
	subq	%r13, %rdi
	call	memset@PLT
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L202:
	movl	$16, %edx
	leaq	zero(%rip), %rsi
	movq	%r15, %rdi
	subq	%rax, %rdx
	call	Poly1305_Update@PLT
	jmp	.L164
	.p2align 4,,10
	.p2align 3
.L198:
	movq	-88(%rbp), %r15
	leaq	208(%rbx), %rdi
	movq	%r14, %rsi
	movq	%r15, %rdx
	call	Poly1305_Update@PLT
	addq	%r15, 168(%rbx)
	movq	%r15, %rax
	movl	$1, 184(%rbx)
	jmp	.L149
.L201:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE474:
	.size	chacha20_poly1305_cipher, .-chacha20_poly1305_cipher
	.p2align 4
	.type	chacha20_poly1305_init_key, @function
chacha20_poly1305_init_key:
.LFB472:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	%rsi, %rax
	orq	%rdx, %rax
	je	.L206
	movq	120(%rdi), %r8
	pxor	%xmm0, %xmm0
	movq	$0, 184(%r8)
	movq	$-1, 200(%r8)
	movups	%xmm0, 168(%r8)
	testq	%rdx, %rdx
	je	.L207
	movslq	196(%r8), %rax
	pxor	%xmm0, %xmm0
	leaq	-48(%rbp), %r9
	movaps	%xmm0, -48(%rbp)
	cmpl	$16, %eax
	jle	.L232
.L208:
	movq	%r9, %rdx
	call	chacha_init_key
	movl	44(%r8), %eax
	movq	36(%r8), %rdx
	movl	%eax, 128(%r8)
	movq	%rdx, 120(%r8)
.L206:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L233
	addq	$40, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L232:
	.cfi_restore_state
	movq	%r9, %r10
	subq	%rax, %r10
	addq	$16, %r10
	cmpq	$8, %rax
	jnb	.L209
	testb	$4, %al
	jne	.L234
	testq	%rax, %rax
	je	.L208
	movzbl	(%rdx), %r11d
	movb	%r11b, (%r10)
	testb	$2, %al
	je	.L208
	movzwl	-2(%rdx,%rax), %edx
	movw	%dx, -2(%r10,%rax)
	jmp	.L208
	.p2align 4,,10
	.p2align 3
.L207:
	testq	%rsi, %rsi
	je	.L215
	movzbl	2(%rsi), %eax
	movzbl	1(%rsi), %edx
	sall	$16, %eax
	sall	$8, %edx
	orl	%edx, %eax
	movzbl	(%rsi), %edx
	orl	%edx, %eax
	movzbl	3(%rsi), %edx
	sall	$24, %edx
	orl	%edx, %eax
	movl	%eax, (%r8)
	movzbl	5(%rsi), %eax
	movzbl	6(%rsi), %edx
	sall	$8, %eax
	sall	$16, %edx
	orl	%edx, %eax
	movzbl	4(%rsi), %edx
	orl	%edx, %eax
	movzbl	7(%rsi), %edx
	sall	$24, %edx
	orl	%edx, %eax
	movl	%eax, 4(%r8)
	movzbl	9(%rsi), %eax
	movzbl	10(%rsi), %edx
	sall	$8, %eax
	sall	$16, %edx
	orl	%edx, %eax
	movzbl	8(%rsi), %edx
	orl	%edx, %eax
	movzbl	11(%rsi), %edx
	sall	$24, %edx
	orl	%edx, %eax
	movl	%eax, 8(%r8)
	movzbl	13(%rsi), %eax
	movzbl	14(%rsi), %edx
	sall	$8, %eax
	sall	$16, %edx
	orl	%edx, %eax
	movzbl	12(%rsi), %edx
	orl	%edx, %eax
	movzbl	15(%rsi), %edx
	sall	$24, %edx
	orl	%edx, %eax
	movl	%eax, 12(%r8)
	movzbl	17(%rsi), %eax
	movzbl	18(%rsi), %edx
	sall	$8, %eax
	sall	$16, %edx
	orl	%edx, %eax
	movzbl	16(%rsi), %edx
	orl	%edx, %eax
	movzbl	19(%rsi), %edx
	sall	$24, %edx
	orl	%edx, %eax
	movl	%eax, 16(%r8)
	movzbl	21(%rsi), %eax
	movzbl	22(%rsi), %edx
	sall	$8, %eax
	sall	$16, %edx
	orl	%edx, %eax
	movzbl	20(%rsi), %edx
	orl	%edx, %eax
	movzbl	23(%rsi), %edx
	sall	$24, %edx
	orl	%edx, %eax
	movl	%eax, 20(%r8)
	movzbl	25(%rsi), %eax
	movzbl	26(%rsi), %edx
	sall	$8, %eax
	sall	$16, %edx
	orl	%edx, %eax
	movzbl	24(%rsi), %edx
	orl	%edx, %eax
	movzbl	27(%rsi), %edx
	sall	$24, %edx
	orl	%edx, %eax
	movl	%eax, 24(%r8)
	movzbl	29(%rsi), %eax
	movzbl	30(%rsi), %edx
	sall	$8, %eax
	sall	$16, %edx
	orl	%edx, %eax
	movzbl	28(%rsi), %edx
	orl	%edx, %eax
	movzbl	31(%rsi), %edx
	sall	$24, %edx
	orl	%edx, %eax
	movl	%eax, 28(%r8)
.L215:
	movl	$0, 112(%r8)
	jmp	.L206
	.p2align 4,,10
	.p2align 3
.L209:
	movq	(%rdx), %r11
	movq	%r11, (%r10)
	movq	-8(%rdx,%rax), %r11
	movq	%r11, -8(%r10,%rax)
	leaq	8(%r10), %r11
	andq	$-8, %r11
	subq	%r11, %r10
	addq	%r10, %rax
	subq	%r10, %rdx
	andq	$-8, %rax
	cmpq	$8, %rax
	jb	.L208
	andq	$-8, %rax
	xorl	%r10d, %r10d
.L213:
	movq	(%rdx,%r10), %rbx
	movq	%rbx, (%r11,%r10)
	addq	$8, %r10
	cmpq	%rax, %r10
	jb	.L213
	jmp	.L208
.L234:
	movl	(%rdx), %r11d
	movl	%r11d, (%r10)
	movl	-4(%rdx,%rax), %edx
	movl	%edx, -4(%r10,%rax)
	jmp	.L208
.L233:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE472:
	.size	chacha20_poly1305_init_key, .-chacha20_poly1305_init_key
	.p2align 4
	.globl	EVP_chacha20
	.type	EVP_chacha20, @function
EVP_chacha20:
.LFB471:
	.cfi_startproc
	endbr64
	leaq	chacha20(%rip), %rax
	ret
	.cfi_endproc
.LFE471:
	.size	EVP_chacha20, .-EVP_chacha20
	.p2align 4
	.globl	EVP_chacha20_poly1305
	.type	EVP_chacha20_poly1305, @function
EVP_chacha20_poly1305:
.LFB477:
	.cfi_startproc
	endbr64
	leaq	chacha20_poly1305(%rip), %rax
	ret
	.cfi_endproc
.LFE477:
	.size	EVP_chacha20_poly1305, .-EVP_chacha20_poly1305
	.section	.data.rel.local,"aw"
	.align 32
	.type	chacha20_poly1305, @object
	.size	chacha20_poly1305, 88
chacha20_poly1305:
	.long	1018
	.long	1
	.long	32
	.long	12
	.quad	3148912
	.quad	chacha20_poly1305_init_key
	.quad	chacha20_poly1305_cipher
	.quad	chacha20_poly1305_cleanup
	.long	0
	.zero	4
	.quad	0
	.quad	0
	.quad	chacha20_poly1305_ctrl
	.quad	0
	.section	.rodata
	.align 32
	.type	zero, @object
	.size	zero, 256
zero:
	.zero	256
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	chacha20, @object
	.size	chacha20, 88
chacha20:
	.long	1019
	.long	1
	.long	32
	.long	16
	.quad	48
	.quad	chacha_init_key
	.quad	chacha_cipher
	.quad	0
	.long	120
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC1:
	.long	0
	.long	0
	.long	0
	.long	12
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
