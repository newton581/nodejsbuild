	.file	"p12_p8d.c"
	.text
	.p2align 4
	.globl	PKCS8_decrypt
	.type	PKCS8_decrypt, @function
PKCS8_decrypt:
.LFB803:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%edx, %r13d
	pushq	%r12
	leaq	-32(%rbp), %rdx
	.cfi_offset 12, -32
	movq	%rsi, %r12
	leaq	-40(%rbp), %rsi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	X509_SIG_get0@PLT
	movq	-32(%rbp), %r8
	movq	-40(%rbp), %rdi
	movl	%r13d, %ecx
	movl	$1, %r9d
	movq	%r12, %rdx
	leaq	PKCS8_PRIV_KEY_INFO_it(%rip), %rsi
	call	PKCS12_item_decrypt_d2i@PLT
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L5
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L5:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE803:
	.size	PKCS8_decrypt, .-PKCS8_decrypt
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
