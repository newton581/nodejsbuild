	.file	"x_pubkey.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/x509/x_pubkey.c"
	.text
	.p2align 4
	.type	x509_pubkey_decode, @function
x509_pubkey_decode:
.LFB859:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	EVP_PKEY_new@PLT
	testq	%rax, %rax
	je	.L10
	movq	%rax, %r12
	movq	0(%r13), %rax
	movq	(%rax), %rdi
	call	OBJ_obj2nid@PLT
	movq	%r12, %rdi
	movl	%eax, %esi
	call	EVP_PKEY_set_type@PLT
	testl	%eax, %eax
	je	.L11
	movq	16(%r12), %rax
	movq	32(%rax), %rax
	testq	%rax, %rax
	je	.L6
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	*%rax
	testl	%eax, %eax
	je	.L12
	movq	%r12, (%rbx)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	.cfi_restore_state
	movl	$125, %r8d
	movl	$125, %edx
	movl	$148, %esi
	movl	$11, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L5:
	movq	%r12, %rdi
	call	EVP_PKEY_free@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	.cfi_restore_state
	movl	$109, %r8d
	movl	$65, %edx
	movl	$148, %esi
	movl	$11, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$8, %rsp
	movl	$-1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	.cfi_restore_state
	movl	$114, %r8d
	movl	$111, %edx
	movl	$148, %esi
	movl	$11, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L6:
	movl	$129, %r8d
	movl	$124, %edx
	movl	$148, %esi
	movl	$11, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L5
	.cfi_endproc
.LFE859:
	.size	x509_pubkey_decode, .-x509_pubkey_decode
	.p2align 4
	.type	pubkey_cb, @function
pubkey_cb:
.LFB853:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	cmpl	$3, %edi
	je	.L20
	movl	$1, %eax
	cmpl	$5, %edi
	je	.L21
.L13:
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	.cfi_restore_state
	movq	(%rsi), %rax
	movq	16(%rax), %rdi
	call	EVP_PKEY_free@PLT
	addq	$8, %rsp
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	.cfi_restore_state
	movq	(%rsi), %r12
	movq	16(%r12), %rdi
	call	EVP_PKEY_free@PLT
	movq	$0, 16(%r12)
	call	ERR_set_mark@PLT
	leaq	16(%r12), %rdi
	movq	%r12, %rsi
	call	x509_pubkey_decode
	movl	%eax, %r8d
	xorl	%eax, %eax
	cmpl	$-1, %r8d
	je	.L13
	call	ERR_pop_to_mark@PLT
	addq	$8, %rsp
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE853:
	.size	pubkey_cb, .-pubkey_cb
	.p2align 4
	.type	X509_PUBKEY_set.part.0, @function
X509_PUBKEY_set.part.0:
.LFB874:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	leaq	X509_PUBKEY_it(%rip), %rdi
	subq	$8, %rsp
	call	ASN1_item_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L23
	movq	16(%r13), %rax
	testq	%rax, %rax
	je	.L24
	movq	40(%rax), %rax
	testq	%rax, %rax
	je	.L25
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	*%rax
	testl	%eax, %eax
	je	.L32
	movq	(%rbx), %rdi
	leaq	X509_PUBKEY_it(%rip), %rsi
	call	ASN1_item_free@PLT
	movq	%r12, (%rbx)
	movq	%r13, %rdi
	movq	%r13, 16(%r12)
	call	EVP_PKEY_up_ref@PLT
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	.cfi_restore_state
	movl	$78, %r8d
	movl	$124, %edx
	movl	$120, %esi
	movl	$11, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L23:
	movq	%r12, %rdi
	leaq	X509_PUBKEY_it(%rip), %rsi
	call	ASN1_item_free@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	.cfi_restore_state
	movl	$73, %r8d
	movl	$126, %edx
	movl	$120, %esi
	movl	$11, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L24:
	movl	$82, %r8d
	movl	$111, %edx
	movl	$120, %esi
	movl	$11, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L23
	.cfi_endproc
.LFE874:
	.size	X509_PUBKEY_set.part.0, .-X509_PUBKEY_set.part.0
	.p2align 4
	.globl	d2i_X509_PUBKEY
	.type	d2i_X509_PUBKEY, @function
d2i_X509_PUBKEY:
.LFB854:
	.cfi_startproc
	endbr64
	leaq	X509_PUBKEY_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE854:
	.size	d2i_X509_PUBKEY, .-d2i_X509_PUBKEY
	.p2align 4
	.globl	i2d_X509_PUBKEY
	.type	i2d_X509_PUBKEY, @function
i2d_X509_PUBKEY:
.LFB855:
	.cfi_startproc
	endbr64
	leaq	X509_PUBKEY_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE855:
	.size	i2d_X509_PUBKEY, .-i2d_X509_PUBKEY
	.p2align 4
	.globl	X509_PUBKEY_new
	.type	X509_PUBKEY_new, @function
X509_PUBKEY_new:
.LFB856:
	.cfi_startproc
	endbr64
	leaq	X509_PUBKEY_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE856:
	.size	X509_PUBKEY_new, .-X509_PUBKEY_new
	.p2align 4
	.globl	X509_PUBKEY_free
	.type	X509_PUBKEY_free, @function
X509_PUBKEY_free:
.LFB857:
	.cfi_startproc
	endbr64
	leaq	X509_PUBKEY_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE857:
	.size	X509_PUBKEY_free, .-X509_PUBKEY_free
	.p2align 4
	.globl	X509_PUBKEY_set
	.type	X509_PUBKEY_set, @function
X509_PUBKEY_set:
.LFB858:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L49
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	leaq	X509_PUBKEY_it(%rip), %rdi
	subq	$8, %rsp
	call	ASN1_item_new@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L39
	movq	16(%r12), %rax
	testq	%rax, %rax
	je	.L40
	movq	40(%rax), %rax
	testq	%rax, %rax
	je	.L41
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	*%rax
	testl	%eax, %eax
	je	.L52
	movq	(%rbx), %rdi
	leaq	X509_PUBKEY_it(%rip), %rsi
	call	ASN1_item_free@PLT
	movq	%r13, (%rbx)
	movq	%r12, %rdi
	movq	%r12, 16(%r13)
	call	EVP_PKEY_up_ref@PLT
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L41:
	.cfi_restore_state
	movl	$78, %r8d
	movl	$124, %edx
	movl	$120, %esi
	movl	$11, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L39:
	movq	%r13, %rdi
	leaq	X509_PUBKEY_it(%rip), %rsi
	call	ASN1_item_free@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L49:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.p2align 4,,10
	.p2align 3
.L52:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movl	$73, %r8d
	movl	$126, %edx
	movl	$120, %esi
	movl	$11, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L40:
	movl	$82, %r8d
	movl	$111, %edx
	movl	$120, %esi
	movl	$11, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L39
	.cfi_endproc
.LFE858:
	.size	X509_PUBKEY_set, .-X509_PUBKEY_set
	.p2align 4
	.globl	X509_PUBKEY_get0
	.type	X509_PUBKEY_get0, @function
X509_PUBKEY_get0:
.LFB860:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$24, %rsp
	.cfi_offset 12, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	$0, -32(%rbp)
	testq	%rdi, %rdi
	je	.L56
	cmpq	$0, 8(%rdi)
	movq	%rdi, %rsi
	je	.L56
	movq	16(%rdi), %r12
	testq	%r12, %r12
	je	.L59
.L53:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L60
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L59:
	.cfi_restore_state
	leaq	-32(%rbp), %rdi
	call	x509_pubkey_decode
	cmpq	$0, -32(%rbp)
	jne	.L61
.L56:
	xorl	%r12d, %r12d
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L61:
	movl	$162, %r8d
	movl	$68, %edx
	movl	$119, %esi
	movl	$11, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	-32(%rbp), %rdi
	call	EVP_PKEY_free@PLT
	jmp	.L53
.L60:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE860:
	.size	X509_PUBKEY_get0, .-X509_PUBKEY_get0
	.p2align 4
	.globl	X509_PUBKEY_get
	.type	X509_PUBKEY_get, @function
X509_PUBKEY_get:
.LFB861:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$24, %rsp
	.cfi_offset 12, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	$0, -32(%rbp)
	testq	%rdi, %rdi
	je	.L65
	cmpq	$0, 8(%rdi)
	movq	%rdi, %rsi
	je	.L65
	movq	16(%rdi), %r12
	testq	%r12, %r12
	je	.L69
	movq	%r12, %rdi
	call	EVP_PKEY_up_ref@PLT
.L62:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L70
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L69:
	.cfi_restore_state
	leaq	-32(%rbp), %rdi
	call	x509_pubkey_decode
	cmpq	$0, -32(%rbp)
	jne	.L71
.L65:
	xorl	%r12d, %r12d
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L71:
	movl	$162, %r8d
	movl	$68, %edx
	movl	$119, %esi
	movl	$11, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	-32(%rbp), %rdi
	call	EVP_PKEY_free@PLT
	jmp	.L62
.L70:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE861:
	.size	X509_PUBKEY_get, .-X509_PUBKEY_get
	.p2align 4
	.globl	d2i_PUBKEY
	.type	d2i_PUBKEY, @function
d2i_PUBKEY:
.LFB862:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	X509_PUBKEY_it(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	xorl	%edi, %edi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	leaq	-56(%rbp), %rsi
	movq	%rax, -56(%rbp)
	call	ASN1_item_d2i@PLT
	testq	%rax, %rax
	je	.L83
	cmpq	$0, 8(%rax)
	movq	$0, -48(%rbp)
	movq	%rax, %r12
	je	.L76
	movq	16(%rax), %r14
	testq	%r14, %r14
	je	.L84
	movq	%r14, %rdi
	call	EVP_PKEY_up_ref@PLT
	leaq	X509_PUBKEY_it(%rip), %rsi
	movq	%r12, %rdi
	call	ASN1_item_free@PLT
	movq	-56(%rbp), %rax
	movq	%rax, (%rbx)
	testq	%r13, %r13
	je	.L72
	movq	0(%r13), %rdi
	call	EVP_PKEY_free@PLT
	movq	%r14, 0(%r13)
.L72:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L85
	addq	$32, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L84:
	.cfi_restore_state
	leaq	-48(%rbp), %rdi
	movq	%rax, %rsi
	call	x509_pubkey_decode
	cmpq	$0, -48(%rbp)
	je	.L76
	movl	$11, %edi
	movl	$162, %r8d
	movl	$68, %edx
	movl	$119, %esi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	-48(%rbp), %rdi
	call	EVP_PKEY_free@PLT
.L76:
	leaq	X509_PUBKEY_it(%rip), %rsi
	movq	%r12, %rdi
	call	ASN1_item_free@PLT
.L83:
	xorl	%r14d, %r14d
	jmp	.L72
.L85:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE862:
	.size	d2i_PUBKEY, .-d2i_PUBKEY
	.p2align 4
	.globl	i2d_PUBKEY
	.type	i2d_PUBKEY, @function
i2d_PUBKEY:
.LFB863:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	xorl	%r12d, %r12d
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	$0, -32(%rbp)
	testq	%rdi, %rdi
	je	.L86
	movq	%rdi, %r8
	movq	%rsi, %r13
	leaq	-32(%rbp), %rdi
	movq	%r8, %rsi
	call	X509_PUBKEY_set.part.0
	testl	%eax, %eax
	je	.L90
	movq	-32(%rbp), %rdi
	movq	%r13, %rsi
	leaq	X509_PUBKEY_it(%rip), %rdx
	call	ASN1_item_i2d@PLT
	movq	-32(%rbp), %rdi
	leaq	X509_PUBKEY_it(%rip), %rsi
	movl	%eax, %r12d
	call	ASN1_item_free@PLT
.L86:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L93
	addq	$16, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L90:
	.cfi_restore_state
	movl	$-1, %r12d
	jmp	.L86
.L93:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE863:
	.size	i2d_PUBKEY, .-i2d_PUBKEY
	.p2align 4
	.globl	d2i_RSA_PUBKEY
	.type	d2i_RSA_PUBKEY, @function
d2i_RSA_PUBKEY:
.LFB864:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	X509_PUBKEY_it(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	xorl	%edi, %edi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	leaq	-72(%rbp), %rsi
	movq	%rax, -72(%rbp)
	call	ASN1_item_d2i@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L94
	cmpq	$0, 8(%rax)
	movq	$0, -64(%rbp)
	je	.L99
	movq	16(%rax), %r15
	testq	%r15, %r15
	je	.L108
	movq	%r15, %rdi
	call	EVP_PKEY_up_ref@PLT
	movq	%r12, %rdi
	leaq	X509_PUBKEY_it(%rip), %rsi
	call	ASN1_item_free@PLT
	movq	%r15, %rdi
	movq	-72(%rbp), %r14
	call	EVP_PKEY_get1_RSA@PLT
	movq	%r15, %rdi
	movq	%rax, %r12
	call	EVP_PKEY_free@PLT
	testq	%r12, %r12
	je	.L94
	movq	%r14, (%rbx)
	testq	%r13, %r13
	je	.L94
	movq	0(%r13), %rdi
	call	RSA_free@PLT
	movq	%r12, 0(%r13)
.L94:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L109
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L108:
	.cfi_restore_state
	leaq	-64(%rbp), %rdi
	movq	%rax, %rsi
	call	x509_pubkey_decode
	cmpq	$0, -64(%rbp)
	je	.L99
	movl	$11, %edi
	movl	$162, %r8d
	movl	$68, %edx
	movl	$119, %esi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	-64(%rbp), %rdi
	call	EVP_PKEY_free@PLT
.L99:
	movq	%r12, %rdi
	leaq	X509_PUBKEY_it(%rip), %rsi
	xorl	%r12d, %r12d
	call	ASN1_item_free@PLT
	jmp	.L94
.L109:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE864:
	.size	d2i_RSA_PUBKEY, .-d2i_RSA_PUBKEY
	.p2align 4
	.globl	i2d_RSA_PUBKEY
	.type	i2d_RSA_PUBKEY, @function
i2d_RSA_PUBKEY:
.LFB865:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	xorl	%r14d, %r14d
	pushq	%r13
	pushq	%r12
	subq	$16, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L110
	movq	%rdi, %r13
	movq	%rsi, %r15
	call	EVP_PKEY_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L119
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	EVP_PKEY_set1_RSA@PLT
	leaq	-48(%rbp), %rdi
	movq	%r12, %rsi
	movq	$0, -48(%rbp)
	call	X509_PUBKEY_set.part.0
	testl	%eax, %eax
	je	.L116
	movq	-48(%rbp), %rdi
	movq	%r15, %rsi
	leaq	X509_PUBKEY_it(%rip), %rdx
	call	ASN1_item_i2d@PLT
	movq	-48(%rbp), %rdi
	leaq	X509_PUBKEY_it(%rip), %rsi
	movl	%eax, %r14d
	call	ASN1_item_free@PLT
.L113:
	movq	%r12, %rdi
	call	EVP_PKEY_free@PLT
.L110:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L120
	addq	$16, %rsp
	movl	%r14d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L116:
	.cfi_restore_state
	movl	$-1, %r14d
	jmp	.L113
.L119:
	movl	$249, %r8d
	movl	$65, %edx
	movl	$165, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	movl	$-1, %r14d
	call	ERR_put_error@PLT
	jmp	.L110
.L120:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE865:
	.size	i2d_RSA_PUBKEY, .-i2d_RSA_PUBKEY
	.p2align 4
	.globl	d2i_DSA_PUBKEY
	.type	d2i_DSA_PUBKEY, @function
d2i_DSA_PUBKEY:
.LFB866:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	X509_PUBKEY_it(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	xorl	%edi, %edi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	leaq	-72(%rbp), %rsi
	movq	%rax, -72(%rbp)
	call	ASN1_item_d2i@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L121
	cmpq	$0, 8(%rax)
	movq	$0, -64(%rbp)
	je	.L126
	movq	16(%rax), %r15
	testq	%r15, %r15
	je	.L135
	movq	%r15, %rdi
	call	EVP_PKEY_up_ref@PLT
	movq	%r12, %rdi
	leaq	X509_PUBKEY_it(%rip), %rsi
	call	ASN1_item_free@PLT
	movq	%r15, %rdi
	movq	-72(%rbp), %r14
	call	EVP_PKEY_get1_DSA@PLT
	movq	%r15, %rdi
	movq	%rax, %r12
	call	EVP_PKEY_free@PLT
	testq	%r12, %r12
	je	.L121
	movq	%r14, (%rbx)
	testq	%r13, %r13
	je	.L121
	movq	0(%r13), %rdi
	call	DSA_free@PLT
	movq	%r12, 0(%r13)
.L121:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L136
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L135:
	.cfi_restore_state
	leaq	-64(%rbp), %rdi
	movq	%rax, %rsi
	call	x509_pubkey_decode
	cmpq	$0, -64(%rbp)
	je	.L126
	movl	$11, %edi
	movl	$162, %r8d
	movl	$68, %edx
	movl	$119, %esi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	-64(%rbp), %rdi
	call	EVP_PKEY_free@PLT
.L126:
	movq	%r12, %rdi
	leaq	X509_PUBKEY_it(%rip), %rsi
	xorl	%r12d, %r12d
	call	ASN1_item_free@PLT
	jmp	.L121
.L136:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE866:
	.size	d2i_DSA_PUBKEY, .-d2i_DSA_PUBKEY
	.p2align 4
	.globl	i2d_DSA_PUBKEY
	.type	i2d_DSA_PUBKEY, @function
i2d_DSA_PUBKEY:
.LFB867:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	xorl	%r14d, %r14d
	pushq	%r13
	pushq	%r12
	subq	$16, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L137
	movq	%rdi, %r13
	movq	%rsi, %r15
	call	EVP_PKEY_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L146
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	EVP_PKEY_set1_DSA@PLT
	leaq	-48(%rbp), %rdi
	movq	%r12, %rsi
	movq	$0, -48(%rbp)
	call	X509_PUBKEY_set.part.0
	testl	%eax, %eax
	je	.L143
	movq	-48(%rbp), %rdi
	movq	%r15, %rsi
	leaq	X509_PUBKEY_it(%rip), %rdx
	call	ASN1_item_i2d@PLT
	movq	-48(%rbp), %rdi
	leaq	X509_PUBKEY_it(%rip), %rsi
	movl	%eax, %r14d
	call	ASN1_item_free@PLT
.L140:
	movq	%r12, %rdi
	call	EVP_PKEY_free@PLT
.L137:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L147
	addq	$16, %rsp
	movl	%r14d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L143:
	.cfi_restore_state
	movl	$-1, %r14d
	jmp	.L140
.L146:
	movl	$289, %r8d
	movl	$65, %edx
	movl	$161, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	movl	$-1, %r14d
	call	ERR_put_error@PLT
	jmp	.L137
.L147:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE867:
	.size	i2d_DSA_PUBKEY, .-i2d_DSA_PUBKEY
	.p2align 4
	.globl	d2i_EC_PUBKEY
	.type	d2i_EC_PUBKEY, @function
d2i_EC_PUBKEY:
.LFB868:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	X509_PUBKEY_it(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	xorl	%edi, %edi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	leaq	-72(%rbp), %rsi
	movq	%rax, -72(%rbp)
	call	ASN1_item_d2i@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L148
	cmpq	$0, 8(%rax)
	movq	$0, -64(%rbp)
	je	.L153
	movq	16(%rax), %r15
	testq	%r15, %r15
	je	.L162
	movq	%r15, %rdi
	call	EVP_PKEY_up_ref@PLT
	movq	%r12, %rdi
	leaq	X509_PUBKEY_it(%rip), %rsi
	call	ASN1_item_free@PLT
	movq	%r15, %rdi
	movq	-72(%rbp), %r14
	call	EVP_PKEY_get1_EC_KEY@PLT
	movq	%r15, %rdi
	movq	%rax, %r12
	call	EVP_PKEY_free@PLT
	testq	%r12, %r12
	je	.L148
	movq	%r14, (%rbx)
	testq	%r13, %r13
	je	.L148
	movq	0(%r13), %rdi
	call	EC_KEY_free@PLT
	movq	%r12, 0(%r13)
.L148:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L163
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L162:
	.cfi_restore_state
	leaq	-64(%rbp), %rdi
	movq	%rax, %rsi
	call	x509_pubkey_decode
	cmpq	$0, -64(%rbp)
	je	.L153
	movl	$11, %edi
	movl	$162, %r8d
	movl	$68, %edx
	movl	$119, %esi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	-64(%rbp), %rdi
	call	EVP_PKEY_free@PLT
.L153:
	movq	%r12, %rdi
	leaq	X509_PUBKEY_it(%rip), %rsi
	xorl	%r12d, %r12d
	call	ASN1_item_free@PLT
	jmp	.L148
.L163:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE868:
	.size	d2i_EC_PUBKEY, .-d2i_EC_PUBKEY
	.p2align 4
	.globl	i2d_EC_PUBKEY
	.type	i2d_EC_PUBKEY, @function
i2d_EC_PUBKEY:
.LFB869:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	xorl	%r14d, %r14d
	pushq	%r13
	pushq	%r12
	subq	$16, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L164
	movq	%rdi, %r13
	movq	%rsi, %r15
	call	EVP_PKEY_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L173
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	EVP_PKEY_set1_EC_KEY@PLT
	leaq	-48(%rbp), %rdi
	movq	%r12, %rsi
	movq	$0, -48(%rbp)
	call	X509_PUBKEY_set.part.0
	testl	%eax, %eax
	je	.L170
	movq	-48(%rbp), %rdi
	movq	%r15, %rsi
	leaq	X509_PUBKEY_it(%rip), %rdx
	call	ASN1_item_i2d@PLT
	movq	-48(%rbp), %rdi
	leaq	X509_PUBKEY_it(%rip), %rsi
	movl	%eax, %r14d
	call	ASN1_item_free@PLT
.L167:
	movq	%r12, %rdi
	call	EVP_PKEY_free@PLT
.L164:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L174
	addq	$16, %rsp
	movl	%r14d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L170:
	.cfi_restore_state
	movl	$-1, %r14d
	jmp	.L167
.L173:
	movl	$328, %r8d
	movl	$65, %edx
	movl	$181, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	movl	$-1, %r14d
	call	ERR_put_error@PLT
	jmp	.L164
.L174:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE869:
	.size	i2d_EC_PUBKEY, .-i2d_EC_PUBKEY
	.p2align 4
	.globl	X509_PUBKEY_set0_param
	.type	X509_PUBKEY_set0_param, @function
X509_PUBKEY_set0_param:
.LFB870:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%r9d, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movq	%r8, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	call	X509_ALGOR_set0@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	je	.L175
	movl	$1, %r12d
	testq	%r13, %r13
	je	.L175
	movq	8(%rbx), %rax
	movl	$345, %edx
	leaq	.LC0(%rip), %rsi
	movq	8(%rax), %rdi
	call	CRYPTO_free@PLT
	movq	8(%rbx), %rdx
	movq	16(%rdx), %rax
	movq	%r13, 8(%rdx)
	movl	%r14d, (%rdx)
	andq	$-16, %rax
	orq	$8, %rax
	movq	%rax, 16(%rdx)
.L175:
	popq	%rbx
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE870:
	.size	X509_PUBKEY_set0_param, .-X509_PUBKEY_set0_param
	.p2align 4
	.globl	X509_PUBKEY_get0_param
	.type	X509_PUBKEY_get0_param, @function
X509_PUBKEY_get0_param:
.LFB871:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L184
	movq	(%r8), %rax
	movq	(%rax), %rax
	movq	%rax, (%rdi)
.L184:
	testq	%rsi, %rsi
	je	.L185
	movq	8(%r8), %rax
	movq	8(%rax), %rdi
	movl	(%rax), %eax
	movq	%rdi, (%rsi)
	movl	%eax, (%rdx)
.L185:
	testq	%rcx, %rcx
	je	.L186
	movq	(%r8), %rax
	movq	%rax, (%rcx)
.L186:
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE871:
	.size	X509_PUBKEY_get0_param, .-X509_PUBKEY_get0_param
	.p2align 4
	.globl	X509_get0_pubkey_bitstr
	.type	X509_get0_pubkey_bitstr, @function
X509_get0_pubkey_bitstr:
.LFB872:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L198
	movq	80(%rdi), %rax
	movq	8(%rax), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L198:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE872:
	.size	X509_get0_pubkey_bitstr, .-X509_get0_pubkey_bitstr
	.globl	X509_PUBKEY_it
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"X509_PUBKEY"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	X509_PUBKEY_it, @object
	.size	X509_PUBKEY_it, 56
X509_PUBKEY_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	X509_PUBKEY_seq_tt
	.quad	2
	.quad	X509_PUBKEY_aux
	.quad	24
	.quad	.LC1
	.section	.rodata.str1.1
.LC2:
	.string	"algor"
.LC3:
	.string	"public_key"
	.section	.data.rel.ro,"aw"
	.align 32
	.type	X509_PUBKEY_seq_tt, @object
	.size	X509_PUBKEY_seq_tt, 80
X509_PUBKEY_seq_tt:
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC2
	.quad	X509_ALGOR_it
	.quad	0
	.quad	0
	.quad	8
	.quad	.LC3
	.quad	ASN1_BIT_STRING_it
	.section	.data.rel.ro.local
	.align 32
	.type	X509_PUBKEY_aux, @object
	.size	X509_PUBKEY_aux, 40
X509_PUBKEY_aux:
	.quad	0
	.long	0
	.long	0
	.long	0
	.zero	4
	.quad	pubkey_cb
	.long	0
	.zero	4
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
