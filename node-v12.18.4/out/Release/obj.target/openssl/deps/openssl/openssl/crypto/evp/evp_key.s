	.file	"evp_key.c"
	.text
	.p2align 4
	.globl	EVP_set_pw_prompt
	.type	EVP_set_pw_prompt, @function
EVP_set_pw_prompt:
.LFB803:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L8
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movl	$79, %edx
	leaq	prompt_string(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	strncpy@PLT
	movb	$0, 79+prompt_string(%rip)
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	.cfi_restore 6
	movb	$0, prompt_string(%rip)
	ret
	.cfi_endproc
.LFE803:
	.size	EVP_set_pw_prompt, .-EVP_set_pw_prompt
	.p2align 4
	.globl	EVP_get_pw_prompt
	.type	EVP_get_pw_prompt, @function
EVP_get_pw_prompt:
.LFB804:
	.cfi_startproc
	endbr64
	cmpb	$0, prompt_string(%rip)
	movl	$0, %edx
	leaq	prompt_string(%rip), %rax
	cmove	%rdx, %rax
	ret
	.cfi_endproc
.LFE804:
	.size	EVP_get_pw_prompt, .-EVP_get_pw_prompt
	.p2align 4
	.globl	EVP_read_pw_string_min
	.type	EVP_read_pw_string_min, @function
EVP_read_pw_string_min:
.LFB806:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -8264(%rbp)
	movl	%esi, %r12d
	movl	%edx, %ebx
	movq	%rcx, %r13
	movl	%r8d, %r14d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rcx, %rcx
	je	.L23
.L13:
	call	UI_new@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L21
	cmpl	$8191, %ebx
	movl	%r12d, %r8d
	movq	%r13, %rsi
	movq	%rax, %rdi
	movl	$8191, %r9d
	movq	-8264(%rbp), %rcx
	cmovg	%r9d, %ebx
	xorl	%edx, %edx
	movl	%ebx, %r9d
	call	UI_add_input_string@PLT
	testl	%eax, %eax
	js	.L18
	testl	%r14d, %r14d
	leaq	-8256(%rbp), %r14
	jne	.L17
.L19:
	movq	%r15, %rdi
	call	UI_process@PLT
	movl	$8192, %esi
	movq	%r14, %rdi
	movl	%eax, %r12d
	call	OPENSSL_cleanse@PLT
.L16:
	movq	%r15, %rdi
	call	UI_free@PLT
.L12:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L24
	leaq	-40(%rbp), %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	.cfi_restore_state
	subq	$8, %rsp
	pushq	-8264(%rbp)
	movq	%r14, %rcx
	xorl	%edx, %edx
	movl	%ebx, %r9d
	movl	%r12d, %r8d
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	UI_add_verify_string@PLT
	popq	%rdx
	popq	%rcx
	testl	%eax, %eax
	jns	.L19
.L18:
	movl	$-1, %r12d
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L23:
	cmpb	$0, prompt_string(%rip)
	leaq	prompt_string(%rip), %rax
	cmovne	%rax, %r13
	jmp	.L13
.L21:
	movl	$-1, %r12d
	jmp	.L12
.L24:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE806:
	.size	EVP_read_pw_string_min, .-EVP_read_pw_string_min
	.p2align 4
	.globl	EVP_read_pw_string
	.type	EVP_read_pw_string, @function
EVP_read_pw_string:
.LFB805:
	.cfi_startproc
	endbr64
	movl	%ecx, %r8d
	movq	%rdx, %rcx
	movl	%esi, %edx
	xorl	%esi, %esi
	jmp	EVP_read_pw_string_min
	.cfi_endproc
.LFE805:
	.size	EVP_read_pw_string, .-EVP_read_pw_string
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/evp/evp_key.c"
	.align 8
.LC1:
	.string	"assertion failed: nkey <= EVP_MAX_KEY_LENGTH"
	.align 8
.LC2:
	.string	"assertion failed: niv <= EVP_MAX_IV_LENGTH"
	.text
	.p2align 4
	.globl	EVP_BytesToKey
	.type	EVP_BytesToKey, @function
EVP_BytesToKey:
.LFB807:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%r8d, %ebx
	subq	$168, %rsp
	movq	16(%rbp), %rax
	movq	%rdi, -208(%rbp)
	movq	%rdx, -176(%rbp)
	movq	%rax, -192(%rbp)
	movq	24(%rbp), %rax
	movq	%rcx, -160(%rbp)
	movl	%r9d, -148(%rbp)
	movq	%rax, -200(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$0, -132(%rbp)
	call	EVP_CIPHER_key_length@PLT
	movq	%r14, %rdi
	movl	%eax, -152(%rbp)
	movl	%eax, %r15d
	call	EVP_CIPHER_iv_length@PLT
	movl	%eax, -164(%rbp)
	cmpl	$64, %r15d
	jg	.L111
	cmpl	$16, -164(%rbp)
	jg	.L112
	cmpq	$0, -160(%rbp)
	je	.L26
	call	EVP_MD_CTX_new@PLT
	leaq	-128(%rbp), %r12
	movl	$0, -168(%rbp)
	movq	%rax, %r15
	movslq	%ebx, %rax
	movq	%rax, -184(%rbp)
	testq	%r15, %r15
	je	.L32
.L52:
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	EVP_DigestInit_ex@PLT
	testl	%eax, %eax
	je	.L32
	movl	-168(%rbp), %edi
	testl	%edi, %edi
	jne	.L33
.L35:
	movq	-184(%rbp), %rdx
	movq	-160(%rbp), %rsi
	movq	%r15, %rdi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	je	.L32
	cmpq	$0, -176(%rbp)
	je	.L38
	movq	-176(%rbp), %rsi
	movl	$8, %edx
	movq	%r15, %rdi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	je	.L32
.L38:
	leaq	-132(%rbp), %r14
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	%r14, %rdx
	call	EVP_DigestFinal_ex@PLT
	testl	%eax, %eax
	je	.L32
	cmpl	$1, -148(%rbp)
	movl	$1, %ebx
	ja	.L39
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L113:
	movl	-132(%rbp), %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	je	.L32
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	EVP_DigestFinal_ex@PLT
	testl	%eax, %eax
	je	.L32
	addl	$1, %ebx
	cmpl	-148(%rbp), %ebx
	je	.L42
.L39:
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	EVP_DigestInit_ex@PLT
	testl	%eax, %eax
	jne	.L113
.L32:
	movl	$0, -152(%rbp)
.L31:
	movq	%r15, %rdi
	call	EVP_MD_CTX_free@PLT
	movl	$64, %esi
	movq	%r12, %rdi
	call	OPENSSL_cleanse@PLT
.L26:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L114
	movl	-152(%rbp), %eax
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L33:
	.cfi_restore_state
	movl	-132(%rbp), %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	jne	.L35
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L42:
	movl	-152(%rbp), %esi
	xorl	%edx, %edx
	testl	%esi, %esi
	jne	.L115
.L41:
	movl	-164(%rbp), %eax
	testl	%eax, %eax
	je	.L51
	movl	$0, -152(%rbp)
.L53:
	movl	-132(%rbp), %eax
	cmpl	%edx, %eax
	jne	.L116
.L48:
	addl	$1, -168(%rbp)
	jmp	.L52
.L115:
	movl	-152(%rbp), %esi
	movl	-132(%rbp), %edi
	xorl	%eax, %eax
	movq	-192(%rbp), %rcx
	leal	-1(%rsi), %r8d
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L117:
	movq	%r9, %rax
.L45:
	movl	%esi, %r9d
	movl	%eax, %edx
	subl	%eax, %r9d
	cmpq	%rdi, %rax
	je	.L43
	testq	%rcx, %rcx
	je	.L44
	movzbl	(%r12,%rax), %r9d
	addq	$1, %rcx
	movb	%r9b, -1(%rcx)
.L44:
	addl	$1, %edx
	leaq	1(%rax), %r9
	cmpq	%r8, %rax
	jne	.L117
	movq	%rcx, -192(%rbp)
	jmp	.L41
.L116:
	movl	-164(%rbp), %esi
	movq	-200(%rbp), %rcx
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L50:
	cmpl	%edx, %eax
	je	.L108
.L47:
	testq	%rcx, %rcx
	je	.L49
	movl	%edx, %edi
	addq	$1, %rcx
	movzbl	-128(%rbp,%rdi), %edi
	movb	%dil, -1(%rcx)
.L49:
	addl	$1, %edx
	subl	$1, %esi
	jne	.L50
	movq	%rcx, -200(%rbp)
.L46:
	movl	-152(%rbp), %edx
	testl	%edx, %edx
	je	.L51
	movl	$0, -164(%rbp)
	jmp	.L48
.L43:
	movq	%rcx, -192(%rbp)
	movl	-164(%rbp), %ecx
	movl	%r9d, -152(%rbp)
	testl	%ecx, %ecx
	je	.L46
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L108:
	movl	%esi, -164(%rbp)
	movq	%rcx, -200(%rbp)
	jmp	.L48
.L51:
	movq	-208(%rbp), %rdi
	call	EVP_CIPHER_key_length@PLT
	movl	%eax, -152(%rbp)
	jmp	.L31
.L114:
	call	__stack_chk_fail@PLT
.L112:
	movl	$87, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	OPENSSL_die@PLT
.L111:
	movl	$86, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	OPENSSL_die@PLT
	.cfi_endproc
.LFE807:
	.size	EVP_BytesToKey, .-EVP_BytesToKey
	.local	prompt_string
	.comm	prompt_string,80,32
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
