	.file	"rand_key.c"
	.text
	.p2align 4
	.globl	DES_random_key
	.type	DES_random_key, @function
DES_random_key:
.LFB20:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L9:
	movq	%r13, %rdi
	call	DES_is_weak_key@PLT
	testl	%eax, %eax
	je	.L8
.L3:
	movl	$8, %esi
	movq	%r13, %rdi
	call	RAND_priv_bytes@PLT
	movl	%eax, %r12d
	cmpl	$1, %eax
	je	.L9
	xorl	%r12d, %r12d
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	.cfi_restore_state
	movq	%r13, %rdi
	call	DES_set_odd_parity@PLT
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20:
	.size	DES_random_key, .-DES_random_key
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
