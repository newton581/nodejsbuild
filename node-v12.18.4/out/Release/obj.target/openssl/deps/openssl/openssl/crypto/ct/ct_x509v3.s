	.file	"ct_x509v3.c"
	.text
	.p2align 4
	.type	s2i_poison, @function
s2i_poison:
.LFB1284:
	.cfi_startproc
	endbr64
	jmp	ASN1_NULL_new@PLT
	.cfi_endproc
.LFE1284:
	.size	s2i_poison, .-s2i_poison
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/ct/ct_x509v3.c"
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"NULL"
	.text
	.p2align 4
	.type	i2s_poison, @function
i2s_poison:
.LFB1283:
	.cfi_startproc
	endbr64
	movl	$18, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	jmp	CRYPTO_strdup@PLT
	.cfi_endproc
.LFE1283:
	.size	i2s_poison, .-i2s_poison
	.section	.rodata.str1.1
.LC2:
	.string	"\n"
	.text
	.p2align 4
	.type	i2r_SCT_LIST, @function
i2r_SCT_LIST:
.LFB1285:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	xorl	%r8d, %r8d
	movq	%rdx, %rsi
	movl	%ecx, %edx
	leaq	.LC2(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	SCT_LIST_print@PLT
	movl	$1, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1285:
	.size	i2r_SCT_LIST, .-i2r_SCT_LIST
	.p2align 4
	.type	x509_ext_d2i_SCT_LIST, @function
x509_ext_d2i_SCT_LIST:
.LFB1287:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	xorl	%ebx, %ebx
	subq	$8, %rsp
	call	d2i_SCT_LIST@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L7
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L10:
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	OPENSSL_sk_value@PLT
	movl	$2, %esi
	movq	%rax, %rdi
	call	SCT_set_source@PLT
	cmpl	$1, %eax
	jne	.L9
	addl	$1, %ebx
.L7:
	movq	%r12, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jl	.L10
.L6:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	.cfi_restore_state
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	SCT_LIST_free@PLT
	movq	$0, 0(%r13)
	movq	%r12, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1287:
	.size	x509_ext_d2i_SCT_LIST, .-x509_ext_d2i_SCT_LIST
	.p2align 4
	.type	ocsp_ext_d2i_SCT_LIST, @function
ocsp_ext_d2i_SCT_LIST:
.LFB1288:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	xorl	%ebx, %ebx
	subq	$8, %rsp
	call	d2i_SCT_LIST@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L16
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L19:
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	OPENSSL_sk_value@PLT
	movl	$3, %esi
	movq	%rax, %rdi
	call	SCT_set_source@PLT
	cmpl	$1, %eax
	jne	.L18
	addl	$1, %ebx
.L16:
	movq	%r12, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jl	.L19
.L15:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	.cfi_restore_state
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	SCT_LIST_free@PLT
	movq	$0, 0(%r13)
	movq	%r12, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1288:
	.size	ocsp_ext_d2i_SCT_LIST, .-ocsp_ext_d2i_SCT_LIST
	.globl	v3_ct_scts
	.section	.data.rel.ro,"aw"
	.align 32
	.type	v3_ct_scts, @object
	.size	v3_ct_scts, 312
v3_ct_scts:
	.long	951
	.long	0
	.quad	0
	.quad	0
	.quad	SCT_LIST_free
	.quad	x509_ext_d2i_SCT_LIST
	.quad	i2d_SCT_LIST
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	i2r_SCT_LIST
	.quad	0
	.quad	0
	.long	952
	.long	0
	.quad	ASN1_NULL_it
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	i2s_poison
	.quad	s2i_poison
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.long	954
	.long	0
	.quad	0
	.quad	0
	.quad	SCT_LIST_free
	.quad	ocsp_ext_d2i_SCT_LIST
	.quad	i2d_SCT_LIST
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	i2r_SCT_LIST
	.quad	0
	.quad	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
