	.file	"pcy_lib.c"
	.text
	.p2align 4
	.globl	X509_policy_tree_level_count
	.type	X509_policy_tree_level_count, @function
X509_policy_tree_level_count:
.LFB1320:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L3
	movl	8(%rdi), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1320:
	.size	X509_policy_tree_level_count, .-X509_policy_tree_level_count
	.p2align 4
	.globl	X509_policy_tree_get0_level
	.type	X509_policy_tree_get0_level, @function
X509_policy_tree_get0_level:
.LFB1321:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L8
	testl	%esi, %esi
	js	.L8
	cmpl	%esi, 8(%rdi)
	jle	.L8
	movslq	%esi, %rax
	salq	$5, %rax
	addq	(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1321:
	.size	X509_policy_tree_get0_level, .-X509_policy_tree_get0_level
	.p2align 4
	.globl	X509_policy_tree_get0_policies
	.type	X509_policy_tree_get0_policies, @function
X509_policy_tree_get0_policies:
.LFB1322:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L11
	movq	24(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1322:
	.size	X509_policy_tree_get0_policies, .-X509_policy_tree_get0_policies
	.p2align 4
	.globl	X509_policy_tree_get0_user_policies
	.type	X509_policy_tree_get0_user_policies, @function
X509_policy_tree_get0_user_policies:
.LFB1323:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L15
	testb	$2, 40(%rdi)
	je	.L14
	movq	24(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	movq	32(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1323:
	.size	X509_policy_tree_get0_user_policies, .-X509_policy_tree_get0_user_policies
	.p2align 4
	.globl	X509_policy_level_node_count
	.type	X509_policy_level_node_count, @function
X509_policy_level_node_count:
.LFB1324:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	testq	%rdi, %rdi
	je	.L18
	xorl	%r12d, %r12d
	cmpq	$0, 16(%rdi)
	movq	8(%rdi), %rdi
	setne	%r12b
	testq	%rdi, %rdi
	je	.L16
	call	OPENSSL_sk_num@PLT
	addl	%eax, %r12d
.L16:
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	.cfi_restore_state
	xorl	%r12d, %r12d
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1324:
	.size	X509_policy_level_node_count, .-X509_policy_level_node_count
	.p2align 4
	.globl	X509_policy_level_get0_node
	.type	X509_policy_level_get0_node, @function
X509_policy_level_get0_node:
.LFB1325:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L26
	movq	16(%rdi), %rax
	testq	%rax, %rax
	je	.L25
	testl	%esi, %esi
	je	.L23
	subl	$1, %esi
.L25:
	movq	8(%rdi), %rdi
	jmp	OPENSSL_sk_value@PLT
	.p2align 4,,10
	.p2align 3
.L26:
	xorl	%eax, %eax
.L23:
	ret
	.cfi_endproc
.LFE1325:
	.size	X509_policy_level_get0_node, .-X509_policy_level_get0_node
	.p2align 4
	.globl	X509_policy_node_get0_policy
	.type	X509_policy_node_get0_policy, @function
X509_policy_node_get0_policy:
.LFB1326:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L35
	movq	(%rdi), %rax
	movq	8(%rax), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L35:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1326:
	.size	X509_policy_node_get0_policy, .-X509_policy_node_get0_policy
	.p2align 4
	.globl	X509_policy_node_get0_qualifiers
	.type	X509_policy_node_get0_qualifiers, @function
X509_policy_node_get0_qualifiers:
.LFB1327:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L38
	movq	(%rdi), %rax
	movq	16(%rax), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L38:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1327:
	.size	X509_policy_node_get0_qualifiers, .-X509_policy_node_get0_qualifiers
	.p2align 4
	.globl	X509_policy_node_get0_parent
	.type	X509_policy_node_get0_parent, @function
X509_policy_node_get0_parent:
.LFB1328:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L41
	movq	8(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L41:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1328:
	.size	X509_policy_node_get0_parent, .-X509_policy_node_get0_parent
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
