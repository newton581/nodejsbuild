	.file	"v3_sxnet.c"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	""
.LC1:
	.string	"%*sVersion: %ld (0x%lX)"
.LC2:
	.string	"\n%*sZone: %s, User: "
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"../deps/openssl/openssl/crypto/x509v3/v3_sxnet.c"
	.text
	.p2align 4
	.type	sxnet_i2r, @function
sxnet_i2r:
.LFB1328:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	xorl	%r14d, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%ecx, %ebx
	subq	$24, %rsp
	movq	(%rsi), %rdi
	movl	%ecx, -52(%rbp)
	call	ASN1_INTEGER_get@PLT
	leaq	.LC0(%rip), %rcx
	movl	%ebx, %edx
	movq	%r12, %rdi
	movq	%rax, %r9
	leaq	1(%rax), %r8
	leaq	.LC1(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L3:
	movq	8(%r13), %rdi
	movl	%r14d, %esi
	addl	$1, %r14d
	call	OPENSSL_sk_value@PLT
	xorl	%edi, %edi
	movq	(%rax), %rsi
	movq	%rax, %rbx
	call	i2s_ASN1_INTEGER@PLT
	movl	-52(%rbp), %edx
	leaq	.LC0(%rip), %rcx
	movq	%r12, %rdi
	movq	%rax, %r8
	movq	%rax, %r15
	leaq	.LC2(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movl	$70, %edx
	leaq	.LC3(%rip), %rsi
	movq	%r15, %rdi
	call	CRYPTO_free@PLT
	movq	8(%rbx), %rsi
	movq	%r12, %rdi
	call	ASN1_STRING_print@PLT
.L2:
	movq	8(%r13), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r14d
	jl	.L3
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1328:
	.size	sxnet_i2r, .-sxnet_i2r
	.p2align 4
	.type	SXNET_add_id_INTEGER.part.0, @function
SXNET_add_id_INTEGER.part.0:
.LFB1336:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%ecx, %r12d
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	%rdi, -56(%rbp)
	movq	%rdx, -64(%rbp)
	cmpl	$-1, %ecx
	jne	.L7
	movq	%rdx, %rdi
	call	strlen@PLT
	movl	%eax, %r12d
.L7:
	cmpl	$64, %r12d
	jg	.L31
	movq	-56(%rbp), %rax
	movq	(%rax), %r15
	testq	%r15, %r15
	je	.L32
.L10:
	xorl	%ebx, %ebx
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L16:
	movq	8(%r15), %rdi
	movl	%ebx, %esi
	call	OPENSSL_sk_value@PLT
	movq	%r14, %rsi
	movq	(%rax), %rdi
	movq	%rax, %r13
	call	ASN1_INTEGER_cmp@PLT
	testl	%eax, %eax
	je	.L33
	addl	$1, %ebx
.L12:
	movq	8(%r15), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jl	.L16
.L15:
	leaq	SXNETID_it(%rip), %rdi
	call	ASN1_item_new@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L11
	cmpl	$-1, %r12d
	je	.L34
.L17:
	movq	8(%r13), %rdi
	movq	-64(%rbp), %rsi
	movl	%r12d, %edx
	call	ASN1_OCTET_STRING_set@PLT
	testl	%eax, %eax
	jne	.L35
.L11:
	movl	$179, %r8d
	movl	$65, %edx
	movl	$126, %esi
	movl	$34, %edi
	leaq	.LC3(%rip), %rcx
	call	ERR_put_error@PLT
	leaq	SXNETID_it(%rip), %rsi
	movq	%r13, %rdi
	call	ASN1_item_free@PLT
	leaq	SXNET_it(%rip), %rsi
	movq	%r15, %rdi
	call	ASN1_item_free@PLT
	movq	-56(%rbp), %rax
	movq	$0, (%rax)
	xorl	%eax, %eax
.L6:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	.cfi_restore_state
	leaq	SXNET_it(%rip), %rdi
	call	ASN1_item_new@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L18
	movq	(%rax), %rdi
	xorl	%esi, %esi
	call	ASN1_INTEGER_set@PLT
	testl	%eax, %eax
	je	.L19
	movq	-56(%rbp), %rax
	movq	%r13, %r15
	movq	%r13, (%rax)
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L33:
	cmpq	$0, 8(%r13)
	je	.L15
	movl	$162, %r8d
	leaq	.LC3(%rip), %rcx
	movl	$133, %edx
	movl	%eax, -56(%rbp)
	movl	$126, %esi
	movl	$34, %edi
	call	ERR_put_error@PLT
	movl	-56(%rbp), %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L31:
	.cfi_restore_state
	movl	$150, %r8d
	movl	$132, %edx
	movl	$126, %esi
	movl	$34, %edi
	leaq	.LC3(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L35:
	.cfi_restore_state
	movq	8(%r15), %rdi
	movq	%r13, %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L11
	movq	%r14, 0(%r13)
	movl	$1, %eax
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L34:
	movq	-64(%rbp), %rdi
	call	strlen@PLT
	movl	%eax, %r12d
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L18:
	movq	%rax, %r15
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L19:
	movq	%r13, %r15
	xorl	%r13d, %r13d
	jmp	.L11
	.cfi_endproc
.LFE1336:
	.size	SXNET_add_id_INTEGER.part.0, .-SXNET_add_id_INTEGER.part.0
	.p2align 4
	.type	sxnet_v2i, @function
sxnet_v2i:
.LFB1329:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-48(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	xorl	%ebx, %ebx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -48(%rbp)
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L38:
	testq	%r14, %r14
	je	.L45
	movl	$-1, %ecx
	movq	%r14, %rdx
	movq	%r13, %rdi
	call	SXNET_add_id_INTEGER.part.0
	testl	%eax, %eax
	je	.L43
	addl	$1, %ebx
.L37:
	movq	%r12, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jge	.L46
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	OPENSSL_sk_value@PLT
	xorl	%edi, %edi
	movq	8(%rax), %rsi
	movq	16(%rax), %r14
	call	s2i_ASN1_INTEGER@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	jne	.L38
	movl	$109, %r8d
	leaq	.LC3(%rip), %rcx
	movl	$131, %edx
	xorl	%r14d, %r14d
	movl	$125, %esi
	movl	$34, %edi
	call	ERR_put_error@PLT
.L36:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L47
	addq	$16, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L45:
	.cfi_restore_state
	movl	$143, %r8d
	movl	$107, %edx
	movl	$126, %esi
	movl	$34, %edi
	leaq	.LC3(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L46:
	movq	-48(%rbp), %r14
	jmp	.L36
.L43:
	xorl	%r14d, %r14d
	jmp	.L36
.L47:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1329:
	.size	sxnet_v2i, .-sxnet_v2i
	.p2align 4
	.globl	d2i_SXNETID
	.type	d2i_SXNETID, @function
d2i_SXNETID:
.LFB1320:
	.cfi_startproc
	endbr64
	leaq	SXNETID_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE1320:
	.size	d2i_SXNETID, .-d2i_SXNETID
	.p2align 4
	.globl	i2d_SXNETID
	.type	i2d_SXNETID, @function
i2d_SXNETID:
.LFB1321:
	.cfi_startproc
	endbr64
	leaq	SXNETID_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE1321:
	.size	i2d_SXNETID, .-i2d_SXNETID
	.p2align 4
	.globl	SXNETID_new
	.type	SXNETID_new, @function
SXNETID_new:
.LFB1322:
	.cfi_startproc
	endbr64
	leaq	SXNETID_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE1322:
	.size	SXNETID_new, .-SXNETID_new
	.p2align 4
	.globl	SXNETID_free
	.type	SXNETID_free, @function
SXNETID_free:
.LFB1323:
	.cfi_startproc
	endbr64
	leaq	SXNETID_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE1323:
	.size	SXNETID_free, .-SXNETID_free
	.p2align 4
	.globl	d2i_SXNET
	.type	d2i_SXNET, @function
d2i_SXNET:
.LFB1324:
	.cfi_startproc
	endbr64
	leaq	SXNET_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE1324:
	.size	d2i_SXNET, .-d2i_SXNET
	.p2align 4
	.globl	i2d_SXNET
	.type	i2d_SXNET, @function
i2d_SXNET:
.LFB1325:
	.cfi_startproc
	endbr64
	leaq	SXNET_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE1325:
	.size	i2d_SXNET, .-i2d_SXNET
	.p2align 4
	.globl	SXNET_new
	.type	SXNET_new, @function
SXNET_new:
.LFB1326:
	.cfi_startproc
	endbr64
	leaq	SXNET_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE1326:
	.size	SXNET_new, .-SXNET_new
	.p2align 4
	.globl	SXNET_free
	.type	SXNET_free, @function
SXNET_free:
.LFB1327:
	.cfi_startproc
	endbr64
	leaq	SXNET_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE1327:
	.size	SXNET_free, .-SXNET_free
	.p2align 4
	.globl	SXNET_add_id_asc
	.type	SXNET_add_id_asc, @function
SXNET_add_id_asc:
.LFB1330:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%ecx, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	xorl	%edi, %edi
	subq	$8, %rsp
	call	s2i_ASN1_INTEGER@PLT
	testq	%rax, %rax
	je	.L63
	testq	%r12, %r12
	je	.L61
	testq	%r13, %r13
	je	.L61
	addq	$8, %rsp
	movl	%r14d, %ecx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	SXNET_add_id_INTEGER.part.0
	.p2align 4,,10
	.p2align 3
.L61:
	.cfi_restore_state
	movl	$143, %r8d
	movl	$107, %edx
	movl	$126, %esi
	movl	$34, %edi
	leaq	.LC3(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L63:
	.cfi_restore_state
	movl	$109, %r8d
	movl	$131, %edx
	movl	$125, %esi
	movl	$34, %edi
	leaq	.LC3(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1330:
	.size	SXNET_add_id_asc, .-SXNET_add_id_asc
	.p2align 4
	.globl	SXNET_add_id_ulong
	.type	SXNET_add_id_ulong, @function
SXNET_add_id_ulong:
.LFB1331:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%ecx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$8, %rsp
	call	ASN1_INTEGER_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L67
	movq	%rbx, %rsi
	movq	%rax, %rdi
	call	ASN1_INTEGER_set@PLT
	testl	%eax, %eax
	jne	.L75
.L67:
	movl	$34, %edi
	movl	$124, %r8d
	movl	$65, %edx
	movl	$127, %esi
	leaq	.LC3(%rip), %rcx
	call	ERR_put_error@PLT
	movq	%r12, %rdi
	call	ASN1_INTEGER_free@PLT
.L64:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L75:
	.cfi_restore_state
	testq	%r13, %r13
	je	.L70
	testq	%r14, %r14
	je	.L70
	addq	$8, %rsp
	movl	%r15d, %ecx
	movq	%r14, %rdx
	movq	%r12, %rsi
	popq	%rbx
	movq	%r13, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	SXNET_add_id_INTEGER.part.0
	.p2align 4,,10
	.p2align 3
.L70:
	.cfi_restore_state
	movl	$143, %r8d
	movl	$107, %edx
	movl	$126, %esi
	movl	$34, %edi
	leaq	.LC3(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L64
	.cfi_endproc
.LFE1331:
	.size	SXNET_add_id_ulong, .-SXNET_add_id_ulong
	.p2align 4
	.globl	SXNET_add_id_INTEGER
	.type	SXNET_add_id_INTEGER, @function
SXNET_add_id_INTEGER:
.LFB1332:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%rsi, %rsi
	movq	%rdi, -64(%rbp)
	sete	%dl
	testq	%r14, %r14
	sete	%al
	orb	%al, %dl
	jne	.L92
	testq	%rdi, %rdi
	je	.L92
	movq	%rsi, %rbx
	movl	%ecx, %r12d
	cmpl	$-1, %ecx
	je	.L103
	cmpl	$64, %r12d
	jg	.L104
.L81:
	movq	-64(%rbp), %rax
	movq	(%rax), %r13
	testq	%r13, %r13
	je	.L105
.L82:
	xorl	%r15d, %r15d
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L88:
	movq	8(%r13), %rdi
	movl	%r15d, %esi
	call	OPENSSL_sk_value@PLT
	movq	%rbx, %rsi
	movq	(%rax), %rdi
	movq	%rax, -56(%rbp)
	call	ASN1_INTEGER_cmp@PLT
	movq	-56(%rbp), %rdx
	testl	%eax, %eax
	je	.L106
	addl	$1, %r15d
.L84:
	movq	8(%r13), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r15d
	jl	.L88
.L87:
	leaq	SXNETID_it(%rip), %rdi
	call	ASN1_item_new@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L83
	cmpl	$-1, %r12d
	jne	.L89
	movq	%r14, %rdi
	call	strlen@PLT
	movl	%eax, %r12d
.L89:
	movq	8(%r15), %rdi
	movl	%r12d, %edx
	movq	%r14, %rsi
	call	ASN1_OCTET_STRING_set@PLT
	testl	%eax, %eax
	je	.L83
	movq	8(%r13), %rdi
	movq	%r15, %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L83
	movq	%rbx, (%r15)
	movl	$1, %eax
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L103:
	movq	%r14, %rdi
	call	strlen@PLT
	movl	%eax, %r12d
	cmpl	$64, %r12d
	jle	.L81
.L104:
	movl	$150, %r8d
	movl	$132, %edx
	movl	$126, %esi
	movl	$34, %edi
	leaq	.LC3(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L91:
	.cfi_restore_state
	movq	%r15, %r13
	xorl	%r15d, %r15d
	.p2align 4,,10
	.p2align 3
.L83:
	movl	$179, %r8d
	movl	$65, %edx
	movl	$126, %esi
	movl	$34, %edi
	leaq	.LC3(%rip), %rcx
	call	ERR_put_error@PLT
	leaq	SXNETID_it(%rip), %rsi
	movq	%r15, %rdi
	call	ASN1_item_free@PLT
	leaq	SXNET_it(%rip), %rsi
	movq	%r13, %rdi
	call	ASN1_item_free@PLT
	movq	-64(%rbp), %rax
	movq	$0, (%rax)
	xorl	%eax, %eax
.L76:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L92:
	.cfi_restore_state
	movl	$143, %r8d
	movl	$107, %edx
	movl	$126, %esi
	movl	$34, %edi
	leaq	.LC3(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L106:
	.cfi_restore_state
	cmpq	$0, 8(%rdx)
	je	.L87
	movl	$162, %r8d
	leaq	.LC3(%rip), %rcx
	movl	$133, %edx
	movl	%eax, -56(%rbp)
	movl	$126, %esi
	movl	$34, %edi
	call	ERR_put_error@PLT
	movl	-56(%rbp), %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L105:
	.cfi_restore_state
	leaq	SXNET_it(%rip), %rdi
	call	ASN1_item_new@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L90
	movq	(%rax), %rdi
	xorl	%esi, %esi
	call	ASN1_INTEGER_set@PLT
	testl	%eax, %eax
	je	.L91
	movq	-64(%rbp), %rax
	movq	%r15, %r13
	movq	%r15, (%rax)
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L90:
	movq	%rax, %r13
	jmp	.L83
	.cfi_endproc
.LFE1332:
	.size	SXNET_add_id_INTEGER, .-SXNET_add_id_INTEGER
	.p2align 4
	.globl	SXNET_get_id_asc
	.type	SXNET_get_id_asc, @function
SXNET_get_id_asc:
.LFB1333:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	xorl	%edi, %edi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	xorl	%ebx, %ebx
	call	s2i_ASN1_INTEGER@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L108
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L112:
	movq	8(%r13), %rdi
	movl	%ebx, %esi
	call	OPENSSL_sk_value@PLT
	movq	%r12, %rsi
	movq	(%rax), %rdi
	movq	%rax, %r14
	call	ASN1_INTEGER_cmp@PLT
	testl	%eax, %eax
	je	.L118
	addl	$1, %ebx
.L108:
	movq	8(%r13), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jl	.L112
	xorl	%r13d, %r13d
.L111:
	movq	%r12, %rdi
	call	ASN1_INTEGER_free@PLT
.L107:
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L118:
	.cfi_restore_state
	movq	8(%r14), %r13
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L117:
	movl	$192, %r8d
	leaq	.LC3(%rip), %rcx
	movl	$131, %edx
	xorl	%r13d, %r13d
	movl	$128, %esi
	movl	$34, %edi
	call	ERR_put_error@PLT
	jmp	.L107
	.cfi_endproc
.LFE1333:
	.size	SXNET_get_id_asc, .-SXNET_get_id_asc
	.p2align 4
	.globl	SXNET_get_id_ulong
	.type	SXNET_get_id_ulong, @function
SXNET_get_id_ulong:
.LFB1334:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	call	ASN1_INTEGER_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L120
	movq	%r13, %rsi
	movq	%rax, %rdi
	xorl	%r13d, %r13d
	call	ASN1_INTEGER_set@PLT
	testl	%eax, %eax
	jne	.L121
	jmp	.L120
	.p2align 4,,10
	.p2align 3
.L125:
	movq	8(%rbx), %rdi
	movl	%r13d, %esi
	call	OPENSSL_sk_value@PLT
	movq	%r12, %rsi
	movq	(%rax), %rdi
	movq	%rax, %r14
	call	ASN1_INTEGER_cmp@PLT
	testl	%eax, %eax
	je	.L132
	addl	$1, %r13d
.L121:
	movq	8(%rbx), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r13d
	jl	.L125
	xorl	%r13d, %r13d
.L124:
	movq	%r12, %rdi
	call	ASN1_INTEGER_free@PLT
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L120:
	.cfi_restore_state
	movl	$207, %r8d
	leaq	.LC3(%rip), %rcx
	movl	$65, %edx
	xorl	%r13d, %r13d
	movl	$129, %esi
	movl	$34, %edi
	call	ERR_put_error@PLT
	movq	%r12, %rdi
	call	ASN1_INTEGER_free@PLT
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L132:
	.cfi_restore_state
	movq	8(%r14), %r13
	jmp	.L124
	.cfi_endproc
.LFE1334:
	.size	SXNET_get_id_ulong, .-SXNET_get_id_ulong
	.p2align 4
	.globl	SXNET_get_id_INTEGER
	.type	SXNET_get_id_INTEGER, @function
SXNET_get_id_INTEGER:
.LFB1335:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	xorl	%ebx, %ebx
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L137:
	movq	8(%r13), %rdi
	movl	%ebx, %esi
	call	OPENSSL_sk_value@PLT
	movq	%r14, %rsi
	movq	(%rax), %rdi
	movq	%rax, %r12
	call	ASN1_INTEGER_cmp@PLT
	testl	%eax, %eax
	je	.L139
	addl	$1, %ebx
.L134:
	movq	8(%r13), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jl	.L137
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L139:
	.cfi_restore_state
	popq	%rbx
	movq	8(%r12), %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1335:
	.size	SXNET_get_id_INTEGER, .-SXNET_get_id_INTEGER
	.globl	SXNET_it
	.section	.rodata.str1.1
.LC4:
	.string	"SXNET"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	SXNET_it, @object
	.size	SXNET_it, 56
SXNET_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	SXNET_seq_tt
	.quad	2
	.quad	0
	.quad	16
	.quad	.LC4
	.section	.rodata.str1.1
.LC5:
	.string	"version"
.LC6:
	.string	"ids"
	.section	.data.rel.ro,"aw"
	.align 32
	.type	SXNET_seq_tt, @object
	.size	SXNET_seq_tt, 80
SXNET_seq_tt:
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC5
	.quad	ASN1_INTEGER_it
	.quad	4
	.quad	0
	.quad	8
	.quad	.LC6
	.quad	SXNETID_it
	.globl	SXNETID_it
	.section	.rodata.str1.1
.LC7:
	.string	"SXNETID"
	.section	.data.rel.ro.local
	.align 32
	.type	SXNETID_it, @object
	.size	SXNETID_it, 56
SXNETID_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	SXNETID_seq_tt
	.quad	2
	.quad	0
	.quad	16
	.quad	.LC7
	.section	.rodata.str1.1
.LC8:
	.string	"zone"
.LC9:
	.string	"user"
	.section	.data.rel.ro
	.align 32
	.type	SXNETID_seq_tt, @object
	.size	SXNETID_seq_tt, 80
SXNETID_seq_tt:
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC8
	.quad	ASN1_INTEGER_it
	.quad	0
	.quad	0
	.quad	8
	.quad	.LC9
	.quad	ASN1_OCTET_STRING_it
	.globl	v3_sxnet
	.section	.data.rel.ro.local
	.align 32
	.type	v3_sxnet, @object
	.size	v3_sxnet, 104
v3_sxnet:
	.long	143
	.long	4
	.quad	SXNET_it
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	sxnet_v2i
	.quad	sxnet_i2r
	.quad	0
	.quad	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
