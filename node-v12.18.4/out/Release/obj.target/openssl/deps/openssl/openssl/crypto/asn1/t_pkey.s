	.file	"t_pkey.c"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	""
.LC1:
	.string	":"
.LC2:
	.string	"\n"
.LC3:
	.string	"%02x%s"
	.text
	.p2align 4
	.globl	ASN1_buf_print
	.type	ASN1_buf_print, @function
ASN1_buf_print:
.LFB419:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%ecx, -52(%rbp)
	testq	%rdx, %rdx
	je	.L2
	movq	%rsi, %r12
	movq	%rdx, %rbx
	xorl	%r13d, %r13d
	movabsq	$-1229782938247303441, %r14
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L20:
	addq	$1, %r13
	cmpq	%r13, %rbx
	je	.L2
.L11:
	movabsq	$1229782938247303441, %rsi
	movq	%r13, %rax
	imulq	%r14, %rax
	cmpq	%rsi, %rax
	ja	.L8
	testq	%r13, %r13
	je	.L9
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	BIO_puts@PLT
	testl	%eax, %eax
	jle	.L7
.L9:
	movl	-52(%rbp), %esi
	movl	$128, %edx
	movq	%r15, %rdi
	call	BIO_indent@PLT
	testl	%eax, %eax
	je	.L7
.L8:
	leaq	-1(%rbx), %rax
	movzbl	(%r12,%r13), %edx
	movq	%r15, %rdi
	leaq	.LC1(%rip), %rcx
	cmpq	%r13, %rax
	leaq	.LC0(%rip), %rax
	leaq	.LC3(%rip), %rsi
	cmove	%rax, %rcx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	testl	%eax, %eax
	jg	.L20
.L7:
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	.cfi_restore_state
	movq	%r15, %rdi
	movl	$1, %edx
	leaq	.LC2(%rip), %rsi
	call	BIO_write@PLT
	testl	%eax, %eax
	setg	%al
	addq	$24, %rsp
	popq	%rbx
	movzbl	%al, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE419:
	.size	ASN1_buf_print, .-ASN1_buf_print
	.section	.rodata.str1.1
.LC4:
	.string	"-"
.LC5:
	.string	" (Negative)"
.LC6:
	.string	"%s 0\n"
.LC7:
	.string	"%s %s%lu (%s0x%lx)\n"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC8:
	.string	"../deps/openssl/openssl/crypto/asn1/t_pkey.c"
	.section	.rodata.str1.1
.LC9:
	.string	"%s%s\n"
	.text
	.p2align 4
	.globl	ASN1_bn_print
	.type	ASN1_bn_print, @function
ASN1_bn_print:
.LFB420:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%r8d, %ebx
	movl	$1, %r8d
	subq	$40, %rsp
	movq	%rsi, -56(%rbp)
	testq	%rdx, %rdx
	je	.L23
	movq	%rdi, %r12
	movq	%rdx, %rdi
	leaq	.LC4(%rip), %r14
	movq	%rdx, %r15
	call	BN_is_negative@PLT
	movl	$128, %edx
	movl	%ebx, %esi
	movq	%r12, %rdi
	cmpl	$1, %eax
	sbbl	%r13d, %r13d
	notl	%r13d
	andl	$45, %r13d
	testl	%eax, %eax
	leaq	.LC0(%rip), %rax
	cmove	%rax, %r14
	call	BIO_indent@PLT
	movl	%eax, %r8d
	testl	%eax, %eax
	jne	.L41
.L23:
	leaq	-40(%rbp), %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L41:
	.cfi_restore_state
	movq	%r15, %rdi
	call	BN_is_zero@PLT
	testl	%eax, %eax
	jne	.L42
	movq	%r15, %rdi
	movl	%eax, -64(%rbp)
	call	BN_num_bits@PLT
	movl	-64(%rbp), %r8d
	cmpl	$64, %eax
	jg	.L27
	movq	%r15, %rdi
	call	bn_get_words@PLT
	movq	%r15, %rdi
	movq	(%rax), %rbx
	call	bn_get_words@PLT
	subq	$8, %rsp
	movq	-56(%rbp), %rdx
	movq	%r14, %rcx
	pushq	%rbx
	movq	(%rax), %r8
	movq	%r14, %r9
	xorl	%eax, %eax
	leaq	.LC7(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_printf@PLT
	xorl	%r8d, %r8d
	popq	%rdx
	popq	%rcx
	testl	%eax, %eax
	setg	%r8b
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L42:
	movq	-56(%rbp), %rdx
	xorl	%eax, %eax
	leaq	.LC6(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_printf@PLT
	xorl	%r8d, %r8d
	testl	%eax, %eax
	setg	%r8b
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L27:
	movq	%r15, %rdi
	movl	%r8d, -64(%rbp)
	call	BN_num_bits@PLT
	movl	$73, %edx
	leaq	.LC8(%rip), %rsi
	leal	14(%rax), %r14d
	addl	$7, %eax
	cmovns	%eax, %r14d
	sarl	$3, %r14d
	addl	$1, %r14d
	movslq	%r14d, %r14
	movq	%r14, %rdi
	call	CRYPTO_malloc@PLT
	movl	-64(%rbp), %r8d
	testq	%rax, %rax
	movq	%rax, %r10
	je	.L29
	cmpb	$45, %r13b
	movb	$0, (%rax)
	movq	-56(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	leaq	.LC5(%rip), %rcx
	leaq	.LC0(%rip), %rax
	cmovne	%rax, %rcx
	leaq	.LC9(%rip), %rsi
	xorl	%eax, %eax
	movl	%r8d, -68(%rbp)
	call	BIO_printf@PLT
	movq	-64(%rbp), %r10
	movl	-68(%rbp), %r8d
	testl	%eax, %eax
	jle	.L29
	leaq	1(%r10), %r13
	movq	%r15, %rdi
	movq	%r10, -56(%rbp)
	movq	%r13, %rsi
	call	BN_bn2bin@PLT
	movq	-56(%rbp), %r10
	cmpb	$0, 1(%r10)
	jns	.L32
	addl	$1, %eax
	movq	%r10, %r13
.L32:
	leal	4(%rbx), %ecx
	movslq	%eax, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r10, -56(%rbp)
	call	ASN1_buf_print
	xorl	%r8d, %r8d
	movq	-56(%rbp), %r10
	testl	%eax, %eax
	setne	%r8b
.L29:
	movl	$91, %ecx
	movq	%r14, %rsi
	movq	%r10, %rdi
	movl	%r8d, -56(%rbp)
	leaq	.LC8(%rip), %rdx
	call	CRYPTO_clear_free@PLT
	movl	-56(%rbp), %r8d
	jmp	.L23
	.cfi_endproc
.LFE420:
	.size	ASN1_bn_print, .-ASN1_bn_print
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
