	.file	"e_des.c"
	.text
	.p2align 4
	.type	des_init_key, @function
des_init_key:
.LFB457:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	%r12, %rdi
	movq	$0, 128(%rax)
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	DES_set_key_unchecked@PLT
	popq	%r12
	movl	$1, %eax
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE457:
	.size	des_init_key, .-des_init_key
	.p2align 4
	.type	des_cfb64_cipher, @function
des_cfb64_cipher:
.LFB448:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$72, %rsp
	movq	%rsi, -96(%rbp)
	movq	%rdx, -104(%rbp)
	movq	%fs:40, %rdx
	movq	%rdx, -56(%rbp)
	xorl	%edx, %edx
	movabsq	$4611686018427387903, %rdx
	cmpq	%rdx, %rcx
	jbe	.L5
	leaq	(%rsi,%rcx), %rbx
	addq	%rcx, %rax
	movq	%rdx, -72(%rbp)
	movq	%rcx, %r13
	movq	%rbx, -80(%rbp)
	leaq	-60(%rbp), %r15
	movq	%rax, -88(%rbp)
	movq	%rcx, -112(%rbp)
.L6:
	movq	%r12, %rdi
	call	EVP_CIPHER_CTX_num@PLT
	movq	%r12, %rdi
	movl	%eax, -60(%rbp)
	call	EVP_CIPHER_CTX_encrypting@PLT
	movq	%r12, %rdi
	movl	%eax, %r14d
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	subq	$8, %rsp
	movq	-80(%rbp), %rsi
	movq	%r15, %r9
	pushq	%r14
	movq	-88(%rbp), %rdi
	movq	%rax, %rcx
	movq	%rbx, %r8
	movabsq	$4611686018427387904, %rdx
	subq	%r13, %rsi
	subq	%r13, %rdi
	call	DES_cfb64_encrypt@PLT
	movl	-60(%rbp), %esi
	movq	%r12, %rdi
	call	EVP_CIPHER_CTX_set_num@PLT
	popq	%rcx
	popq	%rsi
	movabsq	$-4611686018427387904, %rax
	addq	%rax, %r13
	cmpq	-72(%rbp), %r13
	ja	.L6
	movq	-112(%rbp), %r13
	movq	-72(%rbp), %rbx
	leaq	0(%r13,%rax), %r11
	andq	%r13, %rbx
	shrq	$62, %r11
	addq	$1, %r11
	salq	$62, %r11
	addq	%r11, -104(%rbp)
	addq	%r11, -96(%rbp)
.L5:
	testq	%rbx, %rbx
	jne	.L16
.L7:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L17
	leaq	-40(%rbp), %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	.cfi_restore_state
	movq	%r12, %rdi
	call	EVP_CIPHER_CTX_num@PLT
	movq	%r12, %rdi
	movl	%eax, -60(%rbp)
	call	EVP_CIPHER_CTX_encrypting@PLT
	movq	%r12, %rdi
	movl	%eax, %r15d
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	subq	$8, %rsp
	movq	-96(%rbp), %rsi
	movq	%rbx, %rdx
	pushq	%r15
	movq	-104(%rbp), %rdi
	movq	%rax, %rcx
	leaq	-60(%rbp), %r9
	movq	%r14, %r8
	call	DES_cfb64_encrypt@PLT
	movl	-60(%rbp), %esi
	movq	%r12, %rdi
	call	EVP_CIPHER_CTX_set_num@PLT
	popq	%rax
	popq	%rdx
	jmp	.L7
.L17:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE448:
	.size	des_cfb64_cipher, .-des_cfb64_cipher
	.p2align 4
	.type	des_ofb_cipher, @function
des_ofb_cipher:
.LFB446:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	%rdx, -96(%rbp)
	movabsq	$4611686018427387903, %rdx
	movq	%rsi, -88(%rbp)
	movq	%fs:40, %rbx
	movq	%rbx, -56(%rbp)
	xorl	%ebx, %ebx
	movq	%rcx, %rbx
	cmpq	%rdx, %rcx
	jbe	.L19
	addq	%rcx, %rax
	movq	%rcx, -104(%rbp)
	leaq	(%rsi,%rcx), %rbx
	movq	%rcx, %r13
	movq	%rax, -80(%rbp)
	leaq	-60(%rbp), %r15
	movq	%rbx, -72(%rbp)
	movq	%rdx, %rbx
.L20:
	movq	%r12, %rdi
	call	EVP_CIPHER_CTX_num@PLT
	movq	%r12, %rdi
	movl	%eax, -60(%rbp)
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	-72(%rbp), %rsi
	movq	-80(%rbp), %rdi
	movq	%r15, %r9
	movabsq	$4611686018427387904, %rdx
	movq	%rax, %rcx
	movq	%r14, %r8
	subq	%r13, %rsi
	subq	%r13, %rdi
	call	DES_ofb64_encrypt@PLT
	movl	-60(%rbp), %esi
	movq	%r12, %rdi
	call	EVP_CIPHER_CTX_set_num@PLT
	movabsq	$-4611686018427387904, %rax
	addq	%rax, %r13
	cmpq	%rbx, %r13
	ja	.L20
	movq	-104(%rbp), %r13
	leaq	0(%r13,%rax), %r11
	andq	%r13, %rbx
	shrq	$62, %r11
	addq	$1, %r11
	salq	$62, %r11
	addq	%r11, -96(%rbp)
	addq	%r11, -88(%rbp)
.L19:
	testq	%rbx, %rbx
	jne	.L30
.L21:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L31
	addq	$72, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	.cfi_restore_state
	movq	%r12, %rdi
	call	EVP_CIPHER_CTX_num@PLT
	movq	%r12, %rdi
	movl	%eax, -60(%rbp)
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	-88(%rbp), %rsi
	movq	-96(%rbp), %rdi
	movq	%r14, %r8
	movq	%rax, %rcx
	leaq	-60(%rbp), %r9
	movq	%rbx, %rdx
	call	DES_ofb64_encrypt@PLT
	movl	-60(%rbp), %esi
	movq	%r12, %rdi
	call	EVP_CIPHER_CTX_set_num@PLT
	jmp	.L21
.L31:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE446:
	.size	des_ofb_cipher, .-des_ofb_cipher
	.p2align 4
	.type	des_ecb_cipher, @function
des_ecb_cipher:
.LFB445:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%rsi, -56(%rbp)
	call	EVP_CIPHER_CTX_cipher@PLT
	movslq	4(%rax), %r15
	cmpq	%r12, %r15
	ja	.L33
	subq	%r15, %r12
	movq	%r12, -64(%rbp)
	xorl	%r12d, %r12d
	.p2align 4,,10
	.p2align 3
.L34:
	movq	%rbx, %rdi
	call	EVP_CIPHER_CTX_encrypting@PLT
	movq	%rbx, %rdi
	movl	%eax, %r13d
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	leaq	(%r14,%r12), %rdi
	movl	%r13d, %ecx
	movq	%rax, %rdx
	movq	-56(%rbp), %rax
	leaq	(%rax,%r12), %rsi
	addq	%r15, %r12
	call	DES_ecb_encrypt@PLT
	cmpq	%r12, -64(%rbp)
	jnb	.L34
.L33:
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE445:
	.size	des_ecb_cipher, .-des_ecb_cipher
	.p2align 4
	.type	des_cfb1_cipher, @function
des_cfb1_cipher:
.LFB449:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$88, %rsp
	movq	%rsi, -80(%rbp)
	movq	%rdx, -88(%rbp)
	movq	%rcx, -112(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movabsq	$576460752303423487, %rax
	cmpq	%rax, %rcx
	ja	.L45
	movq	%rcx, -120(%rbp)
	testq	%rcx, %rcx
	je	.L39
.L38:
	leaq	-57(%rbp), %rax
	movq	%rax, -104(%rbp)
	.p2align 4,,10
	.p2align 3
.L41:
	movq	-120(%rbp), %rax
	xorl	%r12d, %r12d
	leaq	-58(%rbp), %r14
	movq	%r12, %r13
	salq	$3, %rax
	movq	%rax, -96(%rbp)
	je	.L43
	.p2align 4,,10
	.p2align 3
.L40:
	movq	-88(%rbp), %rax
	movq	%r13, %r15
	movl	%r13d, %ecx
	movq	%rbx, %rdi
	shrq	$3, %r15
	notl	%ecx
	movzbl	(%rax,%r15), %eax
	andl	$7, %ecx
	sarl	%cl, %eax
	sall	$7, %eax
	movb	%al, -58(%rbp)
	call	EVP_CIPHER_CTX_encrypting@PLT
	movq	%rbx, %rdi
	movl	%eax, %r12d
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movq	%rbx, %rdi
	movq	%rax, -72(%rbp)
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	subq	$8, %rsp
	movq	-104(%rbp), %rsi
	movq	%r14, %rdi
	pushq	%r12
	movq	-72(%rbp), %r9
	movq	%rax, %r8
	movl	$1, %ecx
	movl	$1, %edx
	call	DES_cfb_encrypt@PLT
	movq	-80(%rbp), %rax
	movzbl	-57(%rbp), %edx
	movl	%r13d, %ecx
	andl	$7, %ecx
	addq	$1, %r13
	leaq	(%rax,%r15), %rsi
	andl	$-128, %edx
	movl	$128, %eax
	sarl	%cl, %eax
	movzbl	%dl, %edx
	sarl	%cl, %edx
	notl	%eax
	andb	(%rsi), %al
	orl	%edx, %eax
	movb	%al, (%rsi)
	popq	%rax
	popq	%rdx
	cmpq	-96(%rbp), %r13
	jne	.L40
.L43:
	movq	-120(%rbp), %rax
	subq	%rax, -112(%rbp)
	movq	-112(%rbp), %rsi
	addq	%rax, -88(%rbp)
	addq	%rax, -80(%rbp)
	cmpq	%rsi, %rax
	cmova	%rsi, %rax
	movq	%rax, -120(%rbp)
	testq	%rsi, %rsi
	je	.L39
	cmpq	%rsi, %rax
	jbe	.L41
.L39:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L56
	leaq	-40(%rbp), %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L45:
	.cfi_restore_state
	movabsq	$576460752303423488, %rax
	movq	%rax, -120(%rbp)
	jmp	.L38
.L56:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE449:
	.size	des_cfb1_cipher, .-des_cfb1_cipher
	.p2align 4
	.type	des_cfb8_cipher, @function
des_cfb8_cipher:
.LFB450:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	movabsq	$4611686018427387903, %rcx
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r14, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -72(%rbp)
	movq	%rdx, -80(%rbp)
	cmpq	%rcx, %r14
	jbe	.L58
	leaq	(%rsi,%r14), %rax
	movq	%rcx, -56(%rbp)
	movq	%r14, %rbx
	leaq	(%rdx,%r14), %r15
	movq	%rax, -64(%rbp)
	movq	%r14, -88(%rbp)
.L59:
	movq	%r13, %rdi
	call	EVP_CIPHER_CTX_encrypting@PLT
	movq	%r13, %rdi
	movl	%eax, %r12d
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movq	%r13, %rdi
	movq	%rax, %r14
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	subq	$8, %rsp
	movq	-64(%rbp), %rsi
	movq	%r15, %rdi
	pushq	%r12
	movq	%rax, %r8
	subq	%rbx, %rdi
	movq	%r14, %r9
	subq	%rbx, %rsi
	movl	$8, %edx
	movabsq	$4611686018427387904, %rcx
	call	DES_cfb_encrypt@PLT
	popq	%rcx
	popq	%rsi
	movabsq	$-4611686018427387904, %rax
	addq	%rax, %rbx
	cmpq	-56(%rbp), %rbx
	ja	.L59
	movq	-88(%rbp), %r14
	movq	-56(%rbp), %r12
	leaq	(%r14,%rax), %r10
	andq	%r14, %r12
	shrq	$62, %r10
	addq	$1, %r10
	salq	$62, %r10
	addq	%r10, -80(%rbp)
	addq	%r10, -72(%rbp)
.L58:
	testq	%r12, %r12
	jne	.L68
.L60:
	leaq	-40(%rbp), %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L68:
	.cfi_restore_state
	movq	%r13, %rdi
	call	EVP_CIPHER_CTX_encrypting@PLT
	movq	%r13, %rdi
	movl	%eax, %r15d
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	subq	$8, %rsp
	movq	-72(%rbp), %rsi
	movq	%rbx, %r9
	pushq	%r15
	movq	-80(%rbp), %rdi
	movq	%rax, %r8
	movl	$8, %edx
	movq	%r12, %rcx
	call	DES_cfb_encrypt@PLT
	popq	%rax
	popq	%rdx
	jmp	.L60
	.cfi_endproc
.LFE450:
	.size	des_cfb8_cipher, .-des_cfb8_cipher
	.p2align 4
	.type	des_ctrl, @function
des_ctrl:
.LFB458:
	.cfi_startproc
	endbr64
	movl	$-1, %eax
	cmpl	$6, %esi
	je	.L77
	ret
	.p2align 4,,10
	.p2align 3
.L77:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$8, %esi
	movq	%rcx, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rcx, %r12
	subq	$8, %rsp
	call	RAND_priv_bytes@PLT
	movl	%eax, %r8d
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jle	.L69
	movq	%r12, %rdi
	call	DES_set_odd_parity@PLT
	movl	$1, %eax
.L69:
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE458:
	.size	des_ctrl, .-des_ctrl
	.p2align 4
	.type	des_cbc_cipher, @function
des_cbc_cipher:
.LFB447:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%rdx, -56(%rbp)
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	128(%rax), %rbx
	testq	%rbx, %rbx
	jne	.L79
	movabsq	$4611686018427387903, %rbx
	cmpq	%rbx, %r12
	jbe	.L89
	leaq	(%r14,%r12), %rax
	movq	%r14, -80(%rbp)
	movq	%rax, -64(%rbp)
	movq	-56(%rbp), %rax
	movq	%r12, -88(%rbp)
	addq	%r12, %rax
	movq	%rax, -72(%rbp)
.L80:
	movq	%r13, %rdi
	call	EVP_CIPHER_CTX_encrypting@PLT
	movq	%r13, %rdi
	movl	%eax, %r15d
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movq	%r13, %rdi
	movq	%rax, %r14
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	-64(%rbp), %rsi
	movq	-72(%rbp), %rdi
	movl	%r15d, %r9d
	movabsq	$4611686018427387904, %rdx
	movq	%rax, %rcx
	movq	%r14, %r8
	subq	%r12, %rsi
	subq	%r12, %rdi
	call	DES_ncbc_encrypt@PLT
	movabsq	$-4611686018427387904, %rax
	addq	%rax, %r12
	cmpq	%rbx, %r12
	ja	.L80
	movq	-88(%rbp), %r12
	movq	-80(%rbp), %r14
	andq	%r12, %rbx
	addq	%rax, %r12
	shrq	$62, %r12
	addq	$1, %r12
	salq	$62, %r12
	addq	%r12, -56(%rbp)
	addq	%r12, %r14
.L81:
	testq	%rbx, %rbx
	jne	.L90
.L82:
	addq	$56, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L79:
	.cfi_restore_state
	movq	%r13, %rdi
	movq	%rax, %r15
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movq	-56(%rbp), %rdi
	movq	%r15, %rcx
	movq	%r12, %rdx
	movq	%rax, %r8
	movq	%r14, %rsi
	call	*%rbx
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L90:
	movq	%r13, %rdi
	call	EVP_CIPHER_CTX_encrypting@PLT
	movq	%r13, %rdi
	movl	%eax, %r15d
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movq	%r13, %rdi
	movq	%rax, %r12
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	-56(%rbp), %rdi
	movl	%r15d, %r9d
	movq	%r12, %r8
	movq	%rax, %rcx
	movq	%rbx, %rdx
	movq	%r14, %rsi
	call	DES_ncbc_encrypt@PLT
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L89:
	movq	%r12, %rbx
	jmp	.L81
	.cfi_endproc
.LFE447:
	.size	des_cbc_cipher, .-des_cbc_cipher
	.p2align 4
	.globl	EVP_des_cbc
	.type	EVP_des_cbc, @function
EVP_des_cbc:
.LFB451:
	.cfi_startproc
	endbr64
	leaq	des_cbc(%rip), %rax
	ret
	.cfi_endproc
.LFE451:
	.size	EVP_des_cbc, .-EVP_des_cbc
	.p2align 4
	.globl	EVP_des_cfb64
	.type	EVP_des_cfb64, @function
EVP_des_cfb64:
.LFB452:
	.cfi_startproc
	endbr64
	leaq	des_cfb64(%rip), %rax
	ret
	.cfi_endproc
.LFE452:
	.size	EVP_des_cfb64, .-EVP_des_cfb64
	.p2align 4
	.globl	EVP_des_ofb
	.type	EVP_des_ofb, @function
EVP_des_ofb:
.LFB453:
	.cfi_startproc
	endbr64
	leaq	des_ofb(%rip), %rax
	ret
	.cfi_endproc
.LFE453:
	.size	EVP_des_ofb, .-EVP_des_ofb
	.p2align 4
	.globl	EVP_des_ecb
	.type	EVP_des_ecb, @function
EVP_des_ecb:
.LFB454:
	.cfi_startproc
	endbr64
	leaq	des_ecb(%rip), %rax
	ret
	.cfi_endproc
.LFE454:
	.size	EVP_des_ecb, .-EVP_des_ecb
	.p2align 4
	.globl	EVP_des_cfb1
	.type	EVP_des_cfb1, @function
EVP_des_cfb1:
.LFB455:
	.cfi_startproc
	endbr64
	leaq	des_cfb1(%rip), %rax
	ret
	.cfi_endproc
.LFE455:
	.size	EVP_des_cfb1, .-EVP_des_cfb1
	.p2align 4
	.globl	EVP_des_cfb8
	.type	EVP_des_cfb8, @function
EVP_des_cfb8:
.LFB456:
	.cfi_startproc
	endbr64
	leaq	des_cfb8(%rip), %rax
	ret
	.cfi_endproc
.LFE456:
	.size	EVP_des_cfb8, .-EVP_des_cfb8
	.section	.data.rel.ro,"aw"
	.align 32
	.type	des_cfb8, @object
	.size	des_cfb8, 88
des_cfb8:
	.long	657
	.long	1
	.long	8
	.long	8
	.quad	515
	.quad	des_init_key
	.quad	des_cfb8_cipher
	.quad	0
	.long	136
	.zero	4
	.quad	EVP_CIPHER_set_asn1_iv
	.quad	EVP_CIPHER_get_asn1_iv
	.quad	des_ctrl
	.quad	0
	.align 32
	.type	des_cfb1, @object
	.size	des_cfb1, 88
des_cfb1:
	.long	656
	.long	1
	.long	8
	.long	8
	.quad	515
	.quad	des_init_key
	.quad	des_cfb1_cipher
	.quad	0
	.long	136
	.zero	4
	.quad	EVP_CIPHER_set_asn1_iv
	.quad	EVP_CIPHER_get_asn1_iv
	.quad	des_ctrl
	.quad	0
	.align 32
	.type	des_ecb, @object
	.size	des_ecb, 88
des_ecb:
	.long	29
	.long	8
	.long	8
	.long	0
	.quad	513
	.quad	des_init_key
	.quad	des_ecb_cipher
	.quad	0
	.long	136
	.zero	4
	.quad	EVP_CIPHER_set_asn1_iv
	.quad	EVP_CIPHER_get_asn1_iv
	.quad	des_ctrl
	.quad	0
	.align 32
	.type	des_ofb, @object
	.size	des_ofb, 88
des_ofb:
	.long	45
	.long	1
	.long	8
	.long	8
	.quad	516
	.quad	des_init_key
	.quad	des_ofb_cipher
	.quad	0
	.long	136
	.zero	4
	.quad	EVP_CIPHER_set_asn1_iv
	.quad	EVP_CIPHER_get_asn1_iv
	.quad	des_ctrl
	.quad	0
	.align 32
	.type	des_cfb64, @object
	.size	des_cfb64, 88
des_cfb64:
	.long	30
	.long	1
	.long	8
	.long	8
	.quad	515
	.quad	des_init_key
	.quad	des_cfb64_cipher
	.quad	0
	.long	136
	.zero	4
	.quad	EVP_CIPHER_set_asn1_iv
	.quad	EVP_CIPHER_get_asn1_iv
	.quad	des_ctrl
	.quad	0
	.align 32
	.type	des_cbc, @object
	.size	des_cbc, 88
des_cbc:
	.long	31
	.long	8
	.long	8
	.long	8
	.quad	514
	.quad	des_init_key
	.quad	des_cbc_cipher
	.quad	0
	.long	136
	.zero	4
	.quad	EVP_CIPHER_set_asn1_iv
	.quad	EVP_CIPHER_get_asn1_iv
	.quad	des_ctrl
	.quad	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
