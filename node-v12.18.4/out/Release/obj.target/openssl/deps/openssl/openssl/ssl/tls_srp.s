	.file	"tls_srp.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/ssl/tls_srp.c"
	.text
	.p2align 4
	.globl	SSL_CTX_SRP_CTX_free
	.type	SSL_CTX_SRP_CTX_free, @function
SSL_CTX_SRP_CTX_free:
.LFB1036:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L5
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$26, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	776(%rdi), %rdi
	call	CRYPTO_free@PLT
	movq	848(%rbx), %rdi
	movl	$27, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	784(%rbx), %rdi
	call	BN_free@PLT
	movq	792(%rbx), %rdi
	call	BN_free@PLT
	movq	800(%rbx), %rdi
	call	BN_free@PLT
	movq	808(%rbx), %rdi
	call	BN_free@PLT
	movq	816(%rbx), %rdi
	call	BN_free@PLT
	movq	824(%rbx), %rdi
	call	BN_free@PLT
	movq	832(%rbx), %rdi
	call	BN_free@PLT
	movq	840(%rbx), %rdi
	call	BN_free@PLT
	leaq	752(%rbx), %rdi
	movl	%ebx, %eax
	movq	$0, 744(%rbx)
	movq	$0, 864(%rbx)
	andq	$-8, %rdi
	subl	%edi, %eax
	leal	872(%rax), %ecx
	xorl	%eax, %eax
	shrl	$3, %ecx
	rep stosq
	movl	$1, %eax
	movl	$1024, 856(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	.cfi_restore 3
	.cfi_restore 6
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1036:
	.size	SSL_CTX_SRP_CTX_free, .-SSL_CTX_SRP_CTX_free
	.p2align 4
	.globl	SSL_SRP_CTX_free
	.type	SSL_SRP_CTX_free, @function
SSL_SRP_CTX_free:
.LFB1037:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L15
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$45, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	2008(%rdi), %rdi
	call	CRYPTO_free@PLT
	movq	2080(%rbx), %rdi
	movl	$46, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	2016(%rbx), %rdi
	call	BN_free@PLT
	movq	2024(%rbx), %rdi
	call	BN_free@PLT
	movq	2032(%rbx), %rdi
	call	BN_free@PLT
	movq	2040(%rbx), %rdi
	call	BN_free@PLT
	movq	2048(%rbx), %rdi
	call	BN_free@PLT
	movq	2056(%rbx), %rdi
	call	BN_free@PLT
	movq	2064(%rbx), %rdi
	call	BN_free@PLT
	movq	2072(%rbx), %rdi
	call	BN_free@PLT
	leaq	1984(%rbx), %rdi
	movl	%ebx, %eax
	movq	$0, 1976(%rbx)
	movq	$0, 2096(%rbx)
	andq	$-8, %rdi
	subl	%edi, %eax
	leal	2104(%rax), %ecx
	xorl	%eax, %eax
	shrl	$3, %ecx
	rep stosq
	movl	$1, %eax
	movl	$1024, 2088(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	.cfi_restore 3
	.cfi_restore 6
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1037:
	.size	SSL_SRP_CTX_free, .-SSL_SRP_CTX_free
	.p2align 4
	.globl	SSL_SRP_CTX_init
	.type	SSL_SRP_CTX_init, @function
SSL_SRP_CTX_init:
.LFB1038:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L42
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	1440(%rdi), %r12
	testq	%r12, %r12
	je	.L20
	movq	$0, 1976(%rdi)
	leaq	1976(%rdi), %r13
	leaq	1984(%rdi), %rdi
	xorl	%eax, %eax
	movq	$0, 112(%rdi)
	movq	%r13, %rcx
	andq	$-8, %rdi
	subq	%rdi, %rcx
	subl	$-128, %ecx
	shrl	$3, %ecx
	rep stosq
	movq	744(%r12), %rax
	movq	%rax, 1976(%rbx)
	movdqu	752(%r12), %xmm0
	movups	%xmm0, 1984(%rbx)
	movq	768(%r12), %rax
	movq	784(%r12), %rdi
	movq	%rax, 2000(%rbx)
	movl	856(%r12), %eax
	movl	%eax, 2088(%rbx)
	testq	%rdi, %rdi
	je	.L25
	call	BN_dup@PLT
	movq	%rax, 2016(%rbx)
	testq	%rax, %rax
	je	.L29
.L25:
	movq	792(%r12), %rdi
	testq	%rdi, %rdi
	je	.L24
	call	BN_dup@PLT
	movq	%rax, 2024(%rbx)
	testq	%rax, %rax
	je	.L29
.L24:
	movq	800(%r12), %rdi
	testq	%rdi, %rdi
	je	.L28
	call	BN_dup@PLT
	movq	%rax, 2032(%rbx)
	testq	%rax, %rax
	je	.L29
.L28:
	movq	808(%r12), %rdi
	testq	%rdi, %rdi
	je	.L31
	call	BN_dup@PLT
	movq	%rax, 2040(%rbx)
	testq	%rax, %rax
	je	.L29
.L31:
	movq	816(%r12), %rdi
	testq	%rdi, %rdi
	je	.L33
	call	BN_dup@PLT
	movq	%rax, 2048(%rbx)
	testq	%rax, %rax
	je	.L29
.L33:
	movq	824(%r12), %rdi
	testq	%rdi, %rdi
	je	.L35
	call	BN_dup@PLT
	movq	%rax, 2056(%rbx)
	testq	%rax, %rax
	je	.L29
.L35:
	movq	840(%r12), %rdi
	testq	%rdi, %rdi
	je	.L37
	call	BN_dup@PLT
	movq	%rax, 2072(%rbx)
	testq	%rax, %rax
	je	.L29
.L37:
	movq	832(%r12), %rdi
	testq	%rdi, %rdi
	je	.L39
	call	BN_dup@PLT
	movq	%rax, 2064(%rbx)
	testq	%rax, %rax
	je	.L29
.L39:
	movq	776(%r12), %rdi
	testq	%rdi, %rdi
	je	.L40
	movl	$102, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_strdup@PLT
	movl	$103, %r8d
	movq	%rax, 2008(%rbx)
	testq	%rax, %rax
	je	.L76
.L40:
	movq	848(%r12), %rdi
	testq	%rdi, %rdi
	je	.L41
	movl	$107, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_strdup@PLT
	movq	%rax, 2080(%rbx)
	testq	%rax, %rax
	je	.L77
.L41:
	movq	864(%r12), %rax
	movq	%rax, 2096(%rbx)
	movl	$1, %eax
.L20:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	.cfi_restore_state
	movl	$98, %r8d
	movl	$3, %edx
	movl	$313, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L26:
	movq	2008(%rbx), %rdi
	movl	$115, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	2080(%rbx), %rdi
	movl	$116, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	2016(%rbx), %rdi
	call	BN_free@PLT
	movq	2024(%rbx), %rdi
	call	BN_free@PLT
	movq	2032(%rbx), %rdi
	call	BN_free@PLT
	movq	2040(%rbx), %rdi
	call	BN_free@PLT
	movq	2048(%rbx), %rdi
	call	BN_free@PLT
	movq	2056(%rbx), %rdi
	call	BN_free@PLT
	movq	2064(%rbx), %rdi
	call	BN_free@PLT
	movq	2072(%rbx), %rdi
	call	BN_free@PLT
	leaq	8(%r13), %rdi
	xorl	%eax, %eax
	movq	$0, 1976(%rbx)
	andq	$-8, %rdi
	movq	$0, 120(%r13)
	subq	%rdi, %r13
	leal	128(%r13), %ecx
	shrl	$3, %ecx
	rep stosq
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L42:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L77:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movl	$108, %r8d
.L76:
	movl	$68, %edx
	movl	$313, %esi
	movl	$20, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L26
	.cfi_endproc
.LFE1038:
	.size	SSL_SRP_CTX_init, .-SSL_SRP_CTX_init
	.p2align 4
	.globl	SSL_CTX_SRP_CTX_init
	.type	SSL_CTX_SRP_CTX_init, @function
SSL_CTX_SRP_CTX_init:
.LFB1039:
	.cfi_startproc
	endbr64
	movq	%rdi, %rdx
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L78
	movq	$0, 744(%rdi)
	leaq	752(%rdi), %rdi
	movl	%edx, %eax
	movq	$0, 112(%rdi)
	andq	$-8, %rdi
	subl	%edi, %eax
	leal	872(%rax), %ecx
	xorl	%eax, %eax
	shrl	$3, %ecx
	rep stosq
	movl	$1, %eax
	movl	$1024, 856(%rdx)
.L78:
	ret
	.cfi_endproc
.LFE1039:
	.size	SSL_CTX_SRP_CTX_init, .-SSL_CTX_SRP_CTX_init
	.p2align 4
	.globl	SSL_srp_server_param_with_username
	.type	SSL_srp_server_param_with_username, @function
SSL_srp_server_param_with_username:
.LFB1040:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$64, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	1984(%rdi), %rax
	movl	$115, (%rsi)
	testq	%rax, %rax
	je	.L87
	movq	1976(%rdi), %rdx
	call	*%rax
	testl	%eax, %eax
	jne	.L82
.L87:
	cmpq	$0, 2016(%rbx)
	movl	$80, (%r12)
	je	.L84
	cmpq	$0, 2024(%rbx)
	je	.L84
	cmpq	$0, 2032(%rbx)
	je	.L84
	cmpq	$0, 2072(%rbx)
	je	.L84
	leaq	-80(%rbp), %r12
	movl	$48, %esi
	movq	%r12, %rdi
	call	RAND_priv_bytes@PLT
	testl	%eax, %eax
	jle	.L84
	xorl	%edx, %edx
	movl	$48, %esi
	movq	%r12, %rdi
	call	BN_bin2bn@PLT
	movl	$48, %esi
	movq	%r12, %rdi
	movq	%rax, 2064(%rbx)
	call	OPENSSL_cleanse@PLT
	movq	2024(%rbx), %rdx
	movq	2072(%rbx), %rcx
	movq	2016(%rbx), %rsi
	movq	2064(%rbx), %rdi
	call	SRP_Calc_B@PLT
	movq	%rax, %rdx
	movq	%rax, 2040(%rbx)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L84
.L82:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L92
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L84:
	.cfi_restore_state
	movl	$2, %eax
	jmp	.L82
.L92:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1040:
	.size	SSL_srp_server_param_with_username, .-SSL_srp_server_param_with_username
	.p2align 4
	.globl	SSL_set_srp_server_param_pw
	.type	SSL_set_srp_server_param_pw, @function
SSL_set_srp_server_param_pw:
.LFB1041:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%rcx, %rdi
	call	SRP_get_default_gN@PLT
	testq	%rax, %rax
	je	.L94
	movq	16(%rax), %rdi
	movq	%rax, %r12
	call	BN_dup@PLT
	movq	8(%r12), %rdi
	movq	%rax, 2016(%rbx)
	call	BN_dup@PLT
	movq	2072(%rbx), %rdi
	movq	%rax, 2024(%rbx)
	call	BN_clear_free@PLT
	movq	2032(%rbx), %rdi
	movq	$0, 2072(%rbx)
	call	BN_clear_free@PLT
	movq	16(%r12), %r8
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	$0, 2032(%rbx)
	movq	8(%r12), %r9
	leaq	2072(%rbx), %rcx
	leaq	2032(%rbx), %rdx
	call	SRP_create_verifier_BN@PLT
	movl	%eax, %r8d
	movl	$1, %eax
	testl	%r8d, %r8d
	je	.L94
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L94:
	.cfi_restore_state
	popq	%rbx
	movl	$-1, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1041:
	.size	SSL_set_srp_server_param_pw, .-SSL_set_srp_server_param_pw
	.p2align 4
	.globl	SSL_set_srp_server_param
	.type	SSL_set_srp_server_param, @function
SSL_set_srp_server_param:
.LFB1042:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r9, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	testq	%rsi, %rsi
	je	.L103
	movq	2016(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L104
	call	BN_copy@PLT
	testq	%rax, %rax
	je	.L133
.L103:
	testq	%r15, %r15
	je	.L107
	movq	2024(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L108
	movq	%r15, %rsi
	call	BN_copy@PLT
	testq	%rax, %rax
	je	.L134
.L107:
	testq	%r14, %r14
	je	.L111
	movq	2032(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L112
	movq	%r14, %rsi
	call	BN_copy@PLT
	testq	%rax, %rax
	je	.L135
.L111:
	testq	%r13, %r13
	je	.L115
	movq	2072(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L116
	movq	%r13, %rsi
	call	BN_copy@PLT
	testq	%rax, %rax
	je	.L136
.L115:
	testq	%r12, %r12
	je	.L122
	movq	2080(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L121
	movl	$237, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
.L121:
	movl	$238, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_strdup@PLT
	movq	%rax, 2080(%rbx)
	testq	%rax, %rax
	je	.L119
.L122:
	cmpq	$0, 2016(%rbx)
	je	.L119
	cmpq	$0, 2024(%rbx)
	je	.L119
	cmpq	$0, 2032(%rbx)
	je	.L119
	cmpq	$0, 2072(%rbx)
	movl	$1, %eax
	jne	.L101
	.p2align 4,,10
	.p2align 3
.L119:
	movl	$-1, %eax
.L101:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L104:
	.cfi_restore_state
	movq	%rsi, %rdi
	call	BN_dup@PLT
	movq	%rax, 2016(%rbx)
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L116:
	movq	%r13, %rdi
	call	BN_dup@PLT
	movq	%rax, 2072(%rbx)
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L112:
	movq	%r14, %rdi
	call	BN_dup@PLT
	movq	%rax, 2032(%rbx)
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L108:
	movq	%r15, %rdi
	call	BN_dup@PLT
	movq	%rax, 2024(%rbx)
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L133:
	movq	2016(%rbx), %rdi
	call	BN_free@PLT
	movq	$0, 2016(%rbx)
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L134:
	movq	2024(%rbx), %rdi
	call	BN_free@PLT
	movq	$0, 2024(%rbx)
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L135:
	movq	2032(%rbx), %rdi
	call	BN_free@PLT
	movq	$0, 2032(%rbx)
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L136:
	movq	2072(%rbx), %rdi
	call	BN_free@PLT
	movq	$0, 2072(%rbx)
	jmp	.L115
	.cfi_endproc
.LFE1042:
	.size	SSL_set_srp_server_param, .-SSL_set_srp_server_param
	.p2align 4
	.globl	srp_generate_server_master_secret
	.type	srp_generate_server_master_secret, @function
srp_generate_server_master_secret:
.LFB1043:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movq	2016(%rdi), %rsi
	movq	2048(%rdi), %rdi
	call	SRP_Verify_A_mod_N@PLT
	testl	%eax, %eax
	je	.L140
	movq	2016(%r12), %rdx
	movq	2040(%r12), %rsi
	movq	2048(%r12), %rdi
	call	SRP_Calc_u@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L140
	movq	2064(%r12), %rcx
	movq	2072(%r12), %rsi
	movq	%rax, %rdx
	movq	2048(%r12), %rdi
	movq	2016(%r12), %r8
	call	SRP_Calc_server_key@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L142
	movq	%rax, %rdi
	call	BN_num_bits@PLT
	movl	$264, %edx
	leaq	.LC0(%rip), %rsi
	leal	14(%rax), %r13d
	addl	$7, %eax
	cmovns	%eax, %r13d
	sarl	$3, %r13d
	movslq	%r13d, %r13
	movq	%r13, %rdi
	call	CRYPTO_malloc@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L147
	movq	%rax, %rsi
	movq	%r15, %rdi
	call	BN_bn2bin@PLT
	movq	%r12, %rdi
	movl	$1, %ecx
	movq	%r13, %rdx
	movq	%rbx, %rsi
	call	ssl_generate_master_secret@PLT
	movl	%eax, %r12d
.L139:
	movq	%r15, %rdi
	call	BN_clear_free@PLT
	movq	%r14, %rdi
	call	BN_clear_free@PLT
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L140:
	.cfi_restore_state
	movl	$-1, %r12d
	xorl	%r14d, %r14d
	xorl	%r15d, %r15d
	jmp	.L139
.L142:
	movl	$-1, %r12d
	jmp	.L139
.L147:
	movq	%r12, %rdi
	movl	$265, %r9d
	movl	$65, %ecx
	movl	$589, %edx
	leaq	.LC0(%rip), %r8
	movl	$80, %esi
	movl	$-1, %r12d
	call	ossl_statem_fatal@PLT
	jmp	.L139
	.cfi_endproc
.LFE1043:
	.size	srp_generate_server_master_secret, .-srp_generate_server_master_secret
	.p2align 4
	.globl	srp_generate_client_master_secret
	.type	srp_generate_client_master_secret, @function
srp_generate_client_master_secret:
.LFB1044:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	2016(%rdi), %rsi
	movq	2040(%rdi), %rdi
	call	SRP_Verify_B_mod_N@PLT
	testl	%eax, %eax
	je	.L151
	movq	2016(%r12), %rdx
	movq	2040(%r12), %rsi
	movq	2048(%r12), %rdi
	call	SRP_Calc_u@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L151
	movq	2000(%r12), %rax
	testq	%rax, %rax
	je	.L150
	movq	1976(%r12), %rsi
	movq	%r12, %rdi
	call	*%rax
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L168
	movq	2008(%r12), %rsi
	movq	2032(%r12), %rdi
	movq	%rax, %rdx
	call	SRP_Calc_x@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L157
	movq	2024(%r12), %rdx
	movq	%r15, %r9
	movq	%rax, %rcx
	movq	2040(%r12), %rsi
	movq	2016(%r12), %rdi
	movq	2056(%r12), %r8
	call	SRP_Calc_client_key@PLT
	testq	%rax, %rax
	je	.L157
	movq	%rax, %rdi
	movq	%rax, -56(%rbp)
	call	BN_num_bits@PLT
	leaq	.LC0(%rip), %rsi
	movl	$315, %edx
	leal	14(%rax), %r13d
	addl	$7, %eax
	cmovns	%eax, %r13d
	sarl	$3, %r13d
	movslq	%r13d, %r13
	movq	%r13, %rdi
	call	CRYPTO_malloc@PLT
	movq	-56(%rbp), %r10
	testq	%rax, %rax
	movq	%rax, %rsi
	je	.L169
	movq	%r10, %rdi
	movq	%r10, -56(%rbp)
	movq	%rax, -64(%rbp)
	call	BN_bn2bin@PLT
	movq	-64(%rbp), %rsi
	movq	%r12, %rdi
	movq	%r13, %rdx
	movl	$1, %ecx
	call	ssl_generate_master_secret@PLT
	movq	-56(%rbp), %r10
	movl	%eax, %r12d
.L156:
	movq	%r10, %rdi
	call	BN_clear_free@PLT
	movq	%rbx, %rdi
	call	BN_clear_free@PLT
	movq	%r14, %rdi
	call	strlen@PLT
	movl	$327, %ecx
	leaq	.LC0(%rip), %rdx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	CRYPTO_clear_free@PLT
.L159:
	movq	%r15, %rdi
	call	BN_clear_free@PLT
	addq	$24, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L151:
	.cfi_restore_state
	xorl	%r15d, %r15d
.L150:
	movl	$293, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movq	%r12, %rdi
	movl	$595, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
.L153:
	xorl	%edi, %edi
	movl	$-1, %r12d
	call	BN_clear_free@PLT
	xorl	%edi, %edi
	call	BN_clear_free@PLT
	jmp	.L159
	.p2align 4,,10
	.p2align 3
.L157:
	movq	%r12, %rdi
	movl	$309, %r9d
	movl	$68, %ecx
	movl	$595, %edx
	leaq	.LC0(%rip), %r8
	movl	$80, %esi
	movl	$-1, %r12d
	call	ossl_statem_fatal@PLT
	xorl	%r10d, %r10d
	jmp	.L156
.L168:
	movl	$300, %r9d
	leaq	.LC0(%rip), %r8
	movl	$234, %ecx
	movq	%r12, %rdi
	movl	$595, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L153
.L169:
	movq	%r12, %rdi
	movl	$316, %r9d
	movl	$65, %ecx
	movl	$595, %edx
	leaq	.LC0(%rip), %r8
	movl	$80, %esi
	movl	$-1, %r12d
	call	ossl_statem_fatal@PLT
	movq	-56(%rbp), %r10
	jmp	.L156
	.cfi_endproc
.LFE1044:
	.size	srp_generate_client_master_secret, .-srp_generate_client_master_secret
	.p2align 4
	.globl	srp_verify_server_param
	.type	srp_verify_server_param, @function
srp_verify_server_param:
.LFB1045:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	2016(%rdi), %rsi
	movq	2024(%rdi), %rdi
	call	BN_ucmp@PLT
	testl	%eax, %eax
	jns	.L173
	movq	2016(%r12), %rsi
	movq	2040(%r12), %rdi
	call	BN_ucmp@PLT
	testl	%eax, %eax
	js	.L180
.L173:
	movl	$390, %ecx
	movl	$596, %edx
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	movl	$341, %r9d
	leaq	.LC0(%rip), %r8
	movl	$47, %esi
	call	ossl_statem_fatal@PLT
.L170:
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L180:
	.cfi_restore_state
	movq	2040(%r12), %rdi
	call	BN_is_zero@PLT
	movl	%eax, %r13d
	testl	%eax, %eax
	jne	.L173
	movq	2016(%r12), %rdi
	call	BN_num_bits@PLT
	movl	$347, %r9d
	cmpl	2088(%r12), %eax
	jl	.L179
	movq	1992(%r12), %rax
	testq	%rax, %rax
	je	.L175
	movq	1976(%r12), %rsi
	movq	%r12, %rdi
	call	*%rax
	testl	%eax, %eax
	jle	.L176
.L177:
	movl	$1, %r13d
	jmp	.L170
	.p2align 4,,10
	.p2align 3
.L175:
	movq	2016(%r12), %rsi
	movq	2024(%r12), %rdi
	call	SRP_check_known_gN_param@PLT
	testq	%rax, %rax
	jne	.L177
	movl	$360, %r9d
	.p2align 4,,10
	.p2align 3
.L179:
	leaq	.LC0(%rip), %r8
	movl	$241, %ecx
	movl	$596, %edx
	movq	%r12, %rdi
	movl	$71, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L170
	.p2align 4,,10
	.p2align 3
.L176:
	movl	$354, %r9d
	leaq	.LC0(%rip), %r8
	movl	$234, %ecx
	movq	%r12, %rdi
	movl	$596, %edx
	movl	$71, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L170
	.cfi_endproc
.LFE1045:
	.size	srp_verify_server_param, .-srp_verify_server_param
	.p2align 4
	.globl	SRP_Calc_A_param
	.type	SRP_Calc_A_param, @function
SRP_Calc_A_param:
.LFB1046:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$48, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	leaq	-80(%rbp), %r12
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$64, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	RAND_priv_bytes@PLT
	movl	%eax, %r8d
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jle	.L181
	movq	2056(%rbx), %rdx
	movl	$48, %esi
	movq	%r12, %rdi
	call	BN_bin2bn@PLT
	movl	$48, %esi
	movq	%r12, %rdi
	movq	%rax, 2056(%rbx)
	call	OPENSSL_cleanse@PLT
	movq	2024(%rbx), %rdx
	movq	2016(%rbx), %rsi
	movq	2056(%rbx), %rdi
	call	SRP_Calc_A@PLT
	testq	%rax, %rax
	movq	%rax, 2048(%rbx)
	setne	%al
	movzbl	%al, %eax
.L181:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L187
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L187:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1046:
	.size	SRP_Calc_A_param, .-SRP_Calc_A_param
	.p2align 4
	.globl	SSL_get_srp_g
	.type	SSL_get_srp_g, @function
SSL_get_srp_g:
.LFB1047:
	.cfi_startproc
	endbr64
	movq	2024(%rdi), %rax
	testq	%rax, %rax
	je	.L190
	ret
	.p2align 4,,10
	.p2align 3
.L190:
	movq	1440(%rdi), %rax
	movq	792(%rax), %rax
	ret
	.cfi_endproc
.LFE1047:
	.size	SSL_get_srp_g, .-SSL_get_srp_g
	.p2align 4
	.globl	SSL_get_srp_N
	.type	SSL_get_srp_N, @function
SSL_get_srp_N:
.LFB1048:
	.cfi_startproc
	endbr64
	movq	2016(%rdi), %rax
	testq	%rax, %rax
	je	.L193
	ret
	.p2align 4,,10
	.p2align 3
.L193:
	movq	1440(%rdi), %rax
	movq	784(%rax), %rax
	ret
	.cfi_endproc
.LFE1048:
	.size	SSL_get_srp_N, .-SSL_get_srp_N
	.p2align 4
	.globl	SSL_get_srp_username
	.type	SSL_get_srp_username, @function
SSL_get_srp_username:
.LFB1049:
	.cfi_startproc
	endbr64
	movq	2008(%rdi), %rax
	testq	%rax, %rax
	je	.L196
	ret
	.p2align 4,,10
	.p2align 3
.L196:
	movq	1440(%rdi), %rax
	movq	776(%rax), %rax
	ret
	.cfi_endproc
.LFE1049:
	.size	SSL_get_srp_username, .-SSL_get_srp_username
	.p2align 4
	.globl	SSL_get_srp_userinfo
	.type	SSL_get_srp_userinfo, @function
SSL_get_srp_userinfo:
.LFB1050:
	.cfi_startproc
	endbr64
	movq	2080(%rdi), %rax
	testq	%rax, %rax
	je	.L199
	ret
	.p2align 4,,10
	.p2align 3
.L199:
	movq	1440(%rdi), %rax
	movq	848(%rax), %rax
	ret
	.cfi_endproc
.LFE1050:
	.size	SSL_get_srp_userinfo, .-SSL_get_srp_userinfo
	.p2align 4
	.globl	SSL_CTX_set_srp_username
	.type	SSL_CTX_set_srp_username, @function
SSL_CTX_set_srp_username:
.LFB1051:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movl	$79, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ssl3_ctx_ctrl@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1051:
	.size	SSL_CTX_set_srp_username, .-SSL_CTX_set_srp_username
	.p2align 4
	.globl	SSL_CTX_set_srp_password
	.type	SSL_CTX_set_srp_password, @function
SSL_CTX_set_srp_password:
.LFB1052:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movl	$81, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ssl3_ctx_ctrl@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1052:
	.size	SSL_CTX_set_srp_password, .-SSL_CTX_set_srp_password
	.p2align 4
	.globl	SSL_CTX_set_srp_strength
	.type	SSL_CTX_set_srp_strength, @function
SSL_CTX_set_srp_strength:
.LFB1053:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%esi, %rdx
	xorl	%ecx, %ecx
	movl	$80, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ssl3_ctx_ctrl@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1053:
	.size	SSL_CTX_set_srp_strength, .-SSL_CTX_set_srp_strength
	.p2align 4
	.globl	SSL_CTX_set_srp_verify_param_callback
	.type	SSL_CTX_set_srp_verify_param_callback, @function
SSL_CTX_set_srp_verify_param_callback:
.LFB1054:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdx
	movl	$76, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ssl3_ctx_callback_ctrl@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1054:
	.size	SSL_CTX_set_srp_verify_param_callback, .-SSL_CTX_set_srp_verify_param_callback
	.p2align 4
	.globl	SSL_CTX_set_srp_cb_arg
	.type	SSL_CTX_set_srp_cb_arg, @function
SSL_CTX_set_srp_cb_arg:
.LFB1055:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movl	$78, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ssl3_ctx_ctrl@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1055:
	.size	SSL_CTX_set_srp_cb_arg, .-SSL_CTX_set_srp_cb_arg
	.p2align 4
	.globl	SSL_CTX_set_srp_username_callback
	.type	SSL_CTX_set_srp_username_callback, @function
SSL_CTX_set_srp_username_callback:
.LFB1056:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdx
	movl	$75, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ssl3_ctx_callback_ctrl@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1056:
	.size	SSL_CTX_set_srp_username_callback, .-SSL_CTX_set_srp_username_callback
	.p2align 4
	.globl	SSL_CTX_set_srp_client_pwd_callback
	.type	SSL_CTX_set_srp_client_pwd_callback, @function
SSL_CTX_set_srp_client_pwd_callback:
.LFB1057:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdx
	movl	$77, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ssl3_ctx_callback_ctrl@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1057:
	.size	SSL_CTX_set_srp_client_pwd_callback, .-SSL_CTX_set_srp_client_pwd_callback
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
