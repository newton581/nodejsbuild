	.file	"seed.c"
	.text
	.p2align 4
	.globl	SEED_set_key
	.type	SEED_set_key, @function
SEED_set_key:
.LFB151:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	4(%rdi), %r8d
	movl	(%rdi), %edx
	movl	8(%rdi), %r9d
	movl	12(%rdi), %edi
	bswap	%r8d
	leal	-1640531527(%r8), %eax
	bswap	%edx
	movl	%r8d, %r12d
	bswap	%edi
	subl	%edi, %eax
	bswap	%r9d
	shrl	$8, %r8d
	leal	1640531527(%rdx,%r9), %ebx
	movl	%eax, -60(%rbp)
	movl	%edx, %eax
	sall	$24, %edx
	xorl	%r8d, %edx
	shrl	$8, %eax
	movl	%edi, %r10d
	movl	%ebx, -56(%rbp)
	sall	$24, %r12d
	shrl	$24, %r10d
	movl	%edx, %r13d
	xorl	%eax, %r12d
	movl	%edx, %eax
	sall	$24, %r13d
	subl	%edi, %eax
	leal	-1013904243(%r9,%r12), %esi
	sall	$8, %edi
	leal	1013904243(%rax), %ecx
	movl	%r9d, %eax
	shrl	$24, %r9d
	movl	%esi, -64(%rbp)
	sall	$8, %eax
	xorl	%edi, %r9d
	movl	%ecx, -68(%rbp)
	xorl	%eax, %r10d
	leal	2027808486(%rdx), %eax
	shrl	$8, %edx
	movl	%eax, %esi
	leal	-2027808486(%r12,%r10), %r14d
	movl	%r12d, %eax
	sall	$24, %r12d
	subl	%r9d, %esi
	shrl	$8, %eax
	movl	%r14d, -72(%rbp)
	movl	%esi, -76(%rbp)
	movl	%r12d, %esi
	xorl	%eax, %r13d
	movl	%r9d, %r12d
	xorl	%edx, %esi
	shrl	$24, %r12d
	leal	239350324(%r10,%r13), %edx
	movl	%esi, %eax
	movl	%edx, -80(%rbp)
	leal	-478700647(%rsi), %edx
	subl	%r9d, %eax
	sall	$8, %r9d
	leal	-239350324(%rax), %r11d
	movl	%r10d, %eax
	shrl	$24, %r10d
	sall	$8, %eax
	movl	%r11d, -84(%rbp)
	xorl	%eax, %r12d
	movl	%r10d, %eax
	movl	%r13d, %r10d
	xorl	%r9d, %eax
	shrl	$8, %r10d
	leal	478700647(%r13,%r12), %r8d
	subl	%eax, %edx
	movl	%r8d, -88(%rbp)
	movl	%edx, -92(%rbp)
	movl	%r10d, %edx
	movl	%esi, %r10d
	sall	$24, %r10d
	xorl	%edx, %r10d
	shrl	$8, %esi
	sall	$24, %r13d
	movl	%r13d, %ecx
	leal	957401293(%r12,%r10), %r13d
	xorl	%esi, %ecx
	movl	%r13d, -96(%rbp)
	movl	%eax, %r13d
	movl	%ecx, %edx
	shrl	$24, %r13d
	movl	%ecx, %edi
	subl	%eax, %edx
	sall	$8, %eax
	leal	-957401293(%rdx), %r11d
	movl	%r12d, %edx
	shrl	$24, %r12d
	sall	$8, %edx
	movl	%r12d, %ebx
	sall	$24, %edi
	movl	%r11d, -100(%rbp)
	xorl	%edx, %r13d
	xorl	%eax, %ebx
	leal	-1914802585(%rcx), %eax
	shrl	$8, %ecx
	leal	1914802585(%r10,%r13), %r12d
	movl	%eax, %r9d
	movl	%r10d, %eax
	movl	%r12d, -104(%rbp)
	shrl	$8, %eax
	movl	%edi, %r12d
	movl	%ebx, %edi
	xorl	%eax, %r12d
	movl	%r10d, %eax
	shrl	$24, %edi
	subl	%ebx, %r9d
	sall	$24, %eax
	movl	%edi, %r10d
	movl	%r12d, %edi
	movl	%r9d, -108(%rbp)
	movl	%eax, %r11d
	leal	-465362127(%r13,%r12), %eax
	shrl	$8, %edi
	xorl	%ecx, %r11d
	movl	%eax, -112(%rbp)
	movl	%r11d, %eax
	subl	%ebx, %eax
	sall	$8, %ebx
	leal	465362127(%rax), %ecx
	movl	%r13d, %eax
	shrl	$24, %r13d
	sall	$8, %eax
	movl	%r13d, %r8d
	movl	%ecx, -116(%rbp)
	xorl	%eax, %r10d
	xorl	%ebx, %r8d
	leal	930724254(%r11), %eax
	subl	%r8d, %eax
	leal	-930724254(%r12,%r10), %r13d
	movl	%eax, -124(%rbp)
	movl	%edi, %eax
	movl	%r11d, %edi
	shrl	$8, %r11d
	sall	$24, %edi
	movl	%r13d, -120(%rbp)
	movl	%edi, %ebx
	movl	%r12d, %edi
	sall	$24, %edi
	xorl	%eax, %ebx
	xorl	%r11d, %edi
	leal	-1861448508(%r10,%rbx), %edx
	movl	%r8d, %r11d
	movl	%edi, %eax
	movl	%edx, -128(%rbp)
	movl	%r10d, %edx
	subl	%r8d, %eax
	leal	1861448508(%rax), %ecx
	movl	%r10d, %eax
	sall	$8, %eax
	shrl	$24, %r11d
	movl	%ecx, -132(%rbp)
	movl	%edi, %ecx
	sall	$8, %r8d
	xorl	%eax, %r11d
	shrl	$24, %edx
	leal	-572070280(%rdi), %eax
	xorl	%r8d, %edx
	movl	%eax, %r10d
	sall	$24, %ecx
	movl	%ebx, %eax
	shrl	$8, %eax
	subl	%edx, %r10d
	shrl	$8, %edi
	movl	%r11d, %esi
	movl	%r10d, -136(%rbp)
	movl	%ecx, %r10d
	movl	%edx, %ecx
	shrl	$24, %esi
	xorl	%eax, %r10d
	movl	%ebx, %eax
	shrl	$24, %ecx
	leal	572070280(%rbx,%r11), %r8d
	sall	$24, %eax
	movl	%ecx, %r12d
	movl	%eax, %r9d
	xorl	%edi, %r9d
	leal	1144140559(%r11,%r10), %edi
	movl	%r9d, %eax
	leal	2006686179(%r9), %ebx
	subl	%edx, %eax
	sall	$8, %edx
	subl	$1144140559, %eax
	xorl	%edx, %esi
	movl	%r9d, %edx
	shrl	$8, %r9d
	movl	%eax, -140(%rbp)
	movl	%r11d, %eax
	sall	$24, %edx
	movl	%esi, %r14d
	sall	$8, %eax
	shrl	$24, %r14d
	subl	%esi, %ebx
	xorl	%eax, %r12d
	movl	%r10d, %eax
	movl	%r14d, %r13d
	shrl	$8, %eax
	leal	-2006686179(%r10,%r12), %r11d
	xorl	%eax, %edx
	movl	%r10d, %eax
	sall	$24, %eax
	movl	%edx, %r14d
	leal	281594938(%r12,%rdx), %r10d
	xorl	%r9d, %eax
	movl	%eax, %ecx
	subl	%esi, %ecx
	leal	-281594938(%rcx), %r9d
	movl	%r12d, %ecx
	sall	$8, %ecx
	xorl	%ecx, %r13d
	sall	$8, %esi
	movl	%r12d, %ecx
	shrl	$8, %r14d
	leal	563189875(%rdx,%r13), %r12d
	shrl	$24, %ecx
	movl	%r14d, -52(%rbp)
	movl	%eax, %r14d
	sall	$24, %edx
	xorl	%esi, %ecx
	shrl	$8, %r14d
	leal	-563189875(%rax), %esi
	sall	$24, %eax
	xorl	-52(%rbp), %eax
	xorl	%r14d, %edx
	subl	%ecx, %esi
	leal	1126379749(%r13,%rax), %r13d
	subl	%ecx, %edx
	leaq	SS(%rip), %rax
	leal	-1126379749(%rdx), %ecx
	movzbl	%r12b, %edx
	movl	%ecx, -52(%rbp)
	movl	%r12d, %ecx
	movl	(%rax,%rdx,4), %edx
	shrl	$24, %ecx
	movslq	%ecx, %r14
	movl	%r12d, %ecx
	shrl	$16, %r12d
	movzbl	%ch, %ecx
	xorl	3072(%rax,%r14,4), %edx
	movzbl	%r12b, %r12d
	xorl	1024(%rax,%rcx,4), %edx
	movl	%esi, %ecx
	xorl	2048(%rax,%r12,4), %edx
	movzbl	%sil, %r12d
	shrl	$24, %ecx
	movl	(%rax,%r12,4), %r12d
	movd	%edx, %xmm3
	movzbl	%r11b, %edx
	movslq	%ecx, %r14
	movl	%esi, %ecx
	shrl	$16, %esi
	movl	(%rax,%rdx,4), %edx
	movzbl	%ch, %ecx
	xorl	3072(%rax,%r14,4), %r12d
	movzbl	%sil, %esi
	xorl	1024(%rax,%rcx,4), %r12d
	movl	%r13d, %ecx
	xorl	2048(%rax,%rsi,4), %r12d
	movzbl	%r13b, %esi
	shrl	$24, %ecx
	movl	(%rax,%rsi,4), %esi
	movslq	%ecx, %r14
	movl	%r13d, %ecx
	shrl	$16, %r13d
	movzbl	%ch, %ecx
	xorl	3072(%rax,%r14,4), %esi
	movzbl	%r13b, %r13d
	xorl	1024(%rax,%rcx,4), %esi
	movl	-52(%rbp), %ecx
	xorl	2048(%rax,%r13,4), %esi
	movzbl	%cl, %r13d
	shrl	$24, %ecx
	movd	%esi, %xmm0
	movl	%ebx, %esi
	movslq	%ecx, %r14
	movl	-52(%rbp), %ecx
	movl	(%rax,%r13,4), %r13d
	xorl	3072(%rax,%r14,4), %r13d
	movzbl	%ch, %ecx
	xorl	1024(%rax,%rcx,4), %r13d
	movl	-52(%rbp), %ecx
	shrl	$16, %ecx
	movzbl	%cl, %ecx
	xorl	2048(%rax,%rcx,4), %r13d
	movl	%r11d, %ecx
	shrl	$24, %ecx
	movd	%r13d, %xmm2
	xorl	3072(%rax,%rcx,4), %edx
	movl	%r11d, %ecx
	shrl	$24, %esi
	punpckldq	%xmm2, %xmm0
	movzbl	%ch, %ecx
	shrl	$16, %r11d
	movd	%r12d, %xmm2
	xorl	1024(%rax,%rcx,4), %edx
	movzbl	%bl, %ecx
	movzbl	%r11b, %r11d
	movl	(%rax,%rcx,4), %ecx
	xorl	3072(%rax,%rsi,4), %ecx
	movzbl	%bh, %esi
	shrl	$16, %ebx
	movzbl	%bl, %ebx
	xorl	2048(%rax,%r11,4), %edx
	punpckldq	%xmm2, %xmm3
	movl	%r10d, %r11d
	xorl	1024(%rax,%rsi,4), %ecx
	shrl	$24, %r11d
	movzbl	%r10b, %esi
	movd	%edx, %xmm4
	xorl	2048(%rax,%rbx,4), %ecx
	movl	%r10d, %ebx
	shrl	$16, %r10d
	movl	(%rax,%rsi,4), %esi
	movzbl	%bh, %ebx
	movd	%ecx, %xmm5
	movzbl	%r10b, %r10d
	movl	%r8d, %ecx
	shrl	$24, %ecx
	punpcklqdq	%xmm0, %xmm3
	punpckldq	%xmm5, %xmm4
	movzbl	%r8b, %edx
	xorl	3072(%rax,%r11,4), %esi
	movl	%r9d, %r11d
	movl	(%rax,%rdx,4), %edx
	xorl	1024(%rax,%rbx,4), %esi
	movl	%r9d, %ebx
	xorl	2048(%rax,%r10,4), %esi
	shrl	$24, %r11d
	movzbl	%r9b, %r10d
	movzbl	%bh, %ebx
	movd	%esi, %xmm0
	shrl	$16, %r9d
	xorl	3072(%rax,%rcx,4), %edx
	movl	(%rax,%r10,4), %r10d
	movl	%r8d, %ecx
	shrl	$16, %r8d
	xorl	3072(%rax,%r11,4), %r10d
	movzbl	%ch, %ecx
	movzbl	%r8b, %r8d
	movzbl	%r9b, %r9d
	xorl	1024(%rax,%rbx,4), %r10d
	movl	-136(%rbp), %ebx
	xorl	1024(%rax,%rcx,4), %edx
	xorl	2048(%rax,%r8,4), %edx
	movl	%ebx, %r8d
	movzbl	%bl, %ecx
	xorl	2048(%rax,%r9,4), %r10d
	shrl	$24, %r8d
	movl	(%rax,%rcx,4), %ecx
	movd	%r10d, %xmm2
	movd	%edx, %xmm5
	movslq	%r8d, %rsi
	movl	%edi, %r8d
	movl	-140(%rbp), %r10d
	punpckldq	%xmm2, %xmm0
	xorl	3072(%rax,%rsi,4), %ecx
	movzbl	%bh, %esi
	shrl	$24, %r8d
	punpcklqdq	%xmm0, %xmm4
	xorl	1024(%rax,%rsi,4), %ecx
	movl	%ebx, %esi
	movl	%edi, %ebx
	shrl	$16, %esi
	movzbl	%bh, %ebx
	movzbl	%sil, %esi
	xorl	2048(%rax,%rsi,4), %ecx
	movzbl	%dil, %esi
	movl	(%rax,%rsi,4), %esi
	xorl	3072(%rax,%r8,4), %esi
	movl	%r10d, %r8d
	movd	%ecx, %xmm7
	xorl	1024(%rax,%rbx,4), %esi
	shrl	$16, %edi
	movl	%r10d, %ebx
	punpckldq	%xmm7, %xmm5
	movzbl	%dil, %edi
	shrl	$24, %r8d
	movzbl	%bh, %ebx
	xorl	2048(%rax,%rdi,4), %esi
	movzbl	%r10b, %edi
	movl	(%rax,%rdi,4), %edi
	xorl	3072(%rax,%r8,4), %edi
	movl	%r10d, %r8d
	movd	%esi, %xmm0
	xorl	1024(%rax,%rbx,4), %edi
	movl	-120(%rbp), %ebx
	shrl	$16, %r8d
	movzbl	%r8b, %r8d
	movl	%ebx, %ecx
	xorl	2048(%rax,%r8,4), %edi
	movzbl	%bl, %edx
	shrl	$24, %ecx
	movd	%edi, %xmm6
	movl	(%rax,%rdx,4), %edx
	movl	%ebx, %edi
	xorl	3072(%rax,%rcx,4), %edx
	movzbl	%bh, %ecx
	movl	-124(%rbp), %ebx
	shrl	$16, %edi
	xorl	1024(%rax,%rcx,4), %edx
	movzbl	%dil, %ecx
	punpckldq	%xmm6, %xmm0
	movl	%ebx, %r10d
	xorl	2048(%rax,%rcx,4), %edx
	movzbl	%bl, %ecx
	movl	%ebx, %edi
	shrl	$24, %r10d
	movl	(%rax,%rcx,4), %ecx
	shrl	$16, %edi
	movd	%edx, %xmm6
	movslq	%r10d, %rsi
	punpcklqdq	%xmm0, %xmm5
	xorl	3072(%rax,%rsi,4), %ecx
	movzbl	%bh, %esi
	movl	-128(%rbp), %ebx
	xorl	1024(%rax,%rsi,4), %ecx
	movzbl	%dil, %esi
	movl	-132(%rbp), %r13d
	movl	%ebx, %edi
	xorl	2048(%rax,%rsi,4), %ecx
	movzbl	%bl, %esi
	movl	-116(%rbp), %r12d
	shrl	$24, %edi
	movl	(%rax,%rsi,4), %esi
	movl	%r13d, %r8d
	movd	%ecx, %xmm2
	xorl	3072(%rax,%rdi,4), %esi
	movzbl	%bh, %edi
	shrl	$24, %r8d
	punpckldq	%xmm2, %xmm6
	xorl	1024(%rax,%rdi,4), %esi
	movl	%ebx, %edi
	movl	%r13d, %ebx
	shrl	$16, %edi
	movzbl	%bh, %ebx
	movzbl	%dil, %edi
	xorl	2048(%rax,%rdi,4), %esi
	movzbl	%r13b, %edi
	shrl	$16, %r13d
	movl	(%rax,%rdi,4), %edi
	xorl	3072(%rax,%r8,4), %edi
	movzbl	%r13b, %r8d
	movd	%esi, %xmm0
	xorl	1024(%rax,%rbx,4), %edi
	movl	-104(%rbp), %ebx
	xorl	2048(%rax,%r8,4), %edi
	movl	%ebx, %ecx
	movzbl	%bl, %edx
	movd	%edi, %xmm1
	shrl	$24, %ecx
	movl	(%rax,%rdx,4), %edx
	punpckldq	%xmm1, %xmm0
	xorl	3072(%rax,%rcx,4), %edx
	movzbl	%bh, %ecx
	punpcklqdq	%xmm0, %xmm6
	xorl	1024(%rax,%rcx,4), %edx
	movl	%ebx, %ecx
	movl	-108(%rbp), %ebx
	shrl	$16, %ecx
	movzbl	%cl, %ecx
	movl	%ebx, %r11d
	movl	%ebx, %r13d
	xorl	2048(%rax,%rcx,4), %edx
	shrl	$24, %r11d
	movzbl	%bl, %ecx
	movslq	%r11d, %rsi
	movl	(%rax,%rcx,4), %ecx
	shrl	$16, %r13d
	xorl	3072(%rax,%rsi,4), %ecx
	movzbl	%bh, %esi
	movl	-112(%rbp), %ebx
	xorl	1024(%rax,%rsi,4), %ecx
	movzbl	%r13b, %esi
	movl	%r12d, %r13d
	movl	%ebx, %edi
	xorl	2048(%rax,%rsi,4), %ecx
	movzbl	%bl, %esi
	shrl	$24, %r13d
	shrl	$24, %edi
	movl	(%rax,%rsi,4), %esi
	movslq	%r13d, %r8
	movd	%ecx, %xmm1
	xorl	3072(%rax,%rdi,4), %esi
	movzbl	%bh, %edi
	xorl	1024(%rax,%rdi,4), %esi
	movl	%ebx, %edi
	movl	%r12d, %ebx
	shrl	$16, %edi
	movzbl	%bh, %ebx
	movzbl	%dil, %edi
	xorl	2048(%rax,%rdi,4), %esi
	movzbl	%r12b, %edi
	shrl	$16, %r12d
	movl	(%rax,%rdi,4), %edi
	xorl	3072(%rax,%r8,4), %edi
	movzbl	%r12b, %r8d
	movd	%esi, %xmm0
	xorl	1024(%rax,%rbx,4), %edi
	xorl	2048(%rax,%r8,4), %edi
	movl	-88(%rbp), %ebx
	movd	%edi, %xmm7
	punpckldq	%xmm7, %xmm0
	movd	%edx, %xmm7
	movl	%ebx, %ecx
	movzbl	%bl, %edx
	punpckldq	%xmm1, %xmm7
	shrl	$24, %ecx
	movl	(%rax,%rdx,4), %edx
	punpcklqdq	%xmm0, %xmm7
	xorl	3072(%rax,%rcx,4), %edx
	movzbl	%bh, %ecx
	xorl	1024(%rax,%rcx,4), %edx
	movl	%ebx, %ecx
	movl	-92(%rbp), %ebx
	shrl	$16, %ecx
	movl	%ebx, %r11d
	movzbl	%cl, %ecx
	movl	%ebx, %r8d
	shrl	$24, %r11d
	xorl	2048(%rax,%rcx,4), %edx
	movzbl	%bl, %ecx
	shrl	$16, %r8d
	movslq	%r11d, %rsi
	movl	(%rax,%rcx,4), %ecx
	movd	%edx, %xmm8
	xorl	3072(%rax,%rsi,4), %ecx
	movzbl	%bh, %esi
	movl	-96(%rbp), %ebx
	xorl	1024(%rax,%rsi,4), %ecx
	movzbl	%r8b, %esi
	movl	%ebx, %r8d
	xorl	2048(%rax,%rsi,4), %ecx
	movzbl	%bl, %esi
	shrl	$24, %r8d
	movl	(%rax,%rsi,4), %esi
	movd	%ecx, %xmm1
	movslq	%r8d, %rdi
	movl	%ebx, %r8d
	punpckldq	%xmm1, %xmm8
	xorl	3072(%rax,%rdi,4), %esi
	shrl	$16, %r8d
	movzbl	%bh, %edi
	movl	-100(%rbp), %ebx
	xorl	1024(%rax,%rdi,4), %esi
	movzbl	%r8b, %edi
	movl	2048(%rax,%rdi,4), %r8d
	movl	%ebx, %r10d
	xorl	%esi, %r8d
	movzbl	%bl, %esi
	movd	%r8d, %xmm0
	movl	%ebx, %r8d
	movl	(%rax,%rsi,4), %esi
	shrl	$24, %r8d
	movslq	%r8d, %rdi
	xorl	3072(%rax,%rdi,4), %esi
	movzbl	%bh, %edi
	movl	-72(%rbp), %ebx
	xorl	1024(%rax,%rdi,4), %esi
	shrl	$16, %r10d
	movl	%ebx, %ecx
	movzbl	%bl, %edx
	movzbl	%r10b, %edi
	shrl	$24, %ecx
	movl	(%rax,%rdx,4), %edx
	xorl	2048(%rax,%rdi,4), %esi
	xorl	3072(%rax,%rcx,4), %edx
	movzbl	%bh, %ecx
	movd	%esi, %xmm2
	xorl	1024(%rax,%rcx,4), %edx
	movl	%ebx, %ecx
	movl	-76(%rbp), %ebx
	punpckldq	%xmm2, %xmm0
	shrl	$16, %ecx
	punpcklqdq	%xmm0, %xmm8
	movzbl	%cl, %ecx
	xorl	2048(%rax,%rcx,4), %edx
	movzbl	%bl, %ecx
	movd	%edx, %xmm0
	movl	%ebx, %edx
	movl	(%rax,%rcx,4), %ecx
	shrl	$24, %edx
	xorl	3072(%rax,%rdx,4), %ecx
	movzbl	%bh, %edx
	xorl	1024(%rax,%rdx,4), %ecx
	movl	%ebx, %edx
	movl	-80(%rbp), %ebx
	shrl	$16, %edx
	movzbl	%dl, %edx
	movl	2048(%rax,%rdx,4), %esi
	movzbl	%bl, %edx
	movl	(%rax,%rdx,4), %edx
	movups	%xmm8, 32(%r15)
	xorl	%ecx, %esi
	movl	%ebx, %ecx
	shrl	$24, %ecx
	xorl	3072(%rax,%rcx,4), %edx
	movzbl	%bh, %ecx
	xorl	1024(%rax,%rcx,4), %edx
	movl	%ebx, %ecx
	movl	-84(%rbp), %ebx
	shrl	$16, %ecx
	movzbl	%cl, %ecx
	xorl	2048(%rax,%rcx,4), %edx
	movzbl	%bl, %ecx
	movd	%edx, %xmm1
	movl	%ebx, %edx
	movl	(%rax,%rcx,4), %ecx
	shrl	$24, %edx
	xorl	3072(%rax,%rdx,4), %ecx
	movzbl	%bh, %edx
	xorl	1024(%rax,%rdx,4), %ecx
	movl	%ebx, %edx
	movl	-56(%rbp), %ebx
	shrl	$16, %edx
	movzbl	%dl, %edx
	xorl	2048(%rax,%rdx,4), %ecx
	movzbl	%bl, %edx
	movd	%ecx, %xmm2
	movl	%ebx, %ecx
	movl	(%rax,%rdx,4), %edx
	shrl	$24, %ecx
	punpckldq	%xmm2, %xmm1
	movd	%esi, %xmm2
	xorl	3072(%rax,%rcx,4), %edx
	movzbl	%bh, %ecx
	punpckldq	%xmm2, %xmm0
	xorl	1024(%rax,%rcx,4), %edx
	movl	%ebx, %ecx
	movl	-60(%rbp), %ebx
	punpcklqdq	%xmm1, %xmm0
	shrl	$16, %ecx
	movups	%xmm0, 16(%r15)
	movzbl	%cl, %ecx
	xorl	2048(%rax,%rcx,4), %edx
	movzbl	%bl, %ecx
	movd	%edx, %xmm1
	movl	%ebx, %edx
	movl	(%rax,%rcx,4), %ecx
	shrl	$24, %edx
	xorl	3072(%rax,%rdx,4), %ecx
	movzbl	%bh, %edx
	xorl	1024(%rax,%rdx,4), %ecx
	movl	%ebx, %edx
	movl	-64(%rbp), %ebx
	shrl	$16, %edx
	movzbl	%dl, %edx
	movl	2048(%rax,%rdx,4), %edi
	movzbl	%bl, %edx
	movl	(%rax,%rdx,4), %edx
	xorl	%ecx, %edi
	movl	%ebx, %ecx
	movl	%edi, %esi
	shrl	$24, %ecx
	movl	%ebx, %edi
	xorl	3072(%rax,%rcx,4), %edx
	shrl	$16, %edi
	movzbl	%bh, %ecx
	movl	-68(%rbp), %ebx
	xorl	1024(%rax,%rcx,4), %edx
	movzbl	%dil, %ecx
	movd	%esi, %xmm10
	xorl	2048(%rax,%rcx,4), %edx
	movzbl	%bl, %ecx
	punpckldq	%xmm10, %xmm1
	movd	%edx, %xmm2
	movl	%ebx, %edx
	movl	(%rax,%rcx,4), %ecx
	shrl	$24, %edx
	xorl	3072(%rax,%rdx,4), %ecx
	movzbl	%bh, %edx
	shrl	$16, %ebx
	xorl	1024(%rax,%rdx,4), %ecx
	movzbl	%bl, %edx
	xorl	2048(%rax,%rdx,4), %ecx
	movd	%ecx, %xmm9
	punpckldq	%xmm9, %xmm2
	punpcklqdq	%xmm2, %xmm1
	movups	%xmm1, (%r15)
	movups	%xmm7, 48(%r15)
	movups	%xmm6, 64(%r15)
	movups	%xmm5, 80(%r15)
	movups	%xmm4, 96(%r15)
	movups	%xmm3, 112(%r15)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE151:
	.size	SEED_set_key, .-SEED_set_key
	.p2align 4
	.globl	SEED_encrypt
	.type	SEED_encrypt, @function
SEED_encrypt:
.LFB152:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	SS(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rdi), %ebx
	movl	4(%rdi), %ecx
	movl	8(%rdi), %r11d
	movl	12(%rdi), %r12d
	bswap	%ebx
	movl	%ebx, %r8d
	movl	4(%rdx), %ebx
	bswap	%ecx
	movl	%ecx, %r15d
	movl	(%rdx), %ecx
	movl	%r11d, %r14d
	movl	%r12d, %r13d
	bswap	%r14d
	bswap	%r13d
	xorl	%r14d, %ecx
	xorl	%r13d, %ebx
	xorl	%ecx, %ebx
	movl	%ebx, %edi
	movl	%ebx, %r10d
	movzbl	%bl, %r9d
	movzbl	%bh, %ebx
	shrl	$16, %edi
	shrl	$24, %r10d
	movl	(%rax,%r9,4), %r9d
	movzbl	%dil, %edi
	xorl	3072(%rax,%r10,4), %r9d
	xorl	1024(%rax,%rbx,4), %r9d
	movl	2048(%rax,%rdi,4), %ebx
	xorl	%r9d, %ebx
	addl	%ebx, %ecx
	movl	%ecx, %r11d
	movl	%ecx, %edi
	movzbl	%cl, %r10d
	movzbl	%ch, %ecx
	shrl	$16, %r11d
	shrl	$24, %edi
	movl	(%rax,%r10,4), %r10d
	movzbl	%r11b, %r11d
	xorl	3072(%rax,%rdi,4), %r10d
	xorl	1024(%rax,%rcx,4), %r10d
	xorl	2048(%rax,%r11,4), %r10d
	leal	(%rbx,%r10), %ebx
	movl	%ebx, %r9d
	movl	%ebx, %ecx
	movzbl	%bl, %edi
	movzbl	%bh, %ebx
	shrl	$24, %ecx
	shrl	$16, %r9d
	movl	(%rax,%rdi,4), %edi
	movzbl	%r9b, %r9d
	xorl	3072(%rax,%rcx,4), %edi
	movl	%edi, %ecx
	xorl	1024(%rax,%rbx,4), %ecx
	xorl	2048(%rax,%r9,4), %ecx
	xorl	%ecx, %r15d
	addl	%ecx, %r10d
	movl	12(%rdx), %ecx
	movl	8(%rdx), %r9d
	movl	%r10d, %ebx
	xorl	%r8d, %ebx
	xorl	%r15d, %ecx
	xorl	%ebx, %r9d
	movl	%ecx, %r8d
	xorl	%r9d, %r8d
	movl	%r8d, %r11d
	movl	%r8d, %edi
	movl	%r8d, %ecx
	movzbl	%r8b, %r10d
	shrl	$24, %edi
	shrl	$16, %r11d
	movl	(%rax,%r10,4), %r10d
	movzbl	%r11b, %r11d
	xorl	3072(%rax,%rdi,4), %r10d
	movzbl	%ch, %edi
	xorl	1024(%rax,%rdi,4), %r10d
	movl	2048(%rax,%r11,4), %edi
	xorl	%r10d, %edi
	leal	(%r9,%rdi), %r8d
	movl	%edi, %r10d
	movl	%r8d, %r11d
	movl	%r8d, %edi
	movl	%r8d, %ecx
	movzbl	%r8b, %r9d
	shrl	$16, %r11d
	shrl	$24, %edi
	movl	(%rax,%r9,4), %r9d
	movzbl	%r11b, %r11d
	xorl	3072(%rax,%rdi,4), %r9d
	movzbl	%ch, %edi
	xorl	1024(%rax,%rdi,4), %r9d
	xorl	2048(%rax,%r11,4), %r9d
	leal	(%r10,%r9), %edi
	movl	%edi, %r8d
	movl	%edi, %r10d
	movl	%edi, %ecx
	movzbl	%dil, %r11d
	shrl	$16, %r8d
	shrl	$24, %r10d
	movzbl	%ch, %edi
	movl	(%rax,%r11,4), %r12d
	movzbl	%r8b, %r8d
	xorl	3072(%rax,%r10,4), %r12d
	xorl	1024(%rax,%rdi,4), %r12d
	movl	2048(%rax,%r8,4), %edi
	movl	16(%rdx), %r8d
	xorl	%r12d, %edi
	xorl	%edi, %r13d
	addl	%edi, %r9d
	movl	20(%rdx), %edi
	xorl	%r14d, %r9d
	movl	%r13d, %r12d
	xorl	%r9d, %r8d
	xorl	%r13d, %edi
	movl	%r9d, %r11d
	xorl	%r8d, %edi
	movl	%edi, %r10d
	movl	%edi, %r13d
	movl	%edi, %ecx
	movzbl	%dil, %r9d
	shrl	$16, %r10d
	shrl	$24, %r13d
	movzbl	%ch, %edi
	movl	(%rax,%r9,4), %r9d
	movzbl	%r10b, %r10d
	xorl	3072(%rax,%r13,4), %r9d
	xorl	1024(%rax,%rdi,4), %r9d
	xorl	2048(%rax,%r10,4), %r9d
	leal	(%r8,%r9), %edi
	movl	%edi, %r10d
	movl	%edi, %r13d
	movl	%edi, %ecx
	movzbl	%dil, %r8d
	shrl	$16, %r10d
	shrl	$24, %r13d
	movzbl	%ch, %edi
	movl	(%rax,%r8,4), %r8d
	movzbl	%r10b, %r10d
	xorl	3072(%rax,%r13,4), %r8d
	xorl	1024(%rax,%rdi,4), %r8d
	xorl	2048(%rax,%r10,4), %r8d
	addl	%r8d, %r9d
	movl	%r9d, %r10d
	movl	%r9d, %r13d
	movl	%r9d, %ecx
	movzbl	%r9b, %edi
	shrl	$16, %r10d
	shrl	$24, %r13d
	movzbl	%ch, %ecx
	movl	(%rax,%rdi,4), %edi
	movzbl	%r10b, %r10d
	xorl	3072(%rax,%r13,4), %edi
	xorl	1024(%rax,%rcx,4), %edi
	movl	%r15d, %ecx
	xorl	2048(%rax,%r10,4), %edi
	addl	%edi, %r8d
	xorl	%ebx, %r8d
	xorl	%edi, %ecx
	movl	28(%rdx), %ebx
	movl	24(%rdx), %edi
	movl	%r8d, %r10d
	xorl	%r8d, %edi
	xorl	%ecx, %ebx
	xorl	%edi, %ebx
	movl	%ebx, %r9d
	movl	%ebx, %r13d
	movzbl	%bl, %r8d
	movzbl	%bh, %ebx
	shrl	$16, %r9d
	shrl	$24, %r13d
	movl	(%rax,%r8,4), %r8d
	movzbl	%r9b, %r9d
	xorl	3072(%rax,%r13,4), %r8d
	xorl	1024(%rax,%rbx,4), %r8d
	movl	2048(%rax,%r9,4), %ebx
	xorl	%r8d, %ebx
	movl	%ebx, %r8d
	leal	(%rdi,%rbx), %ebx
	movl	%ebx, %r13d
	movl	%ebx, %edi
	movzbl	%bl, %r9d
	movzbl	%bh, %ebx
	shrl	$16, %r13d
	shrl	$24, %edi
	movl	(%rax,%r9,4), %r9d
	movzbl	%r13b, %r13d
	xorl	3072(%rax,%rdi,4), %r9d
	xorl	1024(%rax,%rbx,4), %r9d
	xorl	2048(%rax,%r13,4), %r9d
	leal	(%r8,%r9), %ebx
	movl	%ebx, %r13d
	movl	%ebx, %edi
	movzbl	%bl, %r8d
	movzbl	%bh, %ebx
	shrl	$24, %edi
	shrl	$16, %r13d
	movl	(%rax,%r8,4), %r8d
	movzbl	%r13b, %r13d
	xorl	3072(%rax,%rdi,4), %r8d
	movl	%r8d, %edi
	xorl	1024(%rax,%rbx,4), %edi
	movl	32(%rdx), %r8d
	xorl	2048(%rax,%r13,4), %edi
	movl	36(%rdx), %ebx
	addl	%edi, %r9d
	xorl	%r12d, %edi
	xorl	%r11d, %r9d
	xorl	%edi, %ebx
	xorl	%r9d, %r8d
	xorl	%r8d, %ebx
	movl	%ebx, %r12d
	movl	%ebx, %r13d
	movzbl	%bl, %r11d
	movzbl	%bh, %ebx
	shrl	$16, %r12d
	shrl	$24, %r13d
	movl	(%rax,%r11,4), %r11d
	movzbl	%r12b, %r12d
	xorl	3072(%rax,%r13,4), %r11d
	xorl	1024(%rax,%rbx,4), %r11d
	movl	2048(%rax,%r12,4), %ebx
	xorl	%r11d, %ebx
	movl	%ebx, %r11d
	leal	(%r8,%rbx), %ebx
	movl	%ebx, %r12d
	movl	%ebx, %r13d
	movzbl	%bl, %r8d
	movzbl	%bh, %ebx
	shrl	$16, %r12d
	shrl	$24, %r13d
	movl	(%rax,%r8,4), %r8d
	movzbl	%r12b, %r12d
	xorl	3072(%rax,%r13,4), %r8d
	xorl	1024(%rax,%rbx,4), %r8d
	xorl	2048(%rax,%r12,4), %r8d
	leal	(%r11,%r8), %ebx
	movl	%ebx, %r12d
	movl	%ebx, %r13d
	movzbl	%bl, %r11d
	movzbl	%bh, %ebx
	shrl	$16, %r12d
	shrl	$24, %r13d
	movl	(%rax,%r11,4), %r11d
	movzbl	%r12b, %r12d
	xorl	3072(%rax,%r13,4), %r11d
	xorl	1024(%rax,%rbx,4), %r11d
	xorl	2048(%rax,%r12,4), %r11d
	movl	44(%rdx), %ebx
	addl	%r11d, %r8d
	xorl	%r11d, %ecx
	xorl	%r10d, %r8d
	movl	40(%rdx), %r10d
	xorl	%ecx, %ebx
	xorl	%r8d, %r10d
	xorl	%r10d, %ebx
	movl	%ebx, %r11d
	movl	%ebx, %r13d
	movzbl	%bl, %r12d
	movzbl	%bh, %ebx
	shrl	$16, %r11d
	shrl	$24, %r13d
	movl	(%rax,%r12,4), %r12d
	movzbl	%r11b, %r11d
	xorl	3072(%rax,%r13,4), %r12d
	xorl	1024(%rax,%rbx,4), %r12d
	movl	2048(%rax,%r11,4), %ebx
	xorl	%r12d, %ebx
	movl	%ebx, %r12d
	leal	(%r10,%rbx), %ebx
	movl	%ebx, %r10d
	movl	%ebx, %r13d
	movzbl	%bl, %r11d
	movzbl	%bh, %ebx
	shrl	$16, %r10d
	shrl	$24, %r13d
	movl	(%rax,%r11,4), %r11d
	movzbl	%r10b, %r10d
	xorl	3072(%rax,%r13,4), %r11d
	xorl	1024(%rax,%rbx,4), %r11d
	xorl	2048(%rax,%r10,4), %r11d
	leal	(%r12,%r11), %ebx
	movl	%ebx, %r12d
	movl	%ebx, %r13d
	movzbl	%bl, %r10d
	movzbl	%bh, %ebx
	shrl	$16, %r12d
	shrl	$24, %r13d
	movl	(%rax,%r10,4), %r10d
	movzbl	%r12b, %r12d
	xorl	3072(%rax,%r13,4), %r10d
	xorl	1024(%rax,%rbx,4), %r10d
	xorl	2048(%rax,%r12,4), %r10d
	movl	52(%rdx), %ebx
	addl	%r10d, %r11d
	xorl	%r10d, %edi
	movl	48(%rdx), %r10d
	xorl	%r11d, %r9d
	xorl	%r9d, %r10d
	xorl	%edi, %ebx
	xorl	%r10d, %ebx
	movl	%ebx, %r11d
	movl	%ebx, %r13d
	movzbl	%bl, %r12d
	movzbl	%bh, %ebx
	shrl	$16, %r11d
	shrl	$24, %r13d
	movl	(%rax,%r12,4), %r12d
	movzbl	%r11b, %r11d
	xorl	3072(%rax,%r13,4), %r12d
	xorl	1024(%rax,%rbx,4), %r12d
	xorl	2048(%rax,%r11,4), %r12d
	leal	(%r10,%r12), %ebx
	movl	%ebx, %r10d
	movl	%ebx, %r13d
	movzbl	%bl, %r11d
	movzbl	%bh, %ebx
	shrl	$16, %r10d
	shrl	$24, %r13d
	movl	(%rax,%r11,4), %r11d
	movzbl	%r10b, %r10d
	xorl	3072(%rax,%r13,4), %r11d
	xorl	1024(%rax,%rbx,4), %r11d
	xorl	2048(%rax,%r10,4), %r11d
	addl	%r11d, %r12d
	movl	%r12d, %r13d
	movzbl	%r12b, %ebx
	movl	%r12d, %r10d
	shrl	$24, %r13d
	movl	(%rax,%rbx,4), %ebx
	shrl	$16, %r10d
	xorl	3072(%rax,%r13,4), %ebx
	movzbl	%r10b, %r10d
	movl	%ebx, %r15d
	movl	%r12d, %ebx
	movzbl	%bh, %ebx
	movl	1024(%rax,%rbx,4), %ebx
	xorl	%r15d, %ebx
	xorl	2048(%rax,%r10,4), %ebx
	movl	56(%rdx), %r10d
	addl	%ebx, %r11d
	xorl	%ebx, %ecx
	movl	60(%rdx), %ebx
	xorl	%r11d, %r8d
	xorl	%r8d, %r10d
	xorl	%ecx, %ebx
	xorl	%r10d, %ebx
	movl	%ebx, %r11d
	movl	%ebx, %r13d
	movzbl	%bl, %r12d
	movzbl	%bh, %ebx
	shrl	$16, %r11d
	shrl	$24, %r13d
	movl	(%rax,%r12,4), %r12d
	movzbl	%r11b, %r11d
	xorl	3072(%rax,%r13,4), %r12d
	xorl	1024(%rax,%rbx,4), %r12d
	movl	2048(%rax,%r11,4), %ebx
	xorl	%r12d, %ebx
	movl	%ebx, %r12d
	leal	(%r10,%rbx), %ebx
	movl	%ebx, %r10d
	movl	%ebx, %r13d
	movzbl	%bl, %r11d
	movzbl	%bh, %ebx
	shrl	$16, %r10d
	shrl	$24, %r13d
	movl	(%rax,%r11,4), %r11d
	movzbl	%r10b, %r10d
	xorl	3072(%rax,%r13,4), %r11d
	xorl	1024(%rax,%rbx,4), %r11d
	xorl	2048(%rax,%r10,4), %r11d
	leal	(%r12,%r11), %ebx
	movl	%ebx, %r12d
	movl	%ebx, %r13d
	movzbl	%bl, %r10d
	movzbl	%bh, %ebx
	shrl	$16, %r12d
	shrl	$24, %r13d
	movl	(%rax,%r10,4), %r10d
	movzbl	%r12b, %r12d
	xorl	3072(%rax,%r13,4), %r10d
	xorl	1024(%rax,%rbx,4), %r10d
	xorl	2048(%rax,%r12,4), %r10d
	movl	68(%rdx), %ebx
	addl	%r10d, %r11d
	xorl	%r10d, %edi
	movl	64(%rdx), %r10d
	xorl	%r11d, %r9d
	xorl	%edi, %ebx
	xorl	%r9d, %r10d
	xorl	%r10d, %ebx
	movl	%ebx, %r11d
	movl	%ebx, %r13d
	movzbl	%bl, %r12d
	movzbl	%bh, %ebx
	shrl	$16, %r11d
	shrl	$24, %r13d
	movl	(%rax,%r12,4), %r12d
	movzbl	%r11b, %r11d
	xorl	3072(%rax,%r13,4), %r12d
	xorl	1024(%rax,%rbx,4), %r12d
	xorl	2048(%rax,%r11,4), %r12d
	leal	(%r10,%r12), %ebx
	movl	%ebx, %r10d
	movl	%ebx, %r13d
	movzbl	%bl, %r11d
	movzbl	%bh, %ebx
	shrl	$16, %r10d
	shrl	$24, %r13d
	movl	(%rax,%r11,4), %r11d
	movzbl	%r10b, %r10d
	xorl	3072(%rax,%r13,4), %r11d
	xorl	1024(%rax,%rbx,4), %r11d
	xorl	2048(%rax,%r10,4), %r11d
	addl	%r11d, %r12d
	movl	%r12d, %r13d
	movzbl	%r12b, %ebx
	movl	%r12d, %r10d
	shrl	$24, %r13d
	movl	(%rax,%rbx,4), %ebx
	shrl	$16, %r10d
	xorl	3072(%rax,%r13,4), %ebx
	movzbl	%r10b, %r10d
	movl	%ebx, %r14d
	movl	%r12d, %ebx
	movzbl	%bh, %ebx
	movl	1024(%rax,%rbx,4), %ebx
	xorl	%r14d, %ebx
	xorl	2048(%rax,%r10,4), %ebx
	movl	72(%rdx), %r10d
	addl	%ebx, %r11d
	xorl	%r11d, %r8d
	xorl	%ebx, %ecx
	movl	76(%rdx), %ebx
	xorl	%r8d, %r10d
	xorl	%ecx, %ebx
	xorl	%r10d, %ebx
	movl	%ebx, %r11d
	movl	%ebx, %r13d
	movzbl	%bl, %r12d
	movzbl	%bh, %ebx
	shrl	$16, %r11d
	shrl	$24, %r13d
	movl	(%rax,%r12,4), %r12d
	movzbl	%r11b, %r11d
	xorl	3072(%rax,%r13,4), %r12d
	xorl	1024(%rax,%rbx,4), %r12d
	movl	2048(%rax,%r11,4), %ebx
	xorl	%r12d, %ebx
	movl	%ebx, %r12d
	leal	(%r10,%rbx), %ebx
	movl	%ebx, %r10d
	movl	%ebx, %r13d
	movzbl	%bl, %r11d
	movzbl	%bh, %ebx
	shrl	$16, %r10d
	shrl	$24, %r13d
	movl	(%rax,%r11,4), %r11d
	movzbl	%r10b, %r10d
	xorl	3072(%rax,%r13,4), %r11d
	xorl	1024(%rax,%rbx,4), %r11d
	xorl	2048(%rax,%r10,4), %r11d
	leal	(%r12,%r11), %ebx
	movl	%ebx, %r12d
	movl	%ebx, %r13d
	movzbl	%bl, %r10d
	movzbl	%bh, %ebx
	shrl	$16, %r12d
	shrl	$24, %r13d
	movl	(%rax,%r10,4), %r10d
	movzbl	%r12b, %r12d
	xorl	3072(%rax,%r13,4), %r10d
	xorl	1024(%rax,%rbx,4), %r10d
	xorl	2048(%rax,%r12,4), %r10d
	movl	84(%rdx), %ebx
	addl	%r10d, %r11d
	xorl	%r10d, %edi
	movl	80(%rdx), %r10d
	xorl	%r11d, %r9d
	xorl	%edi, %ebx
	xorl	%r9d, %r10d
	xorl	%r10d, %ebx
	movl	%ebx, %r11d
	movl	%ebx, %r13d
	movzbl	%bl, %r12d
	movzbl	%bh, %ebx
	shrl	$16, %r11d
	shrl	$24, %r13d
	movl	(%rax,%r12,4), %r12d
	movzbl	%r11b, %r11d
	xorl	3072(%rax,%r13,4), %r12d
	xorl	1024(%rax,%rbx,4), %r12d
	xorl	2048(%rax,%r11,4), %r12d
	leal	(%r10,%r12), %ebx
	movl	%ebx, %r11d
	movl	%ebx, %r13d
	movzbl	%bl, %r10d
	movzbl	%bh, %ebx
	shrl	$16, %r11d
	shrl	$24, %r13d
	movl	(%rax,%r10,4), %r10d
	movzbl	%r11b, %r11d
	xorl	3072(%rax,%r13,4), %r10d
	xorl	1024(%rax,%rbx,4), %r10d
	xorl	2048(%rax,%r11,4), %r10d
	addl	%r10d, %r12d
	movl	%r12d, %r13d
	movzbl	%r12b, %ebx
	movl	%r12d, %r11d
	shrl	$24, %r13d
	movl	(%rax,%rbx,4), %ebx
	shrl	$16, %r11d
	xorl	3072(%rax,%r13,4), %ebx
	movzbl	%r11b, %r11d
	movl	%ebx, %r15d
	movl	%r12d, %ebx
	movzbl	%bh, %ebx
	movl	1024(%rax,%rbx,4), %ebx
	xorl	%r15d, %ebx
	xorl	2048(%rax,%r11,4), %ebx
	addl	%ebx, %r10d
	xorl	%ebx, %ecx
	movl	92(%rdx), %ebx
	movl	%r10d, %r11d
	xorl	%r8d, %r11d
	movl	88(%rdx), %r8d
	xorl	%ecx, %ebx
	xorl	%r11d, %r8d
	xorl	%r8d, %ebx
	movl	%ebx, %r10d
	movl	%ebx, %r13d
	movzbl	%bl, %r12d
	movzbl	%bh, %ebx
	shrl	$16, %r10d
	shrl	$24, %r13d
	movl	(%rax,%r12,4), %r12d
	movzbl	%r10b, %r10d
	xorl	3072(%rax,%r13,4), %r12d
	xorl	1024(%rax,%rbx,4), %r12d
	movl	2048(%rax,%r10,4), %ebx
	xorl	%r12d, %ebx
	movl	%ebx, %r12d
	leal	(%r8,%rbx), %ebx
	movl	%ebx, %r8d
	movl	%ebx, %r13d
	movzbl	%bl, %r10d
	movzbl	%bh, %ebx
	shrl	$16, %r8d
	shrl	$24, %r13d
	movl	(%rax,%r10,4), %r10d
	movzbl	%r8b, %r8d
	xorl	3072(%rax,%r13,4), %r10d
	xorl	1024(%rax,%rbx,4), %r10d
	xorl	2048(%rax,%r8,4), %r10d
	leal	(%r12,%r10), %ebx
	movl	%ebx, %r12d
	movl	%ebx, %r13d
	movzbl	%bl, %r8d
	movzbl	%bh, %ebx
	shrl	$16, %r12d
	shrl	$24, %r13d
	movl	(%rax,%r8,4), %r8d
	movzbl	%r12b, %r12d
	xorl	3072(%rax,%r13,4), %r8d
	xorl	1024(%rax,%rbx,4), %r8d
	xorl	2048(%rax,%r12,4), %r8d
	movl	100(%rdx), %ebx
	addl	%r8d, %r10d
	xorl	%r10d, %r9d
	xorl	%r8d, %edi
	movl	96(%rdx), %r8d
	xorl	%edi, %ebx
	xorl	%r9d, %r8d
	xorl	%r8d, %ebx
	movl	%ebx, %r10d
	movl	%ebx, %r13d
	movzbl	%bl, %r12d
	movzbl	%bh, %ebx
	shrl	$16, %r10d
	shrl	$24, %r13d
	movl	(%rax,%r12,4), %r12d
	movzbl	%r10b, %r10d
	xorl	3072(%rax,%r13,4), %r12d
	xorl	1024(%rax,%rbx,4), %r12d
	xorl	2048(%rax,%r10,4), %r12d
	leal	(%r8,%r12), %ebx
	movl	%ebx, %r10d
	movl	%ebx, %r13d
	movzbl	%bl, %r8d
	movzbl	%bh, %ebx
	shrl	$16, %r10d
	shrl	$24, %r13d
	movl	(%rax,%r8,4), %r8d
	movzbl	%r10b, %r10d
	xorl	3072(%rax,%r13,4), %r8d
	xorl	1024(%rax,%rbx,4), %r8d
	xorl	2048(%rax,%r10,4), %r8d
	addl	%r8d, %r12d
	movl	%r12d, %r13d
	movzbl	%r12b, %ebx
	movl	%r12d, %r10d
	shrl	$24, %r13d
	movl	(%rax,%rbx,4), %ebx
	shrl	$16, %r10d
	xorl	3072(%rax,%r13,4), %ebx
	movzbl	%r10b, %r10d
	movl	%ebx, %r14d
	movl	%r12d, %ebx
	movzbl	%bh, %ebx
	movl	1024(%rax,%rbx,4), %ebx
	xorl	%r14d, %ebx
	xorl	2048(%rax,%r10,4), %ebx
	addl	%ebx, %r8d
	xorl	%ebx, %ecx
	xorl	%r11d, %r8d
	movl	%r8d, %r10d
	xorl	104(%rdx), %r8d
	movl	108(%rdx), %ebx
	xorl	%ecx, %ebx
	xorl	%r8d, %ebx
	movl	%ebx, %r11d
	movl	%ebx, %r13d
	movzbl	%bl, %r12d
	movzbl	%bh, %ebx
	shrl	$16, %r11d
	shrl	$24, %r13d
	movl	(%rax,%r12,4), %r12d
	movzbl	%r11b, %r11d
	xorl	3072(%rax,%r13,4), %r12d
	xorl	1024(%rax,%rbx,4), %r12d
	movl	2048(%rax,%r11,4), %ebx
	xorl	%r12d, %ebx
	movl	%ebx, %r12d
	leal	(%r8,%rbx), %ebx
	movl	%ebx, %r8d
	movl	%ebx, %r13d
	movzbl	%bl, %r11d
	movzbl	%bh, %ebx
	shrl	$16, %r8d
	shrl	$24, %r13d
	movl	(%rax,%r11,4), %r11d
	movzbl	%r8b, %r8d
	xorl	3072(%rax,%r13,4), %r11d
	xorl	1024(%rax,%rbx,4), %r11d
	xorl	2048(%rax,%r8,4), %r11d
	leal	(%r12,%r11), %ebx
	movl	%ebx, %r12d
	movl	%ebx, %r13d
	movzbl	%bl, %r8d
	movzbl	%bh, %ebx
	shrl	$16, %r12d
	shrl	$24, %r13d
	movl	(%rax,%r8,4), %r8d
	movzbl	%r12b, %r12d
	xorl	3072(%rax,%r13,4), %r8d
	xorl	1024(%rax,%rbx,4), %r8d
	xorl	2048(%rax,%r12,4), %r8d
	movl	116(%rdx), %ebx
	addl	%r8d, %r11d
	xorl	%r8d, %edi
	movl	112(%rdx), %r8d
	xorl	%r11d, %r9d
	xorl	%edi, %ebx
	xorl	%r9d, %r8d
	xorl	%r8d, %ebx
	movl	%ebx, %r12d
	movl	%ebx, %r13d
	movzbl	%bl, %r11d
	movzbl	%bh, %ebx
	shrl	$16, %r12d
	shrl	$24, %r13d
	movl	(%rax,%r11,4), %r11d
	movzbl	%r12b, %r12d
	xorl	3072(%rax,%r13,4), %r11d
	xorl	1024(%rax,%rbx,4), %r11d
	movl	2048(%rax,%r12,4), %ebx
	xorl	%r11d, %ebx
	movl	%ebx, %r11d
	leal	(%r8,%rbx), %ebx
	movl	%ebx, %r12d
	movl	%ebx, %r13d
	movzbl	%bl, %r8d
	movzbl	%bh, %ebx
	shrl	$16, %r12d
	shrl	$24, %r13d
	movl	(%rax,%r8,4), %r8d
	movzbl	%r12b, %r12d
	xorl	3072(%rax,%r13,4), %r8d
	xorl	1024(%rax,%rbx,4), %r8d
	xorl	2048(%rax,%r12,4), %r8d
	leal	(%r11,%r8), %ebx
	movl	%ebx, %r12d
	movl	%ebx, %r13d
	movzbl	%bl, %r11d
	movzbl	%bh, %ebx
	shrl	$16, %r12d
	shrl	$24, %r13d
	movl	(%rax,%r11,4), %r11d
	movzbl	%r12b, %r12d
	xorl	3072(%rax,%r13,4), %r11d
	xorl	1024(%rax,%rbx,4), %r11d
	xorl	2048(%rax,%r12,4), %r11d
	movl	124(%rdx), %ebx
	addl	%r11d, %r8d
	xorl	%r10d, %r8d
	xorl	%r11d, %ecx
	movl	120(%rdx), %r11d
	xorl	%ecx, %ebx
	bswap	%ecx
	xorl	%r8d, %r11d
	movl	%ebx, %edx
	bswap	%r8d
	xorl	%r11d, %edx
	movl	%edx, %r10d
	movl	%edx, %r12d
	movzbl	%dl, %ebx
	movzbl	%dh, %edx
	shrl	$16, %r10d
	shrl	$24, %r12d
	movl	(%rax,%rbx,4), %ebx
	movzbl	%r10b, %r10d
	xorl	3072(%rax,%r12,4), %ebx
	xorl	1024(%rax,%rdx,4), %ebx
	movl	%r8d, 8(%rsi)
	xorl	2048(%rax,%r10,4), %ebx
	movl	%ecx, 12(%rsi)
	movl	%ebx, %r10d
	leal	(%r11,%rbx), %ebx
	movl	%ebx, %r11d
	movl	%ebx, %r12d
	movzbl	%bl, %edx
	movzbl	%bh, %ebx
	shrl	$16, %r11d
	shrl	$24, %r12d
	movl	(%rax,%rdx,4), %edx
	movzbl	%r11b, %r11d
	xorl	3072(%rax,%r12,4), %edx
	xorl	1024(%rax,%rbx,4), %edx
	xorl	2048(%rax,%r11,4), %edx
	leal	(%r10,%rdx), %ebx
	movl	%edx, %r11d
	movl	%ebx, %r10d
	movl	%ebx, %r12d
	movzbl	%bl, %edx
	movzbl	%bh, %ebx
	shrl	$16, %r10d
	shrl	$24, %r12d
	movl	(%rax,%rdx,4), %edx
	movzbl	%r10b, %r10d
	xorl	3072(%rax,%r12,4), %edx
	xorl	1024(%rax,%rbx,4), %edx
	xorl	2048(%rax,%r10,4), %edx
	popq	%rbx
	movl	%edx, %eax
	leal	(%r11,%rdx), %edx
	popq	%r12
	popq	%r13
	xorl	%r9d, %edx
	xorl	%eax, %edi
	popq	%r14
	popq	%r15
	bswap	%edx
	bswap	%edi
	movl	%edx, (%rsi)
	popq	%rbp
	.cfi_def_cfa 7, 8
	movl	%edi, 4(%rsi)
	ret
	.cfi_endproc
.LFE152:
	.size	SEED_encrypt, .-SEED_encrypt
	.p2align 4
	.globl	SEED_decrypt
	.type	SEED_decrypt, @function
SEED_decrypt:
.LFB153:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	SS(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rdi), %ebx
	movl	4(%rdi), %ecx
	movl	8(%rdi), %r11d
	movl	12(%rdi), %r12d
	bswap	%ebx
	movl	%ebx, %r8d
	movl	124(%rdx), %ebx
	bswap	%ecx
	movl	%ecx, %r15d
	movl	120(%rdx), %ecx
	movl	%r11d, %r14d
	movl	%r12d, %r13d
	bswap	%r14d
	bswap	%r13d
	xorl	%r14d, %ecx
	xorl	%r13d, %ebx
	xorl	%ecx, %ebx
	movl	%ebx, %edi
	movl	%ebx, %r10d
	movzbl	%bl, %r9d
	movzbl	%bh, %ebx
	shrl	$16, %edi
	shrl	$24, %r10d
	movl	(%rax,%r9,4), %r9d
	movzbl	%dil, %edi
	xorl	3072(%rax,%r10,4), %r9d
	xorl	1024(%rax,%rbx,4), %r9d
	movl	2048(%rax,%rdi,4), %ebx
	xorl	%r9d, %ebx
	addl	%ebx, %ecx
	movl	%ecx, %r11d
	movl	%ecx, %edi
	movzbl	%cl, %r10d
	movzbl	%ch, %ecx
	shrl	$16, %r11d
	shrl	$24, %edi
	movl	(%rax,%r10,4), %r10d
	movzbl	%r11b, %r11d
	xorl	3072(%rax,%rdi,4), %r10d
	xorl	1024(%rax,%rcx,4), %r10d
	xorl	2048(%rax,%r11,4), %r10d
	leal	(%rbx,%r10), %ebx
	movl	%ebx, %r9d
	movl	%ebx, %ecx
	movzbl	%bl, %edi
	movzbl	%bh, %ebx
	shrl	$24, %ecx
	shrl	$16, %r9d
	movl	(%rax,%rdi,4), %edi
	movzbl	%r9b, %r9d
	xorl	3072(%rax,%rcx,4), %edi
	movl	%edi, %ecx
	xorl	1024(%rax,%rbx,4), %ecx
	xorl	2048(%rax,%r9,4), %ecx
	xorl	%ecx, %r15d
	addl	%ecx, %r10d
	movl	116(%rdx), %ecx
	movl	112(%rdx), %r9d
	movl	%r10d, %ebx
	xorl	%r8d, %ebx
	xorl	%r15d, %ecx
	xorl	%ebx, %r9d
	movl	%ecx, %r8d
	xorl	%r9d, %r8d
	movl	%r8d, %r11d
	movl	%r8d, %edi
	movl	%r8d, %ecx
	movzbl	%r8b, %r10d
	shrl	$24, %edi
	shrl	$16, %r11d
	movl	(%rax,%r10,4), %r10d
	movzbl	%r11b, %r11d
	xorl	3072(%rax,%rdi,4), %r10d
	movzbl	%ch, %edi
	xorl	1024(%rax,%rdi,4), %r10d
	movl	2048(%rax,%r11,4), %edi
	xorl	%r10d, %edi
	leal	(%r9,%rdi), %r8d
	movl	%edi, %r10d
	movl	%r8d, %r11d
	movl	%r8d, %edi
	movl	%r8d, %ecx
	movzbl	%r8b, %r9d
	shrl	$16, %r11d
	shrl	$24, %edi
	movl	(%rax,%r9,4), %r9d
	movzbl	%r11b, %r11d
	xorl	3072(%rax,%rdi,4), %r9d
	movzbl	%ch, %edi
	xorl	1024(%rax,%rdi,4), %r9d
	xorl	2048(%rax,%r11,4), %r9d
	leal	(%r10,%r9), %edi
	movl	%edi, %r8d
	movl	%edi, %r10d
	movl	%edi, %ecx
	movzbl	%dil, %r11d
	shrl	$16, %r8d
	shrl	$24, %r10d
	movzbl	%ch, %edi
	movl	(%rax,%r11,4), %r12d
	movzbl	%r8b, %r8d
	xorl	3072(%rax,%r10,4), %r12d
	xorl	1024(%rax,%rdi,4), %r12d
	movl	2048(%rax,%r8,4), %edi
	movl	104(%rdx), %r8d
	xorl	%r12d, %edi
	xorl	%edi, %r13d
	addl	%edi, %r9d
	movl	108(%rdx), %edi
	xorl	%r14d, %r9d
	movl	%r13d, %r12d
	xorl	%r9d, %r8d
	xorl	%r13d, %edi
	movl	%r9d, %r11d
	xorl	%r8d, %edi
	movl	%edi, %r10d
	movl	%edi, %r13d
	movl	%edi, %ecx
	movzbl	%dil, %r9d
	shrl	$16, %r10d
	shrl	$24, %r13d
	movzbl	%ch, %edi
	movl	(%rax,%r9,4), %r9d
	movzbl	%r10b, %r10d
	xorl	3072(%rax,%r13,4), %r9d
	xorl	1024(%rax,%rdi,4), %r9d
	xorl	2048(%rax,%r10,4), %r9d
	leal	(%r8,%r9), %edi
	movl	%edi, %r10d
	movl	%edi, %r13d
	movl	%edi, %ecx
	movzbl	%dil, %r8d
	shrl	$16, %r10d
	shrl	$24, %r13d
	movzbl	%ch, %edi
	movl	(%rax,%r8,4), %r8d
	movzbl	%r10b, %r10d
	xorl	3072(%rax,%r13,4), %r8d
	xorl	1024(%rax,%rdi,4), %r8d
	xorl	2048(%rax,%r10,4), %r8d
	addl	%r8d, %r9d
	movl	%r9d, %r10d
	movl	%r9d, %r13d
	movl	%r9d, %ecx
	movzbl	%r9b, %edi
	shrl	$16, %r10d
	shrl	$24, %r13d
	movzbl	%ch, %ecx
	movl	(%rax,%rdi,4), %edi
	movzbl	%r10b, %r10d
	xorl	3072(%rax,%r13,4), %edi
	xorl	1024(%rax,%rcx,4), %edi
	movl	%r15d, %ecx
	xorl	2048(%rax,%r10,4), %edi
	addl	%edi, %r8d
	xorl	%ebx, %r8d
	xorl	%edi, %ecx
	movl	100(%rdx), %ebx
	movl	96(%rdx), %edi
	movl	%r8d, %r10d
	xorl	%r8d, %edi
	xorl	%ecx, %ebx
	xorl	%edi, %ebx
	movl	%ebx, %r9d
	movl	%ebx, %r13d
	movzbl	%bl, %r8d
	movzbl	%bh, %ebx
	shrl	$16, %r9d
	shrl	$24, %r13d
	movl	(%rax,%r8,4), %r8d
	movzbl	%r9b, %r9d
	xorl	3072(%rax,%r13,4), %r8d
	xorl	1024(%rax,%rbx,4), %r8d
	movl	2048(%rax,%r9,4), %ebx
	xorl	%r8d, %ebx
	movl	%ebx, %r8d
	leal	(%rdi,%rbx), %ebx
	movl	%ebx, %r13d
	movl	%ebx, %edi
	movzbl	%bl, %r9d
	movzbl	%bh, %ebx
	shrl	$16, %r13d
	shrl	$24, %edi
	movl	(%rax,%r9,4), %r9d
	movzbl	%r13b, %r13d
	xorl	3072(%rax,%rdi,4), %r9d
	xorl	1024(%rax,%rbx,4), %r9d
	xorl	2048(%rax,%r13,4), %r9d
	leal	(%r8,%r9), %ebx
	movl	%ebx, %r13d
	movl	%ebx, %edi
	movzbl	%bl, %r8d
	movzbl	%bh, %ebx
	shrl	$24, %edi
	shrl	$16, %r13d
	movl	(%rax,%r8,4), %r8d
	movzbl	%r13b, %r13d
	xorl	3072(%rax,%rdi,4), %r8d
	movl	%r8d, %edi
	xorl	1024(%rax,%rbx,4), %edi
	movl	88(%rdx), %r8d
	xorl	2048(%rax,%r13,4), %edi
	movl	92(%rdx), %ebx
	addl	%edi, %r9d
	xorl	%r12d, %edi
	xorl	%r11d, %r9d
	xorl	%edi, %ebx
	xorl	%r9d, %r8d
	xorl	%r8d, %ebx
	movl	%ebx, %r12d
	movl	%ebx, %r13d
	movzbl	%bl, %r11d
	movzbl	%bh, %ebx
	shrl	$16, %r12d
	shrl	$24, %r13d
	movl	(%rax,%r11,4), %r11d
	movzbl	%r12b, %r12d
	xorl	3072(%rax,%r13,4), %r11d
	xorl	1024(%rax,%rbx,4), %r11d
	movl	2048(%rax,%r12,4), %ebx
	xorl	%r11d, %ebx
	movl	%ebx, %r11d
	leal	(%r8,%rbx), %ebx
	movl	%ebx, %r12d
	movl	%ebx, %r13d
	movzbl	%bl, %r8d
	movzbl	%bh, %ebx
	shrl	$16, %r12d
	shrl	$24, %r13d
	movl	(%rax,%r8,4), %r8d
	movzbl	%r12b, %r12d
	xorl	3072(%rax,%r13,4), %r8d
	xorl	1024(%rax,%rbx,4), %r8d
	xorl	2048(%rax,%r12,4), %r8d
	leal	(%r11,%r8), %ebx
	movl	%ebx, %r12d
	movl	%ebx, %r13d
	movzbl	%bl, %r11d
	movzbl	%bh, %ebx
	shrl	$16, %r12d
	shrl	$24, %r13d
	movl	(%rax,%r11,4), %r11d
	movzbl	%r12b, %r12d
	xorl	3072(%rax,%r13,4), %r11d
	xorl	1024(%rax,%rbx,4), %r11d
	xorl	2048(%rax,%r12,4), %r11d
	movl	84(%rdx), %ebx
	addl	%r11d, %r8d
	xorl	%r11d, %ecx
	xorl	%r10d, %r8d
	movl	80(%rdx), %r10d
	xorl	%ecx, %ebx
	xorl	%r8d, %r10d
	xorl	%r10d, %ebx
	movl	%ebx, %r11d
	movl	%ebx, %r13d
	movzbl	%bl, %r12d
	movzbl	%bh, %ebx
	shrl	$16, %r11d
	shrl	$24, %r13d
	movl	(%rax,%r12,4), %r12d
	movzbl	%r11b, %r11d
	xorl	3072(%rax,%r13,4), %r12d
	xorl	1024(%rax,%rbx,4), %r12d
	movl	2048(%rax,%r11,4), %ebx
	xorl	%r12d, %ebx
	movl	%ebx, %r12d
	leal	(%r10,%rbx), %ebx
	movl	%ebx, %r10d
	movl	%ebx, %r13d
	movzbl	%bl, %r11d
	movzbl	%bh, %ebx
	shrl	$16, %r10d
	shrl	$24, %r13d
	movl	(%rax,%r11,4), %r11d
	movzbl	%r10b, %r10d
	xorl	3072(%rax,%r13,4), %r11d
	xorl	1024(%rax,%rbx,4), %r11d
	xorl	2048(%rax,%r10,4), %r11d
	leal	(%r12,%r11), %ebx
	movl	%ebx, %r12d
	movl	%ebx, %r13d
	movzbl	%bl, %r10d
	movzbl	%bh, %ebx
	shrl	$16, %r12d
	shrl	$24, %r13d
	movl	(%rax,%r10,4), %r10d
	movzbl	%r12b, %r12d
	xorl	3072(%rax,%r13,4), %r10d
	xorl	1024(%rax,%rbx,4), %r10d
	xorl	2048(%rax,%r12,4), %r10d
	movl	76(%rdx), %ebx
	addl	%r10d, %r11d
	xorl	%r10d, %edi
	movl	72(%rdx), %r10d
	xorl	%r11d, %r9d
	xorl	%r9d, %r10d
	xorl	%edi, %ebx
	xorl	%r10d, %ebx
	movl	%ebx, %r11d
	movl	%ebx, %r13d
	movzbl	%bl, %r12d
	movzbl	%bh, %ebx
	shrl	$16, %r11d
	shrl	$24, %r13d
	movl	(%rax,%r12,4), %r12d
	movzbl	%r11b, %r11d
	xorl	3072(%rax,%r13,4), %r12d
	xorl	1024(%rax,%rbx,4), %r12d
	xorl	2048(%rax,%r11,4), %r12d
	leal	(%r10,%r12), %ebx
	movl	%ebx, %r10d
	movl	%ebx, %r13d
	movzbl	%bl, %r11d
	movzbl	%bh, %ebx
	shrl	$16, %r10d
	shrl	$24, %r13d
	movl	(%rax,%r11,4), %r11d
	movzbl	%r10b, %r10d
	xorl	3072(%rax,%r13,4), %r11d
	xorl	1024(%rax,%rbx,4), %r11d
	xorl	2048(%rax,%r10,4), %r11d
	addl	%r11d, %r12d
	movl	%r12d, %r13d
	movzbl	%r12b, %ebx
	movl	%r12d, %r10d
	shrl	$24, %r13d
	movl	(%rax,%rbx,4), %ebx
	shrl	$16, %r10d
	xorl	3072(%rax,%r13,4), %ebx
	movzbl	%r10b, %r10d
	movl	%ebx, %r15d
	movl	%r12d, %ebx
	movzbl	%bh, %ebx
	movl	1024(%rax,%rbx,4), %ebx
	xorl	%r15d, %ebx
	xorl	2048(%rax,%r10,4), %ebx
	movl	64(%rdx), %r10d
	addl	%ebx, %r11d
	xorl	%ebx, %ecx
	movl	68(%rdx), %ebx
	xorl	%r11d, %r8d
	xorl	%r8d, %r10d
	xorl	%ecx, %ebx
	xorl	%r10d, %ebx
	movl	%ebx, %r11d
	movl	%ebx, %r13d
	movzbl	%bl, %r12d
	movzbl	%bh, %ebx
	shrl	$16, %r11d
	shrl	$24, %r13d
	movl	(%rax,%r12,4), %r12d
	movzbl	%r11b, %r11d
	xorl	3072(%rax,%r13,4), %r12d
	xorl	1024(%rax,%rbx,4), %r12d
	movl	2048(%rax,%r11,4), %ebx
	xorl	%r12d, %ebx
	movl	%ebx, %r12d
	leal	(%r10,%rbx), %ebx
	movl	%ebx, %r10d
	movl	%ebx, %r13d
	movzbl	%bl, %r11d
	movzbl	%bh, %ebx
	shrl	$16, %r10d
	shrl	$24, %r13d
	movl	(%rax,%r11,4), %r11d
	movzbl	%r10b, %r10d
	xorl	3072(%rax,%r13,4), %r11d
	xorl	1024(%rax,%rbx,4), %r11d
	xorl	2048(%rax,%r10,4), %r11d
	leal	(%r12,%r11), %ebx
	movl	%ebx, %r12d
	movl	%ebx, %r13d
	movzbl	%bl, %r10d
	movzbl	%bh, %ebx
	shrl	$16, %r12d
	shrl	$24, %r13d
	movl	(%rax,%r10,4), %r10d
	movzbl	%r12b, %r12d
	xorl	3072(%rax,%r13,4), %r10d
	xorl	1024(%rax,%rbx,4), %r10d
	xorl	2048(%rax,%r12,4), %r10d
	movl	60(%rdx), %ebx
	addl	%r10d, %r11d
	xorl	%r10d, %edi
	movl	56(%rdx), %r10d
	xorl	%r11d, %r9d
	xorl	%edi, %ebx
	xorl	%r9d, %r10d
	xorl	%r10d, %ebx
	movl	%ebx, %r11d
	movl	%ebx, %r13d
	movzbl	%bl, %r12d
	movzbl	%bh, %ebx
	shrl	$16, %r11d
	shrl	$24, %r13d
	movl	(%rax,%r12,4), %r12d
	movzbl	%r11b, %r11d
	xorl	3072(%rax,%r13,4), %r12d
	xorl	1024(%rax,%rbx,4), %r12d
	xorl	2048(%rax,%r11,4), %r12d
	leal	(%r10,%r12), %ebx
	movl	%ebx, %r10d
	movl	%ebx, %r13d
	movzbl	%bl, %r11d
	movzbl	%bh, %ebx
	shrl	$16, %r10d
	shrl	$24, %r13d
	movl	(%rax,%r11,4), %r11d
	movzbl	%r10b, %r10d
	xorl	3072(%rax,%r13,4), %r11d
	xorl	1024(%rax,%rbx,4), %r11d
	xorl	2048(%rax,%r10,4), %r11d
	addl	%r11d, %r12d
	movl	%r12d, %r13d
	movzbl	%r12b, %ebx
	movl	%r12d, %r10d
	shrl	$24, %r13d
	movl	(%rax,%rbx,4), %ebx
	shrl	$16, %r10d
	xorl	3072(%rax,%r13,4), %ebx
	movzbl	%r10b, %r10d
	movl	%ebx, %r14d
	movl	%r12d, %ebx
	movzbl	%bh, %ebx
	movl	1024(%rax,%rbx,4), %ebx
	xorl	%r14d, %ebx
	xorl	2048(%rax,%r10,4), %ebx
	movl	48(%rdx), %r10d
	addl	%ebx, %r11d
	xorl	%r11d, %r8d
	xorl	%ebx, %ecx
	movl	52(%rdx), %ebx
	xorl	%r8d, %r10d
	xorl	%ecx, %ebx
	xorl	%r10d, %ebx
	movl	%ebx, %r11d
	movl	%ebx, %r13d
	movzbl	%bl, %r12d
	movzbl	%bh, %ebx
	shrl	$16, %r11d
	shrl	$24, %r13d
	movl	(%rax,%r12,4), %r12d
	movzbl	%r11b, %r11d
	xorl	3072(%rax,%r13,4), %r12d
	xorl	1024(%rax,%rbx,4), %r12d
	movl	2048(%rax,%r11,4), %ebx
	xorl	%r12d, %ebx
	movl	%ebx, %r12d
	leal	(%r10,%rbx), %ebx
	movl	%ebx, %r10d
	movl	%ebx, %r13d
	movzbl	%bl, %r11d
	movzbl	%bh, %ebx
	shrl	$16, %r10d
	shrl	$24, %r13d
	movl	(%rax,%r11,4), %r11d
	movzbl	%r10b, %r10d
	xorl	3072(%rax,%r13,4), %r11d
	xorl	1024(%rax,%rbx,4), %r11d
	xorl	2048(%rax,%r10,4), %r11d
	leal	(%r12,%r11), %ebx
	movl	%ebx, %r12d
	movl	%ebx, %r13d
	movzbl	%bl, %r10d
	movzbl	%bh, %ebx
	shrl	$16, %r12d
	shrl	$24, %r13d
	movl	(%rax,%r10,4), %r10d
	movzbl	%r12b, %r12d
	xorl	3072(%rax,%r13,4), %r10d
	xorl	1024(%rax,%rbx,4), %r10d
	xorl	2048(%rax,%r12,4), %r10d
	movl	44(%rdx), %ebx
	addl	%r10d, %r11d
	xorl	%r10d, %edi
	movl	40(%rdx), %r10d
	xorl	%r11d, %r9d
	xorl	%edi, %ebx
	xorl	%r9d, %r10d
	xorl	%r10d, %ebx
	movl	%ebx, %r11d
	movl	%ebx, %r13d
	movzbl	%bl, %r12d
	movzbl	%bh, %ebx
	shrl	$16, %r11d
	shrl	$24, %r13d
	movl	(%rax,%r12,4), %r12d
	movzbl	%r11b, %r11d
	xorl	3072(%rax,%r13,4), %r12d
	xorl	1024(%rax,%rbx,4), %r12d
	xorl	2048(%rax,%r11,4), %r12d
	leal	(%r10,%r12), %ebx
	movl	%ebx, %r11d
	movl	%ebx, %r13d
	movzbl	%bl, %r10d
	movzbl	%bh, %ebx
	shrl	$16, %r11d
	shrl	$24, %r13d
	movl	(%rax,%r10,4), %r10d
	movzbl	%r11b, %r11d
	xorl	3072(%rax,%r13,4), %r10d
	xorl	1024(%rax,%rbx,4), %r10d
	xorl	2048(%rax,%r11,4), %r10d
	addl	%r10d, %r12d
	movl	%r12d, %r13d
	movzbl	%r12b, %ebx
	movl	%r12d, %r11d
	shrl	$24, %r13d
	movl	(%rax,%rbx,4), %ebx
	shrl	$16, %r11d
	xorl	3072(%rax,%r13,4), %ebx
	movzbl	%r11b, %r11d
	movl	%ebx, %r15d
	movl	%r12d, %ebx
	movzbl	%bh, %ebx
	movl	1024(%rax,%rbx,4), %ebx
	xorl	%r15d, %ebx
	xorl	2048(%rax,%r11,4), %ebx
	addl	%ebx, %r10d
	xorl	%ebx, %ecx
	movl	36(%rdx), %ebx
	movl	%r10d, %r11d
	xorl	%r8d, %r11d
	movl	32(%rdx), %r8d
	xorl	%ecx, %ebx
	xorl	%r11d, %r8d
	xorl	%r8d, %ebx
	movl	%ebx, %r10d
	movl	%ebx, %r13d
	movzbl	%bl, %r12d
	movzbl	%bh, %ebx
	shrl	$16, %r10d
	shrl	$24, %r13d
	movl	(%rax,%r12,4), %r12d
	movzbl	%r10b, %r10d
	xorl	3072(%rax,%r13,4), %r12d
	xorl	1024(%rax,%rbx,4), %r12d
	movl	2048(%rax,%r10,4), %ebx
	xorl	%r12d, %ebx
	movl	%ebx, %r12d
	leal	(%r8,%rbx), %ebx
	movl	%ebx, %r8d
	movl	%ebx, %r13d
	movzbl	%bl, %r10d
	movzbl	%bh, %ebx
	shrl	$16, %r8d
	shrl	$24, %r13d
	movl	(%rax,%r10,4), %r10d
	movzbl	%r8b, %r8d
	xorl	3072(%rax,%r13,4), %r10d
	xorl	1024(%rax,%rbx,4), %r10d
	xorl	2048(%rax,%r8,4), %r10d
	leal	(%r12,%r10), %ebx
	movl	%ebx, %r12d
	movl	%ebx, %r13d
	movzbl	%bl, %r8d
	movzbl	%bh, %ebx
	shrl	$16, %r12d
	shrl	$24, %r13d
	movl	(%rax,%r8,4), %r8d
	movzbl	%r12b, %r12d
	xorl	3072(%rax,%r13,4), %r8d
	xorl	1024(%rax,%rbx,4), %r8d
	xorl	2048(%rax,%r12,4), %r8d
	movl	28(%rdx), %ebx
	addl	%r8d, %r10d
	xorl	%r10d, %r9d
	xorl	%r8d, %edi
	movl	24(%rdx), %r8d
	xorl	%edi, %ebx
	xorl	%r9d, %r8d
	xorl	%r8d, %ebx
	movl	%ebx, %r10d
	movl	%ebx, %r13d
	movzbl	%bl, %r12d
	movzbl	%bh, %ebx
	shrl	$16, %r10d
	shrl	$24, %r13d
	movl	(%rax,%r12,4), %r12d
	movzbl	%r10b, %r10d
	xorl	3072(%rax,%r13,4), %r12d
	xorl	1024(%rax,%rbx,4), %r12d
	xorl	2048(%rax,%r10,4), %r12d
	leal	(%r8,%r12), %ebx
	movl	%ebx, %r10d
	movl	%ebx, %r13d
	movzbl	%bl, %r8d
	movzbl	%bh, %ebx
	shrl	$16, %r10d
	shrl	$24, %r13d
	movl	(%rax,%r8,4), %r8d
	movzbl	%r10b, %r10d
	xorl	3072(%rax,%r13,4), %r8d
	xorl	1024(%rax,%rbx,4), %r8d
	xorl	2048(%rax,%r10,4), %r8d
	addl	%r8d, %r12d
	movl	%r12d, %r13d
	movzbl	%r12b, %ebx
	movl	%r12d, %r10d
	shrl	$24, %r13d
	movl	(%rax,%rbx,4), %ebx
	shrl	$16, %r10d
	xorl	3072(%rax,%r13,4), %ebx
	movzbl	%r10b, %r10d
	movl	%ebx, %r14d
	movl	%r12d, %ebx
	movzbl	%bh, %ebx
	movl	1024(%rax,%rbx,4), %ebx
	xorl	%r14d, %ebx
	xorl	2048(%rax,%r10,4), %ebx
	addl	%ebx, %r8d
	xorl	%ebx, %ecx
	xorl	%r11d, %r8d
	movl	%r8d, %r10d
	xorl	16(%rdx), %r8d
	movl	20(%rdx), %ebx
	xorl	%ecx, %ebx
	xorl	%r8d, %ebx
	movl	%ebx, %r11d
	movl	%ebx, %r13d
	movzbl	%bl, %r12d
	movzbl	%bh, %ebx
	shrl	$16, %r11d
	shrl	$24, %r13d
	movl	(%rax,%r12,4), %r12d
	movzbl	%r11b, %r11d
	xorl	3072(%rax,%r13,4), %r12d
	xorl	1024(%rax,%rbx,4), %r12d
	movl	2048(%rax,%r11,4), %ebx
	xorl	%r12d, %ebx
	movl	%ebx, %r12d
	leal	(%r8,%rbx), %ebx
	movl	%ebx, %r8d
	movl	%ebx, %r13d
	movzbl	%bl, %r11d
	movzbl	%bh, %ebx
	shrl	$16, %r8d
	shrl	$24, %r13d
	movl	(%rax,%r11,4), %r11d
	movzbl	%r8b, %r8d
	xorl	3072(%rax,%r13,4), %r11d
	xorl	1024(%rax,%rbx,4), %r11d
	xorl	2048(%rax,%r8,4), %r11d
	leal	(%r12,%r11), %ebx
	movl	%ebx, %r12d
	movl	%ebx, %r13d
	movzbl	%bl, %r8d
	movzbl	%bh, %ebx
	shrl	$16, %r12d
	shrl	$24, %r13d
	movl	(%rax,%r8,4), %r8d
	movzbl	%r12b, %r12d
	xorl	3072(%rax,%r13,4), %r8d
	xorl	1024(%rax,%rbx,4), %r8d
	xorl	2048(%rax,%r12,4), %r8d
	movl	12(%rdx), %ebx
	addl	%r8d, %r11d
	xorl	%r8d, %edi
	movl	8(%rdx), %r8d
	xorl	%r11d, %r9d
	xorl	%edi, %ebx
	xorl	%r9d, %r8d
	xorl	%r8d, %ebx
	movl	%ebx, %r12d
	movl	%ebx, %r13d
	movzbl	%bl, %r11d
	movzbl	%bh, %ebx
	shrl	$16, %r12d
	shrl	$24, %r13d
	movl	(%rax,%r11,4), %r11d
	movzbl	%r12b, %r12d
	xorl	3072(%rax,%r13,4), %r11d
	xorl	1024(%rax,%rbx,4), %r11d
	movl	2048(%rax,%r12,4), %ebx
	xorl	%r11d, %ebx
	movl	%ebx, %r11d
	leal	(%r8,%rbx), %ebx
	movl	%ebx, %r12d
	movl	%ebx, %r13d
	movzbl	%bl, %r8d
	movzbl	%bh, %ebx
	shrl	$16, %r12d
	shrl	$24, %r13d
	movl	(%rax,%r8,4), %r8d
	movzbl	%r12b, %r12d
	xorl	3072(%rax,%r13,4), %r8d
	xorl	1024(%rax,%rbx,4), %r8d
	xorl	2048(%rax,%r12,4), %r8d
	leal	(%r11,%r8), %ebx
	movl	%ebx, %r12d
	movl	%ebx, %r13d
	movzbl	%bl, %r11d
	movzbl	%bh, %ebx
	shrl	$16, %r12d
	shrl	$24, %r13d
	movl	(%rax,%r11,4), %r11d
	movzbl	%r12b, %r12d
	xorl	3072(%rax,%r13,4), %r11d
	xorl	1024(%rax,%rbx,4), %r11d
	xorl	2048(%rax,%r12,4), %r11d
	movl	4(%rdx), %ebx
	addl	%r11d, %r8d
	xorl	%r10d, %r8d
	xorl	%r11d, %ecx
	movl	(%rdx), %r11d
	xorl	%ecx, %ebx
	bswap	%ecx
	xorl	%r8d, %r11d
	movl	%ebx, %edx
	bswap	%r8d
	xorl	%r11d, %edx
	movl	%edx, %r10d
	movl	%edx, %r12d
	movzbl	%dl, %ebx
	movzbl	%dh, %edx
	shrl	$16, %r10d
	shrl	$24, %r12d
	movl	(%rax,%rbx,4), %ebx
	movzbl	%r10b, %r10d
	xorl	3072(%rax,%r12,4), %ebx
	xorl	1024(%rax,%rdx,4), %ebx
	movl	%r8d, 8(%rsi)
	xorl	2048(%rax,%r10,4), %ebx
	movl	%ecx, 12(%rsi)
	movl	%ebx, %r10d
	leal	(%r11,%rbx), %ebx
	movl	%ebx, %r11d
	movl	%ebx, %r12d
	movzbl	%bl, %edx
	movzbl	%bh, %ebx
	shrl	$16, %r11d
	shrl	$24, %r12d
	movl	(%rax,%rdx,4), %edx
	movzbl	%r11b, %r11d
	xorl	3072(%rax,%r12,4), %edx
	xorl	1024(%rax,%rbx,4), %edx
	xorl	2048(%rax,%r11,4), %edx
	leal	(%r10,%rdx), %ebx
	movl	%edx, %r11d
	movl	%ebx, %r10d
	movl	%ebx, %r12d
	movzbl	%bl, %edx
	movzbl	%bh, %ebx
	shrl	$16, %r10d
	shrl	$24, %r12d
	movl	(%rax,%rdx,4), %edx
	movzbl	%r10b, %r10d
	xorl	3072(%rax,%r12,4), %edx
	xorl	1024(%rax,%rbx,4), %edx
	xorl	2048(%rax,%r10,4), %edx
	popq	%rbx
	movl	%edx, %eax
	leal	(%r11,%rdx), %edx
	popq	%r12
	popq	%r13
	xorl	%r9d, %edx
	xorl	%eax, %edi
	popq	%r14
	popq	%r15
	bswap	%edx
	bswap	%edi
	movl	%edx, (%rsi)
	popq	%rbp
	.cfi_def_cfa 7, 8
	movl	%edi, 4(%rsi)
	ret
	.cfi_endproc
.LFE153:
	.size	SEED_decrypt, .-SEED_decrypt
	.section	.rodata
	.align 32
	.type	SS, @object
	.size	SS, 4096
SS:
	.long	696885672
	.long	92635524
	.long	382128852
	.long	331600848
	.long	340021332
	.long	487395612
	.long	747413676
	.long	621093156
	.long	491606364
	.long	54739776
	.long	403181592
	.long	504238620
	.long	289493328
	.long	1020063996
	.long	181060296
	.long	591618912
	.long	671621160
	.long	71581764
	.long	536879136
	.long	495817116
	.long	549511392
	.long	583197408
	.long	147374280
	.long	386339604
	.long	629514660
	.long	261063564
	.long	50529024
	.long	994800504
	.long	999011256
	.long	318968592
	.long	314757840
	.long	785310444
	.long	809529456
	.long	210534540
	.long	1057960764
	.long	680042664
	.long	839004720
	.long	500027868
	.long	919007988
	.long	876900468
	.long	751624428
	.long	361075092
	.long	185271048
	.long	390550356
	.long	474763356
	.long	457921368
	.long	1032696252
	.long	16843008
	.long	604250148
	.long	470552604
	.long	860058480
	.long	411603096
	.long	268439568
	.long	214745292
	.long	851636976
	.long	432656856
	.long	738992172
	.long	667411428
	.long	843215472
	.long	58950528
	.long	462132120
	.long	297914832
	.long	109478532
	.long	164217288
	.long	541089888
	.long	272650320
	.long	595829664
	.long	734782440
	.long	218956044
	.long	914797236
	.long	512660124
	.long	256852812
	.long	931640244
	.long	441078360
	.long	113689284
	.long	944271480
	.long	646357668
	.long	302125584
	.long	797942700
	.long	365285844
	.long	557932896
	.long	63161280
	.long	881111220
	.long	21053760
	.long	306336336
	.long	1028485500
	.long	227377548
	.long	134742024
	.long	521081628
	.long	428446104
	.long	0
	.long	420024600
	.long	67371012
	.long	323179344
	.long	935850996
	.long	566354400
	.long	1036907004
	.long	910586484
	.long	789521196
	.long	654779172
	.long	813740208
	.long	193692552
	.long	235799052
	.long	730571688
	.long	578986656
	.long	776888940
	.long	327390096
	.long	223166796
	.long	692674920
	.long	1011642492
	.long	151585032
	.long	168428040
	.long	1066382268
	.long	802153452
	.long	868479984
	.long	96846276
	.long	126321540
	.long	335810580
	.long	1053750012
	.long	608460900
	.long	516870876
	.long	772678188
	.long	189481800
	.long	436867608
	.long	101057028
	.long	553722144
	.long	726360936
	.long	642146916
	.long	33686016
	.long	902164980
	.long	310547088
	.long	176849544
	.long	202113036
	.long	864269232
	.long	1045328508
	.long	281071824
	.long	977957496
	.long	122110788
	.long	377918100
	.long	633725412
	.long	637936164
	.long	8421504
	.long	764256684
	.long	533713884
	.long	562143648
	.long	805318704
	.long	923218740
	.long	781099692
	.long	906375732
	.long	352653588
	.long	570565152
	.long	940060728
	.long	885321972
	.long	663200676
	.long	88424772
	.long	206323788
	.long	25264512
	.long	701096424
	.long	75792516
	.long	394761108
	.long	889532724
	.long	197903304
	.long	248431308
	.long	1007431740
	.long	826372464
	.long	285282576
	.long	130532292
	.long	160006536
	.long	893743476
	.long	1003222008
	.long	449499864
	.long	952692984
	.long	344232084
	.long	424235352
	.long	42107520
	.long	80003268
	.long	1070593020
	.long	155795784
	.long	956903736
	.long	658989924
	.long	12632256
	.long	265274316
	.long	398971860
	.long	948482232
	.long	252642060
	.long	244220556
	.long	37896768
	.long	587408160
	.long	293704080
	.long	743202924
	.long	466342872
	.long	612671652
	.long	872689716
	.long	834793968
	.long	138952776
	.long	46318272
	.long	793731948
	.long	1024274748
	.long	755835180
	.long	4210752
	.long	1049539260
	.long	1041117756
	.long	1015853244
	.long	29475264
	.long	713728680
	.long	982168248
	.long	240009804
	.long	356864340
	.long	990589752
	.long	483184860
	.long	675831912
	.long	1062171516
	.long	478974108
	.long	415813848
	.long	172638792
	.long	373707348
	.long	927429492
	.long	545300640
	.long	768467436
	.long	105267780
	.long	897954228
	.long	722150184
	.long	625303908
	.long	986379000
	.long	600040416
	.long	965325240
	.long	830583216
	.long	529503132
	.long	508449372
	.long	969535992
	.long	650568420
	.long	847426224
	.long	822161712
	.long	717939432
	.long	760045932
	.long	525292380
	.long	616882404
	.long	817950960
	.long	231588300
	.long	143163528
	.long	369496596
	.long	973746744
	.long	407392344
	.long	348442836
	.long	574775904
	.long	688464168
	.long	117900036
	.long	855847728
	.long	684253416
	.long	453710616
	.long	84214020
	.long	961114488
	.long	276861072
	.long	709517928
	.long	705307176
	.long	445289112
	.long	943196208
	.long	-399980320
	.long	741149985
	.long	-1540979038
	.long	-871379005
	.long	-601960750
	.long	-1338801229
	.long	-1204254544
	.long	-1406169181
	.long	1612726368
	.long	1410680145
	.long	-1006123069
	.long	1141130304
	.long	1815039843
	.long	1747667811
	.long	1478183763
	.long	-1073495101
	.long	1612857954
	.long	808649523
	.long	-1271560783
	.long	673777953
	.long	-1608482656
	.long	-534592798
	.long	-1540913245
	.long	-804011053
	.long	-1877900911
	.long	269549841
	.long	67503618
	.long	471600144
	.long	-1136882512
	.long	875955762
	.long	1208699715
	.long	-332410909
	.long	-2012706688
	.long	1814842464
	.long	-1473738592
	.long	337053459
	.long	-1006320448
	.long	336987666
	.long	-197868304
	.long	-1073560894
	.long	1141196097
	.long	-534658591
	.long	-736704814
	.long	1010765619
	.long	1010634033
	.long	-1945203070
	.long	-1743222640
	.long	673712160
	.long	1276005954
	.long	-197736718
	.long	1010699826
	.long	-1541044831
	.long	-130430479
	.long	202181889
	.long	-601894957
	.long	-669464368
	.long	673909539
	.long	1680229986
	.long	2017086066
	.long	606537507
	.long	741281571
	.long	-265174543
	.long	1882342002
	.long	1073889858
	.long	-736836400
	.long	1073824065
	.long	-1073692480
	.long	1882407795
	.long	1680295779
	.long	-1406366560
	.long	-2012509309
	.long	-197670925
	.long	-1406300767
	.long	-2147450752
	.long	471797523
	.long	-938816830
	.long	741084192
	.long	-1473607006
	.long	875824176
	.long	-804076846
	.long	134941443
	.long	-332476702
	.long	-399914527
	.long	1545424209
	.long	-1810594672
	.long	404228112
	.long	-130496272
	.long	1410811731
	.long	-1406234974
	.long	134744064
	.long	-1006254655
	.long	269681427
	.long	-871510591
	.long	-2079947134
	.long	-1204188751
	.long	-62926861
	.long	2084392305
	.long	-1073626687
	.long	808517937
	.long	-197802511
	.long	-2012575102
	.long	1747602018
	.long	-1338932815
	.long	-804142639
	.long	538968096
	.long	-736639021
	.long	131586
	.long	539099682
	.long	67372032
	.long	1747470432
	.long	1882276209
	.long	67569411
	.long	-669266989
	.long	-1675784815
	.long	-1743156847
	.long	1612792161
	.long	-1136750926
	.long	-467220766
	.long	1478052177
	.long	-602026543
	.long	1343308113
	.long	-1877966704
	.long	-602092336
	.long	-1743091054
	.long	-1608285277
	.long	-1473541213
	.long	-804208432
	.long	-2147384959
	.long	202313475
	.long	1141327683
	.long	404359698
	.long	-534527005
	.long	-332608288
	.long	-1945268863
	.long	-1136685133
	.long	-1810463086
	.long	2017151859
	.long	1545358416
	.long	-1608351070
	.long	-1608416863
	.long	1612923747
	.long	539165475
	.long	1275940161
	.long	-938948416
	.long	-1675719022
	.long	-1675850608
	.long	943327794
	.long	202116096
	.long	741215778
	.long	-1204122958
	.long	1814974050
	.long	-1675653229
	.long	1478117970
	.long	-265108750
	.long	-1877835118
	.long	-265042957
	.long	1208568129
	.long	2016954480
	.long	-871576384
	.long	336921873
	.long	-130298893
	.long	1882210416
	.long	1949648241
	.long	2084523891
	.long	875889969
	.long	269484048
	.long	197379
	.long	1680098400
	.long	1814908257
	.long	-1006188862
	.long	1949582448
	.long	-736770607
	.long	-1271626576
	.long	-399848734
	.long	134809857
	.long	1949714034
	.long	404293905
	.long	-62992654
	.long	1073758272
	.long	269615634
	.long	-534724384
	.long	-1136816719
	.long	67437825
	.long	-130364686
	.long	65793
	.long	-265240336
	.long	673843746
	.long	1545490002
	.long	-1473672799
	.long	1410745938
	.long	1073955651
	.long	-2080012927
	.long	336856080
	.long	-2012640895
	.long	-1743025261
	.long	-1338998608
	.long	-467286559
	.long	1208502336
	.long	2017020273
	.long	-1810397293
	.long	-63124240
	.long	471731730
	.long	-2147319166
	.long	539033889
	.long	-1945334656
	.long	404425491
	.long	1545555795
	.long	1949779827
	.long	1410614352
	.long	-1338867022
	.long	471665937
	.long	606405921
	.long	1276071747
	.long	0
	.long	1141261890
	.long	-332542495
	.long	1477986384
	.long	1343373906
	.long	-399782941
	.long	2084458098
	.long	-669332782
	.long	-938882623
	.long	-63058447
	.long	808452144
	.long	-1810528879
	.long	1680164193
	.long	1010568240
	.long	-1271494990
	.long	-467352352
	.long	-1204057165
	.long	2084326512
	.long	202247682
	.long	1343242320
	.long	943262001
	.long	606471714
	.long	808583730
	.long	-2080078720
	.long	1747536225
	.long	-1877769325
	.long	876021555
	.long	-467154973
	.long	606340128
	.long	-1541110624
	.long	-938751037
	.long	1343439699
	.long	134875650
	.long	-2079881341
	.long	-669398575
	.long	1275874368
	.long	-2147253373
	.long	-1945137277
	.long	-871444798
	.long	943393587
	.long	1208633922
	.long	-1271429197
	.long	-1582814839
	.long	-2122054267
	.long	-757852474
	.long	-741338173
	.long	1347687492
	.long	287055117
	.long	-1599329140
	.long	556016901
	.long	1364991309
	.long	1128268611
	.long	270014472
	.long	303832590
	.long	1364201793
	.long	-251904820
	.long	-1027077430
	.long	1667244867
	.long	539502600
	.long	1078199364
	.long	538976256
	.long	-1852039795
	.long	-522182464
	.long	-488627518
	.long	-1060632376
	.long	320083719
	.long	-1583078011
	.long	-2087972977
	.long	50332419
	.long	1937259339
	.long	-1279771765
	.long	319820547
	.long	-758115646
	.long	-487838002
	.long	1886400576
	.long	-2138305396
	.long	859586319
	.long	-1599592312
	.long	842019330
	.long	-774103603
	.long	-218876218
	.long	1886663748
	.long	-521392948
	.long	-1852566139
	.long	50858763
	.long	1398019911
	.long	1348213836
	.long	1398283083
	.long	-1313063539
	.long	16777473
	.long	539239428
	.long	270277644
	.long	1936732995
	.long	-1869080440
	.long	269488128
	.long	-1060369204
	.long	-219139390
	.long	-774366775
	.long	539765772
	.long	-471586873
	.long	1919955522
	.long	-2088762493
	.long	-1818748021
	.long	-774893119
	.long	-2105276794
	.long	-1043854903
	.long	1616912448
	.long	1347424320
	.long	-1549786237
	.long	-471323701
	.long	17566989
	.long	-1296812410
	.long	-1835262322
	.long	1129058127
	.long	-1280034937
	.long	1381505610
	.long	-1027340602
	.long	1886926920
	.long	-1566300538
	.long	303043074
	.long	-1548996721
	.long	-774629947
	.long	1633689921
	.long	-1010826301
	.long	-1330367356
	.long	1094713665
	.long	1380979266
	.long	1903967565
	.long	-2121527923
	.long	526344
	.long	320610063
	.long	-1852302967
	.long	0
	.long	286791945
	.long	263172
	.long	1397756739
	.long	-202098745
	.long	-505404991
	.long	-235127347
	.long	1920218694
	.long	590098191
	.long	589571847
	.long	-1330630528
	.long	-2088236149
	.long	34344462
	.long	-1549259893
	.long	-1566563710
	.long	1651256910
	.long	-1819274365
	.long	1095503181
	.long	1634216265
	.long	1887190092
	.long	17303817
	.long	34081290
	.long	-1279508593
	.long	-471060529
	.long	-202361917
	.long	-1044118075
	.long	-2088499321
	.long	269751300
	.long	-218349874
	.long	1617175620
	.long	-757326130
	.long	573320718
	.long	1128794955
	.long	303569418
	.long	33818118
	.long	555753729
	.long	1667771211
	.long	1650730566
	.long	33554946
	.long	-235653691
	.long	-1836051838
	.long	-2105013622
	.long	789516
	.long	-1280298109
	.long	1920745038
	.long	-791670592
	.long	1920481866
	.long	1128531783
	.long	-1835788666
	.long	-505141819
	.long	572794374
	.long	-2139094912
	.long	-1582551667
	.long	-740548657
	.long	-1583341183
	.long	808464384
	.long	859059975
	.long	-1565774194
	.long	842282502
	.long	286528773
	.long	572531202
	.long	808990728
	.long	-252431164
	.long	-1549523065
	.long	1094976837
	.long	1078725708
	.long	-2122317439
	.long	-504878647
	.long	-2138831740
	.long	-1819011193
	.long	825505029
	.long	-1010299957
	.long	-1026814258
	.long	809253900
	.long	1903178049
	.long	286265601
	.long	-1010563129
	.long	-2121791095
	.long	1903441221
	.long	-201835573
	.long	-757589302
	.long	-252167992
	.long	-1869343612
	.long	1364728137
	.long	-2105539966
	.long	-1060895548
	.long	-201572401
	.long	1095240009
	.long	825768201
	.long	1667508039
	.long	-1061158720
	.long	-1010036785
	.long	-741075001
	.long	-1330104184
	.long	51121935
	.long	-2104750450
	.long	1111491138
	.long	589308675
	.long	-1852829311
	.long	1617701964
	.long	-740811829
	.long	-1599855484
	.long	808727556
	.long	-235916863
	.long	1078462536
	.long	-1027603774
	.long	1668034383
	.long	826031373
	.long	556543245
	.long	1077936192
	.long	-1296286066
	.long	842808846
	.long	-1329841012
	.long	-1044381247
	.long	-1566037366
	.long	-1296549238
	.long	1112280654
	.long	1364464965
	.long	859323147
	.long	-790881076
	.long	1617438792
	.long	1937522511
	.long	-1868817268
	.long	-791144248
	.long	1112017482
	.long	1381242438
	.long	1936996167
	.long	-1600118656
	.long	-504615475
	.long	1111754310
	.long	-1313589883
	.long	589835019
	.long	1633953093
	.long	-218613046
	.long	-471850045
	.long	-1313326711
	.long	-1313853055
	.long	-1818484849
	.long	1381768782
	.long	-235390519
	.long	-488364346
	.long	-1297075582
	.long	825241857
	.long	-488101174
	.long	1634479437
	.long	1398546255
	.long	-521919292
	.long	-252694336
	.long	-1043591731
	.long	-2138568568
	.long	303306246
	.long	842545674
	.long	1347950664
	.long	-791407420
	.long	1650467394
	.long	556280073
	.long	50595591
	.long	858796803
	.long	-521656120
	.long	320346891
	.long	17040645
	.long	1903704393
	.long	-1869606784
	.long	1650993738
	.long	573057546
	.long	-1835525494
	.long	137377848
	.long	-924784600
	.long	220277805
	.long	-2036161498
	.long	-809251825
	.long	-825041890
	.long	-2085375949
	.long	-2001684424
	.long	-1885098961
	.long	1080057888
	.long	1162957845
	.long	-943471609
	.long	1145062404
	.long	1331915823
	.long	1264805931
	.long	1263753243
	.long	-1010581501
	.long	1113743394
	.long	53686323
	.long	-2051951563
	.long	153167913
	.long	-2136956896
	.long	-1025318878
	.long	-2019318745
	.long	-1009528813
	.long	-2121166831
	.long	17895441
	.long	100795398
	.long	202382364
	.long	-1934574532
	.long	103953462
	.long	1262700555
	.long	-807146449
	.long	-2004842488
	.long	1281387564
	.long	-2002737112
	.long	118690839
	.long	-993999868
	.long	101848086
	.long	-990841804
	.long	-1027424254
	.long	1161905157
	.long	-1042161631
	.long	-959261674
	.long	255015999
	.long	221330493
	.long	-1904047090
	.long	-2003789800
	.long	136325160
	.long	1312967694
	.long	-957156298
	.long	238173246
	.long	-2053004251
	.long	-906889159
	.long	218172429
	.long	-808199137
	.long	-925837288
	.long	186853419
	.long	1180853286
	.long	1249015866
	.long	119743527
	.long	253963311
	.long	-1041108943
	.long	1114796082
	.long	1111638018
	.long	-992947180
	.long	1094795265
	.long	-1061109760
	.long	1131638835
	.long	1197696039
	.long	-1935627220
	.long	-1954314229
	.long	-940313545
	.long	-1918784467
	.long	-2139062272
	.long	252910623
	.long	-893204470
	.long	203435052
	.long	-1969051606
	.long	70267956
	.long	-1026371566
	.long	184748043
	.long	-823989202
	.long	-907941847
	.long	1297177629
	.long	-2070899692
	.long	135272472
	.long	-923731912
	.long	1196643351
	.long	-1901941714
	.long	134219784
	.long	-977157115
	.long	51580947
	.long	-842937331
	.long	-2038266874
	.long	-1984841671
	.long	-806093761
	.long	1299283005
	.long	-1044267007
	.long	20000817
	.long	-973999051
	.long	-1971156982
	.long	1247963178
	.long	-2119061455
	.long	-1043214319
	.long	2105376
	.long	-942418921
	.long	33685506
	.long	35790882
	.long	67109892
	.long	1214277672
	.long	1097953329
	.long	117638151
	.long	-875309029
	.long	-1919837155
	.long	-1986947047
	.long	1096900641
	.long	-1900889026
	.long	-958208986
	.long	1230067737
	.long	-841884643
	.long	1095847953
	.long	-2138009584
	.long	-858727396
	.long	-1970104294
	.long	-2086428637
	.long	-1952208853
	.long	-1060057072
	.long	-2122219519
	.long	251857935
	.long	1195590663
	.long	168957978
	.long	-1008476125
	.long	-857674708
	.long	-1920889843
	.long	-1884046273
	.long	-2037214186
	.long	1265858619
	.long	1280334876
	.long	-2103271390
	.long	-2120114143
	.long	1130586147
	.long	52633635
	.long	1296124941
	.long	-926889976
	.long	-1902994402
	.long	-1936679908
	.long	171063354
	.long	201329676
	.long	237120558
	.long	-1967998918
	.long	1315073070
	.long	-1886151649
	.long	1246910490
	.long	-1024266190
	.long	-2104324078
	.long	-1007423437
	.long	1229015049
	.long	1215330360
	.long	-859780084
	.long	85005333
	.long	-873203653
	.long	1081110576
	.long	1165063221
	.long	1332968511
	.long	87110709
	.long	1052688
	.long	50528259
	.long	1147167780
	.long	1298230317
	.long	-960314362
	.long	1148220468
	.long	-976104427
	.long	-2068794316
	.long	-891099094
	.long	151062537
	.long	1181905974
	.long	152115225
	.long	-822936514
	.long	1077952512
	.long	34738194
	.long	-1059004384
	.long	-1917731779
	.long	83952645
	.long	-890046406
	.long	16842753
	.long	-1057951696
	.long	170010666
	.long	1314020382
	.long	-1985894359
	.long	1179800598
	.long	1128480771
	.long	-2055109627
	.long	68162580
	.long	-1987999735
	.long	-1953261541
	.long	-2135904208
	.long	-975051739
	.long	1212172296
	.long	1232173113
	.long	-2020371433
	.long	-856622020
	.long	236067870
	.long	-2105376766
	.long	18948129
	.long	-1937732596
	.long	185800731
	.long	1330863135
	.long	1198748727
	.long	1146115092
	.long	-2102218702
	.long	219225117
	.long	86058021
	.long	1329810447
	.long	0
	.long	1178747910
	.long	-840831955
	.long	1213224984
	.long	1112690706
	.long	-874256341
	.long	1316125758
	.long	-892151782
	.long	-910047223
	.long	-839779267
	.long	3158064
	.long	-2054056939
	.long	1164010533
	.long	204487740
	.long	-2035108810
	.long	-991894492
	.long	-1951156165
	.long	1282440252
	.long	235015182
	.long	1079005200
	.long	154220601
	.long	102900774
	.long	36843570
	.long	-2071952380
	.long	1231120425
	.long	-2087481325
	.long	120796215
	.long	-941366233
	.long	69215268
	.long	-2069847004
	.long	-876361717
	.long	1129533459
	.long	167905290
	.long	-2021424121
	.long	-908994535
	.long	1279282188
	.long	-2088534013
	.long	-1887204337
	.long	-826094578
	.long	187906107
	.long	1245857802
	.long	-2018266057
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
