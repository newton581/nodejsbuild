	.file	"rsa_chk.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/rsa/rsa_chk.c"
	.text
	.p2align 4
	.type	RSA_check_key_ex.part.0, @function
RSA_check_key_ex.part.0:
.LFB399:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$88, %rsp
	cmpl	$1, 4(%rdi)
	movq	%rsi, -96(%rbp)
	je	.L145
.L2:
	call	BN_new@PLT
	movq	%rax, %r12
	call	BN_new@PLT
	movq	%rax, -88(%rbp)
	movq	%rax, %r14
	call	BN_new@PLT
	movq	%rax, -64(%rbp)
	call	BN_new@PLT
	movq	%rax, -72(%rbp)
	call	BN_new@PLT
	movq	%rax, -80(%rbp)
	call	BN_CTX_new@PLT
	movq	%rax, %r13
	testq	%r12, %r12
	je	.L6
	testq	%r14, %r14
	je	.L6
	cmpq	$0, -64(%rbp)
	je	.L6
	cmpq	$0, -72(%rbp)
	je	.L6
	cmpq	$0, -80(%rbp)
	je	.L6
	testq	%rax, %rax
	je	.L6
	movq	32(%rbx), %rdi
	call	BN_is_one@PLT
	movl	$1, -52(%rbp)
	testl	%eax, %eax
	jne	.L146
.L10:
	movq	32(%rbx), %rdi
	call	BN_is_odd@PLT
	testl	%eax, %eax
	je	.L147
.L11:
	movq	48(%rbx), %rdi
	movq	-96(%rbp), %rcx
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	BN_is_prime_ex@PLT
	cmpl	$1, %eax
	jne	.L148
.L12:
	movq	56(%rbx), %rdi
	movq	-96(%rbp), %rcx
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	BN_is_prime_ex@PLT
	cmpl	$1, %eax
	jne	.L149
	testl	%r15d, %r15d
	je	.L14
.L152:
	xorl	%r14d, %r14d
	movq	%r12, -104(%rbp)
	movq	%r13, -112(%rbp)
	movl	%r14d, %r12d
	movl	-52(%rbp), %r13d
	movq	-96(%rbp), %r14
	.p2align 4,,10
	.p2align 3
.L16:
	movq	88(%rbx), %rdi
	movl	%r12d, %esi
	call	OPENSSL_sk_value@PLT
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r14, %rcx
	movq	(%rax), %rdi
	call	BN_is_prime_ex@PLT
	cmpl	$1, %eax
	je	.L15
	movl	$81, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$170, %edx
	xorl	%r13d, %r13d
	movl	$160, %esi
	movl	$4, %edi
	call	ERR_put_error@PLT
.L15:
	addl	$1, %r12d
	cmpl	%r15d, %r12d
	jne	.L16
	movq	-104(%rbp), %r12
	movq	56(%rbx), %rdx
	movl	%r13d, -52(%rbp)
	xorl	%r14d, %r14d
	movq	-112(%rbp), %r13
	movq	48(%rbx), %rsi
	movq	%r12, %rdi
	movq	%r13, %rcx
	call	BN_mul@PLT
	testl	%eax, %eax
	jne	.L19
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L151:
	addl	$1, %r14d
	cmpl	%r14d, %r15d
	je	.L150
.L19:
	movq	88(%rbx), %rdi
	movl	%r14d, %esi
	call	OPENSSL_sk_value@PLT
	movq	%r13, %rcx
	movq	%r12, %rsi
	movq	%r12, %rdi
	movq	(%rax), %rdx
	call	BN_mul@PLT
	testl	%eax, %eax
	jne	.L151
.L18:
	movl	$-1, -52(%rbp)
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L6:
	movl	$51, %r8d
	movl	$65, %edx
	movl	$160, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-1, -52(%rbp)
.L9:
	movq	%r12, %rdi
	call	BN_free@PLT
	movq	-88(%rbp), %rdi
	call	BN_free@PLT
	movq	-64(%rbp), %rdi
	call	BN_free@PLT
	movq	-72(%rbp), %rdi
	call	BN_free@PLT
	movq	-80(%rbp), %rdi
	call	BN_free@PLT
	movq	%r13, %rdi
	call	BN_CTX_free@PLT
.L1:
	movl	-52(%rbp), %eax
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L145:
	.cfi_restore_state
	movq	88(%rdi), %rdi
	call	OPENSSL_sk_num@PLT
	movl	%eax, %r15d
	testl	%eax, %eax
	jle	.L5
	movq	24(%rbx), %rdi
	call	BN_num_bits@PLT
	movl	%eax, %edi
	call	rsa_multip_cap@PLT
	leal	1(%r15), %edx
	cmpl	%eax, %edx
	jl	.L2
.L5:
	movl	$37, %r8d
	movl	$167, %edx
	movl	$160, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$0, -52(%rbp)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L149:
	movl	$73, %r8d
	movl	$129, %edx
	movl	$160, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$0, -52(%rbp)
	testl	%r15d, %r15d
	jne	.L152
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L148:
	movl	$67, %r8d
	movl	$128, %edx
	movl	$160, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$0, -52(%rbp)
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L147:
	movl	$61, %r8d
	movl	$101, %edx
	movl	$160, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$0, -52(%rbp)
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L146:
	movl	$57, %r8d
	movl	$101, %edx
	movl	$160, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$0, -52(%rbp)
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L150:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	BN_cmp@PLT
	testl	%eax, %eax
	jne	.L153
.L22:
	call	BN_value_one@PLT
	movq	48(%rbx), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	BN_sub@PLT
	testl	%eax, %eax
	je	.L18
	call	BN_value_one@PLT
	movq	-88(%rbp), %r14
	movq	56(%rbx), %rsi
	movq	%rax, %rdx
	movq	%r14, %rdi
	call	BN_sub@PLT
	testl	%eax, %eax
	je	.L18
	movq	-72(%rbp), %rdi
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	%r12, %rsi
	call	BN_mul@PLT
	testl	%eax, %eax
	je	.L18
	movq	-80(%rbp), %rdi
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	%r12, %rsi
	call	BN_gcd@PLT
	testl	%eax, %eax
	je	.L18
	testl	%r15d, %r15d
	je	.L23
	movl	%r15d, -112(%rbp)
	xorl	%r14d, %r14d
	movq	-80(%rbp), %r15
	movq	%r12, -120(%rbp)
	movq	-72(%rbp), %r12
	movq	%rbx, -104(%rbp)
	movq	-64(%rbp), %rbx
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L155:
	movq	%r13, %rcx
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	BN_mul@PLT
	testl	%eax, %eax
	je	.L137
	movq	%r13, %rcx
	movq	%rbx, %rdx
	movq	%r15, %rsi
	movq	%r15, %rdi
	call	BN_gcd@PLT
	testl	%eax, %eax
	je	.L137
	addl	$1, %r14d
	cmpl	%r14d, -112(%rbp)
	je	.L154
.L24:
	movq	-104(%rbp), %rax
	movl	%r14d, %esi
	movq	88(%rax), %rdi
	call	OPENSSL_sk_value@PLT
	movq	%rax, -96(%rbp)
	call	BN_value_one@PLT
	movq	-96(%rbp), %rcx
	movq	%rbx, %rdi
	movq	%rax, %rdx
	movq	(%rcx), %rsi
	call	BN_sub@PLT
	testl	%eax, %eax
	jne	.L155
.L137:
	movq	-120(%rbp), %r12
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L14:
	movq	56(%rbx), %rdx
	movq	48(%rbx), %rsi
	movq	%r13, %rcx
	movq	%r12, %rdi
	call	BN_mul@PLT
	testl	%eax, %eax
	je	.L18
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	BN_cmp@PLT
	testl	%eax, %eax
	je	.L22
	movl	$103, %r8d
	movl	$127, %edx
	movl	$160, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$0, -52(%rbp)
	jmp	.L22
.L153:
	movl	$100, %r8d
	movl	$172, %edx
	movl	$160, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$0, -52(%rbp)
	jmp	.L22
.L154:
	movl	-112(%rbp), %r15d
	movq	-120(%rbp), %r12
	movq	-104(%rbp), %rbx
.L23:
	movq	-64(%rbp), %r14
	movq	-80(%rbp), %rcx
	xorl	%esi, %esi
	movq	%r13, %r8
	movq	-72(%rbp), %rdx
	movq	%r14, %rdi
	call	BN_div@PLT
	testl	%eax, %eax
	je	.L18
	movq	32(%rbx), %rdx
	movq	40(%rbx), %rsi
	movq	%r13, %r8
	movq	%r14, %rcx
	movq	%r12, %rdi
	call	BN_mod_mul@PLT
	testl	%eax, %eax
	je	.L18
	movq	%r12, %rdi
	call	BN_is_one@PLT
	testl	%eax, %eax
	je	.L156
.L25:
	cmpq	$0, 64(%rbx)
	je	.L26
	cmpq	$0, 72(%rbx)
	je	.L26
	cmpq	$0, 80(%rbx)
	je	.L26
	call	BN_value_one@PLT
	movq	48(%rbx), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	BN_sub@PLT
	testl	%eax, %eax
	je	.L18
	movq	-88(%rbp), %r14
	movq	40(%rbx), %rdx
	xorl	%edi, %edi
	movq	%r13, %r8
	movq	%r12, %rcx
	movq	%r14, %rsi
	call	BN_div@PLT
	testl	%eax, %eax
	je	.L18
	movq	64(%rbx), %rsi
	movq	%r14, %rdi
	call	BN_cmp@PLT
	testl	%eax, %eax
	jne	.L157
.L27:
	call	BN_value_one@PLT
	movq	56(%rbx), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	BN_sub@PLT
	testl	%eax, %eax
	je	.L18
	movq	-88(%rbp), %r14
	movq	40(%rbx), %rdx
	xorl	%edi, %edi
	movq	%r13, %r8
	movq	%r12, %rcx
	movq	%r14, %rsi
	call	BN_div@PLT
	testl	%eax, %eax
	je	.L18
	movq	72(%rbx), %rsi
	movq	%r14, %rdi
	call	BN_cmp@PLT
	testl	%eax, %eax
	jne	.L158
.L28:
	movq	48(%rbx), %rdx
	movq	56(%rbx), %rsi
	movq	%r13, %rcx
	movq	%r12, %rdi
	call	BN_mod_inverse@PLT
	testq	%rax, %rax
	je	.L18
	movq	80(%rbx), %rsi
	movq	%r12, %rdi
	call	BN_cmp@PLT
	testl	%eax, %eax
	jne	.L159
.L26:
	testl	%r15d, %r15d
	je	.L9
	movl	$0, -96(%rbp)
	jmp	.L31
.L29:
	movq	24(%r14), %rsi
	movq	(%r14), %rdx
	movq	%r13, %rcx
	movq	%r12, %rdi
	call	BN_mod_inverse@PLT
	testq	%rax, %rax
	je	.L18
	movq	16(%r14), %rsi
	movq	%r12, %rdi
	call	BN_cmp@PLT
	testl	%eax, %eax
	jne	.L160
.L30:
	addl	$1, -96(%rbp)
	movl	-96(%rbp), %eax
	cmpl	%eax, %r15d
	je	.L9
.L31:
	movq	88(%rbx), %rdi
	movl	-96(%rbp), %esi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %r14
	call	BN_value_one@PLT
	movq	(%r14), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	BN_sub@PLT
	testl	%eax, %eax
	je	.L18
	movq	40(%rbx), %rdx
	movq	-88(%rbp), %rsi
	xorl	%edi, %edi
	movq	%r13, %r8
	movq	%r12, %rcx
	call	BN_div@PLT
	testl	%eax, %eax
	je	.L18
	movq	8(%r14), %rsi
	movq	-88(%rbp), %rdi
	call	BN_cmp@PLT
	testl	%eax, %eax
	je	.L29
	movl	$207, %r8d
	movl	$169, %edx
	movl	$160, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$0, -52(%rbp)
	jmp	.L29
.L160:
	movl	$216, %r8d
	movl	$168, %edx
	movl	$160, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$0, -52(%rbp)
	jmp	.L30
.L156:
	movl	$151, %r8d
	movl	$123, %edx
	movl	$160, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$0, -52(%rbp)
	jmp	.L25
.L159:
	movl	$190, %r8d
	movl	$126, %edx
	movl	$160, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$0, -52(%rbp)
	jmp	.L26
.L158:
	movl	$180, %r8d
	movl	$125, %edx
	movl	$160, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$0, -52(%rbp)
	jmp	.L28
.L157:
	movl	$166, %r8d
	movl	$124, %edx
	movl	$160, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$0, -52(%rbp)
	jmp	.L27
	.cfi_endproc
.LFE399:
	.size	RSA_check_key_ex.part.0, .-RSA_check_key_ex.part.0
	.p2align 4
	.globl	RSA_check_key
	.type	RSA_check_key, @function
RSA_check_key:
.LFB397:
	.cfi_startproc
	endbr64
	cmpq	$0, 48(%rdi)
	je	.L162
	cmpq	$0, 56(%rdi)
	je	.L162
	cmpq	$0, 24(%rdi)
	je	.L162
	cmpq	$0, 32(%rdi)
	je	.L162
	cmpq	$0, 40(%rdi)
	je	.L162
	xorl	%esi, %esi
	jmp	RSA_check_key_ex.part.0
	.p2align 4,,10
	.p2align 3
.L162:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$147, %edx
	movl	$160, %esi
	movl	$4, %edi
	movl	$28, %r8d
	leaq	.LC0(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE397:
	.size	RSA_check_key, .-RSA_check_key
	.p2align 4
	.globl	RSA_check_key_ex
	.type	RSA_check_key_ex, @function
RSA_check_key_ex:
.LFB398:
	.cfi_startproc
	endbr64
	cmpq	$0, 48(%rdi)
	je	.L169
	cmpq	$0, 56(%rdi)
	je	.L169
	cmpq	$0, 24(%rdi)
	je	.L169
	cmpq	$0, 32(%rdi)
	je	.L169
	cmpq	$0, 40(%rdi)
	je	.L169
	jmp	RSA_check_key_ex.part.0
	.p2align 4,,10
	.p2align 3
.L169:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$147, %edx
	movl	$160, %esi
	movl	$4, %edi
	movl	$28, %r8d
	leaq	.LC0(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE398:
	.size	RSA_check_key_ex, .-RSA_check_key_ex
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
