	.file	"bss_conn.c"
	.text
	.p2align 4
	.type	conn_callback_ctrl, @function
conn_callback_ctrl:
.LFB277:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpl	$14, %esi
	je	.L5
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	movq	56(%rdi), %rax
	movq	%rdx, 48(%rax)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE277:
	.size	conn_callback_ctrl, .-conn_callback_ctrl
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/bio/bss_conn.c"
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	" service="
.LC2:
	.string	"hostname="
	.text
	.p2align 4
	.type	conn_state, @function
conn_state:
.LFB267:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	.L9(%rip), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	$-1, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	48(%rsi), %r13
.L32:
	movl	(%rbx), %esi
.L25:
	cmpl	$7, %esi
	ja	.L7
	movslq	(%r15,%rsi,4), %rax
	addq	%r15, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L9:
	.long	.L7-.L9
	.long	.L15-.L9
	.long	.L14-.L9
	.long	.L13-.L9
	.long	.L12-.L9
	.long	.L11-.L9
	.long	.L10-.L9
	.long	.L8-.L9
	.text
	.p2align 4,,10
	.p2align 3
.L59:
	cmpq	$0, 16(%rbx)
	jne	.L16
	movl	$88, %r8d
	movl	$144, %edx
	movl	$115, %esi
	movl	$32, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	8(%rbx), %rdx
	movq	16(%rbx), %r8
	xorl	%eax, %eax
	leaq	.LC1(%rip), %rcx
	leaq	.LC2(%rip), %rsi
	movl	$4, %edi
	call	ERR_add_error_data@PLT
	.p2align 4,,10
	.p2align 3
.L7:
	testq	%r13, %r13
	je	.L6
	movl	(%rbx), %esi
	addq	$24, %rsp
	movl	%r14d, %edx
	movq	%r12, %rdi
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L10:
	.cfi_restore_state
	movl	48(%r12), %edi
	call	BIO_sock_error@PLT
	testl	%eax, %eax
	jne	.L57
.L27:
	movl	$5, (%rbx)
	testq	%r13, %r13
	jne	.L58
.L31:
.L28:
	movl	$1, %r14d
.L6:
	addq	$24, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	.cfi_restore_state
	cmpq	$0, 8(%rbx)
	je	.L59
.L16:
	movl	$2, (%rbx)
	testq	%r13, %r13
	jne	.L33
.L14:
	movl	4(%rbx), %eax
	cmpl	$6, %eax
	je	.L34
	cmpl	$256, %eax
	je	.L35
	cmpl	$4, %eax
	je	.L36
	movl	$121, %r8d
	movl	$146, %edx
	movl	$115, %esi
	movl	$32, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L8:
	movl	$203, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$103, %edx
	xorl	%r14d, %r14d
	movl	$115, %esi
	movl	$32, %edi
	call	ERR_put_error@PLT
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L35:
	xorl	%ecx, %ecx
.L18:
	movq	16(%rbx), %rsi
	movq	8(%rbx), %rdi
	xorl	%edx, %edx
	leaq	32(%rbx), %r9
	movl	$1, %r8d
	call	BIO_lookup@PLT
	testl	%eax, %eax
	je	.L7
	movq	32(%rbx), %rax
	testq	%rax, %rax
	je	.L60
	movq	%rax, 40(%rbx)
	movl	$3, (%rbx)
	testq	%r13, %r13
	jne	.L37
.L13:
	movq	40(%rbx), %rdi
	call	BIO_ADDRINFO_protocol@PLT
	movq	40(%rbx), %rdi
	movl	%eax, -52(%rbp)
	call	BIO_ADDRINFO_socktype@PLT
	movq	40(%rbx), %rdi
	movl	%eax, %r14d
	call	BIO_ADDRINFO_family@PLT
	movl	-52(%rbp), %edx
	movl	%r14d, %esi
	xorl	%ecx, %ecx
	movl	%eax, %edi
	call	BIO_socket@PLT
	movl	%eax, %r14d
	cmpl	$-1, %eax
	je	.L61
	movl	%eax, 48(%r12)
	movl	$4, (%rbx)
	testq	%r13, %r13
	jne	.L62
	movl	$15, %esi
	movq	%r12, %rdi
	call	BIO_clear_flags@PLT
	movl	24(%rbx), %r14d
	movq	40(%rbx), %rdi
	call	BIO_ADDRINFO_address@PLT
	orl	$4, %r14d
	movl	48(%r12), %edi
	movq	%rax, %rsi
	movl	%r14d, %edx
	call	BIO_connect@PLT
	movl	$0, 44(%r12)
	testl	%eax, %eax
	je	.L29
	movl	$5, (%rbx)
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L36:
	movl	$2, %ecx
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L12:
	movl	$15, %esi
	movq	%r12, %rdi
	call	BIO_clear_flags@PLT
	movl	24(%rbx), %r14d
	movq	40(%rbx), %rdi
	call	BIO_ADDRINFO_address@PLT
	orl	$4, %r14d
	movl	48(%r12), %edi
	movl	%r14d, %edx
	movq	%rax, %rsi
	call	BIO_connect@PLT
	movl	$0, 44(%r12)
	movl	%eax, %r14d
	testl	%eax, %eax
	jne	.L27
	.p2align 4,,10
	.p2align 3
.L29:
	xorl	%edi, %edi
	call	BIO_sock_should_retry@PLT
	movl	%eax, %r14d
	testl	%eax, %eax
	jne	.L63
	movq	40(%rbx), %rdi
	call	BIO_ADDRINFO_next@PLT
	movq	%rax, 40(%rbx)
	testq	%rax, %rax
	je	.L24
	movl	48(%r12), %edi
	call	BIO_closesocket@PLT
	movl	$3, (%rbx)
	call	ERR_clear_error@PLT
	movl	(%rbx), %esi
	testq	%r13, %r13
	je	.L25
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L34:
	movl	$10, %ecx
	jmp	.L18
.L37:
	movl	$3, %esi
	.p2align 4,,10
	.p2align 3
.L17:
	movl	%r14d, %edx
	movq	%r12, %rdi
	call	*%r13
	movl	%eax, %r14d
	testl	%eax, %eax
	jne	.L32
	jmp	.L6
.L11:
	movl	$1, %r14d
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L24:
	call	__errno_location@PLT
	movl	$174, %r8d
	movl	$2, %esi
	leaq	.LC0(%rip), %rcx
	movl	(%rax), %edx
	movl	$2, %edi
	call	ERR_put_error@PLT
	movq	8(%rbx), %rdx
	movq	16(%rbx), %r8
	xorl	%eax, %eax
	leaq	.LC1(%rip), %rcx
	leaq	.LC2(%rip), %rsi
	movl	$4, %edi
	call	ERR_add_error_data@PLT
	movl	$7, (%rbx)
	testq	%r13, %r13
	je	.L8
	movl	$7, %esi
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L58:
	movl	$5, %esi
	jmp	.L17
.L33:
	movl	$2, %esi
	jmp	.L17
.L61:
	call	__errno_location@PLT
	movl	$142, %r8d
	movl	$4, %esi
	leaq	.LC0(%rip), %rcx
	movl	(%rax), %edx
	movl	$2, %edi
	call	ERR_put_error@PLT
	movq	8(%rbx), %rdx
	movq	16(%rbx), %r8
	xorl	%eax, %eax
	leaq	.LC1(%rip), %rcx
	leaq	.LC2(%rip), %rsi
	movl	$4, %edi
	call	ERR_add_error_data@PLT
	movl	$146, %r8d
	movl	$118, %edx
	leaq	.LC0(%rip), %rcx
	movl	$115, %esi
	movl	$32, %edi
	call	ERR_put_error@PLT
	jmp	.L7
.L62:
	movl	$4, %esi
	jmp	.L17
.L57:
	movl	$15, %esi
	movq	%r12, %rdi
	movl	%eax, -52(%rbp)
	xorl	%r14d, %r14d
	call	BIO_clear_flags@PLT
	movl	-52(%rbp), %eax
	movl	$191, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$2, %esi
	movl	$2, %edi
	movl	%eax, %edx
	call	ERR_put_error@PLT
	movq	8(%rbx), %rdx
	movq	16(%rbx), %r8
	xorl	%eax, %eax
	leaq	.LC1(%rip), %rcx
	leaq	.LC2(%rip), %rsi
	movl	$4, %edi
	call	ERR_add_error_data@PLT
	movl	$195, %r8d
	movl	$110, %edx
	leaq	.LC0(%rip), %rcx
	movl	$115, %esi
	movl	$32, %edi
	call	ERR_put_error@PLT
	jmp	.L7
.L60:
	movl	$130, %r8d
	movl	$142, %edx
	movl	$115, %esi
	movl	$32, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L7
.L63:
	movl	$12, %esi
	movq	%r12, %rdi
	xorl	%r14d, %r14d
	call	BIO_set_flags@PLT
	movl	$6, (%rbx)
	movl	$2, 44(%r12)
	call	ERR_clear_error@PLT
	jmp	.L7
	.cfi_endproc
.LFE267:
	.size	conn_state, .-conn_state
	.p2align 4
	.type	conn_write, @function
conn_write:
.LFB275:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	56(%rdi), %rsi
	movl	%edx, %ebx
	cmpl	$5, (%rsi)
	je	.L68
	call	conn_state
	movl	%eax, %r13d
	testl	%eax, %eax
	jle	.L64
.L68:
	call	__errno_location@PLT
	movslq	%ebx, %rdx
	movq	%r14, %rsi
	movl	$0, (%rax)
	movl	48(%r12), %edi
	call	write@PLT
	movl	$15, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	movl	%eax, %r13d
	call	BIO_clear_flags@PLT
	testl	%ebx, %ebx
	jle	.L73
.L64:
	popq	%rbx
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L73:
	.cfi_restore_state
	movl	%ebx, %edi
	call	BIO_sock_should_retry@PLT
	testl	%eax, %eax
	je	.L64
	movl	$10, %esi
	movq	%r12, %rdi
	call	BIO_set_flags@PLT
	jmp	.L64
	.cfi_endproc
.LFE275:
	.size	conn_write, .-conn_write
	.p2align 4
	.type	conn_ctrl, @function
conn_ctrl:
.LFB276:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	56(%rdi), %r15
	cmpl	$15, %esi
	jg	.L75
	testl	%esi, %esi
	jg	.L76
.L134:
	xorl	%r12d, %r12d
.L74:
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L75:
	.cfi_restore_state
	cmpl	$123, %esi
	jg	.L92
	cmpl	$99, %esi
	jle	.L134
	subl	$100, %esi
	cmpl	$23, %esi
	ja	.L134
	leaq	.L79(%rip), %rcx
	movslq	(%rcx,%rsi,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L79:
	.long	.L83-.L79
	.long	.L82-.L79
	.long	.L81-.L79
	.long	.L134-.L79
	.long	.L134-.L79
	.long	.L80-.L79
	.long	.L134-.L79
	.long	.L134-.L79
	.long	.L134-.L79
	.long	.L134-.L79
	.long	.L134-.L79
	.long	.L134-.L79
	.long	.L134-.L79
	.long	.L134-.L79
	.long	.L134-.L79
	.long	.L134-.L79
	.long	.L134-.L79
	.long	.L134-.L79
	.long	.L134-.L79
	.long	.L134-.L79
	.long	.L134-.L79
	.long	.L134-.L79
	.long	.L134-.L79
	.long	.L78-.L79
	.text
	.p2align 4,,10
	.p2align 3
.L76:
	cmpl	$15, %esi
	ja	.L134
	leaq	.L86(%rip), %rcx
	movl	%esi, %esi
	movslq	(%rcx,%rsi,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L86:
	.long	.L134-.L86
	.long	.L91-.L86
	.long	.L90-.L86
	.long	.L134-.L86
	.long	.L134-.L86
	.long	.L134-.L86
	.long	.L134-.L86
	.long	.L134-.L86
	.long	.L89-.L86
	.long	.L88-.L86
	.long	.L134-.L86
	.long	.L109-.L86
	.long	.L87-.L86
	.long	.L134-.L86
	.long	.L134-.L86
	.long	.L85-.L86
	.text
.L109:
	movl	$1, %r12d
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L92:
	.cfi_restore_state
	cmpl	$155, %esi
	jne	.L134
	movl	%edx, 24(%r15)
	movl	$1, %r12d
	jmp	.L74
.L85:
	movq	48(%r15), %rax
	movl	$1, %r12d
	movq	%rax, (%r14)
	jmp	.L74
.L83:
	movl	$1, %r12d
	testq	%r14, %r14
	je	.L74
	movl	$1, 32(%r13)
	testq	%rdx, %rdx
	je	.L135
	cmpq	$1, %rdx
	je	.L136
	cmpq	$2, %rdx
	je	.L137
	cmpq	$3, %rdx
	jne	.L134
	movl	(%r14), %eax
	movl	%eax, 4(%r15)
	jmp	.L74
.L82:
	cmpl	$5, (%r15)
	movl	$1, %r12d
	je	.L74
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	conn_state
	movslq	%eax, %r12
	jmp	.L74
.L81:
	movl	24(%r15), %eax
	testq	%rdx, %rdx
	je	.L104
	orl	$8, %eax
	movl	$1, %r12d
	movl	%eax, 24(%r15)
	jmp	.L74
.L80:
	movl	32(%r13), %eax
	testl	%eax, %eax
	je	.L115
	movslq	48(%r13), %r12
	testq	%r14, %r14
	je	.L74
	movl	%r12d, (%r14)
	movslq	48(%r13), %r12
	jmp	.L74
.L78:
	testq	%r14, %r14
	je	.L134
	testq	%rdx, %rdx
	jne	.L97
	movq	8(%r15), %rax
	movl	$1, %r12d
	movq	%rax, (%r14)
	jmp	.L74
.L91:
	movl	$1, (%r15)
	movl	48(%rdi), %edi
	cmpl	$-1, %edi
	je	.L95
	call	BIO_closesocket@PLT
	movl	$-1, 48(%r13)
.L95:
	movq	32(%r15), %rdi
	call	BIO_ADDRINFO_free@PLT
	movq	$0, 32(%r15)
	movl	$0, 40(%r13)
	jmp	.L134
.L90:
	movl	40(%rdi), %r12d
	sarl	$11, %r12d
	andl	$1, %r12d
	jmp	.L74
.L88:
	movl	%edx, 36(%rdi)
	movl	$1, %r12d
	jmp	.L74
.L87:
	movq	8(%r15), %rcx
	testq	%rcx, %rcx
	je	.L106
	xorl	%edx, %edx
	movl	$100, %esi
	movq	%r14, %rdi
	call	BIO_ctrl@PLT
.L106:
	movq	16(%r15), %rcx
	testq	%rcx, %rcx
	je	.L107
	movl	$1, %edx
	movl	$100, %esi
	movq	%r14, %rdi
	call	BIO_ctrl@PLT
.L107:
	movl	4(%r15), %ecx
	movl	$3, %edx
	movl	$100, %esi
	movq	%r14, %rdi
	movl	$1, %r12d
	call	BIO_int_ctrl@PLT
	movslq	24(%r15), %rdx
	xorl	%ecx, %ecx
	movl	$155, %esi
	movq	%r14, %rdi
	call	BIO_ctrl@PLT
	movq	48(%r15), %rdx
	movl	$14, %esi
	movq	%r14, %rdi
	call	BIO_callback_ctrl@PLT
	jmp	.L74
.L89:
	movslq	36(%rdi), %r12
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L104:
	andl	$-9, %eax
	movl	$1, %r12d
	movl	%eax, 24(%r15)
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L97:
	cmpq	$1, %rdx
	je	.L138
	cmpq	$2, %rdx
	je	.L139
	cmpq	$3, %rdx
	jne	.L134
	movq	40(%r15), %rdi
	movl	$4, %r12d
	call	BIO_ADDRINFO_family@PLT
	cmpl	$2, %eax
	je	.L74
	movl	$6, %r12d
	cmpl	$10, %eax
	je	.L74
	movq	$-1, %r12
	testl	%eax, %eax
	jne	.L74
	movslq	4(%r15), %r12
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L135:
	movq	8(%r15), %rdi
	movl	$416, %edx
	leaq	.LC0(%rip), %rsi
	movq	16(%r15), %r13
	call	CRYPTO_free@PLT
	leaq	16(%r15), %rdx
	leaq	8(%r15), %rsi
	xorl	%ecx, %ecx
	movq	$0, 8(%r15)
	movq	%r14, %rdi
	call	BIO_parse_hostserv@PLT
	movslq	%eax, %r12
	cmpq	%r13, 16(%r15)
	je	.L74
	movl	$423, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r13, %rdi
	call	CRYPTO_free@PLT
	jmp	.L74
.L138:
	movq	16(%r15), %rax
	movl	$1, %r12d
	movq	%rax, (%r14)
	jmp	.L74
.L136:
	movq	16(%r15), %rdi
	movl	$425, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movl	$426, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r14, %rdi
	call	CRYPTO_strdup@PLT
	movq	%rax, 16(%r15)
	jmp	.L74
.L137:
	movl	$1, %esi
	movq	%r14, %rdi
	call	BIO_ADDR_hostname_string@PLT
	movl	$1, %esi
	movq	%r14, %rdi
	movq	%rax, 8(%r15)
	call	BIO_ADDR_service_string@PLT
	movq	32(%r15), %rdi
	movq	%rax, 16(%r15)
	call	BIO_ADDRINFO_free@PLT
	pxor	%xmm0, %xmm0
	movups	%xmm0, 32(%r15)
	jmp	.L74
.L139:
	movq	40(%r15), %rdi
	movl	$1, %r12d
	call	BIO_ADDRINFO_address@PLT
	movq	%rax, (%r14)
	jmp	.L74
.L115:
	movq	$-1, %r12
	jmp	.L74
	.cfi_endproc
.LFE276:
	.size	conn_ctrl, .-conn_ctrl
	.p2align 4
	.type	conn_read, @function
conn_read:
.LFB274:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	xorl	%r13d, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	56(%rdi), %rsi
	movl	%edx, %ebx
	cmpl	$5, (%rsi)
	jne	.L151
.L141:
	testq	%r14, %r14
	je	.L140
	call	__errno_location@PLT
	movslq	%ebx, %rdx
	movq	%r14, %rsi
	movl	$0, (%rax)
	movl	48(%r12), %edi
	call	read@PLT
	movl	$15, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	movl	%eax, %r13d
	call	BIO_clear_flags@PLT
	testl	%ebx, %ebx
	jle	.L152
.L140:
	popq	%rbx
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L151:
	.cfi_restore_state
	call	conn_state
	movl	%eax, %r13d
	testl	%eax, %eax
	jg	.L141
	popq	%rbx
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L152:
	.cfi_restore_state
	movl	%ebx, %edi
	call	BIO_sock_should_retry@PLT
	testl	%eax, %eax
	jne	.L153
	testl	%ebx, %ebx
	jne	.L140
	orl	$2048, 40(%r12)
	jmp	.L140
	.p2align 4,,10
	.p2align 3
.L153:
	movl	$9, %esi
	movq	%r12, %rdi
	call	BIO_set_flags@PLT
	jmp	.L140
	.cfi_endproc
.LFE274:
	.size	conn_read, .-conn_read
	.p2align 4
	.type	conn_free, @function
conn_free:
.LFB273:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L160
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movl	36(%rdi), %edx
	movq	%rdi, %rbx
	testl	%edx, %edx
	jne	.L169
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L169:
	.cfi_restore_state
	movq	56(%rdi), %r12
	movl	48(%rdi), %edi
	cmpl	$-1, %edi
	je	.L156
	cmpl	$5, (%r12)
	je	.L170
.L157:
	call	BIO_closesocket@PLT
	movl	$-1, 48(%rbx)
.L158:
	movq	8(%r12), %rdi
	movl	$246, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	16(%r12), %rdi
	movl	$247, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	32(%r12), %rdi
	call	BIO_ADDRINFO_free@PLT
	movl	$249, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
.L159:
	movq	$0, 56(%rbx)
	movl	$1, %eax
	movl	$0, 40(%rbx)
	movl	$0, 32(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L156:
	.cfi_restore_state
	testq	%r12, %r12
	je	.L159
	jmp	.L158
	.p2align 4,,10
	.p2align 3
.L160:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L170:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	movl	$2, %esi
	call	shutdown@PLT
	movl	48(%rbx), %edi
	jmp	.L157
	.cfi_endproc
.LFE273:
	.size	conn_free, .-conn_free
	.p2align 4
	.type	conn_new, @function
conn_new:
.LFB271:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$233, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	$0, 32(%rdi)
	movl	$-1, 48(%rdi)
	movl	$0, 40(%rdi)
	movl	$56, %edi
	call	CRYPTO_zalloc@PLT
	testq	%rax, %rax
	je	.L175
	movabsq	$1099511627777, %rcx
	movq	%rcx, (%rax)
	movq	%rax, 56(%rbx)
	movl	$1, %eax
.L171:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L175:
	.cfi_restore_state
	movl	$234, %r8d
	movl	$65, %edx
	movl	$153, %esi
	movl	$32, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	$0, 56(%rbx)
	xorl	%eax, %eax
	jmp	.L171
	.cfi_endproc
.LFE271:
	.size	conn_new, .-conn_new
	.p2align 4
	.type	conn_puts, @function
conn_puts:
.LFB278:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	call	strlen@PLT
	movq	56(%r12), %rsi
	movq	%rax, %rbx
	cmpl	$5, (%rsi)
	je	.L180
	movq	%r12, %rdi
	call	conn_state
	movl	%eax, %r14d
	testl	%eax, %eax
	jle	.L176
.L180:
	call	__errno_location@PLT
	movslq	%ebx, %rdx
	movq	%r13, %rsi
	movl	$0, (%rax)
	movl	48(%r12), %edi
	call	write@PLT
	movl	$15, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	movl	%eax, %r14d
	call	BIO_clear_flags@PLT
	testl	%ebx, %ebx
	jle	.L185
.L176:
	popq	%rbx
	movl	%r14d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L185:
	.cfi_restore_state
	movl	%ebx, %edi
	call	BIO_sock_should_retry@PLT
	testl	%eax, %eax
	je	.L176
	movl	$10, %esi
	movq	%r12, %rdi
	call	BIO_set_flags@PLT
	jmp	.L176
	.cfi_endproc
.LFE278:
	.size	conn_puts, .-conn_puts
	.p2align 4
	.globl	BIO_CONNECT_new
	.type	BIO_CONNECT_new, @function
BIO_CONNECT_new:
.LFB268:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$233, %edx
	movl	$56, %edi
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	call	CRYPTO_zalloc@PLT
	testq	%rax, %rax
	je	.L190
	movabsq	$1099511627777, %rcx
	movq	%rcx, (%rax)
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L190:
	.cfi_restore_state
	movl	$234, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	movl	$153, %esi
	movl	$32, %edi
	movq	%rax, -8(%rbp)
	call	ERR_put_error@PLT
	movq	-8(%rbp), %rax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE268:
	.size	BIO_CONNECT_new, .-BIO_CONNECT_new
	.p2align 4
	.globl	BIO_CONNECT_free
	.type	BIO_CONNECT_free, @function
BIO_CONNECT_free:
.LFB269:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L191
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$246, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	8(%rdi), %rdi
	call	CRYPTO_free@PLT
	movq	16(%r12), %rdi
	movl	$247, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	32(%r12), %rdi
	call	BIO_ADDRINFO_free@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$249, %edx
	popq	%r12
	.cfi_restore 12
	leaq	.LC0(%rip), %rsi
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_free@PLT
	.p2align 4,,10
	.p2align 3
.L191:
	ret
	.cfi_endproc
.LFE269:
	.size	BIO_CONNECT_free, .-BIO_CONNECT_free
	.p2align 4
	.globl	BIO_s_connect
	.type	BIO_s_connect, @function
BIO_s_connect:
.LFB270:
	.cfi_startproc
	endbr64
	leaq	methods_connectp(%rip), %rax
	ret
	.cfi_endproc
.LFE270:
	.size	BIO_s_connect, .-BIO_s_connect
	.p2align 4
	.globl	BIO_new_connect
	.type	BIO_new_connect, @function
BIO_new_connect:
.LFB279:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	leaq	methods_connectp(%rip), %rdi
	pushq	%r12
	.cfi_offset 12, -32
	call	BIO_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L197
	xorl	%edx, %edx
	movq	%r13, %rcx
	movl	$100, %esi
	movq	%rax, %rdi
	call	BIO_ctrl@PLT
	testq	%rax, %rax
	je	.L203
.L197:
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L203:
	.cfi_restore_state
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	BIO_free@PLT
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE279:
	.size	BIO_new_connect, .-BIO_new_connect
	.section	.rodata.str1.1
.LC3:
	.string	"socket connect"
	.section	.data.rel.ro,"aw"
	.align 32
	.type	methods_connectp, @object
	.size	methods_connectp, 96
methods_connectp:
	.long	1292
	.zero	4
	.quad	.LC3
	.quad	bwrite_conv
	.quad	conn_write
	.quad	bread_conv
	.quad	conn_read
	.quad	conn_puts
	.quad	0
	.quad	conn_ctrl
	.quad	conn_new
	.quad	conn_free
	.quad	conn_callback_ctrl
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
