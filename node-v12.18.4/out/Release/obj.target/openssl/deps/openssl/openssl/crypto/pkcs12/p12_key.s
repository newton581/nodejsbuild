	.file	"p12_key.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/pkcs12/p12_key.c"
	.text
	.p2align 4
	.globl	PKCS12_key_gen_uni
	.type	PKCS12_key_gen_uni, @function
PKCS12_key_gen_uni:
.LFB805:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$104, %rsp
	movl	%esi, -80(%rbp)
	movq	32(%rbp), %r15
	movq	%rdx, -96(%rbp)
	movl	%ecx, -88(%rbp)
	movl	%r8d, -64(%rbp)
	movl	%r9d, -60(%rbp)
	call	EVP_MD_CTX_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L2
	movq	%r15, %rdi
	call	EVP_MD_block_size@PLT
	movq	%r15, %rdi
	movl	%eax, %r14d
	call	EVP_MD_size@PLT
	movl	%eax, -104(%rbp)
	movl	%eax, %r13d
	testl	%eax, %eax
	js	.L2
	testl	%r14d, %r14d
	jle	.L2
	movslq	%r14d, %rdi
	movl	$106, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rdi, -120(%rbp)
	call	CRYPTO_malloc@PLT
	movslq	%r13d, %rdi
	movl	$107, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rdi, -128(%rbp)
	movq	%rax, -72(%rbp)
	call	CRYPTO_malloc@PLT
	movl	%r14d, %edi
	movl	$108, %edx
	leaq	.LC0(%rip), %rsi
	addl	$1, %edi
	movl	%r14d, -100(%rbp)
	movq	%rax, %r13
	movslq	%edi, %rdi
	call	CRYPTO_malloc@PLT
	movl	-100(%rbp), %edi
	movl	-88(%rbp), %ecx
	movq	%rax, %r14
	leal	-1(%rdi,%rcx), %eax
	cltd
	idivl	%edi
	movl	%eax, %ecx
	movl	-80(%rbp), %eax
	imull	%edi, %ecx
	movl	%ecx, -112(%rbp)
	testl	%eax, %eax
	jne	.L69
	movl	%ecx, -108(%rbp)
	movl	$0, -140(%rbp)
.L3:
	movslq	-108(%rbp), %rax
	movl	$115, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, -136(%rbp)
	call	CRYPTO_malloc@PLT
	movq	-72(%rbp), %rdi
	movq	%rax, -56(%rbp)
	testq	%rdi, %rdi
	je	.L4
	testq	%r13, %r13
	je	.L4
	testq	%r14, %r14
	je	.L4
	testq	%rax, %rax
	je	.L4
	movl	-100(%rbp), %esi
	movl	$1, %eax
	leal	-1(%rsi), %edx
	movl	%edx, -144(%rbp)
	addq	$1, %rdx
	testl	%esi, %esi
	movzbl	-64(%rbp), %esi
	cmovle	%rax, %rdx
	call	memset@PLT
	movl	-112(%rbp), %esi
	movl	-144(%rbp), %r9d
	testl	%esi, %esi
	jle	.L21
	leal	-1(%rsi), %edi
	movq	-56(%rbp), %r10
	movq	-96(%rbp), %r11
	xorl	%ecx, %ecx
	movl	-88(%rbp), %r8d
	leaq	1(%rdi), %rsi
	.p2align 4,,10
	.p2align 3
.L6:
	movl	%ecx, %eax
	cltd
	idivl	%r8d
	movslq	%edx, %rdx
	movzbl	(%r11,%rdx), %eax
	movb	%al, (%r10,%rcx)
	movq	%rcx, %rax
	addq	$1, %rcx
	cmpq	%rax, %rdi
	jne	.L6
	addq	-56(%rbp), %rsi
.L5:
	movl	-140(%rbp), %eax
	movl	-80(%rbp), %r8d
	xorl	%ecx, %ecx
	leal	-1(%rax), %edi
	testl	%eax, %eax
	jle	.L10
	.p2align 4,,10
	.p2align 3
.L9:
	movl	%ecx, %eax
	cltd
	idivl	%r8d
	movslq	%edx, %rdx
	movzbl	(%rbx,%rdx), %eax
	movb	%al, (%rsi,%rcx)
	movq	%rcx, %rax
	addq	$1, %rcx
	cmpq	%rax, %rdi
	jne	.L9
.L10:
	movslq	%r9d, %rax
	movq	%rax, -88(%rbp)
.L8:
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	EVP_DigestInit_ex@PLT
	testl	%eax, %eax
	je	.L4
	movq	-120(%rbp), %rdx
	movq	-72(%rbp), %rsi
	movq	%r12, %rdi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	je	.L4
	movq	-136(%rbp), %rdx
	movq	-56(%rbp), %rsi
	movq	%r12, %rdi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	je	.L4
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	EVP_DigestFinal_ex@PLT
	testl	%eax, %eax
	je	.L4
	cmpl	$1, -60(%rbp)
	movl	$1, %ebx
	jle	.L14
	movq	%r14, -80(%rbp)
	movq	-128(%rbp), %r14
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L71:
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	je	.L66
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	EVP_DigestFinal_ex@PLT
	testl	%eax, %eax
	je	.L66
	addl	$1, %ebx
	cmpl	%ebx, -60(%rbp)
	je	.L70
.L11:
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	EVP_DigestInit_ex@PLT
	testl	%eax, %eax
	jne	.L71
.L66:
	movq	-80(%rbp), %r14
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L2:
	movq	$0, -56(%rbp)
	xorl	%r13d, %r13d
	xorl	%r14d, %r14d
	movq	$0, -72(%rbp)
.L4:
	movl	$165, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r15d, %r15d
	movl	$111, %esi
	movl	$35, %edi
	call	ERR_put_error@PLT
.L12:
	movq	%r13, %rdi
	movl	$168, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%r14, %rdi
	movl	$169, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-72(%rbp), %rdi
	movl	$170, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-56(%rbp), %rdi
	movl	$171, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%r12, %rdi
	call	EVP_MD_CTX_free@PLT
	addq	$104, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L69:
	.cfi_restore_state
	leal	-1(%rdi,%rax), %eax
	cltd
	idivl	%edi
	imull	%edi, %eax
	addl	%eax, %ecx
	movl	%eax, -140(%rbp)
	movl	%ecx, -108(%rbp)
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L70:
	movq	-80(%rbp), %r14
.L14:
	movl	-104(%rbp), %ebx
	cmpl	%ebx, 16(%rbp)
	movq	%r13, %rsi
	movq	24(%rbp), %rdi
	movl	%ebx, %edx
	cmovle	16(%rbp), %edx
	movslq	%edx, %rdx
	call	memcpy@PLT
	cmpl	%ebx, 16(%rbp)
	jle	.L72
	movl	-104(%rbp), %esi
	movq	-128(%rbp), %rax
	xorl	%ecx, %ecx
	subl	%esi, 16(%rbp)
	movl	-100(%rbp), %edi
	addq	%rax, 24(%rbp)
	.p2align 4,,10
	.p2align 3
.L15:
	movl	%ecx, %eax
	cltd
	idivl	%esi
	movslq	%edx, %rdx
	movzbl	0(%r13,%rdx), %eax
	movb	%al, (%r14,%rcx)
	addq	$1, %rcx
	cmpl	%ecx, %edi
	jg	.L15
	movl	-108(%rbp), %r10d
	movq	-56(%rbp), %rsi
	xorl	%r8d, %r8d
	testl	%r10d, %r10d
	jle	.L8
	movq	-120(%rbp), %rdi
	movl	-100(%rbp), %r9d
	movq	-88(%rbp), %r11
	.p2align 4,,10
	.p2align 3
.L18:
	movq	%r11, %rcx
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L17:
	movzbl	(%rsi,%rcx), %edx
	movzbl	(%r14,%rcx), %ebx
	addl	%ebx, %edx
	addl	%edx, %eax
	movb	%al, (%rsi,%rcx)
	subq	$1, %rcx
	shrw	$8, %ax
	cmpl	$-1, %ecx
	jne	.L17
	addl	%r9d, %r8d
	addq	%rdi, %rsi
	cmpl	%r10d, %r8d
	jl	.L18
	jmp	.L8
.L21:
	movq	-56(%rbp), %rsi
	jmp	.L5
.L72:
	movl	$1, %r15d
	jmp	.L12
	.cfi_endproc
.LFE805:
	.size	PKCS12_key_gen_uni, .-PKCS12_key_gen_uni
	.p2align 4
	.globl	PKCS12_key_gen_asc
	.type	PKCS12_key_gen_asc, @function
PKCS12_key_gen_asc:
.LFB803:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%r8d, %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%ecx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	24(%rbp), %rbx
	movq	32(%rbp), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L81
	leaq	-68(%rbp), %rcx
	leaq	-64(%rbp), %rdx
	movl	%r9d, -84(%rbp)
	call	OPENSSL_asc2uni@PLT
	movl	-84(%rbp), %r9d
	testq	%rax, %rax
	je	.L82
	movl	-68(%rbp), %esi
	movq	-64(%rbp), %rdi
.L75:
	movl	16(%rbp), %eax
	subq	$8, %rsp
	movq	%r12, %rdx
	movl	%r15d, %r8d
	pushq	%r14
	movl	%r13d, %ecx
	pushq	%rbx
	pushq	%rax
	call	PKCS12_key_gen_uni
	addq	$32, %rsp
	movl	%eax, %r12d
	testl	%eax, %eax
	jle	.L79
	movslq	-68(%rbp), %rsi
	movq	-64(%rbp), %rdi
	movl	$49, %ecx
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_clear_free@PLT
.L73:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L83
	leaq	-40(%rbp), %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L81:
	.cfi_restore_state
	movq	$0, -64(%rbp)
	xorl	%esi, %esi
	movl	$0, -68(%rbp)
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L79:
	xorl	%r12d, %r12d
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L82:
	movl	$42, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r12d, %r12d
	movl	$110, %esi
	movl	$35, %edi
	call	ERR_put_error@PLT
	jmp	.L73
.L83:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE803:
	.size	PKCS12_key_gen_asc, .-PKCS12_key_gen_asc
	.p2align 4
	.globl	PKCS12_key_gen_utf8
	.type	PKCS12_key_gen_utf8, @function
PKCS12_key_gen_utf8:
.LFB804:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%r8d, %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%ecx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	24(%rbp), %rbx
	movq	32(%rbp), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L92
	leaq	-68(%rbp), %rcx
	leaq	-64(%rbp), %rdx
	movl	%r9d, -84(%rbp)
	call	OPENSSL_utf82uni@PLT
	movl	-84(%rbp), %r9d
	testq	%rax, %rax
	je	.L93
	movl	-68(%rbp), %esi
	movq	-64(%rbp), %rdi
.L86:
	movl	16(%rbp), %eax
	subq	$8, %rsp
	movq	%r12, %rdx
	movl	%r15d, %r8d
	pushq	%r14
	movl	%r13d, %ecx
	pushq	%rbx
	pushq	%rax
	call	PKCS12_key_gen_uni
	addq	$32, %rsp
	movl	%eax, %r12d
	testl	%eax, %eax
	jle	.L90
	movslq	-68(%rbp), %rsi
	movq	-64(%rbp), %rdi
	movl	$72, %ecx
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_clear_free@PLT
.L84:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L94
	leaq	-40(%rbp), %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L92:
	.cfi_restore_state
	movq	$0, -64(%rbp)
	xorl	%esi, %esi
	movl	$0, -68(%rbp)
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L90:
	xorl	%r12d, %r12d
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L93:
	movl	$65, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r12d, %r12d
	movl	$116, %esi
	movl	$35, %edi
	call	ERR_put_error@PLT
	jmp	.L84
.L94:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE804:
	.size	PKCS12_key_gen_utf8, .-PKCS12_key_gen_utf8
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
