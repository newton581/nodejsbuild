	.file	"evp_err.c"
	.text
	.p2align 4
	.globl	ERR_load_EVP_strings
	.type	ERR_load_EVP_strings, @function
ERR_load_EVP_strings:
.LFB203:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$101339136, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_func_error_string@PLT
	testq	%rax, %rax
	je	.L5
	movl	$1, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	.cfi_restore_state
	leaq	EVP_str_functs(%rip), %rdi
	call	ERR_load_strings_const@PLT
	leaq	EVP_str_reasons(%rip), %rdi
	call	ERR_load_strings_const@PLT
	movl	$1, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE203:
	.size	ERR_load_EVP_strings, .-ERR_load_EVP_strings
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"aes key setup failed"
.LC1:
	.string	"aria key setup failed"
.LC2:
	.string	"bad decrypt"
.LC3:
	.string	"bad key length"
.LC4:
	.string	"buffer too small"
.LC5:
	.string	"camellia key setup failed"
.LC6:
	.string	"cipher parameter error"
.LC7:
	.string	"command not supported"
.LC8:
	.string	"copy error"
.LC9:
	.string	"ctrl not implemented"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC10:
	.string	"ctrl operation not implemented"
	.align 8
.LC11:
	.string	"data not multiple of block length"
	.section	.rodata.str1.1
.LC12:
	.string	"decode error"
.LC13:
	.string	"different key types"
.LC14:
	.string	"different parameters"
.LC15:
	.string	"error loading section"
.LC16:
	.string	"error setting fips mode"
.LC17:
	.string	"expecting an hmac key"
.LC18:
	.string	"expecting an rsa key"
.LC19:
	.string	"expecting a dh key"
.LC20:
	.string	"expecting a dsa key"
.LC21:
	.string	"expecting a ec key"
.LC22:
	.string	"expecting a poly1305 key"
.LC23:
	.string	"expecting a siphash key"
.LC24:
	.string	"fips mode not supported"
.LC25:
	.string	"get raw key failed"
.LC26:
	.string	"illegal scrypt parameters"
.LC27:
	.string	"initialization error"
.LC28:
	.string	"input not initialized"
.LC29:
	.string	"invalid digest"
.LC30:
	.string	"invalid fips mode"
.LC31:
	.string	"invalid iv length"
.LC32:
	.string	"invalid key"
.LC33:
	.string	"invalid key length"
.LC34:
	.string	"invalid operation"
.LC35:
	.string	"keygen failure"
.LC36:
	.string	"key setup failed"
.LC37:
	.string	"memory limit exceeded"
.LC38:
	.string	"message digest is null"
.LC39:
	.string	"method not supported"
.LC40:
	.string	"missing parameters"
.LC41:
	.string	"not XOF or invalid length"
.LC42:
	.string	"no cipher set"
.LC43:
	.string	"no default digest"
.LC44:
	.string	"no digest set"
.LC45:
	.string	"no key set"
.LC46:
	.string	"no operation set"
.LC47:
	.string	"only oneshot supported"
	.section	.rodata.str1.8
	.align 8
.LC48:
	.string	"operation not supported for this keytype"
	.section	.rodata.str1.1
.LC49:
	.string	"operaton not initialized"
.LC50:
	.string	"partially overlapping buffers"
.LC51:
	.string	"pbkdf2 error"
	.section	.rodata.str1.8
	.align 8
.LC52:
	.string	"pkey application asn1 method already registered"
	.section	.rodata.str1.1
.LC53:
	.string	"private key decode error"
.LC54:
	.string	"private key encode error"
.LC55:
	.string	"public key not rsa"
.LC56:
	.string	"unknown cipher"
.LC57:
	.string	"unknown digest"
.LC58:
	.string	"unknown option"
.LC59:
	.string	"unknown pbe algorithm"
.LC60:
	.string	"unsupported algorithm"
.LC61:
	.string	"unsupported cipher"
.LC62:
	.string	"unsupported keylength"
	.section	.rodata.str1.8
	.align 8
.LC63:
	.string	"unsupported key derivation function"
	.section	.rodata.str1.1
.LC64:
	.string	"unsupported key size"
.LC65:
	.string	"unsupported number of rounds"
.LC66:
	.string	"unsupported prf"
	.section	.rodata.str1.8
	.align 8
.LC67:
	.string	"unsupported private key algorithm"
	.section	.rodata.str1.1
.LC68:
	.string	"unsupported salt type"
.LC69:
	.string	"wrap mode not allowed"
.LC70:
	.string	"wrong final block length"
.LC71:
	.string	"xts duplicated keys"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	EVP_str_reasons, @object
	.size	EVP_str_reasons, 1168
EVP_str_reasons:
	.quad	100663439
	.quad	.LC0
	.quad	100663472
	.quad	.LC1
	.quad	100663396
	.quad	.LC2
	.quad	100663491
	.quad	.LC3
	.quad	100663451
	.quad	.LC4
	.quad	100663453
	.quad	.LC5
	.quad	100663418
	.quad	.LC6
	.quad	100663443
	.quad	.LC7
	.quad	100663469
	.quad	.LC8
	.quad	100663428
	.quad	.LC9
	.quad	100663429
	.quad	.LC10
	.quad	100663434
	.quad	.LC11
	.quad	100663410
	.quad	.LC12
	.quad	100663397
	.quad	.LC13
	.quad	100663449
	.quad	.LC14
	.quad	100663461
	.quad	.LC15
	.quad	100663462
	.quad	.LC16
	.quad	100663470
	.quad	.LC17
	.quad	100663423
	.quad	.LC18
	.quad	100663424
	.quad	.LC19
	.quad	100663425
	.quad	.LC20
	.quad	100663438
	.quad	.LC21
	.quad	100663460
	.quad	.LC22
	.quad	100663471
	.quad	.LC23
	.quad	100663463
	.quad	.LC24
	.quad	100663478
	.quad	.LC25
	.quad	100663467
	.quad	.LC26
	.quad	100663430
	.quad	.LC27
	.quad	100663407
	.quad	.LC28
	.quad	100663448
	.quad	.LC29
	.quad	100663464
	.quad	.LC30
	.quad	100663490
	.quad	.LC31
	.quad	100663459
	.quad	.LC32
	.quad	100663426
	.quad	.LC33
	.quad	100663444
	.quad	.LC34
	.quad	100663416
	.quad	.LC35
	.quad	100663476
	.quad	.LC36
	.quad	100663468
	.quad	.LC37
	.quad	100663455
	.quad	.LC38
	.quad	100663440
	.quad	.LC39
	.quad	100663399
	.quad	.LC40
	.quad	100663474
	.quad	.LC41
	.quad	100663427
	.quad	.LC42
	.quad	100663454
	.quad	.LC43
	.quad	100663435
	.quad	.LC44
	.quad	100663450
	.quad	.LC45
	.quad	100663445
	.quad	.LC46
	.quad	100663473
	.quad	.LC47
	.quad	100663446
	.quad	.LC48
	.quad	100663447
	.quad	.LC49
	.quad	100663458
	.quad	.LC50
	.quad	100663477
	.quad	.LC51
	.quad	100663475
	.quad	.LC52
	.quad	100663441
	.quad	.LC53
	.quad	100663442
	.quad	.LC54
	.quad	100663402
	.quad	.LC55
	.quad	100663456
	.quad	.LC56
	.quad	100663457
	.quad	.LC57
	.quad	100663465
	.quad	.LC58
	.quad	100663417
	.quad	.LC59
	.quad	100663452
	.quad	.LC60
	.quad	100663403
	.quad	.LC61
	.quad	100663419
	.quad	.LC62
	.quad	100663420
	.quad	.LC63
	.quad	100663404
	.quad	.LC64
	.quad	100663431
	.quad	.LC65
	.quad	100663421
	.quad	.LC66
	.quad	100663414
	.quad	.LC67
	.quad	100663422
	.quad	.LC68
	.quad	100663466
	.quad	.LC69
	.quad	100663405
	.quad	.LC70
	.quad	100663479
	.quad	.LC71
	.quad	0
	.quad	0
	.section	.rodata.str1.1
.LC72:
	.string	"aesni_init_key"
.LC73:
	.string	"aesni_xts_init_key"
.LC74:
	.string	"aes_gcm_ctrl"
.LC75:
	.string	"aes_init_key"
.LC76:
	.string	"aes_ocb_cipher"
.LC77:
	.string	"aes_t4_init_key"
.LC78:
	.string	"aes_t4_xts_init_key"
.LC79:
	.string	"aes_wrap_cipher"
.LC80:
	.string	"aes_xts_init_key"
.LC81:
	.string	"alg_module_init"
.LC82:
	.string	"aria_ccm_init_key"
.LC83:
	.string	"aria_gcm_ctrl"
.LC84:
	.string	"aria_gcm_init_key"
.LC85:
	.string	"aria_init_key"
.LC86:
	.string	"b64_new"
.LC87:
	.string	"camellia_init_key"
.LC88:
	.string	"chacha20_poly1305_ctrl"
.LC89:
	.string	"cmll_t4_init_key"
.LC90:
	.string	"des_ede3_wrap_cipher"
.LC91:
	.string	"do_sigver_init"
.LC92:
	.string	"enc_new"
.LC93:
	.string	"EVP_CipherInit_ex"
.LC94:
	.string	"EVP_CIPHER_asn1_to_param"
.LC95:
	.string	"EVP_CIPHER_CTX_copy"
.LC96:
	.string	"EVP_CIPHER_CTX_ctrl"
.LC97:
	.string	"EVP_CIPHER_CTX_set_key_length"
.LC98:
	.string	"EVP_CIPHER_param_to_asn1"
.LC99:
	.string	"EVP_DecryptFinal_ex"
.LC100:
	.string	"EVP_DecryptUpdate"
.LC101:
	.string	"EVP_DigestFinalXOF"
.LC102:
	.string	"EVP_DigestInit_ex"
.LC103:
	.string	"evp_EncryptDecryptUpdate"
.LC104:
	.string	"EVP_EncryptFinal_ex"
.LC105:
	.string	"EVP_EncryptUpdate"
.LC106:
	.string	"EVP_MD_CTX_copy_ex"
.LC107:
	.string	"EVP_MD_size"
.LC108:
	.string	"EVP_OpenInit"
.LC109:
	.string	"EVP_PBE_alg_add"
.LC110:
	.string	"EVP_PBE_alg_add_type"
.LC111:
	.string	"EVP_PBE_CipherInit"
.LC112:
	.string	"EVP_PBE_scrypt"
.LC113:
	.string	"EVP_PKCS82PKEY"
.LC114:
	.string	"EVP_PKEY2PKCS8"
.LC115:
	.string	"EVP_PKEY_asn1_add0"
.LC116:
	.string	"EVP_PKEY_check"
.LC117:
	.string	"EVP_PKEY_copy_parameters"
.LC118:
	.string	"EVP_PKEY_CTX_ctrl"
.LC119:
	.string	"EVP_PKEY_CTX_ctrl_str"
.LC120:
	.string	"EVP_PKEY_CTX_dup"
.LC121:
	.string	"EVP_PKEY_CTX_md"
.LC122:
	.string	"EVP_PKEY_decrypt"
.LC123:
	.string	"EVP_PKEY_decrypt_init"
.LC124:
	.string	"EVP_PKEY_decrypt_old"
.LC125:
	.string	"EVP_PKEY_derive"
.LC126:
	.string	"EVP_PKEY_derive_init"
.LC127:
	.string	"EVP_PKEY_derive_set_peer"
.LC128:
	.string	"EVP_PKEY_encrypt"
.LC129:
	.string	"EVP_PKEY_encrypt_init"
.LC130:
	.string	"EVP_PKEY_encrypt_old"
.LC131:
	.string	"EVP_PKEY_get0_DH"
.LC132:
	.string	"EVP_PKEY_get0_DSA"
.LC133:
	.string	"EVP_PKEY_get0_EC_KEY"
.LC134:
	.string	"EVP_PKEY_get0_hmac"
.LC135:
	.string	"EVP_PKEY_get0_poly1305"
.LC136:
	.string	"EVP_PKEY_get0_RSA"
.LC137:
	.string	"EVP_PKEY_get0_siphash"
.LC138:
	.string	"EVP_PKEY_get_raw_private_key"
.LC139:
	.string	"EVP_PKEY_get_raw_public_key"
.LC140:
	.string	"EVP_PKEY_keygen"
.LC141:
	.string	"EVP_PKEY_keygen_init"
.LC142:
	.string	"EVP_PKEY_meth_add0"
.LC143:
	.string	"EVP_PKEY_meth_new"
.LC144:
	.string	"EVP_PKEY_new"
.LC145:
	.string	"EVP_PKEY_new_CMAC_key"
.LC146:
	.string	"EVP_PKEY_new_raw_private_key"
.LC147:
	.string	"EVP_PKEY_new_raw_public_key"
.LC148:
	.string	"EVP_PKEY_paramgen"
.LC149:
	.string	"EVP_PKEY_paramgen_init"
.LC150:
	.string	"EVP_PKEY_param_check"
.LC151:
	.string	"EVP_PKEY_public_check"
.LC152:
	.string	"EVP_PKEY_set1_engine"
.LC153:
	.string	"EVP_PKEY_set_alias_type"
.LC154:
	.string	"EVP_PKEY_sign"
.LC155:
	.string	"EVP_PKEY_sign_init"
.LC156:
	.string	"EVP_PKEY_verify"
.LC157:
	.string	"EVP_PKEY_verify_init"
.LC158:
	.string	"EVP_PKEY_verify_recover"
.LC159:
	.string	"EVP_PKEY_verify_recover_init"
.LC160:
	.string	"EVP_SignFinal"
.LC161:
	.string	"EVP_VerifyFinal"
.LC162:
	.string	"int_ctx_new"
.LC163:
	.string	"ok_new"
.LC164:
	.string	"PKCS5_PBE_keyivgen"
.LC165:
	.string	"PKCS5_v2_PBE_keyivgen"
.LC166:
	.string	"PKCS5_v2_PBKDF2_keyivgen"
.LC167:
	.string	"PKCS5_v2_scrypt_keyivgen"
.LC168:
	.string	"pkey_set_type"
.LC169:
	.string	"rc2_magic_to_meth"
.LC170:
	.string	"rc5_ctrl"
.LC171:
	.string	"r_32_12_16_init_key"
.LC172:
	.string	"s390x_aes_gcm_ctrl"
.LC173:
	.string	"update"
	.section	.data.rel.ro.local
	.align 32
	.type	EVP_str_functs, @object
	.size	EVP_str_functs, 1648
EVP_str_functs:
	.quad	101339136
	.quad	.LC72
	.quad	101511168
	.quad	.LC73
	.quad	101466112
	.quad	.LC74
	.quad	101208064
	.quad	.LC75
	.quad	101355520
	.quad	.LC76
	.quad	101392384
	.quad	.LC77
	.quad	101515264
	.quad	.LC78
	.quad	101359616
	.quad	.LC79
	.quad	101519360
	.quad	.LC80
	.quad	101388288
	.quad	.LC81
	.quad	101380096
	.quad	.LC82
	.quad	101470208
	.quad	.LC83
	.quad	101384192
	.quad	.LC84
	.quad	101421056
	.quad	.LC85
	.quad	101474304
	.quad	.LC86
	.quad	101314560
	.quad	.LC87
	.quad	101408768
	.quad	.LC88
	.quad	101396480
	.quad	.LC89
	.quad	101363712
	.quad	.LC90
	.quad	101322752
	.quad	.LC91
	.quad	101478400
	.quad	.LC92
	.quad	101167104
	.quad	.LC93
	.quad	101498880
	.quad	.LC94
	.quad	101330944
	.quad	.LC95
	.quad	101171200
	.quad	.LC96
	.quad	101163008
	.quad	.LC97
	.quad	101502976
	.quad	.LC98
	.quad	101076992
	.quad	.LC99
	.quad	101343232
	.quad	.LC100
	.quad	101376000
	.quad	.LC101
	.quad	101187584
	.quad	.LC102
	.quad	101560320
	.quad	.LC103
	.quad	101183488
	.quad	.LC104
	.quad	101347328
	.quad	.LC105
	.quad	101113856
	.quad	.LC106
	.quad	101326848
	.quad	.LC107
	.quad	101081088
	.quad	.LC108
	.quad	101134336
	.quad	.LC109
	.quad	101318656
	.quad	.LC110
	.quad	101138432
	.quad	.LC111
	.quad	101404672
	.quad	.LC112
	.quad	101117952
	.quad	.LC113
	.quad	101126144
	.quad	.LC114
	.quad	101433344
	.quad	.LC115
	.quad	101425152
	.quad	.LC116
	.quad	101085184
	.quad	.LC117
	.quad	101224448
	.quad	.LC118
	.quad	101277696
	.quad	.LC119
	.quad	101302272
	.quad	.LC120
	.quad	101351424
	.quad	.LC121
	.quad	101089280
	.quad	.LC122
	.quad	101228544
	.quad	.LC123
	.quad	101281792
	.quad	.LC124
	.quad	101289984
	.quad	.LC125
	.quad	101294080
	.quad	.LC126
	.quad	101298176
	.quad	.LC127
	.quad	101093376
	.quad	.LC128
	.quad	101232640
	.quad	.LC129
	.quad	101285888
	.quad	.LC130
	.quad	101150720
	.quad	.LC131
	.quad	101154816
	.quad	.LC132
	.quad	101199872
	.quad	.LC133
	.quad	101412864
	.quad	.LC134
	.quad	101416960
	.quad	.LC135
	.quad	101158912
	.quad	.LC136
	.quad	101367808
	.quad	.LC137
	.quad	101490688
	.quad	.LC138
	.quad	101494784
	.quad	.LC139
	.quad	101261312
	.quad	.LC140
	.quad	101265408
	.quad	.LC141
	.quad	101457920
	.quad	.LC142
	.quad	101462016
	.quad	.LC143
	.quad	101097472
	.quad	.LC144
	.quad	101453824
	.quad	.LC145
	.quad	101445632
	.quad	.LC146
	.quad	101449728
	.quad	.LC147
	.quad	101269504
	.quad	.LC148
	.quad	101273600
	.quad	.LC149
	.quad	101437440
	.quad	.LC150
	.quad	101441536
	.quad	.LC151
	.quad	101429248
	.quad	.LC152
	.quad	101507072
	.quad	.LC153
	.quad	101236736
	.quad	.LC154
	.quad	101240832
	.quad	.LC155
	.quad	101244928
	.quad	.LC156
	.quad	101249024
	.quad	.LC157
	.quad	101253120
	.quad	.LC158
	.quad	101257216
	.quad	.LC159
	.quad	101101568
	.quad	.LC160
	.quad	101105664
	.quad	.LC161
	.quad	101306368
	.quad	.LC162
	.quad	101482496
	.quad	.LC163
	.quad	101142528
	.quad	.LC164
	.quad	101146624
	.quad	.LC165
	.quad	101335040
	.quad	.LC166
	.quad	101400576
	.quad	.LC167
	.quad	101310464
	.quad	.LC168
	.quad	101109760
	.quad	.LC169
	.quad	101175296
	.quad	.LC170
	.quad	101654528
	.quad	.LC171
	.quad	101486592
	.quad	.LC172
	.quad	101371904
	.quad	.LC173
	.quad	0
	.quad	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
