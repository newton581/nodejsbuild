	.file	"a_bitstr.c"
	.text
	.p2align 4
	.globl	ASN1_BIT_STRING_set
	.type	ASN1_BIT_STRING_set, @function
ASN1_BIT_STRING_set:
.LFB467:
	.cfi_startproc
	endbr64
	jmp	ASN1_STRING_set@PLT
	.cfi_endproc
.LFE467:
	.size	ASN1_BIT_STRING_set, .-ASN1_BIT_STRING_set
	.p2align 4
	.globl	i2c_ASN1_BIT_STRING
	.type	i2c_ASN1_BIT_STRING, @function
i2c_ASN1_BIT_STRING:
.LFB468:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testq	%rdi, %rdi
	je	.L15
	movslq	(%rdi), %rbx
	movq	%rsi, %r12
	testl	%ebx, %ebx
	jle	.L5
	movq	16(%rdi), %rcx
	testb	$8, %cl
	je	.L6
	andl	$7, %ecx
	leal	1(%rbx), %r13d
	testq	%rsi, %rsi
	je	.L3
	movq	(%rsi), %rax
	movl	$255, %r14d
	sall	%cl, %r14d
	movb	%cl, (%rax)
	movq	8(%rdi), %rsi
	leaq	1(%rax), %r8
.L14:
	movq	%r8, %rdi
	movq	%rbx, %rdx
	call	memcpy@PLT
	movq	%rax, %r8
	addq	%rbx, %r8
	andb	%r14b, -1(%r8)
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L5:
	leal	1(%rbx), %r13d
	testq	%rsi, %rsi
	je	.L3
	movq	(%r12), %rax
	movb	$0, (%rax)
	leaq	1(%rax), %r8
.L12:
	movq	%r8, (%r12)
.L3:
	popq	%rbx
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	.cfi_restore_state
	movq	8(%rdi), %rcx
	.p2align 4,,10
	.p2align 3
.L9:
	movzbl	-1(%rcx,%rbx), %eax
	movl	%ebx, %edx
	testb	%al, %al
	jne	.L8
	subq	$1, %rbx
	movl	%ebx, %edx
	testl	%ebx, %ebx
	jne	.L9
	movzbl	-1(%rcx), %eax
	movl	$1, %r13d
	xorl	%ebx, %ebx
.L10:
	testb	$1, %al
	jne	.L16
	testb	$2, %al
	jne	.L17
	testb	$4, %al
	jne	.L18
	testb	$8, %al
	jne	.L19
	testb	$16, %al
	jne	.L20
	testb	$32, %al
	jne	.L21
	movl	%eax, %ecx
	andl	$64, %ecx
	jne	.L22
	movl	%eax, %r14d
	sarb	$7, %r14b
	andl	$-127, %r14d
	subl	$1, %r14d
	testb	%al, %al
	movl	$7, %eax
	cmovs	%eax, %ecx
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L15:
	xorl	%r13d, %r13d
	popq	%rbx
	popq	%r12
	movl	%r13d, %eax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	.cfi_restore_state
	movl	$-1, %r14d
	xorl	%ecx, %ecx
.L11:
	testq	%r12, %r12
	je	.L3
	movq	(%r12), %rax
	movb	%cl, (%rax)
	leaq	1(%rax), %r8
	testl	%edx, %edx
	je	.L12
	movq	8(%rdi), %rsi
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L8:
	leal	1(%rbx), %r13d
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L17:
	movl	$-2, %r14d
	movl	$1, %ecx
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L18:
	movl	$-4, %r14d
	movl	$2, %ecx
	jmp	.L11
.L19:
	movl	$-8, %r14d
	movl	$3, %ecx
	jmp	.L11
.L20:
	movl	$-16, %r14d
	movl	$4, %ecx
	jmp	.L11
.L21:
	movl	$-32, %r14d
	movl	$5, %ecx
	jmp	.L11
.L22:
	movl	$-64, %r14d
	movl	$6, %ecx
	jmp	.L11
	.cfi_endproc
.LFE468:
	.size	i2c_ASN1_BIT_STRING, .-i2c_ASN1_BIT_STRING
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/asn1/a_bitstr.c"
	.text
	.p2align 4
	.globl	c2i_ASN1_BIT_STRING
	.type	c2i_ASN1_BIT_STRING, @function
c2i_ASN1_BIT_STRING:
.LFB469:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%rdx, %rdx
	jle	.L53
	movq	%rdx, %rbx
	cmpq	$2147483647, %rdx
	jg	.L54
	movq	%rsi, %r15
	testq	%rdi, %rdi
	je	.L42
	movq	(%rdi), %r12
	testq	%r12, %r12
	je	.L42
	movq	(%rsi), %r13
	movzbl	0(%r13), %ecx
	movl	%ecx, %eax
	cmpl	$7, %ecx
	jg	.L51
.L68:
	movq	16(%r12), %rdx
	orl	$8, %eax
	movl	%ecx, -56(%rbp)
	addq	$1, %r13
	movzbl	%al, %eax
	leaq	-1(%rbx), %r9
	andq	$-16, %rdx
	orq	%rdx, %rax
	movq	%rax, 16(%r12)
	cmpq	$1, %rbx
	je	.L56
	movl	$117, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r9, %rdi
	movq	%r9, -64(%rbp)
	call	CRYPTO_malloc@PLT
	testq	%rax, %rax
	je	.L57
	movq	-64(%rbp), %r9
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%r9, %rdx
	call	memcpy@PLT
	movl	-56(%rbp), %ecx
	movq	-64(%rbp), %r9
	movq	%rax, %r8
	movl	$255, %eax
	sall	%cl, %eax
	andb	%al, -2(%r8,%rbx)
	addq	%r9, %r13
.L46:
	movl	%r9d, (%r12)
	movq	8(%r12), %rdi
	movl	$129, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r8, -56(%rbp)
	call	CRYPTO_free@PLT
	movq	-56(%rbp), %r8
	movl	$3, 4(%r12)
	movq	%r8, 8(%r12)
	testq	%r14, %r14
	je	.L47
	movq	%r12, (%r14)
.L47:
	movq	%r13, (%r15)
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L54:
	movl	$151, %edx
	xorl	%r12d, %r12d
.L41:
	movl	$137, %r8d
	movl	$189, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	testq	%r14, %r14
	je	.L48
.L50:
	cmpq	%r12, (%r14)
	je	.L69
.L48:
	movq	%r12, %rdi
	call	ASN1_BIT_STRING_free@PLT
.L69:
	xorl	%r12d, %r12d
.L40:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L53:
	.cfi_restore_state
	movl	$152, %edx
	xorl	%r12d, %r12d
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L42:
	call	ASN1_BIT_STRING_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L69
	movq	(%r15), %r13
	movzbl	0(%r13), %ecx
	movl	%ecx, %eax
	cmpl	$7, %ecx
	jle	.L68
	movl	$220, %edx
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L51:
	movl	$137, %r8d
	movl	$220, %edx
	movl	$189, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L57:
	movl	$65, %edx
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L56:
	xorl	%r8d, %r8d
	jmp	.L46
	.cfi_endproc
.LFE469:
	.size	c2i_ASN1_BIT_STRING, .-c2i_ASN1_BIT_STRING
	.p2align 4
	.globl	ASN1_BIT_STRING_set_bit
	.type	ASN1_BIT_STRING_set_bit, @function
ASN1_BIT_STRING_set_bit:
.LFB470:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	$0, %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leal	7(%rsi), %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movl	$1, %r12d
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	testl	%esi, %esi
	cmovns	%esi, %r13d
	notl	%esi
	movl	%esi, %ecx
	andl	$7, %ecx
	sarl	$3, %r13d
	sall	%cl, %r12d
	testl	%edx, %edx
	cmovne	%r12d, %r15d
	testq	%rdi, %rdi
	je	.L82
	movslq	(%rdi), %rsi
	andq	$-16, 16(%rdi)
	movq	%rdi, %rbx
	cmpl	%r13d, %esi
	jle	.L73
	movq	8(%rdi), %r14
	testq	%r14, %r14
	je	.L73
.L74:
	movslq	%r13d, %r13
	notl	%r12d
	addq	%r13, %r14
	andb	(%r14), %r12b
	orl	%r15d, %r12d
	movb	%r12b, (%r14)
	movslq	(%rbx), %rdx
	testl	%edx, %edx
	jle	.L78
	leal	-1(%rdx), %esi
	subq	$2, %rdx
	movq	8(%rbx), %rcx
	movslq	%esi, %rax
	movl	%esi, %esi
	subq	%rsi, %rdx
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L80:
	movl	%eax, (%rbx)
	subq	$1, %rax
	cmpq	%rax, %rdx
	je	.L78
.L79:
	cmpb	$0, (%rcx,%rax)
	je	.L80
.L78:
	movl	$1, %eax
.L70:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L73:
	.cfi_restore_state
	testl	%edx, %edx
	je	.L78
	leal	1(%r13), %r9d
	movq	8(%rbx), %rdi
	movl	$165, %r8d
	leaq	.LC0(%rip), %rcx
	movslq	%r9d, %rdx
	movl	%r9d, -52(%rbp)
	call	CRYPTO_clear_realloc@PLT
	movl	-52(%rbp), %r9d
	testq	%rax, %rax
	movq	%rax, %r14
	je	.L84
	movslq	(%rbx), %rax
	movl	%r9d, %edx
	subl	%eax, %edx
	testl	%edx, %edx
	jg	.L85
.L77:
	movq	%r14, 8(%rbx)
	movl	%r9d, (%rbx)
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L85:
	movslq	%edx, %rdx
	leaq	(%r14,%rax), %rdi
	xorl	%esi, %esi
	movl	%r9d, -52(%rbp)
	call	memset@PLT
	movl	-52(%rbp), %r9d
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L82:
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L84:
	.cfi_restore_state
	movl	$167, %r8d
	movl	$65, %edx
	movl	$183, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L70
	.cfi_endproc
.LFE470:
	.size	ASN1_BIT_STRING_set_bit, .-ASN1_BIT_STRING_set_bit
	.p2align 4
	.globl	ASN1_BIT_STRING_get_bit
	.type	ASN1_BIT_STRING_get_bit, @function
ASN1_BIT_STRING_get_bit:
.LFB471:
	.cfi_startproc
	endbr64
	testl	%esi, %esi
	leal	7(%rsi), %eax
	cmovns	%esi, %eax
	xorl	%r8d, %r8d
	sarl	$3, %eax
	testq	%rdi, %rdi
	je	.L86
	cmpl	%eax, (%rdi)
	jle	.L86
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L86
	notl	%esi
	cltq
	movl	$1, %edx
	xorl	%r8d, %r8d
	movl	%esi, %ecx
	movzbl	(%rdi,%rax), %eax
	andl	$7, %ecx
	sall	%cl, %edx
	testl	%eax, %edx
	setne	%r8b
.L86:
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE471:
	.size	ASN1_BIT_STRING_get_bit, .-ASN1_BIT_STRING_get_bit
	.p2align 4
	.globl	ASN1_BIT_STRING_check
	.type	ASN1_BIT_STRING_check, @function
ASN1_BIT_STRING_check:
.LFB472:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L98
	movq	8(%rdi), %r8
	testq	%r8, %r8
	je	.L98
	movl	(%rdi), %eax
	testl	%eax, %eax
	jle	.L98
	leal	-1(%rax), %edi
	xorl	%eax, %eax
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L94:
	addq	$1, %rax
	testb	%cl, %cl
	jne	.L102
.L95:
	movl	$-1, %ecx
	cmpl	%eax, %edx
	jle	.L93
	movzbl	(%rsi,%rax), %ecx
	notl	%ecx
.L93:
	andb	(%r8,%rax), %cl
	cmpq	%rax, %rdi
	jne	.L94
	xorl	%eax, %eax
	testb	%cl, %cl
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L98:
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L102:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE472:
	.size	ASN1_BIT_STRING_check, .-ASN1_BIT_STRING_check
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
