	.file	"x86_64-gcc.c"
	.text
	.p2align 4
	.globl	bn_mul_add_words
	.type	bn_mul_add_words, @function
bn_mul_add_words:
.LFB141:
	.cfi_startproc
	endbr64
	movl	%edx, %r10d
	xorl	%r9d, %r9d
	testl	%edx, %edx
	jle	.L1
	testl	$-4, %edx
	je	.L3
	.p2align 4,,10
	.p2align 3
.L4:
	subl	$4, %r10d
	movq	%rcx, %rax
	addq	$32, %rsi
#APP
# 120 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq -32(%rsi)
# 0 "" 2
# 120 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%r9; adcq $0,%rdx
# 0 "" 2
#NO_APP
	movq	%rcx, %rax
#APP
# 120 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %r9,(%rdi); adcq $0,%rdx
# 0 "" 2
#NO_APP
	movq	%rdx, %r8
#APP
# 121 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq -24(%rsi)
# 0 "" 2
# 121 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%r8; adcq $0,%rdx
# 0 "" 2
#NO_APP
	movq	%rcx, %rax
#APP
# 121 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %r8,8(%rdi); adcq $0,%rdx
# 0 "" 2
#NO_APP
	movq	%rdx, %r9
#APP
# 122 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq -16(%rsi)
# 0 "" 2
# 122 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%r9; adcq $0,%rdx
# 0 "" 2
#NO_APP
	movq	%rcx, %rax
#APP
# 122 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %r9,16(%rdi); adcq $0,%rdx
# 0 "" 2
#NO_APP
	movq	%rdx, %r8
#APP
# 123 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq -8(%rsi)
# 0 "" 2
# 123 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%r8; adcq $0,%rdx
# 0 "" 2
# 123 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %r8,24(%rdi); adcq $0,%rdx
# 0 "" 2
#NO_APP
	addq	$32, %rdi
	movq	%rdx, %r9
	testl	$-4, %r10d
	jne	.L4
	testl	%r10d, %r10d
	jne	.L3
.L1:
	movq	%r9, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	movq	%rcx, %rax
#APP
# 129 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq (%rsi)
# 0 "" 2
# 129 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%r9; adcq $0,%rdx
# 0 "" 2
# 129 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %r9,(%rdi); adcq $0,%rdx
# 0 "" 2
#NO_APP
	movq	%rdx, %r9
	cmpl	$1, %r10d
	je	.L1
	movq	%rcx, %rax
#APP
# 132 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq 8(%rsi)
# 0 "" 2
# 132 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%r9; adcq $0,%rdx
# 0 "" 2
# 132 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %r9,8(%rdi); adcq $0,%rdx
# 0 "" 2
#NO_APP
	movq	%rdx, %r9
	cmpl	$2, %r10d
	je	.L1
	movq	%rcx, %rax
#APP
# 135 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq 16(%rsi)
# 0 "" 2
# 135 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%r9; adcq $0,%rdx
# 0 "" 2
# 135 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %r9,16(%rdi); adcq $0,%rdx
# 0 "" 2
#NO_APP
	movq	%rdx, %r9
	movq	%r9, %rax
	ret
	.cfi_endproc
.LFE141:
	.size	bn_mul_add_words, .-bn_mul_add_words
	.p2align 4
	.globl	bn_mul_words
	.type	bn_mul_words, @function
bn_mul_words:
.LFB142:
	.cfi_startproc
	endbr64
	movl	%edx, %r10d
	xorl	%r9d, %r9d
	testl	%edx, %edx
	jle	.L17
	testl	$-4, %edx
	je	.L19
	.p2align 4,,10
	.p2align 3
.L20:
	subl	$4, %r10d
	movq	%rcx, %rax
	addq	$32, %rsi
	addq	$32, %rdi
#APP
# 150 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq -32(%rsi)
# 0 "" 2
# 150 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%r9; adcq $0,%rdx
# 0 "" 2
#NO_APP
	movq	%rcx, %rax
	movq	%r9, -32(%rdi)
	movq	%rdx, %r8
#APP
# 151 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq -24(%rsi)
# 0 "" 2
# 151 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%r8; adcq $0,%rdx
# 0 "" 2
#NO_APP
	movq	%rcx, %rax
	movq	%r8, -24(%rdi)
	movq	%rdx, %r9
#APP
# 152 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq -16(%rsi)
# 0 "" 2
# 152 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%r9; adcq $0,%rdx
# 0 "" 2
#NO_APP
	movq	%rcx, %rax
	movq	%r9, -16(%rdi)
	movq	%rdx, %r8
#APP
# 153 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq -8(%rsi)
# 0 "" 2
# 153 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%r8; adcq $0,%rdx
# 0 "" 2
#NO_APP
	movq	%r8, -8(%rdi)
	movq	%rdx, %r9
	testl	$-4, %r10d
	jne	.L20
	testl	%r10d, %r10d
	jne	.L19
.L17:
	movq	%r9, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	movq	%rcx, %rax
#APP
# 159 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq (%rsi)
# 0 "" 2
# 159 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%r9; adcq $0,%rdx
# 0 "" 2
#NO_APP
	movq	%r9, (%rdi)
	movq	%rdx, %r9
	cmpl	$1, %r10d
	je	.L17
	movq	%rcx, %rax
#APP
# 162 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq 8(%rsi)
# 0 "" 2
# 162 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%r9; adcq $0,%rdx
# 0 "" 2
#NO_APP
	movq	%r9, 8(%rdi)
	movq	%rdx, %r9
	cmpl	$2, %r10d
	je	.L17
	movq	%rcx, %rax
	movq	%r9, %rcx
#APP
# 165 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq 16(%rsi)
# 0 "" 2
# 165 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%rcx; adcq $0,%rdx
# 0 "" 2
#NO_APP
	movq	%rdx, %r9
	movq	%rcx, 16(%rdi)
	movq	%r9, %rax
	ret
	.cfi_endproc
.LFE142:
	.size	bn_mul_words, .-bn_mul_words
	.p2align 4
	.globl	bn_sqr_words
	.type	bn_sqr_words, @function
bn_sqr_words:
.LFB143:
	.cfi_startproc
	endbr64
	movl	%edx, %ecx
	testl	%edx, %edx
	jle	.L31
	testl	$-4, %edx
	je	.L34
	.p2align 4,,10
	.p2align 3
.L35:
	subl	$4, %ecx
	movq	(%rsi), %rax
	addq	$64, %rdi
	addq	$32, %rsi
#APP
# 176 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq %rax
# 0 "" 2
#NO_APP
	movq	%rdx, -56(%rdi)
	movq	%rax, -64(%rdi)
	movq	-24(%rsi), %rax
#APP
# 177 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq %rax
# 0 "" 2
#NO_APP
	movq	%rdx, -40(%rdi)
	movq	%rax, -48(%rdi)
	movq	-16(%rsi), %rax
#APP
# 178 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq %rax
# 0 "" 2
#NO_APP
	movq	%rdx, -24(%rdi)
	movq	%rax, -32(%rdi)
	movq	-8(%rsi), %rax
#APP
# 179 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq %rax
# 0 "" 2
#NO_APP
	movq	%rax, -16(%rdi)
	movq	%rdx, -8(%rdi)
	testl	$-4, %ecx
	jne	.L35
	testl	%ecx, %ecx
	jne	.L34
.L31:
	ret
	.p2align 4,,10
	.p2align 3
.L34:
	movq	(%rsi), %rax
#APP
# 185 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq %rax
# 0 "" 2
#NO_APP
	movq	%rax, (%rdi)
	movq	%rdx, 8(%rdi)
	cmpl	$1, %ecx
	je	.L31
	movq	8(%rsi), %rax
#APP
# 188 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq %rax
# 0 "" 2
#NO_APP
	movq	%rax, 16(%rdi)
	movq	%rdx, 24(%rdi)
	cmpl	$2, %ecx
	je	.L31
	movq	16(%rsi), %rax
#APP
# 191 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq %rax
# 0 "" 2
#NO_APP
	movq	%rax, 32(%rdi)
	movq	%rdx, 40(%rdi)
	ret
	.cfi_endproc
.LFE143:
	.size	bn_sqr_words, .-bn_sqr_words
	.p2align 4
	.globl	bn_div_words
	.type	bn_div_words, @function
bn_div_words:
.LFB144:
	.cfi_startproc
	endbr64
	movq	%rdx, %r8
	movq	%rsi, %rax
	movq	%rdi, %rdx
#APP
# 199 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	divq      %r8
# 0 "" 2
#NO_APP
	ret
	.cfi_endproc
.LFE144:
	.size	bn_div_words, .-bn_div_words
	.p2align 4
	.globl	bn_add_words
	.type	bn_add_words, @function
bn_add_words:
.LFB145:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jle	.L51
#APP
# 215 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	       subq    %r8,%r8           
       jmp     1f              
.p2align 4                     
1:     movq    (%rsi,%rax,8),%r8    
       adcq    (%rdx,%rax,8),%r8    
       movq    %r8,(%rdi,%rax,8)    
       lea     1(%rax),%rax        
       dec     %ecx              
       jnz     1b              
       sbbq    %r8,%r8           

# 0 "" 2
#NO_APP
	movq	%r8, %rax
	andl	$1, %eax
.L51:
	ret
	.cfi_endproc
.LFE145:
	.size	bn_add_words, .-bn_add_words
	.p2align 4
	.globl	bn_sub_words
	.type	bn_sub_words, @function
bn_sub_words:
.LFB146:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jle	.L54
#APP
# 242 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	       subq    %r8,%r8           
       jmp     1f              
.p2align 4                     
1:     movq    (%rsi,%rax,8),%r8    
       sbbq    (%rdx,%rax,8),%r8    
       movq    %r8,(%rdi,%rax,8)    
       lea     1(%rax),%rax        
       dec     %ecx              
       jnz     1b              
       sbbq    %r8,%r8           

# 0 "" 2
#NO_APP
	movq	%r8, %rax
	andl	$1, %eax
.L54:
	ret
	.cfi_endproc
.LFE146:
	.size	bn_sub_words, .-bn_sub_words
	.p2align 4
	.globl	bn_mul_comba8
	.type	bn_mul_comba8, @function
bn_mul_comba8:
.LFB147:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r10d, %r10d
	movq	%rdx, %r8
	movq	(%rsi), %rax
	movq	%r10, %rcx
	movq	%r10, %r11
	movq	%r10, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%r10, %rbx
#APP
# 401 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq (%rdx)
# 0 "" 2
# 401 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%rcx; adcq %rdx,%r11; adcq $0,%r9
# 0 "" 2
#NO_APP
	movq	%rcx, (%rdi)
	movq	%r10, %rcx
	movq	(%rsi), %rax
#APP
# 404 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq 8(%r8)
# 0 "" 2
# 404 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%r11; adcq %rdx,%r9; adcq $0,%rcx
# 0 "" 2
#NO_APP
	movq	8(%rsi), %rax
#APP
# 405 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq (%r8)
# 0 "" 2
# 405 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%r11; adcq %rdx,%r9; adcq $0,%rcx
# 0 "" 2
#NO_APP
	movq	%r11, 8(%rdi)
	movq	16(%rsi), %rax
#APP
# 408 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq (%r8)
# 0 "" 2
# 408 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%r9; adcq %rdx,%rcx; adcq $0,%rbx
# 0 "" 2
#NO_APP
	movq	8(%rsi), %rax
	movq	%r9, %r11
	movq	%rbx, %r9
	movq	%r10, %rbx
#APP
# 409 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq 8(%r8)
# 0 "" 2
# 409 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%r11; adcq %rdx,%rcx; adcq $0,%r9
# 0 "" 2
#NO_APP
	movq	(%rsi), %rax
#APP
# 410 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq 16(%r8)
# 0 "" 2
# 410 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%r11; adcq %rdx,%rcx; adcq $0,%r9
# 0 "" 2
#NO_APP
	movq	%r11, 16(%rdi)
	movq	(%rsi), %rax
#APP
# 413 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq 24(%r8)
# 0 "" 2
# 413 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%rcx; adcq %rdx,%r9; adcq $0,%rbx
# 0 "" 2
#NO_APP
	movq	8(%rsi), %rax
#APP
# 414 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq 16(%r8)
# 0 "" 2
# 414 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%rcx; adcq %rdx,%r9; adcq $0,%rbx
# 0 "" 2
#NO_APP
	movq	16(%rsi), %rax
#APP
# 415 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq 8(%r8)
# 0 "" 2
# 415 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%rcx; adcq %rdx,%r9; adcq $0,%rbx
# 0 "" 2
#NO_APP
	movq	24(%rsi), %rax
	movq	%rcx, %r11
	movq	%rbx, %rcx
	movq	%r10, %rbx
#APP
# 416 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq (%r8)
# 0 "" 2
# 416 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%r11; adcq %rdx,%r9; adcq $0,%rcx
# 0 "" 2
#NO_APP
	movq	%r11, 24(%rdi)
	movq	32(%rsi), %rax
#APP
# 419 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq (%r8)
# 0 "" 2
# 419 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%r9; adcq %rdx,%rcx; adcq $0,%rbx
# 0 "" 2
#NO_APP
	movq	24(%rsi), %rax
#APP
# 420 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq 8(%r8)
# 0 "" 2
# 420 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%r9; adcq %rdx,%rcx; adcq $0,%rbx
# 0 "" 2
#NO_APP
	movq	16(%rsi), %rax
#APP
# 421 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq 16(%r8)
# 0 "" 2
# 421 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%r9; adcq %rdx,%rcx; adcq $0,%rbx
# 0 "" 2
#NO_APP
	movq	8(%rsi), %rax
#APP
# 422 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq 24(%r8)
# 0 "" 2
# 422 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%r9; adcq %rdx,%rcx; adcq $0,%rbx
# 0 "" 2
#NO_APP
	movq	(%rsi), %rax
	movq	%r9, %r11
	movq	%rbx, %r9
	movq	%r10, %rbx
#APP
# 423 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq 32(%r8)
# 0 "" 2
# 423 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%r11; adcq %rdx,%rcx; adcq $0,%r9
# 0 "" 2
#NO_APP
	movq	%r11, 32(%rdi)
	movq	(%rsi), %rax
#APP
# 426 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq 40(%r8)
# 0 "" 2
# 426 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%rcx; adcq %rdx,%r9; adcq $0,%rbx
# 0 "" 2
#NO_APP
	movq	8(%rsi), %rax
#APP
# 427 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq 32(%r8)
# 0 "" 2
# 427 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%rcx; adcq %rdx,%r9; adcq $0,%rbx
# 0 "" 2
#NO_APP
	movq	16(%rsi), %rax
#APP
# 428 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq 24(%r8)
# 0 "" 2
# 428 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%rcx; adcq %rdx,%r9; adcq $0,%rbx
# 0 "" 2
#NO_APP
	movq	24(%rsi), %rax
#APP
# 429 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq 16(%r8)
# 0 "" 2
# 429 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%rcx; adcq %rdx,%r9; adcq $0,%rbx
# 0 "" 2
#NO_APP
	movq	32(%rsi), %rax
#APP
# 430 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq 8(%r8)
# 0 "" 2
# 430 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%rcx; adcq %rdx,%r9; adcq $0,%rbx
# 0 "" 2
#NO_APP
	movq	40(%rsi), %rax
	movq	%rcx, %r11
	movq	%rbx, %rcx
	movq	%r10, %rbx
#APP
# 431 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq (%r8)
# 0 "" 2
# 431 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%r11; adcq %rdx,%r9; adcq $0,%rcx
# 0 "" 2
#NO_APP
	movq	%r11, 40(%rdi)
	movq	48(%rsi), %rax
#APP
# 434 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq (%r8)
# 0 "" 2
# 434 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%r9; adcq %rdx,%rcx; adcq $0,%rbx
# 0 "" 2
#NO_APP
	movq	40(%rsi), %rax
#APP
# 435 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq 8(%r8)
# 0 "" 2
# 435 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%r9; adcq %rdx,%rcx; adcq $0,%rbx
# 0 "" 2
#NO_APP
	movq	32(%rsi), %rax
#APP
# 436 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq 16(%r8)
# 0 "" 2
# 436 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%r9; adcq %rdx,%rcx; adcq $0,%rbx
# 0 "" 2
#NO_APP
	movq	24(%rsi), %rax
#APP
# 437 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq 24(%r8)
# 0 "" 2
# 437 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%r9; adcq %rdx,%rcx; adcq $0,%rbx
# 0 "" 2
#NO_APP
	movq	16(%rsi), %rax
#APP
# 438 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq 32(%r8)
# 0 "" 2
# 438 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%r9; adcq %rdx,%rcx; adcq $0,%rbx
# 0 "" 2
#NO_APP
	movq	8(%rsi), %rax
#APP
# 439 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq 40(%r8)
# 0 "" 2
# 439 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%r9; adcq %rdx,%rcx; adcq $0,%rbx
# 0 "" 2
#NO_APP
	movq	(%rsi), %rax
	movq	%r9, %r11
	movq	%rbx, %r9
#APP
# 440 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq 48(%r8)
# 0 "" 2
# 440 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%r11; adcq %rdx,%rcx; adcq $0,%r9
# 0 "" 2
#NO_APP
	movq	%r11, 48(%rdi)
	movq	%r10, %r11
	movq	(%rsi), %rax
#APP
# 443 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq 56(%r8)
# 0 "" 2
# 443 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%rcx; adcq %rdx,%r9; adcq $0,%r11
# 0 "" 2
#NO_APP
	movq	8(%rsi), %rax
	movq	%r9, %rbx
#APP
# 444 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq 48(%r8)
# 0 "" 2
# 444 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%rcx; adcq %rdx,%rbx; adcq $0,%r11
# 0 "" 2
#NO_APP
	movq	16(%rsi), %rax
#APP
# 445 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq 40(%r8)
# 0 "" 2
# 445 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%rcx; adcq %rdx,%rbx; adcq $0,%r11
# 0 "" 2
#NO_APP
	movq	24(%rsi), %rax
#APP
# 446 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq 32(%r8)
# 0 "" 2
# 446 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%rcx; adcq %rdx,%rbx; adcq $0,%r11
# 0 "" 2
#NO_APP
	movq	32(%rsi), %rax
#APP
# 447 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq 24(%r8)
# 0 "" 2
# 447 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%rcx; adcq %rdx,%rbx; adcq $0,%r11
# 0 "" 2
#NO_APP
	movq	40(%rsi), %rax
#APP
# 448 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq 16(%r8)
# 0 "" 2
# 448 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%rcx; adcq %rdx,%rbx; adcq $0,%r11
# 0 "" 2
#NO_APP
	movq	48(%rsi), %rax
#APP
# 449 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq 8(%r8)
# 0 "" 2
# 449 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%rcx; adcq %rdx,%rbx; adcq $0,%r11
# 0 "" 2
#NO_APP
	movq	56(%rsi), %rax
	movq	%rcx, %r9
	movq	%rbx, %rcx
	movq	%r10, %rbx
#APP
# 450 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq (%r8)
# 0 "" 2
# 450 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%r9; adcq %rdx,%rcx; adcq $0,%r11
# 0 "" 2
#NO_APP
	movq	%r9, 56(%rdi)
	movq	56(%rsi), %rax
#APP
# 453 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq 8(%r8)
# 0 "" 2
# 453 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%rcx; adcq %rdx,%r11; adcq $0,%rbx
# 0 "" 2
#NO_APP
	movq	48(%rsi), %rax
#APP
# 454 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq 16(%r8)
# 0 "" 2
# 454 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%rcx; adcq %rdx,%r11; adcq $0,%rbx
# 0 "" 2
#NO_APP
	movq	40(%rsi), %rax
#APP
# 455 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq 24(%r8)
# 0 "" 2
# 455 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%rcx; adcq %rdx,%r11; adcq $0,%rbx
# 0 "" 2
#NO_APP
	movq	32(%rsi), %rax
#APP
# 456 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq 32(%r8)
# 0 "" 2
# 456 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%rcx; adcq %rdx,%r11; adcq $0,%rbx
# 0 "" 2
#NO_APP
	movq	24(%rsi), %rax
#APP
# 457 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq 40(%r8)
# 0 "" 2
# 457 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%rcx; adcq %rdx,%r11; adcq $0,%rbx
# 0 "" 2
#NO_APP
	movq	16(%rsi), %rax
#APP
# 458 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq 48(%r8)
# 0 "" 2
# 458 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%rcx; adcq %rdx,%r11; adcq $0,%rbx
# 0 "" 2
#NO_APP
	movq	8(%rsi), %rax
	movq	%rcx, %r9
	movq	%rbx, %rcx
#APP
# 459 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq 56(%r8)
# 0 "" 2
# 459 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%r9; adcq %rdx,%r11; adcq $0,%rcx
# 0 "" 2
#NO_APP
	movq	%r9, 64(%rdi)
	movq	%r10, %r9
	movq	16(%rsi), %rax
#APP
# 462 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq 56(%r8)
# 0 "" 2
# 462 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%r11; adcq %rdx,%rcx; adcq $0,%r9
# 0 "" 2
#NO_APP
	movq	24(%rsi), %rax
#APP
# 463 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq 48(%r8)
# 0 "" 2
# 463 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%r11; adcq %rdx,%rcx; adcq $0,%r9
# 0 "" 2
#NO_APP
	movq	32(%rsi), %rax
#APP
# 464 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq 40(%r8)
# 0 "" 2
# 464 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%r11; adcq %rdx,%rcx; adcq $0,%r9
# 0 "" 2
#NO_APP
	movq	40(%rsi), %rax
#APP
# 465 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq 32(%r8)
# 0 "" 2
# 465 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%r11; adcq %rdx,%rcx; adcq $0,%r9
# 0 "" 2
#NO_APP
	movq	48(%rsi), %rax
#APP
# 466 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq 24(%r8)
# 0 "" 2
# 466 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%r11; adcq %rdx,%rcx; adcq $0,%r9
# 0 "" 2
#NO_APP
	movq	56(%rsi), %rax
#APP
# 467 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq 16(%r8)
# 0 "" 2
# 467 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%r11; adcq %rdx,%rcx; adcq $0,%r9
# 0 "" 2
#NO_APP
	movq	%r11, 72(%rdi)
	movq	%r10, %r11
	movq	56(%rsi), %rax
#APP
# 470 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq 24(%r8)
# 0 "" 2
# 470 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%rcx; adcq %rdx,%r9; adcq $0,%r11
# 0 "" 2
#NO_APP
	movq	48(%rsi), %rax
#APP
# 471 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq 32(%r8)
# 0 "" 2
# 471 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%rcx; adcq %rdx,%r9; adcq $0,%r11
# 0 "" 2
#NO_APP
	movq	40(%rsi), %rax
#APP
# 472 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq 40(%r8)
# 0 "" 2
# 472 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%rcx; adcq %rdx,%r9; adcq $0,%r11
# 0 "" 2
#NO_APP
	movq	32(%rsi), %rax
#APP
# 473 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq 48(%r8)
# 0 "" 2
# 473 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%rcx; adcq %rdx,%r9; adcq $0,%r11
# 0 "" 2
#NO_APP
	movq	24(%rsi), %rax
#APP
# 474 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq 56(%r8)
# 0 "" 2
# 474 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%rcx; adcq %rdx,%r9; adcq $0,%r11
# 0 "" 2
#NO_APP
	movq	%rcx, 80(%rdi)
	movq	%r10, %rcx
	movq	32(%rsi), %rax
#APP
# 477 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq 56(%r8)
# 0 "" 2
# 477 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%r9; adcq %rdx,%r11; adcq $0,%rcx
# 0 "" 2
#NO_APP
	movq	40(%rsi), %rax
#APP
# 478 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq 48(%r8)
# 0 "" 2
# 478 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%r9; adcq %rdx,%r11; adcq $0,%rcx
# 0 "" 2
#NO_APP
	movq	48(%rsi), %rax
#APP
# 479 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq 40(%r8)
# 0 "" 2
# 479 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%r9; adcq %rdx,%r11; adcq $0,%rcx
# 0 "" 2
#NO_APP
	movq	56(%rsi), %rax
#APP
# 480 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq 32(%r8)
# 0 "" 2
# 480 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%r9; adcq %rdx,%r11; adcq $0,%rcx
# 0 "" 2
#NO_APP
	movq	%r9, 88(%rdi)
	movq	%r10, %r9
	movq	56(%rsi), %rax
#APP
# 483 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq 40(%r8)
# 0 "" 2
# 483 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%r11; adcq %rdx,%rcx; adcq $0,%r9
# 0 "" 2
#NO_APP
	movq	48(%rsi), %rax
#APP
# 484 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq 48(%r8)
# 0 "" 2
#NO_APP
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
#APP
# 484 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%r11; adcq %rdx,%rcx; adcq $0,%r9
# 0 "" 2
#NO_APP
	movq	40(%rsi), %rax
#APP
# 485 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq 56(%r8)
# 0 "" 2
# 485 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%r11; adcq %rdx,%rcx; adcq $0,%r9
# 0 "" 2
#NO_APP
	movq	%r11, 96(%rdi)
	movq	%r10, %r11
	movq	48(%rsi), %rax
#APP
# 488 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq 56(%r8)
# 0 "" 2
# 488 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%rcx; adcq %rdx,%r9; adcq $0,%r11
# 0 "" 2
#NO_APP
	movq	56(%rsi), %rax
#APP
# 489 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq 48(%r8)
# 0 "" 2
# 489 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%rcx; adcq %rdx,%r9; adcq $0,%r11
# 0 "" 2
#NO_APP
	movq	%rcx, 104(%rdi)
	movq	%r11, %rcx
	movq	56(%rsi), %rax
#APP
# 492 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq 56(%r8)
# 0 "" 2
# 492 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%r9; adcq %rdx,%rcx; adcq $0,%r10
# 0 "" 2
#NO_APP
	movq	%r9, 112(%rdi)
	movq	%rcx, 120(%rdi)
	ret
	.cfi_endproc
.LFE147:
	.size	bn_mul_comba8, .-bn_mul_comba8
	.p2align 4
	.globl	bn_mul_comba4
	.type	bn_mul_comba4, @function
bn_mul_comba4:
.LFB148:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r10d, %r10d
	movq	%rdx, %r9
	movq	(%rsi), %rax
	movq	%r10, %r8
	movq	%r10, %r11
	movq	%r10, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%r10, %rbx
#APP
# 504 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq (%rdx)
# 0 "" 2
# 504 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%r8; adcq %rdx,%r11; adcq $0,%rcx
# 0 "" 2
#NO_APP
	movq	%r8, (%rdi)
	movq	%r10, %r8
	movq	(%rsi), %rax
#APP
# 507 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq 8(%r9)
# 0 "" 2
# 507 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%r11; adcq %rdx,%rcx; adcq $0,%r8
# 0 "" 2
#NO_APP
	movq	8(%rsi), %rax
#APP
# 508 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq (%r9)
# 0 "" 2
# 508 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%r11; adcq %rdx,%rcx; adcq $0,%r8
# 0 "" 2
#NO_APP
	movq	%r11, 8(%rdi)
	movq	16(%rsi), %rax
#APP
# 511 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq (%r9)
# 0 "" 2
# 511 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%rcx; adcq %rdx,%r8; adcq $0,%rbx
# 0 "" 2
#NO_APP
	movq	8(%rsi), %rax
	movq	%rcx, %r11
	movq	%rbx, %rcx
	movq	%r10, %rbx
#APP
# 512 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq 8(%r9)
# 0 "" 2
# 512 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%r11; adcq %rdx,%r8; adcq $0,%rcx
# 0 "" 2
#NO_APP
	movq	(%rsi), %rax
#APP
# 513 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq 16(%r9)
# 0 "" 2
# 513 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%r11; adcq %rdx,%r8; adcq $0,%rcx
# 0 "" 2
#NO_APP
	movq	%r11, 16(%rdi)
	movq	(%rsi), %rax
#APP
# 516 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq 24(%r9)
# 0 "" 2
# 516 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%r8; adcq %rdx,%rcx; adcq $0,%rbx
# 0 "" 2
#NO_APP
	movq	8(%rsi), %rax
#APP
# 517 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq 16(%r9)
# 0 "" 2
# 517 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%r8; adcq %rdx,%rcx; adcq $0,%rbx
# 0 "" 2
#NO_APP
	movq	16(%rsi), %rax
#APP
# 518 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq 8(%r9)
# 0 "" 2
# 518 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%r8; adcq %rdx,%rcx; adcq $0,%rbx
# 0 "" 2
#NO_APP
	movq	24(%rsi), %rax
	movq	%r8, %r11
	movq	%rbx, %r8
	movq	%r10, %rbx
#APP
# 519 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq (%r9)
# 0 "" 2
# 519 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%r11; adcq %rdx,%rcx; adcq $0,%r8
# 0 "" 2
#NO_APP
	movq	%r11, 24(%rdi)
	movq	24(%rsi), %rax
#APP
# 522 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq 8(%r9)
# 0 "" 2
# 522 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%rcx; adcq %rdx,%r8; adcq $0,%rbx
# 0 "" 2
#NO_APP
	movq	16(%rsi), %rax
#APP
# 523 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq 16(%r9)
# 0 "" 2
# 523 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%rcx; adcq %rdx,%r8; adcq $0,%rbx
# 0 "" 2
#NO_APP
	movq	8(%rsi), %rax
	movq	%rcx, %r11
	movq	%r8, %rcx
	movq	%rbx, %r8
#APP
# 524 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq 24(%r9)
# 0 "" 2
#NO_APP
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
#APP
# 524 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%r11; adcq %rdx,%rcx; adcq $0,%r8
# 0 "" 2
#NO_APP
	movq	%r11, 32(%rdi)
	movq	%r10, %r11
	movq	16(%rsi), %rax
#APP
# 527 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq 24(%r9)
# 0 "" 2
# 527 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%rcx; adcq %rdx,%r8; adcq $0,%r11
# 0 "" 2
#NO_APP
	movq	24(%rsi), %rax
#APP
# 528 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq 16(%r9)
# 0 "" 2
# 528 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%rcx; adcq %rdx,%r8; adcq $0,%r11
# 0 "" 2
#NO_APP
	movq	%rcx, 40(%rdi)
	movq	%r11, %rcx
	movq	24(%rsi), %rax
#APP
# 531 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq 24(%r9)
# 0 "" 2
# 531 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%r8; adcq %rdx,%rcx; adcq $0,%r10
# 0 "" 2
#NO_APP
	movq	%r8, 48(%rdi)
	movq	%rcx, 56(%rdi)
	ret
	.cfi_endproc
.LFE148:
	.size	bn_mul_comba4, .-bn_mul_comba4
	.p2align 4
	.globl	bn_sqr_comba8
	.type	bn_sqr_comba8, @function
bn_sqr_comba8:
.LFB149:
	.cfi_startproc
	endbr64
	xorl	%r8d, %r8d
	movq	(%rsi), %rax
	movq	%r8, %r9
	movq	%r8, %r10
	movq	%r8, %rcx
#APP
# 543 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq %rax
# 0 "" 2
# 543 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%r9; adcq %rdx,%r10; adcq $0,%rcx
# 0 "" 2
#NO_APP
	movq	%r9, (%rdi)
	movq	%r8, %r9
	movq	8(%rsi), %rax
#APP
# 546 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq (%rsi)
# 0 "" 2
# 546 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%r10; adcq %rdx,%rcx; adcq $0,%r9
# 0 "" 2
# 546 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%r10; adcq %rdx,%rcx; adcq $0,%r9
# 0 "" 2
#NO_APP
	movq	%r10, 8(%rdi)
	movq	%r8, %r10
	movq	8(%rsi), %rax
#APP
# 549 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq %rax
# 0 "" 2
# 549 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%rcx; adcq %rdx,%r9; adcq $0,%r10
# 0 "" 2
#NO_APP
	movq	16(%rsi), %rax
#APP
# 550 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq (%rsi)
# 0 "" 2
# 550 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%rcx; adcq %rdx,%r9; adcq $0,%r10
# 0 "" 2
# 550 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%rcx; adcq %rdx,%r9; adcq $0,%r10
# 0 "" 2
#NO_APP
	movq	%rcx, 16(%rdi)
	movq	%r8, %rcx
	movq	24(%rsi), %rax
#APP
# 553 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq (%rsi)
# 0 "" 2
# 553 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%r9; adcq %rdx,%r10; adcq $0,%rcx
# 0 "" 2
# 553 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%r9; adcq %rdx,%r10; adcq $0,%rcx
# 0 "" 2
#NO_APP
	movq	16(%rsi), %rax
#APP
# 554 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq 8(%rsi)
# 0 "" 2
# 554 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%r9; adcq %rdx,%r10; adcq $0,%rcx
# 0 "" 2
# 554 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%r9; adcq %rdx,%r10; adcq $0,%rcx
# 0 "" 2
#NO_APP
	movq	%r9, 24(%rdi)
	movq	%r8, %r9
	movq	16(%rsi), %rax
#APP
# 557 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq %rax
# 0 "" 2
# 557 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%r10; adcq %rdx,%rcx; adcq $0,%r9
# 0 "" 2
#NO_APP
	movq	24(%rsi), %rax
#APP
# 558 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq 8(%rsi)
# 0 "" 2
# 558 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%r10; adcq %rdx,%rcx; adcq $0,%r9
# 0 "" 2
# 558 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%r10; adcq %rdx,%rcx; adcq $0,%r9
# 0 "" 2
#NO_APP
	movq	32(%rsi), %rax
#APP
# 559 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq (%rsi)
# 0 "" 2
# 559 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%r10; adcq %rdx,%rcx; adcq $0,%r9
# 0 "" 2
# 559 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%r10; adcq %rdx,%rcx; adcq $0,%r9
# 0 "" 2
#NO_APP
	movq	%r10, 32(%rdi)
	movq	%r8, %r10
	movq	40(%rsi), %rax
#APP
# 562 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq (%rsi)
# 0 "" 2
# 562 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%rcx; adcq %rdx,%r9; adcq $0,%r10
# 0 "" 2
# 562 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%rcx; adcq %rdx,%r9; adcq $0,%r10
# 0 "" 2
#NO_APP
	movq	32(%rsi), %rax
#APP
# 563 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq 8(%rsi)
# 0 "" 2
# 563 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%rcx; adcq %rdx,%r9; adcq $0,%r10
# 0 "" 2
# 563 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%rcx; adcq %rdx,%r9; adcq $0,%r10
# 0 "" 2
#NO_APP
	movq	24(%rsi), %rax
#APP
# 564 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq 16(%rsi)
# 0 "" 2
# 564 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%rcx; adcq %rdx,%r9; adcq $0,%r10
# 0 "" 2
# 564 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%rcx; adcq %rdx,%r9; adcq $0,%r10
# 0 "" 2
#NO_APP
	movq	%rcx, 40(%rdi)
	movq	%r8, %rcx
	movq	24(%rsi), %rax
#APP
# 567 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq %rax
# 0 "" 2
# 567 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%r9; adcq %rdx,%r10; adcq $0,%rcx
# 0 "" 2
#NO_APP
	movq	32(%rsi), %rax
#APP
# 568 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq 16(%rsi)
# 0 "" 2
# 568 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%r9; adcq %rdx,%r10; adcq $0,%rcx
# 0 "" 2
# 568 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%r9; adcq %rdx,%r10; adcq $0,%rcx
# 0 "" 2
#NO_APP
	movq	40(%rsi), %rax
#APP
# 569 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq 8(%rsi)
# 0 "" 2
# 569 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%r9; adcq %rdx,%r10; adcq $0,%rcx
# 0 "" 2
# 569 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%r9; adcq %rdx,%r10; adcq $0,%rcx
# 0 "" 2
#NO_APP
	movq	48(%rsi), %rax
#APP
# 570 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq (%rsi)
# 0 "" 2
# 570 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%r9; adcq %rdx,%r10; adcq $0,%rcx
# 0 "" 2
# 570 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%r9; adcq %rdx,%r10; adcq $0,%rcx
# 0 "" 2
#NO_APP
	movq	%r9, 48(%rdi)
	movq	%r8, %r9
	movq	56(%rsi), %rax
#APP
# 573 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq (%rsi)
# 0 "" 2
# 573 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%r10; adcq %rdx,%rcx; adcq $0,%r9
# 0 "" 2
# 573 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%r10; adcq %rdx,%rcx; adcq $0,%r9
# 0 "" 2
#NO_APP
	movq	48(%rsi), %rax
#APP
# 574 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq 8(%rsi)
# 0 "" 2
# 574 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%r10; adcq %rdx,%rcx; adcq $0,%r9
# 0 "" 2
# 574 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%r10; adcq %rdx,%rcx; adcq $0,%r9
# 0 "" 2
#NO_APP
	movq	40(%rsi), %rax
#APP
# 575 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq 16(%rsi)
# 0 "" 2
# 575 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%r10; adcq %rdx,%rcx; adcq $0,%r9
# 0 "" 2
# 575 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%r10; adcq %rdx,%rcx; adcq $0,%r9
# 0 "" 2
#NO_APP
	movq	32(%rsi), %rax
#APP
# 576 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq 24(%rsi)
# 0 "" 2
# 576 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%r10; adcq %rdx,%rcx; adcq $0,%r9
# 0 "" 2
# 576 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%r10; adcq %rdx,%rcx; adcq $0,%r9
# 0 "" 2
#NO_APP
	movq	%r10, 56(%rdi)
	movq	%r8, %r10
	movq	32(%rsi), %rax
#APP
# 579 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq %rax
# 0 "" 2
# 579 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%rcx; adcq %rdx,%r9; adcq $0,%r10
# 0 "" 2
#NO_APP
	movq	40(%rsi), %rax
#APP
# 580 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq 24(%rsi)
# 0 "" 2
# 580 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%rcx; adcq %rdx,%r9; adcq $0,%r10
# 0 "" 2
# 580 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%rcx; adcq %rdx,%r9; adcq $0,%r10
# 0 "" 2
#NO_APP
	movq	48(%rsi), %rax
#APP
# 581 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq 16(%rsi)
# 0 "" 2
# 581 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%rcx; adcq %rdx,%r9; adcq $0,%r10
# 0 "" 2
# 581 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%rcx; adcq %rdx,%r9; adcq $0,%r10
# 0 "" 2
#NO_APP
	movq	56(%rsi), %rax
#APP
# 582 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq 8(%rsi)
# 0 "" 2
# 582 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%rcx; adcq %rdx,%r9; adcq $0,%r10
# 0 "" 2
# 582 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%rcx; adcq %rdx,%r9; adcq $0,%r10
# 0 "" 2
#NO_APP
	movq	%rcx, 64(%rdi)
	movq	%r9, %rcx
	movq	56(%rsi), %rax
	movq	%r10, %r9
#APP
# 585 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq 16(%rsi)
# 0 "" 2
#NO_APP
	movq	%r8, %r10
#APP
# 585 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%rcx; adcq %rdx,%r9; adcq $0,%r10
# 0 "" 2
# 585 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%rcx; adcq %rdx,%r9; adcq $0,%r10
# 0 "" 2
#NO_APP
	movq	48(%rsi), %rax
#APP
# 586 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq 24(%rsi)
# 0 "" 2
# 586 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%rcx; adcq %rdx,%r9; adcq $0,%r10
# 0 "" 2
# 586 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%rcx; adcq %rdx,%r9; adcq $0,%r10
# 0 "" 2
#NO_APP
	movq	40(%rsi), %rax
#APP
# 587 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq 32(%rsi)
# 0 "" 2
# 587 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%rcx; adcq %rdx,%r9; adcq $0,%r10
# 0 "" 2
# 587 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%rcx; adcq %rdx,%r9; adcq $0,%r10
# 0 "" 2
#NO_APP
	movq	%rcx, 72(%rdi)
	movq	%r9, %rcx
	movq	40(%rsi), %rax
	movq	%r10, %r9
#APP
# 590 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq %rax
# 0 "" 2
#NO_APP
	movq	%r8, %r10
#APP
# 590 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%rcx; adcq %rdx,%r9; adcq $0,%r10
# 0 "" 2
#NO_APP
	movq	48(%rsi), %rax
#APP
# 591 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq 32(%rsi)
# 0 "" 2
# 591 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%rcx; adcq %rdx,%r9; adcq $0,%r10
# 0 "" 2
# 591 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%rcx; adcq %rdx,%r9; adcq $0,%r10
# 0 "" 2
#NO_APP
	movq	56(%rsi), %rax
#APP
# 592 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq 24(%rsi)
# 0 "" 2
# 592 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%rcx; adcq %rdx,%r9; adcq $0,%r10
# 0 "" 2
# 592 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%rcx; adcq %rdx,%r9; adcq $0,%r10
# 0 "" 2
#NO_APP
	movq	%rcx, 80(%rdi)
	movq	%r9, %rcx
	movq	56(%rsi), %rax
	movq	%r10, %r9
#APP
# 595 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq 32(%rsi)
# 0 "" 2
#NO_APP
	movq	%r8, %r10
#APP
# 595 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%rcx; adcq %rdx,%r9; adcq $0,%r10
# 0 "" 2
# 595 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%rcx; adcq %rdx,%r9; adcq $0,%r10
# 0 "" 2
#NO_APP
	movq	48(%rsi), %rax
#APP
# 596 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq 40(%rsi)
# 0 "" 2
# 596 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%rcx; adcq %rdx,%r9; adcq $0,%r10
# 0 "" 2
# 596 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%rcx; adcq %rdx,%r9; adcq $0,%r10
# 0 "" 2
#NO_APP
	movq	%rcx, 88(%rdi)
	movq	%r9, %rcx
	movq	48(%rsi), %rax
	movq	%r10, %r9
#APP
# 599 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq %rax
# 0 "" 2
#NO_APP
	movq	%r8, %r10
#APP
# 599 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%rcx; adcq %rdx,%r9; adcq $0,%r10
# 0 "" 2
#NO_APP
	movq	56(%rsi), %rax
#APP
# 600 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq 40(%rsi)
# 0 "" 2
# 600 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%rcx; adcq %rdx,%r9; adcq $0,%r10
# 0 "" 2
# 600 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%rcx; adcq %rdx,%r9; adcq $0,%r10
# 0 "" 2
#NO_APP
	movq	%rcx, 96(%rdi)
	movq	%r10, %rcx
	movq	56(%rsi), %rax
	movq	%r8, %r10
#APP
# 603 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq 48(%rsi)
# 0 "" 2
# 603 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%r9; adcq %rdx,%rcx; adcq $0,%r10
# 0 "" 2
# 603 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%r9; adcq %rdx,%rcx; adcq $0,%r10
# 0 "" 2
#NO_APP
	movq	%r9, 104(%rdi)
	movq	56(%rsi), %rax
#APP
# 606 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq %rax
# 0 "" 2
# 606 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%rcx; adcq %rdx,%r10; adcq $0,%r8
# 0 "" 2
#NO_APP
	movq	%rcx, 112(%rdi)
	movq	%r10, 120(%rdi)
	ret
	.cfi_endproc
.LFE149:
	.size	bn_sqr_comba8, .-bn_sqr_comba8
	.p2align 4
	.globl	bn_sqr_comba4
	.type	bn_sqr_comba4, @function
bn_sqr_comba4:
.LFB150:
	.cfi_startproc
	endbr64
	xorl	%r8d, %r8d
	movq	(%rsi), %rax
	movq	%r8, %r10
	movq	%r8, %rcx
	movq	%r8, %r9
#APP
# 618 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq %rax
# 0 "" 2
# 618 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%r10; adcq %rdx,%rcx; adcq $0,%r9
# 0 "" 2
#NO_APP
	movq	%r10, (%rdi)
	movq	%r8, %r10
	movq	8(%rsi), %rax
#APP
# 621 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq (%rsi)
# 0 "" 2
# 621 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%rcx; adcq %rdx,%r9; adcq $0,%r10
# 0 "" 2
# 621 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%rcx; adcq %rdx,%r9; adcq $0,%r10
# 0 "" 2
#NO_APP
	movq	%rcx, 8(%rdi)
	movq	%r8, %rcx
	movq	8(%rsi), %rax
#APP
# 624 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq %rax
# 0 "" 2
# 624 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%r9; adcq %rdx,%r10; adcq $0,%rcx
# 0 "" 2
#NO_APP
	movq	16(%rsi), %rax
#APP
# 625 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq (%rsi)
# 0 "" 2
# 625 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%r9; adcq %rdx,%r10; adcq $0,%rcx
# 0 "" 2
# 625 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%r9; adcq %rdx,%r10; adcq $0,%rcx
# 0 "" 2
#NO_APP
	movq	%r9, 16(%rdi)
	movq	%r8, %r9
	movq	24(%rsi), %rax
#APP
# 628 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq (%rsi)
# 0 "" 2
# 628 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%r10; adcq %rdx,%rcx; adcq $0,%r9
# 0 "" 2
# 628 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%r10; adcq %rdx,%rcx; adcq $0,%r9
# 0 "" 2
#NO_APP
	movq	16(%rsi), %rax
#APP
# 629 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq 8(%rsi)
# 0 "" 2
# 629 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%r10; adcq %rdx,%rcx; adcq $0,%r9
# 0 "" 2
# 629 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%r10; adcq %rdx,%rcx; adcq $0,%r9
# 0 "" 2
#NO_APP
	movq	%r10, 24(%rdi)
	movq	%r8, %r10
	movq	16(%rsi), %rax
#APP
# 632 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq %rax
# 0 "" 2
# 632 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%rcx; adcq %rdx,%r9; adcq $0,%r10
# 0 "" 2
#NO_APP
	movq	24(%rsi), %rax
#APP
# 633 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq 8(%rsi)
# 0 "" 2
# 633 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%rcx; adcq %rdx,%r9; adcq $0,%r10
# 0 "" 2
# 633 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%rcx; adcq %rdx,%r9; adcq $0,%r10
# 0 "" 2
#NO_APP
	movq	%rcx, 32(%rdi)
	movq	%r10, %rcx
	movq	24(%rsi), %rax
	movq	%r8, %r10
#APP
# 636 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq 16(%rsi)
# 0 "" 2
# 636 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%r9; adcq %rdx,%rcx; adcq $0,%r10
# 0 "" 2
# 636 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%r9; adcq %rdx,%rcx; adcq $0,%r10
# 0 "" 2
#NO_APP
	movq	%r9, 40(%rdi)
	movq	24(%rsi), %rax
#APP
# 639 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	mulq %rax
# 0 "" 2
# 639 "../deps/openssl/openssl/crypto/bn/asm/x86_64-gcc.c" 1
	addq %rax,%rcx; adcq %rdx,%r10; adcq $0,%r8
# 0 "" 2
#NO_APP
	movq	%rcx, 48(%rdi)
	movq	%r10, 56(%rdi)
	ret
	.cfi_endproc
.LFE150:
	.size	bn_sqr_comba4, .-bn_sqr_comba4
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
