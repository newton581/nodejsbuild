	.file	"tls1_prf.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/kdf/tls1_prf.c"
	.text
	.p2align 4
	.type	pkey_tls1_prf_ctrl, @function
pkey_tls1_prf_ctrl:
.LFB447:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movslq	%edx, %rbx
	subq	$8, %rsp
	movq	40(%rdi), %r13
	cmpl	$4097, %esi
	je	.L2
	cmpl	$4098, %esi
	je	.L3
	movl	$-2, %eax
	cmpl	$4096, %esi
	je	.L18
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	.cfi_restore_state
	testl	%ebx, %ebx
	js	.L7
	movq	8(%r13), %rdi
	testq	%rdi, %rdi
	je	.L6
	movq	16(%r13), %rsi
	movl	$69, %ecx
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_clear_free@PLT
.L6:
	movq	1048(%r13), %rsi
	leaq	24(%r13), %rdi
	call	OPENSSL_cleanse@PLT
	movl	$72, %ecx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	$0, 1048(%r13)
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_memdup@PLT
	movq	%rax, 8(%r13)
	testq	%rax, %rax
	je	.L7
	movq	%rbx, 16(%r13)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	.cfi_restore_state
	movq	%rcx, 0(%r13)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	.cfi_restore_state
	testl	%ebx, %ebx
	je	.L9
	testq	%rcx, %rcx
	je	.L9
	testl	%ebx, %ebx
	js	.L7
	movq	1048(%r13), %rdx
	movl	$1024, %eax
	subl	%edx, %eax
	cmpl	%ebx, %eax
	jl	.L7
	leaq	24(%r13,%rdx), %rdi
	movq	%rcx, %rsi
	movq	%rbx, %rdx
	call	memcpy@PLT
	addq	%rbx, 1048(%r13)
	movl	$1, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L9:
	movl	$1, %eax
	jmp	.L1
	.cfi_endproc
.LFE447:
	.size	pkey_tls1_prf_ctrl, .-pkey_tls1_prf_ctrl
	.p2align 4
	.type	tls1_prf_P_hash, @function
tls1_prf_P_hash:
.LFB450:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$152, %rsp
	movq	%rdx, -160(%rbp)
	movq	%rcx, -168(%rbp)
	movq	%r8, -176(%rbp)
	movq	%r9, -152(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	EVP_MD_size@PLT
	movl	%eax, -184(%rbp)
	testl	%eax, %eax
	jle	.L28
	call	EVP_MD_CTX_new@PLT
	movq	%rax, %r12
	call	EVP_MD_CTX_new@PLT
	movq	%rax, %r15
	call	EVP_MD_CTX_new@PLT
	movq	%rax, %r13
	testq	%r12, %r12
	je	.L29
	testq	%r15, %r15
	je	.L29
	testq	%rax, %rax
	je	.L30
	movq	%rax, %rdi
	movl	$8, %esi
	call	EVP_MD_CTX_set_flags@PLT
	movq	%rbx, %rdx
	xorl	%esi, %esi
	movl	$855, %edi
	movq	-160(%rbp), %rcx
	call	EVP_PKEY_new_raw_private_key@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L31
	movq	%r14, %rdx
	movq	%rax, %r8
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	movq	%r13, %rdi
	leaq	-128(%rbp), %r14
	call	EVP_DigestSignInit@PLT
	testl	%eax, %eax
	je	.L20
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	EVP_MD_CTX_copy_ex@PLT
	testl	%eax, %eax
	je	.L20
	movq	-176(%rbp), %rdx
	movq	-168(%rbp), %rsi
	movq	%r12, %rdi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	je	.L20
	leaq	-144(%rbp), %rcx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rcx, %rdx
	movq	%rcx, -160(%rbp)
	call	EVP_DigestSignFinal@PLT
	testl	%eax, %eax
	je	.L20
	movslq	-184(%rbp), %rax
	movq	%rax, -184(%rbp)
	.p2align 4,,10
	.p2align 3
.L24:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	EVP_MD_CTX_copy_ex@PLT
	testl	%eax, %eax
	je	.L20
	movq	-144(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	je	.L20
	movq	-184(%rbp), %rax
	cmpq	16(%rbp), %rax
	jb	.L73
	movq	-176(%rbp), %rdx
	movq	-168(%rbp), %rsi
	movq	%r12, %rdi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	je	.L20
	movq	-160(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	EVP_DigestSignFinal@PLT
	testl	%eax, %eax
	je	.L20
	movq	16(%rbp), %rdx
	movq	-152(%rbp), %rdi
	movq	%r14, %rsi
	call	memcpy@PLT
	movl	$1, %eax
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L28:
	xorl	%eax, %eax
	xorl	%ebx, %ebx
	xorl	%r13d, %r13d
	xorl	%r15d, %r15d
	xorl	%r12d, %r12d
	leaq	-128(%rbp), %r14
.L20:
	movq	%rbx, %rdi
	movl	%eax, -152(%rbp)
	call	EVP_PKEY_free@PLT
	movq	%r12, %rdi
	call	EVP_MD_CTX_free@PLT
	movq	%r15, %rdi
	call	EVP_MD_CTX_free@PLT
	movq	%r13, %rdi
	call	EVP_MD_CTX_free@PLT
	movl	$64, %esi
	movq	%r14, %rdi
	call	OPENSSL_cleanse@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	movl	-152(%rbp), %eax
	jne	.L74
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	.cfi_restore_state
	xorl	%eax, %eax
	xorl	%ebx, %ebx
	leaq	-128(%rbp), %r14
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L31:
	xorl	%eax, %eax
	leaq	-128(%rbp), %r14
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L30:
	xorl	%ebx, %ebx
	xorl	%eax, %eax
	leaq	-128(%rbp), %r14
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L73:
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	EVP_MD_CTX_copy_ex@PLT
	testl	%eax, %eax
	je	.L20
	movq	-176(%rbp), %rdx
	movq	-168(%rbp), %rsi
	movq	%r12, %rdi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	je	.L20
	movq	-152(%rbp), %rsi
	leaq	-136(%rbp), %rdx
	movq	%r12, %rdi
	call	EVP_DigestSignFinal@PLT
	testl	%eax, %eax
	je	.L23
	movq	-160(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	-136(%rbp), %rax
	addq	%rax, -152(%rbp)
	subq	%rax, 16(%rbp)
	call	EVP_DigestSignFinal@PLT
	testl	%eax, %eax
	jne	.L24
.L23:
	xorl	%eax, %eax
	jmp	.L20
.L74:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE450:
	.size	tls1_prf_P_hash, .-tls1_prf_P_hash
	.p2align 4
	.type	pkey_tls1_prf_cleanup, @function
pkey_tls1_prf_cleanup:
.LFB446:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdx
	movl	$52, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	40(%rdi), %r12
	movq	16(%r12), %rsi
	movq	8(%r12), %rdi
	call	CRYPTO_clear_free@PLT
	movq	1048(%r12), %rsi
	leaq	24(%r12), %rdi
	call	OPENSSL_cleanse@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$54, %edx
	popq	%r12
	leaq	.LC0(%rip), %rsi
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_free@PLT
	.cfi_endproc
.LFE446:
	.size	pkey_tls1_prf_cleanup, .-pkey_tls1_prf_cleanup
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"secret"
.LC2:
	.string	"hexsecret"
.LC3:
	.string	"seed"
.LC4:
	.string	"hexseed"
	.text
	.p2align 4
	.type	pkey_tls1_prf_ctrl_str, @function
pkey_tls1_prf_ctrl_str:
.LFB448:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	testq	%rdx, %rdx
	je	.L90
	cmpb	$109, (%rsi)
	movq	%rdi, %r8
	movq	%rsi, %rax
	jne	.L81
	cmpb	$100, 1(%rsi)
	jne	.L81
	movzbl	2(%rsi), %r12d
	testl	%r12d, %r12d
	jne	.L81
	movq	40(%rdi), %rbx
	movq	%rdx, %rdi
	call	EVP_get_digestbyname@PLT
	testq	%rax, %rax
	je	.L91
	movq	%rax, (%rbx)
	movl	$1, %r12d
.L77:
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L81:
	.cfi_restore_state
	movl	$7, %ecx
	movq	%rax, %rsi
	leaq	.LC1(%rip), %rdi
	repz cmpsb
	movl	$4097, %esi
	seta	%cl
	sbbb	$0, %cl
	testb	%cl, %cl
	je	.L88
	movl	$10, %ecx
	movq	%rax, %rsi
	leaq	.LC2(%rip), %rdi
	repz cmpsb
	movl	$4097, %esi
	seta	%cl
	sbbb	$0, %cl
	testb	%cl, %cl
	je	.L89
	movl	$5, %ecx
	leaq	.LC3(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%cl
	sbbb	$0, %cl
	testb	%cl, %cl
	je	.L92
	movq	%rax, %rsi
	movl	$8, %ecx
	leaq	.LC4(%rip), %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L86
	movl	$4098, %esi
.L89:
	popq	%rbx
	movq	%r8, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	EVP_PKEY_CTX_hex2ctrl@PLT
	.p2align 4,,10
	.p2align 3
.L92:
	.cfi_restore_state
	movl	$4098, %esi
.L88:
	popq	%rbx
	movq	%r8, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	EVP_PKEY_CTX_str2ctrl@PLT
	.p2align 4,,10
	.p2align 3
.L90:
	.cfi_restore_state
	movl	$97, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$102, %edx
	xorl	%r12d, %r12d
	movl	$100, %esi
	movl	$52, %edi
	call	ERR_put_error@PLT
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L91:
	movl	$105, %r8d
	movl	$100, %edx
	movl	$100, %esi
	movl	$52, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L86:
	movl	$120, %r8d
	movl	$103, %edx
	movl	$100, %esi
	movl	$52, %edi
	leaq	.LC0(%rip), %rcx
	movl	$-2, %r12d
	call	ERR_put_error@PLT
	jmp	.L77
	.cfi_endproc
.LFE448:
	.size	pkey_tls1_prf_ctrl_str, .-pkey_tls1_prf_ctrl_str
	.p2align 4
	.type	pkey_tls1_prf_derive, @function
pkey_tls1_prf_derive:
.LFB449:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	40(%rdi), %rax
	movq	(%rax), %r15
	testq	%r15, %r15
	je	.L120
	movq	8(%rax), %r14
	testq	%r14, %r14
	je	.L121
	movq	1048(%rax), %r8
	testq	%r8, %r8
	je	.L122
	leaq	24(%rax), %rcx
	movq	%r15, %rdi
	movq	%r8, -64(%rbp)
	movq	(%rdx), %r13
	movq	%rcx, -56(%rbp)
	movq	16(%rax), %r12
	movq	%rsi, %rbx
	call	EVP_MD_type@PLT
	movq	-64(%rbp), %r8
	cmpl	$114, %eax
	je	.L123
	subq	$8, %rsp
	movq	-56(%rbp), %rcx
	movq	%r12, %rdx
	movq	%rbx, %r9
	pushq	%r13
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	tls1_prf_P_hash
	popq	%rdx
	popq	%rcx
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
.L93:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L122:
	.cfi_restore_state
	movl	$137, %r8d
	movl	$106, %edx
	movl	$101, %esi
	movl	$52, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	leaq	-40(%rbp), %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L123:
	.cfi_restore_state
	call	EVP_md5@PLT
	movq	%r12, %r15
	subq	$8, %rsp
	andl	$1, %r12d
	pushq	%r13
	shrq	%r15
	movq	-64(%rbp), %r8
	movq	%rbx, %r9
	movq	-56(%rbp), %rcx
	addq	%r15, %r12
	movq	%rax, %rdi
	movq	%r14, %rsi
	movq	%r12, %rdx
	call	tls1_prf_P_hash
	popq	%r8
	popq	%r9
	testl	%eax, %eax
	je	.L93
	movl	$260, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r13, %rdi
	call	CRYPTO_malloc@PLT
	movq	-64(%rbp), %r8
	testq	%rax, %rax
	je	.L124
	movq	%r8, -72(%rbp)
	addq	%r14, %r15
	movq	%rax, -64(%rbp)
	call	EVP_sha1@PLT
	subq	$8, %rsp
	movq	-64(%rbp), %r9
	movq	%r15, %rsi
	pushq	%r13
	movq	-72(%rbp), %r8
	movq	%rax, %rdi
	movq	%r12, %rdx
	movq	-56(%rbp), %rcx
	call	tls1_prf_P_hash
	popq	%rsi
	movq	-64(%rbp), %r9
	testl	%eax, %eax
	popq	%rdi
	je	.L100
	testq	%r13, %r13
	je	.L102
	leaq	15(%rbx), %rax
	subq	%r9, %rax
	cmpq	$30, %rax
	jbe	.L107
	leaq	-1(%r13), %rax
	cmpq	$14, %rax
	jbe	.L107
	movq	%r13, %rdx
	xorl	%eax, %eax
	andq	$-16, %rdx
	.p2align 4,,10
	.p2align 3
.L104:
	movdqu	(%rbx,%rax), %xmm0
	movdqu	(%r9,%rax), %xmm1
	pxor	%xmm1, %xmm0
	movups	%xmm0, (%rbx,%rax)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L104
	movq	%r13, %rax
	andq	$-16, %rax
	testb	$15, %r13b
	je	.L102
	movzbl	(%r9,%rax), %edx
	xorb	%dl, (%rbx,%rax)
	leaq	1(%rax), %rdx
	cmpq	%rdx, %r13
	jbe	.L102
	movzbl	1(%r9,%rax), %ecx
	xorb	%cl, (%rbx,%rdx)
	leaq	2(%rax), %rdx
	cmpq	%rdx, %r13
	jbe	.L102
	movzbl	2(%r9,%rax), %ecx
	xorb	%cl, (%rbx,%rdx)
	leaq	3(%rax), %rdx
	cmpq	%rdx, %r13
	jbe	.L102
	movzbl	3(%r9,%rax), %ecx
	xorb	%cl, (%rbx,%rdx)
	leaq	4(%rax), %rdx
	cmpq	%rdx, %r13
	jbe	.L102
	movzbl	4(%r9,%rax), %ecx
	xorb	%cl, (%rbx,%rdx)
	leaq	5(%rax), %rdx
	cmpq	%rdx, %r13
	jbe	.L102
	movzbl	5(%r9,%rax), %ecx
	xorb	%cl, (%rbx,%rdx)
	leaq	6(%rax), %rdx
	cmpq	%rdx, %r13
	jbe	.L102
	movzbl	6(%r9,%rax), %ecx
	xorb	%cl, (%rbx,%rdx)
	leaq	7(%rax), %rdx
	cmpq	%rdx, %r13
	jbe	.L102
	movzbl	7(%r9,%rax), %ecx
	xorb	%cl, (%rbx,%rdx)
	leaq	8(%rax), %rdx
	cmpq	%rdx, %r13
	jbe	.L102
	movzbl	8(%r9,%rax), %ecx
	xorb	%cl, (%rbx,%rdx)
	leaq	9(%rax), %rdx
	cmpq	%rdx, %r13
	jbe	.L102
	movzbl	9(%r9,%rax), %ecx
	xorb	%cl, (%rbx,%rdx)
	leaq	10(%rax), %rdx
	cmpq	%rdx, %r13
	jbe	.L102
	movzbl	10(%r9,%rax), %ecx
	xorb	%cl, (%rbx,%rdx)
	leaq	11(%rax), %rdx
	cmpq	%rdx, %r13
	jbe	.L102
	movzbl	11(%r9,%rax), %ecx
	xorb	%cl, (%rbx,%rdx)
	leaq	12(%rax), %rdx
	cmpq	%rdx, %r13
	jbe	.L102
	movzbl	12(%r9,%rax), %ecx
	xorb	%cl, (%rbx,%rdx)
	leaq	13(%rax), %rdx
	cmpq	%rdx, %r13
	jbe	.L102
	movzbl	13(%r9,%rax), %ecx
	xorb	%cl, (%rbx,%rdx)
	leaq	14(%rax), %rdx
	cmpq	%rdx, %r13
	jbe	.L102
	movzbl	14(%r9,%rax), %eax
	xorb	%al, (%rbx,%rdx)
	.p2align 4,,10
	.p2align 3
.L102:
	movl	$271, %ecx
	leaq	.LC0(%rip), %rdx
	movq	%r13, %rsi
	movq	%r9, %rdi
	call	CRYPTO_clear_free@PLT
	movl	$1, %eax
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L120:
	movl	$129, %r8d
	movl	$105, %edx
	movl	$101, %esi
	movl	$52, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L121:
	movl	$133, %r8d
	movl	$107, %edx
	movl	$101, %esi
	movl	$52, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L100:
	movl	$266, %ecx
	movq	%r13, %rsi
	movq	%r9, %rdi
	movl	%eax, -56(%rbp)
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_clear_free@PLT
	movl	-56(%rbp), %eax
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L124:
	movl	$261, %r8d
	movl	$65, %edx
	movl	$111, %esi
	movl	$52, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L93
.L107:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L103:
	movzbl	(%r9,%rax), %edx
	xorb	%dl, (%rbx,%rax)
	addq	$1, %rax
	cmpq	%rax, %r13
	jne	.L103
	jmp	.L102
	.cfi_endproc
.LFE449:
	.size	pkey_tls1_prf_derive, .-pkey_tls1_prf_derive
	.p2align 4
	.type	pkey_tls1_prf_init, @function
pkey_tls1_prf_init:
.LFB445:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$40, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$1056, %edi
	subq	$8, %rsp
	call	CRYPTO_zalloc@PLT
	testq	%rax, %rax
	je	.L129
	movq	%rax, 40(%rbx)
	movl	$1, %eax
.L125:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L129:
	.cfi_restore_state
	movl	$41, %r8d
	movl	$65, %edx
	movl	$110, %esi
	movl	$52, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L125
	.cfi_endproc
.LFE445:
	.size	pkey_tls1_prf_init, .-pkey_tls1_prf_init
	.globl	tls1_prf_pkey_meth
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	tls1_prf_pkey_meth, @object
	.size	tls1_prf_pkey_meth, 256
tls1_prf_pkey_meth:
	.long	1021
	.long	0
	.quad	pkey_tls1_prf_init
	.quad	0
	.quad	pkey_tls1_prf_cleanup
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	pkey_tls1_prf_derive
	.quad	pkey_tls1_prf_ctrl
	.quad	pkey_tls1_prf_ctrl_str
	.zero	48
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
