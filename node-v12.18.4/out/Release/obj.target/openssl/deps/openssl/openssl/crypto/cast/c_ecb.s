	.file	"c_ecb.c"
	.text
	.p2align 4
	.globl	CAST_ecb_encrypt
	.type	CAST_ecb_encrypt, @function
CAST_ecb_encrypt:
.LFB0:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rsi, %rbx
	movq	%rdx, %rsi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	(%rdi), %eax
	bswap	%eax
	movl	%eax, -32(%rbp)
	movl	4(%rdi), %eax
	leaq	-32(%rbp), %rdi
	bswap	%eax
	movl	%eax, -28(%rbp)
	testl	%ecx, %ecx
	je	.L2
	call	CAST_encrypt@PLT
.L3:
	movl	-32(%rbp), %eax
	bswap	%eax
	movl	%eax, (%rbx)
	movl	-28(%rbp), %eax
	bswap	%eax
	movl	%eax, 4(%rbx)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L7
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	.cfi_restore_state
	call	CAST_decrypt@PLT
	jmp	.L3
.L7:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE0:
	.size	CAST_ecb_encrypt, .-CAST_ecb_encrypt
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
