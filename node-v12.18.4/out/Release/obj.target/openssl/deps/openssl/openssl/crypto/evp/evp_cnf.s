	.file	"evp_cnf.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/evp/evp_cnf.c"
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"fips_mode"
.LC2:
	.string	", value="
.LC3:
	.string	"name="
	.text
	.p2align 4
	.type	alg_module_init, @function
alg_module_init:
.LFB1296:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	.LC1(%rip), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	xorl	%ebx, %ebx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	CONF_imodule_get_value@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	NCONF_get_section@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	jne	.L2
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L20:
	leaq	-60(%rbp), %rsi
	movq	%rax, %rdi
	call	X509V3_get_value_bool@PLT
	testl	%eax, %eax
	je	.L17
	movl	-60(%rbp), %eax
	testl	%eax, %eax
	jg	.L18
.L8:
	addl	$1, %ebx
.L2:
	movq	%r13, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jge	.L19
	movl	%ebx, %esi
	movq	%r13, %rdi
	call	OPENSSL_sk_value@PLT
	movl	$10, %ecx
	movq	%r14, %rdi
	movq	8(%rax), %rsi
	movq	%rax, %r15
	repz cmpsb
	seta	%r12b
	sbbb	$0, %r12b
	movsbl	%r12b, %r12d
	testl	%r12d, %r12d
	je	.L20
	movl	$44, %r8d
	movl	$169, %edx
	movl	$177, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	8(%r15), %rdx
	movq	16(%r15), %r8
	xorl	%eax, %eax
	leaq	.LC2(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	movl	$4, %edi
	call	ERR_add_error_data@PLT
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L19:
	movl	$1, %r12d
.L1:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L21
	addq	$24, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	.cfi_restore_state
	movl	$36, %r8d
	movl	$168, %edx
	movl	$177, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L18:
	movl	$40, %r8d
	movl	$167, %edx
	movl	$177, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L1
.L16:
	movl	$28, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$165, %edx
	xorl	%r12d, %r12d
	movl	$177, %esi
	movl	$6, %edi
	call	ERR_put_error@PLT
	jmp	.L1
.L21:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1296:
	.size	alg_module_init, .-alg_module_init
	.section	.rodata.str1.1
.LC4:
	.string	"alg_section"
	.text
	.p2align 4
	.globl	EVP_add_alg_module
	.type	EVP_add_alg_module, @function
EVP_add_alg_module:
.LFB1297:
	.cfi_startproc
	endbr64
	xorl	%edx, %edx
	leaq	alg_module_init(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	jmp	CONF_module_add@PLT
	.cfi_endproc
.LFE1297:
	.size	EVP_add_alg_module, .-EVP_add_alg_module
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
