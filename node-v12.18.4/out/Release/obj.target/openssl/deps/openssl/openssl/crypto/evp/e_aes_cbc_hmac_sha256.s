	.file	"e_aes_cbc_hmac_sha256.c"
	.text
	.p2align 4
	.type	sha256_update, @function
sha256_update:
.LFB406:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	104(%rdi), %eax
	movq	%rdx, %rbx
	testq	%rax, %rax
	jne	.L17
	movq	%rbx, %r14
	andl	$63, %r14d
	andq	$-64, %rbx
	jne	.L18
.L3:
	testq	%r14, %r14
	jne	.L19
.L1:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	.cfi_restore_state
	movl	$64, %r14d
	subq	%rax, %r14
	cmpq	%r14, %rdx
	cmovbe	%rdx, %r14
	subq	%r14, %rbx
	movq	%r14, %rdx
	addq	%r14, %r13
	call	SHA256_Update@PLT
	movq	%rbx, %r14
	andl	$63, %r14d
	andq	$-64, %rbx
	je	.L3
.L18:
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	addq	%rbx, %r13
	shrq	$6, %rdx
	call	sha256_block_data_order@PLT
	movq	%rbx, %rax
	salq	$3, %rbx
	shrq	$29, %rax
	addl	32(%r12), %ebx
	adcl	%eax, 36(%r12)
	movl	%ebx, 32(%r12)
	testq	%r14, %r14
	je	.L1
.L19:
	popq	%rbx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	SHA256_Update@PLT
	.cfi_endproc
.LFE406:
	.size	sha256_update, .-sha256_update
	.p2align 4
	.type	aesni_cbc_hmac_sha256_cipher, @function
aesni_cbc_hmac_sha256_cipher:
.LFB408:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	subq	$216, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -168(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	584(%rax), %rbx
	movl	572(%rax), %ecx
	movq	%rax, %r15
	movq	$-1, 584(%rax)
	movq	%r12, %rax
	andl	$15, %eax
	movq	%rax, -176(%rbp)
	je	.L21
.L25:
	xorl	%r11d, %r11d
.L20:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L73
	leaq	-40(%rbp), %rsp
	movl	%r11d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	.cfi_restore_state
	movq	%r13, %rdi
	movl	%ecx, -184(%rbp)
	call	EVP_CIPHER_CTX_encrypting@PLT
	testl	%eax, %eax
	je	.L23
	cmpq	$-1, %rbx
	movl	-184(%rbp), %ecx
	je	.L54
	leaq	48(%rbx), %rax
	andq	$-16, %rax
	cmpq	%r12, %rax
	jne	.L25
	cmpl	$769, 592(%r15)
	jbe	.L55
	leaq	16(%r14), %rsi
	movl	$16, %eax
.L24:
	leaq	468(%r15), %r9
	testb	$32, 11+OPENSSL_ia32cap_P(%rip)
	jne	.L26
	movl	4+OPENSSL_ia32cap_P(%rip), %edx
	testl	$268435456, %edx
	je	.L57
	movl	OPENSSL_ia32cap_P(%rip), %edi
	andl	$2048, %edx
	andl	$1073741824, %edi
	orl	%edx, %edi
	je	.L57
.L26:
	movl	$64, %edx
	subl	%ecx, %edx
	leaq	(%rax,%rdx), %r11
	cmpq	%rbx, %r11
	jnb	.L58
	movq	-168(%rbp), %rdi
	movq	%rbx, %r10
	movq	%r12, -192(%rbp)
	subq	%r11, %r10
	movq	%rdi, -184(%rbp)
	cmpq	$63, %r10
	ja	.L74
.L27:
	movq	%rbx, %rdx
	movq	%r9, %rdi
	movq	%r9, -208(%rbp)
	subq	%rax, %rdx
	call	sha256_update
	cmpq	%r12, %rbx
	je	.L30
	cmpq	-168(%rbp), %r14
	movq	-208(%rbp), %r9
	je	.L31
	movq	-176(%rbp), %rax
	movq	-184(%rbp), %rdi
	movq	%rbx, %rdx
	subq	%rax, %rdx
	leaq	(%r14,%rax), %rsi
	call	memcpy@PLT
	movq	-208(%rbp), %r9
.L31:
	movq	-168(%rbp), %rax
	movq	%r9, %rsi
	movq	%r9, -176(%rbp)
	leaq	(%rax,%rbx), %r14
	movq	%r14, %rdi
	call	SHA256_Final@PLT
	movq	-176(%rbp), %r9
	movl	$32, %edx
	movq	%r14, %rsi
	movdqu	356(%r15), %xmm3
	movdqu	372(%r15), %xmm4
	movdqu	388(%r15), %xmm5
	movq	%r9, %rdi
	movdqu	404(%r15), %xmm6
	movdqu	420(%r15), %xmm7
	movups	%xmm3, 468(%r15)
	movdqu	436(%r15), %xmm3
	movups	%xmm4, 484(%r15)
	movdqu	452(%r15), %xmm4
	movups	%xmm5, 500(%r15)
	movups	%xmm6, 516(%r15)
	movups	%xmm7, 532(%r15)
	movups	%xmm3, 548(%r15)
	movups	%xmm4, 564(%r15)
	call	sha256_update
	movq	-176(%rbp), %r9
	movq	%r14, %rdi
	movq	%r9, %rsi
	call	SHA256_Final@PLT
	leaq	32(%rbx), %rdi
	leal	-1(%r12), %esi
	subl	%edi, %esi
	cmpq	%rdi, %r12
	jbe	.L33
	leaq	-32(%r12), %rdx
	movzbl	%sil, %esi
	addq	-168(%rbp), %rdi
	subq	%rbx, %rdx
	call	memset@PLT
.L33:
	movq	%r13, %rdi
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movq	-184(%rbp), %rdi
	movq	-192(%rbp), %rdx
	movq	%r15, %rcx
	movq	%rax, %r8
	movl	$1, %r9d
	movq	%rdi, %rsi
	call	aesni_cbc_encrypt@PLT
	movl	$1, %r11d
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L57:
	movq	-168(%rbp), %rcx
	movq	%r12, -192(%rbp)
	movq	%rcx, -184(%rbp)
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L23:
	movq	%r13, %rdi
	movl	%eax, -184(%rbp)
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	xorl	%r9d, %r9d
	movq	%r15, %rcx
	movq	%r12, %rdx
	movq	-168(%rbp), %rsi
	movq	%rax, %r8
	movq	%r14, %rdi
	call	aesni_cbc_encrypt@PLT
	cmpq	$-1, %rbx
	je	.L34
	movzbl	588(%r15,%rbx), %eax
	movl	$49, %esi
	movzbl	589(%r15,%rbx), %edx
	movl	-184(%rbp), %r11d
	sall	$8, %eax
	orl	%edx, %eax
	movl	$33, %edx
	cmpl	$770, %eax
	movl	$16, %eax
	cmovl	-176(%rbp), %rax
	cmovge	%rsi, %rdx
	cmpq	%rdx, %r12
	jb	.L20
	movq	-168(%rbp), %rdi
	subq	%rax, %r12
	movdqu	244(%r15), %xmm2
	leaq	468(%r15), %r13
	movdqu	276(%r15), %xmm6
	leal	-33(%r12), %esi
	movdqu	292(%r15), %xmm7
	addq	%rax, %rdi
	movl	$255, %eax
	subl	%esi, %eax
	movzbl	-1(%rdi,%r12), %edx
	movq	%rdi, -232(%rbp)
	shrl	$24, %eax
	orl	%esi, %eax
	movzbl	%al, %ecx
	movl	%ecx, %eax
	movl	%ecx, %esi
	movl	%ecx, -236(%rbp)
	subl	%edx, %eax
	xorl	%edx, %esi
	xorl	%edx, %eax
	orl	%esi, %eax
	xorl	%ecx, %eax
	sarl	$31, %eax
	movl	%eax, %esi
	leal	1(%rax), %edi
	notl	%esi
	movl	%edi, -240(%rbp)
	movq	%r13, %rdi
	andl	%esi, %edx
	andl	%ecx, %eax
	leaq	592(%r15), %rsi
	movq	%r12, %rcx
	orl	%eax, %edx
	leal	33(%rdx), %eax
	movl	%edx, -244(%rbp)
	movq	%rbx, %rdx
	subq	%rax, %rcx
	movq	%rcx, %rax
	movb	%cl, 591(%r15,%rbx)
	shrq	$8, %rax
	movq	%rcx, -216(%rbp)
	movb	%al, 590(%r15,%rbx)
	movups	%xmm2, 468(%r15)
	movdqu	260(%r15), %xmm2
	movups	%xmm6, 500(%r15)
	movdqu	324(%r15), %xmm6
	movups	%xmm2, 484(%r15)
	movdqu	308(%r15), %xmm2
	movups	%xmm7, 516(%r15)
	movdqu	340(%r15), %xmm7
	movups	%xmm2, 532(%r15)
	movups	%xmm6, 548(%r15)
	movups	%xmm7, 564(%r15)
	call	sha256_update
	leaq	-32(%r12), %rax
	movq	%rax, -224(%rbp)
	cmpq	$319, %rax
	ja	.L75
.L37:
	movl	500(%r15), %eax
	leaq	-97(%rbp), %r12
	pxor	%xmm0, %xmm0
	movq	-216(%rbp), %rbx
	movq	-224(%rbp), %rsi
	andq	$-64, %r12
	movl	572(%r15), %edx
	leaq	508(%r15), %r8
	leal	(%rax,%rbx,8), %eax
	movaps	%xmm0, (%r12)
	movl	%eax, %ecx
	movaps	%xmm0, 16(%r12)
#APP
# 576 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha256.c" 1
	bswapl %ecx
# 0 "" 2
#NO_APP
	testq	%rsi, %rsi
	je	.L61
	movq	%rbx, %rax
	movl	%ecx, -208(%rbp)
	negq	%rbx
	subq	%rax, %rsi
	movq	%rsi, %r11
	movq	-232(%rbp), %rsi
	leaq	(%rsi,%rax), %r14
	movq	%r14, %rcx
	movq	%r13, %r14
	movq	%r11, %r13
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L39:
	addq	$1, %rbx
	cmpq	%rbx, %r13
	je	.L76
.L40:
	movl	%r9d, %edx
.L42:
	movzbl	(%rcx,%rbx), %eax
	movq	%rbx, %r11
	movl	%edx, %esi
	leal	1(%rdx), %r9d
	shrq	$56, %r11
	andq	%r11, %rax
	movq	%rax, %r10
	movq	%rbx, %rax
	negq	%rax
	shrq	$56, %rax
	orq	%r11, %rax
	notq	%rax
	andl	$128, %eax
	orq	%r10, %rax
	movb	%al, 508(%r15,%rsi)
	cmpl	$63, %edx
	jne	.L39
	movl	$7, %eax
	movq	%r8, %rsi
	movl	$1, %edx
	movq	%r14, %rdi
	subq	%rbx, %rax
	movq	%rcx, -192(%rbp)
	sarq	$63, %rax
	movq	%r8, -168(%rbp)
	movq	%rax, %r9
	movl	-208(%rbp), %eax
	movq	%r9, -184(%rbp)
	andl	%r9d, %eax
	orl	%eax, 568(%r15)
	call	sha256_block_data_order@PLT
	leaq	-72(%rbx), %rax
	addq	$1, %rbx
	movq	-184(%rbp), %r9
	movl	484(%r15), %ecx
	sarq	$63, %rax
	movl	488(%r15), %edx
	andq	%r9, %rax
	movl	496(%r15), %esi
	movq	-168(%rbp), %r8
	andl	%eax, %ecx
	andl	%eax, %edx
	movd	%ecx, %xmm0
	movl	492(%r15), %ecx
	andl	%eax, %esi
	movd	%edx, %xmm6
	movd	%esi, %xmm5
	punpckldq	%xmm6, %xmm0
	movl	472(%r15), %edx
	andl	%eax, %ecx
	movd	%ecx, %xmm1
	movl	468(%r15), %ecx
	andl	%eax, %edx
	punpckldq	%xmm5, %xmm1
	movd	%edx, %xmm5
	andl	%eax, %ecx
	punpcklqdq	%xmm1, %xmm0
	por	16(%r12), %xmm0
	movd	%ecx, %xmm1
	movl	476(%r15), %ecx
	punpckldq	%xmm5, %xmm1
	andl	%eax, %ecx
	andl	480(%r15), %eax
	cmpq	%rbx, %r13
	movaps	%xmm0, 16(%r12)
	movd	%ecx, %xmm2
	movd	%eax, %xmm7
	movq	-192(%rbp), %rcx
	punpckldq	%xmm7, %xmm2
	punpcklqdq	%xmm2, %xmm1
	por	(%r12), %xmm1
	movaps	%xmm1, (%r12)
	je	.L77
	xorl	%r9d, %r9d
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L54:
	movq	%r14, %rsi
	xorl	%eax, %eax
	movq	%r12, %rbx
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L58:
	movq	-168(%rbp), %rdi
	movq	%r12, -192(%rbp)
	movq	%rdi, -184(%rbp)
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L55:
	movq	%r14, %rsi
	xorl	%eax, %eax
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L74:
	movq	%r9, %rdi
	movq	%r10, -216(%rbp)
	movq	%r9, -208(%rbp)
	movq	%r11, -176(%rbp)
	call	sha256_update
	movq	-176(%rbp), %r11
	movq	%r13, %rdi
	leaq	(%r14,%r11), %rcx
	movq	%r11, -192(%rbp)
	movq	%rcx, -184(%rbp)
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movq	-184(%rbp), %rcx
	subq	$8, %rsp
	movq	-216(%rbp), %r10
	movq	-208(%rbp), %r9
	movq	-168(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, %r8
	pushq	%rcx
	movq	%r10, %rdx
	movq	%r15, %rcx
	shrq	$6, %rdx
	movq	%r10, -176(%rbp)
	call	aesni_cbc_sha256_enc@PLT
	movq	-176(%rbp), %r10
	xorl	%edi, %edi
	movq	-192(%rbp), %r11
	movq	-208(%rbp), %r9
	movq	%r10, %rax
	movq	%r10, %rdx
	movq	%r12, %r10
	andq	$-64, %rax
	shrq	$29, %rdx
	addl	504(%r15), %edx
	movq	%rax, %rcx
	movq	%rax, -176(%rbp)
	popq	%rax
	leaq	0(,%rcx,8), %rax
	popq	%rsi
	addl	500(%r15), %eax
	movq	-168(%rbp), %rsi
	setc	%dil
	movl	%eax, 500(%r15)
	subq	%rcx, %r10
	leaq	(%r11,%rcx), %rax
	addl	%edi, %edx
	movq	%r10, -192(%rbp)
	addq	%rcx, %rsi
	movl	%edx, 504(%r15)
	movq	%rsi, -184(%rbp)
	leaq	(%r14,%rax), %rsi
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L30:
	movq	%r13, %rdi
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movq	-176(%rbp), %rdi
	movq	%r15, %rcx
	movq	-192(%rbp), %rdx
	movq	-184(%rbp), %rsi
	movq	%rax, %r8
	movl	$1, %r9d
	addq	%r14, %rdi
	call	aesni_cbc_encrypt@PLT
	movl	$1, %r11d
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L34:
	movq	-168(%rbp), %rsi
	leaq	468(%r15), %rdi
	movq	%r12, %rdx
	call	sha256_update
	movl	$1, %r11d
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L76:
	movq	-224(%rbp), %rax
	movl	-208(%rbp), %ecx
	movq	%r14, %r13
	movq	%rax, -168(%rbp)
.L38:
	movl	%r9d, %esi
	cmpl	$63, %r9d
	ja	.L43
.L41:
	movl	$64, %eax
	leaq	508(%r15,%rsi), %rdx
	xorl	%r11d, %r11d
	subq	%rsi, %rax
	cmpl	$8, %eax
	jnb	.L44
	testb	$4, %al
	jne	.L78
	testl	%eax, %eax
	je	.L45
	movb	$0, (%rdx)
	testb	$2, %al
	jne	.L79
.L45:
	movq	-168(%rbp), %rax
	movq	$-73, %r14
	subq	-216(%rbp), %r14
	addq	$64, %rax
	subq	%rsi, %rax
	leaq	(%rax,%r14), %rbx
	movq	%rax, -168(%rbp)
	sarq	$63, %rbx
	cmpl	$56, %r9d
	ja	.L52
.L50:
	movl	%ecx, 568(%r15)
	movq	%r8, %rsi
	movl	$1, %edx
	movq	%r13, %rdi
	call	sha256_block_data_order@PLT
	movl	472(%r15), %r10d
	movl	476(%r15), %r9d
	movl	480(%r15), %r8d
	movl	484(%r15), %edi
	movl	488(%r15), %esi
	andl	%ebx, %r10d
	andl	%ebx, %r9d
	movl	492(%r15), %edx
	movl	496(%r15), %eax
	orl	4(%r12), %r10d
	andl	%ebx, %r8d
	andl	%ebx, %edi
	movdqu	388(%r15), %xmm2
	orl	8(%r12), %r9d
	andl	%ebx, %esi
	andl	%ebx, %edx
	movdqu	404(%r15), %xmm6
	orl	12(%r12), %r8d
	andl	%ebx, %eax
	movdqu	356(%r15), %xmm3
	andl	468(%r15), %ebx
	movdqu	372(%r15), %xmm4
	orl	16(%r12), %edi
	orl	20(%r12), %esi
	orl	24(%r12), %edx
	orl	28(%r12), %eax
	orl	(%r12), %ebx
#APP
# 654 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha256.c" 1
	bswapl %r10d
# 0 "" 2
# 655 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha256.c" 1
	bswapl %r9d
# 0 "" 2
#NO_APP
	movl	%r10d, 4(%r12)
	movl	%r9d, 8(%r12)
#APP
# 656 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha256.c" 1
	bswapl %r8d
# 0 "" 2
# 660 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha256.c" 1
	bswapl %eax
# 0 "" 2
#NO_APP
	movl	%r8d, 12(%r12)
	movl	%eax, 28(%r12)
#APP
# 653 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha256.c" 1
	bswapl %ebx
# 0 "" 2
# 657 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha256.c" 1
	bswapl %edi
# 0 "" 2
#NO_APP
	movl	%ebx, (%r12)
	movl	%edi, 16(%r12)
	movq	%r13, %rdi
#APP
# 658 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha256.c" 1
	bswapl %esi
# 0 "" 2
# 659 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha256.c" 1
	bswapl %edx
# 0 "" 2
#NO_APP
	movl	%esi, 20(%r12)
	movq	%r12, %rsi
	movl	%edx, 24(%r12)
	movl	$32, %edx
	movups	%xmm2, 500(%r15)
	movups	%xmm6, 516(%r15)
	movups	%xmm3, 468(%r15)
	movups	%xmm4, 484(%r15)
	movdqu	420(%r15), %xmm7
	movdqu	436(%r15), %xmm3
	movdqu	452(%r15), %xmm4
	movups	%xmm7, 532(%r15)
	movups	%xmm3, 548(%r15)
	movups	%xmm4, 564(%r15)
	call	sha256_update
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	SHA256_Final@PLT
	movl	-236(%rbp), %r15d
	movq	-216(%rbp), %rax
	movl	$-32, %edi
	movq	-176(%rbp), %rcx
	movl	-244(%rbp), %r10d
	movq	%r15, %rbx
	notq	%r15
	addq	-224(%rbp), %r15
	subq	%r15, %rax
	leal	32(%rbx), %r8d
	addq	-232(%rbp), %r15
	subl	%eax, %edi
	addq	%r15, %r8
	leal	-1(%r15,%rax), %r9d
	xorl	%esi, %esi
	subl	%r15d, %edi
	.p2align 4,,10
	.p2align 3
.L51:
	movzbl	(%r15), %r11d
	movl	%r9d, %eax
	leal	(%rdi,%r15), %ebx
	subl	%r15d, %eax
	sarl	$31, %ebx
	addq	$1, %r15
	movl	%r11d, %edx
	sarl	$31, %eax
	xorb	(%r12,%rcx), %dl
	xorl	%r10d, %r11d
	andl	%ebx, %eax
	movzbl	%dl, %edx
	notl	%ebx
	andl	%eax, %edx
	andl	%ebx, %r11d
	andl	$1, %eax
	orl	%r11d, %edx
	addq	%rax, %rcx
	orl	%edx, %esi
	cmpq	%r15, %r8
	jne	.L51
	negl	%esi
	movl	%esi, %r11d
	sarl	$31, %r11d
	notl	%r11d
	andl	-240(%rbp), %r11d
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L44:
	movl	%eax, %edi
	movq	$0, (%rdx)
	movq	$0, -8(%rdx,%rdi)
	leaq	8(%rdx), %rdi
	andq	$-8, %rdi
	subq	%rdi, %rdx
	addl	%edx, %eax
	andl	$-8, %eax
	cmpl	$8, %eax
	jb	.L45
	andl	$-8, %eax
	xorl	%edx, %edx
.L48:
	movl	%edx, %r10d
	addl	$8, %edx
	movq	%r11, (%rdi,%r10)
	cmpl	%eax, %edx
	jb	.L48
	jmp	.L45
.L43:
	movq	-168(%rbp), %rax
	movq	$-73, %r14
	subq	-216(%rbp), %r14
	leaq	(%rax,%r14), %rbx
	sarq	$63, %rbx
.L52:
	movq	%rbx, %xmm1
	movl	%ecx, %esi
	movl	$1, %edx
	movq	%r13, %rdi
	movq	-216(%rbp), %rax
	punpcklqdq	%xmm1, %xmm1
	movl	%ecx, -192(%rbp)
	movq	%r8, -184(%rbp)
	leaq	8(%rax), %rbx
	subq	-168(%rbp), %rbx
	movaps	%xmm1, -208(%rbp)
	sarq	$63, %rbx
	andl	%ebx, %esi
	orl	%esi, 568(%r15)
	movq	%r8, %rsi
	call	sha256_block_data_order@PLT
	movq	%rbx, %xmm0
	movdqa	-208(%rbp), %xmm1
	movdqu	468(%r15), %xmm2
	punpcklqdq	%xmm0, %xmm0
	movq	-168(%rbp), %rax
	movq	-184(%rbp), %r8
	shufps	$136, %xmm1, %xmm1
	shufps	$136, %xmm0, %xmm0
	pand	%xmm1, %xmm0
	movdqu	484(%r15), %xmm1
	leaq	64(%rax,%r14), %rbx
	movl	-192(%rbp), %ecx
	pand	%xmm0, %xmm1
	pand	%xmm2, %xmm0
	por	(%r12), %xmm0
	sarq	$63, %rbx
	por	16(%r12), %xmm1
	movaps	%xmm0, (%r12)
	pxor	%xmm0, %xmm0
	movaps	%xmm1, 16(%r12)
	movups	%xmm0, 508(%r15)
	movups	%xmm0, 16(%r8)
	movups	%xmm0, 32(%r8)
	movups	%xmm0, 48(%r8)
	jmp	.L50
.L75:
	subq	$352, %r12
	movq	-232(%rbp), %rbx
	movl	$64, %eax
	movq	%r13, %rdi
	subl	572(%r15), %eax
	andq	$-64, %r12
	addq	%rax, %r12
	movq	%rbx, %rsi
	movq	%r12, %rdx
	call	sha256_update
	movq	%rbx, %rax
	subq	%r12, -224(%rbp)
	addq	%r12, %rax
	subq	%r12, -216(%rbp)
	movq	%rax, -232(%rbp)
	jmp	.L37
.L77:
	movq	-224(%rbp), %rax
	movq	%r14, %r13
	xorl	%esi, %esi
	xorl	%r9d, %r9d
	movl	-208(%rbp), %ecx
	movq	%rax, -168(%rbp)
	jmp	.L41
.L61:
	movq	$0, -168(%rbp)
	movl	%edx, %r9d
	jmp	.L38
.L78:
	movl	%eax, %eax
	movl	$0, (%rdx)
	movl	$0, -4(%rdx,%rax)
	jmp	.L45
.L79:
	movl	%eax, %eax
	movw	$0, -2(%rdx,%rax)
	jmp	.L45
.L73:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE408:
	.size	aesni_cbc_hmac_sha256_cipher, .-aesni_cbc_hmac_sha256_cipher
	.p2align 4
	.type	aesni_cbc_hmac_sha256_init_key, @function
aesni_cbc_hmac_sha256_init_key:
.LFB405:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%ecx, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	%r12, %rdi
	movq	%rax, %rbx
	testl	%r14d, %r14d
	je	.L81
	call	EVP_CIPHER_CTX_key_length@PLT
	movq	%rbx, %rdx
	movq	%r13, %rdi
	leal	0(,%rax,8), %esi
	call	aesni_set_encrypt_key@PLT
	movl	%eax, %r12d
.L82:
	leaq	244(%rbx), %rdi
	call	SHA256_Init@PLT
	movdqu	244(%rbx), %xmm6
	movl	%r12d, %eax
	movdqu	260(%rbx), %xmm5
	movdqu	276(%rbx), %xmm4
	movdqu	292(%rbx), %xmm3
	notl	%eax
	movq	$-1, 584(%rbx)
	movdqu	308(%rbx), %xmm2
	movdqu	324(%rbx), %xmm1
	movups	%xmm6, 356(%rbx)
	shrl	$31, %eax
	movdqu	340(%rbx), %xmm0
	movups	%xmm5, 372(%rbx)
	movups	%xmm4, 388(%rbx)
	movups	%xmm3, 404(%rbx)
	movups	%xmm2, 420(%rbx)
	movups	%xmm1, 436(%rbx)
	movups	%xmm0, 452(%rbx)
	movups	%xmm6, 468(%rbx)
	movups	%xmm5, 484(%rbx)
	movups	%xmm4, 500(%rbx)
	movups	%xmm3, 516(%rbx)
	movups	%xmm2, 532(%rbx)
	movups	%xmm1, 548(%rbx)
	movups	%xmm0, 564(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L81:
	.cfi_restore_state
	call	EVP_CIPHER_CTX_key_length@PLT
	movq	%rbx, %rdx
	movq	%r13, %rdi
	leal	0(,%rax,8), %esi
	call	aesni_set_decrypt_key@PLT
	movl	%eax, %r12d
	jmp	.L82
	.cfi_endproc
.LFE405:
	.size	aesni_cbc_hmac_sha256_init_key, .-aesni_cbc_hmac_sha256_init_key
	.p2align 4
	.type	tls1_1_multi_block_encrypt, @function
tls1_1_multi_block_encrypt:
.LFB407:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%r8d, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	leal	0(,%r8,4), %ecx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$2040, %rsp
	movq	%rsi, -2048(%rbp)
	movl	%r8d, %esi
	movl	%r8d, -2000(%rbp)
	sall	$6, %esi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-1376(%rbp), %rax
	movl	%ecx, -1956(%rbp)
	movq	%rax, %rdi
	movq	%rax, -1976(%rbp)
	call	RAND_bytes@PLT
	movq	$0, -1992(%rbp)
	testl	%eax, %eax
	jle	.L84
	leal	1(%r14), %ecx
	movl	%r13d, %edi
	leaq	-352(%rbp), %rax
	shrl	%cl, %edi
	andq	$-32, %rax
	leaq	32(%rax), %r12
	addl	%edi, %r13d
	movl	%edi, %eax
	movl	%edi, -1960(%rbp)
	sall	%cl, %eax
	movl	%r13d, %esi
	subl	%eax, %esi
	movl	%esi, -1964(%rbp)
	cmpl	%esi, %edi
	jb	.L153
.L86:
	movl	-1960(%rbp), %edi
	movl	-1964(%rbp), %edx
	movq	%rbx, -1952(%rbp)
	movq	-2048(%rbp), %rsi
	movdqa	-1376(%rbp), %xmm0
	movq	%rbx, -1696(%rbp)
	leal	48(%rdi), %eax
	movl	-1956(%rbp), %r11d
	andl	$-16, %eax
	leaq	21(%rsi), %rcx
	movups	%xmm0, 5(%rsi)
	addl	$21, %eax
	cmpl	%edx, %edi
	movq	%rcx, -1688(%rbp)
	cmovbe	%edi, %edx
	movups	%xmm0, -1672(%rbp)
	leal	-51(%rdx), %esi
	movl	%esi, -2056(%rbp)
	cmpl	$1, %r11d
	jle	.L87
	movl	%edi, %edx
	addq	%rax, %rcx
	movdqa	-1360(%rbp), %xmm3
	addq	%rdx, %rbx
	movq	%rcx, %xmm2
	movq	%rbx, %xmm0
	movq	%rbx, -1936(%rbp)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, -1656(%rbp)
	movups	%xmm3, -16(%rcx)
	movq	-1936(%rbp), %rcx
	movq	-1648(%rbp), %rsi
	movdqa	-1360(%rbp), %xmm4
	movdqa	-1344(%rbp), %xmm2
	addq	%rdx, %rcx
	addq	%rax, %rsi
	movq	%rsi, %xmm5
	movq	%rcx, %xmm0
	movq	%rcx, -1920(%rbp)
	punpcklqdq	%xmm5, %xmm0
	movaps	%xmm4, -1632(%rbp)
	movaps	%xmm0, -1616(%rbp)
	movups	%xmm2, -16(%rsi)
	movq	-1920(%rbp), %rcx
	movq	-1608(%rbp), %rsi
	movdqa	-1344(%rbp), %xmm3
	movdqa	-1328(%rbp), %xmm5
	addq	%rdx, %rcx
	addq	%rax, %rsi
	movq	%rcx, %xmm0
	movq	%rsi, %xmm4
	movq	%rcx, -1904(%rbp)
	punpcklqdq	%xmm4, %xmm0
	movups	%xmm3, -1592(%rbp)
	movups	%xmm0, -1576(%rbp)
	movups	%xmm5, -16(%rsi)
	movdqa	-1328(%rbp), %xmm6
	movaps	%xmm6, -1552(%rbp)
	cmpl	$4, %r11d
	je	.L88
	movq	-1904(%rbp), %rcx
	movq	-1568(%rbp), %rsi
	addq	%rdx, %rcx
	addq	%rax, %rsi
	movq	%rsi, %xmm7
	movq	%rcx, %xmm0
	movq	%rcx, -1888(%rbp)
	punpcklqdq	%xmm7, %xmm0
	movdqa	-1312(%rbp), %xmm7
	movaps	%xmm0, -1536(%rbp)
	movups	%xmm7, -16(%rsi)
	movq	-1888(%rbp), %rcx
	movq	-1528(%rbp), %rsi
	movdqa	-1312(%rbp), %xmm2
	movdqa	-1296(%rbp), %xmm4
	addq	%rdx, %rcx
	addq	%rax, %rsi
	movq	%rsi, %xmm3
	movq	%rcx, %xmm0
	movq	%rcx, -1872(%rbp)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm2, -1512(%rbp)
	movups	%xmm0, -1496(%rbp)
	movups	%xmm4, -16(%rsi)
	movq	-1872(%rbp), %rcx
	movq	-1488(%rbp), %rsi
	movdqa	-1296(%rbp), %xmm5
	movdqa	-1280(%rbp), %xmm7
	addq	%rdx, %rcx
	addq	%rax, %rsi
	movq	%rcx, %xmm0
	movq	%rsi, %xmm6
	movq	%rcx, -1856(%rbp)
	punpcklqdq	%xmm6, %xmm0
	movaps	%xmm5, -1472(%rbp)
	movaps	%xmm0, -1456(%rbp)
	movups	%xmm7, -16(%rsi)
	addq	-1856(%rbp), %rdx
	addq	-1448(%rbp), %rax
	movdqa	-1280(%rbp), %xmm2
	movq	%rdx, %xmm0
	movdqa	-1264(%rbp), %xmm4
	movq	%rax, %xmm3
	punpcklqdq	%xmm3, %xmm0
	movq	%rdx, -1840(%rbp)
	movups	%xmm2, -1432(%rbp)
	movups	%xmm0, -1416(%rbp)
	movups	%xmm4, -16(%rax)
	movdqa	-1264(%rbp), %xmm5
	movaps	%xmm5, -1392(%rbp)
.L88:
	movq	508(%r15), %r10
	movq	%r10, -1376(%rbp)
#APP
# 207 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha256.c" 1
	bswapq %r10
# 0 "" 2
#NO_APP
.L118:
	movl	-1956(%rbp), %eax
	leaq	-1952(%rbp), %rdi
	movzbl	516(%r15), %ebx
	movq	%r10, -2040(%rbp)
	movq	%rdi, -2008(%rbp)
	leaq	-1824(%rbp), %r13
	xorl	%esi, %esi
	movzbl	517(%r15), %ecx
	subl	$1, %eax
	movq	%r12, -2064(%rbp)
	movq	%r12, %rdx
	movq	%r13, %r9
	movl	%eax, -1968(%rbp)
	movl	468(%r15), %eax
	movl	%ebx, %r12d
	movzbl	518(%r15), %r8d
	movq	%r15, -2072(%rbp)
	movl	%eax, -1984(%rbp)
	movl	476(%r15), %eax
	movq	%r13, -2080(%rbp)
	movl	472(%r15), %r14d
	movl	%ecx, %r13d
	movl	%eax, -1992(%rbp)
	movl	484(%r15), %eax
	movl	480(%r15), %r11d
	movl	%eax, -1996(%rbp)
	movl	488(%r15), %eax
	movl	%eax, -2016(%rbp)
	movl	492(%r15), %eax
	movl	%eax, -2024(%rbp)
	movl	496(%r15), %eax
	movl	%r8d, %r15d
	movl	%eax, -2032(%rbp)
	movq	-1976(%rbp), %rax
	.p2align 4,,10
	.p2align 3
.L91:
	movl	-1984(%rbp), %ecx
	cmpl	%esi, -1968(%rbp)
	movl	%r14d, 32(%rdx)
	movl	-1964(%rbp), %ebx
	cmovne	-1960(%rbp), %ebx
	movl	%r11d, 96(%rdx)
	addq	$16, %rdi
	movl	%ecx, (%rdx)
	movl	-1992(%rbp), %ecx
	addq	$4, %rdx
	addq	$16, %r9
	movb	%bh, 11(%rax)
	movl	%ecx, 60(%rdx)
	movl	-1996(%rbp), %ecx
	movb	%bl, 12(%rax)
	subl	$51, %ebx
	movl	%ecx, 124(%rdx)
	movl	-2016(%rbp), %ecx
	shrl	$6, %ebx
	movb	%r12b, 8(%rax)
	movl	%ecx, 156(%rdx)
	movl	-2024(%rbp), %ecx
	movb	%r13b, 9(%rax)
	movl	%ecx, 188(%rdx)
	movl	-2032(%rbp), %ecx
	movb	%r15b, 10(%rax)
	movl	%ecx, 220(%rdx)
	movq	-2040(%rbp), %rcx
	addq	%rsi, %rcx
	addq	$1, %rsi
#APP
# 226 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha256.c" 1
	bswapq %rcx
# 0 "" 2
#NO_APP
	movq	%rcx, (%rax)
	movq	-16(%rdi), %rcx
	movdqu	(%rcx), %xmm2
	addq	$51, %rcx
	movups	%xmm2, 13(%rax)
	movdqu	-35(%rcx), %xmm3
	movups	%xmm3, 29(%rax)
	movdqu	-19(%rcx), %xmm4
	movups	%xmm4, 45(%rax)
	movzwl	-3(%rcx), %r10d
	movw	%r10w, 61(%rax)
	movzbl	-1(%rcx), %r10d
	movq	%rax, -16(%r9)
	subq	$-128, %rax
	movb	%r10b, -65(%rax)
	movq	%rcx, -16(%rdi)
	movl	%ebx, -8(%rdi)
	movl	$1, -8(%r9)
	cmpl	%esi, -1956(%rbp)
	ja	.L91
	movq	-2064(%rbp), %r12
	movq	-2080(%rbp), %r13
	movl	-2000(%rbp), %edx
	movq	-2072(%rbp), %r15
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	sha256_multi_block@PLT
	cmpl	$2111, -2056(%rbp)
	ja	.L117
	leaq	-1696(%rbp), %rax
	movl	$0, -1996(%rbp)
	movq	%rax, -2032(%rbp)
.L97:
	movl	-2000(%rbp), %edx
	movq	-2008(%rbp), %rsi
	movq	%r12, %rdi
	call	sha256_multi_block@PLT
	movq	-1976(%rbp), %rdi
	xorl	%eax, %eax
	movl	$128, %ecx
	rep stosq
	movl	-1956(%rbp), %eax
	testl	%eax, %eax
	je	.L99
	subl	$1, %eax
	movl	$-51, %ebx
	xorl	%r9d, %r9d
	xorl	%r14d, %r14d
	subl	-1996(%rbp), %ebx
	movl	%eax, -1968(%rbp)
	leaq	-1816(%rbp), %rax
	movl	%ebx, -2016(%rbp)
	leaq	-1944(%rbp), %rbx
	movq	%r12, -2040(%rbp)
	movq	%rbx, %r12
	movl	%r9d, %ebx
	movq	%rax, -2024(%rbp)
	movq	%r15, -2056(%rbp)
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L155:
	movl	%eax, 60(%rsi,%r14,8)
	movq	-2024(%rbp), %rax
	movl	$1, (%rax,%r14)
.L152:
	movq	%rdi, 0(%r13,%r14)
	addl	$1, %ebx
	addq	$16, %r14
	cmpl	%ebx, -1956(%rbp)
	je	.L154
.L104:
	movl	(%r12,%r14), %esi
	movl	-2016(%rbp), %ecx
	cmpl	-1968(%rbp), %ebx
	movl	-1960(%rbp), %r8d
	cmove	-1964(%rbp), %r8d
	sall	$6, %esi
	movq	-1976(%rbp), %rax
	subl	%esi, %ecx
	leal	(%rcx,%r8), %edx
	movq	-2008(%rbp), %rcx
	leaq	(%rax,%r14,8), %rdi
	movl	%r8d, -1992(%rbp)
	movq	%rdx, -1984(%rbp)
	movq	%rdx, %r15
	addq	(%rcx,%r14), %rsi
	call	memcpy@PLT
	movq	-1984(%rbp), %rdx
	leaq	-48(%rbp), %rcx
	movl	-1992(%rbp), %r8d
	movq	%rax, %rdi
	movl	%ebx, %eax
	movq	-1976(%rbp), %rsi
	salq	$7, %rax
	addq	%rcx, %rax
	movb	$-128, -1328(%rdx,%rax)
	leal	616(,%r8,8), %eax
#APP
# 300 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha256.c" 1
	bswapl %eax
# 0 "" 2
#NO_APP
	cmpl	$55, %r15d
	jbe	.L155
	movl	%eax, 124(%rsi,%r14,8)
	movq	-2024(%rbp), %rax
	movl	$2, (%rax,%r14)
	jmp	.L152
	.p2align 4,,10
	.p2align 3
.L159:
	movq	-2016(%rbp), %r12
.L113:
	movl	-2000(%rbp), %edx
	movq	-2032(%rbp), %rdi
	movq	%r15, %rsi
	call	aesni_multi_cbc_encrypt@PLT
	movq	-1976(%rbp), %rdi
	movl	$1024, %esi
	call	OPENSSL_cleanse@PLT
	movl	$256, %esi
	movq	%r12, %rdi
	call	OPENSSL_cleanse@PLT
.L84:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L156
	movq	-1992(%rbp), %rax
	addq	$2040, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L153:
	.cfi_restore_state
	movl	-1956(%rbp), %ecx
	leal	22(%rsi), %eax
	andl	$63, %eax
	leal	-1(%rcx), %edx
	cmpl	%edx, %eax
	jnb	.L86
	movl	%esi, %eax
	addl	$1, %edi
	subl	%ecx, %eax
	movl	%edi, -1960(%rbp)
	addl	$1, %eax
	movl	%eax, -1964(%rbp)
	jmp	.L86
.L117:
	movq	-1952(%rbp), %rax
	movl	-1956(%rbp), %edi
	movl	$32, -1816(%rbp)
	movl	$128, -1680(%rbp)
	movq	%rax, -1824(%rbp)
	cmpl	$1, %edi
	jbe	.L95
	movq	-1936(%rbp), %rax
	movl	$32, -1800(%rbp)
	movl	$128, -1640(%rbp)
	movq	%rax, -1808(%rbp)
	movq	-1920(%rbp), %rax
	movl	$32, -1784(%rbp)
	movq	%rax, -1792(%rbp)
	movq	-1904(%rbp), %rax
	movl	$128, -1600(%rbp)
	movq	%rax, -1776(%rbp)
	movl	$32, -1768(%rbp)
	movl	$128, -1560(%rbp)
	cmpl	$4, %edi
	je	.L95
	movq	-1888(%rbp), %rax
	movl	$32, -1752(%rbp)
	movl	$128, -1520(%rbp)
	movq	%rax, -1760(%rbp)
	movq	-1872(%rbp), %rax
	movl	$32, -1736(%rbp)
	movq	%rax, -1744(%rbp)
	movq	-1856(%rbp), %rax
	movl	$128, -1480(%rbp)
	movq	%rax, -1728(%rbp)
	movl	$32, -1720(%rbp)
	movl	$128, -1440(%rbp)
	cmpl	$8, %edi
	jne	.L95
	movq	-1840(%rbp), %rax
	movl	$32, -1704(%rbp)
	movl	$128, -1400(%rbp)
	movq	%rax, -1712(%rbp)
.L95:
	leaq	-1696(%rbp), %rax
	movl	-2056(%rbp), %ebx
	movl	$0, -1996(%rbp)
	movq	%rax, -2032(%rbp)
	movl	-2000(%rbp), %r14d
	shrl	$6, %ebx
	.p2align 4,,10
	.p2align 3
.L94:
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	sha256_multi_block@PLT
	movq	-2032(%rbp), %rdi
	movl	%r14d, %edx
	movq	%r15, %rsi
	call	aesni_multi_cbc_encrypt@PLT
	movl	-1956(%rbp), %eax
	movdqa	.LC0(%rip), %xmm0
	testl	%eax, %eax
	je	.L98
	movq	-1952(%rbp), %rax
	movdqa	-1696(%rbp), %xmm1
	movl	$32, -1816(%rbp)
	subl	$32, -1944(%rbp)
	addq	$2048, %rax
	paddq	%xmm0, %xmm1
	subl	$32, -1928(%rbp)
	movq	%rax, -1952(%rbp)
	movq	%rax, -1824(%rbp)
	movq	-1688(%rbp), %rax
	movaps	%xmm1, -1696(%rbp)
	movdqu	-1656(%rbp), %xmm1
	movdqu	2032(%rax), %xmm5
	movq	-1936(%rbp), %rax
	movl	$128, -1680(%rbp)
	paddq	%xmm0, %xmm1
	subl	$32, -1912(%rbp)
	addq	$2048, %rax
	movups	%xmm5, -1672(%rbp)
	movq	%rax, -1936(%rbp)
	movq	%rax, -1808(%rbp)
	movq	-1648(%rbp), %rax
	movups	%xmm1, -1656(%rbp)
	movdqa	-1616(%rbp), %xmm1
	movdqu	2032(%rax), %xmm6
	movq	-1920(%rbp), %rax
	movl	$32, -1800(%rbp)
	movl	$128, -1640(%rbp)
	paddq	%xmm0, %xmm1
	addq	$2048, %rax
	movaps	%xmm6, -1632(%rbp)
	movq	%rax, -1920(%rbp)
	movq	%rax, -1792(%rbp)
	movq	-1608(%rbp), %rax
	movl	$32, -1784(%rbp)
	movaps	%xmm1, -1616(%rbp)
	movdqu	2032(%rax), %xmm7
	movq	-1904(%rbp), %rax
	movdqu	-1576(%rbp), %xmm1
	subl	$32, -1896(%rbp)
	addq	$2048, %rax
	cmpl	$4, -1956(%rbp)
	movl	$128, -1600(%rbp)
	movq	%rax, -1904(%rbp)
	paddq	%xmm0, %xmm1
	movq	%rax, -1776(%rbp)
	movq	-1568(%rbp), %rax
	movl	$32, -1768(%rbp)
	movdqu	2032(%rax), %xmm5
	movups	%xmm7, -1592(%rbp)
	movl	$128, -1560(%rbp)
	movups	%xmm1, -1576(%rbp)
	movaps	%xmm5, -1552(%rbp)
	je	.L98
	movq	-1888(%rbp), %rax
	movdqa	-1536(%rbp), %xmm1
	movl	$32, -1752(%rbp)
	subl	$32, -1880(%rbp)
	addq	$2048, %rax
	paddq	%xmm0, %xmm1
	subl	$32, -1864(%rbp)
	movq	%rax, -1888(%rbp)
	movq	%rax, -1760(%rbp)
	movq	-1528(%rbp), %rax
	movaps	%xmm1, -1536(%rbp)
	movdqu	-1496(%rbp), %xmm1
	movdqu	2032(%rax), %xmm6
	movq	-1872(%rbp), %rax
	movl	$128, -1520(%rbp)
	paddq	%xmm0, %xmm1
	subl	$32, -1848(%rbp)
	addq	$2048, %rax
	movups	%xmm6, -1512(%rbp)
	movq	%rax, -1872(%rbp)
	movq	%rax, -1744(%rbp)
	movq	-1488(%rbp), %rax
	movups	%xmm1, -1496(%rbp)
	movdqa	-1456(%rbp), %xmm1
	movdqu	2032(%rax), %xmm7
	movq	-1856(%rbp), %rax
	movl	$32, -1736(%rbp)
	movl	$128, -1480(%rbp)
	paddq	%xmm0, %xmm1
	addq	$2048, %rax
	movaps	%xmm7, -1472(%rbp)
	movq	%rax, -1856(%rbp)
	movq	%rax, -1728(%rbp)
	movq	-1448(%rbp), %rax
	movl	$32, -1720(%rbp)
	movaps	%xmm1, -1456(%rbp)
	movdqu	2032(%rax), %xmm6
	movq	-1840(%rbp), %rax
	movdqu	-1416(%rbp), %xmm1
	subl	$32, -1832(%rbp)
	addq	$2048, %rax
	movl	$128, -1440(%rbp)
	movq	%rax, -1840(%rbp)
	paddq	%xmm0, %xmm1
	movq	%rax, -1712(%rbp)
	movq	-1408(%rbp), %rax
	movl	$32, -1704(%rbp)
	movdqu	2032(%rax), %xmm7
	movups	%xmm6, -1432(%rbp)
	movl	$128, -1400(%rbp)
	movups	%xmm1, -1416(%rbp)
	movaps	%xmm7, -1392(%rbp)
.L98:
	addl	$2048, -1996(%rbp)
	subl	$32, %ebx
	cmpl	$32, %ebx
	ja	.L94
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L154:
	movq	-2040(%rbp), %r12
	movl	-2000(%rbp), %edx
	movq	%r13, %rsi
	movq	-2056(%rbp), %r15
	movq	%r12, %rdi
	call	sha256_multi_block@PLT
	movq	-1976(%rbp), %rdi
	xorl	%eax, %eax
	movl	$128, %ecx
	movl	356(%r15), %r8d
	movl	364(%r15), %esi
	rep stosq
	movl	372(%r15), %edx
	movl	360(%r15), %edi
	movl	368(%r15), %ecx
	movl	376(%r15), %r10d
	movl	380(%r15), %r11d
	movl	384(%r15), %ebx
	movl	(%r12), %eax
	movl	%r8d, (%r12)
#APP
# 322 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha256.c" 1
	bswapl %eax
# 0 "" 2
#NO_APP
	movl	%eax, -1376(%rbp)
	movl	32(%r12), %eax
	movl	%edi, 32(%r12)
#APP
# 324 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha256.c" 1
	bswapl %eax
# 0 "" 2
#NO_APP
	movl	%eax, -1372(%rbp)
	movl	64(%r12), %eax
	movl	%esi, 64(%r12)
#APP
# 326 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha256.c" 1
	bswapl %eax
# 0 "" 2
#NO_APP
	movl	%eax, -1368(%rbp)
	movl	96(%r12), %eax
	movl	%ecx, 96(%r12)
#APP
# 328 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha256.c" 1
	bswapl %eax
# 0 "" 2
#NO_APP
	movl	%eax, -1364(%rbp)
	movl	128(%r12), %eax
	movl	%edx, 128(%r12)
#APP
# 330 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha256.c" 1
	bswapl %eax
# 0 "" 2
#NO_APP
	movl	%eax, -1360(%rbp)
	movl	160(%r12), %eax
	movl	%r10d, 160(%r12)
#APP
# 332 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha256.c" 1
	bswapl %eax
# 0 "" 2
#NO_APP
	movl	%eax, -1356(%rbp)
	movl	192(%r12), %eax
	movl	%r11d, 192(%r12)
#APP
# 334 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha256.c" 1
	bswapl %eax
# 0 "" 2
#NO_APP
	movl	%eax, -1352(%rbp)
	movl	224(%r12), %eax
#APP
# 336 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha256.c" 1
	bswapl %eax
# 0 "" 2
#NO_APP
	movl	%eax, -1348(%rbp)
	movl	4(%r12), %r9d
	movl	$768, %eax
#APP
# 322 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha256.c" 1
	bswapl %r9d
# 0 "" 2
# 339 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha256.c" 1
	bswapl %eax
# 0 "" 2
#NO_APP
	movl	%r9d, -1248(%rbp)
	movl	36(%r12), %r9d
	movl	%ebx, 224(%r12)
#APP
# 324 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha256.c" 1
	bswapl %r9d
# 0 "" 2
#NO_APP
	movb	$-128, -1344(%rbp)
	movl	%r9d, -1244(%rbp)
	movl	68(%r12), %r9d
	movl	%eax, -1316(%rbp)
#APP
# 326 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha256.c" 1
	bswapl %r9d
# 0 "" 2
#NO_APP
	movl	%r8d, 4(%r12)
	movl	%r9d, -1240(%rbp)
	movl	100(%r12), %r9d
	movl	%edi, 36(%r12)
#APP
# 328 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha256.c" 1
	bswapl %r9d
# 0 "" 2
#NO_APP
	movl	%esi, 68(%r12)
	movl	%r9d, -1236(%rbp)
	movl	132(%r12), %r9d
	movl	%ecx, 100(%r12)
#APP
# 330 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha256.c" 1
	bswapl %r9d
# 0 "" 2
#NO_APP
	movl	%edx, 132(%r12)
	movl	%r9d, -1232(%rbp)
	movl	164(%r12), %r9d
	movb	$-128, -1216(%rbp)
#APP
# 332 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha256.c" 1
	bswapl %r9d
# 0 "" 2
#NO_APP
	movl	%r10d, 164(%r12)
	movl	%r9d, -1228(%rbp)
	movl	196(%r12), %r9d
	movl	%eax, -1188(%rbp)
#APP
# 334 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha256.c" 1
	bswapl %r9d
# 0 "" 2
#NO_APP
	movl	%r11d, 196(%r12)
	movl	%r9d, -1224(%rbp)
	movl	228(%r12), %r9d
	movl	%ebx, 228(%r12)
#APP
# 336 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha256.c" 1
	bswapl %r9d
# 0 "" 2
#NO_APP
	movl	%r9d, -1220(%rbp)
	movl	8(%r12), %r9d
	movl	%r8d, 8(%r12)
#APP
# 322 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha256.c" 1
	bswapl %r9d
# 0 "" 2
#NO_APP
	movl	%r9d, -1120(%rbp)
	movl	40(%r12), %r9d
	movl	%edi, 40(%r12)
#APP
# 324 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha256.c" 1
	bswapl %r9d
# 0 "" 2
#NO_APP
	movb	$-128, -1088(%rbp)
	movl	%r9d, -1116(%rbp)
	movl	72(%r12), %r9d
	movl	%eax, -1060(%rbp)
#APP
# 326 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha256.c" 1
	bswapl %r9d
# 0 "" 2
#NO_APP
	movl	%esi, 72(%r12)
	movl	%r9d, -1112(%rbp)
	movl	104(%r12), %r9d
	movl	%ecx, 104(%r12)
#APP
# 328 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha256.c" 1
	bswapl %r9d
# 0 "" 2
#NO_APP
	movl	%r9d, -1108(%rbp)
	movl	136(%r12), %r9d
	movl	%edx, 136(%r12)
#APP
# 330 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha256.c" 1
	bswapl %r9d
# 0 "" 2
#NO_APP
	movl	%r9d, -1104(%rbp)
	movl	168(%r12), %r9d
	movl	%r10d, 168(%r12)
#APP
# 332 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha256.c" 1
	bswapl %r9d
# 0 "" 2
#NO_APP
	movl	%r9d, -1100(%rbp)
	movl	200(%r12), %r9d
	movl	%r11d, 200(%r12)
#APP
# 334 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha256.c" 1
	bswapl %r9d
# 0 "" 2
#NO_APP
	movl	%r9d, -1096(%rbp)
	movl	232(%r12), %r9d
	movl	%ebx, 232(%r12)
#APP
# 336 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha256.c" 1
	bswapl %r9d
# 0 "" 2
#NO_APP
	movl	%r9d, -1092(%rbp)
	movl	12(%r12), %r9d
	movl	%r8d, 12(%r12)
#APP
# 322 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha256.c" 1
	bswapl %r9d
# 0 "" 2
#NO_APP
	movl	%r9d, -992(%rbp)
	movl	44(%r12), %r9d
	movl	%edi, 44(%r12)
#APP
# 324 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha256.c" 1
	bswapl %r9d
# 0 "" 2
#NO_APP
	movl	%r9d, -988(%rbp)
	movl	76(%r12), %r9d
	movl	%esi, 76(%r12)
#APP
# 326 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha256.c" 1
	bswapl %r9d
# 0 "" 2
#NO_APP
	movl	%r9d, -984(%rbp)
	movl	108(%r12), %r9d
#APP
# 328 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha256.c" 1
	bswapl %r9d
# 0 "" 2
#NO_APP
	movl	%r9d, -980(%rbp)
	movl	140(%r12), %r9d
#APP
# 330 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha256.c" 1
	bswapl %r9d
# 0 "" 2
#NO_APP
	movl	%r9d, -976(%rbp)
	movl	172(%r12), %r9d
#APP
# 332 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha256.c" 1
	bswapl %r9d
# 0 "" 2
#NO_APP
	movl	%r9d, -972(%rbp)
	movl	204(%r12), %r9d
#APP
# 334 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha256.c" 1
	bswapl %r9d
# 0 "" 2
#NO_APP
	movl	%r9d, -968(%rbp)
	movl	236(%r12), %r9d
#APP
# 336 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha256.c" 1
	bswapl %r9d
# 0 "" 2
#NO_APP
	cmpl	$4, -1956(%rbp)
	movl	%ecx, 108(%r12)
	movl	%edx, 140(%r12)
	movl	%r10d, 172(%r12)
	movl	%r11d, 204(%r12)
	movl	%r9d, -964(%rbp)
	movl	%ebx, 236(%r12)
	movb	$-128, -960(%rbp)
	movl	%eax, -932(%rbp)
	je	.L114
	movl	16(%r12), %r9d
	movb	$-128, -832(%rbp)
	movl	%r8d, 16(%r12)
#APP
# 322 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha256.c" 1
	bswapl %r9d
# 0 "" 2
#NO_APP
	movl	%eax, -804(%rbp)
	movl	%r9d, -864(%rbp)
	movl	48(%r12), %r9d
	movl	%edi, 48(%r12)
#APP
# 324 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha256.c" 1
	bswapl %r9d
# 0 "" 2
#NO_APP
	movl	%r9d, -860(%rbp)
	movl	80(%r12), %r9d
	movl	%esi, 80(%r12)
#APP
# 326 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha256.c" 1
	bswapl %r9d
# 0 "" 2
#NO_APP
	movl	%r9d, -856(%rbp)
	movl	112(%r12), %r9d
	movl	%ecx, 112(%r12)
#APP
# 328 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha256.c" 1
	bswapl %r9d
# 0 "" 2
#NO_APP
	movl	%r9d, -852(%rbp)
	movl	144(%r12), %r9d
	movl	%edx, 144(%r12)
#APP
# 330 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha256.c" 1
	bswapl %r9d
# 0 "" 2
#NO_APP
	movl	%r9d, -848(%rbp)
	movl	176(%r12), %r9d
	movl	%r10d, 176(%r12)
#APP
# 332 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha256.c" 1
	bswapl %r9d
# 0 "" 2
#NO_APP
	movl	%r9d, -844(%rbp)
	movl	208(%r12), %r9d
	movl	%r11d, 208(%r12)
#APP
# 334 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha256.c" 1
	bswapl %r9d
# 0 "" 2
#NO_APP
	movl	%r9d, -840(%rbp)
	movl	240(%r12), %r9d
	movl	%ebx, 240(%r12)
#APP
# 336 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha256.c" 1
	bswapl %r9d
# 0 "" 2
#NO_APP
	movl	%r9d, -836(%rbp)
	movl	20(%r12), %r9d
	movl	%r8d, 20(%r12)
#APP
# 322 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha256.c" 1
	bswapl %r9d
# 0 "" 2
#NO_APP
	movl	%r9d, -736(%rbp)
	movl	52(%r12), %r9d
	movl	%edi, 52(%r12)
#APP
# 324 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha256.c" 1
	bswapl %r9d
# 0 "" 2
#NO_APP
	movl	%r9d, -732(%rbp)
	movl	84(%r12), %r9d
	movl	%esi, 84(%r12)
#APP
# 326 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha256.c" 1
	bswapl %r9d
# 0 "" 2
#NO_APP
	movb	$-128, -704(%rbp)
	movl	%r9d, -728(%rbp)
	movl	116(%r12), %r9d
	movl	%eax, -676(%rbp)
	movl	%ecx, 116(%r12)
#APP
# 328 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha256.c" 1
	bswapl %r9d
# 0 "" 2
#NO_APP
	movl	%r9d, -724(%rbp)
	movl	148(%r12), %r9d
	movl	%edx, 148(%r12)
#APP
# 330 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha256.c" 1
	bswapl %r9d
# 0 "" 2
#NO_APP
	movl	%r9d, -720(%rbp)
	movl	180(%r12), %r9d
	movl	%r10d, 180(%r12)
#APP
# 332 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha256.c" 1
	bswapl %r9d
# 0 "" 2
#NO_APP
	movl	%r9d, -716(%rbp)
	movl	212(%r12), %r9d
	movl	%r11d, 212(%r12)
#APP
# 334 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha256.c" 1
	bswapl %r9d
# 0 "" 2
#NO_APP
	movl	%r9d, -712(%rbp)
	movl	244(%r12), %r9d
	movl	%ebx, 244(%r12)
#APP
# 336 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha256.c" 1
	bswapl %r9d
# 0 "" 2
#NO_APP
	movl	%r9d, -708(%rbp)
	movl	24(%r12), %r9d
	movl	%r8d, 24(%r12)
#APP
# 322 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha256.c" 1
	bswapl %r9d
# 0 "" 2
#NO_APP
	movl	%r9d, -608(%rbp)
	movl	56(%r12), %r9d
	movl	%edi, 56(%r12)
#APP
# 324 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha256.c" 1
	bswapl %r9d
# 0 "" 2
#NO_APP
	movl	%r9d, -604(%rbp)
	movl	88(%r12), %r9d
	movl	%esi, 88(%r12)
#APP
# 326 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha256.c" 1
	bswapl %r9d
# 0 "" 2
#NO_APP
	movl	%r9d, -600(%rbp)
	movl	120(%r12), %r9d
	movl	%ecx, 120(%r12)
#APP
# 328 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha256.c" 1
	bswapl %r9d
# 0 "" 2
#NO_APP
	movl	%r9d, -596(%rbp)
	movl	152(%r12), %r9d
	movl	%edx, 152(%r12)
#APP
# 330 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha256.c" 1
	bswapl %r9d
# 0 "" 2
#NO_APP
	movb	$-128, -576(%rbp)
	movl	%r9d, -592(%rbp)
	movl	184(%r12), %r9d
	movl	%eax, -548(%rbp)
#APP
# 332 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha256.c" 1
	bswapl %r9d
# 0 "" 2
#NO_APP
	movl	%r10d, 184(%r12)
	movl	%r9d, -588(%rbp)
	movl	216(%r12), %r9d
	movl	%r11d, 216(%r12)
#APP
# 334 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha256.c" 1
	bswapl %r9d
# 0 "" 2
#NO_APP
	movl	%r9d, -584(%rbp)
	movl	248(%r12), %r9d
	movl	%ebx, 248(%r12)
#APP
# 336 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha256.c" 1
	bswapl %r9d
# 0 "" 2
#NO_APP
	movl	%r9d, -580(%rbp)
	movl	28(%r12), %r9d
	movl	%r8d, 28(%r12)
	movl	60(%r12), %r8d
#APP
# 322 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha256.c" 1
	bswapl %r9d
# 0 "" 2
#NO_APP
	movl	%edi, 60(%r12)
	movl	92(%r12), %edi
	movl	%r9d, -480(%rbp)
	movl	%esi, 92(%r12)
	movl	124(%r12), %esi
#APP
# 324 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha256.c" 1
	bswapl %r8d
# 0 "" 2
#NO_APP
	movl	%ecx, 124(%r12)
	movl	156(%r12), %ecx
	movl	%r8d, -476(%rbp)
	movl	%edx, 156(%r12)
	movl	188(%r12), %edx
#APP
# 326 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha256.c" 1
	bswapl %edi
# 0 "" 2
# 332 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha256.c" 1
	bswapl %edx
# 0 "" 2
#NO_APP
	movl	%edi, -472(%rbp)
	movl	%edx, -460(%rbp)
	movl	%r10d, 188(%r12)
#APP
# 328 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha256.c" 1
	bswapl %esi
# 0 "" 2
# 330 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha256.c" 1
	bswapl %ecx
# 0 "" 2
#NO_APP
	movl	%esi, -468(%rbp)
	movl	%ecx, -464(%rbp)
	movl	220(%r12), %edx
	movl	%r11d, 220(%r12)
#APP
# 334 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha256.c" 1
	bswapl %edx
# 0 "" 2
#NO_APP
	movb	$-128, -448(%rbp)
	movl	%edx, -456(%rbp)
	movl	252(%r12), %edx
	movl	%eax, -420(%rbp)
#APP
# 336 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha256.c" 1
	bswapl %edx
# 0 "" 2
#NO_APP
	movl	%ebx, 252(%r12)
	movl	%edx, -452(%rbp)
.L114:
	cmpl	$4, -1956(%rbp)
	movl	$1, -1816(%rbp)
	movl	$1, -1800(%rbp)
	movl	$1, -1784(%rbp)
	movl	$1, -1768(%rbp)
	je	.L115
	movl	$1, -1752(%rbp)
	movl	$1, -1736(%rbp)
	movl	$1, -1720(%rbp)
	movl	$1, -1704(%rbp)
.L115:
	movq	-1976(%rbp), %rax
	cmpl	$4, -1956(%rbp)
	movq	%rax, -1824(%rbp)
	leaq	-1248(%rbp), %rax
	movq	%rax, -1808(%rbp)
	leaq	-1120(%rbp), %rax
	movq	%rax, -1792(%rbp)
	leaq	-992(%rbp), %rax
	movq	%rax, -1776(%rbp)
	je	.L116
	leaq	-864(%rbp), %rax
	movq	%rax, -1760(%rbp)
	leaq	-736(%rbp), %rax
	movq	%rax, -1744(%rbp)
	leaq	-608(%rbp), %rax
	movq	%rax, -1728(%rbp)
	leaq	-480(%rbp), %rax
	movq	%rax, -1712(%rbp)
.L116:
	movl	-2000(%rbp), %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r12, %rbx
	xorl	%r14d, %r14d
	call	sha256_multi_block@PLT
	movl	$1, %eax
	subl	-1996(%rbp), %eax
	movq	%r12, -2016(%rbp)
	movl	%eax, -2008(%rbp)
	movq	-2032(%rbp), %r13
	movq	$0, -1992(%rbp)
	movq	-2048(%rbp), %r12
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L160:
	testb	$4, %cl
	jne	.L157
	testl	%ecx, %ecx
	je	.L107
	movb	%dil, 32(%rax)
	testb	$2, %cl
	jne	.L158
.L107:
	movl	-2008(%rbp), %eax
	addl	%esi, %edx
	addq	%r10, %rcx
	addl	$1, %r14d
	addq	$40, %r13
	addq	$4, %rbx
	addl	%edx, %eax
	shrl	$4, %eax
	movl	%eax, -24(%r13)
	movzbl	516(%r15), %eax
	movb	%al, (%r12)
	movzbl	517(%r15), %eax
	movb	%al, 1(%r12)
	movzbl	518(%r15), %eax
	movb	%al, 2(%r12)
	leal	17(%rdx), %eax
	rolw	$8, %ax
	movw	%ax, 3(%r12)
	leal	22(%rdx), %eax
	addq	%rax, -1992(%rbp)
	cmpl	%r14d, -1956(%rbp)
	je	.L159
	movq	%rcx, %r12
.L112:
	cmpl	-1968(%rbp), %r14d
	movl	-1960(%rbp), %ecx
	cmove	-1964(%rbp), %ecx
	movq	8(%r13), %rdi
	movq	0(%r13), %rsi
	movl	%ecx, %edx
	subl	-1996(%rbp), %edx
	movl	%ecx, -1984(%rbp)
	call	memcpy@PLT
	movl	-1984(%rbp), %ecx
	movq	8(%r13), %rax
	movabsq	$72340172838076673, %r11
	movl	(%rbx), %edx
	movq	%rax, 0(%r13)
	leal	21(%rcx), %eax
	leal	32(%rcx), %esi
	addq	%r12, %rax
#APP
# 377 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha256.c" 1
	bswapl %edx
# 0 "" 2
#NO_APP
	movl	%edx, (%rax)
	movl	32(%rbx), %edx
	leaq	32(%rax), %r10
#APP
# 378 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha256.c" 1
	bswapl %edx
# 0 "" 2
#NO_APP
	movl	%edx, 4(%rax)
	movl	64(%rbx), %edx
#APP
# 379 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha256.c" 1
	bswapl %edx
# 0 "" 2
#NO_APP
	movl	%edx, 8(%rax)
	movl	96(%rbx), %edx
#APP
# 380 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha256.c" 1
	bswapl %edx
# 0 "" 2
#NO_APP
	movl	%edx, 12(%rax)
	movl	128(%rbx), %edx
#APP
# 381 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha256.c" 1
	bswapl %edx
# 0 "" 2
#NO_APP
	movl	%edx, 16(%rax)
	movl	160(%rbx), %edx
#APP
# 382 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha256.c" 1
	bswapl %edx
# 0 "" 2
#NO_APP
	movl	%edx, 20(%rax)
	movl	192(%rbx), %edx
#APP
# 383 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha256.c" 1
	bswapl %edx
# 0 "" 2
#NO_APP
	movl	%edx, 24(%rax)
	movl	224(%rbx), %edx
#APP
# 384 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha256.c" 1
	bswapl %edx
# 0 "" 2
#NO_APP
	movl	%edx, 28(%rax)
	movl	%esi, %edx
	notl	%edx
	andl	$15, %edx
	movl	%edx, %ecx
	movzbl	%dl, %edi
	addq	$1, %rcx
	imulq	%r11, %rdi
	cmpl	$8, %ecx
	jb	.L160
	movq	%rdi, 32(%rax)
	addq	$40, %rax
	movq	%r10, %r9
	andq	$-8, %rax
	movq	%rdi, -8(%r10,%rcx)
	subq	%rax, %r9
	addl	%ecx, %r9d
	andl	$-8, %r9d
	cmpl	$8, %r9d
	jb	.L107
	andl	$-8, %r9d
	xorl	%r8d, %r8d
.L110:
	movl	%r8d, %r11d
	addl	$8, %r8d
	movq	%rdi, (%rax,%r11)
	cmpl	%r9d, %r8d
	jb	.L110
	jmp	.L107
.L157:
	movl	%edi, 32(%rax)
	movl	%edi, -4(%r10,%rcx)
	jmp	.L107
.L158:
	movw	%di, -2(%r10,%rcx)
	jmp	.L107
.L87:
	movq	508(%r15), %r10
	movl	-1956(%rbp), %edx
	movq	%r10, -1376(%rbp)
#APP
# 207 "../deps/openssl/openssl/crypto/evp/e_aes_cbc_hmac_sha256.c" 1
	bswapq %r10
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	jne	.L118
	movl	-2000(%rbp), %edx
	leaq	-1824(%rbp), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	sha256_multi_block@PLT
	cmpl	$2111, -2056(%rbp)
	jbe	.L93
	leaq	-1952(%rbp), %rax
	movq	%rax, -2008(%rbp)
	jmp	.L95
.L93:
	movl	-2000(%rbp), %edx
	movq	%r12, %rdi
	leaq	-1952(%rbp), %rsi
	call	sha256_multi_block@PLT
	movq	-1976(%rbp), %rdi
	xorl	%eax, %eax
	movl	$128, %ecx
	rep stosq
	leaq	-1696(%rbp), %rax
	movq	%rax, -2032(%rbp)
.L99:
	movl	-2000(%rbp), %ebx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%ebx, %edx
	call	sha256_multi_block@PLT
	movq	-1976(%rbp), %rdi
	xorl	%eax, %eax
	movl	%ebx, %edx
	movl	$128, %ecx
	movq	%r13, %rsi
	rep stosq
	movq	%r12, %rdi
	call	sha256_multi_block@PLT
	movq	$0, -1992(%rbp)
	jmp	.L113
.L156:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE407:
	.size	tls1_1_multi_block_encrypt, .-tls1_1_multi_block_encrypt
	.p2align 4
	.type	aesni_cbc_hmac_sha256_ctrl, @function
aesni_cbc_hmac_sha256_ctrl:
.LFB409:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	leal	-22(%rbx), %esi
	cmpl	$6, %esi
	ja	.L182
	leaq	.L164(%rip), %rdx
	movq	%rax, %r12
	movslq	(%rdx,%rsi,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L164:
	.long	.L168-.L164
	.long	.L167-.L164
	.long	.L182-.L164
	.long	.L166-.L164
	.long	.L165-.L164
	.long	.L182-.L164
	.long	.L163-.L164
	.text
	.p2align 4,,10
	.p2align 3
.L182:
	movl	$-1, %eax
	.p2align 4,,10
	.p2align 3
.L161:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L190
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L168:
	.cfi_restore_state
	cmpl	$13, %r13d
	jne	.L182
	movzwl	11(%r14), %r13d
	movq	%r15, %rdi
	rolw	$8, %r13w
	movzwl	%r13w, %ebx
	call	EVP_CIPHER_CTX_encrypting@PLT
	testl	%eax, %eax
	je	.L173
	movzwl	%r13w, %eax
	movq	%rax, 584(%r12)
	movzwl	9(%r14), %eax
	rolw	$8, %ax
	movzwl	%ax, %edx
	movl	%edx, 592(%r12)
	cmpw	$769, %ax
	jbe	.L174
	cmpw	$15, %r13w
	jbe	.L178
	subl	$16, %ebx
	movl	%ebx, %eax
	rolw	$8, %ax
	movw	%ax, 11(%r14)
.L174:
	movdqu	244(%r12), %xmm3
	movl	$13, %edx
	movq	%r14, %rsi
	movdqu	260(%r12), %xmm4
	movdqu	276(%r12), %xmm5
	leaq	468(%r12), %rdi
	movdqu	292(%r12), %xmm6
	movdqu	308(%r12), %xmm7
	movups	%xmm3, 468(%r12)
	movdqu	324(%r12), %xmm3
	movups	%xmm4, 484(%r12)
	movdqu	340(%r12), %xmm4
	movups	%xmm5, 500(%r12)
	movups	%xmm6, 516(%r12)
	movups	%xmm7, 532(%r12)
	movups	%xmm3, 548(%r12)
	movups	%xmm4, 564(%r12)
	call	sha256_update
	leal	48(%rbx), %eax
	andl	$-16, %eax
	subl	%ebx, %eax
	jmp	.L161
	.p2align 4,,10
	.p2align 3
.L167:
	pxor	%xmm0, %xmm0
	leaq	-128(%rbp), %r15
	movl	$-1, %eax
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	testl	%r13d, %r13d
	js	.L161
	leaq	244(%r12), %rbx
	movslq	%r13d, %rdx
	cmpl	$64, %r13d
	jle	.L170
	movq	%rbx, %rdi
	movq	%rdx, -136(%rbp)
	call	SHA256_Init@PLT
	movq	-136(%rbp), %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	sha256_update
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	SHA256_Final@PLT
.L171:
	movdqa	.LC1(%rip), %xmm0
	movdqa	-128(%rbp), %xmm1
	movq	%rbx, %rdi
	addq	$356, %r12
	pxor	%xmm0, %xmm1
	movaps	%xmm1, -128(%rbp)
	movdqa	-112(%rbp), %xmm1
	pxor	%xmm0, %xmm1
	movaps	%xmm1, -112(%rbp)
	movdqa	-96(%rbp), %xmm1
	pxor	%xmm0, %xmm1
	pxor	-80(%rbp), %xmm0
	movaps	%xmm1, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	SHA256_Init@PLT
	movl	$64, %edx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	sha256_update
	movdqa	.LC2(%rip), %xmm0
	movdqa	-128(%rbp), %xmm1
	movq	%r12, %rdi
	pxor	%xmm0, %xmm1
	movaps	%xmm1, -128(%rbp)
	movdqa	-112(%rbp), %xmm1
	pxor	%xmm0, %xmm1
	movaps	%xmm1, -112(%rbp)
	movdqa	-96(%rbp), %xmm1
	pxor	%xmm0, %xmm1
	pxor	-80(%rbp), %xmm0
	movaps	%xmm1, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	SHA256_Init@PLT
	movl	$64, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	sha256_update
	movl	$64, %esi
	movq	%r15, %rdi
	call	OPENSSL_cleanse@PLT
	movl	$1, %eax
	jmp	.L161
	.p2align 4,,10
	.p2align 3
.L166:
	cmpl	$31, %r13d
	jle	.L182
	movq	8(%r14), %rax
	movq	%r15, %rdi
	movzwl	11(%rax), %r13d
	rolw	$8, %r13w
	call	EVP_CIPHER_CTX_encrypting@PLT
	testl	%eax, %eax
	je	.L182
	movq	8(%r14), %rsi
	movzwl	9(%rsi), %eax
	rolw	$8, %ax
	cmpw	$769, %ax
	jbe	.L182
	testw	%r13w, %r13w
	je	.L177
	movzwl	%r13w, %ebx
	cmpw	$4095, %r13w
	jbe	.L178
	cmpl	$8191, %ebx
	jbe	.L184
	movl	8+OPENSSL_ia32cap_P(%rip), %eax
	andl	$32, %eax
	cmpl	$1, %eax
	sbbl	%r15d, %r15d
	addl	$3, %r15d
	cmpl	$1, %eax
	sbbl	%r13d, %r13d
	andl	$-4, %r13d
	addl	$8, %r13d
	jmp	.L179
	.p2align 4,,10
	.p2align 3
.L165:
	movl	24(%r14), %r8d
	movq	16(%r14), %rcx
	movq	%r12, %rdi
	movq	8(%r14), %rdx
	movq	(%r14), %rsi
	shrl	$2, %r8d
	call	tls1_1_multi_block_encrypt
	jmp	.L161
	.p2align 4,,10
	.p2align 3
.L163:
	leal	48(%r13), %eax
	andl	$-16, %eax
	leal	21(%rax), %eax
	jmp	.L161
	.p2align 4,,10
	.p2align 3
.L178:
	xorl	%eax, %eax
	jmp	.L161
.L177:
	movl	24(%r14), %eax
	movl	%eax, %r15d
	subl	$4, %eax
	shrl	$2, %r15d
	cmpl	$7, %eax
	ja	.L182
	movl	16(%r14), %ebx
	leal	0(,%r15,4), %r13d
	addl	$1, %r15d
.L179:
	movdqu	244(%r12), %xmm2
	movl	$13, %edx
	movdqu	260(%r12), %xmm3
	leaq	468(%r12), %rdi
	movdqu	276(%r12), %xmm4
	movdqu	292(%r12), %xmm5
	movdqu	308(%r12), %xmm6
	movdqu	324(%r12), %xmm7
	movups	%xmm2, 468(%r12)
	movdqu	340(%r12), %xmm2
	movups	%xmm3, 484(%r12)
	movups	%xmm4, 500(%r12)
	movups	%xmm5, 516(%r12)
	movups	%xmm6, 532(%r12)
	movups	%xmm7, 548(%r12)
	movups	%xmm2, 564(%r12)
	call	sha256_update
	movl	%ebx, %edx
	movl	%r15d, %ecx
	shrl	%cl, %edx
	leal	(%rbx,%rdx), %eax
	movl	%edx, %ebx
	sall	%cl, %ebx
	subl	%ebx, %eax
	cmpl	%eax, %edx
	jnb	.L180
	leal	22(%rax), %ecx
	leal	-1(%r13), %esi
	andl	$63, %ecx
	cmpl	%esi, %ecx
	jnb	.L180
	subl	%r13d, %eax
	addl	$1, %edx
	addl	$1, %eax
.L180:
	addl	$48, %edx
	addl	$48, %eax
	movl	%r15d, %ecx
	movl	%r13d, 24(%r14)
	andl	$-16, %edx
	andl	$-16, %eax
	addl	$21, %edx
	movl	%edx, %ebx
	sall	%cl, %ebx
	leal	21(%rax,%rbx), %eax
	subl	%edx, %eax
	jmp	.L161
.L170:
	movl	$64, %ecx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	__memcpy_chk@PLT
	jmp	.L171
.L184:
	movl	$2, %r15d
	movl	$4, %r13d
	jmp	.L179
.L173:
	movq	(%r14), %rdx
	movl	$32, %eax
	movq	%rdx, 592(%r12)
	movl	8(%r14), %edx
	movl	%edx, 600(%r12)
	movzbl	12(%r14), %edx
	movq	$13, 584(%r12)
	movb	%dl, 604(%r12)
	jmp	.L161
.L190:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE409:
	.size	aesni_cbc_hmac_sha256_ctrl, .-aesni_cbc_hmac_sha256_ctrl
	.p2align 4
	.globl	EVP_aes_128_cbc_hmac_sha256
	.type	EVP_aes_128_cbc_hmac_sha256, @function
EVP_aes_128_cbc_hmac_sha256:
.LFB410:
	.cfi_startproc
	endbr64
	testb	$2, 7+OPENSSL_ia32cap_P(%rip)
	je	.L199
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	xorl	%r9d, %r9d
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$8, %rsp
	pushq	$0
	call	aesni_cbc_sha256_enc@PLT
	movl	%eax, %r8d
	popq	%rax
	leaq	aesni_128_cbc_hmac_sha256_cipher(%rip), %rax
	popq	%rdx
	testl	%r8d, %r8d
	je	.L203
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L203:
	.cfi_restore_state
	leave
	.cfi_def_cfa 7, 8
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L199:
	.cfi_restore 6
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE410:
	.size	EVP_aes_128_cbc_hmac_sha256, .-EVP_aes_128_cbc_hmac_sha256
	.p2align 4
	.globl	EVP_aes_256_cbc_hmac_sha256
	.type	EVP_aes_256_cbc_hmac_sha256, @function
EVP_aes_256_cbc_hmac_sha256:
.LFB411:
	.cfi_startproc
	endbr64
	testb	$2, 7+OPENSSL_ia32cap_P(%rip)
	je	.L212
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	xorl	%r9d, %r9d
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$8, %rsp
	pushq	$0
	call	aesni_cbc_sha256_enc@PLT
	movl	%eax, %r8d
	popq	%rax
	leaq	aesni_256_cbc_hmac_sha256_cipher(%rip), %rax
	popq	%rdx
	testl	%r8d, %r8d
	je	.L216
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L216:
	.cfi_restore_state
	leave
	.cfi_def_cfa 7, 8
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L212:
	.cfi_restore 6
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE411:
	.size	EVP_aes_256_cbc_hmac_sha256, .-EVP_aes_256_cbc_hmac_sha256
	.section	.data.rel.local,"aw"
	.align 32
	.type	aesni_256_cbc_hmac_sha256_cipher, @object
	.size	aesni_256_cbc_hmac_sha256_cipher, 88
aesni_256_cbc_hmac_sha256_cipher:
	.long	950
	.long	16
	.long	32
	.long	16
	.quad	6295554
	.quad	aesni_cbc_hmac_sha256_init_key
	.quad	aesni_cbc_hmac_sha256_cipher
	.quad	0
	.long	608
	.zero	4
	.quad	0
	.quad	0
	.quad	aesni_cbc_hmac_sha256_ctrl
	.quad	0
	.align 32
	.type	aesni_128_cbc_hmac_sha256_cipher, @object
	.size	aesni_128_cbc_hmac_sha256_cipher, 88
aesni_128_cbc_hmac_sha256_cipher:
	.long	948
	.long	16
	.long	16
	.long	16
	.quad	6295554
	.quad	aesni_cbc_hmac_sha256_init_key
	.quad	aesni_cbc_hmac_sha256_cipher
	.quad	0
	.long	608
	.zero	4
	.quad	0
	.quad	0
	.quad	aesni_cbc_hmac_sha256_ctrl
	.quad	0
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.quad	2048
	.quad	2048
	.align 16
.LC1:
	.byte	54
	.byte	54
	.byte	54
	.byte	54
	.byte	54
	.byte	54
	.byte	54
	.byte	54
	.byte	54
	.byte	54
	.byte	54
	.byte	54
	.byte	54
	.byte	54
	.byte	54
	.byte	54
	.align 16
.LC2:
	.byte	106
	.byte	106
	.byte	106
	.byte	106
	.byte	106
	.byte	106
	.byte	106
	.byte	106
	.byte	106
	.byte	106
	.byte	106
	.byte	106
	.byte	106
	.byte	106
	.byte	106
	.byte	106
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
