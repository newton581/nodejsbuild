	.file	"tasn_fre.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/asn1/tasn_fre.c"
	.text
	.p2align 4
	.globl	asn1_primitive_free
	.type	asn1_primitive_free, @function
asn1_primitive_free:
.LFB408:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	testq	%rsi, %rsi
	je	.L2
	movq	32(%rsi), %rax
	testl	%edx, %edx
	je	.L3
	testq	%rax, %rax
	je	.L4
	movq	32(%rax), %rax
	testq	%rax, %rax
	je	.L4
.L51:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L3:
	.cfi_restore_state
	testq	%rax, %rax
	je	.L4
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L51
.L4:
	cmpb	$5, (%rsi)
	je	.L53
	movq	8(%rsi), %rax
	cmpl	$1, %eax
	je	.L9
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1
	cmpl	$5, %eax
	je	.L11
	jg	.L15
	cmpl	$-4, %eax
	jne	.L8
.L13:
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	asn1_primitive_free
	movq	(%r12), %rdi
	movl	$200, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	.p2align 4,,10
	.p2align 3
.L11:
	movq	$0, (%r12)
.L1:
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	.cfi_restore_state
	movq	(%rdi), %rcx
	movq	8(%rcx), %rdi
	leaq	8(%rcx), %r12
	testq	%rdi, %rdi
	je	.L1
	movl	(%rcx), %eax
	cmpl	$5, %eax
	je	.L11
	jg	.L15
	cmpl	$-4, %eax
	je	.L13
	cmpl	$1, %eax
	je	.L54
.L8:
	movl	%edx, %esi
	call	asn1_string_embed_free@PLT
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L15:
	cmpl	$6, %eax
	jne	.L8
	call	ASN1_OBJECT_free@PLT
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L53:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	jne	.L8
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L9:
	movq	40(%rsi), %rax
	movl	%eax, (%r12)
	jmp	.L1
.L54:
	movl	$-1, 8(%rcx)
	jmp	.L1
	.cfi_endproc
.LFE408:
	.size	asn1_primitive_free, .-asn1_primitive_free
	.p2align 4
	.globl	asn1_item_embed_free
	.type	asn1_item_embed_free, @function
asn1_item_embed_free:
.LFB406:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, -92(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L55
	movzbl	(%rsi), %eax
	movq	%rdi, %r12
	movq	%rsi, %r13
	testb	%al, %al
	jne	.L135
	movq	32(%rsi), %rcx
	testq	%rcx, %rcx
	je	.L63
.L104:
	movq	24(%rcx), %rbx
	movq	%rbx, -112(%rbp)
	testq	%rbx, %rbx
	je	.L136
	cmpb	$6, %al
	ja	.L55
	leaq	.L98(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L98:
	.long	.L63-.L98
	.long	.L83-.L98
	.long	.L71-.L98
	.long	.L55-.L98
	.long	.L80-.L98
	.long	.L70-.L98
	.long	.L83-.L98
	.text
.L80:
	movq	16(%rcx), %rax
	testq	%rax, %rax
	je	.L55
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	*%rax
.L55:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L137
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L135:
	.cfi_restore_state
	cmpq	$0, (%rdi)
	je	.L55
	movq	32(%rsi), %rcx
	testq	%rcx, %rcx
	jne	.L104
	cmpb	$6, %al
	ja	.L55
	leaq	.L103(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L103:
	.long	.L55-.L103
	.long	.L81-.L103
	.long	.L95-.L103
	.long	.L55-.L103
	.long	.L55-.L103
	.long	.L70-.L103
	.long	.L81-.L103
	.text
	.p2align 4,,10
	.p2align 3
.L136:
	cmpb	$6, %al
	ja	.L55
	leaq	.L102(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L102:
	.long	.L63-.L102
	.long	.L81-.L102
	.long	.L95-.L102
	.long	.L55-.L102
	.long	.L80-.L102
	.long	.L70-.L102
	.long	.L81-.L102
	.text
.L70:
	movl	-92(%rbp), %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	asn1_primitive_free
	jmp	.L55
.L63:
	movq	16(%r13), %r14
	testq	%r14, %r14
	je	.L70
	movq	(%r14), %rax
	movl	%eax, %r13d
	andl	$4096, %r13d
	jne	.L138
.L65:
	testb	$6, %al
	je	.L66
	xorl	%ebx, %ebx
	leaq	-64(%rbp), %rax
	movq	%r12, -88(%rbp)
	movq	(%r12), %r15
	movl	%ebx, %r12d
	movq	%rax, %rbx
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L68:
	movl	%r12d, %esi
	movq	%r15, %rdi
	addl	$1, %r12d
	call	OPENSSL_sk_value@PLT
	movq	32(%r14), %rsi
	movl	%r13d, %edx
	movq	%rbx, %rdi
	movq	%rax, -64(%rbp)
	call	asn1_item_embed_free
.L67:
	movq	%r15, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r12d
	jl	.L68
	movq	-88(%rbp), %r12
	movq	%r15, %rdi
	call	OPENSSL_sk_free@PLT
	movq	$0, (%r12)
	jmp	.L55
.L71:
	xorl	%ecx, %ecx
	movq	-112(%rbp), %rax
	movq	%r13, %rdx
	movq	%r12, %rsi
	movl	$2, %edi
	call	*%rax
	cmpl	$2, %eax
	je	.L55
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	asn1_get_choice_selector@PLT
	testl	%eax, %eax
	js	.L78
	cltq
	cmpq	24(%r13), %rax
	jl	.L101
.L78:
	movq	-112(%rbp), %rax
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movq	%r12, %rsi
	movl	$3, %edi
	call	*%rax
.L79:
	movl	-92(%rbp), %edx
	testl	%edx, %edx
	jne	.L55
	movq	(%r12), %rdi
	movl	$75, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, (%r12)
	jmp	.L55
.L81:
	movq	%r13, %rdx
	movl	$-1, %esi
	movq	%r12, %rdi
	call	asn1_do_lock@PLT
	testl	%eax, %eax
	jne	.L55
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	asn1_enc_free@PLT
	movq	24(%r13), %rax
	movq	16(%r13), %rdx
	movq	$0, -112(%rbp)
	leaq	(%rax,%rax,4), %rcx
	leaq	(%rdx,%rcx,8), %rcx
	testq	%rax, %rax
	jle	.L93
.L99:
	leaq	-64(%rbp), %rax
	movq	$0, -88(%rbp)
	movq	%rax, -144(%rbp)
	movq	%r12, -120(%rbp)
	movq	%r13, -128(%rbp)
	movq	%rcx, %r13
	.p2align 4,,10
	.p2align 3
.L91:
	movq	-120(%rbp), %r14
	subq	$40, %r13
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	asn1_do_adb@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L85
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	asn1_get_field_ptr@PLT
	movq	%rax, -104(%rbp)
	movq	%rax, %rcx
	movq	(%rbx), %rax
	movl	%eax, %r14d
	andl	$4096, %r14d
	je	.L86
	movq	%rcx, -72(%rbp)
	leaq	-72(%rbp), %rcx
	movq	%rcx, -104(%rbp)
.L86:
	testb	$6, %al
	je	.L87
	movq	-104(%rbp), %rax
	xorl	%r12d, %r12d
	movq	%r13, -136(%rbp)
	movl	%r12d, %r13d
	movq	-144(%rbp), %r12
	movq	(%rax), %r15
	jmp	.L88
	.p2align 4,,10
	.p2align 3
.L89:
	movl	%r13d, %esi
	movq	%r15, %rdi
	addl	$1, %r13d
	call	OPENSSL_sk_value@PLT
	movq	32(%rbx), %rsi
	movl	%r14d, %edx
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	call	asn1_item_embed_free
.L88:
	movq	%r15, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r13d
	jl	.L89
	movq	%r15, %rdi
	movq	-136(%rbp), %r13
	call	OPENSSL_sk_free@PLT
	movq	-104(%rbp), %rax
	movq	$0, (%rax)
.L85:
	movq	-128(%rbp), %rcx
	addq	$1, -88(%rbp)
	movq	-88(%rbp), %rax
	cmpq	%rax, 24(%rcx)
	jg	.L91
	cmpq	$0, -112(%rbp)
	movq	-120(%rbp), %r12
	movq	%rcx, %r13
	je	.L93
.L92:
	movq	-112(%rbp), %rax
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movq	%r12, %rsi
	movl	$3, %edi
	call	*%rax
.L93:
	movl	-92(%rbp), %eax
	testl	%eax, %eax
	jne	.L55
	movq	(%r12), %rdi
	movl	$115, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, (%r12)
	jmp	.L55
.L83:
	movq	%r13, %rdx
	movl	$-1, %esi
	movq	%r12, %rdi
	call	asn1_do_lock@PLT
	testl	%eax, %eax
	jne	.L55
	xorl	%ecx, %ecx
	movq	-112(%rbp), %rax
	movq	%r13, %rdx
	movq	%r12, %rsi
	movl	$2, %edi
	call	*%rax
	cmpl	$2, %eax
	je	.L55
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	asn1_enc_free@PLT
	movq	24(%r13), %rax
	movq	16(%r13), %rdx
	leaq	(%rax,%rax,4), %rcx
	leaq	(%rdx,%rcx,8), %rcx
	testq	%rax, %rax
	jg	.L99
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L87:
	movq	32(%rbx), %rsi
	movq	-104(%rbp), %rdi
	movl	%r14d, %edx
	call	asn1_item_embed_free
	jmp	.L85
.L95:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	asn1_get_choice_selector@PLT
	testl	%eax, %eax
	js	.L79
	cltq
	cmpq	%rax, 24(%r13)
	jle	.L79
	movq	$0, -112(%rbp)
.L101:
	leaq	(%rax,%rax,4), %rdx
	movq	16(%r13), %rax
	movq	%r12, %rdi
	leaq	(%rax,%rdx,8), %rbx
	movq	%rbx, %rsi
	call	asn1_get_field_ptr@PLT
	movq	%rax, -88(%rbp)
	movq	%rax, %rdi
	movq	(%rbx), %rax
	movl	%eax, %ecx
	andl	$4096, %ecx
	movl	%ecx, %edx
	je	.L73
	leaq	-72(%rbp), %rcx
	movq	%rdi, -72(%rbp)
	movq	%rcx, -88(%rbp)
.L73:
	testb	$6, %al
	je	.L74
	movq	-88(%rbp), %rax
	xorl	%r14d, %r14d
	movq	%r12, -104(%rbp)
	movl	%edx, %r12d
	movq	%r13, -120(%rbp)
	movl	%r14d, %r13d
	movq	(%rax), %r15
	leaq	-64(%rbp), %rax
	movq	%rax, %r14
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L76:
	movl	%r13d, %esi
	movq	%r15, %rdi
	addl	$1, %r13d
	call	OPENSSL_sk_value@PLT
	movq	32(%rbx), %rsi
	movl	%r12d, %edx
	movq	%r14, %rdi
	movq	%rax, -64(%rbp)
	call	asn1_item_embed_free
.L75:
	movq	%r15, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r13d
	jl	.L76
	movq	%r15, %rdi
	movq	-104(%rbp), %r12
	movq	-120(%rbp), %r13
	call	OPENSSL_sk_free@PLT
	movq	-88(%rbp), %rax
	movq	$0, (%rax)
.L77:
	cmpq	$0, -112(%rbp)
	je	.L79
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L138:
	movq	%r12, -72(%rbp)
	leaq	-72(%rbp), %r12
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L66:
	movq	32(%r14), %rsi
	movl	%r13d, %edx
	movq	%r12, %rdi
	call	asn1_item_embed_free
	jmp	.L55
.L74:
	movq	32(%rbx), %rsi
	movq	-88(%rbp), %rdi
	call	asn1_item_embed_free
	jmp	.L77
.L137:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE406:
	.size	asn1_item_embed_free, .-asn1_item_embed_free
	.p2align 4
	.globl	ASN1_item_free
	.type	ASN1_item_free, @function
ASN1_item_free:
.LFB404:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	leaq	-8(%rbp), %rdi
	call	asn1_item_embed_free
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE404:
	.size	ASN1_item_free, .-ASN1_item_free
	.p2align 4
	.globl	ASN1_item_ex_free
	.type	ASN1_item_ex_free, @function
ASN1_item_ex_free:
.LFB405:
	.cfi_startproc
	endbr64
	xorl	%edx, %edx
	jmp	asn1_item_embed_free
	.cfi_endproc
.LFE405:
	.size	ASN1_item_ex_free, .-ASN1_item_ex_free
	.p2align 4
	.globl	asn1_template_free
	.type	asn1_template_free, @function
asn1_template_free:
.LFB407:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%rdi, -88(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movl	%eax, %r13d
	andl	$4096, %r13d
	je	.L143
	leaq	-72(%rbp), %rcx
	movq	%rdi, -72(%rbp)
	movq	%rcx, -88(%rbp)
.L143:
	testb	$6, %al
	je	.L144
	movq	-88(%rbp), %rax
	xorl	%ebx, %ebx
	leaq	-64(%rbp), %r14
	movq	(%rax), %r15
	jmp	.L145
	.p2align 4,,10
	.p2align 3
.L146:
	movl	%ebx, %esi
	movq	%r15, %rdi
	addl	$1, %ebx
	call	OPENSSL_sk_value@PLT
	movq	32(%r12), %rsi
	movl	%r13d, %edx
	movq	%r14, %rdi
	movq	%rax, -64(%rbp)
	call	asn1_item_embed_free
.L145:
	movq	%r15, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jl	.L146
	movq	%r15, %rdi
	call	OPENSSL_sk_free@PLT
	movq	-88(%rbp), %rax
	movq	$0, (%rax)
.L142:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L153
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L144:
	.cfi_restore_state
	movq	32(%r12), %rsi
	movq	-88(%rbp), %rdi
	movl	%r13d, %edx
	call	asn1_item_embed_free
	jmp	.L142
.L153:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE407:
	.size	asn1_template_free, .-asn1_template_free
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
