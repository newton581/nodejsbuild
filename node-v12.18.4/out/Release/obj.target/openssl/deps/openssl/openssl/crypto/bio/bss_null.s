	.file	"bss_null.c"
	.text
	.p2align 4
	.type	null_read, @function
null_read:
.LFB268:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE268:
	.size	null_read, .-null_read
	.p2align 4
	.type	null_write, @function
null_write:
.LFB269:
	.cfi_startproc
	endbr64
	movl	%edx, %eax
	ret
	.cfi_endproc
.LFE269:
	.size	null_write, .-null_write
	.p2align 4
	.type	null_ctrl, @function
null_ctrl:
.LFB270:
	.cfi_startproc
	endbr64
	movl	%esi, %ecx
	cmpl	$12, %esi
	ja	.L6
	movl	$1, %eax
	salq	%cl, %rax
	testl	$6678, %eax
	setne	%al
	movzbl	%al, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE270:
	.size	null_ctrl, .-null_ctrl
	.p2align 4
	.type	null_puts, @function
null_puts:
.LFB272:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L9
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	strlen@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	.cfi_restore 6
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE272:
	.size	null_puts, .-null_puts
	.p2align 4
	.type	null_gets, @function
null_gets:
.LFB274:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE274:
	.size	null_gets, .-null_gets
	.p2align 4
	.globl	BIO_s_null
	.type	BIO_s_null, @function
BIO_s_null:
.LFB267:
	.cfi_startproc
	endbr64
	leaq	null_method(%rip), %rax
	ret
	.cfi_endproc
.LFE267:
	.size	BIO_s_null, .-BIO_s_null
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"NULL"
	.section	.data.rel.ro,"aw"
	.align 32
	.type	null_method, @object
	.size	null_method, 96
null_method:
	.long	1030
	.zero	4
	.quad	.LC0
	.quad	bwrite_conv
	.quad	null_write
	.quad	bread_conv
	.quad	null_read
	.quad	null_puts
	.quad	null_gets
	.quad	null_ctrl
	.quad	0
	.quad	0
	.quad	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
