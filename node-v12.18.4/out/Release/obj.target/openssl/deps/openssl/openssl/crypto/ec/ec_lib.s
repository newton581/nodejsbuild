	.file	"ec_lib.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/ec/ec_lib.c"
	.text
	.p2align 4
	.globl	EC_GROUP_new
	.type	EC_GROUP_new, @function
EC_GROUP_new:
.LFB388:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	testq	%rdi, %rdi
	je	.L14
	cmpq	$0, 8(%rdi)
	movq	%rdi, %rbx
	je	.L15
	movl	$33, %edx
	leaq	.LC0(%rip), %rsi
	movl	$168, %edi
	call	CRYPTO_zalloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L16
	movq	%rbx, (%rax)
	testb	$2, (%rbx)
	je	.L17
.L6:
	movabsq	$17179869185, %rax
	movq	%r12, %rdi
	movq	%rax, 36(%r12)
	call	*8(%rbx)
	testl	%eax, %eax
	je	.L12
.L1:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	.cfi_restore_state
	call	BN_new@PLT
	movq	%rax, 16(%r12)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L7
	call	BN_new@PLT
	movq	%rax, 24(%r12)
	testq	%rax, %rax
	jne	.L6
	.p2align 4,,10
	.p2align 3
.L12:
	movq	16(%r12), %rdi
.L7:
	call	BN_free@PLT
	movq	24(%r12), %rdi
	call	BN_free@PLT
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	movl	$57, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	.cfi_restore_state
	movl	$29, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$66, %edx
	xorl	%r12d, %r12d
	movl	$108, %esi
	movl	$16, %edi
	call	ERR_put_error@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L14:
	movl	$25, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$108, %edx
	xorl	%r12d, %r12d
	movl	$108, %esi
	movl	$16, %edi
	call	ERR_put_error@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L16:
	movl	$35, %r8d
	movl	$65, %edx
	movl	$108, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L1
	.cfi_endproc
.LFE388:
	.size	EC_GROUP_new, .-EC_GROUP_new
	.p2align 4
	.globl	EC_pre_comp_free
	.type	EC_pre_comp_free, @function
EC_pre_comp_free:
.LFB389:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	152(%rdi), %eax
	cmpl	$4, %eax
	je	.L19
	cmpl	$5, %eax
	jne	.L21
	movq	160(%rdi), %rdi
	call	EC_ec_pre_comp_free@PLT
.L21:
	movq	$0, 160(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	.cfi_restore_state
	movq	160(%rdi), %rdi
	call	EC_nistz256_pre_comp_free@PLT
	jmp	.L21
	.cfi_endproc
.LFE389:
	.size	EC_pre_comp_free, .-EC_pre_comp_free
	.p2align 4
	.globl	EC_GROUP_free
	.type	EC_GROUP_free, @function
EC_GROUP_free:
.LFB390:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L23
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	(%rdi), %rax
	movq	%rdi, %r12
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L25
	call	*%rax
.L25:
	movl	152(%r12), %eax
	cmpl	$4, %eax
	je	.L26
	cmpl	$5, %eax
	je	.L27
.L28:
	movq	$0, 160(%r12)
	movq	144(%r12), %rdi
	call	BN_MONT_CTX_free@PLT
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L29
	movq	0(%r13), %rax
	movq	88(%rax), %rax
	testq	%rax, %rax
	je	.L30
	movq	%r13, %rdi
	call	*%rax
.L30:
	movl	$686, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r13, %rdi
	call	CRYPTO_free@PLT
.L29:
	movq	16(%r12), %rdi
	call	BN_free@PLT
	movq	24(%r12), %rdi
	call	BN_free@PLT
	movq	48(%r12), %rdi
	movl	$107, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%r12, %rdi
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	movl	$108, %edx
	leaq	.LC0(%rip), %rsi
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_free@PLT
	.p2align 4,,10
	.p2align 3
.L27:
	.cfi_restore_state
	movq	160(%r12), %rdi
	call	EC_ec_pre_comp_free@PLT
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L26:
	movq	160(%r12), %rdi
	call	EC_nistz256_pre_comp_free@PLT
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L23:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.cfi_endproc
.LFE390:
	.size	EC_GROUP_free, .-EC_GROUP_free
	.p2align 4
	.globl	EC_GROUP_clear_free
	.type	EC_GROUP_clear_free, @function
EC_GROUP_clear_free:
.LFB391:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L43
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	(%rdi), %rdx
	movq	%rdi, %r12
	movq	24(%rdx), %rax
	testq	%rax, %rax
	je	.L45
.L65:
	movq	%r12, %rdi
	call	*%rax
.L46:
	movl	152(%r12), %eax
	cmpl	$4, %eax
	je	.L47
	cmpl	$5, %eax
	je	.L48
.L49:
	movq	$0, 160(%r12)
	movq	144(%r12), %rdi
	call	BN_MONT_CTX_free@PLT
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L50
	movq	0(%r13), %rdx
	movq	96(%rdx), %rax
	testq	%rax, %rax
	je	.L51
.L66:
	movq	%r13, %rdi
	call	*%rax
.L52:
	movl	$698, %ecx
	leaq	.LC0(%rip), %rdx
	movl	$48, %esi
	movq	%r13, %rdi
	call	CRYPTO_clear_free@PLT
.L50:
	movq	16(%r12), %rdi
	call	BN_clear_free@PLT
	movq	24(%r12), %rdi
	call	BN_clear_free@PLT
	movq	56(%r12), %rsi
	movq	48(%r12), %rdi
	movl	$126, %ecx
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_clear_free@PLT
	movq	%r12, %rdi
	movl	$127, %ecx
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	leaq	.LC0(%rip), %rdx
	popq	%r13
	.cfi_restore 13
	movl	$168, %esi
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_clear_free@PLT
	.p2align 4,,10
	.p2align 3
.L48:
	.cfi_restore_state
	movq	160(%r12), %rdi
	call	EC_ec_pre_comp_free@PLT
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L51:
	movq	88(%rdx), %rax
	testq	%rax, %rax
	jne	.L66
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L47:
	movq	160(%r12), %rdi
	call	EC_nistz256_pre_comp_free@PLT
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L45:
	movq	16(%rdx), %rax
	testq	%rax, %rax
	jne	.L65
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L43:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.cfi_endproc
.LFE391:
	.size	EC_GROUP_clear_free, .-EC_GROUP_clear_free
	.p2align 4
	.globl	EC_GROUP_method_of
	.type	EC_GROUP_method_of, @function
EC_GROUP_method_of:
.LFB394:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE394:
	.size	EC_GROUP_method_of, .-EC_GROUP_method_of
	.p2align 4
	.globl	EC_METHOD_get_field_type
	.type	EC_METHOD_get_field_type, @function
EC_METHOD_get_field_type:
.LFB395:
	.cfi_startproc
	endbr64
	movl	4(%rdi), %eax
	ret
	.cfi_endproc
.LFE395:
	.size	EC_METHOD_get_field_type, .-EC_METHOD_get_field_type
	.p2align 4
	.globl	EC_GROUP_get0_generator
	.type	EC_GROUP_get0_generator, @function
EC_GROUP_get0_generator:
.LFB398:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	ret
	.cfi_endproc
.LFE398:
	.size	EC_GROUP_get0_generator, .-EC_GROUP_get0_generator
	.p2align 4
	.globl	EC_GROUP_get_mont_data
	.type	EC_GROUP_get_mont_data, @function
EC_GROUP_get_mont_data:
.LFB399:
	.cfi_startproc
	endbr64
	movq	144(%rdi), %rax
	ret
	.cfi_endproc
.LFE399:
	.size	EC_GROUP_get_mont_data, .-EC_GROUP_get_mont_data
	.p2align 4
	.globl	EC_GROUP_get_order
	.type	EC_GROUP_get_order, @function
EC_GROUP_get_order:
.LFB400:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	subq	$8, %rsp
	movq	16(%rdi), %rsi
	testq	%rsi, %rsi
	je	.L74
	movq	%r12, %rdi
	call	BN_copy@PLT
	testq	%rax, %rax
	je	.L74
	movq	%r12, %rdi
	call	BN_is_zero@PLT
	testl	%eax, %eax
	sete	%al
	addq	$8, %rsp
	movzbl	%al, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L74:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE400:
	.size	EC_GROUP_get_order, .-EC_GROUP_get_order
	.p2align 4
	.globl	EC_GROUP_get0_order
	.type	EC_GROUP_get0_order, @function
EC_GROUP_get0_order:
.LFB401:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	ret
	.cfi_endproc
.LFE401:
	.size	EC_GROUP_get0_order, .-EC_GROUP_get0_order
	.p2align 4
	.globl	EC_GROUP_order_bits
	.type	EC_GROUP_order_bits, @function
EC_GROUP_order_bits:
.LFB402:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	jmp	*64(%rax)
	.cfi_endproc
.LFE402:
	.size	EC_GROUP_order_bits, .-EC_GROUP_order_bits
	.p2align 4
	.globl	EC_GROUP_get_cofactor
	.type	EC_GROUP_get_cofactor, @function
EC_GROUP_get_cofactor:
.LFB403:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$8, %rsp
	movq	24(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L84
	call	BN_copy@PLT
	testq	%rax, %rax
	je	.L84
	movq	24(%rbx), %rdi
	call	BN_is_zero@PLT
	testl	%eax, %eax
	sete	%al
	addq	$8, %rsp
	movzbl	%al, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L84:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE403:
	.size	EC_GROUP_get_cofactor, .-EC_GROUP_get_cofactor
	.p2align 4
	.globl	EC_GROUP_get0_cofactor
	.type	EC_GROUP_get0_cofactor, @function
EC_GROUP_get0_cofactor:
.LFB404:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	ret
	.cfi_endproc
.LFE404:
	.size	EC_GROUP_get0_cofactor, .-EC_GROUP_get0_cofactor
	.p2align 4
	.globl	EC_GROUP_set_curve_name
	.type	EC_GROUP_set_curve_name, @function
EC_GROUP_set_curve_name:
.LFB405:
	.cfi_startproc
	endbr64
	movl	%esi, 32(%rdi)
	ret
	.cfi_endproc
.LFE405:
	.size	EC_GROUP_set_curve_name, .-EC_GROUP_set_curve_name
	.p2align 4
	.globl	EC_GROUP_get_curve_name
	.type	EC_GROUP_get_curve_name, @function
EC_GROUP_get_curve_name:
.LFB406:
	.cfi_startproc
	endbr64
	movl	32(%rdi), %eax
	ret
	.cfi_endproc
.LFE406:
	.size	EC_GROUP_get_curve_name, .-EC_GROUP_get_curve_name
	.p2align 4
	.globl	EC_GROUP_set_asn1_flag
	.type	EC_GROUP_set_asn1_flag, @function
EC_GROUP_set_asn1_flag:
.LFB407:
	.cfi_startproc
	endbr64
	movl	%esi, 36(%rdi)
	ret
	.cfi_endproc
.LFE407:
	.size	EC_GROUP_set_asn1_flag, .-EC_GROUP_set_asn1_flag
	.p2align 4
	.globl	EC_GROUP_get_asn1_flag
	.type	EC_GROUP_get_asn1_flag, @function
EC_GROUP_get_asn1_flag:
.LFB408:
	.cfi_startproc
	endbr64
	movl	36(%rdi), %eax
	ret
	.cfi_endproc
.LFE408:
	.size	EC_GROUP_get_asn1_flag, .-EC_GROUP_get_asn1_flag
	.p2align 4
	.globl	EC_GROUP_set_point_conversion_form
	.type	EC_GROUP_set_point_conversion_form, @function
EC_GROUP_set_point_conversion_form:
.LFB409:
	.cfi_startproc
	endbr64
	movl	%esi, 40(%rdi)
	ret
	.cfi_endproc
.LFE409:
	.size	EC_GROUP_set_point_conversion_form, .-EC_GROUP_set_point_conversion_form
	.p2align 4
	.globl	EC_GROUP_get_point_conversion_form
	.type	EC_GROUP_get_point_conversion_form, @function
EC_GROUP_get_point_conversion_form:
.LFB410:
	.cfi_startproc
	endbr64
	movl	40(%rdi), %eax
	ret
	.cfi_endproc
.LFE410:
	.size	EC_GROUP_get_point_conversion_form, .-EC_GROUP_get_point_conversion_form
	.p2align 4
	.globl	EC_GROUP_set_seed
	.type	EC_GROUP_set_seed, @function
EC_GROUP_set_seed:
.LFB411:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	leaq	.LC0(%rip), %rsi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdx, %rbx
	movl	$480, %edx
	subq	$8, %rsp
	movq	48(%rdi), %rdi
	call	CRYPTO_free@PLT
	movq	$0, 48(%r12)
	movq	$0, 56(%r12)
	testq	%rbx, %rbx
	je	.L99
	testq	%r13, %r13
	je	.L99
	movq	%rbx, %rdi
	movl	$487, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_malloc@PLT
	movq	%rax, 48(%r12)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L101
	movq	%rbx, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	%rbx, 56(%r12)
	addq	$8, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L99:
	.cfi_restore_state
	movl	$1, %eax
.L96:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L101:
	.cfi_restore_state
	movl	$488, %r8d
	movl	$65, %edx
	movl	$286, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L96
	.cfi_endproc
.LFE411:
	.size	EC_GROUP_set_seed, .-EC_GROUP_set_seed
	.p2align 4
	.globl	EC_GROUP_get0_seed
	.type	EC_GROUP_get0_seed, @function
EC_GROUP_get0_seed:
.LFB412:
	.cfi_startproc
	endbr64
	movq	48(%rdi), %rax
	ret
	.cfi_endproc
.LFE412:
	.size	EC_GROUP_get0_seed, .-EC_GROUP_get0_seed
	.p2align 4
	.globl	EC_GROUP_get_seed_len
	.type	EC_GROUP_get_seed_len, @function
EC_GROUP_get_seed_len:
.LFB413:
	.cfi_startproc
	endbr64
	movq	56(%rdi), %rax
	ret
	.cfi_endproc
.LFE413:
	.size	EC_GROUP_get_seed_len, .-EC_GROUP_get_seed_len
	.p2align 4
	.globl	EC_GROUP_set_curve
	.type	EC_GROUP_set_curve, @function
EC_GROUP_set_curve:
.LFB414:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	40(%rax), %rax
	testq	%rax, %rax
	je	.L108
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L108:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$66, %edx
	movl	$292, %esi
	movl	$16, %edi
	movl	$511, %r8d
	leaq	.LC0(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE414:
	.size	EC_GROUP_set_curve, .-EC_GROUP_set_curve
	.p2align 4
	.globl	EC_GROUP_get_curve
	.type	EC_GROUP_get_curve, @function
EC_GROUP_get_curve:
.LFB415:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	testq	%rax, %rax
	je	.L115
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L115:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$66, %edx
	movl	$291, %esi
	movl	$16, %edi
	movl	$521, %r8d
	leaq	.LC0(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE415:
	.size	EC_GROUP_get_curve, .-EC_GROUP_get_curve
	.p2align 4
	.globl	EC_GROUP_set_curve_GFp
	.type	EC_GROUP_set_curve_GFp, @function
EC_GROUP_set_curve_GFp:
.LFB416:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	40(%rax), %rax
	testq	%rax, %rax
	je	.L122
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L122:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$66, %edx
	movl	$292, %esi
	movl	$16, %edi
	movl	$511, %r8d
	leaq	.LC0(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE416:
	.size	EC_GROUP_set_curve_GFp, .-EC_GROUP_set_curve_GFp
	.p2align 4
	.globl	EC_GROUP_get_curve_GFp
	.type	EC_GROUP_get_curve_GFp, @function
EC_GROUP_get_curve_GFp:
.LFB417:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	testq	%rax, %rax
	je	.L129
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L129:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$66, %edx
	movl	$291, %esi
	movl	$16, %edi
	movl	$521, %r8d
	leaq	.LC0(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE417:
	.size	EC_GROUP_get_curve_GFp, .-EC_GROUP_get_curve_GFp
	.p2align 4
	.globl	EC_GROUP_set_curve_GF2m
	.type	EC_GROUP_set_curve_GF2m, @function
EC_GROUP_set_curve_GF2m:
.LFB469:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	40(%rax), %rax
	testq	%rax, %rax
	je	.L136
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L136:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$66, %edx
	movl	$292, %esi
	movl	$16, %edi
	movl	$511, %r8d
	leaq	.LC0(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE469:
	.size	EC_GROUP_set_curve_GF2m, .-EC_GROUP_set_curve_GF2m
	.p2align 4
	.globl	EC_GROUP_get_curve_GF2m
	.type	EC_GROUP_get_curve_GF2m, @function
EC_GROUP_get_curve_GF2m:
.LFB471:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	testq	%rax, %rax
	je	.L143
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L143:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$66, %edx
	movl	$291, %esi
	movl	$16, %edi
	movl	$521, %r8d
	leaq	.LC0(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE471:
	.size	EC_GROUP_get_curve_GF2m, .-EC_GROUP_get_curve_GF2m
	.p2align 4
	.globl	EC_GROUP_get_degree
	.type	EC_GROUP_get_degree, @function
EC_GROUP_get_degree:
.LFB420:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	56(%rax), %rax
	testq	%rax, %rax
	je	.L150
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L150:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$66, %edx
	movl	$173, %esi
	movl	$16, %edi
	movl	$558, %r8d
	leaq	.LC0(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE420:
	.size	EC_GROUP_get_degree, .-EC_GROUP_get_degree
	.p2align 4
	.globl	EC_GROUP_check_discriminant
	.type	EC_GROUP_check_discriminant, @function
EC_GROUP_check_discriminant:
.LFB421:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	72(%rax), %rax
	testq	%rax, %rax
	je	.L157
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L157:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$66, %edx
	movl	$171, %esi
	movl	$16, %edi
	movl	$567, %r8d
	leaq	.LC0(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE421:
	.size	EC_GROUP_check_discriminant, .-EC_GROUP_check_discriminant
	.p2align 4
	.globl	EC_POINT_new
	.type	EC_POINT_new, @function
EC_POINT_new:
.LFB423:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	testq	%rdi, %rdi
	je	.L166
	movq	(%rdi), %rax
	movq	%rdi, %rbx
	cmpq	$0, 80(%rax)
	je	.L167
	movl	$662, %edx
	leaq	.LC0(%rip), %rsi
	movl	$48, %edi
	call	CRYPTO_zalloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L168
	movq	(%rbx), %rax
	movl	32(%rbx), %edx
	movq	%r12, %rdi
	movq	%rax, (%r12)
	movl	%edx, 8(%r12)
	call	*80(%rax)
	testl	%eax, %eax
	je	.L169
.L160:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L167:
	.cfi_restore_state
	movl	$658, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$66, %edx
	xorl	%r12d, %r12d
	movl	$121, %esi
	movl	$16, %edi
	call	ERR_put_error@PLT
	jmp	.L160
	.p2align 4,,10
	.p2align 3
.L169:
	movq	%r12, %rdi
	movl	$672, %edx
	leaq	.LC0(%rip), %rsi
	xorl	%r12d, %r12d
	call	CRYPTO_free@PLT
	jmp	.L160
	.p2align 4,,10
	.p2align 3
.L166:
	movl	$654, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$67, %edx
	xorl	%r12d, %r12d
	movl	$121, %esi
	movl	$16, %edi
	call	ERR_put_error@PLT
	jmp	.L160
	.p2align 4,,10
	.p2align 3
.L168:
	movl	$664, %r8d
	movl	$65, %edx
	movl	$121, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L160
	.cfi_endproc
.LFE423:
	.size	EC_POINT_new, .-EC_POINT_new
	.p2align 4
	.globl	EC_GROUP_copy
	.type	EC_GROUP_copy, @function
EC_GROUP_copy:
.LFB392:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	(%rdi), %rax
	cmpq	$0, 32(%rax)
	je	.L231
	movq	%rsi, %r12
	cmpq	(%rsi), %rax
	jne	.L232
	movq	%rdi, %r14
	cmpq	%rsi, %rdi
	je	.L199
	movl	32(%rsi), %eax
	movl	%eax, 32(%rdi)
	movl	152(%rsi), %eax
	movl	%eax, 152(%rdi)
	movl	152(%rsi), %eax
	cmpl	$4, %eax
	je	.L174
	cmpl	$5, %eax
	je	.L175
	testl	%eax, %eax
	je	.L233
.L176:
	movq	144(%r12), %rsi
	movq	144(%r14), %rdi
	testq	%rsi, %rsi
	je	.L177
	testq	%rdi, %rdi
	je	.L234
.L178:
	call	BN_MONT_CTX_copy@PLT
	testq	%rax, %rax
	je	.L230
.L181:
	movq	8(%r12), %rsi
	movq	8(%r14), %r13
	testq	%rsi, %rsi
	je	.L182
	testq	%r13, %r13
	je	.L235
.L183:
	movq	0(%r13), %rax
	movq	104(%rax), %rdx
	testq	%rdx, %rdx
	je	.L236
	cmpq	(%rsi), %rax
	jne	.L185
	movl	8(%r13), %eax
	movl	8(%rsi), %ecx
	testl	%eax, %eax
	setne	%dil
	cmpl	%ecx, %eax
	setne	%al
	testb	%al, %dil
	je	.L186
	testl	%ecx, %ecx
	jne	.L185
.L186:
	cmpq	%rsi, %r13
	je	.L188
	movq	%r13, %rdi
	call	*%rdx
	testl	%eax, %eax
	je	.L230
.L188:
	movq	(%r12), %rax
	testb	$2, (%rax)
	je	.L193
.L196:
	movl	36(%r12), %eax
	cmpq	$0, 48(%r12)
	movq	48(%r14), %rdi
	movl	%eax, 36(%r14)
	movl	40(%r12), %eax
	movl	%eax, 40(%r14)
	je	.L237
	movl	$216, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	56(%r12), %rdi
	movl	$217, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_malloc@PLT
	movq	%rax, 48(%r14)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L238
	movq	48(%r12), %rsi
	movq	56(%r12), %rdx
	call	memcpy@PLT
	movq	56(%r12), %rax
	movq	%rax, 56(%r14)
.L198:
	movq	(%r14), %rax
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	32(%rax), %rax
	addq	$8, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L232:
	.cfi_restore_state
	movl	$137, %r8d
	movl	$101, %edx
	movl	$106, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
.L170:
	addq	$8, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L185:
	.cfi_restore_state
	movl	$711, %r8d
	movl	$101, %edx
	movl	$114, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L230:
	xorl	%eax, %eax
.L239:
	addq	$8, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L199:
	.cfi_restore_state
	addq	$8, %rsp
	movl	$1, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L233:
	.cfi_restore_state
	movq	$0, 160(%rdi)
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L175:
	movq	160(%rsi), %rdi
	call	EC_ec_pre_comp_dup@PLT
	movq	%rax, 160(%r14)
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L174:
	movq	160(%rsi), %rdi
	call	EC_nistz256_pre_comp_dup@PLT
	movq	%rax, 160(%r14)
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L234:
	call	BN_MONT_CTX_new@PLT
	movq	%rax, 144(%r14)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L230
	movq	144(%r12), %rsi
	jmp	.L178
	.p2align 4,,10
	.p2align 3
.L231:
	movl	$133, %r8d
	movl	$66, %edx
	movl	$106, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L170
	.p2align 4,,10
	.p2align 3
.L235:
	movq	%r14, %rdi
	call	EC_POINT_new
	movq	%rax, 8(%r14)
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L230
	movq	8(%r12), %rsi
	jmp	.L183
	.p2align 4,,10
	.p2align 3
.L193:
	movq	16(%r12), %rsi
	movq	16(%r14), %rdi
	call	BN_copy@PLT
	testq	%rax, %rax
	je	.L230
	movq	24(%r12), %rsi
	movq	24(%r14), %rdi
	call	BN_copy@PLT
	testq	%rax, %rax
	jne	.L196
	xorl	%eax, %eax
	jmp	.L239
	.p2align 4,,10
	.p2align 3
.L177:
	call	BN_MONT_CTX_free@PLT
	movq	$0, 144(%r14)
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L237:
	movl	$225, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 48(%r14)
	movq	$0, 56(%r14)
	jmp	.L198
	.p2align 4,,10
	.p2align 3
.L182:
	testq	%r13, %r13
	je	.L190
	movq	0(%r13), %rdx
	movq	96(%rdx), %rax
	testq	%rax, %rax
	je	.L191
.L229:
	movq	%r13, %rdi
	call	*%rax
.L192:
	movl	$698, %ecx
	leaq	.LC0(%rip), %rdx
	movl	$48, %esi
	movq	%r13, %rdi
	call	CRYPTO_clear_free@PLT
.L190:
	movq	$0, 8(%r14)
	jmp	.L188
	.p2align 4,,10
	.p2align 3
.L191:
	movq	88(%rdx), %rax
	testq	%rax, %rax
	jne	.L229
	jmp	.L192
	.p2align 4,,10
	.p2align 3
.L236:
	movl	$704, %r8d
	movl	$66, %edx
	movl	$114, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L170
	.p2align 4,,10
	.p2align 3
.L238:
	movl	$218, %r8d
	movl	$65, %edx
	movl	$106, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L170
	.cfi_endproc
.LFE392:
	.size	EC_GROUP_copy, .-EC_GROUP_copy
	.p2align 4
	.globl	EC_GROUP_set_generator
	.type	EC_GROUP_set_generator, @function
EC_GROUP_set_generator:
.LFB397:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%rsi, %rsi
	je	.L310
	movq	%rdi, %rbx
	movq	64(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L244
	movq	%rsi, %r12
	movq	%rdx, %r13
	movq	%rcx, %r14
	call	BN_is_zero@PLT
	testl	%eax, %eax
	je	.L311
.L244:
	movl	$340, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$103, %edx
	xorl	%r15d, %r15d
	movl	$111, %esi
	movl	$16, %edi
	call	ERR_put_error@PLT
.L240:
	addq	$24, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L311:
	.cfi_restore_state
	movq	64(%rbx), %rdi
	call	BN_is_negative@PLT
	movl	%eax, %r15d
	testl	%eax, %eax
	jne	.L244
	testq	%r13, %r13
	je	.L246
	movq	%r13, %rdi
	call	BN_is_zero@PLT
	testl	%eax, %eax
	jne	.L246
	movq	%r13, %rdi
	call	BN_is_negative@PLT
	testl	%eax, %eax
	je	.L312
.L246:
	movl	$351, %r8d
	movl	$122, %edx
	movl	$111, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L240
	.p2align 4,,10
	.p2align 3
.L310:
	movl	$333, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$67, %edx
	xorl	%r15d, %r15d
	movl	$111, %esi
	movl	$16, %edi
	call	ERR_put_error@PLT
	jmp	.L240
	.p2align 4,,10
	.p2align 3
.L312:
	movq	%r13, %rdi
	call	BN_num_bits@PLT
	movq	64(%rbx), %rdi
	movl	%eax, -52(%rbp)
	call	BN_num_bits@PLT
	addl	$1, %eax
	cmpl	%eax, -52(%rbp)
	jg	.L246
	testq	%r14, %r14
	je	.L247
	movq	%r14, %rdi
	call	BN_is_negative@PLT
	testl	%eax, %eax
	jne	.L313
.L247:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L314
.L248:
	movq	(%rdi), %rax
	movq	104(%rax), %rdx
	testq	%rdx, %rdx
	je	.L315
	cmpq	(%r12), %rax
	jne	.L250
	movl	8(%rdi), %eax
	movl	8(%r12), %ecx
	cmpl	%ecx, %eax
	setne	%sil
	testl	%eax, %eax
	setne	%al
	testb	%al, %sil
	je	.L251
	testl	%ecx, %ecx
	jne	.L250
.L251:
	cmpq	%rdi, %r12
	je	.L255
	movq	%r12, %rsi
	call	*%rdx
	testl	%eax, %eax
	je	.L309
.L255:
	movq	16(%rbx), %rdi
	movq	%r13, %rsi
	call	BN_copy@PLT
	testq	%rax, %rax
	je	.L309
	testq	%r14, %r14
	je	.L256
	movq	%r14, %rdi
	call	BN_is_zero@PLT
	testl	%eax, %eax
	je	.L316
.L256:
	movq	16(%rbx), %rdi
	call	BN_num_bits@PLT
	movq	64(%rbx), %rdi
	movl	%eax, %r12d
	call	BN_num_bits@PLT
	leal	1(%rax), %edx
	movl	%edx, %eax
	shrl	$31, %eax
	addl	%edx, %eax
	sarl	%eax
	addl	$3, %eax
	cmpl	%eax, %r12d
	jle	.L317
	call	BN_CTX_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L259
	movq	%rax, %rdi
	call	BN_CTX_start@PLT
	movq	%r12, %rdi
	call	BN_CTX_get@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L260
	movq	(%rbx), %rax
	cmpl	$407, 4(%rax)
	je	.L318
	movq	64(%rbx), %rsi
	movq	%r13, %rdi
	call	BN_copy@PLT
	testq	%rax, %rax
	je	.L260
.L266:
	movq	16(%rbx), %rsi
	movq	24(%rbx), %rdi
	call	BN_rshift1@PLT
	testl	%eax, %eax
	je	.L260
	movq	24(%rbx), %rdi
	movq	%r13, %rdx
	movq	%rdi, %rsi
	call	BN_add@PLT
	testl	%eax, %eax
	je	.L260
	call	BN_value_one@PLT
	movq	24(%rbx), %rdi
	movq	%rax, %rdx
	movq	%rdi, %rsi
	call	BN_add@PLT
	testl	%eax, %eax
	je	.L260
	movq	24(%rbx), %rdi
	movq	16(%rbx), %rcx
	movq	%r12, %r8
	xorl	%esi, %esi
	movq	%rdi, %rdx
	call	BN_div@PLT
	movq	%r12, %rdi
	testl	%eax, %eax
	je	.L308
	call	BN_CTX_end@PLT
	movq	%r12, %rdi
	call	BN_CTX_free@PLT
.L257:
	movq	16(%rbx), %rdi
	call	BN_is_odd@PLT
	testl	%eax, %eax
	jne	.L319
	movq	144(%rbx), %rdi
	movl	$1, %r15d
	call	BN_MONT_CTX_free@PLT
	movq	$0, 144(%rbx)
	jmp	.L240
	.p2align 4,,10
	.p2align 3
.L250:
	movl	$711, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$101, %edx
	xorl	%r15d, %r15d
	movl	$114, %esi
	movl	$16, %edi
	call	ERR_put_error@PLT
	jmp	.L240
.L314:
	movq	%rbx, %rdi
	call	EC_POINT_new
	movq	%rax, 8(%rbx)
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L248
.L309:
	xorl	%r15d, %r15d
	jmp	.L240
.L318:
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	BN_set_word@PLT
	movq	64(%rbx), %rdi
	call	BN_num_bits@PLT
	movq	%r13, %rdi
	leal	-1(%rax), %esi
	call	BN_set_bit@PLT
	testl	%eax, %eax
	jne	.L266
.L260:
	movq	%r12, %rdi
.L308:
	call	BN_CTX_end@PLT
	movq	%r12, %rdi
	call	BN_CTX_free@PLT
.L259:
	movq	24(%rbx), %rdi
	xorl	%esi, %esi
	call	BN_set_word@PLT
	jmp	.L309
.L313:
	movl	$361, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$164, %edx
	xorl	%r15d, %r15d
	movl	$111, %esi
	movl	$16, %edi
	call	ERR_put_error@PLT
	jmp	.L240
.L319:
	call	BN_CTX_new@PLT
	movq	144(%rbx), %rdi
	movq	%rax, %r12
	call	BN_MONT_CTX_free@PLT
	movq	$0, 144(%rbx)
	testq	%r12, %r12
	je	.L273
	call	BN_MONT_CTX_new@PLT
	movq	%rax, 144(%rbx)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L273
	movq	16(%rbx), %rsi
	movq	%r12, %rdx
	call	BN_MONT_CTX_set@PLT
	movl	%eax, %r15d
	testl	%eax, %eax
	je	.L320
	movl	$1, %r15d
.L272:
	movq	%r12, %rdi
	call	BN_CTX_free@PLT
	jmp	.L240
.L317:
	movq	24(%rbx), %rdi
	xorl	%esi, %esi
	call	BN_set_word@PLT
	jmp	.L257
.L315:
	movl	$704, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$66, %edx
	xorl	%r15d, %r15d
	movl	$114, %esi
	movl	$16, %edi
	call	ERR_put_error@PLT
	jmp	.L240
.L316:
	movq	24(%rbx), %rdi
	movq	%r14, %rsi
	call	BN_copy@PLT
	testq	%rax, %rax
	jne	.L257
	jmp	.L309
.L273:
	xorl	%r15d, %r15d
	jmp	.L272
.L320:
	movq	144(%rbx), %rdi
	call	BN_MONT_CTX_free@PLT
	movq	$0, 144(%rbx)
	jmp	.L272
	.cfi_endproc
.LFE397:
	.size	EC_GROUP_set_generator, .-EC_GROUP_set_generator
	.p2align 4
	.globl	EC_GROUP_dup
	.type	EC_GROUP_dup, @function
EC_GROUP_dup:
.LFB393:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	testq	%rdi, %rdi
	je	.L389
	movq	%rdi, %r12
	movq	(%rdi), %rdi
	call	EC_GROUP_new
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L389
	movq	(%rax), %rax
	cmpq	$0, 32(%rax)
	je	.L391
	cmpq	(%r12), %rax
	jne	.L392
	cmpq	%r13, %r12
	je	.L321
	movl	32(%r12), %eax
	movl	%eax, 32(%r13)
	movl	152(%r12), %eax
	movl	%eax, 152(%r13)
	movl	152(%r12), %eax
	cmpl	$4, %eax
	je	.L328
	cmpl	$5, %eax
	je	.L329
	testl	%eax, %eax
	je	.L393
.L330:
	movq	144(%r12), %rsi
	movq	144(%r13), %rdi
	testq	%rsi, %rsi
	je	.L331
	testq	%rdi, %rdi
	je	.L394
.L332:
	call	BN_MONT_CTX_copy@PLT
	testq	%rax, %rax
	je	.L326
.L335:
	movq	8(%r12), %rsi
	movq	8(%r13), %r14
	testq	%rsi, %rsi
	je	.L336
	testq	%r14, %r14
	je	.L395
.L337:
	movq	(%r14), %rax
	movq	104(%rax), %rdx
	testq	%rdx, %rdx
	je	.L396
	cmpq	(%rsi), %rax
	jne	.L339
	movl	8(%r14), %eax
	movl	8(%rsi), %ecx
	cmpl	%ecx, %eax
	setne	%dil
	testl	%eax, %eax
	setne	%al
	testb	%al, %dil
	je	.L340
	testl	%ecx, %ecx
	jne	.L339
.L340:
	cmpq	%rsi, %r14
	je	.L342
	movq	%r14, %rdi
	call	*%rdx
	testl	%eax, %eax
	je	.L326
.L342:
	movq	(%r12), %rax
	testb	$2, (%rax)
	je	.L347
.L350:
	movl	36(%r12), %eax
	cmpq	$0, 48(%r12)
	movq	48(%r13), %rdi
	movl	%eax, 36(%r13)
	movl	40(%r12), %eax
	movl	%eax, 40(%r13)
	je	.L397
	movl	$216, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	56(%r12), %rdi
	movl	$217, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_malloc@PLT
	movq	%rax, 48(%r13)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L398
	movq	48(%r12), %rsi
	movq	56(%r12), %rdx
	call	memcpy@PLT
	movq	56(%r12), %rax
	movq	%rax, 56(%r13)
.L352:
	movq	0(%r13), %rax
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	*32(%rax)
	testl	%eax, %eax
	je	.L326
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L392:
	.cfi_restore_state
	movl	$137, %r8d
	movl	$101, %edx
	movl	$106, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L326:
	movq	%r13, %rdi
	call	EC_GROUP_free
	.p2align 4,,10
	.p2align 3
.L389:
	xorl	%r13d, %r13d
.L321:
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L394:
	.cfi_restore_state
	call	BN_MONT_CTX_new@PLT
	movq	%rax, 144(%r13)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L326
	movq	144(%r12), %rsi
	jmp	.L332
	.p2align 4,,10
	.p2align 3
.L395:
	movq	%r13, %rdi
	call	EC_POINT_new
	movq	%rax, 8(%r13)
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L326
	movq	8(%r12), %rsi
	jmp	.L337
	.p2align 4,,10
	.p2align 3
.L339:
	movl	$711, %r8d
	movl	$101, %edx
	movl	$114, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L326
	.p2align 4,,10
	.p2align 3
.L393:
	movq	$0, 160(%r13)
	jmp	.L330
	.p2align 4,,10
	.p2align 3
.L329:
	movq	160(%r12), %rdi
	call	EC_ec_pre_comp_dup@PLT
	movq	%rax, 160(%r13)
	jmp	.L330
	.p2align 4,,10
	.p2align 3
.L328:
	movq	160(%r12), %rdi
	call	EC_nistz256_pre_comp_dup@PLT
	movq	%rax, 160(%r13)
	jmp	.L330
	.p2align 4,,10
	.p2align 3
.L391:
	movl	$133, %r8d
	movl	$66, %edx
	movl	$106, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L326
	.p2align 4,,10
	.p2align 3
.L347:
	movq	16(%r12), %rsi
	movq	16(%r13), %rdi
	call	BN_copy@PLT
	testq	%rax, %rax
	je	.L326
	movq	24(%r12), %rsi
	movq	24(%r13), %rdi
	call	BN_copy@PLT
	testq	%rax, %rax
	jne	.L350
	jmp	.L326
	.p2align 4,,10
	.p2align 3
.L331:
	call	BN_MONT_CTX_free@PLT
	movq	$0, 144(%r13)
	jmp	.L335
	.p2align 4,,10
	.p2align 3
.L396:
	movl	$704, %r8d
	movl	$66, %edx
	movl	$114, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L326
	.p2align 4,,10
	.p2align 3
.L397:
	movl	$225, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 48(%r13)
	movq	$0, 56(%r13)
	jmp	.L352
	.p2align 4,,10
	.p2align 3
.L336:
	testq	%r14, %r14
	je	.L344
	movq	(%r14), %rdx
	movq	96(%rdx), %rax
	testq	%rax, %rax
	je	.L345
.L388:
	movq	%r14, %rdi
	call	*%rax
.L346:
	movl	$698, %ecx
	leaq	.LC0(%rip), %rdx
	movl	$48, %esi
	movq	%r14, %rdi
	call	CRYPTO_clear_free@PLT
.L344:
	movq	$0, 8(%r13)
	jmp	.L342
	.p2align 4,,10
	.p2align 3
.L398:
	movl	$218, %r8d
	movl	$65, %edx
	movl	$106, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L326
	.p2align 4,,10
	.p2align 3
.L345:
	movq	88(%rdx), %rax
	testq	%rax, %rax
	jne	.L388
	jmp	.L346
	.cfi_endproc
.LFE393:
	.size	EC_GROUP_dup, .-EC_GROUP_dup
	.p2align 4
	.globl	EC_POINT_free
	.type	EC_POINT_free, @function
EC_POINT_free:
.LFB424:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L399
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	(%rdi), %rax
	movq	88(%rax), %rax
	testq	%rax, %rax
	je	.L401
	call	*%rax
.L401:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$686, %edx
	popq	%r12
	.cfi_restore 12
	leaq	.LC0(%rip), %rsi
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_free@PLT
	.p2align 4,,10
	.p2align 3
.L399:
	ret
	.cfi_endproc
.LFE424:
	.size	EC_POINT_free, .-EC_POINT_free
	.p2align 4
	.globl	EC_POINT_clear_free
	.type	EC_POINT_clear_free, @function
EC_POINT_clear_free:
.LFB425:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L408
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	(%rdi), %rdx
	movq	96(%rdx), %rax
	testq	%rax, %rax
	je	.L410
.L418:
	movq	%r12, %rdi
	call	*%rax
.L411:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$698, %ecx
	movl	$48, %esi
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	leaq	.LC0(%rip), %rdx
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_clear_free@PLT
	.p2align 4,,10
	.p2align 3
.L410:
	.cfi_restore_state
	movq	88(%rdx), %rax
	testq	%rax, %rax
	jne	.L418
	jmp	.L411
	.p2align 4,,10
	.p2align 3
.L408:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE425:
	.size	EC_POINT_clear_free, .-EC_POINT_clear_free
	.p2align 4
	.globl	EC_POINT_copy
	.type	EC_POINT_copy, @function
EC_POINT_copy:
.LFB426:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rdi), %rax
	movq	104(%rax), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testq	%rdx, %rdx
	je	.L432
	cmpq	(%rsi), %rax
	jne	.L422
	movl	8(%rdi), %eax
	movl	8(%rsi), %ecx
	cmpl	%ecx, %eax
	setne	%r8b
	testl	%eax, %eax
	setne	%al
	testb	%al, %r8b
	je	.L423
	testl	%ecx, %ecx
	jne	.L422
.L423:
	cmpq	%rsi, %rdi
	je	.L424
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rdx
	.p2align 4,,10
	.p2align 3
.L422:
	.cfi_restore_state
	movl	$711, %r8d
	movl	$101, %edx
	movl	$114, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L424:
	.cfi_restore_state
	movl	$1, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L432:
	.cfi_restore_state
	movl	$704, %r8d
	movl	$66, %edx
	movl	$114, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE426:
	.size	EC_POINT_copy, .-EC_POINT_copy
	.p2align 4
	.globl	EC_POINT_dup
	.type	EC_POINT_dup, @function
EC_POINT_dup:
.LFB427:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	testq	%rdi, %rdi
	je	.L444
	movq	%rsi, %rbx
	testq	%rsi, %rsi
	je	.L455
	movq	(%rsi), %rax
	cmpq	$0, 80(%rax)
	je	.L456
	movq	%rdi, %r13
	movl	$662, %edx
	leaq	.LC0(%rip), %rsi
	movl	$48, %edi
	call	CRYPTO_zalloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L457
	movq	(%rbx), %rax
	movl	32(%rbx), %edx
	movq	%r12, %rdi
	movq	%rax, (%r12)
	movl	%edx, 8(%r12)
	call	*80(%rax)
	testl	%eax, %eax
	je	.L458
	movq	(%r12), %rax
	movq	104(%rax), %rdx
	testq	%rdx, %rdx
	je	.L459
	cmpq	0(%r13), %rax
	jne	.L441
	movl	8(%r12), %eax
	movl	8(%r13), %ecx
	testl	%eax, %eax
	setne	%sil
	cmpl	%ecx, %eax
	setne	%al
	testb	%al, %sil
	je	.L442
	testl	%ecx, %ecx
	jne	.L441
.L442:
	cmpq	%r12, %r13
	je	.L433
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	*%rdx
	testl	%eax, %eax
	je	.L440
.L433:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L441:
	.cfi_restore_state
	movl	$711, %r8d
	movl	$101, %edx
	movl	$114, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L440:
	movq	(%r12), %rax
	movq	88(%rax), %rax
	testq	%rax, %rax
	je	.L443
	movq	%r12, %rdi
	call	*%rax
.L443:
	movq	%r12, %rdi
	movl	$686, %edx
	leaq	.LC0(%rip), %rsi
	xorl	%r12d, %r12d
	call	CRYPTO_free@PLT
	jmp	.L433
	.p2align 4,,10
	.p2align 3
.L444:
	addq	$8, %rsp
	xorl	%r12d, %r12d
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L459:
	.cfi_restore_state
	movl	$704, %r8d
	movl	$66, %edx
	movl	$114, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L440
	.p2align 4,,10
	.p2align 3
.L456:
	movl	$658, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$66, %edx
	xorl	%r12d, %r12d
	movl	$121, %esi
	movl	$16, %edi
	call	ERR_put_error@PLT
	jmp	.L433
	.p2align 4,,10
	.p2align 3
.L458:
	movq	%r12, %rdi
	movl	$672, %edx
	leaq	.LC0(%rip), %rsi
	xorl	%r12d, %r12d
	call	CRYPTO_free@PLT
	jmp	.L433
	.p2align 4,,10
	.p2align 3
.L455:
	movl	$654, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$67, %edx
	xorl	%r12d, %r12d
	movl	$121, %esi
	movl	$16, %edi
	call	ERR_put_error@PLT
	jmp	.L433
	.p2align 4,,10
	.p2align 3
.L457:
	movl	$664, %r8d
	movl	$65, %edx
	movl	$121, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L433
	.cfi_endproc
.LFE427:
	.size	EC_POINT_dup, .-EC_POINT_dup
	.p2align 4
	.globl	EC_POINT_method_of
	.type	EC_POINT_method_of, @function
EC_POINT_method_of:
.LFB428:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE428:
	.size	EC_POINT_method_of, .-EC_POINT_method_of
	.p2align 4
	.globl	EC_POINT_set_to_infinity
	.type	EC_POINT_set_to_infinity, @function
EC_POINT_set_to_infinity:
.LFB429:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rdi), %rax
	movq	112(%rax), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testq	%rdx, %rdx
	je	.L466
	cmpq	(%rsi), %rax
	jne	.L467
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rdx
	.p2align 4,,10
	.p2align 3
.L467:
	.cfi_restore_state
	movl	$751, %r8d
	movl	$101, %edx
	movl	$127, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L461:
	xorl	%eax, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L466:
	.cfi_restore_state
	movl	$746, %r8d
	movl	$66, %edx
	movl	$127, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L461
	.cfi_endproc
.LFE429:
	.size	EC_POINT_set_to_infinity, .-EC_POINT_set_to_infinity
	.p2align 4
	.globl	EC_POINT_set_Jprojective_coordinates_GFp
	.type	EC_POINT_set_Jprojective_coordinates_GFp, @function
EC_POINT_set_Jprojective_coordinates_GFp:
.LFB430:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rdi), %rax
	movq	120(%rax), %r10
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testq	%r10, %r10
	je	.L483
	cmpq	(%rsi), %rax
	je	.L484
.L471:
	movl	$768, %r8d
	movl	$101, %edx
	movl	$126, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L468:
	xorl	%eax, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L484:
	.cfi_restore_state
	movl	32(%rdi), %eax
	testl	%eax, %eax
	je	.L472
	movl	8(%rsi), %r11d
	testl	%r11d, %r11d
	je	.L472
	cmpl	%r11d, %eax
	jne	.L471
.L472:
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%r10
	.p2align 4,,10
	.p2align 3
.L483:
	.cfi_restore_state
	movl	$763, %r8d
	movl	$66, %edx
	movl	$126, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L468
	.cfi_endproc
.LFE430:
	.size	EC_POINT_set_Jprojective_coordinates_GFp, .-EC_POINT_set_Jprojective_coordinates_GFp
	.p2align 4
	.globl	EC_POINT_get_Jprojective_coordinates_GFp
	.type	EC_POINT_get_Jprojective_coordinates_GFp, @function
EC_POINT_get_Jprojective_coordinates_GFp:
.LFB431:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rdi), %rax
	movq	128(%rax), %r10
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testq	%r10, %r10
	je	.L500
	cmpq	(%rsi), %rax
	je	.L501
.L488:
	movl	$787, %r8d
	movl	$101, %edx
	movl	$117, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L485:
	xorl	%eax, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L501:
	.cfi_restore_state
	movl	32(%rdi), %eax
	testl	%eax, %eax
	je	.L489
	movl	8(%rsi), %r11d
	testl	%r11d, %r11d
	je	.L489
	cmpl	%r11d, %eax
	jne	.L488
.L489:
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%r10
	.p2align 4,,10
	.p2align 3
.L500:
	.cfi_restore_state
	movl	$782, %r8d
	movl	$66, %edx
	movl	$117, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L485
	.cfi_endproc
.LFE431:
	.size	EC_POINT_get_Jprojective_coordinates_GFp, .-EC_POINT_get_Jprojective_coordinates_GFp
	.p2align 4
	.globl	EC_POINT_set_affine_coordinates
	.type	EC_POINT_set_affine_coordinates, @function
EC_POINT_set_affine_coordinates:
.LFB432:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	(%rdi), %rax
	movq	136(%rax), %r9
	testq	%r9, %r9
	je	.L534
	movq	%rsi, %r13
	cmpq	(%rsi), %rax
	je	.L535
.L505:
	movl	$805, %r8d
	movl	$101, %edx
	movl	$294, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
.L502:
	addq	$8, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L535:
	.cfi_restore_state
	movl	32(%rdi), %eax
	movq	%rdi, %r12
	movq	%r8, %r14
	testl	%eax, %eax
	je	.L506
	movl	8(%rsi), %esi
	testl	%esi, %esi
	je	.L506
	cmpl	%esi, %eax
	jne	.L505
.L506:
	movq	%r14, %r8
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	*%r9
	testl	%eax, %eax
	je	.L502
	movq	(%r12), %rax
	movq	208(%rax), %rcx
	testq	%rcx, %rcx
	je	.L536
	cmpq	0(%r13), %rax
	je	.L537
.L509:
	movl	$945, %r8d
	movl	$101, %edx
	movl	$119, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L508:
	movl	$812, %r8d
	movl	$107, %edx
	movl	$294, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L502
	.p2align 4,,10
	.p2align 3
.L534:
	movl	$800, %r8d
	movl	$66, %edx
	movl	$294, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L502
	.p2align 4,,10
	.p2align 3
.L537:
	movl	32(%r12), %eax
	testl	%eax, %eax
	je	.L510
	movl	8(%r13), %edx
	testl	%edx, %edx
	je	.L510
	cmpl	%edx, %eax
	jne	.L509
.L510:
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	*%rcx
	testl	%eax, %eax
	jle	.L508
	movl	$1, %eax
	jmp	.L502
	.p2align 4,,10
	.p2align 3
.L536:
	movl	$941, %r8d
	movl	$66, %edx
	movl	$119, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L508
	.cfi_endproc
.LFE432:
	.size	EC_POINT_set_affine_coordinates, .-EC_POINT_set_affine_coordinates
	.p2align 4
	.globl	EC_POINT_set_affine_coordinates_GFp
	.type	EC_POINT_set_affine_coordinates_GFp, @function
EC_POINT_set_affine_coordinates_GFp:
.LFB433:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	(%rdi), %rax
	movq	136(%rax), %r9
	testq	%r9, %r9
	je	.L570
	movq	%rsi, %r13
	cmpq	(%rsi), %rax
	je	.L571
.L541:
	movl	$805, %r8d
	movl	$101, %edx
	movl	$294, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
.L538:
	addq	$8, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L571:
	.cfi_restore_state
	movl	32(%rdi), %eax
	movq	%rdi, %r12
	movq	%r8, %r14
	testl	%eax, %eax
	je	.L542
	movl	8(%rsi), %esi
	testl	%esi, %esi
	je	.L542
	cmpl	%esi, %eax
	jne	.L541
.L542:
	movq	%r14, %r8
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	*%r9
	testl	%eax, %eax
	je	.L538
	movq	(%r12), %rax
	movq	208(%rax), %rcx
	testq	%rcx, %rcx
	je	.L572
	cmpq	0(%r13), %rax
	je	.L573
.L545:
	movl	$945, %r8d
	movl	$101, %edx
	movl	$119, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L544:
	movl	$812, %r8d
	movl	$107, %edx
	movl	$294, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L538
	.p2align 4,,10
	.p2align 3
.L570:
	movl	$800, %r8d
	movl	$66, %edx
	movl	$294, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L538
	.p2align 4,,10
	.p2align 3
.L573:
	movl	32(%r12), %eax
	testl	%eax, %eax
	je	.L546
	movl	8(%r13), %edx
	testl	%edx, %edx
	je	.L546
	cmpl	%edx, %eax
	jne	.L545
.L546:
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	*%rcx
	testl	%eax, %eax
	jle	.L544
	movl	$1, %eax
	jmp	.L538
	.p2align 4,,10
	.p2align 3
.L572:
	movl	$941, %r8d
	movl	$66, %edx
	movl	$119, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L544
	.cfi_endproc
.LFE433:
	.size	EC_POINT_set_affine_coordinates_GFp, .-EC_POINT_set_affine_coordinates_GFp
	.p2align 4
	.globl	EC_POINT_set_affine_coordinates_GF2m
	.type	EC_POINT_set_affine_coordinates_GF2m, @function
EC_POINT_set_affine_coordinates_GF2m:
.LFB473:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	(%rdi), %rax
	movq	136(%rax), %r9
	testq	%r9, %r9
	je	.L606
	movq	%rsi, %r13
	cmpq	(%rsi), %rax
	je	.L607
.L577:
	movl	$805, %r8d
	movl	$101, %edx
	movl	$294, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
.L574:
	addq	$8, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L607:
	.cfi_restore_state
	movl	32(%rdi), %eax
	movq	%rdi, %r12
	movq	%r8, %r14
	testl	%eax, %eax
	je	.L578
	movl	8(%rsi), %esi
	cmpl	%esi, %eax
	je	.L578
	testl	%esi, %esi
	jne	.L577
.L578:
	movq	%r14, %r8
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	*%r9
	testl	%eax, %eax
	je	.L574
	movq	(%r12), %rax
	movq	208(%rax), %rcx
	testq	%rcx, %rcx
	je	.L608
	cmpq	0(%r13), %rax
	je	.L609
.L581:
	movl	$945, %r8d
	movl	$101, %edx
	movl	$119, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L580:
	movl	$812, %r8d
	movl	$107, %edx
	movl	$294, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L574
	.p2align 4,,10
	.p2align 3
.L606:
	movl	$800, %r8d
	movl	$66, %edx
	movl	$294, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L574
	.p2align 4,,10
	.p2align 3
.L609:
	movl	32(%r12), %eax
	testl	%eax, %eax
	je	.L582
	movl	8(%r13), %edx
	testl	%edx, %edx
	je	.L582
	cmpl	%edx, %eax
	jne	.L581
.L582:
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	*%rcx
	testl	%eax, %eax
	jle	.L580
	movl	$1, %eax
	jmp	.L574
	.p2align 4,,10
	.p2align 3
.L608:
	movl	$941, %r8d
	movl	$66, %edx
	movl	$119, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L580
	.cfi_endproc
.LFE473:
	.size	EC_POINT_set_affine_coordinates_GF2m, .-EC_POINT_set_affine_coordinates_GF2m
	.p2align 4
	.globl	EC_POINT_get_affine_coordinates
	.type	EC_POINT_get_affine_coordinates, @function
EC_POINT_get_affine_coordinates:
.LFB435:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rax
	cmpq	$0, 144(%rax)
	je	.L635
	movq	%rsi, %r13
	cmpq	(%rsi), %rax
	je	.L636
.L613:
	movl	$846, %r8d
	movl	$101, %edx
	movl	$293, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L610:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L636:
	.cfi_restore_state
	movq	%rdx, %r15
	movl	32(%rdi), %edx
	movq	%rdi, %r12
	movq	%rcx, %r14
	movq	%r8, %rbx
	testl	%edx, %edx
	je	.L615
	movl	8(%rsi), %ecx
	testl	%ecx, %ecx
	je	.L615
	cmpl	%ecx, %edx
	jne	.L613
.L615:
	movq	200(%rax), %rax
	testq	%rax, %rax
	jne	.L637
	movl	$919, %r8d
	movl	$66, %edx
	movl	$118, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L616:
	movq	(%r12), %rax
	movq	%rbx, %r8
	movq	%r14, %rcx
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	144(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L637:
	.cfi_restore_state
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	*%rax
	testl	%eax, %eax
	je	.L616
	movl	$850, %r8d
	movl	$106, %edx
	movl	$293, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L610
	.p2align 4,,10
	.p2align 3
.L635:
	movl	$841, %r8d
	movl	$66, %edx
	movl	$293, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L610
	.cfi_endproc
.LFE435:
	.size	EC_POINT_get_affine_coordinates, .-EC_POINT_get_affine_coordinates
	.p2align 4
	.globl	EC_POINT_get_affine_coordinates_GFp
	.type	EC_POINT_get_affine_coordinates_GFp, @function
EC_POINT_get_affine_coordinates_GFp:
.LFB436:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rax
	cmpq	$0, 144(%rax)
	je	.L663
	movq	%rsi, %r13
	cmpq	(%rsi), %rax
	je	.L664
.L641:
	movl	$846, %r8d
	movl	$101, %edx
	movl	$293, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L638:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L664:
	.cfi_restore_state
	movq	%rdx, %r15
	movl	32(%rdi), %edx
	movq	%rdi, %r12
	movq	%rcx, %r14
	movq	%r8, %rbx
	testl	%edx, %edx
	je	.L643
	movl	8(%rsi), %ecx
	testl	%ecx, %ecx
	je	.L643
	cmpl	%ecx, %edx
	jne	.L641
.L643:
	movq	200(%rax), %rax
	testq	%rax, %rax
	jne	.L665
	movl	$919, %r8d
	movl	$66, %edx
	movl	$118, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L644:
	movq	(%r12), %rax
	movq	%rbx, %r8
	movq	%r14, %rcx
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	144(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L665:
	.cfi_restore_state
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	*%rax
	testl	%eax, %eax
	je	.L644
	movl	$850, %r8d
	movl	$106, %edx
	movl	$293, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L638
	.p2align 4,,10
	.p2align 3
.L663:
	movl	$841, %r8d
	movl	$66, %edx
	movl	$293, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L638
	.cfi_endproc
.LFE436:
	.size	EC_POINT_get_affine_coordinates_GFp, .-EC_POINT_get_affine_coordinates_GFp
	.p2align 4
	.globl	EC_POINT_get_affine_coordinates_GF2m
	.type	EC_POINT_get_affine_coordinates_GF2m, @function
EC_POINT_get_affine_coordinates_GF2m:
.LFB475:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rax
	cmpq	$0, 144(%rax)
	je	.L691
	movq	%rsi, %r13
	cmpq	(%rsi), %rax
	je	.L692
.L669:
	movl	$846, %r8d
	movl	$101, %edx
	movl	$293, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L666:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L692:
	.cfi_restore_state
	movq	%rdx, %r15
	movl	32(%rdi), %edx
	movq	%rdi, %r12
	movq	%rcx, %r14
	movq	%r8, %rbx
	testl	%edx, %edx
	je	.L671
	movl	8(%rsi), %ecx
	testl	%ecx, %ecx
	je	.L671
	cmpl	%ecx, %edx
	jne	.L669
.L671:
	movq	200(%rax), %rax
	testq	%rax, %rax
	jne	.L693
	movl	$919, %r8d
	movl	$66, %edx
	movl	$118, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L672:
	movq	(%r12), %rax
	movq	%rbx, %r8
	movq	%r14, %rcx
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	144(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L693:
	.cfi_restore_state
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	*%rax
	testl	%eax, %eax
	je	.L672
	movl	$850, %r8d
	movl	$106, %edx
	movl	$293, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L666
	.p2align 4,,10
	.p2align 3
.L691:
	movl	$841, %r8d
	movl	$66, %edx
	movl	$293, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L666
	.cfi_endproc
.LFE475:
	.size	EC_POINT_get_affine_coordinates_GF2m, .-EC_POINT_get_affine_coordinates_GF2m
	.p2align 4
	.globl	EC_POINT_add
	.type	EC_POINT_add, @function
EC_POINT_add:
.LFB438:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rdi), %rax
	movq	176(%rax), %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testq	%r9, %r9
	je	.L723
	cmpq	(%rsi), %rax
	je	.L724
.L697:
	movl	$883, %r8d
	movl	$101, %edx
	movl	$112, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L694:
	xorl	%eax, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L724:
	.cfi_restore_state
	movl	32(%rdi), %r10d
	testl	%r10d, %r10d
	je	.L698
	movl	8(%rsi), %r11d
	testl	%r11d, %r11d
	je	.L700
	cmpl	%r11d, %r10d
	jne	.L697
.L700:
	cmpq	(%rdx), %rax
	jne	.L697
	movl	8(%rdx), %r11d
	cmpl	%r11d, %r10d
	je	.L702
	testl	%r11d, %r11d
	jne	.L697
.L702:
	cmpq	(%rcx), %rax
	jne	.L697
	movl	8(%rcx), %eax
	testl	%eax, %eax
	je	.L703
	cmpl	%eax, %r10d
	jne	.L697
.L703:
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%r9
	.p2align 4,,10
	.p2align 3
.L698:
	.cfi_restore_state
	cmpq	(%rdx), %rax
	jne	.L697
	cmpq	(%rcx), %rax
	jne	.L697
	jmp	.L703
	.p2align 4,,10
	.p2align 3
.L723:
	movl	$878, %r8d
	movl	$66, %edx
	movl	$112, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L694
	.cfi_endproc
.LFE438:
	.size	EC_POINT_add, .-EC_POINT_add
	.p2align 4
	.globl	EC_POINT_dbl
	.type	EC_POINT_dbl, @function
EC_POINT_dbl:
.LFB439:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rdi), %rax
	movq	184(%rax), %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testq	%r8, %r8
	je	.L745
	cmpq	(%rsi), %rax
	je	.L746
.L728:
	movl	$897, %r8d
	movl	$101, %edx
	movl	$115, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L725:
	xorl	%eax, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L746:
	.cfi_restore_state
	movl	32(%rdi), %r9d
	testl	%r9d, %r9d
	je	.L729
	movl	8(%rsi), %r10d
	cmpl	%r10d, %r9d
	je	.L731
	testl	%r10d, %r10d
	jne	.L728
.L731:
	cmpq	(%rdx), %rax
	jne	.L728
	movl	8(%rdx), %eax
	cmpl	%eax, %r9d
	je	.L732
	testl	%eax, %eax
	jne	.L728
.L732:
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%r8
	.p2align 4,,10
	.p2align 3
.L729:
	.cfi_restore_state
	cmpq	(%rdx), %rax
	jne	.L728
	jmp	.L732
	.p2align 4,,10
	.p2align 3
.L745:
	movl	$893, %r8d
	movl	$66, %edx
	movl	$115, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L725
	.cfi_endproc
.LFE439:
	.size	EC_POINT_dbl, .-EC_POINT_dbl
	.p2align 4
	.globl	EC_POINT_invert
	.type	EC_POINT_invert, @function
EC_POINT_invert:
.LFB440:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rdi), %rax
	movq	192(%rax), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testq	%rcx, %rcx
	je	.L762
	cmpq	(%rsi), %rax
	je	.L763
.L750:
	movl	$910, %r8d
	movl	$101, %edx
	movl	$210, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L747:
	xorl	%eax, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L763:
	.cfi_restore_state
	movl	32(%rdi), %eax
	testl	%eax, %eax
	je	.L751
	movl	8(%rsi), %r8d
	testl	%r8d, %r8d
	je	.L751
	cmpl	%r8d, %eax
	jne	.L750
.L751:
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rcx
	.p2align 4,,10
	.p2align 3
.L762:
	.cfi_restore_state
	movl	$906, %r8d
	movl	$66, %edx
	movl	$210, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L747
	.cfi_endproc
.LFE440:
	.size	EC_POINT_invert, .-EC_POINT_invert
	.p2align 4
	.globl	EC_POINT_is_at_infinity
	.type	EC_POINT_is_at_infinity, @function
EC_POINT_is_at_infinity:
.LFB441:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rdi), %rax
	movq	200(%rax), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testq	%rdx, %rdx
	je	.L779
	cmpq	(%rsi), %rax
	je	.L780
.L767:
	movl	$924, %r8d
	movl	$101, %edx
	movl	$118, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L764:
	xorl	%eax, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L780:
	.cfi_restore_state
	movl	32(%rdi), %eax
	testl	%eax, %eax
	je	.L768
	movl	8(%rsi), %ecx
	testl	%ecx, %ecx
	je	.L768
	cmpl	%ecx, %eax
	jne	.L767
.L768:
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rdx
	.p2align 4,,10
	.p2align 3
.L779:
	.cfi_restore_state
	movl	$919, %r8d
	movl	$66, %edx
	movl	$118, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L764
	.cfi_endproc
.LFE441:
	.size	EC_POINT_is_at_infinity, .-EC_POINT_is_at_infinity
	.p2align 4
	.globl	EC_POINT_is_on_curve
	.type	EC_POINT_is_on_curve, @function
EC_POINT_is_on_curve:
.LFB442:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rdi), %rax
	movq	208(%rax), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testq	%rcx, %rcx
	je	.L796
	cmpq	(%rsi), %rax
	je	.L797
.L784:
	movl	$945, %r8d
	movl	$101, %edx
	movl	$119, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L781:
	xorl	%eax, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L797:
	.cfi_restore_state
	movl	32(%rdi), %eax
	testl	%eax, %eax
	je	.L785
	movl	8(%rsi), %r8d
	testl	%r8d, %r8d
	je	.L785
	cmpl	%r8d, %eax
	jne	.L784
.L785:
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rcx
	.p2align 4,,10
	.p2align 3
.L796:
	.cfi_restore_state
	movl	$941, %r8d
	movl	$66, %edx
	movl	$119, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L781
	.cfi_endproc
.LFE442:
	.size	EC_POINT_is_on_curve, .-EC_POINT_is_on_curve
	.p2align 4
	.globl	EC_POINT_cmp
	.type	EC_POINT_cmp, @function
EC_POINT_cmp:
.LFB443:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rdi), %rax
	movq	216(%rax), %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testq	%r8, %r8
	je	.L818
	cmpq	(%rsi), %rax
	je	.L819
.L801:
	movl	$959, %r8d
	movl	$101, %edx
	movl	$113, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L798:
	movl	$-1, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L819:
	.cfi_restore_state
	movl	32(%rdi), %r9d
	testl	%r9d, %r9d
	je	.L802
	movl	8(%rsi), %r10d
	cmpl	%r10d, %r9d
	je	.L804
	testl	%r10d, %r10d
	jne	.L801
.L804:
	cmpq	(%rdx), %rax
	jne	.L801
	movl	8(%rdx), %eax
	cmpl	%eax, %r9d
	je	.L805
	testl	%eax, %eax
	jne	.L801
.L805:
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%r8
	.p2align 4,,10
	.p2align 3
.L802:
	.cfi_restore_state
	cmpq	(%rdx), %rax
	jne	.L801
	jmp	.L805
.L818:
	movl	$955, %r8d
	movl	$66, %edx
	movl	$113, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L798
	.cfi_endproc
.LFE443:
	.size	EC_POINT_cmp, .-EC_POINT_cmp
	.p2align 4
	.globl	EC_GROUP_cmp
	.type	EC_GROUP_cmp, @function
EC_GROUP_cmp:
.LFB422:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rsi), %rcx
	movq	(%rdi), %rdx
	movl	4(%rcx), %ecx
	cmpl	%ecx, 4(%rdx)
	jne	.L820
	movl	32(%rdi), %ecx
	movq	%rdi, %rbx
	movq	%rsi, %r14
	testl	%ecx, %ecx
	jne	.L847
.L822:
	xorl	%eax, %eax
	testb	$2, (%rdx)
	jne	.L820
	movq	$0, -56(%rbp)
	testq	%r15, %r15
	je	.L848
.L824:
	movq	%r15, %rdi
	call	BN_CTX_start@PLT
	movq	%r15, %rdi
	call	BN_CTX_get@PLT
	movq	%r15, %rdi
	movq	%rax, %r13
	call	BN_CTX_get@PLT
	movq	%r15, %rdi
	movq	%rax, -64(%rbp)
	call	BN_CTX_get@PLT
	movq	%r15, %rdi
	movq	%rax, -72(%rbp)
	call	BN_CTX_get@PLT
	movq	%r15, %rdi
	movq	%rax, -80(%rbp)
	call	BN_CTX_get@PLT
	movq	%r15, %rdi
	movq	%rax, -88(%rbp)
	call	BN_CTX_get@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L836
	movq	(%rbx), %rax
	movq	-72(%rbp), %rcx
	movq	%r15, %r8
	movq	%r13, %rsi
	movq	-64(%rbp), %rdx
	movq	%rbx, %rdi
	call	*48(%rax)
	testl	%eax, %eax
	je	.L828
	movq	(%r14), %rax
	movq	-88(%rbp), %rdx
	movq	%r15, %r8
	movq	%r12, %rcx
	movq	-80(%rbp), %rsi
	movq	%r14, %rdi
	call	*48(%rax)
	testl	%eax, %eax
	jne	.L849
.L828:
	movl	$1, %eax
.L827:
	movq	%r15, %rdi
	movl	%eax, -64(%rbp)
	call	BN_CTX_end@PLT
	movq	-56(%rbp), %rdi
	call	BN_CTX_free@PLT
	movl	-64(%rbp), %eax
.L820:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L847:
	.cfi_restore_state
	movl	32(%rsi), %esi
	cmpl	%ecx, %esi
	je	.L822
	testl	%esi, %esi
	je	.L822
	jmp	.L820
	.p2align 4,,10
	.p2align 3
.L848:
	call	BN_CTX_new@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	jne	.L850
	movl	$-1, %eax
	jmp	.L820
	.p2align 4,,10
	.p2align 3
.L836:
	movq	%r15, %rdi
	call	BN_CTX_end@PLT
	movq	-56(%rbp), %rdi
	call	BN_CTX_free@PLT
	movl	$-1, %eax
	jmp	.L820
	.p2align 4,,10
	.p2align 3
.L849:
	movq	-80(%rbp), %rsi
	movq	%r13, %rdi
	call	BN_cmp@PLT
	testl	%eax, %eax
	jne	.L828
	movq	-88(%rbp), %rsi
	movq	-64(%rbp), %rdi
	call	BN_cmp@PLT
	testl	%eax, %eax
	jne	.L828
	movq	-72(%rbp), %rdi
	movq	%r12, %rsi
	call	BN_cmp@PLT
	testl	%eax, %eax
	jne	.L828
	movq	8(%r14), %rdx
	movq	8(%rbx), %rsi
	movq	%r15, %rcx
	movq	%rbx, %rdi
	call	EC_POINT_cmp
	testl	%eax, %eax
	jne	.L828
	movq	16(%rbx), %rdi
	movq	16(%r14), %rsi
	testq	%rdi, %rdi
	je	.L836
	testq	%rsi, %rsi
	je	.L836
	movq	24(%rbx), %r12
	movq	24(%r14), %r13
	call	BN_cmp@PLT
	testl	%eax, %eax
	jne	.L828
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	BN_cmp@PLT
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
	jmp	.L827
.L850:
	movq	%rax, -56(%rbp)
	jmp	.L824
	.cfi_endproc
.LFE422:
	.size	EC_GROUP_cmp, .-EC_GROUP_cmp
	.p2align 4
	.globl	EC_POINT_make_affine
	.type	EC_POINT_make_affine, @function
EC_POINT_make_affine:
.LFB444:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rdi), %rax
	movq	224(%rax), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testq	%rcx, %rcx
	je	.L866
	cmpq	(%rsi), %rax
	je	.L867
.L854:
	movl	$972, %r8d
	movl	$101, %edx
	movl	$120, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L851:
	xorl	%eax, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L867:
	.cfi_restore_state
	movl	32(%rdi), %eax
	testl	%eax, %eax
	je	.L855
	movl	8(%rsi), %r8d
	testl	%r8d, %r8d
	je	.L855
	cmpl	%r8d, %eax
	jne	.L854
.L855:
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rcx
	.p2align 4,,10
	.p2align 3
.L866:
	.cfi_restore_state
	movl	$968, %r8d
	movl	$66, %edx
	movl	$120, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L851
	.cfi_endproc
.LFE444:
	.size	EC_POINT_make_affine, .-EC_POINT_make_affine
	.p2align 4
	.globl	EC_POINTs_make_affine
	.type	EC_POINTs_make_affine, @function
EC_POINTs_make_affine:
.LFB445:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rdi), %r8
	movq	232(%r8), %r10
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testq	%r10, %r10
	je	.L869
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L871
.L870:
	movq	(%rdx,%rax,8), %r9
	cmpq	(%r9), %r8
	je	.L888
.L873:
	movl	$989, %r8d
	movl	$101, %edx
	movl	$136, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L868:
	xorl	%eax, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L888:
	.cfi_restore_state
	movl	32(%rdi), %r11d
	testl	%r11d, %r11d
	je	.L874
	movl	8(%r9), %r9d
	cmpl	%r9d, %r11d
	je	.L874
	testl	%r9d, %r9d
	jne	.L873
.L874:
	addq	$1, %rax
	cmpq	%rax, %rsi
	jne	.L870
.L871:
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%r10
	.p2align 4,,10
	.p2align 3
.L869:
	.cfi_restore_state
	movl	$984, %r8d
	movl	$66, %edx
	movl	$136, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L868
	.cfi_endproc
.LFE445:
	.size	EC_POINTs_make_affine, .-EC_POINTs_make_affine
	.p2align 4
	.globl	EC_POINTs_mul
	.type	EC_POINTs_mul, @function
EC_POINTs_mul:
.LFB446:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	16(%rbp), %r11
	movq	(%rdi), %rax
	cmpq	%rax, (%rsi)
	je	.L926
.L890:
	movl	$1011, %r8d
.L925:
	movl	$101, %edx
	movl	$290, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
.L889:
	leaq	-16(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L926:
	.cfi_restore_state
	movl	32(%rdi), %r10d
	testl	%r10d, %r10d
	je	.L891
	movl	8(%rsi), %r12d
	cmpl	%r12d, %r10d
	je	.L891
	testl	%r12d, %r12d
	jne	.L890
.L891:
	movq	%rdx, %rbx
	orq	%rcx, %rbx
	je	.L893
	testq	%rcx, %rcx
	je	.L895
	testl	%r10d, %r10d
	jne	.L905
	xorl	%r10d, %r10d
.L899:
	movq	(%r8,%r10,8), %rbx
	cmpq	%rax, (%rbx)
	jne	.L898
	addq	$1, %r10
	cmpq	%r10, %rcx
	jne	.L899
	.p2align 4,,10
	.p2align 3
.L895:
	xorl	%r12d, %r12d
	testq	%r11, %r11
	je	.L927
.L901:
	movq	240(%rax), %rax
	testq	%rax, %rax
	je	.L903
	subq	$8, %rsp
	pushq	%r11
	call	*%rax
	popq	%rsi
	popq	%rdi
.L904:
	movq	%r12, %rdi
	movl	%eax, -24(%rbp)
	call	BN_CTX_free@PLT
	movl	-24(%rbp), %eax
	jmp	.L889
	.p2align 4,,10
	.p2align 3
.L905:
	xorl	%r12d, %r12d
.L897:
	movq	(%r8,%r12,8), %rbx
	cmpq	(%rbx), %rax
	je	.L928
.L898:
	movl	$1020, %r8d
	jmp	.L925
	.p2align 4,,10
	.p2align 3
.L893:
	movq	112(%rax), %rax
	testq	%rax, %rax
	je	.L929
	leaq	-16(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L928:
	.cfi_restore_state
	movl	8(%rbx), %ebx
	cmpl	%ebx, %r10d
	je	.L900
	testl	%ebx, %ebx
	jne	.L898
.L900:
	addq	$1, %r12
	cmpq	%r12, %rcx
	jne	.L897
	jmp	.L895
.L929:
	movl	$746, %r8d
	movl	$66, %edx
	movl	$127, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L889
.L927:
	movq	%r9, -64(%rbp)
	movq	%r8, -56(%rbp)
	movq	%rcx, -48(%rbp)
	movq	%rdx, -40(%rbp)
	movq	%rsi, -32(%rbp)
	movq	%rdi, -24(%rbp)
	call	BN_CTX_secure_new@PLT
	movq	%rax, %r11
	testq	%rax, %rax
	je	.L902
	movq	-24(%rbp), %rdi
	movq	-32(%rbp), %rsi
	movq	%r11, %r12
	movq	-40(%rbp), %rdx
	movq	-48(%rbp), %rcx
	movq	(%rdi), %rax
	movq	-56(%rbp), %r8
	movq	-64(%rbp), %r9
	jmp	.L901
.L903:
	subq	$8, %rsp
	pushq	%r11
	call	ec_wNAF_mul@PLT
	popq	%rdx
	popq	%rcx
	jmp	.L904
.L902:
	movl	$1026, %r8d
	movl	$68, %edx
	movl	$290, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L889
	.cfi_endproc
.LFE446:
	.size	EC_POINTs_mul, .-EC_POINTs_mul
	.p2align 4
	.globl	EC_POINT_mul
	.type	EC_POINT_mul, @function
EC_POINT_mul:
.LFB447:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	subq	$64, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	%rcx, -40(%rbp)
	movq	(%rdi), %rax
	movq	%r8, -32(%rbp)
	cmpq	%rax, (%rsi)
	je	.L969
.L931:
	movl	$1011, %r8d
.L968:
	movl	$101, %edx
	movl	$290, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
.L930:
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L970
	leaq	-16(%rbp), %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L969:
	.cfi_restore_state
	movl	32(%rdi), %r11d
	testl	%r11d, %r11d
	je	.L932
	movl	8(%rsi), %r10d
	testl	%r10d, %r10d
	je	.L932
	cmpl	%r10d, %r11d
	jne	.L931
.L932:
	testq	%rcx, %rcx
	setne	%r10b
	testq	%r8, %r8
	setne	%r8b
	andl	%r10d, %r8d
	testq	%rdx, %rdx
	jne	.L934
	testb	%r8b, %r8b
	je	.L971
.L934:
	movzbl	%r8b, %r13d
	xorl	%r8d, %r8d
.L937:
	testl	%r11d, %r11d
	jne	.L939
.L941:
	cmpq	%r8, %r13
	jbe	.L940
	movl	$1, %r8d
	cmpq	%rax, (%rcx)
	je	.L941
.L936:
	movl	$1020, %r8d
	jmp	.L968
	.p2align 4,,10
	.p2align 3
.L939:
	cmpq	%r8, %r13
	jbe	.L940
	cmpq	(%rcx), %rax
	jne	.L936
	movl	8(%rcx), %r10d
	movl	$1, %r8d
	testl	%r10d, %r10d
	je	.L937
	cmpl	%r10d, %r11d
	je	.L937
	jmp	.L936
	.p2align 4,,10
	.p2align 3
.L971:
	movq	112(%rax), %rax
	testq	%rax, %rax
	je	.L972
	call	*%rax
	jmp	.L930
	.p2align 4,,10
	.p2align 3
.L940:
	xorl	%r12d, %r12d
	testq	%r9, %r9
	je	.L973
.L943:
	movq	240(%rax), %rax
	testq	%rax, %rax
	je	.L945
	subq	$8, %rsp
	leaq	-40(%rbp), %r8
	movq	%r13, %rcx
	pushq	%r9
	leaq	-32(%rbp), %r9
	call	*%rax
	popq	%rsi
	popq	%rdi
.L946:
	movq	%r12, %rdi
	movl	%eax, -56(%rbp)
	call	BN_CTX_free@PLT
	movl	-56(%rbp), %eax
	jmp	.L930
.L972:
	movl	$746, %r8d
	movl	$66, %edx
	movl	$127, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L930
.L973:
	movq	%rdx, -72(%rbp)
	movq	%rsi, -64(%rbp)
	movq	%rdi, -56(%rbp)
	call	BN_CTX_secure_new@PLT
	movq	%rax, %r9
	testq	%rax, %rax
	je	.L944
	movq	-56(%rbp), %rdi
	movq	-64(%rbp), %rsi
	movq	%r9, %r12
	movq	-72(%rbp), %rdx
	movq	(%rdi), %rax
	jmp	.L943
.L945:
	subq	$8, %rsp
	movq	%r13, %rcx
	leaq	-40(%rbp), %r8
	pushq	%r9
	leaq	-32(%rbp), %r9
	call	ec_wNAF_mul@PLT
	popq	%rdx
	popq	%rcx
	jmp	.L946
.L970:
	call	__stack_chk_fail@PLT
.L944:
	movl	$1026, %r8d
	movl	$68, %edx
	movl	$290, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L930
	.cfi_endproc
.LFE447:
	.size	EC_POINT_mul, .-EC_POINT_mul
	.p2align 4
	.globl	EC_GROUP_precompute_mult
	.type	EC_GROUP_precompute_mult, @function
EC_GROUP_precompute_mult:
.LFB448:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	cmpq	$0, 240(%rax)
	je	.L977
	movq	248(%rax), %rax
	testq	%rax, %rax
	je	.L976
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L976:
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L977:
	jmp	ec_wNAF_precompute_mult@PLT
	.cfi_endproc
.LFE448:
	.size	EC_GROUP_precompute_mult, .-EC_GROUP_precompute_mult
	.p2align 4
	.globl	EC_GROUP_have_precompute_mult
	.type	EC_GROUP_have_precompute_mult, @function
EC_GROUP_have_precompute_mult:
.LFB449:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	cmpq	$0, 240(%rax)
	je	.L981
	movq	256(%rax), %rax
	testq	%rax, %rax
	je	.L980
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L980:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L981:
	jmp	ec_wNAF_have_precompute_mult@PLT
	.cfi_endproc
.LFE449:
	.size	EC_GROUP_have_precompute_mult, .-EC_GROUP_have_precompute_mult
	.p2align 4
	.globl	EC_KEY_set_ex_data
	.type	EC_KEY_set_ex_data, @function
EC_KEY_set_ex_data:
.LFB451:
	.cfi_startproc
	endbr64
	addq	$64, %rdi
	jmp	CRYPTO_set_ex_data@PLT
	.cfi_endproc
.LFE451:
	.size	EC_KEY_set_ex_data, .-EC_KEY_set_ex_data
	.p2align 4
	.globl	EC_KEY_get_ex_data
	.type	EC_KEY_get_ex_data, @function
EC_KEY_get_ex_data:
.LFB452:
	.cfi_startproc
	endbr64
	addq	$64, %rdi
	jmp	CRYPTO_get_ex_data@PLT
	.cfi_endproc
.LFE452:
	.size	EC_KEY_get_ex_data, .-EC_KEY_get_ex_data
	.p2align 4
	.globl	ec_group_simple_order_bits
	.type	ec_group_simple_order_bits, @function
ec_group_simple_order_bits:
.LFB453:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L985
	jmp	BN_num_bits@PLT
	.p2align 4,,10
	.p2align 3
.L985:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE453:
	.size	ec_group_simple_order_bits, .-ec_group_simple_order_bits
	.p2align 4
	.globl	ec_group_do_inverse_ord
	.type	ec_group_do_inverse_ord, @function
ec_group_do_inverse_ord:
.LFB455:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %rax
	movq	392(%rax), %rax
	testq	%rax, %rax
	je	.L987
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L987:
	.cfi_restore_state
	cmpq	$0, 144(%rdi)
	je	.L991
	xorl	%ebx, %ebx
	testq	%rcx, %rcx
	je	.L1006
.L990:
	movq	%r13, %rdi
	call	BN_CTX_start@PLT
	movq	%r13, %rdi
	call	BN_CTX_get@PLT
	testq	%rax, %rax
	je	.L994
	movl	$2, %esi
	movq	%rax, %rdi
	movq	%rax, -56(%rbp)
	call	BN_set_word@PLT
	testl	%eax, %eax
	jne	.L1007
.L994:
	xorl	%r12d, %r12d
.L993:
	movq	%r13, %rdi
	call	BN_CTX_end@PLT
	movq	%rbx, %rdi
	call	BN_CTX_free@PLT
.L986:
	addq	$24, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1007:
	.cfi_restore_state
	movq	-56(%rbp), %rdx
	movq	16(%r12), %rsi
	movq	%rdx, %rdi
	call	BN_sub@PLT
	testl	%eax, %eax
	je	.L994
	movq	16(%r12), %rcx
	movq	-56(%rbp), %rdx
	movq	%r13, %r8
	movq	%r15, %rsi
	movq	144(%r12), %r9
	movq	%r14, %rdi
	xorl	%r12d, %r12d
	call	BN_mod_exp_mont@PLT
	testl	%eax, %eax
	setne	%r12b
	jmp	.L993
	.p2align 4,,10
	.p2align 3
.L1006:
	call	BN_CTX_secure_new@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L991
	movq	%rax, %rbx
	jmp	.L990
	.p2align 4,,10
	.p2align 3
.L991:
	xorl	%r12d, %r12d
	jmp	.L986
	.cfi_endproc
.LFE455:
	.size	ec_group_do_inverse_ord, .-ec_group_do_inverse_ord
	.p2align 4
	.globl	ec_point_blind_coordinates
	.type	ec_point_blind_coordinates, @function
ec_point_blind_coordinates:
.LFB456:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	400(%rax), %rax
	testq	%rax, %rax
	je	.L1009
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L1009:
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE456:
	.size	ec_point_blind_coordinates, .-ec_point_blind_coordinates
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
