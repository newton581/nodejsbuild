	.file	"asn_mime.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/asn1/asn_mime.c"
	.text
	.p2align 4
	.type	mime_param_free, @function
mime_param_free:
.LFB899:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$921, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	(%rdi), %rdi
	call	CRYPTO_free@PLT
	movq	8(%r12), %rdi
	movl	$922, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$923, %edx
	popq	%r12
	leaq	.LC0(%rip), %rsi
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_free@PLT
	.cfi_endproc
.LFE899:
	.size	mime_param_free, .-mime_param_free
	.p2align 4
	.type	mime_hdr_cmp, @function
mime_hdr_cmp:
.LFB894:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	(%rax), %rdi
	movq	(%rsi), %rax
	movq	(%rax), %rsi
	testq	%rdi, %rdi
	je	.L5
	testq	%rsi, %rsi
	je	.L5
	jmp	strcmp@PLT
	.p2align 4,,10
	.p2align 3
.L5:
	xorl	%eax, %eax
	testq	%rdi, %rdi
	setne	%al
	xorl	%edx, %edx
	testq	%rsi, %rsi
	setne	%dl
	subl	%edx, %eax
	ret
	.cfi_endproc
.LFE894:
	.size	mime_hdr_cmp, .-mime_hdr_cmp
	.p2align 4
	.type	mime_param_cmp, @function
mime_param_cmp:
.LFB895:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	(%rax), %rdi
	movq	(%rsi), %rax
	movq	(%rax), %rsi
	testq	%rdi, %rdi
	je	.L13
	testq	%rsi, %rsi
	je	.L13
	jmp	strcmp@PLT
	.p2align 4,,10
	.p2align 3
.L13:
	xorl	%eax, %eax
	testq	%rdi, %rdi
	setne	%al
	xorl	%edx, %edx
	testq	%rsi, %rsi
	setne	%dl
	subl	%edx, %eax
	ret
	.cfi_endproc
.LFE895:
	.size	mime_param_cmp, .-mime_param_cmp
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"--"
.LC2:
	.string	"\r\n"
	.text
	.p2align 4
	.type	multi_split, @function
multi_split:
.LFB887:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movq	%rsi, %rdi
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$1096, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -1104(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strlen@PLT
	movl	%eax, %ebx
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, -1128(%rbp)
	movq	%rax, (%r12)
	testq	%rax, %rax
	je	.L59
	movb	$1, -1092(%rbp)
	xorl	%r13d, %r13d
	leaq	-1088(%rbp), %r12
	movq	$0, -1120(%rbp)
	movl	$0, -1096(%rbp)
	.p2align 4,,10
	.p2align 3
.L21:
	movl	$1024, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	BIO_gets@PLT
	movl	%eax, %r15d
	testl	%eax, %eax
	jle	.L61
	movl	%ebx, %eax
	cmpl	$-1, %ebx
	jne	.L23
	movq	-1104(%rbp), %rdi
	call	strlen@PLT
.L23:
	leal	1(%rax), %edx
	cmpl	%edx, %r15d
	jle	.L24
	cmpw	$11565, (%r12)
	je	.L62
.L24:
	testb	%r13b, %r13b
	je	.L21
	movslq	%r15d, %rax
	xorl	%r9d, %r9d
	leaq	-1(%r12,%rax), %rax
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L63:
	cmpb	$13, %dl
	jne	.L31
	subq	$1, %rax
	subl	$1, %r15d
	je	.L31
.L32:
	movzbl	(%rax), %edx
	cmpb	$10, %dl
	jne	.L63
	movl	$1, %r9d
	subq	$1, %rax
	subl	$1, %r15d
	jne	.L32
.L31:
	cmpb	$0, -1092(%rbp)
	je	.L33
	movq	-1120(%rbp), %rsi
	testq	%rsi, %rsi
	je	.L34
	movq	-1128(%rbp), %rdi
	movl	%r9d, -1092(%rbp)
	call	OPENSSL_sk_push@PLT
	movl	-1092(%rbp), %r9d
	testl	%eax, %eax
	movl	%eax, %r10d
	je	.L60
.L34:
	movl	%r9d, -1092(%rbp)
	call	BIO_s_mem@PLT
	movq	%rax, %rdi
	call	BIO_new@PLT
	movq	%rax, -1120(%rbp)
	testq	%rax, %rax
	je	.L59
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$130, %esi
	movq	%rax, %rdi
	call	BIO_ctrl@PLT
	movl	-1092(%rbp), %r9d
.L36:
	movl	%r9d, -1096(%rbp)
	movb	$0, -1092(%rbp)
	testl	%r15d, %r15d
	je	.L21
	movq	-1120(%rbp), %rdi
	movl	%r15d, %edx
	movq	%r12, %rsi
	call	BIO_write@PLT
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L62:
	movq	-1104(%rbp), %rsi
	movslq	%eax, %rdx
	leaq	-1086(%rbp), %rdi
	movq	%rdx, -1112(%rbp)
	call	strncmp@PLT
	testl	%eax, %eax
	jne	.L24
	movq	-1112(%rbp), %rdx
	leaq	2(%r12,%rdx), %rax
	cmpb	$45, (%rax)
	jne	.L46
	cmpb	$45, 1(%rax)
	jne	.L46
	movq	-1120(%rbp), %rsi
	movq	-1128(%rbp), %rdi
	call	OPENSSL_sk_push@PLT
	movl	%eax, %r10d
	testl	%eax, %eax
	je	.L60
	movl	$1, %r10d
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L46:
	movb	$1, -1092(%rbp)
	addl	$1, %r13d
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L33:
	movl	-1096(%rbp), %eax
	testl	%eax, %eax
	je	.L36
	movq	-1120(%rbp), %rdi
	movl	$2, %edx
	leaq	.LC2(%rip), %rsi
	movl	%r9d, -1092(%rbp)
	call	BIO_write@PLT
	movl	-1092(%rbp), %r9d
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L61:
	movq	-1120(%rbp), %rdi
	call	BIO_free@PLT
.L59:
	xorl	%r10d, %r10d
.L20:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L64
	addq	$1096, %rsp
	movl	%r10d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L60:
	.cfi_restore_state
	movq	-1120(%rbp), %rdi
	movl	%r10d, -1092(%rbp)
	call	BIO_free@PLT
	movl	-1092(%rbp), %r10d
	jmp	.L20
.L64:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE887:
	.size	multi_split, .-multi_split
	.p2align 4
	.type	mime_hdr_new, @function
mime_hdr_new:
.LFB892:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	testq	%rdi, %rdi
	je	.L66
	movl	$805, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_strdup@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L72
	movsbl	(%rax), %edi
	testb	%dil, %dil
	je	.L66
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L68:
	call	ossl_tolower@PLT
	addq	$1, %rbx
	movb	%al, -1(%rbx)
	movsbl	(%rbx), %edi
	testb	%dil, %dil
	jne	.L68
.L66:
	testq	%r12, %r12
	je	.L69
	movq	%r12, %rdi
	movl	$811, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_strdup@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L73
	movsbl	(%rax), %edi
	testb	%dil, %dil
	je	.L69
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L71:
	call	ossl_tolower@PLT
	addq	$1, %rbx
	movb	%al, -1(%rbx)
	movsbl	(%rbx), %edi
	testb	%dil, %dil
	jne	.L71
.L69:
	movl	$816, %edx
	leaq	.LC0(%rip), %rsi
	movl	$24, %edi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L70
	movq	%r12, %xmm1
	movq	%r13, %xmm0
	leaq	mime_param_cmp(%rip), %rdi
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rax)
	call	OPENSSL_sk_new@PLT
	movq	%rax, 16(%r14)
	testq	%rax, %rax
	je	.L70
.L65:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L73:
	.cfi_restore_state
	xorl	%r14d, %r14d
.L70:
	movq	%r13, %rdi
	movl	$826, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%r12, %rdi
	movl	$827, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%r14, %rdi
	xorl	%r14d, %r14d
	movl	$828, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L72:
	.cfi_restore_state
	xorl	%r14d, %r14d
	jmp	.L65
	.cfi_endproc
.LFE892:
	.size	mime_hdr_new, .-mime_hdr_new
	.p2align 4
	.type	mime_hdr_addparam.isra.0, @function
mime_hdr_addparam.isra.0:
.LFB904:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	testq	%rsi, %rsi
	je	.L93
	movq	%r12, %rdi
	movl	$838, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_strdup@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L98
	movsbl	(%rax), %edi
	testb	%dil, %dil
	je	.L93
	movq	%rax, %r14
	.p2align 4,,10
	.p2align 3
.L95:
	call	ossl_tolower@PLT
	addq	$1, %r14
	movb	%al, -1(%r14)
	movsbl	(%r14), %edi
	testb	%dil, %dil
	jne	.L95
.L93:
	testq	%r13, %r13
	je	.L96
	movq	%r13, %rdi
	movl	$845, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_strdup@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L99
.L96:
	movl	$850, %edx
	leaq	.LC0(%rip), %rsi
	movl	$16, %edi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L94
	movq	%r13, %xmm1
	movq	%r12, %xmm0
	movq	(%rbx), %rdi
	movq	%rax, %rsi
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rax)
	call	OPENSSL_sk_push@PLT
	movl	%eax, %r8d
	movl	$1, %eax
	testl	%r8d, %r8d
	je	.L94
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L99:
	.cfi_restore_state
	xorl	%r14d, %r14d
.L94:
	movq	%r12, %rdi
	movl	$859, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%r13, %rdi
	movl	$860, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%r14, %rdi
	movl	$861, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L98:
	.cfi_restore_state
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	jmp	.L94
	.cfi_endproc
.LFE904:
	.size	mime_hdr_addparam.isra.0, .-mime_hdr_addparam.isra.0
	.p2align 4
	.type	b64_read_asn1, @function
b64_read_asn1:
.LFB880:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -40
	call	BIO_f_base64@PLT
	movq	%rax, %rdi
	call	BIO_new@PLT
	testq	%rax, %rax
	je	.L120
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	BIO_push@PLT
	movq	%r14, %rdi
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%rax, %r13
	call	ASN1_item_d2i_bio@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L121
.L118:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	movq	%r13, %rdi
	call	BIO_ctrl@PLT
	movq	%r13, %rdi
	call	BIO_pop@PLT
	movq	%r12, %rdi
	call	BIO_free@PLT
.L115:
	addq	$8, %rsp
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L121:
	.cfi_restore_state
	movl	$143, %r8d
	movl	$110, %edx
	movl	$209, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L120:
	movl	$137, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r14d, %r14d
	movl	$209, %esi
	movl	$13, %edi
	call	ERR_put_error@PLT
	jmp	.L115
	.cfi_endproc
.LFE880:
	.size	b64_read_asn1, .-b64_read_asn1
	.p2align 4
	.type	mime_hdr_free, @function
mime_hdr_free:
.LFB898:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L122
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$912, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	(%rdi), %rdi
	call	CRYPTO_free@PLT
	movq	8(%r12), %rdi
	movl	$913, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	16(%r12), %rdi
	testq	%rdi, %rdi
	je	.L124
	leaq	mime_param_free(%rip), %rsi
	call	OPENSSL_sk_pop_free@PLT
.L124:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$916, %edx
	popq	%r12
	.cfi_restore 12
	leaq	.LC0(%rip), %rsi
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_free@PLT
	.p2align 4,,10
	.p2align 3
.L122:
	ret
	.cfi_endproc
.LFE898:
	.size	mime_hdr_free, .-mime_hdr_free
	.p2align 4
	.type	mime_parse_hdr, @function
mime_parse_hdr:
.LFB888:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$1096, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -1120(%rbp)
	leaq	mime_hdr_cmp(%rip), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	OPENSSL_sk_new@PLT
	movq	%rax, -1128(%rbp)
	testq	%rax, %rax
	je	.L131
	leaq	-1088(%rbp), %rax
	movq	$0, -1112(%rbp)
	movq	%rax, -1104(%rbp)
	.p2align 4,,10
	.p2align 3
.L133:
	movq	-1104(%rbp), %rsi
	movq	-1120(%rbp), %rdi
	movl	$1024, %edx
	call	BIO_gets@PLT
	testl	%eax, %eax
	jle	.L131
	cmpq	$0, -1112(%rbp)
	movsbl	-1088(%rbp), %edi
	movl	$1, %r14d
	je	.L134
	movl	$8, %esi
	call	ossl_ctype_check@PLT
	movzbl	-1088(%rbp), %edi
	testl	%eax, %eax
	je	.L134
	movl	$3, %r14d
.L134:
	testb	%dil, %dil
	je	.L131
	movq	-1104(%rbp), %rbx
	xorl	%r13d, %r13d
	leaq	.L139(%rip), %r12
	movq	%rbx, %r15
.L136:
	cmpb	$13, %dil
	je	.L213
	cmpb	$10, %dil
	je	.L213
	cmpl	$4, %r14d
	ja	.L137
	movl	%r14d, %eax
	movslq	(%r12,%rax,4), %rax
	addq	%r12, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L139:
	.long	.L137-.L139
	.long	.L142-.L139
	.long	.L153-.L139
	.long	.L173-.L139
	.long	.L205-.L139
	.text
	.p2align 4,,10
	.p2align 3
.L167:
	movl	$9217, %edx
	btq	%rax, %rdx
	jc	.L165
.L169:
	movzbl	2(%rbx), %eax
	leaq	2(%rbx), %rdx
	cmpb	$13, %al
	ja	.L249
	movl	$9217, %ecx
	btq	%rax, %rcx
	jc	.L208
	movq	%rdx, %rbx
	movl	$2, %r14d
	.p2align 4,,10
	.p2align 3
.L172:
	movzbl	1(%rbx), %eax
	addq	$1, %rdx
	cmpb	$13, %al
	ja	.L251
	movl	$9217, %ecx
	btq	%rax, %rcx
	jc	.L208
.L251:
	movq	%rdx, %rbx
	cmpb	$41, %al
	jne	.L172
.L137:
	movzbl	1(%rbx), %edi
	leaq	1(%rbx), %rax
	movq	%rax, %rbx
	testb	%dil, %dil
	jne	.L136
.L213:
	cmpl	$2, %r14d
	jne	.L344
.L216:
	movsbl	(%r15), %edi
	testb	%dil, %dil
	jne	.L220
	jmp	.L254
	.p2align 4,,10
	.p2align 3
.L218:
	movl	$8, %esi
	call	ossl_ctype_check@PLT
	testl	%eax, %eax
	je	.L219
	movsbl	1(%r15), %edi
	addq	$1, %r15
	testb	%dil, %dil
	je	.L254
.L220:
	cmpb	$34, %dil
	jne	.L218
	cmpb	$0, 1(%r15)
	je	.L254
	addq	$1, %r15
.L219:
	movq	%r15, %rdi
	call	strlen@PLT
	leaq	-1(%r15,%rax), %r12
	cmpq	%r15, %r12
	jnb	.L222
	jmp	.L254
	.p2align 4,,10
	.p2align 3
.L221:
	movl	$8, %esi
	call	ossl_ctype_check@PLT
	testl	%eax, %eax
	je	.L151
	movb	$0, (%r12)
	subq	$1, %r12
	cmpq	%r15, %r12
	jb	.L254
.L222:
	movsbl	(%r12), %edi
	cmpb	$34, %dil
	jne	.L221
	leaq	-1(%r12), %rax
	cmpq	%rax, %r15
	je	.L254
	movb	$0, (%r12)
	.p2align 4,,10
	.p2align 3
.L151:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	mime_hdr_new
	movq	%rax, -1112(%rbp)
	testq	%rax, %rax
	je	.L163
	movq	-1128(%rbp), %rdi
	movq	%rax, %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L164
.L207:
	cmpq	-1104(%rbp), %rbx
	jne	.L133
.L131:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L351
	movq	-1128(%rbp), %rax
	addq	$1096, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L244:
	.cfi_restore_state
	movq	%r14, %rbx
	movq	%r14, %r15
.L205:
	leaq	1(%rbx), %rax
	movq	%rax, -1096(%rbp)
	cmpb	$59, %dil
	je	.L352
	movzbl	1(%rbx), %eax
	cmpb	$34, %dil
	je	.L191
	cmpb	$40, %dil
	je	.L353
	cmpb	$13, %al
	ja	.L198
	movl	$9217, %edx
	btq	%rax, %rdx
	jc	.L354
.L198:
	movq	-1096(%rbp), %rbx
	movl	%eax, %edi
	jmp	.L205
	.p2align 4,,10
	.p2align 3
.L359:
	movb	$0, (%rbx)
	movsbl	(%r15), %edi
	testb	%dil, %dil
	jne	.L144
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L146:
	movl	$8, %esi
	call	ossl_ctype_check@PLT
	testl	%eax, %eax
	je	.L238
	movsbl	1(%r15), %edi
	addq	$1, %r15
	testb	%dil, %dil
	je	.L147
.L144:
	cmpb	$34, %dil
	jne	.L146
	cmpb	$0, 1(%r15)
	leaq	1(%r15), %r13
	je	.L147
.L148:
	movq	%r13, %rdi
	call	strlen@PLT
	leaq	-1(%r13,%rax), %r15
	cmpq	%r13, %r15
	jnb	.L150
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L149:
	movl	$8, %esi
	call	ossl_ctype_check@PLT
	testl	%eax, %eax
	je	.L145
	movb	$0, (%r15)
	subq	$1, %r15
	cmpq	%r13, %r15
	jb	.L147
.L150:
	movsbl	(%r15), %edi
	cmpb	$34, %dil
	jne	.L149
	leaq	-1(%r15), %rax
	cmpq	%rax, %r13
	je	.L147
	movb	$0, (%r15)
.L145:
	movsbl	1(%rbx), %edi
	movq	%r14, %rbx
	testb	%dil, %dil
	je	.L254
	.p2align 4,,10
	.p2align 3
.L361:
	movq	%r14, %r15
	cmpb	$13, %dil
	je	.L220
	cmpb	$10, %dil
	je	.L220
.L153:
	cmpb	$59, %dil
	je	.L355
.L154:
	leaq	1(%rbx), %rcx
	movzbl	1(%rbx), %eax
	movq	%rcx, -1096(%rbp)
	cmpb	$40, %dil
	je	.L356
	cmpb	$13, %al
	ja	.L170
	movl	$9217, %ecx
	btq	%rax, %rcx
	jc	.L171
.L170:
	movl	%eax, %edi
	movq	-1096(%rbp), %rbx
	cmpb	$59, %dil
	jne	.L154
.L355:
	movb	$0, (%rbx)
	movsbl	(%r15), %edi
	testb	%dil, %dil
	jne	.L155
	jmp	.L158
	.p2align 4,,10
	.p2align 3
.L157:
	movl	$8, %esi
	call	ossl_ctype_check@PLT
	testl	%eax, %eax
	je	.L159
	movsbl	1(%r15), %edi
	addq	$1, %r15
	testb	%dil, %dil
	je	.L158
.L155:
	cmpb	$34, %dil
	jne	.L157
	cmpb	$0, 1(%r15)
	je	.L158
	addq	$1, %r15
.L159:
	movq	%r15, %rdi
	call	strlen@PLT
	leaq	-1(%r15,%rax), %r14
	cmpq	%r15, %r14
	jnb	.L161
	jmp	.L158
	.p2align 4,,10
	.p2align 3
.L160:
	movl	$8, %esi
	call	ossl_ctype_check@PLT
	testl	%eax, %eax
	je	.L156
	movb	$0, (%r14)
	subq	$1, %r14
	cmpq	%r15, %r14
	jb	.L158
.L161:
	movsbl	(%r14), %edi
	cmpb	$34, %dil
	jne	.L160
	leaq	-1(%r14), %rax
	cmpq	%rax, %r15
	je	.L158
	movb	$0, (%r14)
	.p2align 4,,10
	.p2align 3
.L156:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	mime_hdr_new
	movq	%rax, -1112(%rbp)
	testq	%rax, %rax
	je	.L163
	movq	-1128(%rbp), %rdi
	movq	%rax, %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L164
	movzbl	1(%rbx), %edi
	leaq	1(%rbx), %r15
	cmpb	$13, %dil
	ja	.L346
	movl	$9217, %eax
	btq	%rdi, %rax
	jc	.L357
.L346:
	movq	%r15, %rbx
.L173:
	leaq	1(%rbx), %r14
	cmpb	$61, %dil
	je	.L358
.L174:
	movzbl	1(%rbx), %edi
	movq	%r14, %rbx
	cmpb	$13, %dil
	ja	.L173
	movl	$9217, %eax
	btq	%rdi, %rax
	jc	.L207
	leaq	1(%rbx), %r14
	cmpb	$61, %dil
	jne	.L174
.L358:
	movb	$0, (%rbx)
	movsbl	(%r15), %edi
	testb	%dil, %dil
	jne	.L175
	jmp	.L178
	.p2align 4,,10
	.p2align 3
.L177:
	movl	$8, %esi
	call	ossl_ctype_check@PLT
	testl	%eax, %eax
	je	.L243
	movsbl	1(%r15), %edi
	addq	$1, %r15
	testb	%dil, %dil
	je	.L178
.L175:
	cmpb	$34, %dil
	jne	.L177
	cmpb	$0, 1(%r15)
	leaq	1(%r15), %r13
	je	.L178
.L179:
	movq	%r13, %rdi
	call	strlen@PLT
	leaq	-1(%r13,%rax), %r15
	cmpq	%r13, %r15
	jnb	.L181
	jmp	.L178
	.p2align 4,,10
	.p2align 3
.L180:
	movl	$8, %esi
	call	ossl_ctype_check@PLT
	testl	%eax, %eax
	je	.L176
	movb	$0, (%r15)
	subq	$1, %r15
	cmpq	%r13, %r15
	jb	.L178
.L181:
	movsbl	(%r15), %edi
	cmpb	$34, %dil
	jne	.L180
	leaq	-1(%r15), %rax
	cmpq	%rax, %r13
	je	.L178
	movb	$0, (%r15)
	.p2align 4,,10
	.p2align 3
.L176:
	movsbl	1(%rbx), %edi
	cmpb	$13, %dil
	ja	.L244
	movl	$9217, %eax
	movq	%r14, %rbx
	movq	%r14, %r15
	btq	%rdi, %rax
	jnc	.L205
	jmp	.L234
.L142:
	leaq	1(%rbx), %r14
	cmpb	$58, %dil
	je	.L359
	movzbl	1(%rbx), %edi
	movq	%r14, %rbx
	cmpb	$13, %dil
	ja	.L142
	movl	$9217, %eax
	btq	%rdi, %rax
	jnc	.L142
	jmp	.L207
	.p2align 4,,10
	.p2align 3
.L356:
	cmpb	$13, %al
	jbe	.L167
	cmpb	$41, %al
	jne	.L169
	leaq	2(%rbx), %rax
	movq	%rax, -1096(%rbp)
	movzbl	2(%rbx), %eax
	cmpb	$13, %al
	ja	.L170
	movl	$9217, %edx
	btq	%rax, %rdx
	jnc	.L170
.L171:
	movq	-1096(%rbp), %rbx
	jmp	.L216
	.p2align 4,,10
	.p2align 3
.L191:
	cmpb	$13, %al
	ja	.L203
	movl	$9217, %ecx
	btq	%rax, %rcx
	jc	.L165
.L203:
	movq	-1096(%rbp), %rbx
	addq	$1, %rbx
	movzbl	(%rbx), %edi
	cmpb	$34, %al
	je	.L360
.L201:
	cmpb	$13, %dil
	ja	.L212
	movl	$9217, %eax
	btq	%rdi, %rax
	jc	.L207
.L212:
	movl	%edi, %eax
	addq	$1, %rbx
	movzbl	(%rbx), %edi
	cmpb	$34, %al
	jne	.L201
.L360:
	cmpb	$13, %dil
	ja	.L205
	movl	$9217, %eax
	btq	%rdi, %rax
	jnc	.L205
.L199:
	movsbl	(%r15), %edi
.L234:
	testb	%dil, %dil
	jne	.L224
	jmp	.L227
	.p2align 4,,10
	.p2align 3
.L226:
	movl	$8, %esi
	call	ossl_ctype_check@PLT
	testl	%eax, %eax
	je	.L228
	movsbl	1(%r15), %edi
	addq	$1, %r15
	testb	%dil, %dil
	je	.L227
.L224:
	cmpb	$34, %dil
	jne	.L226
	cmpb	$0, 1(%r15)
	je	.L227
	addq	$1, %r15
.L228:
	movq	%r15, %rdi
	call	strlen@PLT
	leaq	-1(%r15,%rax), %r12
	cmpq	%r15, %r12
	jnb	.L230
	jmp	.L227
	.p2align 4,,10
	.p2align 3
.L229:
	movl	$8, %esi
	call	ossl_ctype_check@PLT
	testl	%eax, %eax
	je	.L225
	movb	$0, (%r12)
	subq	$1, %r12
	cmpq	%r15, %r12
	jb	.L227
.L230:
	movsbl	(%r12), %edi
	cmpb	$34, %dil
	jne	.L229
	leaq	-1(%r12), %rax
	cmpq	%rax, %r15
	je	.L227
	movb	$0, (%r12)
	jmp	.L225
.L227:
	xorl	%r15d, %r15d
.L225:
	movq	-1112(%rbp), %rax
	movq	%r15, %rdx
	movq	%r13, %rsi
	leaq	16(%rax), %rdi
	call	mime_hdr_addparam.isra.0
	jmp	.L207
	.p2align 4,,10
	.p2align 3
.L352:
	movb	$0, (%rbx)
	movsbl	(%r15), %edi
	testb	%dil, %dil
	jne	.L184
	jmp	.L187
	.p2align 4,,10
	.p2align 3
.L186:
	movl	$8, %esi
	call	ossl_ctype_check@PLT
	testl	%eax, %eax
	je	.L188
	movsbl	1(%r15), %edi
	addq	$1, %r15
	testb	%dil, %dil
	je	.L187
.L184:
	cmpb	$34, %dil
	jne	.L186
	cmpb	$0, 1(%r15)
	je	.L187
	addq	$1, %r15
.L188:
	movq	%r15, %rdi
	call	strlen@PLT
	leaq	-1(%r15,%rax), %r14
	cmpq	%r15, %r14
	jnb	.L190
	jmp	.L187
	.p2align 4,,10
	.p2align 3
.L189:
	movl	$8, %esi
	call	ossl_ctype_check@PLT
	testl	%eax, %eax
	je	.L185
	movb	$0, (%r14)
	subq	$1, %r14
	cmpq	%r15, %r14
	jb	.L187
.L190:
	movsbl	(%r14), %edi
	cmpb	$34, %dil
	jne	.L189
	leaq	-1(%r14), %rax
	cmpq	%rax, %r15
	je	.L187
	movb	$0, (%r14)
	.p2align 4,,10
	.p2align 3
.L185:
	movq	-1112(%rbp), %rax
	movq	%r15, %rdx
	movq	%r13, %rsi
	leaq	16(%rax), %rdi
	call	mime_hdr_addparam.isra.0
	movzbl	1(%rbx), %edi
	cmpb	$13, %dil
	ja	.L247
	movl	$9217, %eax
	btq	%rdi, %rax
	jc	.L165
.L247:
	movq	-1096(%rbp), %r15
	jmp	.L346
	.p2align 4,,10
	.p2align 3
.L353:
	cmpb	$13, %al
	jbe	.L193
	cmpb	$41, %al
	jne	.L195
	leaq	2(%rbx), %rax
	movq	%rax, -1096(%rbp)
	movzbl	2(%rbx), %eax
	cmpb	$13, %al
	ja	.L198
	movl	$9217, %edx
	btq	%rax, %rdx
	jnc	.L198
.L354:
	movq	-1096(%rbp), %rbx
	jmp	.L199
	.p2align 4,,10
	.p2align 3
.L178:
	xorl	%r13d, %r13d
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L147:
	movsbl	1(%rbx), %edi
	xorl	%r13d, %r13d
	movq	%r14, %rbx
	testb	%dil, %dil
	jne	.L361
.L254:
	xorl	%r15d, %r15d
	jmp	.L151
	.p2align 4,,10
	.p2align 3
.L158:
	xorl	%r15d, %r15d
	jmp	.L156
	.p2align 4,,10
	.p2align 3
.L187:
	xorl	%r15d, %r15d
	jmp	.L185
.L357:
	movq	%r15, -1096(%rbp)
.L165:
	movq	-1096(%rbp), %rbx
	jmp	.L207
.L344:
	cmpl	$4, %r14d
	jne	.L207
	jmp	.L199
	.p2align 4,,10
	.p2align 3
.L193:
	movl	$9217, %edx
	btq	%rax, %rdx
	jc	.L165
.L195:
	movzbl	2(%rbx), %eax
	leaq	2(%rbx), %rdx
	cmpb	$13, %al
	ja	.L248
	movl	$9217, %ecx
	btq	%rax, %rcx
	jc	.L208
	movq	%rdx, %rbx
	movl	$4, %r14d
	jmp	.L172
	.p2align 4,,10
	.p2align 3
.L249:
	movl	$2, %r14d
	jmp	.L251
.L243:
	movq	%r15, %r13
	jmp	.L179
.L238:
	movq	%r15, %r13
	jmp	.L148
.L248:
	movl	$4, %r14d
	jmp	.L251
.L208:
	movq	%rdx, %rbx
	jmp	.L207
.L164:
	movq	-1112(%rbp), %rbx
	movl	$912, %edx
	leaq	.LC0(%rip), %rsi
	movq	(%rbx), %rdi
	call	CRYPTO_free@PLT
	movq	8(%rbx), %rdi
	movl	$913, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L233
	leaq	mime_param_free(%rip), %rsi
	call	OPENSSL_sk_pop_free@PLT
.L233:
	movq	-1112(%rbp), %rdi
	movl	$916, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
.L163:
	movq	-1128(%rbp), %rdi
	leaq	mime_hdr_free(%rip), %rsi
	call	OPENSSL_sk_pop_free@PLT
	movq	$0, -1128(%rbp)
	jmp	.L131
.L351:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE888:
	.size	mime_parse_hdr, .-mime_parse_hdr
	.section	.rodata.str1.1
.LC3:
	.string	"content-type"
.LC4:
	.string	"multipart/signed"
.LC5:
	.string	"boundary"
.LC6:
	.string	"application/x-pkcs7-signature"
.LC7:
	.string	"application/pkcs7-signature"
.LC8:
	.string	"type: "
.LC9:
	.string	"application/x-pkcs7-mime"
.LC10:
	.string	"application/pkcs7-mime"
	.text
	.p2align 4
	.globl	SMIME_read_ASN1
	.type	SMIME_read_ASN1, @function
SMIME_read_ASN1:
.LFB884:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -88(%rbp)
	testq	%rsi, %rsi
	je	.L363
	movq	$0, (%rsi)
.L363:
	movq	%r12, %rdi
	call	mime_parse_hdr
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L408
	leaq	-80(%rbp), %r14
	leaq	.LC3(%rip), %rax
	movq	%r15, %rdi
	movq	$0, -72(%rbp)
	movq	%r14, %rsi
	movq	%rax, -80(%rbp)
	movq	$0, -64(%rbp)
	call	OPENSSL_sk_find@PLT
	movq	%r15, %rdi
	movl	%eax, %esi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %r10
	testq	%rax, %rax
	je	.L366
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L366
	movl	$17, %ecx
	leaq	.LC4(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L409
	movl	$25, %ecx
	leaq	.LC9(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L379
	movq	%rax, %rsi
	movl	$23, %ecx
	leaq	.LC10(%rip), %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L410
.L379:
	movq	%r15, %rdi
	leaq	mime_hdr_free(%rip), %rsi
	call	OPENSSL_sk_pop_free@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	b64_read_asn1
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L411
.L362:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L412
	addq	$72, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L409:
	.cfi_restore_state
	movq	16(%r10), %rdi
	leaq	.LC5(%rip), %rax
	movq	%r14, %rsi
	movq	%r10, -104(%rbp)
	movq	%rax, -80(%rbp)
	movq	$0, -72(%rbp)
	call	OPENSSL_sk_find@PLT
	movq	-104(%rbp), %r10
	movl	%eax, %esi
	movq	16(%r10), %rdi
	call	OPENSSL_sk_value@PLT
	testq	%rax, %rax
	je	.L369
	movq	8(%rax), %rsi
	testq	%rsi, %rsi
	je	.L369
	movq	%r12, %rdi
	leaq	-88(%rbp), %rdx
	call	multi_split
	leaq	mime_hdr_free(%rip), %rsi
	movq	%r15, %rdi
	movl	%eax, %r12d
	call	OPENSSL_sk_pop_free@PLT
	testl	%r12d, %r12d
	je	.L372
	movq	-88(%rbp), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	$2, %eax
	jne	.L372
	movq	-88(%rbp), %rdi
	movl	$1, %esi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	mime_parse_hdr
	movl	$425, %r8d
	movl	$208, %edx
	leaq	.LC0(%rip), %rcx
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L407
	movq	%r14, %rsi
	leaq	.LC3(%rip), %rax
	movq	%r15, %rdi
	movq	$0, -72(%rbp)
	movq	%rax, -80(%rbp)
	movq	$0, -64(%rbp)
	call	OPENSSL_sk_find@PLT
	movq	%r15, %rdi
	movl	%eax, %esi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L374
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L374
	movl	$30, %ecx
	leaq	.LC6(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L376
	movq	%rax, %rsi
	movl	$28, %ecx
	leaq	.LC7(%rip), %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L413
.L376:
	movq	%r15, %rdi
	leaq	mime_hdr_free(%rip), %rsi
	call	OPENSSL_sk_pop_free@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	b64_read_asn1
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L414
	movq	-88(%rbp), %rdi
	testq	%rbx, %rbx
	je	.L378
	xorl	%esi, %esi
	call	OPENSSL_sk_value@PLT
	movq	%r12, %rdi
	movq	%rax, (%rbx)
	call	BIO_free@PLT
	movq	-88(%rbp), %rdi
	call	OPENSSL_sk_free@PLT
	jmp	.L362
	.p2align 4,,10
	.p2align 3
.L372:
	movl	$416, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$210, %edx
.L406:
	movl	$212, %esi
	movl	$13, %edi
	xorl	%r15d, %r15d
	call	ERR_put_error@PLT
	movq	BIO_vfree@GOTPCREL(%rip), %rsi
	movq	-88(%rbp), %rdi
	call	OPENSSL_sk_pop_free@PLT
	jmp	.L362
	.p2align 4,,10
	.p2align 3
.L366:
	movq	%r15, %rdi
	leaq	mime_hdr_free(%rip), %rsi
	xorl	%r15d, %r15d
	call	OPENSSL_sk_pop_free@PLT
	movl	$399, %r8d
	movl	$209, %edx
	leaq	.LC0(%rip), %rcx
	movl	$212, %esi
	movl	$13, %edi
	call	ERR_put_error@PLT
	jmp	.L362
	.p2align 4,,10
	.p2align 3
.L408:
	movl	$392, %r8d
	movl	$207, %edx
	movl	$212, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L362
	.p2align 4,,10
	.p2align 3
.L369:
	movq	%r15, %rdi
	leaq	mime_hdr_free(%rip), %rsi
	xorl	%r15d, %r15d
	call	OPENSSL_sk_pop_free@PLT
	movl	$410, %r8d
	movl	$211, %edx
	leaq	.LC0(%rip), %rcx
	movl	$212, %esi
	movl	$13, %edi
	call	ERR_put_error@PLT
	jmp	.L362
	.p2align 4,,10
	.p2align 3
.L410:
	movl	$469, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$205, %edx
	movl	$212, %esi
	movl	$13, %edi
	movq	%r10, -104(%rbp)
	call	ERR_put_error@PLT
	movq	-104(%rbp), %r10
	movl	$2, %edi
	xorl	%eax, %eax
	leaq	.LC8(%rip), %rsi
	movq	8(%r10), %rdx
	call	ERR_add_error_data@PLT
	movq	%r15, %rdi
	leaq	mime_hdr_free(%rip), %rsi
	xorl	%r15d, %r15d
	call	OPENSSL_sk_pop_free@PLT
	jmp	.L362
	.p2align 4,,10
	.p2align 3
.L411:
	movl	$478, %r8d
	movl	$203, %edx
	movl	$212, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L362
	.p2align 4,,10
	.p2align 3
.L378:
	movq	BIO_vfree@GOTPCREL(%rip), %rsi
	call	OPENSSL_sk_pop_free@PLT
	jmp	.L362
.L413:
	movl	$442, %r8d
	movl	$213, %edx
	movl	$212, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	8(%r14), %rdx
	xorl	%eax, %eax
	movl	$2, %edi
	leaq	.LC8(%rip), %rsi
	call	ERR_add_error_data@PLT
	movq	%r15, %rdi
	leaq	mime_hdr_free(%rip), %rsi
	xorl	%r15d, %r15d
	call	OPENSSL_sk_pop_free@PLT
	movq	BIO_vfree@GOTPCREL(%rip), %rsi
	movq	-88(%rbp), %rdi
	call	OPENSSL_sk_pop_free@PLT
	jmp	.L362
.L414:
	movl	$451, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$204, %edx
.L407:
	movl	$212, %esi
	movl	$13, %edi
	call	ERR_put_error@PLT
	movq	BIO_vfree@GOTPCREL(%rip), %rsi
	movq	-88(%rbp), %rdi
	call	OPENSSL_sk_pop_free@PLT
	jmp	.L362
	.p2align 4,,10
	.p2align 3
.L374:
	leaq	mime_hdr_free(%rip), %rsi
	movq	%r15, %rdi
	call	OPENSSL_sk_pop_free@PLT
	movl	$435, %r8d
	movl	$212, %edx
	leaq	.LC0(%rip), %rcx
	jmp	.L406
.L412:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE884:
	.size	SMIME_read_ASN1, .-SMIME_read_ASN1
	.section	.rodata.str1.1
.LC11:
	.string	"Content-Type: text/plain\r\n\r\n"
	.text
	.p2align 4
	.globl	SMIME_crlf_copy
	.type	SMIME_crlf_copy, @function
SMIME_crlf_copy:
.LFB885:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%edx, %r12d
	pushq	%rbx
	subq	$1080, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	BIO_f_buffer@PLT
	movq	%rax, %rdi
	call	BIO_new@PLT
	movq	%rax, %rdi
	movq	%rax, -1112(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L415
	movq	%r13, %rsi
	leaq	-1088(%rbp), %rbx
	call	BIO_push@PLT
	movq	%rax, %r13
	testb	$-128, %r12b
	jne	.L417
	testb	$1, %r12b
	jne	.L469
.L420:
	andl	$524288, %r12d
	leaq	-1088(%rbp), %rbx
	movl	$0, -1100(%rbp)
	movl	%r12d, -1092(%rbp)
	.p2align 4,,10
	.p2align 3
.L421:
	movl	$1024, %edx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	BIO_gets@PLT
	movl	%eax, %r15d
	testl	%eax, %eax
	jle	.L431
	movslq	%r15d, %rax
	xorl	%r12d, %r12d
	leaq	-1(%rbx,%rax), %rax
	jmp	.L426
	.p2align 4,,10
	.p2align 3
.L472:
	testl	%r12d, %r12d
	je	.L423
	cmpb	$32, %dl
	jne	.L423
	movl	-1092(%rbp), %esi
	testl	%esi, %esi
	jne	.L422
.L423:
	cmpb	$13, %dl
	jne	.L470
.L422:
	subq	$1, %rax
	subl	$1, %r15d
	je	.L471
.L426:
	movzbl	(%rax), %edx
	cmpb	$10, %dl
	jne	.L472
	movl	$1, %r12d
	subq	$1, %rax
	subl	$1, %r15d
	jne	.L426
.L471:
	movl	-1092(%rbp), %ecx
	testl	%ecx, %ecx
	je	.L473
	addl	$1, -1100(%rbp)
	jmp	.L421
	.p2align 4,,10
	.p2align 3
.L419:
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	BIO_write@PLT
.L417:
	movl	$1024, %edx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	BIO_read@PLT
	movl	%eax, %edx
	testl	%eax, %eax
	jg	.L419
.L431:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	movq	%r13, %rdi
	call	BIO_ctrl@PLT
	movq	%r13, %rdi
	call	BIO_pop@PLT
	movq	-1112(%rbp), %rdi
	call	BIO_free@PLT
	movl	$1, %eax
.L415:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L474
	addq	$1080, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L470:
	.cfi_restore_state
	movl	-1092(%rbp), %edx
	testl	%edx, %edx
	jne	.L475
.L434:
	movl	%r15d, %edx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	BIO_write@PLT
	testl	%r12d, %r12d
	je	.L421
.L468:
	movl	$2, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r13, %rdi
	call	BIO_write@PLT
	jmp	.L421
	.p2align 4,,10
	.p2align 3
.L473:
	testl	%r12d, %r12d
	je	.L421
	jmp	.L468
	.p2align 4,,10
	.p2align 3
.L475:
	movl	-1100(%rbp), %eax
	xorl	%r9d, %r9d
	testl	%eax, %eax
	je	.L435
	.p2align 4,,10
	.p2align 3
.L428:
	movl	$2, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r13, %rdi
	movl	%r9d, -1096(%rbp)
	call	BIO_write@PLT
	movl	-1096(%rbp), %r9d
	addl	$1, %r9d
	cmpl	%r9d, -1100(%rbp)
	jne	.L428
.L435:
	movl	$0, -1100(%rbp)
	jmp	.L434
.L469:
	movq	%rax, %rdi
	leaq	.LC11(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L420
.L474:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE885:
	.size	SMIME_crlf_copy, .-SMIME_crlf_copy
	.p2align 4
	.globl	i2d_ASN1_bio_stream
	.type	i2d_ASN1_bio_stream, @function
i2d_ASN1_bio_stream:
.LFB877:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testb	$16, %ch
	je	.L477
	movq	%rdx, %r14
	movq	%r8, %rdx
	movl	%ecx, %r12d
	call	BIO_new_NDEF@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L484
	movl	%r12d, %edx
	movq	%rax, %rsi
	movq	%r14, %rdi
	call	SMIME_crlf_copy
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	movq	%rbx, %rdi
	call	BIO_ctrl@PLT
	.p2align 4,,10
	.p2align 3
.L480:
	movq	%rbx, %rdi
	movq	%rbx, %r12
	call	BIO_pop@PLT
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	BIO_free@PLT
	cmpq	%rbx, %r13
	jne	.L480
.L483:
	movl	$1, %eax
.L476:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L477:
	.cfi_restore_state
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	movq	%r8, %rdi
	call	ASN1_item_i2d_bio@PLT
	jmp	.L483
	.p2align 4,,10
	.p2align 3
.L484:
	movl	$75, %r8d
	movl	$65, %edx
	movl	$211, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L476
	.cfi_endproc
.LFE877:
	.size	i2d_ASN1_bio_stream, .-i2d_ASN1_bio_stream
	.p2align 4
	.type	B64_write_ASN1, @function
B64_write_ASN1:
.LFB878:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%ecx, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	%rdx, -56(%rbp)
	call	BIO_f_base64@PLT
	movq	%rax, %rdi
	call	BIO_new@PLT
	testq	%rax, %rax
	je	.L495
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	BIO_push@PLT
	movq	%rax, %r12
	testl	$4096, %r14d
	jne	.L496
	movq	%rbx, %rdx
	movq	%rax, %rsi
	movq	%r15, %rdi
	call	ASN1_item_i2d_bio@PLT
.L494:
	movl	$1, %r14d
.L490:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	movq	%r12, %rdi
	call	BIO_ctrl@PLT
	movq	%r12, %rdi
	call	BIO_pop@PLT
	movq	%r13, %rdi
	call	BIO_free@PLT
.L485:
	addq	$24, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L496:
	.cfi_restore_state
	movq	%rbx, %rsi
	movq	%r15, %rdx
	movq	%rax, %rdi
	call	BIO_new_NDEF@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L497
	movq	-56(%rbp), %rdi
	movl	%r14d, %edx
	movq	%rax, %rsi
	call	SMIME_crlf_copy
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	movq	%rbx, %rdi
	call	BIO_ctrl@PLT
	.p2align 4,,10
	.p2align 3
.L491:
	movq	%rbx, %rdi
	movq	%rbx, %r14
	call	BIO_pop@PLT
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	BIO_free@PLT
	cmpq	%rbx, %r12
	jne	.L491
	jmp	.L494
	.p2align 4,,10
	.p2align 3
.L495:
	movl	$105, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r14d, %r14d
	movl	$210, %esi
	movl	$13, %edi
	call	ERR_put_error@PLT
	jmp	.L485
	.p2align 4,,10
	.p2align 3
.L497:
	movl	$75, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r14d, %r14d
	movl	$211, %esi
	movl	$13, %edi
	call	ERR_put_error@PLT
	jmp	.L490
	.cfi_endproc
.LFE878:
	.size	B64_write_ASN1, .-B64_write_ASN1
	.section	.rodata.str1.1
.LC12:
	.string	"-----BEGIN %s-----\n"
.LC13:
	.string	"-----END %s-----\n"
	.text
	.p2align 4
	.globl	PEM_write_bio_ASN1_stream
	.type	PEM_write_bio_ASN1_stream, @function
PEM_write_bio_ASN1_stream:
.LFB879:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	movq	%r8, %rdx
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	leaq	.LC12(%rip), %rsi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%ecx, %ebx
	subq	$24, %rsp
	movq	%r9, -56(%rbp)
	call	BIO_printf@PLT
	movq	-56(%rbp), %r8
	movl	%ebx, %ecx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	B64_write_ASN1
	movq	%r13, %rdx
	movq	%r12, %rdi
	leaq	.LC13(%rip), %rsi
	movl	%eax, %r14d
	xorl	%eax, %eax
	call	BIO_printf@PLT
	addq	$24, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE879:
	.size	PEM_write_bio_ASN1_stream, .-PEM_write_bio_ASN1_stream
	.section	.rodata.str1.1
.LC14:
	.string	"application/x-pkcs7-"
.LC15:
	.string	"application/pkcs7-"
.LC16:
	.string	"\n"
.LC17:
	.string	"smime.p7m"
.LC18:
	.string	"certs-only"
.LC19:
	.string	"signed-receipt"
.LC20:
	.string	"enveloped-data"
.LC21:
	.string	"signed-data"
.LC22:
	.string	"compressed-data"
.LC23:
	.string	"smime.p7z"
.LC28:
	.string	"MIME-Version: 1.0%s"
	.section	.rodata.str1.8
	.align 8
.LC29:
	.string	"Content-Type: multipart/signed;"
	.section	.rodata.str1.1
.LC30:
	.string	" protocol=\"%ssignature\";"
.LC31:
	.string	" micalg=\""
.LC32:
	.string	","
.LC33:
	.string	"sha1"
.LC34:
	.string	"md5"
.LC35:
	.string	"sha-256"
.LC36:
	.string	"sha-384"
.LC37:
	.string	"sha-512"
.LC38:
	.string	"gostr3411-94"
.LC39:
	.string	"gostr3411-2012-256"
.LC40:
	.string	"gostr3411-2012-512"
.LC41:
	.string	"unknown"
.LC42:
	.string	"\"; boundary=\"----%s\"%s%s"
	.section	.rodata.str1.8
	.align 8
.LC43:
	.string	"This is an S/MIME signed message%s%s"
	.section	.rodata.str1.1
.LC44:
	.string	"------%s%s"
.LC45:
	.string	"%s------%s%s"
.LC46:
	.string	"Content-Type: %ssignature;"
.LC47:
	.string	" name=\"smime.p7s\"%s"
	.section	.rodata.str1.8
	.align 8
.LC48:
	.string	"Content-Transfer-Encoding: base64%s"
	.align 8
.LC49:
	.string	"Content-Disposition: attachment;"
	.section	.rodata.str1.1
.LC50:
	.string	" filename=\"smime.p7s\"%s%s"
.LC51:
	.string	"%s------%s--%s%s"
.LC52:
	.string	" name=\"%s\"%s"
	.section	.rodata.str1.8
	.align 8
.LC53:
	.string	"Content-Transfer-Encoding: base64%s%s"
	.section	.rodata.str1.1
.LC54:
	.string	"%s"
.LC55:
	.string	" filename=\"%s\"%s"
.LC56:
	.string	"Content-Type: %smime;"
.LC57:
	.string	" smime-type=%s;"
	.text
	.p2align 4
	.globl	SMIME_write_ASN1
	.type	SMIME_write_ASN1, @function
SMIME_write_ASN1:
.LFB882:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%ecx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	leaq	.LC2(%rip), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$168, %rsp
	movq	24(%rbp), %rax
	movq	16(%rbp), %r15
	movq	%rdx, -176(%rbp)
	movq	%rsi, -160(%rbp)
	leaq	.LC14(%rip), %rdx
	movq	%rax, -152(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testb	$4, %ch
	leaq	.LC15(%rip), %rax
	cmovne	%rdx, %rax
	testb	$8, %ch
	movq	%rax, -168(%rbp)
	leaq	.LC16(%rip), %rax
	cmove	%rax, %r13
	andl	$64, %ecx
	je	.L503
	testq	%rbx, %rbx
	jne	.L576
.L503:
	cmpl	$23, %r8d
	je	.L540
	cmpl	$22, %r8d
	je	.L577
	cmpl	$786, %r8d
	je	.L578
	movq	%r13, %rdx
	leaq	.LC28(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	leaq	.LC49(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	%r13, %rcx
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC17(%rip), %rdx
	leaq	.LC55(%rip), %rsi
	call	BIO_printf@PLT
	movq	-168(%rbp), %rdx
	leaq	.LC56(%rip), %rsi
	xorl	%eax, %eax
	movq	%r12, %rdi
	leaq	.LC17(%rip), %rbx
	call	BIO_printf@PLT
.L535:
	movq	%r13, %rcx
	movq	%rbx, %rdx
	leaq	.LC52(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	%r13, %rcx
	movq	%r13, %rdx
	movq	%r12, %rdi
	leaq	.LC53(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	-152(%rbp), %r8
	movl	%r14d, %ecx
	movq	%r12, %rdi
	movq	-176(%rbp), %rdx
	movq	-160(%rbp), %rsi
	call	B64_write_ASN1
	testl	%eax, %eax
	je	.L575
	movq	%r13, %rdx
	leaq	.LC54(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movl	$1, %eax
.L500:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L579
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L576:
	.cfi_restore_state
	leaq	-96(%rbp), %rax
	movl	$32, %esi
	movq	%rax, %rdi
	movq	%rax, -192(%rbp)
	call	RAND_bytes@PLT
	testl	%eax, %eax
	jle	.L575
	movdqa	.LC24(%rip), %xmm0
	movdqa	-96(%rbp), %xmm1
	movq	%r13, %rdx
	leaq	.LC28(%rip), %rsi
	movdqa	.LC25(%rip), %xmm5
	movq	%r12, %rdi
	xorl	%eax, %eax
	xorl	%ebx, %ebx
	pand	%xmm0, %xmm1
	movdqa	.LC26(%rip), %xmm3
	pand	-80(%rbp), %xmm0
	movb	$0, -64(%rbp)
	movdqa	.LC27(%rip), %xmm2
	movdqa	%xmm1, %xmm6
	movdqa	%xmm1, %xmm4
	paddb	%xmm3, %xmm6
	pcmpgtb	%xmm5, %xmm1
	paddb	%xmm0, %xmm3
	paddb	%xmm2, %xmm4
	paddb	%xmm0, %xmm2
	pcmpgtb	%xmm5, %xmm0
	pand	%xmm1, %xmm4
	pandn	%xmm6, %xmm1
	pand	%xmm0, %xmm2
	pandn	%xmm3, %xmm0
	por	%xmm4, %xmm1
	por	%xmm2, %xmm0
	movaps	%xmm1, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	BIO_printf@PLT
	leaq	.LC29(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	-168(%rbp), %rdx
	leaq	.LC30(%rip), %rsi
	xorl	%eax, %eax
	movq	%r12, %rdi
	call	BIO_printf@PLT
	leaq	.LC31(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_puts@PLT
	movq	%r13, -200(%rbp)
	xorl	%edx, %edx
	movl	$0, -204(%rbp)
.L506:
	movq	%r15, %rdi
	movl	%edx, -184(%rbp)
	call	OPENSSL_sk_num@PLT
	movl	-184(%rbp), %edx
	cmpl	%eax, %ebx
	jge	.L572
	testl	%edx, %edx
	jne	.L580
.L507:
	movl	%ebx, %esi
	movq	%r15, %rdi
	call	OPENSSL_sk_value@PLT
	movq	(%rax), %rdi
	call	OBJ_obj2nid@PLT
	movl	%eax, %edi
	movl	%eax, %r13d
	call	OBJ_nid2sn@PLT
	movq	%rax, %rdi
	call	EVP_get_digestbyname@PLT
	testq	%rax, %rax
	je	.L508
	movq	72(%rax), %rax
	testq	%rax, %rax
	je	.L508
	xorl	%edx, %edx
	xorl	%edi, %edi
	leaq	-128(%rbp), %rcx
	movl	$2, %esi
	call	*%rax
	testl	%eax, %eax
	jg	.L581
	cmpl	$-2, %eax
	jne	.L572
.L508:
	cmpl	$674, %r13d
	je	.L513
	jg	.L514
	cmpl	$672, %r13d
	je	.L515
	cmpl	$673, %r13d
	jne	.L582
	leaq	.LC36(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_puts@PLT
	movl	$1, %edx
	.p2align 4,,10
	.p2align 3
.L510:
	addl	$1, %ebx
	jmp	.L506
	.p2align 4,,10
	.p2align 3
.L540:
	leaq	.LC20(%rip), %r8
	leaq	.LC17(%rip), %rbx
.L532:
	movq	%r13, %rdx
	movq	%r12, %rdi
	leaq	.LC28(%rip), %rsi
	xorl	%eax, %eax
	movq	%r8, -184(%rbp)
	call	BIO_printf@PLT
	movq	%r12, %rdi
	leaq	.LC49(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	%r13, %rcx
	movq	%rbx, %rdx
	movq	%r12, %rdi
	leaq	.LC55(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	-168(%rbp), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC56(%rip), %rsi
	call	BIO_printf@PLT
	movq	-184(%rbp), %r8
	leaq	.LC57(%rip), %rsi
	xorl	%eax, %eax
	movq	%r12, %rdi
	movq	%r8, %rdx
	call	BIO_printf@PLT
	jmp	.L535
	.p2align 4,,10
	.p2align 3
.L514:
	cmpl	$982, %r13d
	je	.L520
	cmpl	$983, %r13d
	jne	.L583
	leaq	.LC40(%rip), %rsi
	movq	%r12, %rdi
	movq	-200(%rbp), %r13
	call	BIO_puts@PLT
.L512:
	movq	-192(%rbp), %rbx
	movq	%r13, %r8
	movq	%r13, %rcx
	movq	%r12, %rdi
	leaq	.LC42(%rip), %rsi
	xorl	%eax, %eax
	movq	%rbx, %rdx
	call	BIO_printf@PLT
	movq	%r13, %rcx
	movq	%r13, %rdx
	movq	%r12, %rdi
	leaq	.LC43(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	%r13, %rcx
	movq	%rbx, %rdx
	movq	%r12, %rdi
	leaq	.LC44(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	-160(%rbp), %rax
	movq	%rax, -136(%rbp)
	movl	%r14d, %eax
	andl	$32832, %eax
	cmpl	$64, %eax
	jne	.L584
	movq	-152(%rbp), %rax
	movq	32(%rax), %rbx
	testq	%rbx, %rbx
	je	.L526
	movq	24(%rbx), %rax
	testq	%rax, %rax
	je	.L526
	leaq	-128(%rbp), %rcx
	leaq	-136(%rbp), %r15
	movq	%r12, -128(%rbp)
	movq	-152(%rbp), %rdx
	movq	%rcx, -184(%rbp)
	movq	%r15, %rsi
	movl	$12, %edi
	movq	$0, -120(%rbp)
	movq	$0, -112(%rbp)
	call	*%rax
	movq	-184(%rbp), %rcx
	testl	%eax, %eax
	jle	.L575
	movq	-120(%rbp), %rsi
	movq	-176(%rbp), %rdi
	movl	%r14d, %edx
	movq	%rcx, -184(%rbp)
	call	SMIME_crlf_copy
	movq	-184(%rbp), %rcx
	movq	%r15, %rsi
	movq	-152(%rbp), %rdx
	movl	$13, %edi
	call	*24(%rbx)
	movq	-120(%rbp), %r14
	movl	%eax, %ebx
	cmpq	%r14, %r12
	je	.L531
	.p2align 4,,10
	.p2align 3
.L529:
	movq	%r14, %rdi
	call	BIO_pop@PLT
	movq	-120(%rbp), %rdi
	movq	%rax, %r14
	call	BIO_free@PLT
	movq	%r14, -120(%rbp)
	cmpq	%r14, %r12
	jne	.L529
.L531:
	testl	%ebx, %ebx
	jg	.L525
	.p2align 4,,10
	.p2align 3
.L575:
	xorl	%eax, %eax
	jmp	.L500
.L584:
	movq	-176(%rbp), %rdi
	movl	%r14d, %edx
	movq	%r12, %rsi
	call	SMIME_crlf_copy
.L525:
	movq	-192(%rbp), %rbx
	movq	%r13, %r8
	movq	%r13, %rdx
	movq	%r12, %rdi
	leaq	.LC45(%rip), %rsi
	xorl	%eax, %eax
	movq	%rbx, %rcx
	call	BIO_printf@PLT
	movq	-168(%rbp), %rdx
	leaq	.LC46(%rip), %rsi
	xorl	%eax, %eax
	movq	%r12, %rdi
	call	BIO_printf@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC47(%rip), %rsi
	call	BIO_printf@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC48(%rip), %rsi
	call	BIO_printf@PLT
	leaq	.LC49(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	%r13, %rcx
	movq	%r13, %rdx
	movq	%r12, %rdi
	leaq	.LC50(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	-152(%rbp), %r8
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	-160(%rbp), %rsi
	movq	%r12, %rdi
	call	B64_write_ASN1
	movq	%r13, %r9
	movq	%r13, %r8
	movq	%rbx, %rcx
	movq	%r13, %rdx
	leaq	.LC51(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movl	$1, %eax
	jmp	.L500
.L583:
	cmpl	$809, %r13d
	jne	.L519
	leaq	.LC38(%rip), %rsi
	movq	%r12, %rdi
	movq	-200(%rbp), %r13
	call	BIO_puts@PLT
	jmp	.L512
	.p2align 4,,10
	.p2align 3
.L582:
	cmpl	$4, %r13d
	je	.L517
	cmpl	$64, %r13d
	jne	.L519
	leaq	.LC33(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_puts@PLT
	movl	$1, %edx
	jmp	.L510
	.p2align 4,,10
	.p2align 3
.L580:
	movl	$1, %edx
	leaq	.LC32(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_write@PLT
	jmp	.L507
	.p2align 4,,10
	.p2align 3
.L577:
	leaq	.LC19(%rip), %r8
	leaq	.LC17(%rip), %rbx
	cmpl	$204, %r9d
	je	.L532
	movq	%r15, %rdi
	call	OPENSSL_sk_num@PLT
	leaq	.LC21(%rip), %r8
	testl	%eax, %eax
	leaq	.LC18(%rip), %rax
	cmovs	%rax, %r8
	jmp	.L532
	.p2align 4,,10
	.p2align 3
.L578:
	leaq	.LC22(%rip), %r8
	leaq	.LC23(%rip), %rbx
	jmp	.L532
	.p2align 4,,10
	.p2align 3
.L581:
	movq	-128(%rbp), %rsi
	movq	%r12, %rdi
	call	BIO_puts@PLT
	movq	-128(%rbp), %rdi
	movl	$170, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movl	$1, %edx
	jmp	.L510
	.p2align 4,,10
	.p2align 3
.L520:
	leaq	.LC39(%rip), %rsi
	movq	%r12, %rdi
	movq	-200(%rbp), %r13
	call	BIO_puts@PLT
	jmp	.L512
	.p2align 4,,10
	.p2align 3
.L515:
	leaq	.LC35(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_puts@PLT
	movl	$1, %edx
	jmp	.L510
.L519:
	movl	-204(%rbp), %eax
	xorl	%edx, %edx
	testl	%eax, %eax
	jne	.L510
	leaq	.LC41(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_puts@PLT
	movl	$1, %edx
	movl	$1, -204(%rbp)
	jmp	.L510
.L513:
	leaq	.LC37(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_puts@PLT
	movl	$1, %edx
	jmp	.L510
	.p2align 4,,10
	.p2align 3
.L517:
	leaq	.LC34(%rip), %rsi
	movq	%r12, %rdi
	call	BIO_puts@PLT
	movl	$1, %edx
	jmp	.L510
.L526:
	movl	$340, %r8d
	movl	$202, %edx
	movl	$214, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L500
.L572:
	movq	-200(%rbp), %r13
	jmp	.L512
.L579:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE882:
	.size	SMIME_write_ASN1, .-SMIME_write_ASN1
	.section	.rodata.str1.1
.LC58:
	.string	"text/plain"
	.text
	.p2align 4
	.globl	SMIME_text
	.type	SMIME_text, @function
SMIME_text:
.LFB886:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$48, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%rdi, %rbx
	movq	%rsi, %r13
	call	mime_parse_hdr
	testq	%rax, %rax
	je	.L598
	movq	%rax, %r12
	leaq	-4176(%rbp), %rsi
	leaq	.LC3(%rip), %rax
	movq	$0, -4168(%rbp)
	movq	%r12, %rdi
	movq	%rax, -4176(%rbp)
	movq	$0, -4160(%rbp)
	call	OPENSSL_sk_find@PLT
	movq	%r12, %rdi
	movl	%eax, %esi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L588
	movq	8(%rax), %rsi
	testq	%rsi, %rsi
	je	.L588
	movl	$11, %ecx
	leaq	.LC58(%rip), %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L599
	movq	%r12, %rdi
	leaq	mime_hdr_free(%rip), %rsi
	leaq	-4144(%rbp), %r12
	call	OPENSSL_sk_pop_free@PLT
	jmp	.L591
	.p2align 4,,10
	.p2align 3
.L592:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	BIO_write@PLT
.L591:
	movl	$4096, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	BIO_read@PLT
	movl	%eax, %edx
	testl	%eax, %eax
	jg	.L592
	sete	%al
	movzbl	%al, %eax
.L585:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L600
	addq	$4144, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L599:
	.cfi_restore_state
	movl	$551, %r8d
	movl	$205, %edx
	movl	$213, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	8(%r14), %rdx
	xorl	%eax, %eax
	movl	$2, %edi
	leaq	.LC8(%rip), %rsi
	call	ERR_add_error_data@PLT
	leaq	mime_hdr_free(%rip), %rsi
	movq	%r12, %rdi
	call	OPENSSL_sk_pop_free@PLT
	xorl	%eax, %eax
	jmp	.L585
	.p2align 4,,10
	.p2align 3
.L588:
	movl	$546, %r8d
	movl	$206, %edx
	movl	$213, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	leaq	mime_hdr_free(%rip), %rsi
	movq	%r12, %rdi
	call	OPENSSL_sk_pop_free@PLT
	xorl	%eax, %eax
	jmp	.L585
	.p2align 4,,10
	.p2align 3
.L598:
	movl	$541, %r8d
	movl	$207, %edx
	movl	$213, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L585
.L600:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE886:
	.size	SMIME_text, .-SMIME_text
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC24:
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.align 16
.LC25:
	.byte	9
	.byte	9
	.byte	9
	.byte	9
	.byte	9
	.byte	9
	.byte	9
	.byte	9
	.byte	9
	.byte	9
	.byte	9
	.byte	9
	.byte	9
	.byte	9
	.byte	9
	.byte	9
	.align 16
.LC26:
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.align 16
.LC27:
	.byte	55
	.byte	55
	.byte	55
	.byte	55
	.byte	55
	.byte	55
	.byte	55
	.byte	55
	.byte	55
	.byte	55
	.byte	55
	.byte	55
	.byte	55
	.byte	55
	.byte	55
	.byte	55
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
