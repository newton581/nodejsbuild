	.file	"dsa_ossl.c"
	.text
	.p2align 4
	.type	dsa_init, @function
dsa_init:
.LFB428:
	.cfi_startproc
	endbr64
	orl	$1, 48(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE428:
	.size	dsa_init, .-dsa_init
	.p2align 4
	.type	dsa_finish, @function
dsa_finish:
.LFB429:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	56(%rdi), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	BN_MONT_CTX_free@PLT
	movl	$1, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE429:
	.size	dsa_finish, .-dsa_finish
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/dsa/dsa_ossl.c"
	.text
	.p2align 4
	.type	dsa_do_verify, @function
dsa_do_verify:
.LFB427:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 8(%rcx)
	je	.L6
	movq	%rdi, %r13
	movq	16(%rcx), %rdi
	movq	%rcx, %r15
	testq	%rdi, %rdi
	je	.L6
	cmpq	$0, 24(%rcx)
	je	.L6
	movl	%esi, %ebx
	movq	%rdx, %r12
	call	BN_num_bits@PLT
	movl	%eax, %r14d
	andl	$-65, %eax
	cmpl	$160, %eax
	je	.L9
	cmpl	$256, %r14d
	jne	.L57
.L9:
	movq	8(%r15), %rdi
	call	BN_num_bits@PLT
	cmpl	$10000, %eax
	jg	.L58
	call	BN_new@PLT
	movq	%rax, -88(%rbp)
	call	BN_new@PLT
	movq	%rax, -96(%rbp)
	call	BN_new@PLT
	movq	%rax, -104(%rbp)
	call	BN_CTX_new@PLT
	cmpq	$0, -88(%rbp)
	movq	%rax, %r11
	je	.L11
	cmpq	$0, -96(%rbp)
	je	.L11
	cmpq	$0, -104(%rbp)
	je	.L11
	testq	%rax, %rax
	je	.L11
	leaq	-64(%rbp), %rdx
	leaq	-72(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -112(%rbp)
	call	DSA_SIG_get0@PLT
	movq	-72(%rbp), %rdi
	call	BN_is_zero@PLT
	movq	-112(%rbp), %r11
	testl	%eax, %eax
	je	.L12
.L14:
	xorl	%r12d, %r12d
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L6:
	movl	$308, %r8d
	movl	$101, %edx
	movl	$113, %esi
	movl	$10, %edi
	leaq	.LC0(%rip), %rcx
	movl	$-1, %r12d
	call	ERR_put_error@PLT
.L5:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L59
	leaq	-40(%rbp), %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L57:
	.cfi_restore_state
	movl	$315, %r8d
	movl	$102, %edx
	movl	$113, %esi
	movl	$10, %edi
	leaq	.LC0(%rip), %rcx
	movl	$-1, %r12d
	call	ERR_put_error@PLT
	jmp	.L5
.L16:
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %r8
	pushq	%rax
	pushq	%r11
	movq	-104(%rbp), %rdi
	movq	%r11, -112(%rbp)
	call	BN_mod_exp2_mont@PLT
	popq	%rdx
	movq	-112(%rbp), %r11
	testl	%eax, %eax
	popq	%rcx
	jne	.L18
	.p2align 4,,10
	.p2align 3
.L11:
	movl	$396, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$3, %edx
	movl	$113, %esi
	movl	$10, %edi
	movq	%r11, -112(%rbp)
	movl	$-1, %r12d
	call	ERR_put_error@PLT
	movq	-112(%rbp), %r11
.L13:
	movq	%r11, %rdi
	call	BN_CTX_free@PLT
	movq	-88(%rbp), %rdi
	call	BN_free@PLT
	movq	-96(%rbp), %rdi
	call	BN_free@PLT
	movq	-104(%rbp), %rdi
	call	BN_free@PLT
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L58:
	movl	$320, %r8d
	movl	$103, %edx
	movl	$113, %esi
	movl	$10, %edi
	leaq	.LC0(%rip), %rcx
	movl	$-1, %r12d
	call	ERR_put_error@PLT
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L12:
	movq	-72(%rbp), %rdi
	movq	%r11, -112(%rbp)
	call	BN_is_negative@PLT
	movq	-112(%rbp), %r11
	testl	%eax, %eax
	jne	.L14
	movq	16(%r15), %rsi
	movq	-72(%rbp), %rdi
	call	BN_ucmp@PLT
	movq	-112(%rbp), %r11
	testl	%eax, %eax
	jns	.L14
	movq	-64(%rbp), %rdi
	call	BN_is_zero@PLT
	movq	-112(%rbp), %r11
	testl	%eax, %eax
	jne	.L14
	movq	-64(%rbp), %rdi
	call	BN_is_negative@PLT
	movq	-112(%rbp), %r11
	testl	%eax, %eax
	jne	.L14
	movq	16(%r15), %rsi
	movq	-64(%rbp), %rdi
	call	BN_ucmp@PLT
	movq	-112(%rbp), %r11
	testl	%eax, %eax
	jns	.L14
	movq	-96(%rbp), %r12
	movq	16(%r15), %rdx
	movq	%r11, %rcx
	movq	-64(%rbp), %rsi
	movq	%r12, %rdi
	call	BN_mod_inverse@PLT
	movq	-112(%rbp), %r11
	testq	%rax, %rax
	je	.L11
	sarl	$3, %r14d
	movq	%r13, %rdi
	cmpl	%ebx, %r14d
	cmovle	%r14d, %ebx
	movl	%ebx, %esi
	movq	-88(%rbp), %rbx
	movq	%rbx, %rdx
	call	BN_bin2bn@PLT
	movq	-112(%rbp), %r11
	testq	%rax, %rax
	je	.L11
	movq	16(%r15), %rcx
	movq	%r11, %r8
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movq	%rbx, %rdi
	call	BN_mod_mul@PLT
	movq	-112(%rbp), %r11
	testl	%eax, %eax
	je	.L11
	movq	16(%r15), %rcx
	movq	-72(%rbp), %rsi
	movq	%r11, %r8
	movq	%r12, %rdx
	movq	%r12, %rdi
	call	BN_mod_mul@PLT
	movq	-112(%rbp), %r11
	testl	%eax, %eax
	je	.L11
	xorl	%eax, %eax
	testb	$1, 48(%r15)
	jne	.L60
.L15:
	movq	80(%r15), %rdx
	movq	8(%r15), %r9
	movq	32(%r15), %rcx
	movq	24(%r15), %rsi
	movq	32(%rdx), %rbx
	testq	%rbx, %rbx
	je	.L16
	pushq	%rdi
	movq	%rcx, %r8
	movq	%rsi, %rdx
	movq	-88(%rbp), %rcx
	pushq	%rax
	movq	-104(%rbp), %rsi
	movq	%r15, %rdi
	pushq	%r11
	pushq	%r9
	movq	-96(%rbp), %r9
	movq	%r11, -112(%rbp)
	call	*%rbx
	movq	-112(%rbp), %r11
	addq	$32, %rsp
	testl	%eax, %eax
	je	.L11
.L18:
	movq	16(%r15), %rcx
	movq	-104(%rbp), %rdx
	movq	%r11, %r8
	xorl	%edi, %edi
	movq	-88(%rbp), %rsi
	movq	%r11, -112(%rbp)
	call	BN_div@PLT
	movq	-112(%rbp), %r11
	testl	%eax, %eax
	je	.L11
	movq	-72(%rbp), %rsi
	movq	-88(%rbp), %rdi
	movq	%r11, -112(%rbp)
	xorl	%r12d, %r12d
	call	BN_ucmp@PLT
	movq	-112(%rbp), %r11
	testl	%eax, %eax
	sete	%r12b
	jmp	.L13
.L59:
	call	__stack_chk_fail@PLT
.L60:
	movq	8(%r15), %rdx
	movq	96(%r15), %rsi
	movq	%r11, %rcx
	leaq	56(%r15), %rdi
	call	BN_MONT_CTX_set_locked@PLT
	movq	-112(%rbp), %r11
	testq	%rax, %rax
	jne	.L15
	jmp	.L11
	.cfi_endproc
.LFE427:
	.size	dsa_do_verify, .-dsa_do_verify
	.p2align 4
	.type	dsa_do_sign, @function
dsa_do_sign:
.LFB424:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdx), %r15
	movq	%rdi, -64(%rbp)
	movl	%esi, -52(%rbp)
	testq	%r15, %r15
	je	.L91
	movq	16(%rdx), %r15
	movq	%rdx, %rbx
	testq	%r15, %r15
	je	.L91
	movq	24(%rdx), %r15
	testq	%r15, %r15
	je	.L91
	movq	40(%rdx), %r15
	testq	%r15, %r15
	je	.L92
	call	DSA_SIG_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L93
	call	BN_new@PLT
	movq	%rax, (%r12)
	call	BN_new@PLT
	cmpq	$0, (%r12)
	movq	%rax, 8(%r12)
	je	.L94
	testq	%rax, %rax
	je	.L94
	call	BN_CTX_new@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L96
	movq	%rax, %rdi
	call	BN_CTX_get@PLT
	movq	%r15, %rdi
	movq	%rax, -80(%rbp)
	call	BN_CTX_get@PLT
	movq	%r15, %rdi
	movq	%rax, %r13
	call	BN_CTX_get@PLT
	movq	%r15, %rdi
	movq	%rax, -88(%rbp)
	call	BN_CTX_get@PLT
	movq	%rax, -112(%rbp)
	testq	%rax, %rax
	je	.L96
	movq	(%r12), %rax
	movq	8(%rbx), %rdi
	movq	$0, -72(%rbp)
	movq	%rax, -120(%rbp)
	testq	%rdi, %rdi
	je	.L63
	leaq	56(%rbx), %rax
	movq	%rax, -128(%rbp)
.L64:
	cmpq	$0, 16(%rbx)
	je	.L63
	cmpq	$0, 24(%rbx)
	je	.L63
	call	BN_is_zero@PLT
	testl	%eax, %eax
	jne	.L67
	movq	16(%rbx), %rdi
	call	BN_is_zero@PLT
	testl	%eax, %eax
	jne	.L67
	movq	24(%rbx), %rdi
	call	BN_is_zero@PLT
	testl	%eax, %eax
	jne	.L67
	cmpq	$0, 40(%rbx)
	je	.L171
	call	BN_new@PLT
	movq	%rax, %r14
	call	BN_new@PLT
	movq	%rax, -104(%rbp)
	testq	%r14, %r14
	je	.L69
	testq	%rax, %rax
	je	.L69
	movq	16(%rbx), %rdi
	call	BN_num_bits@PLT
	movq	16(%rbx), %rdi
	movl	%eax, -136(%rbp)
	call	bn_get_top@PLT
	movq	%r14, %rdi
	leal	2(%rax), %ecx
	movl	%ecx, %esi
	movl	%ecx, -96(%rbp)
	call	bn_wexpand@PLT
	testq	%rax, %rax
	je	.L165
	movl	-96(%rbp), %esi
	movq	-104(%rbp), %rdi
	call	bn_wexpand@PLT
	testq	%rax, %rax
	je	.L165
	cmpq	$0, -64(%rbp)
	movslq	-52(%rbp), %rax
	je	.L72
	movq	%r13, -152(%rbp)
	movq	-64(%rbp), %r13
	movq	%r12, -144(%rbp)
	movq	%rbx, %r12
	movq	%rax, %rbx
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L173:
	movq	%r14, %rdi
	call	BN_is_zero@PLT
	testl	%eax, %eax
	je	.L172
.L71:
	movq	40(%r12), %rdx
	movq	16(%r12), %rsi
	movq	%r15, %r9
	movq	%rbx, %r8
	movq	%r13, %rcx
	movq	%r14, %rdi
	call	BN_generate_dsa_nonce@PLT
	testl	%eax, %eax
	jne	.L173
	movq	-144(%rbp), %r12
	movq	%r14, %rbx
.L81:
	movl	$10, %edi
	movl	$291, %r8d
	movl	$3, %edx
	movl	$107, %esi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	%rbx, %rdi
.L170:
	call	BN_clear_free@PLT
	movq	-104(%rbp), %rdi
	call	BN_clear_free@PLT
	movq	-72(%rbp), %r14
	movl	$3, %edx
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L91:
	xorl	%r14d, %r14d
	xorl	%r12d, %r12d
	movl	$101, %edx
.L62:
	movl	$167, %r8d
	movl	$112, %esi
	movl	$10, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	DSA_SIG_free@PLT
.L86:
	movq	%r15, %rdi
	call	BN_CTX_free@PLT
	movq	%r14, %rdi
	call	BN_clear_free@PLT
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L92:
	.cfi_restore_state
	xorl	%r14d, %r14d
	xorl	%r12d, %r12d
	movl	$111, %edx
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L94:
	xorl	%r14d, %r14d
	movl	$3, %edx
	xorl	%r15d, %r15d
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L67:
	movl	$199, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$112, %edx
.L169:
	movl	$107, %esi
	movl	$10, %edi
	call	ERR_put_error@PLT
	movq	-72(%rbp), %r14
	movl	$3, %edx
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L93:
	xorl	%r14d, %r14d
	xorl	%r15d, %r15d
	movl	$3, %edx
	jmp	.L62
.L98:
	movq	-96(%rbp), %r14
	movq	%r14, -72(%rbp)
	.p2align 4,,10
	.p2align 3
.L63:
	movl	$193, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$101, %edx
	jmp	.L169
	.p2align 4,,10
	.p2align 3
.L96:
	xorl	%r14d, %r14d
	movl	$3, %edx
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L174:
	movq	%r14, %rdi
	call	BN_is_zero@PLT
	testl	%eax, %eax
	je	.L73
.L72:
	movq	16(%rbx), %rsi
	movq	%r14, %rdi
	call	BN_priv_rand_range@PLT
	testl	%eax, %eax
	jne	.L174
.L165:
	movq	%r14, %rbx
	jmp	.L81
.L69:
	movl	$291, %r8d
	movl	$3, %edx
	movl	$107, %esi
	movl	$10, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%edi, %edi
	call	BN_CTX_free@PLT
	movq	%r14, %rdi
	jmp	.L170
.L171:
	movl	$203, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$111, %edx
	jmp	.L169
.L172:
	movq	%r12, %rbx
	movq	-152(%rbp), %r13
	movq	-144(%rbp), %r12
.L73:
	movl	$4, %esi
	movq	%r14, %rdi
	call	BN_set_flags@PLT
	movq	-104(%rbp), %rdi
	movl	$4, %esi
	call	BN_set_flags@PLT
	testb	$1, 48(%rbx)
	je	.L76
	movq	8(%rbx), %rdx
	movq	96(%rbx), %rsi
	movq	%r15, %rcx
	movq	-128(%rbp), %rdi
	call	BN_MONT_CTX_set_locked@PLT
	testq	%rax, %rax
	je	.L165
.L76:
	movq	16(%rbx), %rdx
	movq	-104(%rbp), %rdi
	movq	%r14, %rsi
	call	BN_add@PLT
	testl	%eax, %eax
	je	.L165
	movq	16(%rbx), %rdx
	movq	-104(%rbp), %rsi
	movq	%r14, %rdi
	call	BN_add@PLT
	testl	%eax, %eax
	je	.L165
	movl	-136(%rbp), %esi
	movq	-104(%rbp), %rdi
	call	BN_is_bit_set@PLT
	movl	-96(%rbp), %ecx
	movq	-104(%rbp), %rdx
	movq	%r14, %rsi
	movslq	%eax, %rdi
	call	BN_consttime_swap@PLT
	movq	80(%rbx), %rax
	movq	40(%rax), %rax
	testq	%rax, %rax
	je	.L77
	movq	24(%rbx), %rdx
	movq	-120(%rbp), %rsi
	movq	%rbx, %rdi
	movq	%r15, %r9
	pushq	%rcx
	movq	%r14, %rcx
	movq	8(%rbx), %r8
	pushq	56(%rbx)
	call	*%rax
	popq	%rsi
	popq	%rdi
	testl	%eax, %eax
	je	.L165
.L79:
	movq	-120(%rbp), %rdx
	movq	16(%rbx), %rcx
	xorl	%edi, %edi
	movq	%r15, %r8
	movq	%rdx, %rsi
	call	BN_div@PLT
	testl	%eax, %eax
	je	.L165
	movq	16(%rbx), %rax
	movq	%rax, -120(%rbp)
	call	BN_new@PLT
	movq	%rax, -96(%rbp)
	testq	%rax, %rax
	je	.L165
	movq	%r15, %rdi
	call	BN_CTX_start@PLT
	movq	%r15, %rdi
	call	BN_CTX_get@PLT
	testq	%rax, %rax
	je	.L167
	movq	-96(%rbp), %rdi
	movl	$2, %esi
	movq	%rax, -136(%rbp)
	call	BN_set_word@PLT
	movq	-136(%rbp), %r10
	testl	%eax, %eax
	jne	.L175
.L167:
	movq	%r14, %rbx
	movq	-96(%rbp), %r14
	movq	%r14, %rdi
	call	BN_free@PLT
	movq	%r15, %rdi
	call	BN_CTX_end@PLT
	jmp	.L81
.L77:
	movq	8(%rbx), %rcx
	movq	24(%rbx), %rsi
	movq	%r15, %r8
	movq	%r14, %rdx
	movq	56(%rbx), %r9
	movq	-120(%rbp), %rdi
	call	BN_mod_exp_mont@PLT
	testl	%eax, %eax
	jne	.L79
	jmp	.L165
.L175:
	movq	-96(%rbp), %rdx
	movq	-120(%rbp), %rsi
	movq	%r10, %rdi
	call	BN_sub@PLT
	movq	-136(%rbp), %r10
	testl	%eax, %eax
	je	.L167
	movq	-120(%rbp), %rcx
	movq	-96(%rbp), %rdi
	xorl	%r9d, %r9d
	movq	%r15, %r8
	movq	%r10, %rdx
	movq	%r14, %rsi
	call	BN_mod_exp_mont@PLT
	testl	%eax, %eax
	je	.L167
	movq	%r15, %rdi
	call	BN_CTX_end@PLT
	movq	-72(%rbp), %rdi
	call	BN_clear_free@PLT
	movq	%r14, %rdi
	call	BN_clear_free@PLT
	movq	-104(%rbp), %rdi
	call	BN_clear_free@PLT
	movq	16(%rbx), %rdi
	call	BN_num_bits@PLT
	leal	14(%rax), %edx
	addl	$7, %eax
	cmovs	%edx, %eax
	sarl	$3, %eax
	cmpl	-52(%rbp), %eax
	jl	.L176
.L88:
	movq	-80(%rbp), %rdx
	movl	-52(%rbp), %esi
	movq	-64(%rbp), %rdi
	call	BN_bin2bn@PLT
	testq	%rax, %rax
	jne	.L84
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L178:
	movq	%r13, %rdi
	call	BN_is_zero@PLT
	testl	%eax, %eax
	je	.L177
.L84:
	movq	16(%rbx), %rdi
	call	BN_num_bits@PLT
	xorl	%ecx, %ecx
	movl	$-1, %edx
	movq	%r13, %rdi
	leal	-1(%rax), %esi
	call	BN_priv_rand@PLT
	testl	%eax, %eax
	jne	.L178
.L107:
	movq	-96(%rbp), %r14
	movl	$3, %edx
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L177:
	movl	$4, %esi
	movq	%r13, %rdi
	call	BN_set_flags@PLT
	movq	-88(%rbp), %rdi
	movl	$4, %esi
	call	BN_set_flags@PLT
	movq	-112(%rbp), %r14
	movl	$4, %esi
	movq	%r14, %rdi
	call	BN_set_flags@PLT
	movq	16(%rbx), %rcx
	movq	40(%rbx), %rdx
	movq	%r15, %r8
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	BN_mod_mul@PLT
	testl	%eax, %eax
	je	.L107
	movq	16(%rbx), %rcx
	movq	(%r12), %rdx
	movq	%r15, %r8
	movq	%r14, %rsi
	movq	%r14, %rdi
	call	BN_mod_mul@PLT
	testl	%eax, %eax
	je	.L107
	movq	16(%rbx), %rcx
	movq	-80(%rbp), %rdx
	movq	%r15, %r8
	movq	%r13, %rsi
	movq	-88(%rbp), %rdi
	call	BN_mod_mul@PLT
	testl	%eax, %eax
	je	.L107
	movq	16(%rbx), %rcx
	movq	8(%r12), %rdi
	movq	%r14, %rsi
	movq	-88(%rbp), %rdx
	call	BN_mod_add_quick@PLT
	testl	%eax, %eax
	je	.L107
	movq	8(%r12), %rdi
	movq	16(%rbx), %rcx
	movq	%r15, %r8
	movq	-96(%rbp), %rdx
	movq	%rdi, %rsi
	call	BN_mod_mul@PLT
	testl	%eax, %eax
	je	.L107
	movq	16(%rbx), %rdx
	movq	%r15, %rcx
	movq	%r13, %rsi
	movq	%r13, %rdi
	call	BN_mod_inverse@PLT
	testq	%rax, %rax
	je	.L107
	movq	8(%r12), %rdi
	movq	16(%rbx), %rcx
	movq	%r15, %r8
	movq	%r13, %rdx
	movq	%rdi, %rsi
	call	BN_mod_mul@PLT
	testl	%eax, %eax
	je	.L107
	movq	(%r12), %rdi
	call	BN_is_zero@PLT
	testl	%eax, %eax
	jne	.L85
	movq	8(%r12), %rdi
	call	BN_is_zero@PLT
	testl	%eax, %eax
	je	.L179
.L85:
	movq	(%r12), %rax
	movq	8(%rbx), %rdi
	movq	%rax, -120(%rbp)
	testq	%rdi, %rdi
	je	.L98
	movq	-96(%rbp), %rax
	movq	%rax, -72(%rbp)
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L176:
	movq	16(%rbx), %rdi
	call	BN_num_bits@PLT
	leal	14(%rax), %edx
	addl	$7, %eax
	cmovs	%edx, %eax
	sarl	$3, %eax
	movl	%eax, -52(%rbp)
	jmp	.L88
.L179:
	movq	-96(%rbp), %r14
	jmp	.L86
	.cfi_endproc
.LFE424:
	.size	dsa_do_sign, .-dsa_do_sign
	.p2align 4
	.type	dsa_sign_setup_no_digest, @function
dsa_sign_setup_no_digest:
.LFB425:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L181
	cmpq	$0, 16(%r14)
	je	.L181
	cmpq	$0, 24(%r14)
	je	.L181
	movq	(%rcx), %rax
	movq	%rsi, %rbx
	movq	%rdx, %r12
	movq	%rax, -56(%rbp)
	call	BN_is_zero@PLT
	testl	%eax, %eax
	jne	.L185
	movq	16(%r14), %rdi
	call	BN_is_zero@PLT
	testl	%eax, %eax
	je	.L252
.L185:
	movl	$199, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$112, %edx
	xorl	%r15d, %r15d
	movl	$107, %esi
	movl	$10, %edi
	call	ERR_put_error@PLT
.L180:
	leaq	-40(%rbp), %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L181:
	.cfi_restore_state
	movl	$193, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$101, %edx
	xorl	%r15d, %r15d
	movl	$107, %esi
	movl	$10, %edi
	call	ERR_put_error@PLT
	jmp	.L180
	.p2align 4,,10
	.p2align 3
.L252:
	movq	24(%r14), %rdi
	call	BN_is_zero@PLT
	movl	%eax, %r15d
	testl	%eax, %eax
	jne	.L185
	cmpq	$0, 40(%r14)
	je	.L253
	call	BN_new@PLT
	movq	%rax, %r13
	call	BN_new@PLT
	movq	%rax, -64(%rbp)
	testq	%r13, %r13
	je	.L187
	testq	%rax, %rax
	je	.L187
	movq	%rbx, -72(%rbp)
	testq	%rbx, %rbx
	je	.L254
.L188:
	movq	16(%r14), %rdi
	call	BN_num_bits@PLT
	movq	16(%r14), %rdi
	movl	%eax, -84(%rbp)
	call	bn_get_top@PLT
	movq	%r13, %rdi
	addl	$2, %eax
	movl	%eax, %esi
	movl	%eax, -80(%rbp)
	call	bn_wexpand@PLT
	testq	%rax, %rax
	je	.L189
	movl	-80(%rbp), %esi
	movq	-64(%rbp), %rdi
	call	bn_wexpand@PLT
	testq	%rax, %rax
	jne	.L190
	jmp	.L189
	.p2align 4,,10
	.p2align 3
.L256:
	movq	%r13, %rdi
	call	BN_is_zero@PLT
	testl	%eax, %eax
	je	.L255
.L190:
	movq	16(%r14), %rsi
	movq	%r13, %rdi
	call	BN_priv_rand_range@PLT
	testl	%eax, %eax
	jne	.L256
	.p2align 4,,10
	.p2align 3
.L189:
	movl	$291, %r8d
	movl	$3, %edx
	movl	$107, %esi
	movl	$10, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L199:
	movq	-72(%rbp), %rax
	cmpq	%rax, %rbx
	je	.L200
	movq	%rax, %rdi
	call	BN_CTX_free@PLT
.L200:
	movq	%r13, %rdi
	call	BN_clear_free@PLT
	movq	-64(%rbp), %rdi
	call	BN_clear_free@PLT
	jmp	.L180
	.p2align 4,,10
	.p2align 3
.L254:
	call	BN_CTX_new@PLT
	movq	%rax, -72(%rbp)
	testq	%rax, %rax
	jne	.L188
	.p2align 4,,10
	.p2align 3
.L187:
	movq	$0, -72(%rbp)
	jmp	.L189
	.p2align 4,,10
	.p2align 3
.L253:
	movl	$203, %r8d
	movl	$111, %edx
	movl	$107, %esi
	movl	$10, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L180
	.p2align 4,,10
	.p2align 3
.L255:
	movl	$4, %esi
	movq	%r13, %rdi
	call	BN_set_flags@PLT
	movq	-64(%rbp), %rdi
	movl	$4, %esi
	call	BN_set_flags@PLT
	testb	$1, 48(%r14)
	je	.L193
	movq	8(%r14), %rdx
	movq	96(%r14), %rsi
	leaq	56(%r14), %rdi
	movq	-72(%rbp), %rcx
	call	BN_MONT_CTX_set_locked@PLT
	testq	%rax, %rax
	je	.L189
.L193:
	movq	16(%r14), %rdx
	movq	-64(%rbp), %rdi
	movq	%r13, %rsi
	call	BN_add@PLT
	testl	%eax, %eax
	je	.L189
	movq	16(%r14), %rdx
	movq	-64(%rbp), %rsi
	movq	%r13, %rdi
	call	BN_add@PLT
	testl	%eax, %eax
	je	.L189
	movl	-84(%rbp), %esi
	movq	-64(%rbp), %rdi
	call	BN_is_bit_set@PLT
	movl	-80(%rbp), %ecx
	movq	-64(%rbp), %rdx
	movq	%r13, %rsi
	movslq	%eax, %rdi
	call	BN_consttime_swap@PLT
	movq	80(%r14), %rax
	movq	40(%rax), %rax
	testq	%rax, %rax
	je	.L194
	subq	$8, %rsp
	movq	24(%r14), %rdx
	movq	%r13, %rcx
	pushq	56(%r14)
	movq	-72(%rbp), %r9
	movq	%r14, %rdi
	movq	8(%r14), %r8
	movq	-56(%rbp), %rsi
	call	*%rax
	popq	%rdx
	popq	%rcx
	testl	%eax, %eax
	je	.L189
.L196:
	movq	-56(%rbp), %rdx
	movq	16(%r14), %rcx
	xorl	%edi, %edi
	movq	-72(%rbp), %r8
	movq	%rdx, %rsi
	call	BN_div@PLT
	testl	%eax, %eax
	je	.L189
	movq	16(%r14), %rax
	movq	%rax, -56(%rbp)
	call	BN_new@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L189
	movq	-72(%rbp), %rdi
	call	BN_CTX_start@PLT
	movq	-72(%rbp), %rdi
	call	BN_CTX_get@PLT
	testq	%rax, %rax
	je	.L198
	movl	$2, %esi
	movq	%r14, %rdi
	movq	%rax, -80(%rbp)
	call	BN_set_word@PLT
	movq	-80(%rbp), %r10
	testl	%eax, %eax
	jne	.L257
.L198:
	movq	%r14, %rdi
	call	BN_free@PLT
	movq	-72(%rbp), %rdi
	call	BN_CTX_end@PLT
	jmp	.L189
.L194:
	movq	8(%r14), %rcx
	movq	24(%r14), %rsi
	movq	%r13, %rdx
	movq	56(%r14), %r9
	movq	-72(%rbp), %r8
	movq	-56(%rbp), %rdi
	call	BN_mod_exp_mont@PLT
	testl	%eax, %eax
	jne	.L196
	jmp	.L189
.L257:
	movq	-56(%rbp), %rsi
	movq	%r10, %rdi
	movq	%r14, %rdx
	call	BN_sub@PLT
	movq	-80(%rbp), %r10
	testl	%eax, %eax
	je	.L198
	movq	-72(%rbp), %r8
	movq	-56(%rbp), %rcx
	xorl	%r9d, %r9d
	movq	%r10, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	BN_mod_exp_mont@PLT
	testl	%eax, %eax
	je	.L198
	movq	-72(%rbp), %rdi
	movl	$1, %r15d
	call	BN_CTX_end@PLT
	movq	(%r12), %rdi
	call	BN_clear_free@PLT
	movq	%r14, (%r12)
	jmp	.L199
	.cfi_endproc
.LFE425:
	.size	dsa_sign_setup_no_digest, .-dsa_sign_setup_no_digest
	.p2align 4
	.globl	DSA_set_default_method
	.type	DSA_set_default_method, @function
DSA_set_default_method:
.LFB421:
	.cfi_startproc
	endbr64
	movq	%rdi, default_DSA_method(%rip)
	ret
	.cfi_endproc
.LFE421:
	.size	DSA_set_default_method, .-DSA_set_default_method
	.p2align 4
	.globl	DSA_get_default_method
	.type	DSA_get_default_method, @function
DSA_get_default_method:
.LFB422:
	.cfi_startproc
	endbr64
	movq	default_DSA_method(%rip), %rax
	ret
	.cfi_endproc
.LFE422:
	.size	DSA_get_default_method, .-DSA_get_default_method
	.p2align 4
	.globl	DSA_OpenSSL
	.type	DSA_OpenSSL, @function
DSA_OpenSSL:
.LFB423:
	.cfi_startproc
	endbr64
	leaq	openssl_dsa_meth(%rip), %rax
	ret
	.cfi_endproc
.LFE423:
	.size	DSA_OpenSSL, .-DSA_OpenSSL
	.section	.data.rel.local,"aw"
	.align 8
	.type	default_DSA_method, @object
	.size	default_DSA_method, 8
default_DSA_method:
	.quad	openssl_dsa_meth
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"OpenSSL DSA method"
	.section	.data.rel.local
	.align 32
	.type	openssl_dsa_meth, @object
	.size	openssl_dsa_meth, 96
openssl_dsa_meth:
	.quad	.LC1
	.quad	dsa_do_sign
	.quad	dsa_sign_setup_no_digest
	.quad	dsa_do_verify
	.quad	0
	.quad	0
	.quad	dsa_init
	.quad	dsa_finish
	.long	1024
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
