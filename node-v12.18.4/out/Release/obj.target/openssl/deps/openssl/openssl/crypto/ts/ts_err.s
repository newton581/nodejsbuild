	.file	"ts_err.c"
	.text
	.p2align 4
	.globl	ERR_load_TS_strings
	.type	ERR_load_TS_strings, @function
ERR_load_TS_strings:
.LFB203:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$788979712, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_func_error_string@PLT
	testq	%rax, %rax
	je	.L5
	movl	$1, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	.cfi_restore_state
	leaq	TS_str_functs(%rip), %rdi
	call	ERR_load_strings_const@PLT
	leaq	TS_str_reasons(%rip), %rdi
	call	ERR_load_strings_const@PLT
	movl	$1, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE203:
	.size	ERR_load_TS_strings, .-ERR_load_TS_strings
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"bad pkcs7 type"
.LC1:
	.string	"bad type"
.LC2:
	.string	"cannot load certificate"
.LC3:
	.string	"cannot load private key"
.LC4:
	.string	"certificate verify error"
.LC5:
	.string	"could not set engine"
.LC6:
	.string	"could not set time"
.LC7:
	.string	"detached content"
.LC8:
	.string	"ess add signing cert error"
.LC9:
	.string	"ess add signing cert v2 error"
.LC10:
	.string	"ess signing certificate error"
.LC11:
	.string	"invalid null pointer"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC12:
	.string	"invalid signer certificate purpose"
	.section	.rodata.str1.1
.LC13:
	.string	"message imprint mismatch"
.LC14:
	.string	"nonce mismatch"
.LC15:
	.string	"nonce not returned"
.LC16:
	.string	"no content"
.LC17:
	.string	"no time stamp token"
.LC18:
	.string	"pkcs7 add signature error"
.LC19:
	.string	"pkcs7 add signed attr error"
.LC20:
	.string	"pkcs7 to ts tst info failed"
.LC21:
	.string	"policy mismatch"
	.section	.rodata.str1.8
	.align 8
.LC22:
	.string	"private key does not match certificate"
	.section	.rodata.str1.1
.LC23:
	.string	"response setup error"
.LC24:
	.string	"signature failure"
.LC25:
	.string	"there must be one signer"
.LC26:
	.string	"time syscall error"
.LC27:
	.string	"token not present"
.LC28:
	.string	"token present"
.LC29:
	.string	"tsa name mismatch"
.LC30:
	.string	"tsa untrusted"
.LC31:
	.string	"tst info setup error"
.LC32:
	.string	"ts datasign"
.LC33:
	.string	"unacceptable policy"
.LC34:
	.string	"unsupported md algorithm"
.LC35:
	.string	"unsupported version"
.LC36:
	.string	"var bad value"
.LC37:
	.string	"cannot find config variable"
.LC38:
	.string	"wrong content type"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	TS_str_reasons, @object
	.size	TS_str_reasons, 640
TS_str_reasons:
	.quad	788529284
	.quad	.LC0
	.quad	788529285
	.quad	.LC1
	.quad	788529289
	.quad	.LC2
	.quad	788529290
	.quad	.LC3
	.quad	788529252
	.quad	.LC4
	.quad	788529279
	.quad	.LC5
	.quad	788529267
	.quad	.LC6
	.quad	788529286
	.quad	.LC7
	.quad	788529268
	.quad	.LC8
	.quad	788529291
	.quad	.LC9
	.quad	788529253
	.quad	.LC10
	.quad	788529254
	.quad	.LC11
	.quad	788529269
	.quad	.LC12
	.quad	788529255
	.quad	.LC13
	.quad	788529256
	.quad	.LC14
	.quad	788529257
	.quad	.LC15
	.quad	788529258
	.quad	.LC16
	.quad	788529259
	.quad	.LC17
	.quad	788529270
	.quad	.LC18
	.quad	788529271
	.quad	.LC19
	.quad	788529281
	.quad	.LC20
	.quad	788529260
	.quad	.LC21
	.quad	788529272
	.quad	.LC22
	.quad	788529273
	.quad	.LC23
	.quad	788529261
	.quad	.LC24
	.quad	788529262
	.quad	.LC25
	.quad	788529274
	.quad	.LC26
	.quad	788529282
	.quad	.LC27
	.quad	788529283
	.quad	.LC28
	.quad	788529263
	.quad	.LC29
	.quad	788529264
	.quad	.LC30
	.quad	788529275
	.quad	.LC31
	.quad	788529276
	.quad	.LC32
	.quad	788529277
	.quad	.LC33
	.quad	788529278
	.quad	.LC34
	.quad	788529265
	.quad	.LC35
	.quad	788529287
	.quad	.LC36
	.quad	788529288
	.quad	.LC37
	.quad	788529266
	.quad	.LC38
	.quad	0
	.quad	0
	.section	.rodata.str1.1
.LC39:
	.string	"def_serial_cb"
.LC40:
	.string	"def_time_cb"
.LC41:
	.string	"ess_add_signing_cert"
.LC42:
	.string	"ess_add_signing_cert_v2"
.LC43:
	.string	"ess_CERT_ID_new_init"
.LC44:
	.string	"ess_cert_id_v2_new_init"
.LC45:
	.string	"ess_SIGNING_CERT_new_init"
.LC46:
	.string	"ess_signing_cert_v2_new_init"
.LC47:
	.string	"int_ts_RESP_verify_token"
.LC48:
	.string	"PKCS7_to_TS_TST_INFO"
.LC49:
	.string	"TS_ACCURACY_set_micros"
.LC50:
	.string	"TS_ACCURACY_set_millis"
.LC51:
	.string	"TS_ACCURACY_set_seconds"
.LC52:
	.string	"ts_check_imprints"
.LC53:
	.string	"ts_check_nonces"
.LC54:
	.string	"ts_check_policy"
.LC55:
	.string	"ts_check_signing_certs"
.LC56:
	.string	"ts_check_status_info"
.LC57:
	.string	"ts_compute_imprint"
.LC58:
	.string	"ts_CONF_invalid"
.LC59:
	.string	"TS_CONF_load_cert"
.LC60:
	.string	"TS_CONF_load_certs"
.LC61:
	.string	"TS_CONF_load_key"
.LC62:
	.string	"ts_CONF_lookup_fail"
.LC63:
	.string	"TS_CONF_set_default_engine"
.LC64:
	.string	"ts_get_status_text"
.LC65:
	.string	"TS_MSG_IMPRINT_set_algo"
.LC66:
	.string	"TS_REQ_set_msg_imprint"
.LC67:
	.string	"TS_REQ_set_nonce"
.LC68:
	.string	"TS_REQ_set_policy_id"
.LC69:
	.string	"TS_RESP_create_response"
.LC70:
	.string	"ts_RESP_create_tst_info"
.LC71:
	.string	"TS_RESP_CTX_add_failure_info"
.LC72:
	.string	"TS_RESP_CTX_add_md"
.LC73:
	.string	"TS_RESP_CTX_add_policy"
.LC74:
	.string	"TS_RESP_CTX_new"
.LC75:
	.string	"TS_RESP_CTX_set_accuracy"
.LC76:
	.string	"TS_RESP_CTX_set_certs"
.LC77:
	.string	"TS_RESP_CTX_set_def_policy"
.LC78:
	.string	"TS_RESP_CTX_set_signer_cert"
.LC79:
	.string	"TS_RESP_CTX_set_status_info"
.LC80:
	.string	"ts_RESP_get_policy"
	.section	.rodata.str1.8
	.align 8
.LC81:
	.string	"TS_RESP_set_genTime_with_precision"
	.section	.rodata.str1.1
.LC82:
	.string	"TS_RESP_set_status_info"
.LC83:
	.string	"TS_RESP_set_tst_info"
.LC84:
	.string	"ts_RESP_sign"
.LC85:
	.string	"TS_RESP_verify_signature"
.LC86:
	.string	"TS_TST_INFO_set_accuracy"
.LC87:
	.string	"TS_TST_INFO_set_msg_imprint"
.LC88:
	.string	"TS_TST_INFO_set_nonce"
.LC89:
	.string	"TS_TST_INFO_set_policy_id"
.LC90:
	.string	"TS_TST_INFO_set_serial"
.LC91:
	.string	"TS_TST_INFO_set_time"
.LC92:
	.string	"TS_TST_INFO_set_tsa"
.LC93:
	.string	""
.LC94:
	.string	"ts_verify_cert"
.LC95:
	.string	"TS_VERIFY_CTX_new"
	.section	.data.rel.ro.local
	.align 32
	.type	TS_str_functs, @object
	.size	TS_str_functs, 928
TS_str_functs:
	.quad	788979712
	.quad	.LC39
	.quad	788983808
	.quad	.LC40
	.quad	788987904
	.quad	.LC41
	.quad	789131264
	.quad	.LC42
	.quad	788992000
	.quad	.LC43
	.quad	789168128
	.quad	.LC44
	.quad	788996096
	.quad	.LC45
	.quad	789172224
	.quad	.LC46
	.quad	789139456
	.quad	.LC47
	.quad	789135360
	.quad	.LC48
	.quad	789000192
	.quad	.LC49
	.quad	789004288
	.quad	.LC50
	.quad	789008384
	.quad	.LC51
	.quad	788938752
	.quad	.LC52
	.quad	788942848
	.quad	.LC53
	.quad	788946944
	.quad	.LC54
	.quad	788951040
	.quad	.LC55
	.quad	788955136
	.quad	.LC56
	.quad	789123072
	.quad	.LC57
	.quad	789147648
	.quad	.LC58
	.quad	789155840
	.quad	.LC59
	.quad	789159936
	.quad	.LC60
	.quad	789164032
	.quad	.LC61
	.quad	789151744
	.quad	.LC62
	.quad	789127168
	.quad	.LC63
	.quad	788959232
	.quad	.LC64
	.quad	789012480
	.quad	.LC65
	.quad	789016576
	.quad	.LC66
	.quad	789020672
	.quad	.LC67
	.quad	789024768
	.quad	.LC68
	.quad	789028864
	.quad	.LC69
	.quad	789032960
	.quad	.LC70
	.quad	789037056
	.quad	.LC71
	.quad	789041152
	.quad	.LC72
	.quad	789045248
	.quad	.LC73
	.quad	789049344
	.quad	.LC74
	.quad	789053440
	.quad	.LC75
	.quad	789057536
	.quad	.LC76
	.quad	789061632
	.quad	.LC77
	.quad	789065728
	.quad	.LC78
	.quad	789069824
	.quad	.LC79
	.quad	789073920
	.quad	.LC80
	.quad	789078016
	.quad	.LC81
	.quad	789082112
	.quad	.LC82
	.quad	789143552
	.quad	.LC83
	.quad	789086208
	.quad	.LC84
	.quad	788963328
	.quad	.LC85
	.quad	789090304
	.quad	.LC86
	.quad	789094400
	.quad	.LC87
	.quad	789098496
	.quad	.LC88
	.quad	789102592
	.quad	.LC89
	.quad	789106688
	.quad	.LC90
	.quad	789110784
	.quad	.LC91
	.quad	789114880
	.quad	.LC92
	.quad	788971520
	.quad	.LC93
	.quad	788975616
	.quad	.LC94
	.quad	789118976
	.quad	.LC95
	.quad	0
	.quad	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
