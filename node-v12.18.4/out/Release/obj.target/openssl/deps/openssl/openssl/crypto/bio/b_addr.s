	.file	"b_addr.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/bio/b_addr.c"
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"%d"
	.text
	.p2align 4
	.type	addr_strings.part.0, @function
addr_strings.part.0:
.LFB298:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movl	$16, %r10d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	leaq	-1072(%rbp), %rdi
	subq	$1096, %rsp
	movq	%rcx, -1128(%rbp)
	movl	$126, %ecx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%esi, %esi
	movaps	%xmm0, -1088(%rbp)
	movaps	%xmm0, -1120(%rbp)
	movaps	%xmm0, -1104(%rbp)
	rep stosq
	movl	$3, %eax
	cmovne	%eax, %esi
	movzwl	(%rbx), %eax
	movb	$0, (%rdi)
	cmpw	$2, %ax
	je	.L3
	movl	$28, %r10d
	cmpw	$10, %ax
	je	.L3
	xorl	%r10d, %r10d
	cmpw	$1, %ax
	setne	%r10b
	leal	110(%r10,%r10), %r10d
.L3:
	subq	$8, %rsp
	leaq	-1120(%rbp), %r14
	movl	$1025, %ecx
	movl	$32, %r9d
	pushq	%rsi
	leaq	-1088(%rbp), %r15
	movq	%r14, %r8
	movl	%r10d, %esi
	movq	%r15, %rdx
	movq	%rbx, %rdi
	call	getnameinfo@PLT
	movl	%eax, %r12d
	popq	%rax
	popq	%rdx
	testl	%r12d, %r12d
	jne	.L40
	cmpb	$0, -1120(%rbp)
	jne	.L8
	movzwl	(%rbx), %eax
	cmpw	$2, %ax
	je	.L38
	xorl	%ecx, %ecx
	cmpw	$10, %ax
	je	.L38
.L10:
	leaq	.LC1(%rip), %rdx
	movl	$32, %esi
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	BIO_snprintf@PLT
.L8:
	testq	%r13, %r13
	je	.L11
	movl	$233, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r15, %rdi
	call	CRYPTO_strdup@PLT
	cmpq	$0, -1128(%rbp)
	movq	%rax, 0(%r13)
	jne	.L41
	cmpq	$0, 0(%r13)
	je	.L42
.L16:
	movl	$1, %r12d
.L1:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L43
	leaq	-40(%rbp), %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	.cfi_restore_state
	cmpq	$0, -1128(%rbp)
	je	.L16
	movq	%r14, %rdi
	movl	$235, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_strdup@PLT
	movq	%rax, %rdi
	movq	-1128(%rbp), %rax
	movq	%rdi, (%rax)
	testq	%rdi, %rdi
	jne	.L16
.L14:
	movl	$254, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-1128(%rbp), %rax
	movq	$0, (%rax)
.L17:
	movl	$257, %r8d
	movl	$65, %edx
	movl	$134, %esi
	movl	$32, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L40:
	cmpl	$-11, %r12d
	je	.L44
	movl	$134, %esi
	movl	$215, %r8d
	movl	$2, %edx
	movl	$32, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	%r12d, %edi
	call	gai_strerror@PLT
	movl	$1, %edi
	movq	%rax, %rsi
	xorl	%eax, %eax
	call	ERR_add_error_data@PLT
.L6:
	xorl	%r12d, %r12d
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L41:
	movl	$235, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r14, %rdi
	call	CRYPTO_strdup@PLT
	movq	-1128(%rbp), %rbx
	movq	%rax, (%rbx)
	cmpq	$0, 0(%r13)
	je	.L45
	cmpq	$0, (%rbx)
	jne	.L16
	movq	0(%r13), %rdi
	movl	$250, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-1128(%rbp), %rax
	movq	$0, 0(%r13)
	movq	(%rax), %rdi
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L42:
	movl	$250, %edx
	leaq	.LC0(%rip), %rsi
	xorl	%edi, %edi
	call	CRYPTO_free@PLT
	movq	$0, 0(%r13)
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L38:
	movzwl	2(%rbx), %ecx
	rolw	$8, %cx
	movzwl	%cx, %ecx
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L44:
	call	__errno_location@PLT
	movl	$210, %r8d
	movl	$13, %esi
	leaq	.LC0(%rip), %rcx
	movl	(%rax), %edx
	movl	$2, %edi
	call	ERR_put_error@PLT
	movl	$211, %r8d
	movl	$2, %edx
	leaq	.LC0(%rip), %rcx
	movl	$134, %esi
	movl	$32, %edi
	call	ERR_put_error@PLT
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L45:
	xorl	%edi, %edi
	movl	$250, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-1128(%rbp), %rax
	movq	$0, 0(%r13)
	movq	(%rax), %rdi
	jmp	.L14
.L43:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE298:
	.size	addr_strings.part.0, .-addr_strings.part.0
	.p2align 4
	.globl	BIO_ADDR_new
	.type	BIO_ADDR_new, @function
BIO_ADDR_new:
.LFB267:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$40, %edx
	movl	$112, %edi
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	call	CRYPTO_zalloc@PLT
	testq	%rax, %rax
	je	.L50
	xorl	%edx, %edx
	movw	%dx, (%rax)
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L50:
	.cfi_restore_state
	movl	$43, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	movl	$144, %esi
	movl	$32, %edi
	movq	%rax, -8(%rbp)
	call	ERR_put_error@PLT
	movq	-8(%rbp), %rax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE267:
	.size	BIO_ADDR_new, .-BIO_ADDR_new
	.p2align 4
	.globl	BIO_ADDR_free
	.type	BIO_ADDR_free, @function
BIO_ADDR_free:
.LFB268:
	.cfi_startproc
	endbr64
	movl	$53, %edx
	leaq	.LC0(%rip), %rsi
	jmp	CRYPTO_free@PLT
	.cfi_endproc
.LFE268:
	.size	BIO_ADDR_free, .-BIO_ADDR_free
	.p2align 4
	.globl	BIO_ADDR_clear
	.type	BIO_ADDR_clear, @function
BIO_ADDR_clear:
.LFB269:
	.cfi_startproc
	endbr64
	movq	$0, (%rdi)
	movq	%rdi, %rdx
	leaq	8(%rdi), %rdi
	xorl	%eax, %eax
	movq	$0, 96(%rdi)
	movq	%rdx, %rcx
	andq	$-8, %rdi
	subq	%rdi, %rcx
	addl	$112, %ecx
	shrl	$3, %ecx
	rep stosq
	xorl	%eax, %eax
	movw	%ax, (%rdx)
	ret
	.cfi_endproc
.LFE269:
	.size	BIO_ADDR_clear, .-BIO_ADDR_clear
	.p2align 4
	.globl	BIO_ADDR_make
	.type	BIO_ADDR_make, @function
BIO_ADDR_make:
.LFB270:
	.cfi_startproc
	endbr64
	movzwl	(%rsi), %eax
	cmpw	$2, %ax
	je	.L58
	cmpw	$10, %ax
	je	.L59
	xorl	%r8d, %r8d
	cmpw	$1, %ax
	je	.L60
.L53:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L58:
	movdqu	(%rsi), %xmm0
	movl	$1, %r8d
	movl	%r8d, %eax
	movups	%xmm0, (%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L59:
	movdqu	(%rsi), %xmm1
	movl	$1, %r8d
	movups	%xmm1, (%rdi)
	movq	16(%rsi), %rax
	movq	%rax, 16(%rdi)
	movl	24(%rsi), %eax
	movl	%eax, 24(%rdi)
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L60:
	movdqu	(%rsi), %xmm2
	movl	$1, %r8d
	movups	%xmm2, (%rdi)
	movdqu	16(%rsi), %xmm3
	movups	%xmm3, 16(%rdi)
	movdqu	32(%rsi), %xmm4
	movups	%xmm4, 32(%rdi)
	movdqu	48(%rsi), %xmm5
	movups	%xmm5, 48(%rdi)
	movdqu	64(%rsi), %xmm6
	movups	%xmm6, 64(%rdi)
	movdqu	80(%rsi), %xmm7
	movups	%xmm7, 80(%rdi)
	movq	96(%rsi), %rax
	movq	%rax, 96(%rdi)
	movl	104(%rsi), %eax
	movl	%eax, 104(%rdi)
	movzwl	108(%rsi), %eax
	movw	%ax, 108(%rdi)
	jmp	.L53
	.cfi_endproc
.LFE270:
	.size	BIO_ADDR_make, .-BIO_ADDR_make
	.p2align 4
	.globl	BIO_ADDR_rawmake
	.type	BIO_ADDR_rawmake, @function
BIO_ADDR_rawmake:
.LFB271:
	.cfi_startproc
	endbr64
	movq	%rdi, %r9
	movq	%rdx, %r10
	cmpl	$1, %esi
	je	.L72
	cmpl	$2, %esi
	je	.L73
	cmpl	$10, %esi
	jne	.L67
	cmpq	$16, %rcx
	je	.L74
.L67:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L73:
	xorl	%eax, %eax
	cmpq	$4, %rcx
	je	.L75
.L69:
	ret
	.p2align 4,,10
	.p2align 3
.L72:
	addq	$1, %rcx
	xorl	%eax, %eax
	cmpq	$108, %rcx
	ja	.L69
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	8(%rdi), %rdi
	movq	%r9, %rcx
	xorl	%eax, %eax
	movq	$0, -8(%rdi)
	movl	$107, %edx
	movq	%r10, %rsi
	movq	$0, 94(%rdi)
	andq	$-8, %rdi
	subq	%rdi, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	addl	$110, %ecx
	shrl	$3, %ecx
	rep stosq
	movl	$1, %ecx
	leaq	2(%r9), %rdi
	movw	%cx, (%r9)
	call	strncpy@PLT
	movl	$1, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L75:
	.cfi_restore 6
	pxor	%xmm0, %xmm0
	movl	$2, %edx
	movups	%xmm0, (%rdi)
	movw	%dx, (%rdi)
	movw	%r8w, 2(%rdi)
	movl	(%r10), %eax
	movl	%eax, 4(%rdi)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L74:
	pxor	%xmm0, %xmm0
	movl	$10, %eax
	movq	$0, 16(%rdi)
	movups	%xmm0, (%rdi)
	movl	$0, 24(%rdi)
	movw	%ax, (%rdi)
	movl	$1, %eax
	movw	%r8w, 2(%rdi)
	movdqu	(%rdx), %xmm1
	movups	%xmm1, 8(%rdi)
	ret
	.cfi_endproc
.LFE271:
	.size	BIO_ADDR_rawmake, .-BIO_ADDR_rawmake
	.p2align 4
	.globl	BIO_ADDR_family
	.type	BIO_ADDR_family, @function
BIO_ADDR_family:
.LFB272:
	.cfi_startproc
	endbr64
	movzwl	(%rdi), %eax
	ret
	.cfi_endproc
.LFE272:
	.size	BIO_ADDR_family, .-BIO_ADDR_family
	.p2align 4
	.globl	BIO_ADDR_rawaddress
	.type	BIO_ADDR_rawaddress, @function
BIO_ADDR_rawaddress:
.LFB273:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	$4, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	leaq	4(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movzwl	(%rdi), %eax
	movq	%rdx, %rbx
	cmpw	$2, %ax
	je	.L79
	leaq	8(%rdi), %r12
	movl	$16, %r14d
	cmpw	$10, %ax
	je	.L79
	xorl	%r8d, %r8d
	cmpw	$1, %ax
	jne	.L77
	leaq	2(%rdi), %r12
	movq	%r12, %rdi
	call	strlen@PLT
	movq	%rax, %r14
.L79:
	testq	%r13, %r13
	je	.L82
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	memcpy@PLT
.L82:
	movl	$1, %r8d
	testq	%rbx, %rbx
	je	.L77
	movq	%r14, (%rbx)
.L77:
	popq	%rbx
	movl	%r8d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE273:
	.size	BIO_ADDR_rawaddress, .-BIO_ADDR_rawaddress
	.p2align 4
	.globl	BIO_ADDR_rawport
	.type	BIO_ADDR_rawport, @function
BIO_ADDR_rawport:
.LFB274:
	.cfi_startproc
	endbr64
	movzwl	(%rdi), %eax
	cmpw	$2, %ax
	je	.L94
	xorl	%r8d, %r8d
	cmpw	$10, %ax
	je	.L94
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L94:
	movzwl	2(%rdi), %r8d
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE274:
	.size	BIO_ADDR_rawport, .-BIO_ADDR_rawport
	.p2align 4
	.globl	BIO_ADDR_hostname_string
	.type	BIO_ADDR_hostname_string, @function
BIO_ADDR_hostname_string:
.LFB276:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	$0, -32(%rbp)
	call	BIO_sock_init@PLT
	cmpl	$1, %eax
	jne	.L98
	xorl	%ecx, %ecx
	leaq	-32(%rbp), %rdx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	addr_strings.part.0
	testl	%eax, %eax
	je	.L98
	movq	-32(%rbp), %rax
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L98:
	xorl	%eax, %eax
.L95:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L104
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L104:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE276:
	.size	BIO_ADDR_hostname_string, .-BIO_ADDR_hostname_string
	.p2align 4
	.globl	BIO_ADDR_service_string
	.type	BIO_ADDR_service_string, @function
BIO_ADDR_service_string:
.LFB277:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	$0, -32(%rbp)
	call	BIO_sock_init@PLT
	cmpl	$1, %eax
	jne	.L108
	xorl	%edx, %edx
	leaq	-32(%rbp), %rcx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	addr_strings.part.0
	testl	%eax, %eax
	je	.L108
	movq	-32(%rbp), %rax
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L108:
	xorl	%eax, %eax
.L105:
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L114
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L114:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE277:
	.size	BIO_ADDR_service_string, .-BIO_ADDR_service_string
	.p2align 4
	.globl	BIO_ADDR_path_string
	.type	BIO_ADDR_path_string, @function
BIO_ADDR_path_string:
.LFB278:
	.cfi_startproc
	endbr64
	cmpw	$1, (%rdi)
	je	.L117
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L117:
	addq	$2, %rdi
	movl	$288, %edx
	leaq	.LC0(%rip), %rsi
	jmp	CRYPTO_strdup@PLT
	.cfi_endproc
.LFE278:
	.size	BIO_ADDR_path_string, .-BIO_ADDR_path_string
	.p2align 4
	.globl	BIO_ADDR_sockaddr
	.type	BIO_ADDR_sockaddr, @function
BIO_ADDR_sockaddr:
.LFB279:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	ret
	.cfi_endproc
.LFE279:
	.size	BIO_ADDR_sockaddr, .-BIO_ADDR_sockaddr
	.p2align 4
	.globl	BIO_ADDR_sockaddr_noconst
	.type	BIO_ADDR_sockaddr_noconst, @function
BIO_ADDR_sockaddr_noconst:
.LFB302:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	ret
	.cfi_endproc
.LFE302:
	.size	BIO_ADDR_sockaddr_noconst, .-BIO_ADDR_sockaddr_noconst
	.p2align 4
	.globl	BIO_ADDR_sockaddr_size
	.type	BIO_ADDR_sockaddr_size, @function
BIO_ADDR_sockaddr_size:
.LFB281:
	.cfi_startproc
	endbr64
	movzwl	(%rdi), %edx
	movl	$16, %eax
	cmpw	$2, %dx
	je	.L120
	movl	$28, %eax
	cmpw	$10, %dx
	je	.L120
	xorl	%eax, %eax
	cmpw	$1, %dx
	setne	%al
	leal	110(%rax,%rax), %eax
.L120:
	ret
	.cfi_endproc
.LFE281:
	.size	BIO_ADDR_sockaddr_size, .-BIO_ADDR_sockaddr_size
	.p2align 4
	.globl	BIO_ADDRINFO_next
	.type	BIO_ADDRINFO_next, @function
BIO_ADDRINFO_next:
.LFB282:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L127
	movq	40(%rdi), %rax
.L127:
	ret
	.cfi_endproc
.LFE282:
	.size	BIO_ADDRINFO_next, .-BIO_ADDRINFO_next
	.p2align 4
	.globl	BIO_ADDRINFO_family
	.type	BIO_ADDRINFO_family, @function
BIO_ADDRINFO_family:
.LFB283:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L131
	movl	4(%rdi), %eax
.L131:
	ret
	.cfi_endproc
.LFE283:
	.size	BIO_ADDRINFO_family, .-BIO_ADDRINFO_family
	.p2align 4
	.globl	BIO_ADDRINFO_socktype
	.type	BIO_ADDRINFO_socktype, @function
BIO_ADDRINFO_socktype:
.LFB284:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L135
	movl	8(%rdi), %eax
.L135:
	ret
	.cfi_endproc
.LFE284:
	.size	BIO_ADDRINFO_socktype, .-BIO_ADDRINFO_socktype
	.p2align 4
	.globl	BIO_ADDRINFO_protocol
	.type	BIO_ADDRINFO_protocol, @function
BIO_ADDRINFO_protocol:
.LFB285:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L139
	movl	12(%rdi), %eax
	testl	%eax, %eax
	jne	.L139
	cmpl	$1, 4(%rdi)
	je	.L139
	movl	8(%rdi), %edx
	cmpl	$1, %edx
	je	.L142
	cmpl	$2, %edx
	movl	$17, %edx
	cmove	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L142:
	movl	$6, %eax
.L139:
	ret
	.cfi_endproc
.LFE285:
	.size	BIO_ADDRINFO_protocol, .-BIO_ADDRINFO_protocol
	.p2align 4
	.globl	BIO_ADDRINFO_sockaddr_size
	.type	BIO_ADDRINFO_sockaddr_size, @function
BIO_ADDRINFO_sockaddr_size:
.LFB286:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L144
	movl	16(%rdi), %eax
.L144:
	ret
	.cfi_endproc
.LFE286:
	.size	BIO_ADDRINFO_sockaddr_size, .-BIO_ADDRINFO_sockaddr_size
	.p2align 4
	.globl	BIO_ADDRINFO_sockaddr
	.type	BIO_ADDRINFO_sockaddr, @function
BIO_ADDRINFO_sockaddr:
.LFB287:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L148
	movq	24(%rdi), %rax
.L148:
	ret
	.cfi_endproc
.LFE287:
	.size	BIO_ADDRINFO_sockaddr, .-BIO_ADDRINFO_sockaddr
	.p2align 4
	.globl	BIO_ADDRINFO_address
	.type	BIO_ADDRINFO_address, @function
BIO_ADDRINFO_address:
.LFB288:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L152
	movq	24(%rdi), %rax
.L152:
	ret
	.cfi_endproc
.LFE288:
	.size	BIO_ADDRINFO_address, .-BIO_ADDRINFO_address
	.p2align 4
	.globl	BIO_ADDRINFO_free
	.type	BIO_ADDRINFO_free, @function
BIO_ADDRINFO_free:
.LFB289:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L165
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	leaq	.LC0(%rip), %rbx
	subq	$8, %rsp
	cmpl	$1, 4(%rdi)
	jne	.L168
	.p2align 4,,10
	.p2align 3
.L158:
	movq	%r12, %r13
	movl	$436, %edx
	movq	40(%r12), %r12
	movq	%rbx, %rsi
	movq	24(%r13), %rdi
	call	CRYPTO_free@PLT
	movl	$437, %edx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	CRYPTO_free@PLT
	testq	%r12, %r12
	jne	.L158
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L168:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	freeaddrinfo@PLT
	.p2align 4,,10
	.p2align 3
.L165:
	ret
	.cfi_endproc
.LFE289:
	.size	BIO_ADDRINFO_free, .-BIO_ADDRINFO_free
	.p2align 4
	.globl	BIO_parse_hostserv
	.type	BIO_parse_hostserv, @function
BIO_parse_hostserv:
.LFB290:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	cmpb	$91, (%rdi)
	je	.L212
	movl	$58, %esi
	movl	%ecx, %r12d
	call	strrchr@PLT
	movl	$58, %esi
	movq	%r15, %rdi
	movq	%rax, %rbx
	call	strchr@PLT
	cmpq	%rax, %rbx
	jne	.L213
	testq	%rbx, %rbx
	je	.L176
	movq	%rbx, %rax
	leaq	1(%rbx), %r12
	subq	%r15, %rax
	movq	%r12, %rdi
	movq	%rax, -56(%rbp)
	call	strlen@PLT
	movq	%rax, %rbx
.L173:
	movl	$58, %esi
	movq	%r12, %rdi
	call	strchr@PLT
	testq	%rax, %rax
	je	.L214
.L171:
	movl	$547, %r8d
	movl	$130, %edx
	movl	$136, %esi
	movl	$32, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
.L169:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L213:
	.cfi_restore_state
	movl	$544, %r8d
	movl	$129, %edx
	movl	$136, %esi
	movl	$32, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L212:
	.cfi_restore_state
	movl	$93, %esi
	call	strchr@PLT
	testq	%rax, %rax
	je	.L171
	addq	$1, %r15
	movq	%rax, %rbx
	movzbl	1(%rax), %edx
	subq	%r15, %rbx
	movq	%rbx, -56(%rbp)
	testb	%dl, %dl
	je	.L172
	cmpb	$58, %dl
	jne	.L171
	leaq	2(%rax), %r12
	movq	%r12, %rdi
	call	strlen@PLT
	movq	%rax, %rbx
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L187:
	movq	%rax, -56(%rbp)
.L172:
	testq	%r13, %r13
	je	.L211
	xorl	%ebx, %ebx
	xorl	%r12d, %r12d
.L186:
	movq	-56(%rbp), %rax
	testq	%rax, %rax
	je	.L178
	cmpq	$1, %rax
	jne	.L179
	cmpb	$42, (%r15)
	je	.L178
.L179:
	movq	-56(%rbp), %rsi
	movl	$526, %ecx
	leaq	.LC0(%rip), %rdx
	movq	%r15, %rdi
	call	CRYPTO_strndup@PLT
	movq	%rax, 0(%r13)
	testq	%rax, %rax
	je	.L180
.L177:
	testq	%r12, %r12
	je	.L211
	testq	%r14, %r14
	je	.L211
	testq	%rbx, %rbx
	je	.L183
	cmpq	$1, %rbx
	jne	.L184
	cmpb	$42, (%r12)
	je	.L183
.L184:
	movl	$536, %ecx
	leaq	.LC0(%rip), %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	CRYPTO_strndup@PLT
	movq	%rax, (%r14)
	testq	%rax, %rax
	je	.L180
.L211:
	movl	$1, %eax
.L215:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L214:
	.cfi_restore_state
	testq	%r13, %r13
	jne	.L186
	jmp	.L177
	.p2align 4,,10
	.p2align 3
.L176:
	movq	%r15, %rdi
	call	strlen@PLT
	movq	%rax, %rbx
	testl	%r12d, %r12d
	je	.L187
	movq	%r15, %r12
	jmp	.L177
	.p2align 4,,10
	.p2align 3
.L178:
	movq	$0, 0(%r13)
	jmp	.L177
	.p2align 4,,10
	.p2align 3
.L183:
	movq	$0, (%r14)
	movl	$1, %eax
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L180:
	movl	$550, %r8d
	movl	$65, %edx
	movl	$136, %esi
	movl	$32, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L169
	.cfi_endproc
.LFE290:
	.size	BIO_parse_hostserv, .-BIO_parse_hostserv
	.p2align 4
	.globl	BIO_lookup
	.type	BIO_lookup, @function
BIO_lookup:
.LFB294:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%r9, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%ecx, %ebx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$2, %ecx
	jg	.L217
	testl	%ecx, %ecx
	jns	.L255
.L219:
	movl	$659, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$131, %edx
	xorl	%r12d, %r12d
	movl	$143, %esi
	movl	$32, %edi
	call	ERR_put_error@PLT
.L216:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L256
	addq	$88, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L217:
	.cfi_restore_state
	cmpl	$10, %ecx
	jne	.L219
.L220:
	movl	%r8d, -120(%rbp)
	movl	%edx, -116(%rbp)
	call	BIO_sock_init@PLT
	movl	-116(%rbp), %edx
	movl	-120(%rbp), %r8d
	cmpl	$1, %eax
	movl	%eax, %r12d
	je	.L257
	xorl	%r12d, %r12d
	jmp	.L216
	.p2align 4,,10
	.p2align 3
.L255:
	cmpl	$1, %ecx
	jne	.L220
	movl	%r8d, -116(%rbp)
	call	strlen@PLT
	movl	$568, %edx
	movl	$48, %edi
	leaq	.LC0(%rip), %rsi
	movq	%rax, %rbx
	call	CRYPTO_zalloc@PLT
	movl	-116(%rbp), %r8d
	testq	%rax, %rax
	movq	%rax, (%r14)
	je	.L258
	movl	%r8d, 8(%rax)
	movl	$40, %edx
	movl	$112, %edi
	leaq	.LC0(%rip), %rsi
	movl	$1, 4(%rax)
	movl	$0, 12(%rax)
	call	CRYPTO_zalloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L259
	addq	$1, %rbx
	cmpq	$108, %rbx
	jbe	.L237
	xorl	%eax, %eax
	movw	%ax, (%r12)
.L238:
	movq	(%r14), %rax
	movq	%r12, 24(%rax)
.L225:
	movq	$0, 40(%rax)
	movq	(%r14), %r12
	movq	24(%r12), %rdi
	testq	%rdi, %rdi
	je	.L236
	movl	$1, %r12d
	jmp	.L216
	.p2align 4,,10
	.p2align 3
.L257:
	pxor	%xmm0, %xmm0
	movl	$1, %eax
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	%ebx, -108(%rbp)
	movl	%r8d, -104(%rbp)
	movl	$0, -100(%rbp)
	movaps	%xmm0, -80(%rbp)
	testl	%ebx, %ebx
	jne	.L228
	movl	$32, -112(%rbp)
	movl	$33, %eax
.L228:
	cmpl	$1, %edx
	jne	.L229
	movl	%eax, -112(%rbp)
.L229:
	movl	$0, -116(%rbp)
	leaq	-112(%rbp), %rbx
.L234:
	movq	%r14, %rcx
	movq	%rbx, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	getaddrinfo@PLT
	cmpl	$-10, %eax
	je	.L230
	testl	%eax, %eax
	je	.L216
	cmpl	$-11, %eax
	je	.L260
	movl	-112(%rbp), %ecx
	testb	$32, %cl
	je	.L233
	andl	$-33, %ecx
	movl	%eax, -116(%rbp)
	orl	$4, %ecx
	movl	%ecx, -112(%rbp)
	jmp	.L234
	.p2align 4,,10
	.p2align 3
.L259:
	movl	$43, %r8d
	movl	$65, %edx
	movl	$144, %esi
	movl	$32, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	(%r14), %rax
	jmp	.L225
.L233:
	movl	$726, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$2, %edx
	movl	%eax, -120(%rbp)
	movl	$143, %esi
	movl	$32, %edi
	call	ERR_put_error@PLT
	movl	-116(%rbp), %esi
	movl	-120(%rbp), %eax
	testl	%esi, %esi
	cmovne	%esi, %eax
	xorl	%r12d, %r12d
	movl	%eax, %edi
	call	gai_strerror@PLT
	movl	$1, %edi
	movq	%rax, %rsi
	xorl	%eax, %eax
	call	ERR_add_error_data@PLT
	jmp	.L216
	.p2align 4,,10
	.p2align 3
.L230:
	movl	$711, %r8d
.L253:
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	movl	$143, %esi
	xorl	%r12d, %r12d
	movl	$32, %edi
	call	ERR_put_error@PLT
	jmp	.L216
	.p2align 4,,10
	.p2align 3
.L260:
	call	__errno_location@PLT
	movl	$705, %r8d
	movl	$12, %esi
	xorl	%r12d, %r12d
	movl	(%rax), %edx
	leaq	.LC0(%rip), %rcx
	movl	$2, %edi
	call	ERR_put_error@PLT
	movl	$706, %r8d
	movl	$2, %edx
	leaq	.LC0(%rip), %rcx
	movl	$143, %esi
	movl	$32, %edi
	call	ERR_put_error@PLT
	jmp	.L216
	.p2align 4,,10
	.p2align 3
.L237:
	leaq	8(%r12), %rdi
	movq	%r12, %rcx
	xorl	%eax, %eax
	movl	$1, %edx
	andq	$-8, %rdi
	movq	$0, (%r12)
	movq	%r13, %rsi
	movq	$0, 102(%r12)
	subq	%rdi, %rcx
	addl	$110, %ecx
	shrl	$3, %ecx
	rep stosq
	movw	%dx, (%r12)
	leaq	2(%r12), %rdi
	movl	$107, %edx
	call	strncpy@PLT
	jmp	.L238
	.p2align 4,,10
	.p2align 3
.L258:
	movl	$569, %r8d
	movl	$65, %edx
	movl	$148, %esi
	movl	$32, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L223:
	movl	$668, %r8d
	jmp	.L253
	.p2align 4,,10
	.p2align 3
.L236:
	cmpl	$1, 4(%r12)
	leaq	.LC0(%rip), %r13
	je	.L226
	jmp	.L261
	.p2align 4,,10
	.p2align 3
.L262:
	movq	24(%rbx), %rdi
	movq	%rbx, %r12
.L226:
	movq	40(%r12), %rbx
	movl	$436, %edx
	movq	%r13, %rsi
	call	CRYPTO_free@PLT
	movl	$437, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
	testq	%rbx, %rbx
	jne	.L262
.L227:
	movq	$0, (%r14)
	jmp	.L223
.L261:
	movq	%r12, %rdi
	call	freeaddrinfo@PLT
	jmp	.L227
.L256:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE294:
	.size	BIO_lookup, .-BIO_lookup
	.p2align 4
	.globl	BIO_lookup_ex
	.type	BIO_lookup_ex, @function
BIO_lookup_ex:
.LFB295:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%r9d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%ecx, %ebx
	subq	$88, %rsp
	movq	16(%rbp), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$2, %ecx
	jg	.L264
	testl	%ecx, %ecx
	jns	.L302
.L266:
	movl	$659, %r8d
	movl	$131, %edx
	movl	$143, %esi
	movl	$32, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%r9d, %r9d
.L263:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L303
	addq	$88, %rsp
	movl	%r9d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L264:
	.cfi_restore_state
	cmpl	$10, %ecx
	jne	.L266
.L267:
	movl	%r8d, -120(%rbp)
	movl	%edx, -116(%rbp)
	call	BIO_sock_init@PLT
	movl	-116(%rbp), %edx
	movl	-120(%rbp), %r8d
	cmpl	$1, %eax
	movl	%eax, %r9d
	je	.L304
	xorl	%r9d, %r9d
	jmp	.L263
	.p2align 4,,10
	.p2align 3
.L302:
	cmpl	$1, %ecx
	jne	.L267
	movl	%r8d, -116(%rbp)
	call	strlen@PLT
	movl	$568, %edx
	movl	$48, %edi
	leaq	.LC0(%rip), %rsi
	movq	%rax, %rbx
	call	CRYPTO_zalloc@PLT
	movl	-116(%rbp), %r8d
	testq	%rax, %rax
	movq	%rax, (%r15)
	je	.L305
	movl	%r8d, 8(%rax)
	movl	$40, %edx
	movl	$112, %edi
	leaq	.LC0(%rip), %rsi
	movl	$1, 4(%rax)
	movl	$0, 12(%rax)
	call	CRYPTO_zalloc@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L306
	addq	$1, %rbx
	cmpq	$108, %rbx
	jbe	.L284
	xorl	%eax, %eax
	movw	%ax, 0(%r13)
.L285:
	movq	(%r15), %rax
	movq	%r13, 24(%rax)
.L272:
	movq	$0, 40(%rax)
	movq	(%r15), %r12
	movq	24(%r12), %rdi
	testq	%rdi, %rdi
	je	.L283
	movl	$1, %r9d
	jmp	.L263
	.p2align 4,,10
	.p2align 3
.L304:
	pxor	%xmm0, %xmm0
	movl	$1, %eax
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	%ebx, -108(%rbp)
	movl	%r8d, -104(%rbp)
	movl	%r13d, -100(%rbp)
	movaps	%xmm0, -80(%rbp)
	testl	%ebx, %ebx
	jne	.L275
	movl	$32, -112(%rbp)
	movl	$33, %eax
.L275:
	cmpl	$1, %edx
	jne	.L276
	movl	%eax, -112(%rbp)
.L276:
	xorl	%r13d, %r13d
	leaq	-112(%rbp), %rbx
.L281:
	movq	%r15, %rcx
	movq	%rbx, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	%r9d, -116(%rbp)
	call	getaddrinfo@PLT
	cmpl	$-10, %eax
	je	.L277
	testl	%eax, %eax
	movl	-116(%rbp), %r9d
	je	.L263
	cmpl	$-11, %eax
	je	.L307
	movl	-112(%rbp), %edx
	testb	$32, %dl
	je	.L280
	andl	$-33, %edx
	movl	%eax, %r13d
	orl	$4, %edx
	movl	%edx, -112(%rbp)
	jmp	.L281
	.p2align 4,,10
	.p2align 3
.L306:
	movl	$43, %r8d
	movl	$65, %edx
	movl	$144, %esi
	movl	$32, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	(%r15), %rax
	jmp	.L272
.L280:
	movl	$726, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$2, %edx
	movl	%eax, -116(%rbp)
	movl	$143, %esi
	movl	$32, %edi
	call	ERR_put_error@PLT
	movl	-116(%rbp), %eax
	testl	%r13d, %r13d
	cmove	%eax, %r13d
	movl	%r13d, %edi
	call	gai_strerror@PLT
	movl	$1, %edi
	movq	%rax, %rsi
	xorl	%eax, %eax
	call	ERR_add_error_data@PLT
	xorl	%r9d, %r9d
	jmp	.L263
	.p2align 4,,10
	.p2align 3
.L277:
	movl	$711, %r8d
.L300:
	movl	$65, %edx
	movl	$143, %esi
	movl	$32, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%r9d, %r9d
	jmp	.L263
	.p2align 4,,10
	.p2align 3
.L307:
	call	__errno_location@PLT
	movl	$705, %r8d
	movl	$12, %esi
	leaq	.LC0(%rip), %rcx
	movl	(%rax), %edx
	movl	$2, %edi
	call	ERR_put_error@PLT
	movl	$706, %r8d
	movl	$2, %edx
	leaq	.LC0(%rip), %rcx
	movl	$143, %esi
	movl	$32, %edi
	call	ERR_put_error@PLT
	xorl	%r9d, %r9d
	jmp	.L263
	.p2align 4,,10
	.p2align 3
.L284:
	leaq	8(%r13), %rdi
	movq	%r13, %rcx
	xorl	%eax, %eax
	movl	$1, %edx
	andq	$-8, %rdi
	movq	$0, 0(%r13)
	movq	%r12, %rsi
	subq	%rdi, %rcx
	movq	$0, 102(%r13)
	addl	$110, %ecx
	shrl	$3, %ecx
	rep stosq
	movw	%dx, 0(%r13)
	leaq	2(%r13), %rdi
	movl	$107, %edx
	call	strncpy@PLT
	jmp	.L285
	.p2align 4,,10
	.p2align 3
.L305:
	movl	$569, %r8d
	movl	$65, %edx
	movl	$148, %esi
	movl	$32, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L270:
	movl	$668, %r8d
	jmp	.L300
	.p2align 4,,10
	.p2align 3
.L283:
	cmpl	$1, 4(%r12)
	leaq	.LC0(%rip), %r13
	je	.L273
	jmp	.L308
	.p2align 4,,10
	.p2align 3
.L309:
	movq	24(%rbx), %rdi
	movq	%rbx, %r12
.L273:
	movq	40(%r12), %rbx
	movl	$436, %edx
	movq	%r13, %rsi
	call	CRYPTO_free@PLT
	movl	$437, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
	testq	%rbx, %rbx
	jne	.L309
.L274:
	movq	$0, (%r15)
	jmp	.L270
.L308:
	movq	%r12, %rdi
	call	freeaddrinfo@PLT
	jmp	.L274
.L303:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE295:
	.size	BIO_lookup_ex, .-BIO_lookup_ex
	.comm	bio_lookup_lock,8,8
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
