	.file	"e_sm4.c"
	.text
	.p2align 4
	.type	sm4_init_key, @function
sm4_init_key:
.LFB445:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	subq	$8, %rsp
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	SM4_set_key@PLT
	addq	$8, %rsp
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE445:
	.size	sm4_init_key, .-sm4_init_key
	.p2align 4
	.type	sm4_cfb128_cipher, @function
sm4_cfb128_cipher:
.LFB451:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movabsq	$4611686018427387903, %rax
	cmpq	%rax, %rcx
	ja	.L10
	testq	%rcx, %rcx
	je	.L7
	call	EVP_CIPHER_CTX_num@PLT
	movq	%r14, %rdi
	movl	%eax, -60(%rbp)
	call	EVP_CIPHER_CTX_encrypting@PLT
	movq	%r14, %rdi
	movl	%eax, -72(%rbp)
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movl	-72(%rbp), %edx
	leaq	-60(%rbp), %r9
	movq	%rbx, %r8
	pushq	SM4_encrypt@GOTPCREL(%rip)
	movq	%rax, %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	pushq	%rdx
	movq	%r15, %rdx
	call	CRYPTO_cfb128_encrypt@PLT
	movl	-60(%rbp), %esi
	movq	%r14, %rdi
	call	EVP_CIPHER_CTX_set_num@PLT
	popq	%rax
	popq	%rdx
.L7:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L17
	leaq	-40(%rbp), %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	.cfi_restore_state
	movabsq	$4611686018427387904, %rbx
	leaq	-60(%rbp), %rax
	movq	%rax, -88(%rbp)
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L18:
	cmpq	%rbx, %r15
	jb	.L7
.L5:
	movq	%r14, %rdi
	subq	%rbx, %r15
	call	EVP_CIPHER_CTX_num@PLT
	movq	%r14, %rdi
	movl	%eax, -60(%rbp)
	call	EVP_CIPHER_CTX_encrypting@PLT
	movq	%r14, %rdi
	movl	%eax, -76(%rbp)
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movq	%r14, %rdi
	movq	%rax, -72(%rbp)
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movl	-76(%rbp), %edx
	movq	-88(%rbp), %r9
	movq	%r13, %rsi
	pushq	SM4_encrypt@GOTPCREL(%rip)
	movq	-72(%rbp), %r8
	movq	%rax, %rcx
	movq	%r12, %rdi
	pushq	%rdx
	movq	%rbx, %rdx
	addq	%rbx, %r12
	addq	%rbx, %r13
	call	CRYPTO_cfb128_encrypt@PLT
	movl	-60(%rbp), %esi
	movq	%r14, %rdi
	call	EVP_CIPHER_CTX_set_num@PLT
	cmpq	%r15, %rbx
	popq	%rcx
	popq	%rsi
	cmova	%r15, %rbx
	testq	%r15, %r15
	jne	.L18
	jmp	.L7
.L17:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE451:
	.size	sm4_cfb128_cipher, .-sm4_cfb128_cipher
	.p2align 4
	.type	sm4_ofb_cipher, @function
sm4_ofb_cipher:
.LFB453:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	%rdx, -96(%rbp)
	movabsq	$4611686018427387903, %rdx
	movq	%rsi, -88(%rbp)
	movq	%fs:40, %rbx
	movq	%rbx, -56(%rbp)
	xorl	%ebx, %ebx
	movq	%rcx, %rbx
	cmpq	%rdx, %rcx
	jbe	.L20
	addq	%rcx, %rax
	movq	%rcx, -104(%rbp)
	leaq	(%rsi,%rcx), %rbx
	movq	%rcx, %r13
	movq	%rax, -80(%rbp)
	leaq	-60(%rbp), %r15
	movq	%rbx, -72(%rbp)
	movq	%rdx, %rbx
.L21:
	movq	%r12, %rdi
	call	EVP_CIPHER_CTX_num@PLT
	movq	%r12, %rdi
	movl	%eax, -60(%rbp)
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	subq	$8, %rsp
	movq	-72(%rbp), %rsi
	movq	%r15, %r9
	pushq	SM4_encrypt@GOTPCREL(%rip)
	movq	-80(%rbp), %rdi
	movq	%rax, %rcx
	movq	%r14, %r8
	movabsq	$4611686018427387904, %rdx
	subq	%r13, %rsi
	subq	%r13, %rdi
	call	CRYPTO_ofb128_encrypt@PLT
	movl	-60(%rbp), %esi
	movq	%r12, %rdi
	call	EVP_CIPHER_CTX_set_num@PLT
	popq	%rcx
	popq	%rsi
	movabsq	$-4611686018427387904, %rax
	addq	%rax, %r13
	cmpq	%rbx, %r13
	ja	.L21
	movq	-104(%rbp), %r13
	leaq	0(%r13,%rax), %r11
	andq	%r13, %rbx
	shrq	$62, %r11
	addq	$1, %r11
	salq	$62, %r11
	addq	%r11, -96(%rbp)
	addq	%r11, -88(%rbp)
.L20:
	testq	%rbx, %rbx
	jne	.L31
.L22:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L32
	leaq	-40(%rbp), %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L31:
	.cfi_restore_state
	movq	%r12, %rdi
	call	EVP_CIPHER_CTX_num@PLT
	movq	%r12, %rdi
	movl	%eax, -60(%rbp)
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	subq	$8, %rsp
	movq	-88(%rbp), %rsi
	movq	%rbx, %rdx
	pushq	SM4_encrypt@GOTPCREL(%rip)
	movq	-96(%rbp), %rdi
	movq	%rax, %rcx
	movq	%r14, %r8
	leaq	-60(%rbp), %r9
	call	CRYPTO_ofb128_encrypt@PLT
	movl	-60(%rbp), %esi
	movq	%r12, %rdi
	call	EVP_CIPHER_CTX_set_num@PLT
	popq	%rax
	popq	%rdx
	jmp	.L22
.L32:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE453:
	.size	sm4_ofb_cipher, .-sm4_ofb_cipher
	.p2align 4
	.type	sm4_ecb_cipher, @function
sm4_ecb_cipher:
.LFB452:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%rsi, -56(%rbp)
	call	EVP_CIPHER_CTX_cipher@PLT
	movslq	4(%rax), %r15
	cmpq	%r12, %r15
	ja	.L34
	subq	%r15, %r12
	movq	%r12, -64(%rbp)
	xorl	%r12d, %r12d
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L40:
	call	SM4_encrypt@PLT
	addq	%r15, %r12
	cmpq	%r12, -64(%rbp)
	jb	.L34
.L37:
	movq	%rbx, %rdi
	call	EVP_CIPHER_CTX_encrypting@PLT
	movq	%rbx, %rdi
	movl	%eax, %r13d
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	leaq	(%r14,%r12), %rdi
	movq	%rax, %rdx
	movq	-56(%rbp), %rax
	leaq	(%rax,%r12), %rsi
	testl	%r13d, %r13d
	jne	.L40
	call	SM4_decrypt@PLT
	addq	%r15, %r12
	cmpq	%r12, -64(%rbp)
	jnb	.L37
.L34:
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE452:
	.size	sm4_ecb_cipher, .-sm4_ecb_cipher
	.p2align 4
	.type	sm4_ctr_cipher, @function
sm4_ctr_cipher:
.LFB458:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	EVP_CIPHER_CTX_num@PLT
	movq	%r12, %rdi
	movl	%eax, -60(%rbp)
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	EVP_CIPHER_CTX_buf_noconst@PLT
	movq	%r12, %rdi
	movq	%rax, -72(%rbp)
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movq	-72(%rbp), %r9
	movq	%rbx, %rcx
	movq	%r15, %rdx
	movq	%rax, %r8
	leaq	-60(%rbp), %rax
	pushq	SM4_encrypt@GOTPCREL(%rip)
	movq	%r14, %rsi
	pushq	%rax
	movq	%r13, %rdi
	call	CRYPTO_ctr128_encrypt@PLT
	movl	-60(%rbp), %esi
	movq	%r12, %rdi
	call	EVP_CIPHER_CTX_set_num@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L44
	leaq	-40(%rbp), %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L44:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE458:
	.size	sm4_ctr_cipher, .-sm4_ctr_cipher
	.p2align 4
	.type	sm4_cbc_cipher, @function
sm4_cbc_cipher:
.LFB450:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	%rdx, -96(%rbp)
	movabsq	$4611686018427387903, %rdx
	movq	%rsi, -88(%rbp)
	cmpq	%rdx, %rcx
	jbe	.L46
	addq	%rcx, %rsi
	addq	%rcx, %rax
	movq	%rdx, -64(%rbp)
	movq	%rcx, %r14
	movq	%rsi, -72(%rbp)
	movq	%rax, -80(%rbp)
	movq	%rcx, -104(%rbp)
.L49:
	movq	%r12, %rdi
	movq	-72(%rbp), %r15
	movq	-80(%rbp), %r13
	call	EVP_CIPHER_CTX_encrypting@PLT
	movq	%r12, %rdi
	movl	%eax, -52(%rbp)
	subq	%r14, %r15
	subq	%r14, %r13
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movl	-52(%rbp), %edx
	movq	%rax, %rcx
	testl	%edx, %edx
	je	.L47
	movq	SM4_encrypt@GOTPCREL(%rip), %r9
	movq	%rbx, %r8
	movq	%r15, %rsi
	movq	%r13, %rdi
	movabsq	$4611686018427387904, %rdx
	call	CRYPTO_cbc128_encrypt@PLT
.L48:
	movabsq	$-4611686018427387904, %rax
	addq	%rax, %r14
	cmpq	-64(%rbp), %r14
	ja	.L49
	movq	-104(%rbp), %rbx
	movq	-64(%rbp), %r13
	andq	%rbx, %r13
	addq	%rax, %rbx
	shrq	$62, %rbx
	addq	$1, %rbx
	salq	$62, %rbx
	addq	%rbx, -96(%rbp)
	addq	%rbx, -88(%rbp)
.L46:
	testq	%r13, %r13
	jne	.L59
.L50:
	addq	$72, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L47:
	.cfi_restore_state
	movq	SM4_decrypt@GOTPCREL(%rip), %r9
	movq	%rbx, %r8
	movq	%r15, %rsi
	movq	%r13, %rdi
	movabsq	$4611686018427387904, %rdx
	call	CRYPTO_cbc128_decrypt@PLT
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L59:
	movq	%r12, %rdi
	call	EVP_CIPHER_CTX_encrypting@PLT
	movq	%r12, %rdi
	movl	%eax, %r15d
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	%rax, %rcx
	testl	%r15d, %r15d
	je	.L51
	movq	-88(%rbp), %rsi
	movq	-96(%rbp), %rdi
	movq	%rbx, %r8
	movq	%r13, %rdx
	movq	SM4_encrypt@GOTPCREL(%rip), %r9
	call	CRYPTO_cbc128_encrypt@PLT
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L51:
	movq	-88(%rbp), %rsi
	movq	-96(%rbp), %rdi
	movq	%rbx, %r8
	movq	%r13, %rdx
	movq	SM4_decrypt@GOTPCREL(%rip), %r9
	call	CRYPTO_cbc128_decrypt@PLT
	jmp	.L50
	.cfi_endproc
.LFE450:
	.size	sm4_cbc_cipher, .-sm4_cbc_cipher
	.p2align 4
	.globl	EVP_sm4_cbc
	.type	EVP_sm4_cbc, @function
EVP_sm4_cbc:
.LFB454:
	.cfi_startproc
	endbr64
	leaq	sm4_cbc(%rip), %rax
	ret
	.cfi_endproc
.LFE454:
	.size	EVP_sm4_cbc, .-EVP_sm4_cbc
	.p2align 4
	.globl	EVP_sm4_cfb128
	.type	EVP_sm4_cfb128, @function
EVP_sm4_cfb128:
.LFB455:
	.cfi_startproc
	endbr64
	leaq	sm4_cfb128(%rip), %rax
	ret
	.cfi_endproc
.LFE455:
	.size	EVP_sm4_cfb128, .-EVP_sm4_cfb128
	.p2align 4
	.globl	EVP_sm4_ofb
	.type	EVP_sm4_ofb, @function
EVP_sm4_ofb:
.LFB456:
	.cfi_startproc
	endbr64
	leaq	sm4_ofb(%rip), %rax
	ret
	.cfi_endproc
.LFE456:
	.size	EVP_sm4_ofb, .-EVP_sm4_ofb
	.p2align 4
	.globl	EVP_sm4_ecb
	.type	EVP_sm4_ecb, @function
EVP_sm4_ecb:
.LFB457:
	.cfi_startproc
	endbr64
	leaq	sm4_ecb(%rip), %rax
	ret
	.cfi_endproc
.LFE457:
	.size	EVP_sm4_ecb, .-EVP_sm4_ecb
	.p2align 4
	.globl	EVP_sm4_ctr
	.type	EVP_sm4_ctr, @function
EVP_sm4_ctr:
.LFB459:
	.cfi_startproc
	endbr64
	leaq	sm4_ctr_mode(%rip), %rax
	ret
	.cfi_endproc
.LFE459:
	.size	EVP_sm4_ctr, .-EVP_sm4_ctr
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	sm4_ctr_mode, @object
	.size	sm4_ctr_mode, 88
sm4_ctr_mode:
	.long	1139
	.long	1
	.long	16
	.long	16
	.quad	5
	.quad	sm4_init_key
	.quad	sm4_ctr_cipher
	.quad	0
	.long	128
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.align 32
	.type	sm4_ecb, @object
	.size	sm4_ecb, 88
sm4_ecb:
	.long	1133
	.long	16
	.long	16
	.long	0
	.quad	4097
	.quad	sm4_init_key
	.quad	sm4_ecb_cipher
	.quad	0
	.long	128
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.align 32
	.type	sm4_ofb, @object
	.size	sm4_ofb, 88
sm4_ofb:
	.long	1135
	.long	1
	.long	16
	.long	16
	.quad	4100
	.quad	sm4_init_key
	.quad	sm4_ofb_cipher
	.quad	0
	.long	128
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.align 32
	.type	sm4_cfb128, @object
	.size	sm4_cfb128, 88
sm4_cfb128:
	.long	1137
	.long	1
	.long	16
	.long	16
	.quad	4099
	.quad	sm4_init_key
	.quad	sm4_cfb128_cipher
	.quad	0
	.long	128
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.align 32
	.type	sm4_cbc, @object
	.size	sm4_cbc, 88
sm4_cbc:
	.long	1134
	.long	16
	.long	16
	.long	16
	.quad	4098
	.quad	sm4_init_key
	.quad	sm4_cbc_cipher
	.quad	0
	.long	128
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
