	.file	"cms_ess.c"
	.text
	.p2align 4
	.globl	d2i_CMS_ReceiptRequest
	.type	d2i_CMS_ReceiptRequest, @function
d2i_CMS_ReceiptRequest:
.LFB1440:
	.cfi_startproc
	endbr64
	leaq	CMS_ReceiptRequest_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE1440:
	.size	d2i_CMS_ReceiptRequest, .-d2i_CMS_ReceiptRequest
	.p2align 4
	.globl	i2d_CMS_ReceiptRequest
	.type	i2d_CMS_ReceiptRequest, @function
i2d_CMS_ReceiptRequest:
.LFB1441:
	.cfi_startproc
	endbr64
	leaq	CMS_ReceiptRequest_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE1441:
	.size	i2d_CMS_ReceiptRequest, .-i2d_CMS_ReceiptRequest
	.p2align 4
	.globl	CMS_ReceiptRequest_new
	.type	CMS_ReceiptRequest_new, @function
CMS_ReceiptRequest_new:
.LFB1442:
	.cfi_startproc
	endbr64
	leaq	CMS_ReceiptRequest_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE1442:
	.size	CMS_ReceiptRequest_new, .-CMS_ReceiptRequest_new
	.p2align 4
	.globl	CMS_ReceiptRequest_free
	.type	CMS_ReceiptRequest_free, @function
CMS_ReceiptRequest_free:
.LFB1443:
	.cfi_startproc
	endbr64
	leaq	CMS_ReceiptRequest_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE1443:
	.size	CMS_ReceiptRequest_free, .-CMS_ReceiptRequest_free
	.p2align 4
	.globl	CMS_get1_ReceiptRequest
	.type	CMS_get1_ReceiptRequest, @function
CMS_get1_ReceiptRequest:
.LFB1444:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	testq	%rsi, %rsi
	je	.L7
	movq	$0, (%rsi)
.L7:
	movl	$212, %edi
	call	OBJ_nid2obj@PLT
	movq	%r12, %rdi
	movl	$16, %ecx
	movl	$-3, %edx
	movq	%rax, %rsi
	call	CMS_signed_get0_data_by_OBJ@PLT
	movq	%rax, %rdi
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L6
	leaq	CMS_ReceiptRequest_it(%rip), %rsi
	call	ASN1_item_unpack@PLT
	testq	%rax, %rax
	je	.L11
	testq	%rbx, %rbx
	je	.L9
	movq	%rax, (%rbx)
	movl	$1, %eax
.L6:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	.cfi_restore_state
	movq	%rax, %rdi
	leaq	CMS_ReceiptRequest_it(%rip), %rsi
	call	ASN1_item_free@PLT
	movl	$1, %eax
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L11:
	popq	%rbx
	movl	$-1, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1444:
	.size	CMS_get1_ReceiptRequest, .-CMS_get1_ReceiptRequest
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/cms/cms_ess.c"
	.text
	.p2align 4
	.globl	CMS_ReceiptRequest_create0
	.type	CMS_ReceiptRequest_create0, @function
CMS_ReceiptRequest_create0:
.LFB1445:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	leaq	CMS_ReceiptRequest_it(%rip), %rdi
	pushq	%r14
	.cfi_offset 14, -32
	movl	%esi, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$24, %rsp
	movl	%edx, -52(%rbp)
	call	ASN1_item_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L18
	movq	(%rax), %rdi
	testq	%r15, %r15
	je	.L19
	movl	%r14d, %edx
	movq	%r15, %rsi
	call	ASN1_STRING_set0@PLT
.L20:
	movq	16(%r12), %rdi
	movq	GENERAL_NAMES_free@GOTPCREL(%rip), %rsi
	call	OPENSSL_sk_pop_free@PLT
	movq	%r13, 16(%r12)
	movq	8(%r12), %rax
	testq	%rbx, %rbx
	je	.L22
	movl	$1, (%rax)
	movq	%rbx, 8(%rax)
.L17:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	.cfi_restore_state
	xorl	%esi, %esi
	movl	$32, %edx
	call	ASN1_STRING_set@PLT
	testl	%eax, %eax
	je	.L18
	movq	(%r12), %rax
	movl	$32, %esi
	movq	8(%rax), %rdi
	call	RAND_bytes@PLT
	testl	%eax, %eax
	jg	.L20
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L22:
	movl	-52(%rbp), %ebx
	movl	$0, (%rax)
	movl	%ebx, 8(%rax)
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L18:
	movl	$80, %r8d
	movl	$65, %edx
	movl	$159, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L21:
	movq	%r12, %rdi
	leaq	CMS_ReceiptRequest_it(%rip), %rsi
	xorl	%r12d, %r12d
	call	ASN1_item_free@PLT
	jmp	.L17
	.cfi_endproc
.LFE1445:
	.size	CMS_ReceiptRequest_create0, .-CMS_ReceiptRequest_create0
	.p2align 4
	.globl	CMS_add1_ReceiptRequest
	.type	CMS_add1_ReceiptRequest, @function
CMS_add1_ReceiptRequest:
.LFB1446:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	CMS_ReceiptRequest_it(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%rsi, %rdi
	leaq	-32(%rbp), %rsi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	$0, -32(%rbp)
	call	ASN1_item_i2d@PLT
	testl	%eax, %eax
	js	.L34
	movq	-32(%rbp), %rcx
	movl	%eax, %r8d
	movl	$16, %edx
	movq	%r12, %rdi
	movl	$212, %esi
	call	CMS_signed_add1_attr_by_NID@PLT
	testl	%eax, %eax
	je	.L34
	movl	$1, %r12d
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L34:
	movl	$105, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r12d, %r12d
	movl	$158, %esi
	movl	$46, %edi
	call	ERR_put_error@PLT
.L33:
	movq	-32(%rbp), %rdi
	movl	$107, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L40
	addq	$24, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L40:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1446:
	.size	CMS_add1_ReceiptRequest, .-CMS_add1_ReceiptRequest
	.p2align 4
	.globl	CMS_ReceiptRequest_get0_values
	.type	CMS_ReceiptRequest_get0_values, @function
CMS_ReceiptRequest_get0_values:
.LFB1447:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L42
	movq	(%rdi), %rax
	movq	%rax, (%rsi)
.L42:
	movq	8(%rdi), %rax
	movl	(%rax), %esi
	testl	%esi, %esi
	jne	.L43
	testq	%rdx, %rdx
	je	.L44
	movl	8(%rax), %eax
	movl	%eax, (%rdx)
.L44:
	testq	%rcx, %rcx
	je	.L46
	movq	$0, (%rcx)
.L46:
	testq	%r8, %r8
	je	.L41
	movq	16(%rdi), %rax
	movq	%rax, (%r8)
.L41:
	ret
	.p2align 4,,10
	.p2align 3
.L43:
	testq	%rdx, %rdx
	je	.L47
	movl	$-1, (%rdx)
.L47:
	testq	%rcx, %rcx
	je	.L46
	movq	8(%rax), %rax
	movq	%rax, (%rcx)
	jmp	.L46
	.cfi_endproc
.LFE1447:
	.size	CMS_ReceiptRequest_get0_values, .-CMS_ReceiptRequest_get0_values
	.p2align 4
	.globl	cms_msgSigDigest_add1
	.type	cms_msgSigDigest_add1, @function
cms_msgSigDigest_add1:
.LFB1449:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	16(%rsi), %rax
	movq	(%rax), %rdi
	call	OBJ_obj2nid@PLT
	movl	%eax, %edi
	call	OBJ_nid2sn@PLT
	movq	%rax, %rdi
	call	EVP_get_digestbyname@PLT
	testq	%rax, %rax
	je	.L66
	leaq	-112(%rbp), %r13
	movq	24(%rbx), %rdx
	movq	%rax, %rsi
	leaq	-116(%rbp), %r8
	movq	%r13, %rcx
	leaq	CMS_Attributes_Verify_it(%rip), %rdi
	call	ASN1_item_digest@PLT
	testl	%eax, %eax
	je	.L66
	movl	-116(%rbp), %r8d
	movq	%r13, %rcx
	movl	$4, %edx
	movq	%r12, %rdi
	movl	$216, %esi
	call	CMS_signed_add1_attr_by_NID@PLT
	testl	%eax, %eax
	je	.L67
	movl	$1, %eax
.L65:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L77
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L66:
	.cfi_restore_state
	movl	$158, %r8d
	movl	$172, %edx
	movl	$162, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L67:
	movl	$163, %r8d
	movl	$65, %edx
	movl	$162, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	movl	%eax, -132(%rbp)
	call	ERR_put_error@PLT
	movl	-132(%rbp), %eax
	jmp	.L65
.L77:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1449:
	.size	cms_msgSigDigest_add1, .-cms_msgSigDigest_add1
	.p2align 4
	.globl	cms_Receipt_verify
	.type	cms_Receipt_verify, @function
cms_Receipt_verify:
.LFB1450:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%rsi, %rdi
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	CMS_get0_SignerInfos@PLT
	movq	%r13, %rdi
	movq	%rax, %r12
	call	CMS_get0_SignerInfos@PLT
	testq	%r12, %r12
	je	.L98
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L98
	movq	%rax, %rdi
	call	OPENSSL_sk_num@PLT
	movl	%eax, %r15d
	cmpl	$1, %eax
	je	.L80
	movl	$190, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$164, %edx
.L121:
	movl	$160, %esi
	movl	$46, %edi
	xorl	%r12d, %r12d
	xorl	%r13d, %r13d
	call	ERR_put_error@PLT
	xorl	%r15d, %r15d
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L98:
	xorl	%r12d, %r12d
	xorl	%r13d, %r13d
	xorl	%r15d, %r15d
.L79:
	leaq	CMS_ReceiptRequest_it(%rip), %rsi
	movq	%r12, %rdi
	call	ASN1_item_free@PLT
	leaq	CMS_Receipt_it(%rip), %rsi
	movq	%r13, %rdi
	call	ASN1_item_free@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L124
	addq	$120, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L80:
	.cfi_restore_state
	movq	%r13, %rdi
	call	CMS_get0_eContentType@PLT
	movq	%rax, %rdi
	call	OBJ_obj2nid@PLT
	cmpl	$204, %eax
	jne	.L125
	movq	%r13, %rdi
	call	CMS_get0_content@PLT
	testq	%rax, %rax
	je	.L82
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L82
	leaq	CMS_Receipt_it(%rip), %rsi
	xorl	%ebx, %ebx
	call	ASN1_item_unpack@PLT
	movl	$210, %r8d
	movl	$169, %edx
	movq	$0, -152(%rbp)
	movq	%rax, %r13
	leaq	.LC0(%rip), %rcx
	testq	%rax, %rax
	jne	.L84
	jmp	.L122
	.p2align 4,,10
	.p2align 3
.L86:
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	OPENSSL_sk_value@PLT
	movq	24(%r13), %rsi
	movq	40(%rax), %rdi
	movq	%rax, -152(%rbp)
	call	ASN1_STRING_cmp@PLT
	testl	%eax, %eax
	je	.L85
	addl	$1, %ebx
.L84:
	movq	%r12, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jl	.L86
.L85:
	movq	%r12, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	je	.L126
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	OPENSSL_sk_value@PLT
	movl	$216, %edi
	movq	%rax, %r12
	call	OBJ_nid2obj@PLT
	movq	%r12, %rdi
	movl	$4, %ecx
	movl	$-3, %edx
	movq	%rax, %rsi
	call	CMS_signed_get0_data_by_OBJ@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L127
	movq	-152(%rbp), %rbx
	movq	16(%rbx), %rax
	movq	(%rax), %rdi
	call	OBJ_obj2nid@PLT
	movl	%eax, %edi
	call	OBJ_nid2sn@PLT
	movq	%rax, %rdi
	call	EVP_get_digestbyname@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L89
	leaq	-128(%rbp), %r14
	movq	24(%rbx), %rdx
	leaq	-132(%rbp), %r8
	movq	%r14, %rcx
	leaq	CMS_Attributes_Verify_it(%rip), %rdi
	call	ASN1_item_digest@PLT
	testl	%eax, %eax
	je	.L89
	movl	-132(%rbp), %edx
	cmpl	%edx, (%r12)
	jne	.L128
	movq	8(%r12), %rsi
	movq	%r14, %rdi
	call	memcmp@PLT
	movl	$252, %r8d
	movl	$162, %edx
	leaq	.LC0(%rip), %rcx
	testl	%eax, %eax
	jne	.L122
	movl	$50, %edi
	call	OBJ_nid2obj@PLT
	movl	$6, %ecx
	movl	$-3, %edx
	movq	-152(%rbp), %rdi
	movq	%rax, %rsi
	call	CMS_signed_get0_data_by_OBJ@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L129
	movq	8(%r13), %rsi
	movq	%rax, %rdi
	call	OBJ_cmp@PLT
	movl	$270, %r8d
	movl	$171, %edx
	leaq	.LC0(%rip), %rcx
	testl	%eax, %eax
	jne	.L123
	movl	$212, %edi
	call	OBJ_nid2obj@PLT
	movl	$16, %ecx
	movl	$-3, %edx
	movq	-152(%rbp), %rdi
	movq	%rax, %rsi
	call	CMS_signed_get0_data_by_OBJ@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L95
	leaq	CMS_ReceiptRequest_it(%rip), %rsi
	call	ASN1_item_unpack@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L95
	movq	16(%r13), %rsi
	movq	(%rax), %rdi
	call	ASN1_STRING_cmp@PLT
	testl	%eax, %eax
	je	.L79
	movl	$283, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$170, %edx
	xorl	%r15d, %r15d
	movl	$160, %esi
	movl	$46, %edi
	call	ERR_put_error@PLT
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L125:
	movl	$196, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$165, %edx
	jmp	.L121
	.p2align 4,,10
	.p2align 3
.L126:
	movl	$223, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$166, %edx
.L122:
	movl	$160, %esi
	movl	$46, %edi
	xorl	%r12d, %r12d
	xorl	%r15d, %r15d
	call	ERR_put_error@PLT
	jmp	.L79
.L127:
	movl	$237, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$167, %edx
	xorl	%r15d, %r15d
	movl	$160, %esi
	movl	$46, %edi
	call	ERR_put_error@PLT
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L82:
	movl	$203, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$127, %edx
	jmp	.L121
	.p2align 4,,10
	.p2align 3
.L89:
	movl	$242, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$172, %edx
	jmp	.L122
	.p2align 4,,10
	.p2align 3
.L128:
	movl	$247, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$163, %edx
	jmp	.L122
.L95:
	movl	$277, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$168, %edx
.L123:
	movl	$160, %esi
	movl	$46, %edi
	xorl	%r15d, %r15d
	xorl	%r12d, %r12d
	call	ERR_put_error@PLT
	jmp	.L79
.L129:
	movl	$263, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$173, %edx
	xorl	%r15d, %r15d
	movl	$160, %esi
	movl	$46, %edi
	call	ERR_put_error@PLT
	jmp	.L79
.L124:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1450:
	.size	cms_Receipt_verify, .-cms_Receipt_verify
	.p2align 4
	.globl	cms_encode_Receipt
	.type	cms_encode_Receipt, @function
cms_encode_Receipt:
.LFB1451:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movl	$212, %edi
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	OBJ_nid2obj@PLT
	movl	$16, %ecx
	movl	$-3, %edx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	CMS_signed_get0_data_by_OBJ@PLT
	testq	%rax, %rax
	je	.L131
	movq	%rax, %rdi
	leaq	CMS_ReceiptRequest_it(%rip), %rsi
	call	ASN1_item_unpack@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L131
	movl	$50, %edi
	call	OBJ_nid2obj@PLT
	movl	$6, %ecx
	movl	$-3, %edx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	CMS_signed_get0_data_by_OBJ@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L144
	movq	%rax, -72(%rbp)
	movq	0(%r13), %rax
	xorl	%edx, %edx
	leaq	-80(%rbp), %rdi
	leaq	CMS_Receipt_it(%rip), %rsi
	movl	$1, -80(%rbp)
	movq	%rax, -64(%rbp)
	movq	40(%rbx), %rax
	movq	%rax, -56(%rbp)
	call	ASN1_item_pack@PLT
	movq	%rax, %r12
.L134:
	leaq	CMS_ReceiptRequest_it(%rip), %rsi
	movq	%r13, %rdi
	call	ASN1_item_free@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L145
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L131:
	.cfi_restore_state
	movl	$168, %edx
	movl	$161, %esi
	xorl	%r13d, %r13d
	xorl	%r12d, %r12d
	movl	$313, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$46, %edi
	call	ERR_put_error@PLT
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L144:
	movl	$323, %r8d
	movl	$173, %edx
	movl	$161, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L134
.L145:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1451:
	.size	cms_encode_Receipt, .-cms_encode_Receipt
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
