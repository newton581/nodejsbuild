	.file	"dsa_pmeth.c"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"dsa_paramgen_bits"
.LC1:
	.string	"dsa_paramgen_q_bits"
.LC2:
	.string	"dsa_paramgen_md"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"../deps/openssl/openssl/crypto/dsa/dsa_pmeth.c"
	.text
	.p2align 4
	.type	pkey_dsa_ctrl_str, @function
pkey_dsa_ctrl_str:
.LFB835:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$18, %ecx
	movq	%rsi, %rax
	movq	%rdx, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	leaq	.LC0(%rip), %rdi
	pushq	%r12
	.cfi_offset 12, -32
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L10
	movl	$20, %ecx
	leaq	.LC1(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L11
	movl	$16, %ecx
	leaq	.LC2(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%r12b
	sbbb	$0, %r12b
	movsbl	%r12b, %r12d
	testl	%r12d, %r12d
	jne	.L6
	movq	%r8, %rdi
	call	EVP_get_digestbyname@PLT
	xorl	%r8d, %r8d
	movl	$4099, %ecx
	movq	%rax, %r9
	testq	%rax, %rax
	je	.L12
.L8:
	popq	%r12
	movq	%r13, %rdi
	movl	$2, %edx
	popq	%r13
	movl	$116, %esi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	EVP_PKEY_CTX_ctrl@PLT
	.p2align 4,,10
	.p2align 3
.L10:
	.cfi_restore_state
	movq	%r8, %rdi
	movl	$10, %edx
	xorl	%esi, %esi
	call	strtol@PLT
	xorl	%r9d, %r9d
	movl	$4097, %ecx
	movl	%eax, %r8d
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L12:
	movl	$187, %r8d
	movl	$106, %edx
	movl	$104, %esi
	movl	$10, %edi
	leaq	.LC3(%rip), %rcx
	call	ERR_put_error@PLT
.L1:
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	.cfi_restore_state
	movq	%r8, %rdi
	movl	$10, %edx
	xorl	%esi, %esi
	call	strtol@PLT
	xorl	%r9d, %r9d
	movl	$4098, %ecx
	movl	%eax, %r8d
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L6:
	movl	$-2, %r12d
	jmp	.L1
	.cfi_endproc
.LFE835:
	.size	pkey_dsa_ctrl_str, .-pkey_dsa_ctrl_str
	.p2align 4
	.type	pkey_dsa_ctrl, @function
pkey_dsa_ctrl:
.LFB834:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movl	%esi, %ebx
	subq	$8, %rsp
	movq	40(%rdi), %r13
	cmpl	$13, %esi
	jg	.L14
	movl	$-2, %eax
	testl	%esi, %esi
	jle	.L13
	cmpl	$13, %esi
	ja	.L30
	movl	%esi, %edx
	leaq	.L18(%rip), %rsi
	movslq	(%rsi,%rdx,4), %rax
	addq	%rsi, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L18:
	.long	.L30-.L18
	.long	.L20-.L18
	.long	.L19-.L18
	.long	.L30-.L18
	.long	.L30-.L18
	.long	.L27-.L18
	.long	.L30-.L18
	.long	.L27-.L18
	.long	.L30-.L18
	.long	.L30-.L18
	.long	.L30-.L18
	.long	.L27-.L18
	.long	.L30-.L18
	.long	.L17-.L18
	.text
.L27:
	movl	$1, %eax
.L13:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L79:
	.cfi_restore_state
	cmpl	$4097, %esi
	jne	.L30
	cmpl	$255, %edx
	jg	.L78
.L30:
	addq	$8, %rsp
	movl	$-2, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	.cfi_restore_state
	cmpl	$4098, %esi
	je	.L21
	cmpl	$4099, %esi
	jne	.L79
	movq	%rcx, %rdi
	call	EVP_MD_type@PLT
	cmpl	$64, %eax
	jne	.L80
.L24:
	movq	%r12, 8(%r13)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L17:
	.cfi_restore_state
	movq	24(%r13), %rax
	movq	%rax, (%rcx)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L20:
	.cfi_restore_state
	movq	%rcx, %rdi
	call	EVP_MD_type@PLT
	cmpl	$64, %eax
	je	.L25
	movq	%r12, %rdi
	call	EVP_MD_type@PLT
	cmpl	$116, %eax
	jne	.L81
.L25:
	movq	%r12, 24(%r13)
	addq	$8, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L19:
	.cfi_restore_state
	movl	$162, %r8d
	movl	$150, %edx
	movl	$120, %esi
	movl	$10, %edi
	leaq	.LC3(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$8, %rsp
	movl	$-2, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L78:
	.cfi_restore_state
	movl	%edx, 0(%r13)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L80:
	.cfi_restore_state
	movq	%r12, %rdi
	call	EVP_MD_type@PLT
	cmpl	$675, %eax
	je	.L24
	movq	%r12, %rdi
	call	EVP_MD_type@PLT
	movl	$128, %r8d
	cmpl	$672, %eax
	je	.L24
.L77:
	movl	$106, %edx
	movl	$120, %esi
	movl	$10, %edi
	leaq	.LC3(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L21:
	movl	%edx, %eax
	andl	$-65, %eax
	cmpl	$160, %eax
	je	.L23
	testl	$-257, %edx
	jne	.L30
.L23:
	movl	%edx, 4(%r13)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L81:
	.cfi_restore_state
	movq	%r12, %rdi
	call	EVP_MD_type@PLT
	cmpl	$66, %eax
	je	.L25
	movq	%r12, %rdi
	call	EVP_MD_type@PLT
	cmpl	$675, %eax
	je	.L25
	movq	%r12, %rdi
	call	EVP_MD_type@PLT
	cmpl	$672, %eax
	je	.L25
	movq	%r12, %rdi
	call	EVP_MD_type@PLT
	cmpl	$673, %eax
	je	.L25
	movq	%r12, %rdi
	call	EVP_MD_type@PLT
	cmpl	$674, %eax
	je	.L25
	movq	%r12, %rdi
	call	EVP_MD_type@PLT
	cmpl	$1096, %eax
	je	.L25
	movq	%r12, %rdi
	call	EVP_MD_type@PLT
	cmpl	$1097, %eax
	je	.L25
	movq	%r12, %rdi
	call	EVP_MD_type@PLT
	cmpl	$1098, %eax
	je	.L25
	movq	%r12, %rdi
	call	EVP_MD_type@PLT
	cmpl	$1099, %eax
	je	.L25
	movl	$146, %r8d
	jmp	.L77
	.cfi_endproc
.LFE834:
	.size	pkey_dsa_ctrl, .-pkey_dsa_ctrl
	.p2align 4
	.type	pkey_dsa_verify, @function
pkey_dsa_verify:
.LFB833:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$8, %rsp
	movq	16(%rdi), %rax
	movq	40(%rax), %r15
	movq	40(%rdi), %rax
	movq	24(%rax), %rdi
	testq	%rdi, %rdi
	je	.L84
	call	EVP_MD_size@PLT
	cltq
	cmpq	%rbx, %rax
	je	.L84
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L84:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r15, %r9
	movl	%r13d, %r8d
	movq	%r12, %rcx
	movl	%ebx, %edx
	movq	%r14, %rsi
	popq	%rbx
	xorl	%edi, %edi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	DSA_verify@PLT
	.cfi_endproc
.LFE833:
	.size	pkey_dsa_verify, .-pkey_dsa_verify
	.p2align 4
	.type	pkey_dsa_sign, @function
pkey_dsa_sign:
.LFB832:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	40(%rax), %r15
	movq	40(%rdi), %rax
	movq	24(%rax), %rdi
	testq	%rdi, %rdi
	je	.L90
	call	EVP_MD_size@PLT
	movslq	%eax, %rdx
	xorl	%eax, %eax
	cmpq	%rbx, %rdx
	je	.L90
.L86:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L95
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L90:
	.cfi_restore_state
	xorl	%edi, %edi
	movq	%r15, %r9
	leaq	-60(%rbp), %r8
	movq	%r13, %rcx
	movl	%ebx, %edx
	movq	%r14, %rsi
	call	DSA_sign@PLT
	testl	%eax, %eax
	jle	.L86
	movl	-60(%rbp), %eax
	movq	%rax, (%r12)
	movl	$1, %eax
	jmp	.L86
.L95:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE832:
	.size	pkey_dsa_sign, .-pkey_dsa_sign
	.p2align 4
	.type	pkey_dsa_paramgen, @function
pkey_dsa_paramgen:
.LFB836:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	cmpq	$0, 56(%rdi)
	movq	40(%rdi), %rbx
	je	.L101
	movq	%rdi, %r12
	xorl	%r15d, %r15d
	call	BN_GENCB_new@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L96
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	evp_pkey_set_cb_translate@PLT
	call	DSA_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L105
.L99:
	movq	8(%rbx), %rcx
	movslq	4(%rbx), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movslq	(%rbx), %rsi
	pushq	%r13
	movq	%rax, %rdi
	pushq	$0
	pushq	$0
	pushq	$0
	call	dsa_builtin_paramgen@PLT
	addq	$32, %rsp
	movq	%r13, %rdi
	movl	%eax, %r15d
	call	BN_GENCB_free@PLT
	testl	%r15d, %r15d
	je	.L100
	movq	%r12, %rdx
	movl	$116, %esi
	movq	%r14, %rdi
	call	EVP_PKEY_assign@PLT
.L96:
	leaq	-40(%rbp), %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L100:
	.cfi_restore_state
	movq	%r12, %rdi
	call	DSA_free@PLT
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L101:
	call	DSA_new@PLT
	xorl	%r13d, %r13d
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L99
.L105:
	movq	%r13, %rdi
	xorl	%r15d, %r15d
	call	BN_GENCB_free@PLT
	jmp	.L96
	.cfi_endproc
.LFE836:
	.size	pkey_dsa_paramgen, .-pkey_dsa_paramgen
	.p2align 4
	.type	pkey_dsa_cleanup, @function
pkey_dsa_cleanup:
.LFB831:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rdi
	movl	$68, %edx
	leaq	.LC3(%rip), %rsi
	jmp	CRYPTO_free@PLT
	.cfi_endproc
.LFE831:
	.size	pkey_dsa_cleanup, .-pkey_dsa_cleanup
	.p2align 4
	.type	pkey_dsa_init, @function
pkey_dsa_init:
.LFB829:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$34, %edx
	leaq	.LC3(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$32, %edi
	subq	$8, %rsp
	call	CRYPTO_malloc@PLT
	testq	%rax, %rax
	je	.L109
	movabsq	$962072676352, %rcx
	movq	$0, 8(%rax)
	movq	%rcx, (%rax)
	movq	$0, 24(%rax)
	movq	%rax, 40(%rbx)
	addq	$16, %rax
	movq	%rax, 64(%rbx)
	movl	$1, %eax
	movl	$2, 72(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L109:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE829:
	.size	pkey_dsa_init, .-pkey_dsa_init
	.p2align 4
	.type	pkey_dsa_keygen, @function
pkey_dsa_keygen:
.LFB837:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	cmpq	$0, 16(%rdi)
	je	.L120
	movq	%rdi, %rbx
	movq	%rsi, %r12
	call	DSA_new@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L111
	movl	$116, %esi
	movq	%r12, %rdi
	call	EVP_PKEY_assign@PLT
	movq	16(%rbx), %rsi
	movq	%r12, %rdi
	call	EVP_PKEY_copy_parameters@PLT
	testl	%eax, %eax
	jne	.L121
.L111:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L121:
	.cfi_restore_state
	movq	40(%r12), %rdi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	DSA_generate_key@PLT
	.p2align 4,,10
	.p2align 3
.L120:
	.cfi_restore_state
	movl	$229, %r8d
	movl	$107, %edx
	movl	$121, %esi
	movl	$10, %edi
	leaq	.LC3(%rip), %rcx
	call	ERR_put_error@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE837:
	.size	pkey_dsa_keygen, .-pkey_dsa_keygen
	.p2align 4
	.type	pkey_dsa_copy, @function
pkey_dsa_copy:
.LFB830:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$34, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	leaq	.LC3(%rip), %rsi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movl	$32, %edi
	call	CRYPTO_malloc@PLT
	testq	%rax, %rax
	je	.L124
	leaq	16(%rax), %rdx
	movq	$0, 8(%rax)
	movabsq	$962072676352, %rsi
	movq	%rsi, (%rax)
	movq	$0, 24(%rax)
	movq	%rax, 40(%rbx)
	movq	%rdx, 64(%rbx)
	movq	40(%r12), %rdx
	movl	$2, 72(%rbx)
	movl	(%rdx), %ecx
	movl	%ecx, (%rax)
	movl	4(%rdx), %ecx
	movl	%ecx, 4(%rax)
	movq	8(%rdx), %rcx
	movq	24(%rdx), %rdx
	movq	%rcx, 8(%rax)
	movq	%rdx, 24(%rax)
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L124:
	.cfi_restore_state
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE830:
	.size	pkey_dsa_copy, .-pkey_dsa_copy
	.globl	dsa_pkey_meth
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	dsa_pkey_meth, @object
	.size	dsa_pkey_meth, 256
dsa_pkey_meth:
	.long	116
	.long	2
	.quad	pkey_dsa_init
	.quad	pkey_dsa_copy
	.quad	pkey_dsa_cleanup
	.quad	0
	.quad	pkey_dsa_paramgen
	.quad	0
	.quad	pkey_dsa_keygen
	.quad	0
	.quad	pkey_dsa_sign
	.quad	0
	.quad	pkey_dsa_verify
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	pkey_dsa_ctrl
	.quad	pkey_dsa_ctrl_str
	.zero	48
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
