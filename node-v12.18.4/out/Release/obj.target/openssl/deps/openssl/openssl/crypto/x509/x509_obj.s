	.file	"x509_obj.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/x509/x509_obj.c"
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"NO X509_NAME"
	.text
	.p2align 4
	.globl	X509_NAME_oneline
	.type	X509_NAME_oneline, @function
X509_NAME_oneline:
.LFB781:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$184, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -168(%rbp)
	movq	%rsi, -216(%rbp)
	movl	%edx, -192(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L62
	movl	-192(%rbp), %ebx
	testl	%ebx, %ebx
	je	.L35
	cmpq	$0, -168(%rbp)
	movq	$0, -176(%rbp)
	je	.L63
.L5:
	leaq	-144(%rbp), %rax
	xorl	%ebx, %ebx
	xorl	%r14d, %r14d
	movq	%rax, -208(%rbp)
	leaq	hex.16914(%rip), %r13
	.p2align 4,,10
	.p2align 3
.L8:
	movq	-168(%rbp), %rax
	movq	(%rax), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r14d
	jge	.L64
	movq	-168(%rbp), %rax
	movl	%r14d, %esi
	movq	(%rax), %rdi
	call	OPENSSL_sk_value@PLT
	movq	(%rax), %rdi
	movq	%rax, %r12
	call	OBJ_obj2nid@PLT
	movl	%eax, %edi
	testl	%eax, %eax
	je	.L11
	call	OBJ_nid2sn@PLT
	movq	%rax, %r9
	testq	%rax, %rax
	je	.L11
.L10:
	movq	%r9, %rdi
	movq	%r9, -184(%rbp)
	call	strlen@PLT
	movq	-184(%rbp), %r9
	movq	%rax, %rcx
	movq	8(%r12), %rax
	movl	(%rax), %r8d
	movl	4(%rax), %edx
	cmpl	$1048576, %r8d
	jg	.L65
	movq	8(%rax), %r10
	cmpl	$27, %edx
	je	.L66
.L14:
	movdqa	.LC2(%rip), %xmm1
	movaps	%xmm1, -160(%rbp)
	testl	%r8d, %r8d
	jle	.L38
.L69:
	leal	-1(%r8), %edi
	xorl	%eax, %eax
	xorl	%edx, %edx
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L39:
	movq	%rsi, %rax
.L22:
	movq	%rax, %rsi
	andl	$3, %esi
	movl	-160(%rbp,%rsi,4), %r11d
	testl	%r11d, %r11d
	je	.L20
	movzbl	(%r10,%rax), %esi
	leal	1(%rdx), %r11d
	addl	$4, %edx
	subl	$32, %esi
	cmpb	$94, %sil
	cmovbe	%r11d, %edx
.L20:
	leaq	1(%rax), %rsi
	cmpq	%rdi, %rax
	jne	.L39
.L19:
	leal	2(%rdx,%rcx), %r10d
	leal	(%r10,%rbx), %r15d
	cmpl	$1048576, %r15d
	jg	.L67
	movq	-176(%rbp), %rax
	testq	%rax, %rax
	je	.L24
	leal	1(%r15), %esi
	movq	%rax, %rdi
	movq	%rcx, -200(%rbp)
	movslq	%esi, %rsi
	movl	%r8d, -188(%rbp)
	movq	%r9, -184(%rbp)
	call	BUF_MEM_grow@PLT
	testq	%rax, %rax
	je	.L3
	movq	-176(%rbp), %rax
	movq	-184(%rbp), %r9
	movl	-188(%rbp), %r8d
	movq	-200(%rbp), %rcx
	addq	8(%rax), %rbx
.L25:
	movb	$47, (%rbx)
	leaq	1(%rbx), %rdi
	movl	%ecx, %edx
	movq	%r9, %rsi
	movl	%r8d, -188(%rbp)
	movq	%rcx, -184(%rbp)
	call	memcpy@PLT
	movq	-184(%rbp), %rcx
	movl	-188(%rbp), %r8d
	movslq	%ecx, %rcx
	addq	%rax, %rcx
	movb	$61, (%rcx)
	movq	8(%r12), %rax
	leaq	1(%rcx), %rsi
	movq	8(%rax), %rdi
	testl	%r8d, %r8d
	jle	.L28
	subl	$1, %r8d
	xorl	%eax, %eax
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L68:
	sarl	$4, %edx
	andl	$15, %ecx
	movl	$30812, %r9d
	addq	$4, %rsi
	movslq	%edx, %rdx
	movw	%r9w, -4(%rsi)
	movzbl	0(%r13,%rdx), %edx
	movb	%dl, -2(%rsi)
	movzbl	0(%r13,%rcx), %edx
	movb	%dl, -1(%rsi)
.L29:
	leaq	1(%rax), %rdx
	cmpq	%rax, %r8
	je	.L28
.L40:
	movq	%rdx, %rax
.L31:
	movq	%rax, %rdx
	andl	$3, %edx
	movl	-160(%rbp,%rdx,4), %r10d
	testl	%r10d, %r10d
	je	.L29
	movzbl	(%rdi,%rax), %edx
	leal	-32(%rdx), %r9d
	movl	%edx, %ecx
	cmpl	$94, %r9d
	ja	.L68
	movb	%dl, (%rsi)
	addq	$1, %rsi
	leaq	1(%rax), %rdx
	cmpq	%rax, %r8
	jne	.L40
.L28:
	movb	$0, (%rsi)
	addl	$1, %r14d
	movslq	%r15d, %rbx
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L11:
	movq	-208(%rbp), %r15
	movq	(%r12), %rdx
	movl	$80, %esi
	movq	%r15, %rdi
	call	i2t_ASN1_OBJECT@PLT
	movq	%r15, %r9
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L24:
	cmpl	-192(%rbp), %r15d
	jge	.L33
	addq	-216(%rbp), %rbx
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L66:
	testb	$3, %r8b
	jne	.L14
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -160(%rbp)
	testl	%r8d, %r8d
	jle	.L15
	leal	-1(%r8), %esi
	xorl	%eax, %eax
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L37:
	movq	%rdx, %rax
.L17:
	cmpb	$0, (%r10,%rax)
	je	.L16
	movq	%rax, %rdx
	andl	$3, %edx
	movl	$1, -160(%rbp,%rdx,4)
.L16:
	leaq	1(%rax), %rdx
	cmpq	%rsi, %rax
	jne	.L37
	movl	-160(%rbp), %eax
	orl	-156(%rbp), %eax
	orl	-152(%rbp), %eax
	jne	.L14
.L15:
	movl	$1, -148(%rbp)
	testl	%r8d, %r8d
	jg	.L69
	.p2align 4,,10
	.p2align 3
.L38:
	xorl	%edx, %edx
	jmp	.L19
.L62:
	call	BUF_MEM_new@PLT
	movq	%rax, -176(%rbp)
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L3
	movl	$200, %esi
	movq	%rax, %rdi
	call	BUF_MEM_grow@PLT
	testq	%rax, %rax
	je	.L3
	movq	8(%rbx), %rax
	cmpq	$0, -168(%rbp)
	movb	$0, (%rax)
	je	.L4
	movl	$200, -192(%rbp)
	jmp	.L5
.L65:
	movl	$74, %r8d
.L60:
	movl	$134, %edx
	movl	$116, %esi
	movl	$11, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L13:
	movq	-176(%rbp), %rdi
	xorl	%r12d, %r12d
	call	BUF_MEM_free@PLT
.L1:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L70
	addq	$184, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L64:
	.cfi_restore_state
	movq	-176(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L33
	movl	$170, %edx
	leaq	.LC0(%rip), %rsi
	movq	8(%rdi), %r12
	call	CRYPTO_free@PLT
.L27:
	testl	%r14d, %r14d
	jne	.L1
	movb	$0, (%r12)
	jmp	.L1
.L67:
	movl	$123, %r8d
	jmp	.L60
.L35:
	xorl	%r12d, %r12d
	jmp	.L1
.L3:
	movl	$177, %r8d
	movl	$65, %edx
	movl	$116, %esi
	movl	$11, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L13
.L63:
	movslq	-192(%rbp), %rdx
	movq	%rsi, %r12
	leaq	-1(%rdx), %rbx
.L7:
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	strncpy@PLT
	movb	$0, (%r12,%rbx)
	jmp	.L1
.L33:
	movq	-216(%rbp), %r12
	jmp	.L27
.L4:
	movq	-176(%rbp), %rdi
	movl	$53, %edx
	movl	$199, %ebx
	leaq	.LC0(%rip), %rsi
	movq	8(%rdi), %r12
	call	CRYPTO_free@PLT
	movl	$200, %edx
	jmp	.L7
.L70:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE781:
	.size	X509_NAME_oneline, .-X509_NAME_oneline
	.section	.rodata
	.align 16
	.type	hex.16914, @object
	.size	hex.16914, 17
hex.16914:
	.string	"0123456789ABCDEF"
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC2:
	.long	1
	.long	1
	.long	1
	.long	1
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
