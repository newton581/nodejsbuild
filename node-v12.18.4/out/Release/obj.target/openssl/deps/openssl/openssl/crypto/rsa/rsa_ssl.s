	.file	"rsa_ssl.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/rsa/rsa_ssl.c"
	.text
	.p2align 4
	.globl	RSA_padding_add_SSLv23
	.type	RSA_padding_add_SSLv23, @function
RSA_padding_add_SSLv23:
.LFB455:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leal	-10(%rsi), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	cmpl	%ecx, %eax
	jle	.L16
	movl	$512, %eax
	leal	-11(%rsi), %r14d
	leaq	2(%rdi), %rbx
	movq	%rdi, %r15
	movw	%ax, (%rdi)
	subl	%ecx, %r14d
	movq	%rbx, %rdi
	movq	%rdx, %r13
	movl	%r14d, %esi
	movl	%ecx, %r12d
	call	RAND_bytes@PLT
	testl	%eax, %eax
	jle	.L7
	testl	%r14d, %r14d
	jle	.L10
	leal	-1(%r14), %eax
	leaq	3(%r15,%rax), %r14
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L17:
	addq	$1, %rbx
	cmpq	%r14, %rbx
	je	.L5
	.p2align 4,,10
	.p2align 3
.L9:
	cmpb	$0, (%rbx)
	je	.L8
	addq	$1, %rbx
	cmpq	%r14, %rbx
	jne	.L9
.L5:
	movb	$0, 8(%r14)
	leaq	9(%r14), %rdi
	movl	%r12d, %edx
	movq	%r13, %rsi
	movabsq	$217020518514230019, %rax
	movq	%rax, (%r14)
	call	memcpy@PLT
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	.cfi_restore_state
	cmpb	$0, (%rbx)
	jne	.L17
.L8:
	movl	$1, %esi
	movq	%rbx, %rdi
	call	RAND_bytes@PLT
	testl	%eax, %eax
	jg	.L18
.L7:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	.cfi_restore_state
	movl	$24, %r8d
	movl	$110, %edx
	movl	$110, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L10:
	.cfi_restore_state
	movq	%rbx, %r14
	jmp	.L5
	.cfi_endproc
.LFE455:
	.size	RSA_padding_add_SSLv23, .-RSA_padding_add_SSLv23
	.p2align 4
	.globl	RSA_padding_check_SSLv23
	.type	RSA_padding_check_SSLv23, @function
RSA_padding_check_SSLv23:
.LFB456:
	.cfi_startproc
	endbr64
	testl	%esi, %esi
	jle	.L45
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%ecx, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testl	%ecx, %ecx
	jle	.L35
	movl	%r8d, %r14d
	cmpl	%r8d, %ecx
	jg	.L36
	cmpl	$10, %r8d
	jle	.L36
	movslq	%r8d, %rax
	movl	%esi, -60(%rbp)
	movq	%rdi, %r12
	movq	%rdx, %rbx
	leaq	.LC0(%rip), %rsi
	movl	$78, %edx
	movq	%rax, %rdi
	movq	%rax, -56(%rbp)
	call	CRYPTO_malloc@PLT
	movl	-60(%rbp), %r9d
	testq	%rax, %rax
	je	.L49
	movq	-56(%rbp), %rdi
	movslq	%r13d, %rdx
	addq	%rdx, %rbx
	leaq	(%rax,%rdi), %rcx
	movq	%rcx, %rdx
	.p2align 4,,10
	.p2align 3
.L24:
	movl	%r13d, %esi
	leal	-1(%r13), %eax
	subq	$1, %rdx
	notl	%esi
	andl	%eax, %esi
	sarl	$31, %esi
	movl	%esi, %eax
	addl	$1, %esi
	subq	%rsi, %rbx
	subl	%esi, %r13d
	movl	%ecx, %esi
	notl	%eax
	subl	%edx, %esi
	andb	(%rbx), %al
	movb	%al, (%rdx)
	cmpl	%esi, %r14d
	jg	.L24
	leal	-1(%r14), %r8d
	testl	%r14d, %r14d
	movzbl	%al, %eax
	movl	$2, %r10d
	movq	$-1, %rdx
	notq	%r8
	cmovle	%rdx, %r8
	xorl	%ebx, %ebx
	xorl	%edi, %edi
	xorl	%r15d, %r15d
	addq	%rcx, %r8
	movzbl	1(%r8), %edx
	xorl	$2, %edx
	movzbl	%dl, %edx
	leal	-1(%rdx), %r13d
	notl	%edx
	andl	%r13d, %edx
	movl	%eax, %r13d
	subl	$1, %eax
	notl	%r13d
	andl	%r13d, %eax
	andl	%edx, %eax
	movl	$-1, %edx
	movl	%eax, %r13d
	sarl	$31, %r13d
	movl	%r13d, %r11d
	notl	%r11d
	.p2align 4,,10
	.p2align 3
.L25:
	movzbl	(%r8,%r10), %eax
	movl	%eax, %esi
	movl	%eax, %ecx
	subl	$1, %eax
	notl	%esi
	xorl	$3, %ecx
	andl	%eax, %esi
	movl	%edx, %eax
	movzbl	%cl, %ecx
	sarl	$31, %esi
	andl	%esi, %eax
	orl	%esi, %r15d
	movl	%eax, %edx
	notl	%eax
	andl	%r10d, %edx
	andl	%eax, %ebx
	addq	$1, %r10
	orl	%edx, %ebx
	movl	%r15d, %edx
	notl	%edx
	movl	%edx, %eax
	andl	$1, %eax
	addl	%edi, %eax
	movl	%ecx, %edi
	subl	$1, %ecx
	notl	%edi
	andl	%edi, %ecx
	movl	%ecx, %edi
	sarl	$31, %edi
	orl	%r15d, %edi
	andl	%eax, %edi
	cmpl	%r10d, %r14d
	jg	.L25
	leal	-10(%rbx), %eax
	leal	-8(%rdi), %ecx
	movl	%edi, %r10d
	movl	%eax, %edx
	movl	%eax, -68(%rbp)
	movl	%ebx, %eax
	xorl	$8, %r10d
	xorl	$10, %eax
	xorl	$10, %edx
	xorl	$8, %ecx
	orl	%eax, %edx
	orl	%r10d, %ecx
	xorl	%ebx, %edx
	xorl	%ecx, %edi
	addl	$1, %ebx
	sarl	$31, %edx
	sarl	$31, %edi
	movl	%edx, %eax
	movl	%edi, %r10d
	andl	%r13d, %edx
	notl	%eax
	notl	%r10d
	movl	%edx, %esi
	andl	%r13d, %eax
	notl	%esi
	andl	%eax, %r10d
	andl	%edi, %eax
	movl	%r14d, %edi
	subl	%ebx, %edi
	movl	%eax, %ecx
	movl	%edi, %ebx
	movl	%r9d, %edi
	notl	%ecx
	subl	%ebx, %edi
	movl	%ebx, %r13d
	xorl	%r9d, %r13d
	xorl	%ebx, %edi
	orl	%r13d, %edi
	xorl	%r9d, %edi
	sarl	$31, %edi
	movl	%edi, %r13d
	andl	%r10d, %edi
	movl	%edi, %r15d
	andl	%esi, %r11d
	notl	%r13d
	andl	$107, %r11d
	andl	%r10d, %r13d
	notl	%r15d
	andl	$113, %edx
	andl	$115, %eax
	andl	$109, %edi
	orl	%r11d, %edx
	movl	$1, %r11d
	andl	%edx, %ecx
	orl	%ecx, %eax
	andl	%eax, %r15d
	movl	%r9d, %eax
	orl	%edi, %r15d
	movl	%r15d, -64(%rbp)
	leal	-11(%r14), %r15d
	movl	%r15d, %esi
	xorl	%r15d, %eax
	subl	%r9d, %esi
	xorl	%r9d, %esi
	orl	%eax, %esi
	xorl	%r15d, %esi
	sarl	$31, %esi
	movl	%esi, %eax
	notl	%esi
	andl	%r15d, %eax
	andl	%esi, %r9d
	orl	%r9d, %eax
	movl	%eax, -60(%rbp)
	leaq	12(%r8), %rax
	cmpl	$1, %r15d
	jle	.L31
	movl	%ebx, -72(%rbp)
	movl	-68(%rbp), %ebx
	movl	%r13d, -76(%rbp)
	movq	%rax, %r13
	.p2align 4,,10
	.p2align 3
.L30:
	movl	%r11d, %eax
	movl	%r14d, %edx
	andl	%ebx, %eax
	subl	%r11d, %edx
	movl	%eax, %esi
	subl	$1, %eax
	notl	%esi
	andl	%eax, %esi
	sarl	$31, %esi
	movl	%esi, %edi
	notl	%edi
	cmpl	$11, %edx
	jle	.L33
	leal	-12(%rdx), %r9d
	leaq	11(%r8), %rax
	movslq	%r11d, %r10
	movzbl	%dil, %edi
	addq	%r13, %r9
	orl	$-256, %esi
	.p2align 4,,10
	.p2align 3
.L32:
	movzbl	(%rax,%r10), %edx
	movzbl	(%rax), %ecx
	addq	$1, %rax
	andl	%edi, %edx
	andl	%esi, %ecx
	orl	%ecx, %edx
	movb	%dl, -1(%rax)
	cmpq	%rax, %r9
	jne	.L32
.L33:
	addl	%r11d, %r11d
	cmpl	%r11d, %r15d
	jg	.L30
	movl	-72(%rbp), %ebx
	movl	-76(%rbp), %r13d
.L31:
	movl	-60(%rbp), %eax
	testl	%eax, %eax
	je	.L28
	movl	-60(%rbp), %eax
	movl	%ebx, %ecx
	xorl	%edx, %edx
	negl	%ecx
	leal	-1(%rax), %esi
	.p2align 4,,10
	.p2align 3
.L34:
	movl	%ebx, %eax
	movl	%ebx, %edi
	xorl	%ecx, %edi
	xorl	%edx, %eax
	addl	$1, %ecx
	orl	%edi, %eax
	movzbl	11(%r8,%rdx), %edi
	xorl	%edx, %eax
	sarl	$31, %eax
	andl	%r13d, %eax
	movzbl	%al, %eax
	movl	%eax, %r9d
	notl	%eax
	andl	%r9d, %edi
	movzbl	(%r12,%rdx), %r9d
	andl	%r9d, %eax
	orl	%edi, %eax
	movb	%al, (%r12,%rdx)
	movq	%rdx, %rax
	addq	$1, %rdx
	cmpq	%rsi, %rax
	jne	.L34
.L28:
	movq	-56(%rbp), %rsi
	movq	%r8, %rdi
	movl	$165, %ecx
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_clear_free@PLT
	movl	-64(%rbp), %edx
	movl	$166, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$114, %esi
	movl	$4, %edi
	call	ERR_put_error@PLT
	movl	%r13d, %edi
	andl	$1, %edi
	call	err_clear_last_constant_time@PLT
	movl	%r13d, %eax
	notl	%r13d
	andl	%ebx, %eax
	orl	%r13d, %eax
.L19:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L35:
	.cfi_restore_state
	movl	$-1, %eax
	jmp	.L19
.L36:
	movl	$74, %r8d
	movl	$111, %edx
	movl	$114, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-1, %eax
	jmp	.L19
.L45:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	orl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L49:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movl	$80, %r8d
	movl	$65, %edx
	movl	$114, %esi
	movl	$4, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-1, %eax
	jmp	.L19
	.cfi_endproc
.LFE456:
	.size	RSA_padding_check_SSLv23, .-RSA_padding_check_SSLv23
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
