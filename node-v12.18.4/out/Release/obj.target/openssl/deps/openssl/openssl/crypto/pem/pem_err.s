	.file	"pem_err.c"
	.text
	.p2align 4
	.globl	ERR_load_PEM_strings
	.type	ERR_load_PEM_strings, @function
ERR_load_PEM_strings:
.LFB203:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$151515136, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_func_error_string@PLT
	testq	%rax, %rax
	je	.L5
	movl	$1, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	.cfi_restore_state
	leaq	PEM_str_functs(%rip), %rdi
	call	ERR_load_strings_const@PLT
	leaq	PEM_str_reasons(%rip), %rdi
	call	ERR_load_strings_const@PLT
	movl	$1, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE203:
	.size	ERR_load_PEM_strings, .-ERR_load_PEM_strings
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"bad base64 decode"
.LC1:
	.string	"bad decrypt"
.LC2:
	.string	"bad end line"
.LC3:
	.string	"bad iv chars"
.LC4:
	.string	"bad magic number"
.LC5:
	.string	"bad password read"
.LC6:
	.string	"bad version number"
.LC7:
	.string	"bio write failure"
.LC8:
	.string	"cipher is null"
.LC9:
	.string	"error converting private key"
.LC10:
	.string	"expecting private key blob"
.LC11:
	.string	"expecting public key blob"
.LC12:
	.string	"header too long"
.LC13:
	.string	"inconsistent header"
.LC14:
	.string	"keyblob header parse error"
.LC15:
	.string	"keyblob too short"
.LC16:
	.string	"missing dek iv"
.LC17:
	.string	"not dek info"
.LC18:
	.string	"not encrypted"
.LC19:
	.string	"not proc type"
.LC20:
	.string	"no start line"
.LC21:
	.string	"problems getting password"
.LC22:
	.string	"pvk data too short"
.LC23:
	.string	"pvk too short"
.LC24:
	.string	"read key"
.LC25:
	.string	"short header"
.LC26:
	.string	"unexpected dek iv"
.LC27:
	.string	"unsupported cipher"
.LC28:
	.string	"unsupported encryption"
.LC29:
	.string	"unsupported key components"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	PEM_str_reasons, @object
	.size	PEM_str_reasons, 496
PEM_str_reasons:
	.quad	150995044
	.quad	.LC0
	.quad	150995045
	.quad	.LC1
	.quad	150995046
	.quad	.LC2
	.quad	150995047
	.quad	.LC3
	.quad	150995060
	.quad	.LC4
	.quad	150995048
	.quad	.LC5
	.quad	150995061
	.quad	.LC6
	.quad	150995062
	.quad	.LC7
	.quad	150995071
	.quad	.LC8
	.quad	150995059
	.quad	.LC9
	.quad	150995063
	.quad	.LC10
	.quad	150995064
	.quad	.LC11
	.quad	150995072
	.quad	.LC12
	.quad	150995065
	.quad	.LC13
	.quad	150995066
	.quad	.LC14
	.quad	150995067
	.quad	.LC15
	.quad	150995073
	.quad	.LC16
	.quad	150995049
	.quad	.LC17
	.quad	150995050
	.quad	.LC18
	.quad	150995051
	.quad	.LC19
	.quad	150995052
	.quad	.LC20
	.quad	150995053
	.quad	.LC21
	.quad	150995068
	.quad	.LC22
	.quad	150995069
	.quad	.LC23
	.quad	150995055
	.quad	.LC24
	.quad	150995056
	.quad	.LC25
	.quad	150995074
	.quad	.LC26
	.quad	150995057
	.quad	.LC27
	.quad	150995058
	.quad	.LC28
	.quad	150995070
	.quad	.LC29
	.quad	0
	.quad	0
	.section	.rodata.str1.1
.LC30:
	.string	"b2i_dss"
.LC31:
	.string	"b2i_PVK_bio"
.LC32:
	.string	"b2i_rsa"
.LC33:
	.string	"check_bitlen_dsa"
.LC34:
	.string	"check_bitlen_rsa"
.LC35:
	.string	"d2i_PKCS8PrivateKey_bio"
.LC36:
	.string	"d2i_PKCS8PrivateKey_fp"
.LC37:
	.string	"do_b2i"
.LC38:
	.string	"do_b2i_bio"
.LC39:
	.string	"do_blob_header"
.LC40:
	.string	"do_i2b"
.LC41:
	.string	"do_pk8pkey"
.LC42:
	.string	"do_pk8pkey_fp"
.LC43:
	.string	"do_PVK_body"
.LC44:
	.string	"do_PVK_header"
.LC45:
	.string	"get_header_and_data"
.LC46:
	.string	"get_name"
.LC47:
	.string	"i2b_PVK"
.LC48:
	.string	"i2b_PVK_bio"
.LC49:
	.string	"load_iv"
.LC50:
	.string	"PEM_ASN1_read"
.LC51:
	.string	"PEM_ASN1_read_bio"
.LC52:
	.string	"PEM_ASN1_write"
.LC53:
	.string	"PEM_ASN1_write_bio"
.LC54:
	.string	"PEM_def_callback"
.LC55:
	.string	"PEM_do_header"
.LC56:
	.string	"PEM_get_EVP_CIPHER_INFO"
.LC57:
	.string	"PEM_read"
.LC58:
	.string	"PEM_read_bio"
.LC59:
	.string	"PEM_read_bio_DHparams"
.LC60:
	.string	"PEM_read_bio_ex"
.LC61:
	.string	"PEM_read_bio_Parameters"
.LC62:
	.string	"PEM_read_bio_PrivateKey"
.LC63:
	.string	"PEM_read_DHparams"
.LC64:
	.string	"PEM_read_PrivateKey"
.LC65:
	.string	"PEM_SignFinal"
.LC66:
	.string	"PEM_write"
.LC67:
	.string	"PEM_write_bio"
.LC68:
	.string	"PEM_write_PrivateKey"
.LC69:
	.string	"PEM_X509_INFO_read"
.LC70:
	.string	"PEM_X509_INFO_read_bio"
.LC71:
	.string	"PEM_X509_INFO_write_bio"
	.section	.data.rel.ro.local
	.align 32
	.type	PEM_str_functs, @object
	.size	PEM_str_functs, 688
PEM_str_functs:
	.quad	151515136
	.quad	.LC30
	.quad	151519232
	.quad	.LC31
	.quad	151523328
	.quad	.LC32
	.quad	151527424
	.quad	.LC33
	.quad	151531520
	.quad	.LC34
	.quad	151486464
	.quad	.LC35
	.quad	151490560
	.quad	.LC36
	.quad	151535616
	.quad	.LC37
	.quad	151539712
	.quad	.LC38
	.quad	151543808
	.quad	.LC39
	.quad	151592960
	.quad	.LC40
	.quad	151511040
	.quad	.LC41
	.quad	151506944
	.quad	.LC42
	.quad	151547904
	.quad	.LC43
	.quad	151552000
	.quad	.LC44
	.quad	151580672
	.quad	.LC45
	.quad	151584768
	.quad	.LC46
	.quad	151556096
	.quad	.LC47
	.quad	151560192
	.quad	.LC48
	.quad	151408640
	.quad	.LC49
	.quad	151412736
	.quad	.LC50
	.quad	151416832
	.quad	.LC51
	.quad	151420928
	.quad	.LC52
	.quad	151425024
	.quad	.LC53
	.quad	151404544
	.quad	.LC54
	.quad	151429120
	.quad	.LC55
	.quad	151433216
	.quad	.LC56
	.quad	151437312
	.quad	.LC57
	.quad	151441408
	.quad	.LC58
	.quad	151572480
	.quad	.LC59
	.quad	151588864
	.quad	.LC60
	.quad	151568384
	.quad	.LC61
	.quad	151498752
	.quad	.LC62
	.quad	151576576
	.quad	.LC63
	.quad	151502848
	.quad	.LC64
	.quad	151453696
	.quad	.LC65
	.quad	151457792
	.quad	.LC66
	.quad	151461888
	.quad	.LC67
	.quad	151564288
	.quad	.LC68
	.quad	151465984
	.quad	.LC69
	.quad	151470080
	.quad	.LC70
	.quad	151474176
	.quad	.LC71
	.quad	0
	.quad	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
