	.file	"ts_req_print.c"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"yes"
.LC1:
	.string	"no"
.LC2:
	.string	"Version: %d\n"
.LC3:
	.string	"Policy OID: "
.LC4:
	.string	"unspecified\n"
.LC5:
	.string	"Nonce: "
.LC6:
	.string	"unspecified"
.LC7:
	.string	"\n"
.LC8:
	.string	"Certificate required: %s\n"
	.text
	.p2align 4
	.globl	TS_REQ_print_bio
	.type	TS_REQ_print_bio, @function
TS_REQ_print_bio:
.LFB1368:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L11
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	call	TS_REQ_get_version@PLT
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	8(%rbx), %rsi
	movq	%r12, %rdi
	call	TS_MSG_IMPRINT_print_bio@PLT
	leaq	.LC3(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	%rbx, %rdi
	call	TS_REQ_get_policy_id@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L17
	movq	%r12, %rdi
	call	TS_OBJ_print_bio@PLT
.L4:
	leaq	.LC5(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	24(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L18
	movq	%r12, %rdi
	call	TS_ASN1_INTEGER_print_bio@PLT
.L6:
	movq	%r12, %rdi
	movl	$1, %edx
	leaq	.LC7(%rip), %rsi
	call	BIO_write@PLT
	movl	32(%rbx), %eax
	movq	%r12, %rdi
	leaq	.LC0(%rip), %rdx
	leaq	.LC8(%rip), %rsi
	testl	%eax, %eax
	leaq	.LC1(%rip), %rax
	cmove	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	40(%rbx), %rsi
	movq	%r12, %rdi
	call	TS_ext_print_bio@PLT
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	.cfi_restore_state
	leaq	.LC4(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L11:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	leaq	.LC6(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L6
	.cfi_endproc
.LFE1368:
	.size	TS_REQ_print_bio, .-TS_REQ_print_bio
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
