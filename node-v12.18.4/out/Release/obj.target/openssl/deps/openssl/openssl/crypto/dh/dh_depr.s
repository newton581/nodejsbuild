	.file	"dh_depr.c"
	.text
	.p2align 4
	.globl	DH_generate_parameters
	.type	DH_generate_parameters, @function
DH_generate_parameters:
.LFB419:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%esi, %r14d
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$24, %rsp
	movl	%edi, -52(%rbp)
	call	DH_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1
	call	BN_GENCB_new@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L9
	movq	%rax, %rdi
	movq	%rbx, %rdx
	movq	%r15, %rsi
	call	BN_GENCB_set_old@PLT
	movl	-52(%rbp), %esi
	movq	%r12, %rdi
	movq	%r13, %rcx
	movl	%r14d, %edx
	call	DH_generate_parameters_ex@PLT
	movq	%r13, %rdi
	testl	%eax, %eax
	je	.L4
	call	BN_GENCB_free@PLT
.L1:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	.cfi_restore_state
	call	BN_GENCB_free@PLT
.L9:
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	DH_free@PLT
	jmp	.L1
	.cfi_endproc
.LFE419:
	.size	DH_generate_parameters, .-DH_generate_parameters
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
