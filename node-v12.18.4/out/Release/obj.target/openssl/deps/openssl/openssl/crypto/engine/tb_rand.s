	.file	"tb_rand.c"
	.text
	.p2align 4
	.type	engine_unregister_all_RAND, @function
engine_unregister_all_RAND:
.LFB867:
	.cfi_startproc
	endbr64
	leaq	rand_table(%rip), %rdi
	jmp	engine_table_cleanup@PLT
	.cfi_endproc
.LFE867:
	.size	engine_unregister_all_RAND, .-engine_unregister_all_RAND
	.p2align 4
	.globl	ENGINE_unregister_RAND
	.type	ENGINE_unregister_RAND, @function
ENGINE_unregister_RAND:
.LFB866:
	.cfi_startproc
	endbr64
	movq	%rdi, %rsi
	leaq	rand_table(%rip), %rdi
	jmp	engine_table_unregister@PLT
	.cfi_endproc
.LFE866:
	.size	ENGINE_unregister_RAND, .-ENGINE_unregister_RAND
	.p2align 4
	.globl	ENGINE_register_RAND
	.type	ENGINE_register_RAND, @function
ENGINE_register_RAND:
.LFB868:
	.cfi_startproc
	endbr64
	cmpq	$0, 48(%rdi)
	jne	.L6
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	movq	%rdi, %rdx
	xorl	%r9d, %r9d
	movl	$1, %r8d
	leaq	dummy_nid(%rip), %rcx
	leaq	engine_unregister_all_RAND(%rip), %rsi
	leaq	rand_table(%rip), %rdi
	jmp	engine_table_register@PLT
	.cfi_endproc
.LFE868:
	.size	ENGINE_register_RAND, .-ENGINE_register_RAND
	.p2align 4
	.globl	ENGINE_register_all_RAND
	.type	ENGINE_register_all_RAND, @function
ENGINE_register_all_RAND:
.LFB869:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	call	ENGINE_get_first@PLT
	testq	%rax, %rax
	je	.L7
	movq	%rax, %r12
	leaq	dummy_nid(%rip), %rbx
	.p2align 4,,10
	.p2align 3
.L11:
	cmpq	$0, 48(%r12)
	je	.L9
	xorl	%r9d, %r9d
	movl	$1, %r8d
	movq	%rbx, %rcx
	movq	%r12, %rdx
	leaq	engine_unregister_all_RAND(%rip), %rsi
	leaq	rand_table(%rip), %rdi
	call	engine_table_register@PLT
.L9:
	movq	%r12, %rdi
	call	ENGINE_get_next@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L11
.L7:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE869:
	.size	ENGINE_register_all_RAND, .-ENGINE_register_all_RAND
	.p2align 4
	.globl	ENGINE_set_default_RAND
	.type	ENGINE_set_default_RAND, @function
ENGINE_set_default_RAND:
.LFB870:
	.cfi_startproc
	endbr64
	cmpq	$0, 48(%rdi)
	jne	.L21
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	movq	%rdi, %rdx
	movl	$1, %r9d
	movl	$1, %r8d
	leaq	dummy_nid(%rip), %rcx
	leaq	engine_unregister_all_RAND(%rip), %rsi
	leaq	rand_table(%rip), %rdi
	jmp	engine_table_register@PLT
	.cfi_endproc
.LFE870:
	.size	ENGINE_set_default_RAND, .-ENGINE_set_default_RAND
	.p2align 4
	.globl	ENGINE_get_default_RAND
	.type	ENGINE_get_default_RAND, @function
ENGINE_get_default_RAND:
.LFB871:
	.cfi_startproc
	endbr64
	movl	$1, %esi
	leaq	rand_table(%rip), %rdi
	jmp	engine_table_select@PLT
	.cfi_endproc
.LFE871:
	.size	ENGINE_get_default_RAND, .-ENGINE_get_default_RAND
	.p2align 4
	.globl	ENGINE_get_RAND
	.type	ENGINE_get_RAND, @function
ENGINE_get_RAND:
.LFB872:
	.cfi_startproc
	endbr64
	movq	48(%rdi), %rax
	ret
	.cfi_endproc
.LFE872:
	.size	ENGINE_get_RAND, .-ENGINE_get_RAND
	.p2align 4
	.globl	ENGINE_set_RAND
	.type	ENGINE_set_RAND, @function
ENGINE_set_RAND:
.LFB873:
	.cfi_startproc
	endbr64
	movq	%rsi, 48(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE873:
	.size	ENGINE_set_RAND, .-ENGINE_set_RAND
	.section	.rodata
	.align 4
	.type	dummy_nid, @object
	.size	dummy_nid, 4
dummy_nid:
	.long	1
	.local	rand_table
	.comm	rand_table,8,8
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
