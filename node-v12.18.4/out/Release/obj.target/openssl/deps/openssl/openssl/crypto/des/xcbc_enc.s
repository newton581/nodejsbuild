	.file	"xcbc_enc.c"
	.text
	.p2align 4
	.globl	DES_xcbc_encrypt
	.type	DES_xcbc_encrypt, @function
DES_xcbc_encrypt:
.LFB54:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$120, %rsp
	movq	%rdx, -120(%rbp)
	movq	16(%rbp), %rdx
	movq	%r8, -144(%rbp)
	movq	%rdi, -128(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	(%r9), %eax
	movl	%eax, -68(%rbp)
	movl	4(%r9), %eax
	movl	%eax, -72(%rbp)
	movl	(%rdx), %eax
	movl	%eax, -76(%rbp)
	movl	4(%rdx), %eax
	movl	24(%rbp), %edx
	movl	%eax, -80(%rbp)
	movq	%r8, %rax
	movl	(%r8), %r8d
	movl	4(%rax), %ecx
	leaq	-8(%rbx), %rax
	movq	%rax, -136(%rbp)
	testl	%edx, %edx
	je	.L2
	testq	%rax, %rax
	js	.L31
	shrq	$3, %rax
	leaq	-64(%rbp), %rbx
	movl	%r8d, %edx
	movq	%rsi, %r15
	movq	%rax, -88(%rbp)
	leaq	8(,%rax,8), %rax
	movq	%rax, -96(%rbp)
	leaq	(%rsi,%rax), %r14
	movq	%rbx, %rax
	movq	%rdi, %rbx
	movq	%rax, %r13
	.p2align 4,,10
	.p2align 3
.L4:
	movl	(%rbx), %edi
	xorl	-68(%rbp), %edi
	addq	$8, %r15
	addq	$8, %rbx
	xorl	%edi, %edx
	movl	-4(%rbx), %esi
	xorl	-72(%rbp), %esi
	movq	%r13, %rdi
	xorl	%esi, %ecx
	movl	%edx, -64(%rbp)
	movq	%r12, %rsi
	movl	$1, %edx
	movl	%ecx, -60(%rbp)
	call	DES_encrypt1@PLT
	movl	-76(%rbp), %edx
	xorl	-64(%rbp), %edx
	movl	%edx, %ecx
	movzbl	%dh, %eax
	movb	%dl, -8(%r15)
	shrl	$16, %ecx
	movb	%al, -7(%r15)
	movb	%cl, -6(%r15)
	movl	%edx, %ecx
	shrl	$24, %ecx
	movb	%cl, -5(%r15)
	movl	-80(%rbp), %ecx
	xorl	-60(%rbp), %ecx
	movl	%ecx, %esi
	movb	%cl, -4(%r15)
	movzbl	%ch, %eax
	shrl	$16, %esi
	movb	%al, -3(%r15)
	movb	%sil, -2(%r15)
	movl	%ecx, %esi
	shrl	$24, %esi
	movb	%sil, -1(%r15)
	cmpq	%r14, %r15
	jne	.L4
	movl	%edx, %r8d
	movq	-88(%rbp), %rdx
	movq	-120(%rbp), %rax
	movq	%r15, %r13
	movq	-96(%rbp), %rdi
	addq	%rdi, -128(%rbp)
	negq	%rdx
	salq	$3, %rdx
	leaq	-16(%rax,%rdx), %rdi
	addq	-136(%rbp), %rdx
	movq	%rdx, -120(%rbp)
.L3:
	cmpq	$-8, %rdi
	je	.L50
	movq	-128(%rbp), %rax
	leaq	8(%rax,%rdi), %rdx
	movq	-120(%rbp), %rax
	cmpq	$7, %rax
	ja	.L32
	leaq	.L9(%rip), %r9
	movslq	(%r9,%rax,4), %rdi
	addq	%r9, %rdi
	notrack jmp	*%rdi
	.section	.rodata
	.align 4
	.align 4
.L9:
	.long	.L32-.L9
	.long	.L33-.L9
	.long	.L34-.L9
	.long	.L35-.L9
	.long	.L36-.L9
	.long	.L37-.L9
	.long	.L38-.L9
	.long	.L8-.L9
	.text
	.p2align 4,,10
	.p2align 3
.L2:
	cmpq	$0, -136(%rbp)
	jle	.L17
	leaq	-9(%rbx), %rdx
	leaq	-64(%rbp), %rbx
	movq	%r12, -96(%rbp)
	movl	%ecx, %r13d
	shrq	$3, %rdx
	movq	%rbx, -104(%rbp)
	movl	%r8d, %r12d
	movq	%rdi, %rbx
	leaq	8(,%rdx,8), %rax
	movq	%rdx, -160(%rbp)
	movq	%rax, -152(%rbp)
	addq	%rsi, %rax
	movq	%rax, -112(%rbp)
	movq	%rsi, %rax
	.p2align 4,,10
	.p2align 3
.L18:
	movl	-76(%rbp), %edx
	movl	%r12d, %r15d
	movl	(%rbx), %r12d
	movl	%r13d, %r14d
	movl	4(%rbx), %r13d
	movq	-96(%rbp), %rsi
	movq	%rax, -88(%rbp)
	addq	$8, %rbx
	xorl	%r12d, %edx
	movq	-104(%rbp), %rdi
	movl	%edx, -64(%rbp)
	movl	-80(%rbp), %edx
	xorl	%r13d, %edx
	movl	%edx, -60(%rbp)
	xorl	%edx, %edx
	call	DES_encrypt1@PLT
	movl	-68(%rbp), %edx
	xorl	-64(%rbp), %edx
	xorl	%edx, %r15d
	movl	-72(%rbp), %edx
	xorl	-60(%rbp), %edx
	movq	-88(%rbp), %rax
	xorl	%edx, %r14d
	movl	%r15d, %edx
	movl	%r15d, %ecx
	shrl	$16, %edx
	movb	%dl, 2(%rax)
	movl	%r14d, %edx
	addq	$8, %rax
	movb	%r15b, -8(%rax)
	shrl	$16, %edx
	shrl	$24, %r15d
	movb	%ch, -7(%rax)
	movl	%r14d, %ecx
	movb	%r14b, -4(%rax)
	shrl	$24, %r14d
	movb	%r15b, -5(%rax)
	movb	%ch, -3(%rax)
	movb	%dl, -2(%rax)
	movb	%r14b, -1(%rax)
	cmpq	-112(%rbp), %rax
	jne	.L18
	movq	-160(%rbp), %rdx
	movl	%r13d, %ecx
	movq	%rax, %r13
	movq	-120(%rbp), %rax
	movq	-152(%rbp), %rdi
	addq	%rdi, -128(%rbp)
	movl	%r12d, %r8d
	negq	%rdx
	movq	-96(%rbp), %r12
	movq	-104(%rbp), %rbx
	salq	$3, %rdx
	leaq	-16(%rax,%rdx), %r15
	addq	-136(%rbp), %rdx
	movq	%rdx, -120(%rbp)
.L19:
	movq	-128(%rbp), %rdi
	movl	-76(%rbp), %eax
	xorl	%edx, %edx
	movq	%r12, %rsi
	movl	%ecx, -96(%rbp)
	movl	(%rdi), %r14d
	movl	4(%rdi), %r9d
	movq	%rbx, %rdi
	movl	%r8d, -88(%rbp)
	xorl	%r14d, %eax
	movl	%r9d, -76(%rbp)
	movl	%eax, -64(%rbp)
	movl	-80(%rbp), %eax
	xorl	%r9d, %eax
	movl	%eax, -60(%rbp)
	call	DES_encrypt1@PLT
	movl	-88(%rbp), %r8d
	movl	-96(%rbp), %ecx
	movq	-120(%rbp), %rdi
	movl	-68(%rbp), %eax
	movl	-72(%rbp), %edx
	xorl	-64(%rbp), %eax
	xorl	-60(%rbp), %edx
	xorl	%eax, %r8d
	movl	-76(%rbp), %r9d
	leaq	8(%r13,%r15), %rax
	xorl	%edx, %ecx
	cmpq	$8, %rdi
	ja	.L39
	leaq	.L22(%rip), %rsi
	movslq	(%rsi,%rdi,4), %rdx
	addq	%rsi, %rdx
	notrack jmp	*%rdx
	.section	.rodata
	.align 4
	.align 4
.L22:
	.long	.L39-.L22
	.long	.L29-.L22
	.long	.L40-.L22
	.long	.L41-.L22
	.long	.L26-.L22
	.long	.L42-.L22
	.long	.L24-.L22
	.long	.L23-.L22
	.long	.L21-.L22
	.text
	.p2align 4,,10
	.p2align 3
.L17:
	cmpq	$-8, -136(%rbp)
	jne	.L51
.L20:
	movq	-144(%rbp), %rax
	movl	%r8d, (%rax)
	movl	%ecx, 4(%rax)
.L1:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L52
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L21:
	.cfi_restore_state
	movl	%ecx, %edx
	subq	$1, %rax
	shrl	$24, %edx
	movb	%dl, (%rax)
.L23:
	movl	%ecx, %edx
	subq	$1, %rax
	shrl	$16, %edx
	movb	%dl, (%rax)
.L24:
	movb	%ch, -1(%rax)
	leaq	-1(%rax), %rdx
.L25:
	movb	%cl, -1(%rdx)
	leaq	-1(%rdx), %rax
.L26:
	movl	%r8d, %edx
	leaq	-1(%rax), %rcx
	shrl	$24, %edx
	movb	%dl, -1(%rax)
.L27:
	movl	%r8d, %eax
	leaq	-1(%rcx), %rdx
	shrl	$16, %eax
	movb	%al, -1(%rcx)
.L28:
	movl	%r8d, %ebx
	leaq	-1(%rdx), %rax
	movb	%bh, -1(%rdx)
.L29:
	movb	%r8b, -1(%rax)
	movl	%r9d, %ecx
	movl	%r14d, %r8d
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L31:
	movq	%rax, %rdi
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L50:
	movl	%r8d, %eax
	movl	%r8d, %esi
	movl	%ecx, %edx
	movl	%r8d, %r9d
	shrl	$16, %esi
	movzbl	%ah, %edi
	movl	%ecx, %r11d
	movzbl	%ch, %eax
	shrl	$24, %r8d
	shrl	$16, %edx
	shrl	$24, %ecx
.L6:
	movq	-144(%rbp), %rbx
	movb	%r9b, (%rbx)
	movb	%dil, 1(%rbx)
	movb	%sil, 2(%rbx)
	movb	%r8b, 3(%rbx)
	movb	%r11b, 4(%rbx)
	movb	%al, 5(%rbx)
	movb	%dl, 6(%rbx)
	movb	%cl, 7(%rbx)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L38:
	xorl	%edi, %edi
.L10:
	movzbl	-1(%rdx), %r9d
	subq	$1, %rdx
	sall	$8, %r9d
	orl	%edi, %r9d
.L11:
	leaq	-1(%rdx), %rdi
	movzbl	-1(%rdx), %edx
	orl	%edx, %r9d
.L12:
	movzbl	-1(%rdi), %r10d
	leaq	-1(%rdi), %rdx
	sall	$24, %r10d
	movl	%r10d, %r14d
.L13:
	movzbl	-1(%rdx), %edi
	leaq	-1(%rdx), %r11
	sall	$16, %edi
	orl	%r14d, %edi
	movl	%edi, %r10d
.L14:
	movzbl	-1(%r11), %edi
	leaq	-1(%r11), %rdx
	sall	$8, %edi
	orl	%r10d, %edi
.L15:
	movzbl	-1(%rdx), %edx
	orl	%edx, %edi
.L7:
	xorl	-72(%rbp), %ecx
	xorl	-68(%rbp), %r8d
	movl	$1, %edx
	movq	%r12, %rsi
	xorl	%edi, %r8d
	xorl	%ecx, %r9d
	leaq	-64(%rbp), %rdi
	movl	%r8d, -64(%rbp)
	movl	%r9d, -60(%rbp)
	call	DES_encrypt1@PLT
	movl	-76(%rbp), %edx
	xorl	-64(%rbp), %edx
	movl	%edx, %eax
	movl	-80(%rbp), %ebx
	xorl	-60(%rbp), %ebx
	movl	%edx, %esi
	shrl	$24, %eax
	movl	%edx, 0(%r13)
	movl	%edx, %r9d
	movzbl	%dh, %edi
	movl	%ebx, %ecx
	movl	%ebx, %edx
	movl	%eax, %r8d
	movl	%ebx, 4(%r13)
	shrl	$16, %esi
	movl	%ebx, %r11d
	movzbl	%bh, %eax
	shrl	$16, %edx
	shrl	$24, %ecx
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L33:
	xorl	%r9d, %r9d
	xorl	%edi, %edi
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L34:
	movq	%rdx, %r11
	xorl	%r9d, %r9d
	xorl	%r10d, %r10d
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L35:
	xorl	%r9d, %r9d
	xorl	%r14d, %r14d
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L36:
	movq	%rdx, %rdi
	xorl	%r9d, %r9d
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L37:
	xorl	%r9d, %r9d
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L8:
	movzbl	-1(%rdx), %r9d
	subq	$1, %rdx
	movl	%r9d, %edi
	sall	$16, %edi
	jmp	.L10
.L42:
	movq	%rax, %rdx
	jmp	.L25
.L41:
	movq	%rax, %rcx
	jmp	.L27
.L40:
	movq	%rax, %rdx
	jmp	.L28
.L52:
	call	__stack_chk_fail@PLT
.L51:
	movq	-136(%rbp), %r15
	leaq	-64(%rbp), %rbx
	jmp	.L19
.L32:
	xorl	%r9d, %r9d
	xorl	%edi, %edi
	jmp	.L7
.L39:
	movl	%r9d, %ecx
	movl	%r14d, %r8d
	jmp	.L20
	.cfi_endproc
.LFE54:
	.size	DES_xcbc_encrypt, .-DES_xcbc_encrypt
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
