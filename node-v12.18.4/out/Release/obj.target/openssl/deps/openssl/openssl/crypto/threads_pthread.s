	.file	"threads_pthread.c"
	.text
	.p2align 4
	.type	fork_once_func, @function
fork_once_func:
.LFB275:
	.cfi_startproc
	endbr64
	movq	OPENSSL_fork_child@GOTPCREL(%rip), %rdx
	movq	OPENSSL_fork_parent@GOTPCREL(%rip), %rsi
	movq	OPENSSL_fork_prepare@GOTPCREL(%rip), %rdi
	jmp	pthread_atfork@PLT
	.cfi_endproc
.LFE275:
	.size	fork_once_func, .-fork_once_func
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/threads_pthread.c"
	.text
	.p2align 4
	.globl	CRYPTO_THREAD_lock_new
	.type	CRYPTO_THREAD_lock_new, @function
CRYPTO_THREAD_lock_new:
.LFB262:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$29, %edx
	movl	$56, %edi
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	CRYPTO_zalloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L3
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	pthread_rwlock_init@PLT
	testl	%eax, %eax
	jne	.L12
.L3:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	.cfi_restore_state
	movq	%r12, %rdi
	movl	$35, %edx
	leaq	.LC0(%rip), %rsi
	xorl	%r12d, %r12d
	call	CRYPTO_free@PLT
	jmp	.L3
	.cfi_endproc
.LFE262:
	.size	CRYPTO_THREAD_lock_new, .-CRYPTO_THREAD_lock_new
	.p2align 4
	.globl	CRYPTO_THREAD_read_lock
	.type	CRYPTO_THREAD_read_lock, @function
CRYPTO_THREAD_read_lock:
.LFB263:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	pthread_rwlock_rdlock@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	testl	%eax, %eax
	sete	%al
	movzbl	%al, %eax
	ret
	.cfi_endproc
.LFE263:
	.size	CRYPTO_THREAD_read_lock, .-CRYPTO_THREAD_read_lock
	.p2align 4
	.globl	CRYPTO_THREAD_write_lock
	.type	CRYPTO_THREAD_write_lock, @function
CRYPTO_THREAD_write_lock:
.LFB264:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	pthread_rwlock_wrlock@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	testl	%eax, %eax
	sete	%al
	movzbl	%al, %eax
	ret
	.cfi_endproc
.LFE264:
	.size	CRYPTO_THREAD_write_lock, .-CRYPTO_THREAD_write_lock
	.p2align 4
	.globl	CRYPTO_THREAD_unlock
	.type	CRYPTO_THREAD_unlock, @function
CRYPTO_THREAD_unlock:
.LFB265:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	pthread_rwlock_unlock@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	testl	%eax, %eax
	sete	%al
	movzbl	%al, %eax
	ret
	.cfi_endproc
.LFE265:
	.size	CRYPTO_THREAD_unlock, .-CRYPTO_THREAD_unlock
	.p2align 4
	.globl	CRYPTO_THREAD_lock_free
	.type	CRYPTO_THREAD_lock_free, @function
CRYPTO_THREAD_lock_free:
.LFB266:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L19
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	pthread_rwlock_destroy@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$111, %edx
	popq	%r12
	.cfi_restore 12
	leaq	.LC0(%rip), %rsi
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_free@PLT
	.p2align 4,,10
	.p2align 3
.L19:
	ret
	.cfi_endproc
.LFE266:
	.size	CRYPTO_THREAD_lock_free, .-CRYPTO_THREAD_lock_free
	.p2align 4
	.globl	CRYPTO_THREAD_run_once
	.type	CRYPTO_THREAD_run_once, @function
CRYPTO_THREAD_run_once:
.LFB267:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	pthread_once@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	testl	%eax, %eax
	sete	%al
	movzbl	%al, %eax
	ret
	.cfi_endproc
.LFE267:
	.size	CRYPTO_THREAD_run_once, .-CRYPTO_THREAD_run_once
	.p2align 4
	.globl	CRYPTO_THREAD_init_local
	.type	CRYPTO_THREAD_init_local, @function
CRYPTO_THREAD_init_local:
.LFB268:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	pthread_key_create@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	testl	%eax, %eax
	sete	%al
	movzbl	%al, %eax
	ret
	.cfi_endproc
.LFE268:
	.size	CRYPTO_THREAD_init_local, .-CRYPTO_THREAD_init_local
	.p2align 4
	.globl	CRYPTO_THREAD_get_local
	.type	CRYPTO_THREAD_get_local, @function
CRYPTO_THREAD_get_local:
.LFB269:
	.cfi_startproc
	endbr64
	movl	(%rdi), %edi
	jmp	pthread_getspecific@PLT
	.cfi_endproc
.LFE269:
	.size	CRYPTO_THREAD_get_local, .-CRYPTO_THREAD_get_local
	.p2align 4
	.globl	CRYPTO_THREAD_set_local
	.type	CRYPTO_THREAD_set_local, @function
CRYPTO_THREAD_set_local:
.LFB270:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	(%rdi), %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	pthread_setspecific@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	testl	%eax, %eax
	sete	%al
	movzbl	%al, %eax
	ret
	.cfi_endproc
.LFE270:
	.size	CRYPTO_THREAD_set_local, .-CRYPTO_THREAD_set_local
	.p2align 4
	.globl	CRYPTO_THREAD_cleanup_local
	.type	CRYPTO_THREAD_cleanup_local, @function
CRYPTO_THREAD_cleanup_local:
.LFB271:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	(%rdi), %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	pthread_key_delete@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	testl	%eax, %eax
	sete	%al
	movzbl	%al, %eax
	ret
	.cfi_endproc
.LFE271:
	.size	CRYPTO_THREAD_cleanup_local, .-CRYPTO_THREAD_cleanup_local
	.p2align 4
	.globl	CRYPTO_THREAD_get_current_id
	.type	CRYPTO_THREAD_get_current_id, @function
CRYPTO_THREAD_get_current_id:
.LFB272:
	.cfi_startproc
	endbr64
	jmp	pthread_self@PLT
	.cfi_endproc
.LFE272:
	.size	CRYPTO_THREAD_get_current_id, .-CRYPTO_THREAD_get_current_id
	.p2align 4
	.globl	CRYPTO_THREAD_compare_id
	.type	CRYPTO_THREAD_compare_id, @function
CRYPTO_THREAD_compare_id:
.LFB273:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpq	%rsi, %rdi
	sete	%al
	ret
	.cfi_endproc
.LFE273:
	.size	CRYPTO_THREAD_compare_id, .-CRYPTO_THREAD_compare_id
	.p2align 4
	.globl	CRYPTO_atomic_add
	.type	CRYPTO_atomic_add, @function
CRYPTO_atomic_add:
.LFB274:
	.cfi_startproc
	endbr64
	movl	%esi, %eax
	lock xaddl	%eax, (%rdi)
	addl	%esi, %eax
	movl	%eax, (%rdx)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE274:
	.size	CRYPTO_atomic_add, .-CRYPTO_atomic_add
	.p2align 4
	.globl	openssl_init_fork_handlers
	.type	openssl_init_fork_handlers, @function
openssl_init_fork_handlers:
.LFB276:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	fork_once_func(%rip), %rsi
	leaq	fork_once_control(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	pthread_once@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	testl	%eax, %eax
	sete	%al
	movzbl	%al, %eax
	ret
	.cfi_endproc
.LFE276:
	.size	openssl_init_fork_handlers, .-openssl_init_fork_handlers
	.p2align 4
	.globl	openssl_get_fork_id
	.type	openssl_get_fork_id, @function
openssl_get_fork_id:
.LFB277:
	.cfi_startproc
	endbr64
	jmp	getpid@PLT
	.cfi_endproc
.LFE277:
	.size	openssl_get_fork_id, .-openssl_get_fork_id
	.local	fork_once_control
	.comm	fork_once_control,4,4
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
