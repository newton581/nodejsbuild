	.file	"tasn_scn.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/asn1/tasn_scn.c"
	.text
	.p2align 4
	.globl	ASN1_SCTX_new
	.type	ASN1_SCTX_new, @function
ASN1_SCTX_new:
.LFB1368:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$27, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$80, %edi
	subq	$24, %rsp
	call	CRYPTO_zalloc@PLT
	testq	%rax, %rax
	je	.L6
	movq	%rbx, 64(%rax)
.L1:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	.cfi_restore_state
	movl	$30, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	movl	$221, %esi
	movl	$13, %edi
	movq	%rax, -24(%rbp)
	call	ERR_put_error@PLT
	movq	-24(%rbp), %rax
	jmp	.L1
	.cfi_endproc
.LFE1368:
	.size	ASN1_SCTX_new, .-ASN1_SCTX_new
	.p2align 4
	.globl	ASN1_SCTX_free
	.type	ASN1_SCTX_free, @function
ASN1_SCTX_free:
.LFB1369:
	.cfi_startproc
	endbr64
	movl	$39, %edx
	leaq	.LC0(%rip), %rsi
	jmp	CRYPTO_free@PLT
	.cfi_endproc
.LFE1369:
	.size	ASN1_SCTX_free, .-ASN1_SCTX_free
	.p2align 4
	.globl	ASN1_SCTX_get_item
	.type	ASN1_SCTX_get_item, @function
ASN1_SCTX_get_item:
.LFB1370:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE1370:
	.size	ASN1_SCTX_get_item, .-ASN1_SCTX_get_item
	.p2align 4
	.globl	ASN1_SCTX_get_template
	.type	ASN1_SCTX_get_template, @function
ASN1_SCTX_get_template:
.LFB1371:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	ret
	.cfi_endproc
.LFE1371:
	.size	ASN1_SCTX_get_template, .-ASN1_SCTX_get_template
	.p2align 4
	.globl	ASN1_SCTX_get_flags
	.type	ASN1_SCTX_get_flags, @function
ASN1_SCTX_get_flags:
.LFB1372:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	ret
	.cfi_endproc
.LFE1372:
	.size	ASN1_SCTX_get_flags, .-ASN1_SCTX_get_flags
	.p2align 4
	.globl	ASN1_SCTX_set_app_data
	.type	ASN1_SCTX_set_app_data, @function
ASN1_SCTX_set_app_data:
.LFB1373:
	.cfi_startproc
	endbr64
	movq	%rsi, 72(%rdi)
	ret
	.cfi_endproc
.LFE1373:
	.size	ASN1_SCTX_set_app_data, .-ASN1_SCTX_set_app_data
	.p2align 4
	.globl	ASN1_SCTX_get_app_data
	.type	ASN1_SCTX_get_app_data, @function
ASN1_SCTX_get_app_data:
.LFB1374:
	.cfi_startproc
	endbr64
	movq	72(%rdi), %rax
	ret
	.cfi_endproc
.LFE1374:
	.size	ASN1_SCTX_get_app_data, .-ASN1_SCTX_get_app_data
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
