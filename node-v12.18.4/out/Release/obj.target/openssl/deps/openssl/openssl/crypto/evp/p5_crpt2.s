	.file	"p5_crpt2.c"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	""
	.text
	.p2align 4
	.globl	PKCS5_PBKDF2_HMAC
	.type	PKCS5_PBKDF2_HMAC, @function
PKCS5_PBKDF2_HMAC:
.LFB803:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%r9, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%r9, %rdi
	pushq	%r12
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%ecx, %ebx
	subq	$184, %rsp
	movq	%rdx, -192(%rbp)
	movq	24(%rbp), %r15
	movl	%r8d, -156(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	EVP_MD_size@PLT
	movl	%eax, -180(%rbp)
	testl	%eax, %eax
	js	.L50
	call	HMAC_CTX_new@PLT
	movq	%rax, -152(%rbp)
	testq	%rax, %rax
	je	.L50
	testq	%r13, %r13
	je	.L23
	cmpl	$-1, %r12d
	jne	.L5
	movq	%r13, %rdi
	call	strlen@PLT
	movl	%eax, %r12d
.L5:
	movq	-152(%rbp), %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	%r12d, %edx
	movq	%r13, %rsi
	call	HMAC_Init_ex@PLT
	testl	%eax, %eax
	je	.L53
	call	HMAC_CTX_new@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L7
	movl	16(%rbp), %eax
	testl	%eax, %eax
	je	.L9
	movq	$1, -200(%rbp)
	movslq	%ebx, %rax
	movq	%rax, -208(%rbp)
	leaq	-132(%rbp), %rax
	movq	%rax, -216(%rbp)
	movslq	-180(%rbp), %rax
	movq	%rax, -168(%rbp)
.L8:
	movl	-180(%rbp), %eax
	movq	-152(%rbp), %rsi
	movq	%r14, %rdi
	cmpl	16(%rbp), %eax
	movl	%eax, %ebx
	movl	-200(%rbp), %eax
	cmovg	16(%rbp), %ebx
	bswap	%eax
	movl	%eax, -132(%rbp)
	call	HMAC_CTX_copy@PLT
	testl	%eax, %eax
	je	.L51
	movq	-208(%rbp), %rdx
	movq	-192(%rbp), %rsi
	movq	%r14, %rdi
	call	HMAC_Update@PLT
	testl	%eax, %eax
	je	.L15
	movq	-216(%rbp), %rsi
	movl	$4, %edx
	movq	%r14, %rdi
	call	HMAC_Update@PLT
	testl	%eax, %eax
	je	.L15
	leaq	-128(%rbp), %r13
	xorl	%edx, %edx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	HMAC_Final@PLT
	testl	%eax, %eax
	je	.L15
	movslq	%ebx, %rax
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%rax, %rdx
	movq	%rax, -224(%rbp)
	call	memcpy@PLT
	cmpl	$1, -156(%rbp)
	jle	.L13
	leal	-1(%rbx), %eax
	movl	$1, %r12d
	movl	%eax, -160(%rbp)
	movl	%ebx, %eax
	shrl	$4, %eax
	movl	%eax, -176(%rbp)
	movl	%ebx, %eax
	andl	$-16, %eax
	movl	%eax, -172(%rbp)
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L54:
	movdqu	(%r15), %xmm0
	movl	-176(%rbp), %eax
	pxor	-128(%rbp), %xmm0
	movups	%xmm0, (%r15)
	cmpl	$1, %eax
	jbe	.L19
	movdqu	16(%r15), %xmm0
	pxor	-112(%rbp), %xmm0
	movups	%xmm0, 16(%r15)
	cmpl	$2, %eax
	je	.L19
	movdqu	32(%r15), %xmm0
	pxor	-96(%rbp), %xmm0
	movups	%xmm0, 32(%r15)
	cmpl	$3, %eax
	je	.L19
	movdqu	48(%r15), %xmm0
	pxor	-80(%rbp), %xmm0
	movups	%xmm0, 48(%r15)
.L19:
	movl	-172(%rbp), %eax
	cmpl	%eax, %ebx
	je	.L17
.L18:
	movslq	%eax, %rdx
	movzbl	-128(%rbp,%rdx), %esi
	xorb	%sil, (%r15,%rdx)
	leal	1(%rax), %edx
	cmpl	%edx, %ebx
	jle	.L17
	movslq	%edx, %rdx
	movzbl	-128(%rbp,%rdx), %esi
	xorb	%sil, (%r15,%rdx)
	leal	2(%rax), %edx
	cmpl	%edx, %ebx
	jle	.L17
	movslq	%edx, %rdx
	movzbl	-128(%rbp,%rdx), %esi
	xorb	%sil, (%r15,%rdx)
	leal	3(%rax), %edx
	cmpl	%edx, %ebx
	jle	.L17
	movslq	%edx, %rdx
	movzbl	-128(%rbp,%rdx), %esi
	xorb	%sil, (%r15,%rdx)
	leal	4(%rax), %edx
	cmpl	%edx, %ebx
	jle	.L17
	movslq	%edx, %rdx
	movzbl	-128(%rbp,%rdx), %esi
	xorb	%sil, (%r15,%rdx)
	leal	5(%rax), %edx
	cmpl	%edx, %ebx
	jle	.L17
	movslq	%edx, %rdx
	movzbl	-128(%rbp,%rdx), %esi
	xorb	%sil, (%r15,%rdx)
	leal	6(%rax), %edx
	cmpl	%edx, %ebx
	jle	.L17
	movslq	%edx, %rdx
	movzbl	-128(%rbp,%rdx), %esi
	xorb	%sil, (%r15,%rdx)
	leal	7(%rax), %edx
	cmpl	%edx, %ebx
	jle	.L17
	movslq	%edx, %rdx
	movzbl	-128(%rbp,%rdx), %esi
	xorb	%sil, (%r15,%rdx)
	leal	8(%rax), %edx
	cmpl	%edx, %ebx
	jle	.L17
	movslq	%edx, %rdx
	movzbl	-128(%rbp,%rdx), %esi
	xorb	%sil, (%r15,%rdx)
	leal	9(%rax), %edx
	cmpl	%edx, %ebx
	jle	.L17
	movslq	%edx, %rdx
	movzbl	-128(%rbp,%rdx), %esi
	xorb	%sil, (%r15,%rdx)
	leal	10(%rax), %edx
	cmpl	%edx, %ebx
	jle	.L17
	movslq	%edx, %rdx
	movzbl	-128(%rbp,%rdx), %esi
	xorb	%sil, (%r15,%rdx)
	leal	11(%rax), %edx
	cmpl	%edx, %ebx
	jle	.L17
	movslq	%edx, %rdx
	movzbl	-128(%rbp,%rdx), %esi
	xorb	%sil, (%r15,%rdx)
	leal	12(%rax), %edx
	cmpl	%edx, %ebx
	jle	.L17
	movslq	%edx, %rdx
	movzbl	-128(%rbp,%rdx), %esi
	xorb	%sil, (%r15,%rdx)
	leal	13(%rax), %edx
	cmpl	%edx, %ebx
	jle	.L17
	movslq	%edx, %rdx
	addl	$14, %eax
	movzbl	-128(%rbp,%rdx), %esi
	xorb	%sil, (%r15,%rdx)
	cmpl	%eax, %ebx
	jle	.L17
	cltq
	movzbl	-128(%rbp,%rax), %edx
	xorb	%dl, (%r15,%rax)
	.p2align 4,,10
	.p2align 3
.L17:
	addl	$1, %r12d
	cmpl	%r12d, -156(%rbp)
	je	.L13
.L21:
	movq	-152(%rbp), %rsi
	movq	%r14, %rdi
	call	HMAC_CTX_copy@PLT
	testl	%eax, %eax
	je	.L51
	movq	-168(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	HMAC_Update@PLT
	testl	%eax, %eax
	je	.L15
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	HMAC_Final@PLT
	testl	%eax, %eax
	je	.L15
	testl	%ebx, %ebx
	jle	.L17
	cmpl	$14, -160(%rbp)
	ja	.L54
	xorl	%eax, %eax
	jmp	.L18
.L7:
	movq	-152(%rbp), %rdi
	call	HMAC_CTX_free@PLT
	.p2align 4,,10
	.p2align 3
.L50:
	xorl	%eax, %eax
.L1:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L55
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L15:
	.cfi_restore_state
	movq	%r14, %rdi
	call	HMAC_CTX_free@PLT
	movq	-152(%rbp), %rdi
	call	HMAC_CTX_free@PLT
	xorl	%eax, %eax
	jmp	.L1
.L53:
	movq	-152(%rbp), %rdi
	movl	%eax, -156(%rbp)
	call	HMAC_CTX_free@PLT
	movl	-156(%rbp), %eax
	jmp	.L1
.L23:
	xorl	%r12d, %r12d
	leaq	.LC0(%rip), %r13
	jmp	.L5
.L51:
	movq	%r14, %rdi
	movl	%eax, -156(%rbp)
	call	HMAC_CTX_free@PLT
	movq	-152(%rbp), %rdi
	call	HMAC_CTX_free@PLT
	movl	-156(%rbp), %eax
	jmp	.L1
.L13:
	addq	$1, -200(%rbp)
	addq	-224(%rbp), %r15
	subl	%ebx, 16(%rbp)
	jne	.L8
.L9:
	movq	%r14, %rdi
	call	HMAC_CTX_free@PLT
	movq	-152(%rbp), %rdi
	call	HMAC_CTX_free@PLT
	movl	$1, %eax
	jmp	.L1
.L55:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE803:
	.size	PKCS5_PBKDF2_HMAC, .-PKCS5_PBKDF2_HMAC
	.p2align 4
	.globl	PKCS5_PBKDF2_HMAC_SHA1
	.type	PKCS5_PBKDF2_HMAC_SHA1, @function
PKCS5_PBKDF2_HMAC_SHA1:
.LFB804:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%ecx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%r8d, %ebx
	subq	$24, %rsp
	movl	%r9d, -52(%rbp)
	call	EVP_sha1@PLT
	movl	-52(%rbp), %edx
	pushq	16(%rbp)
	movl	%ebx, %r8d
	movl	%r15d, %ecx
	movl	%r13d, %esi
	movq	%r12, %rdi
	movq	%rax, %r9
	pushq	%rdx
	movq	%r14, %rdx
	call	PKCS5_PBKDF2_HMAC
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE804:
	.size	PKCS5_PBKDF2_HMAC_SHA1, .-PKCS5_PBKDF2_HMAC_SHA1
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"../deps/openssl/openssl/crypto/evp/p5_crpt2.c"
	.text
	.p2align 4
	.globl	PKCS5_v2_PBE_keyivgen
	.type	PKCS5_v2_PBE_keyivgen, @function
PKCS5_v2_PBE_keyivgen:
.LFB805:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	movq	%rcx, %rsi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	leaq	PBE2PARAM_it(%rip), %rdi
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	ASN1_TYPE_unpack_sequence@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L69
	movq	(%rax), %rax
	movq	(%rax), %rdi
	call	OBJ_obj2nid@PLT
	leaq	-64(%rbp), %r8
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	%eax, %esi
	movl	$2, %edi
	call	EVP_PBE_find@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	je	.L70
	movq	8(%rbx), %rax
	movq	(%rax), %rdi
	call	OBJ_obj2nid@PLT
	movl	%eax, %edi
	call	OBJ_nid2sn@PLT
	movq	%rax, %rdi
	call	EVP_get_cipherbyname@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L71
	movl	16(%rbp), %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r13, %rdi
	call	EVP_CipherInit_ex@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	je	.L60
	movq	8(%rbx), %rax
	movq	%r13, %rdi
	movq	8(%rax), %rsi
	call	EVP_CIPHER_asn1_to_param@PLT
	testl	%eax, %eax
	js	.L72
	movq	(%rbx), %rax
	subq	$8, %rsp
	movl	%r15d, %edx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	8(%rax), %rcx
	movl	16(%rbp), %eax
	pushq	%rax
	call	*-64(%rbp)
	movl	%eax, %r12d
	popq	%rax
	popq	%rdx
	.p2align 4,,10
	.p2align 3
.L60:
	movq	%rbx, %rdi
	call	PBE2PARAM_free@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L73
	leaq	-40(%rbp), %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L70:
	.cfi_restore_state
	movl	$157, %r8d
	movl	$124, %edx
	movl	$118, %esi
	movl	$6, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L71:
	movl	$169, %r8d
	leaq	.LC1(%rip), %rcx
	movl	$107, %edx
	xorl	%r12d, %r12d
	movl	$118, %esi
	movl	$6, %edi
	call	ERR_put_error@PLT
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L69:
	movl	$150, %r8d
	leaq	.LC1(%rip), %rcx
	movl	$114, %edx
	xorl	%r12d, %r12d
	movl	$118, %esi
	movl	$6, %edi
	call	ERR_put_error@PLT
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L72:
	movl	$177, %r8d
	leaq	.LC1(%rip), %rcx
	movl	$122, %edx
	xorl	%r12d, %r12d
	movl	$118, %esi
	movl	$6, %edi
	call	ERR_put_error@PLT
	jmp	.L60
.L73:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE805:
	.size	PKCS5_v2_PBE_keyivgen, .-PKCS5_v2_PBE_keyivgen
	.section	.rodata.str1.8
	.align 8
.LC2:
	.string	"assertion failed: keylen <= sizeof(key)"
	.text
	.p2align 4
	.globl	PKCS5_v2_PBKDF2_keyivgen
	.type	PKCS5_v2_PBKDF2_keyivgen, @function
PKCS5_v2_PBKDF2_keyivgen:
.LFB806:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -152(%rbp)
	movl	%edx, -156(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	EVP_CIPHER_CTX_cipher@PLT
	testq	%rax, %rax
	je	.L93
	movq	%r13, %rdi
	call	EVP_CIPHER_CTX_key_length@PLT
	movl	%eax, %r14d
	cmpl	$64, %r14d
	ja	.L94
	movq	%r15, %rsi
	leaq	PBKDF2PARAM_it(%rip), %rdi
	call	ASN1_TYPE_unpack_sequence@PLT
	movl	$210, %r8d
	movl	$114, %edx
	leaq	.LC1(%rip), %rcx
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L92
	movq	%r13, %rdi
	call	EVP_CIPHER_CTX_key_length@PLT
	movq	16(%r12), %rdi
	movl	%eax, %ebx
	testq	%rdi, %rdi
	je	.L79
	call	ASN1_INTEGER_get@PLT
	movl	%ebx, %r14d
	movq	%rax, %r8
	movslq	%ebx, %rax
	cmpq	%rax, %r8
	jne	.L95
.L80:
	movq	24(%r12), %rax
	movl	$163, %esi
	testq	%rax, %rax
	je	.L81
	movq	(%rax), %rdi
	call	OBJ_obj2nid@PLT
	movl	%eax, %esi
.L81:
	leaq	-132(%rbp), %rcx
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movl	$1, %edi
	call	EVP_PBE_find@PLT
	movl	%eax, %r15d
	testl	%eax, %eax
	je	.L96
	movl	-132(%rbp), %edi
	call	OBJ_nid2sn@PLT
	movq	%rax, %rdi
	call	EVP_get_digestbyname@PLT
	movl	$235, %r8d
	movl	$125, %edx
	leaq	.LC1(%rip), %rcx
	movq	%rax, %r9
	testq	%rax, %rax
	je	.L92
	movq	(%r12), %rax
	cmpl	$4, (%rax)
	jne	.L97
	movq	8(%rax), %rax
	movq	8(%r12), %rdi
	movq	%r9, -176(%rbp)
	movl	(%rax), %ecx
	movq	8(%rax), %r15
	movl	%ecx, -160(%rbp)
	call	ASN1_INTEGER_get@PLT
	leaq	-128(%rbp), %r10
	movq	%r15, %rdx
	movq	-176(%rbp), %r9
	pushq	%r10
	movl	-160(%rbp), %ecx
	movl	%eax, %r8d
	movl	-156(%rbp), %esi
	movq	-152(%rbp), %rdi
	pushq	%rbx
	movq	%r10, -168(%rbp)
	call	PKCS5_PBKDF2_HMAC
	movq	-168(%rbp), %r10
	movl	%eax, %r15d
	popq	%rax
	popq	%rdx
	testl	%r15d, %r15d
	je	.L76
	movl	16(%rbp), %r9d
	movq	%r10, %rcx
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r13, %rdi
	movq	%r10, -152(%rbp)
	call	EVP_CipherInit_ex@PLT
	movq	-152(%rbp), %r10
	movl	%eax, %r15d
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L79:
	movl	%eax, %r14d
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L95:
	movl	$219, %r8d
	leaq	.LC1(%rip), %rcx
	movl	$123, %edx
.L92:
	movl	$164, %esi
	movl	$6, %edi
	xorl	%r15d, %r15d
	call	ERR_put_error@PLT
	leaq	-128(%rbp), %r10
.L76:
	movq	%r10, %rdi
	movq	%r14, %rsi
	call	OPENSSL_cleanse@PLT
	movq	%r12, %rdi
	call	PBKDF2PARAM_free@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L98
	leaq	-40(%rbp), %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L97:
	.cfi_restore_state
	movl	$240, %r8d
	leaq	.LC1(%rip), %rcx
	movl	$126, %edx
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L96:
	movl	$229, %r8d
	movl	$125, %edx
	movl	$164, %esi
	movl	$6, %edi
	leaq	.LC1(%rip), %rcx
	call	ERR_put_error@PLT
	leaq	-128(%rbp), %r10
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L93:
	movl	$131, %edx
	movl	$164, %esi
	movq	%rax, %r12
	xorl	%r14d, %r14d
	movl	$199, %r8d
	leaq	.LC1(%rip), %rcx
	movl	$6, %edi
	xorl	%r15d, %r15d
	call	ERR_put_error@PLT
	leaq	-128(%rbp), %r10
	jmp	.L76
.L98:
	call	__stack_chk_fail@PLT
.L94:
	movl	$203, %edx
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	OPENSSL_die@PLT
	.cfi_endproc
.LFE806:
	.size	PKCS5_v2_PBKDF2_keyivgen, .-PKCS5_v2_PBKDF2_keyivgen
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
