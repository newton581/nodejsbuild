	.file	"pk7_doit.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/pkcs7/pk7_doit.c"
	.text
	.p2align 4
	.type	pkcs7_decrypt_rinfo, @function
pkcs7_decrypt_rinfo:
.LFB1300:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	$-1, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movq	%rcx, %rdi
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$40, %rsp
	movq	%rsi, -72(%rbp)
	xorl	%esi, %esi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	EVP_PKEY_CTX_new@PLT
	testq	%rax, %rax
	je	.L1
	movq	%rax, %rdi
	movq	%rax, %r13
	call	EVP_PKEY_decrypt_init@PLT
	testl	%eax, %eax
	jle	.L5
	xorl	%r8d, %r8d
	movl	$4, %ecx
	movl	$512, %edx
	movq	%rbx, %r9
	movl	$-1, %esi
	movq	%r13, %rdi
	call	EVP_PKEY_CTX_ctrl@PLT
	movl	$158, %r8d
	movl	$152, %edx
	leaq	.LC0(%rip), %rcx
	testl	%eax, %eax
	jle	.L22
	movq	24(%rbx), %rax
	leaq	-64(%rbp), %r11
	xorl	%esi, %esi
	movq	%r13, %rdi
	movq	%r11, %rdx
	movq	%r11, -80(%rbp)
	movq	8(%rax), %rcx
	movslq	(%rax), %r8
	call	EVP_PKEY_decrypt@PLT
	testl	%eax, %eax
	jle	.L5
	movq	-64(%rbp), %rdi
	movl	$166, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_malloc@PLT
	movq	-80(%rbp), %r11
	testq	%rax, %rax
	movq	%rax, %r9
	je	.L24
	movq	24(%rbx), %rax
	movq	%r9, %rsi
	movq	%r11, %rdx
	movq	%r13, %rdi
	movq	%r9, -80(%rbp)
	movq	8(%rax), %rcx
	movslq	(%rax), %r8
	call	EVP_PKEY_decrypt@PLT
	movq	-80(%rbp), %r9
	testl	%eax, %eax
	jle	.L7
	movq	-64(%rbp), %rax
	testq	%rax, %rax
	je	.L7
	cmpq	%r12, %rax
	je	.L8
	testq	%r12, %r12
	je	.L8
.L7:
	movl	$178, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$6, %edx
	movl	$133, %esi
	movl	$33, %edi
	movq	%r9, -72(%rbp)
	xorl	%r15d, %r15d
	call	ERR_put_error@PLT
	movq	%r13, %rdi
	call	EVP_PKEY_CTX_free@PLT
	movq	-72(%rbp), %r9
	movl	$191, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r9, %rdi
	call	CRYPTO_free@PLT
	.p2align 4,,10
	.p2align 3
.L1:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L25
	addq	$40, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	.cfi_restore_state
	movq	%r13, %rdi
	call	EVP_PKEY_CTX_free@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L8:
	movq	-72(%rbp), %rbx
	movq	(%r14), %rdi
	movl	$184, %ecx
	movq	%r9, -80(%rbp)
	leaq	.LC0(%rip), %rdx
	movl	$1, %r15d
	movslq	(%rbx), %rsi
	call	CRYPTO_clear_free@PLT
	movq	-80(%rbp), %r9
	movq	-64(%rbp), %rax
	movq	%r13, %rdi
	movq	%r9, (%r14)
	movl	%eax, (%rbx)
	call	EVP_PKEY_CTX_free@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L24:
	movl	$169, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
.L22:
	movl	$133, %esi
	movl	$33, %edi
	call	ERR_put_error@PLT
	movq	%r13, %rdi
	call	EVP_PKEY_CTX_free@PLT
	jmp	.L1
.L25:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1300:
	.size	pkcs7_decrypt_rinfo, .-pkcs7_decrypt_rinfo
	.p2align 4
	.type	add_attribute, @function
add_attribute:
.LFB1319:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rdi), %rdi
	movl	%edx, -52(%rbp)
	testq	%rdi, %rdi
	jne	.L27
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L33:
	movq	(%rbx), %rdi
	movl	%r12d, %esi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %rdi
	movq	%rax, %r14
	call	X509_ATTRIBUTE_get0_object@PLT
	movq	%rax, %rdi
	call	OBJ_obj2nid@PLT
	cmpl	%r13d, %eax
	je	.L49
	movq	(%rbx), %rdi
	addl	$1, %r12d
.L27:
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r12d
	jl	.L33
.L34:
	movl	-52(%rbp), %esi
	movq	%r15, %rdx
	movl	%r13d, %edi
	call	X509_ATTRIBUTE_create@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L28
	movq	(%rbx), %rdi
	movq	%rax, %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L29
.L32:
	movl	$1, %eax
.L26:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L48:
	.cfi_restore_state
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, (%rbx)
	testq	%rax, %rax
	jne	.L34
.L28:
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L49:
	.cfi_restore_state
	movq	%r14, %rdi
	call	X509_ATTRIBUTE_free@PLT
	movl	-52(%rbp), %esi
	movl	%r13d, %edi
	movq	%r15, %rdx
	call	X509_ATTRIBUTE_create@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L28
	movq	(%rbx), %rdi
	movq	%rax, %rdx
	movl	%r12d, %esi
	call	OPENSSL_sk_set@PLT
	testq	%rax, %rax
	jne	.L32
	movq	%r13, %rdi
	call	X509_ATTRIBUTE_free@PLT
	xorl	%eax, %eax
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L29:
	movq	%r12, %rdi
	movl	%eax, -52(%rbp)
	call	X509_ATTRIBUTE_free@PLT
	movl	-52(%rbp), %eax
	jmp	.L26
	.cfi_endproc
.LFE1319:
	.size	add_attribute, .-add_attribute
	.p2align 4
	.type	PKCS7_bio_add_digest.isra.0, @function
PKCS7_bio_add_digest.isra.0:
.LFB1322:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	BIO_f_md@PLT
	movq	%rax, %rdi
	call	BIO_new@PLT
	movl	$60, %r8d
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L58
	movq	0(%r13), %rdi
	call	OBJ_obj2nid@PLT
	movl	%eax, %edi
	call	OBJ_nid2sn@PLT
	movq	%rax, %rdi
	call	EVP_get_digestbyname@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L59
	movq	%r12, %rdi
	xorl	%edx, %edx
	movl	$111, %esi
	call	BIO_ctrl@PLT
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L60
	movq	%r12, %rsi
	call	BIO_push@PLT
	movq	%rax, %r8
	movl	$1, %eax
	testq	%r8, %r8
	je	.L61
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L60:
	.cfi_restore_state
	movq	%r12, (%rbx)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L61:
	.cfi_restore_state
	movl	$74, %r8d
	.p2align 4,,10
	.p2align 3
.L58:
	movl	$32, %edx
	movl	$125, %esi
	movl	$33, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L52:
	movq	%r12, %rdi
	call	BIO_free@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L59:
	.cfi_restore_state
	movl	$66, %r8d
	movl	$109, %edx
	movl	$125, %esi
	movl	$33, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L52
	.cfi_endproc
.LFE1322:
	.size	PKCS7_bio_add_digest.isra.0, .-PKCS7_bio_add_digest.isra.0
	.p2align 4
	.type	PKCS7_find_digest, @function
PKCS7_find_digest:
.LFB1304:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%edx, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$8, %rsp
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L63:
	movq	%rax, %rdi
	movq	%rbx, %rcx
	xorl	%edx, %edx
	movl	$120, %esi
	call	BIO_ctrl@PLT
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L68
	call	EVP_MD_CTX_md@PLT
	movq	%rax, %rdi
	call	EVP_MD_type@PLT
	cmpl	%r13d, %eax
	je	.L62
	movq	%r12, %rdi
	call	BIO_next@PLT
	movq	%rax, %rdi
.L66:
	movl	$520, %esi
	call	BIO_find_type@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L63
	movl	$599, %r8d
	movl	$108, %edx
	movl	$127, %esi
	movl	$33, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L62:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L68:
	.cfi_restore_state
	movl	$605, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$68, %edx
	xorl	%r12d, %r12d
	movl	$127, %esi
	movl	$33, %edi
	call	ERR_put_error@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1304:
	.size	PKCS7_find_digest, .-PKCS7_find_digest
	.p2align 4
	.globl	PKCS7_dataInit
	.type	PKCS7_dataInit, @function
PKCS7_dataInit:
.LFB1301:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$200, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -184(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -168(%rbp)
	testq	%rdi, %rdi
	je	.L158
	movq	32(%rdi), %rax
	movq	%rdi, %r15
	testq	%rax, %rax
	je	.L159
	movq	24(%rdi), %rdi
	call	OBJ_obj2nid@PLT
	movl	$0, 16(%r15)
	subl	$21, %eax
	cmpl	$4, %eax
	ja	.L73
	leaq	.L75(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L75:
	.long	.L116-.L75
	.long	.L78-.L75
	.long	.L77-.L75
	.long	.L76-.L75
	.long	.L74-.L75
	.text
	.p2align 4,,10
	.p2align 3
.L116:
	movq	$0, -216(%rbp)
	xorl	%ebx, %ebx
	xorl	%r13d, %r13d
	movq	$0, -200(%rbp)
	movq	$0, -208(%rbp)
	movq	$0, -192(%rbp)
.L79:
	xorl	%r14d, %r14d
	leaq	-168(%rbp), %r12
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L85:
	movl	%r14d, %esi
	movq	%rbx, %rdi
	call	OPENSSL_sk_value@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	PKCS7_bio_add_digest.isra.0
	testl	%eax, %eax
	je	.L84
	addl	$1, %r14d
.L83:
	movq	%rbx, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r14d
	jl	.L85
	cmpq	$0, -192(%rbp)
	je	.L89
	movq	-192(%rbp), %rsi
	leaq	-168(%rbp), %rdi
	call	PKCS7_bio_add_digest.isra.0
	testl	%eax, %eax
	je	.L84
.L89:
	testq	%r13, %r13
	je	.L88
	call	BIO_f_cipher@PLT
	movq	%rax, %rdi
	call	BIO_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L160
	leaq	-160(%rbp), %rcx
	xorl	%edx, %edx
	movl	$129, %esi
	movq	%rax, %rdi
	call	BIO_ctrl@PLT
	movq	%r13, %rdi
	call	EVP_CIPHER_key_length@PLT
	movq	%r13, %rdi
	movl	%eax, -220(%rbp)
	call	EVP_CIPHER_iv_length@PLT
	movq	%r13, %rdi
	movl	%eax, %ebx
	call	EVP_CIPHER_type@PLT
	movl	%eax, %edi
	call	OBJ_nid2obj@PLT
	movq	-200(%rbp), %rcx
	movq	%rax, (%rcx)
	testl	%ebx, %ebx
	jle	.L95
	leaq	-144(%rbp), %rdi
	movl	%ebx, %esi
	call	RAND_bytes@PLT
	testl	%eax, %eax
	jle	.L81
.L95:
	movq	-160(%rbp), %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r9d
	movq	%r13, %rsi
	call	EVP_CipherInit_ex@PLT
	testl	%eax, %eax
	jle	.L81
	leaq	-128(%rbp), %rax
	movq	-160(%rbp), %rdi
	movq	%rax, %rsi
	movq	%rax, -192(%rbp)
	movq	%rax, %r14
	call	EVP_CIPHER_CTX_rand_key@PLT
	testl	%eax, %eax
	jle	.L81
	movq	-160(%rbp), %rdi
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r14, %rcx
	movl	$1, %r9d
	leaq	-144(%rbp), %r8
	call	EVP_CipherInit_ex@PLT
	testl	%eax, %eax
	jle	.L81
	testl	%ebx, %ebx
	jle	.L99
	movq	-200(%rbp), %rax
	movq	8(%rax), %rsi
	testq	%rsi, %rsi
	je	.L161
.L98:
	movq	-160(%rbp), %rdi
	call	EVP_CIPHER_param_to_asn1@PLT
	testl	%eax, %eax
	js	.L81
.L99:
	movslq	-220(%rbp), %rax
	movq	%r15, -232(%rbp)
	xorl	%r13d, %r13d
	movq	-208(%rbp), %r15
	movq	%rax, -200(%rbp)
	.p2align 4,,10
	.p2align 3
.L97:
	movq	%r15, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r13d
	jge	.L162
	movl	%r13d, %esi
	movq	%r15, %rdi
	call	OPENSSL_sk_value@PLT
	movq	32(%rax), %rdi
	movq	%rax, %rbx
	call	X509_get0_pubkey@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L81
	xorl	%esi, %esi
	call	EVP_PKEY_CTX_new@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L81
	movq	%rax, %rdi
	call	EVP_PKEY_encrypt_init@PLT
	testl	%eax, %eax
	jle	.L102
	xorl	%r8d, %r8d
	movq	%rbx, %r9
	movl	$3, %ecx
	movl	$256, %edx
	movl	$-1, %esi
	movq	%r14, %rdi
	call	EVP_PKEY_CTX_ctrl@PLT
	testl	%eax, %eax
	jle	.L163
	leaq	-152(%rbp), %r9
	movq	-200(%rbp), %r8
	xorl	%esi, %esi
	movq	%r14, %rdi
	movq	-192(%rbp), %rcx
	movq	%r9, %rdx
	movq	%r9, -208(%rbp)
	call	EVP_PKEY_encrypt@PLT
	testl	%eax, %eax
	jle	.L102
	movq	-152(%rbp), %rdi
	leaq	.LC0(%rip), %rsi
	movl	$117, %edx
	call	CRYPTO_malloc@PLT
	movq	-208(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %rsi
	je	.L164
	movq	-200(%rbp), %r8
	movq	-192(%rbp), %rcx
	movq	%r9, %rdx
	movq	%r14, %rdi
	movq	%rax, -208(%rbp)
	call	EVP_PKEY_encrypt@PLT
	movq	-208(%rbp), %rsi
	testl	%eax, %eax
	jle	.L106
	movl	-152(%rbp), %edx
	movq	24(%rbx), %rdi
	addl	$1, %r13d
	call	ASN1_STRING_set0@PLT
	movq	%r14, %rdi
	call	EVP_PKEY_CTX_free@PLT
	movl	$134, %edx
	leaq	.LC0(%rip), %rsi
	xorl	%edi, %edi
	call	CRYPTO_free@PLT
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L78:
	movq	32(%r15), %rax
	movq	40(%rax), %r12
	movq	8(%rax), %rbx
	movq	24(%r12), %rdi
	call	OBJ_obj2nid@PLT
	cmpl	$21, %eax
	je	.L165
	movq	24(%r12), %rdi
	call	OBJ_obj2nid@PLT
	subl	$21, %eax
	cmpl	$5, %eax
	jbe	.L119
	movq	32(%r12), %rax
	movq	%rax, -192(%rbp)
	testq	%rax, %rax
	je	.L123
	cmpl	$4, (%rax)
	jne	.L119
	movq	8(%rax), %rax
	jmp	.L155
	.p2align 4,,10
	.p2align 3
.L76:
	movq	32(%r15), %rax
	movl	$241, %r8d
	movq	$0, -216(%rbp)
	movq	$0, -192(%rbp)
	movq	48(%rax), %rcx
	movq	8(%rax), %rbx
	movq	40(%rax), %rax
	movq	%rcx, -208(%rbp)
	movq	24(%rax), %r13
	movq	8(%rax), %rcx
	movq	%rcx, -200(%rbp)
	testq	%r13, %r13
	jne	.L79
.L157:
	leaq	.LC0(%rip), %rcx
	movl	$116, %edx
	movl	$105, %esi
	xorl	%r12d, %r12d
	movl	$33, %edi
	call	ERR_put_error@PLT
	.p2align 4,,10
	.p2align 3
.L81:
	movq	-168(%rbp), %rdi
	call	BIO_free_all@PLT
	movq	%r12, %rdi
	call	BIO_free_all@PLT
	xorl	%eax, %eax
.L69:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L166
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L74:
	.cfi_restore_state
	movq	32(%r15), %rax
	movq	16(%rax), %rbx
	movq	8(%rax), %rcx
	movq	24(%rbx), %rdi
	movq	%rcx, -192(%rbp)
	call	OBJ_obj2nid@PLT
	cmpl	$21, %eax
	je	.L167
	movq	24(%rbx), %rdi
	call	OBJ_obj2nid@PLT
	subl	$21, %eax
	cmpl	$5, %eax
	jbe	.L124
	movq	32(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L123
	cmpl	$4, (%rbx)
	jne	.L124
	movq	8(%rbx), %rax
	jmp	.L156
	.p2align 4,,10
	.p2align 3
.L77:
	movq	32(%r15), %rax
	xorl	%ebx, %ebx
	movq	$0, -216(%rbp)
	movq	$0, -192(%rbp)
	movq	8(%rax), %rcx
	movq	16(%rax), %rax
	movq	%rcx, -208(%rbp)
	movq	24(%rax), %r13
	movq	8(%rax), %rcx
	movq	%rcx, -200(%rbp)
	testq	%r13, %r13
	jne	.L79
	movl	$250, %r8d
	jmp	.L157
	.p2align 4,,10
	.p2align 3
.L119:
	movq	$0, -216(%rbp)
	xorl	%r13d, %r13d
	movq	$0, -200(%rbp)
	movq	$0, -208(%rbp)
	movq	$0, -192(%rbp)
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L124:
	movq	$0, -216(%rbp)
	xorl	%ebx, %ebx
	xorl	%r13d, %r13d
	movq	$0, -200(%rbp)
	movq	$0, -208(%rbp)
	jmp	.L79
.L171:
	movq	8(%rax), %rdi
	call	BIO_new_mem_buf@PLT
	movq	%rax, -184(%rbp)
.L112:
	cmpq	$0, -184(%rbp)
	jne	.L110
	.p2align 4,,10
	.p2align 3
.L84:
	xorl	%r12d, %r12d
	jmp	.L81
.L164:
	movl	$120, %r8d
	movl	$65, %edx
	movl	$132, %esi
	movl	$33, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	.p2align 4,,10
	.p2align 3
.L102:
	movq	%r14, %rdi
	call	EVP_PKEY_CTX_free@PLT
	movl	$134, %edx
	leaq	.LC0(%rip), %rsi
	xorl	%edi, %edi
	call	CRYPTO_free@PLT
	jmp	.L81
.L162:
	movq	-192(%rbp), %rdi
	movslq	-220(%rbp), %rsi
	movq	-232(%rbp), %r15
	call	OPENSSL_cleanse@PLT
	movq	-168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L168
	movq	%r12, %rsi
	call	BIO_push@PLT
	.p2align 4,,10
	.p2align 3
.L88:
	cmpq	$0, -184(%rbp)
	je	.L169
.L110:
	movq	-168(%rbp), %rdi
	movq	-184(%rbp), %rax
	testq	%rdi, %rdi
	je	.L69
	movq	%rax, %rsi
	call	BIO_push@PLT
	movq	-168(%rbp), %rax
	jmp	.L69
.L73:
	movl	$261, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$112, %edx
	xorl	%r12d, %r12d
	movl	$105, %esi
	movl	$33, %edi
	call	ERR_put_error@PLT
	jmp	.L81
	.p2align 4,,10
	.p2align 3
.L159:
	movl	$223, %r8d
	movl	$122, %edx
	movl	$105, %esi
	movl	$33, %edi
	leaq	.LC0(%rip), %rcx
	movq	%rax, -184(%rbp)
	call	ERR_put_error@PLT
	movq	-184(%rbp), %rax
	jmp	.L69
.L167:
	movq	32(%rbx), %rax
.L156:
	movq	%rax, -216(%rbp)
	xorl	%ebx, %ebx
	xorl	%r13d, %r13d
	movq	$0, -200(%rbp)
	movq	$0, -208(%rbp)
	jmp	.L79
.L165:
	movq	32(%r12), %rax
.L155:
	movq	%rax, -216(%rbp)
	xorl	%r13d, %r13d
	movq	$0, -200(%rbp)
	movq	$0, -208(%rbp)
	movq	$0, -192(%rbp)
	jmp	.L79
.L106:
	movq	%r14, %rdi
	movq	%rsi, -184(%rbp)
	call	EVP_PKEY_CTX_free@PLT
	movq	-184(%rbp), %r9
	movl	$134, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r9, %rdi
	call	CRYPTO_free@PLT
	jmp	.L81
.L163:
	movl	$110, %r8d
	movl	$152, %edx
	movl	$132, %esi
	movl	$33, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L102
.L158:
	movl	$209, %r8d
	movl	$143, %edx
	movl	$105, %esi
	movl	$33, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L69
.L169:
	movq	24(%r15), %rdi
	call	OBJ_obj2nid@PLT
	cmpl	$22, %eax
	je	.L170
.L111:
	movq	-216(%rbp), %rax
	testq	%rax, %rax
	je	.L113
	movl	(%rax), %esi
	testl	%esi, %esi
	jg	.L171
.L113:
	call	BIO_s_mem@PLT
	movq	%rax, %rdi
	call	BIO_new@PLT
	movq	%rax, -184(%rbp)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L84
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$130, %esi
	call	BIO_ctrl@PLT
	jmp	.L110
.L168:
	movq	%r12, -168(%rbp)
	jmp	.L88
.L160:
	movl	$279, %r8d
	movl	$32, %edx
	movl	$105, %esi
	movl	$33, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L81
.L123:
	movq	$0, -216(%rbp)
	xorl	%r13d, %r13d
	movq	$0, -200(%rbp)
	movq	$0, -208(%rbp)
	jmp	.L79
.L161:
	call	ASN1_TYPE_new@PLT
	movq	%rax, %rsi
	movq	-200(%rbp), %rax
	movq	%rsi, 8(%rax)
	testq	%rsi, %rsi
	jne	.L98
	jmp	.L81
.L170:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$2, %esi
	movq	%r15, %rdi
	call	PKCS7_ctrl@PLT
	testq	%rax, %rax
	je	.L111
	call	BIO_s_null@PLT
	movq	%rax, %rdi
	call	BIO_new@PLT
	movq	%rax, -184(%rbp)
	jmp	.L112
.L166:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1301:
	.size	PKCS7_dataInit, .-PKCS7_dataInit
	.p2align 4
	.globl	PKCS7_dataDecode
	.type	PKCS7_dataDecode, @function
PKCS7_dataDecode:
.LFB1303:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -104(%rbp)
	movq	%rdx, -96(%rbp)
	movq	%rcx, -88(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -72(%rbp)
	movq	$0, -64(%rbp)
	movl	$0, -76(%rbp)
	testq	%rdi, %rdi
	je	.L257
	movq	32(%rdi), %r14
	movq	%rdi, %rbx
	testq	%r14, %r14
	je	.L258
	movq	24(%rdi), %rdi
	call	OBJ_obj2nid@PLT
	movl	$0, 16(%rbx)
	cmpl	$23, %eax
	je	.L176
	cmpl	$24, %eax
	je	.L177
	movl	$430, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$112, %edx
	cmpl	$22, %eax
	je	.L259
.L255:
	movl	$112, %esi
	movl	$33, %edi
	xorl	%r15d, %r15d
	xorl	%r13d, %r13d
	call	ERR_put_error@PLT
	movq	$0, -88(%rbp)
	xorl	%r12d, %r12d
	xorl	%r14d, %r14d
.L188:
	movslq	-76(%rbp), %rsi
	movq	-64(%rbp), %rdi
	movl	$585, %ecx
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_clear_free@PLT
	movq	-88(%rbp), %rdi
	movl	$586, %ecx
	movq	%r15, %rsi
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_clear_free@PLT
	movq	%r14, %rdi
	xorl	%r14d, %r14d
	call	BIO_free_all@PLT
	movq	%r12, %rdi
	call	BIO_free_all@PLT
	movq	%r13, %rdi
	call	BIO_free_all@PLT
	xorl	%edi, %edi
	call	BIO_free_all@PLT
.L172:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L260
	addq	$104, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L176:
	.cfi_restore_state
	movq	32(%rbx), %rax
	movq	8(%rax), %rdx
	movq	16(%rax), %rax
	movq	%rdx, -120(%rbp)
	movq	8(%rax), %rdx
	movq	16(%rax), %rax
	movq	(%rdx), %rdi
	movq	%rdx, -128(%rbp)
	movq	%rax, -112(%rbp)
	call	OBJ_obj2nid@PLT
	movl	%eax, %edi
	call	OBJ_nid2sn@PLT
	movq	%rax, %rdi
	call	EVP_get_cipherbyname@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L261
	movq	-112(%rbp), %rax
	orq	-96(%rbp), %rax
	je	.L215
	xorl	%r14d, %r14d
.L216:
	call	BIO_f_cipher@PLT
	movq	%rax, %rdi
	call	BIO_new@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L262
	xorl	%ebx, %ebx
	cmpq	$0, -88(%rbp)
	je	.L263
	movq	%r12, -144(%rbp)
	movq	-88(%rbp), %r13
	movq	-120(%rbp), %r12
	movq	%rax, -136(%rbp)
	movq	%r14, -120(%rbp)
	jmp	.L201
	.p2align 4,,10
	.p2align 3
.L206:
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	OPENSSL_sk_value@PLT
	movq	%r13, %rdi
	movq	%rax, %r15
	call	X509_get_issuer_name@PLT
	movq	%rax, %rsi
	movq	8(%r15), %rax
	movq	(%rax), %rdi
	call	X509_NAME_cmp@PLT
	testl	%eax, %eax
	jne	.L205
	movq	8(%r15), %rax
	movq	%r13, %rdi
	movq	8(%rax), %r14
	call	X509_get_serialNumber@PLT
	movq	%rax, %rdi
	movq	%r14, %rsi
	call	ASN1_INTEGER_cmp@PLT
	testl	%eax, %eax
	je	.L264
.L205:
	addl	$1, %ebx
.L201:
	movq	%r12, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jl	.L206
	movl	$115, %edx
	movl	$112, %esi
	xorl	%r15d, %r15d
	xorl	%r12d, %r12d
	movl	$490, %r8d
	movl	$33, %edi
	movq	-120(%rbp), %r14
	leaq	.LC0(%rip), %rcx
	movq	-136(%rbp), %r13
	call	ERR_put_error@PLT
	movq	$0, -88(%rbp)
	jmp	.L188
	.p2align 4,,10
	.p2align 3
.L259:
	movq	32(%rbx), %rax
	movq	40(%rax), %r12
	movq	24(%r12), %rdi
	call	OBJ_obj2nid@PLT
	cmpl	$21, %eax
	je	.L265
	movq	24(%r12), %rdi
	call	OBJ_obj2nid@PLT
	subl	$21, %eax
	cmpl	$5, %eax
	jbe	.L181
	movq	32(%r12), %rax
	testq	%rax, %rax
	je	.L181
	cmpl	$4, (%rax)
	je	.L266
.L181:
	movq	24(%rbx), %rdi
	call	OBJ_obj2nid@PLT
	cmpl	$22, %eax
	jne	.L183
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$2, %esi
	movq	%rbx, %rdi
	call	PKCS7_ctrl@PLT
	testq	%rax, %rax
	je	.L183
	movq	$0, -112(%rbp)
	movq	-96(%rbp), %rax
	jmp	.L187
	.p2align 4,,10
	.p2align 3
.L177:
	movq	32(%rbx), %rax
	movq	48(%rax), %rdx
	movq	8(%rax), %r15
	movq	40(%rax), %rax
	movq	%rdx, -120(%rbp)
	movq	16(%rax), %rbx
	movq	8(%rax), %rax
	movq	(%rax), %rdi
	movq	%rbx, -112(%rbp)
	movq	%rax, -128(%rbp)
	call	OBJ_obj2nid@PLT
	movl	%eax, %edi
	call	OBJ_nid2sn@PLT
	movq	%rax, %rdi
	call	EVP_get_cipherbyname@PLT
	orq	-96(%rbp), %rbx
	movq	%rax, %rdx
	movq	%rax, -136(%rbp)
	movq	%rbx, %rax
	testq	%rdx, %rdx
	je	.L267
.L189:
	testq	%rax, %rax
	je	.L215
	xorl	%r14d, %r14d
	testq	%r15, %r15
	je	.L194
	xorl	%ebx, %ebx
	jmp	.L193
	.p2align 4,,10
	.p2align 3
.L270:
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	BIO_push@PLT
.L197:
	addl	$1, %ebx
.L193:
	movq	%r15, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jge	.L194
	movl	%ebx, %esi
	movq	%r15, %rdi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %r13
	call	BIO_f_md@PLT
	movq	%rax, %rdi
	call	BIO_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L268
	movq	0(%r13), %rdi
	call	OBJ_obj2nid@PLT
	movl	%eax, %edi
	call	OBJ_nid2sn@PLT
	movq	%rax, %rdi
	call	EVP_get_digestbyname@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L269
	xorl	%edx, %edx
	movl	$111, %esi
	movq	%r12, %rdi
	call	BIO_ctrl@PLT
	testq	%r14, %r14
	jne	.L270
	movq	%r12, %r14
	jmp	.L197
	.p2align 4,,10
	.p2align 3
.L265:
	movq	32(%r12), %rax
	movq	%rax, -112(%rbp)
.L180:
	movq	24(%rbx), %rdi
	call	OBJ_obj2nid@PLT
	cmpl	$22, %eax
	je	.L182
.L185:
	cmpq	$0, -112(%rbp)
	je	.L183
.L249:
	movq	-96(%rbp), %rax
	orq	-112(%rbp), %rax
.L187:
	movq	32(%rbx), %rdx
	movq	$0, -120(%rbp)
	movq	$0, -128(%rbp)
	movq	$0, -136(%rbp)
	movq	8(%rdx), %r15
	jmp	.L189
	.p2align 4,,10
	.p2align 3
.L215:
	movl	$436, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$122, %edx
	jmp	.L255
	.p2align 4,,10
	.p2align 3
.L194:
	cmpq	$0, -136(%rbp)
	jne	.L271
	xorl	%r15d, %r15d
.L199:
	cmpq	$0, -96(%rbp)
	je	.L272
.L212:
	movq	-96(%rbp), %rsi
	movq	%r14, %rdi
	call	BIO_push@PLT
	jmp	.L172
	.p2align 4,,10
	.p2align 3
.L268:
	movl	$32, %edx
	movl	$112, %esi
	xorl	%r13d, %r13d
	xorl	%r15d, %r15d
	movl	$445, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$33, %edi
	call	ERR_put_error@PLT
	movq	$0, -88(%rbp)
	jmp	.L188
	.p2align 4,,10
	.p2align 3
.L269:
	movl	$109, %edx
	movl	$112, %esi
	movq	%rax, %r13
	xorl	%r15d, %r15d
	movl	$452, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$33, %edi
	call	ERR_put_error@PLT
	movq	$0, -88(%rbp)
	jmp	.L188
	.p2align 4,,10
	.p2align 3
.L272:
	movq	-112(%rbp), %rax
	movl	(%rax), %esi
	testl	%esi, %esi
	jle	.L213
	movq	8(%rax), %rdi
	call	BIO_new_mem_buf@PLT
	movq	%rax, -96(%rbp)
	testq	%rax, %rax
	jne	.L212
.L230:
	movq	$0, -88(%rbp)
	xorl	%r13d, %r13d
	xorl	%r12d, %r12d
	jmp	.L188
	.p2align 4,,10
	.p2align 3
.L258:
	movl	$381, %r8d
	movl	$122, %edx
	movl	$112, %esi
	movl	$33, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L172
	.p2align 4,,10
	.p2align 3
.L264:
	movq	-104(%rbp), %rcx
	xorl	%r8d, %r8d
	leaq	-76(%rbp), %rsi
	leaq	-64(%rbp), %rdi
	movq	%r15, %rdx
	movq	-144(%rbp), %r12
	movq	-136(%rbp), %r13
	movq	-120(%rbp), %r14
	call	pkcs7_decrypt_rinfo
	testl	%eax, %eax
	js	.L231
	call	ERR_clear_error@PLT
.L209:
	xorl	%edx, %edx
	leaq	-72(%rbp), %rcx
	movl	$129, %esi
	movq	%r13, %rdi
	movq	$0, -72(%rbp)
	call	BIO_ctrl@PLT
	movq	-72(%rbp), %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	call	EVP_CipherInit_ex@PLT
	testl	%eax, %eax
	jle	.L231
	movq	-128(%rbp), %rax
	movq	-72(%rbp), %rdi
	movq	8(%rax), %rsi
	call	EVP_CIPHER_asn1_to_param@PLT
	testl	%eax, %eax
	js	.L231
	movq	-72(%rbp), %rdi
	call	EVP_CIPHER_CTX_key_length@PLT
	movl	$525, %edx
	leaq	.LC0(%rip), %rsi
	movslq	%eax, %r15
	movq	%r15, %rdi
	movq	%r15, %rbx
	call	CRYPTO_malloc@PLT
	movq	%rax, -88(%rbp)
	testq	%rax, %rax
	je	.L256
	movq	-72(%rbp), %rdi
	movq	%rax, %rsi
	call	EVP_CIPHER_CTX_rand_key@PLT
	testl	%eax, %eax
	jle	.L256
	cmpq	$0, -64(%rbp)
	je	.L273
.L210:
	movq	-72(%rbp), %rdi
	call	EVP_CIPHER_CTX_key_length@PLT
	movl	-76(%rbp), %esi
	cmpl	%esi, %eax
	je	.L211
	movq	-72(%rbp), %rdi
	call	EVP_CIPHER_CTX_set_key_length@PLT
	testl	%eax, %eax
	je	.L274
.L211:
	call	ERR_clear_error@PLT
	movq	-64(%rbp), %rcx
	movq	-72(%rbp), %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	EVP_CipherInit_ex@PLT
	testl	%eax, %eax
	jle	.L256
	movslq	-76(%rbp), %rsi
	movq	-64(%rbp), %rdi
	movl	$555, %ecx
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_clear_free@PLT
	movq	-88(%rbp), %rdi
	movl	$557, %ecx
	movq	%r15, %rsi
	leaq	.LC0(%rip), %rdx
	movq	$0, -64(%rbp)
	call	CRYPTO_clear_free@PLT
	testq	%r14, %r14
	je	.L228
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	BIO_push@PLT
	jmp	.L199
	.p2align 4,,10
	.p2align 3
.L231:
	movq	$0, -88(%rbp)
	xorl	%r15d, %r15d
.L256:
	xorl	%r12d, %r12d
	jmp	.L188
.L213:
	call	BIO_s_mem@PLT
	movq	%rax, %rdi
	call	BIO_new@PLT
	movq	%rax, -96(%rbp)
	testq	%rax, %rax
	je	.L230
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$130, %esi
	movq	%rax, %rdi
	call	BIO_ctrl@PLT
	jmp	.L212
.L266:
	movq	8(%rax), %rax
	movq	%rax, -112(%rbp)
	jmp	.L180
	.p2align 4,,10
	.p2align 3
.L263:
	movq	-120(%rbp), %r13
	movq	%rax, -136(%rbp)
	leaq	-76(%rbp), %r15
	movq	%r14, -120(%rbp)
	movq	-104(%rbp), %r14
	jmp	.L202
	.p2align 4,,10
	.p2align 3
.L208:
	movl	%ebx, %esi
	movq	%r13, %rdi
	call	OPENSSL_sk_value@PLT
	movq	%r12, %rdi
	movq	%rax, -104(%rbp)
	call	EVP_CIPHER_key_length@PLT
	movq	-104(%rbp), %rdx
	leaq	-64(%rbp), %rdi
	movq	%r14, %rcx
	movslq	%eax, %r8
	movq	%r15, %rsi
	call	pkcs7_decrypt_rinfo
	testl	%eax, %eax
	js	.L222
	call	ERR_clear_error@PLT
	addl	$1, %ebx
.L202:
	movq	%r13, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jl	.L208
	movq	-136(%rbp), %r13
	movq	-120(%rbp), %r14
	jmp	.L209
	.p2align 4,,10
	.p2align 3
.L257:
	movl	$376, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$143, %edx
	xorl	%r14d, %r14d
	movl	$112, %esi
	movl	$33, %edi
	call	ERR_put_error@PLT
	jmp	.L172
.L222:
	movq	-136(%rbp), %r13
	movq	-120(%rbp), %r14
	xorl	%r12d, %r12d
	xorl	%r15d, %r15d
	jmp	.L188
.L228:
	movq	%r13, %r14
	jmp	.L199
.L273:
	movq	-88(%rbp), %rax
	movl	%r15d, -76(%rbp)
	movq	$0, -88(%rbp)
	movq	%rax, -64(%rbp)
	jmp	.L210
.L267:
	movl	$111, %edx
	movl	$112, %esi
	xorl	%r13d, %r13d
	xorl	%r12d, %r12d
	movl	$412, %r8d
	leaq	.LC0(%rip), %rcx
	xorl	%r14d, %r14d
	xorl	%r15d, %r15d
	movl	$33, %edi
	call	ERR_put_error@PLT
	movq	$0, -88(%rbp)
	jmp	.L188
.L261:
	movl	$111, %edx
	movl	$112, %esi
	xorl	%r13d, %r13d
	xorl	%r14d, %r14d
	movl	$424, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$33, %edi
	xorl	%r15d, %r15d
	call	ERR_put_error@PLT
	movq	$0, -88(%rbp)
	jmp	.L188
.L183:
	movl	$398, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$155, %edx
	jmp	.L255
.L262:
	movl	$32, %edx
	movl	$112, %esi
	xorl	%r12d, %r12d
	xorl	%r15d, %r15d
	movl	$468, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$33, %edi
	call	ERR_put_error@PLT
	movq	$0, -88(%rbp)
	jmp	.L188
.L274:
	movslq	-76(%rbp), %rsi
	movq	-64(%rbp), %rdi
	movl	$544, %ecx
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_clear_free@PLT
	movq	-88(%rbp), %rax
	movl	%ebx, -76(%rbp)
	movq	$0, -88(%rbp)
	movq	%rax, -64(%rbp)
	jmp	.L211
.L260:
	call	__stack_chk_fail@PLT
.L182:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$2, %esi
	movq	%rbx, %rdi
	call	PKCS7_ctrl@PLT
	testq	%rax, %rax
	je	.L185
	jmp	.L249
.L271:
	movq	-136(%rbp), %r12
	jmp	.L216
	.cfi_endproc
.LFE1303:
	.size	PKCS7_dataDecode, .-PKCS7_dataDecode
	.p2align 4
	.globl	PKCS7_SIGNER_INFO_sign
	.type	PKCS7_SIGNER_INFO_sign, @function
PKCS7_SIGNER_INFO_sign:
.LFB1307:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	$0, -64(%rbp)
	movq	$0, -56(%rbp)
	movq	(%rax), %rdi
	call	OBJ_obj2nid@PLT
	movl	%eax, %edi
	call	OBJ_nid2sn@PLT
	movq	%rax, %rdi
	call	EVP_get_digestbyname@PLT
	movq	%rax, %r12
	xorl	%eax, %eax
	testq	%r12, %r12
	je	.L275
	call	EVP_MD_CTX_new@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L296
	movq	56(%rbx), %r8
	xorl	%ecx, %ecx
	leaq	-64(%rbp), %rsi
	movq	%r12, %rdx
	movq	%rax, %rdi
	call	EVP_DigestSignInit@PLT
	testl	%eax, %eax
	jle	.L279
	movq	-64(%rbp), %rdi
	xorl	%r8d, %r8d
	movq	%rbx, %r9
	movl	$5, %ecx
	movl	$8, %edx
	movl	$-1, %esi
	call	EVP_PKEY_CTX_ctrl@PLT
	movl	$843, %r8d
	testl	%eax, %eax
	jle	.L295
	movq	24(%rbx), %rdi
	leaq	-56(%rbp), %rsi
	leaq	PKCS7_ATTR_SIGN_it(%rip), %rdx
	call	ASN1_item_i2d@PLT
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L278
	movq	%rdi, %rsi
	movslq	%eax, %rdx
	movq	%r13, %rdi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	jle	.L279
	movq	-56(%rbp), %rdi
	movl	$853, %edx
	leaq	.LC0(%rip), %rsi
	leaq	-48(%rbp), %r12
	call	CRYPTO_free@PLT
	xorl	%esi, %esi
	movq	%r12, %rdx
	movq	%r13, %rdi
	movq	$0, -56(%rbp)
	call	EVP_DigestSignFinal@PLT
	testl	%eax, %eax
	jle	.L279
	movq	-48(%rbp), %rdi
	movl	$857, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_malloc@PLT
	movq	%rax, -56(%rbp)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L278
	movq	%r12, %rdx
	movq	%rax, %rsi
	movq	%r13, %rdi
	call	EVP_DigestSignFinal@PLT
	testl	%eax, %eax
	jle	.L279
	movq	-64(%rbp), %rdi
	movq	%rbx, %r9
	movl	$1, %r8d
	movl	$5, %ecx
	movl	$8, %edx
	movl	$-1, %esi
	call	EVP_PKEY_CTX_ctrl@PLT
	testl	%eax, %eax
	jle	.L297
	movq	%r13, %rdi
	call	EVP_MD_CTX_free@PLT
	movq	40(%rbx), %rdi
	movl	-48(%rbp), %edx
	movq	-56(%rbp), %rsi
	call	ASN1_STRING_set0@PLT
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L275:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L298
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L279:
	.cfi_restore_state
	movq	-56(%rbp), %rdi
.L278:
	movl	$876, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%r13, %rdi
	call	EVP_MD_CTX_free@PLT
	xorl	%eax, %eax
	jmp	.L275
	.p2align 4,,10
	.p2align 3
.L296:
	movl	$33, %edi
	movl	$834, %r8d
	movl	$65, %edx
	movl	$139, %esi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	-56(%rbp), %rdi
	jmp	.L278
.L297:
	movl	$865, %r8d
	.p2align 4,,10
	.p2align 3
.L295:
	movl	$33, %edi
	movl	$152, %edx
	movl	$139, %esi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	-56(%rbp), %rdi
	jmp	.L278
.L298:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1307:
	.size	PKCS7_SIGNER_INFO_sign, .-PKCS7_SIGNER_INFO_sign
	.p2align 4
	.globl	PKCS7_dataFinal
	.type	PKCS7_dataFinal, @function
PKCS7_dataFinal:
.LFB1306:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -152(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L384
	cmpq	$0, 32(%rdi)
	movq	%rdi, %r13
	je	.L385
	call	EVP_MD_CTX_new@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L386
	movq	24(%r13), %rdi
	call	OBJ_obj2nid@PLT
	movl	$0, 16(%r13)
	subl	$21, %eax
	cmpl	$4, %eax
	ja	.L304
	leaq	.L306(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L306:
	.long	.L310-.L306
	.long	.L309-.L306
	.long	.L308-.L306
	.long	.L307-.L306
	.long	.L305-.L306
	.text
	.p2align 4,,10
	.p2align 3
.L307:
	movq	32(%r13), %rax
	movq	32(%rax), %r15
	movq	40(%rax), %rax
	movq	16(%rax), %rax
	movq	%rax, -176(%rbp)
	testq	%rax, %rax
	je	.L387
.L312:
	leaq	-144(%rbp), %rax
	xorl	%ebx, %ebx
	movq	%rax, -160(%rbp)
	testq	%r15, %r15
	jne	.L339
	jmp	.L311
	.p2align 4,,10
	.p2align 3
.L391:
	movq	24(%r12), %rdi
	movl	$-1, %edx
	movl	$52, %esi
	movq	%rdi, -168(%rbp)
	call	X509at_get_attr_by_NID@PLT
	movq	-168(%rbp), %rdi
	movl	%eax, %esi
	call	X509at_get_attr@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	X509_ATTRIBUTE_get0_type@PLT
	testq	%rax, %rax
	je	.L388
.L326:
	leaq	-128(%rbp), %rsi
	leaq	-136(%rbp), %rdx
	movq	%r14, %rdi
	movq	%rsi, -168(%rbp)
	call	EVP_DigestFinal_ex@PLT
	movq	-168(%rbp), %rsi
	testl	%eax, %eax
	je	.L389
	movl	-136(%rbp), %edx
	movq	%r12, %rdi
	call	PKCS7_add1_attrib_digest@PLT
	testl	%eax, %eax
	je	.L390
	movq	%r12, %rdi
	call	PKCS7_SIGNER_INFO_sign
	testl	%eax, %eax
	je	.L314
	.p2align 4,,10
	.p2align 3
.L322:
	addl	$1, %ebx
.L339:
	movq	%r15, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jge	.L311
	movl	%ebx, %esi
	movq	%r15, %rdi
	call	OPENSSL_sk_value@PLT
	cmpq	$0, 56(%rax)
	movq	%rax, %r12
	je	.L322
	movq	16(%rax), %rax
	movq	(%rax), %rdi
	call	OBJ_obj2nid@PLT
	movq	-152(%rbp), %rsi
	movq	-160(%rbp), %rdi
	movl	%eax, %edx
	call	PKCS7_find_digest
	testq	%rax, %rax
	je	.L382
	movq	-144(%rbp), %rsi
	movq	%r14, %rdi
	call	EVP_MD_CTX_copy_ex@PLT
	testl	%eax, %eax
	je	.L382
	movq	24(%r12), %rdi
	call	OPENSSL_sk_num@PLT
	testl	%eax, %eax
	jg	.L391
	movq	56(%r12), %rdi
	call	EVP_PKEY_size@PLT
	movl	$764, %edx
	leaq	.LC0(%rip), %rsi
	movl	%eax, %edi
	movl	%eax, -136(%rbp)
	call	CRYPTO_malloc@PLT
	testq	%rax, %rax
	je	.L382
	movq	56(%r12), %rcx
	leaq	-136(%rbp), %rdx
	movq	%rax, %rsi
	movq	%r14, %rdi
	movq	%rax, -168(%rbp)
	call	EVP_SignFinal@PLT
	movq	-168(%rbp), %r8
	testl	%eax, %eax
	je	.L392
	movq	40(%r12), %rdi
	movl	-136(%rbp), %edx
	movq	%r8, %rsi
	call	ASN1_STRING_set0@PLT
	jmp	.L322
	.p2align 4,,10
	.p2align 3
.L305:
	movq	32(%r13), %rax
	movq	16(%rax), %rbx
	movq	24(%rbx), %rdi
	call	OBJ_obj2nid@PLT
	cmpl	$21, %eax
	je	.L393
	movq	24(%rbx), %rdi
	call	OBJ_obj2nid@PLT
	movq	$0, -176(%rbp)
	subl	$21, %eax
	cmpl	$5, %eax
	jbe	.L320
	movq	32(%rbx), %rax
	movq	%rax, -176(%rbp)
	testq	%rax, %rax
	je	.L320
	cmpl	$4, (%rax)
	je	.L394
	movq	$0, -176(%rbp)
	.p2align 4,,10
	.p2align 3
.L320:
	movq	32(%r13), %rax
	movq	16(%rax), %rax
	movq	24(%rax), %rdi
	call	OBJ_obj2nid@PLT
	cmpl	$21, %eax
	je	.L395
.L321:
	movq	32(%r13), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rdi
	call	OBJ_obj2nid@PLT
	movq	-152(%rbp), %rsi
	leaq	-144(%rbp), %rdi
	movl	%eax, %edx
	call	PKCS7_find_digest
	testq	%rax, %rax
	je	.L382
	leaq	-128(%rbp), %r12
	movq	-144(%rbp), %rdi
	leaq	-136(%rbp), %rdx
	movq	%r12, %rsi
	call	EVP_DigestFinal_ex@PLT
	testl	%eax, %eax
	jne	.L396
	.p2align 4,,10
	.p2align 3
.L382:
	xorl	%eax, %eax
	jmp	.L314
	.p2align 4,,10
	.p2align 3
.L310:
	movq	32(%r13), %rax
	movq	%rax, -176(%rbp)
.L311:
	movq	24(%r13), %rdi
	call	OBJ_obj2nid@PLT
	cmpl	$22, %eax
	je	.L397
.L334:
	cmpq	$0, -176(%rbp)
	je	.L382
.L337:
	movq	-176(%rbp), %rax
	testb	$16, 16(%rax)
	je	.L398
.L383:
	movl	$1, %eax
.L314:
	movq	%r14, %rdi
	movl	%eax, -152(%rbp)
	call	EVP_MD_CTX_free@PLT
	movl	-152(%rbp), %eax
.L299:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L399
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L309:
	.cfi_restore_state
	movq	32(%r13), %rax
	movq	40(%rax), %rbx
	movq	32(%rax), %r15
	movq	24(%rbx), %rdi
	call	OBJ_obj2nid@PLT
	cmpl	$21, %eax
	je	.L400
	movq	24(%rbx), %rdi
	call	OBJ_obj2nid@PLT
	movq	$0, -176(%rbp)
	subl	$21, %eax
	cmpl	$5, %eax
	jbe	.L318
	movq	32(%rbx), %rax
	movq	%rax, -176(%rbp)
	testq	%rax, %rax
	je	.L318
	cmpl	$4, (%rax)
	je	.L401
	movq	$0, -176(%rbp)
	.p2align 4,,10
	.p2align 3
.L318:
	movq	32(%r13), %rax
	movq	40(%rax), %rax
	movq	24(%rax), %rdi
	call	OBJ_obj2nid@PLT
	cmpl	$21, %eax
	jne	.L312
	movl	20(%r13), %edx
	testl	%edx, %edx
	je	.L312
	movq	-176(%rbp), %rdi
	call	ASN1_OCTET_STRING_free@PLT
	movq	32(%r13), %rax
	movq	$0, -176(%rbp)
	movq	40(%rax), %rax
	movq	$0, 32(%rax)
	jmp	.L312
	.p2align 4,,10
	.p2align 3
.L308:
	movq	32(%r13), %rax
	movq	16(%rax), %rax
	movq	16(%rax), %rax
	movq	%rax, -176(%rbp)
	testq	%rax, %rax
	je	.L402
.L315:
	movq	24(%r13), %rdi
	call	OBJ_obj2nid@PLT
	cmpl	$22, %eax
	jne	.L337
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$2, %esi
	movq	%r13, %rdi
	call	PKCS7_ctrl@PLT
	testq	%rax, %rax
	je	.L337
	jmp	.L383
	.p2align 4,,10
	.p2align 3
.L389:
	movl	$630, %r8d
	movl	$6, %edx
	movl	$136, %esi
	movl	$33, %edi
	leaq	.LC0(%rip), %rcx
	movl	%eax, -152(%rbp)
	call	ERR_put_error@PLT
	movl	-152(%rbp), %eax
	jmp	.L314
	.p2align 4,,10
	.p2align 3
.L388:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	PKCS7_add0_attrib_signing_time@PLT
	testl	%eax, %eax
	jne	.L326
	movl	%eax, -152(%rbp)
	movl	$623, %r8d
.L381:
	movl	$65, %edx
	movl	$136, %esi
	movl	$33, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	-152(%rbp), %eax
	jmp	.L314
	.p2align 4,,10
	.p2align 3
.L390:
	movl	%eax, -152(%rbp)
	movl	$634, %r8d
	jmp	.L381
	.p2align 4,,10
	.p2align 3
.L385:
	movl	$662, %r8d
	movl	$122, %edx
	movl	$128, %esi
	movl	$33, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L299
	.p2align 4,,10
	.p2align 3
.L386:
	movl	$668, %r8d
	movl	$65, %edx
	movl	$128, %esi
	movl	$33, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L299
.L304:
	movl	$726, %r8d
	movl	$112, %edx
	movl	$128, %esi
	movl	$33, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L314
	.p2align 4,,10
	.p2align 3
.L384:
	movl	$657, %r8d
	movl	$143, %edx
	movl	$128, %esi
	movl	$33, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L299
	.p2align 4,,10
	.p2align 3
.L398:
	movq	-152(%rbp), %rdi
	movl	$1025, %esi
	call	BIO_find_type@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L403
	leaq	-136(%rbp), %rcx
	xorl	%edx, %edx
	movl	$3, %esi
	movq	%rax, %rdi
	call	BIO_ctrl@PLT
	movq	%r12, %rdi
	movl	$512, %esi
	movq	%rax, %rbx
	call	BIO_set_flags@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	xorl	%ecx, %ecx
	movl	$130, %esi
	call	BIO_ctrl@PLT
	movq	-136(%rbp), %rsi
	movq	-176(%rbp), %rdi
	movl	%ebx, %edx
	call	ASN1_STRING_set0@PLT
	jmp	.L383
	.p2align 4,,10
	.p2align 3
.L397:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$2, %esi
	movq	%r13, %rdi
	call	PKCS7_ctrl@PLT
	testq	%rax, %rax
	je	.L334
	jmp	.L383
	.p2align 4,,10
	.p2align 3
.L396:
	movq	32(%r13), %rax
	movl	-136(%rbp), %edx
	movq	%r12, %rsi
	movq	24(%rax), %rdi
	call	ASN1_OCTET_STRING_set@PLT
	testl	%eax, %eax
	jne	.L311
	xorl	%eax, %eax
	jmp	.L314
	.p2align 4,,10
	.p2align 3
.L400:
	movq	32(%rbx), %rax
	movq	%rax, -176(%rbp)
	jmp	.L318
	.p2align 4,,10
	.p2align 3
.L393:
	movq	32(%rbx), %rax
	movq	%rax, -176(%rbp)
	jmp	.L320
	.p2align 4,,10
	.p2align 3
.L395:
	movl	20(%r13), %eax
	testl	%eax, %eax
	je	.L321
	movq	-176(%rbp), %rdi
	call	ASN1_OCTET_STRING_free@PLT
	movq	32(%r13), %rax
	movq	$0, -176(%rbp)
	movq	16(%rax), %rax
	movq	$0, 32(%rax)
	jmp	.L321
.L387:
	call	ASN1_OCTET_STRING_new@PLT
	movl	$686, %r8d
	movq	%rax, -176(%rbp)
	testq	%rax, %rax
	je	.L380
	movq	%rax, %rcx
	movq	32(%r13), %rax
	movq	40(%rax), %rax
	movq	%rcx, 16(%rax)
	jmp	.L312
.L402:
	call	ASN1_OCTET_STRING_new@PLT
	movq	%rax, -176(%rbp)
	testq	%rax, %rax
	je	.L404
	movq	%rax, %rcx
	movq	32(%r13), %rax
	movq	16(%rax), %rax
	movq	%rcx, 16(%rax)
	jmp	.L315
.L403:
	movl	$800, %r8d
	movl	$107, %edx
	movl	$128, %esi
	movl	$33, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L314
.L392:
	movq	%r8, %rdi
	movl	$769, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movl	$770, %r8d
	movl	$6, %edx
	leaq	.LC0(%rip), %rcx
	movl	$128, %esi
	movl	$33, %edi
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L314
.L394:
	movq	8(%rax), %rax
	movq	%rax, -176(%rbp)
	jmp	.L320
.L401:
	movq	8(%rax), %rax
	movq	%rax, -176(%rbp)
	jmp	.L318
.L404:
	movl	$698, %r8d
.L380:
	movl	$65, %edx
	movl	$128, %esi
	movl	$33, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L314
.L399:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1306:
	.size	PKCS7_dataFinal, .-PKCS7_dataFinal
	.p2align 4
	.globl	PKCS7_signatureVerify
	.type	PKCS7_signatureVerify, @function
PKCS7_signatureVerify:
.LFB1309:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$136, %rsp
	movq	%rcx, -168(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	EVP_MD_CTX_new@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L463
	movq	24(%rbx), %rdi
	call	OBJ_obj2nid@PLT
	cmpl	$22, %eax
	je	.L408
	movq	24(%rbx), %rdi
	call	OBJ_obj2nid@PLT
	cmpl	$24, %eax
	jne	.L464
.L408:
	movq	16(%r15), %rax
	leaq	-144(%rbp), %rbx
	movq	(%rax), %rdi
	call	OBJ_obj2nid@PLT
	movl	%eax, %r13d
	testq	%r12, %r12
	jne	.L409
	jmp	.L410
	.p2align 4,,10
	.p2align 3
.L466:
	movq	%rax, %rdi
	movq	%rbx, %rcx
	xorl	%edx, %edx
	movl	$120, %esi
	call	BIO_ctrl@PLT
	movq	-144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L465
	call	EVP_MD_CTX_md@PLT
	movq	%rax, %rdi
	call	EVP_MD_type@PLT
	cmpl	%r13d, %eax
	je	.L412
	movq	-144(%rbp), %rdi
	call	EVP_MD_CTX_md@PLT
	movq	%rax, %rdi
	call	EVP_MD_pkey_type@PLT
	cmpl	%r13d, %eax
	je	.L412
	movq	%r12, %rdi
	call	BIO_next@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L410
.L409:
	movq	%r12, %rdi
	movl	$520, %esi
	call	BIO_find_type@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L466
.L410:
	movl	$967, %r8d
.L461:
	movl	$108, %edx
	movl	$113, %esi
	movl	$33, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L462:
	xorl	%r12d, %r12d
.L407:
	movq	%r14, %rdi
	call	EVP_MD_CTX_free@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L467
	addq	$136, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L412:
	.cfi_restore_state
	movq	-144(%rbp), %rsi
	movq	%r14, %rdi
	call	EVP_MD_CTX_copy_ex@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	je	.L407
	movq	24(%r15), %rbx
	testq	%rbx, %rbx
	je	.L421
	movq	%rbx, %rdi
	call	OPENSSL_sk_num@PLT
	testl	%eax, %eax
	jne	.L468
.L421:
	movq	-168(%rbp), %rdi
	movq	40(%r15), %rbx
	movl	$-1, %r12d
	call	X509_get0_pubkey@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L407
	movq	8(%rbx), %rsi
	movl	(%rbx), %edx
	movq	%r14, %rdi
	movl	$1, %r12d
	call	EVP_VerifyFinal@PLT
	testl	%eax, %eax
	jg	.L407
	movl	$1041, %r8d
	movl	$105, %edx
	movl	$113, %esi
	movl	$33, %edi
	leaq	.LC0(%rip), %rcx
	movl	$-1, %r12d
	call	ERR_put_error@PLT
	jmp	.L407
	.p2align 4,,10
	.p2align 3
.L464:
	movl	$957, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$114, %edx
	xorl	%r12d, %r12d
	movl	$113, %esi
	movl	$33, %edi
	call	ERR_put_error@PLT
	jmp	.L407
	.p2align 4,,10
	.p2align 3
.L463:
	movl	$952, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r12d, %r12d
	movl	$113, %esi
	movl	$33, %edi
	call	ERR_put_error@PLT
	jmp	.L407
	.p2align 4,,10
	.p2align 3
.L465:
	movl	$973, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$68, %edx
	xorl	%r12d, %r12d
	movl	$113, %esi
	movl	$33, %edi
	call	ERR_put_error@PLT
	jmp	.L407
.L468:
	leaq	-128(%rbp), %r12
	movq	%r14, %rdi
	leaq	-148(%rbp), %rdx
	movq	$0, -136(%rbp)
	movq	%r12, %rsi
	call	EVP_DigestFinal_ex@PLT
	testl	%eax, %eax
	je	.L462
	movl	$-1, %edx
	movl	$51, %esi
	movq	%rbx, %rdi
	call	X509at_get_attr_by_NID@PLT
	movq	%rbx, %rdi
	movl	%eax, %esi
	call	X509at_get_attr@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	X509_ATTRIBUTE_get0_type@PLT
	testq	%rax, %rax
	je	.L415
	movq	8(%rax), %rcx
	testq	%rcx, %rcx
	je	.L415
	movl	-148(%rbp), %eax
	cmpl	%eax, (%rcx)
	jne	.L418
	movq	8(%rcx), %rdi
	movl	%eax, %edx
	movq	%r12, %rsi
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L418
	movl	%r13d, %edi
	call	OBJ_nid2sn@PLT
	movq	%rax, %rdi
	call	EVP_get_digestbyname@PLT
	xorl	%edx, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	EVP_DigestInit_ex@PLT
	testl	%eax, %eax
	je	.L462
	leaq	-136(%rbp), %rsi
	leaq	PKCS7_ATTR_VERIFY_it(%rip), %rdx
	movq	%rbx, %rdi
	call	ASN1_item_i2d@PLT
	testl	%eax, %eax
	jle	.L469
	movq	-136(%rbp), %rsi
	movslq	%eax, %rdx
	movq	%r14, %rdi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	je	.L462
	movq	-136(%rbp), %rdi
	movl	$1029, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	jmp	.L421
	.p2align 4,,10
	.p2align 3
.L418:
	movl	$1011, %r8d
	movl	$101, %edx
	movl	$113, %esi
	movl	$33, %edi
	leaq	.LC0(%rip), %rcx
	movl	$-1, %r12d
	call	ERR_put_error@PLT
	jmp	.L407
.L415:
	movl	$1005, %r8d
	jmp	.L461
.L467:
	call	__stack_chk_fail@PLT
.L469:
	movl	$1022, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$13, %edx
	movl	$113, %esi
	movl	$33, %edi
	orl	$-1, %r12d
	call	ERR_put_error@PLT
	jmp	.L407
	.cfi_endproc
.LFE1309:
	.size	PKCS7_signatureVerify, .-PKCS7_signatureVerify
	.p2align 4
	.globl	PKCS7_dataVerify
	.type	PKCS7_dataVerify, @function
PKCS7_dataVerify:
.LFB1308:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -56(%rbp)
	testq	%rcx, %rcx
	je	.L482
	cmpq	$0, 32(%rcx)
	movq	%rcx, %r12
	je	.L483
	movq	%rdi, %r15
	movq	24(%rcx), %rdi
	movq	%rsi, %r13
	movq	%r8, %r14
	call	OBJ_obj2nid@PLT
	cmpl	$22, %eax
	jne	.L474
.L481:
	movq	32(%r12), %rax
	movq	16(%rax), %rcx
	movq	8(%r14), %rax
	movq	8(%rax), %rdx
	movq	(%rax), %rsi
	movq	%rcx, %rdi
	movq	%rcx, -64(%rbp)
	call	X509_find_by_issuer_and_serial@PLT
	movq	-64(%rbp), %rcx
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.L484
	movq	%rax, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	X509_STORE_CTX_init@PLT
	testl	%eax, %eax
	je	.L485
	movq	%r13, %rdi
	movl	$4, %esi
	call	X509_STORE_CTX_set_purpose@PLT
	movq	%r13, %rdi
	call	X509_verify_cert@PLT
	testl	%eax, %eax
	jle	.L486
	movq	%r13, %rdi
	call	X509_STORE_CTX_cleanup@PLT
	movq	-56(%rbp), %rdi
	addq	$24, %rsp
	movq	%rbx, %rcx
	movq	%r14, %rdx
	popq	%rbx
	movq	%r12, %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	PKCS7_signatureVerify
	.p2align 4,,10
	.p2align 3
.L474:
	.cfi_restore_state
	movq	24(%r12), %rdi
	call	OBJ_obj2nid@PLT
	cmpl	$24, %eax
	je	.L481
	movl	$905, %r8d
	movl	$114, %edx
	movl	$107, %esi
	movl	$33, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L470
	.p2align 4,,10
	.p2align 3
.L485:
	movl	$922, %r8d
	movl	$11, %edx
	movl	$107, %esi
	movl	$33, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L470:
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L483:
	.cfi_restore_state
	movl	$896, %r8d
	movl	$122, %edx
	movl	$107, %esi
	movl	$33, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L470
	.p2align 4,,10
	.p2align 3
.L484:
	movl	$915, %r8d
	movl	$106, %edx
	movl	$107, %esi
	movl	$33, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L470
	.p2align 4,,10
	.p2align 3
.L482:
	movl	$891, %r8d
	movl	$143, %edx
	movl	$107, %esi
	movl	$33, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L470
	.p2align 4,,10
	.p2align 3
.L486:
	movl	$928, %r8d
	movl	$11, %edx
	movl	$107, %esi
	movl	$33, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	%r13, %rdi
	call	X509_STORE_CTX_cleanup@PLT
	jmp	.L470
	.cfi_endproc
.LFE1308:
	.size	PKCS7_dataVerify, .-PKCS7_dataVerify
	.p2align 4
	.globl	PKCS7_get_issuer_and_serial
	.type	PKCS7_get_issuer_and_serial, @function
PKCS7_get_issuer_and_serial:
.LFB1310:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	24(%rdi), %rdi
	call	OBJ_obj2nid@PLT
	cmpl	$24, %eax
	jne	.L490
	movq	32(%rbx), %rax
	testq	%rax, %rax
	je	.L490
	movq	48(%rax), %r13
	testq	%r13, %r13
	je	.L490
	movq	%r13, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r12d
	jge	.L490
	movl	%r12d, %esi
	movq	%r13, %rdi
	call	OPENSSL_sk_value@PLT
	movq	8(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L490:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1310:
	.size	PKCS7_get_issuer_and_serial, .-PKCS7_get_issuer_and_serial
	.p2align 4
	.globl	PKCS7_get_signed_attribute
	.type	PKCS7_get_signed_attribute, @function
PKCS7_get_signed_attribute:
.LFB1311:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$-1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	24(%rdi), %r12
	movq	%r12, %rdi
	call	X509at_get_attr_by_NID@PLT
	movq	%r12, %rdi
	movl	%eax, %esi
	call	X509at_get_attr@PLT
	addq	$8, %rsp
	xorl	%esi, %esi
	popq	%r12
	movq	%rax, %rdi
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	X509_ATTRIBUTE_get0_type@PLT
	.cfi_endproc
.LFE1311:
	.size	PKCS7_get_signed_attribute, .-PKCS7_get_signed_attribute
	.p2align 4
	.globl	PKCS7_get_attribute
	.type	PKCS7_get_attribute, @function
PKCS7_get_attribute:
.LFB1312:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$-1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	48(%rdi), %r12
	movq	%r12, %rdi
	call	X509at_get_attr_by_NID@PLT
	movq	%r12, %rdi
	movl	%eax, %esi
	call	X509at_get_attr@PLT
	addq	$8, %rsp
	xorl	%esi, %esi
	popq	%r12
	movq	%rax, %rdi
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	X509_ATTRIBUTE_get0_type@PLT
	.cfi_endproc
.LFE1312:
	.size	PKCS7_get_attribute, .-PKCS7_get_attribute
	.p2align 4
	.globl	PKCS7_digest_from_attributes
	.type	PKCS7_digest_from_attributes, @function
PKCS7_digest_from_attributes:
.LFB1314:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$-1, %edx
	movl	$51, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	X509at_get_attr_by_NID@PLT
	movq	%r12, %rdi
	movl	%eax, %esi
	call	X509at_get_attr@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	X509_ATTRIBUTE_get0_type@PLT
	testq	%rax, %rax
	je	.L502
	movq	8(%rax), %rax
.L502:
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1314:
	.size	PKCS7_digest_from_attributes, .-PKCS7_digest_from_attributes
	.p2align 4
	.globl	PKCS7_set_signed_attributes
	.type	PKCS7_set_signed_attributes, @function
PKCS7_set_signed_attributes:
.LFB1315:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	xorl	%ebx, %ebx
	subq	$8, %rsp
	movq	24(%rdi), %rdi
	movq	X509_ATTRIBUTE_free@GOTPCREL(%rip), %rsi
	call	OPENSSL_sk_pop_free@PLT
	movq	%r12, %rdi
	call	OPENSSL_sk_dup@PLT
	movq	%rax, 24(%r13)
	testq	%rax, %rax
	jne	.L509
	jmp	.L511
	.p2align 4,,10
	.p2align 3
.L512:
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %rdi
	call	X509_ATTRIBUTE_dup@PLT
	movq	24(%r13), %rdi
	movl	%ebx, %esi
	movq	%rax, %rdx
	call	OPENSSL_sk_set@PLT
	testq	%rax, %rax
	je	.L511
	addl	$1, %ebx
.L509:
	movq	%r12, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jl	.L512
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L511:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1315:
	.size	PKCS7_set_signed_attributes, .-PKCS7_set_signed_attributes
	.p2align 4
	.globl	PKCS7_set_attributes
	.type	PKCS7_set_attributes, @function
PKCS7_set_attributes:
.LFB1316:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	xorl	%ebx, %ebx
	subq	$8, %rsp
	movq	48(%rdi), %rdi
	movq	X509_ATTRIBUTE_free@GOTPCREL(%rip), %rsi
	call	OPENSSL_sk_pop_free@PLT
	movq	%r12, %rdi
	call	OPENSSL_sk_dup@PLT
	movq	%rax, 48(%r13)
	testq	%rax, %rax
	jne	.L521
	jmp	.L523
	.p2align 4,,10
	.p2align 3
.L524:
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %rdi
	call	X509_ATTRIBUTE_dup@PLT
	movq	48(%r13), %rdi
	movl	%ebx, %esi
	movq	%rax, %rdx
	call	OPENSSL_sk_set@PLT
	testq	%rax, %rax
	je	.L523
	addl	$1, %ebx
.L521:
	movq	%r12, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jl	.L524
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L523:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1316:
	.size	PKCS7_set_attributes, .-PKCS7_set_attributes
	.p2align 4
	.globl	PKCS7_add_signed_attribute
	.type	PKCS7_add_signed_attribute, @function
PKCS7_add_signed_attribute:
.LFB1317:
	.cfi_startproc
	endbr64
	addq	$24, %rdi
	jmp	add_attribute
	.cfi_endproc
.LFE1317:
	.size	PKCS7_add_signed_attribute, .-PKCS7_add_signed_attribute
	.p2align 4
	.globl	PKCS7_add_attribute
	.type	PKCS7_add_attribute, @function
PKCS7_add_attribute:
.LFB1318:
	.cfi_startproc
	endbr64
	addq	$48, %rdi
	jmp	add_attribute
	.cfi_endproc
.LFE1318:
	.size	PKCS7_add_attribute, .-PKCS7_add_attribute
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
