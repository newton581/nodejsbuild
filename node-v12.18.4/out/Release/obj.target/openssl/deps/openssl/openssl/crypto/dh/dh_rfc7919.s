	.file	"dh_rfc7919.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/dh/dh_rfc7919.c"
	.text
	.p2align 4
	.globl	DH_new_by_nid
	.type	DH_new_by_nid, @function
DH_new_by_nid:
.LFB422:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	subl	$1126, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	cmpl	$4, %edi
	ja	.L2
	leaq	.L4(%rip), %rdx
	movslq	(%rdx,%rdi,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L4:
	.long	.L8-.L4
	.long	.L7-.L4
	.long	.L6-.L4
	.long	.L5-.L4
	.long	.L3-.L4
	.text
	.p2align 4,,10
	.p2align 3
.L5:
	call	DH_new@PLT
	testq	%rax, %rax
	je	.L1
	leaq	_bignum_const_2(%rip), %rdx
	leaq	_bignum_ffdhe6144_p(%rip), %rsi
	movl	$375, 24(%rax)
	movq	%rsi, %xmm0
	movq	%rdx, %xmm4
	punpcklqdq	%xmm4, %xmm0
	movups	%xmm0, 8(%rax)
.L1:
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	.cfi_restore_state
	call	DH_new@PLT
	testq	%rax, %rax
	je	.L1
	leaq	_bignum_const_2(%rip), %rdx
	leaq	_bignum_ffdhe8192_p(%rip), %rcx
	movl	$400, 24(%rax)
	movq	%rcx, %xmm0
	movq	%rdx, %xmm5
	punpcklqdq	%xmm5, %xmm0
	movups	%xmm0, 8(%rax)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	.cfi_restore_state
	call	DH_new@PLT
	testq	%rax, %rax
	je	.L1
	leaq	_bignum_const_2(%rip), %rdx
	leaq	_bignum_ffdhe3072_p(%rip), %rsi
	movl	$275, 24(%rax)
	movq	%rsi, %xmm0
	movq	%rdx, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 8(%rax)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	.cfi_restore_state
	call	DH_new@PLT
	testq	%rax, %rax
	je	.L1
	leaq	_bignum_const_2(%rip), %rdx
	leaq	_bignum_ffdhe4096_p(%rip), %rcx
	movl	$325, 24(%rax)
	movq	%rcx, %xmm0
	movq	%rdx, %xmm3
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 8(%rax)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	.cfi_restore_state
	call	DH_new@PLT
	testq	%rax, %rax
	je	.L1
	leaq	_bignum_const_2(%rip), %rdx
	leaq	_bignum_ffdhe2048_p(%rip), %rcx
	movl	$225, 24(%rax)
	movq	%rcx, %xmm0
	movq	%rdx, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 8(%rax)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2:
	.cfi_restore_state
	movl	$42, %r8d
	movl	$114, %edx
	movl	$104, %esi
	movl	$5, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE422:
	.size	DH_new_by_nid, .-DH_new_by_nid
	.p2align 4
	.globl	DH_get_nid
	.type	DH_get_nid, @function
DH_get_nid:
.LFB423:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	16(%rdi), %rdi
	call	BN_get_word@PLT
	cmpq	$2, %rax
	je	.L28
.L31:
	xorl	%r12d, %r12d
.L27:
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L28:
	.cfi_restore_state
	movq	8(%rbx), %rdi
	leaq	_bignum_ffdhe2048_p(%rip), %rsi
	movl	$1126, %r12d
	call	BN_cmp@PLT
	testl	%eax, %eax
	jne	.L49
.L30:
	cmpq	$0, 64(%rbx)
	je	.L27
	movq	8(%rbx), %rdi
	call	BN_dup@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L32
	movq	%rax, %rsi
	movq	%rax, %rdi
	call	BN_rshift1@PLT
	testl	%eax, %eax
	jne	.L50
.L32:
	xorl	%r12d, %r12d
.L33:
	movq	%r13, %rdi
	call	BN_free@PLT
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L49:
	.cfi_restore_state
	movq	8(%rbx), %rdi
	leaq	_bignum_ffdhe3072_p(%rip), %rsi
	movl	$1127, %r12d
	call	BN_cmp@PLT
	testl	%eax, %eax
	je	.L30
	movq	8(%rbx), %rdi
	leaq	_bignum_ffdhe4096_p(%rip), %rsi
	movl	$1128, %r12d
	call	BN_cmp@PLT
	testl	%eax, %eax
	je	.L30
	movq	8(%rbx), %rdi
	leaq	_bignum_ffdhe6144_p(%rip), %rsi
	movl	$1129, %r12d
	call	BN_cmp@PLT
	testl	%eax, %eax
	je	.L30
	movq	8(%rbx), %rdi
	leaq	_bignum_ffdhe8192_p(%rip), %rsi
	call	BN_cmp@PLT
	testl	%eax, %eax
	jne	.L31
	movl	$1130, %r12d
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L50:
	movq	64(%rbx), %rdi
	movq	%r13, %rsi
	call	BN_cmp@PLT
	testl	%eax, %eax
	jne	.L33
	jmp	.L32
	.cfi_endproc
.LFE423:
	.size	DH_get_nid, .-DH_get_nid
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
