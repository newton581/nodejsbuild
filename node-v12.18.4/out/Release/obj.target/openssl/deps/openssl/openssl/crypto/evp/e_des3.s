	.file	"e_des3.c"
	.text
	.p2align 4
	.type	des_ede_cbc_cipher, @function
des_ede_cbc_cipher:
.LFB471:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	384(%rax), %rbx
	movq	%rax, %r13
	testq	%rbx, %rbx
	jne	.L2
	movabsq	$4611686018427387903, %rdx
	movq	%r12, %r15
	cmpq	%rdx, %r12
	jbe	.L4
	leaq	256(%rax), %rax
	movq	%rdx, -56(%rbp)
	leaq	128(%r13), %r15
	movq	%rax, -80(%rbp)
	movq	-64(%rbp), %rax
	movq	%r12, -104(%rbp)
	addq	%r12, %rax
	movq	%rax, -96(%rbp)
	movq	-72(%rbp), %rax
	addq	%r12, %rax
	movq	%rax, -88(%rbp)
.L6:
	movq	%r14, %rdi
	call	EVP_CIPHER_CTX_encrypting@PLT
	movq	%r14, %rdi
	movl	%eax, %ebx
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	pushq	%rbx
	movq	-96(%rbp), %rsi
	movq	%r13, %rcx
	movq	-88(%rbp), %rdi
	movq	-80(%rbp), %r9
	pushq	%rax
	movq	%r15, %r8
	movabsq	$4611686018427387904, %rdx
	subq	%r12, %rsi
	subq	%r12, %rdi
	call	DES_ede3_cbc_encrypt@PLT
	popq	%rcx
	popq	%rsi
	movabsq	$-4611686018427387904, %rax
	addq	%rax, %r12
	cmpq	-56(%rbp), %r12
	ja	.L6
	movq	-104(%rbp), %r12
	movq	-56(%rbp), %r15
	andq	%r12, %r15
	addq	%rax, %r12
	shrq	$62, %r12
	addq	$1, %r12
	salq	$62, %r12
	addq	%r12, -72(%rbp)
	addq	%r12, -64(%rbp)
.L4:
	testq	%r15, %r15
	jne	.L13
.L5:
	leaq	-40(%rbp), %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	.cfi_restore_state
	movq	%r14, %rdi
	call	EVP_CIPHER_CTX_encrypting@PLT
	movq	%r14, %rdi
	movl	%eax, %ebx
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	pushq	%rbx
	movq	-64(%rbp), %rsi
	movq	%r15, %rdx
	pushq	%rax
	movq	-72(%rbp), %rdi
	leaq	256(%r13), %r9
	movq	%r13, %rcx
	leaq	128(%r13), %r8
	call	DES_ede3_cbc_encrypt@PLT
	popq	%rax
	popq	%rdx
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L2:
	movq	%r14, %rdi
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movq	-64(%rbp), %rsi
	movq	-72(%rbp), %rdi
	movq	%r13, %rcx
	movq	%rax, %r8
	movq	%r12, %rdx
	call	*%rbx
	jmp	.L5
	.cfi_endproc
.LFE471:
	.size	des_ede_cbc_cipher, .-des_ede_cbc_cipher
	.p2align 4
	.type	des_ede3_init_key, @function
des_ede3_init_key:
.LFB486:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	%r12, %rdi
	movq	$0, 384(%rax)
	movq	%rax, %rbx
	movq	%rax, %rsi
	call	DES_set_key_unchecked@PLT
	leaq	128(%rbx), %rsi
	leaq	8(%r12), %rdi
	call	DES_set_key_unchecked@PLT
	leaq	256(%rbx), %rsi
	leaq	16(%r12), %rdi
	call	DES_set_key_unchecked@PLT
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE486:
	.size	des_ede3_init_key, .-des_ede3_init_key
	.p2align 4
	.type	des_ede_init_key, @function
des_ede_init_key:
.LFB485:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	%r12, %rdi
	movq	$0, 384(%rax)
	movq	%rax, %rbx
	movq	%rax, %rsi
	call	DES_set_key_unchecked@PLT
	leaq	128(%rbx), %rsi
	leaq	8(%r12), %rdi
	call	DES_set_key_unchecked@PLT
	movdqu	(%rbx), %xmm0
	movdqu	16(%rbx), %xmm1
	movl	$1, %eax
	movdqu	32(%rbx), %xmm2
	movdqu	48(%rbx), %xmm3
	movdqu	64(%rbx), %xmm4
	movdqu	80(%rbx), %xmm5
	movups	%xmm0, 256(%rbx)
	movdqu	96(%rbx), %xmm6
	movdqu	112(%rbx), %xmm7
	movups	%xmm1, 272(%rbx)
	movups	%xmm2, 288(%rbx)
	movups	%xmm3, 304(%rbx)
	movups	%xmm4, 320(%rbx)
	movups	%xmm5, 336(%rbx)
	movups	%xmm6, 352(%rbx)
	movups	%xmm7, 368(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE485:
	.size	des_ede_init_key, .-des_ede_init_key
	.p2align 4
	.type	des_ede_cfb64_cipher, @function
des_ede_cfb64_cipher:
.LFB472:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -112(%rbp)
	movq	%rdx, -120(%rbp)
	movq	%fs:40, %rdx
	movq	%rdx, -56(%rbp)
	xorl	%edx, %edx
	movabsq	$4611686018427387903, %rdx
	cmpq	%rdx, %rcx
	jbe	.L19
	addq	%r14, %rax
	movq	%rcx, %rbx
	leaq	(%rsi,%rcx), %rcx
	movq	%rdx, -80(%rbp)
	movq	%rax, -88(%rbp)
	leaq	-60(%rbp), %rax
	movq	%rcx, -104(%rbp)
	movq	%rax, -96(%rbp)
	movq	%r14, -128(%rbp)
.L20:
	movq	%r12, %rdi
	call	EVP_CIPHER_CTX_num@PLT
	movq	%r12, %rdi
	movl	%eax, -60(%rbp)
	call	EVP_CIPHER_CTX_encrypting@PLT
	movq	%r12, %rdi
	movl	%eax, -68(%rbp)
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	%r12, %rdi
	movq	%rax, %r15
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movl	-68(%rbp), %r9d
	subq	$8, %rsp
	movq	-104(%rbp), %rsi
	movq	-88(%rbp), %rdi
	movq	%rax, %rcx
	leaq	128(%r13), %r8
	movabsq	$4611686018427387904, %rdx
	pushq	%r9
	subq	%rbx, %rsi
	leaq	256(%r15), %r9
	pushq	-96(%rbp)
	subq	%rbx, %rdi
	pushq	%r14
	call	DES_ede3_cfb64_encrypt@PLT
	movl	-60(%rbp), %esi
	addq	$32, %rsp
	movq	%r12, %rdi
	call	EVP_CIPHER_CTX_set_num@PLT
	movabsq	$-4611686018427387904, %rax
	addq	%rax, %rbx
	cmpq	-80(%rbp), %rbx
	ja	.L20
	movq	-128(%rbp), %r13
	movq	-80(%rbp), %r14
	andq	%r13, %r14
	addq	%rax, %r13
	shrq	$62, %r13
	addq	$1, %r13
	salq	$62, %r13
	addq	%r13, -120(%rbp)
	addq	%r13, -112(%rbp)
.L19:
	testq	%r14, %r14
	jne	.L30
.L21:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L31
	leaq	-40(%rbp), %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	.cfi_restore_state
	movq	%r12, %rdi
	call	EVP_CIPHER_CTX_num@PLT
	movq	%r12, %rdi
	movl	%eax, -60(%rbp)
	call	EVP_CIPHER_CTX_encrypting@PLT
	movq	%r12, %rdi
	movl	%eax, -68(%rbp)
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movq	%r12, %rdi
	movq	%rax, %r15
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movl	-68(%rbp), %edx
	subq	$8, %rsp
	movq	-112(%rbp), %rsi
	movq	%rax, %rcx
	leaq	-60(%rbp), %rax
	movq	-120(%rbp), %rdi
	leaq	256(%r13), %r9
	pushq	%rdx
	leaq	128(%rbx), %r8
	movq	%r14, %rdx
	pushq	%rax
	pushq	%r15
	call	DES_ede3_cfb64_encrypt@PLT
	movl	-60(%rbp), %esi
	addq	$32, %rsp
	movq	%r12, %rdi
	call	EVP_CIPHER_CTX_set_num@PLT
	jmp	.L21
.L31:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE472:
	.size	des_ede_cfb64_cipher, .-des_ede_cfb64_cipher
	.p2align 4
	.type	des_ede_ofb_cipher, @function
des_ede_ofb_cipher:
.LFB470:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	%rdx, -112(%rbp)
	movabsq	$4611686018427387903, %rdx
	movq	%rsi, -104(%rbp)
	movq	%fs:40, %rbx
	movq	%rbx, -56(%rbp)
	xorl	%ebx, %ebx
	cmpq	%rdx, %rcx
	jbe	.L33
	addq	%r13, %rax
	movq	%rcx, %r15
	leaq	(%rsi,%rcx), %rcx
	movq	%rdx, -72(%rbp)
	movq	%rax, -96(%rbp)
	leaq	-60(%rbp), %rax
	movq	%rcx, -88(%rbp)
	movq	%rax, -80(%rbp)
	movq	%r13, -120(%rbp)
.L34:
	movq	%r12, %rdi
	call	EVP_CIPHER_CTX_num@PLT
	movq	%r12, %rdi
	movl	%eax, -60(%rbp)
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	-88(%rbp), %rsi
	movq	-96(%rbp), %rdi
	pushq	-80(%rbp)
	pushq	%r13
	movq	%rax, %rcx
	leaq	256(%r14), %r9
	leaq	128(%rbx), %r8
	movabsq	$4611686018427387904, %rdx
	subq	%r15, %rsi
	subq	%r15, %rdi
	call	DES_ede3_ofb64_encrypt@PLT
	movl	-60(%rbp), %esi
	movq	%r12, %rdi
	call	EVP_CIPHER_CTX_set_num@PLT
	popq	%rcx
	popq	%rsi
	movabsq	$-4611686018427387904, %rax
	addq	%rax, %r15
	cmpq	-72(%rbp), %r15
	ja	.L34
	movq	-120(%rbp), %r15
	movq	-72(%rbp), %r13
	leaq	(%r15,%rax), %r10
	andq	%r15, %r13
	shrq	$62, %r10
	addq	$1, %r10
	salq	$62, %r10
	addq	%r10, -112(%rbp)
	addq	%r10, -104(%rbp)
.L33:
	testq	%r13, %r13
	jne	.L44
.L35:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L45
	leaq	-40(%rbp), %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L44:
	.cfi_restore_state
	movq	%r12, %rdi
	call	EVP_CIPHER_CTX_num@PLT
	movq	%r12, %rdi
	movl	%eax, -60(%rbp)
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movq	%r12, %rdi
	movq	%rax, %r15
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	-104(%rbp), %rsi
	movq	-112(%rbp), %rdi
	movq	%r13, %rdx
	movq	%rax, %rcx
	leaq	-60(%rbp), %rax
	leaq	256(%r14), %r9
	pushq	%rax
	leaq	128(%rbx), %r8
	pushq	%r15
	call	DES_ede3_ofb64_encrypt@PLT
	movl	-60(%rbp), %esi
	movq	%r12, %rdi
	call	EVP_CIPHER_CTX_set_num@PLT
	popq	%rax
	popq	%rdx
	jmp	.L35
.L45:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE470:
	.size	des_ede_ofb_cipher, .-des_ede_ofb_cipher
	.p2align 4
	.type	des_ede_ecb_cipher, @function
des_ede_ecb_cipher:
.LFB469:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$40, %rsp
	movq	%rsi, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	EVP_CIPHER_CTX_cipher@PLT
	movslq	4(%rax), %rax
	movq	%rax, -56(%rbp)
	cmpq	%rbx, %rax
	ja	.L47
	subq	%rax, %rbx
	xorl	%r13d, %r13d
	movq	%rbx, -80(%rbp)
	.p2align 4,,10
	.p2align 3
.L48:
	movq	%r12, %rdi
	call	EVP_CIPHER_CTX_encrypting@PLT
	movq	%r12, %rdi
	movl	%eax, %r15d
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	leaq	128(%rbx), %rcx
	movl	%r15d, %r9d
	leaq	256(%r14), %r8
	movq	%rax, %rdx
	movq	-64(%rbp), %rax
	leaq	(%rax,%r13), %rsi
	movq	-72(%rbp), %rax
	leaq	(%rax,%r13), %rdi
	call	DES_ecb3_encrypt@PLT
	addq	-56(%rbp), %r13
	cmpq	%r13, -80(%rbp)
	jnb	.L48
.L47:
	addq	$40, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE469:
	.size	des_ede_ecb_cipher, .-des_ede_ecb_cipher
	.p2align 4
	.type	des_ede3_cfb1_cipher, @function
des_ede3_cfb1_cipher:
.LFB473:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$88, %rsp
	movq	%rsi, -88(%rbp)
	movl	$8192, %esi
	movq	%rdx, -96(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	EVP_CIPHER_CTX_test_flags@PLT
	movl	%eax, %r8d
	leaq	0(,%rbx,8), %rax
	testl	%r8d, %r8d
	cmovne	%rbx, %rax
	movq	%rax, -104(%rbp)
	testq	%rax, %rax
	je	.L53
	leaq	-57(%rbp), %rax
	xorl	%r13d, %r13d
	movq	%rax, -120(%rbp)
	leaq	-58(%rbp), %rax
	movq	%r13, %r14
	movq	%rax, -112(%rbp)
	.p2align 4,,10
	.p2align 3
.L54:
	movq	-96(%rbp), %rax
	movq	%r14, %r13
	movl	%r14d, %ecx
	movq	%r12, %rdi
	shrq	$3, %r13
	notl	%ecx
	movzbl	(%rax,%r13), %eax
	andl	$7, %ecx
	sarl	%cl, %eax
	sall	$7, %eax
	movb	%al, -58(%rbp)
	call	EVP_CIPHER_CTX_encrypting@PLT
	movq	%r12, %rdi
	movl	%eax, -76(%rbp)
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	%r12, %rdi
	movq	%rax, -72(%rbp)
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	%r12, %rdi
	movq	%rax, %r15
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movl	-76(%rbp), %esi
	subq	$8, %rsp
	movq	-112(%rbp), %rdi
	movq	%rax, %r8
	movl	$1, %ecx
	movl	$1, %edx
	pushq	%rsi
	movq	-120(%rbp), %rsi
	leaq	128(%r15), %r9
	pushq	%rbx
	movq	-72(%rbp), %rbx
	addq	$256, %rbx
	pushq	%rbx
	call	DES_ede3_cfb_encrypt@PLT
	movq	-88(%rbp), %rax
	movzbl	-57(%rbp), %edx
	movl	%r14d, %ecx
	andl	$7, %ecx
	addq	$1, %r14
	addq	$32, %rsp
	leaq	(%rax,%r13), %rsi
	andl	$-128, %edx
	movl	$128, %eax
	sarl	%cl, %eax
	movzbl	%dl, %edx
	notl	%eax
	sarl	%cl, %edx
	andb	(%rsi), %al
	orl	%edx, %eax
	movb	%al, (%rsi)
	cmpq	%r14, -104(%rbp)
	jne	.L54
.L53:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L61
	leaq	-40(%rbp), %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L61:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE473:
	.size	des_ede3_cfb1_cipher, .-des_ede3_cfb1_cipher
	.p2align 4
	.type	des_ede3_cfb8_cipher, @function
des_ede3_cfb8_cipher:
.LFB474:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	movabsq	$4611686018427387903, %rcx
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r14, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -88(%rbp)
	movq	%rdx, -96(%rbp)
	cmpq	%rcx, %r14
	jbe	.L63
	leaq	(%rsi,%r14), %rax
	movq	%rcx, -64(%rbp)
	movq	%rax, -72(%rbp)
	leaq	(%rdx,%r14), %rax
	movq	%rax, -80(%rbp)
	movq	%r14, -104(%rbp)
.L64:
	movq	%r12, %rdi
	call	EVP_CIPHER_CTX_encrypting@PLT
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movq	%r12, %rdi
	movq	%rax, %r15
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	%r12, %rdi
	movq	%rax, -56(%rbp)
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	subq	$8, %rsp
	movq	-72(%rbp), %rsi
	movq	-80(%rbp), %rdi
	pushq	%rbx
	movq	-56(%rbp), %rbx
	movq	%rax, %r8
	leaq	128(%r13), %r9
	pushq	%r15
	subq	%r14, %rsi
	subq	%r14, %rdi
	movl	$8, %edx
	addq	$256, %rbx
	movabsq	$4611686018427387904, %rcx
	pushq	%rbx
	call	DES_ede3_cfb_encrypt@PLT
	addq	$32, %rsp
	movabsq	$-4611686018427387904, %rax
	addq	%rax, %r14
	cmpq	-64(%rbp), %r14
	ja	.L64
	movq	-104(%rbp), %r14
	movq	-64(%rbp), %r13
	leaq	(%r14,%rax), %r11
	andq	%r14, %r13
	shrq	$62, %r11
	addq	$1, %r11
	salq	$62, %r11
	addq	%r11, -96(%rbp)
	addq	%r11, -88(%rbp)
.L63:
	testq	%r13, %r13
	jne	.L73
.L65:
	leaq	-40(%rbp), %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L73:
	.cfi_restore_state
	movq	%r12, %rdi
	call	EVP_CIPHER_CTX_encrypting@PLT
	movq	%r12, %rdi
	movl	%eax, -56(%rbp)
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	%r12, %rdi
	movq	%rax, %r15
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movl	-56(%rbp), %ecx
	subq	$8, %rsp
	movq	-88(%rbp), %rsi
	movq	-96(%rbp), %rdi
	movq	%rax, %r8
	movl	$8, %edx
	leaq	128(%r15), %r9
	pushq	%rcx
	movq	%r13, %rcx
	pushq	%rbx
	leaq	256(%r14), %rbx
	pushq	%rbx
	call	DES_ede3_cfb_encrypt@PLT
	addq	$32, %rsp
	jmp	.L65
	.cfi_endproc
.LFE474:
	.size	des_ede3_cfb8_cipher, .-des_ede3_cfb8_cipher
	.p2align 4
	.type	des3_ctrl, @function
des3_ctrl:
.LFB487:
	.cfi_startproc
	endbr64
	movl	$-1, %eax
	cmpl	$6, %esi
	je	.L84
	ret
	.p2align 4,,10
	.p2align 3
.L84:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rcx, %rbx
	subq	$16, %rsp
	call	EVP_CIPHER_CTX_key_length@PLT
	movq	%rbx, %rdi
	movl	%eax, %esi
	call	RAND_priv_bytes@PLT
	movl	%eax, %r8d
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jle	.L74
	movq	%rbx, %rdi
	call	DES_set_odd_parity@PLT
	movq	%r12, %rdi
	call	EVP_CIPHER_CTX_key_length@PLT
	cmpl	$15, %eax
	jg	.L85
.L76:
	movq	%r12, %rdi
	call	EVP_CIPHER_CTX_key_length@PLT
	movl	%eax, %r8d
	movl	$1, %eax
	cmpl	$23, %r8d
	jg	.L86
.L74:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L86:
	.cfi_restore_state
	leaq	16(%rbx), %rdi
	movl	%eax, -20(%rbp)
	call	DES_set_odd_parity@PLT
	movl	-20(%rbp), %eax
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L85:
	leaq	8(%rbx), %rdi
	call	DES_set_odd_parity@PLT
	jmp	.L76
	.cfi_endproc
.LFE487:
	.size	des3_ctrl, .-des3_ctrl
	.p2align 4
	.type	des_ede_cbc_cipher.constprop.0, @function
des_ede_cbc_cipher.constprop.0:
.LFB500:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	call	EVP_CIPHER_CTX_get_cipher_data@PLT
	movq	%r15, %rdi
	movq	384(%rax), %rbx
	movq	%rax, %r12
	testq	%rbx, %rbx
	jne	.L91
	call	EVP_CIPHER_CTX_encrypting@PLT
	movq	%r15, %rdi
	movl	%eax, %ebx
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	pushq	%rbx
	movl	$8, %edx
	movq	%r12, %rcx
	pushq	%rax
	leaq	256(%r12), %r9
	movq	%r14, %rsi
	movq	%r13, %rdi
	leaq	128(%r12), %r8
	call	DES_ede3_cbc_encrypt@PLT
	popq	%rax
	popq	%rdx
.L89:
	leaq	-40(%rbp), %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L91:
	.cfi_restore_state
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movq	%r12, %rcx
	movl	$8, %edx
	movq	%r14, %rsi
	movq	%rax, %r8
	movq	%r13, %rdi
	call	*%rbx
	jmp	.L89
	.cfi_endproc
.LFE500:
	.size	des_ede_cbc_cipher.constprop.0, .-des_ede_cbc_cipher.constprop.0
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/evp/e_des3.c"
	.text
	.p2align 4
	.type	des_ede3_wrap_cipher, @function
des_ede3_wrap_cipher:
.LFB492:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movabsq	$4611686018427387903, %rax
	cmpq	%rax, %rcx
	ja	.L107
	movq	%rcx, %rbx
	testb	$7, %cl
	jne	.L107
	movq	%rsi, %r12
	movq	%rdx, %r14
	movq	%rdi, %r13
	movl	%ecx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	is_partially_overlapping@PLT
	testl	%eax, %eax
	jne	.L112
	movq	%r13, %rdi
	call	EVP_CIPHER_CTX_encrypting@PLT
	testl	%eax, %eax
	je	.L95
	testq	%r12, %r12
	je	.L111
	leaq	8(%r12), %r15
	movq	%r14, %rsi
	movq	%rbx, %rdx
	movq	%r15, %rdi
	call	memmove@PLT
	leaq	-80(%rbp), %r8
	movq	%r14, %rdi
	movq	%rbx, %rsi
	movq	%r8, %rdx
	movq	%r8, -104(%rbp)
	leaq	8(%rbx), %r14
	call	SHA1@PLT
	movq	-80(%rbp), %rax
	movq	-104(%rbp), %r8
	movl	$20, %esi
	movq	%rax, 8(%r12,%rbx)
	movq	%r8, %rdi
	call	OPENSSL_cleanse@PLT
	movq	%r13, %rdi
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movl	$8, %esi
	movq	%rax, %rdi
	call	RAND_bytes@PLT
	testl	%eax, %eax
	jle	.L107
	movq	%r13, %rdi
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movq	%r14, %rcx
	movq	%r15, %rdx
	movq	%r15, %rsi
	movq	(%rax), %rax
	movq	%r13, %rdi
	leaq	16(%rbx), %r14
	movq	%rax, (%r12)
	call	des_ede_cbc_cipher
	movq	%r14, %rdx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	BUF_reverse@PLT
	movq	%r13, %rdi
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movq	%r12, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movabsq	$369832251558649162, %rcx
	movq	%rcx, (%rax)
	movq	%r14, %rcx
	call	des_ede_cbc_cipher
.L111:
	leal	16(%rbx), %eax
.L92:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L113
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L95:
	.cfi_restore_state
	cmpq	$23, %rbx
	jbe	.L107
	testq	%r12, %r12
	je	.L103
	movq	%r13, %rdi
	leaq	-96(%rbp), %r15
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	movabsq	$369832251558649162, %rcx
	movq	%rcx, (%rax)
	call	des_ede_cbc_cipher.constprop.0
	cmpq	%r14, %r12
	je	.L100
	leaq	8(%r14), %rdx
.L101:
	leaq	-16(%rbx), %rax
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %rcx
	movq	%rax, -104(%rbp)
	call	des_ede_cbc_cipher
	leaq	-8(%rbx), %rdx
	movq	%r13, %rdi
	addq	%r14, %rdx
	leaq	-88(%rbp), %r14
	movq	%r14, %rsi
	call	des_ede_cbc_cipher.constprop.0
	xorl	%esi, %esi
	movl	$8, %edx
	movq	%r15, %rdi
	call	BUF_reverse@PLT
	movq	-104(%rbp), %rdx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	BUF_reverse@PLT
	movq	%r13, %rdi
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movl	$8, %edx
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	BUF_reverse@PLT
	movq	-104(%rbp), %rcx
	movq	%r12, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	des_ede_cbc_cipher
	movq	%r15, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	des_ede_cbc_cipher.constprop.0
	leaq	-80(%rbp), %r9
	movq	-104(%rbp), %rsi
	movq	%r12, %rdi
	movq	%r9, %rdx
	movq	%r9, -112(%rbp)
	call	SHA1@PLT
	movq	-112(%rbp), %r9
	movl	$8, %edx
	movq	%r15, %rsi
	movq	%r9, %rdi
	call	CRYPTO_memcmp@PLT
	movq	-112(%rbp), %r9
	testl	%eax, %eax
	je	.L114
	movl	$8, %esi
	movq	%r15, %rdi
	movq	%r9, -112(%rbp)
	call	OPENSSL_cleanse@PLT
	movq	-112(%rbp), %r9
	movl	$20, %esi
	movq	%r9, %rdi
	call	OPENSSL_cleanse@PLT
	movl	$8, %esi
	movq	%r14, %rdi
	call	OPENSSL_cleanse@PLT
	movq	%r13, %rdi
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movl	$8, %esi
	movq	%rax, %rdi
	call	OPENSSL_cleanse@PLT
.L104:
	movq	-104(%rbp), %rsi
	movq	%r12, %rdi
	call	OPENSSL_cleanse@PLT
	movl	$-1, %eax
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L112:
	movl	$398, %r8d
	movl	$162, %edx
	movl	$171, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L114:
	movl	$8, %esi
	movq	%r15, %rdi
	call	OPENSSL_cleanse@PLT
	movq	-112(%rbp), %r9
	movl	$20, %esi
	movq	%r9, %rdi
	call	OPENSSL_cleanse@PLT
	movl	$8, %esi
	movq	%r14, %rdi
	call	OPENSSL_cleanse@PLT
	movq	%r13, %rdi
	call	EVP_CIPHER_CTX_iv_noconst@PLT
	movl	$8, %esi
	movq	%rax, %rdi
	call	OPENSSL_cleanse@PLT
	cmpl	$15, %ebx
	je	.L104
.L103:
	leal	-16(%rbx), %eax
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L100:
	leaq	-8(%rbx), %rdx
	leaq	8(%r12), %rsi
	movq	%r12, %rdi
	call	memmove@PLT
	leaq	-8(%r12), %r14
	movq	%r12, %rdx
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L107:
	movl	$-1, %eax
	jmp	.L92
.L113:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE492:
	.size	des_ede3_wrap_cipher, .-des_ede3_wrap_cipher
	.p2align 4
	.globl	EVP_des_ede_cbc
	.type	EVP_des_ede_cbc, @function
EVP_des_ede_cbc:
.LFB475:
	.cfi_startproc
	endbr64
	leaq	des_ede_cbc(%rip), %rax
	ret
	.cfi_endproc
.LFE475:
	.size	EVP_des_ede_cbc, .-EVP_des_ede_cbc
	.p2align 4
	.globl	EVP_des_ede_cfb64
	.type	EVP_des_ede_cfb64, @function
EVP_des_ede_cfb64:
.LFB476:
	.cfi_startproc
	endbr64
	leaq	des_ede_cfb64(%rip), %rax
	ret
	.cfi_endproc
.LFE476:
	.size	EVP_des_ede_cfb64, .-EVP_des_ede_cfb64
	.p2align 4
	.globl	EVP_des_ede_ofb
	.type	EVP_des_ede_ofb, @function
EVP_des_ede_ofb:
.LFB477:
	.cfi_startproc
	endbr64
	leaq	des_ede_ofb(%rip), %rax
	ret
	.cfi_endproc
.LFE477:
	.size	EVP_des_ede_ofb, .-EVP_des_ede_ofb
	.p2align 4
	.globl	EVP_des_ede_ecb
	.type	EVP_des_ede_ecb, @function
EVP_des_ede_ecb:
.LFB497:
	.cfi_startproc
	endbr64
	leaq	des_ede_ecb(%rip), %rax
	ret
	.cfi_endproc
.LFE497:
	.size	EVP_des_ede_ecb, .-EVP_des_ede_ecb
	.p2align 4
	.globl	EVP_des_ede3_cbc
	.type	EVP_des_ede3_cbc, @function
EVP_des_ede3_cbc:
.LFB479:
	.cfi_startproc
	endbr64
	leaq	des_ede3_cbc(%rip), %rax
	ret
	.cfi_endproc
.LFE479:
	.size	EVP_des_ede3_cbc, .-EVP_des_ede3_cbc
	.p2align 4
	.globl	EVP_des_ede3_cfb64
	.type	EVP_des_ede3_cfb64, @function
EVP_des_ede3_cfb64:
.LFB480:
	.cfi_startproc
	endbr64
	leaq	des_ede3_cfb64(%rip), %rax
	ret
	.cfi_endproc
.LFE480:
	.size	EVP_des_ede3_cfb64, .-EVP_des_ede3_cfb64
	.p2align 4
	.globl	EVP_des_ede3_ofb
	.type	EVP_des_ede3_ofb, @function
EVP_des_ede3_ofb:
.LFB481:
	.cfi_startproc
	endbr64
	leaq	des_ede3_ofb(%rip), %rax
	ret
	.cfi_endproc
.LFE481:
	.size	EVP_des_ede3_ofb, .-EVP_des_ede3_ofb
	.p2align 4
	.globl	EVP_des_ede3_ecb
	.type	EVP_des_ede3_ecb, @function
EVP_des_ede3_ecb:
.LFB499:
	.cfi_startproc
	endbr64
	leaq	des_ede3_ecb(%rip), %rax
	ret
	.cfi_endproc
.LFE499:
	.size	EVP_des_ede3_ecb, .-EVP_des_ede3_ecb
	.p2align 4
	.globl	EVP_des_ede3_cfb1
	.type	EVP_des_ede3_cfb1, @function
EVP_des_ede3_cfb1:
.LFB483:
	.cfi_startproc
	endbr64
	leaq	des_ede3_cfb1(%rip), %rax
	ret
	.cfi_endproc
.LFE483:
	.size	EVP_des_ede3_cfb1, .-EVP_des_ede3_cfb1
	.p2align 4
	.globl	EVP_des_ede3_cfb8
	.type	EVP_des_ede3_cfb8, @function
EVP_des_ede3_cfb8:
.LFB484:
	.cfi_startproc
	endbr64
	leaq	des_ede3_cfb8(%rip), %rax
	ret
	.cfi_endproc
.LFE484:
	.size	EVP_des_ede3_cfb8, .-EVP_des_ede3_cfb8
	.p2align 4
	.globl	EVP_des_ede
	.type	EVP_des_ede, @function
EVP_des_ede:
.LFB488:
	.cfi_startproc
	endbr64
	leaq	des_ede_ecb(%rip), %rax
	ret
	.cfi_endproc
.LFE488:
	.size	EVP_des_ede, .-EVP_des_ede
	.p2align 4
	.globl	EVP_des_ede3
	.type	EVP_des_ede3, @function
EVP_des_ede3:
.LFB489:
	.cfi_startproc
	endbr64
	leaq	des_ede3_ecb(%rip), %rax
	ret
	.cfi_endproc
.LFE489:
	.size	EVP_des_ede3, .-EVP_des_ede3
	.p2align 4
	.globl	EVP_des_ede3_wrap
	.type	EVP_des_ede3_wrap, @function
EVP_des_ede3_wrap:
.LFB493:
	.cfi_startproc
	endbr64
	leaq	des3_wrap(%rip), %rax
	ret
	.cfi_endproc
.LFE493:
	.size	EVP_des_ede3_wrap, .-EVP_des_ede3_wrap
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	des3_wrap, @object
	.size	des3_wrap, 88
des3_wrap:
	.long	246
	.long	8
	.long	24
	.long	0
	.quad	1118226
	.quad	des_ede3_init_key
	.quad	des_ede3_wrap_cipher
	.quad	0
	.long	392
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.align 32
	.type	des_ede3_cfb8, @object
	.size	des_ede3_cfb8, 88
des_ede3_cfb8:
	.long	659
	.long	1
	.long	24
	.long	8
	.quad	4611
	.quad	des_ede3_init_key
	.quad	des_ede3_cfb8_cipher
	.quad	0
	.long	392
	.zero	4
	.quad	0
	.quad	0
	.quad	des3_ctrl
	.quad	0
	.align 32
	.type	des_ede3_cfb1, @object
	.size	des_ede3_cfb1, 88
des_ede3_cfb1:
	.long	658
	.long	1
	.long	24
	.long	8
	.quad	4611
	.quad	des_ede3_init_key
	.quad	des_ede3_cfb1_cipher
	.quad	0
	.long	392
	.zero	4
	.quad	0
	.quad	0
	.quad	des3_ctrl
	.quad	0
	.align 32
	.type	des_ede3_ecb, @object
	.size	des_ede3_ecb, 88
des_ede3_ecb:
	.long	33
	.long	8
	.long	24
	.long	0
	.quad	4609
	.quad	des_ede3_init_key
	.quad	des_ede_ecb_cipher
	.quad	0
	.long	392
	.zero	4
	.quad	0
	.quad	0
	.quad	des3_ctrl
	.quad	0
	.align 32
	.type	des_ede3_ofb, @object
	.size	des_ede3_ofb, 88
des_ede3_ofb:
	.long	63
	.long	1
	.long	24
	.long	8
	.quad	4612
	.quad	des_ede3_init_key
	.quad	des_ede_ofb_cipher
	.quad	0
	.long	392
	.zero	4
	.quad	0
	.quad	0
	.quad	des3_ctrl
	.quad	0
	.align 32
	.type	des_ede3_cfb64, @object
	.size	des_ede3_cfb64, 88
des_ede3_cfb64:
	.long	61
	.long	1
	.long	24
	.long	8
	.quad	4611
	.quad	des_ede3_init_key
	.quad	des_ede_cfb64_cipher
	.quad	0
	.long	392
	.zero	4
	.quad	0
	.quad	0
	.quad	des3_ctrl
	.quad	0
	.align 32
	.type	des_ede3_cbc, @object
	.size	des_ede3_cbc, 88
des_ede3_cbc:
	.long	44
	.long	8
	.long	24
	.long	8
	.quad	4610
	.quad	des_ede3_init_key
	.quad	des_ede_cbc_cipher
	.quad	0
	.long	392
	.zero	4
	.quad	0
	.quad	0
	.quad	des3_ctrl
	.quad	0
	.align 32
	.type	des_ede_ecb, @object
	.size	des_ede_ecb, 88
des_ede_ecb:
	.long	32
	.long	8
	.long	16
	.long	0
	.quad	4609
	.quad	des_ede_init_key
	.quad	des_ede_ecb_cipher
	.quad	0
	.long	392
	.zero	4
	.quad	0
	.quad	0
	.quad	des3_ctrl
	.quad	0
	.align 32
	.type	des_ede_ofb, @object
	.size	des_ede_ofb, 88
des_ede_ofb:
	.long	62
	.long	1
	.long	16
	.long	8
	.quad	4612
	.quad	des_ede_init_key
	.quad	des_ede_ofb_cipher
	.quad	0
	.long	392
	.zero	4
	.quad	0
	.quad	0
	.quad	des3_ctrl
	.quad	0
	.align 32
	.type	des_ede_cfb64, @object
	.size	des_ede_cfb64, 88
des_ede_cfb64:
	.long	60
	.long	1
	.long	16
	.long	8
	.quad	4611
	.quad	des_ede_init_key
	.quad	des_ede_cfb64_cipher
	.quad	0
	.long	392
	.zero	4
	.quad	0
	.quad	0
	.quad	des3_ctrl
	.quad	0
	.align 32
	.type	des_ede_cbc, @object
	.size	des_ede_cbc, 88
des_ede_cbc:
	.long	43
	.long	8
	.long	16
	.long	8
	.quad	4610
	.quad	des_ede_init_key
	.quad	des_ede_cbc_cipher
	.quad	0
	.long	392
	.zero	4
	.quad	0
	.quad	0
	.quad	des3_ctrl
	.quad	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
