	.file	"v3_ncons.c"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"permitted"
.LC1:
	.string	"excluded"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"../deps/openssl/openssl/crypto/x509v3/v3_ncons.c"
	.text
	.p2align 4
	.type	v2i_NAME_CONSTRAINTS, @function
v2i_NAME_CONSTRAINTS:
.LFB1352:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	%rdi, -88(%rbp)
	leaq	NAME_CONSTRAINTS_it(%rip), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	ASN1_item_new@PLT
	movq	%rax, -104(%rbp)
	testq	%rax, %rax
	je	.L2
	addq	$8, %rax
	xorl	%ebx, %ebx
	movq	%rax, -96(%rbp)
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L34:
	cmpb	$0, 9(%rdx)
	je	.L4
	addq	$10, %rdx
	movq	-104(%rbp), %r13
	movq	%rdx, -72(%rbp)
.L5:
	movq	16(%rax), %rax
	leaq	GENERAL_SUBTREE_it(%rip), %rdi
	movq	%rax, -64(%rbp)
	call	ASN1_item_new@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2
	movq	-88(%rbp), %rsi
	movq	(%rax), %rdi
	leaq	-80(%rbp), %rcx
	movq	%r15, %rdx
	movl	$1, %r8d
	call	v2i_GENERAL_NAME_ex@PLT
	testq	%rax, %rax
	je	.L7
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L33
.L8:
	movq	%r14, %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L9
	addl	$1, %ebx
.L3:
	movq	%r12, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jge	.L1
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	OPENSSL_sk_value@PLT
	movl	$9, %ecx
	leaq	.LC0(%rip), %rdi
	movq	8(%rax), %rdx
	movq	%rdx, %rsi
	repz cmpsb
	seta	%cl
	sbbb	$0, %cl
	testb	%cl, %cl
	je	.L34
.L4:
	movl	$8, %ecx
	movq	%rdx, %rsi
	leaq	.LC1(%rip), %rdi
	repz cmpsb
	seta	%cl
	sbbb	$0, %cl
	testb	%cl, %cl
	jne	.L6
	cmpb	$0, 8(%rdx)
	je	.L6
	addq	$9, %rdx
	movq	-96(%rbp), %r13
	movq	%rdx, -72(%rbp)
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L33:
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, 0(%r13)
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L8
.L9:
	movl	$146, %r8d
	movl	$65, %edx
	movl	$147, %esi
	movl	$34, %edi
	leaq	.LC2(%rip), %rcx
	call	ERR_put_error@PLT
.L7:
	movq	-104(%rbp), %rdi
	leaq	NAME_CONSTRAINTS_it(%rip), %rsi
	call	ASN1_item_free@PLT
	leaq	GENERAL_SUBTREE_it(%rip), %rsi
	movq	%r14, %rdi
	call	ASN1_item_free@PLT
	movq	$0, -104(%rbp)
.L1:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L35
	movq	-104(%rbp), %rax
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	.cfi_restore_state
	xorl	%r14d, %r14d
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L6:
	movl	$127, %r8d
	leaq	.LC2(%rip), %rcx
	movl	$143, %edx
	xorl	%r14d, %r14d
	movl	$147, %esi
	movl	$34, %edi
	call	ERR_put_error@PLT
	jmp	.L7
.L35:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1352:
	.size	v2i_NAME_CONSTRAINTS, .-v2i_NAME_CONSTRAINTS
	.section	.rodata.str1.1
.LC3:
	.string	""
.LC4:
	.string	"%*s%s:\n"
.LC5:
	.string	"%*s"
.LC6:
	.string	"IP:"
.LC7:
	.string	"%d.%d.%d.%d/%d.%d.%d.%d"
.LC8:
	.string	"%X"
.LC9:
	.string	"/"
.LC10:
	.string	":"
.LC11:
	.string	"IP Address:<invalid>"
.LC12:
	.string	"\n"
	.text
	.p2align 4
	.type	do_i2r_name_constraints.isra.0, @function
do_i2r_name_constraints.isra.0:
.LFB1370:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movl	%edx, -52(%rbp)
	call	OPENSSL_sk_num@PLT
	testl	%eax, %eax
	jle	.L37
	movq	%r12, %r8
	leaq	.LC3(%rip), %rcx
	movl	%r14d, %edx
	movq	%rbx, %rdi
	leaq	.LC4(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
.L37:
	xorl	%r12d, %r12d
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L39:
	movq	%rbx, %rdi
	call	GENERAL_NAME_print@PLT
.L41:
	leaq	.LC12(%rip), %rsi
	movq	%rbx, %rdi
	addl	$1, %r12d
	call	BIO_puts@PLT
.L38:
	movq	%r13, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r12d
	jge	.L50
	movl	%r12d, %esi
	movq	%r13, %rdi
	call	OPENSSL_sk_value@PLT
	leaq	.LC5(%rip), %rsi
	leaq	.LC3(%rip), %rcx
	movq	%rbx, %rdi
	movq	%rax, %r14
	movl	-52(%rbp), %eax
	leal	2(%rax), %edx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	(%r14), %rsi
	cmpl	$7, (%rsi)
	jne	.L39
	movq	8(%rsi), %rax
	movq	%rbx, %rdi
	leaq	.LC6(%rip), %rsi
	movl	(%rax), %r15d
	movq	8(%rax), %r14
	call	BIO_puts@PLT
	cmpl	$8, %r15d
	je	.L51
	cmpl	$32, %r15d
	jne	.L42
	xorl	%r15d, %r15d
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L43:
	cmpl	$15, %r15d
	je	.L41
	leaq	.LC10(%rip), %rsi
	movq	%rbx, %rdi
	call	BIO_puts@PLT
	cmpl	$15, %r15d
	je	.L41
.L44:
	addq	$1, %r15
.L47:
	movzwl	(%r14,%r15,2), %edx
	xorl	%eax, %eax
	leaq	.LC8(%rip), %rsi
	movq	%rbx, %rdi
	rolw	$8, %dx
	movzwl	%dx, %edx
	call	BIO_printf@PLT
	cmpq	$7, %r15
	jne	.L43
	leaq	.LC9(%rip), %rsi
	movq	%rbx, %rdi
	call	BIO_puts@PLT
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L42:
	leaq	.LC11(%rip), %rsi
	movq	%rbx, %rdi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L50:
	leaq	-40(%rbp), %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L51:
	.cfi_restore_state
	movzbl	7(%r14), %eax
	movzbl	3(%r14), %r9d
	leaq	.LC7(%rip), %rsi
	movq	%rbx, %rdi
	movzbl	2(%r14), %r8d
	movzbl	1(%r14), %ecx
	movzbl	(%r14), %edx
	pushq	%rax
	movzbl	6(%r14), %eax
	pushq	%rax
	movzbl	5(%r14), %eax
	pushq	%rax
	movzbl	4(%r14), %eax
	pushq	%rax
	xorl	%eax, %eax
	call	BIO_printf@PLT
	addq	$32, %rsp
	jmp	.L41
	.cfi_endproc
.LFE1370:
	.size	do_i2r_name_constraints.isra.0, .-do_i2r_name_constraints.isra.0
	.section	.rodata.str1.1
.LC13:
	.string	"Permitted"
.LC14:
	.string	"Excluded"
	.text
	.p2align 4
	.type	i2r_NAME_CONSTRAINTS, @function
i2r_NAME_CONSTRAINTS:
.LFB1353:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%ecx, %r13d
	leaq	.LC13(%rip), %rcx
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	movl	%r13d, %edx
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	movq	%r12, %rsi
	subq	$8, %rsp
	movq	(%rbx), %rdi
	call	do_i2r_name_constraints.isra.0
	movq	8(%rbx), %rdi
	movl	%r13d, %edx
	movq	%r12, %rsi
	leaq	.LC14(%rip), %rcx
	call	do_i2r_name_constraints.isra.0
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1353:
	.size	i2r_NAME_CONSTRAINTS, .-i2r_NAME_CONSTRAINTS
	.p2align 4
	.type	nc_match_single.isra.0, @function
nc_match_single.isra.0:
.LFB1373:
	.cfi_startproc
	cmpl	$7, %esi
	ja	.L154
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.L57(%rip), %rcx
	movl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$24, %rsp
	movslq	(%rcx,%rsi,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L57:
	.long	.L105-.L57
	.long	.L61-.L57
	.long	.L60-.L57
	.long	.L105-.L57
	.long	.L59-.L57
	.long	.L105-.L57
	.long	.L58-.L57
	.long	.L56-.L57
	.text
	.p2align 4,,10
	.p2align 3
.L56:
	movq	(%rdi), %rsi
	movl	(%rsi), %edx
	cmpl	$4, %edx
	je	.L114
	movl	$53, %eax
	cmpl	$16, %edx
	jne	.L54
.L114:
	movq	(%rbx), %r8
	movl	(%r8), %ecx
	cmpl	$8, %ecx
	je	.L115
	movl	$53, %eax
	cmpl	$32, %ecx
	jne	.L54
.L115:
	leal	(%rdx,%rdx), %eax
	cmpl	%eax, %ecx
	jne	.L66
	movslq	%edx, %r9
	testl	%edx, %edx
	jle	.L69
	movq	8(%r8), %rcx
	movq	8(%rsi), %rdi
	leal	-1(%rdx), %r8d
	xorl	%eax, %eax
	leaq	(%rcx,%r9), %rsi
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L160:
	leaq	1(%rax), %rdx
	cmpq	%rax, %r8
	je	.L69
	movq	%rdx, %rax
.L104:
	movzbl	(%rdi,%rax), %edx
	xorb	(%rcx,%rax), %dl
	testb	%dl, (%rsi,%rax)
	je	.L160
.L66:
	addq	$24, %rsp
	movl	$47, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L61:
	.cfi_restore_state
	movq	(%rdx), %r15
	movq	(%rdi), %rdx
	movl	$64, %esi
	movq	8(%r15), %r13
	movq	8(%rdx), %r14
	movq	%rdx, -56(%rbp)
	movq	%r13, %rdi
	call	strchr@PLT
	movl	$64, %esi
	movq	%r14, %rdi
	movq	%rax, %r12
	call	strchr@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L106
	testq	%r12, %r12
	movq	-56(%rbp), %rdx
	je	.L161
	cmpq	%r12, %r13
	je	.L83
	movq	%r12, %rdx
	subq	%r14, %rax
	subq	%r13, %rdx
	cmpq	%rax, %rdx
	jne	.L66
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	strncmp@PLT
	testl	%eax, %eax
	jne	.L66
.L83:
	leaq	1(%r12), %r13
.L77:
	xorl	%ecx, %ecx
.L88:
	movzbl	0(%r13,%rcx), %eax
	movzbl	1(%rbx,%rcx), %edx
	cmpb	%dl, %al
	je	.L84
	leal	-65(%rax), %edi
	leal	32(%rax), %esi
	cmpb	$26, %dil
	leal	-65(%rdx), %edi
	cmovb	%esi, %eax
	leal	32(%rdx), %esi
	cmpb	$26, %dil
	cmovb	%esi, %edx
	cmpb	%dl, %al
	jne	.L66
.L87:
	addq	$1, %rcx
	cmpq	$-1, %rcx
	jne	.L88
	.p2align 4,,10
	.p2align 3
.L69:
	xorl	%eax, %eax
.L162:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L60:
	.cfi_restore_state
	movq	(%rdx), %rdx
	movq	8(%rdx), %r8
	movzbl	(%r8), %ecx
	testb	%cl, %cl
	je	.L69
	movq	(%rdi), %rsi
	movl	(%rdx), %edx
	movl	(%rsi), %eax
	movq	8(%rsi), %rdi
	cmpl	%edx, %eax
	jle	.L70
	subl	%edx, %eax
	cltq
	addq	%rax, %rdi
	cmpb	$46, %cl
	je	.L70
	cmpb	$46, -1(%rdi)
	jne	.L66
.L70:
	xorl	%ecx, %ecx
.L75:
	movzbl	(%r8,%rcx), %eax
	movzbl	(%rdi,%rcx), %edx
	cmpb	%dl, %al
	je	.L71
	leal	-65(%rax), %r9d
	leal	32(%rax), %esi
	cmpb	$26, %r9b
	leal	-65(%rdx), %r9d
	cmovb	%esi, %eax
	leal	32(%rdx), %esi
	cmpb	$26, %r9b
	cmovb	%esi, %edx
	cmpb	%dl, %al
	jne	.L66
.L74:
	addq	$1, %rcx
	cmpq	$-1, %rcx
	jne	.L75
	xorl	%eax, %eax
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L59:
	movq	(%rdi), %rbx
	movq	(%rdx), %r12
	movl	8(%rbx), %edx
	testl	%edx, %edx
	jne	.L62
.L65:
	movl	8(%r12), %eax
	testl	%eax, %eax
	jne	.L163
.L64:
	movslq	32(%r12), %rdx
	cmpl	32(%rbx), %edx
	jg	.L66
	movq	24(%rbx), %rsi
	movq	24(%r12), %rdi
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L66
	xorl	%eax, %eax
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L58:
	movq	(%rdi), %rax
	movl	$58, %esi
	movq	8(%rax), %rdi
	call	strchr@PLT
	movq	%rax, %rdx
	movl	$53, %eax
	testq	%rdx, %rdx
	je	.L54
	cmpb	$47, 1(%rdx)
	jne	.L54
	cmpb	$47, 2(%rdx)
	jne	.L54
	leaq	3(%rdx), %r12
	movl	$58, %esi
	movq	%r12, %rdi
	movq	%r12, %r13
	call	strchr@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L164
.L89:
	subl	%r12d, %edx
.L90:
	movl	$53, %eax
	testl	%edx, %edx
	je	.L54
	movq	(%rbx), %rax
	movslq	(%rax), %rcx
	movq	8(%rax), %rax
	cmpb	$46, (%rax)
	je	.L165
	cmpl	%edx, %ecx
	jne	.L66
	movslq	%edx, %rdx
	addq	%r12, %rdx
.L101:
	movzbl	0(%r13), %ecx
	movzbl	(%rax), %esi
	cmpb	%sil, %cl
	je	.L97
	leal	-65(%rcx), %r8d
	leal	32(%rcx), %edi
	cmpb	$26, %r8b
	leal	-65(%rsi), %r8d
	cmovb	%edi, %ecx
	leal	32(%rsi), %edi
	cmpb	$26, %r8b
	cmovb	%edi, %esi
	cmpb	%sil, %cl
	jne	.L66
.L100:
	addq	$1, %r13
	addq	$1, %rax
	cmpq	%rdx, %r13
	jne	.L101
	xorl	%eax, %eax
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L105:
	movl	$51, %eax
.L54:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L84:
	.cfi_restore_state
	testb	%al, %al
	jne	.L87
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L71:
	testb	%al, %al
	jne	.L74
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L62:
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	i2d_X509_NAME@PLT
	testl	%eax, %eax
	jns	.L65
.L68:
	movl	$17, %eax
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L163:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	i2d_X509_NAME@PLT
	testl	%eax, %eax
	jns	.L64
	jmp	.L68
	.p2align 4,,10
	.p2align 3
.L164:
	movl	$47, %esi
	movq	%r12, %rdi
	call	strchr@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	jne	.L89
	movq	%r12, %rdi
	call	strlen@PLT
	movl	%eax, %edx
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L161:
	cmpb	$46, 0(%r13)
	jne	.L77
	movl	(%rdx), %eax
	movl	(%r15), %edx
	cmpl	%edx, %eax
	jle	.L66
	subl	%edx, %eax
	xorl	%ecx, %ecx
	cltq
	addq	%rax, %r14
.L82:
	movzbl	0(%r13,%rcx), %eax
	movzbl	(%r14,%rcx), %edx
	cmpb	%dl, %al
	je	.L78
	leal	-65(%rax), %edi
	leal	32(%rax), %esi
	cmpb	$26, %dil
	leal	-65(%rdx), %edi
	cmovnb	%eax, %esi
	leal	32(%rdx), %eax
	cmpb	$26, %dil
	cmovb	%eax, %edx
	cmpb	%dl, %sil
	jne	.L66
.L81:
	addq	$1, %rcx
	cmpq	$-1, %rcx
	jne	.L82
	xorl	%eax, %eax
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L154:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	movl	$51, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L106:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movl	$53, %eax
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L165:
	cmpl	%edx, %ecx
	jge	.L66
	movslq	%edx, %rdx
	subq	%rcx, %rdx
	addq	%r12, %rdx
	testq	%rcx, %rcx
	je	.L69
	addq	%rdx, %rcx
.L96:
	movzbl	(%rdx), %esi
	movzbl	(%rax), %edi
	cmpb	%dil, %sil
	je	.L92
	leal	-65(%rsi), %r9d
	leal	32(%rsi), %r8d
	cmpb	$26, %r9b
	leal	-65(%rdi), %r9d
	cmovnb	%esi, %r8d
	leal	32(%rdi), %esi
	cmpb	$26, %r9b
	cmovb	%esi, %edi
	cmpb	%dil, %r8b
	jne	.L66
.L95:
	addq	$1, %rdx
	addq	$1, %rax
	cmpq	%rcx, %rdx
	jne	.L96
	xorl	%eax, %eax
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L97:
	testb	%cl, %cl
	jne	.L100
	xorl	%eax, %eax
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L92:
	testb	%sil, %sil
	jne	.L95
	xorl	%eax, %eax
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L78:
	testb	%al, %al
	jne	.L81
	xorl	%eax, %eax
	jmp	.L162
	.cfi_endproc
.LFE1373:
	.size	nc_match_single.isra.0, .-nc_match_single.isra.0
	.p2align 4
	.type	nc_match.isra.0, @function
nc_match.isra.0:
.LFB1374:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	8(%rdi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	xorl	%r14d, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	xorl	%ebx, %ebx
	subq	$24, %rsp
	movq	%rax, -56(%rbp)
	jmp	.L167
	.p2align 4,,10
	.p2align 3
.L186:
	cmpl	$47, %eax
	jne	.L166
	movl	$1, %r14d
	.p2align 4,,10
	.p2align 3
.L168:
	addl	$1, %ebx
.L167:
	movq	(%r12), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jge	.L185
	movq	(%r12), %rdi
	movl	%ebx, %esi
	call	OPENSSL_sk_value@PLT
	movq	(%rax), %rdx
	movl	(%rdx), %esi
	cmpl	%esi, 0(%r13)
	jne	.L168
	cmpq	$0, 8(%rax)
	jne	.L171
	cmpq	$0, 16(%rax)
	jne	.L171
	cmpl	$2, %r14d
	je	.L173
	movq	-56(%rbp), %rdi
	addq	$8, %rdx
	call	nc_match_single.isra.0
	testl	%eax, %eax
	jne	.L186
.L173:
	movl	$2, %r14d
	jmp	.L168
.L180:
	movl	$48, %eax
.L166:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L171:
	.cfi_restore_state
	addq	$24, %rsp
	movl	$49, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L185:
	.cfi_restore_state
	cmpl	$1, %r14d
	je	.L179
	xorl	%ebx, %ebx
	leaq	8(%r13), %r12
	jmp	.L175
	.p2align 4,,10
	.p2align 3
.L178:
	movq	(%r15), %rdi
	movl	%ebx, %esi
	call	OPENSSL_sk_value@PLT
	movq	(%rax), %rdx
	movl	(%rdx), %esi
	cmpl	%esi, 0(%r13)
	jne	.L177
	cmpq	$0, 8(%rax)
	jne	.L171
	cmpq	$0, 16(%rax)
	jne	.L171
	addq	$8, %rdx
	movq	%r12, %rdi
	call	nc_match_single.isra.0
	testl	%eax, %eax
	je	.L180
	cmpl	$47, %eax
	jne	.L166
.L177:
	addl	$1, %ebx
.L175:
	movq	(%r15), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jl	.L178
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L179:
	.cfi_restore_state
	movl	$47, %eax
	jmp	.L166
	.cfi_endproc
.LFE1374:
	.size	nc_match.isra.0, .-nc_match.isra.0
	.p2align 4
	.globl	GENERAL_SUBTREE_new
	.type	GENERAL_SUBTREE_new, @function
GENERAL_SUBTREE_new:
.LFB1346:
	.cfi_startproc
	endbr64
	leaq	GENERAL_SUBTREE_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE1346:
	.size	GENERAL_SUBTREE_new, .-GENERAL_SUBTREE_new
	.p2align 4
	.globl	GENERAL_SUBTREE_free
	.type	GENERAL_SUBTREE_free, @function
GENERAL_SUBTREE_free:
.LFB1347:
	.cfi_startproc
	endbr64
	leaq	GENERAL_SUBTREE_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE1347:
	.size	GENERAL_SUBTREE_free, .-GENERAL_SUBTREE_free
	.p2align 4
	.globl	NAME_CONSTRAINTS_new
	.type	NAME_CONSTRAINTS_new, @function
NAME_CONSTRAINTS_new:
.LFB1348:
	.cfi_startproc
	endbr64
	leaq	NAME_CONSTRAINTS_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE1348:
	.size	NAME_CONSTRAINTS_new, .-NAME_CONSTRAINTS_new
	.p2align 4
	.globl	NAME_CONSTRAINTS_free
	.type	NAME_CONSTRAINTS_free, @function
NAME_CONSTRAINTS_free:
.LFB1349:
	.cfi_startproc
	endbr64
	leaq	NAME_CONSTRAINTS_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE1349:
	.size	NAME_CONSTRAINTS_free, .-NAME_CONSTRAINTS_free
	.p2align 4
	.globl	NAME_CONSTRAINTS_check
	.type	NAME_CONSTRAINTS_check, @function
NAME_CONSTRAINTS_check:
.LFB1357:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	X509_get_subject_name@PLT
	movq	272(%rbx), %rdi
	movq	%rax, %r13
	call	OPENSSL_sk_num@PLT
	movq	%r13, %rdi
	movl	%eax, %r14d
	call	X509_NAME_entry_count@PLT
	xorl	%ecx, %ecx
	movl	$2147483647, %r8d
	testl	%eax, %eax
	cmovs	%ecx, %eax
	testl	%r14d, %r14d
	cmovs	%ecx, %r14d
	movl	%eax, %r15d
	movl	%r8d, %eax
	subl	%r14d, %eax
	cmpl	%eax, %r15d
	jg	.L194
	movq	8(%r12), %rdi
	call	OPENSSL_sk_num@PLT
	movq	(%r12), %rdi
	movl	%eax, -88(%rbp)
	call	OPENSSL_sk_num@PLT
	movl	-88(%rbp), %edx
	movl	$0, %ecx
	movl	$2147483647, %r8d
	testl	%eax, %eax
	cmovs	%ecx, %eax
	testl	%edx, %edx
	cmovns	%edx, %ecx
	movl	%eax, %esi
	movl	%r8d, %eax
	subl	%ecx, %eax
	cmpl	%eax, %esi
	jg	.L194
	addl	%r15d, %r14d
	je	.L195
	movl	$1048576, %eax
	addl	%esi, %ecx
	cltd
	idivl	%r14d
	cmpl	%ecx, %eax
	jl	.L194
.L195:
	movq	%r13, %rdi
	call	X509_NAME_entry_count@PLT
	testl	%eax, %eax
	jg	.L211
.L196:
	xorl	%r14d, %r14d
	leaq	8(%r12), %r13
	jmp	.L200
	.p2align 4,,10
	.p2align 3
.L201:
	movq	272(%rbx), %rdi
	movl	%r14d, %esi
	call	OPENSSL_sk_value@PLT
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	nc_match.isra.0
	testl	%eax, %eax
	jne	.L191
	addl	$1, %r14d
.L200:
	movq	272(%rbx), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r14d
	jl	.L201
	xorl	%eax, %eax
	jmp	.L191
	.p2align 4,,10
	.p2align 3
.L194:
	movl	$1, %eax
.L191:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L212
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L211:
	.cfi_restore_state
	leaq	8(%r12), %rax
	leaq	-80(%rbp), %r15
	movq	%r12, %rsi
	movq	%r13, -72(%rbp)
	movq	%rax, %rdx
	movq	%r15, %rdi
	movl	$4, -80(%rbp)
	movq	%rax, -88(%rbp)
	call	nc_match.isra.0
	testl	%eax, %eax
	jne	.L191
	movl	$1, -80(%rbp)
	movl	$-1, %r14d
	jmp	.L199
	.p2align 4,,10
	.p2align 3
.L213:
	movl	%eax, %esi
	movq	%r13, %rdi
	call	X509_NAME_get_entry@PLT
	movq	%rax, %rdi
	call	X509_NAME_ENTRY_get_data@PLT
	cmpl	$22, 4(%rax)
	movq	%rax, -72(%rbp)
	jne	.L203
	movq	-88(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	nc_match.isra.0
	testl	%eax, %eax
	jne	.L191
.L199:
	movl	%r14d, %edx
	movl	$48, %esi
	movq	%r13, %rdi
	call	X509_NAME_get_index_by_NID@PLT
	movl	%eax, %r14d
	cmpl	$-1, %eax
	jne	.L213
	jmp	.L196
	.p2align 4,,10
	.p2align 3
.L203:
	movl	$53, %eax
	jmp	.L191
.L212:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1357:
	.size	NAME_CONSTRAINTS_check, .-NAME_CONSTRAINTS_check
	.p2align 4
	.globl	NAME_CONSTRAINTS_check_CN
	.type	NAME_CONSTRAINTS_check_CN, @function
NAME_CONSTRAINTS_check_CN:
.LFB1359:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	$-1, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-104(%rbp), %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -120(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	X509_get_subject_name@PLT
	movq	$0, -64(%rbp)
	movq	%rax, %r12
	leaq	-80(%rbp), %rax
	movl	$22, -76(%rbp)
	movl	$2, -96(%rbp)
	movq	%rax, -88(%rbp)
	.p2align 4,,10
	.p2align 3
.L215:
	movl	%r15d, %edx
	movl	$13, %esi
	movq	%r12, %rdi
	call	X509_NAME_get_index_by_NID@PLT
	movl	%eax, %r15d
	cmpl	$-1, %eax
	je	.L230
	movl	%eax, %esi
	movq	%r12, %rdi
	call	X509_NAME_get_entry@PLT
	movq	%rax, %rdi
	call	X509_NAME_ENTRY_get_data@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	ASN1_STRING_to_UTF8@PLT
	movl	%eax, %ebx
	testl	%eax, %eax
	js	.L217
	movq	-104(%rbp), %r13
	movq	%r13, %rdi
	call	strlen@PLT
	testl	%ebx, %ebx
	je	.L222
	movslq	%ebx, %rdx
	jmp	.L219
	.p2align 4,,10
	.p2align 3
.L223:
	subq	$1, %rdx
	testl	%edx, %edx
	je	.L222
.L219:
	cmpb	$0, -1(%r13,%rdx)
	movl	%edx, %esi
	je	.L223
	cmpq	%rdx, %rax
	jne	.L220
	testl	%edx, %edx
	je	.L221
	leal	-1(%rax), %edi
	xorl	%edx, %edx
	leal	-1(%rsi), %r10d
	xorl	%r9d, %r9d
.L226:
	movzbl	0(%r13,%rdx), %ecx
	movl	%ecx, %eax
	andl	$-33, %eax
	subl	$65, %eax
	cmpb	$25, %al
	jbe	.L225
	leal	-48(%rcx), %eax
	cmpb	$9, %al
	jbe	.L225
	cmpb	$95, %cl
	je	.L225
	testq	%rdx, %rdx
	jne	.L251
.L221:
	movl	$389, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r13, %rdi
	call	CRYPTO_free@PLT
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L222:
	testq	%rax, %rax
	je	.L221
.L220:
	movl	$341, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r13, %rdi
	movl	$53, %ebx
	call	CRYPTO_free@PLT
.L214:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L252
	addq	$88, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L251:
	.cfi_restore_state
	cmpl	%r10d, %edx
	jge	.L221
	cmpb	$45, %cl
	je	.L225
	cmpb	$46, %cl
	jne	.L221
	movzbl	1(%r13,%rdx), %eax
	cmpb	$46, %al
	je	.L221
	cmpb	$45, -1(%r13,%rdx)
	je	.L221
	cmpb	$45, %al
	je	.L221
	movl	$1, %r9d
	.p2align 4,,10
	.p2align 3
.L225:
	leaq	1(%rdx), %rax
	cmpq	%rdx, %rdi
	je	.L253
	movq	%rax, %rdx
	jmp	.L226
	.p2align 4,,10
	.p2align 3
.L217:
	movl	$17, %ebx
	jmp	.L214
.L253:
	testl	%r9d, %r9d
	je	.L221
	movl	%esi, -80(%rbp)
	movq	-120(%rbp), %rsi
	leaq	-96(%rbp), %rdi
	movq	%r13, -72(%rbp)
	leaq	8(%rsi), %rdx
	call	nc_match.isra.0
	movl	$431, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r13, %rdi
	movl	%eax, %ebx
	call	CRYPTO_free@PLT
	testl	%ebx, %ebx
	je	.L215
	jmp	.L214
	.p2align 4,,10
	.p2align 3
.L230:
	xorl	%ebx, %ebx
	jmp	.L214
.L252:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1359:
	.size	NAME_CONSTRAINTS_check_CN, .-NAME_CONSTRAINTS_check_CN
	.globl	NAME_CONSTRAINTS_it
	.section	.rodata.str1.1
.LC15:
	.string	"NAME_CONSTRAINTS"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	NAME_CONSTRAINTS_it, @object
	.size	NAME_CONSTRAINTS_it, 56
NAME_CONSTRAINTS_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	NAME_CONSTRAINTS_seq_tt
	.quad	2
	.quad	0
	.quad	16
	.quad	.LC15
	.section	.rodata.str1.1
.LC16:
	.string	"permittedSubtrees"
.LC17:
	.string	"excludedSubtrees"
	.section	.data.rel.ro.local
	.align 32
	.type	NAME_CONSTRAINTS_seq_tt, @object
	.size	NAME_CONSTRAINTS_seq_tt, 80
NAME_CONSTRAINTS_seq_tt:
	.quad	141
	.quad	0
	.quad	0
	.quad	.LC16
	.quad	GENERAL_SUBTREE_it
	.quad	141
	.quad	1
	.quad	8
	.quad	.LC17
	.quad	GENERAL_SUBTREE_it
	.globl	GENERAL_SUBTREE_it
	.section	.rodata.str1.1
.LC18:
	.string	"GENERAL_SUBTREE"
	.section	.data.rel.ro.local
	.align 32
	.type	GENERAL_SUBTREE_it, @object
	.size	GENERAL_SUBTREE_it, 56
GENERAL_SUBTREE_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	GENERAL_SUBTREE_seq_tt
	.quad	3
	.quad	0
	.quad	24
	.quad	.LC18
	.section	.rodata.str1.1
.LC19:
	.string	"base"
.LC20:
	.string	"minimum"
.LC21:
	.string	"maximum"
	.section	.data.rel.ro,"aw"
	.align 32
	.type	GENERAL_SUBTREE_seq_tt, @object
	.size	GENERAL_SUBTREE_seq_tt, 120
GENERAL_SUBTREE_seq_tt:
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC19
	.quad	GENERAL_NAME_it
	.quad	137
	.quad	0
	.quad	8
	.quad	.LC20
	.quad	ASN1_INTEGER_it
	.quad	137
	.quad	1
	.quad	16
	.quad	.LC21
	.quad	ASN1_INTEGER_it
	.globl	v3_name_constraints
	.section	.data.rel.ro.local
	.align 32
	.type	v3_name_constraints, @object
	.size	v3_name_constraints, 104
v3_name_constraints:
	.long	666
	.long	0
	.quad	NAME_CONSTRAINTS_it
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	v2i_NAME_CONSTRAINTS
	.quad	i2r_NAME_CONSTRAINTS
	.quad	0
	.quad	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
