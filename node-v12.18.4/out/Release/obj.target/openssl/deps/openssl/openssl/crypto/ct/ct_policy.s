	.file	"ct_policy.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/ct/ct_policy.c"
	.text
	.p2align 4
	.globl	CT_POLICY_EVAL_CTX_new
	.type	CT_POLICY_EVAL_CTX_new, @function
CT_POLICY_EVAL_CTX_new:
.LFB1296:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$30, %edx
	movl	$32, %edi
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	CRYPTO_zalloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L6
	xorl	%edi, %edi
	call	time@PLT
	addq	$300, %rax
	imulq	$1000, %rax, %rax
	movq	%rax, 24(%r12)
.L1:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	.cfi_restore_state
	movl	$33, %r8d
	movl	$65, %edx
	movl	$133, %esi
	movl	$50, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L1
	.cfi_endproc
.LFE1296:
	.size	CT_POLICY_EVAL_CTX_new, .-CT_POLICY_EVAL_CTX_new
	.p2align 4
	.globl	CT_POLICY_EVAL_CTX_free
	.type	CT_POLICY_EVAL_CTX_free, @function
CT_POLICY_EVAL_CTX_free:
.LFB1297:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L7
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	(%rdi), %rdi
	call	X509_free@PLT
	movq	8(%r12), %rdi
	call	X509_free@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$50, %edx
	popq	%r12
	.cfi_restore 12
	leaq	.LC0(%rip), %rsi
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_free@PLT
	.p2align 4,,10
	.p2align 3
.L7:
	ret
	.cfi_endproc
.LFE1297:
	.size	CT_POLICY_EVAL_CTX_free, .-CT_POLICY_EVAL_CTX_free
	.p2align 4
	.globl	CT_POLICY_EVAL_CTX_set1_cert
	.type	CT_POLICY_EVAL_CTX_set1_cert, @function
CT_POLICY_EVAL_CTX_set1_cert:
.LFB1298:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	call	X509_up_ref@PLT
	testl	%eax, %eax
	je	.L12
	movq	%rbx, (%r12)
	movl	$1, %eax
.L12:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1298:
	.size	CT_POLICY_EVAL_CTX_set1_cert, .-CT_POLICY_EVAL_CTX_set1_cert
	.p2align 4
	.globl	CT_POLICY_EVAL_CTX_set1_issuer
	.type	CT_POLICY_EVAL_CTX_set1_issuer, @function
CT_POLICY_EVAL_CTX_set1_issuer:
.LFB1299:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	call	X509_up_ref@PLT
	testl	%eax, %eax
	je	.L18
	movq	%rbx, 8(%r12)
	movl	$1, %eax
.L18:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1299:
	.size	CT_POLICY_EVAL_CTX_set1_issuer, .-CT_POLICY_EVAL_CTX_set1_issuer
	.p2align 4
	.globl	CT_POLICY_EVAL_CTX_set_shared_CTLOG_STORE
	.type	CT_POLICY_EVAL_CTX_set_shared_CTLOG_STORE, @function
CT_POLICY_EVAL_CTX_set_shared_CTLOG_STORE:
.LFB1300:
	.cfi_startproc
	endbr64
	movq	%rsi, 16(%rdi)
	ret
	.cfi_endproc
.LFE1300:
	.size	CT_POLICY_EVAL_CTX_set_shared_CTLOG_STORE, .-CT_POLICY_EVAL_CTX_set_shared_CTLOG_STORE
	.p2align 4
	.globl	CT_POLICY_EVAL_CTX_set_time
	.type	CT_POLICY_EVAL_CTX_set_time, @function
CT_POLICY_EVAL_CTX_set_time:
.LFB1301:
	.cfi_startproc
	endbr64
	movq	%rsi, 24(%rdi)
	ret
	.cfi_endproc
.LFE1301:
	.size	CT_POLICY_EVAL_CTX_set_time, .-CT_POLICY_EVAL_CTX_set_time
	.p2align 4
	.globl	CT_POLICY_EVAL_CTX_get0_cert
	.type	CT_POLICY_EVAL_CTX_get0_cert, @function
CT_POLICY_EVAL_CTX_get0_cert:
.LFB1302:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE1302:
	.size	CT_POLICY_EVAL_CTX_get0_cert, .-CT_POLICY_EVAL_CTX_get0_cert
	.p2align 4
	.globl	CT_POLICY_EVAL_CTX_get0_issuer
	.type	CT_POLICY_EVAL_CTX_get0_issuer, @function
CT_POLICY_EVAL_CTX_get0_issuer:
.LFB1303:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	ret
	.cfi_endproc
.LFE1303:
	.size	CT_POLICY_EVAL_CTX_get0_issuer, .-CT_POLICY_EVAL_CTX_get0_issuer
	.p2align 4
	.globl	CT_POLICY_EVAL_CTX_get0_log_store
	.type	CT_POLICY_EVAL_CTX_get0_log_store, @function
CT_POLICY_EVAL_CTX_get0_log_store:
.LFB1304:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	ret
	.cfi_endproc
.LFE1304:
	.size	CT_POLICY_EVAL_CTX_get0_log_store, .-CT_POLICY_EVAL_CTX_get0_log_store
	.p2align 4
	.globl	CT_POLICY_EVAL_CTX_get_time
	.type	CT_POLICY_EVAL_CTX_get_time, @function
CT_POLICY_EVAL_CTX_get_time:
.LFB1305:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	ret
	.cfi_endproc
.LFE1305:
	.size	CT_POLICY_EVAL_CTX_get_time, .-CT_POLICY_EVAL_CTX_get_time
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
