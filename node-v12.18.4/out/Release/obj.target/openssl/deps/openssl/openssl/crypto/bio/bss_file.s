	.file	"bss_file.c"
	.text
	.p2align 4
	.type	file_new, @function
file_new:
.LFB270:
	.cfi_startproc
	endbr64
	movl	$0, 32(%rdi)
	movl	$1, %eax
	movl	$0, 48(%rdi)
	movq	$0, 56(%rdi)
	movl	$0, 40(%rdi)
	ret
	.cfi_endproc
.LFE270:
	.size	file_new, .-file_new
	.p2align 4
	.type	file_gets, @function
file_gets:
.LFB275:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	movl	%edx, %esi
	subq	$8, %rsp
	movb	$0, (%r12)
	movq	56(%rdi), %rdx
	movq	%r12, %rdi
	call	fgets@PLT
	testq	%rax, %rax
	movl	$0, %eax
	je	.L3
.L4:
	endbr64
	cmpb	$0, (%r12)
	je	.L3
	movq	%r12, %rdi
	call	strlen@PLT
.L3:
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE275:
	.size	file_gets, .-file_gets
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/bio/bss_file.c"
	.text
	.p2align 4
	.type	file_read, @function
file_read:
.LFB272:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movl	32(%rdi), %eax
	movq	%rdi, %rbx
	testl	%eax, %eax
	je	.L10
	movq	%rsi, %rdi
	testq	%rsi, %rsi
	je	.L10
	movq	56(%rbx), %rcx
	movslq	%edx, %rdx
	movl	$1, %esi
	call	fread@PLT
	movq	56(%rbx), %rdi
	movl	%eax, %r12d
	call	ferror@PLT
	testl	%eax, %eax
	je	.L8
	call	__errno_location@PLT
	movl	$149, %r8d
	movl	$11, %esi
	leaq	.LC0(%rip), %rcx
	movl	(%rax), %edx
	movl	$2, %edi
	movl	$-1, %r12d
	call	ERR_put_error@PLT
	movl	$150, %r8d
	movl	$2, %edx
	leaq	.LC0(%rip), %rcx
	movl	$130, %esi
	movl	$32, %edi
	call	ERR_put_error@PLT
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L10:
	xorl	%r12d, %r12d
.L8:
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE272:
	.size	file_read, .-file_read
	.p2align 4
	.type	file_write, @function
file_write:
.LFB273:
	.cfi_startproc
	endbr64
	movl	32(%rdi), %eax
	testl	%eax, %eax
	je	.L32
	movq	%rsi, %r8
	testq	%rsi, %rsi
	jne	.L36
.L32:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L36:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edx, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movl	%edx, %ebx
	movl	$1, %edx
	subq	$8, %rsp
	movq	56(%rdi), %rcx
	movq	%r8, %rdi
	call	fwrite@PLT
	movq	%rax, %r8
	movl	%ebx, %eax
	testl	%r8d, %r8d
	je	.L37
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L37:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE273:
	.size	file_write, .-file_write
	.p2align 4
	.type	file_puts, @function
file_puts:
.LFB276:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movl	32(%rdi), %eax
	testl	%eax, %eax
	je	.L39
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	movq	%rsi, %r12
	call	strlen@PLT
	movq	56(%rbx), %rcx
	movl	$1, %edx
	movq	%r12, %rdi
	movslq	%eax, %rsi
	movl	%eax, %r13d
	call	fwrite@PLT
	testl	%eax, %eax
	je	.L39
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L39:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%r13d, %r13d
	popq	%rbx
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE276:
	.size	file_puts, .-file_puts
	.p2align 4
	.type	file_free, @function
file_free:
.LFB271:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L45
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	36(%rdi), %edx
	testl	%edx, %edx
	je	.L42
	movl	32(%rdi), %eax
	testl	%eax, %eax
	je	.L44
	movq	56(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L44
	call	fclose@PLT
	movq	$0, 56(%rbx)
	movl	$0, 40(%rbx)
.L44:
	movl	$0, 32(%rbx)
	movl	$1, %eax
.L42:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L45:
	.cfi_restore 3
	.cfi_restore 6
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE271:
	.size	file_free, .-file_free
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"a+"
.LC2:
	.string	"a"
.LC3:
	.string	"r+"
.LC4:
	.string	"w"
.LC5:
	.string	"r"
.LC6:
	.string	"')"
.LC7:
	.string	"','"
.LC8:
	.string	"fopen('"
.LC9:
	.string	"fflush()"
	.text
	.p2align 4
	.type	file_ctrl, @function
file_ctrl:
.LFB274:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	subq	$40, %rsp
	movq	56(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpl	$12, %esi
	jg	.L55
	testl	%esi, %esi
	jle	.L54
	cmpl	$12, %esi
	ja	.L86
	leaq	.L65(%rip), %rdx
	movl	%esi, %esi
	movslq	(%rdx,%rsi,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L65:
	.long	.L86-.L65
	.long	.L60-.L65
	.long	.L69-.L65
	.long	.L58-.L65
	.long	.L86-.L65
	.long	.L86-.L65
	.long	.L86-.L65
	.long	.L86-.L65
	.long	.L68-.L65
	.long	.L67-.L65
	.long	.L86-.L65
	.long	.L66-.L65
	.long	.L85-.L65
	.text
.L66:
	call	fflush@PLT
	cmpl	$-1, %eax
	je	.L102
.L85:
	movl	$1, %eax
.L54:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L103
	addq	$40, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L86:
	.cfi_restore_state
	xorl	%eax, %eax
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L55:
	subl	$106, %esi
	cmpl	$27, %esi
	ja	.L86
	leaq	.L59(%rip), %rdx
	movq	%rcx, %r14
	movslq	(%rdx,%rsi,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L59:
	.long	.L63-.L59
	.long	.L62-.L59
	.long	.L61-.L59
	.long	.L86-.L59
	.long	.L86-.L59
	.long	.L86-.L59
	.long	.L86-.L59
	.long	.L86-.L59
	.long	.L86-.L59
	.long	.L86-.L59
	.long	.L86-.L59
	.long	.L86-.L59
	.long	.L86-.L59
	.long	.L86-.L59
	.long	.L86-.L59
	.long	.L86-.L59
	.long	.L86-.L59
	.long	.L86-.L59
	.long	.L86-.L59
	.long	.L86-.L59
	.long	.L86-.L59
	.long	.L86-.L59
	.long	.L60-.L59
	.long	.L86-.L59
	.long	.L86-.L59
	.long	.L86-.L59
	.long	.L86-.L59
	.long	.L58-.L59
	.text
.L61:
	movl	36(%r13), %edx
	testl	%edx, %edx
	je	.L72
	movl	32(%r13), %eax
	testl	%eax, %eax
	je	.L73
	testq	%rdi, %rdi
	je	.L73
	call	fclose@PLT
	movq	$0, 56(%r13)
	movl	$0, 40(%r13)
.L73:
	movl	$0, 32(%r13)
.L72:
	movl	%r12d, %eax
	andl	$1, %eax
	movl	%eax, 36(%r13)
	testb	$8, %r12b
	jne	.L104
	movq	%r12, %rax
	andl	$6, %eax
	cmpq	$6, %rax
	je	.L105
	testb	$4, %r12b
	jne	.L106
	movq	%r12, %rax
	andl	$2, %eax
	je	.L79
	leaq	-44(%rbp), %r12
	movl	$4, %edx
	leaq	.LC5(%rip), %rsi
	movq	%r12, %rdi
	call	OPENSSL_strlcpy@PLT
	.p2align 4,,10
	.p2align 3
.L76:
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	openssl_fopen@PLT
	testq	%rax, %rax
	je	.L107
	movq	%rax, 56(%r13)
	xorl	%esi, %esi
	movq	%r13, %rdi
	movl	$1, 32(%r13)
	call	BIO_clear_flags@PLT
	movl	$1, %eax
	jmp	.L54
.L60:
	xorl	%edx, %edx
	movq	%r12, %rsi
	call	fseek@PLT
	cltq
	jmp	.L54
.L67:
	movl	%r12d, 36(%r13)
	movl	$1, %eax
	jmp	.L54
.L63:
	movl	36(%r13), %esi
	testl	%esi, %esi
	je	.L71
	movl	32(%r13), %ecx
	testl	%ecx, %ecx
	je	.L71
	testq	%rdi, %rdi
	je	.L71
	call	fclose@PLT
	movl	$0, 40(%r13)
	.p2align 4,,10
	.p2align 3
.L71:
	movl	%r12d, %eax
	movq	%r14, 56(%r13)
	andl	$1, %eax
	movl	$1, 32(%r13)
	movl	%eax, 36(%r13)
	movl	$1, %eax
	jmp	.L54
.L62:
	testq	%rcx, %rcx
	je	.L85
	movq	%rdi, (%rcx)
	movl	$1, %eax
	jmp	.L54
.L58:
	call	ftell@PLT
	jmp	.L54
.L68:
	movslq	36(%r13), %rax
	jmp	.L54
.L69:
	call	feof@PLT
	cltq
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L104:
	andl	$2, %r12d
	movl	$4, %edx
	leaq	-44(%rbp), %r12
	je	.L75
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	OPENSSL_strlcpy@PLT
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L102:
	call	__errno_location@PLT
	movl	$316, %r8d
	movl	$18, %esi
	leaq	.LC0(%rip), %rcx
	movl	(%rax), %edx
	movl	$2, %edi
	call	ERR_put_error@PLT
	leaq	.LC9(%rip), %rsi
	movl	$1, %edi
	xorl	%eax, %eax
	call	ERR_add_error_data@PLT
	movl	$318, %r8d
.L101:
	movl	$2, %edx
	movl	$116, %esi
	movl	$32, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L107:
	call	__errno_location@PLT
	movl	$288, %r8d
	movl	$1, %esi
	leaq	.LC0(%rip), %rcx
	movl	(%rax), %edx
	movl	$2, %edi
	call	ERR_put_error@PLT
	movq	%r12, %r8
	movq	%r14, %rdx
	movl	$5, %edi
	leaq	.LC6(%rip), %r9
	leaq	.LC7(%rip), %rcx
	xorl	%eax, %eax
	leaq	.LC8(%rip), %rsi
	call	ERR_add_error_data@PLT
	movl	$290, %r8d
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L75:
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	OPENSSL_strlcpy@PLT
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L79:
	movl	$273, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$101, %edx
	movl	$116, %esi
	movl	$32, %edi
	movq	%rax, -56(%rbp)
	call	ERR_put_error@PLT
	movq	-56(%rbp), %rax
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L106:
	leaq	-44(%rbp), %r12
	movl	$4, %edx
	leaq	.LC4(%rip), %rsi
	movq	%r12, %rdi
	call	OPENSSL_strlcpy@PLT
	jmp	.L76
.L105:
	leaq	-44(%rbp), %r12
	movl	$4, %edx
	leaq	.LC3(%rip), %rsi
	movq	%r12, %rdi
	call	OPENSSL_strlcpy@PLT
	jmp	.L76
.L103:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE274:
	.size	file_ctrl, .-file_ctrl
	.p2align 4
	.globl	BIO_new_file
	.type	BIO_new_file, @function
BIO_new_file:
.LFB267:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	call	openssl_fopen@PLT
	movl	$98, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	strchr@PLT
	cmpq	$1, %rax
	sbbq	%r13, %r13
	andl	$16, %r13d
	addq	$1, %r13
	testq	%r14, %r14
	je	.L116
	leaq	methods_filep(%rip), %rdi
	call	BIO_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L117
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	BIO_clear_flags@PLT
	movq	%r14, %rcx
	movq	%r13, %rdx
	movl	$106, %esi
	movq	%r12, %rdi
	call	BIO_ctrl@PLT
.L108:
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L116:
	.cfi_restore_state
	call	__errno_location@PLT
	movl	$69, %r8d
	movl	$1, %esi
	leaq	.LC0(%rip), %rcx
	movl	(%rax), %edx
	movl	$2, %edi
	movq	%rax, %r13
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	movq	%r12, %r8
	movq	%r15, %rdx
	leaq	.LC6(%rip), %r9
	leaq	.LC7(%rip), %rcx
	movl	$5, %edi
	leaq	.LC8(%rip), %rsi
	call	ERR_add_error_data@PLT
	movl	0(%r13), %eax
	andl	$-5, %eax
	cmpl	$2, %eax
	jne	.L111
	movl	$76, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$128, %edx
	xorl	%r12d, %r12d
	movl	$109, %esi
	movl	$32, %edi
	call	ERR_put_error@PLT
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L117:
	movq	%r14, %rdi
	call	fclose@PLT
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L111:
	movl	$78, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$2, %edx
	xorl	%r12d, %r12d
	movl	$109, %esi
	movl	$32, %edi
	call	ERR_put_error@PLT
	jmp	.L108
	.cfi_endproc
.LFE267:
	.size	BIO_new_file, .-BIO_new_file
	.p2align 4
	.globl	BIO_new_fp
	.type	BIO_new_fp, @function
BIO_new_fp:
.LFB268:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	leaq	methods_filep(%rip), %rdi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movl	%esi, %ebx
	subq	$8, %rsp
	call	BIO_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L118
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	BIO_set_flags@PLT
	movslq	%ebx, %rdx
	movq	%r13, %rcx
	movl	$106, %esi
	movq	%r12, %rdi
	call	BIO_ctrl@PLT
.L118:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE268:
	.size	BIO_new_fp, .-BIO_new_fp
	.p2align 4
	.globl	BIO_s_file
	.type	BIO_s_file, @function
BIO_s_file:
.LFB269:
	.cfi_startproc
	endbr64
	leaq	methods_filep(%rip), %rax
	ret
	.cfi_endproc
.LFE269:
	.size	BIO_s_file, .-BIO_s_file
	.section	.rodata.str1.1
.LC10:
	.string	"FILE pointer"
	.section	.data.rel.ro,"aw"
	.align 32
	.type	methods_filep, @object
	.size	methods_filep, 96
methods_filep:
	.long	1026
	.zero	4
	.quad	.LC10
	.quad	bwrite_conv
	.quad	file_write
	.quad	bread_conv
	.quad	file_read
	.quad	file_puts
	.quad	file_gets
	.quad	file_ctrl
	.quad	file_new
	.quad	file_free
	.quad	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
