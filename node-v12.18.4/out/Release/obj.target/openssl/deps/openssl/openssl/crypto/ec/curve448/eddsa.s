	.file	"eddsa.c"
	.text
	.p2align 4
	.type	hash_init_with_dom.constprop.0, @function
hash_init_with_dom.constprop.0:
.LFB413:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	subq	$32, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movb	$0, -41(%rbp)
	movabsq	$4049919570178697555, %rax
	movq	%rax, -49(%rbp)
	cmpq	$255, %rcx
	ja	.L4
	testb	%sil, %sil
	movq	%rdi, %r13
	movq	%rdx, %r14
	movb	%cl, -50(%rbp)
	setne	-51(%rbp)
	movq	%rcx, %r12
	call	EVP_shake256@PLT
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	EVP_DigestInit_ex@PLT
	testl	%eax, %eax
	jne	.L17
.L4:
	xorl	%eax, %eax
.L1:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L18
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	.cfi_restore_state
	leaq	-49(%rbp), %r15
	movq	%r15, %rdi
	call	strlen@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	je	.L4
	leaq	-51(%rbp), %rsi
	movl	$2, %edx
	movq	%r13, %rdi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	je	.L4
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
	negl	%eax
	jmp	.L1
.L18:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE413:
	.size	hash_init_with_dom.constprop.0, .-hash_init_with_dom.constprop.0
	.p2align 4
	.type	c448_ed448_verify.part.0, @function
c448_ed448_verify.part.0:
.LFB411:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-688(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r9, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r14, %rdi
	subq	$808, %rsp
	movq	%rdx, -832(%rbp)
	movzbl	16(%rbp), %r15d
	movq	%rcx, -840(%rbp)
	movl	%r8d, -824(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	curve448_point_decode_like_eddsa_and_mul_by_ratio@PLT
	cmpl	$-1, %eax
	je	.L40
.L19:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L41
	addq	$808, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L40:
	.cfi_restore_state
	leaq	-432(%rbp), %rax
	movq	%rbx, %rsi
	movq	%rax, %rdi
	movq	%rax, -848(%rbp)
	call	curve448_point_decode_like_eddsa_and_mul_by_ratio@PLT
	cmpl	$-1, %eax
	jne	.L19
	call	EVP_MD_CTX_new@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L22
	movzbl	-824(%rbp), %esi
	movzbl	%r15b, %ecx
	movq	%r13, %rdx
	movq	%rax, -824(%rbp)
	call	hash_init_with_dom.constprop.0
	movq	-824(%rbp), %rdi
	testl	%eax, %eax
	jne	.L42
.L22:
	call	EVP_MD_CTX_free@PLT
	xorl	%eax, %eax
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L42:
	movl	$57, %edx
	movq	%rbx, %rsi
	call	EVP_DigestUpdate@PLT
	movq	-824(%rbp), %rdi
	testl	%eax, %eax
	je	.L22
	movl	$57, %edx
	movq	%r12, %rsi
	call	EVP_DigestUpdate@PLT
	movq	-824(%rbp), %rdi
	testl	%eax, %eax
	je	.L22
	movq	-840(%rbp), %rdx
	movq	-832(%rbp), %rsi
	call	EVP_DigestUpdate@PLT
	movq	-824(%rbp), %rdi
	testl	%eax, %eax
	je	.L22
	leaq	-176(%rbp), %r12
	movl	$114, %edx
	movq	%r12, %rsi
	call	EVP_DigestFinalXOF@PLT
	movq	-824(%rbp), %rdi
	testl	%eax, %eax
	je	.L22
	call	EVP_MD_CTX_free@PLT
	leaq	-816(%rbp), %r13
	movl	$114, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	curve448_scalar_decode_long@PLT
	movq	%r12, %rdi
	movl	$114, %esi
	leaq	-752(%rbp), %r12
	call	OPENSSL_cleanse@PLT
	movq	%r13, %rdx
	movq	%r13, %rdi
	leaq	curve448_scalar_zero(%rip), %rsi
	call	curve448_scalar_sub@PLT
	leaq	57(%rbx), %rsi
	movq	%r12, %rdi
	movl	$57, %edx
	call	curve448_scalar_decode_long@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%r13, %rcx
	movq	%r14, %rdx
	call	curve448_base_double_scalarmul_non_secret@PLT
	movq	-848(%rbp), %rsi
	movq	%r14, %rdi
	call	curve448_point_eq@PLT
	jmp	.L19
.L41:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE411:
	.size	c448_ed448_verify.part.0, .-c448_ed448_verify.part.0
	.p2align 4
	.globl	c448_ed448_convert_private_key_to_x448
	.type	c448_ed448_convert_private_key_to_x448, @function
c448_ed448_convert_private_key_to_x448:
.LFB400:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -40
	call	EVP_MD_CTX_new@PLT
	movq	%rax, %r12
	xorl	%eax, %eax
	testq	%r12, %r12
	je	.L43
	call	EVP_shake256@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	EVP_DigestInit_ex@PLT
	testl	%eax, %eax
	je	.L46
	movl	$57, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	je	.L46
	movl	$56, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	EVP_DigestFinalXOF@PLT
	testl	%eax, %eax
	jne	.L56
.L46:
	movq	%r12, %rdi
	call	EVP_MD_CTX_free@PLT
	xorl	%eax, %eax
.L43:
	addq	$8, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L56:
	.cfi_restore_state
	movq	%r12, %rdi
	call	EVP_MD_CTX_free@PLT
	movl	$-1, %eax
	jmp	.L43
	.cfi_endproc
.LFE400:
	.size	c448_ed448_convert_private_key_to_x448, .-c448_ed448_convert_private_key_to_x448
	.p2align 4
	.globl	c448_ed448_derive_public_key
	.type	c448_ed448_derive_public_key, @function
c448_ed448_derive_public_key:
.LFB401:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	subq	$400, %rsp
	.cfi_offset 12, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	EVP_MD_CTX_new@PLT
	movq	%rax, %r12
	xorl	%eax, %eax
	testq	%r12, %r12
	je	.L57
	call	EVP_shake256@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	EVP_DigestInit_ex@PLT
	testl	%eax, %eax
	je	.L60
	movl	$57, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	je	.L60
	leaq	-112(%rbp), %r13
	movl	$57, %edx
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	EVP_DigestFinalXOF@PLT
	testl	%eax, %eax
	jne	.L71
.L60:
	movq	%r12, %rdi
	call	EVP_MD_CTX_free@PLT
	xorl	%eax, %eax
.L57:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L72
	addq	$400, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L71:
	.cfi_restore_state
	movq	%r12, %rdi
	leaq	-432(%rbp), %r12
	leaq	-368(%rbp), %r15
	call	EVP_MD_CTX_free@PLT
	movl	$57, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	andb	$-4, -112(%rbp)
	orb	$-128, -57(%rbp)
	movb	$0, -56(%rbp)
	call	curve448_scalar_decode_long@PLT
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	curve448_scalar_halve@PLT
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	curve448_scalar_halve@PLT
	movq	curve448_precomputed_base(%rip), %rsi
	movq	%r12, %rdx
	movq	%r15, %rdi
	call	curve448_precomputed_scalarmul@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	curve448_point_mul_by_ratio_and_encode_like_eddsa@PLT
	movq	%r12, %rdi
	call	curve448_scalar_destroy@PLT
	movq	%r15, %rdi
	call	curve448_point_destroy@PLT
	movl	$57, %esi
	movq	%r13, %rdi
	call	OPENSSL_cleanse@PLT
	movl	$-1, %eax
	jmp	.L57
.L72:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE401:
	.size	c448_ed448_derive_public_key, .-c448_ed448_derive_public_key
	.p2align 4
	.globl	c448_ed448_sign
	.type	c448_ed448_sign, @function
c448_ed448_sign:
.LFB402:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%r9d, %ebx
	subq	$712, %rsp
	movq	%rdi, -720(%rbp)
	movq	16(%rbp), %r15
	movq	%rdx, -712(%rbp)
	movq	%rcx, -696(%rbp)
	movq	%r8, -704(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	EVP_MD_CTX_new@PLT
	pxor	%xmm0, %xmm0
	movb	$0, -184(%rbp)
	movq	%rax, %r12
	movaps	%xmm0, -240(%rbp)
	xorl	%eax, %eax
	movq	$0, -192(%rbp)
	movaps	%xmm0, -224(%rbp)
	movaps	%xmm0, -208(%rbp)
	testq	%r12, %r12
	je	.L73
	call	EVP_MD_CTX_new@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L84
	call	EVP_shake256@PLT
	xorl	%edx, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	EVP_DigestInit_ex@PLT
	testl	%eax, %eax
	je	.L77
	movl	$57, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	jne	.L121
.L77:
	movq	%r14, %rdi
	call	EVP_MD_CTX_free@PLT
.L84:
	xorl	%eax, %eax
.L82:
	movq	%r12, %rdi
	movl	%eax, -696(%rbp)
	call	EVP_MD_CTX_free@PLT
	movl	-696(%rbp), %eax
.L73:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L122
	addq	$712, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L121:
	.cfi_restore_state
	leaq	-176(%rbp), %r13
	movl	$114, %edx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	EVP_DigestFinalXOF@PLT
	testl	%eax, %eax
	je	.L77
	movq	%r14, %rdi
	movzbl	%bl, %ebx
	call	EVP_MD_CTX_free@PLT
	leaq	-688(%rbp), %rax
	movq	%r13, %rsi
	movl	$57, %edx
	movq	%rax, %rdi
	andb	$-4, -176(%rbp)
	orb	$-128, -121(%rbp)
	movb	$0, -120(%rbp)
	movq	%rax, -728(%rbp)
	call	curve448_scalar_decode_long@PLT
	movq	24(%rbp), %rcx
	movq	%r15, %rdx
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	hash_init_with_dom.constprop.0
	testl	%eax, %eax
	je	.L79
	leaq	-119(%rbp), %rsi
	movl	$57, %edx
	movq	%r12, %rdi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	je	.L79
	movq	-704(%rbp), %rdx
	movq	-696(%rbp), %rsi
	movq	%r12, %rdi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	je	.L79
	movl	$114, %esi
	movq	%r13, %rdi
	call	OPENSSL_cleanse@PLT
	movl	$114, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	EVP_DigestFinalXOF@PLT
	testl	%eax, %eax
	je	.L82
	leaq	-624(%rbp), %rax
	movl	$114, %edx
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%rax, %r14
	movq	%rax, -752(%rbp)
	call	curve448_scalar_decode_long@PLT
	movl	$114, %esi
	movq	%r13, %rdi
	call	OPENSSL_cleanse@PLT
	leaq	-560(%rbp), %r8
	movq	%r14, %rsi
	movq	%r8, %rdi
	movq	%r8, -736(%rbp)
	call	curve448_scalar_halve@PLT
	movq	-736(%rbp), %r8
	movq	%r8, %rsi
	movq	%r8, %rdi
	call	curve448_scalar_halve@PLT
	movq	-736(%rbp), %r8
	leaq	-496(%rbp), %rax
	movq	curve448_precomputed_base(%rip), %rsi
	movq	%rax, %r14
	movq	%rax, %rdi
	movq	%r8, %rdx
	call	curve448_precomputed_scalarmul@PLT
	movq	%r14, %rsi
	leaq	-240(%rbp), %rdi
	movq	%rdi, -744(%rbp)
	call	curve448_point_mul_by_ratio_and_encode_like_eddsa@PLT
	movq	%r14, %rdi
	call	curve448_point_destroy@PLT
	movq	-736(%rbp), %r8
	movq	%r8, %rdi
	call	curve448_scalar_destroy@PLT
	movq	24(%rbp), %rcx
	movq	%r15, %rdx
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	hash_init_with_dom.constprop.0
	testl	%eax, %eax
	je	.L84
	movq	-744(%rbp), %rbx
	movl	$57, %edx
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	je	.L84
	movq	-712(%rbp), %rsi
	movl	$57, %edx
	movq	%r12, %rdi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	je	.L84
	movq	-704(%rbp), %rdx
	movq	-696(%rbp), %rsi
	movq	%r12, %rdi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	je	.L84
	movl	$114, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	EVP_DigestFinalXOF@PLT
	testl	%eax, %eax
	je	.L84
	movl	$114, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%r14, %r15
	call	curve448_scalar_decode_long@PLT
	movq	%r13, %rdi
	movl	$114, %esi
	call	OPENSSL_cleanse@PLT
	movq	-728(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r14, %rdi
	call	curve448_scalar_mul@PLT
	movq	-752(%rbp), %r13
	movq	%r14, %rsi
	movq	%r14, %rdi
	movq	%r13, %rdx
	call	curve448_scalar_add@PLT
	movq	-720(%rbp), %r14
	movl	$114, %esi
	movq	%r14, %rdi
	call	OPENSSL_cleanse@PLT
	movl	$57, %ecx
	movq	%r14, %rdi
	movq	%rbx, %rsi
	rep movsb
	movq	%r15, %rsi
	call	curve448_scalar_encode@PLT
	movq	-728(%rbp), %rdi
	call	curve448_scalar_destroy@PLT
	movq	%r13, %rdi
	call	curve448_scalar_destroy@PLT
	movq	%r15, %rdi
	call	curve448_scalar_destroy@PLT
	orl	$-1, %eax
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L79:
	movl	$114, %esi
	movq	%r13, %rdi
	call	OPENSSL_cleanse@PLT
	jmp	.L84
.L122:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE402:
	.size	c448_ed448_sign, .-c448_ed448_sign
	.p2align 4
	.globl	c448_ed448_sign_prehash
	.type	c448_ed448_sign_prehash, @function
c448_ed448_sign_prehash:
.LFB403:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r9
	movl	$1, %r9d
	pushq	%r8
	movl	$64, %r8d
	call	c448_ed448_sign
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE403:
	.size	c448_ed448_sign_prehash, .-c448_ed448_sign_prehash
	.p2align 4
	.globl	c448_ed448_verify
	.type	c448_ed448_verify, @function
c448_ed448_verify:
.LFB404:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$56, %eax
	leaq	order.10500(%rip), %r10
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movl	16(%rbp), %r11d
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L132:
	jb	.L127
	subq	$1, %rax
	cmpq	$-1, %rax
	je	.L125
.L128:
	movzbl	(%r10,%rax), %ebx
	cmpb	%bl, 57(%rdi,%rax)
	jbe	.L132
.L125:
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L127:
	.cfi_restore_state
	movzbl	%r11b, %r11d
	popq	%rbx
	movzbl	%r8b, %r8d
	movl	%r11d, 16(%rbp)
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	c448_ed448_verify.part.0
	.cfi_endproc
.LFE404:
	.size	c448_ed448_verify, .-c448_ed448_verify
	.p2align 4
	.globl	c448_ed448_verify_prehash
	.type	c448_ed448_verify_prehash, @function
c448_ed448_verify_prehash:
.LFB405:
	.cfi_startproc
	endbr64
	movl	$56, %eax
	leaq	order.10500(%rip), %r9
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L142:
	jb	.L135
	subq	$1, %rax
	cmpq	$-1, %rax
	je	.L137
.L136:
	movzbl	(%r9,%rax), %r10d
	cmpb	%r10b, 57(%rdi,%rax)
	jbe	.L142
.L137:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L135:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movzbl	%r8b, %r8d
	movq	%rcx, %r9
	movl	$64, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$8, %rsp
	pushq	%r8
	movl	$1, %r8d
	call	c448_ed448_verify.part.0
	popq	%rdx
	popq	%rcx
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE405:
	.size	c448_ed448_verify_prehash, .-c448_ed448_verify_prehash
	.p2align 4
	.globl	ED448_sign
	.type	ED448_sign, @function
ED448_sign:
.LFB406:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r10
	movq	%rdx, %r11
	movq	%r8, %rsi
	movq	%rcx, %rdx
	movq	%r11, %r8
	movq	%r10, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	16(%rbp)
	pushq	%r9
	xorl	%r9d, %r9d
	call	c448_ed448_sign
	popq	%rdx
	popq	%rcx
	leave
	.cfi_def_cfa 7, 8
	cmpl	$-1, %eax
	sete	%al
	movzbl	%al, %eax
	ret
	.cfi_endproc
.LFE406:
	.size	ED448_sign, .-ED448_sign
	.p2align 4
	.globl	ED448_verify
	.type	ED448_verify, @function
ED448_verify:
.LFB407:
	.cfi_startproc
	endbr64
	movq	%rdi, %r10
	movq	%rsi, %r11
	movq	%rdx, %rdi
	movq	%rcx, %rsi
	movl	$56, %eax
	leaq	order.10500(%rip), %rdx
	jmp	.L148
	.p2align 4,,10
	.p2align 3
.L154:
	jb	.L147
	subq	$1, %rax
	cmpq	$-1, %rax
	je	.L149
.L148:
	movzbl	(%rdx,%rax), %ecx
	cmpb	%cl, 57(%rdi,%rax)
	jbe	.L154
.L149:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L147:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movzbl	%r9b, %r9d
	movq	%r11, %rcx
	movq	%r10, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$8, %rsp
	pushq	%r9
	movq	%r8, %r9
	xorl	%r8d, %r8d
	call	c448_ed448_verify.part.0
	popq	%rdx
	popq	%rcx
	leave
	.cfi_def_cfa 7, 8
	cmpl	$-1, %eax
	sete	%al
	movzbl	%al, %eax
	ret
	.cfi_endproc
.LFE407:
	.size	ED448_verify, .-ED448_verify
	.p2align 4
	.globl	ED448ph_sign
	.type	ED448ph_sign, @function
ED448ph_sign:
.LFB408:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r10
	movq	%rcx, %rsi
	movq	%r10, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r9
	movl	$1, %r9d
	pushq	%r8
	movl	$64, %r8d
	call	c448_ed448_sign
	popq	%rdx
	popq	%rcx
	leave
	.cfi_def_cfa 7, 8
	cmpl	$-1, %eax
	sete	%al
	movzbl	%al, %eax
	ret
	.cfi_endproc
.LFE408:
	.size	ED448ph_sign, .-ED448ph_sign
	.p2align 4
	.globl	ED448ph_verify
	.type	ED448ph_verify, @function
ED448ph_verify:
.LFB409:
	.cfi_startproc
	endbr64
	movq	%rdi, %r10
	movl	$56, %eax
	movq	%rsi, %rdi
	movq	%rdx, %rsi
	leaq	order.10500(%rip), %rdx
	jmp	.L160
	.p2align 4,,10
	.p2align 3
.L166:
	jb	.L159
	subq	$1, %rax
	cmpq	$-1, %rax
	je	.L161
.L160:
	movzbl	(%rdx,%rax), %r11d
	cmpb	%r11b, 57(%rdi,%rax)
	jbe	.L166
.L161:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L159:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movzbl	%r8b, %r8d
	movq	%rcx, %r9
	movq	%r10, %rdx
	movl	$64, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$8, %rsp
	pushq	%r8
	movl	$1, %r8d
	call	c448_ed448_verify.part.0
	popq	%rdx
	popq	%rcx
	leave
	.cfi_def_cfa 7, 8
	cmpl	$-1, %eax
	sete	%al
	movzbl	%al, %eax
	ret
	.cfi_endproc
.LFE409:
	.size	ED448ph_verify, .-ED448ph_verify
	.p2align 4
	.globl	ED448_public_from_private
	.type	ED448_public_from_private, @function
ED448_public_from_private:
.LFB410:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	c448_ed448_derive_public_key
	popq	%rbp
	.cfi_def_cfa 7, 8
	cmpl	$-1, %eax
	sete	%al
	movzbl	%al, %eax
	ret
	.cfi_endproc
.LFE410:
	.size	ED448_public_from_private, .-ED448_public_from_private
	.section	.rodata
	.align 32
	.type	order.10500, @object
	.size	order.10500, 57
order.10500:
	.string	"\363DX\253\222\302x#U\217\305\215r\302l!\2206\326\256I\333N\304\351#\312|\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377?"
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
