	.file	"a_i2d_fp.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/asn1/a_i2d_fp.c"
	.text
	.p2align 4
	.globl	ASN1_i2d_bio
	.type	ASN1_i2d_bio, @function
ASN1_i2d_bio:
.LFB420:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movq	%rdx, %rdi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	xorl	%esi, %esi
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	*%r14
	xorl	%r9d, %r9d
	testl	%eax, %eax
	jle	.L1
	movslq	%eax, %rdi
	movl	$44, %edx
	movl	%r9d, -68(%rbp)
	movl	%eax, %ebx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L12
	movq	%rax, -64(%rbp)
	leaq	-64(%rbp), %rsi
	movq	%r15, %rdi
	call	*%r14
	xorl	%r14d, %r14d
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L13:
	testl	%eax, %eax
	jle	.L9
	addl	%eax, %r14d
	subl	%eax, %ebx
.L5:
	movslq	%r14d, %rsi
	movl	%ebx, %edx
	movq	%r13, %rdi
	addq	%r12, %rsi
	call	BIO_write@PLT
	cmpl	%eax, %ebx
	jne	.L13
	movl	$1, %r9d
.L4:
	movl	$64, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movl	%r9d, -68(%rbp)
	call	CRYPTO_free@PLT
	movl	-68(%rbp), %r9d
.L1:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L14
	addq	$40, %rsp
	movl	%r9d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	.cfi_restore_state
	xorl	%r9d, %r9d
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L12:
	movl	$46, %r8d
	movl	$65, %edx
	movl	$116, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	-68(%rbp), %r9d
	jmp	.L1
.L14:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE420:
	.size	ASN1_i2d_bio, .-ASN1_i2d_bio
	.p2align 4
	.globl	ASN1_i2d_fp
	.type	ASN1_i2d_fp, @function
ASN1_i2d_fp:
.LFB419:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	call	BIO_s_file@PLT
	movq	%rax, %rdi
	call	BIO_new@PLT
	testq	%rax, %rax
	je	.L19
	movq	%rax, %r12
	movq	%r15, %rcx
	xorl	%edx, %edx
	movl	$106, %esi
	movq	%rax, %rdi
	call	BIO_ctrl@PLT
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	ASN1_i2d_bio
	movq	%r12, %rdi
	movl	%eax, %r13d
	call	BIO_free@PLT
	popq	%r12
	movl	%r13d, %eax
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	.cfi_restore_state
	xorl	%r13d, %r13d
	movl	$24, %r8d
	movl	$7, %edx
	movl	$117, %esi
	leaq	.LC0(%rip), %rcx
	movl	$13, %edi
	call	ERR_put_error@PLT
	popq	%r12
	movl	%r13d, %eax
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE419:
	.size	ASN1_i2d_fp, .-ASN1_i2d_fp
	.p2align 4
	.globl	ASN1_item_i2d_fp
	.type	ASN1_item_i2d_fp, @function
ASN1_item_i2d_fp:
.LFB421:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	BIO_s_file@PLT
	movq	%rax, %rdi
	call	BIO_new@PLT
	testq	%rax, %rax
	je	.L32
	movq	%r15, %rcx
	xorl	%edx, %edx
	movl	$106, %esi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	BIO_ctrl@PLT
	movq	%r13, %rdi
	leaq	-64(%rbp), %rsi
	movq	%r14, %rdx
	movq	$0, -64(%rbp)
	xorl	%r13d, %r13d
	call	ASN1_item_i2d@PLT
	movq	-64(%rbp), %rdi
	movl	%eax, %ebx
	testq	%rdi, %rdi
	jne	.L23
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L34:
	testl	%eax, %eax
	jle	.L29
	addl	%eax, %r13d
	subl	%eax, %ebx
.L23:
	movslq	%r13d, %rsi
	movl	%ebx, %edx
	addq	%rdi, %rsi
	movq	%r12, %rdi
	call	BIO_write@PLT
	movq	-64(%rbp), %rdi
	cmpl	%eax, %ebx
	jne	.L34
	movl	$1, %r13d
.L26:
	movl	$109, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
.L24:
	movq	%r12, %rdi
	call	BIO_free@PLT
.L20:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L35
	addq	$24, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	.cfi_restore_state
	xorl	%r13d, %r13d
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L32:
	movl	$77, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$7, %edx
	xorl	%r13d, %r13d
	movl	$193, %esi
	movl	$13, %edi
	call	ERR_put_error@PLT
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L33:
	movl	$94, %r8d
	movl	$65, %edx
	movl	$192, %esi
	movl	$13, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L24
.L35:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE421:
	.size	ASN1_item_i2d_fp, .-ASN1_item_i2d_fp
	.p2align 4
	.globl	ASN1_item_i2d_bio
	.type	ASN1_item_i2d_bio, @function
ASN1_item_i2d_bio:
.LFB422:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r8
	movq	%rdx, %rdi
	movq	%r8, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	xorl	%r13d, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	leaq	-48(%rbp), %rsi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -48(%rbp)
	call	ASN1_item_i2d@PLT
	movq	-48(%rbp), %rdi
	movl	%eax, %r12d
	testq	%rdi, %rdi
	jne	.L37
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L48:
	testl	%eax, %eax
	jle	.L43
	addl	%eax, %r13d
	subl	%eax, %r12d
.L37:
	movslq	%r13d, %rsi
	movl	%r12d, %edx
	addq	%rdi, %rsi
	movq	%rbx, %rdi
	call	BIO_write@PLT
	movq	-48(%rbp), %rdi
	cmpl	%eax, %r12d
	jne	.L48
	movl	$1, %r12d
.L40:
	movl	$109, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
.L36:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L49
	addq	$24, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L43:
	.cfi_restore_state
	xorl	%r12d, %r12d
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L47:
	movl	$94, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r12d, %r12d
	movl	$192, %esi
	movl	$13, %edi
	call	ERR_put_error@PLT
	jmp	.L36
.L49:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE422:
	.size	ASN1_item_i2d_bio, .-ASN1_item_i2d_bio
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
