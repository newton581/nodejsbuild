	.file	"loader_file.c"
	.text
	.p2align 4
	.type	file_expect, @function
file_expect:
.LFB922:
	.cfi_startproc
	endbr64
	movl	%esi, 72(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE922:
	.size	file_expect, .-file_expect
	.p2align 4
	.type	file_error, @function
file_error:
.LFB934:
	.cfi_startproc
	endbr64
	movl	4(%rdi), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	setg	%al
	ret
	.cfi_endproc
.LFE934:
	.size	file_error, .-file_error
	.p2align 4
	.type	store_file_loader_deinit, @function
store_file_loader_deinit:
.LFB938:
	.cfi_startproc
	endbr64
	movq	file_loader(%rip), %rdi
	jmp	ossl_store_unregister_loader_int@PLT
	.cfi_endproc
.LFE938:
	.size	store_file_loader_deinit, .-store_file_loader_deinit
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/store/loader_file.c"
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"pass phrase"
	.text
	.p2align 4
	.type	file_get_pass, @function
file_get_pass:
.LFB907:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	%rdx, -56(%rbp)
	call	UI_new@PLT
	testq	%rax, %rax
	je	.L19
	movq	%rax, %r12
	testq	%r14, %r14
	je	.L8
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	UI_set_method@PLT
.L8:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	UI_add_user_data@PLT
	movq	%r13, %rdx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	UI_construct_prompt@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L20
	movl	-56(%rbp), %r9d
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movl	$2, %edx
	movq	%rax, %rsi
	movq	%r12, %rdi
	subl	$1, %r9d
	call	UI_add_input_string@PLT
	movl	$68, %r8d
	testl	%eax, %eax
	jne	.L21
.L18:
	leaq	.LC0(%rip), %rcx
	movl	$40, %edx
	movl	$118, %esi
	xorl	%r14d, %r14d
	movl	$44, %edi
	call	ERR_put_error@PLT
.L10:
	movq	%r13, %rdi
	movl	$86, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%r12, %rdi
	call	UI_free@PLT
.L5:
	addq	$24, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	.cfi_restore_state
	movq	%r12, %rdi
	call	UI_process@PLT
	cmpl	$-2, %eax
	je	.L12
	movq	%rbx, %r14
	cmpl	$-1, %eax
	jne	.L10
	movl	$78, %r8d
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L20:
	movl	$64, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r14d, %r14d
	movl	$118, %esi
	movl	$44, %edi
	call	ERR_put_error@PLT
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L12:
	movl	$73, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$109, %edx
	xorl	%r14d, %r14d
	movl	$118, %esi
	movl	$44, %edi
	call	ERR_put_error@PLT
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L19:
	movl	$54, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r14d, %r14d
	movl	$118, %esi
	movl	$44, %edi
	call	ERR_put_error@PLT
	jmp	.L5
	.cfi_endproc
.LFE907:
	.size	file_get_pass, .-file_get_pass
	.p2align 4
	.type	file_get_pem_pass, @function
file_get_pem_pass:
.LFB909:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %rax
	movslq	%esi, %rdx
	movq	16(%rcx), %rcx
	movq	%rdi, %rsi
	movq	8(%rax), %r8
	movq	(%rax), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	file_get_pass
	movq	%rax, %rdi
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L22
	call	strlen@PLT
.L22:
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE909:
	.size	file_get_pem_pass, .-file_get_pem_pass
	.section	.rodata.str1.1
.LC2:
	.string	"PRIVATE KEY"
	.text
	.p2align 4
	.type	try_decode_PrivateKey, @function
try_decode_PrivateKey:
.LFB914:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r9, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%rdx, -72(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L42
	leaq	.LC2(%rip), %r8
	movq	%rdi, %rsi
	movq	%rdi, %r14
	movl	$12, %ecx
	movq	%r8, %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L55
	movq	%r8, %rsi
	movq	%r14, %rdi
	call	pem_check_suffix@PLT
	movl	%eax, %edx
	testl	%eax, %eax
	jle	.L54
	xorl	%edi, %edi
	movq	%r14, %rsi
	call	EVP_PKEY_asn1_find_str@PLT
	testq	%rax, %rax
	je	.L54
	movl	$1, (%r12)
	movl	(%rax), %edi
	leaq	-72(%rbp), %rdx
	movq	%r13, %rcx
	xorl	%esi, %esi
	call	d2i_PrivateKey@PLT
	movq	%rax, %r14
.L33:
	testq	%r14, %r14
	je	.L54
	movq	%r14, %rdi
	call	OSSL_STORE_INFO_new_PKEY@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L56
.L28:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L57
	addq	$40, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L55:
	.cfi_restore_state
	movq	%r13, %rdx
	leaq	-72(%rbp), %rsi
	xorl	%edi, %edi
	call	d2i_PKCS8_PRIV_KEY_INFO@PLT
	movl	$1, (%r12)
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L58
	movq	%rax, %rdi
	call	EVP_PKCS82PKEY@PLT
	movq	%r13, %rdi
	movq	%rax, %r14
	call	PKCS8_PRIV_KEY_INFO_free@PLT
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L42:
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	leaq	-64(%rbp), %r15
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L60:
	movq	%rax, %rdi
	call	EVP_PKEY_free@PLT
.L39:
	addl	$1, (%r12)
.L37:
	addl	$1, %ebx
.L29:
	call	EVP_PKEY_asn1_get_count@PLT
	cmpl	%ebx, %eax
	jle	.L59
	movq	-72(%rbp), %rax
	movl	%ebx, %edi
	movq	%rax, -64(%rbp)
	call	EVP_PKEY_asn1_get0@PLT
	testb	$1, 8(%rax)
	jne	.L37
	movl	(%rax), %edi
	xorl	%esi, %esi
	movq	%r13, %rcx
	movq	%r15, %rdx
	call	d2i_PrivateKey@PLT
	testq	%rax, %rax
	je	.L37
	testq	%r14, %r14
	jne	.L60
	movq	%rax, %r14
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L59:
	cmpl	$1, (%r12)
	jle	.L33
	movq	%r14, %rdi
	call	EVP_PKEY_free@PLT
.L54:
	xorl	%r13d, %r13d
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L58:
	xorl	%edi, %edi
	call	PKCS8_PRIV_KEY_INFO_free@PLT
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L56:
	movq	%r14, %rdi
	call	EVP_PKEY_free@PLT
	jmp	.L28
.L57:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE914:
	.size	try_decode_PrivateKey, .-try_decode_PrivateKey
	.section	.rodata.str1.1
.LC3:
	.string	"PUBLIC KEY"
	.text
	.p2align 4
	.type	try_decode_PUBKEY, @function
try_decode_PUBKEY:
.LFB915:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%r9, %rbx
	subq	$24, %rsp
	movq	%rdx, -24(%rbp)
	movq	%rcx, %rdx
	testq	%rdi, %rdi
	je	.L62
	movq	%rdi, %rsi
	movl	$11, %ecx
	leaq	.LC3(%rip), %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L65
	movl	$1, (%r9)
.L62:
	xorl	%edi, %edi
	leaq	-24(%rbp), %rsi
	call	d2i_PUBKEY@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L65
	movl	$1, (%rbx)
	call	OSSL_STORE_INFO_new_PKEY@PLT
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L65:
	.cfi_restore_state
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE915:
	.size	try_decode_PUBKEY, .-try_decode_PUBKEY
	.section	.rodata.str1.1
.LC4:
	.string	"PARAMETERS"
	.text
	.p2align 4
	.type	try_decode_params, @function
try_decode_params:
.LFB916:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%r9, %r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -72(%rbp)
	movq	%rcx, -80(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L77
	leaq	.LC4(%rip), %rsi
	movq	%rdi, %r12
	call	pem_check_suffix@PLT
	movl	%eax, %r15d
	testl	%eax, %eax
	je	.L89
	movl	$1, 0(%r13)
	jg	.L113
.L77:
	xorl	%r12d, %r12d
	xorl	%ebx, %ebx
	leaq	-64(%rbp), %r15
	xorl	%r14d, %r14d
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L81:
	movl	%ebx, %edi
	call	EVP_PKEY_asn1_get0@PLT
	testb	$1, 8(%rax)
	jne	.L84
	movl	(%rax), %esi
	movq	%r12, %rdi
	call	EVP_PKEY_set_type@PLT
	testl	%eax, %eax
	jne	.L114
.L84:
	addl	$1, %ebx
.L75:
	call	EVP_PKEY_asn1_get_count@PLT
	cmpl	%ebx, %eax
	jle	.L82
	movq	-72(%rbp), %rax
	movq	%rax, -64(%rbp)
	testq	%r12, %r12
	jne	.L81
	call	EVP_PKEY_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L81
	movl	$549, %r8d
	movl	$6, %edx
	movl	$121, %esi
	movl	$44, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L82:
	movq	%r12, %rdi
	call	EVP_PKEY_free@PLT
	cmpl	$1, 0(%r13)
	je	.L80
.L79:
	movq	%r14, %rdi
	call	EVP_PKEY_free@PLT
	xorl	%eax, %eax
.L73:
	movq	-56(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L115
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L114:
	.cfi_restore_state
	movq	%r12, %rdi
	call	EVP_PKEY_get0_asn1@PLT
	testq	%rax, %rax
	je	.L84
	movq	112(%rax), %rax
	testq	%rax, %rax
	je	.L84
	movl	-80(%rbp), %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	*%rax
	testl	%eax, %eax
	je	.L84
	testq	%r14, %r14
	je	.L90
	movq	%r12, %rdi
	call	EVP_PKEY_free@PLT
.L86:
	addl	$1, 0(%r13)
	xorl	%r12d, %r12d
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L113:
	call	EVP_PKEY_new@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L116
	movl	%r15d, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	EVP_PKEY_set_type_str@PLT
	testl	%eax, %eax
	je	.L79
	movq	%r14, %rdi
	call	EVP_PKEY_get0_asn1@PLT
	testq	%rax, %rax
	je	.L79
	movq	112(%rax), %rax
	testq	%rax, %rax
	je	.L79
	movl	-80(%rbp), %edx
	leaq	-72(%rbp), %rsi
	movq	%r14, %rdi
	call	*%rax
	testl	%eax, %eax
	je	.L79
	.p2align 4,,10
	.p2align 3
.L80:
	movq	%r14, %rdi
	call	OSSL_STORE_INFO_new_PARAMS@PLT
	testq	%rax, %rax
	jne	.L73
	jmp	.L79
.L89:
	xorl	%eax, %eax
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L90:
	movq	%r12, %r14
	jmp	.L86
.L116:
	movl	$531, %r8d
	movl	$6, %edx
	movl	$121, %esi
	movl	$44, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L73
.L115:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE916:
	.size	try_decode_params, .-try_decode_params
	.section	.rodata.str1.1
.LC5:
	.string	"X509 CRL"
	.text
	.p2align 4
	.type	try_decode_X509CRL, @function
try_decode_X509CRL:
.LFB918:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%r9, %rbx
	subq	$16, %rsp
	movq	%rdx, -24(%rbp)
	movq	%rcx, %rdx
	testq	%rdi, %rdi
	je	.L118
	movq	%rdi, %rsi
	movl	$9, %ecx
	leaq	.LC5(%rip), %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L122
	movl	$1, (%r9)
.L118:
	leaq	-24(%rbp), %rsi
	xorl	%edi, %edi
	call	d2i_X509_CRL@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L121
	movl	$1, (%rbx)
	movq	%rax, %rdi
	call	OSSL_STORE_INFO_new_CRL@PLT
	testq	%rax, %rax
	je	.L121
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L121:
	.cfi_restore_state
	movq	%r12, %rdi
	call	X509_CRL_free@PLT
	addq	$16, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L122:
	.cfi_restore_state
	addq	$16, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE918:
	.size	try_decode_X509CRL, .-try_decode_X509CRL
	.section	.rodata.str1.1
.LC6:
	.string	"TRUSTED CERTIFICATE"
.LC7:
	.string	"X509 CERTIFICATE"
.LC8:
	.string	"CERTIFICATE"
	.text
	.p2align 4
	.type	try_decode_X509Certificate, @function
try_decode_X509Certificate:
.LFB917:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rcx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%r9, %rbx
	subq	$16, %rsp
	movq	%rdx, -40(%rbp)
	testq	%rdi, %rdi
	je	.L145
	movq	%rdi, %rax
	movl	$20, %ecx
	leaq	.LC6(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	jne	.L147
	movl	$1, (%r9)
	leaq	-40(%rbp), %rsi
	movq	%r13, %rdx
	xorl	%edi, %edi
	call	d2i_X509_AUX@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L132
.L138:
	xorl	%r12d, %r12d
.L137:
	movq	%r12, %rdi
	call	X509_free@PLT
	xorl	%eax, %eax
.L130:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L147:
	.cfi_restore_state
	movl	$17, %ecx
	leaq	.LC7(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L135
	movq	%rax, %rsi
	movl	$12, %ecx
	leaq	.LC8(%rip), %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L148
.L135:
	movl	$1, (%rbx)
.L145:
	leaq	-40(%rbp), %r14
	movq	%r13, %rdx
	xorl	%edi, %edi
	movq	%r14, %rsi
	call	d2i_X509_AUX@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L149
.L132:
	movl	$1, (%rbx)
	movq	%r12, %rdi
	call	OSSL_STORE_INFO_new_CERT@PLT
	testq	%rax, %rax
	je	.L137
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L149:
	.cfi_restore_state
	movq	%r13, %rdx
	movq	%r14, %rsi
	xorl	%edi, %edi
	call	d2i_X509@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L132
	jmp	.L138
	.p2align 4,,10
	.p2align 3
.L148:
	xorl	%eax, %eax
	jmp	.L130
	.cfi_endproc
.LFE917:
	.size	try_decode_X509Certificate, .-try_decode_X509Certificate
	.section	.rodata.str1.1
.LC9:
	.string	"ENCRYPTED PRIVATE KEY"
.LC10:
	.string	"PKCS8 decrypt password"
	.text
	.p2align 4
	.type	try_decode_PKCS8Encrypted, @function
try_decode_PKCS8Encrypted:
.LFB913:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%r9, %r12
	pushq	%rbx
	subq	$1088, %rsp
	.cfi_offset 3, -48
	movq	16(%rbp), %r13
	movq	24(%rbp), %rbx
	movq	%rdx, -1112(%rbp)
	movq	%rcx, %rdx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -1096(%rbp)
	movq	$0, -1088(%rbp)
	movq	$0, -1080(%rbp)
	testq	%rdi, %rdi
	je	.L151
	movq	%rdi, %rsi
	movl	$22, %ecx
	leaq	.LC9(%rip), %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L170
	movl	$1, (%r9)
.L151:
	leaq	-1112(%rbp), %rsi
	xorl	%edi, %edi
	call	d2i_X509_SIG@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L170
	movl	$1, (%r12)
	call	BUF_MEM_new@PLT
	movl	$353, %r8d
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L169
	movq	%r13, %rdi
	movq	%rbx, %r8
	leaq	-1072(%rbp), %rsi
	movl	$1024, %edx
	leaq	.LC10(%rip), %rcx
	call	file_get_pass
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L171
	leaq	-1088(%rbp), %rdx
	leaq	-1096(%rbp), %rsi
	movq	%r14, %rdi
	call	X509_SIG_get0@PLT
	movq	%r13, %rdi
	movq	-1088(%rbp), %rbx
	call	strlen@PLT
	movq	-1096(%rbp), %rdi
	leaq	-1080(%rbp), %r9
	movq	%r13, %rsi
	movq	%rax, %rdx
	leaq	-1100(%rbp), %rax
	movq	8(%rbx), %rcx
	pushq	$0
	pushq	%rax
	movl	(%rbx), %r8d
	call	PKCS12_pbe_crypt@PLT
	popq	%rdx
	popq	%rcx
	testq	%rax, %rax
	je	.L156
	movq	-1080(%rbp), %rax
	movq	%r14, %rdi
	movq	%rax, 8(%r12)
	movslq	-1100(%rbp), %rax
	movq	%rax, (%r12)
	movq	%rax, 16(%r12)
	call	X509_SIG_free@PLT
	movq	%r12, %rsi
	leaq	.LC2(%rip), %rdi
	call	ossl_store_info_new_EMBEDDED@PLT
	testq	%rax, %rax
	je	.L172
.L150:
	movq	-40(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L173
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L171:
	.cfi_restore_state
	movl	$360, %r8d
	movl	$115, %edx
	movl	$125, %esi
	movl	$44, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L156:
	movq	%r14, %rdi
	call	X509_SIG_free@PLT
	movq	%r12, %rdi
	call	BUF_MEM_free@PLT
.L170:
	xorl	%eax, %eax
	jmp	.L150
	.p2align 4,,10
	.p2align 3
.L172:
	movl	$376, %r8d
.L169:
	movl	$65, %edx
	movl	$125, %esi
	movl	$44, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L156
.L173:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE913:
	.size	try_decode_PKCS8Encrypted, .-try_decode_PKCS8Encrypted
	.p2align 4
	.type	destroy_ctx_PKCS12, @function
destroy_ctx_PKCS12:
.LFB912:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	OSSL_STORE_INFO_free@GOTPCREL(%rip), %rsi
	movq	(%rdi), %rdi
	call	OPENSSL_sk_pop_free@PLT
	movq	$0, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE912:
	.size	destroy_ctx_PKCS12, .-destroy_ctx_PKCS12
	.section	.rodata.str1.1
.LC11:
	.string	""
.LC12:
	.string	"PKCS12 import password"
	.text
	.p2align 4
	.type	try_decode_PKCS12, @function
try_decode_PKCS12:
.LFB910:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%r9, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$1112, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%r8), %r12
	movq	16(%rbp), %r15
	movq	%rdx, -1128(%rbp)
	movq	24(%rbp), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%r12, %r12
	je	.L209
.L177:
	movl	$1, (%r14)
	movq	%r12, %rdi
	call	OPENSSL_sk_shift@PLT
	movq	%rax, %r12
.L176:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L210
	addq	$1112, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L209:
	.cfi_restore_state
	testq	%rdi, %rdi
	jne	.L176
	leaq	-1128(%rbp), %rsi
	movq	%rcx, %rdx
	movq	%r8, %r13
	call	d2i_PKCS12@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L179
	movl	$1, (%r14)
	xorl	%edx, %edx
	leaq	.LC11(%rip), %rsi
	movq	%rax, %rdi
	movq	$0, -1112(%rbp)
	movq	$0, -1104(%rbp)
	movq	$0, -1096(%rbp)
	call	PKCS12_verify_mac@PLT
	testl	%eax, %eax
	jne	.L180
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	PKCS12_verify_mac@PLT
	testl	%eax, %eax
	jne	.L180
	movq	%r15, %rdi
	movq	%rbx, %r8
	leaq	-1088(%rbp), %rsi
	movl	$1024, %edx
	leaq	.LC12(%rip), %rcx
	call	file_get_pass
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L211
	movq	%rax, %rdi
	call	strlen@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	PKCS12_verify_mac@PLT
	testl	%eax, %eax
	jne	.L183
	movl	$236, %r8d
	movl	$113, %edx
	movl	$122, %esi
	movl	$44, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	.p2align 4,,10
	.p2align 3
.L207:
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	PKCS12_free@PLT
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L180:
	leaq	.LC11(%rip), %r15
.L183:
	leaq	-1104(%rbp), %rcx
	leaq	-1112(%rbp), %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	leaq	-1096(%rbp), %r8
	call	PKCS12_parse@PLT
	testl	%eax, %eax
	je	.L207
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L184
	movq	-1112(%rbp), %rdi
	call	OSSL_STORE_INFO_new_PKEY@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L184
	movq	%rax, %rsi
	movq	%r15, %rdi
	call	OPENSSL_sk_push@PLT
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	testl	%eax, %eax
	jne	.L212
	.p2align 4,,10
	.p2align 3
.L185:
	movq	%r8, %rdi
	movq	%r9, -1136(%rbp)
	call	OSSL_STORE_INFO_free@PLT
	movq	-1136(%rbp), %r9
	movq	%r9, %rdi
	call	OSSL_STORE_INFO_free@PLT
	movq	%rbx, %rdi
	call	OSSL_STORE_INFO_free@PLT
	movq	OSSL_STORE_INFO_free@GOTPCREL(%rip), %rsi
	movq	%r15, %rdi
	call	OPENSSL_sk_pop_free@PLT
	movq	-1112(%rbp), %rdi
	call	EVP_PKEY_free@PLT
	movq	-1104(%rbp), %rdi
	call	X509_free@PLT
	movq	X509_free@GOTPCREL(%rip), %rsi
	movq	-1096(%rbp), %rdi
	call	OPENSSL_sk_pop_free@PLT
	movq	$0, 0(%r13)
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	PKCS12_free@PLT
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L179:
	xorl	%edi, %edi
	call	PKCS12_free@PLT
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L184:
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	xorl	%ebx, %ebx
	jmp	.L185
.L211:
	movl	$231, %r8d
	movl	$114, %edx
	movl	$122, %esi
	movl	$44, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L207
.L212:
	movq	-1104(%rbp), %rdi
	movq	%r8, -1136(%rbp)
	call	OSSL_STORE_INFO_new_CERT@PLT
	movq	%rax, %r9
	testq	%rax, %rax
	je	.L193
	movq	%rax, %rsi
	movq	%r15, %rdi
	movq	%rax, -1144(%rbp)
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	jne	.L186
	movq	-1144(%rbp), %r9
	movq	-1136(%rbp), %r8
	jmp	.L185
.L187:
	movq	-1096(%rbp), %rdi
	xorl	%esi, %esi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %rdi
	call	OSSL_STORE_INFO_new_CERT@PLT
	testq	%rax, %rax
	je	.L184
	movq	%rax, %rsi
	movq	%r15, %rdi
	movq	%rax, -1136(%rbp)
	call	OPENSSL_sk_push@PLT
	movq	-1136(%rbp), %r8
	testl	%eax, %eax
	je	.L194
	movq	-1096(%rbp), %rdi
	call	OPENSSL_sk_shift@PLT
.L186:
	movq	-1096(%rbp), %rdi
	call	OPENSSL_sk_num@PLT
	testl	%eax, %eax
	jg	.L187
	movq	%r15, 0(%r13)
	movq	%r12, %rdi
	movq	%r15, %r12
	call	PKCS12_free@PLT
	jmp	.L177
.L193:
	movq	%rax, %r8
	jmp	.L185
.L210:
	call	__stack_chk_fail@PLT
.L194:
	xorl	%r9d, %r9d
	xorl	%ebx, %ebx
	jmp	.L185
	.cfi_endproc
.LFE910:
	.size	try_decode_PKCS12, .-try_decode_PKCS12
	.section	.rodata.str1.1
.LC13:
	.string	"file:"
.LC14:
	.string	"localhost/"
.LC15:
	.string	"rb"
.LC16:
	.string	"-----BEGIN "
	.text
	.p2align 4
	.type	file_open, @function
file_open:
.LFB920:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$216, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	$5, %edx
	movq	%rsi, -4344(%rbp)
	movq	%rsi, %r14
	movq	%r14, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rsi, -4336(%rbp)
	leaq	.LC13(%rip), %rsi
	andb	$-2, -4328(%rbp)
	call	strncasecmp@PLT
	testl	%eax, %eax
	jne	.L237
	cmpb	$47, 5(%r14)
	leaq	5(%r14), %rbx
	je	.L262
.L238:
	movl	$2, %r14d
	movl	$1, %eax
.L216:
	salq	$4, %rax
	orb	$1, -4328(%rbp,%rax)
	movq	%rbx, -4336(%rbp,%rax)
	jmp	.L214
	.p2align 4,,10
	.p2align 3
.L237:
	movl	$1, %r14d
.L214:
	xorl	%ebx, %ebx
	leaq	-4336(%rbp), %r12
	leaq	-4304(%rbp), %r13
	.p2align 4,,10
	.p2align 3
.L221:
	movq	%rbx, %rax
	salq	$4, %rax
	testb	$1, -4328(%rbp,%rax)
	je	.L219
	movq	(%r12,%rax), %r15
	cmpb	$47, (%r15)
	jne	.L263
.L219:
	movq	%rbx, %rax
	movq	%r13, %rdx
	movl	$1, %edi
	salq	$4, %rax
	movq	(%r12,%rax), %r15
	movq	%r15, %rsi
	call	__xstat@PLT
	testl	%eax, %eax
	js	.L264
	call	ERR_clear_error@PLT
	movl	$840, %edx
	movl	$80, %edi
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_zalloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L265
	movl	-4280(%rbp), %eax
	andl	$61440, %eax
	cmpl	$16384, %eax
	je	.L266
	pxor	%xmm0, %xmm0
	xorl	%eax, %eax
	movl	$510, %ecx
	leaq	-4144(%rbp), %rdi
	movaps	%xmm0, -4160(%rbp)
	rep stosq
	call	BIO_f_buffer@PLT
	movq	%rax, %rdi
	call	BIO_new@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L228
	leaq	.LC15(%rip), %rsi
	movq	%r15, %rdi
	call	BIO_new_file@PLT
	movq	%rax, 16(%r12)
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L228
	movq	%r13, %rdi
	leaq	-4160(%rbp), %r13
	call	BIO_push@PLT
	movq	%r13, %rcx
	movl	$4095, %edx
	movl	$29, %esi
	movq	%rax, 16(%r12)
	movq	%rax, %rdi
	call	BIO_ctrl@PLT
	testq	%rax, %rax
	jle	.L213
	leaq	.LC16(%rip), %rsi
	movq	%r13, %rdi
	movb	$0, -65(%rbp)
	call	strstr@PLT
	testq	%rax, %rax
	je	.L213
	movl	$1, (%r12)
	.p2align 4,,10
	.p2align 3
.L213:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L267
	addq	$4312, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L264:
	.cfi_restore_state
	call	__errno_location@PLT
	movl	$827, %r8d
	movl	$22, %esi
	leaq	.LC0(%rip), %rcx
	movl	(%rax), %edx
	movl	$2, %edi
	addq	$1, %rbx
	call	ERR_put_error@PLT
	movq	%r15, %rsi
	movl	$1, %edi
	xorl	%eax, %eax
	call	ERR_add_error_data@PLT
	cmpq	%rbx, %r14
	jne	.L221
	xorl	%r12d, %r12d
	jmp	.L213
	.p2align 4,,10
	.p2align 3
.L262:
	cmpb	$47, 1(%rbx)
	jne	.L238
	leaq	7(%r14), %rbx
	movl	$10, %edx
	leaq	.LC14(%rip), %rsi
	movq	%rbx, %rdi
	call	strncasecmp@PLT
	testl	%eax, %eax
	jne	.L217
	leaq	16(%r14), %rbx
	xorl	%eax, %eax
	movl	$1, %r14d
	jmp	.L216
	.p2align 4,,10
	.p2align 3
.L263:
	movl	$820, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$108, %edx
	xorl	%r12d, %r12d
	movl	$120, %esi
	movl	$44, %edi
	call	ERR_put_error@PLT
	movq	%r15, %rsi
	movl	$1, %edi
	xorl	%eax, %eax
	call	ERR_add_error_data@PLT
	jmp	.L213
	.p2align 4,,10
	.p2align 3
.L266:
	movq	-4344(%rbp), %rdi
	movl	$852, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_strdup@PLT
	movl	$2, (%r12)
	movq	%rax, 32(%r12)
	testq	%rax, %rax
	je	.L223
	leaq	16(%r12), %rdi
	movq	%r15, %rsi
	call	OPENSSL_DIR_read@PLT
	movq	%rax, 56(%r12)
	movq	%rax, %rbx
	call	__errno_location@PLT
	movq	%rax, %r13
	movl	(%rax), %eax
	movl	%eax, 64(%r12)
	testq	%rbx, %rbx
	jne	.L213
	testl	%eax, %eax
	jne	.L268
	movl	$1, 24(%r12)
	jmp	.L213
	.p2align 4,,10
	.p2align 3
.L217:
	movq	%r14, %rcx
	xorl	%eax, %eax
	movl	$1, %r14d
	cmpb	$47, 7(%rcx)
	je	.L216
	movl	$791, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$111, %edx
	xorl	%r12d, %r12d
	movl	$120, %esi
	movl	$44, %edi
	call	ERR_put_error@PLT
	jmp	.L213
.L228:
	movq	%r13, %rdi
	call	BIO_free_all@PLT
.L226:
	cmpl	$2, (%r12)
	je	.L269
	movq	24(%r12), %rax
	testq	%rax, %rax
	je	.L233
	leaq	32(%r12), %rdi
	call	*24(%rax)
	pxor	%xmm0, %xmm0
	movups	%xmm0, 24(%r12)
.L233:
	movq	%r12, %rdi
	movl	$752, %edx
	leaq	.LC0(%rip), %rsi
	xorl	%r12d, %r12d
	call	CRYPTO_free@PLT
	jmp	.L213
.L265:
	movl	$842, %r8d
	movl	$65, %edx
	movl	$120, %esi
	movl	$44, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L213
.L269:
	movq	32(%r12), %rax
.L223:
	movl	$744, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rax, %rdi
	call	CRYPTO_free@PLT
	jmp	.L233
.L268:
	movl	$2, %edx
	movl	$120, %esi
	movl	$44, %edi
	movl	$863, %r8d
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	64(%r12), %edi
	movl	$256, %edx
	movl	%edi, 0(%r13)
	leaq	-4160(%rbp), %r13
	movq	%r13, %rsi
	call	openssl_strerror_r@PLT
	testl	%eax, %eax
	je	.L226
	movq	%r13, %rsi
	movl	$1, %edi
	xorl	%eax, %eax
	call	ERR_add_error_data@PLT
	jmp	.L226
.L267:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE920:
	.size	file_open, .-file_open
	.p2align 4
	.type	file_ctrl, @function
file_ctrl:
.LFB921:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	cmpl	$1, %esi
	je	.L283
	ret
	.p2align 4,,10
	.p2align 3
.L283:
	movl	(%rdx), %eax
	cmpl	$47, %eax
	ja	.L272
	movl	%eax, %ecx
	addl	$8, %eax
	addq	16(%rdx), %rcx
	movl	%eax, (%rdx)
	movq	(%rcx), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	je	.L274
.L285:
	cmpl	$1, %eax
	jne	.L284
	orl	$1, 8(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L272:
	movq	8(%rdx), %rcx
	leaq	8(%rcx), %rax
	movq	%rax, 8(%rdx)
	movq	(%rcx), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L285
.L274:
	andl	$-2, 8(%rdi)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L284:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$7, %edx
	movl	$129, %esi
	movl	$44, %edi
	movl	$912, %r8d
	leaq	.LC0(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE921:
	.size	file_ctrl, .-file_ctrl
	.p2align 4
	.type	file_eof, @function
file_eof:
.LFB935:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpl	$2, (%rdi)
	je	.L292
	movq	24(%rdi), %rax
	testq	%rax, %rax
	je	.L290
	movq	32(%rdi), %rdi
	call	*16(%rax)
	testl	%eax, %eax
	je	.L286
.L290:
	movq	16(%rbx), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$2, %esi
	call	BIO_ctrl@PLT
.L286:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L292:
	.cfi_restore_state
	movl	24(%rdi), %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE935:
	.size	file_eof, .-file_eof
	.p2align 4
	.type	eof_PKCS12, @function
eof_PKCS12:
.LFB911:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L297
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	OPENSSL_sk_num@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	testl	%eax, %eax
	sete	%al
	movzbl	%al, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L297:
	.cfi_restore 6
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE911:
	.size	eof_PKCS12, .-eof_PKCS12
	.section	.rodata.str1.1
.LC17:
	.string	"%08lx"
	.text
	.p2align 4
	.type	file_find, @function
file_find:
.LFB923:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$8, %rsp
	call	OSSL_STORE_SEARCH_get_type@PLT
	cmpl	$1, %eax
	je	.L312
	xorl	%r12d, %r12d
	testq	%rbx, %rbx
	je	.L302
	movl	$958, %r8d
	movl	$120, %edx
	movl	$138, %esi
	movl	$44, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L302:
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L312:
	.cfi_restore_state
	movl	%eax, %r12d
	testq	%rbx, %rbx
	je	.L302
	cmpl	$2, (%rbx)
	je	.L305
	movl	$946, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$119, %edx
	xorl	%r12d, %r12d
	movl	$138, %esi
	movl	$44, %edi
	call	ERR_put_error@PLT
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L305:
	.cfi_restore_state
	movq	%r13, %rdi
	call	OSSL_STORE_SEARCH_get0_name@PLT
	movq	%rax, %rdi
	call	X509_NAME_hash@PLT
	leaq	40(%rbx), %rdi
	movl	$9, %esi
	leaq	.LC17(%rip), %rdx
	movq	%rax, %rcx
	xorl	%eax, %eax
	call	BIO_snprintf@PLT
	jmp	.L302
	.cfi_endproc
.LFE923:
	.size	file_find, .-file_find
	.p2align 4
	.type	file_close, @function
file_close:
.LFB936:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	cmpl	$2, (%rdi)
	je	.L322
	movq	16(%rdi), %rdi
	call	BIO_free_all@PLT
	cmpl	$2, (%r12)
	je	.L323
.L316:
	movq	24(%r12), %rax
	testq	%rax, %rax
	je	.L317
	leaq	32(%r12), %rdi
	call	*24(%rax)
	pxor	%xmm0, %xmm0
	movups	%xmm0, 24(%r12)
.L317:
	movq	%r12, %rdi
	movl	$752, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	addq	$8, %rsp
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L322:
	.cfi_restore_state
	leaq	16(%rdi), %rdi
	call	OPENSSL_DIR_end@PLT
	cmpl	$2, (%r12)
	jne	.L316
.L323:
	movq	32(%r12), %rdi
	movl	$744, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	jmp	.L317
	.cfi_endproc
.LFE936:
	.size	file_close, .-file_close
	.section	.rodata.str1.1
.LC18:
	.string	"/"
.LC19:
	.string	"PEM"
.LC20:
	.string	"'"
.LC21:
	.string	"PEM type is '"
	.text
	.p2align 4
	.type	file_load, @function
file_load:
.LFB933:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%r14, %rbx
	subq	$504, %rsp
	movq	%rsi, -528(%rbp)
	movq	%rdx, -536(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$0, 4(%rdi)
	call	ERR_clear_error@PLT
	leaq	-392(%rbp), %rax
	cmpl	$2, (%r14)
	movq	%rax, -488(%rbp)
	je	.L469
	.p2align 4,,10
	.p2align 3
.L325:
	movl	$0, -384(%rbp)
	movq	24(%rbx), %rax
	testq	%rax, %rax
	je	.L353
	leaq	32(%rbx), %r12
	pushq	-536(%rbp)
	xorl	%edi, %edi
	xorl	%ecx, %ecx
	pushq	-528(%rbp)
	movq	%r12, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	leaq	-384(%rbp), %r9
	call	*8(%rax)
	popq	%rdi
	popq	%r8
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L470
.L324:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L471
	leaq	-40(%rbp), %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L470:
	.cfi_restore_state
	movq	24(%rbx), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	pxor	%xmm0, %xmm0
	movups	%xmm0, 24(%rbx)
.L353:
	cmpl	$2, (%rbx)
	jne	.L472
	movl	24(%rbx), %eax
.L397:
	testl	%eax, %eax
	jne	.L468
	xorl	%r15d, %r15d
	movq	%rbx, %r13
	.p2align 4,,10
	.p2align 3
.L388:
	cmpl	$1, 0(%r13)
	movq	16(%r13), %rdi
	movq	$0, -424(%rbp)
	movq	$0, -416(%rbp)
	movq	$0, -408(%rbp)
	movq	$0, -400(%rbp)
	je	.L473
	leaq	-384(%rbp), %rsi
	movq	$0, -384(%rbp)
	call	asn1_d2i_read_bio@PLT
	testl	%eax, %eax
	js	.L362
	movq	-384(%rbp), %rdi
	movl	$1141, %edx
	leaq	.LC0(%rip), %rsi
	movq	8(%rdi), %rax
	movq	%rax, -408(%rbp)
	movq	(%rdi), %rax
	movq	%rax, -400(%rbp)
	call	CRYPTO_free@PLT
.L465:
	movq	-416(%rbp), %rax
	movq	%rax, -512(%rbp)
.L358:
	movq	-400(%rbp), %rax
	movq	%r13, -456(%rbp)
	movl	$-1, %ebx
	xorl	%r14d, %r14d
	movq	$0, -448(%rbp)
	movq	%rax, -480(%rbp)
	movq	-408(%rbp), %rax
	movq	%rax, -472(%rbp)
	movq	-424(%rbp), %rax
	movq	%rax, -464(%rbp)
	leaq	-384(%rbp), %rax
	movq	%rax, -520(%rbp)
	.p2align 4,,10
	.p2align 3
.L363:
	movl	$997, %edx
	movl	$56, %edi
	leaq	.LC0(%rip), %rsi
	movq	$0, -392(%rbp)
	call	CRYPTO_zalloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L474
	leaq	file_handlers(%rip), %rax
	xorl	%r13d, %r13d
	xorl	%ebx, %ebx
	movq	%r14, -504(%rbp)
	movq	%rax, -440(%rbp)
	leaq	-428(%rbp), %rax
	movq	%r13, %r14
	movq	%r12, %r13
	movq	%rax, -496(%rbp)
	movl	%ebx, %r12d
	jmp	.L370
	.p2align 4,,10
	.p2align 3
.L368:
	testq	%r14, %r14
	cmove	%r15, %r14
.L366:
	addq	$8, -440(%rbp)
	movq	-440(%rbp), %rax
	leaq	56+file_handlers(%rip), %rcx
	cmpq	%rcx, %rax
	je	.L475
.L370:
	movq	-440(%rbp), %rax
	movq	-480(%rbp), %rcx
	movl	$0, -428(%rbp)
	movq	-512(%rbp), %rsi
	movq	-496(%rbp), %r9
	movq	$0, -384(%rbp)
	movq	(%rax), %rbx
	movq	-520(%rbp), %r8
	pushq	-536(%rbp)
	movq	-472(%rbp), %rdx
	pushq	-528(%rbp)
	movq	-464(%rbp), %rdi
	call	*8(%rbx)
	popq	%rcx
	popq	%rsi
	movq	%rax, %r15
	movl	-428(%rbp), %eax
	testl	%eax, %eax
	jle	.L366
	movslq	%r12d, %rdx
	movq	%rbx, 0(%r13,%rdx,8)
	cmpq	$0, -392(%rbp)
	je	.L367
	movq	-488(%rbp), %rdi
	call	*24(%rbx)
	movl	-428(%rbp), %eax
.L367:
	movq	-384(%rbp), %rdx
	addl	%eax, %r12d
	movq	%rdx, -392(%rbp)
	cmpl	$1, %r12d
	jle	.L368
	movq	%r14, %rdi
	call	OSSL_STORE_INFO_free@PLT
	movq	%r15, %rdi
	call	OSSL_STORE_INFO_free@PLT
	movq	24(%rbx), %rax
	testq	%rax, %rax
	je	.L369
	movq	-488(%rbp), %rdi
	call	*%rax
.L369:
	movq	$0, -392(%rbp)
	xorl	%r14d, %r14d
	jmp	.L366
	.p2align 4,,10
	.p2align 3
.L475:
	movl	%r12d, %ebx
	movq	%r13, %r12
	movq	%r14, %r13
	movq	-504(%rbp), %r14
	cmpl	$1, %ebx
	je	.L476
.L371:
	movl	$1044, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
	movq	-448(%rbp), %rdi
	movl	$1048, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%r14, %rdi
	call	BUF_MEM_free@PLT
	testq	%r13, %r13
	je	.L460
	movq	%r13, %rdi
	call	OSSL_STORE_INFO_get_type@PLT
	cmpl	$-1, %eax
	jne	.L372
	movq	%r13, %rdi
	call	ossl_store_info_get0_EMBEDDED_pem_name@PLT
	movq	%r13, %rdi
	movq	%rax, %r15
	movq	%rax, -464(%rbp)
	call	ossl_store_info_get0_EMBEDDED_buffer@PLT
	movl	$1058, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r13, %rdi
	movq	%rax, %r14
	movq	8(%rax), %rax
	movq	%rax, -472(%rbp)
	movq	(%r14), %rax
	movq	%rax, -480(%rbp)
	call	CRYPTO_free@PLT
	movq	%r15, -448(%rbp)
	jmp	.L363
	.p2align 4,,10
	.p2align 3
.L474:
	movl	$1001, %r8d
	movl	$65, %edx
	movl	$124, %esi
	movl	$44, %edi
	leaq	.LC0(%rip), %rcx
	movq	-456(%rbp), %r13
	call	ERR_put_error@PLT
	movq	-448(%rbp), %rdi
	movl	$1048, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%r14, %rdi
	call	BUF_MEM_free@PLT
.L365:
	movq	-424(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L477
	cmpl	$1, %ebx
	jg	.L374
.L373:
	cmpl	$1, %ebx
	je	.L400
.L466:
	xorl	%r15d, %r15d
.L361:
	testb	$1, 8(%r13)
	je	.L377
	movl	$1095, %ecx
	leaq	.LC0(%rip), %rdx
	xorl	%esi, %esi
	call	CRYPTO_secure_clear_free@PLT
.L378:
	movq	-416(%rbp), %rdi
	testb	$1, 8(%r13)
	je	.L379
	movl	$1095, %ecx
	leaq	.LC0(%rip), %rdx
	xorl	%esi, %esi
	call	CRYPTO_secure_clear_free@PLT
.L380:
	movq	-408(%rbp), %rdi
	testb	$1, 8(%r13)
	je	.L381
	movq	-400(%rbp), %rsi
	movl	$1095, %ecx
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_secure_clear_free@PLT
	testl	%ebx, %ebx
	jne	.L383
.L479:
	cmpl	$2, 0(%r13)
	je	.L478
	movq	24(%r13), %rax
	testq	%rax, %rax
	je	.L387
	movq	32(%r13), %rdi
	call	*16(%rax)
	testl	%eax, %eax
	je	.L390
.L387:
	movq	16(%r13), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$2, %esi
	call	BIO_ctrl@PLT
.L385:
	testl	%eax, %eax
	jne	.L467
.L390:
	movl	4(%r13), %eax
	testl	%eax, %eax
	jle	.L388
.L467:
	movq	%r13, %rbx
.L389:
	testq	%r15, %r15
	je	.L468
	movl	72(%rbx), %r12d
	testl	%r12d, %r12d
	je	.L324
	movq	%r15, %rdi
	call	OSSL_STORE_INFO_get_type@PLT
	cmpl	%eax, %r12d
	je	.L324
	movq	%r15, %rdi
	call	OSSL_STORE_INFO_free@PLT
	jmp	.L325
	.p2align 4,,10
	.p2align 3
.L476:
	movq	(%r12), %rax
	movl	32(%rax), %edx
	testl	%edx, %edx
	je	.L371
	movq	-456(%rbp), %rcx
	movq	%rax, 24(%rcx)
	movq	-392(%rbp), %rax
	movq	%rax, 32(%rcx)
	jmp	.L371
	.p2align 4,,10
	.p2align 3
.L381:
	movl	$1097, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	testl	%ebx, %ebx
	je	.L479
.L383:
	movl	%ebx, %r14d
	movq	%r13, %rbx
	cmpl	$1, %r14d
	jle	.L389
.L468:
	xorl	%r15d, %r15d
	jmp	.L324
	.p2align 4,,10
	.p2align 3
.L379:
	movl	$1097, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	jmp	.L380
	.p2align 4,,10
	.p2align 3
.L377:
	movl	$1097, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	jmp	.L378
	.p2align 4,,10
	.p2align 3
.L473:
	leaq	-400(%rbp), %r12
	leaq	-408(%rbp), %rcx
	leaq	-416(%rbp), %rdx
	leaq	-424(%rbp), %rsi
	testb	$1, 8(%r13)
	je	.L355
	movl	$3, %r9d
	movq	%r12, %r8
	call	PEM_read_bio_ex@PLT
.L356:
	testl	%eax, %eax
	jle	.L362
	movq	-416(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -512(%rbp)
	call	strlen@PLT
	cmpq	$10, %rax
	jbe	.L358
	leaq	-352(%rbp), %r14
	movq	-512(%rbp), %rdi
	movq	%r14, %rsi
	call	PEM_get_EVP_CIPHER_INFO@PLT
	testl	%eax, %eax
	je	.L362
	movq	-408(%rbp), %rsi
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	-528(%rbp), %xmm0
	leaq	.LC19(%rip), %rax
	leaq	-384(%rbp), %r8
	movhps	-536(%rbp), %xmm0
	leaq	file_get_pem_pass(%rip), %rcx
	movq	%rax, -368(%rbp)
	movaps	%xmm0, -384(%rbp)
	call	PEM_do_header@PLT
	testl	%eax, %eax
	jne	.L465
	.p2align 4,,10
	.p2align 3
.L362:
	addl	$1, 4(%r13)
	movq	-424(%rbp), %rdi
	movl	$-1, %ebx
	jmp	.L361
	.p2align 4,,10
	.p2align 3
.L372:
	movq	%r13, %r15
	movq	-456(%rbp), %r13
	call	ERR_clear_error@PLT
	movq	-424(%rbp), %rdi
	jmp	.L361
	.p2align 4,,10
	.p2align 3
.L400:
	call	ERR_peek_error@PLT
	movq	-424(%rbp), %rdi
	testq	%rax, %rax
	jne	.L374
	movl	$44, %edi
	movl	$1352, %r8d
	movl	$110, %edx
	movl	$119, %esi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	-424(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L374
	movq	%rdi, %rdx
	leaq	.LC20(%rip), %rcx
	movl	$3, %edi
	xorl	%eax, %eax
	leaq	.LC21(%rip), %rsi
	call	ERR_add_error_data@PLT
	movq	-424(%rbp), %rdi
	.p2align 4,,10
	.p2align 3
.L374:
	addl	$1, 4(%r13)
	jmp	.L466
	.p2align 4,,10
	.p2align 3
.L478:
	movl	24(%r13), %eax
	jmp	.L385
	.p2align 4,,10
	.p2align 3
.L355:
	movq	%r12, %r8
	call	PEM_read_bio@PLT
	jmp	.L356
	.p2align 4,,10
	.p2align 3
.L477:
	cmpl	$1, %ebx
	jle	.L373
	movl	$44, %edi
	movl	$1344, %r8d
	movl	$107, %edx
	movl	$119, %esi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addl	$1, 4(%r13)
	movq	-424(%rbp), %rdi
	jmp	.L466
	.p2align 4,,10
	.p2align 3
.L472:
	movq	16(%rbx), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$2, %esi
	call	BIO_ctrl@PLT
	jmp	.L397
	.p2align 4,,10
	.p2align 3
.L460:
	movq	-456(%rbp), %r13
	jmp	.L365
.L469:
	movq	56(%r14), %r12
	leaq	40(%r14), %r13
	testq	%r12, %r12
	je	.L329
	.p2align 4,,10
	.p2align 3
.L326:
	cmpb	$46, (%r12)
	je	.L332
	cmpb	$0, 40(%r14)
	je	.L333
	movl	72(%r14), %ebx
	testl	%ebx, %ebx
	leal	-4(%rbx), %eax
	setne	%r15b
	cmpl	$1, %eax
	jbe	.L403
	testb	%r15b, %r15b
	jne	.L332
.L403:
	movl	$8, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	strncasecmp@PLT
	testl	%eax, %eax
	jne	.L332
	cmpb	$46, 8(%r12)
	jne	.L332
	movzbl	9(%r12), %eax
	cmpb	$114, %al
	je	.L480
	cmpl	$5, %ebx
	je	.L332
	leaq	9(%r12), %rbx
.L337:
	movsbl	%al, %edi
	movl	$4, %esi
	call	ossl_ctype_check@PLT
	testl	%eax, %eax
	jne	.L338
	jmp	.L332
	.p2align 4,,10
	.p2align 3
.L339:
	addq	$1, %rbx
.L338:
	movsbl	(%rbx), %edi
	movl	$4, %esi
	call	ossl_ctype_check@PLT
	testl	%eax, %eax
	jne	.L339
	cmpb	$0, (%rbx)
	jne	.L332
	movq	56(%r14), %r12
	.p2align 4,,10
	.p2align 3
.L333:
	movq	32(%r14), %rbx
	movq	%rbx, %rdi
	call	strlen@PLT
	cmpb	$0, (%rbx)
	leaq	1(%rax), %r13
	je	.L464
	cmpb	$47, -1(%rbx,%rax)
	leaq	.LC11(%rip), %r15
	jne	.L464
.L341:
	movq	%r12, %rdi
	call	strlen@PLT
	movl	$1170, %edx
	leaq	.LC0(%rip), %rsi
	leaq	(%rax,%r13), %rbx
	movq	%rbx, %rdi
	call	CRYPTO_zalloc@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L481
	movq	32(%r14), %rsi
	movq	%rbx, %rdx
	movq	%rax, %rdi
	call	OPENSSL_strlcat@PLT
	movq	%rbx, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	OPENSSL_strlcat@PLT
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	OPENSSL_strlcat@PLT
	movq	32(%r14), %rsi
	leaq	16(%r14), %rdi
	call	OPENSSL_DIR_read@PLT
	movq	%rax, 56(%r14)
	movq	%rax, %rbx
	call	__errno_location@PLT
	movl	(%rax), %eax
	movl	%eax, 64(%r14)
	testq	%rbx, %rbx
	je	.L482
.L344:
	movq	%r13, %rdi
	call	OSSL_STORE_INFO_new_NAME@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	jne	.L324
	movq	%r13, %rdi
	movl	$1291, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movl	$1292, %r8d
	movl	$44, %edx
	leaq	.LC0(%rip), %rcx
	movl	$119, %esi
	movl	$44, %edi
	call	ERR_put_error@PLT
	jmp	.L324
	.p2align 4,,10
	.p2align 3
.L332:
	movq	32(%r14), %rsi
	leaq	16(%r14), %rdi
	call	OPENSSL_DIR_read@PLT
	movq	%rax, 56(%r14)
	movq	%rax, %rbx
	call	__errno_location@PLT
	movl	(%rax), %eax
	movl	%eax, 64(%r14)
	testq	%rbx, %rbx
	je	.L483
.L345:
	cmpl	$2, (%r14)
	je	.L484
.L346:
	movq	24(%r14), %rax
	testq	%rax, %rax
	je	.L350
	movq	32(%r14), %rdi
	call	*16(%rax)
	testl	%eax, %eax
	je	.L349
.L350:
	movq	16(%r14), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$2, %esi
	call	BIO_ctrl@PLT
.L347:
	testl	%eax, %eax
	jne	.L468
.L349:
	movq	56(%r14), %r12
	testq	%r12, %r12
	jne	.L326
.L329:
	movl	24(%r14), %r9d
	testl	%r9d, %r9d
	jne	.L468
	movl	$2, %edx
	movl	$119, %esi
	movl	$44, %edi
	movl	$1263, %r8d
	leaq	.LC0(%rip), %rcx
	leaq	-320(%rbp), %r12
	call	ERR_put_error@PLT
	call	__errno_location@PLT
	movl	64(%r14), %edx
	movq	%r12, %rsi
	movl	%edx, (%rax)
	movl	$256, %edx
	addl	$1, 4(%r14)
	movl	(%rax), %edi
	call	openssl_strerror_r@PLT
	testl	%eax, %eax
	je	.L468
	movq	%r12, %rsi
	movl	$1, %edi
	xorl	%eax, %eax
	call	ERR_add_error_data@PLT
	jmp	.L468
	.p2align 4,,10
	.p2align 3
.L483:
	testl	%eax, %eax
	jne	.L345
	cmpl	$2, (%r14)
	movl	$1, 24(%r14)
	jne	.L346
.L484:
	movl	24(%r14), %eax
	jmp	.L347
	.p2align 4,,10
	.p2align 3
.L464:
	leaq	2(%rax), %r13
	leaq	.LC18(%rip), %r15
	jmp	.L341
.L481:
	movl	$1172, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r15d, %r15d
	movl	$126, %esi
	movl	$44, %edi
	call	ERR_put_error@PLT
	jmp	.L324
.L480:
	cmpl	$5, %ebx
	je	.L404
	testb	%r15b, %r15b
	jne	.L332
.L404:
	movzbl	10(%r12), %eax
	leaq	10(%r12), %rbx
	jmp	.L337
.L471:
	call	__stack_chk_fail@PLT
.L482:
	testl	%eax, %eax
	jne	.L344
	movl	$1, 24(%r14)
	jmp	.L344
	.cfi_endproc
.LFE933:
	.size	file_load, .-file_load
	.p2align 4
	.globl	ossl_store_file_attach_pem_bio_int
	.type	ossl_store_file_attach_pem_bio_int, @function
ossl_store_file_attach_pem_bio_int:
.LFB924:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$966, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$80, %edi
	subq	$24, %rsp
	call	CRYPTO_zalloc@PLT
	testq	%rax, %rax
	je	.L489
	movq	%rbx, 16(%rax)
	movl	$1, (%rax)
.L485:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L489:
	.cfi_restore_state
	movl	$969, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	movl	$128, %esi
	movl	$44, %edi
	movq	%rax, -24(%rbp)
	call	ERR_put_error@PLT
	movq	-24(%rbp), %rax
	jmp	.L485
	.cfi_endproc
.LFE924:
	.size	ossl_store_file_attach_pem_bio_int, .-ossl_store_file_attach_pem_bio_int
	.p2align 4
	.globl	ossl_store_file_detach_pem_bio_int
	.type	ossl_store_file_detach_pem_bio_int, @function
ossl_store_file_detach_pem_bio_int:
.LFB937:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	cmpl	$2, (%rdi)
	je	.L497
	movq	24(%rdi), %rax
	testq	%rax, %rax
	je	.L492
	leaq	32(%rdi), %rdi
	call	*24(%rax)
	pxor	%xmm0, %xmm0
	movups	%xmm0, 24(%r12)
.L492:
	movq	%r12, %rdi
	movl	$752, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	addq	$8, %rsp
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L497:
	.cfi_restore_state
	movq	32(%rdi), %rdi
	movl	$744, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	jmp	.L492
	.cfi_endproc
.LFE937:
	.size	ossl_store_file_detach_pem_bio_int, .-ossl_store_file_detach_pem_bio_int
	.p2align 4
	.globl	ossl_store_file_loader_init
	.type	ossl_store_file_loader_init, @function
ossl_store_file_loader_init:
.LFB939:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	file_loader(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	ossl_store_register_loader_int@PLT
	leaq	store_file_loader_deinit(%rip), %rdi
	movl	%eax, %r12d
	call	OPENSSL_atexit@PLT
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE939:
	.size	ossl_store_file_loader_init, .-ossl_store_file_loader_init
	.section	.rodata.str1.1
.LC22:
	.string	"file"
	.section	.data.rel.local,"aw"
	.align 32
	.type	file_loader, @object
	.size	file_loader, 80
file_loader:
	.quad	.LC22
	.quad	0
	.quad	file_open
	.quad	file_ctrl
	.quad	file_expect
	.quad	file_find
	.quad	file_load
	.quad	file_eof
	.quad	file_error
	.quad	file_close
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	file_handlers, @object
	.size	file_handlers, 56
file_handlers:
	.quad	PKCS12_handler
	.quad	PKCS8Encrypted_handler
	.quad	X509Certificate_handler
	.quad	X509CRL_handler
	.quad	params_handler
	.quad	PUBKEY_handler
	.quad	PrivateKey_handler
	.section	.rodata.str1.1
.LC23:
	.string	"X509CRL"
	.section	.data.rel.local
	.align 32
	.type	X509CRL_handler, @object
	.size	X509CRL_handler, 40
X509CRL_handler:
	.quad	.LC23
	.quad	try_decode_X509CRL
	.zero	24
	.section	.rodata.str1.1
.LC24:
	.string	"X509Certificate"
	.section	.data.rel.local
	.align 32
	.type	X509Certificate_handler, @object
	.size	X509Certificate_handler, 40
X509Certificate_handler:
	.quad	.LC24
	.quad	try_decode_X509Certificate
	.zero	24
	.section	.rodata.str1.1
.LC25:
	.string	"params"
	.section	.data.rel.local
	.align 32
	.type	params_handler, @object
	.size	params_handler, 40
params_handler:
	.quad	.LC25
	.quad	try_decode_params
	.zero	24
	.section	.rodata.str1.1
.LC26:
	.string	"PUBKEY"
	.section	.data.rel.local
	.align 32
	.type	PUBKEY_handler, @object
	.size	PUBKEY_handler, 40
PUBKEY_handler:
	.quad	.LC26
	.quad	try_decode_PUBKEY
	.zero	24
	.section	.rodata.str1.1
.LC27:
	.string	"PrivateKey"
	.section	.data.rel.local
	.align 32
	.type	PrivateKey_handler, @object
	.size	PrivateKey_handler, 40
PrivateKey_handler:
	.quad	.LC27
	.quad	try_decode_PrivateKey
	.zero	24
	.section	.rodata.str1.1
.LC28:
	.string	"PKCS8Encrypted"
	.section	.data.rel.local
	.align 32
	.type	PKCS8Encrypted_handler, @object
	.size	PKCS8Encrypted_handler, 40
PKCS8Encrypted_handler:
	.quad	.LC28
	.quad	try_decode_PKCS8Encrypted
	.zero	24
	.section	.rodata.str1.1
.LC29:
	.string	"PKCS12"
	.section	.data.rel.local
	.align 32
	.type	PKCS12_handler, @object
	.size	PKCS12_handler, 40
PKCS12_handler:
	.quad	.LC29
	.quad	try_decode_PKCS12
	.quad	eof_PKCS12
	.quad	destroy_ctx_PKCS12
	.long	1
	.zero	4
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
