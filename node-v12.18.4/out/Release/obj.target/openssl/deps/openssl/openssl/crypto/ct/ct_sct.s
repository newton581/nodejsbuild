	.file	"ct_sct.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/ct/ct_sct.c"
	.text
	.p2align 4
	.globl	SCT_free
	.type	SCT_free, @function
SCT_free:
.LFB1297:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L1
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$41, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	24(%rdi), %rdi
	call	CRYPTO_free@PLT
	movq	48(%r12), %rdi
	movl	$42, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	72(%r12), %rdi
	movl	$43, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	8(%r12), %rdi
	movl	$44, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$45, %edx
	popq	%r12
	.cfi_restore 12
	leaq	.LC0(%rip), %rsi
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_free@PLT
	.p2align 4,,10
	.p2align 3
.L1:
	ret
	.cfi_endproc
.LFE1297:
	.size	SCT_free, .-SCT_free
	.p2align 4
	.globl	SCT_new
	.type	SCT_new, @function
SCT_new:
.LFB1296:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$24, %edx
	movl	$104, %edi
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	call	CRYPTO_zalloc@PLT
	testq	%rax, %rax
	je	.L11
	movl	$-1, 88(%rax)
	movl	$-1, (%rax)
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	.cfi_restore_state
	movl	$27, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	movl	$100, %esi
	movl	$50, %edi
	movq	%rax, -8(%rbp)
	call	ERR_put_error@PLT
	movq	-8(%rbp), %rax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1296:
	.size	SCT_new, .-SCT_new
	.p2align 4
	.globl	SCT_LIST_free
	.type	SCT_LIST_free, @function
SCT_LIST_free:
.LFB1298:
	.cfi_startproc
	endbr64
	leaq	SCT_free(%rip), %rsi
	jmp	OPENSSL_sk_pop_free@PLT
	.cfi_endproc
.LFE1298:
	.size	SCT_LIST_free, .-SCT_LIST_free
	.p2align 4
	.globl	SCT_set_version
	.type	SCT_set_version, @function
SCT_set_version:
.LFB1299:
	.cfi_startproc
	endbr64
	testl	%esi, %esi
	jne	.L20
	movl	$0, (%rdi)
	movl	$1, %eax
	movl	$0, 96(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$103, %edx
	movl	$104, %esi
	movl	$50, %edi
	movl	$56, %r8d
	leaq	.LC0(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1299:
	.size	SCT_set_version, .-SCT_set_version
	.p2align 4
	.globl	SCT_set_log_entry_type
	.type	SCT_set_log_entry_type, @function
SCT_set_log_entry_type:
.LFB1300:
	.cfi_startproc
	endbr64
	movl	$0, 96(%rdi)
	cmpl	$1, %esi
	ja	.L22
	movl	%esi, 88(%rdi)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$102, %edx
	movl	$102, %esi
	movl	$50, %edi
	movl	$76, %r8d
	leaq	.LC0(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1300:
	.size	SCT_set_log_entry_type, .-SCT_set_log_entry_type
	.p2align 4
	.globl	SCT_set0_log_id
	.type	SCT_set0_log_id, @function
SCT_set0_log_id:
.LFB1301:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	(%rdi), %eax
	testl	%eax, %eax
	jne	.L28
	cmpq	$32, %rdx
	jne	.L37
.L28:
	movq	24(%rbx), %rdi
	movl	$87, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%r13, 24(%rbx)
	movl	$1, %eax
	movq	%r12, 32(%rbx)
	movl	$0, 96(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L37:
	.cfi_restore_state
	movl	$83, %r8d
	movl	$100, %edx
	movl	$101, %esi
	movl	$50, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1301:
	.size	SCT_set0_log_id, .-SCT_set0_log_id
	.p2align 4
	.globl	SCT_set1_log_id
	.type	SCT_set1_log_id, @function
SCT_set1_log_id:
.LFB1302:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	(%rdi), %eax
	testl	%eax, %eax
	jne	.L39
	cmpq	$32, %rdx
	jne	.L56
.L39:
	movq	24(%rbx), %rdi
	movl	$101, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	$0, 24(%rbx)
	movq	$0, 32(%rbx)
	movl	$0, 96(%rbx)
	testq	%r13, %r13
	je	.L42
	testq	%r12, %r12
	jne	.L57
.L42:
	movl	$1, %eax
.L38:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L57:
	.cfi_restore_state
	movl	$107, %ecx
	leaq	.LC0(%rip), %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	CRYPTO_memdup@PLT
	movq	%rax, 24(%rbx)
	testq	%rax, %rax
	je	.L58
	movq	%r12, 32(%rbx)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L56:
	.cfi_restore_state
	movl	$97, %r8d
	movl	$100, %edx
	movl	$115, %esi
	movl	$50, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L58:
	.cfi_restore_state
	movl	$109, %r8d
	movl	$65, %edx
	movl	$115, %esi
	movl	$50, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L38
	.cfi_endproc
.LFE1302:
	.size	SCT_set1_log_id, .-SCT_set1_log_id
	.p2align 4
	.globl	SCT_set_timestamp
	.type	SCT_set_timestamp, @function
SCT_set_timestamp:
.LFB1303:
	.cfi_startproc
	endbr64
	movq	%rsi, 40(%rdi)
	movl	$0, 96(%rdi)
	ret
	.cfi_endproc
.LFE1303:
	.size	SCT_set_timestamp, .-SCT_set_timestamp
	.p2align 4
	.globl	SCT_set_signature_nid
	.type	SCT_set_signature_nid, @function
SCT_set_signature_nid:
.LFB1304:
	.cfi_startproc
	endbr64
	cmpl	$668, %esi
	je	.L61
	cmpl	$794, %esi
	jne	.L70
	movl	$772, %eax
	movl	$0, 96(%rdi)
	movw	%ax, 64(%rdi)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L70:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$101, %edx
	movl	$103, %esi
	movl	$50, %edi
	movl	$138, %r8d
	leaq	.LC0(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L61:
	.cfi_restore 6
	movl	$260, %edx
	movl	$0, 96(%rdi)
	movl	$1, %eax
	movw	%dx, 64(%rdi)
	ret
	.cfi_endproc
.LFE1304:
	.size	SCT_set_signature_nid, .-SCT_set_signature_nid
	.p2align 4
	.globl	SCT_set0_extensions
	.type	SCT_set0_extensions, @function
SCT_set0_extensions:
.LFB1305:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	leaq	.LC0(%rip), %rsi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	movl	$145, %edx
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	48(%rdi), %rdi
	call	CRYPTO_free@PLT
	movq	%r13, 48(%rbx)
	movq	%r12, 56(%rbx)
	movl	$0, 96(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1305:
	.size	SCT_set0_extensions, .-SCT_set0_extensions
	.p2align 4
	.globl	SCT_set1_extensions
	.type	SCT_set1_extensions, @function
SCT_set1_extensions:
.LFB1306:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	movl	$153, %edx
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	leaq	.LC0(%rip), %rsi
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	48(%rdi), %rdi
	call	CRYPTO_free@PLT
	movq	$0, 48(%rbx)
	movq	$0, 56(%rbx)
	movl	$0, 96(%rbx)
	testq	%r12, %r12
	je	.L76
	testq	%r13, %r13
	jne	.L84
.L76:
	movl	$1, %eax
.L73:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L84:
	.cfi_restore_state
	movl	$159, %ecx
	leaq	.LC0(%rip), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	CRYPTO_memdup@PLT
	movq	%rax, 48(%rbx)
	testq	%rax, %rax
	je	.L85
	movq	%r13, 56(%rbx)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L85:
	.cfi_restore_state
	movl	$161, %r8d
	movl	$65, %edx
	movl	$114, %esi
	movl	$50, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L73
	.cfi_endproc
.LFE1306:
	.size	SCT_set1_extensions, .-SCT_set1_extensions
	.p2align 4
	.globl	SCT_set0_signature
	.type	SCT_set0_signature, @function
SCT_set0_signature:
.LFB1307:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	leaq	.LC0(%rip), %rsi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	movl	$171, %edx
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	72(%rdi), %rdi
	call	CRYPTO_free@PLT
	movq	%r13, 72(%rbx)
	movq	%r12, 80(%rbx)
	movl	$0, 96(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1307:
	.size	SCT_set0_signature, .-SCT_set0_signature
	.p2align 4
	.globl	SCT_set1_signature
	.type	SCT_set1_signature, @function
SCT_set1_signature:
.LFB1308:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	movl	$179, %edx
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	leaq	.LC0(%rip), %rsi
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	72(%rdi), %rdi
	call	CRYPTO_free@PLT
	movq	$0, 72(%rbx)
	movq	$0, 80(%rbx)
	movl	$0, 96(%rbx)
	testq	%r12, %r12
	je	.L91
	testq	%r13, %r13
	jne	.L99
.L91:
	movl	$1, %eax
.L88:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L99:
	.cfi_restore_state
	movl	$185, %ecx
	leaq	.LC0(%rip), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	CRYPTO_memdup@PLT
	movq	%rax, 72(%rbx)
	testq	%rax, %rax
	je	.L100
	movq	%r13, 80(%rbx)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L100:
	.cfi_restore_state
	movl	$187, %r8d
	movl	$65, %edx
	movl	$116, %esi
	movl	$50, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L88
	.cfi_endproc
.LFE1308:
	.size	SCT_set1_signature, .-SCT_set1_signature
	.p2align 4
	.globl	SCT_get_version
	.type	SCT_get_version, @function
SCT_get_version:
.LFB1309:
	.cfi_startproc
	endbr64
	movl	(%rdi), %eax
	ret
	.cfi_endproc
.LFE1309:
	.size	SCT_get_version, .-SCT_get_version
	.p2align 4
	.globl	SCT_get_log_entry_type
	.type	SCT_get_log_entry_type, @function
SCT_get_log_entry_type:
.LFB1310:
	.cfi_startproc
	endbr64
	movl	88(%rdi), %eax
	ret
	.cfi_endproc
.LFE1310:
	.size	SCT_get_log_entry_type, .-SCT_get_log_entry_type
	.p2align 4
	.globl	SCT_get0_log_id
	.type	SCT_get0_log_id, @function
SCT_get0_log_id:
.LFB1311:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	movq	%rax, (%rsi)
	movq	32(%rdi), %rax
	ret
	.cfi_endproc
.LFE1311:
	.size	SCT_get0_log_id, .-SCT_get0_log_id
	.p2align 4
	.globl	SCT_get_timestamp
	.type	SCT_get_timestamp, @function
SCT_get_timestamp:
.LFB1312:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	ret
	.cfi_endproc
.LFE1312:
	.size	SCT_get_timestamp, .-SCT_get_timestamp
	.p2align 4
	.globl	SCT_get_signature_nid
	.type	SCT_get_signature_nid, @function
SCT_get_signature_nid:
.LFB1313:
	.cfi_startproc
	endbr64
	movl	(%rdi), %eax
	testl	%eax, %eax
	jne	.L107
	cmpb	$4, 64(%rdi)
	je	.L109
	ret
	.p2align 4,,10
	.p2align 3
.L107:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L109:
	movzbl	65(%rdi), %edx
	cmpb	$1, %dl
	je	.L108
	cmpb	$3, %dl
	movl	$794, %edx
	cmove	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L108:
	movl	$668, %eax
	ret
	.cfi_endproc
.LFE1313:
	.size	SCT_get_signature_nid, .-SCT_get_signature_nid
	.p2align 4
	.globl	SCT_get0_extensions
	.type	SCT_get0_extensions, @function
SCT_get0_extensions:
.LFB1314:
	.cfi_startproc
	endbr64
	movq	48(%rdi), %rax
	movq	%rax, (%rsi)
	movq	56(%rdi), %rax
	ret
	.cfi_endproc
.LFE1314:
	.size	SCT_get0_extensions, .-SCT_get0_extensions
	.p2align 4
	.globl	SCT_get0_signature
	.type	SCT_get0_signature, @function
SCT_get0_signature:
.LFB1315:
	.cfi_startproc
	endbr64
	movq	72(%rdi), %rax
	movq	%rax, (%rsi)
	movq	80(%rdi), %rax
	ret
	.cfi_endproc
.LFE1315:
	.size	SCT_get0_signature, .-SCT_get0_signature
	.p2align 4
	.globl	SCT_is_complete
	.type	SCT_is_complete, @function
SCT_is_complete:
.LFB1316:
	.cfi_startproc
	endbr64
	movl	(%rdi), %eax
	cmpl	$-1, %eax
	je	.L115
	testl	%eax, %eax
	jne	.L114
	cmpq	$0, 24(%rdi)
	je	.L112
	cmpb	$4, 64(%rdi)
	je	.L116
.L112:
	ret
	.p2align 4,,10
	.p2align 3
.L115:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L114:
	xorl	%eax, %eax
	cmpq	$0, 8(%rdi)
	setne	%al
	ret
	.p2align 4,,10
	.p2align 3
.L116:
	movzbl	65(%rdi), %edx
	andl	$-3, %edx
	cmpb	$1, %dl
	jne	.L112
	cmpq	$0, 72(%rdi)
	je	.L112
	xorl	%eax, %eax
	cmpq	$0, 80(%rdi)
	setne	%al
	ret
	.cfi_endproc
.LFE1316:
	.size	SCT_is_complete, .-SCT_is_complete
	.p2align 4
	.globl	SCT_signature_is_complete
	.type	SCT_signature_is_complete, @function
SCT_signature_is_complete:
.LFB1317:
	.cfi_startproc
	endbr64
	movl	(%rdi), %eax
	testl	%eax, %eax
	jne	.L119
	cmpb	$4, 64(%rdi)
	je	.L120
.L117:
	ret
	.p2align 4,,10
	.p2align 3
.L119:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L120:
	movzbl	65(%rdi), %edx
	andl	$-3, %edx
	cmpb	$1, %dl
	jne	.L117
	cmpq	$0, 72(%rdi)
	je	.L117
	xorl	%eax, %eax
	cmpq	$0, 80(%rdi)
	setne	%al
	ret
	.cfi_endproc
.LFE1317:
	.size	SCT_signature_is_complete, .-SCT_signature_is_complete
	.p2align 4
	.globl	SCT_get_source
	.type	SCT_get_source, @function
SCT_get_source:
.LFB1318:
	.cfi_startproc
	endbr64
	movl	92(%rdi), %eax
	ret
	.cfi_endproc
.LFE1318:
	.size	SCT_get_source, .-SCT_get_source
	.p2align 4
	.globl	SCT_set_source
	.type	SCT_set_source, @function
SCT_set_source:
.LFB1319:
	.cfi_startproc
	endbr64
	movl	%esi, 92(%rdi)
	movl	$0, 96(%rdi)
	cmpl	$2, %esi
	je	.L123
	andl	$-3, %esi
	cmpl	$1, %esi
	je	.L125
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L123:
	movl	$1, 88(%rdi)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L125:
	movl	$0, 88(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE1319:
	.size	SCT_set_source, .-SCT_set_source
	.p2align 4
	.globl	SCT_get_validation_status
	.type	SCT_get_validation_status, @function
SCT_get_validation_status:
.LFB1320:
	.cfi_startproc
	endbr64
	movl	96(%rdi), %eax
	ret
	.cfi_endproc
.LFE1320:
	.size	SCT_get_validation_status, .-SCT_get_validation_status
	.p2align 4
	.globl	SCT_validate
	.type	SCT_validate, @function
SCT_validate:
.LFB1321:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movl	(%rdi), %r12d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -72(%rbp)
	movq	$0, -64(%rbp)
	testl	%r12d, %r12d
	je	.L128
	movl	$5, 96(%rdi)
	xorl	%r12d, %r12d
.L127:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L145
	addq	$40, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L128:
	.cfi_restore_state
	movq	%rsi, %r14
	movq	32(%rdi), %rdx
	movq	24(%rdi), %rsi
	movq	16(%r14), %rdi
	call	CTLOG_STORE_get0_log_by_id@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L146
	call	SCT_CTX_new@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L133
	movq	%r13, %rdi
	call	CTLOG_get0_public_key@PLT
	leaq	-64(%rbp), %rdi
	movq	%rax, %rsi
	call	X509_PUBKEY_set@PLT
	cmpl	$1, %eax
	je	.L147
.L133:
	movl	$-1, %r12d
.L138:
.L132:
	movq	-72(%rbp), %rdi
	call	X509_PUBKEY_free@PLT
	movq	-64(%rbp), %rdi
	call	X509_PUBKEY_free@PLT
	movq	%r15, %rdi
	call	SCT_CTX_free@PLT
	jmp	.L127
	.p2align 4,,10
	.p2align 3
.L147:
	movq	-64(%rbp), %rsi
	movq	%r15, %rdi
	call	SCT_CTX_set1_pubkey@PLT
	cmpl	$1, %eax
	jne	.L133
	cmpl	$1, 88(%rbx)
	je	.L134
.L139:
	movq	24(%r14), %rsi
	movq	%r15, %rdi
	call	SCT_CTX_set_time@PLT
	movq	(%r14), %rsi
	xorl	%edx, %edx
	movq	%r15, %rdi
	call	SCT_CTX_set1_cert@PLT
	cmpl	$1, %eax
	je	.L148
.L135:
	movl	$4, 96(%rbx)
	jmp	.L132
	.p2align 4,,10
	.p2align 3
.L146:
	movl	$1, 96(%rbx)
	jmp	.L127
	.p2align 4,,10
	.p2align 3
.L134:
	movq	8(%r14), %rdi
	testq	%rdi, %rdi
	je	.L135
	call	X509_get0_pubkey@PLT
	leaq	-72(%rbp), %rdi
	movq	%rax, %rsi
	call	X509_PUBKEY_set@PLT
	cmpl	$1, %eax
	jne	.L133
	movq	-72(%rbp), %rsi
	movq	%r15, %rdi
	call	SCT_CTX_set1_issuer_pubkey@PLT
	cmpl	$1, %eax
	jne	.L133
	jmp	.L139
.L148:
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	SCT_CTX_verify@PLT
	xorl	%edx, %edx
	cmpl	$1, %eax
	setne	%dl
	xorl	%r12d, %r12d
	addl	$2, %edx
	cmpl	$1, %eax
	movl	%edx, 96(%rbx)
	sete	%r12b
	jmp	.L132
.L145:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1321:
	.size	SCT_validate, .-SCT_validate
	.p2align 4
	.globl	SCT_LIST_validate
	.type	SCT_LIST_validate, @function
SCT_LIST_validate:
.LFB1322:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -96(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L152
	movq	%rdi, %rbx
	call	OPENSSL_sk_num@PLT
	movl	%eax, -84(%rbp)
	testl	%eax, %eax
	jle	.L152
	leaq	-64(%rbp), %rax
	xorl	%r14d, %r14d
	movl	$1, %r13d
	movq	%rax, -112(%rbp)
	jmp	.L165
	.p2align 4,,10
	.p2align 3
.L177:
	movl	$5, 96(%r12)
	xorl	%r13d, %r13d
.L153:
	addl	$1, %r14d
	cmpl	-84(%rbp), %r14d
	je	.L149
.L165:
	movl	%r14d, %esi
	movq	%rbx, %rdi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L153
	movl	(%rax), %eax
	movq	$0, -72(%rbp)
	movq	$0, -64(%rbp)
	testl	%eax, %eax
	jne	.L177
	movq	-96(%rbp), %rax
	movq	32(%r12), %rdx
	movq	24(%r12), %rsi
	movq	16(%rax), %rdi
	call	CTLOG_STORE_get0_log_by_id@PLT
	testq	%rax, %rax
	je	.L178
	movq	%rax, -104(%rbp)
	call	SCT_CTX_new@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L157
	movq	-104(%rbp), %rdi
	call	CTLOG_get0_public_key@PLT
	movq	-112(%rbp), %rdi
	movq	%rax, %rsi
	call	X509_PUBKEY_set@PLT
	cmpl	$1, %eax
	je	.L179
.L157:
	movq	-72(%rbp), %rdi
	movl	$-1, %r13d
	call	X509_PUBKEY_free@PLT
	movq	-64(%rbp), %rdi
	call	X509_PUBKEY_free@PLT
	movq	%r15, %rdi
	call	SCT_CTX_free@PLT
.L149:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L180
	addq	$72, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L178:
	.cfi_restore_state
	movl	$1, 96(%r12)
	xorl	%r13d, %r13d
	jmp	.L153
	.p2align 4,,10
	.p2align 3
.L179:
	movq	-64(%rbp), %rsi
	movq	%r15, %rdi
	call	SCT_CTX_set1_pubkey@PLT
	cmpl	$1, %eax
	jne	.L157
	cmpl	$1, 88(%r12)
	jne	.L163
	movq	-96(%rbp), %rax
	movq	8(%rax), %rdi
	testq	%rdi, %rdi
	je	.L159
	call	X509_get0_pubkey@PLT
	leaq	-72(%rbp), %rdi
	movq	%rax, %rsi
	call	X509_PUBKEY_set@PLT
	cmpl	$1, %eax
	jne	.L157
	movq	-72(%rbp), %rsi
	movq	%r15, %rdi
	call	SCT_CTX_set1_issuer_pubkey@PLT
	cmpl	$1, %eax
	jne	.L157
	.p2align 4,,10
	.p2align 3
.L163:
	movq	-96(%rbp), %rax
	movq	%r15, %rdi
	movq	24(%rax), %rsi
	call	SCT_CTX_set_time@PLT
	movq	-96(%rbp), %rax
	xorl	%edx, %edx
	movq	%r15, %rdi
	movq	(%rax), %rsi
	call	SCT_CTX_set1_cert@PLT
	cmpl	$1, %eax
	je	.L181
.L159:
	movl	$4, 96(%r12)
	xorl	%r13d, %r13d
.L162:
	movq	-72(%rbp), %rdi
	call	X509_PUBKEY_free@PLT
	movq	-64(%rbp), %rdi
	call	X509_PUBKEY_free@PLT
	movq	%r15, %rdi
	call	SCT_CTX_free@PLT
	jmp	.L153
	.p2align 4,,10
	.p2align 3
.L152:
	movl	$1, %r13d
	jmp	.L149
.L181:
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	SCT_CTX_verify@PLT
	movl	$0, %edx
	cmpl	$1, %eax
	setne	%al
	cmovne	%edx, %r13d
	movzbl	%al, %eax
	addl	$2, %eax
	movl	%eax, 96(%r12)
	jmp	.L162
.L180:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1322:
	.size	SCT_LIST_validate, .-SCT_LIST_validate
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
