	.file	"eng_fat.c"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"ALL"
.LC1:
	.string	"RSA"
.LC2:
	.string	"DSA"
.LC3:
	.string	"DH"
.LC4:
	.string	"EC"
.LC5:
	.string	"RAND"
.LC6:
	.string	"CIPHERS"
.LC7:
	.string	"DIGESTS"
.LC8:
	.string	"PKEY"
.LC9:
	.string	"PKEY_CRYPTO"
.LC10:
	.string	"PKEY_ASN1"
	.text
	.p2align 4
	.type	int_def_cb, @function
int_def_cb:
.LFB952:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L13
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%esi, %rax
	movl	$4, %r9d
	movq	%rdi, %r8
	movq	%r9, %rcx
	leaq	.LC0(%rip), %rdi
	movq	%r8, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdx, %rbx
	subq	$8, %rsp
	cmpq	$4, %rax
	cmovbe	%rax, %rcx
	cmpq	%rcx, %rcx
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L20
	cmpq	$4, %rax
	movq	%r9, %rcx
	leaq	.LC1(%rip), %rdi
	movq	%r8, %rsi
	cmovbe	%rax, %rcx
	cmpq	%rcx, %rcx
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	jne	.L4
	orl	$1, (%rbx)
	movl	$1, %eax
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	.cfi_restore_state
	orl	$65535, (%rbx)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	.cfi_restore_state
	cmpq	$4, %rax
	movq	%r9, %rcx
	leaq	.LC2(%rip), %rdi
	movq	%r8, %rsi
	cmovbe	%rax, %rcx
	cmpq	%rcx, %rcx
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L21
	movl	$3, %edx
	cmpq	$3, %rax
	leaq	.LC3(%rip), %rdi
	movq	%r8, %rsi
	movq	%rdx, %rcx
	cmovbe	%rax, %rcx
	cmpq	%rcx, %rcx
	repz cmpsb
	seta	%cl
	sbbb	$0, %cl
	testb	%cl, %cl
	jne	.L6
	orl	$4, (%rbx)
	movl	$1, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L21:
	orl	$2, (%rbx)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	.cfi_restore 3
	.cfi_restore 6
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	cmpq	$3, %rax
	leaq	.LC4(%rip), %rdi
	movq	%r8, %rsi
	cmovbe	%rax, %rdx
	movq	%rdx, %rcx
	cmpq	%rdx, %rdx
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L22
	movl	$5, %r9d
	cmpq	$5, %rax
	leaq	.LC5(%rip), %rdi
	movq	%r8, %rsi
	movq	%r9, %rcx
	cmovbe	%rax, %rcx
	cmpq	%rcx, %rcx
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	jne	.L8
	orl	$8, (%rbx)
	movl	$1, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L22:
	orl	$2048, (%rbx)
	movl	$1, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L8:
	movl	$8, %edx
	cmpq	$8, %rax
	leaq	.LC6(%rip), %rdi
	movq	%r8, %rsi
	movq	%rdx, %rcx
	cmovbe	%rax, %rcx
	cmpq	%rcx, %rcx
	repz cmpsb
	seta	%cl
	sbbb	$0, %cl
	testb	%cl, %cl
	jne	.L9
	orl	$64, (%rbx)
	movl	$1, %eax
	jmp	.L1
.L9:
	cmpq	$8, %rax
	leaq	.LC7(%rip), %rdi
	movq	%r8, %rsi
	cmovbe	%rax, %rdx
	movq	%rdx, %rcx
	cmpq	%rdx, %rdx
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	jne	.L10
	orl	$128, (%rbx)
	movl	$1, %eax
	jmp	.L1
.L10:
	cmpq	$5, %rax
	movq	%r9, %rcx
	leaq	.LC8(%rip), %rdi
	movq	%r8, %rsi
	cmovbe	%rax, %rcx
	cmpq	%rcx, %rcx
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	jne	.L11
	orl	$1536, (%rbx)
	movl	$1, %eax
	jmp	.L1
.L11:
	cmpq	$12, %rax
	movl	$12, %ecx
	leaq	.LC9(%rip), %rdi
	movq	%r8, %rsi
	cmovbe	%rax, %rcx
	cmpq	%rcx, %rcx
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	jne	.L12
	orl	$512, (%rbx)
	movl	$1, %eax
	jmp	.L1
.L12:
	cmpq	$10, %rax
	movl	$10, %edx
	leaq	.LC10(%rip), %rsi
	movq	%r8, %rdi
	cmovbe	%rax, %rdx
	call	strncmp@PLT
	testl	%eax, %eax
	jne	.L14
	orl	$1024, (%rbx)
	movl	$1, %eax
	jmp	.L1
.L14:
	xorl	%eax, %eax
	jmp	.L1
	.cfi_endproc
.LFE952:
	.size	int_def_cb, .-int_def_cb
	.p2align 4
	.globl	ENGINE_set_default
	.type	ENGINE_set_default, @function
ENGINE_set_default:
.LFB951:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%esi, %ebx
	andl	$64, %esi
	jne	.L24
	testb	$-128, %bl
	jne	.L70
.L26:
	testb	$1, %bl
	jne	.L71
.L30:
	testb	$2, %bl
	jne	.L72
.L33:
	testb	$4, %bl
	jne	.L73
	testb	$8, %bh
	jne	.L74
.L37:
	testb	$8, %bl
	jne	.L75
.L39:
	testb	$2, %bh
	jne	.L76
.L41:
	andb	$4, %bh
	movl	$1, %eax
	jne	.L77
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L72:
	.cfi_restore_state
	movq	%r12, %rdi
	call	ENGINE_set_default_DSA@PLT
	testl	%eax, %eax
	jne	.L33
	.p2align 4,,10
	.p2align 3
.L31:
	xorl	%eax, %eax
.L78:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L73:
	.cfi_restore_state
	movq	%r12, %rdi
	call	ENGINE_set_default_DH@PLT
	testl	%eax, %eax
	je	.L31
	testb	$8, %bh
	je	.L37
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L24:
	call	ENGINE_set_default_ciphers@PLT
	testl	%eax, %eax
	je	.L31
	testb	$-128, %bl
	je	.L26
.L70:
	movq	%r12, %rdi
	call	ENGINE_set_default_digests@PLT
	testl	%eax, %eax
	je	.L31
	testb	$1, %bl
	je	.L30
.L71:
	movq	%r12, %rdi
	call	ENGINE_set_default_RSA@PLT
	testl	%eax, %eax
	jne	.L30
	xorl	%eax, %eax
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L74:
	movq	%r12, %rdi
	call	ENGINE_set_default_EC@PLT
	testl	%eax, %eax
	je	.L31
	testb	$8, %bl
	je	.L39
.L75:
	movq	%r12, %rdi
	call	ENGINE_set_default_RAND@PLT
	testl	%eax, %eax
	je	.L31
	testb	$2, %bh
	je	.L41
.L76:
	movq	%r12, %rdi
	call	ENGINE_set_default_pkey_meths@PLT
	testl	%eax, %eax
	jne	.L41
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L77:
	movq	%r12, %rdi
	call	ENGINE_set_default_pkey_asn1_meths@PLT
	popq	%rbx
	popq	%r12
	testl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	setne	%al
	movzbl	%al, %eax
	ret
	.cfi_endproc
.LFE951:
	.size	ENGINE_set_default, .-ENGINE_set_default
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC11:
	.string	"../deps/openssl/openssl/crypto/engine/eng_fat.c"
	.section	.rodata.str1.1
.LC12:
	.string	"str="
	.text
	.p2align 4
	.globl	ENGINE_set_default_string
	.type	ENGINE_set_default_string, @function
ENGINE_set_default_string:
.LFB953:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	int_def_cb(%rip), %rcx
	movl	$1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	leaq	-44(%rbp), %r8
	movl	$44, %esi
	pushq	%r12
	movq	%r13, %rdi
	subq	$24, %rsp
	.cfi_offset 12, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	$0, -44(%rbp)
	call	CONF_parse_list@PLT
	testl	%eax, %eax
	je	.L84
	movl	-44(%rbp), %esi
	movq	%r14, %rdi
	call	ENGINE_set_default
	movl	%eax, %r12d
.L79:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L85
	addq	$24, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L84:
	.cfi_restore_state
	movl	$85, %r8d
	leaq	.LC11(%rip), %rcx
	movl	$150, %edx
	movl	%eax, %r12d
	movl	$189, %esi
	movl	$38, %edi
	call	ERR_put_error@PLT
	movq	%r13, %rdx
	movl	$2, %edi
	xorl	%eax, %eax
	leaq	.LC12(%rip), %rsi
	call	ERR_add_error_data@PLT
	jmp	.L79
.L85:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE953:
	.size	ENGINE_set_default_string, .-ENGINE_set_default_string
	.p2align 4
	.globl	ENGINE_register_complete
	.type	ENGINE_register_complete, @function
ENGINE_register_complete:
.LFB954:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	ENGINE_register_ciphers@PLT
	movq	%r12, %rdi
	call	ENGINE_register_digests@PLT
	movq	%r12, %rdi
	call	ENGINE_register_RSA@PLT
	movq	%r12, %rdi
	call	ENGINE_register_DSA@PLT
	movq	%r12, %rdi
	call	ENGINE_register_DH@PLT
	movq	%r12, %rdi
	call	ENGINE_register_EC@PLT
	movq	%r12, %rdi
	call	ENGINE_register_RAND@PLT
	movq	%r12, %rdi
	call	ENGINE_register_pkey_meths@PLT
	movq	%r12, %rdi
	call	ENGINE_register_pkey_asn1_meths@PLT
	addq	$8, %rsp
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE954:
	.size	ENGINE_register_complete, .-ENGINE_register_complete
	.p2align 4
	.globl	ENGINE_register_all_complete
	.type	ENGINE_register_all_complete, @function
ENGINE_register_all_complete:
.LFB955:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	ENGINE_get_first@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L92
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L90:
	movq	%r12, %rdi
	call	ENGINE_get_next@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L89
.L92:
	testb	$8, 152(%r12)
	jne	.L90
	movq	%r12, %rdi
	call	ENGINE_register_ciphers@PLT
	movq	%r12, %rdi
	call	ENGINE_register_digests@PLT
	movq	%r12, %rdi
	call	ENGINE_register_RSA@PLT
	movq	%r12, %rdi
	call	ENGINE_register_DSA@PLT
	movq	%r12, %rdi
	call	ENGINE_register_DH@PLT
	movq	%r12, %rdi
	call	ENGINE_register_EC@PLT
	movq	%r12, %rdi
	call	ENGINE_register_RAND@PLT
	movq	%r12, %rdi
	call	ENGINE_register_pkey_meths@PLT
	movq	%r12, %rdi
	call	ENGINE_register_pkey_asn1_meths@PLT
	movq	%r12, %rdi
	call	ENGINE_get_next@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L92
.L89:
	addq	$8, %rsp
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE955:
	.size	ENGINE_register_all_complete, .-ENGINE_register_all_complete
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
