	.file	"bn_intern.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/bn/bn_intern.c"
	.text
	.p2align 4
	.globl	bn_compute_wNAF
	.type	bn_compute_wNAF, @function
bn_compute_wNAF:
.LFB252:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$72, %rsp
	movq	%rdx, -88(%rbp)
	call	BN_is_zero@PLT
	testl	%eax, %eax
	jne	.L36
	leal	-1(%rbx), %eax
	movl	$43, %r8d
	cmpl	$6, %eax
	ja	.L34
	movq	%r12, %rdi
	call	BN_is_negative@PLT
	cmpl	$1, %eax
	sbbl	%eax, %eax
	andl	$2, %eax
	subl	$1, %eax
	cmpq	$0, (%r12)
	movb	%al, -53(%rbp)
	je	.L8
	movl	8(%r12), %eax
	testl	%eax, %eax
	je	.L8
	movq	%r12, %rdi
	call	BN_num_bits@PLT
	movl	$60, %edx
	leaq	.LC0(%rip), %rsi
	cltq
	movq	%rax, -72(%rbp)
	addq	$1, %rax
	movq	%rax, %rdi
	movq	%rax, -104(%rbp)
	call	CRYPTO_malloc@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L37
	movq	(%r12), %rdx
	movl	%ebx, %ecx
	movl	$1, %r11d
	movq	%r14, -64(%rbp)
	sall	%cl, %r11d
	movl	$1, %r15d
	leal	(%r11,%r11), %eax
	movl	(%rdx), %r13d
	movl	%eax, -52(%rbp)
	subl	$1, %eax
	andl	%eax, %r13d
	sarl	%eax
	movl	%eax, -92(%rbp)
	movslq	%ebx, %rax
	movl	%r13d, %r14d
	movq	%r15, %r13
	movq	%rax, -80(%rbp)
	movq	%r12, %r15
	movl	%ebx, %r12d
	movl	%r11d, %ebx
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L40:
	movq	-80(%rbp), %rcx
	leaq	(%rcx,%r13), %rsi
	cmpq	%rsi, -72(%rbp)
	jbe	.L38
	xorl	%eax, %eax
.L20:
	movq	-64(%rbp), %rcx
	leal	(%r12,%r13), %esi
	movq	%r15, %rdi
	movb	%al, -1(%rcx,%r13)
	addq	$1, %r13
	call	BN_is_bit_set@PLT
	movl	%r12d, %ecx
	sall	%cl, %eax
	addl	%eax, %r14d
	cmpl	%r14d, -52(%rbp)
	jl	.L39
.L11:
	leaq	-1(%r13), %rax
	testl	%r14d, %r14d
	je	.L40
	testb	$1, %r14b
	je	.L12
	testl	%ebx, %r14d
	je	.L24
	movq	-80(%rbp), %rax
	addq	%r13, %rax
	cmpq	%rax, -72(%rbp)
	jbe	.L14
	movl	%r14d, %esi
	subl	-52(%rbp), %esi
.L13:
	movl	%ebx, %eax
	negl	%eax
	cmpl	%esi, %eax
	jge	.L16
.L15:
	cmpl	%esi, %ebx
	jle	.L16
	testb	$1, %sil
	je	.L16
	movl	%r14d, %edx
	subl	%esi, %edx
	setne	%dil
	cmpl	%edx, -52(%rbp)
	setne	%al
	testb	%al, %dil
	je	.L26
	cmpl	%edx, %ebx
	jne	.L18
.L26:
	movzbl	-53(%rbp), %eax
	sarl	%edx
	movl	%edx, %r14d
	imull	%esi, %eax
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L24:
	movl	%r14d, %esi
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L14:
	movl	-92(%rbp), %esi
	andl	%r14d, %esi
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L36:
	movl	$31, %edx
	leaq	.LC0(%rip), %rsi
	movl	$1, %edi
	call	CRYPTO_malloc@PLT
	movl	$33, %r8d
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L33
	movb	$0, (%rax)
	movq	-88(%rbp), %rax
	movq	$1, (%rax)
.L1:
	addq	$72, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	.cfi_restore_state
	movl	$55, %r8d
.L34:
	leaq	.LC0(%rip), %rcx
	movl	$68, %edx
	movl	$142, %esi
	xorl	%r14d, %r14d
	movl	$3, %edi
	call	ERR_put_error@PLT
.L4:
	movq	%r14, %rdi
	movl	$137, %edx
	leaq	.LC0(%rip), %rsi
	xorl	%r14d, %r14d
	call	CRYPTO_free@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L16:
	movq	-64(%rbp), %r14
	movl	$101, %r8d
.L32:
	movl	$68, %edx
	movl	$142, %esi
	movl	$3, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L39:
	movq	-64(%rbp), %r14
	movl	$124, %r8d
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L38:
	movq	-64(%rbp), %r14
	cmpq	%rax, -104(%rbp)
	jb	.L41
	movq	-88(%rbp), %rbx
	movq	%rax, (%rbx)
	jmp	.L1
.L37:
	movl	$66, %r8d
.L33:
	movl	$65, %edx
	movl	$142, %esi
	movl	$3, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L4
.L18:
	movq	-64(%rbp), %r14
	movl	$113, %r8d
	jmp	.L32
.L41:
	movl	$130, %r8d
	jmp	.L32
.L12:
	sarl	%r14d
	xorl	%eax, %eax
	jmp	.L20
	.cfi_endproc
.LFE252:
	.size	bn_compute_wNAF, .-bn_compute_wNAF
	.p2align 4
	.globl	bn_get_top
	.type	bn_get_top, @function
bn_get_top:
.LFB253:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	ret
	.cfi_endproc
.LFE253:
	.size	bn_get_top, .-bn_get_top
	.p2align 4
	.globl	bn_get_dmax
	.type	bn_get_dmax, @function
bn_get_dmax:
.LFB254:
	.cfi_startproc
	endbr64
	movl	12(%rdi), %eax
	ret
	.cfi_endproc
.LFE254:
	.size	bn_get_dmax, .-bn_get_dmax
	.p2align 4
	.globl	bn_set_all_zero
	.type	bn_set_all_zero, @function
bn_set_all_zero:
.LFB255:
	.cfi_startproc
	endbr64
	movslq	8(%rdi), %rdx
	movl	12(%rdi), %eax
	cmpl	%eax, %edx
	jge	.L44
	subl	$1, %eax
	xorl	%esi, %esi
	subl	%edx, %eax
	leaq	8(,%rax,8), %r8
	movq	(%rdi), %rax
	leaq	(%rax,%rdx,8), %rdi
	movq	%r8, %rdx
	jmp	memset@PLT
	.p2align 4,,10
	.p2align 3
.L44:
	ret
	.cfi_endproc
.LFE255:
	.size	bn_set_all_zero, .-bn_set_all_zero
	.p2align 4
	.globl	bn_copy_words
	.type	bn_copy_words, @function
bn_copy_words:
.LFB256:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -32
	cmpl	%edx, 8(%rsi)
	jle	.L52
.L46:
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L52:
	.cfi_restore_state
	movslq	%edx, %rdx
	movq	%rsi, %rbx
	xorl	%esi, %esi
	movl	$1, %r12d
	salq	$3, %rdx
	call	memset@PLT
	movq	(%rbx), %rsi
	movq	%rax, %rdi
	testq	%rsi, %rsi
	je	.L46
	movslq	8(%rbx), %rdx
	salq	$3, %rdx
	call	memcpy@PLT
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE256:
	.size	bn_copy_words, .-bn_copy_words
	.p2align 4
	.globl	bn_get_words
	.type	bn_get_words, @function
bn_get_words:
.LFB257:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE257:
	.size	bn_get_words, .-bn_get_words
	.p2align 4
	.globl	bn_set_static_words
	.type	bn_set_static_words, @function
bn_set_static_words:
.LFB258:
	.cfi_startproc
	endbr64
	orl	$2, 20(%rdi)
	movq	%rsi, (%rdi)
	movl	%edx, 8(%rdi)
	movl	%edx, 12(%rdi)
	movl	$0, 16(%rdi)
	jmp	bn_correct_top@PLT
	.cfi_endproc
.LFE258:
	.size	bn_set_static_words, .-bn_set_static_words
	.p2align 4
	.globl	bn_set_words
	.type	bn_set_words, @function
bn_set_words:
.LFB259:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	movl	%edx, %esi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movl	%edx, %ebx
	subq	$8, %rsp
	call	bn_wexpand@PLT
	testq	%rax, %rax
	je	.L59
	movq	(%r12), %rdi
	movslq	%ebx, %rdx
	movq	%r13, %rsi
	salq	$3, %rdx
	call	memcpy@PLT
	movl	%ebx, 8(%r12)
	movq	%r12, %rdi
	call	bn_correct_top@PLT
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L59:
	.cfi_restore_state
	movl	$191, %r8d
	movl	$65, %edx
	movl	$144, %esi
	movl	$3, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE259:
	.size	bn_set_words, .-bn_set_words
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
