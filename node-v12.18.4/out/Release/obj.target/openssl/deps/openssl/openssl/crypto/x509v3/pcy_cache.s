	.file	"pcy_cache.c"
	.text
	.p2align 4
	.type	policy_data_cmp, @function
policy_data_cmp:
.LFB1327:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rax
	movq	8(%rax), %rsi
	movq	(%rdi), %rax
	movq	8(%rax), %rdi
	jmp	OBJ_cmp@PLT
	.cfi_endproc
.LFE1327:
	.size	policy_data_cmp, .-policy_data_cmp
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/x509v3/pcy_cache.c"
	.text
	.p2align 4
	.globl	policy_cache_free
	.type	policy_cache_free, @function
policy_cache_free:
.LFB1324:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L3
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	(%rdi), %rdi
	call	policy_data_free@PLT
	movq	8(%r12), %rdi
	movq	policy_data_free@GOTPCREL(%rip), %rsi
	call	OPENSSL_sk_pop_free@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$184, %edx
	popq	%r12
	.cfi_restore 12
	leaq	.LC0(%rip), %rsi
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_free@PLT
	.p2align 4,,10
	.p2align 3
.L3:
	ret
	.cfi_endproc
.LFE1324:
	.size	policy_cache_free, .-policy_cache_free
	.p2align 4
	.globl	policy_cache_set
	.type	policy_cache_set, @function
policy_cache_set:
.LFB1325:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	256(%rdi), %rax
	testq	%rax, %rax
	je	.L53
.L8:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L54
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L53:
	.cfi_restore_state
	movq	%rdi, %r14
	movq	336(%rdi), %rdi
	call	CRYPTO_THREAD_write_lock@PLT
	movq	256(%r14), %r12
	testq	%r12, %r12
	je	.L55
.L11:
	movq	336(%r14), %rdi
	call	CRYPTO_THREAD_unlock@PLT
	movq	256(%r14), %rax
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L55:
	movl	$92, %edx
	leaq	.LC0(%rip), %rsi
	movl	$40, %edi
	call	CRYPTO_malloc@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L56
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movl	$401, %esi
	movq	%r14, %rdi
	movups	%xmm0, (%rax)
	pcmpeqd	%xmm0, %xmm0
	movq	$-1, 32(%rax)
	movups	%xmm0, 16(%rax)
	movq	%rax, 256(%r14)
	leaq	-60(%rbp), %rax
	movq	%rax, %rdx
	movq	%rax, -104(%rbp)
	call	X509_get_ext_d2i@PLT
	movq	%rax, -72(%rbp)
	testq	%rax, %rax
	je	.L57
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L58
	cmpl	$258, 4(%rdi)
	je	.L35
	call	ASN1_INTEGER_get@PLT
	movq	%rax, 24(%rbx)
	movq	-72(%rbp), %rax
	movq	8(%rax), %rdi
	testq	%rdi, %rdi
	je	.L15
.L17:
	cmpl	$258, 4(%rdi)
	je	.L35
	call	ASN1_INTEGER_get@PLT
	movq	%rax, 32(%rbx)
.L15:
	movq	-104(%rbp), %rdx
	xorl	%ecx, %ecx
	movl	$89, %esi
	movq	%r14, %rdi
	call	X509_get_ext_d2i@PLT
	movq	%rax, -88(%rbp)
	testq	%rax, %rax
	je	.L59
	movq	256(%r14), %r15
	movq	%rax, %rdi
	movl	-60(%rbp), %eax
	movq	%r15, -80(%rbp)
	movl	%eax, -92(%rbp)
	call	OPENSSL_sk_num@PLT
	movl	%eax, -96(%rbp)
	testl	%eax, %eax
	jle	.L21
	leaq	policy_data_cmp(%rip), %rdi
	xorl	%r13d, %r13d
	call	OPENSSL_sk_new@PLT
	movl	$38, %r8d
	movq	%rax, 8(%r15)
	testq	%rax, %rax
	jne	.L22
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L62:
	cmpq	$0, (%rax)
	jne	.L26
	movq	%r15, (%rax)
.L27:
	addl	$1, %r13d
	cmpl	%r13d, -96(%rbp)
	je	.L60
.L22:
	movq	-88(%rbp), %rdi
	movl	%r13d, %esi
	call	OPENSSL_sk_value@PLT
	movl	-92(%rbp), %edx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	policy_data_new@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L61
	movq	8(%rax), %rdi
	call	OBJ_obj2nid@PLT
	cmpl	$746, %eax
	movq	-80(%rbp), %rax
	je	.L62
	movq	8(%rax), %rdi
	movq	%r15, %rsi
	call	OPENSSL_sk_find@PLT
	testl	%eax, %eax
	jns	.L26
	movq	-80(%rbp), %rax
	movq	%r15, %rsi
	movq	8(%rax), %rdi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	jne	.L27
	movl	$61, %r8d
	movl	$65, %edx
	movl	$169, %esi
	movl	$34, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L52:
	movq	%r15, %rdi
	call	policy_data_free@PLT
	movq	POLICYINFO_free@GOTPCREL(%rip), %rsi
	movq	-88(%rbp), %rdi
	call	OPENSSL_sk_pop_free@PLT
.L23:
	movq	-80(%rbp), %rbx
	movq	policy_data_free@GOTPCREL(%rip), %rsi
	movq	8(%rbx), %rdi
	call	OPENSSL_sk_pop_free@PLT
	movq	$0, 8(%rbx)
	jmp	.L11
.L56:
	movl	$94, %r8d
	movl	$65, %edx
	movl	$170, %esi
	movl	$34, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L11
.L38:
	movq	%rax, %r12
	.p2align 4,,10
	.p2align 3
.L35:
	orl	$2048, 224(%r14)
.L34:
	movq	-72(%rbp), %rdi
	call	POLICY_CONSTRAINTS_free@PLT
	movq	%r12, %rdi
	call	ASN1_INTEGER_free@PLT
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L57:
	cmpl	$-1, -60(%rbp)
	jne	.L35
	jmp	.L15
.L59:
	cmpl	$-1, -60(%rbp)
	jne	.L35
	jmp	.L11
.L58:
	movq	8(%rax), %rdi
	testq	%rdi, %rdi
	jne	.L17
	jmp	.L35
.L60:
	xorl	%edi, %edi
	call	policy_data_free@PLT
	movq	POLICYINFO_free@GOTPCREL(%rip), %rsi
	movq	-88(%rbp), %rdi
	call	OPENSSL_sk_pop_free@PLT
	movq	-104(%rbp), %rdx
	movl	$747, %esi
	xorl	%ecx, %ecx
	movq	%r14, %rdi
	movl	$1, -60(%rbp)
	call	X509_get_ext_d2i@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L63
	movq	%r14, %rdi
	call	policy_cache_set_mapping@PLT
	movl	%eax, -60(%rbp)
	testl	%eax, %eax
	jle	.L35
.L33:
	movq	-104(%rbp), %rdx
	xorl	%ecx, %ecx
	movl	$748, %esi
	movq	%r14, %rdi
	call	X509_get_ext_d2i@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L64
	cmpl	$258, 4(%rax)
	je	.L38
	movq	%rax, %rdi
	movq	%r13, %r12
	call	ASN1_INTEGER_get@PLT
	movq	%rax, 16(%rbx)
	jmp	.L34
.L21:
	xorl	%edi, %edi
	call	policy_data_free@PLT
	movq	POLICYINFO_free@GOTPCREL(%rip), %rsi
	movq	-88(%rbp), %rdi
	call	OPENSSL_sk_pop_free@PLT
	jmp	.L23
.L63:
	cmpl	$-1, -60(%rbp)
	jne	.L35
	jmp	.L33
.L61:
	movl	$45, %r8d
.L51:
	movl	$65, %edx
	movl	$169, %esi
	movl	$34, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	POLICYINFO_free@GOTPCREL(%rip), %rsi
	movq	-88(%rbp), %rdi
	call	OPENSSL_sk_pop_free@PLT
	jmp	.L23
.L26:
	orl	$2048, 224(%r14)
	jmp	.L52
.L64:
	cmpl	$-1, -60(%rbp)
	jne	.L35
	xorl	%r12d, %r12d
	jmp	.L34
.L54:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1325:
	.size	policy_cache_set, .-policy_cache_set
	.p2align 4
	.globl	policy_cache_find_data
	.type	policy_cache_find_data, @function
policy_cache_find_data:
.LFB1326:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	8(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	%rsi, -56(%rbp)
	leaq	-64(%rbp), %rsi
	call	OPENSSL_sk_find@PLT
	movq	8(%rbx), %rdi
	movl	%eax, %esi
	call	OPENSSL_sk_value@PLT
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L68
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L68:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1326:
	.size	policy_cache_find_data, .-policy_cache_find_data
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
