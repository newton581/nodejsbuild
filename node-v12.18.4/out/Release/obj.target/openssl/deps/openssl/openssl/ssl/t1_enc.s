	.file	"t1_enc.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/ssl/t1_enc.c"
	.text
	.p2align 4
	.type	tls1_PRF.constprop.0, @function
tls1_PRF.constprop.0:
.LFB972:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%rdx, -56(%rbp)
	movq	%r8, -64(%rbp)
	movq	%r9, -72(%rbp)
	call	ssl_prf_md@PLT
	testq	%rax, %rax
	je	.L13
	xorl	%esi, %esi
	movl	$1021, %edi
	movq	%rax, %rbx
	call	EVP_PKEY_CTX_new_id@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L8
	movq	%rax, %rdi
	call	EVP_PKEY_derive_init@PLT
	testl	%eax, %eax
	jle	.L8
	xorl	%r8d, %r8d
	movq	%rbx, %r9
	movl	$4096, %ecx
	movl	$1024, %edx
	movl	$-1, %esi
	movq	%r15, %rdi
	call	EVP_PKEY_CTX_ctrl@PLT
	testl	%eax, %eax
	jle	.L8
	movq	40(%rbp), %r9
	movl	48(%rbp), %r8d
	movl	$4097, %ecx
	movq	%r15, %rdi
	movl	$1024, %edx
	movl	$-1, %esi
	call	EVP_PKEY_CTX_ctrl@PLT
	testl	%eax, %eax
	jle	.L8
	movl	-56(%rbp), %r8d
	movq	%r13, %r9
	movl	$4098, %ecx
	movq	%r15, %rdi
	movl	$1024, %edx
	movl	$-1, %esi
	call	EVP_PKEY_CTX_ctrl@PLT
	testl	%eax, %eax
	jle	.L8
	movl	-64(%rbp), %r8d
	movq	%r14, %r9
	movl	$4098, %ecx
	movq	%r15, %rdi
	movl	$1024, %edx
	movl	$-1, %esi
	call	EVP_PKEY_CTX_ctrl@PLT
	testl	%eax, %eax
	jle	.L8
	movq	-72(%rbp), %r9
	movl	16(%rbp), %r8d
	movl	$4098, %ecx
	movq	%r15, %rdi
	movl	$1024, %edx
	movl	$-1, %esi
	call	EVP_PKEY_CTX_ctrl@PLT
	testl	%eax, %eax
	jle	.L8
	movq	24(%rbp), %r9
	movl	32(%rbp), %r8d
	movl	$4098, %ecx
	movq	%r15, %rdi
	movl	$1024, %edx
	movl	$-1, %esi
	call	EVP_PKEY_CTX_ctrl@PLT
	testl	%eax, %eax
	jle	.L8
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$4098, %ecx
	movl	$1024, %edx
	movl	$-1, %esi
	movq	%r15, %rdi
	call	EVP_PKEY_CTX_ctrl@PLT
	testl	%eax, %eax
	jle	.L8
	movq	56(%rbp), %rsi
	leaq	64(%rbp), %rdx
	movq	%r15, %rdi
	call	EVP_PKEY_derive@PLT
	testl	%eax, %eax
	jle	.L8
	movl	$1, %r12d
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L8:
	movl	72(%rbp), %eax
	testl	%eax, %eax
	jne	.L6
	movl	$55, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$68, %edx
	xorl	%r12d, %r12d
	movl	$284, %esi
	movl	$20, %edi
	call	ERR_put_error@PLT
.L9:
	movq	%r15, %rdi
	call	EVP_PKEY_CTX_free@PLT
.L1:
	addq	$40, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	.cfi_restore_state
	movq	%r12, %rdi
	movl	$52, %r9d
	movl	$68, %ecx
	xorl	%r12d, %r12d
	leaq	.LC0(%rip), %r8
	movl	$284, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L13:
	movl	72(%rbp), %edx
	testl	%edx, %edx
	je	.L3
	movq	%r12, %rdi
	movl	$35, %r9d
	movl	$68, %ecx
	xorl	%r12d, %r12d
	leaq	.LC0(%rip), %r8
	movl	$284, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L3:
	movl	$38, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$68, %edx
	xorl	%r12d, %r12d
	movl	$284, %esi
	movl	$20, %edi
	call	ERR_put_error@PLT
	jmp	.L1
	.cfi_endproc
.LFE972:
	.size	tls1_PRF.constprop.0, .-tls1_PRF.constprop.0
	.p2align 4
	.globl	tls1_change_cipher_state
	.type	tls1_change_cipher_state, @function
tls1_change_cipher_state:
.LFB966:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$72, %rsp
	movq	168(%rdi), %rax
	movq	640(%rax), %rcx
	movq	632(%rax), %r13
	movq	(%rax), %rdx
	movq	%rcx, -104(%rbp)
	movl	648(%rax), %ecx
	movl	%ecx, -96(%rbp)
	movl	1812(%rdi), %ecx
	andl	$1, %r12d
	je	.L15
	movq	%rdx, %rsi
	andb	$-2, %dh
	movq	1088(%rdi), %r14
	orq	$256, %rsi
	testl	%ecx, %ecx
	movq	568(%rax), %rcx
	cmovne	%rsi, %rdx
	movq	%rdx, (%rax)
	movl	312(%rdi), %edx
	movl	%edx, %eax
	andl	$-2, %edx
	orl	$1, %eax
	testb	$1, 66(%rcx)
	cmovne	%eax, %edx
	movl	%edx, 312(%rdi)
	testq	%r14, %r14
	je	.L95
.L20:
	leaq	1112(%r15), %rdi
	xorl	%esi, %esi
	call	ssl_replace_hash@PLT
	movq	%rax, -88(%rbp)
	testq	%rax, %rax
	je	.L55
	movq	8(%r15), %rax
	movq	192(%rax), %rax
	testb	$8, 96(%rax)
	je	.L96
.L23:
	movq	168(%r15), %rax
	movl	%r12d, %edx
	leaq	16(%rax), %rdi
	leaq	8(%rax), %rcx
	movq	%rdi, -64(%rbp)
	movq	%rcx, -56(%rbp)
	testl	%edx, %edx
	je	.L35
.L98:
	movq	%r14, %rdi
	call	EVP_CIPHER_CTX_reset@PLT
	movq	168(%r15), %rax
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L15:
	movq	%rdx, %rsi
	andb	$-5, %dh
	movl	$1, 124(%rdi)
	movq	1136(%rdi), %r14
	orq	$1024, %rsi
	testl	%ecx, %ecx
	movq	568(%rax), %rcx
	cmovne	%rsi, %rdx
	movq	%rdx, (%rax)
	movl	312(%rdi), %edx
	movl	%edx, %eax
	andl	$-3, %edx
	orl	$2, %eax
	testb	$1, 66(%rcx)
	cmovne	%eax, %edx
	movl	%edx, 312(%rdi)
	testq	%r14, %r14
	je	.L29
	movq	8(%rdi), %rax
	movq	192(%rax), %rax
	testb	$8, 96(%rax)
	jne	.L29
	movl	$1, %edx
.L30:
	xorl	%esi, %esi
	leaq	1160(%r15), %rdi
	movl	%edx, -56(%rbp)
	call	ssl_replace_hash@PLT
	movl	-56(%rbp), %edx
	movl	$183, %r9d
	testq	%rax, %rax
	movq	%rax, -88(%rbp)
	je	.L92
.L33:
	movq	8(%r15), %rax
	movq	192(%rax), %rax
	testb	$8, 96(%rax)
	je	.L97
.L34:
	movq	168(%r15), %rax
	leaq	88(%rax), %rcx
	movq	%rcx, -64(%rbp)
	leaq	80(%rax), %rcx
	movq	%rcx, -56(%rbp)
	testl	%edx, %edx
	jne	.L98
.L35:
	movq	656(%rax), %r8
	movq	624(%rax), %rsi
	movq	%r13, %rdi
	movq	-56(%rbp), %rax
	movq	%rsi, -80(%rbp)
	movq	%r8, (%rax)
	movq	%r8, -72(%rbp)
	call	EVP_CIPHER_key_length@PLT
	movq	%r13, %rdi
	movslq	%eax, %r12
	call	EVP_CIPHER_flags@PLT
	movq	-72(%rbp), %r8
	movq	-80(%rbp), %rsi
	andl	$983047, %eax
	cmpq	$6, %rax
	je	.L38
	movq	%r13, %rdi
	movq	%rsi, -80(%rbp)
	movq	%r8, -72(%rbp)
	call	EVP_CIPHER_flags@PLT
	movq	-72(%rbp), %r8
	movq	-80(%rbp), %rsi
	andl	$983047, %eax
	cmpq	$7, %rax
	jne	.L99
.L38:
	movl	$4, -80(%rbp)
	movl	$4, %edx
.L37:
	leaq	(%r8,%r8), %rax
	cmpl	$18, %ebx
	je	.L59
	cmpl	$33, %ebx
	je	.L59
	addq	%r12, %rax
	addq	%rax, %r12
	leaq	(%rsi,%rax), %rcx
	leaq	(%rdx,%r12), %rax
	movq	%rcx, -72(%rbp)
	leaq	(%rsi,%rax), %r12
	addq	%rdx, %rax
	movq	168(%r15), %rdx
	addq	%r8, %rsi
	cmpq	%rax, 616(%rdx)
	jb	.L100
.L42:
	movq	-64(%rbp), %rdi
	movq	%r8, %rdx
	call	memcpy@PLT
	movq	%r13, %rdi
	call	EVP_CIPHER_flags@PLT
	testl	$2097152, %eax
	jne	.L43
	movq	-56(%rbp), %rax
	movq	-64(%rbp), %rdx
	xorl	%esi, %esi
	movl	-96(%rbp), %edi
	movq	(%rax), %rax
	movq	%rax, -112(%rbp)
	movl	-112(%rbp), %ecx
	call	EVP_PKEY_new_mac_key@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L45
	movq	-104(%rbp), %rdx
	movq	-88(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	movq	%rax, -96(%rbp)
	call	EVP_DigestSignInit@PLT
	movq	-96(%rbp), %r8
	testl	%eax, %eax
	jle	.L45
	movq	%r8, %rdi
	call	EVP_PKEY_free@PLT
.L43:
	movq	%r13, %rdi
	andl	$2, %ebx
	call	EVP_CIPHER_flags@PLT
	andl	$983047, %eax
	cmpq	$6, %rax
	je	.L101
	movq	%r13, %rdi
	call	EVP_CIPHER_flags@PLT
	andl	$983047, %eax
	cmpq	$7, %rax
	jne	.L50
	movq	168(%r15), %rax
	movl	%ebx, %r9d
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	568(%rax), %rax
	movl	36(%rax), %eax
	andl	$196608, %eax
	cmpl	$1, %eax
	sbbl	%r10d, %r10d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	andl	$8, %r10d
	addl	$8, %r10d
	movl	%r10d, -88(%rbp)
	call	EVP_CipherInit_ex@PLT
	movl	-88(%rbp), %r10d
	testl	%eax, %eax
	je	.L53
	xorl	%ecx, %ecx
	movl	$12, %edx
	movl	$9, %esi
	movq	%r14, %rdi
	movl	%r10d, -88(%rbp)
	call	EVP_CIPHER_CTX_ctrl@PLT
	testl	%eax, %eax
	jne	.L102
.L53:
	movl	$296, %r9d
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L29:
	call	EVP_CIPHER_CTX_new@PLT
	movl	$166, %r9d
	movq	%rax, 1136(%r15)
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L92
	movq	8(%r15), %rax
	movq	192(%rax), %rax
	testb	$8, 96(%rax)
	je	.L57
	call	EVP_MD_CTX_new@PLT
	movq	%rax, -88(%rbp)
	testq	%rax, %rax
	je	.L103
	movq	%rax, 1160(%r15)
	xorl	%edx, %edx
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L59:
	leaq	(%rsi,%rax), %rcx
	leaq	(%rax,%r12,2), %rax
	leaq	(%rsi,%rax), %r12
	leaq	(%rax,%rdx,2), %rax
	movq	168(%r15), %rdx
	movq	%rcx, -72(%rbp)
	cmpq	%rax, 616(%rdx)
	jnb	.L42
.L100:
	movl	$247, %r9d
.L94:
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
.L91:
	movl	$209, %edx
	movl	$80, %esi
	movq	%r15, %rdi
	xorl	%r12d, %r12d
	call	ossl_statem_fatal@PLT
.L14:
	addq	$72, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L95:
	.cfi_restore_state
	call	EVP_CIPHER_CTX_new@PLT
	movl	$119, %r9d
	movl	$65, %ecx
	leaq	.LC0(%rip), %r8
	movq	%rax, 1088(%r15)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L91
	call	EVP_CIPHER_CTX_reset@PLT
	xorl	%r12d, %r12d
	movq	1088(%r15), %r14
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L50:
	movq	-72(%rbp), %rcx
	movl	%ebx, %r9d
	movq	%r12, %r8
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	EVP_CipherInit_ex@PLT
	movl	$302, %r9d
	movl	%eax, %r12d
	testl	%eax, %eax
	je	.L93
.L49:
	movq	%r13, %rdi
	call	EVP_CIPHER_flags@PLT
	testl	$2097152, %eax
	je	.L54
	movq	-56(%rbp), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	jne	.L104
.L54:
	movl	$0, 124(%r15)
	movl	$1, %r12d
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L45:
	movq	%r8, %rdi
	call	EVP_PKEY_free@PLT
	movl	$261, %r9d
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L99:
	movq	%r13, %rdi
	movq	%rsi, -112(%rbp)
	call	EVP_CIPHER_iv_length@PLT
	movq	-112(%rbp), %rsi
	movq	-72(%rbp), %r8
	movl	%eax, -80(%rbp)
	movslq	%eax, %rdx
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L97:
	leaq	2112(%r15), %rdi
	movl	%edx, -56(%rbp)
	call	RECORD_LAYER_reset_write_sequence@PLT
	movl	-56(%rbp), %edx
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L101:
	movq	-72(%rbp), %rcx
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movl	%ebx, %r9d
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	EVP_CipherInit_ex@PLT
	testl	%eax, %eax
	je	.L48
	movl	-80(%rbp), %edx
	movq	%r12, %rcx
	movl	$18, %esi
	movq	%r14, %rdi
	call	EVP_CIPHER_CTX_ctrl@PLT
	testl	%eax, %eax
	jne	.L49
.L48:
	movl	$280, %r9d
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L96:
	leaq	2112(%r15), %rdi
	call	RECORD_LAYER_reset_read_sequence@PLT
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L55:
	xorl	%r12d, %r12d
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L103:
	movl	$174, %r9d
.L92:
	leaq	.LC0(%rip), %r8
	movl	$65, %ecx
	movl	$209, %edx
	movq	%r15, %rdi
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L102:
	movl	-88(%rbp), %r10d
	xorl	%ecx, %ecx
	movl	$17, %esi
	movq	%r14, %rdi
	movl	%r10d, %edx
	call	EVP_CIPHER_CTX_ctrl@PLT
	testl	%eax, %eax
	je	.L53
	movl	-80(%rbp), %edx
	movq	%r12, %rcx
	movl	$18, %esi
	movq	%r14, %rdi
	call	EVP_CIPHER_CTX_ctrl@PLT
	testl	%eax, %eax
	je	.L53
	movq	-72(%rbp), %rcx
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
	movl	$-1, %r9d
	movq	%r14, %rdi
	call	EVP_CipherInit_ex@PLT
	testl	%eax, %eax
	jne	.L49
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L104:
	movq	-64(%rbp), %rcx
	movl	$23, %esi
	movq	%r14, %rdi
	call	EVP_CIPHER_CTX_ctrl@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	jne	.L54
	movl	$311, %r9d
	.p2align 4,,10
	.p2align 3
.L93:
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movl	$209, %edx
	movq	%r15, %rdi
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L57:
	xorl	%edx, %edx
	jmp	.L30
	.cfi_endproc
.LFE966:
	.size	tls1_change_cipher_state, .-tls1_change_cipher_state
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"key expansion"
	.text
	.p2align 4
	.globl	tls1_setup_key_block
	.type	tls1_setup_key_block, @function
tls1_setup_key_block:
.LFB967:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	168(%rdi), %rax
	movl	$0, -76(%rbp)
	movq	$0, -48(%rbp)
	cmpq	$0, 616(%rax)
	je	.L106
.L120:
	movl	$1, %eax
.L105:
	movq	-40(%rbp), %rsi
	xorq	%fs:40, %rsi
	jne	.L122
	leaq	-24(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L106:
	.cfi_restore_state
	movq	%rdi, %r12
	leaq	-76(%rbp), %rcx
	leaq	-64(%rbp), %rdx
	subq	$8, %rsp
	movl	1812(%r12), %eax
	movq	1296(%rdi), %rdi
	leaq	-72(%rbp), %rsi
	leaq	-56(%rbp), %r9
	leaq	-48(%rbp), %r8
	pushq	%rax
	call	ssl_cipher_get_evp@PLT
	popq	%rdx
	popq	%rcx
	testl	%eax, %eax
	je	.L123
	movq	168(%r12), %rax
	movl	-76(%rbp), %edx
	movq	-72(%rbp), %xmm0
	movq	-72(%rbp), %rdi
	movl	%edx, 648(%rax)
	movq	-48(%rbp), %rdx
	movhps	-64(%rbp), %xmm0
	movq	%rdx, 656(%rax)
	movups	%xmm0, 632(%rax)
	call	EVP_CIPHER_key_length@PLT
	movq	-72(%rbp), %rdi
	movq	-48(%rbp), %r13
	movslq	%eax, %rbx
	call	EVP_CIPHER_iv_length@PLT
	movq	%r12, %rdi
	cltq
	addq	%rax, %rbx
	call	ssl3_cleanup_key_block@PLT
	addq	%r13, %rbx
	movl	$367, %edx
	leaq	.LC0(%rip), %rsi
	addq	%rbx, %rbx
	movq	%rbx, %rdi
	call	CRYPTO_malloc@PLT
	testq	%rax, %rax
	je	.L124
	movl	$32, %r8d
	movq	168(%r12), %rdx
	movq	%r12, %rdi
	movq	1296(%r12), %rcx
	leaq	.LC1(%rip), %rsi
	movq	%rbx, 616(%rdx)
	addq	$80, %rcx
	leaq	152(%rdx), %r10
	leaq	184(%rdx), %r9
	movq	%rax, 624(%rdx)
	movl	$13, %edx
	pushq	$1
	pushq	%rbx
	pushq	%rax
	pushq	-72(%rcx)
	pushq	%rcx
	movq	%r10, %rcx
	pushq	$0
	pushq	$0
	pushq	$32
	call	tls1_PRF.constprop.0
	addq	$64, %rsp
	testl	%eax, %eax
	je	.L105
	testb	$8, 1493(%r12)
	jne	.L120
	movq	8(%r12), %rax
	cmpl	$769, (%rax)
	jg	.L120
	movq	1296(%r12), %rax
	movq	168(%r12), %rdx
	movq	504(%rax), %rax
	movl	$1, 216(%rdx)
	testq	%rax, %rax
	je	.L120
	movl	36(%rax), %eax
	cmpl	$32, %eax
	je	.L121
	cmpl	$4, %eax
	jne	.L120
.L121:
	movl	$0, 216(%rdx)
	jmp	.L120
	.p2align 4,,10
	.p2align 3
.L123:
	movl	$138, %ecx
	movl	$211, %edx
	movq	%r12, %rdi
	movl	%eax, -84(%rbp)
	movl	$353, %r9d
	leaq	.LC0(%rip), %r8
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	movl	-84(%rbp), %eax
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L124:
	movl	$368, %r9d
	leaq	.LC0(%rip), %r8
	movl	$65, %ecx
	movq	%r12, %rdi
	movl	$211, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	jmp	.L105
.L122:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE967:
	.size	tls1_setup_key_block, .-tls1_setup_key_block
	.p2align 4
	.globl	tls1_final_finish_mac
	.type	tls1_final_finish_mac, @function
tls1_final_finish_mac:
.LFB968:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	xorl	%esi, %esi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	ssl3_digest_cached_records@PLT
	testl	%eax, %eax
	jne	.L126
.L128:
	xorl	%eax, %eax
.L125:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L137
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L126:
	.cfi_restore_state
	leaq	-128(%rbp), %r15
	leaq	-136(%rbp), %rcx
	movl	$64, %edx
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	ssl_handshake_hash@PLT
	testl	%eax, %eax
	je	.L128
	movq	1296(%r12), %rax
	pushq	$1
	xorl	%r9d, %r9d
	movq	%r15, %rcx
	pushq	$12
	movq	-136(%rbp), %r8
	movq	%r14, %rdx
	movq	%r13, %rsi
	pushq	%rbx
	addq	$80, %rax
	movq	%r12, %rdi
	pushq	-72(%rax)
	pushq	%rax
	pushq	$0
	pushq	$0
	pushq	$0
	call	tls1_PRF.constprop.0
	addq	$64, %rsp
	testl	%eax, %eax
	je	.L128
	movq	-136(%rbp), %rsi
	movq	%r15, %rdi
	call	OPENSSL_cleanse@PLT
	movl	$12, %eax
	jmp	.L125
.L137:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE968:
	.size	tls1_final_finish_mac, .-tls1_final_finish_mac
	.section	.rodata.str1.1
.LC2:
	.string	"extended master secret"
.LC3:
	.string	"master secret"
	.text
	.p2align 4
	.globl	tls1_generate_master_secret
	.type	tls1_generate_master_secret, @function
tls1_generate_master_secret:
.LFB969:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$184, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	1296(%rdi), %rax
	testb	$1, 632(%rax)
	je	.L139
	movl	$1, %esi
	call	ssl3_digest_cached_records@PLT
	testl	%eax, %eax
	jne	.L157
.L140:
	xorl	%eax, %eax
.L138:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L158
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L139:
	.cfi_restore_state
	movq	168(%rdi), %rax
	pushq	$1
	xorl	%r9d, %r9d
	movl	$32, %r8d
	pushq	$48
	leaq	184(%rax), %rcx
	addq	$152, %rax
	pushq	%rsi
	leaq	.LC3(%rip), %rsi
	pushq	%r15
	pushq	%rdx
	movl	$13, %edx
	pushq	$32
	pushq	%rax
	pushq	$0
	call	tls1_PRF.constprop.0
	addq	$64, %rsp
	testl	%eax, %eax
	je	.L138
.L141:
	movq	$48, 0(%r13)
	movl	$1, %eax
	jmp	.L138
	.p2align 4,,10
	.p2align 3
.L157:
	leaq	-192(%rbp), %r10
	leaq	-200(%rbp), %rcx
	movl	$128, %edx
	movq	%r12, %rdi
	movq	%r10, %rsi
	movq	%r10, -216(%rbp)
	call	ssl_handshake_hash@PLT
	testl	%eax, %eax
	je	.L140
	pushq	$1
	movq	-216(%rbp), %r10
	xorl	%r9d, %r9d
	movq	%r12, %rdi
	pushq	$48
	movq	-200(%rbp), %r8
	movl	$22, %edx
	leaq	.LC2(%rip), %rsi
	pushq	%r14
	movq	%r10, %rcx
	pushq	%r15
	pushq	%rbx
	pushq	$0
	pushq	$0
	pushq	$0
	call	tls1_PRF.constprop.0
	addq	$64, %rsp
	testl	%eax, %eax
	je	.L140
	movq	-216(%rbp), %r10
	movq	-200(%rbp), %rsi
	movq	%r10, %rdi
	call	OPENSSL_cleanse@PLT
	jmp	.L141
.L158:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE969:
	.size	tls1_generate_master_secret, .-tls1_generate_master_secret
	.section	.rodata.str1.1
.LC4:
	.string	"client finished"
.LC5:
	.string	"server finished"
	.text
	.p2align 4
	.globl	tls1_export_keying_material
	.type	tls1_export_keying_material, @function
tls1_export_keying_material:
.LFB970:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	64(%r8), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$40, %rsp
	movl	24(%rbp), %eax
	movq	%rsi, -64(%rbp)
	movq	%rdx, -72(%rbp)
	movq	%r9, -56(%rbp)
	testl	%eax, %eax
	je	.L160
	movq	16(%rbp), %rax
	movl	$542, %edx
	leaq	.LC0(%rip), %rsi
	leaq	2(%r12,%rax), %r12
	movq	%r12, %rdi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L176
	movq	%r14, %rdx
	movq	%rbx, %rsi
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	168(%r13), %rdx
	leaq	(%r15,%r14), %rax
	movdqu	184(%rdx), %xmm0
	movups	%xmm0, (%rax)
	movdqu	200(%rdx), %xmm1
	movups	%xmm1, 16(%rax)
	movq	168(%r13), %rdx
	leaq	32(%r15,%r14), %rax
	movdqu	152(%rdx), %xmm2
	movups	%xmm2, (%rax)
	movdqu	168(%rdx), %xmm3
	movups	%xmm3, 16(%rax)
	movq	16(%rbp), %rax
	shrq	$8, %rax
	movb	%al, 64(%r15,%r14)
	movzbl	16(%rbp), %eax
	movb	%al, 65(%r15,%r14)
	movq	-56(%rbp), %rax
	orq	16(%rbp), %rax
	je	.L175
	movq	16(%rbp), %rdx
	movq	-56(%rbp), %rsi
	leaq	66(%r15,%r14), %rdi
	call	memcpy@PLT
	movabsq	$7359009808620022883, %rax
	cmpq	%rax, (%r15)
	jne	.L163
	.p2align 4,,10
	.p2align 3
.L187:
	cmpl	$1936289385, 8(%r15)
	jne	.L163
	cmpw	$25960, 12(%r15)
	jne	.L163
	cmpb	$100, 14(%r15)
	jne	.L163
	jmp	.L165
	.p2align 4,,10
	.p2align 3
.L160:
	movl	$542, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L176
	movq	%r14, %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	memcpy@PLT
	movq	168(%r13), %rdx
	leaq	(%r15,%r14), %rax
	movdqu	184(%rdx), %xmm4
	movups	%xmm4, (%rax)
	movdqu	200(%rdx), %xmm5
	movups	%xmm5, 16(%rax)
	movq	168(%r13), %rdx
	leaq	32(%r15,%r14), %rax
	movdqu	152(%rdx), %xmm6
	movups	%xmm6, (%rax)
	movdqu	168(%rdx), %xmm7
	movups	%xmm7, 16(%rax)
.L175:
	movabsq	$7359009808620022883, %rax
	cmpq	%rax, (%r15)
	je	.L187
.L163:
	movabsq	$7359007571227862387, %rax
	cmpq	%rax, (%r15)
	je	.L188
.L166:
	movabsq	$8295756293687435629, %rax
	cmpq	%rax, (%r15)
	je	.L189
.L168:
	movabsq	$2338042707385085216, %rdx
	xorq	8(%r15), %rdx
	movabsq	$7234298801751881829, %rax
	xorq	(%r15), %rax
	orq	%rax, %rdx
	je	.L190
.L170:
	movabsq	$7021244194802460011, %rax
	cmpq	%rax, (%r15)
	je	.L191
.L172:
	movq	1296(%r13), %rax
	pushq	$0
	movq	%r13, %rdi
	xorl	%r9d, %r9d
	pushq	-72(%rbp)
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r12, %rdx
	pushq	-64(%rbp)
	addq	$80, %rax
	movq	%r15, %rsi
	pushq	-72(%rax)
	pushq	%rax
	pushq	$0
	pushq	$0
	pushq	$0
	call	tls1_PRF.constprop.0
	addq	$64, %rsp
	movl	%eax, %r13d
.L162:
	movq	%r12, %rsi
	movq	%r15, %rdi
	movl	$602, %ecx
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_clear_free@PLT
	leaq	-40(%rbp), %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L190:
	.cfi_restore_state
	cmpl	$1919116659, 16(%r15)
	jne	.L170
	cmpw	$29797, 20(%r15)
	jne	.L170
	jmp	.L165
	.p2align 4,,10
	.p2align 3
.L176:
	movl	$65, %edx
	movl	$314, %esi
	xorl	%r15d, %r15d
	xorl	%r13d, %r13d
	movl	$599, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$20, %edi
	call	ERR_put_error@PLT
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L191:
	cmpl	$1869181806, 8(%r15)
	jne	.L172
	cmpb	$110, 12(%r15)
	jne	.L172
	jmp	.L165
	.p2align 4,,10
	.p2align 3
.L188:
	cmpl	$1936289385, 8(%r15)
	jne	.L166
	cmpw	$25960, 12(%r15)
	jne	.L166
	cmpb	$100, 14(%r15)
	jne	.L166
	jmp	.L165
	.p2align 4,,10
	.p2align 3
.L189:
	cmpl	$1701995365, 8(%r15)
	jne	.L168
	cmpb	$116, 12(%r15)
	jne	.L168
.L165:
	movl	$595, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$367, %edx
	xorl	%r13d, %r13d
	movl	$314, %esi
	movl	$20, %edi
	call	ERR_put_error@PLT
	jmp	.L162
	.cfi_endproc
.LFE970:
	.size	tls1_export_keying_material, .-tls1_export_keying_material
	.p2align 4
	.globl	tls1_alert_code
	.type	tls1_alert_code, @function
tls1_alert_code:
.LFB971:
	.cfi_startproc
	endbr64
	movl	$-1, %eax
	cmpl	$120, %edi
	ja	.L192
	movl	%edi, %edi
	leaq	CSWTCH.20(%rip), %rax
	movsbl	(%rax,%rdi), %eax
.L192:
	ret
	.cfi_endproc
.LFE971:
	.size	tls1_alert_code, .-tls1_alert_code
	.section	.rodata
	.align 32
	.type	CSWTCH.20, @object
	.size	CSWTCH.20, 121
CSWTCH.20:
	.byte	0
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	10
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	20
	.byte	21
	.byte	22
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	30
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	40
	.byte	-1
	.byte	42
	.byte	43
	.byte	44
	.byte	45
	.byte	46
	.byte	47
	.byte	48
	.byte	49
	.byte	50
	.byte	51
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	60
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	70
	.byte	71
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	80
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	86
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	90
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	100
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	110
	.byte	111
	.byte	112
	.byte	113
	.byte	114
	.byte	115
	.byte	40
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	120
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
