	.file	"pk7_smime.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/pkcs7/pk7_smime.c"
	.text
	.p2align 4
	.globl	PKCS7_final
	.type	PKCS7_final, @function
PKCS7_final:
.LFB1297:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	xorl	%esi, %esi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	call	PKCS7_dataInit@PLT
	testq	%rax, %rax
	je	.L8
	movq	%rax, %r12
	movl	%r15d, %edx
	movq	%rax, %rsi
	movq	%r14, %rdi
	call	SMIME_crlf_copy@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$11, %esi
	movq	%r12, %rdi
	call	BIO_ctrl@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	PKCS7_dataFinal@PLT
	movl	%eax, %r13d
	testl	%eax, %eax
	je	.L9
	movl	$1, %r13d
.L4:
	movq	%r12, %rdi
	call	BIO_free_all@PLT
	popq	%r12
	movl	%r13d, %eax
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	.cfi_restore_state
	xorl	%r13d, %r13d
	movl	$71, %r8d
	movl	$65, %edx
	movl	$134, %esi
	leaq	.LC0(%rip), %rcx
	movl	$33, %edi
	call	ERR_put_error@PLT
	popq	%r12
	movl	%r13d, %eax
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	.cfi_restore_state
	movl	$80, %r8d
	movl	$145, %edx
	movl	$134, %esi
	movl	$33, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L4
	.cfi_endproc
.LFE1297:
	.size	PKCS7_final, .-PKCS7_final
	.p2align 4
	.globl	PKCS7_sign_add_signer
	.type	PKCS7_sign_add_signer, @function
PKCS7_sign_add_signer:
.LFB1300:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movq	%rdx, %rsi
	pushq	%r12
	movq	%r13, %rdi
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%r8d, %ebx
	subq	$8, %rsp
	call	X509_check_private_key@PLT
	testl	%eax, %eax
	je	.L104
	movq	%r12, %rdx
	movq	%r15, %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	PKCS7_add_signature@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L105
	testb	$2, %bl
	je	.L14
.L16:
	movl	%ebx, %r15d
	andl	$256, %r15d
	je	.L106
.L10:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L106:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	PKCS7_add_attrib_content_type@PLT
	testl	%eax, %eax
	je	.L103
	testb	$2, %bh
	je	.L107
.L19:
	testb	$-128, %bh
	je	.L10
	movq	%r14, %rdi
	call	PKCS7_get_signer_info@PLT
	movq	%rax, %r14
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L50:
	movl	%r15d, %esi
	movq	%r14, %rdi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %r13
	cmpq	%rax, %r12
	je	.L47
	movq	24(%rax), %rdi
	call	OPENSSL_sk_num@PLT
	testl	%eax, %eax
	jle	.L48
	movq	16(%r13), %rdx
	movq	16(%r12), %rax
	movq	(%rdx), %rsi
	movq	(%rax), %rdi
	call	OBJ_cmp@PLT
	testl	%eax, %eax
	je	.L108
.L48:
	addl	$1, %r15d
.L46:
	movq	%r14, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r15d
	jl	.L50
.L47:
	movl	$199, %r8d
	movl	$154, %edx
	movl	$138, %esi
	movl	$33, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L103:
	xorl	%r13d, %r13d
.L17:
	movq	X509_ALGOR_free@GOTPCREL(%rip), %rsi
	movq	%r13, %rdi
	xorl	%r12d, %r12d
	call	OPENSSL_sk_pop_free@PLT
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L14:
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	PKCS7_add_certificate@PLT
	testl	%eax, %eax
	jne	.L16
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L107:
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L109
	movl	$427, %edi
	call	OBJ_nid2sn@PLT
	movq	%rax, %rdi
	call	EVP_get_cipherbyname@PLT
	testq	%rax, %rax
	jne	.L21
.L24:
	movl	$982, %edi
	call	OBJ_nid2sn@PLT
	movq	%rax, %rdi
	call	EVP_get_digestbyname@PLT
	testq	%rax, %rax
	jne	.L110
.L23:
	movl	$983, %edi
	call	OBJ_nid2sn@PLT
	movq	%rax, %rdi
	call	EVP_get_digestbyname@PLT
	testq	%rax, %rax
	jne	.L111
.L26:
	movl	$809, %edi
	call	OBJ_nid2sn@PLT
	movq	%rax, %rdi
	call	EVP_get_digestbyname@PLT
	testq	%rax, %rax
	jne	.L112
.L28:
	movl	$813, %edi
	call	OBJ_nid2sn@PLT
	movq	%rax, %rdi
	call	EVP_get_cipherbyname@PLT
	testq	%rax, %rax
	jne	.L113
.L30:
	movl	$423, %edi
	call	OBJ_nid2sn@PLT
	movq	%rax, %rdi
	call	EVP_get_cipherbyname@PLT
	testq	%rax, %rax
	jne	.L114
.L32:
	movl	$419, %edi
	call	OBJ_nid2sn@PLT
	movq	%rax, %rdi
	call	EVP_get_cipherbyname@PLT
	testq	%rax, %rax
	jne	.L115
.L34:
	movl	$44, %edi
	call	OBJ_nid2sn@PLT
	movq	%rax, %rdi
	call	EVP_get_cipherbyname@PLT
	testq	%rax, %rax
	jne	.L116
.L36:
	movl	$37, %edi
	call	OBJ_nid2sn@PLT
	movq	%rax, %rdi
	call	EVP_get_cipherbyname@PLT
	testq	%rax, %rax
	jne	.L117
.L38:
	movl	$37, %edi
	call	OBJ_nid2sn@PLT
	movq	%rax, %rdi
	call	EVP_get_cipherbyname@PLT
	testq	%rax, %rax
	jne	.L118
.L40:
	movl	$31, %edi
	call	OBJ_nid2sn@PLT
	movq	%rax, %rdi
	call	EVP_get_cipherbyname@PLT
	testq	%rax, %rax
	jne	.L119
.L42:
	movl	$37, %edi
	call	OBJ_nid2sn@PLT
	movq	%rax, %rdi
	call	EVP_get_cipherbyname@PLT
	testq	%rax, %rax
	jne	.L120
.L44:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	PKCS7_add_attrib_smimecap@PLT
	testl	%eax, %eax
	je	.L17
	movq	X509_ALGOR_free@GOTPCREL(%rip), %rsi
	movq	%r13, %rdi
	call	OPENSSL_sk_pop_free@PLT
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L104:
	movl	$116, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$127, %edx
	xorl	%r12d, %r12d
	movl	$137, %esi
	movl	$33, %edi
	call	ERR_put_error@PLT
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L21:
	movl	$-1, %edx
	movl	$427, %esi
	movq	%r13, %rdi
	call	PKCS7_simple_smimecap@PLT
	testl	%eax, %eax
	je	.L17
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L105:
	movl	$122, %r8d
	movl	$124, %edx
	movl	$137, %esi
	movl	$33, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L10
.L108:
	movq	24(%r13), %rdi
	call	PKCS7_digest_from_attributes@PLT
	testq	%rax, %rax
	je	.L47
	movq	8(%rax), %rsi
	movl	(%rax), %edx
	movq	%r12, %rdi
	call	PKCS7_add1_attrib_digest@PLT
	testl	%eax, %eax
	je	.L103
	andb	$64, %bh
	jne	.L10
	movq	%r12, %rdi
	call	PKCS7_SIGNER_INFO_sign@PLT
	testl	%eax, %eax
	jne	.L10
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L110:
	movl	$-1, %edx
	movl	$982, %esi
	movq	%r13, %rdi
	call	PKCS7_simple_smimecap@PLT
	testl	%eax, %eax
	je	.L17
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L111:
	movl	$-1, %edx
	movl	$983, %esi
	movq	%r13, %rdi
	call	PKCS7_simple_smimecap@PLT
	testl	%eax, %eax
	je	.L17
	jmp	.L26
.L112:
	movl	$-1, %edx
	movl	$809, %esi
	movq	%r13, %rdi
	call	PKCS7_simple_smimecap@PLT
	testl	%eax, %eax
	je	.L17
	jmp	.L28
.L113:
	movl	$-1, %edx
	movl	$813, %esi
	movq	%r13, %rdi
	call	PKCS7_simple_smimecap@PLT
	testl	%eax, %eax
	je	.L17
	jmp	.L30
.L109:
	movl	$138, %r8d
	movl	$65, %edx
	movl	$137, %esi
	movl	$33, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L17
.L114:
	movl	$-1, %edx
	movl	$423, %esi
	movq	%r13, %rdi
	call	PKCS7_simple_smimecap@PLT
	testl	%eax, %eax
	je	.L17
	jmp	.L32
.L115:
	movl	$-1, %edx
	movl	$419, %esi
	movq	%r13, %rdi
	call	PKCS7_simple_smimecap@PLT
	testl	%eax, %eax
	je	.L17
	jmp	.L34
.L116:
	movl	$-1, %edx
	movl	$44, %esi
	movq	%r13, %rdi
	call	PKCS7_simple_smimecap@PLT
	testl	%eax, %eax
	je	.L17
	jmp	.L36
.L117:
	movl	$128, %edx
	movl	$37, %esi
	movq	%r13, %rdi
	call	PKCS7_simple_smimecap@PLT
	testl	%eax, %eax
	je	.L17
	jmp	.L38
.L118:
	movl	$64, %edx
	movl	$37, %esi
	movq	%r13, %rdi
	call	PKCS7_simple_smimecap@PLT
	testl	%eax, %eax
	je	.L17
	jmp	.L40
.L119:
	orl	$-1, %edx
	movl	$31, %esi
	movq	%r13, %rdi
	call	PKCS7_simple_smimecap@PLT
	testl	%eax, %eax
	je	.L17
	jmp	.L42
.L120:
	movl	$40, %edx
	movl	$37, %esi
	movq	%r13, %rdi
	call	PKCS7_simple_smimecap@PLT
	testl	%eax, %eax
	je	.L17
	jmp	.L44
	.cfi_endproc
.LFE1300:
	.size	PKCS7_sign_add_signer, .-PKCS7_sign_add_signer
	.p2align 4
	.globl	PKCS7_sign
	.type	PKCS7_sign, @function
PKCS7_sign:
.LFB1296:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%r8d, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$24, %rsp
	movq	%rcx, -56(%rbp)
	call	PKCS7_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L145
	movl	$22, %esi
	movq	%rax, %rdi
	call	PKCS7_set_type@PLT
	testl	%eax, %eax
	je	.L125
	movl	$21, %esi
	movq	%r12, %rdi
	call	PKCS7_content_new@PLT
	testl	%eax, %eax
	je	.L125
	testq	%r14, %r14
	je	.L127
	xorl	%ecx, %ecx
	movl	%r13d, %r8d
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	PKCS7_sign_add_signer
	testq	%rax, %rax
	je	.L146
.L127:
	movl	%r13d, %r14d
	andl	$2, %r14d
	je	.L128
	testb	$64, %r13b
	jne	.L147
.L130:
	testl	$20480, %r13d
	jne	.L121
	movq	-56(%rbp), %rsi
	movl	%r13d, %edx
	movq	%r12, %rdi
	call	PKCS7_final
	testl	%eax, %eax
	jne	.L121
	.p2align 4,,10
	.p2align 3
.L125:
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	PKCS7_free@PLT
.L121:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L131:
	.cfi_restore_state
	movl	%r14d, %esi
	movq	%rbx, %rdi
	call	OPENSSL_sk_value@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	PKCS7_add_certificate@PLT
	testl	%eax, %eax
	je	.L125
	addl	$1, %r14d
.L128:
	movq	%rbx, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r14d
	jl	.L131
	testb	$64, %r13b
	je	.L130
.L147:
	xorl	%ecx, %ecx
	movl	$1, %edx
	movl	$1, %esi
	movq	%r12, %rdi
	call	PKCS7_ctrl@PLT
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L145:
	movl	$29, %r8d
	movl	$65, %edx
	movl	$116, %esi
	movl	$33, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L121
.L146:
	movl	$40, %r8d
	movl	$153, %edx
	movl	$116, %esi
	movl	$33, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L125
	.cfi_endproc
.LFE1296:
	.size	PKCS7_sign, .-PKCS7_sign
	.p2align 4
	.globl	PKCS7_get0_signers
	.type	PKCS7_get0_signers, @function
PKCS7_get0_signers:
.LFB1303:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%rdi, %rdi
	je	.L187
	movq	%rdi, %r14
	movq	24(%rdi), %rdi
	movq	%rsi, %r15
	movl	%edx, %ebx
	call	OBJ_obj2nid@PLT
	cmpl	$22, %eax
	jne	.L188
	movq	%r14, %rdi
	call	PKCS7_get_signer_info@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	OPENSSL_sk_num@PLT
	testl	%eax, %eax
	jle	.L189
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L153
	andl	$16, %ebx
	je	.L190
	xorl	%ebx, %ebx
	testq	%r15, %r15
	jne	.L161
	movq	%r12, %rdi
	call	OPENSSL_sk_num@PLT
	testl	%eax, %eax
	jle	.L148
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	OPENSSL_sk_value@PLT
.L162:
	movl	$420, %r8d
	movl	$128, %edx
	movl	$124, %esi
	movl	$33, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	%r13, %rdi
	xorl	%r13d, %r13d
	call	OPENSSL_sk_free@PLT
.L148:
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L190:
	.cfi_restore_state
	testq	%r15, %r15
	jne	.L155
	jmp	.L160
	.p2align 4,,10
	.p2align 3
.L191:
	movq	32(%r14), %rax
	movq	16(%rax), %rdi
	testq	%rdi, %rdi
	je	.L162
	movq	(%rcx), %rsi
	movq	8(%rcx), %rdx
	call	X509_find_by_issuer_and_serial@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L162
.L164:
	movq	%r13, %rdi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L159
	addl	$1, %ebx
.L155:
	movq	%r12, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jge	.L148
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	OPENSSL_sk_value@PLT
	movq	%r15, %rdi
	movq	8(%rax), %rcx
	movq	(%rcx), %rsi
	movq	8(%rcx), %rdx
	movq	%rcx, -56(%rbp)
	call	X509_find_by_issuer_and_serial@PLT
	movq	-56(%rbp), %rcx
	testq	%rax, %rax
	movq	%rax, %rsi
	jne	.L164
	jmp	.L191
	.p2align 4,,10
	.p2align 3
.L192:
	movq	(%rcx), %rsi
	movq	8(%rcx), %rdx
	call	X509_find_by_issuer_and_serial@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L162
	movq	%r13, %rdi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L159
	addl	$1, %ebx
.L160:
	movq	%r12, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jge	.L148
	movq	%r12, %rdi
	movl	%ebx, %esi
	call	OPENSSL_sk_value@PLT
	movq	8(%rax), %rcx
	movq	32(%r14), %rax
	movq	16(%rax), %rdi
	testq	%rdi, %rdi
	jne	.L192
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L193:
	movq	%r13, %rdi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L159
	addl	$1, %ebx
.L161:
	movq	%r12, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jge	.L148
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	OPENSSL_sk_value@PLT
	movq	%r15, %rdi
	movq	8(%rax), %rax
	movq	(%rax), %rsi
	movq	8(%rax), %rdx
	call	X509_find_by_issuer_and_serial@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	jne	.L193
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L188:
	movl	$388, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$113, %edx
	xorl	%r13d, %r13d
	movl	$124, %esi
	movl	$33, %edi
	call	ERR_put_error@PLT
	jmp	.L148
	.p2align 4,,10
	.p2align 3
.L159:
	movq	%r13, %rdi
	xorl	%r13d, %r13d
	call	OPENSSL_sk_free@PLT
	jmp	.L148
	.p2align 4,,10
	.p2align 3
.L189:
	movl	$397, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$142, %edx
	xorl	%r13d, %r13d
	movl	$124, %esi
	movl	$33, %edi
	call	ERR_put_error@PLT
	jmp	.L148
	.p2align 4,,10
	.p2align 3
.L187:
	movl	$383, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$143, %edx
	xorl	%r13d, %r13d
	movl	$124, %esi
	movl	$33, %edi
	call	ERR_put_error@PLT
	jmp	.L148
.L153:
	movl	$402, %r8d
	movl	$65, %edx
	movl	$124, %esi
	movl	$33, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L148
	.cfi_endproc
.LFE1303:
	.size	PKCS7_get0_signers, .-PKCS7_get0_signers
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"smime_sign"
.LC2:
	.string	"Verify error:"
	.text
	.p2align 4
	.globl	PKCS7_verify
	.type	PKCS7_verify, @function
PKCS7_verify:
.LFB1302:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, -72(%rbp)
	movq	%r8, -88(%rbp)
	movl	%r9d, -80(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L302
	movq	%rdi, %rbx
	movq	24(%rdi), %rdi
	movq	%rsi, %r12
	movq	%rdx, %r14
	call	OBJ_obj2nid@PLT
	cmpl	$22, %eax
	je	.L197
	movl	$223, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$113, %edx
	xorl	%r15d, %r15d
	movl	$117, %esi
	movl	$33, %edi
	call	ERR_put_error@PLT
.L194:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L303
	addq	$88, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L197:
	.cfi_restore_state
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$2, %esi
	movq	%rbx, %rdi
	call	PKCS7_ctrl@PLT
	cmpq	$0, -72(%rbp)
	movq	%rax, %r8
	sete	%al
	movb	%al, -105(%rbp)
	testq	%r8, %r8
	je	.L198
	testb	%al, %al
	jne	.L304
.L198:
	testl	$65536, -80(%rbp)
	je	.L199
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$2, %esi
	movq	%rbx, %rdi
	call	PKCS7_ctrl@PLT
	testq	%rax, %rax
	jne	.L199
	cmpq	$0, -72(%rbp)
	jne	.L305
.L199:
	movq	%rbx, %rdi
	call	PKCS7_get_signer_info@PLT
	movq	%rax, -104(%rbp)
	testq	%rax, %rax
	je	.L201
	movq	%rax, %rdi
	call	OPENSSL_sk_num@PLT
	testl	%eax, %eax
	jne	.L306
.L201:
	movl	$250, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$123, %edx
	xorl	%r15d, %r15d
	movl	$117, %esi
	movl	$33, %edi
	call	ERR_put_error@PLT
	jmp	.L194
	.p2align 4,,10
	.p2align 3
.L306:
	movl	-80(%rbp), %r13d
	movq	%r12, %rsi
	movq	%rbx, %rdi
	xorl	%r15d, %r15d
	movl	%r13d, %edx
	call	PKCS7_get0_signers
	movq	%rax, -120(%rbp)
	testq	%rax, %rax
	je	.L194
	call	X509_STORE_CTX_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L300
	movl	%r13d, %r15d
	andl	$32, %r15d
	jne	.L208
	movl	-80(%rbp), %eax
	movl	%eax, %r13d
	andl	$8, %r13d
	andl	$8192, %eax
	je	.L206
	testl	%r13d, %r13d
	jne	.L284
	movl	%r15d, -96(%rbp)
	movq	-120(%rbp), %r15
	jmp	.L207
	.p2align 4,,10
	.p2align 3
.L307:
	movq	%r12, %rdi
	addl	$1, %r13d
	call	X509_STORE_CTX_cleanup@PLT
.L207:
	movq	%r15, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%r13d, %eax
	jle	.L208
	movl	%r13d, %esi
	movq	%r15, %rdi
	call	OPENSSL_sk_value@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	32(%rbx), %rax
	movq	16(%rax), %rcx
	call	X509_STORE_CTX_init@PLT
	testl	%eax, %eax
	je	.L290
	movq	%r12, %rdi
	leaq	.LC1(%rip), %rsi
	call	X509_STORE_CTX_set_default@PLT
	movq	%r12, %rdi
	call	X509_verify_cert@PLT
	testl	%eax, %eax
	jg	.L307
.L295:
	movl	-96(%rbp), %r15d
.L210:
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	xorl	%r14d, %r14d
	call	X509_STORE_CTX_get_error@PLT
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	X509_STORE_CTX_cleanup@PLT
	movl	$284, %r8d
	movl	$117, %edx
	leaq	.LC0(%rip), %rcx
	movl	$117, %esi
	movl	$33, %edi
	call	ERR_put_error@PLT
	movslq	%ebx, %rdi
	call	X509_verify_cert_error_string@PLT
	leaq	.LC2(%rip), %rsi
	movl	$2, %edi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	ERR_add_error_data@PLT
.L202:
	movq	%r12, %rdi
	call	X509_STORE_CTX_free@PLT
	movl	$362, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r14, %rdi
	call	CRYPTO_free@PLT
	cmpq	$0, -72(%rbp)
	je	.L220
	cmpb	$0, -105(%rbp)
	jne	.L231
.L220:
	movq	%r13, %rdi
	call	BIO_free_all@PLT
	movq	-120(%rbp), %rdi
	call	OPENSSL_sk_free@PLT
	jmp	.L194
	.p2align 4,,10
	.p2align 3
.L284:
	movq	%rbx, -96(%rbp)
	xorl	%r13d, %r13d
	movq	-120(%rbp), %rbx
	jmp	.L211
	.p2align 4,,10
	.p2align 3
.L308:
	movl	%r13d, %esi
	movq	%rbx, %rdi
	call	OPENSSL_sk_value@PLT
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	X509_STORE_CTX_init@PLT
	testl	%eax, %eax
	je	.L209
	movq	%r12, %rdi
	call	X509_verify_cert@PLT
	testl	%eax, %eax
	jle	.L210
	movq	%r12, %rdi
	addl	$1, %r13d
	call	X509_STORE_CTX_cleanup@PLT
.L211:
	movq	%rbx, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%r13d, %eax
	jg	.L308
	movq	-96(%rbp), %rbx
	.p2align 4,,10
	.p2align 3
.L208:
	cmpq	$0, -72(%rbp)
	je	.L309
	movq	-72(%rbp), %rdi
	call	BIO_method_type@PLT
	cmpl	$1025, %eax
	je	.L310
	movq	-72(%rbp), %rsi
	movq	%rbx, %rdi
	call	PKCS7_dataInit@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L232
.L298:
	movb	$1, -105(%rbp)
.L233:
	movl	-80(%rbp), %r15d
	andl	$1, %r15d
	je	.L239
	call	BIO_s_mem@PLT
	movq	%rax, %rdi
	call	BIO_new@PLT
	movq	%rax, -96(%rbp)
	testq	%rax, %rax
	je	.L311
	movq	%rax, %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$130, %esi
	call	BIO_ctrl@PLT
.L221:
	movl	$325, %edx
	leaq	.LC0(%rip), %rsi
	movl	$4096, %edi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	jne	.L225
	jmp	.L312
	.p2align 4,,10
	.p2align 3
.L314:
	cmpq	$0, -96(%rbp)
	jne	.L313
.L225:
	movl	$4096, %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	BIO_read@PLT
	testl	%eax, %eax
	jg	.L314
	testl	%r15d, %r15d
	jne	.L315
.L226:
	movl	-80(%rbp), %r15d
	andl	$4, %r15d
	jne	.L301
	xorl	%eax, %eax
	movq	%r14, -80(%rbp)
	movq	-104(%rbp), %r14
	movq	%r12, -88(%rbp)
	movl	%eax, %r12d
	movl	%r15d, -96(%rbp)
	jmp	.L228
	.p2align 4,,10
	.p2align 3
.L230:
	movl	%r12d, %esi
	movq	%r14, %rdi
	call	OPENSSL_sk_value@PLT
	movq	-120(%rbp), %rdi
	movl	%r12d, %esi
	movq	%rax, %r15
	call	OPENSSL_sk_value@PLT
	movq	%r15, %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rax, %rcx
	call	PKCS7_signatureVerify@PLT
	testl	%eax, %eax
	jle	.L316
	addl	$1, %r12d
.L228:
	movq	%r14, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r12d
	jl	.L230
	movq	-80(%rbp), %r14
	movq	-88(%rbp), %r12
.L301:
	movl	$1, %r15d
	jmp	.L202
	.p2align 4,,10
	.p2align 3
.L302:
	movl	$218, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$143, %edx
	xorl	%r15d, %r15d
	movl	$117, %esi
	movl	$33, %edi
	call	ERR_put_error@PLT
	jmp	.L194
	.p2align 4,,10
	.p2align 3
.L305:
	movl	$242, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$118, %edx
	xorl	%r15d, %r15d
	movl	$117, %esi
	movl	$33, %edi
	call	ERR_put_error@PLT
	jmp	.L194
	.p2align 4,,10
	.p2align 3
.L304:
	movl	$229, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$122, %edx
	xorl	%r15d, %r15d
	movl	$117, %esi
	movl	$33, %edi
	call	ERR_put_error@PLT
	jmp	.L194
	.p2align 4,,10
	.p2align 3
.L309:
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	PKCS7_dataInit@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	jne	.L298
	movq	%r12, %rdi
	xorl	%r15d, %r15d
	call	X509_STORE_CTX_free@PLT
	movl	$362, %edx
	leaq	.LC0(%rip), %rsi
	xorl	%edi, %edi
	call	CRYPTO_free@PLT
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L313:
	movq	-96(%rbp), %rdi
	movl	%eax, %edx
	movq	%r14, %rsi
	call	BIO_write@PLT
	jmp	.L225
.L294:
	movl	-96(%rbp), %r15d
.L209:
	movl	$274, %r8d
.L299:
	movl	$11, %edx
	movl	$117, %esi
	movl	$33, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L300:
	xorl	%r13d, %r13d
	xorl	%r14d, %r14d
	jmp	.L202
	.p2align 4,,10
	.p2align 3
.L239:
	movq	-88(%rbp), %rax
	movq	%rax, -96(%rbp)
	jmp	.L221
.L232:
	movq	%r12, %rdi
	xorl	%r15d, %r15d
	call	X509_STORE_CTX_free@PLT
	movl	$362, %edx
	leaq	.LC0(%rip), %rsi
	xorl	%edi, %edi
	call	CRYPTO_free@PLT
.L231:
	movq	%r13, %rdi
	call	BIO_pop@PLT
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L206:
	testl	%r13d, %r13d
	jne	.L292
	movl	%r15d, -96(%rbp)
	movq	-120(%rbp), %r15
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L285:
	movq	%r12, %rdi
	addl	$1, %r13d
	call	X509_STORE_CTX_cleanup@PLT
.L215:
	movq	%r15, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r13d
	jge	.L208
	movl	%r13d, %esi
	movq	%r15, %rdi
	call	OPENSSL_sk_value@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	32(%rbx), %rax
	movq	16(%rax), %rcx
	call	X509_STORE_CTX_init@PLT
	testl	%eax, %eax
	je	.L290
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	X509_STORE_CTX_set_default@PLT
	movq	32(%rbx), %rax
	movq	%r12, %rdi
	movq	24(%rax), %rsi
	call	X509_STORE_CTX_set0_crls@PLT
	movq	%r12, %rdi
	call	X509_verify_cert@PLT
	testl	%eax, %eax
	jg	.L285
	jmp	.L295
	.p2align 4,,10
	.p2align 3
.L315:
	movq	-88(%rbp), %rsi
	movq	-96(%rbp), %rdi
	call	SMIME_text@PLT
	movl	%eax, %r15d
	testl	%eax, %eax
	je	.L317
	movq	-96(%rbp), %rdi
	call	BIO_free@PLT
	jmp	.L226
.L316:
	movl	$353, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$105, %edx
	movl	$117, %esi
	movl	$33, %edi
	movq	-80(%rbp), %r14
	movq	-88(%rbp), %r12
	movl	-96(%rbp), %r15d
	call	ERR_put_error@PLT
	jmp	.L202
.L310:
	movq	-72(%rbp), %rdi
	leaq	-64(%rbp), %rcx
	xorl	%edx, %edx
	movl	$3, %esi
	call	BIO_ctrl@PLT
	movq	-64(%rbp), %rdi
	movq	%rax, %rsi
	call	BIO_new_mem_buf@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L318
	movq	%rax, %rsi
	movq	%rbx, %rdi
	call	PKCS7_dataInit@PLT
	cmpq	%r14, -72(%rbp)
	movq	%rax, %r13
	sete	-105(%rbp)
	testq	%rax, %rax
	jne	.L233
	xorl	%r14d, %r14d
	xorl	%r15d, %r15d
	jmp	.L202
.L290:
	movl	-96(%rbp), %r15d
	movl	$269, %r8d
	jmp	.L299
.L292:
	movl	%r15d, -96(%rbp)
	movl	%eax, %r13d
	movq	-120(%rbp), %r15
	jmp	.L216
	.p2align 4,,10
	.p2align 3
.L319:
	movq	%r12, %rdi
	addl	$1, %r13d
	call	X509_STORE_CTX_cleanup@PLT
.L216:
	movq	%r15, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r13d
	jge	.L208
	movl	%r13d, %esi
	movq	%r15, %rdi
	call	OPENSSL_sk_value@PLT
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	X509_STORE_CTX_init@PLT
	testl	%eax, %eax
	je	.L294
	movq	32(%rbx), %rax
	movq	%r12, %rdi
	movq	24(%rax), %rsi
	call	X509_STORE_CTX_set0_crls@PLT
	movq	%r12, %rdi
	call	X509_verify_cert@PLT
	testl	%eax, %eax
	jg	.L319
	jmp	.L295
.L317:
	movl	$339, %r8d
	movl	$129, %edx
	movl	$117, %esi
	movl	$33, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	-96(%rbp), %rdi
	call	BIO_free@PLT
	jmp	.L202
.L311:
	movl	$65, %edx
	movl	$117, %esi
	xorl	%r14d, %r14d
	xorl	%r15d, %r15d
	movl	$317, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$33, %edi
	call	ERR_put_error@PLT
	jmp	.L202
.L312:
	movl	$326, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r15d, %r15d
	movl	$117, %esi
	movl	$33, %edi
	call	ERR_put_error@PLT
	jmp	.L202
.L318:
	movl	$65, %edx
	movl	$117, %esi
	xorl	%r13d, %r13d
	xorl	%r15d, %r15d
	movl	$306, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$33, %edi
	call	ERR_put_error@PLT
	movq	%r12, %rdi
	call	X509_STORE_CTX_free@PLT
	movl	$362, %edx
	leaq	.LC0(%rip), %rsi
	xorl	%edi, %edi
	call	CRYPTO_free@PLT
	jmp	.L220
.L303:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1302:
	.size	PKCS7_verify, .-PKCS7_verify
	.p2align 4
	.globl	PKCS7_encrypt
	.type	PKCS7_encrypt, @function
PKCS7_encrypt:
.LFB1304:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%ecx, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$8, %rsp
	call	PKCS7_new@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L332
	movl	$23, %esi
	movq	%rax, %rdi
	call	PKCS7_set_type@PLT
	testl	%eax, %eax
	je	.L324
	movq	%rbx, %rsi
	movq	%r13, %rdi
	xorl	%ebx, %ebx
	call	PKCS7_set_cipher@PLT
	testl	%eax, %eax
	jne	.L325
	jmp	.L333
	.p2align 4,,10
	.p2align 3
.L327:
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	OPENSSL_sk_value@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	PKCS7_add_recipient@PLT
	testq	%rax, %rax
	je	.L334
	addl	$1, %ebx
.L325:
	movq	%r12, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jl	.L327
	testl	$4096, %r14d
	jne	.L320
	movl	%r14d, %edx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	PKCS7_final
	testl	%eax, %eax
	jne	.L320
	.p2align 4,,10
	.p2align 3
.L324:
	xorl	%edi, %edi
	call	BIO_free_all@PLT
	movq	%r13, %rdi
	xorl	%r13d, %r13d
	call	PKCS7_free@PLT
.L320:
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L334:
	.cfi_restore_state
	movl	$458, %r8d
	movl	$120, %edx
	movl	$115, %esi
	movl	$33, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L324
	.p2align 4,,10
	.p2align 3
.L332:
	movl	$444, %r8d
	movl	$65, %edx
	movl	$115, %esi
	movl	$33, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L320
	.p2align 4,,10
	.p2align 3
.L333:
	movl	$451, %r8d
	movl	$121, %edx
	movl	$115, %esi
	movl	$33, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L324
	.cfi_endproc
.LFE1304:
	.size	PKCS7_encrypt, .-PKCS7_encrypt
	.p2align 4
	.globl	PKCS7_decrypt
	.type	PKCS7_decrypt, @function
PKCS7_decrypt:
.LFB1305:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%r8d, -52(%rbp)
	testq	%rdi, %rdi
	je	.L355
	movq	%rdi, %r12
	movq	24(%rdi), %rdi
	movq	%rsi, %rbx
	movq	%rdx, %r15
	movq	%rcx, %r13
	call	OBJ_obj2nid@PLT
	cmpl	$23, %eax
	jne	.L356
	testq	%r15, %r15
	je	.L339
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	X509_check_private_key@PLT
	movl	%eax, %r14d
	testl	%eax, %eax
	je	.L357
.L339:
	movq	%r12, %rdi
	movq	%r15, %rcx
	xorl	%edx, %edx
	movq	%rbx, %rsi
	call	PKCS7_dataDecode@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L358
	movl	-52(%rbp), %r14d
	andl	$1, %r14d
	je	.L341
	call	BIO_f_buffer@PLT
	movq	%rax, %rdi
	call	BIO_new@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L359
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	BIO_push@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L360
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	SMIME_text@PLT
	movl	%eax, %r14d
	testl	%eax, %eax
	jg	.L361
.L344:
	movq	%r15, %rdi
	call	BIO_free_all@PLT
	jmp	.L335
	.p2align 4,,10
	.p2align 3
.L356:
	movl	$489, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$113, %edx
	xorl	%r14d, %r14d
	movl	$114, %esi
	movl	$33, %edi
	call	ERR_put_error@PLT
.L335:
	addq	$24, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L341:
	.cfi_restore_state
	movl	$526, %edx
	leaq	.LC0(%rip), %rsi
	movl	$4096, %edi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	jne	.L345
	movl	$527, %r8d
	movl	$65, %edx
	movl	$114, %esi
	movl	$33, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	.p2align 4,,10
	.p2align 3
.L346:
	movq	%r15, %rdi
	movl	$546, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%r12, %rdi
	call	BIO_free_all@PLT
	jmp	.L335
	.p2align 4,,10
	.p2align 3
.L347:
	movl	%eax, %edx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	BIO_write@PLT
	cmpl	%ebx, %eax
	jne	.L346
.L345:
	movl	$4096, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	BIO_read@PLT
	movl	%eax, %ebx
	testl	%eax, %eax
	jg	.L347
	movq	%r12, %rdi
	movl	$1, %r14d
	call	BIO_method_type@PLT
	cmpl	$522, %eax
	jne	.L346
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$113, %esi
	movq	%r12, %rdi
	call	BIO_ctrl@PLT
	xorl	%r14d, %r14d
	testq	%rax, %rax
	setne	%r14b
	jmp	.L346
	.p2align 4,,10
	.p2align 3
.L355:
	movl	$484, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$143, %edx
	xorl	%r14d, %r14d
	movl	$114, %esi
	movl	$33, %edi
	call	ERR_put_error@PLT
	jmp	.L335
	.p2align 4,,10
	.p2align 3
.L357:
	movl	$494, %r8d
	movl	$127, %edx
	movl	$114, %esi
	movl	$33, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L335
	.p2align 4,,10
	.p2align 3
.L360:
	movl	$513, %r8d
	movl	$65, %edx
	movl	$114, %esi
	movl	$33, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	%r14, %rdi
	xorl	%r14d, %r14d
	call	BIO_free_all@PLT
	movq	%r12, %rdi
	call	BIO_free_all@PLT
	jmp	.L335
	.p2align 4,,10
	.p2align 3
.L361:
	movq	%r12, %rdi
	call	BIO_method_type@PLT
	cmpl	$522, %eax
	jne	.L344
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$113, %esi
	movq	%r12, %rdi
	call	BIO_ctrl@PLT
	testq	%rax, %rax
	movl	$0, %eax
	cmove	%eax, %r14d
	jmp	.L344
	.p2align 4,,10
	.p2align 3
.L359:
	movl	$508, %r8d
	movl	$65, %edx
	movl	$114, %esi
	movl	$33, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	%r12, %rdi
	call	BIO_free_all@PLT
	jmp	.L335
	.p2align 4,,10
	.p2align 3
.L358:
	movl	$500, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$119, %edx
	xorl	%r14d, %r14d
	movl	$114, %esi
	movl	$33, %edi
	call	ERR_put_error@PLT
	jmp	.L335
	.cfi_endproc
.LFE1305:
	.size	PKCS7_decrypt, .-PKCS7_decrypt
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
