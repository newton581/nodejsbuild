	.file	"bn_sqr.c"
	.text
	.p2align 4
	.globl	bn_sqr_normal
	.type	bn_sqr_normal, @function
bn_sqr_normal:
.LFB254:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leal	(%rdx,%rdx), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	8(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	leal	-1(%rdx), %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$40, %rsp
	movl	%eax, -56(%rbp)
	cltq
	movq	$0, -8(%rdi,%rax,8)
	movq	%rsi, -64(%rbp)
	movl	%edx, -52(%rbp)
	movq	%rcx, -72(%rbp)
	movq	$0, (%rdi)
	testl	%r14d, %r14d
	jle	.L2
	movq	(%rsi), %rcx
	leaq	8(%rsi), %rbx
	movslq	%r14d, %rax
	movq	%r15, %rdi
	movl	%r14d, %edx
	movq	%rbx, %rsi
	leaq	(%r15,%rax,8), %r13
	call	bn_mul_words@PLT
	leaq	24(%r12), %r15
	movq	%rax, 0(%r13)
.L2:
	movl	-52(%rbp), %eax
	leal	-2(%rax), %r13d
	testl	%r13d, %r13d
	jle	.L3
	movslq	%r14d, %r8
	leaq	-8(%r15,%r8,8), %r14
	.p2align 4,,10
	.p2align 3
.L4:
	addq	$8, %rbx
	movq	-8(%rbx), %rcx
	movq	%r15, %rdi
	movl	%r13d, %edx
	movq	%rbx, %rsi
	addq	$16, %r15
	addq	$8, %r14
	call	bn_mul_add_words@PLT
	movq	%rax, -8(%r14)
	subl	$1, %r13d
	jne	.L4
.L3:
	movl	-56(%rbp), %r15d
	movq	%r12, %rdx
	movq	%r12, %rsi
	movq	%r12, %rdi
	movl	%r15d, %ecx
	call	bn_add_words@PLT
	movq	-72(%rbp), %rbx
	movl	-52(%rbp), %edx
	movq	-64(%rbp), %rsi
	movq	%rbx, %rdi
	call	bn_sqr_words@PLT
	addq	$40, %rsp
	movl	%r15d, %ecx
	movq	%rbx, %rdx
	movq	%r12, %rsi
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	bn_add_words@PLT
	.cfi_endproc
.LFE254:
	.size	bn_sqr_normal, .-bn_sqr_normal
	.p2align 4
	.globl	bn_sqr_recursive
	.type	bn_sqr_recursive, @function
bn_sqr_recursive:
.LFB255:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r10
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	cmpl	$4, %edx
	je	.L30
	movl	%edx, %r15d
	cmpl	$8, %edx
	je	.L31
	movq	%rcx, %r14
	cmpl	$15, %edx
	jle	.L32
	movl	%edx, %r13d
	movq	%r10, -80(%rbp)
	movslq	%r15d, %rbx
	sarl	%r13d
	salq	$3, %rbx
	movslq	%r13d, %rdi
	movl	%r13d, %edx
	leaq	0(,%rdi,8), %rax
	movq	%r10, %rdi
	movq	%rax, -72(%rbp)
	addq	%rsi, %rax
	movq	%rax, %rsi
	movq	%rax, -64(%rbp)
	call	bn_cmp_words@PLT
	movq	-80(%rbp), %r10
	leaq	(%r14,%rbx), %r9
	movl	%eax, %r8d
	leal	(%r15,%r15), %eax
	cltq
	testl	%r8d, %r8d
	leaq	(%r14,%rax,8), %rcx
	movq	%rcx, -56(%rbp)
	jg	.L33
	jne	.L16
	movq	%r9, %rdi
	movq	%rbx, %rdx
	xorl	%esi, %esi
	movq	%r10, -80(%rbp)
	call	memset@PLT
	movq	-80(%rbp), %r10
	movq	%rax, %r9
.L17:
	movq	-56(%rbp), %rcx
	movq	%r10, %rsi
	movl	%r13d, %edx
	movq	%r12, %rdi
	movq	%r9, -80(%rbp)
	addq	%r12, %rbx
	call	bn_sqr_recursive
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %rsi
	movq	%rbx, %rdi
	movl	%r13d, %edx
	call	bn_sqr_recursive
	movq	%rbx, %rdx
	movl	%r15d, %ecx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	bn_add_words@PLT
	movq	-80(%rbp), %r9
	movl	%r15d, %ecx
	movq	%r14, %rsi
	movq	%rax, %rbx
	movq	%r9, %rdx
	movq	%r9, %rdi
	movq	%r9, -56(%rbp)
	call	bn_sub_words@PLT
	movq	-72(%rbp), %rdi
	movq	-56(%rbp), %r9
	movl	%r15d, %ecx
	subl	%eax, %ebx
	addq	%r12, %rdi
	movq	%r9, %rdx
	movq	%rdi, %rsi
	call	bn_add_words@PLT
	addl	%eax, %ebx
	je	.L10
	addl	%r15d, %r13d
	movslq	%ebx, %rbx
	movslq	%r13d, %r13
	leaq	(%r12,%r13,8), %rdx
	addq	(%rdx), %rbx
	movq	%rbx, (%rdx)
	jnc	.L10
	.p2align 4,,10
	.p2align 3
.L23:
	addq	$8, %rdx
	addq	$1, (%rdx)
	je	.L23
.L10:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L33:
	.cfi_restore_state
	movq	-64(%rbp), %rdx
	movq	%r10, %rsi
	movl	%r13d, %ecx
	movq	%r14, %rdi
	movq	%r9, -88(%rbp)
	call	bn_sub_words@PLT
	movq	-80(%rbp), %r10
	movq	-88(%rbp), %r9
.L15:
	movq	-56(%rbp), %rcx
	movq	%r9, %rdi
	movl	%r13d, %edx
	movq	%r14, %rsi
	movq	%r10, -88(%rbp)
	movq	%r9, -80(%rbp)
	call	bn_sqr_recursive
	movq	-88(%rbp), %r10
	movq	-80(%rbp), %r9
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L32:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	bn_sqr_normal
	.p2align 4,,10
	.p2align 3
.L30:
	.cfi_restore_state
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	bn_sqr_comba4@PLT
	.p2align 4,,10
	.p2align 3
.L31:
	.cfi_restore_state
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	bn_sqr_comba8@PLT
	.p2align 4,,10
	.p2align 3
.L16:
	.cfi_restore_state
	movq	-64(%rbp), %rsi
	movq	%r10, %rdx
	movl	%r13d, %ecx
	movq	%r14, %rdi
	movq	%r9, -88(%rbp)
	movq	%r10, -80(%rbp)
	call	bn_sub_words@PLT
	movq	-88(%rbp), %r9
	movq	-80(%rbp), %r10
	jmp	.L15
	.cfi_endproc
.LFE255:
	.size	bn_sqr_recursive, .-bn_sqr_recursive
	.p2align 4
	.globl	bn_sqr_fixed_top
	.type	bn_sqr_fixed_top, @function
bn_sqr_fixed_top:
.LFB253:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$296, %rsp
	.cfi_offset 3, -56
	movl	8(%rsi), %r15d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r15d, %r15d
	jle	.L60
	movq	%rsi, %r14
	movq	%rdx, %rdi
	movq	%rdx, %r13
	movq	%r12, %rbx
	call	BN_CTX_start@PLT
	cmpq	%r12, %r14
	je	.L61
	movq	%r13, %rdi
	call	BN_CTX_get@PLT
	testq	%rbx, %rbx
	je	.L41
.L65:
	testq	%rax, %rax
	je	.L41
	movq	%rax, -336(%rbp)
	leal	(%r15,%r15), %eax
	movq	%rbx, %rdi
	movl	%eax, %esi
	movl	%eax, -324(%rbp)
	call	bn_wexpand@PLT
	testq	%rax, %rax
	je	.L41
	cmpl	$4, %r15d
	movq	-336(%rbp), %rdx
	je	.L62
	cmpl	$8, %r15d
	je	.L63
	cmpl	$15, %r15d
	jg	.L45
	movq	(%r14), %rsi
	movq	(%rbx), %rdi
	leaq	-320(%rbp), %rcx
	movl	%r15d, %edx
	call	bn_sqr_normal
.L43:
	movl	-324(%rbp), %eax
	movl	$0, 16(%rbx)
	movl	$1, %r14d
	movl	%eax, 8(%rbx)
	cmpq	%r12, %rbx
	je	.L40
	movq	%rbx, %rsi
	movq	%r12, %rdi
	xorl	%r14d, %r14d
	call	BN_copy@PLT
	testq	%rax, %rax
	setne	%r14b
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L41:
	xorl	%r14d, %r14d
.L40:
	movq	%r13, %rdi
	call	BN_CTX_end@PLT
.L34:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L64
	addq	$296, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L60:
	.cfi_restore_state
	movl	$0, 8(%rdi)
	movl	$1, %r14d
	movl	$0, 16(%rdi)
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L61:
	movq	%r13, %rdi
	call	BN_CTX_get@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	BN_CTX_get@PLT
	testq	%rbx, %rbx
	jne	.L65
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L45:
	movslq	%r15d, %rdi
	movq	%rdx, -336(%rbp)
	call	BN_num_bits_word@PLT
	movq	-336(%rbp), %rdx
	leal	-1(%rax), %ecx
	movl	$1, %eax
	sall	%cl, %eax
	cmpl	%eax, %r15d
	je	.L66
	movl	-324(%rbp), %esi
	movq	%rdx, %rdi
	movq	%rdx, -336(%rbp)
	call	bn_wexpand@PLT
	testq	%rax, %rax
	je	.L41
	movq	-336(%rbp), %rdx
	movq	(%r14), %rsi
	movq	(%rbx), %rdi
	movq	(%rdx), %rcx
	movl	%r15d, %edx
	call	bn_sqr_normal
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L62:
	movq	(%r14), %rsi
	movq	(%rbx), %rdi
	call	bn_sqr_comba4@PLT
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L66:
	movl	$4, %esi
	movq	%rdx, %rdi
	sall	%cl, %esi
	call	bn_wexpand@PLT
	testq	%rax, %rax
	je	.L41
	movq	-336(%rbp), %rdx
	movq	(%r14), %rsi
	movq	(%rbx), %rdi
	movq	(%rdx), %rcx
	movl	%r15d, %edx
	call	bn_sqr_recursive
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L63:
	movq	(%r14), %rsi
	movq	(%rbx), %rdi
	call	bn_sqr_comba8@PLT
	jmp	.L43
.L64:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE253:
	.size	bn_sqr_fixed_top, .-bn_sqr_fixed_top
	.p2align 4
	.globl	BN_sqr
	.type	BN_sqr, @function
BN_sqr:
.LFB252:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$296, %rsp
	.cfi_offset 3, -56
	movl	8(%rsi), %r15d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r15d, %r15d
	jle	.L93
	movq	%rsi, %r14
	movq	%rdx, %rdi
	movq	%rdx, %r13
	movq	%r12, %rbx
	call	BN_CTX_start@PLT
	cmpq	%r14, %r12
	je	.L94
	movq	%r13, %rdi
	call	BN_CTX_get@PLT
	testq	%rbx, %rbx
	je	.L74
.L98:
	testq	%rax, %rax
	je	.L74
	movq	%rax, -336(%rbp)
	leal	(%r15,%r15), %eax
	movq	%rbx, %rdi
	movl	%eax, %esi
	movl	%eax, -324(%rbp)
	call	bn_wexpand@PLT
	testq	%rax, %rax
	je	.L74
	cmpl	$4, %r15d
	movq	-336(%rbp), %rdx
	je	.L95
	cmpl	$8, %r15d
	je	.L96
	cmpl	$15, %r15d
	jg	.L78
	movq	(%r14), %rsi
	movq	(%rbx), %rdi
	leaq	-320(%rbp), %rcx
	movl	%r15d, %edx
	call	bn_sqr_normal
.L76:
	movl	-324(%rbp), %eax
	movl	$0, 16(%rbx)
	movl	$1, %r14d
	movl	%eax, 8(%rbx)
	cmpq	%rbx, %r12
	je	.L73
	movq	%rbx, %rsi
	movq	%r12, %rdi
	xorl	%r14d, %r14d
	call	BN_copy@PLT
	testq	%rax, %rax
	setne	%r14b
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L74:
	xorl	%r14d, %r14d
.L73:
	movq	%r13, %rdi
	call	BN_CTX_end@PLT
.L69:
	movq	%r12, %rdi
	call	bn_correct_top@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L97
	addq	$296, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L93:
	.cfi_restore_state
	movl	$0, 8(%rdi)
	movl	$1, %r14d
	movl	$0, 16(%rdi)
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L94:
	movq	%r13, %rdi
	call	BN_CTX_get@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	BN_CTX_get@PLT
	testq	%rbx, %rbx
	jne	.L98
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L78:
	movslq	%r15d, %rdi
	movq	%rdx, -336(%rbp)
	call	BN_num_bits_word@PLT
	movq	-336(%rbp), %rdx
	leal	-1(%rax), %ecx
	movl	$1, %eax
	sall	%cl, %eax
	cmpl	%eax, %r15d
	je	.L99
	movl	-324(%rbp), %esi
	movq	%rdx, %rdi
	movq	%rdx, -336(%rbp)
	call	bn_wexpand@PLT
	testq	%rax, %rax
	je	.L74
	movq	-336(%rbp), %rdx
	movq	(%r14), %rsi
	movq	(%rbx), %rdi
	movq	(%rdx), %rcx
	movl	%r15d, %edx
	call	bn_sqr_normal
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L95:
	movq	(%r14), %rsi
	movq	(%rbx), %rdi
	call	bn_sqr_comba4@PLT
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L99:
	movl	$4, %esi
	movq	%rdx, %rdi
	sall	%cl, %esi
	call	bn_wexpand@PLT
	testq	%rax, %rax
	je	.L74
	movq	-336(%rbp), %rdx
	movq	(%r14), %rsi
	movq	(%rbx), %rdi
	movq	(%rdx), %rcx
	movl	%r15d, %edx
	call	bn_sqr_recursive
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L96:
	movq	(%r14), %rsi
	movq	(%rbx), %rdi
	call	bn_sqr_comba8@PLT
	jmp	.L76
.L97:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE252:
	.size	BN_sqr, .-BN_sqr
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
