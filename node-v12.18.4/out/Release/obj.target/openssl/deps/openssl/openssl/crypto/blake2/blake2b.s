	.file	"blake2b.c"
	.text
	.p2align 4
	.type	blake2b_compress, @function
blake2b_compress:
.LFB163:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$472, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movdqu	(%rdi), %xmm0
	movdqu	16(%rdi), %xmm2
	movq	%rdi, -352(%rbp)
	movq	%rsi, -360(%rbp)
	movdqu	32(%rdi), %xmm4
	movq	%rdx, -504(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	$128, %rdx
	movl	$128, %eax
	movaps	%xmm0, -208(%rbp)
	cmovbe	%rdx, %rax
	movq	%rdi, %rdx
	movq	-208(%rbp), %r8
	movaps	%xmm0, -192(%rbp)
	movq	80(%rdx), %rbx
	movdqu	48(%rdx), %xmm6
	movaps	%xmm2, -208(%rbp)
	movq	%rax, -368(%rbp)
	movq	%rax, %r14
	movq	-208(%rbp), %rax
	movq	%rbx, -488(%rbp)
	movq	88(%rdx), %rbx
	movaps	%xmm4, -208(%rbp)
	movq	-208(%rbp), %rdi
	movq	%rax, %r10
	movaps	%xmm6, -208(%rbp)
	movq	-208(%rbp), %r12
	movaps	%xmm2, -176(%rbp)
	movq	%rdi, %r11
	movaps	%xmm4, -160(%rbp)
	movq	%r12, %rsi
	movaps	%xmm6, -144(%rbp)
	movq	%rbx, -496(%rbp)
	movq	72(%rdx), %r13
	movq	%r8, %rbx
	movq	-152(%rbp), %rcx
	movq	-184(%rbp), %r15
	movq	%r13, -336(%rbp)
	movq	(%rdx), %r13
	addq	64(%rdx), %r14
	movq	%rcx, %rdi
	movq	-168(%rbp), %r9
	movq	%r15, %r12
	movq	%r13, -424(%rbp)
	movq	8(%rdx), %r13
	movq	%r14, -344(%rbp)
	movq	-136(%rbp), %rcx
	movq	%r13, -432(%rbp)
	movq	16(%rdx), %r13
	movq	%r13, -440(%rbp)
	movq	24(%rdx), %r13
	movq	%r13, -448(%rbp)
	movq	32(%rdx), %r13
	movq	%r13, -456(%rbp)
	movq	40(%rdx), %r13
	movq	%r13, -464(%rbp)
	movq	48(%rdx), %r13
	movq	%r13, -472(%rbp)
	movq	56(%rdx), %r13
	movq	%r13, -480(%rbp)
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L4:
	movq	%rcx, -480(%rbp)
	movq	%rsi, -472(%rbp)
	movq	%rdi, -464(%rbp)
	movq	%r11, -456(%rbp)
	movq	%r9, -448(%rbp)
	movq	%r10, -440(%rbp)
	movq	%r12, -432(%rbp)
	movq	%rbx, -424(%rbp)
.L2:
	movq	-360(%rbp), %rax
	movq	24(%rax), %r14
	movq	32(%rax), %r13
	movq	(%rax), %rdx
	movq	8(%rax), %r15
	movq	%r14, -208(%rbp)
	movq	40(%rax), %r14
	movq	%r13, -216(%rbp)
	movq	48(%rax), %r13
	movq	%r14, -224(%rbp)
	movq	56(%rax), %r14
	movq	%r13, -232(%rbp)
	movq	64(%rax), %r13
	movq	%r14, -240(%rbp)
	movq	72(%rax), %r14
	movq	%r13, -248(%rbp)
	movq	80(%rax), %r13
	movq	%r14, -256(%rbp)
	movq	88(%rax), %r14
	movq	%r13, -264(%rbp)
	movq	96(%rax), %r13
	movq	16(%rax), %r8
	movq	%r14, -272(%rbp)
	movq	%r13, -280(%rbp)
	movq	104(%rax), %r14
	movq	112(%rax), %r13
	movq	120(%rax), %rax
	movq	%r14, -288(%rbp)
	movq	-344(%rbp), %r14
	movq	%rax, -304(%rbp)
	movq	-352(%rbp), %rax
	movq	%r13, -296(%rbp)
	movq	%r14, 64(%rax)
	cmpq	-368(%rbp), %r14
	adcq	$0, -336(%rbp)
	movq	-336(%rbp), %r13
	movq	%rdx, -312(%rbp)
	movq	%r13, 72(%rax)
	leaq	(%r11,%rdx), %r13
	leaq	(%rdi,%r8), %rax
	movabsq	$5840696475078001361, %rdx
	addq	%rbx, %r13
	movq	%r14, %rbx
	addq	%rax, %r12
	movq	%r8, -328(%rbp)
	xorq	%r13, %rbx
	addq	%r15, %r13
	movq	%r15, -320(%rbp)
	movq	-232(%rbp), %r15
	xorq	%rdx, %rbx
	movabsq	$7640891576956012808, %rdx
	rolq	$32, %rbx
	leaq	(%rbx,%rdx), %r14
	xorq	%r14, %r11
	rorq	$24, %r11
	addq	%r11, %r13
	xorq	%r13, %rbx
	rorq	$16, %rbx
	addq	%rbx, %r14
	xorq	%r14, %r11
	rolq	%r11
	movq	%r11, -392(%rbp)
	movq	-336(%rbp), %r11
	xorq	%r12, %r11
	addq	-208(%rbp), %r12
	movq	%r11, %r8
	movabsq	$-7276294671716946913, %r11
	xorq	%r11, %r8
	movabsq	$-4942790177534073029, %r11
	rolq	$32, %r8
	leaq	(%r8,%r11), %rdx
	xorq	%rdx, %rdi
	rorq	$24, %rdi
	leaq	(%r12,%rdi), %rax
	movabsq	$2270897969802886507, %r12
	xorq	%rax, %r8
	rorq	$16, %r8
	leaq	(%rdx,%r8), %r11
	movq	%r11, -376(%rbp)
	xorq	%r11, %rdi
	movq	-216(%rbp), %r11
	rolq	%rdi
	leaq	(%rsi,%r11), %rdx
	addq	%r10, %rdx
	movq	-488(%rbp), %r10
	xorq	%rdx, %r10
	addq	-224(%rbp), %rdx
	xorq	%r12, %r10
	movabsq	$4354685564936845355, %r12
	rolq	$32, %r10
	leaq	(%r10,%r12), %r11
	leaq	(%rcx,%r15), %r12
	movabsq	$6620516959819538809, %r15
	xorq	%r11, %rsi
	rorq	$24, %rsi
	addq	%rsi, %rdx
	addq	%r12, %r9
	movq	-496(%rbp), %r12
	addq	-248(%rbp), %r13
	addq	%rdi, %r13
	xorq	%rdx, %r10
	xorq	%r9, %r12
	addq	-240(%rbp), %r9
	rorq	$16, %r10
	xorq	%r15, %r12
	addq	%r10, %r11
	movabsq	$-6534734903238641935, %r15
	rolq	$32, %r12
	xorq	%r11, %rsi
	addq	%r12, %r15
	rolq	%rsi
	xorq	%r15, %rcx
	rorq	$24, %rcx
	addq	%rcx, %r9
	xorq	%r9, %r12
	rorq	$16, %r12
	addq	%r12, %r15
	xorq	%r13, %r12
	addq	-256(%rbp), %r13
	rolq	$32, %r12
	xorq	%r15, %rcx
	addq	%r12, %r11
	rolq	%rcx
	xorq	%r11, %rdi
	rorq	$24, %rdi
	addq	%rdi, %r13
	xorq	%r13, %r12
	rorq	$16, %r12
	addq	%r12, %r11
	movq	%r12, -400(%rbp)
	movq	-392(%rbp), %r12
	xorq	%r11, %rdi
	addq	-264(%rbp), %rax
	addq	-280(%rbp), %rdx
	addq	%rsi, %rax
	addq	%rcx, %rdx
	addq	-296(%rbp), %r9
	rolq	%rdi
	xorq	%rax, %rbx
	xorq	%rdx, %r8
	addq	-272(%rbp), %rax
	addq	%r12, %r9
	rolq	$32, %rbx
	rolq	$32, %r8
	addq	-288(%rbp), %rdx
	addq	%rbx, %r15
	addq	%r8, %r14
	xorq	%r15, %rsi
	xorq	%r14, %rcx
	rorq	$24, %rsi
	rorq	$24, %rcx
	addq	%rsi, %rax
	addq	%rcx, %rdx
	xorq	%rax, %rbx
	xorq	%rdx, %r8
	rorq	$16, %rbx
	rorq	$16, %r8
	addq	%rbx, %r15
	addq	%r8, %r14
	xorq	%r15, %rsi
	xorq	%r14, %rcx
	movq	%r15, -384(%rbp)
	movq	-376(%rbp), %r15
	rolq	%rsi
	rolq	%rcx
	xorq	%r9, %r10
	addq	-304(%rbp), %r9
	rolq	$32, %r10
	addq	-296(%rbp), %r13
	addq	%r10, %r15
	xorq	%r15, %r12
	rorq	$24, %r12
	addq	%r12, %r9
	xorq	%r9, %r10
	rorq	$16, %r10
	addq	%r10, %r15
	xorq	%r15, %r12
	rolq	%r12
	addq	%r12, %r13
	xorq	%r13, %rbx
	addq	-264(%rbp), %r13
	rolq	$32, %rbx
	addq	%rbx, %r14
	xorq	%r14, %r12
	rorq	$24, %r12
	addq	%r12, %r13
	xorq	%r13, %rbx
	rorq	$16, %rbx
	addq	%rbx, %r14
	xorq	%r14, %r12
	rolq	%r12
	movq	%r12, -392(%rbp)
	addq	-216(%rbp), %rax
	addq	%rdi, %rax
	movq	-400(%rbp), %r12
	xorq	%rax, %r8
	rolq	$32, %r8
	addq	%r8, %r15
	xorq	%r15, %rdi
	rorq	$24, %rdi
	addq	-248(%rbp), %rax
	addq	-256(%rbp), %rdx
	addq	%rdi, %rax
	addq	%rsi, %rdx
	addq	-288(%rbp), %r9
	xorq	%rax, %r8
	xorq	%rdx, %r10
	addq	%rcx, %r9
	addq	-304(%rbp), %rdx
	rorq	$16, %r8
	rolq	$32, %r10
	xorq	%r9, %r12
	addq	-232(%rbp), %r9
	addq	%r8, %r15
	addq	%r10, %r11
	rolq	$32, %r12
	movq	%r15, -376(%rbp)
	xorq	%r15, %rdi
	xorq	%r11, %rsi
	movq	-384(%rbp), %r15
	rorq	$24, %rsi
	rolq	%rdi
	addq	%rsi, %rdx
	addq	%r12, %r15
	xorq	%rdx, %r10
	xorq	%r15, %rcx
	rorq	$16, %r10
	rorq	$24, %rcx
	addq	%r10, %r11
	addq	%rcx, %r9
	xorq	%r11, %rsi
	xorq	%r9, %r12
	rolq	%rsi
	rorq	$16, %r12
	addq	-320(%rbp), %r13
	addq	-312(%rbp), %rax
	addq	%rdi, %r13
	addq	%rsi, %rax
	addq	%r12, %r15
	xorq	%rax, %rbx
	xorq	%r13, %r12
	xorq	%r15, %rcx
	addq	-280(%rbp), %r13
	rolq	$32, %r12
	rolq	$32, %rbx
	addq	-328(%rbp), %rax
	addq	%r12, %r11
	addq	%rbx, %r15
	rolq	%rcx
	xorq	%r11, %rdi
	xorq	%r15, %rsi
	rorq	$24, %rdi
	rorq	$24, %rsi
	addq	%rdi, %r13
	addq	%rsi, %rax
	xorq	%r13, %r12
	xorq	%rax, %rbx
	rorq	$16, %r12
	rorq	$16, %rbx
	addq	%rbx, %r15
	addq	%r12, %r11
	movq	%r12, -400(%rbp)
	movq	-392(%rbp), %r12
	xorq	%r11, %rdi
	xorq	%r15, %rsi
	movq	%r15, -384(%rbp)
	movq	-376(%rbp), %r15
	rolq	%rdi
	rolq	%rsi
	addq	-272(%rbp), %rdx
	addq	-224(%rbp), %r9
	addq	%r12, %r9
	addq	%rcx, %rdx
	addq	-272(%rbp), %r13
	xorq	%r9, %r10
	xorq	%rdx, %r8
	addq	-208(%rbp), %r9
	addq	-240(%rbp), %rdx
	rolq	$32, %r10
	rolq	$32, %r8
	addq	%r10, %r15
	addq	%r8, %r14
	xorq	%r15, %r12
	xorq	%r14, %rcx
	rorq	$24, %r12
	rorq	$24, %rcx
	addq	%r12, %r9
	addq	%rcx, %rdx
	xorq	%r9, %r10
	xorq	%rdx, %r8
	rorq	$16, %r10
	rorq	$16, %r8
	addq	%r10, %r15
	addq	%r8, %r14
	xorq	%r15, %r12
	xorq	%r14, %rcx
	rolq	%r12
	rolq	%rcx
	addq	%r12, %r13
	xorq	%r13, %rbx
	rolq	$32, %rbx
	addq	%rbx, %r14
	addq	-280(%rbp), %rax
	addq	-248(%rbp), %r13
	addq	%rdi, %rax
	xorq	%r14, %r12
	addq	-224(%rbp), %rdx
	xorq	%rax, %r8
	rorq	$24, %r12
	addq	%rsi, %rdx
	addq	-312(%rbp), %rax
	rolq	$32, %r8
	addq	%r12, %r13
	xorq	%rdx, %r10
	addq	-328(%rbp), %rdx
	addq	%r8, %r15
	xorq	%r13, %rbx
	rolq	$32, %r10
	xorq	%r15, %rdi
	rorq	$16, %rbx
	addq	%r10, %r11
	rorq	$24, %rdi
	addq	%rbx, %r14
	xorq	%r11, %rsi
	addq	%rdi, %rax
	xorq	%r14, %r12
	rorq	$24, %rsi
	xorq	%rax, %r8
	rolq	%r12
	rorq	$16, %r8
	movq	%r12, -392(%rbp)
	movq	-400(%rbp), %r12
	addq	%r8, %r15
	xorq	%r15, %rdi
	movq	%r15, -376(%rbp)
	movq	-384(%rbp), %r15
	rolq	%rdi
	addq	%rsi, %rdx
	addq	-304(%rbp), %r9
	addq	%rcx, %r9
	xorq	%rdx, %r10
	xorq	%r9, %r12
	addq	-288(%rbp), %r9
	rorq	$16, %r10
	addq	-264(%rbp), %r13
	rolq	$32, %r12
	addq	%rdi, %r13
	addq	%r10, %r11
	addq	%r12, %r15
	xorq	%r11, %rsi
	xorq	%r15, %rcx
	rolq	%rsi
	rorq	$24, %rcx
	addq	%rcx, %r9
	xorq	%r9, %r12
	rorq	$16, %r12
	addq	%r12, %r15
	xorq	%r13, %r12
	addq	-296(%rbp), %r13
	rolq	$32, %r12
	xorq	%r15, %rcx
	addq	%r12, %r11
	rolq	%rcx
	xorq	%r11, %rdi
	rorq	$24, %rdi
	addq	%rdi, %r13
	xorq	%r13, %r12
	rorq	$16, %r12
	addq	%r12, %r11
	movq	%r12, -400(%rbp)
	movq	-392(%rbp), %r12
	xorq	%r11, %rdi
	addq	-208(%rbp), %rax
	addq	-240(%rbp), %rdx
	addq	%rsi, %rax
	addq	%rcx, %rdx
	addq	-256(%rbp), %r9
	rolq	%rdi
	xorq	%rax, %rbx
	xorq	%rdx, %r8
	addq	-232(%rbp), %rax
	addq	%r12, %r9
	rolq	$32, %rbx
	rolq	$32, %r8
	addq	-320(%rbp), %rdx
	addq	%rbx, %r15
	addq	%r8, %r14
	xorq	%r15, %rsi
	xorq	%r14, %rcx
	rorq	$24, %rsi
	rorq	$24, %rcx
	addq	%rsi, %rax
	addq	%rcx, %rdx
	xorq	%rax, %rbx
	xorq	%rdx, %r8
	rorq	$16, %rbx
	rorq	$16, %r8
	addq	%rbx, %r15
	addq	%r8, %r14
	xorq	%r15, %rsi
	xorq	%r14, %rcx
	movq	%r15, -384(%rbp)
	movq	-376(%rbp), %r15
	rolq	%rsi
	rolq	%rcx
	xorq	%r9, %r10
	addq	-216(%rbp), %r9
	rolq	$32, %r10
	addq	-240(%rbp), %r13
	addq	-208(%rbp), %rax
	addq	%r10, %r15
	addq	%rdi, %rax
	xorq	%r15, %r12
	xorq	%rax, %r8
	rorq	$24, %r12
	rolq	$32, %r8
	addq	%r12, %r9
	xorq	%r9, %r10
	rorq	$16, %r10
	addq	%r10, %r15
	xorq	%r15, %r12
	addq	%r8, %r15
	rolq	%r12
	xorq	%r15, %rdi
	addq	%r12, %r13
	xorq	%r13, %rbx
	addq	-256(%rbp), %r13
	rolq	$32, %rbx
	addq	%rbx, %r14
	xorq	%r14, %r12
	rorq	$24, %r12
	addq	%r12, %r13
	xorq	%r13, %rbx
	rorq	$16, %rbx
	addq	%rbx, %r14
	xorq	%r14, %r12
	rolq	%r12
	rorq	$24, %rdi
	addq	-320(%rbp), %rax
	addq	-288(%rbp), %rdx
	addq	%rdi, %rax
	addq	%rsi, %rdx
	addq	-272(%rbp), %r9
	movq	%r12, -392(%rbp)
	xorq	%rax, %r8
	xorq	%rdx, %r10
	movq	-400(%rbp), %r12
	addq	%rcx, %r9
	rorq	$16, %r8
	rolq	$32, %r10
	addq	-280(%rbp), %rdx
	addq	%r8, %r15
	addq	%r10, %r11
	xorq	%r9, %r12
	addq	-296(%rbp), %r9
	xorq	%r15, %rdi
	xorq	%r11, %rsi
	movq	%r15, -376(%rbp)
	rolq	$32, %r12
	movq	-384(%rbp), %r15
	rorq	$24, %rsi
	rolq	%rdi
	addq	%rsi, %rdx
	addq	%r12, %r15
	xorq	%rdx, %r10
	xorq	%r15, %rcx
	rorq	$16, %r10
	rorq	$24, %rcx
	addq	%r10, %r11
	addq	%rcx, %r9
	xorq	%r11, %rsi
	xorq	%r9, %r12
	rolq	%rsi
	rorq	$16, %r12
	addq	-328(%rbp), %r13
	addq	-224(%rbp), %rax
	addq	%rdi, %r13
	addq	%rsi, %rax
	addq	%r12, %r15
	xorq	%rax, %rbx
	xorq	%r13, %r12
	xorq	%r15, %rcx
	addq	-232(%rbp), %r13
	rolq	$32, %r12
	rolq	$32, %rbx
	addq	-264(%rbp), %rax
	addq	%r12, %r11
	addq	%rbx, %r15
	rolq	%rcx
	xorq	%r11, %rdi
	xorq	%r15, %rsi
	rorq	$24, %rdi
	rorq	$24, %rsi
	addq	%rdi, %r13
	addq	%rsi, %rax
	xorq	%r13, %r12
	xorq	%rax, %rbx
	rorq	$16, %r12
	rorq	$16, %rbx
	addq	%rbx, %r15
	addq	%r12, %r11
	movq	%r12, -400(%rbp)
	xorq	%r11, %rdi
	xorq	%r15, %rsi
	movq	%r15, -384(%rbp)
	rolq	%rdi
	rolq	%rsi
	addq	-216(%rbp), %rdx
	addq	%rcx, %rdx
	xorq	%rdx, %r8
	addq	-312(%rbp), %rdx
	movq	-392(%rbp), %r12
	addq	-304(%rbp), %r9
	movq	-376(%rbp), %r15
	rolq	$32, %r8
	addq	%r12, %r9
	addq	%r8, %r14
	addq	-256(%rbp), %r13
	xorq	%r9, %r10
	addq	-248(%rbp), %r9
	xorq	%r14, %rcx
	rolq	$32, %r10
	rorq	$24, %rcx
	addq	%r10, %r15
	addq	%rcx, %rdx
	xorq	%r15, %r12
	xorq	%rdx, %r8
	rorq	$24, %r12
	rorq	$16, %r8
	addq	%r12, %r9
	addq	%r8, %r14
	xorq	%r9, %r10
	xorq	%r14, %rcx
	rorq	$16, %r10
	rolq	%rcx
	addq	%r10, %r15
	xorq	%r15, %r12
	rolq	%r12
	addq	%r12, %r13
	xorq	%r13, %rbx
	rolq	$32, %rbx
	addq	%rbx, %r14
	addq	-224(%rbp), %rax
	addq	-312(%rbp), %r13
	addq	%rdi, %rax
	xorq	%r14, %r12
	addq	-328(%rbp), %rdx
	xorq	%rax, %r8
	rorq	$24, %r12
	addq	%rsi, %rdx
	addq	-240(%rbp), %rax
	rolq	$32, %r8
	addq	%r12, %r13
	xorq	%rdx, %r10
	addq	-216(%rbp), %rdx
	addq	%r8, %r15
	xorq	%r13, %rbx
	rolq	$32, %r10
	xorq	%r15, %rdi
	rorq	$16, %rbx
	addq	%r10, %r11
	rorq	$24, %rdi
	addq	%rbx, %r14
	xorq	%r11, %rsi
	addq	%rdi, %rax
	xorq	%r14, %r12
	rorq	$24, %rsi
	xorq	%rax, %r8
	rolq	%r12
	rorq	$16, %r8
	movq	%r12, -392(%rbp)
	movq	-400(%rbp), %r12
	addq	%r8, %r15
	xorq	%r15, %rdi
	movq	%r15, -376(%rbp)
	movq	-384(%rbp), %r15
	rolq	%rdi
	addq	%rsi, %rdx
	addq	-264(%rbp), %r9
	addq	-296(%rbp), %r13
	addq	%rcx, %r9
	addq	%rdi, %r13
	xorq	%rdx, %r10
	xorq	%r9, %r12
	addq	-304(%rbp), %r9
	rorq	$16, %r10
	rolq	$32, %r12
	addq	%r10, %r11
	addq	%r12, %r15
	xorq	%r11, %rsi
	xorq	%r15, %rcx
	rolq	%rsi
	rorq	$24, %rcx
	addq	%rcx, %r9
	xorq	%r9, %r12
	rorq	$16, %r12
	addq	%r12, %r15
	xorq	%r13, %r12
	addq	-320(%rbp), %r13
	rolq	$32, %r12
	xorq	%r15, %rcx
	addq	%r12, %r11
	rolq	%rcx
	xorq	%r11, %rdi
	rorq	$24, %rdi
	addq	%rdi, %r13
	xorq	%r13, %r12
	rorq	$16, %r12
	addq	%r12, %r11
	movq	%r12, -400(%rbp)
	movq	-392(%rbp), %r12
	xorq	%r11, %rdi
	addq	-272(%rbp), %rax
	addq	-232(%rbp), %rdx
	addq	%rsi, %rax
	addq	%rcx, %rdx
	addq	-208(%rbp), %r9
	rolq	%rdi
	xorq	%rax, %rbx
	xorq	%rdx, %r8
	addq	-280(%rbp), %rax
	addq	%r12, %r9
	rolq	$32, %rbx
	rolq	$32, %r8
	addq	-248(%rbp), %rdx
	addq	%rbx, %r15
	addq	%r8, %r14
	xorq	%r15, %rsi
	xorq	%r14, %rcx
	rorq	$24, %rsi
	rorq	$24, %rcx
	addq	%rsi, %rax
	addq	%rcx, %rdx
	xorq	%rax, %rbx
	xorq	%rdx, %r8
	rorq	$16, %rbx
	rorq	$16, %r8
	addq	%rbx, %r15
	addq	%r8, %r14
	xorq	%r15, %rsi
	xorq	%r14, %rcx
	movq	%r15, -384(%rbp)
	movq	-376(%rbp), %r15
	rolq	%rsi
	rolq	%rcx
	xorq	%r9, %r10
	addq	-288(%rbp), %r9
	rolq	$32, %r10
	addq	-328(%rbp), %r13
	addq	%r10, %r15
	xorq	%r15, %r12
	rorq	$24, %r12
	addq	%r12, %r9
	xorq	%r9, %r10
	rorq	$16, %r10
	addq	%r10, %r15
	xorq	%r15, %r12
	rolq	%r12
	addq	%r12, %r13
	xorq	%r13, %rbx
	addq	-280(%rbp), %r13
	rolq	$32, %rbx
	addq	%rbx, %r14
	xorq	%r14, %r12
	rorq	$24, %r12
	addq	%r12, %r13
	xorq	%r13, %rbx
	rorq	$16, %rbx
	addq	%rbx, %r14
	xorq	%r14, %r12
	rolq	%r12
	movq	%r12, -392(%rbp)
	addq	-232(%rbp), %rax
	addq	%rdi, %rax
	movq	-400(%rbp), %r12
	xorq	%rax, %r8
	rolq	$32, %r8
	addq	%r8, %r15
	xorq	%r15, %rdi
	rorq	$24, %rdi
	addq	-264(%rbp), %rax
	addq	-312(%rbp), %rdx
	addq	%rdi, %rax
	addq	%rsi, %rdx
	addq	-248(%rbp), %r9
	xorq	%rax, %r8
	xorq	%rdx, %r10
	addq	%rcx, %r9
	addq	-272(%rbp), %rdx
	rorq	$16, %r8
	rolq	$32, %r10
	xorq	%r9, %r12
	addq	-208(%rbp), %r9
	addq	%r8, %r15
	addq	%r10, %r11
	rolq	$32, %r12
	movq	%r15, -376(%rbp)
	xorq	%r15, %rdi
	xorq	%r11, %rsi
	movq	-384(%rbp), %r15
	rorq	$24, %rsi
	rolq	%rdi
	addq	%rsi, %rdx
	addq	%r12, %r15
	xorq	%rdx, %r10
	xorq	%r15, %rcx
	rorq	$16, %r10
	rorq	$24, %rcx
	addq	%r10, %r11
	addq	%rcx, %r9
	xorq	%r11, %rsi
	xorq	%r9, %r12
	rolq	%rsi
	rorq	$16, %r12
	addq	-216(%rbp), %r13
	addq	-240(%rbp), %rax
	addq	%rdi, %r13
	addq	%rsi, %rax
	addq	%r12, %r15
	xorq	%rax, %rbx
	xorq	%r13, %r12
	xorq	%r15, %rcx
	addq	-288(%rbp), %r13
	rolq	$32, %r12
	rolq	$32, %rbx
	addq	-224(%rbp), %rax
	addq	%r12, %r11
	addq	%rbx, %r15
	rolq	%rcx
	xorq	%r11, %rdi
	xorq	%r15, %rsi
	rorq	$24, %rdi
	rorq	$24, %rsi
	addq	%rdi, %r13
	addq	%rsi, %rax
	xorq	%r13, %r12
	xorq	%rax, %rbx
	rorq	$16, %r12
	rorq	$16, %rbx
	addq	%rbx, %r15
	addq	%r12, %r11
	movq	%r12, -400(%rbp)
	movq	-392(%rbp), %r12
	xorq	%r11, %rdi
	xorq	%r15, %rsi
	movq	%r15, -384(%rbp)
	movq	-376(%rbp), %r15
	rolq	%rdi
	rolq	%rsi
	addq	-304(%rbp), %rdx
	addq	-320(%rbp), %r9
	addq	%r12, %r9
	addq	%rcx, %rdx
	addq	-280(%rbp), %r13
	xorq	%r9, %r10
	xorq	%rdx, %r8
	addq	-256(%rbp), %r9
	addq	-296(%rbp), %rdx
	rolq	$32, %r10
	rolq	$32, %r8
	addq	%r10, %r15
	addq	%r8, %r14
	xorq	%r15, %r12
	xorq	%r14, %rcx
	rorq	$24, %r12
	rorq	$24, %rcx
	addq	%r12, %r9
	addq	%rcx, %rdx
	xorq	%r9, %r10
	xorq	%rdx, %r8
	rorq	$16, %r10
	rorq	$16, %r8
	addq	%r10, %r15
	addq	%r8, %r14
	xorq	%r15, %r12
	xorq	%r14, %rcx
	rolq	%r12
	rolq	%rcx
	addq	%r12, %r13
	xorq	%r13, %rbx
	rolq	$32, %rbx
	addq	%rbx, %r14
	addq	-320(%rbp), %rax
	addq	-224(%rbp), %r13
	addq	%rdi, %rax
	xorq	%r14, %r12
	addq	-296(%rbp), %rdx
	xorq	%rax, %r8
	rorq	$24, %r12
	addq	%rsi, %rdx
	addq	-304(%rbp), %rax
	rolq	$32, %r8
	addq	%r12, %r13
	xorq	%rdx, %r10
	addq	-288(%rbp), %rdx
	addq	%r8, %r15
	xorq	%r13, %rbx
	rolq	$32, %r10
	xorq	%r15, %rdi
	rorq	$16, %rbx
	addq	%r10, %r11
	rorq	$24, %rdi
	addq	%rbx, %r14
	xorq	%r11, %rsi
	addq	%rdi, %rax
	xorq	%r14, %r12
	rorq	$24, %rsi
	xorq	%rax, %r8
	rolq	%r12
	rorq	$16, %r8
	movq	%r12, -392(%rbp)
	movq	-400(%rbp), %r12
	addq	%r8, %r15
	xorq	%r15, %rdi
	movq	%r15, -376(%rbp)
	movq	-384(%rbp), %r15
	rolq	%rdi
	addq	%rsi, %rdx
	addq	-216(%rbp), %r9
	addq	%rcx, %r9
	xorq	%rdx, %r10
	xorq	%r9, %r12
	addq	-264(%rbp), %r9
	rorq	$16, %r10
	addq	-312(%rbp), %r13
	rolq	$32, %r12
	addq	%rdi, %r13
	addq	%r10, %r11
	addq	%r12, %r15
	xorq	%r11, %rsi
	xorq	%r15, %rcx
	rolq	%rsi
	rorq	$24, %rcx
	addq	%rcx, %r9
	xorq	%r9, %r12
	rorq	$16, %r12
	addq	%r12, %r15
	xorq	%r13, %r12
	addq	-240(%rbp), %r13
	rolq	$32, %r12
	xorq	%r15, %rcx
	addq	%r12, %r11
	rolq	%rcx
	xorq	%r11, %rdi
	rorq	$24, %rdi
	addq	%rdi, %r13
	xorq	%r13, %r12
	rorq	$16, %r12
	addq	%r12, %r11
	movq	%r12, -400(%rbp)
	movq	-392(%rbp), %r12
	xorq	%r11, %rdi
	addq	-232(%rbp), %rax
	addq	-256(%rbp), %rdx
	addq	%rsi, %rax
	addq	%rcx, %rdx
	addq	-248(%rbp), %r9
	rolq	%rdi
	xorq	%rax, %rbx
	xorq	%rdx, %r8
	addq	-208(%rbp), %rax
	addq	%r12, %r9
	rolq	$32, %rbx
	rolq	$32, %r8
	addq	-328(%rbp), %rdx
	addq	%rbx, %r15
	addq	%r8, %r14
	xorq	%r15, %rsi
	xorq	%r14, %rcx
	rorq	$24, %rsi
	rorq	$24, %rcx
	addq	%rsi, %rax
	addq	%rcx, %rdx
	xorq	%rax, %rbx
	xorq	%rdx, %r8
	rorq	$16, %rbx
	rorq	$16, %r8
	addq	%rbx, %r15
	addq	%r8, %r14
	xorq	%r15, %rsi
	xorq	%r14, %rcx
	movq	%r15, -384(%rbp)
	movq	-376(%rbp), %r15
	rolq	%rsi
	rolq	%rcx
	xorq	%r9, %r10
	addq	-272(%rbp), %r9
	rolq	$32, %r10
	addq	-288(%rbp), %r13
	addq	-240(%rbp), %rax
	addq	%r10, %r15
	addq	%rdi, %rax
	xorq	%r15, %r12
	xorq	%rax, %r8
	rorq	$24, %r12
	rolq	$32, %r8
	addq	%r12, %r9
	xorq	%r9, %r10
	rorq	$16, %r10
	addq	%r10, %r15
	xorq	%r15, %r12
	addq	%r8, %r15
	rolq	%r12
	xorq	%r15, %rdi
	addq	%r12, %r13
	xorq	%r13, %rbx
	addq	-272(%rbp), %r13
	rolq	$32, %rbx
	addq	%rbx, %r14
	xorq	%r14, %r12
	rorq	$24, %r12
	addq	%r12, %r13
	xorq	%r13, %rbx
	rorq	$16, %rbx
	addq	%rbx, %r14
	xorq	%r14, %r12
	rolq	%r12
	rorq	$24, %rdi
	addq	-296(%rbp), %rax
	addq	-280(%rbp), %rdx
	addq	%rdi, %rax
	addq	%rsi, %rdx
	addq	-208(%rbp), %r9
	movq	%r12, -392(%rbp)
	xorq	%rax, %r8
	xorq	%rdx, %r10
	movq	-400(%rbp), %r12
	addq	%rcx, %r9
	rorq	$16, %r8
	rolq	$32, %r10
	addq	-320(%rbp), %rdx
	addq	%r8, %r15
	addq	%r10, %r11
	xorq	%r9, %r12
	addq	-256(%rbp), %r9
	xorq	%r15, %rdi
	xorq	%r11, %rsi
	movq	%r15, -376(%rbp)
	rolq	$32, %r12
	movq	-384(%rbp), %r15
	rorq	$24, %rsi
	rolq	%rdi
	addq	%rsi, %rdx
	addq	%r12, %r15
	xorq	%rdx, %r10
	xorq	%r15, %rcx
	rorq	$16, %r10
	rorq	$24, %rcx
	addq	%r10, %r11
	addq	%rcx, %r9
	xorq	%r11, %rsi
	xorq	%r9, %r12
	rolq	%rsi
	rorq	$16, %r12
	addq	-224(%rbp), %r13
	addq	-304(%rbp), %rax
	addq	%rdi, %r13
	addq	%rsi, %rax
	addq	%r12, %r15
	xorq	%rax, %rbx
	xorq	%r13, %r12
	xorq	%r15, %rcx
	addq	-312(%rbp), %r13
	rolq	$32, %r12
	rolq	$32, %rbx
	addq	-216(%rbp), %rax
	addq	%r12, %r11
	addq	%rbx, %r15
	rolq	%rcx
	xorq	%r11, %rdi
	xorq	%r15, %rsi
	rorq	$24, %rdi
	rorq	$24, %rsi
	addq	%rdi, %r13
	addq	%rsi, %rax
	xorq	%r13, %r12
	xorq	%rax, %rbx
	rorq	$16, %r12
	rorq	$16, %rbx
	addq	%rbx, %r15
	addq	%r12, %r11
	movq	%r12, -400(%rbp)
	xorq	%r11, %rdi
	xorq	%r15, %rsi
	movq	%r15, -384(%rbp)
	rolq	%rdi
	rolq	%rsi
	addq	-248(%rbp), %rdx
	addq	%rcx, %rdx
	xorq	%rdx, %r8
	addq	-232(%rbp), %rdx
	movq	-392(%rbp), %r12
	addq	-328(%rbp), %r9
	movq	-376(%rbp), %r15
	rolq	$32, %r8
	addq	%r12, %r9
	addq	%r8, %r14
	addq	-232(%rbp), %r13
	xorq	%r9, %r10
	addq	-264(%rbp), %r9
	xorq	%r14, %rcx
	rolq	$32, %r10
	rorq	$24, %rcx
	addq	%r10, %r15
	addq	%rcx, %rdx
	xorq	%r15, %r12
	xorq	%rdx, %r8
	rorq	$24, %r12
	rorq	$16, %r8
	addq	%r12, %r9
	addq	%r8, %r14
	xorq	%r9, %r10
	xorq	%r14, %rcx
	rorq	$16, %r10
	rolq	%rcx
	addq	%r10, %r15
	xorq	%r15, %r12
	rolq	%r12
	addq	%r12, %r13
	xorq	%r13, %rbx
	rolq	$32, %rbx
	addq	%rbx, %r14
	addq	-296(%rbp), %rax
	addq	-272(%rbp), %rdx
	addq	%rdi, %rax
	xorq	%r14, %r12
	addq	%rsi, %rdx
	addq	-304(%rbp), %r13
	xorq	%rax, %r8
	rorq	$24, %r12
	addq	-256(%rbp), %rax
	xorq	%rdx, %r10
	rolq	$32, %r8
	addq	%r12, %r13
	rolq	$32, %r10
	addq	-208(%rbp), %rdx
	addq	%r8, %r15
	xorq	%r13, %rbx
	addq	%r10, %r11
	xorq	%r15, %rdi
	rorq	$16, %rbx
	xorq	%r11, %rsi
	rorq	$24, %rdi
	addq	%rbx, %r14
	rorq	$24, %rsi
	addq	%rdi, %rax
	xorq	%r14, %r12
	xorq	%rax, %r8
	rolq	%r12
	rorq	$16, %r8
	movq	%r12, -392(%rbp)
	addq	%r8, %r15
	xorq	%r15, %rdi
	movq	%r15, -376(%rbp)
	movq	-400(%rbp), %r15
	rolq	%rdi
	addq	%rsi, %rdx
	addq	-312(%rbp), %r9
	addq	-280(%rbp), %r13
	addq	%rcx, %r9
	addq	%rdi, %r13
	xorq	%rdx, %r10
	xorq	%r9, %r15
	addq	-248(%rbp), %r9
	rorq	$16, %r10
	movq	%r15, %r12
	movq	-384(%rbp), %r15
	addq	%r10, %r11
	rolq	$32, %r12
	xorq	%r11, %rsi
	addq	%r12, %r15
	rolq	%rsi
	xorq	%r15, %rcx
	rorq	$24, %rcx
	addq	%rcx, %r9
	xorq	%r9, %r12
	rorq	$16, %r12
	addq	%r12, %r15
	xorq	%r13, %r12
	addq	-328(%rbp), %r13
	rolq	$32, %r12
	xorq	%r15, %rcx
	addq	%r12, %r11
	rolq	%rcx
	xorq	%r11, %rdi
	rorq	$24, %rdi
	addq	%rdi, %r13
	xorq	%r13, %r12
	rorq	$16, %r12
	addq	%r12, %r11
	movq	%r12, -400(%rbp)
	movq	-392(%rbp), %r12
	xorq	%r11, %rdi
	addq	-288(%rbp), %rax
	addq	-320(%rbp), %rdx
	addq	%rsi, %rax
	addq	%rcx, %rdx
	addq	-264(%rbp), %r9
	rolq	%rdi
	xorq	%rax, %rbx
	xorq	%rdx, %r8
	addq	-240(%rbp), %rax
	addq	%r12, %r9
	rolq	$32, %rbx
	rolq	$32, %r8
	addq	-216(%rbp), %rdx
	addq	%rbx, %r15
	addq	%r8, %r14
	xorq	%r15, %rsi
	xorq	%r14, %rcx
	rorq	$24, %rsi
	rorq	$24, %rcx
	addq	%rsi, %rax
	addq	%rcx, %rdx
	xorq	%rax, %rbx
	xorq	%rdx, %r8
	rorq	$16, %rbx
	rorq	$16, %r8
	addq	%rbx, %r15
	addq	%r8, %r14
	xorq	%r15, %rsi
	xorq	%r14, %rcx
	movq	%r15, -384(%rbp)
	movq	-376(%rbp), %r15
	rolq	%rsi
	rolq	%rcx
	xorq	%r9, %r10
	addq	-224(%rbp), %r9
	rolq	$32, %r10
	addq	-264(%rbp), %r13
	addq	%r10, %r15
	xorq	%r15, %r12
	rorq	$24, %r12
	addq	%r12, %r9
	xorq	%r9, %r10
	rorq	$16, %r10
	addq	%r10, %r15
	xorq	%r15, %r12
	rolq	%r12
	addq	%r12, %r13
	xorq	%r13, %rbx
	addq	-328(%rbp), %r13
	rolq	$32, %rbx
	addq	%rbx, %r14
	xorq	%r14, %r12
	rorq	$24, %r12
	addq	%r12, %r13
	xorq	%r13, %rbx
	rorq	$16, %rbx
	addq	%rbx, %r14
	xorq	%r14, %r12
	rolq	%r12
	movq	%r12, -392(%rbp)
	addq	-248(%rbp), %rax
	addq	%rdi, %rax
	xorq	%rax, %r8
	rolq	$32, %r8
	addq	%r8, %r15
	xorq	%r15, %rdi
	rorq	$24, %rdi
	addq	-216(%rbp), %rax
	addq	-240(%rbp), %rdx
	addq	%rdi, %rax
	addq	%rsi, %rdx
	addq	-320(%rbp), %r9
	xorq	%rax, %r8
	xorq	%rdx, %r10
	addq	%rcx, %r9
	addq	-232(%rbp), %rdx
	rorq	$16, %r8
	rolq	$32, %r10
	addq	%r8, %r15
	addq	%r10, %r11
	movq	%r15, -376(%rbp)
	xorq	%r15, %rdi
	xorq	%r11, %rsi
	movq	-400(%rbp), %r15
	rorq	$24, %rsi
	rolq	%rdi
	xorq	%r9, %r15
	addq	%rsi, %rdx
	addq	-224(%rbp), %r9
	movq	%r15, %r12
	movq	-384(%rbp), %r15
	xorq	%rdx, %r10
	rolq	$32, %r12
	rorq	$16, %r10
	addq	%r12, %r15
	addq	%r10, %r11
	xorq	%r15, %rcx
	xorq	%r11, %rsi
	rorq	$24, %rcx
	rolq	%rsi
	addq	%rcx, %r9
	xorq	%r9, %r12
	rorq	$16, %r12
	addq	-304(%rbp), %r13
	addq	-256(%rbp), %rax
	addq	%rdi, %r13
	addq	%rsi, %rax
	addq	%r12, %r15
	xorq	%rax, %rbx
	xorq	%r13, %r12
	xorq	%r15, %rcx
	addq	-272(%rbp), %r13
	rolq	$32, %r12
	rolq	$32, %rbx
	addq	-296(%rbp), %rax
	addq	%r12, %r11
	addq	%rbx, %r15
	rolq	%rcx
	xorq	%r11, %rdi
	xorq	%r15, %rsi
	rorq	$24, %rdi
	rorq	$24, %rsi
	addq	%rdi, %r13
	addq	%rsi, %rax
	xorq	%r13, %r12
	xorq	%rax, %rbx
	rorq	$16, %r12
	rorq	$16, %rbx
	addq	%r12, %r11
	addq	%rbx, %r15
	movq	%r12, -400(%rbp)
	movq	-392(%rbp), %r12
	xorq	%r11, %rdi
	xorq	%r15, %rsi
	movq	%r15, -384(%rbp)
	movq	-376(%rbp), %r15
	rolq	%rdi
	rolq	%rsi
	addq	-208(%rbp), %rdx
	addq	-288(%rbp), %r9
	addq	%r12, %r9
	addq	%rcx, %rdx
	addq	-312(%rbp), %r13
	xorq	%r9, %r10
	xorq	%rdx, %r8
	addq	-312(%rbp), %r9
	addq	-280(%rbp), %rdx
	rolq	$32, %r10
	rolq	$32, %r8
	addq	%r10, %r15
	addq	%r8, %r14
	xorq	%r15, %r12
	xorq	%r14, %rcx
	rorq	$24, %r12
	rorq	$24, %rcx
	addq	%r12, %r9
	addq	%rcx, %rdx
	xorq	%r9, %r10
	xorq	%rdx, %r8
	rorq	$16, %r10
	rorq	$16, %r8
	addq	%r10, %r15
	addq	%r8, %r14
	xorq	%r15, %r12
	xorq	%r14, %rcx
	rolq	%r12
	rolq	%rcx
	addq	%r12, %r13
	xorq	%r13, %rbx
	rolq	$32, %rbx
	addq	%rbx, %r14
	addq	-320(%rbp), %r13
	addq	-328(%rbp), %rax
	xorq	%r14, %r12
	addq	%rdi, %rax
	addq	-216(%rbp), %rdx
	rorq	$24, %r12
	xorq	%rax, %r8
	addq	-208(%rbp), %rax
	addq	%rsi, %rdx
	addq	%r12, %r13
	rolq	$32, %r8
	xorq	%rdx, %r10
	addq	-224(%rbp), %rdx
	xorq	%r13, %rbx
	rolq	$32, %r10
	rorq	$16, %rbx
	addq	%rbx, %r14
	xorq	%r14, %r12
	rolq	%r12
	movq	%r12, -408(%rbp)
	leaq	(%r15,%r8), %r12
	xorq	%r12, %rdi
	rorq	$24, %rdi
	leaq	(%rax,%rdi), %r15
	movq	%r12, %rax
	xorq	%r15, %r8
	rorq	$16, %r8
	addq	%r8, %rax
	movq	%r8, -416(%rbp)
	movq	-400(%rbp), %r8
	xorq	%rax, %rdi
	movq	%rax, -376(%rbp)
	leaq	(%r11,%r10), %rax
	xorq	%rax, %rsi
	rolq	%rdi
	rorq	$24, %rsi
	addq	%rsi, %rdx
	xorq	%rdx, %r10
	rorq	$16, %r10
	addq	%r10, %rax
	addq	-232(%rbp), %r9
	movq	%r10, %r11
	xorq	%rax, %rsi
	addq	%rcx, %r9
	rolq	%rsi
	xorq	%r9, %r8
	movq	%rsi, %r10
	movq	-384(%rbp), %rsi
	rolq	$32, %r8
	addq	-240(%rbp), %r9
	addq	-248(%rbp), %r13
	addq	-264(%rbp), %r15
	leaq	(%rsi,%r8), %r12
	xorq	%r12, %rcx
	movq	%rcx, %rsi
	rorq	$24, %rsi
	addq	%rsi, %r9
	xorq	%r9, %r8
	rorq	$16, %r8
	leaq	(%r12,%r8), %rcx
	leaq	0(%r13,%rdi), %r12
	xorq	%r12, %r8
	addq	-256(%rbp), %r12
	xorq	%rcx, %rsi
	rolq	$32, %r8
	rolq	%rsi
	leaq	(%rax,%r8), %r13
	xorq	%r13, %rdi
	movq	%rdi, %rax
	rorq	$24, %rax
	addq	%rax, %r12
	xorq	%r12, %r8
	movq	%r8, %rdi
	rorq	$16, %rdi
	addq	%rdi, %r13
	movq	%rdi, -400(%rbp)
	leaq	(%r15,%r10), %rdi
	xorq	%rdi, %rbx
	movq	%r13, -384(%rbp)
	xorq	%r13, %rax
	movq	%rbx, %r8
	movq	-272(%rbp), %rbx
	rolq	%rax
	rolq	$32, %r8
	leaq	(%rcx,%r8), %r13
	leaq	(%rdi,%rbx), %rcx
	movq	-416(%rbp), %rdi
	xorq	%r13, %r10
	rorq	$24, %r10
	addq	%r10, %rcx
	addq	-280(%rbp), %rdx
	addq	-296(%rbp), %r9
	xorq	%rcx, %r8
	addq	%rsi, %rdx
	rorq	$16, %r8
	xorq	%rdx, %rdi
	addq	-288(%rbp), %rdx
	leaq	0(%r13,%r8), %rbx
	rolq	$32, %rdi
	movq	-408(%rbp), %r13
	xorq	%rbx, %r10
	movq	%rbx, -392(%rbp)
	rolq	%r10
	addq	%r13, %r9
	movq	%r10, %r15
	leaq	(%r14,%rdi), %r10
	xorq	%r10, %rsi
	rorq	$24, %rsi
	addq	%rsi, %rdx
	xorq	%rdx, %rdi
	rorq	$16, %rdi
	addq	%rdi, %r10
	xorq	%r10, %rsi
	movq	%rsi, %r14
	movq	%r11, %rsi
	movq	-376(%rbp), %r11
	xorq	%r9, %rsi
	addq	-304(%rbp), %r9
	rolq	%r14
	rolq	$32, %rsi
	leaq	(%r11,%rsi), %rbx
	movq	%r13, %r11
	xorq	%rbx, %r11
	movq	%r11, %r13
	rorq	$24, %r13
	addq	%r13, %r9
	xorq	%r9, %rsi
	rorq	$16, %rsi
	leaq	(%rbx,%rsi), %r11
	movq	-296(%rbp), %rbx
	xorq	%r11, %r13
	rolq	%r13
	addq	%r12, %rbx
	addq	%r13, %rbx
	xorq	%rbx, %r8
	addq	-264(%rbp), %rbx
	addq	-216(%rbp), %rcx
	rolq	$32, %r8
	addq	%rax, %rcx
	addq	-256(%rbp), %rdx
	addq	%r8, %r10
	xorq	%rcx, %rdi
	addq	%r15, %rdx
	xorq	%r10, %r13
	rolq	$32, %rdi
	xorq	%rdx, %rsi
	rorq	$24, %r13
	addq	%rdi, %r11
	rolq	$32, %rsi
	addq	%r13, %rbx
	xorq	%r11, %rax
	xorq	%rbx, %r8
	rorq	$24, %rax
	rorq	$16, %r8
	leaq	(%r10,%r8), %r12
	movq	-248(%rbp), %r10
	xorq	%r12, %r13
	movq	%r12, -264(%rbp)
	leaq	(%rcx,%r10), %r12
	rolq	%r13
	movq	-400(%rbp), %rcx
	addq	%rax, %r12
	movq	%r13, -296(%rbp)
	xorq	%r12, %rdi
	rorq	$16, %rdi
	addq	%rdi, %r11
	xorq	%r11, %rax
	movq	%rax, %r10
	rolq	%r10
	movq	%r10, %r13
	movq	-384(%rbp), %r10
	leaq	(%r10,%rsi), %rax
	movq	-304(%rbp), %r10
	xorq	%rax, %r15
	rorq	$24, %r15
	addq	%rdx, %r10
	movq	-392(%rbp), %rdx
	addq	%r15, %r10
	xorq	%r10, %rsi
	addq	-288(%rbp), %r9
	addq	-320(%rbp), %rbx
	addq	%r14, %r9
	addq	%r13, %rbx
	rorq	$16, %rsi
	xorq	%r9, %rcx
	addq	-232(%rbp), %r9
	addq	%rsi, %rax
	rolq	$32, %rcx
	xorq	%rax, %r15
	addq	%rcx, %rdx
	rolq	%r15
	xorq	%rdx, %r14
	rorq	$24, %r14
	addq	%r14, %r9
	xorq	%r9, %rcx
	rorq	$16, %rcx
	addq	%rcx, %rdx
	xorq	%rbx, %rcx
	addq	-280(%rbp), %rbx
	rolq	$32, %rcx
	xorq	%rdx, %r14
	addq	%rcx, %rax
	rolq	%r14
	xorq	%rax, %r13
	rorq	$24, %r13
	addq	%r13, %rbx
	movq	%r13, -232(%rbp)
	xorq	%rbx, %rcx
	rorq	$16, %rcx
	movq	%rcx, -248(%rbp)
	addq	-312(%rbp), %r12
	addq	%rcx, %rax
	addq	%r15, %r12
	addq	-272(%rbp), %r10
	addq	-224(%rbp), %r9
	xorq	%r12, %r8
	addq	-328(%rbp), %r12
	addq	%r14, %r10
	rolq	$32, %r8
	xorq	%r10, %rdi
	addq	-240(%rbp), %r10
	leaq	(%rdx,%r8), %rcx
	movq	-264(%rbp), %rdx
	rolq	$32, %rdi
	xorq	%rcx, %r15
	rorq	$24, %r15
	addq	%r15, %r12
	xorq	%r12, %r8
	rorq	$16, %r8
	leaq	(%rcx,%r8), %r13
	movq	%r13, -216(%rbp)
	leaq	(%rdx,%rdi), %r13
	movq	-352(%rbp), %rdx
	xorq	%r13, %r14
	rorq	$24, %r14
	addq	%r14, %r10
	xorq	%r10, %rdi
	rorq	$16, %rdi
	leaq	0(%r13,%rdi), %rcx
	movq	-296(%rbp), %r13
	xorq	%rcx, %rbx
	addq	%r13, %r9
	xorq	%r9, %rsi
	addq	-208(%rbp), %r9
	rolq	$32, %rsi
	addq	%rsi, %r11
	xorq	%r11, %r13
	rorq	$24, %r13
	addq	%r13, %r9
	xorq	%r9, %rsi
	rorq	$16, %rsi
	addq	%rsi, %r11
	xorq	-424(%rbp), %rbx
	xorq	%rax, %r10
	xorq	%r14, %rcx
	xorq	%r11, %r12
	xorq	-232(%rbp), %rax
	xorq	%r13, %r11
	movq	%rbx, (%rdx)
	rolq	%r11
	rolq	%rax
	xorq	-432(%rbp), %r12
	xorq	-440(%rbp), %r10
	xorq	-216(%rbp), %r9
	xorq	%r8, %r11
	xorq	%rax, %rdi
	movq	%rdx, %r8
	xorq	-448(%rbp), %r9
	xorq	-456(%rbp), %r11
	movq	%r12, 8(%rdx)
	rolq	%rcx
	xorq	-464(%rbp), %rdi
	xorq	-248(%rbp), %rcx
	movq	%r10, 16(%rdx)
	movq	%r9, 24(%rdx)
	xorq	-480(%rbp), %rcx
	movq	%r11, 32(%rdx)
	movq	%rdi, 40(%rdx)
	movq	-216(%rbp), %rdx
	movq	%rcx, 56(%r8)
	xorq	%r15, %rdx
	rolq	%rdx
	xorq	%rdx, %rsi
	xorq	-472(%rbp), %rsi
	movq	-368(%rbp), %rdx
	movq	%rsi, 48(%r8)
	addq	%rdx, -360(%rbp)
	addq	%rdx, -344(%rbp)
	subq	%rdx, -504(%rbp)
	jne	.L4
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L7
	addq	$472, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L7:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE163:
	.size	blake2b_compress, .-blake2b_compress
	.p2align 4
	.globl	BLAKE2b_Init
	.type	BLAKE2b_Init, @function
BLAKE2b_Init:
.LFB162:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rdx
	addq	$72, %rdi
	xorl	%ecx, %ecx
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$80, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	xorl	%eax, %eax
	movq	$0, -8(%rdi)
	movq	$0, 152(%rdi)
	andq	$-8, %rdi
	movw	%ax, -64(%rbp)
	movl	%edx, %eax
	subl	%edi, %eax
	movw	%cx, -50(%rbp)
	movaps	%xmm0, -48(%rbp)
	leal	232(%rax), %ecx
	xorl	%eax, %eax
	movaps	%xmm0, -32(%rbp)
	movdqa	.LC0(%rip), %xmm0
	shrl	$3, %ecx
	rep stosq
	movq	$16842816, -80(%rbp)
	movq	$0, -72(%rbp)
	pxor	-80(%rbp), %xmm0
	movq	$0, -62(%rbp)
	movl	$0, -54(%rbp)
	movups	%xmm0, (%rdx)
	movdqa	.LC1(%rip), %xmm0
	pxor	-64(%rbp), %xmm0
	movups	%xmm0, 16(%rdx)
	movdqa	.LC2(%rip), %xmm0
	pxor	-48(%rbp), %xmm0
	movups	%xmm0, 32(%rdx)
	movdqa	.LC3(%rip), %xmm0
	pxor	-32(%rbp), %xmm0
	movups	%xmm0, 48(%rdx)
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L11
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movl	$1, %eax
	ret
.L11:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE162:
	.size	BLAKE2b_Init, .-BLAKE2b_Init
	.p2align 4
	.globl	BLAKE2b_Update
	.type	BLAKE2b_Update, @function
BLAKE2b_Update:
.LFB164:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	$128, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	leaq	96(%rdi), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$24, %rsp
	movq	224(%rdi), %r12
	subq	%r12, %r14
	cmpq	%rdx, %r14
	jnb	.L13
	testq	%r12, %r12
	jne	.L22
	xorl	%r12d, %r12d
	cmpq	$128, %rbx
	jbe	.L13
.L23:
	movq	%rbx, %r14
	movl	$128, %eax
	movq	%r8, %rsi
	movq	%r15, %rdi
	andl	$127, %r14d
	movq	%r8, -56(%rbp)
	cmove	%rax, %r14
	subq	%r14, %rbx
	movq	%rbx, %rdx
	call	blake2b_compress
	movq	-56(%rbp), %r8
	movq	224(%r15), %r12
	addq	%rbx, %r8
	movq	%r14, %rbx
.L13:
	leaq	0(%r13,%r12), %rdi
	movq	%rbx, %rdx
	movq	%r8, %rsi
	call	memcpy@PLT
	addq	%rbx, 224(%r15)
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	.cfi_restore_state
	leaq	0(%r13,%r12), %rdi
	movq	%r14, %rdx
	movq	%rsi, -56(%rbp)
	leaq	-128(%r12,%rbx), %rbx
	call	memcpy@PLT
	movl	$128, %edx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	blake2b_compress
	movq	-56(%rbp), %r8
	xorl	%r12d, %r12d
	movq	$0, 224(%r15)
	addq	%r14, %r8
	cmpq	$128, %rbx
	jbe	.L13
	jmp	.L23
	.cfi_endproc
.LFE164:
	.size	BLAKE2b_Update, .-BLAKE2b_Update
	.p2align 4
	.globl	BLAKE2b_Final
	.type	BLAKE2b_Final, @function
BLAKE2b_Final:
.LFB165:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$128, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	96(%rsi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	$-1, 80(%rsi)
	movq	224(%rsi), %rdi
	xorl	%esi, %esi
	subq	%rdi, %rdx
	addq	%r13, %rdi
	call	memset@PLT
	movq	224(%r12), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	blake2b_compress
	leaq	15(%r12), %rax
	subq	%rbx, %rax
	cmpq	$30, %rax
	jbe	.L25
	movdqu	(%r12), %xmm0
	movups	%xmm0, (%rbx)
	movdqu	16(%r12), %xmm1
	movups	%xmm1, 16(%rbx)
	movdqu	32(%r12), %xmm2
	movups	%xmm2, 32(%rbx)
	movdqu	48(%r12), %xmm3
	movups	%xmm3, 48(%rbx)
.L26:
	movq	%r12, %rdi
	movl	$232, %esi
	call	OPENSSL_cleanse@PLT
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	.cfi_restore_state
	movq	(%r12), %rax
	movq	%rax, (%rbx)
	movq	8(%r12), %rax
	movq	%rax, 8(%rbx)
	movq	16(%r12), %rax
	movq	%rax, 16(%rbx)
	movq	24(%r12), %rax
	movq	%rax, 24(%rbx)
	movq	32(%r12), %rax
	movq	%rax, 32(%rbx)
	movq	40(%r12), %rax
	movq	%rax, 40(%rbx)
	movq	48(%r12), %rax
	movq	%rax, 48(%rbx)
	movq	56(%r12), %rax
	movq	%rax, 56(%rbx)
	jmp	.L26
	.cfi_endproc
.LFE165:
	.size	BLAKE2b_Final, .-BLAKE2b_Final
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.quad	7640891576956012808
	.quad	-4942790177534073029
	.align 16
.LC1:
	.quad	4354685564936845355
	.quad	-6534734903238641935
	.align 16
.LC2:
	.quad	5840696475078001361
	.quad	-7276294671716946913
	.align 16
.LC3:
	.quad	2270897969802886507
	.quad	6620516959819538809
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
