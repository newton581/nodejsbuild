	.file	"dh_check.c"
	.text
	.p2align 4
	.globl	DH_check_params
	.type	DH_check_params, @function
DH_check_params:
.LFB422:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	movl	$0, (%rsi)
	call	BN_CTX_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L4
	movq	%rax, %rdi
	call	BN_CTX_start@PLT
	movq	%r12, %rdi
	call	BN_CTX_get@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L4
	movq	8(%r14), %rdi
	call	BN_is_odd@PLT
	testl	%eax, %eax
	jne	.L5
	orl	$1, (%rbx)
.L5:
	movq	16(%r14), %rdi
	call	BN_is_negative@PLT
	testl	%eax, %eax
	je	.L6
.L8:
	orl	$8, (%rbx)
.L7:
	movq	8(%r14), %rsi
	movq	%r13, %rdi
	call	BN_copy@PLT
	testq	%rax, %rax
	je	.L4
	movl	$1, %esi
	movq	%r13, %rdi
	call	BN_sub_word@PLT
	testl	%eax, %eax
	jne	.L21
.L4:
	xorl	%r13d, %r13d
.L3:
	movq	%r12, %rdi
	call	BN_CTX_end@PLT
	movq	%r12, %rdi
	call	BN_CTX_free@PLT
	popq	%rbx
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	.cfi_restore_state
	movq	16(%r14), %rdi
	movq	%r13, %rsi
	movl	$1, %r13d
	call	BN_cmp@PLT
	testl	%eax, %eax
	js	.L3
	orl	$8, (%rbx)
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L6:
	movq	16(%r14), %rdi
	call	BN_is_zero@PLT
	testl	%eax, %eax
	jne	.L8
	movq	16(%r14), %rdi
	call	BN_is_one@PLT
	testl	%eax, %eax
	jne	.L8
	jmp	.L7
	.cfi_endproc
.LFE422:
	.size	DH_check_params, .-DH_check_params
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/dh/dh_check.c"
	.text
	.p2align 4
	.globl	DH_check_params_ex
	.type	DH_check_params_ex, @function
DH_check_params_ex:
.LFB421:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-12(%rbp), %rsi
	movl	$0, -12(%rbp)
	call	DH_check_params
	testl	%eax, %eax
	je	.L22
	movl	-12(%rbp), %eax
	testb	$1, %al
	jne	.L37
	testb	$8, %al
	jne	.L38
.L25:
	testl	%eax, %eax
	sete	%al
	movzbl	%al, %eax
.L22:
	movq	-8(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L39
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L38:
	.cfi_restore_state
	movl	$33, %r8d
	movl	$120, %edx
	movl	$122, %esi
	movl	$5, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	-12(%rbp), %eax
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L37:
	movl	$31, %r8d
	movl	$117, %edx
	movl	$122, %esi
	movl	$5, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	-12(%rbp), %eax
	testb	$8, %al
	je	.L25
	jmp	.L38
.L39:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE421:
	.size	DH_check_params_ex, .-DH_check_params_ex
	.p2align 4
	.globl	DH_check
	.type	DH_check, @function
DH_check:
.LFB424:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	DH_check_params
	movl	%eax, %r12d
	testl	%eax, %eax
	jne	.L81
.L40:
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L81:
	.cfi_restore_state
	call	BN_CTX_new@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L44
	movq	%rax, %rdi
	call	BN_CTX_start@PLT
	movq	%r14, %rdi
	call	BN_CTX_get@PLT
	movq	%r14, %rdi
	movq	%rax, %r15
	call	BN_CTX_get@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L44
	cmpq	$0, 64(%rbx)
	je	.L46
	call	BN_value_one@PLT
	movq	16(%rbx), %rdi
	movq	%rax, %rsi
	call	BN_cmp@PLT
	testl	%eax, %eax
	jg	.L47
.L79:
	orl	$8, 0(%r13)
.L48:
	movq	64(%rbx), %rdi
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movl	$64, %esi
	call	BN_is_prime_ex@PLT
	testl	%eax, %eax
	js	.L44
	jne	.L50
	orl	$16, 0(%r13)
.L50:
	movq	64(%rbx), %rcx
	movq	8(%rbx), %rdx
	movq	%r14, %r8
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	BN_div@PLT
	testl	%eax, %eax
	je	.L44
	movq	%r12, %rdi
	call	BN_is_one@PLT
	testl	%eax, %eax
	jne	.L51
	orl	$32, 0(%r13)
.L51:
	movq	72(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L46
	movq	%r15, %rsi
	call	BN_cmp@PLT
	testl	%eax, %eax
	je	.L46
	orl	$64, 0(%r13)
	.p2align 4,,10
	.p2align 3
.L46:
	movq	8(%rbx), %rdi
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movl	$64, %esi
	call	BN_is_prime_ex@PLT
	testl	%eax, %eax
	js	.L44
	jne	.L53
	orl	$1, 0(%r13)
	movl	$1, %r12d
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L44:
	xorl	%r12d, %r12d
.L43:
	movq	%r14, %rdi
	call	BN_CTX_end@PLT
	movq	%r14, %rdi
	call	BN_CTX_free@PLT
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L47:
	movq	8(%rbx), %rsi
	movq	16(%rbx), %rdi
	call	BN_cmp@PLT
	testl	%eax, %eax
	jns	.L79
	movq	8(%rbx), %rcx
	movq	64(%rbx), %rdx
	movq	%r14, %r8
	movq	%r15, %rdi
	movq	16(%rbx), %rsi
	call	BN_mod_exp@PLT
	testl	%eax, %eax
	je	.L44
	movq	%r15, %rdi
	call	BN_is_one@PLT
	testl	%eax, %eax
	jne	.L48
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L53:
	cmpq	$0, 64(%rbx)
	je	.L54
.L80:
	movl	$1, %r12d
	jmp	.L43
.L54:
	movq	8(%rbx), %rsi
	movq	%r15, %rdi
	call	BN_rshift1@PLT
	testl	%eax, %eax
	je	.L44
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movl	$64, %esi
	movq	%r15, %rdi
	call	BN_is_prime_ex@PLT
	testl	%eax, %eax
	js	.L44
	jne	.L80
	orl	$2, 0(%r13)
	jmp	.L80
	.cfi_endproc
.LFE424:
	.size	DH_check, .-DH_check
	.p2align 4
	.globl	DH_check_ex
	.type	DH_check_ex, @function
DH_check_ex:
.LFB423:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-12(%rbp), %rsi
	movl	$0, -12(%rbp)
	call	DH_check
	testl	%eax, %eax
	je	.L82
	movl	-12(%rbp), %eax
	testb	$8, %al
	jne	.L117
	testb	$16, %al
	jne	.L118
.L85:
	testb	$32, %al
	jne	.L119
.L86:
	testb	$64, %al
	jne	.L120
.L87:
	testb	$4, %al
	jne	.L121
.L88:
	testb	$1, %al
	jne	.L122
.L89:
	testb	$2, %al
	jne	.L123
.L90:
	testl	%eax, %eax
	sete	%al
	movzbl	%al, %eax
.L82:
	movq	-8(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L124
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L123:
	.cfi_restore_state
	movl	$93, %r8d
	movl	$118, %edx
	movl	$121, %esi
	movl	$5, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	-12(%rbp), %eax
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L122:
	movl	$91, %r8d
	movl	$117, %edx
	movl	$121, %esi
	movl	$5, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	-12(%rbp), %eax
	testb	$2, %al
	je	.L90
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L121:
	movl	$89, %r8d
	movl	$121, %edx
	movl	$121, %esi
	movl	$5, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	-12(%rbp), %eax
	testb	$1, %al
	je	.L89
	jmp	.L122
	.p2align 4,,10
	.p2align 3
.L120:
	movl	$87, %r8d
	movl	$115, %edx
	movl	$121, %esi
	movl	$5, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	-12(%rbp), %eax
	testb	$4, %al
	je	.L88
	jmp	.L121
	.p2align 4,,10
	.p2align 3
.L119:
	movl	$85, %r8d
	movl	$116, %edx
	movl	$121, %esi
	movl	$5, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	-12(%rbp), %eax
	testb	$64, %al
	je	.L87
	jmp	.L120
	.p2align 4,,10
	.p2align 3
.L118:
	movl	$83, %r8d
	movl	$119, %edx
	movl	$121, %esi
	movl	$5, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	-12(%rbp), %eax
	testb	$32, %al
	je	.L86
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L117:
	movl	$81, %r8d
	movl	$120, %edx
	movl	$121, %esi
	movl	$5, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	-12(%rbp), %eax
	testb	$16, %al
	je	.L85
	jmp	.L118
.L124:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE423:
	.size	DH_check_ex, .-DH_check_ex
	.p2align 4
	.globl	DH_check_pub_key
	.type	DH_check_pub_key, @function
DH_check_pub_key:
.LFB426:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$8, %rsp
	movl	$0, (%rdx)
	call	BN_CTX_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L128
	movq	%rax, %rdi
	call	BN_CTX_start@PLT
	movq	%r12, %rdi
	call	BN_CTX_get@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L128
	movl	$1, %esi
	movq	%rax, %rdi
	call	BN_set_word@PLT
	testl	%eax, %eax
	je	.L128
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	BN_cmp@PLT
	testl	%eax, %eax
	jle	.L150
.L129:
	movq	8(%r15), %rsi
	movq	%r13, %rdi
	call	BN_copy@PLT
	testq	%rax, %rax
	je	.L128
	movl	$1, %esi
	movq	%r13, %rdi
	call	BN_sub_word@PLT
	testl	%eax, %eax
	je	.L128
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	BN_cmp@PLT
	testl	%eax, %eax
	js	.L130
	orl	$2, (%rbx)
.L130:
	movq	64(%r15), %rdx
	testq	%rdx, %rdx
	je	.L149
	movq	8(%r15), %rcx
	movq	%r12, %r8
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	BN_mod_exp@PLT
	testl	%eax, %eax
	je	.L128
	movq	%r13, %rdi
	call	BN_is_one@PLT
	testl	%eax, %eax
	jne	.L149
	orl	$4, (%rbx)
	.p2align 4,,10
	.p2align 3
.L149:
	movl	$1, %r13d
	jmp	.L127
	.p2align 4,,10
	.p2align 3
.L128:
	xorl	%r13d, %r13d
.L127:
	movq	%r12, %rdi
	call	BN_CTX_end@PLT
	movq	%r12, %rdi
	call	BN_CTX_free@PLT
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L150:
	.cfi_restore_state
	orl	$1, (%rbx)
	jmp	.L129
	.cfi_endproc
.LFE426:
	.size	DH_check_pub_key, .-DH_check_pub_key
	.p2align 4
	.globl	DH_check_pub_key_ex
	.type	DH_check_pub_key_ex, @function
DH_check_pub_key_ex:
.LFB425:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-12(%rbp), %rdx
	movl	$0, -12(%rbp)
	call	DH_check_pub_key
	testl	%eax, %eax
	je	.L151
	movl	-12(%rbp), %eax
	testb	$1, %al
	jne	.L170
	testb	$2, %al
	jne	.L171
.L154:
	testb	$4, %al
	jne	.L172
.L155:
	testl	%eax, %eax
	sete	%al
	movzbl	%al, %eax
.L151:
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L173
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L172:
	.cfi_restore_state
	movl	$175, %r8d
	movl	$122, %edx
	movl	$123, %esi
	movl	$5, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	-12(%rbp), %eax
	jmp	.L155
	.p2align 4,,10
	.p2align 3
.L171:
	movl	$173, %r8d
	movl	$123, %edx
	movl	$123, %esi
	movl	$5, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	-12(%rbp), %eax
	testb	$4, %al
	je	.L155
	jmp	.L172
	.p2align 4,,10
	.p2align 3
.L170:
	movl	$171, %r8d
	movl	$124, %edx
	movl	$123, %esi
	movl	$5, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	-12(%rbp), %eax
	testb	$2, %al
	je	.L154
	jmp	.L171
.L173:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE425:
	.size	DH_check_pub_key_ex, .-DH_check_pub_key_ex
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
