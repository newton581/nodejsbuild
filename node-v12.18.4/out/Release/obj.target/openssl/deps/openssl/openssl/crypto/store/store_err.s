	.file	"store_err.c"
	.text
	.p2align 4
	.globl	ERR_load_OSSL_STORE_strings
	.type	ERR_load_OSSL_STORE_strings, @function
ERR_load_OSSL_STORE_strings:
.LFB203:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$738725888, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_func_error_string@PLT
	testq	%rax, %rax
	je	.L5
	movl	$1, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	.cfi_restore_state
	leaq	OSSL_STORE_str_functs(%rip), %rdi
	call	ERR_load_strings_const@PLT
	leaq	OSSL_STORE_str_reasons(%rip), %rdi
	call	ERR_load_strings_const@PLT
	movl	$1, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE203:
	.size	ERR_load_OSSL_STORE_strings, .-ERR_load_OSSL_STORE_strings
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"ambiguous content type"
.LC1:
	.string	"bad password read"
.LC2:
	.string	"error verifying pkcs12 mac"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"fingerprint size does not match digest"
	.section	.rodata.str1.1
.LC4:
	.string	"invalid scheme"
.LC5:
	.string	"is not a"
.LC6:
	.string	"loader incomplete"
.LC7:
	.string	"loading started"
.LC8:
	.string	"not a certificate"
.LC9:
	.string	"not a crl"
.LC10:
	.string	"not a key"
.LC11:
	.string	"not a name"
.LC12:
	.string	"not parameters"
.LC13:
	.string	"passphrase callback error"
.LC14:
	.string	"path must be absolute"
	.section	.rodata.str1.8
	.align 8
.LC15:
	.string	"search only supported for directories"
	.align 8
.LC16:
	.string	"ui process interrupted or cancelled"
	.section	.rodata.str1.1
.LC17:
	.string	"unregistered scheme"
.LC18:
	.string	"unsupported content type"
.LC19:
	.string	"unsupported operation"
.LC20:
	.string	"unsupported search type"
.LC21:
	.string	"uri authority unsupported"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	OSSL_STORE_str_reasons, @object
	.size	OSSL_STORE_str_reasons, 368
OSSL_STORE_str_reasons:
	.quad	738197611
	.quad	.LC0
	.quad	738197619
	.quad	.LC1
	.quad	738197617
	.quad	.LC2
	.quad	738197625
	.quad	.LC3
	.quad	738197610
	.quad	.LC4
	.quad	738197616
	.quad	.LC5
	.quad	738197620
	.quad	.LC6
	.quad	738197621
	.quad	.LC7
	.quad	738197604
	.quad	.LC8
	.quad	738197605
	.quad	.LC9
	.quad	738197606
	.quad	.LC10
	.quad	738197607
	.quad	.LC11
	.quad	738197608
	.quad	.LC12
	.quad	738197618
	.quad	.LC13
	.quad	738197612
	.quad	.LC14
	.quad	738197623
	.quad	.LC15
	.quad	738197613
	.quad	.LC16
	.quad	738197609
	.quad	.LC17
	.quad	738197614
	.quad	.LC18
	.quad	738197622
	.quad	.LC19
	.quad	738197624
	.quad	.LC20
	.quad	738197615
	.quad	.LC21
	.quad	0
	.quad	0
	.section	.rodata.str1.1
.LC22:
	.string	"file_ctrl"
.LC23:
	.string	"file_find"
.LC24:
	.string	"file_get_pass"
.LC25:
	.string	"file_load"
.LC26:
	.string	"file_load_try_decode"
.LC27:
	.string	"file_name_to_uri"
.LC28:
	.string	"file_open"
.LC29:
	.string	"ossl_store_attach_pem_bio"
.LC30:
	.string	"OSSL_STORE_expect"
	.section	.rodata.str1.8
	.align 8
.LC31:
	.string	"ossl_store_file_attach_pem_bio_int"
	.section	.rodata.str1.1
.LC32:
	.string	"OSSL_STORE_find"
.LC33:
	.string	"ossl_store_get0_loader_int"
.LC34:
	.string	"OSSL_STORE_INFO_get1_CERT"
.LC35:
	.string	"OSSL_STORE_INFO_get1_CRL"
.LC36:
	.string	"OSSL_STORE_INFO_get1_NAME"
	.section	.rodata.str1.8
	.align 8
.LC37:
	.string	"OSSL_STORE_INFO_get1_NAME_description"
	.section	.rodata.str1.1
.LC38:
	.string	"OSSL_STORE_INFO_get1_PARAMS"
.LC39:
	.string	"OSSL_STORE_INFO_get1_PKEY"
.LC40:
	.string	"OSSL_STORE_INFO_new_CERT"
.LC41:
	.string	"OSSL_STORE_INFO_new_CRL"
.LC42:
	.string	"ossl_store_info_new_EMBEDDED"
.LC43:
	.string	"OSSL_STORE_INFO_new_NAME"
.LC44:
	.string	"OSSL_STORE_INFO_new_PARAMS"
.LC45:
	.string	"OSSL_STORE_INFO_new_PKEY"
	.section	.rodata.str1.8
	.align 8
.LC46:
	.string	"OSSL_STORE_INFO_set0_NAME_description"
	.section	.rodata.str1.1
.LC47:
	.string	"ossl_store_init_once"
.LC48:
	.string	"OSSL_STORE_LOADER_new"
.LC49:
	.string	"OSSL_STORE_open"
.LC50:
	.string	""
	.section	.rodata.str1.8
	.align 8
.LC51:
	.string	"ossl_store_register_loader_int"
	.section	.rodata.str1.1
.LC52:
	.string	"OSSL_STORE_SEARCH_by_alias"
	.section	.rodata.str1.8
	.align 8
.LC53:
	.string	"OSSL_STORE_SEARCH_by_issuer_serial"
	.align 8
.LC54:
	.string	"OSSL_STORE_SEARCH_by_key_fingerprint"
	.section	.rodata.str1.1
.LC55:
	.string	"OSSL_STORE_SEARCH_by_name"
	.section	.rodata.str1.8
	.align 8
.LC56:
	.string	"ossl_store_unregister_loader_int"
	.section	.rodata.str1.1
.LC57:
	.string	"try_decode_params"
.LC58:
	.string	"try_decode_PKCS12"
.LC59:
	.string	"try_decode_PKCS8Encrypted"
	.section	.data.rel.ro.local
	.align 32
	.type	OSSL_STORE_str_functs, @object
	.size	OSSL_STORE_str_functs, 624
OSSL_STORE_str_functs:
	.quad	738725888
	.quad	.LC22
	.quad	738762752
	.quad	.LC23
	.quad	738680832
	.quad	.LC24
	.quad	738684928
	.quad	.LC25
	.quad	738705408
	.quad	.LC26
	.quad	738713600
	.quad	.LC27
	.quad	738689024
	.quad	.LC28
	.quad	738717696
	.quad	.LC29
	.quad	738729984
	.quad	.LC30
	.quad	738721792
	.quad	.LC31
	.quad	738734080
	.quad	.LC32
	.quad	738607104
	.quad	.LC33
	.quad	738611200
	.quad	.LC34
	.quad	738615296
	.quad	.LC35
	.quad	738619392
	.quad	.LC36
	.quad	738750464
	.quad	.LC37
	.quad	738623488
	.quad	.LC38
	.quad	738627584
	.quad	.LC39
	.quad	738631680
	.quad	.LC40
	.quad	738635776
	.quad	.LC41
	.quad	738701312
	.quad	.LC42
	.quad	738643968
	.quad	.LC43
	.quad	738648064
	.quad	.LC44
	.quad	738652160
	.quad	.LC45
	.quad	738746368
	.quad	.LC46
	.quad	738656256
	.quad	.LC47
	.quad	738660352
	.quad	.LC48
	.quad	738664448
	.quad	.LC49
	.quad	738668544
	.quad	.LC50
	.quad	738676736
	.quad	.LC51
	.quad	738738176
	.quad	.LC52
	.quad	738742272
	.quad	.LC53
	.quad	738754560
	.quad	.LC54
	.quad	738758656
	.quad	.LC55
	.quad	738672640
	.quad	.LC56
	.quad	738693120
	.quad	.LC57
	.quad	738697216
	.quad	.LC58
	.quad	738709504
	.quad	.LC59
	.quad	0
	.quad	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
