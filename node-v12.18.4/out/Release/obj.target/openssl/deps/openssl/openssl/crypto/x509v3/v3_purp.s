	.file	"v3_purp.c"
	.text
	.p2align 4
	.type	xp_cmp, @function
xp_cmp:
.LFB1298:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	(%rsi), %rdx
	movl	(%rax), %eax
	subl	(%rdx), %eax
	ret
	.cfi_endproc
.LFE1298:
	.size	xp_cmp, .-xp_cmp
	.p2align 4
	.type	nid_cmp_BSEARCH_CMP_FN, @function
nid_cmp_BSEARCH_CMP_FN:
.LFB1313:
	.cfi_startproc
	endbr64
	movl	(%rdi), %eax
	subl	(%rsi), %eax
	ret
	.cfi_endproc
.LFE1313:
	.size	nid_cmp_BSEARCH_CMP_FN, .-nid_cmp_BSEARCH_CMP_FN
	.p2align 4
	.type	no_check, @function
no_check:
.LFB1333:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE1333:
	.size	no_check, .-no_check
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/x509v3/v3_purp.c"
	.text
	.p2align 4
	.type	xptable_free, @function
xptable_free:
.LFB1306:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L18
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movl	8(%rdi), %eax
	testb	$1, %al
	je	.L5
	testb	$2, %al
	jne	.L21
.L7:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$228, %edx
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	leaq	.LC0(%rip), %rsi
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_free@PLT
	.p2align 4,,10
	.p2align 3
.L5:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	.cfi_restore_state
	movq	24(%rdi), %rdi
	movl	$225, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	32(%r12), %rdi
	movl	$226, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L18:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE1306:
	.size	xptable_free, .-xptable_free
	.p2align 4
	.type	check_purpose_ssl_client, @function
check_purpose_ssl_client:
.LFB1324:
	.cfi_startproc
	endbr64
	movl	224(%rsi), %edi
	testb	$4, %dil
	je	.L23
	xorl	%eax, %eax
	testb	$2, 232(%rsi)
	je	.L22
.L23:
	movq	224(%rsi), %rcx
	testl	%edx, %edx
	jne	.L45
	movabsq	$584115552258, %rax
	andq	%rax, %rcx
	xorl	%eax, %eax
	cmpq	$2, %rcx
	je	.L22
	andl	$8, %edi
	movl	$1, %eax
	je	.L22
	movl	236(%rsi), %eax
	shrl	$7, %eax
	andl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	movl	$3, %eax
.L22:
	ret
	.p2align 4,,10
	.p2align 3
.L45:
	movabsq	$17179869186, %rax
	andq	%rax, %rcx
	xorl	%eax, %eax
	cmpq	$2, %rcx
	je	.L22
	movl	%edi, %eax
	testb	$1, %dil
	jne	.L46
	andl	$8256, %eax
	cmpl	$8256, %eax
	je	.L29
	movl	$4, %eax
	testb	$2, %dil
	jne	.L22
	xorl	%eax, %eax
	andl	$8, %edi
	je	.L22
	movl	236(%rsi), %edx
	testb	$7, %dl
	je	.L22
	movl	%edx, %eax
	sall	$29, %eax
	sarl	$31, %eax
	andl	$5, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L46:
	shrl	$4, %eax
	andl	$1, %eax
	ret
	.cfi_endproc
.LFE1324:
	.size	check_purpose_ssl_client, .-check_purpose_ssl_client
	.p2align 4
	.type	check_purpose_crl_sign, @function
check_purpose_crl_sign:
.LFB1330:
	.cfi_startproc
	endbr64
	movq	224(%rsi), %rax
	testl	%edx, %edx
	je	.L48
	movabsq	$17179869186, %rdx
	xorl	%r8d, %r8d
	andq	%rdx, %rax
	cmpq	$2, %rax
	je	.L47
	movl	224(%rsi), %eax
	testb	$1, %al
	jne	.L58
	movl	%eax, %edx
	andl	$8256, %edx
	cmpl	$8256, %edx
	je	.L52
	movl	$4, %r8d
	testb	$2, %al
	jne	.L47
	xorl	%r8d, %r8d
	testb	$8, %al
	je	.L47
	movl	236(%rsi), %eax
	andl	$7, %eax
	cmpl	$1, %eax
	sbbl	%r8d, %r8d
	notl	%r8d
	andl	$5, %r8d
.L47:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L48:
	movabsq	$8589934594, %rdx
	xorl	%r8d, %r8d
	andq	%rdx, %rax
	cmpq	$2, %rax
	setne	%r8b
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L58:
	shrl	$4, %eax
	andl	$1, %eax
	movl	%eax, %r8d
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L52:
	movl	$3, %r8d
	jmp	.L47
	.cfi_endproc
.LFE1330:
	.size	check_purpose_crl_sign, .-check_purpose_crl_sign
	.p2align 4
	.type	ocsp_helper, @function
ocsp_helper:
.LFB1331:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	testl	%edx, %edx
	jne	.L71
.L59:
	ret
	.p2align 4,,10
	.p2align 3
.L71:
	movabsq	$17179869186, %rdx
	xorl	%eax, %eax
	andq	224(%rsi), %rdx
	cmpq	$2, %rdx
	je	.L59
	movl	224(%rsi), %edx
	movl	%edx, %eax
	testb	$1, %dl
	jne	.L72
	andl	$8256, %eax
	cmpl	$8256, %eax
	je	.L64
	movl	$4, %eax
	testb	$2, %dl
	jne	.L59
	xorl	%eax, %eax
	andl	$8, %edx
	je	.L59
	movl	236(%rsi), %eax
	andl	$7, %eax
	cmpl	$1, %eax
	sbbl	%eax, %eax
	notl	%eax
	andl	$5, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L72:
	shrl	$4, %eax
	andl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L64:
	movl	$3, %eax
	ret
	.cfi_endproc
.LFE1331:
	.size	ocsp_helper, .-ocsp_helper
	.p2align 4
	.type	check_purpose_smime_sign, @function
check_purpose_smime_sign:
.LFB1328:
	.cfi_startproc
	endbr64
	movl	224(%rsi), %ecx
	testb	$4, %cl
	je	.L74
	xorl	%eax, %eax
	testb	$4, 232(%rsi)
	je	.L73
.L74:
	testl	%edx, %edx
	je	.L76
	movabsq	$17179869186, %rdx
	xorl	%eax, %eax
	andq	224(%rsi), %rdx
	cmpq	$2, %rdx
	je	.L73
	movl	%ecx, %eax
	testb	$1, %cl
	jne	.L97
	andl	$8256, %eax
	cmpl	$8256, %eax
	je	.L81
	movl	$4, %eax
	testb	$2, %cl
	jne	.L73
	xorl	%eax, %eax
	andl	$8, %ecx
	je	.L73
	movl	236(%rsi), %edx
	testb	$7, %dl
	jne	.L98
.L73:
	ret
	.p2align 4,,10
	.p2align 3
.L76:
	andl	$8, %ecx
	je	.L87
	movl	236(%rsi), %eax
	testb	$32, %al
	jne	.L87
	testb	$-128, %al
	jne	.L88
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L87:
	movl	$1, %eax
.L78:
	movabsq	$824633720834, %rdx
	andq	224(%rsi), %rdx
	cmpq	$2, %rdx
	movl	$0, %edx
	cmove	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L97:
	shrl	$4, %eax
	andl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L98:
	movl	%edx, %eax
	sall	$30, %eax
	sarl	$31, %eax
	andl	$5, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L88:
	movl	$2, %eax
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L81:
	movl	$3, %eax
	ret
	.cfi_endproc
.LFE1328:
	.size	check_purpose_smime_sign, .-check_purpose_smime_sign
	.p2align 4
	.type	check_purpose_smime_encrypt, @function
check_purpose_smime_encrypt:
.LFB1329:
	.cfi_startproc
	endbr64
	movl	224(%rsi), %ecx
	testb	$4, %cl
	je	.L100
	xorl	%eax, %eax
	testb	$4, 232(%rsi)
	je	.L99
.L100:
	testl	%edx, %edx
	je	.L102
	movabsq	$17179869186, %rdx
	xorl	%eax, %eax
	andq	224(%rsi), %rdx
	cmpq	$2, %rdx
	je	.L99
	movl	%ecx, %eax
	testb	$1, %cl
	jne	.L123
	andl	$8256, %eax
	cmpl	$8256, %eax
	je	.L107
	movl	$4, %eax
	testb	$2, %cl
	jne	.L99
	xorl	%eax, %eax
	andl	$8, %ecx
	je	.L99
	movl	236(%rsi), %edx
	testb	$7, %dl
	jne	.L124
.L99:
	ret
	.p2align 4,,10
	.p2align 3
.L102:
	andl	$8, %ecx
	je	.L113
	movl	236(%rsi), %eax
	testb	$32, %al
	jne	.L113
	testb	$-128, %al
	jne	.L114
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L113:
	movl	$1, %eax
.L104:
	movabsq	$137438953474, %rdx
	andq	224(%rsi), %rdx
	cmpq	$2, %rdx
	movl	$0, %edx
	cmove	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L123:
	shrl	$4, %eax
	andl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L124:
	movl	%edx, %eax
	sall	$30, %eax
	sarl	$31, %eax
	andl	$5, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L114:
	movl	$2, %eax
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L107:
	movl	$3, %eax
	ret
	.cfi_endproc
.LFE1329:
	.size	check_purpose_smime_encrypt, .-check_purpose_smime_encrypt
	.p2align 4
	.type	check_purpose_ns_ssl_server, @function
check_purpose_ns_ssl_server:
.LFB1326:
	.cfi_startproc
	endbr64
	movl	224(%rsi), %ecx
	testb	$4, %cl
	je	.L126
	xorl	%eax, %eax
	testb	$17, 232(%rsi)
	je	.L125
.L126:
	testl	%edx, %edx
	jne	.L152
	andl	$8, %ecx
	jne	.L153
.L130:
	movq	224(%rsi), %rax
	movabsq	$721554505730, %rdx
	andq	%rax, %rdx
	cmpq	$2, %rdx
	je	.L154
	movabsq	$137438953474, %rdx
	andq	%rdx, %rax
	cmpq	$2, %rax
	setne	%al
	movzbl	%al, %eax
.L125:
	ret
	.p2align 4,,10
	.p2align 3
.L153:
	xorl	%eax, %eax
	testb	$64, 236(%rsi)
	jne	.L130
	ret
	.p2align 4,,10
	.p2align 3
.L152:
	movabsq	$17179869186, %rdx
	xorl	%eax, %eax
	andq	224(%rsi), %rdx
	cmpq	$2, %rdx
	je	.L125
	movl	%ecx, %eax
	testb	$1, %cl
	jne	.L155
	andl	$8256, %eax
	cmpl	$8256, %eax
	je	.L134
	movl	$4, %eax
	testb	$2, %cl
	jne	.L125
	xorl	%eax, %eax
	andl	$8, %ecx
	je	.L125
	movl	236(%rsi), %edx
	testb	$7, %dl
	je	.L125
	movl	%edx, %eax
	sall	$29, %eax
	sarl	$31, %eax
	andl	$5, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L155:
	shrl	$4, %eax
	andl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L154:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L134:
	movl	$3, %eax
	ret
	.cfi_endproc
.LFE1326:
	.size	check_purpose_ns_ssl_server, .-check_purpose_ns_ssl_server
	.p2align 4
	.type	check_purpose_timestamp_sign, @function
check_purpose_timestamp_sign:
.LFB1332:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	subq	$8, %rsp
	testl	%edx, %edx
	jne	.L179
	movl	%edx, %eax
	movl	224(%rsi), %edx
	testb	$2, %dl
	jne	.L180
.L160:
	andl	$4, %edx
	je	.L158
	cmpl	$64, 232(%r12)
	je	.L181
.L158:
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L180:
	.cfi_restore_state
	movl	228(%rsi), %ecx
	testl	$-193, %ecx
	jne	.L158
	andl	$192, %ecx
	jne	.L160
	jmp	.L158
	.p2align 4,,10
	.p2align 3
.L179:
	movabsq	$17179869186, %rdx
	xorl	%eax, %eax
	andq	224(%rsi), %rdx
	cmpq	$2, %rdx
	je	.L158
	movl	224(%rsi), %edx
	movl	%edx, %eax
	testb	$1, %dl
	jne	.L182
	andl	$8256, %eax
	cmpl	$8256, %eax
	je	.L162
	movl	$4, %eax
	testb	$2, %dl
	jne	.L158
	xorl	%eax, %eax
	andl	$8, %edx
	je	.L158
	movl	236(%rsi), %eax
	andl	$7, %eax
	cmpl	$1, %eax
	sbbl	%eax, %eax
	notl	%eax
	andl	$5, %eax
	jmp	.L158
	.p2align 4,,10
	.p2align 3
.L182:
	addq	$8, %rsp
	shrl	$4, %eax
	andl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L181:
	.cfi_restore_state
	movl	$126, %esi
	movl	$-1, %edx
	movq	%r12, %rdi
	call	X509_get_ext_by_NID@PLT
	movl	%eax, %esi
	movl	$1, %eax
	testl	%esi, %esi
	js	.L158
	movq	%r12, %rdi
	call	X509_get_ext@PLT
	movq	%rax, %rdi
	call	X509_EXTENSION_get_critical@PLT
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
	jmp	.L158
	.p2align 4,,10
	.p2align 3
.L162:
	movl	$3, %eax
	jmp	.L158
	.cfi_endproc
.LFE1332:
	.size	check_purpose_timestamp_sign, .-check_purpose_timestamp_sign
	.p2align 4
	.type	check_purpose_ssl_server, @function
check_purpose_ssl_server:
.LFB1325:
	.cfi_startproc
	endbr64
	movl	224(%rsi), %eax
	testb	$4, %al
	je	.L184
	xorl	%r8d, %r8d
	testb	$17, 232(%rsi)
	je	.L183
.L184:
	testl	%edx, %edx
	jne	.L208
	testb	$8, %al
	je	.L188
	xorl	%r8d, %r8d
	testb	$64, 236(%rsi)
	je	.L183
.L188:
	movabsq	$721554505730, %rax
	xorl	%r8d, %r8d
	andq	224(%rsi), %rax
	cmpq	$2, %rax
	setne	%r8b
.L183:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L208:
	movabsq	$17179869186, %rdx
	xorl	%r8d, %r8d
	andq	224(%rsi), %rdx
	cmpq	$2, %rdx
	je	.L183
	testb	$1, %al
	jne	.L209
	movl	%eax, %edx
	andl	$8256, %edx
	cmpl	$8256, %edx
	je	.L191
	movl	$4, %r8d
	testb	$2, %al
	jne	.L183
	xorl	%r8d, %r8d
	testb	$8, %al
	je	.L183
	movl	236(%rsi), %eax
	testb	$7, %al
	je	.L183
	sall	$29, %eax
	movl	%eax, %r8d
	sarl	$31, %r8d
	andl	$5, %r8d
	jmp	.L183
	.p2align 4,,10
	.p2align 3
.L209:
	shrl	$4, %eax
	andl	$1, %eax
	movl	%eax, %r8d
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L191:
	movl	$3, %r8d
	jmp	.L183
	.cfi_endproc
.LFE1325:
	.size	check_purpose_ssl_server, .-check_purpose_ssl_server
	.p2align 4
	.globl	X509_PURPOSE_set
	.type	X509_PURPOSE_set, @function
X509_PURPOSE_set:
.LFB1300:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%esi, %ebx
	subq	$64, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leal	-1(%rsi), %eax
	cmpl	$8, %eax
	jbe	.L211
	movq	xptable(%rip), %rdi
	testq	%rdi, %rdi
	je	.L214
	movl	%esi, -80(%rbp)
	leaq	-80(%rbp), %rsi
	call	OPENSSL_sk_find@PLT
	testl	%eax, %eax
	js	.L214
.L211:
	movl	%ebx, (%r12)
	movl	$1, %eax
.L210:
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L217
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L214:
	.cfi_restore_state
	movl	$100, %r8d
	movl	$146, %edx
	movl	$141, %esi
	movl	$34, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L210
.L217:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1300:
	.size	X509_PURPOSE_set, .-X509_PURPOSE_set
	.p2align 4
	.globl	X509_PURPOSE_get_count
	.type	X509_PURPOSE_get_count, @function
X509_PURPOSE_get_count:
.LFB1301:
	.cfi_startproc
	endbr64
	movq	xptable(%rip), %rdi
	testq	%rdi, %rdi
	je	.L220
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	OPENSSL_sk_num@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	addl	$9, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L220:
	.cfi_restore 6
	movl	$9, %eax
	ret
	.cfi_endproc
.LFE1301:
	.size	X509_PURPOSE_get_count, .-X509_PURPOSE_get_count
	.p2align 4
	.globl	X509_PURPOSE_get0
	.type	X509_PURPOSE_get0, @function
X509_PURPOSE_get0:
.LFB1302:
	.cfi_startproc
	endbr64
	testl	%edi, %edi
	js	.L228
	cmpl	$8, %edi
	jg	.L227
	movslq	%edi, %rdi
	leaq	xstandard(%rip), %rdx
	leaq	(%rdi,%rdi,2), %rax
	salq	$4, %rax
	addq	%rdx, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L227:
	leal	-9(%rdi), %esi
	movq	xptable(%rip), %rdi
	jmp	OPENSSL_sk_value@PLT
	.p2align 4,,10
	.p2align 3
.L228:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1302:
	.size	X509_PURPOSE_get0, .-X509_PURPOSE_get0
	.p2align 4
	.globl	X509_PURPOSE_get_by_sname
	.type	X509_PURPOSE_get_by_sname, @function
X509_PURPOSE_get_by_sname:
.LFB1303:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	xstandard(%rip), %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	xorl	%ebx, %ebx
	subq	$8, %rsp
	jmp	.L230
	.p2align 4,,10
	.p2align 3
.L244:
	leaq	(%rbx,%rbx,2), %rax
	movq	%r13, %rsi
	addq	$1, %rbx
	salq	$4, %rax
	movq	32(%r15,%rax), %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L229
.L230:
	movq	xptable(%rip), %rdi
	movl	%ebx, %r12d
	movl	%ebx, %r14d
	testq	%rdi, %rdi
	je	.L237
	call	OPENSSL_sk_num@PLT
	addl	$9, %eax
	cmpl	%ebx, %eax
	jle	.L243
.L236:
	cmpl	$8, %ebx
	jle	.L244
	movq	xptable(%rip), %rdi
	leal	-9(%r12), %esi
	addq	$1, %rbx
	call	OPENSSL_sk_value@PLT
	movq	%r13, %rsi
	movq	32(%rax), %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L230
.L229:
	addq	$8, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L237:
	.cfi_restore_state
	movl	$9, %eax
	cmpl	%ebx, %eax
	jg	.L236
	.p2align 4,,10
	.p2align 3
.L243:
	movl	$-1, %r14d
	jmp	.L229
	.cfi_endproc
.LFE1303:
	.size	X509_PURPOSE_get_by_sname, .-X509_PURPOSE_get_by_sname
	.p2align 4
	.globl	X509_PURPOSE_get_by_id
	.type	X509_PURPOSE_get_by_id, @function
X509_PURPOSE_get_by_id:
.LFB1304:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$64, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leal	-1(%rdi), %eax
	cmpl	$8, %eax
	jbe	.L245
	movq	xptable(%rip), %r8
	testq	%r8, %r8
	je	.L250
	movl	%edi, -64(%rbp)
	leaq	-64(%rbp), %rsi
	movq	%r8, %rdi
	call	OPENSSL_sk_find@PLT
	testl	%eax, %eax
	js	.L250
	addl	$9, %eax
.L245:
	movq	-8(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L253
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L250:
	.cfi_restore_state
	movl	$-1, %eax
	jmp	.L245
.L253:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1304:
	.size	X509_PURPOSE_get_by_id, .-X509_PURPOSE_get_by_id
	.section	.text.unlikely,"ax",@progbits
.LCOLDB1:
	.text
.LHOTB1:
	.p2align 4
	.globl	X509_PURPOSE_add
	.type	X509_PURPOSE_add, @function
X509_PURPOSE_add:
.LFB1305:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%esi, %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r9, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edi, %ebx
	subq	$104, %rsp
	movq	16(%rbp), %rax
	movl	%edx, -116(%rbp)
	movq	%rcx, -128(%rbp)
	movq	%rax, -136(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leal	-1(%rdi), %eax
	cmpl	$8, %eax
	jbe	.L282
	movq	xptable(%rip), %rdi
	testq	%rdi, %rdi
	je	.L260
	leaq	-112(%rbp), %rsi
	movl	%ebx, -112(%rbp)
	call	OPENSSL_sk_find@PLT
	movl	%eax, %esi
	testl	%eax, %eax
	js	.L260
	addl	$9, %eax
	js	.L283
	movq	xptable(%rip), %rdi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %r14
	testb	$2, 8(%r14)
	jne	.L284
.L266:
	movl	$181, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r13, %rdi
	call	CRYPTO_strdup@PLT
	movl	$182, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, 24(%r14)
	call	CRYPTO_strdup@PLT
	cmpq	$0, 24(%r14)
	movq	%rax, 32(%r14)
	je	.L270
	testq	%rax, %rax
	je	.L270
	movl	8(%r14), %edx
	movl	-116(%rbp), %eax
	movl	%ebx, (%r14)
	movl	%r15d, 4(%r14)
	andl	$-2, %eax
	andl	$1, %edx
	orl	%eax, %edx
	movq	-128(%rbp), %rax
	orl	$2, %edx
	movq	%rax, 16(%r14)
	movq	-136(%rbp), %rax
	movl	%edx, 8(%r14)
	movq	%rax, 40(%r14)
.L269:
	movl	$1, %eax
.L254:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L285
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L284:
	.cfi_restore_state
	movq	24(%r14), %rdi
	movl	$177, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	32(%r14), %rdi
	movl	$178, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	jmp	.L266
	.p2align 4,,10
	.p2align 3
.L270:
	movl	$184, %r8d
.L281:
	movl	$65, %edx
	movl	$137, %esi
	movl	$34, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L254
	.p2align 4,,10
	.p2align 3
.L282:
	cltq
	leaq	(%rax,%rax,2), %rcx
	leaq	xstandard(%rip), %rax
	salq	$4, %rcx
	leaq	(%rcx,%rax), %r14
	testb	$2, 8(%r14)
	je	.L266
	jmp	.L284
	.p2align 4,,10
	.p2align 3
.L260:
	movl	$167, %edx
	leaq	.LC0(%rip), %rsi
	movl	$48, %edi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L286
	movl	$1, 8(%rax)
	movl	$181, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r13, %rdi
	call	CRYPTO_strdup@PLT
	movl	$182, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, 24(%r14)
	call	CRYPTO_strdup@PLT
	cmpq	$0, 24(%r14)
	movq	%rax, 32(%r14)
	je	.L264
	testq	%rax, %rax
	je	.L264
	movl	8(%r14), %eax
	movl	-116(%rbp), %edx
	movl	%ebx, (%r14)
	movq	xptable(%rip), %rdi
	movl	%r15d, 4(%r14)
	andl	$1, %eax
	andl	$-2, %edx
	orl	%eax, %edx
	movq	-128(%rbp), %rax
	orl	$2, %edx
	movq	%rax, 16(%r14)
	movq	-136(%rbp), %rax
	movl	%edx, 8(%r14)
	movq	%rax, 40(%r14)
	testq	%rdi, %rdi
	je	.L287
.L267:
	movq	%r14, %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	jne	.L269
	movl	$205, %r8d
.L280:
	movl	$65, %edx
	movl	$137, %esi
	movl	$34, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	24(%r14), %rdi
	movl	$212, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	32(%r14), %rdi
	movl	$213, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movl	$214, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r14, %rdi
	call	CRYPTO_free@PLT
	xorl	%eax, %eax
	jmp	.L254
	.p2align 4,,10
	.p2align 3
.L287:
	leaq	xp_cmp(%rip), %rdi
	call	OPENSSL_sk_new@PLT
	movl	$201, %r8d
	movq	%rax, xptable(%rip)
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L267
	jmp	.L280
	.p2align 4,,10
	.p2align 3
.L286:
	movl	$168, %r8d
	jmp	.L281
.L264:
	movl	$184, %r8d
	jmp	.L280
.L285:
	call	__stack_chk_fail@PLT
.L283:
	jmp	.L261
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	X509_PURPOSE_add.cold, @function
X509_PURPOSE_add.cold:
.LFSB1305:
.L261:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movl	8, %eax
	ud2
	.cfi_endproc
.LFE1305:
	.text
	.size	X509_PURPOSE_add, .-X509_PURPOSE_add
	.section	.text.unlikely
	.size	X509_PURPOSE_add.cold, .-X509_PURPOSE_add.cold
.LCOLDE1:
	.text
.LHOTE1:
	.p2align 4
	.globl	X509_PURPOSE_cleanup
	.type	X509_PURPOSE_cleanup, @function
X509_PURPOSE_cleanup:
.LFB1307:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	xptable(%rip), %rdi
	leaq	xptable_free(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	OPENSSL_sk_pop_free@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	movq	$0, xptable(%rip)
	ret
	.cfi_endproc
.LFE1307:
	.size	X509_PURPOSE_cleanup, .-X509_PURPOSE_cleanup
	.p2align 4
	.globl	X509_PURPOSE_get_id
	.type	X509_PURPOSE_get_id, @function
X509_PURPOSE_get_id:
.LFB1308:
	.cfi_startproc
	endbr64
	movl	(%rdi), %eax
	ret
	.cfi_endproc
.LFE1308:
	.size	X509_PURPOSE_get_id, .-X509_PURPOSE_get_id
	.p2align 4
	.globl	X509_PURPOSE_get0_name
	.type	X509_PURPOSE_get0_name, @function
X509_PURPOSE_get0_name:
.LFB1309:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	ret
	.cfi_endproc
.LFE1309:
	.size	X509_PURPOSE_get0_name, .-X509_PURPOSE_get0_name
	.p2align 4
	.globl	X509_PURPOSE_get0_sname
	.type	X509_PURPOSE_get0_sname, @function
X509_PURPOSE_get0_sname:
.LFB1310:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	ret
	.cfi_endproc
.LFE1310:
	.size	X509_PURPOSE_get0_sname, .-X509_PURPOSE_get0_sname
	.p2align 4
	.globl	X509_PURPOSE_get_trust
	.type	X509_PURPOSE_get_trust, @function
X509_PURPOSE_get_trust:
.LFB1311:
	.cfi_startproc
	endbr64
	movl	4(%rdi), %eax
	ret
	.cfi_endproc
.LFE1311:
	.size	X509_PURPOSE_get_trust, .-X509_PURPOSE_get_trust
	.p2align 4
	.globl	X509_supported_extension
	.type	X509_supported_extension, @function
X509_supported_extension:
.LFB1315:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	call	X509_EXTENSION_get_object@PLT
	movq	%rax, %rdi
	call	OBJ_obj2nid@PLT
	movl	%eax, -12(%rbp)
	testl	%eax, %eax
	je	.L294
	leaq	-12(%rbp), %rdi
	movl	$4, %ecx
	leaq	nid_cmp_BSEARCH_CMP_FN(%rip), %r8
	movl	$14, %edx
	leaq	supported_nids.20538(%rip), %rsi
	call	OBJ_bsearch_@PLT
	testq	%rax, %rax
	setne	%al
	movzbl	%al, %eax
.L294:
	movq	-8(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L301
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L301:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1315:
	.size	X509_supported_extension, .-X509_supported_extension
	.p2align 4
	.globl	X509_set_proxy_flag
	.type	X509_set_proxy_flag, @function
X509_set_proxy_flag:
.LFB1320:
	.cfi_startproc
	endbr64
	orl	$1024, 224(%rdi)
	ret
	.cfi_endproc
.LFE1320:
	.size	X509_set_proxy_flag, .-X509_set_proxy_flag
	.p2align 4
	.globl	X509_set_proxy_pathlen
	.type	X509_set_proxy_pathlen, @function
X509_set_proxy_pathlen:
.LFB1321:
	.cfi_startproc
	endbr64
	movq	%rsi, 216(%rdi)
	ret
	.cfi_endproc
.LFE1321:
	.size	X509_set_proxy_pathlen, .-X509_set_proxy_pathlen
	.p2align 4
	.globl	X509_check_akid
	.type	X509_check_akid, @function
X509_check_akid:
.LFB1335:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L329
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	movq	(%rsi), %rdi
	testq	%rdi, %rdi
	je	.L310
	movq	240(%r13), %rsi
	testq	%rsi, %rsi
	je	.L310
	call	ASN1_OCTET_STRING_cmp@PLT
	movl	%eax, %r8d
	movl	$30, %eax
	testl	%r8d, %r8d
	jne	.L304
.L310:
	movq	16(%rbx), %r12
	testq	%r12, %r12
	je	.L309
	movq	%r13, %rdi
	call	X509_get_serialNumber@PLT
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	ASN1_INTEGER_cmp@PLT
	testl	%eax, %eax
	jne	.L313
.L309:
	movq	8(%rbx), %r12
	xorl	%ebx, %ebx
	testq	%r12, %r12
	jne	.L311
.L312:
	xorl	%eax, %eax
.L304:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L316:
	.cfi_restore_state
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	OPENSSL_sk_value@PLT
	cmpl	$4, (%rax)
	je	.L330
	addl	$1, %ebx
.L311:
	movq	%r12, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jl	.L316
	jmp	.L312
	.p2align 4,,10
	.p2align 3
.L330:
	movq	8(%rax), %r12
	testq	%r12, %r12
	je	.L312
	movq	%r13, %rdi
	call	X509_get_issuer_name@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	X509_NAME_cmp@PLT
	testl	%eax, %eax
	je	.L312
.L313:
	addq	$8, %rsp
	movl	$31, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L329:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1335:
	.size	X509_check_akid, .-X509_check_akid
	.p2align 4
	.type	x509v3_cache_extensions, @function
x509v3_cache_extensions:
.LFB1318:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	344(%rdi), %eax
	testl	%eax, %eax
	je	.L437
.L331:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L438
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L437:
	.cfi_restore_state
	movq	%rdi, %rbx
	movq	336(%rdi), %rdi
	call	CRYPTO_THREAD_write_lock@PLT
	movl	224(%rbx), %r14d
	andl	$256, %r14d
	jne	.L436
	call	EVP_sha1@PLT
	xorl	%ecx, %ecx
	leaq	304(%rbx), %rdx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	X509_digest@PLT
	testl	%eax, %eax
	jne	.L334
	orl	$128, 224(%rbx)
.L334:
	movq	%rbx, %rdi
	call	X509_get_version@PLT
	testq	%rax, %rax
	je	.L439
.L335:
	leaq	-64(%rbp), %r13
	xorl	%ecx, %ecx
	movl	$87, %esi
	movq	%rbx, %rdi
	movq	%r13, %rdx
	call	X509_get_ext_d2i@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L336
	movl	(%rax), %ecx
	testl	%ecx, %ecx
	je	.L337
	orl	$16, 224(%rbx)
.L337:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L338
	cmpl	$258, 4(%rdi)
	je	.L434
	call	ASN1_INTEGER_get@PLT
	movl	(%r12), %edx
	movq	%rax, 208(%rbx)
	testl	%edx, %edx
	jne	.L340
	testq	%rax, %rax
	je	.L340
	.p2align 4,,10
	.p2align 3
.L434:
	orl	$128, 224(%rbx)
	movq	$0, 208(%rbx)
	jmp	.L340
	.p2align 4,,10
	.p2align 3
.L391:
	movq	16(%r12), %rdi
	movl	%r15d, %esi
	call	OPENSSL_sk_value@PLT
	cmpl	$4, (%rax)
	je	.L440
	addl	$1, %r15d
.L388:
	movq	16(%r12), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r15d
	jl	.L391
.L392:
	movq	%rbx, %rdi
	call	X509_get_issuer_name@PLT
	movq	%rax, %rsi
.L390:
	movq	(%r12), %rdi
	call	DIST_POINT_set_dpname@PLT
	testl	%eax, %eax
	jne	.L387
.L393:
	orl	$128, 224(%rbx)
.L380:
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movl	$290, %esi
	movq	%rbx, %rdi
	call	X509_get_ext_d2i@PLT
	movq	%rax, 288(%rbx)
	testq	%rax, %rax
	je	.L441
.L396:
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movl	$291, %esi
	movq	%rbx, %rdi
	call	X509_get_ext_d2i@PLT
	movq	%rax, 296(%rbx)
	testq	%rax, %rax
	je	.L442
.L397:
	movl	$0, -64(%rbp)
	leaq	nid_cmp_BSEARCH_CMP_FN(%rip), %r13
	jmp	.L398
	.p2align 4,,10
	.p2align 3
.L443:
	movq	%r13, %r8
	movl	$4, %ecx
	movl	$14, %edx
	movq	%r14, %rdi
	leaq	supported_nids.20538(%rip), %rsi
	call	OBJ_bsearch_@PLT
	testq	%rax, %rax
	je	.L401
.L400:
	addl	$1, -64(%rbp)
.L398:
	movq	%rbx, %rdi
	call	X509_get_ext_count@PLT
	movl	-64(%rbp), %esi
	cmpl	%esi, %eax
	jle	.L402
	movq	%rbx, %rdi
	call	X509_get_ext@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	X509_EXTENSION_get_object@PLT
	movq	%rax, %rdi
	call	OBJ_obj2nid@PLT
	cmpl	$857, %eax
	jne	.L399
	orl	$4096, 224(%rbx)
.L399:
	movq	%r12, %rdi
	call	X509_EXTENSION_get_critical@PLT
	testl	%eax, %eax
	je	.L400
	movq	%r12, %rdi
	call	X509_EXTENSION_get_object@PLT
	movq	%rax, %rdi
	call	OBJ_obj2nid@PLT
	movl	%eax, -60(%rbp)
	testl	%eax, %eax
	jne	.L443
.L401:
	orl	$512, 224(%rbx)
.L402:
	movq	%rbx, %rdi
	call	x509_init_sig_info@PLT
	orl	$256, 224(%rbx)
	movl	$1, 344(%rbx)
.L436:
	movq	336(%rbx), %rdi
	call	CRYPTO_THREAD_unlock@PLT
	jmp	.L331
	.p2align 4,,10
	.p2align 3
.L439:
	orl	$64, 224(%rbx)
	jmp	.L335
	.p2align 4,,10
	.p2align 3
.L338:
	movq	$-1, 208(%rbx)
.L340:
	movq	%r12, %rdi
	call	BASIC_CONSTRAINTS_free@PLT
	orl	$1, 224(%rbx)
.L341:
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movl	$663, %esi
	movq	%rbx, %rdi
	call	X509_get_ext_d2i@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L342
	movl	224(%rbx), %eax
	testb	$16, %al
	je	.L444
.L343:
	orb	$-128, %al
	movl	%eax, 224(%rbx)
.L345:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L346
	call	ASN1_INTEGER_get@PLT
	movq	%rax, 216(%rbx)
.L347:
	movq	%r12, %rdi
	call	PROXY_CERT_INFO_EXTENSION_free@PLT
	orl	$1024, 224(%rbx)
.L348:
	movq	%rbx, %rdi
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movl	$83, %esi
	call	X509_get_ext_d2i@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L349
	movl	(%rax), %eax
	testl	%eax, %eax
	jle	.L350
	movq	8(%rdi), %rcx
	movzbl	(%rcx), %edx
	movl	%edx, 228(%rbx)
	cmpl	$1, %eax
	je	.L351
	movzbl	1(%rcx), %eax
	sall	$8, %eax
	orl	%edx, %eax
	movl	%eax, 228(%rbx)
.L351:
	orl	$2, 224(%rbx)
	call	ASN1_BIT_STRING_free@PLT
.L352:
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movl	$126, %esi
	movq	%rbx, %rdi
	movl	$0, 232(%rbx)
	call	X509_get_ext_d2i@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L353
	orl	$4, 224(%rbx)
	movq	%r15, %rdi
	leaq	.L358(%rip), %r12
	movl	$0, -64(%rbp)
	call	OPENSSL_sk_num@PLT
	movl	-64(%rbp), %esi
	cmpl	%eax, %esi
	jge	.L445
	.p2align 4,,10
	.p2align 3
.L367:
	movq	%r15, %rdi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %rdi
	call	OBJ_obj2nid@PLT
	cmpl	$180, %eax
	jg	.L355
	cmpl	$128, %eax
	jle	.L356
	subl	$129, %eax
	cmpl	$51, %eax
	ja	.L356
	movslq	(%r12,%rax,4), %rax
	addq	%r12, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L358:
	.long	.L364-.L358
	.long	.L363-.L358
	.long	.L362-.L358
	.long	.L361-.L358
	.long	.L360-.L358
	.long	.L356-.L358
	.long	.L356-.L358
	.long	.L356-.L358
	.long	.L359-.L358
	.long	.L356-.L358
	.long	.L359-.L358
	.long	.L356-.L358
	.long	.L356-.L358
	.long	.L356-.L358
	.long	.L356-.L358
	.long	.L356-.L358
	.long	.L356-.L358
	.long	.L356-.L358
	.long	.L356-.L358
	.long	.L356-.L358
	.long	.L356-.L358
	.long	.L356-.L358
	.long	.L356-.L358
	.long	.L356-.L358
	.long	.L356-.L358
	.long	.L356-.L358
	.long	.L356-.L358
	.long	.L356-.L358
	.long	.L356-.L358
	.long	.L356-.L358
	.long	.L356-.L358
	.long	.L356-.L358
	.long	.L356-.L358
	.long	.L356-.L358
	.long	.L356-.L358
	.long	.L356-.L358
	.long	.L356-.L358
	.long	.L356-.L358
	.long	.L356-.L358
	.long	.L356-.L358
	.long	.L356-.L358
	.long	.L356-.L358
	.long	.L356-.L358
	.long	.L356-.L358
	.long	.L356-.L358
	.long	.L356-.L358
	.long	.L356-.L358
	.long	.L356-.L358
	.long	.L356-.L358
	.long	.L356-.L358
	.long	.L356-.L358
	.long	.L357-.L358
	.text
	.p2align 4,,10
	.p2align 3
.L355:
	cmpl	$297, %eax
	je	.L365
	cmpl	$910, %eax
	jne	.L356
	orl	$256, 232(%rbx)
.L356:
	addl	$1, -64(%rbp)
	.p2align 4,,10
	.p2align 3
.L453:
	movq	%r15, %rdi
	call	OPENSSL_sk_num@PLT
	movl	-64(%rbp), %esi
	cmpl	%eax, %esi
	jl	.L367
.L445:
	movq	ASN1_OBJECT_free@GOTPCREL(%rip), %rsi
	movq	%r15, %rdi
	call	OPENSSL_sk_pop_free@PLT
.L368:
	movq	%rbx, %rdi
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movl	$71, %esi
	call	X509_get_ext_d2i@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L369
	movl	(%rax), %eax
	testl	%eax, %eax
	jle	.L370
	movq	8(%rdi), %rax
	movzbl	(%rax), %r14d
.L370:
	orl	$8, 224(%rbx)
	movl	%r14d, 236(%rbx)
	call	ASN1_BIT_STRING_free@PLT
.L371:
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movl	$82, %esi
	movq	%rbx, %rdi
	call	X509_get_ext_d2i@PLT
	movq	%rax, 240(%rbx)
	testq	%rax, %rax
	je	.L446
.L372:
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movl	$90, %esi
	movq	%rbx, %rdi
	call	X509_get_ext_d2i@PLT
	movq	%rax, 248(%rbx)
	testq	%rax, %rax
	je	.L447
.L373:
	movq	%rbx, %rdi
	call	X509_get_issuer_name@PLT
	movq	%rbx, %rdi
	movq	%rax, %r12
	call	X509_get_subject_name@PLT
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	X509_NAME_cmp@PLT
	testl	%eax, %eax
	je	.L448
.L375:
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movl	$85, %esi
	movq	%rbx, %rdi
	call	X509_get_ext_d2i@PLT
	movq	%rax, 272(%rbx)
	testq	%rax, %rax
	je	.L449
.L377:
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movl	$666, %esi
	movq	%rbx, %rdi
	call	X509_get_ext_d2i@PLT
	movq	%rax, 280(%rbx)
	testq	%rax, %rax
	je	.L450
.L378:
	leaq	-60(%rbp), %r14
	movq	%rbx, %rdi
	xorl	%ecx, %ecx
	movl	$103, %esi
	movq	%r14, %rdx
	call	X509_get_ext_d2i@PLT
	movq	%rax, 264(%rbx)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L451
.L379:
	movl	$0, -60(%rbp)
	jmp	.L381
	.p2align 4,,10
	.p2align 3
.L387:
	addl	$1, -60(%rbp)
	movq	264(%rbx), %rdi
.L381:
	call	OPENSSL_sk_num@PLT
	movl	-60(%rbp), %esi
	cmpl	%eax, %esi
	jge	.L380
	movq	264(%rbx), %rdi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %r12
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L382
	movl	(%rax), %edx
	testl	%edx, %edx
	jle	.L452
	movq	8(%rax), %rcx
	movzbl	(%rcx), %eax
	movl	%eax, 24(%r12)
	cmpl	$1, %edx
	je	.L384
	movzbl	1(%rcx), %edx
	sall	$8, %edx
	orl	%edx, %eax
.L384:
	andl	$32895, %eax
	movl	%eax, 24(%r12)
.L385:
	movq	(%r12), %rax
	testq	%rax, %rax
	je	.L387
	xorl	%r15d, %r15d
	cmpl	$1, (%rax)
	je	.L388
	jmp	.L387
.L357:
	orl	$32, 232(%rbx)
	addl	$1, -64(%rbp)
	jmp	.L453
.L359:
	orl	$16, 232(%rbx)
	addl	$1, -64(%rbp)
	jmp	.L453
.L360:
	orl	$64, 232(%rbx)
	addl	$1, -64(%rbp)
	jmp	.L453
.L361:
	orl	$4, 232(%rbx)
	addl	$1, -64(%rbp)
	jmp	.L453
.L362:
	orl	$8, 232(%rbx)
	addl	$1, -64(%rbp)
	jmp	.L453
.L363:
	orl	$2, 232(%rbx)
	addl	$1, -64(%rbp)
	jmp	.L453
.L364:
	orl	$1, 232(%rbx)
	addl	$1, -64(%rbp)
	jmp	.L453
	.p2align 4,,10
	.p2align 3
.L440:
	movq	8(%rax), %rsi
	testq	%rsi, %rsi
	jne	.L390
	jmp	.L392
	.p2align 4,,10
	.p2align 3
.L382:
	movl	$32895, 24(%r12)
	jmp	.L385
	.p2align 4,,10
	.p2align 3
.L452:
	movl	24(%r12), %eax
	jmp	.L384
	.p2align 4,,10
	.p2align 3
.L365:
	orl	$128, 232(%rbx)
	addl	$1, -64(%rbp)
	jmp	.L453
	.p2align 4,,10
	.p2align 3
.L448:
	orl	$32, 224(%rbx)
	movq	248(%rbx), %rsi
	movq	%rbx, %rdi
	call	X509_check_akid
	testl	%eax, %eax
	jne	.L375
	movabsq	$17179869186, %rax
	andq	224(%rbx), %rax
	cmpq	$2, %rax
	je	.L375
	orl	$8192, 224(%rbx)
	jmp	.L375
	.p2align 4,,10
	.p2align 3
.L346:
	movq	$-1, 216(%rbx)
	jmp	.L347
	.p2align 4,,10
	.p2align 3
.L342:
	cmpl	$-1, -64(%rbp)
	je	.L348
	orl	$128, 224(%rbx)
	jmp	.L348
	.p2align 4,,10
	.p2align 3
.L336:
	cmpl	$-1, -64(%rbp)
	je	.L341
	orl	$128, 224(%rbx)
	jmp	.L341
	.p2align 4,,10
	.p2align 3
.L447:
	cmpl	$-1, -64(%rbp)
	je	.L373
	orl	$128, 224(%rbx)
	jmp	.L373
	.p2align 4,,10
	.p2align 3
.L349:
	cmpl	$-1, -64(%rbp)
	je	.L352
	orl	$128, 224(%rbx)
	jmp	.L352
	.p2align 4,,10
	.p2align 3
.L442:
	cmpl	$-1, -64(%rbp)
	je	.L397
	orl	$128, 224(%rbx)
	jmp	.L397
	.p2align 4,,10
	.p2align 3
.L441:
	cmpl	$-1, -64(%rbp)
	je	.L396
	orl	$128, 224(%rbx)
	jmp	.L396
	.p2align 4,,10
	.p2align 3
.L450:
	cmpl	$-1, -64(%rbp)
	je	.L378
	orl	$128, 224(%rbx)
	jmp	.L378
	.p2align 4,,10
	.p2align 3
.L449:
	cmpl	$-1, -64(%rbp)
	je	.L377
	orl	$128, 224(%rbx)
	jmp	.L377
	.p2align 4,,10
	.p2align 3
.L451:
	cmpl	$-1, -60(%rbp)
	je	.L379
	jmp	.L393
	.p2align 4,,10
	.p2align 3
.L353:
	cmpl	$-1, -64(%rbp)
	je	.L368
	orl	$128, 224(%rbx)
	jmp	.L368
	.p2align 4,,10
	.p2align 3
.L446:
	cmpl	$-1, -64(%rbp)
	je	.L372
	orl	$128, 224(%rbx)
	jmp	.L372
	.p2align 4,,10
	.p2align 3
.L369:
	cmpl	$-1, -64(%rbp)
	je	.L371
	orl	$128, 224(%rbx)
	jmp	.L371
	.p2align 4,,10
	.p2align 3
.L350:
	movl	$0, 228(%rbx)
	jmp	.L351
	.p2align 4,,10
	.p2align 3
.L444:
	movl	$-1, %edx
	movl	$85, %esi
	movq	%rbx, %rdi
	call	X509_get_ext_by_NID@PLT
	testl	%eax, %eax
	js	.L344
.L435:
	movl	224(%rbx), %eax
	jmp	.L343
.L344:
	movl	$-1, %edx
	movl	$86, %esi
	movq	%rbx, %rdi
	call	X509_get_ext_by_NID@PLT
	testl	%eax, %eax
	jns	.L435
	jmp	.L345
.L438:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1318:
	.size	x509v3_cache_extensions, .-x509v3_cache_extensions
	.section	.text.unlikely
.LCOLDB2:
	.text
.LHOTB2:
	.p2align 4
	.globl	X509_check_purpose
	.type	X509_check_purpose, @function
X509_check_purpose:
.LFB1299:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movl	%esi, %ebx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	x509v3_cache_extensions
	testb	$-128, 224(%r12)
	jne	.L462
	cmpl	$-1, %ebx
	je	.L463
	leal	-1(%rbx), %eax
	cmpl	$8, %eax
	jbe	.L465
	movq	xptable(%rip), %rdi
	testq	%rdi, %rdi
	je	.L462
	leaq	-96(%rbp), %rsi
	movl	%ebx, -96(%rbp)
	call	OPENSSL_sk_find@PLT
	movl	%eax, %esi
	testl	%eax, %eax
	js	.L462
	addl	$9, %eax
	js	.L460
	movq	xptable(%rip), %rdi
	call	OPENSSL_sk_value@PLT
.L457:
	movl	%r13d, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	*16(%rax)
.L454:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L466
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L465:
	.cfi_restore_state
	cltq
	leaq	xstandard(%rip), %rdx
	leaq	(%rax,%rax,2), %rax
	salq	$4, %rax
	addq	%rdx, %rax
	jmp	.L457
	.p2align 4,,10
	.p2align 3
.L463:
	movl	$1, %eax
	jmp	.L454
	.p2align 4,,10
	.p2align 3
.L462:
	movl	$-1, %eax
	jmp	.L454
.L466:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	X509_check_purpose.cold, @function
X509_check_purpose.cold:
.LFSB1299:
.L460:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movq	16, %rax
	ud2
	.cfi_endproc
.LFE1299:
	.text
	.size	X509_check_purpose, .-X509_check_purpose
	.section	.text.unlikely
	.size	X509_check_purpose.cold, .-X509_check_purpose.cold
.LCOLDE2:
	.text
.LHOTE2:
	.p2align 4
	.globl	X509_check_ca
	.type	X509_check_ca, @function
X509_check_ca:
.LFB1322:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	x509v3_cache_extensions
	xorl	%eax, %eax
	movabsq	$17179869186, %rdx
	andq	224(%rbx), %rdx
	cmpq	$2, %rdx
	je	.L467
	movl	224(%rbx), %edx
	movl	%edx, %eax
	testb	$1, %dl
	jne	.L478
	andl	$8256, %eax
	cmpl	$8256, %eax
	je	.L471
	movl	$4, %eax
	testb	$2, %dl
	jne	.L467
	xorl	%eax, %eax
	andl	$8, %edx
	je	.L467
	movl	236(%rbx), %eax
	andl	$7, %eax
	cmpl	$1, %eax
	sbbl	%eax, %eax
	notl	%eax
	andl	$5, %eax
.L467:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L478:
	.cfi_restore_state
	addq	$8, %rsp
	shrl	$4, %eax
	andl	$1, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L471:
	.cfi_restore_state
	movl	$3, %eax
	jmp	.L467
	.cfi_endproc
.LFE1322:
	.size	X509_check_ca, .-X509_check_ca
	.p2align 4
	.globl	X509_check_issued
	.type	X509_check_issued, @function
X509_check_issued:
.LFB1334:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$8, %rsp
	call	X509_get_issuer_name@PLT
	movq	%rbx, %rdi
	movq	%rax, %r12
	call	X509_get_subject_name@PLT
	movq	%r12, %rsi
	movl	$29, %r12d
	movq	%rax, %rdi
	call	X509_NAME_cmp@PLT
	testl	%eax, %eax
	jne	.L479
	movq	%rbx, %rdi
	movl	%eax, %r12d
	call	x509v3_cache_extensions
	testb	$-128, 224(%rbx)
	je	.L481
.L482:
	movl	$1, %r12d
.L479:
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L481:
	.cfi_restore_state
	movq	%r13, %rdi
	call	x509v3_cache_extensions
	movl	224(%r13), %eax
	testb	$-128, %al
	jne	.L482
	movq	248(%r13), %rsi
	testq	%rsi, %rsi
	je	.L483
	movq	%rbx, %rdi
	call	X509_check_akid
	testl	%eax, %eax
	jne	.L486
	movl	224(%r13), %eax
.L483:
	movq	224(%rbx), %rdx
	testb	$4, %ah
	je	.L484
	movabsq	$549755813890, %rax
	andq	%rax, %rdx
	movl	$39, %eax
	cmpq	$2, %rdx
	cmove	%eax, %r12d
	jmp	.L479
	.p2align 4,,10
	.p2align 3
.L484:
	movabsq	$17179869186, %rax
	andq	%rax, %rdx
	movl	$32, %eax
	cmpq	$2, %rdx
	cmove	%eax, %r12d
	jmp	.L479
	.p2align 4,,10
	.p2align 3
.L486:
	movl	%eax, %r12d
	jmp	.L479
	.cfi_endproc
.LFE1334:
	.size	X509_check_issued, .-X509_check_issued
	.p2align 4
	.globl	X509_get_extension_flags
	.type	X509_get_extension_flags, @function
X509_get_extension_flags:
.LFB1336:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	x509v3_cache_extensions
	movl	224(%rbx), %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1336:
	.size	X509_get_extension_flags, .-X509_get_extension_flags
	.p2align 4
	.globl	X509_get_key_usage
	.type	X509_get_key_usage, @function
X509_get_key_usage:
.LFB1337:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	x509v3_cache_extensions
	movl	224(%rbx), %eax
	testb	$-128, %al
	jne	.L495
	movl	$-1, %r8d
	testb	$2, %al
	je	.L493
	movl	228(%rbx), %r8d
.L493:
	addq	$8, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L495:
	.cfi_restore_state
	xorl	%r8d, %r8d
	jmp	.L493
	.cfi_endproc
.LFE1337:
	.size	X509_get_key_usage, .-X509_get_key_usage
	.p2align 4
	.globl	X509_get_extended_key_usage
	.type	X509_get_extended_key_usage, @function
X509_get_extended_key_usage:
.LFB1338:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	x509v3_cache_extensions
	movl	224(%rbx), %eax
	testb	$-128, %al
	jne	.L501
	movl	$-1, %r8d
	testb	$4, %al
	je	.L499
	movl	232(%rbx), %r8d
.L499:
	addq	$8, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L501:
	.cfi_restore_state
	xorl	%r8d, %r8d
	jmp	.L499
	.cfi_endproc
.LFE1338:
	.size	X509_get_extended_key_usage, .-X509_get_extended_key_usage
	.p2align 4
	.globl	X509_get0_subject_key_id
	.type	X509_get0_subject_key_id, @function
X509_get0_subject_key_id:
.LFB1339:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	x509v3_cache_extensions
	testb	$-128, 224(%rbx)
	jne	.L507
	movq	240(%rbx), %rax
.L505:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L507:
	.cfi_restore_state
	xorl	%eax, %eax
	jmp	.L505
	.cfi_endproc
.LFE1339:
	.size	X509_get0_subject_key_id, .-X509_get0_subject_key_id
	.p2align 4
	.globl	X509_get0_authority_key_id
	.type	X509_get0_authority_key_id, @function
X509_get0_authority_key_id:
.LFB1340:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	x509v3_cache_extensions
	testb	$-128, 224(%rbx)
	jne	.L511
	movq	248(%rbx), %rax
	testq	%rax, %rax
	je	.L509
	movq	(%rax), %rax
.L509:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L511:
	.cfi_restore_state
	xorl	%eax, %eax
	jmp	.L509
	.cfi_endproc
.LFE1340:
	.size	X509_get0_authority_key_id, .-X509_get0_authority_key_id
	.p2align 4
	.globl	X509_get0_authority_issuer
	.type	X509_get0_authority_issuer, @function
X509_get0_authority_issuer:
.LFB1341:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	x509v3_cache_extensions
	testb	$-128, 224(%rbx)
	jne	.L518
	movq	248(%rbx), %rax
	testq	%rax, %rax
	je	.L516
	movq	8(%rax), %rax
.L516:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L518:
	.cfi_restore_state
	xorl	%eax, %eax
	jmp	.L516
	.cfi_endproc
.LFE1341:
	.size	X509_get0_authority_issuer, .-X509_get0_authority_issuer
	.p2align 4
	.globl	X509_get0_authority_serial
	.type	X509_get0_authority_serial, @function
X509_get0_authority_serial:
.LFB1342:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	x509v3_cache_extensions
	testb	$-128, 224(%rbx)
	jne	.L525
	movq	248(%rbx), %rax
	testq	%rax, %rax
	je	.L523
	movq	16(%rax), %rax
.L523:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L525:
	.cfi_restore_state
	xorl	%eax, %eax
	jmp	.L523
	.cfi_endproc
.LFE1342:
	.size	X509_get0_authority_serial, .-X509_get0_authority_serial
	.p2align 4
	.globl	X509_get_pathlen
	.type	X509_get_pathlen, @function
X509_get_pathlen:
.LFB1343:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	x509v3_cache_extensions
	movl	224(%rbx), %eax
	testb	$-128, %al
	jne	.L533
	testb	$1, %al
	je	.L533
	movq	208(%rbx), %rax
.L530:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L533:
	.cfi_restore_state
	movq	$-1, %rax
	jmp	.L530
	.cfi_endproc
.LFE1343:
	.size	X509_get_pathlen, .-X509_get_pathlen
	.p2align 4
	.globl	X509_get_proxy_pathlen
	.type	X509_get_proxy_pathlen, @function
X509_get_proxy_pathlen:
.LFB1344:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	x509v3_cache_extensions
	movl	224(%rbx), %eax
	testb	$-128, %al
	jne	.L538
	testb	$4, %ah
	je	.L538
	movq	216(%rbx), %rax
.L535:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L538:
	.cfi_restore_state
	movq	$-1, %rax
	jmp	.L535
	.cfi_endproc
.LFE1344:
	.size	X509_get_proxy_pathlen, .-X509_get_proxy_pathlen
	.section	.rodata
	.align 32
	.type	supported_nids.20538, @object
	.size	supported_nids.20538, 56
supported_nids.20538:
	.long	71
	.long	83
	.long	85
	.long	87
	.long	89
	.long	103
	.long	126
	.long	290
	.long	291
	.long	401
	.long	663
	.long	666
	.long	747
	.long	748
	.local	xptable
	.comm	xptable,8,8
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC3:
	.string	"SSL client"
.LC4:
	.string	"sslclient"
.LC5:
	.string	"SSL server"
.LC6:
	.string	"sslserver"
.LC7:
	.string	"Netscape SSL server"
.LC8:
	.string	"nssslserver"
.LC9:
	.string	"S/MIME signing"
.LC10:
	.string	"smimesign"
.LC11:
	.string	"S/MIME encryption"
.LC12:
	.string	"smimeencrypt"
.LC13:
	.string	"CRL signing"
.LC14:
	.string	"crlsign"
.LC15:
	.string	"Any Purpose"
.LC16:
	.string	"any"
.LC17:
	.string	"OCSP helper"
.LC18:
	.string	"ocsphelper"
.LC19:
	.string	"Time Stamp signing"
.LC20:
	.string	"timestampsign"
	.section	.data.rel.local,"aw"
	.align 32
	.type	xstandard, @object
	.size	xstandard, 432
xstandard:
	.long	1
	.long	2
	.long	0
	.zero	4
	.quad	check_purpose_ssl_client
	.quad	.LC3
	.quad	.LC4
	.quad	0
	.long	2
	.long	3
	.long	0
	.zero	4
	.quad	check_purpose_ssl_server
	.quad	.LC5
	.quad	.LC6
	.quad	0
	.long	3
	.long	3
	.long	0
	.zero	4
	.quad	check_purpose_ns_ssl_server
	.quad	.LC7
	.quad	.LC8
	.quad	0
	.long	4
	.long	4
	.long	0
	.zero	4
	.quad	check_purpose_smime_sign
	.quad	.LC9
	.quad	.LC10
	.quad	0
	.long	5
	.long	4
	.long	0
	.zero	4
	.quad	check_purpose_smime_encrypt
	.quad	.LC11
	.quad	.LC12
	.quad	0
	.long	6
	.long	1
	.long	0
	.zero	4
	.quad	check_purpose_crl_sign
	.quad	.LC13
	.quad	.LC14
	.quad	0
	.long	7
	.long	0
	.long	0
	.zero	4
	.quad	no_check
	.quad	.LC15
	.quad	.LC16
	.quad	0
	.long	8
	.long	1
	.long	0
	.zero	4
	.quad	ocsp_helper
	.quad	.LC17
	.quad	.LC18
	.quad	0
	.long	9
	.long	8
	.long	0
	.zero	4
	.quad	check_purpose_timestamp_sign
	.quad	.LC19
	.quad	.LC20
	.quad	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
