	.file	"bn_mul.c"
	.text
	.p2align 4
	.globl	bn_sub_part_words
	.type	bn_sub_part_words, @function
bn_sub_part_words:
.LFB252:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movslq	%ecx, %r12
	pushq	%rbx
	movl	%r12d, %ecx
	.cfi_offset 3, -56
	movl	%r8d, %ebx
	subq	$8, %rsp
	call	bn_sub_words@PLT
	testl	%ebx, %ebx
	je	.L1
	salq	$3, %r12
	movl	$1, %ecx
	leaq	(%r15,%r12), %rdi
	leaq	(%r14,%r12), %rsi
	testl	%ebx, %ebx
	js	.L4
	leaq	0(%r13,%r12), %rsi
	testq	%rax, %rax
	je	.L85
	leal	-1(%rbx), %edx
	leal	-4(%rbx), %r10d
	movl	%ebx, %r9d
	andl	$-4, %edx
	andl	$3, %r9d
	subl	%edx, %r10d
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L16:
	movq	(%rsi), %rcx
	movq	%rcx, %r8
	subq	%rax, %r8
	testq	%rcx, %rcx
	movq	%r8, (%rdi)
	cmovne	%rdx, %rax
	cmpl	$1, %ebx
	je	.L1
	movq	8(%rsi), %rcx
	movq	%rcx, %r8
	subq	%rax, %r8
	testq	%rcx, %rcx
	movq	%r8, 8(%rdi)
	cmovne	%rdx, %rax
	cmpl	$2, %ebx
	je	.L1
	movq	16(%rsi), %rcx
	movq	%rcx, %r8
	subq	%rax, %r8
	testq	%rcx, %rcx
	movq	%r8, 16(%rdi)
	cmovne	%rdx, %rax
	cmpl	%ebx, %r9d
	je	.L1
	movq	24(%rsi), %rcx
	subl	$4, %ebx
	movq	%rcx, %r8
	subq	%rax, %r8
	movq	%r8, 24(%rdi)
	testq	%rcx, %rcx
	je	.L14
	addq	$32, %rsi
	addq	$32, %rdi
	testl	%ebx, %ebx
	jg	.L85
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L80:
	movq	8(%rsi), %rax
	movq	%rax, 8(%rdi)
	cmpl	$2, %ebx
	je	.L17
	movq	16(%rsi), %rax
	movq	%rax, 16(%rdi)
	cmpl	$3, %ebx
	je	.L17
	movq	24(%rsi), %rax
	movq	%rax, 24(%rdi)
	subl	$4, %ebx
	je	.L17
	addq	$32, %rsi
	addq	$32, %rdi
.L85:
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	cmpl	$1, %ebx
	jne	.L80
.L17:
	xorl	%eax, %eax
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L87:
	.cfi_restore_state
	movq	8(%rsi), %r8
	leaq	(%rax,%r8), %rdx
	negq	%rdx
	testq	%r8, %r8
	movq	%rdx, 8(%rdi)
	cmovne	%rcx, %rax
	cmpl	$-2, %ebx
	je	.L1
	movq	16(%rsi), %r8
	leaq	(%rax,%r8), %rdx
	negq	%rdx
	testq	%r8, %r8
	movq	%rdx, 16(%rdi)
	cmovne	%rcx, %rax
	cmpl	$-3, %ebx
	je	.L1
	movq	24(%rsi), %r8
	leaq	(%rax,%r8), %rdx
	negq	%rdx
	testq	%r8, %r8
	movq	%rdx, 24(%rdi)
	cmovne	%rcx, %rax
	addl	$4, %ebx
	je	.L1
	addq	$32, %rsi
	addq	$32, %rdi
.L4:
	movq	(%rsi), %r8
	leaq	(%rax,%r8), %rdx
	negq	%rdx
	testq	%r8, %r8
	movq	%rdx, (%rdi)
	cmovne	%rcx, %rax
	cmpl	$-1, %ebx
	jne	.L87
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L14:
	cmpl	%r10d, %ebx
	je	.L1
	addq	$32, %rsi
	addq	$32, %rdi
	testq	%rax, %rax
	jne	.L16
	jmp	.L85
	.cfi_endproc
.LFE252:
	.size	bn_sub_part_words, .-bn_sub_part_words
	.p2align 4
	.globl	bn_mul_normal
	.type	bn_mul_normal, @function
bn_mul_normal:
.LFB258:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movl	%r8d, -52(%rbp)
	cmpl	%r8d, %edx
	jge	.L89
	movl	%edx, -52(%rbp)
	movl	%r8d, %r15d
	movq	%rcx, %rbx
	movq	%rsi, %r13
.L89:
	movl	-52(%rbp), %eax
	testl	%eax, %eax
	jle	.L107
	movq	0(%r13), %rcx
	movslq	%r15d, %rax
	movl	%r15d, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	leaq	(%r12,%rax,8), %r14
	call	bn_mul_words@PLT
	cmpl	$1, -52(%rbp)
	movq	%rax, (%r14)
	jne	.L92
	jmp	.L88
	.p2align 4,,10
	.p2align 3
.L108:
	movq	16(%r13), %rcx
	leaq	16(%r12), %rdi
	movl	%r15d, %edx
	movq	%rbx, %rsi
	call	bn_mul_add_words@PLT
	cmpl	$3, -52(%rbp)
	movq	%rax, 16(%r14)
	je	.L88
	movq	24(%r13), %rcx
	leaq	24(%r12), %rdi
	movl	%r15d, %edx
	movq	%rbx, %rsi
	call	bn_mul_add_words@PLT
	subl	$4, -52(%rbp)
	movq	%rax, 24(%r14)
	je	.L88
	movq	32(%r13), %rcx
	addq	$32, %r12
	movl	%r15d, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	addq	$32, %r14
	addq	$32, %r13
	call	bn_mul_add_words@PLT
	movq	%rax, (%r14)
	cmpl	$1, -52(%rbp)
	je	.L88
.L92:
	movq	8(%r13), %rcx
	leaq	8(%r12), %rdi
	movl	%r15d, %edx
	movq	%rbx, %rsi
	call	bn_mul_add_words@PLT
	cmpl	$2, -52(%rbp)
	movq	%rax, 8(%r14)
	jne	.L108
.L88:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L107:
	.cfi_restore_state
	addq	$24, %rsp
	movl	%r15d, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	popq	%rbx
	xorl	%ecx, %ecx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	bn_mul_words@PLT
	.cfi_endproc
.LFE258:
	.size	bn_mul_normal, .-bn_mul_normal
	.p2align 4
	.type	bn_mul_recursive.constprop.0, @function
bn_mul_recursive.constprop.0:
.LFB261:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	cmpl	$8, %ecx
	jne	.L110
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	bn_mul_comba8@PLT
	.p2align 4,,10
	.p2align 3
.L110:
	.cfi_restore_state
	movl	%ecx, %r12d
	cmpl	$15, %ecx
	jle	.L159
	movl	%ecx, %ebx
	movq	%rdx, -64(%rbp)
	sarl	%ebx
	movq	%r8, -72(%rbp)
	movslq	%ebx, %rdi
	movl	%ebx, %edx
	leaq	0(,%rdi,8), %rax
	movq	%r14, %rdi
	leaq	(%rsi,%rax), %rcx
	movq	%rax, -56(%rbp)
	movq	%rcx, %rsi
	movq	%rcx, -80(%rbp)
	xorl	%ecx, %ecx
	call	bn_cmp_part_words@PLT
	movq	-64(%rbp), %r11
	xorl	%ecx, %ecx
	movl	%ebx, %edx
	movl	%eax, %r15d
	movq	-56(%rbp), %rax
	movq	%r11, %rsi
	leaq	(%r11,%rax), %rdi
	movq	%rdi, -88(%rbp)
	call	bn_cmp_part_words@PLT
	leal	(%r15,%r15,2), %edx
	movq	-64(%rbp), %r11
	movq	-72(%rbp), %r9
	leal	4(%rdx,%rax), %eax
	cmpl	$8, %eax
	ja	.L112
	leaq	.L114(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L114:
	.long	.L120-.L114
	.long	.L119-.L114
	.long	.L118-.L114
	.long	.L119-.L114
	.long	.L119-.L114
	.long	.L119-.L114
	.long	.L116-.L114
	.long	.L119-.L114
	.long	.L113-.L114
	.text
	.p2align 4,,10
	.p2align 3
.L119:
	cmpl	$8, %ebx
	jne	.L160
	movslq	%r12d, %r15
	leaq	0(,%r15,8), %rax
	leaq	(%r9,%rax), %r15
	movq	%rax, -96(%rbp)
	xorl	%eax, %eax
	leaq	8(%r15), %rdi
	movq	%r15, %rcx
	movq	$0, (%r15)
	andq	$-8, %rdi
	movq	$0, 120(%r15)
	subq	%rdi, %rcx
	subl	$-128, %ecx
	shrl	$3, %ecx
	rep stosq
	movl	$0, -72(%rbp)
	jmp	.L125
	.p2align 4,,10
	.p2align 3
.L159:
	addq	$72, %rsp
	movl	%ecx, %r8d
	movq	%rdx, %rcx
	movl	%r12d, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	bn_mul_normal
	.p2align 4,,10
	.p2align 3
.L120:
	.cfi_restore_state
	movq	-80(%rbp), %rsi
	movl	%ebx, %ecx
	movq	%r14, %rdx
	movq	%r9, %rdi
	movq	%r11, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	bn_sub_words@PLT
	movq	-64(%rbp), %r9
	movq	-56(%rbp), %rax
	movl	%ebx, %ecx
	movq	-72(%rbp), %r11
	movq	-88(%rbp), %rdx
	movq	%r9, -96(%rbp)
	leaq	(%r9,%rax), %r10
	movq	%r11, %rsi
.L157:
	movq	%r10, %rdi
	movq	%r10, -64(%rbp)
	call	bn_sub_words@PLT
	cmpl	$8, %ebx
	movq	-72(%rbp), %r11
	movq	-64(%rbp), %r10
	movl	$0, -72(%rbp)
	movq	-96(%rbp), %r9
	jne	.L122
.L121:
	movslq	%r12d, %r15
	movq	%r9, %rsi
	movq	%r10, %rdx
	movq	%r11, -104(%rbp)
	leaq	0(,%r15,8), %rax
	movq	%r9, -64(%rbp)
	leaq	(%r9,%rax), %r15
	movq	%rax, -96(%rbp)
	movq	%r15, %rdi
	call	bn_mul_comba8@PLT
	movq	-64(%rbp), %r9
	movq	-104(%rbp), %r11
.L125:
	movq	%r11, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%r9, -64(%rbp)
	call	bn_mul_comba8@PLT
	movq	-96(%rbp), %r14
	movq	-88(%rbp), %rdx
	movq	-80(%rbp), %rsi
	addq	%r13, %r14
	movq	%r14, %rdi
	call	bn_mul_comba8@PLT
	movq	-64(%rbp), %r9
.L126:
	movl	%r12d, %ecx
	movq	%r14, %rdx
	movq	%r9, %rdi
	movq	%r13, %rsi
	movq	%r9, -64(%rbp)
	call	bn_add_words@PLT
	movq	-64(%rbp), %r9
	movl	%r12d, %ecx
	movq	%rax, %r14
	movl	-72(%rbp), %eax
	testl	%eax, %eax
	je	.L127
	movq	%r15, %rdx
	movq	%r9, %rsi
	movq	%r15, %rdi
	call	bn_sub_words@PLT
	subl	%eax, %r14d
.L128:
	movq	-56(%rbp), %rdi
	movl	%r12d, %ecx
	movq	%r15, %rdx
	addq	%r13, %rdi
	movq	%rdi, %rsi
	call	bn_add_words@PLT
	addl	%eax, %r14d
	je	.L109
	addl	%ebx, %r12d
	movslq	%r14d, %r14
	movslq	%r12d, %r12
	leaq	0(%r13,%r12,8), %rdx
	addq	(%rdx), %r14
	movq	%r14, (%rdx)
	jc	.L134
.L109:
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L134:
	.cfi_restore_state
	addq	$8, %rdx
	addq	$1, (%rdx)
	jne	.L109
	addq	$8, %rdx
	addq	$1, (%rdx)
	je	.L134
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L118:
	movq	-80(%rbp), %rsi
	movl	%ebx, %ecx
	movq	%r14, %rdx
	movq	%r9, %rdi
	movq	%r11, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	bn_sub_words@PLT
	movq	-64(%rbp), %r9
	movq	-56(%rbp), %rax
	movl	%ebx, %ecx
	movq	-72(%rbp), %r11
	movq	-88(%rbp), %rsi
	movq	%r9, -96(%rbp)
	leaq	(%r9,%rax), %r10
	movq	%r11, %rdx
.L156:
	movq	%r10, %rdi
	movq	%r10, -64(%rbp)
	call	bn_sub_words@PLT
	cmpl	$8, %ebx
	movq	-72(%rbp), %r11
	movq	-64(%rbp), %r10
	movl	$1, -72(%rbp)
	movq	-96(%rbp), %r9
	je	.L121
.L122:
	leal	(%r12,%r12), %eax
	movslq	%r12d, %r15
	movq	%r9, %rsi
	movl	%ebx, %ecx
	cltq
	movq	%r10, %rdx
	movq	%r11, -112(%rbp)
	leaq	(%r9,%rax,8), %r8
	leaq	0(,%r15,8), %rax
	movq	%r9, -64(%rbp)
	leaq	(%r9,%rax), %r15
	movq	%r8, -96(%rbp)
	movq	%r15, %rdi
	movq	%rax, -104(%rbp)
	call	bn_mul_recursive.constprop.0
	movq	-64(%rbp), %r9
	movq	-96(%rbp), %r8
	movq	-112(%rbp), %r11
.L135:
	movq	%r11, %rdx
	movq	%r14, %rsi
	movl	%ebx, %ecx
	movq	%r13, %rdi
	movq	%r9, -96(%rbp)
	movq	%r8, -64(%rbp)
	call	bn_mul_recursive.constprop.0
	movq	-104(%rbp), %r14
	movq	-64(%rbp), %r8
	movl	%ebx, %ecx
	movq	-88(%rbp), %rdx
	movq	-80(%rbp), %rsi
	addq	%r13, %r14
	movq	%r14, %rdi
	call	bn_mul_recursive.constprop.0
	movq	-96(%rbp), %r9
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L116:
	movq	-80(%rbp), %rdx
	movl	%ebx, %ecx
	movq	%r14, %rsi
	movq	%r9, %rdi
	movq	%r11, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	bn_sub_words@PLT
	movq	-64(%rbp), %r9
	movq	-56(%rbp), %rax
	movl	%ebx, %ecx
	movq	-72(%rbp), %r11
	movq	-88(%rbp), %rdx
	movq	%r9, -96(%rbp)
	leaq	(%r9,%rax), %r10
	movq	%r11, %rsi
	jmp	.L156
	.p2align 4,,10
	.p2align 3
.L113:
	movq	-80(%rbp), %rdx
	movl	%ebx, %ecx
	movq	%r14, %rsi
	movq	%r9, %rdi
	movq	%r11, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	bn_sub_words@PLT
	movq	-64(%rbp), %r9
	movq	-56(%rbp), %rax
	movl	%ebx, %ecx
	movq	-72(%rbp), %r11
	movq	-88(%rbp), %rsi
	movq	%r9, -96(%rbp)
	leaq	(%r9,%rax), %r10
	movq	%r11, %rdx
	jmp	.L157
	.p2align 4,,10
	.p2align 3
.L160:
	movslq	%r12d, %r15
	leal	(%r12,%r12), %eax
	xorl	%esi, %esi
	movq	%r11, -112(%rbp)
	leaq	0(,%r15,8), %rdx
	cltq
	movq	%r9, -64(%rbp)
	leaq	(%r9,%rdx), %r15
	leaq	(%r9,%rax,8), %r8
	movq	%rdx, -104(%rbp)
	movq	%r15, %rdi
	movq	%r8, -96(%rbp)
	call	memset@PLT
	movl	$0, -72(%rbp)
	movq	-64(%rbp), %r9
	movq	-96(%rbp), %r8
	movq	-112(%rbp), %r11
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L127:
	movq	%r9, %rdx
	movq	%r15, %rsi
	movq	%r15, %rdi
	call	bn_add_words@PLT
	addl	%eax, %r14d
	jmp	.L128
.L112:
	movq	-56(%rbp), %rax
	movl	$0, -72(%rbp)
	leaq	(%r9,%rax), %r10
	cmpl	$8, %ebx
	je	.L121
	jmp	.L122
	.cfi_endproc
.LFE261:
	.size	bn_mul_recursive.constprop.0, .-bn_mul_recursive.constprop.0
	.p2align 4
	.globl	bn_mul_recursive
	.type	bn_mul_recursive, @function
bn_mul_recursive:
.LFB253:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r10
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%r9d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%ecx, %ebx
	subq	$104, %rsp
	testl	%r8d, %r8d
	movl	%r8d, -60(%rbp)
	movq	16(%rbp), %r15
	sete	-56(%rbp)
	movzbl	-56(%rbp), %eax
	cmpl	$8, %ecx
	jne	.L162
	testb	%al, %al
	je	.L162
	testl	%r9d, %r9d
	je	.L216
.L163:
	movl	-60(%rbp), %eax
	movq	%r12, %rcx
	leal	(%rbx,%r13), %r8d
	movq	%r10, %rsi
	movq	%r14, %rdi
	leal	(%rbx,%rax), %r15d
	movl	%r15d, %edx
	call	bn_mul_normal
	movl	-60(%rbp), %r12d
	addl	%r13d, %r12d
	js	.L217
.L161:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L162:
	.cfi_restore_state
	cmpl	$15, %ebx
	jle	.L163
	movl	%ebx, %eax
	movl	-60(%rbp), %ecx
	movq	%r10, -128(%rbp)
	sarl	%eax
	movl	%eax, %r11d
	movslq	%eax, %rdi
	leal	(%rax,%r13), %esi
	movl	%eax, -64(%rbp)
	addl	%ecx, %r11d
	leaq	0(,%rdi,8), %rax
	negl	%ecx
	movq	%r10, %rdi
	movl	%r11d, %edx
	movl	%esi, -84(%rbp)
	leaq	(%r10,%rax), %rsi
	movq	%rsi, -96(%rbp)
	movl	%ecx, -132(%rbp)
	movl	%r11d, -88(%rbp)
	movq	%rax, -80(%rbp)
	call	bn_cmp_part_words@PLT
	movl	-84(%rbp), %edx
	movq	%r12, %rsi
	movl	%r13d, %ecx
	movl	%eax, -120(%rbp)
	movq	-80(%rbp), %rax
	addq	%r12, %rax
	movq	%rax, %rdi
	movq	%rax, -104(%rbp)
	call	bn_cmp_part_words@PLT
	movl	-120(%rbp), %r9d
	movslq	%ebx, %rdx
	movq	-128(%rbp), %r10
	leaq	0(,%rdx,8), %rdi
	leal	(%r9,%r9,2), %edx
	leaq	(%r15,%rdi), %rsi
	movq	%rdi, -112(%rbp)
	leal	4(%rdx,%rax), %eax
	movq	%rsi, -72(%rbp)
	cmpl	$8, %eax
	ja	.L167
	leaq	.L169(%rip), %rdx
	movl	-88(%rbp), %r11d
	movl	-132(%rbp), %r8d
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L169:
	.long	.L173-.L169
	.long	.L170-.L169
	.long	.L172-.L169
	.long	.L170-.L169
	.long	.L170-.L169
	.long	.L170-.L169
	.long	.L171-.L169
	.long	.L170-.L169
	.long	.L168-.L169
	.text
	.p2align 4,,10
	.p2align 3
.L170:
	cmpl	$8, -64(%rbp)
	jne	.L176
	cmpb	$0, -56(%rbp)
	je	.L176
	testl	%r13d, %r13d
	je	.L175
.L176:
	movq	-72(%rbp), %rdi
	movq	-112(%rbp), %rdx
	xorl	%esi, %esi
	movq	%r10, -120(%rbp)
	movq	%rdi, %r8
	addq	%rdx, %r8
	movq	%r8, -56(%rbp)
	call	memset@PLT
	movl	$0, -84(%rbp)
	movq	-56(%rbp), %r8
	movq	-120(%rbp), %r10
.L181:
	movl	-64(%rbp), %ecx
	movq	%r12, %rdx
	movq	%r10, %rsi
	movq	%r14, %rdi
	movq	%r8, -56(%rbp)
	call	bn_mul_recursive.constprop.0
	movq	-56(%rbp), %r8
	subq	$8, %rsp
	movl	-64(%rbp), %ecx
	movq	-112(%rbp), %r12
	movq	-104(%rbp), %rdx
	movl	%r13d, %r9d
	pushq	%r8
	movq	-96(%rbp), %rsi
	movl	-60(%rbp), %r8d
	addq	%r14, %r12
	movq	%r12, %rdi
	call	bn_mul_recursive
	popq	%rdx
	popq	%rcx
	jmp	.L180
	.p2align 4,,10
	.p2align 3
.L216:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	bn_mul_comba8@PLT
	.p2align 4,,10
	.p2align 3
.L171:
	.cfi_restore_state
	movq	-96(%rbp), %rdx
	movl	%r11d, %ecx
	movq	%r10, %rsi
	movq	%r15, %rdi
	movq	%r10, -128(%rbp)
	call	bn_sub_part_words
	movq	-80(%rbp), %rax
	movl	%r13d, %r8d
	movl	-84(%rbp), %ecx
	movq	-104(%rbp), %rdx
	negl	%r8d
	movq	%r12, %rsi
	leaq	(%r15,%rax), %r9
.L214:
	movq	%r9, %rdi
	movq	%r9, -120(%rbp)
	call	bn_sub_part_words
	movl	$1, -84(%rbp)
	movq	-120(%rbp), %r9
	movq	-128(%rbp), %r10
.L174:
	cmpl	$8, -64(%rbp)
	jne	.L190
	cmpb	$0, -56(%rbp)
	je	.L190
	testl	%r13d, %r13d
	jne	.L190
	movq	-72(%rbp), %rdi
	movq	%r9, %rdx
	movq	%r15, %rsi
	movq	%r10, -56(%rbp)
	call	bn_mul_comba8@PLT
	movq	-56(%rbp), %r10
.L179:
	movq	%r12, %rdx
	movq	%r10, %rsi
	movq	%r14, %rdi
	call	bn_mul_comba8@PLT
	movq	-112(%rbp), %r12
	movq	-104(%rbp), %rdx
	movq	-96(%rbp), %rsi
	addq	%r14, %r12
	movq	%r12, %rdi
	call	bn_mul_comba8@PLT
.L180:
	movl	%ebx, %ecx
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	bn_add_words@PLT
	movl	%ebx, %ecx
	movq	%rax, %r12
	movl	-84(%rbp), %eax
	testl	%eax, %eax
	je	.L182
	movq	-72(%rbp), %rdi
	movq	%r15, %rsi
	movq	%rdi, %rdx
	call	bn_sub_words@PLT
	subl	%eax, %r12d
.L183:
	movq	-80(%rbp), %rdi
	movq	-72(%rbp), %rdx
	movl	%ebx, %ecx
	addq	%r14, %rdi
	movq	%rdi, %rsi
	call	bn_add_words@PLT
	addl	%eax, %r12d
	je	.L161
	addl	-64(%rbp), %ebx
	movslq	%r12d, %r12
	movslq	%ebx, %rbx
	leaq	(%r14,%rbx,8), %rdx
	addq	(%rdx), %r12
	movq	%r12, (%rdx)
	jnc	.L161
	.p2align 4,,10
	.p2align 3
.L187:
	addq	$8, %rdx
	addq	$1, (%rdx)
	je	.L187
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L173:
	.cfi_restore_state
	movl	-60(%rbp), %r8d
	movq	-96(%rbp), %rsi
	movl	%r11d, %ecx
	movq	%r10, %rdx
	movq	%r15, %rdi
	movq	%r10, -128(%rbp)
	call	bn_sub_part_words
	movq	-80(%rbp), %rax
	movl	%r13d, %r8d
	movl	-84(%rbp), %ecx
	movq	-104(%rbp), %rdx
	negl	%r8d
	movq	%r12, %rsi
	leaq	(%r15,%rax), %r9
.L215:
	movq	%r9, %rdi
	movq	%r9, -120(%rbp)
	call	bn_sub_part_words
	movl	$0, -84(%rbp)
	movq	-120(%rbp), %r9
	movq	-128(%rbp), %r10
	jmp	.L174
	.p2align 4,,10
	.p2align 3
.L172:
	movl	-60(%rbp), %r8d
	movq	-96(%rbp), %rsi
	movl	%r11d, %ecx
	movq	%r10, %rdx
	movq	%r15, %rdi
	movq	%r10, -128(%rbp)
	call	bn_sub_part_words
	movq	-80(%rbp), %rax
	movl	-84(%rbp), %ecx
	movl	%r13d, %r8d
	movq	-104(%rbp), %rsi
	movq	%r12, %rdx
	leaq	(%r15,%rax), %r9
	jmp	.L214
	.p2align 4,,10
	.p2align 3
.L168:
	movq	-96(%rbp), %rdx
	movl	%r11d, %ecx
	movq	%r10, %rsi
	movq	%r15, %rdi
	movq	%r10, -128(%rbp)
	call	bn_sub_part_words
	movq	-80(%rbp), %rax
	movl	-84(%rbp), %ecx
	movl	%r13d, %r8d
	movq	-104(%rbp), %rsi
	movq	%r12, %rdx
	leaq	(%r15,%rax), %r9
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L175:
	leaq	8(%rsi), %rdi
	movq	%rsi, %rax
	movq	$0, (%rsi)
	andq	$-8, %rdi
	movq	$0, 120(%rsi)
	subq	%rdi, %rax
	movq	%rax, %rcx
	xorl	%eax, %eax
	subl	$-128, %ecx
	shrl	$3, %ecx
	rep stosq
	movl	$0, -84(%rbp)
	jmp	.L179
	.p2align 4,,10
	.p2align 3
.L182:
	movq	-72(%rbp), %rsi
	movq	%r15, %rdx
	movq	%rsi, %rdi
	call	bn_add_words@PLT
	addl	%eax, %r12d
	jmp	.L183
	.p2align 4,,10
	.p2align 3
.L190:
	movl	-64(%rbp), %ecx
	movq	-72(%rbp), %rdi
	movq	%r9, %rdx
	movq	%r15, %rsi
	movq	-72(%rbp), %r8
	addq	-112(%rbp), %r8
	movq	%r10, -120(%rbp)
	movq	%r8, -56(%rbp)
	call	bn_mul_recursive.constprop.0
	movq	-120(%rbp), %r10
	movq	-56(%rbp), %r8
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L217:
	addl	%r15d, %ebx
	negl	%r12d
	leaq	-40(%rbp), %rsp
	xorl	%esi, %esi
	addl	%ebx, %r13d
	movslq	%r12d, %rdx
	popq	%rbx
	popq	%r12
	movslq	%r13d, %r13
	salq	$3, %rdx
	leaq	(%r14,%r13,8), %rdi
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	memset@PLT
.L167:
	.cfi_restore_state
	movq	-80(%rbp), %rax
	movl	$0, -84(%rbp)
	leaq	(%r15,%rax), %r9
	jmp	.L174
	.cfi_endproc
.LFE253:
	.size	bn_mul_recursive, .-bn_mul_recursive
	.p2align 4
	.globl	bn_mul_part_recursive
	.type	bn_mul_part_recursive, @function
bn_mul_part_recursive:
.LFB254:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r11
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%r9d, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%ecx, %ebx
	subq	$104, %rsp
	movq	16(%rbp), %rax
	movq	%rdi, -56(%rbp)
	movq	%rax, -64(%rbp)
	cmpl	$7, %ecx
	jle	.L265
	movslq	%ecx, %rax
	movl	%ecx, %r15d
	movl	%r8d, %edx
	movq	%r11, %rdi
	subl	%r8d, %r15d
	movq	%rax, -120(%rbp)
	salq	$3, %rax
	movl	%r12d, %r14d
	leaq	(%rsi,%rax), %rsi
	movl	%r15d, %ecx
	movl	%r8d, -112(%rbp)
	subl	%ebx, %r14d
	movq	%r11, -88(%rbp)
	movq	%rsi, -96(%rbp)
	movq	%rax, -72(%rbp)
	call	bn_cmp_part_words@PLT
	movl	%r14d, %ecx
	movl	%r12d, %edx
	movq	%r13, %rsi
	movl	%eax, -80(%rbp)
	movq	-72(%rbp), %rax
	leaq	0(%r13,%rax), %rdi
	movq	%rdi, -104(%rbp)
	call	bn_cmp_part_words@PLT
	movl	-80(%rbp), %r8d
	movq	-88(%rbp), %r11
	movl	%eax, %r9d
	movl	-112(%rbp), %r10d
	leal	(%r8,%r8,2), %eax
	addl	%r9d, %eax
	movq	-64(%rbp), %r9
	addq	-72(%rbp), %r9
	cmpl	$2, %eax
	jg	.L220
	cmpl	$-1, %eax
	jge	.L221
	cmpl	$-4, %eax
	je	.L222
	movl	$0, -80(%rbp)
	jl	.L223
	addl	$3, %eax
	cmpl	$1, %eax
	ja	.L223
	movq	-96(%rbp), %rsi
	movl	%r10d, %r8d
	movq	-64(%rbp), %rdi
	movl	%r10d, %ecx
	subl	%ebx, %r8d
	movq	%r11, %rdx
	movq	%r9, -80(%rbp)
	movl	%r10d, -128(%rbp)
	movq	%r11, -112(%rbp)
	call	bn_sub_part_words
	movq	-104(%rbp), %rsi
	movl	%r14d, %r8d
	movl	%r12d, %ecx
	movq	%r13, %rdx
.L264:
	movq	-80(%rbp), %r9
	movq	%r9, %rdi
	movq	%r9, -88(%rbp)
	call	bn_sub_part_words
	movl	$1, -80(%rbp)
	movq	-88(%rbp), %r9
	movq	-112(%rbp), %r11
	movl	-128(%rbp), %r10d
.L223:
	leal	(%rbx,%rbx), %r14d
	movq	-72(%rbp), %rsi
	movq	-56(%rbp), %rcx
	movslq	%r14d, %rax
	salq	$3, %rax
	leaq	(%r9,%rsi), %r15
	leaq	(%rcx,%rax), %rsi
	movq	%rsi, -88(%rbp)
	cmpl	$8, %ebx
	je	.L266
	movq	-120(%rbp), %r8
	movq	-64(%rbp), %rsi
	movq	%r9, %rdx
	movl	%ebx, %ecx
	movq	%r15, %rdi
	movl	%r10d, -140(%rbp)
	salq	$4, %r8
	movq	%rax, -128(%rbp)
	addq	%r15, %r8
	movq	%r11, -136(%rbp)
	movq	%r8, -112(%rbp)
	call	bn_mul_recursive.constprop.0
	movq	-112(%rbp), %r8
	movq	-56(%rbp), %rdi
	movq	%r13, %rdx
	movq	-136(%rbp), %r11
	movl	%ebx, %ecx
	movl	%ebx, %r13d
	sarl	%r13d
	movq	%r11, %rsi
	call	bn_mul_recursive.constprop.0
	movl	%r12d, %r9d
	movq	-112(%rbp), %r8
	movq	-128(%rbp), %rax
	movl	-140(%rbp), %r10d
	subl	%r13d, %r9d
	movl	%r10d, %edx
	subl	%r13d, %edx
	cmpl	%r12d, %r10d
	movl	%edx, %ecx
	cmovle	%r9d, %ecx
	testl	%ecx, %ecx
	je	.L267
	jle	.L231
	subq	$8, %rsp
	movq	-96(%rbp), %rsi
	movq	-88(%rbp), %rdi
	movl	%r13d, %ecx
	pushq	%r8
	movl	%edx, %r8d
	movq	-104(%rbp), %rdx
	movl	%r10d, -112(%rbp)
	call	bn_mul_part_recursive
	movl	-112(%rbp), %r10d
	movl	%r14d, %edx
	movq	-56(%rbp), %rax
	xorl	%esi, %esi
	subl	%r10d, %edx
	addl	%r14d, %r10d
	subl	%r12d, %edx
	addl	%r10d, %r12d
	movslq	%edx, %rdx
	movslq	%r12d, %r12
	salq	$3, %rdx
	leaq	(%rax,%r12,8), %rdi
	call	memset@PLT
	popq	%r8
	popq	%r9
	jmp	.L227
	.p2align 4,,10
	.p2align 3
.L220:
	subl	$3, %eax
	movl	$0, -80(%rbp)
	cmpl	$1, %eax
	ja	.L223
	movq	-96(%rbp), %rdx
	movq	-64(%rbp), %rdi
	movl	%r10d, %ecx
	movq	%r11, %rsi
	movl	%r15d, %r8d
	movl	%r10d, -128(%rbp)
	movq	%r11, -112(%rbp)
	movq	%r9, -88(%rbp)
	call	bn_sub_part_words
	movq	-88(%rbp), %r9
	movq	-104(%rbp), %rsi
	movl	%r14d, %r8d
	movl	%r12d, %ecx
	movq	%r13, %rdx
	movq	%r9, %rdi
	call	bn_sub_part_words
	movl	-128(%rbp), %r10d
	movq	-112(%rbp), %r11
	movq	-88(%rbp), %r9
	jmp	.L223
	.p2align 4,,10
	.p2align 3
.L266:
	movq	-64(%rbp), %rsi
	movq	%r9, %rdx
	movl	%r10d, -120(%rbp)
	movq	%r11, -112(%rbp)
	leaq	128(%rsi), %rdi
	call	bn_mul_comba8@PLT
	movq	-112(%rbp), %r11
	movq	%r13, %rdx
	movq	-56(%rbp), %r13
	movq	%r11, %rsi
	movq	%r13, %rdi
	call	bn_mul_comba8@PLT
	movl	-120(%rbp), %r10d
	movq	-104(%rbp), %rcx
	movl	%r12d, %r8d
	movq	-96(%rbp), %rsi
	leaq	128(%r13), %rdi
	movl	%r10d, %edx
	movl	%r10d, -104(%rbp)
	call	bn_mul_normal
	movl	-104(%rbp), %r10d
	movl	$16, %edx
	xorl	%esi, %esi
	subl	%r10d, %edx
	leal	16(%r10,%r12), %eax
	subl	%r12d, %edx
	cltq
	movslq	%edx, %rdx
	leaq	0(%r13,%rax,8), %rdi
	salq	$3, %rdx
	call	memset@PLT
.L227:
	movq	-64(%rbp), %r13
	movq	-88(%rbp), %rdx
	movl	%r14d, %ecx
	movq	-56(%rbp), %rsi
	movq	%r13, %rdi
	call	bn_add_words@PLT
	movl	%r14d, %ecx
	movq	%rax, %r12
	movl	-80(%rbp), %eax
	testl	%eax, %eax
	je	.L236
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	bn_sub_words@PLT
	subl	%eax, %r12d
.L237:
	movq	-72(%rbp), %rdi
	movq	-56(%rbp), %r13
	movl	%r14d, %ecx
	movq	%r15, %rdx
	addq	%r13, %rdi
	movq	%rdi, %rsi
	call	bn_add_words@PLT
	addl	%eax, %r12d
	je	.L218
	addl	%r14d, %ebx
	movslq	%r12d, %r12
	movslq	%ebx, %rbx
	leaq	0(%r13,%rbx,8), %rdx
	addq	(%rdx), %r12
	movq	%r12, (%rdx)
	jnc	.L218
	.p2align 4,,10
	.p2align 3
.L243:
	addq	$8, %rdx
	addq	$1, (%rdx)
	je	.L243
.L218:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L265:
	.cfi_restore_state
	leaq	-40(%rbp), %rsp
	leal	(%rcx,%r8), %edx
	popq	%rbx
	leal	(%rcx,%r9), %r8d
	popq	%r12
	movq	%r13, %rcx
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	bn_mul_normal
	.p2align 4,,10
	.p2align 3
.L236:
	.cfi_restore_state
	movq	-64(%rbp), %rdx
	movq	%r15, %rsi
	movq	%r15, %rdi
	call	bn_add_words@PLT
	addl	%eax, %r12d
	jmp	.L237
	.p2align 4,,10
	.p2align 3
.L222:
	movl	%r10d, %r8d
	movq	-96(%rbp), %rsi
	movq	-64(%rbp), %rdi
	movl	%r10d, %ecx
	movq	%r11, %rdx
	subl	%ebx, %r8d
	movl	%r10d, -128(%rbp)
	movq	%r11, -112(%rbp)
	movq	%r9, -80(%rbp)
	call	bn_sub_part_words
	movq	-80(%rbp), %r9
	movq	-104(%rbp), %rdx
	movl	%ebx, %r8d
	subl	%r12d, %r8d
	movl	%r12d, %ecx
	movq	%r13, %rsi
	movq	%r9, %rdi
	movq	%r9, -88(%rbp)
	call	bn_sub_part_words
	movl	$0, -80(%rbp)
	movq	-88(%rbp), %r9
	movq	-112(%rbp), %r11
	movl	-128(%rbp), %r10d
	jmp	.L223
	.p2align 4,,10
	.p2align 3
.L231:
	movq	-88(%rbp), %rdi
	xorl	%esi, %esi
	movq	%rax, %rdx
	movl	%r10d, -120(%rbp)
	movq	%r8, -112(%rbp)
	call	memset@PLT
	movl	-120(%rbp), %r10d
	movq	-112(%rbp), %r8
	cmpl	$15, %r10d
	jg	.L232
	cmpl	$15, %r12d
	jle	.L268
.L232:
	cmpl	%r12d, %r10d
	movl	%r12d, %eax
	cmovge	%r10d, %eax
	jmp	.L234
	.p2align 4,,10
	.p2align 3
.L233:
	cmpl	%ecx, %r10d
	je	.L247
	cmpl	%ecx, %r12d
	je	.L247
.L234:
	movl	%r13d, %ecx
	shrl	$31, %ecx
	addl	%r13d, %ecx
	sarl	%ecx
	movl	%ecx, %r13d
	cmpl	%eax, %ecx
	jge	.L233
	subq	$8, %rsp
	movq	-96(%rbp), %rsi
	movq	-88(%rbp), %rdi
	subl	%ecx, %r10d
	pushq	%r8
	movq	-104(%rbp), %rdx
	movl	%r12d, %r9d
	movl	%r10d, %r8d
	subl	%ecx, %r9d
	call	bn_mul_part_recursive
	popq	%rsi
	popq	%rdi
	jmp	.L227
	.p2align 4,,10
	.p2align 3
.L267:
	subq	$8, %rsp
	movq	-96(%rbp), %rsi
	movq	-88(%rbp), %rdi
	movl	%r13d, %ecx
	pushq	%r8
	movl	%edx, %r8d
	movq	-104(%rbp), %rdx
	addl	%r13d, %r13d
	call	bn_mul_recursive
	movl	%r14d, %edx
	movq	-56(%rbp), %rax
	xorl	%esi, %esi
	subl	%r13d, %edx
	addl	%r14d, %r13d
	movslq	%edx, %rdx
	movslq	%r13d, %r13
	salq	$3, %rdx
	leaq	(%rax,%r13,8), %rdi
	call	memset@PLT
	popq	%r10
	popq	%r11
	jmp	.L227
	.p2align 4,,10
	.p2align 3
.L221:
	movq	-96(%rbp), %rdx
	movq	-64(%rbp), %rdi
	movl	%r15d, %r8d
	movl	%r10d, %ecx
	movq	%r11, %rsi
	movq	%r9, -80(%rbp)
	movl	%r10d, -128(%rbp)
	movq	%r11, -112(%rbp)
	call	bn_sub_part_words
	movl	%ebx, %r8d
	movq	-104(%rbp), %rdx
	movl	%r12d, %ecx
	subl	%r12d, %r8d
	movq	%r13, %rsi
	jmp	.L264
	.p2align 4,,10
	.p2align 3
.L268:
	movq	-104(%rbp), %rcx
	movq	-96(%rbp), %rsi
	movl	%r12d, %r8d
	movl	%r10d, %edx
	movq	-88(%rbp), %rdi
	call	bn_mul_normal
	jmp	.L227
	.p2align 4,,10
	.p2align 3
.L247:
	subq	$8, %rsp
	subl	%ecx, %r10d
	movq	-104(%rbp), %rdx
	movq	-96(%rbp), %rsi
	pushq	%r8
	movq	-88(%rbp), %rdi
	movl	%r12d, %r9d
	movl	%r10d, %r8d
	subl	%ecx, %r9d
	call	bn_mul_recursive
	popq	%rdx
	popq	%rcx
	jmp	.L227
	.cfi_endproc
.LFE254:
	.size	bn_mul_part_recursive, .-bn_mul_part_recursive
	.p2align 4
	.globl	bn_mul_fixed_top
	.type	bn_mul_fixed_top, @function
bn_mul_fixed_top:
.LFB257:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movl	8(%rsi), %r13d
	movl	8(%rdx), %r15d
	movq	%rdx, -56(%rbp)
	testl	%r13d, %r13d
	je	.L286
	testl	%r15d, %r15d
	je	.L286
	movq	%rsi, %rbx
	leal	0(%r13,%r15), %eax
	movq	%rcx, %rdi
	movq	%rcx, %r14
	movl	%eax, -64(%rbp)
	call	BN_CTX_start@PLT
	cmpq	%r12, %rbx
	je	.L287
	movq	%r12, %r10
	cmpq	%r12, -56(%rbp)
	je	.L287
.L273:
	movl	%r13d, %eax
	subl	%r15d, %eax
	jne	.L276
	cmpl	$8, %r13d
	je	.L323
.L276:
	cmpl	$15, %r13d
	jle	.L279
	cmpl	$15, %r15d
	jle	.L279
	leal	1(%rax), %ecx
	cmpl	$2, %ecx
	jbe	.L324
.L279:
	leal	0(%r13,%r15), %esi
	movq	%r10, %rdi
	movq	%r10, -64(%rbp)
	call	bn_wexpand@PLT
	testq	%rax, %rax
	je	.L277
	movq	-64(%rbp), %r10
	leal	0(%r13,%r15), %eax
	movq	(%rbx), %rsi
	movl	%r15d, %r8d
	movl	%r13d, %edx
	movl	%eax, 8(%r10)
	movq	-56(%rbp), %rax
	movq	(%r10), %rdi
	movq	(%rax), %rcx
	call	bn_mul_normal
	movq	-64(%rbp), %r10
.L278:
	movl	16(%rbx), %eax
	movq	-56(%rbp), %rbx
	movl	$1, %r13d
	xorl	16(%rbx), %eax
	movl	%eax, 16(%r10)
	cmpq	%r12, %r10
	je	.L275
	movq	%r10, %rsi
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	call	BN_copy@PLT
	testq	%rax, %rax
	setne	%r13b
.L275:
	movq	%r14, %rdi
	call	BN_CTX_end@PLT
.L269:
	leaq	-40(%rbp), %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L287:
	.cfi_restore_state
	movq	%r14, %rdi
	call	BN_CTX_get@PLT
	movq	%rax, %r10
	testq	%rax, %rax
	jne	.L273
	.p2align 4,,10
	.p2align 3
.L277:
	xorl	%r13d, %r13d
	jmp	.L275
	.p2align 4,,10
	.p2align 3
.L286:
	xorl	%esi, %esi
	movq	%r12, %rdi
	movl	$1, %r13d
	call	BN_set_word@PLT
	jmp	.L269
	.p2align 4,,10
	.p2align 3
.L324:
	movq	%r10, -72(%rbp)
	cmpl	$-1, %eax
	jne	.L325
	movslq	%r15d, %rdi
	call	BN_num_bits_word@PLT
	movq	-72(%rbp), %r10
.L283:
	leal	-1(%rax), %ecx
	movl	$2, %esi
	movl	$1, %eax
	movq	%r14, %rdi
	sall	%cl, %eax
	sall	%cl, %esi
	movq	%r10, -80(%rbp)
	movl	%eax, -72(%rbp)
	movl	%esi, -88(%rbp)
	call	BN_CTX_get@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L277
	cmpl	%r15d, %r13d
	movq	-80(%rbp), %r10
	movl	%r15d, %edx
	cmovge	%r13d, %edx
	cmpl	-72(%rbp), %edx
	movq	%r10, -96(%rbp)
	jle	.L281
	movl	-88(%rbp), %edi
	movq	%rax, -88(%rbp)
	sall	$2, %edi
	movl	%edi, -80(%rbp)
	movl	%edi, %esi
	movq	%rax, %rdi
	call	bn_wexpand@PLT
	testq	%rax, %rax
	je	.L277
	movq	-96(%rbp), %r10
	movl	-80(%rbp), %esi
	movq	%r10, %rdi
	movq	%r10, -80(%rbp)
	call	bn_wexpand@PLT
	testq	%rax, %rax
	je	.L277
	movl	-72(%rbp), %eax
	movq	-88(%rbp), %rcx
	movl	%r15d, %r9d
	movl	%r13d, %r8d
	movq	-80(%rbp), %r10
	subq	$8, %rsp
	subl	%eax, %r9d
	pushq	(%rcx)
	subl	%eax, %r8d
	movl	%eax, %ecx
	movq	-56(%rbp), %rax
	movq	%r10, -72(%rbp)
	movq	(%rbx), %rsi
	movq	(%r10), %rdi
	movq	(%rax), %rdx
	call	bn_mul_part_recursive
	popq	%rcx
	movq	-72(%rbp), %r10
	popq	%rsi
	jmp	.L282
	.p2align 4,,10
	.p2align 3
.L323:
	movl	$16, %esi
	movq	%r10, %rdi
	movq	%r10, -64(%rbp)
	call	bn_wexpand@PLT
	testq	%rax, %rax
	je	.L277
	movq	-64(%rbp), %r10
	movq	-56(%rbp), %rax
	movq	(%rbx), %rsi
	movq	(%r10), %rdi
	movq	(%rax), %rdx
	movl	$16, 8(%r10)
	call	bn_mul_comba8@PLT
	movq	-64(%rbp), %r10
	jmp	.L278
	.p2align 4,,10
	.p2align 3
.L325:
	movslq	%r13d, %rdi
	call	BN_num_bits_word@PLT
	movq	-72(%rbp), %r10
	jmp	.L283
	.p2align 4,,10
	.p2align 3
.L281:
	movl	-88(%rbp), %esi
	movq	%rcx, %rdi
	movq	%rcx, -88(%rbp)
	addl	%esi, %esi
	movl	%esi, -80(%rbp)
	call	bn_wexpand@PLT
	testq	%rax, %rax
	je	.L277
	movq	-96(%rbp), %r10
	movl	-80(%rbp), %esi
	movq	%r10, %rdi
	movq	%r10, -80(%rbp)
	call	bn_wexpand@PLT
	testq	%rax, %rax
	je	.L277
	movl	-72(%rbp), %eax
	movq	-88(%rbp), %rcx
	movl	%r15d, %r9d
	movl	%r13d, %r8d
	movq	-80(%rbp), %r10
	subq	$8, %rsp
	subl	%eax, %r9d
	pushq	(%rcx)
	subl	%eax, %r8d
	movl	%eax, %ecx
	movq	-56(%rbp), %rax
	movq	%r10, -72(%rbp)
	movq	(%r10), %rdi
	movq	(%rbx), %rsi
	movq	(%rax), %rdx
	call	bn_mul_recursive
	movq	-72(%rbp), %r10
	popq	%rax
	popq	%rdx
.L282:
	movl	-64(%rbp), %eax
	movl	%eax, 8(%r10)
	jmp	.L278
	.cfi_endproc
.LFE257:
	.size	bn_mul_fixed_top, .-bn_mul_fixed_top
	.p2align 4
	.globl	BN_mul
	.type	BN_mul, @function
BN_mul:
.LFB256:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movl	8(%rsi), %r13d
	movl	8(%rdx), %r15d
	movq	%rdx, -56(%rbp)
	testl	%r13d, %r13d
	je	.L343
	testl	%r15d, %r15d
	je	.L343
	movq	%rsi, %rbx
	leal	0(%r13,%r15), %eax
	movq	%rcx, %rdi
	movq	%rcx, %r14
	movl	%eax, -64(%rbp)
	call	BN_CTX_start@PLT
	cmpq	%rbx, %r12
	je	.L344
	movq	%r12, %r10
	cmpq	-56(%rbp), %r12
	je	.L344
.L330:
	movl	%r13d, %eax
	subl	%r15d, %eax
	jne	.L333
	cmpl	$8, %r13d
	je	.L381
.L333:
	cmpl	$15, %r13d
	jle	.L336
	cmpl	$15, %r15d
	jle	.L336
	leal	1(%rax), %ecx
	cmpl	$2, %ecx
	jbe	.L382
.L336:
	leal	0(%r13,%r15), %esi
	movq	%r10, %rdi
	movq	%r10, -64(%rbp)
	call	bn_wexpand@PLT
	testq	%rax, %rax
	je	.L334
	movq	-64(%rbp), %r10
	leal	0(%r13,%r15), %eax
	movq	(%rbx), %rsi
	movl	%r15d, %r8d
	movl	%r13d, %edx
	movl	%eax, 8(%r10)
	movq	-56(%rbp), %rax
	movq	(%r10), %rdi
	movq	(%rax), %rcx
	call	bn_mul_normal
	movq	-64(%rbp), %r10
.L335:
	movl	16(%rbx), %eax
	movq	-56(%rbp), %rbx
	movl	$1, %r13d
	xorl	16(%rbx), %eax
	movl	%eax, 16(%r10)
	cmpq	%r10, %r12
	je	.L332
	movq	%r10, %rsi
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	call	BN_copy@PLT
	testq	%rax, %rax
	setne	%r13b
.L332:
	movq	%r14, %rdi
	call	BN_CTX_end@PLT
.L329:
	movq	%r12, %rdi
	call	bn_correct_top@PLT
	leaq	-40(%rbp), %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L344:
	.cfi_restore_state
	movq	%r14, %rdi
	call	BN_CTX_get@PLT
	movq	%rax, %r10
	testq	%rax, %rax
	jne	.L330
	.p2align 4,,10
	.p2align 3
.L334:
	xorl	%r13d, %r13d
	jmp	.L332
	.p2align 4,,10
	.p2align 3
.L343:
	xorl	%esi, %esi
	movq	%r12, %rdi
	movl	$1, %r13d
	call	BN_set_word@PLT
	jmp	.L329
	.p2align 4,,10
	.p2align 3
.L382:
	movq	%r10, -72(%rbp)
	cmpl	$-1, %eax
	jne	.L383
	movslq	%r15d, %rdi
	call	BN_num_bits_word@PLT
	movq	-72(%rbp), %r10
.L340:
	leal	-1(%rax), %ecx
	movl	$2, %esi
	movl	$1, %eax
	movq	%r14, %rdi
	sall	%cl, %eax
	sall	%cl, %esi
	movq	%r10, -80(%rbp)
	movl	%eax, -72(%rbp)
	movl	%esi, -88(%rbp)
	call	BN_CTX_get@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L334
	cmpl	%r15d, %r13d
	movq	-80(%rbp), %r10
	movl	%r15d, %edx
	cmovge	%r13d, %edx
	cmpl	%edx, -72(%rbp)
	movq	%r10, -96(%rbp)
	jge	.L338
	movl	-88(%rbp), %edi
	movq	%rax, -88(%rbp)
	sall	$2, %edi
	movl	%edi, -80(%rbp)
	movl	%edi, %esi
	movq	%rax, %rdi
	call	bn_wexpand@PLT
	testq	%rax, %rax
	je	.L334
	movq	-96(%rbp), %r10
	movl	-80(%rbp), %esi
	movq	%r10, %rdi
	movq	%r10, -80(%rbp)
	call	bn_wexpand@PLT
	testq	%rax, %rax
	je	.L334
	movl	-72(%rbp), %eax
	movq	-88(%rbp), %rcx
	movl	%r15d, %r9d
	movl	%r13d, %r8d
	movq	-80(%rbp), %r10
	subq	$8, %rsp
	subl	%eax, %r9d
	pushq	(%rcx)
	subl	%eax, %r8d
	movl	%eax, %ecx
	movq	-56(%rbp), %rax
	movq	%r10, -72(%rbp)
	movq	(%rbx), %rsi
	movq	(%r10), %rdi
	movq	(%rax), %rdx
	call	bn_mul_part_recursive
	popq	%rcx
	movq	-72(%rbp), %r10
	popq	%rsi
	jmp	.L339
	.p2align 4,,10
	.p2align 3
.L381:
	movl	$16, %esi
	movq	%r10, %rdi
	movq	%r10, -64(%rbp)
	call	bn_wexpand@PLT
	testq	%rax, %rax
	je	.L334
	movq	-64(%rbp), %r10
	movq	-56(%rbp), %rax
	movq	(%rbx), %rsi
	movq	(%r10), %rdi
	movq	(%rax), %rdx
	movl	$16, 8(%r10)
	call	bn_mul_comba8@PLT
	movq	-64(%rbp), %r10
	jmp	.L335
	.p2align 4,,10
	.p2align 3
.L383:
	movslq	%r13d, %rdi
	call	BN_num_bits_word@PLT
	movq	-72(%rbp), %r10
	jmp	.L340
	.p2align 4,,10
	.p2align 3
.L338:
	movl	-88(%rbp), %esi
	movq	%rcx, %rdi
	movq	%rcx, -88(%rbp)
	addl	%esi, %esi
	movl	%esi, -80(%rbp)
	call	bn_wexpand@PLT
	testq	%rax, %rax
	je	.L334
	movq	-96(%rbp), %r10
	movl	-80(%rbp), %esi
	movq	%r10, %rdi
	movq	%r10, -80(%rbp)
	call	bn_wexpand@PLT
	testq	%rax, %rax
	je	.L334
	movl	-72(%rbp), %eax
	movq	-88(%rbp), %rcx
	movl	%r15d, %r9d
	movl	%r13d, %r8d
	movq	-80(%rbp), %r10
	subq	$8, %rsp
	subl	%eax, %r9d
	pushq	(%rcx)
	subl	%eax, %r8d
	movl	%eax, %ecx
	movq	-56(%rbp), %rax
	movq	%r10, -72(%rbp)
	movq	(%r10), %rdi
	movq	(%rbx), %rsi
	movq	(%rax), %rdx
	call	bn_mul_recursive
	movq	-72(%rbp), %r10
	popq	%rax
	popq	%rdx
.L339:
	movl	-64(%rbp), %eax
	movl	%eax, 8(%r10)
	jmp	.L335
	.cfi_endproc
.LFE256:
	.size	BN_mul, .-BN_mul
	.p2align 4
	.globl	bn_mul_low_normal
	.type	bn_mul_low_normal, @function
bn_mul_low_normal:
.LFB259:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%ecx, %r13d
	pushq	%r12
	leal	-1(%r13), %r15d
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rdx), %rcx
	movl	%r13d, %edx
	call	bn_mul_words@PLT
	testl	%r15d, %r15d
	jle	.L384
	leal	1(%r13), %eax
	movl	%r15d, %esi
	leal	-5(%r13), %edx
	andl	$-4, %eax
	subl	%eax, %esi
	leal	-2(%r13), %eax
	andl	$3, %r13d
	andl	$-4, %eax
	movl	%esi, -52(%rbp)
	subl	$1, %r13d
	subl	%eax, %edx
	movl	%r15d, %eax
	andl	$3, %eax
	movl	%edx, -60(%rbp)
	movl	%eax, -56(%rbp)
	jmp	.L386
	.p2align 4,,10
	.p2align 3
.L389:
	movq	16(%r14), %rcx
	leaq	16(%rbx), %rdi
	movq	%r12, %rsi
	call	bn_mul_add_words@PLT
	leal	-2(%r15), %edx
	cmpl	%r13d, %r15d
	je	.L384
	movq	24(%r14), %rcx
	leaq	24(%rbx), %rdi
	movq	%r12, %rsi
	call	bn_mul_add_words@PLT
	leal	-3(%r15), %edx
	cmpl	-56(%rbp), %r15d
	je	.L384
	movq	32(%r14), %rcx
	addq	$32, %rbx
	movq	%r12, %rsi
	addq	$32, %r14
	movq	%rbx, %rdi
	subl	$4, %r15d
	call	bn_mul_add_words@PLT
	cmpl	%r15d, -60(%rbp)
	je	.L384
.L386:
	movq	8(%r14), %rcx
	movl	%r15d, %edx
	leaq	8(%rbx), %rdi
	movq	%r12, %rsi
	call	bn_mul_add_words@PLT
	leal	-1(%r15), %edx
	cmpl	-52(%rbp), %r15d
	jne	.L389
.L384:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE259:
	.size	bn_mul_low_normal, .-bn_mul_low_normal
	.p2align 4
	.globl	bn_mul_low_recursive
	.type	bn_mul_low_recursive, @function
bn_mul_low_recursive:
.LFB255:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movslq	%ecx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%r14d, %r12d
	pushq	%rbx
	shrl	$31, %r12d
	addl	%r14d, %r12d
	sarl	%r12d
	subq	$24, %rsp
	.cfi_offset 3, -56
	movl	%r12d, %ecx
	movq	%rdx, -64(%rbp)
	movslq	%r12d, %rbx
	movq	%rsi, -56(%rbp)
	salq	$3, %rbx
	call	bn_mul_recursive.constprop.0
	movq	-64(%rbp), %r9
	movq	-56(%rbp), %rsi
	addq	%rbx, %r13
	leaq	(%r9,%rbx), %rdx
	leaq	(%rsi,%rbx), %r10
	cmpl	$63, %r14d
	jle	.L391
	leaq	(%r15,%r14,8), %rbx
	movl	%r12d, %ecx
	movq	%r15, %rdi
	movq	%r10, -56(%rbp)
	movq	%rbx, %r8
	call	bn_mul_low_recursive
	movl	%r12d, %ecx
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r13, %rdi
	call	bn_add_words@PLT
	movq	-64(%rbp), %r9
	movq	-56(%rbp), %r10
	movl	%r12d, %ecx
	movq	%rbx, %r8
	movq	%r15, %rdi
	movq	%r9, %rdx
	movq	%r10, %rsi
	call	bn_mul_low_recursive
	movl	%r12d, %ecx
	movq	%r15, %rdx
.L393:
	addq	$24, %rsp
	movq	%r13, %rsi
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	bn_add_words@PLT
	.p2align 4,,10
	.p2align 3
.L391:
	.cfi_restore_state
	movl	%r12d, %ecx
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	leaq	(%r15,%rbx), %r14
	movq	%r10, -56(%rbp)
	call	bn_mul_low_normal
	movq	-64(%rbp), %r9
	movq	-56(%rbp), %r10
	movl	%r12d, %ecx
	movq	%r14, %rdi
	movq	%r9, %rdx
	movq	%r10, %rsi
	call	bn_mul_low_normal
	movl	%r12d, %ecx
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r13, %rdi
	call	bn_add_words@PLT
	movl	%r12d, %ecx
	movq	%r14, %rdx
	jmp	.L393
	.cfi_endproc
.LFE255:
	.size	bn_mul_low_recursive, .-bn_mul_low_recursive
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
