	.file	"s3_cbc.c"
	.text
	.p2align 4
	.type	tls1_md5_final_raw, @function
tls1_md5_final_raw:
.LFB1037:
	.cfi_startproc
	endbr64
	movl	(%rdi), %eax
	movb	%al, (%rsi)
	movl	(%rdi), %eax
	movb	%ah, 1(%rsi)
	movl	(%rdi), %eax
	shrl	$16, %eax
	movb	%al, 2(%rsi)
	movl	(%rdi), %eax
	shrl	$24, %eax
	movb	%al, 3(%rsi)
	movl	4(%rdi), %eax
	movb	%al, 4(%rsi)
	movl	4(%rdi), %eax
	movb	%ah, 5(%rsi)
	movzwl	6(%rdi), %eax
	movb	%al, 6(%rsi)
	movzbl	7(%rdi), %eax
	movb	%al, 7(%rsi)
	movl	8(%rdi), %eax
	movb	%al, 8(%rsi)
	movl	8(%rdi), %eax
	movb	%ah, 9(%rsi)
	movzwl	10(%rdi), %eax
	movb	%al, 10(%rsi)
	movzbl	11(%rdi), %eax
	movb	%al, 11(%rsi)
	movl	12(%rdi), %eax
	movb	%al, 12(%rsi)
	movl	12(%rdi), %eax
	movb	%ah, 13(%rsi)
	movzwl	14(%rdi), %eax
	movb	%al, 14(%rsi)
	movzbl	15(%rdi), %eax
	movb	%al, 15(%rsi)
	ret
	.cfi_endproc
.LFE1037:
	.size	tls1_md5_final_raw, .-tls1_md5_final_raw
	.p2align 4
	.type	tls1_sha1_final_raw, @function
tls1_sha1_final_raw:
.LFB1038:
	.cfi_startproc
	endbr64
	movl	(%rdi), %eax
	shrl	$24, %eax
	movb	%al, (%rsi)
	movl	(%rdi), %eax
	shrl	$16, %eax
	movb	%al, 1(%rsi)
	movl	(%rdi), %eax
	movb	%ah, 2(%rsi)
	movl	(%rdi), %eax
	movb	%al, 3(%rsi)
	movzbl	7(%rdi), %eax
	movb	%al, 4(%rsi)
	movzwl	6(%rdi), %eax
	movb	%al, 5(%rsi)
	movl	4(%rdi), %eax
	movb	%ah, 6(%rsi)
	movl	4(%rdi), %eax
	movb	%al, 7(%rsi)
	movzbl	11(%rdi), %eax
	movb	%al, 8(%rsi)
	movzwl	10(%rdi), %eax
	movb	%al, 9(%rsi)
	movl	8(%rdi), %eax
	movb	%ah, 10(%rsi)
	movl	8(%rdi), %eax
	movb	%al, 11(%rsi)
	movzbl	15(%rdi), %eax
	movb	%al, 12(%rsi)
	movzwl	14(%rdi), %eax
	movb	%al, 13(%rsi)
	movl	12(%rdi), %eax
	movb	%ah, 14(%rsi)
	movl	12(%rdi), %eax
	movb	%al, 15(%rsi)
	movzbl	19(%rdi), %eax
	movb	%al, 16(%rsi)
	movzwl	18(%rdi), %eax
	movb	%al, 17(%rsi)
	movl	16(%rdi), %eax
	movb	%ah, 18(%rsi)
	movl	16(%rdi), %eax
	movb	%al, 19(%rsi)
	ret
	.cfi_endproc
.LFE1038:
	.size	tls1_sha1_final_raw, .-tls1_sha1_final_raw
	.p2align 4
	.type	tls1_sha256_final_raw, @function
tls1_sha256_final_raw:
.LFB1039:
	.cfi_startproc
	endbr64
	movl	(%rdi), %eax
	shrl	$24, %eax
	movb	%al, (%rsi)
	movl	(%rdi), %eax
	shrl	$16, %eax
	movb	%al, 1(%rsi)
	movl	(%rdi), %eax
	movb	%ah, 2(%rsi)
	movl	(%rdi), %eax
	movb	%al, 3(%rsi)
	movzbl	7(%rdi), %eax
	movb	%al, 4(%rsi)
	movzwl	6(%rdi), %eax
	movb	%al, 5(%rsi)
	movl	4(%rdi), %eax
	movb	%ah, 6(%rsi)
	movl	4(%rdi), %eax
	movb	%al, 7(%rsi)
	movzbl	11(%rdi), %eax
	movb	%al, 8(%rsi)
	movzwl	10(%rdi), %eax
	movb	%al, 9(%rsi)
	movl	8(%rdi), %eax
	movb	%ah, 10(%rsi)
	movl	8(%rdi), %eax
	movb	%al, 11(%rsi)
	movzbl	15(%rdi), %eax
	movb	%al, 12(%rsi)
	movzwl	14(%rdi), %eax
	movb	%al, 13(%rsi)
	movl	12(%rdi), %eax
	movb	%ah, 14(%rsi)
	movl	12(%rdi), %eax
	movb	%al, 15(%rsi)
	movzbl	19(%rdi), %eax
	movb	%al, 16(%rsi)
	movzwl	18(%rdi), %eax
	movb	%al, 17(%rsi)
	movl	16(%rdi), %eax
	movb	%ah, 18(%rsi)
	movl	16(%rdi), %eax
	movb	%al, 19(%rsi)
	movzbl	23(%rdi), %eax
	movb	%al, 20(%rsi)
	movzwl	22(%rdi), %eax
	movb	%al, 21(%rsi)
	movl	20(%rdi), %eax
	movb	%ah, 22(%rsi)
	movl	20(%rdi), %eax
	movb	%al, 23(%rsi)
	movzbl	27(%rdi), %eax
	movb	%al, 24(%rsi)
	movzwl	26(%rdi), %eax
	movb	%al, 25(%rsi)
	movl	24(%rdi), %eax
	movb	%ah, 26(%rsi)
	movl	24(%rdi), %eax
	movb	%al, 27(%rsi)
	movzbl	31(%rdi), %eax
	movb	%al, 28(%rsi)
	movzwl	30(%rdi), %eax
	movb	%al, 29(%rsi)
	movl	28(%rdi), %eax
	movb	%ah, 30(%rsi)
	movl	28(%rdi), %eax
	movb	%al, 31(%rsi)
	ret
	.cfi_endproc
.LFE1039:
	.size	tls1_sha256_final_raw, .-tls1_sha256_final_raw
	.p2align 4
	.type	tls1_sha512_final_raw, @function
tls1_sha512_final_raw:
.LFB1040:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	shrq	$56, %rax
	movb	%al, (%rsi)
	movq	(%rdi), %rax
	shrq	$48, %rax
	movb	%al, 1(%rsi)
	movq	(%rdi), %rax
	shrq	$40, %rax
	movb	%al, 2(%rsi)
	movq	(%rdi), %rax
	shrq	$32, %rax
	movb	%al, 3(%rsi)
	movq	(%rdi), %rax
	shrq	$24, %rax
	movb	%al, 4(%rsi)
	movq	(%rdi), %rax
	shrq	$16, %rax
	movb	%al, 5(%rsi)
	movq	(%rdi), %rax
	shrq	$8, %rax
	movb	%al, 6(%rsi)
	movq	(%rdi), %rax
	movb	%al, 7(%rsi)
	movzbl	15(%rdi), %eax
	movb	%al, 8(%rsi)
	movzwl	14(%rdi), %eax
	movb	%al, 9(%rsi)
	movq	8(%rdi), %rax
	shrq	$40, %rax
	movb	%al, 10(%rsi)
	movl	12(%rdi), %eax
	movb	%al, 11(%rsi)
	movq	8(%rdi), %rax
	shrq	$24, %rax
	movb	%al, 12(%rsi)
	movq	8(%rdi), %rax
	shrq	$16, %rax
	movb	%al, 13(%rsi)
	movq	8(%rdi), %rax
	shrq	$8, %rax
	movb	%al, 14(%rsi)
	movq	8(%rdi), %rax
	movb	%al, 15(%rsi)
	movzbl	23(%rdi), %eax
	movb	%al, 16(%rsi)
	movzwl	22(%rdi), %eax
	movb	%al, 17(%rsi)
	movq	16(%rdi), %rax
	shrq	$40, %rax
	movb	%al, 18(%rsi)
	movl	20(%rdi), %eax
	movb	%al, 19(%rsi)
	movq	16(%rdi), %rax
	shrq	$24, %rax
	movb	%al, 20(%rsi)
	movq	16(%rdi), %rax
	shrq	$16, %rax
	movb	%al, 21(%rsi)
	movq	16(%rdi), %rax
	shrq	$8, %rax
	movb	%al, 22(%rsi)
	movq	16(%rdi), %rax
	movb	%al, 23(%rsi)
	movzbl	31(%rdi), %eax
	movb	%al, 24(%rsi)
	movzwl	30(%rdi), %eax
	movb	%al, 25(%rsi)
	movq	24(%rdi), %rax
	shrq	$40, %rax
	movb	%al, 26(%rsi)
	movl	28(%rdi), %eax
	movb	%al, 27(%rsi)
	movq	24(%rdi), %rax
	shrq	$24, %rax
	movb	%al, 28(%rsi)
	movq	24(%rdi), %rax
	shrq	$16, %rax
	movb	%al, 29(%rsi)
	movq	24(%rdi), %rax
	shrq	$8, %rax
	movb	%al, 30(%rsi)
	movq	24(%rdi), %rax
	movb	%al, 31(%rsi)
	movzbl	39(%rdi), %eax
	movb	%al, 32(%rsi)
	movzwl	38(%rdi), %eax
	movb	%al, 33(%rsi)
	movq	32(%rdi), %rax
	shrq	$40, %rax
	movb	%al, 34(%rsi)
	movl	36(%rdi), %eax
	movb	%al, 35(%rsi)
	movq	32(%rdi), %rax
	shrq	$24, %rax
	movb	%al, 36(%rsi)
	movq	32(%rdi), %rax
	shrq	$16, %rax
	movb	%al, 37(%rsi)
	movq	32(%rdi), %rax
	shrq	$8, %rax
	movb	%al, 38(%rsi)
	movq	32(%rdi), %rax
	movb	%al, 39(%rsi)
	movzbl	47(%rdi), %eax
	movb	%al, 40(%rsi)
	movzwl	46(%rdi), %eax
	movb	%al, 41(%rsi)
	movq	40(%rdi), %rax
	shrq	$40, %rax
	movb	%al, 42(%rsi)
	movl	44(%rdi), %eax
	movb	%al, 43(%rsi)
	movq	40(%rdi), %rax
	shrq	$24, %rax
	movb	%al, 44(%rsi)
	movq	40(%rdi), %rax
	shrq	$16, %rax
	movb	%al, 45(%rsi)
	movq	40(%rdi), %rax
	shrq	$8, %rax
	movb	%al, 46(%rsi)
	movq	40(%rdi), %rax
	movb	%al, 47(%rsi)
	movzbl	55(%rdi), %eax
	movb	%al, 48(%rsi)
	movzwl	54(%rdi), %eax
	movb	%al, 49(%rsi)
	movq	48(%rdi), %rax
	shrq	$40, %rax
	movb	%al, 50(%rsi)
	movl	52(%rdi), %eax
	movb	%al, 51(%rsi)
	movq	48(%rdi), %rax
	shrq	$24, %rax
	movb	%al, 52(%rsi)
	movq	48(%rdi), %rax
	shrq	$16, %rax
	movb	%al, 53(%rsi)
	movq	48(%rdi), %rax
	shrq	$8, %rax
	movb	%al, 54(%rsi)
	movq	48(%rdi), %rax
	movb	%al, 55(%rsi)
	movzbl	63(%rdi), %eax
	movb	%al, 56(%rsi)
	movzwl	62(%rdi), %eax
	movb	%al, 57(%rsi)
	movq	56(%rdi), %rax
	shrq	$40, %rax
	movb	%al, 58(%rsi)
	movl	60(%rdi), %eax
	movb	%al, 59(%rsi)
	movq	56(%rdi), %rax
	shrq	$24, %rax
	movb	%al, 60(%rsi)
	movq	56(%rdi), %rax
	shrq	$16, %rax
	movb	%al, 61(%rsi)
	movq	56(%rdi), %rax
	shrq	$8, %rax
	movb	%al, 62(%rsi)
	movq	56(%rdi), %rax
	movb	%al, 63(%rsi)
	ret
	.cfi_endproc
.LFE1040:
	.size	tls1_sha512_final_raw, .-tls1_sha512_final_raw
	.p2align 4
	.globl	ssl3_cbc_record_digest_supported
	.type	ssl3_cbc_record_digest_supported, @function
ssl3_cbc_record_digest_supported:
.LFB1041:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	EVP_MD_CTX_md@PLT
	movq	%rax, %rdi
	call	EVP_MD_type@PLT
	cmpl	$64, %eax
	je	.L9
	jg	.L8
	cmpl	$4, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	.cfi_restore_state
	subl	$672, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	cmpl	$3, %eax
	setbe	%al
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	.cfi_restore_state
	movl	$1, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1041:
	.size	ssl3_cbc_record_digest_supported, .-ssl3_cbc_record_digest_supported
	.p2align 4
	.globl	ssl3_cbc_digest_record
	.type	ssl3_cbc_digest_record, @function
ssl3_cbc_digest_record:
.LFB1042:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$968, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	24(%rbp), %rax
	movq	%rdi, -960(%rbp)
	movq	%rsi, -992(%rbp)
	movq	16(%rbp), %r15
	movq	%rax, -984(%rbp)
	movl	40(%rbp), %eax
	movq	%rdx, -976(%rbp)
	movq	%rcx, -800(%rbp)
	movq	%r8, -872(%rbp)
	movl	%eax, -964(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	$1048575, %r15
	jbe	.L12
.L143:
	xorl	%eax, %eax
.L11:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L144
	addq	$968, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	.cfi_restore_state
	movq	%r9, %rbx
	call	EVP_MD_CTX_md@PLT
	movq	%rax, %rdi
	call	EVP_MD_type@PLT
	cmpl	$673, %eax
	je	.L14
	jg	.L15
	cmpl	$64, %eax
	je	.L16
	cmpl	$672, %eax
	jne	.L145
	leaq	-752(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -880(%rbp)
	call	SHA256_Init@PLT
	testl	%eax, %eax
	jle	.L143
	movq	SHA256_Transform@GOTPCREL(%rip), %rsi
	movl	$39, %eax
	movq	$6, -864(%rbp)
	movl	$51, %r9d
	movl	$512, %r13d
	movl	$1, %r8d
	movq	$40, -1000(%rbp)
	movl	$8, %r14d
	movq	%rsi, -896(%rbp)
	leaq	tls1_sha256_final_raw(%rip), %rsi
	movq	$64, -784(%rbp)
	movq	$32, -888(%rbp)
	movq	%rsi, -904(%rbp)
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L15:
	cmpl	$674, %eax
	je	.L20
	cmpl	$675, %eax
	jne	.L19
	leaq	-752(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -880(%rbp)
	call	SHA224_Init@PLT
	testl	%eax, %eax
	jle	.L143
	movq	SHA256_Transform@GOTPCREL(%rip), %rsi
	movl	$43, %eax
	movq	$6, -864(%rbp)
	movl	$51, %r9d
	movl	$512, %r13d
	movl	$1, %r8d
	movq	$40, -1000(%rbp)
	movl	$8, %r14d
	movq	%rsi, -896(%rbp)
	leaq	tls1_sha256_final_raw(%rip), %rsi
	movq	$64, -784(%rbp)
	movq	$28, -888(%rbp)
	movq	%rsi, -904(%rbp)
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L145:
	cmpl	$4, %eax
	jne	.L19
	leaq	-752(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -880(%rbp)
	call	MD5_Init@PLT
	testl	%eax, %eax
	jle	.L143
	movq	MD5_Transform@GOTPCREL(%rip), %rsi
	movl	$55, %eax
	movq	$6, -864(%rbp)
	movl	$51, %r9d
	movl	$512, %r13d
	xorl	%r8d, %r8d
	movl	$8, %r14d
	movq	$48, -1000(%rbp)
	movq	%rsi, -896(%rbp)
	leaq	tls1_md5_final_raw(%rip), %rsi
	movq	$64, -784(%rbp)
	movq	$16, -888(%rbp)
	movq	%rsi, -904(%rbp)
	.p2align 4,,10
	.p2align 3
.L23:
	cmpb	$0, -964(%rbp)
	je	.L24
	movq	32(%rbp), %rcx
	movq	-1000(%rbp), %rsi
	xorl	%edx, %edx
	leaq	11(%rsi,%rcx), %rsi
	movq	-784(%rbp), %rcx
	addq	%rsi, %r15
	movq	%rsi, -776(%rbp)
	subq	-888(%rbp), %rsi
	addq	%r15, %rax
	addq	%rsi, %rbx
	movq	%r15, -808(%rbp)
	divq	%rcx
	xorl	%edx, %edx
	movq	%rax, -864(%rbp)
	movq	%rbx, %rax
	divq	%rcx
	movq	%rax, -912(%rbp)
	movq	%rdx, %r15
	leaq	(%r14,%rbx), %rax
	xorl	%edx, %edx
	divq	%rcx
	salq	$3, %rbx
	cmpq	$3, -864(%rbp)
	movq	%rax, -920(%rbp)
	ja	.L25
	movq	$2, -864(%rbp)
	movq	$0, -792(%rbp)
	movq	$0, -824(%rbp)
.L26:
	movq	%rbx, %r12
	movq	%rbx, %r13
	movq	%rbx, %r10
	movl	%ebx, %edi
	shrq	$24, %r12
	shrq	$16, %r13
	shrq	$8, %r10
	testb	%r8b, %r8b
	je	.L31
	leaq	-4(%r14), %rdx
	leaq	-528(%rbp), %rdi
	movl	$16, %ecx
	xorl	%esi, %esi
	movq	%r10, -848(%rbp)
	movq	%r9, -840(%rbp)
	movq	%rdx, -832(%rbp)
	movq	%rdi, -816(%rbp)
	call	__memset_chk@PLT
	movq	-832(%rbp), %rdx
	movq	-848(%rbp), %r10
	movq	-840(%rbp), %r9
	movb	%r12b, -528(%rbp,%rdx)
	movb	%r13b, -531(%rbp,%r14)
	movb	%r10b, -530(%rbp,%r14)
	movb	%bl, -529(%rbp,%r14)
.L32:
	cmpq	$0, -792(%rbp)
	je	.L42
	cmpb	$0, -964(%rbp)
	je	.L41
	movq	-784(%rbp), %r12
	movq	-776(%rbp), %rbx
	cmpq	%rbx, %r12
	jnb	.L143
	movq	-800(%rbp), %r13
	movq	-880(%rbp), %rdi
	subq	%r12, %rbx
	movq	-896(%rbp), %rax
	movq	%r13, %rsi
	call	*%rax
	movq	%r13, %rsi
	movq	%r12, %r13
	movl	$128, %ecx
	addq	%r12, %rsi
	leaq	-320(%rbp), %r12
	movq	%rbx, %rdx
	movq	%r12, %rdi
	call	__memcpy_chk@PLT
	movq	%r13, %rax
	subq	-776(%rbp), %r13
	movq	-872(%rbp), %rsi
	addq	%rax, %r13
	leaq	(%r12,%rbx), %rdi
	movq	%rax, -784(%rbp)
	movq	%r13, %rdx
	call	memcpy@PLT
	movq	-896(%rbp), %rbx
	movq	-880(%rbp), %rdi
	movq	%r12, %rsi
	call	*%rbx
	movq	-792(%rbp), %rax
	xorl	%edx, %edx
	divq	-784(%rbp)
	leaq	-1(%rax), %rbx
	cmpq	$1, %rbx
	jbe	.L42
	movq	-872(%rbp), %rsi
	movq	%r14, -832(%rbp)
	movq	%r15, -840(%rbp)
	leaq	(%rsi,%r13), %r12
	movl	$1, %r13d
	movq	%r13, %r15
	movq	%r12, %r14
	movq	-880(%rbp), %r13
	movq	%rbx, %r12
	movq	-896(%rbp), %rbx
	.p2align 4,,10
	.p2align 3
.L43:
	addq	$1, %r15
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	*%rbx
	addq	-784(%rbp), %r14
	cmpq	%r12, %r15
	jne	.L43
.L141:
	movq	-832(%rbp), %r14
	movq	-840(%rbp), %r15
.L42:
	pxor	%xmm0, %xmm0
	movq	-864(%rbp), %rbx
	movaps	%xmm0, -512(%rbp)
	movaps	%xmm0, -496(%rbp)
	movaps	%xmm0, -480(%rbp)
	movaps	%xmm0, -464(%rbp)
	cmpq	%rbx, -824(%rbp)
	ja	.L40
	movq	-784(%rbp), %rcx
	movq	-776(%rbp), %rdi
	leaq	1(%r15), %rbx
	leaq	-192(%rbp), %r13
	movq	-872(%rbp), %rdx
	movq	%rcx, %rax
	subq	%r14, %rax
	movq	%rax, -936(%rbp)
	movq	-792(%rbp), %rax
	subq	%rdi, %rax
	subq	%rdx, %rdi
	addq	%rdx, %rax
	movq	%rdi, -944(%rbp)
	movq	%rax, -832(%rbp)
	movq	-888(%rbp), %rax
	movq	%rax, %rsi
	andq	$-16, %rax
	movq	%rax, -952(%rbp)
	movq	%r15, %rax
	shrq	$4, %rsi
	notq	%rax
	movq	%rsi, -928(%rbp)
	movq	%rax, -856(%rbp)
	movq	%r14, %rax
	movq	%r13, %r14
	subq	%rcx, %rax
	addq	-816(%rbp), %rax
	movq	%rax, -816(%rbp)
	.p2align 4,,10
	.p2align 3
.L58:
	movq	-824(%rbp), %rcx
	movq	-912(%rbp), %rax
	movq	-936(%rbp), %r13
	movq	-944(%rbp), %rsi
	xorq	%rcx, %rax
	addq	-832(%rbp), %rsi
	movq	-856(%rbp), %r9
	movq	%rax, %r10
	subq	$1, %rax
	notq	%r10
	andq	%rax, %r10
	movq	-920(%rbp), %rax
	sarq	$63, %r10
	xorq	%rcx, %rax
	movl	%r10d, %r8d
	movq	%rax, %rdx
	subq	$1, %rax
	notq	%rdx
	andq	%rdx, %rax
	cqto
	movl	%edx, %eax
	movq	%rdx, -840(%rbp)
	notl	%eax
	movb	%dl, -792(%rbp)
	orl	%eax, %r10d
	movq	-784(%rbp), %rax
	cmpq	%r13, %rax
	cmovbe	%rax, %r13
	xorl	%ecx, %ecx
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L146:
	movq	-800(%rbp), %rax
	movzbl	(%rax,%rsi), %r12d
.L48:
	leaq	1(%r9), %r11
	movq	%rcx, %rax
	xorq	%rbx, %r9
	addq	$1, %rsi
	movq	%r11, %rdx
	xorq	%r15, %rax
	xorq	%r15, %rdx
	orq	%rdx, %rax
	movq	%rcx, %rdx
	xorq	%rbx, %rdx
	xorq	%rcx, %rax
	orq	%r9, %rdx
	sarq	$63, %rax
	xorq	%rcx, %rdx
	notq	%rax
	sarq	$63, %rdx
	andl	%r8d, %eax
	notq	%rdx
	movzbl	%al, %eax
	andl	%r8d, %edx
	movl	%eax, %edi
	notl	%eax
	notl	%edx
	andl	$128, %edi
	andl	%r12d, %eax
	andl	%r10d, %edx
	orl	%eax, %edi
	andl	%edx, %edi
	movb	%dil, (%r14,%rcx)
	addq	$1, %rcx
	cmpq	%r13, %rcx
	jnb	.L49
	movq	%r11, %r9
.L50:
	cmpq	%rsi, -776(%rbp)
	ja	.L146
	xorl	%r12d, %r12d
	cmpq	-808(%rbp), %rsi
	jnb	.L48
	movq	-832(%rbp), %rax
	movzbl	(%rax,%rcx), %r12d
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L49:
	movq	-856(%rbp), %rax
	movzbl	-792(%rbp), %r12d
	addq	%rcx, %rax
	cmpq	%rcx, -784(%rbp)
	jbe	.L54
	movq	-872(%rbp), %rdi
	subq	-776(%rbp), %rdi
	movq	%rdi, -848(%rbp)
	movl	%r12d, %edi
	notl	%r12d
	movl	%edi, -792(%rbp)
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L147:
	movq	-800(%rbp), %rdi
	movzbl	(%rdi,%rsi), %r11d
.L52:
	leaq	1(%rax), %r9
	movq	%rcx, %rdi
	movq	%r15, %rdx
	movq	%rcx, %r13
	xorq	%r15, %rdi
	xorq	%r9, %rdx
	xorq	%rbx, %rax
	xorq	%rbx, %r13
	orq	%rdi, %rdx
	orq	%r13, %rax
	addq	$1, %rsi
	xorq	%rcx, %rdx
	xorq	%rcx, %rax
	sarq	$63, %rdx
	sarq	$63, %rax
	notq	%rdx
	notq	%rax
	andl	%r8d, %edx
	andl	%r8d, %eax
	movzbl	%dl, %edx
	notl	%eax
	movl	%edx, %edi
	notl	%edx
	andl	%r10d, %eax
	andl	$128, %edi
	andl	%r11d, %edx
	orl	%edi, %edx
	movq	-816(%rbp), %rdi
	andl	%eax, %edx
	movzbl	%dl, %eax
	movzbl	(%rdi,%rcx), %edi
	andl	-792(%rbp), %edi
	andl	%r12d, %eax
	orl	%edi, %eax
	movb	%al, (%r14,%rcx)
	addq	$1, %rcx
	cmpq	%rcx, -784(%rbp)
	jbe	.L54
	movq	%r9, %rax
.L53:
	cmpq	-776(%rbp), %rsi
	jb	.L147
	xorl	%r11d, %r11d
	cmpq	-808(%rbp), %rsi
	jnb	.L52
	movq	-848(%rbp), %rdi
	movzbl	(%rdi,%rsi), %r11d
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L54:
	movq	-880(%rbp), %r12
	movq	-896(%rbp), %rax
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	*%rax
	movq	-904(%rbp), %rax
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	*%rax
	movd	-840(%rbp), %xmm0
	movq	-928(%rbp), %rax
	movdqa	-192(%rbp), %xmm1
	punpcklbw	%xmm0, %xmm0
	punpcklwd	%xmm0, %xmm0
	pshufd	$0, %xmm0, %xmm0
	pand	%xmm0, %xmm1
	por	-512(%rbp), %xmm1
	movaps	%xmm1, -512(%rbp)
	cmpq	$1, %rax
	je	.L56
	movdqa	-176(%rbp), %xmm1
	pand	%xmm0, %xmm1
	por	-496(%rbp), %xmm1
	movaps	%xmm1, -496(%rbp)
	cmpq	$2, %rax
	je	.L56
	movdqa	-160(%rbp), %xmm1
	pand	%xmm0, %xmm1
	por	-480(%rbp), %xmm1
	movaps	%xmm1, -480(%rbp)
	cmpq	$4, %rax
	jne	.L56
	pand	-144(%rbp), %xmm0
	por	-464(%rbp), %xmm0
	movaps	%xmm0, -464(%rbp)
.L56:
	movq	-888(%rbp), %rcx
	movq	-952(%rbp), %rsi
	cmpq	%rsi, %rcx
	je	.L57
	movq	-840(%rbp), %rdi
	movzbl	-192(%rbp,%rsi), %eax
	andl	%edi, %eax
	orb	%al, -512(%rbp,%rsi)
	leaq	1(%rsi), %rax
	cmpq	%rax, %rcx
	jbe	.L57
	movzbl	-191(%rbp,%rsi), %edx
	andl	%edi, %edx
	orb	%dl, -512(%rbp,%rax)
	leaq	2(%rsi), %rax
	cmpq	%rax, %rcx
	jbe	.L57
	movzbl	-190(%rbp,%rsi), %edx
	andl	%edi, %edx
	orb	%dl, -512(%rbp,%rax)
	leaq	3(%rsi), %rax
	cmpq	%rax, %rcx
	jbe	.L57
	movzbl	-189(%rbp,%rsi), %edx
	andl	%edi, %edx
	orb	%dl, -512(%rbp,%rax)
	leaq	4(%rsi), %rax
	cmpq	%rax, %rcx
	jbe	.L57
	movzbl	-188(%rbp,%rsi), %edx
	andl	%edi, %edx
	orb	%dl, -512(%rbp,%rax)
	leaq	5(%rsi), %rax
	cmpq	%rax, %rcx
	jbe	.L57
	movzbl	-187(%rbp,%rsi), %edx
	andl	%edi, %edx
	orb	%dl, -512(%rbp,%rax)
	leaq	6(%rsi), %rax
	cmpq	%rax, %rcx
	jbe	.L57
	movzbl	-186(%rbp,%rsi), %edx
	andl	%edi, %edx
	orb	%dl, -512(%rbp,%rax)
	leaq	7(%rsi), %rax
	cmpq	%rcx, %rax
	jnb	.L57
	movzbl	-185(%rbp,%rsi), %edx
	andl	%edi, %edx
	orb	%dl, -512(%rbp,%rax)
	leaq	8(%rsi), %rax
	cmpq	%rax, %rcx
	jbe	.L57
	movzbl	-184(%rbp,%rsi), %edx
	andl	%edi, %edx
	orb	%dl, -512(%rbp,%rax)
	leaq	9(%rsi), %rax
	cmpq	%rax, %rcx
	jbe	.L57
	movzbl	-183(%rbp,%rsi), %edx
	andl	%edi, %edx
	orb	%dl, -512(%rbp,%rax)
	leaq	10(%rsi), %rax
	cmpq	%rax, %rcx
	jbe	.L57
	movzbl	-182(%rbp,%rsi), %edx
	andl	%edi, %edx
	orb	%dl, -512(%rbp,%rax)
	leaq	11(%rsi), %rax
	cmpq	%rcx, %rax
	jnb	.L57
	movzbl	-181(%rbp,%rsi), %edx
	andl	%edi, %edx
	orb	%dl, -512(%rbp,%rax)
	leaq	12(%rsi), %rax
	cmpq	%rcx, %rax
	jnb	.L57
	movzbl	-180(%rbp,%rsi), %edx
	andl	%edi, %edx
	orb	%dl, -512(%rbp,%rax)
	leaq	13(%rsi), %rax
	cmpq	%rcx, %rax
	jnb	.L57
	movzbl	-179(%rbp,%rsi), %edx
	andl	%edi, %edx
	orb	%dl, -512(%rbp,%rax)
	leaq	14(%rsi), %rax
	cmpq	%rcx, %rax
	jnb	.L57
	movzbl	-178(%rbp,%rsi), %edx
	andl	%edi, %edx
	orb	%dl, -512(%rbp,%rax)
	.p2align 4,,10
	.p2align 3
.L57:
	addq	$1, -824(%rbp)
	movq	-784(%rbp), %rcx
	movq	-824(%rbp), %rax
	addq	%rcx, -832(%rbp)
	cmpq	-864(%rbp), %rax
	jbe	.L58
.L40:
	call	EVP_MD_CTX_new@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L45
	movq	-960(%rbp), %rdi
	call	EVP_MD_CTX_md@PLT
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	EVP_DigestInit_ex@PLT
	testl	%eax, %eax
	jle	.L45
	cmpb	$0, -964(%rbp)
	jne	.L148
	movdqa	.LC1(%rip), %xmm0
	movdqa	-448(%rbp), %xmm1
	movq	-784(%rbp), %rbx
	pxor	%xmm0, %xmm1
	movaps	%xmm1, -448(%rbp)
	movdqa	-432(%rbp), %xmm1
	movq	%rbx, %rax
	shrq	$4, %rax
	pxor	%xmm0, %xmm1
	movaps	%xmm1, -432(%rbp)
	movdqa	-416(%rbp), %xmm1
	pxor	%xmm0, %xmm1
	movaps	%xmm1, -416(%rbp)
	movdqa	-400(%rbp), %xmm1
	pxor	%xmm0, %xmm1
	movaps	%xmm1, -400(%rbp)
	cmpq	$64, %rbx
	je	.L66
	movdqa	-384(%rbp), %xmm1
	pxor	%xmm0, %xmm1
	movaps	%xmm1, -384(%rbp)
	cmpq	$5, %rax
	je	.L66
	movdqa	-368(%rbp), %xmm1
	pxor	%xmm0, %xmm1
	movaps	%xmm1, -368(%rbp)
	cmpq	$6, %rax
	je	.L66
	movdqa	-352(%rbp), %xmm1
	pxor	%xmm0, %xmm1
	movaps	%xmm1, -352(%rbp)
	cmpq	$8, %rax
	jne	.L66
	pxor	-336(%rbp), %xmm0
	movaps	%xmm0, -336(%rbp)
.L66:
	movq	-784(%rbp), %rdx
	leaq	-448(%rbp), %rsi
	movq	%r13, %rdi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	jle	.L45
.L142:
	movq	-888(%rbp), %rdx
	leaq	-512(%rbp), %rsi
	movq	%r13, %rdi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	jle	.L45
	movq	-992(%rbp), %rsi
	leaq	-756(%rbp), %rdx
	movq	%r13, %rdi
	call	EVP_DigestFinal@PLT
	testl	%eax, %eax
	je	.L65
	cmpq	$0, -976(%rbp)
	je	.L65
	movq	-976(%rbp), %rax
	movl	-756(%rbp), %ebx
	movq	%rbx, (%rax)
.L65:
	movq	%r13, %rdi
	call	EVP_MD_CTX_free@PLT
	movl	$1, %eax
	jmp	.L11
.L148:
	movq	-1000(%rbp), %rbx
	leaq	-448(%rbp), %r12
	movq	32(%rbp), %rdx
	movabsq	$6655295901103053916, %rax
	movq	%r12, %rdi
	movq	-984(%rbp), %rsi
	movq	%rbx, %rcx
	shrq	$3, %rcx
	rep stosq
	movq	%r13, %rdi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	jle	.L45
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	jg	.L142
	.p2align 4,,10
	.p2align 3
.L45:
	movq	%r13, %rdi
	call	EVP_MD_CTX_free@PLT
	xorl	%eax, %eax
	jmp	.L11
.L24:
	movq	-784(%rbp), %rsi
	xorl	%edx, %edx
	movq	-864(%rbp), %rcx
	leaq	12(%rsi,%r15), %rax
	addq	%r14, %rax
	subq	-888(%rbp), %rax
	divq	%rsi
	cmpq	%rcx, %rax
	jbe	.L73
	movq	%rax, %rdi
	movq	%rax, -864(%rbp)
	subq	%rcx, %rdi
	movq	-784(%rbp), %rcx
	imulq	%rdi, %rsi
	movq	%rdi, -824(%rbp)
	movq	%rsi, -792(%rbp)
.L68:
	leaq	-448(%rbp), %r12
	shrq	$3, %rcx
	xorl	%eax, %eax
	cmpq	$128, 32(%rbp)
	movq	%r12, %rdi
	movq	%r9, -808(%rbp)
	rep stosq
	movb	%r8b, -776(%rbp)
	ja	.L143
	movq	-984(%rbp), %rsi
	movq	32(%rbp), %rdx
	movl	$128, %ecx
	movq	%r12, %rdi
	call	__memcpy_chk@PLT
	movdqa	.LC0(%rip), %xmm0
	movdqa	-448(%rbp), %xmm1
	movq	-784(%rbp), %rsi
	movzbl	-776(%rbp), %r8d
	pxor	%xmm0, %xmm1
	movq	-808(%rbp), %r9
	movaps	%xmm1, -448(%rbp)
	movdqa	-432(%rbp), %xmm1
	movq	%rsi, %rax
	shrq	$4, %rax
	cmpq	$64, %rsi
	pxor	%xmm0, %xmm1
	movaps	%xmm1, -432(%rbp)
	movdqa	-416(%rbp), %xmm1
	pxor	%xmm0, %xmm1
	movaps	%xmm1, -416(%rbp)
	movdqa	-400(%rbp), %xmm1
	pxor	%xmm0, %xmm1
	movaps	%xmm1, -400(%rbp)
	je	.L30
	movdqa	-384(%rbp), %xmm1
	pxor	%xmm0, %xmm1
	movaps	%xmm1, -384(%rbp)
	cmpq	$5, %rax
	je	.L30
	movdqa	-368(%rbp), %xmm1
	pxor	%xmm0, %xmm1
	movaps	%xmm1, -368(%rbp)
	cmpq	$6, %rax
	je	.L30
	movdqa	-352(%rbp), %xmm1
	pxor	%xmm0, %xmm1
	movaps	%xmm1, -352(%rbp)
	cmpq	$8, %rax
	jne	.L30
	pxor	-336(%rbp), %xmm0
	movaps	%xmm0, -336(%rbp)
.L30:
	movq	-784(%rbp), %rsi
	leaq	13(%r15), %rax
	addq	$13, %rbx
	xorl	%edx, %edx
	subq	-888(%rbp), %rbx
	movq	%rax, -808(%rbp)
	movq	%rbx, %rax
	movq	%r9, -832(%rbp)
	movq	-880(%rbp), %rdi
	divq	%rsi
	movb	%r8b, -816(%rbp)
	movq	%rax, -912(%rbp)
	movq	%rdx, %r15
	leaq	(%r14,%rbx), %rax
	xorl	%edx, %edx
	divq	%rsi
	leaq	0(%r13,%rbx,8), %rbx
	movq	%r12, %rsi
	movq	%rax, -920(%rbp)
	movq	-896(%rbp), %rax
	call	*%rax
	movq	-832(%rbp), %r9
	movq	$13, -776(%rbp)
	movzbl	-816(%rbp), %r8d
	jmp	.L26
.L31:
	leaq	-528(%rbp), %r8
	movl	%r14d, %ecx
	xorl	%esi, %esi
	xorl	%eax, %eax
	movq	%r8, -816(%rbp)
	andl	$-8, %ecx
.L33:
	movl	%eax, %edx
	addl	$8, %eax
	movq	%rsi, (%r8,%rdx)
	cmpl	%ecx, %eax
	jb	.L33
	movb	%r12b, -533(%rbp,%r14)
	movb	%r13b, -534(%rbp,%r14)
	movb	%r10b, -535(%rbp,%r14)
	movb	%dil, -536(%rbp,%r14)
	jmp	.L32
.L73:
	movq	$0, -792(%rbp)
	movq	%rsi, %rcx
	movq	$0, -824(%rbp)
	jmp	.L68
.L25:
	movq	-864(%rbp), %rax
	movq	%rcx, %rsi
	subq	$2, %rax
	imulq	%rax, %rsi
	movq	%rax, -824(%rbp)
	movq	%rsi, -792(%rbp)
	jmp	.L26
.L16:
	leaq	-752(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -880(%rbp)
	call	SHA1_Init@PLT
	testl	%eax, %eax
	jle	.L143
	movq	SHA1_Transform@GOTPCREL(%rip), %rsi
	movl	$51, %eax
	movq	$6, -864(%rbp)
	movl	$51, %r9d
	movl	$512, %r13d
	movl	$1, %r8d
	movq	$40, -1000(%rbp)
	movl	$8, %r14d
	movq	%rsi, -896(%rbp)
	leaq	tls1_sha1_final_raw(%rip), %rsi
	movq	$64, -784(%rbp)
	movq	$20, -888(%rbp)
	movq	%rsi, -904(%rbp)
	jmp	.L23
.L14:
	leaq	-752(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -880(%rbp)
	call	SHA384_Init@PLT
	testl	%eax, %eax
	jle	.L143
	movq	SHA512_Transform@GOTPCREL(%rip), %rsi
	movl	$95, %eax
	movq	$4, -864(%rbp)
	movl	$115, %r9d
	movl	$1024, %r13d
	movl	$1, %r8d
	movq	$40, -1000(%rbp)
	movl	$16, %r14d
	movq	%rsi, -896(%rbp)
	leaq	tls1_sha512_final_raw(%rip), %rsi
	movq	$128, -784(%rbp)
	movq	$48, -888(%rbp)
	movq	%rsi, -904(%rbp)
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L19:
	movq	-976(%rbp), %rax
	testq	%rax, %rax
	je	.L143
	movq	$0, (%rax)
	jmp	.L143
	.p2align 4,,10
	.p2align 3
.L20:
	leaq	-752(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -880(%rbp)
	call	SHA512_Init@PLT
	testl	%eax, %eax
	jle	.L143
	movq	SHA512_Transform@GOTPCREL(%rip), %rsi
	movl	$79, %eax
	movq	$4, -864(%rbp)
	movl	$115, %r9d
	movl	$1024, %r13d
	movl	$1, %r8d
	movq	$40, -1000(%rbp)
	movl	$16, %r14d
	movq	%rsi, -896(%rbp)
	leaq	tls1_sha512_final_raw(%rip), %rsi
	movq	$128, -784(%rbp)
	movq	$64, -888(%rbp)
	movq	%rsi, -904(%rbp)
	jmp	.L23
.L41:
	movq	-800(%rbp), %rbx
	movq	-872(%rbp), %r12
	leaq	-304(%rbp), %rdi
	leaq	-320(%rbp), %r8
	movq	(%rbx), %rax
	movq	%r12, %rsi
	movq	%rax, -320(%rbp)
	movl	8(%rbx), %eax
	movl	%eax, -312(%rbp)
	movzbl	12(%rbx), %eax
	movb	%al, -308(%rbp)
	movq	(%r12), %rax
	movq	%rax, -307(%rbp)
	movq	-8(%r12,%r9), %rax
	movq	%rax, -315(%rbp,%r9)
	movq	$-3, %rax
	leal	(%r9,%rax), %ecx
	subq	%rax, %rsi
	shrl	$3, %ecx
	rep movsq
	movq	%r8, %rsi
	movq	-896(%rbp), %rax
	movq	-880(%rbp), %rdi
	call	*%rax
	movq	-792(%rbp), %rax
	movq	-784(%rbp), %rsi
	xorl	%edx, %edx
	divq	%rsi
	cmpq	$1, %rax
	jbe	.L42
	leaq	-13(%r12,%rsi), %r13
	movq	%r14, -832(%rbp)
	movl	$1, %r12d
	movq	-896(%rbp), %rbx
	movq	%r13, %r14
	movq	-880(%rbp), %r13
	movq	%r15, -840(%rbp)
	movq	%r12, %r15
	movq	%rax, %r12
	.p2align 4,,10
	.p2align 3
.L44:
	addq	$1, %r15
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	*%rbx
	addq	-784(%rbp), %r14
	cmpq	%r12, %r15
	jne	.L44
	jmp	.L141
.L144:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1042:
	.size	ssl3_cbc_digest_record, .-ssl3_cbc_digest_record
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.byte	54
	.byte	54
	.byte	54
	.byte	54
	.byte	54
	.byte	54
	.byte	54
	.byte	54
	.byte	54
	.byte	54
	.byte	54
	.byte	54
	.byte	54
	.byte	54
	.byte	54
	.byte	54
	.align 16
.LC1:
	.byte	106
	.byte	106
	.byte	106
	.byte	106
	.byte	106
	.byte	106
	.byte	106
	.byte	106
	.byte	106
	.byte	106
	.byte	106
	.byte	106
	.byte	106
	.byte	106
	.byte	106
	.byte	106
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
