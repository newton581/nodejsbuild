	.file	"p5_crpt.c"
	.text
	.p2align 4
	.globl	PKCS5_PBE_add
	.type	PKCS5_PBE_add, @function
PKCS5_PBE_add:
.LFB779:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE779:
	.size	PKCS5_PBE_add, .-PKCS5_PBE_add
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/evp/p5_crpt.c"
	.text
	.p2align 4
	.globl	PKCS5_PBE_keyivgen
	.type	PKCS5_PBE_keyivgen, @function
PKCS5_PBE_keyivgen:
.LFB780:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$216, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -216(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rcx, %rcx
	je	.L4
	cmpl	$16, (%rcx)
	je	.L54
.L4:
	movl	$41, %r8d
.L52:
	movl	$114, %edx
	movl	$117, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
.L3:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L55
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L54:
	.cfi_restore_state
	cmpq	$0, 8(%rcx)
	je	.L4
	movq	%rsi, %r13
	leaq	PBEPARAM_it(%rip), %rdi
	movq	%rcx, %rsi
	movl	%edx, %ebx
	movq	%r8, %r12
	movq	%r9, %r14
	call	ASN1_TYPE_unpack_sequence@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L56
	movq	%r12, %rdi
	call	EVP_CIPHER_iv_length@PLT
	movl	$53, %r8d
	movl	$194, %edx
	leaq	.LC0(%rip), %rcx
	movl	%eax, -220(%rbp)
	cmpl	$16, %eax
	ja	.L53
	movq	%r12, %rdi
	call	EVP_CIPHER_key_length@PLT
	movl	%eax, -224(%rbp)
	cmpl	$64, %eax
	ja	.L57
	movl	$1, -232(%rbp)
	movq	8(%r15), %rdi
	testq	%rdi, %rdi
	je	.L10
	call	ASN1_INTEGER_get@PLT
	movl	%eax, -232(%rbp)
.L10:
	movq	(%r15), %rax
	movq	8(%rax), %rdx
	movl	(%rax), %eax
	movq	%rdx, -248(%rbp)
	movl	%eax, -252(%rbp)
	testq	%r13, %r13
	je	.L20
	cmpl	$-1, %ebx
	jne	.L11
	movq	%r13, %rdi
	call	strlen@PLT
	movl	%eax, %ebx
.L11:
	call	EVP_MD_CTX_new@PLT
	testq	%rax, %rax
	je	.L58
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -240(%rbp)
	call	EVP_DigestInit_ex@PLT
	movq	-240(%rbp), %r10
	testl	%eax, %eax
	je	.L13
	movq	%r10, %rdi
	movslq	%ebx, %rdx
	movq	%r13, %rsi
	call	EVP_DigestUpdate@PLT
	movq	-240(%rbp), %r10
	testl	%eax, %eax
	jne	.L59
.L13:
	movq	%r15, %rdi
	movl	%eax, -220(%rbp)
	movq	%r10, -216(%rbp)
	call	PBEPARAM_free@PLT
	movq	-216(%rbp), %r10
	movq	%r10, %rdi
	call	EVP_MD_CTX_free@PLT
	movl	-220(%rbp), %eax
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L57:
	movl	$59, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$130, %edx
.L53:
	movl	$117, %esi
	movl	$6, %edi
	call	ERR_put_error@PLT
	movq	%r15, %rdi
	call	PBEPARAM_free@PLT
	xorl	%eax, %eax
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L56:
	movl	$47, %r8d
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L59:
	movslq	-252(%rbp), %rdx
	movq	-248(%rbp), %rsi
	movq	%r10, %rdi
	call	EVP_DigestUpdate@PLT
	movq	-240(%rbp), %r10
	testl	%eax, %eax
	je	.L13
	movq	%r15, %rdi
	leaq	-192(%rbp), %r13
	call	PBEPARAM_free@PLT
	movq	-240(%rbp), %r10
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r10, %rdi
	call	EVP_DigestFinal_ex@PLT
	movq	-240(%rbp), %r10
	testl	%eax, %eax
	je	.L16
	movq	%r14, %rdi
	movq	%r10, -240(%rbp)
	call	EVP_MD_size@PLT
	movslq	%eax, %r15
	xorl	%eax, %eax
	testl	%r15d, %r15d
	js	.L3
	cmpl	$1, -232(%rbp)
	movq	-240(%rbp), %r10
	jle	.L15
	movq	%r12, -240(%rbp)
	movl	$1, %ebx
	movq	%r10, %r12
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L61:
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	je	.L50
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	EVP_DigestFinal_ex@PLT
	testl	%eax, %eax
	je	.L50
	addl	$1, %ebx
	cmpl	%ebx, -232(%rbp)
	je	.L60
.L17:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	EVP_DigestInit_ex@PLT
	testl	%eax, %eax
	jne	.L61
.L50:
	movq	%r12, %r10
.L16:
	xorl	%eax, %eax
	xorl	%r15d, %r15d
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L20:
	xorl	%ebx, %ebx
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L58:
	movl	$78, %r8d
	movl	$65, %edx
	movl	$117, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	movq	%rax, -216(%rbp)
	call	ERR_put_error@PLT
	movq	-216(%rbp), %r10
	xorl	%eax, %eax
	jmp	.L13
.L60:
	movq	%r12, %r10
	movq	-240(%rbp), %r12
.L15:
	movslq	-224(%rbp), %rdx
	leaq	-128(%rbp), %r14
	movl	$64, %ecx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%r10, -232(%rbp)
	leaq	-208(%rbp), %r15
	call	__memcpy_chk@PLT
	movslq	-220(%rbp), %rdx
	movl	$16, %esi
	movq	%r15, %rdi
	movl	$16, %ecx
	subl	%edx, %esi
	movslq	%esi, %rsi
	addq	%r13, %rsi
	call	__memcpy_chk@PLT
	movl	16(%rbp), %r9d
	xorl	%edx, %edx
	movq	%r15, %r8
	movq	-216(%rbp), %rdi
	movq	%r14, %rcx
	movq	%r12, %rsi
	call	EVP_CipherInit_ex@PLT
	movq	-232(%rbp), %r10
	testl	%eax, %eax
	je	.L16
	movl	$64, %esi
	movq	%r13, %rdi
	movq	%r10, -216(%rbp)
	call	OPENSSL_cleanse@PLT
	movl	$64, %esi
	movq	%r14, %rdi
	call	OPENSSL_cleanse@PLT
	movq	%r15, %rdi
	movl	$16, %esi
	xorl	%r15d, %r15d
	call	OPENSSL_cleanse@PLT
	movq	-216(%rbp), %r10
	movl	$1, %eax
	jmp	.L13
.L55:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE780:
	.size	PKCS5_PBE_keyivgen, .-PKCS5_PBE_keyivgen
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
