	.file	"x509cset.c"
	.text
	.p2align 4
	.globl	X509_CRL_set_version
	.type	X509_CRL_set_version, @function
X509_CRL_set_version:
.LFB781:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L10
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L14
.L4:
	addq	$24, %rsp
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	ASN1_INTEGER_set@PLT
	.p2align 4,,10
	.p2align 3
.L14:
	.cfi_restore_state
	movq	%rsi, -24(%rbp)
	call	ASN1_INTEGER_new@PLT
	movq	-24(%rbp), %rsi
	testq	%rax, %rax
	movq	%rax, (%rbx)
	movq	%rax, %rdi
	jne	.L4
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	.cfi_restore 3
	.cfi_restore 6
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE781:
	.size	X509_CRL_set_version, .-X509_CRL_set_version
	.p2align 4
	.globl	X509_CRL_set_issuer_name
	.type	X509_CRL_set_issuer_name, @function
X509_CRL_set_issuer_name:
.LFB782:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L16
	addq	$24, %rdi
	jmp	X509_NAME_set@PLT
	.p2align 4,,10
	.p2align 3
.L16:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE782:
	.size	X509_CRL_set_issuer_name, .-X509_CRL_set_issuer_name
	.p2align 4
	.globl	X509_CRL_set1_lastUpdate
	.type	X509_CRL_set1_lastUpdate, @function
X509_CRL_set1_lastUpdate:
.LFB783:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L18
	addq	$32, %rdi
	jmp	x509_set1_time@PLT
	.p2align 4,,10
	.p2align 3
.L18:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE783:
	.size	X509_CRL_set1_lastUpdate, .-X509_CRL_set1_lastUpdate
	.p2align 4
	.globl	X509_CRL_set1_nextUpdate
	.type	X509_CRL_set1_nextUpdate, @function
X509_CRL_set1_nextUpdate:
.LFB784:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L20
	addq	$40, %rdi
	jmp	x509_set1_time@PLT
	.p2align 4,,10
	.p2align 3
.L20:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE784:
	.size	X509_CRL_set1_nextUpdate, .-X509_CRL_set1_nextUpdate
	.p2align 4
	.globl	X509_CRL_sort
	.type	X509_CRL_sort, @function
X509_CRL_sort:
.LFB785:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	48(%rdi), %rdi
	xorl	%ebx, %ebx
	call	OPENSSL_sk_sort@PLT
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L23:
	movq	48(%r12), %rdi
	movl	%ebx, %esi
	call	OPENSSL_sk_value@PLT
	movl	%ebx, 52(%rax)
	addl	$1, %ebx
.L22:
	movq	48(%r12), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jl	.L23
	movl	$1, 80(%r12)
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE785:
	.size	X509_CRL_sort, .-X509_CRL_sort
	.p2align 4
	.globl	X509_CRL_up_ref
	.type	X509_CRL_up_ref, @function
X509_CRL_up_ref:
.LFB786:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	lock xaddl	%eax, 128(%rdi)
	testl	%eax, %eax
	setg	%al
	movzbl	%al, %eax
	ret
	.cfi_endproc
.LFE786:
	.size	X509_CRL_up_ref, .-X509_CRL_up_ref
	.p2align 4
	.globl	X509_CRL_get_version
	.type	X509_CRL_get_version, @function
X509_CRL_get_version:
.LFB787:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdi
	jmp	ASN1_INTEGER_get@PLT
	.cfi_endproc
.LFE787:
	.size	X509_CRL_get_version, .-X509_CRL_get_version
	.p2align 4
	.globl	X509_CRL_get0_lastUpdate
	.type	X509_CRL_get0_lastUpdate, @function
X509_CRL_get0_lastUpdate:
.LFB788:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	ret
	.cfi_endproc
.LFE788:
	.size	X509_CRL_get0_lastUpdate, .-X509_CRL_get0_lastUpdate
	.p2align 4
	.globl	X509_CRL_get0_nextUpdate
	.type	X509_CRL_get0_nextUpdate, @function
X509_CRL_get0_nextUpdate:
.LFB789:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	ret
	.cfi_endproc
.LFE789:
	.size	X509_CRL_get0_nextUpdate, .-X509_CRL_get0_nextUpdate
	.p2align 4
	.globl	X509_CRL_get_lastUpdate
	.type	X509_CRL_get_lastUpdate, @function
X509_CRL_get_lastUpdate:
.LFB805:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	ret
	.cfi_endproc
.LFE805:
	.size	X509_CRL_get_lastUpdate, .-X509_CRL_get_lastUpdate
	.p2align 4
	.globl	X509_CRL_get_nextUpdate
	.type	X509_CRL_get_nextUpdate, @function
X509_CRL_get_nextUpdate:
.LFB807:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	ret
	.cfi_endproc
.LFE807:
	.size	X509_CRL_get_nextUpdate, .-X509_CRL_get_nextUpdate
	.p2align 4
	.globl	X509_CRL_get_issuer
	.type	X509_CRL_get_issuer, @function
X509_CRL_get_issuer:
.LFB792:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	ret
	.cfi_endproc
.LFE792:
	.size	X509_CRL_get_issuer, .-X509_CRL_get_issuer
	.p2align 4
	.globl	X509_CRL_get0_extensions
	.type	X509_CRL_get0_extensions, @function
X509_CRL_get0_extensions:
.LFB793:
	.cfi_startproc
	endbr64
	movq	56(%rdi), %rax
	ret
	.cfi_endproc
.LFE793:
	.size	X509_CRL_get0_extensions, .-X509_CRL_get0_extensions
	.p2align 4
	.globl	X509_CRL_get_REVOKED
	.type	X509_CRL_get_REVOKED, @function
X509_CRL_get_REVOKED:
.LFB794:
	.cfi_startproc
	endbr64
	movq	48(%rdi), %rax
	ret
	.cfi_endproc
.LFE794:
	.size	X509_CRL_get_REVOKED, .-X509_CRL_get_REVOKED
	.p2align 4
	.globl	X509_CRL_get0_signature
	.type	X509_CRL_get0_signature, @function
X509_CRL_get0_signature:
.LFB795:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L35
	leaq	104(%rdi), %rax
	movq	%rax, (%rsi)
.L35:
	testq	%rdx, %rdx
	je	.L34
	addq	$88, %rdi
	movq	%rdi, (%rdx)
.L34:
	ret
	.cfi_endproc
.LFE795:
	.size	X509_CRL_get0_signature, .-X509_CRL_get0_signature
	.p2align 4
	.globl	X509_CRL_get_signature_nid
	.type	X509_CRL_get_signature_nid, @function
X509_CRL_get_signature_nid:
.LFB796:
	.cfi_startproc
	endbr64
	movq	88(%rdi), %rdi
	jmp	OBJ_obj2nid@PLT
	.cfi_endproc
.LFE796:
	.size	X509_CRL_get_signature_nid, .-X509_CRL_get_signature_nid
	.p2align 4
	.globl	X509_REVOKED_get0_revocationDate
	.type	X509_REVOKED_get0_revocationDate, @function
X509_REVOKED_get0_revocationDate:
.LFB797:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	ret
	.cfi_endproc
.LFE797:
	.size	X509_REVOKED_get0_revocationDate, .-X509_REVOKED_get0_revocationDate
	.p2align 4
	.globl	X509_REVOKED_set_revocationDate
	.type	X509_REVOKED_set_revocationDate, @function
X509_REVOKED_set_revocationDate:
.LFB798:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L48
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	cmpq	%rsi, 24(%rdi)
	je	.L47
	movq	%rsi, %rdi
	call	ASN1_STRING_dup@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L47
	movq	24(%rbx), %rdi
	call	ASN1_TIME_free@PLT
	movq	%r12, 24(%rbx)
.L47:
	xorl	%eax, %eax
	testq	%r12, %r12
	popq	%rbx
	popq	%r12
	setne	%al
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L48:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE798:
	.size	X509_REVOKED_set_revocationDate, .-X509_REVOKED_set_revocationDate
	.p2align 4
	.globl	X509_REVOKED_get0_serialNumber
	.type	X509_REVOKED_get0_serialNumber, @function
X509_REVOKED_get0_serialNumber:
.LFB799:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	ret
	.cfi_endproc
.LFE799:
	.size	X509_REVOKED_get0_serialNumber, .-X509_REVOKED_get0_serialNumber
	.p2align 4
	.globl	X509_REVOKED_set_serialNumber
	.type	X509_REVOKED_set_serialNumber, @function
X509_REVOKED_set_serialNumber:
.LFB800:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L59
	movl	$1, %eax
	cmpq	%rsi, %rdi
	jne	.L61
	ret
	.p2align 4,,10
	.p2align 3
.L61:
	jmp	ASN1_STRING_copy@PLT
	.p2align 4,,10
	.p2align 3
.L59:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE800:
	.size	X509_REVOKED_set_serialNumber, .-X509_REVOKED_set_serialNumber
	.p2align 4
	.globl	X509_REVOKED_get0_extensions
	.type	X509_REVOKED_get0_extensions, @function
X509_REVOKED_get0_extensions:
.LFB801:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	ret
	.cfi_endproc
.LFE801:
	.size	X509_REVOKED_get0_extensions, .-X509_REVOKED_get0_extensions
	.p2align 4
	.globl	i2d_re_X509_CRL_tbs
	.type	i2d_re_X509_CRL_tbs, @function
i2d_re_X509_CRL_tbs:
.LFB802:
	.cfi_startproc
	endbr64
	movl	$1, 80(%rdi)
	jmp	i2d_X509_CRL_INFO@PLT
	.cfi_endproc
.LFE802:
	.size	i2d_re_X509_CRL_tbs, .-i2d_re_X509_CRL_tbs
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
