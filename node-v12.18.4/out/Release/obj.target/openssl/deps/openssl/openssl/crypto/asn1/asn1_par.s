	.file	"asn1_par.c"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"cons: "
.LC1:
	.string	"prim: "
.LC2:
	.string	"(unknown)"
.LC3:
	.string	"BAD RECURSION DEPTH\n"
.LC4:
	.string	"Error in encoding\n"
.LC5:
	.string	"%5ld:"
.LC6:
	.string	"d=%-2d hl=%ld l=%4ld "
.LC7:
	.string	"d=%-2d hl=%ld l=inf  "
.LC8:
	.string	"priv [ %d ] "
.LC9:
	.string	"cont [ %d ]"
.LC10:
	.string	"appl [ %d ]"
.LC11:
	.string	"<ASN1 %d>"
.LC12:
	.string	"\n"
.LC13:
	.string	"length is greater than %ld\n"
.LC14:
	.string	":"
.LC15:
	.string	":BAD OBJECT"
.LC16:
	.string	":BAD BOOLEAN"
.LC17:
	.string	":%u"
.LC18:
	.string	"[HEX DUMP]:"
.LC19:
	.string	"%02X"
.LC20:
	.string	"-"
.LC21:
	.string	"00"
.LC22:
	.string	":BAD INTEGER"
.LC23:
	.string	":BAD ENUMERATED"
.LC24:
	.string	":["
.LC25:
	.string	"]"
	.text
	.p2align 4
	.type	asn1_parse2, @function
asn1_parse2:
.LFB422:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$312, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -248(%rbp)
	movl	%ecx, -264(%rbp)
	movl	%r8d, -268(%rbp)
	movl	%r9d, -272(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -200(%rbp)
	cmpl	$129, %r8d
	je	.L174
	movq	(%rsi), %r14
	movq	%rdx, %r13
	leaq	(%r14,%rdx), %rax
	movq	%r14, -224(%rbp)
	movq	%rax, -344(%rbp)
	testq	%rdx, %rdx
	jle	.L105
	leaq	-228(%rbp), %rax
	movq	%r14, -280(%rbp)
	movq	%rax, -320(%rbp)
	leaq	-232(%rbp), %rax
	movq	%rax, -312(%rbp)
	leaq	-208(%rbp), %rax
	movq	%rax, -328(%rbp)
	leaq	-224(%rbp), %rax
	movl	$0, -332(%rbp)
	movq	%rax, -296(%rbp)
.L99:
	movq	-320(%rbp), %rcx
	movq	-312(%rbp), %rdx
	movq	%r13, %r8
	movq	-328(%rbp), %rsi
	movq	-296(%rbp), %rdi
	call	ASN1_get_object@PLT
	movl	%eax, %ebx
	andl	$128, %eax
	movl	%eax, -256(%rbp)
	jne	.L175
	movq	-280(%rbp), %rcx
	movq	-224(%rbp), %rax
	leaq	.LC5(%rip), %rsi
	movq	%r15, %rdi
	subq	%rcx, %rax
	movslq	%eax, %r14
	movq	-248(%rbp), %rax
	subq	%r14, %r13
	subq	(%rax), %rcx
	movslq	-264(%rbp), %rax
	movq	%r13, -288(%rbp)
	movq	%rcx, %rdx
	addq	%rax, %rdx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L167
	cmpl	$33, %ebx
	je	.L7
	movq	-208(%rbp), %r8
	xorl	%eax, %eax
	movq	%r14, %rcx
	movq	%r15, %rdi
	movl	-268(%rbp), %edx
	leaq	.LC6(%rip), %rsi
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L167
	movl	-232(%rbp), %eax
	movl	-272(%rbp), %esi
	movl	%ebx, %r13d
	movl	-228(%rbp), %r12d
	testl	%esi, %esi
	movl	%eax, -300(%rbp)
	movl	-256(%rbp), %eax
	leaq	.LC1(%rip), %rsi
	cmovne	-268(%rbp), %eax
	andl	$32, %r13d
	movl	%eax, -304(%rbp)
	leaq	.LC0(%rip), %rax
	cmovne	%rax, %rsi
.L11:
	movl	$6, %edx
	movq	%r15, %rdi
	call	BIO_write@PLT
	cmpl	$5, %eax
	jle	.L167
	movl	-304(%rbp), %esi
	movl	$128, %edx
	movq	%r15, %rdi
	call	BIO_indent@PLT
	movl	%r12d, %eax
	andl	$192, %eax
	cmpl	$192, %eax
	je	.L176
	testb	$-128, %r12b
	jne	.L177
	andl	$64, %r12d
	jne	.L178
	cmpl	$30, -300(%rbp)
	jg	.L179
	leaq	.LC2(%rip), %rdx
	ja	.L14
	movslq	-300(%rbp), %rax
	leaq	tag2str.8674(%rip), %rdi
	movq	(%rdi,%rax,8), %rdx
	jmp	.L14
.L67:
	leaq	.LC22(%rip), %rsi
	movq	%r15, %rdi
	call	BIO_puts@PLT
	testl	%eax, %eax
	jle	.L170
	xorl	%edi, %edi
	call	ASN1_INTEGER_free@PLT
	.p2align 4,,10
	.p2align 3
.L165:
	movl	$0, -332(%rbp)
.L39:
	leaq	.LC24(%rip), %rsi
	movq	%r15, %rdi
	call	BIO_puts@PLT
	testl	%eax, %eax
	jle	.L167
	movq	-280(%rbp), %rax
	xorl	%r13d, %r13d
	cmpq	$0, -208(%rbp)
	leaq	.LC19(%rip), %rbx
	leaq	(%rax,%r14), %r12
	jg	.L92
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L95:
	addq	$1, %r13
	cmpq	%r13, -208(%rbp)
	jle	.L96
.L92:
	movzbl	(%r12,%r13), %edx
	xorl	%eax, %eax
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	BIO_printf@PLT
	testl	%eax, %eax
	jg	.L95
	.p2align 4,,10
	.p2align 3
.L167:
	movq	-200(%rbp), %rdi
	xorl	%ebx, %ebx
	xorl	%r8d, %r8d
	xorl	%r13d, %r13d
.L4:
	movq	%r8, -264(%rbp)
	call	ASN1_OBJECT_free@PLT
	movq	%r13, %rdi
	call	ASN1_OCTET_STRING_free@PLT
	movq	-264(%rbp), %r8
	movq	%r8, %rdi
	call	ASN1_INTEGER_free@PLT
	movq	%rbx, %rdi
	call	ASN1_ENUMERATED_free@PLT
	movq	-224(%rbp), %rax
	movq	-248(%rbp), %rbx
	movq	%rax, (%rbx)
.L1:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L180
	movl	-256(%rbp), %eax
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	.cfi_restore_state
	movl	-268(%rbp), %edx
	xorl	%eax, %eax
	movq	%r14, %rcx
	movq	%r15, %rdi
	leaq	.LC7(%rip), %rsi
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L167
	movl	-232(%rbp), %eax
	movl	-228(%rbp), %r12d
	movl	$32, %r13d
	leaq	.LC0(%rip), %rsi
	movl	%eax, -300(%rbp)
	movl	-272(%rbp), %eax
	testl	%eax, %eax
	movl	-256(%rbp), %eax
	cmovne	-268(%rbp), %eax
	movl	%eax, -304(%rbp)
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L178:
	movl	-300(%rbp), %ecx
	leaq	-192(%rbp), %r12
	leaq	.LC10(%rip), %rdx
.L150:
	movl	$128, %esi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	BIO_snprintf@PLT
	movq	%r12, %rdx
.L14:
	xorl	%eax, %eax
	leaq	fmt.8594(%rip), %rsi
	movq	%r15, %rdi
	call	BIO_printf@PLT
	testl	%eax, %eax
	jle	.L167
	testl	%r13d, %r13d
	jne	.L181
	movl	-228(%rbp), %eax
	movl	%eax, -256(%rbp)
	testl	%eax, %eax
	jne	.L182
	movl	-232(%rbp), %eax
	cmpl	$26, %eax
	jbe	.L183
	cmpl	$30, %eax
	je	.L162
.L45:
	movq	-208(%rbp), %rdx
	cmpl	$4, %eax
	je	.L184
	cmpl	$2, %eax
	je	.L185
	cmpl	$10, %eax
	je	.L186
	testq	%rdx, %rdx
	jle	.L162
	movl	16(%rbp), %esi
	testl	%esi, %esi
	je	.L162
	movl	$1, %edx
	leaq	.LC12(%rip), %rsi
	movq	%r15, %rdi
	call	BIO_write@PLT
	testl	%eax, %eax
	jle	.L167
	cmpl	$-1, 16(%rbp)
	movq	-208(%rbp), %rax
	je	.L87
	movslq	16(%rbp), %rcx
	movl	16(%rbp), %edx
	cmpq	%rax, %rcx
	jg	.L87
.L88:
	movq	-224(%rbp), %rsi
	movl	$6, %ecx
	movq	%r15, %rdi
	call	BIO_dump_indent@PLT
	testl	%eax, %eax
	jle	.L167
.L164:
	movl	-332(%rbp), %r9d
	testl	%r9d, %r9d
	jne	.L187
	movl	$0, -332(%rbp)
	jmp	.L97
.L191:
	movq	-208(%rbp), %rdx
	movq	-280(%rbp), %rax
	leaq	-216(%rbp), %rsi
	leaq	-200(%rbp), %rdi
	addq	%r14, %rdx
	movq	%rax, -216(%rbp)
	call	d2i_ASN1_OBJECT@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L42
	movl	$1, %edx
	leaq	.LC14(%rip), %rsi
	movq	%r15, %rdi
	call	BIO_write@PLT
	testl	%eax, %eax
	jle	.L167
	movq	-200(%rbp), %rsi
	movq	%r15, %rdi
	call	i2a_ASN1_OBJECT@PLT
	.p2align 4,,10
	.p2align 3
.L162:
	movl	-332(%rbp), %edi
	testl	%edi, %edi
	jne	.L165
.L44:
	movl	$0, -332(%rbp)
.L38:
	movl	$1, %edx
	leaq	.LC12(%rip), %rsi
	movq	%r15, %rdi
	call	BIO_write@PLT
	testl	%eax, %eax
	jle	.L167
.L97:
	movq	-208(%rbp), %r14
	movl	-232(%rbp), %eax
	addq	%r14, -224(%rbp)
	orl	-228(%rbp), %eax
	je	.L188
.L25:
	movq	-288(%rbp), %r13
	subq	%r14, %r13
	testq	%r13, %r13
	jle	.L189
	movq	-224(%rbp), %rax
	movq	%rax, -280(%rbp)
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L181:
	movl	$1, %edx
	leaq	.LC12(%rip), %rsi
	movq	%r15, %rdi
	movq	-224(%rbp), %r13
	movq	-208(%rbp), %r12
	call	BIO_write@PLT
	testl	%eax, %eax
	jle	.L167
	movq	-208(%rbp), %r14
	cmpq	-288(%rbp), %r14
	jg	.L190
	cmpl	$33, %ebx
	jne	.L113
	testq	%r14, %r14
	jne	.L113
	movl	-268(%rbp), %eax
	movq	-224(%rbp), %r14
	movq	%r13, -256(%rbp)
	movq	%r15, %r13
	movq	-344(%rbp), %r12
	leal	1(%rax), %ebx
	movq	%r14, %rdx
	movq	-296(%rbp), %r14
	movl	%ebx, %r15d
	movl	-272(%rbp), %ebx
.L28:
	movq	-248(%rbp), %rax
	movq	%rdx, %rcx
	movl	%ebx, %r9d
	movl	%r15d, %r8d
	subq	$8, %rsp
	movq	%r14, %rsi
	movq	%r13, %rdi
	subq	(%rax), %rcx
	movq	%r12, %rax
	addl	-264(%rbp), %ecx
	subq	%rdx, %rax
	movq	%rax, %rdx
	movl	16(%rbp), %eax
	pushq	%rax
	call	asn1_parse2
	popq	%rdx
	popq	%rcx
	testl	%eax, %eax
	je	.L169
	movq	-224(%rbp), %rdx
	cmpl	$2, %eax
	je	.L27
	cmpq	%rdx, %r12
	ja	.L28
.L27:
	movq	%r13, %r15
	movq	-256(%rbp), %r13
	movq	%rdx, %r14
	subq	%r13, %r14
	movq	%r14, -208(%rbp)
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L183:
	movl	$98308096, %edx
	btq	%rax, %rdx
	jnc	.L34
	movl	$1, %edx
	leaq	.LC14(%rip), %rsi
	movq	%r15, %rdi
	call	BIO_write@PLT
	testl	%eax, %eax
	jle	.L167
	movq	-208(%rbp), %rdx
	testq	%rdx, %rdx
	jle	.L162
	movq	-224(%rbp), %rsi
	movq	%r15, %rdi
	call	BIO_write@PLT
	cmpl	-208(%rbp), %eax
	je	.L162
	jmp	.L167
	.p2align 4,,10
	.p2align 3
.L182:
	movl	$1, %edx
	leaq	.LC12(%rip), %rsi
	movq	%r15, %rdi
	movq	-208(%rbp), %rax
	addq	%rax, -224(%rbp)
	call	BIO_write@PLT
	testl	%eax, %eax
	jle	.L32
.L166:
	movq	-208(%rbp), %r14
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L113:
	movl	-268(%rbp), %eax
	movq	-224(%rbp), %rbx
	addq	%r13, %r12
	leal	1(%rax), %r13d
	cmpq	%r12, %rbx
	jnb	.L25
	movq	%r12, -256(%rbp)
	movq	%rbx, %r12
	movl	%r13d, %ebx
	movq	-296(%rbp), %r13
.L30:
	movq	-248(%rbp), %rax
	movq	%r12, %rcx
	movl	%ebx, %r8d
	movq	%r14, %rdx
	subq	$8, %rsp
	movl	-272(%rbp), %r9d
	movq	%r13, %rsi
	movq	%r15, %rdi
	subq	(%rax), %rcx
	movl	16(%rbp), %eax
	addl	-264(%rbp), %ecx
	pushq	%rax
	call	asn1_parse2
	popq	%r10
	popq	%r11
	testl	%eax, %eax
	je	.L169
	movq	-224(%rbp), %rax
	movq	%rax, %rcx
	subq	%r12, %rcx
	subq	%rcx, %r14
	cmpq	-256(%rbp), %rax
	jnb	.L166
	movq	%rax, %r12
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L169:
	movl	%eax, -256(%rbp)
	xorl	%ebx, %ebx
	xorl	%r8d, %r8d
	xorl	%r13d, %r13d
	movq	-200(%rbp), %rdi
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L177:
	movl	-300(%rbp), %ecx
	leaq	-192(%rbp), %r12
	leaq	.LC9(%rip), %rdx
	jmp	.L150
	.p2align 4,,10
	.p2align 3
.L176:
	movl	-300(%rbp), %ecx
	leaq	-192(%rbp), %r12
	leaq	.LC8(%rip), %rdx
	jmp	.L150
	.p2align 4,,10
	.p2align 3
.L174:
	leaq	.LC3(%rip), %rsi
	call	BIO_puts@PLT
	movl	$0, -256(%rbp)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L96:
	leaq	.LC25(%rip), %rsi
	movq	%r15, %rdi
	call	BIO_puts@PLT
	testl	%eax, %eax
	jle	.L167
	movl	-332(%rbp), %ecx
	testl	%ecx, %ecx
	jne	.L97
	movl	$1, -332(%rbp)
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L179:
	movl	-300(%rbp), %ecx
	leaq	-192(%rbp), %r12
	leaq	.LC11(%rip), %rdx
	jmp	.L150
	.p2align 4,,10
	.p2align 3
.L175:
	movq	%r15, %rdi
	movl	$18, %edx
	xorl	%ebx, %ebx
	xorl	%r13d, %r13d
	leaq	.LC4(%rip), %rsi
	call	BIO_write@PLT
	movq	-200(%rbp), %rdi
	xorl	%r8d, %r8d
	movl	$0, -256(%rbp)
	jmp	.L4
.L87:
	movl	%eax, %edx
	jmp	.L88
	.p2align 4,,10
	.p2align 3
.L34:
	cmpl	$6, %eax
	je	.L191
	cmpl	$1, %eax
	jne	.L45
	cmpq	$1, -208(%rbp)
	je	.L46
	leaq	.LC16(%rip), %rsi
	movq	%r15, %rdi
	call	BIO_puts@PLT
	testl	%eax, %eax
	jle	.L167
	cmpq	$0, -208(%rbp)
	jle	.L165
	movq	-224(%rbp), %rax
	leaq	.LC17(%rip), %rsi
	movq	%r15, %rdi
	movzbl	(%rax), %edx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movl	$0, -332(%rbp)
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L189:
	movq	-200(%rbp), %rdi
	xorl	%ebx, %ebx
	xorl	%r8d, %r8d
	xorl	%r13d, %r13d
	movl	$1, -256(%rbp)
	jmp	.L4
.L188:
	movq	-200(%rbp), %rdi
	xorl	%ebx, %ebx
	xorl	%r8d, %r8d
	xorl	%r13d, %r13d
	movl	$2, -256(%rbp)
	jmp	.L4
.L105:
	xorl	%edi, %edi
	xorl	%ebx, %ebx
	xorl	%r8d, %r8d
	xorl	%r13d, %r13d
	movl	$1, -256(%rbp)
	jmp	.L4
.L190:
	movq	-288(%rbp), %rdx
	movq	%r15, %rdi
	xorl	%eax, %eax
	xorl	%ebx, %ebx
	leaq	.LC13(%rip), %rsi
	xorl	%r13d, %r13d
	call	BIO_printf@PLT
	movq	-200(%rbp), %rdi
	xorl	%r8d, %r8d
	jmp	.L4
.L46:
	movq	-224(%rbp), %rax
	leaq	.LC17(%rip), %rsi
	movq	%r15, %rdi
	movzbl	(%rax), %edx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	jmp	.L162
.L32:
	movq	-200(%rbp), %rdi
	xorl	%ebx, %ebx
	xorl	%r8d, %r8d
	xorl	%r13d, %r13d
	movl	$0, -256(%rbp)
	jmp	.L4
.L184:
	movq	-280(%rbp), %rax
	addq	%r14, %rdx
	leaq	-216(%rbp), %rsi
	xorl	%edi, %edi
	movq	%rax, -216(%rbp)
	call	d2i_ASN1_OCTET_STRING@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L50
	movl	(%rax), %eax
	testl	%eax, %eax
	jg	.L192
.L57:
	movq	%r13, %rdi
	call	ASN1_OCTET_STRING_free@PLT
	jmp	.L162
.L185:
	movq	-280(%rbp), %rax
	addq	%r14, %rdx
	leaq	-216(%rbp), %rsi
	xorl	%edi, %edi
	movq	%rax, -216(%rbp)
	call	d2i_ASN1_INTEGER@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L67
	movl	$1, %edx
	leaq	.LC14(%rip), %rsi
	movq	%r15, %rdi
	call	BIO_write@PLT
	testl	%eax, %eax
	jle	.L170
	cmpl	$258, 4(%r13)
	je	.L193
.L69:
	movl	0(%r13), %eax
	testl	%eax, %eax
	jle	.L70
	xorl	%r12d, %r12d
	leaq	.LC19(%rip), %rbx
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L71:
	movl	0(%r13), %eax
	addq	$1, %r12
	cmpl	%r12d, %eax
	jle	.L70
.L72:
	movq	8(%r13), %rax
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movzbl	(%rax,%r12), %edx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	testl	%eax, %eax
	jg	.L71
.L170:
	movq	%r13, %r8
	movq	-200(%rbp), %rdi
	xorl	%ebx, %ebx
	xorl	%r13d, %r13d
	jmp	.L4
.L186:
	movq	-280(%rbp), %rax
	addq	%r14, %rdx
	leaq	-216(%rbp), %rsi
	xorl	%edi, %edi
	movq	%rax, -216(%rbp)
	call	d2i_ASN1_ENUMERATED@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L76
	movl	$1, %edx
	leaq	.LC14(%rip), %rsi
	movq	%r15, %rdi
	call	BIO_write@PLT
	testl	%eax, %eax
	jle	.L168
	cmpl	$266, 4(%rbx)
	je	.L194
.L78:
	movl	(%rbx), %eax
	testl	%eax, %eax
	jle	.L79
	xorl	%r13d, %r13d
	leaq	.LC19(%rip), %r12
	jmp	.L81
.L80:
	movl	(%rbx), %eax
	addq	$1, %r13
	cmpl	%r13d, %eax
	jle	.L79
.L81:
	movq	8(%rbx), %rax
	movq	%r12, %rsi
	movq	%r15, %rdi
	movzbl	(%rax,%r13), %edx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	testl	%eax, %eax
	jg	.L80
.L168:
	movq	-200(%rbp), %rdi
	xorl	%r8d, %r8d
	xorl	%r13d, %r13d
	jmp	.L4
.L42:
	leaq	.LC15(%rip), %rsi
	movq	%r15, %rdi
	call	BIO_puts@PLT
	testl	%eax, %eax
	jg	.L165
.L171:
	movq	-200(%rbp), %rdi
	xorl	%ebx, %ebx
	xorl	%r8d, %r8d
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L70:
	testl	%eax, %eax
	je	.L195
.L73:
	movq	%r13, %rdi
	call	ASN1_INTEGER_free@PLT
	jmp	.L162
.L192:
	movq	8(%r13), %rdx
	subl	$1, %eax
	movq	%rdx, -216(%rbp)
	leaq	1(%rdx,%rax), %rcx
	jmp	.L55
.L197:
	cmpb	$10, %al
	je	.L52
	andl	$-5, %eax
	cmpb	$9, %al
	jne	.L53
.L54:
	addq	$1, %rdx
	cmpq	%rdx, %rcx
	je	.L196
.L55:
	movzbl	(%rdx), %eax
	cmpb	$31, %al
	jbe	.L197
.L52:
	cmpb	$126, %al
	jbe	.L54
.L53:
	movl	16(%rbp), %edx
	testl	%edx, %edx
	je	.L198
	movl	$1, %edx
	leaq	.LC12(%rip), %rsi
	movq	%r15, %rdi
	call	BIO_write@PLT
	testl	%eax, %eax
	jle	.L171
	cmpl	$-1, 16(%rbp)
	movl	0(%r13), %edx
	je	.L63
	cmpl	16(%rbp), %edx
	cmovg	16(%rbp), %edx
.L63:
	movq	-216(%rbp), %rsi
	movl	$6, %ecx
	movq	%r15, %rdi
	call	BIO_dump_indent@PLT
	testl	%eax, %eax
	jle	.L171
	movq	%r13, %rdi
	call	ASN1_OCTET_STRING_free@PLT
	jmp	.L164
.L76:
	leaq	.LC23(%rip), %rsi
	movq	%r15, %rdi
	call	BIO_puts@PLT
	testl	%eax, %eax
	jle	.L168
	xorl	%edi, %edi
	call	ASN1_ENUMERATED_free@PLT
	movl	$0, -332(%rbp)
	jmp	.L39
.L79:
	testl	%eax, %eax
	je	.L199
.L82:
	movq	%rbx, %rdi
	call	ASN1_ENUMERATED_free@PLT
	jmp	.L162
.L50:
	xorl	%edi, %edi
	call	ASN1_OCTET_STRING_free@PLT
	movl	-332(%rbp), %r8d
	testl	%r8d, %r8d
	je	.L44
	jmp	.L165
.L187:
	movl	$1, -332(%rbp)
	jmp	.L39
.L193:
	movl	$1, %edx
	leaq	.LC20(%rip), %rsi
	movq	%r15, %rdi
	call	BIO_write@PLT
	testl	%eax, %eax
	jg	.L69
	jmp	.L170
.L196:
	movl	$1, %edx
	leaq	.LC14(%rip), %rsi
	movq	%r15, %rdi
	call	BIO_write@PLT
	testl	%eax, %eax
	jle	.L171
	movl	0(%r13), %edx
	movq	-216(%rbp), %rsi
	movq	%r15, %rdi
	call	BIO_write@PLT
	testl	%eax, %eax
	jg	.L57
	jmp	.L171
.L194:
	movl	$1, %edx
	leaq	.LC20(%rip), %rsi
	movq	%r15, %rdi
	call	BIO_write@PLT
	testl	%eax, %eax
	jg	.L78
	jmp	.L168
.L180:
	call	__stack_chk_fail@PLT
.L195:
	movl	$2, %edx
	leaq	.LC21(%rip), %rsi
	movq	%r15, %rdi
	call	BIO_write@PLT
	testl	%eax, %eax
	jg	.L73
	movq	%r13, %r8
	movq	-200(%rbp), %rdi
	xorl	%ebx, %ebx
	xorl	%r13d, %r13d
	movl	$0, -256(%rbp)
	jmp	.L4
.L199:
	movl	$2, %edx
	leaq	.LC21(%rip), %rsi
	movq	%r15, %rdi
	call	BIO_write@PLT
	testl	%eax, %eax
	jg	.L82
	movl	$0, -256(%rbp)
	movq	-200(%rbp), %rdi
	xorl	%r8d, %r8d
	xorl	%r13d, %r13d
	jmp	.L4
.L198:
	movl	$11, %edx
	leaq	.LC18(%rip), %rsi
	movq	%r15, %rdi
	call	BIO_write@PLT
	testl	%eax, %eax
	jle	.L172
	cmpl	$0, 0(%r13)
	jle	.L57
	xorl	%ebx, %ebx
	leaq	.LC19(%rip), %r12
	jmp	.L61
.L60:
	addq	$1, %rbx
	cmpl	%ebx, 0(%r13)
	jle	.L57
.L61:
	movq	-216(%rbp), %rax
	movq	%r12, %rsi
	movq	%r15, %rdi
	movzbl	(%rax,%rbx), %edx
	xorl	%eax, %eax
	call	BIO_printf@PLT
	testl	%eax, %eax
	jg	.L60
.L172:
	movl	$0, -256(%rbp)
	movq	-200(%rbp), %rdi
	xorl	%ebx, %ebx
	xorl	%r8d, %r8d
	jmp	.L4
	.cfi_endproc
.LFE422:
	.size	asn1_parse2, .-asn1_parse2
	.p2align 4
	.globl	ASN1_parse
	.type	ASN1_parse, @function
ASN1_parse:
.LFB420:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%ecx, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$24, %rsp
	pushq	$0
	movq	%rsi, -8(%rbp)
	leaq	-8(%rbp), %rsi
	call	asn1_parse2
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE420:
	.size	ASN1_parse, .-ASN1_parse
	.p2align 4
	.globl	ASN1_parse_dump
	.type	ASN1_parse_dump, @function
ASN1_parse_dump:
.LFB421:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%ecx, %r9d
	xorl	%ecx, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$24, %rsp
	pushq	%r8
	xorl	%r8d, %r8d
	movq	%rsi, -8(%rbp)
	leaq	-8(%rbp), %rsi
	call	asn1_parse2
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE421:
	.size	ASN1_parse_dump, .-ASN1_parse_dump
	.p2align 4
	.globl	ASN1_tag2str
	.type	ASN1_tag2str, @function
ASN1_tag2str:
.LFB423:
	.cfi_startproc
	endbr64
	movl	%edi, %eax
	andl	$-9, %eax
	cmpl	$258, %eax
	jne	.L205
	andl	$-257, %edi
.L206:
	movslq	%edi, %rdi
	leaq	tag2str.8674(%rip), %rax
	movq	(%rax,%rdi,8), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L205:
	leaq	.LC2(%rip), %rax
	cmpl	$30, %edi
	jbe	.L206
	ret
	.cfi_endproc
.LFE423:
	.size	ASN1_tag2str, .-ASN1_tag2str
	.section	.rodata.str1.1
.LC26:
	.string	"EOC"
.LC27:
	.string	"BOOLEAN"
.LC28:
	.string	"INTEGER"
.LC29:
	.string	"BIT STRING"
.LC30:
	.string	"OCTET STRING"
.LC31:
	.string	"NULL"
.LC32:
	.string	"OBJECT"
.LC33:
	.string	"OBJECT DESCRIPTOR"
.LC34:
	.string	"EXTERNAL"
.LC35:
	.string	"REAL"
.LC36:
	.string	"ENUMERATED"
.LC37:
	.string	"<ASN1 11>"
.LC38:
	.string	"UTF8STRING"
.LC39:
	.string	"<ASN1 13>"
.LC40:
	.string	"<ASN1 14>"
.LC41:
	.string	"<ASN1 15>"
.LC42:
	.string	"SEQUENCE"
.LC43:
	.string	"SET"
.LC44:
	.string	"NUMERICSTRING"
.LC45:
	.string	"PRINTABLESTRING"
.LC46:
	.string	"T61STRING"
.LC47:
	.string	"VIDEOTEXSTRING"
.LC48:
	.string	"IA5STRING"
.LC49:
	.string	"UTCTIME"
.LC50:
	.string	"GENERALIZEDTIME"
.LC51:
	.string	"GRAPHICSTRING"
.LC52:
	.string	"VISIBLESTRING"
.LC53:
	.string	"GENERALSTRING"
.LC54:
	.string	"UNIVERSALSTRING"
.LC55:
	.string	"<ASN1 29>"
.LC56:
	.string	"BMPSTRING"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	tag2str.8674, @object
	.size	tag2str.8674, 248
tag2str.8674:
	.quad	.LC26
	.quad	.LC27
	.quad	.LC28
	.quad	.LC29
	.quad	.LC30
	.quad	.LC31
	.quad	.LC32
	.quad	.LC33
	.quad	.LC34
	.quad	.LC35
	.quad	.LC36
	.quad	.LC37
	.quad	.LC38
	.quad	.LC39
	.quad	.LC40
	.quad	.LC41
	.quad	.LC42
	.quad	.LC43
	.quad	.LC44
	.quad	.LC45
	.quad	.LC46
	.quad	.LC47
	.quad	.LC48
	.quad	.LC49
	.quad	.LC50
	.quad	.LC51
	.quad	.LC52
	.quad	.LC53
	.quad	.LC54
	.quad	.LC55
	.quad	.LC56
	.section	.rodata
	.type	fmt.8594, @object
	.size	fmt.8594, 6
fmt.8594:
	.string	"%-18s"
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
