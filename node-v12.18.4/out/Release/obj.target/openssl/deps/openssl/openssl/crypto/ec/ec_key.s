	.file	"ec_key.c"
	.text
	.p2align 4
	.globl	EC_KEY_new
	.type	EC_KEY_new, @function
EC_KEY_new:
.LFB809:
	.cfi_startproc
	endbr64
	xorl	%edi, %edi
	jmp	EC_KEY_new_method@PLT
	.cfi_endproc
.LFE809:
	.size	EC_KEY_new, .-EC_KEY_new
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/ec/ec_key.c"
	.text
	.p2align 4
	.globl	EC_KEY_free
	.type	EC_KEY_free, @function
EC_KEY_free:
.LFB811:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L24
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$-1, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	lock xaddl	%eax, 56(%rdi)
	subl	$1, %eax
	testl	%eax, %eax
	je	.L27
	jle	.L7
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L27:
	.cfi_restore_state
.L7:
	movq	(%r12), %rax
	testq	%rax, %rax
	je	.L9
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L9
	movq	%r12, %rdi
	call	*%rax
.L9:
	movq	8(%r12), %rdi
	call	ENGINE_finish@PLT
	movq	24(%r12), %rax
	testq	%rax, %rax
	je	.L10
	movq	(%rax), %rax
	movq	376(%rax), %rax
	testq	%rax, %rax
	je	.L10
	movq	%r12, %rdi
	call	*%rax
.L10:
	leaq	64(%r12), %rdx
	movq	%r12, %rsi
	movl	$8, %edi
	call	CRYPTO_free_ex_data@PLT
	movq	72(%r12), %rdi
	call	CRYPTO_THREAD_lock_free@PLT
	movq	24(%r12), %rdi
	call	EC_GROUP_free@PLT
	movq	32(%r12), %rdi
	call	EC_POINT_free@PLT
	movq	40(%r12), %rdi
	call	BN_clear_free@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$70, %ecx
	popq	%r12
	.cfi_restore 12
	leaq	.LC0(%rip), %rdx
	movl	$80, %esi
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_clear_free@PLT
	.p2align 4,,10
	.p2align 3
.L24:
	ret
	.cfi_endproc
.LFE811:
	.size	EC_KEY_free, .-EC_KEY_free
	.p2align 4
	.globl	EC_KEY_new_by_curve_name
	.type	EC_KEY_new_by_curve_name, @function
EC_KEY_new_by_curve_name:
.LFB810:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%edi, %r13d
	xorl	%edi, %edi
	pushq	%r12
	.cfi_offset 12, -32
	call	EC_KEY_new_method@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L28
	movl	%r13d, %edi
	call	EC_GROUP_new_by_curve_name@PLT
	movq	%rax, 24(%r12)
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L38
	movq	(%r12), %rax
	movq	40(%rax), %rax
	testq	%rax, %rax
	je	.L28
	movq	%r12, %rdi
	call	*%rax
	testl	%eax, %eax
	je	.L38
.L28:
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L38:
	.cfi_restore_state
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	EC_KEY_free
	jmp	.L28
	.cfi_endproc
.LFE810:
	.size	EC_KEY_new_by_curve_name, .-EC_KEY_new_by_curve_name
	.p2align 4
	.globl	EC_KEY_copy
	.type	EC_KEY_copy, @function
EC_KEY_copy:
.LFB812:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	testq	%rdi, %rdi
	je	.L58
	movq	%rsi, %r13
	testq	%rsi, %rsi
	je	.L58
	movq	(%rdi), %rax
	movq	%rdi, %rbx
	cmpq	%rax, (%rsi)
	je	.L43
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L44
	call	*%rax
.L44:
	movq	24(%rbx), %rax
	testq	%rax, %rax
	je	.L45
	movq	(%rax), %rax
	movq	376(%rax), %rax
	testq	%rax, %rax
	je	.L45
	movq	%rbx, %rdi
	call	*%rax
.L45:
	movq	8(%rbx), %rdi
	call	ENGINE_finish@PLT
	testl	%eax, %eax
	je	.L48
	movq	$0, 8(%rbx)
.L43:
	movq	24(%r13), %rdi
	testq	%rdi, %rdi
	je	.L52
	call	EC_GROUP_method_of@PLT
	movq	24(%rbx), %rdi
	movq	%rax, %r12
	call	EC_GROUP_free@PLT
	movq	%r12, %rdi
	call	EC_GROUP_new@PLT
	movq	%rax, 24(%rbx)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L48
	movq	24(%r13), %rsi
	call	EC_GROUP_copy@PLT
	testl	%eax, %eax
	je	.L48
	cmpq	$0, 32(%r13)
	je	.L53
	movq	32(%rbx), %rdi
	call	EC_POINT_free@PLT
	movq	24(%r13), %rdi
	call	EC_POINT_new@PLT
	movq	%rax, 32(%rbx)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L48
	movq	32(%r13), %rsi
	call	EC_POINT_copy@PLT
	testl	%eax, %eax
	je	.L48
.L53:
	movq	40(%r13), %rsi
	testq	%rsi, %rsi
	je	.L52
	movq	40(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L107
.L54:
	call	BN_copy@PLT
	testq	%rax, %rax
	je	.L48
	movq	24(%r13), %rax
	movq	(%rax), %rax
	movq	368(%rax), %rax
	testq	%rax, %rax
	je	.L52
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	*%rax
	testl	%eax, %eax
	je	.L48
.L52:
	movl	48(%r13), %eax
	leaq	64(%r13), %rdx
	leaq	64(%rbx), %rsi
	movl	$8, %edi
	movl	%eax, 48(%rbx)
	movl	52(%r13), %eax
	movl	%eax, 52(%rbx)
	movl	16(%r13), %eax
	movl	%eax, 16(%rbx)
	movl	60(%r13), %eax
	movl	%eax, 60(%rbx)
	call	CRYPTO_dup_ex_data@PLT
	testl	%eax, %eax
	je	.L48
	movq	0(%r13), %rax
	movq	(%rbx), %rdx
	cmpq	%rdx, %rax
	je	.L55
	movq	8(%r13), %rdi
	testq	%rdi, %rdi
	je	.L56
	call	ENGINE_init@PLT
	testl	%eax, %eax
	je	.L48
	movq	8(%r13), %rdi
	movq	0(%r13), %rax
.L56:
	movq	%rax, %xmm0
	movq	%rdi, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rbx)
	movq	0(%r13), %rdx
.L55:
	movq	32(%rdx), %rax
	movq	%rbx, %r12
	testq	%rax, %rax
	je	.L39
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	*%rax
	testl	%eax, %eax
	jne	.L39
	.p2align 4,,10
	.p2align 3
.L48:
	xorl	%r12d, %r12d
.L39:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L107:
	.cfi_restore_state
	call	BN_new@PLT
	movq	%rax, 40(%rbx)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L48
	movq	40(%r13), %rsi
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L58:
	movl	$76, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$67, %edx
	xorl	%r12d, %r12d
	movl	$178, %esi
	movl	$16, %edi
	call	ERR_put_error@PLT
	jmp	.L39
	.cfi_endproc
.LFE812:
	.size	EC_KEY_copy, .-EC_KEY_copy
	.p2align 4
	.globl	EC_KEY_dup
	.type	EC_KEY_dup, @function
EC_KEY_dup:
.LFB813:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	8(%rdi), %rdi
	call	EC_KEY_new_method@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L108
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	EC_KEY_copy
	testq	%rax, %rax
	je	.L114
.L108:
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L114:
	.cfi_restore_state
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	EC_KEY_free
	jmp	.L108
	.cfi_endproc
.LFE813:
	.size	EC_KEY_dup, .-EC_KEY_dup
	.p2align 4
	.globl	EC_KEY_up_ref
	.type	EC_KEY_up_ref, @function
EC_KEY_up_ref:
.LFB814:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	lock xaddl	%eax, 56(%rdi)
	testl	%eax, %eax
	setg	%al
	movzbl	%al, %eax
	ret
	.cfi_endproc
.LFE814:
	.size	EC_KEY_up_ref, .-EC_KEY_up_ref
	.p2align 4
	.globl	EC_KEY_get0_engine
	.type	EC_KEY_get0_engine, @function
EC_KEY_get0_engine:
.LFB815:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	ret
	.cfi_endproc
.LFE815:
	.size	EC_KEY_get0_engine, .-EC_KEY_get0_engine
	.p2align 4
	.globl	EC_KEY_generate_key
	.type	EC_KEY_generate_key, @function
EC_KEY_generate_key:
.LFB816:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testq	%rdi, %rdi
	je	.L118
	cmpq	$0, 24(%rdi)
	je	.L118
	movq	(%rdi), %rax
	movq	64(%rax), %rax
	testq	%rax, %rax
	je	.L121
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L118:
	.cfi_restore_state
	movl	$184, %r8d
	movl	$67, %edx
	movl	$179, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L121:
	.cfi_restore_state
	movl	$189, %r8d
	movl	$152, %edx
	movl	$179, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE816:
	.size	EC_KEY_generate_key, .-EC_KEY_generate_key
	.p2align 4
	.globl	ossl_ec_key_gen
	.type	ossl_ec_key_gen, @function
ossl_ec_key_gen:
.LFB817:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	movq	(%rax), %rax
	jmp	*344(%rax)
	.cfi_endproc
.LFE817:
	.size	ossl_ec_key_gen, .-ossl_ec_key_gen
	.p2align 4
	.globl	ec_key_simple_generate_key
	.type	ec_key_simple_generate_key, @function
ec_key_simple_generate_key:
.LFB818:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	call	BN_CTX_new@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L128
	movq	40(%rbx), %r12
	testq	%r12, %r12
	je	.L157
.L129:
	movq	24(%rbx), %rdi
	call	EC_GROUP_get0_order@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	jne	.L131
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L159:
	movq	%r12, %rdi
	call	BN_is_zero@PLT
	testl	%eax, %eax
	je	.L158
.L131:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	BN_priv_rand_range@PLT
	testl	%eax, %eax
	jne	.L159
	xorl	%r13d, %r13d
.L130:
	cmpq	$0, 32(%rbx)
	je	.L160
.L133:
	xorl	%r13d, %r13d
	cmpq	%r12, 40(%rbx)
	je	.L134
	movq	%r12, %rdi
	call	BN_free@PLT
.L134:
	movq	%r14, %rdi
	call	BN_CTX_free@PLT
	popq	%rbx
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L158:
	.cfi_restore_state
	movq	32(%rbx), %r13
	movq	24(%rbx), %rdi
	testq	%r13, %r13
	je	.L161
.L132:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r14, %r9
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	EC_POINT_mul@PLT
	testl	%eax, %eax
	je	.L130
	movq	%r13, %xmm0
	movq	%r12, %xmm1
	movl	$1, %r13d
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 32(%rbx)
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L157:
	call	BN_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L129
	.p2align 4,,10
	.p2align 3
.L128:
	xorl	%r13d, %r13d
	xorl	%r12d, %r12d
	cmpq	$0, 32(%rbx)
	jne	.L133
.L160:
	movq	%r13, %rdi
	call	EC_POINT_free@PLT
	jmp	.L133
	.p2align 4,,10
	.p2align 3
.L161:
	call	EC_POINT_new@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L130
	movq	24(%rbx), %rdi
	jmp	.L132
	.cfi_endproc
.LFE818:
	.size	ec_key_simple_generate_key, .-ec_key_simple_generate_key
	.p2align 4
	.globl	ec_key_simple_generate_public_key
	.type	ec_key_simple_generate_public_key, @function
ec_key_simple_generate_public_key:
.LFB819:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rdx
	movq	32(%rdi), %rsi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	24(%rdi), %rdi
	xorl	%ecx, %ecx
	jmp	EC_POINT_mul@PLT
	.cfi_endproc
.LFE819:
	.size	ec_key_simple_generate_public_key, .-ec_key_simple_generate_public_key
	.p2align 4
	.globl	EC_KEY_check_key
	.type	EC_KEY_check_key, @function
EC_KEY_check_key:
.LFB820:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testq	%rdi, %rdi
	je	.L164
	movq	24(%rdi), %rax
	testq	%rax, %rax
	je	.L164
	cmpq	$0, 32(%rdi)
	je	.L164
	movq	(%rax), %rax
	movq	352(%rax), %rax
	testq	%rax, %rax
	je	.L175
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L164:
	.cfi_restore_state
	movl	$258, %r8d
	movl	$67, %edx
	movl	$177, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L163:
	xorl	%eax, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L175:
	.cfi_restore_state
	movl	$263, %r8d
	movl	$66, %edx
	movl	$177, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L163
	.cfi_endproc
.LFE820:
	.size	EC_KEY_check_key, .-EC_KEY_check_key
	.p2align 4
	.globl	ec_key_simple_check_key
	.type	ec_key_simple_check_key, @function
ec_key_simple_check_key:
.LFB821:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%rdi, %rdi
	je	.L177
	movq	%rdi, %rbx
	movq	24(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L177
	movq	32(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L177
	call	EC_POINT_is_at_infinity@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	jne	.L205
	call	BN_CTX_new@PLT
	xorl	%r14d, %r14d
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L181
	movq	24(%rbx), %rdi
	call	EC_POINT_new@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L181
	movq	32(%rbx), %rsi
	movq	24(%rbx), %rdi
	movq	%r13, %rdx
	call	EC_POINT_is_on_curve@PLT
	testl	%eax, %eax
	jle	.L206
	movq	24(%rbx), %rax
	movq	16(%rax), %r15
	movq	%r15, %rdi
	call	BN_is_zero@PLT
	testl	%eax, %eax
	jne	.L207
	movq	32(%rbx), %rcx
	movq	24(%rbx), %rdi
	movq	%r15, %r8
	movq	%r13, %r9
	xorl	%edx, %edx
	movq	%r14, %rsi
	call	EC_POINT_mul@PLT
	movl	$304, %r8d
	movl	%eax, %r12d
	testl	%eax, %eax
	je	.L204
	movq	24(%rbx), %rdi
	movq	%r14, %rsi
	call	EC_POINT_is_at_infinity@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	je	.L208
	movq	40(%rbx), %rdi
	movl	$1, %r12d
	testq	%rdi, %rdi
	je	.L181
	movq	%r15, %rsi
	call	BN_cmp@PLT
	testl	%eax, %eax
	jns	.L209
	movq	40(%rbx), %rdx
	movq	24(%rbx), %rdi
	movq	%r13, %r9
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	call	EC_POINT_mul@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	je	.L210
	movq	32(%rbx), %rdx
	movq	24(%rbx), %rdi
	movq	%r13, %rcx
	movq	%r14, %rsi
	movl	$1, %r12d
	call	EC_POINT_cmp@PLT
	testl	%eax, %eax
	je	.L181
	movl	$326, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$123, %edx
	xorl	%r12d, %r12d
	movl	$258, %esi
	movl	$16, %edi
	call	ERR_put_error@PLT
	.p2align 4,,10
	.p2align 3
.L181:
	movq	%r13, %rdi
	call	BN_CTX_free@PLT
	movq	%r14, %rdi
	call	EC_POINT_free@PLT
.L176:
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L177:
	.cfi_restore_state
	movl	$278, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$67, %edx
	xorl	%r12d, %r12d
	movl	$258, %esi
	movl	$16, %edi
	call	ERR_put_error@PLT
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L205:
	movl	$106, %edx
	movl	$258, %esi
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	movl	$283, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$16, %edi
	xorl	%r12d, %r12d
	call	ERR_put_error@PLT
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L208:
	movl	$308, %r8d
	movl	$130, %edx
	movl	$258, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L206:
	movl	$294, %r8d
	movl	$107, %edx
	movl	$258, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L207:
	movl	$300, %r8d
	movl	$122, %edx
	movl	$258, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L181
.L210:
	movl	$322, %r8d
	.p2align 4,,10
	.p2align 3
.L204:
	movl	$16, %edx
	movl	$258, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L181
.L209:
	movl	$317, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$130, %edx
	xorl	%r12d, %r12d
	movl	$258, %esi
	movl	$16, %edi
	call	ERR_put_error@PLT
	jmp	.L181
	.cfi_endproc
.LFE821:
	.size	ec_key_simple_check_key, .-ec_key_simple_check_key
	.p2align 4
	.globl	EC_KEY_set_public_key_affine_coordinates
	.type	EC_KEY_set_public_key_affine_coordinates, @function
EC_KEY_set_public_key_affine_coordinates:
.LFB822:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%rdi, %rdi
	je	.L212
	cmpq	$0, 24(%rdi)
	movq	%rdi, %r12
	je	.L212
	movq	%rsi, %r13
	testq	%rsi, %rsi
	je	.L212
	movq	%rdx, %r14
	testq	%rdx, %rdx
	je	.L212
	call	BN_CTX_new@PLT
	movq	%rax, %rbx
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.L211
	movq	%rbx, %rdi
	call	BN_CTX_start@PLT
	movq	24(%r12), %rdi
	call	EC_POINT_new@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L246
	movq	%rbx, %rdi
	call	BN_CTX_get@PLT
	movq	%rbx, %rdi
	movq	%rax, -56(%rbp)
	call	BN_CTX_get@PLT
	testq	%rax, %rax
	movq	%rax, -64(%rbp)
	je	.L246
	movq	24(%r12), %rdi
	movq	%rbx, %r8
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r15, %rsi
	call	EC_POINT_set_affine_coordinates@PLT
	testl	%eax, %eax
	je	.L246
	movq	-64(%rbp), %r10
	movq	24(%r12), %rdi
	movq	%rbx, %r8
	movq	%r15, %rsi
	movq	-56(%rbp), %rdx
	movq	%r10, %rcx
	call	EC_POINT_get_affine_coordinates@PLT
	testl	%eax, %eax
	jne	.L247
	.p2align 4,,10
	.p2align 3
.L246:
	xorl	%eax, %eax
.L217:
	movq	%rbx, %rdi
	movl	%eax, -56(%rbp)
	call	BN_CTX_end@PLT
	movq	%rbx, %rdi
	call	BN_CTX_free@PLT
	movq	%r15, %rdi
	call	EC_POINT_free@PLT
	movl	-56(%rbp), %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L212:
	.cfi_restore_state
	movl	$346, %r8d
	movl	$67, %edx
	movl	$229, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
.L211:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L247:
	.cfi_restore_state
	movq	-56(%rbp), %rsi
	movq	%r13, %rdi
	call	BN_cmp@PLT
	movq	-64(%rbp), %r10
	testl	%eax, %eax
	jne	.L220
	movq	%r10, %rsi
	movq	%r14, %rdi
	call	BN_cmp@PLT
	testl	%eax, %eax
	jne	.L220
	movq	24(%r12), %rax
	movq	%r13, %rdi
	movq	64(%rax), %rsi
	call	BN_cmp@PLT
	testl	%eax, %eax
	js	.L248
.L220:
	movl	$377, %r8d
	movl	$146, %edx
	movl	$229, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L246
.L248:
	movq	24(%r12), %rax
	movq	%r14, %rdi
	movq	64(%rax), %rsi
	call	BN_cmp@PLT
	testl	%eax, %eax
	jns	.L220
	movq	(%r12), %rax
	movq	56(%rax), %rax
	testq	%rax, %rax
	je	.L223
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	*%rax
	testl	%eax, %eax
	je	.L246
.L223:
	movq	32(%r12), %rdi
	call	EC_POINT_free@PLT
	movq	24(%r12), %rsi
	movq	%r15, %rdi
	call	EC_POINT_dup@PLT
	movq	%rax, 32(%r12)
	testq	%rax, %rax
	je	.L246
	movq	%r12, %rdi
	call	EC_KEY_check_key
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
	jmp	.L217
	.cfi_endproc
.LFE822:
	.size	EC_KEY_set_public_key_affine_coordinates, .-EC_KEY_set_public_key_affine_coordinates
	.p2align 4
	.globl	EC_KEY_get0_group
	.type	EC_KEY_get0_group, @function
EC_KEY_get0_group:
.LFB823:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	ret
	.cfi_endproc
.LFE823:
	.size	EC_KEY_get0_group, .-EC_KEY_get0_group
	.p2align 4
	.globl	EC_KEY_set_group
	.type	EC_KEY_set_group, @function
EC_KEY_set_group:
.LFB824:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	(%rdi), %rax
	movq	%rdi, %rbx
	movq	40(%rax), %rax
	testq	%rax, %rax
	je	.L253
	call	*%rax
	testl	%eax, %eax
	je	.L250
.L253:
	movq	24(%rbx), %rdi
	call	EC_GROUP_free@PLT
	movq	%r12, %rdi
	call	EC_GROUP_dup@PLT
	testq	%rax, %rax
	movq	%rax, 24(%rbx)
	setne	%al
	movzbl	%al, %eax
.L250:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE824:
	.size	EC_KEY_set_group, .-EC_KEY_set_group
	.p2align 4
	.globl	EC_KEY_get0_private_key
	.type	EC_KEY_get0_private_key, @function
EC_KEY_get0_private_key:
.LFB825:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	ret
	.cfi_endproc
.LFE825:
	.size	EC_KEY_get0_private_key, .-EC_KEY_get0_private_key
	.p2align 4
	.globl	EC_KEY_set_private_key
	.type	EC_KEY_set_private_key, @function
EC_KEY_set_private_key:
.LFB826:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	testq	%rax, %rax
	je	.L274
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L274
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	336(%rax), %rax
	movq	%rdi, %rbx
	testq	%rax, %rax
	je	.L263
	call	*%rax
	testl	%eax, %eax
	je	.L259
.L263:
	movq	(%rbx), %rax
	movq	48(%rax), %rax
	testq	%rax, %rax
	je	.L262
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	*%rax
	testl	%eax, %eax
	je	.L259
.L262:
	movq	40(%rbx), %rdi
	call	BN_clear_free@PLT
	movq	%r12, %rdi
	call	BN_dup@PLT
	testq	%rax, %rax
	movq	%rax, 40(%rbx)
	popq	%rbx
	setne	%al
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movzbl	%al, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L259:
	.cfi_restore_state
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L274:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE826:
	.size	EC_KEY_set_private_key, .-EC_KEY_set_private_key
	.p2align 4
	.globl	EC_KEY_get0_public_key
	.type	EC_KEY_get0_public_key, @function
EC_KEY_get0_public_key:
.LFB827:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	ret
	.cfi_endproc
.LFE827:
	.size	EC_KEY_get0_public_key, .-EC_KEY_get0_public_key
	.p2align 4
	.globl	EC_KEY_set_public_key
	.type	EC_KEY_set_public_key, @function
EC_KEY_set_public_key:
.LFB828:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	(%rdi), %rax
	movq	%rdi, %rbx
	movq	56(%rax), %rax
	testq	%rax, %rax
	je	.L281
	call	*%rax
	testl	%eax, %eax
	je	.L278
.L281:
	movq	32(%rbx), %rdi
	call	EC_POINT_free@PLT
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	EC_POINT_dup@PLT
	testq	%rax, %rax
	movq	%rax, 32(%rbx)
	setne	%al
	movzbl	%al, %eax
.L278:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE828:
	.size	EC_KEY_set_public_key, .-EC_KEY_set_public_key
	.p2align 4
	.globl	EC_KEY_get_enc_flags
	.type	EC_KEY_get_enc_flags, @function
EC_KEY_get_enc_flags:
.LFB829:
	.cfi_startproc
	endbr64
	movl	48(%rdi), %eax
	ret
	.cfi_endproc
.LFE829:
	.size	EC_KEY_get_enc_flags, .-EC_KEY_get_enc_flags
	.p2align 4
	.globl	EC_KEY_set_enc_flags
	.type	EC_KEY_set_enc_flags, @function
EC_KEY_set_enc_flags:
.LFB830:
	.cfi_startproc
	endbr64
	movl	%esi, 48(%rdi)
	ret
	.cfi_endproc
.LFE830:
	.size	EC_KEY_set_enc_flags, .-EC_KEY_set_enc_flags
	.p2align 4
	.globl	EC_KEY_get_conv_form
	.type	EC_KEY_get_conv_form, @function
EC_KEY_get_conv_form:
.LFB831:
	.cfi_startproc
	endbr64
	movl	52(%rdi), %eax
	ret
	.cfi_endproc
.LFE831:
	.size	EC_KEY_get_conv_form, .-EC_KEY_get_conv_form
	.p2align 4
	.globl	EC_KEY_set_conv_form
	.type	EC_KEY_set_conv_form, @function
EC_KEY_set_conv_form:
.LFB832:
	.cfi_startproc
	endbr64
	movl	%esi, 52(%rdi)
	movq	24(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L286
	jmp	EC_GROUP_set_point_conversion_form@PLT
	.p2align 4,,10
	.p2align 3
.L286:
	ret
	.cfi_endproc
.LFE832:
	.size	EC_KEY_set_conv_form, .-EC_KEY_set_conv_form
	.p2align 4
	.globl	EC_KEY_set_asn1_flag
	.type	EC_KEY_set_asn1_flag, @function
EC_KEY_set_asn1_flag:
.LFB833:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L288
	jmp	EC_GROUP_set_asn1_flag@PLT
	.p2align 4,,10
	.p2align 3
.L288:
	ret
	.cfi_endproc
.LFE833:
	.size	EC_KEY_set_asn1_flag, .-EC_KEY_set_asn1_flag
	.p2align 4
	.globl	EC_KEY_precompute_mult
	.type	EC_KEY_precompute_mult, @function
EC_KEY_precompute_mult:
.LFB834:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L291
	jmp	EC_GROUP_precompute_mult@PLT
	.p2align 4,,10
	.p2align 3
.L291:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE834:
	.size	EC_KEY_precompute_mult, .-EC_KEY_precompute_mult
	.p2align 4
	.globl	EC_KEY_get_flags
	.type	EC_KEY_get_flags, @function
EC_KEY_get_flags:
.LFB835:
	.cfi_startproc
	endbr64
	movl	60(%rdi), %eax
	ret
	.cfi_endproc
.LFE835:
	.size	EC_KEY_get_flags, .-EC_KEY_get_flags
	.p2align 4
	.globl	EC_KEY_set_flags
	.type	EC_KEY_set_flags, @function
EC_KEY_set_flags:
.LFB836:
	.cfi_startproc
	endbr64
	orl	%esi, 60(%rdi)
	ret
	.cfi_endproc
.LFE836:
	.size	EC_KEY_set_flags, .-EC_KEY_set_flags
	.p2align 4
	.globl	EC_KEY_clear_flags
	.type	EC_KEY_clear_flags, @function
EC_KEY_clear_flags:
.LFB837:
	.cfi_startproc
	endbr64
	notl	%esi
	andl	%esi, 60(%rdi)
	ret
	.cfi_endproc
.LFE837:
	.size	EC_KEY_clear_flags, .-EC_KEY_clear_flags
	.p2align 4
	.globl	EC_KEY_key2buf
	.type	EC_KEY_key2buf, @function
EC_KEY_key2buf:
.LFB838:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L295
	movq	32(%rdi), %r9
	testq	%r9, %r9
	je	.L295
	movq	24(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L295
	movq	%rcx, %r8
	movq	%rdx, %rcx
	movl	%esi, %edx
	movq	%r9, %rsi
	jmp	EC_POINT_point2buf@PLT
	.p2align 4,,10
	.p2align 3
.L295:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE838:
	.size	EC_KEY_key2buf, .-EC_KEY_key2buf
	.p2align 4
	.globl	EC_KEY_oct2key
	.type	EC_KEY_oct2key, @function
EC_KEY_oct2key:
.LFB839:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L321
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	24(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L306
	movq	%rsi, %r12
	movq	32(%rbx), %rsi
	movq	%rdx, %r13
	movq	%rcx, %r14
	testq	%rsi, %rsi
	je	.L322
.L307:
	movq	%r14, %r8
	movq	%r13, %rcx
	movq	%r12, %rdx
	call	EC_POINT_oct2point@PLT
	testl	%eax, %eax
	je	.L306
	movq	24(%rbx), %rax
	movq	(%rax), %rdx
	movl	$1, %eax
	testb	$2, (%rdx)
	jne	.L303
	movzbl	(%r12), %edx
	andl	$-2, %edx
	movl	%edx, 52(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L322:
	.cfi_restore_state
	call	EC_POINT_new@PLT
	movq	%rax, 32(%rbx)
	movq	%rax, %rsi
	testq	%rax, %rax
	jne	.L323
.L306:
	xorl	%eax, %eax
.L303:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L321:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	xorl	%eax, %eax
	ret
.L323:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	movq	24(%rbx), %rdi
	jmp	.L307
	.cfi_endproc
.LFE839:
	.size	EC_KEY_oct2key, .-EC_KEY_oct2key
	.p2align 4
	.globl	EC_KEY_priv2oct
	.type	EC_KEY_priv2oct, @function
EC_KEY_priv2oct:
.LFB840:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	testq	%rax, %rax
	je	.L332
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L332
	movq	320(%rax), %rax
	testq	%rax, %rax
	je	.L335
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L332:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L335:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$66, %edx
	movl	$256, %esi
	movl	$16, %edi
	movl	$534, %r8d
	leaq	.LC0(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE840:
	.size	EC_KEY_priv2oct, .-EC_KEY_priv2oct
	.p2align 4
	.globl	ec_key_simple_priv2oct
	.type	ec_key_simple_priv2oct, @function
ec_key_simple_priv2oct:
.LFB841:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	24(%rdi), %rdi
	movq	%rdx, %rbx
	call	EC_GROUP_order_bits@PLT
	movq	40(%r12), %rdi
	leal	14(%rax), %edx
	addl	$7, %eax
	cmovns	%eax, %edx
	sarl	$3, %edx
	testq	%rdi, %rdi
	je	.L339
	movslq	%edx, %r14
	testq	%r13, %r13
	je	.L336
	cmpq	%rbx, %r14
	jbe	.L344
.L339:
	xorl	%r14d, %r14d
.L336:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L344:
	.cfi_restore_state
	movq	%r13, %rsi
	call	BN_bn2binpad@PLT
	cmpl	$-1, %eax
	jne	.L336
	movl	$557, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$100, %edx
	xorl	%r14d, %r14d
	movl	$260, %esi
	movl	$16, %edi
	call	ERR_put_error@PLT
	jmp	.L336
	.cfi_endproc
.LFE841:
	.size	ec_key_simple_priv2oct, .-ec_key_simple_priv2oct
	.p2align 4
	.globl	EC_KEY_oct2priv
	.type	EC_KEY_oct2priv, @function
EC_KEY_oct2priv:
.LFB842:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	testq	%rax, %rax
	je	.L353
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L353
	movq	328(%rax), %rax
	testq	%rax, %rax
	je	.L356
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L353:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L356:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$66, %edx
	movl	$255, %esi
	movl	$16, %edi
	movl	$569, %r8d
	leaq	.LC0(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE842:
	.size	EC_KEY_oct2priv, .-EC_KEY_oct2priv
	.p2align 4
	.globl	ec_key_simple_oct2priv
	.type	ec_key_simple_oct2priv, @function
ec_key_simple_oct2priv:
.LFB843:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$16, %rsp
	movq	40(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L362
.L358:
	movl	%r12d, %esi
	call	BN_bin2bn@PLT
	movl	$1, %r8d
	movq	%rax, 40(%rbx)
	testq	%rax, %rax
	je	.L363
.L357:
	addq	$16, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L362:
	.cfi_restore_state
	movq	%rsi, -24(%rbp)
	call	BN_secure_new@PLT
	movq	-24(%rbp), %rdi
	testq	%rax, %rax
	movq	%rax, 40(%rbx)
	movq	%rax, %rdx
	jne	.L358
	movl	$580, %r8d
	movl	$65, %edx
	movl	$259, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$16, %rsp
	xorl	%r8d, %r8d
	popq	%rbx
	movl	%r8d, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L363:
	.cfi_restore_state
	movl	$585, %r8d
	movl	$3, %edx
	movl	$259, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%r8d, %r8d
	jmp	.L357
	.cfi_endproc
.LFE843:
	.size	ec_key_simple_oct2priv, .-ec_key_simple_oct2priv
	.p2align 4
	.globl	EC_KEY_priv2buf
	.type	EC_KEY_priv2buf, @function
EC_KEY_priv2buf:
.LFB844:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	testq	%rax, %rax
	je	.L389
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L389
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	320(%rax), %rax
	testq	%rax, %rax
	je	.L393
	movq	%rdi, %r12
	movq	%rsi, %rbx
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	*%rax
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L392
	movl	$599, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rax, %rdi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L394
	movq	24(%r12), %rax
	testq	%rax, %rax
	je	.L371
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L371
	movq	320(%rax), %rax
	testq	%rax, %rax
	je	.L395
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	*%rax
	testq	%rax, %rax
	je	.L371
	movq	%r14, (%rbx)
.L364:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L394:
	.cfi_restore_state
	movl	$600, %r8d
	movl	$65, %edx
	movl	$279, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L392:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L395:
	.cfi_restore_state
	movl	$534, %r8d
	movl	$66, %edx
	movl	$256, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	.p2align 4,,10
	.p2align 3
.L371:
	movl	$605, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r14, %rdi
	call	CRYPTO_free@PLT
	xorl	%eax, %eax
	jmp	.L364
	.p2align 4,,10
	.p2align 3
.L389:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L393:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	movl	$534, %r8d
	movl	$66, %edx
	movl	$256, %esi
	movl	$16, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L364
	.cfi_endproc
.LFE844:
	.size	EC_KEY_priv2buf, .-EC_KEY_priv2buf
	.p2align 4
	.globl	EC_KEY_can_sign
	.type	EC_KEY_can_sign, @function
EC_KEY_can_sign:
.LFB845:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	testq	%rax, %rax
	je	.L399
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L399
	testb	$4, (%rax)
	sete	%al
	movzbl	%al, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L399:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE845:
	.size	EC_KEY_can_sign, .-EC_KEY_can_sign
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
