	.file	"v3_akey.c"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"keyid"
.LC1:
	.string	"always"
.LC2:
	.string	"issuer"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"../deps/openssl/openssl/crypto/x509v3/v3_akey.c"
	.section	.rodata.str1.1
.LC4:
	.string	"name="
	.text
	.p2align 4
	.type	v2i_AUTHORITY_KEYID, @function
v2i_AUTHORITY_KEYID:
.LFB1321:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	leaq	.LC0(%rip), %r14
	pushq	%r13
	.cfi_offset 13, -40
	xorl	%r13d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$24, %rsp
	movq	%rsi, -56(%rbp)
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L54:
	movq	16(%rax), %rsi
	movl	$1, %r12d
	testq	%rsi, %rsi
	je	.L4
	movl	$7, %ecx
	leaq	.LC1(%rip), %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	sete	%r12b
	addl	$1, %r12d
.L4:
	addl	$1, %r15d
.L2:
	movq	%rbx, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r15d
	jge	.L53
	movl	%r15d, %esi
	movq	%rbx, %rdi
	call	OPENSSL_sk_value@PLT
	movl	$6, %ecx
	movq	%r14, %rdi
	movq	8(%rax), %r8
	movq	%r8, %rsi
	repz cmpsb
	seta	%cl
	sbbb	$0, %cl
	testb	%cl, %cl
	je	.L54
	movl	$7, %ecx
	movq	%r8, %rsi
	leaq	.LC2(%rip), %rdi
	repz cmpsb
	seta	%cl
	sbbb	$0, %cl
	testb	%cl, %cl
	jne	.L5
	movq	16(%rax), %rsi
	movl	$1, %r13d
	testq	%rsi, %rsi
	je	.L4
	movl	$7, %ecx
	leaq	.LC1(%rip), %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	sete	%r13b
	addl	$1, %r13d
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L53:
	movq	-56(%rbp), %rax
	testq	%rax, %rax
	je	.L8
	movq	8(%rax), %r15
	testq	%r15, %r15
	je	.L55
	movl	$1, %eax
	xorl	%r14d, %r14d
	testb	%r12b, %r12b
	jne	.L56
.L10:
	testb	%r13b, %r13b
	setne	%dl
	testb	%al, %dl
	jne	.L31
	cmpb	$2, %r13b
	je	.L31
	call	AUTHORITY_KEYID_new@PLT
	xorl	%r15d, %r15d
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L29
	xorl	%ebx, %ebx
.L19:
	movq	%r14, %xmm0
	movq	%rbx, %xmm1
	movq	%r15, 16(%r12)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%r12)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L55:
	cmpl	$1, (%rax)
	je	.L57
.L8:
	movl	$103, %r8d
	leaq	.LC3(%rip), %rcx
	movl	$121, %edx
	xorl	%r12d, %r12d
	movl	$119, %esi
	movl	$34, %edi
	call	ERR_put_error@PLT
.L1:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	.cfi_restore_state
	movl	$94, %r8d
	leaq	.LC3(%rip), %rcx
	movl	$120, %edx
	movl	$119, %esi
	movl	$34, %edi
	movq	%rax, -56(%rbp)
	xorl	%r12d, %r12d
	call	ERR_put_error@PLT
	movq	-56(%rbp), %rax
	movl	$2, %edi
	leaq	.LC4(%rip), %rsi
	movq	8(%rax), %rdx
	xorl	%eax, %eax
	call	ERR_add_error_data@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L57:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	AUTHORITY_KEYID_new@PLT
	.p2align 4,,10
	.p2align 3
.L31:
	.cfi_restore_state
	movq	%r15, %rdi
	call	X509_get_issuer_name@PLT
	movq	%rax, %rdi
	call	X509_NAME_dup@PLT
	movq	%r15, %rdi
	movq	%rax, %r13
	call	X509_get_serialNumber@PLT
	movq	%rax, %rdi
	call	ASN1_INTEGER_dup@PLT
	movq	%rax, %r15
	testq	%r13, %r13
	je	.L32
	testq	%rax, %rax
	je	.L32
	call	AUTHORITY_KEYID_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L30
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L22
	call	GENERAL_NAME_new@PLT
	testq	%rax, %rax
	je	.L22
	movq	%rax, %rsi
	movq	%rbx, %rdi
	movq	%rax, -56(%rbp)
	call	OPENSSL_sk_push@PLT
	movq	-56(%rbp), %r9
	testl	%eax, %eax
	je	.L21
	movl	$4, (%r9)
	movq	%r13, 8(%r9)
	jmp	.L19
.L56:
	movl	$-1, %edx
	movl	$82, %esi
	movq	%r15, %rdi
	call	X509_get_ext_by_NID@PLT
	testl	%eax, %eax
	jns	.L11
.L13:
	movl	$1, %eax
	xorl	%r14d, %r14d
.L12:
	cmpb	$2, %r12b
	jne	.L10
	testb	%al, %al
	je	.L10
	movl	$115, %r8d
	leaq	.LC3(%rip), %rcx
	movl	$123, %edx
	xorl	%r12d, %r12d
	movl	$119, %esi
	movl	$34, %edi
	call	ERR_put_error@PLT
	jmp	.L1
.L22:
	xorl	%r9d, %r9d
.L21:
	movl	$138, %r8d
	leaq	.LC3(%rip), %rcx
	movl	$65, %edx
	movl	$119, %esi
	movl	$34, %edi
	movq	%r9, -56(%rbp)
	call	ERR_put_error@PLT
	movq	-56(%rbp), %r9
.L18:
	movq	%rbx, %rdi
	movq	%r9, -56(%rbp)
	xorl	%r12d, %r12d
	call	OPENSSL_sk_free@PLT
	movq	-56(%rbp), %r9
	movq	%r9, %rdi
	call	GENERAL_NAME_free@PLT
	movq	%r13, %rdi
	call	X509_NAME_free@PLT
	movq	%r15, %rdi
	call	ASN1_INTEGER_free@PLT
	movq	%r14, %rdi
	call	ASN1_OCTET_STRING_free@PLT
	jmp	.L1
.L11:
	movq	%r15, %rdi
	movl	%eax, %esi
	call	X509_get_ext@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L13
	call	X509V3_EXT_d2i@PLT
	testq	%rax, %rax
	movq	%rax, %r14
	sete	%al
	jmp	.L12
.L32:
	movl	$125, %r8d
	leaq	.LC3(%rip), %rcx
	movl	$122, %edx
	xorl	%ebx, %ebx
	movl	$119, %esi
	movl	$34, %edi
	call	ERR_put_error@PLT
	xorl	%r9d, %r9d
	jmp	.L18
.L30:
	xorl	%r9d, %r9d
	xorl	%ebx, %ebx
	jmp	.L18
.L29:
	xorl	%r9d, %r9d
	xorl	%ebx, %ebx
	xorl	%r13d, %r13d
	jmp	.L18
	.cfi_endproc
.LFE1321:
	.size	v2i_AUTHORITY_KEYID, .-v2i_AUTHORITY_KEYID
	.section	.rodata.str1.1
.LC5:
	.string	"serial"
	.text
	.p2align 4
	.type	i2v_AUTHORITY_KEYID, @function
i2v_AUTHORITY_KEYID:
.LFB1320:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	subq	$16, %rsp
	movq	(%rsi), %rax
	movq	%rdx, -24(%rbp)
	testq	%rax, %rax
	je	.L59
	movslq	(%rax), %rsi
	movq	8(%rax), %rdi
	call	OPENSSL_buf2hexstr@PLT
	leaq	-24(%rbp), %rdx
	leaq	.LC0(%rip), %rdi
	movq	%rax, %r12
	movq	%rax, %rsi
	call	X509V3_add_value@PLT
	movl	$46, %edx
	leaq	.LC3(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
.L59:
	movq	8(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L60
	movq	-24(%rbp), %rdx
	xorl	%edi, %edi
	call	i2v_GENERAL_NAMES@PLT
	movq	%rax, -24(%rbp)
.L60:
	movq	16(%rbx), %rax
	testq	%rax, %rax
	je	.L61
	movslq	(%rax), %rsi
	movq	8(%rax), %rdi
	call	OPENSSL_buf2hexstr@PLT
	leaq	-24(%rbp), %rdx
	leaq	.LC5(%rip), %rdi
	movq	%rax, %r12
	movq	%rax, %rsi
	call	X509V3_add_value@PLT
	movl	$53, %edx
	leaq	.LC3(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
.L61:
	movq	-24(%rbp), %rax
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1320:
	.size	i2v_AUTHORITY_KEYID, .-i2v_AUTHORITY_KEYID
	.globl	v3_akey_id
	.section	.data.rel.ro,"aw"
	.align 32
	.type	v3_akey_id, @object
	.size	v3_akey_id, 104
v3_akey_id:
	.long	90
	.long	4
	.quad	AUTHORITY_KEYID_it
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	i2v_AUTHORITY_KEYID
	.quad	v2i_AUTHORITY_KEYID
	.quad	0
	.quad	0
	.quad	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
