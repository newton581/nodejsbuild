	.file	"f_generic.c"
	.text
	.p2align 4
	.globl	gf_strong_reduce
	.type	gf_strong_reduce, @function
gf_strong_reduce:
.LFB87:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	8(%rdi), %ecx
	movl	12(%rdi), %esi
	pshufd	$27, 48(%rdi), %xmm1
	movl	4(%rdi), %r8d
	movl	60(%rdi), %r9d
	movl	%ecx, %eax
	andl	$268435455, %esi
	andl	$268435455, %ecx
	movdqu	44(%rdi), %xmm4
	shrl	$28, %eax
	shrl	$28, %r9d
	addl	%r9d, 32(%rdi)
	movdqa	.LC0(%rip), %xmm3
	addl	%eax, %esi
	movl	%r8d, %eax
	pshufd	$27, %xmm4, %xmm2
	movdqu	28(%rdi), %xmm5
	shrl	$28, %eax
	psrld	$28, %xmm2
	andl	$268435455, %r8d
	movdqu	12(%rdi), %xmm6
	addl	%eax, %ecx
	movl	(%rdi), %eax
	pand	%xmm3, %xmm1
	paddd	%xmm2, %xmm1
	pshufd	$27, %xmm5, %xmm2
	pshufd	$27, %xmm6, %xmm0
	movl	%eax, %r11d
	andl	$268435455, %eax
	pshufd	$27, %xmm1, %xmm1
	addl	%eax, %r9d
	shrl	$28, %r11d
	movd	%xmm1, %edx
	movaps	%xmm1, 48(%rdi)
	psrld	$28, %xmm2
	psrld	$28, %xmm0
	movq	%r9, %rax
	addl	%r11d, %r8d
	subq	$268435455, %r9
	pshufd	$27, 32(%rdi), %xmm1
	pand	%xmm3, %xmm1
	subl	$268435455, %eax
	sarq	$28, %r9
	paddd	%xmm2, %xmm1
	andl	$268435455, %eax
	leaq	-268435455(%r9,%r8), %r12
	pshufd	$27, %xmm1, %xmm1
	movq	%r12, %r8
	movd	%xmm1, %r13d
	movaps	%xmm1, 32(%rdi)
	pshufd	$27, 16(%rdi), %xmm1
	sarq	$28, %r8
	pand	%xmm3, %xmm1
	leaq	-268435455(%r8,%rcx), %rbx
	paddd	%xmm1, %xmm0
	movq	%rbx, %rcx
	pshufd	$27, %xmm0, %xmm0
	sarq	$28, %rcx
	movd	%xmm0, %r10d
	movaps	%xmm0, 16(%rdi)
	leaq	-268435455(%rcx,%rsi), %r11
	movl	%r10d, %ecx
	movq	%r11, %rsi
	sarq	$28, %rsi
	leaq	-268435455(%rsi,%rcx), %r10
	movl	20(%rdi), %ecx
	movq	%r10, %rsi
	sarq	$28, %rsi
	leaq	-268435455(%rsi,%rcx), %r9
	movl	24(%rdi), %ecx
	movq	%r9, %rsi
	sarq	$28, %rsi
	leaq	-268435455(%rsi,%rcx), %rsi
	movl	28(%rdi), %ecx
	movq	%rsi, -56(%rbp)
	sarq	$28, %rsi
	leaq	-268435455(%rsi,%rcx), %rcx
	movq	%rcx, %r15
	movq	%rcx, -64(%rbp)
	movl	%r13d, %ecx
	sarq	$28, %r15
	leaq	-268435454(%r15,%rcx), %rcx
	movq	%rcx, -72(%rbp)
	movq	%rcx, %r14
	movl	36(%rdi), %ecx
	sarq	$28, %r14
	leaq	-268435455(%r14,%rcx), %rcx
	movq	%rcx, -80(%rbp)
	movq	%rcx, %r8
	movl	40(%rdi), %ecx
	sarq	$28, %r8
	leaq	-268435455(%r8,%rcx), %rcx
	movq	%rcx, -88(%rbp)
	movq	%rcx, %r13
	movl	44(%rdi), %ecx
	sarq	$28, %r13
	leaq	-268435455(%r13,%rcx), %rcx
	movl	60(%rdi), %r13d
	movq	%rcx, -96(%rbp)
	sarq	$28, %rcx
	leaq	-268435455(%rcx,%rdx), %r8
	movq	%r8, %rdx
	sarq	$28, %rdx
	movq	%rdx, %rcx
	movl	52(%rdi), %edx
	leaq	-268435455(%rcx,%rdx), %rsi
	movq	%rsi, %rdx
	sarq	$28, %rdx
	movq	%rdx, %rcx
	movl	56(%rdi), %edx
	leaq	-268435455(%rcx,%rdx), %rcx
	movq	%rcx, %rdx
	sarq	$28, %rdx
	leaq	-268435455(%r13,%rdx), %r13
	movq	%r13, %rdx
	sarq	$28, %rdx
	movq	%rdx, %r15
	andl	$268435455, %edx
	andl	$268435455, %r12d
	andl	$268435455, %ebx
	movl	%edx, %r14d
	addq	%rdx, %r12
	addq	%rdx, %rbx
	andl	$268435455, %r11d
	addl	%eax, %r14d
	addq	%rdx, %rax
	addq	%rdx, %r11
	andl	$268435455, %r10d
	shrq	$28, %rax
	addq	%rdx, %r10
	andl	$268435455, %r9d
	andl	$268435455, %r14d
	addq	%r12, %rax
	addq	%rdx, %r9
	movl	%r14d, (%rdi)
	movl	%eax, %r12d
	shrq	$28, %rax
	addq	%rbx, %rax
	andl	$268435455, %r12d
	movl	%eax, %ebx
	shrq	$28, %rax
	movl	%r12d, 4(%rdi)
	addq	%r11, %rax
	andl	$268435455, %ebx
	movl	%eax, %r11d
	shrq	$28, %rax
	movl	%ebx, 8(%rdi)
	addq	%r10, %rax
	andl	$268435455, %r11d
	movl	%eax, %r10d
	shrq	$28, %rax
	movl	%r11d, 12(%rdi)
	addq	%r9, %rax
	movq	-56(%rbp), %r9
	andl	$268435455, %r10d
	movl	%eax, %ebx
	shrq	$28, %rax
	movl	%r10d, 16(%rdi)
	andl	$268435455, %r9d
	andl	$268435455, %ebx
	addq	%rdx, %r9
	movl	%ebx, 20(%rdi)
	addq	%r9, %rax
	andl	$268435454, %r15d
	andl	$268435455, %r8d
	movl	%eax, %r9d
	andl	$268435455, %r9d
	movl	%r9d, 24(%rdi)
	movq	%rax, %r9
	movq	-64(%rbp), %rax
	shrq	$28, %r9
	andl	$268435455, %eax
	addq	%rdx, %rax
	addq	%r9, %rax
	movl	%eax, %r9d
	andl	$268435455, %r9d
	movl	%r9d, 28(%rdi)
	movq	%rax, %r9
	movq	-72(%rbp), %rax
	shrq	$28, %r9
	andl	$268435455, %eax
	addq	%r15, %rax
	addq	%r9, %rax
	movl	%eax, %r9d
	andl	$268435455, %r9d
	movl	%r9d, 32(%rdi)
	movq	%rax, %r9
	movq	-80(%rbp), %rax
	shrq	$28, %r9
	andl	$268435455, %eax
	addq	%rdx, %rax
	addq	%r9, %rax
	movl	%eax, %r9d
	andl	$268435455, %r9d
	movl	%r9d, 36(%rdi)
	movq	%rax, %r9
	movq	-88(%rbp), %rax
	shrq	$28, %r9
	andl	$268435455, %eax
	addq	%rdx, %rax
	addq	%r9, %rax
	movl	%eax, %r9d
	andl	$268435455, %r9d
	movl	%r9d, 40(%rdi)
	movq	%rax, %r9
	movq	-96(%rbp), %rax
	shrq	$28, %r9
	andl	$268435455, %eax
	addq	%rdx, %rax
	addq	%r9, %rax
	movl	%eax, %r9d
	shrq	$28, %rax
	andl	$268435455, %r9d
	movl	%r9d, 44(%rdi)
	movq	%rax, %r9
	leaq	(%r8,%rdx), %rax
	addq	%r9, %rax
	movl	%eax, %r8d
	andl	$268435455, %r8d
	shrq	$28, %rax
	andl	$268435455, %esi
	andl	$268435455, %ecx
	movl	%r8d, 48(%rdi)
	movq	%rax, %r8
	leaq	(%rsi,%rdx), %rax
	andl	$268435455, %r13d
	addq	%r8, %rax
	movl	%eax, %esi
	shrq	$28, %rax
	andl	$268435455, %esi
	movl	%esi, 52(%rdi)
	movq	%rax, %rsi
	leaq	(%rcx,%rdx), %rax
	addq	%r13, %rdx
	addq	%rsi, %rax
	movl	%eax, %ecx
	shrq	$28, %rax
	addq	%rdx, %rax
	andl	$268435455, %ecx
	andl	$268435455, %eax
	movl	%ecx, 56(%rdi)
	movl	%eax, 60(%rdi)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE87:
	.size	gf_strong_reduce, .-gf_strong_reduce
	.p2align 4
	.globl	gf_serialize
	.type	gf_serialize, @function
gf_serialize:
.LFB83:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	-96(%rbp), %rdi
	subq	$88, %rsp
	movdqa	(%rsi), %xmm0
	movdqa	16(%rsi), %xmm1
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movdqa	32(%rsi), %xmm2
	movdqa	48(%rsi), %xmm3
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm1, -80(%rbp)
	movaps	%xmm2, -64(%rbp)
	movaps	%xmm3, -48(%rbp)
	call	gf_strong_reduce
	movq	%rbx, %rdi
	leaq	56(%rbx), %r8
	xorl	%eax, %eax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L8:
	cmpl	$7, %ecx
	ja	.L5
	cmpl	$15, %edx
	ja	.L5
	movl	%edx, %esi
	addq	$1, %rdi
	addl	$1, %edx
	movl	-96(%rbp,%rsi,4), %esi
	salq	%cl, %rsi
	addl	$20, %ecx
	orq	%rsi, %rax
	movb	%al, -1(%rdi)
	shrq	$8, %rax
	cmpq	%r8, %rdi
	jne	.L8
.L4:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L17
	addq	$88, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	.cfi_restore_state
	movb	%al, (%rdi)
	addq	$1, %rdi
	subl	$8, %ecx
	shrq	$8, %rax
	cmpq	%rdi, %r8
	jne	.L8
	jmp	.L4
.L17:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE83:
	.size	gf_serialize, .-gf_serialize
	.p2align 4
	.globl	gf_lobit
	.type	gf_lobit, @function
gf_lobit:
.LFB85:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$80, %rsp
	movdqa	(%rdi), %xmm0
	movdqa	16(%rdi), %xmm1
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movdqa	32(%rdi), %xmm2
	movdqa	48(%rdi), %xmm3
	leaq	-80(%rbp), %rdi
	movaps	%xmm0, -80(%rbp)
	movaps	%xmm1, -64(%rbp)
	movaps	%xmm2, -48(%rbp)
	movaps	%xmm3, -32(%rbp)
	call	gf_strong_reduce
	movl	-80(%rbp), %eax
	andl	$1, %eax
	negl	%eax
	movq	-8(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L21
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L21:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE85:
	.size	gf_lobit, .-gf_lobit
	.p2align 4
	.globl	gf_hibit
	.type	gf_hibit, @function
gf_hibit:
.LFB84:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$80, %rsp
	movdqa	32(%rdi), %xmm1
	movdqa	(%rdi), %xmm0
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movdqa	.LC0(%rip), %xmm3
	pslld	$1, %xmm1
	pslld	$1, %xmm0
	movaps	%xmm1, -48(%rbp)
	movdqa	48(%rdi), %xmm1
	movaps	%xmm0, -80(%rbp)
	movl	-72(%rbp), %ecx
	movdqa	16(%rdi), %xmm0
	leaq	-80(%rbp), %rdi
	pslld	$1, %xmm1
	movl	-68(%rbp), %edx
	movaps	%xmm1, -32(%rbp)
	movdqu	-36(%rbp), %xmm4
	movl	-20(%rbp), %esi
	movl	%ecx, %eax
	shrl	$28, %eax
	pslld	$1, %xmm0
	pshufd	$27, %xmm1, %xmm1
	andl	$268435455, %edx
	pshufd	$27, %xmm4, %xmm2
	movaps	%xmm0, -64(%rbp)
	shrl	$28, %esi
	addl	%eax, %edx
	addl	%esi, -48(%rbp)
	pand	%xmm3, %xmm1
	movdqu	-52(%rbp), %xmm5
	andl	$268435455, %ecx
	psrld	$28, %xmm2
	movdqu	-68(%rbp), %xmm6
	movl	%edx, -68(%rbp)
	pshufd	$27, %xmm0, %xmm0
	movl	-76(%rbp), %edx
	paddd	%xmm2, %xmm1
	pshufd	$27, %xmm5, %xmm2
	pand	%xmm3, %xmm0
	pshufd	$27, %xmm1, %xmm1
	psrld	$28, %xmm2
	movaps	%xmm1, -32(%rbp)
	movl	%edx, %eax
	pshufd	$27, -48(%rbp), %xmm1
	pand	%xmm3, %xmm1
	shrl	$28, %eax
	paddd	%xmm2, %xmm1
	andl	$268435455, %edx
	addl	%eax, %ecx
	pshufd	$27, %xmm1, %xmm1
	movl	-80(%rbp), %eax
	movaps	%xmm1, -48(%rbp)
	pshufd	$27, %xmm6, %xmm1
	movl	%ecx, -72(%rbp)
	psrld	$28, %xmm1
	movl	%eax, %ecx
	andl	$268435455, %eax
	shrl	$28, %ecx
	paddd	%xmm1, %xmm0
	addl	%esi, %eax
	addl	%ecx, %edx
	pshufd	$27, %xmm0, %xmm0
	movl	%eax, -80(%rbp)
	movl	%edx, -76(%rbp)
	movaps	%xmm0, -64(%rbp)
	call	gf_strong_reduce
	movl	-80(%rbp), %eax
	andl	$1, %eax
	negl	%eax
	movq	-8(%rbp), %rsi
	xorq	%fs:40, %rsi
	jne	.L25
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L25:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE84:
	.size	gf_hibit, .-gf_hibit
	.p2align 4
	.globl	gf_deserialize
	.type	gf_deserialize, @function
gf_deserialize:
.LFB86:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%ecx, %r10d
	xorl	%r8d, %r8d
	movl	$268435455, %r9d
	xorl	%r11d, %r11d
	notl	%r10d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	xorl	%edx, %edx
	pushq	%r14
	.cfi_offset 14, -32
	leaq	MODULUS(%rip), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	xorl	%esi, %esi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	xorl	%ebx, %ebx
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$27, %edx
	ja	.L39
	.p2align 4,,10
	.p2align 3
.L48:
	cmpl	$55, %esi
	ja	.L39
	movl	%esi, %edi
	addl	$1, %esi
	addq	%r13, %rdi
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L28:
	salq	%cl, %rax
	addq	$1, %rdi
	orq	%rax, %rbx
	leal	1(%rsi), %eax
	cmpl	$27, %edx
	ja	.L39
	cmpl	$55, %esi
	ja	.L39
	movl	%eax, %esi
.L30:
	movl	%edx, %ecx
	movzbl	(%rdi), %eax
	addl	$8, %edx
	cmpl	$56, %esi
	jne	.L28
	andl	%r10d, %eax
	movzbl	%al, %eax
	salq	%cl, %rax
	orq	%rbx, %rax
	movq	%rax, %rbx
	shrq	$28, %rbx
	cmpq	$15, %r8
	je	.L32
.L49:
	andl	$268435455, %eax
	subl	$28, %edx
	addq	%rax, %r11
	movl	%eax, (%r12,%r8,4)
	addq	$1, %r8
	subq	%r9, %r11
	movl	(%r14,%r8,4), %r9d
	sarq	$32, %r11
	cmpl	$27, %edx
	jbe	.L48
.L39:
	movq	%rbx, %rax
	movq	%rax, %rbx
	shrq	$28, %rbx
	cmpq	$15, %r8
	jne	.L49
	.p2align 4,,10
	.p2align 3
.L32:
	movl	%eax, 60(%r12)
	movl	%eax, %eax
	movl	$-1, %ecx
	addq	%rax, %r11
	subq	%r9, %r11
	sarq	$32, %r11
	testl	%r15d, %r15d
	je	.L50
.L34:
	leal	-1(%r11), %eax
	movl	%ebx, %edx
	notl	%r11d
	subl	$1, %ebx
	andl	%r11d, %eax
	notl	%edx
	sarl	$31, %eax
	andl	%edx, %ebx
	sarl	$31, %ebx
	notl	%eax
	andl	%ebx, %eax
	andl	%ecx, %eax
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L51
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L50:
	.cfi_restore_state
	movdqa	(%r12), %xmm0
	movdqa	48(%r12), %xmm2
	leaq	-128(%rbp), %rdi
	movq	%r11, -136(%rbp)
	movdqa	.LC0(%rip), %xmm3
	movdqa	16(%r12), %xmm1
	pslld	$1, %xmm0
	pslld	$1, %xmm2
	movaps	%xmm0, -128(%rbp)
	movl	-120(%rbp), %esi
	movl	-116(%rbp), %eax
	pslld	$1, %xmm1
	movdqa	32(%r12), %xmm0
	movaps	%xmm2, -80(%rbp)
	pshufd	$27, %xmm2, %xmm2
	movl	-68(%rbp), %edx
	pand	%xmm3, %xmm2
	movl	%esi, %ecx
	movaps	%xmm1, -112(%rbp)
	andl	$268435455, %eax
	shrl	$28, %edx
	pslld	$1, %xmm0
	shrl	$28, %ecx
	movdqu	-116(%rbp), %xmm6
	movaps	%xmm0, -96(%rbp)
	movdqu	-84(%rbp), %xmm4
	addl	%ecx, %eax
	movl	-124(%rbp), %ecx
	addl	%edx, -96(%rbp)
	movdqu	-100(%rbp), %xmm5
	andl	$268435455, %esi
	pshufd	$27, %xmm1, %xmm1
	pshufd	$27, %xmm4, %xmm0
	movl	%eax, -116(%rbp)
	movl	%ecx, %eax
	pand	%xmm3, %xmm1
	psrld	$28, %xmm0
	shrl	$28, %eax
	andl	$268435455, %ecx
	paddd	%xmm2, %xmm0
	pshufd	$27, -96(%rbp), %xmm2
	pand	%xmm3, %xmm2
	addl	%eax, %esi
	pshufd	$27, %xmm0, %xmm0
	movl	-128(%rbp), %eax
	movl	%esi, -120(%rbp)
	movaps	%xmm0, -80(%rbp)
	pshufd	$27, %xmm5, %xmm0
	psrld	$28, %xmm0
	movl	%eax, %esi
	andl	$268435455, %eax
	paddd	%xmm2, %xmm0
	shrl	$28, %esi
	addl	%edx, %eax
	pshufd	$27, %xmm0, %xmm0
	addl	%esi, %ecx
	movl	%eax, -128(%rbp)
	movaps	%xmm0, -96(%rbp)
	pshufd	$27, %xmm6, %xmm0
	psrld	$28, %xmm0
	movl	%ecx, -124(%rbp)
	paddd	%xmm1, %xmm0
	pshufd	$27, %xmm0, %xmm0
	movaps	%xmm0, -112(%rbp)
	call	gf_strong_reduce
	movl	-128(%rbp), %eax
	movq	-136(%rbp), %r11
	andl	$1, %eax
	leal	-1(%rax), %ecx
	jmp	.L34
.L51:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE86:
	.size	gf_deserialize, .-gf_deserialize
	.p2align 4
	.globl	gf_sub
	.type	gf_sub, @function
gf_sub:
.LFB88:
	.cfi_startproc
	endbr64
	movdqa	(%rsi), %xmm0
	psubd	(%rdx), %xmm0
	movaps	%xmm0, (%rdi)
	movdqa	16(%rsi), %xmm0
	psubd	16(%rdx), %xmm0
	movaps	%xmm0, 16(%rdi)
	movdqa	32(%rsi), %xmm0
	psubd	32(%rdx), %xmm0
	movaps	%xmm0, 32(%rdi)
	movdqa	48(%rsi), %xmm0
	psubd	48(%rdx), %xmm0
	addl	$536870910, (%rdi)
	movaps	%xmm0, 48(%rdi)
	addl	$536870910, 4(%rdi)
	addl	$536870910, 8(%rdi)
	addl	$536870910, 12(%rdi)
	addl	$536870910, 16(%rdi)
	addl	$536870910, 20(%rdi)
	addl	$536870910, 24(%rdi)
	addl	$536870910, 28(%rdi)
	addl	$536870908, 32(%rdi)
	addl	$536870910, 36(%rdi)
	addl	$536870910, 40(%rdi)
	movl	8(%rdi), %edx
	pshufd	$27, 16(%rdi), %xmm0
	addl	$536870910, 44(%rdi)
	movl	12(%rdi), %ecx
	addl	$536870910, 48(%rdi)
	movl	%edx, %eax
	movdqa	.LC0(%rip), %xmm3
	andl	$268435455, %edx
	addl	$536870910, 52(%rdi)
	shrl	$28, %eax
	andl	$268435455, %ecx
	movdqu	12(%rdi), %xmm6
	addl	$536870910, 56(%rdi)
	movdqu	44(%rdi), %xmm4
	addl	%eax, %ecx
	pand	%xmm3, %xmm0
	addl	$536870910, 60(%rdi)
	movl	60(%rdi), %esi
	pshufd	$27, 48(%rdi), %xmm1
	pand	%xmm3, %xmm1
	pshufd	$27, %xmm4, %xmm2
	movl	%ecx, 12(%rdi)
	movl	4(%rdi), %ecx
	shrl	$28, %esi
	psrld	$28, %xmm2
	addl	%esi, 32(%rdi)
	movdqu	28(%rdi), %xmm5
	paddd	%xmm2, %xmm1
	movl	%ecx, %eax
	andl	$268435455, %ecx
	pshufd	$27, %xmm1, %xmm1
	pshufd	$27, %xmm5, %xmm2
	shrl	$28, %eax
	movaps	%xmm1, 48(%rdi)
	psrld	$28, %xmm2
	pshufd	$27, 32(%rdi), %xmm1
	pand	%xmm3, %xmm1
	paddd	%xmm2, %xmm1
	addl	%eax, %edx
	movl	(%rdi), %eax
	pshufd	$27, %xmm1, %xmm1
	movl	%edx, 8(%rdi)
	movaps	%xmm1, 32(%rdi)
	pshufd	$27, %xmm6, %xmm1
	movl	%eax, %edx
	andl	$268435455, %eax
	psrld	$28, %xmm1
	shrl	$28, %edx
	addl	%esi, %eax
	paddd	%xmm1, %xmm0
	addl	%edx, %ecx
	movl	%eax, (%rdi)
	pshufd	$27, %xmm0, %xmm0
	movl	%ecx, 4(%rdi)
	movaps	%xmm0, 16(%rdi)
	ret
	.cfi_endproc
.LFE88:
	.size	gf_sub, .-gf_sub
	.p2align 4
	.globl	gf_add
	.type	gf_add, @function
gf_add:
.LFB89:
	.cfi_startproc
	endbr64
	movdqa	(%rsi), %xmm0
	paddd	(%rdx), %xmm0
	movdqa	.LC0(%rip), %xmm3
	movaps	%xmm0, (%rdi)
	movdqa	16(%rsi), %xmm0
	paddd	16(%rdx), %xmm0
	movl	12(%rdi), %ecx
	movaps	%xmm0, 16(%rdi)
	movdqa	32(%rsi), %xmm1
	paddd	32(%rdx), %xmm1
	pshufd	$27, %xmm0, %xmm0
	andl	$268435455, %ecx
	movdqu	12(%rdi), %xmm6
	pand	%xmm3, %xmm0
	movaps	%xmm1, 32(%rdi)
	movdqa	48(%rsi), %xmm1
	paddd	48(%rdx), %xmm1
	movl	8(%rdi), %edx
	movaps	%xmm1, 48(%rdi)
	movdqu	44(%rdi), %xmm4
	movl	60(%rdi), %esi
	pshufd	$27, %xmm1, %xmm1
	movl	%edx, %eax
	pand	%xmm3, %xmm1
	andl	$268435455, %edx
	shrl	$28, %eax
	pshufd	$27, %xmm4, %xmm2
	shrl	$28, %esi
	addl	%esi, 32(%rdi)
	addl	%eax, %ecx
	movdqu	28(%rdi), %xmm5
	psrld	$28, %xmm2
	paddd	%xmm2, %xmm1
	movl	%ecx, 12(%rdi)
	movl	4(%rdi), %ecx
	pshufd	$27, %xmm1, %xmm1
	pshufd	$27, %xmm5, %xmm2
	movl	%ecx, %eax
	movaps	%xmm1, 48(%rdi)
	psrld	$28, %xmm2
	pshufd	$27, 32(%rdi), %xmm1
	pand	%xmm3, %xmm1
	shrl	$28, %eax
	andl	$268435455, %ecx
	paddd	%xmm2, %xmm1
	addl	%eax, %edx
	movl	(%rdi), %eax
	pshufd	$27, %xmm1, %xmm1
	movl	%edx, 8(%rdi)
	movaps	%xmm1, 32(%rdi)
	pshufd	$27, %xmm6, %xmm1
	movl	%eax, %edx
	andl	$268435455, %eax
	psrld	$28, %xmm1
	shrl	$28, %edx
	addl	%esi, %eax
	paddd	%xmm1, %xmm0
	addl	%edx, %ecx
	movl	%eax, (%rdi)
	pshufd	$27, %xmm0, %xmm0
	movl	%ecx, 4(%rdi)
	movaps	%xmm0, 16(%rdi)
	ret
	.cfi_endproc
.LFE89:
	.size	gf_add, .-gf_add
	.p2align 4
	.globl	gf_eq
	.type	gf_eq, @function
gf_eq:
.LFB90:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$80, %rsp
	movdqa	(%rdi), %xmm0
	psubd	(%rsi), %xmm0
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movd	%xmm0, %eax
	movdqa	16(%rdi), %xmm0
	psubd	16(%rsi), %xmm0
	addl	$536870910, -76(%rbp)
	addl	$536870910, %eax
	movaps	%xmm0, -64(%rbp)
	movdqa	32(%rdi), %xmm0
	psubd	32(%rsi), %xmm0
	addl	$536870910, -72(%rbp)
	movaps	%xmm0, -48(%rbp)
	movdqa	48(%rdi), %xmm0
	psubd	48(%rsi), %xmm0
	addl	$536870910, -68(%rbp)
	addl	$536870910, -64(%rbp)
	addl	$536870910, -60(%rbp)
	addl	$536870910, -56(%rbp)
	addl	$536870910, -52(%rbp)
	addl	$536870908, -48(%rbp)
	movaps	%xmm0, -32(%rbp)
	addl	$536870910, -44(%rbp)
	movl	-72(%rbp), %edx
	pshufd	$27, -64(%rbp), %xmm0
	addl	$536870910, -36(%rbp)
	movdqa	.LC0(%rip), %xmm3
	addl	$536870910, -32(%rbp)
	movl	-68(%rbp), %ecx
	movl	%edx, %edi
	andl	$268435455, %edx
	addl	$536870910, -28(%rbp)
	shrl	$28, %edi
	movdqu	-68(%rbp), %xmm6
	pand	%xmm3, %xmm0
	addl	$536870910, -24(%rbp)
	movdqu	-36(%rbp), %xmm4
	andl	$268435455, %ecx
	addl	$536870910, -20(%rbp)
	movl	-20(%rbp), %esi
	pshufd	$27, -32(%rbp), %xmm1
	pand	%xmm3, %xmm1
	pshufd	$27, %xmm4, %xmm2
	addl	$536870910, -40(%rbp)
	addl	%edi, %ecx
	shrl	$28, %esi
	psrld	$28, %xmm2
	addl	%esi, -48(%rbp)
	movdqu	-52(%rbp), %xmm5
	paddd	%xmm2, %xmm1
	movl	%ecx, -68(%rbp)
	movl	-76(%rbp), %ecx
	pshufd	$27, %xmm1, %xmm1
	pshufd	$27, %xmm5, %xmm2
	movaps	%xmm1, -32(%rbp)
	psrld	$28, %xmm2
	pshufd	$27, -48(%rbp), %xmm1
	pand	%xmm3, %xmm1
	paddd	%xmm2, %xmm1
	movl	%ecx, %edi
	andl	$268435455, %ecx
	shrl	$28, %edi
	pshufd	$27, %xmm1, %xmm1
	addl	%edi, %edx
	movaps	%xmm1, -48(%rbp)
	pshufd	$27, %xmm6, %xmm1
	leaq	-80(%rbp), %rdi
	psrld	$28, %xmm1
	movl	%edx, -72(%rbp)
	movl	%eax, %edx
	andl	$268435455, %eax
	shrl	$28, %edx
	paddd	%xmm1, %xmm0
	addl	%esi, %eax
	addl	%edx, %ecx
	pshufd	$27, %xmm0, %xmm0
	movl	%ecx, -76(%rbp)
	movaps	%xmm0, -64(%rbp)
	movl	%eax, -80(%rbp)
	call	gf_strong_reduce
	movdqa	-48(%rbp), %xmm0
	movdqa	-64(%rbp), %xmm1
	por	-32(%rbp), %xmm0
	por	-80(%rbp), %xmm1
	por	%xmm1, %xmm0
	movdqa	%xmm0, %xmm1
	psrldq	$8, %xmm1
	por	%xmm1, %xmm0
	movdqa	%xmm0, %xmm1
	psrldq	$4, %xmm1
	por	%xmm1, %xmm0
	movd	%xmm0, %edx
	movd	%xmm0, %eax
	subl	$1, %edx
	notl	%eax
	andl	%eax, %edx
	movl	%edx, %eax
	sarl	$31, %eax
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L57
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L57:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE90:
	.size	gf_eq, .-gf_eq
	.p2align 4
	.globl	gf_isr
	.type	gf_isr, @function
gf_isr:
.LFB91:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-192(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-256(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-320(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-128(%rbp), %rbx
	subq	$296, %rsp
	movq	%rdi, -328(%rbp)
	movq	%r13, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	gf_sqr@PLT
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	gf_mul@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	gf_sqr@PLT
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	gf_mul@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	gf_sqr@PLT
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	gf_sqr@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	gf_sqr@PLT
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	gf_mul@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	gf_sqr@PLT
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	gf_sqr@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	gf_sqr@PLT
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	gf_mul@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	gf_sqr@PLT
	movl	$4, %eax
	.p2align 4,,10
	.p2align 3
.L59:
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movl	%eax, -332(%rbp)
	call	gf_sqr@PLT
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	gf_sqr@PLT
	movl	-332(%rbp), %eax
	subl	$1, %eax
	jne	.L59
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	gf_mul@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	gf_sqr@PLT
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	gf_mul@PLT
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	gf_sqr@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	gf_sqr@PLT
	movl	$8, %eax
	.p2align 4,,10
	.p2align 3
.L60:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movl	%eax, -332(%rbp)
	call	gf_sqr@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	gf_sqr@PLT
	movl	-332(%rbp), %eax
	subl	$1, %eax
	jne	.L60
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	gf_mul@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	gf_sqr@PLT
	movl	$18, %eax
	.p2align 4,,10
	.p2align 3
.L61:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movl	%eax, -332(%rbp)
	call	gf_sqr@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	gf_sqr@PLT
	movl	-332(%rbp), %eax
	subl	$1, %eax
	jne	.L61
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	gf_mul@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	gf_sqr@PLT
	movl	$18, %eax
	.p2align 4,,10
	.p2align 3
.L62:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movl	%eax, -332(%rbp)
	call	gf_sqr@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	gf_sqr@PLT
	movl	-332(%rbp), %eax
	subl	$1, %eax
	jne	.L62
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	gf_mul@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	gf_sqr@PLT
	movl	$55, %eax
	.p2align 4,,10
	.p2align 3
.L63:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movl	%eax, -332(%rbp)
	call	gf_sqr@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	gf_sqr@PLT
	movl	-332(%rbp), %eax
	subl	$1, %eax
	jne	.L63
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	gf_mul@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	gf_sqr@PLT
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	gf_mul@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	gf_sqr@PLT
	movl	$111, %eax
	.p2align 4,,10
	.p2align 3
.L64:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movl	%eax, -332(%rbp)
	call	gf_sqr@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	gf_sqr@PLT
	movl	-332(%rbp), %eax
	subl	$1, %eax
	jne	.L64
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	gf_mul@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	gf_sqr@PLT
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	gf_mul@PLT
	movq	-328(%rbp), %rax
	movq	%r12, %rdi
	movdqa	-256(%rbp), %xmm0
	movdqa	-240(%rbp), %xmm1
	movdqa	-224(%rbp), %xmm2
	leaq	ONE(%rip), %rsi
	movdqa	-208(%rbp), %xmm3
	movaps	%xmm0, (%rax)
	movaps	%xmm1, 16(%rax)
	movaps	%xmm2, 32(%rax)
	movaps	%xmm3, 48(%rax)
	call	gf_eq
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L73
	addq	$296, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L73:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE91:
	.size	gf_isr, .-gf_isr
	.section	.rodata
	.align 32
	.type	MODULUS, @object
	.size	MODULUS, 64
MODULUS:
	.long	268435455
	.long	268435455
	.long	268435455
	.long	268435455
	.long	268435455
	.long	268435455
	.long	268435455
	.long	268435455
	.long	268435454
	.long	268435455
	.long	268435455
	.long	268435455
	.long	268435455
	.long	268435455
	.long	268435455
	.long	268435455
	.align 32
	.type	ONE, @object
	.size	ONE, 64
ONE:
	.long	1
	.zero	60
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	268435455
	.long	268435455
	.long	268435455
	.long	268435455
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
