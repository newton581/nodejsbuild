	.file	"pem_pk8.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/pem/pem_pk8.c"
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"ENCRYPTED PRIVATE KEY"
.LC2:
	.string	"PRIVATE KEY"
	.text
	.p2align 4
	.type	do_pk8pkey, @function
do_pk8pkey:
.LFB807:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r9, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%rsi, %rdi
	pushq	%r12
	.cfi_offset 12, -48
	movl	%ecx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$1080, %rsp
	movq	24(%rbp), %rax
	movq	%rax, -1096(%rbp)
	movq	32(%rbp), %rax
	movq	%rax, -1104(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	EVP_PKEY2PKCS8@PLT
	testq	%rax, %rax
	je	.L21
	movq	%rax, %r10
	testq	%r14, %r14
	jne	.L17
	cmpl	$-1, %r12d
	jne	.L17
	testl	%ebx, %ebx
	jne	.L22
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%rax, %rcx
	pushq	$0
	movq	i2d_PKCS8_PRIV_KEY_INFO@GOTPCREL(%rip), %rdi
	movq	%r13, %rdx
	leaq	.LC2(%rip), %rsi
	pushq	$0
	pushq	$0
	movq	%rax, -1096(%rbp)
	call	PEM_ASN1_write_bio@PLT
	movq	-1096(%rbp), %r10
	addq	$32, %rsp
	movl	%eax, %r12d
.L13:
	movq	%r10, %rdi
	call	PKCS8_PRIV_KEY_INFO_free@PLT
.L1:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L23
	leaq	-40(%rbp), %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	.cfi_restore_state
	testq	%r15, %r15
	je	.L24
	pushq	%r10
	movl	16(%rbp), %ecx
	movq	%r15, %rdx
	movq	%r14, %rsi
	pushq	$0
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	%r12d, %edi
	movq	%r10, -1096(%rbp)
	call	PKCS8_encrypt@PLT
	movq	-1096(%rbp), %r10
	movq	%rax, %r14
	popq	%rax
	popq	%rdx
.L14:
	movq	%r10, %rdi
	xorl	%r12d, %r12d
	call	PKCS8_PRIV_KEY_INFO_free@PLT
	testq	%r14, %r14
	je	.L1
	testl	%ebx, %ebx
	jne	.L25
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	pushq	$0
	movq	i2d_X509_SIG@GOTPCREL(%rip), %rdi
	movq	%r13, %rdx
	leaq	.LC1(%rip), %rsi
	pushq	$0
	pushq	$0
	call	PEM_ASN1_write_bio@PLT
	addq	$32, %rsp
	movl	%eax, %r12d
.L11:
	movq	%r14, %rdi
	call	X509_SIG_free@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L24:
	leaq	-1088(%rbp), %r11
	cmpq	$0, -1096(%rbp)
	movq	%r10, -1112(%rbp)
	movl	$1, %edx
	movq	-1104(%rbp), %rcx
	movl	$1024, %esi
	movq	%r11, %rdi
	je	.L26
	movq	%r11, -1104(%rbp)
	movq	-1096(%rbp), %rax
	call	*%rax
	movq	-1112(%rbp), %r10
	movq	-1104(%rbp), %r11
	movl	%eax, %r15d
.L8:
	testl	%r15d, %r15d
	jle	.L27
	pushq	%r10
	movl	%r15d, %ecx
	movq	%r11, %rdx
	movq	%r14, %rsi
	pushq	$0
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	%r12d, %edi
	movq	%r10, -1104(%rbp)
	movq	%r11, -1096(%rbp)
	call	PKCS8_encrypt@PLT
	movq	-1096(%rbp), %r11
	movslq	%r15d, %rsi
	movq	%rax, %r14
	movq	%r11, %rdi
	call	OPENSSL_cleanse@PLT
	popq	%rcx
	movq	-1104(%rbp), %r10
	popq	%rsi
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L25:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	i2d_PKCS8_bio@PLT
	movl	%eax, %r12d
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L22:
	movq	%rax, %rsi
	movq	%r13, %rdi
	movq	%rax, -1096(%rbp)
	call	i2d_PKCS8_PRIV_KEY_INFO_bio@PLT
	movq	-1096(%rbp), %r10
	movl	%eax, %r12d
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L21:
	movl	$73, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$115, %edx
	xorl	%r12d, %r12d
	movl	$126, %esi
	movl	$9, %edi
	call	ERR_put_error@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L26:
	movq	%r11, -1096(%rbp)
	call	PEM_def_callback@PLT
	movq	-1096(%rbp), %r11
	movq	-1112(%rbp), %r10
	movl	%eax, %r15d
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L27:
	movl	$83, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$111, %edx
	xorl	%r12d, %r12d
	movl	$126, %esi
	movl	$9, %edi
	movq	%r10, -1096(%rbp)
	call	ERR_put_error@PLT
	movq	-1096(%rbp), %r10
	movq	%r10, %rdi
	call	PKCS8_PRIV_KEY_INFO_free@PLT
	jmp	.L1
.L23:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE807:
	.size	do_pk8pkey, .-do_pk8pkey
	.p2align 4
	.globl	PEM_write_bio_PKCS8PrivateKey_nid
	.type	PEM_write_bio_PKCS8PrivateKey_nid, @function
PEM_write_bio_PKCS8PrivateKey_nid:
.LFB803:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$8, %rsp
	pushq	16(%rbp)
	pushq	%r9
	movq	%rcx, %r9
	movl	%edx, %ecx
	xorl	%edx, %edx
	pushq	%r8
	xorl	%r8d, %r8d
	call	do_pk8pkey
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE803:
	.size	PEM_write_bio_PKCS8PrivateKey_nid, .-PEM_write_bio_PKCS8PrivateKey_nid
	.p2align 4
	.globl	PEM_write_bio_PKCS8PrivateKey
	.type	PEM_write_bio_PKCS8PrivateKey, @function
PEM_write_bio_PKCS8PrivateKey:
.LFB804:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$8, %rsp
	pushq	16(%rbp)
	pushq	%r9
	movq	%rcx, %r9
	movl	$-1, %ecx
	pushq	%r8
	movq	%rdx, %r8
	xorl	%edx, %edx
	call	do_pk8pkey
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE804:
	.size	PEM_write_bio_PKCS8PrivateKey, .-PEM_write_bio_PKCS8PrivateKey
	.p2align 4
	.globl	i2d_PKCS8PrivateKey_bio
	.type	i2d_PKCS8PrivateKey_bio, @function
i2d_PKCS8PrivateKey_bio:
.LFB805:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$8, %rsp
	pushq	16(%rbp)
	pushq	%r9
	movq	%rcx, %r9
	movl	$-1, %ecx
	pushq	%r8
	movq	%rdx, %r8
	movl	$1, %edx
	call	do_pk8pkey
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE805:
	.size	i2d_PKCS8PrivateKey_bio, .-i2d_PKCS8PrivateKey_bio
	.p2align 4
	.globl	i2d_PKCS8PrivateKey_nid_bio
	.type	i2d_PKCS8PrivateKey_nid_bio, @function
i2d_PKCS8PrivateKey_nid_bio:
.LFB806:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$8, %rsp
	pushq	16(%rbp)
	pushq	%r9
	movq	%rcx, %r9
	movl	%edx, %ecx
	movl	$1, %edx
	pushq	%r8
	xorl	%r8d, %r8d
	call	do_pk8pkey
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE806:
	.size	i2d_PKCS8PrivateKey_nid_bio, .-i2d_PKCS8PrivateKey_nid_bio
	.p2align 4
	.globl	d2i_PKCS8PrivateKey_bio
	.type	d2i_PKCS8PrivateKey_bio, @function
d2i_PKCS8PrivateKey_bio:
.LFB808:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	xorl	%esi, %esi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$1048, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	d2i_PKCS8_bio@PLT
	testq	%rax, %rax
	je	.L54
	leaq	-1088(%rbp), %r15
	movq	%rax, %r13
	movq	%r14, %rcx
	xorl	%edx, %edx
	movl	$1024, %esi
	movq	%r15, %rdi
	testq	%rbx, %rbx
	je	.L39
	call	*%rbx
	movl	%eax, %ebx
.L40:
	testl	%ebx, %ebx
	js	.L55
	movq	%r15, %rsi
	movl	%ebx, %edx
	movq	%r13, %rdi
	call	PKCS8_decrypt@PLT
	movq	%r13, %rdi
	movq	%rax, %r14
	call	X509_SIG_free@PLT
	movslq	%ebx, %rsi
	movq	%r15, %rdi
	call	OPENSSL_cleanse@PLT
	testq	%r14, %r14
	je	.L54
	movq	%r14, %rdi
	call	EVP_PKCS82PKEY@PLT
	movq	%r14, %rdi
	movq	%rax, %r13
	call	PKCS8_PRIV_KEY_INFO_free@PLT
	testq	%r13, %r13
	je	.L54
	testq	%r12, %r12
	je	.L36
	movq	(%r12), %rdi
	call	EVP_PKEY_free@PLT
	movq	%r13, (%r12)
.L36:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L56
	addq	$1048, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L55:
	.cfi_restore_state
	movl	$9, %edi
	movl	$128, %r8d
	movl	$104, %edx
	movl	$120, %esi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	%r13, %rdi
	call	X509_SIG_free@PLT
.L54:
	xorl	%r13d, %r13d
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L39:
	call	PEM_def_callback@PLT
	movl	%eax, %ebx
	jmp	.L40
.L56:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE808:
	.size	d2i_PKCS8PrivateKey_bio, .-d2i_PKCS8PrivateKey_bio
	.p2align 4
	.globl	i2d_PKCS8PrivateKey_fp
	.type	i2d_PKCS8PrivateKey_fp, @function
i2d_PKCS8PrivateKey_fp:
.LFB809:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	xorl	%esi, %esi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r9, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%r8d, %ebx
	subq	$24, %rsp
	movq	%rcx, -56(%rbp)
	call	BIO_new_fp@PLT
	movq	-56(%rbp), %rcx
	testq	%rax, %rax
	je	.L61
	subq	$8, %rsp
	pushq	16(%rbp)
	movq	%rax, %r12
	movq	%rcx, %r9
	pushq	%r13
	movq	%rax, %rdi
	movq	%r15, %r8
	movl	$-1, %ecx
	pushq	%rbx
	movl	$1, %edx
	movq	%r14, %rsi
	call	do_pk8pkey
	addq	$32, %rsp
	movq	%r12, %rdi
	movl	%eax, %r13d
	call	BIO_free@PLT
.L57:
	leaq	-40(%rbp), %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L61:
	.cfi_restore_state
	movl	$185, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$7, %edx
	xorl	%r13d, %r13d
	movl	$125, %esi
	movl	$9, %edi
	call	ERR_put_error@PLT
	jmp	.L57
	.cfi_endproc
.LFE809:
	.size	i2d_PKCS8PrivateKey_fp, .-i2d_PKCS8PrivateKey_fp
	.p2align 4
	.globl	i2d_PKCS8PrivateKey_nid_fp
	.type	i2d_PKCS8PrivateKey_nid_fp, @function
i2d_PKCS8PrivateKey_nid_fp:
.LFB810:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	xorl	%esi, %esi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r9, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%r8d, %ebx
	subq	$24, %rsp
	movq	%rcx, -56(%rbp)
	call	BIO_new_fp@PLT
	movq	-56(%rbp), %rcx
	testq	%rax, %rax
	je	.L66
	subq	$8, %rsp
	pushq	16(%rbp)
	movq	%rax, %r12
	movq	%rcx, %r9
	pushq	%r13
	movq	%rax, %rdi
	xorl	%r8d, %r8d
	movl	%r15d, %ecx
	pushq	%rbx
	movl	$1, %edx
	movq	%r14, %rsi
	call	do_pk8pkey
	addq	$32, %rsp
	movq	%r12, %rdi
	movl	%eax, %r13d
	call	BIO_free@PLT
.L62:
	leaq	-40(%rbp), %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L66:
	.cfi_restore_state
	movl	$185, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$7, %edx
	xorl	%r13d, %r13d
	movl	$125, %esi
	movl	$9, %edi
	call	ERR_put_error@PLT
	jmp	.L62
	.cfi_endproc
.LFE810:
	.size	i2d_PKCS8PrivateKey_nid_fp, .-i2d_PKCS8PrivateKey_nid_fp
	.p2align 4
	.globl	PEM_write_PKCS8PrivateKey_nid
	.type	PEM_write_PKCS8PrivateKey_nid, @function
PEM_write_PKCS8PrivateKey_nid:
.LFB811:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	xorl	%esi, %esi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r9, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%r8d, %ebx
	subq	$24, %rsp
	movq	%rcx, -56(%rbp)
	call	BIO_new_fp@PLT
	movq	-56(%rbp), %rcx
	testq	%rax, %rax
	je	.L71
	subq	$8, %rsp
	pushq	16(%rbp)
	movq	%rax, %r12
	movq	%rcx, %r9
	pushq	%r13
	movq	%rax, %rdi
	xorl	%r8d, %r8d
	movl	%r15d, %ecx
	pushq	%rbx
	xorl	%edx, %edx
	movq	%r14, %rsi
	call	do_pk8pkey
	addq	$32, %rsp
	movq	%r12, %rdi
	movl	%eax, %r13d
	call	BIO_free@PLT
.L67:
	leaq	-40(%rbp), %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L71:
	.cfi_restore_state
	movl	$185, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$7, %edx
	xorl	%r13d, %r13d
	movl	$125, %esi
	movl	$9, %edi
	call	ERR_put_error@PLT
	jmp	.L67
	.cfi_endproc
.LFE811:
	.size	PEM_write_PKCS8PrivateKey_nid, .-PEM_write_PKCS8PrivateKey_nid
	.p2align 4
	.globl	PEM_write_PKCS8PrivateKey
	.type	PEM_write_PKCS8PrivateKey, @function
PEM_write_PKCS8PrivateKey:
.LFB812:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	xorl	%esi, %esi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r9, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%r8d, %ebx
	subq	$24, %rsp
	movq	%rcx, -56(%rbp)
	call	BIO_new_fp@PLT
	movq	-56(%rbp), %rcx
	testq	%rax, %rax
	je	.L76
	subq	$8, %rsp
	pushq	16(%rbp)
	movq	%rax, %r12
	movq	%rcx, %r9
	pushq	%r13
	movq	%rax, %rdi
	movq	%r15, %r8
	movl	$-1, %ecx
	pushq	%rbx
	xorl	%edx, %edx
	movq	%r14, %rsi
	call	do_pk8pkey
	addq	$32, %rsp
	movq	%r12, %rdi
	movl	%eax, %r13d
	call	BIO_free@PLT
.L72:
	leaq	-40(%rbp), %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L76:
	.cfi_restore_state
	movl	$185, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$7, %edx
	xorl	%r13d, %r13d
	movl	$125, %esi
	movl	$9, %edi
	call	ERR_put_error@PLT
	jmp	.L72
	.cfi_endproc
.LFE812:
	.size	PEM_write_PKCS8PrivateKey, .-PEM_write_PKCS8PrivateKey
	.p2align 4
	.globl	d2i_PKCS8PrivateKey_fp
	.type	d2i_PKCS8PrivateKey_fp, @function
d2i_PKCS8PrivateKey_fp:
.LFB814:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	xorl	%esi, %esi
	pushq	%r12
	.cfi_offset 12, -48
	call	BIO_new_fp@PLT
	testq	%rax, %rax
	je	.L81
	movq	%rax, %r12
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%r15, %rcx
	movq	%r14, %rdx
	call	d2i_PKCS8PrivateKey_bio
	movq	%r12, %rdi
	movq	%rax, %r13
	call	BIO_free@PLT
.L77:
	popq	%r12
	movq	%r13, %rax
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L81:
	.cfi_restore_state
	movl	$200, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$7, %edx
	xorl	%r13d, %r13d
	movl	$121, %esi
	movl	$9, %edi
	call	ERR_put_error@PLT
	jmp	.L77
	.cfi_endproc
.LFE814:
	.size	d2i_PKCS8PrivateKey_fp, .-d2i_PKCS8PrivateKey_fp
	.p2align 4
	.globl	PEM_read_bio_PKCS8
	.type	PEM_read_bio_PKCS8, @function
PEM_read_bio_PKCS8:
.LFB815:
	.cfi_startproc
	endbr64
	movq	%rdx, %r8
	movq	%rdi, %rdx
	movq	d2i_X509_SIG@GOTPCREL(%rip), %rdi
	movq	%rcx, %r9
	movq	%rsi, %rcx
	leaq	.LC1(%rip), %rsi
	jmp	PEM_ASN1_read_bio@PLT
	.cfi_endproc
.LFE815:
	.size	PEM_read_bio_PKCS8, .-PEM_read_bio_PKCS8
	.p2align 4
	.globl	PEM_read_PKCS8
	.type	PEM_read_PKCS8, @function
PEM_read_PKCS8:
.LFB816:
	.cfi_startproc
	endbr64
	movq	%rdx, %r8
	movq	%rdi, %rdx
	movq	d2i_X509_SIG@GOTPCREL(%rip), %rdi
	movq	%rcx, %r9
	movq	%rsi, %rcx
	leaq	.LC1(%rip), %rsi
	jmp	PEM_ASN1_read@PLT
	.cfi_endproc
.LFE816:
	.size	PEM_read_PKCS8, .-PEM_read_PKCS8
	.p2align 4
	.globl	PEM_write_bio_PKCS8
	.type	PEM_write_bio_PKCS8, @function
PEM_write_bio_PKCS8:
.LFB817:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rdx
	movq	i2d_X509_SIG@GOTPCREL(%rip), %rdi
	movq	%rsi, %rcx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	leaq	.LC1(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$8, %rsp
	pushq	$0
	pushq	$0
	pushq	$0
	call	PEM_ASN1_write_bio@PLT
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE817:
	.size	PEM_write_bio_PKCS8, .-PEM_write_bio_PKCS8
	.p2align 4
	.globl	PEM_write_PKCS8
	.type	PEM_write_PKCS8, @function
PEM_write_PKCS8:
.LFB818:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rdx
	movq	i2d_X509_SIG@GOTPCREL(%rip), %rdi
	movq	%rsi, %rcx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	leaq	.LC1(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$8, %rsp
	pushq	$0
	pushq	$0
	pushq	$0
	call	PEM_ASN1_write@PLT
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE818:
	.size	PEM_write_PKCS8, .-PEM_write_PKCS8
	.p2align 4
	.globl	PEM_read_bio_PKCS8_PRIV_KEY_INFO
	.type	PEM_read_bio_PKCS8_PRIV_KEY_INFO, @function
PEM_read_bio_PKCS8_PRIV_KEY_INFO:
.LFB819:
	.cfi_startproc
	endbr64
	movq	%rdx, %r8
	movq	%rdi, %rdx
	movq	d2i_PKCS8_PRIV_KEY_INFO@GOTPCREL(%rip), %rdi
	movq	%rcx, %r9
	movq	%rsi, %rcx
	leaq	.LC2(%rip), %rsi
	jmp	PEM_ASN1_read_bio@PLT
	.cfi_endproc
.LFE819:
	.size	PEM_read_bio_PKCS8_PRIV_KEY_INFO, .-PEM_read_bio_PKCS8_PRIV_KEY_INFO
	.p2align 4
	.globl	PEM_read_PKCS8_PRIV_KEY_INFO
	.type	PEM_read_PKCS8_PRIV_KEY_INFO, @function
PEM_read_PKCS8_PRIV_KEY_INFO:
.LFB820:
	.cfi_startproc
	endbr64
	movq	%rdx, %r8
	movq	%rdi, %rdx
	movq	d2i_PKCS8_PRIV_KEY_INFO@GOTPCREL(%rip), %rdi
	movq	%rcx, %r9
	movq	%rsi, %rcx
	leaq	.LC2(%rip), %rsi
	jmp	PEM_ASN1_read@PLT
	.cfi_endproc
.LFE820:
	.size	PEM_read_PKCS8_PRIV_KEY_INFO, .-PEM_read_PKCS8_PRIV_KEY_INFO
	.p2align 4
	.globl	PEM_write_bio_PKCS8_PRIV_KEY_INFO
	.type	PEM_write_bio_PKCS8_PRIV_KEY_INFO, @function
PEM_write_bio_PKCS8_PRIV_KEY_INFO:
.LFB821:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rdx
	movq	i2d_PKCS8_PRIV_KEY_INFO@GOTPCREL(%rip), %rdi
	movq	%rsi, %rcx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	leaq	.LC2(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$8, %rsp
	pushq	$0
	pushq	$0
	pushq	$0
	call	PEM_ASN1_write_bio@PLT
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE821:
	.size	PEM_write_bio_PKCS8_PRIV_KEY_INFO, .-PEM_write_bio_PKCS8_PRIV_KEY_INFO
	.p2align 4
	.globl	PEM_write_PKCS8_PRIV_KEY_INFO
	.type	PEM_write_PKCS8_PRIV_KEY_INFO, @function
PEM_write_PKCS8_PRIV_KEY_INFO:
.LFB822:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rdx
	movq	i2d_PKCS8_PRIV_KEY_INFO@GOTPCREL(%rip), %rdi
	movq	%rsi, %rcx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	leaq	.LC2(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$8, %rsp
	pushq	$0
	pushq	$0
	pushq	$0
	call	PEM_ASN1_write@PLT
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE822:
	.size	PEM_write_PKCS8_PRIV_KEY_INFO, .-PEM_write_PKCS8_PRIV_KEY_INFO
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
