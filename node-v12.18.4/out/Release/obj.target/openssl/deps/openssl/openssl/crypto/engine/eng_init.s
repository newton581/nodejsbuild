	.file	"eng_init.c"
	.text
	.p2align 4
	.globl	engine_unlocked_init
	.type	engine_unlocked_init, @function
engine_unlocked_init:
.LFB877:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	160(%rdi), %eax
	testl	%eax, %eax
	jne	.L5
	movq	96(%rdi), %rax
	testq	%rax, %rax
	je	.L5
	call	*%rax
	testl	%eax, %eax
	jne	.L2
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	.cfi_restore_state
	movl	$1, %eax
.L2:
	lock addl	$1, 156(%rbx)
	addl	$1, 160(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE877:
	.size	engine_unlocked_init, .-engine_unlocked_init
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/engine/eng_init.c"
	.text
	.p2align 4
	.globl	engine_unlocked_finish
	.type	engine_unlocked_finish, @function
engine_unlocked_finish:
.LFB878:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subl	$1, 160(%rdi)
	jne	.L18
	movq	104(%rdi), %rax
	testq	%rax, %rax
	je	.L18
	testl	%esi, %esi
	jne	.L24
	call	*%rax
	movl	%eax, %r13d
.L16:
	testl	%r13d, %r13d
	je	.L12
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	engine_free_util@PLT
	testl	%eax, %eax
	jne	.L12
.L25:
	xorl	%r13d, %r13d
	movl	$70, %r8d
	movl	$106, %edx
	movl	$191, %esi
	leaq	.LC0(%rip), %rcx
	movl	$38, %edi
	call	ERR_put_error@PLT
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r12, %rdi
	movl	$1, %r13d
	call	engine_free_util@PLT
	testl	%eax, %eax
	je	.L25
.L12:
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	.cfi_restore_state
	movq	global_engine_lock(%rip), %rdi
	call	CRYPTO_THREAD_unlock@PLT
	movq	%r12, %rdi
	call	*104(%r12)
	movq	global_engine_lock(%rip), %rdi
	movl	%eax, %r13d
	call	CRYPTO_THREAD_write_lock@PLT
	jmp	.L16
	.cfi_endproc
.LFE878:
	.size	engine_unlocked_finish, .-engine_unlocked_finish
	.p2align 4
	.globl	ENGINE_init
	.type	ENGINE_init, @function
ENGINE_init:
.LFB879:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	testq	%rdi, %rdi
	je	.L42
	movq	do_engine_lock_init_ossl_@GOTPCREL(%rip), %rsi
	movq	%rdi, %rbx
	leaq	engine_lock_init(%rip), %rdi
	call	CRYPTO_THREAD_run_once@PLT
	testl	%eax, %eax
	je	.L29
	movl	do_engine_lock_init_ossl_ret_(%rip), %edx
	testl	%edx, %edx
	jne	.L30
.L29:
	xorl	%r12d, %r12d
	movl	$85, %r8d
	movl	$65, %edx
	movl	$119, %esi
	leaq	.LC0(%rip), %rcx
	movl	$38, %edi
	call	ERR_put_error@PLT
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	.cfi_restore_state
	movq	global_engine_lock(%rip), %rdi
	call	CRYPTO_THREAD_write_lock@PLT
	movl	160(%rbx), %eax
	testl	%eax, %eax
	jne	.L34
	movq	96(%rbx), %rax
	testq	%rax, %rax
	je	.L34
	movq	%rbx, %rdi
	call	*%rax
	movl	%eax, %r12d
	testl	%eax, %eax
	je	.L32
.L31:
	lock addl	$1, 156(%rbx)
	addl	$1, 160(%rbx)
.L32:
	movq	global_engine_lock(%rip), %rdi
	call	CRYPTO_THREAD_unlock@PLT
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L34:
	.cfi_restore_state
	movl	$1, %r12d
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L42:
	xorl	%r12d, %r12d
	movl	$81, %r8d
	movl	$67, %edx
	movl	$119, %esi
	leaq	.LC0(%rip), %rcx
	movl	$38, %edi
	call	ERR_put_error@PLT
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE879:
	.size	ENGINE_init, .-ENGINE_init
	.p2align 4
	.globl	ENGINE_finish
	.type	ENGINE_finish, @function
ENGINE_finish:
.LFB880:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	$1, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	testq	%rdi, %rdi
	je	.L43
	movq	%rdi, %r12
	movq	global_engine_lock(%rip), %rdi
	call	CRYPTO_THREAD_write_lock@PLT
	subl	$1, 160(%r12)
	jne	.L45
	cmpq	$0, 104(%r12)
	je	.L45
	movq	global_engine_lock(%rip), %rdi
	call	CRYPTO_THREAD_unlock@PLT
	movq	%r12, %rdi
	call	*104(%r12)
	movq	global_engine_lock(%rip), %rdi
	movl	%eax, %r13d
	call	CRYPTO_THREAD_write_lock@PLT
	testl	%r13d, %r13d
	je	.L55
.L45:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	engine_free_util@PLT
	testl	%eax, %eax
	je	.L56
	movq	global_engine_lock(%rip), %rdi
	call	CRYPTO_THREAD_unlock@PLT
.L43:
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L56:
	.cfi_restore_state
	movl	$70, %r8d
	movl	$106, %edx
	movl	$191, %esi
	movl	$38, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L55:
	movq	global_engine_lock(%rip), %rdi
	xorl	%r13d, %r13d
	call	CRYPTO_THREAD_unlock@PLT
	movl	$105, %r8d
	movl	$106, %edx
	leaq	.LC0(%rip), %rcx
	movl	$107, %esi
	movl	$38, %edi
	call	ERR_put_error@PLT
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE880:
	.size	ENGINE_finish, .-ENGINE_finish
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
