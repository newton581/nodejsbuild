	.file	"ssl3_record.c"
	.text
	.p2align 4
	.globl	SSL3_RECORD_clear
	.type	SSL3_RECORD_clear, @function
SSL3_RECORD_clear:
.LFB1037:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L1
	leaq	(%rsi,%rsi,4), %rdx
	salq	$4, %rdx
	addq	%rdi, %rdx
	.p2align 4,,10
	.p2align 3
.L3:
	movq	48(%rdi), %rax
	pxor	%xmm0, %xmm0
	addq	$80, %rdi
	movups	%xmm0, -32(%rdi)
	movups	%xmm0, -80(%rdi)
	movups	%xmm0, -64(%rdi)
	movups	%xmm0, -48(%rdi)
	movups	%xmm0, -16(%rdi)
	movq	%rax, -32(%rdi)
	cmpq	%rdx, %rdi
	jne	.L3
.L1:
	ret
	.cfi_endproc
.LFE1037:
	.size	SSL3_RECORD_clear, .-SSL3_RECORD_clear
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/ssl/record/ssl3_record.c"
	.text
	.p2align 4
	.globl	SSL3_RECORD_release
	.type	SSL3_RECORD_release, @function
SSL3_RECORD_release:
.LFB1038:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L17
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	.LC0(%rip), %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	(%rsi,%rsi,4), %r12
	pushq	%rbx
	salq	$4, %r12
	.cfi_offset 3, -40
	leaq	48(%rdi), %rbx
	addq	%rbx, %r12
	subq	$8, %rsp
	.p2align 4,,10
	.p2align 3
.L11:
	movq	(%rbx), %rdi
	movl	$55, %edx
	movq	%r13, %rsi
	addq	$80, %rbx
	call	CRYPTO_free@PLT
	movq	$0, -80(%rbx)
	cmpq	%r12, %rbx
	jne	.L11
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.cfi_endproc
.LFE1038:
	.size	SSL3_RECORD_release, .-SSL3_RECORD_release
	.p2align 4
	.globl	SSL3_RECORD_set_seq_num
	.type	SSL3_RECORD_set_seq_num, @function
SSL3_RECORD_set_seq_num:
.LFB1039:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rax
	movq	%rax, 72(%rdi)
	ret
	.cfi_endproc
.LFE1039:
	.size	SSL3_RECORD_set_seq_num, .-SSL3_RECORD_set_seq_num
	.p2align 4
	.globl	early_data_count_ok
	.type	early_data_count_ok, @function
early_data_count_ok:
.LFB1041:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movl	56(%rdi), %eax
	movq	1296(%rdi), %r9
	testl	%eax, %eax
	je	.L40
	cmpl	$2, 1816(%rdi)
	movl	6180(%rdi), %r8d
	je	.L41
	testl	%r8d, %r8d
	jne	.L26
.L43:
	cmpl	$1, %ecx
	movl	$133, %r9d
	sbbl	%esi, %esi
	andl	$-70, %esi
	addl	$80, %esi
.L39:
	leaq	.LC0(%rip), %r8
	movl	$164, %ecx
	movl	$532, %edx
	call	ossl_statem_fatal@PLT
	xorl	%eax, %eax
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L40:
	.cfi_restore_state
	movl	580(%r9), %r8d
	testl	%r8d, %r8d
	jne	.L26
	movq	1304(%rdi), %r8
	testq	%r8, %r8
	je	.L24
	movl	580(%r8), %r8d
	testl	%r8d, %r8d
	je	.L24
.L26:
	movl	6184(%rdi), %eax
	addl	%r8d, %edx
	movq	%rax, %r9
	addq	%rsi, %rax
	cmpq	%rdx, %rax
	ja	.L42
	addl	%esi, %r9d
	movl	$1, %eax
	movl	%r9d, 6184(%rdi)
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L42:
	.cfi_restore_state
	cmpl	$1, %ecx
	movl	$142, %r9d
	sbbl	%esi, %esi
	andl	$-70, %esi
	addl	$80, %esi
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L41:
	movl	580(%r9), %eax
	cmpl	%eax, %r8d
	cmova	%eax, %r8d
	testl	%r8d, %r8d
	je	.L43
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L24:
	movl	$117, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movl	%eax, -4(%rbp)
	movl	$532, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	movl	-4(%rbp), %eax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1041:
	.size	early_data_count_ok, .-early_data_count_ok
	.p2align 4
	.globl	ssl3_do_uncompress
	.type	ssl3_do_uncompress, @function
ssl3_do_uncompress:
.LFB1059:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE1059:
	.size	ssl3_do_uncompress, .-ssl3_do_uncompress
	.p2align 4
	.globl	ssl3_do_compress
	.type	ssl3_do_compress, @function
ssl3_do_compress:
.LFB1044:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE1044:
	.size	ssl3_do_compress, .-ssl3_do_compress
	.p2align 4
	.globl	ssl3_enc
	.type	ssl3_enc, @function
ssl3_enc:
.LFB1045:
	.cfi_startproc
	endbr64
	cmpq	$1, %rdx
	je	.L47
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L47:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%ecx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$40, %rsp
	testl	%ecx, %ecx
	je	.L49
	movq	1136(%rdi), %r15
	testq	%r15, %r15
	je	.L53
.L85:
	movq	%r15, %rdi
	call	EVP_CIPHER_CTX_cipher@PLT
	cmpq	$0, 1296(%r13)
	movq	8(%rbx), %r14
	je	.L51
	testq	%rax, %rax
	je	.L51
	movq	%r15, %rdi
	call	EVP_CIPHER_CTX_block_size@PLT
	movslq	%eax, %rcx
	cmpl	$1, %eax
	setne	-49(%rbp)
	movzbl	-49(%rbp), %eax
	movq	%rcx, -64(%rbp)
	testl	%r12d, %r12d
	je	.L81
	testb	%al, %al
	jne	.L86
	testl	%r12d, %r12d
	jne	.L56
.L81:
	testq	%r14, %r14
	je	.L57
	movq	%r14, %rax
	xorl	%edx, %edx
	divq	-64(%rbp)
	testq	%rdx, %rdx
	jne	.L57
.L56:
	movq	40(%rbx), %rdx
	movq	32(%rbx), %rsi
	movl	%r14d, %ecx
	movq	%r15, %rdi
	call	EVP_Cipher@PLT
	testl	%eax, %eax
	jle	.L60
	movq	1112(%r13), %rdi
	call	EVP_MD_CTX_md@PLT
	testq	%rax, %rax
	je	.L61
	movq	1112(%r13), %rdi
	call	EVP_MD_CTX_md@PLT
	movq	%rax, %rdi
	call	EVP_MD_size@PLT
	testl	%eax, %eax
	js	.L87
	cltq
.L58:
	testl	%r12d, %r12d
	jne	.L62
	cmpb	$0, -49(%rbp)
	jne	.L88
.L62:
	movl	$1, %eax
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L49:
	movq	1088(%rdi), %r15
	testq	%r15, %r15
	jne	.L85
.L53:
	movq	8(%rbx), %r14
.L51:
	movq	40(%rbx), %rsi
	movq	32(%rbx), %rdi
	movq	%r14, %rdx
	call	memmove@PLT
	movq	32(%rbx), %rax
	movq	%rax, 40(%rbx)
	movl	$1, %eax
.L46:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L57:
	.cfi_restore_state
	addq	$40, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L86:
	.cfi_restore_state
	movq	%r14, %rax
	xorl	%edx, %edx
	movq	8(%rbx), %rdi
	xorl	%esi, %esi
	divq	%rcx
	addq	40(%rbx), %rdi
	subq	%rdx, %rcx
	movq	%rcx, %rdx
	addq	%rcx, %r14
	movq	%rcx, -72(%rbp)
	call	memset@PLT
	movq	-72(%rbp), %rdx
	movq	40(%rbx), %rax
	addq	%rdx, 8(%rbx)
	subl	$1, %edx
	movb	%dl, -1(%rax,%r14)
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L61:
	xorl	%eax, %eax
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L88:
	movq	8(%rbx), %rcx
	addq	$1, %rax
	cmpq	%rcx, %rax
	ja	.L57
	movq	32(%rbx), %rdx
	movq	%rcx, %rsi
	movq	-64(%rbp), %r10
	movzbl	-1(%rdx,%rcx), %edx
	movq	%r10, %rdi
	addq	%rdx, %rax
	addq	$1, %rdx
	subq	%rax, %rsi
	xorq	%rdx, %rdi
	xorq	%rax, %rsi
	xorq	%rcx, %rax
	orq	%rsi, %rax
	xorq	%rcx, %rax
	movq	%rax, %rsi
	movq	%r10, %rax
	subq	%rdx, %rax
	xorq	%rdx, %rax
	orq	%rdi, %rax
	xorq	%r10, %rax
	orq	%rsi, %rax
	sarq	$63, %rax
	notq	%rax
	andq	%rax, %rdx
	subq	%rdx, %rcx
	movl	%eax, %edx
	notl	%eax
	movq	%rcx, 8(%rbx)
	movl	%eax, %ecx
	movl	%edx, %eax
	andl	$1, %eax
	orl	%ecx, %eax
	jmp	.L46
.L60:
	movl	$-1, %eax
	jmp	.L46
.L87:
	movl	$913, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movq	%r13, %rdi
	movl	$608, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	orl	$-1, %eax
	jmp	.L46
	.cfi_endproc
.LFE1045:
	.size	ssl3_enc, .-ssl3_enc
	.p2align 4
	.globl	n_ssl3_mac
	.type	n_ssl3_mac, @function
n_ssl3_mac:
.LFB1047:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$152, %rsp
	movq	168(%rdi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%ecx, %ecx
	je	.L90
	movq	1160(%rdi), %r15
	movq	%r15, %rdi
	call	EVP_MD_CTX_md@PLT
	movq	%rax, %rdi
	call	EVP_MD_size@PLT
	testl	%eax, %eax
	js	.L98
	leaq	6112(%r12), %rdx
	movslq	%eax, %rcx
	movl	$48, %eax
	addq	$88, %r14
	movq	%rdx, -176(%rbp)
	xorl	%edx, %edx
	divq	%rcx
	movq	%rcx, -152(%rbp)
	imulq	%rcx, %rax
	movq	%rax, -168(%rbp)
	call	EVP_MD_CTX_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L111
	.p2align 4,,10
	.p2align 3
.L98:
	xorl	%eax, %eax
.L89:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L112
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L90:
	.cfi_restore_state
	movq	1112(%rdi), %r15
	movq	%r15, %rdi
	call	EVP_MD_CTX_md@PLT
	movq	%rax, %rdi
	call	EVP_MD_size@PLT
	testl	%eax, %eax
	js	.L98
	movslq	%eax, %rcx
	xorl	%edx, %edx
	movl	$48, %eax
	movq	1088(%r12), %rdi
	divq	%rcx
	movq	%rcx, -152(%rbp)
	leaq	6104(%r12), %rsi
	addq	$16, %r14
	movq	%rsi, -176(%rbp)
	imulq	%rcx, %rax
	movq	%rax, -168(%rbp)
	call	EVP_CIPHER_CTX_cipher@PLT
	movq	%rax, %rdi
	call	EVP_CIPHER_flags@PLT
	andl	$983047, %eax
	cmpq	$2, %rax
	je	.L113
.L95:
	call	EVP_MD_CTX_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L98
.L111:
	movl	4(%rbx), %eax
	movq	%r15, %rsi
	movq	%r12, %rdi
	movb	%al, -157(%rbp)
	movq	8(%rbx), %rax
	shrq	$8, %rax
	movb	%al, 0(%r13)
	movq	8(%rbx), %rax
	movb	%al, 1(%r13)
	call	EVP_MD_CTX_copy_ex@PLT
	testl	%eax, %eax
	jle	.L100
	movq	-152(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	jle	.L100
	movq	-168(%rbp), %rdx
	leaq	ssl3_pad_1(%rip), %rsi
	movq	%r12, %rdi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	jle	.L100
	movq	-176(%rbp), %rsi
	movl	$8, %edx
	movq	%r12, %rdi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	jle	.L100
	leaq	-157(%rbp), %rsi
	movl	$1, %edx
	movq	%r12, %rdi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	jle	.L100
	movl	$2, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	jle	.L100
	movq	8(%rbx), %rdx
	movq	40(%rbx), %rsi
	movq	%r12, %rdi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	jle	.L100
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	EVP_DigestFinal_ex@PLT
	testl	%eax, %eax
	jle	.L100
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	EVP_MD_CTX_copy_ex@PLT
	testl	%eax, %eax
	jle	.L100
	movq	-152(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	jle	.L100
	movq	-168(%rbp), %rdx
	leaq	ssl3_pad_2(%rip), %rsi
	movq	%r12, %rdi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	jle	.L100
	movq	-152(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	jle	.L100
	leaq	-156(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	EVP_DigestFinal_ex@PLT
	testl	%eax, %eax
	jle	.L100
	movq	%r12, %rdi
	call	EVP_MD_CTX_free@PLT
	.p2align 4,,10
	.p2align 3
.L97:
	movq	-176(%rbp), %rdi
	call	ssl3_record_sequence_update@PLT
	movl	$1, %eax
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L100:
	movq	%r12, %rdi
	call	EVP_MD_CTX_free@PLT
	jmp	.L98
	.p2align 4,,10
	.p2align 3
.L113:
	movq	%r15, %rdi
	call	ssl3_cbc_record_digest_supported@PLT
	testb	%al, %al
	je	.L95
	movq	-152(%rbp), %r9
	leaq	-144(%rbp), %r10
	movl	$75, %ecx
	movq	%r14, %rsi
	movq	%r10, %rdi
	movq	%r9, %rdx
	movq	%r9, -184(%rbp)
	call	__memcpy_chk@PLT
	movq	-184(%rbp), %r9
	movq	-168(%rbp), %rdx
	leaq	ssl3_pad_1(%rip), %rsi
	movq	%rax, -184(%rbp)
	leaq	(%rax,%r9), %rdi
	movq	%r9, -192(%rbp)
	call	memcpy@PLT
	movq	-192(%rbp), %r9
	movq	%r13, %rsi
	movq	-168(%rbp), %rcx
	movq	6104(%r12), %rax
	movq	-184(%rbp), %r10
	movq	%r15, %rdi
	addq	%r9, %rcx
	movq	%rax, -144(%rbp,%rcx)
	movl	4(%rbx), %eax
	movb	%al, -136(%rbp,%rcx)
	movq	8(%rbx), %rax
	pushq	$1
	movq	%rax, %rdx
	pushq	%r9
	addq	%rax, %r9
	pushq	%r14
	shrq	$8, %rdx
	movq	40(%rbx), %r8
	pushq	16(%rbx)
	movb	%dl, -135(%rbp,%rcx)
	leaq	-152(%rbp), %rdx
	movb	%al, -134(%rbp,%rcx)
	movq	%r10, %rcx
	call	ssl3_cbc_digest_record@PLT
	addq	$32, %rsp
	testl	%eax, %eax
	jg	.L97
	jmp	.L98
.L112:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1047:
	.size	n_ssl3_mac, .-n_ssl3_mac
	.p2align 4
	.globl	tls1_mac
	.type	tls1_mac, @function
tls1_mac:
.LFB1048:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$88, %rsp
	movq	%rdx, -120(%rbp)
	movl	312(%rdi), %r8d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%ecx, %ecx
	je	.L115
	andl	$2, %r8d
	movq	1160(%rdi), %r15
	leaq	6112(%rdi), %r14
	movl	%r8d, %r13d
.L116:
	movq	%r15, %rdi
	movl	%ecx, -108(%rbp)
	call	EVP_MD_CTX_md@PLT
	movq	%rax, %rdi
	call	EVP_MD_size@PLT
	testl	%eax, %eax
	js	.L133
	cltq
	testl	%r13d, %r13d
	movq	$0, -104(%rbp)
	movl	-108(%rbp), %ecx
	movq	%rax, -88(%rbp)
	je	.L142
.L118:
	movq	8(%rbx), %rax
	movq	192(%rax), %rax
	testb	$8, 96(%rax)
	jne	.L143
	movq	(%r14), %rax
	movq	%rax, -69(%rbp)
.L124:
	movl	4(%r12), %eax
	movb	%al, -61(%rbp)
	movzwl	(%rbx), %eax
	rolw	$8, %ax
	movw	%ax, -60(%rbp)
	movzwl	8(%r12), %eax
	rolw	$8, %ax
	movw	%ax, -58(%rbp)
	testl	%ecx, %ecx
	jne	.L125
	movq	168(%rbx), %rax
	movq	(%rax), %rax
	testb	$1, %ah
	je	.L144
.L125:
	leaq	-69(%rbp), %rsi
	movl	$13, %edx
	movq	%r15, %rdi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	jle	.L128
	movq	8(%r12), %rdx
	movq	40(%r12), %rsi
	movq	%r15, %rdi
	call	EVP_DigestUpdate@PLT
	testl	%eax, %eax
	jle	.L128
	movq	-120(%rbp), %rsi
	leaq	-88(%rbp), %rdx
	movq	%r15, %rdi
	call	EVP_DigestSignFinal@PLT
	testl	%eax, %eax
	jle	.L128
.L126:
	movq	-104(%rbp), %rdi
	call	EVP_MD_CTX_free@PLT
	movq	8(%rbx), %rax
	movq	192(%rax), %rax
	testb	$8, 96(%rax)
	jne	.L131
	addb	$1, 7(%r14)
	jne	.L131
	addb	$1, 6(%r14)
	jne	.L131
	addb	$1, 5(%r14)
	jne	.L131
	addb	$1, 4(%r14)
	jne	.L131
	addb	$1, 3(%r14)
	jne	.L131
	addb	$1, 2(%r14)
	jne	.L131
	addb	$1, 1(%r14)
	jne	.L131
	addb	$1, (%r14)
.L131:
	movl	$1, %r13d
.L114:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L145
	leaq	-40(%rbp), %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L115:
	.cfi_restore_state
	andl	$1, %r8d
	movq	1112(%rdi), %r15
	leaq	6104(%rdi), %r14
	movl	%r8d, %r13d
	jmp	.L116
	.p2align 4,,10
	.p2align 3
.L143:
	movq	6128(%rbx), %rax
	testl	%ecx, %ecx
	jne	.L146
	movzwl	(%rax), %eax
	movzbl	%ah, %edx
.L123:
	movb	%al, -76(%rbp)
	movl	2(%r14), %eax
	movb	%dl, -77(%rbp)
	movl	%eax, -75(%rbp)
	movzwl	6(%r14), %eax
	movw	%ax, -71(%rbp)
	movq	-77(%rbp), %rax
	movq	%rax, -69(%rbp)
	jmp	.L124
	.p2align 4,,10
	.p2align 3
.L142:
	call	EVP_MD_CTX_new@PLT
	movl	-108(%rbp), %ecx
	testq	%rax, %rax
	movq	%rax, -104(%rbp)
	je	.L120
	movq	%r15, %rsi
	movq	-104(%rbp), %r15
	movl	%ecx, -108(%rbp)
	movq	%r15, %rdi
	call	EVP_MD_CTX_copy@PLT
	testl	%eax, %eax
	je	.L120
	movl	-108(%rbp), %ecx
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L144:
	movq	1088(%rbx), %rdi
	call	EVP_CIPHER_CTX_cipher@PLT
	movq	%rax, %rdi
	call	EVP_CIPHER_flags@PLT
	andl	$983047, %eax
	cmpq	$2, %rax
	jne	.L125
	movq	%r15, %rdi
	call	ssl3_cbc_record_digest_supported@PLT
	testb	%al, %al
	je	.L125
	movq	168(%rbx), %rax
	pushq	$0
	leaq	-69(%rbp), %rcx
	movq	%r15, %rdi
	movq	40(%r12), %r8
	movq	-120(%rbp), %rsi
	leaq	-88(%rbp), %rdx
	pushq	8(%rax)
	addq	$16, %rax
	movq	-88(%rbp), %r9
	addq	8(%r12), %r9
	pushq	%rax
	pushq	16(%r12)
	call	ssl3_cbc_digest_record@PLT
	addq	$32, %rsp
	testl	%eax, %eax
	jg	.L126
	.p2align 4,,10
	.p2align 3
.L128:
	movq	-104(%rbp), %rdi
	xorl	%r13d, %r13d
	call	EVP_MD_CTX_free@PLT
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L133:
	xorl	%r13d, %r13d
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L146:
	movzwl	2(%rax), %eax
	movzbl	%ah, %edx
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L120:
	movq	-104(%rbp), %rdi
	call	EVP_MD_CTX_free@PLT
	jmp	.L114
.L145:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1048:
	.size	tls1_mac, .-tls1_mac
	.p2align 4
	.globl	ssl3_cbc_remove_padding
	.type	ssl3_cbc_remove_padding, @function
ssl3_cbc_remove_padding:
.LFB1049:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rcx
	addq	$1, %rdx
	xorl	%eax, %eax
	cmpq	%rdx, %rcx
	jb	.L147
	movq	32(%rdi), %rax
	movzbl	-1(%rax,%rcx), %r8d
	movq	%rsi, %rax
	addq	%r8, %rdx
	addq	$1, %r8
	subq	%r8, %rax
	movq	%r8, %r9
	xorq	%r8, %rax
	xorq	%rsi, %r9
	orq	%r9, %rax
	xorq	%rax, %rsi
	movq	%rcx, %rax
	subq	%rdx, %rax
	xorq	%rdx, %rax
	xorq	%rcx, %rdx
	orq	%rdx, %rax
	xorq	%rcx, %rax
	orq	%rsi, %rax
	sarq	$63, %rax
	notq	%rax
	andq	%rax, %r8
	movl	%eax, %edx
	notl	%eax
	subq	%r8, %rcx
	movq	%rcx, 8(%rdi)
	andl	$1, %edx
	orl	%edx, %eax
.L147:
	ret
	.cfi_endproc
.LFE1049:
	.size	ssl3_cbc_remove_padding, .-ssl3_cbc_remove_padding
	.p2align 4
	.globl	tls1_cbc_remove_padding
	.type	tls1_cbc_remove_padding, @function
tls1_cbc_remove_padding:
.LFB1050:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	leaq	1(%rcx), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	8(%rdi), %rax
	movq	8(%rsi), %rcx
	movq	192(%rax), %rax
	testb	$1, 96(%rax)
	je	.L151
	leaq	(%r14,%rdx), %rsi
	xorl	%eax, %eax
	cmpq	%rcx, %rsi
	ja	.L150
	movdqu	32(%r12), %xmm1
	movq	%rdx, %xmm0
	movq	32(%r12), %rax
	subq	%rdx, %rcx
	punpcklqdq	%xmm0, %xmm0
	subq	%rdx, 16(%r12)
	paddq	%xmm1, %xmm0
	movq	%rcx, 8(%r12)
	addq	%rdx, %rax
	movups	%xmm0, 32(%r12)
	jmp	.L153
	.p2align 4,,10
	.p2align 3
.L160:
	movl	$256, %r9d
.L155:
	movq	%r8, %rsi
	movq	%rbx, %rcx
	subq	%rbx, %rsi
	addq	32(%r12), %rsi
	.p2align 4,,10
	.p2align 3
.L157:
	movzbl	-1(%rsi,%rcx), %edx
	movq	%rcx, %r11
	subq	$1, %rcx
	xorq	%rax, %r11
	xorl	%r13d, %edx
	movzbl	%dl, %edx
	notq	%rdx
	movq	%rdx, %r10
	movq	%rbx, %rdx
	xorq	%rax, %rdx
	addq	$1, %rax
	orq	%r11, %rdx
	xorq	%rbx, %rdx
	sarq	$63, %rdx
	orq	%r10, %rdx
	andq	%rdx, %rdi
	cmpq	%rax, %r9
	ja	.L157
	movq	%rdi, %rdx
	notq	%rdx
.L156:
	movzbl	%dl, %eax
	orq	$-256, %rdi
	addq	$1, %rbx
	subq	$1, %rax
	andq	%rdi, %rax
	sarq	$63, %rax
	andq	%rax, %rbx
	movl	%eax, %edx
	notl	%eax
	subq	%rbx, %r8
	movq	%r8, 8(%r12)
	andl	$1, %edx
	orl	%edx, %eax
.L150:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L151:
	.cfi_restore_state
	xorl	%eax, %eax
	cmpq	%rcx, %r14
	ja	.L150
	movq	32(%rsi), %rax
.L153:
	movq	1088(%rdi), %rdi
	movzbl	-1(%rax,%rcx), %ebx
	call	EVP_CIPHER_CTX_cipher@PLT
	movq	%rbx, %r13
	movq	%rax, %rdi
	call	EVP_CIPHER_flags@PLT
	andl	$2097152, %eax
	jne	.L166
	movq	8(%r12), %r8
	addq	%rbx, %r14
	movq	%r8, %rdx
	subq	%r14, %rdx
	xorq	%r14, %rdx
	xorq	%r8, %r14
	orq	%r14, %rdx
	xorq	%r8, %rdx
	sarq	$63, %rdx
	movq	%rdx, %rdi
	notq	%rdi
	cmpq	$255, %r8
	ja	.L160
	movq	%r8, %r9
	testq	%r8, %r8
	jne	.L155
	jmp	.L156
	.p2align 4,,10
	.p2align 3
.L166:
	notq	%rbx
	movl	$1, %eax
	addq	%rbx, 8(%r12)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1050:
	.size	tls1_cbc_remove_padding, .-tls1_cbc_remove_padding
	.p2align 4
	.globl	tls1_enc
	.type	tls1_enc, @function
tls1_enc:
.LFB1046:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$1048, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -1056(%rbp)
	movq	%rdx, -1048(%rbp)
	movl	%ecx, -1016(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L287
	movl	-1016(%rbp), %r9d
	testl	%r9d, %r9d
	jne	.L288
	movq	1112(%rdi), %rdi
	call	EVP_MD_CTX_md@PLT
	testq	%rax, %rax
	je	.L177
	movq	1112(%r12), %rdi
	call	EVP_MD_CTX_md@PLT
	movq	%rax, %rdi
	call	EVP_MD_size@PLT
	testl	%eax, %eax
	js	.L289
.L177:
	movq	1088(%r12), %r15
	testq	%r15, %r15
	je	.L178
	movq	%r15, %rdi
	call	EVP_CIPHER_CTX_cipher@PLT
	movq	%rax, -1080(%rbp)
.L173:
	cmpq	$0, 1296(%r12)
	je	.L178
	cmpq	$0, -1080(%rbp)
	je	.L178
	movq	%r15, %rdi
	call	EVP_CIPHER_CTX_cipher@PLT
	movq	%rax, %rdi
	call	EVP_CIPHER_block_size@PLT
	cmpq	$1, -1048(%rbp)
	cltq
	movq	%rax, -1040(%rbp)
	ja	.L181
.L183:
	movl	-1016(%rbp), %ecx
	cmpq	$1, -1040(%rbp)
	leaq	6104(%r12), %r14
	leaq	-496(%rbp), %r13
	setne	-1030(%rbp)
	movzbl	-1030(%rbp), %esi
	movl	$0, -1028(%rbp)
	testl	%ecx, %ecx
	setne	%al
	andl	%esi, %eax
	testl	%ecx, %ecx
	movq	-1048(%rbp), %rsi
	movb	%al, -1029(%rbp)
	leaq	6112(%r12), %rax
	cmovne	%rax, %r14
	movq	%rax, -1072(%rbp)
	movq	-1056(%rbp), %rax
	leaq	4(%rax), %rbx
	leaq	-1008(%rbp), %rax
	movq	%r14, -1064(%rbp)
	movq	%rax, -1088(%rbp)
	movq	%rax, %r14
	leaq	(%rsi,%rsi,2), %rax
	leaq	(%rsi,%rax,4), %rax
	addq	%r13, %rax
	movq	%rax, -1024(%rbp)
	movq	%r13, %rax
	movq	%r14, %r13
	movq	%rax, %r14
	jmp	.L182
	.p2align 4,,10
	.p2align 3
.L292:
	movq	8(%r12), %rax
	movl	-1016(%rbp), %r8d
	movq	192(%rax), %rax
	movl	96(%rax), %eax
	andl	$8, %eax
	testl	%r8d, %r8d
	je	.L185
	testl	%eax, %eax
	je	.L187
	movq	6128(%r12), %rax
	movzwl	2(%rax), %eax
	movzbl	%ah, %edx
	movb	%dl, -65(%rbp)
	movq	-1072(%rbp), %rdx
.L189:
	movb	%al, -64(%rbp)
	movl	2(%rdx), %eax
	movl	%eax, -63(%rbp)
	movzwl	6(%rdx), %eax
	movw	%ax, -59(%rbp)
	movq	-65(%rbp), %rax
	movq	%rax, (%r14)
.L190:
	movl	(%rbx), %eax
	movl	$22, %esi
	movq	%r15, %rdi
	movb	%al, 8(%r14)
	movl	(%r12), %eax
	movzbl	%ah, %ecx
	movb	%al, 10(%r14)
	movq	4(%rbx), %rax
	movb	%cl, 9(%r14)
	movq	%r14, %rcx
	movq	%rax, %rdx
	movb	%al, 12(%r14)
	shrq	$8, %rdx
	movb	%dl, 11(%r14)
	movl	$13, %edx
	call	EVP_CIPHER_CTX_ctrl@PLT
	movl	%eax, -1028(%rbp)
	testl	%eax, %eax
	jle	.L290
	movl	-1016(%rbp), %edi
	movq	0(%r13), %rax
	testl	%edi, %edi
	je	.L192
	movslq	-1028(%rbp), %rdx
	addq	%rdx, 4(%rbx)
	addq	%rdx, %rax
	movq	%rax, 0(%r13)
.L193:
	addq	$80, %rbx
	addq	$8, %r13
	addq	$13, %r14
	cmpq	-1024(%rbp), %r14
	je	.L291
.L182:
	movq	4(%rbx), %rax
	movq	%r15, %rdi
	movq	%rax, 0(%r13)
	call	EVP_CIPHER_CTX_cipher@PLT
	movq	%rax, %rdi
	call	EVP_CIPHER_flags@PLT
	testl	$2097152, %eax
	jne	.L292
	cmpb	$0, -1029(%rbp)
	jne	.L293
	movl	-1016(%rbp), %esi
	testl	%esi, %esi
	jne	.L193
	movq	0(%r13), %rax
.L192:
	testq	%rax, %rax
	je	.L199
	xorl	%edx, %edx
	divq	-1040(%rbp)
	testq	%rdx, %rdx
	je	.L193
.L199:
	xorl	%r15d, %r15d
	jmp	.L167
	.p2align 4,,10
	.p2align 3
.L288:
	movq	1160(%rdi), %rdi
	call	EVP_MD_CTX_md@PLT
	testq	%rax, %rax
	je	.L171
	movq	1160(%r12), %rdi
	call	EVP_MD_CTX_md@PLT
	movq	%rax, %rdi
	call	EVP_MD_size@PLT
	testl	%eax, %eax
	js	.L294
.L171:
	movq	1136(%r12), %r15
	testq	%r15, %r15
	je	.L178
	movq	%r15, %rdi
	call	EVP_CIPHER_CTX_cipher@PLT
	movq	%rax, -1080(%rbp)
	movq	8(%r12), %rax
	movq	192(%rax), %rax
	testb	$1, 96(%rax)
	je	.L173
	movq	-1080(%rbp), %rdi
	call	EVP_CIPHER_flags@PLT
	andl	$983047, %eax
	cmpq	$2, %rax
	jne	.L173
	movq	-1080(%rbp), %rdi
	call	EVP_CIPHER_iv_length@PLT
	movl	%eax, %r13d
	cmpl	$1, %eax
	jle	.L173
	movq	-1056(%rbp), %rax
	xorl	%ebx, %ebx
	movq	%r12, -1024(%rbp)
	movq	%rbx, %r12
	movq	-1048(%rbp), %rbx
	leaq	32(%rax), %r14
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L174:
	movl	%r13d, %esi
	call	RAND_bytes@PLT
	testl	%eax, %eax
	jle	.L295
	addq	$1, %r12
	addq	$80, %r14
	cmpq	%r12, %rbx
	je	.L296
.L176:
	movq	8(%r14), %rdi
	cmpq	%rdi, (%r14)
	je	.L174
	movq	-1024(%rbp), %r12
	movl	$982, %r9d
.L283:
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
.L285:
	movl	$401, %edx
	movl	$80, %esi
	movq	%r12, %rdi
	call	ossl_statem_fatal@PLT
.L284:
	movl	$-1, %r15d
	jmp	.L167
	.p2align 4,,10
	.p2align 3
.L178:
	movq	-1048(%rbp), %rax
	movq	-1056(%rbp), %rbx
	leaq	(%rax,%rax,4), %r12
	addq	$8, %rbx
	salq	$4, %r12
	addq	%rbx, %r12
	.p2align 4,,10
	.p2align 3
.L180:
	movq	32(%rbx), %rsi
	movq	24(%rbx), %rdi
	addq	$80, %rbx
	movq	-80(%rbx), %rdx
	call	memmove@PLT
	movq	-56(%rbx), %rax
	movq	%rax, -48(%rbx)
	cmpq	%r12, %rbx
	jne	.L180
.L286:
	movl	$1, %r15d
.L167:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L297
	addq	$1048, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L287:
	.cfi_restore_state
	movl	$949, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	xorl	%r15d, %r15d
	movl	$401, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L167
	.p2align 4,,10
	.p2align 3
.L185:
	testl	%eax, %eax
	je	.L187
	movq	6128(%r12), %rax
	movzwl	(%rax), %eax
	movzbl	%ah, %edx
	movb	%dl, -65(%rbp)
	movq	-1064(%rbp), %rdx
	jmp	.L189
	.p2align 4,,10
	.p2align 3
.L293:
	movq	0(%r13), %rsi
	movq	-1040(%rbp), %rcx
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rcx
	subq	%rdx, %rcx
	movq	%rcx, %rdx
	cmpq	$256, %rcx
	ja	.L298
	leaq	(%rsi,%rcx), %rax
	leal	-1(%rcx), %edi
	cmpq	%rax, %rsi
	jnb	.L196
	.p2align 4,,10
	.p2align 3
.L197:
	movq	36(%rbx), %rax
	movb	%dil, (%rax,%rsi)
	movq	0(%r13), %rax
	addq	$1, %rsi
	addq	%rdx, %rax
	cmpq	%rsi, %rax
	ja	.L197
.L196:
	addq	%rdx, 4(%rbx)
	movq	%rax, 0(%r13)
	jmp	.L193
	.p2align 4,,10
	.p2align 3
.L187:
	movq	-1064(%rbp), %rcx
	movq	(%rcx), %rax
	movq	%rax, (%r14)
	movzbl	7(%rcx), %eax
	movb	%al, -1028(%rbp)
	addl	$1, %eax
	movb	%al, 7(%rcx)
	testb	%al, %al
	jne	.L190
	movzbl	6(%rcx), %eax
	movb	%al, -1028(%rbp)
	addl	$1, %eax
	movb	%al, 6(%rcx)
	testb	%al, %al
	jne	.L190
	movzbl	5(%rcx), %eax
	movb	%al, -1028(%rbp)
	addl	$1, %eax
	movb	%al, 5(%rcx)
	testb	%al, %al
	jne	.L190
	movzbl	4(%rcx), %eax
	movb	%al, -1028(%rbp)
	addl	$1, %eax
	movb	%al, 4(%rcx)
	testb	%al, %al
	jne	.L190
	movzbl	3(%rcx), %eax
	movb	%al, -1028(%rbp)
	addl	$1, %eax
	movb	%al, 3(%rcx)
	testb	%al, %al
	jne	.L190
	movzbl	2(%rcx), %eax
	movb	%al, -1028(%rbp)
	addl	$1, %eax
	movb	%al, 2(%rcx)
	testb	%al, %al
	jne	.L190
	movzbl	1(%rcx), %eax
	movb	%al, -1028(%rbp)
	addl	$1, %eax
	movb	%al, 1(%rcx)
	testb	%al, %al
	jne	.L190
	addb	$1, (%rcx)
	jmp	.L190
	.p2align 4,,10
	.p2align 3
.L181:
	movq	%r15, %rdi
	call	EVP_CIPHER_CTX_cipher@PLT
	movq	%rax, %rdi
	call	EVP_CIPHER_flags@PLT
	testl	$8388608, %eax
	jne	.L183
	movl	$1025, %r9d
	leaq	.LC0(%rip), %r8
	movl	$406, %ecx
	jmp	.L285
	.p2align 4,,10
	.p2align 3
.L291:
	movq	-1048(%rbp), %rax
	cmpq	$1, %rax
	je	.L200
	movq	-1056(%rbp), %rsi
	leaq	-752(%rbp), %rcx
	leaq	(%rcx,%rax,8), %rbx
	movq	%rcx, %r14
	movq	%rcx, %rax
	leaq	32(%rsi), %rdx
	.p2align 4,,10
	.p2align 3
.L201:
	movq	(%rdx), %rsi
	addq	$8, %rax
	addq	$80, %rdx
	movq	%rsi, -8(%rax)
	cmpq	%rbx, %rax
	jne	.L201
	movl	-1048(%rbp), %edx
	movl	$34, %esi
	movq	%r15, %rdi
	movq	%rcx, -1024(%rbp)
	call	EVP_CIPHER_CTX_ctrl@PLT
	movq	-1024(%rbp), %rcx
	testl	%eax, %eax
	jle	.L299
	movq	-1056(%rbp), %rax
	addq	$40, %rax
	.p2align 4,,10
	.p2align 3
.L204:
	movq	(%rax), %rdx
	addq	$8, %r14
	addq	$80, %rax
	movq	%rdx, -8(%r14)
	cmpq	%rbx, %r14
	jne	.L204
	movl	-1048(%rbp), %edx
	movl	$35, %esi
	movq	%r15, %rdi
	call	EVP_CIPHER_CTX_ctrl@PLT
	testl	%eax, %eax
	jle	.L206
	movq	-1088(%rbp), %rcx
	movl	-1048(%rbp), %edx
	movl	$36, %esi
	movq	%r15, %rdi
	call	EVP_CIPHER_CTX_ctrl@PLT
	testl	%eax, %eax
	jle	.L206
.L200:
	movq	-1056(%rbp), %rax
	movl	-1008(%rbp), %ecx
	movq	%r15, %rdi
	movq	40(%rax), %rdx
	movq	32(%rax), %rsi
	call	EVP_Cipher@PLT
	movq	%r15, %rdi
	movl	%eax, %r14d
	call	EVP_CIPHER_CTX_cipher@PLT
	movq	%rax, %rdi
	call	EVP_CIPHER_flags@PLT
	testl	$1048576, %eax
	jne	.L300
	testl	%r14d, %r14d
	sete	%r14b
.L208:
	testb	%r14b, %r14b
	jne	.L284
	movl	-1016(%rbp), %ecx
	testl	%ecx, %ecx
	je	.L301
	movq	168(%r12), %rax
	movq	(%rax), %rax
	testb	$1, %ah
	jne	.L286
	movq	1112(%r12), %rdi
	call	EVP_MD_CTX_md@PLT
	testq	%rax, %rax
	je	.L286
.L226:
	movq	1112(%r12), %rdi
	call	EVP_MD_CTX_md@PLT
	movq	%rax, %rdi
	call	EVP_MD_size@PLT
	movslq	%eax, %r14
	testl	%r14d, %r14d
	js	.L302
.L218:
	movl	-1016(%rbp), %edx
	testl	%edx, %edx
	sete	%al
	cmpb	$0, -1030(%rbp)
	je	.L220
	testb	%al, %al
	je	.L220
	movq	-1048(%rbp), %rax
	movq	-1056(%rbp), %rsi
	movl	$1, %r15d
	movq	-1040(%rbp), %rbx
	leaq	(%rax,%rax,4), %rax
	salq	$4, %rax
	leaq	(%rax,%rsi), %r13
	movq	%r13, -1016(%rbp)
	movq	%rsi, %r13
	jmp	.L221
	.p2align 4,,10
	.p2align 3
.L304:
	movl	%eax, %edx
	xorl	$-2, %ecx
	addq	$80, %r13
	xorl	$1, %edx
	leal	-1(%rdx), %eax
	andl	%ecx, %eax
	sarl	$31, %eax
	movl	%eax, %edx
	notl	%eax
	andl	%edx, %r15d
	orl	%eax, %r15d
	cmpq	-1016(%rbp), %r13
	je	.L303
.L221:
	movq	%r14, %rcx
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	tls1_cbc_remove_padding
	movl	%eax, %ecx
	testl	%eax, %eax
	jne	.L304
	jmp	.L199
.L300:
	shrl	$31, %r14d
	jmp	.L208
.L290:
	movl	$1065, %r9d
	jmp	.L283
.L301:
	movq	-1080(%rbp), %rdi
	call	EVP_CIPHER_flags@PLT
	andl	$983047, %eax
	cmpq	$6, %rax
	jne	.L210
	movq	-1048(%rbp), %rax
	leaq	-1(%rax), %rdx
	cmpq	$1, %rdx
	jbe	.L229
	movq	%rdx, %rcx
	movq	-1056(%rbp), %rax
	movdqa	.LC1(%rip), %xmm2
	shrq	%rcx
	movdqa	.LC2(%rip), %xmm3
	leaq	(%rcx,%rcx,4), %rcx
	addq	$8, %rax
	salq	$5, %rcx
	addq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L212:
	movdqu	104(%rax), %xmm0
	movdqu	24(%rax), %xmm1
	addq	$160, %rax
	paddq	%xmm2, %xmm0
	paddq	%xmm2, %xmm1
	movups	%xmm0, -56(%rax)
	movq	-160(%rax), %xmm0
	movups	%xmm1, -136(%rax)
	movhps	-80(%rax), %xmm0
	paddq	%xmm3, %xmm0
	movq	%xmm0, -160(%rax)
	movhps	%xmm0, -80(%rax)
	cmpq	%rcx, %rax
	jne	.L212
	movq	%rdx, %rax
	andq	$-2, %rax
.L211:
	leaq	(%rax,%rax,4), %rcx
	movq	-1056(%rbp), %rsi
	addq	$1, %rax
	salq	$4, %rcx
	leaq	(%rsi,%rcx), %rdx
	movdqu	32(%rdx), %xmm0
	subq	$8, 8(%rdx)
	paddq	%xmm2, %xmm0
	movups	%xmm0, 32(%rdx)
	cmpq	%rax, -1048(%rbp)
	jbe	.L213
	leaq	80(%rsi,%rcx), %rax
	movdqu	32(%rax), %xmm4
	subq	$8, 8(%rax)
	paddq	%xmm4, %xmm2
	movups	%xmm2, 32(%rax)
.L213:
	movq	168(%r12), %rax
	movq	(%rax), %rax
	testb	$1, %ah
	je	.L305
.L215:
	xorl	%r14d, %r14d
	jmp	.L218
.L303:
	movl	-1028(%rbp), %eax
	testl	%eax, %eax
	setne	%al
.L222:
	testb	%al, %al
	je	.L167
	movq	-1048(%rbp), %rsi
	movq	-1056(%rbp), %rax
	movslq	-1028(%rbp), %r12
	leaq	(%rsi,%rsi,4), %rdx
	addq	$8, %rax
	salq	$4, %rdx
	addq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L223:
	subq	%r12, (%rax)
	addq	$80, %rax
	cmpq	%rdx, %rax
	jne	.L223
	jmp	.L167
.L210:
	movq	-1080(%rbp), %rdi
	call	EVP_CIPHER_flags@PLT
	andl	$983047, %eax
	cmpq	$7, %rax
	jne	.L213
	movq	-1048(%rbp), %rax
	leaq	-1(%rax), %rdx
	cmpq	$1, %rdx
	jbe	.L230
	movq	%rdx, %rcx
	movq	-1056(%rbp), %rax
	movdqa	.LC1(%rip), %xmm2
	shrq	%rcx
	movdqa	.LC2(%rip), %xmm3
	leaq	(%rcx,%rcx,4), %rcx
	addq	$8, %rax
	salq	$5, %rcx
	addq	%rax, %rcx
.L217:
	movdqu	104(%rax), %xmm0
	movdqu	24(%rax), %xmm1
	addq	$160, %rax
	paddq	%xmm2, %xmm0
	paddq	%xmm2, %xmm1
	movups	%xmm0, -56(%rax)
	movq	-160(%rax), %xmm0
	movups	%xmm1, -136(%rax)
	movhps	-80(%rax), %xmm0
	paddq	%xmm3, %xmm0
	movq	%xmm0, -160(%rax)
	movhps	%xmm0, -80(%rax)
	cmpq	%rcx, %rax
	jne	.L217
	movq	%rdx, %rax
	andq	$-2, %rax
.L216:
	leaq	(%rax,%rax,4), %rdx
	movq	-1056(%rbp), %rsi
	addq	$1, %rax
	salq	$4, %rdx
	leaq	(%rsi,%rdx), %rcx
	movdqu	32(%rcx), %xmm0
	subq	$8, 8(%rcx)
	paddq	%xmm2, %xmm0
	movups	%xmm0, 32(%rcx)
	cmpq	%rax, -1048(%rbp)
	jbe	.L213
	leaq	80(%rsi,%rdx), %rax
	movdqu	32(%rax), %xmm5
	subq	$8, 8(%rax)
	paddq	%xmm5, %xmm2
	movups	%xmm2, 32(%rax)
	jmp	.L213
.L298:
	movl	$1081, %r9d
	jmp	.L283
.L305:
	movq	1112(%r12), %rdi
	call	EVP_MD_CTX_md@PLT
	testq	%rax, %rax
	je	.L215
	jmp	.L226
.L296:
	movq	-1024(%rbp), %r12
	jmp	.L173
.L295:
	movq	-1024(%rbp), %r12
	movl	$986, %r9d
	jmp	.L283
.L229:
	movdqa	.LC1(%rip), %xmm2
	xorl	%eax, %eax
	jmp	.L211
.L206:
	movl	$1119, %r9d
.L282:
	leaq	.LC0(%rip), %r8
	movl	$406, %ecx
	movl	$401, %edx
	movq	%r12, %rdi
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L284
.L230:
	movdqa	.LC1(%rip), %xmm2
	xorl	%eax, %eax
	jmp	.L216
.L297:
	call	__stack_chk_fail@PLT
.L289:
	movl	$997, %r9d
	jmp	.L283
.L294:
	movl	$958, %r9d
	jmp	.L283
.L299:
	movl	$1107, %r9d
	jmp	.L282
.L220:
	cmpl	$0, -1028(%rbp)
	movl	$1, %r15d
	setne	%dl
	andl	%edx, %eax
	jmp	.L222
.L302:
	movl	$1154, %r9d
	jmp	.L283
	.cfi_endproc
.LFE1046:
	.size	tls1_enc, .-tls1_enc
	.p2align 4
	.globl	ssl3_cbc_copy_mac
	.type	ssl3_cbc_copy_mac, @function
ssl3_cbc_copy_mac:
.LFB1051:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -200(%rbp)
	movq	16(%rsi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	$64, %r13
	movl	$64, %eax
	cmovbe	%r13, %rax
	xorl	%r8d, %r8d
	cmpq	%rax, %rdx
	jbe	.L324
.L306:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L325
	addq	$168, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L324:
	.cfi_restore_state
	movq	%rdx, %rbx
	movl	$0, %ecx
	movq	8(%rsi), %r12
	movq	%rsi, %r14
	leaq	-192(%rbp), %rax
	movq	%rax, %r8
	negq	%r8
	andl	$63, %r8d
	addq	%rax, %r8
	leaq	256(%rdx), %rax
	movq	%r13, %rdx
	subq	%rbx, %rdx
	movq	%r8, %rdi
	subq	$256, %rdx
	cmpq	%rax, %r13
	cmova	%rdx, %rcx
	movq	%rbx, %rdx
	xorl	%esi, %esi
	movq	%rcx, -208(%rbp)
	call	memset@PLT
	movq	-208(%rbp), %rcx
	movq	%rax, %r8
	cmpq	%rcx, %r13
	jbe	.L317
	movq	32(%r14), %rsi
	movq	%rcx, %rdx
	movq	%r12, %r15
	movq	%r13, %r10
	subq	%rbx, %r15
	subq	%r12, %rdx
	subq	%r12, %r10
	xorl	%eax, %eax
	addq	%r12, %rsi
	xorl	%ecx, %ecx
	xorl	%r11d, %r11d
	movq	%rsi, %r14
	.p2align 4,,10
	.p2align 3
.L310:
	leaq	(%r12,%rdx), %r9
	movq	%r15, %rdi
	movq	%r12, %r13
	xorq	%r9, %rdi
	xorq	%rdx, %r13
	movq	%rdi, %rsi
	subq	$1, %rdi
	notq	%rsi
	andq	%rsi, %rdi
	movq	%rdi, %rsi
	movq	%r12, %rdi
	xorq	%r9, %rdi
	sarq	$63, %rsi
	orq	%r13, %rdi
	orq	%rsi, %r11
	andq	%rcx, %rsi
	xorq	%r9, %rdi
	orq	%rsi, %rax
	leaq	1(%rcx), %rsi
	sarq	$63, %rdi
	andq	%rdi, %r11
	movzbl	(%r14,%rdx), %edi
	addq	$1, %rdx
	andl	%r11d, %edi
	orb	%dil, (%r8,%rcx)
	movq	%rsi, %rcx
	movq	%rbx, %rdi
	subq	%rbx, %rcx
	xorq	%rsi, %rdi
	xorq	%rbx, %rcx
	orq	%rdi, %rcx
	xorq	%rsi, %rcx
	sarq	$63, %rcx
	andq	%rsi, %rcx
	cmpq	%rdx, %r10
	jne	.L310
.L309:
	movq	-200(%rbp), %rdi
	leaq	(%rdi,%rbx), %rsi
	testq	%rbx, %rbx
	je	.L313
	.p2align 4,,10
	.p2align 3
.L312:
	movq	%rax, %rdx
	movq	%rbx, %rcx
	addq	$1, %rdi
	xorq	$32, %rdx
	addq	%r8, %rdx
	movzbl	(%rdx), %edx
	leaq	1(%rax), %rdx
	movzbl	(%r8,%rax), %eax
	xorq	%rdx, %rcx
	movb	%al, -1(%rdi)
	movq	%rdx, %rax
	subq	%rbx, %rax
	xorq	%rbx, %rax
	orq	%rcx, %rax
	xorq	%rdx, %rax
	sarq	$63, %rax
	andq	%rdx, %rax
	cmpq	%rdi, %rsi
	jne	.L312
.L313:
	movl	$1, %r8d
	jmp	.L306
	.p2align 4,,10
	.p2align 3
.L317:
	xorl	%eax, %eax
	jmp	.L309
.L325:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1051:
	.size	ssl3_cbc_copy_mac, .-ssl3_cbc_copy_mac
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC3:
	.string	"GET "
.LC4:
	.string	"POST "
.LC5:
	.string	"HEAD "
.LC6:
	.string	"PUT "
.LC7:
	.string	"CONNE"
	.text
	.p2align 4
	.globl	ssl3_get_record
	.type	ssl3_get_record, @function
ssl3_get_record:
.LFB1042:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-200(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	3464(%rdi), %r12
	pushq	%rbx
	movq	%r12, %r14
	subq	$232, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	1544(%rdi), %rax
	movq	%r12, -240(%rbp)
	testq	%rax, %rax
	cmovne	%rax, %rdx
	movq	1296(%rdi), %rax
	xorl	%ebx, %ebx
	movq	%rdx, -216(%rbp)
	movq	%rax, -232(%rbp)
	movq	%r13, %rax
	movq	%rbx, %r13
	movq	%rax, %rbx
.L369:
	cmpl	$241, 2124(%r15)
	movq	%r14, -224(%rbp)
	jne	.L327
	cmpq	$4, 6032(%r15)
	ja	.L328
.L327:
	testq	%r13, %r13
	movq	2160(%r15), %rdx
	movq	%rbx, %r9
	movq	%r15, %rdi
	sete	%r8b
	movl	$5, %esi
	xorl	%ecx, %ecx
	movzbl	%r8b, %r8d
	call	ssl3_read_n@PLT
	testl	%eax, %eax
	jle	.L326
	movq	6032(%r15), %rax
	movl	$241, 2124(%r15)
	testq	%rax, %rax
	js	.L559
	cmpq	$2, %rax
	jbe	.L331
	movq	6024(%r15), %rcx
	movl	56(%r15), %edi
	movzbl	2(%rcx), %esi
	testl	%edi, %edi
	je	.L333
	movl	6120(%r15), %edx
	testl	%edx, %edx
	je	.L333
	movzwl	(%rcx), %edx
	rolw	$8, %dx
	cmpb	$1, %sil
	jne	.L333
	testw	%dx, %dx
	jns	.L333
	andl	$32767, %edx
	movl	$22, 4(%r14)
	movq	%rdx, 8(%r14)
	movq	2160(%r15), %rax
	movl	$2, (%r14)
	subq	$2, %rax
	cmpq	%rax, %rdx
	ja	.L560
	cmpq	$8, %rdx
	ja	.L335
	movl	$256, %r9d
	.p2align 4,,10
	.p2align 3
.L556:
	leaq	.LC0(%rip), %r8
	movl	$160, %ecx
	jmp	.L549
	.p2align 4,,10
	.p2align 3
.L333:
	movq	184(%r15), %r11
	testq	%r11, %r11
	je	.L561
	subq	$8, %rsp
	movq	%rax, -256(%rbp)
	xorl	%esi, %esi
	movq	%r15, %r9
	pushq	192(%r15)
	movl	$5, %r8d
	movl	$256, %edx
	xorl	%edi, %edi
	movq	%rcx, -248(%rbp)
	call	*%r11
	movq	-248(%rbp), %rcx
	movq	-256(%rbp), %rax
	popq	%r10
	popq	%r11
	movzbl	(%rcx), %esi
.L337:
	subq	$3, %rax
	cmpq	$1, %rax
	jbe	.L338
	movzwl	1(%rcx), %edi
	movzwl	3(%rcx), %edx
	movl	%esi, 4(%r14)
	rolw	$8, %di
	rolw	$8, %dx
	movzwl	%di, %eax
	movzwl	%dx, %edx
	movl	%eax, (%r14)
	movl	1520(%r15), %r9d
	movq	%rdx, 8(%r14)
	testl	%r9d, %r9d
	jne	.L340
	movq	8(%r15), %rcx
	movq	192(%rcx), %r8
	testb	$8, 96(%r8)
	jne	.L341
	movl	(%rcx), %ecx
	cmpl	$771, %ecx
	jle	.L341
	cmpl	$65536, %ecx
	je	.L341
	shrl	$8, %eax
	cmpl	$3, %eax
	jne	.L426
.L427:
	cmpq	$0, 1088(%r15)
	je	.L353
	cmpl	$23, %esi
	je	.L354
	cmpl	$20, %esi
	je	.L562
	cmpl	$21, %esi
	jne	.L356
	cmpl	$1, 128(%r15)
	jne	.L356
.L354:
	cmpw	$771, %di
	jne	.L563
.L353:
	movq	2160(%r15), %rax
	subq	$5, %rax
	cmpq	%rax, %rdx
	jbe	.L430
.L425:
	movl	$362, %r9d
	leaq	.LC0(%rip), %r8
	movl	$198, %ecx
	.p2align 4,,10
	.p2align 3
.L552:
	movl	$143, %edx
	movl	$22, %esi
	movq	%r15, %rdi
	call	ossl_statem_fatal@PLT
	movl	$-1, %eax
	.p2align 4,,10
	.p2align 3
.L326:
	movq	-56(%rbp), %rsi
	xorq	%fs:40, %rsi
	jne	.L564
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L328:
	.cfi_restore_state
	movq	8(%r14), %rdx
.L335:
	movq	8(%r15), %rax
	movq	192(%rax), %rcx
	testb	$8, 96(%rcx)
	jne	.L357
	movl	(%rax), %eax
	cmpl	$771, %eax
	jle	.L357
	cmpl	$65536, %eax
	je	.L357
.L430:
	cmpq	$16640, %rdx
	jbe	.L358
	movl	$373, %r9d
	leaq	.LC0(%rip), %r8
	movl	$150, %ecx
	jmp	.L552
	.p2align 4,,10
	.p2align 3
.L331:
	movl	$224, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
.L549:
	movl	$143, %edx
	movl	$50, %esi
	movq	%r15, %rdi
	call	ossl_statem_fatal@PLT
	movl	$-1, %eax
	jmp	.L326
	.p2align 4,,10
	.p2align 3
.L341:
	cmpl	$1, 1248(%r15)
	je	.L340
	movl	(%r15), %ecx
	cmpl	%eax, %ecx
	jne	.L565
.L340:
	shrl	$8, %eax
	cmpl	$3, %eax
	jne	.L426
	movq	8(%r15), %rax
	movq	192(%rax), %rcx
	testb	$8, 96(%rcx)
	je	.L566
.L352:
	movq	2160(%r15), %rax
	subq	$5, %rax
	cmpq	%rax, %rdx
	ja	.L425
.L357:
	cmpq	$16704, %rdx
	ja	.L567
.L358:
	leaq	-3(%rdx), %rsi
	cmpl	$2, (%r14)
	cmovne	%rdx, %rsi
	testq	%rsi, %rsi
	jne	.L568
.L360:
	movq	6024(%r15), %rax
	movl	$240, 2124(%r15)
	movq	%rdx, 16(%r14)
	leaq	2(%rax), %rcx
	addq	$5, %rax
	cmpl	$2, (%r14)
	movl	$0, 56(%r14)
	cmovne	%rax, %rcx
	addq	$1, %r13
	movq	%rcx, %xmm0
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 32(%r14)
	movq	$0, 6032(%r15)
	movl	$0, 6120(%r15)
	cmpq	-216(%rbp), %r13
	je	.L544
	cmpl	$23, 4(%r14)
	jne	.L544
	movq	8(%r15), %rax
	movq	192(%rax), %rax
	testb	$1, 96(%rax)
	je	.L367
	movq	1088(%r15), %rdi
	testq	%rdi, %rdi
	je	.L367
	call	EVP_CIPHER_CTX_cipher@PLT
	movq	%rax, %rdi
	call	EVP_CIPHER_flags@PLT
	testl	$8388608, %eax
	je	.L544
	movq	2144(%r15), %rax
	testq	%rax, %rax
	je	.L544
	movq	2176(%r15), %rdx
	cmpq	$4, %rdx
	jbe	.L544
	addq	2168(%r15), %rax
	cmpb	$23, (%rax)
	je	.L569
.L544:
	movq	168(%r15), %rax
	movq	%r13, %rbx
	cmpq	$1, %r13
	je	.L570
.L365:
	movq	(%rax), %rax
	testb	$1, %ah
	je	.L380
	movq	1112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L380
	call	EVP_MD_CTX_md@PLT
	movq	%rax, %rdi
	call	EVP_MD_size@PLT
	cmpl	$64, %eax
	ja	.L571
	movslq	%eax, %r13
	leaq	(%rbx,%rbx,4), %rax
	movq	%r12, -248(%rbp)
	salq	$4, %rax
	movq	%rbx, -224(%rbp)
	addq	%r12, %rax
	movq	%rax, -216(%rbp)
	leaq	-192(%rbp), %rax
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L385:
	movq	8(%r12), %rax
	cmpq	%r13, %rax
	jb	.L572
	subq	%r13, %rax
	xorl	%ecx, %ecx
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%rax, 8(%r12)
	addq	32(%r12), %rax
	movq	%r15, %rdi
	movq	%rax, %r14
	movq	8(%r15), %rax
	movq	192(%rax), %rax
	call	*8(%rax)
	testl	%eax, %eax
	je	.L384
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	CRYPTO_memcmp@PLT
	testl	%eax, %eax
	jne	.L384
	addq	$80, %r12
	cmpq	%r12, -216(%rbp)
	jne	.L385
	movq	-224(%rbp), %rbx
	movq	-248(%rbp), %r12
	.p2align 4,,10
	.p2align 3
.L380:
	movq	3472(%r15), %rax
	xorl	%ecx, %ecx
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	%rax, -224(%rbp)
	movq	8(%r15), %rax
	movq	192(%rax), %rax
	call	*(%rax)
	movl	%eax, %r13d
	testl	%eax, %eax
	je	.L573
	cmpq	$0, -232(%rbp)
	je	.L389
	cmpq	$0, 1088(%r15)
	je	.L389
	movq	168(%r15), %rax
	movq	(%rax), %rax
	testb	$1, %ah
	je	.L574
.L389:
	testl	%r13d, %r13d
	js	.L575
	leaq	(%rbx,%rbx,4), %r13
	leaq	3468(%r15), %r12
	movl	$512, %r14d
	salq	$4, %r13
	addq	%r12, %r13
	jmp	.L422
	.p2align 4,,10
	.p2align 3
.L583:
	movq	6064(%r15), %rax
	addq	$1, %rax
	movq	%rax, 6064(%r15)
	cmpq	$32, %rax
	ja	.L576
.L421:
	addq	$80, %r12
	cmpq	%r13, %r12
	je	.L577
.L422:
	cmpq	$0, 1128(%r15)
	movq	4(%r12), %rax
	je	.L407
	cmpq	$16384, %rax
	ja	.L578
	movq	8(%r15), %rdx
	movq	192(%rdx), %rcx
	testb	$8, 96(%rcx)
	jne	.L418
.L429:
	movl	(%rdx), %edx
	cmpl	$771, %edx
	jle	.L409
	cmpl	$65536, %edx
	je	.L409
	cmpq	$0, 1088(%r15)
	movl	(%r12), %edx
	je	.L428
	cmpl	$21, %edx
	jne	.L579
.L428:
	subl	$21, %edx
	cmpl	$1, %edx
	jbe	.L580
	.p2align 4,,10
	.p2align 3
.L409:
	cmpq	$16384, %rax
	ja	.L581
.L418:
	movq	1296(%r15), %rdx
	testq	%rdx, %rdx
	je	.L419
	movzbl	600(%rdx), %ecx
	leal	-1(%rcx), %edx
	cmpb	$3, %dl
	ja	.L419
	movl	%edx, %ecx
	movl	%r14d, %edx
	sall	%cl, %edx
	cmpq	%rax, %rdx
	jb	.L582
.L419:
	movq	$0, 20(%r12)
	testq	%rax, %rax
	je	.L583
	movq	$0, 6064(%r15)
	jmp	.L421
	.p2align 4,,10
	.p2align 3
.L559:
	movl	$217, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
.L548:
	movl	$143, %edx
	movl	$80, %esi
	movq	%r15, %rdi
	call	ossl_statem_fatal@PLT
	movl	$-1, %eax
	jmp	.L326
	.p2align 4,,10
	.p2align 3
.L569:
	movzwl	3(%rax), %eax
	addq	$80, %r14
	rolw	$8, %ax
	movzwl	%ax, %eax
	addq	$5, %rax
	cmpq	%rax, %rdx
	jnb	.L369
	jmp	.L544
	.p2align 4,,10
	.p2align 3
.L573:
	movq	%r15, %rdi
	call	ossl_statem_in_error@PLT
	testl	%eax, %eax
	jne	.L553
	cmpq	$1, %rbx
	jne	.L387
	movq	%r15, %rdi
	call	ossl_statem_skip_early_data@PLT
	testl	%eax, %eax
	je	.L387
	movq	3472(%r15), %rsi
	xorl	%ecx, %ecx
	movl	$104, %edx
	movq	%r15, %rdi
	call	early_data_count_ok
	testl	%eax, %eax
	je	.L553
.L406:
	movq	$0, 3472(%r15)
	leaq	2112(%r15), %rdi
	movl	$1, 3520(%r15)
	movq	$1, 2128(%r15)
	call	RECORD_LAYER_reset_read_sequence@PLT
	movl	$1, %eax
	jmp	.L326
	.p2align 4,,10
	.p2align 3
.L561:
	movzbl	(%rcx), %esi
	jmp	.L337
	.p2align 4,,10
	.p2align 3
.L407:
	movq	8(%r15), %rdx
	movq	192(%rdx), %rcx
	testb	$8, 96(%rcx)
	jne	.L409
	jmp	.L429
	.p2align 4,,10
	.p2align 3
.L568:
	xorl	%r8d, %r8d
	movq	%rbx, %r9
	movl	$1, %ecx
	movq	%rsi, %rdx
	movq	%r15, %rdi
	call	ssl3_read_n@PLT
	testl	%eax, %eax
	jle	.L326
	movq	8(%r14), %rdx
	jmp	.L360
	.p2align 4,,10
	.p2align 3
.L570:
	movq	-224(%rbp), %rsi
	cmpl	$20, 4(%rsi)
	jne	.L365
	movq	8(%r15), %rdx
	movq	192(%rdx), %rcx
	testb	$8, 96(%rcx)
	jne	.L370
	movl	(%rdx), %edx
	cmpl	$65536, %edx
	je	.L370
	cmpl	$771, %edx
	jle	.L370
.L371:
	cmpq	$0, 408(%rax)
	je	.L373
	cmpq	$0, 544(%rax)
	jne	.L365
.L373:
	movq	-224(%rbp), %rax
	cmpq	$1, 8(%rax)
	jne	.L374
	movq	32(%rax), %rax
	cmpb	$1, (%rax)
	jne	.L374
	movq	-224(%rbp), %rax
	movl	$22, 4(%rax)
	movq	6064(%r15), %rax
	addq	$1, %rax
	movq	%rax, 6064(%r15)
	cmpq	$32, %rax
	ja	.L584
	movq	-224(%rbp), %rax
	movl	$1, 56(%rax)
	movl	$1, %eax
	movq	$1, 2128(%r15)
	jmp	.L326
	.p2align 4,,10
	.p2align 3
.L580:
	testq	%rax, %rax
	jne	.L409
	movl	$737, %r9d
	leaq	.LC0(%rip), %r8
	movl	$271, %ecx
	.p2align 4,,10
	.p2align 3
.L551:
	movl	$143, %edx
	movl	$10, %esi
	movq	%r15, %rdi
	call	ossl_statem_fatal@PLT
.L553:
	movl	$-1, %eax
	jmp	.L326
	.p2align 4,,10
	.p2align 3
.L579:
	testq	%rax, %rax
	je	.L434
	cmpl	$23, %edx
	jne	.L434
	movq	28(%r12), %rdx
	subq	$1, %rax
	je	.L433
.L413:
	cmpb	$0, (%rdx,%rax)
	leaq	(%rdx,%rax), %r11
	je	.L585
.L414:
	movq	%rax, 4(%r12)
	movzbl	(%r11), %edx
	movl	%edx, %ecx
	movl	%edx, (%r12)
	subl	$21, %ecx
	cmpb	$2, %cl
	ja	.L586
	movq	184(%r15), %r10
	testq	%r10, %r10
	je	.L428
	subq	$8, %rsp
	movl	(%r15), %esi
	pushq	192(%r15)
	xorl	%edi, %edi
	movq	%r11, %rcx
	movl	$257, %edx
	movq	%r15, %r9
	movl	$1, %r8d
	call	*%r10
	movq	8(%r15), %rax
	popq	%rcx
	popq	%rsi
	movq	192(%rax), %rdx
	testb	$8, 96(%rdx)
	jne	.L587
	movl	(%rax), %edx
	movq	4(%r12), %rax
	cmpl	$771, %edx
	jle	.L409
	cmpl	$65536, %edx
	je	.L409
	movl	(%r12), %edx
	jmp	.L428
	.p2align 4,,10
	.p2align 3
.L566:
	movl	(%rax), %eax
	cmpl	$65536, %eax
	je	.L352
	cmpl	$771, %eax
	jle	.L352
	jmp	.L427
	.p2align 4,,10
	.p2align 3
.L338:
	movl	$270, %r9d
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	jmp	.L549
	.p2align 4,,10
	.p2align 3
.L575:
	movq	%r15, %rdi
	call	ossl_statem_in_error@PLT
	testl	%eax, %eax
	jne	.L553
	cmpq	$1, %rbx
	jne	.L405
	movq	%r15, %rdi
	call	ossl_statem_skip_early_data@PLT
	testl	%eax, %eax
	je	.L405
	movq	-224(%rbp), %rsi
	xorl	%ecx, %ecx
	movl	$104, %edx
	movq	%r15, %rdi
	call	early_data_count_ok
	testl	%eax, %eax
	jne	.L406
	jmp	.L553
	.p2align 4,,10
	.p2align 3
.L384:
	movl	$520, %r9d
.L555:
	leaq	.LC0(%rip), %r8
	movl	$281, %ecx
.L554:
	movl	$143, %edx
	movl	$20, %esi
	movq	%r15, %rdi
	call	ossl_statem_fatal@PLT
	movl	$-1, %eax
	jmp	.L326
	.p2align 4,,10
	.p2align 3
.L585:
	subq	$1, %rax
	jne	.L413
.L433:
	movq	%rdx, %r11
	jmp	.L414
	.p2align 4,,10
	.p2align 3
.L577:
	cmpl	$11, 132(%r15)
	je	.L423
.L424:
	movq	%rbx, 2128(%r15)
	movl	$1, %eax
	jmp	.L326
	.p2align 4,,10
	.p2align 3
.L370:
	movl	1248(%r15), %edi
	testl	%edi, %edi
	je	.L365
	jmp	.L371
	.p2align 4,,10
	.p2align 3
.L581:
	movl	$743, %r9d
.L550:
	leaq	.LC0(%rip), %r8
	movl	$146, %ecx
	jmp	.L552
	.p2align 4,,10
	.p2align 3
.L574:
	movq	1112(%r15), %rdi
	call	EVP_MD_CTX_md@PLT
	testq	%rax, %rax
	je	.L389
	movq	1112(%r15), %rdi
	call	EVP_MD_CTX_md@PLT
	movq	%rax, %rdi
	call	EVP_MD_size@PLT
	movslq	%eax, %r14
	cmpq	$64, %r14
	ja	.L588
	leaq	(%rbx,%rbx,4), %rax
	movq	%rbx, -264(%rbp)
	movq	%r15, %rbx
	movq	-240(%rbp), %r15
	salq	$4, %rax
	addq	%r12, %rax
	movq	%rax, -248(%rbp)
	leaq	1(%r14), %rax
	movq	%rax, -256(%rbp)
	jmp	.L403
	.p2align 4,,10
	.p2align 3
.L395:
	movq	8(%r15), %r12
	subq	%r14, %r12
	movq	%r12, 8(%r15)
	addq	32(%r15), %r12
	movq	%r12, -232(%rbp)
	sete	-216(%rbp)
.L397:
	movq	8(%rbx), %rax
	leaq	-192(%rbp), %r12
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	movq	%r12, %rdx
	movq	%rbx, %rdi
	movq	192(%rax), %rax
	call	*8(%rax)
	testl	%eax, %eax
	je	.L401
	cmpb	$0, -216(%rbp)
	jne	.L401
	movq	-232(%rbp), %rsi
	movq	%r14, %rdx
	movq	%r12, %rdi
	call	CRYPTO_memcmp@PLT
	testl	%eax, %eax
	je	.L400
	.p2align 4,,10
	.p2align 3
.L401:
	movl	$-1, %r13d
.L400:
	leaq	16384(%r14), %rax
	cmpq	%rax, 8(%r15)
	movl	$-1, %eax
	cmova	%eax, %r13d
	addq	$80, %r15
	cmpq	%r15, -248(%rbp)
	je	.L589
.L403:
	cmpq	%r14, 16(%r15)
	jb	.L394
	movq	1088(%rbx), %rdi
	call	EVP_CIPHER_CTX_cipher@PLT
	movq	%rax, %rdi
	call	EVP_CIPHER_flags@PLT
	andl	$983047, %eax
	cmpq	$2, %rax
	jne	.L393
	movq	-256(%rbp), %rax
	cmpq	%rax, 16(%r15)
	jb	.L394
.L393:
	movq	1088(%rbx), %rdi
	call	EVP_CIPHER_CTX_cipher@PLT
	movq	%rax, %rdi
	call	EVP_CIPHER_flags@PLT
	andl	$983047, %eax
	cmpq	$2, %rax
	jne	.L395
	leaq	-128(%rbp), %r12
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	ssl3_cbc_copy_mac
	testl	%eax, %eax
	je	.L590
	subq	%r14, 8(%r15)
	movb	$0, -216(%rbp)
	movq	%r12, -232(%rbp)
	jmp	.L397
	.p2align 4,,10
	.p2align 3
.L578:
	movl	$687, %r9d
	leaq	.LC0(%rip), %r8
	movl	$140, %ecx
	jmp	.L552
	.p2align 4,,10
	.p2align 3
.L572:
	movl	$512, %r9d
	jmp	.L556
.L576:
	movl	$770, %r9d
	leaq	.LC0(%rip), %r8
	movl	$298, %ecx
	jmp	.L551
.L367:
	movq	168(%r15), %rax
	movq	%r13, %rbx
	jmp	.L365
.L423:
	cmpl	$23, 3468(%r15)
	jne	.L424
	movq	3472(%r15), %rsi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r15, %rdi
	call	early_data_count_ok
	testl	%eax, %eax
	jne	.L424
	jmp	.L553
	.p2align 4,,10
	.p2align 3
.L582:
	movl	$751, %r9d
	jmp	.L550
.L394:
	movl	$603, %r9d
	leaq	.LC0(%rip), %r8
	movl	$160, %ecx
	movq	%rbx, %rdi
	movl	$143, %edx
	movl	$50, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L553
.L567:
	movl	$390, %r9d
	leaq	.LC0(%rip), %r8
	movl	$150, %ecx
	jmp	.L552
.L589:
	movq	%rbx, %r15
	movq	-264(%rbp), %rbx
	jmp	.L389
.L562:
	movq	168(%r15), %rax
	cmpq	$0, 408(%rax)
	je	.L354
	cmpq	$0, 544(%rax)
	je	.L354
.L356:
	movl	$349, %r9d
	leaq	.LC0(%rip), %r8
	movl	$443, %ecx
	jmp	.L551
.L426:
	movl	6120(%r15), %r8d
	testl	%r8d, %r8d
	je	.L347
	movq	6024(%r15), %rax
	movl	$4, %ecx
	leaq	.LC3(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L348
	movl	$5, %ecx
	leaq	.LC4(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L348
	movl	$5, %ecx
	leaq	.LC5(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L348
	movl	$4, %ecx
	leaq	.LC6(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L348
	movq	%rax, %rsi
	movl	$5, %ecx
	leaq	.LC7(%rip), %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L350
	movl	$325, %r9d
	leaq	.LC0(%rip), %r8
	movl	$155, %ecx
.L557:
	movl	$143, %edx
	movl	$-1, %esi
	movq	%r15, %rdi
	call	ossl_statem_fatal@PLT
	movl	$-1, %eax
	jmp	.L326
.L387:
	movl	$562, %r9d
	leaq	.LC0(%rip), %r8
	movl	$129, %ecx
	jmp	.L554
.L434:
	movl	$705, %r9d
	leaq	.LC0(%rip), %r8
	movl	$443, %ecx
	jmp	.L551
.L405:
	movl	$676, %r9d
	jmp	.L555
.L350:
	movl	$331, %r9d
	leaq	.LC0(%rip), %r8
	movl	$267, %ecx
	jmp	.L557
.L565:
	xorl	%eax, %ecx
	andb	$-1, %ch
	jne	.L344
	cmpq	$0, 1136(%r15)
	jne	.L344
	cmpq	$0, 1160(%r15)
	jne	.L344
	movl	$298, %r9d
	leaq	.LC0(%rip), %r8
	movl	$267, %ecx
	cmpl	$21, %esi
	je	.L557
	movl	%eax, (%r15)
.L344:
	movl	$307, %r9d
.L558:
	leaq	.LC0(%rip), %r8
	movl	$267, %ecx
	movl	$143, %edx
	movq	%r15, %rdi
	movl	$70, %esi
	call	ossl_statem_fatal@PLT
	orl	$-1, %eax
	jmp	.L326
.L590:
	movq	%rbx, %r15
	movl	$617, %r9d
.L547:
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movl	$143, %edx
	movq	%r15, %rdi
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L553
.L586:
	movl	$720, %r9d
	leaq	.LC0(%rip), %r8
	movl	$443, %ecx
	jmp	.L551
.L374:
	movl	$472, %r9d
	leaq	.LC0(%rip), %r8
	movl	$260, %ecx
	movq	%r15, %rdi
	movl	$143, %edx
	movl	$47, %esi
	call	ossl_statem_fatal@PLT
	orl	$-1, %eax
	jmp	.L326
.L571:
	movl	$503, %r9d
	leaq	.LC0(%rip), %r8
	movl	$6, %ecx
	jmp	.L548
.L564:
	call	__stack_chk_fail@PLT
.L563:
	movl	$354, %r9d
	leaq	.LC0(%rip), %r8
	movl	$267, %ecx
	jmp	.L549
.L584:
	movl	$484, %r9d
	leaq	.LC0(%rip), %r8
	movl	$262, %ecx
	jmp	.L551
.L560:
	movl	$250, %r9d
	leaq	.LC0(%rip), %r8
	movl	$198, %ecx
	jmp	.L552
.L348:
	movl	$321, %r9d
	leaq	.LC0(%rip), %r8
	movl	$156, %ecx
	jmp	.L557
.L347:
	movl	$335, %r9d
	jmp	.L558
.L588:
	movl	$586, %r9d
	jmp	.L547
.L587:
	movq	4(%r12), %rax
	jmp	.L409
	.cfi_endproc
.LFE1042:
	.size	ssl3_get_record, .-ssl3_get_record
	.p2align 4
	.globl	dtls1_process_record
	.type	dtls1_process_record, @function
dtls1_process_record:
.LFB1052:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1638, %r9d
	movl	$150, %ecx
	leaq	.LC0(%rip), %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$184, %rsp
	.cfi_offset 3, -56
	movq	3472(%rdi), %rdx
	movq	1296(%rdi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	6024(%rdi), %rax
	addq	$13, %rax
	movq	%rax, 3504(%rdi)
	cmpq	$16704, %rdx
	ja	.L640
	movq	%rax, 3496(%rdi)
	movq	168(%rdi), %rax
	movq	%rsi, %r13
	leaq	3464(%rdi), %r14
	movq	%rdx, 3480(%rdi)
	movq	(%rax), %rax
	testb	$1, %ah
	je	.L594
	movq	1112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L594
	call	EVP_MD_CTX_md@PLT
	movq	%rax, %rdi
	call	EVP_MD_size@PLT
	movslq	%eax, %r8
	cmpq	$64, %r8
	ja	.L642
	cmpq	%r8, 3480(%r12)
	jb	.L643
	movq	3496(%r12), %rax
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	3472(%r12), %r9
	leaq	-192(%rbp), %r15
	movq	%r8, -208(%rbp)
	movq	%rax, -216(%rbp)
	movq	8(%r12), %rax
	movq	%r15, %rdx
	subq	%r8, %r9
	movq	192(%rax), %rax
	movq	%r9, 3472(%r12)
	movq	%r9, -200(%rbp)
	call	*8(%rax)
	movq	-200(%rbp), %r9
	movq	-208(%rbp), %r8
	testl	%eax, %eax
	je	.L598
	movq	-216(%rbp), %rsi
	movq	%r8, %rdx
	movq	%r15, %rdi
	addq	%r9, %rsi
	call	CRYPTO_memcmp@PLT
	testl	%eax, %eax
	jne	.L598
	.p2align 4,,10
	.p2align 3
.L594:
	movq	8(%r12), %rax
	xorl	%ecx, %ecx
	movl	$1, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	192(%rax), %rax
	call	*(%rax)
	movl	%eax, %r15d
	testl	%eax, %eax
	je	.L644
	testq	%rbx, %rbx
	je	.L613
	movq	168(%r12), %rax
	movq	(%rax), %rax
	testb	$1, %ah
	je	.L645
.L613:
	testl	%r15d, %r15d
	js	.L612
	cmpq	$0, 1128(%r12)
	movq	3472(%r12), %rax
	je	.L614
	movl	$1775, %r9d
	leaq	.LC0(%rip), %r8
	movl	$140, %ecx
	cmpq	$16384, %rax
	ja	.L640
	movq	1296(%r12), %rdx
	testq	%rdx, %rdx
	je	.L617
	movzbl	600(%rdx), %ecx
	leal	-1(%rcx), %edx
	cmpb	$3, %dl
	ja	.L617
.L618:
	subl	$1, %ecx
	movl	$512, %edx
	sall	%cl, %edx
.L616:
	cmpq	%rax, %rdx
	jnb	.L617
	movl	$1792, %r9d
	leaq	.LC0(%rip), %r8
	movl	$146, %ecx
.L640:
	movl	$257, %edx
	movl	$22, %esi
	movq	%r12, %rdi
	call	ossl_statem_fatal@PLT
.L641:
	xorl	%r15d, %r15d
.L591:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L646
	addq	$184, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L644:
	.cfi_restore_state
	movq	%r12, %rdi
	call	ossl_statem_in_error@PLT
	testl	%eax, %eax
	jne	.L591
.L612:
	movq	$0, 3472(%r12)
	xorl	%r15d, %r15d
	movq	$0, 6032(%r12)
	jmp	.L591
	.p2align 4,,10
	.p2align 3
.L645:
	cmpq	$0, 1088(%r12)
	je	.L613
	movq	1112(%r12), %rdi
	call	EVP_MD_CTX_md@PLT
	testq	%rax, %rax
	je	.L613
	movq	1112(%r12), %rdi
	call	EVP_MD_CTX_md@PLT
	movq	%rax, %rdi
	call	EVP_MD_size@PLT
	testl	%eax, %eax
	js	.L647
	movslq	%eax, %rbx
	movl	$1713, %r9d
	cmpl	$64, %eax
	jg	.L639
	cmpq	%rbx, 3480(%r12)
	jb	.L606
	movq	1088(%r12), %rdi
	call	EVP_CIPHER_CTX_cipher@PLT
	movq	%rax, %rdi
	call	EVP_CIPHER_flags@PLT
	andl	$983047, %eax
	cmpq	$2, %rax
	jne	.L605
	leaq	1(%rbx), %rax
	cmpq	%rax, 3480(%r12)
	jb	.L606
.L605:
	movq	1088(%r12), %rdi
	call	EVP_CIPHER_CTX_cipher@PLT
	movq	%rax, %rdi
	call	EVP_CIPHER_flags@PLT
	andl	$983047, %eax
	cmpq	$2, %rax
	je	.L648
	movq	3472(%r12), %rax
	subq	%rbx, %rax
	movq	%rax, 3472(%r12)
	addq	3496(%r12), %rax
	movq	%rax, -216(%rbp)
	sete	-200(%rbp)
.L609:
	movq	8(%r12), %rax
	movq	%r14, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	-192(%rbp), %r8
	leaq	16384(%rbx), %r14
	movq	192(%rax), %rax
	movq	%r8, -208(%rbp)
	movq	%r8, %rdx
	call	*8(%rax)
	testl	%eax, %eax
	je	.L612
	cmpb	$0, -200(%rbp)
	jne	.L612
	movq	-208(%rbp), %r8
	movq	-216(%rbp), %rsi
	movq	%rbx, %rdx
	movq	%r8, %rdi
	call	CRYPTO_memcmp@PLT
	testl	%eax, %eax
	jne	.L612
	cmpq	%r14, 3472(%r12)
	jbe	.L613
	jmp	.L612
	.p2align 4,,10
	.p2align 3
.L598:
	movl	$281, %ecx
	movl	$257, %edx
	movq	%r12, %rdi
	xorl	%r15d, %r15d
	movl	$1664, %r9d
	leaq	.LC0(%rip), %r8
	movl	$20, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L591
	.p2align 4,,10
	.p2align 3
.L617:
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1, %r15d
	movq	$0, 3488(%r12)
	movq	$0, 6032(%r12)
	call	dtls1_record_bitmap_update@PLT
	jmp	.L591
	.p2align 4,,10
	.p2align 3
.L614:
	movq	1296(%r12), %rcx
	movl	$16384, %edx
	testq	%rcx, %rcx
	je	.L616
	movzbl	600(%rcx), %ecx
	leal	-1(%rcx), %esi
	cmpb	$3, %sil
	ja	.L616
	jmp	.L618
	.p2align 4,,10
	.p2align 3
.L642:
	movl	$68, %ecx
	movl	$257, %edx
	movq	%r12, %rdi
	xorl	%r15d, %r15d
	movl	$1651, %r9d
	leaq	.LC0(%rip), %r8
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L591
	.p2align 4,,10
	.p2align 3
.L643:
	movl	$160, %ecx
	movl	$257, %edx
	movq	%r12, %rdi
	xorl	%r15d, %r15d
	movl	$1656, %r9d
	leaq	.LC0(%rip), %r8
	movl	$50, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L591
	.p2align 4,,10
	.p2align 3
.L606:
	movl	$1728, %r9d
	leaq	.LC0(%rip), %r8
	movl	$160, %ecx
	movq	%r12, %rdi
	movl	$257, %edx
	movl	$50, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L641
.L649:
	movl	$1742, %r9d
.L639:
	leaq	.LC0(%rip), %r8
	movl	$68, %ecx
	movl	$257, %edx
	movq	%r12, %rdi
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L641
.L647:
	movl	$1707, %r9d
	leaq	.LC0(%rip), %r8
	movl	$6, %ecx
	movq	%r12, %rdi
	movl	$257, %edx
	movl	$80, %esi
	call	ossl_statem_fatal@PLT
	jmp	.L641
.L648:
	leaq	-128(%rbp), %rcx
	movq	%rbx, %rdx
	movq	%r14, %rsi
	movq	%rcx, %rdi
	movq	%rcx, -200(%rbp)
	call	ssl3_cbc_copy_mac
	movq	-200(%rbp), %rcx
	testl	%eax, %eax
	je	.L649
	subq	%rbx, 3472(%r12)
	movb	$0, -200(%rbp)
	movq	%rcx, -216(%rbp)
	jmp	.L609
.L646:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1052:
	.size	dtls1_process_record, .-dtls1_process_record
	.p2align 4
	.globl	dtls1_get_record
	.type	dtls1_get_record, @function
dtls1_get_record:
.LFB1053:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	leaq	3464(%rdi), %r14
	pushq	%r13
	.cfi_offset 13, -32
	leaq	-52(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L651:
	movq	%rbx, %rdi
	call	dtls1_process_buffered_records@PLT
	testl	%eax, %eax
	je	.L652
.L700:
	movq	6128(%rbx), %rax
	movq	%rbx, %rdi
	leaq	56(%rax), %rsi
	call	dtls1_retrieve_buffered_record@PLT
	testl	%eax, %eax
	jne	.L674
	cmpl	$241, 2124(%rbx)
	jne	.L655
	movq	6032(%rbx), %rax
	cmpq	$12, %rax
	jbe	.L655
	movq	3472(%rbx), %r12
	subq	$13, %rax
	cmpq	%r12, %rax
	jnb	.L666
.L702:
	leaq	-48(%rbp), %r9
	movl	$1, %r8d
	movq	%r12, %rdx
	movq	%r12, %rsi
	movl	$1, %ecx
	movq	%rbx, %rdi
	call	ssl3_read_n@PLT
	testl	%eax, %eax
	jle	.L699
	cmpq	%r12, -48(%rbp)
	je	.L666
.L699:
	movq	%rbx, %rdi
	call	ossl_statem_in_error@PLT
	testl	%eax, %eax
	jne	.L652
.L662:
	movq	$0, 3472(%rbx)
	movq	%rbx, %rdi
	movl	$1, 3520(%rbx)
	movq	$0, 6032(%rbx)
	call	dtls1_process_buffered_records@PLT
	testl	%eax, %eax
	jne	.L700
.L652:
	movl	$-1, %eax
.L650:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L701
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L655:
	.cfi_restore_state
	movq	2160(%rbx), %rdx
	xorl	%ecx, %ecx
	leaq	-48(%rbp), %r9
	movq	%rbx, %rdi
	movl	$1, %r8d
	movl	$13, %esi
	call	ssl3_read_n@PLT
	testl	%eax, %eax
	jle	.L650
	cmpq	$13, 6032(%rbx)
	jne	.L695
	movq	184(%rbx), %rax
	movq	6024(%rbx), %r12
	movl	$241, 2124(%rbx)
	testq	%rax, %rax
	je	.L659
	subq	$8, %rsp
	movq	%rbx, %r9
	pushq	192(%rbx)
	movq	%r12, %rcx
	movl	$13, %r8d
	movl	$256, %edx
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	*%rax
	popq	%r8
	popq	%r9
.L659:
	movzbl	(%r12), %ecx
	movl	1520(%rbx), %edi
	movl	(%rbx), %esi
	movl	%ecx, 3468(%rbx)
	movzbl	1(%r12), %eax
	sall	$8, %eax
	movl	%eax, %edx
	movzbl	2(%r12), %eax
	orl	%edx, %eax
	movzwl	3(%r12), %edx
	movzwl	%ax, %eax
	rolw	$8, %dx
	movzwl	%dx, %edx
	movq	%rdx, 3528(%rbx)
	movl	5(%r12), %edx
	movl	%edx, 6106(%rbx)
	movzwl	9(%r12), %edx
	movw	%dx, 6110(%rbx)
	movzwl	11(%r12), %edx
	movl	$0, 3520(%rbx)
	rolw	$8, %dx
	movzwl	%dx, %r12d
	movq	%r12, 3472(%rbx)
	testl	%edi, %edi
	jne	.L660
	cmpl	$21, %ecx
	je	.L660
	cmpl	%esi, %eax
	jne	.L662
.L661:
	cmpw	$16704, %dx
	ja	.L662
	movq	1296(%rbx), %rax
	testq	%rax, %rax
	je	.L697
	movzbl	600(%rax), %ecx
	leal	-1(%rcx), %eax
	cmpb	$3, %al
	ja	.L697
	subl	$1, %ecx
	movl	$512, %eax
	sall	%cl, %eax
	addl	$320, %eax
	cmpq	%rax, %r12
	ja	.L662
.L697:
	movq	6032(%rbx), %rax
	subq	$13, %rax
	cmpq	%r12, %rax
	jb	.L702
.L666:
	movl	$240, 2124(%rbx)
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	dtls1_get_bitmap@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L703
	movq	%rax, %rsi
	movq	%rbx, %rdi
	call	dtls1_record_replay_check@PLT
	testl	%eax, %eax
	je	.L662
	cmpq	$0, 3472(%rbx)
	je	.L704
	movl	-52(%rbp), %eax
	testl	%eax, %eax
	je	.L670
	movq	%rbx, %rdi
	call	SSL_in_init@PLT
	testl	%eax, %eax
	je	.L671
.L673:
	movq	6128(%rbx), %rax
	leaq	3536(%rbx), %rdx
	movq	%rbx, %rdi
	leaq	40(%rax), %rsi
	call	dtls1_buffer_record@PLT
	testl	%eax, %eax
	jns	.L662
	jmp	.L652
	.p2align 4,,10
	.p2align 3
.L703:
	movq	$0, 3472(%rbx)
	.p2align 4,,10
	.p2align 3
.L695:
	movq	$0, 6032(%rbx)
	jmp	.L651
	.p2align 4,,10
	.p2align 3
.L660:
	xorl	%esi, %eax
	testb	$-1, %ah
	je	.L661
	jmp	.L662
	.p2align 4,,10
	.p2align 3
.L704:
	movl	$1, 3520(%rbx)
	jmp	.L651
	.p2align 4,,10
	.p2align 3
.L670:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	dtls1_process_record
	testl	%eax, %eax
	je	.L699
	.p2align 4,,10
	.p2align 3
.L674:
	movl	$1, %eax
	jmp	.L650
	.p2align 4,,10
	.p2align 3
.L671:
	movq	%rbx, %rdi
	call	ossl_statem_get_in_handshake@PLT
	testl	%eax, %eax
	jne	.L673
	jmp	.L662
.L701:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1053:
	.size	dtls1_get_record, .-dtls1_get_record
	.p2align 4
	.globl	dtls_buffer_listen_record
	.type	dtls_buffer_listen_record, @function
dtls_buffer_listen_record:
.LFB1054:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movups	%xmm0, 3464(%rdi)
	movups	%xmm0, 3480(%rdi)
	movq	%rsi, 3472(%rdi)
	addq	$13, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movups	%xmm0, 3496(%rdi)
	movups	%xmm0, 3528(%rdi)
	movl	$22, 3468(%rdi)
	movups	%xmm0, 3512(%rdi)
	movq	(%rdx), %rax
	leaq	3536(%rdi), %rdx
	movq	%rsi, 6032(%rdi)
	movq	%rax, 3536(%rdi)
	movq	2144(%rdi), %rax
	movq	%rcx, 3488(%rdi)
	movq	%rax, 6024(%rdi)
	addq	$13, %rax
	movq	%rax, 3496(%rdi)
	movq	6128(%rdi), %rax
	leaq	56(%rax), %rsi
	call	dtls1_buffer_record@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	testl	%eax, %eax
	setg	%al
	movzbl	%al, %eax
	ret
	.cfi_endproc
.LFE1054:
	.size	dtls_buffer_listen_record, .-dtls_buffer_listen_record
	.section	.rodata
	.align 32
	.type	ssl3_pad_2, @object
	.size	ssl3_pad_2, 48
ssl3_pad_2:
	.ascii	"\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\"
	.ascii	"\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\"
	.align 32
	.type	ssl3_pad_1, @object
	.size	ssl3_pad_1, 48
ssl3_pad_1:
	.ascii	"666666666666666666666666666666666666666666666666"
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC1:
	.quad	8
	.quad	8
	.align 16
.LC2:
	.quad	-8
	.quad	-8
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
