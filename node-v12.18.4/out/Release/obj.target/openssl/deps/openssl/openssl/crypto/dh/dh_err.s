	.file	"dh_err.c"
	.text
	.p2align 4
	.globl	ERR_load_DH_strings
	.type	ERR_load_DH_strings, @function
ERR_load_DH_strings:
.LFB203:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$84303872, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_func_error_string@PLT
	testq	%rax, %rax
	je	.L5
	movl	$1, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	.cfi_restore_state
	leaq	DH_str_functs(%rip), %rdi
	call	ERR_load_strings_const@PLT
	leaq	DH_str_reasons(%rip), %rdi
	call	ERR_load_strings_const@PLT
	movl	$1, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE203:
	.size	ERR_load_DH_strings, .-ERR_load_DH_strings
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"bad generator"
.LC1:
	.string	"bn decode error"
.LC2:
	.string	"bn error"
.LC3:
	.string	"check invalid j value"
.LC4:
	.string	"check invalid q value"
.LC5:
	.string	"check pubkey invalid"
.LC6:
	.string	"check pubkey too large"
.LC7:
	.string	"check pubkey too small"
.LC8:
	.string	"check p not prime"
.LC9:
	.string	"check p not safe prime"
.LC10:
	.string	"check q not prime"
.LC11:
	.string	"decode error"
.LC12:
	.string	"invalid parameter name"
.LC13:
	.string	"invalid parameter nid"
.LC14:
	.string	"invalid public key"
.LC15:
	.string	"kdf parameter error"
.LC16:
	.string	"keys not set"
.LC17:
	.string	"missing pubkey"
.LC18:
	.string	"modulus too large"
.LC19:
	.string	"not suitable generator"
.LC20:
	.string	"no parameters set"
.LC21:
	.string	"no private value"
.LC22:
	.string	"parameter encoding error"
.LC23:
	.string	"peer key error"
.LC24:
	.string	"shared info error"
.LC25:
	.string	"unable to check generator"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	DH_str_reasons, @object
	.size	DH_str_reasons, 432
DH_str_reasons:
	.quad	83886181
	.quad	.LC0
	.quad	83886189
	.quad	.LC1
	.quad	83886186
	.quad	.LC2
	.quad	83886195
	.quad	.LC3
	.quad	83886196
	.quad	.LC4
	.quad	83886202
	.quad	.LC5
	.quad	83886203
	.quad	.LC6
	.quad	83886204
	.quad	.LC7
	.quad	83886197
	.quad	.LC8
	.quad	83886198
	.quad	.LC9
	.quad	83886199
	.quad	.LC10
	.quad	83886184
	.quad	.LC11
	.quad	83886190
	.quad	.LC12
	.quad	83886194
	.quad	.LC13
	.quad	83886182
	.quad	.LC14
	.quad	83886192
	.quad	.LC15
	.quad	83886188
	.quad	.LC16
	.quad	83886205
	.quad	.LC17
	.quad	83886183
	.quad	.LC18
	.quad	83886200
	.quad	.LC19
	.quad	83886187
	.quad	.LC20
	.quad	83886180
	.quad	.LC21
	.quad	83886185
	.quad	.LC22
	.quad	83886191
	.quad	.LC23
	.quad	83886193
	.quad	.LC24
	.quad	83886201
	.quad	.LC25
	.quad	0
	.quad	0
	.section	.rodata.str1.1
.LC26:
	.string	"compute_key"
.LC27:
	.string	"DHparams_print_fp"
.LC28:
	.string	"dh_builtin_genparams"
.LC29:
	.string	"DH_check_ex"
.LC30:
	.string	"DH_check_params_ex"
.LC31:
	.string	"DH_check_pub_key_ex"
.LC32:
	.string	"dh_cms_decrypt"
.LC33:
	.string	"dh_cms_set_peerkey"
.LC34:
	.string	"dh_cms_set_shared_info"
.LC35:
	.string	"DH_meth_dup"
.LC36:
	.string	"DH_meth_new"
.LC37:
	.string	"DH_meth_set1_name"
.LC38:
	.string	"DH_new_by_nid"
.LC39:
	.string	"DH_new_method"
.LC40:
	.string	"dh_param_decode"
.LC41:
	.string	"dh_pkey_public_check"
.LC42:
	.string	"dh_priv_decode"
.LC43:
	.string	"dh_priv_encode"
.LC44:
	.string	"dh_pub_decode"
.LC45:
	.string	"dh_pub_encode"
.LC46:
	.string	"do_dh_print"
.LC47:
	.string	"generate_key"
.LC48:
	.string	"pkey_dh_ctrl_str"
.LC49:
	.string	"pkey_dh_derive"
.LC50:
	.string	"pkey_dh_init"
.LC51:
	.string	"pkey_dh_keygen"
	.section	.data.rel.ro.local
	.align 32
	.type	DH_str_functs, @object
	.size	DH_str_functs, 432
DH_str_functs:
	.quad	84303872
	.quad	.LC26
	.quad	84299776
	.quad	.LC27
	.quad	84320256
	.quad	.LC28
	.quad	84381696
	.quad	.LC29
	.quad	84385792
	.quad	.LC30
	.quad	84389888
	.quad	.LC31
	.quad	84353024
	.quad	.LC32
	.quad	84357120
	.quad	.LC33
	.quad	84361216
	.quad	.LC34
	.quad	84365312
	.quad	.LC35
	.quad	84369408
	.quad	.LC36
	.quad	84373504
	.quad	.LC37
	.quad	84312064
	.quad	.LC38
	.quad	84316160
	.quad	.LC39
	.quad	84324352
	.quad	.LC40
	.quad	84393984
	.quad	.LC41
	.quad	84336640
	.quad	.LC42
	.quad	84340736
	.quad	.LC43
	.quad	84328448
	.quad	.LC44
	.quad	84332544
	.quad	.LC45
	.quad	84295680
	.quad	.LC46
	.quad	84307968
	.quad	.LC47
	.quad	84377600
	.quad	.LC48
	.quad	84344832
	.quad	.LC49
	.quad	84398080
	.quad	.LC50
	.quad	84348928
	.quad	.LC51
	.quad	0
	.quad	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
