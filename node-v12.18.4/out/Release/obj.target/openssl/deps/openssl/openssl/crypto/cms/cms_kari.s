	.file	"cms_kari.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/cms/cms_kari.c"
	.text
	.p2align 4
	.type	cms_kek_cipher.isra.0, @function
cms_kek_cipher.isra.0:
.LFB1478:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$136, %rsp
	movq	%rdi, -152(%rbp)
	movq	(%r9), %rdi
	movq	%rsi, -160(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	EVP_CIPHER_CTX_key_length@PLT
	xorl	%r8d, %r8d
	cltq
	movq	%rax, -136(%rbp)
	cmpq	$64, %rax
	jbe	.L15
.L1:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L16
	addq	$136, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	.cfi_restore_state
	leaq	-128(%rbp), %r15
	movq	(%r12), %rdi
	leaq	-136(%rbp), %rdx
	movq	%r15, %rsi
	call	EVP_PKEY_derive@PLT
	testl	%eax, %eax
	jle	.L5
	movl	16(%rbp), %r9d
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rcx
	call	EVP_CipherInit_ex@PLT
	testl	%eax, %eax
	jne	.L17
.L5:
	movq	-136(%rbp), %rsi
	movq	%r15, %rdi
	call	OPENSSL_cleanse@PLT
	xorl	%r9d, %r9d
.L4:
	movl	$220, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r9, %rdi
	call	CRYPTO_free@PLT
	xorl	%r8d, %r8d
.L9:
	movq	(%rbx), %rdi
	movl	%r8d, -152(%rbp)
	call	EVP_CIPHER_CTX_reset@PLT
	movq	(%r12), %rdi
	call	EVP_PKEY_CTX_free@PLT
	movq	$0, (%r12)
	movl	-152(%rbp), %r8d
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L17:
	leaq	-140(%rbp), %r11
	movq	(%rbx), %rdi
	xorl	%esi, %esi
	movl	%r14d, %r8d
	movq	%r13, %rcx
	movq	%r11, %rdx
	movq	%r11, -168(%rbp)
	call	EVP_CipherUpdate@PLT
	testl	%eax, %eax
	je	.L5
	movslq	-140(%rbp), %rdi
	movl	$208, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r9
	testq	%rax, %rax
	je	.L7
	movq	-168(%rbp), %r11
	movq	(%rbx), %rdi
	movl	%r14d, %r8d
	movq	%r13, %rcx
	movq	%rax, %rsi
	movq	%rax, -168(%rbp)
	movq	%r11, %rdx
	call	EVP_CipherUpdate@PLT
	movq	-168(%rbp), %r9
	testl	%eax, %eax
	je	.L7
	movq	-152(%rbp), %rax
	movq	-160(%rbp), %rcx
	movq	%r15, %rdi
	movq	-136(%rbp), %rsi
	movq	%r9, (%rax)
	movslq	-140(%rbp), %rax
	movq	%rax, (%rcx)
	call	OPENSSL_cleanse@PLT
	movl	$1, %r8d
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L7:
	movq	-136(%rbp), %rsi
	movq	%r15, %rdi
	movq	%r9, -152(%rbp)
	call	OPENSSL_cleanse@PLT
	movq	-152(%rbp), %r9
	jmp	.L4
.L16:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1478:
	.size	cms_kek_cipher.isra.0, .-cms_kek_cipher.isra.0
	.p2align 4
	.globl	CMS_RecipientInfo_kari_get0_alg
	.type	CMS_RecipientInfo_kari_get0_alg, @function
CMS_RecipientInfo_kari_get0_alg:
.LFB1464:
	.cfi_startproc
	endbr64
	movl	(%rdi), %eax
	cmpl	$1, %eax
	jne	.L32
	testq	%rsi, %rsi
	je	.L21
	movq	8(%rdi), %rcx
	movq	24(%rcx), %rcx
	movq	%rcx, (%rsi)
.L21:
	testq	%rdx, %rdx
	je	.L29
	movq	8(%rdi), %rcx
	movq	16(%rcx), %rcx
	movq	%rcx, (%rdx)
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$181, %edx
	movl	$175, %esi
	movl	$46, %edi
	movl	$27, %r8d
	leaq	.LC0(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1464:
	.size	CMS_RecipientInfo_kari_get0_alg, .-CMS_RecipientInfo_kari_get0_alg
	.p2align 4
	.globl	CMS_RecipientInfo_kari_get0_reks
	.type	CMS_RecipientInfo_kari_get0_reks, @function
CMS_RecipientInfo_kari_get0_reks:
.LFB1465:
	.cfi_startproc
	endbr64
	cmpl	$1, (%rdi)
	jne	.L40
	movq	8(%rdi), %rax
	movq	32(%rax), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L40:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$181, %edx
	movl	$172, %esi
	movl	$46, %edi
	movl	$44, %r8d
	leaq	.LC0(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1465:
	.size	CMS_RecipientInfo_kari_get0_reks, .-CMS_RecipientInfo_kari_get0_reks
	.p2align 4
	.globl	CMS_RecipientInfo_kari_get0_orig_id
	.type	CMS_RecipientInfo_kari_get0_orig_id, @function
CMS_RecipientInfo_kari_get0_orig_id:
.LFB1466:
	.cfi_startproc
	endbr64
	movl	(%rdi), %eax
	cmpl	$1, %eax
	jne	.L87
	movq	8(%rdi), %rdi
	movq	8(%rdi), %rdi
	testq	%r8, %r8
	je	.L44
	movq	$0, (%r8)
.L44:
	testq	%r9, %r9
	je	.L45
	movq	$0, (%r9)
.L45:
	testq	%rcx, %rcx
	je	.L46
	movq	$0, (%rcx)
.L46:
	testq	%rsi, %rsi
	je	.L47
	movq	$0, (%rsi)
.L47:
	testq	%rdx, %rdx
	je	.L48
	movq	$0, (%rdx)
.L48:
	movl	(%rdi), %r10d
	testl	%r10d, %r10d
	jne	.L49
	testq	%r8, %r8
	je	.L50
	movq	8(%rdi), %rdx
	movq	(%rdx), %rdx
	movq	%rdx, (%r8)
.L50:
	testq	%r9, %r9
	je	.L84
	movq	8(%rdi), %rdx
	movq	8(%rdx), %rdx
	movq	%rdx, (%r9)
	ret
	.p2align 4,,10
	.p2align 3
.L49:
	cmpl	$1, %r10d
	je	.L88
	cmpl	$2, %r10d
	jne	.L55
	testq	%rsi, %rsi
	je	.L54
	movq	8(%rdi), %rcx
	movq	(%rcx), %rcx
	movq	%rcx, (%rsi)
.L54:
	testq	%rdx, %rdx
	je	.L84
	movq	8(%rdi), %rcx
	movq	8(%rcx), %rcx
	movq	%rcx, (%rdx)
	ret
	.p2align 4,,10
	.p2align 3
.L84:
	ret
	.p2align 4,,10
	.p2align 3
.L87:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$181, %edx
	movl	$173, %esi
	movl	$46, %edi
	movl	$60, %r8d
	leaq	.LC0(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L55:
	.cfi_restore 6
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L88:
	testq	%rcx, %rcx
	je	.L84
	movq	8(%rdi), %rax
	movq	%rax, (%rcx)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE1466:
	.size	CMS_RecipientInfo_kari_get0_orig_id, .-CMS_RecipientInfo_kari_get0_orig_id
	.p2align 4
	.globl	CMS_RecipientInfo_kari_orig_id_cmp
	.type	CMS_RecipientInfo_kari_orig_id_cmp, @function
CMS_RecipientInfo_kari_orig_id_cmp:
.LFB1467:
	.cfi_startproc
	endbr64
	cmpl	$1, (%rdi)
	jne	.L98
	movq	8(%rdi), %rax
	movq	8(%rax), %rax
	movl	(%rax), %edx
	testl	%edx, %edx
	je	.L99
	cmpl	$1, %edx
	jne	.L93
	movq	8(%rax), %rdi
	jmp	cms_keyid_cert_cmp@PLT
	.p2align 4,,10
	.p2align 3
.L99:
	movq	8(%rax), %rdi
	jmp	cms_ias_cert_cmp@PLT
	.p2align 4,,10
	.p2align 3
.L93:
	movl	$-1, %eax
	ret
.L98:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$181, %edx
	movl	$174, %esi
	movl	$46, %edi
	movl	$97, %r8d
	leaq	.LC0(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ERR_put_error@PLT
	movl	$-2, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1467:
	.size	CMS_RecipientInfo_kari_orig_id_cmp, .-CMS_RecipientInfo_kari_orig_id_cmp
	.p2align 4
	.globl	CMS_RecipientEncryptedKey_get0_id
	.type	CMS_RecipientEncryptedKey_get0_id, @function
CMS_RecipientEncryptedKey_get0_id:
.LFB1468:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdi
	movl	(%rdi), %eax
	testl	%eax, %eax
	jne	.L101
	testq	%r8, %r8
	je	.L102
	movq	8(%rdi), %rax
	movq	(%rax), %rax
	movq	%rax, (%r8)
.L102:
	testq	%r9, %r9
	je	.L103
	movq	8(%rdi), %rax
	movq	8(%rax), %rax
	movq	%rax, (%r9)
.L103:
	testq	%rsi, %rsi
	je	.L104
	movq	$0, (%rsi)
.L104:
	testq	%rdx, %rdx
	je	.L105
	movq	$0, (%rdx)
.L105:
	testq	%rcx, %rcx
	je	.L141
	movq	$0, (%rcx)
.L141:
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L101:
	cmpl	$1, %eax
	jne	.L113
	testq	%rsi, %rsi
	je	.L108
	movq	8(%rdi), %r10
	movq	(%r10), %r10
	movq	%r10, (%rsi)
.L108:
	testq	%rdx, %rdx
	je	.L109
	movq	8(%rdi), %rsi
	movq	8(%rsi), %rsi
	movq	%rsi, (%rdx)
.L109:
	testq	%rcx, %rcx
	je	.L110
	movq	8(%rdi), %rdx
	movq	16(%rdx), %rdx
	movq	%rdx, (%rcx)
.L110:
	testq	%r8, %r8
	je	.L111
	movq	$0, (%r8)
.L111:
	testq	%r9, %r9
	je	.L141
	movq	$0, (%r9)
	ret
	.p2align 4,,10
	.p2align 3
.L113:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1468:
	.size	CMS_RecipientEncryptedKey_get0_id, .-CMS_RecipientEncryptedKey_get0_id
	.p2align 4
	.globl	CMS_RecipientEncryptedKey_cert_cmp
	.type	CMS_RecipientEncryptedKey_cert_cmp, @function
CMS_RecipientEncryptedKey_cert_cmp:
.LFB1469:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movl	(%rax), %edx
	testl	%edx, %edx
	je	.L145
	cmpl	$1, %edx
	jne	.L144
	movq	8(%rax), %rax
	movq	(%rax), %rdi
	jmp	cms_keyid_cert_cmp@PLT
	.p2align 4,,10
	.p2align 3
.L145:
	movq	8(%rax), %rdi
	jmp	cms_ias_cert_cmp@PLT
	.p2align 4,,10
	.p2align 3
.L144:
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE1469:
	.size	CMS_RecipientEncryptedKey_cert_cmp, .-CMS_RecipientEncryptedKey_cert_cmp
	.p2align 4
	.globl	CMS_RecipientInfo_kari_set0_pkey
	.type	CMS_RecipientInfo_kari_set0_pkey, @function
CMS_RecipientInfo_kari_set0_pkey:
.LFB1470:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	$1, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	8(%rdi), %rbx
	movq	40(%rbx), %rdi
	call	EVP_PKEY_CTX_free@PLT
	movq	$0, 40(%rbx)
	testq	%r12, %r12
	je	.L146
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	EVP_PKEY_CTX_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L149
	movq	%rax, %rdi
	call	EVP_PKEY_derive_init@PLT
	testl	%eax, %eax
	jle	.L149
	movq	%r12, 40(%rbx)
.L146:
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L149:
	.cfi_restore_state
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	call	EVP_PKEY_CTX_free@PLT
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1470:
	.size	CMS_RecipientInfo_kari_set0_pkey, .-CMS_RecipientInfo_kari_set0_pkey
	.p2align 4
	.globl	CMS_RecipientInfo_kari_get0_ctx
	.type	CMS_RecipientInfo_kari_get0_ctx, @function
CMS_RecipientInfo_kari_get0_ctx:
.LFB1471:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpl	$1, (%rdi)
	je	.L156
	ret
	.p2align 4,,10
	.p2align 3
.L156:
	movq	8(%rdi), %rax
	movq	48(%rax), %rax
	ret
	.cfi_endproc
.LFE1471:
	.size	CMS_RecipientInfo_kari_get0_ctx, .-CMS_RecipientInfo_kari_get0_ctx
	.p2align 4
	.globl	CMS_RecipientInfo_kari_decrypt
	.type	CMS_RecipientInfo_kari_decrypt, @function
CMS_RecipientInfo_kari_decrypt:
.LFB1473:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movl	$1, %esi
	movq	%rbx, %rdi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8(%rdx), %rax
	movq	$0, -72(%rbp)
	movl	(%rax), %r14d
	movq	8(%rax), %r15
	call	cms_env_asn1_ctrl@PLT
	testl	%eax, %eax
	jne	.L158
	movq	-72(%rbp), %rdi
	movl	%eax, %r12d
.L159:
	movl	$252, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L163
	leaq	-40(%rbp), %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L158:
	.cfi_restore_state
	subq	$8, %rsp
	movq	8(%rbx), %rax
	movq	%r15, %rdx
	movslq	%r14d, %rcx
	pushq	$0
	leaq	-64(%rbp), %rsi
	leaq	-72(%rbp), %rdi
	leaq	48(%rax), %r9
	leaq	40(%rax), %r8
	call	cms_kek_cipher.isra.0
	movl	%eax, %r12d
	popq	%rax
	popq	%rdx
	testl	%r12d, %r12d
	jne	.L160
	movq	-72(%rbp), %rdi
	jmp	.L159
	.p2align 4,,10
	.p2align 3
.L160:
	movq	8(%r13), %rax
	movl	$246, %ecx
	leaq	.LC0(%rip), %rdx
	movl	$1, %r12d
	movq	24(%rax), %rbx
	movq	32(%rbx), %rdi
	movq	40(%rbx), %rsi
	call	CRYPTO_clear_free@PLT
	movq	-72(%rbp), %rax
	xorl	%edi, %edi
	movq	$0, -72(%rbp)
	movq	%rax, 32(%rbx)
	movq	-64(%rbp), %rax
	movq	%rax, 40(%rbx)
	jmp	.L159
.L163:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1473:
	.size	CMS_RecipientInfo_kari_decrypt, .-CMS_RecipientInfo_kari_decrypt
	.p2align 4
	.globl	cms_RecipientInfo_kari_init
	.type	cms_RecipientInfo_kari_init, @function
cms_RecipientInfo_kari_init:
.LFB1475:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%ecx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	leaq	CMS_KeyAgreeRecipientInfo_it(%rip), %rdi
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	ASN1_item_new@PLT
	movq	%rax, 8(%r12)
	testq	%rax, %rax
	je	.L195
	movl	$1, (%r12)
	leaq	CMS_RecipientEncryptedKey_it(%rip), %rdi
	movq	%rax, %rbx
	movl	$3, (%rax)
	call	ASN1_item_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L195
	movq	32(%rbx), %rdi
	movq	%rax, %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L196
	andl	$65536, %r14d
	movq	(%r12), %rdx
	jne	.L197
	movl	$0, (%rdx)
	leaq	8(%rdx), %rdi
	movq	%r15, %rsi
	call	cms_set1_ias@PLT
	testl	%eax, %eax
	je	.L195
.L172:
	xorl	%esi, %esi
	movq	%r13, %rdi
	movq	$0, -64(%rbp)
	call	EVP_PKEY_CTX_new@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L170
	movq	%rax, %rdi
	call	EVP_PKEY_keygen_init@PLT
	testl	%eax, %eax
	jle	.L173
	leaq	-64(%rbp), %rsi
	movq	%r14, %rdi
	call	EVP_PKEY_keygen@PLT
	testl	%eax, %eax
	jle	.L173
	movq	%r14, %rdi
	call	EVP_PKEY_CTX_free@PLT
	movq	-64(%rbp), %rdi
	xorl	%esi, %esi
	call	EVP_PKEY_CTX_new@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L170
	movq	%rax, %rdi
	call	EVP_PKEY_derive_init@PLT
	testl	%eax, %eax
	jle	.L173
	movq	%r14, 40(%rbx)
	movq	-64(%rbp), %rdi
	call	EVP_PKEY_free@PLT
	movq	%r13, %rdi
	call	EVP_PKEY_up_ref@PLT
	movq	%r13, 16(%r12)
	movl	$1, %eax
	jmp	.L164
	.p2align 4,,10
	.p2align 3
.L170:
	xorl	%r14d, %r14d
.L173:
	movq	%r14, %rdi
	call	EVP_PKEY_CTX_free@PLT
	movq	-64(%rbp), %rdi
	call	EVP_PKEY_free@PLT
.L195:
	xorl	%eax, %eax
.L164:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L198
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L197:
	.cfi_restore_state
	movl	$1, (%rdx)
	leaq	CMS_RecipientKeyIdentifier_it(%rip), %rdi
	movq	%rdx, -72(%rbp)
	call	ASN1_item_new@PLT
	movq	-72(%rbp), %rdx
	movq	%rax, 8(%rdx)
	movq	(%r12), %rax
	movq	8(%rax), %rdi
	testq	%rdi, %rdi
	je	.L195
	movq	%r15, %rsi
	call	cms_set1_keyid@PLT
	testl	%eax, %eax
	jne	.L172
	jmp	.L195
	.p2align 4,,10
	.p2align 3
.L196:
	leaq	CMS_RecipientEncryptedKey_it(%rip), %rsi
	movq	%r12, %rdi
	movl	%eax, -72(%rbp)
	call	ASN1_item_free@PLT
	movl	-72(%rbp), %eax
	jmp	.L164
.L198:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1475:
	.size	cms_RecipientInfo_kari_init, .-cms_RecipientInfo_kari_init
	.p2align 4
	.globl	cms_RecipientInfo_kari_encrypt
	.type	cms_RecipientInfo_kari_encrypt, @function
cms_RecipientInfo_kari_encrypt:
.LFB1477:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	(%rsi), %eax
	movl	%eax, -84(%rbp)
	cmpl	$1, %eax
	je	.L200
	movl	$376, %r8d
	movl	$181, %edx
	movl	$178, %esi
	movl	$46, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$0, -84(%rbp)
.L199:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L228
	movl	-84(%rbp), %eax
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L200:
	.cfi_restore_state
	movq	8(%rdi), %rax
	movq	8(%rsi), %r12
	movq	%rsi, -104(%rbp)
	movq	24(%rax), %r13
	movq	48(%r12), %rbx
	movq	32(%r12), %r14
	movq	24(%r13), %r8
	movq	%r8, %rdi
	movq	%r8, -96(%rbp)
	call	EVP_CIPHER_key_length@PLT
	movq	%rbx, %rdi
	movl	%eax, %r15d
	call	EVP_CIPHER_CTX_cipher@PLT
	movq	-104(%rbp), %r9
	movq	-96(%rbp), %r8
	testq	%rax, %rax
	movq	%r9, -96(%rbp)
	je	.L202
	movq	%rbx, %rdi
	call	EVP_CIPHER_CTX_cipher@PLT
	movq	%rax, %rdi
	call	EVP_CIPHER_flags@PLT
	movq	-96(%rbp), %r9
	andl	$983047, %eax
	cmpq	$65538, %rax
	je	.L210
.L212:
	movl	$0, -84(%rbp)
	jmp	.L199
	.p2align 4,,10
	.p2align 3
.L202:
	movq	%r8, %rdi
	call	EVP_CIPHER_type@PLT
	movq	-96(%rbp), %r9
	cmpl	$44, %eax
	je	.L229
	cmpl	$16, %r15d
	jle	.L230
	movq	%r9, -96(%rbp)
	cmpl	$24, %r15d
	jle	.L231
	call	EVP_aes_256_wrap@PLT
	movq	-96(%rbp), %r9
	movq	%rax, %rsi
.L207:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movq	%r9, -96(%rbp)
	call	EVP_EncryptInit_ex@PLT
	movq	-96(%rbp), %r9
	testl	%eax, %eax
	je	.L212
.L210:
	movq	8(%r12), %rbx
	cmpl	$-1, (%rbx)
	je	.L232
.L205:
	xorl	%esi, %esi
	movq	%r9, %rdi
	call	cms_env_asn1_ctrl@PLT
	testl	%eax, %eax
	je	.L212
	leaq	48(%r12), %rax
	xorl	%ebx, %ebx
	movq	%rax, -96(%rbp)
	jmp	.L211
	.p2align 4,,10
	.p2align 3
.L213:
	movl	%ebx, %esi
	movq	%r14, %rdi
	call	OPENSSL_sk_value@PLT
	movq	40(%r12), %rdi
	movq	16(%rax), %rsi
	movq	%rax, %r15
	call	EVP_PKEY_derive_set_peer@PLT
	testl	%eax, %eax
	jle	.L212
	subq	$8, %rsp
	movq	40(%r13), %rcx
	movq	32(%r13), %rdx
	leaq	-64(%rbp), %rsi
	pushq	$1
	movq	-96(%rbp), %r9
	leaq	-72(%rbp), %rdi
	leaq	40(%r12), %r8
	call	cms_kek_cipher.isra.0
	popq	%rdx
	popq	%rcx
	testl	%eax, %eax
	je	.L212
	movq	8(%r15), %rdi
	movl	-64(%rbp), %edx
	addl	$1, %ebx
	movq	-72(%rbp), %rsi
	call	ASN1_STRING_set0@PLT
.L211:
	movq	%r14, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jl	.L213
	jmp	.L199
	.p2align 4,,10
	.p2align 3
.L232:
	movl	$2, (%rbx)
	leaq	CMS_OriginatorPublicKey_it(%rip), %rdi
	movq	%r9, -96(%rbp)
	call	ASN1_item_new@PLT
	movq	-96(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, 8(%rbx)
	jne	.L205
	jmp	.L212
	.p2align 4,,10
	.p2align 3
.L231:
	call	EVP_aes_192_wrap@PLT
	movq	-96(%rbp), %r9
	movq	%rax, %rsi
	jmp	.L207
	.p2align 4,,10
	.p2align 3
.L230:
	movq	%r9, -96(%rbp)
	call	EVP_aes_128_wrap@PLT
	movq	-96(%rbp), %r9
	movq	%rax, %rsi
	jmp	.L207
	.p2align 4,,10
	.p2align 3
.L229:
	call	EVP_des_ede3_wrap@PLT
	movq	-96(%rbp), %r9
	movq	%rax, %rsi
	jmp	.L207
.L228:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1477:
	.size	cms_RecipientInfo_kari_encrypt, .-cms_RecipientInfo_kari_encrypt
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
