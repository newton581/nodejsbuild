	.file	"x509_att.c"
	.text
	.p2align 4
	.globl	X509at_get_attr_count
	.type	X509at_get_attr_count, @function
X509at_get_attr_count:
.LFB1370:
	.cfi_startproc
	endbr64
	jmp	OPENSSL_sk_num@PLT
	.cfi_endproc
.LFE1370:
	.size	X509at_get_attr_count, .-X509at_get_attr_count
	.p2align 4
	.globl	X509at_get_attr_by_NID
	.type	X509at_get_attr_by_NID, @function
X509at_get_attr_by_NID:
.LFB1371:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movl	%esi, %edi
	call	OBJ_nid2obj@PLT
	testq	%rax, %rax
	je	.L8
	testq	%rbx, %rbx
	je	.L6
	addl	$1, %r12d
	movl	$0, %edx
	movq	%rbx, %rdi
	movq	%rax, %r13
	cmovs	%edx, %r12d
	call	OPENSSL_sk_num@PLT
	movl	%eax, %r14d
	cmpl	%eax, %r12d
	jl	.L7
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L14:
	addl	$1, %r12d
	cmpl	%r12d, %r14d
	je	.L6
.L7:
	movl	%r12d, %esi
	movq	%rbx, %rdi
	call	OPENSSL_sk_value@PLT
	movq	%r13, %rsi
	movq	(%rax), %rdi
	call	OBJ_cmp@PLT
	testl	%eax, %eax
	jne	.L14
.L3:
	popq	%rbx
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	.cfi_restore_state
	movl	$-1, %r12d
	popq	%rbx
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L8:
	.cfi_restore_state
	movl	$-2, %r12d
	jmp	.L3
	.cfi_endproc
.LFE1371:
	.size	X509at_get_attr_by_NID, .-X509at_get_attr_by_NID
	.p2align 4
	.globl	X509at_get_attr_by_OBJ
	.type	X509at_get_attr_by_OBJ, @function
X509at_get_attr_by_OBJ:
.LFB1372:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testq	%rdi, %rdi
	je	.L16
	addl	$1, %edx
	movl	$0, %r12d
	movq	%rdi, %rbx
	movq	%rsi, %r13
	cmovns	%edx, %r12d
	call	OPENSSL_sk_num@PLT
	movl	%eax, %r14d
	cmpl	%eax, %r12d
	jl	.L18
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L27:
	addl	$1, %r12d
	cmpl	%r14d, %r12d
	je	.L16
.L18:
	movl	%r12d, %esi
	movq	%rbx, %rdi
	call	OPENSSL_sk_value@PLT
	movq	%r13, %rsi
	movq	(%rax), %rdi
	call	OBJ_cmp@PLT
	testl	%eax, %eax
	jne	.L27
	popq	%rbx
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	.cfi_restore_state
	movl	$-1, %r12d
	popq	%rbx
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1372:
	.size	X509at_get_attr_by_OBJ, .-X509at_get_attr_by_OBJ
	.p2align 4
	.globl	X509at_get_attr
	.type	X509at_get_attr, @function
X509at_get_attr:
.LFB1373:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L33
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r13d
	jge	.L30
	testl	%r13d, %r13d
	js	.L30
	movl	%r13d, %esi
	movq	%r12, %rdi
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	OPENSSL_sk_value@PLT
	.p2align 4,,10
	.p2align 3
.L30:
	.cfi_restore_state
	popq	%r12
	xorl	%eax, %eax
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L33:
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1373:
	.size	X509at_get_attr, .-X509at_get_attr
	.p2align 4
	.globl	X509at_delete_attr
	.type	X509at_delete_attr, @function
X509at_delete_attr:
.LFB1374:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L41
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r13d
	jge	.L38
	testl	%r13d, %r13d
	js	.L38
	movl	%r13d, %esi
	movq	%r12, %rdi
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	OPENSSL_sk_delete@PLT
	.p2align 4,,10
	.p2align 3
.L38:
	.cfi_restore_state
	popq	%r12
	xorl	%eax, %eax
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L41:
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1374:
	.size	X509at_delete_attr, .-X509at_delete_attr
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/x509/x509_att.c"
	.text
	.p2align 4
	.globl	X509at_add1_attr
	.type	X509at_add1_attr, @function
X509at_add1_attr:
.LFB1375:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	testq	%rdi, %rdi
	je	.L58
	movq	(%rdi), %r12
	movq	%rdi, %rbx
	movq	%rsi, %r13
	testq	%r12, %r12
	je	.L59
.L47:
	movq	%r13, %rdi
	call	X509_ATTRIBUTE_dup@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L46
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L48
	cmpq	$0, (%rbx)
	je	.L60
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L59:
	.cfi_restore_state
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L47
	xorl	%r13d, %r13d
	.p2align 4,,10
	.p2align 3
.L48:
	movl	$98, %r8d
	movl	$65, %edx
	movl	$135, %esi
	movl	$11, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L46:
	movq	%r13, %rdi
	call	X509_ATTRIBUTE_free@PLT
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	OPENSSL_sk_free@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L60:
	.cfi_restore_state
	movq	%r12, (%rbx)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L58:
	.cfi_restore_state
	movl	$67, %edx
	movl	$135, %esi
	xorl	%r12d, %r12d
	xorl	%r13d, %r13d
	movl	$80, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$11, %edi
	call	ERR_put_error@PLT
	jmp	.L46
	.cfi_endproc
.LFE1375:
	.size	X509at_add1_attr, .-X509at_add1_attr
	.p2align 4
	.globl	X509at_get0_data_by_OBJ
	.type	X509at_get0_data_by_OBJ, @function
X509at_get0_data_by_OBJ:
.LFB1379:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%ecx, -52(%rbp)
	testq	%rdi, %rdi
	je	.L93
	movl	%edx, %r12d
	movl	$0, %eax
	movq	%rdi, %r14
	movq	%rsi, %r13
	addl	$1, %r12d
	movl	%edx, %ebx
	cmovs	%eax, %r12d
	call	OPENSSL_sk_num@PLT
	movl	%eax, %r15d
	cmpl	%eax, %r12d
	jl	.L66
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L94:
	addl	$1, %r12d
	cmpl	%r12d, %r15d
	je	.L93
.L66:
	movl	%r12d, %esi
	movq	%r14, %rdi
	call	OPENSSL_sk_value@PLT
	movq	%r13, %rsi
	movq	(%rax), %rdi
	call	OBJ_cmp@PLT
	testl	%eax, %eax
	jne	.L94
	cmpl	$-1, %r12d
	je	.L93
	cmpl	$-1, %ebx
	jl	.L95
.L67:
	movq	%r14, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%r12d, %eax
	jle	.L93
	testl	%r12d, %r12d
	js	.L93
	movl	%r12d, %esi
	movq	%r14, %rdi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %r12
	cmpl	$-2, %ebx
	jge	.L70
	testq	%rax, %rax
	je	.L93
	movq	8(%rax), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	$1, %eax
	jne	.L93
.L72:
	movq	8(%r12), %rdi
	xorl	%esi, %esi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L93
	movl	-52(%rbp), %eax
	andl	$-5, %eax
	cmpl	$1, %eax
	je	.L74
	movq	%rbx, %rdi
	call	ASN1_TYPE_get@PLT
	cmpl	%eax, -52(%rbp)
	jne	.L74
	movq	8(%rbx), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L74:
	.cfi_restore_state
	movl	$320, %r8d
	movl	$122, %edx
	movl	$139, %esi
	movl	$11, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L93:
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L95:
	.cfi_restore_state
	movl	%r12d, %edx
	movq	%r14, %rdi
	addl	$1, %edx
	cmovns	%edx, %eax
	movl	%eax, %r15d
	call	OPENSSL_sk_num@PLT
	movl	%eax, -56(%rbp)
	cmpl	%eax, %r15d
	jl	.L69
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L96:
	addl	$1, %r15d
	cmpl	%r15d, -56(%rbp)
	je	.L67
.L69:
	movl	%r15d, %esi
	movq	%r14, %rdi
	call	OPENSSL_sk_value@PLT
	movq	%r13, %rsi
	movq	(%rax), %rdi
	call	OBJ_cmp@PLT
	testl	%eax, %eax
	jne	.L96
	cmpl	$-1, %r15d
	jne	.L93
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L70:
	testq	%rax, %rax
	jne	.L72
	jmp	.L93
	.cfi_endproc
.LFE1379:
	.size	X509at_get0_data_by_OBJ, .-X509at_get0_data_by_OBJ
	.p2align 4
	.globl	X509_ATTRIBUTE_set1_object
	.type	X509_ATTRIBUTE_set1_object, @function
X509_ATTRIBUTE_set1_object:
.LFB1383:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L101
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	testq	%rsi, %rsi
	je	.L97
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	call	ASN1_OBJECT_free@PLT
	movq	%r12, %rdi
	call	OBJ_dup@PLT
	testq	%rax, %rax
	movq	%rax, (%rbx)
	setne	%al
	movzbl	%al, %eax
.L97:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L101:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1383:
	.size	X509_ATTRIBUTE_set1_object, .-X509_ATTRIBUTE_set1_object
	.p2align 4
	.globl	X509_ATTRIBUTE_set1_data
	.type	X509_ATTRIBUTE_set1_data, @function
X509_ATTRIBUTE_set1_data:
.LFB1384:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L114
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%ecx, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	andl	$4096, %esi
	jne	.L134
	xorl	%r13d, %r13d
	cmpl	$-1, %ecx
	jne	.L135
	testl	%r12d, %r12d
	jne	.L112
.L136:
	movq	%r13, %rdi
	call	ASN1_STRING_free@PLT
	movl	$1, %eax
.L105:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L135:
	.cfi_restore_state
	movl	%r12d, %edi
	movq	%rdx, -56(%rbp)
	call	ASN1_STRING_type_new@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L117
	movq	-56(%rbp), %r9
	movl	%r14d, %edx
	movq	%rax, %rdi
	movq	%r9, %rsi
	call	ASN1_STRING_set@PLT
	testl	%eax, %eax
	je	.L117
	movq	-56(%rbp), %r9
	movl	%r12d, %esi
	testl	%r12d, %r12d
	je	.L136
.L112:
	movq	%r9, -64(%rbp)
	movl	%esi, -56(%rbp)
	call	ASN1_TYPE_new@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L109
	cmpl	$-1, %r14d
	movl	-56(%rbp), %esi
	movq	-64(%rbp), %r9
	jne	.L110
	movq	%r9, %rdx
	movl	%r12d, %esi
	movq	%rax, %rdi
	call	ASN1_TYPE_set1@PLT
	testl	%eax, %eax
	jne	.L113
.L109:
	movl	$290, %r8d
	movl	$65, %edx
	movl	$138, %esi
	movl	$11, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	%r15, %rdi
	call	ASN1_TYPE_free@PLT
	movq	%r13, %rdi
	call	ASN1_STRING_free@PLT
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L134:
	.cfi_restore_state
	movq	(%rdi), %rdi
	movq	%rdx, -56(%rbp)
	call	OBJ_obj2nid@PLT
	movq	-56(%rbp), %r9
	movl	%r12d, %ecx
	movl	%r14d, %edx
	movl	%eax, %r8d
	xorl	%edi, %edi
	movq	%r9, %rsi
	call	ASN1_STRING_set_by_NID@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L137
	movl	4(%rax), %esi
	movl	%esi, -56(%rbp)
	call	ASN1_TYPE_new@PLT
	movl	-56(%rbp), %esi
	testq	%rax, %rax
	movq	%rax, %r15
	je	.L109
.L110:
	movq	%r13, %rdx
	movq	%r15, %rdi
	xorl	%r13d, %r13d
	call	ASN1_TYPE_set@PLT
.L113:
	movq	8(%rbx), %rdi
	movq	%r15, %rsi
	call	OPENSSL_sk_push@PLT
	movl	%eax, %r8d
	movl	$1, %eax
	testl	%r8d, %r8d
	je	.L109
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L117:
	.cfi_restore_state
	xorl	%r15d, %r15d
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L114:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L137:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movl	$257, %r8d
	movl	$13, %edx
	movl	$138, %esi
	movl	$11, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L105
	.cfi_endproc
.LFE1384:
	.size	X509_ATTRIBUTE_set1_data, .-X509_ATTRIBUTE_set1_data
	.p2align 4
	.globl	X509_ATTRIBUTE_create_by_OBJ
	.type	X509_ATTRIBUTE_create_by_OBJ, @function
X509_ATTRIBUTE_create_by_OBJ:
.LFB1381:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%r8d, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movl	%edx, -52(%rbp)
	testq	%rdi, %rdi
	je	.L139
	movq	(%rdi), %r12
	testq	%r12, %r12
	je	.L139
	testq	%rsi, %rsi
	jne	.L177
.L176:
	xorl	%r12d, %r12d
.L138:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L177:
	.cfi_restore_state
	movq	(%r12), %rdi
	call	ASN1_OBJECT_free@PLT
	movq	%r13, %rdi
	call	OBJ_dup@PLT
	movq	%rax, (%r12)
	testq	%rax, %rax
	jne	.L178
.L146:
	cmpq	%r12, (%rbx)
	je	.L176
.L144:
	movq	%r12, %rdi
	call	X509_ATTRIBUTE_free@PLT
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L139:
	call	X509_ATTRIBUTE_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L179
	testq	%r13, %r13
	jne	.L180
.L143:
	testq	%rbx, %rbx
	jne	.L146
	jmp	.L144
	.p2align 4,,10
	.p2align 3
.L178:
	movl	-52(%rbp), %esi
	movl	%r15d, %ecx
	movq	%r14, %rdx
	movq	%r12, %rdi
	call	X509_ATTRIBUTE_set1_data
	testl	%eax, %eax
	je	.L146
.L147:
	cmpq	$0, (%rbx)
	jne	.L138
	movq	%r12, (%rbx)
	jmp	.L138
	.p2align 4,,10
	.p2align 3
.L180:
	movq	(%rax), %rdi
	call	ASN1_OBJECT_free@PLT
	movq	%r13, %rdi
	call	OBJ_dup@PLT
	movq	%rax, (%r12)
	testq	%rax, %rax
	je	.L143
	movl	-52(%rbp), %esi
	movl	%r15d, %ecx
	movq	%r14, %rdx
	movq	%r12, %rdi
	call	X509_ATTRIBUTE_set1_data
	testl	%eax, %eax
	je	.L143
	testq	%rbx, %rbx
	je	.L138
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L179:
	movl	$195, %r8d
	movl	$65, %edx
	movl	$137, %esi
	movl	$11, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L138
	.cfi_endproc
.LFE1381:
	.size	X509_ATTRIBUTE_create_by_OBJ, .-X509_ATTRIBUTE_create_by_OBJ
	.p2align 4
	.globl	X509at_add1_attr_by_OBJ
	.type	X509at_add1_attr_by_OBJ, @function
X509at_add1_attr_by_OBJ:
.LFB1376:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%r8d, %ebx
	subq	$24, %rsp
	movl	%edx, -52(%rbp)
	call	X509_ATTRIBUTE_new@PLT
	testq	%rax, %rax
	je	.L193
	movq	%rax, %r14
	testq	%r12, %r12
	jne	.L184
.L185:
	movq	%r14, %rdi
	xorl	%r12d, %r12d
	call	X509_ATTRIBUTE_free@PLT
.L181:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L184:
	.cfi_restore_state
	movq	(%rax), %rdi
	call	ASN1_OBJECT_free@PLT
	movq	%r12, %rdi
	call	OBJ_dup@PLT
	movq	%rax, (%r14)
	testq	%rax, %rax
	je	.L185
	movl	-52(%rbp), %esi
	movl	%ebx, %ecx
	movq	%r15, %rdx
	movq	%r14, %rdi
	call	X509_ATTRIBUTE_set1_data
	testl	%eax, %eax
	je	.L185
	movq	%r13, %rdi
	movq	%r14, %rsi
	call	X509at_add1_attr
	movq	%r14, %rdi
	movq	%rax, %r12
	call	X509_ATTRIBUTE_free@PLT
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L193:
	movl	$195, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	xorl	%r12d, %r12d
	movl	$137, %esi
	movl	$11, %edi
	call	ERR_put_error@PLT
	jmp	.L181
	.cfi_endproc
.LFE1376:
	.size	X509at_add1_attr_by_OBJ, .-X509at_add1_attr_by_OBJ
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"name="
	.text
	.p2align 4
	.globl	X509at_add1_attr_by_txt
	.type	X509at_add1_attr_by_txt, @function
X509at_add1_attr_by_txt:
.LFB1378:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	xorl	%esi, %esi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%r14, %rdi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%r8d, %ebx
	subq	$24, %rsp
	movl	%edx, -52(%rbp)
	call	OBJ_txt2obj@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L204
	call	X509_ATTRIBUTE_new@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L205
	movq	(%rax), %rdi
	call	ASN1_OBJECT_free@PLT
	movq	%r12, %rdi
	call	OBJ_dup@PLT
	movq	%rax, (%r14)
	testq	%rax, %rax
	jne	.L198
.L199:
	movq	%r14, %rdi
	call	X509_ATTRIBUTE_free@PLT
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	ASN1_OBJECT_free@PLT
.L194:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L198:
	.cfi_restore_state
	movl	-52(%rbp), %esi
	movl	%ebx, %ecx
	movq	%r15, %rdx
	movq	%r14, %rdi
	call	X509_ATTRIBUTE_set1_data
	testl	%eax, %eax
	je	.L199
	movq	%r12, %rdi
	call	ASN1_OBJECT_free@PLT
	movq	%r13, %rdi
	movq	%r14, %rsi
	call	X509at_add1_attr
	movq	%r14, %rdi
	movq	%rax, %r12
	call	X509_ATTRIBUTE_free@PLT
	jmp	.L194
	.p2align 4,,10
	.p2align 3
.L204:
	movl	$226, %r8d
	movl	$119, %edx
	movl	$140, %esi
	movl	$11, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	%r14, %rdx
	movl	$2, %edi
	xorl	%eax, %eax
	leaq	.LC1(%rip), %rsi
	call	ERR_add_error_data@PLT
	jmp	.L194
	.p2align 4,,10
	.p2align 3
.L205:
	movl	$195, %r8d
	movl	$65, %edx
	movl	$137, %esi
	movl	$11, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	ASN1_OBJECT_free@PLT
	jmp	.L194
	.cfi_endproc
.LFE1378:
	.size	X509at_add1_attr_by_txt, .-X509at_add1_attr_by_txt
	.p2align 4
	.globl	X509at_add1_attr_by_NID
	.type	X509at_add1_attr_by_NID, @function
X509at_add1_attr_by_NID:
.LFB1377:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movl	%esi, %edi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%r8d, %ebx
	subq	$24, %rsp
	movl	%edx, -52(%rbp)
	call	OBJ_nid2obj@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L217
	call	X509_ATTRIBUTE_new@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L218
	movq	(%rax), %rdi
	call	ASN1_OBJECT_free@PLT
	movq	%r12, %rdi
	call	OBJ_dup@PLT
	movq	%rax, (%r14)
	testq	%rax, %rax
	jne	.L211
.L212:
	movq	%r14, %rdi
	call	X509_ATTRIBUTE_free@PLT
.L210:
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	ASN1_OBJECT_free@PLT
.L206:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L211:
	.cfi_restore_state
	movl	-52(%rbp), %esi
	movl	%ebx, %ecx
	movq	%r15, %rdx
	movq	%r14, %rdi
	call	X509_ATTRIBUTE_set1_data
	testl	%eax, %eax
	je	.L212
	movq	%r13, %rdi
	movq	%r14, %rsi
	call	X509at_add1_attr
	movq	%r14, %rdi
	movq	%rax, %r12
	call	X509_ATTRIBUTE_free@PLT
	jmp	.L206
	.p2align 4,,10
	.p2align 3
.L217:
	movl	$177, %r8d
	movl	$109, %edx
	movl	$136, %esi
	movl	$11, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L206
	.p2align 4,,10
	.p2align 3
.L218:
	movl	$195, %r8d
	movl	$65, %edx
	movl	$137, %esi
	movl	$11, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L210
	.cfi_endproc
.LFE1377:
	.size	X509at_add1_attr_by_NID, .-X509at_add1_attr_by_NID
	.p2align 4
	.globl	X509_ATTRIBUTE_create_by_NID
	.type	X509_ATTRIBUTE_create_by_NID, @function
X509_ATTRIBUTE_create_by_NID:
.LFB1380:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%r8d, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movl	%esi, %edi
	subq	$24, %rsp
	call	OBJ_nid2obj@PLT
	testq	%rax, %rax
	je	.L254
	movq	%rax, %r12
	testq	%rbx, %rbx
	je	.L222
	movq	(%rbx), %r8
	testq	%r8, %r8
	je	.L222
	movq	(%r8), %rdi
	movq	%r8, -56(%rbp)
	call	ASN1_OBJECT_free@PLT
	movq	%r12, %rdi
	call	OBJ_dup@PLT
	movq	-56(%rbp), %r8
	movq	%rax, (%r8)
	testq	%rax, %rax
	jne	.L255
.L229:
	cmpq	%r8, (%rbx)
	je	.L225
.L227:
	movq	%r8, %rdi
	call	X509_ATTRIBUTE_free@PLT
.L225:
	movq	%r12, %rdi
	call	ASN1_OBJECT_free@PLT
	xorl	%r8d, %r8d
.L219:
	addq	$24, %rsp
	movq	%r8, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L255:
	.cfi_restore_state
	movq	%r8, %rdi
	movl	%r15d, %ecx
	movq	%r14, %rdx
	movl	%r13d, %esi
	call	X509_ATTRIBUTE_set1_data
	movq	-56(%rbp), %r8
	testl	%eax, %eax
	je	.L229
.L230:
	cmpq	$0, (%rbx)
	jne	.L219
	movq	%r8, (%rbx)
	jmp	.L219
	.p2align 4,,10
	.p2align 3
.L222:
	call	X509_ATTRIBUTE_new@PLT
	testq	%rax, %rax
	je	.L256
	movq	(%rax), %rdi
	movq	%rax, -56(%rbp)
	call	ASN1_OBJECT_free@PLT
	movq	%r12, %rdi
	call	OBJ_dup@PLT
	movq	-56(%rbp), %r8
	movq	%rax, (%r8)
	testq	%rax, %rax
	jne	.L257
.L226:
	testq	%rbx, %rbx
	je	.L227
	cmpq	%r8, (%rbx)
	jne	.L227
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L254:
	movl	$177, %r8d
	movl	$109, %edx
	movl	$136, %esi
	movl	$11, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%r8d, %r8d
	jmp	.L219
	.p2align 4,,10
	.p2align 3
.L256:
	movl	$195, %r8d
	movl	$65, %edx
	movl	$137, %esi
	movl	$11, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L225
.L257:
	movq	%r8, %rdi
	movl	%r15d, %ecx
	movq	%r14, %rdx
	movl	%r13d, %esi
	movq	%r8, -56(%rbp)
	call	X509_ATTRIBUTE_set1_data
	movq	-56(%rbp), %r8
	testl	%eax, %eax
	je	.L226
	testq	%rbx, %rbx
	je	.L219
	jmp	.L230
	.cfi_endproc
.LFE1380:
	.size	X509_ATTRIBUTE_create_by_NID, .-X509_ATTRIBUTE_create_by_NID
	.p2align 4
	.globl	X509_ATTRIBUTE_create_by_txt
	.type	X509_ATTRIBUTE_create_by_txt, @function
X509_ATTRIBUTE_create_by_txt:
.LFB1382:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%r8d, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	xorl	%esi, %esi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$24, %rsp
	movl	%edx, -52(%rbp)
	call	OBJ_txt2obj@PLT
	testq	%rax, %rax
	je	.L294
	movq	%rax, %r13
	testq	%rbx, %rbx
	je	.L261
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L261
	movq	(%r12), %rdi
	call	ASN1_OBJECT_free@PLT
	movq	%r13, %rdi
	call	OBJ_dup@PLT
	movq	%rax, (%r12)
	testq	%rax, %rax
	jne	.L295
.L268:
	cmpq	%r12, (%rbx)
	je	.L270
.L266:
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	X509_ATTRIBUTE_free@PLT
.L264:
	movq	%r13, %rdi
	call	ASN1_OBJECT_free@PLT
.L258:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L295:
	.cfi_restore_state
	movl	-52(%rbp), %esi
	movl	%r15d, %ecx
	movq	%r14, %rdx
	movq	%r12, %rdi
	call	X509_ATTRIBUTE_set1_data
	testl	%eax, %eax
	je	.L268
.L269:
	cmpq	$0, (%rbx)
	jne	.L264
	movq	%r12, (%rbx)
	jmp	.L264
	.p2align 4,,10
	.p2align 3
.L270:
	xorl	%r12d, %r12d
	jmp	.L264
	.p2align 4,,10
	.p2align 3
.L261:
	call	X509_ATTRIBUTE_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L296
	movq	(%rax), %rdi
	call	ASN1_OBJECT_free@PLT
	movq	%r13, %rdi
	call	OBJ_dup@PLT
	movq	%rax, (%r12)
	testq	%rax, %rax
	jne	.L297
.L265:
	testq	%rbx, %rbx
	je	.L266
	cmpq	%r12, (%rbx)
	jne	.L266
	jmp	.L270
	.p2align 4,,10
	.p2align 3
.L294:
	movl	$226, %r8d
	movl	$119, %edx
	movl	$140, %esi
	movl	$11, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	%r12, %rdx
	movl	$2, %edi
	xorl	%eax, %eax
	leaq	.LC1(%rip), %rsi
	xorl	%r12d, %r12d
	call	ERR_add_error_data@PLT
	jmp	.L258
	.p2align 4,,10
	.p2align 3
.L296:
	movl	$195, %r8d
	movl	$65, %edx
	movl	$137, %esi
	movl	$11, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L264
.L297:
	movl	-52(%rbp), %esi
	movl	%r15d, %ecx
	movq	%r14, %rdx
	movq	%r12, %rdi
	call	X509_ATTRIBUTE_set1_data
	testl	%eax, %eax
	je	.L265
	testq	%rbx, %rbx
	je	.L264
	jmp	.L269
	.cfi_endproc
.LFE1382:
	.size	X509_ATTRIBUTE_create_by_txt, .-X509_ATTRIBUTE_create_by_txt
	.p2align 4
	.globl	X509_ATTRIBUTE_count
	.type	X509_ATTRIBUTE_count, @function
X509_ATTRIBUTE_count:
.LFB1385:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L299
	movq	8(%rdi), %rdi
	jmp	OPENSSL_sk_num@PLT
	.p2align 4,,10
	.p2align 3
.L299:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1385:
	.size	X509_ATTRIBUTE_count, .-X509_ATTRIBUTE_count
	.p2align 4
	.globl	X509_ATTRIBUTE_get0_object
	.type	X509_ATTRIBUTE_get0_object, @function
X509_ATTRIBUTE_get0_object:
.LFB1386:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L302
	movq	(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L302:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1386:
	.size	X509_ATTRIBUTE_get0_object, .-X509_ATTRIBUTE_get0_object
	.p2align 4
	.globl	X509_ATTRIBUTE_get0_data
	.type	X509_ATTRIBUTE_get0_data, @function
X509_ATTRIBUTE_get0_data:
.LFB1387:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L316
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -32
	movq	8(%rdi), %rdi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L315
	movl	%r12d, %eax
	andl	$-5, %eax
	cmpl	$1, %eax
	je	.L308
	movq	%rbx, %rdi
	call	ASN1_TYPE_get@PLT
	cmpl	%r12d, %eax
	jne	.L308
	movq	8(%rbx), %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L308:
	.cfi_restore_state
	movl	$320, %r8d
	movl	$122, %edx
	movl	$139, %esi
	movl	$11, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L315:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L316:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1387:
	.size	X509_ATTRIBUTE_get0_data, .-X509_ATTRIBUTE_get0_data
	.p2align 4
	.globl	X509_ATTRIBUTE_get0_type
	.type	X509_ATTRIBUTE_get0_type, @function
X509_ATTRIBUTE_get0_type:
.LFB1388:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L318
	movq	8(%rdi), %rdi
	jmp	OPENSSL_sk_value@PLT
	.p2align 4,,10
	.p2align 3
.L318:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1388:
	.size	X509_ATTRIBUTE_get0_type, .-X509_ATTRIBUTE_get0_type
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
