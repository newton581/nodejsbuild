	.file	"v3_info.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/x509v3/v3_info.c"
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"%s - %s"
	.text
	.p2align 4
	.type	i2v_AUTHORITY_INFO_ACCESS, @function
i2v_AUTHORITY_INFO_ACCESS:
.LFB1328:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-144(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	xorl	%r13d, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$136, %rsp
	movq	%rdi, -168(%rbp)
	movq	%rsi, -152(%rbp)
	movq	%rdx, -176(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L25:
	movq	8(%rbx), %r8
	movq	%rax, %rdi
	movq	%r14, %rcx
	addl	$1, %r13d
	movq	-160(%rbp), %r10
	leaq	.LC1(%rip), %rdx
	movq	%rax, -160(%rbp)
	xorl	%eax, %eax
	movq	%r10, %rsi
	call	BIO_snprintf@PLT
	movq	8(%rbx), %rdi
	movl	$86, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	-160(%rbp), %r9
	movq	%r9, 8(%rbx)
	movq	%r12, %rbx
.L2:
	movq	-152(%rbp), %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r13d
	jge	.L24
	movq	-152(%rbp), %rdi
	movl	%r13d, %esi
	call	OPENSSL_sk_value@PLT
	movq	-168(%rbp), %rdi
	movq	%rbx, %rdx
	movq	8(%rax), %rsi
	movq	%rax, %r15
	call	i2v_GENERAL_NAME@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L3
	movl	%r13d, %esi
	movq	%rax, %rdi
	call	OPENSSL_sk_value@PLT
	movq	(%r15), %rdx
	movl	$80, %esi
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	i2t_ASN1_OBJECT@PLT
	movq	%r14, %rdx
.L4:
	movl	(%rdx), %ecx
	addq	$4, %rdx
	leal	-16843009(%rcx), %eax
	notl	%ecx
	andl	%ecx, %eax
	andl	$-2139062144, %eax
	je	.L4
	movl	%eax, %ecx
	movq	8(%rbx), %rdi
	shrl	$16, %ecx
	testl	$32896, %eax
	cmove	%ecx, %eax
	leaq	2(%rdx), %rcx
	cmove	%rcx, %rdx
	movl	%eax, %esi
	addb	%al, %sil
	sbbq	$3, %rdx
	subq	%r14, %rdx
	movq	%rdx, -160(%rbp)
	call	strlen@PLT
	movq	-160(%rbp), %rdx
	leaq	.LC0(%rip), %rsi
	leal	4(%rdx,%rax), %r10d
	movl	$82, %edx
	movslq	%r10d, %r10
	movq	%r10, %rdi
	movq	%r10, -160(%rbp)
	call	CRYPTO_malloc@PLT
	testq	%rax, %rax
	jne	.L25
	movq	%r12, %rbx
.L3:
	movl	$94, %r8d
	movl	$65, %edx
	movl	$138, %esi
	movl	$34, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	cmpq	$0, -176(%rbp)
	jne	.L10
	testq	%rbx, %rbx
	je	.L10
	movq	X509V3_conf_free@GOTPCREL(%rip), %rsi
	movq	%rbx, %rdi
	xorl	%ebx, %ebx
	call	OPENSSL_sk_pop_free@PLT
.L1:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L26
	addq	$136, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	.cfi_restore_state
	xorl	%ebx, %ebx
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L24:
	movq	-176(%rbp), %rax
	orq	%rbx, %rax
	jne	.L1
	call	OPENSSL_sk_new_null@PLT
	movq	%rax, %rbx
	jmp	.L1
.L26:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1328:
	.size	i2v_AUTHORITY_INFO_ACCESS, .-i2v_AUTHORITY_INFO_ACCESS
	.section	.rodata.str1.1
.LC2:
	.string	"value="
	.text
	.p2align 4
	.type	v2i_AUTHORITY_INFO_ACCESS, @function
v2i_AUTHORITY_INFO_ACCESS:
.LFB1329:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -96(%rbp)
	movq	%rdx, %rdi
	movq	%rsi, -104(%rbp)
	movq	%rdx, -88(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	OPENSSL_sk_num@PLT
	xorl	%edi, %edi
	movl	%eax, %esi
	movl	%eax, -116(%rbp)
	movl	%eax, %ebx
	call	OPENSSL_sk_new_reserve@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L28
	testl	%ebx, %ebx
	jle	.L27
	leaq	-80(%rbp), %rax
	xorl	%r13d, %r13d
	movq	%rax, -112(%rbp)
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L31:
	movq	%rax, %rsi
	movq	%r15, %rdi
	call	OPENSSL_sk_push@PLT
	movq	8(%r12), %r14
	movl	$59, %esi
	movq	%r14, %rdi
	call	strchr@PLT
	testq	%rax, %rax
	je	.L45
	movq	%rax, %rcx
	addq	$1, %rax
	movq	8(%rbx), %rdi
	movq	-104(%rbp), %rdx
	subq	%r14, %rcx
	movq	%rax, %xmm0
	movq	-96(%rbp), %rsi
	xorl	%r8d, %r8d
	movq	%rcx, %r14
	movq	-112(%rbp), %rcx
	movhps	16(%r12), %xmm0
	movups	%xmm0, -72(%rbp)
	call	v2i_GENERAL_NAME_ex@PLT
	testq	%rax, %rax
	je	.L32
	movq	8(%r12), %rdi
	movslq	%r14d, %rsi
	movl	$136, %ecx
	leaq	.LC0(%rip), %rdx
	call	CRYPTO_strndup@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L46
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	OBJ_txt2obj@PLT
	movq	%rax, (%rbx)
	testq	%rax, %rax
	je	.L47
	movl	$149, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	addl	$1, %r13d
	call	CRYPTO_free@PLT
	cmpl	-116(%rbp), %r13d
	je	.L27
.L29:
	movq	-88(%rbp), %rdi
	movl	%r13d, %esi
	call	OPENSSL_sk_value@PLT
	leaq	ACCESS_DESCRIPTION_it(%rip), %rdi
	movq	%rax, %r12
	call	ASN1_item_new@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	jne	.L31
	movl	$120, %r8d
.L44:
	movl	$65, %edx
	movl	$139, %esi
	movl	$34, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L32:
	movq	%r15, %rdi
	leaq	ACCESS_DESCRIPTION_free(%rip), %rsi
	xorl	%r15d, %r15d
	call	OPENSSL_sk_pop_free@PLT
.L27:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L48
	addq	$88, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L45:
	.cfi_restore_state
	movl	$127, %r8d
	movl	$143, %edx
	movl	$139, %esi
	movl	$34, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L46:
	movl	$137, %r8d
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L47:
	movl	$143, %r8d
	movl	$119, %edx
	movl	$139, %esi
	movl	$34, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movq	%r12, %rdx
	movl	$2, %edi
	xorl	%eax, %eax
	leaq	.LC2(%rip), %rsi
	call	ERR_add_error_data@PLT
	movl	$146, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
	jmp	.L32
.L28:
	movl	$114, %r8d
	movl	$65, %edx
	movl	$139, %esi
	movl	$34, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L27
.L48:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1329:
	.size	v2i_AUTHORITY_INFO_ACCESS, .-v2i_AUTHORITY_INFO_ACCESS
	.p2align 4
	.globl	ACCESS_DESCRIPTION_free
	.type	ACCESS_DESCRIPTION_free, @function
ACCESS_DESCRIPTION_free:
.LFB1323:
	.cfi_startproc
	endbr64
	leaq	ACCESS_DESCRIPTION_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE1323:
	.size	ACCESS_DESCRIPTION_free, .-ACCESS_DESCRIPTION_free
	.p2align 4
	.globl	d2i_ACCESS_DESCRIPTION
	.type	d2i_ACCESS_DESCRIPTION, @function
d2i_ACCESS_DESCRIPTION:
.LFB1320:
	.cfi_startproc
	endbr64
	leaq	ACCESS_DESCRIPTION_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE1320:
	.size	d2i_ACCESS_DESCRIPTION, .-d2i_ACCESS_DESCRIPTION
	.p2align 4
	.globl	i2d_ACCESS_DESCRIPTION
	.type	i2d_ACCESS_DESCRIPTION, @function
i2d_ACCESS_DESCRIPTION:
.LFB1321:
	.cfi_startproc
	endbr64
	leaq	ACCESS_DESCRIPTION_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE1321:
	.size	i2d_ACCESS_DESCRIPTION, .-i2d_ACCESS_DESCRIPTION
	.p2align 4
	.globl	ACCESS_DESCRIPTION_new
	.type	ACCESS_DESCRIPTION_new, @function
ACCESS_DESCRIPTION_new:
.LFB1322:
	.cfi_startproc
	endbr64
	leaq	ACCESS_DESCRIPTION_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE1322:
	.size	ACCESS_DESCRIPTION_new, .-ACCESS_DESCRIPTION_new
	.p2align 4
	.globl	d2i_AUTHORITY_INFO_ACCESS
	.type	d2i_AUTHORITY_INFO_ACCESS, @function
d2i_AUTHORITY_INFO_ACCESS:
.LFB1324:
	.cfi_startproc
	endbr64
	leaq	AUTHORITY_INFO_ACCESS_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE1324:
	.size	d2i_AUTHORITY_INFO_ACCESS, .-d2i_AUTHORITY_INFO_ACCESS
	.p2align 4
	.globl	i2d_AUTHORITY_INFO_ACCESS
	.type	i2d_AUTHORITY_INFO_ACCESS, @function
i2d_AUTHORITY_INFO_ACCESS:
.LFB1325:
	.cfi_startproc
	endbr64
	leaq	AUTHORITY_INFO_ACCESS_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE1325:
	.size	i2d_AUTHORITY_INFO_ACCESS, .-i2d_AUTHORITY_INFO_ACCESS
	.p2align 4
	.globl	AUTHORITY_INFO_ACCESS_new
	.type	AUTHORITY_INFO_ACCESS_new, @function
AUTHORITY_INFO_ACCESS_new:
.LFB1326:
	.cfi_startproc
	endbr64
	leaq	AUTHORITY_INFO_ACCESS_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE1326:
	.size	AUTHORITY_INFO_ACCESS_new, .-AUTHORITY_INFO_ACCESS_new
	.p2align 4
	.globl	AUTHORITY_INFO_ACCESS_free
	.type	AUTHORITY_INFO_ACCESS_free, @function
AUTHORITY_INFO_ACCESS_free:
.LFB1327:
	.cfi_startproc
	endbr64
	leaq	AUTHORITY_INFO_ACCESS_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE1327:
	.size	AUTHORITY_INFO_ACCESS_free, .-AUTHORITY_INFO_ACCESS_free
	.p2align 4
	.globl	i2a_ACCESS_DESCRIPTION
	.type	i2a_ACCESS_DESCRIPTION, @function
i2a_ACCESS_DESCRIPTION:
.LFB1330:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rsi), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	i2a_ASN1_OBJECT@PLT
	movl	$2, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1330:
	.size	i2a_ACCESS_DESCRIPTION, .-i2a_ACCESS_DESCRIPTION
	.globl	AUTHORITY_INFO_ACCESS_it
	.section	.rodata.str1.1
.LC3:
	.string	"AUTHORITY_INFO_ACCESS"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	AUTHORITY_INFO_ACCESS_it, @object
	.size	AUTHORITY_INFO_ACCESS_it, 56
AUTHORITY_INFO_ACCESS_it:
	.byte	0
	.zero	7
	.quad	-1
	.quad	AUTHORITY_INFO_ACCESS_item_tt
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC3
	.section	.rodata.str1.1
.LC4:
	.string	"GeneralNames"
	.section	.data.rel.ro.local
	.align 32
	.type	AUTHORITY_INFO_ACCESS_item_tt, @object
	.size	AUTHORITY_INFO_ACCESS_item_tt, 40
AUTHORITY_INFO_ACCESS_item_tt:
	.quad	4
	.quad	0
	.quad	0
	.quad	.LC4
	.quad	ACCESS_DESCRIPTION_it
	.globl	ACCESS_DESCRIPTION_it
	.section	.rodata.str1.1
.LC5:
	.string	"ACCESS_DESCRIPTION"
	.section	.data.rel.ro.local
	.align 32
	.type	ACCESS_DESCRIPTION_it, @object
	.size	ACCESS_DESCRIPTION_it, 56
ACCESS_DESCRIPTION_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	ACCESS_DESCRIPTION_seq_tt
	.quad	2
	.quad	0
	.quad	16
	.quad	.LC5
	.section	.rodata.str1.1
.LC6:
	.string	"method"
.LC7:
	.string	"location"
	.section	.data.rel.ro,"aw"
	.align 32
	.type	ACCESS_DESCRIPTION_seq_tt, @object
	.size	ACCESS_DESCRIPTION_seq_tt, 80
ACCESS_DESCRIPTION_seq_tt:
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC6
	.quad	ASN1_OBJECT_it
	.quad	0
	.quad	0
	.quad	8
	.quad	.LC7
	.quad	GENERAL_NAME_it
	.globl	v3_sinfo
	.section	.data.rel.ro.local
	.align 32
	.type	v3_sinfo, @object
	.size	v3_sinfo, 104
v3_sinfo:
	.long	398
	.long	4
	.quad	AUTHORITY_INFO_ACCESS_it
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	i2v_AUTHORITY_INFO_ACCESS
	.quad	v2i_AUTHORITY_INFO_ACCESS
	.quad	0
	.quad	0
	.quad	0
	.globl	v3_info
	.align 32
	.type	v3_info, @object
	.size	v3_info, 104
v3_info:
	.long	177
	.long	4
	.quad	AUTHORITY_INFO_ACCESS_it
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	i2v_AUTHORITY_INFO_ACCESS
	.quad	v2i_AUTHORITY_INFO_ACCESS
	.quad	0
	.quad	0
	.quad	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
