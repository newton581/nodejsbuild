	.file	"pmeth_lib.c"
	.text
	.p2align 4
	.type	pmeth_cmp, @function
pmeth_cmp:
.LFB1370:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	(%rsi), %rdx
	movl	(%rax), %eax
	subl	(%rdx), %eax
	ret
	.cfi_endproc
.LFE1370:
	.size	pmeth_cmp, .-pmeth_cmp
	.p2align 4
	.type	pmeth_cmp_BSEARCH_CMP_FN, @function
pmeth_cmp_BSEARCH_CMP_FN:
.LFB1371:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	(%rsi), %rdx
	movl	(%rax), %eax
	subl	(%rdx), %eax
	ret
	.cfi_endproc
.LFE1371:
	.size	pmeth_cmp_BSEARCH_CMP_FN, .-pmeth_cmp_BSEARCH_CMP_FN
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/openssl/openssl/crypto/evp/pmeth_lib.c"
	.text
	.p2align 4
	.globl	EVP_PKEY_meth_free
	.type	EVP_PKEY_meth_free, @function
EVP_PKEY_meth_free:
.LFB1378:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L4
	testb	$1, 4(%rdi)
	jne	.L12
.L4:
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	movl	$240, %edx
	leaq	.LC0(%rip), %rsi
	jmp	CRYPTO_free@PLT
	.cfi_endproc
.LFE1378:
	.size	EVP_PKEY_meth_free, .-EVP_PKEY_meth_free
	.p2align 4
	.globl	EVP_PKEY_meth_find
	.type	EVP_PKEY_meth_find, @function
EVP_PKEY_meth_find:
.LFB1373:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$288, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movl	%edi, -272(%rbp)
	movq	app_pkey_methods(%rip), %rdi
	leaq	-272(%rbp), %rsi
	movq	%rsi, -280(%rbp)
	testq	%rdi, %rdi
	je	.L14
	call	OPENSSL_sk_find@PLT
	testl	%eax, %eax
	jns	.L24
.L14:
	leaq	-280(%rbp), %rdi
	movl	$8, %ecx
	leaq	pmeth_cmp_BSEARCH_CMP_FN(%rip), %r8
	movl	$18, %edx
	leaq	standard_methods(%rip), %rsi
	call	OBJ_bsearch_@PLT
	testq	%rax, %rax
	je	.L13
	movq	(%rax), %rax
.L13:
	movq	-8(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L25
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	.cfi_restore_state
	movq	app_pkey_methods(%rip), %rdi
	movl	%eax, %esi
	call	OPENSSL_sk_value@PLT
	jmp	.L13
.L25:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1373:
	.size	EVP_PKEY_meth_find, .-EVP_PKEY_meth_find
	.p2align 4
	.globl	EVP_PKEY_meth_new
	.type	EVP_PKEY_meth_new, @function
EVP_PKEY_meth_new:
.LFB1375:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$174, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%edi, %r12d
	movl	$256, %edi
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%esi, %ebx
	leaq	.LC0(%rip), %rsi
	subq	$16, %rsp
	call	CRYPTO_zalloc@PLT
	testq	%rax, %rax
	je	.L30
	orl	$1, %ebx
	movl	%r12d, (%rax)
	movl	%ebx, 4(%rax)
.L26:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	.cfi_restore_state
	movl	$176, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	movl	$195, %esi
	movl	$6, %edi
	movq	%rax, -24(%rbp)
	call	ERR_put_error@PLT
	movq	-24(%rbp), %rax
	jmp	.L26
	.cfi_endproc
.LFE1375:
	.size	EVP_PKEY_meth_new, .-EVP_PKEY_meth_new
	.p2align 4
	.globl	EVP_PKEY_meth_get0_info
	.type	EVP_PKEY_meth_get0_info, @function
EVP_PKEY_meth_get0_info:
.LFB1376:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L32
	movl	(%rdx), %eax
	movl	%eax, (%rdi)
.L32:
	testq	%rsi, %rsi
	je	.L31
	movl	4(%rdx), %eax
	movl	%eax, (%rsi)
.L31:
	ret
	.cfi_endproc
.LFE1376:
	.size	EVP_PKEY_meth_get0_info, .-EVP_PKEY_meth_get0_info
	.p2align 4
	.globl	EVP_PKEY_meth_copy
	.type	EVP_PKEY_meth_copy, @function
EVP_PKEY_meth_copy:
.LFB1377:
	.cfi_startproc
	endbr64
	movdqu	24(%rsi), %xmm10
	movdqu	40(%rsi), %xmm9
	movdqu	56(%rsi), %xmm8
	movdqu	72(%rsi), %xmm7
	movdqu	88(%rsi), %xmm6
	movdqu	104(%rsi), %xmm5
	movdqu	120(%rsi), %xmm4
	movdqu	136(%rsi), %xmm3
	movdqu	152(%rsi), %xmm2
	movdqu	168(%rsi), %xmm1
	movdqu	184(%rsi), %xmm0
	movdqu	8(%rsi), %xmm11
	movups	%xmm10, 24(%rdi)
	movups	%xmm9, 40(%rdi)
	movups	%xmm11, 8(%rdi)
	movups	%xmm8, 56(%rdi)
	movups	%xmm7, 72(%rdi)
	movups	%xmm6, 88(%rdi)
	movups	%xmm5, 104(%rdi)
	movups	%xmm4, 120(%rdi)
	movups	%xmm3, 136(%rdi)
	movups	%xmm2, 152(%rdi)
	movups	%xmm1, 168(%rdi)
	movups	%xmm0, 184(%rdi)
	movq	200(%rsi), %rax
	movq	%rax, 200(%rdi)
	movq	224(%rsi), %rax
	movq	%rax, 224(%rdi)
	ret
	.cfi_endproc
.LFE1377:
	.size	EVP_PKEY_meth_copy, .-EVP_PKEY_meth_copy
	.p2align 4
	.globl	EVP_PKEY_CTX_new
	.type	EVP_PKEY_CTX_new, @function
EVP_PKEY_CTX_new:
.LFB1379:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testq	%rdi, %rdi
	je	.L50
	movl	(%rdi), %r13d
	movq	%rdi, %r14
	movq	%rsi, %r12
	testq	%rsi, %rsi
	je	.L55
.L43:
	movq	%r12, %rdi
	call	ENGINE_init@PLT
	testl	%eax, %eax
	je	.L56
.L45:
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	ENGINE_get_pkey_meth@PLT
	movq	%rax, %rbx
.L47:
	testq	%rbx, %rbx
	je	.L57
	movl	$144, %edx
	leaq	.LC0(%rip), %rsi
	movl	$80, %edi
	call	CRYPTO_zalloc@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L58
	movq	%r12, %xmm1
	movl	$0, 32(%rax)
	movq	%rbx, %xmm0
	movq	%r14, %rdi
	movq	%r14, 16(%rax)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rax)
	call	EVP_PKEY_up_ref@PLT
	movq	8(%rbx), %rax
	testq	%rax, %rax
	je	.L41
	movq	%r13, %rdi
	call	*%rax
	testl	%eax, %eax
	jle	.L59
.L41:
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L55:
	.cfi_restore_state
	movq	32(%rdi), %r12
	testq	%r12, %r12
	jne	.L43
	movq	24(%rdi), %r12
	testq	%r12, %r12
	jne	.L43
	movl	%r13d, %edi
	call	ENGINE_get_pkey_meth_engine@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L45
	movl	%r13d, %edi
	call	EVP_PKEY_meth_find
	movq	%rax, %rbx
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L50:
	xorl	%r13d, %r13d
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L57:
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	call	ENGINE_finish@PLT
	movl	$140, %r8d
	movl	$156, %edx
	leaq	.LC0(%rip), %rcx
	movl	$157, %esi
	movl	$6, %edi
	call	ERR_put_error@PLT
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L56:
	movl	$119, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$38, %edx
	xorl	%r13d, %r13d
	movl	$157, %esi
	movl	$6, %edi
	call	ERR_put_error@PLT
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L59:
	movq	$0, 0(%r13)
	movq	16(%r13), %rdi
	call	EVP_PKEY_free@PLT
	movq	24(%r13), %rdi
	call	EVP_PKEY_free@PLT
	movq	8(%r13), %rdi
	call	ENGINE_finish@PLT
	movq	%r13, %rdi
	xorl	%r13d, %r13d
	movl	$363, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L58:
	movq	%r12, %rdi
	call	ENGINE_finish@PLT
	movl	$149, %r8d
	movl	$65, %edx
	leaq	.LC0(%rip), %rcx
	movl	$157, %esi
	movl	$6, %edi
	call	ERR_put_error@PLT
	jmp	.L41
	.cfi_endproc
.LFE1379:
	.size	EVP_PKEY_CTX_new, .-EVP_PKEY_CTX_new
	.p2align 4
	.globl	EVP_PKEY_CTX_new_id
	.type	EVP_PKEY_CTX_new_id, @function
EVP_PKEY_CTX_new_id:
.LFB1380:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	xorl	%r12d, %r12d
	subq	$8, %rsp
	cmpl	$-1, %edi
	je	.L60
	movl	%edi, %r14d
	movq	%rsi, %r13
	testq	%rsi, %rsi
	je	.L62
	movq	%rsi, %rdi
	call	ENGINE_init@PLT
	testl	%eax, %eax
	je	.L74
.L63:
	movl	%r14d, %esi
	movq	%r13, %rdi
	call	ENGINE_get_pkey_meth@PLT
	movq	%rax, %r14
.L65:
	testq	%r14, %r14
	je	.L75
	movl	$144, %edx
	leaq	.LC0(%rip), %rsi
	movl	$80, %edi
	call	CRYPTO_zalloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L76
	movq	%r14, %xmm0
	movq	%r13, %xmm1
	movl	$0, 32(%rax)
	punpcklqdq	%xmm1, %xmm0
	movq	$0, 16(%rax)
	movups	%xmm0, (%rax)
	movq	8(%r14), %rax
	testq	%rax, %rax
	je	.L60
	movq	%r12, %rdi
	call	*%rax
	testl	%eax, %eax
	jle	.L77
.L60:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L62:
	.cfi_restore_state
	call	ENGINE_get_pkey_meth_engine@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	jne	.L63
	movl	%r14d, %edi
	call	EVP_PKEY_meth_find
	movq	%rax, %r14
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L75:
	movq	%r13, %rdi
	xorl	%r12d, %r12d
	call	ENGINE_finish@PLT
	movl	$140, %r8d
	movl	$156, %edx
	leaq	.LC0(%rip), %rcx
	movl	$157, %esi
	movl	$6, %edi
	call	ERR_put_error@PLT
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L77:
	movq	$0, (%r12)
	movq	16(%r12), %rdi
	call	EVP_PKEY_free@PLT
	movq	24(%r12), %rdi
	call	EVP_PKEY_free@PLT
	movq	8(%r12), %rdi
	call	ENGINE_finish@PLT
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	movl	$363, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L74:
	movl	$119, %r8d
	movl	$38, %edx
	movl	$157, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L76:
	movq	%r13, %rdi
	call	ENGINE_finish@PLT
	movl	$149, %r8d
	movl	$65, %edx
	leaq	.LC0(%rip), %rcx
	movl	$157, %esi
	movl	$6, %edi
	call	ERR_put_error@PLT
	jmp	.L60
	.cfi_endproc
.LFE1380:
	.size	EVP_PKEY_CTX_new_id, .-EVP_PKEY_CTX_new_id
	.p2align 4
	.globl	EVP_PKEY_CTX_dup
	.type	EVP_PKEY_CTX_dup, @function
EVP_PKEY_CTX_dup:
.LFB1381:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	(%rdi), %r12
	testq	%r12, %r12
	je	.L78
	cmpq	$0, 16(%r12)
	je	.L84
	movq	%rdi, %r13
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L80
	call	ENGINE_init@PLT
	testl	%eax, %eax
	je	.L98
.L80:
	movl	$265, %edx
	leaq	.LC0(%rip), %rsi
	movl	$80, %edi
	call	CRYPTO_malloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L99
	movdqu	0(%r13), %xmm1
	movups	%xmm1, (%rax)
	movq	16(%r13), %rdi
	testq	%rdi, %rdi
	je	.L82
	call	EVP_PKEY_up_ref@PLT
	movq	16(%r13), %rdi
.L82:
	movq	%rdi, 16(%r12)
	movq	24(%r13), %rdi
	testq	%rdi, %rdi
	je	.L83
	call	EVP_PKEY_up_ref@PLT
	movq	24(%r13), %rdi
.L83:
	movl	32(%r13), %eax
	pxor	%xmm0, %xmm0
	movq	%rdi, 24(%r12)
	movq	%r13, %rsi
	movups	%xmm0, 40(%r12)
	movq	%r12, %rdi
	movl	%eax, 32(%r12)
	movq	0(%r13), %rax
	call	*16(%rax)
	testl	%eax, %eax
	jle	.L100
.L78:
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L100:
	.cfi_restore_state
	movq	$0, (%r12)
	movq	16(%r12), %rdi
	call	EVP_PKEY_free@PLT
	movq	24(%r12), %rdi
	call	EVP_PKEY_free@PLT
	movq	8(%r12), %rdi
	call	ENGINE_finish@PLT
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	movl	$363, %edx
	leaq	.LC0(%rip), %rsi
	call	CRYPTO_free@PLT
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L84:
	.cfi_restore_state
	xorl	%r12d, %r12d
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L98:
	movl	$261, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$38, %edx
	xorl	%r12d, %r12d
	movl	$156, %esi
	movl	$6, %edi
	call	ERR_put_error@PLT
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L99:
	movl	$267, %r8d
	movl	$65, %edx
	movl	$156, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	jmp	.L78
	.cfi_endproc
.LFE1381:
	.size	EVP_PKEY_CTX_dup, .-EVP_PKEY_CTX_dup
	.p2align 4
	.globl	EVP_PKEY_meth_add0
	.type	EVP_PKEY_meth_add0, @function
EVP_PKEY_meth_add0:
.LFB1382:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	app_pkey_methods(%rip), %rdi
	testq	%rdi, %rdi
	je	.L106
.L102:
	movq	%r12, %rsi
	call	OPENSSL_sk_push@PLT
	testl	%eax, %eax
	je	.L107
	movq	app_pkey_methods(%rip), %rdi
	call	OPENSSL_sk_sort@PLT
	movl	$1, %eax
.L101:
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L107:
	.cfi_restore_state
	movl	$309, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$65, %edx
	movl	%eax, -20(%rbp)
	movl	$194, %esi
	movl	$6, %edi
	call	ERR_put_error@PLT
	movl	-20(%rbp), %eax
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L106:
	.cfi_restore_state
	leaq	pmeth_cmp(%rip), %rdi
	call	OPENSSL_sk_new@PLT
	movq	%rax, app_pkey_methods(%rip)
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L102
	movl	$304, %r8d
	movl	$65, %edx
	movl	$194, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	xorl	%eax, %eax
	jmp	.L101
	.cfi_endproc
.LFE1382:
	.size	EVP_PKEY_meth_add0, .-EVP_PKEY_meth_add0
	.p2align 4
	.globl	evp_app_cleanup_int
	.type	evp_app_cleanup_int, @function
evp_app_cleanup_int:
.LFB1383:
	.cfi_startproc
	endbr64
	movq	app_pkey_methods(%rip), %rdi
	testq	%rdi, %rdi
	je	.L108
	leaq	EVP_PKEY_meth_free(%rip), %rsi
	jmp	OPENSSL_sk_pop_free@PLT
	.p2align 4,,10
	.p2align 3
.L108:
	ret
	.cfi_endproc
.LFE1383:
	.size	evp_app_cleanup_int, .-evp_app_cleanup_int
	.p2align 4
	.globl	EVP_PKEY_meth_remove
	.type	EVP_PKEY_meth_remove, @function
EVP_PKEY_meth_remove:
.LFB1384:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	app_pkey_methods(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	OPENSSL_sk_delete_ptr@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	testq	%rax, %rax
	setne	%al
	movzbl	%al, %eax
	ret
	.cfi_endproc
.LFE1384:
	.size	EVP_PKEY_meth_remove, .-EVP_PKEY_meth_remove
	.p2align 4
	.globl	EVP_PKEY_meth_get_count
	.type	EVP_PKEY_meth_get_count, @function
EVP_PKEY_meth_get_count:
.LFB1385:
	.cfi_startproc
	endbr64
	movq	app_pkey_methods(%rip), %rdi
	testq	%rdi, %rdi
	je	.L116
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	OPENSSL_sk_num@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	cltq
	addq	$18, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L116:
	.cfi_restore 6
	movl	$18, %eax
	ret
	.cfi_endproc
.LFE1385:
	.size	EVP_PKEY_meth_get_count, .-EVP_PKEY_meth_get_count
	.p2align 4
	.globl	EVP_PKEY_meth_get0
	.type	EVP_PKEY_meth_get0, @function
EVP_PKEY_meth_get0:
.LFB1386:
	.cfi_startproc
	endbr64
	cmpq	$17, %rdi
	jbe	.L130
	movq	app_pkey_methods(%rip), %r8
	testq	%r8, %r8
	je	.L131
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	leaq	-18(%rdi), %rbx
	movq	%r8, %rdi
	subq	$8, %rsp
	call	OPENSSL_sk_num@PLT
	cltq
	cmpq	%rbx, %rax
	jbe	.L125
	movq	app_pkey_methods(%rip), %rdi
	addq	$8, %rsp
	movl	%ebx, %esi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	OPENSSL_sk_value@PLT
	.p2align 4,,10
	.p2align 3
.L125:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L130:
	.cfi_restore 3
	.cfi_restore 6
	leaq	standard_methods(%rip), %rax
	movq	(%rax,%rdi,8), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L131:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1386:
	.size	EVP_PKEY_meth_get0, .-EVP_PKEY_meth_get0
	.p2align 4
	.globl	EVP_PKEY_CTX_free
	.type	EVP_PKEY_CTX_free, @function
EVP_PKEY_CTX_free:
.LFB1387:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L132
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	(%rdi), %rax
	testq	%rax, %rax
	je	.L134
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L134
	call	*%rax
.L134:
	movq	16(%r12), %rdi
	call	EVP_PKEY_free@PLT
	movq	24(%r12), %rdi
	call	EVP_PKEY_free@PLT
	movq	8(%r12), %rdi
	call	ENGINE_finish@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$363, %edx
	popq	%r12
	.cfi_restore 12
	leaq	.LC0(%rip), %rsi
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	CRYPTO_free@PLT
	.p2align 4,,10
	.p2align 3
.L132:
	ret
	.cfi_endproc
.LFE1387:
	.size	EVP_PKEY_CTX_free, .-EVP_PKEY_CTX_free
	.p2align 4
	.globl	EVP_PKEY_CTX_ctrl
	.type	EVP_PKEY_CTX_ctrl, @function
EVP_PKEY_CTX_ctrl:
.LFB1388:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	testq	%rdi, %rdi
	je	.L145
	movq	(%rdi), %rax
	testq	%rax, %rax
	je	.L145
	movl	%edx, %r11d
	movl	%r8d, %edx
	movq	192(%rax), %r8
	testq	%r8, %r8
	je	.L145
	movl	%ecx, %r10d
	movq	%r9, %rcx
	cmpl	$-1, %esi
	je	.L148
	cmpl	%esi, (%rax)
	jne	.L151
.L148:
	cmpq	$0, 248(%rax)
	je	.L165
.L149:
	movl	%r10d, %esi
	call	*%r8
	cmpl	$-2, %eax
	je	.L166
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L165:
	.cfi_restore_state
	movl	32(%rdi), %eax
	testl	%eax, %eax
	je	.L167
	cmpl	$-1, %r11d
	je	.L149
	testl	%r11d, %eax
	jne	.L149
	movl	$388, %r8d
	movl	$148, %edx
	movl	$137, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-1, %eax
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L166:
	.cfi_restore_state
	movl	$396, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$147, %edx
	movl	%eax, -4(%rbp)
	movl	$137, %esi
	movl	$6, %edi
	call	ERR_put_error@PLT
	movl	-4(%rbp), %eax
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L151:
	.cfi_restore_state
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L145:
	.cfi_restore_state
	movl	$372, %r8d
	movl	$147, %edx
	movl	$137, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-2, %eax
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L167:
	.cfi_restore_state
	movl	$383, %r8d
	movl	$149, %edx
	movl	$137, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-1, %eax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1388:
	.size	EVP_PKEY_CTX_ctrl, .-EVP_PKEY_CTX_ctrl
	.p2align 4
	.globl	EVP_PKEY_CTX_ctrl_uint64
	.type	EVP_PKEY_CTX_ctrl_uint64, @function
EVP_PKEY_CTX_ctrl_uint64:
.LFB1389:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%r8, -8(%rbp)
	testq	%rdi, %rdi
	je	.L169
	movq	(%rdi), %rax
	testq	%rax, %rax
	je	.L169
	movq	192(%rax), %r8
	testq	%r8, %r8
	je	.L169
	movl	%ecx, %r9d
	cmpl	$-1, %esi
	je	.L172
	cmpl	(%rax), %esi
	jne	.L175
.L172:
	cmpq	$0, 248(%rax)
	je	.L189
.L173:
	xorl	%edx, %edx
	leaq	-8(%rbp), %rcx
	movl	%r9d, %esi
	call	*%r8
	cmpl	$-2, %eax
	je	.L190
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L189:
	.cfi_restore_state
	movl	32(%rdi), %eax
	testl	%eax, %eax
	je	.L191
	cmpl	$-1, %edx
	je	.L173
	testl	%eax, %edx
	jne	.L173
	movl	$388, %r8d
	movl	$148, %edx
	movl	$137, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-1, %eax
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L190:
	.cfi_restore_state
	movl	$396, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$147, %edx
	movl	%eax, -12(%rbp)
	movl	$137, %esi
	movl	$6, %edi
	call	ERR_put_error@PLT
	movl	-12(%rbp), %eax
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L175:
	.cfi_restore_state
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L169:
	.cfi_restore_state
	movl	$372, %r8d
	movl	$147, %edx
	movl	$137, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-2, %eax
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L191:
	.cfi_restore_state
	movl	$383, %r8d
	movl	$149, %edx
	movl	$137, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-1, %eax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1389:
	.size	EVP_PKEY_CTX_ctrl_uint64, .-EVP_PKEY_CTX_ctrl_uint64
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"digest"
	.text
	.p2align 4
	.globl	EVP_PKEY_CTX_ctrl_str
	.type	EVP_PKEY_CTX_ctrl_str, @function
EVP_PKEY_CTX_ctrl_str:
.LFB1390:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	testq	%rdi, %rdi
	je	.L193
	movq	(%rdi), %rax
	movq	%rdi, %r13
	testq	%rax, %rax
	je	.L193
	movq	200(%rax), %rax
	testq	%rax, %rax
	je	.L193
	movl	$7, %ecx
	leaq	.LC1(%rip), %rdi
	movq	%rsi, %r8
	repz cmpsb
	seta	%r12b
	sbbb	$0, %r12b
	movsbl	%r12b, %r12d
	testl	%r12d, %r12d
	je	.L209
	popq	%r12
	movq	%r13, %rdi
	movq	%r8, %rsi
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L209:
	.cfi_restore_state
	testq	%rdx, %rdx
	je	.L198
	movq	%rdx, %rdi
	call	EVP_get_digestbyname@PLT
	movq	%rax, %r9
	testq	%rax, %rax
	je	.L198
	popq	%r12
	movq	%r13, %rdi
	xorl	%r8d, %r8d
	popq	%r13
	movl	$1, %ecx
	movl	$248, %edx
	movl	$-1, %esi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	EVP_PKEY_CTX_ctrl
	.p2align 4,,10
	.p2align 3
.L198:
	.cfi_restore_state
	movl	$453, %r8d
	movl	$152, %edx
	movl	$168, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
.L192:
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L193:
	.cfi_restore_state
	movl	$411, %r8d
	movl	$147, %edx
	movl	$150, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	movl	$-2, %r12d
	call	ERR_put_error@PLT
	jmp	.L192
	.cfi_endproc
.LFE1390:
	.size	EVP_PKEY_CTX_ctrl_str, .-EVP_PKEY_CTX_ctrl_str
	.p2align 4
	.globl	EVP_PKEY_CTX_str2ctrl
	.type	EVP_PKEY_CTX_str2ctrl, @function
EVP_PKEY_CTX_str2ctrl:
.LFB1391:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%esi, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	movq	%rdx, %rdi
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	subq	$8, %rsp
	call	strlen@PLT
	cmpq	$2147483647, %rax
	ja	.L211
	movq	%rax, %rdx
	movq	0(%r13), %rax
	movq	%r12, %rcx
	movl	%r14d, %esi
	movq	%r13, %rdi
	movq	192(%rax), %rax
	addq	$8, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L211:
	.cfi_restore_state
	addq	$8, %rsp
	movl	$-1, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1391:
	.size	EVP_PKEY_CTX_str2ctrl, .-EVP_PKEY_CTX_str2ctrl
	.p2align 4
	.globl	EVP_PKEY_CTX_hex2ctrl
	.type	EVP_PKEY_CTX_hex2ctrl, @function
EVP_PKEY_CTX_hex2ctrl:
.LFB1392:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movq	%rdx, %rdi
	pushq	%r13
	.cfi_offset 13, -40
	movl	%esi, %r13d
	leaq	-48(%rbp), %rsi
	pushq	%r12
	subq	$16, %rsp
	.cfi_offset 12, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	OPENSSL_hexstr2buf@PLT
	testq	%rax, %rax
	je	.L217
	movq	-48(%rbp), %rdx
	movq	%rax, %r12
	movl	$-1, %r15d
	cmpq	$2147483647, %rdx
	jle	.L220
.L215:
	movl	$443, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	CRYPTO_free@PLT
.L213:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L221
	addq	$16, %rsp
	movl	%r15d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L220:
	.cfi_restore_state
	movq	(%r14), %rax
	movq	%r12, %rcx
	movl	%r13d, %esi
	movq	%r14, %rdi
	call	*192(%rax)
	movl	%eax, %r15d
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L217:
	xorl	%r15d, %r15d
	jmp	.L213
.L221:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1392:
	.size	EVP_PKEY_CTX_hex2ctrl, .-EVP_PKEY_CTX_hex2ctrl
	.p2align 4
	.globl	EVP_PKEY_CTX_md
	.type	EVP_PKEY_CTX_md, @function
EVP_PKEY_CTX_md:
.LFB1393:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	testq	%rcx, %rcx
	je	.L225
	movq	%rdi, %r12
	movq	%rcx, %rdi
	movl	%esi, %ebx
	movl	%edx, %r13d
	call	EVP_get_digestbyname@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L225
	testq	%r12, %r12
	je	.L226
	movq	(%r12), %rax
	testq	%rax, %rax
	je	.L226
	movq	192(%rax), %r8
	testq	%r8, %r8
	je	.L226
	cmpq	$0, 248(%rax)
	je	.L243
.L228:
	xorl	%edx, %edx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	*%r8
	cmpl	$-2, %eax
	je	.L244
.L222:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L243:
	.cfi_restore_state
	movl	32(%r12), %eax
	testl	%eax, %eax
	je	.L245
	cmpl	$-1, %ebx
	je	.L228
	testl	%eax, %ebx
	jne	.L228
	movl	$388, %r8d
	movl	$148, %edx
	movl	$137, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	orl	$-1, %eax
	jmp	.L222
	.p2align 4,,10
	.p2align 3
.L225:
	movl	$453, %r8d
	movl	$152, %edx
	movl	$168, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L244:
	.cfi_restore_state
	movl	$396, %r8d
	leaq	.LC0(%rip), %rcx
	movl	$147, %edx
	movl	%eax, -36(%rbp)
	movl	$137, %esi
	movl	$6, %edi
	call	ERR_put_error@PLT
	movl	-36(%rbp), %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L226:
	.cfi_restore_state
	movl	$372, %r8d
	movl	$147, %edx
	movl	$137, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-2, %eax
	jmp	.L222
.L245:
	movl	$383, %r8d
	movl	$149, %edx
	movl	$137, %esi
	movl	$6, %edi
	leaq	.LC0(%rip), %rcx
	call	ERR_put_error@PLT
	movl	$-1, %eax
	jmp	.L222
	.cfi_endproc
.LFE1393:
	.size	EVP_PKEY_CTX_md, .-EVP_PKEY_CTX_md
	.p2align 4
	.globl	EVP_PKEY_CTX_get_operation
	.type	EVP_PKEY_CTX_get_operation, @function
EVP_PKEY_CTX_get_operation:
.LFB1394:
	.cfi_startproc
	endbr64
	movl	32(%rdi), %eax
	ret
	.cfi_endproc
.LFE1394:
	.size	EVP_PKEY_CTX_get_operation, .-EVP_PKEY_CTX_get_operation
	.p2align 4
	.globl	EVP_PKEY_CTX_set0_keygen_info
	.type	EVP_PKEY_CTX_set0_keygen_info, @function
EVP_PKEY_CTX_set0_keygen_info:
.LFB1395:
	.cfi_startproc
	endbr64
	movq	%rsi, 64(%rdi)
	movl	%edx, 72(%rdi)
	ret
	.cfi_endproc
.LFE1395:
	.size	EVP_PKEY_CTX_set0_keygen_info, .-EVP_PKEY_CTX_set0_keygen_info
	.p2align 4
	.globl	EVP_PKEY_CTX_set_data
	.type	EVP_PKEY_CTX_set_data, @function
EVP_PKEY_CTX_set_data:
.LFB1396:
	.cfi_startproc
	endbr64
	movq	%rsi, 40(%rdi)
	ret
	.cfi_endproc
.LFE1396:
	.size	EVP_PKEY_CTX_set_data, .-EVP_PKEY_CTX_set_data
	.p2align 4
	.globl	EVP_PKEY_CTX_get_data
	.type	EVP_PKEY_CTX_get_data, @function
EVP_PKEY_CTX_get_data:
.LFB1397:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	ret
	.cfi_endproc
.LFE1397:
	.size	EVP_PKEY_CTX_get_data, .-EVP_PKEY_CTX_get_data
	.p2align 4
	.globl	EVP_PKEY_CTX_get0_pkey
	.type	EVP_PKEY_CTX_get0_pkey, @function
EVP_PKEY_CTX_get0_pkey:
.LFB1398:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	ret
	.cfi_endproc
.LFE1398:
	.size	EVP_PKEY_CTX_get0_pkey, .-EVP_PKEY_CTX_get0_pkey
	.p2align 4
	.globl	EVP_PKEY_CTX_get0_peerkey
	.type	EVP_PKEY_CTX_get0_peerkey, @function
EVP_PKEY_CTX_get0_peerkey:
.LFB1399:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	ret
	.cfi_endproc
.LFE1399:
	.size	EVP_PKEY_CTX_get0_peerkey, .-EVP_PKEY_CTX_get0_peerkey
	.p2align 4
	.globl	EVP_PKEY_CTX_set_app_data
	.type	EVP_PKEY_CTX_set_app_data, @function
EVP_PKEY_CTX_set_app_data:
.LFB1400:
	.cfi_startproc
	endbr64
	movq	%rsi, 48(%rdi)
	ret
	.cfi_endproc
.LFE1400:
	.size	EVP_PKEY_CTX_set_app_data, .-EVP_PKEY_CTX_set_app_data
	.p2align 4
	.globl	EVP_PKEY_CTX_get_app_data
	.type	EVP_PKEY_CTX_get_app_data, @function
EVP_PKEY_CTX_get_app_data:
.LFB1401:
	.cfi_startproc
	endbr64
	movq	48(%rdi), %rax
	ret
	.cfi_endproc
.LFE1401:
	.size	EVP_PKEY_CTX_get_app_data, .-EVP_PKEY_CTX_get_app_data
	.p2align 4
	.globl	EVP_PKEY_meth_set_init
	.type	EVP_PKEY_meth_set_init, @function
EVP_PKEY_meth_set_init:
.LFB1402:
	.cfi_startproc
	endbr64
	movq	%rsi, 8(%rdi)
	ret
	.cfi_endproc
.LFE1402:
	.size	EVP_PKEY_meth_set_init, .-EVP_PKEY_meth_set_init
	.p2align 4
	.globl	EVP_PKEY_meth_set_copy
	.type	EVP_PKEY_meth_set_copy, @function
EVP_PKEY_meth_set_copy:
.LFB1403:
	.cfi_startproc
	endbr64
	movq	%rsi, 16(%rdi)
	ret
	.cfi_endproc
.LFE1403:
	.size	EVP_PKEY_meth_set_copy, .-EVP_PKEY_meth_set_copy
	.p2align 4
	.globl	EVP_PKEY_meth_set_cleanup
	.type	EVP_PKEY_meth_set_cleanup, @function
EVP_PKEY_meth_set_cleanup:
.LFB1404:
	.cfi_startproc
	endbr64
	movq	%rsi, 24(%rdi)
	ret
	.cfi_endproc
.LFE1404:
	.size	EVP_PKEY_meth_set_cleanup, .-EVP_PKEY_meth_set_cleanup
	.p2align 4
	.globl	EVP_PKEY_meth_set_paramgen
	.type	EVP_PKEY_meth_set_paramgen, @function
EVP_PKEY_meth_set_paramgen:
.LFB1405:
	.cfi_startproc
	endbr64
	movq	%rsi, %xmm0
	movq	%rdx, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 32(%rdi)
	ret
	.cfi_endproc
.LFE1405:
	.size	EVP_PKEY_meth_set_paramgen, .-EVP_PKEY_meth_set_paramgen
	.p2align 4
	.globl	EVP_PKEY_meth_set_keygen
	.type	EVP_PKEY_meth_set_keygen, @function
EVP_PKEY_meth_set_keygen:
.LFB1406:
	.cfi_startproc
	endbr64
	movq	%rsi, %xmm0
	movq	%rdx, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 48(%rdi)
	ret
	.cfi_endproc
.LFE1406:
	.size	EVP_PKEY_meth_set_keygen, .-EVP_PKEY_meth_set_keygen
	.p2align 4
	.globl	EVP_PKEY_meth_set_sign
	.type	EVP_PKEY_meth_set_sign, @function
EVP_PKEY_meth_set_sign:
.LFB1407:
	.cfi_startproc
	endbr64
	movq	%rsi, %xmm0
	movq	%rdx, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 64(%rdi)
	ret
	.cfi_endproc
.LFE1407:
	.size	EVP_PKEY_meth_set_sign, .-EVP_PKEY_meth_set_sign
	.p2align 4
	.globl	EVP_PKEY_meth_set_verify
	.type	EVP_PKEY_meth_set_verify, @function
EVP_PKEY_meth_set_verify:
.LFB1408:
	.cfi_startproc
	endbr64
	movq	%rsi, %xmm0
	movq	%rdx, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 80(%rdi)
	ret
	.cfi_endproc
.LFE1408:
	.size	EVP_PKEY_meth_set_verify, .-EVP_PKEY_meth_set_verify
	.p2align 4
	.globl	EVP_PKEY_meth_set_verify_recover
	.type	EVP_PKEY_meth_set_verify_recover, @function
EVP_PKEY_meth_set_verify_recover:
.LFB1409:
	.cfi_startproc
	endbr64
	movq	%rsi, %xmm0
	movq	%rdx, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 96(%rdi)
	ret
	.cfi_endproc
.LFE1409:
	.size	EVP_PKEY_meth_set_verify_recover, .-EVP_PKEY_meth_set_verify_recover
	.p2align 4
	.globl	EVP_PKEY_meth_set_signctx
	.type	EVP_PKEY_meth_set_signctx, @function
EVP_PKEY_meth_set_signctx:
.LFB1410:
	.cfi_startproc
	endbr64
	movq	%rsi, %xmm0
	movq	%rdx, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 112(%rdi)
	ret
	.cfi_endproc
.LFE1410:
	.size	EVP_PKEY_meth_set_signctx, .-EVP_PKEY_meth_set_signctx
	.p2align 4
	.globl	EVP_PKEY_meth_set_verifyctx
	.type	EVP_PKEY_meth_set_verifyctx, @function
EVP_PKEY_meth_set_verifyctx:
.LFB1411:
	.cfi_startproc
	endbr64
	movq	%rsi, %xmm0
	movq	%rdx, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 128(%rdi)
	ret
	.cfi_endproc
.LFE1411:
	.size	EVP_PKEY_meth_set_verifyctx, .-EVP_PKEY_meth_set_verifyctx
	.p2align 4
	.globl	EVP_PKEY_meth_set_encrypt
	.type	EVP_PKEY_meth_set_encrypt, @function
EVP_PKEY_meth_set_encrypt:
.LFB1412:
	.cfi_startproc
	endbr64
	movq	%rsi, %xmm0
	movq	%rdx, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 144(%rdi)
	ret
	.cfi_endproc
.LFE1412:
	.size	EVP_PKEY_meth_set_encrypt, .-EVP_PKEY_meth_set_encrypt
	.p2align 4
	.globl	EVP_PKEY_meth_set_decrypt
	.type	EVP_PKEY_meth_set_decrypt, @function
EVP_PKEY_meth_set_decrypt:
.LFB1413:
	.cfi_startproc
	endbr64
	movq	%rsi, %xmm0
	movq	%rdx, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 160(%rdi)
	ret
	.cfi_endproc
.LFE1413:
	.size	EVP_PKEY_meth_set_decrypt, .-EVP_PKEY_meth_set_decrypt
	.p2align 4
	.globl	EVP_PKEY_meth_set_derive
	.type	EVP_PKEY_meth_set_derive, @function
EVP_PKEY_meth_set_derive:
.LFB1414:
	.cfi_startproc
	endbr64
	movq	%rsi, %xmm0
	movq	%rdx, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 176(%rdi)
	ret
	.cfi_endproc
.LFE1414:
	.size	EVP_PKEY_meth_set_derive, .-EVP_PKEY_meth_set_derive
	.p2align 4
	.globl	EVP_PKEY_meth_set_ctrl
	.type	EVP_PKEY_meth_set_ctrl, @function
EVP_PKEY_meth_set_ctrl:
.LFB1415:
	.cfi_startproc
	endbr64
	movq	%rsi, %xmm0
	movq	%rdx, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 192(%rdi)
	ret
	.cfi_endproc
.LFE1415:
	.size	EVP_PKEY_meth_set_ctrl, .-EVP_PKEY_meth_set_ctrl
	.p2align 4
	.globl	EVP_PKEY_meth_set_digestsign
	.type	EVP_PKEY_meth_set_digestsign, @function
EVP_PKEY_meth_set_digestsign:
.LFB1416:
	.cfi_startproc
	endbr64
	movq	%rsi, 208(%rdi)
	ret
	.cfi_endproc
.LFE1416:
	.size	EVP_PKEY_meth_set_digestsign, .-EVP_PKEY_meth_set_digestsign
	.p2align 4
	.globl	EVP_PKEY_meth_set_digestverify
	.type	EVP_PKEY_meth_set_digestverify, @function
EVP_PKEY_meth_set_digestverify:
.LFB1417:
	.cfi_startproc
	endbr64
	movq	%rsi, 216(%rdi)
	ret
	.cfi_endproc
.LFE1417:
	.size	EVP_PKEY_meth_set_digestverify, .-EVP_PKEY_meth_set_digestverify
	.p2align 4
	.globl	EVP_PKEY_meth_set_check
	.type	EVP_PKEY_meth_set_check, @function
EVP_PKEY_meth_set_check:
.LFB1418:
	.cfi_startproc
	endbr64
	movq	%rsi, 224(%rdi)
	ret
	.cfi_endproc
.LFE1418:
	.size	EVP_PKEY_meth_set_check, .-EVP_PKEY_meth_set_check
	.p2align 4
	.globl	EVP_PKEY_meth_set_public_check
	.type	EVP_PKEY_meth_set_public_check, @function
EVP_PKEY_meth_set_public_check:
.LFB1419:
	.cfi_startproc
	endbr64
	movq	%rsi, 232(%rdi)
	ret
	.cfi_endproc
.LFE1419:
	.size	EVP_PKEY_meth_set_public_check, .-EVP_PKEY_meth_set_public_check
	.p2align 4
	.globl	EVP_PKEY_meth_set_param_check
	.type	EVP_PKEY_meth_set_param_check, @function
EVP_PKEY_meth_set_param_check:
.LFB1420:
	.cfi_startproc
	endbr64
	movq	%rsi, 240(%rdi)
	ret
	.cfi_endproc
.LFE1420:
	.size	EVP_PKEY_meth_set_param_check, .-EVP_PKEY_meth_set_param_check
	.p2align 4
	.globl	EVP_PKEY_meth_set_digest_custom
	.type	EVP_PKEY_meth_set_digest_custom, @function
EVP_PKEY_meth_set_digest_custom:
.LFB1421:
	.cfi_startproc
	endbr64
	movq	%rsi, 248(%rdi)
	ret
	.cfi_endproc
.LFE1421:
	.size	EVP_PKEY_meth_set_digest_custom, .-EVP_PKEY_meth_set_digest_custom
	.p2align 4
	.globl	EVP_PKEY_meth_get_init
	.type	EVP_PKEY_meth_get_init, @function
EVP_PKEY_meth_get_init:
.LFB1422:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	movq	%rax, (%rsi)
	ret
	.cfi_endproc
.LFE1422:
	.size	EVP_PKEY_meth_get_init, .-EVP_PKEY_meth_get_init
	.p2align 4
	.globl	EVP_PKEY_meth_get_copy
	.type	EVP_PKEY_meth_get_copy, @function
EVP_PKEY_meth_get_copy:
.LFB1423:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	movq	%rax, (%rsi)
	ret
	.cfi_endproc
.LFE1423:
	.size	EVP_PKEY_meth_get_copy, .-EVP_PKEY_meth_get_copy
	.p2align 4
	.globl	EVP_PKEY_meth_get_cleanup
	.type	EVP_PKEY_meth_get_cleanup, @function
EVP_PKEY_meth_get_cleanup:
.LFB1424:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	movq	%rax, (%rsi)
	ret
	.cfi_endproc
.LFE1424:
	.size	EVP_PKEY_meth_get_cleanup, .-EVP_PKEY_meth_get_cleanup
	.p2align 4
	.globl	EVP_PKEY_meth_get_paramgen
	.type	EVP_PKEY_meth_get_paramgen, @function
EVP_PKEY_meth_get_paramgen:
.LFB1425:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L278
	movq	32(%rdi), %rax
	movq	%rax, (%rsi)
.L278:
	testq	%rdx, %rdx
	je	.L277
	movq	40(%rdi), %rax
	movq	%rax, (%rdx)
.L277:
	ret
	.cfi_endproc
.LFE1425:
	.size	EVP_PKEY_meth_get_paramgen, .-EVP_PKEY_meth_get_paramgen
	.p2align 4
	.globl	EVP_PKEY_meth_get_keygen
	.type	EVP_PKEY_meth_get_keygen, @function
EVP_PKEY_meth_get_keygen:
.LFB1426:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L287
	movq	48(%rdi), %rax
	movq	%rax, (%rsi)
.L287:
	testq	%rdx, %rdx
	je	.L286
	movq	56(%rdi), %rax
	movq	%rax, (%rdx)
.L286:
	ret
	.cfi_endproc
.LFE1426:
	.size	EVP_PKEY_meth_get_keygen, .-EVP_PKEY_meth_get_keygen
	.p2align 4
	.globl	EVP_PKEY_meth_get_sign
	.type	EVP_PKEY_meth_get_sign, @function
EVP_PKEY_meth_get_sign:
.LFB1427:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L296
	movq	64(%rdi), %rax
	movq	%rax, (%rsi)
.L296:
	testq	%rdx, %rdx
	je	.L295
	movq	72(%rdi), %rax
	movq	%rax, (%rdx)
.L295:
	ret
	.cfi_endproc
.LFE1427:
	.size	EVP_PKEY_meth_get_sign, .-EVP_PKEY_meth_get_sign
	.p2align 4
	.globl	EVP_PKEY_meth_get_verify
	.type	EVP_PKEY_meth_get_verify, @function
EVP_PKEY_meth_get_verify:
.LFB1428:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L305
	movq	80(%rdi), %rax
	movq	%rax, (%rsi)
.L305:
	testq	%rdx, %rdx
	je	.L304
	movq	88(%rdi), %rax
	movq	%rax, (%rdx)
.L304:
	ret
	.cfi_endproc
.LFE1428:
	.size	EVP_PKEY_meth_get_verify, .-EVP_PKEY_meth_get_verify
	.p2align 4
	.globl	EVP_PKEY_meth_get_verify_recover
	.type	EVP_PKEY_meth_get_verify_recover, @function
EVP_PKEY_meth_get_verify_recover:
.LFB1429:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L314
	movq	96(%rdi), %rax
	movq	%rax, (%rsi)
.L314:
	testq	%rdx, %rdx
	je	.L313
	movq	104(%rdi), %rax
	movq	%rax, (%rdx)
.L313:
	ret
	.cfi_endproc
.LFE1429:
	.size	EVP_PKEY_meth_get_verify_recover, .-EVP_PKEY_meth_get_verify_recover
	.p2align 4
	.globl	EVP_PKEY_meth_get_signctx
	.type	EVP_PKEY_meth_get_signctx, @function
EVP_PKEY_meth_get_signctx:
.LFB1430:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L323
	movq	112(%rdi), %rax
	movq	%rax, (%rsi)
.L323:
	testq	%rdx, %rdx
	je	.L322
	movq	120(%rdi), %rax
	movq	%rax, (%rdx)
.L322:
	ret
	.cfi_endproc
.LFE1430:
	.size	EVP_PKEY_meth_get_signctx, .-EVP_PKEY_meth_get_signctx
	.p2align 4
	.globl	EVP_PKEY_meth_get_verifyctx
	.type	EVP_PKEY_meth_get_verifyctx, @function
EVP_PKEY_meth_get_verifyctx:
.LFB1431:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L332
	movq	128(%rdi), %rax
	movq	%rax, (%rsi)
.L332:
	testq	%rdx, %rdx
	je	.L331
	movq	136(%rdi), %rax
	movq	%rax, (%rdx)
.L331:
	ret
	.cfi_endproc
.LFE1431:
	.size	EVP_PKEY_meth_get_verifyctx, .-EVP_PKEY_meth_get_verifyctx
	.p2align 4
	.globl	EVP_PKEY_meth_get_encrypt
	.type	EVP_PKEY_meth_get_encrypt, @function
EVP_PKEY_meth_get_encrypt:
.LFB1432:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L341
	movq	144(%rdi), %rax
	movq	%rax, (%rsi)
.L341:
	testq	%rdx, %rdx
	je	.L340
	movq	152(%rdi), %rax
	movq	%rax, (%rdx)
.L340:
	ret
	.cfi_endproc
.LFE1432:
	.size	EVP_PKEY_meth_get_encrypt, .-EVP_PKEY_meth_get_encrypt
	.p2align 4
	.globl	EVP_PKEY_meth_get_decrypt
	.type	EVP_PKEY_meth_get_decrypt, @function
EVP_PKEY_meth_get_decrypt:
.LFB1433:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L350
	movq	160(%rdi), %rax
	movq	%rax, (%rsi)
.L350:
	testq	%rdx, %rdx
	je	.L349
	movq	168(%rdi), %rax
	movq	%rax, (%rdx)
.L349:
	ret
	.cfi_endproc
.LFE1433:
	.size	EVP_PKEY_meth_get_decrypt, .-EVP_PKEY_meth_get_decrypt
	.p2align 4
	.globl	EVP_PKEY_meth_get_derive
	.type	EVP_PKEY_meth_get_derive, @function
EVP_PKEY_meth_get_derive:
.LFB1434:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L359
	movq	176(%rdi), %rax
	movq	%rax, (%rsi)
.L359:
	testq	%rdx, %rdx
	je	.L358
	movq	184(%rdi), %rax
	movq	%rax, (%rdx)
.L358:
	ret
	.cfi_endproc
.LFE1434:
	.size	EVP_PKEY_meth_get_derive, .-EVP_PKEY_meth_get_derive
	.p2align 4
	.globl	EVP_PKEY_meth_get_ctrl
	.type	EVP_PKEY_meth_get_ctrl, @function
EVP_PKEY_meth_get_ctrl:
.LFB1435:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L368
	movq	192(%rdi), %rax
	movq	%rax, (%rsi)
.L368:
	testq	%rdx, %rdx
	je	.L367
	movq	200(%rdi), %rax
	movq	%rax, (%rdx)
.L367:
	ret
	.cfi_endproc
.LFE1435:
	.size	EVP_PKEY_meth_get_ctrl, .-EVP_PKEY_meth_get_ctrl
	.p2align 4
	.globl	EVP_PKEY_meth_get_digestsign
	.type	EVP_PKEY_meth_get_digestsign, @function
EVP_PKEY_meth_get_digestsign:
.LFB1436:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L376
	movq	208(%rdi), %rax
	movq	%rax, (%rsi)
.L376:
	ret
	.cfi_endproc
.LFE1436:
	.size	EVP_PKEY_meth_get_digestsign, .-EVP_PKEY_meth_get_digestsign
	.p2align 4
	.globl	EVP_PKEY_meth_get_digestverify
	.type	EVP_PKEY_meth_get_digestverify, @function
EVP_PKEY_meth_get_digestverify:
.LFB1437:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L381
	movq	216(%rdi), %rax
	movq	%rax, (%rsi)
.L381:
	ret
	.cfi_endproc
.LFE1437:
	.size	EVP_PKEY_meth_get_digestverify, .-EVP_PKEY_meth_get_digestverify
	.p2align 4
	.globl	EVP_PKEY_meth_get_check
	.type	EVP_PKEY_meth_get_check, @function
EVP_PKEY_meth_get_check:
.LFB1438:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L386
	movq	224(%rdi), %rax
	movq	%rax, (%rsi)
.L386:
	ret
	.cfi_endproc
.LFE1438:
	.size	EVP_PKEY_meth_get_check, .-EVP_PKEY_meth_get_check
	.p2align 4
	.globl	EVP_PKEY_meth_get_public_check
	.type	EVP_PKEY_meth_get_public_check, @function
EVP_PKEY_meth_get_public_check:
.LFB1439:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L391
	movq	232(%rdi), %rax
	movq	%rax, (%rsi)
.L391:
	ret
	.cfi_endproc
.LFE1439:
	.size	EVP_PKEY_meth_get_public_check, .-EVP_PKEY_meth_get_public_check
	.p2align 4
	.globl	EVP_PKEY_meth_get_param_check
	.type	EVP_PKEY_meth_get_param_check, @function
EVP_PKEY_meth_get_param_check:
.LFB1440:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L396
	movq	240(%rdi), %rax
	movq	%rax, (%rsi)
.L396:
	ret
	.cfi_endproc
.LFE1440:
	.size	EVP_PKEY_meth_get_param_check, .-EVP_PKEY_meth_get_param_check
	.p2align 4
	.globl	EVP_PKEY_meth_get_digest_custom
	.type	EVP_PKEY_meth_get_digest_custom, @function
EVP_PKEY_meth_get_digest_custom:
.LFB1441:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L401
	movq	248(%rdi), %rax
	movq	%rax, (%rsi)
.L401:
	ret
	.cfi_endproc
.LFE1441:
	.size	EVP_PKEY_meth_get_digest_custom, .-EVP_PKEY_meth_get_digest_custom
	.section	.data.rel,"aw"
	.align 32
	.type	standard_methods, @object
	.size	standard_methods, 144
standard_methods:
	.quad	rsa_pkey_meth
	.quad	dh_pkey_meth
	.quad	dsa_pkey_meth
	.quad	ec_pkey_meth
	.quad	hmac_pkey_meth
	.quad	cmac_pkey_meth
	.quad	rsa_pss_pkey_meth
	.quad	dhx_pkey_meth
	.quad	scrypt_pkey_meth
	.quad	tls1_prf_pkey_meth
	.quad	ecx25519_pkey_meth
	.quad	ecx448_pkey_meth
	.quad	hkdf_pkey_meth
	.quad	poly1305_pkey_meth
	.quad	siphash_pkey_meth
	.quad	ed25519_pkey_meth
	.quad	ed448_pkey_meth
	.quad	sm2_pkey_meth
	.local	app_pkey_methods
	.comm	app_pkey_methods,8,8
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
