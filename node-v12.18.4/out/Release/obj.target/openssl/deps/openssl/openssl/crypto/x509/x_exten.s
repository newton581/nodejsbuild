	.file	"x_exten.c"
	.text
	.p2align 4
	.globl	d2i_X509_EXTENSION
	.type	d2i_X509_EXTENSION, @function
d2i_X509_EXTENSION:
.LFB816:
	.cfi_startproc
	endbr64
	leaq	X509_EXTENSION_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE816:
	.size	d2i_X509_EXTENSION, .-d2i_X509_EXTENSION
	.p2align 4
	.globl	i2d_X509_EXTENSION
	.type	i2d_X509_EXTENSION, @function
i2d_X509_EXTENSION:
.LFB817:
	.cfi_startproc
	endbr64
	leaq	X509_EXTENSION_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE817:
	.size	i2d_X509_EXTENSION, .-i2d_X509_EXTENSION
	.p2align 4
	.globl	X509_EXTENSION_new
	.type	X509_EXTENSION_new, @function
X509_EXTENSION_new:
.LFB818:
	.cfi_startproc
	endbr64
	leaq	X509_EXTENSION_it(%rip), %rdi
	jmp	ASN1_item_new@PLT
	.cfi_endproc
.LFE818:
	.size	X509_EXTENSION_new, .-X509_EXTENSION_new
	.p2align 4
	.globl	X509_EXTENSION_free
	.type	X509_EXTENSION_free, @function
X509_EXTENSION_free:
.LFB819:
	.cfi_startproc
	endbr64
	leaq	X509_EXTENSION_it(%rip), %rsi
	jmp	ASN1_item_free@PLT
	.cfi_endproc
.LFE819:
	.size	X509_EXTENSION_free, .-X509_EXTENSION_free
	.p2align 4
	.globl	d2i_X509_EXTENSIONS
	.type	d2i_X509_EXTENSIONS, @function
d2i_X509_EXTENSIONS:
.LFB820:
	.cfi_startproc
	endbr64
	leaq	X509_EXTENSIONS_it(%rip), %rcx
	jmp	ASN1_item_d2i@PLT
	.cfi_endproc
.LFE820:
	.size	d2i_X509_EXTENSIONS, .-d2i_X509_EXTENSIONS
	.p2align 4
	.globl	i2d_X509_EXTENSIONS
	.type	i2d_X509_EXTENSIONS, @function
i2d_X509_EXTENSIONS:
.LFB821:
	.cfi_startproc
	endbr64
	leaq	X509_EXTENSIONS_it(%rip), %rdx
	jmp	ASN1_item_i2d@PLT
	.cfi_endproc
.LFE821:
	.size	i2d_X509_EXTENSIONS, .-i2d_X509_EXTENSIONS
	.p2align 4
	.globl	X509_EXTENSION_dup
	.type	X509_EXTENSION_dup, @function
X509_EXTENSION_dup:
.LFB822:
	.cfi_startproc
	endbr64
	movq	%rdi, %rsi
	leaq	X509_EXTENSION_it(%rip), %rdi
	jmp	ASN1_item_dup@PLT
	.cfi_endproc
.LFE822:
	.size	X509_EXTENSION_dup, .-X509_EXTENSION_dup
	.globl	X509_EXTENSIONS_it
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"X509_EXTENSIONS"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	X509_EXTENSIONS_it, @object
	.size	X509_EXTENSIONS_it, 56
X509_EXTENSIONS_it:
	.byte	0
	.zero	7
	.quad	-1
	.quad	X509_EXTENSIONS_item_tt
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC0
	.section	.rodata.str1.1
.LC1:
	.string	"Extension"
	.section	.data.rel.ro.local
	.align 32
	.type	X509_EXTENSIONS_item_tt, @object
	.size	X509_EXTENSIONS_item_tt, 40
X509_EXTENSIONS_item_tt:
	.quad	4
	.quad	0
	.quad	0
	.quad	.LC1
	.quad	X509_EXTENSION_it
	.globl	X509_EXTENSION_it
	.section	.rodata.str1.1
.LC2:
	.string	"X509_EXTENSION"
	.section	.data.rel.ro.local
	.align 32
	.type	X509_EXTENSION_it, @object
	.size	X509_EXTENSION_it, 56
X509_EXTENSION_it:
	.byte	1
	.zero	7
	.quad	16
	.quad	X509_EXTENSION_seq_tt
	.quad	3
	.quad	0
	.quad	40
	.quad	.LC2
	.section	.rodata.str1.1
.LC3:
	.string	"object"
.LC4:
	.string	"critical"
.LC5:
	.string	"value"
	.section	.data.rel.ro,"aw"
	.align 32
	.type	X509_EXTENSION_seq_tt, @object
	.size	X509_EXTENSION_seq_tt, 120
X509_EXTENSION_seq_tt:
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC3
	.quad	ASN1_OBJECT_it
	.quad	1
	.quad	0
	.quad	8
	.quad	.LC4
	.quad	ASN1_BOOLEAN_it
	.quad	4096
	.quad	0
	.quad	16
	.quad	.LC5
	.quad	ASN1_OCTET_STRING_it
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
